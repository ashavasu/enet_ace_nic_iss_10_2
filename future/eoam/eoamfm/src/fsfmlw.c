/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsfmlw.c,v 1.11 2014/05/16 13:28:25 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include "emfminc.h"
#include "emfmcli.h"

extern UINT4        fsfm[8];
extern UINT4        FsFmSystemControl[10];
PRIVATE VOID        FmNotifyProtocolShutdownStatus (VOID);

PRIVATE INT4 FmSnmpLowValidateIfIndex PROTO ((INT4 i4IfIndex,
                                              UINT4 *pu4ErrorCode));
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsFmSystemControl
 Input       :  The Indices

                The Object 
                retValFsFmSystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFmSystemControl (INT4 *pi4RetValFsFmSystemControl)
{
    *pi4RetValFsFmSystemControl = (INT4) gFmGlobalInfo.u1SystemCtrlStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsFmModuleStatus
 Input       :  The Indices

                The Object 
                retValFsFmModuleStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFmModuleStatus (INT4 *pi4RetValFsFmModuleStatus)
{
    *pi4RetValFsFmModuleStatus = (INT4) gFmGlobalInfo.u1ModuleStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsFmTraceOption
 Input       :  The Indices

                The Object 
                retValFsFmTraceOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFmTraceOption (INT4 *pi4RetValFsFmTraceOption)
{
    *pi4RetValFsFmTraceOption = (INT4) gFmGlobalInfo.u4TraceOption;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsFmSystemControl
 Input       :  The Indices

                The Object 
                setValFsFmSystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsFmSystemControl (INT4 i4SetValFsFmSystemControl)
{
    if (i4SetValFsFmSystemControl == (INT4) gFmGlobalInfo.u1SystemCtrlStatus)
    {
        FM_TRC ((MGMT_TRC,
                 "SNMP: FM is already in the same System Control state\n"));
        return SNMP_SUCCESS;
    }

    if (i4SetValFsFmSystemControl == FM_START)
    {
        if (FmMainStart () != OSIX_SUCCESS)
        {
            FM_TRC ((MGMT_TRC | ALL_FAILURE_TRC,
                     "SNMP: FM Initialization FAILED\n"));
            FmMainShutDown ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (FmMainShutDown () != OSIX_SUCCESS)
        {
            FM_TRC ((MGMT_TRC | ALL_FAILURE_TRC, "SNMP: FM Shutdown FAILED\n"));
            return SNMP_FAILURE;
        }
        /* Notify MSR with the eoamfm oids */
        FmNotifyProtocolShutdownStatus ();
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsFmModuleStatus
 Input       :  The Indices

                The Object 
                setValFsFmModuleStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsFmModuleStatus (INT4 i4SetValFsFmModuleStatus)
{
    if (gFmGlobalInfo.u1SystemCtrlStatus != FM_START)
    {
        FM_TRC ((MGMT_TRC | ALL_FAILURE_TRC,
                 "SNMP: FM System Shutdown, Cannot Enable/Disable FM\n"));
        return SNMP_FAILURE;
    }

    if (i4SetValFsFmModuleStatus == (INT4) gFmGlobalInfo.u1ModuleStatus)
    {
        FM_TRC ((MGMT_TRC, "SNMP: FM Module already has the same status\n"));
        return SNMP_SUCCESS;
    }

    if (i4SetValFsFmModuleStatus == FM_ENABLED)
    {
        if (FmMainEnable () != OSIX_SUCCESS)
        {
            FM_TRC ((MGMT_TRC | ALL_FAILURE_TRC,
                     "SNMP: FM Module Enable FAILED\n"));
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (FmMainDisable () != OSIX_SUCCESS)
        {
            FM_TRC ((MGMT_TRC | ALL_FAILURE_TRC,
                     "SNMP: FM Module Disable FAILED\n"));
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsFmTraceOption
 Input       :  The Indices

                The Object 
                setValFsFmTraceOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsFmTraceOption (INT4 i4SetValFsFmTraceOption)
{
    gFmGlobalInfo.u4TraceOption = (UINT4) i4SetValFsFmTraceOption;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsFmSystemControl
 Input       :  The Indices

                The Object 
                testValFsFmSystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsFmSystemControl (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsFmSystemControl)
{
    if ((i4TestValFsFmSystemControl == FM_START) ||
        (i4TestValFsFmSystemControl == FM_SHUTDOWN))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsFmModuleStatus
 Input       :  The Indices

                The Object 
                testValFsFmModuleStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsFmModuleStatus (UINT4 *pu4ErrorCode, INT4 i4TestValFsFmModuleStatus)
{
    if ((i4TestValFsFmModuleStatus == FM_ENABLED) ||
        (i4TestValFsFmModuleStatus == FM_DISABLED))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsFmTraceOption
 Input       :  The Indices

                The Object 
                testValFsFmTraceOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsFmTraceOption (UINT4 *pu4ErrorCode, INT4 i4TestValFsFmTraceOption)
{
    if (i4TestValFsFmTraceOption < 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsFmSystemControl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsFmSystemControl (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsFmModuleStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsFmModuleStatus (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsFmTraceOption
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsFmTraceOption (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsFmLinkEventTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsFmLinkEventTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsFmLinkEventTable (INT4 i4IfIndex)
{
    UINT4               u4ErrorCode = 0;
    if (gFmGlobalInfo.u1SystemCtrlStatus != FM_START)
    {
        return SNMP_FAILURE;
    }
    if (FmSnmpLowValidateIfIndex (i4IfIndex, &u4ErrorCode) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsFmLinkEventTable
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsFmLinkEventTable (INT4 *pi4IfIndex)
{
    UINT4               u4Index = 0;

    if (gFmGlobalInfo.u1SystemCtrlStatus != FM_START)
    {
        return SNMP_FAILURE;
    }
/* Get the First Used Index */
    for (u4Index = 1; u4Index <= FM_MAX_PORTS; u4Index++)
    {
        if ((FM_IF_ENTRY (u4Index).u4Port) == u4Index)
        {
            *pi4IfIndex = (INT4) u4Index;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsFmLinkEventTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsFmLinkEventTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    UINT4               u4Index = 0;

    if ((i4IfIndex < FM_MIN_PORTS) || (i4IfIndex > FM_MAX_PORTS))
    {
        return SNMP_FAILURE;
    }

    /* Next Index should always return the next used Index */
    for (u4Index = (i4IfIndex + 1); u4Index <= FM_MAX_PORTS; u4Index++)
    {
        if ((FM_IF_ENTRY (u4Index).u4Port) == u4Index)
        {
            *pi4NextIfIndex = (INT4) u4Index;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsFmSymPeriodAction
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsFmSymPeriodAction
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFmSymPeriodAction (INT4 i4IfIndex, INT4 *pi4RetValFsFmSymPeriodAction)
{
    UINT4               u4IfIndex = 0;

    u4IfIndex = (UINT4) i4IfIndex;
    *pi4RetValFsFmSymPeriodAction =
        (INT4) (FM_IF_ENTRY (u4IfIndex).EventInfo.SymPeriodAction);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsFmFrameAction
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsFmFrameAction
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFmFrameAction (INT4 i4IfIndex, INT4 *pi4RetValFsFmFrameAction)
{
    UINT4               u4IfIndex = 0;

    u4IfIndex = (UINT4) i4IfIndex;
    *pi4RetValFsFmFrameAction =
        (INT4) (FM_IF_ENTRY (u4IfIndex).EventInfo.FrameAction);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsFmFramePeriodAction
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsFmFramePeriodAction
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFmFramePeriodAction (INT4 i4IfIndex,
                             INT4 *pi4RetValFsFmFramePeriodAction)
{
    UINT4               u4IfIndex = 0;

    u4IfIndex = (UINT4) i4IfIndex;
    *pi4RetValFsFmFramePeriodAction =
        (INT4) (FM_IF_ENTRY (u4IfIndex).EventInfo.FramePeriodAction);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsFmFrameSecSummAction
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsFmFrameSecSummAction
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFmFrameSecSummAction (INT4 i4IfIndex,
                              INT4 *pi4RetValFsFmFrameSecSummAction)
{
    UINT4               u4IfIndex = 0;

    u4IfIndex = (UINT4) i4IfIndex;
    *pi4RetValFsFmFrameSecSummAction =
        (INT4) (FM_IF_ENTRY (u4IfIndex).EventInfo.FrameSecSummAction);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsFmCriticalEventAction
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsFmCriticalEventAction
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFmCriticalEventAction (INT4 i4IfIndex,
                               INT4 *pi4RetValFsFmCriticalEventAction)
{
    UINT4               u4IfIndex = 0;

    u4IfIndex = (UINT4) i4IfIndex;
    *pi4RetValFsFmCriticalEventAction =
        (INT4) (FM_IF_ENTRY (u4IfIndex).EventInfo.CriticalEventAction);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsFmDyingGaspAction
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsFmDyingGaspAction
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFmDyingGaspAction (INT4 i4IfIndex, INT4 *pi4RetValFsFmDyingGaspAction)
{
    UINT4               u4IfIndex = 0;

    u4IfIndex = (UINT4) i4IfIndex;
    *pi4RetValFsFmDyingGaspAction =
        (INT4) (FM_IF_ENTRY (u4IfIndex).EventInfo.DyingGaspAction);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsFmLinkFaultAction
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsFmLinkFaultAction
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFmLinkFaultAction (INT4 i4IfIndex, INT4 *pi4RetValFsFmLinkFaultAction)
{
    UINT4               u4IfIndex = 0;

    u4IfIndex = (UINT4) i4IfIndex;
    *pi4RetValFsFmLinkFaultAction =
        (INT4) (FM_IF_ENTRY (u4IfIndex).EventInfo.LinkFaultAction);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsFmSymPeriodAction
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsFmSymPeriodAction
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsFmSymPeriodAction (INT4 i4IfIndex, INT4 i4SetValFsFmSymPeriodAction)
{
    UINT4               u4IfIndex = 0;

    u4IfIndex = (UINT4) i4IfIndex;
    FM_IF_ENTRY (u4IfIndex).EventInfo.SymPeriodAction =
        i4SetValFsFmSymPeriodAction;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsFmFrameAction
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsFmFrameAction
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsFmFrameAction (INT4 i4IfIndex, INT4 i4SetValFsFmFrameAction)
{
    UINT4               u4IfIndex = 0;

    u4IfIndex = (UINT4) i4IfIndex;
    FM_IF_ENTRY (u4IfIndex).EventInfo.FrameAction = i4SetValFsFmFrameAction;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsFmFramePeriodAction
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsFmFramePeriodAction
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsFmFramePeriodAction (INT4 i4IfIndex, INT4 i4SetValFsFmFramePeriodAction)
{
    UINT4               u4IfIndex = 0;

    u4IfIndex = (UINT4) i4IfIndex;
    FM_IF_ENTRY (u4IfIndex).EventInfo.FramePeriodAction =
        i4SetValFsFmFramePeriodAction;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsFmFrameSecSummAction
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsFmFrameSecSummAction
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsFmFrameSecSummAction (INT4 i4IfIndex,
                              INT4 i4SetValFsFmFrameSecSummAction)
{
    UINT4               u4IfIndex = 0;

    u4IfIndex = (UINT4) i4IfIndex;
    FM_IF_ENTRY (u4IfIndex).EventInfo.FrameSecSummAction =
        i4SetValFsFmFrameSecSummAction;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsFmCriticalEventAction
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsFmCriticalEventAction
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsFmCriticalEventAction (INT4 i4IfIndex,
                               INT4 i4SetValFsFmCriticalEventAction)
{
    UINT4               u4IfIndex = 0;

    u4IfIndex = (UINT4) i4IfIndex;
    FM_IF_ENTRY (u4IfIndex).EventInfo.CriticalEventAction =
        i4SetValFsFmCriticalEventAction;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsFmDyingGaspAction
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsFmDyingGaspAction
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsFmDyingGaspAction (INT4 i4IfIndex, INT4 i4SetValFsFmDyingGaspAction)
{
    UINT4               u4IfIndex = 0;

    u4IfIndex = (UINT4) i4IfIndex;
    FM_IF_ENTRY (u4IfIndex).EventInfo.DyingGaspAction =
        i4SetValFsFmDyingGaspAction;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsFmLinkFaultAction
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsFmLinkFaultAction
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsFmLinkFaultAction (INT4 i4IfIndex, INT4 i4SetValFsFmLinkFaultAction)
{
    UINT4               u4IfIndex = 0;

    u4IfIndex = (UINT4) i4IfIndex;
    FM_IF_ENTRY (u4IfIndex).EventInfo.LinkFaultAction =
        i4SetValFsFmLinkFaultAction;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsFmSymPeriodAction
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsFmSymPeriodAction
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsFmSymPeriodAction (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                              INT4 i4TestValFsFmSymPeriodAction)
{
    if (gFmGlobalInfo.u1SystemCtrlStatus != FM_START)
    {
        FM_TRC ((MGMT_TRC | ALL_FAILURE_TRC, "SNMP: FM System is Shutdown\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (FmSnmpLowValidateIfIndex (i4IfIndex, pu4ErrorCode) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if ((i4TestValFsFmSymPeriodAction != FM_ACTION_NONE) &&
        (i4TestValFsFmSymPeriodAction != FM_ACTION_WARNING))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsFmFrameAction
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsFmFrameAction
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsFmFrameAction (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                          INT4 i4TestValFsFmFrameAction)
{
    if (gFmGlobalInfo.u1SystemCtrlStatus != FM_START)
    {
        FM_TRC ((MGMT_TRC | ALL_FAILURE_TRC, "SNMP: FM System is Shutdown\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (FmSnmpLowValidateIfIndex (i4IfIndex, pu4ErrorCode) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if ((i4TestValFsFmFrameAction != FM_ACTION_NONE) &&
        (i4TestValFsFmFrameAction != FM_ACTION_WARNING))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsFmFramePeriodAction
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsFmFramePeriodAction
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsFmFramePeriodAction (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                INT4 i4TestValFsFmFramePeriodAction)
{
    if (gFmGlobalInfo.u1SystemCtrlStatus != FM_START)
    {
        FM_TRC ((MGMT_TRC | ALL_FAILURE_TRC, "SNMP: FM System is Shutdown\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (FmSnmpLowValidateIfIndex (i4IfIndex, pu4ErrorCode) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if ((i4TestValFsFmFramePeriodAction != FM_ACTION_NONE) &&
        (i4TestValFsFmFramePeriodAction != FM_ACTION_WARNING))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsFmFrameSecSummAction
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsFmFrameSecSummAction
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsFmFrameSecSummAction (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                 INT4 i4TestValFsFmFrameSecSummAction)
{
    if (gFmGlobalInfo.u1SystemCtrlStatus != FM_START)
    {
        FM_TRC ((MGMT_TRC | ALL_FAILURE_TRC, "SNMP: FM System is Shutdown\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (FmSnmpLowValidateIfIndex (i4IfIndex, pu4ErrorCode) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if ((i4TestValFsFmFrameSecSummAction != FM_ACTION_NONE) &&
        (i4TestValFsFmFrameSecSummAction != FM_ACTION_WARNING))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsFmCriticalEventAction
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsFmCriticalEventAction
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsFmCriticalEventAction (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                  INT4 i4TestValFsFmCriticalEventAction)
{
    if (gFmGlobalInfo.u1SystemCtrlStatus != FM_START)
    {
        FM_TRC ((MGMT_TRC | ALL_FAILURE_TRC, "SNMP: FM System is Shutdown\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (FmSnmpLowValidateIfIndex (i4IfIndex, pu4ErrorCode) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if ((i4TestValFsFmCriticalEventAction != FM_ACTION_NONE) &&
        (i4TestValFsFmCriticalEventAction != FM_ACTION_WARNING) &&
        (i4TestValFsFmCriticalEventAction != FM_ACTION_BLOCK))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsFmDyingGaspAction
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsFmDyingGaspAction
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsFmDyingGaspAction (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                              INT4 i4TestValFsFmDyingGaspAction)
{
    if (gFmGlobalInfo.u1SystemCtrlStatus != FM_START)
    {
        FM_TRC ((MGMT_TRC | ALL_FAILURE_TRC, "SNMP: FM System is Shutdown\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (FmSnmpLowValidateIfIndex (i4IfIndex, pu4ErrorCode) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if ((i4TestValFsFmDyingGaspAction != FM_ACTION_NONE) &&
        (i4TestValFsFmDyingGaspAction != FM_ACTION_WARNING) &&
        (i4TestValFsFmDyingGaspAction != FM_ACTION_BLOCK))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsFmLinkFaultAction
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsFmLinkFaultAction
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsFmLinkFaultAction (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                              INT4 i4TestValFsFmLinkFaultAction)
{
    if (gFmGlobalInfo.u1SystemCtrlStatus != FM_START)
    {
        FM_TRC ((MGMT_TRC | ALL_FAILURE_TRC, "SNMP: FM System is Shutdown\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (FmSnmpLowValidateIfIndex (i4IfIndex, pu4ErrorCode) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if ((i4TestValFsFmLinkFaultAction != FM_ACTION_NONE) &&
        (i4TestValFsFmLinkFaultAction != FM_ACTION_WARNING) &&
        (i4TestValFsFmLinkFaultAction != FM_ACTION_BLOCK))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsFmLinkEventTable
 Input       :  The Indices
                IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsFmLinkEventTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsFmLoopbackTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsFmLoopbackTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsFmLoopbackTable (INT4 i4IfIndex)
{
    UINT4               u4ErrorCode = 0;
    if (gFmGlobalInfo.u1SystemCtrlStatus != FM_START)
    {
        return SNMP_FAILURE;
    }
    if (FmSnmpLowValidateIfIndex (i4IfIndex, &u4ErrorCode) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsFmLoopbackTable
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsFmLoopbackTable (INT4 *pi4IfIndex)
{
    UINT4               u4Index = 0;

    if (gFmGlobalInfo.u1SystemCtrlStatus != FM_START)
    {
        return SNMP_FAILURE;
    }
/* Get the First Used Index */
    for (u4Index = 1; u4Index <= FM_MAX_PORTS; u4Index++)
    {
        if ((FM_IF_ENTRY (u4Index).u4Port) == u4Index)
        {
            *pi4IfIndex = (INT4) u4Index;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsFmLoopbackTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsFmLoopbackTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    UINT4               u4Index = 0;

    if ((i4IfIndex < FM_MIN_PORTS) || (i4IfIndex > FM_MAX_PORTS))
    {
        return SNMP_FAILURE;
    }

    /* Next Index should always return the next used Index */
    for (u4Index = (i4IfIndex + 1); u4Index <= FM_MAX_PORTS; u4Index++)
    {
        if ((FM_IF_ENTRY (u4Index).u4Port) == u4Index)
        {
            *pi4NextIfIndex = (INT4) u4Index;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsFmLoopbackStatus
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsFmLoopbackStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFmLoopbackStatus (INT4 i4IfIndex, INT4 *pi4RetValFsFmLoopbackStatus)
{
    UINT4               u4IfIndex = 0;

    u4IfIndex = (UINT4) i4IfIndex;
    *pi4RetValFsFmLoopbackStatus =
        (INT4) (FM_IF_ENTRY (u4IfIndex).LBTestInfo.u1LoopbackStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsFmLBTestPattern
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsFmLBTestPattern
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFmLBTestPattern (INT4 i4IfIndex, tSNMP_OCTET_STRING_TYPE *
                         pRetValFsFmLBTestPattern)
{
    UINT4               u4IfIndex = 0;

    u4IfIndex = (UINT4) i4IfIndex;
    MEMCPY (pRetValFsFmLBTestPattern->pu1_OctetList,
            (FM_IF_ENTRY (u4IfIndex).LBTestInfo.au1TestPattern),
            FM_LB_TEST_PATTERN_LENGTH);
    pRetValFsFmLBTestPattern->i4_Length = FM_LB_TEST_PATTERN_LENGTH;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsFmLBTestPktSize
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsFmLBTestPktSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFmLBTestPktSize (INT4 i4IfIndex, UINT4 *pu4RetValFsFmLBTestPktSize)
{
    UINT4               u4IfIndex = 0;

    u4IfIndex = (UINT4) i4IfIndex;
    *pu4RetValFsFmLBTestPktSize =
        (FM_IF_ENTRY (u4IfIndex).LBTestInfo.u4TestPktSize);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsFmLBTestCount
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsFmLBTestCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFmLBTestCount (INT4 i4IfIndex, UINT4 *pu4RetValFsFmLBTestCount)
{
    UINT4               u4IfIndex = 0;

    u4IfIndex = (UINT4) i4IfIndex;
    *pu4RetValFsFmLBTestCount =
        (FM_IF_ENTRY (u4IfIndex).LBTestInfo.u4TestPktCount);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsFmLBTestWaitTime
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsFmLBTestWaitTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFmLBTestWaitTime (INT4 i4IfIndex, INT4 *pi4RetValFsFmLBTestWaitTime)
{
    UINT4               u4IfIndex = 0;

    u4IfIndex = (UINT4) i4IfIndex;
    *pi4RetValFsFmLBTestWaitTime =
        (INT4) (FM_IF_ENTRY (u4IfIndex).LBTestInfo.u4TestWaitTime);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsFmLBTestCommand
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsFmLBTestCommand
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFmLBTestCommand (INT4 i4IfIndex, INT4 *pi4RetValFsFmLBTestCommand)
{
    UINT4               u4IfIndex = 0;

    u4IfIndex = (UINT4) i4IfIndex;
    *pi4RetValFsFmLBTestCommand =
        (INT4) (FM_IF_ENTRY (u4IfIndex).LBTestInfo.u1TestCommand);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsFmLBTestStatus
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsFmLBTestStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFmLBTestStatus (INT4 i4IfIndex, INT4 *pi4RetValFsFmLBTestStatus)
{
    UINT4               u4IfIndex = 0;

    u4IfIndex = (UINT4) i4IfIndex;
    *pi4RetValFsFmLBTestStatus =
        (INT4) (FM_IF_ENTRY (u4IfIndex).LBTestInfo.u1TestStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsFmLBTestStartTimestamp
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsFmLBTestStartTimestamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFmLBTestStartTimestamp (INT4 i4IfIndex, tSNMP_OCTET_STRING_TYPE *
                                pRetValFsFmLBTestStartTimestamp)
{
    UINT4               u4IfIndex = 0;
    CHR1                ac1Date[FM_MAX_DATE_LEN];

    MEMSET (ac1Date, 0, FM_MAX_DATE_LEN);
    u4IfIndex = (UINT4) i4IfIndex;
    if (FM_IF_ENTRY (u4IfIndex).LBTestInfo.CurrSessionStats.StartTime != 0)
    {
        UtlGetTimeStrForTicks (FM_IF_ENTRY (u4IfIndex).LBTestInfo.
                               CurrSessionStats.StartTime, ac1Date);
    }
    MEMCPY (pRetValFsFmLBTestStartTimestamp->pu1_OctetList, ac1Date,
            FM_MAX_DATE_LEN);
    pRetValFsFmLBTestStartTimestamp->i4_Length = FM_MAX_DATE_LEN;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsFmLBTestEndTimestamp
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsFmLBTestEndTimestamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFmLBTestEndTimestamp (INT4 i4IfIndex, tSNMP_OCTET_STRING_TYPE *
                              pRetValFsFmLBTestEndTimestamp)
{
    UINT4               u4IfIndex = 0;
    CHR1                ac1Date[FM_MAX_DATE_LEN];

    MEMSET (ac1Date, 0, FM_MAX_DATE_LEN);
    u4IfIndex = (UINT4) i4IfIndex;
    if (FM_IF_ENTRY (u4IfIndex).LBTestInfo.CurrSessionStats.EndTime != 0)
    {
        UtlGetTimeStrForTicks (FM_IF_ENTRY (u4IfIndex).LBTestInfo.
                               CurrSessionStats.EndTime, ac1Date);
    }
    MEMCPY (pRetValFsFmLBTestEndTimestamp->pu1_OctetList, ac1Date,
            FM_MAX_DATE_LEN);
    pRetValFsFmLBTestEndTimestamp->i4_Length = FM_MAX_DATE_LEN;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsFmLBTestTxCount
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsFmLBTestTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFmLBTestTxCount (INT4 i4IfIndex, UINT4 *pu4RetValFsFmLBTestTxCount)
{
    UINT4               u4IfIndex = 0;

    u4IfIndex = (UINT4) i4IfIndex;
    *pu4RetValFsFmLBTestTxCount =
        (FM_IF_ENTRY (u4IfIndex).LBTestInfo.CurrSessionStats.u4TxPktCount);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsFmLBTestRxCount
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsFmLBTestRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFmLBTestRxCount (INT4 i4IfIndex, UINT4 *pu4RetValFsFmLBTestRxCount)
{
    UINT4               u4IfIndex = 0;

    u4IfIndex = (UINT4) i4IfIndex;
    *pu4RetValFsFmLBTestRxCount =
        (FM_IF_ENTRY (u4IfIndex).LBTestInfo.CurrSessionStats.u4RxPktCount);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsFmLBTestMatchCount
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsFmLBTestMatchCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFmLBTestMatchCount (INT4 i4IfIndex,
                            UINT4 *pu4RetValFsFmLBTestMatchCount)
{
    UINT4               u4IfIndex = 0;

    u4IfIndex = (UINT4) i4IfIndex;
    *pu4RetValFsFmLBTestMatchCount =
        (FM_IF_ENTRY (u4IfIndex).LBTestInfo.CurrSessionStats.u4MatchPktCount);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsFmLBTestPattern
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsFmLBTestPattern
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsFmLBTestPattern (INT4 i4IfIndex,
                         tSNMP_OCTET_STRING_TYPE * pSetValFsFmLBTestPattern)
{
    UINT4               u4IfIndex = 0;

    u4IfIndex = (UINT4) i4IfIndex;
    MEMCPY ((FM_IF_ENTRY (u4IfIndex).LBTestInfo.au1TestPattern),
            pSetValFsFmLBTestPattern->pu1_OctetList, FM_LB_TEST_PATTERN_LENGTH);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsFmLBTestPktSize
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsFmLBTestPktSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsFmLBTestPktSize (INT4 i4IfIndex, UINT4 u4SetValFsFmLBTestPktSize)
{
    UINT4               u4IfIndex = 0;

    u4IfIndex = (UINT4) i4IfIndex;
    (FM_IF_ENTRY (u4IfIndex).LBTestInfo.u4TestPktSize) =
        u4SetValFsFmLBTestPktSize;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsFmLBTestCount
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsFmLBTestCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsFmLBTestCount (INT4 i4IfIndex, UINT4 u4SetValFsFmLBTestCount)
{
    UINT4               u4IfIndex = 0;

    u4IfIndex = (UINT4) i4IfIndex;
    (FM_IF_ENTRY (u4IfIndex).LBTestInfo.u4TestPktCount) =
        u4SetValFsFmLBTestCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsFmLBTestWaitTime
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsFmLBTestWaitTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsFmLBTestWaitTime (INT4 i4IfIndex, INT4 i4SetValFsFmLBTestWaitTime)
{
    UINT4               u4IfIndex = 0;

    u4IfIndex = (UINT4) i4IfIndex;
    (FM_IF_ENTRY (u4IfIndex).LBTestInfo.u4TestWaitTime) =
        (UINT4) i4SetValFsFmLBTestWaitTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsFmLBTestCommand
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsFmLBTestCommand
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsFmLBTestCommand (INT4 i4IfIndex, INT4 i4SetValFsFmLBTestCommand)
{
    UINT4               u4IfIndex = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    u4IfIndex = (UINT4) i4IfIndex;
    if (i4SetValFsFmLBTestCommand == FM_NO_LB_TEST)
    {
        /*Configuration of this has no effect so return success */
        return SNMP_SUCCESS;
    }
    (FM_IF_ENTRY (u4IfIndex).LBTestInfo.u1TestCommand) =
        (UINT1) i4SetValFsFmLBTestCommand;
    if (FmFuncPerformLoopbackTest (&FM_IF_ENTRY (u4IfIndex)) != OSIX_SUCCESS)
    {
        i1RetVal = SNMP_FAILURE;
    }
    (FM_IF_ENTRY (u4IfIndex).LBTestInfo.u1TestCommand) = (UINT1) FM_NO_LB_TEST;
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsFmLBTestPattern
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsFmLBTestPattern
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsFmLBTestPattern (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                            tSNMP_OCTET_STRING_TYPE * pTestValFsFmLBTestPattern)
{
    if (gFmGlobalInfo.u1SystemCtrlStatus != FM_START)
    {
        FM_TRC ((MGMT_TRC | ALL_FAILURE_TRC, "SNMP: FM System is Shutdown\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (FmSnmpLowValidateIfIndex (i4IfIndex, pu4ErrorCode) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (pTestValFsFmLBTestPattern->i4_Length != FM_LB_TEST_PATTERN_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsFmLBTestPktSize
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsFmLBTestPktSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsFmLBTestPktSize (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                            UINT4 u4TestValFsFmLBTestPktSize)
{
    if (gFmGlobalInfo.u1SystemCtrlStatus != FM_START)
    {
        FM_TRC ((MGMT_TRC | ALL_FAILURE_TRC, "SNMP: FM System is Shutdown\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (FmSnmpLowValidateIfIndex (i4IfIndex, pu4ErrorCode) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if ((u4TestValFsFmLBTestPktSize < FM_DEFAULT_TEST_PKT_SIZE) ||
        (u4TestValFsFmLBTestPktSize > FM_MAX_TEST_PKT_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsFmLBTestCount
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsFmLBTestCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsFmLBTestCount (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                          UINT4 u4TestValFsFmLBTestCount)
{
    if (gFmGlobalInfo.u1SystemCtrlStatus != FM_START)
    {
        FM_TRC ((MGMT_TRC | ALL_FAILURE_TRC, "SNMP: FM System is Shutdown\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (FmSnmpLowValidateIfIndex (i4IfIndex, pu4ErrorCode) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if ((u4TestValFsFmLBTestCount < FM_LB_TEST_PKT_MIN_COUNT) ||
        (u4TestValFsFmLBTestCount > FM_LB_TEST_PKT_MAX_COUNT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsFmLBTestWaitTime
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsFmLBTestWaitTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsFmLBTestWaitTime (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                             INT4 i4TestValFsFmLBTestWaitTime)
{
    if (gFmGlobalInfo.u1SystemCtrlStatus != FM_START)
    {
        FM_TRC ((MGMT_TRC | ALL_FAILURE_TRC, "SNMP: FM System is Shutdown\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (FmSnmpLowValidateIfIndex (i4IfIndex, pu4ErrorCode) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if ((i4TestValFsFmLBTestWaitTime < 1) ||
        (i4TestValFsFmLBTestWaitTime > FM_MAX_TEST_WAIT_TIME))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsFmLBTestCommand
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsFmLBTestCommand
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsFmLBTestCommand (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                            INT4 i4TestValFsFmLBTestCommand)
{
    UINT4               u4IfIndex = 0;

    u4IfIndex = (UINT4) i4IfIndex;
    if (gFmGlobalInfo.u1SystemCtrlStatus != FM_START)
    {
        FM_TRC ((MGMT_TRC | ALL_FAILURE_TRC, "SNMP: FM System is Shutdown\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (FmSnmpLowValidateIfIndex (i4IfIndex, pu4ErrorCode) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* This test is applicable only for snmpset operations
     * so CLI_SET_ERR is invalid here */
    if (i4TestValFsFmLBTestCommand == FM_NO_LB_TEST)
    {
        /*Configuration of this has no effect so return success */
        return SNMP_SUCCESS;
    }
    if (i4TestValFsFmLBTestCommand != FM_START_LB_TEST)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (FM_IF_ENTRY (u4IfIndex).LBTestInfo.u1LoopbackStatus != FM_REMOTE_LB)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (FM_CLI_ERR_REMOTE_NOT_IN_LB);
        return SNMP_FAILURE;
    }
    if (EoamApiIsEnabledOnPort (u4IfIndex) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (FM_CLI_ERR_EOAM_NOT_STARTED_ON_PORT);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsFmLoopbackTable
 Input       :  The Indices
                IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsFmLoopbackTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsFmLBStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsFmLBStatsTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsFmLBStatsTable (INT4 i4IfIndex)
{
    UINT4               u4ErrorCode = 0;
    if (gFmGlobalInfo.u1SystemCtrlStatus != FM_START)
    {
        return SNMP_FAILURE;
    }
    if (FmSnmpLowValidateIfIndex (i4IfIndex, &u4ErrorCode) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsFmLBStatsTable
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsFmLBStatsTable (INT4 *pi4IfIndex)
{
    UINT4               u4Index = 0;

    if (gFmGlobalInfo.u1SystemCtrlStatus != FM_START)
    {
        return SNMP_FAILURE;
    }
/* Get the First Used Index */
    for (u4Index = 1; u4Index <= FM_MAX_PORTS; u4Index++)
    {
        if ((FM_IF_ENTRY (u4Index).u4Port) == u4Index)
        {
            *pi4IfIndex = (INT4) u4Index;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsFmLBStatsTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsFmLBStatsTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    UINT4               u4Index = 0;

    if ((i4IfIndex < FM_MIN_PORTS) || (i4IfIndex > FM_MAX_PORTS))
    {
        return SNMP_FAILURE;
    }

    /* Next Index should always return the next used Index */
    for (u4Index = (i4IfIndex + 1); u4Index <= FM_MAX_PORTS; u4Index++)
    {
        if ((FM_IF_ENTRY (u4Index).u4Port) == u4Index)
        {
            *pi4NextIfIndex = (INT4) u4Index;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsFmLBStatsStartTimestamp
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsFmLBStatsStartTimestamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFmLBStatsStartTimestamp (INT4 i4IfIndex, tSNMP_OCTET_STRING_TYPE *
                                 pRetValFsFmLBTestStartTimestamp)
{
    UINT4               u4IfIndex = 0;
    CHR1                ac1Date[FM_MAX_DATE_LEN];

    MEMSET (ac1Date, 0, FM_MAX_DATE_LEN);
    u4IfIndex = (UINT4) i4IfIndex;
    if (FM_IF_ENTRY (u4IfIndex).PrevSessionStats.StartTime != 0)
    {
        UtlGetTimeStrForTicks (FM_IF_ENTRY (u4IfIndex).PrevSessionStats.
                               StartTime, ac1Date);
    }
    MEMCPY (pRetValFsFmLBTestStartTimestamp->pu1_OctetList, ac1Date,
            FM_MAX_DATE_LEN);
    pRetValFsFmLBTestStartTimestamp->i4_Length = FM_MAX_DATE_LEN;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsFmLBStatsEndTimestamp
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsFmLBStatsEndTimestamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFmLBStatsEndTimestamp (INT4 i4IfIndex, tSNMP_OCTET_STRING_TYPE *
                               pRetValFsFmLBTestEndTimestamp)
{
    UINT4               u4IfIndex = 0;
    CHR1                ac1Date[FM_MAX_DATE_LEN];

    MEMSET (ac1Date, 0, FM_MAX_DATE_LEN);
    u4IfIndex = (UINT4) i4IfIndex;
    if (FM_IF_ENTRY (u4IfIndex).PrevSessionStats.EndTime != 0)
    {
        UtlGetTimeStrForTicks (FM_IF_ENTRY (u4IfIndex).PrevSessionStats.EndTime,
                               ac1Date);
    }
    MEMCPY (pRetValFsFmLBTestEndTimestamp->pu1_OctetList, ac1Date,
            FM_MAX_DATE_LEN);
    pRetValFsFmLBTestEndTimestamp->i4_Length = FM_MAX_DATE_LEN;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsFmLBStatsTxCount
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsFmLBStatsTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFmLBStatsTxCount (INT4 i4IfIndex, UINT4 *pu4RetValFsFmLBStatsTxCount)
{
    UINT4               u4IfIndex = 0;

    u4IfIndex = (UINT4) i4IfIndex;
    *pu4RetValFsFmLBStatsTxCount =
        (FM_IF_ENTRY (u4IfIndex).PrevSessionStats.u4TxPktCount);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsFmLBStatsRxCount
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsFmLBStatsRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFmLBStatsRxCount (INT4 i4IfIndex, UINT4 *pu4RetValFsFmLBStatsRxCount)
{
    UINT4               u4IfIndex = 0;

    u4IfIndex = (UINT4) i4IfIndex;
    *pu4RetValFsFmLBStatsRxCount =
        (FM_IF_ENTRY (u4IfIndex).PrevSessionStats.u4RxPktCount);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsFmLBStatsMatchCount
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsFmLBStatsMatchCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFmLBStatsMatchCount (INT4 i4IfIndex,
                             UINT4 *pu4RetValFsFmLBStatsMatchCount)
{
    UINT4               u4IfIndex = 0;

    u4IfIndex = (UINT4) i4IfIndex;
    *pu4RetValFsFmLBStatsMatchCount =
        (FM_IF_ENTRY (u4IfIndex).PrevSessionStats.u4MatchPktCount);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsFmVarRetrievalTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsFmVarRetrievalTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsFmVarRetrievalTable (INT4 i4IfIndex)
{
    UINT4               u4ErrorCode = 0;
    if (gFmGlobalInfo.u1SystemCtrlStatus != FM_START)
    {
        return SNMP_FAILURE;
    }
    if (FmSnmpLowValidateIfIndex (i4IfIndex, &u4ErrorCode) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsFmVarRetrievalTable
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsFmVarRetrievalTable (INT4 *pi4IfIndex)
{
    UINT4               u4Index = 0;

    if (gFmGlobalInfo.u1SystemCtrlStatus != FM_START)
    {
        return SNMP_FAILURE;
    }
/* Get the First Used Index */
    for (u4Index = 1; u4Index <= FM_MAX_PORTS; u4Index++)
    {
        if ((FM_IF_ENTRY (u4Index).u4Port) == u4Index)
        {
            *pi4IfIndex = (INT4) u4Index;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsFmVarRetrievalTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsFmVarRetrievalTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    UINT4               u4Index = 0;

    if ((i4IfIndex < FM_MIN_PORTS) || (i4IfIndex > FM_MAX_PORTS))
    {
        return SNMP_FAILURE;
    }

    /* Next Index should always return the next used Index */
    for (u4Index = (i4IfIndex + 1); u4Index <= FM_MAX_PORTS; u4Index++)
    {
        if ((FM_IF_ENTRY (u4Index).u4Port) == u4Index)
        {
            *pi4NextIfIndex = (INT4) u4Index;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsFmVarRetrievalMaxVar
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsFmVarRetrievalMaxVar
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFmVarRetrievalMaxVar (INT4 i4IfIndex,
                              UINT4 *pu4RetValFsFmVarRetrievalMaxVar)
{
    UINT4               u4IfIndex = 0;

    u4IfIndex = (UINT4) i4IfIndex;
    *pu4RetValFsFmVarRetrievalMaxVar =
        (FM_IF_ENTRY (u4IfIndex).VarRetrievalInfo.u2MaxDescriptors);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsFmVarRetrievalRequest
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsFmVarRetrievalRequest
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFmVarRetrievalRequest (INT4 i4IfIndex, tSNMP_OCTET_STRING_TYPE *
                               pRetValFsFmVarRetrievalRequest)
{
    UINT4               u4IfIndex = 0;
    UINT2               u2Descriptors = 0;
    UINT1               au1Temp[FM_STR_DESCRIPTOR_LENGTH + 1];
    INT4                i4DescriptorLen = 0;
    UINT1              *pu1VarReq = NULL;
    INT4                i4Count = 0;
    u4IfIndex = (UINT4) i4IfIndex;
    u2Descriptors =
        (FM_IF_ENTRY (u4IfIndex).VarRetrievalInfo.u2DescriptorsFilled);
    pu1VarReq = FM_IF_ENTRY (u4IfIndex).VarRetrievalInfo.pu1VarReqData;

    MEMSET (pRetValFsFmVarRetrievalRequest->pu1_OctetList, 0,
            FM_VAR_RESP_MAX_CHUNK_LENGTH);

    /* convert three byte descriptor data into ascii string */
    if (u2Descriptors > 0)
    {
        for (i4Count = 0; i4Count < u2Descriptors * FM_DESCRIPTOR_LENGTH;
             i4Count += FM_DESCRIPTOR_LENGTH)
        {
            MEMSET (au1Temp, 0, FM_STR_DESCRIPTOR_LENGTH + 1);
            /* Colon should not be added for the last descriptor */
            if (i4Count !=
                (u2Descriptors * FM_DESCRIPTOR_LENGTH) - FM_DESCRIPTOR_LENGTH)
            {
                SNPRINTF ((CHR1 *) au1Temp, FM_STR_DESCRIPTOR_LENGTH,
                          "%d%d:",
                          *(pu1VarReq + i4Count),
                          *(UINT2 *) (VOID *) (pu1VarReq + i4Count + 1));
            }
            else
            {
                SNPRINTF ((CHR1 *) au1Temp, FM_STR_DESCRIPTOR_LENGTH,
                          "%d%d",
                          *(pu1VarReq + i4Count),
                          *(UINT2 *) (VOID *) (pu1VarReq + i4Count + 1));
            }
            STRCAT (pRetValFsFmVarRetrievalRequest->pu1_OctetList, au1Temp);
            i4DescriptorLen = i4DescriptorLen + STRLEN (au1Temp);
        }
        pRetValFsFmVarRetrievalRequest->i4_Length = i4DescriptorLen;
    }
    else
    {
        STRCPY (pRetValFsFmVarRetrievalRequest->pu1_OctetList, "");
        pRetValFsFmVarRetrievalRequest->i4_Length = 0;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsFmVarRetrievalClearResponse
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsFmVarRetrievalClearResponse
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFmVarRetrievalClearResponse (INT4 i4IfIndex,
                                     INT4
                                     *pi4RetValFsFmVarRetrievalClearResponse)
{
    UINT4               u4IfIndex = 0;

    u4IfIndex = (UINT4) i4IfIndex;
    *pi4RetValFsFmVarRetrievalClearResponse =
        (FM_IF_ENTRY (u4IfIndex).VarRetrievalInfo.RespClearStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsFmVarRetrievalMaxVar
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsFmVarRetrievalMaxVar
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsFmVarRetrievalMaxVar (INT4 i4IfIndex,
                              UINT4 u4SetValFsFmVarRetrievalMaxVar)
{
    UINT4               u4IfIndex = 0;

    u4IfIndex = (UINT4) i4IfIndex;
    if ((FM_IF_ENTRY (u4IfIndex).VarRetrievalInfo.u2MaxDescriptors) ==
        u4SetValFsFmVarRetrievalMaxVar)
    {
        return SNMP_SUCCESS;
    }
    if (FM_IF_ENTRY (u4IfIndex).VarRetrievalInfo.pu1VarReqData != NULL)
    {
        FM_RESP_BUDDY_FREE (FM_IF_ENTRY (u4IfIndex).
                            VarRetrievalInfo.pu1VarReqData);
        FM_IF_ENTRY (u4IfIndex).VarRetrievalInfo.pu1VarReqData = NULL;
    }
    if ((FM_IF_ENTRY (u4IfIndex).VarRetrievalInfo.pu1VarReqData =
         FM_RESP_BUDDY_ALLOC (u4SetValFsFmVarRetrievalMaxVar *
                              FM_DESCRIPTOR_LENGTH)) == NULL)
    {
        FM_TRC ((CONTROL_PLANE_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                 "Port %u: No Memory for pu1VarReqData\n", u4IfIndex));
        return SNMP_FAILURE;
    }
    MEMSET (FM_IF_ENTRY (u4IfIndex).VarRetrievalInfo.pu1VarReqData, 0,
            (u4SetValFsFmVarRetrievalMaxVar * FM_DESCRIPTOR_LENGTH));
    FM_IF_ENTRY (u4IfIndex).VarRetrievalInfo.u2DescriptorsFilled = 0;
    FM_IF_ENTRY (u4IfIndex).VarRetrievalInfo.u2MaxDescriptors =
        (UINT2) u4SetValFsFmVarRetrievalMaxVar;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsFmVarRetrievalRequest
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsFmVarRetrievalRequest
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsFmVarRetrievalRequest (INT4 i4IfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValFsFmVarRetrievalRequest)
{
    tEoamExtData        VarReqData;
    UINT4               u4Descriptors = 0;
    UINT1              *pu1VarReq = NULL;

    pu1VarReq =
        (FM_IF_ENTRY ((UINT4) i4IfIndex).VarRetrievalInfo.pu1VarReqData);
    FSAP_ASSERT (pu1VarReq != NULL);
    MEMSET (&VarReqData, 0, sizeof (tEoamExtData));

    u4Descriptors = FmUtilGetDescriptorCntFromStr
        (pSetValFsFmVarRetrievalRequest->pu1_OctetList);
    if (FmUtilCopyDescriptorsFromStr
        (pu1VarReq, pSetValFsFmVarRetrievalRequest->pu1_OctetList) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    FM_IF_ENTRY ((UINT4) i4IfIndex).VarRetrievalInfo.u2DescriptorsFilled =
        (UINT2) u4Descriptors;
    VarReqData.pu1Data =
        (FM_IF_ENTRY ((UINT4) i4IfIndex).VarRetrievalInfo.pu1VarReqData);
    VarReqData.u4DataLen = (FM_DESCRIPTOR_LENGTH *
                            FM_IF_ENTRY ((UINT4) i4IfIndex).VarRetrievalInfo.
                            u2DescriptorsFilled);
    if (EoamApiSendVarReqToPeer ((UINT4) i4IfIndex, &VarReqData) !=
        OSIX_SUCCESS)
    {
        FM_TRC ((FM_VAR_REQRESP_TRC | ALL_FAILURE_TRC,
                 "FM : Send Variable request to peer Failed"));
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsFmVarRetrievalClearResponse
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsFmVarRetrievalClearResponse
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsFmVarRetrievalClearResponse (INT4 i4IfIndex,
                                     INT4 i4SetValFsFmVarRetrievalClearResponse)
{
    UINT2               u2Count = 0;
    tFmPortEntry       *pPortEntry = NULL;

    if (i4SetValFsFmVarRetrievalClearResponse == FM_RESP_NOT_CLEARED)
    {
        /*Configuration of this has no effect so return success */
        return SNMP_SUCCESS;
    }
    pPortEntry = &(FM_IF_ENTRY ((UINT4) i4IfIndex));
    pPortEntry->VarRetrievalInfo.RespClearStatus =
        i4SetValFsFmVarRetrievalClearResponse;
    for (u2Count = 0; u2Count < FM_MAX_RESPONSES; u2Count++)
    {
        if (pPortEntry->VarResp[u2Count].pu1Data != NULL)
        {
            FM_RESP_BUDDY_FREE (pPortEntry->VarResp[u2Count].pu1Data);
            pPortEntry->VarResp[u2Count].pu1Data = NULL;
            pPortEntry->VarResp[u2Count].u4DataLen = 0;
            pPortEntry->au2VarRespChunks[u2Count] = 0;
        }
    }
    pPortEntry->u2Position = 0;
    pPortEntry->VarRetrievalInfo.RespClearStatus = FM_RESP_NOT_CLEARED;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsFmVarRetrievalMaxVar
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsFmVarRetrievalMaxVar
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsFmVarRetrievalMaxVar (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                 UINT4 u4TestValFsFmVarRetrievalMaxVar)
{
    if (gFmGlobalInfo.u1SystemCtrlStatus != FM_START)
    {
        FM_TRC ((MGMT_TRC | ALL_FAILURE_TRC, "SNMP: FM System is Shutdown\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (FmSnmpLowValidateIfIndex (i4IfIndex, pu4ErrorCode) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if ((u4TestValFsFmVarRetrievalMaxVar < FM_MIN_DESCRIPTORS) ||
        (u4TestValFsFmVarRetrievalMaxVar > FM_MAX_DESCRIPTORS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsFmVarRetrievalRequest
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsFmVarRetrievalRequest
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsFmVarRetrievalRequest (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValFsFmVarRetrievalRequest)
{
    UINT4               u4Count = 0;
    UINT4               u4Descriptors = 0;
    UINT4               u4DescriptorLen = 0;
    UINT4               u4BranchLeaf = 0;
    UINT4               u4CheckBitVector = 0;
    UINT2               u2Leaf = 0;
    UINT1               u1Branch = 0;
    INT1                i1RetVal = SNMP_SUCCESS;
    UINT1              *pu1String = NULL;
    UINT1              *pu1BasePtr = NULL;
    UINT1               u1MulFactor = 0;

    pu1String = pTestValFsFmVarRetrievalRequest->pu1_OctetList;
    pu1BasePtr = pu1String;
    if (gFmGlobalInfo.u1SystemCtrlStatus != FM_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (FmSnmpLowValidateIfIndex (i4IfIndex, pu4ErrorCode) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if ((pTestValFsFmVarRetrievalRequest->i4_Length <= 0) ||
        (pTestValFsFmVarRetrievalRequest->i4_Length
         >= SNMP_MAX_OCTETSTRING_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
#ifdef SNMP_2_WANTED
    if (SNMPCheckForNVTChars (pTestValFsFmVarRetrievalRequest->pu1_OctetList,
                              pTestValFsFmVarRetrievalRequest->i4_Length)
        == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif
    if (EoamApiIsEnabledOnPort ((UINT4) i4IfIndex) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (FM_CLI_ERR_EOAM_NOT_STARTED_ON_PORT);
        return SNMP_FAILURE;
    }

    u4Descriptors = FmUtilGetDescriptorCntFromStr (pu1String);
    if (u4Descriptors >
        FM_IF_ENTRY (i4IfIndex).VarRetrievalInfo.u2MaxDescriptors)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        CLI_SET_ERR (FM_CLI_ERR_EXCEEDING_MAX_DESCRIPTORS);
        return SNMP_FAILURE;
    }

    for (u4Count = 0; u4Count < u4Descriptors; u4Count++)
    {
        if (ISDIGIT (*pu1String) != 0)
        {
            if (FmUtilGetDescriptorLength (&pu1BasePtr, &u4DescriptorLen) ==
                OSIX_FAILURE)
            {
                i1RetVal = SNMP_FAILURE;
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (FM_CLI_ERR_INCORRECT_DESCRIPTOR_VAL);
                break;
            }
            if ((u4DescriptorLen > FM_DESCRIPTOR_STRING_MAX_LENGTH) ||
                (u4DescriptorLen < FM_DESCRIPTOR_STRING_MIN_LENGTH))
            {
                i1RetVal = SNMP_FAILURE;
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                CLI_SET_ERR (FM_CLI_ERR_INCORRECT_DESCRIPTOR_LENGTH);
                break;
            }
            if (FmUtilExtractDescriptor (&pu1String, &u1Branch, &u2Leaf) ==
                OSIX_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                i1RetVal = SNMP_FAILURE;
                break;
            }
            if (FM_IS_VALID_DESCRIPTOR (u1Branch, u2Leaf) == OSIX_FALSE)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (FM_CLI_ERR_INCORRECT_DESCRIPTOR_VAL);
                i1RetVal = SNMP_FAILURE;
                break;
            }
        }
        else
        {
            i1RetVal = SNMP_FAILURE;
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (FM_CLI_ERR_INCORRECT_DESCRIPTOR_VAL);
            break;
        }

        u4BranchLeaf = u1Branch;
        u1MulFactor = (UINT1) (u4DescriptorLen - 1);
        while (u1MulFactor > 0)
        {
            u4BranchLeaf = u4BranchLeaf * 10;
            u1MulFactor--;
        }
        u4BranchLeaf = u4BranchLeaf + u2Leaf;

        if (u4BranchLeaf > 70 && u4BranchLeaf < 80)
        {
            u4BranchLeaf = u4BranchLeaf - 50;
        }

        if ((u4CheckBitVector & (1 << u4BranchLeaf)) > 0)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (FM_CLI_ERR_INCORRECT_DESCRIPTOR_VAL);
            i1RetVal = SNMP_FAILURE;
            break;
        }
        else
        {
            u4CheckBitVector |= (1 << u4BranchLeaf);
        }

        if (*pu1String == ':')
        {
            pu1String++;
        }
        else if (*pu1String != '\0')
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            i1RetVal = SNMP_FAILURE;
            break;
        }
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsFmVarRetrievalClearResponse
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsFmVarRetrievalClearResponse
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsFmVarRetrievalClearResponse
    (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
     INT4 i4TestValFsFmVarRetrievalClearResponse)
{
    if (gFmGlobalInfo.u1SystemCtrlStatus != FM_START)
    {
        FM_TRC ((MGMT_TRC | ALL_FAILURE_TRC, "SNMP: FM System is Shutdown\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (FmSnmpLowValidateIfIndex (i4IfIndex, pu4ErrorCode) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (i4TestValFsFmVarRetrievalClearResponse == FM_RESP_NOT_CLEARED)
    {
        /*Configuration of this has no effect so return success */
        return SNMP_SUCCESS;
    }
    if (i4TestValFsFmVarRetrievalClearResponse != FM_RESP_CLEAR)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsFmVarRetrievalTable
 Input       :  The Indices
                IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsFmVarRetrievalTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsFmVarResponseTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsFmVarResponseTable
 Input       :  The Indices
                IfIndex
                FsFmVarResponseId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsFmVarResponseTable (INT4 i4IfIndex,
                                              UINT4 u4FsFmVarResponseId)
{
    UINT4               u4ErrorCode = 0;
    if (gFmGlobalInfo.u1SystemCtrlStatus != FM_START)
    {
        return SNMP_FAILURE;
    }
    if (FmSnmpLowValidateIfIndex (i4IfIndex, &u4ErrorCode) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if ((u4FsFmVarResponseId == 0) || (u4FsFmVarResponseId > FM_MAX_RESPONSES))
    {
        return SNMP_FAILURE;
    }
    if ((FM_IF_ENTRY ((UINT4) i4IfIndex).VarResp[u4FsFmVarResponseId - 1].
         u4DataLen) == 0)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsFmVarResponseTable
 Input       :  The Indices
                IfIndex
                FsFmVarResponseId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsFmVarResponseTable (INT4 *pi4IfIndex,
                                      UINT4 *pu4FsFmVarResponseId)
{
    UINT4               u4Index = 0;
    UINT4               u4RespId = 0;

    if (gFmGlobalInfo.u1SystemCtrlStatus != FM_START)
    {
        return SNMP_FAILURE;
    }
/* Get the First Used Index */
    for (u4Index = 1; u4Index <= FM_MAX_PORTS; u4Index++)
    {
        if ((FM_IF_ENTRY (u4Index).u4Port) == u4Index)
        {
            for (u4RespId = 1; u4RespId <= FM_MAX_RESPONSES; u4RespId++)
            {
                if ((FM_IF_ENTRY (u4Index).VarResp[u4RespId - 1].u4DataLen)
                    != 0)
                {
                    *pi4IfIndex = (INT4) u4Index;
                    *pu4FsFmVarResponseId = u4RespId;
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsFmVarResponseTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                FsFmVarResponseId
                nextFsFmVarResponseId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsFmVarResponseTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                     UINT4 u4FsFmVarResponseId,
                                     UINT4 *pu4NextFsFmVarResponseId)
{
    UINT4               u4Index = 0;
    UINT4               u4RespId = 0;

    if ((i4IfIndex < FM_MIN_PORTS) || (i4IfIndex > FM_MAX_PORTS))
    {
        return SNMP_FAILURE;
    }
    if ((u4FsFmVarResponseId == 0) || (u4FsFmVarResponseId > FM_MAX_RESPONSES))
    {
        return SNMP_FAILURE;
    }

    /* Next Index should always return the next used Index */
    for (u4Index = i4IfIndex; u4Index <= FM_MAX_PORTS; u4Index++)
    {
        if ((FM_IF_ENTRY (u4Index).u4Port) == u4Index)
        {
            for (u4RespId = (u4FsFmVarResponseId + 1);
                 u4RespId <= FM_MAX_RESPONSES; u4RespId++)
            {
                if ((FM_IF_ENTRY (u4Index).VarResp[u4RespId - 1].u4DataLen)
                    != 0)
                {
                    *pu4NextFsFmVarResponseId = u4RespId;
                    *pi4NextIfIndex = (INT4) u4Index;
                    return SNMP_SUCCESS;
                }
            }
            u4FsFmVarResponseId = 0;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsFmVarResponseRx1
 Input       :  The Indices
                IfIndex
                FsFmVarResponseId

                The Object 
                retValFsFmVarResponseRx1
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFmVarResponseRx1 (INT4 i4IfIndex, UINT4 u4FsFmVarResponseId,
                          tSNMP_OCTET_STRING_TYPE * pRetValFsFmVarResponseRx1)
{
    tEoamExtData       *pVarResp = NULL;
    UINT4               u4IfIndex = 0;
    UINT4               u4RespLength = 0;

    if (u4FsFmVarResponseId > FM_MAX_RESPONSES)
    {
        return SNMP_FAILURE;
    }
    u4IfIndex = (UINT4) i4IfIndex;
    pVarResp = &(FM_IF_ENTRY (u4IfIndex).VarResp[u4FsFmVarResponseId - 1]);
    if (pVarResp->pu1Data == NULL)
    {
        pRetValFsFmVarResponseRx1->i4_Length = 0;
    }
    else
    {
        if (FM_IF_ENTRY (u4IfIndex).au2VarRespChunks[u4FsFmVarResponseId - 1]
            > 1)
        {
            u4RespLength = FM_VAR_RESP_MAX_CHUNK_LENGTH;
        }
        else
        {
            u4RespLength = pVarResp->u4DataLen;
        }
        MEMCPY (pRetValFsFmVarResponseRx1->pu1_OctetList, pVarResp->pu1Data,
                u4RespLength);
        pRetValFsFmVarResponseRx1->i4_Length = (INT4) u4RespLength;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsFmVarResponseRx2
 Input       :  The Indices
                IfIndex
                FsFmVarResponseId

                The Object 
                retValFsFmVarResponseRx2
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFmVarResponseRx2 (INT4 i4IfIndex, UINT4 u4FsFmVarResponseId,
                          tSNMP_OCTET_STRING_TYPE * pRetValFsFmVarResponseRx2)
{
    tEoamExtData       *pVarResp = NULL;
    UINT4               u4IfIndex = 0;
    UINT4               u4RespLength = 0;
    UINT2               u2Offset = FM_VAR_RESP_MAX_CHUNK_LENGTH;

    if (u4FsFmVarResponseId > FM_MAX_RESPONSES)
    {
        return SNMP_FAILURE;
    }
    u4IfIndex = (UINT4) i4IfIndex;
    pVarResp = &(FM_IF_ENTRY (u4IfIndex).VarResp[u4FsFmVarResponseId - 1]);
    if ((pVarResp->pu1Data == NULL) ||
        (FM_IF_ENTRY (u4IfIndex).au2VarRespChunks[u4FsFmVarResponseId - 1] < 2))
    {
        pRetValFsFmVarResponseRx2->i4_Length = 0;
    }
    else
    {
        if (FM_IF_ENTRY (u4IfIndex).au2VarRespChunks[u4FsFmVarResponseId - 1] >
            2)
        {
            u4RespLength = FM_VAR_RESP_MAX_CHUNK_LENGTH;
        }
        else
        {
            u4RespLength = pVarResp->u4DataLen - u2Offset;
        }
        MEMCPY (pRetValFsFmVarResponseRx2->pu1_OctetList,
                pVarResp->pu1Data + u2Offset, u4RespLength);
        pRetValFsFmVarResponseRx2->i4_Length = (INT4) u4RespLength;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsFmVarResponseRx3
 Input       :  The Indices
                IfIndex
                FsFmVarResponseId

                The Object 
                retValFsFmVarResponseRx3
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFmVarResponseRx3 (INT4 i4IfIndex, UINT4 u4FsFmVarResponseId,
                          tSNMP_OCTET_STRING_TYPE * pRetValFsFmVarResponseRx3)
{
    tEoamExtData       *pVarResp = NULL;
    UINT4               u4IfIndex = 0;
    UINT4               u4RespLength = 0;
    UINT2               u2Offset = 2 * FM_VAR_RESP_MAX_CHUNK_LENGTH;

    if (u4FsFmVarResponseId > FM_MAX_RESPONSES)
    {
        return SNMP_FAILURE;
    }
    u4IfIndex = (UINT4) i4IfIndex;
    pVarResp = &(FM_IF_ENTRY (u4IfIndex).VarResp[u4FsFmVarResponseId - 1]);
    if ((pVarResp->pu1Data == NULL) ||
        (FM_IF_ENTRY (u4IfIndex).au2VarRespChunks[u4FsFmVarResponseId - 1] < 3))
    {
        pRetValFsFmVarResponseRx3->i4_Length = 0;
    }
    else
    {
        if (FM_IF_ENTRY (u4IfIndex).au2VarRespChunks[u4FsFmVarResponseId - 1] >
            3)
        {
            u4RespLength = FM_VAR_RESP_MAX_CHUNK_LENGTH;
        }
        else
        {
            u4RespLength = pVarResp->u4DataLen - u2Offset;
        }
        MEMCPY (pRetValFsFmVarResponseRx3->pu1_OctetList,
                pVarResp->pu1Data + u2Offset, u4RespLength);
        pRetValFsFmVarResponseRx3->i4_Length = (INT4) u4RespLength;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsFmVarResponseRx4
 Input       :  The Indices
                IfIndex
                FsFmVarResponseId

                The Object 
                retValFsFmVarResponseRx4
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFmVarResponseRx4 (INT4 i4IfIndex, UINT4 u4FsFmVarResponseId,
                          tSNMP_OCTET_STRING_TYPE * pRetValFsFmVarResponseRx4)
{
    tEoamExtData       *pVarResp = NULL;
    UINT4               u4IfIndex = 0;
    UINT4               u4RespLength = 0;
    UINT2               u2Offset = 3 * FM_VAR_RESP_MAX_CHUNK_LENGTH;

    if (u4FsFmVarResponseId > FM_MAX_RESPONSES)
    {
        return SNMP_FAILURE;
    }
    u4IfIndex = (UINT4) i4IfIndex;
    pVarResp = &(FM_IF_ENTRY (u4IfIndex).VarResp[u4FsFmVarResponseId - 1]);
    if ((pVarResp->pu1Data == NULL) ||
        (FM_IF_ENTRY (u4IfIndex).au2VarRespChunks[u4FsFmVarResponseId - 1] < 4))
    {
        pRetValFsFmVarResponseRx4->i4_Length = 0;
    }
    else
    {
        if (FM_IF_ENTRY (u4IfIndex).au2VarRespChunks[u4FsFmVarResponseId - 1] >
            4)
        {
            u4RespLength = FM_VAR_RESP_MAX_CHUNK_LENGTH;
        }
        else
        {
            u4RespLength = pVarResp->u4DataLen - u2Offset;
        }
        MEMCPY (pRetValFsFmVarResponseRx4->pu1_OctetList,
                pVarResp->pu1Data + u2Offset, u4RespLength);
        pRetValFsFmVarResponseRx4->i4_Length = (INT4) u4RespLength;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsFmVarResponseRx5
 Input       :  The Indices
                IfIndex
                FsFmVarResponseId

                The Object 
                retValFsFmVarResponseRx5
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFmVarResponseRx5 (INT4 i4IfIndex, UINT4 u4FsFmVarResponseId,
                          tSNMP_OCTET_STRING_TYPE * pRetValFsFmVarResponseRx5)
{
    tEoamExtData       *pVarResp = NULL;
    UINT4               u4IfIndex = 0;
    UINT4               u4RespLength = 0;
    UINT2               u2Offset = 4 * FM_VAR_RESP_MAX_CHUNK_LENGTH;

    if (u4FsFmVarResponseId > FM_MAX_RESPONSES)
    {
        return SNMP_FAILURE;
    }
    u4IfIndex = (UINT4) i4IfIndex;
    pVarResp = &(FM_IF_ENTRY (u4IfIndex).VarResp[u4FsFmVarResponseId - 1]);
    if ((pVarResp->pu1Data == NULL) ||
        (FM_IF_ENTRY (u4IfIndex).au2VarRespChunks[u4FsFmVarResponseId - 1] < 5))
    {
        pRetValFsFmVarResponseRx5->i4_Length = 0;
    }
    else
    {
        if (FM_IF_ENTRY (u4IfIndex).au2VarRespChunks[u4FsFmVarResponseId - 1] >
            5)
        {
            u4RespLength = FM_VAR_RESP_MAX_CHUNK_LENGTH;
        }
        else
        {
            u4RespLength = pVarResp->u4DataLen - u2Offset;
        }
        MEMCPY (pRetValFsFmVarResponseRx5->pu1_OctetList,
                pVarResp->pu1Data + u2Offset, u4RespLength);
        pRetValFsFmVarResponseRx5->i4_Length = (INT4) u4RespLength;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsFmVarResponseRx6
 Input       :  The Indices
                IfIndex
                FsFmVarResponseId

                The Object 
                retValFsFmVarResponseRx6
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFmVarResponseRx6 (INT4 i4IfIndex, UINT4 u4FsFmVarResponseId,
                          tSNMP_OCTET_STRING_TYPE * pRetValFsFmVarResponseRx6)
{
    tEoamExtData       *pVarResp = NULL;
    UINT4               u4IfIndex = 0;
    UINT4               u4RespLength = 0;
    UINT2               u2Offset = 5 * FM_VAR_RESP_MAX_CHUNK_LENGTH;

    if (u4FsFmVarResponseId > FM_MAX_RESPONSES)
    {
        return SNMP_FAILURE;
    }
    u4IfIndex = (UINT4) i4IfIndex;
    pVarResp = &(FM_IF_ENTRY (u4IfIndex).VarResp[u4FsFmVarResponseId - 1]);
    if ((pVarResp->pu1Data == NULL) ||
        (FM_IF_ENTRY (u4IfIndex).au2VarRespChunks[u4FsFmVarResponseId - 1] < 6))
    {
        pRetValFsFmVarResponseRx6->i4_Length = 0;
    }
    else
    {
        u4RespLength = pVarResp->u4DataLen - u2Offset;
        MEMCPY (pRetValFsFmVarResponseRx6->pu1_OctetList,
                pVarResp->pu1Data + u2Offset, u4RespLength);
        pRetValFsFmVarResponseRx6->i4_Length = (INT4) u4RespLength;
    }
    return SNMP_SUCCESS;
}

/***************************************************************************
 *     FUNCTION NAME    :  FmSnmpLowValidateIfIndex 
 *                                                                           
 *     DESCRIPTION      :  This function validates the Index for all tables
 *                                                                           
 *     INPUT            :  i4IfIndex - Interface Index 
 *                                                                           
 *     OUTPUT           :  pu4ErrorCode.                                           
 *                                                                           
 *     RETURNS          :  OSIX_SUCCESS / OSIX_FAILURE                       
 ****************************************************************************/
PRIVATE INT4
FmSnmpLowValidateIfIndex (INT4 i4IfIndex, UINT4 *pu4ErrorCode)
{

    if ((i4IfIndex < FM_MIN_PORTS) || (i4IfIndex > FM_MAX_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return OSIX_FAILURE;
    }
    if ((FM_IF_ENTRY ((UINT4) i4IfIndex).u4Port) != (UINT4) i4IfIndex)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FmNotifyProtocolShutdownStatus
 Input       :  None
 Output      :  None
 Returns     :  None
****************************************************************************/

VOID
FmNotifyProtocolShutdownStatus (VOID)
{
#ifdef ISS_WANTED
    UINT1               au1ObjectOid[2][SNMP_MAX_OID_LENGTH];

    /* The oid list contains the list of Oids that has been registered 
     * for the protocol + the oid of the SystemControl object. */
    SNMPGetOidString (fsfm, (sizeof (fsfm) / sizeof (UINT4)), au1ObjectOid[0]);
    SNMPGetOidString (FsFmSystemControl,
                      (sizeof (FsFmSystemControl) / sizeof (UINT4)),
                      au1ObjectOid[1]);

    /* Send a notification to MSR to process the FM shutdown, with 
     * FM oids and its system control object 
     * As the protocol does not have MI support, invalid context id is sent */
    MSR_NOTIFY_PROTOCOL_SHUT (au1ObjectOid, 2, MSR_INVALID_CNTXT);
#endif
    return;
}
