/*****************************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: emfmapi.c,v 1.5 2013/11/05 10:57:24 siva Exp $ 
 *
 * Description: This file contains the callback function API of FutureSoft FM
 *              module that will be registered with FutureSoft EOAM module
 *****************************************************************************/
#include "emfminc.h"

/******************************************************************************
 * Function Name      : FmApiCallbackFromEoam
 *
 * Description        : This API posts a message to FM task queue
 *                      notifying FM about the EOAM events, loopback status
 *                      and loopback data
 *
 * Input(s)           : pCallbackInfo - Callback info from EOAM
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
FmApiCallbackFromEoam (tEoamCallbackInfo * pCallbackInfo)
{
    tFmQMsg            *pMsg = NULL;
    BOOL1               bErrorFlag = OSIX_FALSE;

    if (gFmGlobalInfo.u1SystemCtrlStatus == FM_SHUTDOWN)
    {
        FM_TRC ((ALL_FAILURE_TRC, "FM is not started\r\n"));
        return OSIX_FAILURE;
    }

    if (pCallbackInfo == NULL)
    {
        FM_TRC ((ALL_FAILURE_TRC, "CallbackInfo is NULL\r\n"));
        return OSIX_FAILURE;
    }

    /* Allocate Interface message buffer from the pool */
    pMsg = (tFmQMsg *) MemAllocMemBlk (gFmGlobalInfo.QMsgPoolId);

    if (pMsg == NULL)
    {
        FM_TRC ((BUFFER_TRC | ALL_FAILURE_TRC,
                 "FmApiCallbackFromEoam: Port %d :"
                 " QMsg MemAllocateMemBlock FAILED\r\n",
                 pCallbackInfo->u4Port));
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tFmQMsg));

    pMsg->u4MsgType = FM_EOAM_MSG;
    pMsg->u4Port = pCallbackInfo->u4Port;;
    pMsg->u4EventType = pCallbackInfo->u4EventType;

    switch (pCallbackInfo->u4EventType)
    {
        case EOAM_NOTIFY_ERRORED_SYMBOL:
        case EOAM_NOTIFY_ERRORED_FRAME:
        case EOAM_NOTIFY_ERRORED_FRAME_PERIOD:
        case EOAM_NOTIFY_ERRORED_FRAME_SECONDS:
            MEMCPY (&(pMsg->FmThresEventInfo), &(pCallbackInfo->ThresEventInfo),
                    sizeof (tEoamThresEventInfo));
            pMsg->u2Location = pCallbackInfo->u2Location;
            break;

        case EOAM_NOTIFY_LINK_FAULT:
            /* Fall thru */
        case EOAM_NOTIFY_DYING_GASP:
            /* Fall thru */
        case EOAM_NOTIFY_CRITICAL_EVENT:
            /* Fall thru */
        case EOAM_NOTIFY_LB_ACK_RCVD:
            /* Fall thru */
        case EOAM_NOTIFY_LB_ACK_NOT_RCVD:
            pMsg->FmEventState = pCallbackInfo->EventState;
            pMsg->u2Location = pCallbackInfo->u2Location;
            break;

        case EOAM_NOTIFY_ORG_INFO_RCVD:
            /* Fall thru */
        case EOAM_NOTIFY_ORG_SPECIFIC_EVENT:
            /* Fall thru */
        case EOAM_NOTIFY_VAR_RESP_RCVD:
            /* Allocate buddy memory and copy ExtData */
            pMsg->FmExtData.u4DataLen = pCallbackInfo->ExtData.u4DataLen;
            pMsg->FmExtData.pu1Data =
                FM_EXT_BUDDY_ALLOC (pMsg->FmExtData.u4DataLen);

            if (pMsg->FmExtData.pu1Data == NULL)
            {
                FM_TRC ((CONTROL_PLANE_TRC | BUFFER_TRC |
                         ALL_FAILURE_TRC, "No Memory for ExtData\n"));
                bErrorFlag = OSIX_TRUE;
                break;
            };

            MEMCPY (pMsg->FmExtData.pu1Data, pCallbackInfo->ExtData.pu1Data,
                    pMsg->FmExtData.u4DataLen);
            pMsg->u2Location = pCallbackInfo->u2Location;
            break;

        case EOAM_NOTIFY_PORT_CREATE:
            /* Fall thru */
        case EOAM_NOTIFY_PORT_DELETE:
            break;

        case EOAM_NOTIFY_LB_TESTDATA_RCVD:
            /* Duplicate CRU buffer */
            pMsg->FmLBTestData =
                CRU_BUF_Duplicate_BufChain (pCallbackInfo->LBTestData);
            if (pMsg->FmLBTestData == NULL)
            {
                FM_TRC ((CONTROL_PLANE_TRC | BUFFER_TRC |
                         ALL_FAILURE_TRC, "Mem alloc failed\n"));
                bErrorFlag = OSIX_TRUE;
            };
            break;

        default:
            FM_TRC ((ALL_FAILURE_TRC,
                     "Port %d :Unknown event received\r\n",
                     pCallbackInfo->u4Port));
            bErrorFlag = OSIX_TRUE;
    }

    if (bErrorFlag == OSIX_TRUE)
    {
        if (MemReleaseMemBlock (gFmGlobalInfo.QMsgPoolId, (UINT1 *) pMsg)
            != MEM_SUCCESS)
        {
            FM_TRC ((BUFFER_TRC | ALL_FAILURE_TRC,
                     "Port %d: MemReleaseMemBlock FAILED\r\n",
                     pCallbackInfo->u4Port));
        }

        return OSIX_FAILURE;
    }

    FM_TRC ((CONTROL_PLANE_TRC,
             "FmApiCallbackFromEoam: Posting an event on port %d\r\n",
             pCallbackInfo->u4Port));

    if ((FmQueEnqAppMsg (pMsg)) == OSIX_FAILURE)
    {
        FM_TRC ((ALL_FAILURE_TRC | FM_CRITICAL_TRC,
                 "FmApiCallbackFromEoam: Port %d :"
                 "Enqueue Message FAILED\r\n", pCallbackInfo->u4Port));

        switch (pCallbackInfo->u4EventType)
        {
            case EOAM_NOTIFY_ORG_INFO_RCVD:
                /* Fall thru */
            case EOAM_NOTIFY_ORG_SPECIFIC_EVENT:
                /* Fall thru */
            case EOAM_NOTIFY_VAR_RESP_RCVD:
                /* Free allocated buddy memory */
                FM_EXT_BUDDY_FREE (pMsg->FmExtData.pu1Data);
                break;

            case EOAM_NOTIFY_LB_TESTDATA_RCVD:
                /* Release CRU buffer */
                if (CRU_BUF_Release_MsgBufChain (pMsg->FmLBTestData,
                                                 OSIX_FALSE) != CRU_SUCCESS)
                {
                    FM_TRC ((BUFFER_TRC | ALL_FAILURE_TRC, "Loopback test data "
                             "CRU Buffer Release Failed\n"));
                }
        }
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
EoamGetActionFromFm (UINT4 u4IfIndex, UINT4 u4EventType)
{
    INT4                i4RetVal = OSIX_FAILURE;
    switch (u4EventType)
    {
        case EOAM_NOTIFY_LINK_FAULT:
        case EOAM_LINK_FAULT:
            i4RetVal =
                (INT4) (FM_IF_ENTRY (u4IfIndex).EventInfo.LinkFaultAction);
            break;
        case EOAM_NOTIFY_DYING_GASP:
        case EOAM_DYING_GASP:
            i4RetVal =
                (INT4) (FM_IF_ENTRY (u4IfIndex).EventInfo.DyingGaspAction);
            break;
        case EOAM_NOTIFY_CRITICAL_EVENT:
        case EOAM_CRITICAL_EVENT:
            i4RetVal =
                (INT4) (FM_IF_ENTRY (u4IfIndex).EventInfo.CriticalEventAction);
            break;
        default:
            FM_TRC ((ALL_FAILURE_TRC, "Wrong Event Type\n"));
    }
    return i4RetVal;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  emfmapi.c                      */
/*-----------------------------------------------------------------------*/
