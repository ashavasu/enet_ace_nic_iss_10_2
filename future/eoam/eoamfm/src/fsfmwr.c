/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsfmwr.c,v 1.3 2008/08/20 14:17:20 premap-iss Exp $
 *
 * Description: SNMP wrapper function of Fault Management Module. 
 *********************************************************************/
# include  "lr.h"
#include   "cfa.h"
#include   "eoam.h"
#include  "eoamfm.h"
# include  "fssnmp.h"
# include  "fsfmlw.h"
# include  "fsfmwr.h"
# include  "fsfmdb.h"

VOID
RegisterFSFM ()
{
    SNMPRegisterMibWithLock (&fsfmOID, &fsfmEntry, FmLock, FmUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fsfmOID, (const UINT1 *) "fsfm");
}

INT4
FsFmSystemControlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsFmSystemControl (&(pMultiData->i4_SLongValue)));
}

INT4
FsFmModuleStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsFmModuleStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsFmTraceOptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsFmTraceOption (&(pMultiData->i4_SLongValue)));
}

INT4
FsFmSystemControlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsFmSystemControl (pMultiData->i4_SLongValue));
}

INT4
FsFmModuleStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsFmModuleStatus (pMultiData->i4_SLongValue));
}

INT4
FsFmTraceOptionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsFmTraceOption (pMultiData->i4_SLongValue));
}

INT4
FsFmSystemControlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsFmSystemControl (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsFmModuleStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsFmModuleStatus (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsFmTraceOptionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsFmTraceOption (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsFmSystemControlDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsFmSystemControl
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsFmModuleStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsFmModuleStatus (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsFmTraceOptionDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsFmTraceOption (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsFmLinkEventTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsFmLinkEventTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsFmLinkEventTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsFmSymPeriodActionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsFmLinkEventTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsFmSymPeriodAction (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsFmFrameActionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsFmLinkEventTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsFmFrameAction (pMultiIndex->pIndex[0].i4_SLongValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
FsFmFramePeriodActionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsFmLinkEventTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsFmFramePeriodAction (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsFmFrameSecSummActionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsFmLinkEventTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsFmFrameSecSummAction (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsFmCriticalEventActionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsFmLinkEventTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsFmCriticalEventAction (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
FsFmDyingGaspActionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsFmLinkEventTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsFmDyingGaspAction (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsFmLinkFaultActionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsFmLinkEventTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsFmLinkFaultAction (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsFmSymPeriodActionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsFmSymPeriodAction (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsFmFrameActionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsFmFrameAction (pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiData->i4_SLongValue));

}

INT4
FsFmFramePeriodActionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsFmFramePeriodAction (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsFmFrameSecSummActionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsFmFrameSecSummAction (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsFmCriticalEventActionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsFmCriticalEventAction (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsFmDyingGaspActionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsFmDyingGaspAction (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsFmLinkFaultActionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsFmLinkFaultAction (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsFmSymPeriodActionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsFmSymPeriodAction (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsFmFrameActionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2FsFmFrameAction (pu4Error,
                                      pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsFmFramePeriodActionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsFmFramePeriodAction (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsFmFrameSecSummActionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsFmFrameSecSummAction (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsFmCriticalEventActionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsFmCriticalEventAction (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
FsFmDyingGaspActionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsFmDyingGaspAction (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsFmLinkFaultActionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsFmLinkFaultAction (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsFmLinkEventTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsFmLinkEventTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsFmLoopbackTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsFmLoopbackTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsFmLoopbackTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsFmLoopbackStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsFmLoopbackTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsFmLoopbackStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FsFmLBTestPatternGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsFmLoopbackTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsFmLBTestPattern (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->pOctetStrValue));

}

INT4
FsFmLBTestPktSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsFmLoopbackTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsFmLBTestPktSize (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
FsFmLBTestCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsFmLoopbackTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsFmLBTestCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                   &(pMultiData->u4_ULongValue)));

}

INT4
FsFmLBTestWaitTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsFmLoopbackTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsFmLBTestWaitTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FsFmLBTestCommandGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsFmLoopbackTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsFmLBTestCommand (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
FsFmLBTestStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsFmLoopbackTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsFmLBTestStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                    &(pMultiData->i4_SLongValue)));

}

INT4
FsFmLBTestStartTimestampGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsFmLoopbackTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsFmLBTestStartTimestamp
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsFmLBTestEndTimestampGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsFmLoopbackTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsFmLBTestEndTimestamp (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
FsFmLBTestTxCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsFmLoopbackTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsFmLBTestTxCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
FsFmLBTestRxCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsFmLoopbackTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsFmLBTestRxCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
FsFmLBTestMatchCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsFmLoopbackTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsFmLBTestMatchCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
FsFmLBTestPatternSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsFmLBTestPattern (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->pOctetStrValue));

}

INT4
FsFmLBTestPktSizeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsFmLBTestPktSize (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->u4_ULongValue));

}

INT4
FsFmLBTestCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsFmLBTestCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiData->u4_ULongValue));

}

INT4
FsFmLBTestWaitTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsFmLBTestWaitTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsFmLBTestCommandSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsFmLBTestCommand (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsFmLBTestPatternTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsFmLBTestPattern (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->pOctetStrValue));

}

INT4
FsFmLBTestPktSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsFmLBTestPktSize (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->u4_ULongValue));

}

INT4
FsFmLBTestCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2FsFmLBTestCount (pu4Error,
                                      pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->u4_ULongValue));

}

INT4
FsFmLBTestWaitTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsFmLBTestWaitTime (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsFmLBTestCommandTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsFmLBTestCommand (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsFmLoopbackTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsFmLoopbackTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsFmLBStatsTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsFmLBStatsTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsFmLBStatsTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsFmLBStatsStartTimestampGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsFmLBStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsFmLBStatsStartTimestamp
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsFmLBStatsEndTimestampGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsFmLBStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsFmLBStatsEndTimestamp (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
FsFmLBStatsTxCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsFmLBStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsFmLBStatsTxCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->u4_ULongValue)));

}

INT4
FsFmLBStatsRxCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsFmLBStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsFmLBStatsRxCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->u4_ULongValue)));

}

INT4
FsFmLBStatsMatchCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsFmLBStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsFmLBStatsMatchCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexFsFmVarRetrievalTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsFmVarRetrievalTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsFmVarRetrievalTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsFmVarRetrievalMaxVarGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsFmVarRetrievalTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsFmVarRetrievalMaxVar (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
FsFmVarRetrievalRequestGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsFmVarRetrievalTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsFmVarRetrievalRequest (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
FsFmVarRetrievalClearResponseGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsFmVarRetrievalTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsFmVarRetrievalClearResponse
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsFmVarRetrievalMaxVarSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsFmVarRetrievalMaxVar (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->u4_ULongValue));

}

INT4
FsFmVarRetrievalRequestSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsFmVarRetrievalRequest (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
FsFmVarRetrievalClearResponseSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsFmVarRetrievalClearResponse
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsFmVarRetrievalMaxVarTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsFmVarRetrievalMaxVar (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->u4_ULongValue));

}

INT4
FsFmVarRetrievalRequestTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsFmVarRetrievalRequest (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->pOctetStrValue));

}

INT4
FsFmVarRetrievalClearResponseTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2FsFmVarRetrievalClearResponse (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
FsFmVarRetrievalTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsFmVarRetrievalTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsFmVarResponseTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsFmVarResponseTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsFmVarResponseTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsFmVarResponseRx1Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsFmVarResponseTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsFmVarResponseRx1 (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiData->pOctetStrValue));

}

INT4
FsFmVarResponseRx2Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsFmVarResponseTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsFmVarResponseRx2 (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiData->pOctetStrValue));

}

INT4
FsFmVarResponseRx3Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsFmVarResponseTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsFmVarResponseRx3 (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiData->pOctetStrValue));

}

INT4
FsFmVarResponseRx4Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsFmVarResponseTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsFmVarResponseRx4 (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiData->pOctetStrValue));

}

INT4
FsFmVarResponseRx5Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsFmVarResponseTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsFmVarResponseRx5 (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiData->pOctetStrValue));

}

INT4
FsFmVarResponseRx6Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsFmVarResponseTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsFmVarResponseRx6 (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiData->pOctetStrValue));

}

/*-----------------------------------------------------------------------*/
/*                       End of the file  fsfmwr.c                       */
/*-----------------------------------------------------------------------*/
