/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: emfmcli.c,v 1.6 2014/06/29 11:01:11 siva Exp $
 *
 * Description: This file contains CLI SET/GET/TEST and GETNEXT
 *              routines for the MIB objects specified in fsfm.mib
 *********************************************************************/

#ifndef FMCLI_C
#define FMCLI_C

#include "emfminc.h"
#include "emfmcli.h"
#include "fsfmcli.h"

/*************************************************************************
 *
 *  FUNCTION NAME   : cli_process_fm_cmd
 *
 *  DESCRIPTION     : Protocol CLI message handler function
 *
 *  INPUT           : CliHandle - CliContext ID
 *                    u4Command - Command identifier
 *                    ... -Variable command argument list
 *
 *  OUTPUT          : None
 *
 *  RETURNS         : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
cli_process_fm_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *apu1args[FM_CLI_MAX_ARGS];
    INT1                i1argno = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4ReqCount = 0;
    INT4                i4Inst = 0;
    INT4                i4Status = 0;
    INT4                i4IfIndex = 0;
    INT4                i4RetStatus = CLI_SUCCESS;

    MEMSET (apu1args, 0, FM_CLI_MAX_ARGS);

    CliRegisterLock (CliHandle, FmLock, FmUnLock);
    FmLock ();
    nmhGetFsFmSystemControl (&i4Status);

    if ((u4Command != FM_CLI_SYS_CTRL) && (i4Status != FM_START))
    {
        CliPrintf (CliHandle, "\r%% Fault-management is shutdown\r\n");
        FmUnLock ();
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }

    va_start (ap, u4Command);

    /* third arguement is always interface name/index */

    i4Inst = CLI_PTR_TO_I4 (va_arg (ap, UINT1 *));

    /* Walk through the rest of the arguments and store in apu1args array. 
     * Store FM_CLI_MAX_ARGS arguments at the max
     * has value */

    while (1)
    {
        apu1args[i1argno++] = va_arg (ap, UINT1 *);

        if (i1argno == FM_CLI_MAX_ARGS)
        {
            break;
        }

    }
    va_end (ap);

    switch (u4Command)
    {
        case FM_CLI_SYS_CTRL:

            /* apu1args[0] contains start/shutdown status */
            i4RetStatus =
                FmCliSetSystemCtrl (CliHandle, CLI_PTR_TO_I4 (apu1args[0]));
            break;

        case FM_CLI_MOD_STAT:

            /* apu1args[0] contains enable/disable status */
            i4RetStatus = FmCliSetModuleStatus (CliHandle,
                                                CLI_PTR_TO_I4 (apu1args[0]));
            break;

        case FM_CLI_DEBUG_SHOW:

            FmCliShowDebugging (CliHandle);
            break;

        case FM_CLI_DEBUG:

            i4RetStatus =
                FmCliEnableDebug (CliHandle, CLI_PTR_TO_I4 (apu1args[0]));
            break;

        case FM_CLI_NO_DEBUG:

            i4RetStatus =
                FmCliDisableDebug (CliHandle, CLI_PTR_TO_I4 (apu1args[0]));
            break;

        case FM_CLI_CLEAR_MIB_RESP:

            i4RetStatus = FmCliClearStats (CliHandle, i4Inst);
            break;

        case FM_CLI_SHOW_GLOBAL_INFO:

            i4RetStatus = FmCliShowGlobal (CliHandle);
            break;

        case FM_CLI_SHOW_MIB_RESP:

            i4RetStatus = FmCliShowMibResp (CliHandle, i4Inst);
            break;

        case FM_CLI_SHOW_PORT_CONFIG:

            i4RetStatus = FmCliShowPortConfig (CliHandle, i4Inst);
            break;

        case FM_CLI_SHOW_LB_CUR_DETAIL:

            i4RetStatus = FmCliShowCurrentStatsDetail (CliHandle, i4Inst);
            break;

        case FM_CLI_SHOW_LB_CUR:

            i4RetStatus = FmCliShowCurrentStats (CliHandle, i4Inst);
            break;

        case FM_CLI_SHOW_LB_LAST_DETAIL:

            i4RetStatus = FmCliShowLastStatsDetail (CliHandle, i4Inst);
            break;

        case FM_CLI_SHOW_LB_LAST:

            i4RetStatus = FmCliShowLastStats (CliHandle, i4Inst);
            break;

        case FM_CLI_LB_TEST:

            i4IfIndex = (INT4) CLI_GET_IFINDEX ();

            i4RetStatus = FmCliLBTestConfig (CliHandle,
                                             i4IfIndex,
                                             CLI_PTR_TO_U4 (apu1args[0]),
                                             CLI_PTR_TO_U4 (apu1args[1]),
                                             apu1args[2],
                                             CLI_PTR_TO_I4 (apu1args[3]),
                                             CLI_PTR_TO_I4 (apu1args[4]));
            break;

        case FM_CLI_LM_SYM:

            i4IfIndex = (INT4) CLI_GET_IFINDEX ();

            i4RetStatus = FmCliLinkMonitorSymbol (CliHandle,
                                                  i4IfIndex,
                                                  CLI_PTR_TO_I4 (apu1args[0]));
            break;

        case FM_CLI_LM_FRAME:

            i4IfIndex = (INT4) CLI_GET_IFINDEX ();

            i4RetStatus = FmCliLinkMonitorFrame (CliHandle,
                                                 i4IfIndex,
                                                 CLI_PTR_TO_I4 (apu1args[0]));
            break;

        case FM_CLI_LM_FRAME_PERIOD:

            i4IfIndex = (INT4) CLI_GET_IFINDEX ();

            i4RetStatus = FmCliLinkMonitorFramePeriod (CliHandle,
                                                       i4IfIndex,
                                                       CLI_PTR_TO_I4 (apu1args
                                                                      [0]));
            break;

        case FM_CLI_LM_FRAME_SEC_SUM:

            i4IfIndex = (INT4) CLI_GET_IFINDEX ();

            i4RetStatus = FmCliLinkMonitorFrameSecSum (CliHandle,
                                                       i4IfIndex,
                                                       CLI_PTR_TO_I4 (apu1args
                                                                      [0]));
            break;

        case FM_CLI_CRITICAL_EVENT:

            i4IfIndex = (INT4) CLI_GET_IFINDEX ();

            i4RetStatus = FmCliCriticalEvent (CliHandle,
                                              i4IfIndex,
                                              CLI_PTR_TO_I4 (apu1args[0]));
            break;

        case FM_CLI_DYING_GASP:

            i4IfIndex = (INT4) CLI_GET_IFINDEX ();

            i4RetStatus = FmCliDyingGasp (CliHandle,
                                          i4IfIndex,
                                          CLI_PTR_TO_I4 (apu1args[0]));
            break;

        case FM_CLI_LINK_FAULT:

            i4IfIndex = (INT4) CLI_GET_IFINDEX ();

            i4RetStatus = FmCliLinkFault (CliHandle,
                                          i4IfIndex,
                                          CLI_PTR_TO_I4 (apu1args[0]));
            break;

        case FM_CLI_VAR_REQ_COUNT:

            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            MEMCPY (&u4ReqCount, apu1args[0], sizeof (UINT4));

            i4RetStatus = FmCliSetVarReqCount (CliHandle,
                                               i4IfIndex, u4ReqCount);
            break;

        case FM_CLI_VAR_REQ:

            i4IfIndex = (INT4) CLI_GET_IFINDEX ();

            i4RetStatus = FmCliSetVarReq (CliHandle, i4IfIndex, apu1args[0]);
            break;

        default:

            CliPrintf (CliHandle, "\r%% Unknown command \r\n");
            FmUnLock ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;

    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {

        if ((u4ErrCode > 0) && (u4ErrCode <= FM_CLI_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%% %s", gapc1FmCliErrString[u4ErrCode]);
        }

        CLI_SET_ERR (0);
    }

    FmUnLock ();
    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

/****************************************************************************
 * 
 *     FUNCTION NAME    : FmCliSetSystemCtrl                                 
 *                                                                          
 *     DESCRIPTION      : This function will start/shut Fm module         
 *                                                                          
 *     INPUT            : CliHandle - Context ID
 *                        i4Status - FM System Control                    
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
FmCliSetSystemCtrl (tCliHandle CliHandle, INT4 i4Status)
{

    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsFmSystemControl (&u4ErrorCode, i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsFmSystemControl (i4Status) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to set fault-management SystemControl\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/***************************************************************************
 * 
 *     FUNCTION NAME    : FmCliSetModuleStatus                               
 *                                                                          
 *     DESCRIPTION      : This function will enable/disable Fm module     
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4Status - FM Module status                     
 *                                                                          
 *     OUTPUT           : None         
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
FmCliSetModuleStatus (tCliHandle CliHandle, INT4 i4Status)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsFmModuleStatus (&u4ErrorCode, i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsFmModuleStatus (i4Status) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to set fault-management ModuleStatus\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : FmCliEnableDebug                                   
 *                                                                          
 *     DESCRIPTION      : This function will set the debug level            
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4Status - FM Debug Level                       
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
FmCliEnableDebug (tCliHandle CliHandle, INT4 i4Val)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4Level = 0;

    if (nmhGetFsFmTraceOption (&i4Level) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    i4Val = i4Val | i4Level;

    if (nmhTestv2FsFmTraceOption (&u4ErrorCode, i4Val) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsFmTraceOption (i4Val) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to enable debug option\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : FmCliDisableDebug                                  
 *                                                                          
 *     DESCRIPTION      : This function will reset the debug level         
 *                                                                          
 *      INPUT           : CliHandle  - CliContext ID
 *                        i4Status - FM Debug Level                       
 *                                                                          
 *     OUTPUT           : None 
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
FmCliDisableDebug (tCliHandle CliHandle, INT4 i4Val)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4Level = 0;

    if (nmhGetFsFmTraceOption (&i4Level) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    i4Val = i4Val & i4Level;

    /* If no debug set to default trace option */
    if (nmhTestv2FsFmTraceOption (&u4ErrorCode, i4Val) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsFmTraceOption (i4Val) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to disable debug option\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : FmCliClearStats                                    
 *                                                                          
 *     DESCRIPTION      : This function will clear the statistics            
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number                       
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ***************************************************************************/
PUBLIC INT4
FmCliClearStats (tCliHandle CliHandle, INT4 i4IfIndex)
{
    UINT1               u1IsClearAll = OSIX_TRUE;
    UINT4               u4ErrorCode = 0;
    INT4                i4PrevIfIndex = 0;

    if (i4IfIndex == 0)
    {
        /*Clear statistics for all interfaces */
        if (nmhGetFirstIndexFsFmVarRetrievalTable (&i4IfIndex) != SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }
    else
    {

        if (nmhValidateIndexInstanceFsFmVarRetrievalTable (i4IfIndex)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Not a valid variable retrieval table entry\r\n");
            return CLI_FAILURE;
        }

        /* Clear statistics for a particular interface */
        u1IsClearAll = OSIX_FALSE;
    }

    do
    {

        if (nmhTestv2FsFmVarRetrievalClearResponse (&u4ErrorCode, i4IfIndex,
                                                    FM_RESP_CLEAR) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsFmVarRetrievalClearResponse (i4IfIndex, FM_RESP_CLEAR)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Unable to clear the variable response\r\n");
            return CLI_FAILURE;
        }

        i4PrevIfIndex = i4IfIndex;

    }
    while ((u1IsClearAll == OSIX_TRUE) &&
           (nmhGetNextIndexFsFmVarRetrievalTable (i4PrevIfIndex, &i4IfIndex)
            == SNMP_SUCCESS));

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : FmCliShowGlobal                                    
 *                                                                          
 *     DESCRIPTION      : This function will display the FM global 
 *                        information            
 *                                                                          
 *     INPUT            : CliHandle - CliContext ID                          
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ***************************************************************************/
PUBLIC INT4
FmCliShowGlobal (tCliHandle CliHandle)
{
    INT4                i4ModStat = 0;

    nmhGetFsFmModuleStatus (&i4ModStat);

    if (i4ModStat == FM_DISABLED)
    {
        CliPrintf (CliHandle, "\r\nFault-management module has been");
        CliPrintf (CliHandle, " disabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\nFault-management module has been enabled\r\n");
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : FmCliShowMibResp                                    
 *                                                                          
 *     DESCRIPTION      : This function will display the MIB Variable 
 *                        Response            
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number                       
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ***************************************************************************/
PUBLIC INT4
FmCliShowMibResp (tCliHandle CliHandle, INT4 i4IfIndex)
{
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4Respid = 0;
    UINT4               u4PrevRetVal = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4PrevIfIndex = 0;
    INT4                i4Port = 0;

    MEMSET (au1NameStr, 0, CFA_MAX_PORT_NAME_LENGTH);

    i4Port = i4IfIndex;

    if (nmhGetFirstIndexFsFmVarResponseTable (&i4IfIndex, &u4Respid)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to get variable response table entry\r\n");
        return CLI_FAILURE;
    }

    do
    {
        i4PrevIfIndex = i4IfIndex;
        u4PrevRetVal = u4Respid;
        /* 
         * For a specific port - If the get next gets the response
         * of the unmatched interface entry then continue
         */
        if ((i4Port != 0) && (i4Port != i4IfIndex))
        {
            continue;
        }

        /* 
         * For a specific port - If the get next gets the response of 
         * the given interface entry then validate
         */
        if ((i4Port != 0) && (i4Port == i4IfIndex))
        {

            if (nmhValidateIndexInstanceFsFmVarResponseTable (i4IfIndex,
                                                              u4Respid)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Not a valid variable response id\r\n");
                return CLI_FAILURE;
            }

        }
        /* Show for specific/all port responses */
        u4PagingStatus = FmCliDispMibResp (CliHandle, i4IfIndex, u4Respid);
        if (u4PagingStatus == CLI_FAILURE)
        {
            break;
        }

    }
    while (nmhGetNextIndexFsFmVarResponseTable (i4PrevIfIndex, &i4IfIndex,
                                                u4PrevRetVal, &u4Respid)
           == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : FmCliDispMibResp                                    
 *                                                                          
 *     DESCRIPTION      : This function will display the MIB Variable 
 *                        Response for the given mib variable 
 *                        response entry
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number                       
 *                        u4Respid - Response id                       
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS - Display the output without any break
 *                                      until the end or until the user 
 *                                      presses 'q'
 *                        CLI_FAILURE - If the user pressed 'q' then quit 
 *                                      the display
 *                                                                          
 ***************************************************************************/
PUBLIC UINT4
FmCliDispMibResp (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4Respid)
{
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1OamPdu[FM_VAR_RESP_MAX_CHUNK_LENGTH];
    UINT1               au1Resp[FM_CLI_VALUE_LEN + 1];
    UINT1               u1Val = FM_CLI_BRANCH;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4RetLen = 0;
    tSNMP_OCTET_STRING_TYPE ResponsePdu;

    MEMSET (au1NameStr, 0, CFA_MAX_PORT_NAME_LENGTH);

    ResponsePdu.i4_Length = 0;
    ResponsePdu.pu1_OctetList = au1OamPdu;

    MEMSET (au1OamPdu, 0, FM_VAR_RESP_MAX_CHUNK_LENGTH);
    MEMSET (au1Resp, 0, FM_CLI_VALUE_LEN + 1);

    /* Validation of index is not done here as the passed
     * index is already validated */

    nmhGetFsFmVarResponseRx1 (i4IfIndex, u4Respid, &ResponsePdu);

    /* Check to see the end of the response for the response id 'u4Respid' */
    if (ResponsePdu.i4_Length == 0)
    {
        return u4PagingStatus;
    }                            /*Resp1 */

    /* Prints the port number if it is the new port */
    if (u4Respid == 1)
    {
        CfaCliConfGetIfName ((UINT4) i4IfIndex, (INT1 *) au1NameStr);
        CliPrintf (CliHandle, "\r\nMIB variable responses received on ");
        CliPrintf (CliHandle, "interface %s\r\n", au1NameStr);
    }

    CliOctetToHexStr (ResponsePdu.pu1_OctetList, ResponsePdu.i4_Length,
                      au1Resp);
    CliPrintf (CliHandle, "\n%-20s%s%u\r\n", " ",
               "MIB Variable response: ", u4Respid);
    u4PagingStatus = CliPrintf (CliHandle, "%-20s%s\r\n", " ",
                                "----------------------");
    CliPrintf (CliHandle, "\n%-10s %-10s %-4s %-16s %-5s", " ", "Branch",
               "Leaf", "Width/Indication", "Value");
    CliPrintf (CliHandle, "\n%-10s %-10s %-4s %-16s %-8s\r\n", " ", "------",
               "----", "----------------", "--------");
    FmCliDispMibRespDetail (CliHandle, au1Resp, &u1Val, &i4RetLen);

    MEMSET (au1OamPdu, 0, FM_VAR_RESP_MAX_CHUNK_LENGTH);
    MEMSET (au1Resp, 0, FM_VAR_RESP_MAX_CHUNK_LENGTH + 1);
    nmhGetFsFmVarResponseRx2 (i4IfIndex, u4Respid, &ResponsePdu);

    /* Check to see the end of the response for the response id 'u4Respid' */
    if (ResponsePdu.i4_Length == 0)
    {
        return u4PagingStatus;
    }                            /*Resp2 */

    CliOctetToHexStr (ResponsePdu.pu1_OctetList, ResponsePdu.i4_Length,
                      au1Resp);
    FmCliDispMibRespDetail (CliHandle, au1Resp, &u1Val, &i4RetLen);

    MEMSET (au1OamPdu, 0, FM_VAR_RESP_MAX_CHUNK_LENGTH);
    MEMSET (au1Resp, 0, FM_VAR_RESP_MAX_CHUNK_LENGTH + 1);

    nmhGetFsFmVarResponseRx3 (i4IfIndex, u4Respid, &ResponsePdu);

    /* Check to see the end of the response for the response id 'u4Respid' */
    if (ResponsePdu.i4_Length == 0)
    {
        return u4PagingStatus;
    }                            /*Resp3 */

    CliOctetToHexStr (ResponsePdu.pu1_OctetList, ResponsePdu.i4_Length,
                      au1Resp);
    FmCliDispMibRespDetail (CliHandle, au1Resp, &u1Val, &i4RetLen);

    MEMSET (au1OamPdu, 0, FM_VAR_RESP_MAX_CHUNK_LENGTH);
    MEMSET (au1Resp, 0, FM_VAR_RESP_MAX_CHUNK_LENGTH + 1);

    nmhGetFsFmVarResponseRx4 (i4IfIndex, u4Respid, &ResponsePdu);

    /* Check to see the end of the response for the response id 'u4Respid' */
    if (ResponsePdu.i4_Length == 0)
    {
        return u4PagingStatus;
    }                            /*Resp4 */

    CliOctetToHexStr (ResponsePdu.pu1_OctetList, ResponsePdu.i4_Length,
                      au1Resp);
    FmCliDispMibRespDetail (CliHandle, au1Resp, &u1Val, &i4RetLen);

    MEMSET (au1OamPdu, 0, FM_VAR_RESP_MAX_CHUNK_LENGTH);
    MEMSET (au1Resp, 0, FM_VAR_RESP_MAX_CHUNK_LENGTH + 1);

    nmhGetFsFmVarResponseRx5 (i4IfIndex, u4Respid, &ResponsePdu);

    /* Check to see the end of the response for the response id 'u4Respid' */
    if (ResponsePdu.i4_Length == 0)
    {
        return u4PagingStatus;
    }                            /*Resp5 */

    CliOctetToHexStr (ResponsePdu.pu1_OctetList, ResponsePdu.i4_Length,
                      au1Resp);
    FmCliDispMibRespDetail (CliHandle, au1Resp, &u1Val, &i4RetLen);

    MEMSET (au1OamPdu, 0, FM_VAR_RESP_MAX_CHUNK_LENGTH);
    MEMSET (au1Resp, 0, FM_VAR_RESP_MAX_CHUNK_LENGTH + 1);

    nmhGetFsFmVarResponseRx6 (i4IfIndex, u4Respid, &ResponsePdu);

    /* Check to see the end of the response for the response id 'u4Respid' */
    if (ResponsePdu.i4_Length == 0)
    {
        return u4PagingStatus;
    }                            /*Resp6 */

    CliOctetToHexStr (ResponsePdu.pu1_OctetList, ResponsePdu.i4_Length,
                      au1Resp);
    FmCliDispMibRespDetail (CliHandle, au1Resp, &u1Val, &i4RetLen);
    CliPrintf (CliHandle, "\r\n");

    return u4PagingStatus;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : FmCliDispMibRespDetail                              
 *                                                                          
 *     DESCRIPTION      : This function will display the MIB Variable 
 *                        Response and classify it as branch/leaf/width(or)
 *                        indication & value for the given mib variable 
 *                        response entry. The variable response is split up 
 *                        into six chunks and so to display the response
 *                        according to classification, the response is tracked. 
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        pu1Resp - Pointer to the response                     
 *                        pu1Flg - Flg to specify whether the display is from 
 *                        branch/variable/length/value. This value is got 
 *                        from the previous chunk (in case the response
 *                        more than one buffer length else no meaning).
 *                        pi4RetLen - If the value flag
 *                        is set, it specifies the remaining length of 
 *                        the value to be displayed which is comes in the next
 *                        chunk that follows else no meaning
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : None                           
 *                                                                          
 ***************************************************************************/
PUBLIC VOID
FmCliDispMibRespDetail (tCliHandle CliHandle, UINT1 *pu1Resp, UINT1 *pu1Flg,
                        INT4 *pi4RetLen)
{
    UINT1               u1Flg = 0;
    UINT1               au1Branch[FM_CLI_BRANCH_LEN + 1];
    UINT1               au1Leaf[FM_CLI_LEAF_LEN + 1];
    UINT1               au1Length[FM_CLI_WIDTH_LEN + 1];

    UINT1               au1HexLen[FM_HEXSTR_MIB_RESP_WIDTH_LEN];
    UINT1               au1Value[FM_CLI_VALUE_LEN + 1];
    INT4                i4Len = 0;

    do
    {
        MEMSET (au1Branch, 0, FM_CLI_BRANCH_LEN + 1);
        MEMSET (au1Leaf, 0, FM_CLI_LEAF_LEN + 1);
        MEMSET (au1Length, 0, FM_CLI_WIDTH_LEN + 1);
        MEMSET (au1HexLen, 0, FM_HEXSTR_MIB_RESP_WIDTH_LEN);
        MEMSET (au1Value, 0, FM_CLI_VALUE_LEN + 1);

        /* If the response crosses one chunk then the 
         * display will continue from the appropriate place for
         * the next chunk
         */
        switch (*pu1Flg)
        {
            case FM_CLI_BRANCH:

                MEMCPY (au1Branch, pu1Resp, FM_CLI_BRANCH_LEN);
                CliPrintf (CliHandle, "%-10s %-10s ", " ", au1Branch);
                u1Flg = FM_CLI_BRANCH;
                pu1Resp = pu1Resp + FM_CLI_BRANCH_LEN;

                /* Fall Through */

            case FM_CLI_LEAF:

                MEMCPY (au1Leaf, pu1Resp, FM_CLI_LEAF_LEN);
                CliPrintf (CliHandle, "%-4s ", au1Leaf);
                u1Flg = FM_CLI_LEAF;
                pu1Resp = pu1Resp + FM_CLI_LEAF_LEN;

                /* Fall Through */

            case FM_CLI_LENGTH:

                MEMCPY (au1Length, pu1Resp, FM_CLI_WIDTH_LEN);
                CliPrintf (CliHandle, "%-16s ", au1Length);
                u1Flg = FM_CLI_LENGTH;
                /* B4 converting HexStrToDecimal prepend 0x in front */
                SPRINTF ((CHR1 *) au1HexLen, "0x%s", au1Length);
                i4Len = CliHexStrToDecimal (au1HexLen);
                if (i4Len == HEX_FAILURE)
                {
                    i4Len = 0;
                }
                MEMSET (au1Value, 0, i4Len + 1);
                pu1Resp = pu1Resp + FM_CLI_WIDTH_LEN;

                /* Fall Through */

            case FM_CLI_VALUE:

                /* If the response is variable indication then the 
                 * value is not displayed*/
                if (FM_IS_VARIABLE_INDICATION ((INT1) i4Len) == OSIX_TRUE)
                {
                    CliPrintf (CliHandle, "%-42s\r\n", "-");
                    MEMCPY (au1Branch, pu1Resp, 2);
                    break;
                }
                /* If the previous buffer holds the partial value
                 * continue to display the rest */
                if (*pi4RetLen != 0)
                {
                    i4Len = *pi4RetLen / 2;
                }

                /* The i4Len gives the length in bytes. 
                 * The value is displayed in hex format, so inorder 
                 * to display it, multiply the i4Len by 2 
                 * (Eg. Bits- 00010001(8 bytes = 1 byte) displayed as 11 in hex 
                 * which needs 2 bytes for display) */

                MEMCPY (au1Value, pu1Resp, i4Len * 2);

                /* If the response overflows to next buffer
                 * then print the value and store the length of the
                 * remaining value to be displayed. Also the flag
                 * is kept in accordingly */
                if ((INT4) STRLEN (pu1Resp) < i4Len * 2)
                {
                    u1Flg = FM_CLI_VALUE;
                    *pi4RetLen = (i4Len * 2) - STRLEN (pu1Resp);
                    if (au1Value[0] != '\0')
                    {
                        CliPrintf (CliHandle, "%-42s", au1Value);
                    }
                }
                else
                {
                    if ((*pi4RetLen != 0) && (*pi4RetLen != i4Len * 2))
                    {
                        CliPrintf (CliHandle, "%s\r\n", au1Value);
                    }
                    else
                    {
                        CliPrintf (CliHandle, "%-42s\r\n", au1Value);
                    }

                    *pu1Flg = FM_CLI_BRANCH;
                    *pi4RetLen = 0;
                }

                pu1Resp = pu1Resp + (i4Len * 2);
                MEMCPY (au1Branch, pu1Resp, 2);
                break;

            default:
                break;
        }
    }
    while ((STRCMP (au1Branch, "00") != 0) && (*pu1Resp != '\0'));

    /* u1Flg - used to keep track of whether the response continues to the
     * next chunk and from which place*/
    *pu1Flg = u1Flg;
    return;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : FmCliShowCurrentStats                                
 *                                                                          
 *     DESCRIPTION      : This function will display the current seesion 
 *                        statistics            
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number                       
 *                                                                          
 *     OUTPUT           : None              
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ***************************************************************************/
PUBLIC INT4
FmCliShowCurrentStats (tCliHandle CliHandle, INT4 i4IfIndex)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *pi1IfName = NULL;
    UINT4               u1IsShowAll = OSIX_TRUE;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4RetVal = 0;
    INT4                i4PrevIfIndex = 0;
    INT4                i4Status = 0;

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    pi1IfName = (INT1 *) au1IfName;

    /* For all interface */
    if (i4IfIndex == 0)
    {
        if (nmhGetFirstIndexFsFmLoopbackTable (&i4IfIndex) != SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }
    else
    {

        /* For a particular interface */
        if (nmhValidateIndexInstanceFsFmLoopbackTable (i4IfIndex)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Not a valid loopback table entry\r\n");
            return CLI_FAILURE;
        }

        u1IsShowAll = OSIX_FALSE;
    }

    CliPrintf (CliHandle, "\r\n%-7s %s %-10s %-10s %-10s\r\n",
               "Port", "Loopback at", "TestPkt Rx",
               "TestPkt Tx", "TestPkt Matched");
    CliPrintf (CliHandle, "%-7s %s %s %s %s\r\n",
               "-------", "-----------", "----------",
               "----------", "---------------");

    do
    {
        if (CfaCliGetIfName ((UINT4) i4IfIndex, pi1IfName) != CLI_FAILURE)
        {
            u4PagingStatus = CliPrintf (CliHandle, "%-7s ", pi1IfName);
            nmhGetFsFmLoopbackStatus (i4IfIndex, &i4Status);

            switch (i4Status)
            {
                case FM_NO_LB:
                    CliPrintf (CliHandle, "%-12s", "None");
                    break;
                case FM_REMOTE_LB:
                    CliPrintf (CliHandle, "%-12s", "Remote");
                    break;
                case FM_UNKNOWN:
                    CliPrintf (CliHandle, "%-12s", "Unknown");
                    break;
                default:
                    break;
            }

            nmhGetFsFmLBTestRxCount (i4IfIndex, &u4RetVal);
            CliPrintf (CliHandle, "%10u", u4RetVal);
            nmhGetFsFmLBTestTxCount (i4IfIndex, &u4RetVal);
            CliPrintf (CliHandle, "%11u", u4RetVal);
            nmhGetFsFmLBTestMatchCount (i4IfIndex, &u4RetVal);
            CliPrintf (CliHandle, "%16u\r\n", u4RetVal);
        }

        if (u1IsShowAll == OSIX_FALSE)
        {
            break;
        }

        i4PrevIfIndex = i4IfIndex;

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            break;
        }

    }
    while (nmhGetNextIndexFsFmLoopbackTable (i4PrevIfIndex, &i4IfIndex)
           == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : FmCliShowPortConfig                                   
 *                                                                          
 *     DESCRIPTION      : This function will display the Link event actions 
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number                       
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ***************************************************************************/
PUBLIC INT4
FmCliShowPortConfig (tCliHandle CliHandle, INT4 i4IfIndex)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *pi1IfName = NULL;
    UINT4               u1IsShowAll = OSIX_TRUE;
    UINT4               u4MaxVar = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4RetVal = 0;
    INT4                i4PrevIfIndex = 0;

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    pi1IfName = (INT1 *) (&au1IfName[0]);

    /* For all interface */
    if (i4IfIndex == 0)
    {
        if (nmhGetFirstIndexFsFmLinkEventTable (&i4IfIndex) != SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }
    else
    {
        /* For a particular interface */
        if (nmhValidateIndexInstanceFsFmLinkEventTable (i4IfIndex)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Not a valid link event table entry\r\n");
            return CLI_FAILURE;
        }

        u1IsShowAll = OSIX_FALSE;
    }

    CliPrintf (CliHandle, "%45s\r\n", "Link Event Action");
    CliPrintf (CliHandle, "%s%s\r\n",
               "        ---------",
               "--------------------------------------------------");
    CliPrintf (CliHandle, "%14s %6s %7s %12s %3s %s %6s %9s\n",
               "Symbol", "Frame", "Frame", "Frame secs",
               "Critical", "Dying", "Link", "MaxVar");
    CliPrintf (CliHandle, "%-7s %-7s %14s %8s %8s %7s %8s %6s\r\n",
               "Port", "Period", "Period", "Summary", "Event",
               "Gasp", "Fault", "/Req");
    CliPrintf (CliHandle, "%-7s %s %s %s %s %s %s %s %s\r\n",
               "-------", "-------", "-------", "-------", "----------",
               "--------", "-------", "-------", "------");

    do
    {
        if (CfaCliGetIfName ((UINT4) i4IfIndex, pi1IfName) != CLI_FAILURE)
        {
            CliPrintf (CliHandle, "%-7s ", pi1IfName);
            nmhGetFsFmSymPeriodAction (i4IfIndex, &i4RetVal);

            switch (i4RetVal)
            {
                case FM_ACTION_NONE:
                    CliPrintf (CliHandle, "%-8s", "none");
                    break;
                case FM_ACTION_WARNING:
                    CliPrintf (CliHandle, "%-8s", "warning");
                    break;
                default:
                    break;
            }

            nmhGetFsFmFrameAction (i4IfIndex, &i4RetVal);

            switch (i4RetVal)
            {
                case FM_ACTION_NONE:
                    CliPrintf (CliHandle, "%-8s", "none");
                    break;
                case FM_ACTION_WARNING:
                    CliPrintf (CliHandle, "%-8s", "warning");
                    break;
                default:
                    break;
            }

            nmhGetFsFmFramePeriodAction (i4IfIndex, &i4RetVal);

            switch (i4RetVal)
            {
                case FM_ACTION_NONE:
                    CliPrintf (CliHandle, "%-8s", "none");
                    break;
                case FM_ACTION_WARNING:
                    CliPrintf (CliHandle, "%-8s", "warning");
                    break;
                default:
                    break;
            }

            nmhGetFsFmFrameSecSummAction (i4IfIndex, &i4RetVal);

            switch (i4RetVal)
            {
                case FM_ACTION_NONE:
                    CliPrintf (CliHandle, "%-11s", "none");
                    break;
                case FM_ACTION_WARNING:
                    CliPrintf (CliHandle, "%-11s", "warning");
                    break;
                default:
                    break;
            }

            nmhGetFsFmCriticalEventAction (i4IfIndex, &i4RetVal);

            switch (i4RetVal)
            {
                case FM_ACTION_NONE:
                    CliPrintf (CliHandle, "%-9s", "none");
                    break;
                case FM_ACTION_WARNING:
                    CliPrintf (CliHandle, "%-9s", "warning");
                    break;
                case FM_ACTION_BLOCK:
                    CliPrintf (CliHandle, "%-9s", "block");
                    break;
                default:
                    break;
            }

            nmhGetFsFmDyingGaspAction (i4IfIndex, &i4RetVal);

            switch (i4RetVal)
            {
                case FM_ACTION_NONE:
                    CliPrintf (CliHandle, "%-8s", "none");
                    break;
                case FM_ACTION_WARNING:
                    CliPrintf (CliHandle, "%-8s", "warning");
                    break;
                case FM_ACTION_BLOCK:
                    CliPrintf (CliHandle, "%-9s", "block");
                    break;
                default:
                    break;
            }

            nmhGetFsFmLinkFaultAction (i4IfIndex, &i4RetVal);

            switch (i4RetVal)
            {
                case FM_ACTION_NONE:
                    CliPrintf (CliHandle, "%-8s", "none");
                    break;
                case FM_ACTION_WARNING:
                    CliPrintf (CliHandle, "%-8s", "warning");
                    break;
                case FM_ACTION_BLOCK:
                    CliPrintf (CliHandle, "%-9s", "block");
                    break;
                default:
                    break;
            }

            nmhGetFsFmVarRetrievalMaxVar (i4IfIndex, &u4MaxVar);
            u4PagingStatus = CliPrintf (CliHandle, "%-12u\r\n", u4MaxVar);
        }

        if (u1IsShowAll == OSIX_FALSE)
        {
            break;
        }

        i4PrevIfIndex = i4IfIndex;

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            break;
        }
    }
    while (nmhGetNextIndexFsFmLinkEventTable (i4PrevIfIndex, &i4IfIndex)
           == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : FmCliShowCurrentStatsDetail                          
 *                                                                          
 *     DESCRIPTION      : This function will display the current 
 *                        session statistics in detail            
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number                       
 *                                                                          
 *     OUTPUT           : None
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ***************************************************************************/
PUBLIC INT4
FmCliShowCurrentStatsDetail (tCliHandle CliHandle, INT4 i4IfIndex)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1IsShowAll = OSIX_TRUE;
    INT1               *pi1IfName = NULL;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4PrevIfIndex = 0;
    INT4                i4Status = 0;

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);

    pi1IfName = (INT1 *) au1IfName;

    /* For All Interface */
    if (i4IfIndex == 0)
    {
        if (nmhGetFirstIndexFsFmLoopbackTable (&i4IfIndex) != SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }
    else
    {

        /* For particular interface */
        if (nmhValidateIndexInstanceFsFmLoopbackTable (i4IfIndex)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Not a valid loopback table entry\r\n");
            return CLI_FAILURE;
        }

        u1IsShowAll = OSIX_FALSE;
    }

    do
    {
        if (CfaCliGetIfName ((UINT4) i4IfIndex, pi1IfName) != CLI_FAILURE)
        {
            CliPrintf (CliHandle, "\nPort: %s\r\n", pi1IfName);
            nmhGetFsFmLoopbackStatus (i4IfIndex, &i4Status);

            switch (i4Status)
            {
                case FM_NO_LB:
                    u4PagingStatus = CliPrintf (CliHandle, "Loopback: ");
                    CliPrintf (CliHandle,
                               "Remote OAM not in loopback mode\r\n");
                    CliPrintf (CliHandle, "No current session exist\r\n");
                    break;
                case FM_REMOTE_LB:
                    u4PagingStatus = CliPrintf (CliHandle, "Loopback: ");
                    CliPrintf (CliHandle, "Remote OAM in loopback mode\r\n");
                    FmCliShowCurrentSessDetails (CliHandle, i4IfIndex);
                    break;
                case FM_UNKNOWN:
                    u4PagingStatus = CliPrintf (CliHandle, "Loopback: ");
                    CliPrintf (CliHandle,
                               "Remote OAM loopback mode is not known\r\n");
                    CliPrintf (CliHandle, "No current session exist\r\n");
                    break;
                default:
                    break;
            }

        }
        if (u1IsShowAll == OSIX_FALSE)
        {
            break;
        }

        i4PrevIfIndex = i4IfIndex;

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            break;
        }

    }
    while (nmhGetNextIndexFsFmLoopbackTable (i4PrevIfIndex, &i4IfIndex)
           == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : FmCliShowCurrentSessDetails                          
 *                                                                          
 *     DESCRIPTION      : This function will display the current 
 *                        session details            
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number                       
 *                                                                          
 *     OUTPUT           : None
 *                                                                          
 *     RETURNS          : CLI_SUCCESS                           
 *                                                                          
 ***************************************************************************/
PUBLIC INT4
FmCliShowCurrentSessDetails (tCliHandle CliHandle, INT4 i4IfIndex)
{
    UINT1               au1Date[FM_MAX_DATE_LEN];
    UINT1               au1DispDate[FM_MAX_DATE_LEN];
    UINT1               au1Pattern[FM_LB_TEST_PATTERN_LENGTH];
    UINT1               au1DispPattern[FM_CLI_TST_PTRN_STR_LEN + 1];
    UINT4               u4RetVal = 0;
    INT4                i4RetVal = 0;
    tSNMP_OCTET_STRING_TYPE TestPattern;
    tSNMP_OCTET_STRING_TYPE Date;

    MEMSET (au1Pattern, 0, FM_LB_TEST_PATTERN_LENGTH);
    MEMSET (au1DispPattern, 0, FM_CLI_TST_PTRN_STR_LEN + 1);
    MEMSET (&Date, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&TestPattern, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    TestPattern.pu1_OctetList = au1Pattern;

    Date.pu1_OctetList = au1Date;
    Date.i4_Length = FM_MAX_DATE_LEN;

    MEMSET (au1Date, 0, FM_MAX_DATE_LEN);
    MEMSET (au1DispDate, 0, FM_MAX_DATE_LEN);
    nmhGetFsFmLBTestStartTimestamp (i4IfIndex, &Date);
    MEMCPY (au1DispDate, Date.pu1_OctetList, Date.i4_Length);
    CliPrintf (CliHandle, "Start: %s\r\n", au1DispDate);

    MEMSET (au1DispDate, 0, FM_MAX_DATE_LEN);
    nmhGetFsFmLBTestEndTimestamp (i4IfIndex, &Date);
    MEMCPY (au1DispDate, Date.pu1_OctetList, Date.i4_Length);
    CliPrintf (CliHandle, "End: Still running\r\n");

    nmhGetFsFmLBTestPattern (i4IfIndex, &TestPattern);
    /* Displays the test pattern in hex string (eg: ae01f0c2) */
    CliOctetToHexStr (TestPattern.pu1_OctetList,
                      FM_LB_TEST_PATTERN_LENGTH, au1DispPattern);
    CliPrintf (CliHandle, "Test pattern: %s\n", au1DispPattern);

    nmhGetFsFmLBTestPktSize (i4IfIndex, &u4RetVal);
    CliPrintf (CliHandle, "Test packet size: %u\r\n", u4RetVal);

    nmhGetFsFmLBTestWaitTime (i4IfIndex, &i4RetVal);
    CliPrintf (CliHandle, "Test wait-time: %d\r\n", i4RetVal);

    nmhGetFsFmLBTestCount (i4IfIndex, &u4RetVal);
    CliPrintf (CliHandle, "Test packet count: %u\r\n", u4RetVal);
    CliPrintf (CliHandle, "Test statistics:\r\n");

    nmhGetFsFmLBTestRxCount (i4IfIndex, &u4RetVal);
    CliPrintf (CliHandle, "%-10s%s%u\r\n", " ", "Test packets Rx: ", u4RetVal);

    nmhGetFsFmLBTestTxCount (i4IfIndex, &u4RetVal);
    CliPrintf (CliHandle, "%-10s%s%u\r\n", " ", "Test packets Tx: ", u4RetVal);

    nmhGetFsFmLBTestMatchCount (i4IfIndex, &u4RetVal);
    CliPrintf (CliHandle, "%-10s%s%u\r\n", " ",
               "Test packets matched: ", u4RetVal);

    return CLI_SUCCESS;
}

/***************************************************************************
 *     FUNCTION NAME    : FmCliShowLastStats                                    
 *                                                                          
 *     DESCRIPTION      : This function will display the last 
 *                        session statistics            
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number                       
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ***************************************************************************/
PUBLIC INT4
FmCliShowLastStats (tCliHandle CliHandle, INT4 i4IfIndex)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1Date[FM_MAX_DATE_LEN];
    UINT1               au1DispDate[FM_MAX_DATE_LEN];
    INT1               *pi1IfName = NULL;
    UINT4               u1IsShowAll = OSIX_TRUE;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4RetVal = 0;
    INT4                i4PrevIfIndex = 0;
    tSNMP_OCTET_STRING_TYPE Date;

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1DispDate, 0, FM_MAX_DATE_LEN);
    pi1IfName = (INT1 *) (&au1IfName[0]);
    Date.pu1_OctetList = &au1Date[0];
    Date.i4_Length = FM_MAX_DATE_LEN;

    /* For All Interface */
    if (i4IfIndex == 0)
    {
        if (nmhGetFirstIndexFsFmLBStatsTable (&i4IfIndex) != SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }
    else
    {
        if (nmhValidateIndexInstanceFsFmLBStatsTable (i4IfIndex)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Not a valid loopback statistics table entry\r\n");
            return CLI_FAILURE;
        }

        /* For particular interface */
        u1IsShowAll = OSIX_FALSE;
    }

    CliPrintf (CliHandle,
               "\r\n%-7s %s %-10s %-10s %-10s\r\n",
               "Port", "Loopback at", "TestPkt Rx",
               "TestPkt Tx", "TestPkt Matched");
    CliPrintf (CliHandle, "%-7s %s %s %s %s\r\n",
               "-------", "-----------", "----------",
               "----------", "---------------");

    do
    {
        if (CfaCliGetIfName ((UINT4) i4IfIndex, pi1IfName) != CLI_FAILURE)
        {
            CliPrintf (CliHandle, "%-7s ", pi1IfName);

            MEMSET (au1Date, 0, FM_MAX_DATE_LEN);
            nmhGetFsFmLBStatsStartTimestamp (i4IfIndex, &Date);
            if (MEMCMP (Date.pu1_OctetList, au1DispDate, FM_MAX_DATE_LEN) != 0)
            {
                u4PagingStatus = CliPrintf (CliHandle, "%-12s", "Remote");
            }
            else
            {
                u4PagingStatus = CliPrintf (CliHandle, "%-12s", "None");
            }

            nmhGetFsFmLBStatsRxCount (i4IfIndex, &u4RetVal);
            CliPrintf (CliHandle, "%10u", u4RetVal);
            nmhGetFsFmLBStatsTxCount (i4IfIndex, &u4RetVal);
            CliPrintf (CliHandle, "%11u", u4RetVal);
            nmhGetFsFmLBStatsMatchCount (i4IfIndex, &u4RetVal);
            CliPrintf (CliHandle, "%16u\r\n", u4RetVal);
        }

        i4PrevIfIndex = i4IfIndex;

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            break;
        }

    }
    while ((u1IsShowAll == OSIX_TRUE) &&
           (nmhGetNextIndexFsFmLBStatsTable (i4PrevIfIndex, &i4IfIndex)
            == SNMP_SUCCESS));

    return CLI_SUCCESS;
}

/****************************************************************************
 * 
 *     FUNCTION NAME    : FmCliShowLastStatsDetail                          
 *                                                                          
 *     DESCRIPTION      : This function will display the last 
 *                        session statistics in detail           
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number                       
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ***************************************************************************/
PUBLIC INT4
FmCliShowLastStatsDetail (tCliHandle CliHandle, INT4 i4IfIndex)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *pi1IfName = NULL;
    UINT4               u1IsShowAll = OSIX_TRUE;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4PrevIfIndex = 0;

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    pi1IfName = (INT1 *) au1IfName;

    /* For All Interface */
    if (i4IfIndex == 0)
    {
        if (nmhGetFirstIndexFsFmLBStatsTable (&i4IfIndex) != SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }
    else
    {

        if (nmhValidateIndexInstanceFsFmLBStatsTable (i4IfIndex)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Not a valid loopback statistics table entry\r\n");
            return CLI_FAILURE;
        }

        /* For particular interface */
        u1IsShowAll = OSIX_FALSE;
    }

    do
    {
        if (CfaCliGetIfName ((UINT4) i4IfIndex, pi1IfName) != CLI_FAILURE)
        {
            u4PagingStatus = CliPrintf (CliHandle, "\nPort: %s\r\n", pi1IfName);
            FmCliShowLastSessDetails (CliHandle, i4IfIndex);
        }

        if (u1IsShowAll == OSIX_FALSE)
        {
            break;
        }

        i4PrevIfIndex = i4IfIndex;

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            break;
        }

    }
    while (nmhGetNextIndexFsFmLBStatsTable (i4PrevIfIndex, &i4IfIndex)
           == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/****************************************************************************
 * 
 *     FUNCTION NAME    : FmCliShowLastSessDetails                          
 *                                                                          
 *     DESCRIPTION      : This function will display the last 
 *                        session details           
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number                       
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ***************************************************************************/
PUBLIC INT4
FmCliShowLastSessDetails (tCliHandle CliHandle, INT4 i4IfIndex)
{
    UINT1               au1Date[FM_MAX_DATE_LEN];
    UINT1               au1DispDate[FM_MAX_DATE_LEN];
    UINT4               u4RetVal = 0;
    tSNMP_OCTET_STRING_TYPE Date;

    MEMSET (au1Date, 0, FM_MAX_DATE_LEN);
    MEMSET (au1DispDate, 0, FM_MAX_DATE_LEN);
    MEMSET (&Date, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    Date.pu1_OctetList = au1Date;
    Date.i4_Length = FM_MAX_DATE_LEN;

    nmhGetFsFmLBStatsStartTimestamp (i4IfIndex, &Date);
    if (MEMCMP (Date.pu1_OctetList, au1DispDate, FM_MAX_DATE_LEN) != 0)
    {
        CliPrintf (CliHandle, "Loopback: %s\r\n",
                   "Remote OAM in loopback mode");

        MEMCPY (au1DispDate, Date.pu1_OctetList, Date.i4_Length);

        CliPrintf (CliHandle, "Start: %s\r\n", au1DispDate);

        MEMSET (au1Date, 0, FM_MAX_DATE_LEN);
        MEMSET (au1DispDate, 0, FM_MAX_DATE_LEN);

        nmhGetFsFmLBStatsEndTimestamp (i4IfIndex, &Date);

        MEMCPY (au1DispDate, Date.pu1_OctetList, Date.i4_Length);

        CliPrintf (CliHandle, "End: %s\r\n", au1DispDate);
        CliPrintf (CliHandle, "Test statistics:\r\n", u4RetVal);

        nmhGetFsFmLBStatsRxCount (i4IfIndex, &u4RetVal);
        CliPrintf (CliHandle, "%-10s%s%u\r\n", " ", "Test packets Rx: ",
                   u4RetVal);

        nmhGetFsFmLBStatsTxCount (i4IfIndex, &u4RetVal);
        CliPrintf (CliHandle, "%-10s%s%u\r\n", " ", "Test packets Tx: ",
                   u4RetVal);

        nmhGetFsFmLBStatsMatchCount (i4IfIndex, &u4RetVal);
        CliPrintf (CliHandle, "%-10s%s%u\r\n", " ", "Test packets Matched: ",
                   u4RetVal);

    }
    else
    {
        CliPrintf (CliHandle, " No previous session exist\r\n");
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : FmCliLBTestConfig                            
 *                                                                          
 *     DESCRIPTION      : This function is will configure the loopback 
 *                        test            
 *                                                                          
 *     INPUT            : CliHandle      - CliContext ID
 *                        i4IfIndex      - Port Number
 *                        u4TestPktCnt   - Total number of test packets
 *                        u4TestPktSize  - Test packet size
 *                        pu1TestPattern - Test pattern
 *                        i4TestWaitTime - TestWaitTime
 *                        i4TestCommand  - TestCommand
 *                        
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ***************************************************************************/
PUBLIC INT4
FmCliLBTestConfig (tCliHandle CliHandle, INT4 i4IfIndex,
                   UINT4 u4TestPktCnt, UINT4 u4TestPktSize,
                   UINT1 *pu1TestPattern, INT4 i4TestWaitTime,
                   INT4 i4TestCommand)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1Pattern[FM_LB_TEST_PATTERN_LENGTH];
    INT1               *pi1IfName = NULL;
    UINT4               u4ErrorCode = 0;
    UINT4               u4RetVal = 0;
    tSNMP_OCTET_STRING_TYPE TestPattern;

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1Pattern, 0, FM_LB_TEST_PATTERN_LENGTH);
    pi1IfName = (INT1 *) (&au1IfName[0]);

    /* Converts the given test pattern (hex string (eg: ae01f0c2 )) to its 
     * equivalent octet
     */
    CliHexStrToOctet (pu1TestPattern, &au1Pattern[0],
                      FM_LB_TEST_PATTERN_LENGTH);
    TestPattern.i4_Length = FM_LB_TEST_PATTERN_LENGTH;
    TestPattern.pu1_OctetList = &au1Pattern[0];

    if (u4TestPktCnt)
    {

        if (nmhTestv2FsFmLBTestCount (&u4ErrorCode, i4IfIndex,
                                      u4TestPktCnt) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    if (u4TestPktSize)
    {

        if (nmhTestv2FsFmLBTestPktSize (&u4ErrorCode, i4IfIndex,
                                        u4TestPktSize) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    if (pu1TestPattern)
    {

        if (nmhTestv2FsFmLBTestPattern (&u4ErrorCode, i4IfIndex,
                                        (tSNMP_OCTET_STRING_TYPE *) &
                                        TestPattern) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    if (i4TestWaitTime)
    {

        if (nmhTestv2FsFmLBTestWaitTime (&u4ErrorCode, i4IfIndex,
                                         i4TestWaitTime) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    if (i4TestCommand)
    {
        if (nmhTestv2FsFmLBTestCommand (&u4ErrorCode, i4IfIndex,
                                        FM_START_LB_TEST) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    if (u4TestPktCnt)
    {
        if (nmhSetFsFmLBTestCount (i4IfIndex, u4TestPktCnt) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Unable to set loopback test packet count\r\n");
            return CLI_FAILURE;
        }

    }

    if (u4TestPktSize)
    {
        if (nmhSetFsFmLBTestPktSize (i4IfIndex, u4TestPktSize) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Unable to set loopback test packet size\r\n");
            return CLI_FAILURE;
        }

    }

    if (pu1TestPattern)
    {
        if (nmhSetFsFmLBTestPattern (i4IfIndex,
                                     (tSNMP_OCTET_STRING_TYPE *) & TestPattern)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Unable to set loopback test pattern\r\n");
            return CLI_FAILURE;
        }

    }

    if (i4TestWaitTime)
    {
        if (nmhSetFsFmLBTestWaitTime (i4IfIndex, i4TestWaitTime)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Unable to set loopback test wait time\r\n");
            return CLI_FAILURE;
        }

    }

    if (i4TestCommand)
    {

        CliPrintf (CliHandle, "\r\nTransmitting....\r\n");

        if (nmhSetFsFmLBTestCommand (i4IfIndex, FM_START_LB_TEST)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Unable to set loopback test command\r\n");
            return CLI_FAILURE;
        }

        if (CfaCliGetIfName ((UINT4) i4IfIndex, pi1IfName) != CLI_FAILURE)
        {
            nmhGetFsFmLBTestTxCount (i4IfIndex, &u4RetVal);
            CliPrintf (CliHandle,
                       "\tOAM Remote Loopback Test %s: %u transmitted,",
                       pi1IfName, u4RetVal);
            nmhGetFsFmLBTestRxCount (i4IfIndex, &u4RetVal);
            CliPrintf (CliHandle, " %u received\r\n", u4RetVal);
        }
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : FmCliLinkMonitorSymbol                             
 *                                                                          
 *     DESCRIPTION      : This function will specify the action to be taken 
 *                        when a symbol period notification occurs
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number
 *                        i4Action  - none/warning
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
FmCliLinkMonitorSymbol (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4Action)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsFmSymPeriodAction (&u4ErrorCode, i4IfIndex, i4Action)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsFmSymPeriodAction (i4IfIndex, i4Action) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to set the action for ethernet-oam ");
        CliPrintf (CliHandle, "symbol period\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : FmCliLinkMonitorFrame                                
 *                                                                          
 *     DESCRIPTION      : This function will specify the action to be taken 
 *                        when a frame notification occurs
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number
 *                        i4Action  - none/warning
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
FmCliLinkMonitorFrame (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4Action)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsFmFrameAction (&u4ErrorCode, i4IfIndex, i4Action)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsFmFrameAction (i4IfIndex, i4Action) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to set the action for ethernet-oam ");
        CliPrintf (CliHandle, "frame\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : FmCliLinkMonitorFramePeriod                      
 *                                                                          
 *     DESCRIPTION      : This function will specify the action to be taken 
 *                        when a frame period notification occurs
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number
 *                        i4Action  - none/warning
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
FmCliLinkMonitorFramePeriod (tCliHandle CliHandle, INT4 i4IfIndex,
                             INT4 i4Action)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsFmFramePeriodAction (&u4ErrorCode, i4IfIndex, i4Action)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsFmFramePeriodAction (i4IfIndex, i4Action) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to set the action for ethernet-oam ");
        CliPrintf (CliHandle, "frame period\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : FmCliLinkMonitorFrameSecSum                          
 *                                                                          
 *     DESCRIPTION      : This function will specify the action to be taken 
 *                        when a frames seconds summary notification 
 *                        occurs
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number
 *                        i4Action  - none/warning
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ***************************************************************************/
PUBLIC INT4
FmCliLinkMonitorFrameSecSum (tCliHandle CliHandle, INT4 i4IfIndex,
                             INT4 i4Action)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsFmFrameSecSummAction (&u4ErrorCode, i4IfIndex, i4Action)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsFmFrameSecSummAction (i4IfIndex, i4Action) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to set the action for ethernet-oam ");
        CliPrintf (CliHandle, "frame seconds summary\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : FmCliCriticalEvent
 *                                                                          
 *     DESCRIPTION      : This function will specify the action to be taken 
 *                        when a critical event occurs
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number
 *                        i4Action  - none/warning
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
FmCliCriticalEvent (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4Action)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsFmCriticalEventAction (&u4ErrorCode, i4IfIndex, i4Action)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsFmCriticalEventAction (i4IfIndex, i4Action) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to set the action for critical event\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : FmCliDyingGasp                                  
 *                                                                          
 *     DESCRIPTION      : This function will specify the action to be 
 *                        taken when dying gasp occurs.
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number
 *                        i4Action  - none/warning
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
FmCliDyingGasp (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4Action)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsFmDyingGaspAction (&u4ErrorCode, i4IfIndex, i4Action)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsFmDyingGaspAction (i4IfIndex, i4Action) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to set the action for dying gasp\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : FmCliLinkFault                                   
 *                                                                          
 *     DESCRIPTION      : This function will specify the action to be taken 
 *                        when a link fault occurs
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number
 *                        i4Action  - none/warning
 *                                                                          
 *     OUTPUT           : None             
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ***************************************************************************/
PUBLIC INT4
FmCliLinkFault (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4Action)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsFmLinkFaultAction (&u4ErrorCode, i4IfIndex, i4Action)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsFmLinkFaultAction (i4IfIndex, i4Action) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to set the action for link fault\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : FmCliSetVarReqCount                                
 *                                                                          
 *     DESCRIPTION      : This function will set the maximum number of
 *                        variable descriptors that can be sent on the 
 *                        interface
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number                     
 *                        i4Cnt     - Max No. of descriptors
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
FmCliSetVarReqCount (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4Cnt)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsFmVarRetrievalMaxVar (&u4ErrorCode, i4IfIndex, u4Cnt)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsFmVarRetrievalMaxVar (i4IfIndex, u4Cnt) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to set the maximumum variable descriptors\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : FmCliSetVarReq                                
 *                                                                          
 *     DESCRIPTION      : This function will set the variable request          
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number                       
 *                        pu1Req    - Variable Request
 *                                                                          
 *     OUTPUT           : None
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ***************************************************************************/
PUBLIC INT4
FmCliSetVarReq (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 *pu1Req)
{
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE VarReq;

    VarReq.pu1_OctetList = pu1Req;
    VarReq.i4_Length = CLI_STRLEN (pu1Req);

    if (nmhTestv2FsFmVarRetrievalRequest (&u4ErrorCode, i4IfIndex,
                                          &VarReq) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsFmVarRetrievalRequest (i4IfIndex, &VarReq) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set the variable request\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : FmCliShowDebugging                                    
 *                                                                           
 *     DESCRIPTION      : This function prints the FM debug level            
 *                                                                           
 *     INPUT            : CliHandle  - CliContext ID                            
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            
 *                                                                           
 ****************************************************************************/
PUBLIC VOID
FmCliShowDebugging (tCliHandle CliHandle)
{
    INT4                i4DbgLevel = 0;
    INT4                i4Status = 0;

    nmhGetFsFmSystemControl (&i4Status);

    if (i4Status == FM_SHUTDOWN)
    {
        return;
    }

    nmhGetFsFmTraceOption (&i4DbgLevel);

    if (i4DbgLevel == 0)
    {
        return;
    }

    CliPrintf (CliHandle, "\rFM :");

    if ((i4DbgLevel & INIT_SHUT_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  FM init and shutdown debugging is on");
    }

    if ((i4DbgLevel & MGMT_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  FM management debugging is on");
    }

    if ((i4DbgLevel & CONTROL_PLANE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  FM control plane debugging is on");
    }

    if ((i4DbgLevel & DUMP_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  FM packet dump debugging is on");
    }

    if ((i4DbgLevel & OS_RESOURCE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  FM resources debugging is on");
    }

    if ((i4DbgLevel & ALL_FAILURE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  FM error debugging is on");
    }

    if ((i4DbgLevel & BUFFER_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  FM buffer debugging is on");
    }

    if ((i4DbgLevel & FM_CRITICAL_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  FM critical debugging is on");
    }

    if ((i4DbgLevel & FM_LOOPBACK_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  FM remote loopback debugging is on");
    }

    if ((i4DbgLevel & FM_EVENTTRIG_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  FM fault event trigger debugging is on");
    }

    if ((i4DbgLevel & FM_EVENTRX_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  FM event reception debugging is on");
    }

    if ((i4DbgLevel & FM_VAR_REQRESP_TRC) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  FM MIB variable request/response debugging is on");
    }

    if ((i4DbgLevel & FM_FN_ENTRY_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  FM MIB function entry debugging is on");
    }

    if ((i4DbgLevel & FM_FN_EXIT_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  FM MIB function exit debugging is on");
    }

    CliPrintf (CliHandle, "\r\n");

    return;
}

/****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : FmCliShowRunningConfig                                
 *                                                                           
 *     DESCRIPTION      : This function prints the FM debug level for        
 *                        the system                                         
 *                                                                           
 *     INPUT            : CliHandle  - CliContext ID                          
 *                        u4Module - Specified Module (FM/all) 
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 ****************************************************************************/
PUBLIC VOID
FmCliShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module)
{

    FmCliShowRunningConfigGlobal (CliHandle);

    if (u4Module == ISS_FM_SHOW_RUNNING_CONFIG)
    {

        FmCliShowRunningConfigInterface (CliHandle);

    }

    return;

}

/****************************************************************************
 *                                                                           
 *     Function Name    : FmCliShowRunningConfigGlobal                          
 *                                                                           
 *     Description      : This function displays the current global          
 *                        configuration information of FM                    
 *                                                                           
 *     Input Parameters : CliHandle - CliContext ID                          
 *                                                                           
 *     Output Parameters: None                                               
 *                                                                           
 *     Return Value     : None                            
 *                                                                           
 ****************************************************************************/
PUBLIC VOID
FmCliShowRunningConfigGlobal (tCliHandle CliHandle)
{
    INT4                i4RetVal = 0;

    CliRegisterLock (CliHandle, FmLock, FmUnLock);
    FmLock ();

    nmhGetFsFmSystemControl (&i4RetVal);

    if (i4RetVal == FM_START)
    {
        CliPrintf (CliHandle, "no shutdown fault-management\r\n");

        nmhGetFsFmModuleStatus (&i4RetVal);

        if (i4RetVal == FM_ENABLED)
        {
            CliPrintf (CliHandle, "set fault-management enable\r\n");
        }

    }

    FmUnLock ();
    CliUnRegisterLock (CliHandle);

    return;
}

/****************************************************************************
 *                                                                           
 *     Function Name    : FmCliShowRunningConfigInterface                       
 *                                                                           
 *     Description      : This function displays the current interface       
 *                        configuration information of FM for all            
 *                        interfaces                                         
 *                                                                           
 *     Input Parameters : CliHandle - CliContext ID                          
 *                                                                           
 *     Output Parameters: None                                               
 *                                                                           
 *     Return Value     : None                            
 *                                                                           
 ****************************************************************************/
PUBLIC VOID
FmCliShowRunningConfigInterface (tCliHandle CliHandle)
{
    BOOL1               b1FlgModSpec = OSIX_TRUE;
    UINT4               i4IfIndex = 0;

    for (i4IfIndex = FM_MIN_PORTS; i4IfIndex <= FM_MAX_PORTS; i4IfIndex++)
    {
        FmCliShowRunningConfigIfInfo (CliHandle, i4IfIndex, b1FlgModSpec);
    }

    return;
}

/****************************************************************************
 *                                                                           
 *     Function Name    : FmCliShowRunningConfigIfInfo                
 *                                                                           
 *     Description      : This function displays the current interface       
 *                        configuration information of FM for particular     
 *                        interface                                          
 *                                                                           
 *     Input Parameters : CliHandle - CliContext ID                          
 *                        i4IfIndex - Port Number
 *                        b1FlgModSpec - Flag to specify module specific 
 *                        ShowRunningConfig or not
 *                                                                           
 *     Output Parameters: None                                               
 *                                                                           
 *     Return Value     : None                            
 *                                                                           
 ****************************************************************************/
PUBLIC VOID
FmCliShowRunningConfigIfInfo (tCliHandle CliHandle, INT4 i4IfIndex,
                              BOOL1 b1FlgModSpec)
{
    UINT1               u1DispFlag = OSIX_FALSE;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1Pattern[FM_LB_TEST_PATTERN_LENGTH];
    UINT1               au1DispPattern[FM_CLI_TST_PTRN_STR_LEN + 1];
    UINT1              *pu1DispPattern = NULL;
    UINT4               u4TestPattern = FM_DEFAULT_TEST_PATTERN;
    UINT4               u4PktCnt = 0;
    UINT4               u4PktSize = 0;
    UINT4               u4MaxVar = 0;
    INT4                i4RetVal = 0;
    tSNMP_OCTET_STRING_TYPE TestPattern;

    MEMSET (au1NameStr, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1Pattern, 0, FM_LB_TEST_PATTERN_LENGTH);
    TestPattern.pu1_OctetList = au1Pattern;
    TestPattern.i4_Length = FM_LB_TEST_PATTERN_LENGTH;

    pu1DispPattern = &au1DispPattern[0];
    MEMSET (pu1DispPattern, 0, FM_CLI_TST_PTRN_STR_LEN + 1);

    CliRegisterLock (CliHandle, FmLock, FmUnLock);
    FmLock ();

    if (nmhValidateIndexInstanceFsFmLoopbackTable (i4IfIndex) != SNMP_FAILURE)
    {

        nmhGetFsFmLBTestCount (i4IfIndex, &u4PktCnt);
        nmhGetFsFmLBTestPktSize (i4IfIndex, &u4PktSize);
        nmhGetFsFmLBTestPattern (i4IfIndex, &TestPattern);
        nmhGetFsFmLBTestWaitTime (i4IfIndex, &i4RetVal);

        if ((u4PktCnt != FM_DEFAULT_TEST_PKT_COUNT)
            || (u4PktSize != FM_DEFAULT_TEST_PKT_SIZE)
            || (MEMCMP (TestPattern.pu1_OctetList,
                        &u4TestPattern, FM_LB_TEST_PATTERN_LENGTH) != 0)
            || (i4RetVal != FM_DEFAULT_TEST_WAIT_TIME))
        {
            /* Prints the port name if it is not previously displayed */
            if (FmCliSrcPrintPortName (CliHandle, i4IfIndex, b1FlgModSpec,
                                       &u1DispFlag) == CLI_SUCCESS)
            {
                CliPrintf
                    (CliHandle,
                     " fault-management ethernet-oam remote-loopback");
                if (u4PktCnt != FM_DEFAULT_TEST_PKT_COUNT)
                {
                    CliPrintf (CliHandle, " count %u", u4PktCnt);
                }

                if (u4PktSize != FM_DEFAULT_TEST_PKT_SIZE)
                {
                    CliPrintf (CliHandle, " packet %u", u4PktSize);
                }

                if (MEMCMP (TestPattern.pu1_OctetList,
                            &u4TestPattern, FM_LB_TEST_PATTERN_LENGTH) != 0)
                {
                    /* Displays the test pattern in hex string (eg: ae01f0c2) */
                    CliOctetToHexStr (TestPattern.pu1_OctetList,
                                      FM_LB_TEST_PATTERN_LENGTH,
                                      pu1DispPattern);
                    CliPrintf (CliHandle, " pattern %s", pu1DispPattern);
                }

                if (i4RetVal != FM_DEFAULT_TEST_WAIT_TIME)
                {
                    CliPrintf (CliHandle, " wait-time %d", i4RetVal);
                }

                CliPrintf (CliHandle, "\r\n");
            }
        }

    }

    if (nmhValidateIndexInstanceFsFmLinkEventTable (i4IfIndex) != SNMP_FAILURE)
    {
        nmhGetFsFmSymPeriodAction (i4IfIndex, &i4RetVal);

        if (i4RetVal == FM_ACTION_NONE)
        {
            if (FmCliSrcPrintPortName (CliHandle, i4IfIndex, b1FlgModSpec,
                                       &u1DispFlag) == CLI_SUCCESS)
            {
                CliPrintf (CliHandle,
                           " fault-management ethernet-oam link-monitor");
                CliPrintf (CliHandle, " symbol-period action none\r\n");
            }
        }

        nmhGetFsFmFrameAction (i4IfIndex, &i4RetVal);

        if (i4RetVal == FM_ACTION_NONE)
        {
            if (FmCliSrcPrintPortName (CliHandle, i4IfIndex, b1FlgModSpec,
                                       &u1DispFlag) == CLI_SUCCESS)
            {
                CliPrintf (CliHandle, " fault-management ethernet-oam");
                CliPrintf (CliHandle, " link-monitor frame action none\r\n");
            }
        }

        nmhGetFsFmFramePeriodAction (i4IfIndex, &i4RetVal);

        if (i4RetVal == FM_ACTION_NONE)
        {
            if (FmCliSrcPrintPortName (CliHandle, i4IfIndex, b1FlgModSpec,
                                       &u1DispFlag) == CLI_SUCCESS)
            {
                CliPrintf (CliHandle, " fault-management ethernet-oam link-");
                CliPrintf (CliHandle, "monitor frame-period action none\r\n");
            }
        }

        nmhGetFsFmFrameSecSummAction (i4IfIndex, &i4RetVal);

        if (i4RetVal == FM_ACTION_NONE)
        {
            if (FmCliSrcPrintPortName (CliHandle, i4IfIndex, b1FlgModSpec,
                                       &u1DispFlag) == CLI_SUCCESS)
            {
                CliPrintf (CliHandle,
                           " fault-management ethernet-oam link-monitor");
                CliPrintf (CliHandle, " frame-sec-summary action none\r\n");
            }
        }

        nmhGetFsFmCriticalEventAction (i4IfIndex, &i4RetVal);

        if (i4RetVal == FM_ACTION_NONE)
        {
            if (FmCliSrcPrintPortName (CliHandle, i4IfIndex, b1FlgModSpec,
                                       &u1DispFlag) == CLI_SUCCESS)
            {
                CliPrintf (CliHandle, " fault-management ethernet-oam");
                CliPrintf (CliHandle, " critical-event action none\r\n");
            }
        }
        else if (i4RetVal == FM_ACTION_BLOCK)
        {
            if (FmCliSrcPrintPortName (CliHandle, i4IfIndex, b1FlgModSpec,
                                       &u1DispFlag) == CLI_SUCCESS)
            {
                CliPrintf (CliHandle, " fault-management ethernet-oam");
                CliPrintf (CliHandle, " critical-event action block\r\n");
            }
        }

        nmhGetFsFmDyingGaspAction (i4IfIndex, &i4RetVal);

        if (i4RetVal == FM_ACTION_NONE)
        {
            if (FmCliSrcPrintPortName (CliHandle, i4IfIndex, b1FlgModSpec,
                                       &u1DispFlag) == CLI_SUCCESS)
            {
                CliPrintf (CliHandle, " fault-management ethernet-oam");
                CliPrintf (CliHandle, " dying-gasp action none\r\n");
            }
        }
        else if (i4RetVal == FM_ACTION_BLOCK)
        {
            if (FmCliSrcPrintPortName (CliHandle, i4IfIndex, b1FlgModSpec,
                                       &u1DispFlag) == CLI_SUCCESS)
            {
                CliPrintf (CliHandle, " fault-management ethernet-oam");
                CliPrintf (CliHandle, " dying-gasp action block\r\n");
            }
        }

        nmhGetFsFmLinkFaultAction (i4IfIndex, &i4RetVal);

        if (i4RetVal == FM_ACTION_NONE)
        {
            if (FmCliSrcPrintPortName (CliHandle, i4IfIndex, b1FlgModSpec,
                                       &u1DispFlag) == CLI_SUCCESS)
            {
                CliPrintf (CliHandle, " fault-management ethernet-oam");
                CliPrintf (CliHandle, " link-fault action none\r\n");
            }
        }
        else if (i4RetVal == FM_ACTION_BLOCK)
        {
            if (FmCliSrcPrintPortName (CliHandle, i4IfIndex, b1FlgModSpec,
                                       &u1DispFlag) == CLI_SUCCESS)
            {
                CliPrintf (CliHandle, " fault-management ethernet-oam");
                CliPrintf (CliHandle, " link-fault action block\r\n");
            }
        }

    }

    if (nmhValidateIndexInstanceFsFmVarRetrievalTable (i4IfIndex) !=
        SNMP_FAILURE)
    {
        nmhGetFsFmVarRetrievalMaxVar (i4IfIndex, &u4MaxVar);

        if (u4MaxVar != FM_DEFAULT_NUM_DESCRIPTORS)
        {
            if (FmCliSrcPrintPortName (CliHandle, i4IfIndex, b1FlgModSpec,
                                       &u1DispFlag) == CLI_SUCCESS)
            {
                CliPrintf (CliHandle, " fault-management ethernet-oam ");
                CliPrintf (CliHandle, "mib-variable count %u\r\n", u4MaxVar);
            }
        }

    }

    if ((b1FlgModSpec == OSIX_TRUE) && (u1DispFlag == OSIX_TRUE))
    {
        CliPrintf (CliHandle, "!\r\n");
    }

    FmUnLock ();
    CliUnRegisterLock (CliHandle);

    return;
}

/****************************************************************************
 *                                                                           
 *     FUNCTION         : FmCliSrcPrintPortName              
 *                                                                           
 *     DESCRIPTION      : This function displays the port name for the        
 *                        particular interface if it is not 
 *                        previously displayed
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                         
 *                        i4IfIndex - Port Number
 *                        b1FlgModSpec - Flag to specify module specific 
 *                        ShowRunningConfig or not
 *                        pu1DispFlag - Flag to specify whether the 
 *                        port name is already displayed or not
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            
 *                                                                           
 ****************************************************************************/
PUBLIC INT4
FmCliSrcPrintPortName (tCliHandle CliHandle, INT4 i4IfIndex, BOOL1 b1FlgModSpec,
                       UINT1 *pu1DispFlag)
{
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];

    MEMSET (au1NameStr, 0, CFA_MAX_PORT_NAME_LENGTH);

    if ((b1FlgModSpec == OSIX_TRUE) && (*(pu1DispFlag) == OSIX_FALSE))
    {
        if (CfaCliConfGetIfName
            ((UINT4) i4IfIndex, (INT1 *) au1NameStr) != CFA_SUCCESS)
        {
            FmUnLock ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;
        }

        CliPrintf (CliHandle, "interface %s\r\n", au1NameStr);
        *pu1DispFlag = OSIX_TRUE;
    }

    return CLI_SUCCESS;
}

#endif /* FMCLI_C */

/*--------------------------------------------------------------------------*/
/*                         End of the file emfmcli.c                          */
/*--------------------------------------------------------------------------*/
