/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emfmque.c,v 1.1.1.1 2008/06/03 14:31:04 iss Exp $
 *
 * Description: This file contains procedures related to 
 *              - Processing QMsgs
 *********************************************************************/
#include "emfminc.h"
/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : FmQueMsgHandler 
 *                                                                          
 *    DESCRIPTION      : This function process the queue messages received 
 *                       by FM task. 
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
FmQueMsgHandler (VOID)
{
    tFmQMsg            *pQMsg = NULL;

    FM_TRC_FN_ENTRY ();

    /* Event received, dequeue messages for processing */
    while (OsixQueRecv (gFmGlobalInfo.QId, (UINT1 *) &pQMsg,
                        OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
    {
        switch (pQMsg->u4MsgType)
        {
            case FM_EOAM_MSG:
                FM_TRC ((CONTROL_PLANE_TRC,
                         "FmQueMsgHandler: Received Event from EOAM\r\n"));
                FmFuncHandleEventFromEoam (pQMsg);
                break;
            default:
                FM_TRC ((CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         "FmQueMsgHandler:Unknown message type "
                         "received\r\n"));
                break;
        }

        /* Release the buffer to pool */
        MemReleaseMemBlock (gFmGlobalInfo.QMsgPoolId, (UINT1 *) pQMsg);
    }

    FM_TRC_FN_EXIT ();

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : FmQueEnqAppMsg 
 *                                                                          
 *    DESCRIPTION      : Function to post application message to FM task. 
 *
 *    INPUT            : pMsg - Pointer to msg to be posted 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
FmQueEnqAppMsg (tFmQMsg * pMsg)
{

    FM_TRC_FN_ENTRY ();

    if (OsixQueSend (gFmGlobalInfo.QId, (UINT1 *) &pMsg,
                     OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        FM_TRC ((ALL_FAILURE_TRC | FM_CRITICAL_TRC, "FmQueEnqAppMsg:"
                 "FM Enqueue Control Message FAILED\n"));

        if (MemReleaseMemBlock (gFmGlobalInfo.QMsgPoolId, (UINT1 *) pMsg)
            != MEM_SUCCESS)
        {

            FM_TRC ((BUFFER_TRC | ALL_FAILURE_TRC,
                     "MemReleaseMemBlock FAILED\n"));
        }
        return OSIX_FAILURE;
    }

    if (OsixEvtSend (gFmGlobalInfo.TaskId, FM_QMSG_EVENT) != OSIX_SUCCESS)
    {
        FM_TRC ((ALL_FAILURE_TRC | FM_CRITICAL_TRC,
                 "FmQueEnqAppMsg: SendEvent FAILED\n"));
        return OSIX_FAILURE;
    }

    FM_TRC_FN_EXIT ();

    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  emfmque.c                      */
/*-----------------------------------------------------------------------*/
