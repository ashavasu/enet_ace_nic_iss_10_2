
/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: eoamfmnpwr.c,v 1.2 2012/08/08 10:02:27 siva Exp $
 *
 * Description: This file contains the EOAMFM NPAPI related wrapper routines.
 *
 ***************************************************************************/

#ifndef _EOAM_FM_NP_WR_C_
#define _EOAM_FM_NP_WR_C_

#include "emfminc.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : EoamfmNpWrHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tEoamfmNpModInfo
 *                                                                          
 *    Input(s)            : FsHwNpParam of type tfsHwNp                     
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
EoamfmNpWrHwProgram (tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tEoamfmNpModInfo   *pEoamfmNpModInfo = NULL;

    if (NULL == pFsHwNp)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pEoamfmNpModInfo = &(pFsHwNp->EoamfmNpModInfo);

    if (NULL == pEoamfmNpModInfo)
    {
        return FNP_FAILURE;
    }

    switch (u4Opcode)
    {
        case FM_NP_REGISTER_FOR_FAILURE_INDICATIONS:
        {
            u1RetVal = FmNpRegisterForFailureIndications ();
            break;
        }
        case NP_FM_FAILURE_INDICATION_CALLBACK_FUNC:
        {
            tEoamfmNpWrNpFmFailureIndicationCallbackFunc *pEntry = NULL;
            pEntry =
                &pEoamfmNpModInfo->EoamfmNpNpFmFailureIndicationCallbackFunc;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                NpFmFailureIndicationCallbackFunc (pEntry->u4IfIndex,
                                                   pEntry->u1EventType,
                                                   pEntry->u1Status);
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

#endif /* _EOAM_FM_NP_WR_C_ */
