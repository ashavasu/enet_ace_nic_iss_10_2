/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emfmfunc.c,v 1.7 2014/05/16 13:28:25 siva Exp $
 * 
 * Description: This file contains all the functionality routines of Fault
 *              Management. 
 *****************************************************************************/
#ifndef FMFUNC_C
#define FMFUNC_C
#include "emfminc.h"

PRIVATE INT4 FmFuncFillTestData PROTO ((tFmPortEntry *, UINT1 *));
PRIVATE INT4 FmFuncSendOutTestPkt PROTO ((tFmPortEntry *, UINT1 *));
PRIVATE VOID FmFuncHandleLinkFault PROTO ((tFmPortEntry *, UINT1));
PRIVATE VOID FmFuncHandleDyingGasp PROTO ((tFmPortEntry *, UINT1));
PRIVATE VOID FmFuncHandleCriticalEvt PROTO ((tFmPortEntry *, UINT1));
PRIVATE VOID FmFuncHandleVarResp PROTO ((tFmPortEntry *, tEoamExtData *));
PRIVATE VOID FmFuncHandleLBAck PROTO ((tFmPortEntry *, UINT1));

/* Counter to hold the number of LB test pkts transmitted & matched for 
 * every loopback test data generation */
PRIVATE UINT4       gu4LBTestTxCnt = 0;
PRIVATE UINT4       gu4LBTestMatchCnt = 0;

/***************************************************************************
 *     FUNCTION NAME    :  FmFuncPerformLoopbackTest  
 *                                                                           
 *     DESCRIPTION      :  This function starts the loopback Test and sends 
 *                         out test packets based on the configuration.  
 *                                                                           
 *     INPUT            :  pPortEntry - pointer to the port entry 
 *                                                                           
 *     OUTPUT           :  None.                                             
 *                                                                           
 *     RETURNS          :  OSIX_SUCCESS / OSIX_FAILURE                       
 ****************************************************************************/
PUBLIC INT4
FmFuncPerformLoopbackTest (tFmPortEntry * pPortEntry)
{
    UINT4               u4Duration = 0;
    UINT1              *pu1DataBuf = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    FM_TRC_FN_ENTRY ();

    MEMSET (gau1LinearBuf, 0, FM_MAX_TEST_PKT_SIZE);
    pu1DataBuf = gau1LinearBuf;

    if (FmFuncFillTestData (pPortEntry, pu1DataBuf) != OSIX_SUCCESS)
    {
        FM_TRC ((FM_LOOPBACK_TRC | ALL_FAILURE_TRC,
                 "FM : Filling test data to be sent failed"));
        return OSIX_FAILURE;
    }

    /* Change the TEST status as IN_PROGRESS */
    pPortEntry->LBTestInfo.u1TestStatus = FM_IN_PROGRESS;
    if (FmFuncSendOutTestPkt (pPortEntry, pu1DataBuf) != OSIX_SUCCESS)
    {
        FM_TRC ((FM_LOOPBACK_TRC | ALL_FAILURE_TRC,
                 "FM : Transmitting test data Failed"));
        return OSIX_FAILURE;
    }
    /* Start a timer for the configured duration. */
    u4Duration = pPortEntry->LBTestInfo.u4TestWaitTime;
    if (TmrStart (gFmGlobalInfo.FmTmrListId, &(pPortEntry->LBTestAppTmr),
                  FM_LB_TEST_TMR_TYPE, u4Duration, 0) != TMR_SUCCESS)
    {
        FM_TRC ((FM_LOOPBACK_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                 "TMR: Starting Timer FAILED\n"));
        return OSIX_FAILURE;
    }
    pPortEntry->FmLBTmrFlag = FM_LB_TMR_RUNNING;
    /* Further configurations will not be allowed till all transmitted packets 
     * have been received or the expiry of the timer, whichever is earliest.
     * So entering a while loop and waiting for either of the two to happen 
     * thereby changing the Test Status as COMPLETED */
    while (1)
    {
        /* Lock has been already taken in CLI thread. Unlock here to allow 
         * other threads to schedule here. */
        FmUnLock ();

        /* To allow the Rx thread to schedule, Delay task is 
         * introduced. If delay task is not present, Tx thread keeps on 
         * running and it does not allow Rx thread to schedule */
        OsixTskDelay (1);

        /* Lock again since CLI thread has already taken the lock. */
        FmLock ();

        /* Check if the port entry deleted in-between */
        if (pPortEntry->FmValidEntry == FM_INVALID_ENTRY)
        {

            FM_TRC ((FM_LOOPBACK_TRC, "FM : Invalid Port Entry"));
            i4RetVal = OSIX_FAILURE;
            break;
        }
        else if (pPortEntry->LBTestInfo.u1TestStatus == FM_COMPLETED)
        {
            break;
        }

    }
    FM_TRC_FN_EXIT ();
    return i4RetVal;
}

/***************************************************************************
 *     FUNCTION NAME    :  FmFuncHandleIncomingPacket 
 *                                                                           
 *     DESCRIPTION      :  This function matches the incoming packet with the
 *                         transmitted packet and incrments the appropriate
 *                         counters.
 *                                                                           
 *     INPUT            :  pBuf - pointer to the CRU buffer received by the 
 *                                Packet reception Table
 *                         u4PortIndex - Interface Index 
 *                                                                           
 *     OUTPUT           :  None.                                             
 *                                                                           
 *     RETURNS          :  None. 
 ****************************************************************************/
PUBLIC VOID
FmFuncHandleIncomingPacket (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4PortIndex)
{
    tFmPortEntry       *pPortEntry = NULL;
    UINT4               u4ByteCount = 0;
    UINT1              *pu1FirstValidByte = NULL;

    FM_TRC_FN_ENTRY ();

    if (gFmGlobalInfo.u1ModuleStatus != FM_ENABLED)
    {
        FM_TRC ((MGMT_TRC | ALL_FAILURE_TRC, " FM Module Disabled\n"));
        return;
    }

    pPortEntry = &(gFmGlobalInfo.aFmPortEntry[u4PortIndex - 1]);

    /* Increment the received packet count */
    pPortEntry->LBTestInfo.CurrSessionStats.u4RxPktCount++;
    u4ByteCount = CRU_BUF_Get_ChainValidByteCount (pBuf);

    /* Check if the length of the received packet is equal to that of the 
     * transmitted packet. If not, no further matching is required */

    /*If the received frame includes FCS, the data packet length should 
       be obtained by subtracing 4 bytes (FCS length). This needs to 
       be taken care based on the target environment.The macro 
       FM_FCS_SIZE needs to be modified accordingly. */

    if ((u4ByteCount - FM_FCS_SIZE) == pPortEntry->LBTestInfo.u4TestPktSize)
    {
        pu1FirstValidByte =
            CRU_BUF_GetDataPtr (CRU_BUF_GetFirstDataDesc (pBuf));

        if (MEMCMP (gau1LinearBuf, pu1FirstValidByte,
                    pPortEntry->LBTestInfo.u4TestPktSize) == 0)
        {
            pPortEntry->LBTestInfo.CurrSessionStats.u4MatchPktCount++;
            gu4LBTestMatchCnt++;

            /* Check for the following:
             * 1. if all the Loopback test packets are transmitted and 
             * 2. if all Txed LB test packets are matched */
            if ((pPortEntry->LBTestInfo.u4TestPktCount == gu4LBTestTxCnt) &&
                (gu4LBTestTxCnt == gu4LBTestMatchCnt))
            {
                if (pPortEntry->FmLBTmrFlag == FM_LB_TMR_RUNNING)
                {
                    TmrStop (gFmGlobalInfo.FmTmrListId,
                             &(pPortEntry->LBTestAppTmr));
                    pPortEntry->FmLBTmrFlag = FM_LB_TMR_NOT_RUNNING;
                }
                pPortEntry->LBTestInfo.u1TestStatus = FM_COMPLETED;
            }
        }
    }

    FM_PKT_DUMP (DUMP_TRC, pBuf, u4ByteCount,
                 "FmHandleIncomingPacket:"
                 "Dumping frame received from lower layer...\n");
    FM_TRC_FN_EXIT ();
    return;
}

/***************************************************************************
 *     FUNCTION NAME    :  FmFuncHandleEventFromEoam 
 *                                                                           
 *     DESCRIPTION      :  This function performs appropriate actions based on 
 *                         the type of event. The actions may include 
 *                         1) Logging messages to SYSLOG
 *                         2) Storing variable Responses
 *                         3) Update Loopback Status
 *                                                                           
 *     INPUT            :  *pMsg - pointer to Queue Message
 *                                                                           
 *     OUTPUT           :  None.                                             
 *                                                                           
 *     RETURNS          :  None. 
 ****************************************************************************/
PUBLIC INT4
FmFuncHandleEventFromEoam (tFmQMsg * pMsg)
{
    tFmPortEntry       *pPortEntry = NULL;

    FM_TRC_FN_ENTRY ();

    if (gFmGlobalInfo.u1ModuleStatus != FM_ENABLED)
    {
        FM_TRC ((MGMT_TRC | ALL_FAILURE_TRC, " FM Module Disabled\n"));
        return OSIX_FAILURE;
    }

    FSAP_ASSERT (pMsg != NULL);
    pPortEntry = &gFmGlobalInfo.aFmPortEntry[pMsg->u4Port - 1];
    if ((pMsg->u4EventType != EOAM_NOTIFY_PORT_CREATE) &&
        (pPortEntry->FmValidEntry == FM_INVALID_ENTRY))
    {
        return OSIX_FAILURE;
    }

    switch (pMsg->u4EventType)
    {
            /* Log Messages to SYSLOG only if action is configured as WARNING */
        case EOAM_NOTIFY_ERRORED_SYMBOL:
            if (pPortEntry->EventInfo.SymPeriodAction == FM_ACTION_WARNING)
            {
                if (pMsg->u2Location == EOAM_LOCAL_ENTRY)
                {
                    FM_TRC ((FM_EVENTTRIG_TRC,
                             "EOAM_ERRORED_SYMBOL_EVENT occurred"
                             " at local end for port %u", pMsg->u4Port));
                    SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gFmGlobalInfo.u4SysLogId,
                                  "EOAM_ERRORED_SYMBOL_EVENT occurred at local"
                                  " end for port %u", pMsg->u4Port));
                }
                else
                {
                    FM_TRC ((FM_EVENTRX_TRC,
                             "EOAM_ERRORED_SYMBOL_EVENT occurred"
                             " at remote end for port"));
                    SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gFmGlobalInfo.u4SysLogId,
                                  "EOAM_ERRORED_SYMBOL_EVENT occurred at remote"
                                  " end for port %u", pMsg->u4Port));
                }
            }
            break;
        case EOAM_NOTIFY_ERRORED_FRAME:
            if (pPortEntry->EventInfo.FrameAction == FM_ACTION_WARNING)
            {
                if (pMsg->u2Location == EOAM_LOCAL_ENTRY)
                {
                    FM_TRC ((FM_EVENTTRIG_TRC,
                             "EOAM_ERRORED_FRAME_EVENT occurred"
                             " at local end for port %u", pMsg->u4Port));
                    SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gFmGlobalInfo.u4SysLogId,
                                  "EOAM_ERRORED_FRAME_EVENT occurred at local "
                                  "end for port %u", pMsg->u4Port));
                }
                else
                {
                    FM_TRC ((FM_EVENTRX_TRC,
                             "EOAM_ERRORED_FRAME_EVENT occurred "
                             " at remote end for port %u", pMsg->u4Port));
                    SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gFmGlobalInfo.u4SysLogId,
                                  "EOAM_ERRORED_FRAME_EVENT occurred at remote "
                                  "end for port %u", pMsg->u4Port));
                }
            }
            break;
        case EOAM_NOTIFY_ERRORED_FRAME_PERIOD:
            if (pPortEntry->EventInfo.FramePeriodAction == FM_ACTION_WARNING)
            {
                if (pMsg->u2Location == EOAM_LOCAL_ENTRY)
                {
                    FM_TRC ((FM_EVENTTRIG_TRC, "EOAM_ERRORED_FRAME_PERIOD_EVENT"
                             " occurred at local end for port %u",
                             pMsg->u4Port));
                    SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gFmGlobalInfo.u4SysLogId,
                                  "EOAM_ERRORED_FRAME_PERIOD_EVENT occurred at"
                                  " local end for port %u", pMsg->u4Port));
                }
                else
                {
                    FM_TRC ((FM_EVENTRX_TRC, "EOAM_ERRORED_FRAME_PERIOD_EVENT"
                             " occurred at remote end for port %u",
                             pMsg->u4Port));
                    SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gFmGlobalInfo.u4SysLogId,
                                  "EOAM_ERRORED_FRAME_PERIOD_EVENT occurred at"
                                  " remote end for port %u", pMsg->u4Port));
                }
            }
            break;
        case EOAM_NOTIFY_ERRORED_FRAME_SECONDS:
            if (pPortEntry->EventInfo.FrameSecSummAction == FM_ACTION_WARNING)
            {
                if (pMsg->u2Location == EOAM_LOCAL_ENTRY)
                {
                    FM_TRC ((FM_EVENTTRIG_TRC,
                             "EOAM_ERRORED_FRAME_SECONDS_EVENT"
                             " occurred at local end for port %u",
                             pMsg->u4Port));
                    SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gFmGlobalInfo.u4SysLogId,
                                  "EOAM_ERRORED_FRAME_SECONDS_EVENT occurred at"
                                  " local end for port %u", pMsg->u4Port));
                }
                else
                {
                    FM_TRC ((FM_EVENTRX_TRC, "EOAM_ERRORED_FRAME_SECONDS_EVENT"
                             " occurred at remote end for port %u",
                             pMsg->u4Port));
                    SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gFmGlobalInfo.u4SysLogId,
                                  "EOAM_ERRORED_FRAME_SECONDS_EVENT occurred at"
                                  " remote end for port %u", pMsg->u4Port));
                }
            }
            break;
        case EOAM_NOTIFY_ORG_SPECIFIC_EVENT:
            if (pMsg->u2Location == EOAM_LOCAL_ENTRY)
            {
                FM_TRC ((FM_EVENTTRIG_TRC, "EOAM_ORG_SPECIFIC_LINK_EVENT "
                         "occurred at local end for port %u", pMsg->u4Port));
                SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gFmGlobalInfo.u4SysLogId,
                              "EOAM_ORG_SPECIFIC_LINK_EVENT occurred at"
                              " local end for port %u", pMsg->u4Port));
            }
            else
            {
                FM_TRC ((FM_EVENTRX_TRC, "EOAM_ORG_SPECIFIC_LINK_EVENT "
                         " occurred at remote end for port %u", pMsg->u4Port));
                SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gFmGlobalInfo.u4SysLogId,
                              "EOAM_ORG_SPECIFIC_LINK_EVENT occurred at"
                              " remote end for port %u", pMsg->u4Port));
            }
            FM_EXT_BUDDY_FREE (pMsg->FmExtData.pu1Data);
            break;
        case EOAM_NOTIFY_LINK_FAULT:
            FmFuncHandleLinkFault (pPortEntry, pMsg->FmEventState);
            break;
        case EOAM_NOTIFY_DYING_GASP:
            FmFuncHandleDyingGasp (pPortEntry, pMsg->FmEventState);
            break;
        case EOAM_NOTIFY_CRITICAL_EVENT:
            FmFuncHandleCriticalEvt (pPortEntry, pMsg->FmEventState);
            break;
        case EOAM_NOTIFY_VAR_RESP_RCVD:
            FmFuncHandleVarResp (pPortEntry, &(pMsg->FmExtData));
            FM_EXT_BUDDY_FREE (pMsg->FmExtData.pu1Data);
            break;
        case EOAM_NOTIFY_LB_ACK_RCVD:
            FmFuncHandleLBAck (pPortEntry, pMsg->FmEventState);
            break;
        case EOAM_NOTIFY_LB_ACK_NOT_RCVD:
            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gFmGlobalInfo.u4SysLogId,
                          "Loopback acknowledgement not received from peer"
                          " till timer expiry for port %u", pMsg->u4Port));
            break;
        case EOAM_NOTIFY_LB_TESTDATA_RCVD:
            FmFuncHandleIncomingPacket (pMsg->FmLBTestData, pPortEntry->u4Port);
            if (CRU_BUF_Release_MsgBufChain (pMsg->FmLBTestData,
                                             OSIX_FALSE) != CRU_SUCCESS)
            {
                FM_TRC ((BUFFER_TRC | ALL_FAILURE_TRC | FM_LOOPBACK_TRC,
                         "Loopback test data CRU Buffer Release Failed\n"));
            }
            break;
        case EOAM_NOTIFY_ORG_INFO_RCVD:
            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gFmGlobalInfo.u4SysLogId,
                          "EOAM_ORG_SPEC_PDU received on port %u",
                          pMsg->u4Port));
            FM_EXT_BUDDY_FREE (pMsg->FmExtData.pu1Data);
            break;
        case EOAM_NOTIFY_PORT_CREATE:
            FmIfCreate (pMsg->u4Port);
            break;
        case EOAM_NOTIFY_PORT_DELETE:
            FmIfDelete (pMsg->u4Port);
            break;
        default:
            FM_TRC ((ALL_FAILURE_TRC, "Returned from default case of"
                     "FmHandleCallbackFromEoam\n"));
    }
    FM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/***************************************************************************
 *     FUNCTION NAME    :  FmFuncFillTestData 
 *                                                                           
 *     DESCRIPTION      :  This function fills the buffer with the test data
 *                         pattern that is to be sent.
 *                                                                           
 *     INPUT            :  pPortEntry - pointer to port info structure 
 *     
 *     OUTPUT           :  pu1DataBuf - pointer to the buffer which is to be 
 *                         filled
 *                                                                           
 *     RETURNS          :  OSIX_SUCCESS / OSIX_FAILURE. 
 ****************************************************************************/
PRIVATE INT4
FmFuncFillTestData (tFmPortEntry * pPortEntry, UINT1 *pu1DataBuf)
{
    tMacAddr            ZeroMacAddr;
    tCfaIfInfo          IfInfo;
    UINT4               u4BufLength = 0;
    UINT4               u4DataLength = 0;

    FM_TRC_FN_ENTRY ();

    u4DataLength = (pPortEntry->LBTestInfo.u4TestPktSize -
                    FM_SRC_DEST_MAC_SIZE);
    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));
    MEMSET (&ZeroMacAddr, 0, sizeof (tMacAddr));
    /* Get the Destination MAC address from EOAM */
    EoamApiGetPeerMac (pPortEntry->u4Port, pu1DataBuf);
    /* Check the validity of the returned MAC address */
    if (MEMCMP (pu1DataBuf, ZeroMacAddr, MAC_ADDR_LEN) == 0)
    {
        FM_TRC ((FM_LOOPBACK_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                 "FM Loopback Test: Port %u :: EoamApiGetPeerMac"
                 "returned zero MAC\n", pPortEntry->u4Port));
        return OSIX_FAILURE;
    }

    pu1DataBuf += MAC_ADDR_LEN;
    /* Get the Source MAC address from CFA */
    if (CfaGetIfInfo ((UINT2) pPortEntry->u4Port, &IfInfo) != CFA_SUCCESS)
    {
        FM_TRC ((FM_LOOPBACK_TRC, "FM : Get source MAC address Failed"));
        return OSIX_FAILURE;
    }
    MEMCPY (pu1DataBuf, IfInfo.au1MacAddr, MAC_ADDR_LEN);
    pu1DataBuf += MAC_ADDR_LEN;
    do
    {
        /* Copy the data paatern recursively upto the configured length */
        FM_PUT_4BYTE (pu1DataBuf, pPortEntry->LBTestInfo.au1TestPattern);
        u4BufLength += FM_LB_TEST_PATTERN_LENGTH;

    }
    while (u4BufLength <= u4DataLength);
    FM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/***************************************************************************
 *     FUNCTION NAME    :  FmFuncSendOutTestPkt 
 *                                                                           
 *     DESCRIPTION      :  This function transmits out the test data 
 *                         continuously based on the configuration.
 *                                                                           
 *     INPUT            :  pPortEntry - pointer to port info structure 
 *                         pu1DataBuf - pointer to the data which should be
 *                                      copied and sent out
 *                                                                           
 *     OUTPUT           :  None.                                             
 *                                                                           
 *     RETURNS          :  OSIX_SUCCESS / OSIX_FAILURE. 
 ****************************************************************************/
PRIVATE INT4
FmFuncSendOutTestPkt (tFmPortEntry * pPortEntry, UINT1 *pu1DataBuf)
{
    tCRU_BUF_CHAIN_HEADER *pMsgBuf = NULL;
    UINT4               u4Count = 0;

    FM_TRC_FN_ENTRY ();

    /* Init unique LB test pkt tx & match count, b4 initiating test data 
     * generation */
    gu4LBTestTxCnt = 0;
    gu4LBTestMatchCnt = 0;

    for (u4Count = 0; u4Count < pPortEntry->LBTestInfo.u4TestPktCount;
         u4Count++)
    {
        if ((pMsgBuf = CRU_BUF_Allocate_MsgBufChain
             (pPortEntry->LBTestInfo.u4TestPktSize, 0)) == NULL)
        {
            FM_TRC ((FM_LOOPBACK_TRC | CONTROL_PLANE_TRC |
                     BUFFER_TRC | ALL_FAILURE_TRC,
                     "FM Loopback Test: Port %u :: Buffer Allocation"
                     "failed\n", pPortEntry->u4Port));
            pPortEntry->LBTestInfo.u1TestStatus = FM_NOT_INITIATED;
            return OSIX_FAILURE;
        }

        if (CRU_BUF_Copy_OverBufChain (pMsgBuf, pu1DataBuf, 0,
                                       pPortEntry->LBTestInfo.u4TestPktSize)
            == CRU_FAILURE)
        {

            FM_TRC ((FM_LOOPBACK_TRC | CONTROL_PLANE_TRC |
                     BUFFER_TRC | ALL_FAILURE_TRC,
                     "FM Loopback Test: Port %u :: Copying into Buffer"
                     "failed\n", pPortEntry->u4Port));
            pPortEntry->LBTestInfo.u1TestStatus = FM_NOT_INITIATED;
            CRU_BUF_Release_MsgBufChain (pMsgBuf, FALSE);
            return OSIX_FAILURE;
        }

        if (CfaEnetTxLoopbackTestFrame (pPortEntry->u4Port, pMsgBuf,
                                        pPortEntry->LBTestInfo.u4TestPktSize) !=
            CFA_SUCCESS)
        {
            FM_TRC ((FM_LOOPBACK_TRC | CONTROL_PLANE_TRC |
                     BUFFER_TRC | ALL_FAILURE_TRC,
                     "FM Loopback Test: Port %u :: CfaPostPktFromL2"
                     "failed \n", pPortEntry->u4Port));
            pPortEntry->LBTestInfo.u1TestStatus = FM_NOT_INITIATED;
            return OSIX_FAILURE;
        }

        /* Incr cumulative LB test pkt count txed for each LB session */
        pPortEntry->LBTestInfo.CurrSessionStats.u4TxPktCount++;

        /* Incr unique LB test pkt count txed for each trigger */
        gu4LBTestTxCnt++;

        OsixTskDelay (1);

        pMsgBuf = NULL;

#ifdef OS_TMO
        /* When the code is executed using Network Simulator Env. the process
         * of transmitting loopback test frames at local end and receving 
         * loopback frames at remote end happens in the same machine. Hence,
         * continous Tx in this loop will not allow the CPU to Rx the Txmd 
         * frames. To allow the reception thread to get scheduled, DelayTask
         * is called. */
        OsixTskDelay (1);
#endif /* OS_TMO */
    }
    FM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*******************************************************************************
 *     FUNCTION NAME    :  FmFuncHandleLinkFault
 *                                                                           
 *     DESCRIPTION      :  This function performs warning/log/block/none 
 *                    based on the event actions.
 *                                                                           
 *     INPUT            :  pPortEntry - pointer to port info structure 
 *                         u1State - State denoting whether the event has 
 *                                   occurred or has been rectified
 *                                                                           
 *     OUTPUT           :  None.                                             
 *                                                                           
 *     RETURNS          :  None. 
 ******************************************************************************/
PRIVATE VOID
FmFuncHandleLinkFault (tFmPortEntry * pPortEntry, UINT1 u1State)
{
    FM_TRC_FN_ENTRY ();

    if ((pPortEntry->EventInfo.LinkFaultAction == FM_ACTION_WARNING) ||
        (pPortEntry->EventInfo.LinkFaultAction == FM_ACTION_BLOCK))
    {
        if ((u1State == OSIX_TRUE) &&
            (!(pPortEntry->u1EventFlag & EOAM_LINK_FAULT_BITMASK)))
        {
            FM_TRC ((FM_EVENTRX_TRC, "EOAM_LINK_FAULT occurred at remote end on"
                     "port %u", pPortEntry->u4Port));

            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gFmGlobalInfo.u4SysLogId,
                          "EOAM_LINK_FAULT occurred at remote "
                          "end on port %u", pPortEntry->u4Port));
            pPortEntry->u1EventFlag |= EOAM_LINK_FAULT_BITMASK;
        }
        else if ((u1State == OSIX_FALSE) &&
                 (pPortEntry->u1EventFlag & EOAM_LINK_FAULT_BITMASK))
        {
            FM_TRC ((FM_EVENTRX_TRC, "EOAM_LINK_FAULT rectified notification"
                     "received form  remote end on"
                     "port %u", pPortEntry->u4Port));
            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gFmGlobalInfo.u4SysLogId,
                          "EOAM_LINK_FAULT rectified notification "
                          "received from remote end on port %u",
                          pPortEntry->u4Port));
            pPortEntry->u1EventFlag &= (~EOAM_LINK_FAULT_BITMASK);
        }
    }

#ifdef NPAPI_WANTED

    if (pPortEntry->EventInfo.LinkFaultAction == FM_ACTION_BLOCK)
    {
        /*Enables block action to drop all data traffic */
        if (EoamNpHandleLocalLoopBack
            (pPortEntry->u4Port, NULL, NULL, EOAM_BLOCK_ENABLE) != FNP_SUCCESS)
        {
            FM_TRC ((ALL_FAILURE_TRC,
                     "Link Fault Block Action Failed on"
                     "port %u\n ", pPortEntry->u4Port));
            FM_TRC_FN_EXIT ();
            return;
        }
        FM_TRC ((EOAM_RFI_TRC, "This action blocks all data traffic"
                 "and Allows OAM PDUs"));
    }
    else if (!((pPortEntry->EventInfo.DyingGaspAction == FM_ACTION_BLOCK) ||
               (pPortEntry->EventInfo.CriticalEventAction == FM_ACTION_BLOCK)))
    {
        /*Disables block action and allow all data traffic */
        if (EoamNpHandleLocalLoopBack
            (pPortEntry->u4Port, NULL, NULL, EOAM_BLOCK_DISABLE) != FNP_SUCCESS)
        {
            FM_TRC ((ALL_FAILURE_TRC,
                     "Default Link Fault Action Failed on"
                     "port %u\n ", pPortEntry->u4Port));
            FM_TRC_FN_EXIT ();
            return;
        }
        FM_TRC ((EOAM_RFI_TRC, "This action unblocks the data traffic"));
    }

#endif

    FM_TRC_FN_EXIT ();
    return;
}

/*******************************************************************************
 *     FUNCTION NAME    :  FmFuncHandleDyingGasp
 *                                                                           
 *     DESCRIPTION      :  This function performs warning/log/block/none
 *                         based on the event actions.
 *                                                                           
 *     INPUT            :  pPortEntry - pointer to port info structure 
 *                         u1State - State denoting whether the event has 
 *                                   occurred or has been rectified
 *                                                                           
 *     OUTPUT           :  None.                                             
 *                                                                           
 *     RETURNS          :  None. 
 ******************************************************************************/
PRIVATE VOID
FmFuncHandleDyingGasp (tFmPortEntry * pPortEntry, UINT1 u1State)
{
    FM_TRC_FN_ENTRY ();

    if ((pPortEntry->EventInfo.DyingGaspAction == FM_ACTION_WARNING) ||
        (pPortEntry->EventInfo.DyingGaspAction == FM_ACTION_BLOCK))
    {
        if ((u1State == OSIX_TRUE) &&
            (!(pPortEntry->u1EventFlag & EOAM_DYING_GASP_BITMASK)))
        {
            FM_TRC ((FM_EVENTRX_TRC, "EOAM_DYING_GASP occurred at remote end on"
                     "port %u", pPortEntry->u4Port));
            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gFmGlobalInfo.u4SysLogId,
                          "EOAM_DYING_GASP from remote end received "
                          "through port %u", pPortEntry->u4Port));
            pPortEntry->u1EventFlag |= EOAM_DYING_GASP_BITMASK;
        }
        else if ((u1State == OSIX_FALSE) &&
                 (pPortEntry->u1EventFlag & EOAM_DYING_GASP_BITMASK))
        {
            FM_TRC ((FM_EVENTRX_TRC, "EOAM_DYING_GASP rectified notification"
                     "received form  remote end on"
                     "port %u", pPortEntry->u4Port));

            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gFmGlobalInfo.u4SysLogId,
                          "EOAM_DYING_GASP rectified notification from"
                          " remote end received through port %u",
                          pPortEntry->u4Port));
            pPortEntry->u1EventFlag &= (~EOAM_DYING_GASP_BITMASK);
        }
    }

#ifdef NPAPI_WANTED

    if (pPortEntry->EventInfo.DyingGaspAction == FM_ACTION_BLOCK)
    {
        /*Enables block action to drop all data traffic */
        if (EoamNpHandleLocalLoopBack
            (pPortEntry->u4Port, NULL, NULL, EOAM_BLOCK_ENABLE) != FNP_SUCCESS)

        {
            FM_TRC ((ALL_FAILURE_TRC,
                     "Dying Gasp Block Action Failed on"
                     "port %u\n ", pPortEntry->u4Port));
            return;
        }
        FM_TRC ((EOAM_RFI_TRC, "This action block the data traffic"
                 "and Allows OAM PDUs"));
        return;

    }
    else if (!((pPortEntry->EventInfo.LinkFaultAction == FM_ACTION_BLOCK) ||
               (pPortEntry->EventInfo.CriticalEventAction == FM_ACTION_BLOCK)))
    {

        /*Disables block action and allow all data traffic */
        if (EoamNpHandleLocalLoopBack
            (pPortEntry->u4Port, NULL, NULL, EOAM_BLOCK_DISABLE) != FNP_SUCCESS)
        {
            FM_TRC ((ALL_FAILURE_TRC | EOAM_LOOPBACK_TRC,
                     "Default Dying Gasp  Action Failed on"
                     "port %u\n ", pPortEntry->u4Port));
            FM_TRC_FN_EXIT ();
            return;
        }
        FM_TRC ((EOAM_RFI_TRC, "This action unblocks the data traffic"));
    }

#endif

    FM_TRC_FN_EXIT ();
    return;
}

/*******************************************************************************
 *     FUNCTION NAME    :  FmFuncHandleCriticalEvt
 *                                                                           
 *     DESCRIPTION      :  This function performs warning/log/block/none
 *                         based on the event actions.
 *                                                                           
 *     INPUT            :  pPortEntry - pointer to port info structure 
 *                         u1State - State denoting whether the event has 
 *                                   occurred or has been rectified
 *                                                                           
 *     OUTPUT           :  None.                                             
 *                                                                           
 *     RETURNS          :  None. 
 ******************************************************************************/
PRIVATE VOID
FmFuncHandleCriticalEvt (tFmPortEntry * pPortEntry, UINT1 u1State)
{
    FM_TRC_FN_ENTRY ();

    if ((pPortEntry->EventInfo.CriticalEventAction == FM_ACTION_WARNING) ||
        (pPortEntry->EventInfo.CriticalEventAction == FM_ACTION_BLOCK))
    {
        if ((u1State == OSIX_TRUE) &&
            (!(pPortEntry->u1EventFlag & EOAM_CRITICAL_EVENT_BITMASK)))
        {
            FM_TRC ((FM_EVENTRX_TRC,
                     "EOAM_CRITICAL_EVENT occurred at remote end" "on port %u",
                     pPortEntry->u4Port));
            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gFmGlobalInfo.u4SysLogId,
                          "EOAM_CRITICAL_EVENT from remote end received"
                          "through port %u", pPortEntry->u4Port));
            pPortEntry->u1EventFlag |= EOAM_CRITICAL_EVENT_BITMASK;
        }
        else if ((u1State == OSIX_FALSE) &&
                 (pPortEntry->u1EventFlag & EOAM_CRITICAL_EVENT_BITMASK))
        {
            FM_TRC ((FM_EVENTRX_TRC,
                     "EOAM_CRITICAL_EVENT rectified notification"
                     "received form  remote end on" "port %u",
                     pPortEntry->u4Port));
            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gFmGlobalInfo.u4SysLogId,
                          "EOAM_CRITICAL_EVENT rectified notification "
                          "from remote end received through port %u",
                          pPortEntry->u4Port));
            pPortEntry->u1EventFlag &= (~EOAM_CRITICAL_EVENT_BITMASK);
        }
    }

#ifdef NPAPI_WANTED

    if (pPortEntry->EventInfo.CriticalEventAction == FM_ACTION_BLOCK)
    {
        /*Enables block action to drop all data traffic */
        if (EoamNpHandleLocalLoopBack
            (pPortEntry->u4Port, NULL, NULL, EOAM_BLOCK_ENABLE) != FNP_SUCCESS)
        {
            FM_TRC ((ALL_FAILURE_TRC,
                     "Critical Event Block Action failed on"
                     "port %u\n ", pPortEntry->u4Port));
            FM_TRC_FN_EXIT ();
            return;
        }
        FM_TRC ((EOAM_RFI_TRC, "This action blocks the data traffic"
                 "and Allows OAM PDUs"));
    }
    else if (!((pPortEntry->EventInfo.DyingGaspAction == FM_ACTION_BLOCK) ||
               (pPortEntry->EventInfo.LinkFaultAction == FM_ACTION_BLOCK)))

    {
        /*Disables block action and allow all data traffic */
        if (EoamNpHandleLocalLoopBack
            (pPortEntry->u4Port, NULL, NULL, EOAM_BLOCK_DISABLE) != FNP_SUCCESS)
        {
            FM_TRC ((ALL_FAILURE_TRC | EOAM_LOOPBACK_TRC,
                     "Default Critical Event Action failed on"
                     "port %u\n ", pPortEntry->u4Port));
            FM_TRC_FN_EXIT ();
            return;
        }
        FM_TRC ((EOAM_RFI_TRC, "This action unblocks the data traffic"));
    }

#endif
    FM_TRC_FN_EXIT ();
    return;
}

/***************************************************************************
 *     FUNCTION NAME    :  FmFuncHandleVarResp
 *                                                                           
 *     DESCRIPTION      :  This function stores the received variable response 
 *                                                                           
 *     INPUT            :  pPortEntry - pointer to port info structure 
 *                         pRcvdVarResp - pointer to the received variable 
 *                                        response
 *                                                                           
 *     OUTPUT           :  None.                                             
 *                                                                           
 *     RETURNS          :  None. 
 ****************************************************************************/
PRIVATE VOID
FmFuncHandleVarResp (tFmPortEntry * pPortEntry, tEoamExtData * pRcvdVarResp)
{
    UINT4               u4DataLength = 0;
    UINT1              *pu1VarResp = NULL;

    FM_TRC_FN_ENTRY ();
    pu1VarResp = pPortEntry->VarResp[pPortEntry->u2Position].pu1Data;
    u4DataLength = pRcvdVarResp->u4DataLen;
    if (pu1VarResp != NULL)
    {
        FM_RESP_BUDDY_FREE (pu1VarResp);
        pu1VarResp = NULL;
    }
    pPortEntry->au2VarRespChunks[pPortEntry->u2Position] = 0;
    pu1VarResp = FM_RESP_BUDDY_ALLOC (u4DataLength);
    if (pu1VarResp == NULL)
    {
        FM_TRC ((FM_VAR_REQRESP_TRC | CONTROL_PLANE_TRC | BUFFER_TRC |
                 ALL_FAILURE_TRC, "Port %u: No Memory for"
                 "storing Variable Response\n", pPortEntry->u4Port));
        return;
    }
    pPortEntry->VarResp[pPortEntry->u2Position].pu1Data = pu1VarResp;
    MEMSET (pu1VarResp, 0, u4DataLength);
    MEMCPY (pu1VarResp, pRcvdVarResp->pu1Data, u4DataLength);
    pPortEntry->VarResp[pPortEntry->u2Position].u4DataLen = u4DataLength;
    /* TODO - Call fn. from CMIP to get datatype and convert it
     * from NTOH order */

    /* SNMP cannot display OCTET_STRING of length more than 256.
     * So if the received variable response is more than 256,
     * the number of octet_strings required to display the
     * complete response is calculated */
    FM_CALCULATE_VAR_RESP_CHUNKS
        (u4DataLength, pPortEntry->au2VarRespChunks[pPortEntry->u2Position]);
    pPortEntry->u2Position++;
    pPortEntry->u2Position =
        (UINT2) (pPortEntry->u2Position % FM_MAX_RESPONSES);
    FM_TRC_FN_EXIT ();
    return;
}

/***************************************************************************
 *     FUNCTION NAME    :  FmFuncHandleLBAck
 *                                                                           
 *     DESCRIPTION      :  This function performs appropriate actions based on 
 *                         the type of Loopback acknowledgement received 
 *                                                                           
 *     INPUT            :  pPortEntry - pointer to port info structure 
 *                         u1State - State denoting establishment or 
 *                                   termination of remote loopback
 *                                                                           
 *     OUTPUT           :  None.                                             
 *                                                                           
 *     RETURNS          :  None. 
 ****************************************************************************/
PRIVATE VOID
FmFuncHandleLBAck (tFmPortEntry * pPortEntry, UINT1 u1State)
{
    FM_TRC_FN_ENTRY ();
    if (u1State == EOAM_ENABLED)
    {
        pPortEntry->LBTestInfo.u1LoopbackStatus = FM_REMOTE_LB;

        /* Remote Loopback has been established and is ready for 
         * loopback testing. So get the Session start time */
        OsixGetSysTime (&(pPortEntry->LBTestInfo.CurrSessionStats.StartTime));
    }
    else if (u1State == EOAM_DISABLED)
    {
        pPortEntry->LBTestInfo.u1LoopbackStatus = FM_NO_LB;
        /* Remote Loopback has been terminated. So get the Session End
         * time. Copy the current session stats info to the previous 
         * session stats info and clear the current session stats */
        OsixGetSysTime (&(pPortEntry->LBTestInfo.CurrSessionStats.EndTime));
        MEMCPY (&(pPortEntry->PrevSessionStats),
                &(pPortEntry->LBTestInfo.CurrSessionStats),
                sizeof (tFmLBTestStats));
        MEMSET (&(pPortEntry->LBTestInfo.CurrSessionStats), 0,
                sizeof (tFmLBTestStats));
    }
    FM_TRC_FN_EXIT ();
    return;
}
#endif /* FMFUNC_C */
/*-----------------------------------------------------------------------*/
/*                       End of the file  emfmfunc.c                     */
/*-----------------------------------------------------------------------*/
