/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emfmtrc.c,v 1.1.1.1 2008/06/03 14:31:04 iss Exp $
 *
 * Description: This file contains debugging related functions
 *****************************************************************************/
#include "emfminc.h"

/****************************************************************************
 *                                                                           
 * Function     : FmTrcPrint                                               
 *                                                                           
 * Description  :  prints the trace - with filename and line no              
 *                                                                           
 * Input        : fname   - File name                                        
 *                u4Line  - Line no                                          
 *                s       - string to be printed                             
 *                                                                           
 * Output       : None                                                       
 *                                                                           
 * Returns      : Returns string                                             
 *                                                                           
 *****************************************************************************/
VOID
FmTrcPrint (const CHR1 * fname, UINT4 u4Line, CHR1 * s)
{

    tOsixSysTime        sysTime;
    const CHR1         *pc1Fname = fname;
    char                ai1LogMsgBuf[FM_MAX_LOG_STR_LEN];

    if (s == NULL)
    {
        return;
    }
    while (*fname != '\0')
    {
        if (*fname == '/')
        {
            pc1Fname = (fname + 1);
        }
        fname++;
    }
    OsixGetSysTime (&sysTime);
    SNPRINTF (ai1LogMsgBuf, sizeof (ai1LogMsgBuf), "FM: %d:%s:%d:   %s",
              sysTime, pc1Fname, u4Line, s);
    UtlTrcPrint ((const char *) ai1LogMsgBuf);
}

/****************************************************************************
 *                                                                           
 * Function     : FmTrc                                                    
 *                                                                           
 * Description  : converts variable argument in to string depending on flag  
 *                                                                           
 * Input        : u4Flags  - Trace flag                                      
 *                fmt  - format strong, variable argument
 *                                                                           
 * Output       : None                                                       
 *                                                                           
 * Returns      : Returns string                                             
 *                                                                           
 *****************************************************************************/
CHR1               *
FmTrc (UINT4 u4Flags, const char *fmt, ...)
{
    va_list             ap;
#define FM_TRC_BUF_SIZE    2000
    static CHR1         ac1buf[FM_TRC_BUF_SIZE];

    if ((u4Flags & FM_TRC_FLAG) == 0)
    {
        return (NULL);
    }
    va_start (ap, fmt);
    vsprintf (&ac1buf[0], fmt, ap);
    va_end (ap);

    return (&ac1buf[0]);
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  emfmtrc.c                      */
/*-----------------------------------------------------------------------*/
