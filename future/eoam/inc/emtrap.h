/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emtrap.h,v 1.3 2010/07/28 10:30:50 prabuc Exp $
 * 
 * Description: This file contains header files included in 
 *              EOAM module.
 *********************************************************************/
#ifndef _EMTRAP_H_
#define _EMTRAP_H_

#define EOAM_MIB_EVT_LOG_TIMESTAMP        "dot3OamEventLogTimestamp" 
#define EOAM_MIB_EVT_LOG_OUI              "dot3OamEventLogOui" 
#define EOAM_MIB_EVT_LOG_TYPE             "dot3OamEventLogType" 
#define EOAM_MIB_EVT_LOG_LOCATION         "dot3OamEventLogLocation" 
#define EOAM_MIB_EVT_LOG_EVT_TOTAL        "dot3OamEventLogEventTotal" 

#define EOAM_MIB_EVT_LOG_WIN_HI           "dot3OamEventLogWindowHi" 
#define EOAM_MIB_EVT_LOG_WIN_LO           "dot3OamEventLogWindowLo" 
#define EOAM_MIB_EVT_LOG_THRES_HI         "dot3OamEventLogThresholdHi" 
#define EOAM_MIB_EVT_LOG_THRES_LO         "dot3OamEventLogThresholdLo" 
#define EOAM_MIB_EVT_LOG_VALUE            "dot3OamEventLogValue" 
#define EOAM_MIB_EVT_LOG_RUNNING_TOTAL    "dot3OamEventLogRunningTotal" 

/* Event Notification type - Threshold or Non- Threshold 
 * event OID for notification as mentioned in stdeoam.mib. 
 * Should not be changed.
 **/
enum
{
    EOAM_NOTIFY_THRESH_EVENT = 1,
    EOAM_NOTIFY_NON_THRESH_EVENT
};

/* Eoam trap specific function prototypes */
VOID 
EoamNotifyEventTrap (tEoamEventLogEntry * pTrapInfo,
                             UINT4 u4IfIndex,UINT4);

tSNMP_OID_TYPE     *    
EoamMakeObjIdFromDotNew (INT1 *pi1TextStr);

UINT1 * EoamParseSubIdNew (UINT1 *, UINT4 *);

#endif /* _EMTRAP_H_ */

/*-----------------------------------------------------------------------*/
/*                       End of the file  emtrap.h                        */
/*-----------------------------------------------------------------------*/
