/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emsz.h,v 1.4 2012/04/27 11:58:41 siva Exp $
 *
 * Description: This file contains prototypes for functions
 *              defined in EOAM.
 *********************************************************************/

enum {
    MAX_EOAM_DATA_BUF_SIZING_ID,
    MAX_EOAM_DLL_NODES_FOR_EOAM_ENABLED_PORTS_SIZING_ID,
    MAX_EOAM_ENABLED_PORTS_SIZING_ID,
    MAX_EOAM_EVENT_LOG_ENTRIES_SIZING_ID,
    MAX_EOAM_PDU_BUF_SIZING_ID,
    MAX_EOAM_PORTS_SIZING_ID,
    MAX_EOAM_Q_MESG_SIZING_ID,
    MAX_EOAM_VAR_REQ_RBT_NODES_SIZING_ID,
    MAX_EOAM_VAR_REQUESTS_SIZING_ID,
    EOAM_MAX_SIZING_ID
};


#ifdef  _EOAMSZ_C
tMemPoolId EOAMMemPoolIds[ EOAM_MAX_SIZING_ID];
INT4  EoamSizingMemCreateMemPools(VOID);
VOID  EoamSizingMemDeleteMemPools(VOID);
INT4  EoamSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _EOAMSZ_C  */
extern tMemPoolId EOAMMemPoolIds[ ];
extern INT4  EoamSizingMemCreateMemPools(VOID);
extern VOID  EoamSizingMemDeleteMemPools(VOID);
extern INT4  EoamSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _EOAMSZ_C  */


#ifdef  _EOAMSZ_C
tFsModSizingParams FsEOAMSizingParams [] = {
{ "UINT1[EOAM_MAX_DATA_SIZE]", "MAX_EOAM_DATA_BUF", sizeof(UINT1[EOAM_MAX_DATA_SIZE]),MAX_EOAM_DATA_BUF, MAX_EOAM_DATA_BUF,0 },
{ "tEoamLmEnaPortDllNode", "MAX_EOAM_DLL_NODES_FOR_EOAM_ENABLED_PORTS", sizeof(tEoamLmEnaPortDllNode),MAX_EOAM_DLL_NODES_FOR_EOAM_ENABLED_PORTS, MAX_EOAM_DLL_NODES_FOR_EOAM_ENABLED_PORTS,0 },
{ "tEoamEnaPortInfo", "MAX_EOAM_ENABLED_PORTS", sizeof(tEoamEnaPortInfo),MAX_EOAM_ENABLED_PORTS, MAX_EOAM_ENABLED_PORTS,0 },
{ "tEoamEventLogEntry", "MAX_EOAM_EVENT_LOG_ENTRIES", sizeof(tEoamEventLogEntry),MAX_EOAM_EVENT_LOG_ENTRIES, MAX_EOAM_EVENT_LOG_ENTRIES,0 },
{ "UINT1[EOAM_MAX_PDU_SIZE]", "MAX_EOAM_PDU_BUF", sizeof(UINT1[EOAM_MAX_PDU_SIZE]),MAX_EOAM_PDU_BUF, MAX_EOAM_PDU_BUF,0 },
{ "tEoamPortInfo", "MAX_EOAM_PORTS", sizeof(tEoamPortInfo),MAX_EOAM_PORTS, MAX_EOAM_PORTS,0 },
{ "tEoamQMsg", "MAX_EOAM_Q_MESG", sizeof(tEoamQMsg),MAX_EOAM_Q_MESG, MAX_EOAM_Q_MESG,0 },
{ "tEoamVarReqRBTNode", "MAX_EOAM_VAR_REQ_RBT_NODES", sizeof(tEoamVarReqRBTNode),MAX_EOAM_VAR_REQ_RBT_NODES, MAX_EOAM_VAR_REQ_RBT_NODES,0 },
{ "tEoamVarReq", "MAX_EOAM_VAR_REQUESTS", sizeof(tEoamVarReq),MAX_EOAM_VAR_REQUESTS, MAX_EOAM_VAR_REQUESTS,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _EOAMSZ_C  */
extern tFsModSizingParams FsEOAMSizingParams [];
#endif /*  _EOAMSZ_C  */


