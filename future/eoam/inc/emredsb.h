/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emredsb.h,v 1.5 2008/05/28 10:07:27 iss Exp $
 * 
 * Description: This file contains macros definitions for constants
 *              used for EOAM Redundancy
 *********************************************************************/
#ifndef _EMRED_H_
#define _EMRED_H_

/* Macros for Eoam redundancy feature. */
#define EOAM_RM_IS_STANDBY_UP()
#define EOAM_RM_INIT_NUM_STANDBY_NODES()
#define EOAM_RM_NODE_STATUS()  EOAM_ACTIVE_NODE
#define EOAM_RM_GET_NUM_STANDBY_NODES_UP()
#define EOAM_NUM_STANDBY_NODES()
#define EOAM_RM_GET_NODE_STATUS()  (gEoamGlobalInfo.EoamNodeStatus = EOAM_ACTIVE_NODE)
#define  EOAM_IS_NP_PROGRAMMING_ALLOWED() OSIX_TRUE

/* Prototypes used for EOAM redundancy module. */
PUBLIC INT4 EoamRedRegisterWithRM PROTO((VOID));
PUBLIC INT4 EoamRedDeRegisterWithRM PROTO ((VOID));
PUBLIC VOID EoamRedInitRedGlobalInfo PROTO((VOID));
PUBLIC VOID EoamRedGetNodeStateFromRm PROTO((VOID));
PUBLIC VOID EoamRedProcessRmMsg PROTO ((tEoamQMsg *));
PUBLIC VOID EoamRedSyncUpDynamicInfo PROTO ((tEoamPortInfo *));
PUBLIC VOID EoamRedSyncUpOperStatus PROTO ((tEoamPortInfo *));
#endif /* _EMRED_H */
/*-----------------------------------------------------------------------*/
/*                       End of the file  emred.h                        */
/*-----------------------------------------------------------------------*/
