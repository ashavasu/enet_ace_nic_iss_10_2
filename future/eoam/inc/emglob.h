/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emglob.h,v 1.8 2013/11/05 10:57:31 siva Exp $
 * 
 * Description: This file contains global variables used in EOAM.
 *********************************************************************/
#ifndef _EMGLOB_H
#define _EMGLOB_H

UINT1                gaTlvOui[EOAM_OUI_LENGTH] = { 0x01, 0x80, 0xC2 };
UINT1                gu1EmIfShDownFlag = OSIX_FALSE;
tEoamGlobalInfo      gEoamGlobalInfo;
tEoamLmGlobalInfo    gEoamLmGlobalInfo;
#ifdef L2RED_WANTED
tEoamRedGlobalInfo   gEoamRedGlobalInfo;
#endif

/* Sizing Params Structure. */
tFsModSizingParams gFsEoamSizingParams [] =
{

    {"EOAM_MAX_PDU_SIZE + 2", "EOAM_BUDDY_NUM_BLOCKS", (EOAM_MAX_PDU_SIZE + 2),
     EOAM_BUDDY_NUM_BLOCKS, EOAM_BUDDY_NUM_BLOCKS, 0},

    {"0", "0", 0,0,0,0}
};

tFsModSizingInfo gFsEoamSizingInfo;


#endif /* _EMGLOB_H*/
/*-----------------------------------------------------------------------*/
/*                       End of the file  emglob.h                       */
/*-----------------------------------------------------------------------*/
