/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emlmdefn.h,v 1.4 2012/03/26 12:35:23 siva Exp $
 * 
 * Description: This file contains macros used in EOAM Link
 *              monitoring module
 *********************************************************************/
#ifndef _EMLMDEFN_H
#define _EMLMDEFN_H

#define EOAMLM_TASK_NAME             "ELMT"

/* SEM related definitions */

#define EOAMLM_SEM_NAME              "ELMP"
#define ISSSZ_EOAM_LOCAL_INFO                 2 
#define ISSSZ_EOAM_PEER_INFO                  3
/* Events that will be received by EOAM LM Task */
#define EOAM_LM_TMR_EXPIRY_EVENT            0x01 /* Bit 0 */
#define EOAM_LM_GO_ACTIVE_EVENT             0x02 /* Bit 1 */
#define EOAM_LM_GO_STANDBY_EVENT            0x04 /* Bit 2 */
#define EOAM_LM_ALL_EVENTS          (EOAM_LM_TMR_EXPIRY_EVENT |\
                                     EOAM_LM_GO_ACTIVE_EVENT  |\
                                     EOAM_LM_GO_STANDBY_EVENT)

 /* Polling interval, in tens of milliseconds */
#define EOAMLM_POLLING_INTERVAL        (SYS_TIME_TICKS_IN_A_SEC / 10)

#define EOAMLM_TENTH_SEC_SINCE_LAST_SEC(u4Val) \
                                       (u4Val % 10)
#endif /* _EMLMDEFN_H */
/*-----------------------------------------------------------------------*/
/*                       End of the file  emlmdefn.h                     */
/*-----------------------------------------------------------------------*/
