/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emextn.h,v 1.7 2013/11/05 10:57:31 siva Exp $
 *
 * Description: This file contains extern variables declaration
 *              used in EOAM.
 *********************************************************************/
#ifndef _EMEXTN_H
#define _EMEXTN_H

PUBLIC UINT1              gaTlvOui[EOAM_OUI_LENGTH];
PUBLIC UINT1              gu1EmIfShDownFlag;
PUBLIC tEoamGlobalInfo    gEoamGlobalInfo;
PUBLIC tEoamLmGlobalInfo  gEoamLmGlobalInfo;
#ifdef L2RED_WANTED
PUBLIC tEoamRedGlobalInfo gEoamRedGlobalInfo;
#endif

/* Sizing Params Structure. */
extern tFsModSizingParams gFsEoamSizingParams [];
extern tFsModSizingInfo gFsEoamSizingInfo;
#endif /* _EMEXTN_H*/
/*-----------------------------------------------------------------------*/
/*                       End of the file  emextn.h                       */
/*-----------------------------------------------------------------------*/
