/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: eoamexlw.h,v 1.4 2008/08/20 14:26:57 premap-iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEoamSystemControl ARG_LIST((INT4 *));

INT1
nmhGetFsEoamModuleStatus ARG_LIST((INT4 *));

INT1
nmhGetFsEoamErrorEventResend ARG_LIST((UINT4 *));

INT1
nmhGetFsEoamOui ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsEoamTraceOption ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsEoamSystemControl ARG_LIST((INT4 ));

INT1
nmhSetFsEoamModuleStatus ARG_LIST((INT4 ));

INT1
nmhSetFsEoamErrorEventResend ARG_LIST((UINT4 ));

INT1
nmhSetFsEoamOui ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsEoamTraceOption ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsEoamSystemControl ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsEoamModuleStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsEoamErrorEventResend ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsEoamOui ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsEoamTraceOption ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsEoamSystemControl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsEoamModuleStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsEoamErrorEventResend ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsEoamOui ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsEoamTraceOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
