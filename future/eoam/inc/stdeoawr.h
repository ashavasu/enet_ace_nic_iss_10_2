/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: stdeoawr.h,v 1.3 2008/08/20 14:26:57 premap-iss Exp $
 * 
 * Description: This file contains header files included in 
 *              EOAM module.
 *********************************************************************/
#ifndef _STDEOAWR_H
#define _STDEOAWR_H
INT4 GetNextIndexDot3OamTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterSTDEOA(VOID);

VOID UnRegisterSTDEOA(VOID);
INT4 Dot3OamAdminStateGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamOperStatusGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamModeGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamMaxOamPduSizeGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamConfigRevisionGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamFunctionsSupportedGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamAdminStateSet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamModeSet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamAdminStateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot3OamModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot3OamTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexDot3OamPeerTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot3OamPeerMacAddressGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamPeerVendorOuiGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamPeerVendorInfoGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamPeerModeGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamPeerMaxOamPduSizeGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamPeerConfigRevisionGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamPeerFunctionsSupportedGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexDot3OamLoopbackTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot3OamLoopbackStatusGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamLoopbackIgnoreRxGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamLoopbackStatusSet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamLoopbackIgnoreRxSet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamLoopbackStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot3OamLoopbackIgnoreRxTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot3OamLoopbackTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexDot3OamStatsTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot3OamInformationTxGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamInformationRxGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamUniqueEventNotificationTxGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamUniqueEventNotificationRxGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamDuplicateEventNotificationTxGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamDuplicateEventNotificationRxGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamLoopbackControlTxGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamLoopbackControlRxGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamVariableRequestTxGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamVariableRequestRxGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamVariableResponseTxGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamVariableResponseRxGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamOrgSpecificTxGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamOrgSpecificRxGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamUnsupportedCodesTxGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamUnsupportedCodesRxGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamFramesLostDueToOamGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexDot3OamEventConfigTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot3OamErrSymPeriodWindowHiGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrSymPeriodWindowLoGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrSymPeriodThresholdHiGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrSymPeriodThresholdLoGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrSymPeriodEvNotifEnableGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrFramePeriodWindowGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrFramePeriodThresholdGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrFramePeriodEvNotifEnableGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrFrameWindowGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrFrameThresholdGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrFrameEvNotifEnableGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrFrameSecsSummaryWindowGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrFrameSecsSummaryThresholdGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrFrameSecsEvNotifEnableGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamDyingGaspEnableGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamCriticalEventEnableGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrSymPeriodWindowHiSet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrSymPeriodWindowLoSet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrSymPeriodThresholdHiSet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrSymPeriodThresholdLoSet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrSymPeriodEvNotifEnableSet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrFramePeriodWindowSet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrFramePeriodThresholdSet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrFramePeriodEvNotifEnableSet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrFrameWindowSet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrFrameThresholdSet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrFrameEvNotifEnableSet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrFrameSecsSummaryWindowSet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrFrameSecsSummaryThresholdSet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrFrameSecsEvNotifEnableSet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamDyingGaspEnableSet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamCriticalEventEnableSet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrSymPeriodWindowHiTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrSymPeriodWindowLoTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrSymPeriodThresholdHiTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrSymPeriodThresholdLoTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrSymPeriodEvNotifEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrFramePeriodWindowTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrFramePeriodThresholdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrFramePeriodEvNotifEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrFrameWindowTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrFrameThresholdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrFrameEvNotifEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrFrameSecsSummaryWindowTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrFrameSecsSummaryThresholdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot3OamErrFrameSecsEvNotifEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot3OamDyingGaspEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot3OamCriticalEventEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot3OamEventConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
















INT4 GetNextIndexDot3OamEventLogTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot3OamEventLogTimestampGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamEventLogOuiGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamEventLogTypeGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamEventLogLocationGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamEventLogWindowHiGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamEventLogWindowLoGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamEventLogThresholdHiGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamEventLogThresholdLoGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamEventLogValueGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamEventLogRunningTotalGet(tSnmpIndex *, tRetVal *);
INT4 Dot3OamEventLogEventTotalGet(tSnmpIndex *, tRetVal *);
#endif /* _STDEOAWR_H */
