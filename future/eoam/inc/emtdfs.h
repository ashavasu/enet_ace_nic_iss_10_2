/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emtdfs.h,v 1.22 2012/10/29 12:02:26 siva Exp $
 *
 * Description: This file contains data structures defined for
 *              EOAM module.
 *********************************************************************/
#ifndef _EMTDFS_H
#define _EMTDFS_H

/* Timer Types */
enum 
{
    EOAM_PDU_TX_TMR_TYPE,    /* 0 */
    EOAM_LOST_LINK_TMR_TYPE, /* 1 */
    EOAM_LB_CMD_TX_TMR_TYPE, /* 2 */
    EOAM_VAR_RESP_TMR_TYPE,  /* 3 */
    EOAM_MAX_TIMERS          /* 4 */ 
};

/* Timeout values */
enum
{
    EOAM_PDU_TX_TIMEOUT    = 1,  /* 1 second */
    EOAM_LOST_LINK_TIMEOUT = 5,  /* 5 secs */
    EOAM_LB_CMD_TX_TIMEOUT = 3,  /* 3 secs */
    EOAM_VAR_RESP_TIMEOUT  = 1   /* 1 sec */
};

/* Enum to denote the Discovery status */
enum
{
    EOAM_DISC_UNSATISFIED = 0,
    EOAM_DISC_NOT_COMPLETED,
    EOAM_DISC_COMPLETED
};

/* Link fault status */
enum
{
    EOAM_LINK_STATUS_FAIL = 1,
    EOAM_LINK_STATUS_OK
};
    
/* EOAM PDU codes */
enum
{
    EOAM_INFO = 0,
    EOAM_EVENT_NOTIFICATION,
    EOAM_VAR_REQ,
    EOAM_VAR_RESP,
    EOAM_LB_CTRL,
    EOAM_ORG_SPEC = 254,
    EOAM_MAX_PDU_CODE
};

/* Loop back command */
enum
{
    EOAM_LB_ENABLE = 1,
    EOAM_LB_DISABLE
};

/* EOAM Oper status */
enum
{
    EOAM_OPER_DISABLE = 1,
    EOAM_OPER_LINKFAULT,
    EOAM_OPER_PASSIVEWAIT, 
    EOAM_OPER_ACTIVE_SENDLOCAL,
    EOAM_OPER_SENDLOCALREMOTE,
    EOAM_OPER_SENDLOCALREMOTEOK,
    EOAM_OPER_PEERINGLOCALLYREJECTED,
    EOAM_OPER_PEERINGREMOTELYREJECTED,
    EOAM_OPER_OPERATIONAL,
    EOAM_OPER_HALF_DUPLEX,
    EOAM_MAX_STATES 
};

/* Peer node status */
enum
{
    EOAM_PEER_ACTIVE=1,
    EOAM_PEER_INACTIVE
};

/* Event Notification type - standard or Org specific */
enum
{
    EOAM_THRESH_EVENT = 1,
    EOAM_ORG_SPEC_EVENT
};

/* Macros defined for Link Event TLV processing */
#define EOAM_LNK_EVT_SYM_PER_TLV_BIT    0x01
#define EOAM_LNK_EVT_FRM_TLV_BIT        0x02
#define EOAM_LNK_EVT_FRM_PER_TLV_BIT    0x04
#define EOAM_LNK_EVT_SEC_SUM_TLV_BIT    0x08
#define EOAM_LNK_EVT_ORG_SPEC_TLV_BIT   0x10
#define EOAM_LNK_EVT_MAX_BIT            5

/* Message types received by EOAM module */
enum
{
    EOAM_ERR_SYMBOL_EVENT_TRIG_MSG=1,
    EOAM_ERR_FRAME_PERIOD_EVENT_TRIG_MSG,
    EOAM_ERR_FRAME_EVENT_TRIG_MSG,
    EOAM_ERR_FRAME_SEC_EVENT_TRIG_MSG,
    EOAM_ORG_SPEC_EVENT_TRIG_MSG,
    EOAM_LINK_FAULT_TRIG_MSG,
    EOAM_DYING_GASP_TRIG_MSG,
    EOAM_CRITICAL_EVENT_TRIG_MSG, 
    EOAM_ORG_SPEC_PDU_TRIG_MSG, 
    EOAM_LOOPBACK_ENABLE_MSG, 
    EOAM_LOOPBACK_DISABLE_MSG, 
    EOAM_VAR_REQ_PDU_TRIG_MSG, 
    EOAM_VAR_RESP_PDU_TRIG_MSG, 
    EOAM_PDU_RCVD_MSG, 
    EOAM_CREATE_PORT_MSG,
    EOAM_DELETE_PORT_MSG,
    EOAM_OPER_STATUS_CHG_MSG,
    EOAM_RM_MSG
};

/* List of Discovery state event machine events */
enum
{
    EOAM_DISC_EVENT_LOSTLINK_TMR_EXP = 0,
    EOAM_DISC_EVENT_LINK_STATUS,
    EOAM_DISC_EVENT_REMOTE_STATE_VALID,
    EOAM_DISC_EVENT_LOCAL_SATISFIED,
    EOAM_DISC_EVENT_LOCAL_UNSATISFIED,
    EOAM_DISC_EVENT_REMOTE_STABLE,
    EOAM_DISC_EVENT_REMOTE_UNSTABLE,
    EOAM_DISC_EVENT_OPER_UP,
    EOAM_DISC_EVENT_OPER_DOWN,
    EOAM_DISC_MAX_EVENTS
};

/* PDU state used by the Discovery SEM */
typedef enum
{
    EOAM_RX_INFO = 1,
    EOAM_LF_INFO,
    EOAM_INFO_ONLY,
    EOAM_ANY
} eEoamLocalPduState;

typedef UINT4 eEoamNodeStatus;

#define EOAM_IDLE_NODE                             RM_INIT
#define EOAM_ACTIVE_NODE                           RM_ACTIVE
#define EOAM_STANDBY_NODE                          RM_STANDBY

typedef struct EoamInfoParams
{
    UINT4    u4VendorInfo;         /* Vendor specific Info */
    UINT2    u2Revision;           /* field to indicate a change in a TLV field
                                      thereby causing change in
                                      discovery SEM */
    UINT2    u2OamPduConfig;       /* OAMPDU size and related parameters */
    UINT1    au1OUI[EOAM_OUI_LENGTH];   
                                   /* Organizationally unique Identifier */
    UINT1    u1OamConfig;          /* field indicating OAM mode and 
                                      capabilities */
    UINT1    u1OamVer;             /* OAM protocol version */
    UINT1    u1State;              /* field indicating local MUX and PARSER 
                                      actions */
    UINT1    au1Pad[2];
}tEoamInfoParams;

typedef struct EoamCtlReq
{
    eEoamMuxParAction  LocalMuxAction;        /* governs flow of frames from
                                                 MAC client through the 
                                                 Multiplexer */
    eEoamMuxParAction  LocalParAction;        /* governs the flow of non-OAMPDUs
                                                 through the Parser */
    BOOL1              b1LocalUnidirectional; /* Unidirectional transmission 
                                                 capability */
    BOOL1              b1LocalSatisfied;      /* indicates that local & remote  
                                                 OAM configuration settings are 
                                                 agreeable */
    BOOL1              b1RemoteStable;        /* indicates remote client's 
                                                 acknowledgment and satisfaction 
                                                 with local OAM state 
                                                 information */
    BOOL1              b1RemoteStateValid;    /* indicates that OAM client has
                                                 received remote state
                                                 information*/
    BOOL1              b1LocalDyingGasp;      /* indicates that the DTE has
                                                 experienced an unrecoverable 
                                                 failure condition */
    BOOL1              b1LocalCriticalEvent;  /* indicates that the DTE has
                                                 experienced an unspecified
                                                 critical event condition */
    UINT1              u1LocalLinkStatus;     /* link status */
    UINT1              u1Pad;
}tEoamCtlReq;

typedef struct EoamCtlInd
{
    eEoamLocalPduState  LocalPdu;                 /* governs transmission and 
                                                     reception of OAMPDUs as 
                                                     part of the Discovery 
                                                     process */
    BOOL1               b1LocalStable;            /* Indicates local OAM client
                                                     acknowledgment of and 
                                                     satisfaction with remote 
                                                      OAM state information */
    BOOL1               b1LocalEvaluating;        /* indicates local client has
                                                     completed discovery process 
                                                     or not*/
    BOOL1               b1LocalLostLinkTimerDone; /* Indicates non-receipt of
                                                     any OAMPDU for five
                                                     consecutive seconds */
    UINT1               au1Pad[1];
}tEoamCtlInd;

/* Since the declaration of tEoamPortInfo is needed in struct tEoamEnaPortInfo
 * and vice-versa, this is declared as below against convention */
typedef struct EoamPortSpecificInfo  tEoamPortInfo;

typedef struct {
    tRBTree       RBTVarReq;          /* Handle to the RBTree */
    tEoamPortInfo *pPortInfoBackPtr;  /* port number for which the timer runs */
    tTmrBlk       ReqRespAppTmr;  /* 1 sec Timer to check whether the response 
                                     for a particular Request ID has arrived */
    UINT4         u4ReqId;            /* Request Id */
    UINT4         u4Count;            /* Count of distinct descriptors within a 
                                         single Variable Request PDU */
}tEoamVarReq;                        

/* RBTree node for storing the response received from CMIP. */
typedef struct
{
    UINT1 *pu1Data;   /* Pointer to the variable response */
    UINT4 u4DataLen;  /* Length of the Response */
    UINT2 u2Leaf;     /* CMIP leaf in the request - 'key' */
    UINT1 u1Branch;   /* CMIP branch in the request - 'key' */
    UINT1 u1Pad;
}tEoamVarReqRBTNode;

/* EOAM information per port */
typedef struct {
    tEoamVarReq              *pVarReq[EOAM_MAX_VAR_REQS]; 
                                             /* array of pointers indexed by 
             ReqId to store the var reqs 
             that is forwarded to CMIP */    
    tEoamPortInfo            *pPortInfoBackPtr; 
                                             /* Back Pointer to port info 
             struct to reach the port info 
             during timer expiry */
    tEoamCtlReq              CtrlReq;        /* Discovery SEM related params 
                                                updated by OAM Client */
    tEoamCtlInd              CtrlInd;        /* Discovery SEM related params 
                                                updated by OAM Control */
    tTmrBlk                  LBTxTmr;        /* 3sec timer to check for LB 
                                                ack at the Tx side*/ 
    tTmrBlk                  LostLinkAppTmr; /* 5sec timer to restart
                                                discovery process when 
                                                OAMPDU is not rcvd */
    UINT4                    u4LogIndexCount;
    UINT4                    u4NonThresLogEvtTotalTx;/* Keeps track of Tx evt
                                                        total for non threshold
                                                        event */
    UINT4                    u4NonThresLogEvtTotalRx;/* Keeps track of Rx evt
                                                        total for non threshold
                                                        event */
    UINT2                    u2TxSeqNum;     /* Keeps track of Tx of unique 
                                                Err event OAMPDUs */
    UINT2                    u2RxSeqNum;     /* To keep track of seq no at 
                                                reception side */
    UINT1                    au1InfoPdu[EOAM_MAX_DATA_SIZE]; 
                                             /* Info OAMPDU to be sent on 
                                                EOAM enabled ports every sec */
    UINT1                    u1PduTxCntPerSec;
                                             /* To check no.of OAMPDUs 
                                                sent per second */
    UINT1                    au1Pad[3];
}tEoamEnaPortInfo;

typedef struct EoamLocalInfo
{
    UINT2      u2ConfigRevision;     /* Incremented and sent to denote 
                                        change in some local settings */
    UINT2      u2MaxOamPduSize;      /* Allowed size for OAMPDU */
    UINT2      u2EoamOperStatus;     /* Reflects Discovery SEM status */
    UINT1      u1EoamAdminStatus;    /* Per port Adminstatus */
    UINT1      u1Mode;               /* Active or Passive mode of operation */
    UINT1      u1FnsSupportedBmp;    /* OAM capabilities */                 
    UINT1      au1Pad[3];
}tEoamLocalInfo;

typedef struct EoamPeerInfo
{
    UINT4      u4VendorInfo;      /* Vendor specific value */
    UINT2      u2ConfigRevision;  /* Incremented and sent to denote change 
                                     in some remote settings */ 
    UINT2       u2MaxOamPduSize;  /* Allowed size for OAMPDU */
    UINT2      u2Flags;           /* Indicates any remote faliure and 
                                     evaluating and stable states */ 
    UINT1      u1State;           /* MUX And Parser states of the peer */
    UINT1      u1Mode;            /* Active or Passive mode of operation */
    tMacAddr   MacAddress;        /* MAC address of peer */
    UINT1      u1FnsSupportedBmp; /* OAM capabilities of peer */               
    UINT1      u1Status;          /* Status of the peer node */
    UINT1      au1VendorOui[EOAM_OUI_LENGTH];  
                                  /* Organizationally unique Id of peer*/
    UINT1      u1Pad;
}tEoamPeerInfo;

typedef struct EoamLBInfo
{
    UINT1      u1Status;   /* loopback status of the OAM entity */ 
    UINT1      u1IgnoreRx; /* controls whether received OAM loopback commands
                              are processed or ignored */                               
    UINT1      au1Pad[2];
}tEoamLBInfo;

typedef struct EoamPduStats
{
   /* Counters to keep track of  Tx and Rx OAMPDUs */
    UINT4     u4InformationTx;
    UINT4     u4InformationRx;
    UINT4     u4UniqueEventNotificationTx;
    UINT4     u4UniqueEventNotificationRx;
    UINT4     u4DuplicateEventNotificationTx;
    UINT4     u4DuplicateEventNotificationRx;
    UINT4     u4LoopbackControlTx;
    UINT4     u4LoopbackControlRx;
    UINT4     u4VariableRequestTx;
    UINT4     u4VariableRequestRx;
    UINT4     u4VariableResponseTx;
    UINT4     u4VariableResponseRx;
    UINT4     u4OrgSpecificTx;
    UINT4     u4OrgSpecificRx;
    UINT4     u4UnsupportedCodesTx;
    UINT4     u4UnsupportedCodesRx;
    UINT4     u4FramesLostDueToOam; /*count of the number of frames that were
          dropped by the OAM multiplexer*/
}tEoamPduStats;

typedef struct EventCfgInfo
{
    FS_UINT8        u8ErrSymPeriodWindow;   /* Window size in terms of number 
                                               of symbols */ 
    FS_UINT8        u8ErrSymPeriodThreshold;/* Threshold in terms of number of 
                                               symbol errors */
    UINT4           u4ErrFrameWindow;       /* Window size - no. of seconds */
    UINT4           u4ErrFrameThreshold;    /* Threshold in terms of number of 
                                               frame errors */
    UINT4           u4ErrFramePeriodWindow; /* Window size in no. of frames */
    UINT4           u4ErrFramePeriodThreshold;
                                            /* Threshold in terms of number of 
                                               frame errors*/
    UINT4           u4ErrFrameSecsSummaryWindow;
                                            /* Window size - number of secs */
    UINT4           u4ErrFrameSecsSummaryThreshold;
                                            /* Threshold in terms of number
                                               of errored seconds*/
    UINT1           u1ErrSymPeriodEvNotifEnable;
                                            /* Indicates whether the occurence 
            of an Errored Symbol Period 
            Event should result in an Event
                                               Notification OAMPDU generated by 
                                               the OAM layer*/
    UINT1           u1ErrFrameEvNotifEnable; 
                                            /* Indicates whether the occurence
                                               of an Errored Frame Event should
                                               result in an Event Notification 
                                               OAMPDU generated by OAM layer */
    UINT1           u1ErrFramePeriodEvNotifEnable;
                                          /* Indicates whether the occurence of
                                             Errored Frame Period Event should 
                                             result in an Event Notification 
                                             OAMPDU generated by the OAM layer*/
    UINT1           u1ErrFrameSecsEvNotifEnable;
                                          /* Indicates whether the occurence of 
                                             an Errored Seconds Summary Event 
                                             should result in an Event Notif
                                             OAMPDU generated by the OAM layer*/
    UINT1           u1DyingGaspEnable;    /* Indicates whether the occurence of 
                                             dying gasp event should result in 
                                             Info OAMPDU Tx */ 
    UINT1           u1CriticalEventEnable;/* Indicates whether the occurence of 
                                             critical event should result in 
                                             Info OAMPDU Tx */                     
    UINT1           au1Pad[2];
}tEoamErrEventConfigInfo;

typedef struct  EventLogEntry
{
    tEoamThresEventInfo  EventLogInfo;       /* Structure containing the 
                                                specific event information */
    UINT4                u4EventLogIndex;    /* Unique Log entry index */
    UINT4                u4EventLogType;     /* Type of event for which an 
                                                entry is logged */
    UINT4                u4NonThresLogEvtTotal; /* Total no. of times this 
                                                   event has occured*/
    UINT1                au1EventLogOui[EOAM_OUI_LENGTH]; 
                                             /* OUI of the entity defining the 
                                                object type */
    UINT1                u1EventLogLocation; /* Denotes whether the event 
                                                took place at the local or 
                                                remote end */
}tEoamEventLogEntry;

/* This data structure has all mib objects */
struct EoamPortSpecificInfo
{
    tTMO_DLL_NODE            EoamEnaPortNode;   /* EOAM enabled port list is 
                                                   maintained as a doubly link
                                 list */     
    tEoamEnaPortInfo         *pEoamEnaPortInfo; /* Pointer to the EOAM info
                                                   for EOAM enabled ports */   
    tEoamEventLogEntry       *apEventLogEntry[EOAM_MAX_LOG_ENTRIES]; 
                                                /* Table to maintain log of 
                                                   events at both local and 
                                                   remote end */
    tEoamLocalInfo           LocalInfo;         /* Local Entity Info */
    tEoamPeerInfo            RemoteInfo;        /* Remote Peer Entity Info */
    tEoamLBInfo              LoopbackInfo;      /* Loopback Command Info */
    tEoamPduStats            PduStats;          /* Stats of all Tx & Rx 
                                                   OAMPDUs */
    tEoamErrEventConfigInfo  ErrEventConfigInfo;/* Error Event related 
                                                   configs */
    UINT4                    u4Port;            /* Interface Index */
    UINT4                    u4Mtu;             /* MTU of the interface */
    UINT1                    u1OperStatus;      /* Operational Status of the 
                                                   interface */
    UINT1                    u1LogPosToBeFilled;/* Indicates the array element 
                                                   where the next entryis to be
                                                   logged */ 
    UINT1                    au1Pad[2];
};

typedef struct EoamSaveStats
{
    UINT4             u4InFrames;
    UINT4             u4InErrors;
}tEoamSaveStats;

typedef struct EoamGlobalInfo
{
    tTMO_DLL          EoamEnaPortDllList;  /* DLL list to maintain EOAM enabled
                                              ports information */
    tEoamPortInfo     *apEoamPortEntry[EOAM_MAX_PORTS];  
                                           /* Portwise Info */
    tEoamRegParams    EoamRegParams[EOAM_MAX_APPL_ID];
                                           /* Struct used by external module to 
                                              register with EOAM */
    tTmrDesc          aEoamTmrDesc[EOAM_MAX_TIMERS]; 
                                           /* Timer data struct that contains 
           func ptrs for timer handling and 
           offsets to identify the data 
           struct containing timer block.
           Timer ID is the index to this 
           data structure */
    tOsixTaskId       TaskId;
    tOsixQId          QId;
    tOsixSemId        ProtocolSemId;
    tOsixSemId        SyncSemId;
    tMemPoolId        PortPoolId;
    tMemPoolId        EnaPortPoolId;
    tMemPoolId        QMsgPoolId; 
    tMemPoolId        VarReqPoolId; 
    tMemPoolId        VarContainerPoolId; 
    tMemPoolId        EvtLogPoolId;
    tMemPoolId        EmLinearBufPoolId; 
    tMemPoolId        EmDataBufPoolId;
    tTimerListId      EoamTmrListId; 
    tTmrBlk           PduTxTmr;            /* 1 sec timer to transmit 
                                              info OAMPDU every sec */
    eEoamNodeStatus   EoamNodeStatus;      /* Node Status for RM */
    tEoamSaveStats    aEoamSaveStats[EOAM_MAX_PORTS]; 
    UINT4             u4TraceOption;
    UINT4             u4VendorSpecInfo;
    UINT4             u4ErrEventResendCount;
                                           /* Configurable Resend count for 
                                              Errored Events */
    UINT4             u4VarReqIdCount;     /* Unique ID for every variable 
                                              request submitted to CMIP I/F 
                                              incremented while submitting a 
                                              request to CMIP Interface */
    INT4              i4BuddyId;
    UINT1             au1OUI[EOAM_OUI_LENGTH];
                                           /* configurable OUI value */ 
    UINT1             u1SystemCtrlStatus;
    UINT1             u1ModuleStatus;      /* EOAM module status - enabled only
                                              when EoamNodeStatus is Active and
                                              u1ModuleAdminStatus is enabled */
    UINT1             u1ModuleAdminStatus; /* Stores configured EOAM status
                                              before GO_ACTIVE event is 
                                              received from RM */
    UINT1             au1Pad[2];
}tEoamGlobalInfo;  

/* EOAM PDU header */
typedef struct 
{
    tMacAddr    DestMacAddr;
    tMacAddr    SrcMacAddr;
    UINT2       u2SlowProtoType;
    UINT2       u2Flags;
    UINT1       u1SubType;
    UINT1       u1Code;
    UINT1       au1Pad[2];  
} tEoamPduHeader;

typedef struct 
{
    tEoamInfoParams     LocalInfo;
    tEoamInfoParams     RemoteInfo;
    tEoamExtData        OrgSpecInfo;   /* Stores Org specific Information */
} tEoamInfoPduData;

/* Event Notification PDU structure*/
typedef struct
{
    tEoamThresEventInfo  ErrSymblPrdEvnt;    /* Errd Symbol Prd Evt TLV */
    tEoamThresEventInfo  ErrFrmEvnt;         /* Errd Frm Evt TLV */
    tEoamThresEventInfo  ErrFrmPrdEvnt;      /* Errd Frm Prd Evt TLV */
    tEoamThresEventInfo  ErrFrmSecSumryEvnt; /* Errd Frm Sec Sum Evt TLV */
    tEoamExtData         OrgEvtData;         /* Org Specific Evt TLV */
    UINT2                u2SeqNum;
    UINT1                u1EvtType;   /* Standard Event or Org Spec Event */
    UINT1                u1EventType;  /* Errored Event - Used in Tx Flow 
                                        *   EOAM_ERRORED_SYMBOL_EVENT_TLV= 1 
                                        *   EOAM_ERRORED_FRAME_EVENT_TLV = 2
                                        *   EOAM_ERRORED_FRAME_PERIOD_EVENT_TLV = 3
                                        *   EOAM_ERRORED_FRAME_SECONDS_EVENT_TLV = 4
                                        *   EOAM_ORG_SPEC_LINK_EVENT = 254
                                        */ 
    UINT1                u1EvtBitMap;  /* Erorred Event Type - Used in Rx Flow
                                        * 0x01 - Errored Symbol Period Event
                                        * 0x02 - Errored Frame Event
                                        * 0x04 - Errored Frame Period Event
                                        * 0x08 - Errored Frame Sec summary Event
                                        * 0x10 - Organization Specific Event 
                                        */
    UINT1                au1Pad[3];
} tEoamEventPduData;

/* Structure containing union of all types 
 * of PDU structures for Client - Control 
 * interaction */
typedef struct 
{
    union
    {
        tEoamInfoPduData        InfoData; 
        tEoamEventPduData       EventData;
        UINT4                   u4LBCmd;
        tEoamExtData            ExtPduData;
    }unData;
#define InfoPduData     unData.InfoData
#define EvtPduData      unData.EventData
#define LBCmd           unData.u4LBCmd
#define ExternalPduData unData.ExtPduData
} tEoamPduData;

/* Data structures used for Messaging/Interface with RM  */
#ifdef L2RED_WANTED
typedef VOID * tEoamRedPeerId;
typedef struct _EoamRmCtrlMsg {
    tRmMsg           *pData;
    tEoamRedPeerId    RmPeerId;
    UINT2             u2DataLen;
    UINT1             u1RmEvent;
    UINT1             u1Pad;
} tEoamRmCtrlMsg;
#endif /* L2RED_WANTED */

/* The structure of message passed from other modules to EOAM. */
typedef struct
{
    UINT4 u4MsgType;                       /* Message type */
    UINT4 u4Port;
    union 
    {
        UINT4                  u4State;      /* State denoting 'up' or 'down'. 
                                                This state is used for link 
                                                status updation from CFA and 
                                                Link Fault Indication from FM */
        tEoamThresEventInfo    LMEvents;     /* structure passed from LM to OAM 
                                                intimating a threshold crossing 
                                                event */
        tEoamVarReqResp        VarReqResp;   /* Variable response structure
                                                posted from CMIP I/F to OAM */
        tEoamExtData           EoamExtData;  /* used to send VarReq, Org
                                                specific data from  FM */
        tCRU_BUF_CHAIN_HEADER  *pEoamPdu;    /* EOAMPDU received from CFA */
                                                
        tCRU_BUF_CHAIN_HEADER  *pMbsmProtoMsg;  /* MbsmProtoMsg received from
                                                mbsm */
#ifdef L2RED_WANTED
        tEoamRmCtrlMsg         RmEoamMsg;    /* Control Msg from RM Module */
#endif /* L2RED_WANTED */
    }unEoamMsgParam;
}tEoamQMsg;                               

/* Interface details needed by EOAM */
typedef struct {
    UINT4 u4IfMtu;         
    UINT4 u4IfSpeed;         
    UINT1 au1MacAddr[CFA_ENET_ADDR_LEN];
    UINT1 u1IfOperStatus;  
    UINT1 u1EoamStatus;     /* indicates whether EOAM is enabled/disabled */
    UINT1 u1MuxState;       /* Multiplexer action - FWD/DISCARD */
    UINT1 u1ParState;       /* Parser action - FWD/DISCARD/LOOPBACK */
    UINT1 u1UniDirSupp;     /* To indicate the unidirectional Tx capability */
    UINT1 u1LinkFault;      /* denotes that link fault has occured*/
    UINT1 u1RemoteLB;       /* denotes whether Loopback has been enabled at 
                               remote end */
    UINT1 u1DuplexStatus;   /* denotes whether duplexity status is half or 
                               full */
    UINT1 u1AutoNegStatus;  /* denotes whether AutoNeg is enabled on the port
                               or not */
    UINT1 au1Pad[1];
}tEoamIfInfo;


/* Sizing Params enum */
enum
{
   EOAM_BUDDY_SIZING_ID,
   EOAM_NO_SIZING_ID
};
#endif /* _EMTDFS_H */
/*-----------------------------------------------------------------------*/
/*                       End of the file  emtdfs.h                       */
/*-----------------------------------------------------------------------*/
