/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emred.h,v 1.9 2011/10/28 11:58:42 siva Exp $
 * 
 * Description: This file contains macros definitions for constants
 *              used for EOAM Redundancy
 *********************************************************************/
#ifndef _EMRED_H_
#define _EMRED_H_

/* Dynamic Update Events that will be sent and processed by EOAM
 * redundancy module */
typedef enum
{
    EOAM_BULK_REQ_MSG = RM_BULK_UPDT_REQ_MSG,
    EOAM_BULK_UPD_TAIL_MSG = RM_BULK_UPDT_TAIL_MSG,
    EOAM_DYNAMIC_CHANGE_MSG,
    EOAM_OPER_CHANGE_MSG,
    EOAM_MAX_RED_UPD_MSG,
    EOAM_HR_STDY_ST_PKT_REQ = RM_HR_STDY_ST_PKT_REQ,
    EOAM_HR_STDY_ST_PKT_MSG = RM_HR_STDY_ST_PKT_MSG,
    EOAM_HR_STDY_ST_PKT_TAIL = RM_HR_STDY_ST_PKT_TAIL
} eEoamRedUpdateMsgType;

typedef struct _EoamRedGlobalInfo {
    UINT4               u4BulkUpdNextPort;
    /* Next port for which Bulk update has to be generated. */
    UINT1               u1NumPeersUp;
    /* Indicates number of standby nodes that are up. */
    BOOL1               bBulkReqRcvd;
    /* To check whether bulk request received from standby before
     * RM_STANDBY_UP event. */
    /* HITLESS RESTART */
    UINT1               u1StdyStReqRcvd;
    /* Steady State Request Received Flag - OSIX_TRUE/OSIX_FALSE.
     * This flag is set when the Steady State packet request is received from
     * RM after finishing the bulk storage for Hitless Restart */
    UINT1               au1Pad[1];
} tEoamRedGlobalInfo;

/* Constants for EOAM redundancy feature. */
#define EOAM_RM_MSG_TYPE_SIZE              4
#define EOAM_RM_LEN_SIZE                   2
#define EOAM_RM_PORTNUM_SIZE               4
#define EOAM_RM_DYN_LOCAL_SIZE             15
#define EOAM_RM_DYN_REMOTE_SIZE            23
#define EOAM_RM_DYN_PORTINFO_SIZE          EOAM_RM_PORTNUM_SIZE + \
                                           EOAM_RM_DYN_LOCAL_SIZE + \
                                           EOAM_RM_DYN_REMOTE_SIZE
#define EOAM_RM_SYNCUP_HDR_LEN             6
#define EOAM_RM_DYNAMIC_MSG_SIZE           EOAM_RM_DYN_PORTINFO_SIZE + \
                                           EOAM_RM_SYNCUP_HDR_LEN
#define EOAM_RM_OPERCHG_MSG_SIZE           EOAM_RM_SYNCUP_HDR_LEN + \
                                           EOAM_RM_PORTNUM_SIZE + 1
#define EOAM_RM_OFFSET                     0
#define EOAM_RM_BULK_SPLIT_MSG_SIZE        1500
/* Maximum no of sub updates in bulk updation process. */
#define EOAM_RM_NUM_PORTS_PER_SUB_UPDATE  10

/* Macros for Eoam redundancy feature. */
#define EOAM_RM_IS_STANDBY_UP() \
          ((gEoamRedGlobalInfo.u1NumPeersUp > 0) ? OSIX_TRUE : OSIX_FALSE)


#define EOAM_RM_INIT_NUM_STANDBY_NODES() \
          gEoamRedGlobalInfo.u1NumPeersUp = 0;

#define EOAM_RM_NODE_STATUS()  gEoamGlobalInfo.EoamNodeStatus

#define EOAM_RM_GET_NUM_STANDBY_NODES_UP() \
      gEoamRedGlobalInfo.u1NumPeersUp = RmGetStandbyNodeCount ()

#define EOAM_NUM_STANDBY_NODES() gEoamRedGlobalInfo.u1NumPeersUp

#define EOAM_IS_STANDBY_UP() \
          ((gEoamRedGlobalInfo.u1NumPeersUp > 0) ? OSIX_TRUE : OSIX_FALSE)

#define EOAM_RM_GET_NODE_STATUS()  EoamRedGetNodeStateFromRm ()
        
#ifdef RM_WANTED
#define  EOAM_IS_NP_PROGRAMMING_ALLOWED() \
        L2RED_IS_NP_PROGRAMMING_ALLOWED ()
#else
#define  EOAM_IS_NP_PROGRAMMING_ALLOWED() OSIX_TRUE
#endif

/* HITLESS RESTART */
/* Macros for EOAM Global PDU Tx Timer */
#define EOAM_TMR_RUNNING               0x1
#define EOAM_TMR_EXPD                  0x2

#define EOAM_RM_HR_STATUS()            RmGetHRFlag()
#define EOAM_HR_STATUS_DISABLE         RM_HR_STATUS_DISABLE
#define EOAM_HR_STDY_ST_REQ_RCVD()     gEoamRedGlobalInfo.u1StdyStReqRcvd
#define EOAM_HR_CHECK_STDY_ST_PKT(pPortInfo)    \
    ((pPortInfo->pEoamEnaPortInfo->CtrlReq.b1RemoteStateValid == OSIX_TRUE) && \
     ((pPortInfo->RemoteInfo.u2Flags & EOAM_LOCAL_STABLE_BITMASK) != 0) && \
     (pPortInfo->pEoamEnaPortInfo->CtrlInd.b1LocalStable == OSIX_TRUE))

/* HITLESS RESTART end */

/* Prototypes used for EOAM redundancy module. */
PUBLIC INT4 EoamRedRegisterWithRM PROTO((VOID));
PUBLIC INT4 EoamRedDeRegisterWithRM PROTO ((VOID));
PUBLIC VOID EoamRedInitRedGlobalInfo PROTO((VOID));
PUBLIC VOID EoamRedGetNodeStateFromRm PROTO((VOID));
PUBLIC VOID EoamRedProcessRmMsg PROTO ((tEoamQMsg *));
PUBLIC VOID EoamRedSyncUpDynamicInfo PROTO ((tEoamPortInfo *));
PUBLIC VOID EoamRedSyncUpOperStatus PROTO ((tEoamPortInfo *));
/* HITLESS RESTART */
INT1 EoamRedHRSendStdyStPkt     (UINT1 *, UINT4, UINT2, UINT4);
VOID EoamRedHRProcStdyStPktReq  (VOID);
INT1 EoamRedHRSendStdyStTailMsg (VOID);
/* end HITLESS RESTART */
#endif /* _EMRED_H */
/*-----------------------------------------------------------------------*/
/*                       End of the file  emred.h                        */
/*-----------------------------------------------------------------------*/
