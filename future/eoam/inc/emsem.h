/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emsem.h,v 1.5 2010/05/19 07:12:20 prabuc Exp $
 *
 * Description: Data structure used by Ethernet OAM Discovery State
 *              Event machine.
 *********************************************************************/
#ifndef _EMSEM_H_
#define _EMSEM_H_

PRIVATE VOID   EoamSemEventIgnore PROTO ((tEoamPortInfo *));
PRIVATE VOID   EoamSemStateFault PROTO ((tEoamPortInfo *));
PRIVATE VOID   EoamSemStateActiveSendLocal PROTO ((tEoamPortInfo *));
PRIVATE VOID   EoamSemStatePassiveWait PROTO ((tEoamPortInfo *));
PRIVATE VOID   EoamSemStateSendLocalRemote PROTO ((tEoamPortInfo *));
PRIVATE VOID   EoamSemStateSendLocalRemoteOk PROTO ((tEoamPortInfo *));
PRIVATE VOID   EoamSemStateSendAny PROTO ((tEoamPortInfo *));
PRIVATE VOID   EoamSemNotifyAllEvents PROTO((tEoamPortInfo * pPortInfo));

enum {
    A0 = 0,
    A1,
    A2,
    A3,
    A4,
    MAX_EOAM_FN_PTRS
};

/* EOAM SEM function pointers */
void (*gaEoamActionProc[MAX_EOAM_FN_PTRS]) (tEoamPortInfo *) =
{
   EoamSemEventIgnore,           /* A0 */
   EoamSemStateFault,            /* A1 */
   EoamSemStateSendLocalRemote,  /* A2 */
   EoamSemStateSendLocalRemoteOk,/* A3 */
   EoamSemStateSendAny           /* A4 */
};

/* Eoam State Event Table */
const UINT1 gau1EoamSem[EOAM_DISC_MAX_EVENTS][EOAM_MAX_STATES - 1] =
/* DISAB FAULT PASSV SND_ACT SND_LCL SND_LCL LCL RMT SEND HALF * States *
 *  LED        WAIT  LOCAL     RMT   RMT_OK  REJ REJ ANY  DUPLEX   */
{                                                                     /* Events */
  { A0,   A0,  A0,     A0,    A1,    A1,     A1, A1, A1,  A1 },  /* LINK_LOST_TMR_EXPY */
  { A0,   A1,  A1,     A1,    A1,    A1,     A1, A1, A1,  A1 },  /* LINK_STATUS_CHANGE */
  { A0,   A0,  A2,     A2,    A0,    A0,     A0, A0, A0,  A1 },  /* REMOTE_STATE_VALID */
  { A0,   A0,  A0,     A0,    A3,    A0,     A3, A0, A0,  A1 },  /* LOCAL_SATISFIED */
  { A0,   A0,  A0,     A0,    A0,    A2,     A0, A2, A2,  A1 },  /* LOCAL_UNSATISFIED */
  { A0,   A0,  A0,     A0,    A0,    A4,     A0, A4, A0,  A1 },  /* REMOTE_STABLE */
  { A0,   A0,  A0,     A0,    A0,    A0,     A0, A0, A3,  A1 },  /* REMOTE_UNSTABLE */
  { A1,   A1,  A0,     A0,    A0,    A0,     A0, A0, A0,  A1 },  /* OPER_UP */
  { A0,   A0,  A1,     A1,    A1,    A1,     A1, A1, A1,  A1 },  /* OPER_DOWN */
};

const CHR1 *gau1EoamStateStr[EOAM_MAX_STATES - 1] = {
   "DISABLED",
   "FAULT",
   "PASSIVE_WAIT",
   "SEND_ACTV_LCL",
   "SEND_LCL_RMT",
   "SEND_LCL_RMT_OK",
   "LOCAL_REJECTED",
   "REMOTE_REJECTED",
   "SEND_ANY",
   "HALF_DUPLEX"    
};

const CHR1 *gau1EoamEvntStr[EOAM_DISC_MAX_EVENTS] = {
   "LINK_LOST_TMR_EXPY",
   "LINK_STATUS_FAIL",
   "REMOTE_STATE_VALID",
   "LOCAL_SATISFIED",
   "LOCAL_UNSATISFIED",
   "REMOTE_STABLE",
   "REMOTE_UNSTABLE",
   "OPER_UP",
   "OPER_DOWN"
};
#endif /* _EMSEM_H_ */

/*-----------------------------------------------------------------------*/
/*                       End of the file  emsem.h                        */
/*-----------------------------------------------------------------------*/
