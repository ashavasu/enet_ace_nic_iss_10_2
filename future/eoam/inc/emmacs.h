/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emmacs.h,v 1.8 2012/10/29 12:02:26 siva Exp $
 *
 * Description: This file contains macro definitions used in
 *              EOAM modules
 *********************************************************************/
#ifndef _EMMACS_H
#define _EMMACS_H

#define EOAM_BUDDY_ALLOC(n)              \
                  MemBuddyAlloc ((UINT1) gEoamGlobalInfo.i4BuddyId, n)
#define EOAM_BUDDY_FREE(p)               \
                  MemBuddyFree ((UINT1) gEoamGlobalInfo.i4BuddyId, p)

#define EOAM_IS_LOCAL_MAC_LOWER(MacAddr1, MacAddr2)       \
        ((MEMCMP(MacAddr1, MacAddr2, MAC_ADDR_LEN) <= 0)? \
         OSIX_TRUE : OSIX_FALSE)

#define EOAM_FILL_UNUSED_8BYTE_DATA(Entry) \
{                                          \
     FSAP_U8_ASSIGN_HI(&Entry,0xffffffff);  \
     FSAP_U8_ASSIGN_LO(&Entry,0xffffffff);  \
}

#define EOAM_VALIDATE_EVENT_TYPE(u1Type)          \
         ((u1Type >= EOAM_ERRORED_SYMBOL_EVENT_TLV) && \
          (u1Type <= EOAM_ORG_SPECIFIC_LINK_EVENT_TLV)? OSIX_SUCCESS : OSIX_FAILURE)

#define EOAM_IS_VALID_PDU(i4Code)   \
         ((((i4Code >= EOAM_INFO) && (i4Code <= EOAM_LB_CTRL)) || \
          (i4Code == EOAM_ORG_SPEC))? OSIX_TRUE : OSIX_FALSE)

#define EOAM_CHECK_REMOTE_STABLE(u2Flags)  \
         ((((u2Flags & EOAM_LOCAL_EVAL_STABLE_BITMASK) >> 3) == \
          EOAM_DISC_COMPLETED)? OSIX_TRUE : OSIX_FALSE)

#define EOAM_CHECK_REMOTE_UNSTABLE(u2Flags)  \
         ((((u2Flags & EOAM_LOCAL_EVAL_STABLE_BITMASK) >> 3) == \
          EOAM_DISC_UNSATISFIED)? OSIX_TRUE : OSIX_FALSE)

#define EOAM_CHECK_REMOTE_NOT_COMPLETED(u2Flags)  \
       ((((u2Flags & EOAM_LOCAL_EVAL_STABLE_BITMASK) >> 3) == \
        EOAM_DISC_NOT_COMPLETED)? OSIX_TRUE : OSIX_FALSE)


#define EOAM_IS_VALID_OAM_VERSION(u1Version) \
         ((u1Version == EOAM_VERSION)? OSIX_TRUE : OSIX_FALSE)

#define EOAM_IS_LAST_BRANCH(u1Branch) ((u1Branch == 0)? OSIX_TRUE : OSIX_FALSE)

#define EOAM_IS_VARIABLE_INDICATION(u1Val) \
         ((u1Val & 0x80) ? OSIX_TRUE : OSIX_FALSE)

#define EOAM_IS_VALID_DESCRIPTOR(u1Branch, u2Leaf) \
         ((((u1Branch != EOAM_ATTRIBUTE) && (u1Branch != EOAM_PACKAGE) && \
          (u1Branch != EOAM_OBJECT)) || (u2Leaf < 1))? OSIX_FALSE : OSIX_TRUE)
#define EOAM_FORM_VARIABLE_INDICATION(u1Branch) \
        ((u1Branch == EOAM_ATTRIBUTE) ? (0x80 | 0x20): \
         ((u1Branch == EOAM_OBJECT)) ? (0x80 | 0x41): \
         ((u1Branch == EOAM_PACKAGE)) ? (0x80 | 0x61) : 0)
         
#define  EOAM_OFFSET(x,y) FSAP_OFFSETOF(x,y) 

#define EOAM_GET_PORT_INFO_PTR_FROM_ENA_LST(x) \
 (tEoamPortInfo *)(((UINT1 *)x) - EOAM_OFFSET(tEoamPortInfo, EoamEnaPortNode))
 
#define EOAM_GET_1BYTE(u1Val, pu1Buf)\
        {                         \
           u1Val = *pu1Buf;          \
           pu1Buf += 1;              \
        } 

#define EOAM_GET_2BYTE(u2Val, pu1Buf)    \
        {                             \
           MEMCPY (&u2Val, pu1Buf, 2);\
           pu1Buf += 2;                  \
           u2Val = (UINT2) (OSIX_NTOHS(u2Val)); \
        }

/* Encode Module Related Macros used to assign fields to buffer */
#define EOAM_PUT_1BYTE(pu1PktBuf,u1Val) \
        {                            \
           *pu1PktBuf = u1Val;          \
           pu1PktBuf += 1;              \
        } 

#define EOAM_PUT_2BYTE(pu1PktBuf, u2Val)         \
        {                                     \
            UINT2 u2Value = OSIX_HTONS(u2Val);\
            MEMCPY (pu1PktBuf, &u2Value, sizeof (UINT2));\
            pu1PktBuf += sizeof (UINT2);                      \
        }

#define EOAM_PUT_4BYTE(pu1PktBuf, u4Val)        \
         {                                   \
            UINT4 u4Value = OSIX_HTONL(u4Val);\
            MEMCPY (pu1PktBuf, &u4Value, sizeof (UINT4));\
           pu1PktBuf += sizeof (UINT4);                      \
         }

/*******************************************************
*** MACROs For Network To Host Byte Order Conversion ***
********************************************************/
#define EOAM_CRU_GET_1_BYTE(pMsg, u4Offset, u1Value) \
   CRU_BUF_Copy_FromBufChain(pMsg, &u1Value, u4Offset, 1)

#define EOAM_CRU_GET_2_BYTE(pMsg, u4Offset, u2Value) \
{\
   CRU_BUF_Copy_FromBufChain(pMsg, ((UINT1 *) &u2Value), u4Offset, 2);\
   u2Value = OSIX_NTOHS (u2Value);\
}

#define EOAM_CRU_GET_4_BYTE(pMsg, u4Offset, u4Value) \
{\
   CRU_BUF_Copy_FromBufChain(pMsg, ((UINT1 *) &u4Value), u4Offset, 4);\
   u4Value = OSIX_NTOHL (u4Value);\
}

#define EOAM_CRU_GET_STRING(pBufChain,pu1_StringMemArea,u4Offset,u4_StringLength) \
{\
   CRU_BUF_Copy_FromBufChain(pBufChain, pu1_StringMemArea, u4Offset, u4_StringLength);\
}

#endif /* EMMACS_H */
/*-----------------------------------------------------------------------*/
/*                       End of the file  emmacs.h                       */
/*-----------------------------------------------------------------------*/
