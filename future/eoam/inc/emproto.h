/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emproto.h,v 1.21 2012/05/30 06:22:59 siva Exp $
 *
 * Description: This file contains prototypes for functions
 *              defined in EOAM.
 *********************************************************************/
#ifndef _EMPROTO_H
#define _EMPROTO_H

/* emmain.c */
PUBLIC VOID EoamMainInitGlobalInfo PROTO ((VOID));
PUBLIC VOID EoamMainClearEvtLogs   PROTO ((tEoamPortInfo *));
PUBLIC VOID EoamMainClearPortInfo  PROTO ((tEoamPortInfo *));
PUBLIC INT4 EoamMainMemInit        PROTO ((VOID));
PUBLIC VOID EoamMainMemClear       PROTO ((VOID));
PUBLIC VOID EoamMainNotifyModules  PROTO ((tEoamCallbackInfo *));
PUBLIC VOID EoamMainClearEnabledInfo PROTO ((tEoamEnaPortInfo *));
PUBLIC VOID EoamMainAssignMempoolIds  PROTO((VOID));
/* emmuxpar.c */
PUBLIC VOID EoamMuxParSet PROTO ((tEoamPortInfo *, 
                                  UINT1 u1LoopbackStatus));

/* emmod.c */
PUBLIC INT4 EoamHandleModuleStart PROTO ((VOID));
PUBLIC VOID EoamHandleModuleShutDown PROTO ((VOID));
PUBLIC INT4 EoamModuleEnable   PROTO ((VOID));
PUBLIC VOID EoamModuleDisable  PROTO ((VOID));

/* emif.c */
PUBLIC INT4 EoamIfEnable  PROTO ((tEoamPortInfo *));
PUBLIC INT4 EoamIfDisable PROTO ((tEoamPortInfo *));
PUBLIC INT4 EoamIfCreate  PROTO ((UINT4 u4PortIndex));
PUBLIC INT4 EoamIfDelete  PROTO ((tEoamPortInfo *));
PUBLIC INT4 EoamIfOperChg PROTO ((tEoamPortInfo *, UINT1 u1OperStatus));
PUBLIC VOID EoamIfInit    PROTO ((tEoamPortInfo *));
PUBLIC INT4 EoamIfAddToPortTable PROTO ((tEoamPortInfo *));
PUBLIC VOID EoamIfCreateAllPorts PROTO ((VOID));
#ifdef NPAPI_WANTED
PUBLIC  VOID EoamIfSetUniDirectionalCpb PROTO ((tEoamPortInfo *));
#endif /* NPAPI_WANTED */
/* emlmpoll.c */
VOID EoamLmSaveHwStat PROTO ((UINT4 u4Port));

/* emtmr.c */
PUBLIC INT4 EoamTmrInit   PROTO ((VOID));
PUBLIC INT4 EoamTmrDeInit PROTO ((VOID));
PUBLIC VOID EoamTmrExpHandler PROTO ((VOID));

/* emcfaif.c */
PUBLIC INT4 EoamValidateIfIndex PROTO ((UINT4 u4PortIndex));
PUBLIC INT4 EoamGetIfInfo PROTO ((UINT4 u4IfIndex, 
                                     tEoamIfInfo * pIfInfo));
PUBLIC VOID EoamGetSysMacAddress PROTO ((tMacAddr BaseMacAddr));
PUBLIC INT4 EoamTransmitFrame PROTO ((tCRU_BUF_CHAIN_HEADER *, 
                                      UINT4 u4Port, UINT4 u4PktSize));
PUBLIC INT4 EoamTxLoopbackTestFrame PROTO ((UINT4 u4IfIndex, 
                                            tCRU_BUF_CHAIN_HEADER *,
                                            UINT4 u4PktSize));
PUBLIC INT4 EoamIsPhysicalPort PROTO ((UINT4 u4IfIndex));

/* emport.c */
PUBLIC INT4 EoamNegotiateDiscovery PROTO ((tEoamPortInfo *));
PUBLIC VOID EoamPutOrgSpecInfoTLV PROTO ((tEoamPortInfo *, UINT1 **));
PUBLIC UINT4 EoamSizeofOrgSpecInfoTLV PROTO ((tEoamEnaPortInfo * ));
PUBLIC VOID EoamProcessOrgInfoTLV PROTO ((tEoamPortInfo * pPortInfo,
                                          tEoamExtData * pOrgSpecInfo));
PUBLIC VOID EoamPopulateOrgEvtLogEntry PROTO ((tEoamExtData * pOrgSpecInfo,
                                               tEoamEventLogEntry *pLogEntry));

/* emque.c */
PUBLIC VOID EoamQueMsgHandler PROTO ((VOID));
PUBLIC INT4 EoamQueEnqAppMsg  PROTO ((tEoamQMsg *));
VOID EoamQueHandleEnqFailure PROTO ((tEoamQMsg *pMsg));

/* emutil.c */
PUBLIC INT4 EoamUtilGetPortEntry PROTO ((UINT4 u4PortIndex, 
                                        tEoamPortInfo **));
PUBLIC INT4 EoamUtilRBTreeDescCmp PROTO ((tRBElem *, tRBElem *));
PUBLIC INT4 EoamUtilRBTFreeNodeFn PROTO ((tRBElem *, UINT4 ));
PUBLIC tEoamEventLogEntry * EoamUtilGetLogEntryToBeFilled 
                                  PROTO((tEoamPortInfo *));
PUBLIC INT4 EoamUtilCopyInfo PROTO ((UINT1 **, UINT1 *, UINT4));

/* emctlrx.c */
PUBLIC VOID  EoamCtlRxHandleIncomingPdu PROTO ((tEoamPortInfo *,
                                                tCRU_BUF_CHAIN_HEADER *));

/* emctltx.c */
PUBLIC INT4  EoamCtlTxHandlePduTmrExpiry PROTO ((VOID));
PUBLIC VOID  EoamCtlTxFormPduHeader PROTO ((tEoamPortInfo *, UINT1 ,
                                            tEoamPduHeader *));
PUBLIC INT4  EoamCtlTxConstructInfoPdu PROTO ((tEoamPortInfo *));
PUBLIC INT4  EoamCtlTxSendInfoPdu PROTO ((tEoamPortInfo * ));
PUBLIC INT4  EoamCtlTxPduRequest PROTO ((tEoamPortInfo * ,tEoamPduHeader *,
                                         tEoamPduData *));

/* emsem.c */
PUBLIC VOID  EoamSemMachine PROTO ((tEoamPortInfo *, UINT1 ));

/* emcltpdu.c */
PUBLIC INT4 EoamCltPduProcess PROTO ((tEoamPortInfo *, tEoamPduHeader *, 
                                      tEoamPduData *));
PUBLIC INT4 EoamCltInfHandleDiscovery PROTO ((tEoamPortInfo *, UINT1, UINT1));

/* emcltinf.c */
PUBLIC INT4 EoamCltInfPduProcess PROTO ((tEoamPortInfo *, UINT1,
                                         tEoamPduData *));
PUBLIC INT4 EoamCltInfPduParseHeader PROTO ((tEoamPortInfo *,
                                            tEoamPduHeader *, UINT1 *));

PUBLIC INT4 EoamCltInfSndCriticalLnkEvt2Peer PROTO ((tEoamPortInfo *, UINT4,
                                                        UINT1));
/* emcltevt.c */
PUBLIC INT4 EoamCltEvtNotifPduProcess PROTO ((tEoamPortInfo *, tEoamPduData *));
PUBLIC INT4 EoamCltEvtSendThresholdEvtToPeer PROTO ((tEoamPortInfo *, 
                                                     tEoamThresEventInfo *,
                                                     UINT4));
PUBLIC INT4 EoamCltEvtSendOrgSpecificEvt PROTO ((tEoamPortInfo *,
                                                 tEoamExtData *));
/* emcltlb.c */
PUBLIC INT4 EoamCltLbHandleTmrExpiry PROTO ((tEoamPortInfo * pPortInfo));
PUBLIC INT4 EoamCltLbPduProcess PROTO ((tEoamPortInfo *, tEoamPduData *));
PUBLIC INT4 EoamCltLbCmdSendToPeer PROTO ((tEoamPortInfo *, UINT1));
PUBLIC INT4 EoamCltLbHandleAck PROTO ((tEoamPortInfo *));
PUBLIC VOID EoamCltLbRevertToNormal PROTO ((tEoamPortInfo *pPortInfo));

/* emcltreq.c */
PUBLIC INT4 EoamCltReqPduProcess PROTO ((tEoamPortInfo *, tEoamPduData *));
PUBLIC INT4 EoamCltReqSendToPeer PROTO ((tEoamPortInfo *, tEoamExtData *));
/* emcltres.c */
PUBLIC INT4 EoamCltResAccumulate PROTO ((tEoamPortInfo *, tEoamVarReqResp *));
PUBLIC INT4 EoamCltResSendToPeer PROTO ((tEoamPortInfo *, tEoamVarReq *));
PUBLIC INT4 EoamCltResPduProcess PROTO ((tEoamPortInfo *, tEoamPduData *));
/* emcltorg.c */
PUBLIC INT4 EoamCltOrgSpecPduProcess PROTO ((tEoamPortInfo *, tEoamPduData *));
PUBLIC INT4 EoamCltOrgSendOrgSpecificPdu PROTO ((tEoamPortInfo *, 
                                                 tEoamExtData *));


/* emtrc.c */
CHR1  *EoamTrc           PROTO(( UINT4 u4Trc,  const char *fmt, ...));
CHR1  *EoamTrcConcat     PROTO((const  CHR1 * str, const CHR1 * func));
VOID   EoamTrcPrint      PROTO(( const CHR1 *fname, UINT4 u4Line, CHR1 *s));

#ifdef MBSM_WANTED
/* emmbsm.c */
PUBLIC VOID EoamMbsmHandleLcStatusChg PROTO ((tCRU_BUF_CHAIN_HEADER *, 
                                              UINT4));
#endif

INT4 EoamRegisterWithPacketHandler PROTO ((VOID));

/* emred.c */
#ifdef L2RED_WANTED
PUBLIC VOID EoamRedProcessBulkReqEvent PROTO ((VOID *));
#endif
#endif /* _EMPROTO_H */
/*-----------------------------------------------------------------------*/
/*                       End of the file  emproto.h                      */
/*-----------------------------------------------------------------------*/
