/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: eminc.h,v 1.12 2012/06/01 10:48:32 siva Exp $
 * 
 * Description: This file contains header files included in 
 *              EOAM module.
 *********************************************************************/
#ifndef _EMINC_H
#define _EMINC_H

#include "lr.h"
#include "cfa.h"
#include "msr.h"
#include "redblack.h"
#include "fsbuddy.h"
#include "snmctdfs.h"
#include "snmccons.h"
#include "fssnmp.h"
#include "iss.h"
#ifdef L2RED_WANTED
#include "rmgr.h"
#include "pnac.h"
#include "la.h"
#include "fsvlan.h"
#include "rstp.h"
#endif

#ifdef MBSM_WANTED
#include "mbsm.h"
#endif/* MBSM_WANTED */

#include "eoam.h"
#include "emdefn.h"
#include "emmacs.h"
#include "emtdfs.h"
#include "emtrc.h"
#include "stdeoalw.h" 
#include "eoamexlw.h" 
#include "emproto.h"
#include "emlmtdfs.h"
#include "emlmprot.h"
#include "emtrap.h"
#include "emsz.h"
#ifdef TRACE_WANTED
#include "emtrc.h"
#endif

#ifdef L2RED_WANTED
#include "emred.h"
#else
#include "emredsb.h"
#endif

#include "emextn.h"

#ifdef NPAPI_WANTED
#include "npapi.h"
#include "eoamnp.h"
#include "nputil.h"
#endif

#ifdef SNMP_2_WANTED
#include "stdeoawr.h"
#include "eoamexwr.h"
#endif
#include "snmputil.h"
#endif /* _EMINC_H */
/*-----------------------------------------------------------------------*/
/*                       End of the file  eminc.h                        */
/*-----------------------------------------------------------------------*/
