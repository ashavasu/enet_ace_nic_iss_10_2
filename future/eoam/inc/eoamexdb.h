/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: eoamexdb.h,v 1.4 2008/08/20 14:26:57 premap-iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSEOAMDB_H
#define _FSEOAMDB_H


UINT4 fseoam [] ={1,3,6,1,4,1,2076,121};
tSNMP_OID_TYPE fseoamOID = {8, fseoam};


UINT4 FsEoamSystemControl [ ] ={1,3,6,1,4,1,2076,121,1,1};
UINT4 FsEoamModuleStatus [ ] ={1,3,6,1,4,1,2076,121,1,2};
UINT4 FsEoamErrorEventResend [ ] ={1,3,6,1,4,1,2076,121,1,3};
UINT4 FsEoamOui [ ] ={1,3,6,1,4,1,2076,121,1,4};
UINT4 FsEoamTraceOption [ ] ={1,3,6,1,4,1,2076,121,1,5};


tMbDbEntry fseoamMibEntry[]= {

{{10,FsEoamSystemControl}, NULL, FsEoamSystemControlGet, FsEoamSystemControlSet, FsEoamSystemControlTest, FsEoamSystemControlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FsEoamModuleStatus}, NULL, FsEoamModuleStatusGet, FsEoamModuleStatusSet, FsEoamModuleStatusTest, FsEoamModuleStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsEoamErrorEventResend}, NULL, FsEoamErrorEventResendGet, FsEoamErrorEventResendSet, FsEoamErrorEventResendTest, FsEoamErrorEventResendDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "10"},

{{10,FsEoamOui}, NULL, FsEoamOuiGet, FsEoamOuiSet, FsEoamOuiTest, FsEoamOuiDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsEoamTraceOption}, NULL, FsEoamTraceOptionGet, FsEoamTraceOptionSet, FsEoamTraceOptionTest, FsEoamTraceOptionDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "262144"},
};
tMibData fseoamEntry = { 5, fseoamMibEntry };
#endif /* _FSEOAMDB_H */

