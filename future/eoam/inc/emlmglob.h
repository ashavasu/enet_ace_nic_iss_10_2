/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emlmglob.h,v 1.5 2010/09/20 10:09:13 prabuc Exp $
 * 
 * Description: This file contains global variables used in EOAM Link
 *              monitoring module
 *********************************************************************/
#ifndef _EMLMGLOB_H
#define _EMLMGLOB_H

#ifdef EMLMMAIN_C
UINT1 gu1EoamLMStatFlag = EOAM_LM_DISABLED;
#else
PUBLIC UINT1 gu1EoamLMStatFlag;
#endif /* EMLMMAIN_C */

#endif /* _EMLMGLOB_H*/
/*-----------------------------------------------------------------------*/
/*                       End of the file  emlmglob.h                     */
/*-----------------------------------------------------------------------*/
