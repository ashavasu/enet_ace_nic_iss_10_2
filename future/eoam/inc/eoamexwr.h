/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: eoamexwr.h,v 1.4 2008/08/20 14:26:57 premap-iss Exp $
*
* Description: Proto types for wrapper routines
*********************************************************************/
#ifndef _EOAMEXWR_H
#define _EOAMEXWR_H

VOID RegisterEOAMEX(VOID);
INT4 FsEoamSystemControlGet(tSnmpIndex *, tRetVal *);
INT4 FsEoamModuleStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsEoamErrorEventResendGet(tSnmpIndex *, tRetVal *);
INT4 FsEoamOuiGet(tSnmpIndex *, tRetVal *);
INT4 FsEoamTraceOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsEoamSystemControlSet(tSnmpIndex *, tRetVal *);
INT4 FsEoamModuleStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsEoamErrorEventResendSet(tSnmpIndex *, tRetVal *);
INT4 FsEoamOuiSet(tSnmpIndex *, tRetVal *);
INT4 FsEoamTraceOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsEoamSystemControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEoamModuleStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEoamErrorEventResendTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEoamOuiTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEoamTraceOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEoamSystemControlDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsEoamModuleStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsEoamErrorEventResendDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsEoamOuiDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsEoamTraceOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);





#endif /* _FSEOAMWR_H */
