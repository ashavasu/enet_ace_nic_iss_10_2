/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emtrc.h,v 1.7 2012/05/30 06:22:59 siva Exp $
 *
 * Description:This file contains procedures and definitions 
 *             used for debugging.
 *******************************************************************/
#ifndef _EMTRC_H_
#define _EMTRC_H_

#define  EOAM_TRC_FLAG  gEoamGlobalInfo.u4TraceOption
#define  EOAM_NAME      "EOAM"               

#define EOAM_CLR_TRC_OPT       0

#define EOAM_DEF_TRC_OPT       EOAM_CRITICAL_TRC
#define EOAM_MIN_TRC_VAL       INIT_SHUT_TRC
#define EOAM_MAX_TRC_VAL       EOAM_ALL_TRC

#ifdef TRACE_WANTED

#define  EOAM_TRC(x)       EoamTrcPrint( __FILE__, __LINE__, EoamTrc x)

#ifdef __GNUC__
#define  EOAM_TRC_FN_ENTRY()  \
    if (EOAM_TRC_FLAG & EOAM_FN_ENTRY_TRC) \
    {\
        EoamTrcPrint( __FILE__, __LINE__, EoamTrcConcat("Entered", __FUNCTION__ ));\
    }

#define  EOAM_TRC_FN_EXIT()   \
    if (EOAM_TRC_FLAG & EOAM_FN_EXIT_TRC) \
    {\
        EoamTrcPrint( __FILE__, __LINE__, EoamTrcConcat("Exiting", __FUNCTION__ ));\
    }
#else /* __GNUC__ */
#define  EOAM_TRC_FN_ENTRY()  \
    if (EOAM_TRC_FLAG & EOAM_FN_ENTRY_TRC) \
    {\
        EoamTrcPrint( __FILE__, __LINE__, EoamTrcConcat("Entered", __FUNCTION__ ));\
    }

#define  EOAM_TRC_FN_EXIT()   
#endif /* __GNUC__ */

#define EOAM_PKT_DUMP(TraceType, pBuf, Length, Str)                       \
        MOD_PKT_DUMP(EOAM_TRC_FLAG, TraceType, EOAM_NAME, pBuf, Length,   \
                                          (const char *)Str)

#else /* TRACE_WANTED */

#define  EOAM_TRC(x) 
#define  EOAM_TRC_FN_ENTRY()
#define  EOAM_TRC_FN_EXIT()
#define  EOAM_PKT_DUMP(TraceType, pBuf, Length, Str)

#endif /* TRACE_WANTED */


#endif /* _EMTRC_H */
/*-----------------------------------------------------------------------*/
/*                       End of the file  emtrc.h                        */
/*-----------------------------------------------------------------------*/
