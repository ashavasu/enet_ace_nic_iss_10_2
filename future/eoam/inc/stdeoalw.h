/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdeoalw.h,v 1.3 2008/08/20 14:26:57 premap-iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for Dot3OamTable. */
INT1
nmhValidateIndexInstanceDot3OamTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot3OamTable  */

INT1
nmhGetFirstIndexDot3OamTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot3OamTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot3OamAdminState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot3OamOperStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot3OamMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot3OamMaxOamPduSize ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3OamConfigRevision ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3OamFunctionsSupported ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot3OamAdminState ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot3OamMode ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot3OamAdminState ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot3OamMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot3OamTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot3OamPeerTable. */
INT1
nmhValidateIndexInstanceDot3OamPeerTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot3OamPeerTable  */

INT1
nmhGetFirstIndexDot3OamPeerTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot3OamPeerTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot3OamPeerMacAddress ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetDot3OamPeerVendorOui ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot3OamPeerVendorInfo ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3OamPeerMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot3OamPeerMaxOamPduSize ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3OamPeerConfigRevision ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3OamPeerFunctionsSupported ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for Dot3OamLoopbackTable. */
INT1
nmhValidateIndexInstanceDot3OamLoopbackTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot3OamLoopbackTable  */

INT1
nmhGetFirstIndexDot3OamLoopbackTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot3OamLoopbackTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot3OamLoopbackStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot3OamLoopbackIgnoreRx ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot3OamLoopbackStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot3OamLoopbackIgnoreRx ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot3OamLoopbackStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot3OamLoopbackIgnoreRx ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot3OamLoopbackTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot3OamStatsTable. */
INT1
nmhValidateIndexInstanceDot3OamStatsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot3OamStatsTable  */

INT1
nmhGetFirstIndexDot3OamStatsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot3OamStatsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot3OamInformationTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3OamInformationRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3OamUniqueEventNotificationTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3OamUniqueEventNotificationRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3OamDuplicateEventNotificationTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3OamDuplicateEventNotificationRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3OamLoopbackControlTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3OamLoopbackControlRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3OamVariableRequestTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3OamVariableRequestRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3OamVariableResponseTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3OamVariableResponseRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3OamOrgSpecificTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3OamOrgSpecificRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3OamUnsupportedCodesTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3OamUnsupportedCodesRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3OamFramesLostDueToOam ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for Dot3OamEventConfigTable. */
INT1
nmhValidateIndexInstanceDot3OamEventConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot3OamEventConfigTable  */

INT1
nmhGetFirstIndexDot3OamEventConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot3OamEventConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot3OamErrSymPeriodWindowHi ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3OamErrSymPeriodWindowLo ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3OamErrSymPeriodThresholdHi ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3OamErrSymPeriodThresholdLo ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3OamErrSymPeriodEvNotifEnable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot3OamErrFramePeriodWindow ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3OamErrFramePeriodThreshold ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3OamErrFramePeriodEvNotifEnable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot3OamErrFrameWindow ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3OamErrFrameThreshold ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3OamErrFrameEvNotifEnable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot3OamErrFrameSecsSummaryWindow ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot3OamErrFrameSecsSummaryThreshold ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot3OamErrFrameSecsEvNotifEnable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot3OamDyingGaspEnable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot3OamCriticalEventEnable ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot3OamErrSymPeriodWindowHi ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot3OamErrSymPeriodWindowLo ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot3OamErrSymPeriodThresholdHi ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot3OamErrSymPeriodThresholdLo ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot3OamErrSymPeriodEvNotifEnable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot3OamErrFramePeriodWindow ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot3OamErrFramePeriodThreshold ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot3OamErrFramePeriodEvNotifEnable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot3OamErrFrameWindow ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot3OamErrFrameThreshold ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot3OamErrFrameEvNotifEnable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot3OamErrFrameSecsSummaryWindow ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot3OamErrFrameSecsSummaryThreshold ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot3OamErrFrameSecsEvNotifEnable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot3OamDyingGaspEnable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot3OamCriticalEventEnable ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot3OamErrSymPeriodWindowHi ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot3OamErrSymPeriodWindowLo ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot3OamErrSymPeriodThresholdHi ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot3OamErrSymPeriodThresholdLo ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot3OamErrSymPeriodEvNotifEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot3OamErrFramePeriodWindow ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot3OamErrFramePeriodThreshold ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot3OamErrFramePeriodEvNotifEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot3OamErrFrameWindow ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot3OamErrFrameThreshold ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot3OamErrFrameEvNotifEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot3OamErrFrameSecsSummaryWindow ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot3OamErrFrameSecsSummaryThreshold ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot3OamErrFrameSecsEvNotifEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot3OamDyingGaspEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot3OamCriticalEventEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot3OamEventConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot3OamEventLogTable. */
INT1
nmhValidateIndexInstanceDot3OamEventLogTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot3OamEventLogTable  */

INT1
nmhGetFirstIndexDot3OamEventLogTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot3OamEventLogTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot3OamEventLogTimestamp ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot3OamEventLogOui ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot3OamEventLogType ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot3OamEventLogLocation ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetDot3OamEventLogWindowHi ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot3OamEventLogWindowLo ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot3OamEventLogThresholdHi ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot3OamEventLogThresholdLo ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot3OamEventLogValue ARG_LIST((INT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDot3OamEventLogRunningTotal ARG_LIST((INT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDot3OamEventLogEventTotal ARG_LIST((INT4  , UINT4 ,UINT4 *));
