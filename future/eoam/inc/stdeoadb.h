/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdeoadb.h,v 1.3 2008/08/20 14:26:57 premap-iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDEOADB_H
#define _STDEOADB_H

UINT1 Dot3OamTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot3OamPeerTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot3OamLoopbackTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot3OamStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot3OamEventConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot3OamEventLogTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 stdeoa [] ={1,3,6,1,2,1,158};
tSNMP_OID_TYPE stdeoaOID = {7, stdeoa};


UINT4 Dot3OamAdminState [ ] ={1,3,6,1,2,1,158,1,1,1,1};
UINT4 Dot3OamOperStatus [ ] ={1,3,6,1,2,1,158,1,1,1,2};
UINT4 Dot3OamMode [ ] ={1,3,6,1,2,1,158,1,1,1,3};
UINT4 Dot3OamMaxOamPduSize [ ] ={1,3,6,1,2,1,158,1,1,1,4};
UINT4 Dot3OamConfigRevision [ ] ={1,3,6,1,2,1,158,1,1,1,5};
UINT4 Dot3OamFunctionsSupported [ ] ={1,3,6,1,2,1,158,1,1,1,6};
UINT4 Dot3OamPeerMacAddress [ ] ={1,3,6,1,2,1,158,1,2,1,1};
UINT4 Dot3OamPeerVendorOui [ ] ={1,3,6,1,2,1,158,1,2,1,2};
UINT4 Dot3OamPeerVendorInfo [ ] ={1,3,6,1,2,1,158,1,2,1,3};
UINT4 Dot3OamPeerMode [ ] ={1,3,6,1,2,1,158,1,2,1,4};
UINT4 Dot3OamPeerMaxOamPduSize [ ] ={1,3,6,1,2,1,158,1,2,1,5};
UINT4 Dot3OamPeerConfigRevision [ ] ={1,3,6,1,2,1,158,1,2,1,6};
UINT4 Dot3OamPeerFunctionsSupported [ ] ={1,3,6,1,2,1,158,1,2,1,7};
UINT4 Dot3OamLoopbackStatus [ ] ={1,3,6,1,2,1,158,1,3,1,1};
UINT4 Dot3OamLoopbackIgnoreRx [ ] ={1,3,6,1,2,1,158,1,3,1,2};
UINT4 Dot3OamInformationTx [ ] ={1,3,6,1,2,1,158,1,4,1,1};
UINT4 Dot3OamInformationRx [ ] ={1,3,6,1,2,1,158,1,4,1,2};
UINT4 Dot3OamUniqueEventNotificationTx [ ] ={1,3,6,1,2,1,158,1,4,1,3};
UINT4 Dot3OamUniqueEventNotificationRx [ ] ={1,3,6,1,2,1,158,1,4,1,4};
UINT4 Dot3OamDuplicateEventNotificationTx [ ] ={1,3,6,1,2,1,158,1,4,1,5};
UINT4 Dot3OamDuplicateEventNotificationRx [ ] ={1,3,6,1,2,1,158,1,4,1,6};
UINT4 Dot3OamLoopbackControlTx [ ] ={1,3,6,1,2,1,158,1,4,1,7};
UINT4 Dot3OamLoopbackControlRx [ ] ={1,3,6,1,2,1,158,1,4,1,8};
UINT4 Dot3OamVariableRequestTx [ ] ={1,3,6,1,2,1,158,1,4,1,9};
UINT4 Dot3OamVariableRequestRx [ ] ={1,3,6,1,2,1,158,1,4,1,10};
UINT4 Dot3OamVariableResponseTx [ ] ={1,3,6,1,2,1,158,1,4,1,11};
UINT4 Dot3OamVariableResponseRx [ ] ={1,3,6,1,2,1,158,1,4,1,12};
UINT4 Dot3OamOrgSpecificTx [ ] ={1,3,6,1,2,1,158,1,4,1,13};
UINT4 Dot3OamOrgSpecificRx [ ] ={1,3,6,1,2,1,158,1,4,1,14};
UINT4 Dot3OamUnsupportedCodesTx [ ] ={1,3,6,1,2,1,158,1,4,1,15};
UINT4 Dot3OamUnsupportedCodesRx [ ] ={1,3,6,1,2,1,158,1,4,1,16};
UINT4 Dot3OamFramesLostDueToOam [ ] ={1,3,6,1,2,1,158,1,4,1,17};
UINT4 Dot3OamErrSymPeriodWindowHi [ ] ={1,3,6,1,2,1,158,1,5,1,1};
UINT4 Dot3OamErrSymPeriodWindowLo [ ] ={1,3,6,1,2,1,158,1,5,1,2};
UINT4 Dot3OamErrSymPeriodThresholdHi [ ] ={1,3,6,1,2,1,158,1,5,1,3};
UINT4 Dot3OamErrSymPeriodThresholdLo [ ] ={1,3,6,1,2,1,158,1,5,1,4};
UINT4 Dot3OamErrSymPeriodEvNotifEnable [ ] ={1,3,6,1,2,1,158,1,5,1,5};
UINT4 Dot3OamErrFramePeriodWindow [ ] ={1,3,6,1,2,1,158,1,5,1,6};
UINT4 Dot3OamErrFramePeriodThreshold [ ] ={1,3,6,1,2,1,158,1,5,1,7};
UINT4 Dot3OamErrFramePeriodEvNotifEnable [ ] ={1,3,6,1,2,1,158,1,5,1,8};
UINT4 Dot3OamErrFrameWindow [ ] ={1,3,6,1,2,1,158,1,5,1,9};
UINT4 Dot3OamErrFrameThreshold [ ] ={1,3,6,1,2,1,158,1,5,1,10};
UINT4 Dot3OamErrFrameEvNotifEnable [ ] ={1,3,6,1,2,1,158,1,5,1,11};
UINT4 Dot3OamErrFrameSecsSummaryWindow [ ] ={1,3,6,1,2,1,158,1,5,1,12};
UINT4 Dot3OamErrFrameSecsSummaryThreshold [ ] ={1,3,6,1,2,1,158,1,5,1,13};
UINT4 Dot3OamErrFrameSecsEvNotifEnable [ ] ={1,3,6,1,2,1,158,1,5,1,14};
UINT4 Dot3OamDyingGaspEnable [ ] ={1,3,6,1,2,1,158,1,5,1,15};
UINT4 Dot3OamCriticalEventEnable [ ] ={1,3,6,1,2,1,158,1,5,1,16};
UINT4 Dot3OamEventLogIndex [ ] ={1,3,6,1,2,1,158,1,6,1,1};
UINT4 Dot3OamEventLogTimestamp [ ] ={1,3,6,1,2,1,158,1,6,1,2};
UINT4 Dot3OamEventLogOui [ ] ={1,3,6,1,2,1,158,1,6,1,3};
UINT4 Dot3OamEventLogType [ ] ={1,3,6,1,2,1,158,1,6,1,4};
UINT4 Dot3OamEventLogLocation [ ] ={1,3,6,1,2,1,158,1,6,1,5};
UINT4 Dot3OamEventLogWindowHi [ ] ={1,3,6,1,2,1,158,1,6,1,6};
UINT4 Dot3OamEventLogWindowLo [ ] ={1,3,6,1,2,1,158,1,6,1,7};
UINT4 Dot3OamEventLogThresholdHi [ ] ={1,3,6,1,2,1,158,1,6,1,8};
UINT4 Dot3OamEventLogThresholdLo [ ] ={1,3,6,1,2,1,158,1,6,1,9};
UINT4 Dot3OamEventLogValue [ ] ={1,3,6,1,2,1,158,1,6,1,10};
UINT4 Dot3OamEventLogRunningTotal [ ] ={1,3,6,1,2,1,158,1,6,1,11};
UINT4 Dot3OamEventLogEventTotal [ ] ={1,3,6,1,2,1,158,1,6,1,12};


tMbDbEntry stdeoaMibEntry[]= {

{{11,Dot3OamAdminState}, GetNextIndexDot3OamTable, Dot3OamAdminStateGet, Dot3OamAdminStateSet, Dot3OamAdminStateTest, Dot3OamTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot3OamTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamOperStatus}, GetNextIndexDot3OamTable, Dot3OamOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot3OamTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamMode}, GetNextIndexDot3OamTable, Dot3OamModeGet, Dot3OamModeSet, Dot3OamModeTest, Dot3OamTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot3OamTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamMaxOamPduSize}, GetNextIndexDot3OamTable, Dot3OamMaxOamPduSizeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot3OamTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamConfigRevision}, GetNextIndexDot3OamTable, Dot3OamConfigRevisionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot3OamTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamFunctionsSupported}, GetNextIndexDot3OamTable, Dot3OamFunctionsSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot3OamTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamPeerMacAddress}, GetNextIndexDot3OamPeerTable, Dot3OamPeerMacAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot3OamPeerTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamPeerVendorOui}, GetNextIndexDot3OamPeerTable, Dot3OamPeerVendorOuiGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot3OamPeerTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamPeerVendorInfo}, GetNextIndexDot3OamPeerTable, Dot3OamPeerVendorInfoGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot3OamPeerTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamPeerMode}, GetNextIndexDot3OamPeerTable, Dot3OamPeerModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot3OamPeerTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamPeerMaxOamPduSize}, GetNextIndexDot3OamPeerTable, Dot3OamPeerMaxOamPduSizeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot3OamPeerTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamPeerConfigRevision}, GetNextIndexDot3OamPeerTable, Dot3OamPeerConfigRevisionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot3OamPeerTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamPeerFunctionsSupported}, GetNextIndexDot3OamPeerTable, Dot3OamPeerFunctionsSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot3OamPeerTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamLoopbackStatus}, GetNextIndexDot3OamLoopbackTable, Dot3OamLoopbackStatusGet, Dot3OamLoopbackStatusSet, Dot3OamLoopbackStatusTest, Dot3OamLoopbackTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot3OamLoopbackTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamLoopbackIgnoreRx}, GetNextIndexDot3OamLoopbackTable, Dot3OamLoopbackIgnoreRxGet, Dot3OamLoopbackIgnoreRxSet, Dot3OamLoopbackIgnoreRxTest, Dot3OamLoopbackTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot3OamLoopbackTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamInformationTx}, GetNextIndexDot3OamStatsTable, Dot3OamInformationTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3OamStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamInformationRx}, GetNextIndexDot3OamStatsTable, Dot3OamInformationRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3OamStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamUniqueEventNotificationTx}, GetNextIndexDot3OamStatsTable, Dot3OamUniqueEventNotificationTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3OamStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamUniqueEventNotificationRx}, GetNextIndexDot3OamStatsTable, Dot3OamUniqueEventNotificationRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3OamStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamDuplicateEventNotificationTx}, GetNextIndexDot3OamStatsTable, Dot3OamDuplicateEventNotificationTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3OamStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamDuplicateEventNotificationRx}, GetNextIndexDot3OamStatsTable, Dot3OamDuplicateEventNotificationRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3OamStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamLoopbackControlTx}, GetNextIndexDot3OamStatsTable, Dot3OamLoopbackControlTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3OamStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamLoopbackControlRx}, GetNextIndexDot3OamStatsTable, Dot3OamLoopbackControlRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3OamStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamVariableRequestTx}, GetNextIndexDot3OamStatsTable, Dot3OamVariableRequestTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3OamStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamVariableRequestRx}, GetNextIndexDot3OamStatsTable, Dot3OamVariableRequestRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3OamStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamVariableResponseTx}, GetNextIndexDot3OamStatsTable, Dot3OamVariableResponseTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3OamStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamVariableResponseRx}, GetNextIndexDot3OamStatsTable, Dot3OamVariableResponseRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3OamStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamOrgSpecificTx}, GetNextIndexDot3OamStatsTable, Dot3OamOrgSpecificTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3OamStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamOrgSpecificRx}, GetNextIndexDot3OamStatsTable, Dot3OamOrgSpecificRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3OamStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamUnsupportedCodesTx}, GetNextIndexDot3OamStatsTable, Dot3OamUnsupportedCodesTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3OamStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamUnsupportedCodesRx}, GetNextIndexDot3OamStatsTable, Dot3OamUnsupportedCodesRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3OamStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamFramesLostDueToOam}, GetNextIndexDot3OamStatsTable, Dot3OamFramesLostDueToOamGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3OamStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamErrSymPeriodWindowHi}, GetNextIndexDot3OamEventConfigTable, Dot3OamErrSymPeriodWindowHiGet, Dot3OamErrSymPeriodWindowHiSet, Dot3OamErrSymPeriodWindowHiTest, Dot3OamEventConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot3OamEventConfigTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamErrSymPeriodWindowLo}, GetNextIndexDot3OamEventConfigTable, Dot3OamErrSymPeriodWindowLoGet, Dot3OamErrSymPeriodWindowLoSet, Dot3OamErrSymPeriodWindowLoTest, Dot3OamEventConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot3OamEventConfigTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamErrSymPeriodThresholdHi}, GetNextIndexDot3OamEventConfigTable, Dot3OamErrSymPeriodThresholdHiGet, Dot3OamErrSymPeriodThresholdHiSet, Dot3OamErrSymPeriodThresholdHiTest, Dot3OamEventConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot3OamEventConfigTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamErrSymPeriodThresholdLo}, GetNextIndexDot3OamEventConfigTable, Dot3OamErrSymPeriodThresholdLoGet, Dot3OamErrSymPeriodThresholdLoSet, Dot3OamErrSymPeriodThresholdLoTest, Dot3OamEventConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot3OamEventConfigTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamErrSymPeriodEvNotifEnable}, GetNextIndexDot3OamEventConfigTable, Dot3OamErrSymPeriodEvNotifEnableGet, Dot3OamErrSymPeriodEvNotifEnableSet, Dot3OamErrSymPeriodEvNotifEnableTest, Dot3OamEventConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot3OamEventConfigTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamErrFramePeriodWindow}, GetNextIndexDot3OamEventConfigTable, Dot3OamErrFramePeriodWindowGet, Dot3OamErrFramePeriodWindowSet, Dot3OamErrFramePeriodWindowTest, Dot3OamEventConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot3OamEventConfigTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamErrFramePeriodThreshold}, GetNextIndexDot3OamEventConfigTable, Dot3OamErrFramePeriodThresholdGet, Dot3OamErrFramePeriodThresholdSet, Dot3OamErrFramePeriodThresholdTest, Dot3OamEventConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot3OamEventConfigTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamErrFramePeriodEvNotifEnable}, GetNextIndexDot3OamEventConfigTable, Dot3OamErrFramePeriodEvNotifEnableGet, Dot3OamErrFramePeriodEvNotifEnableSet, Dot3OamErrFramePeriodEvNotifEnableTest, Dot3OamEventConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot3OamEventConfigTableINDEX, 1, 0, 0, NULL},

{{11,Dot3OamErrFrameWindow}, GetNextIndexDot3OamEventConfigTable, Dot3OamErrFrameWindowGet, Dot3OamErrFrameWindowSet, Dot3OamErrFrameWindowTest, Dot3OamEventConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot3OamEventConfigTableINDEX, 1, 0, 0, "10"},

{{11,Dot3OamErrFrameThreshold}, GetNextIndexDot3OamEventConfigTable, Dot3OamErrFrameThresholdGet, Dot3OamErrFrameThresholdSet, Dot3OamErrFrameThresholdTest, Dot3OamEventConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot3OamEventConfigTableINDEX, 1, 0, 0, "1"},

{{11,Dot3OamErrFrameEvNotifEnable}, GetNextIndexDot3OamEventConfigTable, Dot3OamErrFrameEvNotifEnableGet, Dot3OamErrFrameEvNotifEnableSet, Dot3OamErrFrameEvNotifEnableTest, Dot3OamEventConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot3OamEventConfigTableINDEX, 1, 0, 0, "1"},

{{11,Dot3OamErrFrameSecsSummaryWindow}, GetNextIndexDot3OamEventConfigTable, Dot3OamErrFrameSecsSummaryWindowGet, Dot3OamErrFrameSecsSummaryWindowSet, Dot3OamErrFrameSecsSummaryWindowTest, Dot3OamEventConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot3OamEventConfigTableINDEX, 1, 0, 0, "100"},

{{11,Dot3OamErrFrameSecsSummaryThreshold}, GetNextIndexDot3OamEventConfigTable, Dot3OamErrFrameSecsSummaryThresholdGet, Dot3OamErrFrameSecsSummaryThresholdSet, Dot3OamErrFrameSecsSummaryThresholdTest, Dot3OamEventConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot3OamEventConfigTableINDEX, 1, 0, 0, "1"},

{{11,Dot3OamErrFrameSecsEvNotifEnable}, GetNextIndexDot3OamEventConfigTable, Dot3OamErrFrameSecsEvNotifEnableGet, Dot3OamErrFrameSecsEvNotifEnableSet, Dot3OamErrFrameSecsEvNotifEnableTest, Dot3OamEventConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot3OamEventConfigTableINDEX, 1, 0, 0, "1"},

{{11,Dot3OamDyingGaspEnable}, GetNextIndexDot3OamEventConfigTable, Dot3OamDyingGaspEnableGet, Dot3OamDyingGaspEnableSet, Dot3OamDyingGaspEnableTest, Dot3OamEventConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot3OamEventConfigTableINDEX, 1, 0, 0, "1"},

{{11,Dot3OamCriticalEventEnable}, GetNextIndexDot3OamEventConfigTable, Dot3OamCriticalEventEnableGet, Dot3OamCriticalEventEnableSet, Dot3OamCriticalEventEnableTest, Dot3OamEventConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot3OamEventConfigTableINDEX, 1, 0, 0, "1"},

{{11,Dot3OamEventLogIndex}, GetNextIndexDot3OamEventLogTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot3OamEventLogTableINDEX, 2, 0, 0, NULL},

{{11,Dot3OamEventLogTimestamp}, GetNextIndexDot3OamEventLogTable, Dot3OamEventLogTimestampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, Dot3OamEventLogTableINDEX, 2, 0, 0, NULL},

{{11,Dot3OamEventLogOui}, GetNextIndexDot3OamEventLogTable, Dot3OamEventLogOuiGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot3OamEventLogTableINDEX, 2, 0, 0, NULL},

{{11,Dot3OamEventLogType}, GetNextIndexDot3OamEventLogTable, Dot3OamEventLogTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot3OamEventLogTableINDEX, 2, 0, 0, NULL},

{{11,Dot3OamEventLogLocation}, GetNextIndexDot3OamEventLogTable, Dot3OamEventLogLocationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot3OamEventLogTableINDEX, 2, 0, 0, NULL},

{{11,Dot3OamEventLogWindowHi}, GetNextIndexDot3OamEventLogTable, Dot3OamEventLogWindowHiGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot3OamEventLogTableINDEX, 2, 0, 0, NULL},

{{11,Dot3OamEventLogWindowLo}, GetNextIndexDot3OamEventLogTable, Dot3OamEventLogWindowLoGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot3OamEventLogTableINDEX, 2, 0, 0, NULL},

{{11,Dot3OamEventLogThresholdHi}, GetNextIndexDot3OamEventLogTable, Dot3OamEventLogThresholdHiGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot3OamEventLogTableINDEX, 2, 0, 0, NULL},

{{11,Dot3OamEventLogThresholdLo}, GetNextIndexDot3OamEventLogTable, Dot3OamEventLogThresholdLoGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot3OamEventLogTableINDEX, 2, 0, 0, NULL},

{{11,Dot3OamEventLogValue}, GetNextIndexDot3OamEventLogTable, Dot3OamEventLogValueGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Dot3OamEventLogTableINDEX, 2, 0, 0, NULL},

{{11,Dot3OamEventLogRunningTotal}, GetNextIndexDot3OamEventLogTable, Dot3OamEventLogRunningTotalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Dot3OamEventLogTableINDEX, 2, 0, 0, NULL},

{{11,Dot3OamEventLogEventTotal}, GetNextIndexDot3OamEventLogTable, Dot3OamEventLogEventTotalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot3OamEventLogTableINDEX, 2, 0, 0, NULL},
};
tMibData stdeoaEntry = { 60, stdeoaMibEntry };
#endif /* _STDEOADB_H */

