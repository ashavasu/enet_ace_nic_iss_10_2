/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emdefn.h,v 1.20 2017/08/31 14:02:00 siva Exp $
 * 
 * Description: This file contains macros definitions for constants
 *              used in EOAM.
 *********************************************************************/
#ifndef _EMDEFN_H
#define _EMDEFN_H

/****************************************************************************/
/* Sizable parameters                                                       */
/****************************************************************************/
/* Number of times an error event OAMPDU sent repeatedly on occurence of 
 * an error event */
#define EOAM_ERR_EVT_RESEND_COUNT          10
/* Number of times a critical event notification sent repeatedly on occurence
 * of critical event */
#define EOAM_CRITICAL_EVT_RESEND_CNT       20
/* Max number of Variable Requests handled per port */
#define EOAM_MAX_VAR_REQS                  10
/* Depth of EOAM Msg Q: 
 * When EOAM is enabled on all interfaces, all the interfaces can 
 * send EOAM INFO PDU at the same time and hence the minimum size of Q 
 * should be EOAM_MAX_INTERFACES. The interfaces can also send req
 * PDUs apart from INFO PDU. Assuming that all the interfaces sends
 * the req PDU immediately after the INFO PDU. Hence the queue size is 
 * 2*EOAM_MAX_PORTS
 */
#define EOAM_QUEUE_DEPTH                   (2 * EOAM_MAX_PORTS)  
/* Max number of Log entries stored for Event Log table */
#define EOAM_MAX_LOG_ENTRIES               10

/****************************************************************************/
/* General                                                                  */
/****************************************************************************/
#define EOAM_TASK_NAME                ((const UINT1 *)"EOAT")
#define EOAM_QUEUE_NAME               "EOAQ"
#define EOAM_PROTOCOL_SEM             "EOAP"
#define EOAM_SYNC_SEM                 "ESYN"

/* EOAM Version */
#define EOAM_VERSION                  0x01

/* Invalid value */
#define EOAM_INVALID_VALUE            -1

/* Events that will be received by EOAM Task */
#define EOAM_TMR_EXPIRY_EVENT            0x01 /* Bit 0 */
#define EOAM_QMSG_EVENT                  0x02 /* Bit 1 */
#define EOAM_RED_SUB_BULK_UPD_EVENT          0x04 /* Bit 2 */
/* OAMPDU Flag field bit masks */
#define EOAM_MODE_BITMASK                0x01
#define EOAM_FNS_SUPPORTED_BITMASK       0x0f
#define EOAM_PDU_SIZE_BITMASK            0x07ff
#define EOAM_MUX_STATE_BITMASK           0x04
#define EOAM_PAR_STATE_BITMASK           0x03

#define EOAM_MUX_FWD_STATE               0
#define EOAM_MUX_DISCARD_STATE           0x4

#define EOAM_PAR_FWD_STATE               0
#define EOAM_PAR_LB_STATE                1
#define EOAM_PAR_DISCARD_STATE           2



/* Info TLV State Field Mask */
#define EOAM_STATE_MUX_ACTION         0x04
#define EOAM_STATE_PARSER_DISCARD     0x02
#define EOAM_STATE_PARSER_LB          0x01

/* Max number of OAMPDUs that can be transmitted per sec */
#define EOAM_MAX_PDU_PER_SEC          10
 
/* Error Event Configuration default value */
#define EOAM_DEF_CONFIG_REVISION      0

/* Default Window and Threshold values for Link Monitoring Events */
#define EOAM_DEF_SYMBOL_PERIOD_WINDOW_HI   0
#define EOAM_DEF_SYMBOL_PERIOD_THRESH_HI   0
#define EOAM_DEF_SYMBOL_PERIOD_WINDOW_LO   625000000   /* 625 millions */ 
#define EOAM_DEF_SYMBOL_PERIOD_THRESH_LO   1
#define EOAM_DEF_FRAME_PERIOD_WINDOW       10000000    /* 10 millions */
#define EOAM_DEF_FRAME_PERIOD_THRESH       1
#define EOAM_DEF_FRAME_WINDOW              10
#define EOAM_DEF_FRAME_THRESH              1
#define EOAM_DEF_FRAME_SECS_WINDOW         600         /* As per section 57.5.3.4 in 802.3ah,the default frame seconds summary window is 60 seconds.
                                                          But it is represented in intervals of 100ms. Hence the value is represented as 600 */
#define EOAM_DEF_FRAME_SECS_THRESH         1


/****************************************************************************/
/* Architectural Constants                                                  */
/****************************************************************************/
                                            

/* Min & Max values of Errored Frame Seconds Summary Window 
 * & Threshold*/
#define EOAM_FRAME_SECS_SUM_MIN_WI      100
#define EOAM_FRAME_SECS_SUM_MAX_WI      9000
#define EOAM_FRAME_SECS_SUM_MIN_THRESH  1    
#define EOAM_FRAME_SECS_SUM_MAX_THRESH  900

/* Min & Max values of Errored Frame Window*/
#define EOAM_FRAME_MIN_WI      10
#define EOAM_FRAME_MAX_WI      600

/* Count on number of messages to be allocated from MemPool
 * when max number of interface created at a time and
 * 2 for the buffer */
#define EOAM_QMSG_COUNT               (EOAM_MAX_PORTS + 2)


/* EOAM_ENABLED_PORTS macro can be used to restrict the maximum
 * number of ports on which EOAM can be enabled. At present 
 * It is defined as max number of ports.*/
#define EOAM_ENABLED_PORTS            EOAM_MAX_PORTS 

/* When a single Variable Request has 50 Branch and Leaves in it,
 * the memory will be allocated from pool. To support Variable Request
 * which has more than 50 this value should be changed.*/
#define EOAM_BRANCH_LEAF_COUNT        50

/****************************************************************************/
/* EOAM packet related constants                                            */
/****************************************************************************/
#define EOAM_ALL_EVENTS   (EOAM_TMR_EXPIRY_EVENT |\
                           EOAM_QMSG_EVENT | EOAM_RED_SUB_BULK_UPD_EVENT)    

/* Number of bytes including OAMPDU header, Local and Remote Info TLV.
 * Care should be taken to copy the valid length of the buffer
 * while the transmitting */
#define EOAM_ERR_RESEND_MIN_VAL 1
#define EOAM_ERR_RESEND_MAX_VAL 10

#define EOAM_INVALID_VAL         0xffffffff

/* This buddy is used to allocate memory for incoming variable request,
 * variable response or organization specific packet in the Rx side. In the Tx
 * side, it is used to allocate memory for variable requests / Org Specific 
 * data from FM as well as varible containers from CMIP interface */
#define EOAM_BUDDY_NUM_BLOCKS             50

#define EOAM_VAR_DESCRIPTOR_LENGTH         3     

/* Maximum Number of Descriptors is based on the following formula:
 *  Max PDU data size (1500 - 16) / Size of a descriptor (3) and then
 *  rounded off */
#define EOAM_MAX_DESCRIPTORS         450 
#define EOAM_MIN_BLOCK_SIZE            4
#define EOAM_MAX_DATA_SIZE          1500
#define EOAM_MAX_PDU_SIZE           1518
#define EOAM_MIN_PDU_SIZE              0
#define EOAM_ETH_FCS_LEN               4
#define EOAM_MIN_LOGINDEX              1
#define EOAM_LOCAL_INFO_TLV           0x1
#define EOAM_REMOTE_INFO_TLV          0x2
#define EOAM_ORG_INFO_TLV             0xFE

/*If the variable container exceeds the OAMPDU data size, a variable 
 * indication with value 0x01 (0x80 | 0x01)should be returned */
#define EOAM_IND_CONTAINER_TOO_LONG   0x81

/* Standard PDU/TLV size */
#define EOAM_PDU_HEADER_SIZE                 18
#define EOAM_INFO_TLV_SIZE                   16
#define EOAM_ERR_SYMBOL_PERIOD_TLV_SIZE      40
#define EOAM_ERR_FRAME_TLV_SIZE              26
#define EOAM_ERR_FRAME_PERIOD_TLV_SIZE       28
#define EOAM_ERR_FRAME_SEC_SUMMARY_TLV_SIZE  18 
#define EOAM_VAR_DESC_LEN                    3

#define EOAM_DEST_OFFSET                 0
#define EOAM_SRC_OFFSET                  6
#define EOAM_LEN_TYPE_OFFSET             12
#define EOAM_SUBTYPE_OFFSET              14
#define EOAM_FLAG_OFFSET                 15
#define EOAM_CODE_OFFSET                 17

#define EOAM_PDU_DATA_OFFSET             18
#define EOAM_LOCAL_TLV_LEN_OFFSET        (EOAM_PDU_DATA_OFFSET + 1)
#define EOAM_LOCAL_TLV_VERSION_OFFSET    (EOAM_PDU_DATA_OFFSET + 2)
#define EOAM_LOCAL_TLV_REVISION_OFFSET   (EOAM_PDU_DATA_OFFSET + 3)
#define EOAM_LOCAL_TLV_STATE_OFFSET      (EOAM_PDU_DATA_OFFSET + 5)
#define EOAM_LOCAL_TLV_OAM_CFG_OFFSET    (EOAM_PDU_DATA_OFFSET + 6)
#define EOAM_LOCAL_TLV_PDU_CFG_OFFSET    (EOAM_PDU_DATA_OFFSET + 7)
#define EOAM_LOCAL_OUI_OFFSET            (EOAM_PDU_DATA_OFFSET + 9)
#define EOAM_LOCAL_TLV_VENDOR_OFFSET     (EOAM_PDU_DATA_OFFSET + 12)

/* Trace related constants */
#define EOAM_MAX_LOG_STR_LEN             256

/* EOAM Event TLV related MACROS */
#define EOAM_EVENT_SEQ_NUM_SIZE       2
#define EOAM_EVENT_TLV_TYPE_SIZE      1
#define EOAM_EVENT_TLV_LEN_SIZE       1

/* ERRORED SYMBOL PERIOD EVENT Related Macros */
#define EOAM_EVT_SYM_TIMESTAMP_OFF           2
#define EOAM_EVT_SYM_WIN_HIGH_OFF            4
#define EOAM_EVT_SYM_WIN_LOW_OFF             8
#define EOAM_EVT_SYM_THR_HIGH_OFF            12
#define EOAM_EVT_SYM_THR_LOW_OFF             16
#define EOAM_EVT_SYM_VAL_HIGH_OFF            20
#define EOAM_EVT_SYM_VAL_LOW_OFF             24
#define EOAM_EVT_SYM_TOT_HIGH_OFF            28
#define EOAM_EVT_SYM_TOT_LOW_OFF             32
#define EOAM_EVT_SYM_COUNT_OFF               36

/* ERRORED FRAME EVENT Related Macros */
#define EOAM_EVT_FRAME_TIMESTAMP_OFF         2
#define EOAM_EVT_FRAME_WIN_OFF               4
#define EOAM_EVT_FRAME_THR_OFF               6
#define EOAM_EVT_FRAME_VAL_OFF               10
#define EOAM_EVT_FRAME_TOTHI_OFF             14
#define EOAM_EVT_FRAME_TOTLO_OFF             18
#define EOAM_EVT_FRAME_COUNT_OFF             22

/* ERRORED FRAME PERIOD EVENT Related Macros */
#define EOAM_EVT_FRPER_TIMESTAMP_OFF         2 
#define EOAM_EVT_FRPER_WIN_OFF               4
#define EOAM_EVT_FRPER_THR_OFF               8
#define EOAM_EVT_FRPER_VAL_OFF               12
#define EOAM_EVT_FRPER_TOTHI_OFF             16
#define EOAM_EVT_FRPER_TOTLO_OFF             20
#define EOAM_EVT_FRPER_COUNT_OFF             24

/* ERRORED FRAME SECONDS SUMMARY EVENT Related Macros */
#define EOAM_EVT_SS_TIMESTAMP_OFF            2 
#define EOAM_EVT_SS_WIN_OFF                  4
#define EOAM_EVT_SS_THR_OFF                  6
#define EOAM_EVT_SS_VAL_OFF                  8
#define EOAM_EVT_SS_TOTAL_OFF                10
#define EOAM_EVT_SS_COUNT_OFF                14

/* Organization specific EVENT Related Macros */
#define EOAM_EVT_ORG_SPEC_OUI_OFF            2
#define EOAM_EVT_ORG_SPEC_OUI_LEN            3


#define EOAM_END_TLV_MARKER              0x00


#endif /* _EMDEFN_H */
/*-----------------------------------------------------------------------*/
/*                       End of the file  emdefn.h                       */
/*-----------------------------------------------------------------------*/
