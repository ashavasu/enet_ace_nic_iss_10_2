/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emlmtdfs.h,v 1.3 2007/02/01 14:47:28 iss Exp $
 *
 * Description: This file contains data structure used in EOAM Link
 *              monitoring module
 *********************************************************************/
#ifndef _EMLMTDFS_H
#define _EMLMTDFS_H

typedef struct 
{
    tOsixTaskId   TaskId;
    tOsixSemId    SemId;
    tMemPoolId    LmEnaDllNodePoolId;
    tTMO_DLL      LmEoamEnaPortDllList;
                                     /* Doubly linked list to maintain
                                      * details about OAM enabled ports */
    tTimerListId  LmTmrList; 
    tTmrAppTimer  LmTmrNode;         /* 1sec timer to poll the error 
                                      *  statistics for all ports */
}tEoamLmGlobalInfo;

/* Window and Threshold configuration for the error events.
 * These are just a copy of what is maintained in OAM */
typedef struct _EoamLmEventCfgInfo
{
    FS_UINT8      u8ErrSymPeriodWindow;
    FS_UINT8      u8ErrSymPeriodThreshold;
    UINT4         u4Port;            /* Interface Index */
    UINT4         u4ErrFramePeriodWindow;
    UINT4         u4ErrFramePeriodThreshold;
    UINT4         u4ErrFrameWindow;
    UINT4         u4ErrFrameThreshold;
    UINT4         u4ErrFrameSecsSummaryWindow;
    UINT4         u4ErrFrameSecsSummaryThreshold;
    UINT1         u1ErrSymPeriodEvNotifEnable;
                                    /* Indicates whether Errored Symbol
                                     * Period Event should be monitored */
    UINT1         u1ErrFrameEvNotifEnable;
                                    /* Indicates whether Errored Frame 
                                     * Event should be monitored */
    UINT1         u1ErrFramePeriodEvNotifEnable;
                                    /* Indicates whether Errored Frame 
                                     * Period Event should be monitored */
    UINT1         u1ErrFrameSecsEvNotifEnable;
                                    /* Indicates whether Errored seconds 
                                     * Summary Event should be monitored */
}tEoamLmEventConfigInfo;

typedef struct _EoamLmErrInfo
{
    FS_UINT8      u8CurTotalSymbols;    /* Total number of Symbols received */
    FS_UINT8      u8CurErrorSymbols;    /* Total number of Error Symbols
                                         * received */
    FS_UINT8      u8WinStartTotalSymbols;
                                        /* Number of Symbols rx at the start
                                         * of Errored Symbol window */
    FS_UINT8      u8WinStartErrorSymbols;
                                        /* Number of Error Symbols rx at the
                                         * start of Errored Symbol window */
    UINT4         u4FrameWinStartFrameErrors;
                                        /* Number of Error frames rx at the
                                         * start of Errored Frame window */
    UINT4         u4CurTotalFrames;     /* Current number of frames received -
                                         * updated every 100 ms */
    UINT4         u4CurrentErrorFrames; /* Current number of Error frames rx -
                                         * updated every 100 ms */
    UINT4         u4FPWinStartTotalFrames;
                                        /* Total number of frames rx at the
                                         * start of FP window */
    UINT4         u4FPWinStartFrameErrors;
                                        /* Total number of error frames rx at
                                         * the start of FP window */
    UINT4         u4FrameWinCounter;    /* Number of 100 ms intervals since
                                         * start of Errored Frame window */
    UINT4         u4SecSummFrameErrCount;
                                        /* Total number of frame errors at the
                                         * start of each second */
    UINT4         u4SymErrEventCounter; /* Number of times Symbol error event
                                         * has been generated since reset */
    UINT4         u4FrameEventCounter;  /* Number of times Frame event
                                         * has been generated since reset */

    UINT4         u4FramePerEventCounter;
                                        /* Number of times Frame period event
                                         * has been generated since reset */

    UINT4         u4SecSummEventCounter;
                                        /* Number of times Second summary 
                                           event has been generated since 
                                           reset */
    UINT2         u2SecSummWinCounter;  /* Number of 100 ms intervals since 
                                           start of Errored Sec summ window */
    UINT2         u2SecSummErrSecCounter;
                                        /* Number of errored seconds in the
                                           current window */
    UINT2         u2SecSummRunningCounter;
                                        /* Number of errored seconds in the
                                           since reset */
    BOOL1         b1InitFlag;           /* Flag to indicate start of error 
                                           monitoring on the interface */
    UINT1         u1Pad;
}tEoamLmErrInfo;

typedef struct  _EoamLmEnaPortDllNode
{
    tTMO_DLL_NODE          NodeLink;     /* contains pointers to previous and 
                                            next nodes */
    tEoamLmEventConfigInfo LmEvtCfgInfo; /* Config details for all types of 
                                            events */ 
    tEoamLmErrInfo         LmErrInfo;    /* Error statistics maintained in LM */
}tEoamLmEnaPortDllNode;

#endif /* _EMLMTDFS_H */
/*-----------------------------------------------------------------------*/
/*                       End of the file  emlmtdfs.h                     */
/*-----------------------------------------------------------------------*/
