/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emlmprot.h,v 1.5 2011/09/24 07:01:33 siva Exp $
 * 
 * Description: This file contains prototypes for functions
 *              defined in EOAM Link monitoring module.
 *********************************************************************/
#ifndef _EMLMPROT_H
#define _EMLMPROT_H

/************************** emlmmain.c ***************************/

PUBLIC INT4  EoamLmMainInit PROTO ((VOID));
PUBLIC VOID  EoamLmMainShutdown PROTO ((VOID));

/************************** emlmif.c *****************************/

PUBLIC INT4  EoamLmIfAdd PROTO ((UINT4 ));
PUBLIC INT4  EoamLmIfDelete PROTO ((UINT4 ));
PUBLIC INT4  EoamLmIfCopyEventParams PROTO ((UINT4 ,UINT2 ,UINT1 ,
                                            FS_UINT8 ,FS_UINT8 ));

/************************** emlmpoll.c ***************************/

PUBLIC VOID  EoamLmPollTimerStart PROTO ((VOID));
PUBLIC VOID  EoamLmPollTimerStop PROTO ((VOID));
PUBLIC VOID  EoamLmPollTimerExpiry PROTO ((VOID));

#endif  /* _EMLMPROT_H */   
/*-----------------------------------------------------------------------*/
/*                       End of the file  emlmport.h                     */
/*-----------------------------------------------------------------------*/
