/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emtstpt.h,v 1.5 2011/11/08 13:47:09 siva Exp $
 *
 * Description: This file contains the prototypes for 
 *              EOAM test module
 *********************************************************************/

VOID LmTestSendErrEvent PROTO ((UINT4, UINT1));
VOID LmTestSimulateRollOver PROTO ((UINT4, UINT1, UINT1));
VOID LmTestSimulatePdu PROTO ((tCliHandle, UINT4, UINT1 *));
VOID LmTestSendOrgSpecPDU PROTO ((UINT4, UINT1 *));
VOID LmTestSetMibVarCapab PROTO ((UINT4, UINT1));
VOID LmTestSendOrgSpecEvent PROTO ((UINT4, UINT1 *));


#define EM_LM_TEST_CONFIG_REV 1
#define EM_LM_MAX_U2_VAL      65535
/*--------------------------------------------------------------------------*/
/*                         End of the file emtstpt.h                        */
/*--------------------------------------------------------------------------*/
