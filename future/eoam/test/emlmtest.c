/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: emlmtest.c,v 1.9 2012/10/29 12:02:29 siva Exp $
*
* Description: This file contains the test routines for the EOAM 
*              module. These routies are called directly from the 
*              testcmd.def file
*********************************************************************/
#include "eminc.h"
#include "cli.h"
#include "emtstpt.h"

#ifdef NPAPI_WANTED
PUBLIC UINT1        gu1Increment;
PUBLIC UINT4        gu4FrameRx;
PUBLIC UINT4        gu4FrameErrorRx;
PUBLIC FS_UINT8     gu8SymbolRx;
PUBLIC FS_UINT8     gu8SymbolErrorRx;
#else /* NPAPI_WANTED */
UINT1               gu1Increment = 0;
UINT4               gu4FrameRx;
UINT4               gu4FrameErrorRx;
FS_UINT8            gu8SymbolRx;
FS_UINT8            gu8SymbolErrorRx;
#endif /* NPAPI_WANTED */

#ifdef LINUXSIM_WANTED
/*************************************************************************
*
*  FUNCTION NAME   : LmTestSendErrEvent
*
*  DESCRIPTION     : Sends appropriate error event to LM module
*
*  INPUT           : u4Port - Port Number
*                    u1EventType - Type of the error event
*
*  OUTPUT          : None
*
*  RETURNS         : None
*
**************************************************************************/

VOID
LmTestSendErrEvent (UINT4 u4Port, UINT1 u1EventType)
{
    FS_UINT8            u8Value;
    UINT4               u4Window = 0;
    UINT4               u4Thresh = 0;
    UINT4               u4Hi = 0;
    UINT4               u4Lo = 0;
    tEoamThresEventInfo *pEventInfo = NULL;
    tEoamThresEventInfo ErrEventInfo;

    UNUSED_PARAM (u4Port);
    MEMSET (&ErrEventInfo, 0, sizeof (tEoamThresEventInfo));
    FSAP_U8_CLR (&u8Value);
    pEventInfo = &ErrEventInfo;

    EoamLock ();

    if (nmhValidateIndexInstanceDot3OamEventConfigTable ((INT4) u4Port)
        == SNMP_SUCCESS)
    {
        if (u1EventType == EOAM_ERRORED_SYMBOL_EVENT_TLV)
        {
            gu1Increment = 0;
            nmhGetDot3OamErrSymPeriodWindowHi ((INT4) u4Port, &u4Hi);
            nmhGetDot3OamErrSymPeriodWindowLo ((INT4) u4Port, &u4Lo);
            FSAP_U8_ASSIGN_HI (&u8Value, u4Hi);
            FSAP_U8_ASSIGN_LO (&u8Value, u4Lo);
            FSAP_U8_ADD (&gu8SymbolRx, &gu8SymbolRx, &u8Value);
            nmhGetDot3OamErrSymPeriodThresholdHi ((INT4) u4Port, &u4Hi);
            nmhGetDot3OamErrSymPeriodThresholdLo ((INT4) u4Port, &u4Lo);
            FSAP_U8_ASSIGN_HI (&u8Value, u4Hi);
            FSAP_U8_ASSIGN_LO (&u8Value, u4Lo);
            FSAP_U8_ADD (&gu8SymbolErrorRx, &gu8SymbolErrorRx, &u8Value);
        }
        else if (u1EventType == EOAM_ERRORED_FRAME_EVENT_TLV)
        {
            nmhGetDot3OamErrFrameWindow ((INT4) u4Port, &u4Window);
            nmhGetDot3OamErrFrameThreshold ((INT4) u4Port, &u4Thresh);
            gu1Increment = 0;
            gu4FrameRx += u4Window;
            gu4FrameErrorRx += u4Thresh;
        }
        else if (u1EventType == EOAM_ERRORED_FRAME_PERIOD_EVENT_TLV)
        {
            nmhGetDot3OamErrFramePeriodWindow ((INT4) u4Port, &u4Window);
            nmhGetDot3OamErrFramePeriodThreshold ((INT4) u4Port, &u4Thresh);
            gu1Increment = 0;
            gu4FrameRx += u4Window;
            gu4FrameErrorRx += u4Thresh;
        }
        else if (u1EventType == EOAM_ERRORED_FRAME_SECONDS_EVENT_TLV)
        {
            nmhGetDot3OamErrFrameSecsSummaryWindow ((INT4) u4Port,
                                                    (INT4 *) &u4Window);
            nmhGetDot3OamErrFrameSecsSummaryThreshold ((INT4) u4Port,
                                                       (INT4 *) &u4Thresh);
            gu1Increment = 1;
            gu4FrameRx += u4Window;
            gu4FrameErrorRx += u4Thresh;
        }
    }
    EoamUnLock ();
    return;
}
#endif /* LINUXSIM_WANTED */

/*************************************************************************
*
*  FUNCTION NAME   : LmTestSendOrgSpecEvent
*
*  DESCRIPTION     : Sends org specific event in Event Notification OAMPDU
*
*  INPUT           : u4Port - Port Number
*                    pu1OrgString - Org spec data
*
*  OUTPUT          : None
*
*  RETURNS         : None
*
**************************************************************************/

VOID
LmTestSendOrgSpecEvent (UINT4 u4Port, UINT1 *pu1String)
{
    tEoamExtData        OrgData;
    UINT4               u4StrLen = 0;
    UINT1              *pu1OrgString = NULL;
    UINT1               au1OrgString[EOAM_MAX_DATA_SIZE];

    MEMSET (&OrgData, 0, sizeof (tEoamExtData));
    MEMSET (au1OrgString, 0, EOAM_MAX_DATA_SIZE);

    if (pu1String == NULL)
    {
        return;
    }

    pu1OrgString = au1OrgString;
    u4StrLen = STRLEN (pu1String);

    STRCPY (pu1OrgString, pu1String);

    OrgData.pu1Data = pu1OrgString;
    OrgData.u4DataLen = u4StrLen;

    EoamApiSendOrgSpecEventToPeer (u4Port, &OrgData);
    return;
}

/*************************************************************************
*
*  FUNCTION NAME   : LmTestSimulateRollOver
*
*  DESCRIPTION     : To test the rollover condition of config revision 
*                    and sequence number
*
*  INPUT           : u4Port - Port Number
*                    u1Obj - config revision/sequence number
*                    u1Loc - Local/Remote
*
*  OUTPUT          : None
*
*  RETURNS         : None
*
**************************************************************************/
VOID
LmTestSimulateRollOver (UINT4 u4Port, UINT1 u1Obj, UINT1 u1Loc)
{
    tEoamLocalInfo     *pLocalInfo = NULL;
    tEoamPeerInfo      *pPeerInfo = NULL;
#ifdef LINUXSIM_WANTED
    tEoamEnaPortInfo   *pEnaLocalInfo = NULL;
    tEoamEnaPortInfo   *pEnaPeerInfo = NULL;
#endif

    EoamLock ();
    if ((u1Obj == EM_LM_TEST_CONFIG_REV) &&
        (gEoamGlobalInfo.apEoamPortEntry[u4Port - 1] != NULL))
    {
        if (u1Loc == EOAM_LOCAL_ENTRY)
        {
            pLocalInfo = &(gEoamGlobalInfo.apEoamPortEntry[u4Port - 1]->
                           LocalInfo);
            if (pLocalInfo != NULL)
            {
                pLocalInfo->u2ConfigRevision = EM_LM_MAX_U2_VAL;
                /*Config Rev gets incremented when the mode is changed */
                nmhSetDot3OamMode ((INT4) u4Port, EOAM_MODE_ACTIVE);
            }
        }
        else                    /*Peer Entry */
        {

            pPeerInfo = &(gEoamGlobalInfo.apEoamPortEntry[u4Port - 1]->
                          RemoteInfo);
            if (pPeerInfo != NULL)
            {
                pPeerInfo->u2ConfigRevision = EM_LM_MAX_U2_VAL;
            }
        }
    }
#ifdef LINUXSIM_WANTED
    else if (gEoamGlobalInfo.apEoamPortEntry[u4Port - 1] != NULL)    /*Seq Num */
    {
        if (u1Loc == EOAM_LOCAL_ENTRY)
        {
            pEnaLocalInfo =
                gEoamGlobalInfo.apEoamPortEntry[u4Port - 1]->pEoamEnaPortInfo;
            if (pEnaLocalInfo != NULL)
            {
                pEnaLocalInfo->u2TxSeqNum = EM_LM_MAX_U2_VAL;
                /* Transmit count gets incremented after sending the error
                 * event */
                EoamUnLock ();
                LmTestSendErrEvent (u4Port, EOAM_ERRORED_SYMBOL_EVENT_TLV);
                return;
            }

        }
        else                    /*Peer Entry */
        {
            pEnaPeerInfo =
                gEoamGlobalInfo.apEoamPortEntry[u4Port - 1]->pEoamEnaPortInfo;
            if (pEnaPeerInfo != NULL)
            {
                pEnaPeerInfo->u2RxSeqNum = EM_LM_MAX_U2_VAL;
            }
        }
    }
#endif /* LINUXSIM_WANTED */
    EoamUnLock ();
    return;
}

/*************************************************************************
*
*  FUNCTION NAME   : LmTestSimulatePdu
*
*  DESCRIPTION     : To simulate a PDU of given type
*
*  INPUT           : CliHandle - CliContextID 
*                    u4Port -  Port Number
*                    u1Type -  Code-type
*
*  OUTPUT          : None
*
*  RETURNS         : None
*
**************************************************************************/
VOID
LmTestSimulatePdu (tCliHandle CliHandle, UINT4 u4Port, UINT1 *pu1Type)
{
#define EM_LM_TEST_PKT_SIZE 64
    UINT1               au1Pdu[EM_LM_TEST_PKT_SIZE] = {
        0x01, 0x80, 0xC2, 0x00, 0x00, 0x02,
        /*Dest Mac Addr */
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00,    /* Src Mac Addr */
        0x88, 0x09,                /* Slow Protocol */
        0x03,                    /* Sub type - OAM */
        0x00, 0x50
            /* Flag(Remote discovery complete, Local discovery complete) */
    };
    UINT1               au1Pause[EM_LM_TEST_PKT_SIZE] = {
        0x01, 0x80, 0xC2, 0x00, 0x00, 0x01,
        /* Dest Mac Addr(spanning-tree for (bridges)) */
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00,    /* Src Mac Addr */
        0x88, 0x08,                /* MAC Protocol */
        0x00, 0x01,                /* Pause */
        0x03, 0xe8                /* Quanta */
    };
    tEoamIfInfo         IfInfo;
    tCfaIfInfo          CfaIfInfo;
    tCRU_BUF_CHAIN_HEADER *pMsgBuf = NULL;

    if (STRCMP (pu1Type, "unsupported") == 0)
    {
        au1Pdu[17] = 0x05;        /*Invalid code type (reserved) */
    }
    else if (STRCMP (pu1Type, "unknown_branch") == 0)
    {
        au1Pdu[17] = 0x02;        /* Variable Request */
        au1Pdu[18] = 0x09;        /* UnSupported Attribute */
        au1Pdu[19] = 0x00;        /* Leaf - 0x0001 */
        au1Pdu[20] = 0x01;
    }
    else if (STRCMP (pu1Type, "pause_frame") == 0)    /*pause_frame */
    {
        MEMCPY (au1Pdu, au1Pause, EM_LM_TEST_PKT_SIZE);
    }
    else
    {
        return;
    }
    MEMSET (&IfInfo, 0, sizeof (tEoamIfInfo));
    /* Get Src MAC address from CFA */
    if (EoamGetIfInfo (u4Port, &IfInfo) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "%%Error while getting Sre Mac Addr\n");
        return;
    }

    MEMCPY (&au1Pdu[6], IfInfo.au1MacAddr, MAC_ADDR_LEN);

    if ((pMsgBuf =
         CRU_BUF_Allocate_MsgBufChain (EM_LM_TEST_PKT_SIZE, 0)) == NULL)
    {
        CliPrintf (CliHandle, "Buffer Allocation failed\n");
        return;
    }
    if (CRU_BUF_Copy_OverBufChain (pMsgBuf, au1Pdu, 0, EM_LM_TEST_PKT_SIZE)
        == CRU_FAILURE)
    {
        CliPrintf (CliHandle, "Copy into buffer failed\n");
        return;
    }
    if (CfaGetIfInfo (u4Port, &CfaIfInfo) != CFA_SUCCESS)
    {
        CliPrintf (CliHandle, "CfaGetIfInfo failed\n");
        return;
    }
    if (CfaGddWrite (pMsgBuf, (UINT2) u4Port, EM_LM_TEST_PKT_SIZE, CFA_FALSE,
                     &CfaIfInfo) == CFA_FAILURE)
    {
        /* In both success and failure case of CfaGddWrite, the allocated 
         * memory buffer is released hence the memory is not released 
         * here */
        CliPrintf (CliHandle, "CfaPostPktFromL2 failed\n");
        return;
    }
    pMsgBuf = NULL;
    return;
}

/*************************************************************************
*
*  FUNCTION NAME   : LmTestSendOrgSpecPDU
*
*  DESCRIPTION     : Sends org specific pdu
*
*  INPUT           : u4Port - Port Number
*                    pu1OrgString - Org spec data
*
*  OUTPUT          : None
*
*  RETURNS         : None
*
**************************************************************************/

VOID
LmTestSendOrgSpecPDU (UINT4 u4Port, UINT1 *pu1String)
{
    tEoamExtData        OrgData;
    UINT4               u4StrLen = 0;
    UINT1              *pu1OrgString = NULL;
    UINT1               au1OrgString[EOAM_MAX_DATA_SIZE];

    MEMSET (&OrgData, 0, sizeof (tEoamExtData));
    MEMSET (au1OrgString, 0, EOAM_MAX_DATA_SIZE);

    if (pu1String == NULL)
    {
        return;
    }

    pu1OrgString = au1OrgString;
    u4StrLen = STRLEN (pu1String);

    STRCPY (pu1OrgString, pu1String);

    OrgData.pu1Data = pu1OrgString;
    OrgData.u4DataLen = u4StrLen;

    EoamApiSendOrgSpecPduToPeer (u4Port, &OrgData);
    return;
}

/*************************************************************************
*
*  FUNCTION NAME   : LmTestSetMibVarReqRespCpb
*
*  DESCRIPTION     : Enables or disables the variable retrieval capability
*
*  INPUT           : u4Port - Port Number
*                    u1Status - Enable/Disable
*
*  OUTPUT          : None
*
*  RETURNS         : None
*
**************************************************************************/

VOID
LmTestSetMibVarCapab (UINT4 u4Port, UINT1 u1Status)
{
#define EM_LM_CAPAB_ENABLE 1
    EoamLock ();
    if (gEoamGlobalInfo.apEoamPortEntry[u4Port - 1] != NULL)
    {
        if (u1Status == EM_LM_CAPAB_ENABLE)
        {
            gEoamGlobalInfo.apEoamPortEntry[u4Port - 1]->LocalInfo.
                u1FnsSupportedBmp |= EOAM_VARIABLE_SUPPORT;
        }
        else                    /*Disable */
        {
            gEoamGlobalInfo.apEoamPortEntry[u4Port - 1]->LocalInfo.
                u1FnsSupportedBmp &= ~EOAM_VARIABLE_SUPPORT;
        }
        /* Var Retrieval capability has changed, update info pdu and send */
        if (gEoamGlobalInfo.apEoamPortEntry[u4Port - 1]->pEoamEnaPortInfo
            != NULL)
        {
            gEoamGlobalInfo.apEoamPortEntry[u4Port -
                                            1]->LocalInfo.u2ConfigRevision++;
            EoamCtlTxConstructInfoPdu
                (gEoamGlobalInfo.apEoamPortEntry[u4Port - 1]);
            /* Send Info OAMPDU with Local and Remote TLV */
            EoamCtlTxSendInfoPdu (gEoamGlobalInfo.apEoamPortEntry[u4Port - 1]);
        }
    }
    EoamUnLock ();
    return;
}
