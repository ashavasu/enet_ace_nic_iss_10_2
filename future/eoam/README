Copyright (C) 2006 Aricent Inc . All Rights Reserved
--------------------------------------------------

Introduction
============

Aricent EOAM is a portable implementation of the Ethernet Operation 
Administration and Maintenance (EOAM). It provides complete 
management capabilities using SNMP and CLI.

Aricent EOAM conforms to the 802.3ah-2004(Clause 57).

Contents
========

The contents of this distribution are as follows:

inc/            - Header files
src/            - Source files
Makefile        - Module makefile 
make.h          - This file contains variables required for building the module.
../mibs/        - MIB files
   stdeoam.mib  - EFM OAM MIB from IETF - RFC4878
   fseoamex.mib - Proprietary Mib for EOAM.

The files in the following directories are needed for building the module.
o inc
o LR
o fsap2
o util

Portable Files
==============
src/emstub.c    - Contains lower layer interface functions to be ported when 
                  Aricent CFA is not used. 
                  
                  When Aricent CFA is used, src/emcfaif.c will be used. 
                   
src/emport.c    - Contains portable functions of Aricent EOAM.

Other Issues:
=============
o Blocking calls which takes more than 4 secs should not be invoked when EOAM 
  entity is in loopback mode. This may allow discovery to restart as the link 
  lost timer expires in 5 secs.

How to read documents
---------------------

   The product source code documents are best viewed using Acrobat Reader ver.
   6.x. To download ver. 6.x use the link,

   http://www.adobe.com/products/acrobat/readstep2.html.


END OF README
