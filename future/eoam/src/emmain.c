/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emmain.c,v 1.25 2012/04/18 14:14:43 siva Exp $
 *
 * Description: This file contains EOAM task main loop and 
 *              initialization routines.
 *********************************************************************/
#include "eminc.h"
#include "emglob.h"
#include "mux.h"

/* Proto types of the functions private to this file only */
PRIVATE INT4 EoamMainTaskInit PROTO ((VOID));
PRIVATE VOID EoamMainHandleInitFailure PROTO ((VOID));
PRIVATE VOID EoamMainClearVarReqResp PROTO ((tEoamVarReq *));

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamMainTask
 *                                                                          
 *    DESCRIPTION      : Entry point function of EOAM task. 
 *
 *    INPUT            : pArg - Pointer to the arguments 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EoamMainTask (INT1 *pArg)
{
    UINT4               u4Events = 0;

    UNUSED_PARAM (pArg);

    EOAM_TRC_FN_ENTRY ();

    if (EoamMainTaskInit () == OSIX_FAILURE)
    {
        EoamMainHandleInitFailure ();

        /* Indicate the status of initialization to main routine */
        lrInitComplete (OSIX_FAILURE);
        return;
    }

#ifdef SNMP_2_WANTED
    /* Register the protocol MIBS with SNMP */
    RegisterSTDEOA ();
    RegisterEOAMEX ();
#endif

    /* Indicate the status of initialization to main routine */
    lrInitComplete (OSIX_SUCCESS);

    while (1)
    {
        if (OsixEvtRecv (gEoamGlobalInfo.TaskId, EOAM_ALL_EVENTS,
                         OSIX_WAIT, &u4Events) == OSIX_SUCCESS)
        {
            /* Mutual exclusion flag ON */
            EoamLock ();

            EOAM_TRC ((CONTROL_PLANE_TRC, "Rx Event %d\n", u4Events));

            if (u4Events & EOAM_QMSG_EVENT)
            {
                EOAM_TRC ((CONTROL_PLANE_TRC, "Rx MsgQ If Event\n"));
                EoamQueMsgHandler ();
            }

            if (u4Events & EOAM_TMR_EXPIRY_EVENT)
            {
                EOAM_TRC ((CONTROL_PLANE_TRC, "Rx Tmr Exp Event\n"));
                EoamTmrExpHandler ();
            }
#ifdef L2RED_WANTED
            /* For processing subbulk updates */
            if (u4Events & EOAM_RED_SUB_BULK_UPD_EVENT)
            {
                EoamRedProcessBulkReqEvent (NULL);
            }
#endif
            /* Mutual exclusion flag OFF */
            EoamUnLock ();
        }
    }                            /* end of while */
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamMainTaskInit 
 *                                                                          
 *    DESCRIPTION      : This function creates the task queue, allocates 
 *                       MemPools for task queue messages and creates protocol
 *                       semaphore. 
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
EoamMainTaskInit (VOID)
{
    EOAM_TRC_FN_ENTRY ();

    MEMSET (&gEoamGlobalInfo, 0, sizeof (tEoamGlobalInfo));

    MEMSET (&gFsEoamSizingInfo, 0, sizeof (tFsModSizingInfo));
    MEMCPY (gFsEoamSizingInfo.ModName, "EOAM", STRLEN ("EOAM"));
    gFsEoamSizingInfo.u4ModMemPreAllocated = 0;
    gFsEoamSizingInfo.ModSizingParams = gFsEoamSizingParams;

    FsUtlSzCalculateModulePreAllocatedMemory (&gFsEoamSizingInfo);
    OsixTskIdSelf (&(gEoamGlobalInfo.TaskId));

    /* Protocol semaphore - created with initial value 0. */
    if (OsixSemCrt ((UINT1 *) EOAM_PROTOCOL_SEM,
                    &(gEoamGlobalInfo.ProtocolSemId)) == OSIX_FAILURE)
    {
        EOAM_TRC ((INIT_SHUT_TRC | ALL_FAILURE_TRC,
                   "EoamMainTaskInit: EOAM protocol semaphore creation"
                   " FAILED!!!\n"));
        return (OSIX_FAILURE);
    }

    /* Semaphore is by default created with initial value 0. So GiveSem
     * is called before using the semaphore. */
    OsixSemGive (gEoamGlobalInfo.ProtocolSemId);

    /* Synchronization semaphore - created with initial value 0. */
    if (OsixSemCrt ((UINT1 *) EOAM_SYNC_SEM,
                    &(gEoamGlobalInfo.SyncSemId)) == OSIX_FAILURE)
    {
        EOAM_TRC ((INIT_SHUT_TRC | ALL_FAILURE_TRC,
                   "EoamMainTaskInit: EOAM synchronization semaphore creation"
                   " FAILED!!!\n"));
        return (OSIX_FAILURE);
    }

    /* Eoam Task Q */
    if (OsixQueCrt ((UINT1 *) EOAM_QUEUE_NAME, OSIX_MAX_Q_MSG_LEN,
                    EOAM_QUEUE_DEPTH, &(gEoamGlobalInfo.QId)) == OSIX_FAILURE)
    {
        EOAM_TRC ((INIT_SHUT_TRC | ALL_FAILURE_TRC,
                   "EoamMainTaskInit: EOAM Queue creation FAILED !!!\n"));
        return (OSIX_FAILURE);
    }

    if (EoamHandleModuleStart () == OSIX_FAILURE)
    {
        EOAM_TRC ((INIT_SHUT_TRC | ALL_FAILURE_TRC,
                   "EoamMainTaskInit: EOAM Module Start FAILED !!!\n"));
        return (OSIX_FAILURE);
    }

    EOAM_TRC_FN_EXIT ();

    return (OSIX_SUCCESS);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamMainInitGlobalInfo 
 *                                                                          
 *    DESCRIPTION      : This function initalizes global structure with 
 *                       default values.
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EoamMainInitGlobalInfo (VOID)
{
    tMacAddr            SwitchMac;

    EOAM_TRC_FN_ENTRY ();

    TMO_DLL_Init (&(gEoamGlobalInfo.EoamEnaPortDllList));

    gEoamGlobalInfo.u1SystemCtrlStatus = EOAM_SHUTDOWN;
    gEoamGlobalInfo.u1ModuleStatus = EOAM_DISABLED;
    gEoamGlobalInfo.u1ModuleAdminStatus = EOAM_DISABLED;

    gEoamGlobalInfo.u4TraceOption = EOAM_CRITICAL_TRC;

    /* Initialize with first 3 bytes of System MAC address */
    EoamGetSysMacAddress (SwitchMac);
    MEMCPY (gEoamGlobalInfo.au1OUI, SwitchMac, EOAM_OUI_LENGTH);

    gEoamGlobalInfo.u4VendorSpecInfo = 0;

    gEoamGlobalInfo.u4ErrEventResendCount = EOAM_ERR_EVT_RESEND_COUNT;
    gEoamGlobalInfo.u4VarReqIdCount = 0;

    gEoamGlobalInfo.i4BuddyId = -1;

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamMainMemInit 
 *                                                                          
 *    DESCRIPTION      : Function to release the mutual exclusion protocol
 *                       semaphore.  
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamMainMemInit (VOID)
{
    EOAM_TRC_FN_ENTRY ();
    /* To Create Mempools for the module */
    if (EoamSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        EOAM_TRC ((INIT_SHUT_TRC | ALL_FAILURE_TRC,
                   "EoamMainMemInit: Memory Pools Initialization FAILED !!!\n"));
        return (OSIX_FAILURE);
    }

    if ((gEoamGlobalInfo.i4BuddyId =
         MemBuddyCreate
         ((EOAM_MAX_PDU_SIZE + 2), EOAM_MIN_BLOCK_SIZE,
          gFsEoamSizingParams[EOAM_BUDDY_SIZING_ID].u4PreAllocatedUnits,
          BUDDY_CONT_BUF)) == BUDDY_FAILURE)
    {
        EOAM_TRC ((INIT_SHUT_TRC | ALL_FAILURE_TRC,
                   "EoamMainMemInit: Buddy creation FAILED !!!\n"));
        return (OSIX_FAILURE);
    }

    EOAM_TRC_FN_EXIT ();

    return (OSIX_SUCCESS);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamMainMemClear 
 *                                                                          
 *    DESCRIPTION      : Function that deletes all memepools
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EoamMainMemClear (VOID)
{

    EOAM_TRC_FN_ENTRY ();

    /* Bulk update request and L2 complete sync up Message from RM
     * needs to be handled in eoam queue, even when eoam is 
     * in shutdown state, hence queue is not deleted when eoam is
     * shutdown */

    /* To delete all mempools created for the module  */
    EoamSizingMemDeleteMemPools ();

    if (gEoamGlobalInfo.i4BuddyId != -1)
    {
        MemBuddyDestroy ((UINT1) gEoamGlobalInfo.i4BuddyId);
        gEoamGlobalInfo.i4BuddyId = -1;
    }

    EOAM_TRC_FN_EXIT ();

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamMainClearEnabledInfo 
 *                                                                          
 *    DESCRIPTION      : This function is called to delete the EOAM info
 *                       and release the memory allocated to it.   
 *
 *    INPUT            : pEnaEntry - Pointer to the EOAM enabled entry 
 *                      
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : None. 
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EoamMainClearEnabledInfo (tEoamEnaPortInfo * pEnaEntry)
{
    UINT2               u2VarReqId = 0;
    tEoamVarReq        *pVarReq = NULL;

    EOAM_TRC_FN_ENTRY ();

    /* Abort if pointer is NULL */
    FSAP_ASSERT (pEnaEntry != NULL);

    /* Delete the Variable Req/Resp informations stored for a port */
    for (u2VarReqId = 0; u2VarReqId < EOAM_MAX_VAR_REQS; u2VarReqId++)
    {
        pVarReq = pEnaEntry->pVarReq[u2VarReqId];
        if (pVarReq != NULL)
        {
            EoamMainClearVarReqResp (pVarReq);
        }
    }

    /* Set the back pointer of Eoam Enabled node to NULL */
    pEnaEntry->pPortInfoBackPtr = NULL;

    /* Stop the Loopback Tx Timer */
    if (TmrStop (gEoamGlobalInfo.EoamTmrListId, &(pEnaEntry->LBTxTmr))
        == TMR_FAILURE)
    {
        EOAM_TRC ((OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                   "EoamMainClearEnabledInfo: LB Stop Timer FAILED !!! \n"));
    }

    /* Stop the LostLink Timer */
    if (TmrStop (gEoamGlobalInfo.EoamTmrListId, &(pEnaEntry->LostLinkAppTmr))
        == TMR_FAILURE)
    {
        EOAM_TRC ((OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                   "EoamMainClearEnabledInfo: LinkLost Stop Timer FAILED"
                   "!!!\n"));
    }

    /* Release port info maintained for EOAM enabled port */
    if (MemReleaseMemBlock (gEoamGlobalInfo.EnaPortPoolId,
                            (UINT1 *) pEnaEntry) == MEM_FAILURE)
    {
        EOAM_TRC ((INIT_SHUT_TRC, "EoamMainClearEnabledInfo: Free MemBlock"
                   " Enabled port entry FAILED!!!\n"));
    }

    EOAM_TRC_FN_EXIT ();

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamMainClearEvtLogs 
 *                                                                          
 *    DESCRIPTION      : This function is called to delete all events logs
 *                       corresponding to the port entry.   
 *
 *    INPUT            : pEoamEntry - Pointer to the EOAM port entry 
 *                      
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : None. 
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EoamMainClearEvtLogs (tEoamPortInfo * pEoamEntry)
{
    UINT2               u2EvtLogId = 0;
    tEoamEventLogEntry *pEventLogEntry = NULL;

    EOAM_TRC_FN_ENTRY ();

    /* Abort if the pointer is NULL */
    FSAP_ASSERT (pEoamEntry != NULL);

    /* Delete the error event logs maintained for a port */
    for (u2EvtLogId = 0; u2EvtLogId < EOAM_MAX_LOG_ENTRIES; u2EvtLogId++)
    {
        pEventLogEntry = pEoamEntry->apEventLogEntry[u2EvtLogId];
        if (pEventLogEntry == NULL)
        {
            continue;
        }

        if (MemReleaseMemBlock (gEoamGlobalInfo.EvtLogPoolId,
                                (UINT1 *) pEventLogEntry) == MEM_FAILURE)
        {
            EOAM_TRC ((INIT_SHUT_TRC, "EoamMainClearEvtLogs: Free MemBlock"
                       " Event Log entry FAILED!!!\n"));
        }
        pEoamEntry->apEventLogEntry[u2EvtLogId] = NULL;
    }

    pEoamEntry->u1LogPosToBeFilled = 0;
    EOAM_TRC_FN_EXIT ();

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamMainClearVarReqResp 
 *                                                                          
 *    DESCRIPTION      : This function is used to remove the Variable Req Resp
 *                       information maintained for a port.  
 *
 *    INPUT            : pReqResp - Pointer to Variable Req/Resp info
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : None. 
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
EoamMainClearVarReqResp (tEoamVarReq * pReqResp)
{
    tEoamVarReqRBTNode *pRBTNode = NULL;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;

    EOAM_TRC_FN_ENTRY ();

    /* Abort if the pointer is NULL */
    FSAP_ASSERT (pReqResp != NULL);

    /* Stop Var req resp timer */
    if (TmrStop (gEoamGlobalInfo.EoamTmrListId, &(pReqResp->ReqRespAppTmr))
        == TMR_FAILURE)
    {
        EOAM_TRC ((OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                   "EoamMainClearVarReqResp: ReqResp Stop Timer FAILED "
                   "!!! \n"));
    }

    pRBElem = RBTreeGetFirst (pReqResp->RBTVarReq);
    while (pRBElem != NULL)
    {
        pRBTNode = (tEoamVarReqRBTNode *) pRBElem;

        RBTreeRemove (pReqResp->RBTVarReq, (tRBElem *) pRBTNode);
        MemReleaseMemBlock (gEoamGlobalInfo.VarContainerPoolId,
                            (UINT1 *) pRBTNode);

        pRBNextElem = RBTreeGetNext (pReqResp->RBTVarReq, pRBElem, NULL);
        pRBElem = pRBNextElem;
    }

    RBTreeDestroy (pReqResp->RBTVarReq, NULL, 0);
    MemReleaseMemBlock (gEoamGlobalInfo.VarReqPoolId, (UINT1 *) pReqResp);
    pReqResp = NULL;

    EOAM_TRC_FN_EXIT ();

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamMainClearPortInfo 
 *                                                                          
 *    DESCRIPTION      : This function is used to remove the port info in the
 *                       global structure. This is called whenever the port 
 *                       is deleted. 
 *
 *    INPUT            : pPortEntry - Pointer to port information struct
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : None. 
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EoamMainClearPortInfo (tEoamPortInfo * pPortEntry)
{
    UINT4               u4PortIndex = 0;

    EOAM_TRC_FN_ENTRY ();

    u4PortIndex = pPortEntry->u4Port;

    /* Abort if pointer is NULL */
    FSAP_ASSERT (gEoamGlobalInfo.apEoamPortEntry[u4PortIndex - 1] != NULL);
    /* Delete all event logs for this eoam port entry */
    EoamMainClearEvtLogs (pPortEntry);

    if (MemReleaseMemBlock (gEoamGlobalInfo.PortPoolId,
                            (UINT1 *) pPortEntry) == MEM_FAILURE)
    {
        EOAM_TRC ((INIT_SHUT_TRC, "EoamMainClearPortInfo: Free MemBlock"
                   " Enabled port entry FAILED!!!\n"));
    }

    gEoamGlobalInfo.apEoamPortEntry[u4PortIndex - 1] = NULL;

    EOAM_TRC_FN_EXIT ();

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamMainHandleInitFailure 
 *                                                                          
 *    DESCRIPTION      : Function called when the initialization of EOAM
 *                       fails at any point during initialization.  
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
EoamMainHandleInitFailure (VOID)
{
    tEoamQMsg          *pQMsg = NULL;

    EOAM_TRC_FN_ENTRY ();

    if (gEoamGlobalInfo.QId != 0)
    {
        /* Dequeue all messages */
        while (OsixQueRecv (gEoamGlobalInfo.QId, (UINT1 *) &pQMsg,
                            OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
        {
            if (pQMsg == NULL)
            {
                continue;
            }

            if (MemReleaseMemBlock (gEoamGlobalInfo.QMsgPoolId, (UINT1 *) pQMsg)
                == MEM_FAILURE)
            {
                EOAM_TRC ((INIT_SHUT_TRC,
                           "EoamMainMemClear: QMsg Free Mem"
                           " Block FAILED !!!\n"));
            }
        }
    }

    EoamMainMemClear ();
    if (gEoamGlobalInfo.QId != 0)
    {
        /* Delete Q */
        OsixQueDel (gEoamGlobalInfo.QId);
    }

    /* Deinit Timer */
    if (EoamTmrDeInit () == OSIX_FAILURE)
    {
        EOAM_TRC ((INIT_SHUT_TRC, "EoamMainHandleInitFailure: Timer Deinit"
                   " FAILED\n"));
    }

    /* Delete Semaphore */
    if (gEoamGlobalInfo.ProtocolSemId != 0)
    {
        OsixSemDel (gEoamGlobalInfo.ProtocolSemId);
    }

    if (gEoamGlobalInfo.SyncSemId != 0)
    {
        OsixSemDel (gEoamGlobalInfo.SyncSemId);
    }

    EOAM_TRC_FN_EXIT ();

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamMainNotifyModules
 *
 *    DESCRIPTION      : Function to notify external modules registered with
 *                       EOAM.
 *
 *    INPUT            : pCallbackInfo - Callbackinfo to be passed to
 *                                       registered module
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EoamMainNotifyModules (tEoamCallbackInfo * pCallbackInfo)
{
    UINT4               u4Count = 0;

    EOAM_TRC_FN_ENTRY ();

    for (u4Count = 1; u4Count < EOAM_MAX_APPL_ID; u4Count++)
    {
        if ((pCallbackInfo->u4EventType &
             gEoamGlobalInfo.EoamRegParams[u4Count].u4Bitmap) &&
            (gEoamGlobalInfo.EoamRegParams[u4Count].pNotifyCbFunc != NULL))
        {
            /* For loopback test data, the registered module should 
             * copy the CRU buffer contents in the callback function.
             * EOAM will release the CRU buffer immediately after calling the
             * callback function
             */
            (*(gEoamGlobalInfo.EoamRegParams[u4Count].pNotifyCbFunc))
                (pCallbackInfo);
        }
    }

    if (pCallbackInfo->u4EventType == EOAM_NOTIFY_LB_TESTDATA_RCVD)
    {
        if (CRU_BUF_Release_MsgBufChain (pCallbackInfo->LBTestData,
                                         OSIX_FALSE) != CRU_SUCCESS)
        {
            EOAM_TRC ((BUFFER_TRC | ALL_FAILURE_TRC,
                       "EoamMainNotifyModules: Loopback test data "
                       "CRU Buffer Release Failed\n"));
        }
    }

    EOAM_TRC_FN_EXIT ();
    return;
}

/*****************************************************************************/
/* Function Name      : EoamRegisterWithPacketHandler                        */
/*                                                                           */
/* Description        : This function registers with the packet handler for  */
/*                      EOAM control packets.                                */
/*                                                                           */
/* Input (s)          : NONE.                                                */
/*                                                                           */
/* Output (s)         : NONE                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
EoamRegisterWithPacketHandler ()
{
    static tMacAddr     EoamMac = { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x02 };
    tProtocolInfo       EoamProtocolInfo;

    MEMSET (&EoamProtocolInfo, 0, sizeof (tProtocolInfo));

    /* Packet should come through CFA, set the callback to NULL */
    EoamProtocolInfo.ProtocolFnPtr = NULL;
    EoamProtocolInfo.u1SendToFlag = MUX_REG_SEND_TO_ISS;

    MEMCPY (&EoamProtocolInfo.ProtRegTuple.MacInfo.MacAddr, &EoamMac,
            MAC_ADDR_LEN);
    MEMSET (EoamProtocolInfo.ProtRegTuple.MacInfo.MacMask, 0xFF, MAC_ADDR_LEN);
    EoamProtocolInfo.ProtRegTuple.EthInfo.u2Protocol = CFA_ENET_SLOW_PROTOCOL;
    EoamProtocolInfo.ProtRegTuple.EthInfo.u2Mask = 0xFFFF;

    /* Fill the subtypes */
    EoamProtocolInfo.ProtRegTuple.IPInfo.u2Protocol = EOAM_PROTOCOL_SUBTYPE;
    EoamProtocolInfo.ProtRegTuple.IPInfo.u2Mask = 0xFFFF;
    if (CfaMuxRegisterProtocolCallBack (PACKET_HDLR_PROTO_EOAM,
                                        &EoamProtocolInfo) != CFA_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EoamAssignMempoolIds                                 */
/*                                                                           */
/* Description        : This function assign mempools from sz.c ie.          */
/*                      EoamMainAssignMempoolIds  to global mempoolIds.      */
/*                                                                           */
/* Input (s)          : NONE.                                                */
/*                                                                           */
/* Output (s)         : NONE                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    :NONE                                                  */
/*                                                                           */
/*****************************************************************************/
VOID
EoamMainAssignMempoolIds (VOID)
{

    /* Q Msg Mempool */
    gEoamGlobalInfo.QMsgPoolId = EOAMMemPoolIds[MAX_EOAM_Q_MESG_SIZING_ID];

    /* Port Entry MemPool */
    gEoamGlobalInfo.PortPoolId = EOAMMemPoolIds[MAX_EOAM_PORTS_SIZING_ID];

    /* EOAM Enabled Port Entry MemPool */
    gEoamGlobalInfo.EnaPortPoolId =
        EOAMMemPoolIds[MAX_EOAM_ENABLED_PORTS_SIZING_ID];

    /* Variable Request RB Tree MemPool */
    gEoamGlobalInfo.VarReqPoolId =
        EOAMMemPoolIds[MAX_EOAM_VAR_REQUESTS_SIZING_ID];

    /* Variable Request RB Tree Node MemPool */
    gEoamGlobalInfo.VarContainerPoolId =
        EOAMMemPoolIds[MAX_EOAM_VAR_REQ_RBT_NODES_SIZING_ID];

    /* Event Log Mempool */
    gEoamGlobalInfo.EvtLogPoolId =
        EOAMMemPoolIds[MAX_EOAM_EVENT_LOG_ENTRIES_SIZING_ID];

    /* tEoamLmEnaPortDllNode */
    gEoamLmGlobalInfo.LmEnaDllNodePoolId =
        EOAMMemPoolIds[MAX_EOAM_DLL_NODES_FOR_EOAM_ENABLED_PORTS_SIZING_ID];

    /* EOAM PDU Mempool */
    gEoamGlobalInfo.EmLinearBufPoolId =
        EOAMMemPoolIds[MAX_EOAM_PDU_BUF_SIZING_ID];

    /* EOAM DATA Mempool */
    gEoamGlobalInfo.EmDataBufPoolId =
        EOAMMemPoolIds[MAX_EOAM_DATA_BUF_SIZING_ID];

    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  emmain.c                       */
/*-----------------------------------------------------------------------*/
