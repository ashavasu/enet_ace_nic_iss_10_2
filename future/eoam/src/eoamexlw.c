/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: eoamexlw.c,v 1.12 2017/08/31 14:02:00 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include "eminc.h"

extern UINT4        fseoam[8];
extern UINT4        stdeoa[7];
extern UINT4        FsEoamSystemControl[10];
PRIVATE VOID        EoamNotifyProtocolShutdownStatus (VOID);
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEoamSystemControl
 Input       :  The Indices

                The Object 
                retValFsEoamSystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEoamSystemControl (INT4 *pi4RetValFsEoamSystemControl)
{
    *pi4RetValFsEoamSystemControl = (INT4) gEoamGlobalInfo.u1SystemCtrlStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEoamModuleStatus
 Input       :  The Indices

                The Object 
                retValFsEoamModuleStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEoamModuleStatus (INT4 *pi4RetValFsEoamModuleStatus)
{
    if (gEoamGlobalInfo.u1SystemCtrlStatus == EOAM_SHUTDOWN)
    {
        EOAM_TRC ((MGMT_TRC, "\tSNMP: EOAM module is Shutdown\n"));
        *pi4RetValFsEoamModuleStatus = EOAM_DISABLED;
        return SNMP_SUCCESS;
    }

    *pi4RetValFsEoamModuleStatus = (INT4) gEoamGlobalInfo.u1ModuleAdminStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEoamErrorEventResend
 Input       :  The Indices

                The Object 
                retValFsEoamErrorEventResend
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEoamErrorEventResend (UINT4 *pu4RetValFsEoamErrorEventResend)
{
    if (gEoamGlobalInfo.u1SystemCtrlStatus == EOAM_SHUTDOWN)
    {
        EOAM_TRC ((MGMT_TRC | ALL_FAILURE_TRC,
                   "\tSNMP: EOAM module is Shutdown\n"));
        *pu4RetValFsEoamErrorEventResend = 0;
        return SNMP_SUCCESS;
    }

    *pu4RetValFsEoamErrorEventResend = gEoamGlobalInfo.u4ErrEventResendCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEoamOui
 Input       :  The Indices

                The Object 
                retValFsEoamOui
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEoamOui (tSNMP_OCTET_STRING_TYPE * pRetValFsEoamOui)
{
    UINT1              *pu1Oui = NULL;

    if (gEoamGlobalInfo.u1SystemCtrlStatus == EOAM_SHUTDOWN)
    {
        EOAM_TRC ((MGMT_TRC, "\tSNMP: EOAM module is Shutdown\n"));
        pRetValFsEoamOui->pu1_OctetList[0] = 0;
        pRetValFsEoamOui->i4_Length = 0;
        return SNMP_SUCCESS;
    }

    pu1Oui = gEoamGlobalInfo.au1OUI;
    MEMCPY (pRetValFsEoamOui->pu1_OctetList, pu1Oui, EOAM_OUI_LENGTH);
    pRetValFsEoamOui->i4_Length = EOAM_OUI_LENGTH;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEoamTraceOption
 Input       :  The Indices

                The Object 
                retValFsEoamTraceOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEoamTraceOption (INT4 *pi4RetValFsEoamTraceOption)
{
    if (gEoamGlobalInfo.u1SystemCtrlStatus == EOAM_SHUTDOWN)
    {
        EOAM_TRC ((MGMT_TRC, "\tSNMP: EOAM module is Shutdown\n"));
        *pi4RetValFsEoamTraceOption = 0;
        return SNMP_SUCCESS;
    }

    *pi4RetValFsEoamTraceOption = (INT4) gEoamGlobalInfo.u4TraceOption;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsEoamSystemControl
 Input       :  The Indices

                The Object 
                setValFsEoamSystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEoamSystemControl (INT4 i4SetValFsEoamSystemControl)
{
    INT4                i4RetVal = 0;

    /*
     * Check if EOAM is already started/ shutdown before setting this
     * object
     * 
     */
    if (gEoamGlobalInfo.u1SystemCtrlStatus ==
        (UINT1) i4SetValFsEoamSystemControl)
    {
        EOAM_TRC ((MGMT_TRC,
                   "\tSNMP: EOAM already in the same System control state\n"));
        return SNMP_SUCCESS;
    }

    if (i4SetValFsEoamSystemControl != EOAM_SHUTDOWN)
    {
        /*Start EOAM */
        i4RetVal = EoamHandleModuleStart ();

        if (i4RetVal != OSIX_SUCCESS)
        {
            EOAM_TRC ((ALL_FAILURE_TRC | MGMT_TRC,
                       "\tSNMP: EOAM Module start FAILED\n"));
            return SNMP_FAILURE;
        }
    }
    else
    {
        /*Shutdown EOAM */
        EoamHandleModuleShutDown ();
        /* Notify MSR with the eoam oids */
        EoamNotifyProtocolShutdownStatus ();
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEoamModuleStatus
 Input       :  The Indices

                The Object 
                setValFsEoamModuleStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEoamModuleStatus (INT4 i4SetValFsEoamModuleStatus)
{
    INT4                i4RetVal = 0;
    /*
     * Check if EOAM is already eanbled/ disabled before setting this
     * object
     * 
     */
    if (gEoamGlobalInfo.u1ModuleAdminStatus ==
        (UINT1) i4SetValFsEoamModuleStatus)
    {
        EOAM_TRC ((MGMT_TRC, "\tSNMP: EOAM already in the same state\n"));
        return SNMP_SUCCESS;
    }

    if (i4SetValFsEoamModuleStatus == EOAM_ENABLED)
    {
        /*Enable EOAM */
        i4RetVal = EoamModuleEnable ();

        if (i4RetVal != OSIX_SUCCESS)
        {
            EOAM_TRC ((ALL_FAILURE_TRC | MGMT_TRC,
                       "\tSNMP: FAILED to enable EOAM module\n"));
            return SNMP_FAILURE;
        }
    }
    else
    {
        /* Disable EOAM */
        EoamModuleDisable ();
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEoamErrorEventResend
 Input       :  The Indices

                The Object 
                setValFsEoamErrorEventResend
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEoamErrorEventResend (UINT4 u4SetValFsEoamErrorEventResend)
{
    gEoamGlobalInfo.u4ErrEventResendCount = u4SetValFsEoamErrorEventResend;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEoamOui
 Input       :  The Indices

                The Object 
                setValFsEoamOui
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEoamOui (tSNMP_OCTET_STRING_TYPE * pSetValFsEoamOui)
{
    UINT1              *pu1Oui = NULL;
    tTMO_DLL_NODE      *pEnaNode = NULL;
    tEoamPortInfo      *pPortInfo = NULL;

    pu1Oui = gEoamGlobalInfo.au1OUI;
    MEMCPY (pu1Oui, pSetValFsEoamOui->pu1_OctetList, EOAM_OUI_LENGTH);
    /* Scan the EOAM enabled ports in the DLL and start the discovery
     * state machine for all EOAM enabled ports. */
    TMO_DLL_Scan (&(gEoamGlobalInfo.EoamEnaPortDllList),
                  pEnaNode, tTMO_DLL_NODE *)
    {
        pPortInfo = (tEoamPortInfo *) pEnaNode;
        /*  FSAP_ASSERT (pPortInfo != NULL); */
        if ((pPortInfo != NULL) && (pPortInfo->pEoamEnaPortInfo != NULL))
        {
            /* Update the Information OAMPDU structure */
            pPortInfo->LocalInfo.u2ConfigRevision++;
            EoamCtlTxConstructInfoPdu (pPortInfo);
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEoamTraceOption
 Input       :  The Indices

                The Object 
                setValFsEoamTraceOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEoamTraceOption (INT4 i4SetValFsEoamTraceOption)
{
    gEoamGlobalInfo.u4TraceOption = (UINT4) i4SetValFsEoamTraceOption;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsEoamSystemControl
 Input       :  The Indices

                The Object 
                testValFsEoamSystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEoamSystemControl (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsEoamSystemControl)
{
    if ((i4TestValFsEoamSystemControl != EOAM_START) &&
        (i4TestValFsEoamSystemControl != EOAM_SHUTDOWN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEoamModuleStatus
 Input       :  The Indices

                The Object 
                testValFsEoamModuleStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEoamModuleStatus (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsEoamModuleStatus)
{
    if (gEoamGlobalInfo.u1SystemCtrlStatus == EOAM_SHUTDOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsEoamModuleStatus != EOAM_ENABLED) &&
        (i4TestValFsEoamModuleStatus != EOAM_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEoamErrorEventResend
 Input       :  The Indices

                The Object 
                testValFsEoamErrorEventResend
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEoamErrorEventResend (UINT4 *pu4ErrorCode,
                                 UINT4 u4TestValFsEoamErrorEventResend)
{
    if (gEoamGlobalInfo.u1SystemCtrlStatus == EOAM_SHUTDOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((u4TestValFsEoamErrorEventResend < EOAM_ERR_RESEND_MIN_VAL) ||
        (u4TestValFsEoamErrorEventResend > EOAM_ERR_RESEND_MAX_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEoamOui
 Input       :  The Indices

                The Object 
                testValFsEoamOui
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEoamOui (UINT4 *pu4ErrorCode,
                    tSNMP_OCTET_STRING_TYPE * pTestValFsEoamOui)
{
    if (gEoamGlobalInfo.u1SystemCtrlStatus == EOAM_SHUTDOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pTestValFsEoamOui->i4_Length != EOAM_OUI_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    if (pTestValFsEoamOui->pu1_OctetList == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEoamTraceOption
 Input       :  The Indices

                The Object 
                testValFsEoamTraceOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEoamTraceOption (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsEoamTraceOption)
{
    if (gEoamGlobalInfo.u1SystemCtrlStatus == EOAM_SHUTDOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (((i4TestValFsEoamTraceOption < EOAM_MIN_TRC_VAL) ||
         (i4TestValFsEoamTraceOption > EOAM_MAX_TRC_VAL)) &&
        (i4TestValFsEoamTraceOption != EOAM_CLR_TRC_OPT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsEoamSystemControl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEoamSystemControl (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsEoamModuleStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEoamModuleStatus (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsEoamErrorEventResend
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEoamErrorEventResend (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsEoamOui
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEoamOui (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsEoamTraceOption
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEoamTraceOption (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  EoamNotifyProtocolShutdownStatus
 Input       :  None
 Output      :  None
 Returns     :  None
****************************************************************************/

VOID
EoamNotifyProtocolShutdownStatus (VOID)
{
#ifdef ISS_WANTED
    UINT1               au1ObjectOid[3][SNMP_MAX_OID_LENGTH];

    /* The oid list contains the list of Oids that has been registered 
     * for the protocol + the oid of the SystemControl object. */
    SNMPGetOidString (fseoam, (sizeof (fseoam) / sizeof (UINT4)),
                      au1ObjectOid[0]);
    SNMPGetOidString (stdeoa, (sizeof (stdeoa) / sizeof (UINT4)),
                      au1ObjectOid[1]);
    SNMPGetOidString (FsEoamSystemControl,
                      (sizeof (FsEoamSystemControl) / sizeof (UINT4)),
                      au1ObjectOid[2]);

    /* Send a notification to MSR to process the eoam shutdown, with 
     * eoam oids and its system control object 
     * As the protocol does not have MI support, invalid context id is sent */
    MSR_NOTIFY_PROTOCOL_SHUT (au1ObjectOid, 3, MSR_INVALID_CNTXT);
#endif
    return;
}

/*--------------------------------------------------------------------------*/
/*                         End of the file eoamexlw.c                       */
/*--------------------------------------------------------------------------*/
