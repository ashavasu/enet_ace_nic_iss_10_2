
/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: eoamnpwr.c,v 1.2 2012/08/08 10:02:21 siva Exp $
 *
 * Description: This file contains the EOAM NPAPI related wrapper routines.
 *
 ***************************************************************************/

#ifndef _EOAM_NP_WR_C_
#define _EOAM_NP_WR_C_

#include "eminc.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : EoamNpWrHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tEoamNpModInfo
 *                                                                          
 *    Input(s)            : FsHwNpParam of type tfsHwNp                     
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
EoamNpWrHwProgram (tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tEoamNpModInfo     *pEoamNpModInfo = NULL;

    if (NULL == pFsHwNp)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pEoamNpModInfo = &(pFsHwNp->EoamNpModInfo);

    if (NULL == pEoamNpModInfo)
    {
        return FNP_FAILURE;
    }

    switch (u4Opcode)
    {
        case EOAM_LM_NP_GET_STAT:
        {
            tEoamNpWrEoamLmNpGetStat *pEntry = NULL;
            pEntry = &pEoamNpModInfo->EoamNpEoamLmNpGetStat;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                EoamLmNpGetStat (pEntry->u4IfIndex, pEntry->u1StatType,
                                 pEntry->pu4Value);
            break;
        }
        case EOAM_LM_NP_GET_STAT64:
        {
            tEoamNpWrEoamLmNpGetStat64 *pEntry = NULL;
            pEntry = &pEoamNpModInfo->EoamNpEoamLmNpGetStat64;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                EoamLmNpGetStat64 (pEntry->u4IfIndex, pEntry->u1StatType,
                                   pEntry->pu8Value);
            break;
        }
        case EOAM_NP_DE_INIT:
        {
            u1RetVal = EoamNpDeInit ();
            break;
        }
        case EOAM_NP_GET_UNI_DIRECTIONAL_CAPABILITY:
        {
            tEoamNpWrEoamNpGetUniDirectionalCapability *pEntry = NULL;
            pEntry = &pEoamNpModInfo->EoamNpEoamNpGetUniDirectionalCapability;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                EoamNpGetUniDirectionalCapability (pEntry->u4IfIndex,
                                                   pEntry->pu1Capability);
            break;
        }
        case EOAM_NP_HANDLE_LOCAL_LOOP_BACK:
        {
            tEoamNpWrEoamNpHandleLocalLoopBack *pEntry = NULL;
            pEntry = &pEoamNpModInfo->EoamNpEoamNpHandleLocalLoopBack;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                EoamNpHandleLocalLoopBack (pEntry->u4IfIndex, pEntry->SrcMac,
                                           pEntry->DestMac, pEntry->u1Status);
            break;
        }
        case EOAM_NP_HANDLE_REMOTE_LOOP_BACK:
        {
            tEoamNpWrEoamNpHandleRemoteLoopBack *pEntry = NULL;
            pEntry = &pEoamNpModInfo->EoamNpEoamNpHandleRemoteLoopBack;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                EoamNpHandleRemoteLoopBack (pEntry->u4IfIndex, pEntry->SrcMac,
                                            pEntry->PeerMac, pEntry->u1Status);
            break;
        }
        case EOAM_NP_INIT:
        {
            u1RetVal = EoamNpInit ();
            break;
        }
        case EOAM_LM_NP_REGISTER_LINK_MONITOR:
        {
            u1RetVal = EoamLmNpRegisterLinkMonitor ();
            break;
        }
        case EOAM_LM_NP_CONFIGURE_PARAMS:
        {
            tEoamNpWrEoamLmNpConfigureParams *pEntry = NULL;
            pEntry = &pEoamNpModInfo->EoamNpEoamLmNpConfigureParams;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                EoamLmNpConfigureParams (pEntry->u4IfIndex, pEntry->u2Event,
                                         pEntry->Window, pEntry->Threshold);
            break;
        }
#ifdef MBSM_WANTED
        case EOAM_MBSM_HW_INIT:
        {
            tEoamNpWrEoamMbsmHwInit *pEntry = NULL;
            pEntry = &pEoamNpModInfo->EoamNpEoamMbsmHwInit;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = EoamMbsmHwInit (pEntry->pSlotInfo);
            break;
        }
#endif /* MBSM_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

#endif /* _EOAM_NP_WR_C_ */
