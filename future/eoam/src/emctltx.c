/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emctltx.c,v 1.20 2014/07/06 11:48:47 siva Exp $
 *
 * Description: This file contains Ethernet OAM Control transmit related
 *              functions
 *****************************************************************************/
#include "eminc.h"

PRIVATE INT4 EoamCtlTxInfoPduRules PROTO ((tEoamPortInfo *));
PRIVATE UINT4 EoamCtlTxInfoPduSize PROTO ((tEoamEnaPortInfo *));
PRIVATE INT4 EoamCtlTxPduRules PROTO ((tEoamEnaPortInfo *));
PRIVATE UINT4 EoamCtlTxFindPktSize PROTO ((UINT1, tEoamPduData *));
PRIVATE UINT4 EoamCtlTxEventTlvSize PROTO ((tEoamPduData * pPduData));
PRIVATE VOID EoamCtlTxPutPduHeader PROTO ((tEoamPduHeader *, UINT1 **));
PRIVATE VOID EoamCtlTxPutLocalTLV PROTO ((tEoamPortInfo *, UINT1 **));
PRIVATE VOID EoamCtlTxPutRemoteTLV PROTO ((tEoamPortInfo *, UINT1 **));
PRIVATE VOID EoamCtlTxPutPduData PROTO ((UINT1, tEoamPduData *, UINT1 *));
PRIVATE VOID EoamCtlTxPutEventTLV PROTO ((tEoamEventPduData *, UINT1 *));
PRIVATE VOID EoamCtlTxPutSymbolPeriod PROTO ((tEoamThresEventInfo *, UINT1 **));
PRIVATE VOID EoamCtlTxPutFrameEvent PROTO ((tEoamThresEventInfo *,
                                            UINT1 **pu1LinearBuf));
PRIVATE VOID EoamCtlTxPutFramePeriod PROTO ((tEoamThresEventInfo *,
                                             UINT1 **pu1LinearBuf));
PRIVATE VOID EoamCtlTxPutSecondSummary PROTO ((tEoamThresEventInfo *,
                                               UINT1 **));
PRIVATE VOID EoamCtlTxPutOrgEvent PROTO ((tEoamExtData *, UINT1 **));
PRIVATE VOID EoamCtlTxPutVarReq PROTO ((tEoamExtData *, UINT1 *));
PRIVATE INT4 EoamCtlTxFrame PROTO ((UINT4, UINT4, UINT1 *));

/******************************************************************************
 * Function Name      : EoamCtlTxHandlePduTmrExpiry
 *
 * Description        : This function is called when the global one-second
 *                      timer expires. It scans thru the list of EOAM
 *                      enabled interfaces and sends Info OAMPDU if no other
 *                      OAMPDU has been sent on that interface during the
 *                      last one second. This is to keep-alive the discovery
 *                      status
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
EoamCtlTxHandlePduTmrExpiry (VOID)
{
    tEoamPortInfo      *pEoamPortInfo = NULL;
    tTMO_DLL_NODE      *pEoamDllNode = NULL;

    EOAM_TRC_FN_ENTRY ();
    TMO_DLL_Scan (&(gEoamGlobalInfo.EoamEnaPortDllList),
                  pEoamDllNode, tTMO_DLL_NODE *)
    {
        pEoamPortInfo = (tEoamPortInfo *) pEoamDllNode;
        /* Check PDU Tx Cnt over the last second and send
           Information OAMPDU if no other OAMPDU has been sent */
        if (pEoamPortInfo->pEoamEnaPortInfo->u1PduTxCntPerSec == 0)
        {
            EoamCtlTxSendInfoPdu (pEoamPortInfo);
        }
        /* Reset count */
        pEoamPortInfo->pEoamEnaPortInfo->u1PduTxCntPerSec = 0;
    }

    EOAM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function Name      : EoamCtlTxFormPduHeader
 *
 * Description        : Forms the OAMPDU Header using the EOAM Port 
 *                      information and settings and stores it in pPduHeader
 *
 * Input(s)           : pPortInfo - Pointer to Port information
 *                      u1Code - Type of OAMPDU
 *
 * Output(s)          : pPduHeader - Pointer to OAMPDU Header
 *
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
EoamCtlTxFormPduHeader (tEoamPortInfo * pPortInfo, UINT1 u1PduCode,
                        tEoamPduHeader * pPduHeader)
{
    UINT1               au1EoamSlowMcastAddr[] =
        { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x02 };
    tEoamIfInfo         IfInfo;
    UINT2               u2Val = 0;

    EOAM_TRC_FN_ENTRY ();
    FSAP_ASSERT (pPortInfo != NULL);
    FSAP_ASSERT (pPduHeader != NULL);

    MEMSET (&IfInfo, 0, sizeof (tEoamIfInfo));
    /* Dest MAC address */
    MEMCPY (pPduHeader->DestMacAddr, &au1EoamSlowMcastAddr, MAC_ADDR_LEN);

    /* Get Src MAC address from CFA */
    if (EoamGetIfInfo (pPortInfo->u4Port, &IfInfo) != OSIX_SUCCESS)
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "EoamCtlTxFormPduHeader: Error while "
                   "getting Source Mac Address\n"));
        return;
    }

    MEMCPY (pPduHeader->SrcMacAddr, IfInfo.au1MacAddr, MAC_ADDR_LEN);
    /* Slow Protocol Type */
    pPduHeader->u2SlowProtoType = CFA_ENET_SLOW_PROTOCOL;

    /* OAM Subtype */
    pPduHeader->u1SubType = EOAM_PROTOCOL_SUBTYPE;

    /* Examine Local and remote settings and set Flag bits accordingly */
    if (pPortInfo->pEoamEnaPortInfo->CtrlReq.b1RemoteStateValid == OSIX_TRUE)
    {
        /* Copy Remote Info's Local stable and eval bits as remote stable
         * and eval bit */
        if ((pPortInfo->RemoteInfo.u2Flags & EOAM_LOCAL_STABLE_BITMASK) != 0)
        {
            u2Val |= EOAM_REMOTE_STABLE_BITMASK;
        }

        if ((pPortInfo->RemoteInfo.u2Flags & EOAM_LOCAL_EVAL_BITMASK) != 0)
        {
            u2Val |= EOAM_REMOTE_EVAL_BITMASK;
        }
    }

    if (pPortInfo->pEoamEnaPortInfo->CtrlInd.b1LocalStable == OSIX_TRUE)
    {
        u2Val |= EOAM_LOCAL_STABLE_BITMASK;
    }
    else
    {
        if (pPortInfo->pEoamEnaPortInfo->CtrlInd.b1LocalEvaluating == OSIX_TRUE)
        {
            u2Val |= EOAM_LOCAL_EVAL_BITMASK;
        }
    }

    if (pPortInfo->pEoamEnaPortInfo->CtrlReq.b1LocalCriticalEvent == OSIX_TRUE)
    {
        if (pPortInfo->ErrEventConfigInfo.u1CriticalEventEnable != EOAM_FALSE)
        {
            u2Val |= EOAM_CRITICAL_EVENT_BITMASK;
        }
    }

    if (pPortInfo->pEoamEnaPortInfo->CtrlReq.b1LocalDyingGasp == OSIX_TRUE)
    {
        if (pPortInfo->ErrEventConfigInfo.u1DyingGaspEnable != EOAM_FALSE)
        {
            u2Val |= EOAM_DYING_GASP_BITMASK;
        }
    }

    if (pPortInfo->pEoamEnaPortInfo->CtrlReq.u1LocalLinkStatus == OSIX_TRUE)
    {
        u2Val |= EOAM_LINK_FAULT_BITMASK;
    }

    pPduHeader->u2Flags = u2Val;
    pPduHeader->u1Code = u1PduCode;

    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EoamCtlTxConstructInfoPdu
 *
 * Description        : Construct the Information OAMPDU depending on 
 *                      current state and settings. This PDU will be stored 
 *                      globally for every EOAM enabled interface and will be
 *                      used to send every one second when the global timer 
 *                      expires
 *
 * Input(s)           : pPortInfo - Pointer to Port specific info
 *
 * Output(s)          : OSIX_SUCCESS/OSIX_FAILURE
 *
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC INT4
EoamCtlTxConstructInfoPdu (tEoamPortInfo * pPortInfo)
{
    UINT1              *pu1LinearBuf = NULL;
    tEoamPduHeader      InfoPduHeader;

    EOAM_TRC_FN_ENTRY ();
    FSAP_ASSERT (pPortInfo != NULL);
    FSAP_ASSERT (pPortInfo->pEoamEnaPortInfo != NULL);

    MEMSET (&InfoPduHeader, 0, sizeof (tEoamPduHeader));

    pu1LinearBuf = pPortInfo->pEoamEnaPortInfo->au1InfoPdu;
    MEMSET (pu1LinearBuf, 0, EOAM_MAX_DATA_SIZE);

    EoamCtlTxFormPduHeader (pPortInfo, EOAM_INFO, &InfoPduHeader);

    /* Copy the Pdu Header into the buffer */
    EoamCtlTxPutPduHeader (&InfoPduHeader, &pu1LinearBuf);

    if (pPortInfo->pEoamEnaPortInfo->CtrlInd.LocalPdu != EOAM_LF_INFO)
    {
        /* Form the contents of Local TLV */
        EoamCtlTxPutLocalTLV (pPortInfo, &pu1LinearBuf);

        if (pPortInfo->pEoamEnaPortInfo->CtrlReq.b1RemoteStateValid
            == OSIX_TRUE)
        {
            EoamCtlTxPutRemoteTLV (pPortInfo, &pu1LinearBuf);
        }

        /* If required, form the contents of Organization specific
         * information TLV */
        EoamPutOrgSpecInfoTLV (pPortInfo, &pu1LinearBuf);
    }
    EOAM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function Name      : EoamCtlTxSendInfoPdu
 *
 * Description        : Send Information OAMPDU on specified port
 *
 * Input(s)           : pPortInfo - Pointer to Port specific info
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
EoamCtlTxSendInfoPdu (tEoamPortInfo * pPortInfo)
{
    UINT4               u4PktSize = 0;

    EOAM_TRC_FN_ENTRY ();
    FSAP_ASSERT (pPortInfo != NULL);
    FSAP_ASSERT (pPortInfo->pEoamEnaPortInfo != NULL);

    if ((gu1EmIfShDownFlag != OSIX_TRUE) &&
        (pPortInfo->u1OperStatus == CFA_IF_DOWN))
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "EoamCtlTxSendInfoPdu:"
                   " Port: %u :: Port oper status down Tx failed \n",
                   pPortInfo->u4Port));
        return OSIX_FAILURE;
    }

    if (EoamCtlTxInfoPduRules (pPortInfo) != OSIX_SUCCESS)
    {
        EOAM_TRC ((ALL_FAILURE_TRC,
                   "EoamCtlTxSendInfoPdu: Port: %u :: Info PDU Tx rule failed",
                   pPortInfo->u4Port));
        return OSIX_FAILURE;
    }

    EOAM_TRC ((CONTROL_PLANE_TRC,
               "EoamCtlTxSendInfoPdu: Port: %u :: Handing over Info "
               "OAMPDU to Lower Layer...\n", pPortInfo->u4Port));

    u4PktSize = EoamCtlTxInfoPduSize (pPortInfo->pEoamEnaPortInfo);

#ifdef L2RED_WANTED
    /* HITLESS RESTART */
    if ((EOAM_HR_STDY_ST_REQ_RCVD () == OSIX_TRUE) &&
        (EOAM_HR_CHECK_STDY_ST_PKT (pPortInfo)))
    {
        EoamRedHRSendStdyStPkt (pPortInfo->pEoamEnaPortInfo->au1InfoPdu,
                                u4PktSize, (UINT2) pPortInfo->u4Port,
                                EOAM_PDU_TX_TIMEOUT);
    }
#endif

    if ((EoamCtlTxFrame (pPortInfo->u4Port, u4PktSize,
                         pPortInfo->pEoamEnaPortInfo->au1InfoPdu)) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (gu1EmIfShDownFlag == OSIX_TRUE)
    {
        pPortInfo->pEoamEnaPortInfo->CtrlReq.u1LocalLinkStatus =
            EOAM_LINK_STATUS_OK;
    }

    /* Increment Info Pdu tx counter */
    pPortInfo->PduStats.u4InformationTx++;

    /* Update the counter u1PduTxCntPerSec only if the 
     * Info PDU is a normal PDU as per Section 57.3.2.2 in 802.3ah
     */
    if ((pPortInfo->pEoamEnaPortInfo->CtrlReq.b1LocalDyingGasp != OSIX_TRUE) &&
        (pPortInfo->pEoamEnaPortInfo->CtrlReq.b1LocalCriticalEvent !=
         OSIX_TRUE))
    {
        pPortInfo->pEoamEnaPortInfo->u1PduTxCntPerSec++;
    }
    EOAM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function Name      : EoamCtlTxPduRequest
 *
 * Description        : This function is called from EOAM Client to send out
 *                      OAMPDU other than Information OAMPDUs. This function
 *                      allocates a CRU buffer and copies the contents of
 *                      OAMPDU from the client and sends it to CFA.
 *
 * Input(s)           : pPortInfo - Pointer to Port specific info
 *                      pPduHeader   - Pointer to OAMPDU Header
 *                      pPduData  - Pointer to OAMPDU Data
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS
 *                      OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
EoamCtlTxPduRequest (tEoamPortInfo * pPortInfo, tEoamPduHeader * pPduHeader,
                     tEoamPduData * pPduData)
{
    UINT4               u4PktSize = 0;
    UINT2               u2MaxOamPduSize = 0;
    UINT1              *pu1EmPduBuf = NULL;
    UINT1              *pu1LinearBuf = NULL;

    EOAM_TRC_FN_ENTRY ();
    FSAP_ASSERT (pPortInfo != NULL);
    FSAP_ASSERT (pPortInfo->pEoamEnaPortInfo != NULL);
    FSAP_ASSERT (pPduHeader != NULL);
    FSAP_ASSERT (pPduData != NULL);

    if (EoamCtlTxPduRules (pPortInfo->pEoamEnaPortInfo) != OSIX_SUCCESS)
    {
        EOAM_TRC ((ALL_FAILURE_TRC,
                   "EoamCtlTxPduRequest: Port: %u :: non-Info PDU Tx rule"
                   "failed", pPortInfo->u4Port));
        return OSIX_FAILURE;
    }

    u4PktSize = EoamCtlTxFindPktSize (pPduHeader->u1Code, pPduData);

    u2MaxOamPduSize = pPortInfo->LocalInfo.u2MaxOamPduSize;

    if (((pPortInfo->LocalInfo.u2MaxOamPduSize) >
         (pPortInfo->RemoteInfo.u2MaxOamPduSize)) &&
        (pPortInfo->RemoteInfo.u2MaxOamPduSize != 0))
    {
        u2MaxOamPduSize = pPortInfo->RemoteInfo.u2MaxOamPduSize;
    }

    if ((u4PktSize <= EOAM_PDU_HEADER_SIZE) ||
        (u4PktSize > (UINT4) (u2MaxOamPduSize - EOAM_ETH_FCS_LEN)))
    {
        EOAM_TRC ((ALL_FAILURE_TRC,
                   "EoamCtlTxPduRequest: Port: %u :: OAMPDU size "
                   "not valid\n", pPortInfo->u4Port));
        return OSIX_FAILURE;

    }
    if ((pu1EmPduBuf =
         (UINT1 *) (MemAllocMemBlk (gEoamGlobalInfo.EmLinearBufPoolId))) ==
        NULL)
    {
        EOAM_TRC ((ALL_FAILURE_TRC | EOAM_CRITICAL_TRC,
                   "EmLinearBufPoolId : Buffer allocation failed\r\n"));
        return OSIX_FAILURE;
    }

    MEMSET (pu1EmPduBuf, 0, EOAM_MAX_PDU_SIZE);

    pu1LinearBuf = pu1EmPduBuf;

    EoamCtlTxPutPduHeader (pPduHeader, &pu1LinearBuf);
    EoamCtlTxPutPduData (pPduHeader->u1Code, pPduData, pu1LinearBuf);

    if ((EoamCtlTxFrame (pPortInfo->u4Port, u4PktSize,
                         pu1EmPduBuf)) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (gEoamGlobalInfo.EmLinearBufPoolId,
                            (UINT1 *) pu1EmPduBuf);
        return OSIX_FAILURE;
    }

    pPortInfo->pEoamEnaPortInfo->u1PduTxCntPerSec++;

    MemReleaseMemBlock (gEoamGlobalInfo.EmLinearBufPoolId,
                        (UINT1 *) pu1EmPduBuf);

    EOAM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function Name      : EoamCtlTxInfoPduRules
 *
 * Description        : Validate Info OAMPDU Tx rules
 *
 * Input(s)           : pPortInfo - Pointer to Port specific info
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS
 *                      OSIX_FAILURE
 *****************************************************************************/
PRIVATE INT4
EoamCtlTxInfoPduRules (tEoamPortInfo * pPortInfo)
{
    EOAM_TRC_FN_ENTRY ();

    if ((pPortInfo->pEoamEnaPortInfo->CtrlInd.LocalPdu != EOAM_LF_INFO) &&
        (pPortInfo->pEoamEnaPortInfo->CtrlInd.LocalPdu != EOAM_INFO_ONLY) &&
        (pPortInfo->pEoamEnaPortInfo->CtrlInd.LocalPdu != EOAM_ANY))
    {
        EOAM_TRC ((ALL_FAILURE_TRC,
                   "EoamCtlTxInfoPduRules: Not sending Info PDU as localpdu "
                   "value is not valid\n"));
        /* Do not send info OAMPDUs when local_pdu is RX_INFO */
        return OSIX_FAILURE;
    }

    if ((pPortInfo->pEoamEnaPortInfo->CtrlReq.b1LocalDyingGasp !=
         OSIX_TRUE) &&
        (pPortInfo->pEoamEnaPortInfo->CtrlReq.b1LocalCriticalEvent !=
         OSIX_TRUE) &&
        (pPortInfo->pEoamEnaPortInfo->u1PduTxCntPerSec >= EOAM_MAX_PDU_PER_SEC))
    {
        EOAM_TRC ((ALL_FAILURE_TRC,
                   "EoamCtlTxInfoPduRules: Not sending Info PDU as MAX "
                   "number of pdu per second has already been transmitted\n"));
        /* Do not send OAMPDUs when already 10 OAMPDUs has been sent. 
         * This rule is not applicable for dying gasp and critical events */
        return OSIX_FAILURE;
    }

    EOAM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function Name      : EoamCtlTxPduRules
 *
 * Description        : This function checks PDU Tx rules for non-info 
 *                      OAMPDUs
 *
 * Input(s)           : pEoamEnaPortInfo - Pointer to EOAM enabled port info
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE INT4
EoamCtlTxPduRules (tEoamEnaPortInfo * pEoamEnaPortInfo)
{
    EOAM_TRC_FN_ENTRY ();

    if (pEoamEnaPortInfo->CtrlInd.LocalPdu != EOAM_ANY)
    {
        /* Do not send non-info OAMPDUs when local_pdu is not SEND_ANY */
        EOAM_TRC ((ALL_FAILURE_TRC,
                   "EoamCtlTxPduRules: Not sending non-Info PDU as "
                   "local_pdu is not SEND_ANY\n"));
        return OSIX_FAILURE;
    }

    if (pEoamEnaPortInfo->u1PduTxCntPerSec >= EOAM_MAX_PDU_PER_SEC)
    {
        /* Do not send OAMPDUs when already 10 OAMPDUs has been sent */
        EOAM_TRC ((ALL_FAILURE_TRC,
                   "EoamCtlTxPduRules: Not sending non-Info PDU as MAX "
                   "number of pdu per second has already been transmitted\n"));
        return OSIX_FAILURE;
    }

    EOAM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function Name      : EoamCtlTxInfoPduSize
 *
 * Description        : Returns Info PDU size
 *
 * Input(s)           : pPortInfo - Pointer to Port specific info
 *
 * Output(s)          : None
 *
 * Return Value(s)    : Info Pdu size
 *****************************************************************************/
PRIVATE UINT4
EoamCtlTxInfoPduSize (tEoamEnaPortInfo * pEoamEnaPortInfo)
{
    UINT4               u4PktSize = 0;

    EOAM_TRC_FN_ENTRY ();
    u4PktSize = EOAM_PDU_HEADER_SIZE;

    if ((pEoamEnaPortInfo->CtrlInd.LocalPdu) != EOAM_LF_INFO)
    {
        u4PktSize += EOAM_INFO_TLV_SIZE;

        if ((pEoamEnaPortInfo->CtrlReq.b1RemoteStateValid) == OSIX_TRUE)
        {
            u4PktSize += EOAM_INFO_TLV_SIZE;
        }
    }

    u4PktSize += EoamSizeofOrgSpecInfoTLV (pEoamEnaPortInfo);
    EOAM_TRC_FN_EXIT ();
    return u4PktSize;
}

/******************************************************************************
 * Function Name      : EoamCtlTxFindPktSize
 *
 * Description        : This function returns size of non-info OAMPDU
 *                      to be transmitted, based on PDU type and data
 *
 * Input(s)           : u1Code  - OAMPDU Type
 *                      pPduData  - Pointer to OAMPDU data
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OAMPDU Size
 *****************************************************************************/
PRIVATE UINT4
EoamCtlTxFindPktSize (UINT1 u1Code, tEoamPduData * pPduData)
{

    UINT4               u4PktSize = 0;
    UINT4               u4DataLen = 0;

    EOAM_TRC_FN_ENTRY ();
    switch (u1Code)
    {
        case EOAM_EVENT_NOTIFICATION:
            u4DataLen = EoamCtlTxEventTlvSize (pPduData);
            break;

        case EOAM_LB_CTRL:
            u4DataLen = 1;
            break;

        case EOAM_VAR_REQ:
            /* Fall thru */

        case EOAM_VAR_RESP:
            u4DataLen = pPduData->ExternalPduData.u4DataLen;
            break;

        case EOAM_ORG_SPEC:
            u4DataLen = pPduData->ExternalPduData.u4DataLen + EOAM_OUI_LENGTH;
            break;

        default:
            EOAM_TRC ((ALL_FAILURE_TRC,
                       "EoamCtlTxFindPktSize: "
                       "Invalid OAMPDU Type requested\n"));
            break;
    }

    u4PktSize = EOAM_PDU_HEADER_SIZE + u4DataLen;
    EOAM_TRC_FN_EXIT ();
    return u4PktSize;
}

/******************************************************************************
 * Function Name      : EoamCtlTxEventTlvSize
 *
 * Description        : This function returns size of Event Notification
 *                      TLV to be transmitted based on Event type
 *
 * Input(s)           : pPduData  - Pointer to OAMPDU data
 *
 * Output(s)          : None
 *
 * Return Value(s)    : Event TLV length
 *****************************************************************************/
PRIVATE UINT4
EoamCtlTxEventTlvSize (tEoamPduData * pPduData)
{
    UINT4               u4DataLen = 0;

    EOAM_TRC_FN_ENTRY ();

    if (pPduData->EvtPduData.u1EvtType == EOAM_ORG_SPEC_EVENT)
    {
        /* Organization specific Event */
        /* Org value size + OUI size + Type,Len size + 
         * sequence number size */
        u4DataLen = pPduData->EvtPduData.OrgEvtData.u4DataLen +
            EOAM_OUI_LENGTH + sizeof (UINT2) + sizeof (UINT2);
    }
    else
    {
        switch (pPduData->EvtPduData.u1EventType)
        {
            case EOAM_ERRORED_SYMBOL_EVENT_TLV:
                /* TLV size + sequence number size */
                u4DataLen = EOAM_ERR_SYMBOL_PERIOD_TLV_SIZE + sizeof (UINT2);
                break;
            case EOAM_ERRORED_FRAME_EVENT_TLV:
                u4DataLen = EOAM_ERR_FRAME_TLV_SIZE + sizeof (UINT2);
                break;
            case EOAM_ERRORED_FRAME_PERIOD_EVENT_TLV:
                u4DataLen = EOAM_ERR_FRAME_PERIOD_TLV_SIZE + sizeof (UINT2);
                break;
            case EOAM_ERRORED_FRAME_SECONDS_EVENT_TLV:
                u4DataLen = EOAM_ERR_FRAME_SEC_SUMMARY_TLV_SIZE +
                    sizeof (UINT2);
                break;
            default:
                EOAM_TRC ((ALL_FAILURE_TRC,
                           "EoamCtlTxEventTlvSize: "
                           "Invalid Event type requested\n"));
                break;
        }
    }
    return u4DataLen;
}

/******************************************************************************
 * Function Name      : EoamCtlTxPutPduHeader
 *
 * Description        : This function copies the OAMPDU Header information
 *                      to the linear buffer
 *
 * Input(s)           : pPduHeader  - Pointer to OAMPDU Header
 *
 * Output(s)          : *ppu1LinearBuf - pointer to linear buffer
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE VOID
EoamCtlTxPutPduHeader (tEoamPduHeader * pPduHeader, UINT1 **ppu1LinearBuf)
{
    EOAM_TRC_FN_ENTRY ();

    MEMCPY (*ppu1LinearBuf, &pPduHeader->DestMacAddr, MAC_ADDR_LEN);
    *ppu1LinearBuf += MAC_ADDR_LEN;

    MEMCPY (*ppu1LinearBuf, &pPduHeader->SrcMacAddr, MAC_ADDR_LEN);
    *ppu1LinearBuf += MAC_ADDR_LEN;

    EOAM_PUT_2BYTE (*ppu1LinearBuf, pPduHeader->u2SlowProtoType);

    EOAM_PUT_1BYTE (*ppu1LinearBuf, pPduHeader->u1SubType);

    EOAM_PUT_2BYTE (*ppu1LinearBuf, pPduHeader->u2Flags);

    EOAM_PUT_1BYTE (*ppu1LinearBuf, pPduHeader->u1Code);

    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EoamCtlTxPutLocalTLV
 *
 * Description        : Copy Local TLV information to linear buffer
 *
 * Input(s)           : pPduHeader  - Pointer to OAMPDU Header
 *
 * Output(s)          : *ppu1LinearBuf - pointer to linear buffer
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE VOID
EoamCtlTxPutLocalTLV (tEoamPortInfo * pPortInfo, UINT1 **ppu1LinearBuf)
{
    UINT1               u1Val = 0;

    EOAM_TRC_FN_ENTRY ();
    EOAM_PUT_1BYTE (*ppu1LinearBuf, EOAM_LOCAL_INFO_TLV);

    EOAM_PUT_1BYTE (*ppu1LinearBuf, EOAM_INFO_TLV_SIZE);

    EOAM_PUT_1BYTE (*ppu1LinearBuf, EOAM_VERSION);

    EOAM_PUT_2BYTE (*ppu1LinearBuf, pPortInfo->LocalInfo.u2ConfigRevision);

    if (pPortInfo->pEoamEnaPortInfo->CtrlReq.LocalMuxAction == EOAM_DISCARD)
    {
        u1Val |= EOAM_STATE_MUX_ACTION;
    }

    if (pPortInfo->pEoamEnaPortInfo->CtrlReq.LocalParAction == EOAM_DISCARD)
    {
        u1Val |= EOAM_STATE_PARSER_DISCARD;
    }
    else
    {
        if (pPortInfo->pEoamEnaPortInfo->CtrlReq.LocalParAction
            == EOAM_LOOPBACK)
        {
            u1Val |= EOAM_STATE_PARSER_LB;
        }
    }

    EOAM_PUT_1BYTE (*ppu1LinearBuf, u1Val);

    /* Functions supported */
    u1Val = pPortInfo->LocalInfo.u1FnsSupportedBmp;
    u1Val = (UINT1) (u1Val << 1);

    /* Set Mode configured */
    if (pPortInfo->LocalInfo.u1Mode == EOAM_MODE_ACTIVE)
    {
        u1Val = (UINT1) (u1Val | EOAM_MODE_BITMASK);
    }
    else
    {
        u1Val = (UINT1) (u1Val & ~EOAM_MODE_BITMASK);
    }

    EOAM_PUT_1BYTE (*ppu1LinearBuf, u1Val);

    EOAM_PUT_2BYTE (*ppu1LinearBuf, pPortInfo->LocalInfo.u2MaxOamPduSize);

    MEMCPY (*ppu1LinearBuf, &(gEoamGlobalInfo.au1OUI), EOAM_OUI_LENGTH);
    *ppu1LinearBuf += EOAM_OUI_LENGTH;

    EOAM_PUT_4BYTE (*ppu1LinearBuf, gEoamGlobalInfo.u4VendorSpecInfo);
    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EoamCtlTxPutRemoteTLV
 *
 * Description        : Copy Remote TLV information to linear buffer
 *
 * Input(s)           : pPduHeader  - Pointer to OAMPDU Header
 *
 * Output(s)          : *ppu1LinearBuf - pointer to linear buffer
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE VOID
EoamCtlTxPutRemoteTLV (tEoamPortInfo * pPortInfo, UINT1 **ppu1LinearBuf)
{
    UINT1               u1Val = 0;

    EOAM_TRC_FN_ENTRY ();
    /* Form the contents of Remote TLV */
    EOAM_PUT_1BYTE (*ppu1LinearBuf, EOAM_REMOTE_INFO_TLV);

    EOAM_PUT_1BYTE (*ppu1LinearBuf, EOAM_INFO_TLV_SIZE);

    EOAM_PUT_1BYTE (*ppu1LinearBuf, EOAM_VERSION);

    EOAM_PUT_2BYTE (*ppu1LinearBuf, pPortInfo->RemoteInfo.u2ConfigRevision);

    EOAM_PUT_1BYTE (*ppu1LinearBuf, pPortInfo->RemoteInfo.u1State);

    u1Val = pPortInfo->RemoteInfo.u1FnsSupportedBmp;
    u1Val = (UINT1) (u1Val << 1);

    if (pPortInfo->RemoteInfo.u1Mode == EOAM_MODE_ACTIVE)
    {
        u1Val = (UINT1) (u1Val | EOAM_MODE_BITMASK);
    }
    else
    {
        u1Val = (UINT1) (u1Val & ~EOAM_MODE_BITMASK);
    }

    EOAM_PUT_1BYTE (*ppu1LinearBuf, u1Val);

    EOAM_PUT_2BYTE (*ppu1LinearBuf, pPortInfo->RemoteInfo.u2MaxOamPduSize);

    MEMCPY (*ppu1LinearBuf, pPortInfo->RemoteInfo.au1VendorOui,
            EOAM_OUI_LENGTH);
    *ppu1LinearBuf += EOAM_OUI_LENGTH;

    EOAM_PUT_4BYTE (*ppu1LinearBuf, pPortInfo->RemoteInfo.u4VendorInfo);

    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EoamCtlTxPutPduData
 *
 * Description        : This function copies the non-OAMPDU data to the linear
 *                      buffer in the OAMPDU format based on the PDU type
 *
 * Input(s)           : u1PduCode - Type of OAMPDU
 *                      pPduData  - Pointer to PDU data
 *
 * Output(s)          : pu1LinearBuf
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE VOID
EoamCtlTxPutPduData (UINT1 u1PduCode, tEoamPduData * pEoamData,
                     UINT1 *pu1LinearBuf)
{
    EOAM_TRC_FN_ENTRY ();

    switch (u1PduCode)
    {
        case EOAM_EVENT_NOTIFICATION:
            EoamCtlTxPutEventTLV (&(pEoamData->EvtPduData), pu1LinearBuf);
            break;

        case EOAM_LB_CTRL:
            EOAM_PUT_1BYTE (pu1LinearBuf, (UINT1) pEoamData->LBCmd);
            break;

        case EOAM_VAR_REQ:
            if (pEoamData->ExternalPduData.pu1Data != NULL)
            {
                EoamCtlTxPutVarReq (&(pEoamData->ExternalPduData),
                                    pu1LinearBuf);
            }
            break;

        case EOAM_ORG_SPEC:
            MEMCPY (pu1LinearBuf, gEoamGlobalInfo.au1OUI, EOAM_OUI_LENGTH);
            pu1LinearBuf += EOAM_OUI_LENGTH;
            if (pEoamData->ExternalPduData.pu1Data != NULL)
            {
                MEMCPY (pu1LinearBuf, pEoamData->ExternalPduData.pu1Data,
                        pEoamData->ExternalPduData.u4DataLen);
            }
            break;

        case EOAM_VAR_RESP:
            /* Copying var response and org specific data without host
             * to network conversions. This could be done only if the
             * data types are known */
            if (pEoamData->ExternalPduData.pu1Data != NULL)
            {
                MEMCPY (pu1LinearBuf, pEoamData->ExternalPduData.pu1Data,
                        pEoamData->ExternalPduData.u4DataLen);
            }
            break;

        default:
            EOAM_TRC ((ALL_FAILURE_TRC,
                       "EoamCtlTxPutPduStruct: Unsupported OAMPDU Type"
                       " received\r\n"));
            break;
    }

    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EoamCtlTxPutEventTLV
 *
 * Description        : This function copies the Error event into the linear
 *                      buffer in the OAMPDU format based on the error type
 *
 * Input(s)           : pEventData - Pointer to PDU data
 *
 * Output(s)          : pu1LinearBuf
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE VOID
EoamCtlTxPutEventTLV (tEoamEventPduData * pEventData, UINT1 *pu1LinearBuf)
{

    EOAM_TRC_FN_ENTRY ();

    EOAM_PUT_2BYTE (pu1LinearBuf, pEventData->u2SeqNum);

    if (pEventData->u1EvtType == EOAM_THRESH_EVENT)
    {
        /* Standard Threshold Event */

        EOAM_PUT_1BYTE (pu1LinearBuf, pEventData->u1EventType);

        switch (pEventData->u1EventType)
        {
            case EOAM_ERRORED_SYMBOL_EVENT_TLV:

                EoamCtlTxPutSymbolPeriod (&(pEventData->ErrSymblPrdEvnt),
                                          &pu1LinearBuf);

                break;

            case EOAM_ERRORED_FRAME_EVENT_TLV:

                EoamCtlTxPutFrameEvent (&(pEventData->ErrFrmEvnt),
                                        &pu1LinearBuf);

                break;

            case EOAM_ERRORED_FRAME_PERIOD_EVENT_TLV:

                EoamCtlTxPutFramePeriod (&(pEventData->ErrFrmPrdEvnt),
                                         &pu1LinearBuf);

                break;

            case EOAM_ERRORED_FRAME_SECONDS_EVENT_TLV:

                EoamCtlTxPutSecondSummary (&(pEventData->ErrFrmSecSumryEvnt),
                                           &pu1LinearBuf);
                break;

            default:
                EOAM_TRC ((ALL_FAILURE_TRC,
                           "EoamCtlTxPutEventTLV: Unsupported Error Event Type"
                           " requested\r\n"));
                break;
        }
    }
    else
    {
        /* Org Specific Event TLV */
        EoamCtlTxPutOrgEvent (&(pEventData->OrgEvtData), &pu1LinearBuf);
    }
    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EoamCtlTxPutSymbolPeriod
 *
 * Description        : Copy symbol period data to linear buffer
 *
 * Input(s)           : pEventTLV - Pointer to Event TLV data
 *
 * Output(s)          : **ppu1LinearBuf
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE VOID
EoamCtlTxPutSymbolPeriod (tEoamThresEventInfo * pEventTLV, UINT1 **pu1LinearBuf)
{
    UINT2               u2Val = 0;
    UINT4               u4Val = 0;

    EOAM_TRC_FN_ENTRY ();

    EOAM_PUT_1BYTE (*pu1LinearBuf, EOAM_ERR_SYMBOL_PERIOD_TLV_SIZE);

    u2Val = (UINT2) pEventTLV->u4EventLogTimestamp;
    EOAM_PUT_2BYTE (*pu1LinearBuf, u2Val);

    u4Val = FSAP_U8_FETCH_HI (&(pEventTLV->u8EventWindow));
    EOAM_PUT_4BYTE (*pu1LinearBuf, u4Val);

    u4Val = FSAP_U8_FETCH_LO (&(pEventTLV->u8EventWindow));
    EOAM_PUT_4BYTE (*pu1LinearBuf, u4Val);

    u4Val = FSAP_U8_FETCH_HI (&(pEventTLV->u8EventThreshold));
    EOAM_PUT_4BYTE (*pu1LinearBuf, u4Val);

    u4Val = FSAP_U8_FETCH_LO (&(pEventTLV->u8EventThreshold));
    EOAM_PUT_4BYTE (*pu1LinearBuf, u4Val);

    u4Val = FSAP_U8_FETCH_HI (&(pEventTLV->u8EventValue));
    EOAM_PUT_4BYTE (*pu1LinearBuf, u4Val);

    u4Val = FSAP_U8_FETCH_LO (&(pEventTLV->u8EventValue));
    EOAM_PUT_4BYTE (*pu1LinearBuf, u4Val);

    u4Val = FSAP_U8_FETCH_HI (&(pEventTLV->u8ErrorRunningTotal));
    EOAM_PUT_4BYTE (*pu1LinearBuf, u4Val);

    u4Val = FSAP_U8_FETCH_LO (&(pEventTLV->u8ErrorRunningTotal));
    EOAM_PUT_4BYTE (*pu1LinearBuf, u4Val);

    EOAM_PUT_4BYTE (*pu1LinearBuf, pEventTLV->u4EventRunningTotal);

    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EoamCtlTxPutFrameEvent
 *
 * Description        : Copy Frame event data to linear buffer
 *
 * Input(s)           : pEventTLV - Pointer to Event TLV data
 *
 * Output(s)          : **ppu1LinearBuf
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE VOID
EoamCtlTxPutFrameEvent (tEoamThresEventInfo * pEventTLV, UINT1 **ppu1LinearBuf)
{
    UINT2               u2Val = 0;
    UINT4               u4Val = 0;

    EOAM_TRC_FN_ENTRY ();
    EOAM_PUT_1BYTE (*ppu1LinearBuf, EOAM_ERR_FRAME_TLV_SIZE);

    u2Val = (UINT2) pEventTLV->u4EventLogTimestamp;
    EOAM_PUT_2BYTE (*ppu1LinearBuf, u2Val);

    u2Val = (UINT2) FSAP_U8_FETCH_LO (&(pEventTLV->u8EventWindow));
    EOAM_PUT_2BYTE (*ppu1LinearBuf, u2Val);

    u4Val = FSAP_U8_FETCH_LO (&(pEventTLV->u8EventThreshold));
    EOAM_PUT_4BYTE (*ppu1LinearBuf, u4Val);

    u4Val = FSAP_U8_FETCH_LO (&(pEventTLV->u8EventValue));
    EOAM_PUT_4BYTE (*ppu1LinearBuf, u4Val);

    u4Val = FSAP_U8_FETCH_HI (&(pEventTLV->u8ErrorRunningTotal));
    EOAM_PUT_4BYTE (*ppu1LinearBuf, u4Val);

    u4Val = FSAP_U8_FETCH_LO (&(pEventTLV->u8ErrorRunningTotal));
    EOAM_PUT_4BYTE (*ppu1LinearBuf, u4Val);

    EOAM_PUT_4BYTE (*ppu1LinearBuf, pEventTLV->u4EventRunningTotal);

    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EoamCtlTxPutFramePeriod
 *
 * Description        : Copy Frame period data to linear buffer
 *
 * Input(s)           : pEventTLV - Pointer to Event TLV data
 *
 * Output(s)          : **ppu1LinearBuf
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE VOID
EoamCtlTxPutFramePeriod (tEoamThresEventInfo * pEventTLV, UINT1 **ppu1LinearBuf)
{
    UINT2               u2Val = 0;
    UINT4               u4Val = 0;

    EOAM_TRC_FN_ENTRY ();
    EOAM_PUT_1BYTE (*ppu1LinearBuf, EOAM_ERR_FRAME_PERIOD_TLV_SIZE);

    u2Val = (UINT2) pEventTLV->u4EventLogTimestamp;
    EOAM_PUT_2BYTE (*ppu1LinearBuf, u2Val);

    u4Val = FSAP_U8_FETCH_LO (&(pEventTLV->u8EventWindow));
    EOAM_PUT_4BYTE (*ppu1LinearBuf, u4Val);

    u4Val = FSAP_U8_FETCH_LO (&(pEventTLV->u8EventThreshold));
    EOAM_PUT_4BYTE (*ppu1LinearBuf, u4Val);

    u4Val = FSAP_U8_FETCH_LO (&(pEventTLV->u8EventValue));
    EOAM_PUT_4BYTE (*ppu1LinearBuf, u4Val);

    u4Val = FSAP_U8_FETCH_HI (&(pEventTLV->u8ErrorRunningTotal));
    EOAM_PUT_4BYTE (*ppu1LinearBuf, u4Val);

    u4Val = FSAP_U8_FETCH_LO (&(pEventTLV->u8ErrorRunningTotal));
    EOAM_PUT_4BYTE (*ppu1LinearBuf, u4Val);

    EOAM_PUT_4BYTE (*ppu1LinearBuf, pEventTLV->u4EventRunningTotal);

    EOAM_TRC_FN_EXIT ();
    return;

}

/******************************************************************************
 * Function Name      : EoamCtlTxPutSecondSummary
 *
 * Description        : Copy Seconds summary event data to linear buffer
 *
 * Input(s)           : pEventTLV - Pointer to Event TLV data
 *
 * Output(s)          : **ppu1LinearBuf
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE VOID
EoamCtlTxPutSecondSummary (tEoamThresEventInfo * pEventTLV,
                           UINT1 **ppu1LinearBuf)
{
    UINT4               u4Val = 0;
    UINT2               u2Val = 0;

    EOAM_TRC_FN_ENTRY ();
    EOAM_PUT_1BYTE (*ppu1LinearBuf, EOAM_ERR_FRAME_SEC_SUMMARY_TLV_SIZE);

    u2Val = (UINT2) pEventTLV->u4EventLogTimestamp;
    EOAM_PUT_2BYTE (*ppu1LinearBuf, u2Val);

    u2Val = (UINT2) FSAP_U8_FETCH_LO (&(pEventTLV->u8EventWindow));
    EOAM_PUT_2BYTE (*ppu1LinearBuf, u2Val);

    u2Val = (UINT2) FSAP_U8_FETCH_LO (&(pEventTLV->u8EventThreshold));

    EOAM_PUT_2BYTE (*ppu1LinearBuf, u2Val);

    u2Val = (UINT2) FSAP_U8_FETCH_LO (&(pEventTLV->u8EventValue));
    EOAM_PUT_2BYTE (*ppu1LinearBuf, u2Val);

    u4Val = FSAP_U8_FETCH_LO (&(pEventTLV->u8ErrorRunningTotal));

    EOAM_PUT_4BYTE (*ppu1LinearBuf, u4Val);

    EOAM_PUT_4BYTE (*ppu1LinearBuf, pEventTLV->u4EventRunningTotal);

    EOAM_TRC_FN_EXIT ();
    return;

}

/******************************************************************************
 * Function Name      : EoamCtlTxPutOrgEvent
 *
 * Description        : Copy Organization specific Event TLV Data into Pdu
 *                      buffer
 *
 * Input(s)           : pEventTLV - Pointer to Event TLV data
 *
 * Output(s)          : **ppu1LinearBuf
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE VOID
EoamCtlTxPutOrgEvent (tEoamExtData * pOrgEventInfo, UINT1 **ppu1LinearBuf)
{

    EOAM_TRC_FN_ENTRY ();

    EOAM_PUT_1BYTE (*ppu1LinearBuf, EOAM_ORG_SPEC_LINK_EVENT);

    /* Length includes the total length of Type, Length and Value fields.
     * This is a one-octet field as specified in section 57.5.2.3 of
     * IEEE 802.3ah-2004 */
    EOAM_PUT_1BYTE (*ppu1LinearBuf,
                    (UINT1) (pOrgEventInfo->u4DataLen + sizeof (UINT2) +
                             EOAM_OUI_LENGTH));

    MEMCPY (*ppu1LinearBuf, gEoamGlobalInfo.au1OUI, EOAM_OUI_LENGTH);
    *ppu1LinearBuf += EOAM_OUI_LENGTH;

    /* Copying org specific data without host
     * to network conversions. This could be done only if the
     * data types are known */
    if (pOrgEventInfo->pu1Data != NULL)
    {
        MEMCPY (*ppu1LinearBuf, pOrgEventInfo->pu1Data,
                pOrgEventInfo->u4DataLen);
    }

    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EoamCtlTxPutVarReq
 *
 * Description        : This function copies the variable request into the
 *                      linear buffer taking care of host to network
 *                      conversions
 *
 * Input(s)           : pEoamVarReq - Pointer to Variable request data
 *
 * Output(s)          : pu1LinearBuf
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE VOID
EoamCtlTxPutVarReq (tEoamExtData * pEoamVarReq, UINT1 *pu1LinearBuf)
{
    UINT4               u4DataLen = 0;
    UINT1              *pu1Ptr = NULL;
    UINT2               u2Val = 0;
    UINT1               u1Val = 0;

    EOAM_TRC_FN_ENTRY ();
    u4DataLen = pEoamVarReq->u4DataLen;
    pu1Ptr = pEoamVarReq->pu1Data;

    while (u4DataLen >= EOAM_VAR_DESC_LEN)
    {
        /* Copy branch - 1 byte */
        u1Val = *pu1Ptr;
        EOAM_PUT_1BYTE (pu1LinearBuf, u1Val);
        pu1Ptr += sizeof (UINT1);

        /* Copy leaf - 2 bytes */
        u2Val = *(UINT2 *) (VOID *) pu1Ptr;
        EOAM_PUT_2BYTE (pu1LinearBuf, u2Val);
        pu1Ptr += sizeof (UINT2);

        u4DataLen = u4DataLen - EOAM_VAR_DESC_LEN;
    }

    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EoamCtlTxFrame
 *
 * Description        : Allocate CRU buffer, copy data and transmit
 *
 * Input(s)           : u4Port - Port on which to transmit the packet
 *                      u4PktSize - Packet Size
 *                      pu1LinearBuf - pointer to linear buffer containing
 *                                     packet content
 *
 * Output(s)          : None
 *
 * Return Value(s)    :OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
PRIVATE INT4
EoamCtlTxFrame (UINT4 u4Port, UINT4 u4PktSize, UINT1 *pu1LinearBuf)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;

    EOAM_TRC_FN_ENTRY ();
    if (EOAM_RM_NODE_STATUS () != EOAM_ACTIVE_NODE)
    {
        /* Allow Pkt Tx only on active node */
        EOAM_TRC ((EOAM_RED_TRC,
                   "EoamCtlTxFrame: Port: %u :: Attempt to send OAMPDU "
                   "when node is not Active\r\n", u4Port));
        return OSIX_SUCCESS;
    }

#ifdef CMIPINTF_WANTED
    /* When more than 10 Mib Varaible requests are send to peer, 
     * Received response has malformed packet. 
     * To solve that issue,4 bytes are added if the packet 
     * is having variable response code*/
    if ((u4PktSize >= CFA_ENET_MIN_UNTAGGED_FRAME_SIZE) &&
        (u4PktSize < EOAM_MAX_PDU_SIZE - EOAM_ETH_FCS_LEN))
    {
        u4PktSize = u4PktSize + EOAM_MIN_BLOCK_SIZE;
    }
#endif

    if ((pBuf = CRU_BUF_Allocate_MsgBufChain (u4PktSize, 0)) == NULL)
    {

        EOAM_TRC ((BUFFER_TRC | ALL_FAILURE_TRC,
                   "EoamCtlTxFrame: Port: %u :: Buffer "
                   "Allocation failed\n", u4Port));
        return OSIX_FAILURE;
    }

    if (CRU_BUF_Copy_OverBufChain (pBuf, pu1LinearBuf, 0,
                                   u4PktSize) == CRU_FAILURE)
    {

        EOAM_TRC ((BUFFER_TRC | ALL_FAILURE_TRC,
                   "EoamCtlTxFrame: Port: %u :: Copying into "
                   "Buffer failed\n", u4Port));
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return OSIX_FAILURE;
    }

    EOAM_PKT_DUMP (DUMP_TRC, pBuf, u4PktSize,
                   "EoamCtlTxFrame: " "Sending out OAMPDU to lower layer...\n");

    if (EoamTransmitFrame (pBuf, u4Port, u4PktSize) != OSIX_SUCCESS)
    {
        EOAM_TRC ((ALL_FAILURE_TRC,
                   "EoamCtlTxPkt: Port: %u :: Transmit OAMPDU failed\n",
                   u4Port));
        return OSIX_FAILURE;
    }

    EOAM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  emctltx.c                      */
/*-----------------------------------------------------------------------*/
