/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emlmif.c,v 1.10 2012/10/29 12:02:27 siva Exp $
 *
 * Description: This file contains Ethernet OAM Link monitoring task 
 *              Port related functions
 *****************************************************************************/
#include "eminc.h"
#include "emlmdefn.h"
#include "emlmglob.h"

PRIVATE VOID EoamLmIfCopySymPeriod PROTO ((tEoamLmEventConfigInfo *,
                                           UINT1, FS_UINT8, FS_UINT8));
PRIVATE VOID EoamLmIfCopyFrameEvt PROTO ((tEoamLmEventConfigInfo *,
                                          UINT1, FS_UINT8, FS_UINT8));
PRIVATE VOID EoamLmIfCopyFramePeriod PROTO ((tEoamLmEventConfigInfo *,
                                             UINT1, FS_UINT8, FS_UINT8));
PRIVATE VOID EoamLmIfCopySecSumm PROTO ((tEoamLmEventConfigInfo *,
                                         UINT1, FS_UINT8, FS_UINT8));
PRIVATE tEoamLmEnaPortDllNode *EoamLmIfFindNode PROTO ((UINT4));

/******************************************************************************
 * Function     : EoamLmIfAdd
 *
 * Description  : Add Port to EOAM Link Monitoring DLL
 *
 * Input        : u4Port - Port Index that needs be added
 *
 * Output       : None
 *
 * Returns      : OSIX_SUCCESS
 *                OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
EoamLmIfAdd (UINT4 u4Port)
{
    tEoamLmEnaPortDllNode *pEnaPortNode = NULL;

    EOAM_TRC_FN_ENTRY ();

    EOAM_TRC ((EOAM_LM_TRC,
               "EoamLmIfAdd: Adding Port %d to Link Monitoring "
               "list\r\n", u4Port));

    /* Take DataLock for accessing LM port info */
    OsixSemTake (gEoamLmGlobalInfo.SemId);

    if ((pEnaPortNode = (tEoamLmEnaPortDllNode *) (MemAllocMemBlk
                                                   (gEoamLmGlobalInfo.
                                                    LmEnaDllNodePoolId))) ==
        NULL)
    {
        EOAM_TRC ((ALL_FAILURE_TRC | EOAM_CRITICAL_TRC,
                   "EoamLmIfAdd : Buffer allocation failed\r\n"));
        OsixSemGive (gEoamLmGlobalInfo.SemId);
        return OSIX_FAILURE;

    }

    MEMSET (pEnaPortNode, 0, sizeof (tEoamLmEnaPortDllNode));

    TMO_DLL_Init_Node (&(pEnaPortNode->NodeLink));
    TMO_DLL_Add (&(gEoamLmGlobalInfo.LmEoamEnaPortDllList),
                 &(pEnaPortNode->NodeLink));

    pEnaPortNode->LmEvtCfgInfo.u4Port = u4Port;
    pEnaPortNode->LmErrInfo.b1InitFlag = OSIX_TRUE;

    if (TMO_DLL_Count (&gEoamLmGlobalInfo.LmEoamEnaPortDllList) == 1)
    {
        /* First Port created - start timer */
        /* Start timer only if Node is active */
        if (EOAM_RM_NODE_STATUS () == EOAM_ACTIVE_NODE)
        {
            if (TmrStartTimer (gEoamLmGlobalInfo.LmTmrList,
                               &gEoamLmGlobalInfo.LmTmrNode,
                               EOAMLM_POLLING_INTERVAL) != TMR_SUCCESS)
            {
                EOAM_TRC ((CONTROL_PLANE_TRC | OS_RESOURCE_TRC |
                           ALL_FAILURE_TRC,
                           "EoamLmIfAdd: EOAM LM Timer Start failed \r\n"));
                OsixSemGive (gEoamLmGlobalInfo.SemId);
                return OSIX_FAILURE;
            }
            else
            {
                EOAM_TRC ((CONTROL_PLANE_TRC | OS_RESOURCE_TRC,
                           "EoamLmIfAdd: EOAM LM Timer Started "
                           "successfully\r\n"));
            }
        }
    }

    OsixSemGive (gEoamLmGlobalInfo.SemId);
    EOAM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function     : EoamLmIfDelete
 *
 * Description  : This function deletes a port from the EOAM LM DLL
 *
 * Input        : u4Port - Port Index that needs be added
 *
 * Output       : None
 *
 * Returns      : OSIX_SUCCESS
 *                OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
EoamLmIfDelete (UINT4 u4Port)
{
    tEoamLmEnaPortDllNode *pListNode = NULL;

    EOAM_TRC_FN_ENTRY ();
    EOAM_TRC ((EOAM_LM_TRC,
               "Deleting port %d to Link Monitoring list\r\n", u4Port));

    /* Take DataLock for accessing LM port info */
    OsixSemTake (gEoamLmGlobalInfo.SemId);

    pListNode = EoamLmIfFindNode (u4Port);

    if (pListNode == NULL)
    {
        OsixSemGive (gEoamLmGlobalInfo.SemId);
        return OSIX_FAILURE;
    }

    TMO_DLL_Delete (&(gEoamLmGlobalInfo.LmEoamEnaPortDllList),
                    &(pListNode->NodeLink));

    MemReleaseMemBlock (gEoamLmGlobalInfo.LmEnaDllNodePoolId,
                        (UINT1 *) pListNode);

    if (TMO_DLL_Count (&gEoamLmGlobalInfo.LmEoamEnaPortDllList) == 0)
    {
        /* Last port deleted - stop timer */
        if ((TmrStopTimer (gEoamLmGlobalInfo.LmTmrList,
                           &gEoamLmGlobalInfo.LmTmrNode)) != TMR_SUCCESS)
        {
            EOAM_TRC ((CONTROL_PLANE_TRC | OS_RESOURCE_TRC |
                       ALL_FAILURE_TRC,
                       "EoamLmIfDelete: Stopping Timer FAILED\n"));
        }
    }
    /* Release the DataLock for accessing LM info */
    OsixSemGive (gEoamLmGlobalInfo.SemId);
    EOAM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function     : EoamLmIfCopyEventParams
 *
 * Description  : This function updates Link monitoring configurations of a
 *                port
 *
 * Input        : u4Port - Port Index that needs to be updated
 *                u4Event - Error event to update
 *                u1Endis - Error event ENABLE / DISABLE
 *                          EOAM_ENABLED/EOAM_DISABLED
 *                Window  - Error event Window configuration
 *                Threshold - Error event Threshold configuration
 *
 * Output       : None
 *
 * Returns      : OSIX_SUCCESS
 *                OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
EoamLmIfCopyEventParams (UINT4 u4Port, UINT2 u2Event, UINT1 u1Endis,
                         FS_UINT8 u8Window, FS_UINT8 u8Threshold)
{
    INT4                i4RetVal = OSIX_SUCCESS;
    tEoamLmEnaPortDllNode *pListNode = NULL;

    EOAM_TRC_FN_ENTRY ();
    EOAM_TRC ((EOAM_LM_TRC,
               "Updating Link monitoring configuration for %d\r\n", u4Port));

    OsixSemTake (gEoamLmGlobalInfo.SemId);

    pListNode = EoamLmIfFindNode (u4Port);

    if (pListNode == NULL)
    {
        OsixSemGive (gEoamLmGlobalInfo.SemId);
        return OSIX_FAILURE;
    }

    switch (u2Event)
    {
        case EOAM_ERRORED_SYMBOL_EVENT_TLV:
            EoamLmIfCopySymPeriod (&(pListNode->LmEvtCfgInfo),
                                   u1Endis, u8Window, u8Threshold);
            break;

        case EOAM_ERRORED_FRAME_EVENT_TLV:
            EoamLmIfCopyFrameEvt (&(pListNode->LmEvtCfgInfo),
                                  u1Endis, u8Window, u8Threshold);
            break;

        case EOAM_ERRORED_FRAME_PERIOD_EVENT_TLV:
            EoamLmIfCopyFramePeriod (&(pListNode->LmEvtCfgInfo),
                                     u1Endis, u8Window, u8Threshold);
            break;

        case EOAM_ERRORED_FRAME_SECONDS_EVENT_TLV:
            EoamLmIfCopySecSumm (&(pListNode->LmEvtCfgInfo),
                                 u1Endis, u8Window, u8Threshold);
            break;

        default:
        {
            EOAM_TRC ((ALL_FAILURE_TRC | EOAM_LM_TRC,
                       "EoamLmIfCopyEventParams: Attempt to update "
                       "Invalid Event type failed\r\n"));
            i4RetVal = OSIX_FAILURE;
        }
    }

    OsixSemGive (gEoamLmGlobalInfo.SemId);
    EOAM_TRC_FN_EXIT ();
    return i4RetVal;
}

/******************************************************************************
 * Function     : EoamLmIfCopySymPeriod
 *
 * Description  : Copy Symbol Period configurations
 *
 * Input        : pLmEvtCfgInfo - pointer to Event configuration param
 *                u1Endis       - Event enable/disable
 *                u8Window      - Error event Window configuration
 *                u8Threshold   - Error event Threshold configuration
 *
 * Output       : None
 *
 * Returns      : None
 *****************************************************************************/
PRIVATE VOID
EoamLmIfCopySymPeriod (tEoamLmEventConfigInfo * pLmEvtCfgInfo,
                       UINT1 u1Endis, FS_UINT8 u8Window, FS_UINT8 u8Threshold)
{

    pLmEvtCfgInfo->u1ErrSymPeriodEvNotifEnable = u1Endis;

    EOAM_TRC_FN_ENTRY ();

    if (u1Endis == EOAM_ENABLED)
    {
        FSAP_U8_ASSIGN (&(pLmEvtCfgInfo->u8ErrSymPeriodWindow), &u8Window);

        FSAP_U8_ASSIGN (&(pLmEvtCfgInfo->u8ErrSymPeriodThreshold),
                        &u8Threshold);
    }

    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function     : EoamLmIfCopyFrameEvt
 *
 * Description  : Copy Frame Event configurations
 *
 * Input        : pLmEvtCfgInfo - pointer to Event configuration param
 *                u1Endis       - Event enable/disable
 *                u8Window      - Error event Window configuration
 *                u8Threshold   - Error event Threshold configuration
 *
 * Output       : None
 *
 * Returns      : None
 *****************************************************************************/
PRIVATE VOID
EoamLmIfCopyFrameEvt (tEoamLmEventConfigInfo * pLmEvtCfgInfo,
                      UINT1 u1Endis, FS_UINT8 u8Window, FS_UINT8 u8Threshold)
{
    pLmEvtCfgInfo->u1ErrFrameEvNotifEnable = u1Endis;

    EOAM_TRC_FN_ENTRY ();

    if (u1Endis == EOAM_ENABLED)
    {
        pLmEvtCfgInfo->u4ErrFrameWindow = FSAP_U8_FETCH_LO (&u8Window);
        pLmEvtCfgInfo->u4ErrFrameThreshold = FSAP_U8_FETCH_LO (&(u8Threshold));
    }

    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function     : EoamLmIfCopyFramePeriod
 *
 * Description  : Copy Frame Period Event configurations
 *
 * Input        : pLmEvtCfgInfo - pointer to Event configuration param
 *                u1Endis       - Event enable/disable
 *                u8Window      - Error event Window configuration
 *                u8Threshold   - Error event Threshold configuration
 *
 * Output       : None
 *
 * Returns      : None
 *****************************************************************************/
PRIVATE VOID
EoamLmIfCopyFramePeriod (tEoamLmEventConfigInfo * pLmEvtCfgInfo,
                         UINT1 u1Endis, FS_UINT8 u8Window, FS_UINT8 u8Threshold)
{
    pLmEvtCfgInfo->u1ErrFramePeriodEvNotifEnable = u1Endis;

    EOAM_TRC_FN_ENTRY ();

    if (u1Endis == EOAM_ENABLED)
    {
        pLmEvtCfgInfo->u4ErrFramePeriodWindow = FSAP_U8_FETCH_LO (&u8Window);
        pLmEvtCfgInfo->u4ErrFramePeriodThreshold =
            FSAP_U8_FETCH_LO (&(u8Threshold));
    }
    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function     : EoamLmIfCopySecSumm
 *
 * Description  : Copy Seconds summary Event configurations
 *
 * Input        : pLmEvtCfgInfo - pointer to Event configuration param
 *                u1Endis       - Event enable/disable
 *                u8Window      - Error event Window configuration
 *                u8Threshold   - Error event Threshold configuration
 *
 * Output       : None
 *
 * Returns      : None
 *****************************************************************************/
PRIVATE VOID
EoamLmIfCopySecSumm (tEoamLmEventConfigInfo * pLmEvtCfgInfo,
                     UINT1 u1Endis, FS_UINT8 u8Window, FS_UINT8 u8Threshold)
{
    pLmEvtCfgInfo->u1ErrFrameSecsEvNotifEnable = u1Endis;

    EOAM_TRC_FN_ENTRY ();

    if (u1Endis == EOAM_ENABLED)
    {
        pLmEvtCfgInfo->u4ErrFrameSecsSummaryWindow =
            FSAP_U8_FETCH_LO (&u8Window);
        pLmEvtCfgInfo->u4ErrFrameSecsSummaryThreshold =
            FSAP_U8_FETCH_LO (&(u8Threshold));
    }

    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function     : EoamLmIfFindNode
 *
 * Description  : Find DLL Node for the given port number
 *
 * Input        : u4Port - Port Number
 *
 * Output       : None
 *
 * Returns      : Pointer to DLL Node, if exists
 *****************************************************************************/
PRIVATE tEoamLmEnaPortDllNode *
EoamLmIfFindNode (UINT4 u4Port)
{
    tEoamLmEnaPortDllNode *pListNode = NULL;

    EOAM_TRC_FN_ENTRY ();

    TMO_DLL_Scan (&(gEoamLmGlobalInfo.LmEoamEnaPortDllList),
                  pListNode, tEoamLmEnaPortDllNode *)
    {
        if (pListNode->LmEvtCfgInfo.u4Port == u4Port)
        {
            break;
        }
    }

    EOAM_TRC_FN_EXIT ();
    return pListNode;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  emlmif.c                       */
/*-----------------------------------------------------------------------*/
