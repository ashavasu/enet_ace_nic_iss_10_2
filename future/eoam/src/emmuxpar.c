/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emmuxpar.c,v 1.8 2012/05/30 06:23:00 siva Exp $
 *
 * Description: This file contains procedures related to Multiplexer and
 *              Parser module of EOAM. Multiplexer and Parser modules 
 *              of EOAM are implemented in CFA. This is a portable file.
 *********************************************************************/
#include "eminc.h"

/* Proto types of the functions private to this file only */
PRIVATE INT4 EoamMuxParGetMuxAction PROTO ((UINT4 u4IfIndex));
PRIVATE INT4 EoamMuxParGetParserAction PROTO ((UINT4 u4IfIndex));

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamMuxParGetMuxAction 
 *                                                                          
 *    DESCRIPTION      : This function is called during packet transmision,
 *                       to check the Multiplex state of the interface. 
 *                       Packet tranmission is based on the MUX state.
 *                       When MUX is in 
 *                       FWD - Packet will be transmitted
 *                       DISCARD - Packet will not be txed.
 *
 *    INPUT            : u4IfIndex - Interface Index 
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : EOAM_FWD - packet to be txmd.
 *                       EOAM_DISCARD - packet to be discarded.
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
EoamMuxParGetMuxAction (UINT4 u4IfIndex)
{
    INT4                i4RetVal = EOAM_FWD;
    tEoamIfInfo         EoamIfInfo;

    EOAM_TRC_FN_ENTRY ();

    MEMSET (&EoamIfInfo, 0, sizeof (tEoamIfInfo));

    /* Get EOAM params for the interface. */
    EoamGetIfInfo (u4IfIndex, &EoamIfInfo);

    /* Check if EOAM is enabled on this interface */
    if (EoamIfInfo.u1EoamStatus == EOAM_DISABLED)
    {
        /* EOAM is not enabled on the interface. Hence, return FWD 
           to proceed with packet tx. */
        EOAM_TRC ((EOAM_MUX_PAR_TRC, "EoamMuxParGetMuxAction: EOAM is not"
                   " enabled on port %d - FWD\n", u4IfIndex));
        return EOAM_FWD;
    }

    switch (EoamIfInfo.u1MuxState)
    {
        case EOAM_FWD:
            if (EoamIfInfo.u1RemoteLB == EOAM_ENABLED)
            {
                /* Remote is in Loopback mode. So the MUX state is assumed to 
                   be in FWD state. Allow only Loopback frames and discard
                   all other frames. */
                EOAM_TRC ((EOAM_MUX_PAR_TRC, "EoamMuxParGetMuxAction: Port %d,"
                           " Remote in LOOPBACK mode. Only LB frames allowed."
                           "- DISCARD\n", u4IfIndex));
                return (EOAM_DISCARD);
            }

            if ((EoamIfInfo.u1UniDirSupp == EOAM_ENABLED) &&
                (EoamIfInfo.u1LinkFault == OSIX_TRUE))
            {
                EOAM_TRC ((EOAM_MUX_PAR_TRC, "EoamMuxParGetMuxAction: Port %d,"
                           " Unidir supp enabled and Link Fault occurred."
                           " - DISCARD\n", u4IfIndex));
                i4RetVal = EOAM_DISCARD;
            }
            else if ((EoamIfInfo.u1UniDirSupp == EOAM_DISABLED) ||
                     (EoamIfInfo.u1LinkFault == OSIX_FALSE))
            {
                EOAM_TRC ((EOAM_MUX_PAR_TRC, "EoamMuxParGetMuxAction: Port %d,"
                           " Unidir supp disabled and no Link Fault. - FWD\n",
                           u4IfIndex));
                i4RetVal = EOAM_FWD;
            }
            break;

        case EOAM_DISCARD:
            /* Stop packet tx, since the MUX state is DISCARD */
            EOAM_TRC ((EOAM_MUX_PAR_TRC, "EoamMuxParGetMuxAction: Port %d,"
                       " Mux state is DISCARD\n", u4IfIndex));
            i4RetVal = EOAM_DISCARD;
            break;

        default:
            EOAM_TRC ((EOAM_MUX_PAR_TRC, "EoamMuxParGetMuxAction: Port %d,"
                       " Unknown Mux state\n", u4IfIndex));
            i4RetVal = EOAM_DISCARD;
            break;
    }

    EOAM_TRC_FN_EXIT ();
    return (i4RetVal);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamMuxParGetParserAction 
 *                                                                          
 *    DESCRIPTION      : This function is called during packet reception,
 *                       to check the Parser state of the interface.
 *
 *    INPUT            : u4IfIndex - Interface index 
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : EOAM_FWD - Received frame can be delivered to HL.
 *                       EOAM_DISCARD - Discard the received frame as MUX is 
 *                                      in DISCARD state.            
 *                       EOAM_LOOPBACK - This node is in LOOPBACK mode and 
 *                                       hence loopback the rcvd frame.
 *                       EOAM_REMOTE_LB - This node is in REMOTE_LOOPBACK mode 
 *                                        and hence the loopback test data alone
 *                                        will be rcvd. 
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
EoamMuxParGetParserAction (UINT4 u4IfIndex)
{
    INT4                i4RetVal = EOAM_FWD;
    tEoamIfInfo         EoamIfInfo;

    EOAM_TRC_FN_ENTRY ();

    MEMSET (&EoamIfInfo, 0, sizeof (tEoamIfInfo));

    /* Get EOAM params for the interface. */
    EoamGetIfInfo (u4IfIndex, &EoamIfInfo);

    /* Check if EOAM is enabled on this interface */
    if (EoamIfInfo.u1EoamStatus == EOAM_DISABLED)
    {
        /* EOAM is not enabled on the interface. Hence, return SUCCESS
           to proceed with packet tx. */
        EOAM_TRC ((EOAM_MUX_PAR_TRC, "EoamMuxParGetParserAction: EOAM is"
                   " not enabled on port %d - FWD\n", u4IfIndex));
        return EOAM_FWD;
    }

    switch (EoamIfInfo.u1ParState)
    {
        case EOAM_FWD:
            /* Parser State is FWD, proceed with delivering the packets
               to HL. */
            EOAM_TRC ((EOAM_MUX_PAR_TRC, "EoamMuxParGetParserAction: Port %d,"
                       " Parser State in FWD\n", u4IfIndex));
            i4RetVal = EOAM_FWD;
            break;

        case EOAM_DISCARD:
            if (EoamIfInfo.u1RemoteLB == EOAM_ENABLED)
            {
                EOAM_TRC ((EOAM_MUX_PAR_TRC, "EoamMuxParGetParserAction:"
                           " Port %d, REMOTE LOOPBACK\n", u4IfIndex));
                i4RetVal = EOAM_REMOTE_LB;
            }
            else
            {
                /* Parser State is DISCARD, do not deliver it to HL. */
                EOAM_TRC ((EOAM_MUX_PAR_TRC, "EoamMuxParGetParserAction:"
                           " Port %d, Parser State in DISCARD\n", u4IfIndex));
                i4RetVal = EOAM_DISCARD;
            }
            break;

        case EOAM_LOOPBACK:
            /* Parser State is LOOPBACK, i.e., in local LOOPBACK mode.
               Loopback the received frame and do not deliver it to HL */
            EOAM_TRC ((EOAM_MUX_PAR_TRC, "EoamMuxParGetParserAction: Port %d,"
                       "In Local LOOPBACK, Loopback the rcvd frame. LOOPBACK\n",
                       u4IfIndex));
            i4RetVal = EOAM_LOOPBACK;
            break;

        default:
            EOAM_TRC ((EOAM_MUX_PAR_TRC, "EoamMuxParGetParserAction: Port %d,"
                       "Unknown Parser State. DISCARD\n", u4IfIndex));
            i4RetVal = EOAM_DISCARD;
            break;
    }

    EOAM_TRC_FN_EXIT ();
    return (i4RetVal);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamMuxParSet 
 *                                                                          
 *    DESCRIPTION      : This function sets the Mux and Parser state
 *                       of the interface.
 *
 *    INPUT            : u4IfIndex - Interface index 
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : None 
 *                       
 ****************************************************************************/
PUBLIC VOID
EoamMuxParSet (tEoamPortInfo * pPortInfo, UINT1 u1LoopbackStatus)
{
    tEoamEnaPortInfo   *pEnaPortInfo = NULL;
    UINT1               u1MuxAction = 0;
    UINT1               u1ParAction = 0;

    EOAM_TRC_FN_ENTRY ();

    pEnaPortInfo = pPortInfo->pEoamEnaPortInfo;

    switch (u1LoopbackStatus)
    {
        case EOAM_NO_LOOPBACK:
            EOAM_TRC ((EOAM_MUX_PAR_TRC, "EoamMuxParSet: Mux,Par - FWD\n"));
            u1MuxAction = u1ParAction = EOAM_FWD;
            break;

        case EOAM_INITIATING_LOOPBACK:
        case EOAM_TERMINATING_LOOPBACK:
            EOAM_TRC ((EOAM_MUX_PAR_TRC, "EoamMuxParSet: Mux,Par"
                       " - DISCARD\n"));
            u1MuxAction = u1ParAction = EOAM_DISCARD;
            break;

        case EOAM_REMOTE_LOOPBACK:
            EOAM_TRC ((EOAM_MUX_PAR_TRC, "EoamMuxParSet: Mux - FWD,"
                       " Parser - DISCARD\n"));
            u1MuxAction = EOAM_FWD;
            u1ParAction = EOAM_DISCARD;
            break;

        case EOAM_LOCAL_LOOPBACK:
            EOAM_TRC ((EOAM_MUX_PAR_TRC, "EoamMuxParSet: Mux - DISCARD,"
                       " Parser - LOOPBACK\n"));
            u1MuxAction = EOAM_DISCARD;
            u1ParAction = EOAM_LOOPBACK;
            break;

        default:
            break;

    }
    pEnaPortInfo->CtrlReq.LocalMuxAction = u1MuxAction;
    pEnaPortInfo->CtrlReq.LocalParAction = u1ParAction;

    EoamSetIfInfo (IF_EOAM_MUX_STATE, pPortInfo->u4Port, (UINT4) u1MuxAction);
    EoamSetIfInfo (IF_EOAM_PARSER_STATE, pPortInfo->u4Port,
                   (UINT4) u1ParAction);

    EOAM_TRC_FN_EXIT ();

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamMuxParActOnRx
 *                                                                          
 *    DESCRIPTION      : This function checks the Parser state and 
 *                       decides whether to 
 *                       - Forward to received frame to HL or
 *                       - Discard the received frame or
 *                       - Loopback the received frame or
 *                       - Deliver the loopback test data to FM module.
 *
 *    INPUT            : u4IfIndex - Interface index 
 *                       pBuf - pointer to received buffer
 *                       u4PktSize - size of the buffer 
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : EOAM_FWD - If the rcvd frame can be delivered to HL.
 *                       EOAM_DISCARD - If the rcvd frame has to be discarded
 *                                      as PARSER is in DISCARD state.            
 *                       EOAM_LOOPBACK - If the rcvd frame has to be ignored
 *                                       since the node is in local LOOPBACK 
 *                                       mode.
 *                       EOAM_REMOTE_LB - If the rcvd frame has to be ignored 
 *                                        since the node is in REMOTE_LOOPBACK
 *                                        mode.
 ****************************************************************************/
PUBLIC INT4
EoamMuxParActOnRx (UINT4 u4IfIndex, tCRU_BUF_CHAIN_HEADER * pBuf,
                   UINT4 u4PktSize)
{
    tEoamCallbackInfo   CallbackInfo;
    UINT4               u4ParserAction = EOAM_DISCARD;
    INT4                i4RetVal = EOAM_DISCARD;

    EOAM_TRC_FN_ENTRY ();

    MEMSET (&CallbackInfo, 0, sizeof (tEoamCallbackInfo));
    /* If EOAM is not enabled on the port, return SUCCESS 
     * to allow the the PDU to be delivered to HL. */
    if (EoamApiIsEnabledOnPort (u4IfIndex) != OSIX_TRUE)
    {
        EOAM_TRC ((EOAM_MUX_PAR_TRC, "EoamMuxParActOnRx: EOAM not enabled"
                   " on port %d - FWD\n", u4IfIndex));
        return (EOAM_FWD);
    }

    if (EoamApiIsOAMPDU (pBuf) == OSIX_TRUE)
    {
        /* All OAMPDUs are given to OAM sublayer irrespective of Parser state */
        return (EOAM_FWD);
    }

    /* Non OAMPDUs are allowed based on the Remote LB/Parser state */
    u4ParserAction = EoamMuxParGetParserAction (u4IfIndex);
    switch (u4ParserAction)
    {
        case EOAM_FWD:
            EOAM_TRC ((EOAM_MUX_PAR_TRC, "EoamMuxParActOnRx: Port %d,"
                       " Parser State in FWD\n", u4IfIndex));
            i4RetVal = EOAM_FWD;
            break;

        case EOAM_DISCARD:
            EOAM_TRC ((EOAM_MUX_PAR_TRC, "EoamMuxParActOnRx: Port %d,"
                       " Parser State in DISCARD\n", u4IfIndex));
            i4RetVal = EOAM_DISCARD;
            break;

        case EOAM_LOOPBACK:
            /* Parser action is LOOPBACK. Loopback the received frame and do 
             * not deliver it to HL. EoamTxLoopbackTestFrame will 
             * release buffer after tx. */
            EoamTxLoopbackTestFrame (u4IfIndex, pBuf, u4PktSize);

            EOAM_TRC ((EOAM_MUX_PAR_TRC, "EoamMuxParActOnRx: Port %d,"
                       " Parser State in LOOPBACK. Looping back the rcvd"
                       " frame. - LOOPBACK\n", u4IfIndex));
            i4RetVal = EOAM_LOOPBACK;
            break;

        case EOAM_REMOTE_LB:

            CallbackInfo.u4EventType = EOAM_NOTIFY_LB_TESTDATA_RCVD;
            CallbackInfo.u4Port = u4IfIndex;
            CallbackInfo.LBTestData = pBuf;
            /* Call function to notify registered modules */
            EoamMainNotifyModules (&CallbackInfo);

            EOAM_TRC ((EOAM_MUX_PAR_TRC, "EoamMuxParActOnRx: Port %d,"
                       " Parser State in REMOTE_LB. Delivered LB test data"
                       " to FM. - REMOTE LB\n", u4IfIndex));
            i4RetVal = EOAM_REMOTE_LB;
            break;

        default:
            EOAM_TRC ((EOAM_MUX_PAR_TRC, "EoamMuxParActOnRx: Port %d,"
                       " Unknown Parser action. DISCARD\n", u4IfIndex));
            break;
    }

    EOAM_TRC_FN_EXIT ();
    return (i4RetVal);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamMuxParActOnTx
 *                                                                          
 *    DESCRIPTION      : This function checks the Multiplexer state and 
 *                       decides whether to 
 *                       - Forward to received frame to HL or
 *                       - Discard the received frame or
 *                       - Loopback the received frame or
 *                       - Deliver the loopback test data to FM module.
 *
 *    INPUT            : u4IfIndex - Interface index 
 *                       pBuf  - CRU buffer pointer to be transmitted
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : EOAM_FWD - packet to be txmd.
 *                       EOAM_DISCARD - packet to be discarded.
 ****************************************************************************/
PUBLIC INT4
EoamMuxParActOnTx (UINT4 u4IfIndex, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    INT4                i4RetVal = EOAM_FWD;

    EOAM_TRC_FN_ENTRY ();

    if (EoamApiIsEnabledOnPort (u4IfIndex) != OSIX_TRUE)
    {
        EOAM_TRC ((EOAM_MUX_PAR_TRC, "EoamMuxParActOnTx: EOAM not enabled"
                   " on port %d - FWD\n", u4IfIndex));
        return (EOAM_FWD);
    }

    if (pBuf != NULL)
    {
        if (EoamApiIsOAMPDU (pBuf) == OSIX_TRUE)
        {
            /* All OAMPDUs are transmitted irrespective of Multiplexer State. */
            return (EOAM_FWD);
        }
    }

    i4RetVal = EoamMuxParGetMuxAction (u4IfIndex);
    if (i4RetVal == EOAM_FWD)
    {
        EOAM_TRC ((EOAM_MUX_PAR_TRC, "EoamMuxParActOnTx: Port %d,"
                   " Mux state: FWD\n", u4IfIndex));
    }
    else
    {
        EOAM_TRC ((EOAM_MUX_PAR_TRC, "EoamMuxParActOnTx: Port %d,"
                   " Mux state: DISCARD\n", u4IfIndex));
    }

    EOAM_TRC_FN_EXIT ();
    return (i4RetVal);
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  emmuxpar.c                     */
/*-----------------------------------------------------------------------*/
