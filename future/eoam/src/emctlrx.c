/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emctlrx.c,v 1.11 2012/10/29 12:02:27 siva Exp $
 *
 * Description: Ethernet OAM Control Reception related functions
 *****************************************************************************/
#include "eminc.h"

PRIVATE VOID EoamCtlRxPktFree PROTO ((tCRU_BUF_CHAIN_HEADER *));
PRIVATE VOID EoamCtlRxRestartLostLinkTmr PROTO ((tEoamEnaPortInfo *));
PRIVATE VOID EoamCtlRxCopyHeader PROTO ((tCRU_BUF_CHAIN_HEADER *,
                                         tEoamPduHeader *));
PRIVATE VOID EoamCtlRxCopyPduData PROTO ((tCRU_BUF_CHAIN_HEADER *, UINT1,
                                          UINT4, tEoamPduData *));
PRIVATE VOID EoamCtlRxCopyInfoPdu PROTO ((tCRU_BUF_CHAIN_HEADER *,
                                          tEoamInfoPduData *));
PRIVATE VOID EoamCtlRxCopyEventInfo PROTO ((tCRU_BUF_CHAIN_HEADER *,
                                            tEoamEventPduData *,
                                            UINT4 u4PktSize));
PRIVATE VOID EoamCtlRxCopySymbolPeriod PROTO ((tCRU_BUF_CHAIN_HEADER *,
                                               tEoamThresEventInfo *,
                                               UINT4 u4ParsedLen));
PRIVATE VOID        EoamCtlRxCopyFrameEvent
PROTO ((tCRU_BUF_CHAIN_HEADER *, tEoamThresEventInfo *, UINT4 u4ParsedLen));
PRIVATE VOID        EoamCtlRxCopyFramePeriod
PROTO ((tCRU_BUF_CHAIN_HEADER *, tEoamThresEventInfo *, UINT4 u4ParsedLen));
PRIVATE VOID        EoamCtlRxCopySecondSummary
PROTO ((tCRU_BUF_CHAIN_HEADER *, tEoamThresEventInfo *, UINT4 u4ParsedLen));
PRIVATE VOID        EoamCtlRxCopyExtData
PROTO ((tCRU_BUF_CHAIN_HEADER *, UINT4, UINT4, tEoamExtData *));

/******************************************************************************
 * Function Name      : EoamCtlRxHandleIncomingPdu
 *
 * Description        : Receives the incoming frame from lower layer.
 *                      The CRU buffer contents are copied onto
 *                      appropriate structures, the CRU buffer memory is
 *                      freed and the appropriate structure is sent to the
 *                      Client.
 *
 * Input(s)           : pBuf - Pointer to the received buffer
 *                      pPortInfo - Pointer to Port specific info
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
EoamCtlRxHandleIncomingPdu (tEoamPortInfo * pPortInfo,
                            tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tEoamPduHeader      EoamPduHeader;
    tEoamPduData        EoamPduData;
    UINT4               u4ByteCount = 0;
    UINT2               u2MaxOamPduSize = 0;
    UINT1              *pu1ReceivedData = NULL;

    EOAM_TRC_FN_ENTRY ();
    MEMSET (&EoamPduHeader, 0, sizeof (tEoamPduHeader));
    MEMSET (&EoamPduData, 0, sizeof (tEoamPduData));

    if ((pBuf == NULL) && (pPortInfo != NULL))
    {
        EOAM_TRC ((BUFFER_TRC | ALL_FAILURE_TRC,
                   ("EoamHandleIncomingPdu: Port %d :"
                    "CRU Buffer - Null Pointer received \n"),
                   pPortInfo->u4Port));
        return;
    }

    if (pPortInfo == NULL)
    {
        EOAM_TRC ((ALL_FAILURE_TRC, ("EoamHandleIncomingPdu: "
                                     "Null Pointer received\n")));
        EoamCtlRxPktFree (pBuf);
        return;
    }

    if ((gEoamGlobalInfo.u1ModuleStatus != EOAM_ENABLED) ||
        (pPortInfo->LocalInfo.u2EoamOperStatus == EOAM_OPER_DISABLE))
    {
        EoamCtlRxPktFree (pBuf);
        EOAM_TRC ((ALL_FAILURE_TRC, ("EoamHandleIncomingPdu: "
                                     "EOAM not started on port\n")));
        return;
    }

    u2MaxOamPduSize = pPortInfo->LocalInfo.u2MaxOamPduSize;

    if (((pPortInfo->LocalInfo.u2MaxOamPduSize) >
         (pPortInfo->RemoteInfo.u2MaxOamPduSize)) &&
        (pPortInfo->RemoteInfo.u2MaxOamPduSize != 0))
    {
        u2MaxOamPduSize = pPortInfo->RemoteInfo.u2MaxOamPduSize;
    }

    u4ByteCount = CRU_BUF_Get_ChainValidByteCount (pBuf);

    if ((u4ByteCount < EOAM_PDU_HEADER_SIZE) || (u4ByteCount > u2MaxOamPduSize))
    {
        EOAM_TRC ((ALL_FAILURE_TRC,
                   "EoamHandleIncomingPdu: Port %d :"
                   "Invalid length of Ethernet Frame; Discarding frame.\n",
                   pPortInfo->u4Port));

        EoamCtlRxPktFree (pBuf);
        return;
    }

    EOAM_PKT_DUMP (DUMP_TRC, pBuf, u4ByteCount,
                   "EoamHandleIncomingPdu:"
                   "Dumping OAMPDU received from lower layer...\n");

    EoamCtlRxCopyHeader (pBuf, &EoamPduHeader);

    if ((pPortInfo->pEoamEnaPortInfo->CtrlInd.LocalPdu !=
         EOAM_ANY) && (EoamPduHeader.u1Code != EOAM_INFO))
    {
        /* Discard non-info OAMPDUs if local_pdu is not SEND_ANY */
        EoamCtlRxPktFree (pBuf);
        EOAM_TRC ((ALL_FAILURE_TRC,
                   "EoamHandleIncomingPdu: Port %d : Non-info PDUs will be"
                   "processed only in SEND_ANY state; Discarding frame.\n",
                   pPortInfo->u4Port));
        return;
    }

    if ((pu1ReceivedData =
         (UINT1 *) (MemAllocMemBlk (gEoamGlobalInfo.EmLinearBufPoolId))) ==
        NULL)
    {
        EOAM_TRC ((ALL_FAILURE_TRC | EOAM_CRITICAL_TRC,
                   "EmLinearBufPoolId : Buffer allocation failed\r\n"));
        EoamCtlRxPktFree (pBuf);
        return;
    }

    MEMSET (pu1ReceivedData, 0, EOAM_MAX_PDU_SIZE);

    if (EoamPduHeader.u1Code == EOAM_INFO)
    {
        EoamPduData.InfoPduData.OrgSpecInfo.pu1Data = pu1ReceivedData;
    }
    else if (EoamPduHeader.u1Code == EOAM_EVENT_NOTIFICATION)
    {
        EoamPduData.EvtPduData.OrgEvtData.pu1Data = pu1ReceivedData;
    }
    else
    {
        if ((EoamPduHeader.u1Code == EOAM_VAR_REQ) ||
            (EoamPduHeader.u1Code == EOAM_VAR_RESP) ||
            (EoamPduHeader.u1Code == EOAM_ORG_SPEC))
        {
            EoamPduData.ExternalPduData.pu1Data = pu1ReceivedData;
        }
    }

    EoamCtlRxCopyPduData (pBuf, EoamPduHeader.u1Code,
                          u4ByteCount, &EoamPduData);

    EoamCtlRxPktFree (pBuf);

    if (EoamCltPduProcess (pPortInfo, &EoamPduHeader,
                           &EoamPduData) == OSIX_SUCCESS)
    {
        MemReleaseMemBlock (gEoamGlobalInfo.EmLinearBufPoolId,
                            (UINT1 *) pu1ReceivedData);
        EoamCtlRxRestartLostLinkTmr (pPortInfo->pEoamEnaPortInfo);
    }
    else
    {
        MemReleaseMemBlock (gEoamGlobalInfo.EmLinearBufPoolId,
                            (UINT1 *) pu1ReceivedData);
        EOAM_TRC ((ALL_FAILURE_TRC,
                   "EoamHandleIncomingPdu: Port %d:"
                   "Passing OAMPDU to EOAM Client Failed\n",
                   pPortInfo->u4Port));
    }

    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EoamCtlRxPktFree
 *
 * Description        : Free CRU Buffer memory
 *
 * Input(s)           : pBuf - pointer to CRU Buffer
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE VOID
EoamCtlRxPktFree (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    EOAM_TRC_FN_ENTRY ();

    if (CRU_BUF_Release_MsgBufChain (pBuf, OSIX_FALSE) != CRU_SUCCESS)
    {
        EOAM_TRC ((BUFFER_TRC | ALL_FAILURE_TRC,
                   "EoamCtlRxPktFree: "
                   "Received frame CRU Buffer Release Failed\n"));
    }

    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EoamCtlRxRestartLostLinkTmr
 *
 * Description        : Restart Lost Link Timer
 *
 * Input(s)           : pPortInfo - pointer to port info structure
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE VOID
EoamCtlRxRestartLostLinkTmr (tEoamEnaPortInfo * pEoamEnaPortInfo)
{
    EOAM_TRC_FN_ENTRY ();
    if (pEoamEnaPortInfo->CtrlReq.b1RemoteStateValid == OSIX_TRUE)
    {
        if (TmrStop (gEoamGlobalInfo.EoamTmrListId,
                     &(pEoamEnaPortInfo->LostLinkAppTmr)) == TMR_FAILURE)
        {
            EOAM_TRC ((EOAM_DISCOVERY_TRC,
                       "StopTimer for LostLinkAppTmr FAILED!!!\n"));
        }

        if (TmrStart (gEoamGlobalInfo.EoamTmrListId,
                      &(pEoamEnaPortInfo->LostLinkAppTmr),
                      EOAM_LOST_LINK_TMR_TYPE, EOAM_LOST_LINK_TIMEOUT, 0) ==
            TMR_FAILURE)
        {
            EOAM_TRC ((EOAM_DISCOVERY_TRC,
                       "StartTimer for LostLinkAppTmr FAILED!!!\n"));
        }
    }

    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EoamCtlRxCopyHeader
 *
 * Description        : Extract Header information from OAMPDU
 *
 * Input(s)           : pBuf - OAMPDU buffer
 *
 * Output(s)          : pEoamHeader - pointer to PDU header structure
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE VOID
EoamCtlRxCopyHeader (tCRU_BUF_CHAIN_HEADER * pBuf, tEoamPduHeader * pEoamHeader)
{
    EOAM_TRC_FN_ENTRY ();

    MEMSET (pEoamHeader, 0, sizeof (tEoamPduHeader));

    EOAM_CRU_GET_STRING (pBuf, pEoamHeader->DestMacAddr, EOAM_DEST_OFFSET,
                         MAC_ADDR_LEN);

    EOAM_CRU_GET_STRING (pBuf, pEoamHeader->SrcMacAddr, EOAM_SRC_OFFSET,
                         MAC_ADDR_LEN);

    EOAM_CRU_GET_2_BYTE (pBuf, EOAM_LEN_TYPE_OFFSET,
                         pEoamHeader->u2SlowProtoType);

    EOAM_CRU_GET_1_BYTE (pBuf, EOAM_SUBTYPE_OFFSET, pEoamHeader->u1SubType);

    EOAM_CRU_GET_2_BYTE (pBuf, EOAM_FLAG_OFFSET, pEoamHeader->u2Flags);

    EOAM_CRU_GET_1_BYTE (pBuf, EOAM_CODE_OFFSET, pEoamHeader->u1Code);

    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EoamCtlRxCopyPduData
 *
 * Description        : Extract Pdu data from packet buffer and copy to
 *                      PDU data structures defined
 *
 * Input(s)           : pBuf - OAMPDU buffer
 *                      u1Code - OAMPDU Type
 *                      u4PktSize - Packet byte count
 *
 * Output(s)          : pEoamData - EOAM Data
 *
 * Return Value(s)    : None 
 *****************************************************************************/
PRIVATE VOID
EoamCtlRxCopyPduData (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1Code,
                      UINT4 u4PktSize, tEoamPduData * pEoamData)
{
    UINT1               u1LbCmd = EOAM_LB_DISABLE;

    EOAM_TRC_FN_ENTRY ();

    if (u4PktSize == EOAM_PDU_HEADER_SIZE)
    {
        /* Only header is present in OAMPDU */
        return;
    }

    /* Copy data */
    switch (u1Code)
    {
        case EOAM_INFO:
            EoamCtlRxCopyInfoPdu (pBuf, &(pEoamData->InfoPduData));
            break;

        case EOAM_EVENT_NOTIFICATION:
            EoamCtlRxCopyEventInfo (pBuf, &(pEoamData->EvtPduData), u4PktSize);
            break;

        case EOAM_LB_CTRL:
            EOAM_CRU_GET_1_BYTE (pBuf, EOAM_PDU_DATA_OFFSET, u1LbCmd);
            pEoamData->LBCmd = u1LbCmd;
            break;

        case EOAM_VAR_REQ:
            /* Fall thru */
        case EOAM_VAR_RESP:
            /* Fall thru */
        case EOAM_ORG_SPEC:
            /* Var request, response and org specific data are copied as
             * such without any network to host conversions. Variable 
             * request will be taken care by EOAM Client. Var Response and Org 
             * specific data could not be converted  as their format is not
             * known */
            EoamCtlRxCopyExtData (pBuf, EOAM_PDU_DATA_OFFSET,
                                  u4PktSize - EOAM_PDU_HEADER_SIZE,
                                  &(pEoamData->ExternalPduData));
            break;
        default:
            EOAM_TRC ((CONTROL_PLANE_TRC,
                       "EoamCtlRxCopyPduData: Unsupported OAMPDU Type"
                       " received\r\n"));
            break;
    }

    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EoamCtlRxCopyInfoPdu
 *
 * Description        : Extract Information OAMPDU TLV contents
 *
 * Input(s)           : pBuf - OAMPDU buffer
 *
 * Output(s)          : pInfoPduData - Info Pdu data
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE VOID
EoamCtlRxCopyInfoPdu (tCRU_BUF_CHAIN_HEADER * pBuf,
                      tEoamInfoPduData * pInfoPduData)
{
    UINT4               u4Offset = 0;
    UINT1               u1Type = 0;
    UINT1               u1Length = 0;
    EOAM_TRC_FN_ENTRY ();

    EOAM_CRU_GET_1_BYTE (pBuf, EOAM_PDU_DATA_OFFSET, u1Type);

    if (u1Type == EOAM_LOCAL_INFO_TLV)
    {
        /* In an Info PDU, the first TLV should Local Info TLV */
        EOAM_CRU_GET_1_BYTE (pBuf, EOAM_LOCAL_TLV_LEN_OFFSET, u1Length);

        /* Validate TLV length - if length is not correct, ignore 
           the TLV */
        if (u1Length == EOAM_INFO_TLV_SIZE)
        {
            /* Store local TLV info of peer as remote info of local */
            EOAM_CRU_GET_1_BYTE (pBuf, EOAM_LOCAL_TLV_VERSION_OFFSET,
                                 pInfoPduData->LocalInfo.u1OamVer);

            EOAM_CRU_GET_2_BYTE (pBuf, EOAM_LOCAL_TLV_REVISION_OFFSET,
                                 pInfoPduData->LocalInfo.u2Revision);

            EOAM_CRU_GET_1_BYTE (pBuf, EOAM_LOCAL_TLV_STATE_OFFSET,
                                 pInfoPduData->LocalInfo.u1State);

            EOAM_CRU_GET_1_BYTE (pBuf, EOAM_LOCAL_TLV_OAM_CFG_OFFSET,
                                 pInfoPduData->LocalInfo.u1OamConfig);

            EOAM_CRU_GET_2_BYTE (pBuf, EOAM_LOCAL_TLV_PDU_CFG_OFFSET,
                                 pInfoPduData->LocalInfo.u2OamPduConfig);

            EOAM_CRU_GET_STRING (pBuf,
                                 pInfoPduData->LocalInfo.
                                 au1OUI, EOAM_LOCAL_OUI_OFFSET,
                                 EOAM_OUI_LENGTH);

            EOAM_CRU_GET_4_BYTE (pBuf, EOAM_LOCAL_TLV_VENDOR_OFFSET,
                                 pInfoPduData->LocalInfo.u4VendorInfo);

        }
    }

    u4Offset = EOAM_PDU_DATA_OFFSET + EOAM_INFO_TLV_SIZE;
    EOAM_CRU_GET_1_BYTE (pBuf, u4Offset, u1Type);

    if (u1Type == EOAM_REMOTE_INFO_TLV)
    {
        /* Ignore Remote Info TLV even if present since it is
         * not used anywhere. Increment offset to check whether org specific
         * Info TLV is present */
        u4Offset += EOAM_INFO_TLV_SIZE;
        EOAM_CRU_GET_1_BYTE (pBuf, u4Offset, u1Type);
    }

    if (u1Type == EOAM_ORG_INFO_TLV)
    {
        FSAP_ASSERT (pInfoPduData->OrgSpecInfo.pu1Data != NULL);
        /* Find Length of Org Specific Info TLV */
        u4Offset++;
        EOAM_CRU_GET_1_BYTE (pBuf, u4Offset, u1Length);
        u4Offset++;
        /* DataLen = TLV Length - size of (type field + length field) */
        pInfoPduData->OrgSpecInfo.u4DataLen = u1Length -
            (sizeof (UINT1) + sizeof (UINT1));
        EOAM_CRU_GET_STRING (pBuf, pInfoPduData->OrgSpecInfo.pu1Data, u4Offset,
                             pInfoPduData->OrgSpecInfo.u4DataLen);
    }
    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EoamCtlRxExtractEventInfo
 *
 * Description        : Identify Error event type and extract event info
 *
 * Input(s)           : pBuf - OAMPDU buffer
 *                      u4PktSize      - Total length of the PDU
 *
 * Output(s)          : pEoamEventData - Event TLV data
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE VOID
EoamCtlRxCopyEventInfo (tCRU_BUF_CHAIN_HEADER * pBuf,
                        tEoamEventPduData * pEoamEventData, UINT4 u4PktSize)
{
    UINT1               u1Type = 0;
    UINT1               u4TlvLen = 0;
    UINT4               u4ParsedLen = EOAM_PDU_HEADER_SIZE;

    EOAM_TRC_FN_ENTRY ();

    if (u4PktSize < (u4ParsedLen + EOAM_EVENT_SEQ_NUM_SIZE +
                     EOAM_EVENT_TLV_TYPE_SIZE + EOAM_EVENT_TLV_LEN_SIZE))
    {
        EOAM_TRC ((ALL_FAILURE_TRC,
                   "EoamCtlRxCopyEventInfo: Invalid Packet "
                   "Size received\r\n"));
        return;
    }

    /* Copy the sequence No, Type and Length from the pBuf */
    EOAM_CRU_GET_2_BYTE (pBuf, u4ParsedLen, pEoamEventData->u2SeqNum);
    EOAM_CRU_GET_1_BYTE (pBuf, (u4ParsedLen + EOAM_EVENT_SEQ_NUM_SIZE), u1Type);
    EOAM_CRU_GET_1_BYTE (pBuf, (u4ParsedLen + EOAM_EVENT_SEQ_NUM_SIZE +
                                EOAM_EVENT_TLV_TYPE_SIZE), u4TlvLen);

    u4ParsedLen += EOAM_EVENT_SEQ_NUM_SIZE;

    /* EOAM_END_TLV_MARKER = 0x00 */
    while (u1Type != EOAM_END_TLV_MARKER)
    {
        if ((u4ParsedLen + u4TlvLen) > u4PktSize)
        {
            /* TLV length + header + seq-no is greater than the packet size, 
             * so no further processing is allowed.
             */
            EOAM_TRC ((ALL_FAILURE_TRC,
                       "EoamCtlRxCopyEventInfo: Invalid Packet "
                       "Size received\r\n"));
            return;
        }

        switch (u1Type)
        {
            case EOAM_ERRORED_SYMBOL_EVENT_TLV:
                if (u4TlvLen != EOAM_ERR_SYMBOL_PERIOD_TLV_SIZE)
                {
                    pEoamEventData->u1EvtBitMap = 0;
                    EOAM_TRC ((ALL_FAILURE_TRC,
                               "EoamCtlRxCopyEventInfo: Invalid Symbol Event "
                               "TLV Length received\r\n"));
                    return;
                }

                pEoamEventData->u1EvtType = EOAM_THRESH_EVENT;
                pEoamEventData->ErrSymblPrdEvnt.u1EventType = u1Type;
                pEoamEventData->u1EvtBitMap |= EOAM_LNK_EVT_SYM_PER_TLV_BIT;
                EoamCtlRxCopySymbolPeriod (pBuf,
                                           &(pEoamEventData->ErrSymblPrdEvnt),
                                           u4ParsedLen);

                u4ParsedLen += u4TlvLen;
                break;

            case EOAM_ERRORED_FRAME_EVENT_TLV:
                if (u4TlvLen != EOAM_ERR_FRAME_TLV_SIZE)
                {
                    pEoamEventData->u1EvtBitMap = 0;
                    EOAM_TRC ((ALL_FAILURE_TRC,
                               "EoamCtlRxCopyEventInfo: Invalid Frame Event "
                               "TLV Length received\r\n"));
                    return;
                }

                pEoamEventData->u1EvtType = EOAM_THRESH_EVENT;
                pEoamEventData->ErrFrmEvnt.u1EventType = u1Type;
                pEoamEventData->u1EvtBitMap |= EOAM_LNK_EVT_FRM_TLV_BIT;
                EoamCtlRxCopyFrameEvent (pBuf,
                                         &(pEoamEventData->ErrFrmEvnt),
                                         u4ParsedLen);

                u4ParsedLen += u4TlvLen;
                break;

            case EOAM_ERRORED_FRAME_PERIOD_EVENT_TLV:
                if (u4TlvLen != EOAM_ERR_FRAME_PERIOD_TLV_SIZE)
                {
                    pEoamEventData->u1EvtBitMap = 0;
                    EOAM_TRC ((ALL_FAILURE_TRC,
                               "EoamCtlRxCopyEventInfo: Invalid Frame Period "
                               "Event TLV Length received\r\n"));
                    return;
                }

                pEoamEventData->u1EvtType = EOAM_THRESH_EVENT;
                pEoamEventData->ErrFrmPrdEvnt.u1EventType = u1Type;
                pEoamEventData->u1EvtBitMap |= EOAM_LNK_EVT_FRM_PER_TLV_BIT;
                EoamCtlRxCopyFramePeriod (pBuf,
                                          &(pEoamEventData->ErrFrmPrdEvnt),
                                          u4ParsedLen);

                u4ParsedLen += u4TlvLen;
                break;

            case EOAM_ERRORED_FRAME_SECONDS_EVENT_TLV:
                if (u4TlvLen != EOAM_ERR_FRAME_SEC_SUMMARY_TLV_SIZE)
                {
                    pEoamEventData->u1EvtBitMap = 0;
                    EOAM_TRC ((ALL_FAILURE_TRC,
                               "EoamCtlRxCopyEventInfo: Invalid Frame Seconds "
                               "Summary Event TLV Length received\r\n"));
                    return;
                }

                pEoamEventData->u1EvtType = EOAM_THRESH_EVENT;
                pEoamEventData->ErrFrmSecSumryEvnt.u1EventType = u1Type;
                pEoamEventData->u1EvtBitMap |= EOAM_LNK_EVT_SEC_SUM_TLV_BIT;
                EoamCtlRxCopySecondSummary (pBuf,
                                            &(pEoamEventData->
                                              ErrFrmSecSumryEvnt), u4ParsedLen);

                u4ParsedLen += u4TlvLen;
                break;

            case EOAM_ORG_SPEC_LINK_EVENT_TLV:
                /* Copy Org Spec TLV data. Format of the TLV is not known.
                 * Hence network to host conversion could not be done at this
                 * place. Network to Host conversion should be done at
                 * the place where this data is being processed
                 */
                pEoamEventData->u1EvtType = EOAM_ORG_SPEC_EVENT;
                pEoamEventData->u1EvtBitMap |= EOAM_LNK_EVT_ORG_SPEC_TLV_BIT;

                /* Length of Org spec OUI and value = TLV length - (size of 
                 *                                    type and length fields)
                 */
                if (u4TlvLen - (EOAM_EVENT_TLV_TYPE_SIZE +
                                EOAM_EVENT_TLV_LEN_SIZE) <
                    EOAM_EVT_ORG_SPEC_OUI_LEN)
                {
                    pEoamEventData->u1EvtBitMap = 0;
                    EOAM_TRC ((ALL_FAILURE_TRC,
                               "EoamCtlRxCopyEventInfo: Invalid OUI Length"
                               " received\r\n"));
                    return;
                }

                EoamCtlRxCopyExtData (pBuf,
                                      (u4ParsedLen + EOAM_EVT_ORG_SPEC_OUI_OFF),
                                      (UINT4) (u4TlvLen -
                                               (EOAM_EVENT_TLV_TYPE_SIZE +
                                                EOAM_EVENT_TLV_LEN_SIZE)),
                                      &(pEoamEventData->OrgEvtData));
                u4ParsedLen += u4TlvLen;
                break;

            default:
                EOAM_TRC ((ALL_FAILURE_TRC,
                           "EoamCtlRxCopyEventInfo: Unsupported Error Event Type"
                           " received\r\n"));
                return;
        }

        if ((u4ParsedLen + EOAM_EVENT_TLV_TYPE_SIZE + EOAM_EVENT_TLV_LEN_SIZE)
            > u4PktSize)
        {
            return;
        }

        EOAM_CRU_GET_1_BYTE (pBuf, u4ParsedLen, u1Type);
        EOAM_CRU_GET_1_BYTE (pBuf, (u4ParsedLen + EOAM_EVENT_TLV_TYPE_SIZE),
                             u4TlvLen);

    }
    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EoamCtlRxCopySymbolPeriod
 *
 * Description        : Extracts Symbol period event information from
 *                      packet buffer
 *
 * Input(s)           : pBuf - OAMPDU buffer
 *                    : u4ParsedLen - Processed Packet Length
 *
 * Output(s)          : pEventInfo - pointer to Event Information
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE VOID
EoamCtlRxCopySymbolPeriod (tCRU_BUF_CHAIN_HEADER * pBuf,
                           tEoamThresEventInfo * pEventInfo, UINT4 u4ParsedLen)
{
    UINT2               u2Val = 0;
    UINT4               u4Val = 0;

    EOAM_TRC_FN_ENTRY ();

    EOAM_CRU_GET_2_BYTE (pBuf, (u4ParsedLen + EOAM_EVT_SYM_TIMESTAMP_OFF),
                         u2Val);
    pEventInfo->u4EventLogTimestamp = u2Val;

    EOAM_CRU_GET_4_BYTE (pBuf, (u4ParsedLen + EOAM_EVT_SYM_WIN_HIGH_OFF),
                         u4Val);
    FSAP_U8_ASSIGN_HI (&(pEventInfo->u8EventWindow), u4Val);

    EOAM_CRU_GET_4_BYTE (pBuf, (u4ParsedLen + EOAM_EVT_SYM_WIN_LOW_OFF), u4Val);
    FSAP_U8_ASSIGN_LO (&(pEventInfo->u8EventWindow), u4Val);

    EOAM_CRU_GET_4_BYTE (pBuf, (u4ParsedLen + EOAM_EVT_SYM_THR_HIGH_OFF),
                         u4Val);
    FSAP_U8_ASSIGN_HI (&(pEventInfo->u8EventThreshold), u4Val);

    EOAM_CRU_GET_4_BYTE (pBuf, (u4ParsedLen + EOAM_EVT_SYM_THR_LOW_OFF), u4Val);
    FSAP_U8_ASSIGN_LO (&(pEventInfo->u8EventThreshold), u4Val);

    EOAM_CRU_GET_4_BYTE (pBuf, (u4ParsedLen + EOAM_EVT_SYM_VAL_HIGH_OFF),
                         u4Val);
    FSAP_U8_ASSIGN_HI (&(pEventInfo->u8EventValue), u4Val);

    EOAM_CRU_GET_4_BYTE (pBuf, (u4ParsedLen + EOAM_EVT_SYM_VAL_LOW_OFF), u4Val);
    FSAP_U8_ASSIGN_LO (&(pEventInfo->u8EventValue), u4Val);

    EOAM_CRU_GET_4_BYTE (pBuf, (u4ParsedLen + EOAM_EVT_SYM_TOT_HIGH_OFF),
                         u4Val);
    FSAP_U8_ASSIGN_HI (&(pEventInfo->u8ErrorRunningTotal), u4Val);

    EOAM_CRU_GET_4_BYTE (pBuf, (u4ParsedLen + EOAM_EVT_SYM_TOT_LOW_OFF), u4Val);
    FSAP_U8_ASSIGN_LO (&(pEventInfo->u8ErrorRunningTotal), u4Val);

    EOAM_CRU_GET_4_BYTE (pBuf, (u4ParsedLen + EOAM_EVT_SYM_COUNT_OFF),
                         pEventInfo->u4EventRunningTotal);

    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EoamCtlRxCopyFrameEvent
 *
 * Description        : Extracts Frame event information from
 *                      packet buffer
 *
 * Input(s)           : pBuf - OAMPDU buffer
 *                    : u4ParsedLen - Processed Packet Length
 *
 * Output(s)          : pEventInfo - pointer to Event Information
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE VOID
EoamCtlRxCopyFrameEvent (tCRU_BUF_CHAIN_HEADER * pBuf,
                         tEoamThresEventInfo * pEventInfo, UINT4 u4ParsedLen)
{
    UINT2               u2Val = 0;
    UINT4               u4Val = 0;

    EOAM_TRC_FN_ENTRY ();
    EOAM_CRU_GET_2_BYTE (pBuf, (u4ParsedLen + EOAM_EVT_FRAME_TIMESTAMP_OFF),
                         u2Val);
    pEventInfo->u4EventLogTimestamp = u2Val;

    FSAP_U8_ASSIGN_HI (&(pEventInfo->u8EventWindow), 0);
    EOAM_CRU_GET_2_BYTE (pBuf, (u4ParsedLen + EOAM_EVT_FRAME_WIN_OFF), u2Val);
    FSAP_U8_ASSIGN_LO (&(pEventInfo->u8EventWindow), u2Val);

    FSAP_U8_ASSIGN_HI (&(pEventInfo->u8EventThreshold), 0);
    EOAM_CRU_GET_4_BYTE (pBuf, (u4ParsedLen + EOAM_EVT_FRAME_THR_OFF), u4Val);
    FSAP_U8_ASSIGN_LO (&(pEventInfo->u8EventThreshold), u4Val);

    FSAP_U8_ASSIGN_HI (&(pEventInfo->u8EventValue), 0);
    EOAM_CRU_GET_4_BYTE (pBuf, (u4ParsedLen + EOAM_EVT_FRAME_VAL_OFF), u4Val);
    FSAP_U8_ASSIGN_LO (&(pEventInfo->u8EventValue), u4Val);

    EOAM_CRU_GET_4_BYTE (pBuf, (u4ParsedLen + EOAM_EVT_FRAME_TOTHI_OFF), u4Val);
    FSAP_U8_ASSIGN_HI (&(pEventInfo->u8ErrorRunningTotal), u4Val);

    EOAM_CRU_GET_4_BYTE (pBuf, (u4ParsedLen + EOAM_EVT_FRAME_TOTLO_OFF), u4Val);
    FSAP_U8_ASSIGN_LO (&(pEventInfo->u8ErrorRunningTotal), u4Val);

    EOAM_CRU_GET_4_BYTE (pBuf, (u4ParsedLen + EOAM_EVT_FRAME_COUNT_OFF),
                         pEventInfo->u4EventRunningTotal);

    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EoamCtlRxCopyFramePeriod
 *
 * Description        : Extracts Frame Period event information from
 *                      packet buffer
 *
 * Input(s)           : pBuf - OAMPDU buffer
 *                    : u4ParsedLen - Processed Packet Length
 *
 * Output(s)          : pEventInfo - pointer to Event Information
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE VOID
EoamCtlRxCopyFramePeriod (tCRU_BUF_CHAIN_HEADER * pBuf,
                          tEoamThresEventInfo * pEventInfo, UINT4 u4ParsedLen)
{
    UINT2               u2Val = 0;
    UINT4               u4Val = 0;

    EOAM_TRC_FN_ENTRY ();
    EOAM_CRU_GET_2_BYTE (pBuf, (u4ParsedLen + EOAM_EVT_FRPER_TIMESTAMP_OFF),
                         u2Val);
    pEventInfo->u4EventLogTimestamp = u2Val;

    FSAP_U8_ASSIGN_HI (&(pEventInfo->u8EventWindow), 0);
    EOAM_CRU_GET_4_BYTE (pBuf, (u4ParsedLen + EOAM_EVT_FRPER_WIN_OFF), u4Val);
    FSAP_U8_ASSIGN_LO (&(pEventInfo->u8EventWindow), u4Val);

    FSAP_U8_ASSIGN_HI (&(pEventInfo->u8EventThreshold), 0);
    EOAM_CRU_GET_4_BYTE (pBuf, (u4ParsedLen + EOAM_EVT_FRPER_THR_OFF), u4Val);
    FSAP_U8_ASSIGN_LO (&(pEventInfo->u8EventThreshold), u4Val);

    FSAP_U8_ASSIGN_HI (&(pEventInfo->u8EventValue), 0);
    EOAM_CRU_GET_4_BYTE (pBuf, (u4ParsedLen + EOAM_EVT_FRPER_VAL_OFF), u4Val);
    FSAP_U8_ASSIGN_LO (&(pEventInfo->u8EventValue), u4Val);

    EOAM_CRU_GET_4_BYTE (pBuf, (u4ParsedLen + EOAM_EVT_FRPER_TOTHI_OFF), u4Val);
    FSAP_U8_ASSIGN_HI (&(pEventInfo->u8ErrorRunningTotal), u4Val);

    EOAM_CRU_GET_4_BYTE (pBuf, (u4ParsedLen + EOAM_EVT_FRPER_TOTLO_OFF), u4Val);
    FSAP_U8_ASSIGN_LO (&(pEventInfo->u8ErrorRunningTotal), u4Val);

    EOAM_CRU_GET_4_BYTE (pBuf, (u4ParsedLen + EOAM_EVT_FRPER_COUNT_OFF),
                         pEventInfo->u4EventRunningTotal);

    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EoamCtlRxCopySecondSummary
 *
 * Description        : Extracts Seconds summary event information from
 *                      packet buffer
 *
 * Input(s)           : pBuf - OAMPDU buffer
 *                    : u4ParsedLen - Processed Packet Length
 *
 * Output(s)          : pEventInfo - pointer to Event Information
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE VOID
EoamCtlRxCopySecondSummary (tCRU_BUF_CHAIN_HEADER * pBuf,
                            tEoamThresEventInfo * pEventInfo, UINT4 u4ParsedLen)
{
    UINT2               u2Val = 0;
    UINT4               u4Val = 0;

    EOAM_TRC_FN_ENTRY ();
    EOAM_CRU_GET_2_BYTE (pBuf, (u4ParsedLen + EOAM_EVT_SS_TIMESTAMP_OFF),
                         u2Val);
    pEventInfo->u4EventLogTimestamp = u2Val;

    FSAP_U8_ASSIGN_HI (&(pEventInfo->u8EventWindow), 0);
    EOAM_CRU_GET_2_BYTE (pBuf, (u4ParsedLen + EOAM_EVT_SS_WIN_OFF), u2Val);
    FSAP_U8_ASSIGN_LO (&(pEventInfo->u8EventWindow), u2Val);

    FSAP_U8_ASSIGN_HI (&(pEventInfo->u8EventThreshold), 0);
    EOAM_CRU_GET_2_BYTE (pBuf, (u4ParsedLen + EOAM_EVT_SS_THR_OFF), u2Val);
    FSAP_U8_ASSIGN_LO (&(pEventInfo->u8EventThreshold), u2Val);

    FSAP_U8_ASSIGN_HI (&(pEventInfo->u8EventValue), 0);
    EOAM_CRU_GET_2_BYTE (pBuf, (u4ParsedLen + EOAM_EVT_SS_VAL_OFF), u2Val);
    FSAP_U8_ASSIGN_LO (&(pEventInfo->u8EventValue), u2Val);

    FSAP_U8_ASSIGN_HI (&(pEventInfo->u8ErrorRunningTotal), 0);
    EOAM_CRU_GET_4_BYTE (pBuf, (u4ParsedLen + EOAM_EVT_SS_TOTAL_OFF), u4Val);
    FSAP_U8_ASSIGN_LO (&(pEventInfo->u8ErrorRunningTotal), u4Val);

    EOAM_CRU_GET_4_BYTE (pBuf, (u4ParsedLen + EOAM_EVT_SS_COUNT_OFF),
                         pEventInfo->u4EventRunningTotal);
    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EoamCtlRxCopyExtData
 *
 * Description        : Copies Variable request, variable response and 
 *                      organization specific data from packet buffer
 *
 * Input(s)           : pBuf - OAMPDU buffer
 *                      u4DataLen - Size of the data
 *
 * Output(s)          : pExtData - pointer to External Data
 *
 * Return Value(s)    : None. 
 *****************************************************************************/
PRIVATE VOID
EoamCtlRxCopyExtData (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Offset,
                      UINT4 u4DataLen, tEoamExtData * pExtData)
{
    EOAM_TRC_FN_ENTRY ();

    pExtData->u4DataLen = u4DataLen;
    EOAM_CRU_GET_STRING (pBuf, pExtData->pu1Data, u4Offset,
                         pExtData->u4DataLen);

    EOAM_TRC_FN_EXIT ();
    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  emctlrx.c                      */
/*-----------------------------------------------------------------------*/
