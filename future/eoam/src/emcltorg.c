/*******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emcltorg.c,v 1.6 2007/10/12 06:52:32 iss Exp $
 *
 * Description: This file conatains the functions related to Org Specific 
 * PDU handling by EOAM client module. 
 ******************************************************************************/
#include "eminc.h"

/*******************************************************************************
 *                                                                          
 *    FUNCTION NAME    :  EoamCltOrgSpecPduProcess
 *                                                                          
 *    DESCRIPTION      :  This function is called on reception of a          
 *                        Organizationally Specific OAMPDU. It just          
 *                        forwards the received response to the Fault       
 *                        Management Module.                                
 *                                                                          
 *    INPUT            :  pPortInfo - pointer to port specific info          
 *                        pData - pointer to PDU Data                       
 *                                                                          
 *    OUTPUT           :  None.                                             
 *                                                                          
 *    RETURNS          :  OSIX_SUCCESS / OSIX_FAILURE                       
 *                                                                          
*******************************************************************************/
PUBLIC INT4
EoamCltOrgSpecPduProcess (tEoamPortInfo * pPortInfo, tEoamPduData * pOrgData)
{
    tEoamCallbackInfo   CallbackInfo;
    INT4                i4RetVal = OSIX_SUCCESS;

    EOAM_TRC_FN_ENTRY ();
    MEMSET (&CallbackInfo, 0, sizeof (tEoamCallbackInfo));
    /* Increment the Organizationally Specific OAMPDU Rx Counter */
    pPortInfo->PduStats.u4OrgSpecificRx++;

    /* Fill up the Callback Info structure appropraitely indicating the 
     * variable response received */
    CallbackInfo.u4EventType = EOAM_NOTIFY_ORG_INFO_RCVD;
    CallbackInfo.u2Location = EOAM_REMOTE_ENTRY;
    CallbackInfo.u4Port = pPortInfo->u4Port;
    CallbackInfo.ExtData.u4DataLen = pOrgData->ExternalPduData.u4DataLen;
    CallbackInfo.ExtData.pu1Data = pOrgData->ExternalPduData.pu1Data;
    /* Call a function to notify all registered modules */
    EoamMainNotifyModules (&CallbackInfo);
    EOAM_TRC_FN_EXIT ();
    return i4RetVal;
}

/*******************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamCltOrgSendOrgSpecificPdu 
 *                                                                          
 *    DESCRIPTION      : This function is called when an organization       
 *                       specific PDU is to be sent to the peer. It sends   
 *                       an appropriate OAMPDU request to the Control Module
 *                                                                          
 *    INPUT            : pPortEntry - pointer to port information structure 
 *                       pOrgSpecInfo - pointer to Organization specific    
 *                       information                                        
 *                                                                          
 *    OUTPUT           : None.                                              
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE                        
 *                                                                          
*******************************************************************************/
PUBLIC INT4
EoamCltOrgSendOrgSpecificPdu (tEoamPortInfo * pPortEntry,
                              tEoamExtData * pOrgSpecInfo)
{
    tEoamPduHeader      PduHeader;
    tEoamPduData        PduData;

    EOAM_TRC_FN_ENTRY ();
    MEMSET (&PduHeader, 0, sizeof (tEoamPduHeader));
    MEMSET (&PduData, 0, sizeof (tEoamPduData));

    if (gEoamGlobalInfo.u1ModuleStatus != EOAM_ENABLED)
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "EOAM is globally DISABLED!!!\n"));
        return OSIX_FAILURE;
    }

    if (pPortEntry == NULL)
    {
        return OSIX_SUCCESS;
    }

    /* OAMPDUs other than Information PDUs can be sent out only when
     * discovery has settled down */
    if (pPortEntry->LocalInfo.u2EoamOperStatus != EOAM_OPER_OPERATIONAL)
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "CLIENT: Cannot send out Org Specific"
                   "OAMPDU when discovery has not settled down on"
                   "port %u\n", pPortEntry->u4Port));
        return OSIX_FAILURE;
    }
    MEMCPY (&(PduData.ExternalPduData), pOrgSpecInfo, sizeof (tEoamExtData));
    EoamCtlTxFormPduHeader (pPortEntry, EOAM_ORG_SPEC, &PduHeader);
    if (EoamCtlTxPduRequest (pPortEntry, &PduHeader, &PduData) != OSIX_SUCCESS)
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "CLIENT: EoamCtlTxPduRequest call"
                   "for Org Specific PDU failed on port %u\n",
                   pPortEntry->u4Port));
        return OSIX_FAILURE;
    }
    /* Increment the Organizationally Specific OAMPDU Tx Counter */
    pPortEntry->PduStats.u4OrgSpecificTx++;
    EOAM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  emcltorg.c                     */
/*-----------------------------------------------------------------------*/
