/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emred.c,v 1.35 2014/03/06 12:50:26 siva Exp $
 *
 * Description: This file contains EOAM High Availability handling
 *              routines.
 *********************************************************************/
#ifndef _EMRED_c
#define _EMRED_c

#include "eminc.h"
#include "emlmdefn.h"
#include "emlmglob.h"

PRIVATE VOID EoamRedSyncUpData PROTO ((tRmMsg *, UINT2));
PRIVATE VOID EoamRedProcessOperChgMsg PROTO ((tRmMsg *, UINT4 *, UINT2));
PRIVATE VOID EoamRedProcessDynamicMsg PROTO ((tRmMsg *, UINT4 *, UINT2));
PRIVATE INT4 EoamRedReadPortInfo PROTO ((tRmMsg *, UINT2, UINT4 *, UINT4 *));
PRIVATE VOID EoamRedReadLocalInfo PROTO ((tRmMsg *, UINT4 *, tEoamPortInfo *));
PRIVATE VOID EoamRedReadPeerInfo PROTO ((tRmMsg *, UINT4 *, tEoamPortInfo *));
PRIVATE VOID EoamRedMakeNodeActive PROTO ((VOID));
PRIVATE VOID EoamRedMakeNodeActiveFromStandby PROTO ((VOID));
PRIVATE VOID EoamRedMakeNodeActiveFromIdle PROTO ((VOID));
PRIVATE VOID EoamRedGoStandbyEvent PROTO ((VOID));
PRIVATE VOID EoamRedMakeNodeStandbyFromIdle PROTO ((VOID));
PRIVATE VOID EoamRedStandbyUpEvent PROTO ((VOID *));
PRIVATE VOID EoamRedTrigHigherLayer PROTO ((UINT1));
PRIVATE VOID EoamRedSendBulkReq PROTO ((VOID));
PRIVATE INT4 EoamRedSendBulkUpdateTailMsg PROTO ((VOID));
PRIVATE VOID EoamRedFillLocalInfo PROTO ((tEoamPortInfo *, tRmMsg *, UINT4 *));
PRIVATE VOID EoamRedFillPeerInfo PROTO ((tEoamPortInfo *, tRmMsg *, UINT4 *));
PRIVATE INT4 EoamRedSendMsgToRm PROTO ((tRmMsg *, UINT2));
PRIVATE VOID EoamRedConstructMsgHeader PROTO ((UINT4, eEoamRedUpdateMsgType,
                                               tRmMsg **, UINT4 *, UINT4 *));
static UINT1        u1MemEstFlag = 1;
extern INT4         IssSzUpdateSizingInfoForHR (CHR1 * pu1ModName,
                                                CHR1 * pu1StName,
                                                UINT4 u4BulkUnitSize);
/*****************************************************************************
 * Function Name      : EoamRedRegisterWithRM
 * Description        : Registers EOAM with RM by providing an application
 *                      ID for Eoam and a call back function to be called
 *                      whenever RM needs to send an event to EOAM
 * Input(s)           : None
 * Output(s)          : None
 * Return Value(s)    : OSIX_SUCCESS - if registration is successful
 *                      OSIX_FAILURE - otherwise
 *****************************************************************************/
PUBLIC INT4
EoamRedRegisterWithRM (VOID)
{
    tRmRegParams        RmRegParams;

    RmRegParams.u4EntId = RM_EOAM_APP_ID;
    RmRegParams.pFnRcvPkt = EoamRedHandleUpdateEvent;
    if (RmRegisterProtocols (&RmRegParams) == RM_FAILURE)
    {
        EOAM_TRC ((INIT_SHUT_TRC | OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                   "EoamRedRegisterWithRM: Registration with RM FAILED\r\n"));
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EoamRedDeRegisterWithRM
 * Description        : Deregisters EOAM with RM
 * Input(s)           : None
 * Output(s)          : None
 * Return Value(s)    : OSIX_SUCCESS - if registration is successful
 *                      OSIX_FAILURE - otherwise
 *****************************************************************************/
PUBLIC INT4
EoamRedDeRegisterWithRM (VOID)
{
    if (RmDeRegisterProtocols (RM_EOAM_APP_ID) == RM_FAILURE)
    {
        EOAM_TRC ((INIT_SHUT_TRC | OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                   "EoamRedRegisterWithRM: Registration with RM FAILED\r\n"));
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EoamRedInitRedGlobalInfo
 * Description        : This function initializes global variable used to
 *                      store EOAM Redundancy information
 * Input(s)           : None
 * Output(s)          : None
 * Return Value(s)    : None
 * 
 *****************************************************************************/
PUBLIC VOID
EoamRedInitRedGlobalInfo (VOID)
{
    MEMSET (&(gEoamRedGlobalInfo), 0, sizeof (tEoamRedGlobalInfo));
    gEoamRedGlobalInfo.u4BulkUpdNextPort = EOAM_MIN_PORTS;
    gEoamRedGlobalInfo.bBulkReqRcvd = OSIX_FALSE;
    EOAM_RM_NODE_STATUS () = EOAM_IDLE_NODE;
    EOAM_NUM_STANDBY_NODES () = 0;
    /* HITLESS RESTART */
    EOAM_HR_STDY_ST_REQ_RCVD () = OSIX_FALSE;
    return;
}

/*****************************************************************************
 * Function Name      : EoamRedGetNodeStateFromRm
 * Description        : This function gets Node status from RM and stores in
 *                      gEoamNodeStatus
 * Input(s)           : None
 * Output(s)          : None
 * Return Value(s)    : None
 ****************************************************************************/
PUBLIC VOID
EoamRedGetNodeStateFromRm (VOID)
{
    UINT4               u4NodeState;

    u4NodeState = RmGetNodeState ();

    switch (u4NodeState)
    {
        case RM_ACTIVE:
            EOAM_RM_NODE_STATUS () = EOAM_ACTIVE_NODE;
            break;
        case RM_STANDBY:
            EOAM_RM_NODE_STATUS () = EOAM_STANDBY_NODE;
            break;
        default:
            EOAM_RM_NODE_STATUS () = EOAM_IDLE_NODE;
            break;
    }
}

/*****************************************************************************
 * Function Name      : EoamRedHandleUpdateEvent
 * Description        : This API constructs a message containing the
 *                      given RM event and RM message and posts it to the
 *                      EOAM queue and sends an event
 * Input(s)           : u1Event   - Event type given by RM module
 *                      pData     - RM Message to enqueue
 *                      u2DataLen - Message size
 * Output(s)          : None
 * Return Value(s)    : OSIX_SUCCESS - if msg is enqueued and event sent
 *                      OSIX_FAILURE - otherwise
 *****************************************************************************/
PUBLIC INT4
EoamRedHandleUpdateEvent (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tEoamQMsg          *pMsg = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    EOAM_TRC_FN_ENTRY ();

    /* Bulk update request and L2 complete sync up Message from RM 
     * needs to be given to higer layers  even when eoam is in shutdown
     * state, hence shutdown check is removed */

    if (((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP) ||
         (u1Event == RM_STANDBY_DOWN)) && (pData == NULL))
    {
        /* No Message present - no need to process furthur */
        EOAM_TRC ((EOAM_RED_TRC, "EoamRedHandleUpdateEvent: Rcvd valid message"
                   "  with pointer to buffer as NULL\r\n"));

        return RM_FAILURE;
    }
    if (gEoamGlobalInfo.u1SystemCtrlStatus == EOAM_SHUTDOWN)
    {
        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            RmReleaseMemoryForMsg ((UINT1 *) pData);
        }
        EOAM_TRC ((ALL_FAILURE_TRC, "EoamQueEnqAppMsg: EOAM is down\n"));
        return RM_FAILURE;
    }

    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tEoamQMsg *) (MemAllocMemBlk
                               (gEoamGlobalInfo.QMsgPoolId))) == NULL)
    {
        EOAM_TRC ((BUFFER_TRC | ALL_FAILURE_TRC,
                   "EoamRedHandleUpdateEvent: Intf Msg ALLOC_MEM_BLOCK "
                   "FAILED\r\n"));

        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            RmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        return RM_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tEoamQMsg));

    pMsg->u4MsgType = EOAM_RM_MSG;

    pMsg->unEoamMsgParam.RmEoamMsg.u1RmEvent = u1Event;
    pMsg->unEoamMsgParam.RmEoamMsg.pData = pData;
    pMsg->unEoamMsgParam.RmEoamMsg.u2DataLen = u2DataLen;

    i4RetVal = EoamQueEnqAppMsg (pMsg);

    if (i4RetVal == OSIX_FAILURE)
    {
        EOAM_TRC ((ALL_FAILURE_TRC,
                   "EoamRedHandleUpdateEvent: "
                   "EOAM Enqueue Control Message FAILED\r\n"));
        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            RmReleaseMemoryForMsg ((UINT1 *) pData);
        }
    }

    EOAM_TRC_FN_EXIT ();

    if (i4RetVal == OSIX_FAILURE)
    {
        return RM_FAILURE;
    }
    return RM_SUCCESS;

}

/***************************************************************************
 * Function Name      : EoamRedProcessRmMsg
 * Description        : This function  based on the RM event present in the 
 *                      given input buffer, takes appropriate action
 * Input(s)           : pMsg - Pointer to the structure containing RM
 *                             buffer and the RM event
 * Output(s)          : None
 * Return Value(s)    : None
 ***************************************************************************/
PUBLIC VOID
EoamRedProcessRmMsg (tEoamQMsg * pMsg)
{
    tRmNodeInfo        *pData = NULL;
    eEoamNodeStatus     EoamPrevNodeState = EOAM_IDLE_NODE;
    tRmProtoEvt         ProtoEvt;
    tRmProtoAck         ProtoAck;
    UINT4               u4SeqNum = 0;

    ProtoEvt.u4AppId = RM_EOAM_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    switch (pMsg->unEoamMsgParam.RmEoamMsg.u1RmEvent)
    {
        case RM_MESSAGE:
            EOAM_TRC ((EOAM_RED_TRC, "EoamRedProcessRmMsg: Rcvd VALID MESSAGE"
                       " from RM\r\n"));

            /* Read the sequence number from the RM Header */
            RM_PKT_GET_SEQNUM (pMsg->unEoamMsgParam.RmEoamMsg.pData, &u4SeqNum);
            /* Remove the RM Header */
            RM_STRIP_OFF_RM_HDR (pMsg->unEoamMsgParam.RmEoamMsg.pData,
                                 pMsg->unEoamMsgParam.RmEoamMsg.u2DataLen);

            ProtoAck.u4AppId = RM_EOAM_APP_ID;
            ProtoAck.u4SeqNumber = u4SeqNum;

            if (pMsg->unEoamMsgParam.RmEoamMsg.pData != NULL)
            {
                EoamRedSyncUpData (pMsg->unEoamMsgParam.RmEoamMsg.pData,
                                   pMsg->unEoamMsgParam.RmEoamMsg.u2DataLen);

                /* Processing of message is over, hence free the RM message. */
                CRU_BUF_Release_MsgBufChain (pMsg->unEoamMsgParam.RmEoamMsg.
                                             pData, FALSE);
            }
            else
            {
                EOAM_TRC ((EOAM_RED_TRC, "EoamRedProcessRmMsg: VALID MESSAGE"
                           " from RM but pData given from RM is NULL\r\n"));
            }

            /* Sending ACK to RM */
            RmApiSendProtoAckToRM (&ProtoAck);

            break;

        case GO_ACTIVE:

            if (EOAM_RM_NODE_STATUS () == EOAM_ACTIVE_NODE)
            {
                break;
            }

            EoamPrevNodeState = EOAM_RM_NODE_STATUS ();

            EOAM_TRC ((EOAM_RED_TRC, "EoamRedProcessRmMsg: Rcvd GO ACTIVE from "
                       "RM\r\n"));
            EoamRedMakeNodeActive ();
            if (gEoamRedGlobalInfo.bBulkReqRcvd == OSIX_TRUE)
            {
                gEoamRedGlobalInfo.bBulkReqRcvd = OSIX_FALSE;
                gEoamRedGlobalInfo.u4BulkUpdNextPort = EOAM_MIN_PORTS;
                EoamRedProcessBulkReqEvent (NULL);
            }

            if (EoamPrevNodeState == EOAM_IDLE_NODE)
            {
                ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
            }
            else
            {
                ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
            }

            RmApiHandleProtocolEvent (&ProtoEvt);

            break;

        case GO_STANDBY:
            EOAM_TRC ((EOAM_RED_TRC, "EoamRedProcessRmMsg: Rcvd GO STANDBY from"
                       " RM\r\n"));

            if (EOAM_RM_NODE_STATUS () == EOAM_STANDBY_NODE)
            {
                break;
            }

            if (EOAM_RM_NODE_STATUS () == EOAM_IDLE_NODE)
            {
                return;
            }
            else if (EOAM_RM_NODE_STATUS () == EOAM_ACTIVE_NODE)
            {
                EoamRedGoStandbyEvent ();

                ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
                RmApiHandleProtocolEvent (&ProtoEvt);
            }
            gEoamRedGlobalInfo.bBulkReqRcvd = OSIX_FALSE;
            break;

        case RM_INIT_HW_AUDIT:
            /* HW Audit is not implemented for EOAM */
            break;

        case RM_CONFIG_RESTORE_COMPLETE:
            EOAM_TRC ((EOAM_RED_TRC, "EoamRedProcessRmMsg: Rcvd Config "
                       "restore complete from RM\r\n"));
            if (EOAM_RM_NODE_STATUS () == EOAM_IDLE_NODE)
            {
                if (RmGetNodeState () == RM_STANDBY)
                {
                    EoamRedMakeNodeStandbyFromIdle ();
                }

                ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
                RmApiHandleProtocolEvent (&ProtoEvt);
            }
            break;

        case RM_STANDBY_UP:
            EOAM_TRC ((EOAM_RED_TRC, "EoamRedProcessRmMsg: Standby Up event "
                       "from RM\r\n"));
            pData = (tRmNodeInfo *) pMsg->unEoamMsgParam.RmEoamMsg.pData;
            EOAM_NUM_STANDBY_NODES () = pData->u1NumStandby;
            RmReleaseMemoryForMsg ((UINT1 *) pData);
            EoamRedStandbyUpEvent (NULL);
            break;

        case RM_STANDBY_DOWN:
            EOAM_TRC ((EOAM_RED_TRC, "EoamRedProcessRmMsg: Standby Down event "
                       "from RM\r\n"));
            pData = (tRmNodeInfo *) pMsg->unEoamMsgParam.RmEoamMsg.pData;
            EOAM_NUM_STANDBY_NODES () = pData->u1NumStandby;
            RmReleaseMemoryForMsg ((UINT1 *) pData);
            break;

        case RM_COMPLETE_SYNC_UP:
#ifndef CFA_WANTED
            EoamRedTrigHigherLayer (L2_COMPLETE_SYNC_UP);
#endif
            break;

        case L2_COMPLETE_SYNC_UP:
            EoamRedTrigHigherLayer (L2_COMPLETE_SYNC_UP);
            break;

        case L2_INITIATE_BULK_UPDATES:
            EOAM_TRC ((EOAM_RED_TRC, "EoamRedProcessRmMsg: Received "
                       "L2_INITIATE_BULK_UPDATES event from CFA\r\n"));
            EoamRedSendBulkReq ();

            break;
        default:
            EOAM_TRC ((EOAM_RED_TRC, "EoamRedProcessRmMsg: Received "
                       "unknown event from RM - %d\r\n",
                       pMsg->unEoamMsgParam.RmEoamMsg.u1RmEvent));
            break;
    }
    return;
}

/****************************************************************************
 * Function Name      : EoamRedSyncUpData
 * Description        : This function reads the dynamic information present
 *                      in pData and takes neccessary action
 * Input(s)           : pData - Pointer to the RM buffer
 *                      u2DataLen - Size of the buffer given by RM
 * Output(s)          : None
 * Return Value(s)    : None
 ***************************************************************************/
PRIVATE VOID
EoamRedSyncUpData (tRmMsg * pData, UINT2 u2DataLen)
{
    UINT4               u4Offset = 0;
    UINT4               u4MsgType = 0;
    UINT2               u2Len = 0;
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_EOAM_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    EOAM_TRC ((EOAM_RED_TRC, "EoamRedSyncUpData: Rcvd %d bytes from RM.\r\n",
               u2DataLen));

    /* No need to do null pointer check for pMsg as it is done
     * in EoamRedRcvPktFromRm. */
    if (u2DataLen < (EOAM_RM_MSG_TYPE_SIZE + EOAM_RM_LEN_SIZE))
    {
        /* Message size is very less to process. */
        return;
    }

    /* Read message type from pData. */
    RM_GET_DATA_4_BYTE (pData, u4Offset, u4MsgType);
    u4Offset += 4;

    /* Read length from pData. */
    RM_GET_DATA_2_BYTE (pData, u4Offset, u2Len);
    u4Offset += 2;
    if (u2Len != u2DataLen)
    {
        /* Length field present in data and the len given by RM differs. 
         * Something wrong!!!*/
        EOAM_TRC ((EOAM_RED_TRC, "EoamRedSyncUpData: Rcvd VALID MESSAGE from "
                   "RM. Length given by RM and the length of Eoam msg does "
                   "not match - %d,%d\r\n", u2Len, u2DataLen));
        return;
    }

    /* Process the message based on message types. */
    switch (u4MsgType)
    {
        case EOAM_BULK_REQ_MSG:
            if (EOAM_RM_IS_STANDBY_UP () == OSIX_FALSE)
            {
                /* This is a special case, where bulk request msg from
                 * standby is coming before RM_STANDBY_UP. So no need to
                 * process the bulk request now. Bulk updates will be send
                 * on RM_STANDBY_UP event.
                 */
                gEoamRedGlobalInfo.bBulkReqRcvd = OSIX_TRUE;
                break;
            }

            gEoamRedGlobalInfo.bBulkReqRcvd = OSIX_FALSE;
            /* On receiving EOAM_BULK_REQ_MSG, we should restart the
             * bulk updation process.
             */
            gEoamRedGlobalInfo.u4BulkUpdNextPort = EOAM_MIN_PORTS;
            EoamRedProcessBulkReqEvent (NULL);

            break;

        case EOAM_BULK_UPD_TAIL_MSG:
            EOAM_TRC ((EOAM_RED_TRC, "Received Bulk Update tail msg. \r\n"));
            /* On receiving EOAM_BULK_UPD_TAIL_MSG, give indication to RM. 
             * RM gives the trigger to higher
             * layers (LA/STP/VLAN) inorder to send the Bulk Req msg. 
             */
            ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;
            RmApiHandleProtocolEvent (&ProtoEvt);

            break;

        case EOAM_DYNAMIC_CHANGE_MSG:
            EoamRedProcessDynamicMsg (pData, &u4Offset, u2Len);
            break;

        case EOAM_OPER_CHANGE_MSG:
            EoamRedProcessOperChgMsg (pData, &u4Offset, u2Len);
            break;

            /* HITLESS RESTART */
        case EOAM_HR_STDY_ST_PKT_REQ:
            EOAM_TRC ((EOAM_RED_TRC, "Received Steady State Pkt Request "
                       "msg. \n"));
            EoamRedHRProcStdyStPktReq ();
            break;

        default:
            break;
    }
}

/****************************************************************************
 * Function Name      : EoamRedProcessBulkReqEvent
 * Description        : This function gets the dynamic information for all 
 *                      ports
 * Input(s)           : pPeerId - Pointer to the Standby node Id to which
 *                                the bulk update needs to be sent
 * Output(s)          : None
 * Return Value(s)    : None
 ***************************************************************************/
PUBLIC VOID
EoamRedProcessBulkReqEvent (VOID *pvPeerId)
{
    tRmMsg             *pMsg = NULL;
    tEoamPortInfo      *pPortInfo;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4PortNum;
    UINT4               u4Offset = 0;
    UINT4               u4SyncLengthOffset = 0;
    UINT4               u4BulkUpdPortCnt = 0;
    UINT2               u2BufSize = 0;

    ProtoEvt.u4AppId = RM_EOAM_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    UNUSED_PARAM (pvPeerId);

    if (EOAM_RM_NODE_STATUS () != EOAM_ACTIVE_NODE)
    {
        /* Only Active node can process bulk request and can
         * send the bulk update.*/

        gEoamRedGlobalInfo.bBulkReqRcvd = OSIX_TRUE;
        return;
    }

    if ((gEoamGlobalInfo.u1SystemCtrlStatus == EOAM_SHUTDOWN) ||
        (gEoamGlobalInfo.u1ModuleAdminStatus == EOAM_DISABLED))
    {
        /* EOAM completes it's sync up during standby boot-up and this
         * needs to be informed to RMGR
         */
        RmSetBulkUpdatesStatus (RM_EOAM_APP_ID);

        /* No need to update when Module is shutdown or disabled */
        EoamRedSendBulkUpdateTailMsg ();

        return;
    }
    EOAM_TRC ((EOAM_RED_TRC, "EoamRedProcessBulkReqEvent: Rcvd Bulk Req from "
               "new standby node\r\n"));

    u4BulkUpdPortCnt = EOAM_RM_NUM_PORTS_PER_SUB_UPDATE;

    for (u4PortNum = gEoamRedGlobalInfo.u4BulkUpdNextPort;
         u4PortNum <= EOAM_MAX_PORTS && u4BulkUpdPortCnt > 0;
         u4PortNum++, u4BulkUpdPortCnt--)
    {
        /* Update u4BulkUpdNextPort, to resume the next sub bulk update from
         * where the previous sub bulk update left it out.
         */
        gEoamRedGlobalInfo.u4BulkUpdNextPort++;

        EoamUtilGetPortEntry (u4PortNum, &pPortInfo);

        if (pPortInfo == NULL)
        {
            continue;
        }

        /* Sync up only EOAM enabled ports */
        if (pPortInfo->LocalInfo.u1EoamAdminStatus != EOAM_ENABLED)
        {
            continue;
        }

        if ((u2BufSize - u4Offset) < EOAM_RM_DYN_PORTINFO_SIZE)
        {
            /* There is no enough space to fill this port information
             * in to the buffer. Hence send the existing message and
             * construct new message to fill the information of other
             * ports.*/
            if (pMsg != NULL)
            {
                /* Send the existing message to RM. */
                /* Offset of the Length field for update information is
                 * stored in u4SyncLengthOffset. u2SyncMsgLen contains the
                 * size of update information so far written. Hence update
                 * length field at offset u4SyncLengthOffset with the value
                 * u2SyncMsgLen. */
                RM_DATA_ASSIGN_2_BYTE (pMsg, u4SyncLengthOffset,
                                       (u4Offset - EOAM_RM_OFFSET));
                u4SyncLengthOffset += 2;
                /* Note u4Offset contains the number of bytes written in to
                 * the buffer. Hence can be used a buffer size here. */
                if (EoamRedSendMsgToRm (pMsg, (UINT2) u4Offset) == OSIX_FAILURE)
                {
                    ProtoEvt.u4Error = RM_SENDTO_FAIL;
                    RmApiHandleProtocolEvent (&ProtoEvt);
                }
            }
            /* Reset the offset, size fields so that they can be used in the
             * new buffer. */
            u4Offset = EOAM_RM_OFFSET;

            u2BufSize = EOAM_RM_BULK_SPLIT_MSG_SIZE;

            /* Allocate RM buf and fill the message type, and get the pointer
             * to RM buffer, offset of Lenght field. */
            EoamRedConstructMsgHeader (u2BufSize, EOAM_DYNAMIC_CHANGE_MSG,
                                       &pMsg, &u4Offset, &u4SyncLengthOffset);

            if (pMsg == NULL)
            {
                EOAM_TRC ((ALL_FAILURE_TRC,
                           "Bulk Update: Rm alloc failed\r\n"));
                ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
                RmApiHandleProtocolEvent (&ProtoEvt);
                return;
            }

        }

        /* Fill port number */
        RM_DATA_ASSIGN_4_BYTE (pMsg, u4Offset, pPortInfo->u4Port);
        u4Offset += 4;

        EoamRedFillLocalInfo (pPortInfo, pMsg, &u4Offset);

        if (pPortInfo->LocalInfo.u2EoamOperStatus == EOAM_OPER_OPERATIONAL)
        {
            /* Fill Remote Peer Info */
            EoamRedFillPeerInfo (pPortInfo, pMsg, &u4Offset);
        }
    }
    /* Send the last message. */
    if (pMsg != NULL)
    {
        /* Fill the message length at proper offset. */
        RM_DATA_ASSIGN_2_BYTE (pMsg, u4SyncLengthOffset,
                               (u4Offset - EOAM_RM_OFFSET));
        u4SyncLengthOffset += 2;

        /* Note u4Offset contains the number of bytes written in to
         * the buffer. Hence can be used a buffer size here. */
        if (EoamRedSendMsgToRm (pMsg, (UINT2) u4Offset) == OSIX_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            RmApiHandleProtocolEvent (&ProtoEvt);
        }
    }

    if (gEoamRedGlobalInfo.u4BulkUpdNextPort <= EOAM_MAX_PORTS)
    {
        /* Send an event to start the next sub bulk update.
         */
        OsixEvtSend (gEoamGlobalInfo.TaskId, EOAM_RED_SUB_BULK_UPD_EVENT);
    }
    else
    {
        /* EOAM completes it's sync up during standby boot-up and this
         * needs to be informed to RMGR
         */
        RmSetBulkUpdatesStatus (RM_EOAM_APP_ID);

        /* Send the tail msg to indicate the completion of Bulk
         * update process.
         */
        EoamRedSendBulkUpdateTailMsg ();
    }
    return;

}

/****************************************************************************
 * Function Name      : EoamRedProcessOperChgMsg
 * Description        : This function applies the Oper status information
 *                      received for a particular port
 * Input(s)           : pData - pointer to RM buffer
 *                      pu4Offset - offset from read should start
 *                      u2Len - Length of the information in buffer
 * Output(s)          : None
 * Return Value(s)    : None
 ***************************************************************************/
PRIVATE VOID
EoamRedProcessOperChgMsg (tRmMsg * pData, UINT4 *pu4Offset, UINT2 u2Len)
{
    tEoamPortInfo      *pPortInfo = NULL;
    UINT4               u4PortNum = 0;
    UINT1               u1OperStatus = 0;

    EOAM_TRC ((EOAM_RED_TRC, "EoamRedProcessOperChgMsg: Rcvd Oper change "
               "update sync up msg from RM \r\n"));

    if (EOAM_RM_NODE_STATUS () != EOAM_STANDBY_NODE)
    {
        EOAM_TRC ((EOAM_RED_TRC, "EoamRedProcessOperChgMsg: Rcvd oper sync up"
                   "msg from RM when EOAM node is not standby\r\n"));
        return;
    }

    /* EOAM sync data - read the portnum, Oper status */
    /* ------------------------------------------
     * | MsgType | Len | PortNum |  Operational |
     * |         |     |         |  Status      |
     * -----------------------------------------*/

    if ((u2Len - *pu4Offset) != (EOAM_RM_PORTNUM_SIZE + 1))
    {
        /* Pkt len is invalid */
        return;
    }

    /* Read port ID. */
    RM_GET_DATA_4_BYTE (pData, *pu4Offset, u4PortNum);
    *(pu4Offset) += 4;

    EoamUtilGetPortEntry (u4PortNum, &pPortInfo);

    if (pPortInfo == NULL)
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "EoamRedProcessOperChgMsg: PortInfo NULL -"
                   " Invalid port number %d\r\n", u4PortNum));
        return;
    }
    /* Port Operational status */
    RM_GET_DATA_1_BYTE (pData, *pu4Offset, u1OperStatus);
    pPortInfo->u1OperStatus = u1OperStatus;
    return;
}

/****************************************************************************
 * Function Name      : EoamRedProcessDynamicMsg
 * Description        : This function applies the dynamic information 
 *                      received for a particular port
 * Input(s)           : pData - pointer to RM buffer
 *                      pu4Offset - offset from read should start
 *                      u2Len - Length of the information in buffer
 * Output(s)          : None
 * Return Value(s)    : None
 ***************************************************************************/
PRIVATE VOID
EoamRedProcessDynamicMsg (tRmMsg * pData, UINT4 *pu4Offset, UINT2 u2Len)
{
    UINT4               u4PortNum = 0;
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;
    ProtoEvt.u4AppId = RM_EOAM_APP_ID;

    EOAM_TRC ((EOAM_RED_TRC, "EoamRedProcessDynamicMsg: Rcvd dynamic update "
               "sync up msg from RM \r\n"));

    if (EOAM_RM_NODE_STATUS () != EOAM_STANDBY_NODE)
    {
        EOAM_TRC ((EOAM_RED_TRC, "EoamRedProcessDynamicMsg: Rcvd sync up msg "
                   "from RM when EOAM node is not standby\r\n"));
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    /* EOAM sync data - read the portnum, disc status, local info and 
     * peer info */
    /* --------------------------------------------------------
     * | MsgType | Len | PortNum |  Discovery | Local | Peer  |
     * |         |     |         |  Status    | Info  | Info  |
     * --------------------------------------------------------*/

    while (EoamRedReadPortInfo (pData, u2Len, pu4Offset, &u4PortNum)
           != OSIX_FAILURE)
    {
        /* Read sync up data and apply to the standby node */
    }

    return;
}

/****************************************************************************
 * Function Name      : EoamRedReadPortInfo
 * Description        : This function reads a port's info from RM message and
 *                      update the STANDBY node data structures accordingly
 * Input(s)           : pMsg - pointer to RM message
 *                      u2PktLen - Size of the buffer given by RM
 *                      pu4Offset - Offset from where the read has to start
 *
 * Output(s)          : pu4PortNum - Port number read
 * Return Value(s)    : None
 ***************************************************************************/
PRIVATE INT4
EoamRedReadPortInfo (tRmMsg * pMsg, UINT2 u2PktLen, UINT4 *pu4Offset,
                     UINT4 *pu4PortNum)
{
    tEoamPortInfo      *pPortInfo = NULL;
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;
    ProtoEvt.u4AppId = RM_EOAM_APP_ID;

    if (u2PktLen <= *pu4Offset)
    {
        /* Pkt is fully read, so just return failure here. */
        return OSIX_FAILURE;
    }

    if ((u2PktLen - *pu4Offset) <
        (EOAM_RM_PORTNUM_SIZE + EOAM_RM_DYN_LOCAL_SIZE))
    {
        /* Pkt doesn't not contains the full information. */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return OSIX_FAILURE;
    }

    /* Read port ID. */
    RM_GET_DATA_4_BYTE (pMsg, *pu4Offset, *pu4PortNum);
    if (*pu4PortNum == 0)
    {
        EOAM_TRC ((ALL_FAILURE_TRC,
                   "EoamRedReadPortInfo: No more port information \r\n"));
        return OSIX_FAILURE;
    }

    *(pu4Offset) += 4;

    EoamUtilGetPortEntry (*pu4PortNum, &pPortInfo);

    if (pPortInfo == NULL)
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "EoamRedReadPortInfo: PortInfo NULL -"
                   " Invalid port number %d\r\n", *pu4PortNum));
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return OSIX_FAILURE;
    }

    if (pPortInfo->LocalInfo.u1EoamAdminStatus != EOAM_ENABLED)
    {
        EOAM_TRC ((EOAM_RED_TRC, "EoamRedReadPortInfo: Rcvd dynamic update on "
                   "port %d, but EOAM not enabled on the port in standby.."
                   "discarding\r\n", pPortInfo->u4Port));
        /* Move pu4Offset to the end of local info */
        (*pu4Offset) += EOAM_RM_DYN_LOCAL_SIZE;
        return OSIX_SUCCESS;
    }

    /* Read local info and update standby accordingly */
    EoamRedReadLocalInfo (pMsg, pu4Offset, pPortInfo);

    /* If discovery status is Operational, read and update peer info */
    if (pPortInfo->LocalInfo.u2EoamOperStatus == EOAM_OPER_OPERATIONAL)
    {
        EoamRedReadPeerInfo (pMsg, pu4Offset, pPortInfo);
    }
    return OSIX_SUCCESS;;
}

/****************************************************************************
 * Function Name      : EoamRedReadLocalInfo
 * Description        : This function reads local info from RM message and
 *                      update the STANDBY node data structures accordingly
 * Input(s)           : pMsg - pointer to RM message
 *                      pu4Offset - pointer to Offset from where pMsg should
 *                                  be read
 * Output(s)          : pPortInfo - Eoam Port info to be updated
 * Return Value(s)    : None
 ***************************************************************************/
PRIVATE VOID
EoamRedReadLocalInfo (tRmMsg * pMsg, UINT4 *pu4Offset,
                      tEoamPortInfo * pPortInfo)
{
    UINT1               u1LocalPdu = 0;
    UINT1               u1LBStatus = 0;
    /* Read discovery status */
    RM_GET_DATA_2_BYTE (pMsg, *pu4Offset,
                        pPortInfo->LocalInfo.u2EoamOperStatus);
    *(pu4Offset) += 2;

    /* Loopback status */
    RM_GET_DATA_1_BYTE (pMsg, *pu4Offset, u1LBStatus);
    (*pu4Offset)++;

    if ((u1LBStatus == EOAM_LOCAL_LOOPBACK) &&
        (pPortInfo->LoopbackInfo.u1IgnoreRx == EOAM_IGNORE_LB_CMD))
    {
        EOAM_TRC ((EOAM_RED_TRC, "Received local loopback mode update "
                   "when loopback command configured to be ignored\r\n"));
        /* Move pointer to ignore config revision */
        *pu4Offset += 2;
    }
    else
    {
        /* Config Rev should be updated only after loopback status is
         * read and accepted */
        RM_GET_DATA_2_BYTE (pMsg, *pu4Offset,
                            pPortInfo->LocalInfo.u2ConfigRevision);
        *pu4Offset += 2;

        /* Local Loopback mode will be stored only when Loopback
         * commands is configured as permit */
        pPortInfo->LoopbackInfo.u1Status = u1LBStatus;
        /* Update mux, parser state in CtlReq and CFA */
        EoamMuxParSet (pPortInfo, u1LBStatus);

        if (u1LBStatus == EOAM_LOCAL_LOOPBACK)
        {
            /* Update Remote LB flag in CFA */
            EoamSetIfInfo (IF_EOAM_REMOTE_LB, pPortInfo->u4Port, EOAM_ENABLED);
        }
        else if (u1LBStatus == EOAM_NO_LOOPBACK)
        {
            EoamSetIfInfo (IF_EOAM_REMOTE_LB, pPortInfo->u4Port, EOAM_DISABLED);
        }
    }

    /* Read CtlReq */
    RM_GET_DATA_1_BYTE (pMsg, *pu4Offset,
                        pPortInfo->pEoamEnaPortInfo->CtrlReq.
                        b1LocalUnidirectional);
    (*pu4Offset)++;
    RM_GET_DATA_1_BYTE (pMsg, *pu4Offset,
                        pPortInfo->pEoamEnaPortInfo->CtrlReq.b1LocalSatisfied);
    (*pu4Offset)++;
    RM_GET_DATA_1_BYTE (pMsg, *pu4Offset,
                        pPortInfo->pEoamEnaPortInfo->CtrlReq.b1RemoteStable);
    (*pu4Offset)++;
    RM_GET_DATA_1_BYTE (pMsg, *pu4Offset,
                        pPortInfo->pEoamEnaPortInfo->CtrlReq.
                        b1RemoteStateValid);
    (*pu4Offset)++;
    RM_GET_DATA_1_BYTE (pMsg, *pu4Offset,
                        pPortInfo->pEoamEnaPortInfo->CtrlReq.b1LocalDyingGasp);
    (*pu4Offset)++;
    RM_GET_DATA_1_BYTE (pMsg, *pu4Offset,
                        pPortInfo->pEoamEnaPortInfo->CtrlReq.
                        b1LocalCriticalEvent);
    (*pu4Offset)++;
    RM_GET_DATA_1_BYTE (pMsg, *pu4Offset,
                        pPortInfo->pEoamEnaPortInfo->CtrlReq.u1LocalLinkStatus);
    (*pu4Offset)++;

    /* Read CtlInd */
    RM_GET_DATA_1_BYTE (pMsg, *pu4Offset, u1LocalPdu);
    (*pu4Offset)++;
    pPortInfo->pEoamEnaPortInfo->CtrlInd.LocalPdu = u1LocalPdu;
    RM_GET_DATA_1_BYTE (pMsg, *pu4Offset,
                        pPortInfo->pEoamEnaPortInfo->CtrlInd.b1LocalStable);
    (*pu4Offset)++;
    RM_GET_DATA_1_BYTE (pMsg, *pu4Offset,
                        pPortInfo->pEoamEnaPortInfo->CtrlInd.b1LocalEvaluating);
    (*pu4Offset)++;

    return;
}

/****************************************************************************
 * Function Name      : EoamRedReadPeerInfo
 * Description        : This function reads peer info from RM message and
 *                      update the STANDBY node data structures accordingly
 * Input(s)           : pMsg - pointer to RM message
 *                      pu4Offset - pointer to Offset from where pMsg should
 *                                  be read
 * Output(s)          : pPortInfo - Eoam Port info to be updated
 * Return Value(s)    : None
 ***************************************************************************/
PRIVATE VOID
EoamRedReadPeerInfo (tRmMsg * pMsg, UINT4 *pu4Offset, tEoamPortInfo * pPortInfo)
{
    RM_GET_DATA_4_BYTE (pMsg, *pu4Offset, pPortInfo->RemoteInfo.u4VendorInfo);
    *pu4Offset += 4;
    RM_GET_DATA_2_BYTE (pMsg, *pu4Offset,
                        pPortInfo->RemoteInfo.u2ConfigRevision);
    *pu4Offset += 2;
    RM_GET_DATA_2_BYTE (pMsg, *pu4Offset,
                        pPortInfo->RemoteInfo.u2MaxOamPduSize);
    *pu4Offset += 2;
    RM_GET_DATA_2_BYTE (pMsg, *pu4Offset, pPortInfo->RemoteInfo.u2Flags);
    *pu4Offset += 2;
    RM_GET_DATA_1_BYTE (pMsg, *pu4Offset, pPortInfo->RemoteInfo.u1State);
    *pu4Offset += 1;
    RM_GET_DATA_1_BYTE (pMsg, *pu4Offset, pPortInfo->RemoteInfo.u1Mode);
    *pu4Offset += 1;
    CRU_BUF_Copy_FromBufChain (pMsg, pPortInfo->RemoteInfo.MacAddress,
                               *pu4Offset, sizeof (tMacAddr));
    *pu4Offset += sizeof (tMacAddr);
    RM_GET_DATA_1_BYTE (pMsg, *pu4Offset,
                        pPortInfo->RemoteInfo.u1FnsSupportedBmp);
    *pu4Offset += 1;
    CRU_BUF_Copy_FromBufChain (pMsg, pPortInfo->RemoteInfo.au1VendorOui,
                               *pu4Offset, EOAM_OUI_LENGTH);
    *pu4Offset += EOAM_OUI_LENGTH;
    return;
}

/****************************************************************************
 * Function Name      : EoamRedMakeNodeActive
 * Description        : This function makes the node active. If the node is
 *                      is in IDLE state then it enables the module if the
 *                      protocol admin status is enable. If the node is
 *                      standby, then depending on the states it starts the
 *                      timers and triggers the hardware audit
 * Input(s)           : None
 * Output(s)          : None
 * Return Value(s)    : None
 ***************************************************************************/
PRIVATE VOID
EoamRedMakeNodeActive (VOID)
{
    if (EOAM_RM_NODE_STATUS () == EOAM_ACTIVE_NODE)
    {
        /* Node is already active. Nothing to do. */
        EOAM_TRC ((EOAM_RED_TRC, "EoamRedMakeNodeActive: Make active trigger "
                   "received for already active node. "
                   "Something wrong in RM\r\n"));

        return;
    }

    if (gEoamGlobalInfo.u1SystemCtrlStatus != EOAM_START)
    {
        EOAM_RM_NODE_STATUS () = EOAM_ACTIVE_NODE;
        EOAM_RM_GET_NUM_STANDBY_NODES_UP ();
        EOAM_TRC ((EOAM_RED_TRC, "EOAM Node made active when EOAM is "
                   "not started\r\n"));
        return;
    }

    if (EOAM_RM_NODE_STATUS () == EOAM_IDLE_NODE)
    {
        /* Node is coming up for the first time. Move from IDLE state to active 
         * state.*/
        EoamRedMakeNodeActiveFromIdle ();
    }
    else
    {
        EoamRedMakeNodeActiveFromStandby ();
    }
    /* Send event to Eoam LM to start timer */
    OsixEvtSend (gEoamLmGlobalInfo.TaskId, EOAM_LM_GO_ACTIVE_EVENT);

    return;
}

/****************************************************************************
 * Function Name      : EoamRedMakeNodeActiveFromStandby
 * Description        : This function makes node active from standby state
 * Input(s)           : None
 * Output(s)          : None
 * Return Value(s)    : None
 ****************************************************************************/
PRIVATE VOID
EoamRedMakeNodeActiveFromStandby (VOID)
{
    tEoamPortInfo      *pEoamPortInfo = NULL;
    tTMO_DLL_NODE      *pEoamDllNode = NULL;

    EOAM_RM_NODE_STATUS () = EOAM_ACTIVE_NODE;
    EOAM_RM_GET_NUM_STANDBY_NODES_UP ();
    if (gEoamGlobalInfo.u1ModuleAdminStatus == EOAM_ENABLED)
    {
        /* Enable EOAM module status  */
        gEoamGlobalInfo.u1ModuleStatus = EOAM_ENABLED;
        TMO_DLL_Scan (&(gEoamGlobalInfo.EoamEnaPortDllList),
                      pEoamDllNode, tTMO_DLL_NODE *)
        {
            pEoamPortInfo = (tEoamPortInfo *) pEoamDllNode;
            /* Construct Info PDU  for EOAM enabled ports */
            EoamCtlTxConstructInfoPdu (pEoamPortInfo);

            if (pEoamPortInfo->LocalInfo.u2EoamOperStatus ==
                EOAM_OPER_OPERATIONAL)
            {
                EoamCtlTxSendInfoPdu (pEoamPortInfo);
                pEoamPortInfo->pEoamEnaPortInfo->u1PduTxCntPerSec = 0;
                /* Start Lost link timer if discovery already settled */
                if (TmrStart (gEoamGlobalInfo.EoamTmrListId,
                              &(pEoamPortInfo->pEoamEnaPortInfo->
                                LostLinkAppTmr), EOAM_LOST_LINK_TMR_TYPE,
                              EOAM_LOST_LINK_TIMEOUT, 0) == TMR_FAILURE)
                {
                    EOAM_TRC ((EOAM_RED_TRC | EOAM_DISCOVERY_TRC,
                               "StartTimer for LostLinkAppTmr FAILED!!!\n"));
                }
            }
            else
            {
                /* Start discovery SEM */
                EoamSemMachine (pEoamPortInfo, EOAM_DISC_EVENT_OPER_UP);
            }
        }

        /* Start the global PDU Tx Timer */
        if (TmrStart
            (gEoamGlobalInfo.EoamTmrListId, &(gEoamGlobalInfo.PduTxTmr),
             EOAM_PDU_TX_TMR_TYPE, EOAM_PDU_TX_TIMEOUT, 0) == TMR_FAILURE)
        {
            EOAM_TRC ((OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                       "EoamRedMakeNodeActiveFromStandby: PduTx Start Timer "
                       "FAILED !!!\r\n"));
            return;
        }
    }

    return;
}

/****************************************************************************
 * Function Name      : EoamRedMakeNodeActiveFromIdle
 * Description        : This function makes node active from idle state
 * Input(s)           : None
 * Output(s)          : None
 * Return Value(s)    : None
 ***************************************************************************/
PRIVATE VOID
EoamRedMakeNodeActiveFromIdle (VOID)
{
    EOAM_RM_NODE_STATUS () = EOAM_ACTIVE_NODE;
    EOAM_RM_GET_NUM_STANDBY_NODES_UP ();

    EOAM_TRC ((EOAM_RED_TRC, "EoamRedMakeNodeActiveFromIdle: Made node "
               "Active from IDLE state\r\n"));

    if (gEoamGlobalInfo.u1ModuleAdminStatus == EOAM_ENABLED)
    {
        /* Node is enabled for the first time - Enable Module */
        EoamModuleEnable ();
    }
    EOAM_TRC ((EOAM_RED_TRC, "EoamRedMakeNodeActiveFromIdle: "
               "Enabled module\r\n"));
}

/****************************************************************************
 * Function Name      : EoamRedGoStandbyEvent
 * Description        : This function handles the GO_STANDBY event from RM
 *                      It restarts the module and makes the node as standby
 * Input(s)           : None
 * Output(s)          : None
 * Return Value(s)    : None
****************************************************************************/
PRIVATE VOID
EoamRedGoStandbyEvent (VOID)
{

    if (EOAM_RM_NODE_STATUS () == EOAM_ACTIVE_NODE)
    {
        /* Make the node status as standby. */
        EOAM_RM_NODE_STATUS () = EOAM_STANDBY_NODE;
        EOAM_NUM_STANDBY_NODES () = 0;
        /* Disable the module inorder to flush the dynamic information */
        if (gEoamGlobalInfo.u1ModuleAdminStatus == EOAM_ENABLED)
        {
            EoamModuleDisable ();
            if (EoamModuleEnable () != OSIX_SUCCESS)
            {
                EOAM_TRC ((EOAM_RED_TRC, "EoamRedGoStandbyEvent: Not able to "
                           "enable EOAM while making node standby from active "
                           "state\r\n"));
            }
            /* Send event to Eoam LM to stop polling timer */
            OsixEvtSend (gEoamLmGlobalInfo.TaskId, EOAM_LM_GO_STANDBY_EVENT);
        }
    }
    else if (EOAM_RM_NODE_STATUS () == EOAM_STANDBY_NODE)
    {
        /* Node is already a standby node. This should not happen. Assert if 
         * it happens. */
        EOAM_TRC ((EOAM_RED_TRC, "EoamRedGoStandbyEvent: Node is already "
                   "standby\r\n"));
    }
}

/**************************************************************************
 * Function Name      : EoamRedMakeNodeStandbyFromIdle
 * Description        : This function makes node standby from idle state
 * Input(s)           : None
 * Output(s)          : None
 * Return Value(s)    : None
**************************************************************************/
PRIVATE VOID
EoamRedMakeNodeStandbyFromIdle (VOID)
{
    EOAM_RM_NODE_STATUS () = EOAM_STANDBY_NODE;
    EOAM_NUM_STANDBY_NODES () = 0;
    if (gEoamGlobalInfo.u1ModuleAdminStatus == EOAM_ENABLED)
    {
        EoamModuleEnable ();
        EOAM_TRC ((EOAM_RED_TRC,
                   "EoamRedMakeNodeStandbyFromIdle: Enabled module\r\n"));
    }
}

/****************************************************************************
 * Function Name      : EoamRedStandbyUpEvent
 * Description        : This function handles the STANDBY UP event from RM
 *                      If node is active, calls EoamRedProcessBulkReqEvent
 * Input(s)           : pvPeerId - PeerId to which the BulkUpdate has to be
 *                      sent
 * Output(s)          : None
 * Return Value(s)    : None
 ***************************************************************************/
PRIVATE VOID
EoamRedStandbyUpEvent (VOID *pvPeerId)
{

    UNUSED_PARAM (pvPeerId);

    if (gEoamRedGlobalInfo.bBulkReqRcvd == OSIX_TRUE)
    {
#if !defined (CFA_WANTED)
        gEoamRedGlobalInfo.bBulkReqRcvd = OSIX_FALSE;
        /* Bulk request msg is recieved before RM_STANDBY_UP event.
         * So we are sending bulk updates now.
         */
        gEoamRedGlobalInfo.u4BulkUpdNextPort = EOAM_MIN_PORTS;
        EoamRedProcessBulkReqEvent (NULL);
#endif
    }
}

/*****************************************************************************
 * Function Name      : EoamRedTrigHigherLayer
 * Description        : This function will send the given event to the next  
 *                      higher layer (can be PNAC or LA or STP or VLAN)
 * Input(s)           : u1TrigType - Trigger Type
 * Output(s)          : None
 * Return Value(s)    : None
 ****************************************************************************/
PRIVATE VOID
EoamRedTrigHigherLayer (UINT1 u1TrigType)
{
#ifdef PNAC_WANTED
    PnacRedRcvPktFromRm (u1TrigType, NULL, 0);
#else
#ifdef LA_WANTED
    LaRedRcvPktFromRm (u1TrigType, NULL, 0);
#else
#if defined(RSTP_WANTED) || defined(MSTP_WANTED)
    AstHandleUpdateEvents (u1TrigType, NULL, 0);
#else
#ifdef VLAN_WANTED
    VlanRedHandleUpdateEvents (u1TrigType, NULL, 0);
#else
#ifdef LLDP_WANTED
    LldpRedRcvPktFromRm (u1TrigType, NULL, 0);
#else
    if ((EOAM_RM_NODE_STATUS () == EOAM_ACTIVE_NODE) &&
        ((u1TrigType == RM_COMPLETE_SYNC_UP) ||
         (u1TrigType == L2_COMPLETE_SYNC_UP)))
    {
        /* Send an event to RM to notify that sync up is completed */
        if (RmSendEventToRmTask (L2_SYNC_UP_COMPLETED) != RM_SUCCESS)
        {
            EOAM_TRC ((EOAM_RED_TRC, "EOAM send event L2_SYNC_UP_COMPLETED to "
                       "RM failed\n"));
        }
    }
#endif /* LLDP_WANTED */
#endif /* VLAN_WANTED */
#endif /* RSTP_WANTED / MSTP_WANTED */
#endif /* LA_WANTED */
#endif /* PNAC_WANTED */
}

/****************************************************************************
 * Function Name      : EoamRedSendBulkReq                                   
 * Description        : This function is sends a bulk request to the active  
 *                      node of the system.                                  
 * Input(s)           : None.                                                
 * Output(s)          : None                                                 
 * Return Value(s)    : None.                                                
 *****************************************************************************/
PRIVATE VOID
EoamRedSendBulkReq (VOID)
{
    tRmMsg             *pMsg = NULL;
    UINT2               u4BufSize;
    UINT4               u4Offset = 0;
    UINT4               u4LenFieldOffset = 0;
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;
    ProtoEvt.u4AppId = RM_EOAM_APP_ID;

    u4BufSize = EOAM_RM_MSG_TYPE_SIZE + EOAM_RM_LEN_SIZE;

    /* Allocate RM buffer and fill the message type and return the following:
     * RM buffer pointer, offset to the length and NumPorts fields in the 
     * buffer. */
    EoamRedConstructMsgHeader (u4BufSize, EOAM_BULK_REQ_MSG, &pMsg,
                               &u4Offset, &u4LenFieldOffset);

    if (NULL == pMsg)
    {
        /* RM msg allocation failed. Cannot send bulk update. */
        EOAM_TRC ((ALL_FAILURE_TRC, "EoamRedSendBulkReq: RM msg allocation "
                   "failed. Cannot send dynamic update\r\n"));
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    /* Fill the correct len at the offset initially set. */
    RM_DATA_ASSIGN_2_BYTE (pMsg, u4LenFieldOffset, (u4Offset - EOAM_RM_OFFSET));

    EOAM_TRC ((EOAM_RED_TRC, "EoamRedSendBulkReq: "
               "Sending %d bytes to RM\r\n", u4Offset));

    /* Send the constructed message to Standby node. */
    if (EoamRedSendMsgToRm (pMsg, u4BufSize) == OSIX_FAILURE)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
    }

    return;
}

/****************************************************************************
 * Function Name      : EoamRedSendBulkUpdateTailMsg                         
 * Description        : This function will send the tail msg to the standby  
 *                      node, which indicates the completion of Bulk update  
 *                      process.                                             
 * Input(s)           : None.                                                
 * Output(s)          : None                                                 
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE.                         
 ****************************************************************************/
PRIVATE INT4
EoamRedSendBulkUpdateTailMsg (VOID)
{
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4Offset = EOAM_RM_OFFSET;
    UINT2               u2BufSize;

    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;
    ProtoEvt.u4AppId = RM_EOAM_APP_ID;

    if (EOAM_RM_NODE_STATUS () != EOAM_ACTIVE_NODE)
    {
        EOAM_TRC ((EOAM_RED_TRC, "EOAM: Node is not active."
                   "Bulk update tail msg not sent.\r\n"));
        return OSIX_SUCCESS;
    }
    /* Form a bulk update tail message. 

     *        <---------4 Bytes--------><---2 Bytes-->
     ************************************************* 
     *        *                        *             *
     * RM Hdr * EOAM_BULK_UPD_TAIL_MSG * Msg Length  *
     *        *                        *             *
     *************************************************

     * The RM Hdr shall be included by RM.
     */

    u2BufSize = EOAM_RM_MSG_TYPE_SIZE + EOAM_RM_LEN_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "Rm alloc failed\r\n"));
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return (OSIX_FAILURE);
    }

    /* Fill the message type. */
    RM_DATA_ASSIGN_4_BYTE (pMsg, u4Offset, EOAM_BULK_UPD_TAIL_MSG);
    u4Offset += 4;
    /* This length field will not be used. It is just to keep the
     * this message structure similar to that of other sync msgs. */
    RM_DATA_ASSIGN_2_BYTE (pMsg, u4Offset, u2BufSize);
    u4Offset += 2;
    if (EoamRedSendMsgToRm (pMsg, u2BufSize) == OSIX_FAILURE)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function Name      : EoamRedSyncUpOperStatus
 * Description        : This function is sends a sync up message to standby
 *                      node to syncup the change in operational status of
 *                      an interface.
 * Input(s)           : pPortInfo - pointer to Eoam Portinfo
 * Output(s)          : None
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
EoamRedSyncUpOperStatus (tEoamPortInfo * pPortInfo)
{
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4Offset = 0;
    UINT4               u4LenFieldOffset = 0;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;
    ProtoEvt.u4AppId = RM_EOAM_APP_ID;

    if (EOAM_RM_NODE_STATUS () == EOAM_STANDBY_NODE)
    {
        /* When the node is acting as standby, no sync. Just return. */
        EOAM_TRC ((EOAM_RED_TRC, "EoamRedSyncUpOperStatus: Dynamic sync up "
                   "information cannot be sent from Standby node\r\n"));
        return;
    }

    if (EOAM_IS_STANDBY_UP () == OSIX_FALSE)
    {
        /* When there are no standby nodes, just return. */
        EOAM_TRC ((EOAM_RED_TRC, "EoamRedSyncUpOperStatus: Dynamic sync up "
                   "information cannot be sent if Standby node is down\r\n"));
        return;
    }

    /* Allocate RM buffer and fill the message type and return the following:
     * RM buffer pointer, offset to the length and NumPorts fields in the 
     * buffer. */
    EoamRedConstructMsgHeader (EOAM_RM_OPERCHG_MSG_SIZE,
                               EOAM_OPER_CHANGE_MSG, &pMsg,
                               &u4Offset, &u4LenFieldOffset);

    if (NULL == pMsg)
    {
        /* RM msg allocation failed. Cannot send bulk update. */
        EOAM_TRC ((ALL_FAILURE_TRC, "EoamRedSyncUpOperStatus: RM msg "
                   "allocation failed. Cannot send dynamic update!\r\n"));
        return;
    }

    /* Fill port number */
    RM_DATA_ASSIGN_4_BYTE (pMsg, u4Offset, pPortInfo->u4Port);
    u4Offset += 4;

    /* Fill port operational status */
    RM_DATA_ASSIGN_1_BYTE (pMsg, u4Offset, pPortInfo->u1OperStatus);
    u4Offset += 1;

    /* Fill the correct len at the offset initially set. */
    RM_DATA_ASSIGN_2_BYTE (pMsg, u4LenFieldOffset,
                           (UINT2) (u4Offset - EOAM_RM_OFFSET));

    EOAM_TRC ((EOAM_RED_TRC, "EoamRedSyncUpOperStatus: "
               "Sending %d bytes to RM\r\n", u4Offset));

    /* Send the constructed message to Standby node. */
    if (EoamRedSendMsgToRm (pMsg, (UINT2) (u4Offset - EOAM_RM_OFFSET))
        == OSIX_FAILURE)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
    }
    return;
}

/****************************************************************************
 * Function Name      : EoamRedSyncUpDynamicInfo
 * Description        : This function is used send a discovery state sync up
 *                      message to standby node. Only Discovery operational
 *                      and non-operational states are updated to the standby
 *                      node
 * Input(s)           : pPortInfo - pointer to Eoam Portinfo
 * Output(s)          : None
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
EoamRedSyncUpDynamicInfo (tEoamPortInfo * pPortInfo)
{
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4Offset = 0;
    UINT4               u4LenFieldOffset = 0;
    UINT1               au1Tmp[EOAM_RM_DYN_REMOTE_SIZE];

    MEMSET (au1Tmp, 0, EOAM_RM_DYN_REMOTE_SIZE);
    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;
    ProtoEvt.u4AppId = RM_EOAM_APP_ID;

    if (EOAM_RM_NODE_STATUS () == EOAM_STANDBY_NODE)
    {
        /* When the node is acting as standby, no sync. Just return. */
        EOAM_TRC ((EOAM_RED_TRC, "EoamRedSyncUpDynamicInfo: Dynamic sync up "
                   "information cannot be sent from Standby node\r\n"));
        return;
    }
    if (EOAM_IS_STANDBY_UP () == OSIX_FALSE)
    {
        /* When there are no standby nodes, just return. */
        EOAM_TRC ((EOAM_RED_TRC, "EoamRedSyncUpDynamicInfo: Dynamic sync up "
                   "information cannot be sent if Standby node is down\r\n"));
        return;
    }

    /* Allocate RM buffer and fill the message type and return the following:
     * RM buffer pointer, offset to the length and NumPorts fields in the 
     * buffer. */
    EoamRedConstructMsgHeader (EOAM_RM_DYNAMIC_MSG_SIZE,
                               EOAM_DYNAMIC_CHANGE_MSG, &pMsg,
                               &u4Offset, &u4LenFieldOffset);

    if (NULL == pMsg)
    {
        /* RM msg allocation failed. Cannot send bulk update. */
        EOAM_TRC ((ALL_FAILURE_TRC, "EoamRedSyncUpDynamicInfo: RM msg "
                   "allocation failed. Cannot send dynamic update!\r\n"));
        return;
    }

    /* Fill port number */
    RM_DATA_ASSIGN_4_BYTE (pMsg, u4Offset, pPortInfo->u4Port);
    u4Offset += 4;

    /* Fill Local info */
    EoamRedFillLocalInfo (pPortInfo, pMsg, &u4Offset);

    if (pPortInfo->LocalInfo.u2EoamOperStatus == EOAM_OPER_OPERATIONAL)
    {
        /* Fill Remote Peer Info */
        EoamRedFillPeerInfo (pPortInfo, pMsg, &u4Offset);
    }

    /* Fill the correct len at the offset initially set. */
    RM_DATA_ASSIGN_2_BYTE (pMsg, u4LenFieldOffset,
                           (UINT2) (u4Offset - EOAM_RM_OFFSET));

    EOAM_TRC ((EOAM_RED_TRC, "EoamRedSyncUpDynamicInfo: "
               "Sending %d bytes to RM\r\n", u4Offset));

    /* Send the constructed message to Standby node. */
    if (EoamRedSendMsgToRm (pMsg, (UINT2) (u4Offset - EOAM_RM_OFFSET))
        == OSIX_FAILURE)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
    }
    return;
}

/*****************************************************************************
 * Function Name      : EoamRedFillLocalInfo
 * Description        : This function fills the local info - u2ConfigRevision,
 *                      CtrlInd and CtrlReq in pMsg from the given offset
 * Input(s)           : pPortInfo - pointer to Eoam portinfo
 *                      pu4Offset  - Offset to start filling the buffer
 * Output(s)          : pMsg - Pointer to the RM input buffer
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE VOID
EoamRedFillLocalInfo (tEoamPortInfo * pPortInfo, tRmMsg * pMsg,
                      UINT4 *pu4Offset)
{
    UINT4               u4BulkUnitSize = 0;
    UINT2               u2DiscStatus = 0;
    /* Fill Discovery status */
    if (pPortInfo->LocalInfo.u2EoamOperStatus == EOAM_OPER_OPERATIONAL)
    {
        u2DiscStatus = EOAM_OPER_OPERATIONAL;
    }
    else
    {
        u2DiscStatus = EOAM_OPER_DISABLE;
    }

    RM_DATA_ASSIGN_2_BYTE (pMsg, *pu4Offset, u2DiscStatus);
    *pu4Offset += 2;

    /* Loopback status */
    RM_DATA_ASSIGN_1_BYTE (pMsg, *pu4Offset, pPortInfo->LoopbackInfo.u1Status);
    (*pu4Offset)++;

    /* Fill local config revision */
    RM_DATA_ASSIGN_2_BYTE (pMsg, *pu4Offset,
                           pPortInfo->LocalInfo.u2ConfigRevision);
    *pu4Offset += 2;

    RM_DATA_ASSIGN_1_BYTE (pMsg, *pu4Offset,
                           pPortInfo->pEoamEnaPortInfo->CtrlReq.
                           b1LocalUnidirectional);
    (*pu4Offset)++;
    RM_DATA_ASSIGN_1_BYTE (pMsg, *pu4Offset,
                           pPortInfo->pEoamEnaPortInfo->CtrlReq.
                           b1LocalSatisfied);
    (*pu4Offset)++;
    RM_DATA_ASSIGN_1_BYTE (pMsg, *pu4Offset,
                           pPortInfo->pEoamEnaPortInfo->CtrlReq.b1RemoteStable);
    (*pu4Offset)++;
    RM_DATA_ASSIGN_1_BYTE (pMsg, *pu4Offset,
                           pPortInfo->pEoamEnaPortInfo->CtrlReq.
                           b1RemoteStateValid);
    (*pu4Offset)++;
    RM_DATA_ASSIGN_1_BYTE (pMsg, *pu4Offset,
                           pPortInfo->pEoamEnaPortInfo->CtrlReq.
                           b1LocalDyingGasp);
    (*pu4Offset)++;
    RM_DATA_ASSIGN_1_BYTE (pMsg, *pu4Offset,
                           pPortInfo->pEoamEnaPortInfo->CtrlReq.
                           b1LocalCriticalEvent);
    (*pu4Offset)++;
    RM_DATA_ASSIGN_1_BYTE (pMsg, *pu4Offset,
                           pPortInfo->pEoamEnaPortInfo->CtrlReq.
                           u1LocalLinkStatus);
    (*pu4Offset)++;

    /* Fill CtlInd */
    RM_DATA_ASSIGN_1_BYTE (pMsg, *pu4Offset,
                           pPortInfo->pEoamEnaPortInfo->CtrlInd.LocalPdu);
    (*pu4Offset)++;
    RM_DATA_ASSIGN_1_BYTE (pMsg, *pu4Offset,
                           pPortInfo->pEoamEnaPortInfo->CtrlInd.b1LocalStable);
    (*pu4Offset)++;
    RM_DATA_ASSIGN_1_BYTE (pMsg, *pu4Offset,
                           pPortInfo->pEoamEnaPortInfo->CtrlInd.
                           b1LocalEvaluating);
    (*pu4Offset)++;
    if ((u1MemEstFlag == 1) && (EOAM_RM_HR_STATUS () != EOAM_HR_STATUS_DISABLE))

    {
        /* This is to find the memory required for syncing EoamRedFillLOCALInfo */
        u4BulkUnitSize =
            sizeof (UINT4) + sizeof (UINT2) + sizeof (UINT2) + sizeof (UINT1) +
            sizeof (UINT1) + sizeof (UINT1) + sizeof (UINT1) + sizeof (UINT1) +
            sizeof (UINT1) + sizeof (UINT1) + sizeof (UINT1) + sizeof (UINT1) +
            sizeof (UINT1) + sizeof (UINT1);
        /* updating the sizing structure for the data which are stored in file */
        IssSzUpdateSizingInfoForHR ((CHR1 *) "EOAM", (CHR1 *) "tEoamPortInfo",
                                    u4BulkUnitSize);
        /* Memory estimation are done , so changing the flag to next bulk message  */
        u1MemEstFlag = ISSSZ_EOAM_LOCAL_INFO;
    }

    return;
}

/*****************************************************************************
 * Function Name      : EoamRedFillPeerInfo
 * Description        : This function fills the Eoam peer info 
 * Input(s)           : pPortInfo - pointer to Eoam portinfo
 *                      pu4Offset  - Offset to start filling the buffer
 * Output(s)          : pMsg - Pointer to the RM input buffer
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE VOID
EoamRedFillPeerInfo (tEoamPortInfo * pPortInfo, tRmMsg * pMsg, UINT4 *pu4Offset)
{
    UINT4               u4BulkUnitSize = 0;

    /* Fill peer info tEoamPeerInfo */
    RM_DATA_ASSIGN_4_BYTE (pMsg, *pu4Offset,
                           pPortInfo->RemoteInfo.u4VendorInfo);
    *pu4Offset += 4;
    RM_DATA_ASSIGN_2_BYTE (pMsg, *pu4Offset,
                           pPortInfo->RemoteInfo.u2ConfigRevision);
    *pu4Offset += 2;
    RM_DATA_ASSIGN_2_BYTE (pMsg, *pu4Offset,
                           pPortInfo->RemoteInfo.u2MaxOamPduSize);
    *pu4Offset += 2;
    RM_DATA_ASSIGN_2_BYTE (pMsg, *pu4Offset, pPortInfo->RemoteInfo.u2Flags);
    *pu4Offset += 2;
    RM_DATA_ASSIGN_1_BYTE (pMsg, *pu4Offset, pPortInfo->RemoteInfo.u1State);
    *pu4Offset += 1;
    RM_DATA_ASSIGN_1_BYTE (pMsg, *pu4Offset, pPortInfo->RemoteInfo.u1Mode);
    *pu4Offset += 1;

    CRU_BUF_Copy_OverBufChain (pMsg, pPortInfo->RemoteInfo.MacAddress,
                               *pu4Offset, sizeof (tMacAddr));
    *pu4Offset += sizeof (tMacAddr);

    RM_DATA_ASSIGN_1_BYTE (pMsg, *pu4Offset,
                           pPortInfo->RemoteInfo.u1FnsSupportedBmp);
    *pu4Offset += 1;
    CRU_BUF_Copy_OverBufChain (pMsg, pPortInfo->RemoteInfo.au1VendorOui,
                               *pu4Offset, EOAM_OUI_LENGTH);
    *pu4Offset += EOAM_OUI_LENGTH;
    if (u1MemEstFlag == ISSSZ_EOAM_LOCAL_INFO)
    {
        /* This is to find the memory required for syncing EoamRedFillPEERInfo */
        u4BulkUnitSize = sizeof (UINT4) + sizeof (UINT2) + sizeof (UINT2) +
            sizeof (UINT2) + sizeof (UINT1) + sizeof (UINT1) + sizeof (UINT1);
        /* updating the sizing structure for the data which are stored in file */

        IssSzUpdateSizingInfoForHR ((CHR1 *) "EOAM", (CHR1 *) "tEoamPortInfo",
                                    u4BulkUnitSize);
        /* Memory estimation are done , so changing the flag to next bulk message  */
        u1MemEstFlag = ISSSZ_EOAM_PEER_INFO;
    }
    return;
}

/*****************************************************************************
 * Function Name      : EoamRedSendMsgToRm
 * Description        : This function constructs the RM message from the
 *                      given linear buf and sends it to Redundancy Manager
 * Input(s)           : pMsg - Pointer to the RM input buffer
 *                      u2BufSize  - Size of the given input buffer
 * Output(s)          : None
 * Return Value(s)    : OSIX_SUCCESS - if sync msg is sent successfully
 *                      OSIX_FAILURE - otherwise
 *****************************************************************************/
PRIVATE INT4
EoamRedSendMsgToRm (tRmMsg * pMsg, UINT2 u2BufSize)
{
    /* Call the API provided by RM to send the data to RM */
    if (RmEnqMsgToRmFromAppl (pMsg, u2BufSize, RM_EOAM_APP_ID, RM_EOAM_APP_ID)
        == RM_FAILURE)
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "Enq to RM from appl failed\r\n"));
        /* pMsg is freed only in failure case. RM will free the buf
         * in success case. */
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function Name      : EoamRedConstructMsgHeader
 * Description        : Allocate RM buffer and fill the message header       
 *                      required for syncup message.                         
 * Input(s)           : u4Size - Size of the Sync Up message                 
 *                      UpdateMsgType - Type of the sync Up message          
 * Output(s)          : ppMsg - Pointer to the allocated RM buffer.          
 *                      pu4Offset - Update offset value from where the next  
 *                                  write operation in RM buffer should be   
 *                                  started.                                 
 *                      pu4LenFieldOffset - Offset of the length filed       
 * Return Value(s)    : None.                                                
 *****************************************************************************/
PRIVATE VOID
EoamRedConstructMsgHeader (UINT4 u4Size, eEoamRedUpdateMsgType UpdateMsgType,
                           tRmMsg ** ppMsg, UINT4 *pu4Offset,
                           UINT4 *pu4LenFieldOffset)
{

    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting  */

    if ((*ppMsg = RM_ALLOC_TX_BUF (u4Size)) == NULL)
    {
        return;
    }

    *pu4Offset = EOAM_RM_OFFSET;    /* Start filling from EOAM_RM_OFFSET. */

    /* Fill the message type. u4Offset will be incremented by 4 after this 
     * put.*/
    RM_DATA_ASSIGN_4_BYTE (*ppMsg, *pu4Offset, (UINT4) UpdateMsgType);
    *pu4Offset += 4;

    /* Previous put_4_byte would have updated the u4Offset. Set the len 
     * field offset. */
    *pu4LenFieldOffset = *pu4Offset;

    /* move the offset by 2 bytes - Length will be filled in here later */
    *pu4Offset += 2;
}

/******************************************************************************
 * Function Name      : EoamRestartModule
 * Description        : This function will restart EOAM module
 * Input(s)           : None
 * Output(s)          : None
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
EoamRestartModule (VOID)
{
    EoamLock ();

    EoamHandleModuleShutDown ();

    /* Node will move to IDLE state in this function */
    if (EoamHandleModuleStart () != OSIX_SUCCESS)
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "EOAM start failed during restart for "
                   "applying configuration database\r\n"));
        EoamUnLock ();
        return OSIX_FAILURE;
    }

    EoamUnLock ();

    return OSIX_SUCCESS;
}

/* HITLESS RESTART */
/*****************************************************************************/
/* Function Name      : EoamRedHRProcStdyStPktReq                            */
/*                                                                           */
/* Description        : This function triggers the steady state packet from  */
/*                      EOAM to the RM by setting the periodic timeout value */
/*                      as zero                                              */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gEoamGlobalInfo.                                     */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
EoamRedHRProcStdyStPktReq (VOID)
{
    EOAM_TRC_FN_ENTRY ();

    if ((gEoamGlobalInfo.u1SystemCtrlStatus == EOAM_SHUTDOWN) ||
        (gEoamGlobalInfo.u1ModuleAdminStatus == EOAM_DISABLED))
    {
        EOAM_TRC ((EOAM_RED_TRC, "EOAM: Module is shutdown. Sending steady "
                   "state tail msg. \n"));
        EoamRedHRSendStdyStTailMsg ();
        return;
    }

    if (EOAM_RM_NODE_STATUS () != EOAM_ACTIVE_NODE)
    {
        EOAM_TRC ((EOAM_RED_TRC, "EOAM: Node is not active. Processing of "
                   "steady state pkt request failed.\n"));
        return;
    }
    if (EOAM_RM_HR_STATUS () == EOAM_HR_STATUS_DISABLE)
    {
        EOAM_TRC ((EOAM_RED_TRC, "EOAM:  Hitless restart is not enabled. "
                   "Processing of steady state pkt request failed.\n"));
        return;
    }

    /* This flag is set to indicate the LA timer expiry handler that steady
     * state request had received from RM */

    EOAM_HR_STDY_ST_REQ_RCVD () = OSIX_TRUE;

    /* the periodic timer is stopped and restarted with 0 seconds.
     * When restarting this timer with 0 seconds, it is immediately 
     * expires and the periodic PDU will be sent. This periodic PDU 
     * (in hitless restart case, it is called as steady state packet)
     * is hooked, and it is sent to RM. */

    /* If the global PDU Tx Timer is running */
    if (gEoamGlobalInfo.PduTxTmr.TimerNode.u2Flags == EOAM_TMR_RUNNING)
    {
        /* Stop the Timer */
        if (TmrStop (gEoamGlobalInfo.EoamTmrListId, &(gEoamGlobalInfo.PduTxTmr))
            == TMR_FAILURE)
        {
            EOAM_TRC ((OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                       "EoamRedHRProcStdyStPktReq: EOAM PDU Tx Stop Timer "
                       "FAILED!!\n"));
            return;
        }

        /* Restart the global PDU Tx Timer with duration as 0 */
        if (TmrStart (gEoamGlobalInfo.EoamTmrListId,
                      &(gEoamGlobalInfo.PduTxTmr),
                      EOAM_PDU_TX_TMR_TYPE, 0, 0) == TMR_FAILURE)
        {
            EOAM_TRC ((INIT_SHUT_TRC | EOAM_CRITICAL_TRC | ALL_FAILURE_TRC,
                       "EoamRedHRProcStdyStPktReq: EOAM PDU Tx Start Timer "
                       "FAILED!!\n"));
        }

    }
    EOAM_TRC ((EOAM_RED_TRC, "EOAM : Hitless Restart steady state packet"
               " request is processed successfully.\n"));

    EOAM_TRC_FN_EXIT ();
    return;
}

/*****************************************************************************/
/* Function Name      : EoamRedHRSendStdyStPkt                               */
/*                                                                           */
/* Description        : This function sends steady state packet to RM.       */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
INT1
EoamRedHRSendStdyStPkt (UINT1 *pu1LinBuf, UINT4 u4PktLen, UINT2 u2Port,
                        UINT4 u4TimeOut)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = 0;
    UINT4               u4MsgLen = 0;
    UINT4               u4BufSize = 0;

    EOAM_TRC_FN_ENTRY ();

    if (EOAM_RM_NODE_STATUS () != EOAM_ACTIVE_NODE)
    {
        EOAM_TRC ((EOAM_RED_TRC, "EOAM: Node is not active. Sending "
                   "steady state pkt to RM is failed.\n"));
        return OSIX_SUCCESS;
    }
    if (EOAM_RM_HR_STATUS () == EOAM_HR_STATUS_DISABLE)
    {
        EOAM_TRC ((EOAM_RED_TRC, "EOAM:  Hitless restart is not enabled. "
                   "Sending steady state pkt to RM is failed.\n"));
        return OSIX_SUCCESS;
    }

    /* Forming  steady state packet

       <- 24B --><-------  4B -- -------><-- 2B -->< 2B -><-- 4B -->< 50 B >
       ______________________________________________________________________
       |        |                         |         |      |         |        |
       | RM Hdr | EOAM_HR_STDY_ST_PKT_MSG | Msg Len | PORT | TimeOut | Buffer |
       |________|_________________________|_________|______|_________|________|

       * The RM Hdr shall be included by RM.
     */

    u4BufSize = EOAM_RM_MSG_TYPE_SIZE +    /* length of the message type */
        EOAM_RM_LEN_SIZE +        /* length of the message length */
        sizeof (UINT2) +        /* length for the port field */
        sizeof (UINT4) +        /* length for the Timeout filed */
        u4PktLen;                /* buffer length */
    /* Allocate memory for data to be sent to RM. This memory will be freed
     * by RM */
    if ((pMsg = RM_ALLOC_TX_BUF (u4BufSize)) == NULL)
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "RM Memory allocation failed\r\n"));
        return OSIX_FAILURE;
    }

    /* Fill the message type. */
    u4MsgLen = u4PktLen + sizeof (UINT4) + sizeof (UINT2);

    RM_DATA_ASSIGN_4_BYTE (pMsg, u4Offset, EOAM_HR_STDY_ST_PKT_MSG);
    u4Offset += 4;

    RM_DATA_ASSIGN_2_BYTE (pMsg, u4Offset, (UINT2) u4MsgLen);
    u4Offset += 2;

    RM_DATA_ASSIGN_2_BYTE (pMsg, u4Offset, u2Port);
    u4Offset += 2;

    RM_DATA_ASSIGN_4_BYTE (pMsg, u4Offset, u4TimeOut);
    u4Offset += 4;

    RM_COPY_TO_OFFSET (pMsg, pu1LinBuf, u4Offset, u4PktLen);
    u4Offset += u4PktLen;

    /* Fill the port information to be synced up. */
    if (OSIX_FAILURE == EoamRedSendMsgToRm (pMsg, (UINT2) u4BufSize))
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "Sending Message to RM Failed\r\n"));
    }
    EOAM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EoamRedHRSendStdyStTailMsg                           */
/*                                                                           */
/* Description        : This function is called when all the steady state    */
/*                      pkts were sent to the RM. It sends steady state tail */
/*                      message to RM module.                                */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
INT1
EoamRedHRSendStdyStTailMsg (VOID)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = EOAM_RM_OFFSET;
    UINT2               u2BufSize = 0;

    if (EOAM_RM_NODE_STATUS () != EOAM_ACTIVE_NODE)
    {
        EOAM_TRC ((EOAM_RED_TRC, "EOAM: Node is not active. Steady state "
                   "tail msg is not sent to RM.\n"));
        return OSIX_SUCCESS;
    }
    if (EOAM_RM_HR_STATUS () == EOAM_HR_STATUS_DISABLE)
    {
        EOAM_TRC ((EOAM_RED_TRC, "EOAM: Hitless restart is not enabled.Steady "
                   "state tail msg is not sent to RM.\n"));
        return OSIX_SUCCESS;
    }

    EOAM_TRC ((EOAM_RED_TRC, "EOAM: sending steady state tail msg to RM.\n"));
    u2BufSize = EOAM_RM_MSG_TYPE_SIZE + EOAM_RM_LEN_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed
     * by RM  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "Rm alloc failed\n"));
        return (OSIX_FAILURE);
    }

    /* Form a steady state tail message.

     *         <----------4 Byte---------><---2 Byte--->
     _________________________________________________
     |        |                          |             |
     | RM Hdr | EOAM_HR_STDY_ST_PKT_TAIL | Msg Length  |
     |________|__________________________|_____________|

     * The RM Hdr shall be included by RM.
     */

    /* Fill the message type. */
    RM_DATA_ASSIGN_4_BYTE (pMsg, u4Offset, EOAM_HR_STDY_ST_PKT_TAIL);
    u4Offset += 4;

    RM_DATA_ASSIGN_2_BYTE (pMsg, u4Offset, u2BufSize);
    u4Offset += 2;

    if (OSIX_FAILURE == EoamRedSendMsgToRm (pMsg, u2BufSize))
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "EOAM: steady state tail msg sending is "
                   "failed\n"));
    }
    return OSIX_SUCCESS;
}
#endif /* _EMRED_c */

/*-----------------------------------------------------------------------*/
/*                       End of the file  emred.c                        */
/*-----------------------------------------------------------------------*/
