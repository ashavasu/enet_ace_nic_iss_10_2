/************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emcltinf.c,v 1.29 2017/08/31 14:02:00 siva Exp $
 *
 * Description: This file conatains the functions related to Information
 * PDU handling by EOAM client module. 
 *************************************************************************/
#include "eminc.h"

PRIVATE VOID EoamCltInfFillUnusedData PROTO ((tEoamEventLogEntry *));
PRIVATE VOID EoamCltInfCheckLinkFault PROTO ((tEoamPortInfo *, UINT2,
                                              tEoamCallbackInfo *, UINT1 *,
                                              UINT1 *));
PRIVATE VOID EoamCltInfCheckDyingGasp PROTO ((UINT2, UINT2, tEoamCallbackInfo *,
                                              UINT1 *, UINT1 *));
PRIVATE VOID EoamCltInfCheckCriticalEvent PROTO ((UINT2, UINT2,
                                                  tEoamCallbackInfo *, UINT1 *,
                                                  UINT1 *));
PRIVATE INT4 EoamCltInfStorePeerInfo PROTO ((tEoamPeerInfo *,
                                             tEoamInfoParams *));
PRIVATE INT4 EoamCltInfVerifyRevNum PROTO ((tEoamPortInfo *, tEoamPduData *,
                                            UINT1 *));
PRIVATE VOID EoamCltInfAfterDiscNegotiation PROTO ((tEoamPortInfo *, INT4));
PRIVATE VOID EoamCltInfSendGaspOnPort PROTO ((tEoamPortInfo *));
PRIVATE VOID EoamCltInfLogandFormGasp PROTO ((tEoamPortInfo *));
PRIVATE VOID EoamCltInfLogDyingGasp PROTO ((tEoamPortInfo * pPortInfo,
                                            tEoamEventLogEntry * pLogEntry));
PRIVATE INT4 EoamNotifyEvent PROTO ((tEoamPortInfo *, tEoamCallbackInfo *,
                                     tEoamEnaPortInfo *, tEoamPduHeader *,
                                     UINT4, UINT1));
VOID                EoamCltInfTriggerLinkFaultMsg (tEoamPortInfo *, UINT1);
VOID                EoamCltInfTriggerCriticalEvtMsg (tEoamPortInfo *, UINT1);
VOID                EoamCltInfTrigDyingGaspOnAllPorts (UINT1 u1State);

#ifdef EOAM_FM_WANTED
extern INT1
 
 
 
 
nmhGetFsFmLinkFaultAction (INT4 i4IfIndex, INT4 *pi4RetValFsFmLinkFaultAction);
#endif

/*******************************************************************************
 *                                                                          
 *    FUNCTION NAME    :  EoamCltInfPduProcess 
 *                                                                          
 *    DESCRIPTION      :  This function is called on reception of an          
 *                        Information OAMPDU.                               
 *                        1)It is responsible for discovery process to       
 *                          settle down by generating appropriate Events to 
 *                          the Discovery State Event Machine.              
 *                        2)If there is any change in the config revision of 
 *                          the peer, the peer information is stored.
 *                        3)If the local DTE is waiting for loopback 
 *                          acknowledgement and receives an Info PDU, it calls 
 *                          an appropriate function to handle the same.
 *                          
 *    INPUT            :  pPortInfo - pointer to port specific info          
 *                        pHeader - pointer to PDU header                   
 *                        pData - pointer to PDU Data                       
 *                                                                          
 *    OUTPUT           :  None.                                             
 *                                                                          
 *    RETURNS          :  OSIX_SUCCESS / OSIX_FAILURE                       
 *                                                                          
*******************************************************************************/
PUBLIC INT4
EoamCltInfPduProcess (tEoamPortInfo * pPortInfo, UINT1 u1SemStateChange,
                      tEoamPduData * pData)
{
    UINT1               u1RevisionChange = OSIX_FALSE;

    EOAM_TRC_FN_ENTRY ();
    /* Increment the Information OAMPDU Rx Counter */
    pPortInfo->PduStats.u4InformationRx++;
    /* If the Information PDU carried a Link fault indication, it will 
     * not have any TLV fields. So no need to process further*/
    if (pPortInfo->RemoteInfo.u2Flags & EOAM_LINK_FAULT_BITMASK)
    {
        return OSIX_SUCCESS;
    }
    if (EoamCltInfVerifyRevNum (pPortInfo, pData, &u1RevisionChange)
        != OSIX_SUCCESS)
    {
        EOAM_TRC ((ALL_FAILURE_TRC | EOAM_CRITICAL_TRC | EOAM_DISCOVERY_TRC,
                   "CLIENT: EoamCltInfVerifyRevNum failed on port %u\n",
                   pPortInfo->u4Port));
        return OSIX_FAILURE;
    }

    if (EoamCltInfHandleDiscovery (pPortInfo, u1RevisionChange,
                                   u1SemStateChange) != OSIX_SUCCESS)
    {
        EOAM_TRC ((ALL_FAILURE_TRC | EOAM_CRITICAL_TRC | EOAM_DISCOVERY_TRC,
                   "CLIENT: EoamCltInfHandleDiscovery failed on port %u\n",
                   pPortInfo->u4Port));
        return OSIX_FAILURE;
    }

    EoamProcessOrgInfoTLV (pPortInfo, &(pData->InfoPduData.OrgSpecInfo));
    EOAM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*******************************************************************************
 *     FUNCTION NAME    :  EoamCltInfPduParseHeader 
 *                                                                           
 *     DESCRIPTION      :  This function parses the PDU header to check for 
 *                         any remote failure indications received from peer.   
 *                         In case of link fault, dying gasp or critical event
 *                         indication from the remote end, it notifies the same
 *                         to Fault Management Module. It also creates a new 
 *                         log entry and logs the events.
 *                                                                           
 *     INPUT            :  pPortInfo - pointer to port specific info         
 *                         pHeader - pointer to PDU header                   
 *                         pu1SemStateChange - pointer used to denote
 *                                             sem machine change.
 *                                                                           
 *     OUTPUT           :  None.                                             
 *                                                                           
 *     RETURNS          :  OSIX_SUCCESS / OSIX_FAILURE                       
 ******************************************************************************/
PUBLIC INT4
EoamCltInfPduParseHeader (tEoamPortInfo * pPortInfo, tEoamPduHeader * pHeader,
                          UINT1 *pu1SemStateChange)
{
    tEoamEnaPortInfo   *pActivePortInfo = NULL;
    tEoamCallbackInfo   CallbackInfo;
    UINT1               u1NotificationReqd = OSIX_FALSE;
    UINT1               u1NewLogEntry = OSIX_FALSE;

    EOAM_TRC_FN_ENTRY ();
    pActivePortInfo = pPortInfo->pEoamEnaPortInfo;
    MEMSET (&CallbackInfo, 0, sizeof (tEoamCallbackInfo));
    CallbackInfo.u4Port = pPortInfo->u4Port;
    CallbackInfo.u2Location = EOAM_REMOTE_ENTRY;
    EoamCltInfCheckLinkFault (pPortInfo, pHeader->u2Flags,
                              &CallbackInfo, &u1NewLogEntry,
                              &u1NotificationReqd);
    if (u1NotificationReqd == OSIX_TRUE)
    {
        EoamNotifyEvent (pPortInfo, &CallbackInfo, pActivePortInfo, pHeader,
                         EOAM_NOTIFY_LINK_FAULT, u1NewLogEntry);
    }

    u1NotificationReqd = OSIX_FALSE;
    u1NewLogEntry = OSIX_FALSE;
    /* For Dying Gasp and Critical events, Logging of the event and 
     * Notification to FM should be done only for the first time.*/
    EoamCltInfCheckDyingGasp (pHeader->u2Flags, pPortInfo->RemoteInfo.u2Flags,
                              &CallbackInfo, &u1NewLogEntry,
                              &u1NotificationReqd);

    if (u1NotificationReqd == OSIX_TRUE)
    {
        EoamNotifyEvent (pPortInfo, &CallbackInfo, pActivePortInfo, pHeader,
                         EOAM_NOTIFY_DYING_GASP, u1NewLogEntry);
    }

    u1NotificationReqd = OSIX_FALSE;
    u1NewLogEntry = OSIX_FALSE;
    EoamCltInfCheckCriticalEvent (pHeader->u2Flags,
                                  pPortInfo->RemoteInfo.u2Flags,
                                  &CallbackInfo, &u1NewLogEntry,
                                  &u1NotificationReqd);
    if (u1NotificationReqd == OSIX_TRUE)
    {
        EoamNotifyEvent (pPortInfo, &CallbackInfo, pActivePortInfo, pHeader,
                         EOAM_NOTIFY_CRITICAL_EVENT, u1NewLogEntry);
    }

    if ((pPortInfo->RemoteInfo.u2Flags & EOAM_LOCAL_EVAL_STABLE_BITMASK) !=
        (pHeader->u2Flags & EOAM_LOCAL_EVAL_STABLE_BITMASK))
    {
        pPortInfo->RemoteInfo.u2Flags = pHeader->u2Flags;
        EoamCtlTxConstructInfoPdu (pPortInfo);
        *pu1SemStateChange = OSIX_TRUE;
    }
    else
    {
        pPortInfo->RemoteInfo.u2Flags = pHeader->u2Flags;
    }
    MEMCPY (pPortInfo->RemoteInfo.MacAddress, pHeader->SrcMacAddr,
            MAC_ADDR_LEN);

    EOAM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*******************************************************************************
 *     FUNCTION NAME    : EoamNotifyEvent 
 *                                                                           
 *     DESCRIPTION      :  This function checkks for any remote failure indications 
 *                         received from peer.   
 *                         In case of link fault, dying gasp or critical event
 *                         indication from the remote end, it notifies the same
 *                         to Fault Management Module. It also creates a new 
 *                         log entry and logs the events.
 *                                                                           
 *     INPUT            :  pPortInfo - pointer to port specific info         
 *                         pCallbackInfo  - pointer to the Callback Info
 *                                          Structure
 *                         pActivePortInfo - Pointer to ActivePort Info 
 *                                           structure
 *                         u4EventType  - Event received in the PDU.
 *                         u1NewLogEntry - Bool to indicate should a new Event
 *                                         be logged.
 *                                                                           
 *     OUTPUT           :  None.                                             
 *                                                                           
 *     RETURNS          :  OSIX_SUCCESS / OSIX_FAILURE                       
 ******************************************************************************/

PRIVATE INT4
EoamNotifyEvent (tEoamPortInfo * pPortInfo, tEoamCallbackInfo * pCallbackInfo,
                 tEoamEnaPortInfo * pActivePortInfo, tEoamPduHeader * pHeader,
                 UINT4 u4EventType, UINT1 u1NewLogEntry)
{
    tEoamEventLogEntry *pLogEntry = NULL;
    tOsixSysTime        CurrentTime;

    MEMSET (&CurrentTime, 0, sizeof (tOsixSysTime));
    /* Get the System Time stamp when a Critical link Event is received */
    if ((u1NewLogEntry == OSIX_TRUE) && (pPortInfo != NULL)
        && (pActivePortInfo != NULL))
    {
#ifdef EOAM_FM_WANTED
        if (EoamGetActionFromFm (pPortInfo->u4Port, u4EventType) ==
            EOAM_ACTION_NONE)
        {
            EOAM_TRC ((EOAM_RFI_TRC, "This event cannot be processed"
                       "Due to the Action type as None"));
            return OSIX_SUCCESS;
        }
        else
        {
#endif
            /* Get the pointer to the log entry and update with the event 
             * information */
            pLogEntry = EoamUtilGetLogEntryToBeFilled (pPortInfo);

            if (pLogEntry == NULL)
            {
                return OSIX_FAILURE;
            }

            OsixGetSysTime (&CurrentTime);
            MEMCPY (pLogEntry->au1EventLogOui, gaTlvOui, EOAM_OUI_LENGTH);

            switch (u4EventType)
            {
                case EOAM_NOTIFY_LINK_FAULT:
                    pLogEntry->u4EventLogType = EOAM_LINK_FAULT;
                    break;
                case EOAM_NOTIFY_DYING_GASP:
                    pLogEntry->u4EventLogType = EOAM_DYING_GASP;
                    break;
                case EOAM_NOTIFY_CRITICAL_EVENT:
                    pLogEntry->u4EventLogType = EOAM_CRITICAL_EVENT;
                    break;
                default:
                    EOAM_TRC ((EOAM_RFI_TRC, "CLIENT: Invalid Event Type on "
                               "port %u\n ", pPortInfo->u4Port));
            }

            /* Log the Events since the Action type as Log */
            pLogEntry->u4EventLogIndex = ++(pActivePortInfo->u4LogIndexCount);
            pLogEntry->EventLogInfo.u4EventLogTimestamp = CurrentTime;
            pLogEntry->u1EventLogLocation = EOAM_REMOTE_ENTRY;
            pLogEntry->u4NonThresLogEvtTotal = ++(pActivePortInfo->
                                                  u4NonThresLogEvtTotalRx);
            EoamCltInfFillUnusedData (pLogEntry);
            EoamNotifyEventTrap (pLogEntry,
                                 pPortInfo->u4Port,
                                 EOAM_NOTIFY_NON_THRESH_EVENT);
#if EOAM_FM_WANTED
        }
#endif
    }
    /* Call function to notify registered modules */
    if (pCallbackInfo != NULL)
    {
        EoamMainNotifyModules (pCallbackInfo);
    }

    EOAM_TRC_FN_EXIT ();
    UNUSED_PARAM (pHeader);
    return OSIX_SUCCESS;
}

/*******************************************************************************
 *                                                                          
 *    FUNCTION NAME    :  EoamCltInfFillUnusedData 
 *                                                                          
 *    DESCRIPTION      :  This function fills up the unused fields in the log
 *                        entry.
 *                          
 *    INPUT            :  pLogEntry - pointer to log entry          
 *                                                                          
 *    OUTPUT           :  None.                                             
 *                                                                          
 *    RETURNS          :  None. 
 *                                                                          
 ******************************************************************************/
PRIVATE VOID
EoamCltInfFillUnusedData (tEoamEventLogEntry * pLogEntry)
{
    EOAM_TRC_FN_ENTRY ();
    /* For all non-threshold crossing events, the unused fields in
     * the log structure should be filled with 0xffff */
    EOAM_FILL_UNUSED_8BYTE_DATA (pLogEntry->EventLogInfo.u8EventWindow);
    EOAM_FILL_UNUSED_8BYTE_DATA (pLogEntry->EventLogInfo.u8EventThreshold);
    EOAM_FILL_UNUSED_8BYTE_DATA (pLogEntry->EventLogInfo.u8EventValue);
    EOAM_FILL_UNUSED_8BYTE_DATA (pLogEntry->EventLogInfo.u8ErrorRunningTotal);
    pLogEntry->EventLogInfo.u4EventRunningTotal = EOAM_INVALID_VAL;
    EOAM_TRC_FN_EXIT ();
    return;
}

/*******************************************************************************
 *     FUNCTION NAME    :  EoamCltInfCheckLinkFault 
 *                                                                           
 *     DESCRIPTION      :  This function compares the flags of the received 
 *                         INFO PDU and the remote peer's stored flags
 *                         and decides on whether notification and logging
 *                         is required or not.
 *                                                                           
 *     INPUT            :  pPortInfo - pointer to port's information
 *                         u2IncomingFlag - Flag field from the received PDU
 *
 *     OUTPUT           :  pCallbackInfo  - pointer to the Callback Info 
 *                                          Structure
 *                         pu1NewLogEntry - pointer to denote whether New log 
 *                                          entry is required or not
 *                         pu1NotifReqd   - pointer to denote whether 
 *                                          notification is required or not 
 *                                                                           
 *     RETURNS          :  None. 
 ******************************************************************************/
PRIVATE VOID
EoamCltInfCheckLinkFault (tEoamPortInfo * pPortInfo, UINT2 u2IncomingFlag,
                          tEoamCallbackInfo * pCallbackInfo,
                          UINT1 *pu1NewLogEntry, UINT1 *pu1NotifReqd)
{
    EOAM_TRC_FN_ENTRY ();
    /* If Remote Link Fault is indicated in the OAMPDU, Notification to Fault 
     * Management module and Logging of this event should be done only for 
     * the first time.*/
    /* Logging should not be performed for recovery from error status */
    if (u2IncomingFlag & EOAM_LINK_FAULT_BITMASK)
    {
        if (!(pPortInfo->RemoteInfo.u2Flags & EOAM_LINK_FAULT_BITMASK))
        {
            *pu1NewLogEntry = OSIX_TRUE;
            *pu1NotifReqd = OSIX_TRUE;
            pCallbackInfo->u4EventType = EOAM_NOTIFY_LINK_FAULT;
            pCallbackInfo->EventState = OSIX_TRUE;

        }
    }
    else
    {
        if (pPortInfo->RemoteInfo.u2Flags & EOAM_LINK_FAULT_BITMASK)
        {
            *pu1NotifReqd = OSIX_TRUE;
            pCallbackInfo->u4EventType = EOAM_NOTIFY_LINK_FAULT;
            pCallbackInfo->uEventInfo.u1State = OSIX_FALSE;
        }
    }
    EOAM_TRC_FN_EXIT ();
    return;
}

/*******************************************************************************
 *     FUNCTION NAME    :  EoamCltInfCheckDyingGasp
 *                                                                           
 *     DESCRIPTION      :  This function compares the flags of the received 
 *                         INFO PDU and the remote peer's stored flags
 *                         and decides on whether notification and logging
 *                         is required or not.
 *                                                                           
 *     INPUT            :  u2IncomingFlag - Flag field from the received PDU
 *                         u2RemoteFlag   - Flag field from previously received
 *                                          PDU
 *
 *     OUTPUT           :  pCallbackInfo  - pointer to the Callback Info 
 *                                          Structure
 *                         pu1NewLogEntry - pointer to denote whether New log 
 *                                          entry is required or not
 *                         pu1NotifReqd   - pointer to denote whether 
 *                                          notification is required or not 
 *                                                                           
 *     RETURNS          :  None. 
 ******************************************************************************/
PRIVATE VOID
EoamCltInfCheckDyingGasp (UINT2 u2IncomingFlag, UINT2 u2RemoteFlag,
                          tEoamCallbackInfo * pCallbackInfo,
                          UINT1 *pu1NewLogEntry, UINT1 *pu1NotifReqd)
{
    EOAM_TRC_FN_ENTRY ();
    if (u2IncomingFlag & EOAM_DYING_GASP_BITMASK)
    {
        if (!(u2RemoteFlag & EOAM_DYING_GASP_BITMASK))
        {
            EOAM_TRC ((EOAM_RFI_TRC | EOAM_CRITICAL_TRC,
                       "CLIENT: Dying gasp indication for port %u\n ",
                       pCallbackInfo->u4Port));
            *pu1NewLogEntry = OSIX_TRUE;
            *pu1NotifReqd = OSIX_TRUE;
            pCallbackInfo->u4EventType = EOAM_NOTIFY_DYING_GASP;
            pCallbackInfo->EventState = OSIX_TRUE;
        }
    }
    else
    {
        if (u2RemoteFlag & EOAM_DYING_GASP_BITMASK)
        {
            EOAM_TRC ((EOAM_RFI_TRC | EOAM_CRITICAL_TRC,
                       "CLIENT: Dying gasp rectified indication for "
                       "port %u\n ", pCallbackInfo->u4Port));
            *pu1NotifReqd = OSIX_TRUE;
            pCallbackInfo->u4EventType = EOAM_NOTIFY_DYING_GASP;
            pCallbackInfo->uEventInfo.u1State = OSIX_FALSE;
        }
    }
    EOAM_TRC_FN_EXIT ();
    return;
}

/*******************************************************************************
 *     FUNCTION NAME    :  EoamCltInfCheckCriticalEvent
 *                                                                           
 *     DESCRIPTION      :  This function compares the flags of the received 
 *                         INFO PDU and the remote peer's stored flags
 *                         and decides on whether notification and logging
 *                         is required or not.
 *                                                                           
 *     INPUT            :  u2IncomingFlag - Flag field from the received PDU
 *                         u2RemoteFlag   - Flag field from previously received
 *                                          PDU
 *                         
 *     OUTPUT           :  pCallbackInfo  - pointer to the Callback Info 
 *                                          Structure
 *                         pu1NewLogEntry - pointer to denote whether New log 
 *                                          entry is required or not
 *                         pu1NotifReqd   - pointer to denote whether 
 *                                          notification is required or not 
 *                                                                           
 *     RETURNS          :  None. 
 ******************************************************************************/
PRIVATE VOID
EoamCltInfCheckCriticalEvent (UINT2 u2IncomingFlag, UINT2 u2RemoteFlag,
                              tEoamCallbackInfo * pCallbackInfo,
                              UINT1 *pu1NewLogEntry, UINT1 *pu1NotifReqd)
{
    EOAM_TRC_FN_ENTRY ();
    if (u2IncomingFlag & EOAM_CRITICAL_EVENT_BITMASK)
    {
        if (!(u2RemoteFlag & EOAM_CRITICAL_EVENT_BITMASK))
        {
            EOAM_TRC ((EOAM_RFI_TRC | EOAM_CRITICAL_TRC,
                       "CLIENT: Critical Event indication for port %u\n ",
                       pCallbackInfo->u4Port));
            *pu1NewLogEntry = OSIX_TRUE;
            *pu1NotifReqd = OSIX_TRUE;
            pCallbackInfo->u4EventType = EOAM_NOTIFY_CRITICAL_EVENT;
            pCallbackInfo->EventState = OSIX_TRUE;
        }
    }
    else
    {
        if (u2RemoteFlag & EOAM_CRITICAL_EVENT_BITMASK)
        {
            EOAM_TRC ((EOAM_RFI_TRC | EOAM_CRITICAL_TRC,
                       "CLIENT: Critical Event rectified indication for "
                       "port %u\n ", pCallbackInfo->u4Port));
            *pu1NotifReqd = OSIX_TRUE;
            pCallbackInfo->u4EventType = EOAM_NOTIFY_CRITICAL_EVENT;
            pCallbackInfo->uEventInfo.u1State = OSIX_FALSE;
        }
    }
    EOAM_TRC_FN_EXIT ();
    return;
}

/*******************************************************************************
 *                                                                          
 *    FUNCTION NAME    :  EoamCltInfStorePeerInfo
 *                                                                          
 *    DESCRIPTION      :  This function copies the remote peer's information 
 *                        the log entry if not allocated before and updates 
 *                        the log entry information.                        
 *                                                                          
 *    INPUT            :  pPeerInfo - pointer to peer information  
 *                        pTLVInfo - pointer to TLV information from peer
 *                                                                          
 *    OUTPUT           :  None.                                             
 *                                                                          
 *    RETURNS          :  OSIX_SUCCESS / OSIX_FAILURE                       
 *                                                                          
*******************************************************************************/
PRIVATE INT4
EoamCltInfStorePeerInfo (tEoamPeerInfo * pPeerInfo, tEoamInfoParams * pTLVInfo)
{
    EOAM_TRC_FN_ENTRY ();
    if (EOAM_IS_VALID_OAM_VERSION (pTLVInfo->u1OamVer) == OSIX_FALSE)
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "CLIENT: Local Info TLV received with"
                   "wrong OAM Version "));
        return OSIX_FAILURE;
    }
    MEMCPY (pPeerInfo->au1VendorOui, pTLVInfo->au1OUI, EOAM_OUI_LENGTH);
    pPeerInfo->u4VendorInfo = pTLVInfo->u4VendorInfo;
    pPeerInfo->u2ConfigRevision = pTLVInfo->u2Revision;
    pPeerInfo->u1State = (UINT1) (pTLVInfo->u1State & EOAM_MUX_STATE_BITMASK);
    /* Ignore when parser state = 0x03 since it is invalid */
    if ((pTLVInfo->u1State & EOAM_PAR_STATE_BITMASK) < EOAM_PAR_STATE_BITMASK)
    {
        pPeerInfo->u1State |= (pTLVInfo->u1State & EOAM_PAR_STATE_BITMASK);
    }
    if (pTLVInfo->u1OamConfig & EOAM_MODE_BITMASK)
    {
        pPeerInfo->u1Mode = EOAM_MODE_ACTIVE;
    }
    else
    {
        pPeerInfo->u1Mode = EOAM_MODE_PASSIVE;
    }
    pPeerInfo->u1FnsSupportedBmp =
        (UINT1) ((pTLVInfo->u1OamConfig >> 1) & EOAM_FNS_SUPPORTED_BITMASK);
    pPeerInfo->u2MaxOamPduSize =
        (UINT2) (pTLVInfo->u2OamPduConfig & EOAM_PDU_SIZE_BITMASK);
    EOAM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*******************************************************************************
 *                                                                          
 *    FUNCTION NAME    :  EoamCltInfVerifyRevNum 
 *                                                                          
 *    DESCRIPTION      :  This function verifies the revision number of the 
 *                        received Info TLV and stores the remote information
 *                        if applicable.
 *                                                                          
 *    INPUT            :  pPortInfo - pointer to port information  
 *                        pData - pointer to received TLVs 
 *                                                                          
 *    OUTPUT           :  pu1RevisionChange - pointer to denote whether there
 *                        is a revision change or not.
 *                                                                          
 *    RETURNS          :  OSIX_SUCCESS / OSIX_FAILURE                       
 *                                                                          
*******************************************************************************/
PRIVATE INT4
EoamCltInfVerifyRevNum (tEoamPortInfo * pPortInfo, tEoamPduData * pData,
                        UINT1 *pu1RevisionChange)
{
    tEoamCtlReq        *pCtrlReq = NULL;

    EOAM_TRC_FN_ENTRY ();
    pCtrlReq = &(pPortInfo->pEoamEnaPortInfo->CtrlReq);
    /* Store contents of peer's information TLV when there is a revision change
     * (may be a wrap-around condition) or when peer info is got for the first 
     * time */
    if ((pCtrlReq->b1RemoteStateValid == OSIX_FALSE) ||
        ((pData->InfoPduData.LocalInfo.u2Revision) !=
         (pPortInfo->RemoteInfo.u2ConfigRevision)))
    {
        *pu1RevisionChange = OSIX_TRUE;

        if (pPortInfo->RemoteInfo.u2MaxOamPduSize !=
            (UINT2) (pData->InfoPduData.LocalInfo.u2OamPduConfig &
                     EOAM_PDU_SIZE_BITMASK))
        {
            if (pPortInfo->u4Mtu > EOAM_MAX_DATA_SIZE)
            {
                pPortInfo->LocalInfo.u2MaxOamPduSize = EOAM_MAX_PDU_SIZE;
            }
            else
            {
                pPortInfo->LocalInfo.u2MaxOamPduSize =
                    (UINT2) (pPortInfo->u4Mtu + CFA_ENET_V2_HEADER_SIZE +
                             EOAM_ETH_FCS_LEN);
            }
            /* Update the Information OAMPDU structure */
            EoamCtlTxConstructInfoPdu (pPortInfo);
        }

        if (EoamCltInfStorePeerInfo (&(pPortInfo->RemoteInfo),
                                     &(pData->InfoPduData.LocalInfo)) !=
            OSIX_SUCCESS)
        {
            EOAM_TRC ((ALL_FAILURE_TRC | EOAM_DISCOVERY_TRC |
                       EOAM_CRITICAL_TRC, "CLIENT: Invalid Local Info TLV"
                       "received on port %u\n ", pPortInfo->u4Port));
            return OSIX_FAILURE;
        }
    }
    EOAM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*******************************************************************************
 *                                                                          
 *    FUNCTION NAME    :  EoamCltInfHandleDiscovery 
 *                                                                          
 *    DESCRIPTION      :  This function handles discovery functionalities 
 *                        and calls the Discovery state event machine when
 *                        required. It also calls the negotiation fuction for
 *                        discovery.
 *                                                                          
 *    INPUT            :  pPortInfo - pointer to port information  
 *                        u1RevisionChange - denotes whether there is a 
 *                                           revision change or not.
 *                        u1SemStateChange - denotes whether there is a
 *                                           sem machine change or not.
 *                                                                          
 *    OUTPUT           :  None. 
 *                                                                          
 *    RETURNS          :  OSIX_SUCCESS / OSIX_FAILURE                       
 *                                                                          
*******************************************************************************/
PUBLIC INT4
EoamCltInfHandleDiscovery (tEoamPortInfo * pPortInfo, UINT1 u1RevisionChange,
                           UINT1 u1SemStateChange)
{
    tEoamCtlReq        *pCtrlReq = NULL;
    INT4                i4RetVal = OSIX_FAILURE;

    EOAM_TRC_FN_ENTRY ();
    pCtrlReq = &(pPortInfo->pEoamEnaPortInfo->CtrlReq);
    switch (pPortInfo->LocalInfo.u2EoamOperStatus)
    {
        case EOAM_OPER_OPERATIONAL:
            if ((u1RevisionChange == OSIX_FALSE) &&
                (u1SemStateChange == OSIX_FALSE))
            {
                /* There is no revision change. 
                 * So no need to proceed any further*/
                i4RetVal = OSIX_SUCCESS;
                break;
            }
            if ((pPortInfo->LoopbackInfo.u1Status == EOAM_INITIATING_LOOPBACK)
                || (pPortInfo->LoopbackInfo.u1Status ==
                    EOAM_TERMINATING_LOOPBACK)
                || (pPortInfo->LoopbackInfo.u1Status == EOAM_REMOTE_LOOPBACK))
            {
                if (EoamCltLbHandleAck (pPortInfo) == OSIX_SUCCESS)
                {
                    i4RetVal = OSIX_SUCCESS;
                }
            }
            /*While in local loopback, if PDU is received with MUX state
             * and PARSER state as FWD, switch to NO_LOOPBACK mode */
            else if (pPortInfo->LoopbackInfo.u1Status == EOAM_LOCAL_LOOPBACK)
            {
                if (((pPortInfo->RemoteInfo.u1State & EOAM_MUX_STATE_BITMASK)
                     == EOAM_MUX_FWD_STATE) &&
                    ((pPortInfo->RemoteInfo.u1State & EOAM_PAR_STATE_BITMASK)
                     == EOAM_PAR_FWD_STATE))
                {
                    pPortInfo->LoopbackInfo.u1Status = EOAM_NO_LOOPBACK;
                    EoamMuxParSet (pPortInfo, EOAM_NO_LOOPBACK);
                    /* Update Revision number */
                    pPortInfo->LocalInfo.u2ConfigRevision++;
#ifdef NPAPI_WANTED
                    if (EoamEoamNpHandleLocalLoopBack
                        (pPortInfo->u4Port, NULL, NULL, EOAM_NP_LB_DISABLE) !=
                        FNP_SUCCESS)
                    {
                        EOAM_TRC ((ALL_FAILURE_TRC | EOAM_LOOPBACK_TRC,
                                   "CLIENT: EoamEoamNpHandleLocalLoopBack failed"
                                   "on port %u\n ", pPortInfo->u4Port));
                        return OSIX_FAILURE;
                    }
#endif
                    i4RetVal = OSIX_SUCCESS;
                }
            }

            if (i4RetVal == OSIX_SUCCESS)
            {
                /* Update the Information OAMPDU structure with change in
                 * remote info */
                EoamCtlTxConstructInfoPdu (pPortInfo);
            }
            break;

        case EOAM_OPER_PASSIVEWAIT:
            /* Intentional fallthrough */
        case EOAM_OPER_ACTIVE_SENDLOCAL:
            /* Received peer information for the first time */
            /* Call Discovery SEM notifying reception of peer's information */
            pCtrlReq->b1RemoteStateValid = OSIX_TRUE;
            EoamSemMachine (pPortInfo, EOAM_DISC_EVENT_REMOTE_STATE_VALID);
            i4RetVal = OSIX_SUCCESS;
            break;
        default:
            break;
    }
    if (i4RetVal != OSIX_SUCCESS)
    {
        i4RetVal = EoamNegotiateDiscovery (pPortInfo);
        EoamCltInfAfterDiscNegotiation (pPortInfo, i4RetVal);
        i4RetVal = OSIX_SUCCESS;
    }
    EOAM_TRC_FN_EXIT ();
    return i4RetVal;
}

/*******************************************************************************
 *                                                                          
 *    FUNCTION NAME    :  EoamCltInfAfterDiscNegotiation 
 *                                                                          
 *    DESCRIPTION      :  This function is called with the result of discovery
 *                        negotiation. Based on the result, appropriate events
 *                        are sent to the Discovery state event machine.
 *                                                                          
 *    INPUT            :  pPortInfo - pointer to port information  
 *                        i4RetVal - result of the discovery negotiation
 *                                                                          
 *    OUTPUT           :  None. 
 *                                                                          
 *    RETURNS          :  OSIX_SUCCESS / OSIX_FAILURE                       
 *                                                                          
*******************************************************************************/
PRIVATE VOID
EoamCltInfAfterDiscNegotiation (tEoamPortInfo * pPortInfo, INT4 i4RetVal)
{
    tEoamCtlReq        *pCtrlReq = NULL;

    EOAM_TRC_FN_ENTRY ();
    pCtrlReq = &(pPortInfo->pEoamEnaPortInfo->CtrlReq);
    switch (pPortInfo->LocalInfo.u2EoamOperStatus)
    {
        case EOAM_OPER_OPERATIONAL:
            if (i4RetVal == OSIX_SUCCESS)
            {
                if ((EOAM_CHECK_REMOTE_UNSTABLE (pPortInfo->RemoteInfo.u2Flags)
                     == OSIX_TRUE)
                    || (EOAM_CHECK_REMOTE_NOT_COMPLETED
                        (pPortInfo->RemoteInfo.u2Flags) == OSIX_TRUE))
                {
                    pCtrlReq->b1RemoteStable = OSIX_FALSE;
                    EoamSemMachine (pPortInfo, EOAM_DISC_EVENT_REMOTE_UNSTABLE);
                }
                else
                {
                    /* Update the pre-formed INFO PDU when there is a revision
                       change and negotiation successful */
                    EoamCtlTxConstructInfoPdu (pPortInfo);
                }
            }
            else
            {
                pCtrlReq->b1LocalSatisfied = OSIX_FALSE;
                EoamSemMachine (pPortInfo, EOAM_DISC_EVENT_LOCAL_UNSATISFIED);
            }
            break;
        case EOAM_OPER_SENDLOCALREMOTE:
            /* Intentional fallthrough */
        case EOAM_OPER_PEERINGLOCALLYREJECTED:
            if (i4RetVal == OSIX_SUCCESS)
            {
                pCtrlReq->b1LocalSatisfied = OSIX_TRUE;
                EoamSemMachine (pPortInfo, EOAM_DISC_EVENT_LOCAL_SATISFIED);
            }
            break;
        case EOAM_OPER_SENDLOCALREMOTEOK:
        case EOAM_OPER_PEERINGREMOTELYREJECTED:
            if (i4RetVal == OSIX_SUCCESS)
            {
                if (EOAM_CHECK_REMOTE_STABLE (pPortInfo->RemoteInfo.u2Flags) ==
                    OSIX_TRUE)
                {
                    pCtrlReq->b1RemoteStable = OSIX_TRUE;
                    EoamSemMachine (pPortInfo, EOAM_DISC_EVENT_REMOTE_STABLE);
                }
            }
            else
            {
                pCtrlReq->b1LocalSatisfied = OSIX_FALSE;
                EoamSemMachine (pPortInfo, EOAM_DISC_EVENT_LOCAL_UNSATISFIED);
            }
            break;
        default:
            break;
    }
    EOAM_TRC_FN_EXIT ();
    return;
}

/*******************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamCltInfSndCriticalLnkEvt2Peer 
 *                                                                          
 *    DESCRIPTION      : This function is called when a Critical link event  
 *                       is detected by Fault Management module. It sets    
 *                       the appropriate flag fields, logs the events in    
 *                       the local database and sends OAMPDU requests to    
 *                       the Control Module.                                
 *                                                                          
 *    INPUT            : pPortInfo - pointer to port specific information   
 *                       u4MessageType - Type of Critical Link Event        
 *                       u1State - State denoting whether the event has
 *                                 occured or has been rectified
 *                                                                          
 *    OUTPUT           : None.                                              
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE                        
 *                                                                          
*******************************************************************************/
PUBLIC INT4
EoamCltInfSndCriticalLnkEvt2Peer (tEoamPortInfo * pPortInfo,
                                  UINT4 u4MessageType, UINT1 u1State)
{

    EOAM_TRC_FN_ENTRY ();

    if (gEoamGlobalInfo.u1ModuleStatus != EOAM_ENABLED)
    {
        EOAM_TRC ((ALL_FAILURE_TRC | EOAM_RFI_TRC,
                   "EOAM is globally DISABLED!!!\n"));
        return OSIX_FAILURE;
    }

    if (pPortInfo == NULL)
    {
        return OSIX_SUCCESS;
    }
    switch (u4MessageType)
    {
        case EOAM_LINK_FAULT_TRIG_MSG:
            EoamCltInfTriggerLinkFaultMsg (pPortInfo, u1State);
            break;
        case EOAM_DYING_GASP_TRIG_MSG:
            EoamCltInfTrigDyingGaspOnAllPorts (u1State);
            break;
        case EOAM_CRITICAL_EVENT_TRIG_MSG:
            EoamCltInfTriggerCriticalEvtMsg (pPortInfo, u1State);
            break;
        default:
            EOAM_TRC ((ALL_FAILURE_TRC, "CLIENT: Received unknown Event"
                       "on port %u\n", pPortInfo->u4Port));
            return OSIX_FAILURE;
    }
    EOAM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*******************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamCltInfTriggerLinkFaultMsg 
 *                                                                          
 *    DESCRIPTION      : This function triggers a link fault indication message
 *                       to be sent to the peer.
 *                                                                          
 *    INPUT            : pPortInfo - pointer to port specific information   
 *                       pLogEntry - pointer to the log entry
 *                       u1State - State denoting whether the event has
 *                                 occured or has been rectified
 *                                                                          
 *    OUTPUT           : None.                                              
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE                        
 *                                                                          
*******************************************************************************/
VOID
EoamCltInfTriggerLinkFaultMsg (tEoamPortInfo * pPortInfo, UINT1 u1State)
{
    tEoamEnaPortInfo   *pActivePortInfo = NULL;
    tOsixSysTime        CurrentTime;
    EOAM_TRC_FN_ENTRY ();

    /* Get the System Time stamp when a Critical link Event is received */
    MEMSET (&CurrentTime, 0, sizeof (tOsixSysTime));
    OsixGetSysTime (&CurrentTime);
    pActivePortInfo = pPortInfo->pEoamEnaPortInfo;
    if (pActivePortInfo == NULL)
    {
        return;
    }
    if (u1State == OSIX_TRUE)
    {
        pActivePortInfo->CtrlReq.u1LocalLinkStatus = EOAM_LINK_STATUS_FAIL;
#ifdef EOAM_FM_WANTED
        INT4                i4RetValFsFmLinkFaultAction = 0;
        nmhGetFsFmLinkFaultAction (pPortInfo->u4Port,
                                   &i4RetValFsFmLinkFaultAction);
        if (!(i4RetValFsFmLinkFaultAction == EOAM_ACTION_NONE))
#endif
        {
            tEoamEventLogEntry *pLogEntry = NULL;

            pLogEntry = EoamUtilGetLogEntryToBeFilled (pPortInfo);
            if (pLogEntry == NULL)
            {
                return;
            }
            pLogEntry->u4EventLogType = EOAM_LINK_FAULT;
            MEMCPY (pLogEntry->au1EventLogOui, gaTlvOui, EOAM_OUI_LENGTH);
            pLogEntry->u4EventLogIndex = ++(pActivePortInfo->u4LogIndexCount);
            pLogEntry->EventLogInfo.u4EventLogTimestamp = CurrentTime;
            pLogEntry->u1EventLogLocation = EOAM_LOCAL_ENTRY;
            pLogEntry->u4NonThresLogEvtTotal = ++(pActivePortInfo->
                                                  u4NonThresLogEvtTotalTx);
            EoamNotifyEventTrap (pLogEntry,
                                 pPortInfo->u4Port,
                                 EOAM_NOTIFY_NON_THRESH_EVENT);
            EoamCltInfFillUnusedData (pLogEntry);
        }
    }
    else
    {
        pActivePortInfo->CtrlReq.u1LocalLinkStatus = EOAM_LINK_STATUS_OK;
    }
    EoamSetIfInfo (IF_EOAM_LINK_FAULT, pPortInfo->u4Port, u1State);
    EoamSemMachine (pPortInfo, EOAM_DISC_EVENT_LINK_STATUS);
    EOAM_TRC_FN_EXIT ();
    return;
}

/*******************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamCltInfTrigDyingGaspOnAllPorts 
 *                                                                          
 *    DESCRIPTION      : This function scans all EOAM enabled entries and sends
 *                       notifications to the peer.
 *                                                                          
 *    INPUT            : u1State - State denoting whether the event has
 *                                 occured or has been rectified
 *                                                                          
 *    OUTPUT           : None.                                              
 *                                                                          
 *    RETURNS          : None. 
 *                                                                          
*******************************************************************************/
VOID
EoamCltInfTrigDyingGaspOnAllPorts (UINT1 u1State)
{
    tTMO_DLL_NODE      *pEnaNode = NULL;
    tEoamPortInfo      *pPortInfo = NULL;
    UINT2               u2Count = 0;

    EOAM_TRC_FN_ENTRY ();

    if (u1State == OSIX_TRUE)
    {
        for (u2Count = 0; u2Count < EOAM_CRITICAL_EVT_RESEND_CNT; u2Count++)
        {
            /* Scan the EOAM enabled ports in the DLL and send dying gasp 
             * notification on all EOAM enabled ports. */
            TMO_DLL_Scan (&(gEoamGlobalInfo.EoamEnaPortDllList),
                          pEnaNode, tTMO_DLL_NODE *)
            {
                pPortInfo = (tEoamPortInfo *) pEnaNode;
                FSAP_ASSERT (pPortInfo != NULL);

                EoamCltInfLogandFormGasp (pPortInfo);

                /* Sync local status with Standby Node */
                EoamRedSyncUpDynamicInfo (pPortInfo);

                EoamCltInfSendGaspOnPort (pPortInfo);
            }
        }
    }
    else
    {
        /* Scan the EOAM enabled ports in the DLL and send dying gasp 
         * rectified notification on all EOAM enabled ports. */
        TMO_DLL_Scan (&(gEoamGlobalInfo.EoamEnaPortDllList),
                      pEnaNode, tTMO_DLL_NODE *)
        {
            pPortInfo = (tEoamPortInfo *) pEnaNode;
            FSAP_ASSERT (pPortInfo != NULL);
            pPortInfo->pEoamEnaPortInfo->CtrlReq.b1LocalDyingGasp = OSIX_FALSE;
            /* Construct info pdu to indicate the event */
            EoamCtlTxConstructInfoPdu (pPortInfo);

            /* Sync local status with Standby Node */
            EoamRedSyncUpDynamicInfo (pPortInfo);
        }
    }

    EOAM_TRC_FN_EXIT ();
    return;
}

/*******************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamCltInfSendGaspOnPort
 *                                                                          
 *    DESCRIPTION      : This function checks the whether dying gasp indication
 *                       is enabled on a port and if so, sends an information
 *                       OAMPDU to indicate it to the peer
 *                                                                          
 *    INPUT            : pPortInfo - pointer to port specific information   
 *                                                                          
 *    OUTPUT           : None.                                              
 *                                                                          
 *    RETURNS          : None. 
 *                                                                          
*******************************************************************************/
PRIVATE VOID
EoamCltInfSendGaspOnPort (tEoamPortInfo * pPortInfo)
{
    if (pPortInfo->ErrEventConfigInfo.u1DyingGaspEnable == EOAM_TRUE)
    {
        if (EoamCtlTxSendInfoPdu (pPortInfo) != OSIX_SUCCESS)
        {
            EOAM_TRC ((ALL_FAILURE_TRC | EOAM_CRITICAL_TRC | EOAM_RFI_TRC,
                       "CLIENT: EoamCtlTxSendInfoPdu failed on port %u\n",
                       pPortInfo->u4Port));
        }
    }
    return;
}

/*******************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamCltInfLogandFormGasp 
 *                                                                          
 *    DESCRIPTION      : This function logs the dying gasp indication and 
 *                       constructs the information OAMPDU to indicate it to 
 *                       peer
 *                                                                          
 *    INPUT            : pPortInfo - pointer to port specific information   
 *                                                                          
 *    OUTPUT           : None.                                              
 *                                                                          
 *    RETURNS          : None. 
 *                                                                          
*******************************************************************************/
PRIVATE VOID
EoamCltInfLogandFormGasp (tEoamPortInfo * pPortInfo)
{
    tEoamEventLogEntry *pLogEntry = NULL;
    /* Get the pointer to the log entry and update with the 
     * event information */
    pLogEntry = EoamUtilGetLogEntryToBeFilled (pPortInfo);
    if (pLogEntry == NULL)
    {
        EOAM_TRC ((ALL_FAILURE_TRC | EOAM_RFI_TRC, "CLIENT: Unable to "
                   "log local dying gasp event on port %u\n",
                   pPortInfo->u4Port));
    }
    else
    {
        EoamCltInfLogDyingGasp (pPortInfo, pLogEntry);
    }

    if (pPortInfo->ErrEventConfigInfo.u1DyingGaspEnable == EOAM_TRUE)
    {
        pPortInfo->pEoamEnaPortInfo->CtrlReq.b1LocalDyingGasp = OSIX_TRUE;
        /* Construct info pdu to indicate the event */
        EoamCtlTxConstructInfoPdu (pPortInfo);
    }
    else
    {
        EOAM_TRC ((ALL_FAILURE_TRC | EOAM_RFI_TRC, "CLIENT:Local Entity"
                   "not set to intimate dying gasp to peer on port %u\n",
                   pPortInfo->u4Port));
    }
    return;
}

/*******************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamCltInfLogDyingGasp 
 *                                                                          
 *    DESCRIPTION      : This function logs the dying gasp indication.
 *                                                                          
 *    INPUT            : pPortInfo - pointer to port specific information   
 *                       pLogEntry - pointer to the log entry
 *                                                                          
 *    OUTPUT           : None.                                              
 *                                                                          
 *    RETURNS          : None. 
 *                                                                          
*******************************************************************************/
PRIVATE VOID
EoamCltInfLogDyingGasp (tEoamPortInfo * pPortInfo,
                        tEoamEventLogEntry * pLogEntry)
{
    tEoamEnaPortInfo   *pActivePortInfo = NULL;
    tOsixSysTime        CurrentTime;

    EOAM_TRC_FN_ENTRY ();
    /* Get the System Time stamp when a Critical link Event is received */
    MEMSET (&CurrentTime, 0, sizeof (tOsixSysTime));
    OsixGetSysTime (&CurrentTime);
    pActivePortInfo = pPortInfo->pEoamEnaPortInfo;
    pLogEntry->u4EventLogType = EOAM_DYING_GASP;
    MEMCPY (pLogEntry->au1EventLogOui, gaTlvOui, EOAM_OUI_LENGTH);
    pLogEntry->u4EventLogIndex = ++(pActivePortInfo->u4LogIndexCount);
    pLogEntry->EventLogInfo.u4EventLogTimestamp = CurrentTime;
    pLogEntry->u1EventLogLocation = EOAM_LOCAL_ENTRY;
    pLogEntry->u4NonThresLogEvtTotal =
        ++(pActivePortInfo->u4NonThresLogEvtTotalTx);
    EoamNotifyEventTrap (pLogEntry, pPortInfo->u4Port,
                         EOAM_NOTIFY_NON_THRESH_EVENT);
    EoamCltInfFillUnusedData (pLogEntry);
    EOAM_TRC_FN_EXIT ();
    return;
}

/*******************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamClntSendCriticalLinkEvtToPeer                  
 *                                                                          
 *    DESCRIPTION      : This function triggers a critical event indication 
 *                       message to be sent to the peer.
 *                                                                          
 *    INPUT            : pPortInfo - pointer to port specific information   
 *                       pLogEntry - pointer to the log entry
 *                       u1State - State denoting whether the event has
 *                                 occured or has been rectified
 *                                                                          
 *    OUTPUT           : None.                                              
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE                        
 *                                                                          
*******************************************************************************/
VOID
EoamCltInfTriggerCriticalEvtMsg (tEoamPortInfo * pPortInfo, UINT1 u1State)
{
    tEoamEnaPortInfo   *pActivePortInfo = NULL;
    tOsixSysTime        CurrentTime;
    UINT2               u2Count = 0;
    tEoamEventLogEntry *pLogEntry = NULL;

    EOAM_TRC_FN_ENTRY ();
    /* Get the System Time stamp when a Critical link Event is received */
    MEMSET (&CurrentTime, 0, sizeof (tOsixSysTime));
    OsixGetSysTime (&CurrentTime);
    pActivePortInfo = pPortInfo->pEoamEnaPortInfo;
    if (pActivePortInfo == NULL)
    {
        return;
    }
    if (u1State == OSIX_TRUE)
    {
        pLogEntry = EoamUtilGetLogEntryToBeFilled (pPortInfo);
        if (pLogEntry == NULL)
        {
            return;
        }
        pActivePortInfo->CtrlReq.b1LocalCriticalEvent = OSIX_TRUE;
        pLogEntry->u4EventLogType = EOAM_CRITICAL_EVENT;
        MEMCPY (pLogEntry->au1EventLogOui, gaTlvOui, EOAM_OUI_LENGTH);
        pLogEntry->u4EventLogIndex = ++(pActivePortInfo->u4LogIndexCount);
        pLogEntry->EventLogInfo.u4EventLogTimestamp = CurrentTime;
        pLogEntry->u1EventLogLocation = EOAM_LOCAL_ENTRY;
        pLogEntry->u4NonThresLogEvtTotal =
            ++(pActivePortInfo->u4NonThresLogEvtTotalTx);
        EoamNotifyEventTrap (pLogEntry, pPortInfo->u4Port,
                             EOAM_NOTIFY_NON_THRESH_EVENT);
        EoamCltInfFillUnusedData (pLogEntry);
        if (pPortInfo->ErrEventConfigInfo.u1CriticalEventEnable == EOAM_FALSE)
        {
            EOAM_TRC ((ALL_FAILURE_TRC | EOAM_RFI_TRC, "CLIENT:Local Entity"
                       "not set to intimate critical event to peer on"
                       "port %u\n", pPortInfo->u4Port));
            return;
        }
        EoamCtlTxConstructInfoPdu (pPortInfo);
        for (u2Count = 0; u2Count < EOAM_CRITICAL_EVT_RESEND_CNT; u2Count++)
        {
            if (EoamCtlTxSendInfoPdu (pPortInfo) != OSIX_SUCCESS)
            {
                EOAM_TRC ((ALL_FAILURE_TRC, "CLIENT:EoamCtlTxSendInfoPdu "
                           "failed on port %u\n", pPortInfo->u4Port));
            }
        }
    }
    else
    {
        pActivePortInfo->CtrlReq.b1LocalCriticalEvent = OSIX_FALSE;
        /* Construct info pdu to indicate correction of an event */
        EoamCtlTxConstructInfoPdu (pPortInfo);
    }

    /* Sync local status with Standby Node */
    EoamRedSyncUpDynamicInfo (pPortInfo);
    EOAM_TRC_FN_EXIT ();
    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  emcltinf.c                     */
/*-----------------------------------------------------------------------*/
