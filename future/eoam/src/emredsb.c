/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emredsb.c,v 1.4 2008/05/28 10:07:32 iss Exp $
 *
 * Description: This file contains EOAM High Availability handling
 *              routines (stub file).
 *********************************************************************/
#ifndef _EMRED_c
#define _EMRED_c

#include "eminc.h"

/*****************************************************************************
 * Function Name      : EoamRedRegisterWithRM
 * Description        : Registers EOAM with RM by providing an application
 *                      ID for Eoam and a call back function to be called
 *                      whenever RM needs to send an event to EOAM
 * Input(s)           : None
 * Output(s)          : None
 * Return Value(s)    : OSIX_SUCCESS - if registration is successful
 *                      OSIX_FAILURE - otherwise
 *****************************************************************************/
PUBLIC INT4
EoamRedRegisterWithRM (VOID)
{
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EoamRedDeRegisterWithRM
 * Description        : Deregisters EOAM with RM
 * Input(s)           : None
 * Output(s)          : None
 * Return Value(s)    : OSIX_SUCCESS - if registration is successful
 *                      OSIX_FAILURE - otherwise
 *****************************************************************************/
PUBLIC INT4
EoamRedDeRegisterWithRM (VOID)
{
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EoamRedInitRedGlobalInfo
 * Description        : This function initializes global variable used to
 *                      store EOAM Redundancy information
 * Input(s)           : None
 * Output(s)          : None
 * Return Value(s)    : None
 * 
 *****************************************************************************/
PUBLIC VOID
EoamRedInitRedGlobalInfo (VOID)
{
    return;
}

/*****************************************************************************
 * Function Name      : EoamRedGetNodeStateFromRm
 * Description        : This function gets Node status from RM and stores in
 *                      gEoamNodeStatus
 * Input(s)           : None
 * Output(s)          : None
 * Return Value(s)    : None
 ****************************************************************************/
PUBLIC VOID
EoamRedGetNodeStateFromRm (VOID)
{
    return;
}

/***************************************************************************
 * Function Name      : EoamRedProcessRmMsg
 * Description        : This function  based on the RM event present in the 
 *                      given input buffer, takes appropriate action
 * Input(s)           : pMsg - Pointer to the structure containing RM
 *                             buffer and the RM event
 * Output(s)          : None
 * Return Value(s)    : None
 ***************************************************************************/
PUBLIC VOID
EoamRedProcessRmMsg (tEoamQMsg * pMsg)
{
    UNUSED_PARAM (pMsg);
    return;
}

/****************************************************************************
 * Function Name      : EoamRedSyncUpOperStatus
 * Description        : This function is sends a sync up message to standby
 *                      node to syncup the change in operational status of
 *                      an interface.
 * Input(s)           : pPortInfo - pointer to Eoam Portinfo
 * Output(s)          : None
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
EoamRedSyncUpOperStatus (tEoamPortInfo * pPortInfo)
{
    UNUSED_PARAM (pPortInfo);
    return;
}

/****************************************************************************
 * Function Name      : EoamRedSyncUpDynamicInfo
 * Description        : This function is used send a discovery state sync up
 *                      message to standby node. Only Discovery operational
 *                      and non-operational states are updated to the standby
 *                      node
 * Input(s)           : pPortInfo - pointer to Eoam Portinfo
 * Output(s)          : None
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
EoamRedSyncUpDynamicInfo (tEoamPortInfo * pPortInfo)
{
    UNUSED_PARAM (pPortInfo);
    return;
}

/******************************************************************************
 * Function Name      : EoamRestartModule
 * Description        : This function will restart EOAM module
 * Input(s)           : None
 * Output(s)          : None
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
EoamRestartModule (VOID)
{
    return OSIX_SUCCESS;
}

#endif /* _EMRED_c */

/*-----------------------------------------------------------------------*/
/*                       End of the file  emredsb.c                      */
/*-----------------------------------------------------------------------*/
