/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emmbsm.c,v 1.5 2012/08/08 10:02:21 siva Exp $
 * 
 * Description: This file contains functions for handling card insertion 
 *              and removal in a chassis system.
 *********************************************************************/
#include "eminc.h"

PRIVATE INT4 EoamMbsmUpdateOnCardInsert PROTO ((tMbsmSlotInfo *,
                                                tMbsmPortInfo *));
PRIVATE INT4 EoamMbsmUpdateOnCardRemove PROTO ((tMbsmSlotInfo *,
                                                tMbsmPortInfo *));
/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamMbsmHandleLcStatusChg 
 *                                                                          
 *    DESCRIPTION      : This function programs the HW with the EOAM  
 *                       software configuration. 
 *                       This function will be called from the EOAM module
 *                       when an indication for the Card Insertion/Removal   
 *                       is received from the MBSM. 
 *
 *    INPUT            : pBuf  - Buffer containing the protocol message 
 *                               information 
 *                       u4Cmd - Line card - Insert/Remove
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : None.
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EoamMbsmHandleLcStatusChg (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Cmd)
{
    INT4                i4RetStatus = MBSM_SUCCESS;
    tMbsmProtoMsg       ProtoMsg;
    tMbsmProtoAckMsg    ProtoAckMsg;

    FSAP_ASSERT (pBuf != NULL);

    if (gEoamGlobalInfo.u1SystemCtrlStatus == EOAM_SHUTDOWN)
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "EOAM module is shut down\n"));
        i4RetStatus = MBSM_FAILURE;
    }

    else
    {
        if ((CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &ProtoMsg, 0,
                                        sizeof (tMbsmProtoMsg))) == CRU_FAILURE)
        {
            EOAM_TRC ((BUFFER_TRC | ALL_FAILURE_TRC,
                       "CRU Buf Copy From Buf Chain Failed\n"));
            i4RetStatus = MBSM_FAILURE;
        }

        else
        {
            if (u4Cmd == MBSM_MSG_CARD_INSERT)
            {

                i4RetStatus = EoamMbsmUpdateOnCardInsert
                    (&(ProtoMsg.MbsmSlotInfo), &(ProtoMsg.MbsmPortInfo));
            }

            if (u4Cmd == MBSM_MSG_CARD_REMOVE)
            {

                i4RetStatus = EoamMbsmUpdateOnCardRemove
                    (&(ProtoMsg.MbsmSlotInfo), &(ProtoMsg.MbsmPortInfo));
            }
        }
    }
    /* Send an acknowledgment to the MBSM module to inform the completion
     * of updation of interfaces in the NP
     */

    ProtoAckMsg.i4RetStatus = i4RetStatus;
    ProtoAckMsg.i4SlotId = ProtoMsg.MbsmSlotInfo.i4SlotId;
    ProtoAckMsg.i4ProtoCookie = ProtoMsg.i4ProtoCookie;
    MbsmSendAckFromProto (&ProtoAckMsg);

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamMbsmUpdateOnCardInsert 
 *                                                                          
 *    DESCRIPTION      : This function used to perform the necessary actions 
 *                       in HW and SW  when an indication for the 
 *                       Card Insertion is received from the MBSM. 
 *
 *    INPUT            : pPortInfo - Inserted port list     
 *                       pSlotInfo - Information about inserted slot
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : MBSM_SUCCESS/MBSM_FAILURE
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
EoamMbsmUpdateOnCardInsert (tMbsmSlotInfo * pSlotInfo,
                            tMbsmPortInfo * pPortInfo)
{
    UINT4               u4StartIfIndex = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4Index = 0;
    UINT4               u4PortCount = 0;
    INT4                i4RetStatus = MBSM_SUCCESS;
    tEoamPortInfo      *pPortEntry = NULL;
    UINT1               u1IsSetInPortListStatus = OSIX_FALSE;
    UINT1               u1IsSetInPortList = OSIX_FALSE;
    if (OSIX_FALSE == MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo))
    {
        if (EoamEoamMbsmHwInit (pSlotInfo) == FNP_FAILURE)
        {
            i4RetStatus = MBSM_FAILURE;
        }
    }

    u4StartIfIndex = pPortInfo->u4StartIfIndex;
    u4PortCount = pPortInfo->u4PortCount;
    for (u4Index = 0; u4Index < u4PortCount; u4Index++)
    {
        u4IfIndex = u4StartIfIndex + u4Index;
        OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST (pPortInfo), u4IfIndex,
                                 sizeof (tPortList), u1IsSetInPortList);
        OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST_STATUS (pPortInfo),
                                 u4IfIndex, sizeof (tPortList),
                                 u1IsSetInPortListStatus);

        /* PORT INSERT for the corresponding bit in portList */
        if ((u1IsSetInPortList == OSIX_TRUE)
            && (OSIX_FALSE == u1IsSetInPortListStatus))
        {
            if (EoamUtilGetPortEntry (u4IfIndex, &pPortEntry) == OSIX_FAILURE)
            {
                continue;
            }

            /* Get the Unidirectional Capapbility from HW and update the same in 
             * SW. During PreConfiguration the LC may not be present and in that case
             * Unidirectional capbaility for that particular port is not set.
             * The unidirectional Cpb support of any port is known only 
             * when the LC is inserted, so we have to update the same */
            EoamIfSetUniDirectionalCpb (pPortEntry);

            /* EoamLmNpConfigParams API need to be invoked from here if the HW
             * provides support for LM configurations.
             * In such case, the Link Monitor related configurations 
             * like window & threshold for EVERY EVENT
             * should be programmed in HW based on SW configurations. 
             */
        }
    }

    return i4RetStatus;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamMbsmUpdateOnCardRemove 
 *                                                                          
 *    DESCRIPTION      : This function programs the HW with the EOAM  
 *                       software configuration. 
 *                       This function will be called from the EOAM module
 *                       when an indication for the Card Removal is  
 *                       received from the MBSM. 
 *
 *    INPUT            : pPortInfo - Removed port list     
 *                       pSlotInfo - Information about removed slot
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : MBSM_SUCCESS/MBSM_FAILURE
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
EoamMbsmUpdateOnCardRemove (tMbsmSlotInfo * pSlotInfo,
                            tMbsmPortInfo * pPortInfo)
{

    UNUSED_PARAM (pPortInfo);
    UNUSED_PARAM (pSlotInfo);

    return (MBSM_SUCCESS);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamMbsmPostMessage 
 *                                                                          
 *    DESCRIPTION      : Allocates a CRU buffer and enqueues the Line card
 *                       change status to the EOAM Task
 *
 *    INPUT            : pProtoMsg - Contains the Slot and Port Information 
 *                       i4Event  - Line card Up/Down status          
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : MBSM_SUCCESS/MBSM_FAILURE
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamMbsmPostMessage (tMbsmProtoMsg * pProtoMsg, INT4 i4Event)
{
    INT4                i4RetVal = MBSM_SUCCESS;
    tEoamQMsg          *pQMsg = NULL;
    tMbsmProtoAckMsg    MbsmProtoAckMsg;

    MEMSET (&(MbsmProtoAckMsg), 0, sizeof (tMbsmProtoAckMsg));

    if (pProtoMsg == NULL)
    {
        return MBSM_FAILURE;
    }

    if (gEoamGlobalInfo.u1SystemCtrlStatus == EOAM_SHUTDOWN)
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "MbsmEoam: EOAM is not started\n"));
        /* If the module is shutdown no need to proceed further, send the ack 
         * to MBSM so that other modules get the LcStatus Notification. 
         * The ack is sent only for hardware updation to be in
         * synchronization across the dependant modules. 
         */
        MbsmProtoAckMsg.i4ProtoCookie = pProtoMsg->i4ProtoCookie;
        MbsmProtoAckMsg.i4SlotId = pProtoMsg->MbsmSlotInfo.i4SlotId;
        MbsmProtoAckMsg.i4RetStatus = MBSM_SUCCESS;
        MbsmSendAckFromProto (&MbsmProtoAckMsg);
        return MBSM_SUCCESS;
    }

    /* Allocate Interface message buffer from the pool */
    if ((pQMsg = (tEoamQMsg *) (MemAllocMemBlk (gEoamGlobalInfo.QMsgPoolId)))
        == NULL)
    {
        EOAM_TRC ((BUFFER_TRC | ALL_FAILURE_TRC,
                   "EoamMbsmPostMessage: ALLOC_MEM_BLOCK FAILED\n"));
        return MBSM_FAILURE;
    }

    MEMSET (pQMsg, 0, sizeof (tEoamQMsg));
    pQMsg->u4MsgType = (UINT4) i4Event;

    if ((pQMsg->unEoamMsgParam.pMbsmProtoMsg =
         CRU_BUF_Allocate_MsgBufChain (sizeof (tMbsmProtoMsg), 0)) == NULL)
    {
        EOAM_TRC ((BUFFER_TRC | ALL_FAILURE_TRC,
                   "Allocation of CRU Buffer Failed\n"));
        if (MemReleaseMemBlock (gEoamGlobalInfo.QMsgPoolId, (UINT1 *) &pQMsg)
            != MEM_SUCCESS)
        {

            EOAM_TRC ((BUFFER_TRC | ALL_FAILURE_TRC,
                       "MemReleaseMemBlock FAILED\n"));
        }

        return MBSM_FAILURE;
    }

    /* Copy the message to the CRU buffer */
    if ((CRU_BUF_Copy_OverBufChain (pQMsg->unEoamMsgParam.
                                    pMbsmProtoMsg, (UINT1 *) pProtoMsg, 0,
                                    sizeof (tMbsmProtoMsg))) == CRU_FAILURE)
    {
        EOAM_TRC ((BUFFER_TRC | ALL_FAILURE_TRC,
                   "CRU Buf Copy Over Buf Chain Failed\n"));
        CRU_BUF_Release_MsgBufChain (pQMsg->unEoamMsgParam.
                                     pMbsmProtoMsg, FALSE);
        if (MemReleaseMemBlock (gEoamGlobalInfo.QMsgPoolId, (UINT1 *) &pQMsg)
            != MEM_SUCCESS)
        {

            EOAM_TRC ((BUFFER_TRC | ALL_FAILURE_TRC,
                       "MemReleaseMemBlock FAILED\n"));
        }

        return MBSM_FAILURE;
    }

    if (EoamQueEnqAppMsg (pQMsg) == OSIX_FAILURE)
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "EoamQueEngAppMsg Failed\n"));
        i4RetVal = MBSM_FAILURE;
    }

    return i4RetVal;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  emmbsm.c                       */
/*-----------------------------------------------------------------------*/
