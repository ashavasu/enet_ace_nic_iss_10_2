/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emlmmain.c,v 1.8 2010/09/20 10:09:10 prabuc Exp $
 *
 * Description: This file contains Ethernet OAM Link monitoring task 
 *              related functions
 *****************************************************************************/

#ifndef EMLMMAIN_C
#define EMLMMAIN_C

#include "eminc.h"
#include "emlmdefn.h"
#include "emlmglob.h"

PRIVATE VOID EoamLmMainDeleteAllNodes PROTO ((VOID));

/******************************************************************************
 * Function Name      : EoamLmMainTask
 *
 * Description        : Eoam Link monitoring Main task
 *  
 * Input(s)           : pArg - Pointer to the arguments
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
EoamLmMainTask (INT1 *pArg)
{
    UINT4               u4Events = 0;

    EOAM_TRC_FN_ENTRY ();

    UNUSED_PARAM (pArg);
    MEMSET (&gEoamLmGlobalInfo, 0, sizeof (tEoamLmGlobalInfo));
    OsixTskIdSelf (&(gEoamLmGlobalInfo.TaskId));

    if (OsixSemCrt ((UINT1 *) EOAMLM_SEM_NAME, &(gEoamLmGlobalInfo.SemId))
        != OSIX_SUCCESS)
    {
        EOAM_TRC ((ALL_FAILURE_TRC | EOAM_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "EoamLmMainTask: EOAM LM mutual exclusion semaphore creation"
                   " failed\r\n"));
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    OsixSemGive (gEoamLmGlobalInfo.SemId);

    lrInitComplete (OSIX_SUCCESS);

    while (1)
    {
        if (OsixEvtRecv (gEoamLmGlobalInfo.TaskId, EOAM_LM_ALL_EVENTS,
                         OSIX_WAIT, &u4Events) == OSIX_SUCCESS)
        {
            if (u4Events & EOAM_LM_TMR_EXPIRY_EVENT)
            {
                EoamLmPollTimerExpiry ();
            }
            else if (u4Events & EOAM_LM_GO_ACTIVE_EVENT)
            {
                EoamLmPollTimerStart ();
            }
            else if (u4Events & EOAM_LM_GO_STANDBY_EVENT)
            {
                EoamLmPollTimerStop ();
            }
        }
    }
}

/******************************************************************************
 * Function Name      : EoamLmMainInit
 *
 * Description        : Create Mempool and timer list
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
EoamLmMainInit (VOID)
{

    EOAM_TRC_FN_ENTRY ();
    TMO_DLL_Init (&(gEoamLmGlobalInfo.LmEoamEnaPortDllList));

    if (TmrCreateTimerList ((UINT1 *) EOAMLM_TASK_NAME,
                            EOAM_LM_TMR_EXPIRY_EVENT, NULL,
                            &(gEoamLmGlobalInfo.LmTmrList)) != TMR_SUCCESS)

    {
        EOAM_TRC ((ALL_FAILURE_TRC | EOAM_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "EoamLmMainInit: EOAM global Timer list creation "
                   "failed\r\n"));
        EoamLmMainShutdown ();
        return OSIX_FAILURE;
    }
    gu1EoamLMStatFlag = EOAM_LM_ENABLED;

    EOAM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function Name      : EoamLmMainShutDown
 *
 * Description        : This function releases all memory pool and deletes
 *                      the tables
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
EoamLmMainShutdown (VOID)
{
    EOAM_TRC_FN_ENTRY ();

    /* Set status as disabled in the starting itself before any flushing */
    gu1EoamLMStatFlag = EOAM_LM_DISABLED;

    OsixSemTake (gEoamLmGlobalInfo.SemId);

    /* Delete all entries from DLL */
    EoamLmMainDeleteAllNodes ();

    /* Delete Timer list */
    if (gEoamLmGlobalInfo.LmTmrList != 0)
    {
        if (TmrDeleteTimerList (gEoamLmGlobalInfo.LmTmrList) != TMR_SUCCESS)
        {
            EOAM_TRC ((ALL_FAILURE_TRC | EOAM_CRITICAL_TRC | OS_RESOURCE_TRC,
                       "EoamLmMainShutdown: Unable to delete global timer "
                       "list\r\n"));
        }
        gEoamLmGlobalInfo.LmTmrList = 0;
    }

    OsixSemGive (gEoamLmGlobalInfo.SemId);

    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EoamLmMainDeleteAllNodes
 *
 * Description        : This function deletes all the nodes from the   
 *                      DLL 
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE VOID
EoamLmMainDeleteAllNodes (VOID)
{
    UINT1               u1IsEnaPortListPresent = OSIX_FALSE;
    tEoamLmEnaPortDllNode *pListNode = NULL;
    tEoamLmEnaPortDllNode *pTemp = NULL;

    EOAM_TRC_FN_ENTRY ();

    UTL_DLL_OFFSET_SCAN (&(gEoamLmGlobalInfo.LmEoamEnaPortDllList),
                         pListNode, pTemp, tEoamLmEnaPortDllNode *)
    {
        TMO_DLL_Delete (&(gEoamLmGlobalInfo.LmEoamEnaPortDllList),
                        &(pListNode->NodeLink));
        MemReleaseMemBlock (gEoamLmGlobalInfo.LmEnaDllNodePoolId,
                            (UINT1 *) pListNode);
        u1IsEnaPortListPresent = OSIX_TRUE;
    }

    if (u1IsEnaPortListPresent == OSIX_TRUE)
    {
        if ((TmrStopTimer (gEoamLmGlobalInfo.LmTmrList,
                           &gEoamLmGlobalInfo.LmTmrNode)) != TMR_SUCCESS)
        {
            EOAM_TRC ((CONTROL_PLANE_TRC | OS_RESOURCE_TRC |
                       ALL_FAILURE_TRC,
                       "EoamLmMainDeleteAllNodes: Stopping Timer FAILED\n"));
        }
    }
    EOAM_TRC_FN_EXIT ();
    return;
}

#endif /* EMLMMAIN_C */
/*-----------------------------------------------------------------------*/
/*                       End of the file  emlmmain.c                     */
/*-----------------------------------------------------------------------*/
