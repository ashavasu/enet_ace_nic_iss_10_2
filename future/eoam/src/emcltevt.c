/*******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emcltevt.c,v 1.15 2013/02/02 10:12:22 siva Exp $
 *
 * Description: This file conatains the functions related to Event Notification 
 * PDU handling by EOAM client module. 
 ******************************************************************************/
#include "eminc.h"

PRIVATE INT4 EoamCltEvtCheckAndTransmit PROTO ((tEoamPortInfo *,
                                                tEoamThresEventInfo *, UINT4));
PRIVATE VOID EoamCltEvtPduTransmit PROTO ((tEoamPortInfo * pPortEntry,
                                           tEoamThresEventInfo * pEventInfo));
PRIVATE VOID EoamCltEvtTxEvent PROTO ((tEoamPortInfo * pPortEntry,
                                       tEoamPduHeader * pPduHeader,
                                       tEoamPduData * pPduData));
/*******************************************************************************
 *                                                                          
 *    FUNCTION NAME    :  EoamCltEvtNotifPduProcess 
 *                                                                          
 *    DESCRIPTION      :  This function is called on reception of an Event   
 *                        Notification OAMPDU. On reception of this PDU,    
 *                        it notifies the Fault Management module with the  
 *                        appropriate threshold crossing information. It    
 *                        also adds a remote log entry for the event.       
 *                                                                          
 *    INPUT            :  pPortInfo - pointer to port specific info          
 *                        pData - pointer to PDU Data                       
 *                                                                          
 *    OUTPUT           :  None.                                             
 *                                                                          
 *    RETURNS          :  OSIX_SUCCESS / OSIX_FAILURE                       
 *                                                                          
*******************************************************************************/
PUBLIC INT4
EoamCltEvtNotifPduProcess (tEoamPortInfo * pPortInfo, tEoamPduData * pData)
{
    tEoamEnaPortInfo   *pActivePortInfo = NULL;
    tEoamEventLogEntry *pLogEntry = NULL;
    tEoamCallbackInfo   CallbackInfo;
    tOsixSysTime        CurrentTime;
    UINT1               au1LinkEventMap[EOAM_LNK_EVT_MAX_BIT] = {
        EOAM_ERRORED_SYMBOL_EVENT_TLV,
        EOAM_ERRORED_FRAME_EVENT_TLV,
        EOAM_ERRORED_FRAME_PERIOD_EVENT_TLV,
        EOAM_ERRORED_FRAME_SECONDS_EVENT_TLV,
        EOAM_ORG_SPEC_LINK_EVENT_TLV,
    };
    UINT1               u1Count = 0;
    UINT1               u1BitMap = 0;
    UINT1               u1EventType = 0;

    EOAM_TRC_FN_ENTRY ();
    MEMSET (&CallbackInfo, 0, sizeof (tEoamCallbackInfo));
    MEMSET (&CurrentTime, 0, sizeof (tOsixSysTime));
    pActivePortInfo = pPortInfo->pEoamEnaPortInfo;

    if (pData->EvtPduData.u2SeqNum == pActivePortInfo->u2RxSeqNum)
    {
        /* If Event notification OAMPDUs are received with the same sequence 
         * number as that of the last received Original Event notification 
         * OAMPDU, Duplicate Rx counter is incremented */
        pPortInfo->PduStats.u4DuplicateEventNotificationRx++;
        return OSIX_SUCCESS;
    }
    if ((pData->EvtPduData.u2SeqNum < pActivePortInfo->u2RxSeqNum) &&
        (pData->EvtPduData.u2SeqNum != 0))
    {
        /* If Event notification OAMPDUs are received  with the sequence 
         * number which is lesser than that of the last received Original 
         * Event notification OAMPDU, they are simply ignored. No counters 
         * are incremented */
        EOAM_TRC ((ALL_FAILURE_TRC, "CLIENT: Received Stale Event"
                   "Notification OAMPDUs on port %u\n", pPortInfo->u4Port));
        return OSIX_FAILURE;
    }
    /* Get the System Time stamp when a Event Notification OAMPDU is received */
    OsixGetSysTime (&CurrentTime);
    u1BitMap = pData->EvtPduData.u1EvtBitMap;

    while (u1BitMap)
    {
        if (u1BitMap & (1 << u1Count))
        {
            u1BitMap ^= (1 << u1Count);
            u1EventType = au1LinkEventMap[u1Count];
            u1Count++;
        }
        else
        {
            u1Count++;
            continue;
        }

        /* Get the pointer to the log entry and update with the event information */
        pLogEntry = EoamUtilGetLogEntryToBeFilled (pPortInfo);
        if (pLogEntry != NULL)
        {
            pLogEntry->EventLogInfo.u4EventLogTimestamp = CurrentTime;
            pLogEntry->u4EventLogIndex = ++(pActivePortInfo->u4LogIndexCount);
            pLogEntry->u1EventLogLocation = EOAM_REMOTE_ENTRY;
            if (pData->EvtPduData.u1EvtType == EOAM_ORG_SPEC_EVENT)
            {
                MEMCPY (pLogEntry->au1EventLogOui,
                        pPortInfo->RemoteInfo.au1VendorOui, EOAM_OUI_LENGTH);
                pLogEntry->u4EventLogType = EOAM_ORG_SPEC_LINK_EVENT;
                EoamPopulateOrgEvtLogEntry (&(pData->EvtPduData.OrgEvtData),
                                            pLogEntry);
            }
            else
            {
                MEMCPY (pLogEntry->au1EventLogOui, gaTlvOui, EOAM_OUI_LENGTH);
                if (u1EventType == EOAM_ERRORED_FRAME_PERIOD_EVENT_TLV)
                {
                    pLogEntry->u4EventLogType = EOAM_ERRORED_FRAME_PERIOD_EVENT;
                }
                else if (u1EventType == EOAM_ERRORED_FRAME_EVENT_TLV)
                {
                    pLogEntry->u4EventLogType = EOAM_ERRORED_FRAME_EVENT;
                }
                else
                {
                    pLogEntry->u4EventLogType = u1EventType;
                }

                switch (u1EventType)
                {
                    case EOAM_ERRORED_SYMBOL_EVENT_TLV:
                        MEMCPY (&(pLogEntry->EventLogInfo),
                                &(pData->EvtPduData.ErrSymblPrdEvnt),
                                sizeof (tEoamThresEventInfo));
                        break;
                    case EOAM_ERRORED_FRAME_EVENT_TLV:
                        MEMCPY (&(pLogEntry->EventLogInfo),
                                &(pData->EvtPduData.ErrFrmEvnt),
                                sizeof (tEoamThresEventInfo));
                        break;
                    case EOAM_ERRORED_FRAME_PERIOD_EVENT_TLV:
                        MEMCPY (&(pLogEntry->EventLogInfo),
                                &(pData->EvtPduData.ErrFrmPrdEvnt),
                                sizeof (tEoamThresEventInfo));
                        break;
                    case EOAM_ERRORED_FRAME_SECONDS_EVENT_TLV:
                        MEMCPY (&(pLogEntry->EventLogInfo),
                                &(pData->EvtPduData.ErrFrmSecSumryEvnt),
                                sizeof (tEoamThresEventInfo));
                        break;
                    default:
                        EOAM_TRC ((EOAM_LM_TRC,
                                   "CLIENT: Invalid Event Notification "
                                   "Type %u\n ", u1EventType));
                        break;
                }

                /* Setting Local EventLogTimestamp at when OAMPDU receieved */
                pLogEntry->EventLogInfo.u4EventLogTimestamp = CurrentTime;
            }

            EoamNotifyEventTrap (pLogEntry,
                                 pPortInfo->u4Port, EOAM_NOTIFY_THRESH_EVENT);
        }

        /* Increment the Unique Event Notification Rx counter */
        pPortInfo->PduStats.u4UniqueEventNotificationRx++;
        /* Fill up the callback info structure */
        CallbackInfo.u4Port = pPortInfo->u4Port;

        if (pData->EvtPduData.u1EvtType == EOAM_ORG_SPEC_EVENT)
        {
            CallbackInfo.u4EventType = EOAM_NOTIFY_ORG_SPECIFIC_EVENT;
            /* Local Pointer is copied to the callback info of the 
             * external module. If external module runs as a separate task
             * it should allocate its own memory and copy this data before
             * using it */
            CallbackInfo.ExtData.u4DataLen =
                pData->EvtPduData.OrgEvtData.u4DataLen;
            CallbackInfo.ExtData.pu1Data = pData->EvtPduData.OrgEvtData.pu1Data;
        }
        else
        {
            switch (u1EventType)
            {
                case EOAM_ERRORED_SYMBOL_EVENT_TLV:
                    CallbackInfo.u4EventType = EOAM_NOTIFY_ERRORED_SYMBOL;

                    MEMCPY (&(CallbackInfo.ThresEventInfo),
                            &(pData->EvtPduData.ErrSymblPrdEvnt),
                            sizeof (tEoamThresEventInfo));
                    break;
                case EOAM_ERRORED_FRAME_EVENT_TLV:
                    CallbackInfo.u4EventType = EOAM_NOTIFY_ERRORED_FRAME;

                    MEMCPY (&(CallbackInfo.ThresEventInfo),
                            &(pData->EvtPduData.ErrFrmEvnt),
                            sizeof (tEoamThresEventInfo));
                    break;
                case EOAM_ERRORED_FRAME_PERIOD_EVENT_TLV:
                    CallbackInfo.u4EventType = EOAM_NOTIFY_ERRORED_FRAME_PERIOD;

                    MEMCPY (&(CallbackInfo.ThresEventInfo),
                            &(pData->EvtPduData.ErrFrmPrdEvnt),
                            sizeof (tEoamThresEventInfo));
                    break;
                case EOAM_ERRORED_FRAME_SECONDS_EVENT_TLV:
                    CallbackInfo.u4EventType =
                        EOAM_NOTIFY_ERRORED_FRAME_SECONDS;

                    MEMCPY (&(CallbackInfo.ThresEventInfo),
                            &(pData->EvtPduData.ErrFrmSecSumryEvnt),
                            sizeof (tEoamThresEventInfo));

                    break;
                default:
                    EOAM_TRC ((EOAM_LM_TRC,
                               "CLIENT: Invalid Event Notification "
                               "Type received on port %u\n ",
                               pPortInfo->u4Port));
            }
        }

        CallbackInfo.u2Location = EOAM_REMOTE_ENTRY;
        /* Call a function to notify all registered modules */
        EoamMainNotifyModules (&CallbackInfo);
    }

    /* Update the received sequence number in the port structure */
    pActivePortInfo->u2RxSeqNum = pData->EvtPduData.u2SeqNum;

    EOAM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*******************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamCltEvtSendThresholdEvtToPeer 
 *                                                                          
 *    DESCRIPTION      : This function is called when a threshold-crossing   
 *                       event is detected by Link Monitoring Module. It    
 *                       logs the event in the local database and if peer   
 *                       notification is not disabled sends OAMPDU request  
 *                       to the Control Module.                             
 *                                                                          
 *    INPUT            : pPortEntry - pointer to port specific information  
 *                       pEventInfo - pointer to the event information      
 *                       u4MessageType - Type of message denoting which 
 *                       event has occured
 *                                                                          
 *    OUTPUT           : None.                                              
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE                        
 *                                                                          
*******************************************************************************/
PUBLIC INT4
EoamCltEvtSendThresholdEvtToPeer (tEoamPortInfo * pPortEntry,
                                  tEoamThresEventInfo * pEventInfo,
                                  UINT4 u4MessageType)
{
    tEoamEnaPortInfo   *pActivePortInfo = NULL;
    tEoamEventLogEntry *pLogEntry = NULL;
    tEoamCallbackInfo   CallbackInfo;

    EOAM_TRC_FN_ENTRY ();

    if (gEoamGlobalInfo.u1ModuleStatus != EOAM_ENABLED)
    {
        EOAM_TRC ((ALL_FAILURE_TRC | EOAM_LM_TRC,
                   "EOAM is globally DISABLED!!!\n"));
        return OSIX_FAILURE;
    }

    if (pPortEntry == NULL)
    {
        return OSIX_SUCCESS;
    }

    FSAP_ASSERT (pEventInfo != NULL);
    MEMSET (&CallbackInfo, 0, sizeof (tEoamCallbackInfo));
    pActivePortInfo = pPortEntry->pEoamEnaPortInfo;
    if (pActivePortInfo == NULL)
    {
        EOAM_TRC ((ALL_FAILURE_TRC | EOAM_CRITICAL_TRC,
                   "EOAM not enabled on port %u\n", pPortEntry->u4Port));
        return OSIX_SUCCESS;
    }

    /* Get the pointer to the log entry and update with the event information */
    pLogEntry = EoamUtilGetLogEntryToBeFilled (pPortEntry);
    if (pLogEntry != NULL)
    {
        /* Fill up the Event Log structure appropriately */
        MEMCPY (pLogEntry->au1EventLogOui, gaTlvOui, EOAM_OUI_LENGTH);
        pLogEntry->u4EventLogIndex = ++(pActivePortInfo->u4LogIndexCount);
        pLogEntry->EventLogInfo.u4EventLogTimestamp =
            pEventInfo->u4EventLogTimestamp;
        pLogEntry->u1EventLogLocation = EOAM_LOCAL_ENTRY;

        if (pEventInfo->u1EventType == EOAM_ERRORED_FRAME_PERIOD_EVENT_TLV)
        {
            pLogEntry->u4EventLogType = EOAM_ERRORED_FRAME_PERIOD_EVENT;
        }
        else if (pEventInfo->u1EventType == EOAM_ERRORED_FRAME_EVENT_TLV)
        {
            pLogEntry->u4EventLogType = EOAM_ERRORED_FRAME_EVENT;
        }
        else
        {
            pLogEntry->u4EventLogType = pEventInfo->u1EventType;
        }
        MEMCPY (&(pLogEntry->EventLogInfo), pEventInfo,
                sizeof (tEoamThresEventInfo));

        EoamNotifyEventTrap (pLogEntry, pPortEntry->u4Port,
                             EOAM_NOTIFY_THRESH_EVENT);

    }
    /* Fill up the callback info structure */
    CallbackInfo.u4Port = pPortEntry->u4Port;

    switch (pEventInfo->u1EventType)
    {
        case EOAM_ERRORED_SYMBOL_EVENT_TLV:
            CallbackInfo.u4EventType = EOAM_NOTIFY_ERRORED_SYMBOL;
            break;
        case EOAM_ERRORED_FRAME_EVENT_TLV:
            CallbackInfo.u4EventType = EOAM_NOTIFY_ERRORED_FRAME;
            break;
        case EOAM_ERRORED_FRAME_PERIOD_EVENT_TLV:
            CallbackInfo.u4EventType = EOAM_NOTIFY_ERRORED_FRAME_PERIOD;
            break;
        case EOAM_ERRORED_FRAME_SECONDS_EVENT_TLV:
            CallbackInfo.u4EventType = EOAM_NOTIFY_ERRORED_FRAME_SECONDS;
            break;
        default:
            EOAM_TRC ((EOAM_LM_TRC, "CLIENT: Invalid Event Notification Type "
                       "requested to send on port %u\n ", pPortEntry->u4Port));
    }
    CallbackInfo.u2Location = EOAM_LOCAL_ENTRY;
    /* Call a function to notify all registered modules */
    EoamMainNotifyModules (&CallbackInfo);

    if (EoamCltEvtCheckAndTransmit (pPortEntry, pEventInfo, u4MessageType) !=
        OSIX_SUCCESS)
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "CLIENT: Notification to External"
                   "modules failed for port %u\n ", pPortEntry->u4Port));
        return OSIX_FAILURE;
    }
    EOAM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*******************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamCltEvtCheckAndTransmit 
 *                                                                          
 *    DESCRIPTION      : This function checks whether notification to peer 
 *                       is enabled and then sends a request to the control
 *                       module to transmit the Event Notification PDU
 *                                                                          
 *    INPUT            : pPortEntry - pointer to port specific information  
 *                       pEventInfo - pointer to the event information      
 *                       u4MessageType - Type of message denoting which 
 *                       event has occured
 *                                                                          
 *    OUTPUT           : None.                                              
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE                        
 *                                                                          
*******************************************************************************/
PRIVATE INT4
EoamCltEvtCheckAndTransmit (tEoamPortInfo * pPortEntry,
                            tEoamThresEventInfo * pEventInfo,
                            UINT4 u4MessageType)
{
    INT4                i4Transmit = OSIX_TRUE;

    switch (u4MessageType)
    {
        case EOAM_ERR_SYMBOL_EVENT_TRIG_MSG:
            if (pPortEntry->ErrEventConfigInfo.u1ErrSymPeriodEvNotifEnable
                == EOAM_FALSE)
            {
                EOAM_TRC ((EOAM_LM_TRC, "CLIENT: Local Entity not set"
                           "to intimate symbol error event to peer on"
                           "port %u\n", pPortEntry->u4Port));
                i4Transmit = OSIX_FALSE;
            }
            break;
        case EOAM_ERR_FRAME_EVENT_TRIG_MSG:
            if (pPortEntry->ErrEventConfigInfo.u1ErrFrameEvNotifEnable
                == EOAM_FALSE)
            {
                EOAM_TRC ((EOAM_LM_TRC, "CLIENT: Local Entity not set"
                           "to intimate frame event to peer on port %u\n",
                           pPortEntry->u4Port));
                i4Transmit = OSIX_FALSE;
            }
            break;
        case EOAM_ERR_FRAME_PERIOD_EVENT_TRIG_MSG:
            if (pPortEntry->ErrEventConfigInfo.u1ErrFramePeriodEvNotifEnable
                == EOAM_FALSE)
            {
                EOAM_TRC ((EOAM_LM_TRC, "CLIENT: Local Entity not set"
                           "to intimate frame period event to peer on"
                           "port %u\n", pPortEntry->u4Port));
                i4Transmit = OSIX_FALSE;
            }
            break;
        case EOAM_ERR_FRAME_SEC_EVENT_TRIG_MSG:
            if (pPortEntry->ErrEventConfigInfo.u1ErrFrameSecsEvNotifEnable
                == EOAM_FALSE)
            {
                EOAM_TRC ((EOAM_LM_TRC, "CLIENT: Local Entity not set"
                           "to intimate errored seconds event to peer on"
                           "port %u\n", pPortEntry->u4Port));
                i4Transmit = OSIX_FALSE;
            }
            break;
        default:
            EOAM_TRC ((ALL_FAILURE_TRC | EOAM_LM_TRC,
                       "CLIENT: Invalid type of message type received for"
                       "port %u\n ", pPortEntry->u4Port));
            return OSIX_FAILURE;
    }
    if (i4Transmit == OSIX_TRUE)
    {
        EoamCltEvtPduTransmit (pPortEntry, pEventInfo);
    }
    EOAM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*******************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamCltEvtPduTransmit 
 *                                                                          
 *    DESCRIPTION      : This function sends a request to the control
 *                       module to transmit the Event Notification PDU
 *                                                                          
 *    INPUT            : pPortEntry - pointer to port specific information  
 *                       pEventInfo - pointer to the event information      
 *                                                                          
 *    OUTPUT           : None.                                              
 *                                                                          
 *    RETURNS          : None. 
 *                                                                          
*******************************************************************************/
PRIVATE VOID
EoamCltEvtPduTransmit (tEoamPortInfo * pPortEntry,
                       tEoamThresEventInfo * pEventInfo)
{
    tEoamPduHeader      PduHeader;
    tEoamPduData        PduData;
    tEoamEnaPortInfo   *pActivePortInfo = NULL;

    EOAM_TRC_FN_ENTRY ();
    MEMSET (&PduHeader, 0, sizeof (tEoamPduHeader));
    MEMSET (&PduData, 0, sizeof (tEoamPduData));

    switch (pEventInfo->u1EventType)
    {
        case EOAM_ERRORED_SYMBOL_EVENT_TLV:
            MEMCPY (&(PduData.EvtPduData.ErrSymblPrdEvnt), pEventInfo,
                    sizeof (tEoamThresEventInfo));
            break;
        case EOAM_ERRORED_FRAME_EVENT_TLV:
            MEMCPY (&(PduData.EvtPduData.ErrFrmEvnt), pEventInfo,
                    sizeof (tEoamThresEventInfo));
            break;
        case EOAM_ERRORED_FRAME_PERIOD_EVENT_TLV:
            MEMCPY (&(PduData.EvtPduData.ErrFrmPrdEvnt), pEventInfo,
                    sizeof (tEoamThresEventInfo));
            break;
        case EOAM_ERRORED_FRAME_SECONDS_EVENT_TLV:
            MEMCPY (&(PduData.EvtPduData.ErrFrmSecSumryEvnt), pEventInfo,
                    sizeof (tEoamThresEventInfo));
            break;
        default:
            EOAM_TRC ((EOAM_LM_TRC, "CLIENT: Invalid Event Notification "
                       "Type %u\n ", pEventInfo->u1EventType));
            break;
    }

    pActivePortInfo = pPortEntry->pEoamEnaPortInfo;
    PduData.EvtPduData.u2SeqNum = ++(pActivePortInfo->u2TxSeqNum);
    PduData.EvtPduData.u1EvtType = EOAM_THRESH_EVENT;
    PduData.EvtPduData.u1EventType = pEventInfo->u1EventType;
    EoamCtlTxFormPduHeader (pPortEntry, EOAM_EVENT_NOTIFICATION, &PduHeader);

    EoamCltEvtTxEvent (pPortEntry, &PduHeader, &PduData);
    EOAM_TRC_FN_EXIT ();
    return;
}

/*******************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamCltEvtSendOrgSpecificEvt 
 *                                                                          
 *    DESCRIPTION      : This function is called when an Event Notification
 *                       OAMPDU with organization specific Event TLV is to
 *                       be sent to the peer. It sends an appropriate 
 *                       OAMPDU request to the Control Module
 *                                                                          
 *    INPUT            : pPortEntry - pointer to port information structure 
 *                       pOrgSpecData - pointer to Organization specific    
 *                       information                                        
 *                                                                          
 *    OUTPUT           : None.                                              
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE                        
 *                                                                          
*******************************************************************************/
PUBLIC INT4
EoamCltEvtSendOrgSpecificEvt (tEoamPortInfo * pPortEntry,
                              tEoamExtData * pOrgSpecData)
{
    tEoamEnaPortInfo   *pActivePortInfo = NULL;
    tEoamEventLogEntry *pLogEntry = NULL;
    tEoamCallbackInfo   CallbackInfo;
    tEoamPduHeader      PduHeader;
    tEoamPduData        PduData;
    tOsixSysTime        CurrentTime;

    EOAM_TRC_FN_ENTRY ();
    MEMSET (&CallbackInfo, 0, sizeof (tEoamCallbackInfo));
    MEMSET (&CurrentTime, 0, sizeof (tOsixSysTime));

    MEMSET (&PduHeader, 0, sizeof (tEoamPduHeader));
    MEMSET (&PduData, 0, sizeof (tEoamPduData));

    if (gEoamGlobalInfo.u1ModuleStatus != EOAM_ENABLED)
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "EOAM is globally DISABLED!!!\n"));
        return OSIX_FAILURE;
    }

    if (pPortEntry == NULL)
    {
        return OSIX_SUCCESS;
    }

    FSAP_ASSERT (pOrgSpecData != NULL);
    pActivePortInfo = pPortEntry->pEoamEnaPortInfo;
    if (pActivePortInfo == NULL)
    {
        EOAM_TRC ((ALL_FAILURE_TRC | EOAM_CRITICAL_TRC,
                   "CLIENT: EOAM not enabled on port %u\n",
                   pPortEntry->u4Port));
        return OSIX_SUCCESS;
    }
    /* Get the pointer to the log entry and update with the event information */
    pLogEntry = EoamUtilGetLogEntryToBeFilled (pPortEntry);
    if (pLogEntry != NULL)
    {
        /* Get the System Time stamp */
        OsixGetSysTime (&CurrentTime);
        /* Fill up the Event Log structure appropriately */
        MEMCPY (pLogEntry->au1EventLogOui, pPortEntry->RemoteInfo.au1VendorOui,
                EOAM_OUI_LENGTH);
        pLogEntry->EventLogInfo.u4EventLogTimestamp = CurrentTime;
        pLogEntry->u4EventLogIndex = ++(pActivePortInfo->u4LogIndexCount);
        pLogEntry->u1EventLogLocation = EOAM_LOCAL_ENTRY;

        pLogEntry->u4EventLogType = EOAM_ORG_SPEC_LINK_EVENT;
        EoamPopulateOrgEvtLogEntry (pOrgSpecData, pLogEntry);
    }

    /* Fill up the callback info structure */
    CallbackInfo.u4Port = pPortEntry->u4Port;
    CallbackInfo.u4EventType = EOAM_NOTIFY_ORG_SPECIFIC_EVENT;
    CallbackInfo.u2Location = EOAM_LOCAL_ENTRY;
    /* Call a function to notify all registered modules */
    EoamMainNotifyModules (&CallbackInfo);

    MEMCPY (&(PduData.EvtPduData.OrgEvtData),
            pOrgSpecData, sizeof (tEoamExtData));
    PduData.EvtPduData.u2SeqNum = ++(pActivePortInfo->u2TxSeqNum);
    PduData.EvtPduData.u1EvtType = EOAM_ORG_SPEC_EVENT;
    EoamCtlTxFormPduHeader (pPortEntry, EOAM_EVENT_NOTIFICATION, &PduHeader);
    EoamCltEvtTxEvent (pPortEntry, &PduHeader, &PduData);

    EOAM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*******************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamCltEvtTxEvent 
 *                                                                          
 *    DESCRIPTION      : This function calls the EOAM Control to transmit
 *                       Event Notification OAMPDUs for the number of
 *                       times defined by the Resend count
 *                                                                          
 *    INPUT            : pPortEntry - pointer to port information structure 
 *                       pPduHeader - pointer to EOAM Pdu Header    
 *                       pPduData - pointer to EOAM Pdu Data                   
 *                                                                          
 *    OUTPUT           : None.                                              
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE                        
 *                                                                          
 ******************************************************************************/
PRIVATE VOID
EoamCltEvtTxEvent (tEoamPortInfo * pPortEntry,
                   tEoamPduHeader * pPduHeader, tEoamPduData * pPduData)
{
    UINT4               u4Count = 0;

    EOAM_TRC_FN_ENTRY ();

    /* OAMPDUs other than Information PDUs can be sent out only when
     * discovery has settled down */
    if (pPortEntry->LocalInfo.u2EoamOperStatus != EOAM_OPER_OPERATIONAL)
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "CLIENT: Cannot send out Event "
                   "Notification OAMPDU when discovery has not settled down "
                   "on port %u\n", pPortEntry->u4Port));
        return;
    }
    for (u4Count = 0; u4Count <= gEoamGlobalInfo.u4ErrEventResendCount;
         u4Count++)
    {
        if (EoamCtlTxPduRequest (pPortEntry, pPduHeader, pPduData) ==
            OSIX_SUCCESS)
        {
            /* Increment the Event Notification OAMPDU Tx Counter */
            if (u4Count == 0)
            {
                pPortEntry->PduStats.u4UniqueEventNotificationTx++;
            }
            else
            {
                pPortEntry->PduStats.u4DuplicateEventNotificationTx++;
            }
        }
    }
    EOAM_TRC_FN_EXIT ();
    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  emcltevt.c                     */
/*-----------------------------------------------------------------------*/
