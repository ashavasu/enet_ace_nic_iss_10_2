/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emutil.c,v 1.11 2012/02/29 09:14:49 siva Exp $
 *
 * Description: This file contains the utility procedures used by EOAM 
 *              module.
 *********************************************************************/
#include "eminc.h"

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamUtilGetPortEntry 
 *                                                                          
 *    DESCRIPTION      : This is function returns the pointer to the port entry
 *                       from the global structure.
 *
 *    INPUT            : u4PortIndex - Index of the port for which the entry is
 *                                     required,
 *                       ppEoamPortEntry - Address of pointer to port entry.
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamUtilGetPortEntry (UINT4 u4PortIndex, tEoamPortInfo ** ppEoamPortEntry)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    EOAM_TRC_FN_ENTRY ();

    if ((u4PortIndex < EOAM_MIN_PORTS) || (u4PortIndex > EOAM_MAX_PORTS))
    {
        *ppEoamPortEntry = NULL;
        return OSIX_FAILURE;
    }

    *ppEoamPortEntry = gEoamGlobalInfo.apEoamPortEntry[u4PortIndex - 1];
    if (*ppEoamPortEntry == NULL)
    {
        i4RetVal = OSIX_FAILURE;
    }

    EOAM_TRC_FN_EXIT ();

    return (i4RetVal);
}

/******************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamUtilRBTreeDescCmp 
 *                                                                          
 *    DESCRIPTION      : This function compares the key of the incoming node 
 *                       with the nodes already present to find the exact 
 *                       location for the new node.
 *                                                                          
 *    INPUT            : *pVarReqNode - pointer to node already present
 *                       *pVarReqIn - pointer to incoming node
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : -1, 1 or 0 denoting the direction
 *                                                                          
*******************************************************************************/
PUBLIC INT4
EoamUtilRBTreeDescCmp (tRBElem * pVarReqNode, tRBElem * pVarReqIn)
{
    EOAM_TRC_FN_ENTRY ();
    /* Comapare the branch */
    if ((((tEoamVarReqRBTNode *) pVarReqNode)->u1Branch)
        < (((tEoamVarReqRBTNode *) pVarReqIn)->u1Branch))
    {
        return -1;
    }
    else if ((((tEoamVarReqRBTNode *) pVarReqNode)->u1Branch)
             > (((tEoamVarReqRBTNode *) pVarReqIn)->u1Branch))
    {
        return 1;
    }

    /* Comapare the leaf */
    if ((((tEoamVarReqRBTNode *) pVarReqNode)->u2Leaf)
        < (((tEoamVarReqRBTNode *) pVarReqIn)->u2Leaf))
    {
        return -1;
    }
    else if ((((tEoamVarReqRBTNode *) pVarReqNode)->u2Leaf)
             > (((tEoamVarReqRBTNode *) pVarReqIn)->u2Leaf))
    {
        return 1;
    }
    EOAM_TRC_FN_EXIT ();
    return 0;
}

/******************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamUtilRBTFreeNodeFn 
 *                                                                          
 *    DESCRIPTION      : This function releases the memory allocated to each
 *                       node of the RBTree
 *                                                                          
 *    INPUT            : pRBElem - pointer to the node to be freed. 
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *                                                                          
*******************************************************************************/
PUBLIC INT4
EoamUtilRBTFreeNodeFn (tRBElem * pRBElem, UINT4 u4Arg)
{
    EOAM_TRC_FN_ENTRY ();

    UNUSED_PARAM (u4Arg);

    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (gEoamGlobalInfo.VarContainerPoolId,
                            (UINT1 *) pRBElem);
        pRBElem = NULL;
    }
    EOAM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*******************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamUtilGetLogEntryToBeFilled                          
 *                                                                          
 *    DESCRIPTION      : This function returns the pointer to the Log Entry 
 *                       which is to be filled. If memory is not allocated, 
 *                       Memory allocation is done.                         
 *                                                                          
 *    INPUT            : pPortInfo - pointer to port information structure  
 *                                                                          
 *    OUTPUT           : None.                                              
 *                                                                          
 *    RETURNS          : pointer to tEoamEventLogEntry structure            
 *                                                                          
*******************************************************************************/
PUBLIC tEoamEventLogEntry *
EoamUtilGetLogEntryToBeFilled (tEoamPortInfo * pPortInfo)
{
    tEoamEventLogEntry *pLogEntry = NULL;

    EOAM_TRC_FN_ENTRY ();

    if (pPortInfo->u1LogPosToBeFilled >= EOAM_MAX_LOG_ENTRIES)
    {
        return NULL;
    }
    pLogEntry = pPortInfo->apEventLogEntry[pPortInfo->u1LogPosToBeFilled];
    /* Allocate Memory from the Log Memory Pool if not allocated before. 
     * If already allocated, just overwrite the values.*/
    if (pLogEntry == NULL)
    {
        if ((pLogEntry = (tEoamEventLogEntry *) (MemAllocMemBlk
                                                 (gEoamGlobalInfo.
                                                  EvtLogPoolId))) == NULL)
        {
            EOAM_TRC ((ALL_FAILURE_TRC, "CLIENT: Memory Allocation for log"
                       "entry failed for port %u ", pPortInfo->u4Port));
            return NULL;
        }
        pPortInfo->apEventLogEntry[pPortInfo->u1LogPosToBeFilled] = pLogEntry;
    }
    /* Increment the Log Position to be filled, which will be used for the next
     * log entry. If it reaches the maximum level,the log position should again
     * start from the first position i.e, array index = 0 */
    if (++(pPortInfo->u1LogPosToBeFilled) == EOAM_MAX_LOG_ENTRIES)
    {
        pPortInfo->u1LogPosToBeFilled = 0;
    }
    MEMSET (pLogEntry, 0, sizeof (tEoamEventLogEntry));
    EOAM_TRC_FN_EXIT ();
    return pLogEntry;
}

/*****************************************************************************
 *
 *    FUNCTION NAME    : EoamUtilCopyInfo
 *
 *    DESCRIPTION      : This function allocates memory from EOAM buddy memory
 *                       and copies the contents to this memory from the
 *                       given pointer
 *
 *    INPUT            : pu1Src - pointer to the data which should be copied
 *                       u4Length - Lenght until which data should be copied
 *
 *    OUTPUT           : ppu1Dest - pointer to the allocated Buddy memory where
 *                               the contents are copied
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
*******************************************************************************/
PUBLIC INT4
EoamUtilCopyInfo (UINT1 **ppu1Dest, UINT1 *pu1Src, UINT4 u4Length)
{
    UINT1              *pu1Temp = NULL;
    UINT4               u4BuddyLength = 0;

    EOAM_TRC_FN_ENTRY ();
    u4BuddyLength = (u4Length < EOAM_MIN_BLOCK_SIZE) ? EOAM_MIN_BLOCK_SIZE :
        u4Length;
    if ((pu1Temp = EOAM_BUDDY_ALLOC (u4BuddyLength)) == NULL)
    {
        EOAM_TRC ((ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                   "EoamUtilCopyInfo :Buddy memory allocation failed\r\n"));
        return OSIX_FAILURE;
    }
    MEMSET (pu1Temp, 0, u4BuddyLength);
    MEMCPY (pu1Temp, pu1Src, u4Length);
    *ppu1Dest = pu1Temp;
    EOAM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  emutil.c                       */
/*-----------------------------------------------------------------------*/
