/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
* $Id: emtrap.c,v 1.8 2014/02/13 12:08:52 siva Exp $
 *
 * Description: This file contains OAM Trap related Functions
 *****************************************************************************/
#include "eminc.h"

#include "snmctdfs.h"
#include "snmcport.h"
#include "stdeoam.h"

static INT1         ai1TempBuffer[SNMP_MAX_OID_LENGTH];
#ifdef SNMP_3_WANTED
static UINT4        u4CurrSysTime = 0;
static UINT4        u4PrevSysTime = 0;
#endif

/******************************************************************************
* Function      : EoamMakeObjIdFromDotNew                                      
* Input         : pi1TextStr
* Output        : NONE
* Description   : This function is used to get object id from given object
*                 and convert those object id to OID List.
* Returns       : pOidPtr or NULL
*******************************************************************************/

tSNMP_OID_TYPE     *
EoamMakeObjIdFromDotNew (INT1 *pi1TextStr)
{
    tSNMP_OID_TYPE     *pOidPtr;
    INT1               *pi1TempPtr, *pi1DotPtr;
    UINT1               u1Index;
    UINT1               u1DotCount;

    /* see if there is an alpha descriptor at begining */
    if (isalpha (*pi1TextStr))
    {
        pi1DotPtr = (INT1 *) STRCHR ((INT1 *) pi1TextStr, '.');

        /* if no dot, point to end of string */
        if (pi1DotPtr == NULL)
        {
            pi1DotPtr = pi1TextStr + STRLEN ((INT1 *) pi1TextStr);
        }
        pi1TempPtr = pi1TextStr;

        for (u1Index = 0; ((pi1TempPtr < pi1DotPtr) && (u1Index < 255));
             u1Index++)
        {
            ai1TempBuffer[u1Index] = *pi1TempPtr++;
        }
        ai1TempBuffer[u1Index] = '\0';

        for (u1Index = 0; ((u1Index < (sizeof (stdeoam_mib_oid_table) /
                                       sizeof (struct MIB_OID))) &&
                           (stdeoam_mib_oid_table[u1Index].pName != NULL));
             u1Index++)
        {
            if ((STRCMP
                 (stdeoam_mib_oid_table[u1Index].pName,
                  (INT1 *) ai1TempBuffer) == 0)
                && (STRLEN ((INT1 *) ai1TempBuffer) ==
                    STRLEN (stdeoam_mib_oid_table[u1Index].pName)))
            {
                STRNCPY ((INT1 *) ai1TempBuffer,
                         stdeoam_mib_oid_table[u1Index].pNumber,
                         STRLEN (stdeoam_mib_oid_table[u1Index].pNumber) + 1);
                break;
            }
        }
        /*ai1TempBuffer[u1Index] = '\0'; */

        if (u1Index <
            (sizeof (stdeoam_mib_oid_table) / sizeof (struct MIB_OID)))
        {
            if (stdeoam_mib_oid_table[u1Index].pName == NULL)
            {
                return (NULL);
            }
        }
        /* now concatenate the non-alpha part to the begining */
        STRNCAT ((INT1 *) ai1TempBuffer, (INT1 *) pi1DotPtr,
                 STRLEN (pi1DotPtr) + 1);
    }
    else
    {                            /* is not alpha, so just copy into ai1TempBuffer */
        STRNCPY ((INT1 *) ai1TempBuffer, (INT1 *) pi1TextStr,
                 STRLEN (pi1TextStr) + 1);
    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    u1DotCount = 0;
    for (u1Index = 0; ai1TempBuffer[u1Index] != '\0'; u1Index++)
    {
        if (ai1TempBuffer[u1Index] == '.')
        {
            u1DotCount++;
        }
    }

    /* The object specified may be either Tabular or Scalar
     * Object. Tabular Objects needs index to be added at the
     * end. So, allocating for Maximum OID Length
     * */
    if ((pOidPtr = alloc_oid (SNMP_MAX_OID_LENGTH)) == NULL)
    {
        return (NULL);
    }
    pOidPtr->u4_Length = u1DotCount + 1;

    /* now we convert number.number.... strings */
    pi1TempPtr = ai1TempBuffer;
    for (u1Index = 0; u1Index < u1DotCount + 1; u1Index++)
    {
        if ((pi1TempPtr = (INT1 *) EoamParseSubIdNew
             ((UINT1 *) pi1TempPtr, &(pOidPtr->pu4_OidList[u1Index]))) == NULL)
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }

        if (*pi1TempPtr == '.')
        {
            pi1TempPtr++;        /* to skip over dot */
        }
        else if (*pi1TempPtr != '\0')
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }
    }                            /* end of for loop */

    return (pOidPtr);
}

/******************************************************************************
* Function : EoamParseSubIdNew
* Input    : ppu1TempPtr
* Output   : value of ppu1TempPtr
* Returns  : OSIX_SUCCESS or OSIX_FAILURE
*******************************************************************************/

UINT1              *
EoamParseSubIdNew (UINT1 *pu1TempPtr, UINT4 *pu4Value)
{
    UINT4               u4Value = 0;
    UINT1              *pu1Tmp = NULL;

    for (pu1Tmp = pu1TempPtr; (((*pu1Tmp >= '0') && (*pu1Tmp <= '9')) ||
                               ((*pu1Tmp >= 'a') && (*pu1Tmp <= 'f')) ||
                               ((*pu1Tmp >= 'A') && (*pu1Tmp <= 'F')));
         pu1Tmp++)
    {
        u4Value = (u4Value * 10) + (*pu1Tmp & 0xf);
    }

    if (pu1TempPtr == pu1Tmp)
    {
        pu1Tmp = NULL;
    }
    *pu4Value = u4Value;
    return pu1Tmp;
}

/*******************************************************************************
 *                                                                          
 *    FUNCTION NAME    :  EoamNotifyEventtTrap 
 *                                                                          
 *    DESCRIPTION      :  This function is called when any threshold event or 
 *                        Non-threshold event occurs in EOAM. This function forms
 *                        the SNMP Trap VbLIst and Notify to the SNMP.
 *                                                                          
 *    INPUT            :  pTrapInfo - pointer to Trap information
 *                        u4IfIndex - Interface Index
 *                        u4TrapId  - Notification ID as specified in STD MIB.
 *    OUTPUT           :  None. 
 *                                                                          
 *    RETURNS          :  VOID                   
 *                                                                          
*******************************************************************************/
VOID
EoamNotifyEventTrap (tEoamEventLogEntry * pTrapInfo,
                     UINT4 u4IfIndex, UINT4 u4TrapId)
{

#ifdef SNMP_3_WANTED

    tSNMP_VAR_BIND     *pVbList = NULL, *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    UINT4               au4snmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
    UINT4               au4EoamTrapOid[] = { 1, 3, 6, 1, 2, 1, 158, 0 };
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    UINT1               au1Buf[SNMP_MAX_OID_LENGTH];
    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    EOAM_TRC_FN_ENTRY ();

    /* Notification PDU should not be sent more than once, hence 
     * check is added not to send more than one notification 
     * in a second*/
    u4CurrSysTime = OsixGetSysUpTime ();
    if ((u4CurrSysTime - u4PrevSysTime) < 1)
    {
        /* Notification should not be sent more than once per second */
        u4PrevSysTime = u4CurrSysTime;
        return;
    }
    u4PrevSysTime = u4CurrSysTime;

    /* Allocate the Memory for EOAM Trap OID to form the VbList */
    pEnterpriseOid = alloc_oid (SNMP_TRAP_OID_LEN);
    if (pEnterpriseOid == NULL)
    {
        return;
    }
    MEMCPY (pEnterpriseOid->pu4_OidList, au4EoamTrapOid,
            sizeof (au4EoamTrapOid));
    pEnterpriseOid->u4_Length = sizeof (au4EoamTrapOid) / sizeof (UINT4);
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = u4TrapId;

    /* Allocate the Memory for SNMP Trap OID to form the VbList */
    pSnmpTrapOid = alloc_oid (sizeof (au4snmpTrapOid) / sizeof (UINT4));
    if (pSnmpTrapOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }
    MEMCPY (pSnmpTrapOid->pu4_OidList, au4snmpTrapOid, sizeof (au4snmpTrapOid));
    pSnmpTrapOid->u4_Length = sizeof (au4snmpTrapOid) / sizeof (UINT4);
    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
        (pSnmpTrapOid, SNMP_DATA_TYPE_OBJECT_ID,
         0L, 0, NULL, pEnterpriseOid, SnmpCnt64Type);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        return;
    }

    pStartVb = pVbList;

    SPRINTF ((char *) au1Buf, EOAM_MIB_EVT_LOG_TIMESTAMP);
    pOid = EoamMakeObjIdFromDotNew ((INT1 *) au1Buf);
    if (pOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }
    /* Concatenate the OID with the ifIndex value - table indexing */
    pOid->pu4_OidList[pOid->u4_Length++] = u4IfIndex;

    pVbList->pNextVarBind = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
        (pOid, SNMP_DATA_TYPE_INTEGER,
         0, pTrapInfo->EventLogInfo.u4EventLogTimestamp,
         NULL, NULL, SnmpCnt64Type);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;
    SPRINTF ((char *) au1Buf, EOAM_MIB_EVT_LOG_OUI);
    pOid = EoamMakeObjIdFromDotNew ((INT1 *) au1Buf);
    if (pOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    /* Concatenate the OID with the ifIndex value - table indexing */
    pOid->pu4_OidList[pOid->u4_Length++] = u4IfIndex;

    pOstring = SNMP_AGT_FormOctetString ((UINT1 *) &pTrapInfo->au1EventLogOui,
                                         (INT4) EOAM_OUI_LENGTH);
    if (pOstring == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        SNMP_FreeOid (pOid);
        return;
    }
    /* For the SNMP Variable Binding of OID and associated Value
     * for ifIndex */
    pVbList->pNextVarBind = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                                     SNMP_DATA_TYPE_OCTET_PRIM,
                                                                     0, 0,
                                                                     pOstring,
                                                                     NULL,
                                                                     SnmpCnt64Type);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;
    SPRINTF ((char *) au1Buf, EOAM_MIB_EVT_LOG_TYPE);
    pOid = EoamMakeObjIdFromDotNew ((INT1 *) au1Buf);
    if (pOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }
    /* Concatenate the OID with the ifIndex value - table indexing */
    pOid->pu4_OidList[pOid->u4_Length++] = u4IfIndex;

    pVbList->pNextVarBind = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
        (pOid, SNMP_DATA_TYPE_INTEGER,
         0, pTrapInfo->u4EventLogType, NULL, NULL, SnmpCnt64Type);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;
    SPRINTF ((char *) au1Buf, EOAM_MIB_EVT_LOG_LOCATION);
    pOid = EoamMakeObjIdFromDotNew ((INT1 *) au1Buf);
    if (pOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }
    /* Concatenate the OID with the ifIndex value - table indexing */
    pOid->pu4_OidList[pOid->u4_Length++] = u4IfIndex;

    pVbList->pNextVarBind = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                                     SNMP_DATA_TYPE_INTEGER,
                                                                     0,
                                                                     pTrapInfo->
                                                                     u1EventLogLocation,
                                                                     NULL, NULL,
                                                                     SnmpCnt64Type);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    switch (pTrapInfo->u4EventLogType)
    {
        case EOAM_CRITICAL_EVENT:
            /* Intentional fallthrough */
        case EOAM_DYING_GASP:
            /* Intentional fallthrough */
        case EOAM_LINK_FAULT:

            /* Start of Next Variable Binding */
            pVbList = pVbList->pNextVarBind;

            SPRINTF ((char *) au1Buf, EOAM_MIB_EVT_LOG_EVT_TOTAL);
            pOid = EoamMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                /* Release the memory Assigned for Entrerprise Oid */
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                return;
            }
            /* Concatenate the OID with the ifIndex value - table indexing */
            pOid->pu4_OidList[pOid->u4_Length++] = u4IfIndex;

            pVbList->pNextVarBind = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid, SNMP_DATA_TYPE_INTEGER,
                 0, pTrapInfo->u4NonThresLogEvtTotal,
                 NULL, NULL, SnmpCnt64Type);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                return;
            }
            break;
        case EOAM_ERRORED_SYMBOL_EVENT:
            /* Intentional fallthrough */
        case EOAM_ERRORED_FRAME_PERIOD_EVENT:
            /* Intentional fallthrough */
        case EOAM_ERRORED_FRAME_EVENT:
            /* Intentional fallthrough */
        case EOAM_ERRORED_FRAME_SECONDS_EVENT:
            /* Start of Next Variable Binding */
            pVbList = pVbList->pNextVarBind;

            SPRINTF ((char *) au1Buf, EOAM_MIB_EVT_LOG_WIN_HI);
            pOid = EoamMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                /* Release the memory Assigned for Entrerprise Oid */
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                return;
            }
            /* Concatenate the OID with the ifIndex value - table indexing */
            pOid->pu4_OidList[pOid->u4_Length++] = u4IfIndex;

            pVbList->pNextVarBind = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid, SNMP_DATA_TYPE_INTEGER,
                 0, FSAP_U8_FETCH_HI
                 (&(pTrapInfo->EventLogInfo.u8EventWindow)),
                 NULL, NULL, SnmpCnt64Type);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                return;
            }
            pVbList = pVbList->pNextVarBind;

            SPRINTF ((char *) au1Buf, EOAM_MIB_EVT_LOG_WIN_LO);
            pOid = EoamMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                /* Release the memory Assigned for Entrerprise Oid */
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                return;
            }
            /* Concatenate the OID with the ifIndex value - table indexing */
            pOid->pu4_OidList[pOid->u4_Length++] = u4IfIndex;

            pVbList->pNextVarBind = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid, SNMP_DATA_TYPE_INTEGER,
                 0, FSAP_U8_FETCH_LO
                 (&(pTrapInfo->EventLogInfo.u8EventWindow)),
                 NULL, NULL, SnmpCnt64Type);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                return;
            }
            pVbList = pVbList->pNextVarBind;

            SPRINTF ((char *) au1Buf, EOAM_MIB_EVT_LOG_THRES_HI);
            pOid = EoamMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                /* Release the memory Assigned for Entrerprise Oid */
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                return;
            }
            /* Concatenate the OID with the ifIndex value - table indexing */
            pOid->pu4_OidList[pOid->u4_Length++] = u4IfIndex;

            pVbList->pNextVarBind = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid, SNMP_DATA_TYPE_INTEGER,
                 0, FSAP_U8_FETCH_HI
                 (&(pTrapInfo->EventLogInfo.
                    u8EventThreshold)), NULL, NULL, SnmpCnt64Type);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                return;
            }
            pVbList = pVbList->pNextVarBind;

            SPRINTF ((char *) au1Buf, EOAM_MIB_EVT_LOG_THRES_LO);
            pOid = EoamMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                /* Release the memory Assigned for Entrerprise Oid */
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                return;
            }
            /* Concatenate the OID with the ifIndex value - table indexing */
            pOid->pu4_OidList[pOid->u4_Length++] = u4IfIndex;

            pVbList->pNextVarBind = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid, SNMP_DATA_TYPE_INTEGER,
                 0, FSAP_U8_FETCH_LO (&(pTrapInfo->EventLogInfo.
                                        u8EventThreshold)),
                 NULL, NULL, SnmpCnt64Type);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                return;
            }
            pVbList = pVbList->pNextVarBind;

            SPRINTF ((char *) au1Buf, EOAM_MIB_EVT_LOG_VALUE);
            pOid = EoamMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                /* Release the memory Assigned for Entrerprise Oid */
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                return;
            }
            /* Concatenate the OID with the ifIndex value - table indexing */
            pOid->pu4_OidList[pOid->u4_Length++] = u4IfIndex;
            SnmpCnt64Type.msn =
                FSAP_U8_FETCH_HI (&(pTrapInfo->EventLogInfo.u8EventValue));
            SnmpCnt64Type.lsn =
                FSAP_U8_FETCH_LO (&(pTrapInfo->EventLogInfo.u8EventValue));

            pVbList->pNextVarBind = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid, SNMP_DATA_TYPE_COUNTER64,
                 0, 0, NULL, NULL, SnmpCnt64Type);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                return;
            }
            pVbList = pVbList->pNextVarBind;

            SPRINTF ((char *) au1Buf, EOAM_MIB_EVT_LOG_RUNNING_TOTAL);
            pOid = EoamMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                /* Release the memory Assigned for Entrerprise Oid */
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                return;
            }
            /* Concatenate the OID with the ifIndex value - table indexing */
            pOid->pu4_OidList[pOid->u4_Length++] = u4IfIndex;
            SnmpCnt64Type.msn =
                FSAP_U8_FETCH_HI (&
                                  (pTrapInfo->EventLogInfo.
                                   u8ErrorRunningTotal));
            SnmpCnt64Type.lsn =
                FSAP_U8_FETCH_LO (&
                                  (pTrapInfo->EventLogInfo.
                                   u8ErrorRunningTotal));

            pVbList->pNextVarBind = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid, SNMP_DATA_TYPE_COUNTER64,
                 0, 0, NULL, NULL, SnmpCnt64Type);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                return;
            }
            pVbList = pVbList->pNextVarBind;

            SPRINTF ((char *) au1Buf, EOAM_MIB_EVT_LOG_EVT_TOTAL);
            pOid = EoamMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                /* Release the memory Assigned for Entrerprise Oid */
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                return;
            }
            /* Concatenate the OID with the ifIndex value - table indexing */
            pOid->pu4_OidList[pOid->u4_Length++] = u4IfIndex;

            pVbList->pNextVarBind = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid, SNMP_DATA_TYPE_INTEGER,
                 0, pTrapInfo->EventLogInfo.
                 u4EventRunningTotal, NULL, NULL, SnmpCnt64Type);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                return;
            }
            break;
        default:
            break;
    }

    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;

    /* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);
#else
    UNUSED_PARAM (pTrapInfo);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4TrapId);
#endif
    EOAM_TRC_FN_EXIT ();
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  emtrap.c                       */
/*-----------------------------------------------------------------------*/
