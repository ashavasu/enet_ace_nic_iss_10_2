/*******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emcltpdu.c,v 1.5 2011/08/01 06:13:52 siva Exp $
 *
 * Description: This file conatains the PDU branching function of the EOAM 
 *              client module. 
 ******************************************************************************/
#include "eminc.h"

/*******************************************************************************
 *     FUNCTION NAME    :  EoamCltPduProcess
 *                                                                           
 *     DESCRIPTION      :  This function calls appropriate PDU Handler       
 *                         functions based on the type of OAMPDU received.   
 *                                                                           
 *     INPUT            :  pPortInfo - pointer to port specific info         
 *                         pHeader - pointer to PDU header                   
 *                         pData - pointer to PDU Data                       
 *                                                                           
 *     OUTPUT           :  None.                                             
 *                                                                           
 *     RETURNS          :  OSIX_SUCCESS / OSIX_FAILURE                       
 ******************************************************************************/
PUBLIC INT4
EoamCltPduProcess (tEoamPortInfo * pPortInfo,
                   tEoamPduHeader * pHeader, tEoamPduData * pData)
{

    UINT1               u1IsFnsSupported = EOAM_FALSE;
    UINT1               u1SemStateChange = OSIX_FALSE;

    EOAM_TRC_FN_ENTRY ();

    FSAP_ASSERT (pPortInfo != NULL);
    FSAP_ASSERT (pHeader != NULL);
    FSAP_ASSERT (pData != NULL);
    FSAP_ASSERT (pPortInfo->pEoamEnaPortInfo != NULL);

    if (EoamCltInfPduParseHeader (pPortInfo, pHeader, &u1SemStateChange)
        != OSIX_SUCCESS)
    {
        EOAM_TRC ((EOAM_CRITICAL_TRC,
                   "CLIENT: EoamCltInfPduParseHeader failed on port %u\n",
                   pPortInfo->u4Port));
        return OSIX_FAILURE;
    }

    if (pHeader->u1Code != EOAM_INFO)
    {
        if (EoamCltInfHandleDiscovery (pPortInfo, OSIX_FALSE, u1SemStateChange)
            != OSIX_SUCCESS)
        {
            EOAM_TRC ((ALL_FAILURE_TRC | EOAM_CRITICAL_TRC | EOAM_DISCOVERY_TRC,
                       "CLIENT: EoamCltInfHandleDiscovery failed on port %u\n",
                       pPortInfo->u4Port));
            return OSIX_FAILURE;
        }

    }

    switch (pHeader->u1Code)
    {
        case EOAM_INFO:
            EoamCltInfPduProcess (pPortInfo, u1SemStateChange, pData);
            u1IsFnsSupported = EOAM_TRUE;
            break;
        case EOAM_EVENT_NOTIFICATION:
            if (pPortInfo->LocalInfo.u1FnsSupportedBmp & EOAM_EVENT_SUPPORT)
            {
                EoamCltEvtNotifPduProcess (pPortInfo, pData);
                u1IsFnsSupported = EOAM_TRUE;
            }
            break;
        case EOAM_VAR_REQ:
            if (pPortInfo->LocalInfo.u1FnsSupportedBmp & EOAM_VARIABLE_SUPPORT)
            {
                EoamCltReqPduProcess (pPortInfo, pData);
                u1IsFnsSupported = EOAM_TRUE;
            }
            break;
        case EOAM_VAR_RESP:
            EoamCltResPduProcess (pPortInfo, pData);
            u1IsFnsSupported = EOAM_TRUE;
            break;
        case EOAM_LB_CTRL:
            if (pPortInfo->LocalInfo.u1FnsSupportedBmp & EOAM_LOOPBACK_SUPPORT)
            {
                EoamCltLbPduProcess (pPortInfo, pData);
                u1IsFnsSupported = EOAM_TRUE;
            }
            break;
        case EOAM_ORG_SPEC:
            EoamCltOrgSpecPduProcess (pPortInfo, pData);
            u1IsFnsSupported = EOAM_TRUE;
            break;
    }
    if (u1IsFnsSupported == EOAM_FALSE)
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "CLIENT: Received PDU with"
                   "unsupported code on port %u\n", pPortInfo->u4Port));
        pPortInfo->PduStats.u4UnsupportedCodesRx++;
        return OSIX_FAILURE;
    }

    EOAM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  emcltpdu.c                     */
/*-----------------------------------------------------------------------*/
