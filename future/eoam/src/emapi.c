/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emapi.c,v 1.16 2013/11/05 10:57:38 siva Exp $
 *
 * Description: This file contains the procedures called by other modules. 
 *****************************************************************************/
#include "eminc.h"
extern VOID EoamCltInfTriggerLinkFaultMsg PROTO ((tEoamPortInfo *, UINT1));
/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamModuleStart 
 *                                                                          
 *    DESCRIPTION      : This function is an API to start the EOAM module
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamModuleStart (VOID)
{
    EoamLock ();
    if (EoamHandleModuleStart () == OSIX_FAILURE)
    {
        EoamUnLock ();
        return OSIX_FAILURE;
    }
    EoamUnLock ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamModuleShutDown 
 *                                                                          
 *    DESCRIPTION      : This function is an API to shutdown the EOAM module
 *
 *    INPUT            : None.
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EoamModuleShutDown (VOID)
{
    EoamLock ();
    EoamHandleModuleShutDown ();
    EoamUnLock ();
}

/******************************************************************************
 * Function Name      : EoamApiSendEventsToPeer
 *
 * Description        : This function posts a message to EOAM task message
 *                      queue requesting OAM to send out a specific link
 *                      event
 *
 * Input(s)           : u4Port -Interface number
 *                      u2RequestType - Type of Event - Link Fault / Dying
 *                                      Gasp/ Critical event
 *                      u1Status - OSIX_TRUE / OSIX_FALSE
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
EoamApiSendEventsToPeer (UINT4 u4Port, UINT2 u2RequestType, UINT1 u1Status)
{
    tEoamQMsg          *pMsg = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    EOAM_TRC_FN_ENTRY ();

    EOAM_TRC ((EOAM_RFI_TRC,
               "EoamApiSendEventsToPeer: Sending Event on port %d\n", u4Port));

    if (gEoamGlobalInfo.u1SystemCtrlStatus == EOAM_SHUTDOWN)
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "EOAM is not started\n"));
        return OSIX_FAILURE;
    }

    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tEoamQMsg *) (MemAllocMemBlk
                               (gEoamGlobalInfo.QMsgPoolId))) == NULL)
    {
        EOAM_TRC ((BUFFER_TRC | ALL_FAILURE_TRC,
                   "EoamApiSendEventsToPeer: Port %d :"
                   " Intf Msg ALLOC_MEM_BLOCK FAILED\n", u4Port));
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tEoamQMsg));

    pMsg->u4Port = u4Port;
    pMsg->unEoamMsgParam.u4State = u1Status;

    switch (u2RequestType)
    {
        case EOAM_LINK_FAULT:
            pMsg->u4MsgType = EOAM_LINK_FAULT_TRIG_MSG;
            break;

        case EOAM_DYING_GASP:
            pMsg->u4MsgType = EOAM_DYING_GASP_TRIG_MSG;
            break;

        case EOAM_CRITICAL_EVENT:
            pMsg->u4MsgType = EOAM_CRITICAL_EVENT_TRIG_MSG;
            break;

        default:
            EOAM_TRC ((ALL_FAILURE_TRC,
                       "EoamApiSendEventsToPeer: Port %d :"
                       " Invalid Event Type received\n", u4Port));
            i4RetVal = OSIX_FAILURE;
            break;
    }

    i4RetVal = EoamQueEnqAppMsg (pMsg);

    EOAM_TRC_FN_EXIT ();

    return (i4RetVal);
}

/******************************************************************************
 * Function Name      : EoamApiSendLBCommandToPeer
 *
 * Description        : This function posts a message to EOAM task message
 *                      queue requesting OAM to send out a Loopback command
 *
 * Input(s)           : u4Port -Interface number
 *                      u1RequestType - Type of Event - Loopback enable/disable 
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
EoamApiSendLBCommandToPeer (UINT4 u4Port, UINT1 u1RequestType)
{
    tEoamQMsg          *pMsg = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    EOAM_TRC_FN_ENTRY ();

    EOAM_TRC ((EOAM_LOOPBACK_TRC,
               "EoamApiSendLBCommandToPeer: Sending Loopback command on port"
               " %d\n", u4Port));

    if (gEoamGlobalInfo.u1SystemCtrlStatus == EOAM_SHUTDOWN)
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "EOAM is not started\n"));
        return OSIX_FAILURE;
    }

    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tEoamQMsg *) (MemAllocMemBlk
                               (gEoamGlobalInfo.QMsgPoolId))) == NULL)
    {
        EOAM_TRC ((BUFFER_TRC | ALL_FAILURE_TRC,
                   "EoamApiSendLBCommandToPeer: Port %d :"
                   " Intf Msg ALLOC_MEM_BLOCK FAILED\n", u4Port));
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tEoamQMsg));

    pMsg->u4Port = u4Port;

    switch (u1RequestType)
    {
        case EOAM_SEND_LOOPBACK_ENABLE:
            pMsg->u4MsgType = EOAM_LOOPBACK_ENABLE_MSG;
            break;

        case EOAM_SEND_LOOPBACK_DISABLE:
            pMsg->u4MsgType = EOAM_LOOPBACK_DISABLE_MSG;
            break;
        default:
            EOAM_TRC ((ALL_FAILURE_TRC,
                       "EoamApiSendLBCommandToPeer: Port %d :"
                       " Invalid Loopback command received\n", u4Port));
            i4RetVal = OSIX_FAILURE;
            break;
    }

    i4RetVal = EoamQueEnqAppMsg (pMsg);

    EOAM_TRC_FN_EXIT ();

    return (i4RetVal);
}

/******************************************************************************
 * Function Name      : EoamApiSendVarReqToPeer
 *
 * Description        : This function posts a message to EOAM task message
 *                      queue requesting OAM to send out a variable request
 *                      OAMPDU
 *
 * Input(s)           : u4Port -Interface number
 *                      pVarReq - Variable request data
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
EoamApiSendVarReqToPeer (UINT4 u4Port, tEoamExtData * pVarReq)
{
    tEoamQMsg          *pMsg = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    EOAM_TRC_FN_ENTRY ();

    EOAM_TRC ((EOAM_VAR_REQRESP_TRC,
               "EoamApiSendVarReqToPeer: Sending Event on port %d\n", u4Port));

    if (gEoamGlobalInfo.u1SystemCtrlStatus == EOAM_SHUTDOWN)
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "EOAM is not started\n"));
        return OSIX_FAILURE;
    }

    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tEoamQMsg *) (MemAllocMemBlk
                               (gEoamGlobalInfo.QMsgPoolId))) == NULL)
    {
        EOAM_TRC ((BUFFER_TRC | ALL_FAILURE_TRC,
                   "EoamApiSendVarReqToPeer: Port %d :"
                   " Intf Msg ALLOC_MEM_BLOCK FAILED\n", u4Port));
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tEoamQMsg));

    pMsg->u4MsgType = EOAM_VAR_REQ_PDU_TRIG_MSG;
    pMsg->u4Port = u4Port;
    if (EoamUtilCopyInfo (&(pMsg->unEoamMsgParam.EoamExtData.pu1Data),
                          pVarReq->pu1Data, pVarReq->u4DataLen) == OSIX_FAILURE)
    {
        if (MemReleaseMemBlock (gEoamGlobalInfo.QMsgPoolId, (UINT1 *) pMsg)
            != MEM_SUCCESS)
        {

            EOAM_TRC ((BUFFER_TRC | ALL_FAILURE_TRC,
                       "EoamApiSendVarReqToPeer: Port %d:"
                       "MemReleaseMemBlock FAILED\n", u4Port));
        }
        return OSIX_FAILURE;
    }
    pMsg->unEoamMsgParam.EoamExtData.u4DataLen = pVarReq->u4DataLen;

    i4RetVal = EoamQueEnqAppMsg (pMsg);

    EOAM_TRC_FN_EXIT ();

    return (i4RetVal);
}

/******************************************************************************
 * Function Name      : EoamApiSendOrgSpecPduToPeer
 *
 * Description        : This function posts a message to EOAM task message
 *                      queue requesting OAM to send out an organization
 *                      specific OAMPDU
 *
 * Input(s)           : u4Port -Interface number
 *                      pOrgSpecData - Variable request data
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
EoamApiSendOrgSpecPduToPeer (UINT4 u4Port, tEoamExtData * pOrgSpecData)
{
    tEoamQMsg          *pMsg = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    EOAM_TRC_FN_ENTRY ();

    if (gEoamGlobalInfo.u1SystemCtrlStatus == EOAM_SHUTDOWN)
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "EOAM is not started\n"));
        return OSIX_FAILURE;
    }

    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tEoamQMsg *) (MemAllocMemBlk
                               (gEoamGlobalInfo.QMsgPoolId))) == NULL)
    {
        EOAM_TRC ((BUFFER_TRC | ALL_FAILURE_TRC,
                   "EoamApiSendOrgSpecPduToPeer: Port %d :"
                   " Intf Msg ALLOC_MEM_BLOCK FAILED\n", u4Port));
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tEoamQMsg));

    pMsg->u4MsgType = EOAM_ORG_SPEC_PDU_TRIG_MSG;
    pMsg->u4Port = u4Port;

    if (EoamUtilCopyInfo (&(pMsg->unEoamMsgParam.EoamExtData.pu1Data),
                          pOrgSpecData->pu1Data, pOrgSpecData->u4DataLen) ==
        OSIX_FAILURE)
    {
        if (MemReleaseMemBlock (gEoamGlobalInfo.QMsgPoolId, (UINT1 *) pMsg)
            != MEM_SUCCESS)
        {

            EOAM_TRC ((BUFFER_TRC | ALL_FAILURE_TRC,
                       "EoamApiSendOrgSpecPduToPeer: Port %d:"
                       "MemReleaseMemBlock FAILED\n", u4Port));
        }
        return OSIX_FAILURE;
    }
    pMsg->unEoamMsgParam.EoamExtData.u4DataLen = pOrgSpecData->u4DataLen;

    i4RetVal = EoamQueEnqAppMsg (pMsg);

    EOAM_TRC_FN_EXIT ();

    return (i4RetVal);
}

/******************************************************************************
 * Function Name      : EoamApiSendThresholdEvtToPeer
 *
 * Description        : This function posts a message to EOAM task message
 *                      queue requesting OAM to send out an event
 *                      notification OAMPDU. This function will be called by
 *                      link monitoring task when it detects an error event
 *
 * Input(s)           : u4Port -Interface number
 *                      pEventInfo - Error Event Information
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
EoamApiSendThresholdEvtToPeer (UINT4 u4Port, tEoamThresEventInfo * pEventInfo)
{
    tEoamQMsg          *pMsg = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    EOAM_TRC_FN_ENTRY ();

    EOAM_TRC ((EOAM_LM_TRC,
               "EoamApiSendThresholdEvtToPeer: Sending Event on port %d\n",
               u4Port));

    if (gEoamGlobalInfo.u1SystemCtrlStatus == EOAM_SHUTDOWN)
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "EOAM is not started\n"));
        return OSIX_FAILURE;
    }

    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tEoamQMsg *) (MemAllocMemBlk
                               (gEoamGlobalInfo.QMsgPoolId))) == NULL)
    {
        EOAM_TRC ((BUFFER_TRC | ALL_FAILURE_TRC,
                   "EoamApiSendThresholdEvtToPeer: Port %d :"
                   " Intf Msg ALLOC_MEM_BLOCK FAILED\n", u4Port));
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tEoamQMsg));

    pMsg->u4Port = u4Port;

    MEMSET (&(pMsg->unEoamMsgParam.LMEvents), 0, sizeof (tEoamThresEventInfo));
    MEMCPY (&(pMsg->unEoamMsgParam.LMEvents), pEventInfo,
            sizeof (tEoamThresEventInfo));

    switch (pEventInfo->u1EventType)
    {
        case EOAM_ERRORED_SYMBOL_EVENT_TLV:
            pMsg->u4MsgType = EOAM_ERR_SYMBOL_EVENT_TRIG_MSG;
            break;

        case EOAM_ERRORED_FRAME_EVENT_TLV:
            pMsg->u4MsgType = EOAM_ERR_FRAME_EVENT_TRIG_MSG;
            break;

        case EOAM_ERRORED_FRAME_PERIOD_EVENT_TLV:
            pMsg->u4MsgType = EOAM_ERR_FRAME_PERIOD_EVENT_TRIG_MSG;
            break;

        case EOAM_ERRORED_FRAME_SECONDS_EVENT_TLV:
            pMsg->u4MsgType = EOAM_ERR_FRAME_SEC_EVENT_TRIG_MSG;
            break;

        default:
            EOAM_TRC ((ALL_FAILURE_TRC,
                       "EoamApiSendThresholdEvtToPeer: Port %d :"
                       " Invalid Event Type received\n", u4Port));
            i4RetVal = OSIX_FAILURE;
            break;
    }

    i4RetVal = EoamQueEnqAppMsg (pMsg);

    EOAM_TRC_FN_EXIT ();

    return (i4RetVal);
}

/******************************************************************************
 * Function Name      : EoamApiSendOrgSpecEventToPeer
 *
 * Description        : This function posts a message to EOAM task message
 *                      queue requesting OAM to send out an Event Notification
 *                      OAMPDU with organization specific Event TLV
 *
 * Input(s)           : u4Port -Interface number
 *                      pOrgSpecData - Variable request data
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
EoamApiSendOrgSpecEventToPeer (UINT4 u4Port, tEoamExtData * pOrgSpecData)
{
    tEoamQMsg          *pMsg = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    EOAM_TRC_FN_ENTRY ();

    if (gEoamGlobalInfo.u1SystemCtrlStatus == EOAM_SHUTDOWN)
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "EOAM is not started\n"));
        return OSIX_FAILURE;
    }

    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tEoamQMsg *) (MemAllocMemBlk
                               (gEoamGlobalInfo.QMsgPoolId))) == NULL)
    {
        EOAM_TRC ((BUFFER_TRC | ALL_FAILURE_TRC,
                   "EoamApiSendOrgSpecEventToPeer: Port %d :"
                   " Intf Msg ALLOC_MEM_BLOCK FAILED\n", u4Port));
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tEoamQMsg));

    pMsg->u4MsgType = EOAM_ORG_SPEC_EVENT_TRIG_MSG;
    pMsg->u4Port = u4Port;

    if (EoamUtilCopyInfo (&(pMsg->unEoamMsgParam.EoamExtData.pu1Data),
                          pOrgSpecData->pu1Data, pOrgSpecData->u4DataLen) ==
        OSIX_FAILURE)
    {
        if (MemReleaseMemBlock (gEoamGlobalInfo.QMsgPoolId, (UINT1 *) pMsg)
            != MEM_SUCCESS)
        {

            EOAM_TRC ((BUFFER_TRC | ALL_FAILURE_TRC,
                       "EoamApiSendOrgSpecEventToPeer: Port %d:"
                       "MemReleaseMemBlock FAILED\n", u4Port));
        }
        return OSIX_FAILURE;
    }
    pMsg->unEoamMsgParam.EoamExtData.u4DataLen = pOrgSpecData->u4DataLen;

    i4RetVal = EoamQueEnqAppMsg (pMsg);

    EOAM_TRC_FN_EXIT ();

    return (i4RetVal);
}

/******************************************************************************
 * Function Name      : EoamApiSendVarResponseToPeer
 *
 * Description        : This function posts a message to EOAM task message
 *                      queue requesting OAM to send out a variable response
 *                      OAMPDU. This function will be called by CMIP Interface
 *                      module
 *
 * Input(s)           : u4Port -Interface number
 *                      pRespData- Error Event Information
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
EoamApiSendVarResponseToPeer (UINT4 u4Port, tEoamVarReqResp * pRespData)
{
    tEoamQMsg          *pMsg = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    EOAM_TRC_FN_ENTRY ();

    EOAM_TRC ((EOAM_VAR_REQRESP_TRC,
               "EoamApiSendVarResponseToPeer: Sending Event on port %d\n",
               u4Port));

    if (gEoamGlobalInfo.u1SystemCtrlStatus == EOAM_SHUTDOWN)
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "EOAM is not started\n"));
        return OSIX_FAILURE;
    }

    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tEoamQMsg *) (MemAllocMemBlk
                               (gEoamGlobalInfo.QMsgPoolId))) == NULL)
    {
        EOAM_TRC ((BUFFER_TRC | ALL_FAILURE_TRC,
                   "EoamApiSendVarResponseToPeer: Port %d :"
                   " Intf Msg ALLOC_MEM_BLOCK FAILED\n", u4Port));
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tEoamQMsg));

    pMsg->u4MsgType = EOAM_VAR_RESP_PDU_TRIG_MSG;
    pMsg->u4Port = u4Port;
    if (EoamUtilCopyInfo (&(pMsg->unEoamMsgParam.VarReqResp.RespData.pu1Data),
                          pRespData->RespData.pu1Data,
                          pRespData->RespData.u4DataLen) == OSIX_FAILURE)
    {
        if (MemReleaseMemBlock (gEoamGlobalInfo.QMsgPoolId, (UINT1 *) pMsg)
            != MEM_SUCCESS)
        {
            EOAM_TRC ((BUFFER_TRC | ALL_FAILURE_TRC,
                       "EoamApiSendVarResponseToPeer: Port %d:"
                       "MemReleaseMemBlock FAILED\n", u4Port));
        }
        return OSIX_FAILURE;
    }

    pMsg->unEoamMsgParam.VarReqResp.RespData.u4DataLen =
        pRespData->RespData.u4DataLen;
    pMsg->unEoamMsgParam.VarReqResp.u4ReqId = pRespData->u4ReqId;
    pMsg->unEoamMsgParam.VarReqResp.u2Leaf = pRespData->u2Leaf;
    pMsg->unEoamMsgParam.VarReqResp.u1Branch = pRespData->u1Branch;

    i4RetVal = EoamQueEnqAppMsg (pMsg);

    EOAM_TRC_FN_EXIT ();

    return (i4RetVal);
}

/******************************************************************************
 * Function Name      : EoamApiEnqIncomingPdu
 *
 * Description        : This function receives the incoming frame from the
 *                      Packet Reception module and enqueues it to the EOAM
 *                      task's queue and an appropriate event is sent
 *
 * Input(s)           : pBuf - pointer to CRU buffer of the incoming packet
 *                      u4PortIndex - Interface Index 
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
EoamApiEnqIncomingPdu (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4PortIndex)
{
    tEoamQMsg          *pMsg = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    EOAM_TRC_FN_ENTRY ();

    if (gEoamGlobalInfo.u1SystemCtrlStatus == EOAM_SHUTDOWN)
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "EOAM is not started\n"));
        return OSIX_FAILURE;
    }

    if ((u4PortIndex < EOAM_MIN_PORTS) || (u4PortIndex > EOAM_MAX_PORTS))
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "EoamApiEnqIncomingPdu:"
                   "Port %u :Invalid Port Index\n", u4PortIndex));
        return OSIX_FAILURE;
    }

    EOAM_TRC ((DUMP_TRC,
               "Called EoamApiEnqIncomingPdu for Port %d\n", u4PortIndex));

    /* Allocate Control Message buffer from the pool */
    if ((pMsg = (tEoamQMsg *) (MemAllocMemBlk
                               (gEoamGlobalInfo.QMsgPoolId))) == NULL)
    {
        EOAM_TRC ((BUFFER_TRC | ALL_FAILURE_TRC,
                   "EoamApiEnqIncomingPdu: Ctrl Msg ALLOC_MEM_BLOCK FAILED\n",
                   u4PortIndex));
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tEoamQMsg));

    pMsg->u4Port = u4PortIndex;
    pMsg->u4MsgType = EOAM_PDU_RCVD_MSG;
    pMsg->unEoamMsgParam.pEoamPdu = pBuf;

    i4RetVal = EoamQueEnqAppMsg (pMsg);

    EOAM_TRC_FN_EXIT ();

    return (i4RetVal);
}

/******************************************************************************
 * Function Name      : EoamApiNotifyIfCreate
 *
 * Description        : This function posts a message to EOAM task's message
 *                      queue notify  an interface creation
 *
 * Input(s)           : u4IfIndex - Interface index
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
EoamApiNotifyIfCreate (UINT4 u4IfIndex)
{
    tEoamQMsg          *pMsg = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    EOAM_TRC_FN_ENTRY ();

    if (gEoamGlobalInfo.u1SystemCtrlStatus == EOAM_SHUTDOWN)
    {
        EOAM_TRC ((INIT_SHUT_TRC | ALL_FAILURE_TRC, "EOAM is not started\n"));
        return OSIX_FAILURE;
    }

    if ((u4IfIndex < EOAM_MIN_PORTS) || (u4IfIndex > EOAM_MAX_PORTS))
    {
        EOAM_TRC ((INIT_SHUT_TRC | ALL_FAILURE_TRC, "EoamCreatePort called"
                   "for non-physical interface \n"));
        return OSIX_FAILURE;
    }
    EOAM_TRC ((INIT_SHUT_TRC,
               "EoamApiNotifyCreate: Creating port %d\n", u4IfIndex));
    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tEoamQMsg *) (MemAllocMemBlk
                               (gEoamGlobalInfo.QMsgPoolId))) == NULL)
    {
        EOAM_TRC ((INIT_SHUT_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                   "EoamApiNotifyIfCreate: Port %d :"
                   " Intf Msg ALLOC_MEM_BLOCK FAILED\n", u4IfIndex));
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tEoamQMsg));

    pMsg->u4MsgType = EOAM_CREATE_PORT_MSG;
    pMsg->u4Port = u4IfIndex;

    i4RetVal = EoamQueEnqAppMsg (pMsg);
    if (i4RetVal == OSIX_SUCCESS)
    {
        /* To achieve synchronization during interface creation,
         * i.e., to make the current task to wait until the interface
         * is successfully created in EOAM */
        EoamSyncTakeSem ();
    }

    EOAM_TRC_FN_EXIT ();

    return (i4RetVal);
}

/******************************************************************************
 * Function Name      : EoamApiNotifyIfDelete
 *
 * Description        : This function posts a message to EOAM task's message
 *                      queue notify  an interface deletion
 *
 * Input(s)           : u4IfIndex - Interface index
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
EoamApiNotifyIfDelete (UINT4 u4IfIndex)
{
    tEoamQMsg          *pMsg = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    EOAM_TRC_FN_ENTRY ();

    if (gEoamGlobalInfo.u1SystemCtrlStatus == EOAM_SHUTDOWN)
    {
        EOAM_TRC ((INIT_SHUT_TRC | ALL_FAILURE_TRC, "EOAM is not started\n"));
        return OSIX_FAILURE;
    }

    if ((u4IfIndex < EOAM_MIN_PORTS) || (u4IfIndex > EOAM_MAX_PORTS))
    {
        EOAM_TRC ((INIT_SHUT_TRC | ALL_FAILURE_TRC, "EoamApiNotifyIfDelete"
                   " called for non-physical interface \n"));
        return OSIX_FAILURE;
    }

    EOAM_TRC ((INIT_SHUT_TRC,
               "EoamApiNotifyIfDelete: Deleting port %d\n", u4IfIndex));
    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tEoamQMsg *) (MemAllocMemBlk
                               (gEoamGlobalInfo.QMsgPoolId))) == NULL)
    {
        EOAM_TRC ((INIT_SHUT_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                   "EoamApiNotifyIfDelete: Port %d :"
                   " Intf Msg ALLOC_MEM_BLOCK FAILED\n", u4IfIndex));
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tEoamQMsg));

    pMsg->u4MsgType = EOAM_DELETE_PORT_MSG;
    pMsg->u4Port = u4IfIndex;

    i4RetVal = EoamQueEnqAppMsg (pMsg);

    EOAM_TRC_FN_EXIT ();

    return (i4RetVal);
}

/******************************************************************************
 * Function Name      : EoamApiNotifyIfOperChg
 *
 * Description        : This function is invoked from CFA to indicate link
 *                      status change. This function posts a message to EOAM
 *                      task's message queue
 *
 * Input(s)           : u4IfIndex - Interface index
 *                      u1OperStatus - Operational link status of the port
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
EoamApiNotifyIfOperChg (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    tEoamQMsg          *pMsg = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    EOAM_TRC_FN_ENTRY ();

    if (gEoamGlobalInfo.u1SystemCtrlStatus == EOAM_SHUTDOWN)
    {
        EOAM_TRC ((INIT_SHUT_TRC | ALL_FAILURE_TRC, "EOAM is not started\n"));
        return OSIX_FAILURE;
    }
    if ((u4IfIndex < EOAM_MIN_PORTS) || (u4IfIndex > EOAM_MAX_PORTS))
    {
        EOAM_TRC ((INIT_SHUT_TRC | ALL_FAILURE_TRC, "EoamApiNotifyIfOperChg"
                   "called for non-physical interface \n"));
        return OSIX_FAILURE;
    }

    EOAM_TRC ((INIT_SHUT_TRC,
               "EoamApiNotifyIfOperChg:Updating Oper status of port %d\n",
               u4IfIndex));
    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tEoamQMsg *) (MemAllocMemBlk
                               (gEoamGlobalInfo.QMsgPoolId))) == NULL)
    {
        EOAM_TRC ((INIT_SHUT_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                   "EoamApiNotifyIfOperChg: Port %d :"
                   " Intf Msg ALLOC_MEM_BLOCK FAILED\n", u4IfIndex));
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tEoamQMsg));

    pMsg->u4Port = u4IfIndex;
    pMsg->u4MsgType = EOAM_OPER_STATUS_CHG_MSG;
    pMsg->unEoamMsgParam.u4State = u1OperStatus;

    i4RetVal = EoamQueEnqAppMsg (pMsg);

    EOAM_TRC_FN_EXIT ();

    return (i4RetVal);
}

/******************************************************************************
 * Function Name      : EoamApiIfShutDownNotify
 *
 * Description        : This function is invoked from CFA to notify Interface
 *                      shutodown. This function posts a message to EOAM
 *                      for sending the critical event to peer. 
 *
 * Input(s)           : u4IfIndex - Interface index
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
INT4
EoamApiIfShutDownNotify (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    INT4                i4RetVal = OSIX_SUCCESS;
    tEoamPortInfo      *pPortInfo = NULL;

    EOAM_TRC_FN_ENTRY ();

    UNUSED_PARAM (u1OperStatus);
    if (gEoamGlobalInfo.u1ModuleStatus != EOAM_ENABLED)
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "EOAM is globally DISABLED!!!\n"));
        return OSIX_FALSE;
    }
    if (gEoamGlobalInfo.u1SystemCtrlStatus == EOAM_SHUTDOWN)
    {
        EOAM_TRC ((INIT_SHUT_TRC | ALL_FAILURE_TRC, "EOAM is not started\n"));
        return OSIX_FAILURE;
    }
    if ((u4IfIndex < EOAM_MIN_PORTS) || (u4IfIndex > EOAM_MAX_PORTS))
    {
        EOAM_TRC ((INIT_SHUT_TRC | ALL_FAILURE_TRC, "EoamApiNotifyIfOperChg"
                   "called for non-physical interface \n"));
        return OSIX_FAILURE;
    }
    EoamLock ();
    if (EoamUtilGetPortEntry (u4IfIndex, &pPortInfo) != OSIX_FAILURE)
    {
        gu1EmIfShDownFlag = OSIX_TRUE;
        EoamCltInfTriggerLinkFaultMsg (pPortInfo, OSIX_TRUE);
    }
    gu1EmIfShDownFlag = OSIX_FALSE;
    EoamUnLock ();

    EOAM_TRC_FN_EXIT ();
    return (i4RetVal);
}

/******************************************************************************
 * Function Name      : EoamApiIsOAMPDU                                       
 *                                                                            
 * Description        : This function is called to check if the received      
 *                      frame is OAMPDU or not                                
 *                                                                            
 * Input(s)           : pBuf - Received frame buffer                          
 *                                                                            
 * Output(s)          : None.                                                 
 *                                                                           
 * Return Value(s)    : OSIX_TRUE - If the frame is an OAMPDU                 
 *                      OSIX_FALSE - If the frame is not an OAMPDU.           
 *****************************************************************************/
PUBLIC INT4
EoamApiIsOAMPDU (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT2               u2ProtType = 0;
    UINT1               u1SubType = 0;
    tMacAddr            DestMacAddr;
    UINT1               au1EoamSlowMcastAddr[] =
        { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x02 };

    EOAM_TRC_FN_ENTRY ();

    MEMSET (DestMacAddr, 0, MAC_ADDR_LEN);

    if (pBuf == NULL)
    {

        EOAM_TRC ((BUFFER_TRC, "EoamApiIsOAMPDU: OAMPDU BUFFER POINTER"
                   " NULL\n"));
        return OSIX_FALSE;
    }
    /* Copy header */
    EOAM_CRU_GET_STRING (pBuf, DestMacAddr, EOAM_DEST_OFFSET, MAC_ADDR_LEN);

    EOAM_CRU_GET_2_BYTE (pBuf, EOAM_LEN_TYPE_OFFSET, u2ProtType);

    EOAM_CRU_GET_1_BYTE (pBuf, EOAM_SUBTYPE_OFFSET, u1SubType);

    /* Compare the Destination Address of the received frame in order to
     * determine if the packet is a OAMPDU. */
    if ((MEMCMP (DestMacAddr, au1EoamSlowMcastAddr, MAC_ADDR_LEN)) == 0)
    {
        /* Dest addr matches.
         * Check the Type and sub type */
        if ((u2ProtType == CFA_ENET_SLOW_PROTOCOL) &&
            (u1SubType == EOAM_PROTOCOL_SUBTYPE))
        {

            EOAM_TRC ((CONTROL_PLANE_TRC, "EoamApiIsOAMPDU:"
                       "Received an OAMPDU Frame\n"));

            return OSIX_TRUE;
        }
    }

    EOAM_TRC_FN_EXIT ();

    return OSIX_FALSE;
}

/****************************************************************************
 *    FUNCTION NAME    : EoamApiRegisterModules 
 *                                                                          
 *    DESCRIPTION      : This function is called by external modules like 
 *                       Fault Management module to register with EOAM to
 *                       receive the error notifications.
 *
 *    INPUT            : pEoamRegParams - pointer to registration parameters 
 *                                                                          
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamApiRegisterModules (tEoamRegParams * pEoamRegParams)
{
    tTMO_DLL_NODE      *pEnaNode = NULL;
    tEoamPortInfo      *pPortInfo = NULL;
    tEoamCallbackInfo   CallbackInfo;
    UINT4               u4EntId = 0;

    EOAM_TRC_FN_ENTRY ();

    MEMSET (&CallbackInfo, 0, sizeof (tEoamCallbackInfo));

    if (pEoamRegParams == NULL)
    {
        EOAM_TRC ((ALL_FAILURE_TRC | EOAM_CRITICAL_TRC,
                   "EoamApiRegisterModules: FAILED !!!\n"));
        return OSIX_FAILURE;
    }

    u4EntId = pEoamRegParams->u4EntId;

    if ((u4EntId < 1) || (u4EntId >= EOAM_MAX_APPL_ID))
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(gEoamGlobalInfo.EoamRegParams[u4EntId]), 0,
            sizeof (tEoamRegParams));
    gEoamGlobalInfo.EoamRegParams[u4EntId].u4EntId = pEoamRegParams->u4EntId;
    gEoamGlobalInfo.EoamRegParams[u4EntId].u4Bitmap = pEoamRegParams->u4Bitmap;
    gEoamGlobalInfo.EoamRegParams[u4EntId].pNotifyCbFunc =
        pEoamRegParams->pNotifyCbFunc;

    if ((EOAM_NOTIFY_LB_ACK_RCVD & pEoamRegParams->u4Bitmap) &&
        (pEoamRegParams->pNotifyCbFunc != NULL))
    {
        EoamLock ();

        /* Scan the EOAM enabled ports in the DLL and notify the Loopback 
         * status. */
        TMO_DLL_Scan (&(gEoamGlobalInfo.EoamEnaPortDllList),
                      pEnaNode, tTMO_DLL_NODE *)
        {
            pPortInfo = (tEoamPortInfo *) pEnaNode;

            FSAP_ASSERT (pPortInfo != NULL);

            if (pPortInfo->LoopbackInfo.u1Status == EOAM_REMOTE_LOOPBACK)
            {
                CallbackInfo.EventState = EOAM_ENABLED;
                CallbackInfo.u4EventType = EOAM_NOTIFY_LB_ACK_RCVD;
                CallbackInfo.u2Location = EOAM_REMOTE_ENTRY;
                CallbackInfo.u4Port = pPortInfo->u4Port;

                /* Call a function to notify all registered modules */
                EoamMainNotifyModules (&CallbackInfo);
            }
        }

        EoamUnLock ();
    }

    EOAM_TRC_FN_EXIT ();

    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : EoamApiDeRegisterModule 
 *                                                                          
 *    DESCRIPTION      : This function is called by external modules like 
 *                       Fault Management module to de-register from EOAM
 *
 *    INPUT            : u4EntId - Entity-id of the deregistering module
 *                                                                          
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamApiDeRegisterModule (UINT4 u4EntId)
{
    if (u4EntId >= EOAM_MAX_APPL_ID)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(gEoamGlobalInfo.EoamRegParams[u4EntId]), 0,
            sizeof (tEoamRegParams));

    return OSIX_SUCCESS;
}

/*******************************************************************************
 *    FUNCTION NAME    :  EoamApiIsEnabledOnPort
 *                                                                          
 *    DESCRIPTION      :  This function checks if EOAM is administratively
 *                        started on the interface
 *                                                                          
 *    INPUT            :  u4IfIndex - Interface Index of the port  
 *                                                                          
 *    OUTPUT           :  None.                                             
 *                                                                          
 *    RETURNS          :  OSIX_TRUE / OSIX_FALSE                       
 *                                                                          
*******************************************************************************/
PUBLIC INT4
EoamApiIsEnabledOnPort (UINT4 u4IfIndex)
{
    tEoamPortInfo      *pPortEntry = NULL;
    INT4                i4RetVal = OSIX_FALSE;

    EOAM_TRC_FN_ENTRY ();

    if (EoamIsPhysicalPort (u4IfIndex) == OSIX_FALSE)
    {
        /* EOAM can be enabled only on physical ports. */
        return OSIX_FALSE;
    }

    if (gEoamGlobalInfo.u1ModuleStatus != EOAM_ENABLED)
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "EOAM is globally DISABLED!!!\n"));
        return OSIX_FALSE;
    }

    EoamLock ();

    if (EoamUtilGetPortEntry (u4IfIndex, &pPortEntry) != OSIX_FAILURE)
    {
        if (pPortEntry->LocalInfo.u1EoamAdminStatus == EOAM_ENABLED)
        {
            i4RetVal = OSIX_TRUE;
        }
    }

    EoamUnLock ();

    EOAM_TRC_FN_EXIT ();

    return (i4RetVal);
}

/******************************************************************************
 * Function Name      : EoamApiGetPeerMac 
 *
 * Description        : This function is invoked to get the MAC address of the 
                        peer entity. 
 *
 * Input(s)           : u4IfIndex - Interface index
 *
 * Output(s)          : pMacAddr - pointer to the peer's MAC address 
 *
 * Return Value(s)    : None. 
 *****************************************************************************/
PUBLIC VOID
EoamApiGetPeerMac (UINT4 u4IfIndex, UINT1 *pMacAddr)
{
    tEoamPortInfo      *pPortEntry = NULL;

    EoamLock ();

    if (EoamUtilGetPortEntry (u4IfIndex, &pPortEntry) != OSIX_FAILURE)
    {
        MEMCPY (pMacAddr, pPortEntry->RemoteInfo.MacAddress, MAC_ADDR_LEN);
    }

    EoamUnLock ();

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamLock 
 *                                                                          
 *    DESCRIPTION      : Function to take the mutual exclusion protocol
 *                       semaphore.  
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamLock (VOID)
{
    EOAM_TRC_FN_ENTRY ();

    if (OsixSemTake (gEoamGlobalInfo.ProtocolSemId) == OSIX_FAILURE)
    {
        EOAM_TRC ((OS_RESOURCE_TRC | EOAM_CRITICAL_TRC,
                   "EoamLock: Take sem FAILED !!!\n"));
        return (SNMP_FAILURE);
    }

    EOAM_TRC_FN_EXIT ();

    return (SNMP_SUCCESS);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamUnLock 
 *                                                                          
 *    DESCRIPTION      : Function to release the mutual exclusion protocol
 *                       semaphore.  
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamUnLock (VOID)
{
    EOAM_TRC_FN_ENTRY ();

    OsixSemGive (gEoamGlobalInfo.ProtocolSemId);

    EOAM_TRC_FN_EXIT ();

    return (SNMP_SUCCESS);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamSyncTakeSem 
 *                                                                          
 *    DESCRIPTION      : Function to take the synchronization semaphore.  
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamSyncTakeSem (VOID)
{
    if (OsixSemTake (gEoamGlobalInfo.SyncSemId) == OSIX_FAILURE)
    {
        EOAM_TRC ((OS_RESOURCE_TRC | EOAM_CRITICAL_TRC,
                   "EoamSyncTakeSem: Take sem FAILED !!!\n"));
        return (OSIX_FAILURE);
    }

    return (OSIX_SUCCESS);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamSyncGiveSem 
 *                                                                          
 *    DESCRIPTION      : Function to give the synchronization semaphore.  
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamSyncGiveSem (VOID)
{
    OsixSemGive (gEoamGlobalInfo.SyncSemId);

    return (OSIX_SUCCESS);
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  emapi.c                        */
/*-----------------------------------------------------------------------*/
