/*******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emcltlb.c,v 1.15 2014/03/06 12:50:26 siva Exp $
 *
 * Description: This file conatains the functions related to Loopback PDU 
 * handling by EOAM client module. 
 ******************************************************************************/
#include "eminc.h"

PRIVATE INT4 EoamCltLbPerformBasicTests PROTO ((tEoamPortInfo *));

/*******************************************************************************
 *     FUNCTION NAME    :  EoamCltLbHandleTmrExpiry 
 *                                                                           
 *     DESCRIPTION      :  This function reverts to the original MUX and
 *                         PAR states as no acknowledgement is received
 *                         till the expiry of LB Ack Timer
 *                                                                           
 *     INPUT            :  pPortInfo - pointer to port specific info         
 *                                                                           
 *     OUTPUT           :  None.                                             
 *                                                                           
 *     RETURNS          :  OSIX_SUCCESS / OSIX_FAILURE                       
*******************************************************************************/
PUBLIC INT4
EoamCltLbHandleTmrExpiry (tEoamPortInfo * pPortInfo)
{
    tEoamCallbackInfo   CallbackInfo;
    EOAM_TRC_FN_ENTRY ();
    FSAP_ASSERT (pPortInfo != NULL);
    MEMSET (&CallbackInfo, 0, sizeof (tEoamCallbackInfo));
    EOAM_TRC ((EOAM_LOOPBACK_TRC, "CLIENT: Loopback Acknowledgement not "
               "received on port %u\n ", pPortInfo->u4Port));
    pPortInfo->LoopbackInfo.u1Status = EOAM_NO_LOOPBACK;
    EoamMuxParSet (pPortInfo, EOAM_NO_LOOPBACK);
    EoamSetIfInfo (IF_EOAM_REMOTE_LB, pPortInfo->u4Port, EOAM_DISABLED);

    pPortInfo->LocalInfo.u2ConfigRevision++;
    /* Update the Information OAMPDU structure */
    EoamCtlTxConstructInfoPdu (pPortInfo);
#ifdef NPAPI_WANTED
    if (EoamEoamNpHandleRemoteLoopBack (pPortInfo->u4Port, NULL, NULL,
                                        EOAM_NP_LB_DISABLE) != FNP_SUCCESS)
    {
        EOAM_TRC ((ALL_FAILURE_TRC | EOAM_LOOPBACK_TRC,
                   "CLIENT: EoamCltLbHandleTmrExpiry - NP handle failed on "
                   "port %u\n ", pPortInfo->u4Port));
    }
#endif

    CallbackInfo.u4EventType = EOAM_NOTIFY_LB_ACK_NOT_RCVD;
    CallbackInfo.u4Port = pPortInfo->u4Port;
    /* Call a function to notify all registered modules */
    EoamMainNotifyModules (&CallbackInfo);
    EOAM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*******************************************************************************
 *                                                                           
 *    FUNCTION NAME    :  EoamCltLbPduProcess
 *                                                                          
 *    DESCRIPTION      :  This function is called on reception of a          
 *                        Loopback Control OAMPDU. If Loopback PDU          
 *                        processing is allowed, it changes the Local Mux   
 *                        and Parser actions suitably and generates a       
 *                        request for an Information OAMPDU.                
 *                                                                          
 *    INPUT            :  pPortInfo - pointer to port specific info          
 *                        pData - pointer to PDU Data                       
 *                                                                          
 *    OUTPUT           :  None.                                             
 *                                                                          
 *    RETURNS          :  OSIX_SUCCESS / OSIX_FAILURE                       
 *                                                                          
*******************************************************************************/
PUBLIC INT4
EoamCltLbPduProcess (tEoamPortInfo * pPortInfo, tEoamPduData * pData)
{
    tEoamIfInfo         EoamIfInfo;
    tEoamEnaPortInfo   *pActivePortInfo = NULL;
    UINT4               u4RemainingTime = 0;
#ifdef NPAPI_WANTED
    UINT1               u1LBStatus = 0;
#endif

    EOAM_TRC_FN_ENTRY ();
    MEMSET (&EoamIfInfo, 0, sizeof (tEoamIfInfo));
    pActivePortInfo = pPortInfo->pEoamEnaPortInfo;

    /* Loopback Request from a passive node should not be entertained */
    if (pPortInfo->RemoteInfo.u1Mode == EOAM_MODE_PASSIVE)
    {
        EOAM_TRC ((EOAM_LOOPBACK_TRC, "CLIENT: Received Loopback OAMPDU from"
                   "a passive node on port %u\n ", pPortInfo->u4Port));
        return OSIX_FAILURE;
    }

    if ((pData->LBCmd != EOAM_LB_ENABLE) && (pData->LBCmd != EOAM_LB_DISABLE))
    {
        EOAM_TRC ((EOAM_LOOPBACK_TRC,
                   "CLIENT: Loopback command received with unknown type"
                   " on port %u\n", pPortInfo->u4Port));
        return OSIX_FAILURE;
    }

    /* This function may be called either to enable or disable loopback */
    if (pData->LBCmd == EOAM_LB_ENABLE)
    {
        EOAM_TRC ((EOAM_LOOPBACK_TRC, "CLIENT: Received Loopback"
                   "enable request on port %u\n ", pPortInfo->u4Port));

        if (pPortInfo->LoopbackInfo.u1Status == EOAM_LOCAL_LOOPBACK)
        {
            EOAM_TRC ((EOAM_LOOPBACK_TRC,
                       "CLIENT: Received Loopback enable request when in "
                       "LOCAL_LOOPBACK mode on port %u\n ", pPortInfo->u4Port));
            return OSIX_SUCCESS;
        }

        /* If the node has sent a Loopback Control OAMPDU and is waiting for 
         * the peer to respond, and at the same time receives a loopback 
         * request PDU, the action is taken based on the comparison of local 
         * and peer MAC address */
        if ((pPortInfo->LoopbackInfo.u1Status == EOAM_INITIATING_LOOPBACK) ||
            (pPortInfo->LoopbackInfo.u1Status == EOAM_REMOTE_LOOPBACK))
        {
            EoamGetIfInfo (pPortInfo->u4Port, &EoamIfInfo);

            /* If the local DTE has a higher MAC address than the peer, it 
             * should enter OAM remote loopback mode.
             * If the local DTE has a lower MAC address than the peer, it 
             * should ignore the loopback command from the peer. */
            if ((EOAM_IS_LOCAL_MAC_LOWER (EoamIfInfo.au1MacAddr,
                                          pPortInfo->RemoteInfo.MacAddress)) ==
                OSIX_TRUE)
            {
                EOAM_TRC ((EOAM_LOOPBACK_TRC,
                           "CLIENT: Received Loopback enable request when in "
                           "INITIATING_LOOPBACK mode on port %u\n ",
                           pPortInfo->u4Port));
                return OSIX_SUCCESS;
            }
        }

        /* Increment the Loopback OAMPDU Rx Counter */
        pPortInfo->PduStats.u4LoopbackControlRx++;

        /* Remote Loopback commands will not be processed by the DTE if it is 
         * configured to ignore loopback commands */
        if (pPortInfo->LoopbackInfo.u1IgnoreRx == EOAM_IGNORE_LB_CMD)
        {
            EOAM_TRC ((EOAM_LOOPBACK_TRC,
                       "CLIENT: Received Loopback enable request on port %u, "
                       "but DTE configured to ignore LB commands\n",
                       pPortInfo->u4Port));
            return OSIX_FAILURE;
        }

        if (pPortInfo->LoopbackInfo.u1Status == EOAM_INITIATING_LOOPBACK)
        {
            TmrGetRemainingTime (gEoamGlobalInfo.EoamTmrListId,
                                 &(pActivePortInfo->LBTxTmr.TimerNode),
                                 &u4RemainingTime);
            if (u4RemainingTime != 0)
            {
                TmrStop (gEoamGlobalInfo.EoamTmrListId,
                         &pActivePortInfo->LBTxTmr);
            }

#ifdef NPAPI_WANTED
            if (EoamEoamNpHandleRemoteLoopBack
                (pPortInfo->u4Port, NULL, NULL, EOAM_NP_LB_DISABLE) !=
                FNP_SUCCESS)
            {
                EOAM_TRC ((ALL_FAILURE_TRC | EOAM_LOOPBACK_TRC,
                           "CLIENT: EoamNpHandleRemoteLoopBack disable failed "
                           "on port %u\n ", pPortInfo->u4Port));
                return OSIX_FAILURE;
            }
#endif
        }

        pPortInfo->LoopbackInfo.u1Status = EOAM_LOCAL_LOOPBACK;

        /* Update the local Multiplexer and Parser actions */
        EoamMuxParSet (pPortInfo, EOAM_LOCAL_LOOPBACK);
    }
    else if (pData->LBCmd == EOAM_LB_DISABLE)
    {
        /* Increment the Loopback OAMPDU Rx Counter */
        pPortInfo->PduStats.u4LoopbackControlRx++;
        if (pPortInfo->LoopbackInfo.u1Status == EOAM_NO_LOOPBACK)
        {
            EOAM_TRC ((EOAM_LOOPBACK_TRC, "CLIENT: Received Loopback "
                       "enable request when in NO_LOOPBACK mode"
                       "on port %u\n ", pPortInfo->u4Port));
            return OSIX_SUCCESS;
        }
        pPortInfo->LoopbackInfo.u1Status = EOAM_NO_LOOPBACK;

        /* Update the local Multiplexer and Parser actions */
        EoamMuxParSet (pPortInfo, EOAM_NO_LOOPBACK);
    }
    pPortInfo->LocalInfo.u2ConfigRevision++;

    /* Sync loopback status with Standby Node */
    EoamRedSyncUpDynamicInfo (pPortInfo);

    /* Update the Information OAMPDU structure */
    EoamCtlTxConstructInfoPdu (pPortInfo);

    /* Send out an Information OAMPDU to inform peer about the updation
     * of the local Multiplexer and Parser actions */
    if (EoamCtlTxSendInfoPdu (pPortInfo) != OSIX_SUCCESS)
    {
        EOAM_TRC ((EOAM_LOOPBACK_TRC | ALL_FAILURE_TRC,
                   "CLIENT: Transmission of Information OAMPDU acknowledging "
                   "the reception of Loopback OAMPDU failed on port %u\n",
                   pPortInfo->u4Port));
        return OSIX_FAILURE;
    }
#ifdef NPAPI_WANTED
    u1LBStatus = ((pPortInfo->LoopbackInfo.u1Status == EOAM_LOCAL_LOOPBACK)
                  ? EOAM_NP_LB_ENABLE : EOAM_NP_LB_DISABLE);
    EoamGetIfInfo (pPortInfo->u4Port, &EoamIfInfo);
    if (EoamEoamNpHandleLocalLoopBack
        (pPortInfo->u4Port, pPortInfo->RemoteInfo.MacAddress,
         EoamIfInfo.au1MacAddr, u1LBStatus) != FNP_SUCCESS)
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "CLIENT: EoamEoamNpHandleLocalLoopBack"
                   "failed on port %u\n", pPortInfo->u4Port));
        return OSIX_FAILURE;
    }
#endif
    EOAM_TRC ((EOAM_LOOPBACK_TRC,
               "CLIENT: Remote Loopback OAMPDU processed "
               "successfully on port %u\n", pPortInfo->u4Port));
    EOAM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/******************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamCltLbCmdSendToPeer 
 *                                                                          
 *    DESCRIPTION      : This function is called when a loopback command 
 *                       (start / stop) is to be sent to the remote peer. 
 *                       A 3 sec timer is started and an appropriate OAMPDU 
 *                       request is sent to the Control Module.
 *                                                                          
 *    INPUT            : *pPortEntry - pointer to port specific info
 *                        u1LBCmd - Loopback Enable / Disable command
 *                                                                          
 *    OUTPUT           :  None
 *                                                                          
 *    RETURNS          :  OSIX_SUCCESS / OSIX_FAILURE
 *                                                                          
*******************************************************************************/
PUBLIC INT4
EoamCltLbCmdSendToPeer (tEoamPortInfo * pPortInfo, UINT1 u1LBCmd)
{
    tEoamPduHeader      PduHeader;
    tEoamPduData        PduData;
    tEoamEnaPortInfo   *pActivePortInfo = NULL;

    EOAM_TRC_FN_ENTRY ();
    MEMSET (&PduHeader, 0, sizeof (tEoamPduHeader));
    MEMSET (&PduData, 0, sizeof (tEoamPduData));
    if (pPortInfo == NULL)
    {
        return OSIX_SUCCESS;
    }

    if (EOAM_RM_NODE_STATUS () != EOAM_ACTIVE_NODE)
    {
        /* Allow Pkt Tx only on active node */
        return OSIX_SUCCESS;
    }

    pActivePortInfo = pPortInfo->pEoamEnaPortInfo;

    if (EoamCltLbPerformBasicTests (pPortInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (((pPortInfo->LoopbackInfo.u1Status == EOAM_REMOTE_LOOPBACK) &&
         (u1LBCmd == EOAM_LB_ENABLE)) ||
        ((pPortInfo->LoopbackInfo.u1Status == EOAM_NO_LOOPBACK) &&
         (u1LBCmd == EOAM_LB_DISABLE)))
    {
        EOAM_TRC ((EOAM_LOOPBACK_TRC, "CLIENT: Remote LB Request initiated "
                   "on port %u when loopback status is already the same\n",
                   pPortInfo->u4Port));
        return OSIX_SUCCESS;
    }
    if (u1LBCmd == EOAM_LB_ENABLE)
    {
        pPortInfo->LoopbackInfo.u1Status = EOAM_INITIATING_LOOPBACK;
#ifdef NPAPI_WANTED
        if (EoamEoamNpHandleRemoteLoopBack (pPortInfo->u4Port, NULL, NULL,
                                            EOAM_NP_LB_INITIATE) != FNP_SUCCESS)
        {
            EOAM_TRC ((ALL_FAILURE_TRC | EOAM_LOOPBACK_TRC,
                       "CLIENT: EoamNpHandleRemoteLoopBack failed on"
                       "port %u\n ", pPortInfo->u4Port));
            return OSIX_FAILURE;
        }
#endif
    }
    else if (u1LBCmd == EOAM_LB_DISABLE)
    {
        pPortInfo->LoopbackInfo.u1Status = EOAM_TERMINATING_LOOPBACK;
    }
    EoamMuxParSet (pPortInfo, pPortInfo->LoopbackInfo.u1Status);
    PduData.LBCmd = u1LBCmd;
    EoamCtlTxFormPduHeader (pPortInfo, EOAM_LB_CTRL, &PduHeader);
    if (EoamCtlTxPduRequest (pPortInfo, &PduHeader, &PduData) != OSIX_SUCCESS)
    {
        EOAM_TRC ((EOAM_LOOPBACK_TRC | ALL_FAILURE_TRC,
                   "CLIENT: EoamCtlTxPduRequest call for Loopback Control "
                   "PDU failed on port %u\n", pPortInfo->u4Port));

        if (pPortInfo->LoopbackInfo.u1Status == EOAM_INITIATING_LOOPBACK)
        {
            pPortInfo->LoopbackInfo.u1Status = EOAM_NO_LOOPBACK;
            EoamMuxParSet (pPortInfo, EOAM_NO_LOOPBACK);
        }
        else if (pPortInfo->LoopbackInfo.u1Status == EOAM_TERMINATING_LOOPBACK)
        {
            pPortInfo->LoopbackInfo.u1Status = EOAM_REMOTE_LOOPBACK;
            EoamMuxParSet (pPortInfo, EOAM_REMOTE_LOOPBACK);
        }
        return OSIX_FAILURE;
    }

    pPortInfo->LocalInfo.u2ConfigRevision++;
    /* Update the Information OAMPDU structure */
    EoamCtlTxConstructInfoPdu (pPortInfo);

    /* Increment the Loopback Control OAMPDU Tx Counter */
    pPortInfo->PduStats.u4LoopbackControlTx++;
    if (TmrStart (gEoamGlobalInfo.EoamTmrListId,
                  &pActivePortInfo->LBTxTmr, EOAM_LB_CMD_TX_TMR_TYPE,
                  EOAM_LB_CMD_TX_TIMEOUT, 0) != TMR_SUCCESS)
    {
        EOAM_TRC ((EOAM_LOOPBACK_TRC | ALL_FAILURE_TRC,
                   "CLIENT: TmrStart failed for Loopback Control Tx on "
                   "port %u\n", pPortInfo->u4Port));
        return OSIX_FAILURE;
    }
    EOAM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/******************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamCltLbPerformBasicTests 
 *                                                                          
 *    DESCRIPTION      : This function performs basic tests to decide on 
 *                       whether the loopback request PDU can be sent to the 
 *                       peer or not. 
 *                                                                          
 *    INPUT            : pPortInfo - pointer to port specific info
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *                                                                          
*******************************************************************************/
PRIVATE INT4
EoamCltLbPerformBasicTests (tEoamPortInfo * pPortInfo)
{
    EOAM_TRC_FN_ENTRY ();
    if (gEoamGlobalInfo.u1ModuleStatus != EOAM_ENABLED)
    {
        EOAM_TRC ((ALL_FAILURE_TRC | EOAM_LOOPBACK_TRC,
                   "EOAM is globally DISABLED!!!\n"));
        return OSIX_FAILURE;
    }
    /* OAMPDUs other than Information PDUs can be sent out only when
     * discovery has settled down */
    if (pPortInfo->LocalInfo.u2EoamOperStatus != EOAM_OPER_OPERATIONAL)
    {
        EOAM_TRC ((EOAM_LOOPBACK_TRC | ALL_FAILURE_TRC,
                   "CLIENT: Cannot send out Loopback command "
                   "OAMPDU when discovery has not settled down on"
                   "port %u\n ", pPortInfo->u4Port));
        return OSIX_FAILURE;
    }
    /* Remote Loopback Request should not be sent out from a passive node */
    if (pPortInfo->LocalInfo.u1Mode == EOAM_MODE_PASSIVE)
    {
        EOAM_TRC ((EOAM_LOOPBACK_TRC | ALL_FAILURE_TRC,
                   "CLIENT: Remote LB Request initiated "
                   "from a passive node on port %u\n", pPortInfo->u4Port));
        return OSIX_FAILURE;
    }
    if ((pPortInfo->LoopbackInfo.u1Status == EOAM_INITIATING_LOOPBACK) ||
        (pPortInfo->LoopbackInfo.u1Status == EOAM_TERMINATING_LOOPBACK) ||
        (pPortInfo->LoopbackInfo.u1Status == EOAM_LOCAL_LOOPBACK))
    {
        EOAM_TRC ((EOAM_LOOPBACK_TRC | ALL_FAILURE_TRC,
                   "CLIENT: Remote LB Request initiated"
                   "on port %u when loopback status is %u\n",
                   pPortInfo->u4Port, pPortInfo->LoopbackInfo.u1Status));
        return OSIX_FAILURE;
    }
    if ((pPortInfo->RemoteInfo.u1FnsSupportedBmp & EOAM_LOOPBACK_SUPPORT) == 0)
    {
        EOAM_TRC ((EOAM_LOOPBACK_TRC | ALL_FAILURE_TRC,
                   "CLIENT: Remote LB Request initiated"
                   "on port %u, but peer does not have the capability\n",
                   pPortInfo->u4Port));
        return OSIX_FAILURE;
    }
    EOAM_TRC ((EOAM_LOOPBACK_TRC,
               "CLIENT: Remote LB Request initiated successfully on port %u\n",
               pPortInfo->u4Port));
    EOAM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*******************************************************************************
 *                                                                          
 *    FUNCTION NAME    :  EoamCltLbHandleAck 
 *                                                                          
 *    DESCRIPTION      :  This function is called on reception of an         
 *                        Information OAMPDU indicating Loopback acceptance.
 *                        It stops the corresponding timer and notifies the 
 *                        acceptance to Fault Management Module. 
 *                                                                          
 *    INPUT            :  pPortInfo - pointer to port specific info          
 *                                                                          
 *    OUTPUT           :  None.                                             
 *                                                                          
 *    RETURNS          :  OSIX_SUCCESS / OSIX_FAILURE                       
 *                                                                          
*******************************************************************************/
PUBLIC INT4
EoamCltLbHandleAck (tEoamPortInfo * pPortInfo)
{
    tEoamEnaPortInfo   *pActivePortInfo = NULL;
    tEoamCallbackInfo   CallbackInfo;
#ifdef NPAPI_WANTED
    tEoamIfInfo         EoamIfInfo;
#endif
    INT4                i4RetVal = OSIX_SUCCESS;
    BOOL1               b1NotificationReqd = OSIX_FALSE;

#ifdef NPAPI_WANTED
    UINT1               u1LBStatus = 0;

    MEMSET (&EoamIfInfo, 0, sizeof (tEoamIfInfo));
#endif
    EOAM_TRC_FN_ENTRY ();
    MEMSET (&CallbackInfo, 0, sizeof (tEoamCallbackInfo));
    pActivePortInfo = pPortInfo->pEoamEnaPortInfo;

    if (pPortInfo->LoopbackInfo.u1Status == EOAM_INITIATING_LOOPBACK)
    {
        if (((pPortInfo->RemoteInfo.u1State & EOAM_MUX_STATE_BITMASK) ==
             EOAM_MUX_DISCARD_STATE) &&
            ((pPortInfo->RemoteInfo.u1State & EOAM_PAR_STATE_BITMASK) ==
             EOAM_PAR_LB_STATE))
        {
            pPortInfo->LoopbackInfo.u1Status = EOAM_REMOTE_LOOPBACK;
            EoamMuxParSet (pPortInfo, EOAM_REMOTE_LOOPBACK);
            EoamSetIfInfo (IF_EOAM_REMOTE_LB, pPortInfo->u4Port, EOAM_ENABLED);
            /* Update Revision, Info PDU will be updated by the calling
             * function */
            pPortInfo->LocalInfo.u2ConfigRevision++;
            b1NotificationReqd = OSIX_TRUE;
            CallbackInfo.EventState = EOAM_ENABLED;
#ifdef NPAPI_WANTED
            u1LBStatus = EOAM_NP_LB_ENABLE;
#endif
            EOAM_TRC ((EOAM_LOOPBACK_TRC,
                       "CLIENT: Received Enable Remote Loopback "
                       "acknowledgemnt on port %u successfully\n",
                       pPortInfo->u4Port));

            /* Sync loopback status with Standby Node */
            EoamRedSyncUpDynamicInfo (pPortInfo);
        }
    }
    if ((pPortInfo->LoopbackInfo.u1Status == EOAM_TERMINATING_LOOPBACK) ||
        (pPortInfo->LoopbackInfo.u1Status == EOAM_REMOTE_LOOPBACK))
    {
        if (((pPortInfo->RemoteInfo.u1State & EOAM_MUX_STATE_BITMASK) ==
             EOAM_MUX_FWD_STATE) &&
            ((pPortInfo->RemoteInfo.u1State & EOAM_PAR_STATE_BITMASK) ==
             EOAM_PAR_FWD_STATE))
        {
            pPortInfo->LoopbackInfo.u1Status = EOAM_NO_LOOPBACK;
            EoamMuxParSet (pPortInfo, EOAM_NO_LOOPBACK);
            EoamSetIfInfo (IF_EOAM_REMOTE_LB, pPortInfo->u4Port, EOAM_DISABLED);
            /* Update Revision, Info PDU will be updated by the calling 
             * function */
            pPortInfo->LocalInfo.u2ConfigRevision++;
            b1NotificationReqd = OSIX_TRUE;
            CallbackInfo.EventState = EOAM_DISABLED;
#ifdef NPAPI_WANTED
            u1LBStatus = EOAM_NP_LB_DISABLE;
#endif
            EOAM_TRC ((EOAM_LOOPBACK_TRC,
                       "CLIENT: Received Disable Remote Loopback "
                       "acknowledgemnt on port %u successsfully\n",
                       pPortInfo->u4Port));

            /* Sync loopback status with Standby Node */
            EoamRedSyncUpDynamicInfo (pPortInfo);
        }
    }
    if (b1NotificationReqd == OSIX_TRUE)
    {
        /* Stop the corresponding timer */
        TmrStop (gEoamGlobalInfo.EoamTmrListId, &pActivePortInfo->LBTxTmr);
#ifdef NPAPI_WANTED
        if (u1LBStatus == EOAM_NP_LB_ENABLE)
        {
            EoamGetIfInfo (pPortInfo->u4Port, &EoamIfInfo);
            if (EoamEoamNpHandleRemoteLoopBack
                (pPortInfo->u4Port, EoamIfInfo.au1MacAddr,
                 pPortInfo->RemoteInfo.MacAddress,
                 EOAM_NP_LB_ENABLE) != FNP_SUCCESS)
            {
                EOAM_TRC ((ALL_FAILURE_TRC | EOAM_LOOPBACK_TRC,
                           "CLIENT: EoamNpHandleRemoteLoopBack enable failed on"
                           "port %u\n ", pPortInfo->u4Port));
                return OSIX_FAILURE;
            }
        }
        else
        {
            if (EoamEoamNpHandleRemoteLoopBack
                (pPortInfo->u4Port, NULL, NULL, EOAM_NP_LB_DISABLE) !=
                FNP_SUCCESS)
            {
                EOAM_TRC ((ALL_FAILURE_TRC | EOAM_LOOPBACK_TRC,
                           "CLIENT: EoamNpHandleRemoteLoopBack disable failed "
                           "on port %u\n ", pPortInfo->u4Port));
                return OSIX_FAILURE;
            }
        }
#endif /* NPAPI_WANTED */
        CallbackInfo.u4EventType = EOAM_NOTIFY_LB_ACK_RCVD;
        CallbackInfo.u2Location = EOAM_REMOTE_ENTRY;
        CallbackInfo.u4Port = pPortInfo->u4Port;
        /* Call a function to notify all registered modules */
        EoamMainNotifyModules (&CallbackInfo);
    }
    else
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "CLIENT: Info PDU received with"
                   "unexpected MUX and PAR states on port %u\n ",
                   pPortInfo->u4Port));
        i4RetVal = OSIX_FAILURE;
    }

    EOAM_TRC_FN_EXIT ();
    return i4RetVal;
}

/*******************************************************************************
 *                                                                          
 *    FUNCTION NAME    :  EoamCltLbRevertToNormal
 *                                                                          
 *    DESCRIPTION      :  This function is called on reception of an         
 *                        Information OAMPDU indicating Loopback acceptance.
 *                        It stops the corresponding timer and notifies the 
 *                        acceptance to Fault Management Module. 
 *                                                                          
 *    INPUT            :  pPortInfo - pointer to port specific info          
 *                                                                          
 *    OUTPUT           :  None.                                             
 *                                                                          
 *    RETURNS          :  OSIX_SUCCESS / OSIX_FAILURE                       
 *                                                                          
*******************************************************************************/
PUBLIC VOID
EoamCltLbRevertToNormal (tEoamPortInfo * pPortInfo)
{
    if (pPortInfo->LoopbackInfo.u1Status == EOAM_LOCAL_LOOPBACK)
    {
        pPortInfo->LoopbackInfo.u1Status = EOAM_NO_LOOPBACK;
        EoamMuxParSet (pPortInfo, EOAM_NO_LOOPBACK);
        /* Update Revision number */
        pPortInfo->LocalInfo.u2ConfigRevision++;
#ifdef NPAPI_WANTED
        if (EoamEoamNpHandleLocalLoopBack
            (pPortInfo->u4Port, NULL, NULL, EOAM_NP_LB_DISABLE) != FNP_SUCCESS)
        {
            EOAM_TRC ((ALL_FAILURE_TRC | EOAM_LOOPBACK_TRC,
                       "CLIENT: EoamEoamNpHandleLocalLoopBack failed"
                       "on port %u\n ", pPortInfo->u4Port));
            return;
        }
#endif
        EOAM_TRC ((EOAM_RFI_TRC | EOAM_LOOPBACK_TRC,
                   "CLIENT: Link fault indication received while in"
                   "LOCAL_LOOPBACK state on port %u\n ", pPortInfo->u4Port));
    }
    else if (pPortInfo->LoopbackInfo.u1Status == EOAM_REMOTE_LOOPBACK)
    {
        pPortInfo->LoopbackInfo.u1Status = EOAM_NO_LOOPBACK;
        EoamMuxParSet (pPortInfo, EOAM_NO_LOOPBACK);
        /* Update Revision number */
        pPortInfo->LocalInfo.u2ConfigRevision++;
        EoamSetIfInfo (IF_EOAM_REMOTE_LB, pPortInfo->u4Port, EOAM_DISABLED);
#ifdef NPAPI_WANTED
        if (EoamEoamNpHandleRemoteLoopBack
            (pPortInfo->u4Port, NULL, NULL, EOAM_NP_LB_DISABLE) != FNP_SUCCESS)
        {
            EOAM_TRC ((ALL_FAILURE_TRC | EOAM_LOOPBACK_TRC,
                       "CLIENT: EoamNpHandleRemoteLoopBack failed"
                       "on port %u\n ", pPortInfo->u4Port));
            return;
        }
#endif
        EOAM_TRC ((EOAM_RFI_TRC | EOAM_LOOPBACK_TRC,
                   "CLIENT: Link fault indication received while in"
                   "REMOTE_LOOPBACK state on port %u\n ", pPortInfo->u4Port));
    }
    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  emcltlb.c                     */
/*-----------------------------------------------------------------------*/
