/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emque.c,v 1.12 2013/12/18 12:50:15 siva Exp $
 *
 * Description: This file contains procedures related to 
 *              - Processing QMsgs
 *              - Enqing QMsg from external module to EOAM task 
 *********************************************************************/
#include "eminc.h"
/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamQueMsgHandler 
 *                                                                          
 *    DESCRIPTION      : This function process the queue messages received 
 *                       by EOAM task. 
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EoamQueMsgHandler (VOID)
{
    tEoamQMsg          *pQMsg = NULL;
    tEoamPortInfo      *pPortEntry = NULL;

    UINT1               u1IsFnsSupported = EOAM_FALSE;
    /*For Functions Supported Bmp. */
    UINT1               u1ProcessRxEvent = EOAM_FALSE;
    /*For EOAM Event Process. */

    EOAM_TRC_FN_ENTRY ();

    /* Event received, dequeue messages for processing */
    while (OsixQueRecv (gEoamGlobalInfo.QId, (UINT1 *) &pQMsg,
                        OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
    {
        EoamUtilGetPortEntry (pQMsg->u4Port, &pPortEntry);

        switch (pQMsg->u4MsgType)
        {
            case EOAM_CREATE_PORT_MSG:

                u1ProcessRxEvent = EOAM_TRUE;

                EOAM_TRC ((CONTROL_PLANE_TRC,
                           "EoamQueMsgHandler: Received Interface creation"
                           " notification from Lower layer \n"));
                EoamIfCreate (pQMsg->u4Port);

                /* To achieve synchronization during interface creation. 
                 * i.e., to continue with the interface creation thread. */
                EoamSyncGiveSem ();

                break;

            case EOAM_DELETE_PORT_MSG:

                u1ProcessRxEvent = EOAM_TRUE;

                EOAM_TRC ((CONTROL_PLANE_TRC,
                           "EoamQueMsgHandler: Received Interface deletion"
                           " notification from Lower layer \n"));
                EoamIfDelete (pPortEntry);
                break;

            case EOAM_OPER_STATUS_CHG_MSG:

                u1ProcessRxEvent = EOAM_TRUE;

                EOAM_TRC ((CONTROL_PLANE_TRC,
                           "EoamQueMsgHandler: Received Interface OperStChg"
                           " notification from Lower layer \n"));
                EoamIfOperChg (pPortEntry,
                               (UINT1) pQMsg->unEoamMsgParam.u4State);
                break;

            case EOAM_PDU_RCVD_MSG:

                u1ProcessRxEvent = EOAM_TRUE;

                EOAM_TRC ((CONTROL_PLANE_TRC,
                           "EoamQueMsgHandler: Received PDU from Lower "
                           "layer \n"));
                EoamCtlRxHandleIncomingPdu (pPortEntry,
                                            pQMsg->unEoamMsgParam.pEoamPdu);
                break;

            case EOAM_LINK_FAULT_TRIG_MSG:
            case EOAM_DYING_GASP_TRIG_MSG:
            case EOAM_CRITICAL_EVENT_TRIG_MSG:

                u1ProcessRxEvent = EOAM_TRUE;

                EOAM_TRC ((CONTROL_PLANE_TRC,
                           "EoamQueMsgHandler: Received Critical error "
                           "evts \n"));
                EoamCltInfSndCriticalLnkEvt2Peer (pPortEntry,
                                                  pQMsg->u4MsgType,
                                                  (UINT1) pQMsg->
                                                  unEoamMsgParam.u4State);
                break;

            case EOAM_ERR_SYMBOL_EVENT_TRIG_MSG:
            case EOAM_ERR_FRAME_PERIOD_EVENT_TRIG_MSG:
            case EOAM_ERR_FRAME_EVENT_TRIG_MSG:
            case EOAM_ERR_FRAME_SEC_EVENT_TRIG_MSG:
                if (pPortEntry == NULL)
                    break;

                if (pPortEntry->LocalInfo.
                    u1FnsSupportedBmp & EOAM_EVENT_SUPPORT)
                {
                    u1ProcessRxEvent = EOAM_TRUE;
                    u1IsFnsSupported = EOAM_TRUE;
                    EOAM_TRC ((CONTROL_PLANE_TRC,
                               "EoamQueMsgHandler: Received Threshold error "
                               "evts \n"));
                    EoamCltEvtSendThresholdEvtToPeer (pPortEntry,
                                                      &(pQMsg->unEoamMsgParam.
                                                        LMEvents),
                                                      pQMsg->u4MsgType);
                }

                break;
            case EOAM_ORG_SPEC_EVENT_TRIG_MSG:

                u1ProcessRxEvent = EOAM_TRUE;

                EOAM_TRC ((CONTROL_PLANE_TRC,
                           "EoamQueMsgHandler: Received Org Spec PDU \n"));
                EoamCltEvtSendOrgSpecificEvt (pPortEntry,
                                              &(pQMsg->unEoamMsgParam.
                                                EoamExtData));
                EOAM_BUDDY_FREE (pQMsg->unEoamMsgParam.EoamExtData.pu1Data);
                break;

            case EOAM_ORG_SPEC_PDU_TRIG_MSG:

                u1ProcessRxEvent = EOAM_TRUE;

                EOAM_TRC ((CONTROL_PLANE_TRC,
                           "EoamQueMsgHandler: Received Org Spec PDU \n"));
                EoamCltOrgSendOrgSpecificPdu (pPortEntry,
                                              &(pQMsg->unEoamMsgParam.
                                                EoamExtData));
                EOAM_BUDDY_FREE (pQMsg->unEoamMsgParam.EoamExtData.pu1Data);
                break;

            case EOAM_VAR_REQ_PDU_TRIG_MSG:
                if (pPortEntry == NULL)
                    break;

                if (pPortEntry->LocalInfo.
                    u1FnsSupportedBmp & EOAM_VARIABLE_SUPPORT)
                {
                    u1ProcessRxEvent = EOAM_TRUE;
                    u1IsFnsSupported = EOAM_TRUE;

                    EOAM_TRC ((CONTROL_PLANE_TRC,
                               "EoamQueMsgHandler: Received Variable Req \n"));
                    EoamCltReqSendToPeer (pPortEntry,
                                          &(pQMsg->unEoamMsgParam.EoamExtData));
                    EOAM_BUDDY_FREE (pQMsg->unEoamMsgParam.EoamExtData.pu1Data);
                }

                break;

            case EOAM_LOOPBACK_ENABLE_MSG:
                if (pPortEntry == NULL)
                    break;

                if (pPortEntry->LocalInfo.
                    u1FnsSupportedBmp & EOAM_LOOPBACK_SUPPORT)
                {
                    u1ProcessRxEvent = EOAM_TRUE;
                    u1IsFnsSupported = EOAM_TRUE;

                    EOAM_TRC ((CONTROL_PLANE_TRC,
                               "EoamQueMsgHandler: Received LB enable QMsg\n"));
                    EoamCltLbCmdSendToPeer (pPortEntry, EOAM_LB_ENABLE);
                }

                break;

            case EOAM_LOOPBACK_DISABLE_MSG:

                u1ProcessRxEvent = EOAM_TRUE;

                EOAM_TRC ((CONTROL_PLANE_TRC,
                           "EoamQueMsgHandler: Received LB disable QMsg \n"));
                EoamCltLbCmdSendToPeer (pPortEntry, EOAM_LB_DISABLE);
                break;

            case EOAM_VAR_RESP_PDU_TRIG_MSG:

                u1ProcessRxEvent = EOAM_TRUE;

                EOAM_TRC ((CONTROL_PLANE_TRC,
                           "EoamQueMsgHandler: Received Variable Resp \n"));
                EoamCltResAccumulate (pPortEntry,
                                      &(pQMsg->unEoamMsgParam.VarReqResp));
                break;

#ifdef MBSM_WANTED
            case MBSM_MSG_CARD_INSERT:
            case MBSM_MSG_CARD_REMOVE:

                u1ProcessRxEvent = EOAM_TRUE;

                EoamMbsmHandleLcStatusChg (pQMsg->unEoamMsgParam.
                                           pMbsmProtoMsg, pQMsg->u4MsgType);
                CRU_BUF_Release_MsgBufChain (pQMsg->unEoamMsgParam.
                                             pMbsmProtoMsg, FALSE);
                break;
#endif /* MBSM_WANTED */

#ifdef L2RED_WANTED
            case EOAM_RM_MSG:

                u1ProcessRxEvent = EOAM_TRUE;

                EoamRedProcessRmMsg (pQMsg);
                break;
#endif
            default:
                EOAM_TRC ((CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                           "EoamQueMsgHandler:Unknown message type received\n"));
                break;
        }

        /* Release the buffer to pool */
        MemReleaseMemBlock (gEoamGlobalInfo.QMsgPoolId, (UINT1 *) pQMsg);

        if ((u1IsFnsSupported == EOAM_FALSE) && (u1ProcessRxEvent == EOAM_FALSE)
            && (pPortEntry != NULL))
        {
            EOAM_TRC ((ALL_FAILURE_TRC, "CLIENT: Received EOAM Event with "
                       "unsupported code on port %u\n", pPortEntry->u4Port));
            pPortEntry->PduStats.u4UnsupportedCodesTx++;
            return;
        }
    }

    EOAM_TRC_FN_EXIT ();

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamQueEnqAppMsg 
 *                                                                          
 *    DESCRIPTION      : Function to post application message to EOAM task. 
 *
 *    INPUT            : pMsg - Pointer to msg to be posted 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamQueEnqAppMsg (tEoamQMsg * pMsg)
{
    EOAM_TRC_FN_ENTRY ();
    if (gEoamGlobalInfo.u1SystemCtrlStatus == EOAM_SHUTDOWN)
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "EoamQueEnqAppMsg:"
                   "EOAM Enqueue Control Message FAILED\n"));
        EoamQueHandleEnqFailure (pMsg);
        return OSIX_FAILURE;
    }
    if (OsixQueSend (gEoamGlobalInfo.QId, (UINT1 *) &pMsg,
                     OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "EoamQueEnqAppMsg:"
                   "EOAM Enqueue Control Message FAILED\n"));

        EoamQueHandleEnqFailure (pMsg);
        return OSIX_FAILURE;
    }

    OsixEvtSend (gEoamGlobalInfo.TaskId, EOAM_QMSG_EVENT);

    EOAM_TRC_FN_EXIT ();

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamQueHandleEnqFailure 
 *                                                                          
 *    DESCRIPTION      : Handles the message posting failures
 *
 *    INPUT            : pMsg - Pointer to msg to clean up 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
VOID
EoamQueHandleEnqFailure (tEoamQMsg * pMsg)
{
    if (((pMsg->u4MsgType == EOAM_ORG_SPEC_EVENT_TRIG_MSG) ||
         (pMsg->u4MsgType == EOAM_ORG_SPEC_PDU_TRIG_MSG) ||
         (pMsg->u4MsgType == EOAM_VAR_REQ_PDU_TRIG_MSG)) &&
        (pMsg->unEoamMsgParam.EoamExtData.pu1Data != NULL))
    {
        EOAM_BUDDY_FREE (pMsg->unEoamMsgParam.EoamExtData.pu1Data);
    }

    if ((pMsg->u4MsgType == EOAM_VAR_RESP_PDU_TRIG_MSG) &&
        (pMsg->unEoamMsgParam.VarReqResp.RespData.pu1Data != NULL))
    {
        EOAM_BUDDY_FREE (pMsg->unEoamMsgParam.VarReqResp.RespData.pu1Data);
    }

    /* The CRU buffer release for pEoamPdu is not done here as it is 
     * done in CFA. */

#ifdef MBSM_WANTED
    if (((pMsg->u4MsgType == MBSM_MSG_CARD_INSERT) ||
         (pMsg->u4MsgType == MBSM_MSG_CARD_REMOVE)) &&
        (pMsg->unEoamMsgParam.pMbsmProtoMsg != NULL))
    {
        CRU_BUF_Release_MsgBufChain (pMsg->unEoamMsgParam.pMbsmProtoMsg, FALSE);
    }
#endif
    if (MemReleaseMemBlock (gEoamGlobalInfo.QMsgPoolId, (UINT1 *) pMsg)
        != MEM_SUCCESS)
    {

        EOAM_TRC ((BUFFER_TRC | ALL_FAILURE_TRC,
                   "MemReleaseMemBlock FAILED\n"));
    }
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  emque.c                        */
/*-----------------------------------------------------------------------*/
