/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emlmpoll.c,v 1.16 2012/10/29 12:02:27 siva Exp $
 *
 * Description: This file contains Ethernet OAM Link monitoring Polling
 *              routines
 *****************************************************************************/
#include "eminc.h"
#include "emlmdefn.h"
#include "emlmglob.h"

PRIVATE VOID EoamLmPollInitCounters PROTO ((tEoamLmEnaPortDllNode *));
PRIVATE VOID EoamLmPollGetWindowCounter PROTO ((tEoamLmEnaPortDllNode *));
PRIVATE VOID EoamLmPollSymbolPeriod PROTO ((tEoamLmEnaPortDllNode *));
PRIVATE VOID EoamLmPollFrameEvent PROTO ((tEoamLmEnaPortDllNode *));
PRIVATE VOID EoamLmPollFramePeriod PROTO ((tEoamLmEnaPortDllNode *));
PRIVATE VOID EoamLmPollCheckSecSumm PROTO ((tEoamLmEnaPortDllNode *));
PRIVATE VOID EoamLmPollSecondsSummary PROTO ((tEoamLmEnaPortDllNode *));
#ifdef NPAPI_WANTED
PRIVATE UINT4 EoamLmGetRunningTotalOnModReboot PROTO ((UINT4, UINT4));
#endif

/******************************************************************************
 * Function Name      : EoamLmPollTimerStart
 *
 * Description        : Starts the polling timer
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
EoamLmPollTimerStart (VOID)
{
    EOAM_TRC_FN_ENTRY ();
    EOAM_TRC ((EOAM_LM_TRC, "EoamLmPollTimerStart: Rx LM Timer "
               "Start event\r\n"));
    if (TMO_DLL_Count (&(gEoamLmGlobalInfo.LmEoamEnaPortDllList)) > 0)
    {
        if (TmrStartTimer (gEoamLmGlobalInfo.LmTmrList,
                           &gEoamLmGlobalInfo.LmTmrNode,
                           EOAMLM_POLLING_INTERVAL) != TMR_SUCCESS)
        {
            EOAM_TRC ((EOAM_LM_TRC | OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                       "EoamLmPollTimerStart: EOAM LM Timer Start failed\r\n"));
        }
    }
    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EoamLmPollTimerStop
 *
 * Description        : Stop the polling timer
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
EoamLmPollTimerStop (VOID)
{
    EOAM_TRC_FN_ENTRY ();
    EOAM_TRC ((EOAM_LM_TRC, "EoamLmPollTimerStop: Rx LM Timer "
               "Stop event\r\n"));

    if (TmrStopTimer (gEoamLmGlobalInfo.LmTmrList,
                      &gEoamLmGlobalInfo.LmTmrNode) != TMR_SUCCESS)
    {
        EOAM_TRC ((EOAM_LM_TRC | OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                   "EoamLmPollTimerStop: EOAM LM Timer Start failed \r\n"));
    }
    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EoamLmPollTimerExpiry
 *
 * Description        : Poll hardware, collect error statistics and
 *                      raise an event when threshold is crossed in any window
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
EoamLmPollTimerExpiry (VOID)
{
    tEoamLmEnaPortDllNode *pListNode = NULL;
    FS_UINT8            u8Diff;
    INT4                i4Compare = 0;

    EOAM_TRC_FN_ENTRY ();

    FSAP_U8_CLR (&u8Diff);

    /* Take DataLock for accessing LM port info */
    OsixSemTake (gEoamLmGlobalInfo.SemId);

    TMO_DLL_Scan (&(gEoamLmGlobalInfo.LmEoamEnaPortDllList),
                  pListNode, tEoamLmEnaPortDllNode *)
    {
        EoamLmPollGetWindowCounter (pListNode);

        if (pListNode->LmErrInfo.b1InitFlag == OSIX_TRUE)
        {
            EoamLmPollInitCounters (pListNode);
            continue;
        }

        if (pListNode->LmEvtCfgInfo.u1ErrSymPeriodEvNotifEnable == OSIX_TRUE)
        {
            FSAP_U8_SUB (&u8Diff, &(pListNode->LmErrInfo.u8CurTotalSymbols),
                         &(pListNode->LmErrInfo.u8WinStartTotalSymbols));

            i4Compare = FSAP_U8_CMP (&u8Diff, &(pListNode->LmEvtCfgInfo.
                                                u8ErrSymPeriodWindow));

            if (i4Compare >= 0)
            {
                EoamLmPollSymbolPeriod (pListNode);
            }
        }

        if (pListNode->LmEvtCfgInfo.u1ErrFrameEvNotifEnable == OSIX_TRUE)
        {
            pListNode->LmErrInfo.u4FrameWinCounter++;

            if ((pListNode->LmEvtCfgInfo.u4ErrFrameWindow) ==
                (pListNode->LmErrInfo.u4FrameWinCounter))
            {
                EoamLmPollFrameEvent (pListNode);
            }
        }

        if (pListNode->LmEvtCfgInfo.u1ErrFramePeriodEvNotifEnable == OSIX_TRUE)
        {
            if ((pListNode->LmErrInfo.u4CurTotalFrames -
                 pListNode->LmErrInfo.u4FPWinStartTotalFrames) >=
                pListNode->LmEvtCfgInfo.u4ErrFramePeriodWindow)
            {
                EoamLmPollFramePeriod (pListNode);
            }
        }

        if (pListNode->LmEvtCfgInfo.u1ErrFrameSecsEvNotifEnable == OSIX_TRUE)
        {
            EoamLmPollCheckSecSumm (pListNode);
        }
    }

    /* Restart timer */
    if (TmrStartTimer (gEoamLmGlobalInfo.LmTmrList,
                       &gEoamLmGlobalInfo.LmTmrNode,
                       EOAMLM_POLLING_INTERVAL) != TMR_SUCCESS)
    {
        EOAM_TRC ((EOAM_LM_TRC | OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                   "EoamLmPollTimerExpiry: EOAM LM Timer Start failed \r\n"));
    }
    /* Release DataLock for accessing LM info */
    OsixSemGive (gEoamLmGlobalInfo.SemId);
    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EoamLmPollInitCounters
 *
 * Description        : Initialize error statistics and counters
 *
 * Input(s)           : pListNode - Pointer to DLL node which has EOAM LM
 *                                  information
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE VOID
EoamLmPollInitCounters (tEoamLmEnaPortDllNode * pListNode)
{
    EOAM_TRC_FN_ENTRY ();
    /* Initialize window start counters */
    if (pListNode->LmEvtCfgInfo.u1ErrSymPeriodEvNotifEnable == OSIX_TRUE)
    {
        FSAP_U8_ASSIGN (&(pListNode->LmErrInfo.u8WinStartTotalSymbols),
                        &(pListNode->LmErrInfo.u8CurTotalSymbols));

#ifdef NPAPI_WANTED
        if (EOAM_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE)
        {
            EoamEoamLmNpGetStat64 (pListNode->LmEvtCfgInfo.u4Port,
                                   NP_STAT_DOT3_SYMBOL_ERRORS,
                                   &(pListNode->LmErrInfo.u8CurErrorSymbols));
        }
#endif /* NPAPI_WANTED */

        FSAP_U8_ASSIGN (&(pListNode->LmErrInfo.u8WinStartErrorSymbols),
                        &(pListNode->LmErrInfo.u8CurErrorSymbols));

    }

    if ((pListNode->LmEvtCfgInfo.u1ErrFrameEvNotifEnable ==
         OSIX_TRUE) ||
        (pListNode->LmEvtCfgInfo.u1ErrFramePeriodEvNotifEnable ==
         OSIX_TRUE) ||
        (pListNode->LmEvtCfgInfo.u1ErrFrameSecsEvNotifEnable == OSIX_TRUE))
    {

        pListNode->LmErrInfo.u4FPWinStartTotalFrames =
            pListNode->LmErrInfo.u4CurTotalFrames;

        pListNode->LmErrInfo.u4FrameWinStartFrameErrors =
            pListNode->LmErrInfo.u4CurrentErrorFrames;
        pListNode->LmErrInfo.u4FPWinStartFrameErrors =
            pListNode->LmErrInfo.u4CurrentErrorFrames;
        pListNode->LmErrInfo.u4SecSummFrameErrCount =
            pListNode->LmErrInfo.u4CurrentErrorFrames;

        pListNode->LmErrInfo.u4FrameWinCounter = 0;
        pListNode->LmErrInfo.u2SecSummWinCounter = 0;
        pListNode->LmErrInfo.u2SecSummErrSecCounter = 0;
        pListNode->LmErrInfo.u2SecSummRunningCounter = 0;
        pListNode->LmErrInfo.b1InitFlag = OSIX_FALSE;
    }

    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EoamLmPollGetWindowCounter
 *
 * Description        : Get Counters required to determine window crossings
 *
 * Input(s)           : pListNode - Pointer to DLL node which has EOAM LM
 *                                  information
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE VOID
EoamLmPollGetWindowCounter (tEoamLmEnaPortDllNode * pListNode)
{
    UINT4               u4Port = 0;
    EOAM_TRC_FN_ENTRY ();
#ifdef NPAPI_WANTED
    u4Port = pListNode->LmEvtCfgInfo.u4Port;
    if (EOAM_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE)
    {
        if (pListNode->LmEvtCfgInfo.u1ErrSymPeriodEvNotifEnable == OSIX_TRUE)
        {
            /* Read total symbol rx */
            EoamEoamLmNpGetStat64 (u4Port,
                                   NP_STAT_IF_IN_SYMBOLS,
                                   &(pListNode->LmErrInfo.u8CurTotalSymbols));
        }

        if ((pListNode->LmEvtCfgInfo.u1ErrFrameEvNotifEnable == OSIX_TRUE) ||
            (pListNode->LmEvtCfgInfo.u1ErrFramePeriodEvNotifEnable ==
             OSIX_TRUE) ||
            (pListNode->LmEvtCfgInfo.u1ErrFrameSecsEvNotifEnable == OSIX_TRUE))
        {
            /* Read Total Frames Rx */
            EoamEoamLmNpGetStat (u4Port,
                                 NP_STAT_DOT1D_TP_PORT_IN_FRAMES,
                                 &(pListNode->LmErrInfo.u4CurTotalFrames));
            pListNode->LmErrInfo.u4CurTotalFrames =
                EoamLmGetRunningTotalOnModReboot
                (pListNode->LmErrInfo.u4CurTotalFrames,
                 gEoamGlobalInfo.aEoamSaveStats[u4Port - 1].u4InFrames);

            /* Read total Frames errors Rx */
            EoamEoamLmNpGetStat (u4Port,
                                 NP_STAT_IF_IN_ERRORS,
                                 &(pListNode->LmErrInfo.u4CurrentErrorFrames));
            pListNode->LmErrInfo.u4CurrentErrorFrames =
                EoamLmGetRunningTotalOnModReboot
                (pListNode->LmErrInfo.u4CurrentErrorFrames,
                 gEoamGlobalInfo.aEoamSaveStats[u4Port - 1].u4InErrors);
        }
    }
#else
    UNUSED_PARAM (pListNode);
    UNUSED_PARAM (u4Port);
#endif /* NPAPI_WANTED */

    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EoamLmPollSymbolPeriod
 *
 * Description        : This function is called when symbol period window
 *                      has been reached. This function checks whether
 *                      threshold limit has exceeded and if so posts an
 *                      event to EOAM task. Also initializes window start
 *                      counters
 *
 * Input(s)           : pListNode - Pointer to DLL node which has EOAM LM
 *                                  information
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE VOID
EoamLmPollSymbolPeriod (tEoamLmEnaPortDllNode * pListNode)
{
    tEoamThresEventInfo ErrEventInfo;
    FS_UINT8            u8SymbolDiff;
    INT4                i4Compare = 0;

    EOAM_TRC_FN_ENTRY ();

    FSAP_U8_CLR (&u8SymbolDiff);

    MEMSET (&ErrEventInfo, 0, sizeof (tEoamThresEventInfo));

#ifdef NPAPI_WANTED
    if (EOAM_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE)
    {
        /* Read error symbol rx */
        EoamEoamLmNpGetStat64 (pListNode->LmEvtCfgInfo.u4Port,
                               NP_STAT_DOT3_SYMBOL_ERRORS,
                               &(pListNode->LmErrInfo.u8CurErrorSymbols));
    }
#endif /* NPAPI_WANTED */

    EOAM_TRC ((EOAM_LM_TRC, "Symbol Period Window reached on Port %u\n",
               pListNode->LmEvtCfgInfo.u4Port));

    FSAP_U8_SUB (&u8SymbolDiff, &(pListNode->LmErrInfo.u8CurErrorSymbols),
                 &(pListNode->LmErrInfo.u8WinStartErrorSymbols));

    i4Compare = FSAP_U8_CMP (&u8SymbolDiff, &(pListNode->LmEvtCfgInfo.
                                              u8ErrSymPeriodThreshold));

    if (i4Compare >= 0)
    {
        ErrEventInfo.u1EventType = EOAM_ERRORED_SYMBOL_EVENT_TLV;
        FSAP_U8_ASSIGN (&(ErrEventInfo.u8EventWindow),
                        &(pListNode->LmEvtCfgInfo.u8ErrSymPeriodWindow));

        FSAP_U8_ASSIGN (&(ErrEventInfo.u8EventThreshold),
                        &(pListNode->LmEvtCfgInfo.u8ErrSymPeriodThreshold));

        FSAP_U8_ASSIGN (&(ErrEventInfo.u8EventValue), &u8SymbolDiff);

        FSAP_U8_ASSIGN (&(ErrEventInfo.u8ErrorRunningTotal),
                        &(pListNode->LmErrInfo.u8CurErrorSymbols));

        OsixGetSysTime ((tOsixSysTime *) & (ErrEventInfo.u4EventLogTimestamp));

        ++(pListNode->LmErrInfo.u4SymErrEventCounter);
        ErrEventInfo.u4EventRunningTotal =
            pListNode->LmErrInfo.u4SymErrEventCounter;

        EoamApiSendThresholdEvtToPeer (pListNode->LmEvtCfgInfo.u4Port,
                                       &ErrEventInfo);
        EOAM_TRC ((EOAM_LM_TRC, "Sent Symbol Period Event on Port %u\n",
                   pListNode->LmEvtCfgInfo.u4Port));
    }

    FSAP_U8_ASSIGN (&(pListNode->LmErrInfo.u8WinStartTotalSymbols),
                    &(pListNode->LmErrInfo.u8CurTotalSymbols));

    FSAP_U8_ASSIGN (&(pListNode->LmErrInfo.u8WinStartErrorSymbols),
                    &(pListNode->LmErrInfo.u8CurErrorSymbols));

    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EoamLmPollFrameEvent
 *
 * Description        : This function is called when Frame window has
 *                      reached. This function checks whether threshold
 *                      limit has exceeded and if so posts an event to EOAM
 *                      task. Also initializes window start counters
 *
 * Input(s)           : pListNode - Pointer to DLL node which has EOAM LM
 *                                  information
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE VOID
EoamLmPollFrameEvent (tEoamLmEnaPortDllNode * pListNode)
{
    tEoamThresEventInfo ErrEventInfo;
    UINT4               u4FrameErrDiff = 0;

    EOAM_TRC_FN_ENTRY ();

    MEMSET (&ErrEventInfo, 0, sizeof (tEoamThresEventInfo));

    u4FrameErrDiff = pListNode->LmErrInfo.u4CurrentErrorFrames -
        pListNode->LmErrInfo.u4FrameWinStartFrameErrors;

    EOAM_TRC ((EOAM_LM_TRC, "Frame Event Window reached on Port %u\n",
               pListNode->LmEvtCfgInfo.u4Port));

    if ((pListNode->LmEvtCfgInfo.u4ErrFrameThreshold != 0) &&
        (u4FrameErrDiff >= pListNode->LmEvtCfgInfo.u4ErrFrameThreshold))
    {
        ErrEventInfo.u1EventType = EOAM_ERRORED_FRAME_EVENT_TLV;

        FSAP_U8_ASSIGN_HI (&(ErrEventInfo.u8EventWindow), 0);
        FSAP_U8_ASSIGN_LO (&(ErrEventInfo.u8EventWindow),
                           pListNode->LmEvtCfgInfo.u4ErrFrameWindow);

        FSAP_U8_ASSIGN_HI (&(ErrEventInfo.u8EventThreshold), 0);
        FSAP_U8_ASSIGN_LO (&(ErrEventInfo.u8EventThreshold),
                           pListNode->LmEvtCfgInfo.u4ErrFrameThreshold);

        FSAP_U8_ASSIGN_HI (&(ErrEventInfo.u8EventValue), 0);
        FSAP_U8_ASSIGN_LO (&(ErrEventInfo.u8EventValue), u4FrameErrDiff);

        FSAP_U8_ASSIGN_HI (&(ErrEventInfo.u8ErrorRunningTotal), 0);
        FSAP_U8_ASSIGN_LO (&(ErrEventInfo.u8ErrorRunningTotal),
                           pListNode->LmErrInfo.u4CurrentErrorFrames);

        OsixGetSysTime ((tOsixSysTime *) & (ErrEventInfo.u4EventLogTimestamp));

        ++(pListNode->LmErrInfo.u4FrameEventCounter);
        ErrEventInfo.u4EventRunningTotal =
            pListNode->LmErrInfo.u4FrameEventCounter;

        EoamApiSendThresholdEvtToPeer (pListNode->LmEvtCfgInfo.u4Port,
                                       &ErrEventInfo);
        EOAM_TRC ((EOAM_LM_TRC, "Sent Frame Event on Port %u\n",
                   pListNode->LmEvtCfgInfo.u4Port));
    }

    pListNode->LmErrInfo.u4FrameWinCounter = 0;
    pListNode->LmErrInfo.u4FrameWinStartFrameErrors =
        pListNode->LmErrInfo.u4CurrentErrorFrames;

    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EoamLmPollFramePeriod
 *
 * Description        : This function is called when Frame period window
 *                      has been reached. This function checks whether
 *                      threshold limit has exceeded and if so posts an
 *                      event to EOAM task. Also initializes window start
 *                      counters
 *
 * Input(s)           : pListNode - Pointer to DLL node which has EOAM LM
 *                                  information
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE VOID
EoamLmPollFramePeriod (tEoamLmEnaPortDllNode * pListNode)
{
    tEoamThresEventInfo ErrEventInfo;
    UINT4               u4FrameErrDiff = 0;

    EOAM_TRC_FN_ENTRY ();

    MEMSET (&ErrEventInfo, 0, sizeof (tEoamThresEventInfo));

    u4FrameErrDiff = pListNode->LmErrInfo.u4CurrentErrorFrames -
        pListNode->LmErrInfo.u4FPWinStartFrameErrors;

    EOAM_TRC ((EOAM_LM_TRC, "Frame Period Window reached on Port %u\n",
               pListNode->LmEvtCfgInfo.u4Port));
    if ((pListNode->LmEvtCfgInfo.u4ErrFramePeriodThreshold != 0) &&
        (u4FrameErrDiff >= pListNode->LmEvtCfgInfo.u4ErrFramePeriodThreshold))
    {
        ErrEventInfo.u1EventType = EOAM_ERRORED_FRAME_PERIOD_EVENT_TLV;

        FSAP_U8_ASSIGN_HI (&(ErrEventInfo.u8EventWindow), 0);
        FSAP_U8_ASSIGN_LO (&(ErrEventInfo.u8EventWindow),
                           pListNode->LmEvtCfgInfo.u4ErrFramePeriodWindow);

        FSAP_U8_ASSIGN_HI (&(ErrEventInfo.u8EventThreshold), 0);
        FSAP_U8_ASSIGN_LO (&(ErrEventInfo.u8EventThreshold),
                           pListNode->LmEvtCfgInfo.u4ErrFramePeriodThreshold);

        FSAP_U8_ASSIGN_HI (&(ErrEventInfo.u8EventValue), 0);
        FSAP_U8_ASSIGN_LO (&(ErrEventInfo.u8EventValue), u4FrameErrDiff);

        FSAP_U8_ASSIGN_HI (&(ErrEventInfo.u8ErrorRunningTotal), 0);
        FSAP_U8_ASSIGN_LO (&(ErrEventInfo.u8ErrorRunningTotal),
                           pListNode->LmErrInfo.u4CurrentErrorFrames);

        OsixGetSysTime ((tOsixSysTime *) & (ErrEventInfo.u4EventLogTimestamp));

        ++(pListNode->LmErrInfo.u4FramePerEventCounter);
        ErrEventInfo.u4EventRunningTotal =
            pListNode->LmErrInfo.u4FramePerEventCounter;

        EoamApiSendThresholdEvtToPeer (pListNode->LmEvtCfgInfo.u4Port,
                                       &ErrEventInfo);
        EOAM_TRC ((EOAM_LM_TRC, "Sent Frame Period Event on Port %u\n",
                   pListNode->LmEvtCfgInfo.u4Port));
    }

    pListNode->LmErrInfo.u4FPWinStartFrameErrors =
        pListNode->LmErrInfo.u4CurrentErrorFrames;
    pListNode->LmErrInfo.u4FPWinStartTotalFrames =
        pListNode->LmErrInfo.u4CurTotalFrames;

    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EoamLmPollCheckSecSumm
 *
 * Description        : Check for seconds summary event
 *
 * Input(s)           : pListNode - Pointer to DLL node which has EOAM LM
 *                                  information
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE VOID
EoamLmPollCheckSecSumm (tEoamLmEnaPortDllNode * pListNode)
{
    EOAM_TRC_FN_ENTRY ();
    pListNode->LmErrInfo.u2SecSummWinCounter++;
    /* Observe whether there is any frame error if 
     * one second has completed */
    if (EOAMLM_TENTH_SEC_SINCE_LAST_SEC (pListNode->LmErrInfo.
                                         u2SecSummWinCounter) == 0)
    {
        /* For each 1 sec interval */
        if (pListNode->LmErrInfo.u4CurrentErrorFrames >
            pListNode->LmErrInfo.u4SecSummFrameErrCount)
        {
            /* Atleast one error frame is rx in last sec */
            pListNode->LmErrInfo.u2SecSummErrSecCounter++;
            pListNode->LmErrInfo.u2SecSummRunningCounter++;
            pListNode->LmErrInfo.u4SecSummFrameErrCount =
                pListNode->LmErrInfo.u4CurrentErrorFrames;
        }
    }

    if ((pListNode->LmEvtCfgInfo.u4ErrFrameSecsSummaryWindow) ==
        (pListNode->LmErrInfo.u2SecSummWinCounter))
    {
        EoamLmPollSecondsSummary (pListNode);
    }

    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EoamLmPollSecondsSummary
 *
 * Description        : This function is called when Seconds summary window
 *                      is reached. This function checks whether threshold
 *                      limit has exceeded and if so posts an event to EOAM
 *                      task. Also initializes window start counters
 *
 * Input(s)           : pListNode - Pointer to DLL node which has EOAM LM
 *                                  information
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
PRIVATE VOID
EoamLmPollSecondsSummary (tEoamLmEnaPortDllNode * pListNode)
{
    tEoamThresEventInfo ErrEventInfo;

    EOAM_TRC_FN_ENTRY ();

    MEMSET (&ErrEventInfo, 0, sizeof (tEoamThresEventInfo));

    EOAM_TRC ((EOAM_LM_TRC, "Seconds summary event window reached on "
               "Port %u\n", pListNode->LmEvtCfgInfo.u4Port));

    if ((pListNode->LmEvtCfgInfo.u4ErrFrameSecsSummaryThreshold != 0) &&
        (pListNode->LmErrInfo.u2SecSummErrSecCounter >=
         pListNode->LmEvtCfgInfo.u4ErrFrameSecsSummaryThreshold))
    {
        ErrEventInfo.u1EventType = EOAM_ERRORED_FRAME_SECONDS_EVENT_TLV;

        FSAP_U8_ASSIGN_HI (&(ErrEventInfo.u8EventWindow), 0);
        FSAP_U8_ASSIGN_LO (&(ErrEventInfo.u8EventWindow),
                           pListNode->LmEvtCfgInfo.u4ErrFrameSecsSummaryWindow);

        FSAP_U8_ASSIGN_HI (&(ErrEventInfo.u8EventThreshold), 0);
        FSAP_U8_ASSIGN_LO (&(ErrEventInfo.u8EventThreshold),
                           pListNode->LmEvtCfgInfo.
                           u4ErrFrameSecsSummaryThreshold);

        FSAP_U8_ASSIGN_HI (&(ErrEventInfo.u8EventValue), 0);
        FSAP_U8_ASSIGN_LO (&(ErrEventInfo.u8EventValue),
                           pListNode->LmErrInfo.u2SecSummErrSecCounter);

        FSAP_U8_ASSIGN_HI (&(ErrEventInfo.u8ErrorRunningTotal), 0);
        FSAP_U8_ASSIGN_LO (&(ErrEventInfo.u8ErrorRunningTotal),
                           pListNode->LmErrInfo.u2SecSummRunningCounter);

        OsixGetSysTime ((tOsixSysTime *) & (ErrEventInfo.u4EventLogTimestamp));

        ++(pListNode->LmErrInfo.u4SecSummEventCounter);
        ErrEventInfo.u4EventRunningTotal =
            pListNode->LmErrInfo.u4SecSummEventCounter;

        EoamApiSendThresholdEvtToPeer (pListNode->LmEvtCfgInfo.u4Port,
                                       &ErrEventInfo);
        EOAM_TRC ((EOAM_LM_TRC, "Sent Seconds summary Event on Port %u\n",
                   pListNode->LmEvtCfgInfo.u4Port));
    }

    pListNode->LmErrInfo.u2SecSummErrSecCounter = 0;
    pListNode->LmErrInfo.u2SecSummWinCounter = 0;

    EOAM_TRC_FN_EXIT ();
    return;
}

/*****************************************************************************/
/* Function Name      : EoamLmSaveHwStat                                     */
/*                                                                           */
/* Description        : This Function is called when EOAM module is shutdown */
/*                      This will save current value of the frame and error  */
/*                      counters in a Global Structure. This is done in order*/
/*                      reset the value of frame event/ frame seconds event/ */
/*                      frame secs summary event on Module reboot            */
/*                                                                           */
/* Input(s)           : u4Port - Port Number                                 */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gEoamGlobalInfo                                      */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
EoamLmSaveHwStat (UINT4 u4Port)
{
    UINT4               u4Value = 0;

    EOAM_TRC ((EOAM_LM_TRC,
               "\rEoamHwStartSessionCounters invoked for port %d\n", u4Port));

#ifdef  NPAPI_WANTED
    EoamEoamLmNpGetStat (u4Port, NP_STAT_DOT1D_TP_PORT_IN_FRAMES, &(u4Value));
    gEoamGlobalInfo.aEoamSaveStats[u4Port - 1].u4InFrames = u4Value;

    u4Value = 0;
    EoamEoamLmNpGetStat (u4Port, NP_STAT_IF_IN_ERRORS, &(u4Value));
    gEoamGlobalInfo.aEoamSaveStats[u4Port - 1].u4InErrors = u4Value;

#else
    UNUSED_PARAM (u4Value);
    UNUSED_PARAM (u4Port);        /* Added to avoid warning when the TRACE_WANTED
                                   and NPAPI_WANTED compilation switches are 
                                   disabled */
#endif

    EOAM_TRC ((EOAM_LM_TRC, "Eoam counter collection success \n"));

    return;
}

#ifdef NPAPI_WANTED
/****************************************************************************/
/*   FUNCTION NAME           : EoamLmGetRunningTotalOnModReboot             */
/*   DESCRIPTION             : This function gets the number of frames and  */
/*                             error counters since last reset              */
/*                                                                          */
/*   INPUT (S)               : u4CurrVal - Current retreived value of the   */
/*                             frame /frame secs summary /frame period evts */
/*                             u4StoredVal - Value stored during module     */
/*                                           shutdown                       */
/*                                                                          */
/*   OUTPUT (S)              : u4StoredVal - Value stored during module shut*/
/*                                                                          */
/*   GLOBAL VARIABLES REFERRED  : None                                      */
/*                                                                          */
/*   GLOBAL VARIABLES MODIFIED  : None                                      */
/*                                                                          */
/*   RETURNS                 : EventLogRunningTotal on module reboot        */
/*                                                                          */
/****************************************************************************/
PRIVATE UINT4
EoamLmGetRunningTotalOnModReboot (UINT4 u4CurrVal, UINT4 u4StoredVal)
{
    /* This function exactly gets the number of frames and error counters
     *  since the last reset except wrap around scenario of u4CurrVal. */
    if (u4StoredVal > u4CurrVal)
    {
        u4StoredVal = 0;
    }
    return (u4CurrVal - u4StoredVal);
}
#endif

/*-----------------------------------------------------------------------*/
/*                       End of the file  emlmpoll.c                     */
/*-----------------------------------------------------------------------*/
