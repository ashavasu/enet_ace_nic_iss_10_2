
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emmod.c,v 1.22 2013/12/20 03:49:10 siva Exp $
 *
 * Description: This file contains procedures to 
 *              - start/shutdown EOAM module.
 *              - enable/disable EOAM module,
 *********************************************************************/
#include "eminc.h"
#include "emlmglob.h"
#include "mux.h"
extern VOID EoamCltInfTriggerCriticalEvtMsg PROTO ((tEoamPortInfo *, UINT1));
extern VOID EoamCltInfTrigDyingGaspOnAllPorts PROTO ((UINT1 u1State));
#define EMFM_DELAY     5
/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamHandleModuleStart 
 *                                                                          
 *    DESCRIPTION      : This function allocates memory pools for all tables 
 *                       in EOAM module. It also initalizes the global 
 *                       structure.
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamHandleModuleStart (VOID)
{
    EOAM_TRC_FN_ENTRY ();

    EoamMainInitGlobalInfo ();

    if (EoamMainMemInit () == OSIX_FAILURE)
    {
        EOAM_TRC ((INIT_SHUT_TRC | ALL_FAILURE_TRC,
                   "EoamHandleModuleStart: Memory Initialization FAILED !!!\n"));
        EoamHandleModuleShutDown ();
        return (OSIX_FAILURE);
    }
    EoamMainAssignMempoolIds ();
    /* Timer Initialization */
    if (EoamTmrInit () == OSIX_FAILURE)
    {
        EOAM_TRC ((INIT_SHUT_TRC | ALL_FAILURE_TRC,
                   "EoamHandleModuleStart: Timer Initialization FAILED !!!\n"));
        EoamHandleModuleShutDown ();
        return (OSIX_FAILURE);
    }

    /* EOAM has been successfully initialized. Make the System Control START */
    gEoamGlobalInfo.u1SystemCtrlStatus = EOAM_START;
    EOAM_TRC ((INIT_SHUT_TRC, "EoamHandleModuleStart: System Control status set"
               " to START\n"));
#ifdef NPAPI_WANTED
    if (EoamEoamNpInit () == FNP_FAILURE)
    {
        EoamHandleModuleShutDown ();
        return OSIX_FAILURE;
    }
#endif

    EoamIfCreateAllPorts ();
    EoamCltInfTrigDyingGaspOnAllPorts (OSIX_FALSE);
    if (EoamLmMainInit () != OSIX_SUCCESS)
    {
        EOAM_TRC ((INIT_SHUT_TRC | ALL_FAILURE_TRC,
                   "EoamHandleModuleStart: LM Init FAILED!!!\n"));
        EoamHandleModuleShutDown ();
        return (OSIX_FAILURE);
    }

    EoamRedInitRedGlobalInfo ();
    /* Get the Node Status and Num peers from RM. */
#ifdef L2RED_WANTED
    EOAM_RM_NODE_STATUS () = EOAM_IDLE_NODE;
#else
    EOAM_RM_GET_NODE_STATUS ();
#endif
    EOAM_RM_GET_NUM_STANDBY_NODES_UP ();
    /* Register with Redundancy manager */
    if (EoamRedRegisterWithRM () == OSIX_FAILURE)
    {
        EOAM_TRC ((INIT_SHUT_TRC | ALL_FAILURE_TRC,
                   "EoamHandleModuleStart: Registration with RM failed!!!\n"));
        EoamHandleModuleShutDown ();
        return (OSIX_FAILURE);
    }

    EOAM_TRC ((CONTROL_PLANE_TRC,
               "EoamHandleModuleStart: EOAM Module Initialized successfully\n"));
    EOAM_TRC_FN_EXIT ();

    if (EoamRegisterWithPacketHandler () != OSIX_SUCCESS)
    {
        EOAM_TRC ((INIT_SHUT_TRC | ALL_FAILURE_TRC,
                   "EoamHandleModuleStart: Registration with Packet handler failed!!!\n"));
        EoamHandleModuleShutDown ();
        return OSIX_FAILURE;
    }

    return (OSIX_SUCCESS);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamHandleModuleShutDown 
 *                                                                          
 *    DESCRIPTION      : This is function is called during system shutdown or
 *                       when the manager wants to shut the EOAM module.
 *
 *    INPUT            : None.
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EoamHandleModuleShutDown (VOID)
{
    tEoamQMsg          *pQMsg = NULL;
#ifdef L2RED_WANTED
    tRmNodeInfo        *pData = NULL;
#endif
    tEoamPortInfo      *pPortEntry = NULL;
    UINT4               u4PortIndex = 0;

    EOAM_TRC_FN_ENTRY ();

    /* Shut down the system control status */
    gEoamGlobalInfo.u1SystemCtrlStatus = EOAM_SHUTDOWN;

    while (OsixQueRecv (gEoamGlobalInfo.QId, (UINT1 *) &pQMsg,
                        OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
    {
        switch (pQMsg->u4MsgType)
        {
#ifdef MBSM_WANTED
            case MBSM_MSG_CARD_INSERT:
            case MBSM_MSG_CARD_REMOVE:
                CRU_BUF_Release_MsgBufChain (pQMsg->unEoamMsgParam.
                                             pMbsmProtoMsg, FALSE);
                break;
#endif /* MBSM_WANTED */

#ifdef L2RED_WANTED
            case EOAM_RM_MSG:
                if (pQMsg->unEoamMsgParam.RmEoamMsg.u1RmEvent == RM_MESSAGE)
                {
                    /* Processing of message is over, hence free the RM message. */
                    CRU_BUF_Release_MsgBufChain (pQMsg->unEoamMsgParam.
                                                 RmEoamMsg.pData, FALSE);
                }
                else if ((pQMsg->unEoamMsgParam.RmEoamMsg.u1RmEvent
                          == RM_STANDBY_UP) ||
                         ((pQMsg->unEoamMsgParam.RmEoamMsg.u1RmEvent
                           == RM_STANDBY_DOWN)))
                {

                    pData =
                        (tRmNodeInfo *) pQMsg->unEoamMsgParam.RmEoamMsg.pData;
                    RmReleaseMemoryForMsg ((UINT1 *) pData);
                }
                break;
#endif
        }
        /* Release the buffer to pool */
        MemReleaseMemBlock (gEoamGlobalInfo.QMsgPoolId, (UINT1 *) pQMsg);
    }
    EoamCltInfTrigDyingGaspOnAllPorts (OSIX_TRUE);

    /*This delay is introduced to send OAMPDU Event PDUs */
    OsixTskDelay (EMFM_DELAY);

    /* Disable EOAM module */
    EoamModuleDisable ();

    /* Remove the port specific info from port table. */
    for (u4PortIndex = EOAM_MIN_PORTS; u4PortIndex <= EOAM_MAX_PORTS;
         u4PortIndex++)
    {
        EoamUtilGetPortEntry (u4PortIndex, &pPortEntry);
        if (pPortEntry != NULL)
        {
            EoamIfDisable (pPortEntry);
            EoamMainClearPortInfo (pPortEntry);
        }
    }

    /* Deinit Timer */
    if (EoamTmrDeInit () == OSIX_FAILURE)
    {
        EOAM_TRC ((INIT_SHUT_TRC,
                   "EoamHandleModuleShutDown: Timer DeInit FAILED!!!\n"));
    }

    /* Shut down Link monitoring module */
    if (gu1EoamLMStatFlag == EOAM_LM_ENABLED)
    {
        EoamLmMainShutdown ();
    }
    /* Delete all mempools & buddy memory */
    EoamMainMemClear ();

    EoamRedInitRedGlobalInfo ();
    EoamRedDeRegisterWithRM ();
    CfaMuxUnRegisterProtocolCallBack (PACKET_HDLR_PROTO_EOAM);
    EOAM_TRC_FN_EXIT ();
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamModuleEnable 
 *                                                                          
 *    DESCRIPTION      : This function is called to enable EOAM module from
 *                       disabled state. 
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamModuleEnable (VOID)
{
    tTMO_DLL_NODE      *pEnaNode = NULL;
    tEoamPortInfo      *pPortInfo = NULL;
    UINT4               u4PortIndex;
    EOAM_TRC_FN_ENTRY ();

    EOAM_TRC ((INIT_SHUT_TRC, "EoamModuleEnable: Enabling EOAM module\n"));

    /*
     * Configurations are restored before GO_ACTIVE/GO_STANDBY event is
     * received by protocol.Hence if EOAM enable is called before GO_ACTIVE/
     * GO_STANDBY event is received then store that in u1ModuleAdminStatus. In
     * EoamRedMakeNodeActive based on the u1EoamAdminstaus enable/disable EOAM.
     */
    for (u4PortIndex = EOAM_MIN_PORTS; u4PortIndex < EOAM_MAX_PORTS;
         u4PortIndex++)
    {
        EoamLmSaveHwStat (u4PortIndex);
    }
    gEoamGlobalInfo.u1ModuleAdminStatus = EOAM_ENABLED;
    if (EOAM_RM_NODE_STATUS () == EOAM_IDLE_NODE)
    {
        return OSIX_SUCCESS;
    }

    if (EOAM_RM_NODE_STATUS () == EOAM_ACTIVE_NODE)
    {
        /* Start the global PDU Tx Timer */
        if (TmrStart (gEoamGlobalInfo.EoamTmrListId,
                      &(gEoamGlobalInfo.PduTxTmr),
                      EOAM_PDU_TX_TMR_TYPE, EOAM_PDU_TX_TIMEOUT, 0) ==
            TMR_FAILURE)
        {
            EOAM_TRC ((OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                       "EoamModuleEnable: PduTx Start Timer FAILED !!!\n"));
            return (OSIX_FAILURE);
        }

        /* Enable EOAM module status - This should be done before
         * updating Sem machine */
        gEoamGlobalInfo.u1ModuleStatus = EOAM_ENABLED;

        /* Scan the EOAM enabled ports in the DLL and start the discovery 
         * state machine for all EOAM enabled ports. */
        TMO_DLL_Scan (&(gEoamGlobalInfo.EoamEnaPortDllList),
                      pEnaNode, tTMO_DLL_NODE *)
        {
            pPortInfo = (tEoamPortInfo *) pEnaNode;

            FSAP_ASSERT (pPortInfo != NULL);

            if (pPortInfo->u1OperStatus == CFA_IF_UP)
            {
                EOAM_TRC ((CONTROL_PLANE_TRC,
                           "EoamModuleEnable: Discovery State"
                           " Machine initiated for port %u\n",
                           pPortInfo->u4Port));
                EoamCltInfTriggerCriticalEvtMsg (pPortInfo, OSIX_FALSE);
                EoamSemMachine (pPortInfo, EOAM_DISC_EVENT_OPER_UP);
            }
        }
    }

    EOAM_TRC ((CONTROL_PLANE_TRC, "Enabled EOAM Module\n"));

    EOAM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamModuleDisable 
 *                                                                          
 *    DESCRIPTION      : This function is called to disable EOAM module. 
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EoamModuleDisable (VOID)
{
    tEoamPortInfo      *pPortInfo = NULL;
    tTMO_DLL_NODE      *pEnaNode = NULL;

    EOAM_TRC_FN_ENTRY ();

    EOAM_TRC ((INIT_SHUT_TRC, "EoamModuleDisable: Disabling EOAM module\n"));
    gEoamGlobalInfo.u1ModuleAdminStatus = EOAM_DISABLED;

    /* Stop the global PDU Tx Timer */
    if (TmrStop (gEoamGlobalInfo.EoamTmrListId, &(gEoamGlobalInfo.PduTxTmr))
        == TMR_FAILURE)
    {
        EOAM_TRC ((OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                   "EoamModuleDisable: PDU Tx Stop Timer FAILED !!! \n"));
    }
    /* Disable EOAM module status - This should be done before
     * updating Sem machine */
    gEoamGlobalInfo.u1ModuleStatus = EOAM_DISABLED;

    /* Scan the EOAM enabled ports in the DLL and stop the discovery 
     * state machine for all EOAM enabled ports. */
    TMO_DLL_Scan (&(gEoamGlobalInfo.EoamEnaPortDllList),
                  pEnaNode, tTMO_DLL_NODE *)
    {
        pPortInfo = (tEoamPortInfo *) pEnaNode;

        FSAP_ASSERT (pPortInfo != NULL);

        EOAM_TRC ((CONTROL_PLANE_TRC,
                   "EoamModuleDisable: Discovery State Machine"
                   " stopped for port %u\n", pPortInfo->u4Port));
        EoamCltInfTriggerCriticalEvtMsg (pPortInfo, OSIX_TRUE);
        EoamSemMachine (pPortInfo, EOAM_DISC_EVENT_OPER_DOWN);
    }

#ifdef NPAPI_WANTED
    EoamEoamNpDeInit ();
#endif
    EOAM_TRC ((CONTROL_PLANE_TRC, "******  Disabled EOAM ******\n"));

    EOAM_TRC_FN_EXIT ();
    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  emmod.c                        */
/*-----------------------------------------------------------------------*/
