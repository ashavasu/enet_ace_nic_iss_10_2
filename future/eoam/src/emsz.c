/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emsz.c,v 1.4 2013/11/29 11:04:12 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _EOAMSZ_C
#include "eminc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
EoamSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < EOAM_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsEOAMSizingParams[i4SizingId].u4StructSize,
                              FsEOAMSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(EOAMMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            EoamSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
EoamSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsEOAMSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, EOAMMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
EoamSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < EOAM_MAX_SIZING_ID; i4SizingId++)
    {
        if (EOAMMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (EOAMMemPoolIds[i4SizingId]);
            EOAMMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
