/*******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emcltres.c,v 1.9 2012/04/18 14:14:43 siva Exp $
 *
 * Description: This file conatains the functions related to Variable Response
 * PDU handling by EOAM client module. 
 ******************************************************************************/
#include "eminc.h"

PRIVATE VOID EoamCltResProcessRBNode PROTO ((UINT1 **, tEoamVarReqRBTNode *,
                                             UINT4 *));
/******************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamCltResAccumulate 
 *                                                                          
 *    DESCRIPTION      : This function is called when CMIP Interface returns a 
 *                       response for a requested descriptor. Based on the 
 *                       Request ID, appropriate RBTree is scanned for the 
 *                       descriptor and the response is stored. If all the 
 *                       nodes in the RBTree have received responses, the 
 *                       corresponding Timer is stopped and a function is called
 *                       to send out Variable Response OAMPDU.
 *
 *    INPUT            : pPortEntry - pointer to port information structure
 *                       pRespData - pointer thr response returned from CMIP  
 *                                                                          
 *    OUTPUT           : None.                                              
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE                        
 *                                                                          
******************************************************************************/
PUBLIC INT4
EoamCltResAccumulate (tEoamPortInfo * pPortEntry, tEoamVarReqResp * pRespData)
{
    tEoamVarReqRBTNode  ReqRespKey;
    tEoamVarReqRBTNode *pRBTNode = NULL;
    tEoamEnaPortInfo   *pActivePortInfo = NULL;
    tRBElem            *pRBElem = NULL;
    UINT4               u4Index = 0;

    EOAM_TRC_FN_ENTRY ();
    if (gEoamGlobalInfo.u1ModuleStatus != EOAM_ENABLED)
    {
        EOAM_TRC ((ALL_FAILURE_TRC | EOAM_VAR_REQRESP_TRC,
                   "EOAM is globally DISABLED!!!\n"));
        return OSIX_FAILURE;
    }
    if (pPortEntry == NULL)
    {
        return OSIX_SUCCESS;
    }
    MEMSET (&ReqRespKey, 0, sizeof (tEoamVarReqRBTNode));
    FSAP_ASSERT (pRespData != NULL);
    pActivePortInfo = pPortEntry->pEoamEnaPortInfo;
    FSAP_ASSERT (pActivePortInfo != NULL);

    u4Index = (pRespData->u4ReqId) % EOAM_MAX_VAR_REQS;
    u4Index = ((u4Index == 0) ? EOAM_MAX_VAR_REQS : u4Index);
    ReqRespKey.u1Branch = pRespData->u1Branch;
    ReqRespKey.u2Leaf = pRespData->u2Leaf;

    if (pActivePortInfo->pVarReq[u4Index - 1] == NULL)
    {
        EOAM_TRC ((ALL_FAILURE_TRC | EOAM_VAR_REQRESP_TRC,
                   "CLIENT: No corresponding RBTree found for response"
                   "on port %u\n", pPortEntry->u4Port));
        return OSIX_FAILURE;
    }
    pRBElem = RBTreeGet (pActivePortInfo->pVarReq[u4Index - 1]->RBTVarReq,
                         (tRBElem *) & ReqRespKey);
    if (pRBElem == NULL)
    {
        EOAM_TRC ((ALL_FAILURE_TRC | EOAM_VAR_REQRESP_TRC,
                   "CLIENT:RBTreeGet failed on port %u\n", pPortEntry->u4Port));
        return OSIX_FAILURE;
    }
    pRBTNode = (tEoamVarReqRBTNode *) pRBElem;
    pRBTNode->u4DataLen = pRespData->RespData.u4DataLen;
    pRBTNode->pu1Data = pRespData->RespData.pu1Data;
    pActivePortInfo->pVarReq[u4Index - 1]->u4Count--;
    if (pActivePortInfo->pVarReq[u4Index - 1]->u4Count == 0)
    {
        TmrStop (gEoamGlobalInfo.EoamTmrListId,
                 &(pActivePortInfo->pVarReq[u4Index - 1]->ReqRespAppTmr));
        EoamCltResSendToPeer (pPortEntry,
                              pActivePortInfo->pVarReq[u4Index - 1]);
    }
    EOAM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/******************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamCltResSendToPeer 
 *                                                                          
 *    DESCRIPTION      : This function is called either when response has been
 *                       received from CMIP for all the descriptors or when the
 *                       corresponding timer has expired. All the responses are
 *                       collected and sent to the control module for 
 *                       transmission to the peer.
 *                                                                          
 *    INPUT            : pPortEntry - pointer to port specific information 
 *                       pReqResp - pointer to the structure related to a 
 *                                  particular Request ID.
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *                                                                         
*******************************************************************************/
PUBLIC INT4
EoamCltResSendToPeer (tEoamPortInfo * pPortEntry, tEoamVarReq * pReqResp)
{
    UINT4               u4MaxOamPduSize = 0;
    UINT4               u4RespLength = 0;
    UINT4               u4Index = 0;
    UINT1              *pu1RespBuf = NULL;
    UINT1               u1VarIndicationFilled = OSIX_FALSE;
    tEoamPduHeader      PduHeader;
    tEoamPduData        PduData;
    UINT1              *pu1RespData = NULL;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    tEoamVarReqRBTNode *pRBTNode = NULL;

    EOAM_TRC_FN_ENTRY ();
    FSAP_ASSERT (pPortEntry != NULL);
    FSAP_ASSERT (pReqResp != NULL);

    if ((pu1RespBuf =
         (UINT1 *) (MemAllocMemBlk (gEoamGlobalInfo.EmDataBufPoolId))) == NULL)
    {
        EOAM_TRC ((ALL_FAILURE_TRC | EOAM_CRITICAL_TRC,
                   "EmLinearBufPoolId : Buffer allocation failed\r\n"));
        return OSIX_FAILURE;
    }
    MEMSET (pu1RespBuf, 0, EOAM_MAX_DATA_SIZE);
    MEMSET (&PduHeader, 0, sizeof (tEoamPduHeader));
    MEMSET (&PduData, 0, sizeof (tEoamPduData));
    pu1RespData = pu1RespBuf;
    u4Index = (pReqResp->u4ReqId) % EOAM_MAX_VAR_REQS;
    u4Index = ((u4Index == 0) ? EOAM_MAX_VAR_REQS : u4Index);
    /* OAMPDUs other than Information PDUs can be sent out only when
     * discovery has settled down */
    if (pPortEntry->LocalInfo.u2EoamOperStatus != EOAM_OPER_OPERATIONAL)
    {
        EOAM_TRC ((ALL_FAILURE_TRC | EOAM_VAR_REQRESP_TRC,
                   "CLIENT: Cannot send out Variable Response OAMPDU when"
                   "discovery has not settled down on port %u\n ",
                   pPortEntry->u4Port));

        MemReleaseMemBlock (gEoamGlobalInfo.EmDataBufPoolId,
                            (UINT1 *) pu1RespBuf);
        return OSIX_FAILURE;
    }
    EoamCtlTxFormPduHeader (pPortEntry, EOAM_VAR_RESP, &PduHeader);

    pRBElem = RBTreeGetFirst (pReqResp->RBTVarReq);
    while (pRBElem != NULL)
    {
        pRBNextElem = RBTreeGetNext (pReqResp->RBTVarReq, pRBElem, NULL);

        pRBTNode = (tEoamVarReqRBTNode *) pRBElem;

        u1VarIndicationFilled = OSIX_FALSE;
        /*If the variable container exceeds the OAMPDU data size, a variable 
         * indication with value 0x01 (0x80 | 0x01) should be returned */
        if (((pRBTNode->u4DataLen) + EOAM_VAR_DESC_LEN) >
            (UINT4) (pPortEntry->LocalInfo.u2MaxOamPduSize -
                     (EOAM_PDU_HEADER_SIZE + EOAM_ETH_FCS_LEN)))
        {
            EOAM_PUT_1BYTE (pu1RespData, pRBTNode->u1Branch);
            EOAM_PUT_2BYTE (pu1RespData, pRBTNode->u2Leaf);
            u4RespLength += EOAM_VAR_DESC_LEN;
            EOAM_PUT_1BYTE (pu1RespData, EOAM_IND_CONTAINER_TOO_LONG);
            u4RespLength++;
            pRBTNode->u4DataLen = 0;
            u1VarIndicationFilled = OSIX_TRUE;
        }

        u4MaxOamPduSize = pPortEntry->LocalInfo.u2MaxOamPduSize;

        if (((pPortEntry->LocalInfo.u2MaxOamPduSize) >
             (pPortEntry->RemoteInfo.u2MaxOamPduSize)) &&
            (pPortEntry->RemoteInfo.u2MaxOamPduSize != 0))
        {
            u4MaxOamPduSize = pPortEntry->RemoteInfo.u2MaxOamPduSize;
        }

        if ((u4RespLength + (pRBTNode->u4DataLen) + EOAM_VAR_DESC_LEN) >
            (u4MaxOamPduSize - (EOAM_PDU_HEADER_SIZE + EOAM_ETH_FCS_LEN)))
        {
            PduData.ExternalPduData.u4DataLen = u4RespLength;
            PduData.ExternalPduData.pu1Data = pu1RespBuf;
            if (EoamCtlTxPduRequest (pPortEntry, &PduHeader, &PduData) ==
                OSIX_SUCCESS)
            {
                pPortEntry->PduStats.u4VariableResponseTx++;
            }
            MEMSET (pu1RespBuf, 0, EOAM_MAX_DATA_SIZE);
            pu1RespData = pu1RespBuf;
            u4RespLength = 0;
        }
        if (u1VarIndicationFilled == OSIX_FALSE)
        {
            EoamCltResProcessRBNode (&pu1RespData, pRBTNode, &u4RespLength);
        }

        if (pRBTNode->pu1Data != NULL)
        {
            EOAM_BUDDY_FREE (pRBTNode->pu1Data);
        }

        RBTreeRemove (pReqResp->RBTVarReq, (tRBElem *) pRBTNode);
        MemReleaseMemBlock (gEoamGlobalInfo.VarContainerPoolId,
                            (UINT1 *) pRBTNode);
        pRBElem = pRBNextElem;
    }
    PduData.ExternalPduData.u4DataLen = u4RespLength;
    PduData.ExternalPduData.pu1Data = pu1RespBuf;
    if (EoamCtlTxPduRequest (pPortEntry, &PduHeader, &PduData) != OSIX_SUCCESS)
    {
        EOAM_TRC ((ALL_FAILURE_TRC | EOAM_VAR_REQRESP_TRC,
                   "CLIENT:EoamCtlTxPduRequest call for Var Response PDU"
                   "failed on port %u\n", pPortEntry->u4Port));
    }
    else
    {
        pPortEntry->PduStats.u4VariableResponseTx++;
    }
    RBTreeDestroy (pReqResp->RBTVarReq, EoamUtilRBTFreeNodeFn, 0);
    MemReleaseMemBlock (gEoamGlobalInfo.VarReqPoolId, (UINT1 *) pReqResp);
    MemReleaseMemBlock (gEoamGlobalInfo.EmDataBufPoolId, (UINT1 *) pu1RespBuf);
    pPortEntry->pEoamEnaPortInfo->pVarReq[u4Index - 1] = NULL;
    EOAM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/******************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamCltResProcessRBNode 
 *                                                                          
 *    DESCRIPTION      : This function gets the required response data for each
 *                       variable container and forms the PDU data
 *                                                                          
 *    INPUT            : ppu1RespData - pointer to PDU data which is to be 
 *                                      filled 
 *                       pRBTNode - pointer to the RB Tree node
 *                       pu4RespLength - pointer to the length of the formed 
 *                                       response PDU 
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : None.
 *                                                                         
*******************************************************************************/
PRIVATE VOID
EoamCltResProcessRBNode (UINT1 **ppu1RespData, tEoamVarReqRBTNode * pRBTNode,
                         UINT4 *pu4RespLength)
{
    EOAM_TRC_FN_ENTRY ();

    EOAM_PUT_1BYTE (*ppu1RespData, pRBTNode->u1Branch);
    EOAM_PUT_2BYTE (*ppu1RespData, pRBTNode->u2Leaf);
    *pu4RespLength += EOAM_VAR_DESC_LEN;

    if ((pRBTNode->u4DataLen == 0) && (pRBTNode->pu1Data == NULL))
    {
        EOAM_PUT_1BYTE (*ppu1RespData,
                        EOAM_FORM_VARIABLE_INDICATION (pRBTNode->u1Branch));
        (*pu4RespLength)++;
    }
    if ((pRBTNode->u4DataLen == 1) &&
        (EOAM_IS_VARIABLE_INDICATION (pRBTNode->pu1Data[0]) == OSIX_TRUE))
    {
        EOAM_PUT_1BYTE (*ppu1RespData, pRBTNode->pu1Data[0]);
        (*pu4RespLength)++;
    }
    if (pRBTNode->u4DataLen > 1)
    {
        MEMCPY (*ppu1RespData, pRBTNode->pu1Data, pRBTNode->u4DataLen);
        *ppu1RespData += pRBTNode->u4DataLen;
        *pu4RespLength += pRBTNode->u4DataLen;
    }

    EOAM_TRC_FN_EXIT ();
    return;
}

/*******************************************************************************
 *                                                                          
 *    FUNCTION NAME    :  EoamCltResPduProcess 
 *                                                                          
 *    DESCRIPTION      :  This function is called on reception of a          
 *                        Variable Response OAMPDU. It just forwards the    
 *                        received response to the Fault Management Module. 
 *                                                                          
 *    INPUT            :  pPortInfo - pointer to port specific info          
 *                        pData - pointer to PDU Data                       
 *                                                                          
 *    OUTPUT           :  None.                                             
 *                                                                          
 *    RETURNS          :  OSIX_SUCCESS / OSIX_FAILURE                       
 *                                                                          
*******************************************************************************/
PUBLIC INT4
EoamCltResPduProcess (tEoamPortInfo * pPortInfo, tEoamPduData * pRespData)
{
    tEoamCallbackInfo   CallbackInfo;
    INT4                i4RetVal = OSIX_SUCCESS;

    EOAM_TRC_FN_ENTRY ();
    MEMSET (&CallbackInfo, 0, sizeof (tEoamCallbackInfo));
    /* Increment the Variable Response OAMPDU Rx Counter */
    pPortInfo->PduStats.u4VariableResponseRx++;

    /* Fill up the Callback Info structure appropraitely indicating the 
     * variable response received */
    CallbackInfo.u4EventType = EOAM_NOTIFY_VAR_RESP_RCVD;
    CallbackInfo.u4Port = pPortInfo->u4Port;
    CallbackInfo.ExtData.u4DataLen = pRespData->ExternalPduData.u4DataLen;
    CallbackInfo.ExtData.pu1Data = pRespData->ExternalPduData.pu1Data;
    /* Call a function to notify registered modules */
    EoamMainNotifyModules (&CallbackInfo);

    EOAM_TRC_FN_EXIT ();
    return i4RetVal;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  emcltres.c                     */
/*-----------------------------------------------------------------------*/
