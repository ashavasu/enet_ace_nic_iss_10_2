/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emcfaif.c,v 1.4 2013/11/05 10:57:38 siva Exp $
 *
 * Description: This file contains portable API functions of EOAM module     
 *****************************************************************************/
#include "eminc.h"

/****************************************************************************
 *
 *    FUNCTION NAME    : EoamValidateIfIndex
 *
 *    DESCRIPTION      : This function validates the given interface index.
 *
 *    INPUT            : u4PortIndex - Interface Index to be validated 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
EoamValidateIfIndex (UINT4 u4PortIndex)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    EOAM_TRC_FN_ENTRY ();

    if (CfaValidateIfIndex (u4PortIndex) == CFA_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }

    EOAM_TRC_FN_EXIT ();

    return (i4RetVal);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EoamIsPhysicalPort
 *
 *    DESCRIPTION      : This function checks whether the given port  
 *                       is a physical/logical port.
 *                       
 *    INPUT            : u4IfIndex - Interface Index to be checked 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_TRUE - If physical interface
 *                       OSIX_FALSE - If logical interface
 *
 ****************************************************************************/
PUBLIC INT4
EoamIsPhysicalPort (UINT4 u4IfIndex)
{
    INT4                i4RetVal = OSIX_FALSE;

    if (CfaIsPhysicalInterface (u4IfIndex) == CFA_TRUE)
    {
        i4RetVal = OSIX_TRUE;
    }

    return (i4RetVal);
}

/******************************************************************************
 * Function Name      : EoamGetIfInfo
 *
 * Description        : This function is used by EOAM to get the following
 *                      Interface parameters of a particular physical interface.
 *                      1. MTU 2. Operational status 3. Hardware address.
 *                      Also the following Eoam parameters: 
 *                      1. EoamStatus 2. Remote LB status 3. Mux State
 *                      4. Parser State 5. UniDirectional support status
 *                      6. Link fault status 7. Duplexity status
 *
 * Input(s)           : u4IfIndex - Port number
 *
 * Output(s)          : pIfInfo - pointer to the structure holding
 *                                interface parameters
 *
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
EoamGetIfInfo (UINT4 u4IfIndex, tEoamIfInfo * pIfInfo)
{
    INT4                i4RetVal = OSIX_SUCCESS;
    tCfaIfInfo          CfaIfInfo;

    EOAM_TRC_FN_ENTRY ();

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    if ((CfaGetIfInfo (u4IfIndex, &CfaIfInfo)) != CFA_SUCCESS)
    {
        i4RetVal = OSIX_FAILURE;
    }

    pIfInfo->u4IfMtu = CfaIfInfo.u4IfMtu;
    pIfInfo->u4IfSpeed = CfaIfInfo.u4IfSpeed;
    MEMCPY (pIfInfo->au1MacAddr, CfaIfInfo.au1MacAddr, CFA_ENET_ADDR_LEN);
    pIfInfo->u1IfOperStatus = CfaIfInfo.u1IfOperStatus;

    pIfInfo->u1EoamStatus = CfaIfInfo.EoamParams.u1EoamStatus;
    pIfInfo->u1MuxState = CfaIfInfo.EoamParams.u1MuxState;
    pIfInfo->u1ParState = CfaIfInfo.EoamParams.u1ParState;
    pIfInfo->u1UniDirSupp = CfaIfInfo.EoamParams.u1UniDirSupp;
    pIfInfo->u1LinkFault = CfaIfInfo.EoamParams.u1LinkFault;
    pIfInfo->u1RemoteLB = CfaIfInfo.EoamParams.u1RemoteLB;

    pIfInfo->u1DuplexStatus = CfaIfInfo.u1DuplexStatus;
    pIfInfo->u1AutoNegStatus = CfaIfInfo.u1AutoNegStatus;

    EOAM_TRC_FN_EXIT ();

    return (i4RetVal);
}

/******************************************************************************
 * Function Name      : EoamSetIfInfo
 *
 * Description        : This function is used by EOAM to set the following 
 *                      Interface parameters of a particular physical interface
 *                      1. EoamStatus 2. Remote LB status 3. Mux State
 *                      4. Parser State 5. UniDirectional support status
 *                      6. Link fault status 
 *                      
 * Input(s)           : i4CfaIfParam - Type of interface parameter to update
 *                      u4IfIndex - Port number
 *                      u4Value - Value to be updated
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
EoamSetIfInfo (INT4 i4CfaIfParam, UINT4 u4IfIndex, UINT4 u4Value)
{
    tCfaIfInfo          CfaIfInfo;
    UINT1               u1Value = 0;

    EOAM_TRC_FN_ENTRY ();
    /* The maximum value of u4value will not exceed 255 and hence 
     * type casting is done to remove the warning */
    u1Value = (UINT1) u4Value;

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    switch (i4CfaIfParam)
    {
        case IF_EOAM_STATUS:
            CfaIfInfo.EoamParams.u1EoamStatus = u1Value;
            break;

        case IF_EOAM_MUX_STATE:
            CfaIfInfo.EoamParams.u1MuxState = u1Value;
            break;

        case IF_EOAM_PARSER_STATE:
            CfaIfInfo.EoamParams.u1ParState = u1Value;
            break;

        case IF_EOAM_UNIDIR_SUPP:
            CfaIfInfo.EoamParams.u1UniDirSupp = u1Value;
            break;

        case IF_EOAM_LINK_FAULT:
            CfaIfInfo.EoamParams.u1LinkFault = u1Value;
            break;

        case IF_EOAM_REMOTE_LB:
            CfaIfInfo.EoamParams.u1RemoteLB = u1Value;
            break;

        default:
            return OSIX_FAILURE;
    }

    if ((CfaSetIfInfo (i4CfaIfParam, u4IfIndex, &CfaIfInfo)) != CFA_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    EOAM_TRC_FN_EXIT ();

    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function Name      : EoamGetSysMacAddress
 *
 * Description        : This function is used by EOAM to get the System MAC
 *                      address
 *
 * Input(s)           : None
 *
 * Output(s)          : BaseMacAddr- System MAC Address
 *
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
EoamGetSysMacAddress (tMacAddr BaseMacAddr)
{
    EOAM_TRC_FN_ENTRY ();

    CfaGetSysMacAddress (BaseMacAddr);

    EOAM_TRC_FN_EXIT ();

    return;
}

/******************************************************************************
 * Function Name      : EoamTransmitFrame
 *
 * Description        : This function is used by EOAM to post the OAMPDU to
 *                      Packet transmission module
 *
 * Input(s)           : pMsgBuf - CRU Buffer Pointer to the packet
 *                      u4Port - Port number
 *                      u4PktSize - Packet size
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
EoamTransmitFrame (tCRU_BUF_CHAIN_HEADER * pMsgBuf, UINT4 u4Port,
                   UINT4 u4PktSize)
{
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT1               au1DestHwAddr[CFA_ENET_ADDR_LEN];

    EOAM_TRC_FN_ENTRY ();
    MEMSET (au1DestHwAddr, 0, CFA_ENET_ADDR_LEN);
    if (gu1EmIfShDownFlag == OSIX_TRUE)
    {
        EoamUnLock ();
        CRU_BUF_Copy_FromBufChain (pMsgBuf, au1DestHwAddr,
                                   0, CFA_ENET_ADDR_LEN);

        if (CfaHandlePktFromEoam (pMsgBuf, u4Port,
                                  u4PktSize, CFA_ENET_SLOW_PROTOCOL,
                                  au1DestHwAddr, CFA_ENCAP_NONE) != CFA_SUCCESS)
        {
            i4RetVal = OSIX_FAILURE;
        }
    }
    else if (CfaPostPktFromL2 (pMsgBuf, (UINT2) u4Port, u4PktSize,
                               CFA_ENET_SLOW_PROTOCOL,
                               CFA_ENCAP_NONE) != CFA_SUCCESS)
    {
        i4RetVal = OSIX_FAILURE;
    }

    EOAM_TRC_FN_EXIT ();

    return (i4RetVal);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamTxLoopbackTestFrame 
 *                                                                          
 *    DESCRIPTION      : This function is called to loopback the received 
 *                       loopback test data without delivering it to higher
 *                       layer in local loopback mode.
 *
 *    INPUT            : u4IfIndex - Interface index 
 *                       pBuf - Pointer to CRU Buf that has loopback test data
 *                       u4PktSize - Test data size 
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS - when tx is successful.
 *                       OSIX_FAILURE - when tx fails.
 ****************************************************************************/
PUBLIC INT4
EoamTxLoopbackTestFrame (UINT4 u4IfIndex, tCRU_BUF_CHAIN_HEADER * pBuf,
                         UINT4 u4PktSize)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    EOAM_TRC_FN_ENTRY ();

    if (CfaEnetTxLoopbackTestFrame (u4IfIndex, pBuf, u4PktSize) != CFA_SUCCESS)
    {
        EOAM_TRC ((EOAM_LOOPBACK_TRC, "EoamTxLoopbackTestFrame: Port %d,"
                   " CfaEnetTxLoopbackTestFrame Failed\n", u4IfIndex));
        i4RetVal = OSIX_FAILURE;
    }

    EOAM_TRC_FN_EXIT ();

    return (i4RetVal);
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  emcfa.c                        */
/*-----------------------------------------------------------------------*/
