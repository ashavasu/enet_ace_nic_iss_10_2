/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emtmr.c,v 1.7 2011/09/12 07:15:00 siva Exp $
 *
 * Description: This file contains the function related to EOAM timers.  
 *********************************************************************/
#include "eminc.h"

/* Proto types of the functions private to this file only */
PRIVATE VOID EoamTmrInitTmrDesc PROTO ((VOID));
PRIVATE VOID EoamTmrPduTx PROTO ((VOID *pArg));
PRIVATE VOID EoamTmrLostLink PROTO ((VOID *pArg));
PRIVATE VOID EoamTmrLBCmd PROTO ((VOID *pArg));
PRIVATE VOID EoamTmrVarResp PROTO ((VOID *pArg));

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamTmrInit 
 *                                                                          
 *    DESCRIPTION      : This function creates a timer list for all the timers
 *                       in EOAM module.  
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamTmrInit (VOID)
{
    EOAM_TRC_FN_ENTRY ();

    if (TmrCreateTimerList (EOAM_TASK_NAME, EOAM_TMR_EXPIRY_EVENT, NULL,
                            &(gEoamGlobalInfo.EoamTmrListId)) == TMR_FAILURE)
    {
        EOAM_TRC ((INIT_SHUT_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                   "EoamTmrInit: Timer list creation FAILED !!!\n"));
        return (OSIX_FAILURE);
    }

    EoamTmrInitTmrDesc ();

    EOAM_TRC_FN_EXIT ();

    return (OSIX_SUCCESS);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamTmrInitTmrDesc 
 *                                                                          
 *    DESCRIPTION      : This function intializes the timer desc for all 
 *                       the timers in EOAM module.  
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
EoamTmrInitTmrDesc (VOID)
{
    EOAM_TRC_FN_ENTRY ();

    gEoamGlobalInfo.aEoamTmrDesc[EOAM_PDU_TX_TMR_TYPE].i2Offset =
        (INT2) EOAM_INVALID_VALUE;
    gEoamGlobalInfo.aEoamTmrDesc[EOAM_PDU_TX_TMR_TYPE].TmrExpFn = EoamTmrPduTx;

    gEoamGlobalInfo.aEoamTmrDesc[EOAM_LOST_LINK_TMR_TYPE].i2Offset =
        (INT2) EOAM_OFFSET (tEoamEnaPortInfo, LostLinkAppTmr);
    gEoamGlobalInfo.aEoamTmrDesc[EOAM_LOST_LINK_TMR_TYPE].TmrExpFn =
        EoamTmrLostLink;

    gEoamGlobalInfo.aEoamTmrDesc[EOAM_LB_CMD_TX_TMR_TYPE].i2Offset =
        (INT2) EOAM_OFFSET (tEoamEnaPortInfo, LBTxTmr);
    gEoamGlobalInfo.aEoamTmrDesc[EOAM_LB_CMD_TX_TMR_TYPE].TmrExpFn =
        EoamTmrLBCmd;

    gEoamGlobalInfo.aEoamTmrDesc[EOAM_VAR_RESP_TMR_TYPE].i2Offset =
        (INT2) EOAM_OFFSET (tEoamVarReq, ReqRespAppTmr);
    gEoamGlobalInfo.aEoamTmrDesc[EOAM_VAR_RESP_TMR_TYPE].TmrExpFn =
        EoamTmrVarResp;

    EOAM_TRC_FN_EXIT ();

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamTmrDeInit 
 *                                                                          
 *    DESCRIPTION      : This function de-initializes the timer list.
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamTmrDeInit (VOID)
{
    EOAM_TRC_FN_ENTRY ();

    if (gEoamGlobalInfo.EoamTmrListId != 0)
    {
        if (TmrDeleteTimerList (gEoamGlobalInfo.EoamTmrListId) == TMR_FAILURE)
        {
            EOAM_TRC ((INIT_SHUT_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                       "EoamTmrDeInit: Timer list deletion FAILED !!!\n"));
            return (OSIX_FAILURE);
        }
        gEoamGlobalInfo.EoamTmrListId = 0;
    }
    EOAM_TRC_FN_EXIT ();

    return (OSIX_SUCCESS);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamTmrExpHandler 
 *                                                                          
 *    DESCRIPTION      : This function is called whenever a timer expiry 
 *                       message is received by EOAM task. Different timer
 *                       expiry handlers are called based on the timer type. 
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EoamTmrExpHandler (VOID)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    UINT1               u1TimerId = 0;
    INT2                i2Offset;
#ifdef L2RED_WANTED
    /* HITLESS RESTART */
    INT1                i1HRFlag = -1;
#endif

    EOAM_TRC_FN_ENTRY ();

    if ((gEoamGlobalInfo.u1ModuleStatus == EOAM_DISABLED) ||
        (gEoamGlobalInfo.u1SystemCtrlStatus == EOAM_SHUTDOWN))
    {
        EOAM_TRC ((OS_RESOURCE_TRC, "EoamTmrExpHandler: EOAM module status"
                   " disabled\n"));
        return;
    }

    EOAM_TRC ((OS_RESOURCE_TRC, "EoamTimerExpHandler: Timer expiry to be"
               " handled\n"));

    while ((pExpiredTimers =
            TmrGetNextExpiredTimer (gEoamGlobalInfo.EoamTmrListId)) != NULL)
    {
        EOAM_TRC ((OS_RESOURCE_TRC, "Timer to be processed %x\n",
                   pExpiredTimers));

        u1TimerId = ((tTmrBlk *) pExpiredTimers)->u1TimerId;
        if (u1TimerId < EOAM_MAX_TIMERS)
        {
            i2Offset = gEoamGlobalInfo.aEoamTmrDesc[u1TimerId].i2Offset;

            EOAM_TRC ((OS_RESOURCE_TRC, "TimerId: %d\tOffset: %d\n",
                       u1TimerId, i2Offset));

            if (i2Offset == EOAM_INVALID_VALUE)
            {
                /* The timer function does not take any parameter. */
                (*(gEoamGlobalInfo.aEoamTmrDesc[u1TimerId].TmrExpFn)) (NULL);
            }
            else
            {
                (*(gEoamGlobalInfo.aEoamTmrDesc[u1TimerId].TmrExpFn))
                    ((UINT1 *) pExpiredTimers - i2Offset);
            }
#ifdef L2RED_WANTED
            /* HITLESS RESTART */
            if ((i1HRFlag != 1) && (u1TimerId == EOAM_PDU_TX_TMR_TYPE))
            {
                if (EOAM_HR_STDY_ST_REQ_RCVD () == OSIX_TRUE)
                {
                    /* Periodic timer has been made to expire after processing
                     * steady state pkt request from RM. This flag indicates
                     * to send steady tail msg after sending all the steady
                     * state packets of LA to RM. */
                    i1HRFlag = 1;
                }
            }
#endif
        }
    }
    /* End of while */
#ifdef L2RED_WANTED
    /* HITLESS RESTART */
    if (i1HRFlag == 1)
    {
        /* sending steady state tail msg to RM */
        EoamRedHRSendStdyStTailMsg ();
        EOAM_HR_STDY_ST_REQ_RCVD () = OSIX_FALSE;
    }
#endif
    EOAM_TRC_FN_EXIT ();

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamTmrPduTx
 *                                                                          
 *    DESCRIPTION      : Function that handles one second global timer expiry.
 *                       Information OAMPDUs are sent on all EOAM enabled 
 *                       interfaces during this timer expiry. 
 *
 *    INPUT            : pArg - pointer to the port specific struct whose 
 *                       timer has expired. 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
EoamTmrPduTx (VOID *pArg)
{
    UNUSED_PARAM (pArg);

    EOAM_TRC_FN_ENTRY ();

    EoamCtlTxHandlePduTmrExpiry ();
    /* Restart the global PDU Tx Timer */
    if (TmrStart (gEoamGlobalInfo.EoamTmrListId, &(gEoamGlobalInfo.PduTxTmr),
                  EOAM_PDU_TX_TMR_TYPE, EOAM_PDU_TX_TIMEOUT, 0) == TMR_FAILURE)
    {
        EOAM_TRC ((INIT_SHUT_TRC | EOAM_CRITICAL_TRC | ALL_FAILURE_TRC,
                   "EoamPduTxTmrExpiry: EOAM StartTimer for PduTx failed\n"));
    }

    EOAM_TRC_FN_EXIT ();

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamTmrLostLink
 *                                                                          
 *    DESCRIPTION      : Function that handles lost link port specific 
 *                       timer expiry.
 *
 *    INPUT            : pArg - pointer to the port specific struct whose 
 *                       timer has expired. 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
EoamTmrLostLink (VOID *pArg)
{
    tEoamPortInfo      *pPortInfo = NULL;

    EOAM_TRC_FN_ENTRY ();

    pPortInfo = ((tEoamEnaPortInfo *) pArg)->pPortInfoBackPtr;

    /* Link is lost */
    EoamSemMachine (pPortInfo, EOAM_DISC_EVENT_LOSTLINK_TMR_EXP);

    EOAM_TRC_FN_EXIT ();

}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamTmrLBCmd
 *                                                                          
 *    DESCRIPTION      : Function that handles loopback command timer expiry.
 *                       This is a 3 sec timer that waits for ack for LB ctrl
 *                       command sent to the peer. 
 *
 *    INPUT            : pArg - pointer to the port specific struct whose 
 *                       timer has expired. 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
EoamTmrLBCmd (VOID *pArg)
{
    tEoamPortInfo      *pPortInfo = NULL;

    EOAM_TRC_FN_ENTRY ();

    pPortInfo = ((tEoamEnaPortInfo *) pArg)->pPortInfoBackPtr;

    /* Ack not received for Loopback ctrl command. So change the LB status 
       to "noLoopback" and set the MUX and PARSER state to FWD. */
    EoamCltLbHandleTmrExpiry (pPortInfo);

    EOAM_TRC_FN_EXIT ();

}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamTmrVarResp
 *                                                                          
 *    DESCRIPTION      : Function that handles Variable response timer expiry.
 *                       This timer is started for each var request received. 
 *                       This timer is used to send variable response within
 *                       one second fo receiving the variable request. 
 *
 *    INPUT            : pArg - pointer to the port specific struct whose 
 *                       timer has expired. 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
EoamTmrVarResp (VOID *pArg)
{
    tEoamVarReq        *pVarReq = pArg;

    EOAM_TRC_FN_ENTRY ();

    /* One second elapsed after receiving the variable
       request. Form variable resp and reply to the peer. */
    if (pVarReq != NULL)
    {
        EoamCltResSendToPeer (pVarReq->pPortInfoBackPtr, pVarReq);
    }

    EOAM_TRC_FN_EXIT ();

}

/*-----------------------------------------------------------------------*/
/*                       End of the file  emtmr.c                        */
/*-----------------------------------------------------------------------*/
