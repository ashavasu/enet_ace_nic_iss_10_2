/*******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emcltreq.c,v 1.11 2012/07/10 06:06:32 siva Exp $
 *
 * Description: This file conatains the functions related to Variable Request
 * PDU handling by EOAM client module. 
 ******************************************************************************/
#include "eminc.h"

PRIVATE INT4 EoamCltReqStoreDescriptors PROTO ((tEoamVarReq *, tEoamExtData *,
                                                UINT4));
/*******************************************************************************
 *                                                                          
 *    FUNCTION NAME    :  EoamCltReqPduProcess 
 *                                                                          
 *    DESCRIPTION      :  This function is called on reception of a Variable 
 *                        Request OAMPDU. It processes the variable requests,
 *                        stores them, increments the global request ID for 
 *                        each request and sends each descriptor to CMIP 
 *                        Interface module. It starts a 1 sec timer for each 
 *                        varibale request. 
 *                        
 *    INPUT            :  pPortInfo - pointer to port specific info          
 *                        pReqData - pointer to PDU Data                       
 *                                                                          
 *    OUTPUT           :  None.                                             
 *                                                                          
 *    RETURNS          :  OSIX_SUCCESS / OSIX_FAILURE                       
 *                                                                          
*******************************************************************************/
PUBLIC INT4
EoamCltReqPduProcess (tEoamPortInfo * pPortInfo, tEoamPduData * pReqData)
{
    tEoamVarReq        *pVarReqNode = NULL;
    tEoamEnaPortInfo   *pActivePortInfo = NULL;
    INT4                i4RetVal = OSIX_FAILURE;
    UINT4               u4Index = 0;

    EOAM_TRC_FN_ENTRY ();
    pActivePortInfo = pPortInfo->pEoamEnaPortInfo;

    /* Variable Request from a passive node should not be entertained */
    if (pPortInfo->RemoteInfo.u1Mode == EOAM_MODE_PASSIVE)
    {
        EOAM_TRC ((ALL_FAILURE_TRC | EOAM_VAR_REQRESP_TRC, "CLIENT: Received"
                   "Variable Request OAMPDU from a passive node on"
                   "port %u\n ", pPortInfo->u4Port));
        return OSIX_FAILURE;
    }
    /* Increment the Varaible Request OAMPDU Rx counter */
    pPortInfo->PduStats.u4VariableRequestRx++;
    u4Index = ++(gEoamGlobalInfo.u4VarReqIdCount);

    if ((pVarReqNode = (tEoamVarReq *) (MemAllocMemBlk
                                        (gEoamGlobalInfo.VarReqPoolId))) ==
        NULL)
    {
        EOAM_TRC ((ALL_FAILURE_TRC | EOAM_VAR_REQRESP_TRC, "CLIENT: Memory"
                   "Allocation for Variable Request entry failed for"
                   "port %u\n", pPortInfo->u4Port));
        return OSIX_FAILURE;
    }
    FSAP_ASSERT (pVarReqNode != NULL);
    MEMSET (pVarReqNode, 0, sizeof (tEoamVarReq));
    pVarReqNode->u4ReqId = u4Index;

    u4Index = (u4Index % EOAM_MAX_VAR_REQS);
    u4Index = ((u4Index == 0) ? EOAM_MAX_VAR_REQS : u4Index);
    pActivePortInfo->pVarReq[u4Index - 1] = pVarReqNode;
    pVarReqNode->pPortInfoBackPtr = pPortInfo;

    pVarReqNode->RBTVarReq = RBTreeCreate (EOAM_MAX_DESCRIPTORS,
                                           EoamUtilRBTreeDescCmp);
    FSAP_ASSERT (pVarReqNode->RBTVarReq != NULL);
    if (TmrStart (gEoamGlobalInfo.EoamTmrListId,
                  &pVarReqNode->ReqRespAppTmr, EOAM_VAR_RESP_TMR_TYPE,
                  EOAM_VAR_RESP_TIMEOUT, 0) != TMR_SUCCESS)
    {
        RBTreeDestroy (pVarReqNode->RBTVarReq, EoamUtilRBTFreeNodeFn, 0);
        MemReleaseMemBlock (gEoamGlobalInfo.VarReqPoolId,
                            (UINT1 *) pVarReqNode);
        EOAM_TRC ((ALL_FAILURE_TRC | EOAM_VAR_REQRESP_TRC, "CLIENT:"
                   "EoamStartTimer failed for Variable Request"
                   "on port %u\n", pPortInfo->u4Port));
        return OSIX_FAILURE;
    }
    i4RetVal = EoamCltReqStoreDescriptors (pVarReqNode,
                                           &(pReqData->ExternalPduData),
                                           pPortInfo->u4Port);
    if (i4RetVal == OSIX_FAILURE)
    {
        RBTreeDestroy (pVarReqNode->RBTVarReq, EoamUtilRBTFreeNodeFn, 0);
        TmrStop (gEoamGlobalInfo.EoamTmrListId, &(pVarReqNode->ReqRespAppTmr));
        MemReleaseMemBlock (gEoamGlobalInfo.VarReqPoolId,
                            (UINT1 *) pVarReqNode);
        pPortInfo->pEoamEnaPortInfo->pVarReq[u4Index - 1] = NULL;
    }
    EOAM_TRC_FN_EXIT ();
    KW_FALSEPOSITIVE_FIX(pVarReqNode->RBTVarReq->SemId);
    return i4RetVal;
}

/*******************************************************************************
 *                                                                          
 *    FUNCTION NAME    :  EoamCltReqStoreDescriptors 
 *                                                                          
 *    DESCRIPTION      :  This function parses the received request and stores 
 *                        each descriptor in a RB Tree node and also send each
 *                        request to the CMIP interface for retrieval.
 *                        
 *    INPUT            :  pVarReqNode - pointer to the specific RB Tree 
 *                                      information          
 *                        pReqData - pointer to PDU Data                       
 *                        u4Port - Interface Index
 *                                                                          
 *    OUTPUT           :  None.                                             
 *                                                                          
 *    RETURNS          :  OSIX_SUCCESS / OSIX_FAILURE                       
 *                                                                          
*******************************************************************************/
PRIVATE INT4
EoamCltReqStoreDescriptors (tEoamVarReq * pVarReqNode, tEoamExtData * pReqData,
                            UINT4 u4Port)
{
    tEoamCallbackInfo   CallbackInfo;
    tEoamVarReqRBTNode *pRBTNode = NULL;
    UINT1              *pu1Temp = NULL;
    UINT4               u4TempLen = 0;
    UINT2               u2Leaf = 0;
    UINT1               u1Branch = 0;

    EOAM_TRC_FN_ENTRY ();
    MEMSET (&CallbackInfo, 0, sizeof (tEoamCallbackInfo));
    pu1Temp = pReqData->pu1Data;
    do
    {
        EOAM_GET_1BYTE (u1Branch, pu1Temp);
        if (EOAM_IS_LAST_BRANCH (u1Branch) == OSIX_TRUE)
        {
            EOAM_TRC ((ALL_FAILURE_TRC | EOAM_VAR_REQRESP_TRC, "CLIENT: Branch"
                       "Value with 0 encountered in a variable request on"
                       "port %u and therefore ignoring subsequent fields\n",
                       u4Port));
            break;
        }
        if ((pReqData->u4DataLen - u4TempLen) < 2)
        {
            EOAM_TRC ((ALL_FAILURE_TRC | EOAM_VAR_REQRESP_TRC, "CLIENT:"
                       "A variable request received with no leaf value on"
                       "port %u and therefore ignoring subsequent fields\n",
                       u4Port));
            break;
        }
        EOAM_GET_2BYTE (u2Leaf, pu1Temp);
        u4TempLen += EOAM_VAR_DESCRIPTOR_LENGTH;
        if (EOAM_IS_VALID_DESCRIPTOR (u1Branch, u2Leaf) == OSIX_FALSE)
        {
            EOAM_TRC ((ALL_FAILURE_TRC | EOAM_VAR_REQRESP_TRC |
                       EOAM_CRITICAL_TRC,
                       "CLIENT: Invalid Branch/Leaf Value encounteredi"
                       "for the variable request ID %u on port %u and"
                       "therefore ignoring that descriptor\n",
                       gEoamGlobalInfo.u4VarReqIdCount, u4Port));
            continue;
        }
        pRBTNode = NULL;
        if ((pRBTNode = (tEoamVarReqRBTNode *) (MemAllocMemBlk
                                                (gEoamGlobalInfo.
                                                 VarContainerPoolId))) == NULL)
        {
            EOAM_TRC ((ALL_FAILURE_TRC | EOAM_VAR_REQRESP_TRC, "CLIENT: Memory"
                       "Allocation for Variable Request RB tree node failed"
                       "for port %u\n", u4Port));
            return OSIX_FAILURE;
        }
        FSAP_ASSERT (pRBTNode != NULL);
        MEMSET (pRBTNode, 0, sizeof (tEoamVarReqRBTNode));
        pRBTNode->u1Branch = u1Branch;
        pRBTNode->u2Leaf = u2Leaf;
        pRBTNode->pu1Data = NULL;
        if (RBTreeAdd (pVarReqNode->RBTVarReq, (tRBElem *) pRBTNode)
            != RB_SUCCESS)
        {
            EOAM_TRC ((ALL_FAILURE_TRC | EOAM_VAR_REQRESP_TRC,
                       "CLIENT: RBTreeaAdd failed for the variable request"
                       "ID %u on port %u and therefore ignoring that"
                       "variable Request\n",
                       gEoamGlobalInfo.u4VarReqIdCount, u4Port));
            return OSIX_FAILURE;
        }
        pVarReqNode->u4Count++;
        /* Fill up the Callback Info structure appropraitely indicating the 
         * variable response received */
        CallbackInfo.u4EventType = EOAM_NOTIFY_VAR_REQ_RCVD;
        CallbackInfo.u4Port = u4Port;
        CallbackInfo.VarRequest.u4ReqId = gEoamGlobalInfo.u4VarReqIdCount;
        CallbackInfo.VarRequest.u2Leaf = u2Leaf;
        CallbackInfo.VarRequest.u1Branch = u1Branch;
        /* Call a function to notify registered modules */
        EoamMainNotifyModules (&CallbackInfo);
    }
    while (u4TempLen < pReqData->u4DataLen);
    EOAM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*******************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamCltReqSendToPeer 
 *                                                                          
 *    DESCRIPTION      : This function is called whenever there is a         
 *                       variable request to be sent to the peer entity.    
 *                       It sends an appropriate OAMPDU request to the      
 *                       Control Module.                                    
 *                                                                          
 *    INPUT            : pPortEntry - pointer to port information structure 
 *                       pVarReqData - pointer to Variable Request          
 *                       information                                        
 *                                                                          
 *    OUTPUT           : None.                                              
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE                        
*******************************************************************************/
PUBLIC INT4
EoamCltReqSendToPeer (tEoamPortInfo * pPortInfo, tEoamExtData * pVarReqData)
{
    tEoamPduHeader      PduHeader;
    tEoamPduData        PduData;

    EOAM_TRC_FN_ENTRY ();
    MEMSET (&PduHeader, 0, sizeof (tEoamPduHeader));
    MEMSET (&PduData, 0, sizeof (tEoamPduData));

    if (gEoamGlobalInfo.u1ModuleStatus != EOAM_ENABLED)
    {
        EOAM_TRC ((ALL_FAILURE_TRC | EOAM_VAR_REQRESP_TRC,
                   "EOAM is globally DISABLED!!!\n"));
        return OSIX_FAILURE;
    }

    if (pPortInfo == NULL)
    {
        return OSIX_SUCCESS;
    }

    /* OAMPDUs other than Information PDUs can be sent out only when
     * discovery has settled down */
    if (pPortInfo->LocalInfo.u2EoamOperStatus != EOAM_OPER_OPERATIONAL)
    {
        EOAM_TRC ((ALL_FAILURE_TRC |
                   EOAM_VAR_REQRESP_TRC, "CLIENT: Cannot"
                   "send out Variable Request OAMPDU when discovery has not"
                   "settled down on port %u\n ", pPortInfo->u4Port));
        return OSIX_FAILURE;
    }
    /* Variable Request should not be sent out from a passive node */
    if (pPortInfo->LocalInfo.u1Mode == EOAM_MODE_PASSIVE)
    {
        EOAM_TRC ((ALL_FAILURE_TRC | EOAM_VAR_REQRESP_TRC, "CLIENT: Variable"
                   "Request initiated from a passive node on port %u\n",
                   pPortInfo->u4Port));
        return OSIX_FAILURE;
    }
    if ((pPortInfo->RemoteInfo.u1FnsSupportedBmp & EOAM_VARIABLE_SUPPORT) == 0)
    {
        EOAM_TRC ((ALL_FAILURE_TRC | EOAM_VAR_REQRESP_TRC | EOAM_CRITICAL_TRC,
                   "CLIENT: Variable Request initiated on port %u,"
                   "but peer does not have the capability\n",
                   pPortInfo->u4Port));
        return OSIX_FAILURE;
    }
    MEMCPY (&(PduData.ExternalPduData), pVarReqData, sizeof (tEoamExtData));
    EoamCtlTxFormPduHeader (pPortInfo, EOAM_VAR_REQ, &PduHeader);
    if (EoamCtlTxPduRequest (pPortInfo, &PduHeader, &PduData) != OSIX_SUCCESS)
    {
        EOAM_TRC ((ALL_FAILURE_TRC | EOAM_VAR_REQRESP_TRC, "CLIENT:"
                   "EoamCtlTxPduRequest call for Variable Request PDU"
                   "failed on port %u\n", pPortInfo->u4Port));
        return OSIX_FAILURE;
    }
    /* Increment the Variable Request OAMPDU Tx Counter */
    pPortInfo->PduStats.u4VariableRequestTx++;
    EOAM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  emcltreq.c                     */
/*-----------------------------------------------------------------------*/
