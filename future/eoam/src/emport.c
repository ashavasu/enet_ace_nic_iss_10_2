/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emport.c,v 1.8 2010/05/19 07:12:21 prabuc Exp $
 *
 * Description: This file contains portable API functions of EOAM module     
 *****************************************************************************/
#include "eminc.h"

/******************************************************************************
 * Function Name      : EoamNegotiateDiscovery
 *
 * Description        : This function is called when an Information OAMPDU
 *                      is received. This function monitors the peer
 *                      capabilities and if any parameter is found to be
 *                      not acceptable it either modifies its value or
 *                      rejects the peer
 *
 * Input(s)           : pPortInfo - EOAM Port info structure
 *
 * Output(s)          : None 
 *
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
EoamNegotiateDiscovery (tEoamPortInfo * pPortInfo)
{
    EOAM_TRC_FN_ENTRY ();

    /* By default, FutureSoft EOAM will not reject peer on any basis.
     * It just updates the max OAMPDU size of the local node according
     * to that of peer's */

    /* Check Max OAMPDU of remote, if it is lesser than mine,
     * change my max OAMPDU size to that of peer's */
    if ((pPortInfo->LocalInfo.u2MaxOamPduSize) >
        (pPortInfo->RemoteInfo.u2MaxOamPduSize))
    {
        pPortInfo->LocalInfo.u2ConfigRevision++;

        EoamCtlTxConstructInfoPdu (pPortInfo);
    }

    EOAM_TRC_FN_EXIT ();

    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function Name      : EoamPutOrgSpecInfoTLV
 *
 * Description        : This function is called to add an Organization    
 *                      specific Information TLV in the Information OAMPDU.
 *                      This function will be called every time the
 *                      information OAMPDU is constructed and LF_INFO
 *                      is set to FALSE.
 *                      All the fields required in the TLV including the 
 *                      Type (0xFE), Length of the TLV, OUI and Organization
 *                      specific value should be filled in this function.
 *                      Please refer to the function EoamCtlTxPutLocalTLV to
 *                      get help on how to populate the TLV contents to
 *                      ppu1LinearBuf
 *                      Important : If this function is modified, then 
 *                      EoamSizeofOrgSpecInfoTLV should also be modified to
 *                      return the correct Length of the Org specific Info TLV
 *
 * Input(s)           : pPortInfo - EOAM Port info structure
 *
 * Output(s)          : *ppu1LinearBuf - pointer to linear buffer
 *
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
EoamPutOrgSpecInfoTLV (tEoamPortInfo * pPortInfo, UINT1 **ppu1LinearBuf)
{
    EOAM_TRC_FN_ENTRY ();

    /* By default, FutureSoft EOAM will not send any Organization specific 
     * Information TLV in the Information OAMPDU sent.
     * When organization specific info TLV needs to be added, the following
     * format should be followed:
     *             Type of the TLV - 0xFE
     *             Length of the TLV
     *             Organization Unique Identifier (OUI)
     *             Organization specific value
     */
    UNUSED_PARAM (pPortInfo);
    UNUSED_PARAM (ppu1LinearBuf);

    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EoamSizeofOrgSpecInfoTLV
 *
 * Description        : Returns size of Organization specific Info TLV, 
 *                      if defined
 *
 * Input(s)           : pEoamEnaPortInfo - Pointer to Eoam Port specific info
 *
 * Output(s)          : None
 *
 * Return Value(s)    : Organization Specific Info TLV size
 *****************************************************************************/
PUBLIC UINT4
EoamSizeofOrgSpecInfoTLV (tEoamEnaPortInfo * pEoamEnaPortInfo)
{
    UINT4               u4TLVSize = 0;
    EOAM_TRC_FN_ENTRY ();
    UNUSED_PARAM (pEoamEnaPortInfo);
    EOAM_TRC_FN_EXIT ();
    return u4TLVSize;
}

/******************************************************************************
 * Function Name      : EoamProcessOrgInfoTLV
 *
 * Description        : This function is called to process the Organization    
 *                      specific Information TLV received in the Information 
 *                      OAMPDU. The Org specific Info value is stored in
 *                      pOrgSpecInfo->pu1Data and the length of the value
 *                      is stored in pOrgSpecInfo->u4DataLen
 *                      The first three bytes of Org specific data contains
 *                      the Organization Unique Identifier (OUI) followed by
 *                      Organization specific value as defined in the standard
 *
 * Input(s)           : pPortInfo - EOAM Port info structure
 *                      pOrgSpecInfo - Org specific Info received in the 
 *                      Information OAMPDU
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
EoamProcessOrgInfoTLV (tEoamPortInfo * pPortInfo, tEoamExtData * pOrgSpecInfo)
{
    EOAM_TRC_FN_ENTRY ();
    /* By default, FutureSoft EOAM does not process org specific information
     * even if received . This function can be ported if needed based on
     * the organization specific information TLV defined */
    UNUSED_PARAM (pPortInfo);
    UNUSED_PARAM (pOrgSpecInfo);
    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function Name      : EoamPopulateOrgEvtLogEntry
 *
 * Description        : This function populates the Organization specific
 *                      Event TLV details in the Event Log Table.
 *                      This function should fill the OUI and Organization
 *                      specific Event Values in the Log table.
 *
 * Input(s)           : pOrgSpecInfo - Org specific Event TLV data
 *
 * Output(s)          : pLogEntry - pointer to log entry in the Event log table
 *
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
EoamPopulateOrgEvtLogEntry (tEoamExtData * pOrgSpecInfo,
                            tEoamEventLogEntry * pLogEntry)
{
    EOAM_TRC_FN_ENTRY ();

    /* By default, FutureSoft EOAM does not support any Organization specific 
     * Event TLV */
    UNUSED_PARAM (pOrgSpecInfo);
    UNUSED_PARAM (pLogEntry);

    EOAM_TRC_FN_EXIT ();

    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  emport.c                       */
/*-----------------------------------------------------------------------*/
