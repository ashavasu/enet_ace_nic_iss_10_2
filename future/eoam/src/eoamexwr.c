/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: eoamexwr.c,v 1.5 2008/08/20 14:28:30 premap-iss Exp $
 *
 * Description: Protocol wrapper Routines
 *********************************************************************/
#include  "eminc.h"
#include  "eoamexdb.h"

VOID
RegisterEOAMEX ()
{
    SNMPRegisterMibWithLock (&fseoamOID, &fseoamEntry, EoamLock, EoamUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fseoamOID, (const UINT1 *) "fseoamex");
}

INT4
FsEoamSystemControlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsEoamSystemControl (&(pMultiData->i4_SLongValue)));
}

INT4
FsEoamModuleStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsEoamModuleStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsEoamErrorEventResendGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsEoamErrorEventResend (&(pMultiData->u4_ULongValue)));
}

INT4
FsEoamOuiGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsEoamOui (pMultiData->pOctetStrValue));
}

INT4
FsEoamTraceOptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsEoamTraceOption (&(pMultiData->i4_SLongValue)));
}

INT4
FsEoamSystemControlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsEoamSystemControl (pMultiData->i4_SLongValue));
}

INT4
FsEoamModuleStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsEoamModuleStatus (pMultiData->i4_SLongValue));
}

INT4
FsEoamErrorEventResendSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsEoamErrorEventResend (pMultiData->u4_ULongValue));
}

INT4
FsEoamOuiSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsEoamOui (pMultiData->pOctetStrValue));
}

INT4
FsEoamTraceOptionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsEoamTraceOption (pMultiData->i4_SLongValue));
}

INT4
FsEoamSystemControlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsEoamSystemControl (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsEoamModuleStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsEoamModuleStatus (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsEoamErrorEventResendTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsEoamErrorEventResend
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsEoamOuiTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsEoamOui (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsEoamTraceOptionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsEoamTraceOption (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsEoamSystemControlDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsEoamSystemControl
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsEoamModuleStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsEoamModuleStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsEoamErrorEventResendDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsEoamErrorEventResend
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsEoamOuiDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsEoamOui (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsEoamTraceOptionDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsEoamTraceOption
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
