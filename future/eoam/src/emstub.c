/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emstub.c,v 1.6 2007/02/01 14:47:30 iss Exp $
 *
 * Description: This file contains portable API functions of EOAM module        
 *****************************************************************************/
#include "eminc.h"

#ifndef CFA_WANTED
/****************************************************************************
 *
 *    FUNCTION NAME    : EoamValidateIfIndex
 *
 *    DESCRIPTION      : This function validates the given interface index.
 *
 *    INPUT            : u4PortIndex - Interface Index to be validated 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
EoamValidateIfIndex (UINT4 u4PortIndex)
{
    UNUSED_PARAM (u4PortIndex);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : EoamIsPhysicalPort
 *
 *    DESCRIPTION      : This function checks whether the given port  
 *                       is a physical/logical port.
 *                       
 *    INPUT            : u4IfIndex - Interface Index to be checked 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_TRUE - If physical interface
 *                       OSIX_FALSE - If logical interface
 *
 ****************************************************************************/
PUBLIC INT4
EoamIsPhysicalPort (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);

    return (OSIX_FALSE);
}

/******************************************************************************
 * Function Name      : EoamGetIfInfo
 *
 * Description        : This function is used by EOAM to get the following
 *                      Interface parameters of a particular physical interface.
 *                      1. MTU 2. Operational status 3. Hardware address.
 *                      Also the following Eoam parameters: 
 *                      1. EoamStatus 2. Remote LB status 3. Mux State
 *                      4. Parser State 5. UniDirectional support status
 *                      6. Link fault status 
 *
 * Input(s)           : u4IfIndex - Port number
 *
 * Output(s)          : pIfInfo - pointer to the structure holding
 *                                interface parameters
 *
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
EoamGetIfInfo (UINT4 u4IfIndex, tEoamIfInfo * pIfInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pIfInfo);

    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function Name      : EoamSetIfInfo
 *
 * Description        : This function is used by EOAM to set the following 
 *                      Interface parameters of a particular physical interface
 *                      1. EoamStatus 2. Remote LB status 3. Mux State
 *                      4. Parser State 5. UniDirectional support status
 *                      6. Link fault status 
 *                      
 * Input(s)           : i4CfaIfParam - Type of interface parameter to update
 *                      u4IfIndex - Port number
 *                      u4Value - Value to be updated
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
EoamSetIfInfo (INT4 i4CfaIfParam, UINT4 u4IfIndex, UINT4 u4Value)
{
    UNUSED_PARAM (i4CfaIfParam);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4Value);

    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function Name      : EoamGetSysMacAddress
 *
 * Description        : This function is used by EOAM to get the System MAC
 *                      address
 *
 * Input(s)           : None
 *
 * Output(s)          : BaseMacAddr- System MAC Address
 *
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
EoamGetSysMacAddress (tMacAddr BaseMacAddr)
{
    UNUSED_PARAM (BaseMacAddr);

    return;
}

/******************************************************************************
 * Function Name      : EoamTransmitFrame
 *
 * Description        : This function is used by EOAM to post the OAMPDU to
 *                      Packet transmission module
 *
 * Input(s)           : pMsgBuf - CRU Buffer Pointer to the packet
 *                      u4IfIndex - Port number
 *                      u4PktSize - Packet size
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
EoamTransmitFrame (tCRU_BUF_CHAIN_HEADER * pMsgBuf,
                   UINT4 u4Port, UINT4 u4PktSize)
{
    UNUSED_PARAM (pMsgBuf);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (u4PktSize);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamTxLoopbackTestFrame 
 *                                                                          
 *    DESCRIPTION      : This function is called to loopback the received 
 *                       loopback test data without delivering it to higher
 *                       layer in local loopback mode.
 *
 *    INPUT            : u4IfIndex - Interface index 
 *                       pBuf - Pointer to CRU Buf that has loopback test data
 *                       u4PktSize - Test data size 
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS - when tx is successful.
 *                       OSIX_FAILURE - when tx fails.
 ****************************************************************************/
PUBLIC INT4
EoamTxLoopbackTestFrame (UINT4 u4IfIndex, tCRU_BUF_CHAIN_HEADER * pBuf,
                         UINT4 u4PktSize)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (u4PktSize);
    return OSIX_FAILURE;
}
#endif /* CFA_WANTED */
/*-----------------------------------------------------------------------*/
/*                       End of the file  emstub.c                       */
/*-----------------------------------------------------------------------*/
