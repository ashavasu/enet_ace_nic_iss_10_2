/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emsem.c,v 1.16 2016/07/22 06:46:10 siva Exp $
 *
 * Description: This file contains OAM Discovery related functions
 *****************************************************************************/
#include "eminc.h"
#include "emsem.h"

/******************************************************************************
 * Function           : EoamSemMachine
 *
 * Description        : Ethernet OAM Discovery State Machine
 *                      
 * Input(s)           : u1Event - Event 
 * 
 * Output(s)          : None.
 * 
 * Returns            : None.
 ******************************************************************************/
PUBLIC VOID
EoamSemMachine (tEoamPortInfo * pPortInfo, UINT1 u1Event)
{
    UINT1               u1ActionProcIdx = 0;
    UINT1               u1CurrentState = 0;

    EOAM_TRC_FN_ENTRY ();

    FSAP_ASSERT (pPortInfo != NULL);
    FSAP_ASSERT (pPortInfo->pEoamEnaPortInfo != NULL);

    u1CurrentState = (UINT1) pPortInfo->LocalInfo.u2EoamOperStatus;

    /* Since array is declared for the size of (EOAM_MAX_STATES - 1)
     * below check is added. 
     * */
    if ((u1CurrentState == 0) || (u1CurrentState > (EOAM_MAX_STATES - 1)))
    {
        return;
    }

    EOAM_TRC ((EOAM_DISCOVERY_TRC, "SEM: Port: %d  Event: %s"
               "  State: %s\n", pPortInfo->u4Port, gau1EoamEvntStr[u1Event],
               gau1EoamStateStr[u1CurrentState - 1]));

    /* Get the index of the action procedure */
    u1ActionProcIdx = gau1EoamSem[u1Event][u1CurrentState - 1];

    if (u1ActionProcIdx >= MAX_EOAM_FN_PTRS)
    {
        return;
    }

    /* Call corresponding function pointer */
    (*gaEoamActionProc[u1ActionProcIdx]) (pPortInfo);

    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function           : EoamSemEventIgnore 
 * 
 * Description        : Handle Invalid event received
 * 
 * Input(s)           : pPortInfo - Port on which this event has occured
 * 
 * Output(s)          : None.
 * 
 * Returns            : None.
 ******************************************************************************/
PRIVATE VOID
EoamSemEventIgnore (tEoamPortInfo * pPortInfo)
{
    EOAM_TRC_FN_ENTRY ();

    UNUSED_PARAM (pPortInfo);

    EOAM_TRC ((EOAM_DISCOVERY_TRC, "Port %u: Unexpected event to "
               "Discovery SEM\n", pPortInfo->u4Port));

    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function           : EoamSemStateFault
 * 
 * Description        : This procedure is invoked when Fault state is entered.
 *
 * Input(s)           : pPortInfo - Port on which this event has occured
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 ******************************************************************************/
PRIVATE VOID
EoamSemStateFault (tEoamPortInfo * pPortInfo)
{
    tEoamCallbackInfo   CallbackInfo;
    UINT1               u1SyncUpReq = OSIX_FALSE;
#ifdef NPAPI_WANTED
    tEoamIfInfo         IfInfo;

    MEMSET (&IfInfo, 0, sizeof (tEoamIfInfo));
#endif /* NPAPI_WANTED */

    EOAM_TRC_FN_ENTRY ();

    MEMSET (&CallbackInfo, 0, sizeof (tEoamCallbackInfo));

    if ((pPortInfo->LocalInfo.u2EoamOperStatus != EOAM_OPER_DISABLE) &&
        (pPortInfo->pEoamEnaPortInfo->CtrlReq.b1RemoteStateValid == OSIX_TRUE))
    {
        if (TmrStop (gEoamGlobalInfo.EoamTmrListId,
                     &(pPortInfo->pEoamEnaPortInfo->LostLinkAppTmr)) ==
            TMR_FAILURE)
        {
            EOAM_TRC ((EOAM_DISCOVERY_TRC | OS_RESOURCE_TRC |
                       ALL_FAILURE_TRC,
                       "StopTimer for LostLinkAppTmr FAILED!!!\n"));
        }
    }
    if (pPortInfo->LocalInfo.u2EoamOperStatus == EOAM_OPER_OPERATIONAL)
    {
        /* Sync up with Standby only when reaching state from 
         * operational state */
        u1SyncUpReq = OSIX_TRUE;
    }

    /* Initialize RemoteEoamInfo */
    MEMSET (&(pPortInfo->RemoteInfo), 0, sizeof (tEoamPeerInfo));
    pPortInfo->RemoteInfo.u1Mode = EOAM_MODE_PEER_UNKNOWN;
    pPortInfo->RemoteInfo.u2MaxOamPduSize = EOAM_MIN_PDU_SIZE;

    /* Initialize ctrl_request, indication variables */
    pPortInfo->pEoamEnaPortInfo->CtrlReq.b1LocalSatisfied = OSIX_FALSE;
    pPortInfo->pEoamEnaPortInfo->CtrlReq.b1RemoteStable = OSIX_FALSE;
    pPortInfo->pEoamEnaPortInfo->CtrlReq.b1RemoteStateValid = OSIX_FALSE;

    pPortInfo->pEoamEnaPortInfo->CtrlInd.b1LocalStable = OSIX_FALSE;
    pPortInfo->pEoamEnaPortInfo->CtrlInd.b1LocalEvaluating = OSIX_TRUE;

    if (pPortInfo->LoopbackInfo.u1Status != EOAM_NO_LOOPBACK)
    {
        EoamMuxParSet (pPortInfo, EOAM_NO_LOOPBACK);
        EoamSetIfInfo (IF_EOAM_REMOTE_LB, pPortInfo->u4Port, EOAM_DISABLED);
        pPortInfo->LocalInfo.u2ConfigRevision++;

        if (pPortInfo->LoopbackInfo.u1Status == EOAM_REMOTE_LOOPBACK)
        {
            /* When already in Remote loopback, inform FM that Remote
             * loopback mode is being reset */
            CallbackInfo.u4EventType = EOAM_NOTIFY_LB_ACK_RCVD;
            CallbackInfo.EventState = EOAM_DISABLED;
            CallbackInfo.u2Location = EOAM_REMOTE_ENTRY;
            CallbackInfo.u4Port = pPortInfo->u4Port;
            /* Call a function to notify all registered modules */
            EoamMainNotifyModules (&CallbackInfo);
#ifdef NPAPI_WANTED
            if (EOAM_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE)
            {
                if (EoamEoamNpHandleRemoteLoopBack
                    (pPortInfo->u4Port, NULL, NULL, EOAM_NP_LB_DISABLE) !=
                    FNP_SUCCESS)
                {
                    EOAM_TRC ((ALL_FAILURE_TRC, "CLIENT: "
                               "EoamNpHandleRemoteLoopBack "
                               "failed on port %u\n", pPortInfo->u4Port));
                }
            }
#endif
        }
        else if (pPortInfo->LoopbackInfo.u1Status == EOAM_LOCAL_LOOPBACK)
        {
#ifdef NPAPI_WANTED
            if (EOAM_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE)
            {
                if (EoamEoamNpHandleLocalLoopBack
                    (pPortInfo->u4Port, NULL, NULL, EOAM_NP_LB_DISABLE) !=
                    FNP_SUCCESS)
                {
                    EOAM_TRC ((ALL_FAILURE_TRC, "CLIENT: "
                               "EoamEoamNpHandleLocalLoopBack failed on port %u\n",
                               pPortInfo->u4Port));
                }
            }
#endif
        }
        else
        {
        pPortInfo->LoopbackInfo.u1Status = EOAM_NO_LOOPBACK;
    }
    }

#ifdef NPAPI_WANTED
    /* Set the operstatus as nonOperHalfDulex, if the interface is in half
     * duplex mode.
     * This cannot be tested on Linux because the Duplexity status
     * is updated in CFA only for NPAPI */

    EoamGetIfInfo (pPortInfo->u4Port, &IfInfo);
    if (IfInfo.u1DuplexStatus == ETH_STATS_DUP_ST_HALFDUP)
    {
        pPortInfo->LocalInfo.u2EoamOperStatus = EOAM_OPER_HALF_DUPLEX;
        return;
    }
#endif /* NPAPI_WANTED */

    if ((gEoamGlobalInfo.u1ModuleStatus == EOAM_ENABLED))
    {
        /* Set EOAM OperStatus to FAULT */
        pPortInfo->LocalInfo.u2EoamOperStatus = EOAM_OPER_LINKFAULT;

        EOAM_TRC ((EOAM_DISCOVERY_TRC, "Port %u: Entered Fault State\n",
                   pPortInfo->u4Port));
        /* Update local_pdu based on link_status */
        if ((pPortInfo->pEoamEnaPortInfo->CtrlReq.u1LocalLinkStatus
             == EOAM_LINK_STATUS_FAIL)
            || (pPortInfo->u1OperStatus == CFA_IF_DOWN))
        {
            /* Link Fault condition - remain in LINKFAULT state until
             * link_status is OK. In this state, an information OAMPDU with
             * link fault bit set will be sent every second.
             * Link Fault reporting is independent of OAM Mode */
            pPortInfo->pEoamEnaPortInfo->CtrlInd.LocalPdu = EOAM_LF_INFO;

            /* Set LF bit in Info pdu and form PDU without any TLVs */
            EoamCtlTxConstructInfoPdu (pPortInfo);
            /* Send Info OAMPDU with Local TLV */
            EoamCtlTxSendInfoPdu (pPortInfo);
        }
        else
        {
            pPortInfo->pEoamEnaPortInfo->CtrlInd.LocalPdu = EOAM_RX_INFO;
            /* Link status is OK. Check OAM mode and move to next state */
            if (pPortInfo->LocalInfo.u1Mode == EOAM_MODE_ACTIVE)
            {
                EoamSemStateActiveSendLocal (pPortInfo);
            }
            else
            {
                EoamSemStatePassiveWait (pPortInfo);
            }
        }
    }
    else
    {
        /* Set EOAM OperStatus to DISABLED */
        pPortInfo->LocalInfo.u2EoamOperStatus = EOAM_OPER_DISABLE;
    }

    if (u1SyncUpReq == OSIX_TRUE)
    {
        /* Sync discovery status with Standby Node */
        EoamRedSyncUpDynamicInfo (pPortInfo);
    }

    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function           : EoamSemStateActiveSendLocal
 *
 * Description        : This procedure is invoked after the Fault state 
 *                      when the link is up and the mode configured is active
 *
 * Input(s)           : pPortInfo - Port on which this event has occured
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 ******************************************************************************/
PRIVATE VOID
EoamSemStateActiveSendLocal (tEoamPortInfo * pPortInfo)
{
    EOAM_TRC_FN_ENTRY ();

    /* Set EOAM OperStatus to ACTIVE_SEND_LOCAL */
    pPortInfo->LocalInfo.u2EoamOperStatus = EOAM_OPER_ACTIVE_SENDLOCAL;

    /* Update local_pdu to send and receive Info OAMPDU */
    pPortInfo->pEoamEnaPortInfo->CtrlInd.LocalPdu = EOAM_INFO_ONLY;

    EOAM_TRC ((EOAM_DISCOVERY_TRC, "Port %u: Entered "
               "EoamSemStateActiveSendLocal\n", pPortInfo->u4Port));

    /* Form Info pdu after discovery restart with local TLV alone */
    EoamCtlTxConstructInfoPdu (pPortInfo);

    /* Send Info OAMPDU with Local TLV */
    EoamCtlTxSendInfoPdu (pPortInfo);

    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function           : EoamSemStatePassiveWait 
 *
 * Description        : This procedure is invoked after the Fault state 
 *                      when the link is up and the mode configured is passive
 *
 * Input(s)           : pPortInfo - Port on which this event has occured
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 ******************************************************************************/
PRIVATE VOID
EoamSemStatePassiveWait (tEoamPortInfo * pPortInfo)
{
    EOAM_TRC_FN_ENTRY ();

    EOAM_TRC ((EOAM_DISCOVERY_TRC, "Port %u: Entered "
               "EoamSemStatePassiveWait\n", pPortInfo->u4Port));

    /* Set EOAM OperStatus to PASSIVE_WAIT */
    pPortInfo->LocalInfo.u2EoamOperStatus = EOAM_OPER_PASSIVEWAIT;

    /* Update local_pdu to receive only Info OAMPDU */
    pPortInfo->pEoamEnaPortInfo->CtrlInd.LocalPdu = EOAM_RX_INFO;

    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function           : EoamSemStateSendLocalRemote 
 *
 * Description        : This procedure is invoked when a valid Information
 *                      OAMPDU is received from remote peer when in 
 *                      ACTIVE_SEND_LOCAL or PASSIVE_WAIT state or local DTE
 *                      is not satisfied when in SEND_ANY state
 *                      
 * Input(s)           : pPortInfo - Port on which this event has occured
 * 
 * Output(s)          : None.
 * 
 * Returns            : None.
 ******************************************************************************/
PRIVATE VOID
EoamSemStateSendLocalRemote (tEoamPortInfo * pPortInfo)
{
    EOAM_TRC_FN_ENTRY ();

    if (pPortInfo->LocalInfo.u2EoamOperStatus == EOAM_OPER_PASSIVEWAIT)
    {
        /* When both local node and remote node are in passive mode,
         * do not accept as it is not a valid peering combination */
        if (pPortInfo->RemoteInfo.u1Mode == EOAM_MODE_PASSIVE)
        {
            EOAM_TRC ((EOAM_DISCOVERY_TRC, "Port %u: Remote "
                       "Peer is also in Passive mode - discarded\n",
                       pPortInfo->u4Port));
            /* Initialize RemoteEoamInfo */
            MEMSET (&(pPortInfo->RemoteInfo), 0, sizeof (tEoamPeerInfo));
            pPortInfo->RemoteInfo.u1Mode = EOAM_MODE_PEER_UNKNOWN;
            pPortInfo->RemoteInfo.u2MaxOamPduSize = EOAM_MIN_PDU_SIZE;
            pPortInfo->pEoamEnaPortInfo->CtrlReq.b1RemoteStateValid =
                OSIX_FALSE;

            return;
        }
    }

    EOAM_TRC ((EOAM_DISCOVERY_TRC, "Port %u: Entered "
               "EoamSemStateSendLocalRemote\n", pPortInfo->u4Port));

    /* Set EOAM OperStatus to SEND_LOCAL_REMOTE or LOCALLYREJECTED.
     * Both fall within the state SEND_LOCAL_REMOTE of the discovery
     * state diagramm, with the difference being whether the local
     * OAM client has actively rejected the peer or just has not 
     * indicated any decision yet */
    pPortInfo->pEoamEnaPortInfo->CtrlInd.b1LocalStable = OSIX_FALSE;

    /* Update local_pdu to send and receive Info OAMPDU */
    pPortInfo->pEoamEnaPortInfo->CtrlInd.LocalPdu = EOAM_INFO_ONLY;

    if ((EOAM_RM_NODE_STATUS () == EOAM_ACTIVE_NODE) &&
        ((pPortInfo->LocalInfo.u2EoamOperStatus ==
          EOAM_OPER_ACTIVE_SENDLOCAL) ||
         (pPortInfo->LocalInfo.u2EoamOperStatus == EOAM_OPER_PASSIVEWAIT)))
    {
        /* Start the Lost Link Timer */
        if (TmrStart (gEoamGlobalInfo.EoamTmrListId,
                      &(pPortInfo->pEoamEnaPortInfo->LostLinkAppTmr),
                      EOAM_LOST_LINK_TMR_TYPE, EOAM_LOST_LINK_TIMEOUT, 0) ==
            TMR_FAILURE)
        {
            EOAM_TRC ((EOAM_DISCOVERY_TRC | OS_RESOURCE_TRC |
                       ALL_FAILURE_TRC,
                       "StartTimer for LostLinkAppTmr FAILED!!!\n"));
        }

        pPortInfo->LocalInfo.u2EoamOperStatus = EOAM_OPER_SENDLOCALREMOTE;
        pPortInfo->pEoamEnaPortInfo->CtrlInd.b1LocalEvaluating = OSIX_TRUE;
    }
    else
    {
        pPortInfo->LocalInfo.u2EoamOperStatus =
            EOAM_OPER_PEERINGLOCALLYREJECTED;
        pPortInfo->pEoamEnaPortInfo->CtrlInd.b1LocalEvaluating = OSIX_FALSE;

        /* Sync discovery restart with Standby Node */
        EoamRedSyncUpDynamicInfo (pPortInfo);
    }

    /* Remote Stable and evaluating bits have changed, so reconstruct
     * info pdu and send */
    EoamCtlTxConstructInfoPdu (pPortInfo);
    /* Send Info OAMPDU with Local and Remote TLV */
    EoamCtlTxSendInfoPdu (pPortInfo);

    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function           : EoamSemStateSendLocalRemoteOk 
 *
 * Description        : This procedure is invoked when a local DTE is in
 *                      SEND_LOCAL_REMOTE state and is satisfied with remote 
 *                      peer's settings. Also when the remote DTE is not
 *                      when in SEND_ANY state
 *
 * Input(s)           : pPortInfo - Port on which this event has occured
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 ******************************************************************************/
PRIVATE VOID
EoamSemStateSendLocalRemoteOk (tEoamPortInfo * pPortInfo)
{
    EOAM_TRC_FN_ENTRY ();
    EOAM_TRC ((EOAM_DISCOVERY_TRC, "Port %u: Entered "
               "EoamSemStateSendLocalRemoteOk\n", pPortInfo->u4Port));

    /* Update local_pdu to send and receive Info OAMPDU */
    pPortInfo->pEoamEnaPortInfo->CtrlInd.LocalPdu = EOAM_INFO_ONLY;

    pPortInfo->pEoamEnaPortInfo->CtrlInd.b1LocalStable = OSIX_TRUE;

    pPortInfo->pEoamEnaPortInfo->CtrlInd.b1LocalEvaluating = OSIX_FALSE;

    /* Set EOAM OperStatus to SEND_LOCAL_REMOTE_OK or REMOTELYREJECTED
     * Both fall within the state SEND_LOCAL_REMOTE of the discovery
     * state diagramm, with the difference being whether the local
     * OAM client has actively rejected the peer or just has not 
     * indicated any decision yet */

    if (pPortInfo->LocalInfo.u2EoamOperStatus == EOAM_OPER_SENDLOCALREMOTE)
    {
        pPortInfo->LocalInfo.u2EoamOperStatus = EOAM_OPER_SENDLOCALREMOTEOK;
    }
    else
    {
        pPortInfo->LocalInfo.u2EoamOperStatus =
            EOAM_OPER_PEERINGREMOTELYREJECTED;
        /* Sync discovery status with Standby Node */
        EoamRedSyncUpDynamicInfo (pPortInfo);
    }

    /* Local stable has changed, so reconstruct pdu and send */
    EoamCtlTxConstructInfoPdu (pPortInfo);
    /* Send Info OAMPDU with Local and Remote TLV */
    EoamCtlTxSendInfoPdu (pPortInfo);

    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function           : EoamSemStateSendAny
 *
 * Description        : This procedure is invoked when a local DTE is in
 *                      SEND_LOCAL_REMOTE_OK state and remote peer is also      
 *                      satisfied.                               
 *
 * Input(s)           : pPortInfo - Port on which this event has occured
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 ******************************************************************************/
PRIVATE VOID
EoamSemStateSendAny (tEoamPortInfo * pPortInfo)
{
    EOAM_TRC_FN_ENTRY ();
    EOAM_TRC ((EOAM_DISCOVERY_TRC, "Port %u: Entered "
               "EoamSemStateSendAny\n", pPortInfo->u4Port));
    /* Set EOAM OperStatus to SEND_ANY */
    pPortInfo->LocalInfo.u2EoamOperStatus = EOAM_OPER_OPERATIONAL;

    /* Update local_pdu to send and receive Info OAMPDU */
    pPortInfo->pEoamEnaPortInfo->CtrlInd.LocalPdu = EOAM_ANY;

    /* Remote stable has changed, update info pdu and send */
    EoamCtlTxConstructInfoPdu (pPortInfo);
    /* Send Info OAMPDU with Local and Remote TLV */
    EoamCtlTxSendInfoPdu (pPortInfo);

    /* Sync discovery status with Standby Node */
    EoamRedSyncUpDynamicInfo (pPortInfo);
    /* Notify events to all Registered Modules */
    EoamSemNotifyAllEvents (pPortInfo);
    EOAM_TRC_FN_EXIT ();
    return;
}

/******************************************************************************
 * Function           : EoamSemNotifyAllEvents
 *
 * Description        : This procedure is invoked to notify all Events when
 *                      the Eoam Status on an Interface becomes Operational
 *
 * Input(s)           : pPortInfo - Port on which this event has occured
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 ******************************************************************************/
PRIVATE VOID
EoamSemNotifyAllEvents (tEoamPortInfo * pPortInfo)
{
    tEoamCallbackInfo   CallbackInfo;

    MEMSET (&CallbackInfo, 0, sizeof (tEoamCallbackInfo));

    EOAM_TRC_FN_ENTRY ();

    CallbackInfo.u4Port = pPortInfo->u4Port;
    CallbackInfo.u2Location = EOAM_REMOTE_ENTRY;

    /* Link Fault is always false in SendAny state */
    CallbackInfo.uEventInfo.u1State = OSIX_FALSE;
    CallbackInfo.u4EventType = EOAM_NOTIFY_LINK_FAULT;
    EoamMainNotifyModules (&CallbackInfo);

    EOAM_TRC ((EOAM_RFI_TRC, "Link fault Event status sent to the"
               "registered modules for port %u.\n", pPortInfo->u4Port));

    /* Dying Gasp Event */
    if (pPortInfo->RemoteInfo.u2Flags & EOAM_DYING_GASP_BITMASK)
    {
        CallbackInfo.uEventInfo.u1State = OSIX_TRUE;
    }
    else
    {
        CallbackInfo.uEventInfo.u1State = OSIX_FALSE;
    }

    CallbackInfo.u4EventType = EOAM_NOTIFY_DYING_GASP;
    EoamMainNotifyModules (&CallbackInfo);

    EOAM_TRC ((EOAM_RFI_TRC, "Dying Gasp Event status sent to the"
               "registered modules for port %u.\n", pPortInfo->u4Port));

    /* Critical Event */
    if (pPortInfo->RemoteInfo.u2Flags & EOAM_CRITICAL_EVENT_BITMASK)
    {
        CallbackInfo.uEventInfo.u1State = OSIX_TRUE;
    }
    else
    {
        CallbackInfo.uEventInfo.u1State = OSIX_FALSE;
    }

    CallbackInfo.u4EventType = EOAM_NOTIFY_CRITICAL_EVENT;
    EoamMainNotifyModules (&CallbackInfo);

    EOAM_TRC ((EOAM_RFI_TRC, "Critical Event status sent to the"
               "registered modules for port %u.\n", pPortInfo->u4Port));

    EOAM_TRC_FN_EXIT ();
    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  emsem.c                        */
/*-----------------------------------------------------------------------*/
