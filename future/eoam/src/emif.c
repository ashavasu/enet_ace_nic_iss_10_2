/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emif.c,v 1.28 2014/03/06 12:50:26 siva Exp $
 *
 * Description: This file contains procedures for 
 *              - creating/deleting interface structures 
 *              - handling interface up/down, interface parameter change 
 *                notifications from lower layer.  
 *********************************************************************/
#include "eminc.h"
#include "emlmglob.h"

/* Proto types of the functions private to this file only */
PRIVATE VOID EoamIfCopyEvtParamsToLm PROTO ((tEoamPortInfo * pPortInfo));
#ifdef NPAPI_WANTED
PRIVATE INT4 EoamIfIsAutoNegEnabledAndGigPort PROTO ((UINT4));
#endif /* NPAPI_WANTED */
extern VOID EoamCltInfTriggerCriticalEvtMsg PROTO ((tEoamPortInfo *, UINT1));
/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamIfEnable 
 *                                                                          
 *    DESCRIPTION      : This function is called when EOAM is enabled on a 
 *                       port. It adds a DLL node to EOAM enabled list and
 *                       start the lost link timer for the port. 
 *
 *    INPUT            : pPortInfo - Pointer to EOAM info
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamIfEnable (tEoamPortInfo * pPortInfo)
{
    tEoamEnaPortInfo   *pEnaPortInfo = NULL;
    UINT4               u4UniDirSupp = EOAM_ENABLED;

    EOAM_TRC_FN_ENTRY ();

    FSAP_ASSERT (pPortInfo != NULL);

    /* Allocate memory for the new EOAM entry */
    if ((pEnaPortInfo = (tEoamEnaPortInfo *) (MemAllocMemBlk
                                              (gEoamGlobalInfo.
                                               EnaPortPoolId))) == NULL)
    {
        EOAM_TRC ((BUFFER_TRC, "EoamIfEnable: Alloc mem block FAILED!!!\n"));
        return OSIX_FAILURE;
    }

    /* Initialize the EOAM information */
    MEMSET (pEnaPortInfo, 0, sizeof (tEoamEnaPortInfo));

    /* Store the back pointer of the port info struct. */
    pEnaPortInfo->pPortInfoBackPtr = pPortInfo;

    pPortInfo->pEoamEnaPortInfo = pEnaPortInfo;

    /* Add the node to Enabled port DLL.  */
    TMO_DLL_Init_Node (&(pPortInfo->EoamEnaPortNode));
    TMO_DLL_Add (&(gEoamGlobalInfo.EoamEnaPortDllList),
                 &(pPortInfo->EoamEnaPortNode));

    /* Set the EOAM status as ENABLED in CFA. */
    EoamSetIfInfo (IF_EOAM_STATUS, pPortInfo->u4Port, EOAM_ENABLED);

    /* Update Unidirectional capability at CFA. */
    if (pPortInfo->LocalInfo.u1FnsSupportedBmp & EOAM_UNIDIRECTIONAL_SUPPORT)
    {
        u4UniDirSupp = EOAM_ENABLED;
    }
    else
    {
        u4UniDirSupp = EOAM_DISABLED;
    }

    EoamSetIfInfo (IF_EOAM_UNIDIR_SUPP, pPortInfo->u4Port, u4UniDirSupp);

    if (pPortInfo->u1OperStatus == CFA_IF_UP)
    {
        EoamSemMachine (pPortInfo, EOAM_DISC_EVENT_OPER_UP);
    }
    else
    {
        pPortInfo->LocalInfo.u2EoamOperStatus = EOAM_OPER_LINKFAULT;
    }
    /* Add the port entry to Link Monitoring module */
    if (gu1EoamLMStatFlag == EOAM_LM_ENABLED)
    {
        EoamLmIfAdd (pPortInfo->u4Port);
    }

    EoamIfCopyEvtParamsToLm (pPortInfo);

    pPortInfo->LocalInfo.u1EoamAdminStatus = EOAM_ENABLED;

    EOAM_TRC_FN_EXIT ();

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamIfDisable 
 *                                                                          
 *    DESCRIPTION      : This function is called when EOAM is disabled on a 
 *                       port. It deletes the DLL node from the EOAM enabled 
 *                       list and stops the lost link timer for the port. 
 *
 *    INPUT            : pPortInfo - Pointer to EOAM port info
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamIfDisable (tEoamPortInfo * pPortInfo)
{
    tEoamEnaPortInfo   *pEnaPortInfo = NULL;
    tEoamCallbackInfo   CallbackInfo;

    EOAM_TRC_FN_ENTRY ();

    MEMSET (&CallbackInfo, 0, sizeof (tEoamCallbackInfo));

    /* Abort if port entry pointer is NULL */
    FSAP_ASSERT (pPortInfo != NULL);

    MEMSET (&pPortInfo->RemoteInfo, 0, sizeof (pPortInfo->RemoteInfo));
    pPortInfo->RemoteInfo.u1Mode = EOAM_MODE_PEER_UNKNOWN;

    pEnaPortInfo = pPortInfo->pEoamEnaPortInfo;

    if (gEoamGlobalInfo.u1SystemCtrlStatus != EOAM_SHUTDOWN)
    {
        EoamCltInfTriggerCriticalEvtMsg (pPortInfo, OSIX_TRUE);
    }

    /* Reset Loopback status and inform registered modules */
    if (pPortInfo->LoopbackInfo.u1Status == EOAM_LOCAL_LOOPBACK)
    {
        /* Copied from EoamCltInfHandleDiscovery */
        pPortInfo->LoopbackInfo.u1Status = EOAM_NO_LOOPBACK;
        EoamMuxParSet (pPortInfo, EOAM_NO_LOOPBACK);
        pPortInfo->LocalInfo.u2ConfigRevision++;
#ifdef NPAPI_WANTED
        if (EOAM_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE)
        {
            if (EoamEoamNpHandleLocalLoopBack
                (pPortInfo->u4Port, NULL, NULL, EOAM_NP_LB_DISABLE) !=
                FNP_SUCCESS)
            {
                EOAM_TRC ((ALL_FAILURE_TRC, "EoamIfDisable: "
                           "EoamEoamNpHandleLocalLoopBack failed on port %u\n",
                           pPortInfo->u4Port));
            }
        }
#endif
    }
    else if (pPortInfo->LoopbackInfo.u1Status == EOAM_INITIATING_LOOPBACK)
    {
        /* Copied from EoamCltLbHandleTmrExpiry */
        pPortInfo->LoopbackInfo.u1Status = EOAM_NO_LOOPBACK;
        EoamMuxParSet (pPortInfo, EOAM_NO_LOOPBACK);
        EoamSetIfInfo (IF_EOAM_REMOTE_LB, pPortInfo->u4Port, EOAM_DISABLED);
        pPortInfo->LocalInfo.u2ConfigRevision++;
        TmrStop (gEoamGlobalInfo.EoamTmrListId, &pEnaPortInfo->LBTxTmr);
    }
    else if ((pPortInfo->LoopbackInfo.u1Status == EOAM_TERMINATING_LOOPBACK) ||
             (pPortInfo->LoopbackInfo.u1Status == EOAM_REMOTE_LOOPBACK))
    {
        /* Copied from EoamCltLbHandleAck */
        pPortInfo->LoopbackInfo.u1Status = EOAM_NO_LOOPBACK;
        EoamMuxParSet (pPortInfo, EOAM_NO_LOOPBACK);
        EoamSetIfInfo (IF_EOAM_REMOTE_LB, pPortInfo->u4Port, EOAM_DISABLED);
        pPortInfo->LocalInfo.u2ConfigRevision++;
        TmrStop (gEoamGlobalInfo.EoamTmrListId, &pEnaPortInfo->LBTxTmr);

        /* When already in Remote loopback, inform FM that Remote
         * loopback mode is being reset */
        CallbackInfo.u4EventType = EOAM_NOTIFY_LB_ACK_RCVD;
        CallbackInfo.EventState = EOAM_DISABLED;
        CallbackInfo.u2Location = EOAM_REMOTE_ENTRY;
        CallbackInfo.u4Port = pPortInfo->u4Port;
        /* Call a function to notify all registered modules */
        EoamMainNotifyModules (&CallbackInfo);
#ifdef NPAPI_WANTED
        if (EOAM_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE)
        {
            if (EoamEoamNpHandleRemoteLoopBack
                (pPortInfo->u4Port, NULL, NULL, EOAM_NP_LB_DISABLE) !=
                FNP_SUCCESS)
            {
                EOAM_TRC ((ALL_FAILURE_TRC, "EoamIfDisable: "
                           "EoamEoamNpHandleRemoteLoopBack failed on port %u\n",
                           pPortInfo->u4Port));
            }
        }
#endif
    }

    /* Reset discovery status to disable */
    pPortInfo->LocalInfo.u2EoamOperStatus = EOAM_OPER_DISABLE;

    if (pEnaPortInfo != NULL)
    {
        /* Delete information stored when EOAM was enabled. */
        EoamMainClearEnabledInfo (pEnaPortInfo);
        pPortInfo->pEoamEnaPortInfo = NULL;

        /* Delete the node from Enabled port DLL.  */
        TMO_DLL_Delete (&(gEoamGlobalInfo.EoamEnaPortDllList),
                        &(pPortInfo->EoamEnaPortNode));
        TMO_DLL_Init_Node (&(pPortInfo->EoamEnaPortNode));
    }

    /* Set the EOAM status as DISABLED in CFA. */
    if (EoamSetIfInfo (IF_EOAM_STATUS, pPortInfo->u4Port, EOAM_DISABLED)
        == OSIX_FAILURE)
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "EoamSetIfInfo FAILED!!!\n"));
    }

    /* Delete the port entry in Link Monitoring module */
    if (gu1EoamLMStatFlag == EOAM_LM_ENABLED)
    {
        if (EoamLmIfDelete (pPortInfo->u4Port) == OSIX_FAILURE)
        {
            EOAM_TRC ((ALL_FAILURE_TRC, "EoamLmIfDelete FAILED!!!\n"));
        }
    }

    pPortInfo->LocalInfo.u1EoamAdminStatus = EOAM_DISABLED;
    EOAM_TRC_FN_EXIT ();

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamIfCreate
 *                                                                          
 *    DESCRIPTION      : This function allocates memory for the new port entry,
 *                       initializes the entry and adds it to the port table. 
 *
 *    INPUT            : u4PortIndex - Port number to be created
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamIfCreate (UINT4 u4PortIndex)
{
    tEoamPortInfo      *pPortEntry = NULL;
    tEoamIfInfo         IfInfo;
    tEoamCallbackInfo   CallbackInfo;

    EOAM_TRC_FN_ENTRY ();

    MEMSET (&IfInfo, 0, sizeof (tEoamIfInfo));
    MEMSET (&CallbackInfo, 0, sizeof (tEoamCallbackInfo));

    if (gEoamGlobalInfo.u1SystemCtrlStatus == EOAM_SHUTDOWN)
    {
        EOAM_TRC ((INIT_SHUT_TRC, "EOAM module is shut down\n"));
        return OSIX_FAILURE;
    }

    /* Check if an entry already exists for the port */
    EoamUtilGetPortEntry (u4PortIndex, &pPortEntry);

    if (pPortEntry != NULL)
    {
        EOAM_TRC ((INIT_SHUT_TRC, "Port with same index already exists."
                   " Another entry cannot be created\n"));
        return OSIX_SUCCESS;
    }

    /* Allocate memory for the new port entry */
    if ((pPortEntry = (tEoamPortInfo *) (MemAllocMemBlk
                                         (gEoamGlobalInfo.PortPoolId))) == NULL)
    {
        EOAM_TRC ((INIT_SHUT_TRC, "EoamIfCreate: Alloc mem block FAILED!!!\n"));
        return OSIX_FAILURE;
    }

    MEMSET (pPortEntry, 0, sizeof (tEoamPortInfo));

    /* Update the interface specific paramaters obtained from lower layer. */
    pPortEntry->u4Port = u4PortIndex;

    if (EoamGetIfInfo (u4PortIndex, &IfInfo) != OSIX_SUCCESS)
    {
        EOAM_TRC ((INIT_SHUT_TRC, "EoamIfCreate: EoamGetIfInfo FAILED!!!\n"));
        MemReleaseMemBlock (gEoamGlobalInfo.PortPoolId, (UINT1 *) pPortEntry);
        return OSIX_FAILURE;
    }
    pPortEntry->u1OperStatus = IfInfo.u1IfOperStatus;
    pPortEntry->u4Mtu = IfInfo.u4IfMtu;

    EoamIfInit (pPortEntry);

    /* Add the port entry to the port table */
    EoamIfAddToPortTable (pPortEntry);

    CallbackInfo.u4EventType = EOAM_NOTIFY_PORT_CREATE;
    CallbackInfo.u4Port = pPortEntry->u4Port;
    /* Call a function to notify registered modules */
    EoamMainNotifyModules (&CallbackInfo);

    EOAM_TRC_FN_EXIT ();

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamIfDelete
 *                                                                          
 *    DESCRIPTION      : This function releases memory for the port entry,
 *                       and removes it from the port table. 
 *
 *    INPUT            : pPortEntry - Port Entry to be deleted
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamIfDelete (tEoamPortInfo * pPortEntry)
{
    tEoamCallbackInfo   CallbackInfo;
    EOAM_TRC_FN_ENTRY ();

    MEMSET (&CallbackInfo, 0, sizeof (tEoamCallbackInfo));

    if (gEoamGlobalInfo.u1SystemCtrlStatus == EOAM_SHUTDOWN)
    {
        EOAM_TRC ((INIT_SHUT_TRC, "EOAM mdule is shut down\n"));
        return OSIX_FAILURE;
    }

    /* Delete the port entry from the port table */
    if (pPortEntry != NULL)
    {
        CallbackInfo.u4EventType = EOAM_NOTIFY_PORT_DELETE;
        CallbackInfo.u4Port = pPortEntry->u4Port;
        /* Call a function to notify registered modules */
        EoamMainNotifyModules (&CallbackInfo);
        EoamIfDisable (pPortEntry);
        EoamMainClearPortInfo (pPortEntry);
    }
    EOAM_TRC_FN_EXIT ();

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamIfOperChg
 *                                                                          
 *    DESCRIPTION      : This function is called when the oper status of the 
 *                       interface changes.  
 *
 *    INPUT            : pPortInfo - Pointer to port info struct.
 *                       u1OperStatus - Oper status of the interface   
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamIfOperChg (tEoamPortInfo * pPortInfo, UINT1 u1OperStatus)
{
    tEoamIfInfo         IfInfo;
    INT4                i4EoamEnabled = EOAM_DISABLED;
    UINT2               u2PrevMaxPdu = 0;
    UINT1               u1Event = 0;

#ifdef NPAPI_WANTED
    UINT1               u1ConstructFlag = EOAM_FALSE;
#endif

    EOAM_TRC_FN_ENTRY ();

    MEMSET (&IfInfo, 0, sizeof (tEoamIfInfo));

    if (gEoamGlobalInfo.u1SystemCtrlStatus == EOAM_SHUTDOWN)
    {
        EOAM_TRC ((INIT_SHUT_TRC, "EOAM module is shut down\n"));
        return OSIX_FAILURE;
    }

    if (pPortInfo == NULL)
    {
        EOAM_TRC ((ALL_FAILURE_TRC, "EoamIfOperChg: pPortInfo NULL"
                   " pointer received\r\n"));
        return OSIX_FAILURE;
    }

    if (pPortInfo->u1OperStatus == u1OperStatus)
    {
        EOAM_TRC ((INIT_SHUT_TRC, "No change in oper status of the"
                   " interface\n"));
        return OSIX_SUCCESS;
    }

    /* Operation status of the interface is modified. 
     * Update the new oper status in Eoam portInfo. */
    pPortInfo->u1OperStatus = u1OperStatus;
    /* Sync up Oper status change with the peer standby node */
    EoamRedSyncUpOperStatus (pPortInfo);

    i4EoamEnabled = pPortInfo->LocalInfo.u1EoamAdminStatus;

    if (pPortInfo->u1OperStatus == CFA_IF_UP)
    {
        EOAM_TRC ((CONTROL_PLANE_TRC, "EoamIfOperChg: "
                   " OperStatus chg rcvd for interface %u - UP\n",
                   pPortInfo->u4Port));

        /* Initialize LocalEoamInfo */
        pPortInfo->LocalInfo.u2ConfigRevision = EOAM_DEF_CONFIG_REVISION;

        u1Event = EOAM_DISC_EVENT_OPER_UP;

#ifdef NPAPI_WANTED
        /* Check whether AutoNeg status has changed incase of
         * NPAPI. This is required only when NPAPI supports Unidirectional
         * transmission capability */
        if (EoamIfIsAutoNegEnabledAndGigPort (pPortInfo->u4Port) ==
            OSIX_SUCCESS)
        {
            if (pPortInfo->LocalInfo.u1FnsSupportedBmp &
                EOAM_UNIDIRECTIONAL_SUPPORT)
            {

                pPortInfo->LocalInfo.u1FnsSupportedBmp &=
                    ~EOAM_UNIDIRECTIONAL_SUPPORT;
                u1ConstructFlag = EOAM_TRUE;
            }

        }
        else
        {

            if (!(pPortInfo->LocalInfo.u1FnsSupportedBmp &
                  EOAM_UNIDIRECTIONAL_SUPPORT))
            {
                pPortInfo->LocalInfo.u1FnsSupportedBmp |=
                    EOAM_UNIDIRECTIONAL_SUPPORT;
                u1ConstructFlag = EOAM_TRUE;
            }
        }
        if (u1ConstructFlag == EOAM_TRUE)
        {
            if (pPortInfo->pEoamEnaPortInfo != NULL)
            {
                EoamCtlTxConstructInfoPdu (pPortInfo);
            }
        }

#endif

        EoamGetIfInfo (pPortInfo->u4Port, &IfInfo);
        if (pPortInfo->u4Mtu != IfInfo.u4IfMtu)
        {
            /* MTU change */
            pPortInfo->u4Mtu = IfInfo.u4IfMtu;
            u2PrevMaxPdu = pPortInfo->LocalInfo.u2MaxOamPduSize;

            /* LocalInfo.u2MaxOamPduSize is the OAMPDU configuration field
             * in the Local TLV sent. It is defined as MTU (payload) of the 
             * interface + Ethernet header + FCS. The maximum allowable 
             * OAMPDU size is 1518. If jumbo frame support is present, the
             * MTU size may exceed 1500 and hence a check is done to make
             * sure u2MaxOamPduSize never exceeds 1518 */
            if (pPortInfo->u4Mtu > EOAM_MAX_DATA_SIZE)
            {
                pPortInfo->LocalInfo.u2MaxOamPduSize = EOAM_MAX_PDU_SIZE;
            }
            else
            {
                pPortInfo->LocalInfo.u2MaxOamPduSize =
                    (UINT2) (pPortInfo->u4Mtu + CFA_ENET_V2_HEADER_SIZE +
                             EOAM_ETH_FCS_LEN);
            }
            /* Update Config revision if required */
            if ((pPortInfo->LocalInfo.u2MaxOamPduSize != u2PrevMaxPdu) &&
                (i4EoamEnabled == EOAM_ENABLED))
            {
                pPortInfo->LocalInfo.u2ConfigRevision++;
            }
        }
    }
    else if (pPortInfo->u1OperStatus == CFA_IF_DOWN)
    {
        EOAM_TRC ((CONTROL_PLANE_TRC, "EoamIfOperChg: OperStatus "
                   "change  rcvd for interface %u - DOWN\n",
                   pPortInfo->u4Port));
        u1Event = EOAM_DISC_EVENT_OPER_DOWN;
    }

    if (i4EoamEnabled == EOAM_ENABLED)
    {
        if ((u1Event == EOAM_DISC_EVENT_OPER_UP) ||
            (u1Event == EOAM_DISC_EVENT_OPER_DOWN))
        {
            EoamSemMachine (pPortInfo, u1Event);
        }
    }

    EOAM_TRC_FN_EXIT ();

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamIfInit 
 *                                                                          
 *    DESCRIPTION      : This function initializes the port strucutre with
 *                       default values. 
 *
 *    INPUT            : pPortInfo - Eoam Port specific information
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EoamIfInit (tEoamPortInfo * pPortInfo)
{
    EOAM_TRC_FN_ENTRY ();

    TMO_DLL_Init_Node (&(pPortInfo->EoamEnaPortNode));

    /* Initialize LocalEoamInfo */
    pPortInfo->LocalInfo.u2ConfigRevision = EOAM_DEF_CONFIG_REVISION;

    /* LocalInfo.u2MaxOamPduSize is the OAMPDU configuration field
     * in the Local TLV sent. It is defined as MTU (payload) of the 
     * interface + Ethernet header + FCS. The maximum allowable 
     * OAMPDU size is 1518. If jumbo frame support is present, the
     * MTU size may exceed 1500 and hence a check is done to make
     * sure u2MaxOamPduSize never exceeds 1518 */
    if (pPortInfo->u4Mtu > EOAM_MAX_DATA_SIZE)
    {
        pPortInfo->LocalInfo.u2MaxOamPduSize = EOAM_MAX_PDU_SIZE;
    }
    else
    {
        pPortInfo->LocalInfo.u2MaxOamPduSize = (UINT2) (pPortInfo->u4Mtu +
                                                        CFA_ENET_V2_HEADER_SIZE
                                                        + EOAM_ETH_FCS_LEN);
    }
    pPortInfo->LocalInfo.u2EoamOperStatus = EOAM_OPER_DISABLE;
    pPortInfo->LocalInfo.u1EoamAdminStatus = EOAM_DISABLED;
    pPortInfo->LocalInfo.u1Mode = EOAM_MODE_ACTIVE;
    pPortInfo->LocalInfo.u1FnsSupportedBmp = EOAM_LOOPBACK_SUPPORT |
        EOAM_EVENT_SUPPORT | EOAM_VARIABLE_SUPPORT;

#ifdef NPAPI_WANTED

    EoamIfSetUniDirectionalCpb (pPortInfo);

#endif

    /* Initialize RemoteEoamInfo */
    pPortInfo->RemoteInfo.u1Mode = EOAM_MODE_PEER_UNKNOWN;
    pPortInfo->RemoteInfo.u2MaxOamPduSize = EOAM_MIN_PDU_SIZE;

    /* Initalize Loopback Info */
    pPortInfo->LoopbackInfo.u1Status = EOAM_NO_LOOPBACK;
    pPortInfo->LoopbackInfo.u1IgnoreRx = EOAM_IGNORE_LB_CMD;

    /* Initialize Error Event Configurations */
    FSAP_U8_ASSIGN_LO (&(pPortInfo->ErrEventConfigInfo.u8ErrSymPeriodWindow),
                       EOAM_DEF_SYMBOL_PERIOD_WINDOW_LO);
    FSAP_U8_ASSIGN_LO (&(pPortInfo->ErrEventConfigInfo.u8ErrSymPeriodThreshold),
                       EOAM_DEF_SYMBOL_PERIOD_THRESH_LO);
    pPortInfo->ErrEventConfigInfo.u4ErrFrameWindow = EOAM_DEF_FRAME_WINDOW;
    pPortInfo->ErrEventConfigInfo.u4ErrFrameThreshold = EOAM_DEF_FRAME_THRESH;
    pPortInfo->ErrEventConfigInfo.u4ErrFramePeriodWindow =
        EOAM_DEF_FRAME_PERIOD_WINDOW;
    pPortInfo->ErrEventConfigInfo.u4ErrFramePeriodThreshold =
        EOAM_DEF_FRAME_PERIOD_THRESH;
    pPortInfo->ErrEventConfigInfo.u4ErrFrameSecsSummaryWindow =
        EOAM_DEF_FRAME_SECS_WINDOW;
    pPortInfo->ErrEventConfigInfo.u4ErrFrameSecsSummaryThreshold =
        EOAM_DEF_FRAME_SECS_THRESH;

    pPortInfo->ErrEventConfigInfo.u1ErrSymPeriodEvNotifEnable = EOAM_TRUE;
    pPortInfo->ErrEventConfigInfo.u1ErrFrameEvNotifEnable = EOAM_TRUE;
    pPortInfo->ErrEventConfigInfo.u1ErrFramePeriodEvNotifEnable = EOAM_TRUE;
    pPortInfo->ErrEventConfigInfo.u1ErrFrameSecsEvNotifEnable = EOAM_TRUE;
    pPortInfo->ErrEventConfigInfo.u1DyingGaspEnable = EOAM_TRUE;
    pPortInfo->ErrEventConfigInfo.u1CriticalEventEnable = EOAM_TRUE;

    pPortInfo->u1LogPosToBeFilled = 0;

    EOAM_TRC_FN_EXIT ();

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamIfAddToPortTable
 *                                                                          
 *    DESCRIPTION      : This function adds a port into the global struct.
 *                       It is called whenever a new port is created.   
 *
 *    INPUT            : pPortInfo - Pointer to port information struct.
 *                                                                          
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamIfAddToPortTable (tEoamPortInfo * pPortInfo)
{
    UINT4               u4PortIndex = 0;

    EOAM_TRC_FN_ENTRY ();

    u4PortIndex = pPortInfo->u4Port;
    if (gEoamGlobalInfo.apEoamPortEntry[u4PortIndex - 1] != NULL)
    {
        EOAM_TRC ((EOAM_CRITICAL_TRC | ALL_FAILURE_TRC, "Port Entry already"
                   " exists for port %d\n", u4PortIndex));
        return OSIX_FAILURE;
    }

    gEoamGlobalInfo.apEoamPortEntry[u4PortIndex - 1] = pPortInfo;

    EOAM_TRC_FN_EXIT ();

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamIfCreateAllPorts
 *                                                                          
 *    DESCRIPTION      : This function creates all valid ports in EOAM module. 
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
EoamIfCreateAllPorts (VOID)
{
    UINT4               u4PortIndex = 0;

    EOAM_TRC_FN_ENTRY ();

    for (u4PortIndex = EOAM_MIN_PORTS; u4PortIndex <= EOAM_MAX_PORTS;
         u4PortIndex++)
    {
        if (EoamValidateIfIndex (u4PortIndex) != OSIX_SUCCESS)
        {
            continue;
        }

        if (EoamIfCreate (u4PortIndex) == OSIX_FAILURE)
        {
            EOAM_TRC ((INIT_SHUT_TRC, "EoamIfCreateAllPorts: Port %u Entry "
                       "creation FAILED\n", u4PortIndex));
        }
        else
        {
            EOAM_TRC ((INIT_SHUT_TRC, "EoamIfCreateAllPorts: Port %u Entry"
                       " created\n", u4PortIndex));
        }
    }

    EOAM_TRC_FN_EXIT ();

}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamIfCopyEvtParamsToLm
 *                                                                          
 *    DESCRIPTION      : This function is called to copy the current thershold 
 *                       event paramaters to Link Monitoring module, when EOAM
 *                       is enabled on a port. 
 *
 *    INPUT            : pPortInfo - Pointer to EOAM port info structure 
 *                                                                          
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
EoamIfCopyEvtParamsToLm (tEoamPortInfo * pPortInfo)
{
    FS_UINT8            u8EvtWindow;
    FS_UINT8            u8EvtThres;

    EOAM_TRC_FN_ENTRY ();

    FSAP_U8_CLR (&u8EvtWindow);
    FSAP_U8_CLR (&u8EvtThres);

    EoamLmIfCopyEventParams
        (pPortInfo->u4Port, EOAM_ERRORED_SYMBOL_EVENT_TLV,
         pPortInfo->ErrEventConfigInfo.u1ErrSymPeriodEvNotifEnable,
         pPortInfo->ErrEventConfigInfo.u8ErrSymPeriodWindow,
         pPortInfo->ErrEventConfigInfo.u8ErrSymPeriodThreshold);

    FSAP_U8_ASSIGN_LO (&u8EvtWindow,
                       pPortInfo->ErrEventConfigInfo.u4ErrFrameWindow);
    FSAP_U8_ASSIGN_LO (&u8EvtThres,
                       pPortInfo->ErrEventConfigInfo.u4ErrFrameThreshold);
    EoamLmIfCopyEventParams
        (pPortInfo->u4Port, EOAM_ERRORED_FRAME_EVENT_TLV,
         pPortInfo->ErrEventConfigInfo.u1ErrFrameEvNotifEnable,
         u8EvtWindow, u8EvtThres);

    FSAP_U8_ASSIGN_LO (&u8EvtWindow,
                       pPortInfo->ErrEventConfigInfo.u4ErrFramePeriodWindow);
    FSAP_U8_ASSIGN_LO (&u8EvtThres,
                       pPortInfo->ErrEventConfigInfo.u4ErrFramePeriodThreshold);
    EoamLmIfCopyEventParams (pPortInfo->u4Port,
                             EOAM_ERRORED_FRAME_PERIOD_EVENT_TLV,
                             pPortInfo->ErrEventConfigInfo.
                             u1ErrFramePeriodEvNotifEnable, u8EvtWindow,
                             u8EvtThres);

    FSAP_U8_ASSIGN_LO (&u8EvtWindow,
                       pPortInfo->ErrEventConfigInfo.
                       u4ErrFrameSecsSummaryWindow);
    FSAP_U8_ASSIGN_LO (&u8EvtThres,
                       pPortInfo->ErrEventConfigInfo.
                       u4ErrFrameSecsSummaryThreshold);
    EoamLmIfCopyEventParams (pPortInfo->u4Port,
                             EOAM_ERRORED_FRAME_SECONDS_EVENT_TLV,
                             pPortInfo->ErrEventConfigInfo.
                             u1ErrFrameSecsEvNotifEnable, u8EvtWindow,
                             u8EvtThres);

    EOAM_TRC_FN_EXIT ();
}

#ifdef NPAPI_WANTED
/******************************************************************************
 * FUNCTION NAME      : EoamIfIsAutoNegEnabledAndGigPort 
 *
 * DESCRIPTION        : This function is called by EOAM to verify 
 *                      auto-negotiation status of the link so that 
 *                      unidirectional capability can be enabled or disabled
 *
 * INPUT              : u4IfIndex - Interface Index
 *
 * OUTPUT             : None
 *
 * RETURNS            : OSIX_SUCCESS if auto-neg enabled and is a gigabit port
 *                      OSIX_FAILURE if auto-neg disabled and not a gigabit
 *                      port.  
 *****************************************************************************/
PRIVATE INT4
EoamIfIsAutoNegEnabledAndGigPort (UINT4 u4IfIndex)
{
    INT4                i4RetVal = OSIX_FAILURE;
    tEoamIfInfo         IfInfo;

    EOAM_TRC_FN_ENTRY ();

    MEMSET (&IfInfo, 0, sizeof (tEoamIfInfo));

    if ((EoamGetIfInfo (u4IfIndex, &IfInfo)) != CFA_SUCCESS)
    {
        return OSIX_SUCCESS;
    }

    if ((IfInfo.u4IfSpeed >= CFA_ENET_SPEED_1G) &&
        (IfInfo.u1AutoNegStatus == ENET_AUTO_ENABLE))
    {
        /* Unidirectional fault signalling support is mutually exclusive
         * with auto negotiation capabilities defined in 802.3z
         * of Gigabit ethernet ports */
        i4RetVal = OSIX_SUCCESS;
    }

    EOAM_TRC_FN_EXIT ();

    return (i4RetVal);
}

/******************************************************************************
 * FUNCTION NAME      : EoamIfSetUniDirectionalCpb 
 *
 * DESCRIPTION        : This function is used to enable the undirectional 
 *                      capability of EOAM module on a paricular port. 
 *
 * INPUT              : pPortInfo - Pointer to EOAM port info structure 
 *
 * OUTPUT             : None.
 *
 * RETURNS            : None.  
 *****************************************************************************/
PUBLIC VOID
EoamIfSetUniDirectionalCpb (tEoamPortInfo * pPortInfo)
{
    UINT1               u1Capability = FNP_FALSE;

    if (EOAM_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE)
    {
        if (EoamEoamNpGetUniDirectionalCapability
            (pPortInfo->u4Port, &u1Capability) != FNP_SUCCESS)
        {
            return;                /* Can't get the undirectional capability */
        }
    }
    if (u1Capability != FNP_TRUE)
    {
        return;                    /* Undirectional capablity not present */
    }

    /* Unidirectional fault signalling support is mutually exclusive
     * with auto negotiation capabilities defined in 802.3z
     * of Gigabit ethernet ports */
    if (EoamIfIsAutoNegEnabledAndGigPort (pPortInfo->u4Port) == OSIX_SUCCESS)
    {
        if (pPortInfo->LocalInfo.u1FnsSupportedBmp &
            EOAM_UNIDIRECTIONAL_SUPPORT)
        {
            pPortInfo->LocalInfo.u1FnsSupportedBmp &=
                ~EOAM_UNIDIRECTIONAL_SUPPORT;
        }

    }
    else
    {

        if (!(pPortInfo->LocalInfo.u1FnsSupportedBmp &
              EOAM_UNIDIRECTIONAL_SUPPORT))
        {
            pPortInfo->LocalInfo.u1FnsSupportedBmp |=
                EOAM_UNIDIRECTIONAL_SUPPORT;
        }
    }
    return;
}
#endif /* NPAPI_WANTED */

/*-----------------------------------------------------------------------*/
/*                       End of the file  emif.c                         */
/*-----------------------------------------------------------------------*/
