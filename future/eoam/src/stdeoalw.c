/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdeoalw.c,v 1.12 2017/08/31 14:02:00 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include "eminc.h"
#include "emcli.h"

PRIVATE INT4 EoamSnmpLowValidateIfIndex PROTO ((INT4 i4IfIndex));
/* LOW LEVEL Routines for Table : Dot3OamTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot3OamTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceDot3OamTable (INT4 i4IfIndex)
{
    if (EoamSnmpLowValidateIfIndex (i4IfIndex) == OSIX_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot3OamTable
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot3OamTable (INT4 *pi4IfIndex)
{
    UINT4               u4Index = 0;

/* Get the First Used Index */
    for (u4Index = EOAM_MIN_PORTS; u4Index <= EOAM_MAX_PORTS; u4Index++)
    {
        if (gEoamGlobalInfo.apEoamPortEntry[u4Index - 1] != NULL)
        {
            *pi4IfIndex = (INT4) u4Index;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot3OamTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot3OamTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    UINT4               u4Index = 0;

    if ((i4IfIndex < EOAM_MIN_PORTS) || (i4IfIndex >= EOAM_MAX_PORTS))
    {
        return SNMP_FAILURE;
    }

    /* Next Index should always return the next used Index */
    for (u4Index = i4IfIndex + 1; u4Index <= EOAM_MAX_PORTS; u4Index++)
    {
        if (gEoamGlobalInfo.apEoamPortEntry[u4Index - 1] != NULL)
        {
            *pi4NextIfIndex = (INT4) u4Index;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot3OamAdminState
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamAdminState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot3OamAdminState (INT4 i4IfIndex, INT4 *pi4RetValDot3OamAdminState)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    if (pPortEntry->LocalInfo.u1EoamAdminStatus == EOAM_ENABLED)
    {
        *pi4RetValDot3OamAdminState = EOAM_SNMP_ENABLED;
    }
    else
    {
        *pi4RetValDot3OamAdminState = EOAM_SNMP_DISABLED;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamOperStatus
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot3OamOperStatus (INT4 i4IfIndex, INT4 *pi4RetValDot3OamOperStatus)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pi4RetValDot3OamOperStatus = (INT4) pPortEntry->LocalInfo.u2EoamOperStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamMode
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot3OamMode (INT4 i4IfIndex, INT4 *pi4RetValDot3OamMode)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pi4RetValDot3OamMode = (INT4) pPortEntry->LocalInfo.u1Mode;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamMaxOamPduSize
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamMaxOamPduSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot3OamMaxOamPduSize (INT4 i4IfIndex,
                            UINT4 *pu4RetValDot3OamMaxOamPduSize)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pu4RetValDot3OamMaxOamPduSize = pPortEntry->LocalInfo.u2MaxOamPduSize;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamConfigRevision
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamConfigRevision
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot3OamConfigRevision (INT4 i4IfIndex,
                             UINT4 *pu4RetValDot3OamConfigRevision)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pu4RetValDot3OamConfigRevision = pPortEntry->LocalInfo.u2ConfigRevision;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamFunctionsSupported
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamFunctionsSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot3OamFunctionsSupported (INT4 i4IfIndex, tSNMP_OCTET_STRING_TYPE *
                                 pRetValDot3OamFunctionsSupported)
{
    UINT1               u1SupportedBmp = 0;
    UINT1               u1PortFnBmp = 0;
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    u1PortFnBmp = pPortEntry->LocalInfo.u1FnsSupportedBmp;
    u1SupportedBmp |= ((u1PortFnBmp & EOAM_UNIDIRECTIONAL_SUPPORT) ?
                       EOAM_SNMP_UNIDIRECTIONAL_SUPPORT : u1SupportedBmp);
    u1SupportedBmp |= ((u1PortFnBmp & EOAM_LOOPBACK_SUPPORT) ?
                       EOAM_SNMP_LOOPBACK_SUPPORT : u1SupportedBmp);
    u1SupportedBmp |= ((u1PortFnBmp & EOAM_EVENT_SUPPORT) ?
                       EOAM_SNMP_EVENT_SUPPORT : u1SupportedBmp);
    u1SupportedBmp |= ((u1PortFnBmp & EOAM_VARIABLE_SUPPORT) ?
                       EOAM_SNMP_VARIABLE_SUPPORT : u1SupportedBmp);

    pRetValDot3OamFunctionsSupported->pu1_OctetList[0] = u1SupportedBmp;
    pRetValDot3OamFunctionsSupported->i4_Length = 1;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDot3OamAdminState
 Input       :  The Indices
                IfIndex

                The Object 
                setValDot3OamAdminState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot3OamAdminState (INT4 i4IfIndex, INT4 i4SetValDot3OamAdminState)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];

    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (((pPortEntry->LocalInfo.u1EoamAdminStatus == EOAM_ENABLED) &&
         (i4SetValDot3OamAdminState == EOAM_SNMP_ENABLED)) ||
        ((pPortEntry->LocalInfo.u1EoamAdminStatus == EOAM_DISABLED) &&
         (i4SetValDot3OamAdminState == EOAM_SNMP_DISABLED)))
    {
        return SNMP_SUCCESS;
    }
    if (i4SetValDot3OamAdminState == EOAM_SNMP_ENABLED)
    {
        if (EoamIfEnable (pPortEntry) != OSIX_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }
    else if (i4SetValDot3OamAdminState == EOAM_SNMP_DISABLED)
    {
        EoamIfDisable (pPortEntry);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot3OamMode
 Input       :  The Indices
                IfIndex

                The Object 
                setValDot3OamMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot3OamMode (INT4 i4IfIndex, INT4 i4SetValDot3OamMode)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4SetValDot3OamMode == pPortEntry->LocalInfo.u1Mode)
    {
        return SNMP_SUCCESS;
    }
    pPortEntry->LocalInfo.u1Mode = (UINT1) i4SetValDot3OamMode;

    if (pPortEntry->LocalInfo.u1EoamAdminStatus == EOAM_ENABLED)
    {
        pPortEntry->LocalInfo.u2ConfigRevision++;
        EoamSemMachine (pPortEntry, EOAM_DISC_EVENT_OPER_DOWN);
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot3OamAdminState
 Input       :  The Indices
                IfIndex

                The Object 
                testValDot3OamAdminState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot3OamAdminState (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                            INT4 i4TestValDot3OamAdminState)
{

    if (EoamSnmpLowValidateIfIndex (i4IfIndex) != OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValDot3OamAdminState != EOAM_SNMP_DISABLED) &&
        (i4TestValDot3OamAdminState != EOAM_SNMP_ENABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot3OamMode
 Input       :  The Indices
                IfIndex

                The Object 
                testValDot3OamMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot3OamMode (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                      INT4 i4TestValDot3OamMode)
{
    if (EoamSnmpLowValidateIfIndex (i4IfIndex) != OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValDot3OamMode != EOAM_MODE_ACTIVE) &&
        (i4TestValDot3OamMode != EOAM_MODE_PASSIVE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot3OamTable
 Input       :  The Indices
                IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot3OamTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot3OamPeerTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot3OamPeerTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot3OamPeerTable (INT4 i4IfIndex)
{
    if (EoamSnmpLowValidateIfIndex (i4IfIndex) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot3OamPeerTable
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot3OamPeerTable (INT4 *pi4IfIndex)
{
    UINT4               u4Index = 0;

/* Get the First Used Index */
    for (u4Index = EOAM_MIN_PORTS; u4Index <= EOAM_MAX_PORTS; u4Index++)
    {
        if (gEoamGlobalInfo.apEoamPortEntry[u4Index - 1] != NULL)
        {
            *pi4IfIndex = (INT4) u4Index;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot3OamPeerTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot3OamPeerTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    UINT4               u4Index = 0;

    if ((i4IfIndex < EOAM_MIN_PORTS) || (i4IfIndex >= EOAM_MAX_PORTS))
    {
        return SNMP_FAILURE;
    }

    /* Next Index should always return the next used Index */
    for (u4Index = i4IfIndex + 1; u4Index <= EOAM_MAX_PORTS; u4Index++)
    {
        if (gEoamGlobalInfo.apEoamPortEntry[u4Index - 1] != NULL)
        {
            *pi4NextIfIndex = (INT4) u4Index;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot3OamPeerMacAddress
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamPeerMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot3OamPeerMacAddress (INT4 i4IfIndex,
                             tMacAddr * pRetValDot3OamPeerMacAddress)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    MEMCPY (pRetValDot3OamPeerMacAddress, pPortEntry->RemoteInfo.MacAddress,
            MAC_ADDR_LEN);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamPeerVendorOui
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamPeerVendorOui
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot3OamPeerVendorOui (INT4 i4IfIndex, tSNMP_OCTET_STRING_TYPE *
                            pRetValDot3OamPeerVendorOui)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    MEMCPY (pRetValDot3OamPeerVendorOui->pu1_OctetList,
            pPortEntry->RemoteInfo.au1VendorOui, EOAM_OUI_LENGTH);
    pRetValDot3OamPeerVendorOui->i4_Length = EOAM_OUI_LENGTH;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamPeerVendorInfo
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamPeerVendorInfo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot3OamPeerVendorInfo (INT4 i4IfIndex,
                             UINT4 *pu4RetValDot3OamPeerVendorInfo)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pu4RetValDot3OamPeerVendorInfo = pPortEntry->RemoteInfo.u4VendorInfo;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamPeerMode
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamPeerMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot3OamPeerMode (INT4 i4IfIndex, INT4 *pi4RetValDot3OamPeerMode)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pi4RetValDot3OamPeerMode = (INT4) pPortEntry->RemoteInfo.u1Mode;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamPeerMaxOamPduSize
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamPeerMaxOamPduSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot3OamPeerMaxOamPduSize (INT4 i4IfIndex,
                                UINT4 *pu4RetValDot3OamPeerMaxOamPduSize)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pu4RetValDot3OamPeerMaxOamPduSize = pPortEntry->RemoteInfo.u2MaxOamPduSize;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamPeerConfigRevision
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamPeerConfigRevision
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot3OamPeerConfigRevision (INT4 i4IfIndex,
                                 UINT4 *pu4RetValDot3OamPeerConfigRevision)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pu4RetValDot3OamPeerConfigRevision =
        pPortEntry->RemoteInfo.u2ConfigRevision;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamPeerFunctionsSupported
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamPeerFunctionsSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot3OamPeerFunctionsSupported (INT4 i4IfIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValDot3OamPeerFunctionsSupported)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    pRetValDot3OamPeerFunctionsSupported->pu1_OctetList[0] =
        pPortEntry->RemoteInfo.u1FnsSupportedBmp;
    pRetValDot3OamPeerFunctionsSupported->i4_Length = 1;
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot3OamLoopbackTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot3OamLoopbackTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot3OamLoopbackTable (INT4 i4IfIndex)
{
    if (EoamSnmpLowValidateIfIndex (i4IfIndex) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot3OamLoopbackTable
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot3OamLoopbackTable (INT4 *pi4IfIndex)
{
    UINT4               u4Index = 0;

/* Get the First Used Index */
    for (u4Index = EOAM_MIN_PORTS; u4Index <= EOAM_MAX_PORTS; u4Index++)
    {
        if (gEoamGlobalInfo.apEoamPortEntry[u4Index - 1] != NULL)
        {
            *pi4IfIndex = (INT4) u4Index;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot3OamLoopbackTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot3OamLoopbackTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    UINT4               u4Index = 0;

    if ((i4IfIndex < EOAM_MIN_PORTS) || (i4IfIndex >= EOAM_MAX_PORTS))
    {
        return SNMP_FAILURE;
    }

    /* Next Index should always return the next used Index */
    for (u4Index = i4IfIndex + 1; u4Index <= EOAM_MAX_PORTS; u4Index++)
    {
        if (gEoamGlobalInfo.apEoamPortEntry[u4Index - 1] != NULL)
        {
            *pi4NextIfIndex = (INT4) u4Index;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot3OamLoopbackStatus
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamLoopbackStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot3OamLoopbackStatus (INT4 i4IfIndex,
                             INT4 *pi4RetValDot3OamLoopbackStatus)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pi4RetValDot3OamLoopbackStatus = (INT4) pPortEntry->LoopbackInfo.u1Status;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamLoopbackIgnoreRx
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamLoopbackIgnoreRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot3OamLoopbackIgnoreRx (INT4 i4IfIndex,
                               INT4 *pi4RetValDot3OamLoopbackIgnoreRx)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pi4RetValDot3OamLoopbackIgnoreRx =
        (INT4) pPortEntry->LoopbackInfo.u1IgnoreRx;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDot3OamLoopbackStatus
 Input       :  The Indices
                IfIndex

                The Object 
                setValDot3OamLoopbackStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot3OamLoopbackStatus (INT4 i4IfIndex, INT4 i4SetValDot3OamLoopbackStatus)
{
    tEoamPortInfo      *pPortInfo = NULL;
    UINT1               u1LBCmd = 0;

    pPortInfo = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    if (pPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    u1LBCmd = ((i4SetValDot3OamLoopbackStatus ==
                EOAM_INITIATING_LOOPBACK) ? EOAM_LB_ENABLE : EOAM_LB_DISABLE);
    if (((pPortInfo->LoopbackInfo.u1Status == EOAM_REMOTE_LOOPBACK) &&
         (u1LBCmd == EOAM_LB_ENABLE)) ||
        ((pPortInfo->LoopbackInfo.u1Status == EOAM_NO_LOOPBACK) &&
         (u1LBCmd == EOAM_LB_DISABLE)))
    {
        return SNMP_SUCCESS;
    }
    if (EoamCltLbCmdSendToPeer (pPortInfo, u1LBCmd) == OSIX_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetDot3OamLoopbackIgnoreRx
 Input       :  The Indices
                IfIndex

                The Object 
                setValDot3OamLoopbackIgnoreRx
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot3OamLoopbackIgnoreRx (INT4 i4IfIndex,
                               INT4 i4SetValDot3OamLoopbackIgnoreRx)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pPortEntry->LoopbackInfo.u1IgnoreRx =
        (UINT1) i4SetValDot3OamLoopbackIgnoreRx;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot3OamLoopbackStatus
 Input       :  The Indices
                IfIndex

                The Object 
                testValDot3OamLoopbackStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot3OamLoopbackStatus (UINT4 *pu4ErrorCode,
                                INT4 i4IfIndex,
                                INT4 i4TestValDot3OamLoopbackStatus)
{
    tEoamPortInfo      *pPortInfo = NULL;

    if (EoamSnmpLowValidateIfIndex (i4IfIndex) != OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    pPortInfo = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];

    if ((i4TestValDot3OamLoopbackStatus != EOAM_INITIATING_LOOPBACK) &&
        (i4TestValDot3OamLoopbackStatus != EOAM_TERMINATING_LOOPBACK))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (i4TestValDot3OamLoopbackStatus == EOAM_INITIATING_LOOPBACK)
    {
        if (pPortInfo->LoopbackInfo.u1Status != EOAM_NO_LOOPBACK)
        {
            /* Configuration has no effect because current state 
             * is not in EOAM_NO_LOOPBACK. */
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    if (i4TestValDot3OamLoopbackStatus == EOAM_TERMINATING_LOOPBACK)
    {
        if (pPortInfo->LoopbackInfo.u1Status != EOAM_REMOTE_LOOPBACK)
        {

            /* Configuration has no effect because current state 
             * is not in EOAM_REMOTE_LOOPBACK. */
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    if ((pPortInfo->LoopbackInfo.u1Status == EOAM_INITIATING_LOOPBACK) ||
        (pPortInfo->LoopbackInfo.u1Status == EOAM_TERMINATING_LOOPBACK) ||
        (pPortInfo->LoopbackInfo.u1Status == EOAM_LOCAL_LOOPBACK))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((pPortInfo->RemoteInfo.u1FnsSupportedBmp & EOAM_LOOPBACK_SUPPORT) == 0)
    {
        CLI_SET_ERR (EOAM_CLI_REMOTE_LB_INCAPABLE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot3OamLoopbackIgnoreRx
 Input       :  The Indices
                IfIndex

                The Object 
                testValDot3OamLoopbackIgnoreRx
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot3OamLoopbackIgnoreRx (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                  INT4 i4TestValDot3OamLoopbackIgnoreRx)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    if (EoamSnmpLowValidateIfIndex (i4IfIndex) != OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValDot3OamLoopbackIgnoreRx != EOAM_IGNORE_LB_CMD) &&
        (i4TestValDot3OamLoopbackIgnoreRx != EOAM_PROCESS_LB_CMD))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (pPortEntry->LoopbackInfo.u1Status == EOAM_LOCAL_LOOPBACK)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (EOAM_CLI_ERR_NODE_IN_LOCAL_LOOPBACK);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot3OamLoopbackTable
 Input       :  The Indices
                IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot3OamLoopbackTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot3OamStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot3OamStatsTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot3OamStatsTable (INT4 i4IfIndex)
{
    if (EoamSnmpLowValidateIfIndex (i4IfIndex) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot3OamStatsTable
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot3OamStatsTable (INT4 *pi4IfIndex)
{
    UINT4               u4Index = 0;

/* Get the First Used Index */
    for (u4Index = EOAM_MIN_PORTS; u4Index <= EOAM_MAX_PORTS; u4Index++)
    {
        if (gEoamGlobalInfo.apEoamPortEntry[u4Index - 1] != NULL)
        {
            *pi4IfIndex = (INT4) u4Index;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot3OamStatsTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot3OamStatsTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    UINT4               u4Index = 0;

    if ((i4IfIndex < EOAM_MIN_PORTS) || (i4IfIndex >= EOAM_MAX_PORTS))
    {
        return SNMP_FAILURE;
    }

    /* Next Index should always return the next used Index */
    for (u4Index = i4IfIndex + 1; u4Index <= EOAM_MAX_PORTS; u4Index++)
    {
        if (gEoamGlobalInfo.apEoamPortEntry[u4Index - 1] != NULL)
        {
            *pi4NextIfIndex = (INT4) u4Index;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot3OamInformationTx
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamInformationTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot3OamInformationTx (INT4 i4IfIndex,
                            UINT4 *pu4RetValDot3OamInformationTx)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pu4RetValDot3OamInformationTx = pPortEntry->PduStats.u4InformationTx;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamInformationRx
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamInformationRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot3OamInformationRx (INT4 i4IfIndex,
                            UINT4 *pu4RetValDot3OamInformationRx)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pu4RetValDot3OamInformationRx = pPortEntry->PduStats.u4InformationRx;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamUniqueEventNotificationTx
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamUniqueEventNotificationTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot3OamUniqueEventNotificationTx (INT4 i4IfIndex,
                                        UINT4
                                        *pu4RetValDot3OamUniqueEventNotificationTx)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pu4RetValDot3OamUniqueEventNotificationTx =
        pPortEntry->PduStats.u4UniqueEventNotificationTx;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamUniqueEventNotificationRx
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamUniqueEventNotificationRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetDot3OamUniqueEventNotificationRx
    (INT4 i4IfIndex, UINT4 *pu4RetValDot3OamUniqueEventNotificationRx)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pu4RetValDot3OamUniqueEventNotificationRx =
        pPortEntry->PduStats.u4UniqueEventNotificationRx;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamDuplicateEventNotificationTx
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamDuplicateEventNotificationTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetDot3OamDuplicateEventNotificationTx
    (INT4 i4IfIndex, UINT4 *pu4RetValDot3OamDuplicateEventNotificationTx)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pu4RetValDot3OamDuplicateEventNotificationTx =
        pPortEntry->PduStats.u4DuplicateEventNotificationTx;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamDuplicateEventNotificationRx
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamDuplicateEventNotificationRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetDot3OamDuplicateEventNotificationRx
    (INT4 i4IfIndex, UINT4 *pu4RetValDot3OamDuplicateEventNotificationRx)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pu4RetValDot3OamDuplicateEventNotificationRx =
        pPortEntry->PduStats.u4DuplicateEventNotificationRx;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamLoopbackControlTx
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamLoopbackControlTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot3OamLoopbackControlTx (INT4 i4IfIndex,
                                UINT4 *pu4RetValDot3OamLoopbackControlTx)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pu4RetValDot3OamLoopbackControlTx =
        pPortEntry->PduStats.u4LoopbackControlTx;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamLoopbackControlRx
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamLoopbackControlRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot3OamLoopbackControlRx (INT4 i4IfIndex,
                                UINT4 *pu4RetValDot3OamLoopbackControlRx)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pu4RetValDot3OamLoopbackControlRx =
        pPortEntry->PduStats.u4LoopbackControlRx;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamVariableRequestTx
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamVariableRequestTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot3OamVariableRequestTx (INT4 i4IfIndex,
                                UINT4 *pu4RetValDot3OamVariableRequestTx)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pu4RetValDot3OamVariableRequestTx =
        pPortEntry->PduStats.u4VariableRequestTx;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamVariableRequestRx
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamVariableRequestRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot3OamVariableRequestRx (INT4 i4IfIndex,
                                UINT4 *pu4RetValDot3OamVariableRequestRx)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pu4RetValDot3OamVariableRequestRx =
        pPortEntry->PduStats.u4VariableRequestRx;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamVariableResponseTx
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamVariableResponseTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot3OamVariableResponseTx (INT4 i4IfIndex,
                                 UINT4 *pu4RetValDot3OamVariableResponseTx)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pu4RetValDot3OamVariableResponseTx =
        pPortEntry->PduStats.u4VariableResponseTx;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamVariableResponseRx
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamVariableResponseRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot3OamVariableResponseRx (INT4 i4IfIndex,
                                 UINT4 *pu4RetValDot3OamVariableResponseRx)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pu4RetValDot3OamVariableResponseRx =
        pPortEntry->PduStats.u4VariableResponseRx;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamOrgSpecificTx
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamOrgSpecificTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot3OamOrgSpecificTx (INT4 i4IfIndex,
                            UINT4 *pu4RetValDot3OamOrgSpecificTx)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pu4RetValDot3OamOrgSpecificTx = pPortEntry->PduStats.u4OrgSpecificTx;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamOrgSpecificRx
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamOrgSpecificRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot3OamOrgSpecificRx (INT4 i4IfIndex,
                            UINT4 *pu4RetValDot3OamOrgSpecificRx)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pu4RetValDot3OamOrgSpecificRx = pPortEntry->PduStats.u4OrgSpecificRx;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamUnsupportedCodesTx
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamUnsupportedCodesTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot3OamUnsupportedCodesTx (INT4 i4IfIndex,
                                 UINT4 *pu4RetValDot3OamUnsupportedCodesTx)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pu4RetValDot3OamUnsupportedCodesTx =
        pPortEntry->PduStats.u4UnsupportedCodesTx;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamUnsupportedCodesRx
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamUnsupportedCodesRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot3OamUnsupportedCodesRx (INT4 i4IfIndex,
                                 UINT4 *pu4RetValDot3OamUnsupportedCodesRx)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pu4RetValDot3OamUnsupportedCodesRx =
        pPortEntry->PduStats.u4UnsupportedCodesRx;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamFramesLostDueToOam
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamFramesLostDueToOam
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot3OamFramesLostDueToOam (INT4 i4IfIndex,
                                 UINT4 *pu4RetValDot3OamFramesLostDueToOam)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pu4RetValDot3OamFramesLostDueToOam =
        pPortEntry->PduStats.u4FramesLostDueToOam;
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot3OamEventConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot3OamEventConfigTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot3OamEventConfigTable (INT4 i4IfIndex)
{
    if (EoamSnmpLowValidateIfIndex (i4IfIndex) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot3OamEventConfigTable
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot3OamEventConfigTable (INT4 *pi4IfIndex)
{
    UINT4               u4Index = 0;

/* Get the First Used Index */
    for (u4Index = EOAM_MIN_PORTS; u4Index <= EOAM_MAX_PORTS; u4Index++)
    {
        if (gEoamGlobalInfo.apEoamPortEntry[u4Index - 1] != NULL)
        {
            *pi4IfIndex = (INT4) u4Index;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot3OamEventConfigTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot3OamEventConfigTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    UINT4               u4Index = 0;

    if ((i4IfIndex < EOAM_MIN_PORTS) || (i4IfIndex >= EOAM_MAX_PORTS))
    {
        return SNMP_FAILURE;
    }

    /* Next Index should always return the next used Index */
    for (u4Index = i4IfIndex + 1; u4Index <= EOAM_MAX_PORTS; u4Index++)
    {
        if (gEoamGlobalInfo.apEoamPortEntry[u4Index - 1] != NULL)
        {
            *pi4NextIfIndex = (INT4) u4Index;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot3OamErrSymPeriodWindowHi
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamErrSymPeriodWindowHi
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetDot3OamErrSymPeriodWindowHi
    (INT4 i4IfIndex, UINT4 *pu4RetValDot3OamErrSymPeriodWindowHi)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pu4RetValDot3OamErrSymPeriodWindowHi =
        FSAP_U8_FETCH_HI (&
                          (pPortEntry->ErrEventConfigInfo.
                           u8ErrSymPeriodWindow));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamErrSymPeriodWindowLo
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamErrSymPeriodWindowLo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetDot3OamErrSymPeriodWindowLo
    (INT4 i4IfIndex, UINT4 *pu4RetValDot3OamErrSymPeriodWindowLo)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pu4RetValDot3OamErrSymPeriodWindowLo =
        FSAP_U8_FETCH_LO (&
                          (pPortEntry->ErrEventConfigInfo.
                           u8ErrSymPeriodWindow));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamErrSymPeriodThresholdHi
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamErrSymPeriodThresholdHi
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetDot3OamErrSymPeriodThresholdHi
    (INT4 i4IfIndex, UINT4 *pu4RetValDot3OamErrSymPeriodThresholdHi)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pu4RetValDot3OamErrSymPeriodThresholdHi =
        FSAP_U8_FETCH_HI (&(pPortEntry->ErrEventConfigInfo.
                            u8ErrSymPeriodThreshold));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamErrSymPeriodThresholdLo
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamErrSymPeriodThresholdLo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetDot3OamErrSymPeriodThresholdLo
    (INT4 i4IfIndex, UINT4 *pu4RetValDot3OamErrSymPeriodThresholdLo)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pu4RetValDot3OamErrSymPeriodThresholdLo =
        FSAP_U8_FETCH_LO (&(pPortEntry->ErrEventConfigInfo.
                            u8ErrSymPeriodThreshold));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamErrSymPeriodEvNotifEnable
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamErrSymPeriodEvNotifEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetDot3OamErrSymPeriodEvNotifEnable
    (INT4 i4IfIndex, INT4 *pi4RetValDot3OamErrSymPeriodEvNotifEnable)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pi4RetValDot3OamErrSymPeriodEvNotifEnable =
        (INT4) pPortEntry->ErrEventConfigInfo.u1ErrSymPeriodEvNotifEnable;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamErrFramePeriodWindow
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamErrFramePeriodWindow
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetDot3OamErrFramePeriodWindow
    (INT4 i4IfIndex, UINT4 *pu4RetValDot3OamErrFramePeriodWindow)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pu4RetValDot3OamErrFramePeriodWindow =
        pPortEntry->ErrEventConfigInfo.u4ErrFramePeriodWindow;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamErrFramePeriodThreshold
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamErrFramePeriodThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetDot3OamErrFramePeriodThreshold
    (INT4 i4IfIndex, UINT4 *pu4RetValDot3OamErrFramePeriodThreshold)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pu4RetValDot3OamErrFramePeriodThreshold =
        pPortEntry->ErrEventConfigInfo.u4ErrFramePeriodThreshold;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamErrFramePeriodEvNotifEnable
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamErrFramePeriodEvNotifEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetDot3OamErrFramePeriodEvNotifEnable
    (INT4 i4IfIndex, INT4 *pi4RetValDot3OamErrFramePeriodEvNotifEnable)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pi4RetValDot3OamErrFramePeriodEvNotifEnable =
        pPortEntry->ErrEventConfigInfo.u1ErrFramePeriodEvNotifEnable;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamErrFrameWindow
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamErrFrameWindow
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot3OamErrFrameWindow (INT4 i4IfIndex,
                             UINT4 *pu4RetValDot3OamErrFrameWindow)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pu4RetValDot3OamErrFrameWindow =
        pPortEntry->ErrEventConfigInfo.u4ErrFrameWindow;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamErrFrameThreshold
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamErrFrameThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot3OamErrFrameThreshold (INT4 i4IfIndex,
                                UINT4 *pu4RetValDot3OamErrFrameThreshold)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pu4RetValDot3OamErrFrameThreshold =
        pPortEntry->ErrEventConfigInfo.u4ErrFrameThreshold;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamErrFrameEvNotifEnable
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamErrFrameEvNotifEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetDot3OamErrFrameEvNotifEnable
    (INT4 i4IfIndex, INT4 *pi4RetValDot3OamErrFrameEvNotifEnable)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pi4RetValDot3OamErrFrameEvNotifEnable =
        (INT4) pPortEntry->ErrEventConfigInfo.u1ErrFrameEvNotifEnable;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamErrFrameSecsSummaryWindow
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamErrFrameSecsSummaryWindow
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetDot3OamErrFrameSecsSummaryWindow
    (INT4 i4IfIndex, INT4 *pi4RetValDot3OamErrFrameSecsSummaryWindow)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pi4RetValDot3OamErrFrameSecsSummaryWindow =
        (INT4) pPortEntry->ErrEventConfigInfo.u4ErrFrameSecsSummaryWindow;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamErrFrameSecsSummaryThreshold
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamErrFrameSecsSummaryThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetDot3OamErrFrameSecsSummaryThreshold
    (INT4 i4IfIndex, INT4 *pi4RetValDot3OamErrFrameSecsSummaryThreshold)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pi4RetValDot3OamErrFrameSecsSummaryThreshold =
        (INT4) pPortEntry->ErrEventConfigInfo.u4ErrFrameSecsSummaryThreshold;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamErrFrameSecsEvNotifEnable
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamErrFrameSecsEvNotifEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetDot3OamErrFrameSecsEvNotifEnable
    (INT4 i4IfIndex, INT4 *pi4RetValDot3OamErrFrameSecsEvNotifEnable)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pi4RetValDot3OamErrFrameSecsEvNotifEnable =
        (INT4) pPortEntry->ErrEventConfigInfo.u1ErrFrameSecsEvNotifEnable;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamDyingGaspEnable
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamDyingGaspEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot3OamDyingGaspEnable (INT4 i4IfIndex,
                              INT4 *pi4RetValDot3OamDyingGaspEnable)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pi4RetValDot3OamDyingGaspEnable =
        (INT4) pPortEntry->ErrEventConfigInfo.u1DyingGaspEnable;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamCriticalEventEnable
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot3OamCriticalEventEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot3OamCriticalEventEnable (INT4 i4IfIndex,
                                  INT4 *pi4RetValDot3OamCriticalEventEnable)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    *pi4RetValDot3OamCriticalEventEnable =
        (INT4) pPortEntry->ErrEventConfigInfo.u1CriticalEventEnable;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDot3OamErrSymPeriodWindowHi
 Input       :  The Indices
                IfIndex

                The Object 
                setValDot3OamErrSymPeriodWindowHi
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot3OamErrSymPeriodWindowHi (INT4 i4IfIndex,
                                   UINT4 u4SetValDot3OamErrSymPeriodWindowHi)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];

    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (FSAP_U8_FETCH_HI
        (&(pPortEntry->ErrEventConfigInfo.u8ErrSymPeriodWindow)) ==
        u4SetValDot3OamErrSymPeriodWindowHi)
    {
        return SNMP_SUCCESS;
    }
    FSAP_U8_ASSIGN_HI (&(pPortEntry->ErrEventConfigInfo.u8ErrSymPeriodWindow),
                       u4SetValDot3OamErrSymPeriodWindowHi);
    EoamLmIfCopyEventParams ((UINT4) i4IfIndex, EOAM_ERRORED_SYMBOL_EVENT,
                             EOAM_ENABLED, pPortEntry->ErrEventConfigInfo.
                             u8ErrSymPeriodWindow, pPortEntry->
                             ErrEventConfigInfo.u8ErrSymPeriodThreshold);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot3OamErrSymPeriodWindowLo
 Input       :  The Indices
                IfIndex

                The Object 
                setValDot3OamErrSymPeriodWindowLo
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot3OamErrSymPeriodWindowLo (INT4 i4IfIndex,
                                   UINT4 u4SetValDot3OamErrSymPeriodWindowLo)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (FSAP_U8_FETCH_LO
        (&(pPortEntry->ErrEventConfigInfo.u8ErrSymPeriodWindow)) ==
        u4SetValDot3OamErrSymPeriodWindowLo)
    {
        return SNMP_SUCCESS;
    }
    FSAP_U8_ASSIGN_LO (&(pPortEntry->ErrEventConfigInfo.u8ErrSymPeriodWindow),
                       u4SetValDot3OamErrSymPeriodWindowLo);
    EoamLmIfCopyEventParams ((UINT4) i4IfIndex, EOAM_ERRORED_SYMBOL_EVENT,
                             EOAM_ENABLED, pPortEntry->ErrEventConfigInfo.
                             u8ErrSymPeriodWindow,
                             pPortEntry->ErrEventConfigInfo.
                             u8ErrSymPeriodThreshold);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot3OamErrSymPeriodThresholdHi
 Input       :  The Indices
                IfIndex

                The Object 
                setValDot3OamErrSymPeriodThresholdHi
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot3OamErrSymPeriodThresholdHi (INT4 i4IfIndex,
                                      UINT4
                                      u4SetValDot3OamErrSymPeriodThresholdHi)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (FSAP_U8_FETCH_HI (&(pPortEntry->ErrEventConfigInfo.
                            u8ErrSymPeriodThreshold)) ==
        u4SetValDot3OamErrSymPeriodThresholdHi)
    {
        return SNMP_SUCCESS;
    }
    FSAP_U8_ASSIGN_HI (&(pPortEntry->ErrEventConfigInfo.
                         u8ErrSymPeriodThreshold),
                       u4SetValDot3OamErrSymPeriodThresholdHi);
    EoamLmIfCopyEventParams ((UINT4) i4IfIndex, EOAM_ERRORED_SYMBOL_EVENT,
                             EOAM_ENABLED,
                             pPortEntry->ErrEventConfigInfo.
                             u8ErrSymPeriodWindow,
                             pPortEntry->ErrEventConfigInfo.
                             u8ErrSymPeriodThreshold);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot3OamErrSymPeriodThresholdLo
 Input       :  The Indices
                IfIndex

                The Object 
                setValDot3OamErrSymPeriodThresholdLo
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot3OamErrSymPeriodThresholdLo (INT4 i4IfIndex,
                                      UINT4
                                      u4SetValDot3OamErrSymPeriodThresholdLo)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (FSAP_U8_FETCH_LO (&(pPortEntry->ErrEventConfigInfo.
                            u8ErrSymPeriodThreshold)) ==
        u4SetValDot3OamErrSymPeriodThresholdLo)
    {
        return SNMP_SUCCESS;
    }
    FSAP_U8_ASSIGN_LO (&(pPortEntry->ErrEventConfigInfo.
                         u8ErrSymPeriodThreshold),
                       u4SetValDot3OamErrSymPeriodThresholdLo);
    EoamLmIfCopyEventParams ((UINT4) i4IfIndex, EOAM_ERRORED_SYMBOL_EVENT,
                             EOAM_ENABLED,
                             pPortEntry->ErrEventConfigInfo.
                             u8ErrSymPeriodWindow,
                             pPortEntry->ErrEventConfigInfo.
                             u8ErrSymPeriodThreshold);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot3OamErrSymPeriodEvNotifEnable
 Input       :  The Indices
                IfIndex

                The Object 
                setValDot3OamErrSymPeriodEvNotifEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot3OamErrSymPeriodEvNotifEnable (INT4 i4IfIndex,
                                        INT4
                                        i4SetValDot3OamErrSymPeriodEvNotifEnable)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPortEntry->ErrEventConfigInfo.u1ErrSymPeriodEvNotifEnable ==
        (UINT1) i4SetValDot3OamErrSymPeriodEvNotifEnable)
    {
        return SNMP_SUCCESS;
    }
    EoamLmIfCopyEventParams ((UINT4) i4IfIndex, EOAM_ERRORED_SYMBOL_EVENT,
                             (UINT1) i4SetValDot3OamErrSymPeriodEvNotifEnable,
                             pPortEntry->ErrEventConfigInfo.
                             u8ErrSymPeriodWindow,
                             pPortEntry->ErrEventConfigInfo.
                             u8ErrSymPeriodThreshold);
    pPortEntry->ErrEventConfigInfo.u1ErrSymPeriodEvNotifEnable =
        (UINT1) i4SetValDot3OamErrSymPeriodEvNotifEnable;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot3OamErrFramePeriodWindow
 Input       :  The Indices
                IfIndex

                The Object 
                setValDot3OamErrFramePeriodWindow
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot3OamErrFramePeriodWindow (INT4 i4IfIndex,
                                   UINT4 u4SetValDot3OamErrFramePeriodWindow)
{
    tEoamPortInfo      *pPortEntry = NULL;
    FS_UINT8            u8Window;
    FS_UINT8            u8Threshold;

    FSAP_U8_CLR (&u8Window);
    FSAP_U8_CLR (&u8Threshold);

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPortEntry->ErrEventConfigInfo.u4ErrFramePeriodWindow ==
        u4SetValDot3OamErrFramePeriodWindow)
    {
        return SNMP_SUCCESS;
    }
    pPortEntry->ErrEventConfigInfo.u4ErrFramePeriodWindow =
        u4SetValDot3OamErrFramePeriodWindow;
    FSAP_U8_ASSIGN_LO (&u8Window, pPortEntry->
                       ErrEventConfigInfo.u4ErrFramePeriodWindow);
    FSAP_U8_ASSIGN_LO (&u8Threshold, pPortEntry->
                       ErrEventConfigInfo.u4ErrFramePeriodThreshold);
    EoamLmIfCopyEventParams ((UINT4) i4IfIndex,
                             EOAM_ERRORED_FRAME_PERIOD_EVENT,
                             EOAM_ENABLED, u8Window, u8Threshold);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot3OamErrFramePeriodThreshold
 Input       :  The Indices
                IfIndex

                The Object 
                setValDot3OamErrFramePeriodThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot3OamErrFramePeriodThreshold (INT4 i4IfIndex,
                                      UINT4
                                      u4SetValDot3OamErrFramePeriodThreshold)
{
    tEoamPortInfo      *pPortEntry = NULL;
    FS_UINT8            u8Window;
    FS_UINT8            u8Threshold;

    FSAP_U8_CLR (&u8Window);
    FSAP_U8_CLR (&u8Threshold);

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPortEntry->ErrEventConfigInfo.u4ErrFramePeriodThreshold ==
        u4SetValDot3OamErrFramePeriodThreshold)
    {
        return SNMP_SUCCESS;
    }
    pPortEntry->ErrEventConfigInfo.u4ErrFramePeriodThreshold =
        u4SetValDot3OamErrFramePeriodThreshold;
    FSAP_U8_ASSIGN_LO (&u8Window,
                       pPortEntry->ErrEventConfigInfo.u4ErrFramePeriodWindow);
    FSAP_U8_ASSIGN_LO (&u8Threshold, pPortEntry->
                       ErrEventConfigInfo.u4ErrFramePeriodThreshold);
    EoamLmIfCopyEventParams ((UINT4) i4IfIndex, EOAM_ERRORED_FRAME_PERIOD_EVENT,
                             EOAM_ENABLED, u8Window, u8Threshold);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot3OamErrFramePeriodEvNotifEnable
 Input       :  The Indices
                IfIndex

                The Object 
                setValDot3OamErrFramePeriodEvNotifEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot3OamErrFramePeriodEvNotifEnable (INT4 i4IfIndex,
                                          INT4
                                          i4SetValDot3OamErrFramePeriodEvNotifEnable)
{
    tEoamPortInfo      *pPortEntry = NULL;
    FS_UINT8            u8Window;
    FS_UINT8            u8Threshold;

    FSAP_U8_CLR (&u8Window);
    FSAP_U8_CLR (&u8Threshold);

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPortEntry->ErrEventConfigInfo.u1ErrFramePeriodEvNotifEnable ==
        (UINT1) i4SetValDot3OamErrFramePeriodEvNotifEnable)
    {
        return SNMP_SUCCESS;
    }
    pPortEntry->ErrEventConfigInfo.u1ErrFramePeriodEvNotifEnable =
        (UINT1) i4SetValDot3OamErrFramePeriodEvNotifEnable;
    FSAP_U8_ASSIGN_LO (&u8Window,
                       pPortEntry->ErrEventConfigInfo.u4ErrFramePeriodWindow);
    FSAP_U8_ASSIGN_LO (&u8Threshold,
                       pPortEntry->ErrEventConfigInfo.
                       u4ErrFramePeriodThreshold);
    EoamLmIfCopyEventParams ((UINT4) i4IfIndex, EOAM_ERRORED_FRAME_PERIOD_EVENT,
                             (UINT1) i4SetValDot3OamErrFramePeriodEvNotifEnable,
                             u8Window, u8Threshold);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot3OamErrFrameWindow
 Input       :  The Indices
                IfIndex

                The Object 
                setValDot3OamErrFrameWindow
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot3OamErrFrameWindow (INT4 i4IfIndex,
                             UINT4 u4SetValDot3OamErrFrameWindow)
{
    tEoamPortInfo      *pPortEntry = NULL;
    FS_UINT8            u8Window;
    FS_UINT8            u8Threshold;

    FSAP_U8_CLR (&u8Window);
    FSAP_U8_CLR (&u8Threshold);

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPortEntry->ErrEventConfigInfo.u4ErrFrameWindow ==
        u4SetValDot3OamErrFrameWindow)
    {
        return SNMP_SUCCESS;
    }
    pPortEntry->ErrEventConfigInfo.u4ErrFrameWindow =
        u4SetValDot3OamErrFrameWindow;
    if (pPortEntry->ErrEventConfigInfo.u1ErrFrameEvNotifEnable == EOAM_TRUE)
    {
        FSAP_U8_ASSIGN_LO (&u8Window, pPortEntry->
                           ErrEventConfigInfo.u4ErrFrameWindow);
        FSAP_U8_ASSIGN_LO (&u8Threshold, pPortEntry->
                           ErrEventConfigInfo.u4ErrFrameThreshold);
        EoamLmIfCopyEventParams ((UINT4) i4IfIndex,
                                 EOAM_ERRORED_FRAME_EVENT,
                                 EOAM_ENABLED, u8Window, u8Threshold);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot3OamErrFrameThreshold
 Input       :  The Indices
                IfIndex

                The Object 
                setValDot3OamErrFrameThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot3OamErrFrameThreshold (INT4 i4IfIndex,
                                UINT4 u4SetValDot3OamErrFrameThreshold)
{
    tEoamPortInfo      *pPortEntry = NULL;
    FS_UINT8            u8Window;
    FS_UINT8            u8Threshold;

    FSAP_U8_CLR (&u8Window);
    FSAP_U8_CLR (&u8Threshold);

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPortEntry->ErrEventConfigInfo.u4ErrFrameThreshold ==
        u4SetValDot3OamErrFrameThreshold)
    {
        return SNMP_SUCCESS;
    }
    pPortEntry->ErrEventConfigInfo.u4ErrFrameThreshold =
        u4SetValDot3OamErrFrameThreshold;
    if (pPortEntry->ErrEventConfigInfo.u1ErrFrameEvNotifEnable == EOAM_TRUE)
    {
        FSAP_U8_ASSIGN_LO (&u8Window, pPortEntry->
                           ErrEventConfigInfo.u4ErrFrameWindow);
        FSAP_U8_ASSIGN_LO (&u8Threshold, pPortEntry->
                           ErrEventConfigInfo.u4ErrFrameThreshold);
        EoamLmIfCopyEventParams ((UINT4) i4IfIndex,
                                 EOAM_ERRORED_FRAME_EVENT,
                                 EOAM_ENABLED, u8Window, u8Threshold);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot3OamErrFrameEvNotifEnable
 Input       :  The Indices
                IfIndex

                The Object 
                setValDot3OamErrFrameEvNotifEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot3OamErrFrameEvNotifEnable (INT4 i4IfIndex,
                                    INT4 i4SetValDot3OamErrFrameEvNotifEnable)
{
    tEoamPortInfo      *pPortEntry = NULL;
    FS_UINT8            u8Window;
    FS_UINT8            u8Threshold;

    FSAP_U8_CLR (&u8Window);
    FSAP_U8_CLR (&u8Threshold);

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPortEntry->ErrEventConfigInfo.u1ErrFrameEvNotifEnable ==
        (UINT1) i4SetValDot3OamErrFrameEvNotifEnable)
    {
        return SNMP_SUCCESS;
    }
    pPortEntry->ErrEventConfigInfo.u1ErrFrameEvNotifEnable =
        (UINT1) i4SetValDot3OamErrFrameEvNotifEnable;
    FSAP_U8_ASSIGN_LO (&u8Window,
                       pPortEntry->ErrEventConfigInfo.u4ErrFrameWindow);
    FSAP_U8_ASSIGN_LO (&u8Threshold,
                       pPortEntry->ErrEventConfigInfo.u4ErrFrameThreshold);
    EoamLmIfCopyEventParams ((UINT4) i4IfIndex, EOAM_ERRORED_FRAME_EVENT,
                             (UINT1) i4SetValDot3OamErrFrameEvNotifEnable,
                             u8Window, u8Threshold);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot3OamErrFrameSecsSummaryWindow
 Input       :  The Indices
                IfIndex

                The Object 
                setValDot3OamErrFrameSecsSummaryWindow
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot3OamErrFrameSecsSummaryWindow (INT4 i4IfIndex,
                                        INT4
                                        i4SetValDot3OamErrFrameSecsSummaryWindow)
{
    tEoamPortInfo      *pPortEntry = NULL;
    FS_UINT8            u8Window;
    FS_UINT8            u8Threshold;

    FSAP_U8_CLR (&u8Window);
    FSAP_U8_CLR (&u8Threshold);

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPortEntry->ErrEventConfigInfo.u4ErrFrameSecsSummaryWindow ==
        (UINT4) i4SetValDot3OamErrFrameSecsSummaryWindow)
    {
        return SNMP_SUCCESS;
    }
    pPortEntry->ErrEventConfigInfo.u4ErrFrameSecsSummaryWindow =
        (UINT4) i4SetValDot3OamErrFrameSecsSummaryWindow;
    if (pPortEntry->ErrEventConfigInfo.u1ErrFrameSecsEvNotifEnable == EOAM_TRUE)
    {
        FSAP_U8_ASSIGN_LO (&u8Window, pPortEntry->
                           ErrEventConfigInfo.u4ErrFrameSecsSummaryWindow);
        FSAP_U8_ASSIGN_LO (&u8Threshold, pPortEntry->
                           ErrEventConfigInfo.u4ErrFrameSecsSummaryThreshold);
        EoamLmIfCopyEventParams ((UINT4) i4IfIndex,
                                 EOAM_ERRORED_FRAME_SECONDS_EVENT,
                                 EOAM_ENABLED, u8Window, u8Threshold);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot3OamErrFrameSecsSummaryThreshold
 Input       :  The Indices
                IfIndex

                The Object 
                setValDot3OamErrFrameSecsSummaryThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot3OamErrFrameSecsSummaryThreshold (INT4 i4IfIndex,
                                           INT4
                                           i4SetValDot3OamErrFrameSecsSummaryThreshold)
{
    tEoamPortInfo      *pPortEntry = NULL;
    FS_UINT8            u8Window;
    FS_UINT8            u8Threshold;

    FSAP_U8_CLR (&u8Window);
    FSAP_U8_CLR (&u8Threshold);

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPortEntry->ErrEventConfigInfo.u4ErrFrameSecsSummaryThreshold ==
        (UINT4) i4SetValDot3OamErrFrameSecsSummaryThreshold)
    {
        return SNMP_SUCCESS;
    }
    pPortEntry->ErrEventConfigInfo.u4ErrFrameSecsSummaryThreshold =
        (UINT4) i4SetValDot3OamErrFrameSecsSummaryThreshold;
    if (pPortEntry->ErrEventConfigInfo.u1ErrFrameSecsEvNotifEnable == EOAM_TRUE)
    {
        FSAP_U8_ASSIGN_LO (&u8Window, pPortEntry->
                           ErrEventConfigInfo.u4ErrFrameSecsSummaryWindow);
        FSAP_U8_ASSIGN_LO (&u8Threshold, pPortEntry->
                           ErrEventConfigInfo.u4ErrFrameSecsSummaryThreshold);
        EoamLmIfCopyEventParams ((UINT4) i4IfIndex,
                                 EOAM_ERRORED_FRAME_SECONDS_EVENT,
                                 EOAM_ENABLED, u8Window, u8Threshold);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot3OamErrFrameSecsEvNotifEnable
 Input       :  The Indices
                IfIndex

                The Object 
                setValDot3OamErrFrameSecsEvNotifEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot3OamErrFrameSecsEvNotifEnable (INT4 i4IfIndex,
                                        INT4
                                        i4SetValDot3OamErrFrameSecsEvNotifEnable)
{
    tEoamPortInfo      *pPortEntry = NULL;
    FS_UINT8            u8Window;
    FS_UINT8            u8Threshold;

    FSAP_U8_CLR (&u8Window);
    FSAP_U8_CLR (&u8Threshold);

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPortEntry->ErrEventConfigInfo.u1ErrFrameSecsEvNotifEnable ==
        (UINT1) i4SetValDot3OamErrFrameSecsEvNotifEnable)
    {
        return SNMP_SUCCESS;
    }
    pPortEntry->ErrEventConfigInfo.u1ErrFrameSecsEvNotifEnable =
        (UINT1) i4SetValDot3OamErrFrameSecsEvNotifEnable;
    FSAP_U8_ASSIGN_LO (&u8Window, pPortEntry->
                       ErrEventConfigInfo.u4ErrFrameSecsSummaryWindow);
    FSAP_U8_ASSIGN_LO (&u8Threshold, pPortEntry->
                       ErrEventConfigInfo.u4ErrFrameSecsSummaryThreshold);
    EoamLmIfCopyEventParams ((UINT4) i4IfIndex,
                             EOAM_ERRORED_FRAME_SECONDS_EVENT,
                             (UINT1) i4SetValDot3OamErrFrameSecsEvNotifEnable,
                             u8Window, u8Threshold);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot3OamDyingGaspEnable
 Input       :  The Indices
                IfIndex

                The Object 
                setValDot3OamDyingGaspEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot3OamDyingGaspEnable (INT4 i4IfIndex,
                              INT4 i4SetValDot3OamDyingGaspEnable)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pPortEntry->ErrEventConfigInfo.u1DyingGaspEnable =
        (UINT1) i4SetValDot3OamDyingGaspEnable;

    if (pPortEntry->pEoamEnaPortInfo != NULL)
    {
        /* Update the Information OAMPDU structure */
        EoamCtlTxConstructInfoPdu (pPortEntry);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot3OamCriticalEventEnable
 Input       :  The Indices
                IfIndex

                The Object 
                setValDot3OamCriticalEventEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot3OamCriticalEventEnable (INT4 i4IfIndex,
                                  INT4 i4SetValDot3OamCriticalEventEnable)
{
    tEoamPortInfo      *pPortEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pPortEntry->ErrEventConfigInfo.u1CriticalEventEnable =
        (UINT1) i4SetValDot3OamCriticalEventEnable;

    if (pPortEntry->pEoamEnaPortInfo != NULL)
    {
        /* Update the Information OAMPDU structure */
        EoamCtlTxConstructInfoPdu (pPortEntry);
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot3OamErrSymPeriodWindowHi
 Input       :  The Indices
                IfIndex

                The Object 
                testValDot3OamErrSymPeriodWindowHi
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot3OamErrSymPeriodWindowHi (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                      UINT4
                                      u4TestValDot3OamErrSymPeriodWindowHi)
{
    FS_UINT8            u8Window;
    tEoamPortInfo      *pPortEntry = NULL;

    FSAP_U8_CLR (&u8Window);

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    if (EoamSnmpLowValidateIfIndex (i4IfIndex) != OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    FSAP_U8_ASSIGN (&u8Window,
                    &(pPortEntry->ErrEventConfigInfo.u8ErrSymPeriodWindow));
    FSAP_U8_ASSIGN_HI (&u8Window, u4TestValDot3OamErrSymPeriodWindowHi);
    /* Threshold value should always be lesser than that of the configured
     * window. */
    if (FSAP_U8_CMP (&pPortEntry->ErrEventConfigInfo.u8ErrSymPeriodThreshold,
                     &u8Window) > 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (EOAM_CLI_ERR_THRESH_EXCEEDS_WINDOW);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot3OamErrSymPeriodWindowLo
 Input       :  The Indices
                IfIndex

                The Object 
                testValDot3OamErrSymPeriodWindowLo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot3OamErrSymPeriodWindowLo (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                      UINT4
                                      u4TestValDot3OamErrSymPeriodWindowLo)
{
    FS_UINT8            u8Window;
    tEoamPortInfo      *pPortEntry = NULL;

    FSAP_U8_CLR (&u8Window);

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    if (EoamSnmpLowValidateIfIndex (i4IfIndex) != OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    FSAP_U8_ASSIGN (&u8Window,
                    &(pPortEntry->ErrEventConfigInfo.u8ErrSymPeriodWindow));
    FSAP_U8_ASSIGN_LO (&u8Window, u4TestValDot3OamErrSymPeriodWindowLo);
    /* Threshold value should always be lesser than that of the configured
     * window. */
    if (FSAP_U8_CMP (&pPortEntry->ErrEventConfigInfo.u8ErrSymPeriodThreshold,
                     &u8Window) >= 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (EOAM_CLI_ERR_THRESH_EXCEEDS_WINDOW);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot3OamErrSymPeriodThresholdHi
 Input       :  The Indices
                IfIndex

                The Object 
                testValDot3OamErrSymPeriodThresholdHi
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2Dot3OamErrSymPeriodThresholdHi
    (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
     UINT4 u4TestValDot3OamErrSymPeriodThresholdHi)
{
    FS_UINT8            u8Threshold;
    tEoamPortInfo      *pPortEntry = NULL;

    FSAP_U8_CLR (&u8Threshold);

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    if (EoamSnmpLowValidateIfIndex (i4IfIndex) != OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    FSAP_U8_ASSIGN (&u8Threshold,
                    &(pPortEntry->ErrEventConfigInfo.u8ErrSymPeriodThreshold));
    FSAP_U8_ASSIGN_HI (&u8Threshold, u4TestValDot3OamErrSymPeriodThresholdHi);
    /* Threshold value should always be lesser than that of the configured
     * window.*/
    if (FSAP_U8_CMP (&u8Threshold,
                     &pPortEntry->ErrEventConfigInfo.u8ErrSymPeriodWindow) > 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (EOAM_CLI_ERR_THRESHOLD_HIGH);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot3OamErrSymPeriodThresholdLo
 Input       :  The Indices
                IfIndex

                The Object 
                testValDot3OamErrSymPeriodThresholdLo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2Dot3OamErrSymPeriodThresholdLo
    (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
     UINT4 u4TestValDot3OamErrSymPeriodThresholdLo)
{
    FS_UINT8            u8Threshold;
    tEoamPortInfo      *pPortEntry = NULL;

    FSAP_U8_CLR (&u8Threshold);

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    if (EoamSnmpLowValidateIfIndex (i4IfIndex) != OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    FSAP_U8_ASSIGN (&u8Threshold,
                    &(pPortEntry->ErrEventConfigInfo.u8ErrSymPeriodThreshold));
    FSAP_U8_ASSIGN_LO (&u8Threshold, u4TestValDot3OamErrSymPeriodThresholdLo);
    /* Threshold value should always be lesser than that of the configured
     * window. Also it should be a non-zero value. */
    if (FSAP_U8_CMP (&u8Threshold,
                     &pPortEntry->ErrEventConfigInfo.u8ErrSymPeriodWindow) >= 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (EOAM_CLI_ERR_THRESHOLD_HIGH);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot3OamErrSymPeriodEvNotifEnable
 Input       :  The Indices
                IfIndex

                The Object 
                testValDot3OamErrSymPeriodEvNotifEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2Dot3OamErrSymPeriodEvNotifEnable
    (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
     INT4 i4TestValDot3OamErrSymPeriodEvNotifEnable)
{
    if (EoamSnmpLowValidateIfIndex (i4IfIndex) != OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValDot3OamErrSymPeriodEvNotifEnable != EOAM_TRUE) &&
        (i4TestValDot3OamErrSymPeriodEvNotifEnable != EOAM_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot3OamErrFramePeriodWindow
 Input       :  The Indices
                IfIndex

                The Object 
                testValDot3OamErrFramePeriodWindow
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2Dot3OamErrFramePeriodWindow
    (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
     UINT4 u4TestValDot3OamErrFramePeriodWindow)
{
    UINT4               u4Threshold = 0;

    if (EoamSnmpLowValidateIfIndex (i4IfIndex) != OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    u4Threshold = (gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1]->
                   ErrEventConfigInfo.u4ErrFramePeriodThreshold);
    /* Threshold value should always be lesser than that of the configured
     * window. */
    if (u4Threshold >= u4TestValDot3OamErrFramePeriodWindow)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (EOAM_CLI_ERR_THRESH_EXCEEDS_WINDOW);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot3OamErrFramePeriodThreshold
 Input       :  The Indices
                IfIndex

                The Object 
                testValDot3OamErrFramePeriodThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2Dot3OamErrFramePeriodThreshold
    (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
     UINT4 u4TestValDot3OamErrFramePeriodThreshold)
{
    UINT4               u4Window = 0;

    if (EoamSnmpLowValidateIfIndex (i4IfIndex) != OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    u4Window = (gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1]->
                ErrEventConfigInfo.u4ErrFramePeriodWindow);
    /* Threshold value should always be lesser than that of the configured
     * window. Also it should be a non-zero value. */
    if (u4TestValDot3OamErrFramePeriodThreshold >= u4Window)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (EOAM_CLI_ERR_THRESHOLD_HIGH);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot3OamErrFramePeriodEvNotifEnable
 Input       :  The Indices
                IfIndex

                The Object 
                testValDot3OamErrFramePeriodEvNotifEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2Dot3OamErrFramePeriodEvNotifEnable
    (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
     INT4 i4TestValDot3OamErrFramePeriodEvNotifEnable)
{
    if (EoamSnmpLowValidateIfIndex (i4IfIndex) != OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValDot3OamErrFramePeriodEvNotifEnable != EOAM_TRUE) &&
        (i4TestValDot3OamErrFramePeriodEvNotifEnable != EOAM_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot3OamErrFrameWindow
 Input       :  The Indices
                IfIndex

                The Object 
                testValDot3OamErrFrameWindow
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot3OamErrFrameWindow (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                UINT4 u4TestValDot3OamErrFrameWindow)
{

    if (EoamSnmpLowValidateIfIndex (i4IfIndex) != OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((u4TestValDot3OamErrFrameWindow < EOAM_FRAME_MIN_WI) ||
        (u4TestValDot3OamErrFrameWindow > EOAM_FRAME_MAX_WI))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (EOAM_CLI_ERR_UNACCEPTABLE_WINDOW_VALUE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot3OamErrFrameThreshold
 Input       :  The Indices
                IfIndex

                The Object 
                testValDot3OamErrFrameThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2Dot3OamErrFrameThreshold
    (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
     UINT4 u4TestValDot3OamErrFrameThreshold)
{
    if (EoamSnmpLowValidateIfIndex (i4IfIndex) != OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (u4TestValDot3OamErrFrameThreshold);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot3OamErrFrameEvNotifEnable
 Input       :  The Indices
                IfIndex

                The Object 
                testValDot3OamErrFrameEvNotifEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2Dot3OamErrFrameEvNotifEnable
    (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
     INT4 i4TestValDot3OamErrFrameEvNotifEnable)
{
    if (EoamSnmpLowValidateIfIndex (i4IfIndex) != OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValDot3OamErrFrameEvNotifEnable != EOAM_TRUE) &&
        (i4TestValDot3OamErrFrameEvNotifEnable != EOAM_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot3OamErrFrameSecsSummaryWindow
 Input       :  The Indices
                IfIndex

                The Object 
                testValDot3OamErrFrameSecsSummaryWindow
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2Dot3OamErrFrameSecsSummaryWindow
    (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
     INT4 i4TestValDot3OamErrFrameSecsSummaryWindow)
{
    UINT4               u4Threshold = 0;

    if (EoamSnmpLowValidateIfIndex (i4IfIndex) != OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValDot3OamErrFrameSecsSummaryWindow
         < EOAM_FRAME_SECS_SUM_MIN_WI) ||
        (i4TestValDot3OamErrFrameSecsSummaryWindow
         > EOAM_FRAME_SECS_SUM_MAX_WI))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    u4Threshold = (gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1]->
                   ErrEventConfigInfo.u4ErrFrameSecsSummaryThreshold);
    /* Threshold value should always be lesser than that of the configured
     * window. */
    if (u4Threshold >= (UINT4) i4TestValDot3OamErrFrameSecsSummaryWindow)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (EOAM_CLI_ERR_THRESH_EXCEEDS_WINDOW);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot3OamErrFrameSecsSummaryThreshold
 Input       :  The Indices
                IfIndex

                The Object 
                testValDot3OamErrFrameSecsSummaryThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2Dot3OamErrFrameSecsSummaryThreshold
    (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
     INT4 i4TestValDot3OamErrFrameSecsSummaryThreshold)
{
    UINT4               u4Window = 0;

    if (EoamSnmpLowValidateIfIndex (i4IfIndex) != OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValDot3OamErrFrameSecsSummaryThreshold
         < EOAM_FRAME_SECS_SUM_MIN_THRESH) ||
        (i4TestValDot3OamErrFrameSecsSummaryThreshold
         > EOAM_FRAME_SECS_SUM_MAX_THRESH))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    u4Window = (gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1]->
                ErrEventConfigInfo.u4ErrFrameSecsSummaryWindow);
    /* Threshold value should always be lesser than that of the configured
     * window. Also it should be a non-zero value. */
    if ((UINT4) i4TestValDot3OamErrFrameSecsSummaryThreshold >= u4Window)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (EOAM_CLI_ERR_THRESHOLD_HIGH);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot3OamErrFrameSecsEvNotifEnable
 Input       :  The Indices
                IfIndex

                The Object 
                testValDot3OamErrFrameSecsEvNotifEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2Dot3OamErrFrameSecsEvNotifEnable
    (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
     INT4 i4TestValDot3OamErrFrameSecsEvNotifEnable)
{
    if (EoamSnmpLowValidateIfIndex (i4IfIndex) != OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValDot3OamErrFrameSecsEvNotifEnable != EOAM_TRUE) &&
        (i4TestValDot3OamErrFrameSecsEvNotifEnable != EOAM_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot3OamDyingGaspEnable
 Input       :  The Indices
                IfIndex

                The Object 
                testValDot3OamDyingGaspEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2Dot3OamDyingGaspEnable
    (UINT4 *pu4ErrorCode, INT4 i4IfIndex, INT4 i4TestValDot3OamDyingGaspEnable)
{
    if (EoamSnmpLowValidateIfIndex (i4IfIndex) != OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValDot3OamDyingGaspEnable != EOAM_TRUE) &&
        (i4TestValDot3OamDyingGaspEnable != EOAM_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot3OamCriticalEventEnable
 Input       :  The Indices
                IfIndex

                The Object 
                testValDot3OamCriticalEventEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2Dot3OamCriticalEventEnable
    (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
     INT4 i4TestValDot3OamCriticalEventEnable)
{
    if (EoamSnmpLowValidateIfIndex (i4IfIndex) != OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValDot3OamCriticalEventEnable != EOAM_TRUE) &&
        (i4TestValDot3OamCriticalEventEnable != EOAM_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot3OamEventConfigTable
 Input       :  The Indices
                IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot3OamEventConfigTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot3OamEventLogTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot3OamEventLogTable
 Input       :  The Indices
                IfIndex
                Dot3OamEventLogIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceDot3OamEventLogTable (INT4 i4IfIndex,
                                              UINT4 u4Dot3OamEventLogIndex)
{
    tEoamPortInfo      *pPortEntry = NULL;

    if (EoamSnmpLowValidateIfIndex (i4IfIndex) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if ((u4Dot3OamEventLogIndex < EOAM_MIN_LOGINDEX) ||
        (u4Dot3OamEventLogIndex > EOAM_MAX_LOG_ENTRIES))
    {
        return SNMP_FAILURE;
    }

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];

    if (pPortEntry->apEventLogEntry[u4Dot3OamEventLogIndex - 1] == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot3OamEventLogTable
 Input       :  The Indices
                IfIndex
                Dot3OamEventLogIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexDot3OamEventLogTable (INT4 *pi4IfIndex,
                                      UINT4 *pu4Dot3OamEventLogIndex)
{
    UINT4               u4Index = 0;
    UINT4               u4LogIndex = 0;
    tEoamPortInfo      *pPortEntry = NULL;

/* Get the First Used Index */
    for (u4Index = EOAM_MIN_PORTS; u4Index <= EOAM_MAX_PORTS; u4Index++)
    {
        pPortEntry = gEoamGlobalInfo.apEoamPortEntry[u4Index - 1];
        if (pPortEntry != NULL)
        {
            *pi4IfIndex = (INT4) u4Index;
            for (u4LogIndex = EOAM_MIN_LOGINDEX;
                 u4LogIndex <= EOAM_MAX_LOG_ENTRIES; u4LogIndex++)
            {
                if (pPortEntry->apEventLogEntry[u4LogIndex - 1] != NULL)
                {
                    *pi4IfIndex = (INT4) u4Index;
                    *pu4Dot3OamEventLogIndex = u4LogIndex;
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot3OamEventLogTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                Dot3OamEventLogIndex
                nextDot3OamEventLogIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexDot3OamEventLogTable
    (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
     UINT4 u4Dot3OamEventLogIndex, UINT4 *pu4NextDot3OamEventLogIndex)
{
    UINT4               u4Index = 0;
    UINT4               u4LogIndex = 0;
    tEoamPortInfo      *pPortEntry = NULL;

    if ((i4IfIndex < EOAM_MIN_PORTS) || (i4IfIndex > EOAM_MAX_PORTS))
    {
        return SNMP_FAILURE;
    }
    if ((u4Dot3OamEventLogIndex < EOAM_MIN_LOGINDEX) ||
        (u4Dot3OamEventLogIndex > EOAM_MAX_LOG_ENTRIES))
    {
        return SNMP_FAILURE;
    }

    /* Next Index should always return the next used Index */
    for (u4Index = i4IfIndex; u4Index <= EOAM_MAX_PORTS; u4Index++)
    {
        pPortEntry = gEoamGlobalInfo.apEoamPortEntry[u4Index - 1];
        if (pPortEntry != NULL)
        {
            *pi4NextIfIndex = (INT4) u4Index;
            for (u4LogIndex = u4Dot3OamEventLogIndex + 1;
                 u4LogIndex <= EOAM_MAX_LOG_ENTRIES; u4LogIndex++)
            {
                if (pPortEntry->apEventLogEntry[u4LogIndex - 1] != NULL)
                {
                    *pi4NextIfIndex = (INT4) u4Index;
                    *pu4NextDot3OamEventLogIndex = u4LogIndex;
                    return SNMP_SUCCESS;
                }
            }
            u4Dot3OamEventLogIndex = 0;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot3OamEventLogTimestamp
 Input       :  The Indices
                IfIndex
                Dot3OamEventLogIndex

                The Object 
                retValDot3OamEventLogTimestamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetDot3OamEventLogTimestamp
    (INT4 i4IfIndex, UINT4 u4Dot3OamEventLogIndex,
     UINT4 *pu4RetValDot3OamEventLogTimestamp)
{
    tEoamPortInfo      *pPortEntry = NULL;
    tEoamEventLogEntry *pLogEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    if ((pLogEntry = pPortEntry->apEventLogEntry[u4Dot3OamEventLogIndex - 1])
        == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValDot3OamEventLogTimestamp =
        pLogEntry->EventLogInfo.u4EventLogTimestamp;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamEventLogOui
 Input       :  The Indices
                IfIndex
                Dot3OamEventLogIndex

                The Object 
                retValDot3OamEventLogOui
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetDot3OamEventLogOui
    (INT4 i4IfIndex, UINT4 u4Dot3OamEventLogIndex,
     tSNMP_OCTET_STRING_TYPE * pRetValDot3OamEventLogOui)
{
    tEoamPortInfo      *pPortEntry = NULL;
    tEoamEventLogEntry *pLogEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    if ((pLogEntry = pPortEntry->apEventLogEntry[u4Dot3OamEventLogIndex - 1])
        == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pRetValDot3OamEventLogOui->pu1_OctetList, pLogEntry->au1EventLogOui,
            EOAM_OUI_LENGTH);
    pRetValDot3OamEventLogOui->i4_Length = EOAM_OUI_LENGTH;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamEventLogType
 Input       :  The Indices
                IfIndex
                Dot3OamEventLogIndex

                The Object 
                retValDot3OamEventLogType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot3OamEventLogType (INT4 i4IfIndex,
                           UINT4 u4Dot3OamEventLogIndex,
                           UINT4 *pu4RetValDot3OamEventLogType)
{
    tEoamPortInfo      *pPortEntry = NULL;
    tEoamEventLogEntry *pLogEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    if ((pLogEntry = pPortEntry->apEventLogEntry[u4Dot3OamEventLogIndex - 1])
        == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValDot3OamEventLogType = pLogEntry->u4EventLogType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamEventLogLocation
 Input       :  The Indices
                IfIndex
                Dot3OamEventLogIndex

                The Object 
                retValDot3OamEventLogLocation
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot3OamEventLogLocation (INT4 i4IfIndex,
                               UINT4 u4Dot3OamEventLogIndex,
                               INT4 *pi4RetValDot3OamEventLogLocation)
{
    tEoamPortInfo      *pPortEntry = NULL;
    tEoamEventLogEntry *pLogEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    if ((pLogEntry = pPortEntry->apEventLogEntry[u4Dot3OamEventLogIndex - 1])
        == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValDot3OamEventLogLocation = (INT4) pLogEntry->u1EventLogLocation;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamEventLogWindowHi
 Input       :  The Indices
                IfIndex
                Dot3OamEventLogIndex

                The Object 
                retValDot3OamEventLogWindowHi
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot3OamEventLogWindowHi (INT4 i4IfIndex,
                               UINT4 u4Dot3OamEventLogIndex,
                               UINT4 *pu4RetValDot3OamEventLogWindowHi)
{
    tEoamPortInfo      *pPortEntry = NULL;
    tEoamEventLogEntry *pLogEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    if ((pLogEntry = pPortEntry->apEventLogEntry[u4Dot3OamEventLogIndex - 1])
        == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValDot3OamEventLogWindowHi =
        FSAP_U8_FETCH_HI (&(pLogEntry->EventLogInfo.u8EventWindow));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamEventLogWindowLo
 Input       :  The Indices
                IfIndex
                Dot3OamEventLogIndex

                The Object 
                retValDot3OamEventLogWindowLo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot3OamEventLogWindowLo (INT4 i4IfIndex,
                               UINT4 u4Dot3OamEventLogIndex,
                               UINT4 *pu4RetValDot3OamEventLogWindowLo)
{
    tEoamPortInfo      *pPortEntry = NULL;
    tEoamEventLogEntry *pLogEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    if ((pLogEntry = pPortEntry->apEventLogEntry[u4Dot3OamEventLogIndex - 1])
        == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValDot3OamEventLogWindowLo =
        FSAP_U8_FETCH_LO (&(pLogEntry->EventLogInfo.u8EventWindow));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamEventLogThresholdHi
 Input       :  The Indices
                IfIndex
                Dot3OamEventLogIndex

                The Object 
                retValDot3OamEventLogThresholdHi
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetDot3OamEventLogThresholdHi
    (INT4 i4IfIndex, UINT4 u4Dot3OamEventLogIndex,
     UINT4 *pu4RetValDot3OamEventLogThresholdHi)
{
    tEoamPortInfo      *pPortEntry = NULL;
    tEoamEventLogEntry *pLogEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    if ((pLogEntry = pPortEntry->apEventLogEntry[u4Dot3OamEventLogIndex - 1])
        == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValDot3OamEventLogThresholdHi =
        FSAP_U8_FETCH_HI (&(pLogEntry->EventLogInfo.u8EventThreshold));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamEventLogThresholdLo
 Input       :  The Indices
                IfIndex
                Dot3OamEventLogIndex

                The Object 
                retValDot3OamEventLogThresholdLo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetDot3OamEventLogThresholdLo
    (INT4 i4IfIndex, UINT4 u4Dot3OamEventLogIndex,
     UINT4 *pu4RetValDot3OamEventLogThresholdLo)
{
    tEoamPortInfo      *pPortEntry = NULL;
    tEoamEventLogEntry *pLogEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    if ((pLogEntry = pPortEntry->apEventLogEntry[u4Dot3OamEventLogIndex - 1])
        == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValDot3OamEventLogThresholdLo =
        FSAP_U8_FETCH_LO (&(pLogEntry->EventLogInfo.u8EventThreshold));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamEventLogValue
 Input       :  The Indices
                IfIndex
                Dot3OamEventLogIndex

                The Object 
                retValDot3OamEventLogValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetDot3OamEventLogValue
    (INT4 i4IfIndex, UINT4 u4Dot3OamEventLogIndex,
     tSNMP_COUNTER64_TYPE * pu8RetValDot3OamEventLogValue)
{
    tEoamPortInfo      *pPortEntry = NULL;
    tEoamEventLogEntry *pLogEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    if ((pLogEntry = pPortEntry->apEventLogEntry[u4Dot3OamEventLogIndex - 1])
        == NULL)
    {
        return SNMP_FAILURE;
    }
    pu8RetValDot3OamEventLogValue->msn =
        FSAP_U8_FETCH_HI (&(pLogEntry->EventLogInfo.u8EventValue));
    pu8RetValDot3OamEventLogValue->lsn =
        FSAP_U8_FETCH_LO (&(pLogEntry->EventLogInfo.u8EventValue));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamEventLogRunningTotal
 Input       :  The Indices
                IfIndex
                Dot3OamEventLogIndex

                The Object 
                retValDot3OamEventLogRunningTotal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetDot3OamEventLogRunningTotal
    (INT4 i4IfIndex, UINT4 u4Dot3OamEventLogIndex,
     tSNMP_COUNTER64_TYPE * pu8RetValDot3OamEventLogRunningTotal)
{
    tEoamPortInfo      *pPortEntry = NULL;
    tEoamEventLogEntry *pLogEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    if ((pLogEntry = pPortEntry->apEventLogEntry[u4Dot3OamEventLogIndex - 1])
        == NULL)
    {
        return SNMP_FAILURE;
    }
    if ((pLogEntry->u4EventLogType == EOAM_ERRORED_FRAME_SECONDS_EVENT) ||
        (pLogEntry->u4EventLogType == EOAM_ERRORED_FRAME_EVENT) ||
        (pLogEntry->u4EventLogType == EOAM_ERRORED_SYMBOL_EVENT) ||
        (pLogEntry->u4EventLogType == EOAM_ERRORED_FRAME_PERIOD_EVENT))
    {
        pu8RetValDot3OamEventLogRunningTotal->msn =
            FSAP_U8_FETCH_HI (&(pLogEntry->EventLogInfo.u8ErrorRunningTotal));
        pu8RetValDot3OamEventLogRunningTotal->lsn =
            FSAP_U8_FETCH_LO (&(pLogEntry->EventLogInfo.u8ErrorRunningTotal));
    }
    else
    {
        pu8RetValDot3OamEventLogRunningTotal->msn = 0;
        pu8RetValDot3OamEventLogRunningTotal->lsn =
            pLogEntry->u4NonThresLogEvtTotal;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot3OamEventLogEventTotal
 Input       :  The Indices
                IfIndex
                Dot3OamEventLogIndex

                The Object 
                retValDot3OamEventLogEventTotal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetDot3OamEventLogEventTotal
    (INT4 i4IfIndex, UINT4 u4Dot3OamEventLogIndex,
     UINT4 *pu4RetValDot3OamEventLogEventTotal)
{
    tEoamPortInfo      *pPortEntry = NULL;
    tEoamEventLogEntry *pLogEntry = NULL;

    pPortEntry = gEoamGlobalInfo.apEoamPortEntry[i4IfIndex - 1];
    if ((pLogEntry = pPortEntry->apEventLogEntry[u4Dot3OamEventLogIndex - 1])
        == NULL)
    {
        return SNMP_FAILURE;
    }
    if ((pLogEntry->u4EventLogType == EOAM_ERRORED_FRAME_SECONDS_EVENT) ||
        (pLogEntry->u4EventLogType == EOAM_ERRORED_FRAME_EVENT) ||
        (pLogEntry->u4EventLogType == EOAM_ERRORED_SYMBOL_EVENT) ||
        (pLogEntry->u4EventLogType == EOAM_ERRORED_FRAME_PERIOD_EVENT))
    {

        *pu4RetValDot3OamEventLogEventTotal =
            pLogEntry->EventLogInfo.u4EventRunningTotal;
    }
    else
    {
        *pu4RetValDot3OamEventLogEventTotal = pLogEntry->u4NonThresLogEvtTotal;
    }
    return SNMP_SUCCESS;
}

/***************************************************************************
 *     FUNCTION NAME    :  EoamSnmpLowValidateIfIndex 
 *                                                                           
 *     DESCRIPTION      :  This function validates the Index for all tables
 *                                                                           
 *     INPUT            :  i4IfIndex - Interface Index 
 *                                                                           
 *     OUTPUT           :  None.                                             
 *                                                                           
 *     RETURNS          :  OSIX_SUCCESS / OSIX_FAILURE                       
 ****************************************************************************/
PRIVATE INT4
EoamSnmpLowValidateIfIndex (INT4 i4IfIndex)
{
    UINT4               u4Index = 0;

    u4Index = (UINT4) i4IfIndex;
    if ((u4Index < EOAM_MIN_PORTS) || (u4Index > EOAM_MAX_PORTS))
    {
        return OSIX_FAILURE;
    }
    if (gEoamGlobalInfo.apEoamPortEntry[u4Index - 1] == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  stdeoalw.c                     */
/*-----------------------------------------------------------------------*/
