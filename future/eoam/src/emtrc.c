/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emtrc.c,v 1.7 2012/05/30 06:23:00 siva Exp $
 *
 * Description: This file contains debugging related functions
 *****************************************************************************/
#include "eminc.h"

/****************************************************************************
 *                                                                           
 * Function     : EoamTrcPrint                                               
 *                                                                           
 * Description  :  prints the trace - with filename and line no              
 *                                                                           
 * Input        : fname   - File name                                        
 *                u4Line  - Line no                                          
 *                s       - string to be printed                             
 *                                                                           
 * Output       : None                                                       
 *                                                                           
 * Returns      : Returns string                                             
 *                                                                           
 *****************************************************************************/
VOID
EoamTrcPrint (const CHR1 * fname, UINT4 u4Line, CHR1 * s)
{

    tOsixSysTime        sysTime;
    const CHR1         *pc1Fname = fname;
    char                ai1LogMsgBuf[EOAM_MAX_LOG_STR_LEN];

    if (s == NULL)
    {
        return;
    }
    while (*fname != '\0')
    {
        if (*fname == '/')
        {
            pc1Fname = (fname + 1);
        }
        fname++;
    }
    OsixGetSysTime (&sysTime);
    SNPRINTF (ai1LogMsgBuf, sizeof (ai1LogMsgBuf), "EOAM: %d:%s:%d:   %s",
              sysTime, pc1Fname, u4Line, s);
    UtlTrcPrint ((const char *) ai1LogMsgBuf);
}

/****************************************************************************
 *                                                                           
 * Function     : EoamTrc                                                    
 *                                                                           
 * Description  : converts variable argument in to string depending on flag  
 *                                                                           
 * Input        : u4Flags  - Trace flag                                      
 *                fmt  - format strong, variable argument
 *                                                                           
 * Output       : None                                                       
 *                                                                           
 * Returns      : Returns string                                             
 *                                                                           
 *****************************************************************************/
CHR1               *
EoamTrc (UINT4 u4Flags, const char *fmt, ...)
{
    va_list             ap;
#define EOAM_TRC_BUF_SIZE    2000
    static CHR1         ac1buf[EOAM_TRC_BUF_SIZE];

    if ((u4Flags & EOAM_TRC_FLAG) == 0)
    {
        return (NULL);
    }
    va_start (ap, fmt);
    vsprintf (&ac1buf[0], fmt, ap);
    va_end (ap);

    return (&ac1buf[0]);
}

/****************************************************************************
 *                                                                           
 * Function     : EoamTrcConcat                                                 
 *                                                                           
 * Description  : Concatenates the given two strings                         
 *                                                                           
 * Input        : srt  - string to be printed                                
 *                func - function name                                       
 *                                                                           
 * Output       : None                                                       
 *                                                                           
 * Returns      : Returns string                                             
 *                                                                           
 *****************************************************************************/
CHR1               *
EoamTrcConcat (const CHR1 * str, const CHR1 * func)
{
    static CHR1         ai1Buf[EOAM_MAX_LOG_STR_LEN];

    SNPRINTF (ai1Buf, sizeof (ai1Buf), "%s %s \n", str, func);

    return (&ai1Buf[0]);
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  emtrc.c                        */
/*-----------------------------------------------------------------------*/
