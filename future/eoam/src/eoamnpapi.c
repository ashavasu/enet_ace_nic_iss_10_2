/*************************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: eoamnpapi.c,v 1.2 2012/12/13 14:36:40 siva Exp $
 *
 * Description: All  Network Processor API Function calls are done here.
 *
 ***************************************************************************/

#ifndef _EOAM_NPAPI_C_
#define _EOAM_NPAPI_C_

#include "eminc.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : EoamEoamLmNpGetStat                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes EoamLmNpGetStat
 *                                                                          
 *    Input(s)            : Arguments of EoamLmNpGetStat
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
EoamEoamLmNpGetStat (UINT4 u4IfIndex, UINT1 u1StatType, UINT4 *pu4Value)
{
    tFsHwNp             FsHwNp;
    tEoamNpModInfo     *pEoamNpModInfo = NULL;
    tEoamNpWrEoamLmNpGetStat *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_EOAM_MOD,    /* Module ID */
                         EOAM_LM_NP_GET_STAT,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pEoamNpModInfo = &(FsHwNp.EoamNpModInfo);
    pEntry = &pEoamNpModInfo->EoamNpEoamLmNpGetStat;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u1StatType = u1StatType;
    pEntry->pu4Value = pu4Value;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : EoamEoamLmNpGetStat64                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes EoamLmNpGetStat64
 *                                                                          
 *    Input(s)            : Arguments of EoamLmNpGetStat64
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
EoamEoamLmNpGetStat64 (UINT4 u4IfIndex, UINT1 u1StatType, FS_UINT8 * pu8Value)
{
    tFsHwNp             FsHwNp;
    tEoamNpModInfo     *pEoamNpModInfo = NULL;
    tEoamNpWrEoamLmNpGetStat64 *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_EOAM_MOD,    /* Module ID */
                         EOAM_LM_NP_GET_STAT64,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pEoamNpModInfo = &(FsHwNp.EoamNpModInfo);
    pEntry = &pEoamNpModInfo->EoamNpEoamLmNpGetStat64;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u1StatType = u1StatType;
    pEntry->pu8Value = pu8Value;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : EoamEoamNpDeInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes EoamNpDeInit
 *                                                                          
 *    Input(s)            : Arguments of EoamNpDeInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
EoamEoamNpDeInit ()
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_EOAM_MOD,    /* Module ID */
                         EOAM_NP_DE_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : EoamEoamNpGetUniDirectionalCapability                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes EoamNpGetUniDirectionalCapability
 *                                                                          
 *    Input(s)            : Arguments of EoamNpGetUniDirectionalCapability
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
EoamEoamNpGetUniDirectionalCapability (UINT4 u4IfIndex, UINT1 *pu1Capability)
{
    tFsHwNp             FsHwNp;
    tEoamNpModInfo     *pEoamNpModInfo = NULL;
    tEoamNpWrEoamNpGetUniDirectionalCapability *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_EOAM_MOD,    /* Module ID */
                         EOAM_NP_GET_UNI_DIRECTIONAL_CAPABILITY,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pEoamNpModInfo = &(FsHwNp.EoamNpModInfo);
    pEntry = &pEoamNpModInfo->EoamNpEoamNpGetUniDirectionalCapability;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pu1Capability = pu1Capability;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : EoamEoamNpHandleLocalLoopBack                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes EoamNpHandleLocalLoopBack
 *                                                                          
 *    Input(s)            : Arguments of EoamNpHandleLocalLoopBack
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
EoamEoamNpHandleLocalLoopBack (UINT4 u4IfIndex, tMacAddr SrcMac,
                               tMacAddr DestMac, UINT1 u1Status)
{
    tFsHwNp             FsHwNp;
    tEoamNpModInfo     *pEoamNpModInfo = NULL;
    tEoamNpWrEoamNpHandleLocalLoopBack *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_EOAM_MOD,    /* Module ID */
                         EOAM_NP_HANDLE_LOCAL_LOOP_BACK,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pEoamNpModInfo = &(FsHwNp.EoamNpModInfo);
    pEntry = &pEoamNpModInfo->EoamNpEoamNpHandleLocalLoopBack;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->SrcMac = SrcMac;
    pEntry->DestMac = DestMac;
    pEntry->u1Status = u1Status;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : EoamEoamNpHandleRemoteLoopBack                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes EoamNpHandleRemoteLoopBack
 *                                                                          
 *    Input(s)            : Arguments of EoamNpHandleRemoteLoopBack
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
EoamEoamNpHandleRemoteLoopBack (UINT4 u4IfIndex, tMacAddr SrcMac,
                                tMacAddr PeerMac, UINT1 u1Status)
{
    tFsHwNp             FsHwNp;
    tEoamNpModInfo     *pEoamNpModInfo = NULL;
    tEoamNpWrEoamNpHandleRemoteLoopBack *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_EOAM_MOD,    /* Module ID */
                         EOAM_NP_HANDLE_REMOTE_LOOP_BACK,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pEoamNpModInfo = &(FsHwNp.EoamNpModInfo);
    pEntry = &pEoamNpModInfo->EoamNpEoamNpHandleRemoteLoopBack;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->SrcMac = SrcMac;
    pEntry->PeerMac = PeerMac;
    pEntry->u1Status = u1Status;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : EoamEoamNpInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes EoamNpInit
 *                                                                          
 *    Input(s)            : Arguments of EoamNpInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
EoamEoamNpInit ()
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_EOAM_MOD,    /* Module ID */
                         EOAM_NP_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : EoamEoamLmNpRegisterLinkMonitor                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes EoamLmNpRegisterLinkMonitor
 *                                                                          
 *    Input(s)            : Arguments of EoamLmNpRegisterLinkMonitor
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
EoamEoamLmNpRegisterLinkMonitor ()
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_EOAM_MOD,    /* Module ID */
                         EOAM_LM_NP_REGISTER_LINK_MONITOR,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : EoamEoamLmNpConfigureParams                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes EoamLmNpConfigureParams
 *                                                                          
 *    Input(s)            : Arguments of EoamLmNpConfigureParams
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
EoamEoamLmNpConfigureParams (UINT4 u4IfIndex, UINT2 u2Event, FS_UINT8 Window,
                             FS_UINT8 Threshold)
{
    tFsHwNp             FsHwNp;
    tEoamNpModInfo     *pEoamNpModInfo = NULL;
    tEoamNpWrEoamLmNpConfigureParams *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_EOAM_MOD,    /* Module ID */
                         EOAM_LM_NP_CONFIGURE_PARAMS,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pEoamNpModInfo = &(FsHwNp.EoamNpModInfo);
    pEntry = &pEoamNpModInfo->EoamNpEoamLmNpConfigureParams;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u2Event = u2Event;
    pEntry->Window = Window;
    pEntry->Threshold = Threshold;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

#ifdef MBSM_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : EoamEoamMbsmHwInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes EoamMbsmHwInit
 *                                                                          
 *    Input(s)            : Arguments of EoamMbsmHwInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
EoamEoamMbsmHwInit (tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tEoamNpModInfo     *pEoamNpModInfo = NULL;
    tEoamNpWrEoamMbsmHwInit *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_EOAM_MOD,    /* Module ID */
                         EOAM_MBSM_HW_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pEoamNpModInfo = &(FsHwNp.EoamNpModInfo);
    pEntry = &pEoamNpModInfo->EoamNpEoamMbsmHwInit;

    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
#endif /* MBSM_WANTED */
#endif /* _EOAM_NPAPI_C_ */
