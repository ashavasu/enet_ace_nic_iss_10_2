/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emcli.c,v 1.25 2017/08/31 14:02:00 siva Exp $
 *
 * Description: This file contains CLI SET/GET/TEST and GETNEXT
 *              routines for the MIB objects specified in stdeoam.mib
 *              and fseoamex.mib
 *********************************************************************/

#ifndef EMCLI_C
#define EMCLI_C

#include "eminc.h"
#include "emcli.h"
#include "fseoamcli.h"
#include "stdeoacli.h"
#include "emlmglob.h"

/*************************************************************************
 * 
 *  FUNCTION NAME   : cli_process_eoam_cmd 
 *  
 *  DESCRIPTION     : Protocol CLI message handler function               
 *                                                                        
 *  INPUT           : CliHandle - CliContext ID                           
 *                    u4Command - Command identifier                      
 *                    ... -Variable command argument list                 
 *                                                                        
 *  OUTPUT          : None                                                
 *                                                                        
 *  RETURNS         : CLI_SUCCESS/CLI_FAILURE                             
 *
 **************************************************************************/
PUBLIC INT4
cli_process_eoam_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *apu4args[EOAM_CLI_MAX_ARGS];
    UINT1               au1OuiVal[EOAM_OUI_LENGTH];
    INT1                i1argno = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4Inst = 0;
    INT4                i4Status = 0;
    INT4                i4IfIndex = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
    FS_UINT8            u8Val;
    FS_UINT8            u8MaxVal;
    FS_UINT8            u8Mill;
    tMacAddr            MacAddr;
    tSNMP_OCTET_STRING_TYPE LocalClientOui;

    FSAP_U8_CLR (&u8Val);
    FSAP_U8_CLR (&u8Mill);
    FSAP_U8_CLR (&u8MaxVal);

    MEMSET (au1OuiVal, 0, EOAM_OUI_LENGTH);
    MEMSET (MacAddr, 0, MAC_ADDR_LEN);
    MEMSET (apu4args, 0, EOAM_CLI_MAX_ARGS);
    MEMSET (&LocalClientOui, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    LocalClientOui.i4_Length = EOAM_OUI_LENGTH;

    LocalClientOui.pu1_OctetList = &au1OuiVal[0];

    CliRegisterLock (CliHandle, EoamLock, EoamUnLock);
    EoamLock ();
    nmhGetFsEoamSystemControl (&i4Status);

    if ((u4Command != EOAM_CLI_SYS_CTRL) && (i4Status != EOAM_START))
    {
        CliPrintf (CliHandle, "\r%% Ethernet OAM module is shutdown\r\n");
        EoamUnLock ();
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }

    va_start (ap, u4Command);

    /* third argument is always interface name/index */
    i4Inst = va_arg (ap, INT4);

    /* Walk through the rest of the arguments and store in apu4args array.
     * Store EOAM_CLI_MAX_ARGS arguments at the max
     * has value */

    while (1)
    {
        apu4args[i1argno++] = va_arg (ap, UINT4 *);

        if (i1argno == EOAM_CLI_MAX_ARGS)
        {
            break;
        }

    }
    va_end (ap);

    switch (u4Command)
    {
        case EOAM_CLI_SYS_CTRL:

            /* apu4args[0] contains start/shutdown status */
            i4RetStatus = EoamCliSetSystemCtrl (CliHandle,
                                                CLI_PTR_TO_I4 (apu4args[0]));
            break;

        case EOAM_CLI_MOD_STAT:

            /* apu4args[0] contains enable/disable status */
            i4RetStatus = EoamCliSetModuleStatus (CliHandle,
                                                  CLI_PTR_TO_I4 (apu4args[0]));
            break;

        case EOAM_CLI_OUI:

            /* apu4args[0] contains oui value */
            CLI_CONVERT_DOT_STR_TO_ARRAY ((UINT1 *) apu4args[0],
                                          LocalClientOui.pu1_OctetList);
            i4RetStatus = EoamCliConfOui (CliHandle, &LocalClientOui);
            break;

        case EOAM_CLI_NO_OUI:

            EoamGetSysMacAddress (MacAddr);
            MEMCPY (LocalClientOui.pu1_OctetList, &MacAddr, EOAM_OUI_LENGTH);
            i4RetStatus = EoamCliConfOui (CliHandle, &LocalClientOui);
            break;

        case EOAM_CLI_EVENT_RESEND:

            /* apu4args[0] contains Error Resend Count value */
            i4RetStatus = EoamCliSetErrorResendCount (CliHandle,
                                                      *(apu4args[0]));
            break;

        case EOAM_CLI_NO_EVENT_RESEND:

            i4RetStatus = EoamCliSetErrorResendCount
                (CliHandle, EOAM_ERR_EVT_RESEND_COUNT);
            break;

        case EOAM_CLI_DEBUG_SHOW:

            EoamCliShowDebugging (CliHandle);
            break;

        case EOAM_CLI_DEBUG:

            /* apu4args[0] contains Debug level */
            i4RetStatus =
                EoamCliEnableDebug (CliHandle, CLI_PTR_TO_I4 (apu4args[0]));
            break;

        case EOAM_CLI_NO_DEBUG:

            i4RetStatus =
                EoamCliDisableDebug (CliHandle, CLI_PTR_TO_I4 (apu4args[0]));
            break;

        case EOAM_CLI_CLEAR_STAT:

            i4RetStatus = EoamCliClearStats (CliHandle, i4Inst);
            break;

        case EOAM_CLI_CLEAR_LOCAL:

            i4RetStatus = EoamCliClearLocal (CliHandle, i4Inst);
            break;

        case EOAM_CLI_CLEAR_EVENT_LOG:

            i4RetStatus = EoamCliClearEventLog (CliHandle, i4Inst);
            break;

        case EOAM_CLI_SHOW_GLOBAL:

            i4RetStatus = EoamCliShowGlobal (CliHandle);
            break;

        case EOAM_CLI_SHOW_PORT_LOCAL:

            i4RetStatus = EoamCliShowLocalInfo (CliHandle, i4Inst);
            break;

        case EOAM_CLI_SHOW_PORT_NEIGH:

            i4RetStatus = EoamCliShowPeerInfo (CliHandle, i4Inst);
            break;

        case EOAM_CLI_SHOW_LB_CPB:

            i4RetStatus = EoamCliShowLBCpb (CliHandle, i4Inst);
            break;

        case EOAM_CLI_SHOW_EVT_NOTIF:

            i4RetStatus = EoamCliShowEvtNotif (CliHandle, i4Inst);
            break;

        case EOAM_CLI_SHOW_PORT_STAT:

            i4RetStatus = EoamCliShowStats (CliHandle, i4Inst);
            break;

        case EOAM_CLI_SHOW_PORT_EVENT:

            i4RetStatus = EoamCliShowEventLog (CliHandle, i4Inst);
            break;

        case EOAM_CLI_PORT:

            /* apu4args[0] contains enable/disable port admin status */
            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            i4RetStatus = EoamCliSetPortAdminStatus (CliHandle, i4IfIndex,
                                                     CLI_PTR_TO_I4 (apu4args
                                                                    [0]));
            break;

        case EOAM_CLI_MODE:

            /* apu4args[0] contains active/passive port mode */
            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            i4RetStatus = EoamCliSetPortMode (CliHandle, i4IfIndex,
                                              CLI_PTR_TO_I4 (apu4args[0]));
            break;

        case EOAM_CLI_REMOTE_LB_CMD:

            /* apu4args[0] contains deny/permit port local loopback */
            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            i4RetStatus = EoamCliSetPortRemoteLBCmd (CliHandle,
                                                     i4IfIndex,
                                                     CLI_PTR_TO_I4 (apu4args
                                                                    [0]));
            break;

        case EOAM_CLI_REMOTE_LB:

            /* apu4args[0] contains enable/disable port remote loopback */
            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            i4RetStatus = EoamCliSetPortRemoteLB (CliHandle,
                                                  i4IfIndex,
                                                  CLI_PTR_TO_I4 (apu4args[0]));
            break;

        case EOAM_CLI_LM_SYMBOL:

            /* apu4args[0] contains enable/disable symbol period notification */
            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            i4RetStatus = EoamCliSetSymbolPeriodNotif (CliHandle,
                                                       i4IfIndex,
                                                       CLI_PTR_TO_I4 (apu4args
                                                                      [0]));
            break;

        case EOAM_CLI_LM_FRAME:

            /* apu4args[0] contains enable/disable frame event notification */
            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            i4RetStatus = EoamCliSetFrameNotif (CliHandle, i4IfIndex,
                                                CLI_PTR_TO_I4 (apu4args[0]));
            break;

        case EOAM_CLI_LM_FRAME_PERIOD:

            /* apu4args[0] contains enable/disable frame 
             * period event notification */
            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            i4RetStatus = EoamCliSetFramePeriodNotif (CliHandle, i4IfIndex,
                                                      CLI_PTR_TO_I4 (apu4args
                                                                     [0]));
            break;

        case EOAM_CLI_LM_FRAME_SEC_SUM:

            /* apu4args[0] contains enable/disable frame 
             * seconds summary event notification */
            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            i4RetStatus = EoamCliSetFrameSecSummaryNotif (CliHandle, i4IfIndex,
                                                          CLI_PTR_TO_I4
                                                          (apu4args[0]));
            break;

        case EOAM_CLI_LM:

            /* apu4args[0] contains enable/disable frame 
             * seconds summary event notification */
            i4IfIndex = (INT4) CLI_GET_IFINDEX ();

            i4RetStatus |= EoamCliSetSymbolPeriodNotif (CliHandle, i4IfIndex,
                                                        CLI_PTR_TO_I4 (apu4args
                                                                       [0]));
            i4RetStatus |=
                EoamCliSetFrameNotif (CliHandle, i4IfIndex,
                                      CLI_PTR_TO_I4 (apu4args[0]));
            i4RetStatus |=
                EoamCliSetFramePeriodNotif (CliHandle, i4IfIndex,
                                            CLI_PTR_TO_I4 (apu4args[0]));
            i4RetStatus |=
                EoamCliSetFrameSecSummaryNotif (CliHandle, i4IfIndex,
                                                CLI_PTR_TO_I4 (apu4args[0]));
            break;

        case EOAM_CLI_LM_SYMBOL_WINDOW:

            /* apu4args[0] contains window size for symbol 
             * period */
            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            FSAP_STR2_U8 ((CHR1 *) apu4args[0], &u8Val);
            EOAM_CLI_MAX_SYM_PERIOD_WINDOW (&u8MaxVal);
            if (FSAP_U8_CMP (&u8Val, &u8MaxVal) > 0)
            {
                CliPrintf (CliHandle, "\r%% Value should be in millions(upto");
                CliPrintf (CliHandle, " 18446744073709/0x10c6f7a0b5ed)\r\n");
                EoamUnLock ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
            }
            EOAM_CLI_CONV_MILLIONS_TO_U8 (&u8Val, &u8Mill);
            i4RetStatus = EoamCliSetSymbolPeriodWindow (CliHandle, i4IfIndex,
                                                        u8Val);
            break;

        case EOAM_CLI_LM_FRAME_WINDOW:

            /* apu4args[0] contains window size for frame */
            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            i4RetStatus = EoamCliSetFrameWindow (CliHandle, i4IfIndex,
                                                 *(apu4args[0]));
            break;

        case EOAM_CLI_LM_FRAME_PERIOD_WINDOW:

            /* apu4args[0] contains window size for 
             * frame period */
            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            i4RetStatus = EoamCliSetFramePeriodWindow (CliHandle, i4IfIndex,
                                                       *(apu4args[0]));
            break;

        case EOAM_CLI_LM_FRAME_SEC_SUM_WINDOW:

            /* apu4args[0] contains window size for frame 
             * seconds summary */
            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            i4RetStatus = EoamCliSetFrameSecSummaryWindow (CliHandle,
                                                           i4IfIndex,
                                                           *(apu4args[0]));
            break;

        case EOAM_CLI_LM_SYMBOL_THRES:

            /* apu4args[0] contains threshold value for symbol 
             * period */
            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            FSAP_STR2_U8 ((CHR1 *) apu4args[0], &u8Val);
            i4RetStatus = EoamCliSetSymbolPeriodThres (CliHandle, i4IfIndex,
                                                       u8Val);

            break;

        case EOAM_CLI_LM_FRAME_THRES:

            /* apu4args[0] contains threshold value for frame */
            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            i4RetStatus = EoamCliSetFrameThres (CliHandle,
                                                i4IfIndex, *(apu4args[0]));
            break;

        case EOAM_CLI_LM_FRAME_PERIOD_THRES:

            /* apu4args[0] contains threshold value for frame 
             * period */
            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            i4RetStatus = EoamCliSetFramePeriodThres (CliHandle,
                                                      i4IfIndex,
                                                      *(apu4args[0]));

            break;

        case EOAM_CLI_LM_FRAME_SEC_SUM_THRES:

            /* apu4args[0] contains threshold value for frame 
             * seconds summary */
            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            i4RetStatus = EoamCliSetFrameSecSummaryThres (CliHandle,
                                                          i4IfIndex,
                                                          *(apu4args[0]));
            break;

        case EOAM_CLI_NO_LM_SYMBOL_THRES:

            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            FSAP_U8_ASSIGN_HI (&u8Val, EOAM_DEF_SYMBOL_PERIOD_THRESH_HI);
            FSAP_U8_ASSIGN_LO (&u8Val, EOAM_DEF_SYMBOL_PERIOD_THRESH_LO);
            i4RetStatus = EoamCliSetSymbolPeriodThres (CliHandle,
                                                       i4IfIndex, u8Val);
            break;

        case EOAM_CLI_NO_LM_FRAME_THRES:

            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            i4RetStatus = EoamCliSetFrameThres (CliHandle, i4IfIndex,
                                                EOAM_DEF_FRAME_THRESH);
            break;

        case EOAM_CLI_NO_LM_FRAME_PERIOD_THRES:

            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            i4RetStatus = EoamCliSetFramePeriodThres
                (CliHandle, i4IfIndex, EOAM_DEF_FRAME_PERIOD_THRESH);

            break;

        case EOAM_CLI_NO_LM_FRAME_SEC_SUM_THRES:

            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            i4RetStatus = EoamCliSetFrameSecSummaryThres
                (CliHandle, i4IfIndex, EOAM_DEF_FRAME_SECS_THRESH);
            break;

        case EOAM_CLI_NO_LM_SYMBOL_WINDOW:

            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            FSAP_U8_ASSIGN_HI (&u8Val, EOAM_DEF_SYMBOL_PERIOD_WINDOW_HI);
            FSAP_U8_ASSIGN_LO (&u8Val, EOAM_DEF_SYMBOL_PERIOD_WINDOW_LO);
            i4RetStatus = EoamCliSetSymbolPeriodWindow (CliHandle,
                                                        i4IfIndex, u8Val);
            break;

        case EOAM_CLI_NO_LM_FRAME_WINDOW:

            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            i4RetStatus = EoamCliSetFrameWindow (CliHandle, i4IfIndex,
                                                 EOAM_DEF_FRAME_WINDOW);
            break;

        case EOAM_CLI_NO_LM_FRAME_PERIOD_WINDOW:

            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            i4RetStatus = EoamCliSetFramePeriodWindow
                (CliHandle, i4IfIndex, EOAM_DEF_FRAME_PERIOD_WINDOW);
            break;

        case EOAM_CLI_NO_LM_FRAME_SEC_SUM_WINDOW:

            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            i4RetStatus = EoamCliSetFrameSecSummaryWindow
                (CliHandle, i4IfIndex, EOAM_DEF_FRAME_SECS_WINDOW);
            break;

        case EOAM_CLI_CRITICAL_EVENT:

            /* apu4args[0] contains enable/disable critical */
            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            i4RetStatus = EoamCliSetCriticalEvent (CliHandle,
                                                   i4IfIndex,
                                                   CLI_PTR_TO_I4 (apu4args[0]));
            break;

        case EOAM_CLI_DYING_GASP:

            /* apu4args[0] contains enable/disable dying gasp */
            i4IfIndex = (INT4) CLI_GET_IFINDEX ();
            i4RetStatus = EoamCliSetDyingGasp (CliHandle,
                                               i4IfIndex,
                                               CLI_PTR_TO_I4 (apu4args[0]));
            break;
        case EOAM_CLI_LM_STATUS:

            /* apu4args[0] contains enable/disable status */
            i4RetStatus = EoamCliSetLMStatus (CliHandle,
                                              CLI_PTR_TO_I4 (apu4args[0]));
            if ((i4RetStatus == CLI_FAILURE) &&
                (gu1EoamLMStatFlag == EOAM_LM_ENABLED))
            {
                CliPrintf (CliHandle,
                           "\r%% Link Monitoring is already enabled \r\n");
            }
            if ((i4RetStatus == CLI_FAILURE) &&
                (gu1EoamLMStatFlag == EOAM_LM_DISABLED))
            {
                CliPrintf (CliHandle,
                           "\r%% Link Monitoring is already disabled \r\n");
            }
            break;
        default:

            CliPrintf (CliHandle, "\r%% Unknown command \r\n");
            EoamUnLock ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {

        if ((u4ErrCode > 0) && (u4ErrCode < EOAM_CLI_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%% %s", gapc1EoamCliErrString[u4ErrCode]);
        }

        CLI_SET_ERR (0);
    }

    EoamUnLock ();
    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

/****************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliSetSystemCtrl                                 
 *                                                                        
 *     DESCRIPTION      : This function will start/shut Eoam module         
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4Status - Eoam System Control                    
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 *****************************************************************************/
PUBLIC INT4
EoamCliSetSystemCtrl (tCliHandle CliHandle, INT4 i4Status)
{

    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsEoamSystemControl (&u4ErrorCode, i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsEoamSystemControl (i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/****************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliSetModuleStatus                               
 *                                                                          
 *     DESCRIPTION      : This function will enable/disable Eoam module     
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4Status   - Eoam Module status                     
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 *****************************************************************************/
PUBLIC INT4
EoamCliSetModuleStatus (tCliHandle CliHandle, INT4 i4Status)
{

    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsEoamModuleStatus (&u4ErrorCode, i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsEoamModuleStatus (i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliConfOui                                       
 *                                                                          
 *     DESCRIPTION      : This function will set the OUI     
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        pLocalClientOui - OUI                
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamCliConfOui (tCliHandle CliHandle, tSNMP_OCTET_STRING_TYPE * pLocalClientOui)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsEoamOui (&u4ErrorCode, pLocalClientOui) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsEoamOui (pLocalClientOui) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set ethernet-oam OUI\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/****************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliSetErrorResendCount                           
 *                                                                          
 *     DESCRIPTION      : This function will set the error resend count     
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        u4Count - Eoam Error Resend Count                
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 *************************************************************************/
PUBLIC INT4
EoamCliSetErrorResendCount (tCliHandle CliHandle, UINT4 u4Count)
{

    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsEoamErrorEventResend (&u4ErrorCode, u4Count) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsEoamErrorEventResend (u4Count) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to set ethernet-oam ErrorResendCount\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliEnableDebug                                   
 *                                                                          
 *     DESCRIPTION      : This function will set the debug level            
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4Val - Eoam Debug Level                       
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ***************************************************************************/
PUBLIC INT4
EoamCliEnableDebug (tCliHandle CliHandle, INT4 i4Val)
{

    UINT4               u4ErrorCode = 0;
    INT4                i4Level = 0;

    nmhGetFsEoamTraceOption (&i4Level);

    i4Val = i4Val | i4Level;

    if (nmhTestv2FsEoamTraceOption (&u4ErrorCode, i4Val) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsEoamTraceOption (i4Val) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to enable debug option\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliDisableDebug                                  
 *                                                                          
 *     DESCRIPTION      : This function will reset the debug level          
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4Val - Eoam Debug Level                       
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ***************************************************************************/
PUBLIC INT4
EoamCliDisableDebug (tCliHandle CliHandle, INT4 i4Val)
{

    UINT4               u4ErrorCode = 0;
    INT4                i4Level = 0;

    nmhGetFsEoamTraceOption (&i4Level);

    i4Val = i4Val & i4Level;

    if (nmhTestv2FsEoamTraceOption (&u4ErrorCode, i4Val) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsEoamTraceOption (i4Val) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to disable debug option\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliShowGlobal                                    
 *                                                                          
 *     DESCRIPTION      : This function will display the EOAM global 
 *                        information           
 *                                                                          
 *     INPUT            : CliHandle - CliContext ID                          
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ***************************************************************************/
PUBLIC INT4
EoamCliShowGlobal (tCliHandle CliHandle)
{
    UINT1               au1RetOuiVal[EOAM_OUI_LENGTH];
    UINT1               au1OuiVal[EOAM_CLI_OUI_STR_LEN];
    UINT4               u4ResendCnt = 0;
    INT4                i4ModStat = 0;
    tSNMP_OCTET_STRING_TYPE LocalClientOui;

    MEMSET (au1OuiVal, 0, EOAM_CLI_OUI_STR_LEN);
    MEMSET (au1RetOuiVal, 0, EOAM_OUI_LENGTH);
    LocalClientOui.i4_Length = EOAM_OUI_LENGTH;

    LocalClientOui.pu1_OctetList = au1RetOuiVal;

    nmhGetFsEoamModuleStatus (&i4ModStat);
    nmhGetFsEoamOui (&LocalClientOui);
    nmhGetFsEoamErrorEventResend (&u4ResendCnt);

    if (i4ModStat == EOAM_DISABLED)
    {
        CliPrintf (CliHandle, "\r\nEthernet OAM has been disabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r\nEthernet OAM has been enabled\r\n");
    }

    /* Convert the OUI to printable format like 00:01:02 */
    CliOctetToStr (LocalClientOui.pu1_OctetList,
                   LocalClientOui.i4_Length, au1OuiVal, EOAM_CLI_OUI_STR_LEN);
    CliPrintf (CliHandle, "OUI configured is %s\r\n", au1OuiVal);
    CliPrintf (CliHandle,
               "Error event OAMPDU resend count:%u\r\n\r\n", u4ResendCnt);

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliShowLocalInfo                                
 *                                                                          
 *     DESCRIPTION      : This function will display the EOAM local 
 *                        information            
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number                       
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ***************************************************************************/
PUBLIC INT4
EoamCliShowLocalInfo (tCliHandle CliHandle, INT4 i4IfIndex)
{
    UINT1               u1IsShowAll = OSIX_TRUE;

    if (i4IfIndex == 0)
    {
        /*Show for all the ports */
        if (nmhGetFirstIndexDot3OamTable (&i4IfIndex) != SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }

    }
    else
    {
        /*Show for a specific port */
        if (nmhValidateIndexInstanceDot3OamTable (i4IfIndex) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        u1IsShowAll = OSIX_FALSE;
    }

    EoamCliShowPortInfo (CliHandle, i4IfIndex, u1IsShowAll);
    EoamCliShowLocFuncSupp (CliHandle, i4IfIndex, u1IsShowAll);
    EoamCliShowSymPeriod (CliHandle, i4IfIndex, u1IsShowAll);
    EoamCliShowFramePeriod (CliHandle, i4IfIndex, u1IsShowAll);
    EoamCliShowFrame (CliHandle, i4IfIndex, u1IsShowAll);
    EoamCliShowFrameSecSumm (CliHandle, i4IfIndex, u1IsShowAll);

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliShowPortInfo                                
 *                                                                          
 *     DESCRIPTION      : This function will display the EOAM local 
 *                        information from dot3OamTable           
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number     
 *                        u1IsShowAll - Flag to specify display for a
 *                        single port or all the ports
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ***************************************************************************/
PUBLIC INT4
EoamCliShowPortInfo (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1IsShowAll)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *pi1IfName = NULL;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4RetVal = 0;
    INT4                i4PrevIfIndex = 0;
    INT4                i4RetVal = 0;
    INT4                i4Symbol = 0;
    INT4                i4Frame = 0;
    INT4                i4FramePeriod = 0;
    INT4                i4FrameSecs = 0;

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    pi1IfName = (INT1 *) au1IfName;

    CliPrintf (CliHandle,
               "\r\n%-7s %-8s %-7s %-9s %21s %s %s\r\n",
               "Port", "State", "Mode", "Status", "LinkMonitor", "ConfigRev",
               "MaxPdu");
    CliPrintf (CliHandle, "%-7s %s %s %s %s %s %s\r\n",
               "-------", "--------", "-------", "-------------------",
               "-----------", "---------", "------");
    /* Validation of index is not done here as the passed
     * index is already validated */

    do
    {
        CfaCliGetIfName ((UINT4) i4IfIndex, pi1IfName);
        CliPrintf (CliHandle, "%-7s ", pi1IfName);
        nmhGetDot3OamAdminState (i4IfIndex, &i4RetVal);
        if (i4RetVal == EOAM_SNMP_ENABLED)
        {
            CliPrintf (CliHandle, "%-9s", "enable");
        }
        else
        {
            CliPrintf (CliHandle, "%-9s", "disable");
        }
        nmhGetDot3OamMode (i4IfIndex, &i4RetVal);
        if (i4RetVal == EOAM_MODE_ACTIVE)
        {
            CliPrintf (CliHandle, "%-8s", "active");
        }
        else
        {
            CliPrintf (CliHandle, "%-8s", "passive");
        }
        nmhGetDot3OamOperStatus (i4IfIndex, &i4RetVal);
        switch (i4RetVal)
        {
            case EOAM_OPER_DISABLE:
                CliPrintf (CliHandle, "%-20s", "Disabled");
                break;
            case EOAM_OPER_LINKFAULT:
                CliPrintf (CliHandle, "%-20s", "Linkfault");
                break;
            case EOAM_OPER_PASSIVEWAIT:
                CliPrintf (CliHandle, "%-20s", "PassiveWait");
                break;
            case EOAM_OPER_ACTIVE_SENDLOCAL:
                CliPrintf (CliHandle, "%-20s", "ActiveSendLocal");
                break;
            case EOAM_OPER_SENDLOCALREMOTE:
                CliPrintf (CliHandle, "%-20s", "SendLocalRemote");
                break;
            case EOAM_OPER_SENDLOCALREMOTEOK:
                CliPrintf (CliHandle, "%-20s", "SendLocalRemoteOk");
                break;
            case EOAM_OPER_PEERINGLOCALLYREJECTED:
                CliPrintf (CliHandle, "%-20s", "LocallyRejected");
                break;
            case EOAM_OPER_PEERINGREMOTELYREJECTED:
                CliPrintf (CliHandle, "%-20s", "RemotelyRejected");
                break;
            case EOAM_OPER_OPERATIONAL:
                nmhGetDot3OamLoopbackStatus (i4IfIndex, &i4RetVal);
                switch (i4RetVal)
                {
                    case EOAM_NO_LOOPBACK:
                        CliPrintf (CliHandle, "%-20s", "Operational");
                        break;
                    case EOAM_INITIATING_LOOPBACK:
                        CliPrintf (CliHandle, "%-20s", "InitiatingLB");
                        break;
                    case EOAM_REMOTE_LOOPBACK:
                        CliPrintf (CliHandle, "%-20s", "R-Loopback");
                        break;
                    case EOAM_TERMINATING_LOOPBACK:
                        CliPrintf (CliHandle, "%-20s", "TerminatingLB");
                        break;
                    case EOAM_LOCAL_LOOPBACK:
                        CliPrintf (CliHandle, "%-20s", "L-Loopback");
                        break;
                    default:
                        CliPrintf (CliHandle, "%-20s", "Unknown");
                        break;
                }

                break;
            case EOAM_OPER_HALF_DUPLEX:
                CliPrintf (CliHandle, "%-20s", "HalfDuplex");
                break;
            default:
                break;
        }

        nmhGetDot3OamErrSymPeriodEvNotifEnable (i4IfIndex, &i4Symbol);
        nmhGetDot3OamErrFrameEvNotifEnable (i4IfIndex, &i4Frame);
        nmhGetDot3OamErrFramePeriodEvNotifEnable (i4IfIndex, &i4FramePeriod);
        nmhGetDot3OamErrFrameSecsEvNotifEnable (i4IfIndex, &i4FrameSecs);

        if ((i4Symbol == EOAM_TRUE) || (i4Frame == EOAM_TRUE)
            || (i4FramePeriod == EOAM_TRUE) || (i4FrameSecs == EOAM_TRUE))
        {
            CliPrintf (CliHandle, "%-12s", "enable");
        }
        else
        {
            CliPrintf (CliHandle, "%-12s", "disable");
        }

        nmhGetDot3OamConfigRevision (i4IfIndex, &u4RetVal);
        CliPrintf (CliHandle, "%-10u", u4RetVal);
        nmhGetDot3OamMaxOamPduSize (i4IfIndex, &u4RetVal);
        u4PagingStatus = CliPrintf (CliHandle, "%-6u\r\n", u4RetVal);

        if (u1IsShowAll == OSIX_FALSE)
        {
            /* Done with show for particular interface 
             * no need to continue */
            break;
        }

        i4PrevIfIndex = i4IfIndex;

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            break;
        }

    }
    while (nmhGetNextIndexDot3OamTable (i4PrevIfIndex, &i4IfIndex)
           == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliShowLocFuncSupp                                
 *                                                                          
 *     DESCRIPTION      : This function will display the EOAM functions
 *                        supported by local            
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number                       
 *                        u1IsShowAll - Flag to specify display for a
 *                        single port or all the ports
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ***************************************************************************/
PUBLIC INT4
EoamCliShowLocFuncSupp (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1IsShowAll)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1Val = 0;
    INT1               *pi1IfName = NULL;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4PrevIfIndex = 0;
    INT4                i4RetVal = 0;
    tSNMP_OCTET_STRING_TYPE FuncSuppBmp;

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (&FuncSuppBmp, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    pi1IfName = (INT1 *) au1IfName;

    FuncSuppBmp.pu1_OctetList = &u1Val;
    FuncSuppBmp.i4_Length = sizeof (UINT1);

    CliPrintf (CliHandle,
               "\r\n%-7s %-8s %-7s %-7s %s",
               "Port", "Remote", "Link", "UniDir", "Variable");
    CliPrintf (CliHandle,
               "\r\n%16s%6s%20s\r\n", "Loopback", "Event", "retrieval");
    CliPrintf (CliHandle, "%-7s %s %s %s %s\r\n",
               "-------", "--------", "-------", "-------", "---------");
    /* Validation of index is not done here as the passed
     * index is already validated */
    do
    {
        CfaCliGetIfName ((UINT4) i4IfIndex, pi1IfName);
        CliPrintf (CliHandle, "%-7s ", pi1IfName);
        nmhGetDot3OamFunctionsSupported (i4IfIndex, &FuncSuppBmp);
        nmhGetDot3OamLoopbackIgnoreRx (i4IfIndex, &i4RetVal);

        if (i4RetVal != EOAM_IGNORE_LB_CMD)
        {
            u4PagingStatus = CliPrintf (CliHandle, "%-9s", "permit");
        }
        else
        {
            u4PagingStatus = CliPrintf (CliHandle, "%-9s", "deny");
        }

        if (*(FuncSuppBmp.pu1_OctetList) & EOAM_SNMP_EVENT_SUPPORT)
        {
            CliPrintf (CliHandle, "%-8s", "enable");
        }
        else
        {
            CliPrintf (CliHandle, "%-8s", "disable");
        }

        if (*(FuncSuppBmp.pu1_OctetList) & EOAM_SNMP_UNIDIRECTIONAL_SUPPORT)
        {
            CliPrintf (CliHandle, "%-8s", "enable");
        }
        else
        {
            CliPrintf (CliHandle, "%-8s", "disable");
        }

        if (*(FuncSuppBmp.pu1_OctetList) & EOAM_SNMP_VARIABLE_SUPPORT)
        {
            u4PagingStatus = CliPrintf (CliHandle, "%-8s\r\n", "enable");
        }
        else
        {
            u4PagingStatus = CliPrintf (CliHandle, "%-8s\r\n", "disable");
        }

        if (u1IsShowAll == OSIX_FALSE)
        {
            /* Done with show for particular interface 
             * no need to continue */
            break;
        }

        i4PrevIfIndex = i4IfIndex;

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            break;
        }

    }
    while (nmhGetNextIndexDot3OamTable (i4PrevIfIndex, &i4IfIndex)
           == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliShowSymPeriod                               
 *                                                                          
 *     DESCRIPTION      : This function will display the EOAM symbol 
 *                        period event configuration            
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number                       
 *                        u1IsShowAll - Flag to specify display for a
 *                        single port or all the ports
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ***************************************************************************/
PUBLIC INT4
EoamCliShowSymPeriod (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1IsShowAll)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1Str[EOAM_CLI_U8_STR_LEN];
    INT1               *pi1IfName = NULL;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4Hi = 0;
    UINT4               u4Lo = 0;
    INT4                i4PrevIfIndex = 0;
    FS_UINT8            u8RetVal;
    FS_UINT8            u8Ret;

    FSAP_U8_CLR (&u8RetVal);
    FSAP_U8_CLR (&u8Ret);

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    pi1IfName = (INT1 *) au1IfName;

    CliPrintf (CliHandle, "\r\n%-7s %-21s%-21s",
               "Port", "ErrSymbol Period", "ErrSymbol Period");
    CliPrintf (CliHandle, "\r\n%-8s%-21s%-21s", " ", "Window", "Threshold");
    CliPrintf (CliHandle, "\r\n%-8s%-21s%-21s\r\n", " ", "(millions)", "Count");
    CliPrintf (CliHandle, "%-7s %s %s\r\n",
               "-------", "--------------------", "--------------------");
    /* Validation of index is not done here as the passed
     * index is already validated */
    do
    {

        CfaCliGetIfName ((UINT4) i4IfIndex, pi1IfName);
        CliPrintf (CliHandle, "%-7s ", pi1IfName);

        nmhGetDot3OamErrSymPeriodWindowLo (i4IfIndex, &u4Lo);
        nmhGetDot3OamErrSymPeriodWindowHi (i4IfIndex, &u4Hi);

        FSAP_U8_ASSIGN_LO (&u8RetVal, u4Lo);
        FSAP_U8_ASSIGN_HI (&u8RetVal, u4Hi);
        MEMSET (au1Str, 0, EOAM_CLI_U8_STR_LEN);
        EOAM_CLI_CONV_U8_TO_MILLIONS (&u8RetVal, &u8Ret);
        FSAP_U8_2STR (&u8RetVal, (CHR1 *) au1Str);
        CliPrintf (CliHandle, "%-21s", au1Str);
        /* The window size is less than million, 
         * displaying the exact value , 
         * instead of approximating to value zero.
         * This value cannot be configure via CLI/WEB can be configured 
         * via SNMP */
        if ((FSAP_U8_FETCH_LO (&u8RetVal) == 0)
            && (FSAP_U8_FETCH_HI (&u8RetVal) == 0))
        {
            CliPrintf (CliHandle, ".%06d", u8Ret.u4Lo);
        }

        nmhGetDot3OamErrSymPeriodThresholdLo (i4IfIndex, &u4Lo);
        nmhGetDot3OamErrSymPeriodThresholdHi (i4IfIndex, &u4Hi);

        FSAP_U8_ASSIGN_LO (&u8RetVal, u4Lo);
        FSAP_U8_ASSIGN_HI (&u8RetVal, u4Hi);
        MEMSET (au1Str, 0, EOAM_CLI_U8_STR_LEN);
        FSAP_U8_2STR (&u8RetVal, (CHR1 *) au1Str);

        u4PagingStatus = CliPrintf (CliHandle, "%-21s\r\n", au1Str);

        if (u1IsShowAll == OSIX_FALSE)
        {
            /* Done with show for particular interface 
             * no need to continue */
            break;
        }

        i4PrevIfIndex = i4IfIndex;

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            break;
        }

    }
    while (nmhGetNextIndexDot3OamTable (i4PrevIfIndex, &i4IfIndex)
           == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliShowFramePeriod                                
 *                                                                          
 *     DESCRIPTION      : This function will display the EOAM frame period 
 *                        event configuration            
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number                       
 *                        u1IsShowAll - Flag to specify display for a
 *                        single port or all the ports
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ***************************************************************************/
PUBLIC INT4
EoamCliShowFramePeriod (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1IsShowAll)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *pi1IfName = NULL;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4RetVal = 0;
    INT4                i4PrevIfIndex = 0;

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    pi1IfName = (INT1 *) au1IfName;

    CliPrintf (CliHandle, "\r\n%-7s %-21s%-21s",
               "Port", "ErrFrame Period", "ErrFrame Period");
    CliPrintf (CliHandle, "\r\n%-8s%-21s%-21s", " ", "Window", "Threshold");
    CliPrintf (CliHandle, "\r\n%34s\r\n", "Count");
    CliPrintf (CliHandle, "%-7s %s %s\r\n",
               "-------", "--------------------", "--------------------");
    /* Validation of index is not done here as the passed
     * index is already validated */
    do
    {
        CfaCliGetIfName ((UINT4) i4IfIndex, pi1IfName);
        CliPrintf (CliHandle, "%-7s ", pi1IfName);
        nmhGetDot3OamErrFramePeriodWindow (i4IfIndex, &u4RetVal);
        CliPrintf (CliHandle, "%-21u", u4RetVal);
        nmhGetDot3OamErrFramePeriodThreshold (i4IfIndex, &u4RetVal);
        u4PagingStatus = CliPrintf (CliHandle, "%-21u\r\n", u4RetVal);

        if (u1IsShowAll == OSIX_FALSE)
        {
            /* Done with show for particular interface 
             * no need to continue */
            break;
        }

        i4PrevIfIndex = i4IfIndex;

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            break;
        }

    }
    while (nmhGetNextIndexDot3OamTable (i4PrevIfIndex, &i4IfIndex)
           == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliShowFrame                                
 *                                                                          
 *     DESCRIPTION      : This function will display the EOAM frame 
 *                        event configuration
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number                       
 *                        u1IsShowAll - Flag to specify display for a
 *                        single port or all the ports
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ***************************************************************************/
PUBLIC INT4
EoamCliShowFrame (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1IsShowAll)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *pi1IfName = NULL;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4RetVal = 0;
    INT4                i4PrevIfIndex = 0;

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    pi1IfName = (INT1 *) (&au1IfName[0]);

    CliPrintf (CliHandle, "\r\n%-7s %-21s%-21s",
               "Port", "Errored Frame", "Errored Frame");
    CliPrintf (CliHandle, "\r\n%-8s%-21s%-21s", " ", "Window", "Threshold");
    CliPrintf (CliHandle, "\r\n%-8s%-21s%-21s\r\n", " ", "(100 msec)", "Count");
    CliPrintf (CliHandle, "%-7s %s %s\r\n",
               "-------", "--------------------", "--------------------");
    /* Validation of index is not done here as the passed
     * index is already validated */
    do
    {
        CfaCliGetIfName ((UINT4) i4IfIndex, pi1IfName);
        CliPrintf (CliHandle, "%-7s ", pi1IfName);
        nmhGetDot3OamErrFrameWindow (i4IfIndex, &u4RetVal);
        CliPrintf (CliHandle, "%-21u", u4RetVal);
        nmhGetDot3OamErrFrameThreshold (i4IfIndex, &u4RetVal);
        u4PagingStatus = CliPrintf (CliHandle, "%-21u\r\n", u4RetVal);

        if (u1IsShowAll == OSIX_FALSE)
        {
            /* Done with show for particular interface 
             * no need to continue */
            break;
        }

        i4PrevIfIndex = i4IfIndex;

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            break;
        }

    }
    while (nmhGetNextIndexDot3OamTable (i4PrevIfIndex, &i4IfIndex)
           == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliShowFrameSecSumm                               
 *                                                                          
 *     DESCRIPTION      : This function will display the EOAM frame seconds
 *                        summary event configuration
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number                       
 *                        u1IsShowAll - Flag to specify display for a
 *                        single port or all the ports
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ***************************************************************************/
PUBLIC INT4
EoamCliShowFrameSecSumm (tCliHandle CliHandle, INT4 i4IfIndex,
                         UINT1 u1IsShowAll)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *pi1IfName = NULL;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4RetVal = 0;
    INT4                i4PrevIfIndex = 0;

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    pi1IfName = (INT1 *) au1IfName;

    CliPrintf (CliHandle, "\r\n%-7s %-21s%-21s",
               "Port", "ErrFrameSec Summary", "ErrFrameSec Summary");
    CliPrintf (CliHandle, "\r\n%-8s%-21s%-21s", " ", "Window", "Threshold");
    CliPrintf (CliHandle, "\r\n%-8s%-21s%-21s\r\n", " ", "(100 msec)", "Count");
    CliPrintf (CliHandle, "%-7s %s %s\r\n",
               "-------", "--------------------", "--------------------");
    /* Validation of index is not done here as the passed
     * index is already validated */
    do
    {

        CfaCliGetIfName ((UINT4) i4IfIndex, pi1IfName);
        CliPrintf (CliHandle, "%-7s ", pi1IfName);
        nmhGetDot3OamErrFrameSecsSummaryWindow (i4IfIndex, &i4RetVal);
        CliPrintf (CliHandle, "%-21d", i4RetVal);
        nmhGetDot3OamErrFrameSecsSummaryThreshold (i4IfIndex, &i4RetVal);
        u4PagingStatus = CliPrintf (CliHandle, "%-21d\r\n", i4RetVal);

        if (u1IsShowAll == OSIX_FALSE)
        {
            /* Done with show for particular interface 
             * no need to continue */
            break;
        }

        i4PrevIfIndex = i4IfIndex;

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            break;
        }

    }
    while (nmhGetNextIndexDot3OamTable (i4PrevIfIndex, &i4IfIndex)
           == SNMP_SUCCESS);

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliShowPeerInfo                                  
 *                                                                          
 *     DESCRIPTION      : This function will display the peer information
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number                       
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ***************************************************************************/
PUBLIC INT4
EoamCliShowPeerInfo (tCliHandle CliHandle, INT4 i4IfIndex)
{
    UINT1               u1IsShowAll = OSIX_TRUE;

    if (i4IfIndex == 0)
    {
        /*Show for all the ports */
        if (nmhGetFirstIndexDot3OamPeerTable (&i4IfIndex) != SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }
    else
    {

        /*Show for a specific port */
        if (nmhValidateIndexInstanceDot3OamPeerTable (i4IfIndex) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        u1IsShowAll = OSIX_FALSE;
    }

    EoamCliShowPeerPortInfo (CliHandle, i4IfIndex, u1IsShowAll);
    EoamCliShowPeerFuncSupp (CliHandle, i4IfIndex, u1IsShowAll);

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliShowPeerPortInfo
 *     
 *     DESCRIPTION      : This function will display the EOAM peer
 *                        information from dot3OamPeerInfo Table
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number                       
 *                        u1IsShowAll - Flag to specify display for a
 *                        single port or all the ports
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ***************************************************************************/
PUBLIC INT4
EoamCliShowPeerPortInfo (tCliHandle CliHandle, INT4 i4IfIndex,
                         UINT1 u1IsShowAll)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1Oui[EOAM_OUI_LENGTH];
    UINT1               au1Val[EOAM_CLI_DISP_MAC_LEN];
    UINT1               au1OuiVal[EOAM_CLI_OUI_STR_LEN];
    INT1               *pi1IfName = NULL;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4RetVal = 0;
    INT4                i4PrevIfIndex = 0;
    INT4                i4RetVal = 0;
    tMacAddr            MacAddr;
    tSNMP_OCTET_STRING_TYPE PeerInfo;

    MEMSET (MacAddr, 0, MAC_ADDR_LEN);
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (&PeerInfo, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1Val, 0, EOAM_CLI_DISP_MAC_LEN);
    MEMSET (au1Oui, 0, EOAM_OUI_LENGTH);
    MEMSET (au1OuiVal, 0, EOAM_CLI_OUI_STR_LEN);

    pi1IfName = (INT1 *) au1IfName;
    PeerInfo.pu1_OctetList = au1Oui;

    CliPrintf (CliHandle,
               "\r\n%-7s %-17s   %-8s %s %-7s %s %s\r\n",
               "Port", "Mac Addr", "OUI", "VendorInfo", "Mode", "ConfigRev",
               "MaxPdu");
    CliPrintf (CliHandle, "%-7s %s   %s %s %s %s %s\r\n",
               "-------", "-----------------", "--------",
               "----------", "-------", "---------", "------");

    /* Validation of index is not done here as the passed
     * index is already validated */
    do
    {
        CfaCliGetIfName ((UINT4) i4IfIndex, pi1IfName);
        CliPrintf (CliHandle, "%-7s ", pi1IfName);

        nmhGetDot3OamPeerMacAddress (i4IfIndex, &MacAddr);
        PrintMacAddress ((UINT1 *) &MacAddr, au1Val);
        CliPrintf (CliHandle, "%-18s", au1Val);

        MEMSET (PeerInfo.pu1_OctetList, 0, EOAM_OUI_LENGTH);
        nmhGetDot3OamPeerVendorOui (i4IfIndex, &PeerInfo);
        /* Convert the OUI to printable format like 00:01:02 */

        CliOctetToStr (PeerInfo.pu1_OctetList,
                       PeerInfo.i4_Length, au1OuiVal, EOAM_CLI_OUI_STR_LEN);
        CliPrintf (CliHandle, "%-9s", au1OuiVal);

        nmhGetDot3OamPeerVendorInfo (i4IfIndex, &u4RetVal);
        CliPrintf (CliHandle, "%08X   ", u4RetVal);

        nmhGetDot3OamPeerMode (i4IfIndex, &i4RetVal);

        switch (i4RetVal)
        {
            case EOAM_MODE_ACTIVE:
                CliPrintf (CliHandle, "%-8s", "active");
                break;
            case EOAM_MODE_PASSIVE:
                CliPrintf (CliHandle, "%-8s", "passive");
                break;
            case EOAM_MODE_PEER_UNKNOWN:
                CliPrintf (CliHandle, "%-8s", "unknown");
                break;
            default:
                break;
        }

        nmhGetDot3OamPeerConfigRevision (i4IfIndex, &u4RetVal);
        CliPrintf (CliHandle, "%-10u", u4RetVal);

        nmhGetDot3OamPeerMaxOamPduSize (i4IfIndex, &u4RetVal);
        u4PagingStatus = CliPrintf (CliHandle, "%-6u\r\n", u4RetVal);

        if (u1IsShowAll == OSIX_FALSE)
        {
            /* Done with show for particular interface 
             * no need to continue */
            break;
        }

        i4PrevIfIndex = i4IfIndex;

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            break;
        }

    }
    while (nmhGetNextIndexDot3OamTable (i4PrevIfIndex, &i4IfIndex)
           == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliShowPeerFuncSupp                               
 *                                                                          
 *     DESCRIPTION      : This function will display the functions
 *                        supported by peer
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number                       
 *                        u1IsShowAll - Flag to specify display for a
 *                        single port or all the ports
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ***************************************************************************/
PUBLIC INT4
EoamCliShowPeerFuncSupp (tCliHandle CliHandle, INT4 i4IfIndex,
                         UINT1 u1IsShowAll)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1Val = 0;
    INT1               *pi1IfName = NULL;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4PrevIfIndex = 0;
    tSNMP_OCTET_STRING_TYPE FuncSuppBmp;

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (&FuncSuppBmp, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    pi1IfName = (INT1 *) au1IfName;

    FuncSuppBmp.pu1_OctetList = &u1Val;
    FuncSuppBmp.i4_Length = sizeof (UINT1);

    CliPrintf (CliHandle,
               "\r\n%-7s %-9s %-7s %-7s %s",
               "Port", "Remote", "Link", "UniDir", "Variable");
    CliPrintf (CliHandle,
               "\r\n%16s%7s%20s\r\n", "Loopback", "Event", "retrieval");
    CliPrintf (CliHandle, "%-7s %s %s %s %s\r\n",
               "-------", "---------", "-------", "-------", "---------");
    /* Validation of index is not done here as the passed
     * index is already validated */
    do
    {
        CfaCliGetIfName ((UINT4) i4IfIndex, pi1IfName);
        CliPrintf (CliHandle, "%-7s ", pi1IfName);

        nmhGetDot3OamPeerFunctionsSupported (i4IfIndex, &FuncSuppBmp);

        if (*(FuncSuppBmp.pu1_OctetList) & EOAM_LOOPBACK_SUPPORT)
        {
            CliPrintf (CliHandle, "%-10s", "enable");
        }
        else
        {
            CliPrintf (CliHandle, "%-10s", "disable");
        }

        if (*(FuncSuppBmp.pu1_OctetList) & EOAM_EVENT_SUPPORT)
        {
            CliPrintf (CliHandle, "%-8s", "enable");
        }
        else
        {
            CliPrintf (CliHandle, "%-8s", "disable");
        }

        if (*(FuncSuppBmp.pu1_OctetList) & EOAM_UNIDIRECTIONAL_SUPPORT)
        {
            CliPrintf (CliHandle, "%-8s", "enable");
        }
        else
        {
            CliPrintf (CliHandle, "%-8s", "disable");
        }

        if (*(FuncSuppBmp.pu1_OctetList) & EOAM_VARIABLE_SUPPORT)
        {
            u4PagingStatus = CliPrintf (CliHandle, "%-8s\r\n", "enable");
        }
        else
        {
            u4PagingStatus = CliPrintf (CliHandle, "%-8s\r\n", "disable");
        }

        if (u1IsShowAll == OSIX_FALSE)
        {
            /* Done with show for particular interface 
             * no need to continue */
            break;
        }

        i4PrevIfIndex = i4IfIndex;

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            break;
        }

    }
    while (nmhGetNextIndexDot3OamTable (i4PrevIfIndex, &i4IfIndex)
           == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliShowLBCpb                                     
 *                                                                          
 *     DESCRIPTION      : This function will display the loopback 
 *                        capabilities
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number                       
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamCliShowLBCpb (tCliHandle CliHandle, INT4 i4IfIndex)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1IsShowAll = OSIX_TRUE;
    UINT1               u1Val = 0;
    INT1               *pi1IfName = NULL;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4PrevIfIndex = 0;
    INT4                i4RetVal = 0;
    tSNMP_OCTET_STRING_TYPE FuncSuppBmp;

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (&FuncSuppBmp, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    pi1IfName = (INT1 *) au1IfName;

    FuncSuppBmp.pu1_OctetList = &u1Val;
    FuncSuppBmp.i4_Length = sizeof (UINT1);

    if (i4IfIndex == 0)
    {
        /*For all the ports */
        if (nmhGetFirstIndexDot3OamTable (&i4IfIndex) != SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }

    }
    else
    {

        /*For a specific port */
        if (nmhValidateIndexInstanceDot3OamTable (i4IfIndex) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        u1IsShowAll = OSIX_FALSE;
    }

    CliPrintf (CliHandle, "\r\n%-7s   %-8s\r\n", " ", "Remote Loopback");
    CliPrintf (CliHandle, "%-7s %-10s %-7s\r\n", "Port", "Capability",
               "Control");
    CliPrintf (CliHandle, "%-7s %s %s\r\n", "-------", "----------", "-------");
    /* Validation of index is not done here as the passed
     * index is already validated */
    do
    {
        CfaCliGetIfName ((UINT4) i4IfIndex, pi1IfName);
        CliPrintf (CliHandle, "%-7s ", pi1IfName);
        nmhGetDot3OamFunctionsSupported (i4IfIndex, &FuncSuppBmp);
        nmhGetDot3OamLoopbackIgnoreRx (i4IfIndex, &i4RetVal);

        if (*(FuncSuppBmp.pu1_OctetList) & EOAM_SNMP_LOOPBACK_SUPPORT)
        {
            CliPrintf (CliHandle, "%-11s ", "enable");
        }
        else
        {
            CliPrintf (CliHandle, "%-11s ", "disable");
        }

        if (i4RetVal != EOAM_IGNORE_LB_CMD)
        {
            u4PagingStatus = CliPrintf (CliHandle, "%-8s\r\n", "permit");
        }
        else
        {
            u4PagingStatus = CliPrintf (CliHandle, "%-8s\r\n", "deny");
        }

        if (u1IsShowAll == OSIX_FALSE)
        {
            /* Done with show for particular interface 
             * no need to continue */
            break;
        }

        i4PrevIfIndex = i4IfIndex;

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            break;
        }

    }
    while (nmhGetNextIndexDot3OamTable (i4PrevIfIndex, &i4IfIndex)
           == SNMP_SUCCESS);

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliShowEvtNotif                                   
 *                                                                          
 *     DESCRIPTION      : This function will display whether 
 *                        the Event Notifications are enabled or disabled 
 *                        on the port(s)
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number                       
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamCliShowEvtNotif (tCliHandle CliHandle, INT4 i4IfIndex)
{

    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *pi1IfName = NULL;
    UINT4               u1IsShowAll = OSIX_TRUE;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4RetVal = 0;
    INT4                i4PrevIfIndex = 0;

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    pi1IfName = (INT1 *) (&au1IfName[0]);

    /* For all interface */
    if (i4IfIndex == 0)
    {
        if (nmhGetFirstIndexDot3OamEventConfigTable (&i4IfIndex)
            != SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }
    else
    {
        /* For a particular interface */
        if (nmhValidateIndexInstanceDot3OamEventConfigTable (i4IfIndex)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Not a valid link event table entry\r\n");
            return CLI_FAILURE;
        }

        u1IsShowAll = OSIX_FALSE;
    }

    CliPrintf (CliHandle, "%42s\r\n", "Link Event Notifications");
    CliPrintf (CliHandle, "%s%s\r\n",
               "        ---------",
               "------------------------------------------");
    CliPrintf (CliHandle, "%14s %6s %7s %12s %3s %s\r\n",
               "Symbol", "Frame", "Frame", "Frame secs", "Critical", "Dying");
    CliPrintf (CliHandle, "%-7s %-7s %14s %8s %8s %7s\r\n",
               "Port", "Period", "Period", "Summary", "Event", "Gasp");
    CliPrintf (CliHandle, "%-7s %s %s %s %s %s %s\r\n",
               "-------", "-------", "-------", "-------", "----------",
               "--------", "-------");

    do
    {
        if (CfaCliGetIfName ((UINT4) i4IfIndex, pi1IfName) != CLI_FAILURE)
        {
            CliPrintf (CliHandle, "%-7s ", pi1IfName);

            nmhGetDot3OamErrSymPeriodEvNotifEnable (i4IfIndex, &i4RetVal);

            if (i4RetVal == EOAM_TRUE)
            {
                CliPrintf (CliHandle, "%-8s", "enable");
            }
            else
            {
                CliPrintf (CliHandle, "%-8s", "disable");
            }

            nmhGetDot3OamErrFrameEvNotifEnable (i4IfIndex, &i4RetVal);

            if (i4RetVal == EOAM_TRUE)
            {
                CliPrintf (CliHandle, "%-8s", "enable");
            }
            else
            {
                CliPrintf (CliHandle, "%-8s", "disable");
            }

            nmhGetDot3OamErrFramePeriodEvNotifEnable (i4IfIndex, &i4RetVal);

            if (i4RetVal == EOAM_TRUE)
            {
                CliPrintf (CliHandle, "%-8s", "enable");
            }
            else
            {
                CliPrintf (CliHandle, "%-8s", "disable");
            }

            nmhGetDot3OamErrFrameSecsEvNotifEnable (i4IfIndex, &i4RetVal);

            if (i4RetVal == EOAM_TRUE)
            {
                CliPrintf (CliHandle, "%-11s", "enable");
            }
            else
            {
                CliPrintf (CliHandle, "%-11s", "disable");
            }

            nmhGetDot3OamCriticalEventEnable (i4IfIndex, &i4RetVal);

            if (i4RetVal == EOAM_TRUE)
            {
                CliPrintf (CliHandle, "%-9s", "enable");
            }
            else
            {
                CliPrintf (CliHandle, "%-9s", "disable");
            }

            nmhGetDot3OamDyingGaspEnable (i4IfIndex, &i4RetVal);

            if (i4RetVal == EOAM_TRUE)
            {
                u4PagingStatus = CliPrintf (CliHandle, "%-8s", "enable\r\n");
            }
            else
            {
                u4PagingStatus = CliPrintf (CliHandle, "%-8s", "disable\r\n");
            }
        }

        if (u1IsShowAll == OSIX_FALSE)
        {
            break;
        }

        i4PrevIfIndex = i4IfIndex;

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            break;
        }
    }
    while (nmhGetNextIndexDot3OamEventConfigTable (i4PrevIfIndex, &i4IfIndex)
           == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliShowStats                                     
 *                                                                          
 *     DESCRIPTION      : This function will display the statistics          
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number                       
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamCliShowStats (tCliHandle CliHandle, INT4 i4IfIndex)
{
    UINT1               u1IsShowAll = OSIX_TRUE;

    if (i4IfIndex == 0)
    {
        /*For all the ports */
        if (nmhGetFirstIndexDot3OamStatsTable (&i4IfIndex) != SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }

    }
    else
    {

        /*For a specific port */
        if (nmhValidateIndexInstanceDot3OamStatsTable (i4IfIndex) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        u1IsShowAll = OSIX_FALSE;
    }

    EoamCliShowRxStats (CliHandle, i4IfIndex, u1IsShowAll);
    EoamCliShowTxStats (CliHandle, i4IfIndex, u1IsShowAll);
    EoamCliShowRxTxStats (CliHandle, i4IfIndex, u1IsShowAll);

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliShowRxStats                                    
 *                                                                          
 *     DESCRIPTION      : This function will display the statistics          
 *                        of InformationPDUs, UniEventPDUs, DupEventPDUs
 *                        RemoteLBCtrlPDUs, VarReqPDUs and VarResPDUs Rx
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number                       
 *                        u1IsShowAll - Flag to specify display for a
 *                        single port or all the ports
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamCliShowRxStats (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1IsShowAll)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *pi1IfName = NULL;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4RetVal = 0;
    INT4                i4PrevIfIndex = 0;

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    pi1IfName = (INT1 *) au1IfName;

    CliPrintf (CliHandle,
               "\r\n%-7s %s %s %s %s %s %s\r\n",
               "Port", "InfoPduRx", "UniEventRx",
               "DupEventRx", "RLBCtrlRx", "VarReqRx", "VarResRx");
    CliPrintf (CliHandle, "%-7s %s %s %s %s %s %s\r\n",
               "-------", "---------", "----------",
               "----------", "---------", "--------", "--------");
    /* Validation of index is not done here as the passed
     * index is already validated */
    do
    {
        CfaCliGetIfName ((UINT4) i4IfIndex, pi1IfName);
        CliPrintf (CliHandle, "%-7s ", pi1IfName);
        nmhGetDot3OamInformationRx (i4IfIndex, &u4RetVal);
        CliPrintf (CliHandle, "%-10u", u4RetVal);
        nmhGetDot3OamUniqueEventNotificationRx (i4IfIndex, &u4RetVal);
        CliPrintf (CliHandle, "%-11u", u4RetVal);
        nmhGetDot3OamDuplicateEventNotificationRx (i4IfIndex,
                                                   (UINT4 *) &u4RetVal);
        CliPrintf (CliHandle, "%-11u", u4RetVal);
        nmhGetDot3OamLoopbackControlRx (i4IfIndex, &u4RetVal);
        CliPrintf (CliHandle, "%-10u", u4RetVal);
        nmhGetDot3OamVariableRequestRx (i4IfIndex, &u4RetVal);
        CliPrintf (CliHandle, "%-9u", u4RetVal);
        nmhGetDot3OamVariableResponseRx (i4IfIndex, &u4RetVal);
        u4PagingStatus = CliPrintf (CliHandle, "%-9u\r\n", u4RetVal);

        if ((u1IsShowAll == OSIX_FALSE))
        {
            /* Done with show for particular interface 
             * no need to continue */
            break;
        }

        i4PrevIfIndex = i4IfIndex;

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            break;
        }

    }
    while (nmhGetNextIndexDot3OamTable (i4PrevIfIndex, &i4IfIndex)
           == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliShowTxStats                                
 *                                                                          
 *     DESCRIPTION      : This function will display the statistics          
 *                        of InformationPDUs, UniEventPDUs, DupEventPDUs
 *                        RemoteLBCtrlPDUs, VarReqPDUs and VarResPDUs Tx
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number                       
 *                        u1IsShowAll - Flag to specify display for a
 *                        single port or all the ports
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamCliShowTxStats (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1IsShowAll)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *pi1IfName = NULL;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4RetVal = 0;
    INT4                i4PrevIfIndex = 0;

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    pi1IfName = (INT1 *) au1IfName;

    CliPrintf (CliHandle,
               "\r\n%-7s %s %s %s %s %s %s\r\r\n",
               "Port", "InfoPduTx", "UniEventTx",
               "DupEventTx", "RLBCtrlTx", "VarReqTx", "VarResTx");
    CliPrintf (CliHandle, "%-7s %s %s %s %s %s %s\r\n",
               "-------", "---------", "----------",
               "----------", "---------", "--------", "--------");
    /* Validation of index is not done here as the passed
     * index is already validated */

    do
    {
        CfaCliGetIfName ((UINT4) i4IfIndex, pi1IfName);
        CliPrintf (CliHandle, "%-7s ", pi1IfName);
        nmhGetDot3OamInformationTx (i4IfIndex, &u4RetVal);
        CliPrintf (CliHandle, "%-10u", u4RetVal);
        nmhGetDot3OamUniqueEventNotificationTx (i4IfIndex, &u4RetVal);
        CliPrintf (CliHandle, "%-11u", u4RetVal);
        nmhGetDot3OamDuplicateEventNotificationTx (i4IfIndex, &u4RetVal);
        CliPrintf (CliHandle, "%-11u", u4RetVal);
        nmhGetDot3OamLoopbackControlTx (i4IfIndex, &u4RetVal);
        CliPrintf (CliHandle, "%-10u", u4RetVal);
        nmhGetDot3OamVariableRequestTx (i4IfIndex, &u4RetVal);
        CliPrintf (CliHandle, "%-9u", u4RetVal);
        nmhGetDot3OamVariableResponseTx (i4IfIndex, &u4RetVal);
        u4PagingStatus = CliPrintf (CliHandle, "%-9u\r\n", u4RetVal);

        if ((u1IsShowAll == OSIX_FALSE))
        {
            /* Done with show for particular interface 
             * no need to continue */
            break;
        }

        i4PrevIfIndex = i4IfIndex;

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            break;
        }

    }
    while (nmhGetNextIndexDot3OamTable (i4PrevIfIndex, &i4IfIndex)
           == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliShowRxTxStats                             
 *                                                                          
 *     DESCRIPTION      : This function will display the statistics of
 *                        OrgSpecPDUS Tx & RX , UnknownPDUs Tx & Rx
 *                        and also the frames lost
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number                       
 *                        u1IsShowAll - Flag to specify display for a
 *                        single port or all the ports
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamCliShowRxTxStats (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1IsShowAll)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *pi1IfName = NULL;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4RetVal = 0;
    INT4                i4PrevIfIndex = 0;

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    pi1IfName = (INT1 *) (&au1IfName[0]);

    CliPrintf (CliHandle,
               "\r\n%-7s %s %s  %s  %s %s\r\n",
               "Port", "OrgSpecRx", "UnknownRx",
               "OrgSpecTx", "UnknownTx", "FramesLost");
    CliPrintf (CliHandle, "%-7s %s %s %s %s %s\r\n",
               "-------", "---------", "----------",
               "----------", "---------", "----------");
    /* Validation of index is not done here as the passed
     * index is already validated */
    do
    {
        CfaCliGetIfName ((UINT4) i4IfIndex, pi1IfName);
        CliPrintf (CliHandle, "%-7s ", pi1IfName);
        nmhGetDot3OamOrgSpecificRx (i4IfIndex, &u4RetVal);
        CliPrintf (CliHandle, "%-10u", u4RetVal);
        nmhGetDot3OamUnsupportedCodesRx (i4IfIndex, &u4RetVal);
        CliPrintf (CliHandle, "%-11u", u4RetVal);
        nmhGetDot3OamOrgSpecificTx (i4IfIndex, &u4RetVal);
        CliPrintf (CliHandle, "%-11u", u4RetVal);
        nmhGetDot3OamUnsupportedCodesTx (i4IfIndex, &u4RetVal);
        CliPrintf (CliHandle, "%-10u", u4RetVal);
        nmhGetDot3OamFramesLostDueToOam (i4IfIndex, &u4RetVal);
        u4PagingStatus = CliPrintf (CliHandle, "%-10u\r\n", u4RetVal);

        if ((u1IsShowAll == OSIX_FALSE))
        {
            /* Done with show for particular interface 
             * no need to continue */
            break;
        }

        i4PrevIfIndex = i4IfIndex;

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            break;
        }

    }
    while (nmhGetNextIndexDot3OamTable (i4PrevIfIndex, &i4IfIndex)
           == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliShowEventLog                                  
 *                                                                          
 *     DESCRIPTION      : This function will display the event log details
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number                       
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamCliShowEventLog (tCliHandle CliHandle, INT4 i4IfIndex)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *pi1IfName = NULL;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4EvtLogIndex = 0;
    UINT4               u4PrevVal = 0;
    INT4                i4PrevIfIndex = 0;
    INT4                i4Port = 0;

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    pi1IfName = (INT1 *) au1IfName;

    i4Port = i4IfIndex;

    if (nmhGetFirstIndexDot3OamEventLogTable (&i4IfIndex, &u4EvtLogIndex)
        != SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    do
    {
        if (((i4Port != 0) && (i4IfIndex == i4Port)) || ((i4Port == 0)))
        {
            if (i4PrevIfIndex != i4IfIndex)
            {
                CfaCliGetIfName ((UINT4) i4IfIndex, pi1IfName);
                u4PagingStatus = CliPrintf (CliHandle,
                                            "\nPort: %s\r\n", pi1IfName);
            }
        }
        i4PrevIfIndex = i4IfIndex;
        u4PrevVal = u4EvtLogIndex;
        /* 
         * For a specific port - If the get next gets the eventlog
         * of the unmatched interface entry then continue
         */
        if ((i4Port != 0) && (i4Port != i4IfIndex))
        {
            continue;
        }

        /* 
         * For a specific port - If the get next gets the eventlog of 
         * the given interface entry then validate
         */
        if ((i4Port != 0) && (i4Port == i4IfIndex))
        {
            if (nmhValidateIndexInstanceDot3OamEventLogTable (i4IfIndex,
                                                              u4EvtLogIndex)
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
        }
        /* Show for specific/all ports */
        CliPrintf (CliHandle, "%-10s%s%u\r\n", " ", "Event: ", u4EvtLogIndex);

        EoamCliDispEvtLogEntry (CliHandle, i4IfIndex, u4EvtLogIndex);
        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            break;
        }

    }
    while (nmhGetNextIndexDot3OamEventLogTable (i4PrevIfIndex, &i4IfIndex,
                                                u4PrevVal, &u4EvtLogIndex)
           == SNMP_SUCCESS);

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/****************************************************************************
 *                                                                           
 *     FUNCTION         : EoamCliDispEvtLogEntry              
 *                                                                           
 *     DESCRIPTION      : This function prints the event log entry
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                         
 *                        i4IfIndex - Port Number
 *                        u4EvtLogIndex - Flag to specify whether the 
 *                        port name is already displayed or not
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            
 *                                                                           
 ****************************************************************************/
PUBLIC VOID
EoamCliDispEvtLogEntry (tCliHandle CliHandle, INT4 i4IfIndex,
                        UINT4 u4EvtLogIndex)
{
    UINT4               u4RetVal = 0;
    INT4                i4RetVal = 0;
    UINT1               au1OuiVal[EOAM_CLI_OUI_STR_LEN];
    UINT1               au1RetOuiVal[EOAM_OUI_LENGTH];
    tSNMP_COUNTER64_TYPE RetVal64;
    tSNMP_OCTET_STRING_TYPE LocalClientOui;

    MEMSET (au1RetOuiVal, 0, EOAM_OUI_LENGTH);
    MEMSET (au1OuiVal, 0, EOAM_CLI_OUI_STR_LEN);
    MEMSET (&LocalClientOui, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&RetVal64, 0, sizeof (tSNMP_COUNTER64_TYPE));

    LocalClientOui.i4_Length = EOAM_OUI_LENGTH;
    LocalClientOui.pu1_OctetList = au1RetOuiVal;

    nmhGetDot3OamEventLogTimestamp (i4IfIndex, u4EvtLogIndex, &u4RetVal);
    CliPrintf (CliHandle, "%-20s%s%u\r\n", " ", "Time         : ", u4RetVal);

    nmhGetDot3OamEventLogOui (i4IfIndex, u4EvtLogIndex, &LocalClientOui);
    /* Convert the OUI to printable format like 00:01:02 */
    CliOctetToStr (LocalClientOui.pu1_OctetList,
                   LocalClientOui.i4_Length, au1OuiVal, EOAM_CLI_OUI_STR_LEN);
    CliPrintf (CliHandle, "%-20s%s%s\n", " ", "OUI          : ", au1OuiVal);

    nmhGetDot3OamEventLogType (i4IfIndex, u4EvtLogIndex, &u4RetVal);
    CliPrintf (CliHandle, "%-20s%s", " ", "Event Type   : ");

    switch (u4RetVal)
    {
        case EOAM_ERRORED_SYMBOL_EVENT:
            CliPrintf (CliHandle, "Errored Symbol Event\r\n");
            break;
        case EOAM_ERRORED_FRAME_PERIOD_EVENT:
            CliPrintf (CliHandle, "Errored Frame Period Event\r\n");
            break;
        case EOAM_ERRORED_FRAME_EVENT:
            CliPrintf (CliHandle, "Errored Frame Event\r\n");
            break;
        case EOAM_ERRORED_FRAME_SECONDS_EVENT:
            CliPrintf (CliHandle, "Errored Frame Seconds Event\r\n");
            break;
        case EOAM_LINK_FAULT:
            CliPrintf (CliHandle, "Link Fault Event\r\n");
            break;
        case EOAM_DYING_GASP:
            CliPrintf (CliHandle, "Dying Gasp Event\r\n");
            break;
        case EOAM_CRITICAL_EVENT:
            CliPrintf (CliHandle, "Critical Link Event\r\n");
            break;
        case EOAM_ORG_SPEC_LINK_EVENT:
            CliPrintf (CliHandle, "Organization specific Event\r\n");
            break;
        default:
            CliPrintf (CliHandle, "\r\n");
            break;
    }

    nmhGetDot3OamEventLogLocation (i4IfIndex, u4EvtLogIndex, &i4RetVal);
    CliPrintf (CliHandle, "%-20s%s", " ", "Location     : ");

    if (i4RetVal == EOAM_LOCAL_ENTRY)
    {
        CliPrintf (CliHandle, "Local\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "Remote\r\n");
    }
    if ((u4RetVal == EOAM_ERRORED_FRAME_SECONDS_EVENT) ||
        (u4RetVal == EOAM_ERRORED_FRAME_EVENT) ||
        (u4RetVal == EOAM_ERRORED_SYMBOL_EVENT) ||
        (u4RetVal == EOAM_ERRORED_FRAME_PERIOD_EVENT))
    {
        nmhGetDot3OamEventLogWindowHi (i4IfIndex, u4EvtLogIndex, &u4RetVal);
        CliPrintf (CliHandle, "%-20s%s%-10u", " ",
                   "Window       : High: ", u4RetVal);

        nmhGetDot3OamEventLogWindowLo (i4IfIndex, u4EvtLogIndex, &u4RetVal);
        CliPrintf (CliHandle, "%-10s%s%-10u\r\n", " ", "Low: ", u4RetVal);

        nmhGetDot3OamEventLogThresholdHi (i4IfIndex, u4EvtLogIndex, &u4RetVal);
        CliPrintf (CliHandle, "%-20s%s%-10u", " ",
                   "Threshold    : High: ", u4RetVal);

        nmhGetDot3OamEventLogThresholdLo (i4IfIndex, u4EvtLogIndex, &u4RetVal);
        CliPrintf (CliHandle, "%-10s%s%-10u\r\n", " ", "Low: ", u4RetVal);

        nmhGetDot3OamEventLogValue (i4IfIndex, u4EvtLogIndex, &RetVal64);
        CliPrintf (CliHandle, "%-20s%s%-10u%-10s%s%-10u\r\n", " ",
                   "Value        : High: ", RetVal64.msn, " ", "Low: ",
                   RetVal64.lsn);

        nmhGetDot3OamEventLogRunningTotal (i4IfIndex, u4EvtLogIndex, &RetVal64);
        CliPrintf (CliHandle, "%-20s%s%-10u%-10s%s%-10u\r\n", " ",
                   "Running Total: High: ", RetVal64.msn, " ", "Low: ",
                   RetVal64.lsn);

    }
    nmhGetDot3OamEventLogEventTotal (i4IfIndex, u4EvtLogIndex, &u4RetVal);
    CliPrintf (CliHandle, "%-20s%s%-10u\r\n", " ", "Event Total  : ", u4RetVal);

    return;
}

/****************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliSetPortAdminStatus                            
 *                                                                          
 *     DESCRIPTION      : This function will set the admin status 
 *                        per port
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number                       
 *                        i4Status - Port Admin Status
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamCliSetPortAdminStatus (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4Status)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2Dot3OamAdminState (&u4ErrorCode, i4IfIndex, i4Status)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetDot3OamAdminState (i4IfIndex, i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliSetPortMode                                   
 *                                                                          
 *     DESCRIPTION      : This function will set the port mode            
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number                       
 *                        i4Mode - Port Mode
 *                                                                          
 *     OUTPUT           : None            
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamCliSetPortMode (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4Mode)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2Dot3OamMode (&u4ErrorCode, i4IfIndex, i4Mode) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot3OamMode (i4IfIndex, i4Mode) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set ethernet-oam port mode\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliSetPortRemoteLBCmd                            
 *                                                                          
 *     DESCRIPTION      : This function will deny/permit the local loopback 
 *                        command per port
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number
 *                        i4Status  - deny/permit
 *                                                                          
 *     OUTPUT           : None            
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamCliSetPortRemoteLBCmd (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4Status)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2Dot3OamLoopbackIgnoreRx (&u4ErrorCode, i4IfIndex,
                                          i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot3OamLoopbackIgnoreRx (i4IfIndex, i4Status) == SNMP_FAILURE)
    {
        CliPrintf
            (CliHandle,
             "\r%% Unable to set ethernet-oam port local loopback command\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliSetPortRemoteLB                               
 *                                                                          
 *     DESCRIPTION      : This function will enable/disable remote loopback   
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number
 *                        i4Status  - enable/disable
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamCliSetPortRemoteLB (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4Status)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2Dot3OamLoopbackStatus (&u4ErrorCode, i4IfIndex,
                                        i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot3OamLoopbackStatus (i4IfIndex, i4Status) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to set ethernet-oam port remote loopback\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliSetSymbolPeriodNotif                          
 *                                                                          
 *     DESCRIPTION      : This function will enable/disable symbol period 
 *                        notification            
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number
 *                        i4Status  - enable/disable
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 *****************************************************************************/
PUBLIC INT4
EoamCliSetSymbolPeriodNotif (tCliHandle CliHandle, INT4 i4IfIndex,
                             INT4 i4Status)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2Dot3OamErrSymPeriodEvNotifEnable (&u4ErrorCode, i4IfIndex,
                                                   i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot3OamErrSymPeriodEvNotifEnable (i4IfIndex,
                                                i4Status) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set ethernet-oam symbol");
        CliPrintf (CliHandle, " period event notification\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/**************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliSetFrameNotif                                 
 *                                                                          
 *     DESCRIPTION      : This function will enable/disable frame  
 *                        notification            
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number
 *                        i4Status  - enable/disable
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ***************************************************************************/
PUBLIC INT4
EoamCliSetFrameNotif (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4Status)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2Dot3OamErrFrameEvNotifEnable (&u4ErrorCode, i4IfIndex,
                                               i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot3OamErrFrameEvNotifEnable (i4IfIndex, i4Status) ==
        SNMP_FAILURE)
    {
        CliPrintf
            (CliHandle,
             "\r%% Unable to set ethernet-oam frame event notification\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/**************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliSetFramePeriodNotif                           
 *                                                                          
 *     DESCRIPTION      : This function will enable/disable frame period 
 *                        notification            
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number
 *                        i4Status  - enable/disable
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ***************************************************************************/
PUBLIC INT4
EoamCliSetFramePeriodNotif (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4Status)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2Dot3OamErrFramePeriodEvNotifEnable (&u4ErrorCode, i4IfIndex,
                                                     i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot3OamErrFramePeriodEvNotifEnable (i4IfIndex,
                                                  i4Status) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set ethernet-oam frame period");
        CliPrintf (CliHandle, " event notification\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/**************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliSetFrameSecSummaryNotif                       
 *                                                                          
 *     DESCRIPTION      : This function will enable/disable frame seconds 
 *                        summary notification            
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number
 *                        i4Status  - enable/disable
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ***************************************************************************/
PUBLIC INT4
EoamCliSetFrameSecSummaryNotif (tCliHandle CliHandle, INT4 i4IfIndex,
                                INT4 i4Status)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2Dot3OamErrFrameSecsEvNotifEnable (&u4ErrorCode, i4IfIndex,
                                                   i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot3OamErrFrameSecsEvNotifEnable (i4IfIndex, i4Status)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set ethernet-oam frame seconds");
        CliPrintf (CliHandle, " summary event notification\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliSetSymbolPeriodWindow                         
 *                                                                          
 *     DESCRIPTION      : This function will set symbol period window        
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number
 *                        u4Window   - Window value
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamCliSetSymbolPeriodWindow (tCliHandle CliHandle, INT4 i4IfIndex,
                              FS_UINT8 u8Window)
{
    FS_UINT8            u8Threshold;
    UINT4               u4ErrorCode = 0;

    FSAP_U8_CLR (&u8Threshold);

    if (nmhTestv2Dot3OamErrSymPeriodWindowHi (&u4ErrorCode, i4IfIndex,
                                              FSAP_U8_FETCH_HI (&u8Window))
        == SNMP_FAILURE)
    {
        if (u4ErrorCode != SNMP_ERR_WRONG_VALUE)
        {
            return CLI_FAILURE;
        }
    }

    nmhGetDot3OamErrSymPeriodThresholdHi (i4IfIndex,
                                          &(FSAP_U8_FETCH_HI (&u8Threshold)));
    nmhGetDot3OamErrSymPeriodThresholdLo (i4IfIndex,
                                          &(FSAP_U8_FETCH_LO (&u8Threshold)));

    if (FSAP_U8_CMP (&u8Threshold, &u8Window) >= 0)
    {
        CLI_SET_ERR (EOAM_CLI_ERR_THRESH_EXCEEDS_WINDOW);
        return CLI_FAILURE;
    }

    if (nmhSetDot3OamErrSymPeriodWindowHi
        (i4IfIndex, FSAP_U8_FETCH_HI (&u8Window)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set window size for ");
        CliPrintf (CliHandle, "ethernet-oam symbol period\r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2Dot3OamErrSymPeriodWindowLo (&u4ErrorCode, i4IfIndex,
                                              FSAP_U8_FETCH_LO (&u8Window)) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot3OamErrSymPeriodWindowLo
        (i4IfIndex, FSAP_U8_FETCH_LO (&u8Window)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set window size for ");
        CliPrintf (CliHandle, "ethernet-oam symbol period\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliSetFrameWindow                                
 *                                                                          
 *     DESCRIPTION      : This function will set frame window        
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number
 *                        u4Window   - Window value
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamCliSetFrameWindow (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4Window)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2Dot3OamErrFrameWindow (&u4ErrorCode, i4IfIndex,
                                        u4Window) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot3OamErrFrameWindow (i4IfIndex, u4Window) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set window size for ");
        CliPrintf (CliHandle, "ethernet-oam frame\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliSetFramePeriodWindow                          
 *                                                                          
 *     DESCRIPTION      : This function will set frame period 
 *                        window        
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number
 *                        u4Window   - Window value
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamCliSetFramePeriodWindow (tCliHandle CliHandle, INT4 i4IfIndex,
                             UINT4 u4Window)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2Dot3OamErrFramePeriodWindow (&u4ErrorCode, i4IfIndex,
                                              u4Window) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot3OamErrFramePeriodWindow (i4IfIndex, u4Window) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set window size for ");
        CliPrintf (CliHandle, "ethernet-oam frame period\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliSetFrameSecSummaryWindow                      
 *                                                                          
 *     DESCRIPTION      : This function will set frame seconds summary 
 *                        window        
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number
 *                        u4Window   - Window value
 *
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ***************************************************************************/
PUBLIC INT4
EoamCliSetFrameSecSummaryWindow (tCliHandle CliHandle, INT4 i4IfIndex,
                                 UINT4 u4Window)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2Dot3OamErrFrameSecsSummaryWindow (&u4ErrorCode, i4IfIndex,
                                                   u4Window) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot3OamErrFrameSecsSummaryWindow (i4IfIndex, u4Window)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set window size for ");
        CliPrintf (CliHandle, "ethernet-oam frame seconds summary\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliSetSymbolPeriodThres                          
 *                                                                          
 *     DESCRIPTION      : This function will set symbol period 
 *                        threshold        
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number
 *                        u4Thres   - Threshold value
 *                                                                          
 *     OUTPUT           : None              
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamCliSetSymbolPeriodThres (tCliHandle CliHandle, INT4 i4IfIndex,
                             FS_UINT8 u8Thres)
{
    FS_UINT8            u8Window;
    UINT4               u4ErrorCode = 0;

    FSAP_U8_CLR (&u8Window);

    if (nmhTestv2Dot3OamErrSymPeriodThresholdHi (&u4ErrorCode, i4IfIndex,
                                                 FSAP_U8_FETCH_HI (&u8Thres))
        == SNMP_FAILURE)
    {
        if (u4ErrorCode != SNMP_ERR_WRONG_VALUE)
        {
            return CLI_FAILURE;
        }
    }

    nmhGetDot3OamErrSymPeriodWindowHi (i4IfIndex,
                                       &(FSAP_U8_FETCH_HI (&u8Window)));
    nmhGetDot3OamErrSymPeriodWindowLo (i4IfIndex,
                                       &(FSAP_U8_FETCH_LO (&u8Window)));

    if (FSAP_U8_CMP (&u8Thres, &u8Window) >= 0)
    {
        CLI_SET_ERR (EOAM_CLI_ERR_THRESHOLD_HIGH);
        return CLI_FAILURE;
    }

    if (nmhSetDot3OamErrSymPeriodThresholdHi (i4IfIndex,
                                              FSAP_U8_FETCH_HI (&u8Thres))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set threshold value for ");
        CliPrintf (CliHandle, "ethernet-oam symbol period\r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2Dot3OamErrSymPeriodThresholdLo (&u4ErrorCode, i4IfIndex,
                                                 FSAP_U8_FETCH_LO (&u8Thres))
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot3OamErrSymPeriodThresholdLo (i4IfIndex,
                                              FSAP_U8_FETCH_LO (&u8Thres))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set threshold value for ");
        CliPrintf (CliHandle, "ethernet-oam symbol period\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliSetFrameThres                                 
 *                                                                          
 *     DESCRIPTION      : This function will set frame
 *                        threshold        
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number
 *                        u4Thres   - Threshold value
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamCliSetFrameThres (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4Thres)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2Dot3OamErrFrameThreshold (&u4ErrorCode, i4IfIndex,
                                           u4Thres) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot3OamErrFrameThreshold (i4IfIndex, u4Thres) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set threshold value for ");
        CliPrintf (CliHandle, "ethernet-oam frame\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliSetFramePeriodThres                           
 *                                                                          
 *     DESCRIPTION      : This function will set frame period 
 *                        threshold        
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number
 *                        u4Thres   - Threshold value
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamCliSetFramePeriodThres (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4Thres)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2Dot3OamErrFramePeriodThreshold (&u4ErrorCode, i4IfIndex,
                                                 u4Thres) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot3OamErrFramePeriodThreshold (i4IfIndex, u4Thres)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set threshold value for ");
        CliPrintf (CliHandle, "ethernet-oam frame period\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliSetFrameSecSummaryThres                       
 *                                                                          
 *     DESCRIPTION      : This function will set frame seconds summary 
 *                        threshold        
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number
 *                        u4Thres   - Threshold value
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamCliSetFrameSecSummaryThres (tCliHandle CliHandle, INT4 i4IfIndex,
                                UINT4 u4Thres)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2Dot3OamErrFrameSecsSummaryThreshold (&u4ErrorCode, i4IfIndex,
                                                      u4Thres) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot3OamErrFrameSecsSummaryThreshold (i4IfIndex, u4Thres)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set threshold value for ");
        CliPrintf (CliHandle, "ethernet-oam frame seconds summary\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliSetCriticalEvent                              
 *                                                                          
 *     DESCRIPTION      : This function will enable/disable critical event 
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number
 *                        i4Status  - enable/disable
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamCliSetCriticalEvent (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4Status)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2Dot3OamCriticalEventEnable (&u4ErrorCode, i4IfIndex,
                                             i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot3OamCriticalEventEnable (i4IfIndex, i4Status) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to set ethernet-oam critical event\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliSetDyingGasp                                  
 *                                                                          
 *     DESCRIPTION      : This function will enable/disable the dying gasp          
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number
 *                        i4Status  - enable/disable
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamCliSetDyingGasp (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4Status)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2Dot3OamDyingGaspEnable (&u4ErrorCode, i4IfIndex,
                                         i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot3OamDyingGaspEnable (i4IfIndex, i4Status) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set ethernet-oam dying gasp\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : EoamCliSetLMStatus
 *
 *     DESCRIPTION      : This function will Enable or Disable Link Monitoring
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                               i4Status - Enable/Disable
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ****************************************************************************/

PUBLIC INT4
EoamCliSetLMStatus (tCliHandle CliHandle, INT4 i4Status)
{
    INT4                i4Result = CLI_SUCCESS;

    UNUSED_PARAM (CliHandle);

    if (i4Status == EOAM_LM_ENABLED)
    {
        if (gu1EoamLMStatFlag == EOAM_LM_DISABLED)
        {
            if (EoamLmMainInit () != OSIX_SUCCESS)
            {
                EOAM_TRC ((INIT_SHUT_TRC | ALL_FAILURE_TRC,
                           "EoamModuleStart: LM Init FAILED!!!\n"));
                i4Result = CLI_FAILURE;
            }
        }
        else
        {
            i4Result = CLI_FAILURE;
        }
    }
    else
    {
        if (gu1EoamLMStatFlag == EOAM_LM_ENABLED)
        {
            EoamLmMainShutdown ();
        }
        else
        {
            i4Result = CLI_FAILURE;
        }
    }
    return i4Result;
}

/***************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliClearStats                                 
 *                                                                          
 *     DESCRIPTION      : This function will clear the statistics            
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID 
 *                        i4IfIndex - Port Number                       
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamCliClearStats (tCliHandle CliHandle, INT4 i4IfIndex)
{
    tEoamPduStats      *pEoamPduStats = NULL;
    tEoamPortInfo      *pPortEntry = NULL;

    UNUSED_PARAM (CliHandle);

    /* For All Interface */
    if (i4IfIndex == 0)
    {
        for (i4IfIndex = EOAM_MIN_PORTS; i4IfIndex <= EOAM_MAX_PORTS;
             i4IfIndex++)
        {

            if (EoamUtilGetPortEntry ((UINT4) i4IfIndex,
                                      &pPortEntry) != OSIX_FAILURE)
            {
                pEoamPduStats = &(pPortEntry->PduStats);
                MEMSET (pEoamPduStats, 0, sizeof (tEoamPduStats));
            }
        }
    }
    else
    {
        /* For particular interface */
        if (EoamUtilGetPortEntry ((UINT4) i4IfIndex,
                                  &pPortEntry) != OSIX_FAILURE)
        {
            pEoamPduStats = &(pPortEntry->PduStats);
            MEMSET (pEoamPduStats, 0, sizeof (tEoamPduStats));
        }
    }

    return CLI_SUCCESS;

}

/***************************************************************************
 * 
 *     FUNCTION NAME    : EoamCliClearLocal                                    
 *                                                                          
 *     DESCRIPTION      : This function will resets the local configurations     *                                                                          
 *     INPUT            : CliHandle  - CliContext ID 
 *                        i4IfIndex - Port Number                       
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamCliClearLocal (tCliHandle CliHandle, INT4 i4IfIndex)
{
    UINT1               u1IsShowAll = OSIX_TRUE;
    INT4                i4PrevIfIndex = 0;
    INT4                i4RetVal = SNMP_SUCCESS;

    /* For All Interface */
    if (i4IfIndex == 0)
    {
        if (nmhGetFirstIndexDot3OamTable (&i4IfIndex) != SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }
    else
    {

        if (nmhValidateIndexInstanceDot3OamTable (i4IfIndex) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        /* For particular interface */
        u1IsShowAll = OSIX_FALSE;
    }

    do
    {
        /* i4RetVal specifies whether clear is success or not. 
         * If any of the clear fails the error message is displayed
         * accordingly */
        i4RetVal |= nmhSetDot3OamMode (i4IfIndex, EOAM_MODE_ACTIVE);
        i4RetVal |= nmhSetDot3OamErrSymPeriodWindowHi
            (i4IfIndex, EOAM_DEF_SYMBOL_PERIOD_WINDOW_HI);
        i4RetVal |= nmhSetDot3OamErrSymPeriodWindowLo
            (i4IfIndex, EOAM_DEF_SYMBOL_PERIOD_WINDOW_LO);
        i4RetVal |= nmhSetDot3OamErrFramePeriodWindow
            (i4IfIndex, EOAM_DEF_FRAME_PERIOD_WINDOW);
        i4RetVal |= nmhSetDot3OamErrFrameWindow
            (i4IfIndex, EOAM_DEF_FRAME_WINDOW);
        i4RetVal |= nmhSetDot3OamErrFrameSecsSummaryWindow
            (i4IfIndex, EOAM_DEF_FRAME_SECS_WINDOW);
        i4RetVal |= nmhSetDot3OamErrSymPeriodThresholdHi
            (i4IfIndex, EOAM_DEF_SYMBOL_PERIOD_THRESH_HI);
        i4RetVal |= nmhSetDot3OamErrSymPeriodThresholdLo
            (i4IfIndex, EOAM_DEF_SYMBOL_PERIOD_THRESH_LO);
        i4RetVal |= nmhSetDot3OamErrFramePeriodThreshold
            (i4IfIndex, EOAM_DEF_FRAME_PERIOD_THRESH);
        i4RetVal |= nmhSetDot3OamErrFrameThreshold
            (i4IfIndex, EOAM_DEF_FRAME_THRESH);
        i4RetVal |= nmhSetDot3OamErrFrameSecsSummaryThreshold
            (i4IfIndex, EOAM_DEF_FRAME_SECS_THRESH);
        i4RetVal |= nmhSetDot3OamErrSymPeriodEvNotifEnable
            (i4IfIndex, EOAM_TRUE);
        i4RetVal |= nmhSetDot3OamErrFrameEvNotifEnable (i4IfIndex, EOAM_TRUE);
        i4RetVal |= nmhSetDot3OamErrFramePeriodEvNotifEnable
            (i4IfIndex, EOAM_TRUE);
        i4RetVal |= nmhSetDot3OamErrFrameSecsEvNotifEnable
            (i4IfIndex, EOAM_TRUE);
        i4RetVal |= nmhSetDot3OamCriticalEventEnable (i4IfIndex, EOAM_TRUE);
        i4RetVal |= nmhSetDot3OamDyingGaspEnable (i4IfIndex, EOAM_TRUE);
        i4RetVal |= nmhSetDot3OamLoopbackIgnoreRx
            (i4IfIndex, EOAM_IGNORE_LB_CMD);
        i4RetVal |= nmhSetDot3OamAdminState (i4IfIndex, EOAM_DISABLED);

        if (u1IsShowAll == OSIX_FALSE)
        {
            break;
        }

        i4PrevIfIndex = i4IfIndex;

    }
    while (nmhGetNextIndexDot3OamTable (i4PrevIfIndex, &i4IfIndex)
           == SNMP_SUCCESS);

    if (i4RetVal != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Unable to clear the local info\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 *     
 *     FUNCTION NAME    : EoamCliClearEventLog                              
 *                                                                          
 *     DESCRIPTION      : This function will clear the event log            
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4IfIndex - Port Number                       
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
EoamCliClearEventLog (tCliHandle CliHandle, INT4 i4IfIndex)
{
    tEoamPortInfo      *pPortEntry = NULL;

    UNUSED_PARAM (CliHandle);

    /* For All Interface */
    if (i4IfIndex == 0)
    {
        for (i4IfIndex = EOAM_MIN_PORTS; i4IfIndex <= EOAM_MAX_PORTS;
             i4IfIndex++)
        {

            if (EoamUtilGetPortEntry ((UINT4) i4IfIndex,
                                      &pPortEntry) != OSIX_FAILURE)
            {
                EoamMainClearEvtLogs (pPortEntry);
            }

        }
    }
    else
    {
        /* For particular interface */
        if (EoamUtilGetPortEntry ((UINT4) i4IfIndex,
                                  &pPortEntry) != OSIX_FAILURE)
        {
            EoamMainClearEvtLogs (pPortEntry);
        }
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : EoamCliShowDebugging                                  
 *                                                                           
 *     DESCRIPTION      : This function prints the EOAM debug level          
 *                                                                           
 *     INPUT            : CliHandle  - CliContext ID                            
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            
 *                                                                           
 *****************************************************************************/
PUBLIC VOID
EoamCliShowDebugging (tCliHandle CliHandle)
{
    INT4                i4DbgLevel = 0;
    INT4                i4Status = 0;

    nmhGetFsEoamSystemControl (&i4Status);

    if (i4Status == EOAM_SHUTDOWN)
    {
        return;
    }

    nmhGetFsEoamTraceOption (&i4DbgLevel);

    if (i4DbgLevel == 0)
    {
        return;
    }

    CliPrintf (CliHandle, "\rEthernet OAM :");

    if ((i4DbgLevel & INIT_SHUT_TRC) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  Ethernet OAM init and shutdown debugging is on");
    }

    if ((i4DbgLevel & MGMT_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  Ethernet OAM management debugging is on");
    }

    if ((i4DbgLevel & CONTROL_PLANE_TRC) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  Ethernet OAM control plane debugging is on");
    }

    if ((i4DbgLevel & DUMP_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  Ethernet OAM packet dump debugging is on");
    }

    if ((i4DbgLevel & OS_RESOURCE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  Ethernet OAM resources debugging is on");
    }

    if ((i4DbgLevel & ALL_FAILURE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  Ethernet OAM error debugging is on");
    }

    if ((i4DbgLevel & BUFFER_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  Ethernet OAM buffer debugging is on");
    }

    if ((i4DbgLevel & EOAM_CRITICAL_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  Ethernet OAM critical debugging is on");
    }

    if ((i4DbgLevel & EOAM_DISCOVERY_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  Ethernet OAM discovery debugging is on");
    }

    if ((i4DbgLevel & EOAM_LOOPBACK_TRC) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  Ethernet OAM remote loopback debugging is on");
    }

    if ((i4DbgLevel & EOAM_LM_TRC) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  Ethernet OAM link monitoring debugging is on");
    }

    if ((i4DbgLevel & EOAM_VAR_REQRESP_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  Ethernet OAM MIB variable ");
        CliPrintf (CliHandle, "request/response debugging is on");
    }

    if ((i4DbgLevel & EOAM_RFI_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  Ethernet OAM remote failure indication");
        CliPrintf (CliHandle, " debugging is on");
    }

    if ((i4DbgLevel & EOAM_FN_ENTRY_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  Ethernet OAM function entry");
        CliPrintf (CliHandle, " debugging is on");
    }

    if ((i4DbgLevel & EOAM_FN_EXIT_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  Ethernet OAM function exit");
        CliPrintf (CliHandle, " debugging is on");
    }

    if ((i4DbgLevel & EOAM_MUX_PAR_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  Ethernet OAM multiplexer parser");
        CliPrintf (CliHandle, " debugging is on");
    }

    if ((i4DbgLevel & EOAM_RED_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  Ethernet OAM redundancy");
        CliPrintf (CliHandle, " debugging is on");
    }

    CliPrintf (CliHandle, "\r\n");

    return;
}

/****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : EoamCliShowRunningConfig                              
 *                                                                           
 *     DESCRIPTION      : This function displays current EOAM configurations    
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                             
 *                        u4Module - Specified Module (EOAM/all)  
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 *****************************************************************************/
PUBLIC VOID
EoamCliShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module)
{

    EoamCliShowRunningConfigGlobal (CliHandle);

    if (u4Module == ISS_EOAM_SHOW_RUNNING_CONFIG)
    {
        EoamCliShowRunningConfigIntf (CliHandle);
    }

    return;

}

/***************************************************************************
 *                                                                           
 *     FUNCTION NAME    : EoamCliShowRunningConfigGlobal                        
 *                                                                           
 *     DESCRIPTION      : This function displays the current global          
 *                        configuration information of EOAM                  
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                          
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 ****************************************************************************/
PUBLIC VOID
EoamCliShowRunningConfigGlobal (tCliHandle CliHandle)
{
    UINT1               au1RetOuiVal[EOAM_OUI_LENGTH];
    UINT1               au1OuiVal[EOAM_CLI_OUI_STR_LEN];
    UINT4               u4ResendCnt = 0;
    INT4                i4CtrlStatus = 0;
    INT4                i4ModStatus = 0;
    tSNMP_OCTET_STRING_TYPE RetOui;
    tMacAddr            MacAddr;

    CliRegisterLock (CliHandle, EoamLock, EoamUnLock);
    EoamLock ();

    MEMSET (au1RetOuiVal, 0, EOAM_OUI_LENGTH);
    MEMSET (MacAddr, 0, MAC_ADDR_LEN);
    MEMSET (&RetOui, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    RetOui.pu1_OctetList = au1RetOuiVal;
    RetOui.i4_Length = EOAM_OUI_LENGTH;

    nmhGetFsEoamSystemControl (&i4CtrlStatus);
    nmhGetFsEoamModuleStatus (&i4ModStatus);
    nmhGetFsEoamOui (&RetOui);
    nmhGetFsEoamErrorEventResend (&u4ResendCnt);

    if (i4CtrlStatus == EOAM_SHUTDOWN)
    {
        CliPrintf (CliHandle, "shutdown ethernet-oam\r\n");
    }
    else
    {

        if (i4ModStatus != EOAM_DISABLED)
        {
            CliPrintf (CliHandle, "set ethernet-oam enable\r\n");
        }

        EoamGetSysMacAddress (MacAddr);
        MEMSET (au1OuiVal, 0, EOAM_CLI_OUI_STR_LEN);

        if (MEMCMP (RetOui.pu1_OctetList, &MacAddr, EOAM_OUI_LENGTH) != 0)
        {
            /* Convert the OUI to printable format like 00:01:02 */
            CliOctetToStr (RetOui.pu1_OctetList,
                           RetOui.i4_Length, au1OuiVal, EOAM_CLI_OUI_STR_LEN);
            CliPrintf (CliHandle, "set ethernet-oam oui %s\r\n", au1OuiVal);
        }

        if (u4ResendCnt != EOAM_ERR_EVT_RESEND_COUNT)
        {
            CliPrintf (CliHandle,
                       "ethernet-oam link-monitor event-resend %u\r\n",
                       u4ResendCnt);
        }

        if (gu1EoamLMStatFlag == EOAM_LM_DISABLED)
        {
            CliPrintf (CliHandle, "ethernet-oam link-monitor set disable \r\n");
        }

    }
    EoamUnLock ();
    CliUnRegisterLock (CliHandle);

    return;
}

/****************************************************************************
 *                                                                           
 *     FUNCTION         : EoamCliShowRunningConfigIntf                     
 *                                                                           
 *     DESCRIPTION      : This function displays the current interface       
 *                        configuration information of EOAM for all          
 *                        interfaces                                         
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                          
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 ****************************************************************************/
PUBLIC VOID
EoamCliShowRunningConfigIntf (tCliHandle CliHandle)
{
    BOOL1               b1FlgModSpec = OSIX_TRUE;
    UINT4               i4IfIndex = 0;

    for (i4IfIndex = EOAM_MIN_PORTS; i4IfIndex <= EOAM_MAX_PORTS; i4IfIndex++)
    {
        EoamCliShowRunningConfigIfInfo (CliHandle, i4IfIndex, b1FlgModSpec);
    }

    return;
}

/****************************************************************************
 *                                                                           
 *     FUNCTION         : EoamCliShowRunningConfigIfInfo              
 *                                                                           
 *     DESCRIPTION      : This function displays the current interface       
 *                        configuration information of EOAM module for       
 *                        particular interface                               
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                          
 *                        i4IfIndex - Port Number
 *                        b1FlgModSpec - Flag to specify module specific 
 *                        ShowRunningConfig or not
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 ****************************************************************************/
PUBLIC VOID
EoamCliShowRunningConfigIfInfo (tCliHandle CliHandle, INT4 i4IfIndex,
                                BOOL1 b1FlgModSpec)
{
    UINT1               u1DispFlag = OSIX_FALSE;

    CliRegisterLock (CliHandle, EoamLock, EoamUnLock);
    EoamLock ();

    if (nmhValidateIndexInstanceDot3OamTable (i4IfIndex) != SNMP_FAILURE)
    {
        EoamCliSrcOamTable (CliHandle, i4IfIndex, b1FlgModSpec, &u1DispFlag);
    }

    if (nmhValidateIndexInstanceDot3OamLoopbackTable (i4IfIndex) !=
        SNMP_FAILURE)
    {
        EoamCliSrcOamLBTable (CliHandle, i4IfIndex, b1FlgModSpec, &u1DispFlag);
    }

    if (nmhValidateIndexInstanceDot3OamEventConfigTable (i4IfIndex)
        != SNMP_FAILURE)
    {
        EoamCliSrcSymbolPeriod (CliHandle, i4IfIndex, b1FlgModSpec,
                                &u1DispFlag);
        EoamCliSrcFramePeriod (CliHandle, i4IfIndex, b1FlgModSpec, &u1DispFlag);
        EoamCliSrcFrame (CliHandle, i4IfIndex, b1FlgModSpec, &u1DispFlag);
        EoamCliSrcFrameSecsSummary (CliHandle, i4IfIndex,
                                    b1FlgModSpec, &u1DispFlag);
        EoamCliSrcFaultIndication (CliHandle, i4IfIndex, b1FlgModSpec,
                                   &u1DispFlag);
    }

    if ((b1FlgModSpec == OSIX_TRUE) && (u1DispFlag == OSIX_TRUE))
    {
        CliPrintf (CliHandle, "!\r\n");
    }

    EoamUnLock ();
    CliUnRegisterLock (CliHandle);
    return;
}

/****************************************************************************
 *                                                                           
 *     FUNCTION         : EoamCliSrcOamTable              
 *                                                                           
 *     DESCRIPTION      : This function displays the show running config
 *                        for OamTable                          
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                          
 *                        i4IfIndex - Port Number
 *                        b1FlgModSpec - Flag to specify module specific 
 *                        ShowRunningConfig or not
 *                        pu1DispFlag - Pointer to the flag which specifies 
 *                        whether the port name is already displayed or not
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 ****************************************************************************/
PUBLIC VOID
EoamCliSrcOamTable (tCliHandle CliHandle, INT4 i4IfIndex,
                    BOOL1 b1FlgModSpec, UINT1 *pu1DispFlag)
{
    INT4                i4RetVal = 0;

    nmhGetDot3OamAdminState (i4IfIndex, &i4RetVal);

    if (i4RetVal != EOAM_SNMP_DISABLED)
    {
        if (EoamCliSrcPrintPortName (CliHandle, i4IfIndex, b1FlgModSpec,
                                     pu1DispFlag) == CLI_SUCCESS)
        {
            CliPrintf (CliHandle, " ethernet-oam enable\r\n");
        }
    }

    nmhGetDot3OamMode (i4IfIndex, &i4RetVal);

    if (i4RetVal != EOAM_MODE_ACTIVE)
    {
        if (EoamCliSrcPrintPortName (CliHandle, i4IfIndex, b1FlgModSpec,
                                     pu1DispFlag) == CLI_SUCCESS)
        {
            CliPrintf (CliHandle, " ethernet-oam mode passive\r\n");
        }
    }

    return;
}

/****************************************************************************
 *                                                                           
 *     FUNCTION         : EoamCliSrcOamLBTable              
 *                                                                           
 *     DESCRIPTION      : This function displays the show running config
 *                        for loopback table                          
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                          
 *                        i4IfIndex - Port Number
 *                        b1FlgModSpec - Flag to specify module specific 
 *                        ShowRunningConfig or not
 *                        pu1DispFlag - Pointer to the flag which specifies 
 *                        whether the port name is already displayed or not
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 ****************************************************************************/
PUBLIC VOID
EoamCliSrcOamLBTable (tCliHandle CliHandle, INT4 i4IfIndex,
                      BOOL1 b1FlgModSpec, UINT1 *pu1DispFlag)
{
    INT4                i4RetVal = 0;

    nmhGetDot3OamLoopbackIgnoreRx (i4IfIndex, &i4RetVal);

    if (i4RetVal != EOAM_IGNORE_LB_CMD)
    {
        if (EoamCliSrcPrintPortName (CliHandle, i4IfIndex, b1FlgModSpec,
                                     pu1DispFlag) == CLI_SUCCESS)
        {
            CliPrintf (CliHandle, " ethernet-oam remote-loopback permit\r\n");
        }
    }

    nmhGetDot3OamLoopbackStatus (i4IfIndex, &i4RetVal);

    if ((i4RetVal != EOAM_NO_LOOPBACK) && (i4RetVal != EOAM_LOCAL_LOOPBACK))
    {
        if (EoamCliSrcPrintPortName (CliHandle, i4IfIndex, b1FlgModSpec,
                                     pu1DispFlag) == CLI_SUCCESS)
        {
            CliPrintf (CliHandle, "ethernet-oam remote-loopback enable\r\n");
        }
    }

    return;
}

/****************************************************************************
 *                                                                           
 *     FUNCTION         : EoamCliSrcSymbolPeriod              
 *                                                                           
 *     DESCRIPTION      : This function displays the show running config
 *                        for symbol period event                          
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                          
 *                        i4IfIndex - Port Number
 *                        b1FlgModSpec - Flag to specify module specific 
 *                        ShowRunningConfig or not
 *                        pu1DispFlag - Pointer to the flag which specifies 
 *                        whether the port name is already displayed or not
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None 
 *                                                                           
 ****************************************************************************/
PUBLIC VOID
EoamCliSrcSymbolPeriod (tCliHandle CliHandle, INT4 i4IfIndex,
                        BOOL1 b1FlgModSpec, UINT1 *pu1DispFlag)
{
    UINT1               au1Str[EOAM_CLI_U8_STR_LEN];
    UINT4               u4Hi = 0;
    UINT4               u4Lo = 0;
    INT4                i4RetVal = 0;
    FS_UINT8            u8Thresh;
    FS_UINT8            u8Window;
    FS_UINT8            u8Ret;

    FSAP_U8_CLR (&u8Thresh);
    FSAP_U8_CLR (&u8Ret);
    FSAP_U8_CLR (&u8Window);

    nmhGetDot3OamErrSymPeriodWindowHi (i4IfIndex, &u4Hi);
    nmhGetDot3OamErrSymPeriodWindowLo (i4IfIndex, &u4Lo);

    FSAP_U8_ASSIGN_LO (&u8Window, u4Lo);
    FSAP_U8_ASSIGN_HI (&u8Window, u4Hi);
    MEMSET (au1Str, 0, EOAM_CLI_U8_STR_LEN);
    EOAM_CLI_CONV_U8_TO_MILLIONS (&u8Window, &u8Ret);
    FSAP_U8_2STR (&u8Window, (CHR1 *) au1Str);

    if ((u4Hi != EOAM_DEF_SYMBOL_PERIOD_WINDOW_HI) ||
        (u4Lo != EOAM_DEF_SYMBOL_PERIOD_WINDOW_LO))
    {
        /* Prints the port name if it is not previously displayed */
        if (EoamCliSrcPrintPortName (CliHandle, i4IfIndex, b1FlgModSpec,
                                     pu1DispFlag) == CLI_SUCCESS)
        {
            CliPrintf (CliHandle, " ethernet-oam link-monitor ");
            CliPrintf (CliHandle, "symbol-period window %s", au1Str);
            /* The window size is less than million, 
             * displaying the exact value , 
             * instead of approximating to value zero.
             * This value cannot be configure via CLI/WEB can be configured 
             * via SNMP */
            if ((FSAP_U8_FETCH_LO (&u8Window) == 0)
                && (FSAP_U8_FETCH_HI (&u8Window) == 0))
            {
                CliPrintf (CliHandle, ".%06d", u8Ret.u4Lo);
            }
            CliPrintf (CliHandle, "\r\n");
        }
    }

    nmhGetDot3OamErrSymPeriodThresholdHi (i4IfIndex, &u4Hi);
    nmhGetDot3OamErrSymPeriodThresholdLo (i4IfIndex, &u4Lo);

    FSAP_U8_ASSIGN_LO (&u8Thresh, u4Lo);
    FSAP_U8_ASSIGN_HI (&u8Thresh, u4Hi);
    MEMSET (au1Str, 0, EOAM_CLI_U8_STR_LEN);
    FSAP_U8_2STR (&u8Thresh, (CHR1 *) au1Str);

    if ((u4Hi != EOAM_DEF_SYMBOL_PERIOD_THRESH_HI) ||
        (u4Lo != EOAM_DEF_SYMBOL_PERIOD_THRESH_LO))
    {
        if (EoamCliSrcPrintPortName (CliHandle, i4IfIndex, b1FlgModSpec,
                                     pu1DispFlag) == CLI_SUCCESS)
        {
            CliPrintf (CliHandle, " ethernet-oam link-monitor symbol-period");
            CliPrintf (CliHandle, " threshold %s\r\n", au1Str);
        }
    }

    nmhGetDot3OamErrSymPeriodEvNotifEnable (i4IfIndex, &i4RetVal);

    if (i4RetVal != EOAM_TRUE)
    {
        if (EoamCliSrcPrintPortName (CliHandle, i4IfIndex, b1FlgModSpec,
                                     pu1DispFlag) == CLI_SUCCESS)
        {
            CliPrintf (CliHandle,
                       " ethernet-oam link-monitor symbol-period disable\r\n");
        }
    }

    return;
}

/****************************************************************************
 *                                                                           
 *     FUNCTION         : EoamCliSrcFramePeriod              
 *                                                                           
 *     DESCRIPTION      : This function displays the show running config
 *                        for frame period event                          
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                          
 *                        i4IfIndex - Port Number
 *                        b1FlgModSpec - Flag to specify module specific 
 *                        ShowRunningConfig or not
 *                        pu1DispFlag - Pointer to the flag which specifies 
 *                        whether the port name is already displayed or not
 *                                                                           
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None 
 *                                                                           
 ****************************************************************************/
PUBLIC VOID
EoamCliSrcFramePeriod (tCliHandle CliHandle, INT4 i4IfIndex,
                       BOOL1 b1FlgModSpec, UINT1 *pu1DispFlag)
{
    UINT4               u4Thresh = 0;
    UINT4               u4Window = 0;
    INT4                i4RetVal = 0;

    nmhGetDot3OamErrFramePeriodWindow (i4IfIndex, &u4Window);
    nmhGetDot3OamErrFramePeriodThreshold (i4IfIndex, &u4Thresh);
    nmhGetDot3OamErrFramePeriodEvNotifEnable (i4IfIndex, &i4RetVal);

    if (u4Window != EOAM_DEF_FRAME_PERIOD_WINDOW)
    {
        if (EoamCliSrcPrintPortName (CliHandle, i4IfIndex, b1FlgModSpec,
                                     pu1DispFlag) == CLI_SUCCESS)
        {
            CliPrintf (CliHandle,
                       " ethernet-oam link-monitor frame-period window %u\r\n",
                       u4Window);
        }
    }

    if (u4Thresh != EOAM_DEF_FRAME_PERIOD_THRESH)
    {
        if (EoamCliSrcPrintPortName (CliHandle, i4IfIndex, b1FlgModSpec,
                                     pu1DispFlag) == CLI_SUCCESS)
        {
            CliPrintf
                (CliHandle,
                 " ethernet-oam link-monitor frame-period threshold %u\r\n",
                 u4Thresh);
        }
    }

    if (i4RetVal != EOAM_TRUE)
    {
        if (EoamCliSrcPrintPortName (CliHandle, i4IfIndex, b1FlgModSpec,
                                     pu1DispFlag) == CLI_SUCCESS)
        {
            CliPrintf (CliHandle,
                       " ethernet-oam link-monitor frame-period disable\r\n");
        }
    }

    return;
}

/****************************************************************************
 *                                                                           
 *     FUNCTION         : EoamCliSrcFrame              
 *                                                                           
 *     DESCRIPTION      : This function displays the show running config
 *                        for frame event                          
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                          
 *                        i4IfIndex - Port Number
 *                        b1FlgModSpec - Flag to specify module specific 
 *                        ShowRunningConfig or not
 *                        pu1DispFlag - Pointer to the flag which specifies 
 *                        whether the port name is already displayed or not
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 ****************************************************************************/
PUBLIC VOID
EoamCliSrcFrame (tCliHandle CliHandle, INT4 i4IfIndex,
                 BOOL1 b1FlgModSpec, UINT1 *pu1DispFlag)
{
    UINT4               u4Thresh = 0;
    UINT4               u4Window = 0;
    INT4                i4RetVal = 0;

    nmhGetDot3OamErrFrameWindow (i4IfIndex, &u4Window);
    nmhGetDot3OamErrFrameThreshold (i4IfIndex, &u4Thresh);
    nmhGetDot3OamErrFrameEvNotifEnable (i4IfIndex, &i4RetVal);

    if (u4Window != EOAM_DEF_FRAME_WINDOW)
    {
        if (EoamCliSrcPrintPortName (CliHandle, i4IfIndex, b1FlgModSpec,
                                     pu1DispFlag) == CLI_SUCCESS)
        {
            CliPrintf (CliHandle,
                       " ethernet-oam link-monitor frame window %u\r\n",
                       u4Window);
        }
    }

    if (u4Thresh != EOAM_DEF_FRAME_THRESH)
    {
        if (EoamCliSrcPrintPortName (CliHandle, i4IfIndex, b1FlgModSpec,
                                     pu1DispFlag) == CLI_SUCCESS)
        {
            CliPrintf (CliHandle,
                       " ethernet-oam link-monitor frame threshold %u\r\n",
                       u4Thresh);
        }
    }

    if (i4RetVal != EOAM_TRUE)
    {
        if (EoamCliSrcPrintPortName (CliHandle, i4IfIndex, b1FlgModSpec,
                                     pu1DispFlag) == CLI_SUCCESS)
        {
            CliPrintf (CliHandle,
                       " ethernet-oam link-monitor frame disable\r\n");
        }
    }

    return;
}

/****************************************************************************
 *                                                                           
 *     FUNCTION         : EoamCliSrcFrameSecsSummary              
 *                                                                           
 *     DESCRIPTION      : This function displays the show running config
 *                        for frame secs summary event                          
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                          
 *                        i4IfIndex - Port Number
 *                        b1FlgModSpec - Flag to specify module specific 
 *                        ShowRunningConfig or not
 *                        pu1DispFlag - Pointer to the flag which specifies 
 *                        whether the port name is already displayed or not
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 ****************************************************************************/
PUBLIC VOID
EoamCliSrcFrameSecsSummary (tCliHandle CliHandle, INT4 i4IfIndex,
                            BOOL1 b1FlgModSpec, UINT1 *pu1DispFlag)
{
    INT4                i4Thresh = 0;
    INT4                i4Window = 0;
    INT4                i4RetVal = 0;

    nmhGetDot3OamErrFrameSecsSummaryWindow (i4IfIndex, &i4Window);
    nmhGetDot3OamErrFrameSecsSummaryThreshold (i4IfIndex, &i4Thresh);
    nmhGetDot3OamErrFrameSecsEvNotifEnable (i4IfIndex, &i4RetVal);

    if (i4Window != EOAM_DEF_FRAME_SECS_WINDOW)
    {
        if (EoamCliSrcPrintPortName (CliHandle, i4IfIndex, b1FlgModSpec,
                                     pu1DispFlag) == CLI_SUCCESS)
        {
            CliPrintf
                (CliHandle,
                 " ethernet-oam link-monitor frame-sec-summary window %d\r\n",
                 i4Window);
        }
    }

    if (i4Thresh != EOAM_DEF_FRAME_SECS_THRESH)
    {
        if (EoamCliSrcPrintPortName (CliHandle, i4IfIndex, b1FlgModSpec,
                                     pu1DispFlag) == CLI_SUCCESS)
        {
            CliPrintf (CliHandle, " ethernet-oam link-monitor");
            CliPrintf (CliHandle, " frame-sec-summary threshold %d\r\n",
                       i4Thresh);
        }
    }

    if (i4RetVal != EOAM_TRUE)
    {
        if (EoamCliSrcPrintPortName (CliHandle, i4IfIndex, b1FlgModSpec,
                                     pu1DispFlag) == CLI_SUCCESS)
        {
            CliPrintf (CliHandle, " ethernet-oam link-monitor");
            CliPrintf (CliHandle, " frame-sec-summary disable\r\n");
        }
    }

    return;
}

/****************************************************************************
 *                                                                           
 *     FUNCTION         : EoamCliSrcFaultIndication              
 *                                                                           
 *     DESCRIPTION      : This function displays the show running config
 *                        for dying gasp and critical event
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                          
 *                        i4IfIndex - Port Number
 *                        b1FlgModSpec - Flag to specify module specific 
 *                        ShowRunningConfig or not
 *                        pu1DispFlag - Pointer to the flag which specifies 
 *                        whether the port name is already displayed or not
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 ****************************************************************************/
PUBLIC VOID
EoamCliSrcFaultIndication (tCliHandle CliHandle, INT4 i4IfIndex,
                           BOOL1 b1FlgModSpec, UINT1 *pu1DispFlag)
{
    INT4                i4RetVal = 0;

    nmhGetDot3OamDyingGaspEnable (i4IfIndex, &i4RetVal);

    if (i4RetVal != EOAM_TRUE)
    {
        if (EoamCliSrcPrintPortName (CliHandle, i4IfIndex, b1FlgModSpec,
                                     pu1DispFlag) == CLI_SUCCESS)
        {
            CliPrintf (CliHandle, " ethernet-oam dying-gasp disable\r\n");
        }
    }

    nmhGetDot3OamCriticalEventEnable (i4IfIndex, &i4RetVal);

    if (i4RetVal != EOAM_TRUE)
    {
        if (EoamCliSrcPrintPortName (CliHandle, i4IfIndex, b1FlgModSpec,
                                     pu1DispFlag) == CLI_SUCCESS)
        {
            CliPrintf (CliHandle, " ethernet-oam critical-event disable\r\n");
        }
    }

    return;
}

/****************************************************************************
 *                                                                           
 *     FUNCTION         : EoamCliSrcPrintPortName              
 *                                                                           
 *     DESCRIPTION      : This function displays the port name for the        
 *                        particular interface if it is not 
 *                        previously displayed
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                         
 *                        i4IfIndex - Port Number
 *                        b1FlgModSpec - Flag to specify module specific 
 *                        ShowRunningConfig or not
 *                        pu1DispFlag - Pointer to the flag which specifies 
 *                        whether the port name is already displayed or not
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            
 *                                                                           
 ****************************************************************************/
PUBLIC INT4
EoamCliSrcPrintPortName (tCliHandle CliHandle, INT4 i4IfIndex,
                         BOOL1 b1FlgModSpec, UINT1 *pu1DispFlag)
{
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];

    MEMSET (au1NameStr, 0, CFA_MAX_PORT_NAME_LENGTH);

    if ((b1FlgModSpec == OSIX_TRUE) && (*(pu1DispFlag) == OSIX_FALSE))
    {
        if (CfaCliConfGetIfName
            ((UINT4) i4IfIndex, (INT1 *) au1NameStr) != CFA_SUCCESS)
        {
            return CLI_FAILURE;
        }

        CliPrintf (CliHandle, "interface %s\r\n", au1NameStr);
        *pu1DispFlag = OSIX_TRUE;
    }

    return CLI_SUCCESS;
}

#endif /* EMCLI_C */

/*--------------------------------------------------------------------------*/
/*                         End of the file emcli.c                          */
/*--------------------------------------------------------------------------*/
