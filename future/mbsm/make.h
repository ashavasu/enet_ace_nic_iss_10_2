
#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                               |
# |                                                                          |
# |   MAKE TOOL(S) USED      : GNU make                                      |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 12/01/2005                                    |
# |                                                                          |
# |   DESCRIPTION            : Specifies the options and modules to be       |
# |                            including for building the MBSM               |
# |                                                                          |
# |                                                                          |
# +--------------------------------------------------------------------------+
#
#     CHANGE RECORD :
# +--------------------------------------------------------------------------+
# | VERSION | AUTHOR/    | DESCRIPTION OF CHANGE                             |
# |         | DATE       |                                                   |
# +---------|------------|---------------------------------------------------+
# |   1     | 12/01/2005 | Creation of makefile                              |
# +--------------------------------------------------------------------------+

MBSM_BASE_DIR    = $(BASE_DIR)/mbsm

MBSM_INCD        = ${MBSM_BASE_DIR}/inc
MBSM_SRCD        = ${MBSM_BASE_DIR}/src
MBSM_OBJD        = ${MBSM_BASE_DIR}/obj
MBSM_EXED        = ${BASE_DIR}/mbsm/obj

# module includes
FUTURE_INC      = $(BASE_DIR)/inc

###############
# compilation #
###############
PROJECT_COMPILATION_SWITCHES = -D_POSIX_SOURCE \
                               -DSPIM_SM_CLI
                                
PROJECT_FINAL_INCLUDE_DIRS    = -I$(MBSM_INCD) \
                                -I$(FUTURE_INC)

PROJECT_FINAL_INCLUDE_DIRS += $(COMMON_INCLUDE_DIRS)

ISS_C_FLAGS                   = $(CC_FLAGS) \
                                $(GENERAL_COMPILATION_SWITCHES) \
                                ${PROJECT_COMPILATION_SWITCHES} \
                                ${SYSTEM_COMPILATION_SWITCHES} \
                                ${PROJECT_FINAL_INCLUDE_DIRS}

###########
# linking #
###########
LDR                           = ${LINKER} ${LINK_FLAGS}


# Specify the project include directories and dependencies
PROJECT_FINAL_INCLUDE_FILES  = $(MBSM_INCD)/mbsdefs.h

################
# Dependencies #
################

PROJECT_INTERNAL_DEPENDENCIES = \
            $(PROJECT_FINAL_INCLUDE_FILES) \
            $(MBSM_BASE_DIR)/make.h \
            $(MBSM_BASE_DIR)/Makefile 
            
