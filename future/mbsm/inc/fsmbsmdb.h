/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmbsmdb.h,v 1.5 2008/08/20 15:13:39 iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMBSMDB_H
#define _FSMBSMDB_H

UINT1 MbsmSlotModuleMapTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 MbsmLCTypeTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 MbsmLCPortInfoTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 MbsmLCConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};

UINT4 fsmbsm [] ={1,3,6,1,4,1,2076,81,100};
tSNMP_OID_TYPE fsmbsmOID = {9, fsmbsm};


UINT4 MbsmMaxNumOfLCSlots [ ] ={1,3,6,1,4,1,2076,81,100,1,1};
UINT4 MbsmMaxNumOfSlots [ ] ={1,3,6,1,4,1,2076,81,100,1,2};
UINT4 MbsmMaxNumOfPortsPerLC [ ] ={1,3,6,1,4,1,2076,81,100,1,3};
UINT4 MbsmLoadSharingFlag [ ] ={1,3,6,1,4,1,2076,81,100,1,4};
UINT4 MbsmSlotId [ ] ={1,3,6,1,4,1,2076,81,100,2,1,1};
UINT4 MbsmSlotModuleType [ ] ={1,3,6,1,4,1,2076,81,100,2,1,2};
UINT4 MbsmSlotModuleStatus [ ] ={1,3,6,1,4,1,2076,81,100,2,1,3};
UINT4 MbsmLCIndex [ ] ={1,3,6,1,4,1,2076,81,100,3,1,1};
UINT4 MbsmLCName [ ] ={1,3,6,1,4,1,2076,81,100,3,1,2};
UINT4 MbsmLCMaxPorts [ ] ={1,3,6,1,4,1,2076,81,100,3,1,3};
UINT4 MbsmLCRowStatus [ ] ={1,3,6,1,4,1,2076,81,100,3,1,4};
UINT4 MbsmLCPortIndex [ ] ={1,3,6,1,4,1,2076,81,100,4,1,1};
UINT4 MbsmLCPortIfType [ ] ={1,3,6,1,4,1,2076,81,100,4,1,2};
UINT4 MbsmLCPortSpeed [ ] ={1,3,6,1,4,1,2076,81,100,4,1,3};
UINT4 MbsmLCPortHighSpeed [ ] ={1,3,6,1,4,1,2076,81,100,4,1,4};
UINT4 MbsmLCConfigSlotId [ ] ={1,3,6,1,4,1,2076,81,100,5,1,1};
UINT4 MbsmLCConfigCardName [ ] ={1,3,6,1,4,1,2076,81,100,5,1,2};
UINT4 MbsmLCConfigStatus [ ] ={1,3,6,1,4,1,2076,81,100,5,1,3};


tMbDbEntry fsmbsmMibEntry[]= {

{{11,MbsmMaxNumOfLCSlots}, NULL, MbsmMaxNumOfLCSlotsGet, MbsmMaxNumOfLCSlotsSet, MbsmMaxNumOfLCSlotsTest, MbsmMaxNumOfLCSlotsDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,MbsmMaxNumOfSlots}, NULL, MbsmMaxNumOfSlotsGet, MbsmMaxNumOfSlotsSet, MbsmMaxNumOfSlotsTest, MbsmMaxNumOfSlotsDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,MbsmMaxNumOfPortsPerLC}, NULL, MbsmMaxNumOfPortsPerLCGet, MbsmMaxNumOfPortsPerLCSet, MbsmMaxNumOfPortsPerLCTest, MbsmMaxNumOfPortsPerLCDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,MbsmLoadSharingFlag}, NULL, MbsmLoadSharingFlagGet, MbsmLoadSharingFlagSet, MbsmLoadSharingFlagTest, MbsmLoadSharingFlagDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{12,MbsmSlotId}, GetNextIndexMbsmSlotModuleMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, MbsmSlotModuleMapTableINDEX, 1, 0, 0, NULL},

{{12,MbsmSlotModuleType}, GetNextIndexMbsmSlotModuleMapTable, MbsmSlotModuleTypeGet, MbsmSlotModuleTypeSet, MbsmSlotModuleTypeTest, MbsmSlotModuleMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MbsmSlotModuleMapTableINDEX, 1, 0, 0, NULL},

{{12,MbsmSlotModuleStatus}, GetNextIndexMbsmSlotModuleMapTable, MbsmSlotModuleStatusGet, MbsmSlotModuleStatusSet, MbsmSlotModuleStatusTest, MbsmSlotModuleMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MbsmSlotModuleMapTableINDEX, 1, 0, 1, NULL},

{{12,MbsmLCIndex}, GetNextIndexMbsmLCTypeTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, MbsmLCTypeTableINDEX, 1, 0, 0, NULL},

{{12,MbsmLCName}, GetNextIndexMbsmLCTypeTable, MbsmLCNameGet, MbsmLCNameSet, MbsmLCNameTest, MbsmLCTypeTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, MbsmLCTypeTableINDEX, 1, 0, 0, NULL},

{{12,MbsmLCMaxPorts}, GetNextIndexMbsmLCTypeTable, MbsmLCMaxPortsGet, MbsmLCMaxPortsSet, MbsmLCMaxPortsTest, MbsmLCTypeTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, MbsmLCTypeTableINDEX, 1, 0, 0, NULL},

{{12,MbsmLCRowStatus}, GetNextIndexMbsmLCTypeTable, MbsmLCRowStatusGet, MbsmLCRowStatusSet, MbsmLCRowStatusTest, MbsmLCTypeTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MbsmLCTypeTableINDEX, 1, 0, 1, NULL},

{{12,MbsmLCPortIndex}, GetNextIndexMbsmLCPortInfoTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, MbsmLCPortInfoTableINDEX, 2, 0, 0, NULL},

{{12,MbsmLCPortIfType}, GetNextIndexMbsmLCPortInfoTable, MbsmLCPortIfTypeGet, MbsmLCPortIfTypeSet, MbsmLCPortIfTypeTest, MbsmLCPortInfoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MbsmLCPortInfoTableINDEX, 2, 0, 0, NULL},

{{12,MbsmLCPortSpeed}, GetNextIndexMbsmLCPortInfoTable, MbsmLCPortSpeedGet, MbsmLCPortSpeedSet, MbsmLCPortSpeedTest, MbsmLCPortInfoTableDep, SNMP_DATA_TYPE_GAUGE32, SNMP_READWRITE, MbsmLCPortInfoTableINDEX, 2, 0, 0, NULL},

{{12,MbsmLCPortHighSpeed}, GetNextIndexMbsmLCPortInfoTable, MbsmLCPortHighSpeedGet, MbsmLCPortHighSpeedSet, MbsmLCPortHighSpeedTest, MbsmLCPortInfoTableDep, SNMP_DATA_TYPE_GAUGE32, SNMP_READWRITE, MbsmLCPortInfoTableINDEX, 2, 0, 0, NULL},

{{12,MbsmLCConfigSlotId}, GetNextIndexMbsmLCConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, MbsmLCConfigTableINDEX, 1, 0, 0, NULL},

{{12,MbsmLCConfigCardName}, GetNextIndexMbsmLCConfigTable, MbsmLCConfigCardNameGet, MbsmLCConfigCardNameSet, MbsmLCConfigCardNameTest, MbsmLCConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, MbsmLCConfigTableINDEX, 1, 0, 0, NULL},

{{12,MbsmLCConfigStatus}, GetNextIndexMbsmLCConfigTable, MbsmLCConfigStatusGet, MbsmLCConfigStatusSet, MbsmLCConfigStatusTest, MbsmLCConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MbsmLCConfigTableINDEX, 1, 0, 0, NULL},
};
tMibData fsmbsmEntry = { 18, fsmbsmMibEntry };
#endif /* _FSMBSMDB_H */

