/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mbsext.h,v 1.9 2014/10/08 11:04:38 siva Exp $
 *
 * Description:This file contains the global definitions    
 *             for the MBSM system.              
 *
 *******************************************************************/
#ifndef _MBSEXT_H
#define _MBSEXT_H

/******************** Global Declrations ************************/

extern UINT4               gu4MbsmBootFlag;
extern UINT1               gu1IsMbsmInitialised;
extern  UINT4               gu4CurrentProtoCookie ;
#ifdef RM_WANTED
extern tMbsmRedGlobalInfo  gMbsmRedGlobalInfo;
extern UINT4               gLocalAttachRcvd;
extern UINT4               gGiveGoActiveToOthers;
#endif

extern VOID SNMP_AGT_RIF_Notify_V1_Or_V2_Trap (UINT4 u4_Version,
                                   tSNMP_OID_TYPE * pEnterpriseOid,
                                   UINT4 u4_gen_trap_type,
                                   UINT4 u4_spec_trap_type,
                                   tSNMP_VAR_BIND * vb_list);

extern VOID  RmGetNodePriority PROTO ((UINT4*));
extern VOID  RmProcessInfoFromMbsm PROTO ((UINT4,UINT1));
extern VOID  RmRestoreStaticConfig PROTO ((VOID));
#endif /* _MBSEXT_H */
