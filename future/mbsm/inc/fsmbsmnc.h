/* $Id: fsmbsmnc.h,v 1.1 2016/01/11 13:36:03 siva Exp $
    ISS Wrapper header
    module ARICENT-CHASSIS-MIB

 */


#ifndef _H_i_ARICENT_CHASSIS_MIB
#define _H_i_ARICENT_CHASSIS_MIB
/********************************************************************
* FUNCTION NcMbsmMaxNumOfLCSlotsSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMbsmMaxNumOfLCSlotsSet (
                INT4 i4MbsmMaxNumOfLCSlots );

/********************************************************************
* FUNCTION NcMbsmMaxNumOfLCSlotsTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMbsmMaxNumOfLCSlotsTest (UINT4 *pu4Error,
                INT4 i4MbsmMaxNumOfLCSlots );

/********************************************************************
* FUNCTION NcMbsmMaxNumOfSlotsSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMbsmMaxNumOfSlotsSet (
                INT4 i4MbsmMaxNumOfSlots );

/********************************************************************
* FUNCTION NcMbsmMaxNumOfSlotsTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMbsmMaxNumOfSlotsTest (UINT4 *pu4Error,
                INT4 i4MbsmMaxNumOfSlots );

/********************************************************************
* FUNCTION NcMbsmMaxNumOfPortsPerLCSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMbsmMaxNumOfPortsPerLCSet (
                INT4 i4MbsmMaxNumOfPortsPerLC );

/********************************************************************
* FUNCTION NcMbsmMaxNumOfPortsPerLCTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMbsmMaxNumOfPortsPerLCTest (UINT4 *pu4Error,
                INT4 i4MbsmMaxNumOfPortsPerLC );

/********************************************************************
* FUNCTION NcMbsmLoadSharingFlagSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMbsmLoadSharingFlagSet (
                INT4 i4MbsmLoadSharingFlag );

/********************************************************************
* FUNCTION NcMbsmLoadSharingFlagTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMbsmLoadSharingFlagTest (UINT4 *pu4Error,
                INT4 i4MbsmLoadSharingFlag );

/********************************************************************
* FUNCTION NcMbsmSlotModuleTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMbsmSlotModuleTypeSet (
                INT4 i4MbsmSlotId,
                INT4 i4MbsmSlotModuleType );

/********************************************************************
* FUNCTION NcMbsmSlotModuleTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMbsmSlotModuleTypeTest (UINT4 *pu4Error,
                INT4 i4MbsmSlotId,
                INT4 i4MbsmSlotModuleType );

/********************************************************************
* FUNCTION NcMbsmSlotModuleStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMbsmSlotModuleStatusSet (
                INT4 i4MbsmSlotId,
                INT4 i4MbsmSlotModuleStatus );

/********************************************************************
* FUNCTION NcMbsmSlotModuleStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMbsmSlotModuleStatusTest (UINT4 *pu4Error,
                INT4 i4MbsmSlotId,
                INT4 i4MbsmSlotModuleStatus );

/********************************************************************
* FUNCTION NcMbsmLCNameSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMbsmLCNameSet (
                INT4 i4MbsmLCIndex,
                UINT1 *pMbsmLCName );

/********************************************************************
* FUNCTION NcMbsmLCNameTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMbsmLCNameTest (UINT4 *pu4Error,
                INT4 i4MbsmLCIndex,
                UINT1 *pMbsmLCName );

/********************************************************************
* FUNCTION NcMbsmLCMaxPortsSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMbsmLCMaxPortsSet (
                INT4 i4MbsmLCIndex,
                INT4 i4MbsmLCMaxPorts );

/********************************************************************
* FUNCTION NcMbsmLCMaxPortsTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMbsmLCMaxPortsTest (UINT4 *pu4Error,
                INT4 i4MbsmLCIndex,
                INT4 i4MbsmLCMaxPorts );

/********************************************************************
* FUNCTION NcMbsmLCRowStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMbsmLCRowStatusSet (
                INT4 i4MbsmLCIndex,
                INT4 i4MbsmLCRowStatus );

/********************************************************************
* FUNCTION NcMbsmLCRowStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMbsmLCRowStatusTest (UINT4 *pu4Error,
                INT4 i4MbsmLCIndex,
                INT4 i4MbsmLCRowStatus );

/********************************************************************
* FUNCTION NcMbsmLCPortIfTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMbsmLCPortIfTypeSet (
                INT4 i4MbsmLCIndex,
                INT4 i4MbsmLCPortIndex,
                INT4 i4MbsmLCPortIfType );

/********************************************************************
* FUNCTION NcMbsmLCPortIfTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMbsmLCPortIfTypeTest (UINT4 *pu4Error,
                INT4 i4MbsmLCIndex,
                INT4 i4MbsmLCPortIndex,
                INT4 i4MbsmLCPortIfType );

/********************************************************************
* FUNCTION NcMbsmLCPortSpeedSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMbsmLCPortSpeedSet (
                INT4 i4MbsmLCIndex,
                INT4 i4MbsmLCPortIndex,
                UINT4 u4MbsmLCPortSpeed );

/********************************************************************
* FUNCTION NcMbsmLCPortSpeedTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMbsmLCPortSpeedTest (UINT4 *pu4Error,
                INT4 i4MbsmLCIndex,
                INT4 i4MbsmLCPortIndex,
                UINT4 u4MbsmLCPortSpeed );

/********************************************************************
* FUNCTION NcMbsmLCPortHighSpeedSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMbsmLCPortHighSpeedSet (
                INT4 i4MbsmLCIndex,
                INT4 i4MbsmLCPortIndex,
                UINT4 u4MbsmLCPortHighSpeed );

/********************************************************************
* FUNCTION NcMbsmLCPortHighSpeedTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMbsmLCPortHighSpeedTest (UINT4 *pu4Error,
                INT4 i4MbsmLCIndex,
                INT4 i4MbsmLCPortIndex,
                UINT4 u4MbsmLCPortHighSpeed );

/********************************************************************
* FUNCTION NcMbsmLCConfigCardNameSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMbsmLCConfigCardNameSet (
                INT4 i4MbsmLCConfigSlotId,
                UINT1 *pMbsmLCConfigCardName );

/********************************************************************
* FUNCTION NcMbsmLCConfigCardNameTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMbsmLCConfigCardNameTest (UINT4 *pu4Error,
                INT4 i4MbsmLCConfigSlotId,
                UINT1 *pMbsmLCConfigCardName );

/********************************************************************
* FUNCTION NcMbsmLCConfigStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMbsmLCConfigStatusSet (
                INT4 i4MbsmLCConfigSlotId,
                INT4 i4MbsmLCConfigStatus );

/********************************************************************
* FUNCTION NcMbsmLCConfigStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMbsmLCConfigStatusTest (UINT4 *pu4Error,
                INT4 i4MbsmLCConfigSlotId,
                INT4 i4MbsmLCConfigStatus );

/* END i_ARICENT_CHASSIS_MIB.c */


#endif
