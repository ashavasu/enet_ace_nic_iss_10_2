/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: mbsred.h,v 1.7 2014/06/27 11:16:31 siva Exp $
 *
 * Description: This file contains the definitions and data structures
 *              and prototypes to support High Availability for MBSM module.
 *
 ******************************************************************/
#ifndef MBSM_RED_H
#define MBSM_RED_H

/* enums and Defines */

/* MBSM Module states */
typedef UINT4  tMbsmRedStates;

#define MBSM_RED_IDLE                         RM_INIT
#define MBSM_RED_ACTIVE                       RM_ACTIVE
#define MBSM_RED_STANDBY                      RM_STANDBY

/* Message types */
typedef enum {
    MBSM_RED_ATTACH_MSG = 1,
    MBSM_RED_DETACH_MSG,
    MBSM_RED_BULK_REQ,
    MBSM_RED_BULK_UPD_TAIL_MSG
}tMbsmRmMsgTypes;


extern tMbsmRedStates      gMbsmRedPrevNodeState;
/* MBSM Global Structure */
typedef struct _tMbsmRedGlobalInfo
{
    UINT4                u4BulkUpdNextSlot; /* Next Slot for which bulk update 
                                             * has to be generated. */
#ifdef NPAPI_WANTED
    UINT4                u4HwAuditTskId;    /* Hw Audit Task Id */
#endif
    UINT1                u1MbsmNodeStatus;  /* Node Status like ACTIVE, STANDBY.
                                             * To be in Sync with RM */
    
    UINT1                u1NumPeerPresent;  /* Number of Standby Nodes that are
                                             * present. In Standby Node, it will
                                             * be always 0 */
    
    BOOL1                bBulkReqRcvd;      /* To check whether bulk request is
                                             * recieved from standby node before
                                             * RM_STANDBY_UP event. */
    
    BOOL1                bPeerUpRxForRemoteSlot; /* To Check whether peer up is recieved
                                                  * After Standby transitions to Active
                                                  * Used only for dual unit control plane
                                                  * stacking
                                                  */
}tMbsmRedGlobalInfo;


/* MBSM Interface Functions */
INT4 MbsmRedRegisterWithRM PROTO ((VOID));
INT4 MbsmRedDeRegisterWithRM PROTO ((VOID));
INT4 MbsmRcvPktFromRm PROTO ((UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen));
VOID MbsmProcessRmEvent PROTO ((tMbsmMsg * pMsg));
VOID MbsmProcessRmGoActive PROTO ((VOID));
VOID MbsmProcessRmGoStandby PROTO ((VOID));
VOID MbsmRedHandleMessage PROTO ((tMbsmMsg * pMsg));
INT4 MbsmRedUpdateSlotAndPortMapTable PROTO ((VOID *pBuf, UINT4 *pu4Offset, 
                                              UINT2 u2Len));
INT4 MbsmRedUpdateDetach PROTO ((VOID *pBuf, UINT4 *pu4Offset, UINT2 u2Len));
VOID MbsmRedSendBulkRequest PROTO ((VOID));
INT4 MbsmRedSendSyncMessages PROTO ((INT4 i4SlotId, UINT1 u1MsgType));
INT4 MbsmRedFormMessage PROTO ((INT4 i4SlotId, UINT1 u1MsgType,tRmMsg *pBuf,
                                UINT4 *pu4Offset));
INT4 MbsmRedSendUpdateToRM PROTO ((tRmMsg * pMsg, UINT2 u2Len));
VOID MbsmRedHandleBulkRequest PROTO ((VOID));
VOID MbsmProcessFailover PROTO ((VOID));
VOID MbsmRedHandleDynSyncAudit (VOID);
VOID MbsmExecuteCmdAndCalculateChkSum  (VOID);
#ifdef NPAPI_WANTED
INT4 MbsmRedHwAudit PROTO ((VOID));
VOID MbsmRedStartHwAudit PROTO ((INT1 *pi1Param));
#endif
VOID MbsmRedHandleBulkUpdateEvent PROTO ((VOID));
INT4 MbsmRedHandleCCInsertEvent (INT4 i4SlotId);
#ifdef STACKING_WANTED
INT4 MbsmRedHandleLCInsertEventForPizzaStacking (INT4 i4SlotId);
#else
INT4 MbsmRedHandleLCInsertEvent (INT4 i4SlotId);
#endif /* STACKING_WANTED */
INT4 MbsmRedSendBulkUpdateTailMsg PROTO ((VOID));
VOID MbsmRedInitSlotAndPortMapEntry PROTO ((INT4 i4SlotId));
VOID DumpCardTypeTable PROTO ((VOID));
VOID DumpSlotMapAndPortInfoTable PROTO ((VOID));

/* MBSM definitions */
#define MBSM_RED_ATTACH_MSG_LEN              (3 + sizeof (tMbsmSlotInfo) + \
                                                 sizeof (tMbsmPortInfo))
#define MBSM_RED_DETACH_MSG_LEN              (3+4+1+1)
#define MBSM_RED_BULK_REQ_MSG_LEN               3
#define MBSM_RED_BULK_TAIL_MSG_LEN              3

#define MBSM_MAX_RM_BUF_SIZE                 1450
#define MBSM_RED_NO_OF_SLOTS_PER_SUB_UPDATE  10
#define MBSM_RED_TRUE                        1
#define MBSM_RED_FALSE                       0

#define MBSM_RED_NODE_STATUS()               gMbsmRedGlobalInfo.u1MbsmNodeStatus
#define MBSM_RED_NUM_STANDBY_NODES()         gMbsmRedGlobalInfo.u1NumPeerPresent
#define MBSM_RED_BULK_REQ_RCVD()             gMbsmRedGlobalInfo.bBulkReqRcvd
#define MBSM_RED_BULK_UPD_NEXT_SLOT()        gMbsmRedGlobalInfo.u4BulkUpdNextSlot
#define MBSM_RED_PEER_UP_RECEIVED()          gMbsmRedGlobalInfo.bPeerUpRxForRemoteSlot
#define MBSM_RED_IS_STANDBY_UP()  \
             ((gMbsmRedGlobalInfo.u1NumPeerPresent > 0) ? MBSM_TRUE : MBSM_FALSE)

#ifdef NPAPI_WANTED
#define MBSM_RED_HW_AUDIT_TASK_ID()          gMbsmRedGlobalInfo.u4HwAuditTskId
#define MBSM_HARDWARE_AUDIT                  (const UINT1 *) "MbHw"
#define MBSM_HARDWARE_AUDIT_PRIORITY         210
#endif

#define MBSM_RED_RM_GET_STATIC_CONFIG_STATUS()    \
                   RmGetStaticConfigStatus()

#define MBSM_RM_PUT_4_BYTE(pMsg, pu4Offset, u4MesgType) \
{ \
         RM_DATA_ASSIGN_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
                     *(pu4Offset) += 4;\
}

#define MBSM_RM_PUT_2_BYTE(pMsg, pu4Offset, u2MesgType) \
{ \
         RM_DATA_ASSIGN_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
                     *(pu4Offset) += 2;\
}

#define MBSM_RM_PUT_1_BYTE(pMsg, pu4Offset, u1MesgType) \
{ \
         RM_DATA_ASSIGN_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
                     *(pu4Offset) += 1;\
}

#define MBSM_RM_PUT_N_BYTE(pdest, psrc, pu4Offset, u4Size) \
{ \
         RM_COPY_TO_OFFSET(pdest, psrc, *(pu4Offset), u4Size); \
                     *(pu4Offset) +=u4Size; \
}

#define MBSM_RM_GET_1_BYTE(pMsg, pu4Offset, u1MesgType) \
{ \
         RM_GET_DATA_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
                     *(pu4Offset) += 1;\
}

#define MBSM_RM_GET_2_BYTE(pMsg, pu4Offset, u2MesgType) \
{ \
         RM_GET_DATA_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
                     *(pu4Offset) += 2;\
}

#define MBSM_RM_GET_4_BYTE(pMsg, pu4Offset, u4MesgType) \
{ \
        RM_GET_DATA_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
                    *(pu4Offset) += 4;\
}

#define MBSM_RM_GET_N_BYTE(psrc, pdest, pu4Offset, u4Size) \
{ \
        RM_GET_DATA_N_BYTE (psrc, pdest, *(pu4Offset), u4Size); \
                    *(pu4Offset) +=u4Size; \
}

#define MBSM_ASSERT()   printf("%s ,%d\n",__FILE__,__LINE__);

#endif
