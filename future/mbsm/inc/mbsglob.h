/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mbsglob.h,v 1.10 2015/12/16 10:42:20 siva Exp $
 *
 * Description:This file contains the global definitions    
 *             for the MBSM system.              
 *
 *******************************************************************/
#ifndef _MBSGLOB_H
#define _MBSGLOB_H

/******************** Global Declrations ************************/

tMbsmGlobalInfo     gMbsmGlobalInfo;
tMbsmSysInfo        gMbsmSysInfo;    /* Global variable updated from NVRAM */
tOsixSemId          gMbsmSemId;
UINT1               gu1IsMbsmInitialised = MBSM_FALSE;
UINT4               gu4MbsmTrcLevel;
UINT4               gu4CurrentProtoCookie = MBSM_PROTO_COOKIE_INVALID;
UINT4               gu4MbsmBootFlag = MBSM_FALSE;
UINT1               gu1SelfAttachRcvd = MBSM_FALSE; 
#ifdef RM_WANTED
tMbsmRedGlobalInfo  gMbsmRedGlobalInfo;
UINT4               gLocalAttachRcvd = MBSM_FALSE; /* Flag to handle ATTACH msg. 
                                                      ie ATTACH comes before Node 
                                                      moves to ACTIVE State, 
                                                      durin FailOver */
UINT4               gGiveGoActiveToOthers = MBSM_FALSE; /* Flag used to identify
                                       whether GO_ACTIVE needs to be given to
                                       other protocol modules after generating
                                       Remote slot detach */
#endif
UINT1               gau1PortInfoArr[MBSM_PORT_INFO_SIZE];
UINT1               gau1SlotInfoArr[MBSM_SLOT_INFO_SIZE];

CHR1  *mbsmProtoCookieName[] = {
    "MBSM_PROTO_COOKIE_INVALID",
#ifdef BRIDGE_WANTED
    "MBSM_PROTO_COOKIE_BRIDGE",
#endif /* BRIDGE_WANTED */
#ifdef VCM_WANTED
    "MBSM_PROTO_COOKIE_VCM",
#endif /* VCM_WANTED */
    "MBSM_PROTO_COOKIE_CFA", /* Just for ISS system features related configurations */
#ifdef PNAC_WANTED
    "MBSM_PROTO_COOKIE_PNAC",
#endif /* PNAC_WANTED */
#ifdef LA_WANTED
    "MBSM_PROTO_COOKIE_LA",
#endif /* LA_WANTED */
#ifdef VLAN_WANTED
    "MBSM_PROTO_COOKIE_VLAN",
#endif /* VLAN_WANTED */
    "MBSM_PROTO_COOKIE_SISP",
#ifdef PBBTE_WANTED
    "MBSM_PROTO_COOKIE_PBBTE",
#endif /* PBBTE_WANTED */
#ifdef PBB_WANTED
    "MBSM_PROTO_COOKIE_PBB",
#endif /* PBB_WANTED */
#ifdef MRP_WANTED
    "MBSM_PROTO_COOKIE_MRP",
#endif /* MRP_WANTED */
#if defined (IGS_WANTED) || defined (MLDS_WANTED)
    "MBSM_PROTO_COOKIE_SNOOP",
#endif /* IGS_WANTED */
#if defined (RSTP_WANTED) || defined (MSTP_WANTED) 
    "MBSM_PROTO_COOKIE_AST",
#endif /* RSTP_WANTED || MSTP_WANTED */
#ifdef ECFM_WANTED
    "MBSM_PROTO_COOKIE_ECFM",
#endif
#ifdef ELPS_WANTED
    "MBSM_PROTO_COOKIE_ELPS",
#endif
#ifdef ERPS_WANTED
    "MBSM_PROTO_COOKIE_ERPS",
#endif
#ifdef IP_WANTED 
    "MBSM_PROTO_COOKIE_IP_FWD",
#endif
#ifdef ARP_WANTED
    "MBSM_PROTO_COOKIE_IP_ARP",
#endif
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
    "MBSM_PROTO_COOKIE_IP_RTM",
#endif
#ifdef OSPF_WANTED
    "MBSM_PROTO_COOKIE_OSPF",
#endif
#ifdef IP6_WANTED 
    "MBSM_PROTO_COOKIE_IP6_FWD",
    "MBSM_PROTO_COOKIE_IP6_RTM6",
#endif
#ifdef RIP6_WANTED 
    "MBSM_PROTO_COOKIE_RIP6",
#endif
#ifdef OSPF3_WANTED
    "MBSM_PROTO_COOKIE_OSPF3",
#endif
#ifdef IGMP_WANTED 
    "MBSM_PROTO_COOKIE_IGMP",
#endif
#ifdef PIM_WANTED 
    "MBSM_PROTO_COOKIE_PIM",
#endif
#ifdef DVMRP_WANTED 
    "MBSM_PROTO_COOKIE_DVMRP",
#endif
#ifdef EOAM_WANTED 
    "MBSM_PROTO_COOKIE_EOAM",
#endif /* EOAM_WANTED */
#ifdef MPLS_WANTED 
    "MBSM_PROTO_COOKIE_MPLS",
#endif
#ifdef BFD_WANTED 
    "MBSM_PROTO_COOKIE_BFD",
#endif
#ifdef QOSX_WANTED 
    "MBSM_PROTO_COOKIE_QOSX",
#endif
#ifdef DCBX_WANTED 
    "MBSM_PROTO_COOKIE_DCBX",
#endif
#ifdef FSB_WANTED
    "MBSM_PROTO_COOKIE_FSB",
#endif
#ifdef RBRG_WANTED
    "MBSM_PROTO_COOKIE_RBRG",
#endif
#ifdef ISIS_WANTED
    "MBSM_PROTO_COOKIE_ISIS",
#endif
    "MBSM_PROTO_COOKIE_LINKSTATUS", /* Just to update Admin status
                                   * of the interface and update link status
                                   * This should be last vaild entry */
    "MBSM_PROTO_COOKIE_NULL" /* This should always be the last entry */
};
#endif /* _MBSGLOB_H */
