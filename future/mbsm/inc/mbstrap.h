/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mbstrap.h,v 1.6 2010/07/28 10:30:59 prabuc Exp $
 *
 * Description:This file contains the variable definitions   
 *             for the MBSM Trap system.              
 *
 *******************************************************************/
#ifndef _MBSTRAP_H
#define _MBSTRAP_H
#include "mbsm.h"

extern INT1                      MBSM_TRAPS_OID[256];

#define MBSM_TRAPS_OID_LEN       11 
#define MBSM_MAX_OBJ_LEN         256
#define MBSM_SLOTMODULE_MAP_OID  "1.3.6.1.4.1.2076.81.100.2"
#define MBSM_CONFIG_ERR_TRAP_OID "1.3.6.1.4.1.2076.81.100.6.1"
#define MBSM_CARD_INSERT_TRAP_OID "1.3.6.1.4.1.2076.81.100.6.2"
#define MBSM_CARD_REMOVE_TRAP_OID "1.3.6.1.4.1.2076.81.100.6.3"
#define MBSM_CARDNAME_OID        "1.3.6.1.4.1.2076.81.100.3.1.2",

typedef enum
{
    MBSM_CONFIG_ERR_TRAP=1,
    MBSM_CARD_INSERT_TRAP=2,
    MBSM_CARD_REMOVE_TRAP=3
} tMbsmTrapVal;

typedef struct
{
    INT4  i4SlotId;
    INT4  i4ModType;
    INT1  ai1CardName[MBSM_MAX_CARD_NAME_LEN];
    INT1  i1Status;
    INT1  ai1Padding[3];
} tMbsmConfigErrTrap;

PUBLIC VOID
MbsmSnmpSendConfigErrTrap PROTO ((INT4 i4SlotId, INT4 i4ModuleType,
                           INT1 *pi1CardName, INT1 i1Status,
                           INT1 *pi1TrapsOid, UINT1 u1TrapOidLen));

VOID MbsmSnmpSendTrap PROTO ((UINT1 u1TrapId, INT1 *pi1TrapOid, UINT1 u1OidLen,
                   VOID *pTrapInfo));

tSNMP_OID_TYPE     *MbsmMakeObjIdFromDotNew PROTO ((INT1 *pi1TextStr));

INT4 MbsmParseSubIdNew PROTO ((INT1 **ppi1TempPtr, UINT4 *pu4Value));

VOID MbsmMemFreeVarBindList PROTO ((tSNMP_VAR_BIND * pVarBindLst));

VOID MbsmFreeVarbind PROTO ((tSNMP_VAR_BIND * pVarBind));
#endif /* _MBSTRAP_H */
