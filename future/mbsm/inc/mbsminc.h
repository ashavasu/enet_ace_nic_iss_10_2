/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mbsminc.h,v 1.29 2016/03/18 13:22:08 siva Exp $
 *
 * Description:This file includes other header files    
 *             for the CFA system.              
 *
 *******************************************************************/
#ifndef _MBSMINC_H
#define _MBSMINC_H

#include "lr.h"

#include "rmgr.h"
#include "issu.h"

#include "mbsdefs.h"

#include "mbsm.h"

#include "cfa.h"

#include "bridge.h"

#ifdef VCM_WANTED
#include "vcm.h"
#endif

#ifdef VLAN_WANTED
#include "fsvlan.h"
#endif

#ifdef PBB_WANTED
#include "pbb.h"
#endif

#ifdef PBBTE_WANTED
#include "pbbte.h"
#endif

#ifdef ISS_WANTED
#include "iss.h"
#endif


#if defined (IGS_WANTED) || defined (MLDS_WANTED)
#include "snp.h"
#endif

#ifdef PNAC_WANTED
#include "pnac.h"
#endif

#ifdef LA_WANTED
#include "la.h"
#endif

#if defined(RSTP_WANTED) ||  defined(MSTP_WANTED)
#include "rstp.h"
#endif

#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
#include "ip.h"
#endif
#ifdef ARP_WANTED
#include "arp.h"
#endif

#ifdef PIM_WANTED
#include "pim.h"
#endif

#ifdef DVMRP_WANTED
#include "dvmrp.h"
#endif

#ifdef IGMP_WANTED
#include "igmp.h"
#endif

#ifdef OSPF_WANTED
#include "ospf.h"
#endif

#ifdef IP6_WANTED
#include "ipv6.h"
#endif

#ifdef OSPF3_WANTED
#include "ospf3.h"
#endif
#ifdef EOAM_WANTED
#include "eoam.h"
#endif/* EOAM_WANTED */
#ifdef ECFM_WANTED
#include "ecfm.h"
#endif/* ECFM_WANTED */
#ifdef ELPS_WANTED
#include "elps.h"
#endif/* ELPS_WANTED */
#ifdef ERPS_WANTED
#include "erps.h"
#endif/* ERPS_WANTED */

#ifdef BFD_WANTED
#include "bfd.h"
#endif/* BFD_WANTED */

#include "fssyslog.h"

#include "snmccons.h"

#ifdef RM_WANTED
#include "mbsred.h"
#endif

#ifdef MRP_WANTED
#include "mrp.h"
#endif

#ifdef RIP6_WANTED
#include "ripv6.h"
#endif

#ifdef ISIS_WANTED
#include "isis.h"
#endif

#ifdef SNMP_2_WANTED
 #include "snmputil.h"
#endif
/* MBSM modules' proprietary header files */

#include "mbsproto.h"
#include "mbsext.h"
#include "fssnmp.h"
#include "fsmbsmlw.h"
#include "mbstrap.h"
#ifdef QOSX_WANTED
#include "qosxtd.h"
#endif

#ifdef DCBX_WANTED
#include "dcbx.h"
#endif

#ifdef FSB_WANTED
#include "fsb.h"
#endif

#ifdef RBRG_WANTED
#include "rbridge.h"
#endif
#include "sizereg.h"
#include "mbsmsz.h"

#ifdef NPAPI_WANTED
#include "nputil.h"
#include "cfanpwr.h"
#include "mbsmnpwr.h"
#endif
#endif /* _MBSMINC_H */
