#ifndef _FSMBSMWR_H
#define _FSMBSMWR_H

VOID RegisterFSMBSM(VOID);

VOID UnRegisterFSMBSM(VOID);
INT4 MbsmMaxNumOfLCSlotsGet(tSnmpIndex *, tRetVal *);
INT4 MbsmMaxNumOfSlotsGet(tSnmpIndex *, tRetVal *);
INT4 MbsmMaxNumOfPortsPerLCGet(tSnmpIndex *, tRetVal *);
INT4 MbsmLoadSharingFlagGet(tSnmpIndex *, tRetVal *);
INT4 MbsmMaxNumOfLCSlotsSet(tSnmpIndex *, tRetVal *);
INT4 MbsmMaxNumOfSlotsSet(tSnmpIndex *, tRetVal *);
INT4 MbsmMaxNumOfPortsPerLCSet(tSnmpIndex *, tRetVal *);
INT4 MbsmLoadSharingFlagSet(tSnmpIndex *, tRetVal *);
INT4 MbsmMaxNumOfLCSlotsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MbsmMaxNumOfSlotsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MbsmMaxNumOfPortsPerLCTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MbsmLoadSharingFlagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MbsmMaxNumOfLCSlotsDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 MbsmMaxNumOfSlotsDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 MbsmMaxNumOfPortsPerLCDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 MbsmLoadSharingFlagDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);




INT4 GetNextIndexMbsmSlotModuleMapTable(tSnmpIndex *, tSnmpIndex *);
INT4 MbsmSlotIdGet(tSnmpIndex *, tRetVal *);
INT4 MbsmSlotModuleTypeGet(tSnmpIndex *, tRetVal *);
INT4 MbsmSlotModuleStatusGet(tSnmpIndex *, tRetVal *);
INT4 MbsmSlotModuleTypeSet(tSnmpIndex *, tRetVal *);
INT4 MbsmSlotModuleStatusSet(tSnmpIndex *, tRetVal *);
INT4 MbsmSlotModuleTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MbsmSlotModuleStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MbsmSlotModuleMapTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexMbsmLCTypeTable(tSnmpIndex *, tSnmpIndex *);
INT4 MbsmLCIndexGet(tSnmpIndex *, tRetVal *);
INT4 MbsmLCNameGet(tSnmpIndex *, tRetVal *);
INT4 MbsmLCMaxPortsGet(tSnmpIndex *, tRetVal *);
INT4 MbsmLCRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 MbsmLCNameSet(tSnmpIndex *, tRetVal *);
INT4 MbsmLCMaxPortsSet(tSnmpIndex *, tRetVal *);
INT4 MbsmLCRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 MbsmLCNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MbsmLCMaxPortsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MbsmLCRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MbsmLCTypeTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



INT4 GetNextIndexMbsmLCPortInfoTable(tSnmpIndex *, tSnmpIndex *);
INT4 MbsmLCPortIndexGet(tSnmpIndex *, tRetVal *);
INT4 MbsmLCPortIfTypeGet(tSnmpIndex *, tRetVal *);
INT4 MbsmLCPortSpeedGet(tSnmpIndex *, tRetVal *);
INT4 MbsmLCPortHighSpeedGet(tSnmpIndex *, tRetVal *);
INT4 MbsmLCPortIfTypeSet(tSnmpIndex *, tRetVal *);
INT4 MbsmLCPortSpeedSet(tSnmpIndex *, tRetVal *);
INT4 MbsmLCPortHighSpeedSet(tSnmpIndex *, tRetVal *);
INT4 MbsmLCPortIfTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MbsmLCPortSpeedTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MbsmLCPortHighSpeedTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MbsmLCPortInfoTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



INT4 GetNextIndexMbsmLCConfigTable(tSnmpIndex *, tSnmpIndex *);
INT4 MbsmLCConfigSlotIdGet(tSnmpIndex *, tRetVal *);
INT4 MbsmLCConfigCardNameGet(tSnmpIndex *, tRetVal *);
INT4 MbsmLCConfigStatusGet(tSnmpIndex *, tRetVal *);
INT4 MbsmLCConfigCardNameSet(tSnmpIndex *, tRetVal *);
INT4 MbsmLCConfigStatusSet(tSnmpIndex *, tRetVal *);
INT4 MbsmLCConfigCardNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MbsmLCConfigStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MbsmLCConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


#endif /* _FSMBSMWR_H */
