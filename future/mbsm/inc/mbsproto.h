/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mbsproto.h,v 1.22 2014/11/03 11:48:17 siva Exp $
 *
 * Description:This file contains the common definitions    
 *             for the MBSM system.              
 *
 *******************************************************************/
#ifndef _MBSPROTO_H
#define _MBSPROTO_H

INT4 MbsmInit PROTO ((VOID));

INT4 MbsmInitSlotMapTable PROTO ((VOID));

INT4 MbsmInitPortInfoTable PROTO ((VOID));

INT4 MbsmCreateCardTypeEntry PROTO ((UINT4 u4Index));

INT4 MbsmDeleteCardTypeEntry PROTO ((UINT4 u4Index));

INT4 MbsmGetFreeCardTypeTableIndex PROTO ((UINT4 *pu4FreeIndex));

INT4 MbsmAddCardTypeEntry PROTO ((tMbsmCardInfo *pCardInfo));

INT4 MbsmAddCardPortInfoEntry PROTO ((tMbsmLcTypeInfo *pMbsLcTypeInfo));

INT4 MbsmGetSlotIdFromMsg PROTO((tMbsmHwMsg *pMsg, INT4 *pi4SlotId));

INT4 MbsmIsPreConfigured PROTO ((INT4 i4SlotId));
 
VOID MbsmShutdown PROTO ((VOID));


INT4 MbsmNotifyCfaModuleUpdate PROTO ((INT4 i4SlotId));

INT4 MbsmHandleProtoAckMsg PROTO ((tMbsmProtoAckMsg *pMsg, INT4 *pi4ProtoCookie));

INT4 MbsmGetMsgType PROTO ((UINT1 *pMsg, INT4 *pi4MsgType));

INT4 MbsmFormMsgToCfa PROTO ((INT4 i4SlotId, tMbsmCfaHwInfoMsg *pMsg,
   INT2 i2OffSet));

VOID MbsmFormPortInfoPortList PROTO((INT4 i4SlotId, tMbsmPortList MbsmPortList,
                               INT1 i1IsInsert));

INT4 MbsmUpdateTables PROTO ((tMbsmHwSlotInfo * pMsg, INT4 i4ModType, 
   INT1 i1IsInsert));

INT4 MbsmUpdateSlotMapEntry PROTO ((tMbsmHwSlotInfo * pSlotInfo, 
   INT4 i4ModType, INT1 i1IsInsert));

INT4 MbsmUpdatePortInfoEntry PROTO ((tMbsmHwSlotInfo * pSlotInfo, 
   INT4 i4ModType, INT1 i1IsInsert));

INT4 MbsmUpdateSlotMapEntryStatus PROTO ((tMbsmHwSlotInfo * pSlotInfo, 
   INT1 i1Status));

INT4 MbsmGetSlotCardName PROTO ((INT4 i4SlotId, INT1 *pi1Name));

INT4 MbsmGetPortMtuFromPortMap PROTO ((tMbsmLcPortInfo *pEntry, UINT4 u4PortNum, 
  UINT4 *pu4IfMtu));

INT4 MbsmCreateCardPortMapEntry PROTO ((UINT4 u4CardIndex));

INT4 MbsmGetPortIfTypeFromPortMap PROTO ((tMbsmLcPortInfo *pEntry, 
   UINT4 u4PortNum, UINT4 *pu4IfType));

INT4 MbsmNotifyLcUpdate PROTO ((INT4 i4SlotId, INT4 i4ProtoCookie));

INT4 MbsmGetSlotModuleInfoFromFile PROTO ((VOID));

INT4 MbsmGetSlotIdFromFile PROTO ((INT4 *pi4RetSlotId));

INT4 MbsmInitCardTypeTable PROTO ((VOID));

INT4 MbsmGetPortSpeedType PROTO ((UINT1 *pu1Str, UINT4 *pu4SpeedType));

INT4 MbsmInitSlotModuleInfo PROTO ((VOID));

INT4 MbsmGetPortSpeed PROTO ((UINT4 u4IfIndex, tMbsmPortSpeed *pSpeedInfo));

INT4 MbsmGetPortSpeedFromPortMap PROTO ((tMbsmLcPortInfo *pEntry, UINT4 u4PortNum, 
  tMbsmPortSpeed *pSpeedInfo));

INT4 MbsmGetSpeedInfoFromType PROTO ((UINT4 u4SpeedType, 
   tMbsmPortSpeed *pSpeedInfo));

INT4 MbsmSetPortIfTypeFromPortMap PROTO ((tMbsmLcPortInfo *pEntry, UINT4 u4PortNum, 
  UINT4 u4IfType));

INT4 MbsmSetPortSpeedFromPortMap PROTO ((tMbsmLcPortInfo *pEntry, UINT4 u4PortNum, 
  tMbsmPortSpeed PortSpeed));

INT4 MbsmValidateIndexSlotModuleMapTable PROTO ((INT4 i4SlotIndex));

INT4 MbsmGetFirstIndexSlotModuleMapTable PROTO ((INT4 *pi4FirstIndex));

INT4 MbsmGetNextIndexSlotModuleMapTable PROTO ((INT4 i4SlotIndex, 
   INT4 *pi4NextIndex));

INT4 MbsmValidateIndexCardTypeTable PROTO ((INT4 i4CardIndex));

INT4 MbsmGetFirstIndexCardTypeTable PROTO ((INT4 *pi4FirstIndex));

INT4 MbsmGetNextIndexCardTypeTable PROTO ((INT4 i4CardIndex, INT4 *pi4NextIndex));

INT4 MbsmValidateIndexPortInfoTable PROTO ((INT4 i4CardIndex, INT4 i4PortIndex));

INT4 MbsmGetFirstIndexPortInfoTable PROTO ((INT4 *pi4FirstCardIndex, 
   INT4 *pi4FirstPortIndex));

INT4 MbsmGetNextIndexPortInfoTable PROTO ((INT4 i4CardIndex, INT4 i4PortIndex,
  INT4 *pi4NextCardIndex, INT4 *pi4NextPortIndex));

INT4 MbsmValidateIndexLCConfigTable PROTO ((INT4 i4Index));

INT4 MbsmGetFirstIndexLCConfigTable PROTO ((INT4 *pi4FirstIndex));

INT4 MbsmGetNextIndexLCConfigTable PROTO ((INT4 i4Index, INT4 *pi4NextIndex));

INT4 MbsmGetPortMtuType PROTO ((UINT1 *pu1Str, UINT4 *pu4MtuType));

INT4 MbsmGetPortEncapType PROTO ((UINT1 *pu1Str, UINT4 *pu4EncapType));

INT4 MbsmAddCardTypePortInfo PROTO ((tMbsmCardInfo *pCardInfo));

INT4 MbsmGetNextToken PROTO ((UINT1 *pu1Str, UINT1 **pu1Token, 
   UINT1 **pu1RemStr));

INT4 MbsmWriteSlotModuleInfoToFile PROTO ((VOID));

INT4 MbsmGetEncapInfoFromType PROTO ((UINT4 u4EncapType, UINT4 *pu4Encap));

INT4 MbsmGetMtuInfoFromType PROTO ((UINT4 u4MtuType, UINT4 *pu4Mtu));

INT4 MbsmSendBootMsgToCfa PROTO ((VOID));

INT4 MbsmInitSysInfo PROTO ((VOID));

INT4 MbsmNotifyCfaPreConfigDel PROTO ((INT4 i4SlotId));
INT4 MbsmNotifyLoadSharingStatusChgToMbsm PROTO ((INT4 i4MbsmLoadSharingFlag));
INT4 MbsmNotifyLoadSharingStatusChgToProto PROTO ((INT4 i4MbsmLoadSharingFlag));
INT4 MbsmUpdtTblsForLoadSharing (UINT1 u1Flag);
INT4 MbsmStartTimer (tMbsmAppTmrNode * pMbsmAppTmrNode);
INT4 MbsmStopTimer (tMbsmAppTmrNode * pMbsmAppTmrNode);
VOID MbsmTmrExpiryHandler (VOID);
VOID MbsmRedHandlePeerStateChangeEvent (UINT1 u1Event);
INT4 MbsmWaitForSelfAttachCompletion (INT4 i4SlotId);

#ifdef MSR_WANTED
VOID MbsmWaitForMSRCompleteEvent (VOID);
#endif

INT4 MbsmInitHwTopoDisc PROTO ((INT1 i1NodeState));

INT4 MbsmGenerateRemoteSlotDetach (VOID);
INT4
MbsmRmEnqChkSumMsgToRm PROTO ((UINT2, UINT2 ));

INT4
MbsmGetShowCmdOutputAndCalcChkSum (UINT2 *);
INT4
MbsmCliGetShowCmdOutputToFile (UINT1 *);

INT4
MbsmCliCalcSwAudCheckSum (UINT1 *, UINT2 *);

INT1
MbsmCliReadLineFromFile (INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen, INT2 *pi2ReadLen);

INT4 MbsmGetSlotIdFromConnectingPort (UINT4 u4ConnectingPort, UINT4 *pu4SlotId);
INT4 MbsmGetConnectingPortFromSlotId (UINT4 u4SlotId, UINT4 *pu4ConnectingPort);
INT4 MbsmIsConnectingPort (UINT4 u4IfIndex);
VOID MbsmWaitForRestorationComplete (VOID);
#endif /* _MBSPROTO_H */
