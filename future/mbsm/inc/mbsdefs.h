/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mbsdefs.h,v 1.60 2015/10/01 12:28:35 siva Exp $
 *
 * Description:This file contains the common definitions    
 *             for the MBSM system.              
 *
 *******************************************************************/
#ifndef _MBSMDEFS_H
#define _MBSMDEFS_H

#include "lr.h"
#include "mbsm.h"
#include "cfa.h"

#define MBSM_TASK_NODE_ID               SELF
#define MBSM_ACK_QUEUE_NODE_ID          SELF
#define MBSM_INIT_VAL                   0
#define MBSM_MSG_QUEUE_NAME             "MBSQ"
#define MBSM_EVENT_WAIT_FLAGS           OSIX_WAIT
#define MBSM_EVENT_WAIT_TIMEOUT         0                         
#define MBSM_INIT_COMPLETE(u4Status) lrInitComplete(u4Status)

#define MBSM_MAX_IF_TYPES               CFA_INVALID_TYPE /* Number of Interface types
           supported - PortInfoTable */
#define MBSM_MAX_PORT_MTU_TYPES          6 /* Number of MTU values    
           supported - PortInfoTable */
#define MBSM_PORT_MTU_ENET              1500
#define MBSM_PORT_MTU_FDDI              4500

#define MBSM_LOW_SPEED_MAX              0xffffffff


/* Events from Hw Driver to MBSM are clubbed together as a single
   event MBSM_DRIVER_EVENT to MBSM */

#define MBSM_DRIVER_EVENT               0x01 /* Driver to MBSM */
#define MBSM_PROTO_ACK_MSG_EVENT        0x02 /* Protocols to MBSM */
#define MBSM_SYNC_ACK_MSG_EVENT         0x04
#define MBSM_LOAD_SHARING_EVENT         0x08 /* CLI to MBSM */
#define MBSM_TMR_EXPIRY_EVENT           0x10
#define MBSM_RED_MSG_EVENT              0x20
#define MBSM_RED_BULK_UPD_EVENT         0x40
#define MBSM_SELF_ATTACH_ACK_EVENT      0x80 /* CFA to MBSM */
#define MBSM_RESUME_EVENT               0x100
#define MBSM_AUDIT_FILE_ACTIVE "/tmp/mbsm_output_file_active"
#define MBSM_AUDIT_FILE_STDBY "/tmp/mbsm_output_file_stdby"
#define MBSM_AUDIT_SHOW_CMD    "show hardware > "
#define MBSM_CLI_EOF                  2
#define MBSM_CLI_NO_EOF                 1
#define MBSM_CLI_RDONLY               OSIX_FILE_RO
#define MBSM_CLI_WRONLY               OSIX_FILE_WO
#define MBSM_CLI_MAX_GROUPS_LINE_LEN  200

/* This file contains the info about Slot Id Vs. Module Type */
#define MBSM_SLOT_MODULE_FILE          FLASH "SlotModule.conf"

#define MBSM_PORT_INDEX_START           1

#define MBSM_COOKIE_START_INDEX         1


/* Semaphores for Data Protection from Protocols */
#define MBSM_PROTO_SEM                  (const UINT1*) ("MBSM")

#define MBSM_PROTO_SEMID                gMbsmGlobalInfo.gMbsmProtoSemId

#define MBSM_PROTO_CREATE_SEM(SemName, InitCnt, Flags, pSemId) \
 OsixCreateSem (SemName, InitCnt, Flags, pSemId)
#define MBSM_PROTO_TAKE_SEM(SemName)          \
 (OsixTakeSem (SELF, (CONST UINT1 *)SemName, 0, 0))
#define MBSM_PROTO_GIVE_SEM(SemName)          \
 (OsixGiveSem (SELF, (CONST UINT1 *)SemName))
#define MBSM_PROTO_DELETE_SEM(SemName)          \
 (OsixDeleteSem (SELF, (CONST UINT1 *)SemName))

#define MBSM_CREATE_TMR_LIST            TmrCreateTimerList
#define MBSM_DEL_TMR_LIST               TmrDeleteTimerList
#define MBSM_START_TMR                  TmrStartTimer
#define MBSM_STOP_TMR                   TmrStopTimer
#define MBSM_GET_NEXT_EXPRD_TMR         TmrGetNextExpiredTimer

/* Task, Queue and Event related macros, OSIX related macros */

#define MBSM_TASK_ID                    gMbsmGlobalInfo.gMbsmTaskId
#define MBSM_GET_TASK_ID                OsixTskIdSelf
    
#define MBSM_SEND_EVENT                 OsixEvtSend
#define MBSM_RECV_EVENT                 OsixEvtRecv

/* The protocl cookies identifies the Protocol during module insertion
 * notifications and ack messages. The ordering is important as the
 * protocols are notified in the mentioned order starting from 1
 */
typedef enum
{
    MBSM_PROTO_COOKIE_INVALID = 0,
#ifdef BRIDGE_WANTED
    MBSM_PROTO_COOKIE_BRIDGE,
#endif /* BRIDGE_WANTED */
#ifdef VCM_WANTED
    MBSM_PROTO_COOKIE_VCM,
#endif /* VCM_WANTED */
    MBSM_PROTO_COOKIE_CFA, /* Just for ISS system features related configurations */
#ifdef PNAC_WANTED
    MBSM_PROTO_COOKIE_PNAC,
#endif /* PNAC_WANTED */
#ifdef LA_WANTED
    MBSM_PROTO_COOKIE_LA,
#endif /* LA_WANTED */
#ifdef VLAN_WANTED
    MBSM_PROTO_COOKIE_VLAN,
#endif /* VLAN_WANTED */
    MBSM_PROTO_COOKIE_SISP,
#ifdef PBBTE_WANTED
    MBSM_PROTO_COOKIE_PBBTE,
#endif /* PBBTE_WANTED */
#ifdef PBB_WANTED
    MBSM_PROTO_COOKIE_PBB,
#endif /* PBB_WANTED */
#ifdef MRP_WANTED
    MBSM_PROTO_COOKIE_MRP,
#endif /* MRP_WANTED */
#if defined (IGS_WANTED) || defined (MLDS_WANTED)
    MBSM_PROTO_COOKIE_SNOOP,
#endif /* IGS_WANTED */
#if defined (RSTP_WANTED) || defined (MSTP_WANTED) 
    MBSM_PROTO_COOKIE_AST,
#endif /* RSTP_WANTED || MSTP_WANTED */
#ifdef ECFM_WANTED
    MBSM_PROTO_COOKIE_ECFM,
#endif
#ifdef ELPS_WANTED
    MBSM_PROTO_COOKIE_ELPS,
#endif
#ifdef ERPS_WANTED
    MBSM_PROTO_COOKIE_ERPS,
#endif
#ifdef IP_WANTED 
    MBSM_PROTO_COOKIE_IP_FWD,
#endif
#ifdef ARP_WANTED
    MBSM_PROTO_COOKIE_IP_ARP,
#endif
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
    MBSM_PROTO_COOKIE_IP_RTM,
#endif
#ifdef OSPF_WANTED
    MBSM_PROTO_COOKIE_OSPF,
#endif
#ifdef IP6_WANTED 
    MBSM_PROTO_COOKIE_IP6_FWD,
    MBSM_PROTO_COOKIE_IP6_RTM6,
#endif
#ifdef RIP6_WANTED 
    MBSM_PROTO_COOKIE_RIP6,
#endif
#ifdef OSPF3_WANTED
    MBSM_PROTO_COOKIE_OSPF3,
#endif
#ifdef IGMP_WANTED 
    MBSM_PROTO_COOKIE_IGMP,
#endif
#ifdef PIM_WANTED 
    MBSM_PROTO_COOKIE_PIM,
#endif
#ifdef DVMRP_WANTED 
    MBSM_PROTO_COOKIE_DVMRP,
#endif
#ifdef EOAM_WANTED 
    MBSM_PROTO_COOKIE_EOAM,
#endif /* EOAM_WANTED */
#ifdef MPLS_WANTED 
    MBSM_PROTO_COOKIE_MPLS,
#endif
#ifdef BFD_WANTED 
    MBSM_PROTO_COOKIE_BFD,
#endif
#ifdef QOSX_WANTED 
    MBSM_PROTO_COOKIE_QOSX,
#endif
#ifdef DCBX_WANTED 
    MBSM_PROTO_COOKIE_DCBX,
#endif
#ifdef FSB_WANTED
    MBSM_PROTO_COOKIE_FSB,
#endif
#ifdef RBRG_WANTED
    MBSM_PROTO_COOKIE_RBRG,
#endif
#ifdef ISIS_WANTED
    MBSM_PROTO_COOKIE_ISIS,
#endif
    MBSM_PROTO_COOKIE_LINKSTATUS, /* Just to update Admin status
                                   * of the interface and update link status
                                   * This should be last vaild entry */

    MBSM_PROTO_COOKIE_NULL /* This should always be the last entry */
} tMbsmProtoCookie;

/*************   Port Characteristics       ************************/

/* Port MTU types */
typedef enum
{
    MBSM_MTU_INVALID = 0,
    MBSM_MTU_ENET,
    MBSM_MTU_FDDI,
    MBSM_MTU_NULL       /* This should always be the last entry */
} tMbsmPortMtu;

/* Port Encapsulation types */
typedef enum
{
    MBSM_ENCAP_INVALID = 0,
    MBSM_ENCAP_ENETV2,
    MBSM_ENCAP_LLC,
    MBSM_ENCAP_LLC_SNAP,
    MBSM_ENCAP_NULL     /* This should always be the last entry */
} tMbsmPortEncap;

/* Timer flag to indicate whether a timer is running or is stopped */
typedef enum {
    MBSM_TMR_STOPPED = 0,
    MBSM_TMR_RUNNING
}tMbsmTmrFlag;

/* Timer types */
enum {
    MBSM_RECVRY_TMR_TYPE = 1
};

enum {
    MBSM_RECVRY_TIME_OUT = 5 *  MBSM_MAX_SLOTS
};
      

/*************   Structure definitions      ************************/

#ifdef RM_WANTED
/* Peer node Id. */
typedef VOID * tMbsmRedPeerId;

/* To handle message/events given by redundancy manager. */
typedef struct {
    tRmMsg        *pFrame;     /* Message given by RM module. */
    tMbsmRedPeerId  PeerId;    /* This field will be used in case of 1:n
                                * redundancy support. */
    UINT2          u2Length;   /* Length of message given by RM module. */
    UINT1          u1Event;    /* Event given by RM module. */
    UINT1          u1Reserved;
}tMbsmRedRmMsg;
#endif

/* The message header structure used in all messages to MBSM */
typedef struct
{
    INT4            i4MsgType; /* Type of the message */
    union {
#ifdef RM_WANTED
        tMbsmRedRmMsg  RmMsg;
#endif
        UINT1           au1Data[MBSM_MAX_SLOTS * (sizeof (tMbsmHwMsg))]; /* Rest of the message */
        UINT1           au1pad[3];
    }uMsg;
} tMbsmMsg;


/* The structure for the Port speed */
typedef struct
{
    UINT4          u4Speed;
    UINT4          u4HighSpeed;
} tMbsmPortSpeed;


/* The structure definition for the Port Map Table */
typedef struct
{
    tMbsmCardPortList  IfTypeTable[MBSM_MAX_IF_TYPES];
    tMbsmCardPortList  MtuTable[MBSM_MAX_PORT_MTU_TYPES];
    tMbsmCardPortList  SpeedTable[MBSM_MAX_PORT_SPEED_TYPES];
    UINT4          u4LcIndex;    /* Line Card entry corresponding
        to this port */
} tMbsmLcPortInfo;


/* The structure definition for the Card Type Table */
typedef struct
{
    tMbsmLcPortInfo PortMapEntry;  /* Port map table for this Line 
          Card entry */          
    UINT4           u4CardIndex;   /* Index of the entry in the table */
    UINT4           u4NumPorts;    /* Number of ports in the Card */
    INT1            ai1CardName[MBSM_MAX_CARD_NAME_LEN];
    INT1            i1RowStatus;   /* used to create entry thro' SNMP */
    INT1            ai1Padding[3];
} tMbsmLcTypeInfo;


typedef struct
{
    UINT4          u4SpeedType;
    tMbsmPortSpeed PortSpeed;
} tMbsmPortSpeedInfo;


typedef struct
{
    UINT4          u4EncapType;
    UINT4          u4Encap;
} tMbsmPortEncapInfo;

typedef struct
{
    UINT4          u4MtuType;
    UINT4          u4Mtu;
} tMbsmPortMtuInfo;


/* Structure containing Slot Module Info in init file */
typedef struct
{
    INT4                i4SlotId;
    INT4                i4ModuleType; /* Line card or Control card */
    INT4                i4CardIndex;
    INT1                i1IsPreConf;
    INT1                ai1Padding[3];
} tMbsmSlotModuleInfo;


typedef struct {
    tTmrAppTimer   MbsmAppTimer; /* This is the tTmrAppTimer of the FSAP2. The
                                    u4Data field in the structure has to be
                                    initialised to the Timer Type */ 

    tMbsmTmrFlag   MbsmTmrFlag;  /* This flag is used to indicate whether
                                    timer is running or stopped. */
}tMbsmAppTmrNode;

/*************   Global definitions         ************************/


typedef struct
{
    /* The Slot numbers can start from 0 or 1, hence we create
     * an extra entry to accomodate both */
    tMbsmSlotInfo       **gaMbsmSlotMapTable;
    tMbsmPortInfo       **gaMbsmPortInfoTable;

    tMbsmLcTypeInfo    *gaMbsmLcTypeTable[MBSM_MAX_LC_TYPES]; /* Line Card Table */

    tOsixTaskId         gMbsmTaskId;

    tOsixQId            gMbsmQId;

    tOsixSemId          gMbsmProtoSemId;

    tOsixSemId          gMbsmSyncSemId; /* Synchronization sema4 */

    UINT4               gu4MbsmSyslogId;

    /* Mem pool Id for Slot Map table */
    UINT4               gu4MbsmSlotMapMemPoolId; 

    /* Mem pool Id for Port Info table */
    UINT4               gu4MbsmPortInfoMemPoolId; 

    /* Mem pool Id for Card Type table */
    UINT4               gu4MbsmCardTypeMemPoolId; 
    
    UINT4               gu4CtrlQMemPooIId;/* Memory Pool Id of the pool used for
                                           * allocating memory blocks for Messages
                                           * posted to MBSM Queue */
    UINT4               gu4MsgQMemPoolId; /* Memory Pool Id of the pool used for
                                           * allocating memory blocks for Messages
                                           * posted to MBSM Queue */
    tTimerListId        MbsmTmrListId;

    tMbsmAppTmrNode     MbsmRecoveryTmr;  
    /* Timer used to wait for remote attach before giving remote detach
     * to higher layers */

    UINT1               gu1MbsmIsDefInit; 
    /* Flag to indicate if MBSM initialised with default parameters */

    /* Flag to indicate whether load sharing is enabled or not */
    UINT1               gu1MbsmLoadSharing;

    UINT1               gu1SelfSlotId; 
    /* Node ID to differentiate local & Remote attach/detach */

    UINT1               au1Padding[1];

} tMbsmGlobalInfo;

typedef struct _tMbsmMsgQueueBlock
{
    tMbsmMsg     MbsmMsgQueueBlock[MBSM_MAX_SLOTS];
}tMbsmMsgQueueBlock;

extern tMbsmGlobalInfo  gMbsmGlobalInfo;
extern tMbsmSysInfo     gMbsmSysInfo;
/*************   Macro definitions          ************************/

#define MBSM_FOREVER()                   while(1)

#define MBSM_QUEUE_ID                    gMbsmGlobalInfo.gMbsmQId

#define MBSM_SLOT_MAP_ENTRY(u4Index)     \
        gMbsmGlobalInfo.gaMbsmSlotMapTable[u4Index - MBSM_SLOT_INDEX_START]

#define MBSM_SLOT_INDEX_PORTLIST(u4Index)  \
   (MBSM_SLOT_MAP_ENTRY(u4Index))->PortList
#define MBSM_SLOT_INDEX_PORTLIST_STATUS(u4Index)  \
   (MBSM_SLOT_MAP_ENTRY(u4Index))->PortListStatus
   
#define MBSM_SLOT_INDEX_SLOT_ID(u4Index)  \
   (MBSM_SLOT_MAP_ENTRY(u4Index))->i4SlotId

#define MBSM_SLOT_INDEX_ISPRECONF(u4Index)  \
   (MBSM_SLOT_MAP_ENTRY(u4Index))->i1IsPreConfigured

#define MBSM_SLOT_INDEX_NUMPORTS(u4Index)  \
   (MBSM_SLOT_MAP_ENTRY(u4Index))->u4NumPorts

#define MBSM_SLOT_INDEX_CONNECTINGPORT(u4Index,u4Index1) \
   (MBSM_SLOT_MAP_ENTRY(u4Index))->MbsmConnectingPortInfo.au4ConnectingPort[u4Index1]


#define MBSM_SLOT_INDEX_CONNECTINGPORT_UNIT(u4Index,u4Index1) \
   (MBSM_SLOT_MAP_ENTRY(u4Index))->MbsmConnectingPortInfo.au4UnitId[u4Index1]

#define MBSM_SLOT_INDEX_CONNECTING_PORT_COUNT(u4Index) \
   (MBSM_SLOT_MAP_ENTRY(u4Index))->MbsmConnectingPortInfo.u4ConnectingPortCount   

#define MBSM_SLOT_INDEX_CARDTYPE(u4Index)  \
   (MBSM_SLOT_MAP_ENTRY(u4Index))->i4CardType

#define MBSM_SLOT_INDEX_MODTYPE(u4Index)  \
   (MBSM_SLOT_MAP_ENTRY(u4Index))->i4ModuleType

#define MBSM_SLOT_INDEX_STARTINDEX(u4Index)  \
   (MBSM_SLOT_MAP_ENTRY(u4Index))->u4StartIfIndex

#define MBSM_SLOT_INDEX_STATUS(u4Index)  \
   (MBSM_SLOT_MAP_ENTRY(u4Index))->i1Status

#define MBSM_SLOT_INDEX_FABRIC_TYPE(u4Index)  \
   (MBSM_SLOT_MAP_ENTRY(u4Index))->u1SwFabricType

#define MBSM_SLOT_INDEX_PREVSTATUS(u4Index)  \
   (MBSM_SLOT_MAP_ENTRY(u4Index))->i1PrevStatus

#define MBSM_SLOT_INDEX_RM_PEER_UP_MSG_TYPE(u4Index) \
   (MBSM_SLOT_MAP_ENTRY(u4Index))->u1IsRmPeerUpMsg

#define MBSM_PORT_INFO_ENTRY(u4Index)     \
   gMbsmGlobalInfo.gaMbsmPortInfoTable[u4Index - MBSM_SLOT_INDEX_START]

#define MBSM_PORT_INDEX_PORTLIST(u4Index) \
   (MBSM_PORT_INFO_ENTRY(u4Index))->PortList

#define MBSM_PORT_INDEX_PORTLISTSTATUS(u4Index) \
                        (MBSM_PORT_INFO_ENTRY(u4Index))->PortListStatus

#define MBSM_PORT_INDEX_PORTCOUNT(u4Index) \
   (MBSM_PORT_INFO_ENTRY(u4Index))->u4PortCount
   
#define MBSM_PORT_INDEX_STARTINDEX(u4Index) \
   (MBSM_PORT_INFO_ENTRY(u4Index))->u4StartIfIndex

#define MBSM_PORT_INDEX_CONNECTING_PORT_COUNT(u4Index) \
   (MBSM_PORT_INFO_ENTRY(u4Index))->u4ConnectingPortCount   

#define MBSM_CARD_TYPE_ENTRY(u4Index)    \
   gMbsmGlobalInfo.gaMbsmLcTypeTable[u4Index]

#define MBSM_CARD_TYPE_INFO_PORTMAP(u4Index) \
                 (MBSM_CARD_TYPE_ENTRY(u4Index))->PortMapEntry
   
#define MBSM_CARD_TYPE_INFO_CARDINDEX(u4Index) \
                 (MBSM_CARD_TYPE_ENTRY(u4Index))->u4CardIndex

#define MBSM_CARD_TYPE_INFO_NUMPORTS(u4Index) \
                 (MBSM_CARD_TYPE_ENTRY(u4Index))->u4NumPorts

#define MBSM_CARD_TYPE_INFO_CARDNAME(u4Index) \
                 (MBSM_CARD_TYPE_ENTRY(u4Index))->ai1CardName
   
#define MBSM_CARD_TYPE_INFO_ROWSTATUS(u4Index) \
   (MBSM_CARD_TYPE_ENTRY(u4Index))->i1RowStatus

#define MBSM_IS_LINE_CARD(i4CardType)    \
  (i4CardType == MBSM_LINE_CARD) 

#define MBSM_IS_CONTROL_CARD(i4CardType)    \
  (i4CardType == MBSM_CONTROL_CARD) 

#define MBSM_IS_SLOT_ID_VALID(i4SlotId)    \
  ((i4SlotId >= MBSM_SLOT_INDEX_START) && \
  (i4SlotId < (MBSM_MAX_SLOTS + MBSM_SLOT_INDEX_START)))

#define MBSM_BOOT_FLAG()        gu4MbsmBootFlag

#define MBSM_TIMER_LIST_ID      gMbsmGlobalInfo.MbsmTmrListId

#define MBSM_DEF_INIT_FLAG()    gMbsmGlobalInfo.gu1MbsmIsDefInit

#define MBSM_SLOT_MAP_MEMPOOL_ID  gMbsmGlobalInfo.gu4MbsmSlotMapMemPoolId

#define MBSM_PORT_INFO_MEMPOOL_ID gMbsmGlobalInfo.gu4MbsmPortInfoMemPoolId
 
#define MBSM_CARD_TYPE_MEMPOOL_ID gMbsmGlobalInfo.gu4MbsmCardTypeMemPoolId

#define MBSM_CTRL_QUEUE_MEMPOOL_ID gMbsmGlobalInfo.gu4CtrlQMemPooIId 
#define MBSM_MSG_QUEUE_MEMPOOL_ID  gMbsmGlobalInfo.gu4MsgQMemPoolId

#define MBSM_CTRLQ_MSG_MEMBLK_COUNT   (MBSM_MAX_PORTS_PER_SLOT * 20)
/*Constant 20 is just a factor and there is particular no intention*/

#define MBSM_ALLOC_CTRLQ_MEM_BLOCK(ppNode) \
            MemAllocateMemBlock (MBSM_CTRL_QUEUE_MEMPOOL_ID, (UINT1 **)ppNode)

#define MBSM_RELEASE_CTRLQ_MEM_BLOCK(pNode)  \
                 MemReleaseMemBlock (MBSM_CTRL_QUEUE_MEMPOOL_ID, (UINT1 *)pNode)
    
    
#define MBSM_LOAD_SHARING_FLAG()  gMbsmGlobalInfo.gu1MbsmLoadSharing
#define MBSM_SELF_SLOT_ID()       gMbsmGlobalInfo.gu1SelfSlotId
#define MBSM_SEM_NAME   ((const UINT1 *)"MBSS")
#define MBSM_SEM_CREATE() \
        OsixCreateSem (MBSM_SEM_NAME, 1, OSIX_DEFAULT_SEM_MODE, \
                       (&gMbsmSemId))
#define MBSM_RELEASE_SEM(u4NodeId, pu1SemName)\
        OsixGiveSem ((u4NodeId), (pu1SemName))
                                                                             
#define MBSM_DELETE_SEM() \
        OsixDeleteSem (SELF, MBSM_SEM_NAME);

#define MBSM_TAKE_SEM(u4NodeId, pu1Sem, u4Flags, u4Timeout) \
        OsixTakeSem ((u4NodeId), (pu1Sem), (u4Flags), (u4Timeout))


/* Used for Portlist used for a slot */
#define MBSM_IS_MEMBER_MBSMPORTLIST(au1PortArray, u2Port, pu4Result) \
        {\
           UINT2 u2PortBytePos;\
           UINT2 u2PortBitPos;\
           u2PortBytePos = (UINT2)(u2Port / MBSM_PORTS_PER_BYTE);\
           u2PortBitPos  = (UINT2)(u2Port % MBSM_PORTS_PER_BYTE);\
      if (u2PortBitPos  == 0) {u2PortBytePos -= 1;} \
           \
           if ((au1PortArray[u2PortBytePos] \
                & gau1MbsmPortBitMask[u2PortBitPos]) != 0) {\
           \
              *pu4Result = MBSM_SUCCESS;\
           }\
           else {\
           \
              *pu4Result = MBSM_FAILURE; \
           } \
        }

#define MBSM_ARE_PORTS_EXCLUSIVE(au1List1, au1List2, u1Result) \
        {\
           UINT2 u2ByteIndex;\
           u1Result = MBSM_TRUE;\
           for (u2ByteIndex = 0;\
                u2ByteIndex < MBSM_PORT_LIST_SIZE;\
                u2ByteIndex++) {\
              if ((au1List1[u2ByteIndex] & au1List2[u2ByteIndex]) != 0) {\
                 u1Result = MBSM_FALSE;\
                 break;\
              }\
           }\
        }

#define MBSM_ADD_PORT_LIST(au1List1, au1List2) \
              {\
                 UINT2 u2ByteIndex;\
                 \
                 for (u2ByteIndex = 0;\
                      u2ByteIndex < MBSM_PORT_LIST_SIZE;\
                      u2ByteIndex++) {\
                    au1List1[u2ByteIndex] |= au1List2[u2ByteIndex];\
                 }\
              }
              
/*************   Debug/Trace related Macros ************************/

#define MBSM_MAIN                       "[MbsmMain]"
#define MBSM_PROTO                      "[MbsmProto]"
#define MBSM_TOPO                       "[MbsmTopo]"
#define MBSM_UTIL                       "[MbsmUtil]"
#define MBSM_INIT                       "[MbsmInit]"
#define MBSM_RED                        "[MbsmRed]"

#define MBSM_SYSLOG_ID            gMbsmGlobalInfo.gu4MbsmSyslogId

#define MBSM_SLOT_INFO_SIZE       (sizeof (tMbsmSlotInfo *) * MBSM_MAX_SLOTS)
#define MBSM_PORT_INFO_SIZE       (sizeof (tMbsmPortInfo *) * MBSM_MAX_SLOTS)

#define MBSM_VALIDATE_SLOT_ID(i4SlotId)     \
     (((i4SlotId < MBSM_MIN_LC) ||             \
           (i4SlotId > MBSM_MAX_LC)) ? FNP_FAILURE : FNP_SUCCESS)

#define MBSM_MIN_LC                  0
#define MBSM_MAX_LC                  2

#define MBSM_INVALID_TYPE            0
#define MBSM_INVALID_PORTNUM         0




#endif /* _MBSMDEFS_H */
