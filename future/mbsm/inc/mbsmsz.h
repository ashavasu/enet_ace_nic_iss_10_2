enum {
    MAX_MBSM_CTRLQ_MSG_MEMBLK_COUNT_SIZING_ID,
    MAX_MBSM_LC_TYPES_SIZING_ID,
    MAX_MBSM_MSG_QUEUE_DEPTH_SIZING_ID,
    MAX_MBSM_PORT_INFO_BLOCKS_SIZING_ID,
    MAX_MBSM_SLOT_INFO_BLOCKS_SIZING_ID,
    MBSM_MAX_SIZING_ID
};


#ifdef  _MBSMSZ_C
tMemPoolId MBSMMemPoolIds[ MBSM_MAX_SIZING_ID];
INT4  MbsmSizingMemCreateMemPools(VOID);
VOID  MbsmSizingMemDeleteMemPools(VOID);
INT4  MbsmSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _MBSMSZ_C  */
extern tMemPoolId MBSMMemPoolIds[ ];
extern INT4  MbsmSizingMemCreateMemPools(VOID);
extern VOID  MbsmSizingMemDeleteMemPools(VOID);
extern INT4  MbsmSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _MBSMSZ_C  */


#ifdef  _MBSMSZ_C
tFsModSizingParams FsMBSMSizingParams [] = {
{ "tMbsmMsg", "MAX_MBSM_CTRLQ_MSG_MEMBLK_COUNT", sizeof(tMbsmMsg),MAX_MBSM_CTRLQ_MSG_MEMBLK_COUNT, MAX_MBSM_CTRLQ_MSG_MEMBLK_COUNT,0 },
{ "tMbsmLcTypeInfo", "MAX_MBSM_LC_TYPES", sizeof(tMbsmLcTypeInfo),MAX_MBSM_LC_TYPES, MAX_MBSM_LC_TYPES,0 },
{ "tMbsmMsgQueueBlock", "MAX_MBSM_MSG_QUEUE_DEPTH", sizeof(tMbsmMsgQueueBlock),MAX_MBSM_MSG_QUEUE_DEPTH, MAX_MBSM_MSG_QUEUE_DEPTH,0 },
{ "tMbsmPortInfo", "MAX_MBSM_PORT_INFO_BLOCKS", sizeof(tMbsmPortInfo),MAX_MBSM_PORT_INFO_BLOCKS, MAX_MBSM_PORT_INFO_BLOCKS,0 },
{ "tMbsmSlotInfo", "MAX_MBSM_SLOT_INFO_BLOCKS", sizeof(tMbsmSlotInfo),MAX_MBSM_SLOT_INFO_BLOCKS, MAX_MBSM_SLOT_INFO_BLOCKS,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _MBSMSZ_C  */
extern tFsModSizingParams FsMBSMSizingParams [];
#endif /*  _MBSMSZ_C  */


