/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmbsmlw.h,v 1.5 2008/08/20 15:13:39 iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMbsmMaxNumOfLCSlots ARG_LIST((INT4 *));

INT1
nmhGetMbsmMaxNumOfSlots ARG_LIST((INT4 *));

INT1
nmhGetMbsmMaxNumOfPortsPerLC ARG_LIST((INT4 *));

INT1
nmhGetMbsmLoadSharingFlag ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMbsmMaxNumOfLCSlots ARG_LIST((INT4 ));

INT1
nmhSetMbsmMaxNumOfSlots ARG_LIST((INT4 ));

INT1
nmhSetMbsmMaxNumOfPortsPerLC ARG_LIST((INT4 ));

INT1
nmhSetMbsmLoadSharingFlag ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MbsmMaxNumOfLCSlots ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2MbsmMaxNumOfSlots ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2MbsmMaxNumOfPortsPerLC ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2MbsmLoadSharingFlag ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MbsmMaxNumOfLCSlots ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2MbsmMaxNumOfSlots ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2MbsmMaxNumOfPortsPerLC ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2MbsmLoadSharingFlag ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for MbsmSlotModuleMapTable. */
INT1
nmhValidateIndexInstanceMbsmSlotModuleMapTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for MbsmSlotModuleMapTable  */

INT1
nmhGetFirstIndexMbsmSlotModuleMapTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMbsmSlotModuleMapTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMbsmSlotModuleType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetMbsmSlotModuleStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMbsmSlotModuleType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetMbsmSlotModuleStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MbsmSlotModuleType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2MbsmSlotModuleStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MbsmSlotModuleMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for MbsmLCTypeTable. */
INT1
nmhValidateIndexInstanceMbsmLCTypeTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for MbsmLCTypeTable  */

INT1
nmhGetFirstIndexMbsmLCTypeTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMbsmLCTypeTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMbsmLCName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMbsmLCMaxPorts ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetMbsmLCRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMbsmLCName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetMbsmLCMaxPorts ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetMbsmLCRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MbsmLCName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2MbsmLCMaxPorts ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2MbsmLCRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MbsmLCTypeTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for MbsmLCPortInfoTable. */
INT1
nmhValidateIndexInstanceMbsmLCPortInfoTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for MbsmLCPortInfoTable  */

INT1
nmhGetFirstIndexMbsmLCPortInfoTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMbsmLCPortInfoTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMbsmLCPortIfType ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetMbsmLCPortSpeed ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetMbsmLCPortHighSpeed ARG_LIST((INT4  , INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMbsmLCPortIfType ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetMbsmLCPortSpeed ARG_LIST((INT4  , INT4  ,UINT4 ));

INT1
nmhSetMbsmLCPortHighSpeed ARG_LIST((INT4  , INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MbsmLCPortIfType ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2MbsmLCPortSpeed ARG_LIST((UINT4 *  ,INT4  , INT4  ,UINT4 ));

INT1
nmhTestv2MbsmLCPortHighSpeed ARG_LIST((UINT4 *  ,INT4  , INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MbsmLCPortInfoTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for MbsmLCConfigTable. */
INT1
nmhValidateIndexInstanceMbsmLCConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for MbsmLCConfigTable  */

INT1
nmhGetFirstIndexMbsmLCConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMbsmLCConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMbsmLCConfigCardName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMbsmLCConfigStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMbsmLCConfigCardName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetMbsmLCConfigStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MbsmLCConfigCardName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2MbsmLCConfigStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MbsmLCConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
