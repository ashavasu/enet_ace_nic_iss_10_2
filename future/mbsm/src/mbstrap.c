/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mbstrap.c,v 1.10 2012/01/10 12:51:25 siva Exp $
 *
 * Description:This file contains the routines for SNMP Trap     
 *             for MBSM module.
 *
 *******************************************************************/
#include  "lr.h"
#include  "fssnmp.h"
#include "mbstrap.h"
#include "mbsminc.h"
#include "fsmbsm.h"

INT1                MBSM_TRAPS_OID[] = "1.3.6.1.4.1.2076.100.81.6.0";
static INT1         ai1TempBuffer[257];

/*****************************************************************************/
/*                                                                           */
/* Function     : MbsmSnmpSendTrap                                           */
/*                                                                           */
/* Description  : Routine to send trap to SNMP .                             */
/*                                                                           */
/* Input        : i4TrapId   : Trap Identifier                               */
/*                pi1TrapOid : Pointer to the trap OID                       */
/*                u1OidLen   : OID Length                                    */
/*                pTrapInfo  : Pointer to the trap information.              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
MbsmSnmpSendTrap (UINT1 u1TrapId, INT1 *pi1TrapOid, UINT1 u1OidLen,
                  VOID *pTrapInfo)
{
#ifdef SNMP_3_WANTED
    tMbsmConfigErrTrap *pMbsmConfigErrTrap;
    tSNMP_OCTET_STRING_TYPE *pOstring;
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid;
    tSNMP_OID_TYPE     *pOid;
    UINT1               au1Buf[MBSM_MAX_OBJ_LEN];
    UINT4               u4GenTrapType;
    UINT4               u4SpecTrapType;
    INT4               *pi4SlotId = NULL;
    tSNMP_COUNTER64_TYPE SnmpCounter64Type;

    UNUSED_PARAM (u1OidLen);

    pEnterpriseOid = (tSNMP_OID_TYPE *) SNMP_AGT_GetOidFromString (pi1TrapOid);
    if (pEnterpriseOid == NULL)
    {
        return;
    }
    u4GenTrapType = ENTERPRISE_SPECIFIC;
    u4SpecTrapType = (UINT4) u1TrapId;
    SnmpCounter64Type.msn = 0;
    SnmpCounter64Type.lsn = 0;

    switch (u1TrapId)
    {

        case MBSM_CONFIG_ERR_TRAP:

            pMbsmConfigErrTrap = (tMbsmConfigErrTrap *) pTrapInfo;
            SPRINTF ((char *) au1Buf, "mbsmSlotId");
            pOid = (tSNMP_OID_TYPE *) MbsmMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pVbList =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pMbsmConfigErrTrap->i4SlotId,
                                      NULL, NULL, SnmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pStartVb = pVbList;
            SPRINTF ((char *) au1Buf, "mbsmSlotModuleType");
            pOid = (tSNMP_OID_TYPE *) MbsmMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER, 0,
                                      (INT4) pMbsmConfigErrTrap->i4ModType,
                                      NULL, NULL, SnmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pVbList = pVbList->pNextVarBind;
            SPRINTF ((char *) au1Buf, "mbsmLCConfigCardName");
            pOid = (tSNMP_OID_TYPE *) MbsmMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pOstring =
                SNMP_AGT_FormOctetString ((UINT1 *)
                                          &pMbsmConfigErrTrap->ai1CardName,
                                          STRLEN (pMbsmConfigErrTrap->
                                                  ai1CardName));
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                          SNMP_DATA_TYPE_OCTET_PRIM,
                                                          0, 0, pOstring, NULL,
                                                          SnmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pVbList = pVbList->pNextVarBind;
            SPRINTF ((char *) au1Buf, "mbsmSlotModuleStatus");
            pOid = (tSNMP_OID_TYPE *) MbsmMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER, 0,
                                      (INT4) pMbsmConfigErrTrap->i1Status,
                                      NULL, NULL, SnmpCounter64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            break;

        case MBSM_CARD_INSERT_TRAP:
        case MBSM_CARD_REMOVE_TRAP:
            pi4SlotId = (INT4 *) pTrapInfo;
            SPRINTF ((char *) au1Buf, "mbsmSlotId");
            pOid = (tSNMP_OID_TYPE *) MbsmMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pVbList = SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                            *pi4SlotId, NULL, NULL,
                                            SnmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pStartVb = pVbList;
            break;
        default:
            return;
    }

    /* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_AGT_RIF_Notify_Trap (pEnterpriseOid, u4GenTrapType, u4SpecTrapType,
                              pStartVb);

#else /* SNMP_2_WANTED */
    UNUSED_PARAM (u1TrapId);
    UNUSED_PARAM (*pi1TrapOid);
    UNUSED_PARAM (u1OidLen);
    UNUSED_PARAM (pTrapInfo);
#endif /* SNMP_2_WANTED */

}

PUBLIC VOID
MbsmSnmpSendConfigErrTrap (INT4 i4SlotId, INT4 i4ModuleType,
                           INT1 *pi1CardName, INT1 i1Status,
                           INT1 *pi1TrapsOid, UINT1 u1TrapOidLen)
{
#ifdef SNMP_3_WANTED
    tMbsmConfigErrTrap  MbsmConfigErrTrap;

    MbsmConfigErrTrap.i4SlotId = i4SlotId;
    MbsmConfigErrTrap.i4ModType = i4ModuleType;
    MbsmConfigErrTrap.i1Status = i1Status;
    if (pi1CardName)
    {
        STRNCPY (MbsmConfigErrTrap.ai1CardName, pi1CardName,
                 MBSM_MAX_CARD_NAME_LEN);
    }
    else
    {
        return;
    }

    MbsmSnmpSendTrap (MBSM_CONFIG_ERR_TRAP, pi1TrapsOid, u1TrapOidLen,
                      &MbsmConfigErrTrap);

#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1AdminStatus);
    UNUSED_PARAM (u1OperStatus);
    UNUSED_PARAM (pi1TrapsOid);
    UNUSED_PARAM (u1TrapOidLen);
#endif /* SNMP_2_WANTED */

}

/******************************************************************************
* Function : MbsmMakeObjIdFromDotNew
* Input    : pi1TextStr
* Output   : NONE
* Returns  : pOidPtr or NULL
*******************************************************************************/

tSNMP_OID_TYPE     *
MbsmMakeObjIdFromDotNew (INT1 *pi1TextStr)
{
#ifdef SNMP_3_WANTED
    tSNMP_OID_TYPE     *pOidPtr;
    INT1               *pi1TempPtr, *pi1DotPtr;
    UINT2               u2Index;
    UINT2               u2DotCount;

    /* see if there is an alpha descriptor at begining */
    if (isalpha (*pi1TextStr))
    {
        pi1DotPtr = (INT1 *) STRCHR ((INT1 *) pi1TextStr, '.');

        /* if no dot, point to end of string */
        if (pi1DotPtr == NULL)
        {
            pi1DotPtr = pi1TextStr + STRLEN ((INT1 *) pi1TextStr);
        }
        pi1TempPtr = pi1TextStr;

        for (u2Index = 0; ((pi1TempPtr < pi1DotPtr) && (u2Index < 256));
             u2Index++)
        {
            ai1TempBuffer[u2Index] = *pi1TempPtr++;
        }
        ai1TempBuffer[u2Index] = '\0';

        for (u2Index = 0; orig_mib_oid_table[u2Index].pName != NULL; u2Index++)
        {
            if ((STRCMP
                 (orig_mib_oid_table[u2Index].pName,
                  (INT1 *) ai1TempBuffer) == 0)
                && (STRLEN ((INT1 *) ai1TempBuffer) ==
                    STRLEN (orig_mib_oid_table[u2Index].pName)))
            {
                STRCPY ((INT1 *) ai1TempBuffer,
                        orig_mib_oid_table[u2Index].pNumber);
                break;
            }
        }

        if (orig_mib_oid_table[u2Index].pName == NULL)
        {
            return (NULL);
        }
        /* now concatenate the non-alpha part to the begining */
        STRCAT ((INT1 *) ai1TempBuffer, (INT1 *) pi1DotPtr);
    }
    else
    {                            /* is not alpha, so just copy into ai1TempBuffer */
        STRCPY ((INT1 *) ai1TempBuffer, (INT1 *) pi1TextStr);
    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    u2DotCount = 0;
    for (u2Index = 0; ai1TempBuffer[u2Index] != '\0'; u2Index++)
    {
        if (ai1TempBuffer[u2Index] == '.')
        {
            u2DotCount++;
        }
    }
    if ((pOidPtr = alloc_oid (u2DotCount + 1)) == NULL)
    {
        return (NULL);
    }

    pOidPtr->u4_Length = u2DotCount + 1;

    /* now we convert number.number.... strings */
    pi1TempPtr = ai1TempBuffer;
    for (u2Index = 0; u2Index < u2DotCount + 1; u2Index++)
    {
        if (MbsmParseSubIdNew
            ((&(pi1TempPtr)), &(pOidPtr->pu4_OidList[u2Index])) == MBSM_FAILURE)
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }

        if (*pi1TempPtr == '.')
        {
            pi1TempPtr++;        /* to skip over dot */
        }
        else if (*pi1TempPtr != '\0')
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }
    }                            /* end of for loop */

    return (pOidPtr);
#else
    UNUSED_PARAM (pi1TextStr);
    return (NULL);
#endif
}

/******************************************************************************
* Function : MbsmParseSubIdNew
* Input    : ppi1TempPtr
* Output   : value of ppi1TempPtr
* Returns  : MBSM_SUCCESS or MBSM_FAILURE
*******************************************************************************/

INT4
MbsmParseSubIdNew (INT1 **ppi1TempPtr, UINT4 *pu4Value)
{
    UINT4               u4Value = 0;
    INT1               *pi1Tmp;
    INT4                i4RetVal = MBSM_SUCCESS;

    for (pi1Tmp = *ppi1TempPtr; (((*pi1Tmp >= '0') && (*pi1Tmp <= '9')) ||
                                 ((*pi1Tmp >= 'a') && (*pi1Tmp <= 'f')) ||
                                 ((*pi1Tmp >= 'A') && (*pi1Tmp <= 'F')));
         pi1Tmp++)
    {
        u4Value = (u4Value * 10) + (*pi1Tmp & 0xf);
    }

    if (*ppi1TempPtr == pi1Tmp)
    {
        i4RetVal = MBSM_FAILURE;
    }
    *ppi1TempPtr = pi1Tmp;
    *pu4Value = u4Value;
    return (i4RetVal);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : MbsmMemFreeVarBindList                                     */
/*                                                                           */
/* Description  : Frees the memory allocated for SNMP VarBind list,          */
/*                by scanning the list                                       */
/*                                                                           */
/* Input        : pVarBindLst  : pointer to the Varbind list                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

VOID
MbsmMemFreeVarBindList (tSNMP_VAR_BIND * pVarBindLst)
{
#ifdef SNMP_3_WANTED
    SNMP_AGT_FreeVarBindList (pVarBindLst);
#else
    UNUSED_PARAM (pVarBindLst);
#endif
}
