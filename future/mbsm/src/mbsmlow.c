/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mbsmlow.c,v 1.7 2009/01/02 11:45:47 prabuc-iss Exp $
 *
 * Description:This file contains the routines needed for NMH
 *             functions.
 *
 *******************************************************************/
#include "mbsminc.h"

/****************************************************************************
 *                                                                          
 *    Function Name      : MbsmValidateIndexSlotModuleMapTable              
 *                                                                          
 *    Description        : This function validates the given index for the  
 *                         Slot Module Table.                                  
 *                                                                          
 *    Input(s)           : i4SlotIndex - Index of the table to be validated.
 *                                                                          
 *    Output(s)          : None.                                            
 *                                                                          
 *    Returns            : MBSM_SUCCESS or MBSM_FAILURE                     
 ****************************************************************************/
INT4
MbsmValidateIndexSlotModuleMapTable (INT4 i4SlotIndex)
{
    if (!MBSM_IS_SLOT_ID_VALID (i4SlotIndex))
    {
        return (MBSM_FAILURE);
    }

    return (MBSM_SUCCESS);
}

/****************************************************************************
 *                                                                          
 *    Function Name      : MbsmGetFirstIndexSlotModuleMapTable              
 *                                                                          
 *    Description        : This function returns the first valid index of the
 *                         Slot Module Table.                                  
 *                                                                          
 *    Input(s)           : None.                                            
 *                                                                          
 *    Output(s)          : pi4FirstIndex - First Valid index in the table.    
 *                                                                          
 *    Returns            : MBSM_SUCCESS or MBSM_FAILURE                     
 ****************************************************************************/
INT4
MbsmGetFirstIndexSlotModuleMapTable (INT4 *pi4FirstIndex)
{
    INT4                i4Index;

    for (i4Index = MBSM_SLOT_INDEX_START;
         i4Index < (MBSM_MAX_SLOTS + MBSM_SLOT_INDEX_START); i4Index++)
    {
        if (MbsmValidateIndexSlotModuleMapTable (i4Index) == MBSM_SUCCESS)
        {
            *pi4FirstIndex = i4Index;
            return (MBSM_SUCCESS);
        }
    }

    return (MBSM_FAILURE);
}

/****************************************************************************
 *                                                                          
 *    Function Name      : MbsmGetNextIndexSlotModuleMapTable              
 *                                                                          
 *    Description        : This function returns the next valid index of the
 *                         Slot Module Table from the given index.                                  
 *                                                                          
 *    Input(s)           : i4Index - Index of the Slot Module table         
 *                                                                          
 *    Output(s)          : pi4NextIndex - Next valid Index of the Table 
 *                                                                          
 *    Returns            : MBSM_SUCCESS or MBSM_FAILURE                     
 ****************************************************************************/
INT4
MbsmGetNextIndexSlotModuleMapTable (INT4 i4SlotIndex, INT4 *pi4NextIndex)
{
    INT4                i4Index;

    for (i4Index = i4SlotIndex + 1;
         i4Index < (MBSM_MAX_SLOTS + MBSM_SLOT_INDEX_START); i4Index++)
    {
        if (MbsmValidateIndexSlotModuleMapTable (i4Index) == MBSM_SUCCESS)
        {
            *pi4NextIndex = i4Index;
            return (MBSM_SUCCESS);
        }
    }

    return (MBSM_FAILURE);
}

/****************************************************************************
 *                                                                          
 *    Function Name      : MbsmValidateIndexCardTypeTable              
 *                                                                          
 *    Description        : This function validates the given index for the
 *                         Card Type Table.
 *                                                                          
 *    Input(s)           : i4CardIndex - Index of the Card Type table.         
 *                                                                          
 *    Output(s)          : None.
 *                                                                          
 *    Returns            : MBSM_SUCCESS or MBSM_FAILURE                     
 ****************************************************************************/
INT4
MbsmValidateIndexCardTypeTable (INT4 i4CardIndex)
{
    if ((!MBSM_CARD_TYPE_ENTRY (i4CardIndex)) ||
        (MBSM_CARD_TYPE_INFO_ROWSTATUS (i4CardIndex) != ACTIVE))
    {
        return (MBSM_FAILURE);
    }

    return (MBSM_SUCCESS);
}

/****************************************************************************
 *                                                                          
 *    Function Name      : MbsmGetFirstIndexCardTypeTable              
 *                                                                          
 *    Description        : This function returns the first valid index for
 *                         Card Type Table.
 *                                                                          
 *    Input(s)           : None.
 *                                                                          
 *    Output(s)          : pi4FirstIndex - First valid index for the table.
 *                                                                          
 *    Returns            : MBSM_SUCCESS or MBSM_FAILURE                     
 ****************************************************************************/
INT4
MbsmGetFirstIndexCardTypeTable (INT4 *pi4FirstIndex)
{
    INT4                i4Index;

    for (i4Index = 0; i4Index < MBSM_MAX_LC_TYPES; i4Index++)
    {
        if (MbsmValidateIndexCardTypeTable (i4Index) == MBSM_SUCCESS)
        {
            *pi4FirstIndex = i4Index;
            return (MBSM_SUCCESS);
        }
    }

    return (MBSM_FAILURE);
}

/****************************************************************************
 *                                                                          
 *    Function Name      : MbsmGetNextIndexCardTypeTable              
 *                                                                          
 *    Description        : This function returns the next valid index for
 *                         Card Type Table from the given index.
 *                                                                          
 *    Input(s)           : i4Index - Index in the Card Type table.
 *                                                                          
 *    Output(s)          : pi4NextIndex - Next valid index for the table.
 *                                                                          
 *    Returns            : MBSM_SUCCESS or MBSM_FAILURE                     
 ****************************************************************************/
INT4
MbsmGetNextIndexCardTypeTable (INT4 i4CardIndex, INT4 *pi4NextIndex)
{
    INT4                i4Index;

    for (i4Index = i4CardIndex + 1; i4Index < MBSM_MAX_LC_TYPES; i4Index++)
    {
        if (MbsmValidateIndexCardTypeTable (i4Index) == MBSM_SUCCESS)
        {
            *pi4NextIndex = i4Index;
            return (MBSM_SUCCESS);
        }
    }

    return (MBSM_FAILURE);
}

/****************************************************************************
 *                                                                          
 *    Function Name      : MbsmValidateIndexPortInfoTable              
 *                                                                          
 *    Description        : This function validates the given index for the
 *                         Port Info Table.
 *                                                                          
 *    Input(s)           : i4CardIndex - Index of the Port Info table.
 *                         i4PortIndex - Index of the Port in the Table.
 *                                                                          
 *    Output(s)          : None.
 *                                                                          
 *    Returns            : MBSM_SUCCESS or MBSM_FAILURE                     
 ****************************************************************************/
INT4
MbsmValidateIndexPortInfoTable (INT4 i4CardIndex, INT4 i4PortIndex)
{
    if ((!MBSM_CARD_TYPE_ENTRY (i4CardIndex)) ||
        (MBSM_CARD_TYPE_INFO_ROWSTATUS (i4CardIndex) != ACTIVE))
    {
        return (MBSM_FAILURE);
    }

    if ((i4PortIndex <= 0) ||
        ((UINT4) i4PortIndex > MBSM_CARD_TYPE_INFO_NUMPORTS (i4CardIndex)))
    {
        return (MBSM_FAILURE);
    }

    return (MBSM_SUCCESS);
}

/****************************************************************************
 *                                                                          
 *    Function Name      : MbsmGetFirstIndexPortInfoTable              
 *                                                                          
 *    Description        : This function returns the first valid index of the
 *                         Port Info Table.
 *                                                                          
 *    Input(s)           : None.
 *                                                                          
 *    Output(s)          : pi4CardIndex - Index of the Port Info table.
 *                         pi4PortIndex - Index of the Port in the Table.
 *                                                                          
 *    Returns            : MBSM_SUCCESS or MBSM_FAILURE                     
 ****************************************************************************/
INT4
MbsmGetFirstIndexPortInfoTable (INT4 *pi4FirstCardIndex,
                                INT4 *pi4FirstPortIndex)
{
    INT4                i4Index1;
    INT4                i4Index2;

    for (i4Index1 = 0; i4Index1 < MBSM_MAX_LC_TYPES; i4Index1++)
    {
        if (MbsmValidateIndexCardTypeTable (i4Index1) != MBSM_SUCCESS)
        {
            continue;
        }
        for (i4Index2 = 0;
             (UINT4) i4Index2 < MBSM_CARD_TYPE_INFO_NUMPORTS (i4Index1);
             i4Index2++)
        {
            if (MbsmValidateIndexPortInfoTable (i4Index1, i4Index2) ==
                MBSM_SUCCESS)
            {
                *pi4FirstCardIndex = i4Index1;
                *pi4FirstPortIndex = i4Index2;
                return (MBSM_SUCCESS);
            }
        }
    }

    return (MBSM_FAILURE);
}

/****************************************************************************
 *                                                                          
 *    Function Name      : MbsmGetNextIndexPortInfoTable              
 *                                                                          
 *    Description        : This function returns the next valid index of the
 *                         Port Info Table from the given index.
 *                                                                          
 *    Input(s)           : i4CardIndex - Index of the Card Type Table.
 *                         i4PortIndex - Index of the Port in the table.
 *                                                                          
 *    Output(s)          : pi4NextCardIndex - Index of the Port Info table.
 *                         pi4NextPortIndex - Index of the Port in the Table.
 *                                                                          
 *    Returns            : MBSM_SUCCESS or MBSM_FAILURE                     
 ****************************************************************************/
INT4
MbsmGetNextIndexPortInfoTable (INT4 i4CardIndex, INT4 i4PortIndex,
                               INT4 *pi4NextCardIndex, INT4 *pi4NextPortIndex)
{
    INT4                i4Index1;
    INT4                i4Index2;

    /* If this is the last port index in the given Line card, then move
     * to the next line card, else proceed with the same card index */
    if ((UINT4) i4PortIndex == MBSM_CARD_TYPE_INFO_NUMPORTS (i4CardIndex))
    {
        i4CardIndex++;
        i4PortIndex = 0;
    }
    else
    {
        i4PortIndex++;
    }

    for (i4Index1 = i4CardIndex; i4Index1 < MBSM_MAX_LC_TYPES; i4Index1++)
    {
        if (MbsmValidateIndexCardTypeTable (i4Index1) != MBSM_SUCCESS)
        {
            continue;
        }
        for (i4Index2 = i4PortIndex;
             (UINT4) i4Index2 <= MBSM_CARD_TYPE_INFO_NUMPORTS (i4Index1);
             i4Index2++)
        {
            if (MbsmValidateIndexPortInfoTable (i4Index1, i4Index2) ==
                MBSM_SUCCESS)
            {
                *pi4NextCardIndex = i4Index1;
                *pi4NextPortIndex = i4Index2;
                return (MBSM_SUCCESS);
            }
        }
    }

    return (MBSM_FAILURE);
}

/****************************************************************************
 *                                                                          
 *    Function Name      : MbsmValidateIndexLCConfigTable             
 *                                                                          
 *    Description        : This function validates the given index for the  
 *                         LC Config Table.
 *                                                                          
 *    Input(s)           : i4Index - Index of the LC Config Table.
 *                                                                          
 *    Output(s)          : None.
 *                                                                          
 *    Returns            : MBSM_SUCCESS or MBSM_FAILURE                     
 ****************************************************************************/
INT4
MbsmValidateIndexLCConfigTable (INT4 i4Index)
{
    /* There is no specifi table maintained for Pre-configuration
     * rather we use the Slot Module table for the same purpose */
    return (MbsmValidateIndexSlotModuleMapTable (i4Index));
}

/****************************************************************************
 *                                                                          
 *    Function Name      : MbsmGetFirstIndexLCConfigTable             
 *                                                                          
 *    Description        : This function returns the first valid index for  
 *                         LC Config Table.
 *                                                                          
 *    Input(s)           : None.
 *                                                                          
 *    Output(s)          : pi4FirstIndex - First valid index in the table.
 *                                                                          
 *    Returns            : MBSM_SUCCESS or MBSM_FAILURE                     
 ****************************************************************************/
INT4
MbsmGetFirstIndexLCConfigTable (INT4 *pi4FirstIndex)
{
    /* There is no specifi table maintained for Pre-configuration
     * rather we use the Slot Module table for the same purpose */
    INT4                i4Result;
    INT4                i4TempIndex;
    i4Result = MbsmGetFirstIndexSlotModuleMapTable (pi4FirstIndex);
    if (i4Result == MBSM_SUCCESS)
    {
        if (!MBSM_IS_LINE_CARD (MBSM_SLOT_INDEX_MODTYPE (*pi4FirstIndex)))
        {
            while (1)
            {
                if (nmhGetNextIndexMbsmLCConfigTable
                    (*pi4FirstIndex, &i4TempIndex) == SNMP_SUCCESS)
                {
                    if (MBSM_IS_LINE_CARD (MBSM_SLOT_INDEX_MODTYPE
                                           (i4TempIndex)))
                        break;
                }
                else
                {
                    return MBSM_FAILURE;
                }
                *pi4FirstIndex = i4TempIndex;
            }
            *pi4FirstIndex = i4TempIndex;
        }
    }
    else
    {
        return MBSM_FAILURE;
    }
    return (MBSM_SUCCESS);
}

/****************************************************************************
 *                                                                          
 *    Function Name      : MbsmGetNextIndexLCConfigTable             
 *                                                                          
 *    Description        : This function returns the next valid index for  
 *                         LC Config Table from the given index.
 *                                                                          
 *    Input(s)           : i4Index - Index of the table.
 *                                                                          
 *    Output(s)          : pi4NextIndex - Next valid index in the table.
 *                                                                          
 *    Returns            : MBSM_SUCCESS or MBSM_FAILURE                     
 ****************************************************************************/
INT4
MbsmGetNextIndexLCConfigTable (INT4 i4Index, INT4 *pi4NextIndex)
{
    /* There is no specifi table maintained for Pre-configuration
     * rather we use the Slot Module table for the same purpose */
    INT4                i4Result;
    INT4                i4CardType, i4TempIndex;
    i4Result = MbsmGetNextIndexSlotModuleMapTable (i4Index, pi4NextIndex);
    if (i4Result == MBSM_SUCCESS)
    {
        i4CardType = MBSM_SLOT_INDEX_CARDTYPE (*pi4NextIndex);
        if (i4CardType == MBSM_INVALID_CARDTYPE)
        {
            while (1)
            {
                if (MbsmGetNextIndexSlotModuleMapTable
                    (*pi4NextIndex, &i4TempIndex) == MBSM_SUCCESS)
                {
                    if (!(MBSM_SLOT_INDEX_CARDTYPE (i4TempIndex) ==
                          MBSM_INVALID_CARDTYPE))
                        break;
                }
                else
                {
                    return MBSM_FAILURE;
                }
                *pi4NextIndex = i4TempIndex;
            }
            *pi4NextIndex = i4TempIndex;

        }
    }
    else
    {
        return (MBSM_FAILURE);
    }
    return (MBSM_SUCCESS);
}
