/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mbsport.c,v 1.26 2016/06/14 12:30:24 siva Exp $
 *
 * Description:This file contains the routines for static   
 *             initialisations for MBSM Tables.             
 *
 *******************************************************************/
#include "mbsminc.h"
#include "cfanp.h"
/****************************************************************************/
/*                                                                          */
/*    Function Name      : MbsmGetSlotModuleInfoFromFile                    */
/*                                                                          */
/*    Description        : This function initialises the Slot vs. Module    */
/*                         information from a init File(NVRAM).             */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : MBSM_SUCCESS or MBSM_FAILURE                     */
/****************************************************************************/
INT4
MbsmGetSlotModuleInfoFromFile (VOID)
{
#define MBSM_MAX_COLUMN_LENGTH 80
    INT4                i4Index;
    INT4                i4CardIndex;
    INT4                i4SlotId = MBSM_INVALID_SLOT_ID;
    UINT4               u4StartIfIndex = MBSM_INVALID_IFINDEX;
    UINT4               u4EndIfIndex = MBSM_INVALID_IFINDEX;
    INT4                i4SwitchId = 0;
    UINT4               u4NumPorts = MBSM_INVALID_PORTCOUNT;
    UINT4               u4Index = 0;
    UINT4               u4StackingModel = ISS_STACKING_MODEL_NONE;
    UINT4               u4ConnectingPortCount = 0;
    FILE               *fp = NULL;
    char                au1Buffer[MBSM_MAX_COLUMN_LENGTH];
    UINT1              *pTemp = NULL;
    UINT1              *pToken = NULL;
    UINT1              *pRemStr = NULL;
    UINT4               u4ConnectingPortIfIndex = 0;
    tHwPortInfo         HwPortInfo;
    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    fp = fopen (MBSM_SLOT_MODULE_FILE, "r");
    if (fp == NULL)
    {
        MBSM_DBG (MBSM_RED_TRC, MBSM_RED,
                  "\n [ERROR]: Failed to open MBSM INIT FILE\n");
        return MBSM_FAILURE;
    }

    for (i4Index = MBSM_SLOT_INDEX_START;
         i4Index < (MBSM_MAX_SLOTS + MBSM_SLOT_INDEX_START); i4Index++)
    {
        MEMSET (au1Buffer, 0, MBSM_MAX_COLUMN_LENGTH);
        if ((fgets (au1Buffer, MBSM_MAX_COLUMN_LENGTH, fp)) == NULL)
        {
            break;
        }
        pTemp = (UINT1 *) au1Buffer;

        pRemStr = pTemp;
        pToken = pTemp;

        /* Read the Slot Id */
        if (MbsmGetNextToken (pTemp, &pToken, &pRemStr) == MBSM_FAILURE)
        {
            MBSM_DBG (MBSM_RED_TRC, MBSM_RED,
                      "\n [ERROR]: Failed to read Slot Id\n");
            break;
        }

        i4SlotId = (INT4) ATOI (pToken);

        if (!MBSM_IS_SLOT_ID_VALID (i4SlotId))
        {
            MBSM_DBG (MBSM_RED_TRC, MBSM_RED,
                      "\n [ERROR]: Failed to read Slot Id\n");
            break;
        }
        else
        {
            MBSM_SLOT_INDEX_SLOT_ID (i4SlotId) = i4SlotId;
        }

        if (!pRemStr)
        {
            MBSM_DBG (MBSM_RED_TRC, MBSM_RED,
                      "\n [ERROR]: Failed to read Module Type\n");
            break;
        }

        pTemp = pRemStr;
        /* Read the Card Type */
        if (MbsmGetNextToken (pTemp, &pToken, &pRemStr) == MBSM_FAILURE)
        {
            MBSM_DBG (MBSM_RED_TRC, MBSM_RED,
                      "\n [ERROR]: Failed to read Module Type\n");
            break;
        }

        if (!STRCASECMP (pToken, "LC"))
        {
            MBSM_SLOT_INDEX_MODTYPE (i4SlotId) = MBSM_LINE_CARD;
            if (MbsmGetStartIfIndex (i4SlotId, (UINT4 *) &u4StartIfIndex)
                != MBSM_SUCCESS)
            {
                MBSM_DBG (MBSM_RED_TRC, MBSM_RED,
                          "\n [ERROR]: Unable to calculate StartIfIndex\n");
                break;
            }
            MBSM_SLOT_INDEX_STARTINDEX (i4SlotId) = u4StartIfIndex;
        }
        else if (!STRCASECMP (pToken, "CC"))
        {
            MBSM_SLOT_INDEX_MODTYPE (i4SlotId) = MBSM_CONTROL_CARD;
            MBSM_SLOT_INDEX_PREVSTATUS (i4SlotId) =
                MBSM_SLOT_INDEX_STATUS (i4SlotId);

            MBSM_SLOT_INDEX_STATUS (i4SlotId) = MBSM_STATUS_NP;
        }
        else
        {
            MBSM_DBG (MBSM_RED_TRC, MBSM_RED,
                      "\n [ERROR]: Invalid Module Type\n");
            break;
        }
        if (!pRemStr)
        {
            continue;
        }

        pTemp = pRemStr;
        /* Read the Card Name */
        if (MbsmGetNextToken (pTemp, &pToken, &pRemStr) == MBSM_FAILURE)
        {
            continue;
        }

        if (MbsmGetCardIndexFromCardName ((INT1 *) pToken, &i4CardIndex)
            == MBSM_FAILURE)
        {
            MBSM_DBG (MBSM_RED_TRC, MBSM_RED,
                      "\n [ERROR]: Unknown CardName, Setting to INVALID type\n");
            continue;
        }

        if (!pRemStr)
        {
            continue;
        }

        pTemp = pRemStr;
        /* Read the IsPreConf Flag */
        if (MbsmGetNextToken (pTemp, &pToken, &pRemStr) == MBSM_FAILURE)
        {
            MBSM_SLOT_INDEX_ISPRECONF (i4SlotId) = MBSM_FALSE;
            continue;
        }

        if (!STRCASECMP (pToken, "TRUE"))
        {
            MBSM_SLOT_INDEX_ISPRECONF (i4SlotId) = MBSM_TRUE;
        }
        else if (!STRCASECMP (pToken, "FALSE"))
        {
            MBSM_SLOT_INDEX_ISPRECONF (i4SlotId) = MBSM_FALSE;
        }
        else
        {
             MBSM_DBG (MBSM_RED_TRC, MBSM_RED, 
                       "\n [ERROR]: Unknown Value for PreConfig Flag,"
                       " Setting to FALSE\n");

            MBSM_SLOT_INDEX_ISPRECONF (i4SlotId) = MBSM_FALSE;
        }
        u4StackingModel = ISS_GET_STACKING_MODEL ();
        if ((u4StackingModel == ISS_CTRL_PLANE_STACKING_MODEL) ||
            (u4StackingModel == ISS_DISS_STACKING_MODEL))
        {
            /* For Dual Unit Stacking ,Number of Connecting Ports in previous slot 
               should be used to calculate the Start IfIndex  of current Slot */
            pTemp = pRemStr;
            /* Read the Total Number of ports supported in Board */
            if (MbsmGetNextToken (pTemp, &pToken, &pRemStr) == MBSM_FAILURE)
            {
                PRINTF
                    ("\n [ERROR]: Unable to retrieve Maximum number of ports \
                     in Hardware \n");
                break;
            }
            u4NumPorts = (UINT4) ATOI (pToken);
            /* Read the Interconnecting Hardware Port Number */
            pTemp = pRemStr;
            /* Read the Total Number of ports supported in Board */
            if (MbsmGetNextToken (pTemp, &pToken, &pRemStr) == MBSM_FAILURE)
            {
                PRINTF ("\n [ERROR]: Unable to retrieve Interconnecting port \
                 in Hardware \n");
                break;
            }
            pToken = (UINT1 *) STRTOK (pToken, ";");
            while (pToken != NULL)
            {
                if ((INT4) u4ConnectingPortCount <
                    SYS_DEF_MAX_INFRA_SYS_PORT_COUNT)
                {
                    MBSM_SLOT_INDEX_CONNECTINGPORT_UNIT (i4SlotId,
                                                         u4ConnectingPortCount)
                        = ATOI (&pToken[0]);
                    MBSM_SLOT_INDEX_CONNECTINGPORT (i4SlotId,
                                                    u4ConnectingPortCount) =
                        ATOI (&pToken[2]);
                    pToken = (UINT1 *) STRTOK (NULL, ";");
                    u4ConnectingPortCount++;
                }
                else
                    break;
            }
            if (u4ConnectingPortCount == 0)
            {
                PRINTF
                    ("Connecting Port Should be given in Unit-Port; format \n");
                break;
            }
        }
        else
        {
            u4NumPorts = MBSM_CARD_TYPE_INFO_NUMPORTS (i4CardIndex);
        }
        MBSM_SLOT_INDEX_CARDTYPE (i4SlotId) = i4CardIndex;
        MBSM_SLOT_INDEX_NUMPORTS (i4SlotId) = u4NumPorts;
        MBSM_PORT_INDEX_STARTINDEX (i4SlotId) = u4StartIfIndex;
        MBSM_SLOT_INDEX_CONNECTING_PORT_COUNT (i4SlotId) =
            u4ConnectingPortCount;
        MBSM_PORT_INDEX_CONNECTING_PORT_COUNT (i4SlotId) =
            u4ConnectingPortCount;
        MBSM_PORT_INDEX_PORTCOUNT (i4SlotId) = u4NumPorts;
        u4EndIfIndex = u4StartIfIndex + u4NumPorts - 1;
        for (u4Index = 0; u4Index < u4NumPorts; u4Index++)
        {
            /* Assumption is that ports are continous and there will gaps
             * between ports */
            MBSM_SET_MEMBER_PORTLIST (MBSM_PORT_INDEX_PORTLIST
                                      (i4SlotId), (u4StartIfIndex + u4Index));
        }

        /* Modify the status to NP only when the Cardtype
           on the Slot is known, since the Slot is otherwise
           unusable. 
         */
        MBSM_SLOT_INDEX_PREVSTATUS (i4SlotId) =
            MBSM_SLOT_INDEX_STATUS (i4SlotId);

        MBSM_SLOT_INDEX_STATUS (i4SlotId) = MBSM_STATUS_NP;
        i4SwitchId = IssGetSwitchid ();
        if (i4SwitchId == i4SlotId)
        {
            HwPortInfo.u4StartIfIndex = u4StartIfIndex;
            HwPortInfo.u4EndIfIndex = u4EndIfIndex;
            HwPortInfo.u4NumConnectingPorts = u4ConnectingPortCount;
            u4ConnectingPortIfIndex = u4EndIfIndex + 1;
            for (u4Index = 0; u4Index < u4ConnectingPortCount; u4Index++)
            {
                HwPortInfo.au4ConnectingPort[u4Index] =
                    MBSM_SLOT_INDEX_CONNECTINGPORT (i4SlotId, u4Index);

                HwPortInfo.au4ConnectingUnit[u4Index] =
                    MBSM_SLOT_INDEX_CONNECTINGPORT_UNIT (i4SlotId, u4Index);

                HwPortInfo.au4ConnectingPortIfIndex[u4Index] =
                    u4ConnectingPortIfIndex;

                u4ConnectingPortIfIndex++;

            }
            CfaCfaNpSetHwPortInfo (HwPortInfo);
            CfaSetLocalUnitPortInformation (&HwPortInfo);
        }

        /* 
         * There are cases where in Line cards with no front panel ports 
         * are supported. Such cards are classified as Fabric type and 
         * processing will differ accordingly.
         */
        if ((MBSM_SLOT_INDEX_MODTYPE (i4SlotId) == MBSM_LINE_CARD) &&
            (MBSM_SLOT_INDEX_NUMPORTS (i4SlotId) == 0))
        {
            MBSM_SLOT_INDEX_FABRIC_TYPE (i4SlotId) = MBSM_TRUE;
        }
        u4ConnectingPortCount = 0;
    }

    fclose (fp);
    for (i4Index = MBSM_SLOT_INDEX_START;
         i4Index < (MBSM_MAX_SLOTS + MBSM_SLOT_INDEX_START); i4Index++)
    {
        if (MBSM_SLOT_INDEX_MODTYPE (i4Index) == MBSM_INVALID_MODTYPE)
        {
             MBSM_DBG (MBSM_PROTO_TRC, MBSM_RED,
                       "\n [ERROR]: Incomplete slot information\n");
            return MBSM_FAILURE;
        }
    }
    return (MBSM_SUCCESS);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MbsmGetNextToken                                 */
/*                                                                          */
/*    Description        : This function parses the give string and extracts*/
/*                         tokens from the string separated by spaces.      */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : MBSM_SUCCESS or MBSM_FAILURE                     */
/****************************************************************************/
INT4
MbsmGetNextToken (UINT1 *pu1Str, UINT1 **pu1Token, UINT1 **pu1RemStr)
{
    UINT1              *pTemp;
    UINT1              *pToken;

    if ((!pu1Str) || (!(*pu1Str)))
    {
        return (MBSM_FAILURE);
    }
    pTemp = pu1Str;

    while ((*pTemp) && ((*pTemp == ' ') || (*pTemp == '\t')))
    {
        pTemp++;
    }
    pToken = pTemp;

    if ((!(*pTemp)) || (*pTemp == '\n'))
    {
        /* String contains just space and Tab characters */
        return (MBSM_FAILURE);
    }

    while (((*pTemp)) && (*pTemp != ' ') && (*pTemp != '\t')
           && (*pTemp != '\n'))
    {
        pTemp++;
    }

    if ((!(*pTemp)) || (*pTemp == '\n'))
    {
        *pu1RemStr = NULL;
        *pTemp = '\0';
        *pu1Token = pToken;
    }
    else
    {
        *pTemp = '\0';
        pTemp++;
        *pu1RemStr = pTemp;
        *pu1Token = pToken;
    }

    return (MBSM_SUCCESS);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MbsmWriteSlotModuleInfoToFile                    */
/*                                                                          */
/*    Description        : This function writes the Slot Module information */
/*                         to the File(NVRAM).                              */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : MBSM_SUCCESS or MBSM_FAILURE                     */
/****************************************************************************/
INT4
MbsmWriteSlotModuleInfoToFile (VOID)
{
    FILE               *fp = NULL;
    INT4                i4Index;
    UINT4               u4Index = 0;
    UINT4               u4StackingModel = ISS_STACKING_MODEL_NONE;
    u4StackingModel = ISS_GET_STACKING_MODEL ();
    fp = fopen (MBSM_SLOT_MODULE_FILE, "w");
    if (fp == NULL)
    {
        return MBSM_FAILURE;
    }

    for (i4Index = MBSM_SLOT_INDEX_START;
         i4Index < (MBSM_MAX_SLOTS + MBSM_SLOT_INDEX_START); i4Index++)
    {
        fprintf (fp, "%d ", MBSM_SLOT_INDEX_SLOT_ID (i4Index));
        switch (MBSM_SLOT_INDEX_MODTYPE (i4Index))
        {
            case MBSM_LINE_CARD:
                fprintf (fp, "%s", "LC");
                break;

            case MBSM_CONTROL_CARD:
                fprintf (fp, "%s", "CC");
                break;

            default:
                fprintf (fp, "%s", "INVALID");
                break;
        }
        if (MBSM_SLOT_INDEX_CARDTYPE (i4Index) != MBSM_INVALID_CARDTYPE)
        {
            fprintf (fp, " %s",
                     MBSM_CARD_TYPE_INFO_CARDNAME (MBSM_SLOT_INDEX_CARDTYPE
                                                   (i4Index)));
            if (MBSM_SLOT_INDEX_ISPRECONF (i4Index) == MBSM_TRUE)
            {
                fprintf (fp, " %s", "TRUE");
            }
            else
            {
                fprintf (fp, " %s", "FALSE");
            }
            if ((u4StackingModel == ISS_CTRL_PLANE_STACKING_MODEL) ||
                (u4StackingModel == ISS_DISS_STACKING_MODEL))
            {
                fprintf (fp, " %d", MBSM_SLOT_INDEX_NUMPORTS (i4Index));
                fprintf (fp, " ");
                for (u4Index = 0;
                     u4Index < MBSM_PORT_INDEX_CONNECTING_PORT_COUNT (i4Index);
                     u4Index++)
                {
                    fprintf (fp, "%d-%d;",
                             MBSM_SLOT_INDEX_CONNECTINGPORT_UNIT (i4Index,
                                                                  u4Index),
                             MBSM_SLOT_INDEX_CONNECTINGPORT (i4Index, u4Index));
                }
            }
            fprintf (fp, "\n");
        }
        else
        {
            fprintf (fp, "\n");
        }
    }

    fflush (fp);
    fclose (fp);
    return MBSM_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MbsmGetSlotIdFromFile                            */
/*                                                                          */
/*    Description        : This function retrieves the SlotId               */
/*                         from file /proc/slotid                           */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : pi4RetSlotId - return SlotId                     */
/*                                                                          */
/*    Returns            : MBSM_SUCCESS or MBSM_FAILURE                     */
/****************************************************************************/
INT4
MbsmGetSlotIdFromFile (INT4 *pi4RetSlotId)
{
    INT4                i4SlotId = 0;
    INT4                i4Retval;
    FILE               *pRetFd;

#ifndef MBSM_SLOT_ID_FILE
#ifdef STACKING_WANTED
#define MBSM_SLOT_ID_FILE               "nodeid"
#else
#define MBSM_SLOT_ID_FILE               "/proc/slotid"
#endif
#endif

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        *pi4RetSlotId = MBSM_SELF_SLOT_ID ();
    }
    else
    {
        if ((pRetFd = fopen (MBSM_SLOT_ID_FILE, "rb")) == NULL)
        {
            MBSM_DBG (MBSM_RED_TRC, MBSM_RED,
                      "\n [ERROR]: Failed to open Slot Id FILE\n");
            return MBSM_FAILURE;
        }

        i4Retval = fscanf (pRetFd, "%d", &i4SlotId);

        fclose (pRetFd);

        *pi4RetSlotId = i4SlotId;
    }
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MbsmRmEnqChkSumMsgToRm                               */
/*                                                                           */
/* Description        : This function calls the RM Module to enqueue the     */
/*                      message from applications to RM task.                */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : MBSM_SUCCESS/MBSM_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
MbsmRmEnqChkSumMsgToRm (UINT2 u2AppId, UINT2 u2ChkSum)
{
#ifdef RM_WANTED
    if ((RmEnqChkSumMsgToRmFromApp (u2AppId, u2ChkSum)) == RM_FAILURE)
    {
        return MBSM_FAILURE;
    }
#else
    UNUSED_PARAM (u2AppId);
    UNUSED_PARAM (u2ChkSum);
#endif
    return MBSM_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MbsmGetSlotIdFromConnectingPort                  */
/*                                                                          */
/*    Description        : This function returns the SlotId corresponding   */
/*                         to the connecting port.                          */
/*                                                                          */
/*    Input(s)           : u4ConnectingPort                                 */
/*                                                                          */
/*    Output(s)          :  pi4SlotId - SlotId                              */
/*                                                                          */
/*    Returns            : MBSM_SUCCESS or MBSM_FAILURE                     */
/****************************************************************************/
INT4
MbsmGetSlotIdFromConnectingPort (UINT4 u4ConnectingPort, UINT4 *pu4SlotId)
{
    tHwPortInfo         HwPortInfo;
    INT4                i4Index = 0;

    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    CfaGetLocalUnitPortInformation (&HwPortInfo);

    for (i4Index = 0; i4Index < SYS_DEF_MAX_INFRA_SYS_PORT_COUNT; i4Index++)
    {
        if (HwPortInfo.au4ConnectingPortIfIndex[i4Index] == u4ConnectingPort)
        {
            *pu4SlotId = HwPortInfo.au4ConnectingUnit[i4Index];
            return MBSM_SUCCESS;
        }
    }
    return MBSM_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MbsmGetConnectingPortFromSlotId                  */
/*                                                                          */
/*    Description        : This function returns the connecting port        */
/*                         corresponding to the SlotId.                     */
/*                                                                          */
/*    Input(s)           : u4SlotId                                         */
/*                                                                          */
/*    Output(s)          : pu4ConnectingPort - connecting port              */
/*                                                                          */
/*    Returns            : MBSM_SUCCESS or MBSM_FAILURE                     */
/****************************************************************************/
INT4
MbsmGetConnectingPortFromSlotId (UINT4 u4SlotId, UINT4 *pu4ConnectingPort)
{
    tHwPortInfo         HwPortInfo;
    INT4                i4Index = 0;

    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    CfaGetLocalUnitPortInformation (&HwPortInfo);

    for (i4Index = 0; i4Index < SYS_DEF_MAX_INFRA_SYS_PORT_COUNT; i4Index++)
    {
        if (HwPortInfo.au4ConnectingUnit[i4Index] == u4SlotId)
        {
            *pu4ConnectingPort = HwPortInfo.au4ConnectingPortIfIndex[i4Index];
            return MBSM_SUCCESS;
        }
    }
    return MBSM_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MbsmGetDissMasterSlotId                          */
/*                                                                          */
/*    Description        : This function returns the Master SlotId          */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : pu4SlotId - SlotId of Master Node                */
/*                                                                          */
/*    Returns            : MBSM_SUCCESS or MBSM_FAILURE                     */
/****************************************************************************/
VOID
MbsmGetDissMasterSlotId (UINT4 *pu4SlotId)
{
    /* MASTER Slot Id should be configured as 0 */
    *pu4SlotId = 0;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MbsmGetDissRoleFromSlotId                        */
/*                                                                          */
/*    Description        : This function returns the DISS Role corresponding*/
/*                         to the slotId.                                   */
/*                                                                          */
/*    Input(s)           : u4SlotId                                         */
/*                                                                          */
/*    Output(s)          : pu1DissRole - DISS Role                          */
/*                                                                          */
/*    Returns            : MBSM_SUCCESS or MBSM_FAILURE                     */
/****************************************************************************/
VOID
MbsmGetDissRoleFromSlotId (UINT4 u4SlotId, UINT1 *pu1DissRole)
{
    if (u4SlotId == 0)
    {
        *pu1DissRole = ISS_DISS_ROLE_MASTER;
    }
    else
    {
        *pu1DissRole = ISS_DISS_ROLE_SLAVE;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MbsmIsConnectingPort                             */
/*                                                                          */
/*    Description        : This function returns if the port is a           */
/*                         connecting port or not.                          */
/*                                                                          */
/*    Input(s)           : u4IfIndex - ifIndex                              */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : MBSM_TRUE or MBSM_FALSE                          */
/****************************************************************************/
INT4
MbsmIsConnectingPort (UINT4 u4IfIndex)
{
    tHwPortInfo         HwPortInfo;
    UINT4               u4StackingModel = ISS_STACKING_MODEL_NONE;
    INT4                i4Index = 0;

    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    CfaGetLocalUnitPortInformation (&HwPortInfo);

    u4StackingModel = ISS_GET_STACKING_MODEL ();

    if (u4StackingModel == ISS_CTRL_PLANE_STACKING_MODEL)
    {
        if (HwPortInfo.au4ConnectingPortIfIndex[0] == u4IfIndex)
        {
            return MBSM_TRUE;
        }
    }
    else if (u4StackingModel == ISS_DISS_STACKING_MODEL)
    {
        for (i4Index = 0; i4Index < SYS_DEF_MAX_INFRA_SYS_PORT_COUNT; i4Index++)
        {
            if (HwPortInfo.au4ConnectingPortIfIndex[i4Index] == u4IfIndex)
            {
                return MBSM_TRUE;
            }
        }
    }
    return MBSM_FALSE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MbsmGetSlotIdFromRemotePort                      */
/*                                                                          */
/*    Description        : This function returns the SlotId corresponding   */
/*                         to the remote port.                              */
/*                                                                          */
/*    Input(s)           : u4RemotePort                                     */
/*                                                                          */
/*    Output(s)          : pi4SlotId - SlotId                               */
/*                                                                          */
/*    Returns            : MBSM_SUCCESS or MBSM_FAILURE                     */
/****************************************************************************/
INT4
MbsmGetSlotIdFromRemotePort (UINT4 u4RemotePort, UINT4 *pu4SlotId)
{
    UINT4               u4Index = 0;

    for (u4Index = MBSM_SLOT_INDEX_START;
         u4Index < (MBSM_MAX_SLOTS + MBSM_SLOT_INDEX_START); u4Index++)
    {
        if ((gMbsmGlobalInfo.gaMbsmSlotMapTable != NULL) &&
            (MBSM_SLOT_MAP_ENTRY (u4Index) != NULL))
        {
            if ((u4RemotePort >= MBSM_SLOT_INDEX_STARTINDEX (u4Index)) &&
                (u4RemotePort <= (MBSM_SLOT_INDEX_STARTINDEX (u4Index) +
                                  MBSM_SLOT_INDEX_NUMPORTS (u4Index) - 1)))
            {
                *pu4SlotId = u4Index;
                return MBSM_SUCCESS;
            }
        }
    }
    return MBSM_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MbsmGetSlotIndexStatus                           */
/*                                                                          */
/*    Description        : This function returns the status of the          */
/*                         given SlotId                                     */
/*                                                                          */
/*    Input(s)           : SlotId                                           */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : MBSM_STATUS_DOWN/ MBSM_STATUS_ACTIVE             */
/*                         MBSM_STATUS_INACTIVE/ MBSM_STATUS_NP             */
/****************************************************************************/
INT4
MbsmGetSlotIndexStatus (INT4 i4SlotId)
{

    if ((MBSM_VALIDATE_SLOT_ID ((i4SlotId - MBSM_SLOT_INDEX_START)) ==
         FNP_FAILURE) ||
        (gMbsmGlobalInfo.gaMbsmSlotMapTable == NULL) ||
        (MBSM_SLOT_MAP_ENTRY (i4SlotId) == NULL))
    {
        return MBSM_STATUS_INACTIVE;
    }
    else
    {
        return (MBSM_SLOT_INDEX_STATUS (i4SlotId));
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MbsmGetSlotsFromRemotePorts                      */
/*                                                                          */
/*    Description        : This function returns the SlotList corresponding */
/*                         to the remote ports.                             */
/*                                                                          */
/*    Input(s)           : pPortList                                        */
/*                                                                          */
/*    Output(s)          : pu1SlotList - Pointer to SlotId                  */
/*                                                                          */
/*    Returns            : MBSM_SUCCESS or MBSM_FAILURE                     */
/****************************************************************************/
VOID
MbsmGetSlotsFromRemotePorts (UINT1 *pPortList, UINT1 *pu1SlotList)
{
    UINT4               u4SlotId = MBSM_INVALID_SLOT_ID;
    UINT2               u4IfIdx = 0;
    BOOL1               bRetVal;

    for (u4IfIdx = 1; u4IfIdx < (SYS_DEF_MAX_PHYSICAL_INTERFACES +
                                 LA_MAX_AGG_INTF); u4IfIdx++)
    {
        OSIX_BITLIST_IS_BIT_SET (pPortList, u4IfIdx, BRG_PORT_LIST_SIZE,
                                 bRetVal);
        if (bRetVal == OSIX_TRUE)
        {
            MbsmGetSlotIdFromRemotePort (u4IfIdx, &u4SlotId);
            OSIX_BITLIST_IS_BIT_SET (pu1SlotList, (UINT2) u4SlotId,
                                     MBSM_MAX_SLOTS, bRetVal);
            if (bRetVal != OSIX_TRUE)
            {
                OSIX_BITLIST_SET_BIT (pu1SlotList, (UINT2) u4SlotId,
                                      MBSM_MAX_SLOTS);
            }
        }
    }
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MbsmSetMacAddressToSlotId                        */
/*                                                                          */
/*    Description        : This function sets the MacAddress mapping        */
/*                         for the given SlotId                                */
/*                                                                          */
/*    Input(s)           : SlotId,MacAddress                                */
/*                                                                          */
/*    Output(s)          : NONE                                             */
/*                                                                          */
/*    Returns            : MBSM_SUCCESS or MBSM_FAILURE                     */
/****************************************************************************/
INT4
MbsmSetMacAddressToSlotId (tMacAddr DestMacAddr, UINT4 u4SlotId)
{

    if ((gMbsmGlobalInfo.gaMbsmSlotMapTable != NULL) &&
        (MBSM_SLOT_MAP_ENTRY (u4SlotId) != NULL))
    {
        MEMCPY (MBSM_SLOT_MAP_ENTRY (u4SlotId)->SlotBaseMacAddr,
                DestMacAddr, sizeof (tMacAddr));
        return MBSM_SUCCESS;
    }
    else
    {
        MBSM_DBG_ARG1 (MBSM_PROTO_TRC, MBSM_RED,
                       "MBSM_SLOT_MAP_ENTRY is NULL for (slot %d)\r\n",
                       u4SlotId);
        return MBSM_FAILURE;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MbsmGetMacAddressFromSlotId                      */
/*                                                                          */
/*    Description        : This function returns the MacAddress of the      */
/*                          given SlotI                                      */
/*                                                                          */
/*    Input(s)           : SlotId                                           */
/*                                                                          */
/*    Output(s)          : MacAddress                                       */
/*                                                                          */
/*    Returns            : MBSM_SUCCESS or MBSM_FAILURE                     */
/****************************************************************************/
INT4
MbsmGetMacAddressFromSlotId (UINT4 u4SlotId, tMacAddr * pDestMacAddr)
{

    if ((gMbsmGlobalInfo.gaMbsmSlotMapTable != NULL) &&
        (MBSM_SLOT_MAP_ENTRY (u4SlotId) != NULL))
    {
        MEMCPY (pDestMacAddr, MBSM_SLOT_MAP_ENTRY (u4SlotId)->SlotBaseMacAddr,
                sizeof (tMacAddr));
        return MBSM_SUCCESS;
    }
    else
    {
        MBSM_DBG_ARG1 (MBSM_PROTO_TRC, MBSM_RED,
                       "MBSM_SLOT_MAP_ENTRY is NULL for (slot %d)\r\n",
                       u4SlotId);
        return MBSM_FAILURE;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MbsmGetSlotIdFromMacAddress                      */
/*                                                                          */
/*    Description        : This function returns the SlotId for the given   */
/*                          Mac Address                                     */
/*                                                                          */
/*    Input(s)           : SlotId                                           */
/*                                                                          */
/*    Output(s)          : MacAddress                                       */
/*                                                                          */
/*    Returns            : MBSM_SUCCESS or MBSM_FAILURE                     */
/****************************************************************************/
INT4
MbsmGetSlotIdFromMacAddress (tMacAddr MacAddr, UINT4 *pu4SlotId)
{
    UINT4               u4SlotId = FNP_ZERO;

    for (u4SlotId = MBSM_SLOT_INDEX_START; u4SlotId <= MBSM_MAX_SLOTS;
         u4SlotId++)
    {
        if ((gMbsmGlobalInfo.gaMbsmSlotMapTable != NULL) &&
            (MBSM_SLOT_MAP_ENTRY (u4SlotId) != NULL))
        {
            if (MEMCMP
                (MacAddr, MBSM_SLOT_MAP_ENTRY (u4SlotId)->SlotBaseMacAddr,
                 sizeof (tMacAddr)) == 0)
            {
                *pu4SlotId = u4SlotId;
                return MBSM_SUCCESS;
            }
        }
    }
    return MBSM_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MbsmGetPeerNodeCount                             */
/*                                                                          */
/*    Description        : This function returns the SlotId for the given   */
/*                          Mac Address                                     */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : MacAddress                                       */
/*                                                                          */
/*    Returns            : MBSM_SUCCESS or MBSM_FAILURE                     */
/****************************************************************************/
INT4
MbsmGetPeerNodeCount (VOID)
{
    UINT4               u4SlotCount = 0;
    UINT4               u4SlotId = 0;
    for (u4SlotId = MBSM_SLOT_INDEX_START; u4SlotId < MBSM_MAX_SLOTS;
         u4SlotId++)
    {
        if (((UINT4) IssGetSwitchid () != u4SlotId) &&
            (gMbsmGlobalInfo.gaMbsmSlotMapTable != NULL) &&
            (MBSM_SLOT_MAP_ENTRY (u4SlotId) != NULL) &&
            (MbsmGetSlotIndexStatus (u4SlotId) == MBSM_STATUS_ACTIVE))
        {
            u4SlotCount++;
        }
    }
    return u4SlotCount;
}

/*****************************************************************************/
/* Function Name      : MbsmGetBridgeIndexFromIfIndex                        */
/*                                                                           */
/* Description        : This function converts the CFA Index to              */
/*                      Bridge Index by referring MBSM Interface Map Table   */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pu4BrgIndex - pointer to Bridge Index                */
/*                                                                           */
/* Return Value(s)    : MBSM_FAILURE - On Failure                            */
/*                      MBSM_SUCCESS - On Success                            */
/*****************************************************************************/
INT4
MbsmGetBridgeIndexFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4BrgIndex)
{
    *pu4BrgIndex = u4IfIndex;
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MbsmGetIfIndexFromBridgeIndex                        */
/*                                                                           */
/* Description        : This function converts the Bridge Index to           */
/*                      CFA Index by referring MBSM Interface Map Table      */
/*                                                                           */
/* Input(s)           : u4BrgIndex - Bridge index                            */
/*                                                                           */
/* Output(s)          : pu4IfIndex - Pointer to Interface Index              */
/*                                                                           */
/* Return Value(s)    : MBSM_FAILURE - On Failure                            */
/*                      MBSM_SUCCESS - On Success                            */
/*****************************************************************************/
INT4
MbsmGetIfIndexFromBridgeIndex (UINT4 u4BrgIndex, UINT4 *pu4IfIndex)
{
    *pu4IfIndex = u4BrgIndex;
    return MBSM_SUCCESS;
}
