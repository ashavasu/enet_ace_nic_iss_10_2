/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mbstopo.c,v 1.23 2014/07/17 13:12:55 siva Exp $
 *
 * Description:This file contains the routines responsible  
 *             for updating the hardware topology tables.  
 *
 *******************************************************************/
#include "mbsminc.h"

#ifdef NPAPI_WANTED
#include "npapi.h"
#include "mbsnp.h"
#endif /* NPAPI_WANTED */

/*****************************************************************************
 *
 *    Function Name        : MbsmFormMsgToCfa
 *
 *    Description          : This function forms a message containing Slot    
 *                           information to be posted to CFA for interface   
 *                           creation for ports in the slot.
 *
 *    Input(s)             : pCfaMsg - Pointer to the message.
 *                           i4SlotId - Slot Id for which the message is
 *                           formed.
 *
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gaMbsmSlotMapTable.            
 *
 *    Global Variables Modified : None.            
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns              : MBSM_SUCCESS/MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmFormMsgToCfa (INT4 i4SlotId, tMbsmCfaHwInfoMsg * pMsg, INT2 i2OffSet)
{
    tMbsmSlotPortInfo  *pTempPtr;

    if (!pMsg)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_PROTO,
                  "Error in MbsmFormMsgToCfa - NULL Message - FAILURE.\n");
        return (MBSM_FAILURE);
    }

    if (i2OffSet < 0)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_PROTO,
                  "Error in MbsmFormMsgToCfa - Invalid OffSet - FAILURE.\n");
        return (MBSM_FAILURE);
    }

    if (MBSM_IS_SLOT_ID_VALID (i4SlotId))
    {
        pTempPtr = &(pMsg->FirstEntry);
        pTempPtr += i2OffSet;
        MEMCPY (&(pTempPtr->MbsmSlotInfo), (MBSM_SLOT_MAP_ENTRY (i4SlotId)),
                sizeof (pTempPtr->MbsmSlotInfo));
        MEMCPY (&(pTempPtr->MbsmPortInfo), (MBSM_PORT_INFO_ENTRY (i4SlotId)),
                sizeof (pTempPtr->MbsmPortInfo));
        pMsg->i4NumEntries = ++i2OffSet;
    }
    else
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_PROTO,
                  "Error in MbsmFormMsgToCfa - Invalid SlotId - FAILURE.\n");
        return (MBSM_FAILURE);
    }

    return (MBSM_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmFormPortInfoPortList
 *
 *    Description          : This function transforms the PortList in Slot    
 *                           map table to the system wide Port List bitmap   
 *                           based on the slot id passed.
 *
 *    Input(s)             : i4SlotId - Slot Id for which the port list is
 *                           formed.
 *                           MbsmPortList - Portlist for the ports within a
 *                           slot.
 *
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gaMbsmSlotMapTable, gaMbsmPortInfoTable
 *
 *    Global Variables Modified : gaMbsmPortInfoTable
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns              : MBSM_SUCCESS/MBSM_FAILURE
 *
 *****************************************************************************/
VOID
MbsmFormPortInfoPortList (INT4 i4SlotId, tMbsmPortList MbsmPortList,
                          INT1 i1IsInsert)
{
    UINT4               u4StartIfIndex;
    UINT4               u4ByteIndex;
    UINT4               u4TempPort;
    UINT4               u4ActualPort;
    UINT4               u4BitIndex;
    UINT1               u1Byte;

    u4StartIfIndex = MBSM_PORT_INDEX_STARTINDEX (i4SlotId);

    for (u4ByteIndex = 0; u4ByteIndex < sizeof (tMbsmPortList); u4ByteIndex++)
    {
        if (MbsmPortList[u4ByteIndex] != 0)
        {
            u1Byte = MbsmPortList[u4ByteIndex];

            for (u4BitIndex = 0;
                 ((u4BitIndex < MBSM_PORTS_PER_BYTE) && (u1Byte != 0));
                 u4BitIndex++)
            {
                if ((u1Byte & 0x80) != 0)
                {
                    u4TempPort = ((u4ByteIndex * MBSM_PORTS_PER_BYTE) +
                                  u4BitIndex);

                    u4ActualPort = u4TempPort + u4StartIfIndex;

                    if (i1IsInsert == MBSM_TRUE)
                    {
                        MBSM_SET_MEMBER_PORTLIST (MBSM_PORT_INDEX_PORTLIST
                                                  (i4SlotId), u4ActualPort);
                    }
                    else
                    {
                        /* In case of port removal (not slot removal)
                         * reset the port from the portlist 
                         */

                        MBSM_RESET_MEMBER_PORTLIST (MBSM_PORT_INDEX_PORTLIST
                                                    (i4SlotId), u4ActualPort);

                    }
                }
                u1Byte = (UINT1) (u1Byte << 1);
            }
        }
    }

    return;
}

/*****************************************************************************
 *
 *    Function Name        : MbsmUpdateTables
 *
 *    Description          : This function updates the Slot Map table and     
 *                           the Port Info table for a single slot from the  
 *                           message passed.              
 *
 *    Input(s)             : pMsg - Pointer to the slot information entry 
 *                           in the message.
 *                           i1IsPreConfigured - Flag indicating the pre-
 *                           configuration status.
 *                           i4ModType - Module type which has been inserted
 *                           or removaed.
 *                           i1IsInserted - Flag indicates whether this is an
 *                           insertion event (MBSM_TRUE) or removal event 
 *                           (MBSM_FALSE).
 *                        
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gaMbsmSlotMapTable, gaMbsmPortInfoTable
 *
 *    Global Variables Modified : gaMbsmSlotInfoTable, gaMbsmPortInfoTable
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns              : MBSM_SUCCESS/MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmUpdateTables (tMbsmHwSlotInfo * pMsg, INT4 i4ModType, INT1 i1IsInsert)
{
    INT4                i4RetVal = MBSM_SUCCESS;
    /* If i4ModType is port then dont update slot info */
    if (i4ModType != MBSM_LINE_CARD_PORT)
    {
        if ((i4RetVal = MbsmUpdateSlotMapEntry (pMsg, i4ModType, i1IsInsert))
            == MBSM_FAILURE)
        {
            return (i4RetVal);
        }
        MBSM_SLOT_MAP_ENTRY (pMsg->i4SlotId)->u1IsPortMsg = 0;
    }
    else                        /* set  u1IsPortMsg  */
    {
        MBSM_SLOT_MAP_ENTRY (pMsg->i4SlotId)->u1IsPortMsg = 1;
    }
    i4RetVal = MbsmUpdatePortInfoEntry (pMsg, i4ModType, i1IsInsert);

    return (i4RetVal);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmGetStartIfIndex
 *
 *    Description          : This function returns the StartIfIndex for the   
 *                           Slot Id given based on the information in the   
 *                           Slot Map table.              
 *
 *    Input(s)             : i4SlotId - Slot Id for which the StartIfIndex
 *                           is desired.    
 *
 *
 *    Output(s)            : pu4StartIfIndex - Starting If Index for the Slot
 *
 *    Global Variables Referred : gaMbsmSlotMapTable
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns              : MBSM_SUCCESS/MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmGetStartIfIndex (INT4 i4SlotId, UINT4 *pu4StartIfIndex)
{
    INT4                i4LcCount = 0;
    INT4                i4LoopIndex = 0;
    INT4                i4Index = 0;

    if (!MBSM_IS_SLOT_ID_VALID (i4SlotId))
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_PROTO,
                  "Error in MbsmGetStartIfIndex - Invalid SlotId - FAILURE.\n");
        return (MBSM_FAILURE);
    }

    if (!MBSM_IS_LINE_CARD (MBSM_SLOT_INDEX_MODTYPE (i4SlotId)))
    {
        *pu4StartIfIndex = 0;
        return (MBSM_SUCCESS);
    }

    /* Get the number of Line Cards before this Slot */
    for (i4LoopIndex = MBSM_SLOT_INDEX_START;
         ((MBSM_IS_SLOT_ID_VALID (i4LoopIndex)) &&
          (i4LoopIndex != i4SlotId)); i4LoopIndex++)
    {
        if (MBSM_IS_LINE_CARD (MBSM_SLOT_INDEX_MODTYPE (i4LoopIndex)))
        {
            i4LcCount++;
        }
    }

    *pu4StartIfIndex = (i4LcCount * MBSM_MAX_POSSIBLE_PORTS_PER_SLOT) + 1;

    if ((ISS_GET_STACKING_MODEL () == ISS_CTRL_PLANE_STACKING_MODEL) ||
        (ISS_GET_STACKING_MODEL () == ISS_DISS_STACKING_MODEL))
    {
        if (i4SlotId != MBSM_SLOT_INDEX_START)
        {
            /* add the connecting port count of previous slots to get the startifindex */
            for (i4Index = MBSM_SLOT_INDEX_START;
                 i4Index < (MBSM_MAX_SLOTS + MBSM_SLOT_INDEX_START); i4Index++)
            {
                if (i4Index < i4SlotId)
                {
                    *pu4StartIfIndex +=
                        MBSM_PORT_INDEX_CONNECTING_PORT_COUNT (i4Index);
                }
            }
        }
    }
    return (MBSM_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmUpdateSlotMapEntry
 *
 *    Description          : This function updates the Slot Map entry based   
 *                           on the hardware information.                    
 *                                                        
 *    Input(s)             : pSlotInfo- Pointer to the slot information entry 
 *                           in the message.
 *                           i4ModType - Module type which has been inserted
 *                           or removaed.
 *                           i1IsInserted - Flag indicates whether this is an
 *                           insertion event (MBSM_TRUE) or removal event 
 *                           (MBSM_FALSE).
 *
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gaMbsmSlotMapTable
 *
 *    Global Variables Modified : gaMbsmSlotInfoTable
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns              : MBSM_SUCCESS/MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmUpdateSlotMapEntry (tMbsmHwSlotInfo * pSlotInfo, INT4 i4ModType,
                        INT1 i1IsInsert)
{
    INT4                i4SlotId;
    INT4                i4StartIfIndex;

    i4SlotId = pSlotInfo->i4SlotId;

    if (!MBSM_IS_SLOT_ID_VALID (i4SlotId))
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_PROTO,
                  "Error in MbsmUpdatetables - Invalid SlotId - FAILURE.\n");
        return (MBSM_FAILURE);
    }
    if ((i1IsInsert == MBSM_TRUE) &&
        (i4ModType == MBSM_LINE_CARD) &&
        (pSlotInfo->i4CardType == MBSM_INVALID_CARDTYPE))
    {
        /* Either of CardType/CardName should be valid 
         * If CardType is invalid get the CardType from CardName.
         */
        if (MbsmGetCardIndexFromCardName
            (pSlotInfo->ai1CardName, &(pSlotInfo->i4CardType)) == MBSM_FAILURE)
        {
            MBSM_DBG (MBSM_TRC_CRITICAL, MBSM_PROTO,
                      "Error in MbsmGetCardIndexFromCardName - "
                      "Invalid card name - Failure.\n");

            return (MBSM_FAILURE);
        }
    }

    if (MBSM_PROTO_TAKE_SEM (MBSM_PROTO_SEM) != OSIX_SUCCESS)
    {
        MBSM_DBG (MBSM_TRC_CRITICAL, MBSM_PROTO,
                  "Error in MbsmUpdateSlotMapEntry - TakeSem Failed.\n");
        return (MBSM_FAILURE);
    }

    if (i1IsInsert == MBSM_TRUE)
    {
        /* Card Insertion */
        MBSM_SLOT_INDEX_SLOT_ID (i4SlotId) = i4SlotId;
        MBSM_SLOT_INDEX_MODTYPE (i4SlotId) = i4ModType;
        MBSM_SLOT_INDEX_PREVSTATUS (i4SlotId) =
            MBSM_SLOT_INDEX_STATUS (i4SlotId);
        MBSM_SLOT_INDEX_STATUS (i4SlotId) = MBSM_STATUS_ACTIVE;
        if (i4ModType == MBSM_LINE_CARD)
        {

            MbsmGetStartIfIndex (i4SlotId, (UINT4 *) &i4StartIfIndex);
            MBSM_SLOT_INDEX_STARTINDEX (i4SlotId) = i4StartIfIndex;
            /* If the number of ports supported in inserted slot is less
             * than the defined, update the same else do nothing */
            if (pSlotInfo->u4NumPorts != 0)
            {
                if (pSlotInfo->u4NumPorts < MBSM_SLOT_INDEX_NUMPORTS (i4SlotId))
                {
                    MBSM_SLOT_INDEX_NUMPORTS (i4SlotId) = pSlotInfo->u4NumPorts;
                }
                else
                {
                    pSlotInfo->u4NumPorts = MBSM_SLOT_INDEX_NUMPORTS (i4SlotId);
                }
            }
            MEMCPY (MBSM_SLOT_MAP_ENTRY (i4SlotId)->SlotBaseMacAddr,
                    pSlotInfo->SlotBaseMacAddr, sizeof (tMacAddr));

            if (MBSM_SLOT_INDEX_CARDTYPE (i4SlotId) != MBSM_INVALID_CARDTYPE)
            {
                if (MBSM_SLOT_INDEX_CARDTYPE (i4SlotId)
                    != pSlotInfo->i4CardType)
                {
                    if (MBSM_SLOT_INDEX_ISPRECONF (i4SlotId) == MBSM_TRUE)
                    {
#ifdef SNMP_2_WANTED
                        /* Generate Trap */
                        MbsmSnmpSendConfigErrTrap (i4SlotId, i4ModType,
                                                   MBSM_CARD_TYPE_INFO_CARDNAME
                                                   (MBSM_SLOT_INDEX_CARDTYPE
                                                    (i4SlotId)),
                                                   MBSM_SLOT_INDEX_STATUS
                                                   (i4SlotId),
                                                   (INT1 *)
                                                   MBSM_CONFIG_ERR_TRAP_OID,
                                                   MBSM_TRAPS_OID_LEN);
#endif

                        /* The Slot is unusable unless the pre-configuration
                           is deleted */
                        MBSM_SLOT_INDEX_STATUS (i4SlotId) =
                            MBSM_STATUS_INACTIVE;

                        if (MBSM_PROTO_GIVE_SEM (MBSM_PROTO_SEM)
                            != OSIX_SUCCESS)
                        {
                            MBSM_DBG (MBSM_TRC_CRITICAL, MBSM_PROTO,
                                      "Error in MbsmUpdateSlotMapEntry - "
                                      "GiveSem Failed.\n");
                        }

                        return (MBSM_FAILURE);
                    }
                    else
                    {
                        /* Different Line card type inserted, hence
                         * interfaces have to be deleted for previous
                         * Line card */
                        if (MbsmNotifyCfaPreConfigDel (i4SlotId)
                            == MBSM_FAILURE)
                        {
                            MBSM_DBG (MBSM_TRC_ERR, MBSM_PROTO,
                                      "Error in MbsmUpdatetables - Unable to "
                                      "Write to File - FAILURE.\n");
                            if (MBSM_PROTO_GIVE_SEM (MBSM_PROTO_SEM)
                                != OSIX_SUCCESS)
                            {
                                MBSM_DBG (MBSM_TRC_CRITICAL, MBSM_PROTO,
                                          "Error in MbsmUpdateSlotMapEntry - "
                                          "GiveSem Failed.\n");
                            }

                            return (MBSM_FAILURE);
                        }
                    }
                }
            }
            MBSM_SLOT_INDEX_CARDTYPE (i4SlotId) = pSlotInfo->i4CardType;
        }
    }
    else
    {
        /* Card Removal */
        MBSM_SLOT_INDEX_PREVSTATUS (i4SlotId) =
            MBSM_SLOT_INDEX_STATUS (i4SlotId);
        MBSM_SLOT_INDEX_STATUS (i4SlotId) = MBSM_STATUS_NP;
    }

    if (MBSM_PROTO_GIVE_SEM (MBSM_PROTO_SEM) != OSIX_SUCCESS)
    {
        MBSM_DBG (MBSM_TRC_CRITICAL, MBSM_PROTO,
                  "Error in MbsmUpdateSlotMapEntry - GiveSem Failed.\n");
        return (MBSM_FAILURE);
    }

    /* Write the updated information to the SlotModule,txt file */
    if (MbsmWriteSlotModuleInfoToFile () != MBSM_SUCCESS)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_PROTO,
                  "Error in MbsmUpdatetables - Unable to Write to File - FAILURE.\n");
        return (MBSM_FAILURE);

    }
    return (MBSM_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmUpdatePortInfoEntry
 *
 *    Description          : This function updates the Portt Info entry based   
 *                           on the hardware information.                    
 *                                                        
 *    Input(s)             : pSlotInfo- Pointer to the slot information entry 
 *                           in the message.
 *                           i4ModType - Module type which has been inserted
 *                           or removaed.
 *                           i1IsInserted - Flag indicates whether this is an
 *                           insertion event (MBSM_TRUE) or removal event 
 *                           (MBSM_FALSE).
 *
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gaMbsmPortInfoTable
 *
 *    Global Variables Modified : gaMbsmPortInfoTable
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns              : MBSM_SUCCESS/MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmUpdatePortInfoEntry (tMbsmHwSlotInfo * pSlotInfo, INT4 i4ModType,
                         INT1 i1IsInsert)
{
    INT4                i4SlotId;

    i4SlotId = pSlotInfo->i4SlotId;

    if (!MBSM_IS_SLOT_ID_VALID (i4SlotId))
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_PROTO,
                  "Error in MbsmUpdatetables - Invalid SlotId - FAILURE.\n");
        return (MBSM_FAILURE);
    }
    MBSM_PORT_INDEX_STARTINDEX (i4SlotId) =
        MBSM_SLOT_INDEX_STARTINDEX (i4SlotId);
    /* If the number of ports supported in inserted slot is less
     * than the defined, update the same else do nothing */
    if (pSlotInfo->u4NumPorts != 0)
    {
        if (pSlotInfo->u4NumPorts < MBSM_PORT_INDEX_PORTCOUNT (i4SlotId))
        {
            MBSM_PORT_INDEX_PORTCOUNT (i4SlotId) = pSlotInfo->u4NumPorts;
        }
        else
        {
            pSlotInfo->u4NumPorts = MBSM_PORT_INDEX_PORTCOUNT (i4SlotId);
        }
    }

    if ((i4ModType == MBSM_LINE_CARD) || (i4ModType == MBSM_LINE_CARD_PORT))
    {
        MbsmFormPortInfoPortList (i4SlotId, pSlotInfo->MbsmPortList,
                                  i1IsInsert);
    }

    return (MBSM_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmUpdateSlotMapEntryStatus
 *
 *    Description          : This function updates the Slot Map entry status  
 *                           to Card Inactive in the table since Card is
 *                           removed now.
 *                                                        
 *    Input(s)             : pSlotInfo- Pointer to the slot information entry 
 *                           in the message.
 *
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gaMbsmSlotMapTable
 *
 *    Global Variables Modified : gaMbsmSlotInfoTable
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns              : MBSM_SUCCESS/MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmUpdateSlotMapEntryStatus (tMbsmHwSlotInfo * pSlotInfo, INT1 i1Status)
{
    INT4                i4SlotId;

    i4SlotId = pSlotInfo->i4SlotId;

    if (!MBSM_IS_SLOT_ID_VALID (i4SlotId))
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_PROTO,
                  "Error in MbsmUpdatetables - Invalid SlotId - FAILURE.\n");
        return (MBSM_FAILURE);
    }

    MBSM_SLOT_INDEX_PREVSTATUS (i4SlotId) = MBSM_SLOT_INDEX_STATUS (i4SlotId);
    MBSM_SLOT_INDEX_STATUS (i4SlotId) = i1Status;

    return (MBSM_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmInitHwTopoDisc 
 *
 *    Description          : This function will Init the HW topology           
 *                           by starting the h/w Stack Task .
 *
 *    Input(s)             : i1NodeState - Current state of this Node.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None 
 *
 *    Global Variables Modified : None
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns              : MBSM_SUCCESS/MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmInitHwTopoDisc (INT1 i1NodeState)
{
    INT4                i4SlotId = 0;

    i4SlotId = (INT4) IssGetSwitchid ();
    MBSM_SELF_SLOT_ID () = (UINT1) i4SlotId;

#ifdef NPAPI_WANTED
    if (ISS_GET_STACKING_MODEL () == ISS_CTRL_PLANE_STACKING_MODEL)
    {
#ifdef RM_WANTED
        if ((RmGetNodePrevState () == RM_STANDBY) &&
            (i1NodeState == MBSM_STATUS_ACTIVE))
        {
            gLocalAttachRcvd = MBSM_TRUE;
        }
#endif
    }
    if (MbsmMbsmNpInitHwTopoDisc (i4SlotId, i1NodeState) == FNP_FAILURE)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_PROTO,
                  "Error in MbsmNpInitHwTopoDisc - FAILURE.\n");

        return (MBSM_FAILURE);
    }
#else

    UNUSED_PARAM (i1NodeState);
#endif
    return (MBSM_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmGenerateRemoteSlotDetach 
 *
 *    Description          : This function is used to generate remote detach
 *                           to CFA & Protocols.
 *
 *    Input(s)             : None.
 *
 *    Output(s)            : None.
 *
 *    Returns              : MBSM_SUCCESS/MBSM_FAILURE
 *
 *****************************************************************************/

INT4
MbsmGenerateRemoteSlotDetach (VOID)
{
    INT4                i4LoopIdx1 = 0;
    INT4                i4RemoteSlotId = MBSM_INVALID_SLOT_ID;
    INT4                i4ProtoCookie = MBSM_PROTO_COOKIE_INVALID;
    tMbsmHwSlotInfo     SlotInfo;

    MEMSET (&SlotInfo, 0, sizeof (tMbsmHwSlotInfo));

    /* Get Remote Slot Id */
    for (i4LoopIdx1 = MBSM_SLOT_INDEX_START;
         i4LoopIdx1 < (MBSM_MAX_SLOTS + MBSM_SLOT_INDEX_START); i4LoopIdx1++)
    {
#ifdef STACKING_WANTED            /* Pizzabox Stacking case */
        if (!(MBSM_IS_LINE_CARD (MBSM_SLOT_INDEX_MODTYPE (i4LoopIdx1))))
        {
            continue;
        }
#else /* CHASSIS with redundancy support */
        if (!(MBSM_IS_CONTROL_CARD (MBSM_SLOT_INDEX_MODTYPE (i4LoopIdx1))))
        {
            continue;
        }
#endif

        i4RemoteSlotId = MBSM_SLOT_INDEX_SLOT_ID (i4LoopIdx1);
        if (i4RemoteSlotId != MBSM_SELF_SLOT_ID ())
        {
            break;
        }
    }

    if (i4RemoteSlotId == MBSM_INVALID_SLOT_ID)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_PROTO,
                  "Remote slot not found.Can not be able to send "
                  "detach for remote slot to higher layers");
        return MBSM_FAILURE;
    }

    SlotInfo.i4SlotId = i4RemoteSlotId;

    if (MbsmUpdateSlotMapEntryStatus (&SlotInfo, MBSM_STATUS_NP)
        == MBSM_FAILURE)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_PROTO,
                  "Cannot be able to update slot map table for "
                  "remote detach\n");
        return MBSM_FAILURE;
    }

    /* FALSE indicates this is a Removal event */
    if (MbsmUpdateTables (&SlotInfo, MBSM_LINE_CARD, MBSM_FALSE)
        == MBSM_FAILURE)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_PROTO,
                  "Error in update tables for remote detach\n");
        return MBSM_FAILURE;
    }

    /* Going to give remote slot detach to CFA */
    if (MbsmNotifyCfaModuleUpdate (i4RemoteSlotId) == MBSM_FAILURE)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_PROTO,
                  "Error when remote detach is given to CFA\n");
        return MBSM_FAILURE;
    }

    /* Going to give remote slot detach to Protocols */

    /* Initialize the Cookie with the First Protocol in tMbsmProtoCookie */
    i4ProtoCookie = MBSM_COOKIE_START_INDEX;

    /* Notify protocols using handshake mechanism for
     * hardware configurations */
    if (MbsmNotifyLcUpdate (i4RemoteSlotId, i4ProtoCookie) == MBSM_FAILURE)
    {
        i4ProtoCookie = MBSM_PROTO_COOKIE_NULL;
        MBSM_DBG (MBSM_TRC_ERR, MBSM_PROTO,
                  "Error when remote detach is given to Protocols\n");
        return MBSM_FAILURE;
    }

    return MBSM_SUCCESS;
}
