/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mbsprot.c,v 1.55 2017/01/17 14:10:29 siva Exp $
 *
 * Description:This file contains the routines responsible  
 *             for interacting with other protocol modules.
 *
 *******************************************************************/
#include "mbsminc.h"
#ifdef MSR_WANTED
#include "msr.h"
#endif

extern UINT1        gu1SelfAttachRcvd;
extern CHR1  *mbsmProtoCookieName[];
/*****************************************************************************
 *
 *    Function Name        : MbsmSendAckFromProto
 *
 *    Description          : This function is the callback function provided
 *                           to protocols for sending an Ack message for the
 *                           module insertion/deletion mesage sent from MBSM
 *                           to protocols.                            
 *
 *    Input(s)             : pAckMsg - Pointer to the Ack message.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.            
 *
 *    Global Variables Modified : None.            
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns              : MBSM_SUCCESS/MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmSendAckFromProto (tMbsmProtoAckMsg * pAckMsg)
{
    tMbsmMsg           *pMbsmAckMsg;

    if (!pAckMsg)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_PROTO,
                  "Error in MbsmSendAckFromProto - NULL Message - FAILURE.\n");
        return (MBSM_FAILURE);
    }

    if (!(pMbsmAckMsg = MemAllocMemBlk (MBSM_MSG_QUEUE_MEMPOOL_ID)))
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_PROTO,
                  "Cannot allocate memory for message in MbsmSendAckFromProto - FAILURE.\n");
        return (MBSM_FAILURE);
    }
    pMbsmAckMsg->i4MsgType = MBSM_MSG_PROTO_ACK;
    MEMCPY (pMbsmAckMsg->uMsg.au1Data, pAckMsg, sizeof (tMbsmProtoAckMsg));

    if (OsixSendToQ (MBSM_TASK_NODE_ID,
                     (const UINT1 *) MBSM_MSG_QUEUE_NAME,
                     (tOsixMsg *) (VOID *) pMbsmAckMsg,
                     OSIX_MSG_NORMAL) != OSIX_SUCCESS)
    {
        /* Protocols should release the buffer on failure */
        MemReleaseMemBlock (MBSM_MSG_QUEUE_MEMPOOL_ID, (UINT1 *) pMbsmAckMsg);
        return MBSM_FAILURE;
    }

    MBSM_SEND_EVENT (MBSM_TASK_ID, MBSM_PROTO_ACK_MSG_EVENT);

    return (MBSM_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmSendSelfAttachAckFromCfa
 *
 *    Description          : This function sends an Ack message to MBSM after
 *                           Self Attach event is processed at CFA
 *                           
 *    Input(s)             : None
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.            
 *
 *    Global Variables Modified : None.            
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns              : MBSM_SUCCESS/MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmSendSelfAttachAckFromCfa (VOID)
{
    MBSM_SEND_EVENT (MBSM_TASK_ID, MBSM_SELF_ATTACH_ACK_EVENT);

    return (MBSM_SUCCESS);

}

/*****************************************************************************
 *
 *    Function Name        : MbsmSendSyncAckFromCfa
 *
 *    Description          : This function sends an Ack message to MBSM after
 *                           the port creation was success at all the L2 modules.
 *                           
 *    Input(s)             : pAckMsg - Pointer to the Ack message.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.            
 *
 *    Global Variables Modified : None.            
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns              : MBSM_SUCCESS/MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmSendSyncAckFromCfa (tMbsmSyncAckMsg * pAckMsg)
{
    tMbsmMsg           *pMbsmSyncAckMsg;

    if (!pAckMsg)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_PROTO,
                  "Error in MbsmSendSyncAckFromProto - NULL Message - FAILURE.\n");
        return (MBSM_FAILURE);
    }

    if (!(pMbsmSyncAckMsg = MemAllocMemBlk (MBSM_MSG_QUEUE_MEMPOOL_ID)))
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_PROTO,
                  "Cannot allocate memory for message in MbsmSendSyncAckFromProto -"
                  "FAILURE.\n");
        return (MBSM_FAILURE);
    }
    pMbsmSyncAckMsg->i4MsgType = MBSM_MSG_SYNC_ACK;
    MEMCPY (pMbsmSyncAckMsg->uMsg.au1Data, pAckMsg, sizeof (tMbsmSyncAckMsg));

    if (OsixSendToQ (MBSM_TASK_NODE_ID,
                     (const UINT1 *) MBSM_MSG_QUEUE_NAME,
                     (tOsixMsg *) (VOID *) pMbsmSyncAckMsg,
                     OSIX_MSG_NORMAL) != OSIX_SUCCESS)
    {
        /* Protocols should release the buffer on failure */
        MemReleaseMemBlock (MBSM_MSG_QUEUE_MEMPOOL_ID,
                            (UINT1 *) pMbsmSyncAckMsg);
        return MBSM_FAILURE;
    }

    MBSM_SEND_EVENT (MBSM_TASK_ID, MBSM_SYNC_ACK_MSG_EVENT);

    return (MBSM_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmGetMsgType         
 *
 *    Description          : This function extracts the message type from the
 *                           packet buffer passed.                           
 *
 *    Input(s)             : pMsg - Pointer to the message.
 *
 *    Output(s)            : pi4MsgType - Message type of the message
 *
 *    Global Variables Referred : None.            
 *
 *    Global Variables Modified : None.            
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns              : MBSM_SUCCESS/MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmGetMsgType (UINT1 *pMsg, INT4 *pi4MsgType)
{
    tMbsmMsg           *pMsgHdr;

    if (!pMsg)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_PROTO,
                  "Error in MbsmGetMsgType - NULL Message - FAILURE.\n");
        return (MBSM_FAILURE);
    }

    pMsgHdr = (tMbsmMsg *) (VOID *) pMsg;
    *pi4MsgType = pMsgHdr->i4MsgType;

    return (MBSM_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmNotifyMbsmFromDriver
 *
 *    Description          : This function is called from hardware context and
 *                           will post a message containing hardware topology
 *                           information to MBSM.
 *
 *    Input(s)             : pData - Pointer to the message.
 *                           i4Event - Type of Event to be posted to MBSM.
 *
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.            
 *
 *    Global Variables Modified : None.            
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns              : MBSM_SUCCESS/MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmNotifyMbsmFromDriver (tMbsmHwMsg * pData, INT4 i4MsgType)
{
    tMbsmMsg           *pMbsmHwMsg;
    UINT4               u4DataLen;

    if (!pData)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_PROTO,
                  "Error in MbsmNotifyMbsmFromDriver - NULL Message - FAILURE.\n");
        return (MBSM_FAILURE);
    }

    if (pData->i4NumEntries)
    {
        u4DataLen = sizeof (tMbsmHwMsg) + ((pData->i4NumEntries - 1) *
                                           sizeof (tMbsmHwMsg));
    }
    else
    {
        u4DataLen = sizeof (tMbsmHwMsg);
    }

    if (!(pMbsmHwMsg = MemAllocMemBlk (MBSM_MSG_QUEUE_MEMPOOL_ID)))
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_PROTO,
                  "Cannot allocate memory for message in MbsmNotifyMbsmFromDriver - FAILURE.\n");
        return (MBSM_FAILURE);
    }

    pMbsmHwMsg->i4MsgType = i4MsgType;
    MEMCPY (pMbsmHwMsg->uMsg.au1Data, pData, u4DataLen);

    if (OsixSendToQ (MBSM_TASK_NODE_ID,
                     (const UINT1 *) MBSM_MSG_QUEUE_NAME,
                     (tOsixMsg *) (VOID *) pMbsmHwMsg,
                     OSIX_MSG_NORMAL) != OSIX_SUCCESS)
    {

        /* Protocols should release the buffer on failure */
        MemReleaseMemBlock (MBSM_MSG_QUEUE_MEMPOOL_ID, (UINT1 *) pMbsmHwMsg);
        return MBSM_FAILURE;
    }

    MBSM_SEND_EVENT (MBSM_TASK_ID, MBSM_DRIVER_EVENT);

    return (MBSM_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmWaitForSelfAttachCompletion
 *
 *    Description          : This function waits until the CFA module updates about      
 *                           Line card insertion/removal events for interface
 *                           creation and status change.
 *
 *    Input(s)             : None
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.            
 *
 *    Global Variables Modified : None.            
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns              : MBSM_SUCCESS/MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmWaitForSelfAttachCompletion (INT4 i4SlotId)
{

    UINT4               u4Event;
    INT4                i4RetVal = MBSM_SUCCESS;

#if defined (QOSX_WANTED) || defined (ECFM_WANTED)
    tMbsmProtoMsg       Msg;
    MEMSET (&Msg, 0, sizeof (tMbsmProtoMsg));
#endif
    while (1)
    {
        MBSM_RECV_EVENT (MBSM_TASK_ID, MBSM_SELF_ATTACH_ACK_EVENT,
                         MBSM_EVENT_WAIT_FLAGS, &u4Event);

        if (u4Event & MBSM_SELF_ATTACH_ACK_EVENT)
        {
            break;
        }
    }
    /* QOS Initialisation is done during Self attach after completion of CFA module updates */
    /* Needs to be implemented for IP and IPv6 */
#if defined (QOSX_WANTED) || defined (ECFM_WANTED)
    u4Event = MBSM_MSG_SELF_CARD_INSERT;
    Msg.MbsmSlotInfo = *(MBSM_SLOT_MAP_ENTRY (i4SlotId));
    Msg.MbsmPortInfo = *(MBSM_PORT_INFO_ENTRY (i4SlotId));

#ifdef QOSX_WANTED
    Msg.i4ProtoCookie = MBSM_PROTO_COOKIE_QOSX;
    i4RetVal = QosxProcessMbsmMessage (&Msg, (INT4) u4Event);
#endif
#ifdef ECFM_WANTED
    Msg.i4ProtoCookie = MBSM_PROTO_COOKIE_ECFM;
    i4RetVal = EcfmMbsmPostMessage (&Msg, (INT4) u4Event);
#endif

    i4RetVal = QosxProcessMbsmMessage (&Msg, (INT4) u4Event);
#else
    UNUSED_PARAM (i4SlotId);
#endif

#ifdef RM_WANTED
    if (gu1SelfAttachRcvd == MBSM_FALSE)
    {
        gu1SelfAttachRcvd = MBSM_TRUE;
    }
    RmInformOtherModulesBootUp (NULL);
#endif
    return i4RetVal;
}

/*****************************************************************************
 *
 *    Function Name        : MbsmNotifyCfaModuleUpdate
 *
 *    Description          : This function notifies the CFA module about      
 *                           Line card insertion/removal events for interface
 *                           creation and status change.
 *
 *    Input(s)             : i4SlotId - Slot Id information to be notified.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.            
 *
 *    Global Variables Modified : None.            
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns              : MBSM_SUCCESS/MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmNotifyCfaModuleUpdate (INT4 i4SlotId)
{
    INT2                i2OffSet = 0;
    tMbsmCfaHwInfoMsg   MbsmCfaHwInfoMsg;

    if (MbsmFormMsgToCfa (i4SlotId, &MbsmCfaHwInfoMsg, i2OffSet)
        == MBSM_FAILURE)
    {
        return (MBSM_FAILURE);
    }

    /* Post the message to CFA - Copies message into a duplicate CRU buf */
    if (CfaMbsmMsgToCfa ((UINT1 *) &MbsmCfaHwInfoMsg, MBSM_MSG_INTF_CTRL)
        == CFA_FAILURE)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_TOPO,
                  "Error in MbsmNotifyCfaModuleUpdate - Message to CFA Failed - FAILURE.\n");
        return (MBSM_FAILURE);
    }

    return (MBSM_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmHandleProtoAckMsg
 *
 *    Description          : This function is invoked when a Ack message is   
 *                           received from the protocols. It updates the     
 *                           protocol Id in the message and returns it.
 *
 *    Input(s)             : pMsg - Pointer to the Ack message received.   
 *
 *    Output(s)            : pi4ProtoCookie - Protocol Id returned after
 *                           processing from the Ack message.
 *
 *    Global Variables Referred : None.            
 *
 *    Global Variables Modified : None.            
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns              : MBSM_SUCCESS/MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmHandleProtoAckMsg (tMbsmProtoAckMsg * pMsg, INT4 *pi4ProtoCookie)
{

    if (!pMsg)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_PROTO,
                  "Error in MbsmHandleProtoAckMsg - NULL message - FAILURE.\n");
        return (MBSM_FAILURE);
    }

    if ((pMsg->i4ProtoCookie <= MBSM_PROTO_COOKIE_INVALID) ||
        (pMsg->i4ProtoCookie >= MBSM_PROTO_COOKIE_NULL))
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_PROTO,
                  "Error in MbsmHandleProtoAckMsg - Invalid Protocol Id - FAILURE.\n");
        return (MBSM_FAILURE);
    }

    if (pMsg->i4RetStatus == MBSM_FAILURE)
    {
        MBSM_DBG_ARG1 (MBSM_TRC_CRITICAL, MBSM_PROTO,
                       "Error in MbsmHandleProtoAckMsg - Failure status returned from Protocol %s \n",
                        mbsmProtoCookieName[pMsg->i4ProtoCookie]);
    }
    else
    {
        MBSM_DBG_ARG1 (MBSM_RED_TRC, MBSM_RED,
                       "MBSM programming completed for %s.\n", 
                        mbsmProtoCookieName[pMsg->i4ProtoCookie]);
    }

    *pi4ProtoCookie = ++pMsg->i4ProtoCookie;

    if ((*pi4ProtoCookie != MBSM_PROTO_COOKIE_INVALID) &&
        (*pi4ProtoCookie != MBSM_PROTO_COOKIE_NULL))
    {
        if (MbsmNotifyLcUpdate (pMsg->i4SlotId, pMsg->i4ProtoCookie) ==
            MBSM_FAILURE)
        {
            *pi4ProtoCookie = MBSM_PROTO_COOKIE_NULL;
        }
    }

    return (MBSM_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmNotifyLcUpdate
 *
 *    Description          : This function notifies the protocol modules of   
 *                           a card insertion event in Hardware, so that     
 *                           blocked hardware configurations can proceed.
 *
 *    Input(s)             : i4SlotId - Slot Id of the inserted module.
 *                           i4ProtoCookie - Identifier for the Protocol to
 *                           be notified.   
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.            
 *
 *    Global Variables Modified : None.            
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns              : MBSM_SUCCESS/MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmNotifyLcUpdate (INT4 i4SlotId, INT4 i4ProtoCookie)
{
    tMbsmProtoMsg       Msg;
    tHwIdInfo           HwIdInfo;
    UINT4               u4IfIndex = 0;
    INT4                i4RetVal = MBSM_SUCCESS;
    INT4                i4Event;
#ifdef VCM_WANTED
    tMbsmProtoAckMsg    MbsmProtoAckMsg;
#endif

    if (!MBSM_IS_SLOT_ID_VALID (i4SlotId))
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_PROTO,
                  "Error in MbsmNotifyLcUpdate - Invalid SlotId - FAILURE.\n");
        return (MBSM_FAILURE);
    }

    if ((i4ProtoCookie <= MBSM_PROTO_COOKIE_INVALID) ||
        (i4ProtoCookie >= MBSM_PROTO_COOKIE_NULL))
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_PROTO,
                  "Error in MbsmNotifyLcUpdate - Invalid Protocol Id - FAILURE.\n");
        return (MBSM_FAILURE);
    }

    if (MBSM_SLOT_INDEX_STATUS (i4SlotId) == MBSM_STATUS_ACTIVE)
    {
        i4Event = MBSM_MSG_CARD_INSERT;
    }
    else if (MBSM_SLOT_INDEX_STATUS (i4SlotId) == MBSM_STATUS_NP)
    {
        i4Event = MBSM_MSG_CARD_REMOVE;
    }
    else
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_PROTO,
                  "Error in MbsmNotifyLcUpdate - Invalid Slot Status - FAILURE.\n");
        return (MBSM_FAILURE);
    }

    MEMSET (&Msg, 0, sizeof (tMbsmProtoMsg));
    MEMSET (&HwIdInfo, 0, sizeof (tHwIdInfo));
    Msg.MbsmSlotInfo = *(MBSM_SLOT_MAP_ENTRY (i4SlotId));
    Msg.MbsmPortInfo = *(MBSM_PORT_INFO_ENTRY (i4SlotId));
    Msg.i4ProtoCookie = i4ProtoCookie;

    switch (i4ProtoCookie)
    {
#ifdef BRIDGE_WANTED
        case MBSM_PROTO_COOKIE_BRIDGE:
            i4RetVal = BridgeMbsmProcessUpdateMessage (&Msg, i4Event);
            break;
            /* Intentional fallthrough if Bridge is not defined */
#endif
        case MBSM_PROTO_COOKIE_CFA:
            Msg.i4ProtoCookie = MBSM_PROTO_COOKIE_CFA;
            /* For ISS system features related configurations */
            i4RetVal = CfaMbsmProtoMsgToCfa (&Msg, i4Event);
            break;
#ifdef VCM_WANTED
            /* This is an Function call, since we are not having seperate task 
             * for VCM */
        case MBSM_PROTO_COOKIE_VCM:
            i4RetVal = VcmMbsmProcessUpdate (&(Msg.MbsmPortInfo),
                                             &(Msg.MbsmSlotInfo), i4Event);
            MbsmProtoAckMsg.i4ProtoCookie = i4ProtoCookie;
            MbsmProtoAckMsg.i4SlotId =
                MBSM_SLOT_INFO_SLOT_ID (&(Msg.MbsmSlotInfo));
            MbsmProtoAckMsg.i4RetStatus = i4RetVal;
            MbsmSendAckFromProto (&MbsmProtoAckMsg);
            break;
#endif
        case MBSM_PROTO_COOKIE_SISP:

            i4RetVal = VcmSispMbsmProcessUpdate (&(Msg.MbsmPortInfo),
                                                 &(Msg.MbsmSlotInfo), i4Event);

            MbsmProtoAckMsg.i4ProtoCookie = i4ProtoCookie;
            MbsmProtoAckMsg.i4SlotId =
                MBSM_SLOT_INFO_SLOT_ID (&(Msg.MbsmSlotInfo));
            MbsmProtoAckMsg.i4RetStatus = i4RetVal;

            MbsmSendAckFromProto (&MbsmProtoAckMsg);

            break;

#ifdef EOAM_WANTED
        case MBSM_PROTO_COOKIE_EOAM:
            i4RetVal = EoamMbsmPostMessage (&Msg, i4Event);
            break;
#endif /* EOAM_WANTED */
#ifdef PNAC_WANTED
        case MBSM_PROTO_COOKIE_PNAC:
            i4RetVal = PnacMbsmProcessUpdateMessage (&Msg, i4Event);
            break;
#endif /* PNAC_WANTED */

#ifdef LA_WANTED
        case MBSM_PROTO_COOKIE_LA:
            i4RetVal = LaMbsmProcessUpdateMessage (&Msg, i4Event);
            break;
#endif /* LA_WANTED */

#if defined (RSTP_WANTED) || defined (MSTP_WANTED)
        case MBSM_PROTO_COOKIE_AST:
            i4RetVal = AstMbsmProcessUpdateMessage (&Msg, i4Event);
            break;
#endif /* RSTP_WANTED || MSTP_WANTED */
#ifdef VLAN_WANTED
        case MBSM_PROTO_COOKIE_VLAN:
            i4RetVal = VlanMbsmPostMessage (&Msg, i4Event);
            break;
#endif /* VLAN_WANTED */
#ifdef MRP_WANTED
        case MBSM_PROTO_COOKIE_MRP:
            i4RetVal = MrpMbsmProcessUpdate (&(Msg.MbsmPortInfo),
                                             &(Msg.MbsmSlotInfo), i4Event);
            MbsmProtoAckMsg.i4ProtoCookie = i4ProtoCookie;
            MbsmProtoAckMsg.i4SlotId =
                MBSM_SLOT_INFO_SLOT_ID (&(Msg.MbsmSlotInfo));
            MbsmProtoAckMsg.i4RetStatus = i4RetVal;
            MbsmSendAckFromProto (&MbsmProtoAckMsg);
            break;
#endif /* MRP_WANTED */
#ifdef PBBTE_WANTED
        case MBSM_PROTO_COOKIE_PBBTE:
            i4RetVal = PbbTeMbsmPostMessage (&Msg, i4Event);
            break;
#endif /*PBBTE_WANTED */
#ifdef PBB_WANTED
        case MBSM_PROTO_COOKIE_PBB:
            i4RetVal = PbbMbsmPostMessage (&Msg, i4Event);
            break;
#endif /* PBB_WANTED */
#if defined (IGS_WANTED) || defined (MLDS_WANTED)
        case MBSM_PROTO_COOKIE_SNOOP:
            i4RetVal = SnoopMbsmProcessUpdateMessage (&Msg, i4Event);
            break;
#endif /* IGS_WANTED */

#ifdef IP_WANTED
        case MBSM_PROTO_COOKIE_IP_FWD:
            i4RetVal = IpFwdMbsmUpdateCardStatus (&Msg, i4Event);
            break;
#endif
#ifdef ARP_WANTED
        case MBSM_PROTO_COOKIE_IP_ARP:
            i4RetVal = IpArpMbsmUpdateCardStatus (&Msg, i4Event);
            break;
#endif
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
        case MBSM_PROTO_COOKIE_IP_RTM:
            i4RetVal = IpRtmMbsmUpdateCardStatus (&Msg, i4Event);
            break;
#endif

#ifdef OSPF_WANTED
        case MBSM_PROTO_COOKIE_OSPF:
            i4RetVal = OspfMbsmUpdateCardStatus (&Msg, i4Event);
            break;
#endif

#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
        case MBSM_PROTO_COOKIE_IP6_FWD:
            i4RetVal = Ip6FwdMbsmUpdateCardStatus (&Msg, i4Event);
            break;
#endif
        case MBSM_PROTO_COOKIE_IP6_RTM6:
            i4RetVal = Rtm6MbsmUpdateCardStatus (&Msg, i4Event);
            break;
#endif

#ifdef RIP6_WANTED
        case MBSM_PROTO_COOKIE_RIP6:
            i4RetVal = Rip6FwdMbsmUpdateCardStatus (&Msg, i4Event);
            break;
#endif

#ifdef OSPF3_WANTED
        case MBSM_PROTO_COOKIE_OSPF3:
            i4RetVal = V3OspfMbsmUpdateCardStatus (&Msg, i4Event);
            break;
#endif

#ifdef IGMP_WANTED
        case MBSM_PROTO_COOKIE_IGMP:
            i4RetVal = IgmpMbsmPostMessage (&Msg, i4Event);
            break;
#endif

#ifdef PIM_WANTED
        case MBSM_PROTO_COOKIE_PIM:
            i4RetVal = PimMbsmPostMessage (&Msg, i4Event);
            break;
#endif
#ifdef DVMRP_WANTED
        case MBSM_PROTO_COOKIE_DVMRP:
            i4RetVal = DvmrpMbsmPostMessage (&Msg, i4Event);
            break;
#endif
#ifdef MPLS_WANTED
        case MBSM_PROTO_COOKIE_MPLS:
            i4RetVal = MplsMbsmUpdateCardStatus (&Msg, (UINT1) i4Event);
            break;
#endif

#ifdef ISIS_WANTED
        case MBSM_PROTO_COOKIE_ISIS:
             i4RetVal = IsisMbsmUpdateCardStatus (&Msg, (UINT1) i4Event);
             break;
#endif

        case MBSM_PROTO_COOKIE_LINKSTATUS:
            if ((MBSM_SLOT_INDEX_STATUS (i4SlotId) == MBSM_STATUS_ACTIVE) &&
                (MBSM_IS_LINE_CARD (MBSM_SLOT_INDEX_MODTYPE (i4SlotId))))
            {
                i4Event = MBSM_MSG_LINK_STATUS_UPDATE;
                i4RetVal = CfaMbsmProtoMsgToCfa (&Msg, i4Event);
            }
            else
            {
                MEMSET (MBSM_PORT_INDEX_PORTLISTSTATUS (i4SlotId),
                        0, sizeof (tPortList));
            }

#ifdef RM_WANTED
            if ((MBSM_SLOT_INDEX_STATUS (i4SlotId) == MBSM_STATUS_NP))
            {
                if (gGiveGoActiveToOthers == MBSM_TRUE)
                {
                    /* Remote slot detatch is given to all protocols
                     * through MbsmGenerateRemoteSlotDetach. Now we have
                     * to post GO_ACTIVE to all protocols
                     */
                    RmSendEventToAppln (RM_MBSM_APP_ID);
                    gGiveGoActiveToOthers = MBSM_FALSE;
                }
            }
#endif
            break;

#ifdef ECFM_WANTED
        case MBSM_PROTO_COOKIE_ECFM:
            i4RetVal = EcfmMbsmPostMessage (&Msg, i4Event);
            break;
#endif /* ECFM_WANTED */
#ifdef DCBX_WANTED
/* This API will be called to indicate the card insert/remove
 * event to DCBX so that it willl program the hardware based
 * on the slot information present in the Msg */
        case MBSM_PROTO_COOKIE_DCBX:
            i4RetVal = DcbxApiMbsmNotification (&Msg, i4Event);
            break;
#endif /* DCBX_WANTED */
#ifdef FSB_WANTED
        case MBSM_PROTO_COOKIE_FSB:
            i4RetVal = FsbApiMbsmNotification (&Msg, i4Event);
            break;
#endif /* FSB_WANTED */
#ifdef QOSX_WANTED
        case MBSM_PROTO_COOKIE_QOSX:
            i4RetVal = QosxProcessMbsmMessage (&Msg, i4Event);
            break;
#endif

#ifdef ELPS_WANTED
        case MBSM_PROTO_COOKIE_ELPS:
            i4RetVal = ElpsMbsmPostMessage (&Msg, i4Event);
            break;
#endif /* ECFM_WANTED */
#ifdef ERPS_WANTED
        case MBSM_PROTO_COOKIE_ERPS:
            i4RetVal = ErpsMbsmPostMessage (&Msg, i4Event);
            break;
#endif /* ERPS_WANTED */
#ifdef BFD_WANTED
        case MBSM_PROTO_COOKIE_BFD:
            i4RetVal = BfdMbsmPostMessage (&Msg, i4Event);
            break;
#endif /* BFD_WANTED */
#ifdef RBRG_WANTED
        case MBSM_PROTO_COOKIE_RBRG:
            i4RetVal = RbrgApiMbsmNotification (&Msg, i4Event);
            break;
#endif

            /* Similarly for all the protocols */
        default:
            break;
    }
    if ((i4ProtoCookie == MBSM_PROTO_COOKIE_LINKSTATUS) &&
        ((ISS_GET_STACKING_MODEL () == ISS_CTRL_PLANE_STACKING_MODEL) ||
         (ISS_GET_STACKING_MODEL () == ISS_DISS_STACKING_MODEL)))
    {
        /* In Dual Unit stacking,Hardware Information of the remote ports
         * will be maintained , once remote node comes up.
         * When a card is removed , Hardware Information for the remote 
         * ports needs to be cleared .Few Protocols requires the remote port 
         * information to clear their Hardware  tables .
         * So port Information is cleared  after all protocols 
         * processes the card removal event .
         */

        if (MBSM_SLOT_INDEX_STATUS (i4SlotId) == MBSM_STATUS_NP)
        {
            for (u4IfIndex = MBSM_PORT_INFO_STARTINDEX (&(Msg.MbsmPortInfo));
                 u4IfIndex < (MBSM_PORT_INFO_STARTINDEX (&(Msg.MbsmPortInfo))) +
                 (MBSM_PORT_INFO_PORTCOUNT (&(Msg.MbsmPortInfo))); u4IfIndex++)
            {
                MEMSET (&HwIdInfo, 0, sizeof (tHwIdInfo));
                HwIdInfo.u4IfIndex = u4IfIndex;
                HwIdInfo.u1Status = FNP_FALSE;
                CfaCfaNpRemoteSetHwInfo (&HwIdInfo);
            }

        }

    }
    return (i4RetVal);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmNotifyLoadSharingStatusChgToProto
 *
 *    Description          : This function notifies the protocol modules of   
 *                           (vlan, pim & dvmrp) the change in load shraing 
 *                           status. Based on the Load sharing status, 
 *                           protocols program their tbls:
 *                           Vlan table, L2 Multicast table and IP Multicast
 *                           table.     
 *
 *    Input(s)             : i4Event  - Load sharing specific event for the 
 *                                      Protocol to be notified.   
 *
 *    Returns              : MBSM_SUCCESS/MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmNotifyLoadSharingStatusChgToProto (INT4 i4MbsmLoadSharingFlag)
{
    INT4                i4Event = MBSM_MSG_UNKOWN;

    if (i4MbsmLoadSharingFlag == MBSM_LOAD_SHARING_ENABLE)
    {
        i4Event = MBSM_MSG_LOAD_SHARING_ENABLE;
    }
    else if (i4MbsmLoadSharingFlag == MBSM_LOAD_SHARING_DISABLE)
    {
        i4Event = MBSM_MSG_LOAD_SHARING_DISABLE;
    }

    if ((i4Event == MBSM_MSG_LOAD_SHARING_ENABLE) ||
        (i4Event == MBSM_MSG_LOAD_SHARING_DISABLE))
    {
#ifdef VLAN_WANTED
        if (VlanMbsmPostMessage (NULL, i4Event) == MBSM_FAILURE)
        {
            return (MBSM_FAILURE);
        }
#endif /* VLAN_WANTED */

#ifdef PIM_WANTED
        if (PimMbsmPostMessage (NULL, i4Event) == MBSM_FAILURE)
        {
            return (MBSM_FAILURE);
        }
#endif /* PIM_WANTED */

#ifdef DVMRP_WANTED
        if (DvmrpMbsmPostMessage (NULL, i4Event) == MBSM_FAILURE)
        {
            return (MBSM_FAILURE);
        }
#endif /* DVMRP_WANTED */
    }
    return (MBSM_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmNotifyLoadSharingStatusChgToMbsm
 *
 *    Description          : This function notifies the MBSM module of   
 *                           the change in load shraing status. Based on the 
 *                           Load sharing status, MBSM program the tbls:
 *                           SrcModBlk tbl, Unicast tbl, ModPortMap tbl.
 *
 *    Input(s)             : i4Event  - Load sharing specific event for the 
 *                                      MBSM to be notified.   
 *
 *    Returns              : MBSM_SUCCESS/MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmNotifyLoadSharingStatusChgToMbsm (INT4 i4MbsmLoadSharingFlag)
{
    INT4                i4Event = MBSM_MSG_UNKOWN;
    tMbsmMsg           *pMbsmHwMsg;

    if (i4MbsmLoadSharingFlag == MBSM_LOAD_SHARING_ENABLE)
    {
        i4Event = MBSM_MSG_LOAD_SHARING_ENABLE;
    }
    else if (i4MbsmLoadSharingFlag == MBSM_LOAD_SHARING_DISABLE)
    {
        i4Event = MBSM_MSG_LOAD_SHARING_DISABLE;
    }

    if ((i4Event == MBSM_MSG_LOAD_SHARING_ENABLE) ||
        (i4Event == MBSM_MSG_LOAD_SHARING_DISABLE))
    {
        if (!(pMbsmHwMsg = MemAllocMemBlk (MBSM_MSG_QUEUE_MEMPOOL_ID)))
        {
            MBSM_DBG (MBSM_TRC_ERR, MBSM_PROTO,
                      "Cannot allocate memory for message in MbsmNotifyMbsmFromDriver - FAILURE.\n");
            return (MBSM_FAILURE);
        }

        pMbsmHwMsg->i4MsgType = i4Event;

        if (OsixSendToQ (MBSM_TASK_NODE_ID,
                         (const UINT1 *) MBSM_MSG_QUEUE_NAME,
                         (tOsixMsg *) (VOID *) pMbsmHwMsg,
                         OSIX_MSG_NORMAL) != OSIX_SUCCESS)
        {
            /* Protocols should release the buffer on failure */
            MemReleaseMemBlock (MBSM_MSG_QUEUE_MEMPOOL_ID,
                                (UINT1 *) pMbsmHwMsg);
            return MBSM_FAILURE;
        }

        if (MBSM_SEND_EVENT (MBSM_TASK_ID,
                             MBSM_LOAD_SHARING_EVENT) == OSIX_FAILURE)
        {
            return MBSM_FAILURE;
        }
    }

    return (MBSM_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmSendBootMsgToCfa
 *
 *    Description          : This function notifies the CFA of the boot up    
 *                           event with the slot information present in the  
 *                           Slot Map Table.
 *
 *    Input(s)             : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gaMbsmSlotMapTable.
 *
 *    Global Variables Modified : None.            
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns              : MBSM_SUCCESS/MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmSendBootMsgToCfa (VOID)
{
    INT4                i4NumEntries = 0;
    INT4                i4Index;
    tMbsmCfaHwInfoMsg   MbsmCfaBootMsg[MBSM_MAX_SLOTS];
    INT4                i4RetVal = MBSM_SUCCESS;
    INT2                i2OffSet = 0;

    for (i4Index = MBSM_SLOT_INDEX_START; i4Index < (MBSM_MAX_SLOTS +
                                                     MBSM_SLOT_INDEX_START);
         i4Index++)
    {
        if (MBSM_SLOT_INDEX_CARDTYPE (i4Index) != MBSM_INVALID_CARDTYPE)
        {
            i4NumEntries++;
        }
    }

    if (i4NumEntries)
    {
        for (i4Index = MBSM_SLOT_INDEX_START; i4Index < (MBSM_MAX_SLOTS +
                                                         MBSM_SLOT_INDEX_START);
             i4Index++)
        {
            if (MBSM_SLOT_INDEX_CARDTYPE (i4Index) != MBSM_INVALID_CARDTYPE)
            {
                if (MbsmFormMsgToCfa (i4Index, MbsmCfaBootMsg, i2OffSet)
                    == MBSM_FAILURE)
                {
                    return (MBSM_FAILURE);
                }
                i2OffSet++;
            }
        }

    }
    else
    {
        MbsmCfaBootMsg[0].i4NumEntries = 0;
    }

    /* Post the message to CFA */
    if (CfaMbsmMsgToCfa ((UINT1 *) &MbsmCfaBootMsg, MBSM_MSG_BOOTUP_DISC) ==
        CFA_FAILURE)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_PROTO,
                  "Error in MbsmSendBootMsgToCfa - Message to CFA Failed - FAILURE.\n");
        i4RetVal = MBSM_FAILURE;
    }

    return (i4RetVal);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmNotifyCfaPreConfigDel
 *
 *    Description          : This function notifies the CFA of the deletion   
 *                           of the pre-configured slot configuration so that
 *                           the corresponding interfaces are deleted in CFA.
 *
 *    Input(s)             : i4SlotId - Slot ID of the deleted interface.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gaMbsmSlotMapTable.
 *
 *    Global Variables Modified : None.            
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns              : MBSM_SUCCESS/MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmNotifyCfaPreConfigDel (INT4 i4SlotId)
{
    INT2                i2OffSet = 0;
    tMbsmCfaHwInfoMsg   MbsmCfaHwInfoMsg;
    INT4                i4RetVal = MBSM_SUCCESS;

    if (MbsmFormMsgToCfa (i4SlotId, &MbsmCfaHwInfoMsg, i2OffSet)
        == MBSM_FAILURE)
    {
        return (MBSM_FAILURE);
    }

    /* Post the message to CFA */
    if (CfaMbsmMsgToCfa ((UINT1 *) &MbsmCfaHwInfoMsg, MBSM_MSG_PRECONFIG_DEL)
        == CFA_FAILURE)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_PROTO,
                  "Error in MbsmNotifyCfaPreConfigDel - Message to CFA Failed - FAILURE.\n");
        i4RetVal = MBSM_FAILURE;
    }

    return (i4RetVal);
}

/******************************************************************************
 * Function           : MbsmSendEventToMbsmTask
 * Input(s)           : u4Event - Event
 * Output(s)          : None
 * Returns            : MBSM_SUCCESS/MBSM_FAILURE
 * Action             : Other modules should call this routine to
 *                      post any event to MBSM task.
 ******************************************************************************/

UINT4
MbsmSendEventToMbsmTask (UINT4 u4Event)
{
    if (MBSM_TASK_ID == 0)
    {
        if (OsixGetTaskId (SELF, (const UINT1 *) MBSM_TASK_NAME,
                           &MBSM_TASK_ID) != OSIX_SUCCESS)
        {
            MBSM_DBG (MBSM_TRC_ERR, MBSM_PROTO, "Event send to MBSM failed\n");
            return MBSM_FAILURE;
        }
    }

    if (MBSM_SEND_EVENT (MBSM_TASK_ID, u4Event) != OSIX_SUCCESS)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_PROTO, "Event send to MBSM failed\n");
        return MBSM_FAILURE;
    }
    return MBSM_SUCCESS;
}

/******************************************************************************
 *  Function           : MbsmIsNpBulkSyncInProgress
 *
 *  Description        : This functions checks if bulk NP sync update is in progress. 
 *                       for the input module.
 *                       This function return OSIX_TRUE, when the following
 *                       conditions are evaluated to TRUE, for Control Plane
 *                       stacking model
 *                       
 *                       * Remote Node is present. 
 *                       * The input proto cookie is lesser than or equal to the 
 *                         current processed cookie, which means essentially, for 
 *                         this input cookie, all the pre-requisites are still 
 *                         pending as the NP bulk sync-up is in progress. 
 *
 *
 *  Input(s)           : u4NpModId - NP module Identifier 
 *                       ranging from NP_VLAN_MOD to 
 *                                    NP_MPLS_MOD 
 *
 *  Output(s)          : None
 *
 *  Returns            : OSIX_TRUE for the conditions matching in Description 
 *                       otherwise OSIX_FALSE.
 *
 *  Action             : NPUTIL layer calls this function with their 
 *                       NP module IDs to check if the pre-requisite
 *                       NP programming is handled in the remote node by MBSM
 *                       module. 
 ******************************************************************************/
PUBLIC UINT1
MbsmIsNpBulkSyncInProgress PROTO ((UINT4 u4NpModId))
{
   UINT4 u4CallerProtoCookie = MBSM_PROTO_COOKIE_INVALID;

   if ((ISS_GET_STACKING_MODEL () != ISS_CTRL_PLANE_STACKING_MODEL) ||
       (CfaGetPeerNodeCount () == 0))
   {
       return OSIX_FALSE;
   }

   if ((MBSM_PROTO_COOKIE_INVALID == gu4CurrentProtoCookie) ||
       (MBSM_PROTO_COOKIE_NULL <= gu4CurrentProtoCookie))
   {
       return OSIX_FALSE;
   }
   /* Convert the input Module ID to the corresponding proto cookie ID. 
    * Mapping between the enums tMbsmProtoCookie and tNpModule.
    */
   switch (u4NpModId)
   {
#ifdef VLAN_WANTED
      case NP_VLAN_MOD:
           u4CallerProtoCookie = MBSM_PROTO_COOKIE_VLAN;
           break;
#endif
#if defined (RSTP_WANTED) || defined (MSTP_WANTED)
      case NP_MSTP_MOD: /* Intentional fallthrough */
      case NP_RSTP_MOD:
           u4CallerProtoCookie = MBSM_PROTO_COOKIE_AST;
           break;
#endif
#ifdef LA_WANTED
      case NP_LA_MOD:
           u4CallerProtoCookie = MBSM_PROTO_COOKIE_LA;
           break;
#endif
#ifdef PNAC_WANTED
      case NP_PNAC_MOD:
           u4CallerProtoCookie = MBSM_PROTO_COOKIE_PNAC;
           break;
#endif
#ifdef VCM_WANTED
      case NP_VCM_MOD:
           u4CallerProtoCookie = MBSM_PROTO_COOKIE_VCM;
           break;
#endif
#if defined (IGS_WANTED) || defined (MLDS_WANTED)
      case NP_IGS_MOD: /* Intentional fallthrough */
      case NP_MLDS_MOD:
           u4CallerProtoCookie = MBSM_PROTO_COOKIE_SNOOP;
           break;
#endif
      case NP_CFA_MOD: /* Intentional fallthrough */
      case NP_ISSSYS_MOD:
           u4CallerProtoCookie = MBSM_PROTO_COOKIE_CFA;
           break;
#ifdef EOAM_WANTED
      case NP_EOAM_MOD:
      case NP_EOAMFM_MOD:
           u4CallerProtoCookie = MBSM_PROTO_COOKIE_EOAM;
           break;
#endif
#ifdef ELPS_WANTED
      case NP_ELPS_MOD:
           u4CallerProtoCookie = MBSM_PROTO_COOKIE_ELPS;
           break;
#endif
#ifdef ECFM_WANTED
      case NP_ECFM_MOD:
           u4CallerProtoCookie = MBSM_PROTO_COOKIE_ECFM;
           break;
#endif
#ifdef ERPS_WANTED
      case NP_ERPS_MOD:
           u4CallerProtoCookie = MBSM_PROTO_COOKIE_ERPS;
           break;
#endif
#ifdef IP_WANTED
      case NP_IP_MOD:
           u4CallerProtoCookie = MBSM_PROTO_COOKIE_IP_FWD;
           break;
#endif
#ifdef IP6_WANTED
      case NP_IPV6_MOD:
           u4CallerProtoCookie = MBSM_PROTO_COOKIE_IP6_FWD;
           break;
#endif
#ifdef MPLS_WANTED
      case NP_MPLS_MOD:
           u4CallerProtoCookie = MBSM_PROTO_COOKIE_MPLS;
           break;
#endif
      default: /* All the modules without MBSM support, 
                * will fall in this category. Hence there is not a need
                * to check NP Bulk sync-up status for them. 
                * Return OSIX_FALSE always.
                */
           return OSIX_FALSE;
   } /* End of switch */

   if (gu4CurrentProtoCookie <= u4CallerProtoCookie)
   {
       return OSIX_TRUE;
   }
   return OSIX_FALSE;
}

/******************************************************************************
 *  Function           : MbsmGetNpSyncProgressStatus
 *
 *  Description        : This function returns the MBSM NP Sync status whether
 *                       it is in progress or not.
 *
 *  Input(s)           : None
 *
 *  Output(s)          : u4Status - OSIX_TRUE - if MBSM is in progress
 *                                  OSIX_FALSE - if MBSM is not started/completed/
 *                                               no card is attached etc.
 ******************************************************************************/
PUBLIC VOID
MbsmGetNpSyncProgressStatus PROTO ((UINT4 *pu4Status))
{
   if (pu4Status == NULL)
   {
        /* Preventive check */
        return;
   }
   *pu4Status = OSIX_FALSE;

   if ((ISS_GET_STACKING_MODEL () != ISS_CTRL_PLANE_STACKING_MODEL) ||
       (CfaGetPeerNodeCount () == 0))
   {
       /* If not control plane stacking model or no peer exists */
       return;
   }
   if ((gu4CurrentProtoCookie > MBSM_PROTO_COOKIE_INVALID) &&
       (gu4CurrentProtoCookie < MBSM_PROTO_COOKIE_NULL))
   {
        /* if gu4CurrentProtoCookie = MBSM_PROTO_COOKIE_INVALID
         * then MBSM programming itself not started.
         * If gu4CurrentProtoCookie = MBSM_PROTO_COOKIE_NULL,
         * then MBSM programming had finished.
         * Hence the above validation excludes them. */
        *pu4Status = OSIX_TRUE;
        return;
   }
   return;
}

