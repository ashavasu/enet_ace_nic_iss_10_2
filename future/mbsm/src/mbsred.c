/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mbsred.c,v 1.39 2016/06/22 10:21:24 siva Exp $
 *
 * Description: This file contains the routines for interaction  
 *              with RM.
 *******************************************************************/
#ifndef _MBSM_RM_C_
#define _MBSM_RM_C_

#include "mbsminc.h"
#include "rmgr.h"

#ifdef CLI_WANTED
#include "cli.h"
#endif

#ifdef NPAPI_WANTED
#include "npapi.h"
#include "mbsnp.h"
#endif /* NPAPI_WANTED */

tMbsmRedStates      gMbsmRedPrevNodeState = MBSM_RED_IDLE;
/*****************************************************************************/
/* Function Name      : MbsmRedRmInitAndDeRegister                           */
/*                                                                           */
/* Description        : Deinitialise the RED Global variables                */
/*                      pertaining to MBSM and deregister with RM.           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gMbsmRedGlobalInfo                                   */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gMbsmRedGlobalInfo                                   */
/*                                                                           */
/* Return Value(s)    : MBSM_SUCCESS/MBSM_FAILURE                            */
/*****************************************************************************/
INT4
MbsmRedRmInitAndDeRegister (VOID)
{
    MBSM_LOCK ();
    MBSM_RED_NODE_STATUS () = MBSM_RED_IDLE;
    gMbsmRedPrevNodeState = MBSM_RED_IDLE;
    MBSM_RED_BULK_REQ_RCVD () = MBSM_RED_FALSE;
#ifdef NPAPI_WANTED
    MBSM_RED_HW_AUDIT_TASK_ID () = MBSM_INIT_VAL;
#endif
    MBSM_RED_NUM_STANDBY_NODES () = 0;
    MBSM_RED_BULK_UPD_NEXT_SLOT () = MBSM_SLOT_INDEX_START;
    MBSM_UNLOCK ();

    if (MbsmRedDeRegisterWithRM () == MBSM_FAILURE)
    {
        MBSM_DBG (MBSM_RED_TRC, MBSM_RED,
                  "ERROR! MBSM deregistration with RM failed!!!\n");
        return MBSM_FAILURE;
    }
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MbsmRedRmInitAndRegister                             */
/*                                                                           */
/* Description        : Initialise the RED Global variables                  */
/*                      pertaining to MBSM and registration with RM.         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gMbsmRedGlobalInfo                                   */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gMbsmRedGlobalInfo                                   */
/*                                                                           */
/* Return Value(s)    : MBSM_SUCCESS/MBSM_FAILURE                            */
/*****************************************************************************/
INT4
MbsmRedRmInitAndRegister (VOID)
{
    MBSM_LOCK ();
    MBSM_RED_NODE_STATUS () = MBSM_RED_IDLE;
    gMbsmRedPrevNodeState = MBSM_RED_IDLE;
    MBSM_RED_NUM_STANDBY_NODES () = MBSM_INIT_VAL;
    MBSM_RED_BULK_REQ_RCVD () = MBSM_RED_FALSE;
    MBSM_RED_PEER_UP_RECEIVED () = MBSM_RED_FALSE;
#ifdef NPAPI_WANTED
    MBSM_RED_HW_AUDIT_TASK_ID () = MBSM_INIT_VAL;
#endif
    MBSM_RED_BULK_UPD_NEXT_SLOT () = MBSM_SLOT_INDEX_START;
    MBSM_UNLOCK ();

    if (MbsmRedRegisterWithRM () == MBSM_FAILURE)
    {
        MBSM_DBG (MBSM_RED_TRC, MBSM_RED,
                  "ERROR! MBSM Registration with RM failed!!!\n");
        return MBSM_FAILURE;
    }
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MbsmRedRegisterWithRM                                */
/*                                                                           */
/* Description        : Registers MBSM with RM so that RM will give          */
/*                      messages to MBSM.                                    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if registration is success then MBSM_SUCCESS         */
/*                      Otherwise MBSM_FAILURE                               */
/*****************************************************************************/
INT4
MbsmRedRegisterWithRM (VOID)
{
    tRmRegParams        RmRegParams;

    /* Register call-back function with RM module to get
     * node status (ACTIVE/STANDBY) from RM. */
    RmRegParams.u4EntId = RM_MBSM_APP_ID;
    RmRegParams.pFnRcvPkt = MbsmRcvPktFromRm;

    if (RmRegisterProtocols (&RmRegParams) == RM_FAILURE)
    {
        MBSM_DBG (MBSM_RED_TRC, MBSM_RED,
                  "ERROR! MBSM Registration with RM failed!!!\n");
        return MBSM_FAILURE;
    }
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MbsmRedDeRegisterWithRM                              */
/*                                                                           */
/* Description        : Deregisters MBSM with RM                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if deregistration is success then MBSM_SUCCESS       */
/*                      Otherwise MBSM_FAILURE                               */
/*****************************************************************************/
INT4
MbsmRedDeRegisterWithRM (VOID)
{
    if (RmDeRegisterProtocols (RM_MBSM_APP_ID) == RM_FAILURE)
    {
        MBSM_DBG (MBSM_RED_TRC, MBSM_RED,
                  "ERROR! MBSM Deregistration with RM failed!!!\n");
        return MBSM_FAILURE;
    }
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MbsmRcvPktFromRm                                     */
/*                                                                           */
/* Description        : RM processing Entry point. This function  constructs */
/*                      message containing the given RM event and RM message */
/*                      and post it to the MBSM queue.                       */
/*                                                                           */
/* Input(s)           : u1Event - The Event from RM to be handled            */
/*                      pData - The RM message                               */
/*                      u2DataLen - Length of Message                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RM_SUCCESS / RM_FAILURE                              */
/*****************************************************************************/
INT4
MbsmRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tMbsmMsg           *pMsg = NULL;

    if (((u1Event == RM_MESSAGE || (u1Event == RM_STANDBY_UP) ||
          (u1Event == RM_STANDBY_DOWN))) && (pData == NULL))
    {
        return RM_FAILURE;
    }

    if( u1Event == RM_FILE_TRANSFER_COMPLETE)
    {
        return RM_FAILURE;
    }

    if (gu1IsMbsmInitialised != MBSM_TRUE)
    {
        MBSM_DBG (MBSM_RED_TRC, MBSM_RED,
                  "ERROR! Mbsm is not Initialised!!!\r\n");

        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            RmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        return RM_FAILURE;
    }

    if ((pMsg = MemAllocMemBlk (MBSM_CTRL_QUEUE_MEMPOOL_ID)) == NULL)
    {
        MBSM_DBG (MBSM_RED_TRC, MBSM_RED,
                  "Ctrl Mesg ALLOC_MEM_BLOCK FAILED!!!\r\n");

        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            RmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        return RM_FAILURE;
    }

    MEMSET (pMsg, MBSM_INIT_VAL, sizeof (tMbsmMsg));

    pMsg->i4MsgType = MBSM_RM_QMSG;
    pMsg->uMsg.RmMsg.pFrame = pData;
    pMsg->uMsg.RmMsg.u2Length = u2DataLen;
    pMsg->uMsg.RmMsg.u1Event = u1Event;

    if (OsixSendToQ (MBSM_TASK_NODE_ID,
                     (const UINT1 *) MBSM_MSG_QUEUE_NAME,
                     (tOsixMsg *) (VOID *) pMsg,
                     OSIX_MSG_NORMAL) != OSIX_SUCCESS)
    {
        MBSM_DBG (MBSM_RED_TRC, MBSM_RED, "Rm Message enqueue FAILED\n");

        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            RmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        MBSM_RELEASE_CTRLQ_MEM_BLOCK (pMsg);

        return RM_FAILURE;

    }

    if (OsixSendEvent (MBSM_TASK_NODE_ID, (const UINT1 *) MBSM_TASK_NAME,
                       MBSM_RED_MSG_EVENT) != OSIX_SUCCESS)
    {
        MBSM_DBG (MBSM_RED_TRC, MBSM_RED, "Rm Event send FAILED\n");
        return RM_FAILURE;
    }

    return RM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MbsmProcessRmEvent                                   */
/*                                                                           */
/* Description        : This function is invoked to process the following    */
/*                      from RM module:-                                     */
/*                           - RM events and                                 */
/*                           - update messages.                              */
/*                      This function interprets the RM events and calls the */
/*                      corresponding module functions to process those      */
/*                      events.                                              */
/*                                                                           */
/* Input(s)           : pMsg - Pointer to the  input buffer.                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
MbsmProcessRmEvent (tMbsmMsg * pMsg)
{
    tRmNodeInfo        *pData = NULL;
    tRmProtoEvt         ProtoEvt;
    tRmProtoAck         ProtoAck;
    UINT4               u4SeqNum = 0;
    UINT4               u4StackingModel = 0;

    ProtoEvt.u4AppId = RM_MBSM_APP_ID;
    ProtoEvt.u4Error = RM_NONE;
    u4StackingModel = ISS_GET_STACKING_MODEL ();
    switch (pMsg->uMsg.RmMsg.u1Event)
    {
        case GO_ACTIVE:
            MBSM_RED_NUM_STANDBY_NODES () = RmGetStandbyNodeCount ();

            gMbsmRedPrevNodeState = MBSM_RED_NODE_STATUS ();
            MBSM_RED_NODE_STATUS () = MBSM_RED_ACTIVE;

            if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
            {
                if (gMbsmRedPrevNodeState == MBSM_RED_STANDBY)
                {
                    /* When state change from standby to active
                     * it has to wait for other slots in stack setup
                     * get attach to it*/
                    MbsmStartTimer (&gMbsmGlobalInfo.MbsmRecoveryTmr);
                }

            }
            else
            {
                MbsmProcessRmGoActive ();
            }

            if (gMbsmRedPrevNodeState == MBSM_RED_STANDBY)
            {
                /*Force-SwitchOver case */
                if ((RmGetForceSwitchOverFlag () == RM_FSW_OCCURED) ||
                    (RmGetPeerNodeCount () > 0))
                {
                    if (!((MBSM_RED_PEER_UP_RECEIVED () == MBSM_RED_TRUE) &&
                          (u4StackingModel == ISS_CTRL_PLANE_STACKING_MODEL)))
                    {
                        /* Recovery timer can be started in the following two conditions
                         * 1. if stacking model is not dual unit control plane stacking
                         * or 2.(if stacking model is dual unit control plane stacking and
                         * peer up is not received for the remote slot)
                         * Start the recovery timer
                         */
                        MbsmStartTimer (&gMbsmGlobalInfo.MbsmRecoveryTmr);
                    }
                }
                /* FailOver Case */
                else if (MBSM_TRUE == gLocalAttachRcvd)
                {
                    MbsmProcessFailover ();
                    gLocalAttachRcvd = MBSM_FALSE;
                }
                /* RM_STANDBY_TO_ACTIVE_EVT_PROCESSED event is not sent here,
                 * it will be sent when the MBSM level master-slave 
                 * slots are settled.*/
            }
            else
            {
                /* IDLE to Active event. */
                ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
                RmApiHandleProtocolEvent (&ProtoEvt);
            }
#ifdef L2RED_WANTED
            /* In Redundancy force-switchover case, there are chances of
             * Bulk Request may come from Standby before GO_ACTIVE */
            if ((MBSM_RED_BULK_REQ_RCVD () == MBSM_TRUE) &&
                (RmGetStaticConfigStatus () == RM_STATIC_CONFIG_RESTORED) &&
                (MBSM_RED_NUM_STANDBY_NODES () > 0))
            {
                MBSM_RED_BULK_REQ_RCVD () = MBSM_FALSE;
                MBSM_RED_BULK_UPD_NEXT_SLOT () = MBSM_SLOT_INDEX_START;
                MbsmRedHandleBulkUpdateEvent ();
            }
#endif
            break;

        case GO_STANDBY:
            if (MBSM_RED_NODE_STATUS () == MBSM_RED_IDLE)
            {
                MBSM_RED_NUM_STANDBY_NODES () = 0;
            }
            gMbsmRedPrevNodeState = MBSM_RED_NODE_STATUS ();
            MBSM_RED_NODE_STATUS () = MBSM_RED_STANDBY;
#ifdef L2RED_WANTED
#ifdef NPAPI_WANTED
            if (MBSM_RED_HW_AUDIT_TASK_ID () != MBSM_INIT_VAL)
            {
                /* Delete Hw Audit Task. Checking whether is running or not will
                 * be taken care inside the API*/
                OsixDeleteTask (0, MBSM_HARDWARE_AUDIT);
                MBSM_RED_HW_AUDIT_TASK_ID () = MBSM_INIT_VAL;
            }
#endif
            MbsmProcessRmGoStandby ();

#endif
            ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
            RmApiHandleProtocolEvent (&ProtoEvt);
            break;

        case RM_INIT_HW_AUDIT:
#ifdef NPAPI_WANTED
            if (MBSM_RED_NODE_STATUS () == MBSM_RED_ACTIVE)
            {
                /*Start H/w Audit */
                MbsmRedHwAudit ();
            }
#endif
            break;

        case RM_STANDBY_UP:
            pData = (tRmNodeInfo *) pMsg->uMsg.RmMsg.pFrame;
            MBSM_RED_NUM_STANDBY_NODES () = pData->u1NumStandby;
            if ((MBSM_RED_BULK_REQ_RCVD () == MBSM_TRUE) &&
                (RmGetStaticConfigStatus () == RM_STATIC_CONFIG_RESTORED))
            {
                MBSM_RED_BULK_REQ_RCVD () = MBSM_FALSE;
                /* Bulk request msg is recieved before RM_STANDBY_UP
                 * event.So we are sending bulk updates now. */
                MBSM_RED_BULK_UPD_NEXT_SLOT () = MBSM_SLOT_INDEX_START;
                MbsmRedHandleBulkUpdateEvent ();
            }
            RmReleaseMemoryForMsg ((UINT1 *) pData);
            if (u4StackingModel == ISS_CTRL_PLANE_STACKING_MODEL)
            {
                MbsmRedHandlePeerStateChangeEvent (pMsg->uMsg.RmMsg.u1Event);
            }
            break;

        case L2_INITIATE_BULK_UPDATES:
            MbsmRedSendBulkRequest ();
            break;

        case RM_CONFIG_RESTORE_COMPLETE:
            if ((MBSM_RED_BULK_REQ_RCVD () == MBSM_TRUE) &&
                (MBSM_RED_NUM_STANDBY_NODES () > 0))
            {
                MBSM_RED_BULK_REQ_RCVD () = MBSM_FALSE;
                /* Bulk request msg is recieved before RM_STANDBY_UP
                 * event.So we are sending bulk updates now. */
                MBSM_RED_BULK_UPD_NEXT_SLOT () = MBSM_SLOT_INDEX_START;
                MbsmRedHandleBulkUpdateEvent ();
            }
            break;

        case RM_STANDBY_DOWN:
            pData = (tRmNodeInfo *) pMsg->uMsg.RmMsg.pFrame;
            MBSM_RED_NUM_STANDBY_NODES () = pData->u1NumStandby;
            RmReleaseMemoryForMsg ((UINT1 *) pData);
            if (u4StackingModel == ISS_CTRL_PLANE_STACKING_MODEL)
            {
                MBSM_RED_PEER_UP_RECEIVED () = MBSM_RED_FALSE;
                MbsmRedHandlePeerStateChangeEvent (pMsg->uMsg.RmMsg.u1Event);
            }
            break;

        case RM_MESSAGE:
            /* Read the sequence number from the RM Header */
            RM_PKT_GET_SEQNUM (pMsg->uMsg.RmMsg.pFrame, &u4SeqNum);
            /* Remove the RM Header */
            RM_STRIP_OFF_RM_HDR (pMsg->uMsg.RmMsg.pFrame,
                                 pMsg->uMsg.RmMsg.u2Length);

            ProtoAck.u4AppId = RM_MBSM_APP_ID;
            ProtoAck.u4SeqNumber = u4SeqNum;

            MbsmRedHandleMessage (pMsg);
            /* Processing of message is over, hence free the RM message. */
            RM_FREE ((tRmMsg *) (pMsg->uMsg.RmMsg.pFrame));

            /* Sending ACK to RM */
            RmApiSendProtoAckToRM (&ProtoAck);
            break;

        case RM_TRIGGER_SELF_ATTACH:
            MbsmInitHwTopoDisc (MBSM_RED_NODE_STATUS ());
            break;

        case RM_DYNAMIC_SYNCH_AUDIT:
            MbsmRedHandleDynSyncAudit ();
            break;

        default:
            MBSM_DBG (MBSM_PROTO_TRC, MBSM_RED,
                      "MBSM: Receieved Unknown event from RM\n");
            break;
    }
}

/******************************************************************************
 * Function           : MbsmProcessRmGoActive 
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : MBSM_SUCCESS/MBSM_FAILURE. 
 * Action             : Function to process the GO_ACTIVE event received from RM 
 ******************************************************************************/
VOID
MbsmProcessRmGoActive (VOID)
{
    MbsmInitHwTopoDisc (MBSM_RED_NODE_STATUS ());
}

/******************************************************************************
 * Function           : MbsmProcessRmGoStandby 
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : MBSM_SUCCESS/MBSM_FAILURE. 
 * Action             : Function to process the GO_STANDBY event received 
 *                      from RM 
 ******************************************************************************/
VOID
MbsmProcessRmGoStandby (VOID)
{
    MbsmInitHwTopoDisc (MBSM_RED_NODE_STATUS ());
}

/*****************************************************************************/
/* Function Name      : MbsmRedHandleMessage                                 */
/*                                                                           */
/* Description        : This function is invoked whenever MBSM module        */
/*                      receives a message from Peer Node.                   */
/*                                                                           */
/* Input(s)           : pMsg - RM Message                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
MbsmRedHandleMessage (tMbsmMsg * pMsg)
{
    VOID               *pData = NULL;
    UINT4               u4Offset = MBSM_INIT_VAL;
    UINT1               u1MessageType = 0;
    UINT2               u2Len;
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_MBSM_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    pData = (VOID *) pMsg->uMsg.RmMsg.pFrame;

    while (1)
    {
        if (u4Offset >= pMsg->uMsg.RmMsg.u2Length)
            break;

        MBSM_RM_GET_1_BYTE ((tRmMsg *) pData, &u4Offset, u1MessageType);

        /* Length of the Individual Message */
        MBSM_RM_GET_2_BYTE ((tRmMsg *) pData, &u4Offset, u2Len);

        if (u1MessageType == MBSM_RED_BULK_REQ)
        {
            MBSM_DBG (MBSM_PROTO_TRC, MBSM_RED,
                      "MBSM-RED: Rcvd Bulk Req Message \n");

            if(IssuGetMaintModeOperation() == OSIX_TRUE)
            {
                /* During ISSU Maintenance Mode load version,Peer down
                 * wan not notified to higher layer protocols. Hence
                 * when standby is coming up in ISSU Maintenance Mode,
                 * verifying Peer node status from RM Database.
                 * */

                if ((RmGetIssuPeerNodeCount () == 0) ||
                    (RmGetStaticConfigStatus () != RM_STATIC_CONFIG_RESTORED))
                {
                    /* This is a special case, where bulk request msg from
                     * standby is coming before RM_STANDBY_UP. So no need to
                     * process the bulk request now. Bulk updates will be send
                     * on RM_STANDBY_UP event.
                     */
                    MBSM_RED_BULK_REQ_RCVD () = MBSM_TRUE;
                    MBSM_DBG (MBSM_PROTO_TRC, MBSM_RED,
                    "MBSM-RED: Setting MBSM_RED_BULK_REQ_RCVD Flag during ISSU MM \n");
                    return;
                }
            }
            if ((MBSM_RED_IS_STANDBY_UP () == MBSM_FALSE) ||
                (RmGetStaticConfigStatus () != RM_STATIC_CONFIG_RESTORED))
            {
                /* This is a special case, where bulk request msg from
                 * standby is coming before RM_STANDBY_UP. So no need to
                 * process the bulk request now. Bulk updates will be send
                 * on RM_STANDBY_UP event.
                 */
                MBSM_RED_BULK_REQ_RCVD () = MBSM_TRUE;
                return;
            }

            MBSM_RED_BULK_REQ_RCVD () = MBSM_FALSE;
            /* On recieving MBSM_RED_BULK_REQ, Bulk updation process
             * should be restarted.
             */
            MBSM_RED_BULK_UPD_NEXT_SLOT () = MBSM_SLOT_INDEX_START;
            MbsmRedHandleBulkUpdateEvent ();
            return;
        }

        /* If Mbsm Node Status is not Standby, then no need to handle received
         * messages from Peer Node*/
        if (MBSM_RED_NODE_STATUS () != MBSM_RED_STANDBY)
        {
            MBSM_DBG (MBSM_PROTO_TRC, MBSM_RED,
                      "RED: Node Status is not Standby."
                      "So just Drop the msg.!!! \n");
            return;
        }

        /* Decode Message */
        switch (u1MessageType)
        {
            case MBSM_RED_ATTACH_MSG:

                MbsmRedUpdateSlotAndPortMapTable (pData, &u4Offset, u2Len);

                break;

            case MBSM_RED_DETACH_MSG:

                MbsmRedUpdateDetach (pData, &u4Offset, u2Len);

                break;

            case MBSM_RED_BULK_UPD_TAIL_MSG:
                /* In case of IDLE-STANDBY transition, MBSM on completing
                 * its syncup will give GO_STANDBY to all other protocols.
                 * But in case of ACTIVE-STANDBY transition, MBSM will not
                 * do anything.*/
                /* IDLE-STANDBY transition */
                if ((gMbsmRedPrevNodeState == MBSM_RED_IDLE)
                    && (MBSM_RED_NODE_STATUS () == MBSM_RED_STANDBY))
                {
                    RmSendEventToAppln (RM_MBSM_APP_ID);
                }

                /* ACTIVE - STANDBY transition */
                else
                {
                    MBSM_RED_NUM_STANDBY_NODES () = 0;
                }
                ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;
                RmApiHandleProtocolEvent (&ProtoEvt);
                break;

            default:
                MBSM_ASSERT ();
                break;
        }
    }
}

/*****************************************************************************/
/* Function Name      : MbsmRedUpdateSlotAndPortMapTable                     */
/*                                                                           */
/* Description        : This function is used to update MBSM data structure  */
/*                      when a sync-up message receives in Standby Node      */
/*                                                                           */
/* Input(s)           : pBuf - RM Message                                    */
/*                      pu4Offset - Offset                                   */
/*                      u2Len - Msg Length                                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : MBSM_SUCCESS/MBSM_FAILURE                            */
/*****************************************************************************/
INT4
MbsmRedUpdateSlotAndPortMapTable (VOID *pBuf, UINT4 *pu4Offset, UINT2 u2Len)
{
    INT4                i4SlotId;
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_MBSM_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (u2Len != MBSM_RED_ATTACH_MSG_LEN)
    {
        *pu4Offset = *pu4Offset + u2Len;
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return MBSM_FAILURE;
    }

    /* Update Slot-Map Table */
    MBSM_RM_GET_4_BYTE (pBuf, pu4Offset, i4SlotId);

    if (MBSM_SLOT_MAP_ENTRY (i4SlotId) == NULL)
    {
        MBSM_DBG (MBSM_RED_TRC, MBSM_RED,
                  "Slot Map Entry is NULL. So cant update rcvd syncup\r\n");
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return MBSM_FAILURE;
    }

    MBSM_SLOT_INDEX_SLOT_ID (i4SlotId) = i4SlotId;

    MBSM_RM_GET_4_BYTE (pBuf, pu4Offset, MBSM_SLOT_INDEX_MODTYPE (i4SlotId));
    MBSM_RM_GET_4_BYTE (pBuf, pu4Offset, MBSM_SLOT_INDEX_CARDTYPE (i4SlotId));
    MBSM_RM_GET_4_BYTE (pBuf, pu4Offset, MBSM_SLOT_INDEX_NUMPORTS (i4SlotId));
    MBSM_RM_GET_4_BYTE (pBuf, pu4Offset, MBSM_SLOT_INDEX_STARTINDEX (i4SlotId));
    MBSM_RM_GET_1_BYTE (pBuf, pu4Offset,
                        MBSM_SLOT_INDEX_FABRIC_TYPE (i4SlotId));
    MBSM_RM_GET_1_BYTE (pBuf, pu4Offset, MBSM_SLOT_INDEX_STATUS (i4SlotId));
    MBSM_RM_GET_1_BYTE (pBuf, pu4Offset, MBSM_SLOT_INDEX_PREVSTATUS (i4SlotId));
    MBSM_RM_GET_1_BYTE (pBuf, pu4Offset, MBSM_SLOT_INDEX_ISPRECONF (i4SlotId));

    /* Update Port-Map Table */
    if (MBSM_PORT_INFO_ENTRY (i4SlotId) == NULL)
    {
        MBSM_DBG (MBSM_RED_TRC, MBSM_RED,
                  "PortInfo Entry is NULL. So cant update rcvd syncup\r\n");
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return MBSM_FAILURE;
    }
    MBSM_RM_GET_4_BYTE (pBuf, pu4Offset, MBSM_PORT_INDEX_PORTCOUNT (i4SlotId));
    MBSM_RM_GET_4_BYTE (pBuf, pu4Offset, MBSM_PORT_INDEX_STARTINDEX (i4SlotId));
    MBSM_RM_GET_N_BYTE (pBuf, MBSM_PORT_INDEX_PORTLIST (i4SlotId),
                        pu4Offset, sizeof (tPortList));
    MBSM_RM_GET_N_BYTE (pBuf, MBSM_PORT_INDEX_PORTLISTSTATUS (i4SlotId),
                        pu4Offset, sizeof (tPortList));
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MbsmRedUpdateDetach                                  */
/*                                                                           */
/* Description        : This function is used to update MBSM data structure  */
/*                      when a DETACH sync message receives in Standby Node  */
/*                                                                           */
/* Input(s)           : pBuf - RM Message                                    */
/*                      pu4Offset - Offset                                   */
/*                      u2Len - Msg Length                                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : MBSM_SUCCESS/MBSM_FAILURE                            */
/*****************************************************************************/
INT4
MbsmRedUpdateDetach (VOID *pBuf, UINT4 *pu4Offset, UINT2 u2Len)
{
    INT4                i4SlotId;
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_MBSM_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (u2Len != MBSM_RED_DETACH_MSG_LEN)
    {
        *pu4Offset = *pu4Offset + u2Len;
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return MBSM_FAILURE;
    }

    /* Update Slot-Map Table */
    MBSM_RM_GET_4_BYTE (pBuf, pu4Offset, i4SlotId);

    if (MBSM_SLOT_MAP_ENTRY (i4SlotId) == NULL)
    {
        MBSM_DBG (MBSM_RED_TRC, MBSM_RED,
                  "Slot Map Entry is NULL. So cant update rcvd syncup\r\n");
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return MBSM_FAILURE;
    }

    MBSM_SLOT_INDEX_SLOT_ID (i4SlotId) = i4SlotId;

    MBSM_RM_GET_1_BYTE (pBuf, pu4Offset, MBSM_SLOT_INDEX_STATUS (i4SlotId));
    MBSM_RM_GET_1_BYTE (pBuf, pu4Offset, MBSM_SLOT_INDEX_PREVSTATUS (i4SlotId));

    return MBSM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MbsmRedSendBulkRequest                               */
/*                                                                           */
/* Description        : This function is invoked whenever Bulk Request       */
/*                       needs to be send                                    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
MbsmRedSendBulkRequest (VOID)
{
    /* Initialte a Bulk Request to the ACTIVE */
    MbsmRedSendSyncMessages (0, MBSM_RED_BULK_REQ);
}

/*****************************************************************************/
/* Function Name      : MbsmRedSendSyncMessages                              */
/*                                                                           */
/* Description        : This function is invoked to  Alloc and Send Bulk     */
/*                      Sync Up Messages                                     */
/*                                                                           */
/* Input(s)           : u4PortIfIndex - Physical Port Index                  */
/*                      u2LocalPortId - Context based local Port Number      */
/*                      u1MessageType - Message Type                         */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : MBSM_SUCCESS/MBSM_FAILURE.                           */
/*****************************************************************************/
INT4
MbsmRedSendSyncMessages (INT4 i4SlotId, UINT1 u1MsgType)
{
    UINT4               u4Offset = MBSM_INIT_VAL;
    UINT2               u2Len = 0;
    VOID               *pBuf = NULL;
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_MBSM_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (u1MsgType == MBSM_RED_BULK_REQ)
    {
        MBSM_DBG (MBSM_RED_TRC, MBSM_RED, 
                  "MbsmRedSendSyncMessages: Bulk Request to the ACTIVE\n\r");
        u2Len = MBSM_RED_BULK_REQ_MSG_LEN;
        if ((pBuf = RM_ALLOC_TX_BUF (u2Len)) == NULL)
        {
            MBSM_DBG (MBSM_RED_TRC, MBSM_RED, "RM Allocation Failed\n");
            ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
            RmApiHandleProtocolEvent (&ProtoEvt);
            return MBSM_FAILURE;
        }
        MBSM_RM_PUT_1_BYTE (pBuf, &u4Offset, u1MsgType);
        MBSM_RM_PUT_2_BYTE (pBuf, &u4Offset, u2Len);
    }
    else
    {
        /* else all messages are from Active to Standby */

        /* Don't send dynamic updates, when
         *  * The node is not active (or)
         *  * Standby node is not present
         */
        if ((MBSM_RED_NODE_STATUS () != MBSM_RED_ACTIVE) ||
            (MBSM_RED_NUM_STANDBY_NODES () == 0))
        {
            MBSM_DBG (MBSM_TRC_ERR, MBSM_RED,
                      "Dynamic updates can not be sent\n");
            return MBSM_SUCCESS;
        }

        if (u1MsgType == MBSM_RED_ATTACH_MSG)
        {
            MBSM_DBG (MBSM_PROTO_TRC, MBSM_RED,
                      "[MbsmRedSendSyncMessages] MBSM_RED_ATTACH_MSG\n");
            u2Len = MBSM_RED_ATTACH_MSG_LEN;
        }
        else if (u1MsgType == MBSM_RED_DETACH_MSG)
        {
            MBSM_DBG (MBSM_PROTO_TRC, MBSM_RED,
                      "[MbsmRedSendSyncMessages] MBSM_RED_DETACH_MSG\n");
            u2Len = MBSM_RED_DETACH_MSG_LEN;
        }

        if (u2Len != 0)
        {
            if ((pBuf = RM_ALLOC_TX_BUF (u2Len)) == NULL)
            {
                MBSM_DBG (MBSM_RED_TRC, MBSM_RED,
                          "RM Allocation Failed\n");
                return MBSM_FAILURE;
            }

            MbsmRedFormMessage (i4SlotId, u1MsgType, pBuf, &u4Offset);
        }
    }
    if (MbsmRedSendUpdateToRM ((tRmMsg *) pBuf, (UINT2) u4Offset) ==
        MBSM_FAILURE)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
    }

    return MBSM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MbsmRedFormMessage                                   */
/*                                                                           */
/* Description        : This function is invoked to  Update RM Buffers  to   */
/*                      send the Update packet via the RM                    */
/*                                                                           */
/* Input(s)           : i4SlotId - Slot Id                                   */
/*                      u1MsgType - Message Type                             */
/*                      pBuf -      Pointer to the  O/p Buffer.              */
/*                      pu4Offset -   Pointer to the O/P Bufs valid Offset   */
/*                                                                           */
/* Output(s)          : pRmMsg ,pu4Offset                                    */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : MBSM_SUCCESS/MBSM_FAILURE.                           */
/*****************************************************************************/
INT4
MbsmRedFormMessage (INT4 i4SlotId, UINT1 u1MsgType, tRmMsg * pBuf,
                    UINT4 *pu4Offset)
{
    UINT2               u2Len;

    switch (u1MsgType)
    {
        case MBSM_RED_ATTACH_MSG:

            u2Len = MBSM_RED_ATTACH_MSG_LEN;

            MBSM_RM_PUT_1_BYTE (pBuf, pu4Offset, u1MsgType);

            MBSM_RM_PUT_2_BYTE (pBuf, pu4Offset, u2Len);

            /*Copy Slot-Map Table into Buffer */
            MBSM_RM_PUT_4_BYTE (pBuf, pu4Offset,
                                MBSM_SLOT_INDEX_SLOT_ID (i4SlotId));
            MBSM_RM_PUT_4_BYTE (pBuf, pu4Offset,
                                MBSM_SLOT_INDEX_MODTYPE (i4SlotId));
            MBSM_RM_PUT_4_BYTE (pBuf, pu4Offset,
                                MBSM_SLOT_INDEX_CARDTYPE (i4SlotId));
            MBSM_RM_PUT_4_BYTE (pBuf, pu4Offset,
                                MBSM_SLOT_INDEX_NUMPORTS (i4SlotId));
            MBSM_RM_PUT_4_BYTE (pBuf, pu4Offset,
                                MBSM_SLOT_INDEX_STARTINDEX (i4SlotId));
            MBSM_RM_PUT_1_BYTE (pBuf, pu4Offset,
                                MBSM_SLOT_INDEX_FABRIC_TYPE (i4SlotId));
            MBSM_RM_PUT_1_BYTE (pBuf, pu4Offset,
                                MBSM_SLOT_INDEX_STATUS (i4SlotId));
            MBSM_RM_PUT_1_BYTE (pBuf, pu4Offset,
                                MBSM_SLOT_INDEX_PREVSTATUS (i4SlotId));
            MBSM_RM_PUT_1_BYTE (pBuf, pu4Offset,
                                MBSM_SLOT_INDEX_ISPRECONF (i4SlotId));

            /*Copy Port-Map Table into Buffer */
            MBSM_RM_PUT_4_BYTE (pBuf, pu4Offset,
                                MBSM_PORT_INDEX_PORTCOUNT (i4SlotId));
            MBSM_RM_PUT_4_BYTE (pBuf, pu4Offset,
                                MBSM_PORT_INDEX_STARTINDEX (i4SlotId));
            MBSM_RM_PUT_N_BYTE (pBuf, MBSM_PORT_INDEX_PORTLIST (i4SlotId),
                                pu4Offset, sizeof (tPortList));
            MBSM_RM_PUT_N_BYTE (pBuf, MBSM_PORT_INDEX_PORTLISTSTATUS (i4SlotId),
                                pu4Offset, sizeof (tPortList));
            break;

        case MBSM_RED_DETACH_MSG:

            u2Len = MBSM_RED_DETACH_MSG_LEN;

            MBSM_RM_PUT_1_BYTE (pBuf, pu4Offset, u1MsgType);

            MBSM_RM_PUT_2_BYTE (pBuf, pu4Offset, u2Len);

            /*Copy Slot-Map Table Updated Infm into Buffer */
            MBSM_RM_PUT_4_BYTE (pBuf, pu4Offset, i4SlotId);

            MBSM_RM_PUT_1_BYTE (pBuf, pu4Offset,
                                MBSM_SLOT_INDEX_STATUS (i4SlotId));
            MBSM_RM_PUT_1_BYTE (pBuf, pu4Offset,
                                MBSM_SLOT_INDEX_PREVSTATUS (i4SlotId));

            /* Since no updations in Port-Map Table, 
             * its not sync up to Standby Node*/

            break;

        default:
            MBSM_ASSERT ();
            return MBSM_FAILURE;
    }
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MbsmRedSendUpdateToRM                                */
/*                                                                           */
/* Description        : This function sends Update to RM                     */
/*                                                                           */
/* Input(s)           : pMsg - RM Message                                    */
/*                      u2Len - Length of RM message                         */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : MBSM_SUCCESS/MBSM_FAILURE.                           */
/*****************************************************************************/
INT4
MbsmRedSendUpdateToRM (tRmMsg * pMsg, UINT2 u2Len)
{
    /* Call the API provided by RM to send the data to RM */
    if (RmEnqMsgToRmFromAppl
        (pMsg, u2Len, RM_MBSM_APP_ID, RM_MBSM_APP_ID) == RM_FAILURE)
    {
        MBSM_DBG (MBSM_RED_TRC, MBSM_RED, "Enq to RM from appl failed\n");
        /* pMsg is reed only in failure case. RM will free the buf
         * in success case. */

        RM_FREE (pMsg);
        return MBSM_FAILURE;
    }
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MbsmRedHandleBulkRequest                             */
/*                                                                           */
/* Description        : This function is invoked whenever MBSM module        */
/*                      requests for Bulk Update                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : Constructs Bulk Update                               */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
MbsmRedHandleBulkRequest (VOID)
{
    tRmMsg             *pBuf = NULL;
    UINT4               u4SlotId;
    UINT4               u4Offset = 0;
    UINT4               u4BulkUpdSlotCnt;
    UINT2               u2Len;
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_MBSM_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /* Assign Max Message Length to u2Len */
    u2Len = MBSM_RED_ATTACH_MSG_LEN;

    u4BulkUpdSlotCnt = MBSM_RED_NO_OF_SLOTS_PER_SUB_UPDATE;

    for (u4SlotId = MBSM_RED_BULK_UPD_NEXT_SLOT ();
         ((u4SlotId < (MBSM_MAX_SLOTS + MBSM_SLOT_INDEX_START))
          && u4BulkUpdSlotCnt > 0); u4SlotId++, u4BulkUpdSlotCnt--)
    {
        /* Update u4BulkUpdNextSlot, to resume the next sub bulk update
         * from where the previous sub bulk update left it out.
         */
        MBSM_RED_BULK_UPD_NEXT_SLOT ()++;

        /* Send all messages for this Port */
        /* Cant encode */
        if ((MBSM_MAX_RM_BUF_SIZE - u4Offset) < u2Len)
        {
            if (pBuf != NULL)
            {
                /* Send and Allocate */
                if (MbsmRedSendUpdateToRM ((tRmMsg *) pBuf, (UINT2) u4Offset)
                    == MBSM_FAILURE)
                {
                    ProtoEvt.u4Error = RM_SENDTO_FAIL;
                    RmApiHandleProtocolEvent (&ProtoEvt);
                }
            }
            u4Offset = 0;
            /* Buffer Not available */
            if ((pBuf =
                 (VOID *) RM_ALLOC_TX_BUF (MBSM_MAX_RM_BUF_SIZE)) == NULL)
            {
                ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
                RmApiHandleProtocolEvent (&ProtoEvt);
                MBSM_DBG (MBSM_RED_TRC, MBSM_RED,
                          "RM Allocation Failed\n");
                return;
            }
        }
        else
        {
            if (pBuf == NULL)
            {
                u4Offset = 0;
                /* Buffer Not available */
                if ((pBuf =
                     (VOID *) RM_ALLOC_TX_BUF (MBSM_MAX_RM_BUF_SIZE)) == NULL)
                {
                    MBSM_DBG (MBSM_RED_TRC, MBSM_RED,
                              "RM Allocation Failed\n");
                    ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
                    RmApiHandleProtocolEvent (&ProtoEvt);
                    return;
                }

            }
        }

        if (MBSM_SLOT_INDEX_STATUS (u4SlotId) == MBSM_STATUS_ACTIVE)
        {
            MbsmRedFormMessage (u4SlotId, MBSM_RED_ATTACH_MSG, pBuf, &u4Offset);
        }
        else if (MBSM_SLOT_INDEX_STATUS (u4SlotId) == MBSM_STATUS_NP)
        {
            MbsmRedFormMessage (u4SlotId, MBSM_RED_DETACH_MSG, pBuf, &u4Offset);
        }
    }

    /* Send and Allocate */
    if (pBuf != NULL && u4Offset > 0)
    {
        if (MbsmRedSendUpdateToRM ((tRmMsg *) pBuf, (UINT2) u4Offset)
            == MBSM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            RmApiHandleProtocolEvent (&ProtoEvt);
        }
    }

    if (MBSM_RED_BULK_UPD_NEXT_SLOT () <
        (MBSM_MAX_SLOTS + MBSM_SLOT_INDEX_START))
    {
        if (OsixSendEvent (0, (const UINT1 *) MBSM_TASK_NAME,
                           MBSM_RED_BULK_UPD_EVENT) != OSIX_SUCCESS)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            RmApiHandleProtocolEvent (&ProtoEvt);
            MBSM_DBG (MBSM_RED_TRC, MBSM_RED,
                      "Send Event to MBSM Task Failed\n");
        }
    }
    else
    {
        /* MBSM completes it's sync up during standby boot-up and this
         * needs to be informed to RMGR
         */
        RmSetBulkUpdatesStatus (RM_MBSM_APP_ID);

        /* Send the tail msg to indicate the completion of Bulk
         * update process.
         */
        MbsmRedSendBulkUpdateTailMsg ();
    }

}

/*****************************************************************************
 *
 *    Function Name        : MbsmRedPreConfSlotUpdateNotifyToCfa
 *
 *    Description          : This function does the preconfigured slots 
 *                           configuration and it is called from RM after 
 *                           shutdown and start operation.
 *
 *    Input(s)             : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion     : None.
 *
 *    Returns              : None 
 *
 *****************************************************************************/
VOID
MbsmRedPreConfSlotUpdateNotifyToCfa (VOID)
{
    INT4                i4Index = 0;
    for (i4Index = MBSM_SLOT_INDEX_START;
         i4Index < (MBSM_MAX_SLOTS + MBSM_SLOT_INDEX_START); i4Index++)
    {
        CfaMbsmInitDefEtherType (i4Index);
    }
}

/*****************************************************************************
 *
 *    Function Name        : MbsmProcessFailover
 *
 *    Description          : This function will process the failover 
 *                           in MBSM and Update LPort Info as well as 
 *                           generate the remote Detach for remote card
 *
 *    Input(s)             : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion     : None.
 *
 *    Returns              : None 
 *
 *****************************************************************************/
VOID
MbsmProcessFailover (VOID)
{
    INT4                i4SlotId;
    tRmProtoEvt         ProtoEvt;

    i4SlotId = MBSM_SELF_SLOT_ID ();

    /* Update LPort Info */
#ifdef L2RED_WANTED
    MbsmMbsmInitNpInfoOnStandbyToActive (MBSM_SLOT_MAP_ENTRY (i4SlotId));
#endif
    if (MbsmGenerateRemoteSlotDetach () != MBSM_SUCCESS)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_UTIL,
                  "Error in generating detach " "for remote slot\n");
    }

    gGiveGoActiveToOthers = MBSM_TRUE;
    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    ProtoEvt.u4AppId = RM_MBSM_APP_ID;
    ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
    ProtoEvt.u4Error = RM_NONE;
    RmApiHandleProtocolEvent (&ProtoEvt);
}

#ifdef NPAPI_WANTED
/******************************************************************************/
/* Function           : MbsmRedHwAudit                                        */
/*                                                                            */
/* Input(s)           : None                                                  */
/*                                                                            */
/* Output(s)          : None.                                                 */
/*                                                                            */
/* Returns            : MBSM_SUCCESS/MBSM_FAILURE                             */
/*                                                                            */
/* Action             : Routine to create Audit Task.                         */
/*                                                                            */
/******************************************************************************/
INT4
MbsmRedHwAudit (VOID)
{
    UINT4               u4RetVal = MBSM_SUCCESS;
    INT1               *pi1Dummy = NULL;

    if (MBSM_RED_HW_AUDIT_TASK_ID () == MBSM_INIT_VAL)
    {
        if (OsixCreateTask (MBSM_HARDWARE_AUDIT,
                            MBSM_HARDWARE_AUDIT_PRIORITY,
                            OSIX_DEFAULT_STACK_SIZE,
                            (VOID *) MbsmRedStartHwAudit,
                            (INT1 *) pi1Dummy,
                            OSIX_DEFAULT_TASK_MODE,
                            &MBSM_RED_HW_AUDIT_TASK_ID ()) != OSIX_SUCCESS)
        {
            MBSM_DBG (MBSM_RED_TRC, MBSM_RED,
                      "Audit Task creation failed\n");
            u4RetVal = MBSM_FAILURE;
        }
        else
        {
            MBSM_DBG (MBSM_PROTO_TRC, MBSM_RED,
                      "Audit Task creation successful\n");
            u4RetVal = MBSM_SUCCESS;
        }
    }
    return (u4RetVal);
}

/*****************************************************************************/
/* Function Name      : MbsmRedStartHwAudit                                  */
/* Description        : Audit Module to make sure if Slot infm in H/W and    */
/*                      S/W are in sync                                      */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
MbsmRedStartHwAudit (INT1 *pi1Param)
{
    INT4                i4SlotId;
    INT4                i4CardType;
    UINT4               u4RetVal = 0;
    tMbsmHwMsg          HwMsg;

    UNUSED_PARAM (pi1Param);

    for (i4SlotId = MBSM_SLOT_INDEX_START;
         i4SlotId < (MBSM_MAX_SLOTS + MBSM_SLOT_INDEX_START); i4SlotId++)
    {
        MBSM_LOCK ();
        /* Check whether Slot Status is default status */
        if ((MBSM_SLOT_MAP_ENTRY (i4SlotId) == NULL) ||
            (MBSM_SLOT_INDEX_STATUS (i4SlotId) == MBSM_STATUS_INACTIVE))
        {
            MBSM_UNLOCK ();
            continue;
        }

        MEMSET (&HwMsg, 0, sizeof (tMbsmHwMsg));
#ifdef L2RED_WANTED
        /*Get Slot Information from Hardware */
        u4RetVal = MbsmMbsmNpGetSlotInfo (i4SlotId, &HwMsg);
#endif
        if (u4RetVal != FNP_SUCCESS)
        {
            MBSM_DBG (MBSM_TRC_ERR, MBSM_RED,
                      "RED: Get Slot Infm from Hardware Failed!!!\n");
            MBSM_UNLOCK ();
            continue;
        }
        i4CardType = MBSM_SLOT_INDEX_CARDTYPE (i4SlotId);

        /* check whether CardName in Sw and Hw are same */
        /* Since there are chances like Slot Information may be updated 
         * for some other different Card */
        if (STRCMP (MBSM_CARD_TYPE_INFO_CARDNAME (i4CardType),
                    HwMsg.FirstEntry.ai1CardName) != 0)
        {
            if (MBSM_SLOT_INDEX_ISPRECONF (i4SlotId) != MBSM_TRUE)
            {
                /* Different Line card type inserted, hence
                 * interfaces have to be deleted for previous Line card */
                if (MbsmNotifyCfaPreConfigDel (i4SlotId) == MBSM_FAILURE)
                {
                    MBSM_DBG (MBSM_TRC_ERR, MBSM_PROTO,
                              "Error in Audit - Unable to delete PreConfig\n");
                    MBSM_UNLOCK ();
                    continue;
                }
                if (HwMsg.FirstEntry.i4ModType == MBSM_LINE_CARD)
                {
                    MbsmNotifyMbsmFromDriver (&HwMsg, MBSM_MSG_HW_LC_INSERT);
                }
                else if (HwMsg.FirstEntry.i4ModType == MBSM_CONTROL_CARD)
                {
                    MbsmNotifyMbsmFromDriver (&HwMsg, MBSM_MSG_HW_CC_INSERT);
                }
            }
            else
            {
#ifdef SNMP_2_WANTED
                /* Generate Trap */
                MbsmSnmpSendConfigErrTrap (i4SlotId,
                                           MBSM_SLOT_INDEX_MODTYPE (i4SlotId),
                                           MBSM_CARD_TYPE_INFO_CARDNAME
                                           (i4CardType),
                                           MBSM_SLOT_INDEX_STATUS
                                           (i4SlotId),
                                           (INT1 *) MBSM_CONFIG_ERR_TRAP_OID,
                                           MBSM_TRAPS_OID_LEN);
#endif

                /* The Slot is unusable unless the pre-configuration
                   is deleted */
                MBSM_SLOT_INDEX_STATUS (i4SlotId) = MBSM_STATUS_INACTIVE;
            }
        }

        /* Check whether Software and Hardware Slot Status are same */
        else if (MBSM_SLOT_INDEX_STATUS (i4SlotId) != HwMsg.FirstEntry.i1Status)
        {
            switch (HwMsg.FirstEntry.i1Status)
            {
                case MBSM_STATUS_ACTIVE:
                    /*Post Hw Msg to MBSM Queue */
                    if (HwMsg.FirstEntry.i4ModType == MBSM_LINE_CARD)
                    {
                        MbsmNotifyMbsmFromDriver (&HwMsg,
                                                  MBSM_MSG_HW_LC_INSERT);
                    }
                    else if (HwMsg.FirstEntry.i4ModType == MBSM_CONTROL_CARD)
                    {
                        MbsmNotifyMbsmFromDriver (&HwMsg,
                                                  MBSM_MSG_HW_CC_INSERT);
                    }
                    break;

                case MBSM_STATUS_NP:
                    /*Post Hw Msg to MBSM Queue */
                    if (HwMsg.FirstEntry.i4ModType == MBSM_LINE_CARD)
                    {
                        MbsmNotifyMbsmFromDriver (&HwMsg,
                                                  MBSM_MSG_HW_LC_REMOVE);
                    }
                    else if (HwMsg.FirstEntry.i4ModType == MBSM_CONTROL_CARD)
                    {
                        MbsmNotifyMbsmFromDriver (&HwMsg,
                                                  MBSM_MSG_HW_CC_REMOVE);
                    }
                    break;

                case MBSM_STATUS_INACTIVE:
                    /* Initalise Slot and Port Map Table Entry
                     * with default values*/
                    MbsmRedInitSlotAndPortMapEntry (i4SlotId);
                    break;

                case MBSM_STATUS_DOWN:
                    break;

                default:
                    break;
            }
        }
        MBSM_UNLOCK ();
    }
}
#endif

/*****************************************************************************/
/* Function Name      : MbsmRedHandlePeerStateChangeEvent                    */
/*                                                                           */
/* Description        : It Handles the Peer Up event. This event is          */
/*                      triggered when the Stand-by Node comes up.           */
/*                      An Event is post to CFA to update the status of      */
/*                      Stand-by Ports                                       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
MbsmRedHandlePeerStateChangeEvent (UINT1 u1Event)
{
    INT4                i4SlotId = 0;
    INT4                i4Event = 0;
    UINT4               u4Index = 0;
    tMbsmHwMsg          HwMsg;
    MEMSET (&HwMsg, 0, sizeof (tMbsmHwMsg));
    HwMsg.i4NumEntries = 1;
    /* RM Peer Up / Peer Down Message does not have Slot ID */
    /* At present RM supports only 2 Node and Dual Unit Stacking is 
       for 2 nodes */
    /* So the Slot which is not the Self Slot is
       considered as Slot ID of the Peer Node */

    /* CAUTION */
    /* This logic is applicable only for Dual Unit Stcking */

    for (i4SlotId = 0; i4SlotId < MBSM_MAX_SLOTS; i4SlotId++)
    {
        if (i4SlotId != IssGetSwitchid ())
        {
            HwMsg.FirstEntry.i4SlotId = i4SlotId;
            break;
        }
    }
    /* Send the card type as invalid, 
     * Card type will be identified by MBSM from the input card name
     */
    HwMsg.FirstEntry.i4CardType = MBSM_INVALID_CARDTYPE;

    STRCPY (HwMsg.FirstEntry.ai1CardName,
            MBSM_CARD_TYPE_INFO_CARDNAME (MBSM_SLOT_INDEX_CARDTYPE (i4SlotId)));
    HwMsg.FirstEntry.u4NumPorts = MBSM_SLOT_INDEX_NUMPORTS (i4SlotId);
    MBSM_SLOT_INDEX_RM_PEER_UP_MSG_TYPE (i4SlotId) = OSIX_TRUE;
    for (u4Index = MBSM_SLOT_INDEX_STARTINDEX (i4SlotId);
         u4Index <= MBSM_SLOT_INDEX_NUMPORTS (i4SlotId); u4Index++)
    {
        OSIX_BITLIST_SET_BIT (HwMsg.FirstEntry.MbsmPortList,
                              u4Index, sizeof (tMbsmPortList));
    }

    switch (u1Event)
    {
        case RM_STANDBY_UP:

            HwMsg.FirstEntry.i1Status = MBSM_STATUS_ACTIVE;

            if (MBSM_SLOT_INDEX_MODTYPE (i4SlotId) == MBSM_CONTROL_CARD)
            {
                i4Event = MBSM_MSG_HW_CC_INSERT;
            }
            else if (MBSM_SLOT_INDEX_MODTYPE (i4SlotId) == MBSM_LINE_CARD)
            {
                i4Event = MBSM_MSG_HW_LC_INSERT;
            }
            break;

        case RM_STANDBY_DOWN:

            HwMsg.FirstEntry.i1Status = MBSM_STATUS_NP;

            if (MBSM_SLOT_INDEX_MODTYPE (i4SlotId) == MBSM_CONTROL_CARD)
            {
                i4Event = MBSM_MSG_HW_CC_REMOVE;
            }
            else if (MBSM_SLOT_INDEX_MODTYPE (i4SlotId) == MBSM_LINE_CARD)
            {
                i4Event = MBSM_MSG_HW_LC_REMOVE;
            }
            break;

        default:
            break;
    }
    if (MbsmNotifyMbsmFromDriver (&HwMsg, i4Event) == MBSM_FAILURE)
    {
        MBSM_DBG (MBSM_RED_TRC, MBSM_RED, "Card Insert Event ."
                  "not sent.\n");
    }
    return;

}

/*****************************************************************************/
/* Function Name      : MbsmRedHandleBulkUpdateEvent                         */
/*                                                                           */
/* Description        : It Handles the bulk update event. This event is used */
/*                      to start the next sub bulk update. So                */
/*                      MbsmRedHandleBulkRequest is triggered.                */
/*                                                                           */
/* Input(s)           :                                                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
MbsmRedHandleBulkUpdateEvent (VOID)
{
#ifdef L2RED_WANTED
    if (MBSM_RED_NODE_STATUS () == MBSM_RED_ACTIVE)
    {
        MBSM_DBG (MBSM_RED_TRC, MBSM_RED,
                  "RECV_EVENT: MBSM_RED_BULK_UPD_EVENT" " At Active side\r\n");
        MbsmRedHandleBulkRequest ();
    }
    else
    {
        MBSM_DBG (MBSM_RED_TRC, MBSM_RED,
                  "RECV_EVENT: MBSM_RED_BULK_UPD_EVENT" " At Standby side\r\n");
        MBSM_RED_BULK_REQ_RCVD () = MBSM_TRUE;
    }
#endif
}

/*****************************************************************************/
/* Function Name      : MbsmRedSendBulkUpdateTailMsg                         */
/*                                                                           */
/* Description        : This function will send the tail msg to the standy   */
/*                      node, which indicates the completion of Bulk update  */
/*                      process.                                             */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : MBSM_SUCCESS / MBSM_FAILURE.                         */
/*****************************************************************************/
INT4
MbsmRedSendBulkUpdateTailMsg (VOID)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = MBSM_INIT_VAL;
    UINT2               u2BufLen;
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_MBSM_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (MBSM_RED_NODE_STATUS () != MBSM_RED_ACTIVE)
    {
        MBSM_DBG (MBSM_RED_TRC, MBSM_RED, "Node is not active."
                  "Bulk update tail msg not sent.\n");
        return MBSM_SUCCESS;
    }

    /* Form a bulk update tail message.

     *        <-----------1 Byte----------><----2 Byte------>
     *******************************************************
     *        *                           *                *
     * RM Hdr * MBSM_RED_BULK_UPD_TAIL_MSG * Msg Length    *
     * *      *                           *                *
     *******************************************************

     * The RM Hdr shall be included by RM.
     */

    u2BufLen = MBSM_RED_BULK_TAIL_MSG_LEN;

    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufLen)) == NULL)
    {
        MBSM_DBG (MBSM_RED_TRC, MBSM_RED, "Rm alloc failed\n");
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return (MBSM_FAILURE);
    }

    /* Fill the message type. */
    MBSM_RM_PUT_1_BYTE (pMsg, &u4Offset, MBSM_RED_BULK_UPD_TAIL_MSG);

    /* This length field will not be used. It is just to keep the
     * this message structure similar to that of other sync msgs */
    MBSM_RM_PUT_2_BYTE (pMsg, &u4Offset, u2BufLen);
    MBSM_DBG (MBSM_RED_TRC, MBSM_RED,"[ACTIVE]: "
                      "Send the tail message to standby \n\r");

    if (MbsmRedSendUpdateToRM (pMsg, u2BufLen) == MBSM_FAILURE)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
    }

    return MBSM_SUCCESS;
}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/*    Function Name        : MbsmRedInitSlotAndPortMapEntry                  */
/*                                                                           */
/*    Description          : This function performs the initialisation of    */
/*                           the MBSM Slot And Port Info Table.              */
/*                                                                           */
/*    Input(s)             : None.                                           */
/*                                                                           */
/*    Output(s)            : None.                                           */
/*                                                                           */
/*    Global Variables     : gaMbsmPortInfoTable, gaMbsmSlotMapTable         */
/*    Referred                                                               */
/*                                                                           */
/*    Global Variables    : gaMbsmPortInfoTable, gaMbsmSlotMapTable          */
/*    Modified                                                               */
/*                                                                           */
/*    Returns              : None.                                           */
/*****************************************************************************/
VOID
MbsmRedInitSlotAndPortMapEntry (INT4 i4SlotId)
{
    if (MBSM_SLOT_MAP_ENTRY (i4SlotId) != NULL)
    {
        MBSM_SLOT_INDEX_SLOT_ID (i4SlotId) = i4SlotId;
        MBSM_SLOT_INDEX_CARDTYPE (i4SlotId) = MBSM_INVALID_CARDTYPE;
        MBSM_SLOT_INDEX_NUMPORTS (i4SlotId) = MBSM_INVALID_PORTCOUNT;
        MBSM_SLOT_INDEX_STARTINDEX (i4SlotId) = MBSM_INVALID_IFINDEX;
        MBSM_SLOT_INDEX_ISPRECONF (i4SlotId) = MBSM_FALSE;
        MBSM_SLOT_INDEX_STATUS (i4SlotId) = MBSM_STATUS_INACTIVE;
        MBSM_SLOT_INDEX_PREVSTATUS (i4SlotId) = MBSM_STATUS_INACTIVE;
    }

    if (MBSM_PORT_INFO_ENTRY (i4SlotId) != NULL)
    {
        MEMSET (&(MBSM_PORT_INDEX_PORTLIST (i4SlotId)), 0, sizeof (tPortList));
        MBSM_PORT_INDEX_PORTCOUNT (i4SlotId) = MBSM_INVALID_PORTCOUNT;
        MBSM_PORT_INDEX_STARTINDEX (i4SlotId) = MBSM_INVALID_IFINDEX;
    }
}
#endif /* NPAPI_WANTED */
#ifdef STACKING_WANTED
/*****************************************************************************
 * Function Name : MbsmRedHandleLCInsertEventForPizzaStacking 
 * Description   : This Function Handle the Line card insertion event for 
 *                 Pizzabox Stacking Enviornment.And Notify the Higher-layer
 *                 modules if required.
 *                 NOTE: for Pizzabox Stacking we will not receive any
 *                 CC_INSERT event. We use LC_INSERT event alone to indicate 
 *                 Linecard as well as Controlcard insertiion.
 * Input(s)      : i4Slot
 * Output(s)     : None
 * Returns       : MBSM_SUCCESS/MBSM_FAILURE 
 *****************************************************************************/
INT4
MbsmRedHandleLCInsertEventForPizzaStacking (INT4 i4SlotId)
{
    tRmProtoEvt         ProtoEvt;
    UINT4               u4StackingModel = ISS_GET_STACKING_MODEL ();
    if (RmGetForceSwitchOverFlag () == RM_FSW_OCCURED)
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_RED,
                  "Got LC Insert After FORCE SWITCHOVER\r\n");
        /* Update LPort Info */
#ifdef L2RED_WANTED
        MbsmMbsmInitNpInfoOnStandbyToActive (MBSM_SLOT_MAP_ENTRY (i4SlotId));
#endif

        if (MBSM_SELF_SLOT_ID () != i4SlotId)
            /* Remote slot attach received */
        {
            MBSM_DBG_ARG1 (MBSM_PROTO_TRC, MBSM_RED,
                           "Attach received for remote LC (slot %d)\r\n",
                           i4SlotId);
            /* Stop the timer, which is started for remote detach
             * timeout during force switchover
             */
            if (ISS_GET_STACKING_MODEL () == ISS_CTRL_PLANE_STACKING_MODEL)
            {
                /* During Force switchover, RM_STANDBY_UP will be received before
                 * GO_ACTIVE. RM_STANDBY_UP is treated as the peer up indication
                 * the remote slot. (RM_STANDBY_UP triggers MBSM_MSG_HW_LC_INSERT
                 * which in turn triggers MbsmRedHandleLCInsertEventForPizzaStacking
                 *  for remote SLOT)
                 * MBSM_RED_PEER_UP_RECEIVED is used to mark the
                 * Reception of PEER UP. This will be used
                 * This updation will be used in MBSM GO_ACTIVE processing.
                 * if MBSM_RED_PEER_UP_RECEIVED is MBSM_RED_FALSE, then MbsmProcessFailover
                 * will be invoked to trigger Remote slot detach.
                 */
                MBSM_RED_PEER_UP_RECEIVED () = MBSM_RED_TRUE;
            }
            MbsmStopTimer (&gMbsmGlobalInfo.MbsmRecoveryTmr);

            RmSendEventToAppln (RM_MBSM_APP_ID);

            MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
            ProtoEvt.u4AppId = RM_MBSM_APP_ID;
            ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
            ProtoEvt.u4Error = RM_NONE;
            RmApiHandleProtocolEvent (&ProtoEvt);
        }
        else
        {
            MBSM_DBG_ARG1 (MBSM_PROTO_TRC, MBSM_RED,
                           "Self Attach received for LC (slot %d)\r\n",
                           i4SlotId);
        }
        /* Trigger Link Scan - This needs to be done only for LC_INSERT . As in
         * Pizzabox stacking LC is consider as the Control card we need to call
         * this Link Scan Update
         */
        MbsmNotifyLcUpdate (i4SlotId, MBSM_PROTO_COOKIE_LINKSTATUS);
    }
    else                        /* FSW Not occurred */
    {
        if ((RmGetNodePrevState () == RM_INIT) &&
            ((RmGetNodeState () == RM_ACTIVE) ||
             ((RmGetNodeState () == RM_STANDBY) &&
              (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)) ||
             (u4StackingModel == ISS_CTRL_PLANE_STACKING_MODEL)))
        {
            /* During Init -> Standby state transition, only for
             * coldstandby case, Indication to CFA should be given here.*/

            /* Attaching a New CFM in the topology Or Self Attach */

            /* Notify CFA to update Port Creats &
             * Oper Status  */
            MBSM_DBG_ARG1 (MBSM_PROTO_TRC, MBSM_RED,
                           "New LC is attached at slot %d.\r\n", i4SlotId);
            if (MbsmNotifyCfaModuleUpdate (i4SlotId) == MBSM_FAILURE)
            {
                MBSM_DBG (MBSM_TRC_ERR, MBSM_RED,
                          "Return failure from MbsmNotifyCfaModuleUpdate\r\n");
                return MBSM_FAILURE;
            }
        }
        else
        {
            if (MBSM_SELF_SLOT_ID () == i4SlotId)
                /* Local slot attach received */
            {
                MBSM_DBG_ARG1 (MBSM_PROTO_TRC, MBSM_RED,
                               "Received Self-Attach for LC (slot %d)\r\n",
                               i4SlotId);
                if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
                {
                    if (MbsmNotifyCfaModuleUpdate (i4SlotId) == MBSM_FAILURE)
                    {
                        MBSM_DBG (MBSM_TRC_ERR, MBSM_RED,
                                  "Return Failure from MbsmNotifyCfaModuleUpdate\r\n");
                        return MBSM_FAILURE;
                    }
                }
                else
                {
                    if (RmGetNodeState () == RM_STANDBY)
                    {
                        MBSM_DBG (MBSM_PROTO_TRC, MBSM_RED,
                                  "Received attach during failover even"
                                  "before RM becomes active\r\n");

                        gLocalAttachRcvd = MBSM_TRUE;
                    }
                    else        /* FailOver Case */
                    {
                        MBSM_DBG_ARG1 (MBSM_PROTO_TRC, MBSM_RED,
                                       "Got LC Insert (Slot %d) After "
                                       "FAILOVER.\r\n", i4SlotId);
                        MbsmProcessFailover ();
                    }
                }
            }
            else                /* New remote slot attach */
            {
                MBSM_DBG_ARG1 (MBSM_PROTO_TRC, MBSM_RED,
                               "New remote LC attach during FAILOVER "
                               "(slot %d)\r\n", i4SlotId);
                /* Notify CFA to update Port Creats & Oper Status */
                if (MbsmNotifyCfaModuleUpdate (i4SlotId) == MBSM_FAILURE)
                {
                    MBSM_DBG (MBSM_TRC_ERR, MBSM_RED,
                              "Return Failure from MbsmNotifyCfaModuleUpdate\r\n");
                    return MBSM_FAILURE;
                }
            }
        }
    }
#ifdef L2RED_WANTED

    MbsmRedSendSyncMessages (i4SlotId, MBSM_RED_ATTACH_MSG);
#endif
    return MBSM_SUCCESS;
}
#else /* STACKING_WANTED */
/* Used for Chassis Enviornment */
/*****************************************************************************
 * Function Name : MbsmRedHandleLCInsertEvent
 * Description   : This Function Handle the Line card insertion event in the
 *                 Chassis Enviornment.And  Notify the Higher-layer modules
 *                 if required.
 * Input(s)      : i4Slot
 * Output(s)     : None
 * Returns       : MBSM_SUCCESS/MBSM_FAILURE 
 *****************************************************************************/
INT4
MbsmRedHandleLCInsertEvent (INT4 i4SlotId)
{
    /* Get the Current and Previous Node state */
    /* If the current CFM is the Current Master */
    if ((RmGetNodePrevState () == RM_INIT) && (RmGetNodeState () == RM_ACTIVE))
    {
        /* Attach for a Newly inserted LM. Notify CFA to 
         * update Port Create & Oper Status
         */
        MBSM_DBG_ARG1 (MBSM_PROTO_TRC, MBSM_RED,
                       "Attach for a Newly inserted LC on Slot %d."
                       "update CFA to Create ports & update Oper Status\r\n",
                       i4SlotId);
        if (MbsmNotifyCfaModuleUpdate (i4SlotId) == MBSM_FAILURE)
        {
            MBSM_DBG (MBSM_TRC_ERR, MBSM_RED,
                      "MbsmNotifyCfaModuleUpdate Returned FAILURE\r\n");
            return MBSM_FAILURE;
        }
    }
    else if ((RmGetNodePrevState () == RM_STANDBY) &&
             (RmGetNodeState () == RM_ACTIVE))
        /* If the current CFM became Master from Standby */
    {
        /*  Check the Slot status with MBSM database. */
        /* In Sandby to active transition, Previous= Active and current
         * sate = Active
         */
        if ((MBSM_SLOT_INDEX_STATUS (i4SlotId) == MBSM_STATUS_ACTIVE)
            && (MBSM_SLOT_INDEX_PREVSTATUS (i4SlotId) == MBSM_STATUS_ACTIVE))
        {
            /* Got Attach during Standby to Active Transition.
             * Slot is already programmed */
            MBSM_DBG_ARG1 (MBSM_PROTO_TRC, MBSM_RED,
                           "Got attach for the Existing LC (Slot %d) in Standby-to-Active"
                           " side CFM.\r\n", i4SlotId);
#ifdef L2RED_WANTED
            MbsmMbsmInitNpInfoOnStandbyToActive (MBSM_SLOT_MAP_ENTRY
                                                 (i4SlotId));
#endif
            /* Trigger Link Scan */
            MbsmNotifyLcUpdate (i4SlotId, MBSM_PROTO_COOKIE_LINKSTATUS);

        }
        else
        {
            /* Slot Not Present. So it is a New card insertion event.
             * Notify to Higher Layer module. */
            MBSM_DBG_ARG1 (MBSM_PROTO_TRC, MBSM_RED,
                           "Got Attach for a Newly inserted LC (Slot %d) at"
                           "Standby-to-Active side CFMA\r\n", i4SlotId);

            if (MbsmNotifyCfaModuleUpdate (i4SlotId) == MBSM_FAILURE)
            {
                MBSM_DBG (MBSM_TRC_ERR, MBSM_RED,
                          "MbsmNotifyCfaModuleUpdate Returned FAILURE\r\n");
                return MBSM_FAILURE;
            }
        }
    }

    if (RmGetNodeState () == RM_ACTIVE)
    {
#ifdef L2RED_WANTED
        MbsmRedSendSyncMessages (i4SlotId, MBSM_RED_ATTACH_MSG);
#endif
    }
    return MBSM_SUCCESS;
}
#endif /* STACKING_WANTED */

/*****************************************************************************
 * Function Name : MbsmRedHandleCCInsertEvent 
 * Description   : This Function Handle the control card insertion event for 
 *                 Chassis Enviornment. And Notify the Higher-layer modules
 *                 if required.
 * Input(s)      : i4Slot
 * Output(s)     : None
 * Returns       : MBSM_SUCCESS/MBSM_FAILURE 
 *****************************************************************************/
INT4
MbsmRedHandleCCInsertEvent (INT4 i4SlotId)
{
    tRmProtoEvt         ProtoEvt;

    if (RmGetForceSwitchOverFlag () == RM_FSW_OCCURED)
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_RED,
                  "Got CC Insert After FORCE SWITCHOVER\r\n");
        /* Update LPort Info */
#ifdef L2RED_WANTED
        MbsmMbsmInitNpInfoOnStandbyToActive (MBSM_SLOT_MAP_ENTRY (i4SlotId));
#endif

        if (MBSM_SELF_SLOT_ID () != i4SlotId)
            /* Remote slot attach received */
        {
            MBSM_DBG_ARG1 (MBSM_PROTO_TRC, MBSM_RED,
                           "Attach received for remote CFM (slot %d)\r\n",
                           i4SlotId);
            /* Stop the timer, which is started for remote detach
             * timeout during force switchover
             */
            MbsmStopTimer (&gMbsmGlobalInfo.MbsmRecoveryTmr);

            RmSendEventToAppln (RM_MBSM_APP_ID);

            MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
            ProtoEvt.u4AppId = RM_MBSM_APP_ID;
            ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
            ProtoEvt.u4Error = RM_NONE;
            RmApiHandleProtocolEvent (&ProtoEvt);
        }
        else
        {
            MBSM_DBG_ARG1 (MBSM_PROTO_TRC, MBSM_RED,
                           "Self Attach received for CFM (slot %d)\r\n",
                           i4SlotId);
        }
    }
    else                        /* FSW Not occurred */
    {
        if ((RmGetNodePrevState () == RM_INIT) &&
            (RmGetNodeState () == RM_ACTIVE))
        {                        /* Attaching a New CFM in the topology Or Self Attach */

            /* Notify CFA to update Port Creats &
             * Oper Status  */
            MBSM_DBG_ARG1 (MBSM_PROTO_TRC, MBSM_RED,
                           "New Control card is attached at slot %d.\r\n",
                           i4SlotId);
            if (MbsmNotifyCfaModuleUpdate (i4SlotId) == MBSM_FAILURE)
            {
                MBSM_DBG (MBSM_TRC_ERR, MBSM_RED,
                          "Return failure from MbsmNotifyCfaModuleUpdate()\r\n");
                return MBSM_FAILURE;
            }
        }
        else
        {
            if (MBSM_SELF_SLOT_ID () == i4SlotId)
                /* Local slot attach received */
            {
                MBSM_DBG_ARG1 (MBSM_PROTO_TRC, MBSM_RED,
                               "Received Self-Attach for CFM (slot %d)\r\n",
                               i4SlotId);
                if (RmGetNodeState () == RM_STANDBY)
                {
                    MBSM_DBG (MBSM_PROTO_TRC, MBSM_RED,
                              "Received attach during failover even"
                              "before RM becomes active\r\n");

                    gLocalAttachRcvd = MBSM_TRUE;
                }
                else            /* FailOver Case */
                {
                    MBSM_DBG_ARG1 (MBSM_PROTO_TRC, MBSM_RED,
                                   "Got CC Insert (Slot %d) After FAILOVER.\r\n",
                                   i4SlotId);
                    MbsmProcessFailover ();
                }
            }
            else                /* New remote slot attach */
            {
                MBSM_DBG_ARG1 (MBSM_PROTO_TRC, MBSM_RED,
                               "New remote CFM attach during FAILOVER ( slot %d)\r\n",
                               i4SlotId);
                /* Notify CFA to update Port Creats & Oper Status */
                if (MbsmNotifyCfaModuleUpdate (i4SlotId) == MBSM_FAILURE)
                {
                    MBSM_DBG (MBSM_TRC_ERR, MBSM_RED,
                              "Return Failure from MbsmNotifyCfaModuleUpdate\r\n");
                    return MBSM_FAILURE;
                }
            }
        }
    }
#ifdef L2RED_WANTED

    MbsmRedSendSyncMessages (i4SlotId, MBSM_RED_ATTACH_MSG);
#endif
    return MBSM_SUCCESS;
}

/***************** TEST FUNCTIONS ********************/

VOID
DumpSlotMapAndPortInfoTable (VOID)
{
    INT4                i4SlotId;

    for (i4SlotId = MBSM_SLOT_INDEX_START;
         i4SlotId < (MBSM_MAX_SLOTS + MBSM_SLOT_INDEX_START); i4SlotId++)
    {
        if (MBSM_SLOT_MAP_ENTRY (i4SlotId) != NULL)
        {
            PRINTF ("\r\nSlotMap: SlotId:%d ",
                    MBSM_SLOT_INDEX_SLOT_ID (i4SlotId));
            PRINTF ("ModType:%d ", MBSM_SLOT_INDEX_MODTYPE (i4SlotId));
            PRINTF ("CardType:%d ", MBSM_SLOT_INDEX_CARDTYPE (i4SlotId));
            PRINTF ("NumPorts:%d ", MBSM_SLOT_INDEX_NUMPORTS (i4SlotId));
            PRINTF ("StartIdx:%d ", MBSM_SLOT_INDEX_STARTINDEX (i4SlotId));
            PRINTF ("PreConf:%d ", MBSM_SLOT_INDEX_ISPRECONF (i4SlotId));
            PRINTF ("Status:%d ", MBSM_SLOT_INDEX_STATUS (i4SlotId));
            PRINTF ("PrevStatus:%d ", MBSM_SLOT_INDEX_PREVSTATUS (i4SlotId));
        }

        if (MBSM_PORT_INFO_ENTRY (i4SlotId) != NULL)
        {
            PRINTF ("\r\nPortMap: PortCnt:%d ",
                    MBSM_PORT_INDEX_PORTCOUNT (i4SlotId));
            PRINTF ("StartIdx:%d ", MBSM_PORT_INDEX_STARTINDEX (i4SlotId));
        }
    }
    PRINTF ("\n");
}

VOID
DumpCardTypeTable (VOID)
{
    INT4                i4Type;

    for (i4Type = 0; i4Type < MBSM_MAX_LC_TYPES; i4Type++)
    {
        if (MBSM_CARD_TYPE_ENTRY (i4Type) != NULL)
        {
            PRINTF ("\r\nCardTable: Idx:%d ",
                    MBSM_CARD_TYPE_INFO_CARDINDEX (i4Type));
            PRINTF ("NumPorts:%d ", MBSM_CARD_TYPE_INFO_NUMPORTS (i4Type));
            PRINTF ("Name:%s ", MBSM_CARD_TYPE_INFO_CARDNAME (i4Type));
            PRINTF ("RS:%d ", MBSM_CARD_TYPE_INFO_ROWSTATUS (i4Type));
        }
    }
    PRINTF ("\n");
}

/*****************************************************************************/
/* Function Name      : MbsmRedHandleDynSyncAudit                            */
/*                                                                           */
/* Description        : This function Handles the Dynamic Sync-up Audit      */
/*                      event. This event is used to start the audit         */
/*                      between the active and standby units for the         */
/*                      dynamic sync-up happened                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
MbsmRedHandleDynSyncAudit ()
{
    MbsmExecuteCmdAndCalculateChkSum ();
    return;
}

/*****************************************************************************/
/* Function Name      : MbsmExecuteCmdAndCalculateChkSum                     */
/*                                                                           */
/* Description        : This function Handles the execution of show commands */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
MbsmExecuteCmdAndCalculateChkSum (VOID)
{
    /*Execute CLI commands and calculate checksum */
    UINT2               u2AppId = RM_MBSM_APP_ID;
    UINT2               u2ChkSum = 0;

    MBSM_UNLOCK ();
    if (MbsmGetShowCmdOutputAndCalcChkSum (&u2ChkSum) == MBSM_FAILURE)
    {
        MBSM_LOCK ();
        MBSM_DBG (MBSM_TRC_ERR, MBSM_RED,
                  "Checksum of calculation failed for VCM\n");
        return;
    }

    if (MbsmRmEnqChkSumMsgToRm (u2AppId, u2ChkSum) == MBSM_FAILURE)
    {
        MBSM_LOCK ();
        MBSM_DBG (MBSM_TRC_ERR, MBSM_RED, "Sending checkum to RM failed\n");
        return;
    }
    MBSM_LOCK ();
    return;
}

#endif /* _MBSM_RM_C_ */
