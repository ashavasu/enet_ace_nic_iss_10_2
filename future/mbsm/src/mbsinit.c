/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mbsinit.c,v 1.6 2013/03/19 12:22:48 siva Exp $
 *
 * Description:This file contains the routines for static   
 *             initialisations of MBSM Tables.             
 *
 *******************************************************************/
#include "mbsminc.h"

#ifdef NPAPI_WANTED
#include "npapi.h"
#include "mbsnp.h"
#endif /* NPAPI_WANTED */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MbsmInitSlotModuleInfo                           */
/*                                                                          */
/*    Description        : This function initialises the Slot vs. Module    */
/*                         information from a init File(NVRAM).             */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : MBSM_SUCCESS or MBSM_FAILURE                     */
/****************************************************************************/
INT4
MbsmInitSlotModuleInfo (VOID)
{
    INT4                i4RetVal = MBSM_SUCCESS;

    /* Read the information from the init file */
    if (MbsmGetSlotModuleInfoFromFile () == MBSM_FAILURE)
    {
        /* Probably No file exits or a File read error */
        MBSM_DEF_INIT_FLAG () = MBSM_TRUE;
        i4RetVal = MBSM_FAILURE;
    }

    return (i4RetVal);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MbsmInitSysInfo                                  */
/*                                                                          */
/*    Description        : This function initialises the system information */
/*                         from a init File(NVRAM).                         */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : MBSM_SUCCESS or MBSM_FAILURE                     */
/****************************************************************************/
INT4
MbsmInitSysInfo (VOID)
{
    gMbsmSysInfo.u4MbsmMaxSlots = MBSM_MAX_SLOTS;
    gMbsmSysInfo.u4MbsmMaxLcSlots = MBSM_MAX_LC_SLOTS;
    gMbsmSysInfo.u4MbsmMaxPortsPerSlot = MBSM_MAX_PORTS_PER_SLOT;

    if ((gMbsmSysInfo.u4MbsmMaxSlots <= 0) ||
        (gMbsmSysInfo.u4MbsmMaxLcSlots <= 0) ||
        (gMbsmSysInfo.u4MbsmMaxPortsPerSlot <= 0))
    {
        MBSM_DEF_INIT_FLAG () = MBSM_TRUE;
        return (MBSM_FAILURE);
    }

    return (MBSM_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmInitCardTypeTable
 *
 *    Description          : This function performs the initialisation of
 *                           the MBSM Card Type Table.                       
 *
 *    Input(s)             : None. 
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gaMbsmCardTypeTable
 *
 *    Global Variables Modified : gaMbsmCardTypeTable
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion     : None.
 *
 *    Returns              : MBSM_SUCCESS or MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmInitCardTypeTable (VOID)
{
    tMbsmCardInfo      *pTempPtr;
    tMbsmCardInfo       MbsmCardInfo;
    INT4                i4LoopIdx;
    INT4                i4RetVal;

    for (i4LoopIdx = 0; i4LoopIdx < MBSM_MAX_LC_TYPES; i4LoopIdx++)
    {
        MBSM_CARD_TYPE_ENTRY (i4LoopIdx) = NULL;
    }

    pTempPtr = &MbsmCardInfo;

    /* Initialising the Slot Map Table */
    for (i4LoopIdx = 0; i4LoopIdx < MBSM_MAX_LC_TYPES; i4LoopIdx++)
    {
        MEMSET (pTempPtr, 0, sizeof (tMbsmCardInfo));

        i4RetVal = MbsmMbsmNpGetCardTypeTable (i4LoopIdx, pTempPtr);

        if (i4RetVal != FNP_SUCCESS)
        {
            /* not a valid LC type */
            continue;
        }

        if (MbsmAddCardTypeEntry (pTempPtr) != MBSM_SUCCESS)
        {
            MBSM_DBG (MBSM_TRC_ERR, MBSM_INIT,
                      "FAILURE : Unable to Add card Type Entry\r\n");
            return (MBSM_FAILURE);
        }
    }

    return (MBSM_SUCCESS);
}
