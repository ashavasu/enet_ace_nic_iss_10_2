/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmbsmlw.c,v 1.17 2013/03/19 12:22:48 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "mbsminc.h"

#ifdef NPAPI_WANTED
#include "npapi.h"
#include "mbsnp.h"

#endif

extern tMbsmPortSpeedInfo gaMbsmPortSpeedTable[MBSM_MAX_PORT_SPEED_TYPES];

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMbsmMaxNumOfLCSlots
 Input       :  The Indices

                The Object 
                retValMbsmMaxNumOfLCSlots
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMbsmMaxNumOfLCSlots (INT4 *pi4RetValMbsmMaxNumOfLCSlots)
{
    *pi4RetValMbsmMaxNumOfLCSlots = gMbsmSysInfo.u4MbsmMaxLcSlots;
    return ((INT1) SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetMbsmMaxNumOfSlots
 Input       :  The Indices

                The Object 
                retValMbsmMaxNumOfSlots
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMbsmMaxNumOfSlots (INT4 *pi4RetValMbsmMaxNumOfSlots)
{
    *pi4RetValMbsmMaxNumOfSlots = gMbsmSysInfo.u4MbsmMaxSlots;
    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetMbsmMaxNumOfPortsPerLC
 Input       :  The Indices

                The Object 
                retValMbsmMaxNumOfPortsPerLC
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMbsmMaxNumOfPortsPerLC (INT4 *pi4RetValMbsmMaxNumOfPortsPerLC)
{
    *pi4RetValMbsmMaxNumOfPortsPerLC = gMbsmSysInfo.u4MbsmMaxPortsPerSlot;
    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetMbsmLoadSharingFlag
 Input       :  The Indices

                The Object
                retValMbsmLoadSharingFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMbsmLoadSharingFlag (INT4 *pi4RetValMbsmLoadSharingFlag)
{
    *pi4RetValMbsmLoadSharingFlag = gMbsmGlobalInfo.gu1MbsmLoadSharing;
    return ((INT1) SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMbsmMaxNumOfLCSlots
 Input       :  The Indices

                The Object 
                setValMbsmMaxNumOfLCSlots
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMbsmMaxNumOfLCSlots (INT4 i4SetValMbsmMaxNumOfLCSlots)
{
    gMbsmSysInfo.u4MbsmMaxLcSlots = i4SetValMbsmMaxNumOfLCSlots;

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetMbsmMaxNumOfSlots
 Input       :  The Indices

                The Object 
                setValMbsmMaxNumOfSlots
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMbsmMaxNumOfSlots (INT4 i4SetValMbsmMaxNumOfSlots)
{
    gMbsmSysInfo.u4MbsmMaxSlots = i4SetValMbsmMaxNumOfSlots;

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetMbsmMaxNumOfPortsPerLC
 Input       :  The Indices

                The Object 
                setValMbsmMaxNumOfPortsPerLC
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMbsmMaxNumOfPortsPerLC (INT4 i4SetValMbsmMaxNumOfPortsPerLC)
{
    gMbsmSysInfo.u4MbsmMaxPortsPerSlot = i4SetValMbsmMaxNumOfPortsPerLC;

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetMbsmLoadSharingFlag
 Input       :  The Indices

                The Object
                setValMbsmLoadSharingFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMbsmLoadSharingFlag (INT4 i4SetValMbsmLoadSharingFlag)
{
    /* If the target doesn't support Load-Sharing feature, return
     * failure.  */
#ifdef NPAPI_WANTED
    if (MbsmMbsNpSetLoadSharingStatus (i4SetValMbsmLoadSharingFlag) ==
        FNP_FAILURE)
    {
        return ((INT1) SNMP_FAILURE);
    }
#endif

    gMbsmGlobalInfo.gu1MbsmLoadSharing = (UINT1) i4SetValMbsmLoadSharingFlag;

    if (i4SetValMbsmLoadSharingFlag == MBSM_LOAD_SHARING_ENABLE)
    {
        if (MbsmGetCtrlCardCount () < MBSM_MAX_CC_SLOTS)
        {
            /* Programming should not be done when the CFM count < 2 */
            return ((INT1) SNMP_SUCCESS);
        }
    }

    /* Post messages to MBSM to process the load shraing status change. */
    if (MbsmNotifyLoadSharingStatusChgToMbsm (i4SetValMbsmLoadSharingFlag)
        == MBSM_FAILURE)
    {
        return ((INT1) SNMP_FAILURE);
    }

    return ((INT1) SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MbsmMaxNumOfLCSlots
 Input       :  The Indices

                The Object 
                testValMbsmMaxNumOfLCSlots
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MbsmMaxNumOfLCSlots (UINT4 *pu4ErrorCode,
                              INT4 i4TestValMbsmMaxNumOfLCSlots)
{
    if ((i4TestValMbsmMaxNumOfLCSlots <= 0) ||
        (i4TestValMbsmMaxNumOfLCSlots >= (INT4) gMbsmSysInfo.u4MbsmMaxSlots))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return ((INT1) SNMP_FAILURE);
    }
    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2MbsmMaxNumOfSlots
 Input       :  The Indices

                The Object 
                testValMbsmMaxNumOfSlots
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MbsmMaxNumOfSlots (UINT4 *pu4ErrorCode,
                            INT4 i4TestValMbsmMaxNumOfSlots)
{
    if (i4TestValMbsmMaxNumOfSlots <= (INT4) gMbsmSysInfo.u4MbsmMaxLcSlots)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return ((INT1) SNMP_FAILURE);
    }
    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2MbsmMaxNumOfPortsPerLC
 Input       :  The Indices

                The Object 
                testValMbsmMaxNumOfPortsPerLC
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MbsmMaxNumOfPortsPerLC (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValMbsmMaxNumOfPortsPerLC)
{
    if (i4TestValMbsmMaxNumOfPortsPerLC <= 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return ((INT1) SNMP_FAILURE);
    }
    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2MbsmLoadSharingFlag
 Input       :  The Indices

                The Object
                testValMbsmLoadSharingFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MbsmLoadSharingFlag (UINT4 *pu4ErrorCode,
                              INT4 i4TestValMbsmLoadSharingFlag)
{
    if ((i4TestValMbsmLoadSharingFlag != MBSM_LOAD_SHARING_ENABLE) &&
        (i4TestValMbsmLoadSharingFlag != MBSM_LOAD_SHARING_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return ((INT1) SNMP_FAILURE);
    }

    if (MBSM_LOAD_SHARING_FLAG () == i4TestValMbsmLoadSharingFlag)
    {
        /* Test value is same as that of value already set */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return ((INT1) SNMP_FAILURE);
    }

    return ((INT1) SNMP_SUCCESS);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2MbsmMaxNumOfLCSlots
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MbsmMaxNumOfLCSlots (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2MbsmMaxNumOfSlots
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MbsmMaxNumOfSlots (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2MbsmMaxNumOfPortsPerLC
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MbsmMaxNumOfPortsPerLC (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2MbsmLoadSharingFlag
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MbsmLoadSharingFlag (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : MbsmSlotModuleMapTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMbsmSlotModuleMapTable
 Input       :  The Indices
                MbsmSlotId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMbsmSlotModuleMapTable (INT4 i4MbsmSlotId)
{
    if (MbsmValidateIndexSlotModuleMapTable (i4MbsmSlotId) == MBSM_SUCCESS)
    {
        return ((INT1) SNMP_SUCCESS);
    }
    else
    {
        return ((INT1) SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMbsmSlotModuleMapTable
 Input       :  The Indices
                MbsmSlotId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMbsmSlotModuleMapTable (INT4 *pi4MbsmSlotId)
{
    if (MbsmGetFirstIndexSlotModuleMapTable (pi4MbsmSlotId) == MBSM_SUCCESS)
    {
        return ((INT1) SNMP_SUCCESS);
    }
    else
        return ((INT1) SNMP_FAILURE);

}

/****************************************************************************
 Function    :  nmhGetNextIndexMbsmSlotModuleMapTable
 Input       :  The Indices
                MbsmSlotId
                nextMbsmSlotId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMbsmSlotModuleMapTable (INT4 i4MbsmSlotId,
                                       INT4 *pi4NextMbsmSlotId)
{
    if (MbsmGetNextIndexSlotModuleMapTable (i4MbsmSlotId, pi4NextMbsmSlotId)
        == MBSM_SUCCESS)
    {
        return ((INT1) SNMP_SUCCESS);
    }
    else
        return ((INT1) SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMbsmSlotModuleType
 Input       :  The Indices
                MbsmSlotId

                The Object 
                retValMbsmSlotModuleType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMbsmSlotModuleType (INT4 i4MbsmSlotId, INT4 *pi4RetValMbsmSlotModuleType)
{
    *pi4RetValMbsmSlotModuleType = MBSM_SLOT_INDEX_MODTYPE (i4MbsmSlotId);

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetMbsmSlotModuleStatus
 Input       :  The Indices
                MbsmSlotId

                The Object 
                retValMbsmSlotModuleStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMbsmSlotModuleStatus (INT4 i4MbsmSlotId,
                            INT4 *pi4RetValMbsmSlotModuleStatus)
{
    *pi4RetValMbsmSlotModuleStatus = MBSM_SLOT_INDEX_STATUS (i4MbsmSlotId);

    return ((INT1) SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMbsmSlotModuleType
 Input       :  The Indices
                MbsmSlotId

                The Object 
                setValMbsmSlotModuleType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMbsmSlotModuleType (INT4 i4MbsmSlotId, INT4 i4SetValMbsmSlotModuleType)
{
    MBSM_SLOT_INDEX_MODTYPE (i4MbsmSlotId) = i4SetValMbsmSlotModuleType;

    /* Write the updated information to the SlotModule,txt file */
    if (MbsmWriteSlotModuleInfoToFile () != MBSM_SUCCESS)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_PROTO,
                  "Error in MbsmUpdatetables - Unable to Write to File - FAILURE.\n");
    }
    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetMbsmSlotModuleStatus
 Input       :  The Indices
                MbsmSlotId

                The Object 
                setValMbsmSlotModuleStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMbsmSlotModuleStatus (INT4 i4MbsmSlotId,
                            INT4 i4SetValMbsmSlotModuleStatus)
{
    UNUSED_PARAM (i4MbsmSlotId);
    UNUSED_PARAM (i4SetValMbsmSlotModuleStatus);
    return ((INT1) SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MbsmSlotModuleType
 Input       :  The Indices
                MbsmSlotId

                The Object 
                testValMbsmSlotModuleType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MbsmSlotModuleType (UINT4 *pu4ErrorCode, INT4 i4MbsmSlotId,
                             INT4 i4TestValMbsmSlotModuleType)
{
    if (!MBSM_IS_SLOT_ID_VALID (i4MbsmSlotId))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return ((INT1) SNMP_FAILURE);
    }
    else if ((i4TestValMbsmSlotModuleType <= MBSM_INVALID_CARDTYPE) ||
             (i4TestValMbsmSlotModuleType >= MBSM_CARD_NULL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return ((INT1) SNMP_FAILURE);
    }

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2MbsmSlotModuleStatus
 Input       :  The Indices
                MbsmSlotId

                The Object 
                testValMbsmSlotModuleStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MbsmSlotModuleStatus (UINT4 *pu4ErrorCode, INT4 i4MbsmSlotId,
                               INT4 i4TestValMbsmSlotModuleStatus)
{
    if (!MBSM_IS_SLOT_ID_VALID (i4MbsmSlotId))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return ((INT1) SNMP_FAILURE);
    }
    UNUSED_PARAM (i4TestValMbsmSlotModuleStatus);

    return ((INT1) SNMP_SUCCESS);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2MbsmSlotModuleMapTable
 Input       :  The Indices
                MbsmSlotId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MbsmSlotModuleMapTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : MbsmLCTypeTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMbsmLCTypeTable
 Input       :  The Indices
                MbsmLCIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMbsmLCTypeTable (INT4 i4MbsmLCIndex)
{
    if (MbsmValidateIndexCardTypeTable (i4MbsmLCIndex) == MBSM_SUCCESS)
    {
        return ((INT1) SNMP_SUCCESS);
    }
    else
    {
        return ((INT1) SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMbsmLCTypeTable
 Input       :  The Indices
                MbsmLCIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMbsmLCTypeTable (INT4 *pi4MbsmLCIndex)
{
    if (MbsmGetFirstIndexCardTypeTable (pi4MbsmLCIndex) == MBSM_SUCCESS)
    {
        return ((INT1) SNMP_SUCCESS);
    }
    else
        return ((INT1) SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexMbsmLCTypeTable
 Input       :  The Indices
                MbsmLCIndex
                nextMbsmLCIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMbsmLCTypeTable (INT4 i4MbsmLCIndex, INT4 *pi4NextMbsmLCIndex)
{
    if (MbsmGetNextIndexCardTypeTable (i4MbsmLCIndex, pi4NextMbsmLCIndex) ==
        MBSM_SUCCESS)
    {
        return ((INT1) SNMP_SUCCESS);
    }
    else
        return ((INT1) SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMbsmLCName
 Input       :  The Indices
                MbsmLCIndex

                The Object 
                retValMbsmLCName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMbsmLCName (INT4 i4MbsmLCIndex,
                  tSNMP_OCTET_STRING_TYPE * pRetValMbsmLCName)
{
    SNPRINTF ((char *) pRetValMbsmLCName->pu1_OctetList,
              MBSM_MAX_CARD_NAME_LEN + 1, "%s",
              MBSM_CARD_TYPE_INFO_CARDNAME (i4MbsmLCIndex));

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetMbsmLCMaxPorts
 Input       :  The Indices
                MbsmLCIndex

                The Object 
                retValMbsmLCMaxPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMbsmLCMaxPorts (INT4 i4MbsmLCIndex, INT4 *pi4RetValMbsmLCMaxPorts)
{
    *pi4RetValMbsmLCMaxPorts = MBSM_CARD_TYPE_INFO_NUMPORTS (i4MbsmLCIndex);

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetMbsmLCRowStatus
 Input       :  The Indices
                MbsmLCIndex

                The Object 
                retValMbsmLCRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMbsmLCRowStatus (INT4 i4MbsmLCIndex, INT4 *pi4RetValMbsmLCRowStatus)
{
    *pi4RetValMbsmLCRowStatus = MBSM_CARD_TYPE_INFO_ROWSTATUS (i4MbsmLCIndex);

    return ((INT1) SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMbsmLCName
 Input       :  The Indices
                MbsmLCIndex

                The Object 
                setValMbsmLCName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMbsmLCName (INT4 i4MbsmLCIndex,
                  tSNMP_OCTET_STRING_TYPE * pSetValMbsmLCName)
{
    SNPRINTF ((char *) MBSM_CARD_TYPE_INFO_CARDNAME (i4MbsmLCIndex),
              pSetValMbsmLCName->i4_Length + 1, "%s",
              pSetValMbsmLCName->pu1_OctetList);

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetMbsmLCMaxPorts
 Input       :  The Indices
                MbsmLCIndex

                The Object 
                setValMbsmLCMaxPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMbsmLCMaxPorts (INT4 i4MbsmLCIndex, INT4 i4SetValMbsmLCMaxPorts)
{
    MBSM_CARD_TYPE_INFO_NUMPORTS (i4MbsmLCIndex) = i4SetValMbsmLCMaxPorts;

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetMbsmLCRowStatus
 Input       :  The Indices
                MbsmLCIndex

                The Object 
                setValMbsmLCRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMbsmLCRowStatus (INT4 i4MbsmLCIndex, INT4 i4SetValMbsmLCRowStatus)
{
    switch (i4SetValMbsmLCRowStatus)
    {
        case CREATE_AND_WAIT:
        case CREATE_AND_GO:
            if (MbsmCreateCardTypeEntry (i4MbsmLCIndex) == MBSM_FAILURE)
            {
                return ((INT1) SNMP_FAILURE);
            }
            MBSM_CARD_TYPE_INFO_ROWSTATUS (i4MbsmLCIndex) = NOT_READY;
            break;

        case ACTIVE:
            MBSM_CARD_TYPE_INFO_ROWSTATUS (i4MbsmLCIndex) = ACTIVE;
            break;

        case DESTROY:
            if (MbsmDeleteCardTypeEntry (i4MbsmLCIndex) == MBSM_FAILURE)
            {
                return ((INT1) SNMP_SUCCESS);
            }
            /* Entry is deleted, hence don't have to set Rowstatus */
            return ((INT1) SNMP_SUCCESS);

        default:
            return ((INT1) SNMP_FAILURE);

    }

    return ((INT1) SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MbsmLCName
 Input       :  The Indices
                MbsmLCIndex

                The Object 
                testValMbsmLCName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MbsmLCName (UINT4 *pu4ErrorCode, INT4 i4MbsmLCIndex,
                     tSNMP_OCTET_STRING_TYPE * pTestValMbsmLCName)
{
    if ((i4MbsmLCIndex < 0) || (i4MbsmLCIndex >= MBSM_MAX_LC_TYPES))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return ((INT1) SNMP_FAILURE);
    }
    if (pTestValMbsmLCName->i4_Length >= MBSM_MAX_CARD_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return ((INT1) SNMP_FAILURE);
    }

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2MbsmLCMaxPorts
 Input       :  The Indices
                MbsmLCIndex

                The Object 
                testValMbsmLCMaxPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MbsmLCMaxPorts (UINT4 *pu4ErrorCode, INT4 i4MbsmLCIndex,
                         INT4 i4TestValMbsmLCMaxPorts)
{
    if ((i4MbsmLCIndex < 0) || (i4MbsmLCIndex >= MBSM_MAX_LC_TYPES))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return ((INT1) SNMP_FAILURE);
    }
    if ((i4TestValMbsmLCMaxPorts <= 0) ||
        (i4TestValMbsmLCMaxPorts > MBSM_MAX_PORTS_PER_SLOT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return ((INT1) SNMP_FAILURE);
    }

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2MbsmLCRowStatus
 Input       :  The Indices
                MbsmLCIndex

                The Object 
                testValMbsmLCRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MbsmLCRowStatus (UINT4 *pu4ErrorCode, INT4 i4MbsmLCIndex,
                          INT4 i4TestValMbsmLCRowStatus)
{
    if ((i4MbsmLCIndex < 0) || (i4MbsmLCIndex >= MBSM_MAX_LC_TYPES))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return ((INT1) SNMP_FAILURE);
    }

    switch (i4TestValMbsmLCRowStatus)
    {
        case CREATE_AND_WAIT:
        case CREATE_AND_GO:
            if (MBSM_CARD_TYPE_ENTRY (i4MbsmLCIndex))
            {
                /* Entry already exists */
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return ((INT1) SNMP_FAILURE);
            }
            break;

        case ACTIVE:
            if (MBSM_CARD_TYPE_ENTRY (i4MbsmLCIndex) == NULL)
            {
                /* Entry does not exist */
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return ((INT1) SNMP_FAILURE);
            }
            if (MBSM_CARD_TYPE_INFO_CARDNAME (i4MbsmLCIndex) == NULL)
            {
                /* Entry not complete */
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return ((INT1) SNMP_FAILURE);
            }
            if (MBSM_CARD_TYPE_INFO_NUMPORTS (i4MbsmLCIndex)
                == MBSM_INVALID_PORTCOUNT)
            {
                /* Entry not complete */
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return ((INT1) SNMP_FAILURE);
            }

            break;

        case DESTROY:
            if (!MBSM_CARD_TYPE_ENTRY (i4MbsmLCIndex))
            {
                /* Entry does not exist */
                return ((INT1) SNMP_SUCCESS);
            }
            break;

        default:
            return ((INT1) SNMP_FAILURE);

    }

    return ((INT1) SNMP_SUCCESS);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2MbsmLCTypeTable
 Input       :  The Indices
                MbsmLCIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MbsmLCTypeTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : MbsmLCPortInfoTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMbsmLCPortInfoTable
 Input       :  The Indices
                MbsmLCIndex
                MbsmLCPortIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMbsmLCPortInfoTable (INT4 i4MbsmLCIndex,
                                             INT4 i4MbsmLCPortIndex)
{
    if (MbsmValidateIndexPortInfoTable (i4MbsmLCIndex, i4MbsmLCPortIndex) ==
        MBSM_SUCCESS)
    {
        return ((INT1) SNMP_SUCCESS);
    }
    else
    {
        return ((INT1) SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMbsmLCPortInfoTable
 Input       :  The Indices
                MbsmLCIndex
                MbsmLCPortIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMbsmLCPortInfoTable (INT4 *pi4MbsmLCIndex,
                                     INT4 *pi4MbsmLCPortIndex)
{
    if (MbsmGetFirstIndexPortInfoTable (pi4MbsmLCIndex, pi4MbsmLCPortIndex) ==
        MBSM_SUCCESS)
    {
        return ((INT1) SNMP_SUCCESS);
    }
    else
    {
        return ((INT1) SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetNextIndexMbsmLCPortInfoTable
 Input       :  The Indices
                MbsmLCIndex
                nextMbsmLCIndex
                MbsmLCPortIndex
                nextMbsmLCPortIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMbsmLCPortInfoTable (INT4 i4MbsmLCIndex,
                                    INT4 *pi4NextMbsmLCIndex,
                                    INT4 i4MbsmLCPortIndex,
                                    INT4 *pi4NextMbsmLCPortIndex)
{
    if (MbsmGetNextIndexPortInfoTable (i4MbsmLCIndex, i4MbsmLCPortIndex,
                                       pi4NextMbsmLCIndex,
                                       pi4NextMbsmLCPortIndex) == MBSM_SUCCESS)
    {
        return ((INT1) SNMP_SUCCESS);
    }
    else
    {
        return ((INT1) SNMP_FAILURE);
    }
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMbsmLCPortIfType
 Input       :  The Indices
                MbsmLCIndex
                MbsmLCPortIndex

                The Object 
                retValMbsmLCPortIfType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMbsmLCPortIfType (INT4 i4MbsmLCIndex, INT4 i4MbsmLCPortIndex,
                        INT4 *pi4RetValMbsmLCPortIfType)
{
    if (MbsmGetPortIfTypeFromPortMap
        (&MBSM_CARD_TYPE_INFO_PORTMAP (i4MbsmLCIndex),
         (UINT4) i4MbsmLCPortIndex,
         (UINT4 *) pi4RetValMbsmLCPortIfType) == MBSM_FAILURE)
    {
        *pi4RetValMbsmLCPortIfType = 0;
    }

    return ((INT1) SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetMbsmLCPortSpeed
 Input       :  The Indices
                MbsmLCIndex
                MbsmLCPortIndex

                The Object 
                retValMbsmLCPortSpeed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMbsmLCPortSpeed (INT4 i4MbsmLCIndex, INT4 i4MbsmLCPortIndex,
                       UINT4 *pu4RetValMbsmLCPortSpeed)
{
    tMbsmPortSpeed      PortSpeed;

    if (MbsmGetPortSpeedFromPortMap
        (&MBSM_CARD_TYPE_INFO_PORTMAP (i4MbsmLCIndex), i4MbsmLCPortIndex,
         &PortSpeed) == MBSM_FAILURE)
    {
        *pu4RetValMbsmLCPortSpeed = 0;
    }
    else
    {

        *pu4RetValMbsmLCPortSpeed = PortSpeed.u4Speed;
    }
    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetMbsmLCPortHighSpeed
 Input       :  The Indices
                MbsmLCIndex
                MbsmLCPortIndex

                The Object 
                retValMbsmLCPortHighSpeed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMbsmLCPortHighSpeed (INT4 i4MbsmLCIndex, INT4 i4MbsmLCPortIndex,
                           UINT4 *pu4RetValMbsmLCPortHighSpeed)
{
    tMbsmPortSpeed      PortSpeed;

    if (MbsmGetPortSpeedFromPortMap
        (&MBSM_CARD_TYPE_INFO_PORTMAP (i4MbsmLCIndex), i4MbsmLCPortIndex,
         &PortSpeed) == MBSM_FAILURE)
    {
        *pu4RetValMbsmLCPortHighSpeed = 0;
    }
    else
    {

        *pu4RetValMbsmLCPortHighSpeed = PortSpeed.u4HighSpeed;
    }
    return ((INT1) SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMbsmLCPortIfType
 Input       :  The Indices
                MbsmLCIndex
                MbsmLCPortIndex

                The Object 
                setValMbsmLCPortIfType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMbsmLCPortIfType (INT4 i4MbsmLCIndex, INT4 i4MbsmLCPortIndex,
                        INT4 i4SetValMbsmLCPortIfType)
{
    if (MbsmSetPortIfTypeFromPortMap
        (&MBSM_CARD_TYPE_INFO_PORTMAP (i4MbsmLCIndex), i4MbsmLCPortIndex,
         i4SetValMbsmLCPortIfType) == MBSM_FAILURE)
    {
        return ((INT1) SNMP_FAILURE);
    }

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetMbsmLCPortSpeed
 Input       :  The Indices
                MbsmLCIndex
                MbsmLCPortIndex

                The Object 
                setValMbsmLCPortSpeed
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMbsmLCPortSpeed (INT4 i4MbsmLCIndex, INT4 i4MbsmLCPortIndex,
                       UINT4 u4SetValMbsmLCPortSpeed)
{
    tMbsmPortSpeed      PortSpeed;

    PortSpeed.u4Speed = u4SetValMbsmLCPortSpeed;
    PortSpeed.u4HighSpeed = 0;

    if (u4SetValMbsmLCPortSpeed == MBSM_LOW_SPEED_MAX)
    {
        /* We form the portlist based on the HighSpeed value */
        return ((INT1) SNMP_SUCCESS);
    }
    else if (MbsmSetPortSpeedFromPortMap
             (&MBSM_CARD_TYPE_INFO_PORTMAP (i4MbsmLCIndex), i4MbsmLCPortIndex,
              PortSpeed) == MBSM_FAILURE)
    {
        return ((INT1) SNMP_FAILURE);
    }

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetMbsmLCPortHighSpeed
 Input       :  The Indices
                MbsmLCIndex
                MbsmLCPortIndex

                The Object 
                setValMbsmLCPortHighSpeed
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMbsmLCPortHighSpeed (INT4 i4MbsmLCIndex, INT4 i4MbsmLCPortIndex,
                           UINT4 u4SetValMbsmLCPortHighSpeed)
{
    tMbsmPortSpeed      PortSpeed;

    PortSpeed.u4HighSpeed = u4SetValMbsmLCPortHighSpeed;
    PortSpeed.u4Speed = MBSM_LOW_SPEED_MAX;

    if (MbsmSetPortSpeedFromPortMap
        (&MBSM_CARD_TYPE_INFO_PORTMAP (i4MbsmLCIndex), i4MbsmLCPortIndex,
         PortSpeed) == MBSM_FAILURE)
    {
        return ((INT1) SNMP_FAILURE);
    }

    return ((INT1) SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MbsmLCPortIfType
 Input       :  The Indices
                MbsmLCIndex
                MbsmLCPortIndex

                The Object 
                testValMbsmLCPortIfType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MbsmLCPortIfType (UINT4 *pu4ErrorCode, INT4 i4MbsmLCIndex,
                           INT4 i4MbsmLCPortIndex,
                           INT4 i4TestValMbsmLCPortIfType)
{
    if (MbsmValidateIndexPortInfoTable (i4MbsmLCIndex, i4MbsmLCPortIndex) !=
        MBSM_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return ((INT1) SNMP_FAILURE);
    }

    if ((i4TestValMbsmLCPortIfType <= CFA_NONE) ||
        (i4TestValMbsmLCPortIfType >= CFA_INVALID_TYPE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return ((INT1) SNMP_FAILURE);
    }

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2MbsmLCPortSpeed
 Input       :  The Indices
                MbsmLCIndex
                MbsmLCPortIndex

                The Object 
                testValMbsmLCPortSpeed
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MbsmLCPortSpeed (UINT4 *pu4ErrorCode, INT4 i4MbsmLCIndex,
                          INT4 i4MbsmLCPortIndex,
                          UINT4 u4TestValMbsmLCPortSpeed)
{
    INT4                i4Index;

    if (MbsmValidateIndexPortInfoTable (i4MbsmLCIndex, i4MbsmLCPortIndex) !=
        MBSM_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return ((INT1) SNMP_FAILURE);
    }

    for (i4Index = 0; (i4Index < MBSM_MAX_PORT_SPEED_TYPES) &&
         (gaMbsmPortSpeedTable[i4Index].u4SpeedType
          != MBSM_SPEED_INVALID); i4Index++)
    {
        if (gaMbsmPortSpeedTable[i4Index].PortSpeed.u4Speed ==
            u4TestValMbsmLCPortSpeed)
        {
            return ((INT1) SNMP_SUCCESS);
        }
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return ((INT1) SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhTestv2MbsmLCPortHighSpeed
 Input       :  The Indices
                MbsmLCIndex
                MbsmLCPortIndex

                The Object 
                testValMbsmLCPortHighSpeed
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MbsmLCPortHighSpeed (UINT4 *pu4ErrorCode, INT4 i4MbsmLCIndex,
                              INT4 i4MbsmLCPortIndex,
                              UINT4 u4TestValMbsmLCPortHighSpeed)
{
    INT4                i4Index;

    if (MbsmValidateIndexPortInfoTable (i4MbsmLCIndex, i4MbsmLCPortIndex) !=
        MBSM_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return ((INT1) SNMP_FAILURE);
    }

    for (i4Index = 0; (i4Index < MBSM_MAX_PORT_SPEED_TYPES) &&
         (gaMbsmPortSpeedTable[i4Index].u4SpeedType
          != MBSM_SPEED_INVALID); i4Index++)
    {
        if (gaMbsmPortSpeedTable[i4Index].PortSpeed.u4HighSpeed ==
            u4TestValMbsmLCPortHighSpeed)
        {
            return ((INT1) SNMP_SUCCESS);
        }
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return ((INT1) SNMP_FAILURE);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2MbsmLCPortInfoTable
 Input       :  The Indices
                MbsmLCIndex
                MbsmLCPortIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MbsmLCPortInfoTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : MbsmLCConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMbsmLCConfigTable
 Input       :  The Indices
                MbsmLCConfigSlotId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMbsmLCConfigTable (INT4 i4MbsmLCConfigSlotId)
{
    if (MbsmValidateIndexLCConfigTable (i4MbsmLCConfigSlotId) == MBSM_SUCCESS)
    {
        return ((INT1) SNMP_SUCCESS);
    }
    else
    {
        return ((INT1) SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMbsmLCConfigTable
 Input       :  The Indices
                MbsmLCConfigSlotId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMbsmLCConfigTable (INT4 *pi4MbsmLCConfigSlotId)
{
    if (MbsmGetFirstIndexLCConfigTable (pi4MbsmLCConfigSlotId) == MBSM_SUCCESS)
    {
        return ((INT1) SNMP_SUCCESS);
    }
    else
    {
        return ((INT1) SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetNextIndexMbsmLCConfigTable
 Input       :  The Indices
                MbsmLCConfigSlotId
                nextMbsmLCConfigSlotId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMbsmLCConfigTable (INT4 i4MbsmLCConfigSlotId,
                                  INT4 *pi4NextMbsmLCConfigSlotId)
{
    if (MbsmGetNextIndexLCConfigTable (i4MbsmLCConfigSlotId,
                                       pi4NextMbsmLCConfigSlotId) ==
        MBSM_SUCCESS)
    {
        return ((INT1) SNMP_SUCCESS);
    }
    else
    {
        return ((INT1) SNMP_FAILURE);
    }
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMbsmLCConfigCardName
 Input       :  The Indices
                MbsmLCConfigSlotId

                The Object 
                retValMbsmLCConfigCardName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMbsmLCConfigCardName (INT4 i4MbsmLCConfigSlotId,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValMbsmLCConfigCardName)
{
    if (MbsmGetSlotCardName (i4MbsmLCConfigSlotId,
                             (INT1 *) pRetValMbsmLCConfigCardName->
                             pu1_OctetList) == MBSM_FAILURE)
    {
        return ((INT1) SNMP_FAILURE);
    }
    pRetValMbsmLCConfigCardName->i4_Length =
        STRLEN (pRetValMbsmLCConfigCardName->pu1_OctetList);

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetMbsmLCConfigStatus
 Input       :  The Indices
                MbsmLCConfigSlotId

                The Object 
                retValMbsmLCConfigStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMbsmLCConfigStatus (INT4 i4MbsmLCConfigSlotId,
                          INT4 *pi4RetValMbsmLCConfigStatus)
{
    if (!MBSM_IS_SLOT_ID_VALID (i4MbsmLCConfigSlotId))
    {
        return ((INT1) SNMP_FAILURE);
    }
    *pi4RetValMbsmLCConfigStatus =
        MBSM_SLOT_INDEX_ISPRECONF (i4MbsmLCConfigSlotId);

    return ((INT1) SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMbsmLCConfigCardName
 Input       :  The Indices
                MbsmLCConfigSlotId

                The Object 
                setValMbsmLCConfigCardName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMbsmLCConfigCardName (INT4 i4MbsmLCConfigSlotId,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValMbsmLCConfigCardName)
{
    INT4                i4StartIfIndex;
    INT4                i4CardIndex;
    INT4                i4Index;
    INT4                i4PortCount;

    if (MbsmGetCardIndexFromCardName ((INT1 *) pSetValMbsmLCConfigCardName->
                                      pu1_OctetList,
                                      &i4CardIndex) == MBSM_FAILURE)
    {
        return ((INT1) SNMP_FAILURE);
    }

    MBSM_SLOT_INDEX_SLOT_ID (i4MbsmLCConfigSlotId) = i4MbsmLCConfigSlotId;

    MBSM_SLOT_INDEX_CARDTYPE (i4MbsmLCConfigSlotId) = i4CardIndex;

    i4PortCount = MBSM_CARD_TYPE_INFO_NUMPORTS (i4CardIndex);

    MBSM_SLOT_INDEX_NUMPORTS (i4MbsmLCConfigSlotId) = i4PortCount;

    MBSM_SLOT_INDEX_ISPRECONF (i4MbsmLCConfigSlotId) = MBSM_TRUE;

    MBSM_SLOT_INDEX_PREVSTATUS (i4MbsmLCConfigSlotId) =
        MBSM_SLOT_INDEX_STATUS (i4MbsmLCConfigSlotId);

    MBSM_SLOT_INDEX_STATUS (i4MbsmLCConfigSlotId) = MBSM_STATUS_NP;

    MbsmGetStartIfIndex (i4MbsmLCConfigSlotId, (UINT4 *) &i4StartIfIndex);
    MBSM_SLOT_INDEX_STARTINDEX (i4MbsmLCConfigSlotId) = i4StartIfIndex;

    MBSM_SLOT_INDEX_MODTYPE (i4MbsmLCConfigSlotId) = MBSM_LINE_CARD;

    MBSM_PORT_INDEX_STARTINDEX (i4MbsmLCConfigSlotId)
        = MBSM_SLOT_INDEX_STARTINDEX (i4MbsmLCConfigSlotId);

    MBSM_PORT_INDEX_PORTCOUNT (i4MbsmLCConfigSlotId)
        = MBSM_SLOT_INDEX_NUMPORTS (i4MbsmLCConfigSlotId);

    for (i4Index = 0; i4Index < i4PortCount; i4Index++)
    {
        /* Assumption is that ports are continous and there will gaps
         * between ports */
        MBSM_SET_MEMBER_PORTLIST (MBSM_PORT_INDEX_PORTLIST
                                  (i4MbsmLCConfigSlotId),
                                  (i4StartIfIndex + i4Index));
    }

    /* Write the updated information to the SlotModule,txt file */
    if (MbsmWriteSlotModuleInfoToFile () != MBSM_SUCCESS)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_PROTO,
                  "Error in MbsmUpdatetables - Unable to Write to File - FAILURE.\n");
    }

    if (MbsmNotifyCfaModuleUpdate (i4MbsmLCConfigSlotId) == MBSM_FAILURE)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_PROTO,
                  "Unable to notify CFA for Interface create\r\n");
        return ((INT1) SNMP_FAILURE);
    }

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetMbsmLCConfigStatus
 Input       :  The Indices
                MbsmLCConfigSlotId

                The Object 
                setValMbsmLCConfigStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMbsmLCConfigStatus (INT4 i4MbsmLCConfigSlotId,
                          INT4 i4SetValMbsmLCConfigStatus)
{
    MBSM_SLOT_INDEX_ISPRECONF (i4MbsmLCConfigSlotId) =
        (INT1) i4SetValMbsmLCConfigStatus;
    /* Write the updated information to the SlotModule,txt file */
    if (MbsmWriteSlotModuleInfoToFile () != MBSM_SUCCESS)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_PROTO,
                  "Error in MbsmUpdatetables - Unable to Write to File - FAILURE.\n");
    }
    return ((INT1) SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MbsmLCConfigCardName
 Input       :  The Indices
                MbsmLCConfigSlotId

                The Object 
                testValMbsmLCConfigCardName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MbsmLCConfigCardName (UINT4 *pu4ErrorCode, INT4 i4MbsmLCConfigSlotId,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValMbsmLCConfigCardName)
{
    INT4                i4CardIndex;

    if (!MBSM_IS_SLOT_ID_VALID (i4MbsmLCConfigSlotId))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return ((INT1) SNMP_FAILURE);
    }

    /*Check whether there is no entry for that particular slot */
    if (MBSM_SLOT_INDEX_CARDTYPE (i4MbsmLCConfigSlotId) !=
        MBSM_INVALID_CARDTYPE)
    {
        /* To Delete the entry for the slot before adding a new entry */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return ((INT1) SNMP_FAILURE);
    }

    if (!MBSM_IS_LINE_CARD (MBSM_SLOT_INDEX_MODTYPE (i4MbsmLCConfigSlotId)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return ((INT1) SNMP_FAILURE);
    }

    if (MbsmGetCardIndexFromCardName ((INT1 *) pTestValMbsmLCConfigCardName->
                                      pu1_OctetList,
                                      &i4CardIndex) != MBSM_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return ((INT1) SNMP_FAILURE);
    }

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2MbsmLCConfigStatus
 Input       :  The Indices
                MbsmLCConfigSlotId

                The Object 
                testValMbsmLCConfigStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MbsmLCConfigStatus (UINT4 *pu4ErrorCode, INT4 i4MbsmLCConfigSlotId,
                             INT4 i4TestValMbsmLCConfigStatus)
{
    if (!MBSM_IS_SLOT_ID_VALID (i4MbsmLCConfigSlotId))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return ((INT1) SNMP_FAILURE);
    }

    if ((i4TestValMbsmLCConfigStatus != MBSM_TRUE) &&
        (i4TestValMbsmLCConfigStatus != MBSM_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return ((INT1) SNMP_FAILURE);
    }
    return ((INT1) SNMP_SUCCESS);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2MbsmLCConfigTable
 Input       :  The Indices
                MbsmLCConfigSlotId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MbsmLCConfigTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
