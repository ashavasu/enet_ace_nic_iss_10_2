/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mbsmcli.c,v 1.29 2015/04/04 10:33:21 siva Exp $
 *
 * Description:This file contains the routines for CLI commands
 *             of MBSM module.
 *
 *******************************************************************/
#include "mbsminc.h"
#include "mbsmcli.h"
#include "fsmbsmwr.h"
#include "fsmbsmcli.h"
#ifdef NPSIM_WANTED
#include "npsimiss.h"
#endif

#define MBSM_CLI_MAX_ARGS        4    /* Max number of tokens in MBSM CLI 
                                       commands */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : cli_process_mbsm_cmd                             */
/*                                                                          */
/*    Description        : This function is the entry function for the      */
/*                         FS CLI module inside the MBSM module.            */
/*                                                                          */
/*    Input(s)           : Command argument list                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
INT4
cli_process_mbsm_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[MBSM_CLI_MAX_ARGS];
    INT1                argno = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
    INT4                i4ModuleType;
    INT4                i4Arg = 0;
    INT4                i4SlotId = 0;

    va_start (ap, u4Command);

    /* Walk through the rest of the arguments and store in args array. 
     * In the variable argument list, there is a possibility of NULL existing
     * between valid arguments, hence we parse the arg list for a pre-defined
     * number of iterations.
     */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT1 *);
        if (argno == MBSM_CLI_MAX_ARGS)
            break;
    }
    va_end (ap);
    MBSM_LOCK ();
    switch (u4Command)
    {
        case MBSM_CLI_SHOW_HW:
            i4RetStatus = MbsmCliShowHardware (CliHandle);
            break;

        case MBSM_CLI_SLOT_ENTRY_ADD:
            MEMCPY (&i4Arg, args[0], sizeof (INT4));
            i4RetStatus =
                MbsmCliPreConfigAdd (CliHandle, i4Arg, (INT1 *) args[1]);
            break;

        case MBSM_CLI_SLOT_ENTRY_DEL:
            MEMCPY (&i4Arg, args[0], sizeof (INT4));
            i4RetStatus = MbsmCliPreConfigDel (CliHandle, i4Arg);
            break;

        case MBSM_CLI_SLOT_MODTYPE_ADD:
            if (!STRCASECMP (args[1], "LC"))
            {
                i4ModuleType = MBSM_LINE_CARD;
            }
            else if (!STRCASECMP (args[1], "CC"))
            {
                i4ModuleType = MBSM_CONTROL_CARD;
            }
            else
            {
                i4ModuleType = MBSM_INVALID_CARDTYPE;
            }
            MEMCPY (&i4Arg, args[0], sizeof (INT4));
            MbsmCliSlotModTypeAdd (CliHandle, i4Arg, i4ModuleType);

            if ((i4ModuleType == MBSM_LINE_CARD) && (args[2] != NULL) &&
                (i4RetStatus == CLI_SUCCESS))
            {
                MEMCPY (&i4Arg, args[0], sizeof (INT4));
                MbsmCliUpdateLCModType (CliHandle, i4Arg, (INT1 *) args[2],
                                        CLI_PTR_TO_I4 (args[3]));
            }
            break;

        case MBSM_CLI_LOAD_SHARING_ENABLE:
            i4RetStatus = MbsmCliLoadSharingEnable (CliHandle);
            break;

        case MBSM_CLI_LOAD_SHARING_DISABLE:
            i4RetStatus = MbsmCliLoadSharingDisable (CliHandle);
            break;

        case MBSM_CLI_ATTACH_CARD:

            if (args[0] != NULL)
            {
                i4SlotId = *((INT4 *) args[0]);
            }

            i4RetStatus = MbsmCliInsertLC (CliHandle, i4SlotId, args[1]);
            break;

        case MBSM_CLI_DETTACH_CARD:

            if (args[0] != NULL)
            {
                i4SlotId = *((INT4 *) args[0]);
            }
            i4RetStatus = MbsmCliRemoveLC (CliHandle, i4SlotId);
            break;

        default:
            CliPrintf (CliHandle, "\r\nUnknown command\r\n");
            break;
    }
    CLI_SET_CMD_STATUS (i4RetStatus);

    /* No failures will be returned from command Action routines as of now.
     * If in future, error code are handled in Lowlevel routines then 
     * check for i4RetStatus & ErrorCode and display the proper error string 
     */
    MBSM_UNLOCK ();
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MbsmCliPreConfigAdd                                */
/*                                                                           */
/*     DESCRIPTION      : This function associates the given card name with  */
/*                        the specified Slot Id.                             */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        i4SlotId - slot id                                 */
/*                        pi1CardName - name of the card                     */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
MbsmCliPreConfigAdd (tCliHandle CliHandle, INT4 i4SlotId, INT1 *pi1CardName)
{
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE *pName;

    pName = allocmem_octetstring (STRLEN (pi1CardName) + 1);

    if (pName == NULL)
    {
        return CLI_FAILURE;
    }

    STRCPY (pName->pu1_OctetList, pi1CardName);

    if (nmhTestv2MbsmLCConfigCardName (&u4ErrorCode,
                                       i4SlotId, pName) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Invalid Slot Id/Card Name\r\n");

        free_octetstring (pName);
        return CLI_FAILURE;
    }

    if (nmhSetMbsmLCConfigCardName (i4SlotId, pName) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Unable to update Slot Entry\r\n");
        free_octetstring (pName);
        return CLI_FAILURE;
    }

    free_octetstring (pName);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MbsmCliPreConfigDel                                */
/*                                                                           */
/*     DESCRIPTION      : This function dissociates the given card name with */
/*                        the specified Slot Id.                             */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        i4SlotId  - slot id                                */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
MbsmCliPreConfigDel (tCliHandle CliHandle, INT4 i4SlotId)
{
    INT4                i4Status = MBSM_STATUS_INACTIVE;

    /* There is no specifi NMH routines to do this, hence we access
     * the protocol data structures in this case */
    if (!MBSM_IS_SLOT_ID_VALID (i4SlotId))
    {
        CliPrintf (CliHandle, "Invalid Slot Id\r\n");
        return CLI_FAILURE;
    }

    if (nmhGetMbsmSlotModuleStatus (i4SlotId, &i4Status) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Not able to get the Slot Status\r\n");
        return CLI_FAILURE;
    }

    if (i4Status == MBSM_STATUS_ACTIVE)
    {
        CliPrintf (CliHandle, "Cannot remove an Attached Slot. Remove "
                   "the h/w from the Slot first\r\n");
        return CLI_FAILURE;
    }

    MBSM_SLOT_INDEX_CARDTYPE (i4SlotId) = MBSM_INVALID_CARDTYPE;

    MBSM_SLOT_INDEX_NUMPORTS (i4SlotId) = MBSM_INVALID_PORTCOUNT;

    MBSM_SLOT_INDEX_PREVSTATUS (i4SlotId) = MBSM_SLOT_INDEX_STATUS (i4SlotId);

    MBSM_SLOT_INDEX_STATUS (i4SlotId) = MBSM_STATUS_INACTIVE;

    /* Write the updated information to the SlotModule,txt file */
    if (MbsmWriteSlotModuleInfoToFile () != MBSM_SUCCESS)
    {
        CliPrintf (CliHandle, "Unable to Write to File - FAILURE.\r\n");
        /* Don't have to return here, we still can proceed */
    }

    if (MbsmNotifyCfaPreConfigDel (i4SlotId) == MBSM_FAILURE)
    {
        CliPrintf (CliHandle, "Unable to notify CFA for Interface delete\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MbsmCliSlotModTypeAdd                              */
/*                                                                           */
/*     DESCRIPTION      : This function updates the Slot entry with the card */
/*                        module type on that slot.                          */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        i4SlotId - slot id                                 */
/*                        i4ModuleType - module type                         */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
MbsmCliSlotModTypeAdd (tCliHandle CliHandle, INT4 i4SlotId, INT4 i4ModuleType)
{
    INT4                i4StartIndex;
    UINT4               u4ErrorCode;

    if (!MBSM_IS_SLOT_ID_VALID (i4SlotId))
    {
        CliPrintf (CliHandle, "Invalid Slot Id - Cannot add entry\r\n");
        return CLI_FAILURE;
    }

    MBSM_SLOT_INDEX_SLOT_ID (i4SlotId) = i4SlotId;

    if (nmhTestv2MbsmSlotModuleType (&u4ErrorCode, i4SlotId, i4ModuleType)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Invalid Module Type - Cannot add entry\r\n");
        return CLI_FAILURE;
    }

    nmhSetMbsmSlotModuleType (i4SlotId, i4ModuleType);

    if (MbsmGetStartIfIndex (i4SlotId, (UINT4 *) &i4StartIndex) == MBSM_FAILURE)
    {
        CliPrintf (CliHandle,
                   "Unable to get StartIfIndex - Cannot add entry\r\n");
        return CLI_FAILURE;
    }

    MBSM_SLOT_INDEX_STARTINDEX (i4SlotId) = i4StartIndex;

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MbsmCliShowHardware                                */
/*                                                                           */
/*     DESCRIPTION      : This function displays the Slot Id vs. Card        */
/*                        configuration.                                     */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handle                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
MbsmCliShowHardware (tCliHandle CliHandle)
{
    INT2                i2CardNameLen = 0;
    INT4                i4CurrSlotId;
    INT4                i4PrevSlotId;
    INT1                ai1CardName[MBSM_MAX_CARD_NAME_LEN];
    INT4                i4Status;
    INT4                i4OutCome;
    INT4                i4RetVal;

    tSNMP_OCTET_STRING_TYPE *pName;

    i4OutCome = (INT4) nmhGetFirstIndexMbsmLCConfigTable (&i4CurrSlotId);

    if (i4OutCome == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\r\nLine Card Config table \r\n");

    CliPrintf (CliHandle, "---------------------- \r\n");

    CliPrintf (CliHandle, "\r\n%-6s %-8s %-s\r\n",
               "SlotId", "Status", "Card Name");

    CliPrintf (CliHandle, "%-6s %-8s %-s\r\n", "------", "------", "---------");

    pName = allocmem_octetstring (MBSM_MAX_CARD_NAME_LEN + 1);
    if (pName == NULL)
    {
        return CLI_FAILURE;
    }
    pName->i4_Length = MBSM_MAX_CARD_NAME_LEN;

    do
    {
        if (nmhGetMbsmLCConfigCardName (i4CurrSlotId, pName) == SNMP_FAILURE)
        {
            i2CardNameLen = 0;
            i4Status = MBSM_STATUS_NP;
        }
        else
        {
            i2CardNameLen =
                (INT2) ((pName->i4_Length <
                         (MBSM_MAX_CARD_NAME_LEN -
                          1)) ? pName->i4_Length : (MBSM_MAX_CARD_NAME_LEN -
                                                    1));
        }

        if (i2CardNameLen > 0)
        {
            MEMCPY (ai1CardName, pName->pu1_OctetList, i2CardNameLen);
        }
        ai1CardName[i2CardNameLen] = '\0';

        if (nmhGetMbsmSlotModuleStatus (i4CurrSlotId, &i4Status)
            == SNMP_FAILURE)
        {
            i4Status = MBSM_STATUS_INACTIVE;
        }

        CliPrintf (CliHandle, "%-6d ", i4CurrSlotId);

        switch (i4Status)
        {
            case MBSM_STATUS_ACTIVE:
                CliPrintf (CliHandle, "%-8s ", "UP");
                break;

            case MBSM_STATUS_NP:
                CliPrintf (CliHandle, "%-8s ", "NP");
                break;

            case MBSM_STATUS_DOWN:
                CliPrintf (CliHandle, "%-8s ", "DOWN");
                break;

            case MBSM_STATUS_INACTIVE:
                CliPrintf (CliHandle, "%-8s ", "INACTIVE");
                break;

            default:
                CliPrintf (CliHandle, "%-8s ", "UNKNOWN");
                break;
        }

        i4RetVal = CliPrintf (CliHandle, "%-s\r\n", ai1CardName);

        if (i4RetVal == CLI_FAILURE)
        {
            break;
        }

        i4PrevSlotId = i4CurrSlotId;

        i4OutCome =
            (INT4) (nmhGetNextIndexMbsmLCConfigTable
                    (i4PrevSlotId, (INT4 *) &i4CurrSlotId));

    }
    while (i4OutCome != SNMP_FAILURE);

    free_octetstring (pName);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MbsmiCliLoadSharingEnable                          */
/*                                                                           */
/*     DESCRIPTION      : This function enables Load-Sharing                 */
/*                                                                           */
/*     INPUT            : pu1CliInput - Pointer to the CLI information.      */
/*                                                                           */
/*     OUTPUT           : ppu1OutMsg - Output pointer which stores the       */
/*                                     error messages in case of failure.    */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
MbsmCliLoadSharingEnable (tCliHandle CliHandle)
{
    UINT4               u4ErrCode;

    if (nmhTestv2MbsmLoadSharingFlag (&u4ErrCode, MBSM_LOAD_SHARING_ENABLE)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Load Sharing is already enabled.\r\n");
        return (CLI_FAILURE);
    }

    if (nmhSetMbsmLoadSharingFlag (MBSM_LOAD_SHARING_ENABLE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "ERROR: SetMbsmLoadSharingFlag -- Failed.\r\n");
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MbsmCliLoadSharingiDisable                         */
/*                                                                           */
/*     DESCRIPTION      : This function disables Load-Sharing                */
/*                                                                           */
/*     INPUT            : pu1CliInput - Pointer to the CLI information.      */
/*                                                                           */
/*     OUTPUT           : ppu1OutMsg - Output pointer which stores the       */
/*                                     error messages in case of failure.    */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
MbsmCliLoadSharingDisable (tCliHandle CliHandle)
{
    UINT4               u4ErrCode;

    if (nmhTestv2MbsmLoadSharingFlag (&u4ErrCode, MBSM_LOAD_SHARING_DISABLE)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Load Sharing is already disabled.\r\n");
        return (CLI_FAILURE);
    }

    if (nmhSetMbsmLoadSharingFlag (MBSM_LOAD_SHARING_DISABLE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "ERROR: nmhSetMbsmLoadSharingFlag -- Failed.\r\n");
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MbsmShowRunningConfig                              */
/*                                                                           */
/*     DESCRIPTION      : This function displays the current configurations  */
/*                        of this module                                     */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handle                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
MbsmShowRunningConfig (tCliHandle CliHandle)
{
    INT1                ai1CardName[MBSM_MAX_CARD_NAME_LEN];
    UINT1               u1Flag = 0;
    INT2                i2CardNameLen = 0;
    INT4                i4CurrSlotId;
    INT4                i4PrevSlotId;
    INT4                i4Status;
    INT4                i4OutCome;
    INT4                i4ModType;
    tSNMP_OCTET_STRING_TYPE *pName;

    CliRegisterLock (CliHandle, MbsmLock, MbsmUnLock);
    MBSM_LOCK ();

    i4OutCome = (INT4) nmhGetFirstIndexMbsmLCConfigTable (&i4CurrSlotId);

    if (i4OutCome != SNMP_FAILURE)
    {
        pName = allocmem_octetstring (MBSM_MAX_CARD_NAME_LEN + 1);
        if (pName != NULL)
        {
            pName->i4_Length = MBSM_MAX_CARD_NAME_LEN;

            do
            {
                nmhGetMbsmLCConfigStatus (i4CurrSlotId, &i4Status);

                if (i4Status == MBSM_TRUE)
                {
                    nmhGetMbsmSlotModuleStatus (i4CurrSlotId, &i4Status);
                    if (i4Status == MBSM_STATUS_ACTIVE)
                    {
                        if (nmhGetMbsmLCConfigCardName (i4CurrSlotId, pName)
                            != SNMP_FAILURE)
                        {
                            i2CardNameLen =
                                (INT2) ((pName->i4_Length <
                                         (MBSM_MAX_CARD_NAME_LEN - 1))
                                        ? pName->
                                        i4_Length : (MBSM_MAX_CARD_NAME_LEN -
                                                     1));
                            MEMCPY (ai1CardName, pName->pu1_OctetList,
                                    i2CardNameLen);
                            ai1CardName[i2CardNameLen] = '\0';
                            u1Flag = 1;
                        }

                    }
                }

                i4PrevSlotId = i4CurrSlotId;
                i4OutCome =
                    (INT4) (nmhGetNextIndexMbsmLCConfigTable
                            (i4PrevSlotId, &i4CurrSlotId));

            }
            while (i4OutCome != SNMP_FAILURE);

            free_octetstring (pName);
        }
    }
    nmhGetMbsmLoadSharingFlag (&i4Status);
    if (i4Status != MBSM_LOAD_SHARING_DISABLE)
    {
        CliPrintf (CliHandle, "load-sharing enable\r\n");
        u1Flag = 1;
    }

    i4OutCome = (INT4) nmhGetFirstIndexMbsmSlotModuleMapTable (&i4CurrSlotId);

    if (i4OutCome != SNMP_FAILURE)
    {
        do
        {
            nmhGetMbsmSlotModuleStatus (i4CurrSlotId, &i4Status);
            if (i4Status == MBSM_STATUS_ACTIVE)
            {
                nmhGetMbsmSlotModuleType (i4CurrSlotId, &i4ModType);
                if (i4ModType == MBSM_LINE_CARD)
                {
                    u1Flag = 1;
                }
                else if (i4ModType == MBSM_CONTROL_CARD)
                {
                    u1Flag = 1;
                }
            }
            i4PrevSlotId = i4CurrSlotId;
            i4OutCome =
                (INT4) (nmhGetNextIndexMbsmSlotModuleMapTable
                        (i4PrevSlotId, &i4CurrSlotId));

        }
        while (i4OutCome != SNMP_FAILURE);
    }
    if (u1Flag == 1)
    {
        CliPrintf (CliHandle, "!\r\n");
    }
    MBSM_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MbsmCliSlotModTypeAdd                              */
/*                                                                           */
/*     DESCRIPTION      : This function updates the Slot entry with the card */
/*                        module type on that slot.                          */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        i4SlotId - slot id                                 */
/*                        i4ModuleType - module type                         */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
MbsmCliUpdateLCModType (tCliHandle CliHandle, INT4 i4SlotId,
                        INT1 *pi1CardName, INT4 i4SlotStatus)
{
    UINT4               u4ErrorCode;

    if (MbsmCliPreConfigAdd (CliHandle, i4SlotId, pi1CardName) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2MbsmLCConfigStatus (&u4ErrorCode, i4SlotId, i4SlotStatus)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Invalid Slot Module status - "
                   "cannot configure slot status\r\n");
        return CLI_FAILURE;
    }

    nmhSetMbsmLCConfigStatus (i4SlotId, i4SlotStatus);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MbsmCliGetShowCmdOutputToFile                        */
/*                                                                           */
/* Description        : This function handles the execution of show commands */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu1FileName - The output of the show cmd is          */
/*                      redirected to this file                              */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : MBSM_SUCCESS/MBSM_FAILURE                            */
/*****************************************************************************/
INT4
MbsmCliGetShowCmdOutputToFile (UINT1 *pu1FileName)
{
    if (FileStat ((const CHR1 *) pu1FileName) == OSIX_SUCCESS)
    {
        if (0 != FileDelete (pu1FileName))
        {
            return MBSM_FAILURE;
        }
    }
    if (CliGetShowCmdOutputToFile (pu1FileName, (UINT1 *) MBSM_AUDIT_SHOW_CMD)
        == CLI_FAILURE)
    {
        return MBSM_FAILURE;
    }
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MbsmCliCalcSwAudCheckSum                             */
/*                                                                           */
/* Description        : This function handles the Calculation of checksum    */
/*                      for the data in the given input file.                */
/*                                                                           */
/* Input(s)           : pu1FileName - The checksum is calculated for the data*/
/*                      in this file                                         */
/*                                                                           */
/* Output(s)          : pu2ChkSum - The calculated checksum is stored here   */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : MBSM_SUCCESS/MBSM_FAILURE                            */
/*****************************************************************************/
INT4
MbsmCliCalcSwAudCheckSum (UINT1 *pu1FileName, UINT2 *pu2ChkSum)
{
    UINT4               u4Sum = 0;
    UINT4               u4Last = 0;
    INT4                i4Fd;
    UINT2               u2CkSum = 0;
    INT2                i2ReadLen;
    INT1                ai1Buf[MBSM_CLI_MAX_GROUPS_LINE_LEN + 1];

    i4Fd = FileOpen ((CONST UINT1 *) pu1FileName, MBSM_CLI_RDONLY);
    if (i4Fd == -1)
    {
        return MBSM_FAILURE;
    }
    MEMSET (ai1Buf, 0, MBSM_CLI_MAX_GROUPS_LINE_LEN + 1);
    while (MbsmCliReadLineFromFile (i4Fd, ai1Buf, MBSM_CLI_MAX_GROUPS_LINE_LEN,
                                    &i2ReadLen) != MBSM_CLI_EOF)

    {
        if (i2ReadLen > 0)
        {
            UtilCompCheckSum ((UINT1 *) ai1Buf, &u4Sum, &u4Last,
                              &u2CkSum, (UINT4) i2ReadLen, CKSUM_DATA);
            MEMSET (ai1Buf, '\0', MBSM_CLI_MAX_GROUPS_LINE_LEN + 1);
        }
    }
    /*CheckSum for last line */
    if ((u4Sum != 0) || (i2ReadLen != 0))
    {
        UtilCompCheckSum ((UINT1 *) ai1Buf, &u4Sum, &u4Last,
                          &u2CkSum, (UINT4) i2ReadLen, CKSUM_LASTDATA);
    }
    *pu2ChkSum = u2CkSum;
    if (FileClose (i4Fd) < 0)
    {
        return MBSM_FAILURE;
    }
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MbsmCliReadLineFromFile                              */
/*                                                                           */
/* Description        : It is a utility to read a line from the given file   */
/*                      descriptor.                                          */
/*                                                                           */
/* Input(s)           : i4Fd - File Descriptor for the file                  */
/*                      i2MaxLen - Maximum length that can be read and store */
/*                                                                           */
/* Output(s)          : pi1Buf - Buffer to store the line read from the file */
/*                      pi2ReadLen - the total length of the data read       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : MBSM_SUCCESS/MBSM_FAILURE                            */
/*****************************************************************************/
INT1
MbsmCliReadLineFromFile (INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen,
                         INT2 *pi2ReadLen)
{
    INT1                i1Char;

    *pi2ReadLen = 0;

    while (FileRead (i4Fd, (CHR1 *) & i1Char, 1) == 1)
    {
        pi1Buf[*pi2ReadLen] = i1Char;
        (*pi2ReadLen)++;
        if (i1Char == '\n')
        {
            return (MBSM_CLI_NO_EOF);
        }
        if (*pi2ReadLen == i2MaxLen)
        {
            *pi2ReadLen = 0;
            break;
        }
    }
    pi1Buf[*pi2ReadLen] = '\0';
    return (MBSM_CLI_EOF);
}

INT4
MbsmCliInsertLC (tCliHandle CliHandle, INT4 i4SlotId, UINT1 *pu1CardName)
{
    tMbsmHwMsg          MbsmHwMsg;
    UINT4               u4NumPorts = 0;
    INT4                i4ModuleType;
    UINT4               u4IfIndex = 0;
    INT4                i4MbsmMsgType;
#ifdef NPSIM_WANTED
    tMbsmHwCardInfo     MbsmHwCardInfo;
#endif
    tHwIdInfo           HwIdInfo;
    UINT4               u4StartIfIndex = 0;

    if (MBSM_VALIDATE_SLOT_ID ((i4SlotId - MBSM_SLOT_INDEX_START)) ==
        FNP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n Invalid Slot Id\r\n");
        return FNP_FAILURE;
    }

    if (MBSM_SLOT_MAP_ENTRY (i4SlotId) == NULL)
    {
        CliPrintf (CliHandle, "\r\n Invalid Slot Id\r\n");
        return FNP_FAILURE;
    }

    if ((MbsmDetCardTypeandPortNum (pu1CardName, &i4ModuleType, &u4NumPorts) ==
         FNP_FAILURE) || (MBSM_SLOT_INDEX_NUMPORTS (i4SlotId) != u4NumPorts))
    {
        CliPrintf (CliHandle, "\r\nAttached Card Type is Invalid\r\n");
        return FNP_FAILURE;
    }

    /* Giving slot Attach for the Second time */
    if (MBSM_SLOT_INDEX_STATUS (i4SlotId) == MBSM_STATUS_ACTIVE)
    {
        CliPrintf (CliHandle, "\r\nSlot Already Attached\r\n");
        return FNP_SUCCESS;
    }

    /* Form message appropriately and indicate to ISS
     * */
    MEMSET (&MbsmHwMsg, 0, sizeof (tMbsmHwMsg));

    MbsmHwMsg.i4NumEntries = 1;
    MbsmHwMsg.FirstEntry.i4SlotId = i4SlotId;

    /* Send the card type as invalid,
     * Card type will be identified by MBSM from the input card name
     */
    MbsmHwMsg.FirstEntry.i4CardType = MBSM_INVALID_CARDTYPE;
    MbsmHwMsg.FirstEntry.u4NumPorts = u4NumPorts;
    MbsmHwMsg.FirstEntry.i1Status = MBSM_STATUS_ACTIVE;
    MbsmHwMsg.FirstEntry.i4ModType = i4ModuleType;

    STRNCPY (&(MbsmHwMsg.FirstEntry.ai1CardName), pu1CardName,
             MBSM_MAX_CARD_NAME_LEN - MBSM_ONE);

    for (u4IfIndex = 1; u4IfIndex <= u4NumPorts; u4IfIndex++)
    {
        OSIX_BITLIST_SET_BIT (MbsmHwMsg.FirstEntry.MbsmPortList,
                              u4IfIndex, sizeof (tMbsmPortList));
    }
    i4MbsmMsgType = ((i4ModuleType == MBSM_LINE_CARD) ?
                     MBSM_MSG_HW_LC_INSERT : MBSM_MSG_HW_CC_INSERT);

#ifdef NPSIM_WANTED
    MbsmHwCardInfo.i4SlotId = i4SlotId;
    MbsmHwCardInfo.i4ModuleType = i4ModuleType;
    MbsmHwCardInfo.u4NumPorts = u4NumPorts;

    STRNCPY ((MbsmHwCardInfo.ai1CardName), pu1CardName, MBSM_MAX_CARD_NAME_LEN - MBSM_ONE);

    MbsmNpSetCardInfo (&MbsmHwCardInfo);
#endif

    if (MbsmMbsmNpInitHwTopoDisc (i4SlotId, MBSM_STATUS_DOWN) == FNP_FAILURE)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_PROTO,
                  "Error in MbsmNpInitHwTopoDisc - FAILURE.\n");

        return (MBSM_FAILURE);
    }

    MEMSET (&HwIdInfo, 0, sizeof (tHwIdInfo));
    u4StartIfIndex = MBSM_PORT_INDEX_STARTINDEX (i4SlotId);
    for (u4IfIndex = u4StartIfIndex;
         u4IfIndex <= (u4StartIfIndex + u4NumPorts); u4IfIndex++)
    {
        MEMSET (&HwIdInfo, 0, sizeof (tHwIdInfo));
        HwIdInfo.u4IfIndex = u4IfIndex;
        HwIdInfo.u1Status = FNP_TRUE;
        CfaCfaNpGetHwInfo (&HwIdInfo);
        CfaCfaNpRemoteSetHwInfo (&HwIdInfo);
    }

    return FNP_SUCCESS;
}

INT4
MbsmCliRemoveLC (tCliHandle CliHandle, INT4 i4SlotId)
{
    tMbsmHwMsg          MbsmHwMsg;
    UINT4               u4IfIndex = 0;
    UINT4               u4NumPorts = 0;
    INT4                i4MbsmMsgType;

    if (MBSM_VALIDATE_SLOT_ID (i4SlotId - MBSM_SLOT_INDEX_START) == FNP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n Invalid Slot Id\r\n");
        return FNP_FAILURE;
    }

    if (MBSM_SLOT_MAP_ENTRY (i4SlotId) == NULL)
    {
        CliPrintf (CliHandle, "\r\n Invalid Slot Id\r\n");
        return FNP_FAILURE;
    }

    if (MBSM_SLOT_INDEX_STATUS (i4SlotId) == MBSM_STATUS_NP)
    {
        CliPrintf (CliHandle, "\r\nSlot Already Removed\r\n");
        return FNP_SUCCESS;
    }

    MEMSET (&MbsmHwMsg, 0, sizeof (tMbsmHwMsg));
    MbsmHwMsg.i4NumEntries = 1;
    MbsmHwMsg.FirstEntry.i4CardType = MBSM_INVALID_CARDTYPE;
    MbsmHwMsg.FirstEntry.i1Status = MBSM_STATUS_ACTIVE;

    MbsmHwMsg.FirstEntry.i4SlotId = i4SlotId;
    /*Card Remove Event */
    for (u4IfIndex = 1; u4IfIndex <= u4NumPorts; u4IfIndex++)
    {
        OSIX_BITLIST_SET_BIT (MbsmHwMsg.FirstEntry.MbsmPortList,
                              u4IfIndex, sizeof (tMbsmPortList));
    }
    i4MbsmMsgType = MBSM_MSG_HW_LC_REMOVE;

    if (MbsmNotifyMbsmFromDriver (&MbsmHwMsg, i4MbsmMsgType) == MBSM_FAILURE)
    {
        return FNP_FAILURE;
    }

    return FNP_SUCCESS;
}

INT4
MbsmDetCardTypeandPortNum (UINT1 *pu1CardName,
                           INT4 *pi4ModuleType, UINT4 *pu4NumPorts)
{
    *pi4ModuleType = MBSM_LINE_CARD;

    if (STRCMP (pu1CardName, "NPSIM_LC_12G") == 0)
    {
        *pu4NumPorts = 12;        /* NPSIM_LC_12G_MAX_PORTS */
    }
    else if (STRCMP (pu1CardName, "NPSIM_LC_12F") == 0)
    {
        *pu4NumPorts = 12;        /*NPSIM_LC_12F_MAX_PORTS */
    }
    else if (STRCMP (pu1CardName, "NPSIM_LC_12G_6F") == 0)
    {
        *pu4NumPorts = 18;        /* NPSIM_LC_12G_6F_MAX_PORTS */
    }
    else if (STRCMP (pu1CardName, "NPSIM_LC_24X") == 0)
    {
        *pu4NumPorts = 24;        /* NPSIM_LC_24X_MAX_PORTS */
    }
    else if (STRCMP (pu1CardName, "NPSIM_LC_24G") == 0)
    {
        *pu4NumPorts = 24;        /* NPSIM_LC_24G_MAX_PORTS */
    }
    else if (STRCMP (pu1CardName, "NPSIM_LC_48G") == 0)
    {
        *pu4NumPorts = 48;        /* NPSIM_LC_48G_MAX_PORTS */
    }
    else if (STRCMP (pu1CardName, "NPSIM_LC_24XL") == 0)
    {
        *pu4NumPorts = 24;        /* NPSIM_LC_24XL_MAX_PORTS */
    }
    else if (STRCMP (pu1CardName, "NPSIM_LC_24LVI") == 0)
    {
        *pu4NumPorts = 24;        /* NPSIM_LC_24LVI_MAX_PORTS */
    }
    else
    {
        *pi4ModuleType = MBSM_INVALID_TYPE;
        *pu4NumPorts = MBSM_INVALID_PORTNUM;
        return FNP_FAILURE;
    }

    return FNP_SUCCESS;
}
