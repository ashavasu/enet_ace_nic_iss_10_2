/*$Id: fsmbsmwr.c,v 1.7 2011/10/28 10:49:14 siva Exp $*/
# include  "lr.h"
# include  "mbsminc.h"
# include  "fssnmp.h"
# include  "fsmbsmwr.h"
# include  "fsmbsmdb.h"

VOID
RegisterFSMBSM ()
{
    SNMPRegisterMib (&fsmbsmOID, &fsmbsmEntry, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fsmbsmOID, (const UINT1 *) "fsmbsm");
}

VOID
UnRegisterFSMBSM ()
{
    SNMPUnRegisterMib (&fsmbsmOID, &fsmbsmEntry);
    SNMPDelSysorEntry (&fsmbsmOID, (const UINT1 *) "fsmbsm");
}

INT4
MbsmMaxNumOfLCSlotsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    MBSM_LOCK ();
    i4RetVal = nmhGetMbsmMaxNumOfLCSlots (&(pMultiData->i4_SLongValue));
    MBSM_UNLOCK ();
    return (i4RetVal);
}

INT4
MbsmMaxNumOfSlotsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    MBSM_LOCK ();
    i4RetVal = nmhGetMbsmMaxNumOfSlots (&(pMultiData->i4_SLongValue));
    MBSM_UNLOCK ();
    return (i4RetVal);
}

INT4
MbsmMaxNumOfPortsPerLCGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    MBSM_LOCK ();
    i4RetVal = nmhGetMbsmMaxNumOfPortsPerLC (&(pMultiData->i4_SLongValue));
    MBSM_UNLOCK ();
    return (i4RetVal);
}

INT4
MbsmLoadSharingFlagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    MBSM_LOCK ();
    i4RetVal = nmhGetMbsmLoadSharingFlag (&(pMultiData->i4_SLongValue));
    MBSM_UNLOCK ();
    return (i4RetVal);
}

INT4
MbsmMaxNumOfLCSlotsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    MBSM_LOCK ();
    i4RetVal = nmhSetMbsmMaxNumOfLCSlots (pMultiData->i4_SLongValue);
    MBSM_UNLOCK ();
    return (i4RetVal);
}

INT4
MbsmMaxNumOfSlotsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    MBSM_LOCK ();
    i4RetVal = nmhSetMbsmMaxNumOfSlots (pMultiData->i4_SLongValue);
    MBSM_UNLOCK ();
    return (i4RetVal);
}

INT4
MbsmMaxNumOfPortsPerLCSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    MBSM_LOCK ();
    i4RetVal = nmhSetMbsmMaxNumOfPortsPerLC (pMultiData->i4_SLongValue);
    MBSM_UNLOCK ();
    return (i4RetVal);
}

INT4
MbsmLoadSharingFlagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    MBSM_LOCK ();
    i4RetVal = nmhSetMbsmLoadSharingFlag (pMultiData->i4_SLongValue);
    MBSM_UNLOCK ();
    return (i4RetVal);
}

INT4
MbsmMaxNumOfLCSlotsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    MBSM_LOCK ();
    i4RetVal =
        nmhTestv2MbsmMaxNumOfLCSlots (pu4Error, pMultiData->i4_SLongValue);
    MBSM_UNLOCK ();
    return (i4RetVal);
}

INT4
MbsmMaxNumOfSlotsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    MBSM_LOCK ();
    i4RetVal = nmhTestv2MbsmMaxNumOfSlots (pu4Error, pMultiData->i4_SLongValue);
    MBSM_UNLOCK ();
    return (i4RetVal);
}

INT4
MbsmMaxNumOfPortsPerLCTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    MBSM_LOCK ();
    i4RetVal = nmhTestv2MbsmMaxNumOfPortsPerLC
        (pu4Error, pMultiData->i4_SLongValue);
    MBSM_UNLOCK ();
    return (i4RetVal);
}

INT4
MbsmLoadSharingFlagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    MBSM_LOCK ();
    i4RetVal =
        nmhTestv2MbsmLoadSharingFlag (pu4Error, pMultiData->i4_SLongValue);
    MBSM_UNLOCK ();
    return (i4RetVal);
}

INT4
MbsmMaxNumOfLCSlotsDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2MbsmMaxNumOfLCSlots
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
MbsmMaxNumOfSlotsDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2MbsmMaxNumOfSlots
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
MbsmMaxNumOfPortsPerLCDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2MbsmMaxNumOfPortsPerLC
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
MbsmLoadSharingFlagDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2MbsmLoadSharingFlag
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexMbsmSlotModuleMapTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    MBSM_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMbsmSlotModuleMapTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            MBSM_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMbsmSlotModuleMapTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            MBSM_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    MBSM_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
MbsmSlotIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    MBSM_LOCK ();
    if (nmhValidateIndexInstanceMbsmSlotModuleMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        MBSM_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[0].i4_SLongValue;

    MBSM_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
MbsmSlotModuleTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    MBSM_LOCK ();
    if (nmhValidateIndexInstanceMbsmSlotModuleMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        MBSM_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetMbsmSlotModuleType (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue));
    MBSM_UNLOCK ();
    return (i4RetVal);

}

INT4
MbsmSlotModuleStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    MBSM_LOCK ();
    if (nmhValidateIndexInstanceMbsmSlotModuleMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        MBSM_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetMbsmSlotModuleStatus
        (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->i4_SLongValue));

    MBSM_UNLOCK ();
    return (i4RetVal);
}

INT4
MbsmSlotModuleTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    MBSM_LOCK ();
    i4RetVal = nmhSetMbsmSlotModuleType (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue);

    MBSM_UNLOCK ();
    return (i4RetVal);
}

INT4
MbsmSlotModuleStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    MBSM_LOCK ();
    i4RetVal = nmhSetMbsmSlotModuleStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue);
    MBSM_UNLOCK ();
    return (i4RetVal);

}

INT4
MbsmSlotModuleTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    INT4                i4RetVal;
    MBSM_LOCK ();
    i4RetVal = nmhTestv2MbsmSlotModuleType (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue);

    MBSM_UNLOCK ();
    return (i4RetVal);
}

INT4
MbsmSlotModuleStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    INT4                i4RetVal;
    MBSM_LOCK ();
    i4RetVal = nmhTestv2MbsmSlotModuleStatus (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue);

    MBSM_UNLOCK ();
    return (i4RetVal);
}

INT4
MbsmSlotModuleMapTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2MbsmSlotModuleMapTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexMbsmLCTypeTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    MBSM_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMbsmLCTypeTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            MBSM_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMbsmLCTypeTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            MBSM_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    MBSM_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
MbsmLCIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    MBSM_LOCK ();
    if (nmhValidateIndexInstanceMbsmLCTypeTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        MBSM_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[0].i4_SLongValue;

    MBSM_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
MbsmLCNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    MBSM_LOCK ();
    if (nmhValidateIndexInstanceMbsmLCTypeTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        MBSM_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetMbsmLCName (pMultiIndex->pIndex[0].i4_SLongValue,
                                 pMultiData->pOctetStrValue);
    MBSM_UNLOCK ();
    return i4RetVal;;

}

INT4
MbsmLCMaxPortsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    MBSM_LOCK ();
    if (nmhValidateIndexInstanceMbsmLCTypeTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        MBSM_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetMbsmLCMaxPorts (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue));
    MBSM_UNLOCK ();
    return i4RetVal;;

}

INT4
MbsmLCRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    MBSM_LOCK ();
    if (nmhValidateIndexInstanceMbsmLCTypeTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        MBSM_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetMbsmLCRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue));
    MBSM_UNLOCK ();
    return i4RetVal;;

}

INT4
MbsmLCNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    MBSM_LOCK ();
    i4RetVal = nmhSetMbsmLCName (pMultiIndex->pIndex[0].i4_SLongValue,
                                 pMultiData->pOctetStrValue);
    MBSM_UNLOCK ();
    return i4RetVal;;

}

INT4
MbsmLCMaxPortsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    MBSM_LOCK ();
    i4RetVal = nmhSetMbsmLCMaxPorts (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue);

    MBSM_UNLOCK ();
    return i4RetVal;;
}

INT4
MbsmLCRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    MBSM_LOCK ();
    i4RetVal = nmhSetMbsmLCRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->i4_SLongValue);
    MBSM_UNLOCK ();
    return i4RetVal;;

}

INT4
MbsmLCNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    MBSM_LOCK ();
    i4RetVal = nmhTestv2MbsmLCName (pu4Error,
                                    pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiData->pOctetStrValue);
    MBSM_UNLOCK ();
    return i4RetVal;;

}

INT4
MbsmLCMaxPortsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    INT4                i4RetVal;
    MBSM_LOCK ();
    i4RetVal = nmhTestv2MbsmLCMaxPorts (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue);
    MBSM_UNLOCK ();
    return i4RetVal;;

}

INT4
MbsmLCRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    INT4                i4RetVal;
    MBSM_LOCK ();
    i4RetVal = nmhTestv2MbsmLCRowStatus (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue);
    MBSM_UNLOCK ();
    return i4RetVal;;

}

INT4
MbsmLCTypeTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2MbsmLCTypeTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexMbsmLCPortInfoTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    MBSM_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMbsmLCPortInfoTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            MBSM_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMbsmLCPortInfoTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            MBSM_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    MBSM_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
MbsmLCPortIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    MBSM_LOCK ();
    if (nmhValidateIndexInstanceMbsmLCPortInfoTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        MBSM_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[1].i4_SLongValue;

    MBSM_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
MbsmLCPortIfTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    MBSM_LOCK ();
    if (nmhValidateIndexInstanceMbsmLCPortInfoTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        MBSM_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetMbsmLCPortIfType (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue));
    MBSM_UNLOCK ();
    return i4RetVal;

}

INT4
MbsmLCPortSpeedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    MBSM_LOCK ();
    if (nmhValidateIndexInstanceMbsmLCPortInfoTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        MBSM_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetMbsmLCPortSpeed (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      &(pMultiData->u4_ULongValue));

    MBSM_UNLOCK ();
    return i4RetVal;
}

INT4
MbsmLCPortHighSpeedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    MBSM_LOCK ();
    if (nmhValidateIndexInstanceMbsmLCPortInfoTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        MBSM_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetMbsmLCPortHighSpeed (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue));
    MBSM_UNLOCK ();
    return i4RetVal;

}

INT4
MbsmLCPortIfTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    INT4                i4RetVal;
    MBSM_LOCK ();
    i4RetVal = nmhSetMbsmLCPortIfType (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiData->i4_SLongValue);

    MBSM_UNLOCK ();
    return i4RetVal;
}

INT4
MbsmLCPortSpeedSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    MBSM_LOCK ();
    i4RetVal = nmhSetMbsmLCPortSpeed (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      pMultiData->u4_ULongValue);
    MBSM_UNLOCK ();
    return i4RetVal;

}

INT4
MbsmLCPortHighSpeedSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    MBSM_LOCK ();
    i4RetVal = nmhSetMbsmLCPortHighSpeed (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiData->u4_ULongValue);
    MBSM_UNLOCK ();
    return i4RetVal;

}

INT4
MbsmLCPortIfTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    INT4                i4RetVal;
    MBSM_LOCK ();
    i4RetVal = nmhTestv2MbsmLCPortIfType (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiData->i4_SLongValue);

    MBSM_UNLOCK ();
    return i4RetVal;
}

INT4
MbsmLCPortSpeedTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    INT4                i4RetVal;
    MBSM_LOCK ();
    i4RetVal = nmhTestv2MbsmLCPortSpeed (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         pMultiData->u4_ULongValue);
    MBSM_UNLOCK ();
    return i4RetVal;

}

INT4
MbsmLCPortHighSpeedTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    INT4                i4RetVal;
    MBSM_LOCK ();
    i4RetVal = (nmhTestv2MbsmLCPortHighSpeed (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiIndex->pIndex[1].
                                              i4_SLongValue,
                                              pMultiData->u4_ULongValue));

    MBSM_UNLOCK ();
    return i4RetVal;
}

INT4
MbsmLCPortInfoTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2MbsmLCPortInfoTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexMbsmLCConfigTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    MBSM_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMbsmLCConfigTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            MBSM_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMbsmLCConfigTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            MBSM_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    MBSM_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
MbsmLCConfigSlotIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    MBSM_LOCK ();
    if (nmhValidateIndexInstanceMbsmLCConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        MBSM_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[0].i4_SLongValue;

    MBSM_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
MbsmLCConfigCardNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    MBSM_LOCK ();
    if (nmhValidateIndexInstanceMbsmLCConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        MBSM_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetMbsmLCConfigCardName (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->pOctetStrValue);

    MBSM_UNLOCK ();
    return i4RetVal;
}

INT4
MbsmLCConfigStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    MBSM_LOCK ();
    if (nmhValidateIndexInstanceMbsmLCConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        MBSM_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = (nmhGetMbsmLCConfigStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

    MBSM_UNLOCK ();
    return i4RetVal;
}

INT4
MbsmLCConfigCardNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    MBSM_LOCK ();
    i4RetVal =
        (nmhSetMbsmLCConfigCardName
         (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));
    MBSM_UNLOCK ();
    return i4RetVal;

}

INT4
MbsmLCConfigStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    MBSM_LOCK ();
    i4RetVal = (nmhSetMbsmLCConfigStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));
    MBSM_UNLOCK ();
    return i4RetVal;

}

INT4
MbsmLCConfigCardNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    INT4                i4RetVal;
    MBSM_LOCK ();
    i4RetVal = (nmhTestv2MbsmLCConfigCardName (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->pOctetStrValue));

    MBSM_UNLOCK ();
    return i4RetVal;
}

INT4
MbsmLCConfigStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    INT4                i4RetVal;
    MBSM_LOCK ();
    i4RetVal = (nmhTestv2MbsmLCConfigStatus (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));
    MBSM_UNLOCK ();
    return i4RetVal;

}

INT4
MbsmLCConfigTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2MbsmLCConfigTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
