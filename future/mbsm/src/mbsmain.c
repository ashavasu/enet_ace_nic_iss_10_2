/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mbsmain.c,v 1.63 2014/11/03 11:48:17 siva Exp $
 *
 * Description:This file contains the routines for the core 
 *             functionality of the MBSM module. It contains
 *             init functions along with the main loop for 
 *             event processing.                           
 *
 *******************************************************************/
#include "mbsminc.h"
#include "mbsglob.h"
#ifdef MSR_WANTED
#include "msr.h"
#endif

#ifdef SNMP_2_WANTED
#include "fsmbsmwr.h"
#endif

#ifdef NPAPI_WANTED
#include "npapi.h"
#include "mbsnp.h"
#include "cfanp.h"
#endif /* NPAPI_WANTED */

#include "cli.h"
#ifdef KERNEL_WANTED
#include "chrdev.h"
#endif

UINT1               gau1MbsmPortBitMask[MBSM_PORTS_PER_BYTE] =
    { 0x01, 0x80, 0x40, 0x20, 0x10,
    0x08, 0x04, 0x02
};

#ifndef KERNEL_WANTED
UINT1               gu1NumofStkPorts;
UINT1               gu1StackPriority;
#endif

extern INT4         gi4MibResStatus;

/*****************************************************************************
 *
 *    Function Name        : MbsmInit
 *
 *    Description          : This function performs the initialisation of
 *                           the MBSM. It initialises the tables used in MBSM.
 *
 *    Input(s)             : None. 
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gaMbsmSlotMapTable, gMbsmQId
 *
 *    Global Variables Modified : gaMbsmSlotMapTable, gMbsmQid
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion     : None.
 *
 *    Returns              : MBSM_SUCCESS or MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmInit (VOID)
{
#ifdef KERNEL_WANTED
    tStackingParams     StackingParams;
#endif
#if defined (RM_WANTED) || defined (KERNEL_WANTED)
    UINT4               u4StackPriority = 0;
#endif
#ifdef RM_WANTED
    UINT4               u4SwitchId = 0;
#endif
    INT4                i4RetVal = MBSM_SUCCESS;

    MBSM_SEM_CREATE ();
    MBSM_GLB_TRC () = MBSM_TRC_CRITICAL;

    MBSM_DEF_INIT_FLAG () = MBSM_TRUE;

    MBSM_LOAD_SHARING_FLAG () = MBSM_LOAD_SHARING_DISABLE;

    if (MBSM_CREATE_TMR_LIST ((const UINT1 *) MBSM_TASK_NAME,
                              MBSM_TMR_EXPIRY_EVENT,
                              NULL, &(MBSM_TIMER_LIST_ID)) != OSIX_SUCCESS)
    {
        MBSM_DBG (MBSM_TRC_CRITICAL, MBSM_MAIN,
                  "Fatal Error in MbsmInit - Timer creation " "failure.\r\n");
        return (MBSM_FAILURE);
    }

    gMbsmGlobalInfo.MbsmRecoveryTmr.MbsmAppTimer.u4Data = MBSM_RECVRY_TMR_TYPE;

    if (MBSM_PROTO_CREATE_SEM (MBSM_PROTO_SEM, 1, 0, &MBSM_PROTO_SEMID)
        != OSIX_SUCCESS)
    {
        MBSM_DBG (MBSM_TRC_CRITICAL, MBSM_MAIN,
                  "Fatal Error in MbsmInit - Protocol Semaphore creation "
                  "failure.\r\n");
        MBSM_DEL_TMR_LIST (MBSM_TIMER_LIST_ID);
        return (MBSM_FAILURE);
    }

    /* Task synchronisation Semaphore created here. This is to sync the Port
     * Creation/Deletion in CFA/IWF and all the Protocols and then to initiate
     * the NP programming for all the modules once the ports are created
     * in all the modules.
     */

    if (MBSM_SYNC_CREATE_SEM (MBSM_SYNC_SEM, 0, 0, &MBSM_SYNC_SEMID)
        != OSIX_SUCCESS)
    {
        MBSM_DBG (MBSM_TRC_CRITICAL, MBSM_MAIN,
                  "Fatal Error in MbsmInit - Synchronization Semaphore"
                  "creation failure.\r\n");
        MBSM_DEL_TMR_LIST (MBSM_TIMER_LIST_ID);
        MBSM_PROTO_DELETE_SEM (MBSM_PROTO_SEM);
        return (MBSM_FAILURE);
    }

    /* Create Memory pools for the MBSM tables */

    /* Memory also has to be allocated to read slot info from file */

    if (MbsmSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        MBSM_DBG (MBSM_TRC_CRITICAL, MBSM_MAIN,
                  "Fatal Error in MbsmInit - MemPool creation failure.\n");
        MBSM_DEL_TMR_LIST (MBSM_TIMER_LIST_ID);
        MBSM_PROTO_DELETE_SEM (MBSM_PROTO_SEM);
        MBSM_SYNC_DELETE_SEM ();
        return (MBSM_FAILURE);
    }

    MBSM_SLOT_MAP_MEMPOOL_ID =
        MBSMMemPoolIds[MAX_MBSM_SLOT_INFO_BLOCKS_SIZING_ID];
    MBSM_PORT_INFO_MEMPOOL_ID =
        MBSMMemPoolIds[MAX_MBSM_PORT_INFO_BLOCKS_SIZING_ID];
    MBSM_CARD_TYPE_MEMPOOL_ID = MBSMMemPoolIds[MAX_MBSM_LC_TYPES_SIZING_ID];
    MBSM_CTRL_QUEUE_MEMPOOL_ID =
        MBSMMemPoolIds[MAX_MBSM_CTRLQ_MSG_MEMBLK_COUNT_SIZING_ID];
    MBSM_MSG_QUEUE_MEMPOOL_ID =
        MBSMMemPoolIds[MAX_MBSM_MSG_QUEUE_DEPTH_SIZING_ID];

    if (MbsmInitSlotMapTable () == MBSM_FAILURE)
    {
        MbsmSizingMemDeleteMemPools ();
        MBSM_DEL_TMR_LIST (MBSM_TIMER_LIST_ID);
        MBSM_PROTO_DELETE_SEM (MBSM_PROTO_SEM);
        MBSM_SYNC_DELETE_SEM ();
        return MBSM_FAILURE;
    }

    if (MbsmInitPortInfoTable () == MBSM_FAILURE)
    {
        MbsmSizingMemDeleteMemPools ();
        MBSM_DEL_TMR_LIST (MBSM_TIMER_LIST_ID);
        MBSM_PROTO_DELETE_SEM (MBSM_PROTO_SEM);
        MBSM_SYNC_DELETE_SEM ();
        return MBSM_FAILURE;
    }

    if (MbsmInitCardTypeTable () == MBSM_FAILURE)
    {
        MbsmSizingMemDeleteMemPools ();
        MBSM_DEL_TMR_LIST (MBSM_TIMER_LIST_ID);
        MBSM_PROTO_DELETE_SEM (MBSM_PROTO_SEM);
        MBSM_SYNC_DELETE_SEM ();
        return MBSM_FAILURE;
    }

    if (OsixCreateQ
        ((const UINT1 *) MBSM_MSG_QUEUE_NAME, MAX_MBSM_MSG_QUEUE_DEPTH, SELF,
         &(MBSM_QUEUE_ID)) != OSIX_SUCCESS)
    {
        MBSM_DBG (MBSM_TRC_CRITICAL, MBSM_MAIN,
                  "Fatal Error in MbsmInit - MBSM Queue Creation Fail - FAILURE.\n");
        MbsmSizingMemDeleteMemPools ();
        MBSM_DEL_TMR_LIST (MBSM_TIMER_LIST_ID);
        MBSM_PROTO_DELETE_SEM (MBSM_PROTO_SEM);
        MBSM_SYNC_DELETE_SEM ();
        return (MBSM_FAILURE);
    }

    if (MbsmInitSysInfo () != MBSM_SUCCESS)
    {
        i4RetVal = MBSM_FAILURE;
    }

    if (MbsmInitSlotModuleInfo () != MBSM_SUCCESS)
    {
        /* Slot Module information not present */
        /* Nothing to be done for now */
        i4RetVal = MBSM_FAILURE;
    }
    if (i4RetVal == MBSM_SUCCESS)
    {
        MBSM_DEF_INIT_FLAG () = MBSM_FALSE;
        /* Send Boot up indication to CFA with the information in the Slot Map
         * table */
        MbsmSendBootMsgToCfa ();
    }
    else
    {
        MBSM_DEF_INIT_FLAG () = MBSM_TRUE;
#ifdef CLI_WANTED
        /*CLI will be wailting for me to complete.. 
         * Let him resume & will allow to login as Chassisuser, 
         * for SlotModule configuration.
         */
        CliInformRestorationComplete ();
#endif
        MGMT_UNLOCK ();
    }

#ifdef RM_WANTED
    if (MbsmRedRmInitAndRegister () == MBSM_FAILURE)
    {
        MbsmShutdown ();
        MBSM_DEL_TMR_LIST (MBSM_TIMER_LIST_ID);
        MBSM_PROTO_DELETE_SEM (MBSM_PROTO_SEM);
        MBSM_SYNC_DELETE_SEM ();
        return MBSM_FAILURE;
    }
#else
    if (i4RetVal == MBSM_SUCCESS)
    {
        /* This node state will be used only if RM is present */
        if ((MbsmInitHwTopoDisc (MBSM_STATUS_ACTIVE) == MBSM_FAILURE) ||
            ((ISS_GET_STACKING_MODEL () == ISS_DISS_STACKING_MODEL) &&
             (MbsmInitHwTopoDisc (MBSM_STATUS_DOWN) == MBSM_FAILURE)))
        {
            MbsmShutdown ();
            MBSM_DEL_TMR_LIST (MBSM_TIMER_LIST_ID);
            MBSM_PROTO_DELETE_SEM (MBSM_PROTO_SEM);
            MBSM_SYNC_DELETE_SEM ();
            return (MBSM_FAILURE);
        }
    }

#endif /* RM_WANTED */
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
#ifdef RM_WANTED
        RmGetNodePriority (&u4StackPriority);
        RmGetNodeSwitchId (&u4SwitchId);
        MBSM_SELF_SLOT_ID () = (UINT1) u4SwitchId;
#endif
#ifdef KERNEL_WANTED

        MEMSET (&StackingParams, 0, sizeof (tStackingParams));
        StackingParams.u1Param = KERN_STACK_PORTS;
        StackingParams.u1Value = ISS_MAX_STK_PORTS;

        KAPIUpdateInfo (STACK_INFO_IOCTL, (tKernCmdParam *) & StackingParams);
        StackingParams.u1Param = KERN_FP_PORTS;
        StackingParams.u1Value = MBSM_MAX_PORTS_PER_SLOT;

        KAPIUpdateInfo (STACK_INFO_IOCTL, (tKernCmdParam *) & StackingParams);
        StackingParams.u1Param = KERN_STACK_PRI;
        StackingParams.u1Value = (UINT1) u4StackPriority;

        KAPIUpdateInfo (STACK_INFO_IOCTL, (tKernCmdParam *) & StackingParams);
        StackingParams.u1Param = KERN_COLD_STDBY_STATE;
        StackingParams.u1Value = (UINT1) ISS_COLDSTDBY_ENABLE;

        KAPIUpdateInfo (STACK_INFO_IOCTL, (tKernCmdParam *) & StackingParams);
#else
        /* Set Default values */
        gu1NumofStkPorts = (UINT1) ISS_MAX_STK_PORTS;
        gu1StackPriority = (UINT1) ISS_MAX_STK_PORTS;

#endif
        /* Stack task should be started in RM_INIT state
         * based on ATTACH/DETACH indication RM and MBSM status will
         * be updated properly */
        if (MbsmInitHwTopoDisc (MBSM_STATUS_DOWN) == MBSM_FAILURE)
        {
            return MBSM_FAILURE;
        }
    }
    else
    {
#ifdef KERNEL_WANTED
        StackingParams.u1Param = KERN_COLD_STDBY_STATE;
        StackingParams.u1Value = 0;
        KAPIUpdateInfo (STACK_INFO_IOCTL, (tKernCmdParam *) & StackingParams);
#endif
    }

#ifdef SYSLOG_WANTED
    MBSM_SYSLOG_ID =
        SYS_LOG_REGISTER ((CONST UINT1 *) "MBSM", SYSLOG_CRITICAL_LEVEL);
#endif /*SYSLOG_WANTED */

#ifdef SNMP_2_WANTED
    RegisterFSMBSM ();
#endif

    gu1IsMbsmInitialised = MBSM_TRUE;

    return (MBSM_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmInitSlotMapTable
 *
 *    Description          : This function performs the initialisation of
 *                           the MBSM Slot Map Table.                        
 *
 *    Input(s)             : None. 
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gaMbsmSlotMapTable
 *
 *    Global Variables Modified : gaMbsmSlotMapTable
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion     : None.
 *
 *    Returns              : MBSM_SUCCESS or MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmInitSlotMapTable (VOID)
{
    INT4                i4LoopIdx1;

    if (MBSM_MAX_SLOTS <= 0)
    {
        /* Max Slots should be greater than zero */
        return (MBSM_FAILURE);
    }

    gMbsmGlobalInfo.gaMbsmSlotMapTable =
        (tMbsmSlotInfo **) (VOID *) &gau1SlotInfoArr[0];

    /* Initialising the Slot Map Table */
    for (i4LoopIdx1 = MBSM_SLOT_INDEX_START;
         i4LoopIdx1 < (MBSM_MAX_SLOTS + MBSM_SLOT_INDEX_START); i4LoopIdx1++)
    {
        if (MemAllocateMemBlock (MBSM_SLOT_MAP_MEMPOOL_ID,
                                 (UINT1 **) &gMbsmGlobalInfo.
                                 gaMbsmSlotMapTable[i4LoopIdx1 -
                                                    MBSM_SLOT_INDEX_START]) ==
            MEM_FAILURE)
        {
            MBSM_DBG (MBSM_TRC_CRITICAL, MBSM_MAIN,
                      "Error in memory allocation for Slot Map Entry\r\n");
            return (MBSM_FAILURE);
        }

        MBSM_SLOT_INDEX_SLOT_ID (i4LoopIdx1) = i4LoopIdx1;
        MBSM_SLOT_INDEX_CARDTYPE (i4LoopIdx1) = MBSM_INVALID_CARDTYPE;
        MBSM_SLOT_INDEX_NUMPORTS (i4LoopIdx1) = MBSM_INVALID_PORTCOUNT;
        MBSM_SLOT_INDEX_STARTINDEX (i4LoopIdx1) = MBSM_INVALID_IFINDEX;
        MBSM_SLOT_INDEX_ISPRECONF (i4LoopIdx1) = MBSM_FALSE;
        MBSM_SLOT_INDEX_STATUS (i4LoopIdx1) = MBSM_STATUS_INACTIVE;
        MBSM_SLOT_INDEX_PREVSTATUS (i4LoopIdx1) = MBSM_STATUS_INACTIVE;
        MBSM_SLOT_INDEX_FABRIC_TYPE (i4LoopIdx1) = MBSM_FALSE;
    }

    return (MBSM_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmInitPortInfoTable
 *
 *    Description          : This function performs the initialisation of
 *                           the MBSM Port Info Table.                       
 *
 *    Input(s)             : None. 
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gaMbsmPortInfoTable
 *
 *    Global Variables Modified : gaMbsmPortInfoTable
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion     : None.
 *
 *    Returns              : MBSM_SUCCESS or MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmInitPortInfoTable (VOID)
{
    INT4                i4LoopIdx1;

    gMbsmGlobalInfo.gaMbsmPortInfoTable =
        (tMbsmPortInfo **) (VOID *) &gau1PortInfoArr[0];

    /* Initialising the Slot Map Table */
    for (i4LoopIdx1 = MBSM_SLOT_INDEX_START;
         i4LoopIdx1 < (MBSM_MAX_SLOTS + MBSM_SLOT_INDEX_START); i4LoopIdx1++)
    {
        if (MemAllocateMemBlock (MBSM_PORT_INFO_MEMPOOL_ID,
                                 (UINT1 **) &gMbsmGlobalInfo.
                                 gaMbsmPortInfoTable[i4LoopIdx1 -
                                                     MBSM_SLOT_INDEX_START]) ==
            MEM_FAILURE)
        {
            MBSM_DBG (MBSM_TRC_CRITICAL, MBSM_MAIN,
                      "Error in memory allocation for Port Info Entry\r\n");
            return (MBSM_FAILURE);
        }

        MEMSET (&(MBSM_PORT_INDEX_PORTLIST (i4LoopIdx1)),
                0, sizeof (tPortList));
        MBSM_PORT_INDEX_PORTCOUNT (i4LoopIdx1) = MBSM_INVALID_PORTCOUNT;
        MBSM_PORT_INDEX_STARTINDEX (i4LoopIdx1) = MBSM_INVALID_IFINDEX;
    }

    return (MBSM_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmCreateCardTypeEntry
 *
 *    Description          : This function creates a new entry in the Card
 *                           Type Table.                                     
 *
 *    Input(s)             : None. 
 *
 *    Output(s)            : *pu4Index - Index of the new entry
 *
 *    Global Variables Referred : gaMbsmLcTypeTable
 *
 *    Global Variables Modified : gaMbsmLcTypeTable
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion     : None.
 *
 *    Returns              : MBSM_SUCCESS or MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmCreateCardTypeEntry (UINT4 u4Index)
{
    if ((MBSM_CARD_TYPE_ENTRY (u4Index)))
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN,
                  "Error : Entry already existsin table\n");
        return (MBSM_FAILURE);
    }

    if (MemAllocateMemBlock (MBSM_CARD_TYPE_MEMPOOL_ID,
                             (UINT1 **) &MBSM_CARD_TYPE_ENTRY (u4Index))
        == MEM_FAILURE)
    {
        MBSM_DBG (MBSM_TRC_CRITICAL, MBSM_MAIN,
                  "Error in memory allocation for Card Type Entry\r\n");
        return (MBSM_FAILURE);
    }
    MEMSET (MBSM_CARD_TYPE_ENTRY (u4Index)->PortMapEntry.IfTypeTable, 0,
            (MBSM_MAX_IF_TYPES * sizeof (tMbsmCardPortList)));
    MEMSET (MBSM_CARD_TYPE_ENTRY (u4Index)->PortMapEntry.MtuTable, 0,
            (MBSM_MAX_PORT_MTU_TYPES * sizeof (tMbsmCardPortList)));
    MEMSET (MBSM_CARD_TYPE_ENTRY (u4Index)->PortMapEntry.SpeedTable, 0,
            (MBSM_MAX_PORT_SPEED_TYPES * sizeof (tMbsmCardPortList)));

    return (MBSM_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmDeleteCardTypeEntry
 *
 *    Description          : This function deletes the entry for the index
 *                           in the Card Type table.                         
 *
 *    Input(s)             : u4Index - Index of the enty to be deleted.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gaMbsmLcTypeTable
 *
 *    Global Variables Modified : gaMbsmLcTypeTable
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion     : None.
 *
 *    Returns              : MBSM_SUCCESS or MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmDeleteCardTypeEntry (UINT4 u4Index)
{
    if (MBSM_CARD_TYPE_ENTRY (u4Index) == NULL)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN, "Invalid Cardtype Entry\n");
        return (MBSM_FAILURE);
    }

    MemReleaseMemBlock (MBSM_CARD_TYPE_MEMPOOL_ID,
                        (UINT1 *) MBSM_CARD_TYPE_ENTRY (u4Index));

    MBSM_CARD_TYPE_ENTRY (u4Index) = NULL;

    return (MBSM_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmGetFreeCardTypeTableIndex
 *
 *    Description          : This function fetches the free index for a new
 *                           entry in the Card Type table.                   
 *
 *    Input(s)             : None. 
 *
 *    Output(s)            : pu4FreeIndex - Free Index in the table.
 *
 *    Global Variables Referred : gaMbsmLcTypeTable 
 *
 *    Global Variables Modified : gaMbsmLcTypeTable 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion     : None.
 *
 *    Returns              : MBSM_SUCCESS or MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmGetFreeCardTypeTableIndex (UINT4 *pu4FreeIndex)
{
    UINT4               u4LoopIndex;

    for (u4LoopIndex = 0; u4LoopIndex < MBSM_MAX_LC_TYPES; u4LoopIndex++)
    {
        if (!(MBSM_CARD_TYPE_ENTRY (u4LoopIndex)))
        {
            *pu4FreeIndex = u4LoopIndex;
            break;
        }
    }

    if (u4LoopIndex >= MBSM_MAX_LC_TYPES)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN,
                  "Card Type Table Full - Cannot create entry\n");
        return (MBSM_FAILURE);
    }

    return (MBSM_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmMain
 *
 *    Description          : This function is the entry point function to the
 *                           MBSM module. This function initialises all the
 *                           tables of the MBSM module. This function is
 *                           responsible for events and message handling from
 *                           other modules.
 *
 *    Input(s)             : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion     : None.
 *
 *    Returns              : MBSM_SUCCESS or MBSM_FAILURE
 *
 *****************************************************************************/
VOID
MbsmMain (INT1 *pi1Param)
{
    UINT4               u4EventMask;
    INT4                i4SlotId = MBSM_INVALID_SLOT_ID;
    INT4                i4MsgType = MBSM_MSG_UNKOWN;

    UINT4               u4StackingModel = ISS_GET_STACKING_MODEL ();
    /* Used for Ack messages sent from protocols */
    INT4                i4ProtoCookie = MBSM_PROTO_COOKIE_INVALID;

    VOID               *pBuf = NULL;    /* Used to store the message from queue */
    VOID               *pMsg = NULL;    /* pMsg = pBuf - pMsgHdr */

    tOsixMsg           *pQMsg = NULL;
#ifdef RM_WANTED
    tMbsmMsg           *pRmMsg = NULL;
#endif
#ifdef NPAPI_WANTED
    UINT1               u1LoadSharingFlag;
#endif
    UNUSED_PARAM (pi1Param);

    if (MBSM_GET_TASK_ID (&MBSM_TASK_ID) != OSIX_SUCCESS)
    {
        MBSM_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (MbsmInit () == MBSM_FAILURE)
    {
        MBSM_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    MBSM_INIT_COMPLETE (OSIX_SUCCESS);
#ifndef RM_WANTED
#ifdef MSR_WANTED
    MbsmWaitForMSRCompleteEvent ();
#endif
#endif
    MBSM_FOREVER ()
    {

        MBSM_RECV_EVENT (MBSM_TASK_ID, (MBSM_DRIVER_EVENT |
                                        MBSM_SYNC_ACK_MSG_EVENT |
                                        MBSM_PROTO_ACK_MSG_EVENT |
                                        MBSM_LOAD_SHARING_EVENT |
                                        MBSM_TMR_EXPIRY_EVENT |
                                        MBSM_RED_MSG_EVENT |
                                        MBSM_RED_BULK_UPD_EVENT),
                         MBSM_EVENT_WAIT_FLAGS, &u4EventMask);

        if (u4EventMask & MBSM_TMR_EXPIRY_EVENT)
        {
            MBSM_LOCK ();
            MbsmTmrExpiryHandler ();
            MBSM_UNLOCK ();
        }
#ifdef RM_WANTED
        if (u4EventMask & MBSM_RED_BULK_UPD_EVENT)
        {
            MBSM_LOCK ();
            MbsmRedHandleBulkUpdateEvent ();
            MBSM_UNLOCK ();
        }
#endif

        /* Receive the message from the queue */
        while (OsixReceiveFromQ (SELF, (const UINT1 *) MBSM_MSG_QUEUE_NAME,
                                 OSIX_NO_WAIT, 0, &pQMsg) == OSIX_SUCCESS)
        {
            pBuf = (UINT1 *) pQMsg;
            if (MbsmGetMsgType (pBuf, &i4MsgType) == MBSM_FAILURE)
            {
                MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN,
                          "Message Error - Cannot identify message type\n");
            }

            /* As long as MBSM system paramteres are initialised with
               default values, do not proceed processing Driver or
               protocol events
             */
            if (MBSM_DEF_INIT_FLAG () == MBSM_TRUE)
            {
                /* This will have to be changed, if the CLI and SNMP 
                   configurations are to be received as messages to MBSM */
#ifdef RM_WANTED
                pRmMsg = (tMbsmMsg *) pBuf;

                if (i4MsgType == MBSM_RM_QMSG)
                {
                    if ((pRmMsg->uMsg.RmMsg.u1Event == RM_MESSAGE) &&
                        (pRmMsg->uMsg.RmMsg.pFrame != NULL))
                    {
                        RM_FREE (pRmMsg->uMsg.RmMsg.pFrame);
                    }
                    else if (((pRmMsg->uMsg.RmMsg.u1Event == RM_STANDBY_UP) ||
                              (pRmMsg->uMsg.RmMsg.u1Event == RM_STANDBY_DOWN))
                             && (pRmMsg->uMsg.RmMsg.pFrame != NULL))
                    {
                        RmReleaseMemoryForMsg ((UINT1 *) pRmMsg->uMsg.RmMsg.
                                               pFrame);
                    }

                    if (MBSM_RELEASE_CTRLQ_MEM_BLOCK (pRmMsg) != MEM_SUCCESS)
                    {
                        MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN,
                                  "MBSM: CtrlQ Memory Block Release FAILED\n");
                    }
                    pRmMsg = NULL;
                }
                else
#endif
                {
                    MemReleaseMemBlock (MBSM_MSG_QUEUE_MEMPOOL_ID,
                                        (UINT1 *) pBuf);
                    pBuf = NULL;
                }
                continue;
            }

            MBSM_LOCK ();
            switch (i4MsgType)
            {

                case MBSM_MSG_HW_LC_PORT_INSERT:

                    pMsg = ((tMbsmMsg *) pBuf)->uMsg.au1Data;

                    if (MbsmGetSlotIdFromMsg ((tMbsmHwMsg *) pMsg, &i4SlotId)
                        != MBSM_SUCCESS)
                    {
                        MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN,
                                  "Message Error - Cannot identify SlotId\n");
                        break;
                    }
                    MBSM_DBG_ARG1 (MBSM_PROTO_TRC, MBSM_MAIN,
                                   "RECV EVENT: MBSM_MSG_HW_LC_PORT_INSERT for Slot %d\r\n",
                                   i4SlotId);

                    if (!MBSM_IS_SLOT_ID_VALID (i4SlotId))
                    {
                        MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN,
                                  "Message Error - Invalid SlotId\n");
                        break;
                    }
                    if (((tMbsmHwMsg *) pMsg)->i4NumEntries != 0)
                    {
                        /* TRUE indicates this is a Insertion event */
                        if (MbsmUpdateTables (&((tMbsmHwMsg *) pMsg)->
                                              FirstEntry, MBSM_LINE_CARD_PORT,
                                              MBSM_TRUE) == MBSM_FAILURE)
                        {
                            break;
                        }

                    }

#ifndef STACKING_WANTED
#ifdef RM_WANTED
                    if (MbsmRedHandleLCInsertEvent (i4SlotId) == MBSM_FAILURE)
                    {
                        MBSM_DBG (MBSM_PROTO_TRC, MBSM_MAIN,
                                  "MbsmRedHandleLCPORTInsertEvent Returned FAILURE\r\n");
                        break;
                    }
#else    /* RM_WANTED */                 /* CHASSIS with no redundancy support */
                    /* Notify CFA to update Port Creats & Oper Status */
                    if (MbsmNotifyCfaModuleUpdate (i4SlotId) == MBSM_FAILURE)
                    {
                        MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN,
                                  "MbsmNotifyCfaModuleUpdate Returned FAILURE\r\n");
                        break;
                    }

#endif /* CHASSIS Redundancy */
#endif /* STACKING_WANTED */

                    break;

                case MBSM_MSG_HW_LC_PORT_REMOVE:

                    pMsg = ((tMbsmMsg *) pBuf)->uMsg.au1Data;

                    if (MbsmGetSlotIdFromMsg ((tMbsmHwMsg *) pMsg, &i4SlotId)
                        != MBSM_SUCCESS)
                    {
                        MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN,
                                  "Message Error - Cannot identify SlotId\n");
                        break;
                    }
                    MBSM_DBG_ARG1 (MBSM_PROTO_TRC, MBSM_MAIN,
                                   "RECV_EVENT: MBSM_MSG_HW_LC_PORT_REMOVE for Slot %d\r\n",
                                   i4SlotId);
                    if (!MBSM_IS_SLOT_ID_VALID (i4SlotId))
                    {
                        MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN,
                                  "Message Error - Invalid SlotId\n");
                        break;
                    }

                    if (((tMbsmHwMsg *) pMsg)->i4NumEntries != 0)
                    {
                        /* FALSE indicates this is a Removal event */
                        if (MbsmUpdateTables
                            (&((tMbsmHwMsg *) pMsg)->FirstEntry,
                             MBSM_LINE_CARD_PORT, MBSM_FALSE) == MBSM_FAILURE)
                        {
                            break;
                        }

#ifdef RM_WANTED
                        if (RmGetNodeState () != RM_ACTIVE)
                        {
                            /* Only Active should process the REMOVAL event */
                            MBSM_DBG_ARG1 (MBSM_TRC_ERR, MBSM_MAIN,
                                           "LC PORT Detach is received for Slot %d "
                                           "At Standby CFM\r\n", i4SlotId);
                            break;
                        }
#endif
                        if (MbsmNotifyCfaModuleUpdate (i4SlotId)
                            == MBSM_FAILURE)
                        {
                            break;
                        }

#ifdef RM_WANTED
#ifdef L2RED_WANTED
                        /*MbsmRedSendSyncMessages (i4SlotId, MBSM_RED_DETACH_MSG); */
                        MbsmRedSendSyncMessages (i4SlotId, MBSM_RED_ATTACH_MSG);
#endif
#endif
                    }
                    break;

                case MBSM_MSG_HW_LC_INSERT:

                    pMsg = ((tMbsmMsg *) pBuf)->uMsg.au1Data;

                    if (MbsmGetSlotIdFromMsg ((tMbsmHwMsg *) pMsg, &i4SlotId)
                        != MBSM_SUCCESS)
                    {
                        MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN,
                                  "Message Error - Cannot identify SlotId\n");
                        break;
                    }
                    MBSM_DBG_ARG1 (MBSM_PROTO_TRC, MBSM_MAIN,
                                   "RECV EVENT: MBSM_MSG_HW_LC_INSERT for Slot %d\r\n",
                                   i4SlotId);

                    if (!MBSM_IS_SLOT_ID_VALID (i4SlotId))
                    {
                        MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN,
                                  "Message Error - Invalid SlotId\n");
                        break;
                    }

                    /* Removed Check - to see Mbsm Slot Status is already Active.
                     * This is not needed since Hw will not give two or more 
                     * ATTACH for the same slot.
                     */
                    /* if (MBSM_SLOT_INDEX_STATUS (i4SlotId) == MBSM_STATUS_ACTIVE)
                     * {
                     * #ifdef SYSLOG_WANTED
                     * SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, MBSM_SYSLOG_ID,
                     *                 "Slot->%d: Insertion message for "
                     *                 "loaded slot!!!", i4SlotId));
                     * #endif
                     * break;
                     * }
                     */

                    if (((tMbsmHwMsg *) pMsg)->i4NumEntries != 0)
                    {
                        /* TRUE indicates this is a Insertion event */
                        if (MbsmUpdateTables (&((tMbsmHwMsg *) pMsg)->
                                              FirstEntry, MBSM_LINE_CARD,
                                              MBSM_TRUE) == MBSM_FAILURE)
                        {
                            break;
                        }

                        if (MbsmMbsmNpProcCardInsertForLoadSharing
                            (MBSM_MSG_HW_LC_INSERT) == FNP_FAILURE)
                        {
                            MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN,
                                      "Process Line card insert event "
                                      "for load-sharing -- Failed\r\n");
                        }
                    }

#ifdef STACKING_WANTED            /* Pizzabox Stacking case */
                    /* Stacking case we use MBSM_MSG_HW_LC_INSERT event itself
                     * as Controlcard insertion. as there is no separate 
                     * control card is present.
                     */
                    if (MbsmRedHandleLCInsertEventForPizzaStacking (i4SlotId)
                        == MBSM_FAILURE)
                    {
                        MBSM_DBG (MBSM_PROTO_TRC, MBSM_MAIN,
                                  "MbsmRedHandleLCInsertEventForPizzaStacking"
                                  " Returned FAILURE\r\n");
                        break;
                    }
                    /* Generate Trap */
                    MbsmSnmpSendTrap (MBSM_CARD_INSERT_TRAP,
                                      (INT1 *) MBSM_CARD_INSERT_TRAP_OID,
                                      MBSM_TRAPS_OID_LEN, &i4SlotId);
#else /* STACKING_WANTED */
#ifdef RM_WANTED                /* CHASSIS With Redundancy Support */
                    if (MbsmRedHandleLCInsertEvent (i4SlotId) == MBSM_FAILURE)
                    {
                        MBSM_DBG (MBSM_PROTO_TRC, MBSM_MAIN,
                                  "MbsmRedHandleLCInsertEvent Returned FAILURE\r\n");
                        break;
                    }
                    /* Generate Trap */
                    MbsmSnmpSendTrap (MBSM_CARD_INSERT_TRAP,
                                      (INT1 *) MBSM_CARD_INSERT_TRAP_OID,
                                      MBSM_TRAPS_OID_LEN, &i4SlotId);
#else    /* RM_WANTED */                 /* CHASSIS with no redundancy support */
                    /* Notify CFA to update Port Creats & Oper Status */
                    if (MbsmNotifyCfaModuleUpdate (i4SlotId) == MBSM_FAILURE)
                    {
                        MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN,
                                  "MbsmNotifyCfaModuleUpdate Returned FAILURE\r\n");
                        break;
                    }

#endif /* CHASSIS Redundancy */
#endif /* STACKING_WANTED */
#ifdef RM_WANTED
                    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
                    {
                        RmProcessInfoFromMbsm (i4SlotId, RM_ATTACH);

                        if (MBSM_SELF_SLOT_ID () == i4SlotId)
                        {
                            /* During self attach bring up stack interfaces */
                            CfaIfmBringupAllStackInterfaces ();
                        }
                    }
#endif
                    if ((u4StackingModel == ISS_CTRL_PLANE_STACKING_MODEL) ||
                        (u4StackingModel == ISS_DISS_STACKING_MODEL))
                    {
                        if (MBSM_SLOT_INDEX_RM_PEER_UP_MSG_TYPE (i4SlotId) !=
                            OSIX_TRUE)
                        {
                            if (MbsmWaitForSelfAttachCompletion (i4SlotId) ==
                                MBSM_SUCCESS)
                            {
                                break;
                            }
                        }
                    }
                    break;

                case MBSM_MSG_SYNC_ACK:
                    /* The Port Creation/Deletion Events are handled 
                     * at all the Protocols synchronously and now
                     * we can Update to protocols for Programming the H/W.
                     */

                    pMsg = ((tMbsmMsg *) pBuf)->uMsg.au1Data;

                    if (!pMsg)
                    {
                        MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN,
                                  "NULL Entry - Cannot extract SlotId\n");
                        break;
                    }

                    if ((((tMbsmSyncAckMsg *) pMsg)->i4RetStatus) ==
                        MBSM_FAILURE)
                    {
                        MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN,
                                  "NULL Entry - Cannot extract SlotId\n");
                        break;
                    }

                    i4SlotId = ((tMbsmSyncAckMsg *) pMsg)->i4SlotId;

                    if (!MBSM_IS_SLOT_ID_VALID (i4SlotId))
                    {
                        MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN,
                                  "Message Error - Invalid SlotId\n");
                        break;
                    }

                    /* Already Notified the Protocols about
                     * Port Status. So proceed to Initiate
                     * NP Programming.
                     */

                     if (gi4MibResStatus == MIB_RESTORE_IN_PROGRESS)
                     {
                         MBSM_UNLOCK ();
                         MbsmWaitForRestorationComplete ();
                         MBSM_LOCK ();
                     }

                    i4ProtoCookie = MBSM_COOKIE_START_INDEX;

                    /* Notify protocols using handshake mechanism for
                     * hardware configurations  */
                    if (MbsmNotifyLcUpdate (i4SlotId, i4ProtoCookie)
                        == MBSM_FAILURE)
                    {
                        i4ProtoCookie = MBSM_PROTO_COOKIE_NULL;
                    }
                    break;

                case MBSM_MSG_HW_LC_REMOVE:

                    pMsg = ((tMbsmMsg *) pBuf)->uMsg.au1Data;

                    if (MbsmGetSlotIdFromMsg ((tMbsmHwMsg *) pMsg, &i4SlotId)
                        != MBSM_SUCCESS)
                    {
                        MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN,
                                  "Message Error - Cannot identify SlotId\n");
                        break;
                    }
                    MBSM_DBG_ARG1 (MBSM_PROTO_TRC, MBSM_MAIN,
                                   "RECV_EVENT: MBSM_MSG_HW_LC_REMOVE for Slot %d\r\n",
                                   i4SlotId);
                    if (!MBSM_IS_SLOT_ID_VALID (i4SlotId))
                    {
                        MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN,
                                  "Message Error - Invalid SlotId\n");
                        break;
                    }
                    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
                    {
#ifdef RM_WANTED
                        RmProcessInfoFromMbsm (i4SlotId, RM_DETACH);
#endif
                        if (MBSM_SELF_SLOT_ID () == i4SlotId)
                        {
                            /* During self detach bring up stack interfaces */
                            CfaIfmBringupAllStackInterfaces ();
                        }
                    }
                    if (MbsmUpdateSlotMapEntryStatus
                        (&((tMbsmHwMsg *) pMsg)->FirstEntry,
                         MBSM_STATUS_NP) == MBSM_FAILURE)
                    {
                        MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN,
                                  "Message Error - Cannot identify SlotId\n");
                        break;
                    }
                    if (((tMbsmHwMsg *) pMsg)->i4NumEntries != 0)
                    {
                        /* FALSE indicates this is a Removal event */
                        if (MbsmUpdateTables
                            (&((tMbsmHwMsg *) pMsg)->FirstEntry,
                             MBSM_LINE_CARD, MBSM_FALSE) == MBSM_FAILURE)
                        {
                            break;
                        }
                        /* Generate Trap */
                        MbsmSnmpSendTrap (MBSM_CARD_REMOVE_TRAP,
                                          (INT1 *) MBSM_CARD_REMOVE_TRAP_OID,
                                          MBSM_TRAPS_OID_LEN, &i4SlotId);
#ifdef RM_WANTED
                        if (RmGetNodeState () != RM_ACTIVE)
                        {
                            /* Only Active should process the REMOVAL event */
                            MBSM_DBG_ARG1 (MBSM_TRC_ERR, MBSM_MAIN,
                                           "LC Detach is received for Slot %d "
                                           "At Standby CFM\r\n", i4SlotId);
                            break;
                        }
#endif
                        if (MbsmNotifyCfaModuleUpdate (i4SlotId)
                            == MBSM_FAILURE)
                        {
                            break;
                        }
                        if (u4StackingModel != ISS_DISS_STACKING_MODEL)
                        {

                            /* Initialize the Cookie with the First Protocol
                             * in tMbsmProtoCookie
                             */
                            i4ProtoCookie = MBSM_COOKIE_START_INDEX;

                            /* Notify protocols using handshake mechanism for
                             * hardware configurations */
                            if (MbsmNotifyLcUpdate (i4SlotId, i4ProtoCookie)
                                == MBSM_FAILURE)
                            {
                                i4ProtoCookie = MBSM_PROTO_COOKIE_NULL;
                            }
                        }
#ifdef RM_WANTED
#ifdef L2RED_WANTED
                        MbsmRedSendSyncMessages (i4SlotId, MBSM_RED_DETACH_MSG);
#endif
#endif
                    }
                    break;

                case MBSM_MSG_PROTO_ACK:

                    pMsg = ((tMbsmMsg *) pBuf)->uMsg.au1Data;

                    if (MbsmHandleProtoAckMsg ((tMbsmProtoAckMsg *) pMsg,
                                               &i4ProtoCookie) == MBSM_FAILURE)
                    {
                        i4ProtoCookie = MBSM_PROTO_COOKIE_NULL;
                    }
                    gu4CurrentProtoCookie = i4ProtoCookie;
                    /*Update portListStatus From  PortList */
                    if (i4ProtoCookie == MBSM_PROTO_COOKIE_NULL)
                    {
                        i4SlotId = ((tMbsmProtoAckMsg *) pMsg)->i4SlotId;

                        MEMCPY (MBSM_PORT_INDEX_PORTLISTSTATUS (i4SlotId),
                                MBSM_PORT_INDEX_PORTLIST (i4SlotId),
                                sizeof (tPortList));
                    }
                    break;
                case MBSM_MSG_HW_CC_INSERT:

                    pMsg = ((tMbsmMsg *) pBuf)->uMsg.au1Data;

                    if (MbsmGetSlotIdFromMsg ((tMbsmHwMsg *) pMsg, &i4SlotId)
                        != MBSM_SUCCESS)
                    {
                        MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN,
                                  "Message Error - Cannot identify SlotId\n");
                        break;
                    }

                    MBSM_DBG_ARG1 (MBSM_PROTO_TRC, MBSM_MAIN,
                                   "RECV_EVENT: MBSM_MSG_HW_CC_INSERT for Slot %d\r\n",
                                   i4SlotId);

                    if (!MBSM_IS_SLOT_ID_VALID (i4SlotId))
                    {
                        MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN,
                                  "Message Error - Invalid SlotId\n");
                        break;
                    }
                    /* Removed Check - to see Mbsm Slot Status is already Active.
                     * This is not needed since Hw will not give two or more 
                     * ATTACH for the same slot.
                     */
                    /* if (MBSM_SLOT_INDEX_STATUS (i4SlotId) == MBSM_STATUS_ACTIVE)
                     * {
                     * #ifdef SYSLOG_WANTED
                     * SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, MBSM_SYSLOG_ID,
                     *                 "Slot->%d: Insertion message for "
                     *                 "loaded slot!!!", i4SlotId));
                     * #endif
                     * break;
                     * }
                     */
                    if (((tMbsmHwMsg *) pMsg)->i4NumEntries != 0)
                    {
                        /* TRUE indicates this is a Insertion event */
                        if (MbsmUpdateTables
                            (&((tMbsmHwMsg *) pMsg)->FirstEntry,
                             MBSM_CONTROL_CARD, MBSM_TRUE) == MBSM_FAILURE)
                        {
                            break;
                        }

                        if (MbsmMbsmNpProcCardInsertForLoadSharing
                            (MBSM_MSG_HW_CC_INSERT) == FNP_FAILURE)
                        {
                            MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN,
                                      "Process Control card insert event for load-sharing -- Failed\r\n");
                        }
                    }
#ifdef RM_WANTED
                    if (MbsmRedHandleCCInsertEvent (i4SlotId) == MBSM_FAILURE)
                    {
                        break;
                    }
#else
                    i4ProtoCookie = MBSM_COOKIE_START_INDEX;

                    /* Notify protocols using handshake mechanism for
                     * hardware configurations  */
                    if (MbsmNotifyLcUpdate (i4SlotId, i4ProtoCookie)
                        == MBSM_FAILURE)
                    {
                        i4ProtoCookie = MBSM_PROTO_COOKIE_NULL;
                    }
#endif
                    /* Generate Trap */
                    MbsmSnmpSendTrap (MBSM_CARD_INSERT_TRAP,
                                      (INT1 *) MBSM_CARD_INSERT_TRAP_OID,
                                      MBSM_TRAPS_OID_LEN, &i4SlotId);
                    if ((u4StackingModel == ISS_CTRL_PLANE_STACKING_MODEL) ||
                        (u4StackingModel == ISS_DISS_STACKING_MODEL))
                    {
                        if (MbsmWaitForSelfAttachCompletion (i4SlotId) ==
                            MBSM_SUCCESS)
                        {
                            break;
                        }
                    }
                    break;

                case MBSM_MSG_HW_CC_REMOVE:

                    pMsg = ((tMbsmMsg *) pBuf)->uMsg.au1Data;

                    if (MbsmGetSlotIdFromMsg ((tMbsmHwMsg *) pMsg, &i4SlotId)
                        != MBSM_SUCCESS)
                    {
                        MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN,
                                  "Message Error - Cannot identify SlotId\n");
                        break;
                    }
                    MBSM_DBG_ARG1 (MBSM_PROTO_TRC, MBSM_MAIN,
                                   "RECV_EVENT: MBSM_MSG_HW_CC_REMOVE for Slot %d\r\n",
                                   i4SlotId);

                    if (!MBSM_IS_SLOT_ID_VALID (i4SlotId))
                    {
                        MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN,
                                  "Message Error - Invalid SlotId\n");
                        break;
                    }
                    if (((tMbsmHwMsg *) pMsg)->i4NumEntries != 0)
                    {
#ifdef RM_WANTED
                        if (RmGetNodeState () != RM_ACTIVE)
                        {
                            /* Only Active should process the REMOVAL event */
                            MBSM_DBG_ARG1 (MBSM_TRC_ERR, MBSM_MAIN,
                                           "CC Detach is received for Slot %d "
                                           "At Standby CFM\r\n", i4SlotId);
                            break;
                        }
#endif
                        /* TRUE indicates this is a Insertion event */
                        if (MbsmUpdateTables
                            (&((tMbsmHwMsg *) pMsg)->FirstEntry,
                             MBSM_CONTROL_CARD, MBSM_FALSE) == MBSM_FAILURE)
                        {
                            break;
                        }

                        if (MbsmMbsmNpProcCardInsertForLoadSharing
                            (MBSM_MSG_HW_CC_REMOVE) == FNP_FAILURE)
                        {
                            MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN,
                                      "Process Control card remove event for load-sharing -- Failed\r\n");
                        }

                        i4ProtoCookie = MBSM_COOKIE_START_INDEX;

                        /* Notify protocols using handshake mechanism for
                         * hardware configurations  */
                        if (MbsmNotifyLcUpdate (i4SlotId, i4ProtoCookie)
                            == MBSM_FAILURE)
                        {
                            i4ProtoCookie = MBSM_PROTO_COOKIE_NULL;
                        }
                        /* Generate Trap */
                        MbsmSnmpSendTrap (MBSM_CARD_REMOVE_TRAP,
                                          (INT1 *) MBSM_CARD_REMOVE_TRAP_OID,
                                          MBSM_TRAPS_OID_LEN, &i4SlotId);
#ifdef RM_WANTED
#ifdef L2RED_WANTED
                        MbsmRedSendSyncMessages (i4SlotId, MBSM_RED_DETACH_MSG);
#endif
#endif
                    }
                    break;

#ifdef NPAPI_WANTED
                case MBSM_MSG_LOAD_SHARING_ENABLE:
                case MBSM_MSG_LOAD_SHARING_DISABLE:
                    if (i4MsgType == MBSM_MSG_LOAD_SHARING_ENABLE)
                    {
                        u1LoadSharingFlag = MBSM_LOAD_SHARING_ENABLE;
                    }
                    else
                    {
                        u1LoadSharingFlag = MBSM_LOAD_SHARING_DISABLE;
                    }

                    if (MbsmUpdtTblsForLoadSharing (u1LoadSharingFlag) ==
                        MBSM_FAILURE)
                    {
                        MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN,
                                  "ERROR: MbsmUpdtTblsForLoadSharing -- failed \r\n");
                    }
                    break;
#endif /* NPAPI_WANTED */
#ifdef RM_WANTED
                case MBSM_RM_QMSG:

                    MbsmProcessRmEvent (pBuf);

                    if (MBSM_RELEASE_CTRLQ_MEM_BLOCK (pBuf) != MEM_SUCCESS)
                    {
                        MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN,
                                  "ERROR: Release of CTRLQ Msg Memory Block FAILED!!! \n");
                    }
                    pBuf = NULL;
                    break;
#endif
                default:
                    break;
            }
            if (pBuf != NULL)
            {
                MemReleaseMemBlock (MBSM_MSG_QUEUE_MEMPOOL_ID, (UINT1 *) pBuf);
                pBuf = NULL;
            }
            MBSM_UNLOCK ();
        }
    }

    return;
}

/*****************************************************************************
 *
 *    Function Name        : MbsmGetSlotIdFromMsg
 *
 *    Description          : This function extracts the Slot Id information
 *                           from the message passed by the hardware.        
 *
 *    Input(s)             : pMsg - Pointer to the message.             
 *
 *    Output(s)            : pi4SlotId - Slot Id extracted from message.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion     : None.
 *
 *    Returns              : MBSM_SUCCESS or MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmGetSlotIdFromMsg (tMbsmHwMsg * pMsg, INT4 *pi4SlotId)
{
    if (!pMsg)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN,
                  "NULL Entry - Cannot extract SlotId\n");
        return (MBSM_FAILURE);
    }

    *pi4SlotId = pMsg->FirstEntry.i4SlotId;

    return (MBSM_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmIsPreConfigured
 *
 *    Description          : This function checks if the given SlotId is   
 *                           pre-configured and returns the same.            
 *
 *    Input(s)             : i4SlotId - Slot Id to be checked.          
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gaMbsmSlotMapTable
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion     : None.
 *
 *    Returns              : MBSM_TRUE or MBSM_FALSE
 *
 *****************************************************************************/
INT4
MbsmIsPreConfigured (INT4 i4SlotId)
{
    if (!MBSM_IS_SLOT_ID_VALID (i4SlotId))
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN,
                  "Error in MbsmIsPreConfigured - Invalid SlotId - FAILURE.\n");
        return (MBSM_FALSE);
    }

    if (MBSM_SLOT_INDEX_ISPRECONF (i4SlotId) == MBSM_TRUE)
    {
        return MBSM_TRUE;
    }
    else
    {
        return MBSM_FALSE;
    }
}

/*****************************************************************************
 *
 *    Function Name        : MbsmShutdown
 *
 *    Description          : This function takes care of shutting down the 
 *                           MBSM.                                           
 *
 *    Input(s)             : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion     : None.
 *
 *    Returns              : MBSM_TRUE or MBSM_FALSE
 *
 *****************************************************************************/
VOID
MbsmShutdown (VOID)
{
    UINT1              *pBuf = NULL;
    INT4                i4MsgType = 0;
    tOsixMsg           *pQMsg = NULL;
#ifdef RM_WANTED
    tMbsmMsg           *pMsg = NULL;
#endif

    MbsmSizingMemDeleteMemPools ();
    /* delete the Queue after dequeuing all packets from the queue */
    while (OsixReceiveFromQ (SELF, (const UINT1 *) MBSM_MSG_QUEUE_NAME,
                             OSIX_NO_WAIT, 0, &pQMsg) == OSIX_SUCCESS)
    {
        pBuf = (UINT1 *) pQMsg;

        if (MbsmGetMsgType ((UINT1 *) pBuf, &i4MsgType) == MBSM_FAILURE)
        {
            MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN,
                      "Message Error - Cannot identify message type\n");
            continue;
        }

#ifdef RM_WANTED
        pMsg = (tMbsmMsg *) pBuf;

        if (i4MsgType == MBSM_RM_QMSG)
        {
            if ((pMsg->uMsg.RmMsg.u1Event == RM_MESSAGE) &&
                (pMsg->uMsg.RmMsg.pFrame != NULL))
            {
                RM_FREE (pMsg->uMsg.RmMsg.pFrame);
            }
            else if (((pMsg->uMsg.RmMsg.u1Event == RM_STANDBY_UP) ||
                      (pMsg->uMsg.RmMsg.u1Event == RM_STANDBY_DOWN)) &&
                     (pMsg->uMsg.RmMsg.pFrame != NULL))
            {
                RmReleaseMemoryForMsg ((UINT1 *) pMsg->uMsg.RmMsg.pFrame);
            }

            if (MBSM_RELEASE_CTRLQ_MEM_BLOCK (pMsg) != MEM_SUCCESS)
            {
                MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN,
                          "MBSM: CtrlQ Memory Block Release FAILED\n");
            }
            pMsg = NULL;
        }
        else
#endif
        {
            MemReleaseMemBlock (MBSM_MSG_QUEUE_MEMPOOL_ID, (UINT1 *) pBuf);
        }
    }

    if (OsixDeleteQ (SELF, (const UINT1 *) MBSM_MSG_QUEUE_NAME) != OSIX_SUCCESS)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN,
                  "Error in CfaShutDown - Queue Deletion Failed - FAILURE.\n");
        return;
    }
    MBSM_QUEUE_ID = 0;

    MbsmSizingMemDeleteMemPools ();

    return;
}

/*****************************************************************************
 *    Function Name        : MbsmAddCardTypeEntry
 *
 *    Description          : This function adds a new entry into the Card  
 *                           Type table.                                     
 *
 *    Input(s)             : pMbsmLcTypeInfo -  Pointer to the structure
 *                           containing the information about the new entry.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gaMbsmLcTypeTable 
 *
 *    Global Variables Modified : gaMbsmLcTypeTable 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion     : None.
 *
 *    Returns              : MBSM_SUCCESS or MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmAddCardTypeEntry (tMbsmCardInfo * pCardInfo)
{
    UINT4               u4Index;

    if (!pCardInfo)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN, "NULL Entry - Cannot add entry\n");
        return (MBSM_FAILURE);
    }

    if (MbsmGetFreeCardTypeTableIndex (&u4Index) == MBSM_FAILURE)
    {
        return (MBSM_FAILURE);
    }

    if (MbsmCreateCardTypeEntry (u4Index) == MBSM_FAILURE)
    {
        return (MBSM_FAILURE);
    }

    MBSM_CARD_TYPE_INFO_CARDINDEX (u4Index) = u4Index;

    SNPRINTF ((char *) MBSM_CARD_TYPE_INFO_CARDNAME (u4Index),
              MBSM_MAX_CARD_NAME_LEN + 1, "%s", pCardInfo->ai1CardName);

    MBSM_CARD_TYPE_INFO_NUMPORTS (u4Index) = pCardInfo->u4NumPorts;

    MBSM_CARD_TYPE_INFO_ROWSTATUS (u4Index) = ACTIVE;

    if (MbsmAddCardTypePortInfo (pCardInfo) != MBSM_SUCCESS)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN,
                  "Invalid POrt Information - Cannot add entry\r\n");
        return (MBSM_FAILURE);
    }

    return (MBSM_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmAddCardTypePortInfo
 *
 *    Description          : This function updates the port information on the
 *                           Card Type entry.                                
 *
 *    Input(s)             : pMbsmLcTypeInfo -  Pointer to the structure
 *                           containing the information about the entry.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gaMbsmLcTypeTable 
 *
 *    Global Variables Modified : gaMbsmLcTypeTable 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion     : None.
 *
 *    Returns              : MBSM_SUCCESS or MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmAddCardTypePortInfo (tMbsmCardInfo * pCardInfo)
{
    UINT4               u4Index;
    UINT4               u4Type;
    INT4                i4LoopIdx;

    if (!pCardInfo)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN, "NULL Entry - Cannot add entry\n");
        return (MBSM_FAILURE);
    }

    if (MbsmGetCardIndexFromCardName (pCardInfo->ai1CardName, (INT4 *) &u4Index)
        == MBSM_FAILURE)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN,
                  "Unknown Card Type - Cannot add entry\n");
        return (MBSM_FAILURE);
    }

    MBSM_CARD_TYPE_INFO_NUMPORTS (u4Index) = pCardInfo->u4NumPorts;

    for (i4LoopIdx = 0; i4LoopIdx < MBSM_MAX_PORT_SPEED_TYPES; i4LoopIdx++)
    {
        if (STRLEN (pCardInfo->PortSpeed[i4LoopIdx].au1Type) == 0)
        {
            continue;
        }
        if (MbsmGetPortSpeedType
            (pCardInfo->PortSpeed[i4LoopIdx].au1Type, &u4Type) == MBSM_SUCCESS)
        {
            ConvertStrToPortList (pCardInfo->PortSpeed[i4LoopIdx].au1PortList,
                                  MBSM_CARD_TYPE_INFO_PORTMAP (u4Index).
                                  SpeedTable[u4Type],
                                  sizeof (tMbsmCardPortList),
                                  pCardInfo->u4NumPorts);
        }
        if (MbsmGetPortEncapType
            (pCardInfo->PortSpeed[i4LoopIdx].au1Type, &u4Type) == MBSM_SUCCESS)
        {
            switch (u4Type)
            {
                case MBSM_ENCAP_ENETV2:
                    /* Fall through. Currently only ENET is supported */
                default:
                    u4Type = CFA_ENET;
                    break;
            }

            ConvertStrToPortList (pCardInfo->PortSpeed[i4LoopIdx].au1PortList,
                                  MBSM_CARD_TYPE_INFO_PORTMAP (u4Index).
                                  IfTypeTable[u4Type],
                                  sizeof (tMbsmCardPortList),
                                  pCardInfo->u4NumPorts);
        }
        if (MbsmGetPortMtuType
            (pCardInfo->PortSpeed[i4LoopIdx].au1Type, &u4Type) == MBSM_SUCCESS)
        {
            ConvertStrToPortList (pCardInfo->PortSpeed[i4LoopIdx].au1PortList,
                                  MBSM_CARD_TYPE_INFO_PORTMAP (u4Index).
                                  MtuTable[u4Type], sizeof (tMbsmCardPortList),
                                  pCardInfo->u4NumPorts);
        }
    }

    MBSM_CARD_TYPE_INFO_ROWSTATUS (u4Index) = ACTIVE;

    return (MBSM_SUCCESS);
}

/*****************************************************************************
 * Function Name : MbsmUpdtTblsForLoadSharing
 * Description   : Function to enable load-sharing which programs the h/w
 *                 table to share the load to both CFMs.
 * Input(s)      : None
 * Output(s)     : None
 * Returns       : MBSM_SUCCESS/MBSM_FAILURE
 *****************************************************************************/
INT4
MbsmUpdtTblsForLoadSharing (UINT1 u1Flag)
{
#ifdef NPAPI_WANTED
    if (MbsmMbsmNpUpdtHwTblForLoadSharing (u1Flag) == FNP_FAILURE)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN,
                  "ERROR: MbsmMbsmNpUpdtHwTblForLoadSharing failed\r\n");
        return (MBSM_FAILURE);
    }

    if (MbsmNotifyLoadSharingStatusChgToProto (u1Flag) == MBSM_FAILURE)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_MAIN,
                  "ERROR: MbsmNotifyLoadSharingStatusChg failed\r\n");
        return (MBSM_FAILURE);
    }
#endif /* NPAPI_WANTED */

    return (MBSM_SUCCESS);
}

#ifdef MSR_WANTED
/*****************************************************************************
 * Function Name : MbsmWaitForMSRCompleteEvent 
 * Description   : This Function waits for MSR complete event to be received
 * Input(s)      : None
 * Output(s)     : None
 * Returns       : None 
 *****************************************************************************/
VOID
MbsmWaitForMSRCompleteEvent (VOID)
{
    UINT4               u4EventMask;

    MBSM_FOREVER ()
    {

        MBSM_RECV_EVENT (MBSM_TASK_ID, MSR_RESTORE_COMPLETE_EVENT,
                         MBSM_EVENT_WAIT_FLAGS, &u4EventMask);
        if (u4EventMask & MSR_RESTORE_COMPLETE_EVENT)
        {
            break;
        }
    }
    return;
}
#endif
