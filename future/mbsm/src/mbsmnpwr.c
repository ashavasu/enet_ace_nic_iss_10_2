
/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: mbsmnpwr.c,v 1.1 2013/03/19 12:25:49 siva Exp $
*
* Description: This file contains the MBSM NPAPI related wrapper routines.
*************************************************************************/
#ifndef _MBSM_NPWR_C_
#define _MBSM_NPWR_C_

#include "nputil.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : MbsmNpWrHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tMbsmNpModInfo
 *                                                                          
 *    Input(s)            : FsHwNpParam of type tfsHwNp                     
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
MbsmNpWrHwProgram (tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tMbsmNpModInfo     *pMbsmNpModInfo = NULL;

    if (NULL == pFsHwNp)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pMbsmNpModInfo = &(pFsHwNp->MbsmNpModInfo);

    if (NULL == pMbsmNpModInfo)
    {
        return FNP_FAILURE;
    }

    switch (u4Opcode)
    {
#ifdef MBSM_WANTED
#ifdef L2RED_WANTED
        case MBSM_NP_PROC_RM_NODE_TRANSITION:
        {
            tMbsmNpWrMbsmNpProcRmNodeTransition *pEntry = NULL;
            pEntry = &pMbsmNpModInfo->MbsmNpMbsmNpProcRmNodeTransition;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                MbsmNpProcRmNodeTransition (pEntry->i4Event,
                                            pEntry->u1PrevState,
                                            pEntry->u1State);
            break;
        }
        case MBSM_INIT_NP_INFO_ON_STANDBY_TO_ACTIVE:
        {
            tMbsmNpWrMbsmInitNpInfoOnStandbyToActive *pEntry = NULL;
            pEntry = &pMbsmNpModInfo->MbsmNpMbsmInitNpInfoOnStandbyToActive;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = MbsmInitNpInfoOnStandbyToActive (pEntry->pSlotInfo);
            break;
        }
        case MBSM_NP_GET_SLOT_INFO:
        {
            tMbsmNpWrMbsmNpGetSlotInfo *pEntry = NULL;
            pEntry = &pMbsmNpModInfo->MbsmNpMbsmNpGetSlotInfo;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = MbsmNpGetSlotInfo (pEntry->i4SlotId, pEntry->pHwMsg);
            break;
        }
#endif /* L2RED_WANTED */
        case MBSM_NP_INIT_HW_TOPO_DISC:
        {
            tMbsmNpWrMbsmNpInitHwTopoDisc *pEntry = NULL;
            pEntry = &pMbsmNpModInfo->MbsmNpMbsmNpInitHwTopoDisc;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                MbsmNpInitHwTopoDisc (pEntry->i4SlotId, pEntry->i1NodeState);
            break;
        }
        case MBSM_NP_CLEAR_HW_TBL:
        {
            tMbsmNpWrMbsmNpClearHwTbl *pEntry = NULL;
            pEntry = &pMbsmNpModInfo->MbsmNpMbsmNpClearHwTbl;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = MbsmNpClearHwTbl (pEntry->i4SlotId);
            break;
        }
        case MBSM_NP_PROC_CARD_INSERT_FOR_LOAD_SHARING:
        {
            tMbsmNpWrMbsmNpProcCardInsertForLoadSharing *pEntry = NULL;
            pEntry = &pMbsmNpModInfo->MbsmNpMbsmNpProcCardInsertForLoadSharing;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = MbsmNpProcCardInsertForLoadSharing (pEntry->i4MsgType);
            break;
        }
        case MBSM_NP_UPDT_HW_TBL_FOR_LOAD_SHARING:
        {
            tMbsmNpWrMbsmNpUpdtHwTblForLoadSharing *pEntry = NULL;
            pEntry = &pMbsmNpModInfo->MbsmNpMbsmNpUpdtHwTblForLoadSharing;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = MbsmNpUpdtHwTblForLoadSharing (pEntry->u1Flag);
            break;
        }
        case MBS_NP_SET_LOAD_SHARING_STATUS:
        {
            tMbsmNpWrMbsNpSetLoadSharingStatus *pEntry = NULL;
            pEntry = &pMbsmNpModInfo->MbsmNpMbsNpSetLoadSharingStatus;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = MbsNpSetLoadSharingStatus (pEntry->i4LoadSharingFlag);
            break;
        }
        case MBSM_NP_GET_CARD_TYPE_TABLE:
        {
            tMbsmNpWrMbsmNpGetCardTypeTable *pEntry = NULL;
            pEntry = &pMbsmNpModInfo->MbsmNpMbsmNpGetCardTypeTable;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                MbsmNpGetCardTypeTable (pEntry->i4LoopIdx,
                                        pEntry->pMbsmCardInfo);
            break;
        }
        case MBSM_NP_HANDLE_NODE_TRANSITION:
        {
            tMbsmNpWrMbsmNpHandleNodeTransition *pEntry = NULL;
            pEntry = &pMbsmNpModInfo->MbsmNpMbsmNpHandleNodeTransition;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                MbsmNpHandleNodeTransition (pEntry->i4Event,
                                            pEntry->u1PrevState,
                                            pEntry->u1State);
            break;
        }
        case MBSM_NP_TX_ON_STACK_INTERFACE:
        {
            tMbsmNpWrMbsmNpTxOnStackInterface *pEntry = NULL;
            pEntry = &pMbsmNpModInfo->MbsmNpMbsmNpTxOnStackInterface;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                MbsmNpTxOnStackInterface (pEntry->pu1Pkt, pEntry->i4PktSize);
            break;
        }
        case MBSM_NP_GET_STACK_MAC:
        {
            tMbsmNpWrMbsmNpGetStackMac *pEntry = NULL;
            pEntry = &pMbsmNpModInfo->MbsmNpMbsmNpGetStackMac;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            MbsmNpGetStackMac (pEntry->u4SlotId, pEntry->pu1MacAddr);
            break;
        }
#endif /* MBSM_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

#endif /* _MBSM_NPWR_C_ */
