/* $Id: fsmbsmnc.c,v 1.1 2016/01/11 13:36:06 siva Exp $
    ISS Wrapper module
    module ARICENT-CHASSIS-MIB

 */
# include  "lr.h"
# include  "fssnmp.h"
# include  "mbsminc.h"
# include  "fsmbsmnc.h"

#ifdef NPAPI_WANTED
#include "npapi.h"
#include "mbsnp.h"

#endif

/********************************************************************
* FUNCTION NcMbsmMaxNumOfLCSlotsSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcMbsmMaxNumOfLCSlotsSet (
                INT4 i4MbsmMaxNumOfLCSlots )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetMbsmMaxNumOfLCSlots(
                i4MbsmMaxNumOfLCSlots);

    return i1RetVal;


} /* NcMbsmMaxNumOfLCSlotsSet */

/********************************************************************
* FUNCTION NcMbsmMaxNumOfLCSlotsTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcMbsmMaxNumOfLCSlotsTest (UINT4 *pu4Error,
                INT4 i4MbsmMaxNumOfLCSlots )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2MbsmMaxNumOfLCSlots(pu4Error,
                i4MbsmMaxNumOfLCSlots);

    return i1RetVal;


} /* NcMbsmMaxNumOfLCSlotsTest */

/********************************************************************
* FUNCTION NcMbsmMaxNumOfSlotsSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcMbsmMaxNumOfSlotsSet (
                INT4 i4MbsmMaxNumOfSlots )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetMbsmMaxNumOfSlots(
                i4MbsmMaxNumOfSlots);

    return i1RetVal;


} /* NcMbsmMaxNumOfSlotsSet */

/********************************************************************
* FUNCTION NcMbsmMaxNumOfSlotsTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcMbsmMaxNumOfSlotsTest (UINT4 *pu4Error,
                INT4 i4MbsmMaxNumOfSlots )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2MbsmMaxNumOfSlots(pu4Error,
                i4MbsmMaxNumOfSlots);

    return i1RetVal;


} /* NcMbsmMaxNumOfSlotsTest */

/********************************************************************
* FUNCTION NcMbsmMaxNumOfPortsPerLCSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcMbsmMaxNumOfPortsPerLCSet (
                INT4 i4MbsmMaxNumOfPortsPerLC )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetMbsmMaxNumOfPortsPerLC(
                i4MbsmMaxNumOfPortsPerLC);

    return i1RetVal;


} /* NcMbsmMaxNumOfPortsPerLCSet */

/********************************************************************
* FUNCTION NcMbsmMaxNumOfPortsPerLCTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcMbsmMaxNumOfPortsPerLCTest (UINT4 *pu4Error,
                INT4 i4MbsmMaxNumOfPortsPerLC )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2MbsmMaxNumOfPortsPerLC(pu4Error,
                i4MbsmMaxNumOfPortsPerLC);

    return i1RetVal;


} /* NcMbsmMaxNumOfPortsPerLCTest */

/********************************************************************
* FUNCTION NcMbsmLoadSharingFlagSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcMbsmLoadSharingFlagSet (
                INT4 i4MbsmLoadSharingFlag )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetMbsmLoadSharingFlag(
                i4MbsmLoadSharingFlag);

    return i1RetVal;


} /* NcMbsmLoadSharingFlagSet */

/********************************************************************
* FUNCTION NcMbsmLoadSharingFlagTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcMbsmLoadSharingFlagTest (UINT4 *pu4Error,
                INT4 i4MbsmLoadSharingFlag )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2MbsmLoadSharingFlag(pu4Error,
                i4MbsmLoadSharingFlag);

    return i1RetVal;


} /* NcMbsmLoadSharingFlagTest */

/********************************************************************
* FUNCTION NcMbsmSlotModuleTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcMbsmSlotModuleTypeSet (
                INT4 i4MbsmSlotId,
                INT4 i4MbsmSlotModuleType )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetMbsmSlotModuleType(
                 i4MbsmSlotId,
                i4MbsmSlotModuleType);

    return i1RetVal;


} /* NcMbsmSlotModuleTypeSet */

/********************************************************************
* FUNCTION NcMbsmSlotModuleTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcMbsmSlotModuleTypeTest (UINT4 *pu4Error,
                INT4 i4MbsmSlotId,
                INT4 i4MbsmSlotModuleType )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2MbsmSlotModuleType(pu4Error,
                 i4MbsmSlotId,
                i4MbsmSlotModuleType);

    return i1RetVal;


} /* NcMbsmSlotModuleTypeTest */

/********************************************************************
* FUNCTION NcMbsmSlotModuleStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcMbsmSlotModuleStatusSet (
                INT4 i4MbsmSlotId,
                INT4 i4MbsmSlotModuleStatus )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetMbsmSlotModuleStatus(
                 i4MbsmSlotId,
                i4MbsmSlotModuleStatus);

    return i1RetVal;


} /* NcMbsmSlotModuleStatusSet */

/********************************************************************
* FUNCTION NcMbsmSlotModuleStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcMbsmSlotModuleStatusTest (UINT4 *pu4Error,
                INT4 i4MbsmSlotId,
                INT4 i4MbsmSlotModuleStatus )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2MbsmSlotModuleStatus(pu4Error,
                 i4MbsmSlotId,
                i4MbsmSlotModuleStatus);

    return i1RetVal;


} /* NcMbsmSlotModuleStatusTest */

/********************************************************************
* FUNCTION NcMbsmLCNameSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcMbsmLCNameSet (
                INT4 i4MbsmLCIndex,
                UINT1 *pMbsmLCName )
{

    INT1 i1RetVal;
    tSNMP_OCTET_STRING_TYPE MbsmLCName;
    MEMSET (&MbsmLCName, 0,  sizeof (MbsmLCName));

    MbsmLCName.i4_Length = (INT4) STRLEN (pMbsmLCName);
    MbsmLCName.pu1_OctetList = pMbsmLCName;

    i1RetVal = nmhSetMbsmLCName(
                 i4MbsmLCIndex,
                &MbsmLCName);

    return i1RetVal;


} /* NcMbsmLCNameSet */

/********************************************************************
* FUNCTION NcMbsmLCNameTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcMbsmLCNameTest (UINT4 *pu4Error,
                INT4 i4MbsmLCIndex,
                UINT1 *pMbsmLCName )
{

    INT1 i1RetVal;
    tSNMP_OCTET_STRING_TYPE MbsmLCName;
    MEMSET (&MbsmLCName, 0,  sizeof (MbsmLCName));

    MbsmLCName.i4_Length = (INT4) STRLEN (pMbsmLCName);
    MbsmLCName.pu1_OctetList = pMbsmLCName;

    i1RetVal = nmhTestv2MbsmLCName(pu4Error,
                 i4MbsmLCIndex,
                &MbsmLCName);

    return i1RetVal;


} /* NcMbsmLCNameTest */

/********************************************************************
* FUNCTION NcMbsmLCMaxPortsSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcMbsmLCMaxPortsSet (
                INT4 i4MbsmLCIndex,
                INT4 i4MbsmLCMaxPorts )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetMbsmLCMaxPorts(
                 i4MbsmLCIndex,
                i4MbsmLCMaxPorts);

    return i1RetVal;


} /* NcMbsmLCMaxPortsSet */

/********************************************************************
* FUNCTION NcMbsmLCMaxPortsTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcMbsmLCMaxPortsTest (UINT4 *pu4Error,
                INT4 i4MbsmLCIndex,
                INT4 i4MbsmLCMaxPorts )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2MbsmLCMaxPorts(pu4Error,
                 i4MbsmLCIndex,
                i4MbsmLCMaxPorts);

    return i1RetVal;


} /* NcMbsmLCMaxPortsTest */

/********************************************************************
* FUNCTION NcMbsmLCRowStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcMbsmLCRowStatusSet (
                INT4 i4MbsmLCIndex,
                INT4 i4MbsmLCRowStatus )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetMbsmLCRowStatus(
                 i4MbsmLCIndex,
                i4MbsmLCRowStatus);

    return i1RetVal;


} /* NcMbsmLCRowStatusSet */

/********************************************************************
* FUNCTION NcMbsmLCRowStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcMbsmLCRowStatusTest (UINT4 *pu4Error,
                INT4 i4MbsmLCIndex,
                INT4 i4MbsmLCRowStatus )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2MbsmLCRowStatus(pu4Error,
                 i4MbsmLCIndex,
                i4MbsmLCRowStatus);

    return i1RetVal;


} /* NcMbsmLCRowStatusTest */

/********************************************************************
* FUNCTION NcMbsmLCPortIfTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcMbsmLCPortIfTypeSet (
                INT4 i4MbsmLCIndex,
                INT4 i4MbsmLCPortIndex,
                INT4 i4MbsmLCPortIfType )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetMbsmLCPortIfType(
                 i4MbsmLCIndex,
                 i4MbsmLCPortIndex,
                i4MbsmLCPortIfType);

    return i1RetVal;


} /* NcMbsmLCPortIfTypeSet */

/********************************************************************
* FUNCTION NcMbsmLCPortIfTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcMbsmLCPortIfTypeTest (UINT4 *pu4Error,
                INT4 i4MbsmLCIndex,
                INT4 i4MbsmLCPortIndex,
                INT4 i4MbsmLCPortIfType )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2MbsmLCPortIfType(pu4Error,
                 i4MbsmLCIndex,
                 i4MbsmLCPortIndex,
                i4MbsmLCPortIfType);

    return i1RetVal;


} /* NcMbsmLCPortIfTypeTest */

/********************************************************************
* FUNCTION NcMbsmLCPortSpeedSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcMbsmLCPortSpeedSet (
                INT4 i4MbsmLCIndex,
                INT4 i4MbsmLCPortIndex,
                UINT4 u4MbsmLCPortSpeed )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetMbsmLCPortSpeed(
                 i4MbsmLCIndex,
                 i4MbsmLCPortIndex,
                u4MbsmLCPortSpeed);

    return i1RetVal;


} /* NcMbsmLCPortSpeedSet */

/********************************************************************
* FUNCTION NcMbsmLCPortSpeedTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcMbsmLCPortSpeedTest (UINT4 *pu4Error,
                INT4 i4MbsmLCIndex,
                INT4 i4MbsmLCPortIndex,
                UINT4 u4MbsmLCPortSpeed )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2MbsmLCPortSpeed(pu4Error,
                 i4MbsmLCIndex,
                 i4MbsmLCPortIndex,
                u4MbsmLCPortSpeed);

    return i1RetVal;


} /* NcMbsmLCPortSpeedTest */

/********************************************************************
* FUNCTION NcMbsmLCPortHighSpeedSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcMbsmLCPortHighSpeedSet (
                INT4 i4MbsmLCIndex,
                INT4 i4MbsmLCPortIndex,
                UINT4 u4MbsmLCPortHighSpeed )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetMbsmLCPortHighSpeed(
                 i4MbsmLCIndex,
                 i4MbsmLCPortIndex,
                u4MbsmLCPortHighSpeed);

    return i1RetVal;


} /* NcMbsmLCPortHighSpeedSet */

/********************************************************************
* FUNCTION NcMbsmLCPortHighSpeedTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcMbsmLCPortHighSpeedTest (UINT4 *pu4Error,
                INT4 i4MbsmLCIndex,
                INT4 i4MbsmLCPortIndex,
                UINT4 u4MbsmLCPortHighSpeed )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2MbsmLCPortHighSpeed(pu4Error,
                 i4MbsmLCIndex,
                 i4MbsmLCPortIndex,
                u4MbsmLCPortHighSpeed);

    return i1RetVal;


} /* NcMbsmLCPortHighSpeedTest */

/********************************************************************
* FUNCTION NcMbsmLCConfigCardNameSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcMbsmLCConfigCardNameSet (
                INT4 i4MbsmLCConfigSlotId,
                UINT1 *pMbsmLCConfigCardName )
{

    INT1 i1RetVal;
    tSNMP_OCTET_STRING_TYPE MbsmLCConfigCardName;
    MEMSET (&MbsmLCConfigCardName, 0,  sizeof (MbsmLCConfigCardName));

    MbsmLCConfigCardName.i4_Length = (INT4) STRLEN (pMbsmLCConfigCardName);
    MbsmLCConfigCardName.pu1_OctetList = pMbsmLCConfigCardName;

    i1RetVal = nmhSetMbsmLCConfigCardName(
                 i4MbsmLCConfigSlotId,
                &MbsmLCConfigCardName);

    return i1RetVal;


} /* NcMbsmLCConfigCardNameSet */

/********************************************************************
* FUNCTION NcMbsmLCConfigCardNameTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcMbsmLCConfigCardNameTest (UINT4 *pu4Error,
                INT4 i4MbsmLCConfigSlotId,
                UINT1 *pMbsmLCConfigCardName )
{

    INT1 i1RetVal;
    tSNMP_OCTET_STRING_TYPE MbsmLCConfigCardName;
    MEMSET (&MbsmLCConfigCardName, 0,  sizeof (MbsmLCConfigCardName));

    MbsmLCConfigCardName.i4_Length = (INT4) STRLEN (pMbsmLCConfigCardName);
    MbsmLCConfigCardName.pu1_OctetList = pMbsmLCConfigCardName;

    i1RetVal = nmhTestv2MbsmLCConfigCardName(pu4Error,
                 i4MbsmLCConfigSlotId,
                &MbsmLCConfigCardName);

    return i1RetVal;


} /* NcMbsmLCConfigCardNameTest */

/********************************************************************
* FUNCTION NcMbsmLCConfigStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcMbsmLCConfigStatusSet (
                INT4 i4MbsmLCConfigSlotId,
                INT4 i4MbsmLCConfigStatus )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetMbsmLCConfigStatus(
                 i4MbsmLCConfigSlotId,
                i4MbsmLCConfigStatus);

    return i1RetVal;


} /* NcMbsmLCConfigStatusSet */

/********************************************************************
* FUNCTION NcMbsmLCConfigStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcMbsmLCConfigStatusTest (UINT4 *pu4Error,
                INT4 i4MbsmLCConfigSlotId,
                INT4 i4MbsmLCConfigStatus )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2MbsmLCConfigStatus(pu4Error,
                 i4MbsmLCConfigSlotId,
                i4MbsmLCConfigStatus);

    return i1RetVal;


} /* NcMbsmLCConfigStatusTest */

/* END i_ARICENT_CHASSIS_MIB.c */
