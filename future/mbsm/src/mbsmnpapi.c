
/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: mbsmnpapi.c,v 1.1 2013/03/19 12:25:49 siva Exp $
*
* Description: All  Network Processor API Function calls are done here.
***********************************************************************/
#ifndef _MBSM_NPAPI_C_
#define _MBSM_NPAPI_C_

#include "nputil.h"

#ifdef L2RED_WANTED
/***************************************************************************
 *                                                                          
 *    Function Name       : MbsmMbsmNpProcRmNodeTransition                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes MbsmNpProcRmNodeTransition
 *                                                                          
 *    Input(s)            : Arguments of MbsmNpProcRmNodeTransition
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MbsmMbsmNpProcRmNodeTransition (INT4 i4Event, UINT1 u1PrevState, UINT1 u1State)
{
    tFsHwNp             FsHwNp;
    tMbsmNpModInfo     *pMbsmNpModInfo = NULL;
    tMbsmNpWrMbsmNpProcRmNodeTransition *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MBSM_MOD,    /* Module ID */
                         MBSM_NP_PROC_RM_NODE_TRANSITION,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMbsmNpModInfo = &(FsHwNp.MbsmNpModInfo);
    pEntry = &pMbsmNpModInfo->MbsmNpMbsmNpProcRmNodeTransition;

    pEntry->i4Event = i4Event;
    pEntry->u1PrevState = u1PrevState;
    pEntry->u1State = u1State;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MbsmMbsmInitNpInfoOnStandbyToActive                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes MbsmInitNpInfoOnStandbyToActive
 *                                                                          
 *    Input(s)            : Arguments of MbsmInitNpInfoOnStandbyToActive
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MbsmMbsmInitNpInfoOnStandbyToActive (tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tMbsmNpModInfo     *pMbsmNpModInfo = NULL;
    tMbsmNpWrMbsmInitNpInfoOnStandbyToActive *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MBSM_MOD,    /* Module ID */
                         MBSM_INIT_NP_INFO_ON_STANDBY_TO_ACTIVE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMbsmNpModInfo = &(FsHwNp.MbsmNpModInfo);
    pEntry = &pMbsmNpModInfo->MbsmNpMbsmInitNpInfoOnStandbyToActive;

    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MbsmMbsmNpGetSlotInfo                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes MbsmNpGetSlotInfo
 *                                                                          
 *    Input(s)            : Arguments of MbsmNpGetSlotInfo
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MbsmMbsmNpGetSlotInfo (INT4 i4SlotId, tMbsmHwMsg * pHwMsg)
{
    tFsHwNp             FsHwNp;
    tMbsmNpModInfo     *pMbsmNpModInfo = NULL;
    tMbsmNpWrMbsmNpGetSlotInfo *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MBSM_MOD,    /* Module ID */
                         MBSM_NP_GET_SLOT_INFO,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMbsmNpModInfo = &(FsHwNp.MbsmNpModInfo);
    pEntry = &pMbsmNpModInfo->MbsmNpMbsmNpGetSlotInfo;

    pEntry->i4SlotId = i4SlotId;
    pEntry->pHwMsg = pHwMsg;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
#endif /* L2RED_WANTED */

/***************************************************************************
 *                                                                          
 *    Function Name       : MbsmMbsmNpInitHwTopoDisc                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes MbsmNpInitHwTopoDisc
 *                                                                          
 *    Input(s)            : Arguments of MbsmNpInitHwTopoDisc
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MbsmMbsmNpInitHwTopoDisc (INT4 i4SlotId, INT1 i1NodeState)
{
    tFsHwNp             FsHwNp;
    tMbsmNpModInfo     *pMbsmNpModInfo = NULL;
    tMbsmNpWrMbsmNpInitHwTopoDisc *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MBSM_MOD,    /* Module ID */
                         MBSM_NP_INIT_HW_TOPO_DISC,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMbsmNpModInfo = &(FsHwNp.MbsmNpModInfo);
    pEntry = &pMbsmNpModInfo->MbsmNpMbsmNpInitHwTopoDisc;

    pEntry->i4SlotId = i4SlotId;
    pEntry->i1NodeState = i1NodeState;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MbsmMbsmNpClearHwTbl                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes MbsmNpClearHwTbl
 *                                                                          
 *    Input(s)            : Arguments of MbsmNpClearHwTbl
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MbsmMbsmNpClearHwTbl (INT4 i4SlotId)
{
    tFsHwNp             FsHwNp;
    tMbsmNpModInfo     *pMbsmNpModInfo = NULL;
    tMbsmNpWrMbsmNpClearHwTbl *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MBSM_MOD,    /* Module ID */
                         MBSM_NP_CLEAR_HW_TBL,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMbsmNpModInfo = &(FsHwNp.MbsmNpModInfo);
    pEntry = &pMbsmNpModInfo->MbsmNpMbsmNpClearHwTbl;

    pEntry->i4SlotId = i4SlotId;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MbsmMbsmNpProcCardInsertForLoadSharing                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes MbsmNpProcCardInsertForLoadSharing
 *                                                                          
 *    Input(s)            : Arguments of MbsmNpProcCardInsertForLoadSharing
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MbsmMbsmNpProcCardInsertForLoadSharing (INT4 i4MsgType)
{
    tFsHwNp             FsHwNp;
    tMbsmNpModInfo     *pMbsmNpModInfo = NULL;
    tMbsmNpWrMbsmNpProcCardInsertForLoadSharing *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MBSM_MOD,    /* Module ID */
                         MBSM_NP_PROC_CARD_INSERT_FOR_LOAD_SHARING,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMbsmNpModInfo = &(FsHwNp.MbsmNpModInfo);
    pEntry = &pMbsmNpModInfo->MbsmNpMbsmNpProcCardInsertForLoadSharing;

    pEntry->i4MsgType = i4MsgType;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MbsmMbsmNpUpdtHwTblForLoadSharing                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes MbsmNpUpdtHwTblForLoadSharing
 *                                                                          
 *    Input(s)            : Arguments of MbsmNpUpdtHwTblForLoadSharing
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MbsmMbsmNpUpdtHwTblForLoadSharing (UINT1 u1Flag)
{
    tFsHwNp             FsHwNp;
    tMbsmNpModInfo     *pMbsmNpModInfo = NULL;
    tMbsmNpWrMbsmNpUpdtHwTblForLoadSharing *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MBSM_MOD,    /* Module ID */
                         MBSM_NP_UPDT_HW_TBL_FOR_LOAD_SHARING,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMbsmNpModInfo = &(FsHwNp.MbsmNpModInfo);
    pEntry = &pMbsmNpModInfo->MbsmNpMbsmNpUpdtHwTblForLoadSharing;

    pEntry->u1Flag = u1Flag;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MbsmMbsNpSetLoadSharingStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes MbsNpSetLoadSharingStatus
 *                                                                          
 *    Input(s)            : Arguments of MbsNpSetLoadSharingStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MbsmMbsNpSetLoadSharingStatus (INT4 i4LoadSharingFlag)
{
    tFsHwNp             FsHwNp;
    tMbsmNpModInfo     *pMbsmNpModInfo = NULL;
    tMbsmNpWrMbsNpSetLoadSharingStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MBSM_MOD,    /* Module ID */
                         MBS_NP_SET_LOAD_SHARING_STATUS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMbsmNpModInfo = &(FsHwNp.MbsmNpModInfo);
    pEntry = &pMbsmNpModInfo->MbsmNpMbsNpSetLoadSharingStatus;

    pEntry->i4LoadSharingFlag = i4LoadSharingFlag;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MbsmMbsmNpGetCardTypeTable                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes MbsmNpGetCardTypeTable
 *                                                                          
 *    Input(s)            : Arguments of MbsmNpGetCardTypeTable
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MbsmMbsmNpGetCardTypeTable (INT4 i4LoopIdx, tMbsmCardInfo * pMbsmCardInfo)
{
    tFsHwNp             FsHwNp;
    tMbsmNpModInfo     *pMbsmNpModInfo = NULL;
    tMbsmNpWrMbsmNpGetCardTypeTable *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MBSM_MOD,    /* Module ID */
                         MBSM_NP_GET_CARD_TYPE_TABLE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMbsmNpModInfo = &(FsHwNp.MbsmNpModInfo);
    pEntry = &pMbsmNpModInfo->MbsmNpMbsmNpGetCardTypeTable;

    pEntry->i4LoopIdx = i4LoopIdx;
    pEntry->pMbsmCardInfo = pMbsmCardInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MbsmMbsmNpHandleNodeTransition                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes MbsmNpHandleNodeTransition
 *                                                                          
 *    Input(s)            : Arguments of MbsmNpHandleNodeTransition
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MbsmMbsmNpHandleNodeTransition (INT4 i4Event, UINT1 u1PrevState, UINT1 u1State)
{
    tFsHwNp             FsHwNp;
    tMbsmNpModInfo     *pMbsmNpModInfo = NULL;
    tMbsmNpWrMbsmNpHandleNodeTransition *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MBSM_MOD,    /* Module ID */
                         MBSM_NP_HANDLE_NODE_TRANSITION,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMbsmNpModInfo = &(FsHwNp.MbsmNpModInfo);
    pEntry = &pMbsmNpModInfo->MbsmNpMbsmNpHandleNodeTransition;

    pEntry->i4Event = i4Event;
    pEntry->u1PrevState = u1PrevState;
    pEntry->u1State = u1State;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MbsmMbsmNpTxOnStackInterface                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes MbsmNpTxOnStackInterface
 *                                                                          
 *    Input(s)            : Arguments of MbsmNpTxOnStackInterface
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MbsmMbsmNpTxOnStackInterface (UINT1 *pu1Pkt, INT4 i4PktSize)
{
    tFsHwNp             FsHwNp;
    tMbsmNpModInfo     *pMbsmNpModInfo = NULL;
    tMbsmNpWrMbsmNpTxOnStackInterface *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MBSM_MOD,    /* Module ID */
                         MBSM_NP_TX_ON_STACK_INTERFACE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMbsmNpModInfo = &(FsHwNp.MbsmNpModInfo);
    pEntry = &pMbsmNpModInfo->MbsmNpMbsmNpTxOnStackInterface;

    pEntry->pu1Pkt = pu1Pkt;
    pEntry->i4PktSize = i4PktSize;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MbsmMbsmNpGetStackMac                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes MbsmNpGetStackMac
 *                                                                          
 *    Input(s)            : Arguments of MbsmNpGetStackMac
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MbsmMbsmNpGetStackMac (UINT4 u4SlotId, UINT1 *pu1MacAddr)
{
    tFsHwNp             FsHwNp;
    tMbsmNpModInfo     *pMbsmNpModInfo = NULL;
    tMbsmNpWrMbsmNpGetStackMac *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MBSM_MOD,    /* Module ID */
                         MBSM_NP_GET_STACK_MAC,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMbsmNpModInfo = &(FsHwNp.MbsmNpModInfo);
    pEntry = &pMbsmNpModInfo->MbsmNpMbsmNpGetStackMac;

    pEntry->u4SlotId = u4SlotId;
    pEntry->pu1MacAddr = pu1MacAddr;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

#endif /* _MBSM_NPAPI_C_ */
