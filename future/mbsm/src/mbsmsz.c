/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: mbsmsz.c,v 1.1 Fri Aug 20 10:08:16 2010 UTC (3 years, 10 months ago) by prabuc
*
* Description:This file contains memory pools of MBSM module.
*
*******************************************************************/

#define _MBSMSZ_C
#include "mbsminc.h"

extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);

INT4
MbsmSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < MBSM_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsMBSMSizingParams[i4SizingId].u4StructSize,
                              FsMBSMSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(MBSMMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            MbsmSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
MbsmSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsMBSMSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, MBSMMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
MbsmSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < MBSM_MAX_SIZING_ID; i4SizingId++)
    {
        if (MBSMMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (MBSMMemPoolIds[i4SizingId]);
            MBSMMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
