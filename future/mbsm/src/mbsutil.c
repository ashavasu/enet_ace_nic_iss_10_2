/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mbsutil.c,v 1.41 2014/11/03 11:48:17 siva Exp $
 *
 * Description:This file contains the utility routines for  
 *             MBSM as well as other modules.               
 *
 *******************************************************************/
#include "mbsminc.h"

typedef struct
{
    UINT1               au1Name[MBSM_PORT_SPEED_NAME_LEN];
    UINT4               u4SpeedType;
    UINT4               u4EncapType;
    UINT4               u4MtuType;
}
tMbsmPortTypeTable;

tMbsmPortTypeTable  gaMbsmPortTypeTable[MBSM_MAX_IF_TYPES] = {
    {"fe", MBSM_SPEED_FAST_ETHER, MBSM_ENCAP_ENETV2, MBSM_MTU_ENET}
    ,
    {"eth", MBSM_SPEED_ETHER, MBSM_ENCAP_ENETV2, MBSM_MTU_ENET}
    ,
    {"ge", MBSM_SPEED_GIG_ETHER, MBSM_ENCAP_ENETV2, MBSM_MTU_ENET}
    ,
    {"xe", MBSM_SPEED_10GIG_ETHER, MBSM_ENCAP_ENETV2, MBSM_MTU_ENET}
    ,
    {"xl", MBSM_SPEED_40GIG_ETHER, MBSM_ENCAP_ENETV2, MBSM_MTU_ENET}
    ,
    {"", MBSM_SPEED_INVALID, MBSM_ENCAP_INVALID, MBSM_MTU_INVALID}
};

tMbsmPortSpeedInfo  gaMbsmPortSpeedTable[MBSM_MAX_PORT_SPEED_TYPES] = {
    {MBSM_SPEED_ETHER, {10000000, 0}}
    ,
    {MBSM_SPEED_FAST_ETHER, {100000000, 0}}
    ,
    {MBSM_SPEED_GIG_ETHER, {1000000000, 0}}
    ,
    {MBSM_SPEED_10GIG_ETHER, {0XFFFFFFFF, 10000}}
    ,
    {MBSM_SPEED_40GIG_ETHER, {0XFFFFFFFF, 40000}}
    ,
    {MBSM_SPEED_INVALID, {0, 0}}
};

tMbsmPortEncapInfo  gaMbsmPortEncapTable[MBSM_MAX_IF_TYPES] = {
    {MBSM_ENCAP_ENETV2, CFA_ENCAP_ENETV2}
    ,
    {MBSM_ENCAP_LLC, CFA_ENCAP_LLC}
    ,
    {MBSM_ENCAP_LLC_SNAP, CFA_ENCAP_LLC_SNAP}
    ,
    {MBSM_ENCAP_INVALID, CFA_ENCAP_ENETV2}
};

tMbsmPortMtuInfo    gaMbsmPortMtuTable[MBSM_MAX_IF_TYPES] = {
    {MBSM_MTU_ENET, MBSM_PORT_MTU_ENET}
    ,
    {MBSM_MTU_FDDI, MBSM_PORT_MTU_FDDI}
    ,
    {MBSM_MTU_INVALID, 0}
};
extern UINT1        gu1SelfAttachRcvd;
/*****************************************************************************
 *
 *    Function Name        : MbsmGetSlotCardName
 *
 *    Description          : This function returns the card name for the card
 *                           present in the given slot.                       
 *
 *    Input(s)             : i4SlotId - Slot Id to fetch the card name.
 *
 *    Output(s)            : pi1Name - Name of the card
 *
 *    Global Variables Referred : gaMbsmSlotMapTable, gaMbsmCardTypeTable
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion     : None.
 *
 *    Returns              : MBSM_SUCCESS or MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmGetSlotCardName (INT4 i4SlotId, INT1 *pi1Name)
{
    INT4                i4CardType;

    if (!pi1Name)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_UTIL,
                  "Error in MbsmGetSlotCardName - Null entry\n");
        return (MBSM_FAILURE);
    }

    if (!MBSM_IS_SLOT_ID_VALID (i4SlotId))
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_UTIL,
                  "Error in MbsmGetSlotCardName - Invalid Slot Id\n");
        return (MBSM_FAILURE);
    }

    i4CardType = MBSM_SLOT_INDEX_CARDTYPE (i4SlotId);

    if (i4CardType == MBSM_INVALID_CARDTYPE)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_UTIL,
                  "Error in MbsmGetSlotCardName - Invalid Card type\n");
        return (MBSM_FAILURE);
    }

    SNPRINTF ((char *) pi1Name,
              (MBSM_MAX_CARD_NAME_LEN + 1), "%s",
              MBSM_CARD_TYPE_INFO_CARDNAME (i4CardType));

    return (MBSM_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmGetCardIndexFromCardName
 *
 *    Description          : This function retrives the Card Index from the
 *                           Card Type table for the Card Name given.        
 *
 *    Input(s)             : pi1CardName - Card Name to be searched in table.
 *
 *    Output(s)            : pi4CardIndex - Card Index for the Card Name.
 *
 *    Global Variables Referred : gaMbsmCardTypeTable
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion     : None.
 *
 *    Returns              : MBSM_SUCCESS or MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmGetCardIndexFromCardName (INT1 *pi1CardName, INT4 *pi4CardIndex)
{
    INT4                i4LoopIndex;
    INT4                i4RetVal = MBSM_FAILURE;

    if (!pi1CardName)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_UTIL,
                  "Error in MbsmGetCardIndexFromCardName - Null entry\n");
        return (MBSM_FAILURE);
    }

    if (!pi4CardIndex)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_UTIL,
                  "Error in MbsmGetCardIndexFromCardName - Null entry\n");
        return (MBSM_FAILURE);
    }

    if (MBSM_PROTO_TAKE_SEM (MBSM_PROTO_SEM) != OSIX_SUCCESS)
    {
        MBSM_DBG (MBSM_TRC_CRITICAL, MBSM_UTIL,
                  "Error in MbsmGetCardIndexFromCardName - SemTake Failed.\n");
        return (MBSM_FAILURE);
    }

    /* Searching the Card Type Table */
    for (i4LoopIndex = 0; ((i4LoopIndex < MBSM_MAX_LC_TYPES) &&
                           (MBSM_CARD_TYPE_ENTRY (i4LoopIndex))); i4LoopIndex++)
    {
        if (!(STRCMP (MBSM_CARD_TYPE_INFO_CARDNAME (i4LoopIndex), pi1CardName)))
        {
            *pi4CardIndex = i4LoopIndex;
            i4RetVal = MBSM_SUCCESS;
        }
    }

    if (MBSM_PROTO_GIVE_SEM (MBSM_PROTO_SEM) != OSIX_SUCCESS)
    {
        MBSM_DBG (MBSM_TRC_CRITICAL, MBSM_UTIL,
                  "Error in MbsmGetCardIndexFromCardName - SemGive Failed.\n");
        i4RetVal = MBSM_FAILURE;
    }

    return (i4RetVal);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmGetPortMtu
 *
 *    Description          : This function gives the Port MTU for the index  
 *                           passed from the Port Map table.
 *
 *    Input(s)             : u4IfIndex - Interface index of the port.
 *
 *    Output(s)            : pu4IfMtu - Port MTU of the port.
 *
 *    Global Variables Referred : gaMbsmLcTypeTable
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion     : None.
 *
 *    Returns              : MBSM_SUCCESS or MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmGetPortMtu (UINT4 u4IfIndex, UINT4 *pu4IfMtu)
{
    INT4                i4SlotId;
    INT4                i4CardType;
    INT4                i4RetVal = MBSM_SUCCESS;
    tMbsmLcPortInfo     PortMapEntry;
    INT4                i4PortNum;    /* Port number within the Line Card */

    if (u4IfIndex > SYS_DEF_MAX_PHYSICAL_INTERFACES)
    {
        return (MBSM_FAILURE);

    }

    /* The following function is already protected by semaphore, so
       we should not lock before calling this function */
    if (MbsmGetSlotFromPort (u4IfIndex, &i4SlotId) == MBSM_FAILURE)
    {
        return (MBSM_FAILURE);

    }

    if (!MBSM_IS_SLOT_ID_VALID (i4SlotId))
    {
        return (MBSM_FAILURE);
    }

    if (MBSM_PROTO_TAKE_SEM (MBSM_PROTO_SEM) != OSIX_SUCCESS)
    {
        MBSM_DBG (MBSM_TRC_CRITICAL, MBSM_UTIL,
                  "Error in MbsmGetPortMtu - SemTake Failed.\n");
        return (MBSM_FAILURE);
    }
    if (MBSM_SLOT_INDEX_STATUS (i4SlotId) == MBSM_STATUS_INACTIVE)
    {
        if (MBSM_PROTO_GIVE_SEM (MBSM_PROTO_SEM) != OSIX_SUCCESS)
        {
            MBSM_DBG (MBSM_TRC_CRITICAL, MBSM_UTIL,
                      "Error in MbsmGetPortMtu - SemGive Failed.\n");
        }

        return (MBSM_FAILURE);
    }
    i4CardType = MBSM_SLOT_INDEX_CARDTYPE (i4SlotId);

    PortMapEntry = MBSM_CARD_TYPE_INFO_PORTMAP (i4CardType);

    i4PortNum = (u4IfIndex - MBSM_SLOT_INDEX_STARTINDEX (i4SlotId)) + 1;

    if (i4PortNum <= 0)
    {
        if (MBSM_PROTO_GIVE_SEM (MBSM_PROTO_SEM) != OSIX_SUCCESS)
        {
            MBSM_DBG (MBSM_TRC_CRITICAL, MBSM_UTIL,
                      "Error in MbsmGetPortMtu - SemGive Failed.\n");
        }

        return (MBSM_FAILURE);
    }

    if (MbsmGetPortMtuFromPortMap (&PortMapEntry, (UINT4) i4PortNum, pu4IfMtu)
        == MBSM_FAILURE)
    {

        i4RetVal = MBSM_FAILURE;
    }

    if (MBSM_PROTO_GIVE_SEM (MBSM_PROTO_SEM) != OSIX_SUCCESS)
    {
        MBSM_DBG (MBSM_TRC_CRITICAL, MBSM_UTIL,
                  "Error in MbsmGetPortMtu - SemGive Failed.\n");
        i4RetVal = MBSM_FAILURE;
    }

    return (i4RetVal);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmGetSlotFromPort
 *
 *    Description          : This function gives the Slot Id based on the    
 *                           port index passed to the function.
 *
 *    Input(s)             : u4IfIndex - Interface index of the port.
 *
 *    Output(s)            : pi4SlotId- Slot Id for the port.
 *
 *    Global Variables Referred : gaMbsmSlotMapTable
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion     : None.
 *
 *    Returns              : MBSM_SUCCESS or MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmGetSlotFromPort (UINT4 u4IfIndex, INT4 *pi4SlotId)
{
    INT4                i4SlotIndex;
    INT4                i4RetVal = MBSM_SUCCESS;

    if (MBSM_PROTO_TAKE_SEM (MBSM_PROTO_SEM) != OSIX_SUCCESS)
    {
        MBSM_DBG (MBSM_TRC_CRITICAL, MBSM_UTIL,
                  "Error in MbsmGetSlotFromPort - SemTake Failed.\n");
        return (MBSM_FAILURE);
    }

    for (i4SlotIndex = MBSM_SLOT_INDEX_START;
         i4SlotIndex < (MBSM_MAX_SLOTS + MBSM_SLOT_INDEX_START); i4SlotIndex++)
    {
        if ((u4IfIndex < (MBSM_SLOT_INDEX_STARTINDEX (i4SlotIndex)
                          + MBSM_SLOT_INDEX_NUMPORTS (i4SlotIndex)))
            && (u4IfIndex >= MBSM_SLOT_INDEX_STARTINDEX (i4SlotIndex)))
        {
            *pi4SlotId = i4SlotIndex;
            break;
        }
    }

    if (MBSM_PROTO_GIVE_SEM (MBSM_PROTO_SEM) != OSIX_SUCCESS)
    {
        MBSM_DBG (MBSM_TRC_CRITICAL, MBSM_UTIL,
                  "Error in MbsmGetSlotFromPort - SemGive Failed.\n");
        i4RetVal = MBSM_FAILURE;
    }

    return (i4RetVal);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmGetPortMtuFromPortMap
 *
 *    Description          : This function gives the port MTU for the port   
 *                           based on the information in Port map entry of
 *                           the Line card table.
 *
 *    Input(s)             : pEntry - Pointer to the Port Map entry in 
 *                           Line Card type table.
 *                           u4PortNum - Port Number within the Line Card.
 *
 *    Output(s)            : pu4IfMtu - MTU for the port.
 *
 *    Global Variables Referred : gaMbsmLcTypeTable
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion     : None.
 *
 *    Returns              : MBSM_SUCCESS or MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmGetPortMtuFromPortMap (tMbsmLcPortInfo * pEntry, UINT4 u4PortNum,
                           UINT4 *pu4IfMtu)
{
    INT4                i4Index;
    UINT4               u4Result;

    for (i4Index = 0; i4Index < MBSM_MAX_PORT_MTU_TYPES; i4Index++)
    {
        if (!pEntry->MtuTable[i4Index])
            continue;
        MBSM_IS_MEMBER_MBSMPORTLIST (pEntry->MtuTable[i4Index], u4PortNum,
                                     &u4Result);
        if (u4Result == MBSM_SUCCESS)
        {
            switch (i4Index)
            {
                case MBSM_MTU_ENET:
                    *pu4IfMtu = MBSM_PORT_MTU_ENET;
                    break;

                case MBSM_MTU_FDDI:
                    *pu4IfMtu = MBSM_PORT_MTU_FDDI;
                    break;

                default:
                    *pu4IfMtu = MBSM_PORT_MTU_ENET;
                    break;

            }
            return (MBSM_SUCCESS);
        }
    }

    return (MBSM_FAILURE);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmGetPortEncap
 *
 *    Description          : This function gives the Port Encapsulation for
 *                           the port index passed from the Port Map table.
 *
 *    Input(s)             : u4IfIndex - Interface index of the port.
 *
 *    Output(s)            : pu4IfEncap - Port Encapsulation of the port.
 *
 *    Global Variables Referred : gaMbsmLcTypeTable
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion     : None.
 *
 *    Returns              : MBSM_SUCCESS or MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmGetPortEncap (UINT4 u4IfIndex, UINT4 *pu4IfEncap)
{
    INT4                i4SlotId;
    INT4                i4CardType;
    tMbsmLcPortInfo     PortMapEntry;
    INT4                i4PortNum;    /* Port number within the Line Card */
    UINT4               u4IfType;
    INT4                i4RetVal = MBSM_SUCCESS;

    /* The following function is already protected by semaphore, so
       we should not lock before calling this function */
    if (MbsmGetSlotFromPort (u4IfIndex, &i4SlotId) == MBSM_FAILURE)
    {
        return (MBSM_FAILURE);
    }

    if (MBSM_PROTO_TAKE_SEM (MBSM_PROTO_SEM) != OSIX_SUCCESS)
    {
        MBSM_DBG (MBSM_TRC_CRITICAL, MBSM_UTIL,
                  "Error in MbsmGetPortEncap - SemTake Failed.\n");
        return (MBSM_FAILURE);
    }

    i4CardType = MBSM_SLOT_INDEX_CARDTYPE (i4SlotId);

    if (i4CardType == MBSM_INVALID_CARDTYPE)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_UTIL,
                  "Error in MbsmGetPortEncap - Invalid Cardtype\n");
        return (MBSM_FAILURE);
    }

    PortMapEntry = MBSM_CARD_TYPE_INFO_PORTMAP (i4CardType);

    i4PortNum = (u4IfIndex - MBSM_SLOT_INDEX_STARTINDEX (i4SlotId)) + 1;

    if (i4PortNum <= 0)
    {
        if (MBSM_PROTO_GIVE_SEM (MBSM_PROTO_SEM) != OSIX_SUCCESS)
        {
            MBSM_DBG (MBSM_TRC_CRITICAL, MBSM_UTIL,
                      "Error in MbsmGetPortEncap - SemGive Failed.\n");
        }

        return (MBSM_FAILURE);
    }

    if (MbsmGetPortIfTypeFromPortMap
        (&PortMapEntry, (UINT4) i4PortNum, &u4IfType) == MBSM_SUCCESS)
    {
        /* Support exists for ENET type only and Encap defaults to ENETV2 */
        switch (u4IfType)
        {
            case CFA_ENET:
                *pu4IfEncap = MBSM_ENCAP_ENETV2;
                break;

            default:
                *pu4IfEncap = MBSM_ENCAP_ENETV2;
                break;
        }
    }
    else
    {
        i4RetVal = MBSM_FAILURE;
    }

    if (MBSM_PROTO_GIVE_SEM (MBSM_PROTO_SEM) != OSIX_SUCCESS)
    {
        MBSM_DBG (MBSM_TRC_CRITICAL, MBSM_UTIL,
                  "Error in MbsmGetPortEncap - SemGive Failed.\n");
        i4RetVal = MBSM_FAILURE;
    }

    return (i4RetVal);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmGetPortIfTypeFromPortMap
 *
 *    Description          : This function gives the port If type for the   
 *                           port based on the information in Port map entry of
 *                           the Line card table.
 *
 *    Input(s)             : pEntry - Pointer to the Port Map entry in 
 *                           Line Card type table.
 *                           u4PortNum - Port Number within the Line Card.
 *
 *    Output(s)            : pu4IfEncap - Encapsulation type for the port.
 *
 *    Global Variables Referred : gaMbsmLcTypeTable
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion     : None.
 *
 *    Returns              : MBSM_SUCCESS or MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmGetPortIfTypeFromPortMap (tMbsmLcPortInfo * pEntry, UINT4 u4PortNum,
                              UINT4 *pu4IfType)
{
    INT4                i4Index;
    UINT4               u4Result;

    for (i4Index = 0; i4Index < MBSM_MAX_IF_TYPES; i4Index++)
    {
        if (!pEntry->IfTypeTable[i4Index])
            continue;
        MBSM_IS_MEMBER_MBSMPORTLIST (pEntry->IfTypeTable[i4Index], u4PortNum,
                                     &u4Result);
        if (u4Result == MBSM_SUCCESS)
        {
            *pu4IfType = (UINT4) i4Index;
            return (MBSM_SUCCESS);
        }
    }

    return (MBSM_FAILURE);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmGetPortSpeedType
 *
 *    Description          : This function gives the Port Speed type from the  
 *                           string passed as argument.
 *
 *    Input(s)             : pu1Str - String from which type is derived.
 *
 *    Output(s)            : pu4SpeedType - Port Speed type (fe, ge etc...)
 *
 *    Global Variables Referred : gaMbsmPortTypeTable
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion     : None.
 *
 *    Returns              : MBSM_SUCCESS or MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmGetPortSpeedType (UINT1 *pu1Str, UINT4 *pu4SpeedType)
{
    UINT4               u4Index;

    for (u4Index = 0; ((u4Index < MBSM_MAX_IF_TYPES) &&
                       (gaMbsmPortTypeTable[u4Index].u4SpeedType !=
                        MBSM_SPEED_INVALID)); u4Index++)
    {
        if (STRSTR (pu1Str, gaMbsmPortTypeTable[u4Index].au1Name) != NULL)
        {
            *pu4SpeedType = gaMbsmPortTypeTable[u4Index].u4SpeedType;
            return (MBSM_SUCCESS);
        }

    }

    return (MBSM_FAILURE);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmGetPortSpeedFromPortMap
 *
 *    Description          : This function gives the port Speed type for the   
 *                           port based on the information in Port map entry of
 *                           the Line card table.
 *
 *    Input(s)             : pEntry - Pointer to the Port Map entry in 
 *                           Line Card type table.
 *                           u4PortNum - Port Number within the Line Card.
 *
 *    Output(s)            : pSpeedInfo - Speed Information for the port.
 *
 *    Global Variables Referred : gaMbsmLcTypeTable
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion     : None.
 *
 *    Returns              : MBSM_SUCCESS or MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmGetPortSpeedFromPortMap (tMbsmLcPortInfo * pEntry, UINT4 u4PortNum,
                             tMbsmPortSpeed * pSpeedInfo)
{
    INT4                i4Index;
    UINT4               u4Result;

    for (i4Index = 0; i4Index < MBSM_MAX_PORT_SPEED_TYPES; i4Index++)
    {
        MBSM_IS_MEMBER_MBSMPORTLIST (pEntry->SpeedTable[i4Index], u4PortNum,
                                     &u4Result);
        if (u4Result == MBSM_SUCCESS)
        {
            switch (i4Index)
            {
                case MBSM_SPEED_ETHER:    /* Intentional Fall through */

                case MBSM_SPEED_FAST_ETHER:    /* Intentional fall through */

                case MBSM_SPEED_GIG_ETHER:    /* Intentional fall through */
                    MbsmGetSpeedInfoFromType (i4Index, pSpeedInfo);
                    break;

                default:
                    MbsmGetSpeedInfoFromType (MBSM_SPEED_ETHER, pSpeedInfo);
                    break;

            }
            return (MBSM_SUCCESS);
        }
    }

    return (MBSM_FAILURE);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmGetPortSpeedTypeFromCardIndex
 *
 *    Description          : This function gives the port Speed type for the   
 *                           port based on the information in Port map entry of
 *                           the Line card table.
 *
 *    Input(s)             : i4CardIndex - CardIndex for the 
 *                           gaMbsmLcTypeTable table 
 *                           Line Card type table.
 *                           u4PortNum - Port Number within the Line Card.
 *
 *    Output(s)            : pu1SpeedType - Speed Type for the port.
 *
 *    Global Variables Referred : gaMbsmLcTypeTable
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion     : None.
 *
 *    Returns              : MBSM_SUCCESS or MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmGetPortSpeedTypeFromCardIndex (INT4 i4CardIndex, UINT4 u4PortNum,
                                   UINT1 *pu1SpeedType)
{
    UINT1               u1Index;
    UINT4               u4Result;
    tMbsmLcPortInfo     PortMapEntry;

    *pu1SpeedType = MBSM_SPEED_INVALID;

    if (MBSM_PROTO_TAKE_SEM (MBSM_PROTO_SEM) != OSIX_SUCCESS)
    {
        MBSM_DBG (MBSM_TRC_CRITICAL, MBSM_UTIL,
                  "Error in MbsmGetPortSpeedTypeFromCardIndex - SemTake Failed.\n");
        return (MBSM_FAILURE);
    }

    PortMapEntry = MBSM_CARD_TYPE_INFO_PORTMAP (i4CardIndex);

    for (u1Index = 0; u1Index < MBSM_MAX_PORT_SPEED_TYPES; u1Index++)
    {
        MBSM_IS_MEMBER_MBSMPORTLIST (PortMapEntry.SpeedTable[u1Index],
                                     u4PortNum, &u4Result);
        if (u4Result == MBSM_SUCCESS)
        {
            break;
        }
    }

    if (MBSM_PROTO_GIVE_SEM (MBSM_PROTO_SEM) != OSIX_SUCCESS)
    {
        MBSM_DBG (MBSM_TRC_CRITICAL, MBSM_UTIL,
                  "Error in MbsmGetPortSpeedTypeFromCardIndex - SemGive Failed.\n");
    }

    if (u1Index < MBSM_MAX_PORT_SPEED_TYPES)
    {
        *pu1SpeedType = u1Index;
        return MBSM_SUCCESS;
    }
    return MBSM_FAILURE;
}

/*****************************************************************************
 *
 *    Function Name        : MbsmGetPortSpeedTypeFromIfIndex
 *
 *    Description          : This function gives the Port speed type for the 
 *                           Ifindex Passed as argument.
 *
 *    Input(s)             : u4IfIndex - Interface index of the port.
 *
 *    Output(s)            : pu1SpeedType - Speed Type of the Interface.
 *
 *    Global Variables Referred : gaMbsmLcTypeTable
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion     : None.
 *
 *    Returns              : MBSM_SUCCESS or MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmGetPortSpeedTypeFromIfIndex (UINT4 u4IfIndex, UINT1 *pu1SpeedType)
{
    INT4                i4SlotId = MBSM_INVALID_SLOT_ID;
    INT4                i4CardIndex = MBSM_INVALID_CARDINDEX;
    INT4                i4PortNum;    /* Port number within the Line Card */

    if (u4IfIndex > SYS_DEF_MAX_PHYSICAL_INTERFACES)
    {
        return (MBSM_FAILURE);

    }

    /* The following function is already protected by semaphore, so
       we should not lock before calling this function */
    if (MbsmGetSlotFromPort (u4IfIndex, &i4SlotId) == MBSM_FAILURE)
    {
        return (MBSM_FAILURE);

    }

    if (!MBSM_IS_SLOT_ID_VALID (i4SlotId))
    {
        return (MBSM_FAILURE);
    }

    if (MBSM_PROTO_TAKE_SEM (MBSM_PROTO_SEM) != OSIX_SUCCESS)
    {
        MBSM_DBG (MBSM_TRC_CRITICAL, MBSM_UTIL,
                  "Error in MbsmGetPortSpeedTypeFromIfIndex - SemTake Failed.\n");
        return (MBSM_FAILURE);
    }

    if (MBSM_SLOT_INDEX_STATUS (i4SlotId) == MBSM_STATUS_INACTIVE)
    {
        if (MBSM_PROTO_GIVE_SEM (MBSM_PROTO_SEM) != OSIX_SUCCESS)
        {
            MBSM_DBG (MBSM_TRC_CRITICAL, MBSM_UTIL,
                      "Error in MbsmGetPortSpeedTypeFromIfIndex - SemGive Failed.\n");
        }

        return (MBSM_FAILURE);
    }

    i4CardIndex = MBSM_SLOT_INDEX_CARDTYPE (i4SlotId);

    i4PortNum = (u4IfIndex - MBSM_SLOT_INDEX_STARTINDEX (i4SlotId)) + 1;

    if (MBSM_PROTO_GIVE_SEM (MBSM_PROTO_SEM) != OSIX_SUCCESS)
    {
        MBSM_DBG (MBSM_TRC_CRITICAL, MBSM_UTIL,
                  "Error in MbsmGetPortSpeedTypeFromIfIndex - SemGive Failed.\n");
        return (MBSM_FAILURE);
    }

    if (i4PortNum <= 0)
    {
        return (MBSM_FAILURE);
    }

    MbsmGetPortSpeedTypeFromCardIndex (i4CardIndex, (UINT4) i4PortNum,
                                       pu1SpeedType);

    return (MBSM_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmGetSpeedInfoFromType
 *
 *    Description          : This function gives the Port Speed info for the   
 *                           port based on the Port type passed.
 *
 *    Input(s)             : u4SpeedType - Port Speed Type for the Port.
 *
 *    Output(s)            : pSpeedInfo - Speed Information for the port.
 *
 *    Global Variables Referred : gaMbsmPortSpeedTable
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion     : None.
 *
 *    Returns              : MBSM_SUCCESS or MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmGetSpeedInfoFromType (UINT4 u4SpeedType, tMbsmPortSpeed * pSpeedInfo)
{
    INT4                i4Index;

    for (i4Index = 0; ((i4Index < MBSM_MAX_PORT_SPEED_TYPES) &&
                       (gaMbsmPortSpeedTable[i4Index].u4SpeedType
                        != MBSM_SPEED_INVALID)); i4Index++)
    {
        if (gaMbsmPortSpeedTable[i4Index].u4SpeedType == u4SpeedType)
        {
            *pSpeedInfo = gaMbsmPortSpeedTable[i4Index].PortSpeed;
            return (MBSM_SUCCESS);
        }
    }

    return (MBSM_FAILURE);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmSetPortIfTypeFromPortMap
 *
 *    Description          : This function sets the port If type for the   
 *                           port based on the information in Port map entry of
 *                           the Line card table.
 *
 *    Input(s)             : pEntry - Pointer to the Port Map entry in 
 *                           Line Card type table.
 *                           u4PortNum - Port Number within the Line Card.
 *                           u4IfType - Port If type to be set.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gaMbsmLcTypeTable
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion     : None.
 *
 *    Returns              : MBSM_SUCCESS or MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmSetPortIfTypeFromPortMap (tMbsmLcPortInfo * pEntry, UINT4 u4PortNum,
                              UINT4 u4IfType)
{
    UINT4               u4Index;
    UINT4               u4Result;

    for (u4Index = 0; u4Index < MBSM_MAX_IF_TYPES; u4Index++)
    {
        if (!pEntry->IfTypeTable[u4Index])
        {
            continue;
        }
        MBSM_IS_MEMBER_MBSMPORTLIST (pEntry->IfTypeTable[u4Index], u4PortNum,
                                     &u4Result);
        if (u4Result == MBSM_SUCCESS)
        {
            if (u4IfType == u4Index)
            {
                return (MBSM_SUCCESS);
            }
            MBSM_RESET_MEMBER_PORTLIST (pEntry->IfTypeTable[u4Index],
                                        u4PortNum);
        }
    }
    MBSM_SET_MEMBER_PORTLIST (pEntry->IfTypeTable[u4IfType], u4PortNum);

    return (MBSM_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmSetPortSpeedFromPortMap
 *
 *    Description          : This function sets the port speed type for the   
 *                           port based on the information in Port map entry of
 *                           the Line card table.
 *
 *    Input(s)             : pEntry - Pointer to the Port Map entry in 
 *                           Line Card type table.
 *                           u4PortNum - Port Number within the Line Card.
 *                           PortSpeed - Port Speed to be set.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gaMbsmLcTypeTable
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion     : None.
 *
 *    Returns              : MBSM_SUCCESS or MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmSetPortSpeedFromPortMap (tMbsmLcPortInfo * pEntry, UINT4 u4PortNum,
                             tMbsmPortSpeed PortSpeed)
{
    INT4                i4Index;

    for (i4Index = 0; (gaMbsmPortSpeedTable[i4Index].u4SpeedType
                       != MBSM_SPEED_INVALID); i4Index++)
    {
        if ((gaMbsmPortSpeedTable[i4Index].PortSpeed.u4Speed ==
             PortSpeed.u4Speed)
            && (gaMbsmPortSpeedTable[i4Index].PortSpeed.u4HighSpeed ==
                PortSpeed.u4HighSpeed))
        {
            MBSM_SET_MEMBER_PORTLIST (pEntry->SpeedTable[i4Index], u4PortNum);
            return (MBSM_SUCCESS);
        }
    }

    return (MBSM_FAILURE);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmGetPortEncapType
 *
 *    Description          : This function gives the Port Encap type from the  
 *                           string passed as argument.
 *
 *    Input(s)             : pu1Str - String from which type is derived.
 *
 *    Output(s)            : pu4EncapType - Port Encap type (fe, ge etc...)
 *
 *    Global Variables Referred : gaMbsmPortTypeTable
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion     : None.
 *
 *    Returns              : MBSM_SUCCESS or MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmGetPortEncapType (UINT1 *pu1Str, UINT4 *pu4EncapType)
{
    UINT4               u4Index;

    for (u4Index = 0; ((u4Index < MBSM_MAX_IF_TYPES) &&
                       (gaMbsmPortTypeTable[u4Index].u4EncapType !=
                        MBSM_ENCAP_INVALID)); u4Index++)
    {
        if (STRSTR (pu1Str, gaMbsmPortTypeTable[u4Index].au1Name) != NULL)
        {
            *pu4EncapType = gaMbsmPortTypeTable[u4Index].u4EncapType;
            return (MBSM_SUCCESS);
        }

    }

    return (MBSM_FAILURE);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmGetPortMtuType
 *
 *    Description          : This function gives the Port MTU type from the  
 *                           string passed as argument.
 *
 *    Input(s)             : pu1Str - String from which type is derived.
 *
 *    Output(s)            : pu4MtuType - Port MTU type (fe, ge etc...)
 *
 *    Global Variables Referred : gaMbsmPortTypeTable
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion     : None.
 *
 *    Returns              : MBSM_SUCCESS or MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmGetPortMtuType (UINT1 *pu1Str, UINT4 *pu4MtuType)
{
    UINT4               u4Index;

    for (u4Index = 0; ((u4Index < MBSM_MAX_IF_TYPES) &&
                       (gaMbsmPortTypeTable[u4Index].u4MtuType !=
                        MBSM_MTU_INVALID)); u4Index++)
    {
        if (STRSTR (pu1Str, gaMbsmPortTypeTable[u4Index].au1Name) != NULL)
        {
            *pu4MtuType = gaMbsmPortTypeTable[u4Index].u4MtuType;
            return (MBSM_SUCCESS);
        }

    }

    return (MBSM_FAILURE);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmGetPortEncapFromType
 *
 *    Description          : This function gives the Port Encap info for the   
 *                           port based on the Port type passed.
 *
 *    Input(s)             : u4EncapType - Port Type for the Port.
 *
 *    Output(s)            : pu4Encap - Encapsulation type for the port.
 *
 *    Global Variables Referred : gaMbsmPortEncapTable
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion     : None.
 *
 *    Returns              : MBSM_SUCCESS or MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmGetEncapInfoFromType (UINT4 u4EncapType, UINT4 *pu4Encap)
{
    INT4                i4Index;

    for (i4Index = 0; ((i4Index < MBSM_MAX_IF_TYPES) &&
                       (gaMbsmPortEncapTable[i4Index].u4EncapType
                        != MBSM_ENCAP_INVALID)); i4Index++)
    {
        if (gaMbsmPortEncapTable[i4Index].u4EncapType == u4EncapType)
        {
            *pu4Encap = gaMbsmPortEncapTable[i4Index].u4Encap;
            return (MBSM_SUCCESS);
        }
    }

    return (MBSM_FAILURE);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmGetPortMtuFromType
 *
 *    Description          : This function gives the Port MTU info for the   
 *                           port based on the Port type passed.
 *
 *    Input(s)             : u4MTUType - Port Type for the Port.
 *
 *    Output(s)            : pu4Mtu - MTU for the port.
 *
 *    Global Variables Referred : gaMbsmPortMtuTable
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion     : None.
 *
 *    Returns              : MBSM_SUCCESS or MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmGetMtuInfoFromType (UINT4 u4MtuType, UINT4 *pu4Mtu)
{
    INT4                i4Index;

    for (i4Index = 0; ((i4Index < MBSM_MAX_IF_TYPES) &&
                       (gaMbsmPortMtuTable[i4Index].u4MtuType
                        != MBSM_MTU_INVALID)); i4Index++)
    {
        if (gaMbsmPortMtuTable[i4Index].u4MtuType == u4MtuType)
        {
            *pu4Mtu = gaMbsmPortMtuTable[i4Index].u4Mtu;
            return (MBSM_SUCCESS);
        }
    }

    return (MBSM_FAILURE);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmIsDefaultInit
 *
 *    Description          : This function checks if the module is initialised
 *                           with default parameters.
 *
 *    Input(s)             : None. 
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion     : None.
 *
 *    Returns              : MBSM_SUCCESS or MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmIsDefaultInit (VOID)
{
    if (MBSM_DEF_INIT_FLAG () == MBSM_TRUE)
    {
        return (MBSM_SUCCESS);
    }
    else
    {
        return (MBSM_FAILURE);
    }
}

/*****************************************************************************
 *
 *    Function Name        : MbsmGetSlotInfo
 *
 *    Description          : This function returns the slot infomation from  
 *                           the Slot Map table for the Slot specified.       
 *
 *    Input(s)             : i4SlotId - Slot Id to fetch the card name.
 *
 *    Output(s)            : pSlotInfo - Pointer to the Slot info structure.
 *
 *    Global Variables Referred : gaMbsmSlotMapTable
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion     : None.
 *
 *    Returns              : MBSM_SUCCESS or MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmGetSlotInfo (INT4 i4SlotId, tMbsmSlotInfo * pSlotInfo)
{
    INT4                i4RetVal = MBSM_SUCCESS;

    if (!MBSM_IS_SLOT_ID_VALID (i4SlotId))
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_UTIL,
                  "Error in MbsmGetSlotInfo - Invalid Slot Id\n");
        return (MBSM_FAILURE);
    }

    if (!pSlotInfo)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_UTIL,
                  "Error in MbsmGetSlotInfo - Null entry\n");
        return (MBSM_FAILURE);
    }

    if (MBSM_PROTO_TAKE_SEM (MBSM_PROTO_SEM) != OSIX_SUCCESS)
    {
        MBSM_DBG (MBSM_TRC_CRITICAL, MBSM_UTIL,
                  "Error in MbsmGetSlotInfo - SemTake Failed.\n");
        return (MBSM_FAILURE);
    }

    if (MBSM_SLOT_MAP_ENTRY (i4SlotId))
    {
        *pSlotInfo = *(MBSM_SLOT_MAP_ENTRY (i4SlotId));
    }
    else
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_UTIL,
                  "Error in MbsmGetSlotInfo - Entry does not exist\n");
        i4RetVal = MBSM_FAILURE;
    }
    /* Get Card Name */
    i4RetVal =
        MbsmGetSlotCardName (i4SlotId, (INT1 *) pSlotInfo->au1SlotCardName);

    if (MBSM_PROTO_GIVE_SEM (MBSM_PROTO_SEM) != OSIX_SUCCESS)
    {
        MBSM_DBG (MBSM_TRC_CRITICAL, MBSM_PROTO,
                  "Error in MbsmGetSlotInfo - SemGive Failed.\n");
        i4RetVal = MBSM_FAILURE;
    }

    return (i4RetVal);
}

/*****************************************************************************
 *
 *    Function Name        : MbsmGetPortInfo
 *
 *    Description          : This function returns the port infomation from  
 *                           the Port Map table for the Slot specified.       
 *
 *    Input(s)             : i4SlotId - Slot Id to fetch the card name.
 *
 *    Output(s)            : pPortInfo - Pointer to the Port info structure.
 *
 *    Global Variables Referred : gaMbsmSlotMapTable
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion     : None.
 *
 *    Returns              : MBSM_SUCCESS or MBSM_FAILURE
 *
 *****************************************************************************/
INT4
MbsmGetPortInfo (INT4 i4SlotId, tMbsmPortInfo * pPortInfo)
{
    INT4                i4RetVal = MBSM_SUCCESS;

    if (!MBSM_IS_SLOT_ID_VALID (i4SlotId))
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_UTIL,
                  "Error in MbsmGetPortInfo - Invalid Slot Id\n");
        return (MBSM_FAILURE);
    }

    if (!pPortInfo)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_UTIL,
                  "Error in MbsmGetPortInfo - Null entry\n");
        return (MBSM_FAILURE);
    }

    if (MBSM_PROTO_TAKE_SEM (MBSM_PROTO_SEM) != OSIX_SUCCESS)
    {
        MBSM_DBG (MBSM_TRC_CRITICAL, MBSM_UTIL,
                  "Error in MbsmGetPortInfo - SemTake Failed.\n");
        return (MBSM_FAILURE);
    }

    if (MBSM_PORT_INFO_ENTRY (i4SlotId))
    {
        *pPortInfo = *(MBSM_PORT_INFO_ENTRY (i4SlotId));
    }
    else
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_UTIL,
                  "Error in MbsmGetPortInfo - Entry does not exist\n");
        i4RetVal = MBSM_FAILURE;
    }

    if (MBSM_PROTO_GIVE_SEM (MBSM_PROTO_SEM) != OSIX_SUCCESS)
    {
        MBSM_DBG (MBSM_TRC_CRITICAL, MBSM_PROTO,
                  "Error in MbsmGetPortInfo - SemGive Failed.\n");
        i4RetVal = MBSM_FAILURE;
    }

    return (i4RetVal);
}

/*****************************************************************************
 * Function Name : MbsmGetCtrlCardCount
 * Description   : Function to get the count of CFMs in the system
 * Input(s)      : None
 * Output(s)     : None  
 * Returns       : No. of CFMs that exists in the system 
 *****************************************************************************/
INT4
MbsmGetCtrlCardCount (VOID)
{
    INT4                i4SlotId;
    INT4                i4CtrlCardCount = 0;

    for (i4SlotId = MBSM_SLOT_INDEX_START;
         i4SlotId < (MBSM_MAX_SLOTS + MBSM_SLOT_INDEX_START); i4SlotId++)
    {
        if (gMbsmGlobalInfo.gaMbsmSlotMapTable == NULL)
        {
            break;
        }

        if ((MBSM_SLOT_INDEX_MODTYPE (i4SlotId) == MBSM_CONTROL_CARD) &&
            (MBSM_SLOT_INDEX_STATUS (i4SlotId) == MBSM_STATUS_ACTIVE))
        {
            i4CtrlCardCount++;
        }
    }

    return (i4CtrlCardCount);
}

/*****************************************************************************
 * Function Name : MbsmGetLoadSharingStatus
 * Description   : Function to get the status of LoadSharing flag.
 * Input(s)      : None
 * Output(s)     : None  
 * Returns       : Load-sharing flag 
 *****************************************************************************/
UINT1
MbsmGetLoadSharingStatus (VOID)
{
    return (MBSM_LOAD_SHARING_FLAG ());
}

INT4
MbsmLock (VOID)
{
    if (MBSM_TAKE_SEM (SELF, MBSM_SEM_NAME, OSIX_WAIT, 0) != OSIX_SUCCESS)
    {

        return MBSM_FAILURE;
    }

    return MBSM_SUCCESS;
}

INT4
MbsmUnLock (VOID)
{
    if (MBSM_RELEASE_SEM (SELF, MBSM_SEM_NAME) != OSIX_SUCCESS)
    {
        return MBSM_FAILURE;
    }

    return MBSM_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : MbsmGetBootFlag
 *
 *    Description          : This function returns the current value of        
 *                           boot flag.                  
 *
 *    Input(s)             : None. 
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gu4MbsmBootFlag.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion     : None.
 *
 *    Returns              : MBSM_TRUE or MBSM_FALSE
 *
 *****************************************************************************/
INT4
MbsmGetBootFlag (VOID)
{
    return MBSM_BOOT_FLAG ();
}

/*****************************************************************************
 *
 *    Function Name        : MbsmSetBootFlag
 *
 *    Description          : This function set the value for the boot flag.    
 *
 *    Input(s)             : u4Status.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : gu4MbsmBootFlag.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion     : None.
 *
 *    Returns              : None.
 *
 *****************************************************************************/
VOID
MbsmSetBootFlag (UINT4 u4Status)
{
    MBSM_BOOT_FLAG () = u4Status;
}

/*****************************************************************************/
/* Function Name      : MbsmStartTimer                                       */
/*                                                                           */
/* Description        : This function is called whenever a timer needs to    */
/*                      be started by any function.                          */
/*                                                                           */
/* Input(s)           : pMbsmAppTmrNode - Pointer to the timer node          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : MBSM_SUCCESS / MBSM_FAILURE                          */
/*****************************************************************************/
INT4
MbsmStartTimer (tMbsmAppTmrNode * pMbsmAppTmrNode)
{
    UINT4               u4TimerType;
    UINT4               u4Duration = 0;

    /* If the structure entry is null, then return failure */
    if (pMbsmAppTmrNode == NULL)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_UTIL, "TMR: Timer Node Null Pointer\n");

        return MBSM_FAILURE;
    }

    if (pMbsmAppTmrNode->MbsmTmrFlag != MBSM_TMR_STOPPED)
    {
        MBSM_DBG_ARG1 (MBSM_TRC_ERR, MBSM_UTIL,
                       "TMR: Stopping Timer running of type %d\n "
                       "before Restarting",
                       pMbsmAppTmrNode->MbsmAppTimer.u4Data);

        if (MbsmStopTimer (pMbsmAppTmrNode) != MBSM_SUCCESS)
        {
            MBSM_DBG (MBSM_TRC_ERR, MBSM_UTIL,
                      "TMR: Unable to STOP the RUNNING Timer\n");

            return MBSM_FAILURE;
        }
    }

    /*Based on the Timer type, start the corresponding timer for the node */
    u4TimerType = pMbsmAppTmrNode->MbsmAppTimer.u4Data;

    switch (u4TimerType)
    {
        case MBSM_RECVRY_TMR_TYPE:

            u4Duration = MBSM_RECVRY_TIME_OUT * SYS_TIME_TICKS_IN_A_SEC;
            MBSM_DBG_ARG1 (MBSM_TRC_ERR, MBSM_UTIL,
                           "Starting Recovery Timer for duration %d sec\n",
                           u4Duration);

            break;
    }

    if (MBSM_START_TMR (MBSM_TIMER_LIST_ID,
                        &pMbsmAppTmrNode->MbsmAppTimer,
                        u4Duration) != TMR_SUCCESS)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_UTIL, "TMR: Starting Timer FAILED\n");

        return MBSM_FAILURE;
    }

    MBSM_DBG (MBSM_TRC_ERR, MBSM_UTIL, "TMR: Started Timer \n");
    pMbsmAppTmrNode->MbsmTmrFlag = MBSM_TMR_RUNNING;

    return MBSM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MbsmStopTimer                                        */
/*                                                                           */
/* Description        : This function is called whenever a timer needs to    */
/*                      be stopped by any function.                          */
/*                                                                           */
/* Input(s)           : pMbsmAppTmrNode - Pointer to the timer node          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : MBSM_SUCCESS / MBSM_FAILURE                          */
/*****************************************************************************/
INT4
MbsmStopTimer (tMbsmAppTmrNode * pMbsmAppTmrNode)
{
    if (pMbsmAppTmrNode == NULL)
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_UTIL, "TMR: Timer Node Null Pointer\n");
        return MBSM_FAILURE;
    }

    if (pMbsmAppTmrNode->MbsmTmrFlag != MBSM_TMR_STOPPED)
    {
        if (MBSM_STOP_TMR (MBSM_TIMER_LIST_ID,
                           &pMbsmAppTmrNode->MbsmAppTimer) == TMR_FAILURE)
        {
            MBSM_DBG (MBSM_TRC_ERR, MBSM_UTIL, "TMR: Stopping Timer FAILED\n");
            return MBSM_FAILURE;
        }

        pMbsmAppTmrNode->MbsmTmrFlag = MBSM_TMR_STOPPED;

        MBSM_DBG (MBSM_TRC_ERR, MBSM_UTIL, "TMR: Timer Stopped SUCCESSFULLY\n");
    }

    return MBSM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MbsmTmrExpiryHandler                                 */
/*                                                                           */
/* Description        : This function is called to handle the timer expiry   */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
MbsmTmrExpiryHandler (VOID)
{
    tMbsmAppTmrNode    *pMbsmAppTmrNode;
#ifdef RM_WANTED
    tRmProtoEvt         ProtoEvt;
#endif

    while ((pMbsmAppTmrNode =
            (tMbsmAppTmrNode *) MBSM_GET_NEXT_EXPRD_TMR (MBSM_TIMER_LIST_ID))
           != NULL)
    {
        pMbsmAppTmrNode->MbsmTmrFlag = MBSM_TMR_STOPPED;

        /*Call the corresponding timer expiry handler 
         * based on the timer type */
        switch (pMbsmAppTmrNode->MbsmAppTimer.u4Data)
        {
            case MBSM_RECVRY_TMR_TYPE:
                /* Recovery timer expires which indicates that remote slot
                 * attach is not yet received during force switchover.But
                 * it should be given to CFA & Protocols during standby to
                 * active transition.
                 */
                if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
                {
#ifdef RM_WANTED
                    RmRestoreStaticConfig ();
#endif
                }
                else
                {
                    if (MbsmGenerateRemoteSlotDetach () != MBSM_SUCCESS)
                    {
                        MBSM_DBG (MBSM_TRC_ERR, MBSM_UTIL,
                                  "Error in generating detach for "
                                  "remote slot\n");
                    }

#ifdef RM_WANTED
                    gGiveGoActiveToOthers = MBSM_TRUE;
                    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
                    ProtoEvt.u4AppId = RM_MBSM_APP_ID;
                    ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
                    ProtoEvt.u4Error = RM_NONE;
                    RmApiHandleProtocolEvent (&ProtoEvt);
#endif

#if defined (RM_WANTED) && defined (NPAPI_WANTED)
                    /*Start H/w Audit */
                    MbsmRedHwAudit ();
#endif
                }
                break;
        }
    }
}

/*****************************************************************************
 *
 *    Function Name        : MbsmSetSlotInfo
 *
 *    Description          : This function is called to set the physical info
 *                           for container and module       
 *
 *    Input(s)             : i4SlotId - Slot Id to fetch the SlotInfo.
 *                           pMbsmSlotInfo - Slot information
 *
 *    Output(s)            : None.
 *
 *    Returns              : MBSM_SUCCESS or MBSM_FAILURE
 *
 *****************************************************************************/
PUBLIC INT4
MbsmSetSlotInfo (INT4 i4SlotId, tMbsmSlotInfo * pInSlotInfo)
{
    tMbsmSlotInfo      *pSlotInfo = NULL;
    UINT1               au1NullSerialNum[MBSM_ENT_PHY_SER_NUM_LEN];
    UINT1               au1NullAlias[MBSM_ENT_PHY_ALIAS_LEN];
    UINT1               au1NullAssetId[MBSM_ENT_PHY_ASSET_ID_LEN];
    UINT1               au1NullUris[MBSM_ENT_PHY_URIS_LEN];
    INT4                i4RetVal = MBSM_SUCCESS;
    MEMSET (au1NullSerialNum, 0XFF, MBSM_ENT_PHY_SER_NUM_LEN);
    MEMSET (au1NullAlias, 0XFF, MBSM_ENT_PHY_ALIAS_LEN);
    MEMSET (au1NullAssetId, 0XFF, MBSM_ENT_PHY_ASSET_ID_LEN);
    MEMSET (au1NullUris, 0XFF, MBSM_ENT_PHY_URIS_LEN);

    if (!MBSM_IS_SLOT_ID_VALID (i4SlotId))
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_UTIL,
                  "Error in MbsmSetSlotPhyInfo - Invalid Slot Id\n");
        return MBSM_FAILURE;
    }

    if (MBSM_PROTO_TAKE_SEM (MBSM_PROTO_SEM) != OSIX_SUCCESS)
    {
        MBSM_DBG (MBSM_TRC_CRITICAL, MBSM_UTIL,
                  "Error in MbsmSetSlotPhyInfo - SemTake Failed.\n");
        return MBSM_FAILURE;
    }

    if (MBSM_SLOT_MAP_ENTRY (i4SlotId))
    {
        pSlotInfo = (MBSM_SLOT_MAP_ENTRY (i4SlotId));
    }
    else
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_UTIL,
                  "Error in MbsmSetSlotPhyInfo - Entry does not exist\n");
        MBSM_PROTO_GIVE_SEM (MBSM_PROTO_SEM);
        return MBSM_FAILURE;
    }
    if (MEMCMP
        (pInSlotInfo->au1SerialNum, au1NullSerialNum,
         MBSM_ENT_PHY_SER_NUM_LEN) != 0)
    {
        MEMSET (pSlotInfo->au1SerialNum, 0, MBSM_ENT_PHY_SER_NUM_LEN);
        MEMCPY (pSlotInfo->au1SerialNum, pInSlotInfo->au1SerialNum,
                STRLEN (pInSlotInfo->au1SerialNum));
    }
    if (MEMCMP (pInSlotInfo->au1Alias, au1NullAlias, MBSM_ENT_PHY_ALIAS_LEN) !=
        0)
    {
        MEMSET (pSlotInfo->au1Alias, 0, MBSM_ENT_PHY_ALIAS_LEN);
        MEMCPY (pSlotInfo->au1Alias, pInSlotInfo->au1Alias,
                STRLEN (pInSlotInfo->au1Alias));
    }
    if (MEMCMP
        (pInSlotInfo->au1AssetId, au1NullAssetId,
         MBSM_ENT_PHY_ASSET_ID_LEN) != 0)
    {
        MEMSET (pSlotInfo->au1AssetId, 0, MBSM_ENT_PHY_ASSET_ID_LEN);
        MEMCPY (pSlotInfo->au1AssetId, pInSlotInfo->au1AssetId,
                STRLEN (pInSlotInfo->au1AssetId));
    }
    if (MEMCMP (pInSlotInfo->au1Uris, au1NullUris, MBSM_ENT_PHY_URIS_LEN) != 0)
    {
        MEMSET (pSlotInfo->au1Uris, 0, MBSM_ENT_PHY_URIS_LEN);
        MEMCPY (pSlotInfo->au1Uris, pInSlotInfo->au1Uris,
                STRLEN (pInSlotInfo->au1Uris));
    }
    MBSM_PROTO_GIVE_SEM (MBSM_PROTO_SEM);
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : MbsmGetShowCmdOutputAndCalcChkSum                    */
/*                                                                           */
/* Description        : This funcion handles the execution of show commands  */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu2SwAudChkSum                                       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : MBSM_SUCESS/MBSM_FAILURE                             */
/*****************************************************************************/
INT4
MbsmGetShowCmdOutputAndCalcChkSum (UINT2 *pu2SwAudChkSum)
{
#if (defined RM_WANTED && defined CLI_WANTED)
    if (RmGetNodeState () == RM_ACTIVE)
    {
        if (MbsmCliGetShowCmdOutputToFile ((UINT1 *) MBSM_AUDIT_FILE_ACTIVE) !=
            MBSM_SUCCESS)
        {
            MBSM_DBG (MBSM_TRC_ERR, MBSM_UTIL, "GetShRunFile Failed\n");
            return MBSM_FAILURE;
        }
        if (MbsmCliCalcSwAudCheckSum
            ((UINT1 *) MBSM_AUDIT_FILE_ACTIVE, pu2SwAudChkSum) != MBSM_SUCCESS)
        {
            MBSM_DBG (MBSM_TRC_ERR, MBSM_UTIL, "CalcSwAudChkSum Failed\n");
            return MBSM_FAILURE;
        }
    }
    else if (RmGetNodeState () == RM_STANDBY)
    {
        if (MbsmCliGetShowCmdOutputToFile ((UINT1 *) MBSM_AUDIT_FILE_STDBY) !=
            MBSM_SUCCESS)
        {
            MBSM_DBG (MBSM_TRC_ERR, MBSM_UTIL, "GetShRunFile Failed\n");
            return MBSM_FAILURE;
        }
        if (MbsmCliCalcSwAudCheckSum
            ((UINT1 *) MBSM_AUDIT_FILE_STDBY, pu2SwAudChkSum) != MBSM_SUCCESS)
        {
            MBSM_DBG (MBSM_TRC_ERR, MBSM_UTIL, "CalcSwAudChkSum Failed\n");
            return MBSM_FAILURE;
        }
    }
    else
    {
        MBSM_DBG (MBSM_TRC_ERR, MBSM_UTIL,
                  "Node State is neither active nor standby\n");
        return MBSM_FAILURE;
    }
#else
    UNUSED_PARAM (pu2SwAudChkSum);
#endif
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MbsmGetSelfAttachStatus                              */
/*                                                                           */
/* Description        : This funcion returns, whether the MBSM Self Attach   */
/*                      is finished or Not.                                  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu1SelfAttachRcvd                                    */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : MBSM_SUCESS/MBSM_FAILURE                             */
/*****************************************************************************/
PUBLIC INT4
MbsmGetSelfAttachStatus (VOID)
{
    if (gu1SelfAttachRcvd == MBSM_TRUE)
    {
        return MBSM_SUCCESS;
    }
    return MBSM_FAILURE;
}

/*****************************************************************************
 *
 *    Function Name        : MbsmGetPrevSlotsTotalPortCount
 *
 *    Description          : This function is used to get the total port counts of 
 *                           previous slots including connecting ports.       
 *
 *    Input(s)             : i4SelfSlot - slotid .
 *
 *    Output(s)            : pu4PortCount - total port count
 *
 *    Returns              : None
 *
 *****************************************************************************/
PUBLIC VOID
MbsmGetPrevSlotsTotalPortCount (INT4 i4SelfSlot, UINT4 *pu4PortCount)
{
    UINT4               u4Count = 0;
    INT4                i4SlotId = 0;
    tMbsmSlotInfo       MbsmSlotInfo;

    for (i4SlotId = MBSM_SLOT_INDEX_START; i4SlotId < i4SelfSlot; i4SlotId++)
    {
        MEMSET (&MbsmSlotInfo, 0, sizeof (tMbsmSlotInfo));
        if (MbsmGetSlotInfo (i4SlotId, &MbsmSlotInfo) == MBSM_FAILURE)
        {
            break;
        }
        u4Count = (u4Count + MBSM_MAX_PORTS_PER_SLOT +
                   MbsmSlotInfo.MbsmConnectingPortInfo.u4ConnectingPortCount);
    }
    *pu4PortCount = u4Count;
}

/***************************************************************************
 * FUNCTION NAME : MbsmWaitForRestorationComplete
 * DESCRIPTION   : This function waits for event from mib save restoration
 *                 function .
 *
 * INPUT         : NONE.
 *
 * OUTPUT        : NONE
****************************************************************************/
VOID
MbsmWaitForRestorationComplete ()
{
    UINT4               u4Event;
    OsixEvtRecv (MBSM_TASK_ID, MBSM_RESUME_EVENT, MBSM_EVENT_WAIT_FLAGS,
                 &u4Event);
    return;
}

/***************************************************************************
 * FUNCTION NAME :  MbsmInformRestorationComplete
 * DESCRIPTION   : This function sends an event to MBSM main task on completion
                   of mib save restoration
 *
 * INPUT         : NONE.
 *
 * OUTPUT        : NONE
****************************************************************************/
VOID
MbsmInformRestorationComplete ()
{
    OsixEvtSend (MBSM_TASK_ID, MBSM_RESUME_EVENT);
    return;
}

