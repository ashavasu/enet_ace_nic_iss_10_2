/*******************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lspp.h,v 1.14 2011/03/29 13:09:39 siva Exp $
 *
 * Description: This file contains type definitions for Lspp module.
 *********************************************************************/

#ifndef __LSPP_H__
#define __LSPP_H__

#include "mplsapi.h"

enum {
    LSPP_CREATE_CONTEXT_MSG = 1,
    LSPP_DELETE_CONTEXT_MSG,
    LSPP_BFD_BOOTSTRAP_ECHO_REQ,
    LSPP_BFD_BOOTSTRAP_ECHO_REPLY,
    LSPP_RX_PDU_MSG
};

/* Error codes returned to external modules.*/
enum {
    LSPP_MSG_ENQUEUE_FAILED = 1,
    LSPP_QMSG_MEM_ALLOC_FAILED,
    LSPP_MSG_TYPE_INVALID
};

#define LSPP_ENCAP_TYPE_DEFAULT                  0
#define LSPP_ENCAP_TYPE_IP                       1
#define LSPP_ENCAP_TYPE_ACH                      2
#define LSPP_ENCAP_TYPE_ACH_IP                   3
#define LSPP_ENCAP_TYPE_VCCV_NEGOTIATED          4
#define LSPP_ENCAP_TYPE_MAX                      5

#define LSPP_PATH_TYPE_LDP_IPV4             1
#define LSPP_PATH_TYPE_LDP_IPV6             2
#define LSPP_PATH_TYPE_RSVP_IPV4            3
#define LSPP_PATH_TYPE_RSVP_IPV6            4
#define LSPP_PATH_TYPE_PW                   5
#define LSPP_PATH_TYPE_MEP                  7

#define LSPP_NO_REPLY                       1


/* Echo reply return codes.*/
typedef enum {
    LSPP_NO_RETURN_CODE = 0,
    LSPP_MALFORMED_ECHO_REQ_RECEIVED,
    LSPP_ONE_OR_MORE_TLVS_NOT_UNDERSTOOD,
    LSPP_EGRESS_FOR_FEC_AT_STACK_DEPTH,
    LSPP_NO_MAPPING_FOR_FEC_AT_STACK_DEPTH,
    LSPP_DOWNSTREAM_MAPPING_MISMATCH,
    LSPP_UPSTREAM_IF_INDEX_UNKNOWN,
    LSPP_RESERVED_RETURN_CODE,
    LSPP_LBL_SWITCHED_AT_STACK_DEPTH,
    LSPP_LBL_SWITCHED_BUT_NO_MPLS_FWD_AT_STACK_DEPTH,
    LSPP_MAP_FAIL_FEC_TO_LBL_AT_STACK_DEPTH,
    LSPP_NO_LBL_ENTRY_AT_STACK_DEPTH,
    LSPP_PROTOCOL_MISMATCH,
    LSPP_PRE_TERMINATED_REQUEST,
    LSPP_MAX_RETURN_CODE
}tLsppErrorCode;



typedef struct
{
    UINT4    u4BfdSessionIndex;
    UINT4    u4BfdDiscriminator;
    UINT4    u4WFRInterval;
    UINT4    u4LsppEncapType; /*Encap to be used in LSP Ping. If 
                                default encap is used LSPP will 
                               use the default available encap for 
                               the path*/
}tLsppBfdBtStrapInfo;


typedef tMplsNodeId        tLsppNodeId;
typedef tMplsMegMeName     tLsppMegMeName;
typedef tMplsMegId         tLsppMegIndices;


typedef struct
{
    UINT4          u4AddrType;
    tIpAddr        LdpPrefix;
    UINT4          u4PrefixLength;
}tLsppLdpInfo;


typedef struct
{
    tLsppMegMeName      MegName;
    tLsppMegIndices     MegIndices;
}tLsppMegInfo;



typedef struct
{
    tLsppNodeId    SrcNodeId;
    tLsppNodeId    DstNodeId;
    UINT4          u4SrcTnlId;
    UINT4          u4DstTnlId;
    UINT4          u4LspId;
    UINT4          u4InIf;           /* Incoming interface */
}tLsppTnlInfo;

typedef struct
{
    tIpAddr        PeerAddr;
    UINT4          u4PwVcId;
}tLsppPwInfo;


typedef struct
{
    union
    {
        tLsppLdpInfo      LdpInfo;
        tLsppTnlInfo      TnlInfo;
        tLsppPwInfo       PwInfo;
        tLsppMegIndices   MepIndices;
        tLsppMegMeName    MegName;
    }unPathId;
    UINT1           u1PathType;
    UINT1 au1Pad[3];
}tLsppPathId;

typedef struct
{
    tLsppPathId          LsppPathId;
    tLsppBfdBtStrapInfo  BfdBtStrapInfo;
}tLsppExtTrigInfo;



typedef struct
{
    tCRU_BUF_CHAIN_HEADER  *pBuf;
    tLsppPathId            PathId;
    tIpAddr                DestIpAddr;
    tLsppErrorCode         eErrorCode;
    UINT4                  u4OffSet;
    UINT4                  u4SrcUdpPort;
    UINT4                  u4OutIfIndex;
    UINT4                  u4BfdDiscriminator;
    UINT4                  u4SessionIndex;
    UINT1                  au1NextHopMac[MAC_ADDR_LEN];
    UINT1                  u1ReplyPath;      /* LSP/IP */
    UINT1                  NextHopIpAddrType; /* IPv4/IPv6 */
    BOOL1                  bIsEgress;          /* In egress/ In Ingress */
    UINT1                  au1Pad[3];
}tLsppBfdInfo;


typedef struct LsppRxPduInfo
{
    tCRU_BUF_CHAIN_DESC *pBuf;    /* Pointer to the LSP Ping PDU buffer */
    UINT4               u4IfIndex; /*Interface on which the packet is received*/
    /* Need to add pad if needed */
}tLsppRxPduInfo;



typedef struct LsppQMsg
{
    UINT4    u4ContextId;  /* Virtual context id maintained by VCM module */
    union
    {
        tLsppExtTrigInfo   LsppExtTrigInfo; /*BFD external Trigger Info */
        tLsppRxPduInfo     LsppRxPduInfo;   /*PDU Message recevied from MPLS */
        tLsppBfdInfo       LsppBfdInfo;
    }unMsgParam;
    UINT1    u1MsgType;    /* Message type */
    UINT1    au1Pad[3];
}tLsppReqParams;


typedef struct
{
    UINT1      u1ErrorCode;
    UINT1      au1Pad[3];
}tLsppRespParams;

PUBLIC VOID LsppTaskSpawnLsppTask PROTO ((INT1 *));
PUBLIC INT4 LsppApiHandleExtRequest (tLsppReqParams *pLsppReqParams, 
                                     tLsppRespParams *pLsppRespParams);
PUBLIC INT4 LsppUtilGetPathPointer (UINT4 u4ContextId, 
                                    tLsppPathId * pLsppPathId, 
                                    UINT4 *pu4ServiceOid, 
                                    INT4 *pi4OidLength);
#endif

