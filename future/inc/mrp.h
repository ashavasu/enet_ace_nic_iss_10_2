/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mrp.h,v 1.13 2012/06/25 12:50:10 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              by  external modules.
 ********************************************************************/
#ifndef _MRP_H
#define _MRP_H

#define MRP_ENABLED                 1
#define MRP_DISABLED                2

#define MRP_STAP_CIST_ID  MST_CIST_CONTEXT

#define MRP_OPER_UP                 1
#define MRP_OPER_DOWN               2

#define MRP_TRC_MAX_SIZE       256

#define MRP_MVRP_ETHER_TYPE         0x88F5
#define MRP_MMRP_ETHER_TYPE         0x88F6

#define MRP_CONTEXT_ID_OFFSET 14
#define MRP_CONTEXT_ID_LEN 4

/* These MACROs is used by the STP and L2IWF modules to indicate 
 * the MRP, whenever the following changes haapens 
 * - Port role changed 
 * - Port state is changed 
 * - Port Oper P2P 
 * - TC Detected timer status */
#define MSG_PORT_ROLE_CHANGE        1
#define MSG_PORT_STATE_CHANGE       2
#define MSG_PORT_OPER_P2P           3
#define MSG_TC_DETECTED_TMR_STATUS  4


typedef struct MrpHwInfo {
    tMbsmSlotInfo         *pSlotInfo;
    UINT4                 u4ContextId;
    UINT4                 u4Status; /* Application status
                                       MRP_ENABLED/MRP_DISABLED */
} tMrpHwInfo;

typedef struct MrpMsg {
   UINT2                u2MsgType;
   UINT2                u2Port;    /*Local Port Identifier*/
   UINT2                u2MapId;
   tMacAddr             MacAddr;
   tLocalPortList       AddedPorts;
   tLocalPortList       DeletedPorts;
} tMrpMsg;

typedef struct MrpBulkMsg {
   UINT2                u2MsgType;
   UINT2                u2Port;
   tVlanId              VlanId;
   tMacAddr             MacAddr;
   tLocalPortList       Ports;
} tMrpBulkMsg;

typedef struct MrpVidMapMsg {
    UINT2           u2MsgEvent;/* Can be map/unmap VLAN List or single VLAN */
    UINT2           u2NewMapId;  /* Can be Instance Id (or Fid) */
    INT4            i4EventCount; /* No of mapping posted - VlanMapInfo index */
    tVlanMapInfo    VlanMapInfo[1]; /* When List of VLAN is mapped */
} tMrpVidMapMsg;

typedef struct MrpRmCtrlMsg {
    tRmMsg           *pData;     /* RM message pointer */
    UINT2            u2DataLen; /* Length of RM message */
    UINT1            u1Event;   /* RM event */
    UINT1            u1Reserved;
} tMrpRmCtrlMsg;

typedef struct MrpQMsg {
   INT4                  i4Count;    /* Count of msgs in union - 
                                        No. of Bulk/VidMap Messages */
   UINT4                 u4ContextId; /*Context  Identifier*/
   UINT4                 u4IfIndex;   /*Interface Index*/
   UINT2                 u2MsgType;
   UINT1                 u1MsgInfo; /* TcDetected Timer Status / OperP2P /
                                     * Flush or Redeclare event 
                                     */
   UINT1                 au1Reserved[1];
   union
   {
       tMrpRmCtrlMsg  MrpRmMsg; /* Control Msg from RM Module */
       tMrpBulkMsg    MrpBulkMsg [1]; /* Bulk Array of messages from VLAN at 
                                        MVRP/MMRP init time */
       tMrpMsg        MrpMsg;
       tMrpVidMapMsg  MrpVidMapMsg[1];
   }
   unMrpMsg;
    /* !!!!!!!!!!!!!!!!!!!!!!!!CAUTION!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     * DONT DECLARE ANY VARIABLES BELOW THIS UNION. IF DECLARED, THEN THOSE
     * VARIABLES MAY GET CORRUPTED BECAUSE THERE ARE DYNAMICALLY GROWING 
     * ARRAYS INSIDE THIS UNION. 
     * REFER: MrpFillBulkMessage &  MrpUpdateInstVlanMap */
}tMrpQMsg;

/* This structure is used by VLAN module to post the VLAN related information 
 * to MRP module
 */
typedef struct MrpVlanInfo {
   tMrpQMsg             **ppMrpQMsg;
   tPortList            *pAddedPorts;
   tPortList            *pDeletedPorts;
   tMacAddr             MacAddr;
   tVlanId              VlanId;
   UINT4                u4ContextId;
   UINT4                u4IfIndex;
   UINT1                u1MsgType;
   UINT1                u1SubMsgType;
   UINT1                au1Reserved [2];
} tMrpVlanInfo;

typedef struct MmrpStatusInfo
{
    UINT4 u4ContextId;
    UINT4 *pu4ErrorCode;
    INT4  *pi4MmrpEnabledStatus;
    INT4  i4MmrpEnabledStatus;
}tMmrpStatusInfo;

typedef struct MvrpStatusInfo
{
    UINT4 u4ContextId;
    UINT4 *pu4ErrorCode;
    INT4  *pi4MvrpEnabledStatus;
    INT4  i4MvrpEnabledStatus;
}tMvrpStatusInfo;

typedef struct _MvrpPortInfo
{
    tSNMP_COUNTER64_TYPE *pu8PortMvrpFailedRegist;
    UINT4 u4ContextId;
    UINT4 *pu4NextContextId;
    UINT4 u4IfIndex;
    UINT4 *pu4NextIfIndex;
    UINT4 *pu4ErrorCode;
    tMacAddr *pPortMvrpLastPduOrigin;
    INT4 *pi4PortMvrpEnabledStatus;
    INT4 i4PortMvrpEnabledStatus;
}tMvrpPortInfo;


typedef struct _MrpConfigInfo
{
    UINT4     u4InfoType;
    union
    {
        tMmrpStatusInfo MmrpStatusInfo;
        tMvrpStatusInfo MvrpStatusInfo;
        tMvrpPortInfo   MvrpPortInfo;
    }unMrpInfo;
}tMrpConfigInfo;

/* Enum to indicate the information type to be set or queried from VLAN */
enum {
    MMRP_ENABLED_STATUS_GET = 1,
    MMRP_ENABLED_STATUS_SET,
    MMRP_ENABLED_STATUS_TEST,
    MVRP_ENABLED_STATUS_GET,
    MVRP_ENABLED_STATUS_SET,
    MVRP_ENABLED_STATUS_TEST,
    MVRP_PORT_ENABLED_STATUS_GET,
    MVRP_PORT_FAILED_REGIST_GET,
    MVRP_PORT_PDU_ORIGIN_GET,
    MVRP_PORT_ENABLED_STATUS_SET,
    MVRP_PORT_ENABLED_STATUS_TEST
};

VOID MrpMainTask PROTO ((INT1 *));

INT4 MrpLock     PROTO ((VOID));
INT4 MrpUnLock   PROTO ((VOID));

#define MRP_LOCK()    MrpLock ()
#define MRP_UNLOCK()  MrpUnLock ()

INT4
MrpApiGetMmrpAddr PROTO ((tMacAddr *MacAddr));
INT4
MrpApiGetMvrpAddr PROTO ((UINT4 u4ContextId ,tMacAddr *MacAddr));
INT4
MrpApiIsMrpStarted PROTO ((UINT4 u4ContextId));
INT4 
MrpApiIsMvrpEnabled PROTO ((UINT4 u4ContextId));
INT4
MrpApiIsMmrpEnabled PROTO ((UINT4 u4ContextId));

VOID
MrpApiMvrpPropagateVlanInfo          PROTO ((UINT4        u4ContextId,
                                       tVlanId      VlanId,
                                       tLocalPortList    AddPortList, 
                                       tLocalPortList    DelPortList));

VOID
MrpApiMvrpSetVlanForbiddPorts      PROTO ((UINT4        u4ContextId,
                                       tVlanId      VlanId,
                                       tLocalPortList    AddPortList,
                                       tLocalPortList    DelPortList));

VOID
MrpApiMmrpPropagateMacInfo         PROTO ((UINT4        u4ContextId,
                                       tMacAddr     MacAddr, 
                                       tVlanId      VlanId,
                                       tLocalPortList    AddPortList, 
                                       tLocalPortList    DelPortList));

VOID
MrpApiMmrpSetMcastForbiddPorts     PROTO ((UINT4        u4ContextId,
                                       tMacAddr     MacAddr, 
                                       tVlanId      VlanId,
                                       tLocalPortList    AddPortList,
                                       tLocalPortList    DelPortList));

VOID
MrpApiMmrpPropagateDefGroupInfo      PROTO ((UINT4        u4ContextId,
                                       UINT1        u1Type, 
                                       tVlanId      VlanId,
                                       tLocalPortList    AddPortList, 
                                       tLocalPortList    DelPortList));

VOID
MrpApiMmrpSetDefGrpForbiddPorts  PROTO ((UINT4        u4ContextId,
                                       UINT1        u1Type, 
                                       tVlanId      VlanId,
                                       tLocalPortList    AddPortList,
                                       tLocalPortList    DelPortList));

VOID
MrpApiMrpMapUpdateMapPorts          PROTO ((UINT4 u4ContextId,
                                       tLocalPortList AddPortList, 
                                       tLocalPortList DelPortList, 
                                       UINT2 u2MapId)); 

INT4 MrpApiCreatePort            PROTO ((UINT4, UINT4, UINT2 u2Port));
INT4 MrpApiDeletePort            PROTO ((UINT4 u4IfIndex));
INT4 MrpApiPortOperInd           PROTO ((UINT4 u4IfIndex, UINT1 u1OperStatus));
INT4 MrpApiCreateContext         PROTO ((UINT4 u4ContextId));
INT4 MrpApiDeleteContext         PROTO ((UINT4 u4ContextId));
INT4
MrpApiPortMapIndication   PROTO ((UINT4 , UINT4 , UINT2 u2Port));
INT4
MrpApiPortUnmapIndication PROTO ((UINT4 u4IfIndex));
INT4
MrpApiUpdateContextName PROTO ((UINT4 u4ContextId));

INT4
MrpApiFillBulkMessage (UINT4 u4ContextId, tMrpQMsg ** ppMrpQMsg,
                       UINT1 u1MsgType, tVlanId VlanId, 
         UINT2 u2Port, tLocalPortList Ports, UINT1 *pu1MacAddr);
INT4
MrpApiPostBulkCfgMessage PROTO ((tMrpQMsg *pMrpQMsg));

/* For posting the received packet to MRP queue */
INT4
MrpApiEnqueueRxPkt PROTO ((tCRU_BUF_CHAIN_DESC * pFrame, UINT4 u4IfIndex,
                        UINT2 u2MapId));
INT4
MrpApiNotifyPortOperP2PChg PROTO ((UINT4  u4IfIndex, BOOL1 bOperP2PStatus));
INT4
MrpApiNotifyTcDetectedTmrState PROTO ((UINT4 u4IfIndex, UINT2 u2MapID,
                                       UINT1 u1TmrState));
PUBLIC INT4
MrpApiNotifyStpInfo PROTO ((tMrpInfo *pMrpInfo));
INT4
MrpApiNotifyPortRoleChange PROTO ((UINT4 u4IfIndex, UINT2 u2MapID,
                                   UINT1 u1OldRole, UINT1 u1NewRole));
INT4
MrpApiNotifyPortStateChange PROTO ((UINT4 u4IfIndex, UINT2 u2MapId,
                                    UINT1 u1State));
VOID
MrpApiUpdateInstVlanMap PROTO ((UINT4 u4ContextId, UINT1 u1Action, 
                                UINT2 u2NewMapId, UINT2 u2OldMapId,
                                tVlanId VlanId, UINT2 u2MapEvent,
                                UINT1 *pu1Result));
#ifdef MBSM_WANTED
INT4
MrpMbsmProcessUpdate (tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo,
            INT4 i4Event);
#endif

#ifdef L2RED_WANTED
INT4
MrpApiRcvPktFromRm PROTO ((UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen));
#endif /* L2RED_WANTED */
INT4
MrpApiStartModule PROTO ((VOID));
INT4
MrpApiShutdownModule PROTO ((VOID));

INT4
MrpApiPropVlanDelFromEsp PROTO ((UINT4 u4ContextId, tVlanId VlanId));

INT4
MrpApiRemVlanAddedInEsp PROTO ((UINT4 u4ContextId, tVlanId VlanId));

INT4 
MrpApiEnqueueRxPktOnBackPlane PROTO ((tCRU_BUF_CHAIN_DESC * pFrame,
          tCfaBackPlaneParams * pBackPlaneParams,
          UINT4 u4IfIndex, UINT2 u2MapId));
INT4
MrpApiNotifyVlanInfo PROTO ((tMrpVlanInfo *pVlanInfo));

INT4
MrpApiMrpConfigInfo PROTO ((tMrpConfigInfo *pMrpConfigInfo));


#endif/*_MRP_H*/
/*-----------------------------------------------------------------------*/
/*                       End of the file  mrp.h                        */
/*-----------------------------------------------------------------------*/
