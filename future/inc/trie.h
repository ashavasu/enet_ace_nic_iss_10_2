/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: trie.h,v 1.9 2015/04/03 06:37:17 siva Exp $
 *
 * Exported functions/types/defines from FutureTrie module.
 *
 *******************************************************************/
#ifndef _TRIE_H
#define _TRIE_H

#define TRIE_SUCCESS      0
#define TRIE_FAILURE    (-1)

typedef struct _RadixNodeHead * tTrieId;

typedef union _Key {
    UINT1  *pKey;
    UINT4  u4Key;
} tKey;

typedef struct _InputParams {
    tTrieId   pRoot;
    VOID     *pLeafNode;
    INT1      i1AppId;
    UINT1     u1PrefixLen;
    UINT1     u1Reserved1;
    UINT1     u1Reserved2;
    tKey      Key;
} tInputParams;

#define MAX_APPLICATIONS     64

typedef struct _OutputParams {
    void   *pAppSpecInfo;
    tKey   Key;
    UINT4  au4NoOfMultipath[MAX_APPLICATIONS];
} tOutputParams;

typedef struct _ScanOutParams {
    void   *pAppSpecInfo;
    tKey   *pKey;
    UINT4  u4NumEntries;
} tScanOutParams;

typedef tScanOutParams tDeleteOutParams;


struct TrieAppFns 
{
    INT4 (*pAddAppSpecInfo) (tInputParams *pInputParams,
                             VOID *pOutputParams, VOID **ppAppPtr,
                             VOID *pAppSpecInfo);
    INT4 (*pDeleteEntry)    (tInputParams *pInputParams, VOID **ppAppPtr,
                             VOID *pOutputParams, VOID *pNxtHop, tKey Key);
    INT4 (*pSearchEntry)    (tInputParams *pInputParams, VOID *pOutputParams,
                             VOID *pAppPtr);
    INT4 (*pLookupEntry)    (tInputParams *pInputParams, VOID *pOutputParams, 
                             VOID *pAppPtr, UINT2    u2KeySize, tKey Key);
    VOID *(*pInitScanCtx)   (tInputParams *pInputParams,
                             void (*AppSpecScanFunc) (VOID *),VOID *pScanOutParams); 
    VOID (*pDeInitScanCtx)  (VOID *pScanCtx);
    INT4 (*pScan)           (VOID  *pScanCtx, VOID **ppAppPtr, tKey Key);
    INT4 (*pScanAll)        (VOID *pScanCtx, VOID *pAppPtr, tKey Key);
    INT4 (*pUpdate)         (tInputParams *pInputParams, VOID *pOutputParams,
                             VOID **ppAppPtr, void *pAppSpecInfo, UINT4 u4NewMetric);
    VOID *(*pInitDeleteCtx) (tInputParams *pInputParams, void (*AppSpecDelFunc)
                             (VOID *), VOID *pDeleteOutParams);
    VOID (*pDeInitDeleteCtx)(VOID *pDelCtx);
    INT4 (*pDelete)         (VOID *pDelCtx, VOID **ppAppPtr, tKey Key);
    INT4 (*pGetLeafInfo)    (tInputParams *pInParms, VOID * pOutParms);
    INT4 (*pBestMatch)      (UINT2  u2KeySize, tInputParams * pInputParams,
                             VOID *pOutputParams, VOID *pAppPtr, tKey Key);
    INT4 (*pGetNextEntry)   (tInputParams *pInputParams, VOID *pOutputParams,
                             VOID *pAppPtr, tKey Key);
    INT4 (*pLookupEntryOverlap) (tInputParams *pInputParams,
                                 VOID *pOutputParams,
                                 VOID *AppSpecPtr,  UINT2 u2KeySize, tKey Key);
};

typedef struct _CrtParams {
    struct  TrieAppFns *AppFns;
    UINT4   u4Type;
    UINT4   u4NumRadixNodes;
    UINT4   u4NumLeafNodes;
    UINT4   u4NoofRoutes;
    int     u2KeySize;
    int     u1AppId;
    BOOLEAN bPoolPerInst;  /* TRUE - Indicates MEMPOOL per Trie instance
                            * FALSE - Indicates common MEMPOOL */
    BOOLEAN bSemPerInst;   /* TRUE - Indicates Sem per TrieInstance
                            * FALSE - Indicates common MEMPOOL */
    BOOLEAN bValidateType; /* TRUE - To Validate u4Type field 
                            * FALSE - u4Type field is invalid */
    UINT1   u1Reserved;
} tTrieCrtParams;

/* Exported Functions */

INT4 TrieLibInit (void);   
INT4 TrieLibMemPoolInit (void);   
void TrieLibShut (void);   

tTrieId
     TrieCrt     (tTrieCrtParams * pCreateParams);

tTrieId
     TrieCreateInstance    (tTrieCrtParams * pCreateParams);

INT4 TrieDel     (tInputParams * pInputParams, void (*AppSpecDelFunc) (void*),
                  void * pDeleteOutParams);

tTrieId
     TrieCrtInstance     (tTrieCrtParams * pCreateParams);

INT4 TrieDelInstance     (tInputParams * pInputParams, void (*AppSpecDelFunc) (void*),
                  void * pDeleteOutParams);

INT4 TrieAdd     (tInputParams * pInputParams, void *pAppSpecInfo,
                  VOID *pOutputParams);

INT4 TrieRemove  (tInputParams  *pInputParams, VOID *pOutputParams,   
                  VOID *pNxtHop);   

INT4
TrieSearch (tInputParams * pInputParams, VOID *pOutputParams, VOID **ppNode);

INT4
TrieLookup (tInputParams * pInputParams, VOID *pOutputParams, VOID **ppNode);

INT4 TrieWalk    (tInputParams *pInputParams, 
                  INT4         (*AppSpecScanFunc)(VOID *),   
                  VOID         *pScanOutParams);   

INT4
TrieGetNext (tInputParams * pInputParams, VOID *pOutputParams, VOID **ppNode);

INT4 TrieLookupOverlap (tInputParams * pInputParams, VOID * pOutputParams,
                        VOID **);
INT4 TrieGetPrevNode  (tInputParams *pInputParams, VOID *pGetCtx,
                       VOID ** pAppSpecPtr, VOID **);
INT4 TrieGetFirstNode (tInputParams *pInputParams, VOID ** pAppSpecPtr,
                       VOID **);
INT4 TrieGetNextNode  (tInputParams *pInputParams, VOID *pGetCtx,
                       VOID ** pAppSpecPtr, VOID **);
INT4 TrieGetLastNode  (tInputParams *pInputParams, VOID ** pAppSpecPtr,
                       VOID **);

INT4  TrieRevise       (tInputParams *, void *pAppSpecInfo, UINT4 u4NewMetric,
                        VOID *pOutputParams);
INT4 TrieLookupOverlapLessMoreSpecific  (tInputParams * pInputParams, VOID * pOutputParams,
                              VOID **ppNode);
#endif
