/********************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *  
 *  $Id: radioif.h,v 1.37 2017/12/12 13:28:30 siva Exp $
 *  
 *  Description: This file contains type definitions for RadioIf module.
 * *********************************************************************/

#ifndef _RADIOIF_H_
#define _RADIOIF_H_

#include "cfa.h"
#if defined(WPS_WANTED) || defined(WPS_WTP_WANTED)
#include "wps.h"
#endif

#define RADIOIF_ANTENNA_LIST_INDEX       255
#define RADIOIF_INFO_ELEMENT             100
#define RADIOIF_OPER_RATE                8
#define RADIOIF_MAX_POWER_LEVEL          8
#define RADIOIF_MAX_POWER                30
#define RADIOIF_COUNTRY_STR_LEN          4
#define RADIO_COUNTRY_LENGTH             3 
#define RADIOIF_VALUE_HUNDRED            100 

/* Macros for 802.11n */
#define   RADIOIF_11N_MCS_RATE_LEN         32 
#define   RADIOIF_11N_MCS_RATE_LEN_PKT     16
#define   RADIO_DOT11N_CHANNELWIDTH_20     0
#define   RADIO_DOT11N_CHANNELWIDTH_40     1
#define   RADIO_DOT11N_SGI_DISABLE         0
#define   RADIO_HTOPE_INFO      5
#define   RADIO_BASIC_MCS_SET     16
#define   RADIO_VALUE_0                    0
#define   RADIO_VALUE_1                    1
#define   RADIO_VALUE_2                    2
#define   RADIO_VALUE_3                    3
#define   RADIO_VALUE_4                    4
#define   RADIO_VALUE_5                    5
#define   RADIO_VALUE_6                    6
#define   RADIO_VALUE_7                    7
#define   RADIO_VALUE_8                    8
#define   RX_STBC_VALUE_DISABLE     0
#define   RX_STBC_VALUE_ENABLE     1
#define   RADIO_11AC_RX_STS                0x70
#define   RADIO_DOT11N_MCSRATE_ENABLE      0xFF 
#define   RADIO_NSUPPORT_DISABLE                      0
#define   RADIO_NSUPPORT_ENABLE                       1
#define   RADIO_DOT11N_HT_MIN                         0
#define   RADIO_DOT11N_HT_MAX                         255
#define   RADIO_DOT11N_MAX_SUPP_MCS_MIMO1             7
#define   RADIO_DOT11N_MAX_SUPP_MCS_MIMO2             15
#define   RADIO_DOT11N_MAX_SUPP_MCS_MIMO3             23
#define   RADIO_DOT11N_MAX_SUPP_MCS_MIMO4             31
#define   RADIO_DOT11N_MIN_MAN_MCS                    0
#define   RADIO_DOT11N_MAX_MAN_MCS                    31
#define   RADIO_DOT11N_MIN_TX_ANTENNA                 0
#define   RADIO_DOT11N_MAX_TX_ANTENNA                 255
#define   RADIO_DOT11N_MIN_RX_ANTENNA                 0
#define   RADIO_DOT11N_MAX_RX_ANTENNA                 255
#define   RADIO_DOT11N_SGI_DISABLE                    0
#define   RADIO_DOT11N_SGI_ENABLE                     1
#define   RADIO_DOT11N_MCS_BITMAP_SIZE                32
#define   RADIO_DOT11N_MCSTXRATE_DISABLE              0
#define   RADIO_DOT11N_MCSTXRATE_ENABLE               1
#define   RADIO_DOT11N_HTCAPINFO_DEFAULT              0x2C
#define   RADIO_DOT11N_HTFLAG_DEFAULT                 0x18
#define   RADIO_DOT11N_AMPDU_DEFAULT                  0x1b
#define   RADIO_DOT11N_LDPC_ENABLE        0x1
#define   RADIO_DOT11N_CHANNEL_WIDTH_20   0x0
#define   RADIO_DOT11N_SM_POWER_SAVE_DISABLE  0x3
#define   RADIO_DOT11N_TX_STBC_ENABLE   0x1
#define   RADIO_DOT11N_RX_STBC_ONE_SPATIAL_STREAM   0x1
#define   RADIO_DOT11N_DSSS_CCK_MODE_40MHZ  0x1
#define   RADIO_DOT11N_AMPDU_MAX_LEN   0x3
#define   RADIO_DOT11N_AMPDU_TIME_SPACING  0x07
#define   RADIO_DOT11N_MCS_STREAM_MAX          3
#define   RADIO_DOT11N_MCS_BIT_MAX             8
#define   RADIO_DOT11N_SHIFT_BIT               1
#define RADIO_DOT11N_TXMCSSETDEF_DISABLE        0
#define RADIO_DOT11N_TXMCSSETDEF_ENABLE         1
#define RADIO_DOT11N_TCRXMCSSETNOTEQUAL_DISABLE 0
#define RADIO_DOT11N_TCRXMCSSETNOTEQUAL_ENABLE  1
#define RADIO_DOT11N_TXMAXNOSPATSTRM_DISABLE    0
#define RADIO_DOT11N_TXUNEQUALMODSUPP_DISABLE   0
#define RADIO_DOT11N_TXRXMCSSSETNOTEQUAL_DISABLE        0
#define RADIO_DOT11N_TXMCSSETDEFINED_DISABLE            0
#define RADIO_MAX_DFS_CHANNEL       16
#define RADIO_SET_CHAN40_CONFIG                         5 
#define RADIO_DEFAULT_NON_GREEN_FIELD                   4 
#define   RADIO_DOT11N_ENABLE                           1
#define   RADIO_DOT11N_DISABLE                          0
#define   RADIO_DOT11N_DEF_AMPDU_SUBFRAME               32
#define   RADIO_DOT11N_DEF_AMPDU_LIMIT                  65535
#define   RADIO_DOT11N_DEF_AMSDU_LIMIT                  4096


/*Macros for 11AC */
#define   RADIO_11AC_MPDU_DEFAULT                     0x2
#define   RADIO_11AC_RX_LDPC_DEFAULT                  0x1
#define   RADIO_11AC_SHORTGI80_ENABLE                 0x1
#define   RADIO_11AC_SHORTGI80_DISABLE                0
#define   RADIO_11AC_STBC_ENABLE                   0x1
#define   RADIO_11AC_STBC_DISABLE                  0
#define   RADIO_11AC_MAX_RX_STBC                   0x1
#define   RADIO_11AC_AMPDU_DEFAULT                    0x7
#define   RADIO_11AC_MCS_ENABLE                       {0xEA, 0xFF, 0x00, 0x00, 0xEA, 0xFF, 0x00, 0x00}
#define   RADIO_11AC_MCS_DISABLE                      {0XFF, 0xFF, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00}
#define   RADIO_11AC_CHANWIDTH_DEFAULT                0x4
#define   RADIO_11AC_CHANWIDTH80                      0x1
#define   RADIO_11AC_CHANWIDTH160                     0x2
#define   RADIO_11AC_CHANWIDTH80P80                   0x3
#define   RADIO_11N_CHANWIDTH20                       20
#define   RADIO_11N_CHANWIDTH40                       40
#define   RADIO_11AC_SUPP_CENTER_FCY                  0xC8
#define   RADIO_11AC_CENTER_FCY_DEFAULT               0x2A
#define   RADIO_11AC_OPERMCS_DEFAULT                  0xEAFF
#define   RADIO_11AC_VHT_CAPA_MCS_LEN             8
#define   RADIO_11AC_VHT_OPER_INFO_LEN            3
#define   RADIO_11AC_VHT_MCS_DISABLE              0xFF
#define   RADIO_11AC_VHT_MCS_SHIFT            3
#define   RADIO_11AC_VHT_MCS_SHIFT_BITS           2
#define   RADIO_11AC_OPER_NOTIFY_ENABLE               1
#define   RADIO_11AC_OPER_NOTIFY_DISABLE              0
#define   RADIO_VHT_MCS_DISABLE                       3
#define   RADIO_BANDWIDTH0_FACTOR                     52
#define   RADIO_BANDWIDTH1_FACTOR                     108
#define   RADIO_BANDWIDTH2_FACTOR                     234
#define   RADIO_BANDWIDTH3_FACTOR                     468
#define   RADIO_SGI_FACTOR                            3.6
#define   RADIO_LGI_FACTOR                            4
#define   RADIO_MCS0_FACTOR                           0.5
#define   RADIO_MCS1_FACTOR                           1
#define   RADIO_MCS2_FACTOR                           1.5
#define   RADIO_MCS3_FACTOR                           2
#define   RADIO_MCS4_FACTOR                           3
#define   RADIO_MCS5_FACTOR                           4
#define   RADIO_MCS6_FACTOR                           4.5
#define   RADIO_MCS7_FACTOR                           5
#define   RADIO_MCS8_FACTOR                           6
#define   RADIO_MCS9_FACTOR                           6.67
#define RADIO_11N_FRAG_MINRANGE                       256
#define RADIO_11AC_FRAG_MAXRANGE                      11500
#define RADIO_11N_MAXRANGE                            8000
#define RADIO_FRAG_MINRANGE                           450
#define RADIO_FRAG_MAXRANGE                           3000

#define RADIOIF_DEF_RTS_THRESHOLD        2346
#define RADIOIF_DEF_SHORT_RETRY          7
#define RADIOIF_DEF_LONG_RETRY           4
#define RADIOIF_DEF_FRAG_THRESHOLD       1000
#define RADIOIF_DEF_TX_LIFETIME          512
#define RADIOIF_DEF_RX_LIFETIME          512
#define RADIOIF_DEF_ANT_COUNT            1 
#define RADIOIF_DEF_ANT_MODE             3 
#define RADIO_ANT_INTERNAL               1
#define RADIO_ANT_DIVERSITY_DISABLED     0
#define RADIOIF_DEF_POWER_LEVEL          1
#define RADIOIF_DEF_ENERGY_THRESHOLD  -62

#define RADIO_BAND_A                                  5
#define ROGUE_RADIO_A                                  5
#define RADIO_BAND_B                                  1
#define RADIO_TYPE_B                                  1
#define RADIO_TYPE_A                                  2
#define RADIO_TYPE_G                                  4
#define RADIO_TYPE_BG                                 5
#define RADIO_TYPE_AN                                 10
#define RADIO_TYPE_NG          12
#define RADIO_TYPE_BGN                                13
#define RADIO_TYPE_AC                                 0x80000000
#define RADIOIF_11AC_OFFSET                           5
#define RADIOIF_11AC_TYPE                             0x80
#define RADIOIF_START_RADIOID            1
#define RADIOIF_END_RADIOID              31

#define RADIOIF_MAX_QOS_PROFILE_IND      4
#define RADIO_DEF_COUNTRY_NAME                        "US"
#define RADIOIF_DEFAULT_MAX_TX_POWER     23

#define RADIOIF_MAX_CHANNEL_A            28
#define RADIOIF_REG_DOMAIN_COUNT         5
#define RADIOIF_MAX_CHANNEL_B       14
#define RADIOIF_MAX_CHANNEL_G       13
#define RADIO_MIN_VALUE_CHANNELA 34
#define RADIO_MAX_VALUE_CHANNELA 196
#define RADIO_MIN_VALUE_CHANNELB 1
#define RADIO_MAX_VALUE_CHANNELB 14
#define RADIO_DOT11N_AMPDU     0x1B
#define RADIOIF_MULTI_DOMAIN_BASE_INDEX 6
#define RADIOIF_SUPPORTED_COUNTRY_LIST  169
#define RADIOIF_OFFSET_B 0
#define RADIOIF_COUNTRY_CODE_JAPAN      "JP"
/* Macro for 802.11n */
#define RADIO_SHIFT_BIT1  1
#define RADIO_SHIFT_BIT2  2
#define RADIO_SHIFT_BIT3  3
#define RADIO_SHIFT_BIT4  4
#define RADIO_SHIFT_BIT5  5
#define RADIO_SHIFT_BIT6  6
#define RADIO_SHIFT_BIT7  7
#define RADIO_SHIFT_BIT8  8
#define RADIO_SHIFT_BIT9  9
#define RADIO_SHIFT_BIT10  10
#define RADIO_SHIFT_BIT11  11
#define RADIO_SHIFT_BIT12  12
#define RADIO_SHIFT_BIT13  13
#define RADIO_SHIFT_BIT14  14
#define RADIO_SHIFT_BIT15  15
#define RADIO_SHIFT_BIT16  16
#define RADIO_SHIFT_BIT17 17
#define RADIO_SHIFT_BIT19       19
#define RADIO_SHIFT_BIT21 21
#define RADIO_SHIFT_BIT23 23
#define RADIO_SHIFT_BIT25 25
#define RADIO_SHIFT_BIT27 27

#define RADIO_MASK_BIT1  0x1
#define RADIO_MASK_BIT2  0x2
#define RADIO_MASK_BIT3  0x3
#define RADIO_MASK_BIT7  0x7
#define RADIO_FIND_ARRAY_INDEX 8
#define RADIO_DEFAULT_VALUE  0
#define RADIO_RXSTBC_MIN   0
#define RADIO_RXSTBC_MAX   3
#define RADIO_RXSTBC_NEG_MAX  1
#define RADIO_LDPC_ENABLE 0
#define RADIO_LDPC_DISABLE 1
#define RADIO_SMPOWER_SAVE_MIN  0
#define RADIO_SMPOWER_SAVE_MAX  3
#define RADIO_GREEN_FIELD_MIN  0
#define RADIO_GREEN_FIELD_MAX  1
#define RADIO_SHORT_GI20_MIN  0
#define RADIO_SHORT_GI20_MAX  1
#define RADIO_SHORT_GI40_MIN  0
#define RADIO_SHORT_GI40_MAX  1
#define RADIO_TxSTBC_MIN  0
#define RADIO_TxSTBC_MAX  1
#define RADIO_DELAYED_BLOCK_ACK_MIN  0
#define RADIO_DELAYED_BLOCK_ACK_MAX  1
#define RADIO_AMSDU_LENGTH_MIN  0
#define RADIO_AMSDU_LENGTH_MAX  1
#define RADIO_HTDSSCCK_MIN  0
#define RADIO_HTDSSCCK_MAX  1
#define RADIO_FORTYMHz_INTOLERANT_MIN  0
#define RADIO_FORTYMHz_INTOLERANT_MAX  1
#define RADIO_LSIG_MIN  0
#define RADIO__LSIG_MAX  1
#define RADIO_RxAMPDU_FACTOR_MIN  0
#define RADIO_RxAMPDU_FACTOR_MAX  3
#define RADIO_AMPDU_SPACING_MIN  0
#define RADIO_AMPDU_SPACING_MAX  7
#define RADIO_PCO_MIN  0
#define RADIO_PCO_MAX  1
#define RADIO_CONFIG_TRANSITION_MIN  0
#define RADIO_CONFIG_TRANSITION_MAX  3
#define RADIO_MCS_FEEDBACK_MIN  0
#define RADIO_MCS_FEEDBACK_MAX  3
#define RADIO_HT_CONTROL_FIELD_MIN  0
#define RADIO_HT_CONTROL_FIELD_MAX  3
#define RADIO_RD_RESPONDER_MIN  0
#define RADIO_RD_RESPONDER_MAX  3
#define RADIO_IMPLICIT_TRANSMIT_BEAMFORMING_MIN  0
#define RADIO_IMPLICIT_TRANSMIT_BEAMFORMING_MAX  1
#define RADIO_RxSTAGGER_SOUNDING_OPTION_MIN  0
#define RADIO_RxSTAGGER_SOUNDING_OPTION_MAX  1
#define RADIO_TxSTAGGER_SOUNDING_OPTION_MIN  0
#define RADIO_RxNDP_OPTION_MAX  1
#define RADIO_RxNDP_OPTION_MIN  0
#define RADIO_TxSTAGGER_SOUNDING_OPTION_MAX  1
#define RADIO_TxNDP_OPTION_MAX  1
#define RADIO_TxNDP_OPTION_MIN  0
#define RADIO_TxBEAMFORMING_OPTION_MAX  1
#define RADIO_TxBEAMFORMING_OPTION_MIN  0
#define RADIO_CALIBRATION_OPTION_MAX  3
#define RADIO_CALIBRATION_OPTION_MIN  0
#define RADIO_CSI_TRANSMIT_BEAMFORMING_OPTION_MAX  1
#define RADIO_CSI_TRANSMIT_BEAMFORMING_OPTION_MIN  0
#define RADIO_NONCOMPRESSED_BEAMFORMING_OPTION_MAX  1
#define RADIO_NONCOMPRESSED_BEAMFORMING_OPTION_MIN  0
#define RADIO_COMPRESSED_BEAMFORMING_OPTION_MAX  1
#define RADIO_COMPRESSED_BEAMFORMING_OPTION_MIN  0
#define RADIO_TRANSMIT_BEAMFORMING_CSI_FEEDBACKOPTION_MAX  3
#define RADIO_TRANSMIT_BEAMFORMING_CSI_FEEDBACKOPTION_MIN  0
#define RADIO_NONCOMP_BEAMFORMING_CSI_FEEDBACKOPTION_MAX  3
#define RADIO_NONCOMP_BEAMFORMING_CSI_FEEDBACKOPTION_MIN  0
#define RADIO_EXPLICIT_COMPRESSED_BEAMFORMING_FEEDBACKOPTION_MAX  3
#define RADIO_EXPLICIT_COMPRESSED_BEAMFORMING_FEEDBACKOPTION_MIN  0
#define RADIO_MINIMAL_GROUPING_MAX  3
#define RADIO_MINIMAL_GROUPING_MIN  0
#define RADIO_NUMBER_BEAMFORMING_ANTENNA_MAX  3
#define RADIO_NUMBER_BEAMFORMING_ANTENNA_MIN  0
#define RADIO_NUMBER_NONCOMPRESSED_BEAMFORMING_MATRIX_MAX  3
#define RADIO_NUMBER_NONCOMPRESSED_BEAMFORMING_MATRIX_MIN  0
#define RADIO_NUMBER_COMPRESSED_BEAMFORMING_MATRIX_MAX  3
#define RADIO_NUMBER_COMPRESSED_BEAMFORMING_MATRIX_MIN  0
#define RADIO_NUMBER_BEAMFORMING_CSI_SUPPORT_ANTENNA_MAX  3
#define RADIO_NUMBER_BEAMFORMING_CSI_SUPPORT_ANTENNA_MIN  0
#define RADIO_CHANNEL_ESTIMATION_CAPABILITY_MAX  3
#define RADIO_CHANNEL_ESTIMATION_CAPABILITY_MIN  0
#define RADIO_PRIMARY_CHANNEL_MAX  200
#define RADIO_PRIMARY_CHANNEL_MIN  1
#define RADIO_SECONDARY_CHANNEL_MAX  200
#define RADIO_SECONDARY_CHANNEL_MIN  1
#define ZERO 0
#define RADIO_STA_CHANNEL_WIDTH_MAX  1
#define RADIO_STA_CHANNEL_WIDTH_MIN  0
#define RADIO_RIFS_MAX  1
#define RADIO_RIFS_MIN  0
#define RADIO_HTPROTECTION_MAX  3
#define RADIO_HTPROTECTION_MIN  0
#define RADIO_HTSTA_MAX  1
#define RADIO_HTSTA_MIN  0
#define RADIO_OBSS_NONHTSTA_MAX  1
#define RADIO_OBSS_NONHTSTA_MIN  0
#define RADIO_DUAL_BEACON_MAX  1
#define RADIO_DUAL_BEACON_MIN  0
#define RADIO_DUAL_CTS_PROTECTION_MAX  1
#define RADIO_DUAL_CTS_PROTECTION_MIN  0
#define RADIO_STBC_BEACON_MAX  1
#define RADIO_STBC_BEACON_MIN  0
#define RADIO_PCO_ACTIVATED_MAX  1
#define RADIO_PCO_ACTIVATED_MIN  0
#define RADIO_PCO_PHASE_MAX  1
#define RADIO_PCO_PHASE_MIN  0
#define RADIO_Tx_MCS_SET_MAX  1
#define RADIO_Tx_MCS_SET_MIN  0
#define RADIO_TxRx_MCS_SET_NOTEQUAL_MAX  1
#define RADIO_TxRx_MCS_SET_NOTEQUAL_MIN  0
#define RADIO_Rx_STBC_MAX  3
#define RADIO_Rx_STBC_MIN  0

#define DFS_CHANNEL_MIN   52
#define DFS_CHANNEL_MAX   144


typedef struct {
    UINT2       u2MessageType;
    UINT2       u2MessageLength;
    UINT1       u1RadioId;
    UINT1       u1ReceiveDiversity;
    UINT1       u1AntennaMode;
    UINT1       u1CurrentTxAntenna;
    UINT1       au1AntennaSelection [RADIOIF_ANTENNA_LIST_INDEX];
    BOOL1     isPresent;
}tRadioIfAntenna;

typedef struct {
    UINT2       u2MessageType;
    UINT2       u2MessageLength;
    UINT2       u2WtpInternalId;
    UINT1       u1RadioId; 
    UINT1       u1AdminStatus;
    BOOL1 isPresent;
    UINT1       au1Pad[3];
}tRadioIfAdminStatus;

typedef struct {
    UINT2       u2MessageType;
    UINT2       u2MessageLength;
    UINT1       u1RadioId; 
    UINT1       u1OperStatus;
    UINT1       u1FailureCause;
    BOOL1     isPresent;
}tRadioIfOperStatus;

typedef struct {
    UINT2       u2MessageType;
    UINT2       u2MessageLength;
    UINT1       u1RadioId;
    UINT1       u1Type;
    UINT1       u1Status;
    BOOL1    isPresent;
} tRadioIfFailAlarm;

typedef struct {
    INT4        i4EDThreshold;  
    UINT2       u2MessageType;
    UINT2       u2MessageLength;
    UINT1       u1RadioId; 
    UINT1       u1Reserved;
    INT1        i1CurrentChannel;
    INT1        i1CurrentCCAMode;
    BOOL1     isPresent;
    UINT1       au1Pad[3];
} tRadioIfDSSSPhy;

/* HT Capabilities */
typedef struct {
    UINT4       u4TranBeamformCap;
    UINT2       u2HTCapInfo;
    UINT2       u2HTExtCap;
    UINT1       u1ElemId;
    UINT1       u1ElemLen;
    UINT1       u1AMPDUParam;
    UINT1       u1ASELCap;
    BOOL1       isOptional;
    UINT1       au1Pad[3];
    UINT1       au1SuppMCSSet[RADIOIF_11N_MCS_RATE_LEN_PKT];
}tHTCapabilityElem;

/* HT Operation */
typedef struct {
    UINT1       u1ElemId;
    UINT1       u1ElemLen;
    UINT1       u1PrimaryCh;
    BOOL1       isOptional;
    BOOL1       isPresent;
    UINT1       au1HTOpeInfo[RADIO_HTOPE_INFO];
    UINT1       au1BasicMCSSet[RADIO_BASIC_MCS_SET];
}tHTOperationElem;

/*VHT Capabilities*/
typedef struct {
    UINT4 u4VhtCapInfo;
    UINT1 au1SuppMCS[RADIO_11AC_VHT_CAPA_MCS_LEN];
    UINT1 u1ElemId;
    UINT1 u1ElemLen;
    BOOL1 isOptional;
    UINT1 u1Pad;
}tVHTCapabilityElem;

typedef struct {
    UINT2 u2BasicMCS;
    UINT1 u1ElemLen;
    BOOL1 isOptional;
    UINT1 au1VhtOperInfo[RADIO_11AC_VHT_OPER_INFO_LEN];
    UINT1 u1ElemId;
}tVHTOperationElem;

typedef struct{
    union {
        tHTCapabilityElem HTCapability;
        tHTOperationElem  HTOperation;
        tVHTCapabilityElem VHTCapability;
        tVHTOperationElem VHTOperation;
#if defined(WPS_WANTED) || defined(WPS_WTP_WANTED)
        tWpsIEElements   WpsIEElements;
#endif        
    }unInfoElem;
    UINT2    u2MessageType; /* 1029 */
    UINT2    u2MessageLength; /* >=4 */
    UINT1    u1EleId;
    BOOL1    isPresent;
    UINT1    u1RadioId;
    UINT1    u1WlanId;
    UINT1    u1BPFlag;
    UINT1    au1Pad[3];
}tRadioIfInfoElement;

typedef struct {
    UINT2       u2MessageType;
    UINT2       u2MessageLength;
    UINT1       u1RadioId;
    UINT1       u1WlanId;
    UINT1       u1BeaconAndProbeInclude;
    UINT1       au1InfoElement [RADIOIF_INFO_ELEMENT];
    BOOL1     isPresent;
}tDot11InfoElement;

typedef struct {
    UINT4       u4TxMsduLifetime;
    UINT4       u4RxMsduLifetime;
    UINT2       u2MessageType;
    UINT2       u2MessageLength;
    UINT2       u2RTSThreshold;
    UINT2       u2FragmentThreshold;
    UINT1       u1RadioId;
    UINT1       u1Reserved;
    UINT1       u1ShortRetry;
    UINT1       u1LongRetry;
    BOOL1     isPresent;
    UINT1       au1Pad[3];
} tRadioIfMacOperation;

typedef struct {
    UINT2       u2MessageType;
    UINT2       u2MessageLength;
    UINT2       u2FirstChannel;
    UINT2       u2NumOfChannels;
    UINT2       u2MaxTxPowerLevel;
    UINT1       u1RadioId;
    UINT1       u1Reserved;
    UINT1       u1MultiDomainIndex;
    BOOL1       isPresent;
    UINT2       u2MsgLength;
    UINT4       u4VendorId;
}tRadioIfMultiDomainCap;

typedef struct {
    UINT4       u4T1Threshold;
    UINT2       u2MessageType;
    UINT2       u2MessageLength;
    UINT1       u1RadioId;
    UINT1       u1Reserved;
    UINT1       u1CurrentChannel;
    UINT1       u1SupportedBand;
    BOOL1     isPresent;
    UINT1       au1Pad[3];
}tRadioIfOFDMPhy;

typedef struct {
    UINT2       u2MessageType;
    UINT2       u2MessageLength;
    UINT1       au1OperationalRate[RADIOIF_OPER_RATE];
    UINT1       u1RadioId;
    BOOL1     isPresent;
    UINT1       au1Pad[2];
}tRadioIfRateSet;

typedef struct {
    UINT2       u2MessageType;
    UINT2       u2MessageLength;
    BOOL1       bRadioIfConfigSet;
    UINT1       au1SupportedRate[RADIOIF_OPER_RATE];
    UINT1       u1RadioId;
    BOOL1     isPresent;
    UINT1       u1Pad;
}tRadioIfSupportedRate;


typedef struct {
    UINT2       u2MessageType;
    UINT2       u2MessageLength;
    UINT2       u2TxPowerLevel;
    UINT1       u1RadioId;
    UINT1       u1Reserved;
    BOOL1     isPresent;
    UINT1       au1Pad[3];
}tRadioIfTxPower;

typedef struct {
    UINT2       u2MessageType;
    UINT2       u2MessageLength;
    UINT2       au2PowerLevels[RADIOIF_MAX_POWER_LEVEL];
    UINT1       u1RadioId;
    UINT1       u1NumOfLevels;
    BOOL1     isPresent;
    UINT1       u1Pad;
}tRadioIfTxPowerLevel;

typedef struct {
    UINT2       u2MessageType;
    UINT2       u2MessageLength;
    UINT2       u2CwMin[4];
    UINT2       u2CwMax[4];
    UINT1       u1RadioId;
    UINT1       u1TaggingPolicy;
    UINT1       u1QueueDepth[4];
    UINT1       u1Aifsn[4];
    UINT1       u1AdmissionControl[4];
    UINT1       u1Prio[4];  
    UINT1       u1Dscp[4];
    UINT1       u1Index;
    BOOL1 isPresent;
}tRadioIfQos;

typedef struct {
    UINT4       u4VendorId;
    UINT2       u2MessageType;
    UINT2       u2MessageLength;
    UINT2       u2Reserved;
    UINT1       u1RadioId;
    BOOL1       isPresent;
    UINT1       u1HtFlag;
    UINT1       u1MaxSuppMCS;
    UINT1       u1MaxManMCS;
    UINT1       u1TxAntenna;
    UINT1       u1RxAntenna;
    UINT1       au1Pad[3]; 
}tRadioIfDot11nParam;

typedef struct {
    UINT2       u2MessageType;
    UINT2       u2MessageLength;
    UINT2       u2BeaconPeriod;
    UINT2       u2WtpInternalId;
    UINT1       u1RadioId;
    UINT1       u1ShortPreamble;
    UINT1       u1NoOfBssId;
    UINT1       u1DtimPeriod;
    tMacAddr    MacAddr;
    UINT1       au1CountryString[RADIOIF_COUNTRY_STR_LEN];    
    BOOL1       isPresent;
    UINT1       u1Pad;
}tRadioIfConfig;

typedef struct {
    UINT4       u4RadioType;
    UINT2       u2MessageType;
    UINT2       u2MessageLength;
    UINT1       u1RadioId;
    BOOL1     isPresent;
    UINT1       au1Pad[2];
}tRadioIfInfo;

typedef struct 
{
    UINT4       u4IfIndex;
    UINT1       u1IfType;
    UINT1       u1AdminStatus;
    UINT1       u1OperStatus;
    UINT1  u1IsFromMib;
    BOOL1     isPresent;
    UINT1       au1Pad[3];
}tRadioIfSetAdminOperStatus;

typedef struct {
    UINT4           u4IfIndex;
    UINT1           u1IfType;
    UINT1           u1AdminStatus;
    UINT1           u1RadioId;
    UINT1           u1Pad;
}tRadioIfCreate;

typedef struct {
    UINT4           u4IfIndex;
    UINT4           u4Dot11RadioType;
    UINT2           u2WtpInternalId;
    tMacAddr        MacAddr;
    UINT1           u1IfType;
    UINT1           u1AdminStatus;
    UINT1           u1RadioId;
    BOOL1         isPresent;
}tRadioIfVirtualIntfCreate;

typedef struct {
    UINT4    u4VendorId;
    UINT2    u2MessageType;
    UINT2    u2MessageLength;
    UINT2    u2AMPDULimit;
    UINT2    u2AMSDULimit;
    UINT1    u1RadioId;
    UINT1    u1AMPDUStatus;
    UINT1    u1AMPDUSubFrame;
    UINT1    u1AMSDUStatus;
    BOOL1    isPresent;
    UINT1    au1Pad[3];
}tRadioIfDot11nCfg;

typedef struct {
    tRadioIfAdminStatus     RadioIfAdminStatus;
    tRadioIfAntenna         RadioIfAntenna;
    tRadioIfDSSSPhy         RadioIfDSSPhy;
    tRadioIfInfoElement     RadioIfInfoElem;     
    tRadioIfMacOperation    RadioIfMacOperation;
    tRadioIfMultiDomainCap  RadioIfMultiDomainCap;
    tRadioIfOFDMPhy         RadioIfOFDMPhy;
    tRadioIfSupportedRate   RadioIfSupportedRate;
    tRadioIfTxPower         RadioIfTxPower;
    tRadioIfTxPowerLevel    RadioIfTxPowerLevel;
    tRadioIfConfig          RadioIfConfig;
    tRadioIfDot11nParam     RadioIfDot11nParam;
    tRadioIfInfo            RadioIfInfo;
    UINT2                   u2WtpInternalId;
    UINT1                   au1Pad[2];
}tRadioIfConfigStatusReq;

typedef struct {
    tRadioIfAntenna         RadioIfAntenna;
    tRadioIfDSSSPhy         RadioIfDSSPhy;
    tRadioIfInfoElement     RadioIfInfoElem;     
    tRadioIfMacOperation    RadioIfMacOperation;
    tRadioIfMultiDomainCap  RadioIfMultiDomainCap;
    tRadioIfOFDMPhy         RadioIfOFDMPhy;
    tRadioIfRateSet         RadioIfRateSet;
    tRadioIfSupportedRate   RadioIfSupportedRate;
    tRadioIfTxPower         RadioIfTxPower;
    tRadioIfQos             RadioIfQos;
    tRadioIfConfig          RadioIfConfig;
    tRadioIfDot11nParam     RadioIfDot11nParam;
    tRadioIfInfo            RadioIfInfo;
    tDot11InfoElement       Dot11InfoElement;
    tRadioIfDot11nCfg       RadioIfDot11nCfg;
    UINT2                   u2WtpInternalId;
    UINT1                   au1Pad[2];
}tRadioIfConfigStatusRsp;

typedef struct {
    UINT2       u2MessageType;
    UINT2       u2MessageLength;
    UINT2       u2WtpInternalId;
    UINT2       u2ReportInterval;
    UINT1       u1RadioId; 
    BOOL1       isPresent;
    UINT1       au1Pad[2];
}tRadioDecryptReport;

typedef struct {
    tRadioIfAdminStatus     RadioIfAdminStatus;
    tRadioIfAntenna         RadioIfAntenna;
    tRadioIfDSSSPhy         RadioIfDSSSPhy;
    tRadioIfInfoElement     RadioIfInfoElem;     
    tRadioIfMacOperation    RadioIfMacOperation;
    tRadioIfMultiDomainCap  RadioIfMultiDomainCap;
    tRadioIfOFDMPhy         RadioIfOFDMPhy;
    tRadioIfRateSet     RadioIfRateSet; 
    tRadioIfTxPower         RadioIfTxPower;
    tRadioIfConfig          RadioIfConfig;
    tRadioIfDot11nParam     RadioIfDot11nParam;
    tRadioIfInfo            RadioIfInfo;
    tDot11InfoElement       Dot11InfoElement;
    tRadioIfQos             RadioIfQos;
    tRadioIfDot11nCfg       RadioIfDot11nCfg;
    UINT2                   u2WtpInternalId;
    UINT1                   au1Pad[2];
}tRadioIfConfigUpdateReq;

typedef struct {
    tRadioIfOperStatus      RadioIfOperStatus;
    tDot11InfoElement       Dot11InfoElement;
    UINT4      u4ResultCode;
    UINT2      u2WtpInternalId;
    UINT1      au1Pad[2]; 
}tRadioIfConfigUpdateRsp;

typedef struct {
    tRadioIfOperStatus      RadioIfOperStatus;
    tRadioIfFailAlarm       RadioIfFailAlarm;
    tDot11InfoElement       Dot11InfoElement;
    UINT2                   u2WtpInternalId;
    UINT1                   au1Pad[2];
}tRadioIfChangeStateEventReq;

/* The below data structures will not store any Radio IF related information and 
 * are used to only construct the message elements and send to the external
 *  modules */

typedef struct {
       UINT4           u4IfIndex;
       UINT1           u1WlanId; 
       UINT1           au1pad[3];
}tRadioIfWlanUpdate;


typedef struct {
    UINT4           u4IfIndex;
    UINT2           u2CurrentChannel;
    UINT2           u2TxPowerLevel;
}tRadioIfDcaTpcUpdate;

typedef struct {
    UINT4       u4VendorId;
    UINT2       u2MessageType;
    UINT2       u2MessageLength;
    UINT2       u2FirstChannel;
    UINT2       u2NumOfChannels;
    UINT2       u2MaxTxPowerLevel;
    UINT2       u2MsgLength;
    UINT1       u1RadioId;
    UINT1       u1MultiDomainIndex;
    BOOL1       isPresent;
    UINT1       au1pad[1];
}tVendorMultiDomainCap;

typedef struct {
    tVendorMultiDomainCap   VendorMultiDomainCap;
    UINT2                   u2WtpInternalId;
    UINT1                   au1Pad[2];
}tRadioIfPmWtpEventReq;

typedef struct {
   UINT4         u4IfIndex;
   UINT1         u1CurrentChannel;
   UINT1         au1pad[3];
}tRadioIfDFSChannelUpdate;

typedef struct {
   UINT4         u4IfIndex;
   UINT1         u1RadarStatus;
   UINT1         au1pad[3];
}tRadioIfDFSRadarStatusUpdate;

typedef struct {
    union {
        tRadioIfConfigStatusReq         RadioIfConfigStatusReq;
        tRadioIfConfigStatusRsp         RadioIfConfigStatusRsp;
        tRadioIfConfigUpdateReq         RadioIfConfigUpdateReq;
        tRadioIfConfigUpdateRsp         RadioIfConfigUpdateRsp;
        tRadioIfChangeStateEventReq     RadioIfChangeStateEventReq;
        tRadioIfVirtualIntfCreate       RadioIfVirtualIntfCreate;
        tRadioIfCreate                  RadioIfCreate;
        tRadioIfSetAdminOperStatus      RadioIfSetAdminOperStatus;
        tRadioIfWlanUpdate              RadioIfWlanUpdate;
        tRadioDecryptReport             DecryptReportTimer;
        tRadioIfDcaTpcUpdate            RadioIfDcaTpcUpdate;
        tRadioIfPmWtpEventReq           RadioIfPmWtpEventReq;
        tRadioIfDFSChannelUpdate        RadioIfDfsChnlUpdate;
        tRadioIfDFSRadarStatusUpdate    RadioIfRadarStatUpdate;
    }unRadioIfMsg;
}tRadioIfMsgStruct;

enum {
 WSS_RADIOIF_INIT_MSG = 1,
 WSS_RADIOIF_CREATE_PHY_RADIO_INTF_MSG,
 WSS_RADIOIF_ADMIN_STATUS_CHG_MSG,
 WSS_RADIOIF_OPER_STATUS_CHG_MSG,
 WSS_RADIOIF_CONFIG_UPDATE_REQ,
 WSS_RADIOIF_CONFIG_UPDATE_RSP,
 WSS_RADIOIF_CONFIG_STATUS_REQ,
 WSS_RADIOIF_CONFIG_STATUS_RSP,
 WSS_RADIOIF_CHANGE_STATE_EVENT_REQ,
 WSS_RADIOIF_CONFIG_STATUS_REQ_VALIDATE,
 WSS_RADIOIF_CREATE_VIRTUAL_RADIO_MSG,
 WSS_RADIOIF_DELETE_VIRTUAL_RADIO_MSG,
    WSS_RADIOIF_CONFIG_UPDATE_REQ_VALIDATE, 
    WSS_RADIOIF_CONFIG_UPDATE_RSP_VALIDATE,
 WSS_RADIOIF_CONFIG_STATUS_RSP_VALIDATE,
 WSS_RADIOIF_CHANGE_STATE_EVT_REQ_VALIDATE,
    WSS_WLAN_UPDATE_RADIO_IF_DB,
    WSS_RADIOIF_CHANNEL_UPDATE,
    WSS_RADIOIF_POWER_UPDATE,
    WSS_RADIOIF_DECRYPT_REPORT,
 WSS_RADIOIF_TEARDOWN_NOTIFICATION_MSG,
 WSS_RADIOIF_PM_WTP_EVENT_REQ,
 WSS_RADIOIF_RADIODB_RESET,
 WSS_RADIOIF_DFS_CHANNEL_UPDATE,
 WSS_RADIOIF_DFS_RADAR_STAT_UPDATE
};

enum {
    WSS_RADIOIF_INFO_ELEM_HT_CAPABILITY = 45,
    WSS_RADIOIF_INFO_ELEM_VHT_CAPABILITY = 191,
    WSS_RADIOIF_INFO_ELEM_VHT_OPERATION = 192,
    WSS_RADIOIF_INFO_ELEM_MAX_TYPE
};

enum 
{
  RADIO_EXC_COUNTRY_TW,
  RADIO_EXCEPTION_MAX
};
/* Prototypes - RADIO IF Module */
UINT1 WssIfProcessRadioIfMsg PROTO ((UINT4, tRadioIfMsgStruct *));


typedef struct {
   UINT2 u2FirstChannel;
   UINT2 u2NumOfChannels; 
   UINT2 u2MaxTxPowerLevel;
   UINT1 u1CountryString[RADIO_COUNTRY_LENGTH]; 
   UINT1 au1pad[3];
}tCountryMapping;

typedef struct {
    UINT1    au1CountryCode[3];
    UINT1    au1pad;
    UINT1    au1CountryName[32];
    UINT4    u4NumericCode;
} tWsscfgSupportedCountryList;

typedef struct {
    UINT2    u2RegDomain;
    UINT2    u2CountryCode;
} tWsscfgRegDomainCountryMapping;


typedef struct {
    UINT1 au1Dot11aChannelExcList [RADIOIF_MAX_CHANNEL_A];
    UINT1 au1Dot11bChannelExcList [RADIOIF_MAX_CHANNEL_B];
    UINT1 au1CountryStr [3];
    UINT1 au1Pad[3];
}tRadioIfExcList;

enum{
    COUNTRY_AD,
    COUNTRY_AE,
    COUNTRY_AF,
    COUNTRY_AI,
    COUNTRY_AL,
    COUNTRY_AM,
    COUNTRY_AN,
    COUNTRY_AR,
    COUNTRY_AS,
    COUNTRY_AT,
    COUNTRY_AU,
    COUNTRY_AW,
    COUNTRY_AZ,
    COUNTRY_BA,
    COUNTRY_BB,
    COUNTRY_BD,
    COUNTRY_BE,
    COUNTRY_BF,
    COUNTRY_BG,
    COUNTRY_BH,
    COUNTRY_BL,
    COUNTRY_BM,
    COUNTRY_BN,
    COUNTRY_BO,
    COUNTRY_BR,
    COUNTRY_BS,
    COUNTRY_BT,
    COUNTRY_BY,
    COUNTRY_BZ,
    COUNTRY_CA,
    COUNTRY_CF,
    COUNTRY_CH,
    COUNTRY_CI,
    COUNTRY_CL,
    COUNTRY_CN,
    COUNTRY_CO,
    COUNTRY_CR,
    COUNTRY_CX,
    COUNTRY_CY,
    COUNTRY_CZ,
    COUNTRY_DE,
    COUNTRY_DK,
    COUNTRY_DM,
    COUNTRY_DO,
    COUNTRY_DZ,
    COUNTRY_EC,
    COUNTRY_EE,
    COUNTRY_EG,
    COUNTRY_ES,
    COUNTRY_ET,
    COUNTRY_FI,
    COUNTRY_FM,
    COUNTRY_FR,
    COUNTRY_GB,
    COUNTRY_GD,
    COUNTRY_GE,
    COUNTRY_GF,
    COUNTRY_GH,
    COUNTRY_GL,
    COUNTRY_GP,
    COUNTRY_GR,
    COUNTRY_GT,
    COUNTRY_GU,
    COUNTRY_GY,
    COUNTRY_HK,
    COUNTRY_HN,
    COUNTRY_HR,
    COUNTRY_HT,
    COUNTRY_HU,
    COUNTRY_ID,
    COUNTRY_IE,
    COUNTRY_IL,
    COUNTRY_IN,
    COUNTRY_IR,
    COUNTRY_IS,
    COUNTRY_IT,
    COUNTRY_JM,
    COUNTRY_JO,
    COUNTRY_JP,
    COUNTRY_KE,
    COUNTRY_KH,
    COUNTRY_KN,
    COUNTRY_KP,
    COUNTRY_KR,
    COUNTRY_KW,
    COUNTRY_KY,
    COUNTRY_KZ,
    COUNTRY_LB,
    COUNTRY_LC,
    COUNTRY_LI,
    COUNTRY_LK,
    COUNTRY_LS,
    COUNTRY_LT,
    COUNTRY_LU,
    COUNTRY_LV,
    COUNTRY_MA,
    COUNTRY_MC,
    COUNTRY_MD,
    COUNTRY_ME,
    COUNTRY_MF,
    COUNTRY_MH,
    COUNTRY_MK,
    COUNTRY_MN,
    COUNTRY_MO,
    COUNTRY_MP,
    COUNTRY_MQ,
    COUNTRY_MR,
    COUNTRY_MT,
    COUNTRY_MU,
    COUNTRY_MW,
    COUNTRY_MX,
    COUNTRY_MY,
    COUNTRY_NI,
    COUNTRY_NL,
    COUNTRY_NO,
    COUNTRY_NP,
    COUNTRY_NZ,
    COUNTRY_OM,
    COUNTRY_PA,
    COUNTRY_PE,
    COUNTRY_PF,
    COUNTRY_PG,
    COUNTRY_PH,
    COUNTRY_PK,
    COUNTRY_PL,
    COUNTRY_PM,
    COUNTRY_PR,
    COUNTRY_PT,
    COUNTRY_PW,
    COUNTRY_PY,
    COUNTRY_QA,
    COUNTRY_RE,
    COUNTRY_RO,
    COUNTRY_RS,
    COUNTRY_RU,
    COUNTRY_RW,
    COUNTRY_SA,
    COUNTRY_SE,
    COUNTRY_SG,
    COUNTRY_SI,
    COUNTRY_SK,
    COUNTRY_SN,
    COUNTRY_SR,
    COUNTRY_SV,
    COUNTRY_SY,
    COUNTRY_TC,
    COUNTRY_TD,
    COUNTRY_TG,
    COUNTRY_TH,
    COUNTRY_TN,
    COUNTRY_TR,
    COUNTRY_TT,
    COUNTRY_TW,
    COUNTRY_UA,
    COUNTRY_UG,
    COUNTRY_US,
    COUNTRY_UY,
    COUNTRY_UZ,
    COUNTRY_VC,
    COUNTRY_VE,
    COUNTRY_VI,
    COUNTRY_VN,
    COUNTRY_VU,
    COUNTRY_WF,
    COUNTRY_YE,
    COUNTRY_YT,
    COUNTRY_ZA,
    COUNTRY_ZW,
    COUNTRY_MAX
};
enum
{
    REG_DOMAIN_AM,
    REG_DOMAIN_ETSI,
    REG_DOMAIN_ISRAEL,
    REG_DOMAIN_JAPAN,
    REG_DOMAIN_WW
};

enum {
   RADIOIF_EXCEPTION_TW,
   RADIOIF_EXCEPION_COUNT
};

PUBLIC tWsscfgSupportedCountryList gWsscfgSupportedCountryList [RADIOIF_SUPPORTED_COUNTRY_LIST];
PUBLIC tWsscfgRegDomainCountryMapping gRegDomainCountryMapping [RADIOIF_SUPPORTED_COUNTRY_LIST];
PUBLIC UINT1 gau1Dot11aRegDomainChannelList [RADIOIF_MAX_CHANNEL_A];
PUBLIC UINT1 gau1Dot11aChannelRegDomainCheckList [RADIOIF_REG_DOMAIN_COUNT] [RADIOIF_MAX_CHANNEL_A];
PUBLIC UINT1 gau1Dot11aChannelCheckList [RADIOIF_MAX_CHANNEL_A];
PUBLIC UINT1 gau1Dot11bRegDomainChannelList [RADIOIF_MAX_CHANNEL_B];
PUBLIC UINT1 gau1Dot11bChannelRegDomainCheckList [RADIOIF_REG_DOMAIN_COUNT] [RADIOIF_MAX_CHANNEL_B];
PUBLIC tRadioIfExcList gRadioIfExcList [RADIO_EXCEPTION_MAX];  

#define RADIOIF_EDCA_BE  0
#define RADIOIF_EDCA_VI  1
#define RADIOIF_EDCA_VO  2
#define RADIOIF_EDCA_BG  3

#define MAX_TRAFIC 4
#define MAC_ADDR_LEN1 18

#endif  /* _RADIOIF_H_ */
