/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsssl.h,v 1.4 2015/01/02 12:05:34 siva Exp $
 *
 * Description: This file has the typedefs and prototypes for SSL
 *
 ***********************************************************************/

#ifndef __FSSSL_H__
#define __FSSSL_H__


/* Definitions used by both CLI & Low levels */
/* Ciphers suites */
#define SSL_TRUE                        1
#define SSL_FALSE                       0
#define SSL_SUCCESS                     0
#define SSL_FAILURE                    -1
#define SSL_VERSION_ALL                          1
#define SSL_VERSION_SSLV3                        2
#define SSL_VERSION_TLSV1                        3
#define TLS_RSA_NULL_MD5                      0x0001
#define TLS_RSA_NULL_SHA1                     0x0002
#define TLS_RSA_DES_SHA1                      0x0004
#define TLS_RSA_3DES_SHA1                     0x0008
#define TLS_DHE_RSA_DES_SHA1                  0x0010
#define TLS_DHE_RSA_3DES_SHA1                 0x0020
#define TLS_RSA_EXP1024_DES_SHA1              0x0040
#define TLS_RSA_WITH_AES_128_CBC_SHA          0x0080
#define TLS_RSA_WITH_AES_256_CBC_SHA          0x0100
#define TLS_DHE_RSA_WITH_AES_128_CBC_SHA      0x0200
#define TLS_DHE_RSA_WITH_AES_256_CBC_SHA      0x0400


/* Trace levels */
#define SSL_INIT_SHUT_TRC_MASK          0x00000001
#define SSL_MGMT_TRC_MASK               0x00000002
#define SSL_DATA_PATH_TRC_MASK          0x00000004
#define SSL_CNTRL_PLANE_TRC_MASK        0x00000008
#define SSL_DUMP_TRC_MASK               0x00000010
#define SSL_OS_RESOURCE_TRC_MASK        0x00000020
#define SSL_ALL_FAILURE_TRC_MASK        0x00000040
#define SSL_BUFFER_TRC_MASK             0x00000080
#define SSL_ENABLE_ALL_TRC_MASK         0x0000ffff
#define SSL_DISABLE_ALL_TRC_MASK        0x00000000
#define SSL_CALLBACK_FN               gaSslCallBack

#define SSL_CERT_SAVE_FILE         FLASH "sslservcert"



/* SSL APIs */
INT1 SslArInit PROTO((VOID));
INT1 SslArStart PROTO((VOID));
VOID * SslArAccept PROTO((INT4 i4SockFd));
VOID * SslArConnect PROTO((INT4 i4SockFd));
VOID SslArLibInit PROTO((VOID));
INT1
SslArRead PROTO((VOID *pSslConnId, UINT1 *pu1Buf, UINT4 *pu4NumBytes));
INT1
SslArWrite PROTO((VOID *pConnId, UINT1 *pu1Buf, UINT4 *pu4NumBytes));
INT1
SslArWriteImmediate PROTO((VOID *pConnId, UINT1 *pu1Buf, UINT4 *pu4NumBytes));
VOID
SslArClose PROTO((VOID *pSslConnId));
VOID
SslArProcessCtrlMsgs PROTO((VOID));
VOID
SslArClearSessions PROTO((VOID));
INT4 SslArGetCtrlSockFd PROTO((VOID));
UINT2 SslArGetHttpsPort PROTO((VOID));
UINT2 SslArGetHttpsVersion PROTO((VOID));
UINT2 SslArGetCfgCipherList PROTO((VOID));
BOOLEAN SslArIsServerCertConfigured PROTO((VOID));
INT1 SslArSetCtrlMsgSslPort PROTO((INT4 ));
INT1 SslArSetCtrlMsgSslVersion PROTO((INT4 ));
INT1 SslArSetCtrlMsgCipherList PROTO((INT4 ));
INT1 SslArSetCtrlMsgHttpsStatus PROTO((INT4 ));
VOID SslArGenRsaKeyPair PROTO((INT4 ));
VOID SslArResetRsaKeyPair PROTO((VOID));
INT1 SslArGenCertReq PROTO((UINT1 *,UINT1 **));
INT1 SslArGetServerCert PROTO((UINT1 **, UINT4 *, INT1 *));
INT1 SslArSetServerCert PROTO((INT1 *));
UINT2 SslArGetRsaBits PROTO((VOID));
INT1 SslArCheckRsaKey PROTO((VOID));
INT1 SslArSaveServerCert PROTO((VOID));
VOID SslArCtrlGenRsaKey PROTO((INT4));
VOID SslArSetSslTrace PROTO ((INT4 i4SslTrace));
UINT4 SslArGetSslTrace(VOID);
INT1 SslArFlush (VOID *pSslConnId);
VOID SSLDeleteKeys (VOID);

INT4 SslArSetFipsMode PROTO ((VOID));
INT4 SslArSelfTestRsa PROTO ((VOID));
INT4 SslArSelfTestDsa PROTO ((VOID));

VOID SSLSetPowerOnStatus (INT4 i4PowerOnStatus);

/* Register Call Back Function for Customization in SSL */
INT4 SslRegisterCallBackforSSLModule (UINT4 u4Event, tFsCbInfo * pFsCbInfo);

INT1 SslArRsaSignReq PROTO ((UINT4 u4Len, UINT1 *pu1Sha1Digest, 
                             UINT1 *pu1RsaSign, UINT4 *u4SignLen));
INT1 SslArRsaSignVer PROTO ((VOID *pRsa, UINT4 u4Len, UINT1 *pu1RsaInSign, 
                             UINT1 *pu1MsgDigestm, UINT4 *u4DigestLen));
INT1 SslArGetDerRsaPubKey PROTO ((UINT1 **ppu1DerPubKey, UINT2 *pu2DerLen));
INT1 SslArGetRsaPubKey PROTO ((UINT1 **ppu1DerPubKey, UINT4 pu2DerLen,
                                  VOID **ppRsa));
INT1 SslArGetMaxRsaSignSize PROTO ((UINT1 **ppu1DerPubKey, UINT4 u4DerLen,
                                    INT4 *pu4MaxSignLen));
INT1
SSLArTACertNameVerify PROTO ((UINT1 **pu1DerName, INT4 i4TAOptLen));
INT1
SslArGetDerServerCert PROTO ((UINT1 **ppu1Cert, INT4 *pi4CertLen));
INT1
SSLArGetDerTAName PROTO ((UINT1 **ppu1DerTAName, INT4 *pi4DerLen));
INT1
SSLArVerifyCert PROTO ((UINT1 **ppu1DerCert, INT4 i4CertLen));
VOID SSLArRestoreCert PROTO ((VOID));
VOID SslArMemFree PROTO ((UINT1 *pMem));
INT4
SslRmEnqCertGenMsgToRm PROTO ((VOID));
void SSLArRsaFree (void *pRsa);

typedef enum
{
            SSL_CUST_MODE_CHK = 1,
            SSL_CUST_CERTIFICATE_PROCESS,
            SSL_MAX_CALLBACK_EVENTS
}eSslCallBackEvents;


typedef union SslCallBackEntry
{
    INT4 (* pSslCustCheckCustMode) (VOID );
    INT4 (* pSslCustCertificateProcess ) (UINT4, UINT4, ...);

}unSslCallBackEntry;

extern unSslCallBackEntry          gaSslCallBack[SSL_MAX_CALLBACK_EVENTS];
#endif /* __FSSSL_H__ */

