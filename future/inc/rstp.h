/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rstp.h,v 1.80 2016/05/31 10:01:24 siva Exp $
 *
 * Description:This file contains the exported definitions and  
 *             macros of RSTP                                 
 *
 *******************************************************************/
#ifndef _RSTP_H
#define _RSTP_H
#include "rmgr.h"
#include "l2iwf.h"

#define RST_SUCCESS                    0
#define RST_FAILURE                    1

#define AST_SNMP_TRUE                  1
#define AST_SNMP_FALSE                 2

#define RST_FALSE                      AST_FALSE
#define RST_TRUE                       AST_TRUE

#define RST_SNMP_START                 1
#define RST_SNMP_SHUTDOWN              2

#define RST_DATA                       L2_DATA_PKT

#define AST_MAX_PORTS_PER_SWITCH       4094

#define AST_L2_MEMBER_PORT             L2_MEMBER_PORT

#define MST_PORTS_PER_BYTE            8

#define AST_BPDUFILTER_DISABLE       AST_SNMP_TRUE 
#define AST_BPDUFILTER_ENABLE        AST_SNMP_FALSE 

typedef UINT2 tRstVlanId;

typedef enum {
   AST_FALSE = 0,
   AST_TRUE = 1
}tAstBoolean;

enum {
    STP_OPER_POINT_POINT_STATUS_GET =1,
    STP_ADMIN_POINT_POINT_STATUS_GET,
    STP_ADMIN_POINT_POINT_STATUS_SET,
    STP_ADMIN_POINT_POINT_STATUS_TEST
};

/* The Following enum is used for the Admin Point-to-Point 
 * status as per the standard bridge definition
 * given in MIB std1d1ap.mib */

enum 
{
IEEE802ap_FORCETRUE = 1,
IEEE802ap_FORCEFALSE,
IEEE802ap_AUTO,
};



typedef struct StpConfigInfo
{
  UINT4 u4ContextId;
  UINT4  u4IfIndex;
  UINT4 *pu4ErrorCode;
  INT4  i4PortAdminPointToPoint;
  INT4  *pi4PortAdminPointToPoint;
  INT4  *pi4PortOperPointToPoint;
  tAstBoolean bAdminP2PUpdated;
  UINT1 u1InfoType;
  UINT1 au1pad[3];
}tStpConfigInfo;

typedef struct AstSTPTunnelInfo 
{
    UINT4              u4ContextId;
    UINT4              u4PortNum; /*port number for which tunnel status
                                    to be updated*/
    BOOL1              bSTPStatus; /*whether STP is enabled or not*/
    UINT1              au1Pad[3];
}tAstSTPTunnelInfo;

#define AST_PORT_ROLE_DISABLED         0x0
#define AST_PORT_ROLE_ALTERNATE        0x1
#define AST_PORT_ROLE_BACKUP           0x2
#define AST_PORT_ROLE_ROOT             0x3
#define AST_PORT_ROLE_DESIGNATED       0x4
#define AST_PORT_ROLE_MASTER           0x5


#define MST_SYNC_BSYNCED_UPDATE       1
#define MST_SYNC_ROLE_UPDATE          2
#define MST_SYNC_BSELECT_UPDATE       3

#define RST_DEFAULT_INSTANCE           0
#define RST_MAX_TREE_INSTANCES         1

#define RST_ENABLED                    1
#define RST_DISABLED                   2

#define RST_START                      1 
#define MST_START                      2
#define PVRST_START                    4

#define AST_PORT_STATE_DISABLED        1 
#define AST_PORT_STATE_DISCARDING      2
#define AST_PORT_STATE_LEARNING        4
#define AST_PORT_STATE_FORWARDING      5


#ifdef L2RED_WANTED
#define AST_RED_MEMBLK_COUNT           10 /*Increased Q Depth for Scalability*/
#else
#define AST_RED_MEMBLK_COUNT           1
#endif

#define  AST_TASK_NAME                 ((const UINT1*)"AstT")
#define  AST_TASK_PRIORITY             36
#define  AST_QUEUE_NAME                ((UINT1*)"AstQ")

#define  AST_CFG_QUEUE                 ((UINT1*)"AstC")   

#define AST_MIN_MAXAGE_VAL            600 
#define AST_MAX_MAXAGE_VAL            4000 
#define AST_MIN_HELLOTIME_VAL         100 
#define AST_MAX_HELLOTIME_VAL         RST_MAX_HELLOTIME_VAL
#define AST_MAX_IEEE_HELLOTIME_VAL    1000
#define RST_MAX_HELLOTIME_VAL         200
#define AST_MIN_FWDDELAY_VAL          400 
#define AST_MAX_FWDDELAY_VAL          3000
#define AST_MIN_ERRRECOVERY       3000
#define AST_MAX_ERRRECOVERY           6553500

#define AST_PROVIDER_BRIDGE_MODE         VLAN_PROVIDER_BRIDGE_MODE
#define AST_CUSTOMER_BRIDGE_MODE         VLAN_CUSTOMER_BRIDGE_MODE
#define AST_PROVIDER_EDGE_BRIDGE_MODE    VLAN_PROVIDER_EDGE_BRIDGE_MODE
#define AST_PROVIDER_CORE_BRIDGE_MODE    VLAN_PROVIDER_CORE_BRIDGE_MODE
#define AST_ICOMPONENT_BRIDGE_MODE       VLAN_PBB_ICOMPONENT_BRIDGE_MODE
#define AST_BCOMPONENT_BRIDGE_MODE       VLAN_PBB_BCOMPONENT_BRIDGE_MODE
#define AST_INVALID_BRIDGE_MODE          VLAN_INVALID_BRIDGE_MODE


#define AST_PROVIDER_NETWORK_PORT        CFA_PROVIDER_NETWORK_PORT
#define AST_CNP_PORTBASED_PORT           CFA_CNP_PORTBASED_PORT
#define AST_CNP_STAGGED_PORT             CFA_CNP_STAGGED_PORT
#define AST_CUSTOMER_EDGE_PORT           CFA_CUSTOMER_EDGE_PORT
#define AST_PROP_CUSTOMER_EDGE_PORT      CFA_PROP_CUSTOMER_EDGE_PORT
#define AST_PROP_CUSTOMER_NETWORK_PORT   CFA_PROP_CUSTOMER_NETWORK_PORT
#define AST_PROP_PROVIDER_NETWORK_PORT   CFA_PROP_PROVIDER_NETWORK_PORT
#define AST_CUSTOMER_BRIDGE_PORT         CFA_CUSTOMER_BRIDGE_PORT
#define AST_PROVIDER_EDGE_PORT           CFA_PROVIDER_EDGE_PORT
#define AST_VIRTUAL_INSTANCE_PORT        CFA_VIRTUAL_INSTANCE_PORT
#define AST_PROVIDER_INSTANCE_PORT       CFA_PROVIDER_INSTANCE_PORT
#define AST_CUSTOMER_BACKBONE_PORT       CFA_CUSTOMER_BACKBONE_PORT
#define AST_CNP_CTAGGED_PORT             CFA_CNP_CTAGGED_PORT
#define AST_INVALID_PROVIDER_PORT        CFA_INVALID_BRIDGE_PORT
#define AST_C_INTERFACE_TYPE             L2IWF_C_INTERFACE_TYPE


/*TODO*/
#define AST_ELINE                        VLAN_E_LINE
#define AST_ELAN                         VLAN_E_LAN

#define AST_LOCK() AstLock()
#define AST_UNLOCK() AstUnLock()

#ifdef RSTP_WANTED
#define AST_GET_STP_PORT_STATE(u2Port, VlanId)  AstGetPortStpState(u2Port, VlanId)
#else
#define AST_GET_STP_PORT_STATE(u2Port, VlanId) AST_PORT_STATE_FORWARDING
#endif

/* As per 802.1D standard for setting the parameters forward delay and
 * max age the following is to be satisfied
 * ( 2 * (i4StpBridgeFwdDelay - 1.0)) >= (i4StpBridgeMaxAge) */ 

#define AST_BRG_FWDDELAY_MAXAGE_STD(BrgFwdDelay, BrgMaxAge) \
      ((( 2 * (BrgFwdDelay - 100)) >= (BrgMaxAge)) ?  1 : 0)

/* As per 802.1D standard for setting the parameters hello time and
 * max age, the following is to be satisfied
 * (i4StpBridgeMaxAge) >=  ( 2 * (i4StpBridgeHelloTime + 1.0)) */

#define AST_BRG_HELLOTIME_MAXAGE_STD(BrgHelloTime, BrgMaxAge) \
   (((BrgMaxAge) >= (2 * (BrgHelloTime + 100))) ? 1:0)

#define AST_ASSERT() printf("%s ,%d\n",__FILE__,__LINE__);\
do{\
}while(0);


/*The folowing Macros are used  in RSTP and WEB Modules*/

#define RST_P2P_FORCETRUE              0
#define RST_P2P_FORCEFALSE             1
#define RST_P2P_AUTO                   2

#define AST_PORT_OPER_DOWN             CFA_IF_DOWN   
#define AST_PORT_ADMIN_DOWN            CFA_IF_DOWN
#define AST_PORT_ADMIN_UP              CFA_IF_UP
#define AST_PB_CEP_PROT_PORT_NUM       0xfff 

#define AST_MIN_SISP_LOG_INDEX          CFA_MIN_SISP_IF_INDEX 
#define AST_MAX_SISP_LOG_INDEX          CFA_MAX_SISP_IF_INDEX

INT4 AstLock ARG_LIST((VOID));
INT4 AstUnLock ARG_LIST((VOID));
VOID AstTaskMain ARG_LIST((INT1 *));
INT4 AstStartModule (VOID);
INT4 AstShutdownModule (VOID);
INT4 AstModuleDefaultCxtStart ARG_LIST ((VOID));
VOID AstModuleEnable ARG_LIST ((INT1 *pu1Dummy));
INT4 AstHandleInFrame ARG_LIST((tCRU_BUF_CHAIN_HEADER *pCruBuf, UINT4 u4IfIndex));
INT4 AstWrHandleInFrame ARG_LIST((tCRU_BUF_CHAIN_HEADER *pCruBuf, UINT4 u4IfIndex, UINT4 u4ContextId, UINT1 u1IfType));
INT4 AstAllPortDisable ARG_LIST((VOID));
INT4 AstCreatePort ARG_LIST((UINT4, UINT4, UINT2 u2PortNum));
INT4 AstDeletePort ARG_LIST((UINT4 u4PortNum));
INT4 AstCreatePortFromLa ARG_LIST((UINT4, UINT4, UINT2 u2PortNum));
INT4 AstDeletePortFromLa ARG_LIST((UINT4 u4PortNum));
INT4 AstUpdatePortStatus ARG_LIST((UINT4 u4PortNum, UINT1 u1Status));
INT4 AstWrUpdatePortStatus ARG_LIST((UINT4 u4PortNum, UINT1 u1Status, UINT4 u4ContextId));
INT4 RstGetStartedStatus ARG_LIST((VOID));
INT4 RstGetEnabledStatus ARG_LIST((VOID));
UINT1 AstIsPathcostConfigured ARG_LIST((UINT4 u4PortNum, UINT2 u2InstanceId));
INT4 AstGetContextInfoFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4ContextId,
                                     UINT2 *pu2LocalPort);

#ifdef MBSM_WANTED
INT4 AstMbsmProcessUpdateMessage ARG_LIST((tMbsmProtoMsg * pProtoMsg,INT4 ));
#endif    
#ifdef L2RED_WANTED
INT4 AstHandleUpdateEvents (UINT1 , tRmMsg * , UINT2);
#endif
INT4 AstRestartModule (VOID);

INT4 AstCreateContext (UINT4 u4ContextId);
INT4 AstDeleteContext (UINT4 u4ContextId);
INT4 AstMapPort (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2PortNum);
INT4 AstUnmapPort (UINT4 u4IfIndex);
INT4 AstUpdateContextName (UINT4 u4ContextId);

INT4 AstReleaseMemory (VOID);
INT4 AstIsRstStartedInContext (UINT4 u4ContextId);
INT4 AstIsRstEnabledInContext (UINT4 u4ContextId);

INT4 AstIsContextStpCompatible (UINT4 u4ContextId);

INT4 AstPortSpeedChgIndication (UINT4 u4IfIndex);

/****************PROVIDER BRIDGE RELATED FUNCTIONS********************/
INT4 AstCreateProviderEdgePort ARG_LIST((UINT4 u4CepIfIndex, tRstVlanId SVlanId));

INT4 AstDeleteProviderEdgePort ARG_LIST((UINT4 u4IfIndex,
                                         tRstVlanId SVlanId));

INT4 AstPbHandleInFrame ARG_LIST((tCRU_BUF_CHAIN_HEADER* pCruBuf, UINT2 u2IfIndex,
                                  tRstVlanId SVlanId));

INT4 AstPepOperStatusIndication ARG_LIST((UINT4 u4IfIndex, tRstVlanId SVlanId,
                                         UINT1 u1Status));

INT4 AstRedHwPepCreateNotify (UINT4 u4IfIndex, tRstVlanId SVlanId);

INT4 AstResetCounters ( UINT4 u4ContextId);

UINT2 AstGetBrgPrioFromBrgId (tSNMP_OCTET_STRING_TYPE BridgeId);

INT4 AstGetIfIndex (INT4);

INT4 AstCurrContextId (VOID);

/****************PROVIDER BACKBONE BRIDGE RELATED FUNCTIONS********************/
/* VIP identification is also used by WEB Module
 * */
INT4 AstIsPortVip (UINT4 u4PortId);

INT4 AstL2IwfGetIsid (UINT4 u4Vip, UINT4 * pu4Isid);

INT4
AstL2IwfGetVip (UINT4 u4Context, UINT4 u4Isid, UINT2 *pu2Vip);

INT4 
AstUpdateIntfType (UINT4 u4ContextId, UINT1 u1IntfType);

/*API for VLAN module*/
INT4
AstApiStpConfigInfo(tStpConfigInfo *pStpConfigInfo);

VOID
VlanHandleSTPStatusChange (tAstSTPTunnelInfo STPTunnelInfo);

INT4
AstDisableStpOnPort (UINT4 u4IfIndex);

INT4 
AstIsStpEnabled(INT4 i4IfIndex,UINT4 u4ContextId);
#endif /* _RSTP_H */
