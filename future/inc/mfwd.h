/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mfwd.h,v 1.15 2007/02/01 14:52:30 iss Exp $
 *
 * Description: This file contains the exported defines, types and
 *              functions for the Multicast Forwarding module.
 *
 *******************************************************************/

#ifndef _MFWD_H 
#define _MFWD_H


PUBLIC INT4 MfwdCreateTask ARG_LIST ((VOID)); 

PUBLIC INT4 RegisterFSMFWDwithSNMP ARG_LIST ((VOID));

VOID MfwdTaskMain ARG_LIST ((INT1 *));
#endif
