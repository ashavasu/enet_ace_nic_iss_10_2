/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: diffsrv.h,v 1.21 2007/11/26 03:01:07 iss Exp $
 *
 * Description:This file contains the definitions and  
 *             macros of FS DiffServer                                   
 *
 *******************************************************************/
#ifndef _DIFFSRV_H
#define _DIFFSRV_H

#define DFS_LOCK()                   DfsLock ()

#define DFS_UNLOCK()                 DfsUnLock ()

/* Return values for the DS module */
#define DS_FAILURE                   FAILURE
#define DS_SUCCESS                   SUCCESS

#define DS_TRUE                      TRUE
#define DS_FALSE                     FALSE

#define DS_MAC_FILTER                1     
#define DS_IP_FILTER                 2

#define DS_MIN_SCHEDULER_WEIGHT_SIZE 1
#define DS_MAX_SCHEDULER_WEIGHT_SIZE 4
#define DS_SCHEDULER_WEIGHT_SIZE     4

#define DS_MAX_PORTS                SYS_DEF_MAX_PHYSICAL_INTERFACES
/* Port List size */
#define DS_PORT_LIST_SIZE           ((DS_MAX_PORTS + 31)/32 * 4)

#define FS_DS_COSQ_SCHE_ALGO_STRICT     	  0x01
#define FS_DS_COSQ_SCHE_ALGO_RR         	  0x02
#define FS_DS_COSQ_SCHE_ALGO_WRR        	  0x03
#define FS_DS_COSQ_SCHE_ALGO_WFQ        	  0x04
#define FS_DS_COSQ_SCHE_ALGO_STRICT_RR  	  0x05
#define FS_DS_COSQ_SCHE_ALGO_STRICT_WRR 	  0x06
#define FS_DS_COSQ_SCHE_ALGO_STRICT_WFQ 	  0x07
#define FS_DS_COSQ_SCHE_ALGO_DEFICIT   	          0x08

#define DS_COSQ_STRICT_WEIGHT        0
#define DS_COSQ_MIN_WEIGHT           1
#define DS_COSQ_MAX_WEIGHT           15
#define DS_COSQ_MIN_BW               1
#define DS_COSQ_DEF_BW               2047
#define DS_COSQ_MAX_BW               262143
#define DS_COSQ_FLAG_EXS             1
#define DS_COSQ_FLAG_MIN             2

typedef tTMO_SLL_NODE       tDsSllNode;
typedef UINT1 tDsPortList[DS_PORT_LIST_SIZE];

/* Enum for Interface Direction */
typedef enum
{
   DS_INGRESS = 1,
   DS_EGRESS 
}tDsIfDirection;

/* Diffserv Classifier Entry */
typedef struct DiffServClfrEntry
{
    tDsSllNode     DsNextNode;
    INT4           i4DsClfrMFClfrId;
    INT4           i4DsClfrInProActionId;
    INT4           i4DsClfrOutProActionId;
    VOID           *pHwHandle;
    INT4           i4DsClfrId; 
    UINT1          u1DsClfrStatus;
    /* pack it if needed */
    UINT1          u1Pack[3];

}tDiffServClfrEntry;

/* Diffserv MF Classifier Entry - MF Filter */
typedef struct DiffServMultiFieldClfrEntry
{
    tDsSllNode     DsNextNode;
    INT4           i4DsMultiFieldClfrId;
    UINT4          u4DsMultiFieldClfrFilterId;
    UINT1          u1DsMultiFieldClfrFilterType;
    UINT1          u1DsMultiFieldClfrStatus;
    tDsPortList    DsPorts;
    /* pack it if needed */
    UINT1          u1Pack[2];

}tDiffServMultiFieldClfrEntry;
/* In Profile Action Entry */
typedef struct DiffServInProfileActionEntry
{
    tDsSllNode     DsNextNode;
    UINT4          u4DsInProfileActionFlag;
    UINT4          u4DsInProfileActionNewPrio;
    UINT4          u4DsInProfileActionIpTOS;
    UINT4          u4DsInProfileActionPort;
    UINT4          u4DsInProfileActionDscp;
    INT4           i4DsInProfileActionId;               
    UINT1          u1DsInProfileActionStatus;
    /* pack it if needed */
    UINT1          u1Pack[3];

}tDiffServInProfileActionEntry;


/* Out Of Profile Action Entry */
typedef struct DiffServOutProfileActionEntry
{
    tDsSllNode     DsNextNode;
    UINT4          u4DsOutProfileActionFlag;
    UINT4          u4DsOutProfileActionDscp;
    INT4           i4DsOutProfileActionMID; /* This is the meter which would be 
					       added to the filter for the
					       application specific metering */
    INT4           i4DsOutProfileActionId;
    UINT1          u1DsOutProfileActionStatus;
    /* pack it if needed */
    UINT1          u1Pack[3];

}tDiffServOutProfileActionEntry;

/* Meter Entry */
typedef struct DiffServMeterEntry
{
    tDsSllNode     DsNextNode;
    UINT4          u4DsMetertokenSize;
    UINT4          u4DsMeterRefreshCount;
    INT4           i4DsMeterId;
    UINT1          u1DsMeterStatus;
    /* pack it if needed */
    UINT1          u1Pack[3];

}tDiffServMeterEntry;

/* Sechduler Entry */
typedef struct DiffServSchedulerEntry
{
    tDsSllNode     DsNextNode;
    UINT4          u4DsSchedulerQueueCount;
    INT4           i4DsSchedulerDatapathId;
    INT4           i4DsSchedulerId;
    UINT1          au1DsSchedulerWeight[DS_SCHEDULER_WEIGHT_SIZE];
    UINT1          u1DsSchedulerStatus;
    /* pack it if needed */
    UINT1          u1Pack[3];

}tDiffServSchedulerEntry;

typedef struct DiffServCounters {
   UINT4        u4Inpkt;
   UINT4        u4Outpkt;
}tDiffServCounters;

/* Diffserv Classifier Data values - Used for WEB Get */
typedef struct DiffServWebClfrData
{
    UINT1          au1DsClfrInProfActionType[28];
    UINT1          au1DsClfrInProfActionValue[16];
    UINT1          au1DsClfrOutProfActionType[28];
    UINT1          au1DsClfrOutProfActionValue[16];
}tDiffServWebClfrData;

/* Diffserv Classifier Data values - Used for WEB Set */
typedef struct DiffServWebSetClfrEntry
{
    INT4           i4DsClfrMFClfrId;
    INT4           i4DsClfrInProActionId;
    UINT4          u4InProfActType;
    UINT4          u4InDscp;
    UINT4          u4InPrec;
    UINT4          u4CosValue;
    INT4           i4DsClfrOutProActionId;    
    UINT4          u4OutProfActType;
    UINT4          u4OutDscp;
    UINT4          u4TrafficRate;
    INT4           i4MeterId;
    INT4           i4DsClfrId;
}tDiffServWebSetClfrEntry;

typedef struct
{
    UINT4  u4CosqScheduleAlgo;
    UINT4  u4Status;
}tCosqScheduleAlgo;

typedef struct
{
    UINT4 u4CosqWeights;
    UINT4 u4CosqMinBw;
    UINT4 u4CosqMaxBw;
    UINT4 u4CosqBwFlag;
    UINT4 u4Status;
}tCosqWeightBw;


/* Prototype required for other modules */
INT4 DsGetStartedStatus (VOID);
INT4 DsCheckDiffservPort (INT4 i4TrunkPort);
INT4 DsCheckMFClfrTable (INT4 i4FilterNo, INT4 i4FilterType);

VOID InitialiseDiffSrvStatus (VOID);
INT4 DsInit  (VOID);
INT4 DsStart (VOID);

INT4 DfsLock (VOID);
INT4 DfsUnLock (VOID);

INT4 DsCreatePort (UINT2);
INT4 DsProgramSchAlgo (VOID);
#endif /* _DIFFSRV_H */
