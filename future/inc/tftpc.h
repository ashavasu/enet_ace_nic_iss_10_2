/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tftpc.h,v 1.9 2015/04/27 12:49:41 siva Exp $
 *
 * Description:  
 *
 *******************************************************************/
#ifndef __TFTPC_H__
#define __TFTPC_H__
#include "utilipvx.h"
typedef enum {
TFTPC_OK       =0,
TFTPC_E_GEN    =-1,
TFTPC_E_NOFILE =-2,
TFTPC_E_ACCESS =-3,
TFTPC_E_FULL   =-4,
TFTPC_E_EXISTS =-5,
TFTPC_E_HOST   =-6,
TFTPC_E_ARG    =-7,
TFTPC_E_READ   =-8,
TFTPC_E_WRITE  =-9,
TFTPC_E_CREATE =-10,
TFTPC_E_HUGE   =-11,
TFTPC_E_BUF    =-12,
TFTPC_E_SERVER =-13
}tTftpcErr;

typedef enum {
    TFTP_INITIATE,
    TFTP_SEND_DATA,
    TFTP_TERMINATE
} tTftpcConnStatus;

#define TFTPC_ERR_MSGS { \
   (INT1 *) "OK",                          /* TFTPC_OK */ \
   (INT1 *) "INTERNAL ERROR",              /* TFTPC_E_GEN */ \
   (INT1 *) "No Such File",                /* TFTPC_E_NOFILE */ \
   (INT1 *) "Access Violation",            /* TFTPC_E_ACCESS */ \
   (INT1 *) "No Memory Left",              /* TFTPC_E_FULL */ \
   (INT1 *) "File Exists",                 /* TFTPC_E_EXISTS */ \
   (INT1 *) "Unable to Reach Host",        /* TFTPC_E_HOST */ \
   (INT1 *) "Invalid Argument(s)",         /* TFTPC_E_ARG */ \
   (INT1 *) "Error In reading file",       /* TFTPC_E_READ */ \
   (INT1 *) "Error In writing file",       /* TFTPC_E_WRITE */ \
   (INT1 *) "Error in Creating file",      /* TFTPC_E_CREATE */ \
   (INT1 *) "File is too big for TFTP ",   /* TFTPC_E_HUGE */ \
   (INT1 *) "Given Buffer is too small",   /* TFTPC_E_BUF */ \
   (INT1 *) "Server Returned Error Pkt"    /* TFTPC_E_SERVER */ \
}

extern const INT1 *gaTftpcErrMsg[];
#define TFTP_IN_PROGRESS 1
#define TFTP_PROCESS_COMPLETE 0

#define TFTPC_ERR_MSG(err)     gaTftpcErrMsg[-(err)]

/* This function parses the given tftp_url token value and 
 * if valid,returns the ip address in pu4IpAddr & the Filename in pi1TftpFileName
 * Returns CLI_SUCCESS/CLI_FAILURE
 */

INT4 CliGetTftpParams (INT1 *pi1TftpStr, INT1 *pi1TftpFileName,
                  tIPvXAddr *pIpAddress, UINT1 *pu1HostName);
INT4 tftpcRecvFileToBuf(tIPvXAddr SrvIp,const UINT1 *pu1File, UINT1 *pu1Buf,INT4 i4BufSize, UINT4 *pu4Data);
INT4 tftpcRecvFile(tIPvXAddr SrvIp,const UINT1 *pu1RemoteFile, const UINT1 *pu1LocalFile);
INT4 tftpcSendFile(tIPvXAddr SrvIp,const UINT1 *pu1RemoteFile, const UINT1 *pu1LocalFile);
INT4 tftpcSendBuf (tIPvXAddr SrvIp, const UINT1 *pu1File, UINT1 *pu1Buf,
                   UINT4 u4BufSize);
INT4 tftpcSendMultipleBuffers (tIPvXAddr SrvIp, const UINT1 *pu1File, 
                               UINT1 *pu1Buf, UINT4 u4BufSize, 
                               tTftpcConnStatus ConnStatus);
INT4 issCopyLocalFile(UINT1 *pu1Src, UINT1 *pu1Dst);
INT4 issMoveLocalFile(UINT1 *pu1Src, UINT1 *pu1Dst);
INT4 issGetLocalFileSize(const UINT1 *pu1FileName, UINT4 *pu4Size);
INT4 IssTftpUploadLogs (UINT1 u1Status,UINT1 *pu1ErrBuf);
    
#endif /* __TFTPC_H__ */
