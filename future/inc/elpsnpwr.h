/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: elpsnpwr.h,v 1.2 2013/01/10 12:43:21 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              for ELPS wrappers
 *              
 ********************************************************************/
#ifndef __ELPS_NP_WR_H_
#define __ELPS_NP_WR_H_

#include "elps.h"
/* OPER ID */

#define  FS_MI_ELPS_HW_PG_SWITCH_DATA_PATH                      1
#ifdef MBSM_WANTED
#define  FS_MI_ELPS_MBSM_HW_PG_SWITCH_DATA_PATH                 2
#endif

/* Required arguments list for the elps NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

typedef struct {
    UINT4                  u4ContextId;
    UINT4                  u4PgId;
    tElpsHwPgSwitchInfo *  pPgHwInfo;
} tElpsNpWrFsMiElpsHwPgSwitchDataPath;

#ifdef MBSM_WANTED
typedef struct {
    UINT4                  u4ContextId;
    UINT4                  u4PgId;
    tElpsHwPgSwitchInfo *  pPgHwInfo;
    tMbsmSlotInfo *        pSlotInfo;
} tElpsNpWrFsMiElpsMbsmHwPgSwitchDataPath;

#endif
typedef struct ElpsNpModInfo {
union {
    tElpsNpWrFsMiElpsHwPgSwitchDataPath  sFsMiElpsHwPgSwitchDataPath;
#ifdef MBSM_WANTED
    tElpsNpWrFsMiElpsMbsmHwPgSwitchDataPath  sFsMiElpsMbsmHwPgSwitchDataPath;
#endif
    }unOpCode;

#define  ElpsNpFsMiElpsHwPgSwitchDataPath  unOpCode.sFsMiElpsHwPgSwitchDataPath;
#ifdef MBSM_WANTED
#define  ElpsNpFsMiElpsMbsmHwPgSwitchDataPath  unOpCode.sFsMiElpsMbsmHwPgSwitchDataPath;
#endif
} tElpsNpModInfo;


/* Function Prototype for Generic NP OpCode specific wrappers
 * defined in Elpsnpapi.c */

UINT1 ElpsFsMiElpsHwPgSwitchDataPath PROTO ((UINT4 u4ContextId, UINT4 u4PgId, tElpsHwPgSwitchInfo * pPgHwInfo));
#ifdef MBSM_WANTED
UINT1 ElpsFsMiElpsMbsmHwPgSwitchDataPath PROTO ((UINT4 u4ContextId, UINT4 u4PgId, tElpsHwPgSwitchInfo * pPgHwInfo, tMbsmSlotInfo * pSlotInfo));
#endif

#endif /* __ELPS_NP_WR_H_ */
