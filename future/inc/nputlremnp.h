/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: nputlremnp.h,v 1.78 2018/02/13 09:11:18 siva Exp $
 *
 * Description:This file contains the necessary data strucutures
 *             for packet handling in NPAPI layer
 *             <Part of dual node stacking model>
 *
 *******************************************************************/
#ifndef _NPUTLREMNP_H_
#define _NPUTLREMNP_H_

#include "nputil.h"
#include "vlanminp.h"
#include "vlannp.h"
#include "vlnmpbnp.h"
#include "vlnpbnp.h"
#include "cfanp.h"
#include "pvrstminp.h"
#include "pvrstnp.h"
#include "rstminp.h"
#include "rstmpbbnp.h"
#include "rstmpbnp.h"
#include "rstnp.h"
#include "rstpbnp.h"
#include "mstminp.h"
#include "mstnp.h"
#include "maunp.h"
#include "ethernp.h"
#include "vcmnp.h"
#include "pnacnp.h"
#include "fmnp.h"
#include "igsminp.h"
#include "mldsnp.h"
#include "mldsminp.h"
#include "issnp.h"
#include "isspinp.h"
#include "eoamnp.h"
#include "ecfmminp.h"
#include "cfmoffnp.h"
#include "lanp.h"
#include "qosxnp.h"
#include "elpsnp.h"
#include "erpsnp.h"
#include "rmonnp.h"
#include "rmon2np.h"
#include "dsmonnp.h"
#include "syncenp.h"
#include "ipnp.h"
#include "rportnp.h"
#include "mplsnp.h"
#include "igmpnp.h"
#include "mldnp.h"
#include "ipmcnp.h"
#include "ip6mcnp.h"
#include "fsbnp.h"

/* Required arguments list for the module NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

/* Local Structures for the  Special pointer handling */
typedef struct _tRemoteHwPortArray {
    UINT4       au4PortArray[VLAN_MAX_PORTS];
    INT4        i4Length;
}tRemoteHwPortArray;

typedef struct _tRemoteHwVlanPortArray {
    tRemoteHwPortArray  HwMemberPortArray;
    tRemoteHwPortArray  HwUntagPortArray;
    tRemoteHwPortArray  HwFwdAllPortArray;
    tRemoteHwPortArray  HwFwdUnregPortArray;
}tRemoteHwVlanPortArray;

typedef struct _tRemoteHwVlanFwdInfo
{
    tVlanTag      VlanTag;
    tRemoteHwPortArray  TagPorts;
    tRemoteHwPortArray  UnTagPorts;
    UINT4         u4ContextId;
}tRemoteHwVlanFwdInfo;

typedef struct {
    tFsNpVlanPortReflectEntry   PortReflectEntry;
} tVlanRemoteNpWrFsMiVlanHwPortPktReflectStatus;

#ifdef VLAN_WANTED
typedef struct {
    UINT4  u4ContextId;
} tVlanRemoteNpWrFsMiVlanHwInit;

typedef struct {
    UINT4  u4ContextId;
} tVlanRemoteNpWrFsMiVlanHwDeInit;

typedef struct {
    UINT4           u4ContextId;
    tRemoteHwPortArray   HwAllGroupPorts;
    tVlanId         VlanId;
    UINT1           au1Pad[2];
} tVlanRemoteNpWrFsMiVlanHwSetAllGroupsPorts;

typedef struct {
    UINT4           u4ContextId;
    tRemoteHwPortArray   HwResetAllGroupPorts;
    tVlanId         VlanId;
    UINT1           au1Pad[2];
} tVlanRemoteNpWrFsMiVlanHwResetAllGroupsPorts;

typedef struct {
    UINT4           u4ContextId;
    tRemoteHwPortArray   HwResetUnRegGroupPorts;
    tVlanId         VlanId;
    UINT1           au1Pad[2];
} tVlanRemoteNpWrFsMiVlanHwResetUnRegGroupsPorts;

typedef struct {
    UINT4           u4ContextId;
    tRemoteHwPortArray   HwUnRegPorts;
    tVlanId         VlanId;
    UINT1           au1Pad[2];
} tVlanRemoteNpWrFsMiVlanHwSetUnRegGroupsPorts;

typedef struct {
    UINT4           u4ContextId;
    UINT4           u4Fid;
    UINT4           u4Port;
    tRemoteHwPortArray   HwAllowedToGoPorts;
    tMacAddr        MacAddr;
    UINT1           u1Status;
    UINT1           au1Pad[1];
} tVlanRemoteNpWrFsMiVlanHwAddStaticUcastEntry;

typedef struct {
    UINT4     u4ContextId;
    UINT4     u4Fid;
    UINT4     u4Port;
    tMacAddr  MacAddr;
    UINT1     au1Pad[2];
} tVlanRemoteNpWrFsMiVlanHwDelStaticUcastEntry;

typedef struct {
    UINT4                 u4ContextId;
    UINT4                 u4FdbId;
    tHwUnicastMacEntry    Entry;
    tMacAddr              MacAddr;
    UINT1                 au1Pad[2];
} tVlanRemoteNpWrFsMiVlanHwGetFdbEntry;

typedef struct {
    UINT4   u4Port;
    UINT1   pHwAddr[6];
    UINT2   u2VlanId;
} tVlanRemoteNpWrFsNpHwGetPortFromFdb;

#ifndef SW_LEARNING
typedef struct {
    UINT4    u4ContextId;
    UINT4    u4FdbId;
    UINT4    u4Count;
} tVlanRemoteNpWrFsMiVlanHwGetFdbCount;

typedef struct {
    UINT4     u4ContextId;
    UINT4     u4FdbId;
    tMacAddr  MacAddr;
    UINT1     au1Pad[2];
} tVlanRemoteNpWrFsMiVlanHwGetFirstTpFdbEntry;

typedef struct {
    UINT4     u4ContextId;
    UINT4     u4FdbId;
    UINT4     u4NextContextId;
    UINT4     u4NextFdbId;
    tMacAddr  MacAddr;
    tMacAddr  NextMacAddr;
} tVlanRemoteNpWrFsMiVlanHwGetNextTpFdbEntry;

#endif /* SW_LEARNING */
typedef struct {
    UINT4           u4ContextId;
    tRemoteHwPortArray   HwMcastPorts;
    tVlanId         VlanId;
    tMacAddr        MacAddr;
} tVlanRemoteNpWrFsMiVlanHwAddMcastEntry;

typedef struct {
    UINT4           u4ContextId;
    tVlanId         VlanId;
    tMacAddr        MacAddr;
    INT4            i4RcvPort;
    tRemoteHwPortArray   HwMcastPorts;
} tVlanRemoteNpWrFsMiVlanHwAddStMcastEntry;

typedef struct {
    UINT4     u4ContextId;
    UINT4     u4IfIndex;
    tVlanId   VlanId;
    tMacAddr  MacAddr;
} tVlanRemoteNpWrFsMiVlanHwSetMcastPort;

typedef struct {
    UINT4     u4ContextId;
    UINT4     u4IfIndex;
    tVlanId   VlanId;
    tMacAddr  MacAddr;
} tVlanRemoteNpWrFsMiVlanHwResetMcastPort;

typedef struct {
    UINT4     u4ContextId;
    tVlanId   VlanId;
    tMacAddr  MacAddr;
} tVlanRemoteNpWrFsMiVlanHwDelMcastEntry;

typedef struct {
    UINT4     u4ContextId;
    INT4      i4RcvPort;
    tVlanId   VlanId;
    tMacAddr  MacAddr;
} tVlanRemoteNpWrFsMiVlanHwDelStMcastEntry;

typedef struct {
    UINT4           u4ContextId;
    tRemoteHwPortArray   HwEgressPorts;
    tRemoteHwPortArray   HwUnTagPorts;
    tVlanId         VlanId;
    UINT1           au1Pad[2];
} tVlanRemoteNpWrFsMiVlanHwAddVlanEntry;

typedef struct {
    UINT4   u4ContextId;
    tVlanId VlanId;
    UINT1   au1Pad[2];
} tVlanRemoteNpWrFsMiVlanHwDelVlanEntry;

typedef struct {
    UINT4    u4ContextId;
    UINT4    u4IfIndex;
    tVlanId  VlanId;
    UINT1    u1IsTagged;
    UINT1    au1Pad[1];
} tVlanRemoteNpWrFsMiVlanHwSetVlanMemberPort;

typedef struct {
    UINT4    u4ContextId;
    UINT4    u4IfIndex;
    tVlanId  VlanId;
    UINT1    au1Pad[2];
} tVlanRemoteNpWrFsMiVlanHwResetVlanMemberPort;

typedef struct {
    UINT4    u4ContextId;
    UINT4    u4IfIndex;
    tVlanId  VlanId;
    UINT1    au1Pad[2];
} tVlanRemoteNpWrFsMiVlanHwSetPortPvid;

typedef struct {
    UINT4    u4ContextId;
    tVlanId  VlanId;
    UINT1    au1Pad[2];
} tVlanRemoteNpWrFsMiVlanHwSetDefaultVlanId;

#ifdef L2RED_WANTED
typedef struct {
    UINT4    u4ContextId;
    tVlanId  SwVlanId;
    UINT1    au1Pad[2];
} tVlanRemoteNpWrFsMiVlanHwSyncDefaultVlanId;

typedef struct {
    UINT4    u4ContextId;
    tVlanId  VlanId;
    UINT1    au1Pad[2];
} tVlanRemoteNpWrFsMiVlanRedHwUpdateDBForDefaultVlanId;

typedef struct {
    UINT4              u4ContextId;
    UINT4              u4Port;
    FsMiVlanHwProtoCb  ProtoVlanCallBack;
} tVlanRemoteNpWrFsMiVlanHwScanProtocolVlanTbl;

typedef struct {
    UINT4                 u4ContextId;
    UINT4                 u4IfIndex;
    UINT4                 u4GroupId;
    tVlanProtoTemplate    ProtoTemplate;
    tVlanId               VlanId;
    UINT1                 au1Pad[2];
} tVlanRemoteNpWrFsMiVlanHwGetVlanProtocolMap;

typedef struct {
    UINT4              u4ContextId;
    FsMiVlanHwMcastCb  McastCallBack;
} tVlanRemoteNpWrFsMiVlanHwScanMulticastTbl;

typedef struct {
    UINT4           u4ContextId;
    tVlanId         VlanId;
    tMacAddr        MacAddr;
    UINT4           u4RcvPort;
    tRemoteHwPortArray   HwMcastPorts;
} tVlanRemoteNpWrFsMiVlanHwGetStMcastEntry;

typedef struct {
    UINT4              u4ContextId;
    FsMiVlanHwUcastCb  UcastCallBack;
} tVlanRemoteNpWrFsMiVlanHwScanUnicastTbl;

typedef struct {
    UINT4           u4ContextId;
    UINT4           u4Fid;
    UINT4           u4Port;
    tRemoteHwPortArray   AllowedToGoPorts;
    tMacAddr        MacAddr;
    UINT1           u1Status;
    UINT1           au1Pad[1];
} tVlanRemoteNpWrFsMiVlanHwGetStaticUcastEntry;

typedef struct {
    UINT4     u4ContextId;
    UINT2     u2Protocol;
    tMacAddr  MacAddr;
} tVlanRemoteNpWrFsMiVlanHwGetSyncedTnlProtocolMacAddr;

#endif /* L2RED_WANTED */
typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
    INT1   u1AccFrameType;
    UINT1  au1Pad[3];
} tVlanRemoteNpWrFsMiVlanHwSetPortAccFrameType;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
    UINT1  u1IngFilterEnable;
    UINT1  au1Pad[3];
} tVlanRemoteNpWrFsMiVlanHwSetPortIngFiltering;

typedef struct {
    UINT4  u4ContextId;
} tVlanRemoteNpWrFsMiVlanHwVlanEnable;

typedef struct {
    UINT4  u4ContextId;
} tVlanRemoteNpWrFsMiVlanHwVlanDisable;

typedef struct {
    UINT4  u4ContextId;
    UINT1  u1LearningType;
    UINT1  au1Pad[3];
} tVlanRemoteNpWrFsMiVlanHwSetVlanLearningType;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
    UINT1  u1MacBasedVlanEnable;
    UINT1  au1Pad[3];
} tVlanRemoteNpWrFsMiVlanHwSetMacBasedStatusOnPort;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
    UINT1  u1SubnetBasedVlanEnable;
    UINT1  au1Pad[3];
} tVlanRemoteNpWrFsMiVlanHwSetSubnetBasedStatusOnPort;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
    UINT1  u1VlanProtoEnable;
    UINT1  au1Pad[3];
} tVlanRemoteNpWrFsMiVlanHwEnableProtoVlanOnPort;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
    INT4   i4DefPriority;
} tVlanRemoteNpWrFsMiVlanHwSetDefUserPriority;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
    INT4   i4NumTraffClass;
} tVlanRemoteNpWrFsMiVlanHwSetPortNumTrafClasses;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
    INT4   i4UserPriority;
    INT4   i4RegenPriority;
} tVlanRemoteNpWrFsMiVlanHwSetRegenUserPriority;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
    INT4   i4UserPriority;
    INT4   i4TraffClass;
} tVlanRemoteNpWrFsMiVlanHwSetTraffClassMap;

typedef struct {
    UINT4  u4ContextId;
} tVlanRemoteNpWrFsMiVlanHwGmrpEnable;

typedef struct {
    UINT4  u4ContextId;
} tVlanRemoteNpWrFsMiVlanHwGmrpDisable;

typedef struct {
    UINT4  u4ContextId;
} tVlanRemoteNpWrFsMiVlanHwGvrpEnable;

typedef struct {
    UINT4  u4ContextId;
} tVlanRemoteNpWrFsMiVlanHwGvrpDisable;

typedef struct {
    UINT4  u4ContextId;
} tVlanRemoteNpWrFsMiNpDeleteAllFdbEntries;

typedef struct {
    UINT4                 u4ContextId;
    UINT4                 u4IfIndex;
    UINT4                 u4GroupId;
    tVlanProtoTemplate    ProtoTemplate;
    tVlanId               VlanId;
    UINT1                 au1Pad[2];
} tVlanRemoteNpWrFsMiVlanHwAddVlanProtocolMap;

typedef struct {
    UINT4                 u4ContextId;
    UINT4                 u4IfIndex;
    UINT4                 u4GroupId;
    tVlanProtoTemplate    ProtoTemplate;
} tVlanRemoteNpWrFsMiVlanHwDelVlanProtocolMap;

typedef struct {
    UINT4    u4ContextId;
    UINT4    u4Port;
    UINT4    u4PortStatsValue;
    tVlanId  VlanId;
    UINT1    u1StatsType;
    UINT1    au1Pad[1];
} tVlanRemoteNpWrFsMiVlanHwGetPortStats;

typedef struct {
    UINT4                   u4ContextId;
    UINT4                   u4Port;
    tSNMP_COUNTER64_TYPE    Value;
    tVlanId                 VlanId;
    UINT1                   u1StatsType;
    UINT1                   au1Pad[1];
} tVlanRemoteNpWrFsMiVlanHwGetPortStats64;

typedef struct {
    UINT4    u4ContextId;
    UINT4    u4VlanStatsValue;
    tVlanId  VlanId;
    UINT1    u1StatsType;
    UINT1    au1Pad[1];
} tVlanRemoteNpWrFsMiVlanHwGetVlanStats;

typedef struct {
    UINT4    u4ContextId;
    tVlanId  VlanId;
    UINT1    au1Pad[2];
} tVlanRemoteNpWrFsMiVlanHwResetVlanStats;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
    UINT4  u4Mode;
} tVlanRemoteNpWrFsMiVlanHwSetPortTunnelMode;

typedef struct {
    UINT4  u4ContextId;
    INT4   i4BridgeMode;
} tVlanRemoteNpWrFsMiVlanHwSetTunnelFilter;

typedef struct {
    UINT4    u4ContextId;
    UINT1    u1TagSet;
    UINT1    au1Pad[3];
} tVlanRemoteNpWrFsMiVlanHwCheckTagAtEgress;

typedef struct {
    UINT4    u4ContextId;
    UINT1    u1TagSet;
    UINT1    au1Pad[3];
} tVlanRemoteNpWrFsMiVlanHwCheckTagAtIngress;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4Fid;
} tVlanRemoteNpWrFsMiVlanHwCreateFdbId;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4Fid;
} tVlanRemoteNpWrFsMiVlanHwDeleteFdbId;

typedef struct {
    UINT4    u4ContextId;
    UINT4    u4Fid;
    tVlanId  VlanId;
    UINT1    au1Pad[2];
} tVlanRemoteNpWrFsMiVlanHwAssociateVlanFdb;

typedef struct {
    UINT4    u4ContextId;
    UINT4    u4Fid;
    tVlanId  VlanId;
    UINT1    au1Pad[2];
} tVlanRemoteNpWrFsMiVlanHwDisassociateVlanFdb;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4Port;
    UINT4  u4Fid;
    INT4   i4OptimizeFlag;
} tVlanRemoteNpWrFsMiVlanHwFlushPortFdbId;

typedef struct {
    UINT4             u4ContextId;
    tVlanFlushInfo   VlanFlushInfo;
} tVlanRemoteNpWrFsMiVlanHwFlushPortFdbList;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
    INT4   i4OptimizeFlag;
} tVlanRemoteNpWrFsMiVlanHwFlushPort;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4Fid;
} tVlanRemoteNpWrFsMiVlanHwFlushFdbId;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4Port;
    INT4   i4AgingTime;
} tVlanRemoteNpWrFsMiVlanHwSetShortAgeout;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4Port;
    INT4   i4LongAgeout;
} tVlanRemoteNpWrFsMiVlanHwResetShortAgeout;

typedef struct {
    UINT4               u4ContextId;
    tRemoteHwVlanPortArray   HwEntry;
    tVlanId             VlanId;
    UINT1               au1Pad[2];
} tVlanRemoteNpWrFsMiVlanHwGetVlanInfo;

typedef struct {
    UINT4           u4ContextId;
    tVlanId         VlanId;
    tMacAddr        MacAddr;
    tRemoteHwPortArray   HwMcastPorts;
} tVlanRemoteNpWrFsMiVlanHwGetMcastEntry;

typedef struct {
    UINT4  u4ContextId;
    INT4   i4CosqValue;
    UINT1  u1Priority;
    UINT1  au1Pad[3];
} tVlanRemoteNpWrFsMiVlanHwTraffClassMapInit;

#ifndef BRIDGE_WANTED
typedef struct {
    UINT4  u4ContextId;
    INT4   i4AgingTime;
} tVlanRemoteNpWrFsMiBrgSetAgingTime;

#endif /* BRIDGE_WANTED */
typedef struct {
    UINT4  u4ContextId;
    UINT4  u4BridgeMode;
} tVlanRemoteNpWrFsMiVlanHwSetBrgMode;
 
 typedef struct {
    UINT4                u4ContextId;
    UINT4                u4IfIndex;
    tHwVlanPortProperty  VlanPortProperty;
    tMbsmSlotInfo       SlotInfo;
} tVlanRemoteNpWrFsMiVlanMbsmHwSetPortProperty;

typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4Mode;
} tVlanRemoteNpWrFsMiVlanHwSetBaseBridgeMode;

typedef struct {
    UINT4     u4ContextId;
    UINT4     u4IfIndex;
    tMacAddr  MacAddr;
    tVlanId   VlanId;
    BOOL1     bSuppressOption;
    UINT1     au1Pad[3];
} tVlanRemoteNpWrFsMiVlanHwAddPortMacVlanEntry;

typedef struct {
    UINT4     u4ContextId;
    UINT4     u4IfIndex;
    tMacAddr  MacAddr;
    UINT1     au1Pad[2];
} tVlanRemoteNpWrFsMiVlanHwDeletePortMacVlanEntry;

typedef struct {
    UINT4    u4ContextId;
    UINT4    u4IfIndex;
    UINT4    SubnetAddr;
    tVlanId  VlanId;
    BOOL1    bARPOption;
    UINT1    au1Pad[1];
} tVlanRemoteNpWrFsMiVlanHwAddPortSubnetVlanEntry;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
    UINT4  SubnetAddr;
} tVlanRemoteNpWrFsMiVlanHwDeletePortSubnetVlanEntry;

typedef struct {
    UINT4    u4ContextId;
    UINT4    u4IfIndex;
    UINT4    u4SubnetAddr;
    UINT4    u4SubnetMask;
    tVlanId  VlanId;
    BOOL1    bARPOption;
    UINT1    u1Action;
} tVlanRemoteNpWrFsMiVlanHwUpdatePortSubnetVlanEntry;

typedef struct {
    UINT4  u4ContextId;
} tVlanRemoteNpWrFsMiVlanNpHwRunMacAgeing;

typedef struct {
    UINT4    u4ContextId;
    tVlanId  VlanId;
    UINT2    u2FdbId;
    UINT4    u4MacLimit;
} tVlanRemoteNpWrFsMiVlanHwMacLearningLimit;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4MacLimit;
} tVlanRemoteNpWrFsMiVlanHwSwitchMacLearningLimit;

typedef struct {
    UINT4           u4ContextId;
    tVlanId         VlanId;
    UINT2           u2FdbId;
    tRemoteHwPortArray   HwEgressPorts;
    UINT1           u1Status;
    UINT1           au1Pad[3];
} tVlanRemoteNpWrFsMiVlanHwMacLearningStatus;

typedef struct {
    UINT4         u4ContextId;
    UINT4         u4Fid;
    tRemoteHwPortArray  HwPortList;
    UINT4         u4Port;
    UINT1         u1Action;
    UINT1         au1Pad[3];
} tVlanRemoteNpWrFsMiVlanHwSetFidPortLearningStatus;

typedef struct {
    UINT4                 u4ContextId;
    UINT4                 u4IfIndex;
    tVlanHwTunnelFilters  ProtocolId;
    UINT4                 u4TunnelStatus;
} tVlanRemoteNpWrFsMiVlanHwSetProtocolTunnelStatusOnPort;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
    INT4   i4ProtectedStatus;
} tVlanRemoteNpWrFsMiVlanHwSetPortProtectedStatus;

typedef struct {
    UINT4           u4ContextId;
    UINT4           u4Fid;
    UINT4           u4Port;
    tRemoteHwPortArray   HwAllowedToGoPorts;
    tMacAddr        MacAddr;
    tMacAddr        ConnectionId;
    UINT1           u1Status;
    UINT1           au1Pad[3];
} tVlanRemoteNpWrFsMiVlanHwAddStaticUcastEntryEx;

typedef struct {
    UINT4           u4ContextId;
    UINT4           u4Fid;
    UINT4           u4Port;
    tRemoteHwPortArray   AllowedToGoPorts;
    tMacAddr        MacAddr;
    tMacAddr        ConnectionId;
    UINT1           u1Status;
    UINT1           au1Pad[3];
} tVlanRemoteNpWrFsMiVlanHwGetStaticUcastEntryEx;

typedef struct {
    tRemoteHwVlanFwdInfo   VlanFwdInfo;
    UINT2            u2PacketLen;
    UINT1            u1Packet;
    UINT1            au1Pad[1];
} tVlanRemoteNpWrFsVlanHwForwardPktOnPorts;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
    UINT1  u1Status;
    UINT1  au1Pad[3];
} tVlanRemoteNpWrFsMiVlanHwPortMacLearningStatus;

typedef struct {
    UINT4   u4LearningMode;
} tVlanRemoteNpWrFsVlanHwGetMacLearningMode;

typedef struct {
    tHwMcastIndexInfo  HwMcastIndexInfo;
    UINT4             u4McastIndex;
} tVlanRemoteNpWrFsMiVlanHwSetMcastIndex;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
    UINT2  u2EtherType;
    UINT1  au1Pad[2];
} tVlanRemoteNpWrFsMiVlanHwSetPortIngressEtherType;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
    UINT2  u2EtherType;
    UINT1  au1Pad[2];
} tVlanRemoteNpWrFsMiVlanHwSetPortEgressEtherType;

typedef struct {
    tHwVlanPortProperty VlanPortProperty;
    UINT4               u4IfIndex;
    UINT4               u4ContextId;
} tVlanRemoteNpWrFsMiVlanHwSetPortProperty;

typedef struct {
   tVlanEvbHwConfigInfo VlanEvbHwConfigInfo;
} tVlanRemoteNpWrFsMiVlanHwEvbConfigSChIface;

typedef struct {
    tVlanEvbHwConfigInfo VlanEvbHwConfigInfo;
} tVlanRemoteNpWrFsMiVlanMbsmHwEvbConfigSChIface;

typedef struct {
    tVlanHwPortInfo VlanHwPortInfo;
} tVlanRemoteNpWrFsMiVlanHwSetBridgePortType;

typedef struct {
    tVlanHwPortInfo VlanHwPortInfo;
} tVlanRemoteNpWrFsMiVlanMbsmHwSetBridgePortType;

#ifdef PB_WANTED
typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
    UINT4  u4PortType;
} tVlanRemoteNpWrFsMiVlanHwSetProviderBridgePortType;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
    UINT1  u1Status;
    UINT1  au1Pad[3];
} tVlanRemoteNpWrFsMiVlanHwSetPortSVlanTranslationStatus;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
    UINT2  u2LocalSVlan;
    UINT2  u2RelaySVlan;
} tVlanRemoteNpWrFsMiVlanHwAddSVlanTranslationEntry;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
    UINT2  u2LocalSVlan;
    UINT2  u2RelaySVlan;
} tVlanRemoteNpWrFsMiVlanHwDelSVlanTranslationEntry;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
    UINT1  u1Status;
    UINT1  au1Pad[3];
} tVlanRemoteNpWrFsMiVlanHwSetPortEtherTypeSwapStatus;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
    UINT2  u2LocalEtherType;
    UINT2  u2RelayEtherType;
} tVlanRemoteNpWrFsMiVlanHwAddEtherTypeSwapEntry;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
    UINT2  u2LocalEtherType;
    UINT2  u2RelayEtherType;
} tVlanRemoteNpWrFsMiVlanHwDelEtherTypeSwapEntry;

typedef struct {
    UINT4          u4ContextId;
    tVlanSVlanMap  VlanSVlanMap;
} tVlanRemoteNpWrFsMiVlanHwAddSVlanMap;

typedef struct {
    UINT4          u4ContextId;
    tVlanSVlanMap  VlanSVlanMap;
} tVlanRemoteNpWrFsMiVlanHwDeleteSVlanMap;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
    UINT1  u1TableType;
    UINT1  au1Pad[3];
} tVlanRemoteNpWrFsMiVlanHwSetPortSVlanClassifyMethod;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
    UINT4  u4MacLimit;
} tVlanRemoteNpWrFsMiVlanHwPortMacLearningLimit;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4MacLimit;
} tVlanRemoteNpWrFsMiVlanHwMulticastMacTableLimit;

typedef struct {
    UINT4    u4ContextId;
    UINT4    u4IfIndex;
    tVlanId  CVlanId;
    UINT1    au1Pad[2];
} tVlanRemoteNpWrFsMiVlanHwSetPortCustomerVlan;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
} tVlanRemoteNpWrFsMiVlanHwResetPortCustomerVlan;

typedef struct {
    UINT4             u4ContextId;
    UINT4             u4IfIndex;
    tHwVlanPbPepInfo  PepConfig;
    tVlanId           SVlanId;
    UINT1             au1Pad[2];
} tVlanRemoteNpWrFsMiVlanHwCreateProviderEdgePort;

typedef struct {
    UINT4    u4ContextId;
    UINT4    u4IfIndex;
    tVlanId  SVlanId;
    UINT1    au1Pad[2];
} tVlanRemoteNpWrFsMiVlanHwDelProviderEdgePort;

typedef struct {
    UINT4    u4ContextId;
    UINT4    u4IfIndex;
    tVlanId  SVlanId;
    tVlanId  Pvid;
} tVlanRemoteNpWrFsMiVlanHwSetPepPvid;

typedef struct {
    UINT4    u4ContextId;
    UINT4    u4IfIndex;
    tVlanId  SVlanId;
    UINT1    u1AccepFrameType;
    UINT1    au1Pad[1];
} tVlanRemoteNpWrFsMiVlanHwSetPepAccFrameType;

typedef struct {
    UINT4    u4ContextId;
    UINT4    u4IfIndex;
    INT4     i4DefUsrPri;
    tVlanId  SVlanId;
    UINT1    au1Pad[2];
} tVlanRemoteNpWrFsMiVlanHwSetPepDefUserPriority;

typedef struct {
    UINT4    u4ContextId;
    UINT4    u4IfIndex;
    tVlanId  SVlanId;
    UINT1    u1IngFilterEnable;
    UINT1    au1Pad[1];
} tVlanRemoteNpWrFsMiVlanHwSetPepIngFiltering;

typedef struct {
    UINT4          u4ContextId;
    tVlanSVlanMap  VlanSVlanMap;
} tVlanRemoteNpWrFsMiVlanHwSetCvidUntagPep;

typedef struct {
    UINT4          u4ContextId;
    tVlanSVlanMap  VlanSVlanMap;
} tVlanRemoteNpWrFsMiVlanHwSetCvidUntagCep;

typedef struct {
    UINT4             u4ContextId;
    UINT4             u4IfIndex;
    tHwVlanPbPcpInfo  NpPbVlanPcpInfo;
} tVlanRemoteNpWrFsMiVlanHwSetPcpEncodTbl;

typedef struct {
    UINT4             u4ContextId;
    UINT4             u4IfIndex;
    tHwVlanPbPcpInfo  NpPbVlanPcpInfo;
} tVlanRemoteNpWrFsMiVlanHwSetPcpDecodTbl;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
    UINT1  u1UseDei;
    UINT1  au1Pad[3];
} tVlanRemoteNpWrFsMiVlanHwSetPortUseDei;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
    UINT1  u1ReqDrpEncoding;
    UINT1  au1Pad[3];
} tVlanRemoteNpWrFsMiVlanHwSetPortReqDropEncoding;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
    UINT2  u2PcpSelection;
    UINT1  au1Pad[2];
} tVlanRemoteNpWrFsMiVlanHwSetPortPcpSelection;

typedef struct {
    UINT4    u4ContextId;
    UINT4    u4IfIndex;
    INT4     i4RecvPriority;
    INT4     i4RegenPriority;
    tVlanId  SVlanId;
    UINT1    au1Pad[2];
} tVlanRemoteNpWrFsMiVlanHwSetServicePriRegenEntry;

typedef struct {
    UINT4     u4ContextId;
    tMacAddr  MacAddr;
    UINT2     u2Protocol;
} tVlanRemoteNpWrFsMiVlanHwSetTunnelMacAddress;

typedef struct {
    UINT4            u4ContextId;
    tVlanSVlanMap    VlanSVlanMap;
    tVlanSVlanMap   RetVlanSVlanMap;
} tVlanRemoteNpWrFsMiVlanHwGetNextSVlanMap;

typedef struct {
    UINT4                 u4ContextId;
    UINT4                 u4IfIndex;
    tVidTransEntryInfo    VidTransEntryInfo;
    tVlanId               u2LocalSVlan;
    UINT1                 au1Pad[2];
} tVlanRemoteNpWrFsMiVlanHwGetNextSVlanTranslationEntry;
#endif /* PB_WANTED */
#ifdef MBSM_WANTED 
typedef struct {
    tFDBInfoArray   FDBInfoArray;
} tVlanRemoteNpWrFsMiVlanMbsmSyncFDBInfo;

typedef struct {
    tFsNpVlanPortReflectEntry   PortReflectEntry;
} tVlanRemoteNpWrFsMiVlanMbsmHwPortPktReflectStatus;

#endif /* MBSM_WANTED */

typedef struct {
    UINT4       u4ContextId;
    INT4        i4LoopbackStatus;
    tVlanId     VlanId;
 UINT1  au1Pad[2];
} tVlanRemoteNpWrFsMiVlanHwSetVlanLoopbackStatus;

typedef struct {
    UINT4             u4ContextId;
    tHwVlanCVlanStat  VlanStat;
} tVlanRemoteNpWrFsMiVlanHwSetCVlanStat;

typedef struct {
    UINT4    u4ContextId;
    UINT4 *  pu4VlanStatsValue;
    UINT2    u2CVlanId;
    UINT2    u2Port;
    UINT1    u1StatsType;
    UINT1    au1Pad[3];
} tVlanRemoteNpWrFsMiVlanHwGetCVlanStat;


typedef struct {
    UINT4    u4ContextId;
    UINT2    u2CVlanId;
    UINT2    u2Port;
} tVlanRemoteNpWrFsMiVlanHwClearCVlanStat;


typedef struct VlanRemoteNpModInfo {
union {
    tVlanRemoteNpWrFsMiVlanHwInit  sFsMiVlanHwInit;
    tVlanRemoteNpWrFsMiVlanHwDeInit  sFsMiVlanHwDeInit;
    tVlanRemoteNpWrFsMiVlanHwSetAllGroupsPorts  sFsMiVlanHwSetAllGroupsPorts;
    tVlanRemoteNpWrFsMiVlanHwResetAllGroupsPorts  sFsMiVlanHwResetAllGroupsPorts;
    tVlanRemoteNpWrFsMiVlanHwResetUnRegGroupsPorts  sFsMiVlanHwResetUnRegGroupsPorts;
    tVlanRemoteNpWrFsMiVlanHwSetUnRegGroupsPorts  sFsMiVlanHwSetUnRegGroupsPorts;
    tVlanRemoteNpWrFsMiVlanHwAddStaticUcastEntry  sFsMiVlanHwAddStaticUcastEntry;
    tVlanRemoteNpWrFsMiVlanHwDelStaticUcastEntry  sFsMiVlanHwDelStaticUcastEntry;
    tVlanRemoteNpWrFsMiVlanHwGetFdbEntry  sFsMiVlanHwGetFdbEntry;
#ifndef SW_LEARNING
    tVlanRemoteNpWrFsMiVlanHwGetFdbCount  sFsMiVlanHwGetFdbCount;
    tVlanRemoteNpWrFsMiVlanHwGetFirstTpFdbEntry  sFsMiVlanHwGetFirstTpFdbEntry;
    tVlanRemoteNpWrFsMiVlanHwGetNextTpFdbEntry  sFsMiVlanHwGetNextTpFdbEntry;
#endif /* SW_LEARNING */
    tVlanRemoteNpWrFsMiVlanHwAddMcastEntry  sFsMiVlanHwAddMcastEntry;
    tVlanRemoteNpWrFsMiVlanHwAddStMcastEntry  sFsMiVlanHwAddStMcastEntry;
    tVlanRemoteNpWrFsMiVlanHwSetMcastPort  sFsMiVlanHwSetMcastPort;
    tVlanRemoteNpWrFsMiVlanHwResetMcastPort  sFsMiVlanHwResetMcastPort;
    tVlanRemoteNpWrFsMiVlanHwDelMcastEntry  sFsMiVlanHwDelMcastEntry;
    tVlanRemoteNpWrFsMiVlanHwDelStMcastEntry  sFsMiVlanHwDelStMcastEntry;
    tVlanRemoteNpWrFsMiVlanHwAddVlanEntry  sFsMiVlanHwAddVlanEntry;
    tVlanRemoteNpWrFsMiVlanHwDelVlanEntry  sFsMiVlanHwDelVlanEntry;
    tVlanRemoteNpWrFsMiVlanHwSetVlanMemberPort  sFsMiVlanHwSetVlanMemberPort;
    tVlanRemoteNpWrFsMiVlanHwResetVlanMemberPort  sFsMiVlanHwResetVlanMemberPort;
    tVlanRemoteNpWrFsMiVlanHwSetPortPvid  sFsMiVlanHwSetPortPvid;
    tVlanRemoteNpWrFsMiVlanHwSetDefaultVlanId  sFsMiVlanHwSetDefaultVlanId;
#ifdef L2RED_WANTED
    tVlanRemoteNpWrFsMiVlanHwSyncDefaultVlanId  sFsMiVlanHwSyncDefaultVlanId;
    tVlanRemoteNpWrFsMiVlanRedHwUpdateDBForDefaultVlanId  sFsMiVlanRedHwUpdateDBForDefaultVlanId;
    tVlanRemoteNpWrFsMiVlanHwScanProtocolVlanTbl  sFsMiVlanHwScanProtocolVlanTbl;
    tVlanRemoteNpWrFsMiVlanHwGetVlanProtocolMap  sFsMiVlanHwGetVlanProtocolMap;
    tVlanRemoteNpWrFsMiVlanHwScanMulticastTbl  sFsMiVlanHwScanMulticastTbl;
    tVlanRemoteNpWrFsMiVlanHwGetStMcastEntry  sFsMiVlanHwGetStMcastEntry;
    tVlanRemoteNpWrFsMiVlanHwScanUnicastTbl  sFsMiVlanHwScanUnicastTbl;
    tVlanRemoteNpWrFsMiVlanHwGetStaticUcastEntry  sFsMiVlanHwGetStaticUcastEntry;
    tVlanRemoteNpWrFsMiVlanHwGetSyncedTnlProtocolMacAddr  sFsMiVlanHwGetSyncedTnlProtocolMacAddr;
#endif /* L2RED_WANTED */
    tVlanRemoteNpWrFsMiVlanHwSetPortAccFrameType  sFsMiVlanHwSetPortAccFrameType;
    tVlanRemoteNpWrFsMiVlanHwSetPortIngFiltering  sFsMiVlanHwSetPortIngFiltering;
    tVlanRemoteNpWrFsMiVlanHwVlanEnable  sFsMiVlanHwVlanEnable;
    tVlanRemoteNpWrFsMiVlanHwVlanDisable  sFsMiVlanHwVlanDisable;
    tVlanRemoteNpWrFsMiVlanHwSetVlanLearningType  sFsMiVlanHwSetVlanLearningType;
    tVlanRemoteNpWrFsMiVlanHwSetMacBasedStatusOnPort  sFsMiVlanHwSetMacBasedStatusOnPort;
    tVlanRemoteNpWrFsMiVlanHwSetSubnetBasedStatusOnPort  sFsMiVlanHwSetSubnetBasedStatusOnPort;
    tVlanRemoteNpWrFsMiVlanHwEnableProtoVlanOnPort  sFsMiVlanHwEnableProtoVlanOnPort;
    tVlanRemoteNpWrFsMiVlanHwSetDefUserPriority  sFsMiVlanHwSetDefUserPriority;
    tVlanRemoteNpWrFsMiVlanHwSetPortNumTrafClasses  sFsMiVlanHwSetPortNumTrafClasses;
    tVlanRemoteNpWrFsMiVlanHwSetRegenUserPriority  sFsMiVlanHwSetRegenUserPriority;
    tVlanRemoteNpWrFsMiVlanHwSetTraffClassMap  sFsMiVlanHwSetTraffClassMap;
    tVlanRemoteNpWrFsMiVlanHwGmrpEnable  sFsMiVlanHwGmrpEnable;
    tVlanRemoteNpWrFsMiVlanHwGmrpDisable  sFsMiVlanHwGmrpDisable;
    tVlanRemoteNpWrFsMiVlanHwGvrpEnable  sFsMiVlanHwGvrpEnable;
    tVlanRemoteNpWrFsMiVlanHwGvrpDisable  sFsMiVlanHwGvrpDisable;
    tVlanRemoteNpWrFsMiNpDeleteAllFdbEntries  sFsMiNpDeleteAllFdbEntries;
    tVlanRemoteNpWrFsMiVlanHwAddVlanProtocolMap  sFsMiVlanHwAddVlanProtocolMap;
    tVlanRemoteNpWrFsMiVlanHwDelVlanProtocolMap  sFsMiVlanHwDelVlanProtocolMap;
    tVlanRemoteNpWrFsMiVlanHwGetPortStats  sFsMiVlanHwGetPortStats;
    tVlanRemoteNpWrFsMiVlanHwGetPortStats64  sFsMiVlanHwGetPortStats64;
    tVlanRemoteNpWrFsMiVlanHwGetVlanStats  sFsMiVlanHwGetVlanStats;
    tVlanRemoteNpWrFsMiVlanHwResetVlanStats  sFsMiVlanHwResetVlanStats;
    tVlanRemoteNpWrFsMiVlanHwSetPortTunnelMode  sFsMiVlanHwSetPortTunnelMode;
    tVlanRemoteNpWrFsMiVlanHwSetTunnelFilter  sFsMiVlanHwSetTunnelFilter;
    tVlanRemoteNpWrFsMiVlanHwCheckTagAtEgress  sFsMiVlanHwCheckTagAtEgress;
    tVlanRemoteNpWrFsMiVlanHwCheckTagAtIngress  sFsMiVlanHwCheckTagAtIngress;
    tVlanRemoteNpWrFsMiVlanHwCreateFdbId  sFsMiVlanHwCreateFdbId;
    tVlanRemoteNpWrFsMiVlanHwDeleteFdbId  sFsMiVlanHwDeleteFdbId;
    tVlanRemoteNpWrFsMiVlanHwAssociateVlanFdb  sFsMiVlanHwAssociateVlanFdb;
    tVlanRemoteNpWrFsMiVlanHwDisassociateVlanFdb  sFsMiVlanHwDisassociateVlanFdb;
    tVlanRemoteNpWrFsMiVlanHwFlushPortFdbId  sFsMiVlanHwFlushPortFdbId;
    tVlanRemoteNpWrFsMiVlanHwFlushPortFdbList  sFsMiVlanHwFlushPortFdbList;
    tVlanRemoteNpWrFsMiVlanHwFlushPort  sFsMiVlanHwFlushPort;
    tVlanRemoteNpWrFsMiVlanHwFlushFdbId  sFsMiVlanHwFlushFdbId;
    tVlanRemoteNpWrFsMiVlanHwSetShortAgeout  sFsMiVlanHwSetShortAgeout;
    tVlanRemoteNpWrFsMiVlanHwResetShortAgeout  sFsMiVlanHwResetShortAgeout;
    tVlanRemoteNpWrFsMiVlanHwGetVlanInfo  sFsMiVlanHwGetVlanInfo;
    tVlanRemoteNpWrFsMiVlanHwGetMcastEntry  sFsMiVlanHwGetMcastEntry;
    tVlanRemoteNpWrFsMiVlanHwTraffClassMapInit  sFsMiVlanHwTraffClassMapInit;
#ifndef BRIDGE_WANTED
    tVlanRemoteNpWrFsMiBrgSetAgingTime  sFsMiBrgSetAgingTime;
#endif /* BRIDGE_WANTED */
    tVlanRemoteNpWrFsMiVlanHwSetBrgMode  sFsMiVlanHwSetBrgMode;
    tVlanRemoteNpWrFsMiVlanHwSetBaseBridgeMode  sFsMiVlanHwSetBaseBridgeMode;
    tVlanRemoteNpWrFsMiVlanHwAddPortMacVlanEntry  sFsMiVlanHwAddPortMacVlanEntry;
    tVlanRemoteNpWrFsMiVlanHwDeletePortMacVlanEntry  sFsMiVlanHwDeletePortMacVlanEntry;
    tVlanRemoteNpWrFsMiVlanHwAddPortSubnetVlanEntry  sFsMiVlanHwAddPortSubnetVlanEntry;
    tVlanRemoteNpWrFsMiVlanHwDeletePortSubnetVlanEntry  sFsMiVlanHwDeletePortSubnetVlanEntry;
    tVlanRemoteNpWrFsMiVlanHwUpdatePortSubnetVlanEntry  sFsMiVlanHwUpdatePortSubnetVlanEntry;
    tVlanRemoteNpWrFsMiVlanNpHwRunMacAgeing  sFsMiVlanNpHwRunMacAgeing;
    tVlanRemoteNpWrFsMiVlanHwMacLearningLimit  sFsMiVlanHwMacLearningLimit;
    tVlanRemoteNpWrFsMiVlanHwSwitchMacLearningLimit  sFsMiVlanHwSwitchMacLearningLimit;
    tVlanRemoteNpWrFsMiVlanHwMacLearningStatus  sFsMiVlanHwMacLearningStatus;
    tVlanRemoteNpWrFsMiVlanHwSetFidPortLearningStatus  sFsMiVlanHwSetFidPortLearningStatus;
    tVlanRemoteNpWrFsMiVlanHwSetProtocolTunnelStatusOnPort  sFsMiVlanHwSetProtocolTunnelStatusOnPort;
    tVlanRemoteNpWrFsMiVlanHwSetPortProtectedStatus  sFsMiVlanHwSetPortProtectedStatus;
    tVlanRemoteNpWrFsMiVlanHwAddStaticUcastEntryEx  sFsMiVlanHwAddStaticUcastEntryEx;
    tVlanRemoteNpWrFsMiVlanHwGetStaticUcastEntryEx  sFsMiVlanHwGetStaticUcastEntryEx;
    tVlanRemoteNpWrFsVlanHwForwardPktOnPorts  sFsVlanHwForwardPktOnPorts;
    tVlanRemoteNpWrFsMiVlanHwPortMacLearningStatus  sFsMiVlanHwPortMacLearningStatus;
    tVlanRemoteNpWrFsVlanHwGetMacLearningMode  sFsVlanHwGetMacLearningMode;
    tVlanRemoteNpWrFsMiVlanHwSetMcastIndex  sFsMiVlanHwSetMcastIndex;
#ifdef PB_WANTED
    tVlanRemoteNpWrFsMiVlanHwSetProviderBridgePortType  sFsMiVlanHwSetProviderBridgePortType;
    tVlanRemoteNpWrFsMiVlanHwSetPortSVlanTranslationStatus  sFsMiVlanHwSetPortSVlanTranslationStatus;
    tVlanRemoteNpWrFsMiVlanHwAddSVlanTranslationEntry  sFsMiVlanHwAddSVlanTranslationEntry;
    tVlanRemoteNpWrFsMiVlanHwDelSVlanTranslationEntry  sFsMiVlanHwDelSVlanTranslationEntry;
    tVlanRemoteNpWrFsMiVlanHwSetPortEtherTypeSwapStatus  sFsMiVlanHwSetPortEtherTypeSwapStatus;
    tVlanRemoteNpWrFsMiVlanHwAddEtherTypeSwapEntry  sFsMiVlanHwAddEtherTypeSwapEntry;
    tVlanRemoteNpWrFsMiVlanHwDelEtherTypeSwapEntry  sFsMiVlanHwDelEtherTypeSwapEntry;
    tVlanRemoteNpWrFsMiVlanHwAddSVlanMap  sFsMiVlanHwAddSVlanMap;
    tVlanRemoteNpWrFsMiVlanHwDeleteSVlanMap  sFsMiVlanHwDeleteSVlanMap;
    tVlanRemoteNpWrFsMiVlanHwSetPortSVlanClassifyMethod  sFsMiVlanHwSetPortSVlanClassifyMethod;
    tVlanRemoteNpWrFsMiVlanHwPortMacLearningLimit  sFsMiVlanHwPortMacLearningLimit;
    tVlanRemoteNpWrFsMiVlanHwMulticastMacTableLimit  sFsMiVlanHwMulticastMacTableLimit;
    tVlanRemoteNpWrFsMiVlanHwSetPortCustomerVlan  sFsMiVlanHwSetPortCustomerVlan;
    tVlanRemoteNpWrFsMiVlanHwResetPortCustomerVlan  sFsMiVlanHwResetPortCustomerVlan;
    tVlanRemoteNpWrFsMiVlanHwCreateProviderEdgePort  sFsMiVlanHwCreateProviderEdgePort;
    tVlanRemoteNpWrFsMiVlanHwDelProviderEdgePort  sFsMiVlanHwDelProviderEdgePort;
    tVlanRemoteNpWrFsMiVlanHwSetPepPvid  sFsMiVlanHwSetPepPvid;
    tVlanRemoteNpWrFsMiVlanHwSetPepAccFrameType  sFsMiVlanHwSetPepAccFrameType;
    tVlanRemoteNpWrFsMiVlanHwSetPepDefUserPriority  sFsMiVlanHwSetPepDefUserPriority;
    tVlanRemoteNpWrFsMiVlanHwSetPepIngFiltering  sFsMiVlanHwSetPepIngFiltering;
    tVlanRemoteNpWrFsMiVlanHwSetCvidUntagPep  sFsMiVlanHwSetCvidUntagPep;
    tVlanRemoteNpWrFsMiVlanHwSetCvidUntagCep  sFsMiVlanHwSetCvidUntagCep;
    tVlanRemoteNpWrFsMiVlanHwSetPcpEncodTbl  sFsMiVlanHwSetPcpEncodTbl;
    tVlanRemoteNpWrFsMiVlanHwSetPcpDecodTbl  sFsMiVlanHwSetPcpDecodTbl;
    tVlanRemoteNpWrFsMiVlanHwSetPortUseDei  sFsMiVlanHwSetPortUseDei;
    tVlanRemoteNpWrFsMiVlanHwSetPortReqDropEncoding  sFsMiVlanHwSetPortReqDropEncoding;
    tVlanRemoteNpWrFsMiVlanHwSetPortPcpSelection  sFsMiVlanHwSetPortPcpSelection;
    tVlanRemoteNpWrFsMiVlanHwSetServicePriRegenEntry  sFsMiVlanHwSetServicePriRegenEntry;
    tVlanRemoteNpWrFsMiVlanHwSetTunnelMacAddress  sFsMiVlanHwSetTunnelMacAddress;
    tVlanRemoteNpWrFsMiVlanHwGetNextSVlanMap  sFsMiVlanHwGetNextSVlanMap;
    tVlanRemoteNpWrFsMiVlanHwGetNextSVlanTranslationEntry  sFsMiVlanHwGetNextSVlanTranslationEntry;
    tVlanRemoteNpWrFsMiVlanHwGetCVlanStat  sFsMiVlanHwGetCVlanStat;
    tVlanRemoteNpWrFsMiVlanHwSetCVlanStat  sFsMiVlanHwSetCVlanStat;
    tVlanRemoteNpWrFsMiVlanHwClearCVlanStat  sFsMiVlanHwClearCVlanStat;
#endif /* PB_WANTED */
#ifdef MBSM_WANTED
    tVlanRemoteNpWrFsMiVlanMbsmSyncFDBInfo  sFsMiVlanMbsmSyncFDBInfo;
    tVlanRemoteNpWrFsMiVlanMbsmHwEvbConfigSChIface sFsMiVlanMbsmHwEvbConfigSChIface;
    tVlanRemoteNpWrFsMiVlanMbsmHwSetBridgePortType sFsMiVlanMbsmHwSetBridgePortType;
    tVlanRemoteNpWrFsMiVlanMbsmHwPortPktReflectStatus  sFsMiVlanMbsmHwPortPktReflectStatus;
#endif /* MBSM_WANTED */
    tVlanRemoteNpWrFsMiVlanHwSetPortIngressEtherType  sFsMiVlanHwSetPortIngressEtherType;
    tVlanRemoteNpWrFsMiVlanHwSetPortEgressEtherType  sFsMiVlanHwSetPortEgressEtherType;
    tVlanRemoteNpWrFsMiVlanHwSetVlanLoopbackStatus  sFsMiVlanHwSetVlanLoopbackStatus;
    tVlanRemoteNpWrFsMiVlanHwSetPortProperty sFsMiVlanHwSetPortProperty;
    tVlanRemoteNpWrFsMiVlanHwEvbConfigSChIface sFsMiVlanHwEvbConfigSChIface;
    tVlanRemoteNpWrFsMiVlanHwSetBridgePortType sFsMiVlanHwSetBridgePortType;
    tVlanRemoteNpWrFsNpHwGetPortFromFdb  sFsNpHwGetPortFromFdb;
    tVlanRemoteNpWrFsMiVlanHwPortPktReflectStatus  sFsMiVlanHwPortPktReflectStatus;
    }unOpCode;

#define  VlanRemoteNpFsMiVlanHwInit  unOpCode.sFsMiVlanHwInit;
#define  VlanRemoteNpFsMiVlanHwDeInit  unOpCode.sFsMiVlanHwDeInit;
#define  VlanRemoteNpFsMiVlanHwSetAllGroupsPorts  unOpCode.sFsMiVlanHwSetAllGroupsPorts;
#define  VlanRemoteNpFsMiVlanHwResetAllGroupsPorts  unOpCode.sFsMiVlanHwResetAllGroupsPorts;
#define  VlanRemoteNpFsMiVlanHwResetUnRegGroupsPorts  unOpCode.sFsMiVlanHwResetUnRegGroupsPorts;
#define  VlanRemoteNpFsMiVlanHwSetUnRegGroupsPorts  unOpCode.sFsMiVlanHwSetUnRegGroupsPorts;
#define  VlanRemoteNpFsMiVlanHwAddStaticUcastEntry  unOpCode.sFsMiVlanHwAddStaticUcastEntry;
#define  VlanRemoteNpFsMiVlanHwDelStaticUcastEntry  unOpCode.sFsMiVlanHwDelStaticUcastEntry;
#define  VlanRemoteNpFsMiVlanHwGetFdbEntry  unOpCode.sFsMiVlanHwGetFdbEntry;
#ifndef SW_LEARNING
#define  VlanRemoteNpFsMiVlanHwGetFdbCount  unOpCode.sFsMiVlanHwGetFdbCount;
#define  VlanRemoteNpFsMiVlanHwGetFirstTpFdbEntry  unOpCode.sFsMiVlanHwGetFirstTpFdbEntry;
#define  VlanRemoteNpFsMiVlanHwGetNextTpFdbEntry  unOpCode.sFsMiVlanHwGetNextTpFdbEntry;
#endif /* SW_LEARNING */
#define  VlanRemoteNpFsMiVlanHwAddMcastEntry  unOpCode.sFsMiVlanHwAddMcastEntry;
#define  VlanRemoteNpFsMiVlanHwAddStMcastEntry  unOpCode.sFsMiVlanHwAddStMcastEntry;
#define  VlanRemoteNpFsMiVlanHwSetMcastPort  unOpCode.sFsMiVlanHwSetMcastPort;
#define  VlanRemoteNpFsMiVlanHwResetMcastPort  unOpCode.sFsMiVlanHwResetMcastPort;
#define  VlanRemoteNpFsMiVlanHwDelMcastEntry  unOpCode.sFsMiVlanHwDelMcastEntry;
#define  VlanRemoteNpFsMiVlanHwDelStMcastEntry  unOpCode.sFsMiVlanHwDelStMcastEntry;
#define  VlanRemoteNpFsMiVlanHwAddVlanEntry  unOpCode.sFsMiVlanHwAddVlanEntry;
#define  VlanRemoteNpFsMiVlanHwDelVlanEntry  unOpCode.sFsMiVlanHwDelVlanEntry;
#define  VlanRemoteNpFsMiVlanHwSetVlanMemberPort  unOpCode.sFsMiVlanHwSetVlanMemberPort;
#define  VlanRemoteNpFsMiVlanHwResetVlanMemberPort  unOpCode.sFsMiVlanHwResetVlanMemberPort;
#define  VlanRemoteNpFsMiVlanHwSetPortPvid  unOpCode.sFsMiVlanHwSetPortPvid;
#define  VlanRemoteNpFsMiVlanHwSetDefaultVlanId  unOpCode.sFsMiVlanHwSetDefaultVlanId;
#ifdef L2RED_WANTED
#define  VlanRemoteNpFsMiVlanHwSyncDefaultVlanId  unOpCode.sFsMiVlanHwSyncDefaultVlanId;
#define  VlanRemoteNpFsMiVlanRedHwUpdateDBForDefaultVlanId  unOpCode.sFsMiVlanRedHwUpdateDBForDefaultVlanId;
#define  VlanRemoteNpFsMiVlanHwScanProtocolVlanTbl  unOpCode.sFsMiVlanHwScanProtocolVlanTbl;
#define  VlanRemoteNpFsMiVlanHwGetVlanProtocolMap  unOpCode.sFsMiVlanHwGetVlanProtocolMap;
#define  VlanRemoteNpFsMiVlanHwScanMulticastTbl  unOpCode.sFsMiVlanHwScanMulticastTbl;
#define  VlanRemoteNpFsMiVlanHwGetStMcastEntry  unOpCode.sFsMiVlanHwGetStMcastEntry;
#define  VlanRemoteNpFsMiVlanHwScanUnicastTbl  unOpCode.sFsMiVlanHwScanUnicastTbl;
#define  VlanRemoteNpFsMiVlanHwGetStaticUcastEntry  unOpCode.sFsMiVlanHwGetStaticUcastEntry;
#define  VlanRemoteNpFsMiVlanHwGetSyncedTnlProtocolMacAddr  unOpCode.sFsMiVlanHwGetSyncedTnlProtocolMacAddr;
#endif /* L2RED_WANTED */
#define  VlanRemoteNpFsMiVlanHwSetPortAccFrameType  unOpCode.sFsMiVlanHwSetPortAccFrameType;
#define  VlanRemoteNpFsMiVlanHwSetPortIngFiltering  unOpCode.sFsMiVlanHwSetPortIngFiltering;
#define  VlanRemoteNpFsMiVlanHwVlanEnable  unOpCode.sFsMiVlanHwVlanEnable;
#define  VlanRemoteNpFsMiVlanHwVlanDisable  unOpCode.sFsMiVlanHwVlanDisable;
#define  VlanRemoteNpFsMiVlanHwSetVlanLearningType  unOpCode.sFsMiVlanHwSetVlanLearningType;
#define  VlanRemoteNpFsMiVlanHwSetMacBasedStatusOnPort  unOpCode.sFsMiVlanHwSetMacBasedStatusOnPort;
#define  VlanRemoteNpFsMiVlanHwSetSubnetBasedStatusOnPort  unOpCode.sFsMiVlanHwSetSubnetBasedStatusOnPort;
#define  VlanRemoteNpFsMiVlanHwEnableProtoVlanOnPort  unOpCode.sFsMiVlanHwEnableProtoVlanOnPort;
#define  VlanRemoteNpFsMiVlanHwSetDefUserPriority  unOpCode.sFsMiVlanHwSetDefUserPriority;
#define  VlanRemoteNpFsMiVlanHwSetPortNumTrafClasses  unOpCode.sFsMiVlanHwSetPortNumTrafClasses;
#define  VlanRemoteNpFsMiVlanHwSetRegenUserPriority  unOpCode.sFsMiVlanHwSetRegenUserPriority;
#define  VlanRemoteNpFsMiVlanHwSetTraffClassMap  unOpCode.sFsMiVlanHwSetTraffClassMap;
#define  VlanRemoteNpFsMiVlanHwGmrpEnable  unOpCode.sFsMiVlanHwGmrpEnable;
#define  VlanRemoteNpFsMiVlanHwGmrpDisable  unOpCode.sFsMiVlanHwGmrpDisable;
#define  VlanRemoteNpFsMiVlanHwGvrpEnable  unOpCode.sFsMiVlanHwGvrpEnable;
#define  VlanRemoteNpFsMiVlanHwGvrpDisable  unOpCode.sFsMiVlanHwGvrpDisable;
#define  VlanRemoteNpFsMiNpDeleteAllFdbEntries  unOpCode.sFsMiNpDeleteAllFdbEntries;
#define  VlanRemoteNpFsMiVlanHwAddVlanProtocolMap  unOpCode.sFsMiVlanHwAddVlanProtocolMap;
#define  VlanRemoteNpFsMiVlanHwDelVlanProtocolMap  unOpCode.sFsMiVlanHwDelVlanProtocolMap;
#define  VlanRemoteNpFsMiVlanHwGetPortStats  unOpCode.sFsMiVlanHwGetPortStats;
#define  VlanRemoteNpFsMiVlanHwGetPortStats64  unOpCode.sFsMiVlanHwGetPortStats64;
#define  VlanRemoteNpFsMiVlanHwGetVlanStats  unOpCode.sFsMiVlanHwGetVlanStats;
#define  VlanRemoteNpFsMiVlanHwResetVlanStats  unOpCode.sFsMiVlanHwResetVlanStats;
#define  VlanRemoteNpFsMiVlanHwSetPortTunnelMode  unOpCode.sFsMiVlanHwSetPortTunnelMode;
#define  VlanRemoteNpFsMiVlanHwSetTunnelFilter  unOpCode.sFsMiVlanHwSetTunnelFilter;
#define  VlanRemoteNpFsMiVlanHwCheckTagAtEgress  unOpCode.sFsMiVlanHwCheckTagAtEgress;
#define  VlanRemoteNpFsMiVlanHwCheckTagAtIngress  unOpCode.sFsMiVlanHwCheckTagAtIngress;
#define  VlanRemoteNpFsMiVlanHwCreateFdbId  unOpCode.sFsMiVlanHwCreateFdbId;
#define  VlanRemoteNpFsMiVlanHwDeleteFdbId  unOpCode.sFsMiVlanHwDeleteFdbId;
#define  VlanRemoteNpFsMiVlanHwAssociateVlanFdb  unOpCode.sFsMiVlanHwAssociateVlanFdb;
#define  VlanRemoteNpFsMiVlanHwDisassociateVlanFdb  unOpCode.sFsMiVlanHwDisassociateVlanFdb;
#define  VlanRemoteNpFsMiVlanHwFlushPortFdbId  unOpCode.sFsMiVlanHwFlushPortFdbId;
#define  VlanRemoteNpFsMiVlanHwFlushPortFdbList  unOpCode.sFsMiVlanHwFlushPortFdbList;
#define  VlanRemoteNpFsMiVlanHwFlushPort  unOpCode.sFsMiVlanHwFlushPort;
#define  VlanRemoteNpFsMiVlanHwFlushFdbId  unOpCode.sFsMiVlanHwFlushFdbId;
#define  VlanRemoteNpFsMiVlanHwSetShortAgeout  unOpCode.sFsMiVlanHwSetShortAgeout;
#define  VlanRemoteNpFsMiVlanHwResetShortAgeout  unOpCode.sFsMiVlanHwResetShortAgeout;
#define  VlanRemoteNpFsMiVlanHwGetVlanInfo  unOpCode.sFsMiVlanHwGetVlanInfo;
#define  VlanRemoteNpFsMiVlanHwGetMcastEntry  unOpCode.sFsMiVlanHwGetMcastEntry;
#define  VlanRemoteNpFsMiVlanHwTraffClassMapInit  unOpCode.sFsMiVlanHwTraffClassMapInit;
#ifndef BRIDGE_WANTED
#define  VlanRemoteNpFsMiBrgSetAgingTime  unOpCode.sFsMiBrgSetAgingTime;
#endif /* BRIDGE_WANTED */
#define  VlanRemoteNpFsMiVlanHwSetBrgMode  unOpCode.sFsMiVlanHwSetBrgMode;
#define  VlanRemoteNpFsMiVlanHwSetBaseBridgeMode  unOpCode.sFsMiVlanHwSetBaseBridgeMode;
#define  VlanRemoteNpFsMiVlanHwAddPortMacVlanEntry  unOpCode.sFsMiVlanHwAddPortMacVlanEntry;
#define  VlanRemoteNpFsMiVlanHwDeletePortMacVlanEntry  unOpCode.sFsMiVlanHwDeletePortMacVlanEntry;
#define  VlanRemoteNpFsMiVlanHwAddPortSubnetVlanEntry  unOpCode.sFsMiVlanHwAddPortSubnetVlanEntry;
#define  VlanRemoteNpFsMiVlanHwDeletePortSubnetVlanEntry  unOpCode.sFsMiVlanHwDeletePortSubnetVlanEntry;
#define  VlanRemoteNpFsMiVlanHwUpdatePortSubnetVlanEntry  unOpCode.sFsMiVlanHwUpdatePortSubnetVlanEntry;
#define  VlanRemoteNpFsMiVlanNpHwRunMacAgeing  unOpCode.sFsMiVlanNpHwRunMacAgeing;
#define  VlanRemoteNpFsMiVlanHwMacLearningLimit  unOpCode.sFsMiVlanHwMacLearningLimit;
#define  VlanRemoteNpFsMiVlanHwSwitchMacLearningLimit  unOpCode.sFsMiVlanHwSwitchMacLearningLimit;
#define  VlanRemoteNpFsMiVlanHwMacLearningStatus  unOpCode.sFsMiVlanHwMacLearningStatus;
#define  VlanRemoteNpFsMiVlanHwSetFidPortLearningStatus  unOpCode.sFsMiVlanHwSetFidPortLearningStatus;
#define  VlanRemoteNpFsMiVlanHwSetProtocolTunnelStatusOnPort  unOpCode.sFsMiVlanHwSetProtocolTunnelStatusOnPort;
#define  VlanRemoteNpFsMiVlanHwSetPortProtectedStatus  unOpCode.sFsMiVlanHwSetPortProtectedStatus;
#define  VlanRemoteNpFsMiVlanHwAddStaticUcastEntryEx  unOpCode.sFsMiVlanHwAddStaticUcastEntryEx;
#define  VlanRemoteNpFsMiVlanHwGetStaticUcastEntryEx  unOpCode.sFsMiVlanHwGetStaticUcastEntryEx;
#define  VlanRemoteNpFsVlanHwForwardPktOnPorts  unOpCode.sFsVlanHwForwardPktOnPorts;
#define  VlanRemoteNpFsMiVlanHwPortMacLearningStatus  unOpCode.sFsMiVlanHwPortMacLearningStatus;
#define  VlanRemoteNpFsVlanHwGetMacLearningMode  unOpCode.sFsVlanHwGetMacLearningMode;
#define  VlanRemoteNpFsMiVlanHwSetMcastIndex  unOpCode.sFsMiVlanHwSetMcastIndex;
#ifdef PB_WANTED
#define  VlanRemoteNpFsMiVlanHwSetProviderBridgePortType  unOpCode.sFsMiVlanHwSetProviderBridgePortType;
#define  VlanRemoteNpFsMiVlanHwSetPortSVlanTranslationStatus  unOpCode.sFsMiVlanHwSetPortSVlanTranslationStatus;
#define  VlanRemoteNpFsMiVlanHwAddSVlanTranslationEntry  unOpCode.sFsMiVlanHwAddSVlanTranslationEntry;
#define  VlanRemoteNpFsMiVlanHwDelSVlanTranslationEntry  unOpCode.sFsMiVlanHwDelSVlanTranslationEntry;
#define  VlanRemoteNpFsMiVlanHwSetPortEtherTypeSwapStatus  unOpCode.sFsMiVlanHwSetPortEtherTypeSwapStatus;
#define  VlanRemoteNpFsMiVlanHwAddEtherTypeSwapEntry  unOpCode.sFsMiVlanHwAddEtherTypeSwapEntry;
#define  VlanRemoteNpFsMiVlanHwDelEtherTypeSwapEntry  unOpCode.sFsMiVlanHwDelEtherTypeSwapEntry;
#define  VlanRemoteNpFsMiVlanHwAddSVlanMap  unOpCode.sFsMiVlanHwAddSVlanMap;
#define  VlanRemoteNpFsMiVlanHwDeleteSVlanMap  unOpCode.sFsMiVlanHwDeleteSVlanMap;
#define  VlanRemoteNpFsMiVlanHwSetPortSVlanClassifyMethod  unOpCode.sFsMiVlanHwSetPortSVlanClassifyMethod;
#define  VlanRemoteNpFsMiVlanHwPortMacLearningLimit  unOpCode.sFsMiVlanHwPortMacLearningLimit;
#define  VlanRemoteNpFsMiVlanHwMulticastMacTableLimit  unOpCode.sFsMiVlanHwMulticastMacTableLimit;
#define  VlanRemoteNpFsMiVlanHwSetPortCustomerVlan  unOpCode.sFsMiVlanHwSetPortCustomerVlan;
#define  VlanRemoteNpFsMiVlanHwResetPortCustomerVlan  unOpCode.sFsMiVlanHwResetPortCustomerVlan;
#define  VlanRemoteNpFsMiVlanHwCreateProviderEdgePort  unOpCode.sFsMiVlanHwCreateProviderEdgePort;
#define  VlanRemoteNpFsMiVlanHwDelProviderEdgePort  unOpCode.sFsMiVlanHwDelProviderEdgePort;
#define  VlanRemoteNpFsMiVlanHwSetPepPvid  unOpCode.sFsMiVlanHwSetPepPvid;
#define  VlanRemoteNpFsMiVlanHwSetPepAccFrameType  unOpCode.sFsMiVlanHwSetPepAccFrameType;
#define  VlanRemoteNpFsMiVlanHwSetPepDefUserPriority  unOpCode.sFsMiVlanHwSetPepDefUserPriority;
#define  VlanRemoteNpFsMiVlanHwSetPepIngFiltering  unOpCode.sFsMiVlanHwSetPepIngFiltering;
#define  VlanRemoteNpFsMiVlanHwSetCvidUntagPep  unOpCode.sFsMiVlanHwSetCvidUntagPep;
#define  VlanRemoteNpFsMiVlanHwSetCvidUntagCep  unOpCode.sFsMiVlanHwSetCvidUntagCep;
#define  VlanRemoteNpFsMiVlanHwSetPcpEncodTbl  unOpCode.sFsMiVlanHwSetPcpEncodTbl;
#define  VlanRemoteNpFsMiVlanHwSetPcpDecodTbl  unOpCode.sFsMiVlanHwSetPcpDecodTbl;
#define  VlanRemoteNpFsMiVlanHwSetPortUseDei  unOpCode.sFsMiVlanHwSetPortUseDei;
#define  VlanRemoteNpFsMiVlanHwSetPortReqDropEncoding  unOpCode.sFsMiVlanHwSetPortReqDropEncoding;
#define  VlanRemoteNpFsMiVlanHwSetPortPcpSelection  unOpCode.sFsMiVlanHwSetPortPcpSelection;
#define  VlanRemoteNpFsMiVlanHwSetServicePriRegenEntry  unOpCode.sFsMiVlanHwSetServicePriRegenEntry;
#define  VlanRemoteNpFsMiVlanHwSetTunnelMacAddress  unOpCode.sFsMiVlanHwSetTunnelMacAddress;
#define  VlanRemoteNpFsMiVlanHwGetNextSVlanMap  unOpCode.sFsMiVlanHwGetNextSVlanMap;
#define  VlanRemoteNpFsMiVlanHwGetNextSVlanTranslationEntry  unOpCode.sFsMiVlanHwGetNextSVlanTranslationEntry;
#define  VlanRemoteNpFsMiVlanHwGetCVlanStat  unOpCode.sFsMiVlanHwGetCVlanStat;
#define  VlanRemoteNpFsMiVlanHwSetCVlanStat  unOpCode.sFsMiVlanHwSetCVlanStat;
#define  VlanRemoteNpFsMiVlanHwClearCVlanStat  unOpCode.sFsMiVlanHwClearCVlanStat;
#endif /* PB_WANTED */
#ifdef MBSM_WANTED
#define  VlanRemoteNpFsMiVlanMbsmSyncFDBInfo  unOpCode.sFsMiVlanMbsmSyncFDBInfo;
#define  VlanRemoteNpFsMiVlanMbsmHwEvbConfigSChIface  unOpCode.sFsMiVlanMbsmHwEvbConfigSChIface;
#define  VlanRemoteNpFsMiVlanMbsmHwSetBridgePortType  unOpCode.sFsMiVlanMbsmHwSetBridgePortType;
#define  VlanRemoteNpFsMiVlanMbsmHwPortPktReflectStatus  unOpCode.sFsMiVlanMbsmHwPortPktReflectStatus;
#endif /* MBSM_WANTED */
#define  VlanRemoteNpFsMiVlanHwSetPortIngressEtherType  unOpCode.sFsMiVlanHwSetPortIngressEtherType;
#define  VlanRemoteNpFsMiVlanHwSetPortEgressEtherType  unOpCode.sFsMiVlanHwSetPortEgressEtherType;
#define  VlanRemoteNpFsMiVlanHwSetVlanLoopbackStatus  unOpCode.sFsMiVlanHwSetVlanLoopbackStatus;
#define  VlanRemoteNpFsMiVlanHwSetPortProperty  unOpCode.sFsMiVlanHwSetPortProperty;
#define  VlanRemoteNpFsMiVlanHwEvbConfigSChIface  unOpCode.sFsMiVlanHwEvbConfigSChIface;
#define  VlanRemoteNpFsMiVlanHwSetBridgePortType  unOpCode.sFsMiVlanHwSetBridgePortType;
#define  VlanRemoteNpFsNpHwGetPortFromFdb unOpCode.sFsNpHwGetPortFromFdb; 
#define  VlanRemoteNpFsMiVlanHwPortPktReflectStatus  unOpCode.sFsMiVlanHwPortPktReflectStatus;

} tVlanRemoteNpModInfo;

#endif /* VLAN_WANTED */

#ifdef CFA_WANTED
typedef struct _RemoteHwCustIfTLV {
    UINT4 u4Type;
    UINT4 u4Length;
    UINT1 u1Value;
    UINT1 au1Pad[3];
} tRemoteHwCustIfTLV;

typedef union _RemoteHwCustIfParamVal {
    tRemoteHwCustIfTLV TLV;
    tHwCustIfOpqAttrs OpaqueAttrbs;
} tRemoteHwCustIfParamVal;

typedef struct {
    UINT4  u4IfIndex;
    UINT1  u1PortLinkStatus;   
    UINT1  au1Pad[3];
} tCfaRemoteNpWrCfaNpGetLinkStatus;

typedef struct {
    UINT4  u4Port;
} tCfaRemoteNpWrFsCfaHwClearStats;

typedef struct {
    UINT4    u4IfIndex;
    UINT4   u4PortFlowControl;
} tCfaRemoteNpWrFsCfaHwGetIfFlowControl;

typedef struct {
    UINT4    u4IfIndex;
    UINT4   u4MtuSize;
} tCfaRemoteNpWrFsCfaHwGetMtu;

typedef struct {
    UINT4     u4IfIndex;
    tMacAddr  PortMac;
    UINT1     au1Pad[2];
} tCfaRemoteNpWrFsCfaHwSetMacAddr;

typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4MtuSize;
} tCfaRemoteNpWrFsCfaHwSetMtu;


typedef struct {
    tL3MtuInfo L3Mtu;
} tCfaRemoteNpWrFsCfaHwL3SetMtu;

typedef struct {
    tIfWanInfo   CfaWanInfo;
} tCfaRemoteNpWrFsCfaHwSetWanTye;

typedef struct {
    UINT4   u4IfIndex;
    INT4    element;
    INT4    i4Code;
} tCfaRemoteNpWrFsEtherHwGetCntrl;

typedef struct {
    UINT4    u4IfIndex;
    INT4     i4dot3CollCount;
    UINT4    element;
} tCfaRemoteNpWrFsEtherHwGetCollFreq;

typedef struct {
    UINT4    u4IfIndex;
    UINT4   element;
    INT4     i4Code;
} tCfaRemoteNpWrFsEtherHwGetPauseFrames;

typedef struct {
    UINT4   u4IfIndex;
    INT4   element;
    INT4    i4Code;
} tCfaRemoteNpWrFsEtherHwGetPauseMode;

typedef struct {
    UINT4       u4IfIndex;
    tEthStats   EthStats;
} tCfaRemoteNpWrFsEtherHwGetStats;

typedef struct {
    UINT4   u4IfIndex;
    INT4    element;
} tCfaRemoteNpWrFsEtherHwSetPauseAdminMode;

typedef struct {
    UINT4    u4IfIndex;
    UINT1    u1EtherType;
    UINT1    au1Pad[3];
} tCfaRemoteNpWrFsHwGetEthernetType;

typedef struct {
    UINT4    u4IfIndex;
    UINT4    u4Value;
    INT1     i1StatType;
    UINT1    au1Pad[3];
} tCfaRemoteNpWrFsHwGetStat;

typedef struct {
    UINT4                   u4IfIndex;
    tSNMP_COUNTER64_TYPE    Value;
    INT1                    i1StatType;
    UINT1                   au1Pad[3];
} tCfaRemoteNpWrFsHwGetStat64;

typedef struct {
    UINT4               u4IfIndex;
    tHwCustIfParamType  HwCustParamType;
    tRemoteHwCustIfParamVal   CustIfParamVal;
    tNpEntryAction      EntryAction;
} tCfaRemoteNpWrFsHwSetCustIfParams;

typedef struct {
    UINT4  u4IfIndex;
    UINT1  u1AdminEnable;
    UINT1  au1Pad[3];
} tCfaRemoteNpWrFsHwUpdateAdminStatusChange;

typedef struct {
    UINT4   u4IfIndex;
    INT4    i4IfMauId;
    INT4    element;
} tCfaRemoteNpWrFsMauHwGetAutoNegAdminConfig;

typedef struct {
    UINT4   u4IfIndex;
    INT4    i4IfMauId;
    INT4    element;
} tCfaRemoteNpWrFsMauHwGetAutoNegAdminStatus;

typedef struct {
    UINT4   u4IfIndex;
    INT4    i4IfMauId;
    INT4    element;
} tCfaRemoteNpWrFsMauHwGetAutoNegCapAdvtBits;

typedef struct {
    UINT4   u4IfIndex;
    INT4    i4IfMauId;
    INT4    element;
} tCfaRemoteNpWrFsMauHwGetAutoNegCapBits;

typedef struct {
    UINT4   u4IfIndex;
    INT4    i4IfMauId;
    INT4    element;
} tCfaRemoteNpWrFsMauHwGetAutoNegCapRcvdBits;

typedef struct {
    UINT4   u4IfIndex;
    INT4    i4IfMauId;
    INT4    element;
} tCfaRemoteNpWrFsMauHwGetAutoNegRemFltAdvt;

typedef struct {
    UINT4   u4IfIndex;
    INT4    i4IfMauId;
    INT4    element;
} tCfaRemoteNpWrFsMauHwGetAutoNegRemFltRcvd;

typedef struct {
    UINT4   u4IfIndex;
    INT4    i4IfMauId;
    INT4    element;
} tCfaRemoteNpWrFsMauHwGetAutoNegRemoteSignaling;

typedef struct {
    UINT4   u4IfIndex;
    INT4    i4IfMauId;
    INT4    element;
} tCfaRemoteNpWrFsMauHwGetAutoNegRestart;

typedef struct {
    UINT4   u4IfIndex;
    INT4    i4IfMauId;
    INT4    element;
} tCfaRemoteNpWrFsMauHwGetAutoNegSupported;

typedef struct {
    UINT4    u4IfIndex;
    INT4     i4IfMauId;
    UINT4    element;
} tCfaRemoteNpWrFsMauHwGetFalseCarriers;

typedef struct {
    UINT4   u4IfIndex;
    INT4    i4IfMauId;
    INT4    element;
} tCfaRemoteNpWrFsMauHwGetJabberState;

typedef struct {
    UINT4    u4IfIndex;
    INT4     i4IfMauId;
    UINT4    element;
} tCfaRemoteNpWrFsMauHwGetJabberingStateEnters;

typedef struct {
    UINT4   u4IfIndex;
    INT4    i4IfMauId;
    INT4    i4ifJackIndex;
    INT4    element;
} tCfaRemoteNpWrFsMauHwGetJackType;

typedef struct {
    UINT4    u4IfIndex;
    INT4     i4IfMauId;
    UINT4    u4Val;
} tCfaRemoteNpWrFsMauHwGetMauType;

typedef struct {
    UINT4    u4IfIndex;
    INT4     i4IfMauId;
    UINT4    element;
} tCfaRemoteNpWrFsMauHwGetMediaAvailStateExits;

typedef struct {
    UINT4   u4IfIndex;
    INT4    i4IfMauId;
    INT4    element;
} tCfaRemoteNpWrFsMauHwGetMediaAvailable;

typedef struct {
    UINT4   u4IfIndex;
    INT4    i4IfMauId;
    INT4    element;
} tCfaRemoteNpWrFsMauHwGetTypeListBits;

typedef struct {
    UINT4   u4IfIndex;
    INT4    i4IfMauId;
    INT4    element;
} tCfaRemoteNpWrFsMauHwSetAutoNegAdminStatus;

typedef struct {
    UINT4   u4IfIndex;
    INT4    i4IfMauId;
    INT4    element;
} tCfaRemoteNpWrFsMauHwSetAutoNegCapAdvtBits;

typedef struct {
    UINT4   u4IfIndex;
    INT4    i4IfMauId;
    INT4    element;
} tCfaRemoteNpWrFsMauHwSetAutoNegRemFltAdvt;

typedef struct {
    UINT4   u4IfIndex;
    INT4    i4IfMauId;
    INT4    element;
} tCfaRemoteNpWrFsMauHwSetAutoNegRestart;

typedef struct {
    UINT4   u4IfIndex;
    INT4    i4IfMauId;
    INT4    element;
} tCfaRemoteNpWrFsMauHwSetMauStatus;

typedef struct {
    UINT4   u4IfIndex;
    INT4    i4IfMauId;
    INT4    element;
} tCfaRemoteNpWrFsMauHwGetStatus;

typedef struct {
    UINT4  u4IfIndex;
    INT4   i4IfMauId;
    UINT4  u4Val;
} tCfaRemoteNpWrFsMauHwSetDefaultType;

typedef struct {
    UINT4    u4IfIndex;
    INT4     i4IfMauId;
    UINT4    u4Val;
} tCfaRemoteNpWrFsMauHwGetDefaultType;

typedef struct {
    UINT4  u4MaxFrontPanelPorts;
} tCfaRemoteNpWrNpCfaFrontPanelPorts;

typedef struct {
    tVlanIfaceVlanId  VlanId;
    UINT1            u1MacAddr[MAC_ADDR_LEN];
    UINT2            u2Port;
} tCfaRemoteNpWrFsCfaHwMacLookup;

typedef struct {
    UINT2  u2IfIndex;
    UINT1  au1Pad[2];
} tCfaRemoteNpWrCfaNpGetPhyAndLinkStatus;

typedef struct {
    tHwPortInfo  HwPortInfo;
} tCfaRemoteNpWrCfaNpSetHwPortInfo;

typedef struct {
    tHwPortInfo   HwPortInfo;
} tCfaRemoteNpWrCfaNpGetHwPortInfo;

typedef struct {
    UINT4  u4FilterId;
} tCfaRemoteNpWrFsCfaHwDeletePktFilter;

typedef struct {
    tHwCfaFilterInfo   FilterInfo;
    UINT4              u4FilterId;
} tCfaRemoteNpWrFsCfaHwCreatePktFilter;

typedef struct {
    UINT1  u1Status;
    UINT1  au1Pad[3];
} tCfaRemoteNpWrFsNpCfaSetDlfStatus;

typedef struct {
    UINT4  u4StackingModel;
} tCfaRemoteNpWrCfaNpSetStackingModel;

typedef struct {
    tHwIdInfo   HwIdInfo;
} tCfaRemoteNpWrCfaNpGetHwInfo;

typedef struct {
    tHwIdInfo   HwIdInfo;
} tCfaRemoteNpWrCfaNpRemoteSetHwInfo;

typedef struct {
    UINT4    u4IfIndex;
    UINT4    u4Value;
    INT1     i1StatType;
    UINT1    au1pad[3];
} tCfaRemoteNpWrFsHwGetVlanIntfStats;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IpNet;
    UINT4  u4IpMask;
} tCfaRemoteNpWrFsCfaHwRemoveIpNetRcvdDlfInHash;

#ifdef IP6_WANTED
typedef struct {
    tIp6Addr    Ip6Addr;
    UINT4       u4Prefix;
    UINT4       u4ContextId;
} tCfaRemoteNpWrFsCfaHwRemoveIp6NetRcvdDlfInHash;
#endif

typedef struct CfaRemoteNpModInfo {
union {
    tCfaRemoteNpWrCfaNpGetLinkStatus  sCfaNpGetLinkStatus;
    tCfaRemoteNpWrFsCfaHwClearStats  sFsCfaHwClearStats;
    tCfaRemoteNpWrFsCfaHwGetIfFlowControl  sFsCfaHwGetIfFlowControl;
    tCfaRemoteNpWrFsCfaHwGetMtu  sFsCfaHwGetMtu;
    tCfaRemoteNpWrFsCfaHwSetMacAddr  sFsCfaHwSetMacAddr;
    tCfaRemoteNpWrFsCfaHwSetMtu  sFsCfaHwSetMtu;
    tCfaRemoteNpWrFsCfaHwSetWanTye  sFsCfaHwSetWanTye;
    tCfaRemoteNpWrFsEtherHwGetCntrl  sFsEtherHwGetCntrl;
    tCfaRemoteNpWrFsEtherHwGetCollFreq  sFsEtherHwGetCollFreq;
    tCfaRemoteNpWrFsEtherHwGetPauseFrames  sFsEtherHwGetPauseFrames;
    tCfaRemoteNpWrFsEtherHwGetPauseMode  sFsEtherHwGetPauseMode;
    tCfaRemoteNpWrFsEtherHwGetStats  sFsEtherHwGetStats;
    tCfaRemoteNpWrFsEtherHwSetPauseAdminMode  sFsEtherHwSetPauseAdminMode;
    tCfaRemoteNpWrFsHwGetEthernetType  sFsHwGetEthernetType;
    tCfaRemoteNpWrFsHwGetStat  sFsHwGetStat;
    tCfaRemoteNpWrFsHwGetStat64  sFsHwGetStat64;
    tCfaRemoteNpWrFsHwSetCustIfParams  sFsHwSetCustIfParams;
    tCfaRemoteNpWrFsHwUpdateAdminStatusChange  sFsHwUpdateAdminStatusChange;
    tCfaRemoteNpWrFsMauHwGetAutoNegAdminConfig  sFsMauHwGetAutoNegAdminConfig;
    tCfaRemoteNpWrFsMauHwGetAutoNegAdminStatus  sFsMauHwGetAutoNegAdminStatus;
    tCfaRemoteNpWrFsMauHwGetAutoNegCapAdvtBits  sFsMauHwGetAutoNegCapAdvtBits;
    tCfaRemoteNpWrFsMauHwGetAutoNegCapBits  sFsMauHwGetAutoNegCapBits;
    tCfaRemoteNpWrFsMauHwGetAutoNegCapRcvdBits  sFsMauHwGetAutoNegCapRcvdBits;
    tCfaRemoteNpWrFsMauHwGetAutoNegRemFltAdvt  sFsMauHwGetAutoNegRemFltAdvt;
    tCfaRemoteNpWrFsMauHwGetAutoNegRemFltRcvd  sFsMauHwGetAutoNegRemFltRcvd;
    tCfaRemoteNpWrFsMauHwGetAutoNegRemoteSignaling  sFsMauHwGetAutoNegRemoteSignaling;
    tCfaRemoteNpWrFsMauHwGetAutoNegRestart  sFsMauHwGetAutoNegRestart;
    tCfaRemoteNpWrFsMauHwGetAutoNegSupported  sFsMauHwGetAutoNegSupported;
    tCfaRemoteNpWrFsMauHwGetFalseCarriers  sFsMauHwGetFalseCarriers;
    tCfaRemoteNpWrFsMauHwGetJabberState  sFsMauHwGetJabberState;
    tCfaRemoteNpWrFsMauHwGetJabberingStateEnters  sFsMauHwGetJabberingStateEnters;
    tCfaRemoteNpWrFsMauHwGetJackType  sFsMauHwGetJackType;
    tCfaRemoteNpWrFsMauHwGetMauType  sFsMauHwGetMauType;
    tCfaRemoteNpWrFsMauHwGetMediaAvailStateExits  sFsMauHwGetMediaAvailStateExits;
    tCfaRemoteNpWrFsMauHwGetMediaAvailable  sFsMauHwGetMediaAvailable;
    tCfaRemoteNpWrFsMauHwGetTypeListBits  sFsMauHwGetTypeListBits;
    tCfaRemoteNpWrFsMauHwSetAutoNegAdminStatus  sFsMauHwSetAutoNegAdminStatus;
    tCfaRemoteNpWrFsMauHwSetAutoNegCapAdvtBits  sFsMauHwSetAutoNegCapAdvtBits;
    tCfaRemoteNpWrFsMauHwSetAutoNegRemFltAdvt  sFsMauHwSetAutoNegRemFltAdvt;
    tCfaRemoteNpWrFsMauHwSetAutoNegRestart  sFsMauHwSetAutoNegRestart;
    tCfaRemoteNpWrFsMauHwSetMauStatus  sFsMauHwSetMauStatus;
    tCfaRemoteNpWrFsMauHwGetStatus  sFsMauHwGetStatus;
    tCfaRemoteNpWrFsMauHwSetDefaultType  sFsMauHwSetDefaultType;
    tCfaRemoteNpWrFsMauHwGetDefaultType  sFsMauHwGetDefaultType;
    tCfaRemoteNpWrNpCfaFrontPanelPorts  sNpCfaFrontPanelPorts;
    tCfaRemoteNpWrFsCfaHwMacLookup  sFsCfaHwMacLookup;
    tCfaRemoteNpWrCfaNpGetPhyAndLinkStatus  sCfaNpGetPhyAndLinkStatus;
    tCfaRemoteNpWrCfaNpSetHwPortInfo  sCfaNpSetHwPortInfo;
    tCfaRemoteNpWrCfaNpGetHwPortInfo  sCfaNpGetHwPortInfo;
    tCfaRemoteNpWrFsCfaHwDeletePktFilter  sFsCfaHwDeletePktFilter;
    tCfaRemoteNpWrFsCfaHwCreatePktFilter  sFsCfaHwCreatePktFilter;
    tCfaRemoteNpWrFsNpCfaSetDlfStatus  sFsNpCfaSetDlfStatus;
    tCfaRemoteNpWrCfaNpSetStackingModel  sCfaNpSetStackingModel;
    tCfaRemoteNpWrCfaNpGetHwInfo  sCfaNpGetHwInfo;
    tCfaRemoteNpWrCfaNpRemoteSetHwInfo  sCfaNpRemoteSetHwInfo;
    tCfaRemoteNpWrFsHwGetVlanIntfStats  sFsHwGetVlanIntfStats;
    tCfaRemoteNpWrFsCfaHwRemoveIpNetRcvdDlfInHash  sFsCfaHwRemoveIpNetRcvdDlfInHash;
#ifdef IP6_WANTED
    tCfaRemoteNpWrFsCfaHwRemoveIp6NetRcvdDlfInHash  sFsCfaHwRemoveIp6NetRcvdDlfInHash;
#endif
    tCfaRemoteNpWrFsCfaHwL3SetMtu  sFsCfaHwL3SetMtu;
    }unOpCode;

#define  CfaRemoteNpCfaNpGetLinkStatus  unOpCode.sCfaNpGetLinkStatus;
#define  CfaRemoteNpFsCfaHwClearStats  unOpCode.sFsCfaHwClearStats;
#define  CfaRemoteNpFsCfaHwGetIfFlowControl  unOpCode.sFsCfaHwGetIfFlowControl;
#define  CfaRemoteNpFsCfaHwGetMtu  unOpCode.sFsCfaHwGetMtu;
#define  CfaRemoteNpFsCfaHwSetMacAddr  unOpCode.sFsCfaHwSetMacAddr;
#define  CfaRemoteNpFsCfaHwSetMtu  unOpCode.sFsCfaHwSetMtu;
#define  CfaRemoteNpFsCfaHwSetWanTye  unOpCode.sFsCfaHwSetWanTye;
#define  CfaRemoteNpFsEtherHwGetCntrl  unOpCode.sFsEtherHwGetCntrl;
#define  CfaRemoteNpFsEtherHwGetCollFreq  unOpCode.sFsEtherHwGetCollFreq;
#define  CfaRemoteNpFsEtherHwGetPauseFrames  unOpCode.sFsEtherHwGetPauseFrames;
#define  CfaRemoteNpFsEtherHwGetPauseMode  unOpCode.sFsEtherHwGetPauseMode;
#define  CfaRemoteNpFsEtherHwGetStats  unOpCode.sFsEtherHwGetStats;
#define  CfaRemoteNpFsEtherHwSetPauseAdminMode  unOpCode.sFsEtherHwSetPauseAdminMode;
#define  CfaRemoteNpFsHwGetEthernetType  unOpCode.sFsHwGetEthernetType;
#define  CfaRemoteNpFsHwGetStat  unOpCode.sFsHwGetStat;
#define  CfaRemoteNpFsHwGetStat64  unOpCode.sFsHwGetStat64;
#define  CfaRemoteNpFsHwSetCustIfParams  unOpCode.sFsHwSetCustIfParams;
#define  CfaRemoteNpFsHwUpdateAdminStatusChange  unOpCode.sFsHwUpdateAdminStatusChange;
#define  CfaRemoteNpFsMauHwGetAutoNegAdminConfig  unOpCode.sFsMauHwGetAutoNegAdminConfig;
#define  CfaRemoteNpFsMauHwGetAutoNegAdminStatus  unOpCode.sFsMauHwGetAutoNegAdminStatus;
#define  CfaRemoteNpFsMauHwGetAutoNegCapAdvtBits  unOpCode.sFsMauHwGetAutoNegCapAdvtBits;
#define  CfaRemoteNpFsMauHwGetAutoNegCapBits  unOpCode.sFsMauHwGetAutoNegCapBits;
#define  CfaRemoteNpFsMauHwGetAutoNegCapRcvdBits  unOpCode.sFsMauHwGetAutoNegCapRcvdBits;
#define  CfaRemoteNpFsMauHwGetAutoNegRemFltAdvt  unOpCode.sFsMauHwGetAutoNegRemFltAdvt;
#define  CfaRemoteNpFsMauHwGetAutoNegRemFltRcvd  unOpCode.sFsMauHwGetAutoNegRemFltRcvd;
#define  CfaRemoteNpFsMauHwGetAutoNegRemoteSignaling  unOpCode.sFsMauHwGetAutoNegRemoteSignaling;
#define  CfaRemoteNpFsMauHwGetAutoNegRestart  unOpCode.sFsMauHwGetAutoNegRestart;
#define  CfaRemoteNpFsMauHwGetAutoNegSupported  unOpCode.sFsMauHwGetAutoNegSupported;
#define  CfaRemoteNpFsMauHwGetFalseCarriers  unOpCode.sFsMauHwGetFalseCarriers;
#define  CfaRemoteNpFsMauHwGetJabberState  unOpCode.sFsMauHwGetJabberState;
#define  CfaRemoteNpFsMauHwGetJabberingStateEnters  unOpCode.sFsMauHwGetJabberingStateEnters;
#define  CfaRemoteNpFsMauHwGetJackType  unOpCode.sFsMauHwGetJackType;
#define  CfaRemoteNpFsMauHwGetMauType  unOpCode.sFsMauHwGetMauType;
#define  CfaRemoteNpFsMauHwGetMediaAvailStateExits  unOpCode.sFsMauHwGetMediaAvailStateExits;
#define  CfaRemoteNpFsMauHwGetMediaAvailable  unOpCode.sFsMauHwGetMediaAvailable;
#define  CfaRemoteNpFsMauHwGetTypeListBits  unOpCode.sFsMauHwGetTypeListBits;
#define  CfaRemoteNpFsMauHwSetAutoNegAdminStatus  unOpCode.sFsMauHwSetAutoNegAdminStatus;
#define  CfaRemoteNpFsMauHwSetAutoNegCapAdvtBits  unOpCode.sFsMauHwSetAutoNegCapAdvtBits;
#define  CfaRemoteNpFsMauHwSetAutoNegRemFltAdvt  unOpCode.sFsMauHwSetAutoNegRemFltAdvt;
#define  CfaRemoteNpFsMauHwSetAutoNegRestart  unOpCode.sFsMauHwSetAutoNegRestart;
#define  CfaRemoteNpFsMauHwSetMauStatus  unOpCode.sFsMauHwSetMauStatus;
#define  CfaRemoteNpFsMauHwGetStatus  unOpCode.sFsMauHwGetStatus;
#define  CfaRemoteNpFsMauHwSetDefaultType  unOpCode.sFsMauHwSetDefaultType;
#define  CfaRemoteNpFsMauHwGetDefaultType  unOpCode.sFsMauHwGetDefaultType;
#define  CfaRemoteNpNpCfaFrontPanelPorts  unOpCode.sNpCfaFrontPanelPorts;
#define  CfaRemoteNpFsCfaHwMacLookup  unOpCode.sFsCfaHwMacLookup;
#define  CfaRemoteNpCfaNpGetPhyAndLinkStatus  unOpCode.sCfaNpGetPhyAndLinkStatus;
#define  CfaRemoteNpCfaNpSetHwPortInfo  unOpCode.sCfaNpSetHwPortInfo;
#define  CfaRemoteNpCfaNpGetHwPortInfo  unOpCode.sCfaNpGetHwPortInfo;
#define  CfaRemoteNpFsCfaHwDeletePktFilter  unOpCode.sFsCfaHwDeletePktFilter;
#define  CfaRemoteNpFsCfaHwCreatePktFilter  unOpCode.sFsCfaHwCreatePktFilter;
#define  CfaRemoteNpFsNpCfaSetDlfStatus  unOpCode.sFsNpCfaSetDlfStatus;
#define  CfaRemoteNpCfaNpSetStackingModel  unOpCode.sCfaNpSetStackingModel;
#define  CfaRemoteNpCfaNpGetHwInfo  unOpCode.sCfaNpGetHwInfo;
#define  CfaRemoteNpCfaNpRemoteSetHwInfo  unOpCode.sCfaNpRemoteSetHwInfo;
#define  CfaRemoteNpFsHwGetVlanIntfStats  unOpCode.sFsHwGetVlanIntfStats;
#define  CfaRemoteNpFsCfaHwRemoveIpNetRcvdDlfInHash unOpCode.sFsCfaHwRemoveIpNetRcvdDlfInHash;
#ifdef IP6_WANTED
#define  CfaRemoteNpFsCfaHwRemoveIp6NetRcvdDlfInHash unOpCode.sFsCfaHwRemoveIp6NetRcvdDlfInHash;
#endif
#define  CfaRemoteNpFsCfaHwL3SetMtu  unOpCode.sFsCfaHwL3SetMtu;
} tCfaRemoteNpModInfo;

#endif /* CFA_WANTED */

#ifdef  RSTP_WANTED 

typedef struct {
    UINT4  u4ContextId;
    UINT1  au1Pad[4]; /*8-Byte Padding for 64 Bit Compliance*/
} tRstpRemoteNpWrFsMiStpNpHwInit;

typedef struct {
    UINT4  u4ContextId;
    UINT1  au1Pad[4]; /*8-Byte Padding for 64 Bit Compliance*/
} tRstpRemoteNpWrFsMiRstpNpInitHw;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
    UINT1  u1Status;
    UINT1  au1Pad[7]; /*8-Byte Padding for 64 Bit Compliance*/
} tRstpRemoteNpWrFsMiRstpNpSetPortState;

typedef struct {
    UINT4    u4ContextId;
    UINT4    u4IfIndex;
    UINT1    u1Status;
    UINT1    au1Pad[7]; /*8-Byte Padding for 64 Bit Compliance*/
} tRstpRemoteNpWrFsMiRstNpGetPortState;

#ifdef PB_WANTED
typedef struct {
    UINT4    u4ContextId;
    UINT4    u4IfIndex;
    tVlanId  SVid;
    UINT1    u1Status;
    UINT1    au1Pad[1];
} tRstpRemoteNpWrFsMiPbRstpNpSetPortState;

typedef struct {
    UINT4    u4ContextId;
    UINT4    u4IfIndex;
    tVlanId  SVlanId;
    UINT1    u1Status;
    UINT1    au1Pad[1];
} tRstpRemoteNpWrFsMiPbRstNpGetPortState;

#endif
#ifdef PBB_WANTED
typedef struct {
    UINT4  u4ContextId;
    UINT4  u4VipIndex;
    UINT4  u4Isid;
    UINT1  u1PointToPointStatus;
    UINT1  au1Pad[3];
} tRstpRemoteNpWrFsMiPbbRstHwServiceInstancePtToPtStatus;

#endif
typedef struct RstpRemoteNpModInfo {
union {
    tRstpRemoteNpWrFsMiStpNpHwInit  sFsMiStpNpHwInit;
    tRstpRemoteNpWrFsMiRstpNpInitHw  sFsMiRstpNpInitHw;
    tRstpRemoteNpWrFsMiRstpNpSetPortState  sFsMiRstpNpSetPortState;
    tRstpRemoteNpWrFsMiRstNpGetPortState  sFsMiRstNpGetPortState;
#ifdef PB_WANTED
    tRstpRemoteNpWrFsMiPbRstpNpSetPortState  sFsMiPbRstpNpSetPortState;
    tRstpRemoteNpWrFsMiPbRstNpGetPortState  sFsMiPbRstNpGetPortState;
#endif
#ifdef PBB_WANTED
    tRstpRemoteNpWrFsMiPbbRstHwServiceInstancePtToPtStatus  sFsMiPbbRstHwServiceInstancePtToPtStatus;
#endif
    }unOpCode;

#define  RstpRemoteNpFsMiStpNpHwInit  unOpCode.sFsMiStpNpHwInit;
#define  RstpRemoteNpFsMiRstpNpInitHw  unOpCode.sFsMiRstpNpInitHw;
#define  RstpRemoteNpFsMiRstpNpSetPortState  unOpCode.sFsMiRstpNpSetPortState;
#define  RstpRemoteNpFsMiRstNpGetPortState  unOpCode.sFsMiRstNpGetPortState;
#ifdef PB_WANTED
#define  RstpRemoteNpFsMiPbRstpNpSetPortState  unOpCode.sFsMiPbRstpNpSetPortState;
#define  RstpRemoteNpFsMiPbRstNpGetPortState  unOpCode.sFsMiPbRstNpGetPortState;
#endif
#ifdef PBB_WANTED
#define  RstpRemoteNpFsMiPbbRstHwServiceInstancePtToPtStatus  unOpCode.sFsMiPbbRstHwServiceInstancePtToPtStatus;
#endif
} tRstpRemoteNpModInfo;

#endif /* RSTP_WANTED */

#ifdef MSTP_WANTED

typedef struct {
    UINT4  u4ContextId;
    UINT2  u2InstId;
    UINT1  au1Pad[2];
} tMstpRemoteNpWrFsMiMstpNpCreateInstance;

typedef struct {
    UINT4    u4ContextId;
    tVlanId  VlanId;
    UINT2    u2InstId;
} tMstpRemoteNpWrFsMiMstpNpAddVlanInstMapping;

typedef struct {
    UINT1    au1VlanList[AST_VLAN_LIST_SIZE];
    UINT4    u4ContextId;
    UINT2    u2InstId;
    UINT2    u2NumVlans;
    UINT2    u2LastVlan;
    UINT1    au1Pad[2];
} tMstpRemoteNpWrFsMiMstpNpAddVlanListInstMapping;

typedef struct {
    UINT4  u4ContextId;
    UINT2  u2InstId;
    UINT1  au1Pad[2];
} tMstpRemoteNpWrFsMiMstpNpDeleteInstance;

typedef struct {
    UINT4    u4ContextId;
    tVlanId  VlanId;
    UINT2    u2InstId;
} tMstpRemoteNpWrFsMiMstpNpDelVlanInstMapping;

typedef struct {
    UINT1    au1VlanList[AST_VLAN_LIST_SIZE];
    UINT4    u4ContextId;
    UINT2    u2InstId;
    UINT2    u2NumVlans;
    UINT2    u2LastVlan;
    UINT1    au1Pad[2];
} tMstpRemoteNpWrFsMiMstpNpDelVlanListInstMapping;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
    UINT2  u2InstanceId;
    UINT1  u1PortState;
    UINT1  au1Pad[5]; /*8-Byte Padding for 64 Bit Compliance*/
} tMstpRemoteNpWrFsMiMstpNpSetInstancePortState;

typedef struct {
    UINT4  u4ContextId;
    UINT1  au1Pad[4]; /*8-Byte Padding for 64 Bit Compliance*/
} tMstpRemoteNpWrFsMiMstpNpInitHw;

typedef struct {
    UINT4  u4ContextId;
    UINT1  au1Pad[4]; /*8-Byte Padding for 64 Bit Compliance*/
} tMstpRemoteNpWrFsMiMstpNpDeInitHw;

typedef struct {
    UINT4    u4ContextId;
    UINT4    u4IfIndex;
    UINT2    u2InstanceId;
    UINT1    u1Status;
    UINT1    au1Pad[5]; /*8-Byte Padding for 64 Bit Compliance*/
} tMstpRemoteNpWrFsMiMstNpGetPortState;

typedef struct MstpRemoteNpModInfo {
union {
    tMstpRemoteNpWrFsMiMstpNpCreateInstance  sFsMiMstpNpCreateInstance;
    tMstpRemoteNpWrFsMiMstpNpAddVlanInstMapping  sFsMiMstpNpAddVlanInstMapping;
    tMstpRemoteNpWrFsMiMstpNpAddVlanListInstMapping  sFsMiMstpNpAddVlanListInstMapping;
    tMstpRemoteNpWrFsMiMstpNpDeleteInstance  sFsMiMstpNpDeleteInstance;
    tMstpRemoteNpWrFsMiMstpNpDelVlanInstMapping  sFsMiMstpNpDelVlanInstMapping;
    tMstpRemoteNpWrFsMiMstpNpDelVlanListInstMapping  sFsMiMstpNpDelVlanListInstMapping;
    tMstpRemoteNpWrFsMiMstpNpSetInstancePortState  sFsMiMstpNpSetInstancePortState;
    tMstpRemoteNpWrFsMiMstpNpInitHw  sFsMiMstpNpInitHw;
    tMstpRemoteNpWrFsMiMstpNpDeInitHw  sFsMiMstpNpDeInitHw;
    tMstpRemoteNpWrFsMiMstNpGetPortState  sFsMiMstNpGetPortState;
    }unOpCode;

#define  MstpRemoteNpFsMiMstpNpCreateInstance  unOpCode.sFsMiMstpNpCreateInstance;
#define  MstpRemoteNpFsMiMstpNpAddVlanInstMapping  unOpCode.sFsMiMstpNpAddVlanInstMapping;
#define  MstpRemoteNpFsMiMstpNpAddVlanListInstMapping  unOpCode.sFsMiMstpNpAddVlanListInstMapping;
#define  MstpRemoteNpFsMiMstpNpDeleteInstance  unOpCode.sFsMiMstpNpDeleteInstance;
#define  MstpRemoteNpFsMiMstpNpDelVlanInstMapping  unOpCode.sFsMiMstpNpDelVlanInstMapping;
#define  MstpRemoteNpFsMiMstpNpDelVlanListInstMapping  unOpCode.sFsMiMstpNpDelVlanListInstMapping;
#define  MstpRemoteNpFsMiMstpNpSetInstancePortState  unOpCode.sFsMiMstpNpSetInstancePortState;
#define  MstpRemoteNpFsMiMstpNpInitHw  unOpCode.sFsMiMstpNpInitHw;
#define  MstpRemoteNpFsMiMstpNpDeInitHw  unOpCode.sFsMiMstpNpDeInitHw;
#define  MstpRemoteNpFsMiMstNpGetPortState  unOpCode.sFsMiMstNpGetPortState;
} tMstpRemoteNpModInfo;

#endif /* MSTP_WANTED */

#ifdef PVRST_WANTED
typedef struct {
    UINT4    u4ContextId;
    tVlanId  VlanId;
    UINT1    au1Pad[2];
} tPvrstRemoteNpWrFsMiPvrstNpCreateVlanSpanningTree;

typedef struct {
    UINT4    u4ContextId;
    tVlanId  VlanId;
    UINT1    au1Pad[2];
} tPvrstRemoteNpWrFsMiPvrstNpDeleteVlanSpanningTree;

typedef struct {
    UINT4  u4ContextId;
    UINT1  au1Pad[4]; /*8-Byte Padding for 64 Bit Compliance*/
} tPvrstRemoteNpWrFsMiPvrstNpInitHw;

typedef struct {
    UINT4  u4ContextId;
    UINT1  au1Pad[4]; /*8-Byte Padding for 64 Bit Compliance*/
} tPvrstRemoteNpWrFsMiPvrstNpDeInitHw;

typedef struct {
    UINT4    u4ContextId;
    UINT4    u4IfIndex;
    tVlanId  VlanId;
    UINT1    u1PortState;
    UINT1    au1Pad[1];
} tPvrstRemoteNpWrFsMiPvrstNpSetVlanPortState;

typedef struct {
    UINT4    u4ContextId;
    UINT4    u4IfIndex;
    tVlanId  VlanId;
    UINT1    u1Status;
    UINT1    au1Pad[1];
} tPvrstRemoteNpWrFsMiPvrstNpGetVlanPortState;

typedef struct PvrstRemoteNpModInfo {
union {
    tPvrstRemoteNpWrFsMiPvrstNpCreateVlanSpanningTree  sFsMiPvrstNpCreateVlanSpanningTree;
    tPvrstRemoteNpWrFsMiPvrstNpDeleteVlanSpanningTree  sFsMiPvrstNpDeleteVlanSpanningTree;
    tPvrstRemoteNpWrFsMiPvrstNpInitHw  sFsMiPvrstNpInitHw;
    tPvrstRemoteNpWrFsMiPvrstNpDeInitHw  sFsMiPvrstNpDeInitHw;
    tPvrstRemoteNpWrFsMiPvrstNpSetVlanPortState  sFsMiPvrstNpSetVlanPortState;
    tPvrstRemoteNpWrFsMiPvrstNpGetVlanPortState  sFsMiPvrstNpGetVlanPortState;
    }unOpCode;

#define  PvrstRemoteNpFsMiPvrstNpCreateVlanSpanningTree  unOpCode.sFsMiPvrstNpCreateVlanSpanningTree;
#define  PvrstRemoteNpFsMiPvrstNpDeleteVlanSpanningTree  unOpCode.sFsMiPvrstNpDeleteVlanSpanningTree;
#define  PvrstRemoteNpFsMiPvrstNpInitHw  unOpCode.sFsMiPvrstNpInitHw;
#define  PvrstRemoteNpFsMiPvrstNpDeInitHw  unOpCode.sFsMiPvrstNpDeInitHw;
#define  PvrstRemoteNpFsMiPvrstNpSetVlanPortState  unOpCode.sFsMiPvrstNpSetVlanPortState;
#define  PvrstRemoteNpFsMiPvrstNpGetVlanPortState  unOpCode.sFsMiPvrstNpGetVlanPortState;
} tPvrstRemoteNpModInfo;

#endif /* PVRST_WANTED */

#ifdef VCM_WANTED
typedef struct {
    UINT4  u4ContextId;
} tVcmRemoteNpWrFsVcmHwCreateContext;

typedef struct {
    UINT4  u4ContextId;
} tVcmRemoteNpWrFsVcmHwDeleteContext;

typedef struct {
    UINT4            u4ContextId;
    UINT4            u4IfIndex;
    tContextMapInfo  ContextInfo;
} tVcmRemoteNpWrFsVcmHwMapIfIndexToBrgPort;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
} tVcmRemoteNpWrFsVcmHwMapPortToContext;

typedef struct {
    tVrMapAction         VrMapAction;
    tFsNpVcmVrMapInfo    VcmVrMapInfo;
} tVcmRemoteNpWrFsVcmHwMapVirtualRouter;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
} tVcmRemoteNpWrFsVcmHwUnMapPortFromContext;

typedef struct {
    UINT4  u4IfIndex;
    UINT1  u1Status;
    UINT1  au1Pad[3];
} tVcmRemoteNpWrFsVcmSispHwSetPortCtrlStatus;

typedef struct {
    UINT4    u4IfIndex;
    UINT4    u4ContextId;
    tVlanId  VlanId;
    UINT1    u1Status;
    UINT1    au1Pad[1];
} tVcmRemoteNpWrFsVcmSispHwSetPortVlanMapping;

typedef struct {
     tVrfCounter VrfCounter;
}tVcmRemoteNpWrFsVrfCounters;

typedef struct VcmRemoteNpModInfo {
union {
    tVcmRemoteNpWrFsVcmHwCreateContext  sFsVcmHwCreateContext;
    tVcmRemoteNpWrFsVcmHwDeleteContext  sFsVcmHwDeleteContext;
    tVcmRemoteNpWrFsVcmHwMapIfIndexToBrgPort  sFsVcmHwMapIfIndexToBrgPort;
    tVcmRemoteNpWrFsVcmHwMapPortToContext  sFsVcmHwMapPortToContext;
    tVcmRemoteNpWrFsVcmHwMapVirtualRouter  sFsVcmHwMapVirtualRouter;
    tVcmRemoteNpWrFsVcmHwUnMapPortFromContext  sFsVcmHwUnMapPortFromContext;
    tVcmRemoteNpWrFsVcmSispHwSetPortCtrlStatus  sFsVcmSispHwSetPortCtrlStatus;
    tVcmRemoteNpWrFsVcmSispHwSetPortVlanMapping  sFsVcmSispHwSetPortVlanMapping;
     tVcmRemoteNpWrFsVrfCounters sFsVrfCounters;
    }unOpCode;

#define  VcmRemoteNpFsVcmHwCreateContext  unOpCode.sFsVcmHwCreateContext;
#define  VcmRemoteNpFsVcmHwDeleteContext  unOpCode.sFsVcmHwDeleteContext;
#define  VcmRemoteNpFsVcmHwMapIfIndexToBrgPort  unOpCode.sFsVcmHwMapIfIndexToBrgPort;
#define  VcmRemoteNpFsVcmHwMapPortToContext  unOpCode.sFsVcmHwMapPortToContext;
#define  VcmRemoteNpFsVcmHwMapVirtualRouter  unOpCode.sFsVcmHwMapVirtualRouter;
#define  VcmRemoteNpFsVcmHwUnMapPortFromContext  unOpCode.sFsVcmHwUnMapPortFromContext;
#define  VcmRemoteNpFsVcmSispHwSetPortCtrlStatus  unOpCode.sFsVcmSispHwSetPortCtrlStatus;
#define  VcmRemoteNpFsVcmSispHwSetPortVlanMapping  unOpCode.sFsVcmSispHwSetPortVlanMapping;
#define  VcmRemoteNpWrFsVrfCounters unOpCode.sFsVrfCounters;
} tVcmRemoteNpModInfo;

#endif /* VCM_WANTED */

#ifdef PNAC_WANTED
#ifdef L2RED_WANTED
typedef struct {
    UINT4     u4Port;
    tMacAddr    u1SuppAddr;
    UINT1     u1AuthMode;
    UINT1     u1AuthStatus;
    UINT1    u1CtrlDir;
    UINT1     au1Pad[3];
} tPnacRemoteNpWrFsPnacRedHwUpdateDB;

#endif /* L2RED_WANTED */
typedef struct {
    UINT4     u4Port;
    tMacAddr    u1SuppAddr;
    UINT1     u1SessStatus;
    UINT1     au1Pad[1];
} tPnacRemoteNpWrPnacHwAddOrDelMacSess;

typedef struct {
    UINT4     u4PortNum;
    tMacAddr    u1SuppAddr;
    UINT2     u2AuthStatus;
    UINT2     u2CtrlDir;
    UINT1     u1AuthMode;
    UINT1     au1Pad[1];
} tPnacRemoteNpWrPnacHwGetAuthStatus;

typedef struct {
    UINT4    u4Port;
    UINT4    u4HiCounter;
    UINT4    u4LoCounter;
    tMacAddr    u1SuppAddr;
    UINT1    u1CounterType;
    UINT1    au1Pad[1];
} tPnacRemoteNpWrPnacHwGetSessionCounter;

typedef struct {
    UINT4     u4Port;
    tMacAddr    u1SuppAddr;
    UINT1     u1AuthMode;
    UINT1     u1AuthStatus;
    UINT1     u1CtrlDir;
    UINT1    au1Pad[3];
} tPnacRemoteNpWrPnacHwSetAuthStatus;

typedef struct {
    UINT4     u4Port;
    tMacAddr    u1SuppAddr;
    UINT1    au1Pad[2];
} tPnacRemoteNpWrPnacHwStartSessionCounters;

typedef struct {
    UINT4    u4Port;
    tMacAddr    u1SuppAddr;
    UINT1    au1Pad[2];
} tPnacRemoteNpWrPnacHwStopSessionCounters;

typedef struct PnacRemoteNpModInfo {
union {
#ifdef L2RED_WANTED
    tPnacRemoteNpWrFsPnacRedHwUpdateDB  sFsPnacRedHwUpdateDB;
#endif /* L2RED_WANTED */
    tPnacRemoteNpWrPnacHwAddOrDelMacSess  sPnacHwAddOrDelMacSess;
    tPnacRemoteNpWrPnacHwGetAuthStatus  sPnacHwGetAuthStatus;
    tPnacRemoteNpWrPnacHwGetSessionCounter  sPnacHwGetSessionCounter;
    tPnacRemoteNpWrPnacHwSetAuthStatus  sPnacHwSetAuthStatus;
    tPnacRemoteNpWrPnacHwStartSessionCounters  sPnacHwStartSessionCounters;
    tPnacRemoteNpWrPnacHwStopSessionCounters  sPnacHwStopSessionCounters;
    }unOpCode;

#ifdef L2RED_WANTED
#define  PnacRemoteNpFsPnacRedHwUpdateDB  unOpCode.sFsPnacRedHwUpdateDB;
#endif /* L2RED_WANTED */
#define  PnacRemoteNpPnacHwAddOrDelMacSess  unOpCode.sPnacHwAddOrDelMacSess;
#define  PnacRemoteNpPnacHwGetAuthStatus  unOpCode.sPnacHwGetAuthStatus;
#define  PnacRemoteNpPnacHwGetSessionCounter  unOpCode.sPnacHwGetSessionCounter;
#define  PnacRemoteNpPnacHwSetAuthStatus  unOpCode.sPnacHwSetAuthStatus;
#define  PnacRemoteNpPnacHwStartSessionCounters  unOpCode.sPnacHwStartSessionCounters;
#define  PnacRemoteNpPnacHwStopSessionCounters  unOpCode.sPnacHwStopSessionCounters;
} tPnacRemoteNpModInfo;

#endif /* PNAC_WANTED */
#ifdef EOAM_WANTED
typedef struct {
    UINT4    u4IfIndex;
    UINT4    u4Value;
    UINT1    u1StatType;
    UINT1    au1Pad[3];
} tEoamRemoteNpWrEoamLmNpGetStat;

typedef struct {
    UINT4      u4IfIndex;
    FS_UINT8   U8Value;
    UINT1      u1StatType;
    UINT1      au1Pad[3];
} tEoamRemoteNpWrEoamLmNpGetStat64;

typedef struct {
    UINT4    u4IfIndex;
    UINT1    u1Capability;
    UINT1    au1Pad[3];
} tEoamRemoteNpWrEoamNpGetUniDirectionalCapability;

typedef struct {
    UINT4     u4IfIndex;
    tMacAddr  SrcMac;
    tMacAddr  DestMac;
    UINT1     u1Status;
    UINT1     au1Pad[3];
} tEoamRemoteNpWrEoamNpHandleLocalLoopBack;

typedef struct {
    UINT4     u4IfIndex;
    tMacAddr  SrcMac;
    tMacAddr  PeerMac;
    UINT1     u1Status;
    UINT1     au1Pad[3];
} tEoamRemoteNpWrEoamNpHandleRemoteLoopBack;

typedef struct {
    UINT4     u4IfIndex;
    FS_UINT8  Window;
    FS_UINT8  Threshold;
    UINT2     u2Event;
    UINT1     au1Pad[2];
} tEoamRemoteNpWrEoamLmNpConfigureParams;

typedef struct EoamRemoteNpModInfo {
union {
    tEoamRemoteNpWrEoamLmNpGetStat  sEoamLmNpGetStat;
    tEoamRemoteNpWrEoamLmNpGetStat64  sEoamLmNpGetStat64;
    tEoamRemoteNpWrEoamNpGetUniDirectionalCapability  sEoamNpGetUniDirectionalCapability;
    tEoamRemoteNpWrEoamNpHandleLocalLoopBack  sEoamNpHandleLocalLoopBack;
    tEoamRemoteNpWrEoamNpHandleRemoteLoopBack  sEoamNpHandleRemoteLoopBack;
    tEoamRemoteNpWrEoamLmNpConfigureParams  sEoamLmNpConfigureParams;
    }unOpCode;

#define  EoamRemoteNpEoamLmNpGetStat  unOpCode.sEoamLmNpGetStat;
#define  EoamRemoteNpEoamLmNpGetStat64  unOpCode.sEoamLmNpGetStat64;
#define  EoamRemoteNpEoamNpGetUniDirectionalCapability  unOpCode.sEoamNpGetUniDirectionalCapability;
#define  EoamRemoteNpEoamNpHandleLocalLoopBack  unOpCode.sEoamNpHandleLocalLoopBack;
#define  EoamRemoteNpEoamNpHandleRemoteLoopBack  unOpCode.sEoamNpHandleRemoteLoopBack;
#define  EoamRemoteNpEoamLmNpConfigureParams  unOpCode.sEoamLmNpConfigureParams;
} tEoamRemoteNpModInfo;

#endif /* EOAM_WANTED */

#ifdef EOAM_FM_WANTED

typedef struct {
    UINT4  u4IfIndex;
    UINT1  u1EventType;
    UINT1  u1Status;
    UINT1  au1Pad[2];
} tEoamfmRemoteNpWrNpFmFailureIndicationCallbackFunc;

typedef struct EoamfmRemoteNpModInfo {
union {
    tEoamfmRemoteNpWrNpFmFailureIndicationCallbackFunc  sNpFmFailureIndicationCallbackFunc;
    }unOpCode;

#define  EoamfmRemoteNpNpFmFailureIndicationCallbackFunc  unOpCode.sNpFmFailureIndicationCallbackFunc;
} tEoamfmRemoteNpModInfo;

#endif /* EOAMFM_WANTED */

#ifdef IGS_WANTED

typedef struct {
    UINT4  u4Instance;
} tIgsRemoteNpWrFsMiIgsHwClearIpmcFwdEntries;

typedef struct {
    UINT4  u4Instance;
} tIgsRemoteNpWrFsMiIgsHwDisableIgmpSnooping;

typedef struct {
    UINT4  u4Instance;
} tIgsRemoteNpWrFsMiIgsHwDisableIpIgmpSnooping;

typedef struct {
    UINT4  u4Instance;
} tIgsRemoteNpWrFsMiIgsHwEnableIgmpSnooping;

typedef struct {
    UINT4  u4Instance;
} tIgsRemoteNpWrFsMiIgsHwEnableIpIgmpSnooping;

typedef struct {
    UINT4  u4ContextId;
    UINT1  u1EnhStatus;
    UINT1  au1Pad[3];
} tIgsRemoteNpWrFsMiIgsHwEnhancedMode;

typedef struct {
    UINT4            u4Instance;
    tIgsHwRateLmt    IgsHwRateLmt;
    UINT1            u1Action;
    UINT1            au1Pad[3];
} tIgsRemoteNpWrFsMiIgsHwPortRateLimit;

typedef struct {
    UINT4              u4Instance;
    tIgsHwIpFwdInfo    IgsHwIpFwdInfo;
    UINT1              u1Action;
    UINT1              au1Pad[3];
} tIgsRemoteNpWrFsMiIgsHwUpdateIpmcEntry;

typedef struct {
    UINT4         u4Instance;
    UINT4         u4GrpAddr;
    UINT4         u4SrcAddr;
    tSnoopVlanId  VlanId;
    UINT1         au1Pad[2];
} tIgsRemoteNpWrFsMiNpGetFwdEntryHitBitStatus;

typedef struct {
    UINT4         u4Instance;
    UINT4         u4GrpAddr;
    UINT4         u4SrcAddr;
    tPortList     PortList;
    tPortList     UntagPortList;
    tSnoopVlanId  VlanId;
    UINT1         u1EventType;
    UINT1         au1Pad[1];
} tIgsRemoteNpWrFsMiNpUpdateIpmcFwdEntries;

typedef struct {
    UINT4         u4Instance;
    UINT4         u4Port;
    tSnoopVlanId  VlanId;
    UINT1         au1Pad[2];
} tIgsRemoteNpWrFsMiIgsHwAddRtrPort;

typedef struct {
    UINT4         u4Instance;
    UINT4         u4Port;
    tSnoopVlanId  VlanId;
    UINT1         au1Pad[2];
} tIgsRemoteNpWrFsMiIgsHwDelRtrPort;

typedef struct {
    UINT4              u4Instance;
    tIgsHwIpFwdInfo   IgsHwIpFwdInfo;
} tIgsRemoteNpWrFsMiIgsHwGetIpFwdEntryHitBitStatus;

typedef struct {
    UINT4  u4ContextId;
    UINT1  u1SparseStatus;
    UINT1  au1Pad[3];
} tIgsRemoteNpWrFsMiIgsHwSparseMode;

typedef struct {
    tIgsIPMCInfoArray   IgsIPMCInfoArray;
    tMbsmSlotInfo       SlotInfo;
} tIgsRemoteNpWrFsMiIgsMbsmSyncIPMCInfo;

typedef struct IgsRemoteNpModInfo {
union {
    tIgsRemoteNpWrFsMiIgsHwClearIpmcFwdEntries  sFsMiIgsHwClearIpmcFwdEntries;
    tIgsRemoteNpWrFsMiIgsHwDisableIgmpSnooping  sFsMiIgsHwDisableIgmpSnooping;
    tIgsRemoteNpWrFsMiIgsHwDisableIpIgmpSnooping  sFsMiIgsHwDisableIpIgmpSnooping;
    tIgsRemoteNpWrFsMiIgsHwEnableIgmpSnooping  sFsMiIgsHwEnableIgmpSnooping;
    tIgsRemoteNpWrFsMiIgsHwEnableIpIgmpSnooping  sFsMiIgsHwEnableIpIgmpSnooping;
    tIgsRemoteNpWrFsMiIgsHwEnhancedMode  sFsMiIgsHwEnhancedMode;
    tIgsRemoteNpWrFsMiIgsHwPortRateLimit  sFsMiIgsHwPortRateLimit;
    tIgsRemoteNpWrFsMiIgsHwUpdateIpmcEntry  sFsMiIgsHwUpdateIpmcEntry;
    tIgsRemoteNpWrFsMiNpGetFwdEntryHitBitStatus  sFsMiNpGetFwdEntryHitBitStatus;
    tIgsRemoteNpWrFsMiNpUpdateIpmcFwdEntries  sFsMiNpUpdateIpmcFwdEntries;
    tIgsRemoteNpWrFsMiIgsHwAddRtrPort  sFsMiIgsHwAddRtrPort;
    tIgsRemoteNpWrFsMiIgsHwDelRtrPort  sFsMiIgsHwDelRtrPort;
    tIgsRemoteNpWrFsMiIgsHwGetIpFwdEntryHitBitStatus  sFsMiIgsHwGetIpFwdEntryHitBitStatus;
    tIgsRemoteNpWrFsMiIgsHwSparseMode  sFsMiIgsHwSparseMode;
    tIgsRemoteNpWrFsMiIgsMbsmSyncIPMCInfo  sFsMiIgsMbsmSyncIPMCInfo;
    }unOpCode;

#define  IgsRemoteNpFsMiIgsHwClearIpmcFwdEntries  unOpCode.sFsMiIgsHwClearIpmcFwdEntries;
#define  IgsRemoteNpFsMiIgsHwDisableIgmpSnooping  unOpCode.sFsMiIgsHwDisableIgmpSnooping;
#define  IgsRemoteNpFsMiIgsHwDisableIpIgmpSnooping  unOpCode.sFsMiIgsHwDisableIpIgmpSnooping;
#define  IgsRemoteNpFsMiIgsHwEnableIgmpSnooping  unOpCode.sFsMiIgsHwEnableIgmpSnooping;
#define  IgsRemoteNpFsMiIgsHwEnableIpIgmpSnooping  unOpCode.sFsMiIgsHwEnableIpIgmpSnooping;
#define  IgsRemoteNpFsMiIgsHwEnhancedMode  unOpCode.sFsMiIgsHwEnhancedMode;
#define  IgsRemoteNpFsMiIgsHwPortRateLimit  unOpCode.sFsMiIgsHwPortRateLimit;
#define  IgsRemoteNpFsMiIgsHwUpdateIpmcEntry  unOpCode.sFsMiIgsHwUpdateIpmcEntry;
#define  IgsRemoteNpFsMiNpGetFwdEntryHitBitStatus  unOpCode.sFsMiNpGetFwdEntryHitBitStatus;
#define  IgsRemoteNpFsMiNpUpdateIpmcFwdEntries  unOpCode.sFsMiNpUpdateIpmcFwdEntries;
#define  IgsRemoteNpFsMiIgsHwAddRtrPort  unOpCode.sFsMiIgsHwAddRtrPort;
#define  IgsRemoteNpFsMiIgsHwDelRtrPort  unOpCode.sFsMiIgsHwDelRtrPort;
#define  IgsRemoteNpFsMiIgsHwGetIpFwdEntryHitBitStatus  unOpCode.sFsMiIgsHwGetIpFwdEntryHitBitStatus;
#define  IgsRemoteNpFsMiIgsHwSparseMode  unOpCode.sFsMiIgsHwSparseMode;
#define  IgsRemoteNpFsMiIgsMbsmSyncIPMCInfo  unOpCode.sFsMiIgsMbsmSyncIPMCInfo;
} tIgsRemoteNpModInfo;

#endif /* IGS_WANTED */

#ifdef MLDS_WANTED
typedef struct {
    UINT4  u4Instance;
} tMldsRemoteNpWrFsMiMldsHwDisableIpMldSnooping;

typedef struct {
    UINT4  u4Instance;
} tMldsRemoteNpWrFsMiMldsHwDisableMldSnooping;

typedef struct {
    UINT4  u4Instance;
} tMldsRemoteNpWrFsMiMldsHwEnableIpMldSnooping;

typedef struct {
    UINT4  u4Instance;
} tMldsRemoteNpWrFsMiMldsHwEnableMldSnooping;

typedef struct {
    UINT4         u4Instance;
    tSnoopVlanId  VlanId;
    UINT1         grpAddr;
    UINT1         srcAddr;
} tMldsRemoteNpWrFsMiNpGetIp6FwdEntryHitBitStatus;

typedef struct {
    UINT4         u4Instance;
    tSnoopVlanId  VlanId;
    UINT1         grpAddr;
    UINT1         srcAddr;
    tPortList     PortList;
    tPortList     UntagPortList;
    UINT1         u1EventType;
    UINT1         au1Pad[3];
} tMldsRemoteNpWrFsMiNpUpdateIp6mcFwdEntries;

typedef struct MldsRemoteNpModInfo {
union {
    tMldsRemoteNpWrFsMiMldsHwDisableIpMldSnooping  sFsMiMldsHwDisableIpMldSnooping;
    tMldsRemoteNpWrFsMiMldsHwDisableMldSnooping  sFsMiMldsHwDisableMldSnooping;
    tMldsRemoteNpWrFsMiMldsHwEnableIpMldSnooping  sFsMiMldsHwEnableIpMldSnooping;
    tMldsRemoteNpWrFsMiMldsHwEnableMldSnooping  sFsMiMldsHwEnableMldSnooping;
    tMldsRemoteNpWrFsMiNpGetIp6FwdEntryHitBitStatus  sFsMiNpGetIp6FwdEntryHitBitStatus;
    tMldsRemoteNpWrFsMiNpUpdateIp6mcFwdEntries  sFsMiNpUpdateIp6mcFwdEntries;
    }unOpCode;

#define  MldsRemoteNpFsMiMldsHwDisableIpMldSnooping  unOpCode.sFsMiMldsHwDisableIpMldSnooping;
#define  MldsRemoteNpFsMiMldsHwDisableMldSnooping  unOpCode.sFsMiMldsHwDisableMldSnooping;
#define  MldsRemoteNpFsMiMldsHwEnableIpMldSnooping  unOpCode.sFsMiMldsHwEnableIpMldSnooping;
#define  MldsRemoteNpFsMiMldsHwEnableMldSnooping  unOpCode.sFsMiMldsHwEnableMldSnooping;
#define  MldsRemoteNpFsMiNpGetIp6FwdEntryHitBitStatus  unOpCode.sFsMiNpGetIp6FwdEntryHitBitStatus;
#define  MldsRemoteNpFsMiNpUpdateIp6mcFwdEntries  unOpCode.sFsMiNpUpdateIp6mcFwdEntries;
}tMldsRemoteNpModInfo;

#endif /* MLDS_WANTED */
#ifdef ISS_WANTED
typedef struct {
    UINT4  u4IfIndex;
    UINT1  u1EgressEnable;
    UINT1  au1Pad[3];
} tIsssysRemoteNpWrIssHwSetPortEgressStatus;

typedef struct {
    UINT4  u4IfIndex;
    UINT1  u1StatsEnable;
    UINT1  au1Pad[3];
} tIsssysRemoteNpWrIssHwSetPortStatsCollection;

typedef struct {
    UINT4  u4IfIndex;
    INT4   i4Value;
} tIsssysRemoteNpWrIssHwSetPortMode;

typedef struct {
    UINT4  u4IfIndex;
    INT4   i4Value;
} tIsssysRemoteNpWrIssHwSetPortDuplex;

typedef struct {
    UINT4  u4IfIndex;
    INT4   i4Value;
} tIsssysRemoteNpWrIssHwSetPortSpeed;

typedef struct {
    UINT4  u4IfIndex;
    UINT1  u1FlowCtrlEnable;
    UINT1  au1Pad[3];
} tIsssysRemoteNpWrIssHwSetPortFlowControl;

typedef struct {
    UINT4  u4IfIndex;
    INT4   i4Value;
} tIsssysRemoteNpWrIssHwSetPortRenegotiate;

typedef struct {
    UINT4  u4IfIndex;
    INT4   i4Value;
} tIsssysRemoteNpWrIssHwSetPortMaxMacAddr;

typedef struct {
    UINT4  u4IfIndex;
    INT4   i4Value;
} tIsssysRemoteNpWrIssHwSetPortMaxMacAction;

typedef struct {
    INT4  i4MirrorStatus;
} tIsssysRemoteNpWrIssHwSetPortMirroringStatus;

typedef struct {
    UINT4    u4IssCpuMirrorToPort;
    INT4     i4IssCpuMirrorType;
}tIsssysRemoteNpWrIssHwSetCpuMirroring;

typedef struct {
    UINT4  u4IfIndex;
} tIsssysRemoteNpWrIssHwSetMirrorToPort;

typedef struct {
    UINT4  u4IfIndex;
    INT4   i4IngMirroring;
} tIsssysRemoteNpWrIssHwSetIngressMirroring;

typedef struct {
    UINT4  u4IfIndex;
    INT4   i4EgrMirroring;
} tIsssysRemoteNpWrIssHwSetEgressMirroring;

typedef struct {
    UINT4  u4IfIndex;
    INT4   i4RateLimitVal;
    UINT1  u1PacketType;
    UINT1  au1Pad[3];
} tIsssysRemoteNpWrIssHwSetRateLimitingValue;

typedef struct {
    UINT4  u4IfIndex;
    INT4   i4PktRate;
    INT4   i4BurstRate;
} tIsssysRemoteNpWrIssHwSetPortEgressPktRate;

typedef struct {
    UINT4   u4Port;
    INT4   i4PortPktRate;
    INT4   i4PortBurstRate;
} tIsssysRemoteNpWrIssHwGetPortEgressPktRate;

typedef struct {
    UINT4   u4IfIndex;
    INT4   i4PortDuplexStatus;
} tIsssysRemoteNpWrIssHwGetPortDuplex;

typedef struct {
    UINT4   u4IfIndex;
    INT4   i4PortSpeed;
} tIsssysRemoteNpWrIssHwGetPortSpeed;

typedef struct {
    UINT4   u4IfIndex;
    INT4   i4PortFlowControl;
} tIsssysRemoteNpWrIssHwGetPortFlowControl;

typedef struct {
    UINT4  u4IfIndex;
    INT4   i4Value;
} tIsssysRemoteNpWrIssHwSetPortHOLBlockPrevention;

typedef struct {
    UINT4   u4IfIndex;
    INT4   i4Value;
} tIsssysRemoteNpWrIssHwGetPortHOLBlockPrevention;

typedef struct RemoteIssL2FilterEntry{

   tIssSllNode          IssNextNode;
   tIssPriorityFilterEntry   PriorityNode;
   tIssSllNode          IssSortedNextNode;
   INT4                 i4IssL2FilterNo;
   INT4                 i4IssL2NextFilterNo; /*Acl Filter number of L2/L3/UDB type*/
   INT4                 i4IssL2NextFilterType; /*Acl Filter of Type L2/L3/UDB*/
   tIssFilterShadowTable IssFilterShadowInfo;

   INT4                 i4IssL2FilterPriority;
   INT4                 i4IssL2FilterEncapType;
   INT4                 i4IssL2FilterStatsEnabledStatus; /* Hw Statistics */
   INT4                 i4IssClearL2FilterStats; /* Clear Hw Statistics */
   UINT4                u4IssL2FilterProtocolType;
   UINT4                u4IssL2FilterCustomerVlanId;
   UINT4                u4IssL2FilterServiceVlanId;
   UINT4                u4IssL2FilterMatchCount;
   UINT4                u4StatsTransitFlag;
   UINT4                u4IssL2RedirectId;
   UINT4                u4RefCount;              /* RefCount - How Many time
                                                    used by qosxtd module
                                                    for Classification*/
   tIssFilterAction     IssL2FilterAction;
   tIssRedirectIfGrp    RedirectIfGrp;
   tMacAddr             IssL2FilterDstMacAddr;
   tMacAddr             IssL2FilterSrcMacAddr;
   tIssPortList         IssL2FilterInPortList;
   tIssPortList         IssL2FilterOutPortList;
   tIssPortChannelList  IssL2FilterInPortChannelList;
   tIssPortChannelList  IssL2FilterOutPortChannelList;
   tIssFilterRestore    FilterRestore;
   UINT2                u2InnerEtherType;
   UINT2                u2OuterEtherType;
   UINT2                u2IssL2SubActionId; /* Vlan Id to be used for Vlan Action */
   INT1                 i1IssL2FilterCVlanPriority;
   INT1                 i1IssL2FilterSVlanPriority;
   UINT1                u1IssL2FilterTagType;    /* Single Tag or Double tag */
   UINT1                u1FilterDirection;
   UINT1                u1IssL2FilterStatus;
   UINT1                u1IssL2HwStatus; /*ISS_OK: Implies Hw entry exists
                                            ISS_NOTOK implies Hw entry does not exist*/
   UINT1                u1IssL2SubAction; /* Action to Perform Nested/Modify/None */
   
   UINT1                u1PriorityFlag;
   UINT1                u1IssL2FilterCreationMode; /* Internal or External */
   INT1                 i1DropPrecedence; /* Drop precedence of the packet */
   INT1                 i1CfiDei;         /* CFI/DEI of the packet */
   UINT1                u1IssL2FilterUserPriority;
   UINT1                u1IssL2FilterTrapSent;
   UINT1                au1Pad[1];
}tRemoteIssL2FilterEntry;


typedef struct {
    tRemoteIssL2FilterEntry IssL2FilterEntry;
    INT4                 i4Value;
} tIsssysRemoteNpWrIssHwUpdateL2Filter;

typedef struct RemoteIssL3FilterEntry{

   tIssSllNode          IssNextNode;
   tIssPriorityFilterEntry   PriorityNode;
   tIssSllNode          IssSortedNextNode;
   INT4                 i4IssL3FilterNo;
   INT4                 i4IssL3FilterPriority;
   tIssFilterProtocol   IssL3FilterProtocol;
   tIssFilterShadowTable IssFilterShadowInfo;   /* ACL Filter Shadow info to
                                                   store Hw Handle information */
   INT4                 i4IssL3FilterMessageType;
   INT4                 i4IssL3FilterMessageCode;
   UINT4                u4CVlanId;
   UINT4                u4SVlanId;
   UINT4                u4IssL3FilterDstIpAddr;
   UINT4                u4IssL3FilterSrcIpAddr;
   UINT4                u4IssL3FilterDstIpAddrMask;
   UINT4                u4IssL3FilterSrcIpAddrMask;
   UINT4                u4IssL3FilterMinDstProtPort;
   UINT4                u4IssL3FilterMaxDstProtPort;
   UINT4                u4IssL3FilterMinSrcProtPort;
   UINT4                u4IssL3FilterMaxSrcProtPort;
   tIssAckBit           IssL3FilterAckBit;
   tIssRstBit           IssL3FilterRstBit;
   tIssTos              IssL3FilterTos;
   INT4                 i4IssL3FilterDscp;
   tIssDirection        IssL3FilterDirection;
   tIssFilterAction     IssL3FilterAction;
   tIssRedirectIfGrp    RedirectIfGrp;
   UINT4                u4IssL3FilterMatchCount;
   UINT4                u4StatsTransitFlag;
   UINT4                u4RefCount;              /* RefCount - How Many time
                                                    used by qosxtd module
                                                    for Classification*/
   tIssPortList         IssL3FilterInPortList;
   tIssPortList         IssL3FilterOutPortList;
   tIssPortChannelList  IssL3FilterInPortChannelList;
   tIssPortChannelList  IssL3FilterOutPortChannelList;
   tIssFilterRestore    FilterRestore;
   UINT4                u4IssL3FilterMultiFieldClfrDstPrefixLength;   
                                                    /*Ref:stdqos.mib*/
   UINT4                u4IssL3FilterMultiFieldClfrSrcPrefixLength;/*Ref:stdqos.mib*/
   UINT4                u4IssL3MultiFieldClfrFlowId;           /*Ref:stdqos.mib*/
   UINT4             u4IssL3RedirectId;
   INT4                 i4IssL3MultiFieldClfrAddrType;         /*Ref:stdqos.mib*/
   INT4                 i4StorageType;                       /*Ref:stdqos.mib*/
   INT4                 i4IssL3FilterStatsEnabledStatus; /* Hw Statistics */
   INT4                 i4IssClearL3FilterStats; /* Clear Hw Statistics */
   tIp6Addr             ipv6SrcIpAddress;    /*Ref:stdqos.mib ace IP address */
   tIp6Addr             ipv6DstIpAddress;
   UINT2                u2IssL3SubActionId;  /* Vlan Id to be used for Vlan Action */

   INT1                 i1CVlanPriority;
   INT1                 i1SVlanPriority;
   UINT1                u1IssL3FilterTagType;    /* Single Tag or Double tag */
   UINT1                u1IssL3FilterStatus;
   UINT1                u1IssL3HwStatus; /*ISS_OK: Implies Hw entry exists
                                            ISS_NOTOK implies Hw entry does not exist*/
   UINT1                u1IssL3SubAction;  /* Action to Perform Nested/Modify/None */
   UINT1                u1PriorityFlag;
   UINT1                u1IssL3FilterCreationMode; /* Internal or External */
   UINT1                u1IssL3FilterTrapSent;
   UINT1                au1Pad[1];
}tRemoteIssL3FilterEntry;

typedef struct {
    tRemoteIssL3FilterEntry   IssL3FilterEntry;
    INT4                 i4Value;
} tIsssysRemoteNpWrIssHwUpdateL3Filter;

typedef struct {
    tIssL4SFilterEntry   IssL4SFilterEntry;
    INT4                  i4Value;
} tIsssysRemoteNpWrIssHwUpdateL4SFilter;

typedef struct {
    UINT4    u4Len;
    INT4     i4ImageType;
    UINT1    buffer;
    UINT1    au1Pad[3];
} tIsssysRemoteNpWrIssHwSendBufferToLinux;

typedef struct {
    INT4   i4CurrTemperature;
} tIsssysRemoteNpWrIssHwGetCurrTemperature;

typedef struct {
    UINT4   u4CurrPowerSupply;
} tIsssysRemoteNpWrIssHwGetCurrPowerSupply;

typedef struct {
    UINT4    u4FanIndex;
    UINT4    u4FanStatus;
} tIsssysRemoteNpWrIssHwGetFanStatus;

typedef struct {
    tIssHwMirrorInfo   MirrorInfo;
} tIsssysRemoteNpWrIssHwSetMirroring;

typedef struct {
    UINT4  u4SrcIndex;
    UINT4  u4DestIndex;
    UINT1  u1Mode;
    UINT1  u1MirrCfg;
    UINT1  au1Pad[2];
} tIsssysRemoteNpWrIssHwMirrorAddRemovePort;

typedef struct {
    INT4  i4IssMacLearnLimitVal;
} tIsssysRemoteNpWrIssHwSetMacLearningRateLimit;

typedef struct {
    INT4   i4LearnedMacAddrCount;
} tIsssysRemoteNpWrIssHwGetLearnedMacAddrCount;

typedef struct {
    tIssUDBFilterEntry   AccessFilterEntry;
    tRemoteIssL2FilterEntry    L2AccessFilterEntry;
    tRemoteIssL3FilterEntry    L3AccessFilterEntry;
    INT4                  i4FilterAction;
} tIsssysRemoteNpWrIssHwUpdateUserDefinedFilter;

typedef struct {
    UINT4   u4IfIndex;
    INT4   i4MauCapBits;
    INT4   i4IssCapBits;
} tIsssysRemoteNpWrIssHwSetPortAutoNegAdvtCapBits;

typedef struct {
    UINT4   u4IfIndex;
    INT4   element;
} tIsssysRemoteNpWrIssHwGetPortAutoNegAdvtCapBits;

typedef struct {
    UINT4  u4IfIndex;
    INT4   i4Status;
} tIsssysRemoteNpWrIssHwSetLearningMode;

typedef struct {
    UINT4  u4IfIndex;
    INT4   i4Value;
} tIsssysRemoteNpWrIssHwSetPortMdiOrMdixCap;

typedef struct {
    UINT4   u4IfIndex;
    INT4   i4PortStatus;
} tIsssysRemoteNpWrIssHwGetPortMdiOrMdixCap;

typedef struct {
    UINT4  u4IfIndex;
    INT4   i4PortMaxRate;
    INT4   i4PortMinRate;
} tIsssysRemoteNpWrIssHwSetPortFlowControlRate;

typedef struct {
    tIssHwUpdtPortIsolation   PortIsolation;
} tIsssysRemoteNpWrIssHwConfigPortIsolationEntry;

typedef struct {
    UINT1  u1TraceModule;
    UINT1  u1TraceLevel;
    UINT1  au1Pad[2];
} tIsssysRemoteNpWrNpSetTrace;

typedef struct {
    UINT1  u1TraceModule;
    UINT1  u1TraceLevel;
    UINT1   au1Pad[2];
} tIsssysRemoteNpWrNpGetTrace;
#ifdef NPAPI_WANTED
typedef struct {
    UINT2  u2TraceLevel;
    UINT1  au1Pad[2];
} tIsssysRemoteNpWrNpSetTraceLevel;

typedef struct {
    UINT2   u2TraceLevel;
    UINT1   au1Pad[2];
} tIsssysRemoteNpWrNpGetTraceLevel;
#endif
typedef struct IsssysRemoteNpModInfo {
union {
    tIsssysRemoteNpWrIssHwSetPortEgressStatus  sIssHwSetPortEgressStatus;
    tIsssysRemoteNpWrIssHwSetPortStatsCollection  sIssHwSetPortStatsCollection;
    tIsssysRemoteNpWrIssHwSetPortMode  sIssHwSetPortMode;
    tIsssysRemoteNpWrIssHwSetPortDuplex  sIssHwSetPortDuplex;
    tIsssysRemoteNpWrIssHwSetPortSpeed  sIssHwSetPortSpeed;
    tIsssysRemoteNpWrIssHwSetPortFlowControl  sIssHwSetPortFlowControl;
    tIsssysRemoteNpWrIssHwSetPortRenegotiate  sIssHwSetPortRenegotiate;
    tIsssysRemoteNpWrIssHwSetPortMaxMacAddr  sIssHwSetPortMaxMacAddr;
    tIsssysRemoteNpWrIssHwSetPortMaxMacAction  sIssHwSetPortMaxMacAction;
    tIsssysRemoteNpWrIssHwSetPortMirroringStatus  sIssHwSetPortMirroringStatus;
    tIsssysRemoteNpWrIssHwSetMirrorToPort  sIssHwSetMirrorToPort;
    tIsssysRemoteNpWrIssHwSetIngressMirroring  sIssHwSetIngressMirroring;
    tIsssysRemoteNpWrIssHwSetEgressMirroring  sIssHwSetEgressMirroring;
    tIsssysRemoteNpWrIssHwSetRateLimitingValue  sIssHwSetRateLimitingValue;
    tIsssysRemoteNpWrIssHwSetPortEgressPktRate  sIssHwSetPortEgressPktRate;
    tIsssysRemoteNpWrIssHwGetPortEgressPktRate  sIssHwGetPortEgressPktRate;
    tIsssysRemoteNpWrIssHwGetPortDuplex  sIssHwGetPortDuplex;
    tIsssysRemoteNpWrIssHwGetPortSpeed  sIssHwGetPortSpeed;
    tIsssysRemoteNpWrIssHwGetPortFlowControl  sIssHwGetPortFlowControl;
    tIsssysRemoteNpWrIssHwSetPortHOLBlockPrevention  sIssHwSetPortHOLBlockPrevention;
    tIsssysRemoteNpWrIssHwGetPortHOLBlockPrevention  sIssHwGetPortHOLBlockPrevention;
    tIsssysRemoteNpWrIssHwUpdateL2Filter  sIssHwUpdateL2Filter;
    tIsssysRemoteNpWrIssHwUpdateL3Filter  sIssHwUpdateL3Filter;
    tIsssysRemoteNpWrIssHwUpdateL4SFilter  sIssHwUpdateL4SFilter;
    tIsssysRemoteNpWrIssHwSendBufferToLinux  sIssHwSendBufferToLinux;
    tIsssysRemoteNpWrIssHwGetCurrTemperature  sIssHwGetCurrTemperature;
    tIsssysRemoteNpWrIssHwGetCurrPowerSupply  sIssHwGetCurrPowerSupply;
    tIsssysRemoteNpWrIssHwGetFanStatus  sIssHwGetFanStatus;
    tIsssysRemoteNpWrIssHwSetMirroring  sIssHwSetMirroring;
    tIsssysRemoteNpWrIssHwMirrorAddRemovePort  sIssHwMirrorAddRemovePort;
    tIsssysRemoteNpWrIssHwSetMacLearningRateLimit  sIssHwSetMacLearningRateLimit;
    tIsssysRemoteNpWrIssHwGetLearnedMacAddrCount  sIssHwGetLearnedMacAddrCount;
    tIsssysRemoteNpWrIssHwUpdateUserDefinedFilter  sIssHwUpdateUserDefinedFilter;
    tIsssysRemoteNpWrIssHwSetPortAutoNegAdvtCapBits  sIssHwSetPortAutoNegAdvtCapBits;
    tIsssysRemoteNpWrIssHwGetPortAutoNegAdvtCapBits  sIssHwGetPortAutoNegAdvtCapBits;
    tIsssysRemoteNpWrIssHwSetLearningMode  sIssHwSetLearningMode;
    tIsssysRemoteNpWrIssHwSetPortMdiOrMdixCap  sIssHwSetPortMdiOrMdixCap;
    tIsssysRemoteNpWrIssHwGetPortMdiOrMdixCap  sIssHwGetPortMdiOrMdixCap;
    tIsssysRemoteNpWrIssHwSetPortFlowControlRate  sIssHwSetPortFlowControlRate;
    tIsssysRemoteNpWrIssHwConfigPortIsolationEntry  sIssHwConfigPortIsolationEntry;
    tIsssysRemoteNpWrNpSetTrace  sNpSetTrace;
    tIsssysRemoteNpWrNpGetTrace  sNpGetTrace;
#ifdef NPAPI_WANTED
    tIsssysRemoteNpWrNpSetTraceLevel  sNpSetTraceLevel;
    tIsssysRemoteNpWrNpGetTraceLevel  sNpGetTraceLevel;
#endif
    tIsssysRemoteNpWrIssHwSetCpuMirroring sIssHwSetCpuMirroring;
    }unOpCode;

#define  IsssysRemoteNpIssHwSetPortEgressStatus  unOpCode.sIssHwSetPortEgressStatus;
#define  IsssysRemoteNpIssHwSetPortStatsCollection  unOpCode.sIssHwSetPortStatsCollection;
#define  IsssysRemoteNpIssHwSetPortMode  unOpCode.sIssHwSetPortMode;
#define  IsssysRemoteNpIssHwSetPortDuplex  unOpCode.sIssHwSetPortDuplex;
#define  IsssysRemoteNpIssHwSetPortSpeed  unOpCode.sIssHwSetPortSpeed;
#define  IsssysRemoteNpIssHwSetPortFlowControl  unOpCode.sIssHwSetPortFlowControl;
#define  IsssysRemoteNpIssHwSetPortRenegotiate  unOpCode.sIssHwSetPortRenegotiate;
#define  IsssysRemoteNpIssHwSetPortMaxMacAddr  unOpCode.sIssHwSetPortMaxMacAddr;
#define  IsssysRemoteNpIssHwSetPortMaxMacAction  unOpCode.sIssHwSetPortMaxMacAction;
#define  IsssysRemoteNpIssHwSetPortMirroringStatus  unOpCode.sIssHwSetPortMirroringStatus;
#define  IsssysRemoteNpIssHwSetMirrorToPort  unOpCode.sIssHwSetMirrorToPort;
#define  IsssysRemoteNpIssHwSetIngressMirroring  unOpCode.sIssHwSetIngressMirroring;
#define  IsssysRemoteNpIssHwSetEgressMirroring  unOpCode.sIssHwSetEgressMirroring;
#define  IsssysRemoteNpIssHwSetRateLimitingValue  unOpCode.sIssHwSetRateLimitingValue;
#define  IsssysRemoteNpIssHwSetPortEgressPktRate  unOpCode.sIssHwSetPortEgressPktRate;
#define  IsssysRemoteNpIssHwGetPortEgressPktRate  unOpCode.sIssHwGetPortEgressPktRate;
#define  IsssysRemoteNpIssHwGetPortDuplex  unOpCode.sIssHwGetPortDuplex;
#define  IsssysRemoteNpIssHwGetPortSpeed  unOpCode.sIssHwGetPortSpeed;
#define  IsssysRemoteNpIssHwGetPortFlowControl  unOpCode.sIssHwGetPortFlowControl;
#define  IsssysRemoteNpIssHwSetPortHOLBlockPrevention  unOpCode.sIssHwSetPortHOLBlockPrevention;
#define  IsssysRemoteNpIssHwGetPortHOLBlockPrevention  unOpCode.sIssHwGetPortHOLBlockPrevention;
#define  IsssysRemoteNpIssHwUpdateL2Filter  unOpCode.sIssHwUpdateL2Filter;
#define  IsssysRemoteNpIssHwUpdateL3Filter  unOpCode.sIssHwUpdateL3Filter;
#define  IsssysRemoteNpIssHwUpdateL4SFilter  unOpCode.sIssHwUpdateL4SFilter;
#define  IsssysRemoteNpIssHwSendBufferToLinux  unOpCode.sIssHwSendBufferToLinux;
#define  IsssysRemoteNpIssHwGetCurrTemperature  unOpCode.sIssHwGetCurrTemperature;
#define  IsssysRemoteNpIssHwGetCurrPowerSupply  unOpCode.sIssHwGetCurrPowerSupply;
#define  IsssysRemoteNpIssHwGetFanStatus  unOpCode.sIssHwGetFanStatus;
#define  IsssysRemoteNpIssHwSetMirroring  unOpCode.sIssHwSetMirroring;
#define  IsssysRemoteNpIssHwMirrorAddRemovePort  unOpCode.sIssHwMirrorAddRemovePort;
#define  IsssysRemoteNpIssHwSetMacLearningRateLimit  unOpCode.sIssHwSetMacLearningRateLimit;
#define  IsssysRemoteNpIssHwGetLearnedMacAddrCount  unOpCode.sIssHwGetLearnedMacAddrCount;
#define  IsssysRemoteNpIssHwUpdateUserDefinedFilter  unOpCode.sIssHwUpdateUserDefinedFilter;
#define  IsssysRemoteNpIssHwSetPortAutoNegAdvtCapBits  unOpCode.sIssHwSetPortAutoNegAdvtCapBits;
#define  IsssysRemoteNpIssHwGetPortAutoNegAdvtCapBits  unOpCode.sIssHwGetPortAutoNegAdvtCapBits;
#define  IsssysRemoteNpIssHwSetLearningMode  unOpCode.sIssHwSetLearningMode;
#define  IsssysRemoteNpIssHwSetPortMdiOrMdixCap  unOpCode.sIssHwSetPortMdiOrMdixCap;
#define  IsssysRemoteNpIssHwGetPortMdiOrMdixCap  unOpCode.sIssHwGetPortMdiOrMdixCap;
#define  IsssysRemoteNpIssHwSetPortFlowControlRate  unOpCode.sIssHwSetPortFlowControlRate;
#define  IsssysRemoteNpIssHwConfigPortIsolationEntry  unOpCode.sIssHwConfigPortIsolationEntry;
#define  IsssysRemoteNpNpSetTrace  unOpCode.sNpSetTrace;
#define  IsssysRemoteNpNpGetTrace  unOpCode.sNpGetTrace;
#ifdef NPAPI_WANTED
#define  IsssysRemoteNpNpSetTraceLevel  unOpCode.sNpSetTraceLevel;
#define  IsssysRemoteNpNpGetTraceLevel  unOpCode.sNpGetTraceLevel;
#endif
#define  IsssysRemoteNpWrIssHwSetCpuMirroring unOpCode.sIssHwSetCpuMirroring;
} tIsssysRemoteNpModInfo;
#endif /* ISS_WANTED */
#ifdef ERPS_WANTED
typedef struct {
    tVlanList                     VlanList;
    tErpsLspPwInfo                ErpsLspPwInfo;
    UINT4                         u4ContextId;
    UINT4                         u4Port1IfIndex;
    UINT4                         u4Port2IfIndex;
    UINT4                         u4RingId;
    INT4                          ai4HwRingHandle[ERPS_MAX_HW_HANDLE];
    tVlanId                       VlanId;
    UINT2                         u2VlanGroupId;
    UINT1                         u1Port1Action; 
    UINT1                         u1Port2Action;
    UINT1                         u1RingAction;
    UINT1                         u1ProtectionType;
    UINT1                         u1HwUpdatedPort;
                              /* This field tells which port needs to be
                               * programmed in the Hardware.
                               * This can be filled with
                               * 1.PORT1_UPDATED
                               * 2.PORT2_UPDATED
                               * 3.PORT1_PORT2_UPDATED
                               */ 
    UINT1                         u1UnBlockRAPSChannel;
    /*This object is set to OSIX_ENABLED when SubRing without RAPS Virtual 
     * channel is configured ; By default this object will be set to 
     * OSIX_DISABLED (Sub Ring with Virtual Channel */
    UINT1                     u1HighestPriorityRequest;
                              /* This field represent the Highest Priority event
                               * identified by Priority Logic. */
    UINT1                         u1Service;
    /* NOTE : A new entry added inside this structure should be properly synced inside
     * the functions ErpsSyncNpFsErpsHwRingConfigSync and ErpsSyncNpProcessSyncMsg
     * in erpssync.c - Also the value of macro ERPS_RED_NP_SYNC_MSG_SIZE should be
     * adjusted accordingly .
     * Failing to do so, will affect the ERPS HA functionality */

}tRemoteErpsHwRingInfo;

/* Required arguments list for the erps NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

typedef struct {
    tRemoteErpsHwRingInfo   ErpsHwRingInfo;
} tErpsRemoteNpWrFsErpsHwRingConfig;

typedef struct ErpsRemoteNpModInfo {
union {
    tErpsRemoteNpWrFsErpsHwRingConfig  sFsErpsHwRingConfig;
    }unOpCode;

#define  ErpsRemoteNpFsErpsHwRingConfig  unOpCode.sFsErpsHwRingConfig;
} tErpsRemoteNpModInfo;

#endif /* ERPS_WANTED */
#ifdef LA_WANTED
typedef struct {
    UINT4    u4AggIndex;
    UINT2    u2HwAggId;
    UINT1    au1Pad[2];
} tLaRemoteNpWrFsLaHwCreateAggGroup;

typedef struct {
    UINT4    u4PortNumber;
    UINT2   u2HwAggId;
    UINT1    au1Pad[2];
    UINT4    u4AggIndex;
} tLaRemoteNpWrFsLaHwAddLinkToAggGroup;

typedef struct {
    UINT4  u4AggIndex;
    UINT1  u1SelectionPolicy;
    UINT1    au1Pad[3];
} tLaRemoteNpWrFsLaHwSetSelectionPolicy;

typedef struct {
    UINT4  u4AggIndex;
    UINT4  u4SelectionPolicyBitList;
} tLaRemoteNpWrFsLaHwSetSelectionPolicyBitList;

typedef struct {
    UINT4  u4PortNumber;
    UINT4  u4AggIndex;
} tLaRemoteNpWrFsLaHwRemoveLinkFromAggGroup;

typedef struct {
    UINT4  u4AggIndex;
} tLaRemoteNpWrFsLaHwDeleteAggregator;

typedef struct {
    UINT4  u4PortNumber;
    UINT4  u4AggIndex;
} tLaRemoteNpWrFsLaHwEnableCollection;

typedef struct {
    UINT4  u4PortNumber;
    UINT4  u4AggIndex;
} tLaRemoteNpWrFsLaHwEnableDistribution;

typedef struct {
    UINT4  u4PortNumber;
    UINT4  u4AggIndex;
} tLaRemoteNpWrFsLaHwDisableCollection;

typedef struct {
    UINT4  u4AggIndex;
    UINT2  u2Inst;
    UINT1  u1StpState;
    UINT1    au1Pad[1];
} tLaRemoteNpWrFsLaHwSetPortChannelStatus;

typedef struct {
    UINT4  u4PortNumber;
    UINT4  u4AggIndex;
} tLaRemoteNpWrFsLaHwAddPortToConfAggGroup;

typedef struct {
    UINT4  u4PortNumber;
    UINT4  u4AggIndex;
} tLaRemoteNpWrFsLaHwRemovePortFromConfAggGroup;

typedef struct {
    UINT4    u4PortNumber;
    UINT2 *  pu2HwAggId;
    UINT4    u4AggIndex;
} tLaRemoteNpWrFsLaHwDlagAddLinkToAggGroup;

typedef struct {
    UINT4  u4PortNumber;
    UINT4  u4AggIndex;
} tLaRemoteNpWrFsLaHwDlagRemoveLinkFromAggGroup;


typedef struct {
    UINT1 u1DlagStatus;
    UINT1    au1Pad[3];
}tLaRemoteNpWrFsLaHwDlagStatus;

#ifdef L2RED_WANTED
typedef struct {
    UINT4  u4HwAggIndex;
} tLaRemoteNpWrFsLaHwCleanAndDeleteAggregator;

typedef struct {
    tLaHwInfo    HwInfo;
    INT4          i4HwAggIndex;
    INT4          i4NextHwAggIndex;
} tLaRemoteNpWrFsLaGetNextAggregator;

typedef struct {
    UINT4  u4AggIndex;
} tLaRemoteNpWrFsLaRedHwNpDeleteAggregator;

typedef struct {
    tPortList  ConfigPorts;
    tPortList  ActivePorts;
    UINT4      u4AggIndex;
    UINT1      u1SelectionPolicy;
    UINT1      au1Pad[3];
} tLaRemoteNpWrFsLaRedHwUpdateDBForAggregator;

typedef struct {
    UINT4  u4AggIndex;
} tLaRemoteNpWrFsLaRedHwNpUpdateDlfMcIpmcPort;
#endif /* L2RED_WANTED */
typedef struct LaRemoteNpModInfo {
union {
    tLaRemoteNpWrFsLaHwCreateAggGroup  sFsLaHwCreateAggGroup;
    tLaRemoteNpWrFsLaHwAddLinkToAggGroup  sFsLaHwAddLinkToAggGroup;
    tLaRemoteNpWrFsLaHwSetSelectionPolicy  sFsLaHwSetSelectionPolicy;
    tLaRemoteNpWrFsLaHwSetSelectionPolicyBitList  sFsLaHwSetSelectionPolicyBitList;
    tLaRemoteNpWrFsLaHwRemoveLinkFromAggGroup  sFsLaHwRemoveLinkFromAggGroup;
    tLaRemoteNpWrFsLaHwDeleteAggregator  sFsLaHwDeleteAggregator;
    tLaRemoteNpWrFsLaHwEnableCollection  sFsLaHwEnableCollection;
    tLaRemoteNpWrFsLaHwEnableDistribution  sFsLaHwEnableDistribution;
    tLaRemoteNpWrFsLaHwDisableCollection  sFsLaHwDisableCollection;
    tLaRemoteNpWrFsLaHwSetPortChannelStatus  sFsLaHwSetPortChannelStatus;
    tLaRemoteNpWrFsLaHwAddPortToConfAggGroup  sFsLaHwAddPortToConfAggGroup;
    tLaRemoteNpWrFsLaHwRemovePortFromConfAggGroup  sFsLaHwRemovePortFromConfAggGroup;
    tLaRemoteNpWrFsLaHwDlagAddLinkToAggGroup       sFsLaHwDlagAddLinkToAggGroup;
    tLaRemoteNpWrFsLaHwDlagRemoveLinkFromAggGroup  sFsLaHwDlagRemoveLinkFromAggGroup;
    tLaRemoteNpWrFsLaHwDlagStatus  sFsLaHwDlagStatus;
#ifdef L2RED_WANTED
    tLaRemoteNpWrFsLaHwCleanAndDeleteAggregator  sFsLaHwCleanAndDeleteAggregator;
    tLaRemoteNpWrFsLaGetNextAggregator  sFsLaGetNextAggregator;
    tLaRemoteNpWrFsLaRedHwNpDeleteAggregator  sFsLaRedHwNpDeleteAggregator;
    tLaRemoteNpWrFsLaRedHwUpdateDBForAggregator  sFsLaRedHwUpdateDBForAggregator;
    tLaRemoteNpWrFsLaRedHwNpUpdateDlfMcIpmcPort  sFsLaRedHwNpUpdateDlfMcIpmcPort;
#endif /* L2RED_WANTED */
    }unOpCode;

#define  LaRemoteNpFsLaHwCreateAggGroup  unOpCode.sFsLaHwCreateAggGroup;
#define  LaRemoteNpFsLaHwAddLinkToAggGroup  unOpCode.sFsLaHwAddLinkToAggGroup;
#define  LaRemoteNpFsLaHwSetSelectionPolicy  unOpCode.sFsLaHwSetSelectionPolicy;
#define  LaRemoteNpFsLaHwSetSelectionPolicyBitList  unOpCode.sFsLaHwSetSelectionPolicyBitList;
#define  LaRemoteNpFsLaHwRemoveLinkFromAggGroup  unOpCode.sFsLaHwRemoveLinkFromAggGroup;
#define  LaRemoteNpFsLaHwDeleteAggregator  unOpCode.sFsLaHwDeleteAggregator;
#define  LaRemoteNpFsLaHwEnableCollection  unOpCode.sFsLaHwEnableCollection;
#define  LaRemoteNpFsLaHwEnableDistribution  unOpCode.sFsLaHwEnableDistribution;
#define  LaRemoteNpFsLaHwDisableCollection  unOpCode.sFsLaHwDisableCollection;
#define  LaRemoteNpFsLaHwSetPortChannelStatus  unOpCode.sFsLaHwSetPortChannelStatus;
#define  LaRemoteNpFsLaHwAddPortToConfAggGroup  unOpCode.sFsLaHwAddPortToConfAggGroup;
#define  LaRemoteNpFsLaHwRemovePortFromConfAggGroup  unOpCode.sFsLaHwRemovePortFromConfAggGroup;
#define  LaRemoteNpFsLaHwDlagAddLinkToAggGroup  unOpCode.sFsLaHwDlagAddLinkToAggGroup;
#define  LaRemoteNpFsLaHwDlagRemoveLinkFromAggGroup  unOpCode.sFsLaHwDlagRemoveLinkFromAggGroup;
#define  LaRemoteNpFsLaHwDlagStatus unOpCode.sFsLaHwDlagStatus;
#ifdef L2RED_WANTED
#define  LaRemoteNpFsLaHwCleanAndDeleteAggregator  unOpCode.sFsLaHwCleanAndDeleteAggregator;
#define  LaRemoteNpFsLaGetNextAggregator  unOpCode.sFsLaGetNextAggregator;
#define  LaRemoteNpFsLaRedHwNpDeleteAggregator  unOpCode.sFsLaRedHwNpDeleteAggregator;
#define  LaRemoteNpFsLaRedHwUpdateDBForAggregator  unOpCode.sFsLaRedHwUpdateDBForAggregator;
#define  LaRemoteNpFsLaRedHwNpUpdateDlfMcIpmcPort  unOpCode.sFsLaRedHwNpUpdateDlfMcIpmcPort;
#endif /* L2RED_WANTED */
} tLaRemoteNpModInfo;

#endif /* LA_WANTED */
#ifdef RM_WANTED
typedef struct {
    UINT1  u1Event;
    UINT1  au1pad[3];
} tRmRemoteNpWrRmNpUpdateNodeState;

typedef struct {
    tRmHRAction     eAction;
    tRmHRPktInfo   RmHRPktInfo;
} tRmRemoteNpWrFsNpHRSetStdyStInfo;

typedef struct RmRemoteNpModInfo {
union {
    tRmRemoteNpWrRmNpUpdateNodeState  sRmNpUpdateNodeState;
    tRmRemoteNpWrFsNpHRSetStdyStInfo  sFsNpHRSetStdyStInfo;
    }unOpCode;

#define  RmRemoteNpRmNpUpdateNodeState  unOpCode.sRmNpUpdateNodeState;
#define  RmRemoteNpFsNpHRSetStdyStInfo  unOpCode.sFsNpHRSetStdyStInfo;
} tRmRemoteNpModInfo;
#endif /* RM_WANTED */

#ifdef MBSM_WANTED
#ifdef L2RED_WANTED
typedef struct {
    INT4   i4Event;
    UINT1  u1PrevState;
    UINT1  u1State;
    UINT1  au1pad[2];
} tMbsmRemoteNpWrMbsmNpProcRmNodeTransition;

typedef struct {
    tMbsmSlotInfo   SlotInfo;
} tMbsmRemoteNpWrMbsmInitNpInfoOnStandbyToActive;

typedef struct {
    tMbsmHwMsg   HwMsg;
    INT4         i4SlotId;
} tMbsmRemoteNpWrMbsmNpGetSlotInfo;

#endif /* L2RED_WANTED */
typedef struct {
    INT4  i4SlotId;
    INT1  i1NodeState;
    UINT1 au1pad[3];
} tMbsmRemoteNpWrMbsmNpInitHwTopoDisc;

typedef struct {
    INT4  i4SlotId;
} tMbsmRemoteNpWrMbsmNpClearHwTbl;

typedef struct {
    INT4  i4MsgType;
} tMbsmRemoteNpWrMbsmNpProcCardInsertForLoadSharing;

typedef struct {
    UINT1  u1Flag;
    UINT1  au1pad[3];
} tMbsmRemoteNpWrMbsmNpUpdtHwTblForLoadSharing;

typedef struct {
    INT4  i4LoadSharingFlag;
} tMbsmRemoteNpWrMbsNpSetLoadSharingStatus;

typedef struct {
    tMbsmCardInfo   MbsmCardInfo;
    INT4            i4LoopIdx;
} tMbsmRemoteNpWrMbsmNpGetCardTypeTable;

typedef struct {
    INT4   i4Event;
    UINT1  u1PrevState;
    UINT1  u1State;
    UINT1  au1pad[2];
} tMbsmRemoteNpWrMbsmNpHandleNodeTransition;

typedef struct {
    INT4     i4PktSize;
    UINT1    u1Pkt;
    UINT1    au1pad[3];
} tMbsmRemoteNpWrMbsmNpTxOnStackInterface;

typedef struct {
    UINT4    u4SlotId;
    UINT1    u1MacAddr[MAC_LEN];
    UINT1    au1pad[2];
} tMbsmRemoteNpWrMbsmNpGetStackMac;

typedef struct MbsmRemoteNpModInfo {
union {
#ifdef L2RED_WANTED
    tMbsmRemoteNpWrMbsmNpProcRmNodeTransition  sMbsmNpProcRmNodeTransition;
    tMbsmRemoteNpWrMbsmInitNpInfoOnStandbyToActive  sMbsmInitNpInfoOnStandbyToActive;
    tMbsmRemoteNpWrMbsmNpGetSlotInfo  sMbsmNpGetSlotInfo;
#endif /* L2RED_WANTED */
    tMbsmRemoteNpWrMbsmNpInitHwTopoDisc  sMbsmNpInitHwTopoDisc;
    tMbsmRemoteNpWrMbsmNpClearHwTbl  sMbsmNpClearHwTbl;
    tMbsmRemoteNpWrMbsmNpProcCardInsertForLoadSharing  sMbsmNpProcCardInsertForLoadSharing;
    tMbsmRemoteNpWrMbsmNpUpdtHwTblForLoadSharing  sMbsmNpUpdtHwTblForLoadSharing;
    tMbsmRemoteNpWrMbsNpSetLoadSharingStatus  sMbsNpSetLoadSharingStatus;
    tMbsmRemoteNpWrMbsmNpGetCardTypeTable  sMbsmNpGetCardTypeTable;
    tMbsmRemoteNpWrMbsmNpHandleNodeTransition  sMbsmNpHandleNodeTransition;
    tMbsmRemoteNpWrMbsmNpTxOnStackInterface  sMbsmNpTxOnStackInterface;
    tMbsmRemoteNpWrMbsmNpGetStackMac  sMbsmNpGetStackMac;
    }unOpCode;

#ifdef L2RED_WANTED
#define  MbsmRemoteNpMbsmNpProcRmNodeTransition  unOpCode.sMbsmNpProcRmNodeTransition;
#define  MbsmRemoteNpMbsmInitNpInfoOnStandbyToActive  unOpCode.sMbsmInitNpInfoOnStandbyToActive;
#define  MbsmRemoteNpMbsmNpGetSlotInfo  unOpCode.sMbsmNpGetSlotInfo;
#endif /* L2RED_WANTED */
#define  MbsmRemoteNpMbsmNpInitHwTopoDisc  unOpCode.sMbsmNpInitHwTopoDisc;
#define  MbsmRemoteNpMbsmNpClearHwTbl  unOpCode.sMbsmNpClearHwTbl;
#define  MbsmRemoteNpMbsmNpProcCardInsertForLoadSharing  unOpCode.sMbsmNpProcCardInsertForLoadSharing;
#define  MbsmRemoteNpMbsmNpUpdtHwTblForLoadSharing  unOpCode.sMbsmNpUpdtHwTblForLoadSharing;
#define  MbsmRemoteNpMbsNpSetLoadSharingStatus  unOpCode.sMbsNpSetLoadSharingStatus;
#define  MbsmRemoteNpMbsmNpGetCardTypeTable  unOpCode.sMbsmNpGetCardTypeTable;
#define  MbsmRemoteNpMbsmNpHandleNodeTransition  unOpCode.sMbsmNpHandleNodeTransition;
#define  MbsmRemoteNpMbsmNpTxOnStackInterface  unOpCode.sMbsmNpTxOnStackInterface;
#define  MbsmRemoteNpMbsmNpGetStackMac  unOpCode.sMbsmNpGetStackMac;
} tMbsmRemoteNpModInfo;
#endif /* MBSM_WANTED */


#ifdef QOSX_WANTED
/* Class Map Table Entry */
typedef struct RemoteQoSMultiFieldClassMapEntry
{
    tRemoteIssL2FilterEntry L2FilterPtr;
    tRemoteIssL3FilterEntry L3FilterPtr;
    tQoSPriorityMapEntry   PriorityMapPtr;/*  Ptr to priority-map entry */
    UINT4                  u4QoSMFClass;    /* CLASS for the class-map    */
    UINT1                  u1QoSMFCStatus;  /* class-map entry status     */
    UINT1                  u1PreColor;      /* PreColor                   */
    UINT1                  au1Pack[2];       /* packing                    */

} tRemoteQoSClassMapEntry;

/* Scheduler Entry */
typedef struct RemoteQoSSchedulerEntry
{
    tQoSShapeCfgEntry  ShapePtr;
    INT4               i4QosIfIndex;
    INT4               i4QosSchedulerId;
    UINT4              u4QosSchedHwId; /* Hardware Id created in the hardware for
     * schedulers when  BCM56840 devices are used */
    UINT4              u4QosSchedChildren;
    UINT1              u1QosSchedAlgo;
    UINT1              u1QosSchedulerStatus;
    UINT1              u1HL;
    UINT1              u1Flag;

} tRemoteQoSSchedulerEntry;

/* Q Table Entry */
typedef struct RemoteQoSQEntry
{
    tQoSShapeCfgEntry           ShapePtr;
    tRemoteQoSSchedulerEntry    SchedPtr;
    INT4                        i4QosQId;
    INT4                        i4QosSchedulerId;
    INT4                        i4QosQueueHwId;
    UINT4                       u4QueueType;  /* Type of Queue Unicast Queue or Multicast Queue */
    UINT2                       u2QosQWeight;
    UINT1                       u1QosQPriority;
    UINT1                       u1QosQStatus;

} tRemoteQoSQEntry;

typedef struct RemoteQosClassToIntPriEntry
{
    tRemoteIssL2FilterEntry      L2FilterPtr;
    tRemoteIssL3FilterEntry      L3FilterPtr;
    UINT1 u1IntPriority; /* Internal priority for traffic class*/
    UINT1 u1Flag; /* QOS_NP_ADD/QOS_NP_DEL */
    UINT1 u1Pad[2];
} tRemoteQosClassToIntPriEntry;

typedef struct {
    tQosPfcHwEntry   QosPfcHwEntry;
} tQosxRemoteNpWrFsQosHwConfigPfc;

typedef struct {
    tRemoteQoSClassMapEntry   ClsMapEntry;
} tQosxRemoteNpWrQoSHwDeleteClassMapEntry;

typedef struct {
    UINT4                   u4DiffServId;
    UINT4                   u4StatsType;
    tSNMP_COUNTER64_TYPE   U8RetValDiffServCounter;
} tQosxRemoteNpWrQoSHwGetAlgDropStats;

typedef struct {
    INT4                    i4IfIndex;
    UINT4                   u4QId;
    UINT4                   u4StatsType;
    tSNMP_COUNTER64_TYPE   U8CoSQStatsCounter;
} tQosxRemoteNpWrQoSHwGetCoSQStats;

typedef struct {
    UINT4                   u4DiffServId;
    UINT4                   u4StatsType;
    tSNMP_COUNTER64_TYPE   U8RetValDiffServCounter;
} tQosxRemoteNpWrQoSHwGetCountActStats;

typedef struct {
    UINT4                   u4MeterId;
    UINT4                   u4StatsType;
    tSNMP_COUNTER64_TYPE   U8MeterStatsCounter;
} tQosxRemoteNpWrQoSHwGetMeterStats;

typedef struct {
    UINT4                   u4DiffServId;
    UINT4                   u4StatsType;
    tSNMP_COUNTER64_TYPE   U8RetValDiffServCounter;
} tQosxRemoteNpWrQoSHwGetRandomDropStats;

typedef struct {
    tRemoteQoSClassMapEntry           ClsMapEntry;
    tQoSPolicyMapEntry          PlyMapEntry;
    tQoSInProfileActionEntry    InProActEntry;
    tQoSOutProfileActionEntry   OutProActEntry;
    tQoSMeterEntry              MeterEntry;
    UINT1                       u1Flag;
    UINT1                       au1Pad[3];  
} tQosxRemoteNpWrQoSHwMapClassToPolicy;

typedef struct {
    INT4   i4IfIndex;
    INT4   i4ClsOrPriType;
    UINT4  u4ClsOrPri;
    UINT4  u4QId;
    UINT1  u1Flag;
    UINT1  au1Pad[3];  
} tQosxRemoteNpWrQoSHwMapClassToQueue;

typedef struct {
    tRemoteQoSClassMapEntry   ClsMapEntry;
    INT4                 i4IfIndex;
    INT4                 i4ClsOrPriType;
    UINT4                u4ClsOrPri;
    UINT4                u4QId;
    UINT1                u1Flag;
    UINT1                au1Pad[3];  
} tQosxRemoteNpWrQoSHwMapClassToQueueId;

typedef struct {
    tQoSMeterEntry   MeterEntry;
} tQosxRemoteNpWrQoSHwMeterCreate;

typedef struct {
    INT4  i4MeterId;
} tQosxRemoteNpWrQoSHwMeterDelete;

typedef struct {
    INT4               i4IfIndex;
    UINT4              u4QId;
    tRemoteQoSQEntry   QEntry;
    tQoSQtypeEntry    QTypeEntry;
    tQoSREDCfgEntry  * ApRDCfgEntry;
    INT2               i2HL;
    UINT1              au1Pad[2];
} tQosxRemoteNpWrQoSHwQueueCreate;

typedef struct {
    INT4   i4IfIndex;
    UINT4  u4Id;
} tQosxRemoteNpWrQoSHwQueueDelete;

typedef struct {
    tRemoteQoSSchedulerEntry   SchedEntry;
} tQosxRemoteNpWrQoSHwSchedulerAdd;

typedef struct {
    INT4   i4IfIndex;
    UINT4  u4SchedId;
} tQosxRemoteNpWrQoSHwSchedulerDelete;

typedef struct {
    INT4   i4IfIndex;
    UINT4  u4SchedId;
    UINT4  u4NextSchedId;
    UINT4  u4NextQId;
    INT2   i2HL;
    UINT2  u2Sweight;
    UINT1  u1Spriority;
    UINT1  u1Flag;
    UINT1  au1Pad[2];
} tQosxRemoteNpWrQoSHwSchedulerHierarchyMap;

typedef struct {
    tRemoteQoSSchedulerEntry   SchedEntry;
} tQosxRemoteNpWrQoSHwSchedulerUpdateParams;

typedef struct {
    INT4   i4CpuQueueId;
    UINT4  u4MinRate;
    UINT4  u4MaxRate;
} tQosxRemoteNpWrQoSHwSetCpuRateLimit;

typedef struct {
    INT4  i4Port;
    INT4  i4DefPriority;
} tQosxRemoteNpWrQoSHwSetDefUserPriority;

typedef struct {
    INT4  i4Port;
    INT4  i4PbitPref;
} tQosxRemoteNpWrQoSHwSetPbitPreferenceOverDscp;

typedef struct {
    tRemoteQoSClassMapEntry    ClsMapEntry;
    tQoSPolicyMapEntry   PlyMapEntry;
    tQoSMeterEntry       MeterEntry;
    UINT1                 u1Flag;
    UINT1                au1Pad[3];
} tQosxRemoteNpWrQoSHwUnmapClassFromPolicy;

typedef struct {
    tRemoteQoSClassMapEntry           ClsMapEntry;
    tQoSPolicyMapEntry          PlyMapEntry;
    tQoSInProfileActionEntry    InProActEntry;
    tQoSOutProfileActionEntry   OutProActEntry;
    tQoSMeterEntry              MeterEntry;
    UINT1                        u1Flag;
    UINT1                       au1Pad[3];
} tQosxRemoteNpWrQoSHwUpdatePolicyMapForClass;

typedef struct {
    tQosClassToPriMapEntry   QosClassToPriMapEntry;
} tQosxRemoteNpWrQoSHwMapClasstoPriMap;

typedef struct {
    tRemoteQosClassToIntPriEntry   QosClassToIntPriEntry;
} tQosxRemoteNpWrQosHwMapClassToIntPriority;

typedef struct {
    tQosPfcHwStats   QosPfcHwStats;
} tQosxRemoteNpWrFsQosHwGetPfcStats;


typedef struct {
    INT4          i4IfIndex;
    tQoSQEntry   QEntrySubscriber;
} tQosxRemoteNpWrQoSHwSetVlanQueuingStatus;

#ifdef MBSM_WANTED
typedef struct {
    INT4             i4IfIndex;
    INT4             i4ClsOrPriType;
    UINT4            u4ClsOrPri;
    UINT4            u4QId;
    UINT1            u1Flag;
    UINT1            au1Pad[3];
    tMbsmSlotInfo   SlotInfo;
} tQosxRemoteNpWrQoSMbsmHwMapClassToQueue;

typedef struct {
    tRemoteQoSClassMapEntry   ClsMapEntry;
    INT4                 i4IfIndex;
    INT4                 i4ClsOrPriType;
    UINT4                u4ClsOrPri;
    UINT4                u4QId;
    UINT1                u1Flag;
    UINT1                au1Pad[3];
    tMbsmSlotInfo       SlotInfo;
} tQosxRemoteNpWrQoSMbsmHwMapClassToQueueId;

typedef struct {
    INT4               i4IfIndex;
    UINT4              u4QId;
    tRemoteQoSQEntry        QEntry;
    tQoSQtypeEntry    QTypeEntry;
    tQoSREDCfgEntry *   ApRDCfgEntry;
    INT2               i2HL;
    UINT1              au1Pad[2];
    tMbsmSlotInfo     SlotInfo;
} tQosxRemoteNpWrQoSMbsmHwQueueCreate;

typedef struct {
    tRemoteQoSClassMapEntry           ClsMapEntry;
    tQoSPolicyMapEntry          PlyMapEntry;
    tQoSInProfileActionEntry    InProActEntry;
    tQoSOutProfileActionEntry   OutProActEntry;
    tQoSMeterEntry              MeterEntry;
    tMbsmSlotInfo               SlotInfo;
} tQosxRemoteNpWrQoSMbsmHwUpdatePolicyMapForClass;

typedef struct {
    tQosPfcHwEntry   QosPfcHwEntry;
    tMbsmSlotInfo    SlotInfo;
} tQosxRemoteNpWrFsQosMbsmHwConfigPfc;

#endif /* MBSM_WANTED */

typedef struct QosxRemoteNpModInfo {
union {
    tQosxRemoteNpWrFsQosHwConfigPfc  sFsQosHwConfigPfc;
    tQosxRemoteNpWrQoSHwDeleteClassMapEntry  sQoSHwDeleteClassMapEntry;
    tQosxRemoteNpWrQoSHwGetAlgDropStats  sQoSHwGetAlgDropStats;
    tQosxRemoteNpWrQoSHwGetCoSQStats  sQoSHwGetCoSQStats;
    tQosxRemoteNpWrQoSHwGetCountActStats  sQoSHwGetCountActStats;
    tQosxRemoteNpWrQoSHwGetMeterStats  sQoSHwGetMeterStats;
    tQosxRemoteNpWrQoSHwGetRandomDropStats  sQoSHwGetRandomDropStats;
    tQosxRemoteNpWrQoSHwMapClassToPolicy  sQoSHwMapClassToPolicy;
    tQosxRemoteNpWrQoSHwMapClassToQueue  sQoSHwMapClassToQueue;
    tQosxRemoteNpWrQoSHwMapClassToQueueId  sQoSHwMapClassToQueueId;
    tQosxRemoteNpWrQoSHwMeterCreate  sQoSHwMeterCreate;
    tQosxRemoteNpWrQoSHwMeterDelete  sQoSHwMeterDelete;
    tQosxRemoteNpWrQoSHwQueueCreate  sQoSHwQueueCreate;
    tQosxRemoteNpWrQoSHwQueueDelete  sQoSHwQueueDelete;
    tQosxRemoteNpWrQoSHwSchedulerAdd  sQoSHwSchedulerAdd;
    tQosxRemoteNpWrQoSHwSchedulerDelete  sQoSHwSchedulerDelete;
    tQosxRemoteNpWrQoSHwSchedulerHierarchyMap  sQoSHwSchedulerHierarchyMap;
    tQosxRemoteNpWrQoSHwSchedulerUpdateParams  sQoSHwSchedulerUpdateParams;
    tQosxRemoteNpWrQoSHwSetCpuRateLimit  sQoSHwSetCpuRateLimit;
    tQosxRemoteNpWrQoSHwSetDefUserPriority  sQoSHwSetDefUserPriority;
    tQosxRemoteNpWrQoSHwSetPbitPreferenceOverDscp  sQoSHwSetPbitPreferenceOverDscp;
    tQosxRemoteNpWrQoSHwUnmapClassFromPolicy  sQoSHwUnmapClassFromPolicy;
    tQosxRemoteNpWrQoSHwUpdatePolicyMapForClass  sQoSHwUpdatePolicyMapForClass;
    tQosxRemoteNpWrQoSHwMapClasstoPriMap sQoSHwMapClasstoPriMap;
    tQosxRemoteNpWrQosHwMapClassToIntPriority sQosHwMapClassToIntPriority;
    tQosxRemoteNpWrQoSHwSetVlanQueuingStatus  sQoSHwSetVlanQueuingStatus;
    tQosxRemoteNpWrFsQosHwGetPfcStats  sFsQosHwGetPfcStats;
#ifdef MBSM_WANTED
    tQosxRemoteNpWrQoSMbsmHwMapClassToQueue  sQoSMbsmHwMapClassToQueue;
    tQosxRemoteNpWrQoSMbsmHwMapClassToQueueId  sQoSMbsmHwMapClassToQueueId;
    tQosxRemoteNpWrQoSMbsmHwQueueCreate  sQoSMbsmHwQueueCreate;
    tQosxRemoteNpWrQoSMbsmHwUpdatePolicyMapForClass  sQoSMbsmHwUpdatePolicyMapForClass;
    tQosxRemoteNpWrFsQosMbsmHwConfigPfc  sFsQosMbsmHwConfigPfc;
#endif /* MBSM_WANTED */
    }unOpCode;

#define  QosxRemoteNpFsQosHwConfigPfc  unOpCode.sFsQosHwConfigPfc;
#define  QosxRemoteNpQoSHwDeleteClassMapEntry  unOpCode.sQoSHwDeleteClassMapEntry;
#define  QosxRemoteNpQoSHwGetAlgDropStats  unOpCode.sQoSHwGetAlgDropStats;
#define  QosxRemoteNpQoSHwGetCoSQStats  unOpCode.sQoSHwGetCoSQStats;
#define  QosxRemoteNpQoSHwGetCountActStats  unOpCode.sQoSHwGetCountActStats;
#define  QosxRemoteNpQoSHwGetMeterStats  unOpCode.sQoSHwGetMeterStats;
#define  QosxRemoteNpQoSHwGetRandomDropStats  unOpCode.sQoSHwGetRandomDropStats;
#define  QosxRemoteNpQoSHwMapClassToPolicy  unOpCode.sQoSHwMapClassToPolicy;
#define  QosxRemoteNpQoSHwMapClassToQueue  unOpCode.sQoSHwMapClassToQueue;
#define  QosxRemoteNpQoSHwMapClassToQueueId  unOpCode.sQoSHwMapClassToQueueId;
#define  QosxRemoteNpQoSHwMeterCreate  unOpCode.sQoSHwMeterCreate;
#define  QosxRemoteNpQoSHwMeterDelete  unOpCode.sQoSHwMeterDelete;
#define  QosxRemoteNpQoSHwQueueCreate  unOpCode.sQoSHwQueueCreate;
#define  QosxRemoteNpQoSHwQueueDelete  unOpCode.sQoSHwQueueDelete;
#define  QosxRemoteNpQoSHwSchedulerAdd  unOpCode.sQoSHwSchedulerAdd;
#define  QosxRemoteNpQoSHwSchedulerDelete  unOpCode.sQoSHwSchedulerDelete;
#define  QosxRemoteNpQoSHwSchedulerHierarchyMap  unOpCode.sQoSHwSchedulerHierarchyMap;
#define  QosxRemoteNpQoSHwSchedulerUpdateParams  unOpCode.sQoSHwSchedulerUpdateParams;
#define  QosxRemoteNpQoSHwSetCpuRateLimit  unOpCode.sQoSHwSetCpuRateLimit;
#define  QosxRemoteNpQoSHwSetDefUserPriority  unOpCode.sQoSHwSetDefUserPriority;
#define  QosxRemoteNpQoSHwSetPbitPreferenceOverDscp  unOpCode.sQoSHwSetPbitPreferenceOverDscp;
#define  QosxRemoteNpQoSHwUnmapClassFromPolicy  unOpCode.sQoSHwUnmapClassFromPolicy;
#define  QosxRemoteNpQoSHwUpdatePolicyMapForClass  unOpCode.sQoSHwUpdatePolicyMapForClass;
#define  QosxRemoteNpQoSHwMapClasstoPriMap  unOpCode.sQoSHwMapClasstoPriMap;
#define  QosxRemoteNpQosHwMapClassToIntPriority  unOpCode.sQosHwMapClassToIntPriority;
#define  QosxRemoteNpQoSHwSetVlanQueuingStatus  unOpCode.sQoSHwSetVlanQueuingStatus;
#define  QosxRemoteNpFsQosHwGetPfcStats  unOpCode.sFsQosHwGetPfcStats;
#ifdef MBSM_WANTED
#define  QosxRemoteNpQoSMbsmHwMapClassToQueue  unOpCode.sQoSMbsmHwMapClassToQueue;
#define  QosxRemoteNpQoSMbsmHwMapClassToQueueId  unOpCode.sQoSMbsmHwMapClassToQueueId;
#define  QosxRemoteNpQoSMbsmHwQueueCreate  unOpCode.sQoSMbsmHwQueueCreate;
#define  QosxRemoteNpQoSMbsmHwUpdatePolicyMapForClass  unOpCode.sQoSMbsmHwUpdatePolicyMapForClass;
#define  QosxRemoteNpFsQosMbsmHwConfigPfc  unOpCode.sFsQosMbsmHwConfigPfc;
#endif /* MBSM_WANTED */
} tQosxRemoteNpModInfo;
#endif /* QOSX_WANTED */
#ifdef ELPS_WANTED 
/* Required arguments list for the elps NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

typedef struct {
    tElpsHwPgSwitchInfo    PgHwInfo;
    UINT4                  u4ContextId;
    UINT4                  u4PgId;
} tElpsRemoteNpWrFsMiElpsHwPgSwitchDataPath;

typedef struct ElpsRemoteNpModInfo {
union {
    tElpsRemoteNpWrFsMiElpsHwPgSwitchDataPath  sFsMiElpsHwPgSwitchDataPath;
    }unOpCode;

#define  ElpsRemoteNpFsMiElpsHwPgSwitchDataPath  unOpCode.sFsMiElpsHwPgSwitchDataPath;
} tElpsRemoteNpModInfo;


#endif /* ELPS_WANTED */
#ifdef RMON_WANTED
typedef struct {
    tRmonEtherStatsNode   EthStatsEntry;
    UINT4                  u4IfIndex;
} tRmonRemoteNpWrFsRmonHwGetEthStatsTable;

typedef struct {
    UINT4  u4IfIndex;
    UINT1  u1EtherStatsEnable;
    UINT1  au1Pad[3];
} tRmonRemoteNpWrFsRmonHwSetEtherStatsTable;

typedef struct RmonRemoteNpModInfo {
union {
    tRmonRemoteNpWrFsRmonHwGetEthStatsTable  sFsRmonHwGetEthStatsTable;
    tRmonRemoteNpWrFsRmonHwSetEtherStatsTable  sFsRmonHwSetEtherStatsTable;
    }unOpCode;

#define  RmonRemoteNpFsRmonHwGetEthStatsTable  unOpCode.sFsRmonHwGetEthStatsTable;
#define  RmonRemoteNpFsRmonHwSetEtherStatsTable  unOpCode.sFsRmonHwSetEtherStatsTable;
} tRmonRemoteNpModInfo;
#endif /* RMON_WANTED */
#ifdef RMON2_WANTED
typedef struct {
    tPktHeader  FlowStatsTuple;
} tRmonv2RemoteNpWrFsRMONv2AddFlowStats;

typedef struct {
    tPktHeader     FlowStatsTuple;
    tRmon2Stats   StatsCnt;
} tRmonv2RemoteNpWrFsRMONv2CollectStats;

typedef struct {
    UINT4      u4InterfaceIdx;
    tPortList  PortList;
    UINT2      u2VlanId;
    UINT1      au1Pad[2];
} tRmonv2RemoteNpWrFsRMONv2DisableProbe;

typedef struct {
    UINT4  u4ProtocolLclIdx;
} tRmonv2RemoteNpWrFsRMONv2DisableProtocol;

typedef struct {
    UINT4      u4InterfaceIdx;
    tPortList  PortList;
    UINT2      u2VlanId;
    UINT1      au1Pad[2];
} tRmonv2RemoteNpWrFsRMONv2EnableProbe;

typedef struct {
    tRmon2ProtocolIdfr  Rmon2ProtoIdfr;
} tRmonv2RemoteNpWrFsRMONv2EnableProtocol;

typedef struct {
    UINT4  u4RMONv2Status;
} tRmonv2RemoteNpWrFsRMONv2HwSetStatus;

typedef struct {
    tPktHeader  FlowStatsTuple;
} tRmonv2RemoteNpWrFsRMONv2RemoveFlowStats;

typedef struct {

 INT4     i4Index;
 tVlanId  u4VlanIndex;
 UINT1    au1Mac[MAC_ADDR_LEN];
}tRmonv2RemoteNpWrFsRMONv2GetPortID;

typedef struct Rmonv2RemoteNpModInfo {
union {
    tRmonv2RemoteNpWrFsRMONv2AddFlowStats  sFsRMONv2AddFlowStats;
    tRmonv2RemoteNpWrFsRMONv2CollectStats  sFsRMONv2CollectStats;
    tRmonv2RemoteNpWrFsRMONv2DisableProbe  sFsRMONv2DisableProbe;
    tRmonv2RemoteNpWrFsRMONv2DisableProtocol  sFsRMONv2DisableProtocol;
    tRmonv2RemoteNpWrFsRMONv2EnableProbe  sFsRMONv2EnableProbe;
    tRmonv2RemoteNpWrFsRMONv2EnableProtocol  sFsRMONv2EnableProtocol;
    tRmonv2RemoteNpWrFsRMONv2HwSetStatus  sFsRMONv2HwSetStatus;
    tRmonv2RemoteNpWrFsRMONv2RemoveFlowStats  sFsRMONv2RemoveFlowStats;
    tRmonv2RemoteNpWrFsRMONv2GetPortID  sFsRMONv2GetPortID;
    }unOpCode;

#define  Rmonv2RemoteNpFsRMONv2AddFlowStats  unOpCode.sFsRMONv2AddFlowStats;
#define  Rmonv2RemoteNpFsRMONv2CollectStats  unOpCode.sFsRMONv2CollectStats;
#define  Rmonv2RemoteNpFsRMONv2DisableProbe  unOpCode.sFsRMONv2DisableProbe;
#define  Rmonv2RemoteNpFsRMONv2DisableProtocol  unOpCode.sFsRMONv2DisableProtocol;
#define  Rmonv2RemoteNpFsRMONv2EnableProbe  unOpCode.sFsRMONv2EnableProbe;
#define  Rmonv2RemoteNpFsRMONv2EnableProtocol  unOpCode.sFsRMONv2EnableProtocol;
#define  Rmonv2RemoteNpFsRMONv2HwSetStatus  unOpCode.sFsRMONv2HwSetStatus;
#define  Rmonv2RemoteNpFsRMONv2RemoveFlowStats  unOpCode.sFsRMONv2RemoveFlowStats;
#define  Rmonv2RemoteNpFsRMONv2GetPortID  unOpCode.sFsRMONv2GetPortID;
} tRmonv2RemoteNpModInfo;
#endif /* RMON2_WANTED */
#ifdef DSMON_WANTED
typedef struct {
    tPktHeader  FlowStatsTuple;
} tDsmonRemoteNpWrFsDSMONAddFlowStats;

typedef struct {
    tPktHeader     FlowStatsTuple;
    tRmon2Stats   StatsCnt;
} tDsmonRemoteNpWrFsDSMONCollectStats;

typedef struct {
    UINT4      u4InterfaceIdx;
    tPortList  PortList;
    UINT2      u2VlanId;
    UINT1      au1Pad[2];
} tDsmonRemoteNpWrFsDSMONEnableProbe;

typedef struct {
    UINT4      u4InterfaceIdx;
    tPortList  PortList;
    UINT2      u2VlanId;
    UINT1      au1Pad[2];
} tDsmonRemoteNpWrFsDSMONDisableProbe;

typedef struct {
    UINT4  u4DSMONStatus;
} tDsmonRemoteNpWrFsDSMONHwSetStatus;

typedef struct {
    tPktHeader  FlowStatsTuple;
} tDsmonRemoteNpWrFsDSMONRemoveFlowStats;

typedef struct DsmonRemoteNpModInfo {
union {
    tDsmonRemoteNpWrFsDSMONAddFlowStats  sFsDSMONAddFlowStats;
    tDsmonRemoteNpWrFsDSMONCollectStats  sFsDSMONCollectStats;
    tDsmonRemoteNpWrFsDSMONEnableProbe  sFsDSMONEnableProbe;
    tDsmonRemoteNpWrFsDSMONDisableProbe  sFsDSMONDisableProbe;
    tDsmonRemoteNpWrFsDSMONHwSetStatus  sFsDSMONHwSetStatus;
    tDsmonRemoteNpWrFsDSMONRemoveFlowStats  sFsDSMONRemoveFlowStats;
    }unOpCode;

#define  DsmonRemoteNpFsDSMONAddFlowStats  unOpCode.sFsDSMONAddFlowStats;
#define  DsmonRemoteNpFsDSMONCollectStats  unOpCode.sFsDSMONCollectStats;
#define  DsmonRemoteNpFsDSMONEnableProbe  unOpCode.sFsDSMONEnableProbe;
#define  DsmonRemoteNpFsDSMONDisableProbe  unOpCode.sFsDSMONDisableProbe;
#define  DsmonRemoteNpFsDSMONHwSetStatus  unOpCode.sFsDSMONHwSetStatus;
#define  DsmonRemoteNpFsDSMONRemoveFlowStats  unOpCode.sFsDSMONRemoveFlowStats;
} tDsmonRemoteNpModInfo;
#endif /* DSMON_WANTED */

#ifdef SYNCE_WANTED
typedef struct {
    tSynceHwInfo   SynceHwSynceInfo;
} tSynceRemoteNpWrFsNpHwConfigSynceInfo;

typedef struct SynceRemoteNpModInfo {
union {
    tSynceRemoteNpWrFsNpHwConfigSynceInfo  sFsNpHwConfigSynceInfo;
}unOpCode;

#define  SynceRemoteNpFsNpHwConfigSynceInfo  unOpCode.sFsNpHwConfigSynceInfo;
} tSynceRemoteNpModInfo;
#endif /* SYNCE_WANTED */

#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
typedef struct {
    UINT4    u4VrId;
    UINT4    u4CfaIfIndex;
    UINT4    u4IpAddr;
    UINT4    u4IpSubNetMask;
    UINT1    au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1    au1MacAddr[MAC_ADDR_LEN];
    UINT2    u2VlanId;
} tIpRemoteNpWrFsNpIpv4CreateIpInterface;

typedef struct {
    UINT4    u4VrId;
    UINT4    u4CfaIfIndex;
    UINT4    u4IpAddr;
    UINT4    u4IpSubNetMask;
    UINT1    au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1    au1MacAddr[MAC_ADDR_LEN];
    UINT2    u2VlanId;
    UINT4    u4ParentIndex;
} tIpRemoteNpWrFsNpIpv4CreateL3SubInterface;

typedef struct {
    UINT4    u4VrId;
    UINT4    u4CfaIfIndex;
    UINT4    u4IpAddr;
    UINT4    u4IpSubNetMask;
    UINT1    au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1    au1MacAddr[MAC_ADDR_LEN];
    UINT2    u2VlanId;
} tIpRemoteNpWrFsNpIpv4ModifyIpInterface;

typedef struct {
    UINT4    u4VrId;
    UINT4    u4IfIndex;
    UINT1    au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT2    u2VlanId;
    UINT1    au1pad[2];
} tIpRemoteNpWrFsNpIpv4DeleteIpInterface;

typedef struct {
    UINT4    u4IfIndex;
} tIpRemoteNpWrFsNpIpv4DeleteL3SubInterface;

typedef struct {
    UINT4    u4VrId;
    UINT4    u4IfIndex;
    UINT1    au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT2    u2VlanId;
    UINT1    au1pad[2];
    UINT4    u4IpAddr;
} tIpRemoteNpWrFsNpIpv4DeleteSecIpInterface;

typedef struct {
    UINT4    u4VrId;
    UINT4    u4CfaIfIndex;
    UINT4    u4IpAddr;
    UINT4    u4IpSubNetMask;
    UINT4    u4Status;
    UINT1    au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1    au1MacAddr[MAC_ADDR_LEN];
    UINT2    u2VlanId;
} tIpRemoteNpWrFsNpIpv4UpdateIpInterfaceStatus;

typedef struct {
    UINT4  u4VrId;
    UINT1  u1Status;
    UINT1  au1pad[3];
} tIpRemoteNpWrFsNpIpv4SetForwardingStatus;

typedef struct {
    tFsNpNextHopInfo  routeEntry;
    UINT4             u4VrId;
    UINT4             u4IpDestAddr;
    UINT4             u4IpSubNetMask;
    INT4              i4FreeDefIpB4Del;
} tIpRemoteNpWrFsNpIpv4UcDelRoute;

typedef struct {
    UINT1      au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1      macAddr[MAC_ADDR_LEN];
    tNpVlanId  u2VlanId;
    UINT4      u4TblFull;
    UINT4      u4IfIndex;
    UINT4      u4IpAddr;
    INT1       i1State;
    UINT1      au1pad[3];
} tIpRemoteNpWrFsNpIpv4ArpAdd;

typedef struct {
    UINT4      u4IfIndex;
    UINT4      u4PhyIfIndex;
    UINT4      u4IpAddr;
    UINT1      au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1      macAddr[MAC_ADDR_LEN];
    tNpVlanId  u2VlanId;
    INT1       i1State;
    UINT1      au1pad[3];
} tIpRemoteNpWrFsNpIpv4ArpModify;

typedef struct {
    UINT4    u4IpAddr;
    UINT1    au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT1     i1State;
    UINT1    au1pad[3];
} tIpRemoteNpWrFsNpIpv4ArpDel;

typedef struct {
    UINT4  u4IpAddress;
    UINT1  u1NextHopFlag;
    UINT1  u1HitBitStatus;
    UINT1  au1pad[2];
} tIpRemoteNpWrFsNpIpv4CheckHitOnArpEntry;

typedef struct {
    tFsNpNextHopInfo  routeEntry;
    UINT4             u4VrId;
    UINT4             u4IpDestAddr;
    UINT4             u4IpSubNetMask;
    UINT4             u4HwIntfId[2];
    UINT1            bu1TblFull;
    UINT1            au1pad[3];
} tIpRemoteNpWrFsNpIpv4UcAddRoute;

typedef struct {
    UINT4      u4VrId;
    UINT4      u4IfIndex;
    UINT4      u4IpAddr;
    UINT4      u4TblFull;
    UINT1      au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1      macAddr[MAC_ADDR_LEN];
    tNpVlanId  u2VlanId;
    INT1       i1State;
    UINT1      au1pad[7];
} tIpRemoteNpWrFsNpIpv4VrfArpAdd;

typedef struct {
    UINT4      u4VrId;
    UINT4      u4IfIndex;
    UINT4      u4PhyIfIndex;
    UINT4      u4IpAddr;
    UINT1      au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1      macAddr[MAC_ADDR_LEN];
    tNpVlanId  u2VlanId;
    INT1       i1State;
    UINT1      au1pad[3];
} tIpRemoteNpWrFsNpIpv4VrfArpModify;

typedef struct {
    UINT4    u4VrId;
    UINT4    u4IpAddr;
    UINT1    au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT1     i1State;
    UINT1    au1pad[3];
} tIpRemoteNpWrFsNpIpv4VrfArpDel;

typedef struct {
    UINT4  u4VrId;
    UINT4  u4IpAddress;
    UINT1  u1NextHopFlag;
    UINT1  u1HitBitStatus;
    UINT1  au1pad[2];
} tIpRemoteNpWrFsNpIpv4VrfCheckHitOnArpEntry;

typedef struct {
    UINT4  u4VrId;
} tIpRemoteNpWrFsNpIpv4VrfClearArpTable;

typedef struct {
    UINT4   u4VrId;
    UINT4   u4IpAddress;
} tIpRemoteNpWrFsNpIpv4VrfGetSrcMovedIpAddr;

typedef struct {
    UINT4  u4VrfId;
    UINT1  u1Status;
    UINT1  au1pad[3];
} tIpRemoteNpWrFsNpCfaVrfSetDlfStatus;

typedef struct {
    UINT4    u4VrfId;
    UINT4    u4IfIndex;
    UINT4    u4IpAddr;
    UINT1    au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1    macAddr[MAC_ADDR_LEN];
    INT1     i1State;
    UINT1   bu1TblFull;
} tIpRemoteNpWrFsNpL3Ipv4VrfArpAdd;

typedef struct {
    UINT4    u4VrfId;
    UINT4    u4IfIndex;
    UINT4    u4IpAddr;
    UINT1    au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1    macAddr[MAC_ADDR_LEN];
    INT1     i1State;
    UINT1    au1pad[1];
} tIpRemoteNpWrFsNpL3Ipv4VrfArpModify;

typedef struct {
    tNpArpInput     ArpNpInParam;
    tNpArpOutput   ArpNpOutParam;
} tIpRemoteNpWrFsNpIpv4VrfArpGet;

typedef struct {
    tNpArpInput     ArpNpInParam;
    tNpArpOutput   ArpNpOutParam;
} tIpRemoteNpWrFsNpIpv4VrfArpGetNext;

#ifdef VRRP_WANTED
typedef struct {
    UINT4      u4IfIndex;
    UINT4      u4IpAddr;
    UINT1      au1MacAddr[MAC_ADDR_LEN];
    tNpVlanId  u2VlanId;
} tIpRemoteNpWrFsNpIpv4VrrpIntfCreateWr;

typedef struct {
    UINT4      u4IpAddr;
    UINT1      au1MacAddr[MAC_ADDR_LEN];
    tNpVlanId  u2VlanId;
} tIpRemoteNpWrFsNpIpv4CreateVrrpInterface;

typedef struct {
    UINT4      u4IfIndex;
    UINT4      u4IpAddr;
    UINT1      au1MacAddr[MAC_ADDR_LEN];
    tNpVlanId  u2VlanId;
} tIpRemoteNpWrFsNpIpv4VrrpIntfDeleteWr;

typedef struct {
    UINT4      u4IpAddr;
    UINT1      au1MacAddr[MAC_ADDR_LEN];
    tNpVlanId  u2VlanId;
} tIpRemoteNpWrFsNpIpv4DeleteVrrpInterface;

typedef struct {
    INT4  i4IfIndex;
    INT4  i4VrId;
} tIpRemoteNpWrFsNpIpv4GetVrrpInterface;

typedef struct {
    UINT1 u1NpAction;
    UINT1 au1Padding[3];
    tVrrpNwIntf *pVrrpNwIntf;
} tIpRemoteNpWrFsNpVrrpHwProgram;

#endif /* VRRP_WANTED */

#ifdef ISIS_WANTED
typedef struct {
        UINT1 u1Status;
            UINT1 au1Padding[3];
}tIpRemoteNpWrFsNpIsisHwProgram;
#endif /* ISIS_WANTED */

typedef struct {
    UINT4  u4DestAddr;
    UINT4  u4Mask;
} tIpRemoteNpWrFsNpIpv4IsRtPresentInFastPath;

typedef struct {
    INT4     i4StatType;
    UINT4   u4RetVal;
} tIpRemoteNpWrFsNpIpv4GetStats;

typedef struct {
    UINT4  u4VrId;
    UINT1  u1Status;
    UINT1  au1pad[3];
} tIpRemoteNpWrFsNpIpv4VrmEnableVr;

typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4VrId;
} tIpRemoteNpWrFsNpIpv4BindIfToVrId;

typedef struct {
    tFsNpNextHopInfo    NextHopInfo;
    UINT4               u4VrId;
    UINT4               u4Dest;
    UINT4               u4DestMask;
} tIpRemoteNpWrFsNpIpv4GetNextHopInfo;

typedef struct {
    tFsNpNextHopInfo  nextHopInfo;
    UINT4             u4VrId;
    UINT4             u4Dest;
    UINT4             u4DestMask;
} tIpRemoteNpWrFsNpIpv4UcAddTrap;

typedef struct {
    UINT4  u4VrId;
} tIpRemoteNpWrFsNpIpv4ClearFowardingTbl;

typedef struct {
    UINT4   u4IpAddress;
} tIpRemoteNpWrFsNpIpv4GetSrcMovedIpAddr;

typedef struct {
    tNpIpVlanMappingInfo   PvlanMappingInfo;
} tIpRemoteNpWrFsNpIpv4MapVlansToIpInterface;

#ifdef NAT_WANTED
typedef struct {
    INT4  i4Intf;
} tIpRemoteNpWrFsNpNatDisableOnIntf;

typedef struct {
    INT4  i4Intf;
} tIpRemoteNpWrFsNpNatEnableOnIntf;

#endif /* NAT_WANTED */
typedef struct {
    tFsNpIp4IntInfo    IpIntInfo;
    UINT4              u4VrId;
    UINT4              u4IfStatus;
} tIpRemoteNpWrFsNpIpv4IntfStatus;

typedef struct {
    tNpArpInput     ArpNpInParam;
    tNpArpOutput   ArpNpOutParam;
} tIpRemoteNpWrFsNpIpv4ArpGet;

typedef struct {
    tNpArpInput     ArpNpInParam;
    tNpArpOutput   ArpNpOutParam;
} tIpRemoteNpWrFsNpIpv4ArpGetNext;

typedef struct {
    tNpRtmInput     RtmNpInParam;
    tNpRtmOutput   RtmNpOutParam;
} tIpRemoteNpWrFsNpIpv4UcGetRoute;

typedef struct {
    UINT4    u4IfIndex;
    UINT4    u4IpAddr;
    UINT1    au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1    macAddr[MAC_ADDR_LEN];
    INT1     i1State;
    UINT1   bu1TblFull;
} tIpRemoteNpWrFsNpL3Ipv4ArpAdd;

typedef struct {
    UINT4    u4IfIndex;
    UINT4    u4IpAddr;
    UINT1    au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1    macAddr[MAC_ADDR_LEN];
    INT1     i1State;
    UINT1    au1pad[1];
} tIpRemoteNpWrFsNpL3Ipv4ArpModify;

typedef struct {
    tL3Action        L3Action;
    tFsNpL3IfInfo   FsNpL3IfInfo;
} tIpRemoteNpWrFsNpIpv4L3IpInterface;

typedef struct IpRemoteNpModInfo {
union {
    tIpRemoteNpWrFsNpIpv4CreateIpInterface  sFsNpIpv4CreateIpInterface;
    tIpRemoteNpWrFsNpIpv4CreateL3SubInterface sFsNpIpv4CreateL3SubInterface;
    tIpRemoteNpWrFsNpIpv4ModifyIpInterface  sFsNpIpv4ModifyIpInterface;
    tIpRemoteNpWrFsNpIpv4DeleteIpInterface  sFsNpIpv4DeleteIpInterface;
    tIpRemoteNpWrFsNpIpv4DeleteL3SubInterface  sFsNpIpv4DeleteL3SubInterface;
    tIpRemoteNpWrFsNpIpv4UpdateIpInterfaceStatus  sFsNpIpv4UpdateIpInterfaceStatus;
    tIpRemoteNpWrFsNpIpv4SetForwardingStatus  sFsNpIpv4SetForwardingStatus;
    tIpRemoteNpWrFsNpIpv4UcDelRoute  sFsNpIpv4UcDelRoute;
    tIpRemoteNpWrFsNpIpv4ArpAdd  sFsNpIpv4ArpAdd;
    tIpRemoteNpWrFsNpIpv4ArpModify  sFsNpIpv4ArpModify;
    tIpRemoteNpWrFsNpIpv4ArpDel  sFsNpIpv4ArpDel;
    tIpRemoteNpWrFsNpIpv4CheckHitOnArpEntry  sFsNpIpv4CheckHitOnArpEntry;
    tIpRemoteNpWrFsNpIpv4UcAddRoute  sFsNpIpv4UcAddRoute;
    tIpRemoteNpWrFsNpIpv4VrfArpAdd  sFsNpIpv4VrfArpAdd;
    tIpRemoteNpWrFsNpIpv4VrfArpModify  sFsNpIpv4VrfArpModify;
    tIpRemoteNpWrFsNpIpv4VrfArpDel  sFsNpIpv4VrfArpDel;
    tIpRemoteNpWrFsNpIpv4VrfCheckHitOnArpEntry  sFsNpIpv4VrfCheckHitOnArpEntry;
    tIpRemoteNpWrFsNpIpv4VrfClearArpTable  sFsNpIpv4VrfClearArpTable;
    tIpRemoteNpWrFsNpIpv4VrfGetSrcMovedIpAddr  sFsNpIpv4VrfGetSrcMovedIpAddr;
    tIpRemoteNpWrFsNpCfaVrfSetDlfStatus  sFsNpCfaVrfSetDlfStatus;
    tIpRemoteNpWrFsNpL3Ipv4VrfArpAdd  sFsNpL3Ipv4VrfArpAdd;
    tIpRemoteNpWrFsNpL3Ipv4VrfArpModify  sFsNpL3Ipv4VrfArpModify;
    tIpRemoteNpWrFsNpIpv4VrfArpGet  sFsNpIpv4VrfArpGet;
    tIpRemoteNpWrFsNpIpv4VrfArpGetNext  sFsNpIpv4VrfArpGetNext;
#ifdef VRRP_WANTED
    tIpRemoteNpWrFsNpIpv4VrrpIntfCreateWr  sFsNpIpv4VrrpIntfCreateWr;
    tIpRemoteNpWrFsNpIpv4CreateVrrpInterface  sFsNpIpv4CreateVrrpInterface;
    tIpRemoteNpWrFsNpIpv4VrrpIntfDeleteWr  sFsNpIpv4VrrpIntfDeleteWr;
    tIpRemoteNpWrFsNpIpv4DeleteVrrpInterface  sFsNpIpv4DeleteVrrpInterface;
    tIpRemoteNpWrFsNpIpv4GetVrrpInterface  sFsNpIpv4GetVrrpInterface;
    tIpRemoteNpWrFsNpVrrpHwProgram sFsNpVrrpHwProgram;
#endif /* VRRP_WANTED */
#ifdef ISIS_WANTED
        tIpRemoteNpWrFsNpIsisHwProgram sFsNpIsisHwProgram;
#endif /* ISIS_WANTED */
    tIpRemoteNpWrFsNpIpv4IsRtPresentInFastPath  sFsNpIpv4IsRtPresentInFastPath;
    tIpRemoteNpWrFsNpIpv4GetStats  sFsNpIpv4GetStats;
    tIpRemoteNpWrFsNpIpv4VrmEnableVr  sFsNpIpv4VrmEnableVr;
    tIpRemoteNpWrFsNpIpv4BindIfToVrId  sFsNpIpv4BindIfToVrId;
    tIpRemoteNpWrFsNpIpv4GetNextHopInfo  sFsNpIpv4GetNextHopInfo;
    tIpRemoteNpWrFsNpIpv4UcAddTrap  sFsNpIpv4UcAddTrap;
    tIpRemoteNpWrFsNpIpv4ClearFowardingTbl  sFsNpIpv4ClearFowardingTbl;
    tIpRemoteNpWrFsNpIpv4GetSrcMovedIpAddr  sFsNpIpv4GetSrcMovedIpAddr;
    tIpRemoteNpWrFsNpIpv4MapVlansToIpInterface  sFsNpIpv4MapVlansToIpInterface;
#ifdef NAT_WANTED
    tIpRemoteNpWrFsNpNatDisableOnIntf  sFsNpNatDisableOnIntf;
    tIpRemoteNpWrFsNpNatEnableOnIntf  sFsNpNatEnableOnIntf;
#endif /* NAT_WANTED */
    tIpRemoteNpWrFsNpIpv4IntfStatus  sFsNpIpv4IntfStatus;
    tIpRemoteNpWrFsNpIpv4ArpGet  sFsNpIpv4ArpGet;
    tIpRemoteNpWrFsNpIpv4ArpGetNext  sFsNpIpv4ArpGetNext;
    tIpRemoteNpWrFsNpIpv4UcGetRoute  sFsNpIpv4UcGetRoute;
    tIpRemoteNpWrFsNpL3Ipv4ArpAdd  sFsNpL3Ipv4ArpAdd;
    tIpRemoteNpWrFsNpL3Ipv4ArpModify  sFsNpL3Ipv4ArpModify;
    tIpRemoteNpWrFsNpIpv4L3IpInterface  sFsNpIpv4L3IpInterface;
    tIpRemoteNpWrFsNpIpv4DeleteSecIpInterface  sFsNpIpv4DeleteSecIpInterface;
    }unOpCode;

#define  IpRemoteNpFsNpIpv4CreateIpInterface  unOpCode.sFsNpIpv4CreateIpInterface;
#define  IpRemoteNpFsNpIpv4ModifyIpInterface  unOpCode.sFsNpIpv4ModifyIpInterface;
#define  IpRemoteNpFsNpIpv4DeleteIpInterface  unOpCode.sFsNpIpv4DeleteIpInterface;
#define  IpRemoteNpFsNpIpv4DeleteL3SubInterface  unOpCode.sFsNpIpv4DeleteL3SubInterface;
#define  IpRemoteNpFsNpIpv4DeleteSecIpInterface  unOpCode.sFsNpIpv4DeleteSecIpInterface;
#define  IpRemoteNpFsNpIpv4UpdateIpInterfaceStatus  unOpCode.sFsNpIpv4UpdateIpInterfaceStatus;
#define  IpRemoteNpFsNpIpv4SetForwardingStatus  unOpCode.sFsNpIpv4SetForwardingStatus;
#define  IpRemoteNpFsNpIpv4UcDelRoute  unOpCode.sFsNpIpv4UcDelRoute;
#define  IpRemoteNpFsNpIpv4ArpAdd  unOpCode.sFsNpIpv4ArpAdd;
#define  IpRemoteNpFsNpIpv4ArpModify  unOpCode.sFsNpIpv4ArpModify;
#define  IpRemoteNpFsNpIpv4ArpDel  unOpCode.sFsNpIpv4ArpDel;
#define  IpRemoteNpFsNpIpv4CheckHitOnArpEntry  unOpCode.sFsNpIpv4CheckHitOnArpEntry;
#define  IpRemoteNpFsNpIpv4UcAddRoute  unOpCode.sFsNpIpv4UcAddRoute;
#define  IpRemoteNpFsNpIpv4VrfArpAdd  unOpCode.sFsNpIpv4VrfArpAdd;
#define  IpRemoteNpFsNpIpv4VrfArpModify  unOpCode.sFsNpIpv4VrfArpModify;
#define  IpRemoteNpFsNpIpv4VrfArpDel  unOpCode.sFsNpIpv4VrfArpDel;
#define  IpRemoteNpFsNpIpv4VrfCheckHitOnArpEntry  unOpCode.sFsNpIpv4VrfCheckHitOnArpEntry;
#define  IpRemoteNpFsNpIpv4VrfClearArpTable  unOpCode.sFsNpIpv4VrfClearArpTable;
#define  IpRemoteNpFsNpIpv4VrfGetSrcMovedIpAddr  unOpCode.sFsNpIpv4VrfGetSrcMovedIpAddr;
#define  IpRemoteNpFsNpCfaVrfSetDlfStatus  unOpCode.sFsNpCfaVrfSetDlfStatus;
#define  IpRemoteNpFsNpL3Ipv4VrfArpAdd  unOpCode.sFsNpL3Ipv4VrfArpAdd;
#define  IpRemoteNpFsNpL3Ipv4VrfArpModify  unOpCode.sFsNpL3Ipv4VrfArpModify;
#define  IpRemoteNpFsNpIpv4VrfArpGet  unOpCode.sFsNpIpv4VrfArpGet;
#define  IpRemoteNpFsNpIpv4VrfArpGetNext  unOpCode.sFsNpIpv4VrfArpGetNext;
#ifdef VRRP_WANTED
#define  IpRemoteNpFsNpIpv4VrrpIntfCreateWr  unOpCode.sFsNpIpv4VrrpIntfCreateWr;
#define  IpRemoteNpFsNpIpv4CreateVrrpInterface  unOpCode.sFsNpIpv4CreateVrrpInterface;
#define  IpRemoteNpFsNpIpv4VrrpIntfDeleteWr  unOpCode.sFsNpIpv4VrrpIntfDeleteWr;
#define  IpRemoteNpFsNpIpv4DeleteVrrpInterface  unOpCode.sFsNpIpv4DeleteVrrpInterface;
#define  IpRemoteNpFsNpIpv4GetVrrpInterface  unOpCode.sFsNpIpv4GetVrrpInterface;
#define  IpRemoteNpFsNpVrrpHwProgram unOpCode.sFsNpVrrpHwProgram;
#endif /* VRRP_WANTED */
#ifdef ISIS_WANTED
#define  IpRemoteNpFsNpIsisHwProgram unOpCode.sFsNpIsisHwProgram;
#endif /* ISIS_WANTED */
#define  IpRemoteNpFsNpIpv4IsRtPresentInFastPath  unOpCode.sFsNpIpv4IsRtPresentInFastPath;
#define  IpRemoteNpFsNpIpv4GetStats  unOpCode.sFsNpIpv4GetStats;
#define  IpRemoteNpFsNpIpv4VrmEnableVr  unOpCode.sFsNpIpv4VrmEnableVr;
#define  IpRemoteNpFsNpIpv4BindIfToVrId  unOpCode.sFsNpIpv4BindIfToVrId;
#define  IpRemoteNpFsNpIpv4GetNextHopInfo  unOpCode.sFsNpIpv4GetNextHopInfo;
#define  IpRemoteNpFsNpIpv4UcAddTrap  unOpCode.sFsNpIpv4UcAddTrap;
#define  IpRemoteNpFsNpIpv4ClearFowardingTbl  unOpCode.sFsNpIpv4ClearFowardingTbl;
#define  IpRemoteNpFsNpIpv4GetSrcMovedIpAddr  unOpCode.sFsNpIpv4GetSrcMovedIpAddr;
#define  IpRemoteNpFsNpIpv4MapVlansToIpInterface  unOpCode.sFsNpIpv4MapVlansToIpInterface;
#ifdef NAT_WANTED
#define  IpRemoteNpFsNpNatDisableOnIntf  unOpCode.sFsNpNatDisableOnIntf;
#define  IpRemoteNpFsNpNatEnableOnIntf  unOpCode.sFsNpNatEnableOnIntf;
#endif /* NAT_WANTED */
#define  IpRemoteNpFsNpIpv4IntfStatus  unOpCode.sFsNpIpv4IntfStatus;
#define  IpRemoteNpFsNpIpv4ArpGet  unOpCode.sFsNpIpv4ArpGet;
#define  IpRemoteNpFsNpIpv4ArpGetNext  unOpCode.sFsNpIpv4ArpGetNext;
#define  IpRemoteNpFsNpIpv4UcGetRoute  unOpCode.sFsNpIpv4UcGetRoute;
#define  IpRemoteNpFsNpL3Ipv4ArpAdd  unOpCode.sFsNpL3Ipv4ArpAdd;
#define  IpRemoteNpFsNpL3Ipv4ArpModify  unOpCode.sFsNpL3Ipv4ArpModify;
#define  IpRemoteNpFsNpIpv4L3IpInterface  unOpCode.sFsNpIpv4L3IpInterface;
#define  IpRemoteNpFsNpIpv4CreateL3SubInterface unOpCode.sFsNpIpv4CreateL3SubInterface;
} tIpRemoteNpModInfo;
#endif /* (IP_WANTED) || (LNXIP4_WANTED) */

#ifdef ECFM_WANTED
typedef struct _RemoteEcfmHwCcTxParams
{
    tRemoteHwPortArray          PortList; /* Tagged Port list */
    tRemoteHwPortArray          UntagPortList; /* Untagged Port List */
    UINT4                 u4ContextId; /* Virtual Context Id */
    UINT4                 u4IfIndex; /* Interface Index */
    UINT2                 u2TxFilterId; /* Handler */
    UINT2                 u2PduLength; /* Size of the PDU */
    UINT1                 u1Pdu[ECFM_MAX_PDU_SIZE]; /* Pointer to the PDU */
    UINT1                 au1Pad[2];
}tRemoteEcfmHwCcTxParams;

typedef struct _RemoteEcfmHwCcRxParams
{
    tRemoteHwPortArray        PortList; /* Tagged Port List */
    tRemoteHwPortArray        UntagPortList; /* Untagged Port List */
    tEcfmCcOffRxInfo    EcfmCcRxInfo; /* Structure pointer to RxInfo */
    UINT4               u4ContextId; /* Virtual Context Id */
    UINT4               u4IfIndex; /* Interface Index */
    UINT2               u2RxFilterId; /* Handler */
    UINT1               au1Pad [2]; /* Structure Padding */
}tRemoteEcfmHwCcRxParams;

typedef struct _RemoteEcfmHwParams
{
    tEcfmHwMaParams         EcfmHwMaParams;
    tEcfmHwMepParams        EcfmHwMepParams;
    tEcfmHwRMepParams       EcfmHwRMepParams;
    union {
        tRemoteEcfmHwCcTxParams   EcfmHwCcTxParams;
        tRemoteEcfmHwCcRxParams   EcfmHwCcRxParams;
    }unParam;
    UINT1                   u1EcfmOffStatus;
    UINT1                   au1Pad [3];
}tRemoteEcfmHwParams;

/* Required arguments list for the ecfm NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

typedef struct {
    tEcfmMepInfoParams   EcfmMepInfoParams;
} tEcfmRemoteNpWrFsMiEcfmClearRcvdLbrCounter;

typedef struct {
    tEcfmMepInfoParams   EcfmMepInfoParams;
} tEcfmRemoteNpWrFsMiEcfmClearRcvdTstCounter;

typedef struct {
    tEcfmMepInfoParams   EcfmMepInfoParams;
    UINT4                u4LbrIn;
} tEcfmRemoteNpWrFsMiEcfmGetRcvdLbrCounter;

typedef struct {
    tEcfmMepInfoParams   EcfmMepInfoParams;
    UINT4                u4TstIn;
} tEcfmRemoteNpWrFsMiEcfmGetRcvdTstCounter;

typedef struct {
    tRemoteEcfmHwParams   EcfmHwInfo;
    UINT1            u1Type;
    UINT1            au1pad[3];
} tEcfmRemoteNpWrFsMiEcfmHwCallNpApi;

typedef struct {
    UINT4  u4ContextId;
} tEcfmRemoteNpWrFsMiEcfmHwDeInit;

typedef struct {
    UINT4    u4ContextId;
    UINT4    u4HwCapability;
} tEcfmRemoteNpWrFsMiEcfmHwGetCapability;

typedef struct {
    UINT4                   u4ContextId;
    tEcfmCcOffMepRxStats    EcfmCcOffMepRxStats;
    UINT2                   u2RxFilterId;
    UINT1                   au1pad[2];
} tEcfmRemoteNpWrFsMiEcfmHwGetCcmRxStatistics;

typedef struct {
    UINT4                   u4ContextId;
    tEcfmCcOffMepTxStats    EcfmCcOffMepTxStats;
    UINT2                   u2TxFilterId;
    UINT1                   au1pad[2];
} tEcfmRemoteNpWrFsMiEcfmHwGetCcmTxStatistics;

typedef struct {
    UINT4                  u4ContextId;
    UINT4                  u4IfIndex;
    tEcfmCcOffPortStats    EcfmCcOffPortStats;
} tEcfmRemoteNpWrFsMiEcfmHwGetPortCcStats;

typedef struct {
    UINT4                    u4ContextId;
    tEcfmCcOffRxHandleInfo   EcfmCcOffRxHandleInfo;
    UINT2                    u2RxHandle;
    BOOL1                    b1More;
    UINT1                    au1pad[1];
} tEcfmRemoteNpWrFsMiEcfmHwHandleIntQFailure;

typedef struct {
    UINT4  u4ContextId;
} tEcfmRemoteNpWrFsMiEcfmHwInit;

typedef struct {
    UINT4  u4ContextId;
} tEcfmRemoteNpWrFsMiEcfmHwRegister;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
    UINT2  u2EtherTypeValue;
    UINT1  u1EtherType;
    UINT1  au1pad[1];
} tEcfmRemoteNpWrFsMiEcfmHwSetVlanEtherType;

typedef struct {
    tEcfmMepInfoParams   EcfmMepInfoParams;
    tEcfmConfigLbmInfo   EcfmConfigLbmInfo;
} tEcfmRemoteNpWrFsMiEcfmStartLbmTransaction;

typedef struct {
    tEcfmMepInfoParams   EcfmMepInfoParams;
    tEcfmConfigTstInfo   EcfmConfigTstInfo;
} tEcfmRemoteNpWrFsMiEcfmStartTstTransaction;

typedef struct {
    tEcfmMepInfoParams   EcfmMepInfoParams;
} tEcfmRemoteNpWrFsMiEcfmStopLbmTransaction;

typedef struct {
    tEcfmMepInfoParams   EcfmMepInfoParams;
} tEcfmRemoteNpWrFsMiEcfmStopTstTransaction;

typedef struct {
    UINT4     u4ContextId;
    UINT4     u4IfIndex;
    tVlanTag  VlanTag;
    UINT2     u2PduLength;
    UINT1     u1DmPdu[ECFM_MAX_PDU_SIZE];
    UINT1     u1Direction;
    UINT1     au1Pad[3];
} tEcfmRemoteNpWrFsMiEcfmTransmit1Dm;

typedef struct {
    UINT4     u4ContextId;
    UINT4     u4IfIndex;
    tVlanTag  VlanTag;
    UINT2     u2PduLength;
    UINT1     u1DmmPdu[ECFM_MAX_PDU_SIZE];
    UINT1     u1Direction;
    UINT1     au1Pad[3];
} tEcfmRemoteNpWrFsMiEcfmTransmitDmm;

typedef struct {
    UINT4     u4ContextId;
    UINT4     u4IfIndex;
    tVlanTag  VlanTag;
    UINT2     u2PduLength;
    UINT1     u1DmrPdu[ECFM_MAX_PDU_SIZE];
    UINT1     u1Direction;
    UINT1     au1Pad[3];
} tEcfmRemoteNpWrFsMiEcfmTransmitDmr;

typedef struct {
    UINT4         u4ContextId;
    UINT4         u4IfIndex;
    tVlanTagInfo  VlanTag;
    UINT2         u2PduLength;
    UINT1         u1LmmPdu[ECFM_MAX_PDU_SIZE];
    UINT1         u1Direction;
    UINT1         au1Pad[3];
} tEcfmRemoteNpWrFsMiEcfmTransmitLmm;

typedef struct {
    UINT4           u4ContextId;
    UINT4           u4IfIndex;
    tVlanTagInfo   VlanTag;
    UINT2           u2PduLength;
    UINT1           u1LmrPdu[ECFM_MAX_PDU_SIZE];
    UINT1           u1Direction;
    UINT1           au1Pad[3];
} tEcfmRemoteNpWrFsMiEcfmTransmitLmr;

typedef struct {
    tEcfmHwLmParams  HwLmInfo;
} tEcfmRemoteNpWrFsMiEcfmStartLm;

typedef struct {
    tEcfmHwLmParams  HwLmInfo;
} tEcfmRemoteNpWrFsMiEcfmStopLm;

typedef struct EcfmRemoteNpModInfo {
union {
    tEcfmRemoteNpWrFsMiEcfmClearRcvdLbrCounter  sFsMiEcfmClearRcvdLbrCounter;
    tEcfmRemoteNpWrFsMiEcfmClearRcvdTstCounter  sFsMiEcfmClearRcvdTstCounter;
    tEcfmRemoteNpWrFsMiEcfmGetRcvdLbrCounter  sFsMiEcfmGetRcvdLbrCounter;
    tEcfmRemoteNpWrFsMiEcfmGetRcvdTstCounter  sFsMiEcfmGetRcvdTstCounter;
    tEcfmRemoteNpWrFsMiEcfmHwCallNpApi  sFsMiEcfmHwCallNpApi;
    tEcfmRemoteNpWrFsMiEcfmHwDeInit  sFsMiEcfmHwDeInit;
    tEcfmRemoteNpWrFsMiEcfmHwGetCapability  sFsMiEcfmHwGetCapability;
    tEcfmRemoteNpWrFsMiEcfmHwGetCcmRxStatistics  sFsMiEcfmHwGetCcmRxStatistics;
    tEcfmRemoteNpWrFsMiEcfmHwGetCcmTxStatistics  sFsMiEcfmHwGetCcmTxStatistics;
    tEcfmRemoteNpWrFsMiEcfmHwGetPortCcStats  sFsMiEcfmHwGetPortCcStats;
    tEcfmRemoteNpWrFsMiEcfmHwHandleIntQFailure  sFsMiEcfmHwHandleIntQFailure;
    tEcfmRemoteNpWrFsMiEcfmHwInit  sFsMiEcfmHwInit;
    tEcfmRemoteNpWrFsMiEcfmHwRegister  sFsMiEcfmHwRegister;
    tEcfmRemoteNpWrFsMiEcfmHwSetVlanEtherType  sFsMiEcfmHwSetVlanEtherType;
    tEcfmRemoteNpWrFsMiEcfmStartLbmTransaction  sFsMiEcfmStartLbmTransaction;
    tEcfmRemoteNpWrFsMiEcfmStartTstTransaction  sFsMiEcfmStartTstTransaction;
    tEcfmRemoteNpWrFsMiEcfmStopLbmTransaction  sFsMiEcfmStopLbmTransaction;
    tEcfmRemoteNpWrFsMiEcfmStopTstTransaction  sFsMiEcfmStopTstTransaction;
    tEcfmRemoteNpWrFsMiEcfmTransmit1Dm  sFsMiEcfmTransmit1Dm;
    tEcfmRemoteNpWrFsMiEcfmTransmitDmm  sFsMiEcfmTransmitDmm;
    tEcfmRemoteNpWrFsMiEcfmTransmitDmr  sFsMiEcfmTransmitDmr;
    tEcfmRemoteNpWrFsMiEcfmTransmitLmm  sFsMiEcfmTransmitLmm;
    tEcfmRemoteNpWrFsMiEcfmTransmitLmr  sFsMiEcfmTransmitLmr;
    tEcfmRemoteNpWrFsMiEcfmStartLm  sFsMiEcfmStartLm;
    tEcfmRemoteNpWrFsMiEcfmStopLm  sFsMiEcfmStopLm;
    }unOpCode;

#define  EcfmRemoteNpFsMiEcfmClearRcvdLbrCounter  unOpCode.sFsMiEcfmClearRcvdLbrCounter;
#define  EcfmRemoteNpFsMiEcfmClearRcvdTstCounter  unOpCode.sFsMiEcfmClearRcvdTstCounter;
#define  EcfmRemoteNpFsMiEcfmGetRcvdLbrCounter  unOpCode.sFsMiEcfmGetRcvdLbrCounter;
#define  EcfmRemoteNpFsMiEcfmGetRcvdTstCounter  unOpCode.sFsMiEcfmGetRcvdTstCounter;
#define  EcfmRemoteNpFsMiEcfmHwCallNpApi  unOpCode.sFsMiEcfmHwCallNpApi;
#define  EcfmRemoteNpFsMiEcfmHwDeInit  unOpCode.sFsMiEcfmHwDeInit;
#define  EcfmRemoteNpFsMiEcfmHwGetCapability  unOpCode.sFsMiEcfmHwGetCapability;
#define  EcfmRemoteNpFsMiEcfmHwGetCcmRxStatistics  unOpCode.sFsMiEcfmHwGetCcmRxStatistics;
#define  EcfmRemoteNpFsMiEcfmHwGetCcmTxStatistics  unOpCode.sFsMiEcfmHwGetCcmTxStatistics;
#define  EcfmRemoteNpFsMiEcfmHwGetPortCcStats  unOpCode.sFsMiEcfmHwGetPortCcStats;
#define  EcfmRemoteNpFsMiEcfmHwHandleIntQFailure  unOpCode.sFsMiEcfmHwHandleIntQFailure;
#define  EcfmRemoteNpFsMiEcfmHwInit  unOpCode.sFsMiEcfmHwInit;
#define  EcfmRemoteNpFsMiEcfmHwRegister  unOpCode.sFsMiEcfmHwRegister;
#define  EcfmRemoteNpFsMiEcfmHwSetVlanEtherType  unOpCode.sFsMiEcfmHwSetVlanEtherType;
#define  EcfmRemoteNpFsMiEcfmStartLbmTransaction  unOpCode.sFsMiEcfmStartLbmTransaction;
#define  EcfmRemoteNpFsMiEcfmStartTstTransaction  unOpCode.sFsMiEcfmStartTstTransaction;
#define  EcfmRemoteNpFsMiEcfmStopLbmTransaction  unOpCode.sFsMiEcfmStopLbmTransaction;
#define  EcfmRemoteNpFsMiEcfmStopTstTransaction  unOpCode.sFsMiEcfmStopTstTransaction;
#define  EcfmRemoteNpFsMiEcfmTransmit1Dm  unOpCode.sFsMiEcfmTransmit1Dm;
#define  EcfmRemoteNpFsMiEcfmTransmitDmm  unOpCode.sFsMiEcfmTransmitDmm;
#define  EcfmRemoteNpFsMiEcfmTransmitDmr  unOpCode.sFsMiEcfmTransmitDmr;
#define  EcfmRemoteNpFsMiEcfmTransmitLmm  unOpCode.sFsMiEcfmTransmitLmm;
#define  EcfmRemoteNpFsMiEcfmTransmitLmr  unOpCode.sFsMiEcfmTransmitLmr;
#define  EcfmRemoteNpFsMiEcfmStartLm  unOpCode.sFsMiEcfmStartLm;
#define  EcfmRemoteNpFsMiEcfmStopLm  unOpCode.sFsMiEcfmStopLm;
} tEcfmRemoteNpModInfo;
#endif /* ECFM_WANTED */

#if defined (IP6_WANTED) || defined (LNXIP6_WANTED)
typedef struct {
    UINT4      u4VrId;
    tIp6Addr   u1Ip6Addr;
    UINT4      u4IfIndex;
    tMacAddr   u1HwAddr;
    UINT2    u2VlanId;
    UINT1    u1HwAddrLen;
    UINT1    u1ReachStatus;
  UINT1    au1pad[2];
} tIpv6RemoteNpWrFsNpIpv6NeighCacheAdd;

typedef struct {
    UINT4    u4VrId;
    tIp6Addr u1Ip6Addr;
    UINT4    u4IfIndex;
} tIpv6RemoteNpWrFsNpIpv6NeighCacheDel;

typedef struct {
    UINT4    u4VrId;
    tIp6Addr u1Ip6Addr;
    UINT4    u4IfIndex;
    UINT1    u1HitBitStatus;
    UINT1    au1Pad[3];
}tIpv6RemoteNpWrFsNpIpv6CheckHitOnNDCacheEntry; 

typedef struct {
    UINT4          u4VrId;
    tIp6Addr       u1NextHop;
    UINT4          u4NHType;
    tFsNpIntInfo   IntInfo;
    tIp6Addr       u1Ip6Prefix;
    UINT4          u4HwIntfId[2];
    UINT1          u1PrefixLen;
 UINT1        au1pad[3];
} tIpv6RemoteNpWrFsNpIpv6UcRouteAdd;

typedef struct {
    tFsNpRouteInfo   RouteInfo;
    UINT4     u4VrId;
    tIp6Addr  u1NextHop;
    UINT4     u4IfIndex;
    tIp6Addr  u1Ip6Prefix;
    UINT1     u1PrefixLen; 
 UINT1     au1pad[3];
} tIpv6RemoteNpWrFsNpIpv6UcRouteDelete;

typedef struct {
    UINT4      u4VrId;
    tIp6Addr   u1NextHop;
    UINT4      u4IfIndex;
    tIp6Addr   u1Ip6Prefix;
    UINT1      u1PrefixLen;
 UINT1      au1pad[3];
} tIpv6RemoteNpWrFsNpIpv6RtPresentInFastPath;

typedef struct {
    UINT4  u4VrId;
    UINT4  u4RoutingStatus;
} tIpv6RemoteNpWrFsNpIpv6UcRouting;

typedef struct {
    UINT4    u4VrId;
    UINT4    u4IfIndex;
    UINT4    u4StatsFlag;
    UINT4    u4StatsValue;
} tIpv6RemoteNpWrFsNpIpv6GetStats;

typedef struct {
    UINT4          u4VrId;
    UINT4          u4IfStatus;
    tFsNpIntInfo   IntfInfo;
} tIpv6RemoteNpWrFsNpIpv6IntfStatus;

typedef struct {
    UINT4    u4VrId;
    UINT4    u4IfIndex;
    UINT4    u4AddrType;
    tIp6Addr u1Ip6Addr;
    UINT1    u1PrefixLen;
 UINT1     au1pad[3];
} tIpv6RemoteNpWrFsNpIpv6AddrCreate;

typedef struct {
    UINT4      u4VrId;
    UINT4      u4IfIndex;
    UINT4      u4AddrType;
    tIp6Addr   u1Ip6Addr;
    UINT1      u1PrefixLen;
  UINT1      au1pad[3];
} tIpv6RemoteNpWrFsNpIpv6AddrDelete;

typedef struct {
    UINT4    u4IfIndex;
    UINT2    u2VlanId;
    UINT1    au1pad[2];
} tIpv6RemoteNpWrFsNpIpv6Enable;

typedef struct {
    UINT4    u4IfIndex;
    UINT2    u2VlanId;
    UINT1    au1pad[2];
} tIpv6RemoteNpWrFsNpIpv6Disable;


typedef struct {
    UINT4  u4VrId;
    UINT4  u4IfIndex;
    UINT4  u4TunlType;
    UINT4  u4SrcAddr;
    UINT4  u4DstAddr;
} tIpv6RemoteNpWrFsNpIpv6TunlParamSet;

typedef struct {
    UINT4      u4VrId;
    UINT4      u4IfIndex;
    tMacAddr   u1MacAddr;
    UINT1      u1MacAddrLen;
  UINT1      au1pad[1];
} tIpv6RemoteNpWrFsNpIpv6AddMcastMAC;

typedef struct {
    UINT4      u4VrId;
    UINT4      u4IfIndex;
    tMacAddr   u1MacAddr;
    UINT1      u1MacAddrLen;
  UINT1      au1pad[1];
} tIpv6RemoteNpWrFsNpIpv6DelMcastMAC;

typedef struct {
    tIp6Addr   u1L3Nexthop;
    UINT4      u4IfIndex;
    UINT4      u4IsRemoved;
    tMacAddr   u1MacAddr;
  UINT1      au1pad[2];
} tIpv6RemoteNpWrFsNpIpv6HandleFdbMacEntryChange;

typedef struct {
    tNpNDCacheInput     Nd6NpInParam;
    tNpNDCacheOutput    Nd6NpOutParam;
} tIpv6RemoteNpWrFsNpIpv6NeighCacheGet;

typedef struct {
    tNpNDCacheInput     Nd6NpInParam;
    tNpNDCacheOutput    Nd6NpOutParam;
} tIpv6RemoteNpWrFsNpIpv6NeighCacheGetNext;

typedef struct {
    tNpRtm6Input     Rtm6NpInParam;
    tNpRtm6Output    Rtm6NpOutParam;
} tIpv6RemoteNpWrFsNpIpv6UcGetRoute;

typedef struct Ipv6RemoteNpModInfo {
union {
    tIpv6RemoteNpWrFsNpIpv6NeighCacheAdd  sFsNpIpv6NeighCacheAdd;
    tIpv6RemoteNpWrFsNpIpv6NeighCacheDel  sFsNpIpv6NeighCacheDel;
    tIpv6RemoteNpWrFsNpIpv6CheckHitOnNDCacheEntry  sFsNpIpv6CheckHitOnNDCacheEntry;
    tIpv6RemoteNpWrFsNpIpv6UcRouteAdd  sFsNpIpv6UcRouteAdd;
    tIpv6RemoteNpWrFsNpIpv6UcRouteDelete  sFsNpIpv6UcRouteDelete;
    tIpv6RemoteNpWrFsNpIpv6RtPresentInFastPath  sFsNpIpv6RtPresentInFastPath;
    tIpv6RemoteNpWrFsNpIpv6UcRouting  sFsNpIpv6UcRouting;
    tIpv6RemoteNpWrFsNpIpv6GetStats  sFsNpIpv6GetStats;
    tIpv6RemoteNpWrFsNpIpv6IntfStatus  sFsNpIpv6IntfStatus;
    tIpv6RemoteNpWrFsNpIpv6AddrCreate  sFsNpIpv6AddrCreate;
    tIpv6RemoteNpWrFsNpIpv6AddrDelete  sFsNpIpv6AddrDelete;
    tIpv6RemoteNpWrFsNpIpv6TunlParamSet  sFsNpIpv6TunlParamSet;
    tIpv6RemoteNpWrFsNpIpv6AddMcastMAC  sFsNpIpv6AddMcastMAC;
    tIpv6RemoteNpWrFsNpIpv6DelMcastMAC  sFsNpIpv6DelMcastMAC;
    tIpv6RemoteNpWrFsNpIpv6HandleFdbMacEntryChange  sFsNpIpv6HandleFdbMacEntryChange;
    tIpv6RemoteNpWrFsNpIpv6NeighCacheGet  sFsNpIpv6NeighCacheGet;
    tIpv6RemoteNpWrFsNpIpv6NeighCacheGetNext  sFsNpIpv6NeighCacheGetNext;
    tIpv6RemoteNpWrFsNpIpv6UcGetRoute  sFsNpIpv6UcGetRoute;
    tIpv6RemoteNpWrFsNpIpv6Enable  sFsNpIpv6Enable;
    tIpv6RemoteNpWrFsNpIpv6Disable  sFsNpIpv6Disable;
    }unOpCode;

#define  Ipv6RemoteNpFsNpIpv6NeighCacheAdd  unOpCode.sFsNpIpv6NeighCacheAdd;
#define  Ipv6RemoteNpFsNpIpv6NeighCacheDel  unOpCode.sFsNpIpv6NeighCacheDel;
#define  Ipv6RemoteNpFsNpIpv6CheckHitOnNDCacheEntry  unOpCode.sFsNpIpv6CheckHitOnNDCacheEntry;
#define  Ipv6RemoteNpFsNpIpv6UcRouteAdd  unOpCode.sFsNpIpv6UcRouteAdd;
#define  Ipv6RemoteNpFsNpIpv6UcRouteDelete  unOpCode.sFsNpIpv6UcRouteDelete;
#define  Ipv6RemoteNpFsNpIpv6RtPresentInFastPath  unOpCode.sFsNpIpv6RtPresentInFastPath;
#define  Ipv6RemoteNpFsNpIpv6UcRouting  unOpCode.sFsNpIpv6UcRouting;
#define  Ipv6RemoteNpFsNpIpv6GetStats  unOpCode.sFsNpIpv6GetStats;
#define  Ipv6RemoteNpFsNpIpv6IntfStatus  unOpCode.sFsNpIpv6IntfStatus;
#define  Ipv6RemoteNpFsNpIpv6AddrCreate  unOpCode.sFsNpIpv6AddrCreate;
#define  Ipv6RemoteNpFsNpIpv6AddrDelete  unOpCode.sFsNpIpv6AddrDelete;
#define  Ipv6RemoteNpFsNpIpv6TunlParamSet  unOpCode.sFsNpIpv6TunlParamSet;
#define  Ipv6RemoteNpFsNpIpv6AddMcastMAC  unOpCode.sFsNpIpv6AddMcastMAC;
#define  Ipv6RemoteNpFsNpIpv6DelMcastMAC  unOpCode.sFsNpIpv6DelMcastMAC;
#define  Ipv6RemoteNpFsNpIpv6HandleFdbMacEntryChange  unOpCode.sFsNpIpv6HandleFdbMacEntryChange;
#define  Ipv6RemoteNpFsNpIpv6NeighCacheGet  unOpCode.sFsNpIpv6NeighCacheGet;
#define  Ipv6RemoteNpFsNpIpv6NeighCacheGetNext  unOpCode.sFsNpIpv6NeighCacheGetNext;
#define  Ipv6RemoteNpFsNpIpv6UcGetRoute  unOpCode.sFsNpIpv6UcGetRoute;
#define  Ipv6RemoteNpFsNpIpv6Enable  unOpCode.sFsNpIpv6Enable;
#define  Ipv6RemoteNpFsNpIpv6Disable  unOpCode.sFsNpIpv6Disable;
} tIpv6RemoteNpModInfo;

#endif /* IP6_WANTED || LNXIP6_WANTED */

/* Required arguments list for the mpls NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

#ifdef MPLS_WANTED
#ifndef _MPLSNP_C_
typedef struct {
    UINT1   u1PwCCTypeCapabs;
} tMplsRemoteNpWrFsMplsHwGetPwCtrlChnlCapabilities;

typedef struct {
    UINT4               u4VpnId;
    tMplsHwVcTnlInfo   MplsHwVcInfo;
    UINT4               u4Action;
} tMplsRemoteNpWrFsMplsHwCreatePwVc;

typedef struct {
    tMplsHwVcTnlInfo   MplsHwVcInfo;
    tMplsHwVcTnlInfo   NextMplsHwVcInfo;
} tMplsRemoteNpWrFsMplsHwTraversePwVc;

typedef struct {
    UINT4               u4VpnId;
    tMplsHwVcTnlInfo   MplsHwDelVcInfo;
    UINT4               u4Action;
} tMplsRemoteNpWrFsMplsHwDeletePwVc;

typedef struct {
    tMplsHwIlmInfo   MplsHwIlmInfo;
    UINT4             u4Action;
} tMplsRemoteNpWrFsMplsHwCreateILM;

typedef struct {
    tMplsHwIlmInfo   MplsHwIlmInfo;
    tMplsHwIlmInfo   NextMplsHwIlmInfo;
} tMplsRemoteNpWrFsMplsHwTraverseILM;

typedef struct {
    tMplsHwIlmInfo   MplsHwDelIlmParams;
    UINT4             u4Action;
} tMplsRemoteNpWrFsMplsHwDeleteILM;

typedef struct {
    UINT4               u4VpnId;
    tMplsHwL3FTNInfo   MplsHwL3FTNInfo;
} tMplsRemoteNpWrFsMplsHwCreateL3FTN;

typedef struct {
    tMplsHwL3FTNInfo   MplsHwL3FTNInfo;
    tMplsHwL3FTNInfo   NextMplsHwL3FTNInfo;
} tMplsRemoteNpWrFsMplsHwTraverseL3FTN;

typedef struct {
    tMplsHwL3FTNInfo   MplsHwL3FTNInfo;
    tMplsHwL3FTNInfo   NextMplsHwL3FTNInfo;
} tMplsRemoteNpWrFsMplsHwGetL3FTN;


#ifdef NP_BACKWD_COMPATIBILITY
typedef struct {
    UINT4  u4L3EgrIntf;
} tMplsRemoteNpWrFsMplsHwDeleteL3FTN;

#else
typedef struct {
    UINT4               u4VpnId;
    tMplsHwL3FTNInfo   MplsHwL3FTNInfo;
} tMplsRemoteNpWrFsMplsHwDeleteL3FTN;

#endif
typedef struct {
    UINT4               u4VpnId;
    tMplsHwL3FTNInfo   MplsHwL3FTNInfo;
} tMplsRemoteNpWrFsMplsWpHwDeleteL3FTN;

typedef struct {
    UINT4                 u4Intf;
    tMplsIntfParamsSet   MplsIntfParamsSet;
} tMplsRemoteNpWrFsMplsHwEnableMplsIf;

typedef struct {
    UINT4  u4Intf;
} tMplsRemoteNpWrFsMplsHwDisableMplsIf;

typedef struct {
    UINT4              u4VplsInstance;
    UINT4              u4VpnId;
    tMplsHwVplsInfo   MplsHwVplsVpnInfo;
} tMplsRemoteNpWrFsMplsHwCreateVplsVpn;

typedef struct {
    UINT4              u4VplsInstance;
    UINT4              u4VpnId;
    tMplsHwVplsInfo   MplsHwVplsVpnInfo;
} tMplsRemoteNpWrFsMplsHwDeleteVplsVpn;

typedef struct {
    UINT4              u4VplsInstance;
    UINT4              u4VpnId;
    tMplsHwVplsInfo   MplsHwVplsVcInfo;
    UINT4              u4Action;
} tMplsRemoteNpWrFsMplsHwVplsAddPwVc;

typedef struct {
    UINT4              u4VplsInstance;
    UINT4              u4VpnId;
    tMplsHwVplsInfo   MplsHwVplsVcInfo;
    UINT4              u4Action;
} tMplsRemoteNpWrFsMplsHwVplsDeletePwVc;

typedef struct {
    UINT4              u4VplsInstance;
    UINT4              u4VpnId;
    tMplsHwVplsInfo   MplsHwVplsVpnInfo;
} tMplsRemoteNpWrFsMplsHwVplsFlushMac;
#ifdef HVPLS_WANTED
typedef struct {
    UINT4              u4VpnId;
    tMplsHwVplsInfo   MplsHwVplsVpnInfo;
} tMplsRemoteNpWrFsMplsRegisterFwdAlarm;
typedef struct {
    UINT4              u4VpnId;
} tMplsRemoteNpWrFsMplsDeRegisterFwdAlarm;
#endif

typedef struct {
     tMplsHwMplsIntInfo   MplsHwMplsIntInfo;
} tMplsRemoteNpWrNpMplsCreateMplsInterface;

typedef struct {
    UINT4             u4VrId;
    UINT4             u4IpDestAddr;
    UINT4             u4IpSubNetMask;
    tFsNpNextHopInfo  routeEntry;
    UINT1            bu1TblFull;
    UINT1            au1Pad[3];
} tMplsRemoteNpWrFsMplsHwAddMplsRoute;
 
typedef struct {
    UINT4           u4VrId;
    UINT4           u4NHType;
    tFsNpIntInfo   IntInfo;
    tIp6Addr        u1Ip6Prefix;
    tIp6Addr        u1NextHop;
    UINT1           u1PrefixLen;
    UINT1           au1Pad[3];
} tMplsRemoteNpWrFsMplsHwAddMplsIpv6Route;

typedef struct {
    UINT4    u4VrId;
    UINT4    u4IfIndex;
    tIp6Addr u1Ip6Prefix;
    tFsNpRouteInfo RouteInfo;
    tIp6Addr u1NextHop;
    UINT1    u1PrefixLen;
    UINT1    au1Pad[3];
} tMplsRemoteNpWrFsMplsHwDeleteMplsIpv6Route;

typedef struct{
    tMplsInputParams   InputParams;
    tStatsType          StatsType;
    tStatsInfo         StatsInfo;
} tMplsRemoteNpWrFsMplsHwGetMplsStats;

typedef struct {
    UINT4              u4VplsInstance;
    UINT4              u4VpnId;
    tMplsHwVplsInfo   MplsHwVplsVcInfo;
    UINT4              u4Action;
} tMplsRemoteNpWrFsMplsHwAddVpnAc;

typedef struct {
    UINT4              u4VplsInstance;
    UINT4              u4VpnId;
    tMplsHwVplsInfo   MplsHwVplsVcInfo;
    UINT4              u4Action;
} tMplsRemoteNpWrFsMplsHwDeleteVpnAc;

typedef struct {
    UINT4             u4P2mpId;
    tMplsHwIlmInfo   MplsHwP2mpIlmInfo;
} tMplsRemoteNpWrFsMplsHwP2mpAddILM;

typedef struct {
    UINT4             u4P2mpId;
    tMplsHwIlmInfo   MplsHwP2mpIlmInfo;
} tMplsRemoteNpWrFsMplsHwP2mpRemoveILM;

typedef struct {
    UINT4             u4P2mpId;
    tMplsHwIlmInfo   MplsHwP2mpIlmInfo;
    tMplsHwIlmInfo   NextMplsHwP2mpIlmInfo;
} tMplsRemoteNpWrFsMplsHwP2mpTraverseILM;

typedef struct {
    UINT4  u4P2mpId;
    UINT4  u4Action;
} tMplsRemoteNpWrFsMplsHwP2mpAddBud;

typedef struct {
    UINT4  u4P2mpId;
    UINT4  u4Action;
} tMplsRemoteNpWrFsMplsHwP2mpRemoveBud;

typedef struct {
    UINT4               u4VpnId;
    UINT4               u4OperActVpnId;
    tMplsHwVcTnlInfo   MplsHwVcInfo;
} tMplsRemoteNpWrFsMplsHwModifyPwVc;

#ifdef MPLS_L3VPN_WANTED
typedef struct {
    tMplsHwL3vpnIgressInfo   MplsHwL3vpnIgressInfo;
} tMplsRemoteNpWrFsMplsHwL3vpnIngressMap;

typedef struct {
    tMplsHwL3vpnEgressInfo   MplsHwL3vpnEgressInfo;
} tMplsRemoteNpWrFsMplsHwL3vpnEgressMap;

#endif /* MPLS_WANTED */
#endif
#endif
#ifdef MPLS_WANTED
#ifndef _MPLSNPX_C_
typedef struct {
    tMbsmSlotInfo   SlotInfo;
} tMplsRemoteNpWrFsMplsMbsmHwEnableMpls;

typedef struct {
    tMbsmSlotInfo   SlotInfo;
} tMplsRemoteNpWrFsMplsMbsmHwDisableMpls;

typedef struct {
    UINT1   u1PwCCTypeCapabs;
} tMplsRemoteNpWrFsMplsMbsmHwGetPwCtrlChnlCapabilities;

typedef struct {
    UINT4               u4VpnId;
    tMplsHwVcTnlInfo   MplsHwVcInfo;
    UINT4               u4Action;
} tMplsRemoteNpWrFsMplsMbsmHwCreatePwVc;

typedef struct {
    UINT4               u4VpnId;
    tMplsHwVcTnlInfo   MplsHwDelVcInfo;
    UINT4               u4Action;
} tMplsRemoteNpWrFsMplsMbsmHwDeletePwVc;

typedef struct {
    tMplsHwIlmInfo   MplsHwIlmInfo;
    UINT4             u4Action;
} tMplsRemoteNpWrFsMplsMbsmHwCreateILM;

typedef struct {
    tMplsHwIlmInfo   MplsHwDelIlmParams;
    UINT4             u4Action;
} tMplsRemoteNpWrFsMplsMbsmHwDeleteILM;

typedef struct {
    UINT4               u4VpnId;
    tMplsHwL3FTNInfo   MplsHwL3FTNInfo;
} tMplsRemoteNpWrFsMplsMbsmHwCreateL3FTN;

typedef struct {
    UINT4            u4L3EgrIntf;
} tMplsRemoteNpWrFsMplsMbsmHwDeleteL3FTN;

typedef struct {
    UINT4              u4VplsInstance;
    UINT4              u4VpnId;
    tMplsHwVplsInfo   MplsHwVplsVpnInfo;
} tMplsRemoteNpWrFsMplsMbsmHwCreateVplsVpn;

typedef struct {
    UINT4              u4VplsInstance;
    UINT4              u4VpnId;
    tMplsHwVplsInfo   MplsHwVplsVpnInfo;
} tMplsRemoteNpWrFsMplsMbsmHwDeleteVplsVpn;

typedef struct {
    UINT4              u4VplsInstance;
    UINT4              u4VpnId;
    tMplsHwVplsInfo   MplsHwVplsVcInfo;
    UINT4              u4Action;
} tMplsRemoteNpWrFsMplsMbsmHwVplsAddPwVc;

typedef struct {
    UINT4              u4VplsInstance;
    UINT4              u4VpnId;
    tMplsHwVplsInfo   MplsHwVplsVcInfo;
    UINT4              u4Action;
} tMplsRemoteNpWrFsMplsMbsmHwVplsDeletePwVc;

typedef struct {
    UINT4              u4VplsInstance;
    UINT4              u4VpnId;
    tMplsHwVplsInfo   MplsHwVplsVcInfo;
    UINT4              u4Action;
} tMplsRemoteNpWrFsMplsMbsmHwAddVpnAc;

typedef struct {
    UINT4              u4VplsInstance;
    UINT4              u4VpnId;
    tMplsHwVplsInfo   MplsHwVplsVcInfo;
    UINT4              u4Action;
} tMplsRemoteNpWrFsMplsMbsmHwDeleteVpnAc;

typedef struct {
    tMplsHwMplsIntInfo   MplsHwMplsIntInfo; 
} tMplsRemoteNpWrNpMplsMbsmCreateMplsInterface;

typedef struct {
    UINT4             u4P2mpId;
    tMplsHwIlmInfo   MplsHwP2mpIlmInfo;
} tMplsRemoteNpWrFsMplsMbsmHwP2mpAddILM;

typedef struct {
    UINT4             u4P2mpId;
    tMplsHwIlmInfo   MplsHwP2mpIlmInfo;
} tMplsRemoteNpWrFsMplsMbsmHwP2mpRemoveILM;

#ifdef MPLS_L3VPN_WANTED
typedef struct {
    tMplsHwL3vpnIgressInfo   MplsHwL3vpnIgressInfo;
} tMplsRemoteNpWrFsMplsMbsmHwL3vpnIngressMap;

typedef struct {
    tMplsHwL3vpnEgressInfo   MplsHwL3vpnEgressInfo;
} tMplsRemoteNpWrFsMplsMbsmHwL3vpnEgressMap;

#endif /* MPLS_WANTED */
#endif
#endif
typedef struct MplsRemoteNpModInfo {
union {
#ifdef MPLS_WANTED
#ifndef _MPLSNP_C_
    tMplsRemoteNpWrFsMplsHwGetPwCtrlChnlCapabilities  sFsMplsHwGetPwCtrlChnlCapabilities;
    tMplsRemoteNpWrFsMplsHwCreatePwVc  sFsMplsHwCreatePwVc;
    tMplsRemoteNpWrFsMplsHwTraversePwVc  sFsMplsHwTraversePwVc;
    tMplsRemoteNpWrFsMplsHwDeletePwVc  sFsMplsHwDeletePwVc;
    tMplsRemoteNpWrFsMplsHwCreateILM  sFsMplsHwCreateILM;
    tMplsRemoteNpWrFsMplsHwTraverseILM  sFsMplsHwTraverseILM;
    tMplsRemoteNpWrFsMplsHwDeleteILM  sFsMplsHwDeleteILM;
    tMplsRemoteNpWrFsMplsHwCreateL3FTN  sFsMplsHwCreateL3FTN;
    tMplsRemoteNpWrFsMplsHwTraverseL3FTN  sFsMplsHwTraverseL3FTN;
    tMplsRemoteNpWrFsMplsHwGetL3FTN  sFsMplsHwGetL3FTN;
#ifdef NP_BACKWD_COMPATIBILITY
    tMplsRemoteNpWrFsMplsHwDeleteL3FTN  sFsMplsHwDeleteL3FTN;
#else
    tMplsRemoteNpWrFsMplsHwDeleteL3FTN  sFsMplsHwDeleteL3FTN;
#endif
    tMplsRemoteNpWrFsMplsWpHwDeleteL3FTN  sFsMplsWpHwDeleteL3FTN;
    tMplsRemoteNpWrFsMplsHwEnableMplsIf  sFsMplsHwEnableMplsIf;
    tMplsRemoteNpWrFsMplsHwDisableMplsIf  sFsMplsHwDisableMplsIf;
    tMplsRemoteNpWrFsMplsHwCreateVplsVpn  sFsMplsHwCreateVplsVpn;
    tMplsRemoteNpWrFsMplsHwDeleteVplsVpn  sFsMplsHwDeleteVplsVpn;
    tMplsRemoteNpWrFsMplsHwVplsAddPwVc  sFsMplsHwVplsAddPwVc;
    tMplsRemoteNpWrFsMplsHwVplsDeletePwVc  sFsMplsHwVplsDeletePwVc;
    tMplsRemoteNpWrFsMplsHwVplsFlushMac  sFsMplsHwVplsFlushMac;
#ifdef HVPLS_WANTED
    tMplsRemoteNpWrFsMplsRegisterFwdAlarm  sFsMplsRegisterFwdAlarm;
    tMplsRemoteNpWrFsMplsDeRegisterFwdAlarm  sFsMplsDeRegisterFwdAlarm;
#endif
    tMplsRemoteNpWrNpMplsCreateMplsInterface  sNpMplsCreateMplsInterface;
    tMplsRemoteNpWrFsMplsHwAddMplsRoute  sFsMplsHwAddMplsRoute;
     tMplsRemoteNpWrFsMplsHwAddMplsIpv6Route  sFsMplsHwAddMplsIpv6Route;
    tMplsRemoteNpWrFsMplsHwDeleteMplsIpv6Route  sFsMplsHwDeleteMplsIpv6Route;
    tMplsRemoteNpWrFsMplsHwGetMplsStats  sFsMplsHwGetMplsStats;
    tMplsRemoteNpWrFsMplsHwAddVpnAc  sFsMplsHwAddVpnAc;
    tMplsRemoteNpWrFsMplsHwDeleteVpnAc  sFsMplsHwDeleteVpnAc;
    tMplsRemoteNpWrFsMplsHwP2mpAddILM  sFsMplsHwP2mpAddILM;
    tMplsRemoteNpWrFsMplsHwP2mpRemoveILM  sFsMplsHwP2mpRemoveILM;
    tMplsRemoteNpWrFsMplsHwP2mpTraverseILM  sFsMplsHwP2mpTraverseILM;
    tMplsRemoteNpWrFsMplsHwP2mpAddBud  sFsMplsHwP2mpAddBud;
    tMplsRemoteNpWrFsMplsHwP2mpRemoveBud  sFsMplsHwP2mpRemoveBud;
    tMplsRemoteNpWrFsMplsHwModifyPwVc  sFsMplsHwModifyPwVc;
#ifdef MPLS_L3VPN_WANTED
    tMplsRemoteNpWrFsMplsHwL3vpnIngressMap  sFsMplsHwL3vpnIngressMap;
    tMplsRemoteNpWrFsMplsHwL3vpnEgressMap  sFsMplsHwL3vpnEgressMap;
#endif /* MPLS_WANTED */
#endif
#endif
#ifdef MPLS_WANTED
#ifndef _MPLSNPX_C_
    tMplsRemoteNpWrFsMplsMbsmHwEnableMpls  sFsMplsMbsmHwEnableMpls;
    tMplsRemoteNpWrFsMplsMbsmHwDisableMpls  sFsMplsMbsmHwDisableMpls;
    tMplsRemoteNpWrFsMplsMbsmHwGetPwCtrlChnlCapabilities  sFsMplsMbsmHwGetPwCtrlChnlCapabilities;
    tMplsRemoteNpWrFsMplsMbsmHwCreatePwVc  sFsMplsMbsmHwCreatePwVc;
    tMplsRemoteNpWrFsMplsMbsmHwDeletePwVc  sFsMplsMbsmHwDeletePwVc;
    tMplsRemoteNpWrFsMplsMbsmHwCreateILM  sFsMplsMbsmHwCreateILM;
    tMplsRemoteNpWrFsMplsMbsmHwDeleteILM  sFsMplsMbsmHwDeleteILM;
    tMplsRemoteNpWrFsMplsMbsmHwCreateL3FTN  sFsMplsMbsmHwCreateL3FTN;
    tMplsRemoteNpWrFsMplsMbsmHwDeleteL3FTN  sFsMplsMbsmHwDeleteL3FTN;
    tMplsRemoteNpWrFsMplsMbsmHwCreateVplsVpn  sFsMplsMbsmHwCreateVplsVpn;
    tMplsRemoteNpWrFsMplsMbsmHwDeleteVplsVpn  sFsMplsMbsmHwDeleteVplsVpn;
    tMplsRemoteNpWrFsMplsMbsmHwVplsAddPwVc  sFsMplsMbsmHwVplsAddPwVc;
    tMplsRemoteNpWrFsMplsMbsmHwVplsDeletePwVc  sFsMplsMbsmHwVplsDeletePwVc;
    tMplsRemoteNpWrFsMplsMbsmHwAddVpnAc  sFsMplsMbsmHwAddVpnAc;
    tMplsRemoteNpWrFsMplsMbsmHwDeleteVpnAc  sFsMplsMbsmHwDeleteVpnAc;
    tMplsRemoteNpWrNpMplsMbsmCreateMplsInterface  sNpMplsMbsmCreateMplsInterface;
    tMplsRemoteNpWrFsMplsMbsmHwP2mpAddILM  sFsMplsMbsmHwP2mpAddILM;
    tMplsRemoteNpWrFsMplsMbsmHwP2mpRemoveILM  sFsMplsMbsmHwP2mpRemoveILM;
#ifdef MPLS_L3VPN_WANTED
    tMplsRemoteNpWrFsMplsMbsmHwL3vpnIngressMap  sFsMplsMbsmHwL3vpnIngressMap;
    tMplsRemoteNpWrFsMplsMbsmHwL3vpnEgressMap  sFsMplsMbsmHwL3vpnEgressMap;
#endif /* MPLS_WANTED */
#endif
#endif
    }unOpCode;

#ifdef MPLS_WANTED
#ifndef _MPLSNP_C_
#define  MplsRemoteNpFsMplsHwGetPwCtrlChnlCapabilities  unOpCode.sFsMplsHwGetPwCtrlChnlCapabilities;
#define  MplsRemoteNpFsMplsHwCreatePwVc  unOpCode.sFsMplsHwCreatePwVc;
#define  MplsRemoteNpFsMplsHwTraversePwVc  unOpCode.sFsMplsHwTraversePwVc;
#define  MplsRemoteNpFsMplsHwDeletePwVc  unOpCode.sFsMplsHwDeletePwVc;
#define  MplsRemoteNpFsMplsHwCreateILM  unOpCode.sFsMplsHwCreateILM;
#define  MplsRemoteNpFsMplsHwTraverseILM  unOpCode.sFsMplsHwTraverseILM;
#define  MplsRemoteNpFsMplsHwDeleteILM  unOpCode.sFsMplsHwDeleteILM;
#define  MplsRemoteNpFsMplsHwCreateL3FTN  unOpCode.sFsMplsHwCreateL3FTN;
#define  MplsRemoteNpFsMplsHwTraverseL3FTN  unOpCode.sFsMplsHwTraverseL3FTN;
#define  MplsRemoteNpFsMplsHwGetL3FTN  unOpCode.sFsMplsHwGetL3FTN;
#ifdef NP_BACKWD_COMPATIBILITY
#define  MplsRemoteNpFsMplsHwDeleteL3FTN  unOpCode.sFsMplsHwDeleteL3FTN;
#else
#define  MplsRemoteNpFsMplsHwDeleteL3FTN  unOpCode.sFsMplsHwDeleteL3FTN;
#endif
#define  MplsRemoteNpFsMplsWpHwDeleteL3FTN  unOpCode.sFsMplsWpHwDeleteL3FTN;
#define  MplsRemoteNpFsMplsHwEnableMplsIf  unOpCode.sFsMplsHwEnableMplsIf;
#define  MplsRemoteNpFsMplsHwDisableMplsIf  unOpCode.sFsMplsHwDisableMplsIf;
#define  MplsRemoteNpFsMplsHwCreateVplsVpn  unOpCode.sFsMplsHwCreateVplsVpn;
#define  MplsRemoteNpFsMplsHwDeleteVplsVpn  unOpCode.sFsMplsHwDeleteVplsVpn;
#define  MplsRemoteNpFsMplsHwVplsAddPwVc  unOpCode.sFsMplsHwVplsAddPwVc;
#define  MplsRemoteNpFsMplsHwVplsDeletePwVc  unOpCode.sFsMplsHwVplsDeletePwVc;
#define  MplsRemoteNpFsMplsHwVplsFlushMac  unOpCode.sFsMplsHwVplsFlushMac;
#ifdef HVPLS_WANTED
#define  MplsRemoteNpFsMplsRegisterFwdAlarm  unOpCode.sFsMplsRegisterFwdAlarm;
#define  MplsRemoteNpFsMplsDeRegisterFwdAlarm  unOpCode.sFsMplsDeRegisterFwdAlarm;
#endif
#define  MplsRemoteNpNpMplsCreateMplsInterface  unOpCode.sNpMplsCreateMplsInterface;
#define  MplsRemoteNpFsMplsHwAddMplsRoute  unOpCode.sFsMplsHwAddMplsRoute;
#define  MplsRemoteNpFsMplsHwAddMplsIpv6Route  unOpCode.sFsMplsHwAddMplsIpv6Route;
#define  MplsRemoteNpFsMplsHwDeleteMplsIpv6Route  unOpCode.sFsMplsHwDeleteMplsIpv6Route;
#define  MplsRemoteNpFsMplsHwGetMplsStats  unOpCode.sFsMplsHwGetMplsStats;
#define  MplsRemoteNpFsMplsHwAddVpnAc  unOpCode.sFsMplsHwAddVpnAc;
#define  MplsRemoteNpFsMplsHwDeleteVpnAc  unOpCode.sFsMplsHwDeleteVpnAc;
#define  MplsRemoteNpFsMplsHwP2mpAddILM  unOpCode.sFsMplsHwP2mpAddILM;
#define  MplsRemoteNpFsMplsHwP2mpRemoveILM  unOpCode.sFsMplsHwP2mpRemoveILM;
#define  MplsRemoteNpFsMplsHwP2mpTraverseILM  unOpCode.sFsMplsHwP2mpTraverseILM;
#define  MplsRemoteNpFsMplsHwP2mpAddBud  unOpCode.sFsMplsHwP2mpAddBud;
#define  MplsRemoteNpFsMplsHwP2mpRemoveBud  unOpCode.sFsMplsHwP2mpRemoveBud;
#define  MplsRemoteNpFsMplsHwModifyPwVc  unOpCode.sFsMplsHwModifyPwVc;
#ifdef MPLS_L3VPN_WANTED
#define  MplsRemoteNpFsMplsHwL3vpnIngressMap  unOpCode.sFsMplsHwL3vpnIngressMap;
#define  MplsRemoteNpFsMplsHwL3vpnEgressMap  unOpCode.sFsMplsHwL3vpnEgressMap;
#endif /* MPLS_WANTED */
#endif
#endif
#ifdef MPLS_WANTED
#ifndef _MPLSNPX_C_
#define  MplsRemoteNpFsMplsMbsmHwEnableMpls  unOpCode.sFsMplsMbsmHwEnableMpls;
#define  MplsRemoteNpFsMplsMbsmHwDisableMpls  unOpCode.sFsMplsMbsmHwDisableMpls;
#define  MplsRemoteNpFsMplsMbsmHwGetPwCtrlChnlCapabilities  unOpCode.sFsMplsMbsmHwGetPwCtrlChnlCapabilities;
#define  MplsRemoteNpFsMplsMbsmHwCreatePwVc  unOpCode.sFsMplsMbsmHwCreatePwVc;
#define  MplsRemoteNpFsMplsMbsmHwDeletePwVc  unOpCode.sFsMplsMbsmHwDeletePwVc;
#define  MplsRemoteNpFsMplsMbsmHwCreateILM  unOpCode.sFsMplsMbsmHwCreateILM;
#define  MplsRemoteNpFsMplsMbsmHwDeleteILM  unOpCode.sFsMplsMbsmHwDeleteILM;
#define  MplsRemoteNpFsMplsMbsmHwCreateL3FTN  unOpCode.sFsMplsMbsmHwCreateL3FTN;
#define  MplsRemoteNpFsMplsMbsmHwDeleteL3FTN  unOpCode.sFsMplsMbsmHwDeleteL3FTN;
#define  MplsRemoteNpFsMplsMbsmHwCreateVplsVpn  unOpCode.sFsMplsMbsmHwCreateVplsVpn;
#define  MplsRemoteNpFsMplsMbsmHwDeleteVplsVpn  unOpCode.sFsMplsMbsmHwDeleteVplsVpn;
#define  MplsRemoteNpFsMplsMbsmHwVplsAddPwVc  unOpCode.sFsMplsMbsmHwVplsAddPwVc;
#define  MplsRemoteNpFsMplsMbsmHwVplsDeletePwVc  unOpCode.sFsMplsMbsmHwVplsDeletePwVc;
#define  MplsRemoteNpFsMplsMbsmHwAddVpnAc  unOpCode.sFsMplsMbsmHwAddVpnAc;
#define  MplsRemoteNpFsMplsMbsmHwDeleteVpnAc  unOpCode.sFsMplsMbsmHwDeleteVpnAc;
#define  MplsRemoteNpNpMplsMbsmCreateMplsInterface  unOpCode.sNpMplsMbsmCreateMplsInterface;
#define  MplsRemoteNpFsMplsMbsmHwP2mpAddILM  unOpCode.sFsMplsMbsmHwP2mpAddILM;
#define  MplsRemoteNpFsMplsMbsmHwP2mpRemoveILM  unOpCode.sFsMplsMbsmHwP2mpRemoveILM;
#ifdef MPLS_L3VPN_WANTED
#define  MplsRemoteNpFsMplsMbsmHwL3vpnIngressMap  unOpCode.sFsMplsMbsmHwL3vpnIngressMap;
#define  MplsRemoteNpFsMplsMbsmHwL3vpnEgressMap  unOpCode.sFsMplsMbsmHwL3vpnEgressMap;
#endif /* MPLS_WANTED */
#endif
#endif
} tMplsRemoteNpModInfo;


/* Required arguments list for the ipmc NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

#ifdef FS_NPAPI
typedef struct {
    UINT4  u4VlanId;
} tIpmcRemoteNpWrFsNpIpv4VlanMcInit;

typedef struct {
    UINT4  u4Index;
} tIpmcRemoteNpWrFsNpIpv4VlanMcDeInit;

typedef struct {
    tRPortInfo  PortInfo;
} tIpmcRemoteNpWrFsNpIpv4RportMcInit;

typedef struct {
    tRPortInfo  PortInfo;
} tIpmcRemoteNpWrFsNpIpv4RportMcDeInit;

typedef struct {
    tMcRtEntry         rtEntry;
    tMcDownStreamIf   DownStreamIf;
    UINT4              u4VrId;
    UINT4              u4GrpAddr;
    UINT4              u4GrpPrefix;
    UINT4              u4SrcIpAddr;
    UINT4              u4SrcIpPrefix;
    UINT2              u2NoOfDownStreamIf;
    UINT1              u1CallerId;
    UINT1              au1Pad[1];
} tIpmcRemoteNpWrFsNpIpv4McAddRouteEntry;

typedef struct {
    tMcRtEntry         rtEntry;
    tMcDownStreamIf   DownStreamIf;
    UINT4              u4VrId;
    UINT4              u4GrpAddr;
    UINT4              u4GrpPrefix;
    UINT4              u4SrcIpAddr;
    UINT4              u4SrcIpPrefix;
    UINT2              u2NoOfDownStreamIf;
    UINT1              au1Pad[2];
} tIpmcRemoteNpWrFsNpIpv4McDelRouteEntry;

typedef struct {
    UINT4  u4VrId;
} tIpmcRemoteNpWrFsNpIpv4McClearAllRoutes;

typedef struct {
    tMcRtEntry  rtEntry;
    UINT4       u4GrpAddr;
    UINT4       u4SrcAddr;
    UINT2       u2GenRtrId;
    UINT1       au1Pad[2];
} tIpmcRemoteNpWrFsNpIpv4McAddCpuPort;

typedef struct {
    tMcRtEntry  rtEntry;
    UINT4       u4GrpAddr;
    UINT4       u4SrcAddr;
    UINT2       u2GenRtrId;
    UINT1       au1Pad[2];
} tIpmcRemoteNpWrFsNpIpv4McDelCpuPort;

typedef struct {
    UINT4    u4SrcIpAddr;
    UINT4    u4GrpAddr;
    UINT4    u4Iif;
    UINT4   u4HitStatus;
    UINT2    u2VlanId;
    UINT1       au1Pad[2];
} tIpmcRemoteNpWrFsNpIpv4McGetHitStatus;

typedef struct {
    UINT4            u4VrId;
    UINT4            u4GrpAddr;
    UINT4            u4SrcIpAddr;
    tMcRtEntry       rtEntry;
    tMcDownStreamIf  downStreamIf;
} tIpmcRemoteNpWrFsNpIpv4McUpdateOifVlanEntry;

typedef struct {
    UINT4       u4VrId;
    UINT4       u4GrpAddr;
    UINT4       u4SrcIpAddr;
    tMcRtEntry  rtEntry;
} tIpmcRemoteNpWrFsNpIpv4McUpdateIifVlanEntry;

typedef struct {
    UINT4    u4VrId;
    UINT4    u4GrpAddr;
    UINT4    u4SrcAddr;
    INT4     i4StatType;
    UINT4   u4RetValue;
} tIpmcRemoteNpWrFsNpIpv4GetMRouteStats;

typedef struct {
    UINT4                   u4VrId;
    UINT4                   u4GrpAddr;
    UINT4                   u4SrcAddr;
    INT4                    i4StatType;
    tSNMP_COUNTER64_TYPE   U8RetValue;
} tIpmcRemoteNpWrFsNpIpv4GetMRouteHCStats;

typedef struct {
    UINT4    u4VrId;
    UINT4    u4GrpAddr;
    UINT4    u4SrcAddr;
    INT4     i4OutIfIndex;
    INT4     i4StatType;
    UINT4   u4RetValue;
} tIpmcRemoteNpWrFsNpIpv4GetMNextHopStats;

typedef struct {
    UINT4    u4VrId;
    INT4     i4IfIndex;
    INT4     i4StatType;
    UINT4   u4RetValue;
} tIpmcRemoteNpWrFsNpIpv4GetMIfaceStats;

typedef struct {
    UINT4                   u4VrId;
    INT4                    i4IfIndex;
    INT4                    i4StatType;
    tSNMP_COUNTER64_TYPE   U8RetValue;
} tIpmcRemoteNpWrFsNpIpv4GetMIfaceHCStats;

typedef struct {
    UINT4  u4VrId;
    INT4   i4IfIndex;
    INT4   i4TtlTreshold;
} tIpmcRemoteNpWrFsNpIpv4SetMIfaceTtlTreshold;

typedef struct {
    UINT4  u4VrId;
    INT4   i4IfIndex;
    INT4   i4RateLimit;
} tIpmcRemoteNpWrFsNpIpv4SetMIfaceRateLimit;

typedef struct {
    tNpL3McastEntry   NpL3McastEntry;
} tIpmcRemoteNpWrFsNpIpvXHwGetMcastEntry;

#ifdef PIM_WANTED
typedef struct {
    UINT4  u4GrpAddr;
    UINT4  u4SrcAddr;
    UINT2  u2VlanId;
    UINT1  au1Pad[2];
} tIpmcRemoteNpWrFsNpIpv4McClearHitBit;

#endif
typedef struct {
    tMcRpfDFInfo   McRpfDFInfo;
} tIpmcRemoteNpWrFsNpIpv4McRpfDFInfo;

#endif
#ifdef MBSM_WANTED
typedef struct {
    tMbsmSlotInfo   SlotInfo;
} tIpmcRemoteNpWrFsNpIpv4MbsmMcInit;

typedef struct {
    tMcDownStreamIf   DownStreamIf;
    tMbsmSlotInfo     SlotInfo;
    tMcRtEntry         rtEntry;
    UINT4              u4VrId;
    UINT4              u4GrpAddr;
    UINT4              u4GrpPrefix;
    UINT4              u4SrcIpAddr;
    UINT4              u4SrcIpPrefix;
    UINT2              u2NoOfDownStreamIf;
    UINT1              u1CallerId;
    UINT1              au1Pad[1];
} tIpmcRemoteNpWrFsNpIpv4MbsmMcAddRouteEntry;

#ifdef PIM_WANTED
typedef struct {
    tMbsmSlotInfo   SlotInfo;
} tIpmcRemoteNpWrFsPimMbsmNpInitHw;

#endif /* PIM_WANTED */
#ifdef DVMRP_WANTED
typedef struct {
    tMbsmSlotInfo   SlotInfo;
} tIpmcRemoteNpWrFsDvmrpMbsmNpInitHw;

#endif
#endif /* DVMRP_WANTED */
typedef struct IpmcRemoteNpModInfo {
union {
#ifdef FS_NPAPI
    tIpmcRemoteNpWrFsNpIpv4VlanMcInit  sFsNpIpv4VlanMcInit;
    tIpmcRemoteNpWrFsNpIpv4VlanMcDeInit  sFsNpIpv4VlanMcDeInit;
    tIpmcRemoteNpWrFsNpIpv4RportMcInit  sFsNpIpv4RportMcInit;
    tIpmcRemoteNpWrFsNpIpv4RportMcDeInit  sFsNpIpv4RportMcDeInit;
    tIpmcRemoteNpWrFsNpIpv4McAddRouteEntry  sFsNpIpv4McAddRouteEntry;
    tIpmcRemoteNpWrFsNpIpv4McDelRouteEntry  sFsNpIpv4McDelRouteEntry;
    tIpmcRemoteNpWrFsNpIpv4McClearAllRoutes  sFsNpIpv4McClearAllRoutes;
    tIpmcRemoteNpWrFsNpIpv4McAddCpuPort  sFsNpIpv4McAddCpuPort;
    tIpmcRemoteNpWrFsNpIpv4McDelCpuPort  sFsNpIpv4McDelCpuPort;
    tIpmcRemoteNpWrFsNpIpv4McGetHitStatus  sFsNpIpv4McGetHitStatus;
    tIpmcRemoteNpWrFsNpIpv4McUpdateOifVlanEntry  sFsNpIpv4McUpdateOifVlanEntry;
    tIpmcRemoteNpWrFsNpIpv4McUpdateIifVlanEntry  sFsNpIpv4McUpdateIifVlanEntry;
    tIpmcRemoteNpWrFsNpIpv4GetMRouteStats  sFsNpIpv4GetMRouteStats;
    tIpmcRemoteNpWrFsNpIpv4GetMRouteHCStats  sFsNpIpv4GetMRouteHCStats;
    tIpmcRemoteNpWrFsNpIpv4GetMNextHopStats  sFsNpIpv4GetMNextHopStats;
    tIpmcRemoteNpWrFsNpIpv4GetMIfaceStats  sFsNpIpv4GetMIfaceStats;
    tIpmcRemoteNpWrFsNpIpv4GetMIfaceHCStats  sFsNpIpv4GetMIfaceHCStats;
    tIpmcRemoteNpWrFsNpIpv4SetMIfaceTtlTreshold  sFsNpIpv4SetMIfaceTtlTreshold;
    tIpmcRemoteNpWrFsNpIpv4SetMIfaceRateLimit  sFsNpIpv4SetMIfaceRateLimit;
    tIpmcRemoteNpWrFsNpIpvXHwGetMcastEntry  sFsNpIpvXHwGetMcastEntry;
#ifdef PIM_WANTED
    tIpmcRemoteNpWrFsNpIpv4McClearHitBit  sFsNpIpv4McClearHitBit;
#endif
    tIpmcRemoteNpWrFsNpIpv4McRpfDFInfo  sFsNpIpv4McRpfDFInfo;
#endif
#ifdef MBSM_WANTED
    tIpmcRemoteNpWrFsNpIpv4MbsmMcInit  sFsNpIpv4MbsmMcInit;
    tIpmcRemoteNpWrFsNpIpv4MbsmMcAddRouteEntry  sFsNpIpv4MbsmMcAddRouteEntry;
#ifdef PIM_WANTED
    tIpmcRemoteNpWrFsPimMbsmNpInitHw  sFsPimMbsmNpInitHw;
#endif /* PIM_WANTED */
#ifdef DVMRP_WANTED
    tIpmcRemoteNpWrFsDvmrpMbsmNpInitHw  sFsDvmrpMbsmNpInitHw;
#endif
#endif /* DVMRP_WANTED */
    }unOpCode;

#ifdef FS_NPAPI
#define  IpmcRemoteNpFsNpIpv4VlanMcInit  unOpCode.sFsNpIpv4VlanMcInit;
#define  IpmcRemoteNpFsNpIpv4VlanMcDeInit  unOpCode.sFsNpIpv4VlanMcDeInit;
#define  IpmcRemoteNpFsNpIpv4RportMcInit  unOpCode.sFsNpIpv4RportMcInit;
#define  IpmcRemoteNpFsNpIpv4RportMcDeInit  unOpCode.sFsNpIpv4RportMcDeInit;
#define  IpmcRemoteNpFsNpIpv4McAddRouteEntry  unOpCode.sFsNpIpv4McAddRouteEntry;
#define  IpmcRemoteNpFsNpIpv4McDelRouteEntry  unOpCode.sFsNpIpv4McDelRouteEntry;
#define  IpmcRemoteNpFsNpIpv4McClearAllRoutes  unOpCode.sFsNpIpv4McClearAllRoutes;
#define  IpmcRemoteNpFsNpIpv4McAddCpuPort  unOpCode.sFsNpIpv4McAddCpuPort;
#define  IpmcRemoteNpFsNpIpv4McDelCpuPort  unOpCode.sFsNpIpv4McDelCpuPort;
#define  IpmcRemoteNpFsNpIpv4McGetHitStatus  unOpCode.sFsNpIpv4McGetHitStatus;
#define  IpmcRemoteNpFsNpIpv4McUpdateOifVlanEntry  unOpCode.sFsNpIpv4McUpdateOifVlanEntry;
#define  IpmcRemoteNpFsNpIpv4McUpdateIifVlanEntry  unOpCode.sFsNpIpv4McUpdateIifVlanEntry;
#define  IpmcRemoteNpFsNpIpv4GetMRouteStats  unOpCode.sFsNpIpv4GetMRouteStats;
#define  IpmcRemoteNpFsNpIpv4GetMRouteHCStats  unOpCode.sFsNpIpv4GetMRouteHCStats;
#define  IpmcRemoteNpFsNpIpv4GetMNextHopStats  unOpCode.sFsNpIpv4GetMNextHopStats;
#define  IpmcRemoteNpFsNpIpv4GetMIfaceStats  unOpCode.sFsNpIpv4GetMIfaceStats;
#define  IpmcRemoteNpFsNpIpv4GetMIfaceHCStats  unOpCode.sFsNpIpv4GetMIfaceHCStats;
#define  IpmcRemoteNpFsNpIpv4SetMIfaceTtlTreshold  unOpCode.sFsNpIpv4SetMIfaceTtlTreshold;
#define  IpmcRemoteNpFsNpIpv4SetMIfaceRateLimit  unOpCode.sFsNpIpv4SetMIfaceRateLimit;
#define  IpmcRemoteNpFsNpIpvXHwGetMcastEntry  unOpCode.sFsNpIpvXHwGetMcastEntry;
#ifdef PIM_WANTED
#define  IpmcRemoteNpFsNpIpv4McClearHitBit  unOpCode.sFsNpIpv4McClearHitBit;
#endif
#define  IpmcRemoteNpFsNpIpv4McRpfDFInfo  unOpCode.sFsNpIpv4McRpfDFInfo;
#endif
#ifdef MBSM_WANTED
#define  IpmcRemoteNpFsNpIpv4MbsmMcInit  unOpCode.sFsNpIpv4MbsmMcInit;
#define  IpmcRemoteNpFsNpIpv4MbsmMcAddRouteEntry  unOpCode.sFsNpIpv4MbsmMcAddRouteEntry;
#ifdef PIM_WANTED
#define  IpmcRemoteNpFsPimMbsmNpInitHw  unOpCode.sFsPimMbsmNpInitHw;
#endif /* PIM_WANTED */
#ifdef DVMRP_WANTED
#define  IpmcRemoteNpFsDvmrpMbsmNpInitHw  unOpCode.sFsDvmrpMbsmNpInitHw;
#endif
#endif /* DVMRP_WANTED */
} tIpmcRemoteNpModInfo;


/* Required arguments list for the igmp NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

#ifdef IGMP_WANTED
typedef struct {
    INT4  i4IfIndex;
    INT4  i4RateLimit;
} tIgmpRemoteNpWrFsNpIpv4SetIgmpIfaceJoinRate;

#ifdef MBSM_WANTED
typedef struct {
    tMbsmSlotInfo   SlotInfo;
} tIgmpRemoteNpWrFsIgmpMbsmHwEnableIgmp;
#endif
typedef struct IgmpRemoteNpModInfo {
union {
    tIgmpRemoteNpWrFsNpIpv4SetIgmpIfaceJoinRate  sFsNpIpv4SetIgmpIfaceJoinRate;
#ifdef MBSM_WANTED
    tIgmpRemoteNpWrFsIgmpMbsmHwEnableIgmp  sFsIgmpMbsmHwEnableIgmp;
#endif
    }unOpCode;

#define  IgmpRemoteNpFsNpIpv4SetIgmpIfaceJoinRate  unOpCode.sFsNpIpv4SetIgmpIfaceJoinRate;
#ifdef MBSM_WANTED
#define  IgmpRemoteNpFsIgmpMbsmHwEnableIgmp  unOpCode.sFsIgmpMbsmHwEnableIgmp;
#endif
} tIgmpRemoteNpModInfo;
#endif


/* Required arguments list for the mld NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

#ifdef MLD_WANTED
typedef struct {
    INT4  i4IfIndex;
    INT4  i4RateLimit;
} tMldRemoteNpWrFsNpIpv6SetMldIfaceJoinRate;

#ifdef MBSM_WANTED
typedef struct {
    tMbsmSlotInfo   SlotInfo;
} tMldRemoteNpWrFsMldMbsmHwEnableMld;
#endif

typedef struct MldRemoteNpModInfo {
union {
    tMldRemoteNpWrFsNpIpv6SetMldIfaceJoinRate  sFsNpIpv6SetMldIfaceJoinRate;
#ifdef MBSM_WANTED
    tMldRemoteNpWrFsMldMbsmHwEnableMld  sFsMldMbsmHwEnableMld;
#endif
    }unOpCode;

#define  MldRemoteNpFsNpIpv6SetMldIfaceJoinRate  unOpCode.sFsNpIpv6SetMldIfaceJoinRate;
#ifdef MBSM_WANTED
#define  MldRemoteNpFsMldMbsmHwEnableMld  unOpCode.sFsMldMbsmHwEnableMld;
#endif
} tMldRemoteNpModInfo;

#endif


/* Required arguments list for the ip6mc NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

#ifdef FS_NPAPI
typedef struct {
    tMc6RtEntry         rtEntry;
    tMc6DownStreamIf   DownStreamIf;
    tIp6Addr            grpAddr;
    tIp6Addr            srcIpAddr;
    UINT4               u4VrId;
    UINT4               u4GrpPrefix;
    UINT4               u4SrcIpPrefix;
    UINT2               u2NoOfDownStreamIf;
    UINT1               u1CallerId;
    UINT1              au1Pad[1];
} tIp6mcRemoteNpWrFsNpIpv6McAddRouteEntry;

typedef struct {
    tMc6RtEntry         rtEntry;
    tMc6DownStreamIf   DownStreamIf;
    tIp6Addr            grpAddr;
    tIp6Addr            srcIpAddr;
    UINT4               u4VrId;
    UINT4               u4GrpPrefix;
    UINT4               u4SrcIpPrefix;
    UINT2               u2NoOfDownStreamIf;
    UINT1              au1Pad[2];
} tIp6mcRemoteNpWrFsNpIpv6McDelRouteEntry;

typedef struct {
    UINT4  u4VrId;
} tIp6mcRemoteNpWrFsNpIpv6McClearAllRoutes;

typedef struct {
    tMc6RtEntry       rtEntry;
    tMc6DownStreamIf  downStreamIf;
    tIp6Addr          grpAddr;
    tIp6Addr          srcIpAddr;
    UINT4             u4VrId;
    UINT4             u4GrpPrefix;
    UINT4             u4SrcIpPrefix;
} tIp6mcRemoteNpWrFsNpIpv6McUpdateOifVlanEntry;

typedef struct {
    UINT4             u4VrId;
    tIp6Addr          grpAddr;
    UINT4             u4GrpPrefix;
    tIp6Addr          srcIpAddr;
    UINT4             u4SrcIpPrefix;
    tMc6RtEntry       rtEntry;
    tMc6DownStreamIf  downStreamIf;
} tIp6mcRemoteNpWrFsNpIpv6McUpdateIifVlanEntry;

typedef struct {
    tIp6Addr   srcIpAddr;
    tIp6Addr   grpAddr;
    UINT4      u4Iif;
    UINT4      u4HitStatus;
    UINT2      u2VlanId;
    UINT1      au1Pad[2];
} tIp6mcRemoteNpWrFsNpIpv6McGetHitStatus;

typedef struct {
    tMc6RtEntry         rtEntry;
    tMc6DownStreamIf    DownStreamIf;
    tIp6Addr            grpAddr;
    tIp6Addr            srcIpAddr;
    UINT4               u4GrpPrefix;
    UINT4               u4SrcIpPrefix;
    UINT2               u2NoOfDownStreamIf;
    UINT1               u1GenRtrId;
    UINT1               au1Pad[1];
} tIp6mcRemoteNpWrFsNpIpv6McAddCpuPort;

typedef struct {
    tMc6RtEntry  rtEntry;
    tIp6Addr     grpAddr;
    tIp6Addr     srcIpAddr;
    UINT4        u4GrpPrefix;
    UINT4        u4SrcIpPrefix;
    UINT1        u1GenRtrId;
    UINT1        au1Pad[3];
} tIp6mcRemoteNpWrFsNpIpv6McDelCpuPort;

typedef struct {
    UINT4    u4VrId;
    tIp6Addr u1GrpAddr;
    tIp6Addr u1SrcAddr;
    INT4     i4StatType;
    UINT4   u4RetValue;
} tIp6mcRemoteNpWrFsNpIpv6GetMRouteStats;

typedef struct {
    UINT4                  u4VrId;
    tIp6Addr               u1GrpAddr;
    tIp6Addr               u1SrcAddr;
    INT4                   i4StatType;
    tSNMP_COUNTER64_TYPE   U8RetValue;
} tIp6mcRemoteNpWrFsNpIpv6GetMRouteHCStats;

typedef struct {
    UINT4    u4VrId;
    tIp6Addr    u1GrpAddr;
    tIp6Addr    u1SrcAddr;
    INT4      i4OutIfIndex;
    INT4      i4StatType;
    UINT4    u4RetValue;
} tIp6mcRemoteNpWrFsNpIpv6GetMNextHopStats;

typedef struct {
    UINT4    u4VrId;
    INT4     i4IfIndex;
    INT4     i4StatType;
    UINT4   u4RetValue;
} tIp6mcRemoteNpWrFsNpIpv6GetMIfaceStats;

typedef struct {
    UINT4                   u4VrId;
    INT4                    i4IfIndex;
    INT4                    i4StatType;
    tSNMP_COUNTER64_TYPE   U8RetValue;
} tIp6mcRemoteNpWrFsNpIpv6GetMIfaceHCStats;

typedef struct {
    UINT4  u4VrId;
    INT4   i4IfIndex;
    INT4   i4TtlTreshold;
} tIp6mcRemoteNpWrFsNpIpv6SetMIfaceTtlTreshold;

typedef struct {
    UINT4  u4VrId;
    INT4   i4IfIndex;
    INT4   i4RateLimit;
} tIp6mcRemoteNpWrFsNpIpv6SetMIfaceRateLimit;

#ifdef PIMV6_WANTED
typedef struct {
    tIp6Addr   srcIpAddr;
    tIp6Addr   grpAddr;
    UINT2      u2VlanId;
    UINT1      au1Pad[2];
} tIp6mcRemoteNpWrFsNpIpv6McClearHitBit;

#endif
#ifdef MBSM_WANTED
typedef struct {
    tMbsmSlotInfo   SlotInfo;
} tIp6mcRemoteNpWrFsNpIpv6MbsmMcInit;

typedef struct {
    tMbsmSlotInfo     SlotInfo;
    tMc6RtEntry       rtEntry;
    tMc6DownStreamIf  DownStreamIf;
    tIp6Addr          grpAddr;
    tIp6Addr          srcIpAddr;
    UINT4             u4GrpPrefix;
    UINT4             u4SrcIpPrefix;
    UINT4             u4VrId;
    UINT2             u2NoOfDownStreamIf;
    UINT1             u1CallerId;
    UINT1             au1Pad[1];
} tIp6mcRemoteNpWrFsNpIpv6MbsmMcAddRouteEntry;

#ifdef PIMV6_WANTED
typedef struct {
    tMbsmSlotInfo   SlotInfo;
} tIp6mcRemoteNpWrFsPimv6MbsmNpInitHw;

#endif /* PIM_WANTED */
#endif
#endif 

typedef struct Ip6mcRemoteNpModInfo {
union {
#ifdef FS_NPAPI
    tIp6mcRemoteNpWrFsNpIpv6McAddRouteEntry  sFsNpIpv6McAddRouteEntry;
    tIp6mcRemoteNpWrFsNpIpv6McDelRouteEntry  sFsNpIpv6McDelRouteEntry;
    tIp6mcRemoteNpWrFsNpIpv6McClearAllRoutes  sFsNpIpv6McClearAllRoutes;
    tIp6mcRemoteNpWrFsNpIpv6McUpdateOifVlanEntry  sFsNpIpv6McUpdateOifVlanEntry;
    tIp6mcRemoteNpWrFsNpIpv6McUpdateIifVlanEntry  sFsNpIpv6McUpdateIifVlanEntry;
    tIp6mcRemoteNpWrFsNpIpv6McGetHitStatus  sFsNpIpv6McGetHitStatus;
    tIp6mcRemoteNpWrFsNpIpv6McAddCpuPort  sFsNpIpv6McAddCpuPort;
    tIp6mcRemoteNpWrFsNpIpv6McDelCpuPort  sFsNpIpv6McDelCpuPort;
    tIp6mcRemoteNpWrFsNpIpv6GetMRouteStats  sFsNpIpv6GetMRouteStats;
    tIp6mcRemoteNpWrFsNpIpv6GetMRouteHCStats  sFsNpIpv6GetMRouteHCStats;
    tIp6mcRemoteNpWrFsNpIpv6GetMNextHopStats  sFsNpIpv6GetMNextHopStats;
    tIp6mcRemoteNpWrFsNpIpv6GetMIfaceStats  sFsNpIpv6GetMIfaceStats;
    tIp6mcRemoteNpWrFsNpIpv6GetMIfaceHCStats  sFsNpIpv6GetMIfaceHCStats;
    tIp6mcRemoteNpWrFsNpIpv6SetMIfaceTtlTreshold  sFsNpIpv6SetMIfaceTtlTreshold;
    tIp6mcRemoteNpWrFsNpIpv6SetMIfaceRateLimit  sFsNpIpv6SetMIfaceRateLimit;
#ifdef PIMV6_WANTED
    tIp6mcRemoteNpWrFsNpIpv6McClearHitBit  sFsNpIpv6McClearHitBit;
#endif
#ifdef MBSM_WANTED
    tIp6mcRemoteNpWrFsNpIpv6MbsmMcInit  sFsNpIpv6MbsmMcInit;
    tIp6mcRemoteNpWrFsNpIpv6MbsmMcAddRouteEntry  sFsNpIpv6MbsmMcAddRouteEntry;
#ifdef PIMV6_WANTED
    tIp6mcRemoteNpWrFsPimv6MbsmNpInitHw  sFsPimv6MbsmNpInitHw;
#endif /* PIM_WANTED */
#endif
#endif 
    }unOpCode;

#ifdef FS_NPAPI
#define  Ip6mcRemoteNpFsNpIpv6McAddRouteEntry  unOpCode.sFsNpIpv6McAddRouteEntry;
#define  Ip6mcRemoteNpFsNpIpv6McDelRouteEntry  unOpCode.sFsNpIpv6McDelRouteEntry;
#define  Ip6mcRemoteNpFsNpIpv6McClearAllRoutes  unOpCode.sFsNpIpv6McClearAllRoutes;
#define  Ip6mcRemoteNpFsNpIpv6McUpdateOifVlanEntry  unOpCode.sFsNpIpv6McUpdateOifVlanEntry;
#define  Ip6mcRemoteNpFsNpIpv6McUpdateIifVlanEntry  unOpCode.sFsNpIpv6McUpdateIifVlanEntry;
#define  Ip6mcRemoteNpFsNpIpv6McGetHitStatus  unOpCode.sFsNpIpv6McGetHitStatus;
#define  Ip6mcRemoteNpFsNpIpv6McAddCpuPort  unOpCode.sFsNpIpv6McAddCpuPort;
#define  Ip6mcRemoteNpFsNpIpv6McDelCpuPort  unOpCode.sFsNpIpv6McDelCpuPort;
#define  Ip6mcRemoteNpFsNpIpv6GetMRouteStats  unOpCode.sFsNpIpv6GetMRouteStats;
#define  Ip6mcRemoteNpFsNpIpv6GetMRouteHCStats  unOpCode.sFsNpIpv6GetMRouteHCStats;
#define  Ip6mcRemoteNpFsNpIpv6GetMNextHopStats  unOpCode.sFsNpIpv6GetMNextHopStats;
#define  Ip6mcRemoteNpFsNpIpv6GetMIfaceStats  unOpCode.sFsNpIpv6GetMIfaceStats;
#define  Ip6mcRemoteNpFsNpIpv6GetMIfaceHCStats  unOpCode.sFsNpIpv6GetMIfaceHCStats;
#define  Ip6mcRemoteNpFsNpIpv6SetMIfaceTtlTreshold  unOpCode.sFsNpIpv6SetMIfaceTtlTreshold;
#define  Ip6mcRemoteNpFsNpIpv6SetMIfaceRateLimit  unOpCode.sFsNpIpv6SetMIfaceRateLimit;
#ifdef PIMV6_WANTED
#define  Ip6mcRemoteNpFsNpIpv6McClearHitBit  unOpCode.sFsNpIpv6McClearHitBit;
#endif
#ifdef MBSM_WANTED
#define  Ip6mcRemoteNpFsNpIpv6MbsmMcInit  unOpCode.sFsNpIpv6MbsmMcInit;
#define  Ip6mcRemoteNpFsNpIpv6MbsmMcAddRouteEntry  unOpCode.sFsNpIpv6MbsmMcAddRouteEntry;
#ifdef PIMV6_WANTED
#define  Ip6mcRemoteNpFsPimv6MbsmNpInitHw  unOpCode.sFsPimv6MbsmNpInitHw;
#endif /* PIMV6_WANTED */
#endif 
#endif
} tIp6mcRemoteNpModInfo;

#ifdef FSB_WANTED 
typedef struct {
    tFsbHwFilterEntry   FsbHwFilterEntry;
    tFsbLocRemFilterId   FsbLocRemFilterId;
} tFsbRemoteNpWrFsMiNpFsbHwInit;

typedef struct {
    tFsbHwFilterEntry   FsbHwFilterEntry;
    UINT4               u4FilterId;
} tFsbRemoteNpWrFsFsbHwCreateFilter;

typedef struct {
    tFsbHwFilterEntry   FsbHwFilterEntry;
    UINT4               u4FilterId;
} tFsbRemoteNpWrFsFsbHwDeleteFilter;

typedef struct {
    tFsbHwFilterEntry   FsbHwFilterEntry;
    tFsbLocRemFilterId   FsbLocRemFilterId;
} tFsbRemoteNpWrFsMiNpFsbHwDeInit;

typedef struct {
    tFsbHwSChannelFilterEntry   FsbHwSChannelFilterEntry;
    UINT4                       u4FilterId;
} tFsbRemoteNpWrFsFsbHwCreateSChannelFilter;

typedef struct {
    tFsbHwSChannelFilterEntry   FsbHwSChannelFilterEntry;
    UINT4                        u4FilterId;
} tFsbRemoteNpWrFsFsbHwDeleteSChannelFilter;
typedef struct {
    tFsbHwVlanEntry   FsbHwVlanEntry;
} tFsbRemoteNpWrFsFsbHwSetFCoEParams;
#ifdef MBSM_WANTED
typedef struct {
    tFsbHwFilterEntry    FsbHwFilterEntry;
    tFsbLocRemFilterId   FsbLocRemFilterId;
} tFsbRemoteNpWrFsMiNpFsbMbsmHwInit;

typedef struct {
    tFsbHwFilterEntry   FsbHwFilterEntry;
    UINT4               u4FilterId;
} tFsbRemoteNpWrFsFsbMbsmHwCreateFilter;
#endif  /* MBSM_WANTED */

typedef struct FsbRemoteNpModInfo {
union {
    tFsbRemoteNpWrFsMiNpFsbHwInit  sFsMiNpFsbHwInit;
    tFsbRemoteNpWrFsFsbHwCreateFilter  sFsFsbHwCreateFilter;
    tFsbRemoteNpWrFsFsbHwDeleteFilter  sFsFsbHwDeleteFilter;
    tFsbRemoteNpWrFsMiNpFsbHwDeInit  sFsMiNpFsbHwDeInit;
    tFsbRemoteNpWrFsFsbHwCreateSChannelFilter  sFsFsbHwCreateSChannelFilter;
    tFsbRemoteNpWrFsFsbHwDeleteSChannelFilter  sFsFsbHwDeleteSChannelFilter;
    tFsbRemoteNpWrFsFsbHwSetFCoEParams  sFsFsbHwSetFCoEParams;
#ifdef MBSM_WANTED
    tFsbRemoteNpWrFsMiNpFsbMbsmHwInit  sFsMiNpFsbMbsmHwInit;
    tFsbRemoteNpWrFsFsbMbsmHwCreateFilter  sFsFsbMbsmHwCreateFilter;
#endif  /* MBSM_WANTED */
    }unOpCode;

#define  FsbRemoteNpFsMiNpFsbHwInit  unOpCode.sFsMiNpFsbHwInit;
#define  FsbRemoteNpFsFsbHwCreateFilter  unOpCode.sFsFsbHwCreateFilter;
#define  FsbRemoteNpFsFsbHwDeleteFilter  unOpCode.sFsFsbHwDeleteFilter;
#define  FsbRemoteNpFsMiNpFsbHwDeInit  unOpCode.sFsMiNpFsbHwDeInit;
#define  FsbRemoteNpFsFsbHwCreateSChannelFilter  unOpCode.sFsFsbHwCreateSChannelFilter;
#define  FsbRemoteNpFsFsbHwDeleteSChannelFilter  unOpCode.sFsFsbHwDeleteSChannelFilter;
#define  FsbRemoteNpFsFsbHwSetFCoEParams  unOpCode.sFsFsbHwSetFCoEParams;
#ifdef MBSM_WANTED
#define  FsbRemoteNpFsMiNpFsbMbsmHwInit  unOpCode.sFsMiNpFsbMbsmHwInit;
#define  FsbRemoteNpFsFsbMbsmHwCreateFilter  unOpCode.sFsFsbMbsmHwCreateFilter;
#endif  /* MBSM_WANTED */
} tFsbRemoteNpModInfo;
#endif  /* FSB_WANTED */
#ifdef  VXLAN_WANTED
typedef struct
{
    INT4         i4NveIfIndex;
    UINT4        u4IfIndex;
    UINT4        u4VniNumber;
    UINT4        u4VlanId;
    UINT4        u4VlanIdOutTunnel;
    UINT4        u4VrId;
    INT4         i4VtepAddressType;
    INT4         i4RemoteVtepAddressType;
    UINT4         u4NetworkPort;    
    UINT1        au1VtepAddress[16];
    UINT1        au1RemoteVtepAddress[16];
    tMacAddr     DestVmMac;

    BOOL1        b1NveFlushAllVmMacOnly;
    UINT1        u1MacType;
    UINT1        au1NextHopMac[MAC_ADDR_LEN];
    UINT1        u1IsEgIntfTobeDeleted;
    UINT1       au1Pad[1];
} tVxlanRemoteNpWrNveInfo;
typedef struct
 {
    INT4        i4NveIfIndex;
    UINT4        u4VniNumber;
    UINT4        u4VlanId;
    INT4         i4VtepAddressType;
    INT4         i4GroupAddressType;
    UINT1        au1VtepAddress[16];
    UINT1        au1GroupAddress[16];
    tMacAddr     McastMac;
    UINT2        u2Rsvd;
    UINT4        u4VlanIdOutTunnel;
    UINT4        u4VrId;
} tVxlanRemoteNpWrMcastInfo;
typedef struct
 {
    UINT4        u4VniNumber;
    UINT4        u4VlanId;
    UINT4        u4IfIndex;
    UINT4        u4PktSent;
    UINT4        u4PktRcvd;
    UINT4        u4PktDrpd;
    UINT4        u4VrId;
    UINT4        u4AccessPort;
    UINT1        u1IsEgIntfTobeDeleted;
    UINT1        u1IsVlanTagged;
    UINT1       au1Pad[2];
} tVxlanRemoteNpWrVniVlanInfo;

typedef struct
{
    INT4        i4NveIfIndex;
    UINT4        u4IfIndex;
    UINT4        u4VniNumber;
    UINT4        u4VlanId;
    UINT4        u4VlanIdOutTunnel;
    UINT4        u4VrId;
    INT4         i4VtepAddressType;
    INT4         i4RemoteVtepAddressType;
    UINT1        au1VtepAddress[16];
    UINT4        u4NoRemoteVtepsReplicaTo;
    UINT1        *pau1RemoteVtepsReplicaTo;
    UINT1        au1RemoteVtepAddress[16];
    UINT4        u4NetworkBUMPort;
    UINT4        u4NetworkPort;
    UINT1        au1NextHopMac[MAC_ADDR_LEN];
    UINT1        u1ArpSupFlag;
    UINT1        u1IsEgIntfTobeDeleted;
} tVxlanRemoteNpWrBUMReplicaInfo;

typedef struct
 {
    UINT4        u4VniNumber;
    UINT4        u4VlanId;
}tVxlanRemoteNpWrVlanPortInfo;
typedef struct VxlanRemoteNpModInfo {
    union
    {
        tVxlanRemoteNpWrNveInfo               VxlanHwNveInfo; /* NVE database */
        tVxlanRemoteNpWrMcastInfo             VxlanHwMcastInfo; /* Multicast database */
        tVxlanRemoteNpWrVniVlanInfo           VxlanHwVniVlanInfo; /* VNI - VLAN mapping */
        UINT4                                 u4VxlanUdpPortNo; /* UDP port number */
        tVxlanRemoteNpWrBUMReplicaInfo        VxlanHwBUMReplicaInfo;/* VNI - VLAN mapping */
    } unOpCode;
    UINT4     u4InfoType; /* Information type to be set or queried from H/w */
#define VxlanNpRemHwNveInfo        unOpCode.VxlanHwNveInfo;
#define VxlanNpRemHwVniVlanInfo    unOpCode.VxlanHwVniVlanInfo;
#define VxlanNpRemHwBUMReplicaInfo unOpCode.VxlanHwBUMReplicaInfo;
#define u4VxlanRemNpHwUdpPortNo    unOpCode.u4VxlanUdpPortNo
#define VxlanNpRemHwMcastInfo      unOpCode.VxlanHwMcastInfo;
}tVxlanRemoteNpModInfo;
#endif


/* Generic NP Param Structure */
typedef struct __REMOTE_HARDWARE_NP__ {
 tNpModule NpModuleId;          /* Module ID */
 UINT4     u4Opcode;            /* Opcode for specifying the NP call */
 INT4     i4Length;            /* Length of Module Info */

 /* The Ports and Port Lists should always be defined as the
  * first set of parameters in all the NPAPI structure */

 union {
#ifdef VLAN_WANTED
  tVlanRemoteNpModInfo   VlanRemoteNpModInfo;  /* vlan specific h/w parameters */
#endif /* VLAN_WANTED */
#ifdef CFA_WANTED
  tCfaRemoteNpModInfo    CfaRemoteNpModInfo;  /* cfa specific h/w parameters */
#endif /* CFA_WANTED */
#ifdef RSTP_WANTED
  tRstpRemoteNpModInfo   RstpRemoteNpModInfo;  /* rstp specific h/w parameters */
#endif /* RSTP_WANTED */
#ifdef MSTP_WANTED
  tMstpRemoteNpModInfo   MstpRemoteNpModInfo;  /* mstp specific h/w parameters */
#endif /* MSTP_WANTED */
#ifdef PVRST_WANTED
  tPvrstRemoteNpModInfo  PvrstRemoteNpModInfo;  /* pvrst specific h/w parameters */
#endif /* PVRST_WANTED */
#ifdef VCM_WANTED
  tVcmRemoteNpModInfo    VcmRemoteNpModInfo;  /* vcm specific h/w parameters */
#endif /* VCM_WANTED */
#ifdef PNAC_WANTED
  tPnacRemoteNpModInfo    PnacRemoteNpModInfo;  /* pnac specific h/w parameters */
#endif /* PNAC_WANTED */
#ifdef IGS_WANTED 
  tIgsRemoteNpModInfo    IgsRemoteNpModInfo;  /* igs specific h/w parameters */
#endif /* IGS_WANTED */
#ifdef MLDS_WANTED
  tMldsRemoteNpModInfo    MldsRemoteNpModInfo;  /* mlds specific h/w parameters */
#endif /* MLDS_WANTED */
#ifdef ISS_WANTED 
  tIsssysRemoteNpModInfo    IsssysRemoteNpModInfo;  /* isssys specific h/w parameters */
#endif /* ISS_WANTED */
#ifdef EOAM_WANTED
  tEoamRemoteNpModInfo    EoamRemoteNpModInfo;  /* eoam specific h/w parameters */
#endif /* EOAM_WANTED */
#ifdef EOAM_FM_WANTED
  tEoamfmRemoteNpModInfo    EoamfmRemoteNpModInfo;  /* eoamfm specific h/w parameters */
#endif /* EOAM_FM_WANTED */
#ifdef ERPS_WANTED
  tErpsRemoteNpModInfo    ErpsRemoteNpModInfo;  /* erps specific h/w parameters */
#endif /* ERPS_WANTED */
#ifdef LA_WANTED
  tLaRemoteNpModInfo    LaRemoteNpModInfo;  /* eoamfm specific h/w parameters */
#endif /* LA_WANTED */
#ifdef RM_WANTED
  tRmRemoteNpModInfo    RmRemoteNpModInfo;  /* rm specific h/w parameters */
#endif /* RM_WANTED */
#ifdef MBSM_WANTED
  tMbsmRemoteNpModInfo    MbsmRemoteNpModInfo;  /* mbsm specific h/w parameters */
#endif /* MBSM_WANTED */
#ifdef QOSX_WANTED
  tQosxRemoteNpModInfo    QosxRemoteNpModInfo;  /* qosx specific h/w parameters */
#endif /* QOSX_WANTED */
#ifdef ELPS_WANTED
  tElpsRemoteNpModInfo    ElpsRemoteNpModInfo;  /* elps specific h/w parameters */
#endif /* ELPS_WANTED */
#ifdef RMON_WANTED
  tRmonRemoteNpModInfo    RmonRemoteNpModInfo;  /* Rmon specific h/w parameters */
#endif /* RMON_WANTED */
#ifdef RMON2_WANTED
  tRmonv2RemoteNpModInfo    Rmonv2RemoteNpModInfo;  /* Rmonv2 specific h/w parameters */
#endif /* RMON2_WANTED */
#ifdef DSMON_WANTED
  tDsmonRemoteNpModInfo    DsmonRemoteNpModInfo;  /* Rmonv2 specific h/w parameters */
#endif /* DSMON_WANTED */
#ifdef SYNCE_WANTED
  tSynceRemoteNpModInfo  SynceRemoteNpModInfo; /* synce specific h/w parameters */
#endif /* SYNCE_WANTED */
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
  tIpRemoteNpModInfo    IpRemoteNpModInfo;  /* ip specific h/w parameters */
#endif /* (IP_WANTED) || (LNXIP4_WANTED) */
#ifdef ECFM_WANTED
  tEcfmRemoteNpModInfo    EcfmRemoteNpModInfo; /* ecfm specific h/w parameters */
#endif /* ECFM_WANTED */
#if defined (IP6_WANTED) || defined (LNXIP6_WANTED)
  tIpv6RemoteNpModInfo    Ipv6RemoteNpModInfo; /* ecfm specific h/w parameters */
#endif /* IP6_WANTED || LNXIP6_WANTED */
#ifdef MPLS_WANTED
  tMplsRemoteNpModInfo MplsRemoteNpModInfo; /* mpls specific h/w parameters */
#endif
#ifdef IGMP_WANTED
  tIgmpRemoteNpModInfo IgmpRemoteNpModInfo; /* igmp specific h/w parameters */
#endif
#if defined (IP_WANTED) || defined (LNXIP4_WANTED) || defined (PIM_WANTED) || defined (DVMRP_WANTED) || defined (IGMPPRXY_WANTED)
  tIpmcRemoteNpModInfo    IpmcRemoteNpModInfo; /* ecfm specific h/w parameters */
#endif /* IP_WANTED || LNXIP4_WANTED || PIM_WANTED || DVMRP_WANTED || IGMPPRXY_WANTED */
#ifdef MLD_WANTED
  tMldRemoteNpModInfo MldRemoteNpModInfo; /* MLD specific h/w parameters */
#endif
#if defined (IP6_WANTED) || defined (LNXIP6_WANTED) || defined (PIMV6_WANTED) 
  tIp6mcRemoteNpModInfo    Ip6mcRemoteNpModInfo; /* ecfm specific h/w parameters */
#endif /* IP6_WANTED || LNXIP6_WANTED || PIMV6_WANTED */
#ifdef FSB_WANTED
  tFsbRemoteNpModInfo      FsbRemoteNpModInfo;
#endif /* FSB_WANTED */
#ifdef VXLAN_WANTED                                       
  tVxlanRemoteNpModInfo      VxlanRemoteNpModInfo;        
#endif /* VXLAN_WANTED */
 }unModuleInfo;

#ifdef VLAN_WANTED
#define VlanRemoteNpModInfo  unModuleInfo.VlanRemoteNpModInfo
#endif /* VLAN_WANTED */
#ifdef CFA_WANTED
#define CfaRemoteNpModInfo  unModuleInfo.CfaRemoteNpModInfo
#endif /* CFA_WANTED */
#ifdef RSTP_WANTED 
#define RstpRemoteNpModInfo  unModuleInfo.RstpRemoteNpModInfo
#endif /* RSTP_WANTED */
#ifdef MSTP_WANTED
#define MstpRemoteNpModInfo  unModuleInfo.MstpRemoteNpModInfo
#endif /* MSTP_WANTED */
#ifdef PVRST_WANTED
#define PvrstRemoteNpModInfo  unModuleInfo.PvrstRemoteNpModInfo
#endif /* PVRST_WANTED */
#ifdef VCM_WANTED
#define VcmRemoteNpModInfo  unModuleInfo.VcmRemoteNpModInfo
#endif /* VCM_WANTED */
#ifdef PNAC_WANTED
#define PnacRemoteNpModInfo  unModuleInfo.PnacRemoteNpModInfo
#endif /* PNAC_WANTED */
#ifdef IGS_WANTED
#define IgsRemoteNpModInfo  unModuleInfo.IgsRemoteNpModInfo
#endif /* IGS_WANTED */
#ifdef MLDS_WANTED
#define MldsRemoteNpModInfo  unModuleInfo.MldsRemoteNpModInfo
#endif /* MLDS_WANTED */
#ifdef ISS_WANTED
#define IsssysRemoteNpModInfo  unModuleInfo.IsssysRemoteNpModInfo
#endif /* ISS_WANTED */
#ifdef EOAM_WANTED
#define EoamRemoteNpModInfo  unModuleInfo.EoamRemoteNpModInfo
#endif /* EOAM_WANTED */
#ifdef EOAM_FM_WANTED
#define EoamfmRemoteNpModInfo  unModuleInfo.EoamfmRemoteNpModInfo
#endif /* EOAM_FM_WANTED */
#ifdef ERPS_WANTED
#define ErpsRemoteNpModInfo  unModuleInfo.ErpsRemoteNpModInfo
#endif /* ERPS_WANTED */
#ifdef LA_WANTED
#define LaRemoteNpModInfo  unModuleInfo.LaRemoteNpModInfo
#endif /* LA_WANTED */
#ifdef RM_WANTED
#define RmRemoteNpModInfo  unModuleInfo.RmRemoteNpModInfo
#endif /* RM_WANTED */
#ifdef MBSM_WANTED
#define MbsmRemoteNpModInfo  unModuleInfo.MbsmRemoteNpModInfo
#endif /* MBSM_WANTED */
#ifdef QOSX_WANTED
#define QosxRemoteNpModInfo  unModuleInfo.QosxRemoteNpModInfo
#endif /* QOSX_WANTED */
#ifdef ELPS_WANTED
#define ElpsRemoteNpModInfo  unModuleInfo.ElpsRemoteNpModInfo
#endif /* ELPS_WANTED */
#ifdef RMON_WANTED
#define RmonRemoteNpModInfo  unModuleInfo.RmonRemoteNpModInfo
#endif /* RMON_WANTED */
#ifdef RMON2_WANTED
#define Rmonv2RemoteNpModInfo  unModuleInfo.Rmonv2RemoteNpModInfo
#endif /* RMON2_WANTED */
#ifdef DSMON_WANTED
#define DsmonRemoteNpModInfo  unModuleInfo.DsmonRemoteNpModInfo
#endif /* DsMON_WANTED */
#ifdef SYNCE_WANTED
#define SynceRemoteNpModInfo unModuleInfo.SynceRemoteNpModInfo
#endif /* SYNCE_WANTED */
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
#define IpRemoteNpModInfo  unModuleInfo.IpRemoteNpModInfo
#endif /* (IP_WANTED) || (LNXIP4_WANTED) */
#ifdef ECFM_WANTED
#define EcfmRemoteNpModInfo  unModuleInfo.EcfmRemoteNpModInfo
#endif /* ECFM_WANTED */
#if defined (IP6_WANTED) || defined (LNXIP6_WANTED)
#define Ipv6RemoteNpModInfo  unModuleInfo.Ipv6RemoteNpModInfo
#endif  /* IP6_WANTED || LNXIP6_WANTED */
#ifdef MPLS_WANTED
#define MplsRemoteNpModInfo  unModuleInfo.MplsRemoteNpModInfo
#endif /* MPLS_WANTED*/
#ifdef IGMP_WANTED
#define IgmpRemoteNpModInfo  unModuleInfo.IgmpRemoteNpModInfo
#endif /* IGMP_WANTED*/
#if defined (IP_WANTED) || defined (LNXIP4_WANTED) || defined (PIM_WANTED) || defined (DVMRP_WANTED) || defined (IGMPPRXY_WANTED)
#define IpmcRemoteNpModInfo  unModuleInfo.IpmcRemoteNpModInfo
#endif /* IP_WANTED || LNXIP4_WANTED || PIM_WANTED || DVMRP_WANTED || IGMPPRXY_WANTED */
#ifdef MLD_WANTED
#define MldRemoteNpModInfo  unModuleInfo.MldRemoteNpModInfo
#endif /* MLD_WANTED*/
#if defined (IP6_WANTED) || defined (LNXIP6_WANTED) || defined (PIMV6_WANTED) 
#define Ip6mcRemoteNpModInfo  unModuleInfo.Ip6mcRemoteNpModInfo
#endif /* IP6_WANTED || LNXIP6_WANTED || PIMV6_WANTED */
#ifdef FSB_WANTED
#define FsbRemoteNpModInfo    unModuleInfo.FsbRemoteNpModInfo
#endif /* FSB_WANTED */
#ifdef VXLAN_WANTED
#define VxlanRemoteNpModInfo    unModuleInfo.VxlanRemoteNpModInfo
#endif /* VXLAN_WANTED */
}tRemoteHwNp;
/* PROTOTYPE DECLARATIONS */
PUBLIC UINT1 NpUtilConvertLocalArgsToRemoteArgs PROTO ((tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp));
PUBLIC UINT1 NpUtilConvertRemoteArgsToLocalArgs PROTO ((tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp));
PUBLIC UINT1 NpUtilRemoteSvcHwProgram PROTO ((tRemoteHwNp * pRemoteHwNpInput, tRemoteHwNp * pRemoteHwNpOutput));
#ifdef VLAN_WANTED
PUBLIC UINT1 NpUtilVlanConvertLocalToRemoteNp PROTO ((tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp));
PUBLIC UINT1 NpUtilVlanConvertRemoteToLocalNp PROTO ((tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp));
PUBLIC UINT1 NpUtilVlanRemoteSvcHwProgram PROTO ((tRemoteHwNp * pRemoteHwNpInput, tRemoteHwNp * pRemoteHwNpOutput));
PUBLIC VOID NpUtilVlanGetRemotePort PROTO ((tRemoteHwNp * pRemoteHwNp, UINT4 * pu4Port, UINT1 * u1Flag, tPortList * pPortList));
#endif /* VLAN_WANTED */
#ifdef CFA_WANTED
PUBLIC UINT1 NpUtilCfaConvertLocalToRemoteNp PROTO ((tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp));
PUBLIC UINT1 NpUtilCfaConvertRemoteToLocalNp PROTO ((tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp));
PUBLIC UINT1 NpUtilCfaRemoteSvcHwProgram PROTO ((tRemoteHwNp * pRemoteHwNpInput, tRemoteHwNp * pRemoteHwNpOutput));
PUBLIC VOID NpUtilCfaGetRemotePort PROTO ((tRemoteHwNp * pRemoteHwNp, UINT4 * pu4Port));
#endif /* CFA_WANTED */
#ifdef RSTP_WANTED
PUBLIC UINT1 NpUtilRstpConvertLocalToRemoteNp PROTO ((tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp));
PUBLIC UINT1 NpUtilRstpConvertRemoteToLocalNp PROTO ((tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp));
PUBLIC UINT1 NpUtilRstpRemoteSvcHwProgram PROTO ((tRemoteHwNp * pRemoteHwNpInput, tRemoteHwNp * pRemoteHwNpOutput));
PUBLIC VOID  NpUtilRstpGetRemotePort PROTO ((tRemoteHwNp * pRemoteHwNp, UINT4 *pu4Port));
#endif /* RSTP_WANTED */
#ifdef MSTP_WANTED
PUBLIC UINT1 NpUtilMstpConvertLocalToRemoteNp PROTO ((tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp));
PUBLIC UINT1 NpUtilMstpConvertRemoteToLocalNp PROTO ((tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp));
PUBLIC UINT1 NpUtilMstpRemoteSvcHwProgram PROTO ((tRemoteHwNp * pRemoteHwNpInput, tRemoteHwNp * pRemoteHwNpOutput));
PUBLIC VOID  NpUtilMstpGetRemotePort PROTO ((tRemoteHwNp * pRemoteHwNp, UINT4 *pu4Port));
#endif /* MSTP_WANTED */
#ifdef PVRST_WANTED
PUBLIC UINT1 NpUtilPvrstConvertLocalToRemoteNp PROTO ((tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp));
PUBLIC UINT1 NpUtilPvrstConvertRemoteToLocalNp PROTO ((tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp));
PUBLIC UINT1 NpUtilPvrstRemoteSvcHwProgram PROTO ((tRemoteHwNp * pRemoteHwNpInput, tRemoteHwNp * pRemoteHwNpOutput));
PUBLIC VOID NpUtilPvrstpGetRemotePort PROTO ((tRemoteHwNp * pRemoteHwNp, UINT4 * pu4Port));
#endif /* PVRST_WANTED */
#ifdef VCM_WANTED
PUBLIC UINT1 NpUtilVcmConvertLocalToRemoteNp PROTO ((tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp));
PUBLIC UINT1 NpUtilVcmConvertRemoteToLocalNp PROTO ((tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp));
PUBLIC UINT1 NpUtilVcmRemoteSvcHwProgram PROTO ((tRemoteHwNp * pRemoteHwNpInput, tRemoteHwNp * pRemoteHwNpOutput));
#endif /* VCM_WANTED */
#ifdef PNAC_WANTED
PUBLIC UINT1 NpUtilPnacConvertLocalToRemoteNp PROTO ((tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp));
PUBLIC UINT1 NpUtilPnacConvertRemoteToLocalNp PROTO ((tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp));
PUBLIC UINT1 NpUtilPnacRemoteSvcHwProgram PROTO ((tRemoteHwNp * pRemoteHwNpInput, tRemoteHwNp * pRemoteHwNpOutput));
#endif /* PNAC_WANTED */
#ifdef EOAM_WANTED
PUBLIC UINT1 NpUtilEoamConvertLocalToRemoteNp PROTO ((tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp));
PUBLIC UINT1 NpUtilEoamConvertRemoteToLocalNp PROTO ((tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp));
PUBLIC UINT1 NpUtilEoamRemoteSvcHwProgram PROTO ((tRemoteHwNp * pRemoteHwNpInput, tRemoteHwNp * pRemoteHwNpOutput));
#endif /* EOAM_WANTED */
#ifdef EOAM_FM_WANTED
PUBLIC UINT1 NpUtilEoamfmConvertLocalToRemoteNp PROTO ((tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp));
PUBLIC UINT1 NpUtilEoamfmConvertRemoteToLocalNp PROTO ((tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp));
PUBLIC UINT1 NpUtilEoamfmRemoteSvcHwProgram PROTO ((tRemoteHwNp * pRemoteHwNpInput, tRemoteHwNp * pRemoteHwNpOutput));
#endif /* EOAM_FM_WANTED */
#ifdef IGS_WANTED
PUBLIC UINT1 NpUtilIgsConvertLocalToRemoteNp PROTO ((tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp));
PUBLIC UINT1 NpUtilIgsConvertRemoteToLocalNp PROTO ((tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp));
PUBLIC UINT1 NpUtilIgsRemoteSvcHwProgram PROTO ((tRemoteHwNp * pRemoteHwNpInput, tRemoteHwNp * pRemoteHwNpOutput));
PUBLIC VOID NpUtilIgsGetRemotePort PROTO ((tRemoteHwNp * pRemoteHwNp, UINT4 * pu4Port, UINT1 * u1Flag, tPortList * pPortList));
PUBLIC UINT1 NpUtilIgsMergeLocalRemoteNPOutput (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp, INT4 *pi4LocalNpRetVal, 
                                               INT4 *pi4RemoteNpRetVal);

#endif /* IGS_WANTED */
#ifdef MLDS_WANTED
PUBLIC UINT1 NpUtilMldsConvertLocalToRemoteNp PROTO ((tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp));
PUBLIC UINT1 NpUtilMldsConvertRemoteToLocalNp PROTO ((tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp));
PUBLIC UINT1 NpUtilMldsRemoteSvcHwProgram PROTO ((tRemoteHwNp * pRemoteHwNpInput, tRemoteHwNp * pRemoteHwNpOutput));
#endif /* MLDS_WANTED */
#ifdef ISS_WANTED
PUBLIC UINT1 NpUtilIsssysConvertLocalToRemoteNp PROTO ((tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp));
PUBLIC UINT1 NpUtilIsssysConvertRemoteToLocalNp PROTO ((tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp));
PUBLIC UINT1 NpUtilIsssysRemoteSvcHwProgram PROTO ((tRemoteHwNp * pRemoteHwNpInput, tRemoteHwNp * pRemoteHwNpOutput));
#endif /* ISS_WANTED */
#ifdef LA_WANTED
PUBLIC UINT1 NpUtilLaConvertLocalToRemoteNp PROTO ((tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp));
PUBLIC UINT1 NpUtilLaConvertRemoteToLocalNp PROTO ((tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp));
PUBLIC UINT1 NpUtilLaRemoteSvcHwProgram PROTO ((tRemoteHwNp * pRemoteHwNpInput, tRemoteHwNp * pRemoteHwNpOutput));
#endif /* LA_WANTED */
#ifdef ERPS_WANTED
PUBLIC UINT1 NpUtilErpsConvertLocalToRemoteNp PROTO ((tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp));
PUBLIC UINT1 NpUtilErpsConvertRemoteToLocalNp PROTO ((tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp));
PUBLIC UINT1 NpUtilErpsRemoteSvcHwProgram PROTO ((tRemoteHwNp * pRemoteHwNpInput, tRemoteHwNp * pRemoteHwNpOutput));
#endif /* ERPS_WANTED */
#ifdef RM_WANTED
PUBLIC UINT1 NpUtilRmConvertLocalToRemoteNp PROTO ((tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp));
PUBLIC UINT1 NpUtilRmConvertRemoteToLocalNp PROTO ((tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp));
PUBLIC UINT1 NpUtilRmRemoteSvcHwProgram PROTO ((tRemoteHwNp * pRemoteHwNpInput, tRemoteHwNp * pRemoteHwNpOutput));
#endif /* RM_WANTED */
#ifdef MBSM_WANTED
PUBLIC UINT1 NpUtilMbsmConvertLocalToRemoteNp PROTO ((tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp));
PUBLIC UINT1 NpUtilMbsmConvertRemoteToLocalNp PROTO ((tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp));
PUBLIC UINT1 NpUtilMbsmRemoteSvcHwProgram PROTO ((tRemoteHwNp * pRemoteHwNpInput, tRemoteHwNp * pRemoteHwNpOutput));
#endif /* MBSM_WANTED */
#ifdef QOSX_WANTED
PUBLIC UINT1 NpUtilQosxConvertLocalToRemoteNp PROTO ((tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp));
PUBLIC UINT1 NpUtilQosxConvertRemoteToLocalNp PROTO ((tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp));
PUBLIC UINT1 NpUtilQosxRemoteSvcHwProgram PROTO ((tRemoteHwNp * pRemoteHwNpInput, tRemoteHwNp * pRemoteHwNpOutput));
#endif /* QOSX_WANTED */
#ifdef ELPS_WANTED
PUBLIC UINT1 NpUtilElpsConvertLocalToRemoteNp PROTO ((tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp));
PUBLIC UINT1 NpUtilElpsConvertRemoteToLocalNp PROTO ((tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp));
PUBLIC UINT1 NpUtilElpsRemoteSvcHwProgram PROTO ((tRemoteHwNp * pRemoteHwNpInput, tRemoteHwNp * pRemoteHwNpOutput));
#endif /* ELPS_WANTED */
#ifdef RMON_WANTED
PUBLIC UINT1 NpUtilRmonConvertLocalToRemoteNp PROTO ((tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp));
PUBLIC UINT1 NpUtilRmonConvertRemoteToLocalNp PROTO ((tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp));
PUBLIC UINT1 NpUtilRmonRemoteSvcHwProgram PROTO ((tRemoteHwNp * pRemoteHwNpInput, tRemoteHwNp * pRemoteHwNpOutput));
#endif /* RMON_WANTED */
#ifdef RMON2_WANTED
PUBLIC UINT1 NpUtilRmonv2ConvertLocalToRemoteNp PROTO ((tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp));
PUBLIC UINT1 NpUtilRmonv2ConvertRemoteToLocalNp PROTO ((tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp));
PUBLIC UINT1 NpUtilRmonv2RemoteSvcHwProgram PROTO ((tRemoteHwNp * pRemoteHwNpInput, tRemoteHwNp * pRemoteHwNpOutput));
#endif /* RMON2_WANTED */
#ifdef DSMON_WANTED
PUBLIC UINT1 NpUtilDsmonConvertLocalToRemoteNp PROTO ((tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp));
PUBLIC UINT1 NpUtilDsmonConvertRemoteToLocalNp PROTO ((tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp));
PUBLIC UINT1 NpUtilDsmonRemoteSvcHwProgram PROTO ((tRemoteHwNp * pRemoteHwNpInput, tRemoteHwNp * pRemoteHwNpOutput));
#endif /* DSMON_WANTED */
#ifdef SYNCE_WANTED
PUBLIC UINT1 NpUtilSynceConvertLocalToRemoteNp PROTO ((tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp));
PUBLIC UINT1 NpUtilSynceConvertRemoteToLocalNp PROTO ((tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp));
PUBLIC UINT1 NpUtilSynceRemoteSvcHwProgram PROTO ((tRemoteHwNp * pRemoteHwNpInput, tRemoteHwNp * pRemoteHwNpOutput));
#endif
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
PUBLIC UINT1 NpUtilIpConvertLocalToRemoteNp PROTO ((tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp));
PUBLIC UINT1 NpUtilIpConvertRemoteToLocalNp PROTO ((tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp));
PUBLIC UINT1 NpUtilIpRemoteSvcHwProgram PROTO ((tRemoteHwNp * pRemoteHwNpInput, tRemoteHwNp * pRemoteHwNpOutput));
PUBLIC VOID NpUtilIpGetRemotePort PROTO ((tRemoteHwNp * pRemoteHwNp, UINT4 * pu4Port));
#endif /* (IP_WANTED) || (LNXIP4_WANTED) */
#ifdef ECFM_WANTED
PUBLIC UINT1 NpUtilEcfmConvertLocalToRemoteNp PROTO ((tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp));
PUBLIC UINT1 NpUtilEcfmConvertRemoteToLocalNp PROTO ((tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp));
PUBLIC UINT1 NpUtilEcfmRemoteSvcHwProgram PROTO ((tRemoteHwNp * pRemoteHwNpInput, tRemoteHwNp * pRemoteHwNpOutput));
#endif /* ECFM_WANTED */
#if defined (IP6_WANTED) || defined (LNXIP6_WANTED)
PUBLIC UINT1 NpUtilIpv6ConvertLocalToRemoteNp PROTO ((tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp));
PUBLIC UINT1 NpUtilIpv6ConvertRemoteToLocalNp PROTO ((tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp));
PUBLIC UINT1 NpUtilIpv6RemoteSvcHwProgram PROTO ((tRemoteHwNp * pRemoteHwNpInput, tRemoteHwNp * pRemoteHwNpOutput));
#endif /* IP6_WANTED || LNXIP6_WANTED */
#ifdef MPLS_WANTED
PUBLIC UINT1 NpUtilMplsConvertLocalToRemoteNp PROTO ((tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp));
PUBLIC UINT1 NpUtilMplsConvertRemoteToLocalNp PROTO ((tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp));
PUBLIC UINT1 NpUtilMplsRemoteSvcHwProgram PROTO ((tRemoteHwNp * pRemoteHwNpInput, tRemoteHwNp * pRemoteHwNpOutput));
#endif /*MPLS_WANTED*/
#ifdef IGMP_WANTED
PUBLIC UINT1 NpUtilIgmpConvertLocalToRemoteNp PROTO ((tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp));
PUBLIC UINT1 NpUtilIgmpConvertRemoteToLocalNp PROTO ((tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp));
PUBLIC UINT1 NpUtilIgmpRemoteSvcHwProgram PROTO ((tRemoteHwNp * pRemoteHwNpInput, tRemoteHwNp * pRemoteHwNpOutput));
#endif /*IGMP_WANTED*/
#if defined (IP_WANTED) || defined (LNXIP4_WANTED) || defined (PIM_WANTED) || defined (DVMRP_WANTED) || defined (IGMPPRXY_WANTED)
PUBLIC UINT1 NpUtilIpmcConvertLocalToRemoteNp PROTO ((tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp));
PUBLIC UINT1 NpUtilIpmcConvertRemoteToLocalNp PROTO ((tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp));
PUBLIC UINT1 NpUtilIpmcRemoteSvcHwProgram PROTO ((tRemoteHwNp * pRemoteHwNpInput, tRemoteHwNp * pRemoteHwNpOutput));
#endif /* IP_WANTED || LNXIP4_WANTED || PIM_WANTED || DVMRP_WANTED || IGMPPRXY_WANTED */
#ifdef MLD_WANTED
PUBLIC UINT1 NpUtilMldConvertLocalToRemoteNp PROTO ((tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp));
PUBLIC UINT1 NpUtilMldConvertRemoteToLocalNp PROTO ((tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp));
PUBLIC UINT1 NpUtilMldRemoteSvcHwProgram PROTO ((tRemoteHwNp * pRemoteHwNpInput, tRemoteHwNp * pRemoteHwNpOutput));
#endif /*MLD_WANTED*/
#if defined (IP6_WANTED) || defined (LNXIP6_WANTED) || defined (PIMV6_WANTED) 
PUBLIC UINT1 NpUtilIp6mcConvertLocalToRemoteNp PROTO ((tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp));
PUBLIC UINT1 NpUtilIp6mcConvertRemoteToLocalNp PROTO ((tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp));
PUBLIC UINT1 NpUtilIp6mcRemoteSvcHwProgram PROTO ((tRemoteHwNp * pRemoteHwNpInput, tRemoteHwNp * pRemoteHwNpOutput));
#endif /* IP6_WANTED || LNXIP6_WANTED || PIMV6_WANTED */
#ifdef  FSB_WANTED
PUBLIC UINT1 NpUtilFsbConvertLocalToRemoteNp PROTO ((tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp));
PUBLIC UINT1 NpUtilFsbConvertRemoteToLocalNp PROTO ((tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp));
PUBLIC UINT1 NpUtilFsbRemoteSvcHwProgram PROTO ((tRemoteHwNp * pRemoteHwNpInput, tRemoteHwNp * pRemoteHwNpOutput));
#endif  /* FSB_WANTED */
#ifdef  VXLAN_WANTED
PUBLIC UINT1 NpUtilVxlanConvertLocalToRemoteNp PROTO ((tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp));
PUBLIC UINT1 NpUtilVxlanConvertRemoteToLocalNp PROTO ((tRemoteHwNp * pRemoteHwNp, tFsHwNp * pFsHwNp));
PUBLIC UINT1 NpUtilVxlanRemoteSvcHwProgram PROTO ((tRemoteHwNp * pRemoteHwNpInput, tRemoteHwNp * pRemoteHwNpOutput));
PUBLIC VOID NpUtilMaskVxlanNpPorts (tFsHwNp * pFsHwNp, tRemoteHwNp * pRemoteHwNp,
                                    UINT1 *pu1NpCallStatus, INT4 *pi4RpcCallStatus);
PUBLIC UINT1
NpUtilVxlanMergeLocalRemoteNPOutput (tFsHwNp * pFsHwNp,tRemoteHwNp * pRemoteHwNp,
                                     INT4 *pi4LocalNpRetVal,INT4 *pi4RemoteNpRetVal);
#endif  /* VXLAN_WANTED */

#endif /* _NPUTLREMNP_H_ */
