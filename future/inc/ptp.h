/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ptp.h,v 1.9 2014/01/03 12:38:47 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              by external modules.
 ********************************************************************/
#ifndef _PTP_H_
#define _PTP_H_


#include "iss.h"
#include "fsclk.h"
#include "fsvlan.h"
#include "ipv6.h"

#define FS_PROTO_PTP                            42

#define PTP_NANO_SEC_TO_MIILI_SEC_CONV_FACTOR   1000

/* PTP Message type constants */
#define PTP_PORT_STATUS_MSG       1
#define PTP_PORT_DEL_MSG          2
#define PTP_PDU_ENQ_MSG           3
#define PTP_CTXT_DELETE_MSG       4
#define PTP_PRIMARY_PARAMS_CHG    5
#define PTP_FOLLOW_UP_TRIGGER     6

#define PTP_PORT_ID_LEN           10

/* Macros for module status */
#define PTP_ENABLED                      1
#define PTP_DISABLED                     2

typedef enum {
    PTP_SOFTWARE_TIMESTAMPING = ISS_SOFTWARE_TIME_STAMP,
    PTP_HARDWARE_TIMESTAMPING = ISS_HARDWARE_TIME_STAMP,
    PTP_HARDWARE_TRANS_TIMESTAMPING = ISS_HARDWARE_TRANS_TIME_STAMP
}eTimeStamp;

typedef enum {
    PTP_IFACE_UDP_IPV4 = 1,
    PTP_IFACE_UDP_IPV6 = 2,
    PTP_IFACE_IEEE_802_3 = 3,
    PTP_IFACE_VLAN       = 7,
    PTP_IFACE_UNKNOWN    = 65534
} ePtpDeviceType;

typedef enum {
    PTP_BOUNDARY_CLOCK_MODE              = 1,
    PTP_OC_CLOCK_MODE                    = 2,
    PTP_TRANSPARENT_CLOCK_MODE           = 3,
    PTP_FORWARD_MODE                     = 4,
    PTP_MANAGEMENT_MODE                  = 5
} eClkMode;

typedef struct _PtpL2IfStatusChgInfo {
    UINT4        u4ContextId;
    UINT4        u4IfIndex;
    UINT1        u1Status;
    UINT1        u1IfType;
    UINT1        au1Pad[2];
}tPtpL2IfStatusChgInfo;
        
typedef struct _PtpHwSrcIdHash {
    INT4              i4Length;
    UINT1             au1SrcPortId[PTP_PORT_ID_LEN];
    BOOL1             bSrcIdHash;
    UINT1             au1Pad[1];
}tPtpHwSrcIdHash;

/* Data structure to pkt Tx/Rx time stamp */
typedef struct _PtpHwLog {
    tFsClkTimeVal     FsTimeVal; /* Time Stamp */
    UINT4             u4ContextId;/* Context Id */
    UINT4             u4SeqId;   /* Sequence Id */
    UINT4             u4PortNo;  /* Port Number on which packet 
                                    is transmitted or received*/
    UINT2             u2TimeStamp; /* Software or Hardware */
    UINT1             u1DomainId;/* DomainId */
    UINT1             u1MsgType; /* MsgType - Sync/Delay Response */
    UINT1             u1Mode;    /* Mode - Transmission/Reception */
    UINT1             u1Pad[3]; /* Padding for the structure */
}tPtpHwLog;

typedef struct _PtpHwPtpCbParams {
    tFsClkTimeVal     FsClkTimeVal;
    UINT4             u4ContextId;
    UINT4             u4IfIndex;
    VOID             *pu1Pdu; /* Time stamp can be appended on PDU */
    UINT1             u1DomainId;
    UINT1             u1MsgType;
    UINT1             au1Pad[2];
}tPtpHwPtpCbParams;
/* Enum to indicate the information type to be set or queried from H/w */
enum {
    PTP_HW_SET_STATUS = 1,
    PTP_HW_SET_TRANSMIT_TS_OPTION,
    PTP_HW_SET_RECEIVE_TS_OPTION,
    PTP_HW_SET_DOMAIN_MODE,
    PTP_HW_SET_CLOCK,
    PTP_HW_SET_CLOCK_ADJUSTMENT_FACTOR,
    PTP_HW_SET_SRC_ID_HASH,
    PTP_HW_INIT,
    PTP_HW_DEINIT,
    PTP_HW_GET_PKT_LOG_TIMESTAMP,
 PTP_HW_CONFIG_PEER_DELAY,
    PTP_HW_GET_CLOCK
};
enum {
    PTP_MODE_TX = 1,
    PTP_MODE_RX
};

/* Message Types */
#define PTP_SYNC_MSG      0x00
#define PTP_DELAY_REQ_MSG     0x01
#define PTP_PEER_DELAY_REQ_MSG     0x02
#define PTP_PEER_DELAY_RESP_MSG     0x03
#define PTP_FOLLOW_UP_MSG     0x08
#define PTP_DELAY_RESP_MSG     0x09 
#define PTP_PDELAY_RESP_FOLLOWUP_MSG    0x0A
#define PTP_ANNC_MSG      0x0B
#define PTP_SIGNALLING_MSG     0x0C
#define PTP_NPSIM_MSG                      0x0F
/******************************************************************************
 *                             ptpmain.c                                      *
 ******************************************************************************/
PUBLIC VOID PtpMainTask PROTO ((INT1 *pArg));

/******************************************************************************
 *                             ptpapi.c                                       *
 ******************************************************************************/
PUBLIC INT4 
PtpApiLock PROTO ((VOID));

PUBLIC INT4 
PtpApiUnLock PROTO ((VOID));

PUBLIC VOID 
PtpApiIfStatusChgHdlr PROTO ((tPtpL2IfStatusChgInfo  *pPtpL2IfStatusChgInfo));

PUBLIC VOID 
PtpApiIncomingPktHdlr PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4IfIndex,
                              UINT4 u4ContextId, UINT4 u4PktLen,
                              tVlanId VlanId));
PUBLIC INT4
PtpApiContextStatusChgHdlr PROTO ((UINT4 u4ContextId, UINT4 u4Operation));

PUBLIC INT4
PtpApiClkIwfCbkFn PROTO ((tClkIwfPrimParams *pPtpPrimaryParams));

PUBLIC VOID PtpApiPktRcvdOnSocket PROTO ((INT4 i4SockId));

PUBLIC INT4 
PtpApiIfDeleteCallBack PROTO ((tCfaRegInfo * pCfaRegInfo));

PUBLIC INT4 
PtpApiIfOperChgCallBack PROTO ((tCfaRegInfo * pCfaRegInfo));

PUBLIC INT4 
PtpApiPktRxCallBack PROTO ((tCfaRegInfo * pCfaRegInfo));

PUBLIC VOID
PtpApiIpV6IfStatusChgNotify PROTO ((tNetIpv6HliParams * pNetIpv6HlParams));
PUBLIC INT4 
PtpApiTriggerPtpFollowUp PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, 
                                 tFsClkTimeVal * pFsClkTimeVal, 
                                 UINT1 u1DomainId, UINT1 u1MsgType));
PUBLIC INT4 
PtpApiIsTwoStepClock PROTO ((UINT4 u4CxtId, UINT4 u4Port, UINT1 u1Domain));
PUBLIC INT4
PtpApiUpdateDelayOriginTime PROTO ((UINT4 u4CxtId, UINT4 u4Port,
                             tFsClkTimeVal FsClkTimeVal, UINT1 u1Domain, 
                             UINT1 u1MsgType));
PUBLIC INT4
PtpApiGetStatus PROTO ((INT4 *pi4Status));

#endif /* _PTP_H_ */
