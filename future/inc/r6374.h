
/*******************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: r6374.h,v 1.4 2017/07/25 12:00:36 siva Exp $
 *
 * Description: This file contains type definitions for R6374 module.
 *********************************************************************/

#ifndef __R6374_H__
#define __R6374_H__

#include "mplsapi.h"

/* for queue */
enum {
    RFC6374_CREATE_CONTEXT_MSG = 1,
    RFC6374_DELETE_CONTEXT_MSG,
    RFC6374_RX_PDU_MSG,
    RFC6374_LM_START_SESSION,
    RFC6374_LM_STOP_SESSION,
    RFC6374_DM_START_SESSION,
    RFC6374_DM_STOP_SESSION,
    RFC6374_LM_INTERVAL_EXPIRY,
    RFC6374_DM_INTERVAL_EXPIRY,
    RFC6374_MPLS_PATH_STATUS_CHG,
    RFC6374_LMDM_START_SESSION,
    RFC6374_LMDM_STOP_SESSION,
    RFC6374_LMDM_INTERVAL_EXPIRY,
    RFC6374_GET_SESSION_STATS
};

/* Error codes returned to external modules.*/
enum {
    RFC6374_MSG_ENQUEUE_FAILED = 1,
    RFC6374_QMSG_MEM_ALLOC_FAILED,
    RFC6374_MSG_TYPE_INVALID
};


/* Get Session Type */
enum {
    RFC6374_SESSION_TYPE_LOSS = 1,
    RFC6374_SESSION_TYPE_DELAY
};

/* TBD
enum
{
   RFC6374_START = 1,
   RFC6374_SHUTDOWN
};*/

#define RFC6374_LOCK()                    R6374TaskLock ()
#define RFC6374_UNLOCK()                  R6374TaskUnLock ()

#define RFC6374_SERVICE_NAME_MAX_LEN      40 + 1 /*Extra 1 for NULL char*/

#define RFC6374_MAX_PDU_SIZE                9216 
#define RFC6374_PDU_SIZE                    9216 
         /* Padding TLV (9002 bytes) +
          * SQI TLV (6 bytes) +
          * DM PDU Len (44 bytes) /
          * LM PDU Len (52 bytes ) /
          * Combined LMDM Len (74 bytes) 
          * Maximum 9076 bytes. so size 
          * is selected to nearer 
          * supported size*/       

#define RFC6374_ENCAP_TYPE_ACH                 1
#define RFC6374_ENCAP_TYPE_GAL                 2
#define RFC6374_ENCAP_TYPE_MAX                 3

#define RFC6374_PATH_TYPE_TUNNEL               1
#define RFC6374_PATH_TYPE_PW                   2

#define RFC6374_CHANNEL_DIRECT_LM      0x000A
#define RFC6374_CHANNEL_INFERED_LM     0x000B
#define RFC6374_CHANNEL_DM             0x000C

#define   RFC6374_INIT_SHUT_TRC      0x00000001
#define   RFC6374_MGMT_TRC           0x00000002
#define   RFC6374_CRITICAL_TRC       0x00000004
#define   RFC6374_CONTROL_PLANE_TRC  0x00000008
#define   RFC6374_PKT_DUMP_TRC       0x00000010
#define   RFC6374_OS_RESOURCE_TRC    0x00000020
#define   RFC6374_ALL_FAILURE_TRC    0x00000040
#define   RFC6374_FN_ENTRY_TRC       0x00000080
#define   RFC6374_FN_EXIT_TRC        0x00000100

#define RFC6374_UNSUP_VERSION_TRAP                 1
#define RFC6374_UNSUP_CC_TRAP                      2
#define RFC6374_CONN_MISMATCH_TRAP                 3
#define RFC6374_DATA_FORMAT_INVALID_TRAP           4
#define RFC6374_UNSUP_MAND_TLV_TRAP                5
#define RFC6374_UNSUP_TIMESTAMP_TRAP               6
#define RFC6374_RESP_TIMEOUT_TRAP                  7
#define RFC6374_RES_UNAVAIL_TRAP                   8 
#define RFC6374_TEST_ABORT_TRAP                    9
#define RFC6374_PROACTIVE_TEST_RESTART_TRAP        10
#define RFC6374_UNSUPP_QUERY_INTERVAL              11

#define RFC6374_INVALID_PAD_SIZE            0xffffffff
typedef tCRU_BUF_CHAIN_HEADER     tR6374BufChainHeader;

/****************************************************************************/
/* Time Representation*/
typedef struct R6374TSRepresentation
{
    UINT4        u4Seconds;  /* Time units in seconds*/
    UINT4        u4NanoSeconds;
    /* Time units in nano seconds*/
}tR6374TSRepresentation;

typedef struct
{
    tMplsNodeId    SrcNodeId;
    tMplsNodeId    DstNodeId;
    UINT4          u4SrcTnlId;
    UINT4          u4DstTnlId;
    UINT4          u4LspId;
    UINT4          u4InIf;           /* Incoming interface */
}tR6374TnlInfo;

typedef struct
{
    tIpAddr        PeerAddr;
    UINT4          u4PwVcId;
}tR6374PwInfo;

typedef struct
{
    union
    {
        tR6374TnlInfo      TnlInfo;
        tR6374PwInfo       PwInfo;
    }unPathId;
    UINT1           u1PathType;
    UINT1 au1Pad[3];
}tR6374PathId;

typedef struct R6374RxPduInfo
{
    tCRU_BUF_CHAIN_DESC *pBuf;    /* Pointer to the LSP Ping PDU buffer */
    UINT4               u4IfIndex; /*Interface on which the packet is received*/
    /* Need to add pad if needed */
}tR6374RxPduInfo;

typedef struct R6374ApiLMStats
{
    FLT4                     f4ThroughPut;
    UINT4                    u4ContextId;
    UINT4                    u4SessionId;   /* Initiated Session ID */
    UINT4                    u4ChannelId1;  /* Forward Tunnel/PW ID */
    UINT4                    u4ChannelId2;  /* Reverse Tunnel */
    UINT4                    u4ChannelIpAddr1;  /* Source IP/PW IP*/
    UINT4                    u4ChannelIpAddr2;  /* Destination IP*/
    UINT4                    u4MinCalcTxLossSender; 
    UINT4                    u4MaxCalcTxLossSender;
    UINT4                    u4AvgCalcTxLossSender;
    UINT4                    u4MinCalcRxLossSender;
    UINT4                    u4MaxCalcRxLossSender;
    UINT4                    u4AvgCalcRxLossSender;
    UINT4                    u4MinCalcRxLossReceiver;
    UINT4                    u4MaxCalcRxLossReceiver;
    UINT4                    u4AvgCalcRxLossReceiver;
    UINT4                    u4LMMSent;
    UINT4                    u4LMMRcvd;
    UINT4                    u4LMRRcvd;
    UINT4                    u41LMRcvd;
    UINT1                    au1ServiceName[RFC6374_SERVICE_NAME_MAX_LEN];
    UINT1                    u1MplsPathType;    /* Tunnel/PW */
    UINT1                    u1TrafficClass;    
    UINT1                    u1LossType;    /* One-way/two-way*/
    UINT1                    u1LossMode;    /* On-demand/Proactive */
    UINT1                    u1LossTSFormat;    /* Time Stamp format used for Measument */
    UINT1                    u1DyadicMeasurement;
    UINT1                    au1Pad [1];
}tR6374ApiLMStats;

typedef struct R6374ApiDMStats
{
    tR6374TSRepresentation   MinPktDelayValue;
    tR6374TSRepresentation   MaxPktDelayValue;
    tR6374TSRepresentation   AvgPktDelayValue;
    tR6374TSRepresentation   MinPktRTDelayValue;
    tR6374TSRepresentation   MaxPktRTDelayValue;
    tR6374TSRepresentation   AvgPktRTDelayValue;
    tR6374TSRepresentation   MinInterPktDelayVariation;
    tR6374TSRepresentation   MaxInterPktDelayVariation;
    tR6374TSRepresentation   AvgInterPktDelayVariation;
    tR6374TSRepresentation   MinPktDelayVariation;
    tR6374TSRepresentation   MaxPktDelayVariation;
    tR6374TSRepresentation   AvgPktDelayVariation;
    UINT4                    u4SessionId;   /* Initiated Session ID */
    UINT4                    u4ContextId;
    UINT4                    u4ChannelId1;/* Forward Tunnel/PW ID */
    UINT4                    u4ChannelId2;/* Reverse Tunnel */
    UINT4                    u4ChannelIpAddr1; /* Source IP/PW IP*/
    UINT4                    u4ChannelIpAddr2; /* Destination IP*/
    UINT4                    u4DMMSent;
    UINT4                    u4DMMRcvd;
    UINT4                    u4DMRRcvd;
    UINT4                    u41DMRcvd;
    UINT1                    au1ServiceName[RFC6374_SERVICE_NAME_MAX_LEN];
    UINT1                    u1MplsPathType; /* Tunnel/PW */
    UINT1                    u1TrafficClass; 
    UINT1                    u1DelayType; /* One-way/two-way*/
    UINT1                    u1DelayMode;  /* On-demand/Proactive */
    UINT1                    u1DelayTSFormat; /* Time Stamp format used for Measument */
    UINT1                    u1DyadicMeasurement;
    UINT1                    au1Pad [1];
}tR6374ApiDMStats;

typedef struct R6374ServiceStats
{
    UINT1               au1ServiceName[RFC6374_SERVICE_NAME_MAX_LEN];   /* Service from where 
                                                                           stats to be get */
    UINT1               u1SessionStatsType;     /* Can be LM or DM */
    UINT1               u1Pad[2];
}tR6374ServiceStats;

typedef struct R6374ReqParams
{
    UINT4    u4ContextId;  /* Virtual context id maintained by VCM module */
    union
    {
        tR6374RxPduInfo     R6374RxPduInfo;   /*PDU Message recevied from MPLS */
        tR6374ServiceStats  R6374ServiceStats;  /* Get Stats for particular service last session */
    }unMsgParam;
    UINT1    u1MsgType;    /* Message type */
    UINT1    au1Pad[3];
#define InR6374RxPduInfo unMsgParam.R6374RxPduInfo
#define InR6374ServiceStats unMsgParam.R6374ServiceStats
}tR6374ReqParams;

typedef struct
{
    union
    {
        tR6374ApiDMStats    R6374DmStatsTable;
        tR6374ApiLMStats    R6374LmStatsTable;
    }unMsgParam;
    UINT1      u1ErrorCode;
    UINT1      au1Pad[3];
#define OutR6374DmStatsTable unMsgParam.R6374DmStatsTable
#define OutR6374LmStatsTable unMsgParam.R6374LmStatsTable
}tR6374RespParams;

PUBLIC VOID R6374TaskSpawn PROTO ((INT1 *));
PUBLIC INT4 R6374ApiHandleExtRequest (tR6374ReqParams *pR6374ReqParams, 
                                     tR6374RespParams *pR6374RespParams);
PUBLIC INT4 R6374UtilGetPathPointer (UINT4 u4ContextId, 
                                    tR6374PathId * pR6374PathId, 
                                    UINT4 *pu4ServiceOid, 
                                    INT4 *pi4OidLength);
#endif

