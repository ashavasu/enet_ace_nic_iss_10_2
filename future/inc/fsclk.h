/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: fsclk.h,v 1.7 2013/09/07 10:44:33 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              by external modules.
 ****************************************************************************/


#ifndef _FSCLK_H_
#define _FSCLK_H_

#include "lr.h"
#include "ip.h"
#include "cfa.h"
#include "fsbuddy.h"
#include "utlsll.h"

#define CLK_MOD_PTP  1
/*For SNTP module to access clkiwf APIs*/
#define CLK_MOD_NTP  2
#define CLK_MOD_SYS  3

#define CLK_PROTO_PTP     1
#define CLK_PROTO_NTP     2
#define CLK_PROTO_SYS     3
#define CLK_MAX_PROTOCOLS 4

/* Clock Time Source as per Table 7 IEEE 1588 */
#define CLK_ATOMIC_CLK        0x10
#define CLK_GPS_CLK           0x20
#define CLK_TERRESTRIAL_RADIO 0x30
#define CLK_PTP_CLK           0x40
#define CLK_NTP_CLK           0x50
#define CLK_HANDSET_CLK       0x60
#define CLK_OTHER_CLK         0x90
#define CLK_INTERNAL_OSCILLATOR_CLK 0xA0

/* Size of UINT8 string
 * Maximum u8 value = 18446744073709551615
 * strlen ("18446744073709551615") = 20
 * So the max string length should be 21
 */
#define CLK_U8_STR_LEN                  21

#define CLK_TIME_SRC_TRAP       0x00000004
#define CLK_CLASS_TRAP          0x00000008
#define CLK_ACCURACY_TRAP       0x00000010
#define CLK_VARIANCE_TRAP       0x00000020
#define CLK_HOLDOVER_TRAP       0x00000040

#define CLK_LOCK()   ClkMainLock()
#define CLK_UNLOCK() ClkMainUnLock()

typedef struct _FsClkTimeVal
{
    tUtlTm     UtlTmVal;/* Time in Day,Mon,Year,Hrs,Mins and Secs*/
    FS_UINT8   u8Sec;   /* Time in Seconds */
    UINT4      u4Nsec;  /* Time in nanoseconds */
} tFsClkTimeVal;

typedef struct _ClkSysTimeInfo 
{
    tFsClkTimeVal   FsClkTimeVal;
    UINT4           u4ContextId;
    UINT1           u1DomainId;
    UINT1           au1Pad[3];
}tClkSysTimeInfo;


typedef struct _ClkIwfPrimParams {

    FS_UINT8            u8CurrentUtcOffset;
                          /* Offset from current UTC */
    UINT4               u4ContextId;
                          /* Context Id */
    INT4                i4ClockTimeSource;
                          /* Clock Time Source */
    UINT2               u2OffsetScaledLogVariance;
                          /* Variance of the clock */
    UINT1               u1DomainId;
                          /* Domain Identifier */
    UINT1               u1Class;
                          /* Class of the clock */
    UINT1               u1Accuracy;
                          /* Accuracy of the clock */
    BOOL1               bIsHoldOver;
                          /* If OSIX_TRUE, it indicates that the clock
                           * is within hold over specifications
                           * */
    UINT1               au1Pad[2];
}tClkIwfPrimParams;


typedef struct _ClkRegInfo
{
    INT4           (*pClkChgIndiFn)
                        (tClkIwfPrimParams * pClkIwfPrimParams);
    UINT1          u1ModuleId; /* Protocol Id */
    UINT1          u1Event;
    UINT1          au1Pad[2]; /* Padding */
}tClkRegInfo;

/* CLKIWF call back events */ 
typedef enum { 
    CLKIWF_CUST_CLOCK_UPDATION = 1, 
    CLKIWF_WSS_CLOCK_UPDATION = 2, 
    CLKIWF_MAX_CALLBACK_EVENTS  
}tClkIwfCallBackEvents; 


PUBLIC INT4 ClkMainLock PROTO ((VOID));
PUBLIC INT4 ClkMainUnLock PROTO ((VOID));

PUBLIC INT4 ClkIwfGetClock PROTO ((UINT1 u1ModuleId, VOID * pTimeInfo));

PUBLIC INT4 
ClkIwfSetClock PROTO ((tClkSysTimeInfo * pClkSysTimeInfo, INT4 i4TimeSource));

PUBLIC VOID
ClkIwfGetParams PROTO ((tClkIwfPrimParams * pClkIwfPrimParams));


PUBLIC VOID
ClkIwfCbUpdateParams PROTO ((VOID));

PUBLIC VOID ClkRegRegisterProtocol PROTO ((tClkRegInfo * pClkRegInfo));

PUBLIC VOID ClkRegDeRegisterProtocol PROTO ((UINT1 u1ModuleId));

PUBLIC INT4 
ClkIwfCallBackRegister PROTO ((UINT4 u4Event, tFsCbInfo * pFsCbInfo)); 

PUBLIC VOID
ClkIwfGetUtcOffset PROTO ((tSNMP_OCTET_STRING_TYPE * pFsClkIwfCurrentUtcOffset));

PUBLIC VOID
ClkIwfSetUtcOffset PROTO ((tSNMP_OCTET_STRING_TYPE * pFsClkIwfCurrentUtcOffset));
#endif /* _FSCLK_H_ */
