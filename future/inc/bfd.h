/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bfd.h,v 1.37 2016/07/13 12:30:24 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              by external modules.
 ********************************************************************/

#ifndef _BFD_H
#define _BFD_H

#include "mplsapi.h"

#define BFD_MAX_AUTH_KEY_LEN 28

#define BFD_SNMP_TRUE  1
#define BFD_SNMP_FALSE 2

#define BFD_DEFAULT_CONTEXT_ID          0
#define BFD_INVALID_CONTEXT             0xffffffff

#define  BFD_ENABLED                   1
#define  BFD_DISABLED                  2

#define BFD_GLB_DES_MIN_TX_INTVL   1000000
#define BFD_GLB_REQ_MIN_RX_INTVL   1000000
#define BFD_GLB_DETECT_MULT        3
#define BFD_GLB_SLOW_TX_INTVL      1000000

#define BFD_VERSION_ONE                1    

#define BFD_MAX_PASSWD_LEN             256

#define BFD_MAX_CONTEXT_NAME           VCM_ALIAS_MAX_LEN

#define BFD_LSPP_PATH_TYPE_MEP LSPP_PATH_TYPE_MEP

#define BFD_MAX_CALL_BACK_BUF    200

#define MAX_IPV4_ADDR_LEN       4
#define MAX_IPV6_ADDR_LEN      16


/* This is the default detection timer value in BFD
 * This macro will be used by the BFD clients when 
 * registering with BFD. Unit of this value is microseconds
 */
#define BFD_DEFAULT_TIME_OUT       3000000 


/* Queue Message Types */
typedef enum
{
    BFD_CREATE_CONTEXT_MSG=1,
    BFD_DELETE_CONTEXT_MSG,
    BFD_RX_MSG,
    BFD_PATH_STATUS_CHG,
    BFD_OFFLD_CB_MSG,
    BFD_RECEIVE_PKT_FROM_SOCK,
    BFD_PROCESS_BOOTSTRAP_INFO,
#ifdef MBSM_WANTED
    BFD_MBSM_SLOT_CARD_PARAMS,
#endif
#ifdef L2RED_WANTED
    BFD_RM_MSG_RECVD,
#endif
    BFD_SESS_PARAM_CHG_MSG,
    BFD_CLIENT_REGISTER_FOR_IP_PATH_MONITORING,
    BFD_CLIENT_DEREGISTER_FROM_IP_PATH_MONITORING,
    BFD_CLIENT_DEREGISTER_ALL_FROM_IP_PATH_MONITORING,
    BFD_CLIENT_DISABLE_FROM_IP_PATH_MONITORING,
    BFD_QUE_OVERFLOW_MSG,
    BFD_V6_RECEIVE_PKT_FROM_SOCK,
#ifdef RFC6374_WANTED
    BFD_GET_SESS_INDEX_FROM_PW_INFO,
    BFD_GET_SESS_INDEX_FROM_LSP_INFO,
    BFD_GET_TXRX_FROM_SESS_INDEX,
    BFD_GET_EXP_FROM_SESS_INDEX
#endif
}tBfdApiReqTypes;

typedef enum
{
    BFD_CLIENT_ID_IP =1,
    BFD_CLIENT_ID_IP6,
    /*Dynamic clients should be added after this*/
    BFD_CLIENT_ID_OSPF,
    BFD_CLIENT_ID_BGP,
    BFD_CLIENT_ID_LDP,
    BFD_CLIENT_ID_OSPF3,
    BFD_CLIENT_ID_ISIS,
    BFD_MAX_CLIENTS
}tBfdClientId;

typedef enum
{
    BFD_START = 1,
    BFD_SHUTDOWN
}tBfdSystemCtrl;

typedef enum
{
    BFD_MPLS_PATH_UP = 0,
    BFD_MPLS_PATH_DOWN
}tBfdPathStatus;


/* MPLS header related parameters to be passed to the offload module*/
typedef enum
{
    BFD_DIAG_NO_DIAG=0,
    BFD_DIAG_CTRL_DETECTION_TIME_EXP,
    BFD_DIAG_ECHO_FUNC_FAILED, 
    BFD_DIAG_NEIGHBOR_SIGNALED_SESS_DOWN,
    BFD_DIAG_FWD_PLANE_RESET,
    BFD_DIAG_PATH_DOWN,
    BFD_DIAG_CONCAT_PATH_DOWN,
    BFD_DIAG_ADMIN_DOWN,
    BFD_DIAG_REV_CONCAT_PATH_DOWN
}tBfdDiagnostic;

typedef enum
{
    BFD_SESS_TYPE_SINGLE_HOP=1, 
    BFD_SESS_TYPE_MULTIHOP_TOTALLY_ARBITRARYPATHS, 
    BFD_SESS_TYPE_MULTIHOP_OUTOFBAND_SIGNALING, 
    BFD_SESS_TYPE_MULTIHOP_UNIDIRECTIONAL_LINKS, 
    BFD_SESS_TYPE_MULTIPOINT_HEAD, 
    BFD_SESS_TYPE_MULTIPOINT_TAIL
}tBfdSessType;

typedef enum
{
    BFD_SESS_ROLE_ACTIVE=1,
    BFD_SESS_ROLE_PASSIVE
}tBfdSessRole;

typedef enum
{
    BFD_OPER_MODE_ASYNC_W_ECHOFUNCTION=1,
    BFD_OPER_MODE_ASYNC_WO_ECHOFUNCTION,
    BFD_OPER_MODE_DEMAND_W_ECHOFUNCTION,
    BFD_OPER_MODE_DEMAND_WO_ECHOFUNCTION
}tBfdSessOperMode ;

typedef enum
{
    BFD_SESS_MODE_CC=1,
    BFD_SESS_MODE_CCV
}tBfdSessCcCvMode; 

typedef enum
{
    BFD_SESS_STATE_ADMIN_DOWN=0,
    BFD_SESS_STATE_DOWN,
    BFD_SESS_STATE_INIT,
    BFD_SESS_STATE_UP,
    BFD_SESS_MAX_STATES
}tBfdSessState;

typedef enum
{
    BFD_INET_ADDR_UNKNOWN=0,/*An unknown address type. This value MUST
                              be used if the value of the corresponding
                              InetAddress object is a zero-length string.
                              It may also be used to indicate an IP address
                              which is not in one of the formats defined
                              below.*/
    BFD_INET_ADDR_IPV4,    /* An IPv4 address as defined by the
                              InetAddressIPv4 textual convention*/
    BFD_INET_ADDR_IPV6,    /*An IPv6 address as defined by the
                             InetAddressIPv6 textual convention.*/
    BFD_INET_ADDR_DNS = 16 /*A DNS domain name as defined by the
                             InetAddressDNS textual convention.*/
}tBfdInetAddrType;


typedef enum
{
    BFD_AUTH_NONE=-1,
    BFD_AUTH_RESERVED,
    BFD_AUTH_SIMPLE_PASSWORD,
    BFD_AUTH_KEYED_MD5,
    BFD_AUTH_METICULOUS_KEYED_MD5,
    BFD_AUTH_KEYED_SHA1,
    BFD_AUTH_METICULOUS_KEYED_SHA1
}tBfdAuthType;

/* Offload - tBfdSessHwStartBfdCallFlag  - specifies the 
 * reason FsMiBfdHwStartBfd API call */
typedef enum 
{
    BFD_OFFLD_CREATE_SESSION,
    BFD_OFFLD_ENABLE_SESSION,
    BFD_OFFLD_POLL_INITIATE_UPD_PARAMS,
    BFD_OFFLD_POLL_TERMINATE,
    BFD_OFFLD_POLL_RESPONSE
}tBfdSessHwStartBfdCallFlag;


/* Offload - tBfdSessHwStopBfdCallFlag  - specifies the 
 * reason FsMiBfdHwStopBfd API call */  
typedef enum 
{
    BFD_OFFLD_DISABLE_SESSION,
    BFD_OFFLD_DELETE_SESSION
}tBfdSessHwStopBfdCallFlag;


/* BFD Offload callback event type enum definition */
typedef enum
{
    BFD_OFFLD_SESS_OFFLD_FAILED,
    BFD_OFFLD_SESS_DOWN_TO_INIT,
    BFD_OFFLD_SESS_DOWN_TO_UP,
    BFD_OFFLD_SESS_INIT_TO_UP,
    BFD_OFFLD_SESS_INIT_TO_DOWN,
    BFD_OFFLD_SESS_UP_TO_DOWN,
    BFD_OFFLD_SESS_LOC_DEFECT,
    BFD_OFFLD_SESS_MISCON_DEFECT,
    BFD_OFFLD_SESS_POLL_BIT_RX,
    BFD_OFFLD_SESS_FINAL_BIT_RX,
    BFD_OFFLD_SESS_DEFECT,
    BFD_OFFLD_SESS_DEFECT_CLEAR,
    BFD_OFFLD_SESS_PERIOD_MISCONF,
    BFD_OFFLD_SESS_ADMIN_DOWN_TMR_EXP,
    BFD_OFFLD_SESS_PERIOD_MISCONF_CLEAR,
    BFD_OFFLD_SESS_UPD_NEG_DETECT_MULTIPLIER,
    BFD_OFFLD_SESS_EVENT_ALL   /* Used only while registering */
}tBfdEventType;

/* Encapsulation type definition */
typedef enum  
{
    BFD_ENCAP_IPV4,              
    BFD_ENCAP_IPV6,              
    BFD_ENCAP_MPLS_IPV4,         
    BFD_ENCAP_MPLS_IPV6,         
    BFD_ENCAP_MPLS_ACH,          
    BFD_ENCAP_MPLS_IPV4_ACH,     
    BFD_ENCAP_MPLS_IPV6_ACH,     
    BFD_ENCAP_VCCV_NEGOTIATED
}tBfdEncapType;

/*  Encapsulation type as defined in mib */
typedef enum  
{
    BFD_ENCAP_TYPE_MPLS_IP = 1,              
    BFD_ENCAP_TYPE_MPLS_ACH,          
    BFD_ENCAP_TYPE_MPLS_IP_ACH,     
    BFD_ENCAP_TYPE_VCCV_NEGOTIATED
}tBfdMibEncapType;

/*Type of monitoring done*/
typedef enum
{
    BFD_MON_INDEPENDENT_EGRESS =1,
    BFD_MON_INDEPENDENT_INGRESS,
    BFD_MON_BIDIRECTIONAL
}tBfdSessMonMode;

/* Enum defining the path types */
typedef enum
{
    BFD_PATH_TYPE_NONTE_IPV4=1, /* LDP LSP */
    BFD_PATH_TYPE_NONTE_IPV6,
    BFD_PATH_TYPE_TE_IPV4, /* TE Tunnel Working LSP*/
    BFD_PATH_TYPE_TE_IPV6,
    BFD_PATH_TYPE_PW,    /* PW */
    BFD_PATH_TYPE_MEP,    /* Static paths identified by MEG MEP */
    BFD_PATH_TYPE_TE_IPV4_PROTECTING, /* TE Tunnel Protecting LSP */
    BFD_PATH_TYPE_IPV4,
    BFD_PATH_TYPE_IPV6,
    BFD_PATH_TYPE_MAX_ENTRY    /* Maximum allowed Value */
}tBfdPathType;

typedef enum
{
    BFD_ERR_POST_EVENT_FAILED,
    BFD_ERR_ENQUEUE_FAILED
}tBfdErrCode;

typedef enum {
    BFD_OTHER=1, /* Static and dynamic */
    BFD_VOLATILE, /* Dynamic session only */
    BFD_NONVOLATILE, /* Static session only */
    BFD_PERMANENT,
    BFD_READONLY
}tBfdStorageType;

/* This structure is used to store the LDP FEC of the Path used by BFD */
typedef  struct 
{
    tIpAddr  PeerAddr;    /* Peer Address */ 
    UINT1    u1AddrType;  /*IPv4/IPv6 */
    UINT1    u1AddrLen;   /*Prefix length */
    UINT1    au1Pad[2];   /*Pad*/
}tBfdSessNonTeParams;

/* This structure contains the Tunnels params to identify the tunnel */
typedef struct 
{
    tIpAddr  SrcIpAddr; /* Tunnel source - IP Address/Local Map number */
    tIpAddr  DstIpAddr; /* Tunnel destination - IP Address/Local Map number */
    UINT4 u4TunnelId;    /* Tunnel index */
    UINT4 u4TunnelInst;  /* Tunnel LSP number */
    UINT4 u4AddrType;    /* IPv4/IPv6/GlobalId-NodeId */ 
}tBfdSessTeParams;

/* This Structure contains the PW params to indentify the PW */
typedef struct 
{
    tIpAddr PeerAddr; /* Peer Address */
    UINT4     u4VcId;   /* Virtual Circuit Identifier */
}tBfdSessPwParams;

/* This structure contains the ME Information */
typedef struct 
{
    UINT4 u4MegId;  /* Maintenance Entity Group index */
    UINT4 u4MeId;   /* Maintenance Entity index */
    UINT4 u4MpId;   /* Maintenace Point index [MEP/MIP] */
}tBfdSessMeParams; 


/* This structure contains the information of the path BFD 
 * is used to monitor */
typedef struct 
{
    tBfdPathType ePathType;
    union
    {
        tBfdSessNonTeParams  BfdSessNonTeParams;
        tBfdSessTeParams     BfdSessTeParams;
        tBfdSessPwParams     BfdSessPwParams;
        tBfdSessMeParams     BfdSessMeParams;
    }unSessParams;
#define SessMeParams unSessParams.BfdSessMeParams
#define SessPwParams unSessParams.BfdSessPwParams
#define SessTeParams unSessParams.BfdSessTeParams
#define SessNonTeParams unSessParams.BfdSessNonTeParams
}tBfdSessPathParams; 


/* BFD parameters for received control packet */
typedef struct
{
    tIpAddr  BfdPeerIpAddress;       /* IP Address of the peer */
    tBfdDiagnostic      eBfdRxPktDiag;
    UINT4               u4BfdRxPktMinTxIntrvl;
    UINT4               u4BfdRxPktReqMinRxIntrvl;
    UINT1               u1BfdRxPktVersionNumber;
    UINT1               u1BfdRxPktDetectMulti;
    UINT1               u1BfdRxPktSta;            /* Sta field received */
    BOOL1               bBfdRxPktDemandMode;
    BOOL1               bBfdRxPktCFlag;           /* C bit in the packet */
    BOOL1               bBfdRxPktPollBit;
    BOOL1               bBfdRxPktFinalBit;
    UINT1               au1Pad[1]; /*Pad*/
}tBfdRxCtrlPktParams;

typedef struct
{
    tCRU_BUF_CHAIN_HEADER   *pBuf; /*Pointer to the packet buffer */
    UINT4                   u4IfIndex; /* Index on which packet is received */
}tBfdRecvBfdPacket;

/*This structure is used to store info given by OAM */
typedef struct
{
    UINT4  u4SessIndex; /*Session index of the BFD Session*/
    UINT4  u4PathStatus; /*Session index of the BFD Session*/
}tBfdOamInInfo;

/*This structure is the input for the Entry function of BFD*/
typedef struct 
{
    tMplsLblInfo OutLabelList[MPLS_MAX_LABELS_PER_ENTRY];
    tMplsLblInfo InLabelList[MPLS_MAX_LABELS_PER_ENTRY];
    UINT1 u1OutLblStkCnt; /* Outgoing label stack count. */
    UINT1 u1InLblStkCnt; /* Incoming label stack count. */
    UINT1 au1Pad[2];
}tBfdMplsHdrParams;


/* Path info structure */
typedef struct
{
    tBfdSessPathParams  BfdTxPathInfo;
    tBfdMplsHdrParams   LabelList;
    UINT1               au1SrcMacAddr[CFA_ENET_ADDR_LEN];
    UINT1               au1DstMacAddr[CFA_ENET_ADDR_LEN];
    UINT4               u4IngrItf;
    UINT4               u4EggrItf;
    UINT4               u4PwIfIndex; /* Pseudo-Wire Interface Index */
    UINT4               u4PwVcIndex; /* Pseudo-Wire Index */
    UINT4               u4VpnId;     /* VPN Identifier */
    INT4                u4PhyPort;   /* Physical Port of Tunnel/PW */
    UINT2               u2Vlan;
    UINT2               u2VplsFdbId;   /* Field to store the VPLS FDB Id*/
    UINT1               u1ServiceType; /* Service Type - PW/Tunnel */
    UINT1               u1CcSelected; /* VCCV: ACH, TTL expiry and router alert label */
    UINT1               au1Pad[2];
}tBfdSessionPathInfo;


/* BFD session related parameters */
typedef struct 
{
    tBfdSessType        eBfdSessType;
    tBfdSessOperMode    eBfdSessOperMode;
    tBfdSessCcCvMode    eBfdSessCcCvMode;
    tBfdInetAddrType    eBfdSessSrcAddrType;
    tBfdInetAddrType    eBfdSessDstAddrType;
    tBfdSessRole        eBfdSessRole;
    tBfdSessMonMode     eBfdSessMonMode;
    UINT4               u4NegoTxInterval;
    UINT4               u4DetectionTimeIntvl;
    UINT4               u4BfdSessInterface;
    UINT1               u1BfdSessNegoDetectMult;
    BOOL1               bBfdSessRemoteHeardFlag;
    BOOL1               bBfdSessTmrNegotiate;
    UINT1               u1Pad; /*Pad*/
}tBfdSessParams;


typedef struct
{
    UINT4 u4BfdGblSlowTxIntvl;
}tBfdGblParams;


/* BFD control packet parameters */
typedef struct 
{
    tBfdRxCtrlPktParams BfdRxCtrlPktParams;
    tBfdDiagnostic      eBfdSessTxPktDiag;
    tBfdSessState       eBfdSessState;
    tBfdAuthType        eBfdSessAuthType;
    UINT4               u4BfdSessRemoteDiscr;
    UINT4               u4BfdSessTxPktDesiredMinTxInterval;
    UINT4               u4BfdSessTxPktReqMinRxInterval;
    UINT1               au1BfdSessAuthKey[BFD_MAX_PASSWD_LEN];
    UINT1               u1BfdSessTxPktVersionNumber;
    UINT1               u1BfdSessTxPktDetectMult;
    UINT1               u1BfdCtrlPktLen;
    UINT1               u1BfdSessAuthLength;
    UINT1               u1BfdSessAuthKeyLen;
    INT1                i1BfdSessAuthKeyID;
    BOOL1               bBfdTxPktPollBit;
    BOOL1               bBfdTxPktFinalBit;
    BOOL1               bBfdSessTxPktCtrlPlaneIndepFlag;
    BOOL1               bAuthPresFlag;
    BOOL1               bTxPktDemandMode;
    BOOL1               bBfdSessMultipointFlag;

    /* Hardware Action Flag set to TRUE/FALSE by offload module when the events
     are notified.  This flag is set to to TRUE when offload module sends only
     the current state and do not send the previous state. This flag is set to
     FALSE when offload module sends both the current and previous states. If this
     flag is set to TRUE, call back function maps the current state and previous
     state and notifies BFD module about the state change. By default, this will
     be set to FALSE. */
    UINT1               u1BfdOffHwActionFlag;
    UINT1               au1Pad[3]; /*Pad*/
}tBfdCtrlPktParams;


typedef struct
{
    UINT2       u2SrcUdpPort;
    UINT2       u2DstUdpPort;
}tBfdUdpHdrParams;

typedef struct
{
    UINT4       u4SrcIpAddr;
    UINT4       u4DestIpAddr;
}tBfdIpHdrParams;  

typedef struct 
{
    tIpAddr    SrcAddr;
    tIpAddr    DstAddr;
}tBfdIp6HdrParams;



/* Encapsulation header definitions */
typedef struct 
{
    tBfdUdpHdrParams  UdpHdrInfo;
    union
    {
        tBfdIpHdrParams   IpHdrInfo;
        tBfdIp6HdrParams  Ip6HdrInfo;
    }unHdrParams;   
}tBfdIpUdpHdrParams;


typedef struct 
{
    UINT2        u2ChannelType;
    UINT1        u1AchNibble;
    UINT1        u1AchVersion;
    UINT1        u1AchFlags;
    UINT1        au1Pad[3]; /*Pad*/
}tBfdAchHdrParams;



typedef struct 
{
    tBfdAchHdrParams       BfdAchHdrInfo;
    tMplsAchTlvHdr            AchTlvHdrInfo;
    tMplsAchTlv                 MplsAchTlv;
}tBfdAchParams;

/* ACH header parameters to be passed to the offload module*/
typedef struct 
{
    tBfdAchParams BfdAchInfo;
}tBfdMplsTpAchHdrParams;

typedef struct 
{
    tBfdAchParams BfdAchInfo;
    tBfdIpUdpHdrParams BfdPktIpUdpHdrInfo;
}tBfdIpMplsTpAchHdrParams;



/* Encapsulation parameters for BFD */
typedef struct 
{
    tBfdEncapType  eBfdEncapType;
    union 
    {
        tBfdIpUdpHdrParams         BfdIpUdpHdrInfo;
        tBfdMplsTpAchHdrParams     BfdMplsTpAchHdrInfo;
        tBfdIpMplsTpAchHdrParams   BfdIpMplsTpAchHdrInfo;
    }unEncapParams;
}tBfdCtrlPktEncapParams;

/* BFD parameters structure - Defines the BFD session parameters and the 
 * BFD packet information to be passed to the offload module */
typedef struct 
{
    tBfdGblParams           BfdGblInfo;
    tBfdSessParams          BfdSessInfo;
    tBfdCtrlPktParams       BfdCtrlPktInfo;
    tBfdCtrlPktEncapParams  BfdTxCtrlPktEncapInfo;
    tBfdCtrlPktEncapParams  BfdRxCtrlPktEncapInfo;
}tBfdParams;



/* BFD session statistics */
typedef struct 
{
    FS_UINT8  u8BfdSessPerfCtrlPktInHC;
    FS_UINT8  u8BfdSessPerfCtrlPktOutHC;
    FS_UINT8  u8BfdSessPerfCtrlPktDropHC;
    UINT4 u4BfdSessPerfCtrlPktIn;
    UINT4 u4BfdSessPerfCtrlPktOut;
    UINT4 u4BfdSessPerfCtrlPktDrop;
    UINT4 u4BfdSessPerfCtrlPktDropLastTime;
    UINT4 u4BfdSessPerfDiscTime;
    UINT4 u4BfdSessPerfCCPktIn;
    UINT4 u4BfdSessPerfCVPktIn;
    UINT4 u4BfdSessPerfCCPktOut;
    UINT4 u4BfdSessPerfCVPktOut;
    UINT4 u4BfdSessMisDefCount;
    UINT4 u4BfdSessLocDefCount;
    UINT4 u4BfdSessRdiInCount;
    UINT4 u4BfdSessRdiOutCount;
}tBfdSessStats;

typedef struct
{
    UINT4              u4BfdSessHwHandle;
    UINT4              u4BfdOffErrNum;
}tBfdHwhandle;

/* BFD session handle structure */
typedef struct 
{
    UINT4               u4BfdSessId;
    UINT4               u4BfdSessLocalDiscr;
    UINT4               u4BfdSlotId;
    UINT4               u4BfdCardNum;
}tBfdSessionHandle;

/* Structure used by Offload CB */
typedef struct
{
    tBfdSessionHandle     BfdSessHandle;
    tBfdEventType         eBfdEventType;
    tBfdParams            BfdParams;
    tCRU_BUF_CHAIN_HEADER *pBuf;
    tBfdHwhandle         BfdSessHwHandle;
    BOOL1                 bActionFlag;
    UINT1                 au1Pad[3];
}tBfdOffCbParams;

/* structure to give BFD packet to offload module */
typedef struct
{
    UINT1 *pau1PktBuf;
    UINT4 u4Len;
}tBfdOffPktBuf;

#ifdef MBSM_WANTED
typedef struct
{
    UINT4 u4SessionId;
    UINT4 u4CardNum;
    UINT4 u4SlotId;
}tBfdMbsmParams;
#endif

typedef struct
{
    UINT4 u4SessionId;
    BOOL1 bAllowParamChg; /* If OSIX_TRUE, indicates params are allowed to 
                             change after session is active.
                             If OSIX_FALSE, indicates params are not allowed
                             to change after session is active. */
    BOOL1 bForAllSession; /* If OSIX_TRUE, all the sessions configured
                             till that point are modified */
    UINT1 u1Pad[2];
}tBfdAllowParamChgMsg;

typedef struct 
{
    tIpAddr   SrcAddr;
    tIpAddr   NbrAddr;
    UINT4     u4IfIndex;
    UINT4     u4PathStatus;
    UINT1     u1AddrType;
    BOOL1     bIsSessCtrlPlaneInDep;
    UINT1     u1Pad [2];
}tBfdClientNbrIpPathInfo;

typedef struct
{
   UINT4    u4DesiredDetectionTime;
   INT1     (*pBfdNotifyPathStatusChange) (UINT4, tBfdClientNbrIpPathInfo *);
   UINT1    u1EventMask;
   UINT1    au1pad [3];
}tBfdClientParams;

typedef struct
{
    tBfdClientNbrIpPathInfo BfdClientNbrIpPathInfo;
    tBfdClientParams        BfdClientParams;
    UINT1     u1SessionType;
    UINT1     u1BfdClientId;
    UINT1     au1Pad [2];
}tBfdClientInfo;

typedef struct
{
    tCRU_BUF_CHAIN_HEADER  *pBuf;
    tBfdSessPathParams     PathId;
    tIpAddr                DestIpAddr;
    UINT4                  u4ErrorCode;
    UINT4                  u4OffSet;
    UINT4                  u4SrcUdpPort;
    UINT4                  u4OutIfIndex;
    UINT4                  u4BfdDiscriminator;
    UINT4                  u4SessionIndex;
    UINT1                  au1NextHopMac[MAC_ADDR_LEN];
    UINT1                  u1ReplyPath;      /* LSP/IP */
    UINT1                  NextHopIpAddrType; /* IPv4/IPv6 */
    BOOL1                  bIsEgress;          /* In egress/ In Ingress */
    UINT1                  au1Pad[3];
}tBfdLsppInfo;

#ifdef L2RED_WANTED
/* Redundancy related definitions start here */

typedef struct _BfdRmCtrlMsg {
    tRmMsg           *pData;     /* RM message pointer */
    UINT2             u2DataLen; /* Length of RM message */
    UINT1             u1Event;   /* RM event */
    UINT1             au1Pad[1];
}tBfdRmCtrlMsg;

typedef struct _BfdRmMsg
{
    tBfdRmCtrlMsg RmCtrlMsg; /* Control message from RM */
} tBfdRmMsg;

#endif

#ifdef RFC6374_WANTED
typedef struct
{
    tBfdSessPathParams     PathId;
    UINT4            u4SubReqType; /* Sub request type */
    INT4              i4BfdSessAdminStatus;
    UINT4                  u4BfdSessionIndex; /* BFD Session Index*/
    UINT4              u4BfdSessPerfCtrlPktIn;
    UINT4              u4BfdSessPerfCtrlPktOut;
    UINT4                  u4BfdEXPValue;   /* Traffic Class */
}tBfdR6374Info;
#endif

typedef struct
{
    union {
        tBfdRecvBfdPacket    BfdRecvBfdPacket;
        tBfdLsppInfo           BfdLsppInfo;
        tBfdOamInInfo          BfdOamInInfo;
        tBfdOffCbParams      BfdOffCbParams;
#ifdef MBSM_WANTED
        tMbsmProtoMsg         *pMbsmProtoMsg; /* MbsmProtoMsg received from 
                                                 MBSM */
        tBfdMbsmParams       BfdMbsmParams;
#endif
#ifdef L2RED_WANTED
        tBfdRmMsg            BfdRmMsg;
#endif
#ifdef RFC6374_WANTED
        tBfdR6374Info      BfdR6374Info;
#endif
        tBfdAllowParamChgMsg BfdAllowParamChgMsg; /* modify allow param chg */
        tBfdClientInfo       BfdClientInfo;
        INT4                 i4SockId;
    }unReqInfo;
    UINT4   u4ContextId; /* Virtual Context Id */
    UINT4   u4ReqType; /* Indicates Request Type */
#define LsppBtStrapInfo unReqInfo.BfdLsppInfo 
#define BfdClntInfo unReqInfo.BfdClientInfo
#define BfdClntInfoParams unReqInfo.BfdClientInfo.BfdClientParams
}tBfdReqParams;

/*This structure is the output for the Entry function of BFD*/
typedef struct
{
    tBfdErrCode    eBfdErrCode;
#ifdef RFC6374_WANTED
        tBfdR6374Info         BfdR6374Info;
#endif
}tBfdRespParams;

/* Structure used for fetching the H/W session info */
typedef struct _BfdNpInput
{
    UINT4 u4ContextId;
    UINT4 u4BfdSessId;
}tBfdNpInput;

typedef struct _BfdNpOutput
{
    UINT4 u4ContextId;
    UINT4 u4BfdSessId;
    tBfdParams *pBfdParams;
}tBfdNpOutput;

typedef struct _NpBfdInput
{
        UINT4   u4ContextId;
            UINT4   u4BfdSessId;
}tNpBfdInput;

typedef struct _NpBfdOutput
{
        UINT4 u4ContextId;
            UINT4 u4BfdSessId;
                tBfdParams BfdParams;
}tNpBfdOutput;

/**************************bfdapi.c******************************/
INT4 BfdApiHandleExtRequest PROTO ((UINT4 , tBfdReqParams *,
                                    tBfdRespParams *));

PUBLIC VOID BfdTaskSpawnBfdTask PROTO ((INT1 *));

PUBLIC INT4 BfdMbsmPostMessage PROTO ((tMbsmProtoMsg *, INT4));

VOID BfdApiUpdateChannelTypes (tMplsAchChannelType * pMplsAchChannelInfo,
                               BOOL1 bLockReq);

INT4 FsMiBfdGetNextHwBfdSessEntry (tNpBfdInput NpBfdInput,
                                   tNpBfdOutput *pNpBfdOutput);

UINT4 BfdNpGetNext PROTO ((tBfdNpInput BfdNpInput,
                      tBfdNpOutput *pBfdNpOutput));

INT4 BfdApiCheckControlPlaneDependent PROTO ((UINT4, tBfdClientNbrIpPathInfo *));

PUBLIC INT4 BfdMainModuleStart       PROTO ((VOID));
PUBLIC VOID BfdMainModuleShutdown    PROTO ((VOID));

INT4
BfdUtilIsDirectlyConnected (UINT4 u4ContextId, UINT1 *pu1Address,
                            INT4 i4AddrType);
INT4
BfdUtilIpv6IsDirectlyConnected (UINT4 u4ContextId, UINT1 *pu1Address);

#endif /* _BFD_H */
