/********************************************************************
* Copyright (C) 2015 Aricent Inc . All Rights Reserved
*
* $Id: r2544.h,v 1.5 2016/03/18 13:24:36 siva Exp $
*
* Description: This file contains the function prototypes and macros
*              used in RFC2544 module*
*******************************************************************/
#ifndef __R2544_H__
#define __R2544_H__

#define  RFC2544_LOCK()      R2544ApiLock ()
#define  RFC2544_UNLOCK()    R2544ApiUnLock ()

#define RFC2544_SUCCESS OSIX_SUCCESS
#define RFC2544_FAILURE OSIX_FAILURE

/* Trace Options */
#define RFC2544_NO_TRC                 0x00000000
#define RFC2544_INIT_SHUT_TRC          0x00000001
#define RFC2544_MGMT_TRC               0x00000002
#define RFC2544_Y1731_INTF_TRC         0x00000004
#define RFC2544_RESOURCE_TRC           0x00000008
#define RFC2544_TIMER_TRC              0x00000010
#define RFC2544_CRITICAL_TRC           0x00000020
#define RFC2544_BENCHMARK_TEST_TRC     0x00000040
#define RFC2544_SESSION_RECORD_TRC     0x00000080
#define RFC2544_ALL_FAILURE_TRC        0x00000100
#define RFC2544_ALL_TRC                0x000001ff



#define RFC2544_ZERO   0

#define RFC2544_NOT_INITIATED   1
#define RFC2544_INPROGRESS      2
#define RFC2544_COMPLETED       3
#define RFC2544_ABORTED         4


#define ECFM_R2544_RESULT_RECEIVED        40

#define Y1731_TRUE  1
typedef tOsixSysTime  tR2544SysTime; 
VOID
R2544Task (INT1 *pi1Arg);

INT4
R2544ApiLock (VOID);

INT4
R2544ApiUnLock (VOID);

/* Queue Message Types */
typedef enum
{
    R2544_CREATE_CONTEXT_MSG = 300,
    R2544_DELETE_CONTEXT_MSG,
    R2544_THROUGHPUT_TEST_RESULT,
    R2544_LATENCY_TEST_RESULT,
    R2544_FRAMELOSS_TEST_RESULT,
    R2544_BACKTOBACK_TEST_RESULT,
    R2544_PERF_TEST_PARAMS
}tR2544ApiReqTypes;

typedef enum
{
    R2544_REQ_VCM_IS_CONTEXT_VALID = 250,
    R2544_REQ_VCM_GET_CONTEXT_NAME,
    R2544_REQ_START_THROUGHPUT_TEST,
    R2544_REQ_START_LATENCY_TEST,
    R2544_REQ_START_FRAMELOSS_TEST,
    R2544_REQ_START_BACKTOBACK_TEST,
    R2544_REQ_STOP_BENCHMARK_TEST,
    R2544_REQ_CFM_REG_MEP_AND_FLT_NOTIFY,
    R2544_REQ_CFM_DEREGISTER_MEP,
    R2544_REQ_SLA_TEST_RESULT,
    R2544_REQ_CFM_MEP_INDEX_VALIDATION
}tR2544ExtReqType;

#define RFC2544_THROUGHPUT_TEST       1
#define RFC2544_LATENCY_TEST          2
#define RFC2544_FRAMELOSS_TEST        3 
#define RFC2544_BACKTOBACK_TEST       4 

typedef enum
{
    R2544_ERR_POST_EVENT_FAILED,
    R2544_ERR_ENQUEUE_FAILED
}tR2544ErrCode;


typedef struct
{
    tR2544ErrCode    eR2544ErrCode;
}tR2544RespParams;

typedef struct  
{

        struct
        {
            UINT4      u4SlaId;
            UINT4      u4MEG;
            UINT4      u4MEP;
            UINT4      u4ME;
            UINT4      u4MegLevel;
        }SlaInfo;
        struct 
        { 
            tMacAddr        TxSrcMacAddr;
            tVlanId         InVlanId;
            tMacAddr        TxDstMacAddr;
            tVlanId         OutVlanId;
            UINT4           u4IfIndex;
            UINT4           u4TagType;
            UINT4           u4PortSpeed;
        }TestParams;

        union
        {
            struct
            {
                UINT4 u4TrialCount;
                UINT4 u4Rate;
                UINT4 u4TxCount;
                UINT4 u4RxCount;
            }ThroughputResults;
            struct
            {
                UINT4 u4TrialCount;
                UINT4 u4LatencyMin;
                UINT4 u4LatencyMean;
                UINT4 u4LatencyMax;
                UINT4 u4TxCount;
                UINT4 u4RxCount;
            }LatencyResults;
            struct
            {
                UINT4 u4TrialCount;
                UINT4 u4FrameLoss;
                UINT4 u4TxCount;
                UINT4 u4RxCount;
            }FrameLossResults;
            struct
            {
                UINT4 u4TrialCount;
                UINT4 u4BurstSize;
                UINT4 u4TxCount;
                UINT4 u4RxCount;

            }BackToBackResults;
        }unReqParams;
        UINT4   u4ContextId; /* Virtual Context Id */
        UINT4   u4ReqType; /* Indicates Request Type */
        UINT4   u4FrameSize;
        UINT1   u1SubTest;
        UINT1   au1Reserved[3]; 
#define ThroughputResult unReqParams.ThroughputResults
#define LatencyResult unReqParams.LatencyResults
#define FrameLossResult unReqParams.FrameLossResults
#define BackToBackResult unReqParams.BackToBackResults

}tR2544ReqParams;


/**************************r2544api.c******************************/
PUBLIC INT4
R2544ApiHandleExtRequest PROTO ((tR2544ReqParams *,
                                tR2544RespParams *));

PUBLIC VOID R2544ApiModuleShutDown PROTO ((VOID));

PUBLIC INT4 R2544ApiCreateContext PROTO ((UINT4));
PUBLIC INT4 R2544ApiDeleteContext PROTO ((UINT4));

#endif /*__R2544_H__*/
