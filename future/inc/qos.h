/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: qos.h,v 1.16 2010/05/19 07:12:26 prabuc Exp $
 *
 * Description:This file contains the exported definitions and  
 *             macros of QOS                                   
 *
 *******************************************************************/
#ifndef _QOS_H
#define _QOS_H

/* Added for INTSRV for the purpose of sending ARP request*/
#define   tQoSData   tCRU_BUF_CHAIN_HEADER 

#ifdef QoS_INTSRV_WANTED
#define   QoS_MODULE_ID            2
#endif

#define   QoS_SERVICE_TYPE_CLS     5
#define   QoS_SERVICE_TYPE_GS      2
/* QoS return codes */
#define   QoS_FAILURE             -1
#define   QoS_SUCCESS              0
#define   QoS_DROP_PACKET          2
#define   QoS_QUEUE_PKT            3
#define   QoS_PKT_SHAPED           4
#define   QoS_INTSRV_PKT           5

/* Profile/configuration commands */
#define   QoS_INSTANTIATE          1
#define   QoS_UPDATE               2
#define   QoS_DELETE               3

INT4 QoSInit (VOID);
/* Reset PriorityMap Priority field */
#define   QOS_RESET_PRI_MAP_PRI    0xFF
#endif /* _QOS_H */
