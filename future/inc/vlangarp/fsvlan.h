/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsvlan.h,v 1.241 2018/01/19 12:41:34 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              by  external modules.
 *
 *******************************************************************/
#ifndef _VLAN_H_
#define _VLAN_H_

#include "rmgr.h"
#include "mpls.h"
#include "rmon.h"
#include "evcnp.h"

enum {
    VLAN_PI_MASK = 1, /* To remove MCLAG interface in egress list */
    VLAN_PI_UNMASK, /* To add MCLAG interface in egress list */
    VLAN_PI_NOT_REQUIRED /* Port isolation is not needed */
};


extern tMacAddr gReservedAddress;
extern tMacAddr gBcastAddress; 
extern tMacAddr gMulticastAddress; 
extern tMacAddr gVlanVrrpIpv6MCastAddress;

#define DOT_1D_BRIDGE_MODE             1
#define DOT_1Q_VLAN_MODE               2
#define DOT_1D_IN_PROGRESS_MODE        3

#define VLAN_SUCCESS                SUCCESS  /* 0 */
#define VLAN_FAILURE                FAILURE  /* -1 */
#define VLAN_DISABLED_BUT_NOT_DELETED  2

#define VLAN_TRUE                      1
#define VLAN_FALSE                     0

#define VLAN_ENABLED                   1
#define VLAN_DISABLED                  2
#define VLAN_DEFAULT                      3

#define VLAN_OPER_UP                1
#define VLAN_OPER_DOWN                 2
 
#define VLAN_TAG_LEN                   4

#define VLAN_PORT_MAC_LEARNING_ENABLED  1
#define VLAN_PORT_MAC_LEARNING_DISABLED 2

#define VLAN_PORTS_PER_BYTE            8
#define VLAN_VLANS_PER_BYTE            8

#define VLAN_OPTIMIZE                   1
#define VLAN_NO_OPTIMIZE                2

#define VLAN_NULL_VLAN_ID             0
#define VLAN_E_LINE                   1
#define VLAN_E_LAN                    2

#define VLAN_INVALID_SEL_ROW         0
#define VLAN_INVALID_DE              -1

#define VLAN_8P0D_SEL_ROW            1
#define VLAN_7P1D_SEL_ROW            2
#define VLAN_6P2D_SEL_ROW            3
#define VLAN_5P3D_SEL_ROW            4
#define VLAN_STATIC_MAX_NAME_LEN     32
#define   VLAN_DATA_FRAME          1
#define   VLAN_CNTL_FRAME          2
#define   VLAN_MCAST_FRAME         3
#define   VLAN_CFM_FRAME           4

#define VLAN_TAGGED               1
#define VLAN_PRIORITY_TAGGED      2
#define VLAN_UNTAGGED             3

#define VLAN_CLI_UNICAST_MAC_SEC_SAV      1
#define VLAN_CLI_UNICAST_MAC_SEC_SHV      2
#define VLAN_CLI_UNICAST_MAC_SEC_OFF      3

#define VLAN_DE_FALSE         0
#define VLAN_DE_TRUE          1

#define VLAN_OPCODE_SET_MCAST_INDEX 1
#define VLAN_OPCODE_GET_MCAST_INDEX 2
#define VLAN_FRAME_MAXIMUM_LENGTH 1522

#define VLAN_DEFAULT_FILTERING_CRITERIA   1
#define VLAN_ENHANCE_FILTERING_CRITERIA   2

#define VLAN_PORT_EGRESS_PORTBASED 1
#define VLAN_PORT_EGRESS_VLANBASED 2

#define VLAN_PORT_DEFAULT_ALLOWABLE_TPID 0x0000

#define VLAN_MAX_OTHER_PROTOCOL_SUPPORT 10
#define VLAN_UNSUPPORTED 5
/* EVB HW Config Values */
#define VLAN_EVB_HW_SCH_IF_CREATE               1
#define VLAN_EVB_HW_SCH_IF_DELETE               2
#define VLAN_EVB_HW_SCH_IF_TRAFFIC_BLOCK        3
#define VLAN_EVB_HW_SCH_IF_TRAFFIC_FORWARD      4


#define VLAN_EVB_SVID_UPDATE 1
#define VLAN_EVB_SVID_DELETE 2
/* This macro is added for pseudo wire visibility to VLAN.
 * Where this is specific to VLAN module. It is allowed to change
 * this macro from SYS_DEF_MAX_L2_PSW_IFACES to some other value.
 * But it should be less than SYS_DEF_MAX_L2_PSW_IFACES.
 */
#define VLAN_MAX_L2_PW_IFACES       SYS_DEF_MAX_L2_PSW_IFACES

/* This macro is added for Attachment Circuit interface in VLAN.
 * Where this is specific to VLAN module. It is allowed to change
 * this macro from SYS_DEF_MAX_L2_AC_IFACES to some other value.
 * But it should be less than SYS_DEF_MAX_L2_AC_IFACES.
 */
#define VLAN_MAX_L2_AC_IFACES       SYS_DEF_MAX_L2_AC_IFACES

#define VLAN_MAX_NVE_IFACES         SYS_DEF_MAX_NVE_IFACES 

#define VLAN_MAX_PORTS              (L2IWF_MAX_PORTS_PER_CONTEXT +\
                                     VLAN_MAX_L2_PW_IFACES + \
                                     VLAN_MAX_L2_AC_IFACES + \
                                     SYS_DEF_MAX_NVE_IFACES + \
                                     MAX_TAP_INTERFACES + \
                    SYS_DEF_MAX_SBP_IFACES)
           
#define VLAN_MAX_PHY_PORTS          BRG_MAX_PHY_PORTS
#define VLAN_DEF_CONTEXT_ID         L2IWF_DEFAULT_CONTEXT

#define VLAN_STATIC_ADDR          5

#define VLAN_SHOW_WILD_CARD_ENTRY 0
#define VLAN_SHOW_WILD_CARD_ENTRY_ALL 1

#ifdef ICCH_WANTED
#define VLAN_ICCH_DEFAULT_INST ICCH_DEFAULT_INST
#else
#define VLAN_ICCH_DEFAULT_INST 0
#endif

/* Check whether the u2Port is set on the au1PortArray */
#define VLAN_LIST_IS_MEMBER_VLAN(au1PortArray, u2Port, u1Result) \
        {\
           UINT2 u2PortBytePos;\
           UINT2 u2PortBitPos;\
           u2PortBytePos = (UINT2)(u2Port / VLAN_PORTS_PER_BYTE);\
           u2PortBitPos  = (UINT2)(u2Port % VLAN_PORTS_PER_BYTE);\
    if (u2PortBitPos  == 0) {u2PortBytePos -= 1;} \
           \
           if ((u2PortBytePos < VLAN_LIST_SIZE) && \
               (au1PortArray[u2PortBytePos] \
                & gau1PortBitMaskMap[u2PortBitPos]) != 0) {\
           \
              u1Result = VLAN_TRUE;\
           }\
           else {\
           \
              u1Result = VLAN_FALSE; \
           } \
        }

/* Removes VLAN members of au1List2 from au1List1 */
#define VLAN_RESET_VLAN_LIST(au1List1, au1List2) \
              {\
         UINT2 u2ByteIndex = 0;\
  \
  for (u2ByteIndex = 0;\
       u2ByteIndex < VLAN_LIST_SIZE;\
              u2ByteIndex++) {\
     au1List1[u2ByteIndex] &= (UINT1) (~au1List2[u2ByteIndex]);\
            }\
  }

/* Set the u2Port on the au1PortArray */
#define VLAN_SET_MEMBER_VLAN(au1PortArray, u2Port) \
           {\
              UINT2 u2PortBytePos = 0;\
              UINT2 u2PortBitPos = 0;\
              u2PortBytePos = (UINT2)(u2Port / VLAN_PORTS_PER_BYTE);\
              u2PortBitPos  = (UINT2)(u2Port % VLAN_PORTS_PER_BYTE);\
       if (u2PortBitPos  == 0) {u2PortBytePos --;} \
           \
               if (u2PortBytePos < VLAN_LIST_SIZE) { \
              au1PortArray[u2PortBytePos] = (UINT1)(au1PortArray[u2PortBytePos] \
                             | gau1PortBitMaskMap[u2PortBitPos]);\
           }}

/*
 * The macro VLAN_RESET_MEMBER_VLAN (), removes u2Port from the member
 * list of au1PortArray.
 * Warning!!! - Do not call the macro with u2Port as 0 or Invalid Port.
 */
#define VLAN_RESET_MEMBER_VLAN(au1PortArray, u2Port) \
              {\
                 UINT2 u2PortBytePos = 0;\
                 UINT2 u2PortBitPos = 0;\
                 u2PortBytePos = (UINT2)(u2Port / VLAN_PORTS_PER_BYTE);\
                 u2PortBitPos  = (UINT2)(u2Port % VLAN_PORTS_PER_BYTE);\
   if (u2PortBitPos  == 0) {u2PortBytePos --;} \
                 \
                  if (u2PortBytePos < VLAN_LIST_SIZE) { \
                 au1PortArray[u2PortBytePos] = (UINT1)(au1PortArray[u2PortBytePos] \
                               & ~gau1PortBitMaskMap[u2PortBitPos]);\
              }}

/* Adds the vlan list au1List2 to au1List1 - ie adds members of
 * au1List2 to au1List1*/
#define VLAN_ADD_VLAN_LIST(au1List1, au1List2) \
{\
    UINT2 u2ByteIndex = 0;\
    \
    for (u2ByteIndex = 0;\
            u2ByteIndex < VLAN_LIST_SIZE;\
            u2ByteIndex++) {\
        au1List1[u2ByteIndex] |= au1List2[u2ByteIndex];\
    }\
}

/* Removes members of au1List2 from au1List1 */
#define VLAN_RESET_PORT_LIST(au1List1, au1List2) \
              {\
         UINT2 u2ByteIndex;\
  \
  for (u2ByteIndex = 0;\
       u2ByteIndex < VLAN_PORT_LIST_SIZE;\
              u2ByteIndex++) {\
     au1List1[u2ByteIndex] &= (UINT1) (~au1List2[u2ByteIndex]);\
            }\
  }

#define VLAN_ARE_PORTS_EXCLUSIVE(au1List1, au1List2, u1Result) \
        {\
   UINT2 u2ByteIndex;\
   u1Result = VLAN_TRUE;\
   for (u2ByteIndex = 0;\
        u2ByteIndex < VLAN_PORT_LIST_SIZE;\
        u2ByteIndex++) {\
       if ((au1List1[u2ByteIndex] & au1List2[u2ByteIndex]) != 0) {\
   u1Result = VLAN_FALSE;\
   break;\
       }\
   }\
 }

#define VLAN_ARE_PORTS_INCLUSIVE(au1List1, au1List2, au1List3, u1Result) \
        {\
   UINT2 u2ByteIndex;\
   u1Result = VLAN_TRUE;\
   for (u2ByteIndex = 0;\
        u2ByteIndex < VLAN_PORT_LIST_SIZE;\
        u2ByteIndex++) {\
       if ((au1List1[u2ByteIndex] | au1List2[u2ByteIndex]) != au1List3[u2ByteIndex]) {\
   u1Result = VLAN_FALSE;\
   break;\
       }\
   }\
 }


/* 
 * Following macro checks if _au1List2 is a subset of _au1List1, i.e.,
 * all member ports of _au1List2 must be member ports of _au1List1
 */
#define VLAN_IS_PORT_LIST_A_SUBSET(_au1List1, _au1List2, _u1Result) \
        {\
           UINT2 _u2ByteIndex;\
           \
           _u1Result = VLAN_TRUE;\
           for (_u2ByteIndex = 0;\
                _u2ByteIndex < VLAN_PORT_LIST_SIZE;\
                _u2ByteIndex++) \
           {\
              if ((~_au1List1[_u2ByteIndex] & _au1List2[_u2ByteIndex]) != 0) \
              {\
                 _u1Result = VLAN_FALSE;\
                 break;\
              }\
           }\
        }

#define VLAN_IS_BCASTADDR(pMacAddr) \
        ((MEMCMP(pMacAddr,gBcastAddress,ETHERNET_ADDR_SIZE) == 0) ? \
          VLAN_TRUE : VLAN_FALSE)

#define VLAN_IS_RESERVED_ADDR(pAddr) \
         (((MEMCMP(pAddr,gReservedAddress,(ETHERNET_ADDR_SIZE-1)) == 0) \
          &&(pAddr[5] <= gReservedAddress[5])) ? \
          VLAN_TRUE : VLAN_FALSE)
#define VLAN_IS_MCAST_RESERVED_ADDR(pAddr) \
   (((MEMCMP(pAddr,gMulticastAddress,(ETHERNET_ADDR_SIZE-1)) == 0) \
          && (pAddr[5] !=  gMulticastAddress[5]) ) ? \
          VLAN_TRUE : VLAN_FALSE)

#define VLAN_IS_VRRP_IPV6_MCAST_RESERVED_ADDR(pAddr) \
       ((MEMCMP(pAddr,gVlanVrrpIpv6MCastAddress, ETHERNET_ADDR_SIZE) == 0) \
             ? VLAN_TRUE : VLAN_FALSE)


#define VLAN_PORT_LIST_SIZE         CONTEXT_PORT_LIST_SIZE

#define DEFAULT_BVLAN_COMPONENT_ID     2

#define VLAN_FORWARD_ALL               1
#define VLAN_FORWARD_SPECIFIC          2

#define VLAN_ADD                       1
#define VLAN_CREATE                    1
#define VLAN_DELETE                    2
#define VLAN_UPDATE                    3
#define VLAN_DELETE_ALL                4


#define VLAN_DROP_UNKNOWN_MCAST         4
#define VLAN_CLEAR_UNKNOWN_MCAST        5  

#define VLAN_FORWARD                   1
#define VLAN_NO_FORWARD                2
#define VLAN_PB_CUST_BPDU              3
#define VLAN_DATA                      4

#define VLAN_TUNNEL_OVERRIDE           0

#define VLAN_API_ERR_HW_FAILURE            -1
#define VLAN_API_ERR_INVALID_PORTS         -2
#define VLAN_API_ERR_ENTRY_NOT_PRESENT     -3
#define VLAN_API_ERR_CONTEXT_INVALID       -4
#define VLAN_API_ERR_VLAN_NOT_ACTIVE       -5

#define VLAN_ALL_GROUPS                1
#define VLAN_UNREG_GROUPS              2

#define VLAN_INVALID_PORT              0
#define VLAN_DEF_RECVPORT              0

#define VLAN_MAX_CURR_ENTRIES          VLAN_DEV_MAX_NUM_VLAN

#define VLAN_BIT8                      0x80
/* 
 * Timer is not maintained on a per entry basis. Only one timer will be 
 * running to ageout the entries. This timer will fire on every quarter of
 * the ageout time (for VLAN_AGEING_SPLIT_INTERVAL = 4).
 * If the ageout time is 300 seconds, the timer will fire for every 
 * 75 (300/4) seconds.
 */
#if ! defined (NPAPI_WANTED) || defined (SW_LEARNING)
#define VLAN_AGEING_SPLIT_INTERVAL     4
#else
#define VLAN_AGEING_SPLIT_INTERVAL     1
#endif

#define VLAN_INDEP_LEARNING              1
#define VLAN_SHARED_LEARNING             2
#define VLAN_HYBRID_LEARNING             3
#define VLAN_INVALID_LEARNING            4

#define VLAN_WILD_CARD_ID                4095
#define VLAN_SHARED_DEF_FDBID            1

#define VLAN_FDB_OTHER              1
#define VLAN_FDB_INVALID            2
#define VLAN_FDB_LEARNT             3
#define VLAN_FDB_SELF               4
#define VLAN_FDB_MGMT               5

#define VLAN_OTHER                  1
#define VLAN_MGMT_INVALID           2
#define VLAN_PERMANENT              3
#define VLAN_DELETE_ON_RESET        4
#define VLAN_DELETE_ON_TIMEOUT      5

#define VLAN_ACTIVE           1
#define VLAN_NOT_IN_SERVICE   2
#define VLAN_NOT_READY        3
#define VLAN_CREATE_AND_GO    4
#define VLAN_CREATE_AND_WAIT  5
#define VLAN_DESTROY          6

/* M-cast B-cast option for Mac based VLAN */
#define VLAN_MCAST_USE                 1
#define VLAN_MCAST_DROP                2

/* ARP packets option for Subnet based VLAN */
#define VLAN_ARP_ALLOW                 1
#define VLAN_ARP_SUPPRESS              2

/* 
 * Following constants are used for VlanStatus of the VLAN Current table. 
 * Used for MIB view alone 
 */
#define VLAN_CURR_ENTRY_OTHER        1
#define VLAN_CURR_ENTRY_PERMANENT    2 
#define VLAN_CURR_ENTRY_DYNAMIC      3

#define VLAN_NO_OF_MSG_PER_POST          1024
#define VLAN_MAP_INFO_PER_POST       \
    (((VLAN_DEV_MAX_NUM_VLAN) < 250) ? VLAN_DEV_MAX_NUM_VLAN : 250)

#define VLAN_IS_VLAN_ENABLED VlanGetEnabledStatus

/* For maximum size protocol values */
#define VLAN_MAX_PROTO_SIZE            5
#define VLAN_MAX_PRIORITY      8

#define VLAN_MAX_PB_PORT_TYPES      8
#define VLAN_L2_PROTOCOLS           12

/*
 * The following macros are used for NPAPI's invocations.
 */
#define VLAN_NO_TUNNEL_PORT            1     /* For Customer Bridges */
#define VLAN_TUNNEL_INTERNAL           2     /* Provider bridge...Port is 
                                                connected to another Provider */
#define VLAN_TUNNEL_EXTERNAL           3     /* Provider Bridge - Port is 
                                                connected to customer edge */
#define VLAN_DST_MAC_OFFSET            0

#define VLAN_CUSTOMER_BRIDGE_MODE          1
#define VLAN_PROVIDER_BRIDGE_MODE          2
#define VLAN_PROVIDER_EDGE_BRIDGE_MODE     3
#define VLAN_PROVIDER_CORE_BRIDGE_MODE     4
#define VLAN_PBB_ICOMPONENT_BRIDGE_MODE    5
#define VLAN_PBB_BCOMPONENT_BRIDGE_MODE    6
#define VLAN_INVALID_BRIDGE_MODE           7


#define VLAN_INVALID_SERVICE_TYPE         0
#define VLAN_PORT_BASED_SERVICE           1
#define VLAN_STAG_BASED_SERVICE           2

#define VLAN_TAG_PID_LEN               4
#define VLAN_ID_MASK                   0x0FFF
#define VLAN_PRIORITY_MASK             0xE000
#define VLAN_DEI_MASK                  0x1000
#define VLAN_DOUBLE_TAGGED_CVLAN_SIZE  18
#define VLAN_PROTOCOL_ID               0x8100
#define VLAN_PROVIDER_PROTOCOL_ID      0x88a8
#define VLAN_QINQ_PROTOCOL_ID          0x9100
#define VLAN_TAG_OFFSET                12
#define VLAN_TAGGED_HEADER_SIZE        16
#define VLAN_TAG_PRIORITY_SHIFT        13
#define VLAN_TAG_PRIORITY_DEI_SHIFT    12
#define VLAN_TYPE_OR_LEN_SIZE          2    
#define VLAN_TAG_SIZE                  2
#define VLAN_TAG_VLANID_OFFSET         14
#define VLAN_PBB_TAG_PID_LEN           6
#define VLAN_PBB_I_TAGGED_HEADER_SIZE  30
#define VLAN_PROVIDER_BACKBONE_PROTOCOL_ID      0x88e7
#define VLAN_PBB_MAX_TAGGED_HEDER_SIZE     42
#define VLAN_ETHER_TYPE_LEN     2
#define VLAN_PBB_ITAG_LEN       4

#define VLAN_ACCESS_PORT          1
#define VLAN_TRUNK_PORT           2
#define VLAN_HYBRID_PORT          3
#define VLAN_HOST_PORT            4
#define VLAN_PROMISCOUS_PORT      5
#define VLAN_DEFAULT_PORT         VLAN_HYBRID_PORT

#define VLAN_ADMIT_ALL_FRAMES                               1
#define VLAN_ADMIT_ONLY_VLAN_TAGGED_FRAMES                  2
#define VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES 3 

#define STD_ADMIT_ALL_FRAMES                               1
#define STD_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES 2
#define STD_ADMIT_ONLY_VLAN_TAGGED_FRAMES                  3
#define STD_ADMIT_ONLY_UNTAGGED_FRAMES                     4

#define VLAN_SNMP_TRUE 1
#define VLAN_SNMP_FALSE 2

/* the following macros are used by the MRP/GARP module for 
 * message posting */
#define VLAN_PROP_VLAN_INFO_MSG      7
#define VLAN_PROP_MAC_INFO_MSG       8
#define VLAN_PROP_FWDALL_INFO_MSG    9
#define VLAN_PROP_FWDUNREG_INFO_MSG  10
#define VLAN_SET_VLAN_FORBID_MSG     11
#define VLAN_SET_MCAST_FORBID_MSG    12
#define VLAN_SET_FWDALL_FORBID_MSG   13
#define VLAN_SET_FWDUNREG_FORBID_MSG 14
#define VLAN_UPDT_OR_POST_MSG        15
#define VLAN_UPDATE_MAP_PORTS_MSG    16

/* Frame type values as per the standard */
#define VLAN_PORT_PROTO_ETHERTYPE   1
#define VLAN_PORT_PROTO_RFC1042     2
#define VLAN_PORT_PROTO_SNAP8021H   3
#define VLAN_PORT_PROTO_SNAPOTHER   4
#define VLAN_PORT_PROTO_LLCOTHER    5

#define VLAN_IP_PROTO_ID              0x0800
#define VLAN_IPX_PROTO_ID             0x8137
#define VLAN_RAW_IPX_PROTO_ID         0xFFFF
#define VLAN_ARP_PROTO_ID             0x0806
#define VLAN_RARP_PROTO_ID            0x8035
#define VLAN_NETBIOS_PROTO_ID         0xF0F0
#define VLAN_APPLETALK_PROTO_ID       0x809B

/* Provider Bridge Port Types. */
#define VLAN_PROVIDER_NETWORK_PORT        CFA_PROVIDER_NETWORK_PORT
#define VLAN_CNP_PORTBASED_PORT           CFA_CNP_PORTBASED_PORT
#define VLAN_CNP_TAGGED_PORT              CFA_CNP_STAGGED_PORT
#define VLAN_CUSTOMER_EDGE_PORT           CFA_CUSTOMER_EDGE_PORT
#define VLAN_PROP_CUSTOMER_EDGE_PORT      CFA_PROP_CUSTOMER_EDGE_PORT
#define VLAN_PROP_CUSTOMER_NETWORK_PORT   CFA_PROP_CUSTOMER_NETWORK_PORT
#define VLAN_PROP_PROVIDER_NETWORK_PORT   CFA_PROP_PROVIDER_NETWORK_PORT
#define VLAN_CUSTOMER_BRIDGE_PORT         CFA_CUSTOMER_BRIDGE_PORT
#define VLAN_PROVIDER_EDGE_PORT           CFA_PROVIDER_EDGE_PORT
#define VLAN_PROVIDER_INSTANCE_PORT       CFA_PROVIDER_INSTANCE_PORT
#define VLAN_VIRTUAL_INSTANCE_PORT        CFA_VIRTUAL_INSTANCE_PORT
#define VLAN_CUSTOMER_BACKBONE_PORT       CFA_CUSTOMER_BACKBONE_PORT
#define VLAN_UPLINK_ACCESS_PORT           CFA_UPLINK_ACCESS_PORT
#define VLAN_INVALID_PROVIDER_PORT        CFA_INVALID_BRIDGE_PORT

/* VLAN Interface Types */
#define VLAN_C_INTERFACE_TYPE   1
#define VLAN_S_INTERFACE_TYPE   2
#define VLAN_INVALID_INTERFACE_TYPE  3

#define VLAN_UPDT_VLAN_PORTS           1
#define VLAN_UPDT_MCAST_PORTS          2

#define VLAN_ADDR_FMLY_IPV4            1
#define VLAN_ADDR_FMLY_IPV6            2
#define VLAN_ADDR_FMLY_ALL             3

#define VLAN_L2VPN_RAW_MODE          0x0005
#define VLAN_L2VPN_TAGGED_MODE       0x0004

#define VLAN_L2VPN_PORT_BASED        1
#define VLAN_L2VPN_VLAN_BASED        2
#define VLAN_L2VPN_PORT_VLAN_BASED   3

#define VLAN_L2VPN_VPWS             1
#define VLAN_L2VPN_VPLS             2


/* Macros Used in PVRST */
#define VLAN_PVRST_VLAN_NOTSTARTED_ERR    1
#define VLAN_PVRST_CXT_NOTPRESENT_ERR     2
#define VLAN_PVRST_HYBRID_PVID_ERR        3
#define VLAN_PVRST_HYBRID_UNTAG_ERR       4
#define VLAN_PVRST_HYBRID_VLAN_ERR        5
#define VLAN_PVRST_ACCESS_PVID_ERR        6
#define VLAN_PVRST_FORBIDDEN_PORT_ERR     7

#ifdef L2RED_WANTED /* L2RED_ISS_MERGE */
#define VLAN_GVRP_LRNT_PORT_LIST()   (gVlanRedInfo.GvrpLrntList)
#define VLAN_GMRP_LRNT_PORTS_TBL()   (gVlanRedInfo.GmrpLrntTable)
#endif /* L2RED_WANTED */

#define VLAN_NODE_STATUS() (gVlanNodeStatus)

#ifdef RM_WANTED
#define  VLAN_IS_NP_PROGRAMMING_ALLOWED() \
        ((L2RED_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE) ? VLAN_TRUE: VLAN_FALSE)
#else
#define  VLAN_IS_NP_PROGRAMMING_ALLOWED() VLAN_TRUE
#endif

/* used in web & vlan */
#define VLAN_HIGHEST_PRIORITY  7 

#define VLAN_LOCK()       VlanLock ()
#define VLAN_UNLOCK()     VlanUnLock ()
#define VLAN_GET_AGEOUT_TIME() VlanGetAgeOutTime() 

/* The MACROS have been exported Since
 * They are required by WEB Module
 * */
#define VLAN_CAPABILITIES_MASK_LENGTH 1
#define VLAN_PORT_CAPABILITIES_MASK 0xE0
#define VLAN_CAPABILITIES_MASK      0xF2
#define VLAN_PORT_ALL_CAPABILITIES_MASK 0xFF

/* Macros for VLAN device capabilitie */
#define VLAN_EXTEND_FILTERING_CAPABILITY_MASK 0x80
#define VLAN_TRAFFIC_CLASSES_CAPABILITY_MASK 0x40
#define VLAN_STATIC_INDIVIDUAL_PORT_CAPABILITY_MASK 0x20
#define VLAN_IVL_CAPABILITY_MASK 0x10
#define VLAN_SVL_CAPABILITY_MASK 0x08
#define VLAN_HYBRID_CAPABILITY_MASK 0x04
#define VLAN_CONFIG_PVID_TAGGING_CAPABILITY_MASK 0x02
#define VLAN_LOCAL_VLAN_CAPABILITY_MASK 0x01

/* Used by ASTP module for priority of VIPs */
#define VLAN_DEF_TUNNEL_BPDU_PRIORITY       VLAN_HIGHEST_PRIORITY

/* To identify remote FDB entry in FDB table, which is used in MC-LAG nodes */
enum
{
VLAN_LOCAL_FDB = 0,
VLAN_REMOTE_FDB
};

enum {
   VLAN_TUNNEL_PROTOCOL_PEER=1,
   VLAN_TUNNEL_PROTOCOL_TUNNEL,
   VLAN_TUNNEL_PROTOCOL_DISCARD,
   VLAN_TUNNEL_PROTOCOL_INVALID
};

enum {
    VLAN_HW_UPDATED_CPU_LEARNING=1,
    VLAN_HW_INDICATED_CPU_PROGRAMMING,
    VLAN_CPU_DIRECTED_LEARNING,
    VLAN_HW_LEARNING_TYPE_INVALID
};

/*
 * This structure is used to store the Protocol and frame types mapping
 * in Port Protocol Group database. This structure will also be accessed
 * by NPAPI routines.
 */
typedef struct _tVlanProtoTemplate{
    UINT1 u1Length;                           /* Indicates the valid no      */
                                              /* of bytes in au1ProtoValue   */
    UINT1 au1ProtoValue[VLAN_MAX_PROTO_SIZE]; /* Protocol value to be mapped */ 
    UINT1 u1TemplateProtoFrameType;  
    UINT1 u1Reserved; 
} tVlanProtoTemplate;

/* EVB Module interaction structure */
typedef struct _tVlanEvbSbpArray {
    UINT4   au4SbpArray[VLAN_EVB_MAX_SBP_PER_UAP];
} tVlanEvbSbpArray;

/* EVB Hardware Information */
typedef struct _VlanEvbHwConfigInfo
{
    UINT4   u4ContextId;     /* Context Identifier */
    UINT4   u4VpHwIfIndex;   /* HW IfIndex returned*/
    UINT4   u4UapIfIndex;   /* UAP interface index */
    UINT4   u4SChIfIndex;   /* S-Channel interface index */
    UINT4   u4SChId;        /* S-Channel Identifier */
    INT4    i4RxFilterEntry;
    UINT2   u2SVId;         /* SVLAN identifier*/
    UINT2   u2Pvid;            /* PVID setting */
    UINT1   u1OpCode;       /* Create/ delete */
    UINT1   u1AccFrameType;  /* Acceptable frame type*/
    UINT2   u2Reserved;   /* Padding */
}tVlanEvbHwConfigInfo;
/* UAP Port Type hardware Information */
typedef struct _VlanHwPortInfo
{         
    UINT4   u4ContextId;     /* Context Identifier */
    UINT4   u4IfIndex;       /* Physical port */
    UINT1   u1BrgPortType;   /* Bridge port type */
    UINT1   au1Reserved[7];  /* 64-bitPadding */
}tVlanHwPortInfo;


typedef struct portPktReflectionEntry
{
    UINT4 u4ContextId;
    UINT4 u4IfIndex;
    UINT4 u4Status;
}tFsNpVlanPortReflectEntry;

/* Used by the NPAPI routines */

/*Enum used for Provider Bridging*/
typedef enum {
    VLAN_SVLAN_PORT_SRCMAC_TYPE = 1,
    VLAN_SVLAN_PORT_DSTMAC_TYPE,
    VLAN_SVLAN_PORT_CVLAN_SRCMAC_TYPE,
    VLAN_SVLAN_PORT_CVLAN_DSTMAC_TYPE,
    VLAN_SVLAN_PORT_DSCP_TYPE,
    VLAN_SVLAN_PORT_CVLAN_DSCP_TYPE,
    VLAN_SVLAN_PORT_SRCIP_TYPE,
    VLAN_SVLAN_PORT_DSTIP_TYPE,
    VLAN_SVLAN_PORT_SRCDSTIP_TYPE,
    VLAN_SVLAN_PORT_CVLAN_DSTIP_TYPE,
    VLAN_SVLAN_PORT_CVLAN_TYPE,
    VLAN_SVLAN_PORT_PVID_TYPE,
    VLAN_PB_LOGICAL_PORT_ENTRY_TYPE,
    VLAN_PB_PORT_ENTRY_TYPE
}tVlanSVlanTableType;


typedef struct _tVlanTagInfo {
   UINT2 u2VlanId;
   UINT1 u1TagType;      /* VLAN_TAGGED / VLAN_UNTAGGED / 
                          * VLAN_PRIORITY_TAGGED */
   UINT1 u1Priority;     /* The priority associated with the incoming frame */
   UINT1 u1DropEligible; /* Drop eligible value associated with the incoming frame */
   UINT1 au1Pad[3];
}tVlanTagInfo;

typedef struct _tVlanTag {
   tVlanTagInfo  OuterVlanTag;
   tVlanTagInfo  InnerVlanTag;
} tVlanTag;

/* Bit map to store the ids of vlans supported by the device */
#define   VLAN_DEV_VLAN_LIST_SIZE  ((VLAN_DEV_MAX_VLAN_ID % 8) ?\
                            ((VLAN_DEV_MAX_VLAN_ID / 8) + 1) : \
                            (VLAN_DEV_MAX_VLAN_ID / 8)) 

typedef UINT2 tVlanId;

typedef UINT1 tVlanList[VLAN_DEV_VLAN_LIST_SIZE];

/* The following VLAN_DEV_VLAN_LIST_SIZE_EXT and tVlanListExt
 *  * should be used only in VPLS supported modules. These are
 *   * inclusive of additional VFI count. Whereas the macros
 *    * without *EXT notation are exclusive of VFI count.
 *     */
#define   VLAN_DEV_VLAN_LIST_SIZE_EXT  ((VLAN_DEV_MAX_VLAN_ID_EXT % 8) ?\
                            ((VLAN_DEV_MAX_VLAN_ID_EXT / 8) + 1) : \
                            (VLAN_DEV_MAX_VLAN_ID_EXT / 8))

typedef UINT1 tVlanListExt[VLAN_DEV_VLAN_LIST_SIZE_EXT];

typedef struct CVlanInfo {
    tVlanListExt  TaggedCVlanList; /* Customer VLANs on which tagged packet has to be sent */
    tVlanListExt  UntaggedCVlanList; /* Customer VLANs on which untagged packet has to be sent */
    UINT1      u1IsCEPPort; /* Indicates whether the requested port is CEP port */
    UINT1      au1Reserved[3];
}tCVlanInfo;


typedef struct VlanFwdInfo {
    UINT4     u4ContextId;  /* Context Id */
    UINT4     u4InPort;     /* Physical Port Index - IfIndex */
    VOID      *pPortList;  /* Physical Port Bitmap - IfIndex port bit map to which packet has to be transmitted*/
    tVlanTag   VlanTag;  /* VlanTag Information of the packet */
    tMacAddr  DestAddr;    /* Destination Mac Address of the packet */
    UINT1     u1Action;    /* VLAN_FORWARD_ALL - Forward to all the ports of the VLAN
                              VLAN_FORWARD_SPECIFIC - Forward to the specific pPortList passed in this structure.*/
    UINT1     u1SendOnAllCVlans;    /* OSIX_TRUE/OSIX_FALSE - Indicates to send the packet on all the cvlans of the CVID
                                      registration table.*/
    UINT1     u1OverrideSrcMac; /*OSIX_TRUE/OSIX_FALSE - Indicates the API to fill the srcmac for the packet or not */
    UINT1     u1IsPortListLocal; /*OSIX_TRUE/OSIX_FALSE - Indicates whether the portlist is local or physical */
    UINT1     u1FrameType; /*VLAN_DATA_FRAME -data frame
                            VLAN_CNTL_FRAME - control frame
                            VLAN_MCAST_FRAME - multicast frame
                            VLAN_CFM_FRAME - cfm frame */
    UINT1     au1Pad[1];
}tVlanFwdInfo;


/* Used in VLAN NPAPI and CFA NPAPI, fsvlan.h has to be included in CFA NPAPI 
 * file.
 * */
typedef struct HwPortArray {
        UINT4  *pu4PortArray;
        INT4   i4Length;
} tHwPortArray;

typedef struct _tHwUnicastMacEntry {
#ifdef MPLS_WANTED
        UINT4              u4PwVcIndex;
#endif
        UINT4              u4Port;
        tMacAddr           ConnectionId;
        UINT1              u1EntryType;
        UINT1              u1HitStatus;
} tHwUnicastMacEntry;

typedef struct _tHwVlanEntry {
    tPortList       *pHwMemberPbmp;
    tPortList       *pHwUntagPbmp;
    tPortList       *pHwFwdAllPbmp;   /* Only if supported */
    tPortList       *pHwFwdUnregPbmp; /* Only if supported */ 
}tHwVlanEntry;

typedef struct _tHwVlanPortList {
    tLocalPortList      HwMemberPortList;
    tLocalPortList      HwUntagPortList;
    tLocalPortList      HwFwdAllPortList;   /* Only if supported */
    tLocalPortList      HwFwdUnregPortList; /* Only if supported */
}tHwVlanPortList;



typedef struct _tHwVlanPortArray {
    tHwPortArray      *pHwMemberPortArray;
    tHwPortArray      *pHwUntagPortArray;
    tHwPortArray      *pHwFwdAllPortArray;   /* Only if supported */
    tHwPortArray      *pHwFwdUnregPortArray; /* Only if supported */
}tHwVlanPortArray;

typedef struct _tVlanSVlanMap {
#ifdef MEF_WANTED
    tHwEvcInfo HwEvcInfo;
#endif
    tMacAddr   SrcMac;
    tMacAddr   DstMac;
    UINT4      u4Dscp;
    UINT4      u4SrcIp;
    UINT4      u4DstIp;
    UINT4      u4TableType;
    tVlanId    SVlanId;
    tVlanId    CVlanId;
    UINT2      u2Port;
    UINT2      u2Reserved;
    UINT1      u1PepUntag ;
    UINT1      u1CepUntag ;
    UINT1      u1SVlanPriorityType;
    UINT1      u1SVlanPriority;
}tVlanSVlanMap;

typedef struct HwVlanPbPcpInfo {
   UINT2 u2PcpSelRow;
   UINT2 u2Priority;
   UINT2 u2PcpValue;
   UINT1 u1DropEligible;
   UINT1 au1Pad[1];
}tHwVlanPbPcpInfo;


typedef struct _HwVlanCVlanStat {
    tVlanId    CVlanId;
    UINT2      u2Port;
    UINT1      u1Status;
    UINT1      au1Pad[3];

}tHwVlanCVlanStat;



typedef struct HwVlanPortProperty {
    UINT2      u2OpCode;
    UINT2      u2AllowableTPID1;
    UINT2      u2AllowableTPID2;
    UINT2      u2AllowableTPID3;
    UINT2      u2Flag;
    UINT2      u2EgressEtherType;
    tVlanId    VlanId;
    UINT1      u1SVlanPriorityType;
    UINT1      u1SVlanPriority;
    UINT1      u1EgressTPIDType;
    UINT1      u1UntagFrameOnCEP;
    UINT1      au1Pad[2];
}tHwVlanPortProperty;

typedef enum {
   VLAN_NP_STP_PROTO_ID=1,
   VLAN_NP_GVRP_PROTO_ID,
   VLAN_NP_GMRP_PROTO_ID,
   VLAN_NP_DOT1X_PROTO_ID,
   VLAN_NP_LACP_PROTO_ID,
   VLAN_NP_IGMP_PROTO_ID,
   VLAN_NP_MVRP_PROTO_ID,
   VLAN_NP_MMRP_PROTO_ID,
   VLAN_NP_ELMI_PROTO_ID,
   VLAN_NP_LLDP_PROTO_ID,
   VLAN_NP_ECFM_PROTO_ID,
   VLAN_NP_EOAM_PROTO_ID,
   VLAN_MAX_PROT_ID
}tVlanHwTunnelFilters;

#define  VLAN_INVALID_PROTO_ID        0

typedef struct NpPbVlanPepInfo {
   INT4       i4DefUserPri;
   tVlanId    Cpvid;
   UINT1      u1AccptFrameType; 
   UINT1      u1IngFiltering;
}tHwVlanPbPepInfo;

/* PBB NPAPI Specific Structures */

typedef tHwVlanPbPcpInfo tHwPbbPcpInfo;

typedef struct _tVipAttribute {
    UINT4        u4Isid;
    tMacAddr     DefDestMacAddress;
    tMacAddr     BSIGMacAddress;
    UINT1        u1ServiceType;
    UINT1        au1Reserved[3];
} tVipAttribute;

typedef struct _tVipPipMap {
    UINT4        u4PipIfIndex;
    UINT4        u4Isid;
    UINT1        u1IsIsidValid;
    UINT1        au1Reserved[3];
} tVipPipMap;

typedef struct _tBackboneServiceInstEntry {
    UINT4        u4LocalSid;
    tMacAddr     DefDestMac;
    tMacAddr     BSIGMacAddress;
    tVlanId      BVid;
    UINT1        u1ServiceType;
    UINT1        au1Reserved[1]; 
} tBackboneServiceInstEntry;

typedef struct _tFilterEntry{
    INT4              i4IsidOffset;
    INT4              i4EtherTypeOffset;
    INT4              i4MacOffset;
    UINT4             u4Isid; /* Service Instance Id */
    tMacAddr          DstMacAddress; /* Mac Address */
    UINT2             u2EtherType; /* Ethertype */
    UINT1             u1Action; /* CopyToCpu / DoNotSwitch / Drop*/
    UINT1             u1Reserved[3];
}tFilterEntry;

#ifdef NPAPI_WANTED
typedef  tFilterEntry tPbbCtrlPktFilterEntry ;
#endif
/* Enums for PB and VLAN Port Properties combined */
enum{
    PB_PORT_PROPERTY_SVLAN_PRIORITY_TYPE=1,
    PB_PORT_PROPERTY_SVLAN_PRIORITY,
    VLAN_PORT_PROPERTY_CONF_ALLOWABLE_TPID1,
    VLAN_PORT_PROPERTY_CONF_ALLOWABLE_TPID2,
    VLAN_PORT_PROPERTY_CONF_ALLOWABLE_TPID3,
    VLAN_PORT_PROPERTY_CONF_EGRESS_TPID_TYPE,
    VLAN_PORT_PROPERTY_CONF_VLAN_EGRESS_ETHER_TYPE,
    PB_PORT_PROPERTY_CEP_UNTAG_EGRESS_STATUS,
};

/* Represents the node status */

typedef UINT4 tVlanNodeStatus;
#define VLAN_NODE_IDLE                      RM_INIT
#define VLAN_NODE_ACTIVE                    RM_ACTIVE
#define VLAN_NODE_STANDBY                   RM_STANDBY
#define VLAN_NODE_SWITCHOVER_IN_PROGRESS    4

extern tVlanNodeStatus gVlanNodeStatus;

#define VLAN_SVLAN_PRIORITY_TYPE_NONE  0
#define VLAN_SVLAN_PRIORITY_TYPE_FIXED 1
#define VLAN_SVLAN_PRIORITY_TYPE_COPY  2

/* Bit map to store the ids of vlans supported by VLAN module */
#define   VLAN_LIST_SIZE  ((VLAN_MAX_VLAN_ID% 8) ?\
                           ((VLAN_MAX_VLAN_ID + 31)/32 * 4) : \
                           (VLAN_MAX_VLAN_ID/8))
#define   VLAN_LIST_SIZE_EXT ((VLAN_MAX_VLAN_ID_EXT% 8) ?\
                           ((VLAN_MAX_VLAN_ID_EXT/8) + 1) : \
                           (VLAN_MAX_VLAN_ID_EXT/8))
 
#define VLAN_IS_VLAN_ID_VALID(VlanId) \
               (((VlanId == 0) || (VlanId > VLAN_MAX_VLAN_ID)) ? VLAN_FALSE : VLAN_TRUE)

#define VLAN_IS_CUSTOMER_VLAN_ID_VALID(VlanId) \
              (((VlanId == 0) || (VlanId > VLAN_DEV_MAX_CUSTOMER_VLAN_ID)) ? VLAN_FALSE : VLAN_TRUE)


#define VLAN_IS_PRIORITY_TYPE_VALID(PriorityType) \
               (((PriorityType == VLAN_SVLAN_PRIORITY_TYPE_FIXED ) || \
                 (PriorityType == VLAN_SVLAN_PRIORITY_TYPE_COPY ) || \
                 (PriorityType == VLAN_SVLAN_PRIORITY_TYPE_NONE )) ? VLAN_TRUE : VLAN_FALSE)

#define  VLAN_IS_EXCEED_MAX_PORTS(au1List, len, bRetVal)\
{\
 UINT2 u2BitCount = 0;\
 UINT2 u2PrtCnt = 0;\
   for (u2BitCount = 0; u2BitCount < ((len) * 8) ; u2BitCount++) {\
    if ((au1List)[u2BitCount / 8] & (0x80 >> (u2BitCount % 8))) {\
      u2PrtCnt++;\
      if (u2PrtCnt > VLAN_MAX_PORTS) {\
         bRetVal = VLAN_TRUE;\
         break;\
      }\
    }\
   }\
}

#define VLAN_IS_PORT_LIST_EXCEED_MAX_PORTS(au1List, len, bRetVal)\
{\
    UINT2 u2Count = 0;\
 bRetVal = VLAN_FALSE;\
 for (u2Count = VLAN_MAX_PORTS+1; u2Count < ((len)*8); \
      u2Count++) {\
    if ((au1List)[u2Count / 8] & (0x80 >> (u2Count % 8))) {\
  bRetVal = VLAN_TRUE;\
      break;\
     }\
 }\
}
typedef struct _tVlanMapInfo {
    tVlanId        VlanId;
    UINT2          u2MapId;
} tVlanMapInfo;

typedef struct _VlanOutIfMsg {
   UINT2                u2Port;
   UINT2                u2Length;
   UINT4                u4TimeStamp;
   UINT1                u1FrameType;
   UINT1                au1Reserved[3]; /* Padding */
} tVlanOutIfMsg;

/***************************************************************************/
typedef struct VlanSrcRelearnTrap{
    tMacAddr MacAddr;
    UINT2    u2VlanId;
    UINT4    u4NewPort;
    UINT4    u4OldPort;
    UINT4    u4FidIndex;
}tVlanSrcRelearnTrap;
typedef struct ConfigStaticUnicastInfo {
        UINT4     u4ContextId;
        tVlanId   VlanId;
        tMacAddr  DestMac;
        UINT4     u4EgressIfIndex;
        UINT4     u4Status;  
} tConfigStUcastInfo;

typedef struct ConfigStaticMulticastInfo {
        UINT4     u4ContextId;
        tVlanId   VlanId;
        tMacAddr  DestMac;
        tPortList *pEgressIfIndexList;
        UINT4     u4Status;  
} tConfigStMcastInfo;

typedef struct tHwVlanFwdInfo
{
    tVlanTag      VlanTag;
    tHwPortArray  TagPorts;
    tHwPortArray  UnTagPorts;
    UINT4         u4ContextId;
}tHwVlanFwdInfo;

typedef struct VlanFlushInfo
{
    UINT1 au1FdbList[VLAN_LIST_SIZE_EXT]; /* List of Fdb's to be Flushed */
    UINT4 u4IfIndex;                  /* Interface Index */
    UINT2 u2FdbCount;                 /* Total No of Fdb's to be flushed */
    UINT2 u2InstanceId;               /* MST instance ID */
    INT2  i2OptimizeFlag;             /* Optimization Flag - VLAN_NO_OPTIMIZE,
                                         VLAN_OPTIMIZE */
    UINT1 u1Module;                   /* Module that is calling the flush */
    UINT1 au1Padding[1];
}tVlanFlushInfo;
typedef struct HwMcastIndexInfo
{
    tMacAddr MacAddr; /* Mac Address to be programmed */
    tVlanId  VlanId; /* VLAN Id to be programmed */
    UINT4    u4Opcode; /* Indicates whether it is a set call to generate Mcast 
                          Index (or) a get call to retrieve Mcast Index for
                          given VLAN and MAC */
    UINT4    u4McastIndex; /* Multicast Index to be programmed in Hardware. */
#ifdef EVB_WANTED
        UINT4            u4VlanMcastIndex;/* Multicast Index of type VLAN */
    UINT1    au1Pad[4];/* For 64-bit padding when EVB is enabled*/
#endif
}tHwMcastIndexInfo;

/*
 * Structure for holding Ethernet Statistics Per Vlan.
 */
typedef struct VlanEtherStats {

    UINT4           u4VlanStatsDropEvents; /* Total number of events in which packets
                                            were dropped by the probe due to lack of 
                                             resource . */
    UINT4           u4VlanStatsOctets; /* Total number of octets of data (including
                                          those in bad packets) received on the 
                                          network (excluding framing bits but including
                                          FCS octets)*/
    UINT4           u4VlanStatsPkts; /* Total number of packets (bad packets, 
                                        broadcast packets, and multicast packets)*/
    UINT4           u4VlanStatsBroadcastPkts; /* Total number of good packets 
                                                 received that were directed to 
                                                 the broadcast address.*/
    UINT4           u4VlanStatsMulticastPkts; /* Total number of good packets 
                                                 received that were directed to a 
                                                 multicast address.*/
    UINT4           u4VlanStatsCRCAlignErrors; /* Total number of packets received 
                                                  that had a length (excluding 
                                                  framing bits, but including FCS 
                                                  octets) of between 64 and 1518 octets, 
                                                  inclusive, but had either a bad Frame 
                                                  Check Sequence (FCS) with an integral 
                                                  number of octets (FCS Error) or a bad 
                                                  FCS with a non-integral number of octets 
                                                  (Alignment Error).*/
    UINT4           u4VlanStatsUndersizePkts; /* Total number of packets received that 
                                                 were less than 64 octets long (excluding 
                                                 framing bits, but including FCS octets) 
                                                 and were otherwise well formed.*/
    UINT4           u4VlanStatsOversizePkts; /* Total number of packets received that 
                                                were longer than 1518 octets (excluding 
                                                framing bits, but including FCS octets) 
                                                and were otherwise well formed.*/
    UINT4           u4VlanStatsFragments; /* Total number of packets received that were 
                                             less than 64 octets in length (excluding 
                                             framing bits but including FCS octets) 
                                             and had either a bad Frame Check Sequence
                                             (FCS) with an integral number of octets 
                                             (FCS Error) or a bad FCS with a non-integral 
                                             number of octets (Alignment Error).*/
    UINT4           u4VlanStatsJabbers; /*Total number of packets received that were
                                          longer than 1518 octets (excluding framing 
                                          bits, but including FCS octets), and had 
                                          either a bad Frame Check Sequence (FCS) 
                                          with an integral number of octets (FCS Error) 
                                          or a bad FCS with a non-integral
                                          number of octets (Alignment Error)*/
    UINT4           u4VlanStatsCollisions; /*Total number of collisions on this Ethernet 
                                             segment*/
    UINT4           u4VlanStatsPkts64Octets; /* Total number of packets (including bad
                                                packets) received that were 64 octets 
                                                in length (excluding framing bits but 
                                                including FCS octets).*/
    UINT4           u4VlanStatsPkts65to127Octets; /* Total number of packets (including 
                                                     bad packets) received that were 
                                                     between 65 and 127 octets in length 
                                                     inclusive (excluding framing bits 
                                                     but including FCS octets).*/
    UINT4           u4VlanStatsPkts128to255Octets; /* Total number of packets (including 
                                                      bad packets) received that were 
                                                      between 128 and 255 octets in 
                                                      length inclusive (excluding framing 
                                                      bits but including FCS octets).*/
    UINT4           u4VlanStatsPkts256to511Octets; /* Total number of packets 
                                                      (including bad packets) received 
                                                      that were between 256 and 511 octets 
                                                      in length inclusive (excluding 
                                                      framing bits but including FCS 
                                                      octets).*/
    UINT4           u4VlanStatsPkts512to1023Octets; /* Total number of packets (including 
                                                       bad packets) received that were 
                                                       between 512 and 1023 octets in 
                                                       length inclusive (excluding 
                                                       framing bits but including FCS 
                                                       octets).*/
    UINT4           u4VlanStatsPkts1024to1518Octets; /* Total number of packets 
                                                        (including bad packets) received 
                                                        that were between 1024 and 1518 
                                                        octets in length inclusive
                                                        (excluding framing bits
                                                         but including FCS octets).*/
    UINT4           u4VlanStatsFCS; /*Total of packets with frame check sequence (FCS) error*/
}tVlanEthStats;

#ifdef MBSM_WANTED
/* The Following Macro will determine how many entries will be synced in single
 * FsMiVlanMbsmSyncFDBInfo invocation
 */
#define MAX_FDB_INFO 10
typedef struct _tFDBInfo {
   UINT4                  u4ContextId;
   UINT4                  u4FdbId;
   UINT4                  u4Port;
   tMacAddr               MacAddr;
   UINT1                  u1EntryType;
   UINT1                  au1Pad[1];
} tFDBInfo;
typedef struct _tFDBInfoArray {
   tFDBInfo aFDBInfoArray[MAX_FDB_INFO];
   UINT4    u4FdbEntryCount;
} tFDBInfoArray;
#endif /* MBSM_WANTED */


/* This structure is used in VLAN call back structure tVlanRegTbl */
typedef struct _tVlanBasicInfo {
    tLocalPortList LocalPortList; /* Ports */
    UINT2  u2VlanId; /* VLAN identifier */
    UINT2       u2Port;     /* Local Port identifier for UPDATE_MAC */
    tMacAddr    MacAddr;    /* Mac address for UPDATE_MAC */
    UINT1  u1Action; /* Action: VLAN_CB_ADD_VLAN, VLAN_CB_DELETE_VLAN
                             * VLAN_CB_UPDATE_MAC, VLAN_CB_REMOVE_MAC */
    UINT1  u1EntryStatus; /* Entry Status: VLAN_FDB_OTHER, VLAN_FDB_INVALID
                             VLAN_FDB_LEARNT, VLAN_FDB_SELF, VLAN_FDB_MGMT */
    UINT1 u1OperStatus; /* Interface Oper Status */
    UINT1                  au1Pad[3];
}tVlanBasicInfo;

enum
{
   VLAN_CB_ADD_EGRESS_PORTS   = 1,
   VLAN_CB_DELETE_EGRESS_PORTS,
   VLAN_CB_ADD_UNTAGGED_PORTS,
   VLAN_CB_DELETE_UNTAGGED_PORTS,
   VLAN_CB_UPDATE_MAC,
   VLAN_CB_REMOVE_MAC,
   VLAN_CB__INVALID    /* CAUTION: Ensure this element shall be the last
                        * element always */

};
enum
{
   VLAN_INDICATION_REGISTER    = 1,
   VLAN_INDICATION_DEREGISTER,
   VLAN_INDICATION_INVALID /* CAUTION: Ensure this element shall be the last
                                * element always */

};

enum {
   VLAN_WSS_PROTO_ID = 0,
   VLAN_VXLAN_EVPN_PROTO_ID,
   VLAN_MAX_REG_MODULES  /* CAUTION: Ensure this element shall be the last 
                          * element always */
};

enum {
  VLAN_EVB_SCHANNEL_OPER_UP = 1 ,
  VLAN_EVB_SCHANNEL_OPER_DOWN
};

/***********  Providing registration mechanism from VLAN *********/
typedef struct VlanRegTbl {
    VOID (*pVlanBasicInfo) (tVlanBasicInfo *pVlanBasicInfo);
 /* Call back function that a module can register with VLAN */ 
    UINT1                  u1ProtoId;
    /* The protocol Id as defined the in enumeration */
    UINT1                  au1Reserved[3];
    /* Structure padding */
}tVlanRegTbl;

#ifdef ICCH_WANTED

#define MCAG_LOCK() McagLock ()
#define MCAG_UNLOCK() McagUnLock ()

VOID McagMain PROTO ((INT1 *pi1Param));
INT4 McagLock PROTO ((VOID));
INT4 McagUnLock PROTO ((VOID));

#endif

INT4
VlanRegDeRegProtocol PROTO ((UINT4 u4Type, tVlanRegTbl *pVlanRegTbl));

INT4 
VlanStartModule (VOID);
INT4
VlanShutdownModule (VOID);
INT4
GarpStartModule (VOID);
INT4 
VlanGetStartedStatus            PROTO ((UINT4 u4ContextId));

INT4
VlanGetEnabledStatus            PROTO (( VOID ));

VOID 
VlanMain                        PROTO ((INT1 *pi1Param));

INT4
VlanCreatePort PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2Port));

INT4
VlanDeletePort                  PROTO ((UINT4 u4Port));

INT4
VlanUpdateIntfType PROTO ((UINT4 u4ContextId, UINT1 u1IntfType));

INT4
VlanDeletePortAndCopyProperties PROTO ((UINT4 u4IfIndex, UINT4 u4PoIndex));

INT4
VlanPortOperInd                 PROTO ((UINT4 u4Port, UINT1 u1OperStatus));

INT4
VlanProcessPacket               PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf, 
                                        UINT4 u4Port));
INT4 
VlanLock                        PROTO ((VOID));

INT4 
VlanUnLock                      PROTO ((VOID));

INT4 
VlanDeleteFdbEntries            PROTO ((UINT4 u4PortNum, INT4 i4OptimizeFlag));

INT4
VlanFlushFdbEntries             PROTO ((UINT4 u4Port, UINT4 u4Fid, 
                                        INT4 i4OptimizeFlag));

INT4
VlanSetForwardUnregPortsFromIgs (UINT4 VlanIndex ,tPortList *pStaticPorts);      
INT4
VlanSetForwardUnregPorts (UINT4 VlanIndex, UINT1 *pu1PortList);
INT4
VlanFlushFdbTable (tVlanFlushInfo *pVlanFlushInfo);

INT4
VlanFlushFdbEntriesOnPort       PROTO ((UINT4 u4Port, UINT4 u4FdbId, 
                                        INT4 u1ModId, INT4 i4OptimizeFlag));
INT4
VlanMiFlushFdbId                  PROTO ((UINT4 u4ContextId, UINT4 u4FdbId));

/* MI_P2*/
UINT1
VlanMiGetVlanLearningMode (UINT4 u4ContextId);

VOID 
VlanIvrGetVlanIfOperStatus      PROTO ((UINT4 u4IfIndex, UINT1 *pu1OperStatus));

INT4
VlanIvrGetTxPortOrPortList PROTO ((tMacAddr DestAddr, tVlanId VlanId,
                                        UINT1 u1BcastFlag, UINT4 *pu4OutPort,
                                        BOOL1  *pbIsTag, tPortList TagPortList,
                                        tPortList UntagPortList));


INT4
 VlanCheckPortType(UINT2 u2VlanId,UINT2 u2Port);
INT4
VlanIvrGetTxPortOrPortListInCxt PROTO ((UINT4 u4L2ContextId,
                                        tMacAddr DestAddr, tVlanId VlanId,
                                        UINT1 u1BcastFlag, UINT4 *pu4OutPort,
                                        BOOL1  *pbIsTag, tPortList TagPortList,
                                        tPortList UntagPortList));

VOID 
VlanUnTagFrame                  PROTO ((tCRU_BUF_CHAIN_DESC *pFrame));

INT4 
VlanIsVlanDynamic PROTO ((UINT4 u4ContextId, UINT2 VlanId));
INT4
VlanMiUpdateDynamicUcastInfo PROTO ((UINT4 u4ContextId, 
                                     tMacAddr MacAddr,
                                     tVlanId VlanId, 
                                     UINT4 u4Port, 
                                     UINT1 u1Action));

INT4 
VlanIdentifyVlanIdAndUntagFrame PROTO ((INT4 i4ModuleId, 
                                        tCRU_BUF_CHAIN_DESC *pFrame,
                                        UINT4 u4InPort, tVlanId *pVlanId));

INT4 
VlanIdentifyVlanId PROTO ((tCRU_BUF_CHAIN_DESC * pFrame, 
                           UINT4 u4InPort, tVlanId * pVlanId));

INT4 
VlanUnTagGvrpPacket PROTO ((tCRU_BUF_CHAIN_DESC * pFrame, UINT4 u4Port));


INT4 
VlanIsMvrpFrameTagged PROTO ((tCRU_BUF_CHAIN_DESC * pFrame, UINT4 u4Port, 
                              INT4 *pi4FrameType));

INT4 
VlanSetShortAgeoutTime          PROTO ((UINT4 u4PortNum,  INT4 i4AgingTime));

INT4 
VlanResetShortAgeoutTime        PROTO ((UINT4 u4PortNum));

INT4
VlanSnoopGetTxPortList PROTO ((tMacAddr DestAddr, UINT4 u4InPort,
                             tVlanId VlanId, UINT1 u1Action,
                            tPortList IgsPortList, tPortList TagPortList, 
                            tPortList UntagPortList));

INT4
VlanMiSnoopGetTxPortList PROTO ((UINT4 u4ContextId, tMacAddr DestAddr, UINT4 u4InPort,
                             tVlanId VlanId, UINT1 u1Action,
                            tPortList IgsPortList, tPortList TagPortList, 
                            tPortList UntagPortList));
INT4
VlanApiForwardOnPorts PROTO((tCRU_BUF_CHAIN_DESC *pBuf , tVlanFwdInfo *pVlanFwdInfo));

INT4
VlanGetFdbEntryDetails PROTO ((UINT4 u4FdbId, tMacAddr MacAddr,
                               UINT2 *pu2Port, UINT1 *pu1Status));

INT4
VlanCheckPortListPortTypeValid PROTO ((UINT1* pu1Ports, INT4 i4Len,
                                    UINT1 u1PortType));

INT4
VlanCheckAndTagOutgoingGmrpFrame PROTO ((UINT4 u4ContextId,
                                         tCRU_BUF_CHAIN_DESC *pBuf, 
                                         tVlanId              VlanId,
                                         UINT2                u2Port));

VOID
VlanGvrpEnableInd               PROTO ((UINT4 u4ContextId));

VOID
VlanGmrpEnableInd               PROTO ((UINT4 u4ContextId));

VOID
VlanMvrpEnableInd               PROTO ((UINT4 u4ContextId));

VOID
VlanMmrpEnableInd               PROTO ((UINT4 u4ContextId));

VOID
VlanMvrpPortEnableInd           PROTO ((UINT4 u4ContextId, UINT2 u2Port));

VOID
VlanMmrpPortEnableInd           PROTO ((UINT4 u4ContextId, UINT2 u2Port));

INT4 VlanGetBaseBridgeMode  PROTO ((VOID));

INT4
VlanUpdateDynamicVlanInfo       PROTO ((UINT4 u4ContextId, tVlanId VlanId, UINT2 u2Port,
                                        UINT1 u1Action));

VOID
VlanDeleteAllDynamicVlanInfo    PROTO ((UINT4 u4ContextId, UINT2 u2Port));

VOID
VlanMiDelDynamicMcastInfoForVlan (UINT4 u4ContextId, tVlanId VlanId);

VOID
VlanMiDeleteAllDynamicMcastInfo   PROTO ((UINT4 u4ContextId, UINT4 u4Port,
                                        UINT1 u1MacAddrType));

INT4
VlanMiUpdateDynamicMcastInfo      PROTO ((UINT4 u4ContextId, tMacAddr MacAddr,
                                          tVlanId VlanId, UINT4 u4Port,
                                          UINT1 u1Action));
INT4
VlanUpdateDynamicUcastInfo      PROTO ((UINT4 u4ContextId, tMacAddr MacAddr,
                                          tVlanId VlanId, UINT4 u4Port,
                                          UINT1 u1Action));

INT4
VlanIsVlanStaticAndRegNormal PROTO ((UINT4 u4ContextId, UINT2 VlanId, UINT2 u2Port));

INT4
VlanIsMcastStaticAndRegNormal PROTO ((UINT4 u4ContextId, tMacAddr MacAddr, UINT2 u2VlanId,
                                      UINT2 u2Port)); 

INT4 
VlanIsServiceAttrStaticAndRegNormal PROTO ((UINT4 u4ContextId, UINT1 u1Type, UINT2 u2VlanId, 
                                            UINT2 u2Port));

INT4
VlanCheckReservedGroupForFwding PROTO ((UINT4 u4ContextId,
                                        tMacAddr DestAddr));

INT4
VlanGetVlanMemberPorts PROTO ((tVlanId VlanId, tPortList EgressPortList,
                               tPortList UntagPortList));

VOID VlanGetDefaultVlanConfig PROTO ((UINT1 * pu1DefConf));

VOID
VlanDeleteAllDynamicDefGroupInfo PROTO ((UINT4 u4ContextId, UINT2 u2Port));

INT4
VlanUpdateDynamicDefGroupInfo   PROTO ((UINT4 u4ContextId, UINT1 u1Type, tVlanId VlanId, 
                                        UINT2 u2Port, UINT1 u1Action));

#ifdef NPAPI_WANTED 
INT4
VlanCopyPortPropertiesToHw PROTO ((UINT4 u4DstPort, UINT4 u4SrcPort, UINT4 u4ContextId));

INT4 
VlanRemovePortPropertiesFromHw PROTO ((UINT4 u4DstPort, UINT4 u4SrcPort));

INT4 
VlanCopyPortMcastPropertiesToHw PROTO ((UINT4 u4Port));

INT4 
VlanRemovePortMcastPropertiesFromHw PROTO ((UINT4 u4Port));

INT4 
VlanCopyPortUcastPropertiesToHw PROTO ((UINT4 u4Port));

INT4 
VlanRemovePortUcastPropertiesFromHw PROTO ((UINT4 u4Port));

VOID VlanMiDeleteAllFdbEntries (UINT4 u4ContextId);

#ifdef SW_LEARNING
VOID 
VlanFdbTableAdd PROTO ((UINT4 u4Port, tMacAddr MacAddr, tVlanId VlanId, 
                        UINT1 u1EntryType, UINT4 u4PwIndex));
VOID
VlanFdbTableAddEx PROTO ((UINT4 u4IfIndex, tMacAddr MacAddr, tVlanId VlanId,
                          UINT1 u1EntryType, tMacAddr ConnectionId));
VOID 
VlanFdbTableRemove PROTO ((tVlanId VlanId, tMacAddr MacAddr));

VOID
VlanAddFdbTable PROTO ((UINT4 u4IfIndex, tMacAddr MacAddr, tVlanId VlanId,
                 UINT1 u1EntryType));

VOID
VlanAddExFdbTable PROTO ((UINT4 u4IfIndex, tMacAddr MacAddr, tVlanId VlanId,
                   UINT1 u1EntryType, tMacAddr ConnectionId));

#ifdef MBSM_WANTED
UINT1
VlanAddFDBInfo PROTO ((tFDBInfo * pFDBInfo));
#endif /* MBSM_WANTED */
#endif /* SW_LEARNING */
#endif /* NPAPI_WANTED */

VOID
VlanHwAddFdbEntry (UINT4 u4IfIndex, tMacAddr MacAddr, tVlanId VlanId,
                UINT1 u1EntryType);
VOID
VlanHwDelFdbEntry (UINT4 u4IfIndex, tMacAddr MacAddr, tVlanId VlanId);

INT4
VlanSetMcastEthMode PROTO ((UINT4 u4ContextId, UINT1 u1McastEthMode));

INT1
nmhValidateIndexInstanceDot1dPortPriorityTable PROTO ((INT4 i4Dot1dBasePort));

INT1
nmhGetFirstIndexDot1qPortVlanTable PROTO ((INT4 *pi4Dot1dBasePort));

INT1
nmhGetNextIndexDot1qPortVlanTable PROTO ((INT4 i4Dot1dBasePort,
                                          INT4 *pi4NextDot1dBasePort));

#ifdef MBSM_WANTED
INT4 VlanMbsmPostMessage (tMbsmProtoMsg * pProtoMsg, INT4 i4Event);
#endif

VOID VlanGvrpDisableInd PROTO ((UINT4));
VOID VlanGmrpDisableInd PROTO ((UINT4));
tVlanNodeStatus VlanGetNodeStatus PROTO ((VOID));
INT4 VlanRestartModule (VOID);
#ifdef L2RED_WANTED /* L2RED_ISS_MERGE */
INT4 VlanRedHandleUpdateEvents PROTO ((UINT1 u1Event, tRmMsg *pData, 
                                UINT2 u2DataLen));
INT4 VlanRedSendDataAppliedMsg PROTO ((VOID));
VOID VlanRedStartMcastAudit PROTO ((VOID));
#endif /* L2RED_WANTED */


UINT4 VlanGetAgeOutTime PROTO ((VOID )) ;
VOID VlanRegisterSTDBRI PROTO ((VOID));

VOID VlanStartHwAgeTimer (UINT4 u4ContextId, UINT4 u4AgeOut);
VOID VlanIncrFilterInDiscards (UINT2 u2Port);

INT4
VlanIsVlanEnabledInContext (UINT4 u4ContextId);

UINT1 VlanGetDefPortType (UINT4 u4IfIndex);

INT4
VlanCreateContext (UINT4 u4ContextId);

INT4 
VlanDeleteContext (UINT4 u4ContextId);

INT4 
VlanMapPort (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2IfIndex);

INT4 
VlanUnmapPort (UINT4 u4IfIndex);

INT4 
VlanUpdateContextName (UINT4 u4ContextId);

VOID VlanPbNotifyCepOperStatusForPep (UINT2 u2Port, UINT1 u1OperStatus);

INT4 VlanPbIsCreateIvrValid (UINT2 u2IfIvrVlanId);


INT4 VlanGetTagLenInFrame (tCRU_BUF_CHAIN_DESC * pFrame, UINT4 u4IfIndex,
                           UINT4 *pu4VlanOffset); 
INT4
VlanCheckIsTunnelEnabledForOtherMac (UINT4 u4IfIndex, tCRU_BUF_CHAIN_DESC * pFrame);

INT4
VlanTagOutFrameForIvr (UINT1 *pBuf, UINT4 u4IfIndex,
                       tVlanId VlanId, UINT1 u1Priority);

INT4
VlanPbSendCustBpduOnPep (tCRU_BUF_CHAIN_DESC * pFrame, UINT4 u4CepIfIndex,
                         tVlanId SVlanId);

VOID
VlanTagFrame (tCRU_BUF_CHAIN_DESC * pFrame, tVlanId VlanId, UINT1 u1Priority);

VOID VlanVplsLearn (UINT4 u4ContextId, tMacAddr MacAddr, tVlanId VlanId,
                    UINT4 u4PwVcId);

INT4
VlanGetTunnelProtocolMac PROTO ((UINT4 u4ContextId, UINT1 u1ProtocolId,
                          tMacAddr MacAddr));

INT4
VlanValidatePortType PROTO ((UINT4 u4IfIndex, UINT1 u1BrgPortType));

INT4
VlanValidateAndChangePortType PROTO ((UINT4 u4IfIndex,
                                      UINT1 u1BrgPortType,
                                      UINT1 u1IsSetPortType));

VOID
VlanCreateDefaultVlan PROTO ((INT1 *pi1Param));

VOID
VlanDeleteDefaultVlan PROTO ((VOID));

INT4    
VlanL2VpnFwdMplsPktOnPorts PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf, 
                                    UINT4 u4ContextId, tVlanId VlanId, 
                                    UINT1 u1Priority, UINT1 u1PwMode,  UINT4 u4OutPort));


INT4     
VlanL2VpnAddL2VpnInfo  PROTO ((tVplsInfo  *pVplsInfo));

INT4     
VlanL2VpnDelL2VpnInfo PROTO ((tVplsInfo  *pVplsInfo));


INT4 
VlanNpRetainTag (UINT4 u4IfIndex, tMacAddr DstAddr, UINT2 u2Protocol, 
                 INT4 *pi4RetVal);

INT4 VlanIncrRxDiscardCounters (UINT4 u4IfIndex, UINT2 u2ProtocolId);

INT4 VlanIncrRxDiscardCountersPerVlan (UINT4 u4ContextId, tVlanId VlanId, UINT2 u2ProtocolId);

INT4 VlanIncrRxTunnelCountersPerVlan (UINT4 u4ContextId, tVlanId VlanId, UINT2 u2ProtocolId);

INT4 VlanIncrTxTunnelCountersPerVlan (UINT4 u4ContextId, tVlanId VlanId, UINT2 u2ProtocolId);

INT4
VlanRemoveFdbEntry(UINT4 u4ContextId, tVlanId VlanId, tMacAddr pMacAddress);

INT4
VlanRemoveAllFdbEntry (UINT4 u4ContextId);

INT4
VlanIsMemberPortForCfm (UINT4 u4ContextId, UINT2 u2Port,tVlanTag VlanTagInfo);


INT4
VlanAllFdbEntryRemove PROTO ((UINT4 u4ContextId));

INT4
VlanFdbEntryRemove PROTO ((UINT4 u4ContextId, tVlanId VlanId, tMacAddr pMacAddress));

INT4
VlanTransmitCfmFrame (tCRU_BUF_CHAIN_DESC * pBuf, UINT4 u4ContextId,
                      UINT2 u2Port, tVlanTag VlanTagInfo, UINT1 u1FrameType);

INT4
VlanGetVlanInfoFromFrame(UINT4 u4ContextId, UINT2 u2LocalPort,
                         tCRU_BUF_CHAIN_DESC *pBuf, tVlanTag *pVlanTag,
                         UINT1 *pu1IngressAction, UINT4 *pu4TagOffSet);
INT4
VlanIsEcfmTunnelPkt PROTO ((UINT4, tMacAddr ));

INT4
VlanProcessPktForCep PROTO ((UINT4, UINT2, tCRU_BUF_CHAIN_DESC *, tVlanTag *));

INT4
VlanGetFwdPortList (UINT4 u4ContextId, UINT2 u2LocalPort, tMacAddr SrcMacAddr, 
                    tMacAddr DstMacAddr, tVlanId VlanId, tPortList FwdPortList);

INT4
VlanConfigCheckForPvrst (UINT4 u4ContextId, UINT4 *pu4ErrorCode);

INT4
VlanConfigUpdtForPvrstStart (UINT4 u4ContextId);

INT4
VlanConfigUpdtForPvrstShutDown (UINT4 u4ContextId);

INT4 VlanConfigDot1qPvidForPvrstStart (UINT2 u2Port,tVlanId VlanId);

INT4
VlanSendSrcRelearnTrapAndSysLog (tVlanSrcRelearnTrap *pVlanSrcRelearnTrap);
INT4
VlanPortOperPointToPointUpdate (UINT4 u4IfIndex);
INT4
VlanPortStpStateUpdate (UINT4 u4IfIndex);
INT4
VlanIsMcastWildCardEntryPresent (UINT4 u4ContextId, UINT2 u2VlanId, tMacAddr MacAddr);
INT4
VlanIsUcastWildCardEntryPresent (UINT4 u4ContextId, UINT2 u2VlanId, tMacAddr MacAddr);

INT4 VlanGetPortEtherType (UINT4 u4IfIndex, UINT2 *pu2EtherType);

INT4 VlanGetIngressPortEtherType (UINT4 u4IfIndex, UINT2 *pu2EtherType);

INT4
VlanIsProtocolVlanSupported (VOID);

INT4
VlanGetProtoVlanStatusOnPort (UINT4 u4IfIndex, UINT1 *pu1ProtoVlanStatus);

INT4
VlanGetCVlanInfoOnCustomerPort PROTO ((UINT4 u4IfIndex, tVlanId SVlanId,
                                     tCVlanInfo *pCVlanInfo));

INT4
VlanGetProtoVlanOnPort (UINT4 u4IfIndex, UINT2 *apConfProtoVlans,
                        UINT1 *pu1NumVlan);

INT4
VlanAddOuterTagInFrame PROTO ((tCRU_BUF_CHAIN_DESC * pFrame, tVlanId VlanId, 
                               UINT1 u1Priority, UINT4 u4IfIndex));

INT4
VlanGetCVlanIdList PROTO ((UINT4 u4IfIndex, tVlanId SVlanId, 
                           tVlanListExt CVlanList));

INT4
VlanPbClearEvcL2TunnelCounters PROTO ((UINT4 u4ContextId, tVlanId VlanId));

INT4
VlanGetVlanInfoAndUntagFrame PROTO ((tCRU_BUF_CHAIN_DESC * pFrame,
                                     UINT4 u4InPort, tVlanTag * pVlanTag)); 

INT4
VlanValidatePortEntry (UINT4 u4IfIndex);

INT4
VlanSetInternalPort PROTO ((UINT4 u4IfIndex, UINT2 u2VlanId, 
       UINT1 u1IsAdd));

INT4
VlanValidateVlanId PROTO ((UINT4 u4ContextId, UINT4 u4VlanId));

INT4
VlanSetMaxSvlanPerIsid PROTO ((UINT4 u4MaxSvlanPerIsid));

INT4
VlanSetMaxCvlanPerIsid PROTO ((UINT4 u4MaxCvlanPerIsid));

INT4
VlanGetMaxSvlanPerIsid PROTO ((UINT4 *pu4MaxSvlanPerIsid));

INT4
VlanGetMaxCvlanPerIsid PROTO ((UINT4 *pu4MaxSvlanPerIsid));

INT4 
VlanDecodePcpAndTagLenInfo PROTO ((UINT4 u4ContextId, UINT2 u2LocalPort,
                          tCRU_BUF_CHAIN_DESC * pBuf,  UINT4 *pu4TagOffSet,
        UINT1  *pu1Pcp, UINT1 *pu1Priority,
        UINT1 *pu1DropEligible));
INT4
VlanSendFrameToPort PROTO ((tCRU_BUF_CHAIN_DESC * pFrame, 
     tVlanOutIfMsg * pVlanOutIfMsg,
     UINT4 u4ContextId, UINT2 u2Port, 
     tVlanId VlanId, UINT1 u1RegenPri));

INT4
VlanConfigStaticUcastEntry PROTO ((tConfigStUcastInfo *pStaticUnicastInfo,
       UINT1 u1Action, UINT1 * pu1ErrorCode));
INT4
VlanConfigStaticMcastEntry PROTO ((tConfigStMcastInfo
       *pStaticMulticastInfo, 
       UINT1 u1Action, UINT1 * pu1ErrorCode));
INT4
VlanDelStFiltEntStatusOther PROTO ((UINT4 u4ContextId, tVlanId VlanId, 
        UINT1 * pu1ErrorCode));
INT4
VlanSetVlanFloodingStatus PROTO ((UINT4 u4ContextId, tVlanId u2VlanId, 
      UINT1 u1Status));

INT4
VlanGetStaticUnicastEntry PROTO ((tConfigStUcastInfo 
      *pStaticUnicastInfo, UINT1 * pu1ErrorCode));

INT4
VlanCfgLearningFlooding PROTO ((UINT4 u4ContextId, tVlanId VlanId,
      UINT1 u1Action, UINT1 * pu1ErrCode));

INT4
VlanGetStaticMulticastEntry PROTO ((tConfigStMcastInfo 
                                    *pStaticMCastInfo, UINT1 * pu1ErrCode));

VOID
VlanGetPortStats (UINT4 u4IfIndex,tVlanId VlanId,
                  UINT4 *pu4TxFCl, UINT4 *pu4RxFCl);
BOOL1 VlanIsDeiBitSet (UINT4 u4IfIndex);
INT4  VlanConfPortVlanLckStatus (UINT4 u4IfIndex,
                    UINT2 u2VlanId, BOOL1 b1LckStatus);

INT4 VlanTagOutFrame PROTO ((UINT4, UINT2, tCRU_BUF_CHAIN_HEADER *, tVlanTagInfo *));
VOID
VlanMiDelDynamicDefGroupInfoForVlan PROTO ((UINT4 u4ContextId, tVlanId VlanId));

INT4 VlanGetVlanTagInfo PROTO ((tCRU_BUF_CHAIN_DESC * pFrame, UINT4 u4InPort, 
    tVlanId * pVlanId));
VOID VlanProcessRestorationComplete PROTO ((VOID));
#ifdef MRVLLS
VOID 
VlanHwFdbTableAdd PROTO ((UINT4 u4Port, tMacAddr MacAddr, tVlanId VlanId, 
                          UINT1 u1EntryType));
#endif

VOID
VlanIndicateConfigRestoreFail PROTO ((VOID));

PUBLIC INT4
VlanApiDoEgressFiltering PROTO ((UINT4 u4Port, tVlanId VlanId, 
                                 tMacAddr DstMacAddr));
INT4
VlanCheckWildCardEntry(tMacAddr WildCardMacAddr);

VOID
VlanSetDefPortInfo (UINT2 u2Port);

INT4
VlanDelFdbEntryByMacAddr (tMacAddr MacAddr);

INT4
VlanUtlWalkFnGetFdbByMacAddr (tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
                              void *pArg, void *pOut);
INT4
VlanApiGetPvid (UINT4 u4IfIndex , UINT4 *pu4RetValFsDot1qPvid);

INT4
VlanGetRmonStatsPerVlan (UINT4 u4ContextId , tVlanId VlanId,
                         tVlanEthStats *pVlanStatsEntry);
INT4 
VlanGetVlanIdEntry (UINT4 u4ContextId,tVlanId u2VlanId); 

INT4 VlanPbPortEncapTypeIsDot1q(UINT4 u4FsPbPort);
INT4
VlanGetPortDefaultPriority (INT4 i4IfIndex , INT4 *pi4Dot1dDefaultUserPriority);

VOID
VlanApiDeleteFdbEntry (tVlanId VlanId, tMacAddr MacAddress);

VOID VlanTunnelProtocolMac (UINT1 u1ProtocolId, tMacAddr MacAddress);

/**** UNKNOWN_MULTICAST_CHANGE - START ****/
INT4
VlanGetEgressPorts (tVlanId VlanId, tLocalPortList *pPortList);

/**** UNKNOWN_MULTICAST_CHANGE - END ****/
INT4
VlanApiUpdateStaticVlanInfo (UINT4 u4ContextId, tVlanId VlanId, UINT4 u4IfIndex, UINT4 u4Action);

VOID
VlanApiDeleteRemoteFdb (VOID);

VOID 
VlanApiSendBulkReq (VOID);
#ifdef EVPN_VXLAN_WANTED
INT4
VlanApiGetMACUpdate (UINT1 u1ProtoId, UINT2 u2VlanId);
#endif
INT4
VlanGetAdminMacLearnStatus (UINT4 u4ContextId,UINT4 u4VlanIndex,INT4 *pi4VlanAdminMacLearningStatus);
#ifdef VXLAN_WANTED
INT4
VlanApiSetMemberPort (UINT4 u4VlanId, UINT4 u4NveIfIndex, UINT4 u4Flag);
#endif
#ifdef ICCH_WANTED
INT4
VlanApiSetIcchPortProperties (VOID);
INT4 VlanApiSetIcchPortMacLearningStatus (UINT4 u4IfIndex,UINT1 u1Status);
#endif /* ICCH_WANTED */
INT4
VlanUpdatePortIsolationForMcLag (UINT4 u4IfIndex, UINT1 u1Action);

INT4
VlanRemoveAgedFdbEntry (UINT4 u4ContextId, tVlanId VlanId, tMacAddr pMacAddress, UINT4 u4IfIndex);
VOID
VlanAddIsolationTblForIccl (VOID);

INT1
VlanMemberPortType(INT4 i4Vlanid, UINT4 u4IfIndex, UINT1 *u1MemPortType);

VOID
VlanApiIcchFdbCheckRequest (UINT4 u4IfIndex);

VOID
VlanApiIcchProcessMclagDown (UINT4 u4IfIndex, UINT1 u1Status);

VOID
VlanApiIcchProcessMclagOperUp (UINT4 u4IfIndex);

VOID
VlanApiIcchCheckPortStatus (UINT4 u4IfIndex);

UINT1
VlanApiEvbGetSystemStatus (UINT4 u4ContextId);

VOID
VlanApiEvbGetSbpPortsOnUap (UINT4 u4UapIfIndex, tVlanEvbSbpArray  *pSbpArray);


INT4
VlanGetSChFilterStatus (UINT4 u4UapIfIndex, UINT4 u4SVID);

INT4
VlanSetSChFilterStatus (UINT4 u4UapIfIndex, UINT4 u4SVID,
                         INT4 i4RetValFsMIEvbSChannelFilterStatus);


INT4
VlanApiGetSChIfIndex (UINT4 u4ContextId, UINT4 u4UapIfIndex, 
                      UINT2 u2SVID, UINT4 *pu4SChIfIndex);
INT4
VlanApiGetSChInfoFromSChIfIndex (UINT4 u4SChIfIndex, UINT4 *pu4UapIfIndex,
                                  UINT2 *pu2SVID);

VOID
VlanIvrGetMclagEnabledStatus (UINT4 u4ContextId, tVlanId VlanId, UINT1 *pu1MclagStatus);

INT4
VlanPostSyncPktToVlan (tCRU_BUF_CHAIN_HEADER * pBuf);

INT4
VlanUpdateMclagStatus (UINT2 u2Port, UINT1 u1MclagStatus);
INT4
VlanGetCpvidForCep (UINT4 u4Port, UINT2 *pu2CVid);
INT4
VlanApiSetFdbFreezeStatus (INT4 i4MacLearnStatus);
INT4
VlanApiSetVlanMacLearningStatus PROTO ((UINT4, tVlanId, UINT1));

INT4
VlanCheckMemberPort (UINT4 u4ContextId, UINT4 u4IfIndex,UINT4 u4VlanId);

UINT1 VlanApiIsVlanExists (tVlanId VlanId);

INT4
VlanApiFlushFdbId (UINT4 u4ContextId, UINT4 u4FdbId);

INT4
VlanApiIsOnlyUntaggedPorts(tVlanId VlanId);

INT4
VlanApiIsPvidVlanOfAnyPorts (tVlanId VlanId);



#endif /* _VLAN_H_ */
