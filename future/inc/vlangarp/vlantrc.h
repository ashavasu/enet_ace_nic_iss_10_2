/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlantrc.h,v 1.28 2016/07/16 11:15:03 siva Exp $
 *
 * Description: This file contains trace and debug related
 *              macros used in VLAN module.
 *
 *******************************************************************/
#ifndef _VLANTRC_H
#define _VLANTRC_H


#define VLAN_DBG_FLAG gpVlanContextInfo->u4VlanDbg

#define VLAN_GLB_TRC_FLAG  gu4VlanGlobalTrace
#define CONTEXT(x) "%s" x, VLAN_CURR_CONTEXT_STR()


#define   VLAN_NAME       ("VLAN:")
extern VOID VlanPrintMacAddr PROTO ((UINT4 u4ModId, tMacAddr MacAddr));
extern VOID VlanPrintPortList PROTO ((UINT4 u4ModId, tPortList PortList, 
                                      UINT2 u2InPort));

extern VOID
VlanGlobalTrace (UINT4 u4ContextId, UINT4 u4ModTrc, 
   UINT4 u4Flags, const char *fmt, ...);

#define VLAN_MOD_TRC       0x00010000
#define VLAN_PRI_TRC       0x00020000
#define VLAN_RED_TRC       0x00040000 /* VLAN Redundancy module traces */
#define VLAN_ICCH_TRC      0x00080000 /* ICCH module traces */
#define VLAN_EVB_TRC       0x00100000 /* EVB Module Traces */
#define VLAN_ALL_FLAG      126976

#define VLAN_TRC_TYPE_ALL (INIT_SHUT_TRC | MGMT_TRC | DATA_PATH_TRC | \
                           CONTROL_PLANE_TRC | DUMP_TRC | OS_RESOURCE_TRC | \
                           ALL_FAILURE_TRC | BUFFER_TRC)


extern UINT4 gu4VlanTrcLvl; 
#define VLAN_TRC_LVL        gu4VlanTrcLvl
   
#ifdef TRACE_WANTED
        
#define VLAN_PRINT_PORT_LIST(u4ModId, PortList, u2InPort)  \
        VlanPrintPortList (u4ModId, (PortList), u2InPort)

#define VLAN_PRINT_MAC_ADDR(u4ModId, pMacAddr)   \
        VlanPrintMacAddr (u4ModId, (pMacAddr))

#define VLAN_GLOBAL_TRC  VlanGlobalTrace

#define VLAN_GBL_TRC(args) \
        UtlTrcLog (ALL_FAILURE_TRC, ALL_FAILURE_TRC, VLAN_NAME, args)

#define VLAN_DUMP_TRC(ModTrc, Mask, ModuleName, pu1Pkt, u2PktLen, arg1, arg2) \
{                                                                             \
        if((VLAN_DBG_FLAG & ModTrc) != 0)                                     \
        {                                                                     \
            UtlTrcLog(VLAN_DBG_FLAG, Mask , ModuleName,  CONTEXT(arg1), arg2);\
            if ((VLAN_DBG_FLAG & Mask))                                       \
            {                                                                 \
                VlanUtilDumpPkt(pu1Pkt, u2PktLen);                            \
            }                                                                 \
        }                                                                     \
}

#define VLAN_TRC(ModTrc, Mask, ModuleName, Fmt) \
        if ((VLAN_DBG_FLAG & ModTrc) != 0) {\
           UtlTrcLog (VLAN_DBG_FLAG, Mask, ModuleName, CONTEXT(Fmt)); \
        }
#define VLAN_TRC_ARG1(ModTrc, Mask, ModuleName, Fmt, Arg1) \
        if ((VLAN_DBG_FLAG & ModTrc) != 0) {\
           UtlTrcLog (VLAN_DBG_FLAG, Mask, ModuleName,CONTEXT(Fmt), Arg1); \
          }
#define VLAN_TRC_ARG2(ModTrc, Mask, ModuleName, Fmt, Arg1, Arg2) \
        if ((VLAN_DBG_FLAG & ModTrc) != 0) {\
           UtlTrcLog (VLAN_DBG_FLAG, Mask, ModuleName,CONTEXT(Fmt), Arg1, Arg2); \
        }

#define VLAN_TRC_ARG3(ModTrc, Mask, ModuleName, Fmt, Arg1, Arg2, Arg3) \
        if ((VLAN_DBG_FLAG & ModTrc) != 0) {\
           UtlTrcLog (VLAN_DBG_FLAG, Mask, ModuleName, CONTEXT(Fmt), Arg1, Arg2, Arg3); \
        }
#define VLAN_TRC_ARG4(ModTrc, Mask, ModuleName, Fmt, Arg1, Arg2, Arg3, Arg4) \
        if ((VLAN_DBG_FLAG & ModTrc) != 0) {\
           UtlTrcLog (VLAN_DBG_FLAG, Mask, ModuleName, CONTEXT(Fmt), Arg1, Arg2, Arg3, Arg4); \
        }


#define VLAN_TRC_ARG5(ModTrc, Mask, ModuleName, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5) \
        if ((VLAN_DBG_FLAG & ModTrc) != 0) {\
           MOD_TRC_ARG5(VLAN_DBG_FLAG, Mask, ModuleName, CONTEXT(Fmt), Arg1, Arg2, Arg3, Arg4, Arg5); \
        }
#define VLAN_PRINT UtlTrcLog

#define VLAN_TRC_ARG6(ModTrc, Mask, ModuleName, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6) \
        if ((VLAN_DBG_FLAG & ModTrc) != 0) {\
           MOD_TRC_ARG6(VLAN_DBG_FLAG, Mask, ModuleName, CONTEXT(Fmt), Arg1, Arg2, Arg3, Arg4, Arg5, Arg6); \
        }

#define VLAN_TRC_ARG7(ModTrc, Mask, ModuleName, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7) \
        if ((VLAN_DBG_FLAG & ModTrc) != 0) {\
           MOD_TRC_ARG7(VLAN_DBG_FLAG, Mask, ModuleName, CONTEXT(Fmt), Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7); \
        }
#else

#define VLAN_PRINT_PORT_LIST(u4ModId, PortList, u2InPort)
#define VLAN_PRINT_MAC_ADDR(u4ModId, pMacAddr)

#define VLAN_GLOBAL_TRC  VlanGlobalTrace
#define VLAN_GBL_TRC(args) \
{ \
    UNUSED_PARAM(args);\
}
#define VLAN_TRC(ModTrc, Mask, ModuleName, Fmt)
#define VLAN_DUMP_TRC(ModTrc, Mask, ModuleName, pu1Pkt, u2PktLen, arg1, arg2) \
{\
    UNUSED_PARAM (ModTrc);  \
    UNUSED_PARAM (Mask);    \
    UNUSED_PARAM (ModuleName);\
    UNUSED_PARAM (pu1Pkt);  \
    UNUSED_PARAM (u2PktLen);\
    UNUSED_PARAM (arg1);     \
    UNUSED_PARAM (arg2);     \
}

#define VLAN_TRC_ARG1(ModTrc, Mask, ModuleName, Fmt, Arg1) \
{ \
    UNUSED_PARAM(Arg1);\
}

#define VLAN_TRC_ARG2(ModTrc, Mask, ModuleName, Fmt, Arg1, Arg2)\
{ \
  UNUSED_PARAM (Arg1);\
  UNUSED_PARAM (Arg2);\
}
#define VLAN_TRC_ARG3(ModTrc, Mask, ModuleName, Fmt, Arg1, Arg2, Arg3)\
{ \
  UNUSED_PARAM (Arg1);\
  UNUSED_PARAM (Arg2);\
  UNUSED_PARAM (Arg3);\
}
#define VLAN_TRC_ARG4(ModTrc, Mask, ModuleName, Fmt, Arg1, Arg2, Arg3, Arg4)\
{ \
  UNUSED_PARAM (Arg1);\
  UNUSED_PARAM (Arg2);\
  UNUSED_PARAM (Arg3);\
  UNUSED_PARAM (Arg4);\
}
#define VLAN_TRC_ARG5(ModTrc, Mask, ModuleName, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)\
{ \
  UNUSED_PARAM (Arg1);\
  UNUSED_PARAM (Arg2);\
  UNUSED_PARAM (Arg3);\
  UNUSED_PARAM (Arg4);\
  UNUSED_PARAM (Arg5);\
}

#define VLAN_TRC_ARG6(ModTrc, Mask, ModuleName, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6) \
{ \
  UNUSED_PARAM (Arg1);\
  UNUSED_PARAM (Arg2);\
  UNUSED_PARAM (Arg3);\
  UNUSED_PARAM (Arg4);\
  UNUSED_PARAM (Arg5);\
  UNUSED_PARAM (Arg6);\
}

#define VLAN_TRC_ARG7(ModTrc, Mask, ModuleName, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7) \
{ \
  UNUSED_PARAM (Arg1);\
  UNUSED_PARAM (Arg2);\
  UNUSED_PARAM (Arg3);\
  UNUSED_PARAM (Arg4);\
  UNUSED_PARAM (Arg5);\
  UNUSED_PARAM (Arg6);\
  UNUSED_PARAM (Arg7);\
}

#endif /* TRACE_WANTED */

#endif /* _VLANTRC_H_ */
