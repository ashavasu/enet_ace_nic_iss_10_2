/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: vlannpwr.h,v 1.22 2016/06/16 12:34:59 siva Exp $
 *
 * Description:This file contains the necessary data strucutures
 *             for packet handling in NPAPI layer
 *             <Part of dual node stacking model>
 *
 *******************************************************************/

#ifndef __VLAN_NP_WR_H__
#define __VLAN_NP_WR_H__

#include "fsvlan.h"
#ifdef L2RED_WANTED
#include "vlanminp.h"
#endif /* L2RED_WANTED */
#include "evcnp.h"

/* OPER ID */
/* when new NP call is added in VLAN, the following files 
 * has to be updated 
 * 1. npgensup.h 
 * 2. npbcmsup.h
*/

enum
{
  FS_MI_VLAN_HW_INIT  = 1,               
  FS_MI_VLAN_HW_DE_INIT                   ,               
  FS_MI_VLAN_HW_SET_ALL_GROUPS_PORTS      ,               
  FS_MI_VLAN_HW_RESET_ALL_GROUPS_PORTS    ,               
  FS_MI_VLAN_HW_RESET_UN_REG_GROUPS_PORTS ,               
  FS_MI_VLAN_HW_SET_UN_REG_GROUPS_PORTS   ,               
  FS_MI_VLAN_HW_ADD_STATIC_UCAST_ENTRY    ,               
  FS_MI_VLAN_HW_DEL_STATIC_UCAST_ENTRY    ,               
  FS_MI_VLAN_HW_GET_FDB_ENTRY             ,               
  FS_MI_VLAN_HW_GET_FDB_COUNT             ,               
  FS_MI_VLAN_HW_GET_FIRST_TP_FDB_ENTRY    ,               
  FS_MI_VLAN_HW_GET_NEXT_TP_FDB_ENTRY     ,               
  FS_MI_VLAN_HW_ADD_MCAST_ENTRY           ,               
  FS_MI_VLAN_HW_ADD_ST_MCAST_ENTRY        ,               
  FS_MI_VLAN_HW_SET_MCAST_PORT            ,               
  FS_MI_VLAN_HW_RESET_MCAST_PORT          ,               
  FS_MI_VLAN_HW_DEL_MCAST_ENTRY           ,               
  FS_MI_VLAN_HW_DEL_ST_MCAST_ENTRY        ,               
  FS_MI_VLAN_HW_ADD_VLAN_ENTRY            ,               
  FS_MI_VLAN_HW_DEL_VLAN_ENTRY            ,               
  FS_MI_VLAN_HW_SET_VLAN_MEMBER_PORT      ,               
  FS_MI_VLAN_HW_RESET_VLAN_MEMBER_PORT    ,               
  FS_MI_VLAN_HW_SET_PORT_PVID             ,               
  FS_MI_VLAN_HW_SET_DEFAULT_VLAN_ID       ,               
  FS_MI_VLAN_HW_SYNC_DEFAULT_VLAN_ID      ,               
  FS_MI_VLAN_RED_HW_UPDATE_D_B_FOR_DEFAULT_VLAN_ID      , 
  FS_MI_VLAN_HW_SCAN_PROTOCOL_VLAN_TBL                  , 
  FS_MI_VLAN_HW_GET_VLAN_PROTOCOL_MAP                   , 
  FS_MI_VLAN_HW_SCAN_MULTICAST_TBL                      , 
  FS_MI_VLAN_HW_GET_ST_MCAST_ENTRY                      , 
  FS_MI_VLAN_HW_SCAN_UNICAST_TBL                        , 
  FS_MI_VLAN_HW_GET_STATIC_UCAST_ENTRY                  , 
  FS_MI_VLAN_HW_GET_SYNCED_TNL_PROTOCOL_MAC_ADDR        , 
  FS_MI_VLAN_HW_SET_PORT_ACC_FRAME_TYPE                 , 
  FS_MI_VLAN_HW_SET_PORT_ING_FILTERING                  , 
  FS_MI_VLAN_HW_VLAN_ENABLE                             , 
  FS_MI_VLAN_HW_VLAN_DISABLE                            , 
  FS_MI_VLAN_HW_SET_VLAN_LEARNING_TYPE                  , 
  FS_MI_VLAN_HW_SET_MAC_BASED_STATUS_ON_PORT            , 
  FS_MI_VLAN_HW_SET_SUBNET_BASED_STATUS_ON_PORT         , 
  FS_MI_VLAN_HW_ENABLE_PROTO_VLAN_ON_PORT               , 
  FS_MI_VLAN_HW_SET_DEF_USER_PRIORITY                   , 
  FS_MI_VLAN_HW_SET_PORT_NUM_TRAF_CLASSES               , 
  FS_MI_VLAN_HW_SET_REGEN_USER_PRIORITY                 , 
  FS_MI_VLAN_HW_SET_TRAFF_CLASS_MAP                     , 
  FS_MI_VLAN_HW_GMRP_ENABLE                             , 
  FS_MI_VLAN_HW_GMRP_DISABLE                            , 
  FS_MI_VLAN_HW_GVRP_ENABLE                             , 
  FS_MI_VLAN_HW_GVRP_DISABLE                            , 
  FS_MI_NP_DELETE_ALL_FDB_ENTRIES                       , 
  FS_MI_VLAN_HW_ADD_VLAN_PROTOCOL_MAP                   , 
  FS_MI_VLAN_HW_DEL_VLAN_PROTOCOL_MAP                   , 
  FS_MI_VLAN_HW_GET_PORT_STATS                          , 
  FS_MI_VLAN_HW_GET_PORT_STATS64                        , 
  FS_MI_VLAN_HW_GET_VLAN_STATS                          , 
  FS_MI_VLAN_HW_RESET_VLAN_STATS                        , 
  FS_MI_VLAN_HW_SET_PORT_TUNNEL_MODE                    , 
  FS_MI_VLAN_HW_SET_TUNNEL_FILTER                       , 
  FS_MI_VLAN_HW_CHECK_TAG_AT_EGRESS                     , 
  FS_MI_VLAN_HW_CHECK_TAG_AT_INGRESS                    , 
  FS_MI_VLAN_HW_CREATE_FDB_ID                           , 
  FS_MI_VLAN_HW_DELETE_FDB_ID                           , 
  FS_MI_VLAN_HW_ASSOCIATE_VLAN_FDB                      , 
  FS_MI_VLAN_HW_DISASSOCIATE_VLAN_FDB                   , 
  FS_MI_VLAN_HW_FLUSH_PORT_FDB_ID                       , 
  FS_MI_VLAN_HW_FLUSH_PORT_FDB_LIST                     , 
  FS_MI_VLAN_HW_FLUSH_PORT                              , 
  FS_MI_VLAN_HW_FLUSH_FDB_ID                            , 
  FS_MI_VLAN_HW_SET_SHORT_AGEOUT                        , 
  FS_MI_VLAN_HW_RESET_SHORT_AGEOUT                      , 
  FS_MI_VLAN_HW_GET_VLAN_INFO                           , 
  FS_MI_VLAN_HW_GET_MCAST_ENTRY                         , 
  FS_MI_VLAN_HW_TRAFF_CLASS_MAP_INIT                    , 
  FS_MI_BRG_SET_AGING_TIME                              , 
  FS_MI_VLAN_HW_SET_BRG_MODE                            , 
  FS_MI_VLAN_HW_SET_BASE_BRIDGE_MODE                    , 
  FS_MI_VLAN_HW_ADD_PORT_MAC_VLAN_ENTRY                 , 
  FS_MI_VLAN_HW_DELETE_PORT_MAC_VLAN_ENTRY              , 
  FS_MI_VLAN_HW_ADD_PORT_SUBNET_VLAN_ENTRY              , 
  FS_MI_VLAN_HW_DELETE_PORT_SUBNET_VLAN_ENTRY           , 
  FS_MI_VLAN_HW_UPDATE_PORT_SUBNET_VLAN_ENTRY           , 
  FS_MI_VLAN_NP_HW_RUN_MAC_AGEING                       , 
  FS_MI_VLAN_HW_MAC_LEARNING_LIMIT                      , 
  FS_MI_VLAN_HW_SWITCH_MAC_LEARNING_LIMIT               , 
  FS_MI_VLAN_HW_MAC_LEARNING_STATUS                     , 
  FS_MI_VLAN_HW_SET_FID_PORT_LEARNING_STATUS            , 
  FS_MI_VLAN_HW_SET_PROTOCOL_TUNNEL_STATUS_ON_PORT      , 
  FS_MI_VLAN_HW_SET_PORT_PROTECTED_STATUS               , 
  FS_MI_VLAN_HW_ADD_STATIC_UCAST_ENTRY_EX               , 
  FS_MI_VLAN_HW_GET_STATIC_UCAST_ENTRY_EX               , 
  FS_VLAN_HW_FORWARD_PKT_ON_PORTS                       , 
  FS_MI_VLAN_HW_PORT_MAC_LEARNING_STATUS                , 
  FS_VLAN_HW_GET_MAC_LEARNING_MODE                      , 
  FS_MI_VLAN_HW_SET_PROVIDER_BRIDGE_PORT_TYPE           , 
  FS_MI_VLAN_HW_SET_PORT_INGRESS_ETHER_TYPE             , 
  FS_MI_VLAN_HW_SET_PORT_EGRESS_ETHER_TYPE              , 
  FS_MI_VLAN_HW_SET_PORT_S_VLAN_TRANSLATION_STATUS      , 
  FS_MI_VLAN_HW_ADD_S_VLAN_TRANSLATION_ENTRY            , 
  FS_MI_VLAN_HW_DEL_S_VLAN_TRANSLATION_ENTRY            , 
  FS_MI_VLAN_HW_SET_PORT_ETHER_TYPE_SWAP_STATUS         , 
  FS_MI_VLAN_HW_ADD_ETHER_TYPE_SWAP_ENTRY               , 
  FS_MI_VLAN_HW_DEL_ETHER_TYPE_SWAP_ENTRY               , 
  FS_MI_VLAN_HW_ADD_S_VLAN_MAP                          , 
  FS_MI_VLAN_HW_DELETE_S_VLAN_MAP                       , 
  FS_MI_VLAN_HW_SET_PORT_S_VLAN_CLASSIFY_METHOD         , 
  FS_MI_VLAN_HW_PORT_MAC_LEARNING_LIMIT                 , 
  FS_MI_VLAN_HW_MULTICAST_MAC_TABLE_LIMIT               , 
  FS_MI_VLAN_HW_SET_PORT_CUSTOMER_VLAN                  , 
  FS_MI_VLAN_HW_RESET_PORT_CUSTOMER_VLAN                , 
  FS_MI_VLAN_HW_CREATE_PROVIDER_EDGE_PORT               , 
  FS_MI_VLAN_HW_DEL_PROVIDER_EDGE_PORT                  , 
  FS_MI_VLAN_HW_SET_PEP_PVID                            , 
  FS_MI_VLAN_HW_SET_PEP_ACC_FRAME_TYPE                  , 
  FS_MI_VLAN_HW_SET_PEP_DEF_USER_PRIORITY               , 
  FS_MI_VLAN_HW_SET_PEP_ING_FILTERING                   , 
  FS_MI_VLAN_HW_SET_CVID_UNTAG_PEP                      , 
  FS_MI_VLAN_HW_SET_CVID_UNTAG_CEP                      , 
  FS_MI_VLAN_HW_SET_PCP_ENCOD_TBL                       , 
  FS_MI_VLAN_HW_SET_PCP_DECOD_TBL                       , 
  FS_MI_VLAN_HW_SET_PORT_USE_DEI                        , 
  FS_MI_VLAN_HW_SET_PORT_REQ_DROP_ENCODING              , 
  FS_MI_VLAN_HW_SET_PORT_PCP_SELECTION                  , 
  FS_MI_VLAN_HW_SET_SERVICE_PRI_REGEN_ENTRY             , 
  FS_MI_VLAN_HW_SET_TUNNEL_MAC_ADDRESS                  , 
  FS_MI_VLAN_HW_GET_NEXT_S_VLAN_MAP                     , 
  FS_MI_VLAN_HW_GET_NEXT_S_VLAN_TRANSLATION_ENTRY       , 
  FS_MI_VLAN_HW_SET_PORT_PROPERTY                       , 
  FS_MI_VLAN_HW_SET_LOOPBACK_STATUS                     ,
  FS_MI_VLAN_MBSM_HW_SET_PORT_INGRESS_ETHER_TYPE        , 
  FS_MI_VLAN_MBSM_HW_SET_PORT_EGRESS_ETHER_TYPE         ,
  FS_MI_VLAN_MBSM_HW_SET_PORT_PROPERTY                  ,
  FS_MI_VLAN_MBSM_HW_SET_PROVIDER_BRIDGE_PORT_TYPE      , 
  FS_MI_VLAN_MBSM_HW_SET_PORT_S_VLAN_TRANSLATION_STATUS , 
  FS_MI_VLAN_MBSM_HW_ADD_S_VLAN_TRANSLATION_ENTRY       , 
  FS_MI_VLAN_MBSM_HW_SET_PORT_ETHER_TYPE_SWAP_STATUS    , 
  FS_MI_VLAN_MBSM_HW_ADD_ETHER_TYPE_SWAP_ENTRY          , 
  FS_MI_VLAN_MBSM_HW_ADD_S_VLAN_MAP                     , 
  FS_MI_VLAN_MBSM_HW_SET_PORT_S_VLAN_CLASSIFY_METHOD    , 
  FS_MI_VLAN_MBSM_HW_PORT_MAC_LEARNING_LIMIT            , 
  FS_MI_VLAN_MBSM_HW_SET_PORT_CUSTOMER_VLAN             , 
  FS_MI_VLAN_MBSM_HW_CREATE_PROVIDER_EDGE_PORT          , 
  FS_MI_VLAN_MBSM_HW_SET_PCP_ENCOD_TBL                  , 
  FS_MI_VLAN_MBSM_HW_SET_PCP_DECOD_TBL                  , 
  FS_MI_VLAN_MBSM_HW_SET_PORT_USE_DEI                   , 
  FS_MI_VLAN_MBSM_HW_SET_PORT_REQ_DROP_ENCODING         , 
  FS_MI_VLAN_MBSM_HW_SET_PORT_PCP_SELECTION             , 
  FS_MI_VLAN_MBSM_HW_SET_SERVICE_PRI_REGEN_ENTRY        , 
  FS_MI_VLAN_MBSM_HW_MULTICAST_MAC_TABLE_LIMIT          , 
  FS_MI_VLAN_MBSM_HW_INIT                               , 
  FS_MI_VLAN_MBSM_HW_SET_DEFAULT_VLAN_ID                , 
  FS_MI_VLAN_MBSM_HW_ADD_STATIC_UCAST_ENTRY             , 
  FS_MI_VLAN_MBSM_HW_SET_UCAST_PORT                     , 
  FS_MI_VLAN_MBSM_HW_RESET_UCAST_PORT                   , 
  FS_MI_VLAN_MBSM_HW_DEL_STATIC_UCAST_ENTRY             , 
  FS_MI_VLAN_MBSM_HW_ADD_MCAST_ENTRY                    , 
  FS_MI_VLAN_MBSM_HW_SET_MCAST_PORT                     , 
  FS_MI_VLAN_MBSM_HW_RESET_MCAST_PORT                   , 
  FS_MI_VLAN_MBSM_HW_ADD_VLAN_ENTRY                     , 
  FS_MI_VLAN_MBSM_HW_SET_VLAN_MEMBER_PORT               , 
  FS_MI_VLAN_MBSM_HW_RESET_VLAN_MEMBER_PORT             , 
  FS_MI_VLAN_MBSM_HW_SET_PORT_PVID                      , 
  FS_MI_VLAN_MBSM_HW_SET_PORT_ACC_FRAME_TYPE            , 
  FS_MI_VLAN_MBSM_HW_SET_PORT_ING_FILTERING             , 
  FS_MI_VLAN_MBSM_HW_SET_DEF_USER_PRIORITY              , 
  FS_MI_VLAN_MBSM_HW_TRAFF_CLASS_MAP_INIT               , 
  FS_MI_VLAN_MBSM_HW_SET_TRAFF_CLASS_MAP                , 
  FS_MI_VLAN_MBSM_HW_ADD_ST_MCAST_ENTRY                 , 
  FS_MI_VLAN_MBSM_HW_DEL_ST_MCAST_ENTRY                 , 
  FS_MI_VLAN_MBSM_HW_SET_PORT_NUM_TRAF_CLASSES          , 
  FS_MI_VLAN_MBSM_HW_SET_REGEN_USER_PRIORITY            , 
  FS_MI_VLAN_MBSM_HW_SET_SUBNET_BASED_STATUS_ON_PORT    , 
  FS_MI_VLAN_MBSM_HW_ENABLE_PROTO_VLAN_ON_PORT          , 
  FS_MI_VLAN_MBSM_HW_SET_ALL_GROUPS_PORTS               , 
  FS_MI_VLAN_MBSM_HW_SET_UN_REG_GROUPS_PORTS            , 
  FS_MI_VLAN_MBSM_HW_SET_TUNNEL_FILTER                  , 
  FS_MI_VLAN_MBSM_HW_ADD_PORT_SUBNET_VLAN_ENTRY         , 
  FS_MI_BRG_MBSM_SET_AGING_TIME                         , 
  FS_MI_VLAN_MBSM_HW_SET_FID_PORT_LEARNING_STATUS       , 
  FS_MI_VLAN_MBSM_HW_SET_PROTOCOL_TUNNEL_STATUS_ON_PORT , 
  FS_MI_VLAN_MBSM_HW_SET_PORT_PROTECTED_STATUS          , 
  FS_MI_VLAN_MBSM_HW_SWITCH_MAC_LEARNING_LIMIT          , 
  FS_MI_VLAN_MBSM_HW_PORT_MAC_LEARNING_STATUS           , 
  FS_MI_VLAN_MBSM_HW_MAC_LEARNING_LIMIT                 , 
  FS_MI_VLAN_MBSM_HW_ADD_STATIC_UCAST_ENTRY_EX          , 
  FS_MI_VLAN_MBSM_SYNC_F_D_B_INFO                       , 
  FS_MI_VLAN_MBSM_HW_SET_BRG_MODE                       , 
  FS_MI_VLAN_HW_SET_EVC_ATTRIBUTE                       ,
  FS_NP_HW_GET_PORT_FROM_FDB                            ,
  FS_MI_VLAN_HW_SET_CVLAN_STAT                          ,
  FS_MI_VLAN_HW_GET_CVLAN_STAT                          ,
  FS_MI_VLAN_HW_CLEAR_CVLAN_STAT                        ,
  FS_MI_VLAN_HW_PORT_UNICAST_MAC_SEC_TYPE               ,
  FS_MI_VLAN_HW_EVB_CONFIG_S_CH_IFACE                   ,
  FS_MI_VLAN_MBSM_HW_EVB_CONFIG_S_CH_IFACE              ,
  FS_MI_VLAN_HW_SET_BRIDGE_PORT_TYPE                    ,
  FS_MI_VLAN_MBSM_HW_SET_BRIDGE_PORT_TYPE               ,
  FS_MI_VLAN_HW_SET_MCAST_INDEX                         , 
  FS_MI_VLAN_HW_PORT_PKT_REFLECT_STATUS                 ,
  FS_MI_VLAN_MBSM_HW_PORT_PKT_REFLECT_STATUS            ,
  FS_MI_VLAN_HW_MAX_NP  /* Max Count should be the Last entry */
};

/* Required arguments list for the vlan NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */


typedef struct {
    int                          cmd;
    tFsNpVlanPortReflectEntry *  pPortReflectEntry;
    INT4                         rval;
} tNpwFsMiVlanHwPortPktReflectStatus;

typedef struct {
    tFsNpVlanPortReflectEntry *  pPortReflectEntry;
} tVlanNpWrFsMiVlanHwPortPktReflectStatus;


typedef struct {
    int                          cmd;
    tFsNpVlanPortReflectEntry *  pPortReflectEntry;
    INT4                         rval;
} tNpwFsMiVlanMbsmHwPortPktReflectStatus;



typedef struct {
    tFsNpVlanPortReflectEntry *  pPortReflectEntry;
} tVlanNpWrFsMiVlanMbsmHwPortPktReflectStatus;




typedef struct {
    UINT4  u4ContextId;
} tVlanNpWrFsMiVlanHwInit;

typedef struct {
    UINT4  u4ContextId;
} tVlanNpWrFsMiVlanHwDeInit;

typedef struct {
    tHwPortArray *  pHwAllGroupPorts;
    UINT4           u4ContextId;
    tVlanId         VlanId;
 UINT1           au1Pad[2];
} tVlanNpWrFsMiVlanHwSetAllGroupsPorts;

typedef struct {
    tHwPortArray *  pHwResetAllGroupPorts;
    UINT4           u4ContextId;
    tVlanId         VlanId;
 UINT1           au1Pad[2];
} tVlanNpWrFsMiVlanHwResetAllGroupsPorts;

typedef struct {
    tHwPortArray *  pHwResetUnRegGroupPorts;
    UINT4           u4ContextId;
    tVlanId         VlanId;
 UINT1           au1Pad[2];
} tVlanNpWrFsMiVlanHwResetUnRegGroupsPorts;

typedef struct {
    tHwPortArray *  pHwUnRegPorts;
    UINT4           u4ContextId;
    tVlanId         VlanId;
 UINT1           au1Pad[2];
} tVlanNpWrFsMiVlanHwSetUnRegGroupsPorts;

typedef struct {
    UINT4           u4Port;
    tHwPortArray *  pHwAllowedToGoPorts;
    UINT4           u4ContextId;
    UINT4           u4Fid;
    UINT1 *        MacAddr;
    UINT1           u1Status;
 UINT1           au1Pad[3];
} tVlanNpWrFsMiVlanHwAddStaticUcastEntry;

typedef struct {
    UINT4     u4Port;
    UINT4     u4ContextId;
    UINT4     u4Fid;
    UINT1 *  MacAddr;
} tVlanNpWrFsMiVlanHwDelStaticUcastEntry;

typedef struct {
    UINT4                 u4ContextId;
    UINT4                 u4FdbId;
    UINT1 *              MacAddr;
    tHwUnicastMacEntry *  pEntry;
} tVlanNpWrFsMiVlanHwGetFdbEntry;

typedef struct {
    UINT4 *  pu4Port;
    UINT1 *  i1pHwAddr;
    UINT2    u2VlanId;
    UINT1    au1Reserved[2];
} tVlanNpWrFsNpHwGetPortFromFdb;



#ifndef SW_LEARNING
typedef struct {
    UINT4    u4ContextId;
    UINT4    u4FdbId;
    UINT4 *  pu4Count;
} tVlanNpWrFsMiVlanHwGetFdbCount;

typedef struct {
    UINT4     u4ContextId;
    UINT4 *   pu4FdbId;
    UINT1 *  MacAddr;
} tVlanNpWrFsMiVlanHwGetFirstTpFdbEntry;

typedef struct {
    UINT4     u4ContextId;
    UINT4     u4FdbId;
    UINT1 *  MacAddr;
    UINT4 *   pu4NextContextId;
    UINT4 *   pu4NextFdbId;
    UINT1 *  NextMacAddr;
} tVlanNpWrFsMiVlanHwGetNextTpFdbEntry;

#endif /* SW_LEARNING */
typedef struct {
    tHwPortArray *  pHwMcastPorts;
    UINT4           u4ContextId;
    UINT1 *        MacAddr;
    tVlanId         VlanId;
 UINT1           au1Pad[2];
} tVlanNpWrFsMiVlanHwAddMcastEntry;

typedef struct {
    INT4            i4RcvPort;
    tHwPortArray *  pHwMcastPorts;
    UINT4           u4ContextId;
    UINT1 *        MacAddr;
    tVlanId         VlanId;
 UINT1           au1Pad[2];
} tVlanNpWrFsMiVlanHwAddStMcastEntry;

typedef struct {
    UINT4     u4IfIndex;
    UINT4     u4ContextId;
    UINT1 *  MacAddr;
    tVlanId   VlanId;
 UINT1     au1Pad[2];
} tVlanNpWrFsMiVlanHwSetMcastPort;

typedef struct {
    UINT4     u4IfIndex;
    UINT4     u4ContextId;
    UINT1 *  MacAddr;
    tVlanId   VlanId;
 UINT1     au1Pad[2];
} tVlanNpWrFsMiVlanHwResetMcastPort;

typedef struct {
    UINT4     u4ContextId;
    UINT1 *  MacAddr;
    tVlanId   VlanId;
 UINT1     au1Pad[2];
} tVlanNpWrFsMiVlanHwDelMcastEntry;

typedef struct {
    INT4      i4RcvPort;
    UINT4     u4ContextId;
    UINT1 *  MacAddr;
    tVlanId   VlanId;
 UINT1     au1Pad[2];
} tVlanNpWrFsMiVlanHwDelStMcastEntry;

typedef struct {
    tHwPortArray *  pHwEgressPorts;
    tHwPortArray *  pHwUnTagPorts;
    UINT4           u4ContextId;
    tVlanId         VlanId;
 UINT1           au1Pad[2];
} tVlanNpWrFsMiVlanHwAddVlanEntry;

typedef struct {
    UINT4    u4ContextId;
    tVlanId  VlanId;
 UINT1    au1Pad[2];
} tVlanNpWrFsMiVlanHwDelVlanEntry;

typedef struct {
    UINT4    u4IfIndex;
    UINT4    u4ContextId;
    tVlanId  VlanId;
    UINT1    u1IsTagged;
 UINT1    au1Pad[1];
} tVlanNpWrFsMiVlanHwSetVlanMemberPort;

typedef struct {
    UINT4    u4IfIndex;
    UINT4    u4ContextId;
    tVlanId  VlanId;
 UINT1    au1Pad[2];
} tVlanNpWrFsMiVlanHwResetVlanMemberPort;

typedef struct {
    UINT4    u4IfIndex;
    UINT4    u4ContextId;
    tVlanId  VlanId;
 UINT1    au1Pad[2];
} tVlanNpWrFsMiVlanHwSetPortPvid;

typedef struct {
    UINT4    u4ContextId;
    tVlanId  VlanId;
 UINT1    au1Pad[2];
} tVlanNpWrFsMiVlanHwSetDefaultVlanId;

#ifdef L2RED_WANTED
typedef struct {
    UINT4    u4ContextId;
    tVlanId  SwVlanId;
 UINT1    au1Pad[2];
} tVlanNpWrFsMiVlanHwSyncDefaultVlanId;

typedef struct {
    UINT4    u4ContextId;
    tVlanId  VlanId;
 UINT1    au1Pad[2];
} tVlanNpWrFsMiVlanRedHwUpdateDBForDefaultVlanId;

typedef struct {
    UINT4              u4Port;
    UINT4              u4ContextId;
    FsMiVlanHwProtoCb  ProtoVlanCallBack;
} tVlanNpWrFsMiVlanHwScanProtocolVlanTbl;

typedef struct {
    UINT4                 u4IfIndex;
    UINT4                 u4ContextId;
    UINT4                 u4GroupId;
    tVlanProtoTemplate *  pProtoTemplate;
    tVlanId *             pVlanId;
} tVlanNpWrFsMiVlanHwGetVlanProtocolMap;

typedef struct {
    UINT4              u4ContextId;
    FsMiVlanHwMcastCb  McastCallBack;
} tVlanNpWrFsMiVlanHwScanMulticastTbl;

typedef struct {
    UINT4           u4RcvPort;
    tHwPortArray *  pHwMcastPorts;
    UINT4           u4ContextId;
    UINT1 *        MacAddr;
    tVlanId         VlanId;
 UINT1           au1Pad[2];
} tVlanNpWrFsMiVlanHwGetStMcastEntry;

typedef struct {
    UINT4              u4ContextId;
    FsMiVlanHwUcastCb  UcastCallBack;
} tVlanNpWrFsMiVlanHwScanUnicastTbl;

typedef struct {
    UINT4           u4Port;
    tHwPortArray *  pAllowedToGoPorts;
    UINT4           u4ContextId;
    UINT4           u4Fid;
    UINT1 *        MacAddr;
    UINT1 *         pu1Status;
} tVlanNpWrFsMiVlanHwGetStaticUcastEntry;

typedef struct {
    UINT4     u4ContextId;
    UINT1 *  MacAddr;
    UINT2     u2Protocol;
 UINT1     au1Pad[2];
} tVlanNpWrFsMiVlanHwGetSyncedTnlProtocolMacAddr;

#endif /* L2RED_WANTED */
typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4ContextId;
    INT1   u1AccFrameType;
 UINT1  au1Pad[3];
} tVlanNpWrFsMiVlanHwSetPortAccFrameType;

typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4ContextId;
    UINT1  u1IngFilterEnable;
 UINT1  au1Pad[3];
} tVlanNpWrFsMiVlanHwSetPortIngFiltering;

typedef struct {
    UINT4  u4ContextId;
} tVlanNpWrFsMiVlanHwVlanEnable;

typedef struct {
    UINT4  u4ContextId;
} tVlanNpWrFsMiVlanHwVlanDisable;

typedef struct {
    UINT4  u4ContextId;
    UINT1  u1LearningType;
 UINT1  au1Pad[3];
} tVlanNpWrFsMiVlanHwSetVlanLearningType;

typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4ContextId;
    UINT1  u1MacBasedVlanEnable;
 UINT1  au1Pad[3];
} tVlanNpWrFsMiVlanHwSetMacBasedStatusOnPort;

typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4ContextId;
    UINT1  u1SubnetBasedVlanEnable;
 UINT1  au1Pad[3];
} tVlanNpWrFsMiVlanHwSetSubnetBasedStatusOnPort;


typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4ContextId;
    UINT1  u1VlanProtoEnable;
 UINT1  au1Pad[3];
} tVlanNpWrFsMiVlanHwEnableProtoVlanOnPort;

typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4ContextId;
    INT4   i4DefPriority;
} tVlanNpWrFsMiVlanHwSetDefUserPriority;

typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4ContextId;
    INT4   i4NumTraffClass;
} tVlanNpWrFsMiVlanHwSetPortNumTrafClasses;

typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4ContextId;
    INT4   i4UserPriority;
    INT4   i4RegenPriority;
} tVlanNpWrFsMiVlanHwSetRegenUserPriority;

typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4ContextId;
    INT4   i4UserPriority;
    INT4   i4TraffClass;
} tVlanNpWrFsMiVlanHwSetTraffClassMap;

typedef struct {
    UINT4  u4ContextId;
} tVlanNpWrFsMiVlanHwGmrpEnable;

typedef struct {
    UINT4  u4ContextId;
} tVlanNpWrFsMiVlanHwGmrpDisable;

typedef struct {
    UINT4  u4ContextId;
} tVlanNpWrFsMiVlanHwGvrpEnable;

typedef struct {
    UINT4  u4ContextId;
} tVlanNpWrFsMiVlanHwGvrpDisable;

typedef struct {
    UINT4  u4ContextId;
} tVlanNpWrFsMiNpDeleteAllFdbEntries;

typedef struct {
    UINT4                 u4IfIndex;
    UINT4                 u4ContextId;
    UINT4                 u4GroupId;
    tVlanProtoTemplate *  pProtoTemplate;
    tVlanId               VlanId;
 UINT1                 au1Pad[2];
} tVlanNpWrFsMiVlanHwAddVlanProtocolMap;

typedef struct {
    UINT4                 u4IfIndex;
    UINT4                 u4ContextId;
    UINT4                 u4GroupId;
    tVlanProtoTemplate *  pProtoTemplate;
} tVlanNpWrFsMiVlanHwDelVlanProtocolMap;

typedef struct {
    UINT4    u4Port;
    UINT4 *  pu4PortStatsValue;
    UINT4    u4ContextId;
    tVlanId  VlanId;
    UINT1    u1StatsType;
 UINT1    au1Pad[1];
} tVlanNpWrFsMiVlanHwGetPortStats;

typedef struct {
    UINT4                   u4Port;
    UINT4                   u4ContextId;
    tSNMP_COUNTER64_TYPE *  pValue;
    tVlanId                 VlanId;
    UINT1                   u1StatsType;
 UINT1                   au1Pad[1];
} tVlanNpWrFsMiVlanHwGetPortStats64;

typedef struct {
    UINT4    u4ContextId;
    UINT4 *  pu4VlanStatsValue;
    tVlanId  VlanId;
    UINT1    u1StatsType;
 UINT1    au1Pad[1];
} tVlanNpWrFsMiVlanHwGetVlanStats;

typedef struct {
    UINT4    u4ContextId;
    tVlanId  VlanId;
 UINT1    au1Pad[2];
} tVlanNpWrFsMiVlanHwResetVlanStats;

typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4ContextId;
    UINT4  u4Mode;
} tVlanNpWrFsMiVlanHwSetPortTunnelMode;

typedef struct {
    UINT4  u4ContextId;
    INT4   i4BridgeMode;
} tVlanNpWrFsMiVlanHwSetTunnelFilter;

typedef struct {
    UINT4    u4ContextId;
    UINT1 *  pu1TagSet;
} tVlanNpWrFsMiVlanHwCheckTagAtEgress;

typedef struct {
    UINT4    u4ContextId;
    UINT1 *  pu1TagSet;
} tVlanNpWrFsMiVlanHwCheckTagAtIngress;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4Fid;
} tVlanNpWrFsMiVlanHwCreateFdbId;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4Fid;
} tVlanNpWrFsMiVlanHwDeleteFdbId;

typedef struct {
    UINT4    u4ContextId;
    UINT4    u4Fid;
    tVlanId  VlanId;
 UINT1    au1Pad[2];
} tVlanNpWrFsMiVlanHwAssociateVlanFdb;

typedef struct {
    UINT4    u4ContextId;
    UINT4    u4Fid;
    tVlanId  VlanId;
 UINT1    au1Pad[2];
} tVlanNpWrFsMiVlanHwDisassociateVlanFdb;

typedef struct {
    UINT4  u4Port;
    UINT4  u4ContextId;
    UINT4  u4Fid;
    INT4   i4OptimizeFlag;
} tVlanNpWrFsMiVlanHwFlushPortFdbId;

typedef struct {
    UINT4             u4ContextId;
    tVlanFlushInfo *  pVlanFlushInfo;
} tVlanNpWrFsMiVlanHwFlushPortFdbList;

typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4ContextId;
    INT4   i4OptimizeFlag;
} tVlanNpWrFsMiVlanHwFlushPort;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4Fid;
} tVlanNpWrFsMiVlanHwFlushFdbId;

typedef struct {
    UINT4  u4Port;
    UINT4  u4ContextId;
    INT4   i4AgingTime;
} tVlanNpWrFsMiVlanHwSetShortAgeout;

typedef struct {
    UINT4  u4Port;
    UINT4  u4ContextId;
    INT4   i4LongAgeout;
} tVlanNpWrFsMiVlanHwResetShortAgeout;

typedef struct {
    UINT4               u4ContextId;
    tHwVlanPortArray *  pHwEntry;
    tVlanId             VlanId;
 UINT1               au1Pad[2];
} tVlanNpWrFsMiVlanHwGetVlanInfo;

typedef struct {
    tHwPortArray *  pHwMcastPorts;
    UINT4           u4ContextId;
    UINT1 *        MacAddr;
    tVlanId         VlanId;
 UINT1           au1Pad[2];
} tVlanNpWrFsMiVlanHwGetMcastEntry;

typedef struct {
    UINT4  u4ContextId;
    INT4   i4CosqValue;
    UINT1  u1Priority;
 UINT1  au1Pad[3];
} tVlanNpWrFsMiVlanHwTraffClassMapInit;

#ifndef BRIDGE_WANTED
typedef struct {
    UINT4  u4ContextId;
    INT4   i4AgingTime;
} tVlanNpWrFsMiBrgSetAgingTime;

#endif /* BRIDGE_WANTED */
typedef struct {
    UINT4  u4ContextId;
    UINT4  u4BridgeMode;
} tVlanNpWrFsMiVlanHwSetBrgMode;

typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4Mode;
} tVlanNpWrFsMiVlanHwSetBaseBridgeMode;

typedef struct {
    UINT4     u4IfIndex;
    UINT4     u4ContextId;
    UINT1 *  MacAddr;
    tVlanId   VlanId;
    BOOL1     bSuppressOption;
 UINT1     au1Pad[1];
} tVlanNpWrFsMiVlanHwAddPortMacVlanEntry;

typedef struct {
    UINT4     u4IfIndex;
    UINT4     u4ContextId;
    UINT1 *  MacAddr;
} tVlanNpWrFsMiVlanHwDeletePortMacVlanEntry;

typedef struct {
    UINT4    u4IfIndex;
    UINT4    u4ContextId;
    UINT4    SubnetAddr;
    tVlanId  VlanId;
    BOOL1    bARPOption;
 UINT1    au1Pad[1];
} tVlanNpWrFsMiVlanHwAddPortSubnetVlanEntry;

typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4ContextId;
    UINT4  SubnetAddr;
} tVlanNpWrFsMiVlanHwDeletePortSubnetVlanEntry;

typedef struct {
    UINT4    u4IfIndex;
    UINT4    u4ContextId;
    UINT4    u4SubnetAddr;
    UINT4    u4SubnetMask;
    tVlanId  VlanId;
    BOOL1    bARPOption;
    UINT1    u1Action;
} tVlanNpWrFsMiVlanHwUpdatePortSubnetVlanEntry;

typedef struct {
    UINT4  u4ContextId;
} tVlanNpWrFsMiVlanNpHwRunMacAgeing;

typedef struct {
    UINT4    u4ContextId;
    tVlanId  VlanId;
    UINT2    u2FdbId;
    UINT4    u4MacLimit;
} tVlanNpWrFsMiVlanHwMacLearningLimit;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4MacLimit;
} tVlanNpWrFsMiVlanHwSwitchMacLearningLimit;

typedef struct {
    tHwPortArray *  pHwEgressPorts;
    UINT4           u4ContextId;
    tVlanId         VlanId;
    UINT2           u2FdbId;
    UINT1           u1Status;
 UINT1           au1Pad[3];
} tVlanNpWrFsMiVlanHwMacLearningStatus;

typedef struct {
    tHwPortArray  HwPortList;
    UINT4         u4Port;
    UINT4         u4ContextId;
    UINT4         u4Fid;
    UINT1         u1Action;
 UINT1         au1Pad[3];
} tVlanNpWrFsMiVlanHwSetFidPortLearningStatus;

typedef struct {
    UINT4                 u4IfIndex;
    UINT4                 u4ContextId;
    tVlanHwTunnelFilters  ProtocolId;
    UINT4                 u4TunnelStatus;
} tVlanNpWrFsMiVlanHwSetProtocolTunnelStatusOnPort;

typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4ContextId;
    INT4   i4ProtectedStatus;
} tVlanNpWrFsMiVlanHwSetPortProtectedStatus;

typedef struct {
    UINT4           u4Port;
    tHwPortArray *  pHwAllowedToGoPorts;
    UINT4           u4ContextId;
    UINT4           u4Fid;
    UINT1 *        MacAddr;
    UINT1 *        ConnectionId;
    UINT1           u1Status;
 UINT1           au1Pad[3];
} tVlanNpWrFsMiVlanHwAddStaticUcastEntryEx;

typedef struct {
    UINT4           u4Port;
    tHwPortArray *  pAllowedToGoPorts;
    UINT4           u4ContextId;
    UINT4           u4Fid;
    UINT1 *        MacAddr;
    UINT1 *         pu1Status;
    UINT1 *        ConnectionId;
} tVlanNpWrFsMiVlanHwGetStaticUcastEntryEx;

typedef struct {
    UINT1 *           pu1Packet;
    tHwVlanFwdInfo *  pVlanFwdInfo;
    UINT2             u2PacketLen;
 UINT1             au1Pad[2];
} tVlanNpWrFsVlanHwForwardPktOnPorts;

typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4ContextId;
    UINT1  u1Status;
    UINT1  au1Pad[3];
} tVlanNpWrFsMiVlanHwPortMacLearningStatus;

typedef struct {
    UINT4 *  pu4LearningMode;
} tVlanNpWrFsVlanHwGetMacLearningMode;

typedef struct {
    tHwMcastIndexInfo  HwMcastIndexInfo;
    UINT4 *            pu4McastIndex;
} tVlanNpWrFsMiVlanHwSetMcastIndex;

typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4ContextId;
    UINT2  u2EtherType;
 UINT1  au1Pad[2];
} tVlanNpWrFsMiVlanHwSetPortIngressEtherType;

typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4ContextId;
    UINT2  u2EtherType;
 UINT1  au1Pad[2];
} tVlanNpWrFsMiVlanHwSetPortEgressEtherType;

typedef struct {
    tHwVlanPortProperty VlanPortProperty;
    UINT4               u4IfIndex;
    UINT4               u4ContextId;
} tVlanNpWrFsMiVlanHwSetPortProperty;

typedef struct {
    UINT4 *  pu4VlanStatsValue;
    UINT2    u2CVlanId;
    UINT2    u2Port;
    UINT1    u1StatsType;
    UINT1    au1Pad[3];
} tVlanNpWrFsMiVlanHwGetCVlanStat;


typedef struct {
    UINT2    u2CVlanId;
    UINT2    u2Port;
} tVlanNpWrFsMiVlanHwClearCVlanStat;


typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4ContextId;
    INT4   i4MacSecType;
} tVlanNpWrFsMiVlanHwPortUnicastMacSecType;

#ifdef MBSM_WANTED
#ifdef PB_WANTED
typedef struct {
    UINT4            u4PortType;
    UINT4            u4IfIndex;
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
} tVlanNpWrFsMiVlanMbsmHwSetProviderBridgePortType;

typedef struct {
    UINT4            u4IfIndex;
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
    UINT1            u1Status;
 UINT1            au1Pad[3];
} tVlanNpWrFsMiVlanMbsmHwSetPortSVlanTranslationStatus;

typedef struct {
    UINT4            u4IfIndex;
    UINT4            u4ContextId;
    UINT2            u2LocalSVlan;
    UINT2            u2RelaySVlan;
    tMbsmSlotInfo *  pSlotInfo;
} tVlanNpWrFsMiVlanMbsmHwAddSVlanTranslationEntry;

typedef struct {
    UINT4            u4IfIndex;
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
    UINT1            u1Status;
 UINT1            au1Pad[3];
} tVlanNpWrFsMiVlanMbsmHwSetPortEtherTypeSwapStatus;

typedef struct {
    UINT4            u4IfIndex;
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
    UINT2            u2LocalEtherType;
    UINT2            u2RelayEtherType;
} tVlanNpWrFsMiVlanMbsmHwAddEtherTypeSwapEntry;

typedef struct {
    UINT4            u4ContextId;
    tVlanSVlanMap    VlanSVlanMap;
    tMbsmSlotInfo *  pSlotInfo;
} tVlanNpWrFsMiVlanMbsmHwAddSVlanMap;

typedef struct {
    UINT4            u4IfIndex;
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
    UINT1            u1TableType;
 UINT1            au1Pad[3];
} tVlanNpWrFsMiVlanMbsmHwSetPortSVlanClassifyMethod;

typedef struct {
    UINT4            u4IfIndex;
    UINT4            u4ContextId;
    UINT4            u4MacLimit;
    tMbsmSlotInfo *  pSlotInfo;
} tVlanNpWrFsMiVlanMbsmHwPortMacLearningLimit;

typedef struct {
    UINT4            u4IfIndex;
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
    tVlanId          CVlanId;
 UINT1            au1Pad[2];
} tVlanNpWrFsMiVlanMbsmHwSetPortCustomerVlan;

typedef struct {
    UINT4             u4IfIndex;
    UINT4             u4ContextId;
    tHwVlanPbPepInfo  PepConfig;
    tMbsmSlotInfo *   pSlotInfo;
    tVlanId           SVlanId;
 UINT1             au1Pad[2];
} tVlanNpWrFsMiVlanMbsmHwCreateProviderEdgePort;

typedef struct {
    UINT4             u4IfIndex;
    UINT4             u4ContextId;
    tHwVlanPbPcpInfo  NpPbVlanPcpInfo;
    tMbsmSlotInfo *   pSlotInfo;
} tVlanNpWrFsMiVlanMbsmHwSetPcpEncodTbl;

typedef struct {
    UINT4             u4IfIndex;
    UINT4             u4ContextId;
    tHwVlanPbPcpInfo  NpPbVlanPcpInfo;
    tMbsmSlotInfo *   pSlotInfo;
} tVlanNpWrFsMiVlanMbsmHwSetPcpDecodTbl;

typedef struct {
    UINT4            u4IfIndex;
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
    UINT1            u1UseDei;
 UINT1            au1Pad[3];
} tVlanNpWrFsMiVlanMbsmHwSetPortUseDei;

typedef struct {
    UINT4            u4IfIndex;
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
    UINT1            u1ReqDrpEncoding;
 UINT1            au1Pad[3];
} tVlanNpWrFsMiVlanMbsmHwSetPortReqDropEncoding;

typedef struct {
    UINT4            u4IfIndex;
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
    UINT2            u2PcpSelection;
 UINT1            au1Pad[2];
} tVlanNpWrFsMiVlanMbsmHwSetPortPcpSelection;

typedef struct {
    UINT4            u4IfIndex;
    UINT4            u4ContextId;
    INT4             i4RecvPriority;
    INT4             i4RegenPriority;
    tMbsmSlotInfo *  pSlotInfo;
    tVlanId          SVlanId;
 UINT1            au1Pad[2];
} tVlanNpWrFsMiVlanMbsmHwSetServicePriRegenEntry;

typedef struct {
    UINT4            u4ContextId;
    UINT4            u4MacLimit;
    tMbsmSlotInfo *  pSlotInfo;
} tVlanNpWrFsMiVlanMbsmHwMulticastMacTableLimit;

#endif /* PB_WANTED */
typedef struct {
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
} tVlanNpWrFsMiVlanMbsmHwInit;

typedef struct {
    UINT4            u4ContextId;
    UINT4            u4BridgeMode;
    tMbsmSlotInfo *  pSlotInfo;
} tVlanNpWrFsMiVlanMbsmHwSetBrgMode;

typedef struct {
    tHwVlanPortProperty  VlanPortProperty;
    UINT4                u4IfIndex;
    UINT4                u4ContextId;
    tMbsmSlotInfo *      pSlotInfo;
} tVlanNpWrFsMiVlanMbsmHwSetPortProperty;

typedef struct {
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
    tVlanId          VlanId;
 UINT1            au1Pad[2];
} tVlanNpWrFsMiVlanMbsmHwSetDefaultVlanId;

typedef struct {
    UINT4            u4RcvPort;
    tHwPortArray *   pHwAllowedToGoPorts;
    UINT4            u4ContextId;
    UINT4            u4FdbId;
    UINT1 *         MacAddr;
    tMbsmSlotInfo *  pSlotInfo;
    UINT1            u1Status;
 UINT1            au1Pad[3];
} tVlanNpWrFsMiVlanMbsmHwAddStaticUcastEntry;

typedef struct {
    UINT4            u4RcvPort;
    UINT4            u4AllowedToGoPort;
    UINT4            u4ContextId;
    UINT4            u4FdbId;
    UINT1 *         MacAddr;
    tMbsmSlotInfo *  pSlotInfo;
    UINT1            u1Status;
 UINT1            au1Pad[3];
} tVlanNpWrFsMiVlanMbsmHwSetUcastPort;

typedef struct {
    UINT4            u4RcvPort;
    UINT4            u4AllowedToGoPort;
    UINT4            u4ContextId;
    UINT4            u4FdbId;
    UINT1 *         MacAddr;
    tMbsmSlotInfo *  pSlotInfo;
} tVlanNpWrFsMiVlanMbsmHwResetUcastPort;

typedef struct {
    UINT4            u4RcvPort;
    UINT4            u4ContextId;
    UINT4            u4Fid;
    UINT1 *         MacAddr;
    tMbsmSlotInfo *  pSlotInfo;
} tVlanNpWrFsMiVlanMbsmHwDelStaticUcastEntry;

typedef struct {
    tHwPortArray *   pHwMcastPorts;
    UINT4            u4ContextId;
    UINT1 *         MacAddr;
    tMbsmSlotInfo *  pSlotInfo;
    tVlanId          VlanId;
 UINT1            au1Pad[2];
} tVlanNpWrFsMiVlanMbsmHwAddMcastEntry;

typedef struct {
    UINT4            u4Port;
    UINT4            u4ContextId;
    UINT1 *         MacAddr;
    tMbsmSlotInfo *  pSlotInfo;
    tVlanId          VlanId;
 UINT1            au1Pad[2];
} tVlanNpWrFsMiVlanMbsmHwSetMcastPort;

typedef struct {
    UINT4            u4Port;
    UINT4            u4ContextId;
    UINT1 *         MacAddr;
    tMbsmSlotInfo *  pSlotInfo;
    tVlanId          VlanId;
 UINT1            au1Pad[2];
} tVlanNpWrFsMiVlanMbsmHwResetMcastPort;

typedef struct {
    tHwPortArray *   pHwEgressPorts;
    tHwPortArray *   pHwUnTagPorts;
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
    tVlanId          VlanId;
 UINT1            au1Pad[2];
} tVlanNpWrFsMiVlanMbsmHwAddVlanEntry;

typedef struct {
    UINT4            u4Port;
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
    tVlanId          VlanId;
    UINT1            u1IsTagged;
 UINT1            au1Pad[1];
} tVlanNpWrFsMiVlanMbsmHwSetVlanMemberPort;

typedef struct {
    UINT4            u4Port;
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
    tVlanId          VlanId;
 UINT1            au1Pad[2];
} tVlanNpWrFsMiVlanMbsmHwResetVlanMemberPort;

typedef struct {
    UINT4            u4Port;
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
    tVlanId          VlanId;
 UINT1            au1Pad[2];
} tVlanNpWrFsMiVlanMbsmHwSetPortPvid;

typedef struct {
    UINT4            u4Port;
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
    UINT1            u1AccFrameType;
 UINT1            au1Pad[3];
} tVlanNpWrFsMiVlanMbsmHwSetPortAccFrameType;

typedef struct {
    UINT4            u4Port;
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
    UINT1            u1IngFilterEnable;
 UINT1            au1Pad[3];
} tVlanNpWrFsMiVlanMbsmHwSetPortIngFiltering;

typedef struct {
    UINT4            u4Port;
    UINT4            u4ContextId;
    INT4             i4DefPriority;
    tMbsmSlotInfo *  pSlotInfo;
} tVlanNpWrFsMiVlanMbsmHwSetDefUserPriority;

typedef struct {
    UINT4            u4ContextId;
    INT4             i4CosqValue;
    tMbsmSlotInfo *  pSlotInfo;
    UINT1            u1Priority;
 UINT1            au1Pad[3];
} tVlanNpWrFsMiVlanMbsmHwTraffClassMapInit;

typedef struct {
    UINT4            u4Port;
    UINT4            u4ContextId;
    INT4             i4UserPriority;
    INT4             i4TraffClass;
    tMbsmSlotInfo *  pSlotInfo;
} tVlanNpWrFsMiVlanMbsmHwSetTraffClassMap;

typedef struct {
    INT4             i4RcvPort;
    tHwPortArray *   pHwMcastPorts;
    UINT4            u4ContextId;
    UINT1 *         McastAddr;
    tMbsmSlotInfo *  pSlotInfo;
    tVlanId          VlanId;
 UINT1            au1Pad[2];
} tVlanNpWrFsMiVlanMbsmHwAddStMcastEntry;

typedef struct {
    UINT4            u4RcvPort;
    UINT4            u4ContextId;
    UINT1 *         MacAddr;
    tMbsmSlotInfo *  pSlotInfo;
    tVlanId          VlanId;
 UINT1            au1Pad[2];
} tVlanNpWrFsMiVlanMbsmHwDelStMcastEntry;

typedef struct {
    UINT4            u4Port;
    UINT4            u4ContextId;
    INT4             i4NumTraffClass;
    tMbsmSlotInfo *  pSlotInfo;
} tVlanNpWrFsMiVlanMbsmHwSetPortNumTrafClasses;

typedef struct {
    UINT4            u4Port;
    UINT4            u4ContextId;
    INT4             i4UserPriority;
    INT4             i4RegenPriority;
    tMbsmSlotInfo *  pSlotInfo;
} tVlanNpWrFsMiVlanMbsmHwSetRegenUserPriority;

typedef struct {
    UINT4            u4Port;
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
    UINT1            u1VlanSubnetEnable;
 UINT1            au1Pad[3];
} tVlanNpWrFsMiVlanMbsmHwSetSubnetBasedStatusOnPort;

typedef struct {
    UINT4            u4Port;
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
    UINT1            u1VlanProtoEnable;
 UINT1            au1Pad[3];
} tVlanNpWrFsMiVlanMbsmHwEnableProtoVlanOnPort;

typedef struct {
    tHwPortArray *   pHwAllGroupPorts;
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
    tVlanId          VlanId;
 UINT1            au1Pad[2];
} tVlanNpWrFsMiVlanMbsmHwSetAllGroupsPorts;

typedef struct {
    tHwPortArray *   pHwUnRegPorts;
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
    tVlanId          VlanId;
 UINT1            au1Pad[2];
} tVlanNpWrFsMiVlanMbsmHwSetUnRegGroupsPorts;

typedef struct {
    UINT4            u4ContextId;
    INT4             i4BridgeMode;
    UINT1 *         MacAddr;
    tMbsmSlotInfo *  pSlotInfo;
    UINT2            u2ProtocolId;
 UINT1            au1Pad[2];
} tVlanNpWrFsMiVlanMbsmHwSetTunnelFilter;

typedef struct {
    UINT4            u4Port;
    UINT4            u4ContextId;
    UINT4            u4SubnetAddr;
    tMbsmSlotInfo *  pSlotInfo;
    tVlanId          VlanId;
    UINT1            u1ArpOption;
 UINT1            au1Pad[1];
} tVlanNpWrFsMiVlanMbsmHwAddPortSubnetVlanEntry;

#ifndef BRIDGE_WANTED
typedef struct {
    UINT4            u4ContextId;
    INT4             i4AgingTime;
    tMbsmSlotInfo *  pSlotInfo;
} tVlanNpWrFsMiBrgMbsmSetAgingTime;

#endif /* BRIDGE_WANTED */
typedef struct {
    UINT4            u4Port;
    UINT4            u4ContextId;
    UINT4            u4Fid;
    tMbsmSlotInfo *  pSlotInfo;
    UINT1            u1Action;
 UINT1            au1Pad[3];
} tVlanNpWrFsMiVlanMbsmHwSetFidPortLearningStatus;

typedef struct {
    UINT4                 u4IfIndex;
    UINT4                 u4ContextId;
    tVlanHwTunnelFilters  ProtocolId;
    UINT4                 u4TunnelStatus;
    tMbsmSlotInfo *       pSlotInfo;
} tVlanNpWrFsMiVlanMbsmHwSetProtocolTunnelStatusOnPort;

typedef struct {
    UINT4            u4IfIndex;
    UINT4            u4ContextId;
    INT4             u4ProtectedStatus;
    tMbsmSlotInfo *  pSlotInfo;
} tVlanNpWrFsMiVlanMbsmHwSetPortProtectedStatus;

typedef struct {
    UINT4            u4ContextId;
    UINT4            u4MacLimit;
    tMbsmSlotInfo *  pSlotInfo;
} tVlanNpWrFsMiVlanMbsmHwSwitchMacLearningLimit;

typedef struct {
    UINT4            u4IfIndex;
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
    UINT1            u1Status;
    UINT1            au1Pad[3];
} tVlanNpWrFsMiVlanMbsmHwPortMacLearningStatus;

typedef struct {
    UINT4            u4ContextId;
    tVlanId          VlanId;
    UINT2            u2FdbId;
    UINT4            u4MacLimit;
    tMbsmSlotInfo *  pSlotInfo;
} tVlanNpWrFsMiVlanMbsmHwMacLearningLimit;

typedef struct {
    UINT4            u4RcvPort;
    tHwPortArray *   pHwAllowedToGoPorts;
    UINT4            u4ContextId;
    UINT4            u4FdbId;
    UINT1 *         MacAddr;
    UINT1 *         ConnectionId;
    tMbsmSlotInfo *  pSlotInfo;
    UINT1            u1Status;
 UINT1            au1Pad[3];
} tVlanNpWrFsMiVlanMbsmHwAddStaticUcastEntryEx;

typedef struct {
    tFDBInfoArray *  pFDBInfoArray;
    tMbsmSlotInfo *  pSlotInfo;
} tVlanNpWrFsMiVlanMbsmSyncFDBInfo;

typedef struct {
    UINT4            u4IfIndex;
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
    UINT2            u2EtherType;
 UINT1            au1Pad[2];
} tVlanNpWrFsMiVlanMbsmHwSetPortIngressEtherType;

typedef struct {
    UINT4            u4IfIndex;
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
    UINT2            u2EtherType;
 UINT1            au1Pad[2];
} tVlanNpWrFsMiVlanMbsmHwSetPortEgressEtherType;

#endif /* MBSM_WANTED */ 
#ifdef PB_WANTED
typedef struct {
    UINT4  u4PortType;
    UINT4  u4IfIndex;
    UINT4  u4ContextId;
} tVlanNpWrFsMiVlanHwSetProviderBridgePortType;

typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4ContextId;
    UINT1  u1Status;
 UINT1  au1Pad[3];
} tVlanNpWrFsMiVlanHwSetPortSVlanTranslationStatus;

typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4ContextId;
    UINT2  u2LocalSVlan;
    UINT2  u2RelaySVlan;
} tVlanNpWrFsMiVlanHwAddSVlanTranslationEntry;

typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4ContextId;
    UINT2  u2LocalSVlan;
    UINT2  u2RelaySVlan;
} tVlanNpWrFsMiVlanHwDelSVlanTranslationEntry;

typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4ContextId;
    UINT1  u1Status;
 UINT1  au1Pad[3];
} tVlanNpWrFsMiVlanHwSetPortEtherTypeSwapStatus;

typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4ContextId;
    UINT2  u2LocalEtherType;
    UINT2  u2RelayEtherType;
} tVlanNpWrFsMiVlanHwAddEtherTypeSwapEntry;

typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4ContextId;
    UINT2  u2LocalEtherType;
    UINT2  u2RelayEtherType;
} tVlanNpWrFsMiVlanHwDelEtherTypeSwapEntry;

typedef struct {
    UINT4          u4ContextId;
    tVlanSVlanMap  VlanSVlanMap;
} tVlanNpWrFsMiVlanHwAddSVlanMap;

typedef struct {
    tHwVlanCVlanStat  VlanStat;
} tVlanNpWrFsMiVlanHwSetCVlanStat;


typedef struct {
    UINT4          u4ContextId;
    tVlanSVlanMap  VlanSVlanMap;
} tVlanNpWrFsMiVlanHwDeleteSVlanMap;

typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4ContextId;
    UINT1  u1TableType;
 UINT1  au1Pad[3];
} tVlanNpWrFsMiVlanHwSetPortSVlanClassifyMethod;

typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4ContextId;
    UINT4  u4MacLimit;
} tVlanNpWrFsMiVlanHwPortMacLearningLimit;


typedef struct {
    UINT4  u4ContextId;
    UINT4  u4MacLimit;
} tVlanNpWrFsMiVlanHwMulticastMacTableLimit;

typedef struct {
    UINT4    u4IfIndex;
    UINT4    u4ContextId;
    tVlanId  CVlanId;
 UINT1    au1Pad[2];
} tVlanNpWrFsMiVlanHwSetPortCustomerVlan;

typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4ContextId;
} tVlanNpWrFsMiVlanHwResetPortCustomerVlan;

typedef struct {
    UINT4             u4IfIndex;
    UINT4             u4ContextId;
    tHwVlanPbPepInfo  PepConfig;
    tVlanId           SVlanId;
 UINT1             au1Pad[2];
} tVlanNpWrFsMiVlanHwCreateProviderEdgePort;

typedef struct {
    UINT4    u4IfIndex;
    UINT4    u4ContextId;
    tVlanId  SVlanId;
 UINT1    au1Pad[2];
} tVlanNpWrFsMiVlanHwDelProviderEdgePort;

typedef struct {
    UINT4    u4IfIndex;
    UINT4    u4ContextId;
    tVlanId  SVlanId;
    tVlanId  Pvid;
} tVlanNpWrFsMiVlanHwSetPepPvid;

typedef struct {
    UINT4    u4IfIndex;
    UINT4    u4ContextId;
    tVlanId  SVlanId;
    UINT1    u1AccepFrameType;
 UINT1    au1Pad[1];
} tVlanNpWrFsMiVlanHwSetPepAccFrameType;

typedef struct {
    UINT4    u4IfIndex;
    UINT4    u4ContextId;
    INT4     i4DefUsrPri;
    tVlanId  SVlanId;
 UINT1    au1Pad[2];
} tVlanNpWrFsMiVlanHwSetPepDefUserPriority;

typedef struct {
    UINT4    u4IfIndex;
    UINT4    u4ContextId;
    tVlanId  SVlanId;
    UINT1    u1IngFilterEnable;
    UINT1    au1Pad[1];
} tVlanNpWrFsMiVlanHwSetPepIngFiltering;

typedef struct {
    UINT4          u4ContextId;
    tVlanSVlanMap  VlanSVlanMap;
} tVlanNpWrFsMiVlanHwSetCvidUntagPep;

typedef struct {
    UINT4          u4ContextId;
    tVlanSVlanMap  VlanSVlanMap;
} tVlanNpWrFsMiVlanHwSetCvidUntagCep;

typedef struct {
    UINT4             u4IfIndex;
    UINT4             u4ContextId;
    tHwVlanPbPcpInfo  NpPbVlanPcpInfo;
} tVlanNpWrFsMiVlanHwSetPcpEncodTbl;

typedef struct {
    UINT4             u4IfIndex;
    UINT4             u4ContextId;
    tHwVlanPbPcpInfo  NpPbVlanPcpInfo;
} tVlanNpWrFsMiVlanHwSetPcpDecodTbl;

typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4ContextId;
    UINT1  u1UseDei;
 UINT1  au1Pad[3];
} tVlanNpWrFsMiVlanHwSetPortUseDei;

typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4ContextId;
    UINT1  u1ReqDrpEncoding;
 UINT1  au1Pad[3];
} tVlanNpWrFsMiVlanHwSetPortReqDropEncoding;

typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4ContextId;
    UINT2  u2PcpSelection;
 UINT1  au1Pad[2];
} tVlanNpWrFsMiVlanHwSetPortPcpSelection;

typedef struct {
    UINT4    u4IfIndex;
    UINT4    u4ContextId;
    INT4     i4RecvPriority;
    INT4     i4RegenPriority;
    tVlanId  SVlanId;
 UINT1    au1Pad[2];
} tVlanNpWrFsMiVlanHwSetServicePriRegenEntry;

typedef struct {
    UINT4     u4ContextId;
    UINT1 *  MacAddr;
    UINT2     u2Protocol;
 UINT1     au1Pad[2];
} tVlanNpWrFsMiVlanHwSetTunnelMacAddress;

typedef struct {
    UINT4            u4ContextId;
    tVlanSVlanMap    VlanSVlanMap;
    tVlanSVlanMap *  pRetVlanSVlanMap;
} tVlanNpWrFsMiVlanHwGetNextSVlanMap;

typedef struct {
    UINT4                 u4IfIndex;
    UINT4                 u4ContextId;
    tVidTransEntryInfo *  pVidTransEntryInfo;
    tVlanId               u2LocalSVlan;
 UINT1                 au1Pad[2];
} tVlanNpWrFsMiVlanHwGetNextSVlanTranslationEntry;


typedef struct {
    INT4                  i4FsEvcContextId;
    UINT4                 u4Action;
    tEvcInfo            *pIssEvcInfo;
} tVlanNpWrFsMiVlanHwSetEvcAttribute;

#endif /* PB_WANTED */

typedef struct {
    UINT4       u4ContextId;
    INT4        i4LoopbackStatus;
    tVlanId     VlanId;
    UINT1  au1Pad[2];
} tVlanNpWrFsMiVlanHwSetVlanLoopbackStatus;

typedef struct {
    tVlanEvbHwConfigInfo  VlanEvbHwConfigInfo;
} tVlanNpWrFsMiVlanHwEvbConfigSChIface;

typedef struct {
     tVlanHwPortInfo  VlanHwPortInfo;
} tVlanNpWrFsMiVlanHwSetBridgePortType;


typedef struct {
    tVlanEvbHwConfigInfo  VlanEvbHwConfigInfo;
    tMbsmSlotInfo         *pSlotInfo;
} tVlanNpWrFsMiVlanMbsmHwEvbConfigSChIface;

typedef struct {
    tVlanHwPortInfo  VlanHwPortInfo;
    tMbsmSlotInfo    *pSlotInfo;
}tVlanNpWrFsMiVlanMbsmHwSetBridgePortType;

typedef struct VlanNpModInfo {
union {
    tVlanNpWrFsMiVlanHwInit  sFsMiVlanHwInit;
    tVlanNpWrFsMiVlanHwDeInit  sFsMiVlanHwDeInit;
    tVlanNpWrFsMiVlanHwSetAllGroupsPorts  sFsMiVlanHwSetAllGroupsPorts;
    tVlanNpWrFsMiVlanHwResetAllGroupsPorts  sFsMiVlanHwResetAllGroupsPorts;
    tVlanNpWrFsMiVlanHwResetUnRegGroupsPorts  sFsMiVlanHwResetUnRegGroupsPorts;
    tVlanNpWrFsMiVlanHwSetUnRegGroupsPorts  sFsMiVlanHwSetUnRegGroupsPorts;
    tVlanNpWrFsMiVlanHwAddStaticUcastEntry  sFsMiVlanHwAddStaticUcastEntry;
    tVlanNpWrFsMiVlanHwDelStaticUcastEntry  sFsMiVlanHwDelStaticUcastEntry;
    tVlanNpWrFsMiVlanHwGetFdbEntry  sFsMiVlanHwGetFdbEntry;
#ifndef SW_LEARNING
    tVlanNpWrFsMiVlanHwGetFdbCount  sFsMiVlanHwGetFdbCount;
    tVlanNpWrFsMiVlanHwGetFirstTpFdbEntry  sFsMiVlanHwGetFirstTpFdbEntry;
    tVlanNpWrFsMiVlanHwGetNextTpFdbEntry  sFsMiVlanHwGetNextTpFdbEntry;
#endif /* SW_LEARNING */
    tVlanNpWrFsMiVlanHwAddMcastEntry  sFsMiVlanHwAddMcastEntry;
    tVlanNpWrFsMiVlanHwAddStMcastEntry  sFsMiVlanHwAddStMcastEntry;
    tVlanNpWrFsMiVlanHwSetMcastPort  sFsMiVlanHwSetMcastPort;
    tVlanNpWrFsMiVlanHwResetMcastPort  sFsMiVlanHwResetMcastPort;
    tVlanNpWrFsMiVlanHwDelMcastEntry  sFsMiVlanHwDelMcastEntry;
    tVlanNpWrFsMiVlanHwDelStMcastEntry  sFsMiVlanHwDelStMcastEntry;
    tVlanNpWrFsMiVlanHwAddVlanEntry  sFsMiVlanHwAddVlanEntry;
    tVlanNpWrFsMiVlanHwDelVlanEntry  sFsMiVlanHwDelVlanEntry;
    tVlanNpWrFsMiVlanHwSetVlanMemberPort  sFsMiVlanHwSetVlanMemberPort;
    tVlanNpWrFsMiVlanHwResetVlanMemberPort  sFsMiVlanHwResetVlanMemberPort;
    tVlanNpWrFsMiVlanHwSetPortPvid  sFsMiVlanHwSetPortPvid;
    tVlanNpWrFsMiVlanHwSetDefaultVlanId  sFsMiVlanHwSetDefaultVlanId;
#ifdef L2RED_WANTED
    tVlanNpWrFsMiVlanHwSyncDefaultVlanId  sFsMiVlanHwSyncDefaultVlanId;
    tVlanNpWrFsMiVlanRedHwUpdateDBForDefaultVlanId  sFsMiVlanRedHwUpdateDBForDefaultVlanId;
    tVlanNpWrFsMiVlanHwScanProtocolVlanTbl  sFsMiVlanHwScanProtocolVlanTbl;
    tVlanNpWrFsMiVlanHwGetVlanProtocolMap  sFsMiVlanHwGetVlanProtocolMap;
    tVlanNpWrFsMiVlanHwScanMulticastTbl  sFsMiVlanHwScanMulticastTbl;
    tVlanNpWrFsMiVlanHwGetStMcastEntry  sFsMiVlanHwGetStMcastEntry;
    tVlanNpWrFsMiVlanHwScanUnicastTbl  sFsMiVlanHwScanUnicastTbl;
    tVlanNpWrFsMiVlanHwGetStaticUcastEntry  sFsMiVlanHwGetStaticUcastEntry;
    tVlanNpWrFsMiVlanHwGetSyncedTnlProtocolMacAddr  sFsMiVlanHwGetSyncedTnlProtocolMacAddr;
#endif /* L2RED_WANTED */
    tVlanNpWrFsMiVlanHwSetPortAccFrameType  sFsMiVlanHwSetPortAccFrameType;
    tVlanNpWrFsMiVlanHwSetPortIngFiltering  sFsMiVlanHwSetPortIngFiltering;
    tVlanNpWrFsMiVlanHwVlanEnable  sFsMiVlanHwVlanEnable;
    tVlanNpWrFsMiVlanHwVlanDisable  sFsMiVlanHwVlanDisable;
    tVlanNpWrFsMiVlanHwSetVlanLearningType  sFsMiVlanHwSetVlanLearningType;
    tVlanNpWrFsMiVlanHwSetMacBasedStatusOnPort  sFsMiVlanHwSetMacBasedStatusOnPort;
    tVlanNpWrFsMiVlanHwSetSubnetBasedStatusOnPort  sFsMiVlanHwSetSubnetBasedStatusOnPort;
    tVlanNpWrFsMiVlanHwEnableProtoVlanOnPort  sFsMiVlanHwEnableProtoVlanOnPort;
    tVlanNpWrFsMiVlanHwSetDefUserPriority  sFsMiVlanHwSetDefUserPriority;
    tVlanNpWrFsMiVlanHwSetPortNumTrafClasses  sFsMiVlanHwSetPortNumTrafClasses;
    tVlanNpWrFsMiVlanHwSetRegenUserPriority  sFsMiVlanHwSetRegenUserPriority;
    tVlanNpWrFsMiVlanHwSetTraffClassMap  sFsMiVlanHwSetTraffClassMap;
    tVlanNpWrFsMiVlanHwGmrpEnable  sFsMiVlanHwGmrpEnable;
    tVlanNpWrFsMiVlanHwGmrpDisable  sFsMiVlanHwGmrpDisable;
    tVlanNpWrFsMiVlanHwGvrpEnable  sFsMiVlanHwGvrpEnable;
    tVlanNpWrFsMiVlanHwGvrpDisable  sFsMiVlanHwGvrpDisable;
    tVlanNpWrFsMiNpDeleteAllFdbEntries  sFsMiNpDeleteAllFdbEntries;
    tVlanNpWrFsMiVlanHwAddVlanProtocolMap  sFsMiVlanHwAddVlanProtocolMap;
    tVlanNpWrFsMiVlanHwDelVlanProtocolMap  sFsMiVlanHwDelVlanProtocolMap;
    tVlanNpWrFsMiVlanHwGetPortStats  sFsMiVlanHwGetPortStats;
    tVlanNpWrFsMiVlanHwGetPortStats64  sFsMiVlanHwGetPortStats64;
    tVlanNpWrFsMiVlanHwGetVlanStats  sFsMiVlanHwGetVlanStats;
    tVlanNpWrFsMiVlanHwResetVlanStats  sFsMiVlanHwResetVlanStats;
    tVlanNpWrFsMiVlanHwSetPortTunnelMode  sFsMiVlanHwSetPortTunnelMode;
    tVlanNpWrFsMiVlanHwSetTunnelFilter  sFsMiVlanHwSetTunnelFilter;
    tVlanNpWrFsMiVlanHwCheckTagAtEgress  sFsMiVlanHwCheckTagAtEgress;
    tVlanNpWrFsMiVlanHwCheckTagAtIngress  sFsMiVlanHwCheckTagAtIngress;
    tVlanNpWrFsMiVlanHwCreateFdbId  sFsMiVlanHwCreateFdbId;
    tVlanNpWrFsMiVlanHwDeleteFdbId  sFsMiVlanHwDeleteFdbId;
    tVlanNpWrFsMiVlanHwAssociateVlanFdb  sFsMiVlanHwAssociateVlanFdb;
    tVlanNpWrFsMiVlanHwDisassociateVlanFdb  sFsMiVlanHwDisassociateVlanFdb;
    tVlanNpWrFsMiVlanHwFlushPortFdbId  sFsMiVlanHwFlushPortFdbId;
    tVlanNpWrFsMiVlanHwFlushPortFdbList  sFsMiVlanHwFlushPortFdbList;
    tVlanNpWrFsMiVlanHwFlushPort  sFsMiVlanHwFlushPort;
    tVlanNpWrFsMiVlanHwFlushFdbId  sFsMiVlanHwFlushFdbId;
    tVlanNpWrFsMiVlanHwSetShortAgeout  sFsMiVlanHwSetShortAgeout;
    tVlanNpWrFsMiVlanHwResetShortAgeout  sFsMiVlanHwResetShortAgeout;
    tVlanNpWrFsMiVlanHwGetVlanInfo  sFsMiVlanHwGetVlanInfo;
    tVlanNpWrFsMiVlanHwGetMcastEntry  sFsMiVlanHwGetMcastEntry;
    tVlanNpWrFsMiVlanHwTraffClassMapInit  sFsMiVlanHwTraffClassMapInit;
#ifndef BRIDGE_WANTED
    tVlanNpWrFsMiBrgSetAgingTime  sFsMiBrgSetAgingTime;
#endif /* BRIDGE_WANTED */
    tVlanNpWrFsMiVlanHwSetBrgMode  sFsMiVlanHwSetBrgMode;
    tVlanNpWrFsMiVlanHwSetBaseBridgeMode  sFsMiVlanHwSetBaseBridgeMode;
    tVlanNpWrFsMiVlanHwAddPortMacVlanEntry  sFsMiVlanHwAddPortMacVlanEntry;
    tVlanNpWrFsMiVlanHwDeletePortMacVlanEntry  sFsMiVlanHwDeletePortMacVlanEntry;
    tVlanNpWrFsMiVlanHwAddPortSubnetVlanEntry  sFsMiVlanHwAddPortSubnetVlanEntry;
    tVlanNpWrFsMiVlanHwDeletePortSubnetVlanEntry  sFsMiVlanHwDeletePortSubnetVlanEntry;
    tVlanNpWrFsMiVlanHwUpdatePortSubnetVlanEntry  sFsMiVlanHwUpdatePortSubnetVlanEntry;
    tVlanNpWrFsMiVlanNpHwRunMacAgeing  sFsMiVlanNpHwRunMacAgeing;
    tVlanNpWrFsMiVlanHwMacLearningLimit  sFsMiVlanHwMacLearningLimit;
    tVlanNpWrFsMiVlanHwSwitchMacLearningLimit  sFsMiVlanHwSwitchMacLearningLimit;
    tVlanNpWrFsMiVlanHwMacLearningStatus  sFsMiVlanHwMacLearningStatus;
    tVlanNpWrFsMiVlanHwSetFidPortLearningStatus  sFsMiVlanHwSetFidPortLearningStatus;
    tVlanNpWrFsMiVlanHwSetProtocolTunnelStatusOnPort  sFsMiVlanHwSetProtocolTunnelStatusOnPort;
    tVlanNpWrFsMiVlanHwSetPortProtectedStatus  sFsMiVlanHwSetPortProtectedStatus;
    tVlanNpWrFsMiVlanHwAddStaticUcastEntryEx  sFsMiVlanHwAddStaticUcastEntryEx;
    tVlanNpWrFsMiVlanHwGetStaticUcastEntryEx  sFsMiVlanHwGetStaticUcastEntryEx;
    tVlanNpWrFsVlanHwForwardPktOnPorts  sFsVlanHwForwardPktOnPorts;
    tVlanNpWrFsMiVlanHwPortMacLearningStatus  sFsMiVlanHwPortMacLearningStatus;
    tVlanNpWrFsVlanHwGetMacLearningMode  sFsVlanHwGetMacLearningMode;
    tVlanNpWrFsMiVlanHwSetMcastIndex  sFsMiVlanHwSetMcastIndex;
#ifdef MBSM_WANTED
#ifdef PB_WANTED
    tVlanNpWrFsMiVlanMbsmHwSetProviderBridgePortType  sFsMiVlanMbsmHwSetProviderBridgePortType;
    tVlanNpWrFsMiVlanMbsmHwSetPortSVlanTranslationStatus  sFsMiVlanMbsmHwSetPortSVlanTranslationStatus;
    tVlanNpWrFsMiVlanMbsmHwAddSVlanTranslationEntry  sFsMiVlanMbsmHwAddSVlanTranslationEntry;
    tVlanNpWrFsMiVlanMbsmHwSetPortEtherTypeSwapStatus  sFsMiVlanMbsmHwSetPortEtherTypeSwapStatus;
    tVlanNpWrFsMiVlanMbsmHwAddEtherTypeSwapEntry  sFsMiVlanMbsmHwAddEtherTypeSwapEntry;
    tVlanNpWrFsMiVlanMbsmHwAddSVlanMap  sFsMiVlanMbsmHwAddSVlanMap;
    tVlanNpWrFsMiVlanMbsmHwSetPortSVlanClassifyMethod  sFsMiVlanMbsmHwSetPortSVlanClassifyMethod;
    tVlanNpWrFsMiVlanMbsmHwPortMacLearningLimit  sFsMiVlanMbsmHwPortMacLearningLimit;
    tVlanNpWrFsMiVlanMbsmHwSetPortCustomerVlan  sFsMiVlanMbsmHwSetPortCustomerVlan;
    tVlanNpWrFsMiVlanMbsmHwCreateProviderEdgePort  sFsMiVlanMbsmHwCreateProviderEdgePort;
    tVlanNpWrFsMiVlanMbsmHwSetPcpEncodTbl  sFsMiVlanMbsmHwSetPcpEncodTbl;
    tVlanNpWrFsMiVlanMbsmHwSetPcpDecodTbl  sFsMiVlanMbsmHwSetPcpDecodTbl;
    tVlanNpWrFsMiVlanMbsmHwSetPortUseDei  sFsMiVlanMbsmHwSetPortUseDei;
    tVlanNpWrFsMiVlanMbsmHwSetPortReqDropEncoding  sFsMiVlanMbsmHwSetPortReqDropEncoding;
    tVlanNpWrFsMiVlanMbsmHwSetPortPcpSelection  sFsMiVlanMbsmHwSetPortPcpSelection;
    tVlanNpWrFsMiVlanMbsmHwSetServicePriRegenEntry  sFsMiVlanMbsmHwSetServicePriRegenEntry;
    tVlanNpWrFsMiVlanMbsmHwMulticastMacTableLimit  sFsMiVlanMbsmHwMulticastMacTableLimit;
#endif /* PB_WANTED */
    tVlanNpWrFsMiVlanMbsmHwInit  sFsMiVlanMbsmHwInit;
    tVlanNpWrFsMiVlanMbsmHwSetBrgMode  sFsMiVlanMbsmHwSetBrgMode;
    tVlanNpWrFsMiVlanMbsmHwSetDefaultVlanId  sFsMiVlanMbsmHwSetDefaultVlanId;
    tVlanNpWrFsMiVlanMbsmHwAddStaticUcastEntry  sFsMiVlanMbsmHwAddStaticUcastEntry;
    tVlanNpWrFsMiVlanMbsmHwSetUcastPort  sFsMiVlanMbsmHwSetUcastPort;
    tVlanNpWrFsMiVlanMbsmHwResetUcastPort  sFsMiVlanMbsmHwResetUcastPort;
    tVlanNpWrFsMiVlanMbsmHwDelStaticUcastEntry  sFsMiVlanMbsmHwDelStaticUcastEntry;
    tVlanNpWrFsMiVlanMbsmHwAddMcastEntry  sFsMiVlanMbsmHwAddMcastEntry;
    tVlanNpWrFsMiVlanMbsmHwSetMcastPort  sFsMiVlanMbsmHwSetMcastPort;
    tVlanNpWrFsMiVlanMbsmHwResetMcastPort  sFsMiVlanMbsmHwResetMcastPort;
    tVlanNpWrFsMiVlanMbsmHwAddVlanEntry  sFsMiVlanMbsmHwAddVlanEntry;
    tVlanNpWrFsMiVlanMbsmHwSetVlanMemberPort  sFsMiVlanMbsmHwSetVlanMemberPort;
    tVlanNpWrFsMiVlanMbsmHwResetVlanMemberPort  sFsMiVlanMbsmHwResetVlanMemberPort;
    tVlanNpWrFsMiVlanMbsmHwSetPortPvid  sFsMiVlanMbsmHwSetPortPvid;
    tVlanNpWrFsMiVlanMbsmHwSetPortAccFrameType  sFsMiVlanMbsmHwSetPortAccFrameType;
    tVlanNpWrFsMiVlanMbsmHwSetPortIngFiltering  sFsMiVlanMbsmHwSetPortIngFiltering;
    tVlanNpWrFsMiVlanMbsmHwSetDefUserPriority  sFsMiVlanMbsmHwSetDefUserPriority;
    tVlanNpWrFsMiVlanMbsmHwTraffClassMapInit  sFsMiVlanMbsmHwTraffClassMapInit;
    tVlanNpWrFsMiVlanMbsmHwSetTraffClassMap  sFsMiVlanMbsmHwSetTraffClassMap;
    tVlanNpWrFsMiVlanMbsmHwAddStMcastEntry  sFsMiVlanMbsmHwAddStMcastEntry;
    tVlanNpWrFsMiVlanMbsmHwDelStMcastEntry  sFsMiVlanMbsmHwDelStMcastEntry;
    tVlanNpWrFsMiVlanMbsmHwSetPortNumTrafClasses  sFsMiVlanMbsmHwSetPortNumTrafClasses;
    tVlanNpWrFsMiVlanMbsmHwSetRegenUserPriority  sFsMiVlanMbsmHwSetRegenUserPriority;
    tVlanNpWrFsMiVlanMbsmHwSetSubnetBasedStatusOnPort  sFsMiVlanMbsmHwSetSubnetBasedStatusOnPort;
    tVlanNpWrFsMiVlanMbsmHwEnableProtoVlanOnPort  sFsMiVlanMbsmHwEnableProtoVlanOnPort;
    tVlanNpWrFsMiVlanMbsmHwSetAllGroupsPorts  sFsMiVlanMbsmHwSetAllGroupsPorts;
    tVlanNpWrFsMiVlanMbsmHwSetUnRegGroupsPorts  sFsMiVlanMbsmHwSetUnRegGroupsPorts;
    tVlanNpWrFsMiVlanMbsmHwSetTunnelFilter  sFsMiVlanMbsmHwSetTunnelFilter;
    tVlanNpWrFsMiVlanMbsmHwAddPortSubnetVlanEntry  sFsMiVlanMbsmHwAddPortSubnetVlanEntry;
    tVlanNpWrFsMiVlanMbsmHwSetPortProperty  sFsMiVlanMbsmHwSetPortProperty;
    tVlanNpWrFsMiVlanMbsmHwPortPktReflectStatus  sFsMiVlanMbsmHwPortPktReflectStatus;
#ifndef BRIDGE_WANTED
    tVlanNpWrFsMiBrgMbsmSetAgingTime  sFsMiBrgMbsmSetAgingTime;
#endif /* BRIDGE_WANTED */
    tVlanNpWrFsMiVlanMbsmHwSetFidPortLearningStatus  sFsMiVlanMbsmHwSetFidPortLearningStatus;
    tVlanNpWrFsMiVlanMbsmHwSetProtocolTunnelStatusOnPort  sFsMiVlanMbsmHwSetProtocolTunnelStatusOnPort;
    tVlanNpWrFsMiVlanMbsmHwSetPortProtectedStatus  sFsMiVlanMbsmHwSetPortProtectedStatus;
    tVlanNpWrFsMiVlanMbsmHwSwitchMacLearningLimit  sFsMiVlanMbsmHwSwitchMacLearningLimit;
    tVlanNpWrFsMiVlanMbsmHwPortMacLearningStatus  sFsMiVlanMbsmHwPortMacLearningStatus;
    tVlanNpWrFsMiVlanMbsmHwMacLearningLimit  sFsMiVlanMbsmHwMacLearningLimit;
    tVlanNpWrFsMiVlanMbsmHwAddStaticUcastEntryEx  sFsMiVlanMbsmHwAddStaticUcastEntryEx;
    tVlanNpWrFsMiVlanMbsmSyncFDBInfo  sFsMiVlanMbsmSyncFDBInfo;
    tVlanNpWrFsMiVlanMbsmHwSetPortIngressEtherType  sFsMiVlanMbsmHwSetPortIngressEtherType;
    tVlanNpWrFsMiVlanMbsmHwSetPortEgressEtherType  sFsMiVlanMbsmHwSetPortEgressEtherType;
#endif /* MBSM_WANTED */ 
#ifdef PB_WANTED
    tVlanNpWrFsMiVlanHwSetProviderBridgePortType  sFsMiVlanHwSetProviderBridgePortType;
    tVlanNpWrFsMiVlanHwSetPortSVlanTranslationStatus  sFsMiVlanHwSetPortSVlanTranslationStatus;
    tVlanNpWrFsMiVlanHwAddSVlanTranslationEntry  sFsMiVlanHwAddSVlanTranslationEntry;
    tVlanNpWrFsMiVlanHwDelSVlanTranslationEntry  sFsMiVlanHwDelSVlanTranslationEntry;
    tVlanNpWrFsMiVlanHwSetPortEtherTypeSwapStatus  sFsMiVlanHwSetPortEtherTypeSwapStatus;
    tVlanNpWrFsMiVlanHwAddEtherTypeSwapEntry  sFsMiVlanHwAddEtherTypeSwapEntry;
    tVlanNpWrFsMiVlanHwDelEtherTypeSwapEntry  sFsMiVlanHwDelEtherTypeSwapEntry;
    tVlanNpWrFsMiVlanHwAddSVlanMap  sFsMiVlanHwAddSVlanMap;
    tVlanNpWrFsMiVlanHwDeleteSVlanMap  sFsMiVlanHwDeleteSVlanMap;
    tVlanNpWrFsMiVlanHwSetPortSVlanClassifyMethod  sFsMiVlanHwSetPortSVlanClassifyMethod;
    tVlanNpWrFsMiVlanHwPortMacLearningLimit  sFsMiVlanHwPortMacLearningLimit;
    tVlanNpWrFsMiVlanHwMulticastMacTableLimit  sFsMiVlanHwMulticastMacTableLimit;
    tVlanNpWrFsMiVlanHwSetPortCustomerVlan  sFsMiVlanHwSetPortCustomerVlan;
    tVlanNpWrFsMiVlanHwResetPortCustomerVlan  sFsMiVlanHwResetPortCustomerVlan;
    tVlanNpWrFsMiVlanHwCreateProviderEdgePort  sFsMiVlanHwCreateProviderEdgePort;
    tVlanNpWrFsMiVlanHwDelProviderEdgePort  sFsMiVlanHwDelProviderEdgePort;
    tVlanNpWrFsMiVlanHwSetPepPvid  sFsMiVlanHwSetPepPvid;
    tVlanNpWrFsMiVlanHwSetPepAccFrameType  sFsMiVlanHwSetPepAccFrameType;
    tVlanNpWrFsMiVlanHwSetPepDefUserPriority  sFsMiVlanHwSetPepDefUserPriority;
    tVlanNpWrFsMiVlanHwSetPepIngFiltering  sFsMiVlanHwSetPepIngFiltering;
    tVlanNpWrFsMiVlanHwSetCvidUntagPep  sFsMiVlanHwSetCvidUntagPep;
    tVlanNpWrFsMiVlanHwSetCvidUntagCep  sFsMiVlanHwSetCvidUntagCep;
    tVlanNpWrFsMiVlanHwSetPcpEncodTbl  sFsMiVlanHwSetPcpEncodTbl;
    tVlanNpWrFsMiVlanHwSetPcpDecodTbl  sFsMiVlanHwSetPcpDecodTbl;
    tVlanNpWrFsMiVlanHwSetPortUseDei  sFsMiVlanHwSetPortUseDei;
    tVlanNpWrFsMiVlanHwSetPortReqDropEncoding  sFsMiVlanHwSetPortReqDropEncoding;
    tVlanNpWrFsMiVlanHwSetPortPcpSelection  sFsMiVlanHwSetPortPcpSelection;
    tVlanNpWrFsMiVlanHwSetServicePriRegenEntry  sFsMiVlanHwSetServicePriRegenEntry;
    tVlanNpWrFsMiVlanHwSetTunnelMacAddress  sFsMiVlanHwSetTunnelMacAddress;
    tVlanNpWrFsMiVlanHwGetNextSVlanMap  sFsMiVlanHwGetNextSVlanMap;
    tVlanNpWrFsMiVlanHwGetNextSVlanTranslationEntry  sFsMiVlanHwGetNextSVlanTranslationEntry;
    tVlanNpWrFsMiVlanHwSetEvcAttribute  sFsMiVlanHwSetEvcAttribute;
    tVlanNpWrFsMiVlanHwSetCVlanStat       sFsMiVlanHwSetCVlanStat;
    tVlanNpWrFsMiVlanHwGetCVlanStat       sFsMiVlanHwGetCVlanStat;
    tVlanNpWrFsMiVlanHwClearCVlanStat       sFsMiVlanHwClearCVlanStat;
#endif /* PB_WANTED */
    tVlanNpWrFsMiVlanHwSetPortProperty  sFsMiVlanHwSetPortProperty;
    tVlanNpWrFsMiVlanHwSetPortIngressEtherType  sFsMiVlanHwSetPortIngressEtherType;
    tVlanNpWrFsMiVlanHwPortUnicastMacSecType sFsMiVlanHwPortUnicastMacSecType;
    tVlanNpWrFsMiVlanHwSetPortEgressEtherType  sFsMiVlanHwSetPortEgressEtherType;
    tVlanNpWrFsMiVlanHwSetVlanLoopbackStatus  sFsMiVlanHwSetVlanLoopbackStatus;
    tVlanNpWrFsMiVlanHwEvbConfigSChIface  sFsMiVlanHwEvbConfigSChIface;
    tVlanNpWrFsMiVlanMbsmHwEvbConfigSChIface  sFsMiVlanMbsmHwEvbConfigSChIface;
    tVlanNpWrFsNpHwGetPortFromFdb  sFsNpHwGetPortFromFdb;
    tVlanNpWrFsMiVlanHwPortPktReflectStatus  sFsMiVlanHwPortPktReflectStatus;
    tVlanNpWrFsMiVlanHwSetBridgePortType    sFsMiVlanHwSetBridgePortType;
    tVlanNpWrFsMiVlanMbsmHwSetBridgePortType sFsMiVlanMbsmHwSetBridgePortType;
    tNpwFsMiVlanMbsmHwPortPktReflectStatus FsMiVlanMbsmHwPortPktReflectStatus;
 
    }unOpCode;

#define  VlanNpFsMiVlanHwInit  unOpCode.sFsMiVlanHwInit;
#define  VlanNpFsMiVlanHwDeInit  unOpCode.sFsMiVlanHwDeInit;
#define  VlanNpFsMiVlanHwSetAllGroupsPorts  unOpCode.sFsMiVlanHwSetAllGroupsPorts;
#define  VlanNpFsMiVlanHwResetAllGroupsPorts  unOpCode.sFsMiVlanHwResetAllGroupsPorts;
#define  VlanNpFsMiVlanHwResetUnRegGroupsPorts  unOpCode.sFsMiVlanHwResetUnRegGroupsPorts;
#define  VlanNpFsMiVlanHwSetUnRegGroupsPorts  unOpCode.sFsMiVlanHwSetUnRegGroupsPorts;
#define  VlanNpFsMiVlanHwAddStaticUcastEntry  unOpCode.sFsMiVlanHwAddStaticUcastEntry;
#define  VlanNpFsMiVlanHwDelStaticUcastEntry  unOpCode.sFsMiVlanHwDelStaticUcastEntry;
#define  VlanNpFsMiVlanHwGetFdbEntry  unOpCode.sFsMiVlanHwGetFdbEntry;
#ifndef SW_LEARNING
#define  VlanNpFsMiVlanHwGetFdbCount  unOpCode.sFsMiVlanHwGetFdbCount;
#define  VlanNpFsMiVlanHwGetFirstTpFdbEntry  unOpCode.sFsMiVlanHwGetFirstTpFdbEntry;
#define  VlanNpFsMiVlanHwGetNextTpFdbEntry  unOpCode.sFsMiVlanHwGetNextTpFdbEntry;
#endif /* SW_LEARNING */
#define  VlanNpFsMiVlanHwAddMcastEntry  unOpCode.sFsMiVlanHwAddMcastEntry;
#define  VlanNpFsMiVlanHwAddStMcastEntry  unOpCode.sFsMiVlanHwAddStMcastEntry;
#define  VlanNpFsMiVlanHwSetMcastPort  unOpCode.sFsMiVlanHwSetMcastPort;
#define  VlanNpFsMiVlanHwResetMcastPort  unOpCode.sFsMiVlanHwResetMcastPort;
#define  VlanNpFsMiVlanHwDelMcastEntry  unOpCode.sFsMiVlanHwDelMcastEntry;
#define  VlanNpFsMiVlanHwDelStMcastEntry  unOpCode.sFsMiVlanHwDelStMcastEntry;
#define  VlanNpFsMiVlanHwAddVlanEntry  unOpCode.sFsMiVlanHwAddVlanEntry;
#define  VlanNpFsMiVlanHwDelVlanEntry  unOpCode.sFsMiVlanHwDelVlanEntry;
#define  VlanNpFsMiVlanHwSetVlanMemberPort  unOpCode.sFsMiVlanHwSetVlanMemberPort;
#define  VlanNpFsMiVlanHwResetVlanMemberPort  unOpCode.sFsMiVlanHwResetVlanMemberPort;
#define  VlanNpFsMiVlanHwSetPortPvid  unOpCode.sFsMiVlanHwSetPortPvid;
#define  VlanNpFsMiVlanHwSetDefaultVlanId  unOpCode.sFsMiVlanHwSetDefaultVlanId;
#ifdef L2RED_WANTED
#define  VlanNpFsMiVlanHwSyncDefaultVlanId  unOpCode.sFsMiVlanHwSyncDefaultVlanId;
#define  VlanNpFsMiVlanRedHwUpdateDBForDefaultVlanId  unOpCode.sFsMiVlanRedHwUpdateDBForDefaultVlanId;
#define  VlanNpFsMiVlanHwScanProtocolVlanTbl  unOpCode.sFsMiVlanHwScanProtocolVlanTbl;
#define  VlanNpFsMiVlanHwGetVlanProtocolMap  unOpCode.sFsMiVlanHwGetVlanProtocolMap;
#define  VlanNpFsMiVlanHwScanMulticastTbl  unOpCode.sFsMiVlanHwScanMulticastTbl;
#define  VlanNpFsMiVlanHwGetStMcastEntry  unOpCode.sFsMiVlanHwGetStMcastEntry;
#define  VlanNpFsMiVlanHwScanUnicastTbl  unOpCode.sFsMiVlanHwScanUnicastTbl;
#define  VlanNpFsMiVlanHwGetStaticUcastEntry  unOpCode.sFsMiVlanHwGetStaticUcastEntry;
#define  VlanNpFsMiVlanHwGetSyncedTnlProtocolMacAddr  unOpCode.sFsMiVlanHwGetSyncedTnlProtocolMacAddr;
#endif /* L2RED_WANTED */
#define  VlanNpFsMiVlanHwSetPortAccFrameType  unOpCode.sFsMiVlanHwSetPortAccFrameType;
#define  VlanNpFsMiVlanHwSetPortIngFiltering  unOpCode.sFsMiVlanHwSetPortIngFiltering;
#define  VlanNpFsMiVlanHwVlanEnable  unOpCode.sFsMiVlanHwVlanEnable;
#define  VlanNpFsMiVlanHwVlanDisable  unOpCode.sFsMiVlanHwVlanDisable;
#define  VlanNpFsMiVlanHwSetVlanLearningType  unOpCode.sFsMiVlanHwSetVlanLearningType;
#define  VlanNpFsMiVlanHwSetMacBasedStatusOnPort  unOpCode.sFsMiVlanHwSetMacBasedStatusOnPort;
#define  VlanNpFsMiVlanHwSetSubnetBasedStatusOnPort  unOpCode.sFsMiVlanHwSetSubnetBasedStatusOnPort;
#define  VlanNpFsMiVlanHwEnableProtoVlanOnPort  unOpCode.sFsMiVlanHwEnableProtoVlanOnPort;
#define  VlanNpFsMiVlanHwSetDefUserPriority  unOpCode.sFsMiVlanHwSetDefUserPriority;
#define  VlanNpFsMiVlanHwSetPortNumTrafClasses  unOpCode.sFsMiVlanHwSetPortNumTrafClasses;
#define  VlanNpFsMiVlanHwSetRegenUserPriority  unOpCode.sFsMiVlanHwSetRegenUserPriority;
#define  VlanNpFsMiVlanHwSetTraffClassMap  unOpCode.sFsMiVlanHwSetTraffClassMap;
#define  VlanNpFsMiVlanHwGmrpEnable  unOpCode.sFsMiVlanHwGmrpEnable;
#define  VlanNpFsMiVlanHwGmrpDisable  unOpCode.sFsMiVlanHwGmrpDisable;
#define  VlanNpFsMiVlanHwGvrpEnable  unOpCode.sFsMiVlanHwGvrpEnable;
#define  VlanNpFsMiVlanHwGvrpDisable  unOpCode.sFsMiVlanHwGvrpDisable;
#define  VlanNpFsMiNpDeleteAllFdbEntries  unOpCode.sFsMiNpDeleteAllFdbEntries;
#define  VlanNpFsMiVlanHwAddVlanProtocolMap  unOpCode.sFsMiVlanHwAddVlanProtocolMap;
#define  VlanNpFsMiVlanHwDelVlanProtocolMap  unOpCode.sFsMiVlanHwDelVlanProtocolMap;
#define  VlanNpFsMiVlanHwGetPortStats  unOpCode.sFsMiVlanHwGetPortStats;
#define  VlanNpFsMiVlanHwGetPortStats64  unOpCode.sFsMiVlanHwGetPortStats64;
#define  VlanNpFsMiVlanHwGetVlanStats  unOpCode.sFsMiVlanHwGetVlanStats;
#define  VlanNpFsMiVlanHwResetVlanStats  unOpCode.sFsMiVlanHwResetVlanStats;
#define  VlanNpFsMiVlanHwSetPortTunnelMode  unOpCode.sFsMiVlanHwSetPortTunnelMode;
#define  VlanNpFsMiVlanHwSetTunnelFilter  unOpCode.sFsMiVlanHwSetTunnelFilter;
#define  VlanNpFsMiVlanHwCheckTagAtEgress  unOpCode.sFsMiVlanHwCheckTagAtEgress;
#define  VlanNpFsMiVlanHwCheckTagAtIngress  unOpCode.sFsMiVlanHwCheckTagAtIngress;
#define  VlanNpFsMiVlanHwCreateFdbId  unOpCode.sFsMiVlanHwCreateFdbId;
#define  VlanNpFsMiVlanHwDeleteFdbId  unOpCode.sFsMiVlanHwDeleteFdbId;
#define  VlanNpFsMiVlanHwAssociateVlanFdb  unOpCode.sFsMiVlanHwAssociateVlanFdb;
#define  VlanNpFsMiVlanHwDisassociateVlanFdb  unOpCode.sFsMiVlanHwDisassociateVlanFdb;
#define  VlanNpFsMiVlanHwFlushPortFdbId  unOpCode.sFsMiVlanHwFlushPortFdbId;
#define  VlanNpFsMiVlanHwFlushPortFdbList  unOpCode.sFsMiVlanHwFlushPortFdbList;
#define  VlanNpFsMiVlanHwFlushPort  unOpCode.sFsMiVlanHwFlushPort;
#define  VlanNpFsMiVlanHwFlushFdbId  unOpCode.sFsMiVlanHwFlushFdbId;
#define  VlanNpFsMiVlanHwSetShortAgeout  unOpCode.sFsMiVlanHwSetShortAgeout;
#define  VlanNpFsMiVlanHwResetShortAgeout  unOpCode.sFsMiVlanHwResetShortAgeout;
#define  VlanNpFsMiVlanHwGetVlanInfo  unOpCode.sFsMiVlanHwGetVlanInfo;
#define  VlanNpFsMiVlanHwGetMcastEntry  unOpCode.sFsMiVlanHwGetMcastEntry;
#define  VlanNpFsMiVlanHwTraffClassMapInit  unOpCode.sFsMiVlanHwTraffClassMapInit;
#ifndef BRIDGE_WANTED
#define  VlanNpFsMiBrgSetAgingTime  unOpCode.sFsMiBrgSetAgingTime;
#endif /* BRIDGE_WANTED */
#define  VlanNpFsMiVlanHwSetBrgMode  unOpCode.sFsMiVlanHwSetBrgMode;
#define  VlanNpFsMiVlanHwSetBaseBridgeMode  unOpCode.sFsMiVlanHwSetBaseBridgeMode;
#define  VlanNpFsMiVlanHwAddPortMacVlanEntry  unOpCode.sFsMiVlanHwAddPortMacVlanEntry;
#define  VlanNpFsMiVlanHwDeletePortMacVlanEntry  unOpCode.sFsMiVlanHwDeletePortMacVlanEntry;
#define  VlanNpFsMiVlanHwAddPortSubnetVlanEntry  unOpCode.sFsMiVlanHwAddPortSubnetVlanEntry;
#define  VlanNpFsMiVlanHwDeletePortSubnetVlanEntry  unOpCode.sFsMiVlanHwDeletePortSubnetVlanEntry;
#define  VlanNpFsMiVlanHwUpdatePortSubnetVlanEntry  unOpCode.sFsMiVlanHwUpdatePortSubnetVlanEntry;
#define  VlanNpFsMiVlanNpHwRunMacAgeing  unOpCode.sFsMiVlanNpHwRunMacAgeing;
#define  VlanNpFsMiVlanHwMacLearningLimit  unOpCode.sFsMiVlanHwMacLearningLimit;
#define  VlanNpFsMiVlanHwSwitchMacLearningLimit  unOpCode.sFsMiVlanHwSwitchMacLearningLimit;
#define  VlanNpFsMiVlanHwMacLearningStatus  unOpCode.sFsMiVlanHwMacLearningStatus;
#define  VlanNpFsMiVlanHwSetFidPortLearningStatus  unOpCode.sFsMiVlanHwSetFidPortLearningStatus;
#define  VlanNpFsMiVlanHwSetProtocolTunnelStatusOnPort  unOpCode.sFsMiVlanHwSetProtocolTunnelStatusOnPort;
#define  VlanNpFsMiVlanHwSetPortProtectedStatus  unOpCode.sFsMiVlanHwSetPortProtectedStatus;
#define  VlanNpFsMiVlanHwAddStaticUcastEntryEx  unOpCode.sFsMiVlanHwAddStaticUcastEntryEx;
#define  VlanNpFsMiVlanHwGetStaticUcastEntryEx  unOpCode.sFsMiVlanHwGetStaticUcastEntryEx;
#define  VlanNpFsVlanHwForwardPktOnPorts  unOpCode.sFsVlanHwForwardPktOnPorts;
#define  VlanNpFsMiVlanHwPortMacLearningStatus  unOpCode.sFsMiVlanHwPortMacLearningStatus;
#define  VlanNpFsVlanHwGetMacLearningMode  unOpCode.sFsVlanHwGetMacLearningMode;
#define  VlanNpFsMiVlanHwSetMcastIndex  unOpCode.sFsMiVlanHwSetMcastIndex;
#ifdef MBSM_WANTED
#ifdef PB_WANTED
#define  VlanNpFsMiVlanMbsmHwSetProviderBridgePortType  unOpCode.sFsMiVlanMbsmHwSetProviderBridgePortType;
#define  VlanNpFsMiVlanMbsmHwSetPortSVlanTranslationStatus  unOpCode.sFsMiVlanMbsmHwSetPortSVlanTranslationStatus;
#define  VlanNpFsMiVlanMbsmHwAddSVlanTranslationEntry  unOpCode.sFsMiVlanMbsmHwAddSVlanTranslationEntry;
#define  VlanNpFsMiVlanMbsmHwSetPortEtherTypeSwapStatus  unOpCode.sFsMiVlanMbsmHwSetPortEtherTypeSwapStatus;
#define  VlanNpFsMiVlanMbsmHwAddEtherTypeSwapEntry  unOpCode.sFsMiVlanMbsmHwAddEtherTypeSwapEntry;
#define  VlanNpFsMiVlanMbsmHwAddSVlanMap  unOpCode.sFsMiVlanMbsmHwAddSVlanMap;
#define  VlanNpFsMiVlanMbsmHwSetPortSVlanClassifyMethod  unOpCode.sFsMiVlanMbsmHwSetPortSVlanClassifyMethod;
#define  VlanNpFsMiVlanMbsmHwPortMacLearningLimit  unOpCode.sFsMiVlanMbsmHwPortMacLearningLimit;
#define  VlanNpFsMiVlanMbsmHwSetPortCustomerVlan  unOpCode.sFsMiVlanMbsmHwSetPortCustomerVlan;
#define  VlanNpFsMiVlanMbsmHwCreateProviderEdgePort  unOpCode.sFsMiVlanMbsmHwCreateProviderEdgePort;
#define  VlanNpFsMiVlanMbsmHwSetPcpEncodTbl  unOpCode.sFsMiVlanMbsmHwSetPcpEncodTbl;
#define  VlanNpFsMiVlanMbsmHwSetPcpDecodTbl  unOpCode.sFsMiVlanMbsmHwSetPcpDecodTbl;
#define  VlanNpFsMiVlanMbsmHwSetPortUseDei  unOpCode.sFsMiVlanMbsmHwSetPortUseDei;
#define  VlanNpFsMiVlanMbsmHwSetPortReqDropEncoding  unOpCode.sFsMiVlanMbsmHwSetPortReqDropEncoding;
#define  VlanNpFsMiVlanMbsmHwSetPortPcpSelection  unOpCode.sFsMiVlanMbsmHwSetPortPcpSelection;
#define  VlanNpFsMiVlanMbsmHwSetServicePriRegenEntry  unOpCode.sFsMiVlanMbsmHwSetServicePriRegenEntry;
#define  VlanNpFsMiVlanMbsmHwMulticastMacTableLimit  unOpCode.sFsMiVlanMbsmHwMulticastMacTableLimit;
#endif /* PB_WANTED */
#define  VlanNpFsMiVlanMbsmHwInit  unOpCode.sFsMiVlanMbsmHwInit;
#define  VlanNpFsMiVlanMbsmHwSetBrgMode  unOpCode.sFsMiVlanMbsmHwSetBrgMode;
#define  VlanNpFsMiVlanMbsmHwSetDefaultVlanId  unOpCode.sFsMiVlanMbsmHwSetDefaultVlanId;
#define  VlanNpFsMiVlanMbsmHwAddStaticUcastEntry  unOpCode.sFsMiVlanMbsmHwAddStaticUcastEntry;
#define  VlanNpFsMiVlanMbsmHwSetUcastPort  unOpCode.sFsMiVlanMbsmHwSetUcastPort;
#define  VlanNpFsMiVlanMbsmHwResetUcastPort  unOpCode.sFsMiVlanMbsmHwResetUcastPort;
#define  VlanNpFsMiVlanMbsmHwDelStaticUcastEntry  unOpCode.sFsMiVlanMbsmHwDelStaticUcastEntry;
#define  VlanNpFsMiVlanMbsmHwAddMcastEntry  unOpCode.sFsMiVlanMbsmHwAddMcastEntry;
#define  VlanNpFsMiVlanMbsmHwSetMcastPort  unOpCode.sFsMiVlanMbsmHwSetMcastPort;
#define  VlanNpFsMiVlanMbsmHwResetMcastPort  unOpCode.sFsMiVlanMbsmHwResetMcastPort;
#define  VlanNpFsMiVlanMbsmHwAddVlanEntry  unOpCode.sFsMiVlanMbsmHwAddVlanEntry;
#define  VlanNpFsMiVlanMbsmHwSetVlanMemberPort  unOpCode.sFsMiVlanMbsmHwSetVlanMemberPort;
#define  VlanNpFsMiVlanMbsmHwResetVlanMemberPort  unOpCode.sFsMiVlanMbsmHwResetVlanMemberPort;
#define  VlanNpFsMiVlanMbsmHwSetPortPvid  unOpCode.sFsMiVlanMbsmHwSetPortPvid;
#define  VlanNpFsMiVlanMbsmHwSetPortAccFrameType  unOpCode.sFsMiVlanMbsmHwSetPortAccFrameType;
#define  VlanNpFsMiVlanMbsmHwSetPortIngFiltering  unOpCode.sFsMiVlanMbsmHwSetPortIngFiltering;
#define  VlanNpFsMiVlanMbsmHwSetDefUserPriority  unOpCode.sFsMiVlanMbsmHwSetDefUserPriority;
#define  VlanNpFsMiVlanMbsmHwTraffClassMapInit  unOpCode.sFsMiVlanMbsmHwTraffClassMapInit;
#define  VlanNpFsMiVlanMbsmHwSetTraffClassMap  unOpCode.sFsMiVlanMbsmHwSetTraffClassMap;
#define  VlanNpFsMiVlanMbsmHwAddStMcastEntry  unOpCode.sFsMiVlanMbsmHwAddStMcastEntry;
#define  VlanNpFsMiVlanMbsmHwDelStMcastEntry  unOpCode.sFsMiVlanMbsmHwDelStMcastEntry;
#define  VlanNpFsMiVlanMbsmHwSetPortNumTrafClasses  unOpCode.sFsMiVlanMbsmHwSetPortNumTrafClasses;
#define  VlanNpFsMiVlanMbsmHwSetRegenUserPriority  unOpCode.sFsMiVlanMbsmHwSetRegenUserPriority;
#define  VlanNpFsMiVlanMbsmHwSetSubnetBasedStatusOnPort  unOpCode.sFsMiVlanMbsmHwSetSubnetBasedStatusOnPort;
#define  VlanNpFsMiVlanMbsmHwEnableProtoVlanOnPort  unOpCode.sFsMiVlanMbsmHwEnableProtoVlanOnPort;
#define  VlanNpFsMiVlanMbsmHwSetAllGroupsPorts  unOpCode.sFsMiVlanMbsmHwSetAllGroupsPorts;
#define  VlanNpFsMiVlanMbsmHwSetUnRegGroupsPorts  unOpCode.sFsMiVlanMbsmHwSetUnRegGroupsPorts;
#define  VlanNpFsMiVlanMbsmHwSetTunnelFilter  unOpCode.sFsMiVlanMbsmHwSetTunnelFilter;
#define  VlanNpFsMiVlanMbsmHwAddPortSubnetVlanEntry  unOpCode.sFsMiVlanMbsmHwAddPortSubnetVlanEntry;
#define  VlanNpFsMiVlanMbsmHwSetPortProperty  unOpCode.sFsMiVlanMbsmHwSetPortProperty;
#define  VlanNpFsMiVlanMbsmHwPortPktReflectStatus  unOpCode.sFsMiVlanMbsmHwPortPktReflectStatus;
#ifndef BRIDGE_WANTED
#define  VlanNpFsMiBrgMbsmSetAgingTime  unOpCode.sFsMiBrgMbsmSetAgingTime;
#endif /* BRIDGE_WANTED */
#define  VlanNpFsMiVlanMbsmHwSetFidPortLearningStatus  unOpCode.sFsMiVlanMbsmHwSetFidPortLearningStatus;
#define  VlanNpFsMiVlanMbsmHwSetProtocolTunnelStatusOnPort  unOpCode.sFsMiVlanMbsmHwSetProtocolTunnelStatusOnPort;
#define  VlanNpFsMiVlanMbsmHwSetPortProtectedStatus  unOpCode.sFsMiVlanMbsmHwSetPortProtectedStatus;
#define  VlanNpFsMiVlanMbsmHwSwitchMacLearningLimit  unOpCode.sFsMiVlanMbsmHwSwitchMacLearningLimit;
#define  VlanNpFsMiVlanMbsmHwPortMacLearningStatus  unOpCode.sFsMiVlanMbsmHwPortMacLearningStatus;
#define  VlanNpFsMiVlanMbsmHwMacLearningLimit  unOpCode.sFsMiVlanMbsmHwMacLearningLimit;
#define  VlanNpFsMiVlanMbsmHwAddStaticUcastEntryEx  unOpCode.sFsMiVlanMbsmHwAddStaticUcastEntryEx;
#define  VlanNpFsMiVlanMbsmSyncFDBInfo  unOpCode.sFsMiVlanMbsmSyncFDBInfo;
#define  VlanNpFsMiVlanMbsmHwSetPortIngressEtherType  unOpCode.sFsMiVlanMbsmHwSetPortIngressEtherType;
#define  VlanNpFsMiVlanMbsmHwSetPortEgressEtherType  unOpCode.sFsMiVlanMbsmHwSetPortEgressEtherType;
#define  VlanNpFsMiVlanMbsmHwEvbConfigSChIface  unOpCode.sFsMiVlanMbsmHwEvbConfigSChIface;
#define  VlanNpFsMiVlanMbsmHwSetBridgePortType  unOpCode.sFsMiVlanMbsmHwSetBridgePortType; 
#endif /* MBSM_WANTED */ 
#ifdef PB_WANTED
#define  VlanNpFsMiVlanHwSetProviderBridgePortType  unOpCode.sFsMiVlanHwSetProviderBridgePortType;
#define  VlanNpFsMiVlanHwSetPortSVlanTranslationStatus  unOpCode.sFsMiVlanHwSetPortSVlanTranslationStatus;
#define  VlanNpFsMiVlanHwAddSVlanTranslationEntry  unOpCode.sFsMiVlanHwAddSVlanTranslationEntry;
#define  VlanNpFsMiVlanHwDelSVlanTranslationEntry  unOpCode.sFsMiVlanHwDelSVlanTranslationEntry;
#define  VlanNpFsMiVlanHwSetPortEtherTypeSwapStatus  unOpCode.sFsMiVlanHwSetPortEtherTypeSwapStatus;
#define  VlanNpFsMiVlanHwAddEtherTypeSwapEntry  unOpCode.sFsMiVlanHwAddEtherTypeSwapEntry;
#define  VlanNpFsMiVlanHwDelEtherTypeSwapEntry  unOpCode.sFsMiVlanHwDelEtherTypeSwapEntry;
#define  VlanNpFsMiVlanHwAddSVlanMap  unOpCode.sFsMiVlanHwAddSVlanMap;
#define  VlanNpFsMiVlanHwDeleteSVlanMap  unOpCode.sFsMiVlanHwDeleteSVlanMap;
#define  VlanNpFsMiVlanHwSetPortSVlanClassifyMethod  unOpCode.sFsMiVlanHwSetPortSVlanClassifyMethod;
#define  VlanNpFsMiVlanHwPortMacLearningLimit  unOpCode.sFsMiVlanHwPortMacLearningLimit;
#define  VlanNpFsMiVlanHwMulticastMacTableLimit  unOpCode.sFsMiVlanHwMulticastMacTableLimit;
#define  VlanNpFsMiVlanHwSetPortCustomerVlan  unOpCode.sFsMiVlanHwSetPortCustomerVlan;
#define  VlanNpFsMiVlanHwResetPortCustomerVlan  unOpCode.sFsMiVlanHwResetPortCustomerVlan;
#define  VlanNpFsMiVlanHwCreateProviderEdgePort  unOpCode.sFsMiVlanHwCreateProviderEdgePort;
#define  VlanNpFsMiVlanHwDelProviderEdgePort  unOpCode.sFsMiVlanHwDelProviderEdgePort;
#define  VlanNpFsMiVlanHwSetPepPvid  unOpCode.sFsMiVlanHwSetPepPvid;
#define  VlanNpFsMiVlanHwSetPepAccFrameType  unOpCode.sFsMiVlanHwSetPepAccFrameType;
#define  VlanNpFsMiVlanHwSetPepDefUserPriority  unOpCode.sFsMiVlanHwSetPepDefUserPriority;
#define  VlanNpFsMiVlanHwSetPepIngFiltering  unOpCode.sFsMiVlanHwSetPepIngFiltering;
#define  VlanNpFsMiVlanHwSetCvidUntagPep  unOpCode.sFsMiVlanHwSetCvidUntagPep;
#define  VlanNpFsMiVlanHwSetCvidUntagCep  unOpCode.sFsMiVlanHwSetCvidUntagCep;
#define  VlanNpFsMiVlanHwSetPcpEncodTbl  unOpCode.sFsMiVlanHwSetPcpEncodTbl;
#define  VlanNpFsMiVlanHwSetPcpDecodTbl  unOpCode.sFsMiVlanHwSetPcpDecodTbl;
#define  VlanNpFsMiVlanHwSetPortUseDei  unOpCode.sFsMiVlanHwSetPortUseDei;
#define  VlanNpFsMiVlanHwSetPortReqDropEncoding  unOpCode.sFsMiVlanHwSetPortReqDropEncoding;
#define  VlanNpFsMiVlanHwSetPortPcpSelection  unOpCode.sFsMiVlanHwSetPortPcpSelection;
#define  VlanNpFsMiVlanHwSetServicePriRegenEntry  unOpCode.sFsMiVlanHwSetServicePriRegenEntry;
#define  VlanNpFsMiVlanHwSetTunnelMacAddress  unOpCode.sFsMiVlanHwSetTunnelMacAddress;
#define  VlanNpFsMiVlanHwGetNextSVlanMap  unOpCode.sFsMiVlanHwGetNextSVlanMap;
#define  VlanNpFsMiVlanHwGetNextSVlanTranslationEntry  unOpCode.sFsMiVlanHwGetNextSVlanTranslationEntry;
#define  VlanNpFsMiVlanHwSetEvcAttribute  unOpCode.sFsMiVlanHwSetEvcAttribute;
#define  VlanNpFsMiVlanHwSetCVlanStat  unOpCode.sFsMiVlanHwSetCVlanStat;
#define  VlanNpFsMiVlanHwGetCVlanStat  unOpCode.sFsMiVlanHwGetCVlanStat;
#define  VlanNpFsMiVlanHwClearCVlanStat  unOpCode.sFsMiVlanHwClearCVlanStat;
#endif /* PB_WANTED */
#define  VlanNpFsMiVlanHwSetPortProperty  unOpCode.sFsMiVlanHwSetPortProperty;
#define  VlanNpFsMiVlanHwSetPortIngressEtherType  unOpCode.sFsMiVlanHwSetPortIngressEtherType;
#define  VlanNpFsMiVlanHwSetPortEgressEtherType  unOpCode.sFsMiVlanHwSetPortEgressEtherType;
#define  VlanNpFsMiVlanHwSetVlanLoopbackStatus  unOpCode.sFsMiVlanHwSetVlanLoopbackStatus;
#define  VlanNpFsMiVlanHwPortUnicastMacSecType unOpCode.sFsMiVlanHwPortUnicastMacSecType;
#define  VlanNpFsMiVlanHwEvbConfigSChIface  unOpCode.sFsMiVlanHwEvbConfigSChIface;
#define  VlanNpFsMiVlanHwSetBridgePortType  unOpCode.sFsMiVlanHwSetBridgePortType;
#define  VlanNpFsNpHwGetPortFromFdb  unOpCode.sFsNpHwGetPortFromFdb;
#define  VlanNpFsMiVlanHwPortPktReflectStatus  unOpCode.sFsMiVlanHwPortPktReflectStatus;
} tVlanNpModInfo;


/* Function Prototype for Generic NP OpCode specific wrappers
 * defined in Vlannpapi.c */

UINT1 VlanFsMiVlanHwInit PROTO ((UINT4 u4ContextId));
UINT1 VlanFsMiVlanHwDeInit PROTO ((UINT4 u4ContextId));
UINT1 VlanFsMiVlanHwSetAllGroupsPorts PROTO ((UINT4 u4ContextId, tVlanId VlanId, tHwPortArray * pHwAllGroupPorts));
UINT1 VlanFsMiVlanHwResetAllGroupsPorts PROTO ((UINT4 u4ContextId, tVlanId VlanId, tHwPortArray * pHwResetAllGroupPorts));
UINT1 VlanFsMiVlanHwResetUnRegGroupsPorts PROTO ((UINT4 u4ContextId, tVlanId VlanId, tHwPortArray * pHwResetUnRegGroupPorts));
UINT1 VlanFsMiVlanHwSetUnRegGroupsPorts PROTO ((UINT4 u4ContextId, tVlanId VlanId, tHwPortArray * pHwUnRegPorts));
UINT1 VlanFsMiVlanHwAddStaticUcastEntry PROTO ((UINT4 u4ContextId, UINT4 u4Fid, tMacAddr MacAddr, UINT4 u4Port, tHwPortArray * pHwAllowedToGoPorts, UINT1 u1Status));
UINT1 VlanFsMiVlanHwDelStaticUcastEntry PROTO ((UINT4 u4ContextId, UINT4 u4Fid, tMacAddr MacAddr, UINT4 u4Port));
UINT1 VlanFsMiVlanHwGetFdbEntry PROTO ((UINT4 u4ContextId, UINT4 u4FdbId, tMacAddr MacAddr, tHwUnicastMacEntry * pEntry));
#ifndef SW_LEARNING
UINT1 VlanFsMiVlanHwGetFdbCount PROTO ((UINT4 u4ContextId, UINT4 u4FdbId, UINT4 * pu4Count));
UINT1 VlanFsMiVlanHwGetFirstTpFdbEntry PROTO ((UINT4 u4ContextId, UINT4 * pu4FdbId, tMacAddr MacAddr));
UINT1 VlanFsMiVlanHwGetNextTpFdbEntry PROTO ((UINT4 u4ContextId, UINT4 u4FdbId, tMacAddr MacAddr, UINT4 * pu4NextContextId, UINT4 * pu4NextFdbId, tMacAddr NextMacAddr));
#endif /* SW_LEARNING */
UINT1 VlanFsMiVlanHwAddMcastEntry PROTO ((UINT4 u4ContextId, tVlanId VlanId, tMacAddr MacAddr, tHwPortArray * pHwMcastPorts));
UINT1 VlanFsMiVlanHwAddStMcastEntry PROTO ((UINT4 u4ContextId, tVlanId VlanId, tMacAddr MacAddr, INT4 i4RcvPort, tHwPortArray * pHwMcastPorts));
UINT1 VlanFsMiVlanHwSetMcastPort PROTO ((UINT4 u4ContextId, tVlanId VlanId, tMacAddr MacAddr, UINT4 u4IfIndex));
UINT1 VlanFsMiVlanHwResetMcastPort PROTO ((UINT4 u4ContextId, tVlanId VlanId, tMacAddr MacAddr, UINT4 u4IfIndex));
UINT1 VlanFsMiVlanHwDelMcastEntry PROTO ((UINT4 u4ContextId, tVlanId VlanId, tMacAddr MacAddr));
UINT1 VlanFsMiVlanHwDelStMcastEntry PROTO ((UINT4 u4ContextId, tVlanId VlanId, tMacAddr MacAddr, INT4 i4RcvPort));
UINT1 VlanFsMiVlanHwAddVlanEntry PROTO ((UINT4 u4ContextId, tVlanId VlanId, tHwPortArray * pHwEgressPorts, tHwPortArray * pHwUnTagPorts));
UINT1 VlanFsMiVlanHwDelVlanEntry PROTO ((UINT4 u4ContextId, tVlanId VlanId));
UINT1 VlanFsMiVlanHwSetVlanMemberPort PROTO ((UINT4 u4ContextId, tVlanId VlanId, UINT4 u4IfIndex, UINT1 u1IsTagged));
UINT1 VlanFsMiVlanHwResetVlanMemberPort PROTO ((UINT4 u4ContextId, tVlanId VlanId, UINT4 u4IfIndex));
UINT1 VlanFsMiVlanHwSetPortPvid PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId VlanId));
UINT1 VlanFsMiVlanHwSetDefaultVlanId PROTO ((UINT4 u4ContextId, tVlanId VlanId));
#ifdef L2RED_WANTED
UINT1 VlanFsMiVlanHwSyncDefaultVlanId PROTO ((UINT4 u4ContextId, tVlanId SwVlanId));
UINT1 VlanFsMiVlanRedHwUpdateDBForDefaultVlanId PROTO ((UINT4 u4ContextId, tVlanId VlanId));
UINT1 VlanFsMiVlanHwScanProtocolVlanTbl PROTO ((UINT4 u4ContextId, UINT4 u4Port, FsMiVlanHwProtoCb ProtoVlanCallBack));
UINT1 VlanFsMiVlanHwGetVlanProtocolMap PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT4 u4GroupId, tVlanProtoTemplate * pProtoTemplate, tVlanId * pVlanId));
UINT1 VlanFsMiVlanHwScanMulticastTbl PROTO ((UINT4 u4ContextId, FsMiVlanHwMcastCb McastCallBack));
UINT1 VlanFsMiVlanHwGetStMcastEntry PROTO ((UINT4 u4ContextId, tVlanId VlanId, tMacAddr MacAddr, UINT4 u4RcvPort, tHwPortArray * pHwMcastPorts));
UINT1 VlanFsMiVlanHwScanUnicastTbl PROTO ((UINT4 u4ContextId, FsMiVlanHwUcastCb UcastCallBack));
UINT1 VlanFsMiVlanHwGetStaticUcastEntry PROTO ((UINT4 u4ContextId, UINT4 u4Fid, tMacAddr MacAddr, UINT4 u4Port, tHwPortArray * pAllowedToGoPorts, UINT1 * pu1Status));
UINT1 VlanFsMiVlanHwGetSyncedTnlProtocolMacAddr PROTO ((UINT4 u4ContextId, UINT2 u2Protocol, tMacAddr MacAddr));
#endif /* L2RED_WANTED */
UINT1 VlanFsMiVlanHwSetPortAccFrameType PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, INT1 u1AccFrameType));
UINT1 VlanFsMiVlanHwSetPortIngFiltering PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1IngFilterEnable));
UINT1 VlanFsMiVlanHwVlanEnable PROTO ((UINT4 u4ContextId));
UINT1 VlanFsMiVlanHwVlanDisable PROTO ((UINT4 u4ContextId));
UINT1 VlanFsMiVlanHwSetVlanLearningType PROTO ((UINT4 u4ContextId, UINT1 u1LearningType));
UINT1 VlanFsMiVlanHwSetMacBasedStatusOnPort PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1MacBasedVlanEnable));
UINT1 VlanFsMiVlanHwSetSubnetBasedStatusOnPort PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1SubnetBasedVlanEnable));
UINT1 VlanFsMiVlanHwEnableProtoVlanOnPort PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1VlanProtoEnable));
UINT1 VlanFsMiVlanHwSetDefUserPriority PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, INT4 i4DefPriority));
UINT1 VlanFsMiVlanHwSetPortNumTrafClasses PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, INT4 i4NumTraffClass));
UINT1 VlanFsMiVlanHwSetRegenUserPriority PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, INT4 i4UserPriority, INT4 i4RegenPriority));
UINT1 VlanFsMiVlanHwSetTraffClassMap PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, INT4 i4UserPriority, INT4 i4TraffClass));
UINT1 VlanFsMiVlanHwGmrpEnable PROTO ((UINT4 u4ContextId));
UINT1 VlanFsMiVlanHwGmrpDisable PROTO ((UINT4 u4ContextId));
UINT1 VlanFsMiVlanHwGvrpEnable PROTO ((UINT4 u4ContextId));
UINT1 VlanFsMiVlanHwGvrpDisable PROTO ((UINT4 u4ContextId));
UINT1 VlanFsMiNpDeleteAllFdbEntries PROTO ((UINT4 u4ContextId));
UINT1 VlanFsMiVlanHwAddVlanProtocolMap PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT4 u4GroupId, tVlanProtoTemplate * pProtoTemplate, tVlanId VlanId));
UINT1 VlanFsMiVlanHwDelVlanProtocolMap PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT4 u4GroupId, tVlanProtoTemplate * pProtoTemplate));
UINT1 VlanFsMiVlanHwGetPortStats PROTO ((UINT4 u4ContextId, UINT4 u4Port, tVlanId VlanId, UINT1 u1StatsType, UINT4 * pu4PortStatsValue));
UINT1 VlanFsMiVlanHwGetPortStats64 PROTO ((UINT4 u4ContextId, UINT4 u4Port, tVlanId VlanId, UINT1 u1StatsType, tSNMP_COUNTER64_TYPE * pValue));
UINT1 VlanFsMiVlanHwGetVlanStats PROTO ((UINT4 u4ContextId, tVlanId VlanId, UINT1 u1StatsType, UINT4 * pu4VlanStatsValue));
UINT1 VlanFsMiVlanHwResetVlanStats PROTO ((UINT4 u4ContextId, tVlanId VlanId));
UINT1 VlanFsMiVlanHwSetPortTunnelMode PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT4 u4Mode));
UINT1 VlanFsMiVlanHwSetTunnelFilter PROTO ((UINT4 u4ContextId, INT4 i4BridgeMode));
UINT1 VlanFsMiVlanHwCheckTagAtEgress PROTO ((UINT4 u4ContextId, UINT1 * pu1TagSet));
UINT1 VlanFsMiVlanHwCheckTagAtIngress PROTO ((UINT4 u4ContextId, UINT1 * pu1TagSet));
UINT1 VlanFsMiVlanHwCreateFdbId PROTO ((UINT4 u4ContextId, UINT4 u4Fid));
UINT1 VlanFsMiVlanHwDeleteFdbId PROTO ((UINT4 u4ContextId, UINT4 u4Fid));
UINT1 VlanFsMiVlanHwAssociateVlanFdb PROTO ((UINT4 u4ContextId, UINT4 u4Fid, tVlanId VlanId));
UINT1 VlanFsMiVlanHwDisassociateVlanFdb PROTO ((UINT4 u4ContextId, UINT4 u4Fid, tVlanId VlanId));
UINT1 VlanFsMiVlanHwFlushPortFdbId PROTO ((UINT4 u4ContextId, UINT4 u4Port, UINT4 u4Fid, INT4 i4OptimizeFlag));
UINT1 VlanFsMiVlanHwFlushPortFdbList PROTO ((UINT4 u4ContextId, tVlanFlushInfo * pVlanFlushInfo));
UINT1 VlanFsMiVlanHwFlushPort PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, INT4 i4OptimizeFlag));
UINT1 VlanFsMiVlanHwFlushFdbId PROTO ((UINT4 u4ContextId, UINT4 u4Fid));
UINT1 VlanFsMiVlanHwSetShortAgeout PROTO ((UINT4 u4ContextId, UINT4 u4Port, INT4 i4AgingTime));
UINT1 VlanFsMiVlanHwResetShortAgeout PROTO ((UINT4 u4ContextId, UINT4 u4Port, INT4 i4LongAgeout));
UINT1 VlanFsMiVlanHwGetVlanInfo PROTO ((UINT4 u4ContextId, tVlanId VlanId, tHwVlanPortArray * pHwEntry));
UINT1 VlanFsMiVlanHwGetMcastEntry PROTO ((UINT4 u4ContextId, tVlanId VlanId, tMacAddr MacAddr, tHwPortArray * pHwMcastPorts));
UINT1 VlanFsMiVlanHwTraffClassMapInit PROTO ((UINT4 u4ContextId, UINT1 u1Priority, INT4 i4CosqValue));
#ifndef BRIDGE_WANTED
UINT1 VlanFsMiBrgSetAgingTime PROTO ((UINT4 u4ContextId, INT4 i4AgingTime));
#endif /* BRIDGE_WANTED */
UINT1 VlanFsMiVlanHwSetBrgMode PROTO ((UINT4 u4ContextId, UINT4 u4BridgeMode));
UINT1 VlanFsMiVlanHwSetBaseBridgeMode PROTO ((UINT4 u4IfIndex, UINT4 u4Mode));
UINT1 VlanFsMiVlanHwAddPortMacVlanEntry PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, tMacAddr MacAddr, tVlanId VlanId, BOOL1 bSuppressOption));
UINT1 VlanFsMiVlanHwDeletePortMacVlanEntry PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, tMacAddr MacAddr));
UINT1 VlanFsMiVlanHwAddPortSubnetVlanEntry PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT4 SubnetAddr, tVlanId VlanId, BOOL1 bARPOption));
UINT1 VlanFsMiVlanHwDeletePortSubnetVlanEntry PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT4 SubnetAddr));
UINT1 VlanFsMiVlanHwUpdatePortSubnetVlanEntry PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT4 u4SubnetAddr, UINT4 u4SubnetMask, tVlanId VlanId, BOOL1 bARPOption, UINT1 u1Action));
UINT1 VlanFsMiVlanNpHwRunMacAgeing PROTO ((UINT4 u4ContextId));
UINT1 VlanFsMiVlanHwMacLearningLimit PROTO ((UINT4 u4ContextId, tVlanId VlanId, UINT2 u2FdbId, UINT4 u4MacLimit));
UINT1 VlanFsMiVlanHwSwitchMacLearningLimit PROTO ((UINT4 u4ContextId, UINT4 u4MacLimit));
UINT1 VlanFsMiVlanHwMacLearningStatus PROTO ((UINT4 u4ContextId, tVlanId VlanId, UINT2 u2FdbId, tHwPortArray * pHwEgressPorts, UINT1 u1Status));
UINT1 VlanFsMiVlanHwSetFidPortLearningStatus PROTO ((UINT4 u4ContextId, UINT4 u4Fid, tHwPortArray HwPortList, UINT4 u4Port, UINT1 u1Action));
UINT1 VlanFsMiVlanHwSetProtocolTunnelStatusOnPort PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, tVlanHwTunnelFilters ProtocolId, UINT4 u4TunnelStatus));
UINT1 VlanFsMiVlanHwSetPortProtectedStatus PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, INT4 i4ProtectedStatus));
UINT1 VlanFsMiVlanHwAddStaticUcastEntryEx PROTO ((UINT4 u4ContextId, UINT4 u4Fid, tMacAddr MacAddr, UINT4 u4Port, tHwPortArray * pHwAllowedToGoPorts, UINT1 u1Status, tMacAddr ConnectionId));
UINT1 VlanFsMiVlanHwGetStaticUcastEntryEx PROTO ((UINT4 u4ContextId, UINT4 u4Fid, tMacAddr MacAddr, UINT4 u4Port, tHwPortArray * pAllowedToGoPorts, UINT1 * pu1Status, tMacAddr ConnectionId));
UINT1 VlanFsVlanHwForwardPktOnPorts PROTO ((UINT1 * pu1Packet, UINT2 u2PacketLen, tHwVlanFwdInfo * pVlanFwdInfo));
UINT1 VlanFsMiVlanHwPortMacLearningStatus PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1Status));
UINT1 VlanFsVlanHwGetMacLearningMode PROTO ((UINT4 * pu4LearningMode));
UINT1 VlanFsMiVlanHwPortUnicastMacSecType PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, INT4 i4MacSecType));
#ifdef MBSM_WANTED
#ifdef PB_WANTED
UINT1 VlanFsMiVlanMbsmHwSetProviderBridgePortType PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT4 u4PortType, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwSetPortSVlanTranslationStatus PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1Status, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwAddSVlanTranslationEntry PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2LocalSVlan, UINT2 u2RelaySVlan, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwSetPortEtherTypeSwapStatus PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1Status, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwAddEtherTypeSwapEntry PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2LocalEtherType, UINT2 u2RelayEtherType, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwAddSVlanMap PROTO ((UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwSetPortSVlanClassifyMethod PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1TableType, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwPortMacLearningLimit PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT4 u4MacLimit, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwSetPortCustomerVlan PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId CVlanId, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwCreateProviderEdgePort PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId SVlanId, tHwVlanPbPepInfo PepConfig, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwSetPcpEncodTbl PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, tHwVlanPbPcpInfo NpPbVlanPcpInfo, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwSetPcpDecodTbl PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, tHwVlanPbPcpInfo NpPbVlanPcpInfo, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwSetPortUseDei PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1UseDei, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwSetPortReqDropEncoding PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1ReqDrpEncoding, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwSetPortPcpSelection PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2PcpSelection, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwSetServicePriRegenEntry PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId SVlanId, INT4 i4RecvPriority, INT4 i4RegenPriority, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwMulticastMacTableLimit PROTO ((UINT4 u4ContextId, UINT4 u4MacLimit, tMbsmSlotInfo * pSlotInfo));
#endif /* PB_WANTED */
UINT1 VlanFsMiVlanMbsmHwInit PROTO ((UINT4 u4ContextId, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwSetBrgMode PROTO ((UINT4 u4ContextId, UINT4 u4BridgeMode, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwSetDefaultVlanId PROTO ((UINT4 u4ContextId, tVlanId VlanId, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwAddStaticUcastEntry PROTO ((UINT4 u4ContextId, UINT4 u4FdbId, tMacAddr MacAddr, UINT4 u4RcvPort, tHwPortArray * pHwAllowedToGoPorts, UINT1 u1Status, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwSetUcastPort PROTO ((UINT4 u4ContextId, UINT4 u4FdbId, tMacAddr MacAddr, UINT4 u4RcvPort, UINT4 u4AllowedToGoPort, UINT1 u1Status, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwResetUcastPort PROTO ((UINT4 u4ContextId, UINT4 u4FdbId, tMacAddr MacAddr, UINT4 u4RcvPort, UINT4 u4AllowedToGoPort, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwDelStaticUcastEntry PROTO ((UINT4 u4ContextId, UINT4 u4Fid, tMacAddr MacAddr, UINT4 u4RcvPort, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwAddMcastEntry PROTO ((UINT4 u4ContextId, tVlanId VlanId, tMacAddr MacAddr, tHwPortArray * pHwMcastPorts, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwSetMcastPort PROTO ((UINT4 u4ContextId, tVlanId VlanId, tMacAddr MacAddr, UINT4 u4Port, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwResetMcastPort PROTO ((UINT4 u4ContextId, tVlanId VlanId, tMacAddr MacAddr, UINT4 u4Port, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwAddVlanEntry PROTO ((UINT4 u4ContextId, tVlanId VlanId, tHwPortArray * pHwEgressPorts, tHwPortArray * pHwUnTagPorts, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwSetVlanMemberPort PROTO ((UINT4 u4ContextId, tVlanId VlanId, UINT4 u4Port, UINT1 u1IsTagged, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwResetVlanMemberPort PROTO ((UINT4 u4ContextId, tVlanId VlanId, UINT4 u4Port, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwSetPortPvid PROTO ((UINT4 u4ContextId, UINT4 u4Port, tVlanId VlanId, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwSetPortAccFrameType PROTO ((UINT4 u4ContextId, UINT4 u4Port, UINT1 u1AccFrameType, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwSetPortIngFiltering PROTO ((UINT4 u4ContextId, UINT4 u4Port, UINT1 u1IngFilterEnable, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwSetDefUserPriority PROTO ((UINT4 u4ContextId, UINT4 u4Port, INT4 i4DefPriority, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwTraffClassMapInit PROTO ((UINT4 u4ContextId, UINT1 u1Priority, INT4 i4CosqValue, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwSetTraffClassMap PROTO ((UINT4 u4ContextId, UINT4 u4Port, INT4 i4UserPriority, INT4 i4TraffClass, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwAddStMcastEntry PROTO ((UINT4 u4ContextId, tVlanId VlanId, tMacAddr McastAddr, INT4 i4RcvPort, tHwPortArray * pHwMcastPorts, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwDelStMcastEntry PROTO ((UINT4 u4ContextId, tVlanId VlanId, tMacAddr MacAddr, UINT4 u4RcvPort, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwSetPortNumTrafClasses PROTO ((UINT4 u4ContextId, UINT4 u4Port, INT4 i4NumTraffClass, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwSetRegenUserPriority PROTO ((UINT4 u4ContextId, UINT4 u4Port, INT4 i4UserPriority, INT4 i4RegenPriority, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwSetSubnetBasedStatusOnPort PROTO ((UINT4 u4ContextId, UINT4 u4Port, UINT1 u1VlanSubnetEnable, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwEnableProtoVlanOnPort PROTO ((UINT4 u4ContextId, UINT4 u4Port, UINT1 u1VlanProtoEnable, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwSetAllGroupsPorts PROTO ((UINT4 u4ContextId, tVlanId VlanId, tHwPortArray * pHwAllGroupPorts, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwSetUnRegGroupsPorts PROTO ((UINT4 u4ContextId, tVlanId VlanId, tHwPortArray * pHwUnRegPorts, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwSetTunnelFilter PROTO ((UINT4 u4ContextId, INT4 i4BridgeMode, tMacAddr MacAddr, UINT2 u2ProtocolId, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwAddPortSubnetVlanEntry PROTO ((UINT4 u4ContextId, UINT4 u4Port, UINT4 u4SubnetAddr, tVlanId VlanId, UINT1 u1ArpOption, tMbsmSlotInfo * pSlotInfo));
#ifndef BRIDGE_WANTED
UINT1 VlanFsMiBrgMbsmSetAgingTime PROTO ((UINT4 u4ContextId, INT4 i4AgingTime, tMbsmSlotInfo * pSlotInfo));
#endif /* BRIDGE_WANTED */
UINT1 VlanFsMiVlanMbsmHwSetFidPortLearningStatus PROTO ((UINT4 u4ContextId, UINT4 u4Fid, UINT4 u4Port, UINT1 u1Action, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwSetProtocolTunnelStatusOnPort PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, tVlanHwTunnelFilters ProtocolId, UINT4 u4TunnelStatus, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwSetPortProtectedStatus PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, INT4 u4ProtectedStatus, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwSwitchMacLearningLimit PROTO ((UINT4 u4ContextId, UINT4 u4MacLimit, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwPortMacLearningStatus PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1Status, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwMacLearningLimit PROTO ((UINT4 u4ContextId, tVlanId VlanId, UINT2 u2FdbId, UINT4 u4MacLimit, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwSetPortProperty PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, tHwVlanPortProperty VlanPortProperty, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwAddStaticUcastEntryEx PROTO ((UINT4 u4ContextId, UINT4 u4FdbId, tMacAddr MacAddr, UINT4 u4RcvPort, tHwPortArray * pHwAllowedToGoPorts, UINT1 u1Status, tMacAddr ConnectionId, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmSyncFDBInfo PROTO ((tFDBInfoArray * pFDBInfoArray, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwSetPortIngressEtherType PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2EtherType, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwSetPortEgressEtherType PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2EtherType, tMbsmSlotInfo * pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwEvbConfigSChIface PROTO ((tVlanEvbHwConfigInfo *pVlanEvbHwConfigInfo,tMbsmSlotInfo *    pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwSetBridgePortType PROTO ((tVlanHwPortInfo *pVlanHwPortInfo, tMbsmSlotInfo 
            *pSlotInfo));
UINT1 VlanFsMiVlanMbsmHwPortPktReflectStatus PROTO ((tFsNpVlanPortReflectEntry * pPortReflectEntry));

#endif /* MBSM_WANTED */ 
#ifdef PB_WANTED
UINT1 VlanFsMiVlanHwSetProviderBridgePortType PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT4 u4PortType));
UINT1 VlanFsMiVlanHwSetPortSVlanTranslationStatus PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1Status));
UINT1 VlanFsMiVlanHwAddSVlanTranslationEntry PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2LocalSVlan, UINT2 u2RelaySVlan));
UINT1 VlanFsMiVlanHwDelSVlanTranslationEntry PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2LocalSVlan, UINT2 u2RelaySVlan));
UINT1 VlanFsMiVlanHwSetPortEtherTypeSwapStatus PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1Status));
UINT1 VlanFsMiVlanHwAddEtherTypeSwapEntry PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2LocalEtherType, UINT2 u2RelayEtherType));
UINT1 VlanFsMiVlanHwDelEtherTypeSwapEntry PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2LocalEtherType, UINT2 u2RelayEtherType));
UINT1 VlanFsMiVlanHwAddSVlanMap PROTO ((UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap));
INT4 VlanFsMiVlanHwSetCVlanStat PROTO (( UINT4 u4ContextId,tHwVlanCVlanStat VlanSVlanMap));
UINT1
VlanFsMiVlanHwClearCVlanStats PROTO ((UINT4 u4ContextId, UINT2 u2Port, UINT2 u2CVlanId));
UINT1 VlanFsMiVlanHwDeleteSVlanMap PROTO ((UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap));
UINT1 VlanFsMiVlanHwSetPortSVlanClassifyMethod PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1TableType));
UINT1 VlanFsMiVlanHwPortMacLearningLimit PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT4 u4MacLimit));
UINT1 VlanFsMiVlanHwMulticastMacTableLimit PROTO ((UINT4 u4ContextId, UINT4 u4MacLimit));
UINT1 VlanFsMiVlanHwSetPortCustomerVlan PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId CVlanId));
UINT1 VlanFsMiVlanHwResetPortCustomerVlan PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex));
UINT1 VlanFsMiVlanHwCreateProviderEdgePort PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId SVlanId, tHwVlanPbPepInfo PepConfig));
UINT1 VlanFsMiVlanHwDelProviderEdgePort PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId SVlanId));
UINT1 VlanFsMiVlanHwSetPepPvid PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId SVlanId, tVlanId Pvid));
UINT1 VlanFsMiVlanHwSetPepAccFrameType PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId SVlanId, UINT1 u1AccepFrameType));
UINT1 VlanFsMiVlanHwSetPepDefUserPriority PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId SVlanId, INT4 i4DefUsrPri));
UINT1 VlanFsMiVlanHwSetPepIngFiltering PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId SVlanId, UINT1 u1IngFilterEnable));
UINT1 VlanFsMiVlanHwSetCvidUntagPep PROTO ((UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap));
UINT1 VlanFsMiVlanHwSetCvidUntagCep PROTO ((UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap));
UINT1 VlanFsMiVlanHwSetPcpEncodTbl PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, tHwVlanPbPcpInfo NpPbVlanPcpInfo));
UINT1 VlanFsMiVlanHwSetPcpDecodTbl PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, tHwVlanPbPcpInfo NpPbVlanPcpInfo));
UINT1 VlanFsMiVlanHwSetPortUseDei PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1UseDei));
UINT1 VlanFsMiVlanHwSetPortReqDropEncoding PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1ReqDrpEncoding));
UINT1 VlanFsMiVlanHwSetPortPcpSelection PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2PcpSelection));
UINT1 VlanFsMiVlanHwSetServicePriRegenEntry PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId SVlanId, INT4 i4RecvPriority, INT4 i4RegenPriority));
UINT1 VlanFsMiVlanHwSetTunnelMacAddress PROTO ((UINT4 u4ContextId, tMacAddr MacAddr, UINT2 u2Protocol));
UINT1 VlanFsMiVlanHwGetNextSVlanMap PROTO ((UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap, tVlanSVlanMap * pRetVlanSVlanMap));
UINT1 VlanFsMiVlanHwGetNextSVlanTranslationEntry PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId u2LocalSVlan, tVidTransEntryInfo * pVidTransEntryInfo));
INT1 VlanFsMiVlanHwSetEvcAttribute (INT4 i4FsEvcContextId, UINT4 u4Action, tEvcInfo * pIssEvcInfo);
UINT1 VlanFsMiVlanHwGetCVlanStats (UINT4 u4ContextId,UINT4 u2port, UINT2 u2CVlanId,UINT1 u1StatsType, UINT4 *pu4VlanStatsValue);

#endif /* PB_WANTED */
UINT1 VlanFsMiVlanHwSetMcastIndex PROTO ((tHwMcastIndexInfo HwMcastIndexInfo, UINT4 * pu4McastIndex));
UINT1 VlanFsMiVlanHwSetPortProperty PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, tHwVlanPortProperty VlanPortProperty));
UINT1 VlanFsMiVlanHwSetPortIngressEtherType PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2EtherType));
UINT1 VlanFsMiVlanHwSetPortEgressEtherType PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2EtherType));
UINT1 VlanFsMiVlanHwSetVlanLoopbackStatus PROTO ((UINT4 u4ContextId, tVlanId VlanId, INT4 i4LoopbackStatus));
UINT1 VlanFsMiVlanHwEvbConfigSChIface PROTO ((tVlanEvbHwConfigInfo *pVlanEvbHwConfigInfo));
UINT1 VlanFsMiVlanMbsmHwEvbConfigSChIface PROTO ((tVlanEvbHwConfigInfo *pVlanEvbHwConfigInfo,tMbsmSlotInfo * pSlotInfo));
UINT1
VlanFsMiVlanHwGetPortFromFdb (UINT2 u2VlanId, UINT1 *i1pHwAddr, UINT4 *pu4Port);
UINT1 VlanFsMiVlanHwPortPktReflectStatus PROTO ((tFsNpVlanPortReflectEntry *pPortReflectEntry));
UINT1 VlanFsMiVlanHwSetBridgePortType PROTO ((tVlanHwPortInfo *pVlanHwPortInfo));


#endif /* __VLAN_NP_WR_H__ */ 
