/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: garp.h,v 1.47 2013/05/28 14:50:35 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              by  external modules.
 *
 *******************************************************************/
#ifndef _GARP_H
#define _GARP_H

#define GARP_SUCCESS        1
#define GARP_FAILURE        2
#define GARP_ENABLED        1
#define GARP_DISABLED       2
#define GARP_TRUE           1
#define GARP_FALSE          2
#define GARP_SNMP_TRUE      1
#define GARP_SNMP_FALSE     2

#define GVRP_SUCCESS  GARP_SUCCESS
#define GVRP_FAILURE  GARP_FAILURE
#define GVRP_ENABLED  GARP_ENABLED
#define GVRP_DISABLED GARP_DISABLED
#define GVRP_TRUE     GARP_TRUE
#define GVRP_FALSE    GARP_FALSE

#define GMRP_SUCCESS  GARP_SUCCESS
#define GMRP_FAILURE  GARP_FAILURE
#define GMRP_ENABLED  GARP_ENABLED
#define GMRP_DISABLED GARP_DISABLED
#define GMRP_TRUE     GARP_TRUE
#define GMRP_FALSE    GARP_FALSE

#define GARP_OPER_UP       1
#define GARP_OPER_DOWN     2

#define GARP_STAP_CIST_ID  MST_CIST_CONTEXT


#define GARP_PORT_CREATE_MSG          1
#define GARP_PORT_DELETE_MSG          2 
#define GARP_PORT_OPER_UP_MSG         3
#define GARP_PORT_OPER_DOWN_MSG       4
#define GARP_STAP_PORT_FWD_MSG        5
#define GARP_STAP_PORT_BLK_MSG        6
#define GARP_PROP_VLAN_INFO_MSG       7
#define GARP_PROP_MCAST_INFO_MSG      8
#define GARP_PROP_FWDALL_INFO_MSG     9
#define GARP_PROP_FWDUNREG_INFO_MSG  10
#define GARP_SET_VLAN_FORBID_MSG     11
#define GARP_SET_MCAST_FORBID_MSG    12
#define GARP_SET_FWDALL_FORBID_MSG   13
#define GARP_SET_FWDUNREG_FORBID_MSG 14
#define GARP_GIP_UPDATE_GIP_PORTS_MSG 15
#define GARP_CREATE_CONTEXT_MSG       16
#define GARP_DELETE_CONTEXT_MSG       17
#define GARP_PORT_MAP_MSG             18
#define GARP_PORT_UNMAP_MSG           19 
#define GARP_UPDATE_CONTEXT_NAME      20


#define GARP_BULK_MSG                 1
#define GARP_MSG                      2
#define GARP_VLAN_MAP_MSG             3
#define GARP_VLAN_LIST_MAP_MSG        4
#define GARP_RED_MSG                  5
#define GARP_RED_MCAST_ADD_MSG        6

/* List of messages to be processed inside GARP_VLAN_MAP_MSG */
#define GARP_MAP_INST_VLAN_MSG        MST_MAP_INST_VLAN_MSG
#define GARP_UNMAP_INST_VLAN_MSG      MST_UNMAP_INST_VLAN_MSG
#define GARP_MAP_INST_VLAN_LIST_MSG   MST_MAP_INST_VLAN_LIST_MSG
#define GARP_UNMAP_INST_VLAN_LIST_MSG MST_UNMAP_INST_VLAN_LIST_MSG

#define GARP_UPDATE_MAP_MSG           MST_UPDATE_INST_VLAN_MAP_MSG
#define GARP_POST_MAP_MSG             MST_POST_INST_VLAN_MAP_MSG
#define GARP_UPDNPOST_MAP_MSG         3

#define GARP_MAP_MSG_UPDATED          MST_INST_VLAN_MAP_MSG_UPDATED
#define GARP_MAP_MSG_POSTED           MST_INST_VLAN_MAP_MSG_POSTED

#define   GARP_NAME       ("GARP:")
#define   GVRP_NAME       ("GVRP:")
#define   GMRP_NAME       ("GMRP:")

#define GARP_MOD_TRC       0x00010000
#define GMRP_MOD_TRC       0x00020000
#define GVRP_MOD_TRC       0x00040000
#define GARP_RED_TRC       0x00080000 /* GARP Redundancy module traces */

#define GARP_TRC_TYPE_ALL (INIT_SHUT_TRC | MGMT_TRC | DATA_PATH_TRC | \
                           CONTROL_PLANE_TRC | DUMP_TRC | OS_RESOURCE_TRC | \
                           ALL_FAILURE_TRC | BUFFER_TRC)


#define GARP_MAX_PORTS_PER_CONTEXT  L2IWF_MAX_PORTS_PER_CONTEXT


#define GARP_PROVIDER_BRIDGE_MODE       VLAN_PROVIDER_BRIDGE_MODE
#define GARP_CUSTOMER_BRIDGE_MODE       VLAN_CUSTOMER_BRIDGE_MODE
#define GARP_PROVIDER_EDGE_BRIDGE_MODE  VLAN_PROVIDER_EDGE_BRIDGE_MODE
#define GARP_PROVIDER_CORE_BRIDGE_MODE  VLAN_PROVIDER_CORE_BRIDGE_MODE
#define GARP_INVALID_BRIDGE_MODE        VLAN_INVALID_BRIDGE_MODE
#define GARP_PBB_ICOMPONENT_BRIDGE_MODE VLAN_PBB_ICOMPONENT_BRIDGE_MODE 
#define GARP_PBB_BCOMPONENT_BRIDGE_MODE  VLAN_PBB_BCOMPONENT_BRIDGE_MODE



#define GARP_PROVIDER_NETWORK_PORT        CFA_PROVIDER_NETWORK_PORT
#define GARP_CNP_PORTBASED_PORT           CFA_CNP_PORTBASED_PORT
#define GARP_CNP_STAGGED_PORT             CFA_CNP_STAGGED_PORT
#define GARP_CUSTOMER_EDGE_PORT           CFA_CUSTOMER_EDGE_PORT
#define GARP_PROP_CUSTOMER_EDGE_PORT      CFA_PROP_CUSTOMER_EDGE_PORT
#define GARP_PROP_CUSTOMER_NETWORK_PORT   CFA_PROP_CUSTOMER_NETWORK_PORT
#define GARP_PROP_PROVIDER_NETWORK_PORT   CFA_PROP_PROVIDER_NETWORK_PORT
#define GARP_CUSTOMER_BRIDGE_PORT         CFA_CUSTOMER_BRIDGE_PORT

typedef struct _tGarpMsg {
   UINT2                 u2MsgType;
   UINT2                 u2Port; /*Local Port Identifier*/
   UINT2                 u2GipId;
   tMacAddr              MacAddr;
   tLocalPortList             AddedPorts;
   tLocalPortList             DeletedPorts;
} tGarpMsg;

typedef struct _tGarpBulkMsg {
   UINT2                 u2MsgType;
   tVlanId               VlanId;
   tMacAddr              MacAddr;
   tLocalPortList             Ports;
   UINT1                 au1Reserved[2];
} tGarpBulkMsg;

typedef struct _tGarpVidMapMsg {
    UINT2                u2MsgEvent;  /* Can be map/unmap VLAN List or single VLAN */
    UINT2                u2NewMapId;  /* Can be Instance Id (or Fid) */
    INT4                 i4EventCount; /* No of mapping posted - VlanMapInfo index */
    tVlanMapInfo         VlanMapInfo[1]; /* When List of VLAN is mapped */
} tGarpVidMapMsg;

typedef struct _tGarpRedMsg {
    UINT4 u4Events;
}tGarpRedMsg;

typedef struct _tGarpRedAddMsg {
    tVlanId    VlanId; 
    tMacAddr   MacAddr;
    tLocalPortList  PortList;
}tGarpRedAddMsg;

typedef struct _tGarpQMsg {
   INT4                  i4Count;    /* Count of msgs in union - 
                                        No. of Bulk/VidMap Messages */

   UINT4                 u4ContextId; /*Context  Identifier*/
   UINT4                 u4IfIndex;   /*Interface Index*/
   UINT2                 u2MsgType;
   UINT1                 au1Reserved[2];
   union
   {
       tGarpBulkMsg GarpBulkMsg [1]; /* Bulk Array of messages from VLAN at 
                                        GVRP/GMRP init time */
       tGarpMsg   GarpMsg;
       tGarpVidMapMsg    GarpVidMapMsg[1];
       tGarpRedMsg    RedMsg;
       tGarpRedAddMsg RedAddMsg;
   }
   unGarpMsg;
    /* !!!!!!!!!!!!!!!!!!!!!!!!CAUTION!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     * DONT DECLARE ANY VARIABLES BELOW THIS UNION. IF DECLARED, THEN THOSE
     * VARIABLES MAY GET CORRUPTED BECAUSE THERE ARE DYNAMICALLY GROWING 
     * ARRAYS INSIDE THIS UNION. 
     * REFER: GarpFillBulkMessage &  GarpMiUpdateInstVlanMap */
}tGarpQMsg;

#ifdef GARP_WANTED

typedef enum {
    GARP_ATTR_TYPE_START = 0,
    GARP_GROUP_ATTR_TYPE,
    GARP_SERVICE_REQ_ATTR_TYPE,
    GARP_ATTR_TYPE_END
} tGarpAttrType;

#define GARP_IS_GARP_ENABLED_IN_CONTEXT(u4ContextId)\
        GarpIsGarpEnabledInContext (u4ContextId)
#define GVRP_IS_GVRP_ENABLED_IN_CONTEXT(u4ContextId)\
        GvrpIsGvrpEnabledInContext (u4ContextId)
#define GMRP_IS_GMRP_ENABLED_IN_CONTEXT(u4ContextId)\
        GmrpIsGmrpEnabledInContext (u4ContextId)
    
#else
    
#define GARP_IS_GARP_ENABLED_IN_CONTEXT(u4ContextId) GARP_FALSE
#define GVRP_IS_GVRP_ENABLED_IN_CONTEXT(u4ContextId) GARP_FALSE
#define GMRP_IS_GMRP_ENABLED_IN_CONTEXT(u4ContextId) GARP_FALSE
    
#endif


INT4
GarpIsGarpEnabledInContext PROTO ((UINT4 u4ContextId));
INT4 
GvrpIsGvrpEnabledInContext PROTO ((UINT4 u4ContextId));
INT4
GmrpIsGmrpEnabledInContext PROTO ((UINT4 u4ContextId));

INT4
GarpMessageInd                 PROTO ((tCRU_BUF_CHAIN_DESC *pFrame, 
                                       UINT2       u2InPort, 
                                       UINT2       u2GipId));


VOID GarpMain PROTO ((INT1 *));

VOID
GvrpPropagateVlanInfo          PROTO ((UINT4        u4ContextId,
                                       tVlanId      VlanId,
                                       tLocalPortList    AddPortList, 
                                       tLocalPortList    DelPortList));

VOID
GvrpSetVlanForbiddenPorts      PROTO ((UINT4        u4ContextId,
                                       tVlanId      VlanId,
                                       tLocalPortList    AddPortList,
                                       tLocalPortList    DelPortList));

VOID
GmrpPropagateMcastInfo         PROTO ((UINT4        u4ContextId,
                                       tMacAddr     MacAddr, 
                                       tVlanId      VlanId,
                                       tLocalPortList    AddPortList, 
                                       tLocalPortList    DelPortList));

VOID
GmrpSetMcastForbiddenPorts     PROTO ((UINT4        u4ContextId,
                                       tMacAddr     MacAddr, 
                                       tVlanId      VlanId,
                                       tLocalPortList    AddPortList,
                                       tLocalPortList    DelPortList));

VOID
GmrpPropagateDefGroupInfo      PROTO ((UINT4        u4ContextId,
                                       UINT1        u1Type, 
                                       tVlanId      VlanId,
                                       tLocalPortList    AddPortList, 
                                       tLocalPortList    DelPortList));

VOID
GmrpSetDefGroupForbiddenPorts  PROTO ((UINT4        u4ContextId,
                                       UINT1        u1Type, 
                                       tVlanId      VlanId,
                                       tLocalPortList    AddPortList,
                                       tLocalPortList    DelPortList));
VOID
GarpStapPortStateChange        PROTO ((UINT2 u2Port, UINT1 u1State, UINT2 u2GipId));

VOID
GarpGipUpdateGipPorts          PROTO ((UINT4 u4ContextId,
                                       tLocalPortList AddPortList, 
                                       tLocalPortList DelPortList, 
                                       UINT2 u2GipId)); 

INT4 GarpCreatePort            PROTO ((UINT4, UINT4, UINT2 u2Port));
INT4 GarpDeletePort            PROTO ((UINT2 u2Port));
INT4 GarpPortOperInd           PROTO ((UINT2 u2Port, UINT1 u1OperStatus));

INT4 GarpWrPortOperInd           PROTO ((UINT2 u2Port, UINT1 u1OperStatus,
                                         tL2IwfContextInfo * pContextInfo));

INT4 GmrpInit                  PROTO ((VOID));
INT4 GmrpAddPort               PROTO ((UINT2 u2Port));
INT4 GmrpDelPort               PROTO ((UINT2 u2Port));

INT4 
GarpLock                        PROTO ((VOID));

INT4 
GarpUnLock                      PROTO ((VOID));

INT4 
GarpFillBulkMessage PROTO ((UINT4 u4ContextId, tGarpQMsg **ppGarpQMsg, 
                            UINT1 u1MsgType,
                            tVlanId VlanId, tLocalPortList Ports,
                            UINT1 *pu1MacAddr));

 
INT4
GarpPostBulkCfgMessage PROTO ((tGarpQMsg *pGarpQMsg));

VOID
GarpMiUpdateInstVlanMap PROTO ((UINT4 u4ContextId, UINT1 u1Action, UINT2 u2NewMapId, 
                              UINT2 u2OldMapId, tVlanId VlanId, 
                              UINT2 u2MapEvent, UINT1 *pu1Result));

#define GARP_LOCK()     GarpLock ()
#define GARP_UNLOCK()   GarpUnLock ()

VOID GarpRedSendMsg (UINT4 u4MessageType);

INT4 GarpRestartModule (VOID);

VOID GarpAppReleaseMemory (void);
UINT4
GarpCreateContext PROTO ((UINT4 u4ContextId));

UINT4
GarpDeleteContext PROTO ((UINT4 u4ContextId));
INT4
GarpMapPort (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2LocalPortId);
INT4 GarpUnmapPort (UINT2 u2IfIndex);
UINT4 GarpUpdateContextName (UINT4 u4ContextId);

INT1 nmhGetDot1qFutureGarpShutdownStatus (INT4  *);
INT1 nmhSetDot1qFutureGarpShutdownStatus (INT4 );
INT1 nmhTestv2Dot1qFutureGarpShutdownStatus (UINT4 *, INT4);
INT1 nmhTestv2Dot1qGvrpStatus (UINT4 *, INT4 );
INT1 nmhSetDot1qGvrpStatus (INT4 );
INT1 nmhGetDot1qGvrpStatus (INT4 *);
INT1 nmhTestv2Dot1qPortGvrpStatus (UINT4 *, INT4 , INT4 );
INT1 nmhSetDot1qPortGvrpStatus (INT4 , INT4 );
INT1 nmhGetDot1qPortGvrpStatus (INT4 , INT4 *);
INT1 nmhGetDot1qPortGvrpFailedRegistrations (INT4 , UINT4 *);
INT1 nmhGetDot1qPortGvrpLastPduOrigin (INT4 , tMacAddr *);
INT1 nmhSetDot1qPortRestrictedVlanRegistration (INT4 , INT4);
INT1 nmhGetDot1qPortRestrictedVlanRegistration (INT4 , INT4 *);
INT1 nmhTestv2Dot1dGmrpStatus (UINT4 *, INT4 );
INT1 nmhSetDot1dGmrpStatus (INT4 );
INT1 nmhGetDot1dGmrpStatus (INT4 *);
INT1 nmhTestv2Dot1dPortGarpJoinTime (UINT4 *, INT4 , INT4 );
INT1 nmhSetDot1dPortGarpJoinTime (INT4 , INT4 );
INT1 nmhValidateIndexInstanceDot1dPortGarpTable (INT4 );
INT1 nmhGetDot1dPortGarpJoinTime (INT4 , INT4 *);
INT1 nmhTestv2Dot1dPortGarpLeaveTime (UINT4 *, INT4 , INT4 );
INT1 nmhSetDot1dPortGarpLeaveTime (INT4 , INT4 );
INT1 nmhGetDot1dPortGarpLeaveTime (INT4 , INT4 *);
INT1 nmhTestv2Dot1dPortGarpLeaveAllTime (UINT4 *, INT4 , INT4 );
INT1 nmhSetDot1dPortGarpLeaveAllTime (INT4 , INT4 );
INT1 nmhGetDot1dPortGarpLeaveAllTime (INT4 , INT4 *);
INT1 nmhTestv2Dot1dPortGmrpStatus (UINT4 *, INT4 , INT4 );
INT1 nmhSetDot1dPortGmrpStatus (INT4 , INT4 );
INT1 nmhValidateIndexInstanceDot1dPortGmrpTable (INT4 );
INT1 nmhGetDot1dPortGmrpStatus (INT4 , INT4 *);
INT1 nmhGetDot1dPortGmrpFailedRegistrations (INT4 , UINT4 *);
INT1 nmhGetDot1dPortGmrpLastPduOrigin (INT4 , tMacAddr *);
INT1 nmhTestv2Dot1dPortRestrictedGroupRegistration (UINT4 *, INT4 , INT4 );
INT1 nmhGetDot1dPortRestrictedGroupRegistration (INT4 , INT4 *);
INT1 nmhGetNextIndexDot1dPortGmrpTable (INT4 , INT4 *);
INT1 nmhGetFirstIndexDot1dPortGmrpTable (INT4 *);
INT1 nmhGetNextIndexDot1dPortGarpTable (INT4 , INT4 *);
INT1 nmhGetFirstIndexDot1dPortGarpTable (INT4 *);
INT1 nmhGetDot1qFutureGvrpOperStatus (INT4 *);
INT1 nmhGetDot1qFutureGmrpOperStatus (INT4 *);
INT1 nmhSetDot1dPortRestrictedGroupRegistration (INT4 , INT4 );
INT1 nmhTestv2Dot1qPortRestrictedVlanRegistration (UINT4 *, INT4 , INT4 );

INT1 nmhGetDot1qFutureVlanPortGmrpJoinEmptyTxCount(INT4 , UINT4 *);
INT1 nmhGetDot1qFutureVlanPortGmrpJoinEmptyRxCount(INT4 , UINT4 *);
INT1 nmhGetDot1qFutureVlanPortGmrpJoinInTxCount(INT4 , UINT4 *);
INT1 nmhGetDot1qFutureVlanPortGmrpJoinInRxCount(INT4 , UINT4 *);
INT1 nmhGetDot1qFutureVlanPortGmrpLeaveInTxCount(INT4 , UINT4 *);
INT1 nmhGetDot1qFutureVlanPortGmrpLeaveInRxCount(INT4 , UINT4 *);
INT1 nmhGetDot1qFutureVlanPortGmrpLeaveEmptyTxCount(INT4 , UINT4 *);
INT1 nmhGetDot1qFutureVlanPortGmrpLeaveEmptyRxCount(INT4 , UINT4 *);
INT1 nmhGetDot1qFutureVlanPortGmrpEmptyTxCount(INT4 , UINT4 *);
INT1 nmhGetDot1qFutureVlanPortGmrpEmptyRxCount(INT4 , UINT4 *);
INT1 nmhGetDot1qFutureVlanPortGmrpLeaveAllTxCount(INT4 , UINT4 *);
INT1 nmhGetDot1qFutureVlanPortGmrpLeaveAllRxCount(INT4 , UINT4 *);
INT1 nmhGetDot1qFutureVlanPortGmrpDiscardCount(INT4 , UINT4 *);
INT1 nmhGetDot1qFutureVlanPortGvrpJoinEmptyTxCount(INT4 , UINT4 *);
INT1 nmhGetDot1qFutureVlanPortGvrpJoinEmptyRxCount(INT4 , UINT4 *);
INT1 nmhGetDot1qFutureVlanPortGvrpJoinInTxCount(INT4 , UINT4 *);
INT1 nmhGetDot1qFutureVlanPortGvrpJoinInRxCount(INT4 , UINT4 *);
INT1 nmhGetDot1qFutureVlanPortGvrpLeaveInTxCount(INT4 , UINT4 *);
INT1 nmhGetDot1qFutureVlanPortGvrpLeaveInRxCount(INT4 , UINT4 *);
INT1 nmhGetDot1qFutureVlanPortGvrpLeaveEmptyTxCount(INT4 , UINT4 *);
INT1 nmhGetDot1qFutureVlanPortGvrpLeaveEmptyRxCount(INT4 , UINT4 *);
INT1 nmhGetDot1qFutureVlanPortGvrpEmptyTxCount(INT4 , UINT4 *);
INT1 nmhGetDot1qFutureVlanPortGvrpEmptyRxCount(INT4 , UINT4 *);
INT1 nmhGetDot1qFutureVlanPortGvrpLeaveAllTxCount(INT4 , UINT4 *);
INT1 nmhGetDot1qFutureVlanPortGvrpLeaveAllRxCount(INT4 , UINT4 *);
INT1 nmhGetDot1qFutureVlanPortGvrpDiscardCount(INT4 , UINT4 *);

INT1
nmhGetFsDot1qGvrpStatus (INT4 i4FsDot1qVlanContextId,
                         INT4 *pi4RetValFsDot1qGvrpStatus);
INT1
nmhSetFsDot1qGvrpStatus (INT4 i4FsDot1qVlanContextId,
                         INT4 i4SetValFsDot1qGvrpStatus);
INT1
nmhTestv2FsDot1qGvrpStatus (UINT4 *pu4ErrorCode, INT4 i4FsDot1qVlanContextId,
                            INT4 i4TestValFsDot1qGvrpStatus);
INT1
nmhGetFsDot1qPortGvrpStatus (INT4 i4FsDot1dBasePort,
                             INT4 *pi4RetValFsDot1qPortGvrpStatus);
INT1
nmhGetFsDot1qPortGvrpFailedRegistrations (INT4 i4FsDot1dBasePort,
                                          UINT4 *);
INT1
nmhGetFsDot1qPortGvrpLastPduOrigin (INT4 i4FsDot1dBasePort,
                                    tMacAddr * );
INT1
nmhGetFsDot1qPortRestrictedVlanRegistration (INT4 i4FsDot1dBasePort,
                                             INT4 *);
INT1
nmhSetFsDot1qPortGvrpStatus (INT4 i4FsDot1dBasePort,
                             INT4 i4SetValFsDot1qPortGvrpStatus);
INT1
nmhSetFsDot1qPortRestrictedVlanRegistration (INT4 , INT4 );

INT1
nmhTestv2FsDot1qPortGvrpStatus (UINT4 *pu4ErrorCode, INT4 i4FsDot1dBasePort,
                                INT4 i4TestValFsDot1qPortGvrpStatus);
INT1
nmhTestv2FsDot1qPortRestrictedVlanRegistration (UINT4 *pu4ErrorCode,
                                                INT4 , INT4 );
INT1
nmhGetFsDot1dGmrpStatus (INT4 i4FsDot1dBridgeContextId,
                         INT4 *pi4RetValFsDot1dGmrpStatus);
       
INT1
nmhSetFsDot1dGmrpStatus (INT4 i4FsDot1dBridgeContextId,
                         INT4 i4SetValFsDot1dGmrpStatus);

INT1
nmhTestv2FsDot1dGmrpStatus (UINT4 *pu4ErrorCode, INT4 i4FsDot1dBridgeContextId,
                            INT4 i4TestValFsDot1dGmrpStatus);

INT1
nmhGetNextIndexFsDot1dPortGarpTable (INT4 i4FsDot1dBasePort,
                                     INT4 *pi4NextFsDot1dBasePort);
INT1
nmhGetFirstIndexFsDot1dPortGarpTable (INT4 *pi4FsDot1dBasePort);

INT1
nmhValidateIndexInstanceFsDot1dPortGarpTable (INT4 i4FsDot1dBasePort);

INT1
nmhGetFsDot1dPortGarpJoinTime (INT4 i4FsDot1dBasePort,
                               INT4 *pi4RetValFsDot1dPortGarpJoinTime);

INT1
nmhGetFsDot1dPortGarpLeaveTime (INT4 i4FsDot1dBasePort,
                                INT4 *pi4RetValFsDot1dPortGarpLeaveTime);
INT1
nmhGetFsDot1dPortGarpLeaveAllTime (INT4 i4FsDot1dBasePort,
                                   INT4 *pi4RetValFsDot1dPortGarpLeaveAllTime);

INT1
nmhSetFsDot1dPortGarpJoinTime (INT4 i4FsDot1dBasePort,
                               INT4 i4SetValFsDot1dPortGarpJoinTime);

INT1
nmhSetFsDot1dPortGarpLeaveTime (INT4 i4FsDot1dBasePort,
                                INT4 i4SetValFsDot1dPortGarpLeaveTime);

INT1
nmhSetFsDot1dPortGarpLeaveAllTime (INT4 i4FsDot1dBasePort,
                                   INT4 i4SetValFsDot1dPortGarpLeaveAllTime);

INT1
nmhTestv2FsDot1dPortGarpJoinTime (UINT4 *pu4ErrorCode, INT4 i4FsDot1dBasePort,
                                  INT4 i4TestValFsDot1dPortGarpJoinTime);

INT1
nmhTestv2FsDot1dPortGarpLeaveTime (UINT4 *pu4ErrorCode, INT4 i4FsDot1dBasePort,
                                   INT4 i4TestValFsDot1dPortGarpLeaveTime);

INT1
nmhTestv2FsDot1dPortGarpLeaveAllTime (UINT4 *pu4ErrorCode,
                                      INT4 i4FsDot1dBasePort,
                                      INT4 i4TestValFsDot1dPortGarpLeaveAllTime);
INT1
nmhGetNextIndexFsDot1dPortGmrpTable (INT4 i4FsDot1dBasePort,
                                     INT4 *pi4NextFsDot1dBasePort);

INT1
nmhGetFirstIndexFsDot1dPortGmrpTable (INT4 *pi4FsDot1dBasePort);

INT1
nmhValidateIndexInstanceFsDot1dPortGmrpTable (INT4 i4FsDot1dBasePort);

INT1
nmhGetFsDot1dPortGmrpStatus (INT4 i4FsDot1dBasePort,
                             INT4 *pi4RetValFsDot1dPortGmrpStatus);

INT1
nmhGetFsDot1dPortGmrpFailedRegistrations (INT4 i4FsDot1dBasePort, UINT4 *);

INT1
nmhGetFsDot1dPortGmrpLastPduOrigin (INT4 i4FsDot1dBasePort, tMacAddr *);

INT1
nmhGetFsDot1dPortRestrictedGroupRegistration (INT4 i4FsDot1dBasePort, INT4 *);

INT1
nmhSetFsDot1dPortGmrpStatus (INT4 i4FsDot1dBasePort, INT4 );

INT1
nmhSetFsDot1dPortRestrictedGroupRegistration (INT4, INT4);

INT1
nmhTestv2FsDot1dPortGmrpStatus (UINT4 *, INT4 , INT4 );

INT1
nmhTestv2FsDot1dPortRestrictedGroupRegistration (UINT4 *, INT4 , INT4 );

INT1
nmhGetFsMIDot1qFutureGarpShutdownStatus (INT4 , INT4 *);

INT1
nmhGetFsMIDot1qFutureGvrpOperStatus (INT4 , INT4 *);

INT1
nmhGetFsMIDot1qFutureGmrpOperStatus (INT4 , INT4 *);

INT1
nmhSetFsMIDot1qFutureGarpShutdownStatus (INT4 , INT4);

INT1
nmhTestv2FsMIDot1qFutureGarpShutdownStatus (UINT4 *, INT4 , INT4 );
  
INT1
nmhDepv2FsDot1dPortGarpTable PROTO ((UINT4 *pu4ErrorCode, 
                                     tSnmpIndexList * pSnmpIndexList, 
                                     tSNMP_VAR_BIND* pSnmpVarBind));

INT1
nmhDepv2FsDot1dPortGmrpTable PROTO ((UINT4 *pu4ErrorCode, 
                                     tSnmpIndexList *pSnmpIndexList,                                                                  tSNMP_VAR_BIND *pSnmpVarBind ));

INT1
nmhDepv2FsDot1dExtBaseTable PROTO ((UINT4 *pu4ErrorCode, 
                                    tSnmpIndexList *pSnmpIndexList, 
                                    tSNMP_VAR_BIND *pSnmpVarBind));

INT1
nmhDepv2FsMIDot1qFutureGarpGlobalTrace PROTO ((UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind));
INT1
nmhDepv2Dot1dPortGmrpTable PROTO ((UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind));

INT1
nmhDepv2Dot1dPortGarpTable  PROTO ((UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind));

INT1
nmhDepv2Dot1dGmrpStatus  PROTO ((UINT4 *pu4ErrorCode, 
                         tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind));

INT1
nmhDepv2Dot1qGvrpStatus  PROTO ((UINT4 *pu4ErrorCode, 
                         tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind));

INT1
nmhDepv2Dot1qFutureGarpShutdownStatus  PROTO ((UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind));
#endif
