/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igsnpwr.h,v 1.4 2017/08/01 13:49:04 siva Exp $
 *
 * Description: Contains sructure definitions, macros and functions to be used
 *              by  external modules.
 *
 *******************************************************************/
#ifndef __IGS_NP_WR_H
#define __IGS_NP_WR_H

#include "snp.h"

/* OPER ID */
/* when new NP call is added in IGS, the following files
 * has to be updated
 * 1. npgensup.h
 * 2. npbcmsup.h
*/

enum {
      FS_MI_IGS_HW_CLEAR_IPMC_FWD_ENTRIES   = 1,                 
      FS_MI_IGS_HW_DISABLE_IGMP_SNOOPING    ,                 
      FS_MI_IGS_HW_DISABLE_IP_IGMP_SNOOPING ,                 
      FS_MI_IGS_HW_ENABLE_IGMP_SNOOPING     ,                 
      FS_MI_IGS_HW_ENABLE_IP_IGMP_SNOOPING  ,                 
      FS_MI_IGS_HW_ENHANCED_MODE            ,                 
      FS_MI_IGS_HW_PORT_RATE_LIMIT          ,                 
      FS_MI_IGS_HW_UPDATE_IPMC_ENTRY        ,                 
      FS_MI_NP_GET_FWD_ENTRY_HIT_BIT_STATUS ,                 
      FS_MI_NP_UPDATE_IPMC_FWD_ENTRIES      ,                 
      FS_MI_IGS_HW_ADD_RTR_PORT             ,                 
      FS_MI_IGS_HW_GET_IP_FWD_ENTRY_HIT_BIT_STATUS,           
      FS_MI_IGS_HW_SPARSE_MODE                    ,           
      FS_MI_IGS_HW_DEL_RTR_PORT                   ,                 
#ifdef MBSM_WANTED
      FS_MI_IGS_MBSM_HW_ENABLE_IGMP_SNOOPING      ,           
      FS_MI_IGS_MBSM_HW_ENABLE_IP_IGMP_SNOOPING   ,           
      FS_MI_IGS_MBSM_HW_ENHANCED_MODE             ,           
      FS_MI_IGS_MBSM_HW_PORT_RATE_LIMIT           ,           
      FS_MI_IGS_MBSM_HW_SPARSE_MODE               ,           
      FS_MI_IGS_MBSM_SYNC_I_P_M_C_INFO            ,
#endif
      FS_MI_IGS_MAX_NP
};

/* Required arguments list for the igs NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

typedef struct {
    UINT4  u4Instance;
} tIgsNpWrFsMiIgsHwClearIpmcFwdEntries;

typedef struct {
    UINT4  u4Instance;
} tIgsNpWrFsMiIgsHwDisableIgmpSnooping;

typedef struct {
    UINT4  u4Instance;
} tIgsNpWrFsMiIgsHwDisableIpIgmpSnooping;

typedef struct {
    UINT4  u4Instance;
} tIgsNpWrFsMiIgsHwEnableIgmpSnooping;

typedef struct {
    UINT4  u4Instance;
} tIgsNpWrFsMiIgsHwEnableIpIgmpSnooping;

typedef struct {
    UINT4  u4ContextId;
    UINT1  u1EnhStatus;
    UINT1  au1pad[3];
} tIgsNpWrFsMiIgsHwEnhancedMode;

typedef struct {
    UINT4            u4Instance;
    tIgsHwRateLmt *  pIgsHwRateLmt;
    UINT1            u1Action;
    UINT1            au1pad[3];
} tIgsNpWrFsMiIgsHwPortRateLimit;

typedef struct {
    UINT4              u4Instance;
    tIgsHwIpFwdInfo *  pIgsHwIpFwdInfo;
    UINT1              u1Action;
    UINT1              au1pad[3];
} tIgsNpWrFsMiIgsHwUpdateIpmcEntry;

typedef struct {
    UINT4         u4Instance;
    UINT4         u4GrpAddr;
    UINT4         u4SrcAddr;
    tSnoopVlanId  VlanId;
    UINT1         au1pad[2];
} tIgsNpWrFsMiNpGetFwdEntryHitBitStatus;

typedef struct {
    UINT1   *     PortList;
    UINT1   *     UntagPortList;
    UINT4         u4Instance;
    UINT4         u4GrpAddr;
    UINT4         u4SrcAddr;
    tSnoopVlanId  VlanId;
    UINT1         u1EventType;
    UINT1         au1pad[1];
} tIgsNpWrFsMiNpUpdateIpmcFwdEntries;

typedef struct _tIgsIPMCInfoArray {
   tIgsHwIpFwdInfo aIgsHwIpFwdInfo[MAX_SNP_IPMC_INFO];
   UINT4    u4IgsHwIPMCCount;
} tIgsIPMCInfoArray;

typedef struct {
    tIgsIPMCInfoArray *  pIgsIPMCInfoArray;
    tMbsmSlotInfo *      pSlotInfo;
} tIgsNpWrFsMiIgsMbsmSyncIPMCInfo;


typedef struct {
    UINT4         u4Port;
    UINT4         u4Instance;
    tSnoopVlanId  VlanId;
    UINT1         au1pad[2];
} tIgsNpWrFsMiIgsHwAddRtrPort;

typedef struct {
    UINT4         u4Port;
    UINT4         u4Instance;
    tSnoopVlanId  VlanId;
    UINT1         au1pad[2];
} tIgsNpWrFsMiIgsHwDelRtrPort;

#ifdef MBSM_WANTED
typedef struct {
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
} tIgsNpWrFsMiIgsMbsmHwEnableIgmpSnooping;

typedef struct {
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
} tIgsNpWrFsMiIgsMbsmHwEnableIpIgmpSnooping;

typedef struct {
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
    UINT1            u1EnhStatus;
    UINT1            au1pad[3];
} tIgsNpWrFsMiIgsMbsmHwEnhancedMode;

typedef struct {
    UINT4            u4Instance;
    tIgsHwRateLmt *  pIgsHwRateLmt;
    tMbsmSlotInfo *  pSlotInfo;
    UINT1            u1Action;
    UINT1            au1pad[3];
} tIgsNpWrFsMiIgsMbsmHwPortRateLimit;

typedef struct {
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
    UINT1            u1SparseStatus;
    UINT1            au1pad[3];
} tIgsNpWrFsMiIgsMbsmHwSparseMode;

#endif /* MBSM_WANTED */

typedef struct {
    UINT4              u4Instance;
    tIgsHwIpFwdInfo *  pIgsHwIpFwdInfo;
} tIgsNpWrFsMiIgsHwGetIpFwdEntryHitBitStatus;

typedef struct {
    UINT4  u4ContextId;
    UINT1  u1SparseStatus;
    UINT1  au1pad[3];
} tIgsNpWrFsMiIgsHwSparseMode;

typedef struct IgsNpModInfo {
union {
    tIgsNpWrFsMiIgsHwClearIpmcFwdEntries  sFsMiIgsHwClearIpmcFwdEntries;
    tIgsNpWrFsMiIgsHwDisableIgmpSnooping  sFsMiIgsHwDisableIgmpSnooping;
    tIgsNpWrFsMiIgsHwDisableIpIgmpSnooping  sFsMiIgsHwDisableIpIgmpSnooping;
    tIgsNpWrFsMiIgsHwEnableIgmpSnooping  sFsMiIgsHwEnableIgmpSnooping;
    tIgsNpWrFsMiIgsHwEnableIpIgmpSnooping  sFsMiIgsHwEnableIpIgmpSnooping;
    tIgsNpWrFsMiIgsHwEnhancedMode  sFsMiIgsHwEnhancedMode;
    tIgsNpWrFsMiIgsHwPortRateLimit  sFsMiIgsHwPortRateLimit;
    tIgsNpWrFsMiIgsHwUpdateIpmcEntry  sFsMiIgsHwUpdateIpmcEntry;
    tIgsNpWrFsMiNpGetFwdEntryHitBitStatus  sFsMiNpGetFwdEntryHitBitStatus;
    tIgsNpWrFsMiNpUpdateIpmcFwdEntries  sFsMiNpUpdateIpmcFwdEntries;
    tIgsNpWrFsMiIgsHwAddRtrPort  sFsMiIgsHwAddRtrPort;
    tIgsNpWrFsMiIgsHwDelRtrPort  sFsMiIgsHwDelRtrPort;
#ifdef MBSM_WANTED
    tIgsNpWrFsMiIgsMbsmHwEnableIgmpSnooping  sFsMiIgsMbsmHwEnableIgmpSnooping;
    tIgsNpWrFsMiIgsMbsmHwEnableIpIgmpSnooping  sFsMiIgsMbsmHwEnableIpIgmpSnooping;
    tIgsNpWrFsMiIgsMbsmHwEnhancedMode  sFsMiIgsMbsmHwEnhancedMode;
    tIgsNpWrFsMiIgsMbsmHwPortRateLimit  sFsMiIgsMbsmHwPortRateLimit;
    tIgsNpWrFsMiIgsMbsmHwSparseMode  sFsMiIgsMbsmHwSparseMode;
#endif /* MBSM_WANTED */
    tIgsNpWrFsMiIgsHwGetIpFwdEntryHitBitStatus  sFsMiIgsHwGetIpFwdEntryHitBitStatus;
    tIgsNpWrFsMiIgsHwSparseMode  sFsMiIgsHwSparseMode;
    tIgsNpWrFsMiIgsMbsmSyncIPMCInfo  sFsMiIgsMbsmSyncIPMCInfo;
    }unOpCode;

#define  IgsNpFsMiIgsHwClearIpmcFwdEntries  unOpCode.sFsMiIgsHwClearIpmcFwdEntries;
#define  IgsNpFsMiIgsHwDisableIgmpSnooping  unOpCode.sFsMiIgsHwDisableIgmpSnooping;
#define  IgsNpFsMiIgsHwDisableIpIgmpSnooping  unOpCode.sFsMiIgsHwDisableIpIgmpSnooping;
#define  IgsNpFsMiIgsHwEnableIgmpSnooping  unOpCode.sFsMiIgsHwEnableIgmpSnooping;
#define  IgsNpFsMiIgsHwEnableIpIgmpSnooping  unOpCode.sFsMiIgsHwEnableIpIgmpSnooping;
#define  IgsNpFsMiIgsHwEnhancedMode  unOpCode.sFsMiIgsHwEnhancedMode;
#define  IgsNpFsMiIgsHwPortRateLimit  unOpCode.sFsMiIgsHwPortRateLimit;
#define  IgsNpFsMiIgsHwUpdateIpmcEntry  unOpCode.sFsMiIgsHwUpdateIpmcEntry;
#define  IgsNpFsMiNpGetFwdEntryHitBitStatus  unOpCode.sFsMiNpGetFwdEntryHitBitStatus;
#define  IgsNpFsMiNpUpdateIpmcFwdEntries  unOpCode.sFsMiNpUpdateIpmcFwdEntries;
#define  IgsNpFsMiIgsHwAddRtrPort  unOpCode.sFsMiIgsHwAddRtrPort;
#define  IgsNpFsMiIgsHwDelRtrPort  unOpCode.sFsMiIgsHwDelRtrPort;
#ifdef MBSM_WANTED
#define  IgsNpFsMiIgsMbsmHwEnableIgmpSnooping  unOpCode.sFsMiIgsMbsmHwEnableIgmpSnooping;
#define  IgsNpFsMiIgsMbsmHwEnableIpIgmpSnooping  unOpCode.sFsMiIgsMbsmHwEnableIpIgmpSnooping;
#define  IgsNpFsMiIgsMbsmHwEnhancedMode  unOpCode.sFsMiIgsMbsmHwEnhancedMode;
#define  IgsNpFsMiIgsMbsmHwPortRateLimit  unOpCode.sFsMiIgsMbsmHwPortRateLimit;
#define  IgsNpFsMiIgsMbsmHwSparseMode  unOpCode.sFsMiIgsMbsmHwSparseMode;
#endif /* MBSM_WANTED */
#define  IgsNpFsMiIgsHwGetIpFwdEntryHitBitStatus  unOpCode.sFsMiIgsHwGetIpFwdEntryHitBitStatus;
#define  IgsNpFsMiIgsHwSparseMode  unOpCode.sFsMiIgsHwSparseMode;
#define  IgsNpFsMiIgsMbsmSyncIPMCInfo  unOpCode.sFsMiIgsMbsmSyncIPMCInfo;
} tIgsNpModInfo;


/* Function Prototype for Generic NP OpCode specific wrappers
 * defined in Igsnputil.c */

UINT1 IgsFsMiIgsHwClearIpmcFwdEntries PROTO ((UINT4 u4Instance));
UINT1 IgsFsMiIgsHwDisableIgmpSnooping PROTO ((UINT4 u4Instance));
UINT1 IgsFsMiIgsHwDisableIpIgmpSnooping PROTO ((UINT4 u4Instance));
UINT1 IgsFsMiIgsHwEnableIgmpSnooping PROTO ((UINT4 u4Instance));
UINT1 IgsFsMiIgsHwEnableIpIgmpSnooping PROTO ((UINT4 u4Instance));
UINT1 IgsFsMiIgsHwEnhancedMode PROTO ((UINT4 u4ContextId, UINT1 u1EnhStatus));
UINT1 IgsFsMiIgsHwPortRateLimit PROTO ((UINT4 u4Instance, tIgsHwRateLmt * pIgsHwRateLmt, UINT1 u1Action));
UINT1 IgsFsMiIgsHwUpdateIpmcEntry PROTO ((UINT4 u4Instance, tIgsHwIpFwdInfo * pIgsHwIpFwdInfo, UINT1 u1Action));
UINT1 IgsFsMiNpGetFwdEntryHitBitStatus PROTO ((UINT4 u4Instance, tSnoopVlanId VlanId, UINT4 u4GrpAddr, UINT4 u4SrcAddr));
UINT1 IgsFsMiNpUpdateIpmcFwdEntries PROTO ((UINT4 u4Instance, tSnoopVlanId VlanId, UINT4 u4GrpAddr, UINT4 u4SrcAddr, tPortList PortList, tPortList UntagPortList, UINT1 u1EventType));
UINT1 IgsFsMiIgsHwAddRtrPort PROTO ((UINT4 u4Instance, tSnoopVlanId VlanId, UINT2 u2Port));
UINT1 IgsFsMiIgsHwDelRtrPort PROTO ((UINT4 u4Instance, tSnoopVlanId VlanId, UINT2 u2Port));
#ifdef MBSM_WANTED
UINT1 IgsFsMiIgsMbsmHwEnableIgmpSnooping PROTO ((UINT4 u4ContextId, tMbsmSlotInfo * pSlotInfo));
UINT1 IgsFsMiIgsMbsmHwEnableIpIgmpSnooping PROTO ((UINT4 u4ContextId, tMbsmSlotInfo * pSlotInfo));
UINT1 IgsFsMiIgsMbsmHwEnhancedMode PROTO ((UINT4 u4ContextId, UINT1 u1EnhStatus, tMbsmSlotInfo * pSlotInfo));
UINT1 IgsFsMiIgsMbsmHwPortRateLimit PROTO ((UINT4 u4Instance, tIgsHwRateLmt * pIgsHwRateLmt, UINT1 u1Action, tMbsmSlotInfo * pSlotInfo));
UINT1 IgsFsMiIgsMbsmHwSparseMode PROTO ((UINT4 u4ContextId, UINT1 u1SparseStatus, tMbsmSlotInfo * pSlotInfo));
#endif /* MBSM_WANTED */
UINT1 IgsFsMiIgsHwGetIpFwdEntryHitBitStatus PROTO ((UINT4 u4Instance, tIgsHwIpFwdInfo * pIgsHwIpFwdInfo));
UINT1 IgsFsMiIgsHwSparseMode PROTO ((UINT4 u4ContextId, UINT1 u1SparseStatus));
UINT1 IgsFsMiIgsMbsmSyncIPMCInfo PROTO ((tIgsIPMCInfoArray * pIgsIPMCInfoArray, tMbsmSlotInfo * pSlotInfo));

#endif /* __IGS_NP_WR_H */
