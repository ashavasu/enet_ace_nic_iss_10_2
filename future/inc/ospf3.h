/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: ospf3.h,v 1.9 2017/12/26 13:34:20 siva Exp $
 *
 * Description: Exported file for ospfv3.
 *
 *******************************************************************/
#ifndef _OSPF3_H
#define _OSPF3_H

#ifdef MBSM_WANTED
#include "mbsm.h"
#endif

PUBLIC INT4        V3OspfSpawnTask    PROTO ((VOID));
PUBLIC VOID     RegisterOspf3Mibs  PROTO ((VOID));
PUBLIC VOID        UnRegisterOspf3Mibs  PROTO ((VOID));

/* Graceful restart support */
/* OSPFV3_RESTART_NONE and OSPFV3_RESTART_PLANNED are used to indicate
 * that currently no graceful restart process is initiated and
 * planned restart is in progress respectively */
#define OSPFV3_RESTART_NONE       1  /* Not supported */
#define OSPFV3_RESTART_PLANNED    2  /* Supports only planned restart */
#define OSPFV3_RESTART_BOTH       3  /* Supports both planned and unplanned */
#define OSPFV3_RTM6_ROUTE_PROCESS_EVENT   0x00004000

/*Macros for checking BFD monitoing OSPF path*/
#define OSPFV3_BFD_ENABLED      1
#define OSPFV3_BFD_DISABLED     2

/* router-id selection process flags */
#define  OSPFV3_ROUTERID_DYNAMIC               1
#define  OSPFV3_ROUTERID_PERMANENT             2

/* Maximum length of context alias */
#define OSPFV3_CXT_MAX_LEN     VCM_ALIAS_MAX_LEN

/* OSPFv3 default context identifier */
#define OSPFV3_DEFAULT_CXT_ID  0

/* bStatus values as defined in MIB */
#define  OSPFV3_ENABLED            1
#define  OSPFV3_DISABLED           2

/* OSPFv3 modes of operation */
#define OSPFV3_SI_MODE         VCM_SI_MODE
#define OSPFV3_MI_MODE         VCM_MI_MODE

/* Graceful Restart Reason */
#define OSPFV3_GR_UNKNOWN           0
#define OSPFV3_GR_SW_RESTART        1
#define OSPFV3_GR_SW_UPGRADE        2
#define OSPFV3_GR_SW_RED            3

/* Helper Support */
#define OSPFV3_GRACE_NO_HELPER       0x00
#define OSPFV3_GRACE_UNKNOWN                        0x01
#define OSPFV3_GRACE_SW_RESTART                     0x02
#define OSPFV3_GRACE_SW_RELOADUPGRADE               0x04
#define OSPFV3_GRACE_SW_REDUNDANT                   0x08
#define OSPFV3_GRACE_HELPER_ALL                     0x0F

/* Redundancy releated macros */
/* Macros to define the HA state */
#define OSPFV3_HA_STATE_INIT                1
#define OSPFV3_HA_STATE_ACTIVE_STANDBYUP    2
#define OSPFV3_HA_STATE_ACTIVE_STANDBYDOWN  3
#define OSPFV3_HA_STATE_STANDBY             4

/* Dynamic Bulk update status (MIB) */

#define OSPFV3_RED_DYN_BULK_NOT_STARTED  1
#define OSPFV3_RED_DYN_BULK_IN_PROGRESS  2
#define OSPFV3_RED_DYN_BULK_COMPLETED    3
#define OSPFV3_RED_DYN_BULK_ABORTED      4
#define OSPFV3_AT                      FLASH "ospf3at"

#ifdef MBSM_WANTED
/* MBSM Functions. */

INT4 V3OspfMbsmUpdateCardInsertion PROTO ((tMbsmSlotInfo *));

INT4 V3OspfMbsmUpdateCardStatus PROTO ((tMbsmProtoMsg *, INT4)) ;
#endif
PUBLIC INT4 V3OspfLock PROTO ((VOID));
PUBLIC INT4 V3OspfTimedLock PROTO ((VOID));
PUBLIC INT4 V3OspfUnLock PROTO ((VOID));
PUBLIC VOID V3OspfShutdownProcess PROTO ((VOID));
PUBLIC UINT4 V3OspfApiModuleStart PROTO ((VOID));
PUBLIC VOID V3OspfApiModuleShutDown PROTO ((VOID));
PUBLIC VOID
O3RedFileTransRestoreComplete PROTO ((VOID));
PUBLIC VOID V3OspfProcessLowPriQMsg PROTO ((tOsixQId QId));
PUBLIC VOID V3OspfCpuRelinquishHello PROTO ((VOID));
PUBLIC INT4 V3OspfIsLowPriorityMessage PROTO ((UINT4 u4MsgType));
PUBLIC INT4 V3OspfIsLowPriorityOspfPkt PROTO ((UINT1 u1OspfPktType));
PUBLIC VOID V3OSPFGetDynBulkSyncStatus PROTO ((UINT1 *pu1DynBulkUpdatStatus));
PUBLIC VOID V3OspfRmHandleGoActiveNotify PROTO ((VOID));



VOID UtilGetOspfv3RouterIdPermanence(INT4 *pi4RetValFutOspfv3RouterIdPermanence,
                                       UINT4 u4ContextId);


#endif /* _OSPF3_H */
