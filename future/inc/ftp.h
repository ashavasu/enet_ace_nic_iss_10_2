/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ftp.h,v 1.15 2007/02/01 14:52:30 iss Exp $
 *
 * Description:This file contains the exported definitions and  
 *             macros of FTP                                   
 *
 *******************************************************************/
#ifndef _FTP_H
#define _FTP_H

INT4 FtpInit (void);

VOID FtpTaskMain (INT1 *);

#endif /* _FTP_H */
