/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fskernel.h,v 1.23 2016/03/03 10:36:43 siva Exp $  
 *
 * Description: Contains KERNEL definitions, macros and 
 *              functions to be used by external modules.
 *
 *******************************************************************/
#ifndef  _FSKERNEL_H
#define  _FSKERNEL_H

/* lakern.c protos */
INT4 KernFsLaHwCreateAggGroup PROTO ((UINT2, UINT2 *));
INT4 KernFsLaHwAddLinkToAggGroup PROTO ((UINT2, UINT2, UINT2 *));
INT4 KernFsLaHwRemoveLinkFromAggGroup PROTO ((UINT2, UINT2));
INT4 KernFsLaHwDeleteAggregator PROTO ((UINT2));
VOID KernFsLaHwAddPortToConfAggGroup PROTO ((UINT2, UINT2));
VOID KernFsLaHwRemovePortFromConfAggGroup PROTO ((UINT2, UINT2));

/* pnackern.c protos */
INT4  KernPnacHwSetAuthStatus PROTO ((UINT2, UINT1 *, UINT1, UINT1, UINT1));


#ifdef IGMPPRXY_WANTED
VOID KernFsNpIgmpProxyInit (VOID);
VOID KernFsNpIgmpProxyDeInit (VOID);
#endif

/* igmchrdev.c protos */
#ifdef IGMP_WANTED
VOID KernFsIgmpHwEnableIgmp  PROTO ((VOID));
VOID KernFsIgmpHwDisableIgmp PROTO ((VOID));
#endif
/* pimchrdev.c protos */
#ifdef PIM_WANTED
VOID KernFsPimNpInitHw PROTO ((VOID));
VOID KernFsPimNpDeInitHw PROTO ((VOID));
#endif

#ifdef DVMRP_WANTED
VOID KernFsDvmrpNpInitHw PROTO ((VOID));
VOID KernFsDvmrpNpDeInitHw PROTO ((VOID));
#endif
#ifdef SNTP_WANTED
VOID KernFsSntpStatusInit PROTO ((INT4 i4SntpStatus));
#endif
#ifdef VRRP_WANTED
INT4 KernFsNpIpv4CreateVrrpInterface PROTO ((tVlanId, UINT4, UINT1 *));
INT4 KernFsNpIpv4DeleteVrrpInterface PROTO ((tVlanId, UINT4, UINT1 *));
INT4 KernFsNpIpv4AddVrrpAssocIp PROTO ((tVlanId, UINT4, UINT1 *));
INT4 KernFsNpIpv4DelVrrpAssocIp PROTO ((tVlanId, UINT4, UINT1 *));
#endif /*VRRP_WANTED*/
#ifndef NP_BACKWD_COMPATIBILITY
/* astmwrkn.c protos */
INT1 KernFsMiRstpNpSetPortState PROTO ((UINT4, UINT4, UINT1));
INT1 KernFsMiMstpNpAddVlanInstMapping PROTO ((UINT4, tVlanId, UINT2));
INT1 KernFsMiMstpNpAddVlanListInstMapping PROTO ((UINT4, UINT1 *, UINT2, UINT2, UINT2 *));
INT1 KernFsMiMstpNpDeleteInstance PROTO ((UINT4, UINT2));
INT1 KernFsMiMstpNpDelVlanInstMapping PROTO ((UINT4, tVlanId, UINT2));
INT1 KernFsMiMstpNpDelVlanListInstMapping PROTO ((UINT4, UINT1 *, UINT2, UINT2, UINT2 *));
INT4 KernFsMiMstpNpSetInstancePortState PROTO ((UINT4, UINT4, UINT2, UINT1));
UINT1 KernFsMiMstpNpGetInstancePortState PROTO ((UINT4, UINT4, UINT2));
INT1 KernFsMiPvrstNpCreateVlanSpanningTree PROTO ((UINT4 u4ContextId, UINT2 VlanId));
INT1 KernFsMiPvrstNpDeleteVlanSpanningTree PROTO ((UINT4 u4ContextId, UINT2 VlanId));
INT4 
KernFsMiPvrstNpSetVlanPortState PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, 
                                        UINT2 VlanId, UINT1 u1PortState));
/* vlnmwrkn.c protos */
INT4 KernFsMiVlanHwSetAllGroupsPorts PROTO ((UINT4, tVlanId, tHwPortArray *));
INT4 KernFsMiVlanHwResetAllGroupsPorts PROTO ((UINT4, tVlanId, tHwPortArray *));
INT4 KernFsMiVlanHwResetUnRegGroupsPorts PROTO ((UINT4, tVlanId, tHwPortArray *));
INT4 KernFsMiVlanHwSetUnRegGroupsPorts PROTO ((UINT4, tVlanId, tHwPortArray *));
INT4 KernFsMiVlanHwAddStaticUcastEntry PROTO ((UINT4, UINT4, tMacAddr, UINT2, tHwPortArray *, UINT1));
INT4 KernFsMiVlanHwDelStMcastEntry PROTO ((UINT4, tVlanId, tMacAddr, INT4));
INT4 KernFsMiVlanHwDelStaticUcastEntry PROTO ((UINT4, UINT4, tMacAddr, UINT2));
INT4
KernFsMiVlanHwGetFdbEntry PROTO ((UINT4, UINT4, tMacAddr, tHwUnicastMacEntry *));
INT4 KernFsMiVlanHwGetFirstTpFdbEntry PROTO ((UINT4, UINT4 *, tMacAddr ));
INT4 KernFsMiVlanHwGetNextTpFdbEntry PROTO ((UINT4, UINT4, tMacAddr, UINT4 *, tMacAddr));
INT4 KernFsMiVlanHwAddMcastEntry PROTO ((UINT4, tVlanId, tMacAddr, tHwPortArray *));
INT4 KernFsMiVlanHwAddStMcastEntry PROTO ((UINT4, tVlanId, tMacAddr, INT4, tHwPortArray *));
INT4 KernFsMiVlanHwSetMcastPort PROTO ((UINT4, tVlanId, tMacAddr, UINT2));
INT4 KernFsMiVlanHwResetMcastPort PROTO ((UINT4, tVlanId, tMacAddr, UINT2));
INT4 KernFsMiVlanHwDelMcastEntry PROTO ((UINT4, tVlanId, tMacAddr));
INT4 KernFsMiVlanHwAddVlanEntry PROTO ((UINT4, tVlanId, tHwPortArray *, tHwPortArray *));
INT4 KernFsMiVlanHwDelVlanEntry PROTO ((UINT4, tVlanId));
INT4 KernFsMiVlanHwSetVlanMemberPort PROTO ((UINT4, tVlanId, UINT2, INT1));
INT4 KernFsMiVlanHwResetVlanMemberPort PROTO ((UINT4, tVlanId, UINT2));
INT4 KernFsMiVlanHwSetPortAccFrameType PROTO ((UINT4, UINT2, INT1));
INT4 KernFsMiVlanHwSetPortIngFiltering PROTO ((UINT4, UINT2, UINT1));
INT4 KernFsMiVlanHwVlanEnable PROTO ((UINT4));
INT4 KernFsMiVlanHwVlanDisable PROTO ((UINT4));
INT4 KernFsMiVlanHwSetVlanLearningType PROTO ((UINT4, UINT1));
VOID KernFsMiNpDeleteAllFdbEntries PROTO ((UINT4));
INT4 KernFsMiVlanHwFlushPort PROTO ((UINT4, UINT4));
INT4 KernFsMiVlanHwFlushPortFdbId PROTO ((UINT4, UINT2, UINT4));
INT4 KernFsMiVlanHwFlushFdbId PROTO ((UINT4, UINT4));
INT4 KernFsMiVlanHwAssociateVlanFdb PROTO ((UINT4, UINT4, tVlanId));
INT4 KernFsMiVlanHwSetBrgMode PROTO ((UINT4 u4ContextId, UINT4 u4BridgeMode));
INT4 KernFsMiVlanHwEvbConfigSChIface PROTO ((tVlanEvbHwConfigInfo *));
INT4 KernFsMiVlanHwSetBridgePortType PROTO ((tVlanHwPortInfo *));

#ifdef ISS_METRO_WANTED
INT4 KernFsMiVlanHwSetPortEgressEtherType PROTO ((UINT4 u4ContextId, 
                                                  UINT4 u4IfIndex, 
                                                  UINT2 u2EtherType));
#endif /* ISS_METRO_WANTED */
#else
/* astpkern.c protos */
INT1 KernFsRstpNpSetPortState PROTO ((UINT4, UINT1));
INT1 KernFsMstpNpAddVlanInstMapping PROTO ((tVlanId, UINT2));
INT1 KernFsMstpNpAddVlanListInstMapping PROTO ((UINT1 *, UINT2, UINT2, UINT2 *));
INT1 KernFsMstpNpDeleteInstance PROTO ((UINT2));
INT1 KernFsMstpNpDelVlanInstMapping PROTO ((tVlanId, UINT2));
INT1 KernFsMstpNpDelVlanListInstMapping PROTO ((UINT1 *, UINT2, UINT2, UINT2 *));

INT4 KernFsMstpNpSetInstancePortState PROTO ((UINT4, UINT2, UINT1));

UINT1
KernFsMstpNpGetInstancePortState PROTO ((UINT4 u4IfIndex, 
                                         UINT2 u2InstanceId));

INT1 KernFsPvrstNpCreateVlanSpanningTree PROTO ((UINT2 VlanId));
INT1 KernFsPvrstNpDeleteVlanSpanningTree PROTO ((UINT2 VlanId));
INT4 KernFsPvrstNpSetVlanPortState PROTO ((UINT4 u4IfIndex, 
                                           UINT2 VlanId, UINT1 u1PortState));
/* vlankern.c protos */
#ifndef SW_LEARNING

INT4 KernFsVlanHwGetFdbEntry           PROTO ((UINT4, tMacAddr, tHwUnicastMacEntry *));
INT4 KernFsVlanHwGetFirstTpFdbEntry    PROTO ((UINT4 *, tMacAddr));
INT4 KernFsVlanHwGetNextTpFdbEntry     PROTO ((UINT4, tMacAddr, UINT4 *,
                                              tMacAddr));
VOID KernFsNpDeleteAllFdbEntries       PROTO ((VOID));
INT4 KernFsVlanHwFlushFdbId PROTO ((UINT4 u4Fid));
INT4 KernFsVlanHwFlushPort PROTO ((UINT4));
INT4 KernFsVlanHwFlushPortFdbId PROTO ((UINT2, UINT4));
#endif

INT4 KernFsVlanHwSetAllGroupsPorts PROTO ((tVlanId, tPortList));
INT4 KernFsVlanHwResetAllGroupsPorts PROTO ((tVlanId, tPortList));
INT4 KernFsVlanHwSetUnRegGroupsPorts PROTO ((tVlanId, tPortList));
INT4 KernFsVlanHwResetUnRegGroupsPorts PROTO ((tVlanId, tPortList));
INT4 KernFsVlanHwAddStaticUcastEntry   PROTO ((UINT4, tMacAddr, UINT2,
                                                 tPortList, UINT1));
INT4 KernFsVlanHwDelStaticUcastEntry   PROTO ((UINT4, tMacAddr, UINT2));
INT4 KernFsVlanHwAddMcastEntry         PROTO ((tVlanId, tMacAddr,
                                              tPortList));
INT4 KernFsVlanHwAddStMcastEntry       PROTO ((tVlanId, tMacAddr, INT4,
                                              tPortList));
INT4 KernFsVlanHwDelStMcastEntry       PROTO ((tVlanId VlanId, 
                                               tMacAddr McastAddr, 
                                               INT4 i4RcvPort));
INT4 KernFsVlanHwSetMcastPort          PROTO ((tVlanId, tMacAddr, UINT2));
INT4 KernFsVlanHwResetMcastPort        PROTO ((tVlanId, tMacAddr, UINT2));
INT4 KernFsVlanHwDelMcastEntry         PROTO ((tVlanId, tMacAddr));
INT4 KernFsVlanHwAddVlanEntry          PROTO ((tVlanId, tPortList, tPortList));
INT4 KernFsVlanHwDelVlanEntry          PROTO ((tVlanId));
INT4 KernFsVlanHwSetVlanMemberPort     PROTO ((tVlanId, UINT2, INT1));
INT4 KernFsVlanHwResetVlanMemberPort   PROTO ((tVlanId, UINT2));
INT4 KernFsVlanHwSetPortAccFrameType   PROTO ((UINT2, INT1));
INT4 KernFsVlanHwSetPortIngFiltering   PROTO ((UINT2, UINT1));
INT4 KernFsVlanHwSetVlanLearningType   PROTO ((UINT1));
INT4 KernFsVlanHwVlanEnable PROTO ((VOID));
INT4 KernFsVlanHwVlanDisable PROTO ((VOID));
INT4 KernFsVlanHwAssociateVlanFdb PROTO ((UINT4 u4Fid, tVlanId VlanId));
INT4 KernFsVlanHwSetBrgMode PROTO ((UINT4 u4BridgeMode));
INT4 KernFsVlanHwtVlanEvbHwConfigInfo PROTO ((tVlanEvbHwConfigInfo *));
INT4 KernFsVlanHwtVlanSetBridgePortType PROTO ((tVlanHwPortInfo *));

#ifdef ISS_METRO_WANTED
INT4
KernFsVlanHwSetPortEgressEtherType PROTO ((UINT4 u4IfIndex, UINT2 u2EtherType));
#endif /* ISS_METRO_WANTED */

#endif
#endif /* __FSKERNEL_H */
