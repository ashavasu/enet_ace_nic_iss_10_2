/********************************************************************
 * Copyright (C) Aricent Software Ltd.
 *
 * $Id: sipalg.h,v 1.3 2011/09/06 06:50:15 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              by SIP ALG.
 *
 *******************************************************************/

#ifndef __SIP_H__
#define __SIP_H__

/* IPC command identifiers */
/* --  LINK STATUS -- */
#define NAT_SIP_LINK_STATUS_UP                 1
#define NAT_SIP_LINK_STATUS_DOWN               2
#define NAT_SIP_LINK_STATUS_NOT_CONFIGURED     3

#define NAT_SIP_SUCCESS                        0
#define NAT_SIP_FAILURE                        1

     /* -- PROTOCOL ID -- */
#define NAT_SIP_ANY                        0
#define NAT_SIP_TCP                        1
#define NAT_SIP_UDP                        2
#define NAT_SIP_ACTIVE                     1

    /* -- LINK TYPE -- */
#define NAT_SIP_LINK_RA_VPN_TYPE               3
#define NAT_SIP_LINK_SITE_SITE_VPN_TYPE        4

typedef struct NatEntryInfo {
    UINT4  u4SrcIP;                   /* Source IP Address */
    UINT4  u4NATIP;                   /* NAT External IP */
    UINT4  u4DestIP;                   /* Remote IP */
    UINT4  u4Direction;               /* Direction of flow */
    UINT4  u4Enabled;                 /* Status */
    INT4   i4RowStatus;               /* Row status */
    UINT1  au1Desc[12];               /* Description */
    UINT2  u2SrcPort;                 /* Source Port  */
    UINT2  u2NATPort;                 /* NAT External Port */
    UINT2  u2DestPort;                /* Remote IP */
    UINT2  u2Protocol;                /* Protocol (NAT_TCP / NAT_UDP)  */
    UINT1  u1TimerFlag;
    UINT1  u1BindingType;             /*Type of Binding (SIGNALING/MEDIA)*/
    UINT2  u2NumBindings;             /*Number of Bindings   */
    UINT1  u1Parity;                  /*Parity (Any/Even/Odd)*/
    UINT1  au1Padding[3];
} tNatEntryInfo;

typedef struct PinholeInfo {
    UINT4  u4SrcIP;                   /* Source IP Address or NAT_ANY */
    UINT4  u4DestIP;                  /* Destination IP or NAT_ANY */
    UINT2  u2SrcPort;                 /* Source Port or NAT_ANY */
    UINT2  u2DestPort;                /* Destination Port / Base Port */
    UINT2  u2Protocol;                /* Protocol (NAT_TCP / NAT_UDP) */
    UINT2  u2Direction;               /* NAT_INBOUND / NAT_OUTBOUND */        
    UINT1  u1TimerFlag;
    UINT1  u1Parity;                  /* Base NAT Port Parity which would get
                                         alloacted if data flows from LAN to
                                         WAN before 200 OK */
    UINT2  u2NumPinholes;             /* Number of Pinholes To Be Added */
} tPinholeInfo;
    
/* Modified Structure */
typedef struct RouteInfo {
    UINT4 u4IpAddr;         
    UINT4 u4NextHop;       /* Next Hop IP Address for u4IpAddr */  
    UINT4 u4IfIndex;      
    UINT4 u4IfType;        /* Link type - LAN / WAN */ 
    UINT4 u4LinkIPAddr;    /* Link IP Address */
    UINT1 au1LinkName[20]; /* Link Name */
    UINT4 u4ErrCode;
} tRouteInfo;

typedef struct NatAsyncResponse {
    UINT4      u4Cookie;                 /* Cookie passed in the Request */
    UINT4      u4Status;                 /* NATIPC_SUCCESS or NATIPC_FAILURE */
    UINT4      u4ErrorCode;              /* One of the error codes 
                                            described above */
    UINT4      u4RetVal1;                /* First Return value */
    UINT4      u4RetVal2;                /* Second Return value */
    tRouteInfo routeInfo;                /* RouteInfo that is returned in the
                                            async response */
} tNatAsyncResponse;

typedef struct LinkEventInfo {
    UINT4 u4IpAddr;                   /* IP Address of the Link */
    UINT1 au1LinkPort[20];            /* Physical Link Name */    
    UINT2 u2LinkType;                 /* LINK_LAN_TYPE or LINK_WAN_TYPE */
    UINT2 u2Status;                   /* Up - Down - or Not Configured */
    UINT1 u1IfNatStatus;             /* If Nat Status */ 
    UINT1 u1IfFwlStatus;            /* If Firewall Status */
    UINT1 au1Padd[2];               /* for 4 byte alignment */
} tLinkInfo;

typedef struct EventNotificationInfo {
    UINT2 u2EventType;           /* LINK_STATUS_EVENT (or) PORT_MAPPING_EVENT */
    UINT1 au1Pad[2];
    union EventInformation {
        tNatEntryInfo natEntryInfo; /* Filled if Event is PORT_MAPPING_EVENT */
        tLinkInfo     linkInfo;     /* Filled if Event is LINK_STATUS_EVENT */
        tPinholeInfo  pinholeInfo;  /* Filled if Event is PINHOLE_EXPIRY_EVENT*/
    } EventInfo;
} tEventNotificationInfo;

/* New Structures */
typedef struct InIfInfo
{
    UINT4 u4SrcIPAddr;                 /* Source IP Address */ 
    UINT4 u4DestIPAddr;                /* Destination IP Address */ 
    UINT2 u2Protocol;                  /* Protocol No */
    UINT2 u2Reserved;
} tInIfInfo;

UINT4 
NatSipAlgSendPortMapEvent         PROTO ((UINT4, UINT2, UINT4, UINT2, UINT2));
UINT4 
NatSipAlgSendLinkStatusEvent      PROTO ((UINT4, UINT1, UINT1*, UINT4));
UINT4 
NatSipAlgSendLinkStatusAdd        PROTO ((UINT4, UINT1, UINT1*));
UINT4 
NatSipAlgSendLinkStatusDelete     PROTO ((UINT4, UINT1, UINT1*));
UINT4 
NatSipAlgSendPinHoleExpiryEvent   PROTO ((UINT4, UINT2, UINT4, UINT2, UINT2, UINT2));
UINT4
NatSipAlgSendDynEntryEvent PROTO ((UINT4, UINT2, UINT4, UINT2, UINT4, 
                                           UINT2, UINT2, UINT2));
/* scipc.c */
UINT4
sipAlgGetIPInterfaceInfo        PROTO ((tInIfInfo InIfInfo, 
                                         tRouteInfo *pRouteInfo));
VOID
NatSipAlgEventNotificationHandler PROTO((tEventNotificationInfo *));
UINT4
NatGetHashKeyFromIP PROTO ((UINT4 u4IpAddr));

tNatWanUaHashNode *
NatFindWanUaPortHashEntry PROTO ((UINT4 u4IpAddr, UINT2 u2Port,
                                  UINT2 u2Protocol));
UINT4
NatDelWanUaPortHashEntry PROTO ((UINT4 u4IpAddr, UINT2 u2Port,
                                 UINT2 u2TotalBindings, UINT2 u2Protocol));
UINT4
NatAddWanUaPortHashEntry PROTO ((UINT4 u4IpAddr, UINT2 u2BasePort,
                                UINT2 u2NumPorts, eParity eBaseParity,
                                UINT2 u2Porotocol));
PUBLIC VOID
NatSipAlgAddPortMapping PROTO ((tNatEntryInfo *, UINT4, tNatAsyncResponse * ));

PUBLIC VOID
NatSipAlgDeletePortMapping PROTO ((tNatEntryInfo *, UINT4, tNatAsyncResponse *));

PUBLIC VOID
NatSipAlgOpenPinhole PROTO ((tPinholeInfo *, UINT4, tNatAsyncResponse *));

PUBLIC VOID
NatSipAlgClosePinhole PROTO ((tPinholeInfo *, UINT4, tNatAsyncResponse *));

PUBLIC VOID
NatSipAlgDeleteDynamicEntry PROTO ((tNatEntryInfo *, UINT4, tNatAsyncResponse *));

UINT4 NatGetHashPortExistsStatus PROTO ((UINT4, UINT2, UINT4));

extern void sipAlgInit PROTO ((VOID));
extern void sipAlgDeInit PROTO ((VOID));
extern void sipAlgNatNotificationCbkFunc PROTO ((tEventNotificationInfo *));
extern void sipAlgNatDisableNotification PROTO ((VOID));
extern void sipAlgSipPortChangeNotification PROTO ((UINT2));

#endif /* __SIP_H__ */
/*****************************End of file cassip.h*****************************/
