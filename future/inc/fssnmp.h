/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: fssnmp.h,v 1.29 2016/09/13 12:57:11 siva Exp $
 *
 * Description: Snmp Agent module exported macros, typedefs and prototypes 
 *******************************************************************/
#ifndef _FS_SNMP_H_
#define _FS_SNMP_H_

#include "lr.h"

#define MAX_SNMP_DATA_LENGTH                    1500
#define SNMP_MAX_STR_ENGINEID_LEN               95
#define SNMP_FAILURE                            0
#define SNMP_SUCCESS                            1

#define SNMP_READONLY                           1
#define SNMP_READWRITE                          2
#define SNMP_NOACCESS                           4
#define SNMP_ACCESSIBLEFORNOTIFY                5
#define SNMP_READCREATE                         6
#define SNMP_MAX_OID_LENGTH                   272
#define SNMP_MAX_IPADDR_LEN                     4
#define SNMP_DATA_TYPE_INTEGER               0x02
#define SNMP_DATA_TYPE_INTEGER32             0x02
#define SNMP_DATA_TYPE_OCTET_PRIM            0x04
#define SNMP_DATA_TYPE_NULL                  0x05
#define SNMP_DATA_TYPE_OBJECT_ID             0x06
#define SNMP_DATA_TYPE_SEQUENCE              0x30
#define SNMP_DATA_TYPE_IP_ADDR_PRIM          0x40
#define SNMP_DATA_TYPE_COUNTER               0x41
#define SNMP_DATA_TYPE_COUNTER32             0x41
#define SNMP_DATA_TYPE_GAUGE                 0x42
#define SNMP_DATA_TYPE_GAUGE32               0x42
#define SNMP_DATA_TYPE_TIME_TICKS            0x43
#define SNMP_DATA_TYPE_OPAQUE                0x44
#define SNMP_DATA_TYPE_UNSIGNED32            0x42
#define SNMP_DATA_TYPE_COUNTER64             0x46
#define SNMP_DATA_TYPE_IMP_OCTET_PRIM        0xf1
#define SNMP_DATA_TYPE_IMP_OBJECT_ID         0xf2
#define SNMP_DATA_TYPE_ROW_STATUS            0xf3
#define SNMP_DATA_TYPE_MAC_ADDRESS           0xf4

#define SNMP_PORT          161
#define SNMP_ALT_PORT      6161

/* To Handle Fixed Octet String */
#define SNMP_FIXED_LENGTH_OCTET_STRING       0x50

#define SNMP_AGENT_ENABLE    1
#define SNMP_AGENT_DISABLE   2

#define SNMP_AGENTX_ENABLE   1
#define SNMP_AGENTX_DISABLE  2
#define SNMP_MAX_INDICES                                        15
/*SNMP_MAX_INDICES_2 defined to increase memory allocated for VARBIND list beacause
 for rollback,SNMPAllocVarBind is used once more for each varbind */
#define SNMP_MAX_INDICES_2                                    SNMP_MAX_INDICES*2   

/* New macros for v4, v6 Manager */
#define SNMP_IP_V4  2
#define SNMP_IP_V6  10

/* Truth Value */
typedef enum {
    ISS_TRUE = 1,
    ISS_FALSE
}tIssBool;

/* Registration time MSR trigger */
typedef enum {
    SNMP_MSR_TGR_TRUE = 1,
    SNMP_MSR_TGR_FALSE 
}tSnmpMsrBool;


typedef struct SNMP_OCTET_STRING_TYPE {
        UINT1  *pu1_OctetList;
        INT4   i4_Length;
} tSNMP_OCTET_STRING_TYPE;

typedef struct SNMP_OID_TYPE {
        UINT4  u4_Length;
        UINT4  *pu4_OidList;
} tSNMP_OID_TYPE;

typedef struct SNMP_COUNTER64_TYPE {
        UINT4  msn;
        UINT4  lsn;
} tSNMP_COUNTER64_TYPE;

typedef struct SNMP_MULTI_DATA_TYPE {
        tSNMP_COUNTER64_TYPE     u8_Counter64Value;
        UINT4                    u4_ULongValue;
        INT4                     i4_SLongValue;
        tSNMP_OCTET_STRING_TYPE *pOctetStrValue;
        tSNMP_OID_TYPE          *pOidValue;
        INT2                    i2_DataType;
        INT2                    i2Padding;
} tSNMP_MULTI_DATA_TYPE;

typedef tSNMP_MULTI_DATA_TYPE tRetVal;

typedef struct SNMP_VAR_BIND {
        tSNMP_OID_TYPE            *pObjName;
        tSNMP_MULTI_DATA_TYPE      ObjValue;
        struct SNMP_VAR_BIND      *pNextVarBind;
} tSNMP_VAR_BIND;


/* Rollback: added tSNMP_GET_VAR_BIND*/
typedef struct SNMP_GET_VAR_BIND {
        INT4                 isRollBackReqd;
        INT4                 prevRowStatus;
        INT1                 isRowStatus;
        INT1                 activateLater;
        INT1                 isValueSet;
        INT1                 i1Pad;
        tSNMP_VAR_BIND      *pRollbackValue;
        tSNMP_VAR_BIND      *pSetVarBind;
} tSNMP_GET_VAR_BIND;

typedef struct SnmpIndex {
    UINT4 u4No;
    tSNMP_MULTI_DATA_TYPE *pIndex;
} tSnmpIndex;

/* Rollback: added tSnmpIndexList*/
typedef struct {
    UINT4              u4NoVarbind;
    tSnmpIndex             *pIndexList;
}tSnmpIndexList;

typedef struct
{
   UINT4   u4BadPacket;
   UINT4   u4BadPacket1;
   UINT4   u4BadPacket2;
   UINT4   u4BadPacket3;
   UINT4   u4BadPacket4;
} tRipInBadPkts;

typedef struct
{
   UINT4   u4BadRoutes;
   UINT4   u4BadRoutes1;
   UINT4   u4BadRoutes2;
} tRipInBadRoutes;

/* Rollback: added Dep, u1RowStatus, pu1DefVal*/
typedef struct Mbdb {
        tSNMP_OID_TYPE   ObjectID;
        INT4 (*GetNextIndex)(tSnmpIndex *, tSnmpIndex *);
        INT4 (*Get)(tSnmpIndex *, tSNMP_MULTI_DATA_TYPE *);
        INT4 (*Set)(tSnmpIndex *, tSNMP_MULTI_DATA_TYPE *);
        INT4 (*Test)(UINT4 *, tSnmpIndex *, tSNMP_MULTI_DATA_TYPE *);
    INT4 (*Dep) (UINT4 *, tSnmpIndexList *,  tSNMP_VAR_BIND *);
        UINT4  u4OIDType;
        UINT4  u4OIDAccess;
    UINT1 *pu1IndexType;
    UINT4  u4NoIndices;
    UINT2 u2IsDeprecated;
    UINT2 u2RowStatus;
    CHR1 *pu1DefVal;
} tMbDbEntry;


typedef struct MibData {
    UINT4       u4Total;
    tMbDbEntry *pMibEntry;
} tMibData;
typedef struct  {
      void *pAppCookie; /*Data structure to allow user applications to send their own data to the call back function*/
      UINT4   u4InfmReqId;
      UINT4   u4MgrAddr; /*IP address of trap receiver*/
      UINT4   u4AckStatus; /* INFORM RESP Recd/not */
      UINT4   u4InfmSent;
      UINT4   u4InfmFail;
      UINT4   u4InfmRetried;
      UINT4   u4InfmResp; 
      INT4    i4MsgType; 
      UINT2   u2Port;
      INT2    i2Rsvd; 
} tSnmpInfmStatusInfo;
typedef void (*SnmpAppNotifCb) (tSnmpInfmStatusInfo *pSnmpInfmStatusInfo);

/* Notification Structure */
typedef struct {
    UINT4    *pu4ObjectId;              /* Pointer to the oid array defined
                                           in the db.h file */
    UINT4     u4OidLen;                 /* The Oid length */
    UINT4     u4SeqNum;                 /* Sequence number to be send in the 
                                           sync-up message */
    INT4      (*pLockPointer)(VOID);    /* The Lock routine for the protocol */
    INT4      (*pUnLockPointer)(VOID);  /* The UnLock routine for the
                                           protocol */
    UINT4     u4Indices;                /* The no. of indices for the
                                           table objects. For scalar objects
                                           it is set to zero. */
    UINT1     u1RowStatus;              /* Specifies whether the oid is a
                                           rowstatus object or not */
    INT1      i1ConfStatus;             /* The result of the set routine */
    INT2      i2Rsvd; 
} tSnmpNotifyInfo;

typedef struct
{
   UINT4   u4NumWithContextIdAndLock; /* Number of MIBs registered with 
                                       * contextId and lock
                                       */
   UINT4   u4NumWithLock;             /* Number of MIBs registered with lock
                                       */
   UINT4   u4TransportDomain;         /* Transport domain of the agent */
   UINT4   u4LastChangeTime;          /* Last changed time of the system configuration */
   UINT2   u2EngineIdLength;   /* Context Engine Id length*/
   UINT2   u2TransportAddrLength;   /* Transport address length*/
   UINT1   au1NameWithContextIdAndLock[SNMP_MAX_OCTETSTRING_SIZE]; 
            /* Name of the MIB registered with contextId and lock
            */
   UINT1   au1NameWithLock[SNMP_MAX_OCTETSTRING_SIZE];
            /* Name of the MIB registered with lock
            */
   UINT1   au1TransportAddr[SNMP_MAX_OCTETSTRING_SIZE];
            /* Transport address of the SNMP agent
            */
   UINT1   au1EngineId[SNMP_MAX_OCTETSTRING_SIZE];
            /* Context engine id of the SNMP agemt */
}tSnmpAgentParam;

/* This structure contains the values of Object names against OIDS */
struct MIB_OID {
    const char *pName;
    const char *pNumber;
};
typedef struct SNMPDBENTRY
{
  tMbDbEntry *pMibEntry;
  INT4       (*pLockPointer)(VOID);
  INT4       (*pUnlockPointer)(VOID);
  INT4       (*pNextLockPointer)(VOID);
  INT4       (*pNextUnlockPointer)(VOID);
  INT4       (*pSetContextPointer)(UINT4);
  VOID       (*pReleaseContextPointer)(VOID);
  INT4       (*pNextSetContextPointer)(UINT4);
  VOID       (*pNextReleaseContextPointer)(VOID);
  tSnmpMsrBool SnmpMsrTgr;
}tSnmpDbEntry;
typedef struct _tSnmpOidListBlock
{
        UINT4    au4SnmpOidListBlock[SNMP_MAX_OID_LENGTH];
}tSnmpOidListBlock;
typedef struct _tSnmpOctetListBlock
{
        UINT1    au1SnmpOctetListBlock[SNMP_MAX_OCTETLIST_SIZE];
}tSnmpOctetListBlock;
typedef struct _tSnmpMultiDataIndexBlock
{
        tSNMP_MULTI_DATA_TYPE  SnmpMultiDataIndexBlock[SNMP_MAX_INDICES_2];
}tSnmpMultiDataIndexBlock;
typedef struct _tSnmpMultiOidBlock
{
        UINT1     au1SnmpMultiOidBlock[SNMP_MAX_OID_LENGTH];
}tSnmpMultiOidBlock;

typedef enum{
    SNMP_UPDATE_EOID_EVENT,
    SNMP_REVERT_EOID_EVENT,
    SNMP_UPDATE_GETNEXT_EOID_EVENT,
    SNMP_CALL_BACK_EVENTS
}tSnmpCallBackEvents;

#define SNMP_NOTIFY_CFG(X)  IssNotifyConfiguration X

#if (defined (ISS_WANTED) || defined (RM_WANTED)) && defined (SNMP_2_WANTED)
#define SNMP_SET_NOTIFY_INFO(SnmpNotifyInfo, ObjectId, SeqNum, \
                             RowStatus, LockPointer, UnLockPointer, \
                             Indices, ConfStatus) \
        {\
            SnmpNotifyInfo.pu4ObjectId = ObjectId;\
            SnmpNotifyInfo.u4OidLen = sizeof (ObjectId)/sizeof (UINT4);\
            SnmpNotifyInfo.u4SeqNum = SeqNum;\
            SnmpNotifyInfo.u1RowStatus = RowStatus;\
            SnmpNotifyInfo.pLockPointer = LockPointer;\
            SnmpNotifyInfo.pUnLockPointer = UnLockPointer;\
            SnmpNotifyInfo.u4Indices = Indices;\
            SnmpNotifyInfo.i1ConfStatus = ConfStatus;\
        }
#else
#define SNMP_SET_NOTIFY_INFO(SnmpNotifyInfo, ObjectId, SeqNum, \
                             RowStatus, LockPointer, UnLockPointer, \
                             Indices, ConfStatus) \
        {\
            UNUSED_PARAM(SnmpNotifyInfo);\
            UNUSED_PARAM(ObjectId);\
            UNUSED_PARAM(SeqNum);\
            UNUSED_PARAM(RowStatus);\
            UNUSED_PARAM(LockPointer);\
            UNUSED_PARAM(UnLockPointer);\
            UNUSED_PARAM(Indices);\
            UNUSED_PARAM(ConfStatus);\
        }
#endif

#define SNMP_MSR_NOTIFY_CFG(X)  IssMsrNotifyConfiguration X

#define SNMP_INFORM_MSG_RE_TX       1
#define SNMP_INFORM_MSG_DROPPED     2
#define SNMP_INFORM_ACK_RECEIVED    3
#define SNMP_INFORM_NOT_ACKED       4 
#define SNMP_INFORM_NOT_SENT        5 
#define SNMP_INFORM_MSG_TX          6 
#define SNMP_INFM_MAX_TIMEOUT_VAL              150000
#define SNMP_INFM_DEF_TIMEOUT_IN_SECONDS       1500

#define SNMP_INFM_DEF_RETRY_VAL                 3
#define SNMP_INFM_DEF_TIMEOUT_VAL              1500

/* Init Routine called from main module */
VOID SNMPProtoInit              PROTO ((VOID));
VOID SnmpMain                   PROTO ((INT1 *));

/*  Protocol registration with snmp agent */
VOID SNMPAddSysorEntry          PROTO((tSNMP_OID_TYPE *, const UINT1 *));
VOID SNMPDelSysorEntry          PROTO ((tSNMP_OID_TYPE *, const UINT1 *));
INT4 SNMPRegisterMib            PROTO ((tSNMP_OID_TYPE *MibID,
                                        tMibData *pMibData,
                                        tSnmpMsrBool));

INT4 SnmpGetSysOrDescriptor     PROTO ((INT4 i4SysORIndex,
                                        tSNMP_OCTET_STRING_TYPE * pRetValSysORDescr));

INT4 SNMPRegisterMibWithLock    PROTO ((tSNMP_OID_TYPE * MibID,
                                        tMibData * pMibData, 
                                        INT4(*pLockPointer)(VOID),
                                        INT4(*pUnlockPointer)(VOID),
                                        tSnmpMsrBool));
INT4 SNMPRegisterMibWithContextIdAndLock PROTO ((tSNMP_OID_TYPE * MibID, 
                                      tMibData * pMibData, 
                                      INT4 (*pLockPointer) (VOID),
                                      INT4 (*pUnlockPointer) (VOID), 
                                      INT4 (*pSetContextPointer) (UINT4),
                                      VOID (*pReleaseContextPointer) (VOID),
                                      tSnmpMsrBool));

INT4 SNMPUnRegisterMib          PROTO ((tSNMP_OID_TYPE * MibID,
                                        tMibData * pMibData));

/* Snmp agent Get/GetNext/Set/Test/Dep Routines */
INT4 SNMPGet                    PROTO ((tSNMP_OID_TYPE OID,
                                       tSNMP_MULTI_DATA_TYPE *pData,
                                       UINT4 *pErr, tSnmpIndex * pNextIndex,
                       UINT4 u4VcNum));

INT4 SNMPGetNextOID             PROTO ((tSNMP_OID_TYPE OID,
                                        tSNMP_OID_TYPE *pNextOid,
                                       tSNMP_MULTI_DATA_TYPE *pNextData,
                                       UINT4 *pErr, tSnmpIndex *pCurIndex,
                                       tSnmpIndex *pNextIndex,
                       UINT4 u4VcNum));

INT4 SNMPGetNextReadWriteOID             PROTO ((tSNMP_OID_TYPE OID,
                                        tSNMP_OID_TYPE *pNextOid,
                                       tSNMP_MULTI_DATA_TYPE *pNextData,
                                       UINT4 *pErr, tSnmpIndex *pCurIndex,
                                       tSnmpIndex *pNextIndex,
                       UINT4 u4VcNum));

INT4 SNMPSet                    PROTO ((tSNMP_OID_TYPE OID,
                                       tSNMP_MULTI_DATA_TYPE *pVal,
                                       UINT4 *pErr,tSnmpIndex *,
                       UINT4 u4VcNum));

INT4 SNMPTest                   PROTO ((tSNMP_OID_TYPE OID,
                                       tSNMP_MULTI_DATA_TYPE *pVal,
                                       UINT4 *pErr, tSnmpIndex *,
                       UINT4 u4VcNum));


INT4 SNMPDependency         PROTO ((tSNMP_OID_TYPE OID,
                    tSNMP_VAR_BIND * VarBindList,
                    UINT4 *pError,
                    tSnmpIndexList *IndexList ,
                    UINT4 u4VcNum));

INT4
SNMPOIDIsRowStatus(tSNMP_OID_TYPE OID,UINT4 *pu4RowStatus);

VOID
SNMPFormOid (tSNMP_OID_TYPE * pOid, const tMbDbEntry * pCur,
             const tSnmpIndex * pIndex);

VOID
SNMPGetMbDbEntry (tSNMP_OID_TYPE ObjectID, tMbDbEntry **pNext, UINT4 *pu4Count,
                  tMbDbEntry **pMbDbEntry);

#ifdef CLI_WANTED
INT1 nmhSetCmn (UINT4 *, INT4,  
        INT4 (*Wfnptr)(tSnmpIndex *, tSNMP_MULTI_DATA_TYPE *), 
        INT4 (*Lockptr)(VOID), 
        INT4 (*Unlockptr)(VOID), 
        UINT4, UINT4, INT4, CHR1 *fmt, ...);

INT1 nmhSetCmnNew (UINT4 *, INT4,
                   INT4 (*Lockptr)(VOID),
                   INT4 (*Unlockptr)(VOID),
                   UINT4, UINT4, INT4, INT4, CHR1 *fmt, ...);

INT1 nmhSetCmnWithLock (UINT4 *, INT4, 
        INT4 (*Wfnptr)(tSnmpIndex *, tSNMP_MULTI_DATA_TYPE *), 
        INT4 (*Lockptr)(VOID), 
        INT4 (*Unlockptr)(VOID), 
        UINT4, UINT4, INT4, CHR1 *fmt, ...);
#endif
INT1 MsrSetCmn (UINT4 *, INT4,  
        INT4 (*Wfnptr)(tSnmpIndex *, tSNMP_MULTI_DATA_TYPE *), 
        INT4 (*Lockptr)(VOID), 
        INT4 (*Unlockptr)(VOID), 
        UINT4, UINT4, INT4, CHR1 *fmt, ...);

INT1 MsrSetCmnNew (UINT4 *, INT4,
                   INT4 (*Lockptr)(VOID),
                   INT4 (*Unlockptr)(VOID),
                   UINT4, UINT4, INT4, INT4, CHR1 *fmt, ...);

INT1 MsrSetCmnWithLock (UINT4 *, INT4, 
        INT4 (*Wfnptr)(tSnmpIndex *, tSNMP_MULTI_DATA_TYPE *), 
        INT4 (*Lockptr)(VOID), 
        INT4 (*Unlockptr)(VOID), 
        UINT4, UINT4, INT4, CHR1 *fmt, ...);

INT1 MsrSetWithNotify
    (UINT4 *, INT4,
     INT4 (*Wrapperfn) (tSnmpIndex *, tSNMP_MULTI_DATA_TYPE *),
     INT4 (*LockPtr) (VOID),
     INT4 (*UnlockPtr) (VOID), UINT4, UINT4, INT4, CHR1 *, va_list);
INT1 MsrSetWithNotifyNew
    (UINT4 *, INT4,
     INT4 (*LockPtr) (VOID),
     INT4 (*UnlockPtr) (VOID), UINT4, UINT4, INT4, INT4, CHR1 *, va_list);

/* The IssNotify prototype is added so that it is known to all the low level
 * routines */
VOID IssNotifyConfiguration PROTO ((tSnmpNotifyInfo SnmpNotifyInfo, 
                                    CHR1 *pu1Fmt, ...));

VOID IssMsrNotifyConfiguration PROTO ((tSnmpNotifyInfo SnmpNotifyInfo, 
                                       CHR1 *pu1Fmt, ...));

INT1 MsrNotifyProtocolShutdown PROTO ((UINT1 pOid[][SNMP_MAX_OID_LENGTH],
                                       UINT2 u2Oid, INT4 i4ContextId));

INT1 MsrNotifyContextDeletion PROTO ((UINT4));

INT1 MsrNotifyBridgeModeChange PROTO ((UINT4));

INT1 MsrNotifyVlanDeletion PROTO ((UINT4));
INT1 MsrNotifyPortTypeChange PROTO ((UINT4));
INT1 MsrNotifyStpPortDelete PROTO ((UINT4));

/* The Snmp utilities used in the snmp, cli and web msr triggering */
INT1
SnmpAllocMultiIndexAndData (tSnmpIndex * (*ppMultiIndex), UINT4 u4IndicesNo,
                            tRetVal ** ppMultiData, UINT1 **ppOid,
                            UINT4 u4OidLen);

VOID
free_MultiData (tSNMP_MULTI_DATA_TYPE * pMultiData);

VOID
free_MultiIndex (tSnmpIndex * pMultiIndex, UINT4 u4Indices);

VOID
free_MultiOid PROTO ((UINT1 *));

/* Used for filling the variable list values to the 
 * correspoding multi index and multi data. */
VOID
SNMPFillMultiIndexandData (UINT4 u4Indices, CHR1 * pVarIndexFmt,
                           tSnmpIndex * pMultiIndex,
                           tSNMP_MULTI_DATA_TYPE * pMultiData,
                           va_list DataValue);
/* Used for filling the variable list values to the 
 * two instance of multi index and multi data. 
 * First instance of data sent to MSR for Mib Save and Restore operation.
 * Second instance of data is used for sync-up message construction. */
VOID
SNMPFillTwoInstOfMultiIndexandData (UINT4 u4Indices, CHR1 * pVarIndexFmt,
                           tSnmpIndex * pMultiIndex1,
                           tSNMP_MULTI_DATA_TYPE * pMultiData1,
                           tSnmpIndex * pMultiIndex2,
                           tSNMP_MULTI_DATA_TYPE * pMultiData2,
                           va_list DataValue);

INT4
SNMPGetMbDbTgrValue (tSNMP_OID_TYPE ObjectID, BOOL1 *pbSnmpTgr);

UINT4
SNMPUtilGetObjValueLength (tSNMP_MULTI_DATA_TYPE * pVal);

VOID
SNMPConvertDataToString (tSNMP_MULTI_DATA_TYPE * pData,
                         UINT1 *pu1String, UINT2 u2Type);

UINT4  SNMPAddEnterpriseOid (tSNMP_OID_TYPE *, tSNMP_OID_TYPE *, tSNMP_OID_TYPE *);

VOID SNMPGetOidString (UINT4 *, UINT4, UINT1 *);


/* ProtoType for Trap */
VOID SNMP_Notify_Trap   PROTO ((tSNMP_OID_TYPE *, UINT4, UINT4, 
                                tSNMP_VAR_BIND *, tSNMP_OCTET_STRING_TYPE *));
VOID SNMP_AGT_RIF_Notify_Trap   PROTO ((tSNMP_OID_TYPE *, UINT4, UINT4, 
                                       tSNMP_VAR_BIND *));
VOID SNMP_AGT_RIF_Notify_Trap_v1Usr PROTO ((tSNMP_OID_TYPE *, UINT4, 
                                            UINT4, tSNMP_VAR_BIND *,
                                            SnmpAppNotifCb, VOID *));

INT4 SNMP_AGT_RIF_Notify_UsrAppl_v2Trap PROTO ((tSNMP_VAR_BIND *,
                                                SnmpAppNotifCb, VOID *));

VOID SNMP_AGT_RIF_Notify_v2Trap PROTO ((tSNMP_VAR_BIND *));
VOID SNMP_Context_Notify_Trap PROTO((tSNMP_OID_TYPE *,UINT4,UINT4,tSNMP_VAR_BIND *,
                               tSNMP_OCTET_STRING_TYPE *));

VOID SNMPGetTrapOID             PROTO ((tSNMP_OID_TYPE *));
VOID SNMPGetEnterpriseOID       PROTO ((tSNMP_OID_TYPE *));

VOID SNMP_free_snmp_vb_list     PROTO ((tSNMP_VAR_BIND *));

VOID SnmpUpdateEOid             PROTO ((UINT4));

/* Extern Prototype for DisplayString Validation function */
extern UINT4 SNMPCheckForNVTChars ARG_LIST((UINT1 *,INT4));

/* Extern Prototype for providing default value gor a given OID*/
INT4 
SNMPGetDefaultValueForObj (tSNMP_OID_TYPE OID,
                           tSNMP_MULTI_DATA_TYPE  *pVal, 
                           UINT4 *pError);

extern tIssBool gMSRIncrSaveFlag;

/* ProtoType for deleting default SNMPv3 configurations */
VOID SNMPV3DefaultDelete PROTO ((VOID));

PUBLIC INT4 SnmpGetSysName PROTO((UINT1 * pu1RetValSysName));
PUBLIC INT4 SnmpGetSysDescr PROTO((UINT1 * pu1RetValSysDescr));

#define SNMP_ERR_NO_ERROR                                       0
#define OK                                                      0
#define SNMP                                                    1
#define SNMP_ERR_TOO_BIG                                        1
#define NOTOK                                                   1
#define EQUAL                                                   2
#define SNMP_ERR_NO_SUCH_NAME                                   2
#define SNMP_ERR_BAD_VALUE                                      3
#define SNMP_ERR_READ_ONLY                                      4
#define SNMP_ERR_GEN_ERR                                        5
#define SNMP_ERR_NO_ACCESS                                      6
#define ENTERPRISE_SPECIFIC                                     6
#define SNMP_ERR_WRONG_TYPE                                     7
#define SNMP_ERR_WRONG_LENGTH                                   8
#define SNMP_ERR_WRONG_ENCODING                                 9
#define SNMP_ERR_WRONG_VALUE                                   10
#define SNMP_ERR_NO_CREATION                                   11
#define SNMP_ERR_INCONSISTENT_VALUE                            12
#define SNMP_ERR_RESOURCE_UNAVAILABLE                          13
#define SNMP_ERR_COMMIT_FAILED                                 14
#define SNMP_ERR_UNDO_FAILED                                   15
#define SNMP_ERR_AUTHORIZATION_ERROR                           16
#define SNMP_ERR_NOT_WRITABLE                                  17
#define SNMP_ERR_INCONSISTENT_NAME                             18
#define MAX_OID_LEN                                           255
#define SNMP_TRAP_OID_LEN                                     11
#define MAX_OCTETSTRING_SIZE            SNMP_MAX_OCTETSTRING_SIZE

#define ALLOC_MEM(unit_size,n_units,type) MEM_CALLOC(unit_size,n_units,type)
#define REL_MEM(pu1_memory) MEM_FREE(pu1_memory); pu1_memory = NULL

#define  ISS_MAX_INDICES                     SNMP_MAX_INDICES

#define SNMP_DEFAULT_CONTEXT                                    0

UINT4 SnmpCreateVirtualContext ARG_LIST ((UINT4 , tSNMP_OCTET_STRING_TYPE *));
UINT4 SnmpDeleteVirtualContext ARG_LIST ((UINT4));
UINT4 SnmpUpdateAliasName (UINT4 u4ContextId, tSNMP_OCTET_STRING_TYPE * pName);

/* Agentx related APIs */
#define SNX_INSTANCE_REGISTRATION               0x01 /* Indicates that the 
                          registration is for fully qualified instance name */

/* if the SNX_ANY_INDEX bit is set, the master agent should generate an 
 * index value  with the constraint that this value is not currently allocated 
 * to any subagent .For SNX_NEW_INDEX bit, constraint is that this value 
 * must not have been allocated*/
#define SNX_ANY_INDEX                           0x02
#define SNX_NEW_INDEX                           0x04

#define SNX_IP4_LEN                          4
#define SNX_IP6_LEN                          16

/*Agentx transport mechanism*/
#define SNX_TDOMAIN_TCP                      1

/*Snmp Agent or Agentx Subagent enable status*/
#define SNX_SNMP_ACTIVE_AGENT                1
#define SNX_AGENTX_SUBAGENT                  2

VOID SnmpRedSetNodeStateToInit (VOID);
PUBLIC INT1 SnmpGetAgentParam PROTO ((UINT4 u4MIBSequence, tSnmpAgentParam *pAgentParam));
PUBLIC INT1 SnmpUpdateLastTimeChange PROTO ((VOID));
VOID GetMbDbEntry(tSNMP_OID_TYPE, tMbDbEntry **, UINT4 *, tSnmpDbEntry *);
INT4
SNMPConvertStringToOid (tSNMP_OID_TYPE * pOid, UINT1 *pu1Data, UINT1 u1HexFlag);
VOID SNMPCopyOid(tSNMP_OID_TYPE *pFirstOid, tSNMP_OID_TYPE *pSecondOid);
UINT4 OIDCompare(tSNMP_OID_TYPE OID1,tSNMP_OID_TYPE OID2,UINT4 *u4Count);
tSNMP_MULTI_DATA_TYPE *Snmp3CliConvertStringToData (UINT1 *pu1String,
                                                    UINT1 u1Type);
VOID SNMPV3DefaultInit (VOID);
/* For Controlling the Community creation */
VOID SnmpApiSetCommunityCtrl PROTO ((UINT1 u1CtrlFlag));

/* For Releasing the memory */
VOID SnmpApiClearData (UINT1 *pu1Data);

/* Export Proto Types from snmpmbdb.c */
VOID SNMPOIDCopy (tSNMP_OID_TYPE * pDest, tSNMP_OID_TYPE * pSrc);

/* For setting the system name*/
INT4 SnmpApiSetSysName (tSNMP_OCTET_STRING_TYPE * pSwitchName);

/* For setting SNMPUpdateDefault */
INT4 SnmpApiSetSNMPUpdateDefault (UINT1 u1Status);

/* For getting SNMPUpdateDefault */
UINT1 SnmpApiGetSNMPUpdateDefault (VOID);

/*For updating the engine id from issnvram file */
INT4 SnmpApiUpdateEngineId (VOID);
/*For decrementing the counter value of u4SnmpOutNoSuchNames */
VOID SnmpApiDecrementCounter(UINT4 u4Error);
#endif /* _FS_SNMP_H_ */

