
/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: rip6npwr.h,v 1.1 2014/04/10 13:26:05 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              for RIP6 wrappers
 *              
 ********************************************************************/
#ifndef __RIP6_NP_WR_H__
#define __RIP6_NP_WR_H__

UINT1 Rip6FsNpRip6Init PROTO ((VOID));
#ifdef MBSM_WANTED
UINT1 Rip6FsNpMbsmRip6Init PROTO ((tMbsmSlotInfo * pSlotInfo));
#endif /*MBSM_WANTED */

#endif /* __RIP6_NP_WR_H__ */
