/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rtm6.h,v 1.31 2017/12/26 13:34:20 siva Exp $
 *
 * Description: This file contains #definitions used in the RTM module. 
 *
 *******************************************************************/
#ifndef _RTM6_H
#define _RTM6_H

#include "rmap.h"


/* Contains definitions used in RTM6 */
#define  IPV6_ROUTE_TABLE                            7

#define  RTM6_REGISTRATION_MESSAGE                   1
#define  RTM6_REGISTRATION_ACK_MESSAGE               2
#define  RTM6_REDISTRIBUTE_ENABLE_MESSAGE            3
#define  RTM6_ROUTE_UPDATE_MESSAGE                   4
#define  RTM6_ROUTE_CHANGE_NOTIFY_MESSAGE            5
#define  RTM6_REDISTRIBUTE_DISABLE_MESSAGE           6
#define  RTM6_DEREGISTRATION_MESSAGE                 7
#define  RTM6_ROUTEMAP_UPDATE_MSG                    8
#define  RTM6_VCM_MSG_RCVD                           9
#define  RTM6_GR_NOTIFY_MSG                          10
#define  RTM6_INTERFACE_SHUTDOWN                     11
#define  RTM6_PROTOCOL_SHUTDOWN                      12
#define  RTM6_GR_CLEANUP                             13

#define RTM6_DEREGISTER 0
#define RTM6_REGISTER 1


#define  RTM6_ACTIVE                                 1
#define  RTM6_NOT_IN_SERVICE                         2
#define  RTM6_NOT_READY                              3
#define  RTM6_CREATE_AND_WAIT                        5
#define  RTM6_DESTROY                                6

#define  RTM6_AGGR_MASK                              0x0001
#define  RTM6_DIRECT_MASK                            0x0002
#define  RTM6_STATIC_MASK                            0x0004
#define  RTM6_RIP_MASK                               0x0010
#define  RTM6_OSPF_MASK                              0x0020
#define  RTM6_BGP_MASK                               0x0040
#define  RTM6_ISISL1_MASK                            0x0200
#define  RTM6_ISISL2_MASK                            0x0400
#define  RTM6_ISISL1L2_MASK                          0x0600
#define  RTM6_ISIS_MASK                              0x0100

#define  RTM6_OSPF_AND_RIP_MASK                      0x0030
#define  RTM6_BGP_AND_OSPF_MASK                      0x0060
#define  RTM6_BGP_AND_RIP_MASK                       0x0050
#define  RTM6_ALL_RPS_MASK                           0x0770

#define  RTM6_FILTER_BASED_ON_AS_MASK                0x80
#define  RTM6_IFINDEX_MASK                           IP6_BIT_INTRFAC
#define  RTM6_METRIC_MASK                            IP6_BIT_METRIC
#define  RTM6_ROW_STATUS_MASK                        IP6_BIT_STATUS
#define  RTM6_ROUTE_TYPE_MASK                        IP6_BIT_RT_TYPE

#define  RTM6_MAX_TASK_NAME_LEN                      4
#define  RTM6_MAX_Q_NAME_LEN                         4

/* RTM Task Processing Event */
#define  RTM6_MESSAGE_ARRIVAL_EVENT                  0x00000001
#define  RTM6_FILTER_ADDED_EVENT                     0x00000002
#define  RTM6_KERNEL_UPD_EVENT                       0x00000004
#define  RTM6_TMR_EVENT                              0x00000008
#define  RTM6_RM_PKT_EVENT                           0x00000010
#define  RTM6_HA_PEND_BLKUPD_EVENT                   0x00000020
#define  RTM6_RED_TIMER_EVENT                        0x00000040
#define  RTM6_ROUTE_MSG_EVENT                        0x00000080
#define  RTM6_HA_FRT_PEND_BLKUPD_EVENT               0x00000100
#define  RTM6_PROTOCOL_STATUS_EVENT                  0x00000800


#define   RTM6_SUCCESS                               IP6_SUCCESS
#define   RTM6_FAILURE                               IP6_FAILURE
#define   RTM6_NULL_FUN_PTR                          -2
#define   RTM6_NO_RESOURCE                           -3
#define   RTM6_ACK_SEND_FAIL                         -4

#define   RTM6_ADD_ROUTE                             NETIPV6_ADD_ROUTE
#define   RTM6_DELETE_ROUTE                          NETIPV6_DELETE_ROUTE
#define   RTM6_MODIFY_ROUTE                          NETIPV6_MODIFY_ROUTE

#define   RTM6_ROUTE_INDEX                           1
#define   RTM6_ROUTE_TYPE                            2
#define   RTM6_ROUTE_TAG                             3
#define   RTM6_ROUTE_ADDR_TYPE                       4

#define RTM6_SET_BIT(u2Mask, u2SrcId)  \
        (u2Mask) = (UINT2)((u2Mask) | (0x0001 << ((UINT1)(u2SrcId-1))))

#define RTM6_CLEAR_BIT(u2Mask, u2SrcId) \
        (u2Mask) = (UINT2)((u2Mask) & ~(0x0001 << ((UINT1)u2SrcId-1)))

#define RTM6_TEST_BIT(u2Mask, u2SrcId)  \
        (u2Mask) & (0x0001 << ((UINT1)(u2SrcId-1)))

#define  RTM6_ACK_REQUIRED                           0x80

#define  RTM6_QUERIED_FOR_NEXT_HOP                   0x01
#define  RTM6_QUERIED_FOR_SYNC                       0x02
#define  RTM6_MSG_HDR_LEN                            sizeof(tRtm6MsgHdr)

/* Constants declaration for RTM6 TRIE */
#define   RTM6_IPV4_KEY_ARR_SIZE                     2
#define   RTM6_IPV4_KEY_ARR_INDX0                    0
#define   RTM6_IPV4_KEY_ARR_INDX1                    1
#define   RTM6_DEFAULT_CXT_ID              VCM_DEFAULT_CONTEXT
#define   RTM6_INVALID_CXT_ID              VCM_INVALID_VC
#define   RTM6_TASK_LOCK                  Rtm6Lock
#define   RTM6_TASK_UNLOCK                Rtm6UnLock
/* GR Flag */
#define  RTM6_GR_RT                                  0x01
/********************************************************************
 * Structure definitions
 *******************************************************************/
/* RTM Registration Identifier structure. */
typedef struct Rtm6RegnId
{
    UINT4           u4ContextId; /* RTM6 Context ID */
    UINT2           u2ProtoId;          /* Protocol Id */
    UINT2           u2AlignmentBytes;
}
tRtm6RegnId;
    
/* Message Header */
typedef struct Rtm6MsgHdr
{
 UINT4        u4Index;
    tRtm6RegnId  RegnId;         /* Registration Information */
    UINT2        u2MsgLen;       /* Length of this message excluding Header */
    UINT1        u1MessageType;  /* Identifies the type of message */
 UINT1        u1BitMask;
 VOID         (*pDeliverToApplication);
 INT1         i1GrFlag;
 UINT1        u1Pad[3];

} tRtm6MsgHdr;

   /* Registration ack message structure. */
typedef struct Rtm6RegnAckMsg
{
    UINT4  u4RouterId;
    UINT4  u4ContextId; /* RTM6 Context ID */
    UINT4  u4MaxMessageSize;
    UINT2  u2ASNumber;
    UINT2  u2Reserved;
} tRtm6RegnAckMsg;

   /* RTM6 Response message info */
typedef union Rtm6RespInfo
{
    tRtm6RegnAckMsg      *pRegnAck;
    tNetIpv6RtInfo       *pRtInfo;
} tRtm6RespInfo;

   /* Registration message structure. */
typedef struct Rtm6RegnMsg
{
    tRtm6RegnId  RegnId;
    INT4   (*DeliverToApplication)(tRtm6RespInfo *, tRtm6MsgHdr *);
    UINT1  au1TaskName[RTM6_MAX_TASK_NAME_LEN];
    UINT1  au1QName[RTM6_MAX_Q_NAME_LEN];
    UINT4  u4Event;
    UINT1  u1BitMask;
    UINT1  au1Reserved[3];
} tRtm6RegnMsg;

/* Declaration for Structures used by TRIE2. */
typedef struct _Rtm6TrieOutParams
{
    tIp6RtEntry *pRtEntry;
    tKey         Key;
    UINT1        au1NoOfMultipath[IP6_MAX_ROUTING_PROTOCOLS];
    UINT1        u1Pad[3];
}
tRtm6TrieOutParams;

typedef struct _Rtm6TrieDelOutParams
{
    UINT4        u4NoOfRoutes;      /* Number of routes deleted. */
    INT1         i1ProtoId;         /* Protocol of the deleted routes */
    UINT1        au1Pad[3];
}
tRtm6TrieDelOutParams;

typedef struct _Rtm6TrieScanOutParams
{
    tIp6RtEntry    *pRtEntry;
    UINT4           u4ScanEvent;
#define         RTM6_SCAN_BEST_ROUTE        1
#define         RTM6_SCAN_INTF_UP           2
#define         RTM6_SCAN_INTF_DOWN         3
#define         RTM6_SCAN_INTF_DEL          4
#define         RTM6_SCAN_PROTO_DEL         5
    UINT4           u4IfIndex;
    UINT4           u4ContextId; /* RTM6 Context ID */
    INT1            i1ProtoId;
    UINT1           au1Pad[3];
}
tRtm6TrieScanOutParams;

extern struct Rtm6Cxt *pRtm6Context;

#define   RTM6_COPY_ROUTE_INFO_TO_UPDATE_MSG(pRt, RtUpdate) \
                       Ip6AddrCopy (&RtUpdate.Ip6Dst, &pRt->dst); \
                       RtUpdate.u1Prefixlen = pRt->u1Prefixlen; \
                       Ip6AddrCopy (&RtUpdate.NextHop, &pRt->nexthop); \
                       RtUpdate.u4Index = pRt->u4Index; \
                       RtUpdate.u4RouteTag = (UINT4) pRt->u4RouteTag; \
                       RtUpdate.u4RowStatus = (UINT4) pRt->u4RowStatus; \
                       RtUpdate.i1Proto = pRt->i1Proto; \
                       RtUpdate.i1DefRtrFlag = pRt->i1DefRtrFlag; \
                       RtUpdate.u4Metric = (UINT4) pRt->u4Metric;\
                       RtUpdate.u1Preference = (UINT4) pRt->u1Preference;\
                       RtUpdate.u1MetricType = (UINT1) pRt->u1MetricType;\
if (pRt->pCommunity != NULL) \
{ \
    MEMCPY(RtUpdate.pCommunity,pRt->pCommunity,(pRt->u1CommCount *sizeof(UINT4)));\
} \
                       RtUpdate.u1CommCount = pRt->u1CommCount;\
                       RtUpdate.u4SetFlag = pRt->u4SetFlag ;

#define   RTM6_COPY_UPDATE_MSG_TO_ROUTE_INFO(pRt, pRtUpdate) \
                       Ip6AddrCopy (&pRt->dst, &pRtUpdate->Ip6Dst); \
                       pRt->u1Prefixlen = pRtUpdate->u1Prefixlen; \
                       Ip6AddrCopy (&pRt->nexthop, &pRtUpdate->NextHop); \
                       pRt->u4Index = pRtUpdate->u4Index; \
                       pRt->u4RouteTag = (UINT2) RtUpdate.u4RouteTag; \
                       pRt->i1Proto = RtUpdate.i1Proto; \
                       pRt->i1DefRtrFlag = RtUpdate.i1DefRtrFlag; \
                       pRt->u1Preference = RtUpdate.u1Preference; \
                       pRt->u4Metric = (UINT1) RtUpdate.u4Metric; \
                       pRt->u1Preference = RtUpdate.u1Preference;
#define   RTM6_COPY_ECMP6_ROUTE_INFO_TO_UPDATE_MSG(pRt, RtUpdate) \
                       Ip6AddrCopy (&RtUpdate.Ip6Dst, &pRt->dst); \
                       RtUpdate.u1Prefixlen = pRt->u1Prefixlen; \
                       Ip6AddrCopy (&RtUpdate.NextHop, &pRt->nexthop); \
                       RtUpdate.u4Index = pRt->u4Index; \
                       RtUpdate.u4RouteTag = (UINT4) pRt->u4RouteTag; \
                       RtUpdate.u4RowStatus = (UINT4) pRt->u4RowStatus; \
                       RtUpdate.i1Proto = pRt->i1Proto; \
                       RtUpdate.i1DefRtrFlag = pRt->i1DefRtrFlag; \
                       RtUpdate.u4Metric = (UINT4) pRt->u4Metric; \
                       RtUpdate.u1Preference = pRt->u1Preference; \
                       RtUpdate.u1MetricType = pRt->u1MetricType; \
                       RtUpdate.u1AddrType = pRt->u1AddrType; \
                       RtUpdate.u2ChgBit = IP6_BIT_ALL;
/********************************************************************
 * Function Prototypes                              
 *******************************************************************/

/* Contains prototypes of functions defined in RTM module */

INT1    RpsEnqueuePktToRtm6      PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf));
INT4    Rtm6Register             PROTO ((tRtm6RegnId *pRegnId,
                                        UINT1 u1BitMask,
                                        INT4 (*SendToApplication)
                                            (tRtm6RespInfo   *pRespInfo,
                                             tRtm6MsgHdr *RtmHeader)));
INT4    Rtm6Deregister           PROTO ((tRtm6RegnId *pRegnId));

/* Route specific prototype */
INT4    Rtm6Ipv6LeakRoute        PROTO ((UINT1 u1CmdType,
                                         tNetIpv6RtInfo *pNetIp6RtInfo));

#if LNXIP6_WANTED
INT4    Rtm6CheckRtExistsInInvdRouteList PROTO ((tIp6Addr * pIp6Addr, INT4 i4PrefixLen, tIp6Addr * pNextHopIpAddr));

INT4    Rtm6InsertRouteInInvdRouteList PROTO ((tNetIpv6RtInfo *pNetIp6RtInfo));

VOID    Rtm6UpdateInvdRouteList PROTO ((UINT4 u4Index, tIp6Addr * pIp6Addr, INT4 i4PrefixLen, UINT1 u1CmdType));
#endif

INT4    Rtm6NetIpv6GetRoute      PROTO ((tNetIpv6RtInfoQueryMsg  *pRtQuery,
                                         tNetIpv6RtInfo   *pNetIp6RtInfo));
VOID    Ip6GetFwdTableRouteNumInCxt   PROTO ((UINT4 u4ContextId,
                                              UINT4 *pu4Ip6FwdTblRouteNum));

VOID    Rtm6ProcessGRRouteCleanUp PROTO ((tRtm6RegnId * pRegnId,
                                         INT1 i1GRCompFlag));


INT4    Ip6TrieScan              PROTO  ((tInputParams * pInputParams,
                                         INT4 (*pAppSpecScanFunc)
                                                 (tRtm6TrieScanOutParams *),
                                         tRtm6TrieScanOutParams * pOutParams));

INT4  Rtm6ApiRoute6LookupInCxt PROTO ((UINT4 u4ContextId,
                     tIp6Addr * pAddr, UINT1 u1PrefixLen,
                     UINT1 u1Preference, UINT1 *pu1FoundFlag,
                     tNetIpv6RtInfo *pNetIpv6RtInfo));
INT4 Rtm6ApiActOnIpv6IfChange PROTO ((UINT1 u1Event, UINT4 u4Index));
VOID Rtm6ApiPurgeDynamicInCxt PROTO ((UINT4 u4ContextId, INT1 i1Proto));
INT4
Rtm6ApiNetIpv6GetRoute PROTO ((tNetIpv6RtInfoQueryMsg * pNetIpv6RtQuery,
                 tNetIpv6RtInfo * pNetIpv6RtInfo));
VOID
Rtm6ApiAddAllRtWithNxtHopInHWInCxt PROTO ((UINT4 u4ContextId,
                                    tIp6Addr * pNextHopIpAddr));

VOID Rtm6ApiAddLocalRouteInNP PROTO ((UINT4 u4ContextId, tIp6Addr * pNextHopIpAddr));
VOID Rtm6ApiAddDropRouteInNP PROTO ((UINT4 u4ContextId, tNetIpv6RtInfo *pIp6RtInfo));


VOID
Rtm6ApiAddAllRtWithNxtHopInHWInLnx (UINT4 u4ContextId,
                                    tIp6Addr * pNextHopIpAddr);

VOID
Rtm6ApiDelAllRtWithNxtHopInHWInLnx(UINT4 u4ContextId,
                                    tIp6Addr * pNextHopIpAddr);
VOID
Rtm6ApiAddRtInHWInCxt (UINT4 u4ContextId,
                          tIp6Addr * pNextHopIpAddr);
VOID
Rtm6ApiDelNextHopInCxt (UINT4 u4ContextId,
                          tIp6Addr * pNextHopIpAddr);
INT4 Rtm6AddNhToPRTInCxt (UINT4 u4ContextId, tIp6Addr *pIp6Addr);

INT1
Rtm6ApiValIdxInstIpv6RouteTableInCxt PROTO ((UINT4 u4ContextId,
                                 tSNMP_OCTET_STRING_TYPE * pIpv6Dest,
                                 UINT4 u4IPv6PfxLen,
                                 tSNMP_OCTET_STRING_TYPE * pIpv6NextHop));

INT1
Rtm6ApiGetNextIpv6RouteTableEntryInCxt PROTO ((UINT4 u4ContextId,
                                   tSNMP_OCTET_STRING_TYPE * pIpv6Dest,
                                   tSNMP_OCTET_STRING_TYPE * pIpv6NxtDest,
                                   UINT4 u4IPv6PfxLen,
                                   UINT4 *pu4NxtIPv6PfxLen,
                                   tSNMP_OCTET_STRING_TYPE * pIpv6NextHop,
                                   tSNMP_OCTET_STRING_TYPE * pNxtIpv6NextHop,
                                   UINT1 u1IsFirst));
INT1
Rtm6ApiGetIpv6RouteTableEntryObjInCxt PROTO ((UINT4 u4ContextId,
                                  tSNMP_OCTET_STRING_TYPE * pIpv6Dest,
                                  UINT4 u4IPv6PfxLen,
                                  tSNMP_OCTET_STRING_TYPE * pIpv6NextHop,
                                  UINT4 u4ObjName, UINT1 *pu1ObjVal));

INT4
Rtm6ApiGetFwdRouteEntryInCxt PROTO ((UINT4 u4ContextId,
                         tIp6Addr * pDest, UINT1 u1PrefixLen,
                         tNetIpv6RtInfo * pNetIpv6RtInfo));

INT4
Rtm6ApiTrieGetFirstEntryInCxt PROTO ((UINT4 u4ContextId,
                               tNetIpv6RtInfo * pNetIpv6RtInfo));

INT4
Rtm6ApiTrieGetNextEntry PROTO ((tNetIpv6RtInfo * pNetIpv6RtInfo,
                         tNetIpv6RtInfo * pNextNetIpv6RtInfo));

INT4
Rtm6ApiTrieGetFirstBestRtInCxt PROTO ((UINT4 u4ContextId,
                                tNetIpv6RtInfo * pNetIpv6RtInfo));

INT4
Rtm6ApiTrieGetNextBestRtInCxt PROTO ((UINT4 u4ContextId,
                          tIp6Addr * pDest, UINT1 u1PrefixLen,
                          UINT4 u4Ipv6RouteIndex,
                          tNetIpv6RtInfo * pNetIpv6RtInfo));

INT4
Rtm6ApiGetBestRouteEntryInCxt PROTO ((UINT4 u4ContextId,
                          tIp6Addr * pIp6Dest, UINT1 u1PrefixLen,
                          UINT4 u4Ipv6RouteIndex,
                          tNetIpv6RtInfo     *pNetIpv6RtInfo));
INT4
Rtm6GetBestRouteEntryInCxt (UINT4 u4RtmCxtId,
                               tIp6RtEntry *pInRtInfo, tIp6RtEntry ** ppOutRtInfo);

INT4
Rtm6ApiTrieGetFirstRtWithLeastNHInCxt PROTO ((UINT4 u4ContextId,
                                  tNetIpv6RtInfo * pNetIpv6RtInfo));

INT4
Rtm6ApiTrieGetNextRtWithLeastNHInCxt PROTO ((UINT4 u4ContextId,
                                 tIp6Addr * pFsipv6RouteDest,
                                 INT4       i4Fsipv6RoutePfxLength,
                                 INT4       i4Fsipv6RouteProtocol,
                                 tIp6Addr * pFsipv6RouteNextHop,
                                 tNetIpv6RtInfo * pNetIpv6RtInfo));

INT4
Rtm6ApiFindRtEntryInCxt PROTO ((UINT4 u4ContextId, tIp6Addr * pIp6Dest,
                         UINT1 u1PrefixLen, INT1 i1Proto,
                   tIp6Addr * pNextHop, tNetIpv6RtInfo * pNetIpv6RtInfo));

VOID
Rtm6ApiFreeRouteCount PROTO ((UINT4 *u4Count));

INT4
Rtm6ApiSetRtParamsInCxt PROTO ((UINT4 u4ContextId, tIp6Addr * pIp6Dest,
                         UINT1 u1PrefixLen, INT1 i1Proto,
                   tIp6Addr * pNextHop, UINT1 u1ObjType, UINT4 u4ObjValue));

INT4
Rtm6ApiDefRtLookupInCxt PROTO ((UINT4 u4ContextId, 
                                tNetIpv6RtInfo *pNetIp6RtInfo));

INT4
Rtm6ApiIpv6LeakRoute PROTO ((UINT1 u1CmdType,
                      tNetIpv6RtInfo * pNetIp6RtInfo));

INT4 Rtm6ApiHandleProtocolStatusInCxt (UINT4, UINT1, UINT1, UINT4,UINT1,VOID *,INT1);

VOID
Rtm6ApiScanRouteTableForBestRouteInCxt PROTO (
                                       (UINT4 u4ContextId, tIp6Addr * pIp6InAddr,
                                       UINT1 u1InAddrPrefixLen,
                                       INT4 (*pAppSpecFunc) (tIp6RtEntry * pIp6RtEntry,
                                       VOID *pAppSpecData),
                                       UINT4 u4MaxRouteCnt,
                                       tIp6Addr * pIp6OutAddr,
                                       UINT1 *pu1OutAddrPrefixLen, VOID *pAppSpecData));
    
INT4    Rtm6Lock                 PROTO ((VOID));

INT4    Rtm6UnLock               PROTO ((VOID));

INT4    Rtm6RelLock              PROTO ((VOID));

INT4    Rtm6RelUnLock            PROTO ((VOID));

INT4    Rtm6Ip6IsFreeRtTblEntriesAvailable PROTO ((VOID));

INT4    Rtm6ApiGetProtocolPrefInCxt PROTO ((UINT4 u4ContextId, UINT4 u4Proto,
                                            UINT4 *pu4PrefValue));

INT4    Rtm6ApiSetProtocolPrefInCxt PROTO ((UINT4 u4ContextId, UINT4 u4Proto,
                                            UINT4 u4PrefValue));

VOID
Rtm6GRStaleRtMark PROTO((tRtm6RegnId * pRegnId));
INT4    Rtm6IsForwPlanPreserved (tRtm6RegnId *RegnIdv6);
VOID    Rtm6ApiAddOrDelAllRtsInCxt PROTO ((UINT4 u4ContextId, INT4 i4ForwardStatus));
VOID    Rtm6Init PROTO ((VOID));
INT1
Rtm6UtilCheckIsRouteGreater PROTO ((tNetIpv6RtInfo * pIp6RtInfo1, tNetIpv6RtInfo * pIp6RtInfo2));
INT4
Rtm6UtilIpv6LeakRoute (UINT1 u1CmdType, tNetIpv6RtInfo * pNetIp6RtInfo);


#endif /* _RTM6_H */
