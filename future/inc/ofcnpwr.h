/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id $ ofcnpwr.h
 *
 * Description: This file contains the declaration for OFCL NPAPIs
 *
 *****************************************************************************/

#include "ofcnp.h"
/* OPER ID */

#ifndef __OFC_NP_WR__
#define __OFC_NP_WR__

#define  FS_NP_HW_CONFIG_OFC_INFO                             1

/* Required arguments list for the ofc NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

typedef struct {
    tOfcHwInfo  *pOfcHwOfcInfo;
} tOfcNpWrFsNpHwConfigOfcInfo;

typedef struct OfcNpModInfo {
union {
    tOfcNpWrFsNpHwConfigOfcInfo  sFsNpHwConfigOfcInfo;
    }unOpCode;

#define  OfcNpFsNpHwConfigOfcInfo  unOpCode.sFsNpHwConfigOfcInfo;
} tOfcNpModInfo;


/* Function Prototype for Generic NP OpCode specific wrappers
 * defined in Syncenpapi.c */

UINT1 OfcFsNpHwConfigOfcInfo PROTO ((tOfcHwInfo * pOfcHwOfcInfo));
#endif
