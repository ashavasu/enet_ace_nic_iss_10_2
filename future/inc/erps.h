/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: erps.h,v 1.40 2015/11/06 11:34:06 siva Exp $
*
* Description: This file contains the function prototypes and macros of 
*       ERPS module*
*******************************************************************/
#ifndef __ERPS_H__
#define __ERPS_H__

#define  ERPS_SYS_MAX_RING   32

#define  ERPS_LOCK()      ErpsApiLock ()
#define  ERPS_UNLOCK()    ErpsApiUnLock ()

#define ERPS_TRC_MAX_SIZE              255
#define ERPS_TRC_MIN_SIZE              1

#define ERPS_86400_SECS   86400
#define ERPS_3600000_MS   3600000
#define ERPS_60000_MS     60000
#define ERPS_86400000_MS  86400000
#define ERPS_3600_SECS    3600
#define ERPS_1_SECS       1
#define ERPS_60_MINS      60
#define ERPS_1_MIN        1
#define ERPS_1_HOUR       1
#define ERPS_1_MS         1
#define ERPS_1000_MS      1000
#define ERPS_1440_MINS    1440
#define ERPS_24_HOUR      24
#define ERPS_INVALID_TMR_RANGE  0xffffffff
enum {
    ERPS_HW_RING_CREATE,
    ERPS_HW_RING_MODIFY,
    ERPS_HW_RING_DELETE,
    ERPS_HW_ADD_GROUP_AND_VLAN_MAPPING,
    ERPS_HW_DEL_GROUP_AND_VLAN_MAPPING,
    ERPS_HW_ADD_VLAN_MAPPING,
    ERPS_HW_DEL_VLAN_MAPPING
};

/* Request/State notified to Priority Logic to find
 * top priority request, that will be processed by 
 * ERPS state machine.
 */
enum {
    ERPS_RING_NO_ACTIVE_REQUEST = 1,
    ERPS_RING_NO_SWITCH_REQUEST,
    ERPS_RING_CLEAR_REQUEST,
    ERPS_RING_FORCE_SWITCH_REQUEST,
    ERPS_RING_RAPS_FS_REQUEST,
    ERPS_RING_LOCAL_SF_REQUEST,
    ERPS_RING_LOCAL_CLEAR_SF_REQUEST,
    ERPS_RING_RAPS_SF_REQUEST,
    ERPS_RING_RAPS_MS_REQUEST,
    ERPS_RING_MANUAL_SWITCH_REQUEST,
    ERPS_RING_WTR_EXP_REQUEST,
    ERPS_RING_WTR_STOP_REQUEST,
    ERPS_RING_WTR_RUNNING_REQUEST,
    ERPS_RING_WTB_EXP_REQUEST,
    ERPS_RING_WTB_STOP_REQUEST,
    ERPS_RING_WTB_RUNNING_REQUEST,
    ERPS_RING_RAPS_NRRB_REQUEST,
    ERPS_RING_RAPS_NR_REQUEST,
    ERPS_RING_MAX_RING_REQUEST
};

/* State of the ring in the ring state machine */
enum {
    ERPS_RING_DISABLED_STATE,
    ERPS_RING_IDLE_STATE,
    ERPS_RING_PROTECTION_STATE,
    ERPS_RING_MANUAL_SWITCH_STATE,
    ERPS_RING_FORCED_SWITCH_STATE,
    ERPS_RING_PENDING_STATE,
    ERPS_RING_MAX_RING_STATE
};

/* events can be notified to ERPS state machine */
enum {
    ERPS_RING_DISABLE_EVENT,
    ERPS_RING_INIT_EVENT,
    ERPS_RING_CLEAR_EVENT,
    ERPS_RING_LOCAL_FS_EVENT,
    ERPS_RING_RAPS_FS_EVENT,
    ERPS_RING_LOCAL_SF_EVENT,
    ERPS_RING_LOCAL_CLEAR_SF_EVENT,
    ERPS_RING_RAPS_SF_EVENT,
    ERPS_RING_RAPS_MS_EVENT,
    ERPS_RING_LOCAL_MS_EVENT,  
    ERPS_RING_WTR_EXP_EVENT,
    ERPS_RING_WTB_EXP_EVENT,
    ERPS_RING_RAPS_NRRB_EVENT,
    ERPS_RING_RAPS_NR_EVENT,
    ERPS_RING_MAX_RING_EVENT
};

#define ERPS_PORT_STATE_DISABLED      AST_PORT_STATE_DISABLED
#define ERPS_PORT_STATE_BLOCKING      AST_PORT_STATE_DISCARDING
#define ERPS_PORT_STATE_UNBLOCKING    AST_PORT_STATE_FORWARDING
#define ERPS_TUNNEL_STATE_OK            1
#define ERPS_TUNNEL_STATE_FAILED        2


#define ERPS_MAX_HW_HANDLE                 2

#define ERPS_MAX_NAME_LENGTH             36

#define ERPS_RING_REVERTIVE_MODE           1
#define ERPS_RING_NON_REVERTIVE_MODE       2

#define ERPS_RPL_OWNER                     1 
#define ERPS_NON_RPL_OWNER                 2
#define ERPS_RING_AUTO_RECOVERY            1
#define ERPS_RING_MANUAL_RECOVERY          2 /* Bit 1-6 : Reserved
                                              * Bit 7   : DNF status
                                              * Bit 8   : RB status. */
/*Hardware modified port info*/

#define PORT1_UPDATED                     1
#define PORT2_UPDATED                     2
#define PORT1_PORT2_UPDATED               3

#define ERPS_CLEAR_COMMAND_NONE           1
#define ERPS_CLEAR_COMMAND                2
#define ERPS_SWITCH_PORT_NONE             0
/*Defined to give notification to RM and MSR*/
#define ERPS_SWITCH_PORT                  3
enum {
    ERPS_SWITCH_COMMAND_NONE = 1,
    ERPS_SWITCH_COMMAND_FORCE,
    ERPS_SWITCH_COMMAND_MANUAL
};

#define ERPS_NODE_STATUS_PORT1_LOCAL_SF    0x00000001
#define ERPS_NODE_STATUS_PORT2_LOCAL_SF    0x00000002
#define ERPS_NODE_STATUS_PORT1_RAPS_SF     0x00000004
#define ERPS_NODE_STATUS_PORT2_RAPS_SF     0x00000008
#define ERPS_NODE_STATUS_RB                0x00000010
#define ERPS_WTR_TIMER_RUNNING             0x00000020
#define ERPS_HOLDOFF_TIMER_RUNNING         0x00000040
#define ERPS_GUARD_TIMER_RUNNING           0x00000080
#define ERPS_PERIODIC_TIMER_RUNNING        0x00000100
#define ERPS_WTB_TIMER_RUNNING             0x00000200


#define ERPS_TC_LIST_SIZE                  64 

/* ERPS Ring Info */
#define ERPS_MAX_RING_ID                 4294967295U
#define ERPS_MIN_RING_ID                 1

/* VLAN Group Manager - mstp / erps */
#define ERPS_VLAN_GROUP_MANAGER_MSTP 1
#define ERPS_VLAN_GROUP_MANAGER_ERPS 2

/* Defines the maximum vlan group list */
#define ERPS_MAX_VLAN_GROUP_LIST        ((ERPS_MAX_VLAN_GROUP_ID% 8) ?\
                                         ((ERPS_MAX_VLAN_GROUP_ID + 31)/32 * 4) : \
                                         (ERPS_MAX_VLAN_GROUP_ID/8))

/* Protection type - port based / service based */
#define ERPS_PORT_BASED_PROTECTION    1
#define ERPS_SERVICE_BASED_PROTECTION 2

/* Service Type */
#define ERPS_SERVICE_VLAN             1
#define ERPS_SERVICE_MPLS_LSP         2
#define ERPS_SERVICE_MPLS_PW          3
#define ERPS_SERVICE_MPLS_LSP_PW     4

/* Version number of the ring - Version 1 / Version 2 */
#define ERPS_RING_COMPATIBLE_VERSION1 1
#define ERPS_RING_COMPATIBLE_VERSION2 2

/* Maintenance Point Direction - This should be same as the macro values (ECFM_MP_DIR_DOWN, ECFM_MP_DIR_UP defined in ECFM module)*/
#define ERPS_MP_DIR_DOWN        1   /* MP Direction */
#define ERPS_MP_DIR_UP          2   /* MP Direction */

#define ERPS_MONITOR_MECH_CFM              1
#define ERPS_MONITOR_MECH_MPLSOAM          2

/* Management configuration constants used in Minimizing segmentation
 * feature to configure 'Interconnection Node config param`
 * as per Appendix X section X.3.1 of ITU-T in ITU-T G.8032 Y.1344(03/2010). */
enum
{
 ERPS_RING_INTER_CONN_NODE_NONE,
 ERPS_RING_INTER_CONN_NODE_PRIMARY, 
 ERPS_RING_INTER_CONN_NODE_SECONDARY
};

/* Management configuration constants used in Minimizing segmentation
 * feature to configure 'Multiple Failure param` as per Appendix X 
 * section X.3.1 of ITU-T in ITU-T G.8032 Y.1344(03/2010). */
enum
{
    ERPS_RING_MULTIPLE_FAILURE_DISABLED,
    ERPS_RING_MULTIPLE_FAILURE_PRIMARY,
    ERPS_RING_MULTIPLE_FAILURE_SECONDARY
};

/*Presence of ring port - In local line card or remote line card */
#define ERPS_PORT_IN_LOCAL_LINE_CARD 1      
#define ERPS_PORT_IN_REMOTE_LINE_CARD 2

/*ERPS Subportlist List*/
typedef UINT1 tErpsSubPortList [((MAX_ERPS_SUB_PORT_LIST_SIZE + 31)/32) * 4];

/************************ Sub Port Entry ***************************/
typedef struct __tSubPortEntry {
    tRBNodeEmbd        RBNode;      /* RB Node */
    UINT4              u4VpnId;     /* VPN ID */
    UINT4              u4Index;     /* Interface Index*/
    UINT2              u2BitMask;   /* Properties of Vlan for a Port like
                                     * ERPS_SUBPORT_PROPERTY_PORT1
                                     * ERPS_SUBPORT_PROPERTY_PORT2 */
    UINT1              au1Pad[2];   /*Padding */
} tSubPortEntry;

typedef struct _sErpsLspPwInfo
{
#ifndef ERPS_ARRAY_TO_RBTREE_WANTED
    tErpsSubPortList        Port1SubPortList;
                              /* This field corresponds to the mib object
                               * fsErpsRingPort1SubPortList.
                               * Any state change occured in the Port1 of a ring
                               * group will be propogated to the ports configured
                               * as subPortList.
                               */
    tErpsSubPortList        Port2SubPortList;
                              /* This field corresponds to the mib object
                               * fsErpsRingPort2SubPortList.
                               * Any state change occured in the Port2 of a ring
                               * group will be propogated to the ports configured
                               * as subPortList.                               */

    UINT4                  au4VpnList[MAX_ERPS_SUB_PORT_LIST_SIZE];
                              /* This field is used to store the VPN Ids
                               * of the pseudowires in the subport list*/
#else
    tRBTree                SubPortListTable; /*Table for storing SubPortList*/
#endif
    UINT4                  u4RingPort1PwInLabel;
                             /*This field stores the Pw In label of the ring port1*/

    UINT4                  u4RingPort2PwInLabel;
                             /*This field stores the Pw In label of the ring port2*/

    UINT4                  u4RingPort1L3Intf;
                             /*This field stores the L3 intf of the ring port1*/

    UINT4                  u4RingPort2L3Intf;
                             /*This field stores the L3 intf of the ring port2*/

    UINT4                  u4RingPort1VpnId;
                             /*This field stores the VPN Id of the ring port1*/

    UINT4                  u4RingPort2VpnId;
                             /*This field stores the VPN Id of the ring port2*/
    UINT4                  u4RingPort1PhyPort;
       /*This field is used to store the physical port
        * corresponding to the pseudowire port*/
    UINT4                  u4RingPort2PhyPort;       
                            /*This field is used to store the physical port
                              *corresponding to the pseudowire port*/       
    UINT1                  u1Port1TunnelStatus;
                             /*This field is used to store the tunnel status of
                               port1*/
    UINT1                  u1Port2TunnelStatus;
                             /*This field is used to store the tunnel status of
                              port2*/
    UINT1                  au1Pad[2];

}tErpsLspPwInfo;

#define   MAX_ERPS_VLAN_GROUP_LIST_SIZE  (((MAX_ERPS_RING_VLAN_INFO + 31)/32) * 64)
                            
/*ERPS Vlan Group List */
typedef UINT1 tErpsVlanGroupList [MAX_ERPS_VLAN_GROUP_LIST_SIZE];

typedef struct _sErpsHwRingInfo{
    tVlanListExt                  VlanList;
    tErpsLspPwInfo                *pErpsLspPwInfo;
    tErpsVlanGroupList             au1RingVlanGroupList;
                                  /* This field contains the list of vlan groups present in 
                                   * a ring. Here it's added to give the complete list of vlan group's to
                                   * hardware instead of giving single vlan group Id to a ring*/

    VOID                          *pContext;
    UINT4                         u4ContextId;
    UINT4                         u4Port1IfIndex;
    UINT4                         u4Port2IfIndex;
    UINT4                         u4RingId;
    INT4                          ai4HwRingHandle[ERPS_MAX_HW_HANDLE];
    tVlanId                       VlanId;
    UINT2                         u2VlanGroupId;
    UINT2                         u2VlanGroupListCount;
    UINT2                         u2RAPSGroupId;
          /* This field used to represent the bit postion set
              * by VlanGroupList */
    UINT1                         u1Port1Action; 
    UINT1                         u1Port2Action;
    UINT1                         u1RingAction;
    UINT1                         u1ProtectionType;
    UINT1                         u1HwUpdatedPort;
                              /* This field tells which port needs to be
                               * programmed in the Hardware.
                               * This can be filled with
                               * 1.PORT1_UPDATED
                               * 2.PORT2_UPDATED
                               * 3.PORT1_PORT2_UPDATED
                               */ 
    UINT1                         u1UnBlockRAPSChannel;
    /*This object is set to OSIX_ENABLED when SubRing without RAPS Virtual 
     * channel is configured ; By default this object will be set to 
     * OSIX_DISABLED (Sub Ring with Virtual Channel */
    UINT1                     u1HighestPriorityRequest;
                              /* This field represent the Highest Priority event
                               * identified by Priority Logic. */
    UINT1                         u1Service;
    /* NOTE : A new entry added inside this structure should be properly synced inside
     * the functions ErpsSyncNpFsErpsHwRingConfigSync and ErpsSyncNpProcessSyncMsg
     * in erpssync.c - Also the value of macro ERPS_RED_NP_SYNC_MSG_SIZE should be
     * adjusted accordingly .
     * Failing to do so, will affect the ERPS HA functionality */
}tErpsHwRingInfo;

typedef struct _sErpsMbsmInfo{ 
    tMbsmSlotInfo * pSlotInfo;
    UINT1           u1CardAction;
    UINT1           au1Reserved[3];
}tErpsMbsmInfo;

/* Structure for passing the information to the registered application */
typedef struct
{
    tCRU_BUF_CHAIN_DESC *pu1Data;          /*  Pointer to the PDU */
    UINT4               u4Event;           /*  Event that has occured */
       /* The Possible Event IDs are:
        * ECFM_FRAME_LOSS_EXCEEDED_THRESHOLD
        * ECFM_FRAME_DELAY_EXCEEDED_THRESHOLD
        * ECFM_DEFECT_OCCURED_AT_MEP
        * ECFM_CHECKSUM_COMPARISON_FAILED
        * ECFM_AIS_CONDITION_ENCOUNTERED
        * ECFM_AIS_CONDITION_CLEARED
        * ECFM_LCK_CONDITION_ENCOUNTERED
        * ECFM_LCK_CONDITION_CLEARED
        * ECFM_RDI_CONDITION_ENCOUNTERED
        * ECFM_RDI_CONDITION_CLEARED
        * ECFM_MCC_FRAME_RECEIVED
        * ECFM_EXM_FRAME_RECEIVED
        * ECFM_EXR_FRAME_RECEIVED
        * ECFM_VSM_FRAME_RECEIVED
        * ECFM_VSR_FRAME_RECEIVED
        * ECFM_APS_FRAME_RECEIVED
        */
    UINT4               u4ContextId;       /* Context Id of th switch */
    UINT4               u4IfIndex;         /* IfIndex of the interface */
    UINT4               u4VlanIdIsid;      /* Vlan Id */
    UINT4               u4MdIndex;         /* Maintenance Domain Index */
    UINT4               u4MaIndex;         /* Maintenance Association Index */
    UINT2               u2MepId;           /* Maintenance End-Point Id */
    UINT1               u1CfmPduOffset;    /* Pdu Offset */
    UINT1               u1Direction;       /* Direction of MEP installed -
                                              takes values ECFM_MP_DIR_DOWN/
                                              ECFM_MP_DIR_UP */

}tErpsEventNotification;



PUBLIC VOID ErpsMainTask (INT1 *pi1Arg);
PUBLIC VOID ErpsMainRApsTxTask (INT1 *pi1Arg);

/***ERPS API functions ***/
PUBLIC VOID ErpsApiExternalEventNotify (tErpsEventNotification *pEvent);
INT4 ErpsApiDeleteContext (UINT4 u4ContextId);
INT4 ErpsApiLock (VOID);
INT4 ErpsApiUnLock (VOID);
PUBLIC INT4
ErpsApiUpdateCxtNameFromVcm (UINT4 u4ContextId);
PUBLIC INT4
ErpsApiModuleShutDown (VOID);
PUBLIC INT4
ErpsApiModuleStart (VOID);
PUBLIC INT4 
ErpsApiIsErpsStartedInContext (UINT4 u4ContextId); 

/* MSR */
PUBLIC UINT1 ErpsGetClearValue(UINT4 u4ContextId, UINT4 u4RingId,INT4 *i4ClrSaveStatus); 
/* HITLESS RESTART */
PUBLIC INT4
ErpsPortSendStdyStPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4PktSize,
        UINT4 u4IfIndex);

PUBLIC INT4
ErpsApiNotificationRoutine (UINT4 u4ModId, VOID *pEventNotification);

#ifdef MBSM_WANTED
INT4
ErpsMbsmPostMessage (tMbsmProtoMsg *pProtoMsg, INT4 i4Event);
#endif
#endif /*__ERPS_H__*/
