/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ofcl.h,v 1.5 2014/08/14 11:12:54 siva Exp $
 *
 * Description: This file contains declaration of function
 *              prototypes of ofc module.
 *******************************************************************/

#ifndef __OFCL_H__
#define __OFCL_H__

#define OFC_SUCCESS     OSIX_SUCCESS
#define OFC_FAILURE     OSIX_FAILURE

#define OFC_INVALID_CONTEXT  9999

#define OF_PORT_CREATE  0
#define OF_PORT_DELETE  1
#define OF_PORT_MODIFY  2

#define OF_FLOW_CREATE  3
#define OF_FLOW_DELETE  4

/*
 * The LA interfaces for openflow are loaned from ISS, so update the
 * macros below incase there is any changes in the range
 */
#define OFC_LA_MIN_IF   (SYS_DEF_MAX_PHYSICAL_INTERFACES + 1)
#define OFC_LA_MAX_IF   (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG)

#define OFC_TO_CFA         30
#define OFC_IP_CHKSUM_OFF  10

INT4 OpenflowNotifyPortStatus(UINT4 u4IfIndex, INT1 action);
INT4 OpenflowNotifyPktRecv (UINT4 u4IfIndex, tCRU_BUF_CHAIN_HEADER *pBuf);
INT4 OpenflowNotifyPortCreateDelete(UINT4 u4IfIndex, UINT4 u4Context, INT1 i1action);

PUBLIC VOID OfcTaskSpawnOfcTask PROTO((INT1 *pi1Arg));
INT4 OfcVlanSetPorts (UINT4 u4VlanId, UINT1 *pu1MemberPorts ,
                      UINT1 *pu1UntaggedPorts , UINT4 u4Flag);
INT4 OfcVlanCreateOpenflowVlan (UINT4 u4VlanId);
INT4 OfcVlanDeleteOpenflowVlan (UINT4 u4VlanId);
INT4 OfcGetContextIdFromIfIndex (UINT4 u4IfIndex, UINT4  *pu4ContextId);
INT4 OfcHandleOpenflowSwModeProcess (UINT4 *pu4IfIndex, tCRU_BUF_CHAIN_HEADER *pDataBuf);
INT4 OfcHandleVlanPkt(tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 *pu4VlanIfIndex);

/* Inband APIs */
UINT4 OfcInbandCntlrRxPktCheck (UINT4 *pu4IfIndex, tCRU_BUF_CHAIN_HEADER *pDataBuf);
UINT4 OfcInbandCntlrTxPktCheck (UINT1 *pu1DataBuf, UINT4 u4PktSize);

/* Hybrid APIs */
UINT4 OfcHybridHandlePktFromIss (UINT4 u4IfIndex, tCRU_BUF_CHAIN_HEADER *pBuf);
UINT4 OfcHybridRxPktFromIssCheck (UINT4 u4IfIndex, UINT4 *pu4ContextId);
#endif   /* __OFCL_H__ */

/*-----------------------------------------------------------------------*/
/*                       End of the file  ofcl.h                         */
/*-----------------------------------------------------------------------*/


