/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: snp.h,v 1.55 2017/11/27 13:56:07 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              by  external modules.
 *
 *******************************************************************/
#ifndef _SNP_H
#define _SNP_H

#include "fsvlan.h"
#ifdef PBB_WANTED
#include "pbb.h"
#endif
#include "l2iwf.h" 
#include "bridge.h"
#include "utilipvx.h"

#define   SNOOP_SUCCESS                     0
#define   SNOOP_FAILURE                    -1
#define   SNOOP_TRUE                        1
#define   SNOOP_FALSE                       0
#define   SNOOP_ENABLE                      1
#define   SNOOP_DISABLE                     2
#define   SNOOP_STARTED                     1
#define   SNOOP_SHUTDOWN                    2
#define   SNOOP_SHUTDOWN_IN_PROGRESS        3
#define   SNOOP_VLAN_ADD                    1
#define   SNOOP_VLAN_DELETE                 2
#define   SNOOP_INSTANCE_ID                 0

#define   SNOOP_ENABLED                     0
#define   SNOOP_DISABLED                    -1

/* For indicating port add/removal event with aggregation
 * to snooping module from l2iwf module.
 *
 */

#define SNOOP_ADD_PORT_TO_AGG              16
#define SNOOP_REMOVE_PORT_FROM_AGG         17
#define   SNOOP_MAX_SISP_INTERFACES          CFA_MAX_SISP_INTERFACES

#define   SNOOP_MAX_PHY_PORTS              BRG_MAX_PHY_PORTS
#define   SNOOP_MAX_PORTS                  SYS_DEF_MAX_INTERFACES + \
                                           SNOOP_MAX_SISP_INTERFACES

/* This macro is added for pseudo wire visibility to SNOOPING.
 * Where this is specific to snooping module. It is allowed to change
 * this macro from SYS_DEF_MAX_L2_PSW_IFACES to some other value.
 * But it should be less than SYS_DEF_MAX_L2_PSW_IFACES.
 */
#define   SNOOP_MAX_L2_PW_IFACES           SYS_DEF_MAX_L2_PSW_IFACES

/* This macro is added for Attachment Circuit interface for SNOOPING.
 * Where this is specific to snooping module. It is allowed to change
 * this macro from SYS_DEF_MAX_L2_AC_IFACES to some other value.
 * But it should be less than SYS_DEF_MAX_L2_AC_IFACES.
 */
#define   SNOOP_MAX_L2_AC_IFACES           SYS_DEF_MAX_L2_AC_IFACES

#define   SNOOP_MAX_NVE_IFACES             SYS_DEF_MAX_NVE_IFACES

#define   SNOOP_MAX_PORTS_PER_INSTANCE     (L2IWF_MAX_PORTS_PER_CONTEXT +\
                                            SNOOP_MAX_L2_PW_IFACES + \
                                            SNOOP_MAX_L2_AC_IFACES + \
                                            SNOOP_MAX_NVE_IFACES   + \
                             SYS_DEF_MAX_SBP_IFACES)

#define SNOOP_MAX_PHY_PLUS_LAG_PORTS       (SYS_DEF_MAX_PHYSICAL_INTERFACES \
         + LA_MAX_AGG_INTF )

#define   SNOOP_IS_SISP_PORT(u4IfIndex, u1Result) \
          {\
       if (SISP_IS_LOGICAL_PORT (u4IfIndex) == VCM_TRUE)\
       {\
    u1Result = SNOOP_TRUE;\
       }\
       else\
       {\
    u1Result = SNOOP_FALSE;\
       }\
   }
                                                 
/* Default Router port purge interval */
#define   SNOOP_DEF_RTR_PURGE_INTERVAL     125

/* Maximum number inner vlan is supported by the system */
#define   SNOOP_PORT_MAX_INNER_VLAN          100

/* Maximum number of message posted by STP module in one go*/
#ifdef PVRST_WANTED
#define   SNOOP_STP_TOPO_CHG_MSG           AST_MAX_PVRST_INSTANCES
#else
#define   SNOOP_STP_TOPO_CHG_MSG           AST_MAX_MST_INSTANCES
#endif

#define   SNOOP_MAX_PORT_CFG_ENTRIES   (SNOOP_MAX_PHY_PORTS * SNOOP_PORT_MAX_INNER_VLAN)

#define   SNOOP_RATELIMIT_ADD               1
#define   SNOOP_RATELIMIT_REMOVE            2

/* Added SNOOP_MAX_L2_AC_IFACES for Attachment Circuit interface */

#define   SNOOP_PORT_LIST_SIZE             (((L2IWF_MAX_PORTS_PER_CONTEXT + \
                                              SNOOP_MAX_L2_PW_IFACES + \
                                              SNOOP_MAX_L2_AC_IFACES + \
                                              SNOOP_MAX_NVE_IFACES  + \
                                              SYS_DEF_MAX_SBP_IFACES)+ 31)/32 * 4)

#define   SNOOP_IF_PORT_LIST_SIZE          ((SNOOP_MAX_PORTS + 31)/32 *4)

#define   SNOOP_SNMP_PORT_LIST_SIZE        SNMP_MAX_OCTETSTRING_SIZE

#define   SNOOP_PORTS_PER_BYTE             8
#define   SNOOP_BIT8                       0x80

#define   SNOOP_ZERO                       0
#define   SNOOP_TWO                        2

#define SNOOP_MCAST_FWD_MODE_IP            1
#define SNOOP_MCAST_FWD_MODE_MAC           2

#define SNOOP_RED_MCAST_ENTRY_ONLY_IN_HW   1
#define SNOOP_RED_MCAST_PORTS_ONLY_IN_HW   2
#define SNOOP_RED_MCAST_ENTRY_ONLY_IN_SW   3
#define SNOOP_RED_MCAST_PORTS_ONLY_IN_SW   4

/* These definitons needs to be removed
 * Kept for avoiding compilations errors so do not use these defintions */
#define   SNOOP_FORWARD                    0
#define   SNOOP_NO_FORWARD                 1
#define   SNOOP_FORWARD_ALL                2
#define   SNOOP_FORWARD_SPECIFIC           3
#define   SNOOP_LOOKUP_MCAST_FWD_TBL       4
#define   tSnoopMsgAtt                     tMSG_ATTR

/* NPAPI definitions */
#define   SNOOP_HW_CREATE_ENTRY            1
#define   SNOOP_HW_DELETE_ENTRY            2
#define   SNOOP_HW_ADD_PORT                3
#define   SNOOP_HW_DEL_PORT                4


#define   SNOOP_IP_PKT_TYPE                0x800 
#define   SNOOP_IGMP_PROTOCOL              2

/* SNOOP Port list size */
typedef UINT1 tSnoopPortList[SNOOP_PORT_LIST_SIZE];
typedef UINT1 tSnoopIfPortList[SNOOP_IF_PORT_LIST_SIZE];
typedef UINT1 tSnoopVlanList[VLAN_LIST_SIZE];
#define MAX_SNP_IPMC_INFO 1

typedef struct IgsHwIpFwdInfo {
    UINT4              u4Instance; 
    UINT4              u4GrpAddr;
    UINT4              u4SrcAddr;
    tVlanId            OuterVlanId;
    tVlanId            InnerVlanId;
    tPortList          PortList;
    tPortList          UntagPortList;
    UINT4              u4SnoopHwId; /* Stores the Multicast group hardware handle in  IGS */
} tIgsHwIpFwdInfo;

typedef struct IgsHwRateLmt
{
    UINT4         u4Port;
    UINT4         u4Rate;
    tVlanId       InnerVlanId;
    UINT1         au1Reserved[2]; /* PADDING */
}tIgsHwRateLmt;


#define   tSnoopTimerListId                tTimerListId
#define   tSnoopVlanId                     tVlanId
#define   tSnoopVlanInfo                   tVlanTag
#define   tSnoopPortBmp                    tSnoopPortList
#define   tSnoopIfPortBmp                  tSnoopIfPortList

#define SNOOP_ADDR_TYPE_IPV4 IPVX_ADDR_FMLY_IPV4
#define SNOOP_ADDR_TYPE_IPV6 IPVX_ADDR_FMLY_IPV6

#define   SNOOP_LOCK() SnpLock ()
#define   SNOOP_UNLOCK() SnpUnLock ()

#define   SNOOP_IS_IGS_ENABLED(InstId) SnoopIsIgmpSnoopingEnabled (InstId)
/* MLDS_CHANGES */
#define   SNOOP_IS_MLDS_ENABLED(InstId) SnoopIsMldSnoopingEnabled (InstId)


#define IGS_LEAVE_EXPLICIT_TRACK       1
#define IGS_LEAVE_FAST_LEAVE           2
#define IGS_LEAVE_NORMAL_LEAVE         3

#define FS_IGS_NP_CONTROL_DRIVEN_ENABLE 0xFFFF
#define FS_IGS_NP_CONTROL_DRIVEN_DISABLE 0xFFFE
enum {
  SNOOP_PORT_LMT_TYPE_NONE=0,
  SNOOP_PORT_LMT_TYPE_GROUPS,
  SNOOP_PORT_LMT_TYPE_CHANNELS
};


/*  Port Config Entries default values */
#define SNOOP_PORT_CFG_RATE_LMT_DEF         0xFFFFFFFF
#define SNOOP_PORT_CFG_RATE_LMT_TYPE_DEF    0
#define SNOOP_PORT_CFG_PROFILE_ID_DEF       0

#define   SNOOP_MAX_VLAN_TAGS         2
typedef   tVlanId tSnoopTag[SNOOP_MAX_VLAN_TAGS];

INT4 SnpLock (VOID);
INT4 SnpUnLock (VOID);

VOID SnoopMain  (INT1 *pi1Param);

INT1
SnoopMiGetForwardingType (UINT4 u4Instance);

INT4
SnoopMainEnqueueMcastFrame (tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2IfIndex,
                      tSnoopVlanId VlanId, UINT1 u1AddressType);
INT4
SnoopEnqueueMulticastFrame (tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2IfIndex,
                            tSnoopVlanInfo VlanInfo, UINT2 u2TagLen, 
                            UINT1 u1AddressType);
INT4
SnoopCreateVlanIndication (UINT4 u4Instance, tSnoopVlanId VlanId);

INT4
SnoopDeletePort (UINT4 u4IfIndex);

INT4
SnoopIsIgmpSnoopingEnabled (UINT4 u4Instance);

INT4
SnoopIsMldSnoopingEnabled (UINT4 u4Instance);

INT4
SnoopPortOperIndication (UINT4 u4IfIndex, UINT1 u1Status);

INT4
SnoopWrPortOperIndication (UINT4 u4IfIndex, UINT1 u1Status,
                           tL2IwfContextInfo * pL2IwfContextInfo);

INT4
SnoopMiDelMcastFwdEntryForVlan (UINT4 u4ContextId, tSnoopVlanId VlanId);

INT4
SnoopMiUpdatePortList  (UINT4 u4ContextId, tSnoopVlanId VlanId, tMacAddr McastAddr,
                      tSnoopIfPortBmp AddPortBitmap, 
                      tSnoopIfPortBmp DelPortBitmap, UINT1 u1PortType);

INT4 SnoopIndicateTopoChange (UINT4 u4IfIndex, tSnoopVlanList VlanList );

#ifdef MBSM_WANTED
INT4
SnoopMbsmProcessUpdateMessage (tMbsmProtoMsg * pProtoMsg, INT4 i4Cmd);
#endif /*MBSM_WANTED */

/* For MI these functions are added */
INT4 SnoopCreateContext (UINT4 u4ContextId);
INT4 SnoopDeleteContext (UINT4 u4ContextId);
INT4 SnoopMapPort (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2Localport);
INT4 SnoopUnmapPort (UINT4 u4IfIndex);
INT4 SnoopUpdateContextName (UINT4 u4ContextId);

/* These functions needs to be removed 
 * Dependency removal. Its kept here for avoiding Compilation Errors. */
INT4
SnoopUpdateVlanStatus (BOOL1 b1Status);

INT4
SnoopCreatePort (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2LocalPortId);

INT4
SnoopProcessMultiCastFrame (tCRU_BUF_CHAIN_HEADER * pBuf,
                          tBrgIf * pBrgIf,
                          tSnoopVlanId VlanId,
                          UINT2 *pu2Result, tSnoopPortList PortList);
INT4
SnoopProcessMcastDataPkt (tCRU_BUF_CHAIN_HEADER * pBuf, tSnoopMsgAtt* pMsgAttr);


INT4 SnoopRedMcastAuditSyncMsg (UINT4 u4Instance, 
                                UINT1 u1Type, tVlanId VlanId,
                              tMacAddr MacGroupAddr, 
                              tPortList AuditPortBmp);

INT4  SnoopRedRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen);



INT4
SnoopUtilGetMcIgsFwdPorts PROTO ((tSnoopVlanId VlanId, UINT4 u4GrpAddr,
                  UINT4 u4SrcAddr, tPortList PortList, tPortList UntagPortList));

INT4
SnoopIsSnoopStartedInContext PROTO ((UINT4 u4ContextId));

INT4
SnoopUtilGetMcMldsFwdPorts PROTO ((tSnoopVlanId VlanId, UINT1 *pu1GrpAddr,
                                   UINT1 *pu1SrcAddr, tPortList PortList,
                                   tPortList UntagPortList));

INT4
SnoopIsRtrPortConfigured PROTO ((UINT4 u4Instance, UINT1 u1AddrType));

VOID SnoopMainDisableSnooping (UINT4 u4Instance, UINT1 u1AddressType);
INT4 SnoopModuleShutdown (VOID);
INT4 SnoopRestartModule (VOID);
INT4 SnoopStartModule (VOID);
INT4
SnoopUtilIsStaticEntry (UINT4 u4PhyPortIndex, UINT4 u4VlanId, UINT1 u1AddrType);

/* Prototype for testing the threshold limit type and limit */
INT4 SnoopTestPortMaxLimit
PROTO ((UINT4 u4Index, UINT1 u1LimitType, UINT4 u4Limit));
/* Prototype for notifying the Aggregation status to snooping
 * from l2iwf module.
 */

INT4
SnoopUpdateHwProperties PROTO ((UINT2 u2AggId , UINT4 u4IfIndex,
                                UINT1  u1Flag));

/*Prototype for mapping the physical interface to its logical interface*/
INT4
SnoopMapLogicalToPhysical PROTO ((UINT4 u4LogicalIndex, UINT4 u4IfIndex));

VOID
SnoopDeleteFwdAndGroupTableDynamicInfo(UINT4 u4ContextId,tMacAddr MacAddr);

INT4
SnoopVlanSetForwardUnregPorts (UINT4 u4VlanIndex, tPortList * pPortList);

INT4
SnoopGetIgsStatus (tVlanId VlanId);

INT4
SnoopGetForwardingDataBase (tVlanId VlanId,tIPvXAddr *pSrcAddr,
                                    tIPvXAddr *pGrpAddr);

UINT1
SnoopLaGetMCLAGSystemStatus (VOID);

INT1
SnoopIcchIsIcclInterface (UINT4 u4PhyPort);

VOID
SnoopLaIsMclagInterface (UINT4 u4IfIndex, UINT1 *pu1IsMclagEnabled);

VOID
SnoopIcchGetIcclIfIndex (UINT4 *pu4IfIndex);

UINT4
SnoopIcchIsIcclVlan (UINT2 u2VlanId);

INT4
SnoopLaGetAggIndexFromAdminKey (UINT2 u2AdminKey, UINT4 *pu4IfIndex);

INT4
SnoopLaUpdateActorPortChannelAdminKey (UINT4 u4IfIndex, UINT2 *pu2AdminKey);

VOID
SnoopNotifyMcastMode (UINT4 u4SnoopFwdMode);
INT4
SnoopEnqueueRtrAdvtPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex, UINT4 u4PktSize);
VOID
SnoopInformRestorationComplete (VOID);
VOID SnoopNotifyCardRemovalEvent (VOID);

INT4
SnoopUtilUpdateHwIdForIPMC (tIgsHwIpFwdInfo *pIgsHwIpFwdInfo);
UINT1
IgsFsMiNpCreateFwdEntries (UINT4 u4Instance, tSnoopVlanId VlanId,
                               UINT4 u4GrpAddr, UINT4 u4SrcAddr,
tPortList PortList, tPortList UntagPortList);



INT4
SnoopVlanGetAllRtrPorts (UINT4 u4Instance, tVlanId VlanId, tSnoopPortList *SnoopRtrPortList );

#endif /*_SNP_H */
