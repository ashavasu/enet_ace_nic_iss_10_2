/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: enm.h,v 1.6 2011/10/25 10:44:43 siva Exp $
 *
 * Description: This has webnm export function definitions.
 *
 ***********************************************************************/

VOID WebNMLaunch(VOID);
VOID MvsLaunch(VOID);

