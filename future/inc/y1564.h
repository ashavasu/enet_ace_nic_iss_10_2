/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: y1564.h,v 1.6 2016/02/02 12:53:20 siva Exp $
*
* Description: This file contains the function prototypes and macros of 
*       Y1564 module*
*******************************************************************/
#ifndef __Y1564_H__
#define __Y1564_H__

#include "l2iwf.h"

#define  Y1564_LOCK()      Y1564ApiLock ()
#define  Y1564_UNLOCK()    Y1564ApiUnLock ()

/* Trace Options */
                                      
#define Y1564_MIN_TRC_VAL            Y1564_INIT_SHUT_TRC
#define Y1564_MAX_TRC_VAL            Y1564_ALL_TRC

#define Y1564_MAX_SLA_LIST           (((Y1564_MAX_SLA_LIST_ID + 31) / 32 )* 2)
#define Y1564_MIN_SLA_LIST            Y1564_MIN_SLA_LIST_ID


/* SLA current test state */
#define Y1564_SLA_TEST_NOT_INITIATED          1
#define Y1564_SLA_TEST_COMPLETED              2
#define Y1564_SLA_TEST_IN_PROGRESS            3
#define Y1564_SLA_TEST_ABORTED                4

/* Events related to message queue */
#define  Y1564_CREATE_CONTEXT_MSG                 1
#define  Y1564_DELETE_CONTEXT_MSG                 2
#define  ECFM_Y1564_RESULT_RECEIVED               3
#define  Y1564_UPDATE_CONTEXT_NAME                7

/* Tests */
#define Y1564_TEST_SIMPLE_CIR        0x1
#define Y1564_TEST_STEP_LOAD_CIR     0x2
#define Y1564_TEST_EIR               0x4
#define Y1564_TEST_TRAF_POLICY       0x8

#define Y1564_TEST_ALL               (Y1564_TEST_SIMPLE_CIR    |\
                                      Y1564_TEST_STEP_LOAD_CIR |\
                                      Y1564_TEST_EIR           |\
                                      Y1564_TEST_TRAF_POLICY)
#define Y1564_MIN_TEST_VAL          Y1564_TEST_SIMPLE_CIR
#define Y1564_MAX_TEST_VAL          Y1564_TEST_ALL

/* Port Type Macros */
#define Y1564_TYPE_OTHER               0
#define Y1564_TYPE_AUI                 1
#define Y1564_TYPE_10_BASE_5           2
#define Y1564_TYPE_FOIRL               3
#define Y1564_TYPE_10_BASE_2           4
#define Y1564_TYPE_10_BASE_T           5
#define Y1564_TYPE_10_BASE_FP          6
#define Y1564_TYPE_10_BASE_FB          7
#define Y1564_TYPE_10_BASE_FL          8
#define Y1564_TYPE_10_BROAD_36         9
#define Y1564_TYPE_10_BASE_THD        10
#define Y1564_TYPE_10_BASE_TFD        11
#define Y1564_TYPE_10_BASE_FLHD       12
#define Y1564_TYPE_10_BASE_FLFD       13
#define Y1564_TYPE_100_BASE_T4        14
#define Y1564_TYPE_100_BASE_TXHD      15
#define Y1564_TYPE_100_BASE_TXFD      16
#define Y1564_TYPE_100_BASE_FXHD      17
#define Y1564_TYPE_100_BASE_FXFD      18
#define Y1564_TYPE_100_BASE_T2HD      19
#define Y1564_TYPE_100_BASE_T2FD      20
#define Y1564_TYPE_1000_BASE_XHD      21
#define Y1564_TYPE_1000_BASE_XFD      22
#define Y1564_TYPE_1000_BASE_LXHD     23
#define Y1564_TYPE_1000_BASE_LXFD     24
#define Y1564_TYPE_1000_BASE_SXHD     25
#define Y1564_TYPE_1000_BASE_SXFD     26
#define Y1564_TYPE_1000_BASE_CXHD     27
#define Y1564_TYPE_1000_BASE_CXFD     28
#define Y1564_TYPE_1000_BASE_THD      29
#define Y1564_TYPE_1000_BASE_TFD      30
#define Y1564_TYPE_10GIG_BASE_X       31
#define Y1564_TYPE_10GIG_BASE_LX4     32
#define Y1564_TYPE_10GIG_BASE_R       33
#define Y1564_TYPE_10GIG_BASE_ER      34
#define Y1564_TYPE_10GIG_BASE_LR      35
#define Y1564_TYPE_10GIG_BASE_SR      36
#define Y1564_TYPE_10GIG_BASE_W       37
#define Y1564_TYPE_10GIG_BASE_EW      38
#define Y1564_TYPE_10GIG_BASE_LW      39
#define Y1564_TYPE_10GIG_BASE_SW      40
#define Y1564_TYPE_10GIG_BASE_CX4     41
#define Y1564_TYPE_2_BASE_TL          42
#define Y1564_TYPE_10_PASS_TS         43
#define Y1564_TYPE_100_BASE_BX10D     44
#define Y1564_TYPE_100_BASE_BX10U     45
#define Y1564_TYPE_100_BASE_LX10      46
#define Y1564_TYPE_1000_BASE_BX10D    47
#define Y1564_TYPE_1000_BASE_BX10U    48
#define Y1564_TYPE_1000_BASE_LX10     49
#define Y1564_TYPE_1000_BASE_PX10D    50
#define Y1564_TYPE_1000_BASE_PX10U    51
#define Y1564_TYPE_1000_BASE_PX20D    52
#define Y1564_TYPE_1000_BASE_PX20U    53
#define Y1564_TYPE_40GIG_BASE_X       54
#define Y1564_TYPE_56GIG_BASE_X       57
#define Y1564_TYPE_2500_BASE_THD      55
#define Y1564_TYPE_2500_BASE_TFD      56

/* Current Test Modes */
enum
{
  Y1564_NO_TEST_IN_PROGRESS,
  Y1564_CIR_TEST,
  Y1564_STEP_LOAD_CIR_TEST,
  Y1564_EIR_TEST_COLOR_AWARE,
  Y1564_EIR_TEST_COLOR_BLIND,
  Y1564_TRAF_POLICING_TEST_COLOR_AWARE,
  Y1564_TRAF_POLICING_TEST_COLOR_BLIND,
  Y1564_PERF_TEST
};

typedef enum {
    Y1564_DIRECTION_EXTERNAL = 1,
    Y1564_DIRECTION_INTERNAL = 2
}eY1564Direction;

#define Y1564_STEP_ONE                        1
#define Y1564_STEP_TWO                        2

#define Y1564_STET_RATE_10                   10
#define Y1564_STET_RATE_20                   20
#define Y1564_STET_RATE_25                   25
#define Y1564_STET_RATE_50                   50
#define Y1564_STET_RATE_100                 100

#define Y1564_TEST_NOT_INITIATED              0
#define Y1564_TEST_IN_PROGRESS                1
#define Y1564_TEST_ABORTED                    2

#define Y1564_PERF_TEST_MIN_DURATION          1 /*15 minutes*/
#define Y1564_PERF_TEST_DEF_DURATION          2 /*2 hours*/
#define Y1564_PERF_TEST_MAX_DURATION          3 /*24 hours*/

#define Y1564_ZERO                        0

/* SLA Configuration tests */
#define Y1564_SIMPLE_CIR_TEST                 Y1564_TEST_SIMPLE_CIR
#define Y1564_STEPLOAD_CIR_TEST               Y1564_TEST_STEP_LOAD_CIR
#define Y1564_EIR_TEST                        Y1564_TEST_EIR
#define Y1564_TRAF_POLICING_TEST              Y1564_TEST_TRAF_POLICY

#define Y1564_ALL_TEST                        (Y1564_SIMPLE_CIR_TEST    |\
                                               Y1564_STEPLOAD_CIR_TEST  |\
                                               Y1564_EIR_TEST           |\
                                               Y1564_TRAF_POLICING_TEST)

#define Y1564_NO_TRC                 0x00000000
#define Y1564_INIT_SHUT_TRC          0x00000001
#define Y1564_MGMT_TRC               0x00000002
#define Y1564_CTRL_TRC               0x00000004
#define Y1564_RESOURCE_TRC           0x00000008
#define Y1564_TIMER_TRC              0x00000010
#define Y1564_CRITICAL_TRC           0x00000020
#define Y1564_Y1731_TRC              0x00000040
#define Y1564_TEST_TRC               0x00000080
#define Y1564_SESSION_TRC            0x00000100
#define Y1564_ALL_FAILURE_TRC        0x00000200
#define Y1564_ALL_TRC                (Y1564_INIT_SHUT_TRC |\
                                      Y1564_MGMT_TRC      |\
                                      Y1564_CTRL_TRC      |\
                                      Y1564_RESOURCE_TRC  |\
                                      Y1564_TIMER_TRC     |\
                                      Y1564_CRITICAL_TRC  |\
                                      Y1564_Y1731_TRC     |\
                                      Y1564_TEST_TRC      |\
                                      Y1564_SESSION_TRC   |\
                                      Y1564_ALL_FAILURE_TRC)

#define Y1564_SERV_CONF_COLOR_AWARE            1
#define Y1564_SERV_CONF_COLOR_BLIND            2

#define Y1564_COUPLING_TYPE_DECOUPLED          0
#define Y1564_COUPLING_TYPE_COUPLED            1

#define Y1564_TEST_DURATION_15MIN         15
#define Y1564_TEST_DURATION_2HOUR         2
#define Y1564_TEST_DURATION_24HOUR        24

typedef enum {
    Y1564_ERR_POST_EVENT_FAILED,
    Y1564_ERR_ENQUEUE_FAILED
}tY1564ErrCode;

typedef tOsixSysTime  tY1564SysTime;

typedef struct
{
     UINT4               u4ReqType; 
                            /* Event that has occured. The possible events are
                             * ECFM_RDI_CONDITION_ENCOUNTERED
                             * ECFM_RDI_CONDITION_CLEARED
                             * ECFM_DEFECT_CONDITION_ENCOUNTERED
                             * ECFM_DEFECT_CONDITION_CLEARED
                             * ECFM_Y1564_RESULT_RECEIVED*/
     UINT4               u4ContextId; 
                            /* Context Id of th switch */
     UINT4               u4VlanIdIsid;
                             /* Vlan Id */
     UINT4               u4SlaId;
                             /* SLA Id */
     UINT4               u4MdIndex;         
                             /* Maintenance Domain Index */
     UINT4               u4MaIndex;         
                             /* Maintenance Association Index */
     UINT4               u4Status;
                            /* Y1564_CFM_SIGNAL_OK or
                             * Y1564_CFM_SIGNAL_FAIL */
    UINT4              u4TransId;

     UINT2               u2MepId;           
                                 /* Maintenance End-Point Id */
     UINT1               u1Direction;       
                             /* Direction of MEP installed - takes values 
                              * ECFM_MP_DIR_DOWN / ECFM_MP_DIR_UP */
     UINT1               u1SlaCurrentTestState;
                             /* Current Test status of the SLA */
     struct
     {
         tMacAddr        TxSrcMacAddr;
         tMacAddr        TxDstMacAddr;
         tVlanId         InVlanId;
         tVlanId         OutVlanId;
         UINT4           u4IfIndex;
         UINT4           u4TagType;
         UINT4           u4PortSpeed;
         UINT4           u4PortType;
     }TestParams;

     struct
     {
         UINT4               u4CIRPps;

         UINT4            u4YellowFrPps;

         UINT4               u4StatsIrMean;
                                /* Information rate mean */
         UINT4               u4StatsTxPkts;
                                /* Total Packets transmitted */
         UINT4               u4StatsRxPkts;
                                /* Total Packets transmitted */
         FS_UINT8            u8StatsTxBytes;
                                /* Total Packets transmitted */
         FS_UINT8            u8StatsRxBytes;
                                /* Total Packets transmitted */
         UINT4               u4YellowFrTxPkts;
                                /* Total Packets transmitted */
         UINT4               u4YellowFrRxPkts;
                                /* Total Packets transmitted */
         FS_UINT8            u8YellowFrTxBytes;
                                /* Total Packets transmitted */
         FS_UINT8            u8YellowFrRxBytes;
                                /* Total Packets transmitted */
         UINT4               u4StatsFarEndFrmLossThreshold;
                                /* Far End Frame Loss  */
         UINT4               u4StatsNearEndFrmLossThreshold;
                                /* Near End Frame Loss  */
         UINT4               u4StatsFrTxDelayMin;
                                /* Frame Tx Delay min */
         UINT4               u4StatsFrTxDelayMean;
                                /* Frame Tx Delay mean */
         UINT4               u4StatsFrTxDelayMax;
                                /* Frame Tx Delay max */
         UINT4               u4StatsFrDelayVarMin;
                                /* Frame Delay  variation min */
         UINT4               u4StatsFrDelayVarMean;
                                /* Frame Delay variation mean */
         UINT4               u4StatsFrDelayVarMax;
                                /* Frame Delay variation max */
         UINT4               u4UnAvailCnt;
                               /* UnAvaiability count */
         FLT4                f4StatsAvailability;
         UINT2               u2FrameSize;
         UINT1               u1TestMode;
         UINT1               au1Pad [1];
     }ResultInfo;

}tY1564ReqParams;

#if 1
/*This structure is the output for the Entry function of BFD*/
typedef struct
{
    tY1564ErrCode    eY1564ErrCode;
}tY1564RespParams;
#endif

PUBLIC VOID Y1564Task (INT1 *pi1Arg);

/***Y1564 API functions ***/
PUBLIC INT4
Y1564ApiModuleShutDown (VOID);

PUBLIC INT4
Y1564ApiModuleStart (VOID);


INT4
Y1564ApiLock (VOID);

INT4
Y1564ApiUnLock (VOID);

PUBLIC INT4
Y1564ApiCreateContext (UINT4 u4ContextId);

PUBLIC INT4
Y1564ApiDeleteContext (UINT4 u4ContextId);

PUBLIC INT4
Y1564ApiUpdateCxtNameFromVcm (UINT4 u4ContextId);

INT4
Y1564ApiHandleExtRequest (tY1564ReqParams * pY1564ReqParams,
                          tY1564RespParams * pY1564RespParams);

PUBLIC UINT1 Y1564UtilGetEmixStatus (UINT4 u4ContextId, UINT4 u4TrafProfId,
                                     UINT1 *u1EmixSelected);
#endif /*__Y1564_H__*/
