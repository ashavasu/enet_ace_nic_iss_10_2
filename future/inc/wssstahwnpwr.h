#ifndef __WSS_STA_HW_NP_WR_H__
#define __WSS_STA_HW_NP_WR_H__

#ifdef NPAPI_WANTED
#include "capwap.h"
#include "npapi.h"

#define WLAN_CLIENT_ADD 1
#define WLAN_CLIENT_GET 2
#define WLAN_CLIENT_DELETE 3
#define WLAN_CLIENT_RULE_ADD 4
#define WLAN_CLIENT_RULE_DEL 5
#define WLAN_WIFI_LEN 5
#define WLAN_RADIOIF_INDEX 1
typedef struct {
    tWlanClientParams *pWlanClientNpParams;
} tWlanClientNpWrAdd;

typedef struct {
    tWlanClientParams *pWlanClientNpParams;
} tWlanClientNpWrGet;

typedef struct {
    tWlanClientParams *pWlanClientNpParams;
} tWlanClientNpWrDelete;
typedef struct {
        tWlanClientRuleParams *pWlanClientRuleParams;
} tWlanClientNpWrRule;
typedef struct WlanClientNpModInfo {
union {
    tWlanClientNpWrAdd sWlanClientNpAdd;
    tWlanClientNpWrGet sWlanClientNpGet;
    tWlanClientNpWrDelete sWlanClientNpDelete;
    tWlanClientNpWrRule   sWlanClientNpRuleAdd;
    }unOpCode;

#define  WlanClientNpAdd  unOpCode.sWlanClientNpAdd;
#define  WlanClientNpGet  unOpCode.sWlanClientNpGet;
#define  WlanClientNpDelete  unOpCode.sWlanClientNpDelete;
#define  WlanClientNpRuleAdd unOpCode.sWlanClientNpRuleAdd;
} tWlanClientNpModInfo;
#endif
#endif
