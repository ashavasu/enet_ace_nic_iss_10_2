/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: rfmgmt.h,v 1.17 2017/11/24 10:36:59 siva Exp $
 *
 * Description: This file contains type definitions relating to 
 *              rfmgmt module 
 ********************************************************************/

#ifndef _RFMGMT_H_
#define _RFMGMT_H_

#include "wsswlan.h"
#include "radioif.h"

#define RFMGMT_MAX_NEIGHBOR_AP       20
#define RFMGMT_MAX_CLIENT_SCANNED    32
#define RFMGMT_MAX_CHANNELS          14
#define RFMGMT_MAX_RADIO             MAX_NUM_OF_RADIO_PER_AP
#define RFMGMT_MAX_BSS_PER_RADIO     WSSWLAN_END_WLANID_PER_RADIO
#define RFMGMT_MIN_BSS_PER_RADIO     WSSWLAN_START_WLANID_PER_RADIO
#define CAPWAP_RF_GROUP_NAME_SIZE 19
/* #define RFMGMT_MAX_CHANNELA   37 */
#define RFMGMT_MAX_CHANNELA             RADIOIF_MAX_CHANNEL_A
typedef struct{
UINT4  u4Frequency;
UINT4  u4ChannelNum;
UINT4  u4DFSFlag;
UINT4  u4DFSCacTime;
}tRfMgmtDFSChannelStatus;

typedef struct{
UINT4  u4ChannelNum;
UINT4  u4DFSFlag;

}tDFSChannelStats;
typedef struct{
tDFSChannelStats  DFSChStats[16];
UINT4      u4VendorId;
UINT2      u2MsgEleType;
UINT2      u2MsgEleLen;
UINT1      u1RadioId;
BOOL1        isOptional;
UINT1       au1Pad[2];
}tDFSChannelTable;
typedef struct 
{
    INT2        i2Rssi[RFMGMT_MAX_NEIGHBOR_AP];
    tMacAddr    NeighborMacAddr[RFMGMT_MAX_NEIGHBOR_AP];
    tMacAddr    BSSIDRogueApLearntFrom[RFMGMT_MAX_NEIGHBOR_AP];
    UINT4       u4RogueApLastReportedTime[RFMGMT_MAX_NEIGHBOR_AP];
    BOOL1       isRogueApStatus[RFMGMT_MAX_NEIGHBOR_AP];
    UINT1       u1StationCount[RFMGMT_MAX_NEIGHBOR_AP];  
    BOOL1       isRogueApProcetionType[RFMGMT_MAX_NEIGHBOR_AP];
    UINT1       au1ssid[RFMGMT_MAX_CLIENT_SCANNED][WSSMAC_MAX_SSID_LEN];
    UINT1       au1RfGroupID[RFMGMT_MAX_CLIENT_SCANNED][RF_GROUP_ID_MAX_LEN];
    UINT1       u1EntryStatus[RFMGMT_MAX_NEIGHBOR_AP];
    UINT2       u2ScannedChannel;
    UINT1       u1NeighborApCount;
    UINT1       u1NonGFPresent;
    UINT1       u1OBSSNonGFPresent;
    UINT1       au1Pad[3];
}tRfMgmtScanChannel;

/* Below is the interface structure which will be filled by the AP and send to
 * WLC for each Radio when neighbor AP information are scanned */
typedef struct 
{
    tRfMgmtScanChannel       *ScanChannel;
    UINT4                    u4VendorId;
    UINT4                    u4Dot11RadioType;
    UINT2                    u2MsgEleType;
    UINT2                    u2MsgEleLen;
    UINT1                    u1RadioId;
    BOOL1                    isOptional;
    UINT1                    au1Pad[2];
}tVendorNeighborAp;

typedef struct{
    UINT2    u2NeighborMsgPeriod;
    UINT2    u2NeighborAgingPeriod;
    UINT2    u2ChannelScanDuration;
    INT2     i2RssiThreshold;
    UINT1    u1RadioId;
    UINT1    u1AutoScanStatus;
    UINT1    au1Pad[2];
}tVendorNeighParams;

typedef struct{
    tVendorNeighParams  VendorNeighParams[RFMGMT_MAX_RADIO];
    UINT4          u4VendorId;
    UINT2      u2MsgEleType;
    UINT2          u2MsgEleLen;
    BOOL1      isOptional;
    UINT1      au1Pad[3];
}tVendorNeighConfig;

typedef struct{
    UINT2    u2SNRScanPeriod;
    INT2     i2SNRThreshold;
    UINT1    u1RadioId;
    UINT1    u1SNRScanStatus;
    UINT1    u1BssidScanStatus[RFMGMT_MAX_BSS_PER_RADIO];
    UINT1    u1WlanId;
    UINT1    u1Pad;
}tVendorClientParams;

typedef struct{
    tVendorClientParams VendorClientParams[RFMGMT_MAX_RADIO];
    UINT4      u4VendorId;
    UINT2      u2MsgEleType;
    UINT2      u2MsgEleLen;
    BOOL1        isOptional;
    UINT1      au1Pad[3];
}tVendorClientConfig;

typedef struct{
    UINT1    u1RadioId;
    UINT1    u1ChSwitchStatus;
    UINT1    au1Pad[2];
}tVendorChSwitchParams;

typedef struct{
    tVendorChSwitchParams VendorChSwitchParams[RFMGMT_MAX_RADIO];
    UINT4      u4VendorId;
    UINT2      u2MsgEleType;
    UINT2      u2MsgEleLen;
    BOOL1        isOptional;
    UINT1      au1Pad[3];
}tVendorChSwitchStatus;
typedef struct{
    UINT2    u2TpcRequestInterval;
    UINT1    u111hTpcStatus;
    UINT1    u1RadioId;
}tVendorTpcSpectMgmtParams;

typedef struct{
    tVendorTpcSpectMgmtParams VendorTpcSpectMgmtParams[RFMGMT_MAX_RADIO];
    UINT4      u4VendorId;
    UINT2      u2MsgEleType;
    UINT2      u2MsgEleLen;
    BOOL1        isOptional;
    UINT1      au1Pad[3];
}tVendorTpcSpectMgmt;
typedef struct{
    UINT4      u4VendorId;
    UINT2      u2MsgEleType;
    UINT2      u2MsgEleLen;
    BOOL1      isOptional;
    UINT1      au1RfGroupName[CAPWAP_RF_GROUP_NAME_SIZE];
}tVendorRougeAp;
typedef struct{
    UINT2    u2DfsQuietInterval;
    UINT2    u2DfsQuietPeriod;
    UINT2    u2DfsMeasurementInterval;
    UINT2    u2DfsChannelSwitchStatus;
    UINT1    u1RadioId;
    UINT1    u111hDfsStatus;
    UINT1    u1Pad[2];
}tVendorDfsParamsParams;
typedef struct{
    tVendorDfsParamsParams VendorDfsParamsParams[RFMGMT_MAX_RADIO];
    UINT4      u4VendorId;
    UINT2      u2MsgEleType;
    UINT2      u2MsgEleLen;
    BOOL1        isOptional;
    UINT1      au1Pad[3];
}tVendorDfsParams;

typedef struct{
    UINT4      u4VendorId;
    UINT2      u2MsgEleType;
    UINT2      u2MsgEleLen;
    UINT1      au1ChannelList[37];
    UINT1      u1RadioId;    
    BOOL1        isOptional;
    UINT1           au1Pad[1];
}tVendorChAllowedList;

typedef struct{
    UINT4      u4VendorId;
    UINT2      u2MsgEleType;
    UINT2      u2MsgEleLen;
    UINT2           u2NeighborMsgPeriod;
    UINT2           u2NeighborAgingPeriod;
    UINT2           u2ChannelScanDuration;
    INT2            i2RssiThreshold;
    UINT1           u1RadioId;
    UINT1           u1AutoScanStatus;
    BOOL1        isOptional;
    UINT1           au1Pad[1];
}tNeighApTableConfig;

typedef struct{
    UINT4           u4VendorId;
    UINT2           u2MsgEleType;
    UINT2           u2MsgEleLen;
    UINT2           u2SNRScanPeriod;
    INT2            i2SNRThreshold;
    UINT1           u1BssidScanStatus[RFMGMT_MAX_BSS_PER_RADIO];
    UINT1           u1RadioId;
    UINT1           u1SNRScanStatus;
    UINT1           u1WlanId;
    BOOL1           isOptional;
}tClientTableConfig;

typedef struct{
    UINT4      u4VendorId;
    UINT2      u2MsgEleType;
    UINT2      u2MsgEleLen;
    UINT1               u1RadioId;
    UINT1               u1ChSwitchStatus;
    BOOL1        isOptional;
    UINT1           au1Pad[1];
}tChSwitchStatusTable;
typedef struct{         
    UINT4      u4VendorId;
    UINT2      u2MsgEleType;
    UINT2      u2MsgEleLen;
    UINT2      u2DfsQuietInterval;
    UINT2      u2DfsQuietPeriod;
    UINT2      u2DfsMeasurementInterval;
    UINT2      u2DfsChannelSwitchStatus;
    UINT1      u1RadioId;
    UINT1      u111hDfsStatus;
    BOOL1      isOptional;
    UINT1      au1Pad;
}tDfsParamsTable;
typedef struct{
    UINT4      u4VendorId;
    UINT2      u2MsgEleType;
    UINT2      u2MsgEleLen;
    UINT1      u1RadioId;
    UINT1      u111hTpcStatus;
    UINT2      u2TpcRequestInterval;
    BOOL1      isOptional;
    UINT1      au1Pad[3];
}tTpcSpectMgmtTable;

typedef struct
{
    UINT2      u2MsgEleType;
    UINT2      u2MsgEleLen; 
    UINT4      u4VendorId;
    UINT1      au1RfGroupName[CAPWAP_RF_GROUP_NAME_SIZE];
    UINT1      u1RadioId;
    UINT1      u1WlanId;
    UINT1      au1Pad[3];
}tRougeTableConfig;
typedef struct{
    UINT4      u4VendorId;
    UINT2      u2MsgEleType;
    UINT2      u2MsgEleLen;
    UINT1      u1RfGroupName[CAPWAP_RF_GROUP_NAME_SIZE];
    UINT1      u1RfDetection;
    BOOL1      isOptional;
    UINT1      au1Pad[3];
}tRogueMgmt;

/* Below is the interface structure which will be filled by the AP and send to
 * WLC for each Radio when Client information is scanned */
typedef struct 
{
    UINT4       u4VendorId;
    UINT4       u4Dot11RadioType;
    UINT2       u2MsgEleType;
    UINT2       u2MsgEleLen;
    INT2        i2ClientSNR[RFMGMT_MAX_CLIENT_SCANNED];
    tMacAddr    ClientMacAddress[RFMGMT_MAX_CLIENT_SCANNED];
    UINT1       u1EntryStatus[RFMGMT_MAX_CLIENT_SCANNED];
    UINT1       u1RadioId;
    UINT1       u1ClientCount;
    BOOL1       isOptional;
    UINT1       au1Pad;
}tVendorClientScan;

typedef struct 
{
    tNeighApTableConfig     NeighApTableConfig;
    tClientTableConfig      ClientTableConfig;
    tChSwitchStatusTable    ChSwitchStatusTable;
    tTpcSpectMgmtTable      TpcSpectMgmtTable;
    tDfsParamsTable         DfsParamsTable;          
    tRogueMgmt              RogueMgmt; 
    UINT2                   u2WtpInternalId;
    UINT1                   au1Pad[2];
}tRfMgmtConfigUpdateReq;

typedef struct 
{
    tNeighApTableConfig     NeighApTableConfig;
    tClientTableConfig      ClientTableConfig;
    tChSwitchStatusTable    ChSwitchStatusTable;
    tTpcSpectMgmtTable      TpcSpectMgmtTable;
    tDfsParamsTable         DfsParamsTable;    
    UINT4                   u4ResultCode;
    UINT2                   u2WtpInternalId;
    UINT1                   au1Pad[2];
}tRfMgmtConfigUpdateRsp;

typedef struct 
{
    tNeighApTableConfig     NeighApTableConfig;
    tClientTableConfig      ClientTableConfig;
    tChSwitchStatusTable    ChSwitchStatusTable;
    tTpcSpectMgmtTable      TpcSpectMgmtTable;
    tDfsParamsTable         DfsParamsTable;       
    tDFSChannelTable        DFSChannelTable;
    UINT2                   u2WtpInternalId;
    UINT1                   au1Pad[2];
}tRfMgmtConfigStatusReq;

typedef struct 
{
    tNeighApTableConfig     NeighApTableConfig;
    tClientTableConfig      ClientTableConfig;
    tChSwitchStatusTable    ChSwitchStatusTable;
    tTpcSpectMgmtTable      TpcSpectMgmtTable;
    tDfsParamsTable         DfsParamsTable;      
    tRougeTableConfig       RougeTable;   
    UINT2                   u2WtpInternalId;
    UINT1                   au1Pad[2];
}tRfMgmtConfigStatusRsp;

typedef struct{

   UINT4    u4VendorId;
   UINT2    u2MsgEleType;
   UINT2    u2MsgEleLen;
   UINT2    u2ChannelNum;
   UINT1    u1RadioId;
   BOOL1    isOptional;
   BOOL1     isRadarFound;
   UINT1    au1Pad[3];
}tVendorInfoDFSRadarEventInfo;

typedef struct 
{
    tVendorNeighborAp       VendorNeighborAp;
    tVendorClientScan       VendorClientScan;
    tVendorInfoDFSRadarEventInfo DFSRadarEventInfo;
    UINT2                   u2WtpInternalId;
    UINT1                   au1Pad[2];
}tRfMgmtPmEvtStatsReq;

typedef struct 
{
    UINT2                   u2WtpInternalId;
    tMacAddr                ClientMac;
    UINT1                   u1RadioId;
    UINT1                   au1Pad[3];
}tRfMgmtStaProcessReq;

typedef struct 
{
    tVendorNeighborAp      VendorNeighborAp;
    tVendorClientScan      VendorClientScan;
#ifdef WTP_WANTED
    tVendorNeighConfig     VendorNeighConfig;
    tVendorClientConfig    VendorClientConfig;
    tVendorChSwitchStatus  VendorChSwitchStatus;
    tVendorTpcSpectMgmt    VendorTpcSpectMgmt;
    tVendorChAllowedList   ConfigAllowedList;
    tVendorDfsParams       VendorDfsParams;     

#endif
    UINT4                  u4RadioIfIndex;
    UINT4                  u4Dot11RadioType;
    UINT2                  u2Channel;
    UINT2                  u2TxPower;
    UINT2                  u2WtpInternalId;
    tMacAddr               RadioMacAddr;
    UINT1                  u1WlanId;
    BOOL1                  isPresent;
    UINT1                  au1Pad[2];
}tRfMgmtIntfConfigReq;


typedef struct {
 UINT4  u4RadioIfIndex;
 tMacAddr StaMac;
 INT1  i1TxPower;
 INT1  i1LinkMargin;
}tTpcRspInfo;

typedef struct{
  UINT4     u4RadioIfIndex;
  tMacAddr  StationMacAddr;
  UINT1     u1ChannelNum;
  BOOL1     isRadarPresent;
}tMeasReportInfo;

typedef struct
{
#ifdef WLC_WANTED
    tCRU_BUF_CHAIN_HEADER   *pRcvBuf;
    tTpcRspInfo             TpcRspInfo;
    tMeasReportInfo          MeasRep;
#else
    tRfMgmtDFSChannelStatus DFSChannelInfo[RFMGMT_MAX_CHANNELA];
    UINT4                   u4Frequency;
    UINT4                   u4EventType;
    tMacAddr                NeighborMacAddr;
    UINT2                   u2ScannedChannel;
    tMacAddr                ClientMacAddress;
    INT2                    i2Rssi;
    UINT4                   u4RadioIfIndex;
    INT2                    i2ClientSNR;
    UINT1                   u1NonGFPresent;   
    UINT1                   u1OBSSNonGFPresent;
    UINT1                   u1SecChannelOffset; 
    UINT1                   u1Pad[3];
#endif
    UINT1                   *linearBuf;
    UINT4                   u4MsgType; /* Message Type */
    UINT4                   u4DestIp;
    UINT4                   u4DestPort;
    UINT1      au1ssid[WSSMAC_MAX_SSID_LEN];
    UINT1      au1RfGroupID[RF_GROUP_ID_MAX_LEN];
    UINT1                   u1StationCount;
    UINT4       u4RogueApLastReportedTime;
    UINT2                   u2SessId;
    BOOL1        isRogueApProcetionType;
    BOOL1      isRogueApStatus;    
}tRfMgmtQueueReq;

typedef struct
{
   UINT4                   u4MsgType; /* Message Type */
   UINT4                   u4RadioIfIndex ;
   UINT2                   u2WtpInternalId;
   UINT2                   u2ChannelNum ;
   UINT1                   au1ScannedList[37];
   UINT1                   u1RadioId ;                        
   UINT1                   au1pad[2];
}tRfMgmtScanChannelList ;
typedef struct {
 UINT4       u4MsgType;  /* Message Type */
 union
 {
            /* Message need to add */ 
            tRfMgmtQueueReq     RfMgmtQueueReq;
 }unMsgParam;

}tRfMgmtQMsg;

enum {
    /* Message types used for RFMGMT Receiver task */
    RFMGMT_QUE_MSG,
    RFMGMT_ERR_MSG,
};

typedef struct {
    union {
        tRfMgmtConfigUpdateReq     RfMgmtConfigUpdateReq;
        tRfMgmtConfigUpdateRsp     RfMgmtConfigUpdateRsp;
        tRfMgmtConfigStatusReq     RfMgmtConfigStatusReq;
        tRfMgmtConfigStatusRsp     RfMgmtConfigStatusRsp;
        tRfMgmtPmEvtStatsReq       RfMgmtPmEvtStatsReq;
        tRfMgmtIntfConfigReq       RfMgmtIntfConfigReq;
        tRfMgmtStaProcessReq       RfMgmtStaProcessReq;
        tRfMgmtQueueReq            RfMgmtQueueReq;
        tRfMgmtScanChannelList     RfMgmtScanChannelList;
    }unRfMgmtMsg;
    UINT1     u1Opcode;
    UINT1     au1Pad[3];
}tRfMgmtMsgStruct;

/* Prototypes - RF MGMT Module */
UINT1 WssIfProcessRfMgmtMsg PROTO ((UINT4, tRfMgmtMsgStruct *));
VOID RfMgmtWtpMainTask PROTO ((INT1 *pi1Arg));
VOID RfMgmtWlcMainTask PROTO ((INT1 *pi1Arg));

#ifdef WLC_WANTED
enum {
    WSS_RFMGMT_INIT_MSG = 1,
    RFMGMT_CREATE_RADIO_ENTRY,
    RFMGMT_DELETE_RADIO_ENTRY,
    RFMGMT_WLC_RECV_NEIGHBOR_MSG,
    RFMGMT_WLC_RECV_CLIENT_MSG,
    RFMGMT_WLC_RUN_STATE_INDI_MSG,
    RFMGMT_DELETE_NEIGHBOR_INFO,
    RFMGMT_CONFIG_UPDATE_REQ,
    RFMGMT_CONFIG_UPDATE_RSP,
    RFMGMT_CONFIG_STATUS_REQ,
    RFMGMT_CONFIG_STATUS_RSP,
    RFMGMT_VALIDATE_CONFIG_UPDATE_REQ,
    RFMGMT_VALIDATE_CONFIG_UPDATE_RSP,
    RFMGMT_VALIDATE_CONFIG_STATUS_REQ,
    RFMGMT_VALIDATE_CONFIG_STATUS_RSP,
    RFMGMT_DELETE_CLIENT_STATS,
    RFMGMT_TX_POWER_UPDATE,
    RFMGMT_UPDATE_RADIO_MAC_ADDR,
    RFMGMT_WLAN_DELETION_MSG,
    RFMGMT_PROFILE_DISABLE_MSG,
    RFMGMT_RADIO_DOWN_MSG,
    RFMGMT_VALIDATE_SCANNED_CHANNEL,
    RFMGMT_TPC_RSP_MSG,
    RFMGMT_DFS_INFO_UPDATE, 
    RFMGMT_MEAS_REPORT_MSG, 
    RFMGMT_WLC_RECV_RADAREVENT_MSG,
    RFMGMT_NOTIFY_BINDING_COMPLETE
};
#endif

#ifdef WTP_WANTED
enum {
 WSS_RFMGMT_INIT_MSG = 1,
 RFMGMT_CREATE_RADIO_ENTRY,
 RFMGMT_DELETE_RADIO_ENTRY,
    RFMGMT_NEIGH_SCAN_MSG,
 RFMGMT_WTP_CONSTRUCT_NEIGH_MSG,
 RFMGMT_WTP_CONSTRUCT_CLIENT_SCAN_MSG,
    RFMGMT_CLIENT_SCAN_MSG,
    RFMGMT_CONFIG_UPDATE_REQ,
    RFMGMT_CONFIG_UPDATE_RSP,
    RFMGMT_CONFIG_STATUS_REQ,
    RFMGMT_CONFIG_STATUS_RSP,
    RFMGMT_VALIDATE_CONFIG_UPDATE_REQ,
    RFMGMT_VALIDATE_CONFIG_UPDATE_RSP,
    RFMGMT_VALIDATE_CONFIG_STATUS_REQ,
    RFMGMT_VALIDATE_CONFIG_STATUS_RSP,
    RFMGMT_CHANNEL_UPDATE_MSG,
    RFMGMT_TXPOWER_UPDATE_MSG,
    RFMGMT_DELETE_CLIENT_STATS,
    RFMGMT_VENDOR_NEIGHBOR_AP_TYPE,
    RFMGMT_VENDOR_CLIENT_SCAN_TYPE,
    RFMGMT_WLAN_DELETION_MSG,
    RFMGMT_PROFILE_DISABLE_MSG,
    RFMGMT_RADIO_DOWN_MSG,
    RFMGMT_SCAN_CHANNEL_LIST,
    RFMGMT_DFS_CHANNEL_INFO,
    RFMGMT_RADAR_EVENT
};
#endif

UINT1 WssIfProcessRfmgmtMsg PROTO ((UINT4, tRfMgmtMsgStruct *));

#endif
