/********************************************************************
 * Copyright (C) Future Software Limited, 2001
 *
 * Description: This file contains exported definitions and macros
 *              of rsna module
 * $Id: rsna.h,v 1.24 2017/11/24 10:36:59 siva Exp $
 *******************************************************************/
#ifndef __RSNA_H__
#define __RSNA_H__


#define RSNA_MAX_KEY_LEN             512 /* Max Length of a Key */
#define RSNA_MAX_KEY_DESC            5   /* Max no of Key Descriptors  used in mlme setKeys  */
#define RSNA_MAX_PROT_DESC           25  /* max size of protection array */
#define RSNA_MAX_CONTEXT_LEN         1024
#define RSNA_MAX_ENET_FRAME_SIZE             1520
#define RSNA_MAC_ADDR_LEN_ALIGNED          MAC_ADDR_LEN + 2

#define RSNA_MAX_SERVER_KEY_SIZE     120 

#define RSNA_MAX_SUPP                       256
#define RSNA_INIT_VAL                       0
#define RSNA_TKIP_INIT_COUNT                1  

#define RSNA_LOCK() RsnaLock ()
#define RSNA_UNLOCK() RsnaUnLock ()

#if OSIX_HOST == OSIX_BIG_ENDIAN

#define CPU_TO_LE16(x) (UINT2)(((x & 0xFF00)>>8) | ((x & 0x00FF)<<8)) 
#define CPU_TO_LE32(x) (UINT4)(((x & 0xFF000000)>>24) | \
                              ((x & 0x00FF0000)>>8)  | \
                              ((x & 0x0000FF00)<<8 ) | \
                              ((x & 0x000000FF)<<24)   \
                             )
#define LE16_TO_CPU(x) (UINT4)(CPU_TO_LE16(x))
#define LE32_TO_CPU(x) (UINT4)(CPU_TO_LE32(x))

#elif OSIX_HOST == OSIX_LITTLE_ENDIAN

#define CPU_TO_LE16(x) (x) /* MLME_HTONS(x)     (x) */
#define CPU_TO_LE32(x) (x) /* MLME_HTONL(x)     (x) */
#define LE16_TO_CPU(x) (x) /* MLME_NTOHS(x)     (x) */
#define LE32_TO_CPU(x) (x) /* MLME_NTOHL(x)     (x) */

#else

#error Could not determine CPU byte order

#endif

/* Macros for Cipher Suite Selectors */

#define RSNA_CIPHER_SUITE_NONE          gau1RsnaCipherSuiteNone
#define RSNA_CIPHER_SUITE_WEP40         gau1RsnaCipherSuiteWEP40
#define RSNA_CIPHER_SUITE_TKIP          gau1RsnaCipherSuiteTKIP
#define RSNA_CIPHER_SUITE_CCMP          gau1RsnaCipherSuiteCCMP
#define RSNA_CIPHER_SUITE_WEP104        gau1RsnaCipherSuiteWEP104

#define WPA_CIPHER_SUITE_NONE          gau1WpaCipherSuiteNone
#define WPA_CIPHER_SUITE_WEP40         gau1WpaCipherSuiteWEP40
#define WPA_CIPHER_SUITE_TKIP          gau1WpaCipherSuiteTKIP
#define WPA_CIPHER_SUITE_CCMP          gau1WpaCipherSuiteCCMP
#define WPA_CIPHER_SUITE_WEP104        gau1WpaCipherSuiteWEP104
#define WPA_OUI_TYPE                   gau1WpaOuiType



#define RSNA_CCMP_BLOCK_LEN                  8

#define   RSNA_SUCCESS 0
#define   RSNA_FAILURE 1

#define RSN_IE_ELEMENT_LENGTH      1
#define RSN_IE_LENGTH_FIELD        1
#define RSN_IE_VERSION_LENGTH      2
#define RSN_IE_CAPAB_LENGTH        2 
#define RSN_IE_PMKID_COUNT_LENGTH  2 

/* Macros for AKM Suite Selectors */

#define RSNA_AKM_UNSPEC_802_1X           gau1RsnaAuthKeyMgmt8021X
#define RSNA_AKM_PSK_OVER_802_1X         gau1RsnaAuthKeyMgmtPskOver8021X
#define WPA_AKM_PSK_SHA256_OVER_802_1X   gau1WpaAuthKeyMgmtPskSHA256Over8021X
#ifdef PMF_WANTED
#define RSNA_AKM_UNSPEC_802_1X_SHA256    gau1RsnaAuthKeyMgmt8021XSHA256
#define RSNA_AKM_PSK_SHA256_OVER_802_1X  gau1RsnaAuthKeyMgmtPskSHA256Over8021X
#endif


#define WPA_AKM_UNSPEC_802_1X     gau1WpaAuthKeyMgmt8021X
#define WPA_AKM_PSK_OVER_802_1X   gau1WpaAuthKeyMgmtPskOver8021X

/* Macros for Key Data Encapsulation */

#define RSNA_GTK_KDE                         gau1RsnaGtkKde
#define WPA_GTK_KDE                         gau1WpaGtkKde
#define RSNA_STAKEY_KDE                      0x000FAC02
#define RSNA_MAC_ADDR_KDE                    0x000FAC03
#ifdef PMF_WANTED
#define RSNA_IGTK_KDE                        gau1RsnaIGtkKde
#endif
#define RSNA_PMKID_KDE                       gau1RsnaPmkIdKde
#define WPA_PMKID_KDE                        gau1WpaPmkIdKde


#define RSNA_GTK_KDE_LEN                     8
#ifdef PMF_WANTED
#define RSNA_IGTK_KDE_LEN                    14
#endif
#define RSNA_STAKEY_KDE_LEN                  20
#define RSNA_MAC_KDE_LEN                     6
#define RSNA_PMKID_KDE_LEN                   22
#define RSNA_PMKID_ALLIGNED_KDE_LEN          24
#define RSNA_MAX_KEY_DATA_LEN                512

#define RSNA_EID_GENERIC                     0xdd
#define RSNA_GTK_HDR_LEN                     2
#ifdef PMF_WANTED
#define RSNA_IGTK_HDR_LEN                    8
#endif
#define RSNA_IE_ID                           0x30
#define WPA_IE_ID                            0xdd

#define RSNA_NONCE_LEN                       32
#define RSNA_KEY_IV_LEN                      16
#define RSNA_MIC_LEN                         16
#define RSNA_PMK_LEN                         32
#define RSNA_PSK_LEN                         32
#define RSNA_PSK_MIN_LEN                     1
#define RSNA_PSK_MAX_LEN                     32
#define RSNA_REPLAY_CTR_LEN                  8
#define RSNA_GMK_LEN                         32
#define RSNA_GTK_MAX_LEN                     32
#ifdef PMF_WANTED
#define RSNA_IGTK_MAX_LEN                    16
#endif
#define RSNA_KEY_RSC_LEN                     8
#define RSNA_PMKID_LEN                       16
#define RSNA_PMKID_LIST_LEN                  16
#define RSNA_HMACMD5                         1
#define RSNA_HMACSHA1                        2
#define RSNA_RC4                             1
#define RSNA_AES_WRAP                        2
#define RSNA_PTK_LABEL_LEN                   22 
#define RSNA_MAX_PTK_KEY_LEN                 32
#define RSNA_KCK_KEY_LEN                     16
#define RSNA_KEK_KEY_LEN                     16
#define RSNA_ETH_ADDR_LEN                    6
#define RSNA_ALIGNED_ETH_ADDR_LEN            8
#define RSNA_INIT_COUNT_LABLE_LEN            12
#define RSNA_TIME_STAMP_LEN                  8
#define RSNA_PMK_TO_PTK_DATA_LEN             (( 2 * RSNA_ETH_ADDR_LEN) + \
                                               (RSNA_PTK_LABEL_LEN + 2) + \
                                               (2 * RSNA_NONCE_LEN ))

#define RSNA_INIT_CTR_DATA_LEN               27
#define RSNA_PMKID_DATA_LEN                  20

#define RSNA_PTK_BITS                         384
#define RSNA_VALUE_255                        255
#define RSNA_VALUE_256                        256
#define RSNA_SSID_LEN                        32   
#define RSNA_PBKDF2_ITERATIONS               4096
#define RSNA_PASS_PHRASE_LEN                 256
#define RSNA_MIN_PASS_PHRASE_LEN             8
#define RSNA_MAX_PASS_PHRASE_LEN             63
#define RSNA_CIPHER_SUITE_LEN                4
#define RSNA_PAIRWISE_CIPHER_COUNT           2
#define WPA_PAIRWISE_CIPHER_COUNT           1
#define RSNA_MAX_PMKID_IDS                   4
#ifdef PMF_WANTED
#define RSNA_AKM_SUITE_COUNT                 4
#else
#define RSNA_AKM_SUITE_COUNT                 2
#endif
#define RSNA_AKM_SELECTOR_LEN                4
#define RSNA_REPLAY_COUNTER_LEN              8
#define RSNA_ALIGNED_PASS_PHRASE_LEN         260   
#define RSNA_ALIGNED_PSK_LEN                 36       
#define RSNA_MIN_IE_LEN                      22
#define RSNA_IE_PARAMS_LEN                   48

#define WPA_SELECTOR_LEN                     4 



/* Macros for Accessing the Bit Fields of 
 * Key Information Field of the EAPOL Key Frame */ 

#define RSNA_KEY_INFO_MASK                   0x0007  /* Bit 0 | BIT 1| BIT 2 */ 
#define RSNA_KEY_INFO_HMACMD5_RC4            0x0001  /* BIT 0  */
#define RSNA_KEY_INFO_HMACSHA1_AES           0x0002  /* BIT 1  */
#ifdef PMF_WANTED
#define RSNA_KEY_INFO_CMAC_128_AES           0x0003  /* BIT 0 & 1  */
#endif
#define RSNA_KEY_INFO_KEY_TYPE               0x0008  /* BIT 3  */
#define RSNA_KEY_INFO_KEY_INDEX              0x0010  /* BIT 5  */
#define RSNA_KEY_INFO_INSTALL                0x0040  /* BIT 6  */ 
#define RSNA_KEY_INFO_ACK                    0x0080  /* BIT 7  */
#define RSNA_KEY_INFO_MIC                    0x0100  /* BIT 8  */ 
#define RSNA_KEY_INFO_SECURE                 0x0200  /* BIT 9  */
#define RSNA_KEY_INFO_ERROR                  0x0400  /* BIT 10 */
#define RSNA_KEY_INFO_REQUEST                0x0800  /* BIT 11 */
#define RSNA_KEY_INFO_ENCR_KEY_DATA          0x1000  /* BIT 12 */

#ifdef PMF_WANTED
#define RSNA_CAPABILITY_MFPR                 0x0040  /* BIT 6  */ 
#define RSNA_CAPABILITY_MFPC                 0x0080  /* BIT 7  */
#endif

/* Masks for setting the state of the Supplicant */

#define RSNA_STA_AUTHENTICATED               0x01
#define RSNA_STA_ASSOCIATED                  0x02
#define RSNA_STA_AUTHORIZED                  0x04
#define RSNA_STA_PREAUTH                     0x08
#define RSNA_STA_DISASSOCIATED               0x0


#define RSNA_IE_LEN                          255   
#define RSNA_ALIGNED_IE_LEN                  256
#define WPA_ALIGNED_IE_LEN                   24
#define RSNA_PAE_ENET_TYPE                   0x888e 
#define RSNA_OFFSET_NONE                     0 
#define RSNA_EAPOL_KEY                     0x3
#define RSNA_EAPOL_HDR_SIZE                  4 /* Bytes */
#define RSNA_ENET_HDR_SIZE                   14

#define RSNA_ENET_EAPOL_HDR_SIZE (RSNA_ENET_HDR_SIZE+RSNA_EAPOL_HDR_SIZE)


#define RSNA_WEP40_KEY_LEN                   5
#define RSNA_TKIP_KEY_LEN                    32
#ifdef PMF_WANTED
#define RSNA_BIP_KEY_LEN                     16
#endif
#define RSNA_CCMP_KEY_LEN                    16
#define RSNA_WEP104_KEY_LEN                  13        

#define RSNA_MIC_FAIL_CTR_MSR_INTRVL         60
#define RSNA_MIN_DELETE_PMK_LIFETIME         60
#define RSNA_DEF_PMK_LIFETIME                43200
#define RSNA_DEF_PMK_REAUTH_THRESHOLD        70
#define RSNA_MIN_PMK_REAUTH_THRESHOLD        1
#define RSNA_MAX_PMK_REAUTH_THRESHOLD        100

#define RSNA_DEF_GROUP_REKEY_TIME            86400 
#define RSNA_MIN_REKEY_TIME                  360
#define RSNA_MIN_REKEY_PKTS                  1
#define RSNA_DEF_REKEY_PKTS                  2147483647
#define RSNA_MAX_REKEY_PKTS                  4294967100
#define RSNA_DEF_UPDATE_COUNT                3
#define RSNA_MIN_UPDATE_COUNT                1
#define RSNA_MAX_UPDATE_COUNT                4294967295
#define RSNA_DEF_SA_TIME_OUT                 60
#define RSNA_MIN_SA_TIME_OUT                 1
#define RSNA_MAX_SA_TIME_OUT                 604800 /* 1 week */



#define RSNA_TKIP_INDEX                  1
#define RSNA_CCMP_INDEX                  0
#define RSNA_AUTH_PSK_INDEX              0
#define RSNA_AUTH_AS_INDEX               1
#ifdef PMF_WANTED
#define RSNA_AUTH_PSK_SHA256_INDEX       2
#define RSNA_AUTH_AS_SHA256_INDEX        3
#endif
#define RSNA_IE_VERSION_OFF              2  
#define RSNA_VERSION                     1 
#define WPA_VERSION                      1 
#define RSNA_IE_SUITE_COUNT_LEN          2
#define WPA_IE_SUITE_COUNT_LEN           1
#define RSNA_IE_HDR_VER_LEN_OFF          2
#define RSNA_EAPOL_VERSION               2


#define WPA_ENABLED   1 
#define WPA_DISABLED  2
#define RSNA_ENABLED   1 
#define RSNA_DISABLED  2
#define MIX_MODE_ENABLED   1 
#define MIX_MODE_DISABLED  2

#ifdef PMF_WANTED
#define RSNA_MFPC 1
#define RSNA_MFPR 1
#endif

#define RSNA_TKIP  2
#define RSNA_CCMP  1


#define RSNA_AUTHSUITE_PSK               1
#define RSNA_AUTHSUITE_8021X             2 
#ifdef PMF_WANTED
#define RSNA_AUTHSUITE_PSK_SHA256        3 
#define RSNA_AUTHSUITE_8021X_SHA256      4 
#endif 

#define RSNA_GROUP_REKEY_DISABLED  1
#define RSNA_GROUP_REKEY_TIMEBASED 2
#define RSNA_GROUP_REKEY_PACKETBASED 3
#define RSNA_GROUP_REKEY_TIMEPACKETBASED 4

#define RSNA_AKM_PSK_ENABLE       1
#define RSNA_AKM_PSK_DISABLE      2
#define RSNA_AKM_PSK_SETKEY       3
#define RSNA_PSK_TEMP_SIZE        3

#define RSNA_AKM_PSK_SETKEY_ASCII 1
#define RSNA_AKM_PSK_SETKEY_HEX   2

#define RSNA_MAX_CIPHER_TYPE_LENGTH       4
#define RSNA_MAX_AUTH_SUITE_TYPE_LENGTH   4
#define RSNA_MAX_PMKID_LENGTH             20
#define RSNA_MAX_PSK_LENGTH               256
#define RSNA_MAX_PSK_VALUE                32 
#define RSNA_MIN_PSK_VALUE                0 
#define RSNA_MIN_AUTH_SUITE_INDEX         1
#ifdef PMF_WANTED
#define RSNA_MAX_AUTH_SUITE_INDEX         4
#else
#define RSNA_MAX_AUTH_SUITE_INDEX         2
#endif

#define RSNA_MIN_CIPHER_SUITE_INDEX       1
#define RSNA_MAX_CIPHER_SUITE_INDEX       2
#define RSNA_MIN_GROUP_REKEY_PKTS         1
#define RSNA_MAX_GROUP_REKEY_PKTS         4294967295U
#define RSNA_MIN_GROUP_REKEY_TIME         1
#define RSNA_MAX_GROUP_REKEY_TIME         604800U
#define RSNA_MIN_GROUP_UPDATE_COUNT       1
#define RSNA_MAX_GROUP_UPDATE_COUNT       4294967295U
#define RSNA_MIN_PAIRWISE_UPDATE_COUNT    1
#define RSNA_MAX_PAIRWISE_UPDATE_COUNT    4294967295U
#define RSNA_MIN_PMK_LIFETIME             1
#define RSNA_MAX_PMK_LIFETIME             604800U
#define RSNA_MIN_SA_TIMEOUT               1
#define RSNA_MAX_SA_TIMEOUT               604800U
#define RSNA_STATS_MIN_INDEX              1
#define RSNA_CLEAR_FLAG                   1
#define  RSNA_1OF4__SA_TIME_OUT           300

#ifdef PMF_WANTED
#define RSNA_MAX_SAQUERY_RETRY_TIME       4294967295U 
#define RSNA_MIN_SAQUERY_RETRY_TIME       1
 
#define RSNA_MAX_SAQUERY_MAX_TIMEOUT      4294967295U 
#define RSNA_MIN_SAQUERY_MAX_TIMEOUT      1

 
#define RSNA_MAX_ASSOC_COMEBACK_TIME      4294967295U 
#define RSNA_MIN_ASSOC_COMEBACK_TIME      1 
#endif

#define RSNA_GROUP_CIPHER_WEP40_GTK_LEN  5
#define RSNA_GROUP_CIPHER_WEP104_GTK_LEN 13
#define RSNA_GROUP_CIPHER_CCMP_GTK_LEN   16
#define RSNA_GROUP_CIPHER_TKIP_GTK_LEN   32

#define   RSNA_PAIRWISE_CIPHER_CCMP_LEN 128
#define   RSNA_PAIRWISE_CIPHER_TKIP_LEN 256


#define WLAN_MAX_SSID_LEN                         32


#define RF_GROUP_ID_MAX_LEN                       19

PUBLIC CONST UINT1         gau1RsnaAuthKeyMgmt8021X[RSNA_CIPHER_SUITE_LEN] ;
PUBLIC CONST UINT1         gau1RsnaAuthKeyMgmtPskOver8021X[RSNA_CIPHER_SUITE_LEN] ;
#ifdef PMF_WANTED
PUBLIC CONST UINT1         gau1RsnaAuthKeyMgmtPskSHA256Over8021X[RSNA_CIPHER_SUITE_LEN];
PUBLIC CONST UINT1         gau1RsnaAuthKeyMgmt8021XSHA256[RSNA_CIPHER_SUITE_LEN];
#endif
PUBLIC CONST UINT1         gau1RsnaCipherSuiteNone[RSNA_CIPHER_SUITE_LEN] ;
PUBLIC CONST UINT1         gau1RsnaCipherSuiteWEP40[RSNA_CIPHER_SUITE_LEN] ;
PUBLIC CONST UINT1         gau1RsnaCipherSuiteTKIP[RSNA_CIPHER_SUITE_LEN] ;
PUBLIC CONST UINT1         gau1RsnaCipherSuiteCCMP[RSNA_CIPHER_SUITE_LEN] ;
PUBLIC CONST UINT1         gau1RsnaCipherSuiteWEP104[RSNA_CIPHER_SUITE_LEN] ;
PUBLIC CONST UINT1         gau1RsnaPmkIdKde[RSNA_CIPHER_SUITE_LEN] ;
PUBLIC CONST UINT1         gau1WpaPmkIdKde[RSNA_CIPHER_SUITE_LEN] ;
PUBLIC CONST UINT1         gau1RsnaGtkKde[RSNA_CIPHER_SUITE_LEN] ;
PUBLIC CONST UINT1         gau1WpaGtkKde[RSNA_CIPHER_SUITE_LEN] ;
#ifdef PMF_WANTED
PUBLIC CONST UINT1         gau1RsnaIGtkKde[RSNA_CIPHER_SUITE_LEN] ;
PUBLIC CONST UINT1         gau1RsnaCipherSuiteBIP[RSNA_CIPHER_SUITE_LEN] ;
#endif

typedef UINT1 tPmkId[RSNA_PMKID_LEN];

typedef struct {
   UINT1   au1AKMSuite[RSNA_AKM_SELECTOR_LEN];    /* Refers to AKM Suite */
}tRsnaAkmSuiteDB;

typedef struct {
   UINT1   au1AKMSuite[RSNA_AKM_SELECTOR_LEN];    /* Refers to AKM Suite */
}tWpaAkmSuiteDB;


typedef struct {
   UINT1 au1PairwiseCipher[RSNA_CIPHER_SUITE_LEN];    /* Refere to pairwise
                                                          cipher suite */
}tRsnaPwCipherSuiteDB;

typedef struct {
   UINT1 au1PairwiseCipher[RSNA_CIPHER_SUITE_LEN];    /* Refere to pairwise
                                                          cipher suite */
}tWpaPwCipherSuiteDB;



typedef struct RSNAPARAMS {
    tPmkId aPmkidList[RSNA_MAX_PMKID_IDS];
    tRsnaPwCipherSuiteDB aRsnaPwCipherDB[RSNA_PAIRWISE_CIPHER_COUNT];
    UINT2 u2RsnCapab;
    tRsnaAkmSuiteDB aRsnaAkmDB[RSNA_AKM_SUITE_COUNT];
    UINT1 au1GroupCipherSuite[RSNA_CIPHER_SUITE_LEN];
    UINT2 u2PairwiseCipherSuiteCount;
    UINT2 u2AKMSuiteCount;
    UINT2 u2PmkidCount;
    UINT2 u2Ver;
    UINT1 au1GroupMgmtCipherSuite[RSNA_CIPHER_SUITE_LEN]; /*PMF_WANTED*/
    UINT1 u1ElemId;
    UINT1 u1Len;
}tRsnaIEElements ;

typedef struct  {
    tPmkId aPmkidList[RSNA_MAX_PMKID_IDS];
    tWpaPwCipherSuiteDB aWpaPwCipherDB[RSNA_PAIRWISE_CIPHER_COUNT];
    tWpaAkmSuiteDB aWpaAkmDB[RSNA_AKM_SUITE_COUNT];
    UINT4 u4VendorId;
    UINT1 au1GroupCipherSuite[RSNA_CIPHER_SUITE_LEN];
    UINT2 u2PairwiseCipherSuiteCount;
    UINT2 u2AKMSuiteCount;
    UINT2 u2PmkidCount;
    UINT2 u2Ver;
    UINT2 u2MsgEleType;
    UINT2 u2MsgEleLen;
    UINT1 u1ElemId;
    UINT1 u1Len;
    UINT1 u1RadioId;
    BOOL1 isOptional;
}tWpaIEElements ;

typedef struct KEYDESC  {
    UINT1 au1Key[RSNA_MAX_KEY_LEN];
    UINT1 au1StaAddr[RSNA_MAC_ADDR_LEN_ALIGNED];
    UINT1 au1CipherSuite[RSNA_CIPHER_SUITE_LEN];
    UINT1 u1KeyLength;
    UINT1 u1KeyId;
    UINT1 u1KeyType;
    UINT1 u1aligned;
    UINT1 u1AssocType;
    UINT1 au1Pad[3];
} tKeyDesc;

typedef struct DEL_KEYDESC  {
    UINT1 au1StaAddr[RSNA_MAC_ADDR_LEN_ALIGNED];
    UINT1 u1KeyId;
    UINT1 u1KeyType;
    UINT2 u2aligned;
} tDelKeyDesc;

typedef struct MLME_SETKEYS {
        UINT4     u4numKeyDesc;
        tKeyDesc  aKeyDesc[RSNA_MAX_KEY_DESC];
} tMlmeSetKeys;



typedef struct MLME_DELKEYS {
    UINT4    u4numKeyDesc;
    tDelKeyDesc aKeyDescArr[RSNA_MAX_KEY_DESC];
}tMlmeDelKeys;

typedef struct MLME_STAPROT  {
    UINT1 au1StaMac[RSNA_MAC_ADDR_LEN_ALIGNED];
    UINT1 u1ProtType;
    UINT1 u1KeyType;
    UINT2 u2aligned;
}tMlmeStaProt;

typedef struct MLME_SETPROT {
    UINT4        u4numProt;
    tMlmeStaProt protReq[RSNA_MAX_PROT_DESC];
}tMlmeSetProt;


typedef struct  MLME_DISSOC {
    UINT1 au1StaMac[MAC_ADDR_LEN];
    UINT2 u2reasonCode;
    UINT2 u2aligned;
}tMlmeDissoc ;

typedef struct  MLME_DEAUTH  {
    UINT1 au1StaMac[MAC_ADDR_LEN];
    UINT2 u2reasonCode;
    UINT2 u2aligned;
}tMlmeDeauth;

typedef struct APME_RSNSTATS {
    UINT1 au1StaMac[MAC_ADDR_LEN];
    UINT2 u2aligned;
} tApmeRsnStats;

typedef struct APME_RSN_MICFAIL {
    UINT1 au1StaMac[MAC_ADDR_LEN];
} tMlmeMICFailure ;

/* MLME Indications from APME to RSNA          */

typedef struct  MLME_ASSOCIND  {
    UINT1  au1StaMac[MAC_ADDR_LEN];
    UINT1  au1RsnIe[RSNA_IE_LEN];
    /* balaji needs parsed element also */
     /* use same structure as tRsnInfo */
    UINT1  u1RsnaIELen;
} tMlmeAssocInd;

typedef struct MLME_REAASOCIND  {
    UINT1  au1StaMac[MAC_ADDR_LEN];
    UINT1  au1OldApMac[MAC_ADDR_LEN];
    /* balaji needs parsed element also
     * use same structure as tRsnInfo
     */
    UINT1  au1RsnIe[RSNA_IE_LEN];
    UINT1  u1RsnaIELen;
}tMlmeReassocInd;


typedef struct MLME_PNACKEYIND  {
    UINT1  au1StaMac[MAC_ADDR_LEN];
    UINT1  Pad[2];
    UINT1  au1Key[RSNA_MAX_SERVER_KEY_SIZE];
}tMlmePnacKeyInd;

#ifdef PMF_WANTED
typedef struct MLME_SAQUERY {
    UINT1  au1StaMac[MAC_ADDR_LEN];
    UINT2  u2TransactionId;
    UINT2  u2SessId;
    UINT1  au1Pad[2];
}tMlmeSAQuery;


typedef struct {
    UINT1 au1IGTKPktNo[6];
    UINT1 u1KeyIndex;
    UINT1 au1Pad[1];
}tRsnaIGTKSeqInfo;


typedef struct {
    UINT4             u4VendorId;
    UINT2             u2MsgEleType;
    UINT2             u2MsgEleLen;
    tRsnaIGTKSeqInfo  RsnaIGTKSeqInfo[2]; 
    UINT1             u1RadioId;
    UINT1             u1WlanId;
    BOOL1             isOptional;
    UINT1             au1Pad[1];
}tVendorPMFPktInfo; 
#endif

typedef enum {
    WSSRSNA_PROFILEIF_ADD = 0,
    WSSRSNA_PROFILEIF_DEL,
    WSSRSNA_ASSOC_IND,
    WSSRSNA_REASSOC_IND,
    WSSRSNA_DISSOC_IND,
    WSSRSNA_DEAUTH_IND,
    WSSRSNA_MIC_FAILURE_IND,
    WSSRSNA_SET_KEY,
    WSSRSNA_DEL_KEY,
    WSSRSNA_DISSOC_REQ,
    WSSRSNA_DEAUTH_REQ,
    WSSRSNA_PNAC_KEY_AVAILABLE,
#ifdef PMF_WANTED
    WSSRSNA_SAQUERY_REQ_IND,
    WSSRSNA_SAQUERY_RESP_IND
#endif
} enWssRsnaNotifyType;


typedef enum {
    WSSRSNA_PTK_KEY = 1,
    WSSRSNA_GTK_KEY,
    WSSRSNA_IGTK_KEY
}eWssRsnaKeyType;
 

typedef struct WssRSNANotifyParams{
    UINT4 u4IfIndex ; /* this is the CFA index of the wireless device
                       on which the request will be sent
                       */
#ifdef PMF_WANTED
    tVendorPMFPktInfo PMFVendorInfo; 
#endif
    enWssRsnaNotifyType eWssRsnaNotifyType;
    union {
        tMlmeAssocInd      mlmeAssocInd;
        tMlmeReassocInd    mlmeReassocInd;
        tMlmeDissoc        mlmeDissocInd;
        tMlmeDeauth        mlmeDeauthInd;
        tMlmeMICFailure    mlmeMICFailInd;
        tMlmeSetKeys        setKeysReq;
        tMlmeDelKeys        delKeysReq;
        tMlmeSetProt        setProtReq;
        tMlmeDissoc         staDissocReq;
        tMlmeDeauth         staDeauthReq;
        tApmeRsnStats       rsnStatsReq;
        tMlmePnacKeyInd     pnacKeyInd;
#ifdef PMF_WANTED
        tMlmeSAQuery        mlmeSaQueryInd;
#endif
    } unMlmeIndication;
    #define MLMEASSOCIND       unMlmeIndication.mlmeAssocInd
    #define MLMEREASSOCIND     unMlmeIndication.mlmeReassocInd
    #define MLMEDISSOCIND      unMlmeIndication.mlmeDissocInd
    #define MLMEDEAUTHIND      unMlmeIndication.mlmeDeauthInd
    #define MLMEMICFAILIND     unMlmeIndication.mlmeMICFailInd
    #define MLMESETKEYSREQ     unMlmeIndication.setKeysReq
    #define MLMEDELKEYSREQ     unMlmeIndication.delKeysReq
    #define MLMESETPROTREQ     unMlmeIndication.setProtReq
    #define MLMEDISSOCREQ      unMlmeIndication.staDissocReq
    #define MLMEDEAUTHREQ      unMlmeIndication.staDeauthReq
    #define APMERSNCONFIG      unMlmeIndication.rsnConfigReq
    #define APMERSNSTATS       unMlmeIndication.rsnStatsReq
    #define MLMEPNACKEYIND     unMlmeIndication.pnacKeyInd
#ifdef PMF_WANTED
    #define MLMESAQUERYIND     unMlmeIndication.mlmeSaQueryInd
#endif
} tWssRSNANotifyParams;



UINT4  RsnaProcessWssNotification PROTO  ((tWssRSNANotifyParams   *pWssRSNANotifyParams));

PUBLIC VOID RsnaTaskMain PROTO ((INT1 *));

/* Creates MemPools and Timer List for Rsna Module */
UINT4 RsnaInitModuleStart                       PROTO ((VOID));

/*Creates the protocol semaphore */
INT4 RsnaCreateSemaphore PROTO((VOID));

/* Allocates Memory to pRsnaPaePortEntry of PnacPaePortEntry 
   Intialises the Global GTK Sem 
   Initialises pRsnaPortEntry   */
UINT4 RsnaInitPort                         PROTO ((UINT2 u2Port));

/* Api to process the incoming EAPOL Key Frame */
UINT4 RsnaApiHandleInEapolKeyFrame         PROTO ((UINT1 *pu1Pkt,
                                                   UINT2 u2PktLen,
                                                   UINT2 u2PortNum,
                                                   UINT1 *pu1SrcAddr));

VOID  RsnaTmrProcessTimer                PROTO ((VOID));
UINT1 RsnaApiFsmStop                     PROTO ((VOID * pRsnaSessionNode));

UINT4 RsnaPnacTxFrame                    PROTO ((UINT1 *pu1KeyFrame,
                                                  UINT4  u4PktLen,
                                                  UINT2 u2PortNum));

UINT4
RsnaApiNotifyRsnaOfKeyAvailable   PROTO ((UINT2 u2Port,
                                         UINT1 *pu1SuppMacAddr,
                                         UINT1 *pu1Pmk));

#ifdef PMF_WANTED
UINT4 RsnaApiSAQueryInd PROTO ((tWssRSNANotifyParams * pWssRSNANotifyParams));

UINT4 RsnaApiSendSAQueryReq PROTO ((tMacAddr StaMacAddr , UINT4 u4BssIfIndex,UINT2 u2SessId));

UINT1 RsnaGetRsnIGtkKey PROTO ((UINT4 u4WlanProfileIfIndex,
                             UINT1 *pu1IGtkKey, UINT2 *pu2KeyLength, UINT1 *pu1KeyIndex));

UINT4 RsnaApiIsStaPMFCapable  PROTO ((tMacAddr StaMacAddr, UINT4 u4BssIfIndex));
#endif

UINT1 RsnaHandleBSSIfDeletion PROTO ((UINT4 u4BssIfIndex));

UINT1 RsnaGetRsnIEParams PROTO ((UINT4 u4WlanProfileIfIndex, tRsnaIEElements   *pRsnaIEElements));

UINT1 RsnaGetRsnSessionKey PROTO ((tMacAddr staMacAddr,
                                   UINT1 *pu1Key, UINT1 *pu1KeyLength, 
                                   UINT1 *pu1CipherIndex,
                                   UINT1 *pu1AkmSuiteIndex));

UINT1 RsnaGetRsnGtkKey PROTO ((UINT4 u4WlanProfileIfIndex,
                             UINT1 *pu1GtkKey, UINT2 *pu2KeyLength, UINT1 *pu1KeyIndex));

UINT1 RsnaHandleExistingStationAssocRequest PROTO ((tMacAddr staMacAddr , UINT4 u4BssIfIndex, BOOL1 *pbSessDelete));
UINT1 RsnaGetIsPtkInDoneState PROTO ((tMacAddr staMacAddr));
UINT1 RsnaDeletePmkCache  PROTO ((tMacAddr staMacAddr));

INT4 RsnaLock PROTO ((VOID));
INT4 RsnaUnLock PROTO ((VOID));

/* Proto type for GET_FIRST Routine.  */

extern INT1
RsnaGetFirstIndexDot11PrivacyTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
RsnaGetNextIndexDot11PrivacyTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */


extern INT1
RsnaGetDot11RSNAEnabled ARG_LIST((INT4 ,INT4 *));

extern INT1
RsnaGetDot11RSNAPreauthenticationEnabled ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */


extern INT1
RsnaSetDot11RSNAEnabled ARG_LIST((INT4  ,INT4 ));

extern INT1
RsnaSetDot11RSNAPreauthenticationEnabled ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */


extern INT1
RsnaTestv2Dot11RSNAEnabled ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
RsnaTestv2Dot11RSNAPreauthenticationEnabled ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
RsnaIsOpenAuthEnabled ARG_LIST((INT4));

/* Proto Validate Index Instance for Dot11RSNAConfigTable. */ 
extern INT1  
RsnaValidateIndexInstanceDot11RSNAConfigTable ARG_LIST((INT4));

/* Proto Type for Low Level GET FIRST fn for Dot11RSNAConfigTable  */

extern INT1
RsnaGetFirstIndexDot11RSNAConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
RsnaGetNextIndexDot11RSNAConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
RsnaGetDot11RSNAConfigVersion ARG_LIST((INT4 ,INT4 *));

extern INT1
RsnaGetDot11RSNAConfigPairwiseKeysSupported ARG_LIST((INT4 ,UINT4 *));

extern INT1
RsnaGetDot11RSNAConfigGroupCipher ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
RsnaGetDot11RSNAConfigGroupRekeyMethod ARG_LIST((INT4 ,INT4 *));

extern INT1
RsnaGetDot11RSNAConfigGroupRekeyTime ARG_LIST((INT4 ,UINT4 *));

extern INT1
RsnaGetDot11RSNAConfigGroupRekeyPackets ARG_LIST((INT4 ,UINT4 *));

extern INT1
RsnaGetDot11RSNAConfigGroupRekeyStrict ARG_LIST((INT4 ,INT4 *));

extern INT1
RsnaGetDot11RSNAConfigPSKValue ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
RsnaGetDot11RSNAConfigPSKPassPhrase ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
RsnaGetDot11RSNAConfigGroupUpdateCount ARG_LIST((INT4 ,UINT4 *));

extern INT1
RsnaGetDot11RSNAConfigPairwiseUpdateCount ARG_LIST((INT4 ,UINT4 *));

extern INT1
RsnaGetDot11RSNAConfigGroupCipherSize ARG_LIST((INT4 ,UINT4 *));

extern INT1
RsnaGetDot11RSNAConfigPMKLifetime ARG_LIST((INT4 ,UINT4 *));

extern INT1
RsnaGetDot11RSNAConfigPMKReauthThreshold ARG_LIST((INT4 ,UINT4 *));

extern INT1
RsnaGetDot11RSNAConfigNumberOfPTKSAReplayCounters ARG_LIST((INT4 ,INT4 *));

extern INT1
RsnaGetDot11RSNAConfigSATimeout ARG_LIST((INT4 ,UINT4 *));

extern INT1
RsnaGetDot11RSNAAuthenticationSuiteSelected ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
RsnaGetDot11RSNAPairwiseCipherSelected ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
RsnaGetDot11RSNAGroupCipherSelected ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
RsnaGetDot11RSNAPMKIDUsed ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
RsnaGetDot11RSNAAuthenticationSuiteRequested ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
RsnaGetDot11RSNAPairwiseCipherRequested ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
RsnaGetDot11RSNAGroupCipherRequested ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1 
RsnaGetDot11RSNATKIPCounterMeasuresInvoked(INT4 i4IfIndex ,                                                                   UINT4 *pu4RetValDot11RSNATKIPCounterMeasuresInvoked);

extern INT1 
RsnaGetDot11RSNA4WayHandshakeFailures(INT4 i4IfIndex ,
                 UINT4 *pu4RetValDot11RSNA4WayHandshakeFailures);


extern INT1
RsnaGetDot11RSNAConfigNumberOfGTKSAReplayCounters ARG_LIST((INT4 ,INT4 *));
#ifdef PMF_WANTED
extern INT1
RsnaGetDot11RSNAProtectedManagementFramesActivated ARG_LIST ((INT4 ,INT4 *));

extern INT1
RsnaGetDot11RSNAUnprotectedManagementFramesAllowed ARG_LIST ((INT4 , INT4 *));

extern INT1 
RsnaGetDot11AssociationSAQueryMaximumTimeout ARG_LIST ((INT4 , UINT4 *));

extern INT1 
RsnaGetDot11AssociationSAQueryRetryTimeout ARG_LIST ((INT4 , UINT4 *));
#endif


/* Low Level SET Routine for All Objects.  */

extern INT1
RsnaSetDot11RSNAConfigGroupCipher ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
RsnaSetDot11RSNAConfigGroupRekeyMethod ARG_LIST((INT4  ,INT4 ));

extern INT1
RsnaSetDot11RSNAConfigGroupRekeyTime ARG_LIST((INT4  ,UINT4 ));

extern INT1
RsnaSetDot11RSNAConfigGroupRekeyPackets ARG_LIST((INT4  ,UINT4 ));

extern INT1
RsnaSetDot11RSNAConfigGroupRekeyStrict ARG_LIST((INT4  ,INT4 ));

extern INT1
RsnaSetDot11RSNAConfigPSKValue ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
RsnaSetDot11RSNAConfigPSKPassPhrase ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
RsnaSetDot11RSNAConfigGroupUpdateCount ARG_LIST((INT4  ,UINT4 ));

extern INT1
RsnaSetDot11RSNAConfigPairwiseUpdateCount ARG_LIST((INT4  ,UINT4 ));

extern INT1
RsnaSetDot11RSNAConfigPMKLifetime ARG_LIST((INT4  ,UINT4 ));

extern INT1
RsnaSetDot11RSNAConfigPMKReauthThreshold ARG_LIST((INT4  ,UINT4 ));

extern INT1
RsnaSetDot11RSNAConfigSATimeout ARG_LIST((INT4  ,UINT4 ));

extern INT1
RsnaSetDot11RSNATKIPCounterMeasuresInvoked ARG_LIST((INT4  ,UINT4 ));

extern INT1
RsnaSetDot11RSNA4WayHandshakeFailures ARG_LIST((INT4  ,UINT4 ));

#ifdef PMF_WANTED
extern INT1
RsnaSetDot11RSNAProtectedManagementFramesActivated ARG_LIST ((INT4 , INT4 ));

extern INT1
RsnaSetDot11RSNAUnprotectedManagementFramesAllowed ARG_LIST ((INT4 , INT4 ));

extern INT1
RsnaSetDot11AssociationSAQueryMaximumTimeout ARG_LIST ((INT4 , UINT4 ));

extern INT1
RsnaSetDot11AssociationSAQueryRetryTimeout ARG_LIST ((INT4 , UINT4 ));
#endif



/* Low Level TEST Routines for.  */

extern INT1
RsnaTestv2Dot11RSNAConfigGroupCipher ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
RsnaTestv2Dot11RSNAConfigGroupRekeyMethod ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
RsnaTestv2Dot11RSNAConfigGroupRekeyTime ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

extern INT1
RsnaTestv2Dot11RSNAConfigGroupRekeyPackets ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

extern INT1
RsnaTestv2Dot11RSNAConfigGroupRekeyStrict ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
RsnaTestv2Dot11RSNAConfigPSKValue ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
RsnaTestv2Dot11RSNAConfigPSKPassPhrase ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
RsnaTestv2Dot11RSNAConfigGroupUpdateCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

extern INT1
RsnaTestv2Dot11RSNAConfigPairwiseUpdateCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

extern INT1
RsnaTestv2Dot11RSNAConfigPMKLifetime ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

extern INT1
RsnaTestv2Dot11RSNAConfigPMKReauthThreshold ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

extern INT1
RsnaTestv2Dot11RSNAConfigSATimeout ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

extern INT1
RsnaTestv2Dot11RSNATKIPCounterMeasuresInvoked ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

extern INT1
RsnaTestv2Dot11RSNA4WayHandshakeFailures ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

#ifdef PMF_WANTED
extern INT1
RsnaTestv2Dot11RSNAProtectedManagementFramesActivated  ARG_LIST ((UINT4 *, INT4, INT4));

extern INT1
RsnaTestv2Dot11RSNAUnprotectedManagementFramesAllowed ARG_LIST ((UINT4 *, INT4, INT4));

extern INT1 
RsnaTestv2Dot11AssociationSAQueryMaximumTimeout ARG_LIST ((UINT4 *, INT4, UINT4));

extern INT1
RsnaTestv2Dot11AssociationSAQueryRetryTimeout ARG_LIST ((UINT4 *, INT4, UINT4));
#endif

/* Low Level DEP Routines for.  */

INT1
RsnaDepv2Dot11RSNAConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11RSNAConfigPairwiseCiphersTable. */
INT1
RsnaValidateIndexInstanceDot11RSNAConfigPairwiseCiphersTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11RSNAConfigPairwiseCiphersTable  */

INT1
RsnaGetFirstIndexDot11RSNAConfigPairwiseCiphersTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
RsnaGetNextIndexDot11RSNAConfigPairwiseCiphersTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */
INT1
RsnaGetDot11RSNAConfigPairwiseCipher ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
RsnaGetDot11RSNAConfigPairwiseCipherEnabled ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
RsnaGetDot11RSNAConfigPairwiseCipherSize ARG_LIST((INT4  , UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
RsnaSetDot11RSNAConfigPairwiseCipherEnabled ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
RsnaTestv2Dot11RSNAConfigPairwiseCipherEnabled ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
RsnaDepv2Dot11RSNAConfigPairwiseCiphersTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11RSNAConfigAuthenticationSuitesTable. */
INT1
RsnaValidateIndexInstanceDot11RSNAConfigAuthenticationSuitesTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11RSNAConfigAuthenticationSuitesTable  */

INT1
RsnaGetFirstIndexDot11RSNAConfigAuthenticationSuitesTable ARG_LIST((INT4 *, UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
RsnaGetNextIndexDot11RSNAConfigAuthenticationSuitesTable ARG_LIST((INT4, INT4 *, UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
RsnaGetDot11RSNAConfigAuthenticationSuite ARG_LIST((UINT4, UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
RsnaGetDot11RSNAConfigAuthenticationSuiteEnabled ARG_LIST((INT4, UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
RsnaSetDot11RSNAConfigAuthenticationSuiteEnabled ARG_LIST((UINT4, UINT4  ,INT4));

INT1
RsnaValidateIndexInstanceFsRSNAConfigAuthenticationSuitesTable ARG_LIST ((INT4,
                                                                      UINT4));

/* Low Level TEST Routines for.  */

INT1
RsnaTestv2Dot11RSNAConfigAuthenticationSuiteEnabled ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));


/* Low Level DEP Routines for.  */

INT1
RsnaDepv2Dot11RSNAConfigAuthenticationSuitesTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11RSNAStatsTable. */
INT1
RsnaValidateIndexInstanceDot11RSNAStatsTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11RSNAStatsTable  */

INT1
RsnaGetFirstIndexDot11RSNAStatsTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
RsnaGetNextIndexDot11RSNAStatsTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
RsnaGetDot11RSNAStatsSTAAddress ARG_LIST((INT4  , UINT4 ,tMacAddr * ));

INT1
RsnaGetDot11RSNAStatsVersion ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
RsnaGetDot11RSNAStatsSelectedPairwiseCipher ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
RsnaGetDot11RSNAStatsTKIPICVErrors ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
RsnaGetDot11RSNAStatsTKIPLocalMICFailures ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
RsnaGetDot11RSNAStatsTKIPRemoteMICFailures ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
RsnaGetDot11RSNAStatsCCMPReplays ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
RsnaGetDot11RSNAStatsCCMPDecryptErrors ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
RsnaGetDot11RSNAStatsTKIPReplays ARG_LIST((INT4  , UINT4 ,UINT4 *));

UINT4
RsnaEapolRtyDeleteEntry ARG_LIST((tMacAddr srcMacAddr));

UINT1
WpaGetRsnIEParams ARG_LIST ((UINT4 u4WlanProfileIfIndex,
                    tWpaIEElements * ));
UINT1
WpaGetFsRSNAEnabled ARG_LIST ((UINT4 u4IfIndex,INT4 *pi4FsRSNAEnabled));
#endif

