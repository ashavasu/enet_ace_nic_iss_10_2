/*******************************************************************************
 * Copyright (C) Future Software Limited, 1997-98, 2001
 *
 * $Id: isis.h,v 1.44 2017/12/26 13:34:20 siva Exp $
 *
 * Description: This file contains the exported definitions of ISIS module
 *
 ******************************************************************************/

#ifndef __ISIS_H__
#define __ISIS_H__


typedef tOsixMsg   tIsisQBuf; 
                              /* Current implementation assumes CRU buffer 
                               * chains for enqueueing and dequeueing 
                               * messages from message queues. This type should
                               * be appropriately modified based on the type of
                               * buffers used for the aforementioned purpose
                               */ 
/* Status codes
 */

#define ISIS_SUCCESS   0
#define ISIS_FAILURE  -1
#define ISIS_MEM_FAILURE 3

#define ISIS_ENABLE  2
#define ISIS_DISABLE  1
#define ISIS_RTM6_ROUTE_PROCESS_EVENT 0x400

/* Prototypes of exported functions
 */

PUBLIC INT4 IsisDlliIfStatusIndication (UINT2 u2LLIfIndex, UINT1 u1Status);

VOID IsisTaskMain (INT1 *);

PUBLIC VOID IsisGrShutDownProcess PROTO ((VOID));

#ifdef MBSM_WANTED

VOID IsisMbsmUpdateLCStatus PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1Cmd));
INT4 IsisMbsmUpdateCardStatus PROTO ((tMbsmProtoMsg * pProtoMsg, UINT1 u1Cmd));
#endif


/* In the case of running ISISover linux RAW sockets, IsisDlliRecv () routine
 * reads the sockets and triggers the ISIS module based on the received data.
 * Hence it does not require any arguments. But for any other generic DLL driver
 * it takes the buffer and IfIndex as arguments. Hence 2 different prototypes
 */

#ifdef __ISLINUX__
PUBLIC void IsisDlliRecv (void);
#else
PUBLIC INT4 IsisDlliRecv (tIsisQBuf *, UINT2);
#endif
PUBLIC INT4 IsisSendHostNmeUpdateMsg (UINT1 *);

/* Protocol Types 
 */

#define ISIS_IPV4                      0x01   /* IPV4 */ 
#define ISIS_IPV6                      0x02   /* IPV6 */ 
#define ISIS_MAX_CONTEXT_STR_LEN   VCM_ALIAS_MAX_LEN                           
/* Row Status values as per SNMP. 
 */

#define ISIS_ACTIVE                    0x01
#define ISIS_NOT_IN_SER                0x02
#define ISIS_NOT_READY                 0x03
#define ISIS_CR_GO                     0x04
#define ISIS_CR_WT                     0x05
#define ISIS_DESTROY                   0x06
/* Lengths of different Addresses supported */
#define ISIS_SYS_ID_LEN        0x06 
                              /* Modify it to suit the requirement
                               */
#define ISIS_SYS_ID_PDU_LEN     20 
                              /* Modify it to suit the requirement
                               */


#define ISIS_AREA_ADDR_LEN     0x0E 
                              /* Modify to suit the target requirement
                               */
#define  ISIS_MAX_IPV4_ADDR_LEN  0x4
#define ISIS_MAX_IPV6_ADDR_LEN   0x10
#define ISIS_MAX_IPV6_ADDRESSES   5
#define ISIS_MAX_MT    2

#define ISIS_MAX_IP_ADDR_LEN   0x10 
                              /* Modify it appropriately if both IPV4 and
                               * IPV6 are supported
                               */

#define ISIS_TRUE                      0x01
#define ISIS_FALSE                     0x02
#define ISIS_DEF_RE_ADD                0x03
#define ISIS_PNODE_ID_LEN              0x01
                                     /* Pseudo Node ID Length 
                                      */
#define ISIS_SNPA_ADDR_LEN     0x06
/* Intermediate System Types and Circuit Levels
 */

#define ISIS_INTER_AREA                3 

#define ISIS_LEVEL1                    0x01 /* Level1 IS */
#define ISIS_LEVEL2                    0x02 /* Level2 IS */
#define ISIS_LEVEL12                   0x03 /* Level12 IS */
#define ISIS_UNKNOWN                   0x04 /* Unknown Type */
#define ISIS_IS                        0x05 /* Intermediate System, specific 
                                             * Type unknown. Used in ISH
                                             * processing
                                             */ 

/* IS Type in LSP holds the following values
 * 0 - Unused value
 * 1 - (i.e. bit 1 set) Level 1 Intermediate system
 * 2 - Unused value
 * 3 - (i.e. bits 1 and 2 set) Level 2 Intermediate system.
 *
 * It will never carry the value for L12 system as defined in standard*/

#define IS_TYPE_LEVEL2                   0x03
#define IS_TYPE_LEVEL1                   0x01

#define ISIS_MAX_PASSWORD_LEN            80
                                     /* Size of the Authentication key.*/

/* For instance  specific Protocol Supported.*/ 
#define ISIS_IPV4_SUPP                      204  /* Value for IPV4 Support
                                                  */
#define ISIS_IPV6_SUPP                      142  /* Value for IPV6 Support
                                                Network Layer Protocol ID (NLPID)*/
#define ISIS_IPV6_PROTO_SUPP                142  
                                            
/* Used only in WEB - issweb.c */
#define ISIS_IPV4_IPV6_SUPP                 2 
#define ISIS_NO_IPV4_IPV6_SUPP              0
/* ISIS Protocol States
 */

#define ISIS_STATE_OFF                 0x01
                                     /* No more exists
                                      */ 
#define ISIS_STATE_ON                  0x02
                                     /* Status ACTIVE and Kicking
                                      */ 
#define ISIS_STATE_WAITING             0x03
                                     /* LSP Database overloaded state
                                      */ 
#define ISIS_SEC_IP_REMOVED            0x04

#define ISIS_SEC_IP_ADDED             0x05

/* Summary Address Table Definitions
 */

#define ISIS_SUMM_ADMIN_L1             0x01
                                      /* Summary Address Admin State - L1
                                       */
#define ISIS_SUMM_ADMIN_L2             0x02
                                      /* Summary Address Admin State - L2
                                       */
#define ISIS_SUMM_ADMIN_L12            0x03
                                      /* Summary Address Admin State - L12
                                       */
#define ISIS_SUMM_ADMIN_OFF            0x04
                                      /* Summary Address Admin State - OFF
                                       */
#define ISIS_SUMM_DEF_MET              0x14
                                      /* Summary Address Default Metric Value
                                       */ 

#define ISIS_IPV4_ADDR_LEN             0x04
                                     /* Length of the IPV4 Address
                                      */
#define ISIS_IPV6_ADDR_LEN             0x10
                                     /* Length of the IPV6 Address
                                      */
#define ISIS_TRUE                      0x01
#define ISIS_FALSE                     0x02

#define ISIS_IP6_ADDR_ACCESS_IN_SHORT_INT   8



/* The Compliance format of the NETID used */

#define ISIS_DOT_TRUE                  1  /* DOT Compliant NET ID (.) */
#define ISIS_DOT_FALSE                 0  /* COLON Compliant NET ID (:) */
#define ISIS_MAX_LSPID                 8  /* Max array length for LSP-ID */

/* FSAP defines UINT1 and INT1 to be unsigned and signed char respectively. 
 * Most of the system calls however expect only a 'char' type argument. 
 * To avoid warnings the folowing type is explicitly defined
 */
#define CHAR                          char

/* Macro to copy the SysID, IP Address or the SNPA Address to the String for
 * the Event Logging to the Log Table
 */

#define ISIS_FORM_STR(StrLen, au1Src, au1Dest) \
{\
   UINT1 u1Cnt;\
    UINT1 u1Pos;\
    UINT1 u1DestSize = (UINT1) sizeof(au1Dest);\
   for (u1Cnt = 0, u1Pos = 0; ((u1Cnt < (StrLen -1)) && ((u1Pos + 3) < u1DestSize));\
   u1Cnt++, u1Pos = (UINT1) (u1Pos + 3))\
   {\
        SPRINTF(au1Dest + u1Pos, "%02x:", au1Src [u1Cnt]);\
   }\
        SPRINTF(au1Dest + u1Pos, "%02x", au1Src [u1Cnt]);\
}

#define ISIS_IN6_IS_ADDR_V4COMPATIBLE(pAddr, Status) \
{\
    tIp6Addr    Ipv6Addr; \
    MEMCPY((Ipv6Addr.u1_addr), pAddr, ISIS_MAX_IPV6_ADDR_LEN); \
    if (IS_ADDR_V4_COMPAT (Ipv6Addr))  { Status = ISIS_TRUE; } \
        else if (IN6_IS_ADDR_V4MAPPED (Ipv6Addr))  { Status = ISIS_TRUE; } \
            else { Status = ISIS_FALSE; } \
}

#define ISIS_FORM_MTID(acMtid, u1IfMTId)\
{\
 INT1 i1temp;\
 INT1 i1length = 8*(UINT1)sizeof(u1IfMTId);\
 for (i1temp = 0; i1temp < i1length; i1temp++)\
        {\
  if (u1IfMTId & (1 << i1temp))\
   SPRINTF (acMtid+STRLEN(acMtid),"%d,", i1temp);\
        }\
 acMtid [STRLEN(acMtid)-1] = '\0';\
}
#define ISIS_FORM_HSTNMEID(au1HstNmeId,pPDU,u1ApdFrgID, u1ApdNodeId)\
{\
 UINT1 u1temp = 0;\
 if (u1ApdNodeId)\
 {\
     STRCAT (au1HstNmeId,".");\
     u1temp = ((UINT1) *(pPDU + ISIS_SYS_ID_LEN));\
     SPRINTF((CHR1 *)(au1HstNmeId + STRLEN (au1HstNmeId)), "%02x", u1temp);\
 }\
 if (u1ApdFrgID)\
 {\
     STRCAT (au1HstNmeId,"-");\
     u1temp = ((UINT1) *(pPDU + ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN));\
     SPRINTF((CHR1 *)(au1HstNmeId + STRLEN (au1HstNmeId)), "%02x", u1temp);\
 }\
}

#define ISIS_FORM_IPV4_ADDR(pu1Src, pu1Dest, u1AddrLen)\
{\
    INT1                u1Cnt = 0;\
    INT1                u1Inc = 0;\
    UINT1               u1Dot = '.';\
    UINT1               u1Hn = 0;\
    UINT1               u1Tn = 0;\
    UINT1               u1On = 0;\
    while(u1Inc < u1AddrLen)\
    {\
            u1Hn=(pu1Src[u1Inc] / 100);\
            u1Tn=((pu1Src[u1Inc] % 100) / 10);\
            u1On=(pu1Src[u1Inc] % 10);\
            if(u1Hn)\
            {\
                SPRINTF((CHAR *)&pu1Dest[u1Cnt], "%c",(u1Hn + 0x30));\
                u1Cnt++;\
            }\
            if ((u1Tn)||(u1Hn))\
            {\
                SPRINTF((CHAR *)&pu1Dest[u1Cnt], "%c",(u1Tn + 0x30));\
                u1Cnt++;\
            }\
            SPRINTF((CHAR *)&pu1Dest[u1Cnt], "%c",(u1On + 0x30));\
            u1Cnt++;\
            SPRINTF((CHAR *)&pu1Dest[u1Cnt], "%c", u1Dot );\
            u1Cnt++;\
            u1Inc++;\
    }\
    pu1Dest[u1Cnt-1] = '\0';\
}

/*ISIS Graceful restart*/

#define ISIS_MODULE 8

typedef struct _IsisGrCxtInfo { 
     UINT4  u4IsisCxtId;
     UINT4  u4GrEndTime;
     UINT1  u1RestartStatus;
     UINT1  u1RestartReason;
     UINT1  u1ExitReason;
     UINT1  u1Padding;
}tIsisGrCxtInfo;

typedef struct _IsisGrInfo {
     UINT4  u4CxtCount; 
     tIsisGrCxtInfo **pCxtinfo;
} tIsisGrInfo;

PUBLIC tIsisGrCxtInfo ** IsisUtlAllocMemGRInfoList PROTO ((VOID));
PUBLIC tIsisGrCxtInfo * IsisUtlAllocMemGRInfo PROTO ((VOID ));
/*Restart Status*/
#define ISIS_RESTART_NONE  1 /*Acts as a normal router (GR not intiated/complete)*/
#define ISIS_RESTARTING_ROUTER  2/*Restarting router goes down with a exit reason (planned)*/
#define ISIS_STARTING_ROUTER  3 /*Starting router since restart reason is unknown (unplanned)*/


/*Restart Reason*/
#define ISIS_GR_UNKNOWN         1
#define ISIS_GR_SW_RESTART      2
#define ISIS_GR_SW_UPGRADE      3
#define ISIS_GR_SW_RED          4


/*Restarter exit reason*/
#define ISIS_GR_RESTART_NONE            1
#define ISIS_GR_RESTART_INPROGRESS      2
#define ISIS_GR_RESTART_COMPLETED       3
#define ISIS_GR_RESTART_TIMEDOUT        4
#define ISIS_GR_RESTART_TOP_CHG         5

#define ISIS_GR_CONF              (const CHR1 *)"isisgr.txt"

#define  ISIS_IMPORT_DIRECT       RTM_DIRECT_MASK
#define  ISIS_IMPORT_STATIC       RTM_STATIC_MASK
#define  ISIS_IMPORT_RIP          RTM_RIP_MASK
#define  ISIS_IMPORT_OSPF         RTM_OSPF_MASK
#define  ISIS_IMPORT_BGP          RTM_BGP_MASK
#define  ISIS_IMPORT_ALL          0x3086


#define ISIS_IMPORT_LEVEL_1   1
#define ISIS_IMPORT_LEVEL_2   2
#define ISIS_IMPORT_LEVEL_12   3

#define ISIS_AUTH_PASSWD   0x01
#define ISIS_AUTH_MD5      0x02

#endif  /* __ISIS_H__ */
