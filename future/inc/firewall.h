/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: firewall.h,v 1.42 2016/03/19 13:06:26 siva Exp $
 *
 * Description:This file contains the exported definitions and  
 *             macros of FIREWALL                              
 *
 *******************************************************************/
#ifndef _FIREWALL_H
#define _FIREWALL_H
#include "iss.h"
#include "cust.h"
#include "nat.h" 
#ifndef FWL_TIMER_EXPIRY_EVENT
#define   FWL_TIMER_EXPIRY_EVENT    0x00000010
#endif

/* ACL routine's return codes */
#define   FWL_SUCCESS    0
#define   FWL_FAILURE    1
#define   FWL_DMZ_NOT_FOUND 3
#define   FWL_GLOBAL_IDX  0

#define FWL_APP_PERSISTENT     3
#define FWL_PERSISTENT         4
#define FWL_NON_PERSISTENT     5

/* --------------- Definitions related to Firewall Locks --------------- */
#ifdef  FIREWALL_GLOBAL
#define tFwlLock tOsixSemId
tFwlLock  FwlLck;
#else
#define tFwlLock tOsixSemId
extern tFwlLock  FwlLck;
#endif

PUBLIC INT4 FwlLock PROTO ((VOID));
PUBLIC INT4 FwlUnLock PROTO ((VOID));

/*****************************************************************************/
/*      Constants for the status of the Filtering service.                   */
/*****************************************************************************/

#define  FWL_ENABLE                                    1
#define  FWL_DISABLE                                   2
#define  FWL_FILTERING_ENABLED                         1
#define  FWL_FILTERING_DISABLED                        2

#define FWL_SW_ATTACK_CHECKS  0x00000001
#define FWL_SW_IPHDR_VALIDATION     0x00000002

/*DOS attack*/
#define FWL_DOS_ATTACK_REDIRECT 1
#define FWL_DOS_ATTACK_SMURF    2
#define FWL_DOS_LAND_ATTACK     3
#define FWL_DOS_SHORT_HEADER    4
#define FWL_DOS_REVERSE_PATH    5
#define FWL_DOS_SNORK_ATTACK    6

/*DOS system command length*/
#define FWL_DOS_LINE_LEN       200
/*DOS attack types*/


/*Rate-Limit*/
#define FWL_RATE_LIMIT_TCP     1
#define FWL_RATE_LIMIT_UDP     2
#define FWL_RATE_LIMIT_ICMP    3

/*Traffic Mode*/
#define FWL_RATE_LIMIT_PPS     1
#define FWL_RATE_LIMIT_KBPS    2
#define FWL_RATE_LIMIT_BPS     3

#define FWL_MIN_RATE_LIMIT_PPS  1 
#define FWL_MAX_RATE_LIMIT_PPS  10000
#define FWL_MIN_RATE_LIMIT_KBPS 100 
#define FWL_MAX_RATE_LIMIT_KBPS 1000099
#define FWL_MIN_RATE_LIMIT_BPS  102400 
#define FWL_MAX_RATE_LIMIT_BPS  80000000


#define MAX_COLUMN_LENGTH      120
#define FWL_MAX_PORT_NAME_LENGTH    24


#define ISS_NVRAM_FILE        FLASH "issnvram.txt"
#ifdef MBSM_WANTED
#define ISS_DEFAULT_INTERFACE_NAME    ((const char *) "eth0")
#else
#define ISS_DEFAULT_INTERFACE_NAME    ((const char *) "Slot0/1")
#endif

/*IPtables realted macro*/
#define FWL_IPTBL_ADD            "iptables -A "
#define FWL_IPTBL_DEL            "iptables -D "
#define FWL_IPTBL_DROP           "-j DROP"


/*RPF enable/disable*/

#define FWL_RPF_DISABLE         0
#define FWL_RPF_ENABLE          1


#define FWL_PROC_CONF           "/proc/sys/net/ipv4/conf"
#define FWL_PROC_RP_FILTER       "rp_filter"
#define FWL_PROC_REDIRECT        "/proc/sys/net/ipv4/conf/all/accept_redirects"
#define FWL_PROC_SMURF           "/proc/sys/net/ipv4/icmp_echo_ignore_broadcasts"


#define FWL_DEF_TCP_PORT     1
#define FWL_DEF_UDP_PORT     2
#define FWL_DEF_ICMP_PORT    3

#define MAX_PKT_SIZE   100
#define FWL_TCP_MIN_HDR  8

#define FWL_RATE_LIMIT_MAX_PORT  1000

#define FWL_DEF_SHORT_HEADER    10
/* 
 * Below FEATURE_MASK is to be defined based on the hardware support. If 
 * attacks and header validation is done in hardware itself, then to avoid 
 * redundent check in software disable by setting "0".
 * Note: Only few attacks and basic header validation are done for IPv6.
 */
#define FWL_SW_V4_FEATURE_MASK  (0)
#define FWL_SW_V6_FEATURE_MASK  (0)

PUBLIC UINT1  gu1FirewallStatus;

#define tIp4Addr    UINT4

/* using union of UINT4 and tIp6Addr instead of tIpVxAddr, as tIPvXAddr would
 * involve memcpy, memcmp which will reduce the performance for IPv4 */
typedef struct {
    union {
    tIp4Addr    Ip4Addr;
    tIp6Addr    Ip6Addr;
    } uIpAddr;
    UINT4       u4AddrType;
#define v4Addr   uIpAddr.Ip4Addr
#define v6Addr   uIpAddr.Ip6Addr
} tFwlIpAddr;

typedef struct 
{
     tFwlIpAddr LocalIP;
     tFwlIpAddr RemoteIP;
     UINT2  u2LocalPort;
     UINT2  u2RemotePort;
     UINT2  u2InterfaceNum;
     UINT1  u1Protocol;
     UINT1  u1Direction;
     UINT1  u1PersistFlag; 
     UINT1  u1AppCallStatus;
     UINT1  au1Pad[2];
} tPartialInfo;

typedef struct _FwlAclSchedule
{                    
   UINT1 u1StartTimeHrs;
   UINT1 u1StartTimeMins; 
   UINT1 u1EndTimeHrs;
   UINT1 u1EndTimeMins;

   UINT1 u1WholeDayFlag;      /* FWL_ENABLE / FWL_DISABLE              */
   UINT1 u1WeekDay;           /* EVERY_DAY/MON/TUE/WED/THU/FRI/SAT/SUN */
   UINT1 u1ApplyScheduleFlag; /* FWL_ENABLE / FWL_DISABLE              */
   UINT1 u1AclScheduleStatus; /* FWL_ACTIVE / FWL_NOT_IN_SERVICE       */
} tFwlAclSchedule;

/*****************************************************************************/
/*   Global Firewall Rate limiting  stucture                                 */
/*****************************************************************************/

typedef struct {
              INT4 i4PortNo;
              INT4 i4ProtoType;
              INT4 i4RateLimit;
              INT4 i4BurstSize;
              INT4 i4TrafficMode;
              INT4 i4RowStatus;

} tFwlRateLimit;

/*****************************************************************************/
/*   Global Firewall Snork  stucture                                 */
/*****************************************************************************/

typedef struct {
              tTMO_SLL_NODE  nextSnorkInfo;
              INT4 i4PortNo;
              INT4 i4RowStatus;

} tFwlSnorkInfo;
/*****************************************************************************/
/*   Global Firewall Rpf Entry  stucture                                 */
/*****************************************************************************/

typedef struct {
              tTMO_SLL_NODE  nextRpfEntryInfo;
              INT4 i4IfIndex;
              INT4 i4RpfMode;
              INT4 i4RowStatus;

} tFwlRpfEntryInfo;




UINT4 
FwlUpdatePartialLink PROTO((tPartialInfo *, tPartialInfo *));

UINT4
FwlDeletePartialLink PROTO((tPartialInfo *));

/* Macro for Add Partial Link Function */
#define FwlAddPartialLink(x)  FwlUpdatePartialLink( (x),(NULL) )


PUBLIC UINT4 FwlAclInit                   PROTO((VOID));
PUBLIC VOID  FwlUpdateWanInfoOnPhysicalIf PROTO((UINT4,UINT4));

PUBLIC VOID FwlLogMessage          PROTO((UINT4 u4AttackType,
                                          UINT4 u4IfaceNum,
                                          tCRU_BUF_CHAIN_HEADER * pBuf,
                                          UINT4 u4LogLevel, 
                                          UINT4 u4Severioty,
                                          UINT1 * pMsg));
PUBLIC VOID FwlLogInvalidIPPkt     PROTO((tCRU_BUF_CHAIN_HEADER *pBuf));
PUBLIC VOID FwlLogRawIP            PROTO((tCRU_BUF_CHAIN_HEADER *pBuf));
PUBLIC VOID FwlCommitAcl PROTO((VOID));
PUBLIC INT1 FwlHandleInterfaceIndication (UINT4 u4Interface, UINT4 u4Status);
PUBLIC UINT1* FwlUtilShowLogBuffer (VOID);
PUBLIC UINT4 FwlAclProcessPktForInFiltering PROTO((tCRU_BUF_CHAIN_HEADER *pBuf,
                                            UINT4 u4IfIndex));
PUBLIC UINT4 FwlAclProcessPktForOutFiltering PROTO((tCRU_BUF_CHAIN_HEADER *pBuf,
                                             UINT4 u4IfIndex));
PUBLIC VOID FwlTmrHandleTmrExpiry PROTO((VOID));
PUBLIC INT4 FwlHandleBlackListInfoFromIDS PROTO ((UINT1 *pu1Buf)); 

/****************************************************************************/
/*   constants related to Types of attacks                                  */
/****************************************************************************/

enum {
FWL_PERMITTED = 0,
FWL_STATIC_FILTER,              
FWL_INVALID_SRC_IP,
FWL_INVALID_DST_IP,
/* Dos attacks begin here */
FWL_DOS_ATTACKS_START_HERE,
FWL_IP_SPOOF_ATTACK,  
FWL_WIN_NUKE_ATTACK,           
FWL_TINY_FRAGMENT_ATTACK,
FWL_TCP_SYN_FLOODING,      
FWL_UDP_FLOODING,      
FWL_ICMP_FLOODING,      
FWL_LAND_ATTACK,          
FWL_SMURF_ATTACK,               
FWL_BCAST_STORM_ATTACK,
FWL_WINNUKE_ATTACK,
FWL_IRDP_ADVT,
FWL_SNORK_ATTACK,
FWL_ASCEND_ATTACK,
FWL_FRAGGLE_ATTACK,
FWL_UDP_SHTHDR,
FWL_UDP_LEN_FIELD_INVALID,
FWL_ICMP_SHTHDR,
FWL_W2KDC_ATTACK,
FWL_TCP_SHTHDR,
FWL_ZERO_LEN_IP_OPTION,
FWL_INVALID_IP_OPTIONS,
FWL_INVALID_URGENT_OFFSET,
FWL_UNALIGNED_TIMESTAMP,
FWL_UDP_LOOPBACK_ATTACK,
FWL_LARGE_FRAGMENT_ATTACK,
FWL_SESSION_LIMIT_ATTACK,
FWL_INVALID_NH,
FWL_INVALID_HOPLIMIT,

/* TCP Abnormal Flag attacks start here */
FWL_TCP_ABNORMAL_FLAGS_START_HERE,
FWL_TCP_SYN_FIN_ATTACK,
FWL_TCP_NON_SYN_ONLY_PKT,
FWL_TCP_XMAS_ATTACK,
FWL_TCP_NULL_ATTACK,
FWL_TCP_POST_SYN_ATTACK,
FWL_TCP_SYN_WITH_DATA,
/* Firewall options configured */   
FWL_OPTIONS_START_HERE,
FWL_IP_OPTION,          
FWL_SOURCE_ROUTE_ATTACK,
FWL_RECORD_ROUTE_ATTACK,       
FWL_TIMESTAMP_ATTACK,       
FWL_TRACE_ROUTE_ATTACK,
FWL_UNKNOWN_IP,
FWL_TCP_INVALID_SEQ_ACK,
FWL_TCP_PKT_INVALID_4_STATE,
FWL_ZERO_LEN_IP_DATA,
FWL_ICMP_INSPECT_ON,
FWL_ICMP_TIMESTAMP_REQ_ATTACK   ,    
FWL_ICMP_TIMESTAMP_REPLY_ATTACK,
FWL_ICMP_MASK_REQ_ATTACK,
FWL_ICMP_MASK_REPLY_ATTACK, 
FWL_DMZ_DROP_OUTBOUND_SESSION,
/* IP Header and Port Number will not be printed beyond this */

FWL_WEB_LOGIN_FAILED,
FWL_WEB_LOGIN_SUCCESS,
FWL_CLI_LOGIN_FAILED,
FWL_CLI_LOGIN_SUCCESS,
FWL_REMOTE_TELNET,
FWL_REMOTE_WEB_MGMT,
FWL_REMOTE_SNMP,
FWL_ROUTER_STARTUP, 
FWL_COMM_CONF,
FWL_ICMP_CODE,
FWL_URL_FIL,
FWL_NETBIOS_FIL,
FWL_REMOTE_SSH,
FWL_REMOTE_SSH_FAIL,
FWL_TELNET_SUCCESS, 
FWL_MEMORY_FAILURE, 
FWL_LOG_SYSTEM_STARTUP,   /* ICSA V14 */
FWL_INVALID_IP_VERSION,
FWL_IPHDR_LEN_FIELDS_INVALID,
FWL_MALFORMED_IP_PKT,
/* ADD new Log IDs above FWL_NULL */

FWL_CLI_LOGOUT,
FWL_REMOTE_SSH_LOGOUT,
FWL_FWL_MOD_STATUS_CHECK_FAIL,
FWL_WAN_INTF_STATUS_CHECK_FAIL,
FWL_BLACK_WHITE_LIST_IP_PKT,
FWL_MSG_NULL,
FWL_NUM_ATTACKS = FWL_MSG_NULL + 1

};



/* Definition of Log level (1-7) with 1 being the higest level */
#define FWLLOG_ALERT_LEVEL         1   /* Used for logging messages that
                                        * require immediate attention.
                                        */
#define FWLLOG_CRITICAL_LEVEL      2   /* Used for logging critical errors */
#define FWLLOG_ERROR_LEVEL         3   /* Used for error messages */
#define FWLLOG_WARN_LEVEL          4   /* Used for logging warning messages */
#define FWLLOG_NOTICE_LEVEL        5   /* Used for logging messages that
                                        *  require attention but are not errors
                                        */
#define FWLLOG_INFO_LEVEL          6   /* Used for logging informational
                                        *  messages
                                        */
#define FWLLOG_DEBUG_LEVEL         7   /* Used for logging debug messages */

#define  FWL_NUM_SEVERITY          8 

/* Log Messages */
#define FWL_LOG_NONE                   0 
#define FWL_LOG_BRF                    1
#define FWL_LOG_DTL                    2
#define FWL_LOG_MUST                   3 

#define FWL_DYNAMIC_PRIORITY_AVAILABLE     1
#define FWL_DYNAMIC_PRIORITY_NOT_AVAILABLE 2
#define FWL_DYNAMIC_MIN_RESV_PRIORITY      1500


/* ICSA V08, V09, V11 & V12:
 * Log packets from/to router interfaces */
#define FWL_MSG_TRUST_LAN_FROM "Packet from Trusted Network"
#define FWL_MSG_TRUST_LAN_TO   "Packet to Trusted Network"


#define FWL_MSG_RIP_PKT  "RIP packet"
#define FWL_MSG_OSPF_PKT  "OSPF packet"
#define FWL_MSG_IGMP_PKT  "IGMP packet"
#define FWL_MSG_SYN_SET  "Syn Bit is Set"
#define FWL_MSG_DROP     "No Rule to allow" 
#define FWL_MSG_ATTACK   "Attack Prevented"          
#define FWL_DMZ_MSG_ATTACK   "Attack Prevented on DMZ"          
#define FWL_MSG_MATCH    "Access List Matched"
#define FWL_MSG_ACL_DENY "Access List Denied"
#define FWL_MSG_NOMATCH  "Access List not Matched"
#define FWL_MSG_INVALID_USER  "Incorrect User"
#define FWL_MSG_AUTHFAIL "Incorrect Password"
#define FWL_MSG_PKT_TOO_BIG "Packet size too big"
#define FWL_MSG_OPT       "Packet with option set are permitted"
#define FWL_MSG_URL_FILTER "Requested URL Filtered"
#define FWL_MSG_NETBIOS_FILTER "NetBios Filtering is set.Packet dropped"
#define FWL_MSG_MALFORMED_PKT "Malformed Packet"
#define FWL_MSG_STATIC_MATCH "Respond Ping ON. Packet Allowed"
#define FWL_MSG_ICMP_ERR_MATCH       "ICMP Err Msg For Packet Sent Earlier"
#define FWL_MSG_ICMP_ERR_NO_MATCH    "ICMP Err Msg For Packet not Sent Earlier"
#define FWL_MSG_STATEFUL_TABLE_FULL    "Stateful Session Table full"
#define FWL_MSG_IN_TOTAL_SESSIONS_FULL "Total Inbound Sessions full"
#define FWL_MSG_IN_USER_SESSION_FULL   "Inbound User Sessions full"
#define FWL_MSG_OUT_USER_SESSION_FULL  "Outbound User Sessions full"
#define FWL_MSG_IN_TOTAL_UDP_SESSIONS_FULL "Total Inbound UDP Sessions full"
#define FWL_MSG_IN_TOTAL_ICMP_SESSIONS_FULL "Total Inbound ICMP Sessions full"
#define FWL_MSG_POSSIBLE_POST_SYN_ATTACK FWL_MSG_MATCH " (possible Post Syn Attack)"
#define FWL_DHCP_PASS_THROUGH "DHCP Traffic Passthrough"
#define FWL_MSG_INVALID_TCP_PKT "Invalid TCP Packet"
#define FWL_MSG_FWL_MOD_STATUS_CHECK_FAIL \
"Enable the firewall before turning any WAN Interface up"
#define FWL_MSG_WAN_INTF_STATUS_CHECK_FAIL   \
"Turn all the WAN Interface down before disabling the firewall status"
#define FWL_MSG_BLACK_LIST_IP_ADDRESS "Packet is matched with BlackList entry"
#define FWL_MSG_WHITE_LIST_IP_ADDRESS "Packet is matched with WhiteList entry"


#define MAX_DUMP_LEN     96

/* Extern Prototypes */
PUBLIC VOID  FwlAclSetFirewallStatus PROTO ((INT4 i4Status));
PUBLIC INT1  FwlSetApplyScheduleFlag PROTO ((INT4 i4SetApplyStatus));
 
/* firewall schedule action */
#define FWL_DYNAMIC_SCHEDULE_APPLIED       1
#define FWL_DYNAMIC_SCHEDULE_NOT_APPLIED   2

/* firewall direction */
#define FWL_DYNAMIC_DIRECTION_IN     1
#define FWL_DYNAMIC_DIRECTION_OUT    2

/* firewall permission */
#define FWL_DYNAMIC_ACL_PERMIT       1
#define FWL_DYNAMIC_ACL_DENY         2

/* firewall logs */
#define FWL_DYNAMIC_LOG_NONE         0
#define FWL_DYNAMIC_LOG_BRIEF        1
#define FWL_DYNAMIC_LOG_DETAIL       2
#define FWL_MAX_LOG_BUF_SIZE         800 /* ICSA Fix */

/* Firewall log size message types 
 * Macros used while updating maximum log size & 
 * log threshold from configuration module */
#define FWL_MSG_TYPE_LOG_SIZE_THRESH     1   
#define FWL_MSG_TYPE_MAX_LOG_SIZE        2 

/* firewall fragment action */
#define FWL_DYNAMIC_FRAG_PERMIT      1
#define FWL_DYNAMIC_FRAG_DENY        2

#define FWL_MAX_FILTER_SET_LEN      252
#define FWL_MAX_RULE_NAME_LEN       36
#define FWL_MAX_FILTER_NAME_LEN     36
#define FWL_MAX_ACL_NAME_LEN        36
#define FWL_MAX_ADDR_LEN            85
#define FWL_MAX_PORT_LEN            12
#define FWL_MIN_PORT_LEN            1
#define FWL_MAX_URL_FILTER_STRING_LEN    100

#define FWL_DYNAMIC_PRIORITY_AVAILABLE     1
#define FWL_DYNAMIC_PRIORITY_NOT_AVAILABLE 2
#define FWL_DYNAMIC_MIN_RESV_PRIORITY      1500

#define FWL_NON_SIP                 0
#define FWL_APP_UNHOLD              1
#define FWL_APP_HOLD                2

/*Macros to See if a packet is accepted or rejected*/
#define FWL_ACC_PERMITTED 0
#define FWL_ACC_DENIED 1

#define FIREWALL_SYSLOG_MODULEID 0

typedef struct _DynamicFwlFilter
{
    UINT1  au1FilterName[FWL_MAX_FILTER_NAME_LEN];
    UINT1  au1SrcPort   [FWL_MAX_PORT_LEN];
    UINT1  au1DestPort  [FWL_MAX_PORT_LEN];
    UINT4  u4Protocol;
    UINT4  u4Interface; /* Optional */
    UINT1  au1SrcIpAddr [FWL_MAX_ADDR_LEN];
    UINT1  au1DstIpAddr [FWL_MAX_ADDR_LEN];
    UINT1  au1Pad[2];
} tDynamicFwlFilter;

typedef struct _DynamicFwlAccessList
{
   UINT1 au1AclName[FWL_MAX_FILTER_NAME_LEN];
   UINT1 au1FilterComb[FWL_MAX_FILTER_NAME_LEN];
   UINT1 u1Direction;
   UINT1 u1Permit;
   UINT1 u1LogTrigger;
   UINT1 u1FragAction;
   UINT1 u1ScheduleFlag;
   UINT1 au1Pad[3];
   UINT4 u4Interface; 
}tDynamicFwlAccessList;

/*****************************************************************************/
/*             Structure for storing TCP info (Seq No, Ack, Window size      */
/*             and port number)                                              */
/*****************************************************************************/

typedef struct _TcpInfo
{
   UINT4                u4EndSeqNum; /* Seq + length of TCP packet */
   UINT4                u4MaxAckNum; /* Ack + MAX(win-size, 1) */
   UINT4                u4MaxWindow; /* Max Window size seen */
   INT1                 i1WindowScaling; /* Window Scaling factor */
   UINT1                u1Padding;
   UINT2                u2Port;   /* Port number (local or remote
                                   based on direction */
} tTcpInfo;

/*****************************************************************************/
/*       Structure for storing local and remote TCP header info              */
/*****************************************************************************/

typedef struct _TcpHdr
{
   tTcpInfo             LocalTcpInfo;
   tTcpInfo             RemoteTcpInfo;   
} tTcpHdr;

/*****************************************************************************/
/*       Structure for UDP header info                                       */
/*****************************************************************************/

typedef struct _UdpHdr
{
   UINT2                u2LocalPort;
   UINT2                u2RemotePort;
} tUdpHdr;

/*****************************************************************************/
/*       Structure for storing ICMP sequence number and identifier           */
/*****************************************************************************/

typedef struct _IcmpHdr
{
   UINT2                u2IcmpSeqNum;           
   UINT2                u2IcmpId;          
} tIcmpHdr;

/*****************************************************************************/
/*       Structure for storing Stateful Session Node                         */
/*****************************************************************************/

typedef struct _StatefulSessionNode
{
     tTMO_SLL_NODE        StatefulSessionNode;

     tFwlIpAddr LocalIP;
     tFwlIpAddr RemoteIP;

     union {
             tTcpHdr   TcpHdr;
             tUdpHdr   UdpHdr;
             tIcmpHdr  IcmpHdr;
     } ProtHdr;

     UINT4  u4Timestamp;
     UINT1  u1Protocol;
     UINT1  u1LocalState;  
     UINT1  u1RemoteState;  
     UINT1  u1LogLevel;
     UINT1  u1Direction;
     UINT1  u1AppCallStatus;
     UINT1  au1Padding[2];
} tStatefulSessionNode;

#ifdef NPAPI_WANTED /* There is no seperate switch statement for flowmgr */
PUBLIC INT4 FwlUtilUpdStatefulTcpStates PROTO ((UINT4 u4SrcIp,UINT4 DestIp,
                                                   UINT2 SrcPort,UINT2 DestPort,
                                                   UINT1 u1Protocol,
                                           tStatefulSessionNode * pStateInfo));
PUBLIC INT4 FlFwlUpdateFwlInitTable PROTO ((UINT4 u4SrcIp, UINT4 u4DestIp, UINT2 u2SrcPort,
                                            UINT2 u2DestPort, UINT1 u1Proto));
#endif

PUBLIC INT4
FwlClosePinholeEntry PROTO ((tPartialInfo *pPartialInfo));
PUBLIC VOID FwlCleanAllAppEntry PROTO ((VOID)); 
INT4
FwlAppDeleteStateTableEntry PROTO ((UINT4 u4LocalIP, UINT2 u2LocalPort, 
                                    UINT4 RemoteIP,  UINT2 u2RemotePort, 
                                    UINT2 u2Protocol));

PUBLIC UINT1
FwlUtilGetFwlStatus PROTO ((UINT4 u4IfNum));

PUBLIC UINT1
FwlUtilGetGlobalStatus PROTO ((VOID));

PUBLIC UINT4 
FwlChkIfExtInterface PROTO ((UINT4 u4IfaceNum));

PUBLIC VOID FwlAclShutdown        PROTO((VOID));
VOID FwlAddDefaultRules PROTO ((VOID));

PUBLIC VOID FwlApiSetFeatures PROTO ((UINT4 v4featureMaskVal, 
                                      UINT4 v6featureMaskVal));

#ifdef TR69_WANTED
VOID FwlSetTr69AcsPortNum PROTO ((UINT2));
#endif

/*Application Call back Functions*/
INT4 FwlUtilRegisterCallBack PROTO ((UINT4 u4Event, tFsCbInfo *pFsCbInfo));
typedef enum
{
    FWL_CUST_IF_CHECK_EVENT= 1,
    FWL_CUST_MAX_CALL_BACK_EVENTS

}eFwlUtilCallBackEvents;

typedef struct 
{
     UINT4  u4Direction;
     UINT4  u4SrcIpAddr;
     UINT4  u4DestIpAddr;
     UINT4  u4DestPort;
     UINT4  u4OutIpAddr; /* Out Parameter - u4OutIpAddr is filled with IP
                          * address, if DestIpAddr of incoming packet matches 
                          * with SrcAddr of ACL in FWL_DIRECTION_IN case*/
     UINT4  u4PortMatch; /* Out Parameter - It takes value FWL_TRUE/FWL_FALSE
                          * whether u4DestPort of packet is matched with
                          * DestPort of ACL*/
} tFwlPacketInfo;

PUBLIC INT4 FwlUtilAclLookup PROTO ((tFwlPacketInfo *pFwlPacketNode, 
                                     UINT1 *pu1AclName, INT4 i4Type));
PUBLIC INT4 FwlUtilGetAclAction PROTO ((INT4 i4Type, UINT1 *pu1AclName));

PUBLIC VOID FwlUtilUpdateLogParams PROTO ((UINT1 u1MsgType,UINT4 u4Value));

PUBLIC INT1 FwlErrorMessageGenerate PROTO((tCRU_BUF_CHAIN_HEADER *pBuf, 
                                           UINT4 u4IfIndex));

PUBLIC INT1 FwlErrorV6MessageGenerate PROTO((tCRU_BUF_CHAIN_HEADER *pBuf,
                                           UINT4 u4IfIndex));

PUBLIC INT4 FwlSendThresholdExceededTrap PROTO((UINT4 u4IfIndex,
                                                UINT4 u4DenyCount));

PUBLIC VOID IdsSendTrapMessage PROTO ((UINT4 u4MsgType));

PUBLIC UINT1
FwlUtilGetIcmpControlStatus PROTO ((VOID));

PUBLIC INT4 FwlUtilGetSysLogId PROTO ((VOID));

PUBLIC INT4 FwlUtilUpdateIdsRulesStatus PROTO ((UINT4 u4Status));

PUBLIC VOID FwlUtilRestoreIdsStatus PROTO ((VOID));

#endif /* _FIREWALL_H */
