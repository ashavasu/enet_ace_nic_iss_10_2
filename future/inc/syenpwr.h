/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id $
 *
 * Description: This file contains the declaration for SyncE NPAPIs
 *
 *****************************************************************************/

#include "syncenp.h"
/* OPER ID */

#ifndef __SYNCE_NP_WR__
#define __SYNCE_NP_WR__

#define  FS_NP_HW_CONFIG_SYNCE_INFO                             1

/* Required arguments list for the synce NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

typedef struct {
    tSynceHwInfo *  pSynceHwSynceInfo;
} tSynceNpWrFsNpHwConfigSynceInfo;

typedef struct SynceNpModInfo {
union {
    tSynceNpWrFsNpHwConfigSynceInfo  sFsNpHwConfigSynceInfo;
    }unOpCode;

#define  SynceNpFsNpHwConfigSynceInfo  unOpCode.sFsNpHwConfigSynceInfo;
} tSynceNpModInfo;


/* Function Prototype for Generic NP OpCode specific wrappers
 * defined in Syncenpapi.c */

UINT1 SynceFsNpHwConfigSynceInfo PROTO ((tSynceHwInfo * pSynceHwSynceInfo));
#endif
