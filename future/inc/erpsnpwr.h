
/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: erpsnpwr.h,v 1.2 2013/02/02 10:06:08 siva Exp $
 *
 * Description:This file contains the necessary data strucutures
 *             for Hardware Programming.
 *             <Part of dual node stacking model>
 *
 *******************************************************************/

#ifndef __ERPS_NP_WR_H__
#define __ERPS_NP_WR_H__

#include "erps.h"

/* OPER ID */

#define  FS_ERPS_HW_RING_CONFIG                                 1
#ifdef MBSM_WANTED
#define  FS_ERPS_MBSM_HW_RING_CONFIG                            2
#endif /* MBSM_WANTED */

/* Required arguments list for the erps NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

typedef struct {
    tErpsHwRingInfo *  pErpsHwRingInfo;
} tErpsNpWrFsErpsHwRingConfig;

#ifdef MBSM_WANTED
typedef struct {
    tErpsHwRingInfo *  pErpsHwRingInfo;
    tErpsMbsmInfo *    pErpsMbsmInfo;
} tErpsNpWrFsErpsMbsmHwRingConfig;

#endif /* MBSM_WANTED */
typedef struct ErpsNpModInfo {
union {
    tErpsNpWrFsErpsHwRingConfig  sFsErpsHwRingConfig;
#ifdef MBSM_WANTED
    tErpsNpWrFsErpsMbsmHwRingConfig  sFsErpsMbsmHwRingConfig;
#endif /* MBSM_WANTED */
    }unOpCode;

#define  ErpsNpFsErpsHwRingConfig  unOpCode.sFsErpsHwRingConfig;
#ifdef MBSM_WANTED
#define  ErpsNpFsErpsMbsmHwRingConfig  unOpCode.sFsErpsMbsmHwRingConfig;
#endif /* MBSM_WANTED */
} tErpsNpModInfo;


/* Function Prototype for Generic NP OpCode specific wrappers
 * defined in Erpsnpapi.c */

UINT1 ErpsFsErpsHwRingConfig PROTO ((tErpsHwRingInfo * pErpsHwRingInfo));
#ifdef MBSM_WANTED
UINT1 ErpsFsErpsMbsmHwRingConfig PROTO ((tErpsHwRingInfo * pErpsHwRingInfo, tErpsMbsmInfo * pErpsMbsmInfo));
#endif /* MBSM_WANTED */

#endif /* __ERPS_NP_WR_H__ */
