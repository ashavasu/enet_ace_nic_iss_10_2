/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: fsmef.h,v 1.8 2014/10/10 12:04:42 siva Exp $
 *
 * Description:
 *********************************************************************/
#ifndef __FSMEF_H__
#define __FSMEF_H__

/**************************Globle Defines*******************************/
enum
{
   MEF_CREATE_CONTEXT = 1,
   MEF_DELETE_CONTEXT,
   MEF_CREATE_PORT,
   MEF_DELETE_PORT,
   MEF_MAP_PORT,
   MEF_UNMAP_PORT,
};


/*************************Common Defines for CLI and WEB ****************/
enum {
   MEF_L2CP_PROTOCOL_DOT1X,
   MEF_L2CP_PROTOCOL_LACP,
   MEF_L2CP_PROTOCOL_STP,
   MEF_L2CP_PROTOCOL_GVRP,
   MEF_L2CP_PROTOCOL_GMRP,
   MEF_L2CP_PROTOCOL_MVRP,
   MEF_L2CP_PROTOCOL_MMRP,
   MEF_L2CP_PROTOCOL_ELMI,
   MEF_L2CP_PROTOCOL_LLDP,
   MEF_L2CP_PROTOCOL_ECFM,
   MEF_L2CP_PROTOCOL_IGMP,
   MEF_L2CP_PROTOCOL_EOAM
};

enum {
   MEF_L2CP_PROTO_ACTION_DISCARD,
   MEF_L2CP_PROTO_ACTION_PEER,
   MEF_L2CP_PROTO_ACTION_TUNNEL
};

enum {
  MEF_FILTER_DIRECTION_IN = ISS_DIRECTION_IN,
  MEF_FILTER_DIRECTION_OUT = ISS_DIRECTION_OUT
};

enum {
  MEF_METER_COLOR_AWARE = QOS_METER_COLOR_AWARE,
  MEF_METER_COLOR_UNAWARE = QOS_METER_COLOR_BLIND
};

enum {
  MEF_METER_COUPLED = QOS_METER_TYPE_MEF_COUPLED,
  MEF_METER_DECOUPLED = QOS_METER_TYPE_MEF_DECOUPLED
};

#define UNI_ID_MAX_LENGTH             CFA_MAX_INTF_DESC_LEN
#define EVC_ID_MAX_LENGTH             32
#define MEF_MAX_NAME_LENGTH           32
#define MEF_TX_STATUS_START              2
#define MEF_TX_STATUS_STOP               3
#define MEF_SNMP_TRUE                    ECFM_SNMP_TRUE
#define MEF_SNMP_FALSE                   ECFM_SNMP_FALSE
#define MEF_GET_4BYTE(u4Val, pu1Buf)\
            MEMCPY (&u4Val, pu1Buf, 4);\
            pu1Buf += 4;\
            u4Val = (OSIX_NTOHL(u4Val));\

#define MEF_MULTIPLEX_BITMASK               0x01
#define MEF_BUNDLE_BITMASK                  0x02
#define MEF_ALL_TO_ONE_BUNDLE_BITMASK       0x04
#define MEF_DEFAULT_UNI_TYPE                (MEF_MULTIPLEX_BITMASK |\
                                             MEF_BUNDLE_BITMASK)

#define MEF_CLI_FLOAT_STORAGE_SIZE   18 
#define MEF_ETREE_MAX_LEAF_UNIS    ISS_MAX_UPLINK_PORTS

/**************************Globle Prototypes*******************************/
VOID MefTask (INT1 *i1Params);
INT4 MefShowRunningConfig (tCliHandle CliHandle);
PUBLIC INT4 MefApiHandleContextIndication (UINT4 u4ContextId,
                                           UINT4 u4Indication);
PUBLIC INT4 MefApiHandlePortIndication (UINT4 u4ContextId, UINT4 u4IfIndex,
                                        UINT2 u2LocalPortId, UINT4 u4Inication);

PUBLIC VOID MefApiGetEvcInfo (UINT4 SVlanId, tEvcInfo * pIssEvcInfoLocal);
#endif
