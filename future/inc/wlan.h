/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: wlan.h,v 1.1 2007/03/15 05:03:36 iss Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              by  external modules.
 *
 *******************************************************************/

#ifndef _WLAN_H
#define _WLAN_H

#define WLAN_SUCCESS                              1
#define WLAN_FAILURE                              2

#define WLAN_MAX_SSID_LEN                         32
#define WLAN_MAX_ESSID                            16
#define WLAN_MAX_RADIOS                           2
#define WLAN_MAX_MAC_FILTERS                      16
#define WLAN_MAX_NTFY_PKTS                        100


typedef UINT1 tSsid [WLAN_MAX_SSID_LEN + 4];

typedef struct WLANSTATIONINFO {
    tSsid    staSsid;             /* Ssid to which station is associated */
    tMacAddr staMacAddr;          /* Station MAC address */
    UINT2    u2RadioType;         /* Type of radio (A/BG) */
    UINT4    u4VapId;             /* Virtual AP Id */
    UINT4    u4AssocId;           /* Association Id */
    UINT4    u4VlanId;            /* Vlan id associated by the packets */       
    UINT4    u4RxByteCount;       /* Received byte count */
    UINT4    u4TxByteCount;       /* Transmitted byte count */
    UINT4    u4RxPktCount;        /* Received packet count */
    UINT4    u4TxPktCount;        /* Transmitted packet count */
    UINT4    u4AssocTime;         /* Associated time */
    UINT4    u4LastAssocTime;     /* Last associated time */
    UINT4    u4LastDisAssocTime;  /* Last disassociated time */
    UINT4    u4Last1xAuthTime;    /* Last auth time by 802.1x */
    UINT4    u4Last80211AuthTime; /* Last auth time by 802.11 */
    UINT2    u2KeyType;           /* Type of encryption used */
    UINT2    u2AuthType;          /* Type of authentication used */
    BOOL1    b1Authenticated;     /* Authenticated or not */
    BOOL1    b1Associated;        /* Associated or not */   
    BOOL1    b1Authorised;        /* Authorised or not */
    UINT1    u1Pad;               /* Padding byte */
} tWlanSta;

typedef enum {
    WLAN_MAC_FILTCMD_ALLOW = 1,
    WLAN_MAC_FILTCMD_DENY
} eWlanMacFiltCmd;

#define WLAN_MAC_FILTER_ALLOW                    1
#define WLAN_MAC_FILTER_DENY                     2
#define WLAN_MAC_FILTER_DEL                      3

#define WLAN_WMM_DISABLED                        1
#define WLAN_WMM_SUPPORTED                       2
#define WLAN_WMM_REQUIRED                        3

/* Authentication algorithms */
#define WLAN_AUTH_OPEN                           1
#define WLAN_AUTH_SHARED                         2
#define WLAN_AUTH_WPA                            3
#define WLAN_AUTH_WPA2                           4
#define WLAN_AUTH_WPA_WPA2_MIXED                 5
#define WLAN_AUTH_WPA_PSK                        6
#define WLAN_AUTH_WPA2_PSK                       7
#define WLAN_AUTH_WPA_WPA2_PSK_MIXED             8

/* Modes of authentication */
#define WLAN_AUTH_DISABLED                       1
#define WLAN_AUTH_REQUIRED                       2
#define WLAN_AUTH_SUPPORTED                      3

/* Cipher suites */
#define WLAN_CIPHER_NOKEY                        1
#define WLAN_CIPHER_STATIC_WEP                   2
#define WLAN_CIPHER_DYNAMIC_WEP                  3
#define WLAN_CIPHER_AES_CCMP                     4
#define WLAN_CIPHER_TKIP                         5
#define WLAN_CIPHER_WEP                          6
#define WLAN_CIPHER_AES_CCMP_TKIP                7
#define WLAN_CIPHER_AES_CCMP_WEP                 8
#define WLAN_CIPHER_TKIP_WEP                     9
#define WLAN_CIPHER_AES_CCMP_TKIP_WEP            10

/* Encryption status */
#define WLAN_ENCRYPTION_ENABLE                   1
#define WLAN_ENCRYPTION_DISABLE                  2

/* Radio Modes*/
#define WLAN_RADIO_MODE_ALL                      1
#define WLAN_RADIO_MODE_A                        2
#define WLAN_RADIO_MODE_BG                       3
#define WLAN_RADIO_MODE_B                        4
#define WLAN_RADIO_MODE_G                        5

/* PSK Type */
#define WLAN_PSK_HEX                             1
#define WLAN_PSK_ASCII                           2

/* PSK Lengths */
#define WLAN_HEX_PSK_LEN                         64
#define WLAN_MIN_PASS_PHRASE_LEN                 8
#define WLAN_MAX_PASS_PHRASE_LEN                 63

#define WLAN_ALIGNED_PASS_PHRASE_LEN              64
#define WLAN_ALIGNED_HEX_PSK_LEN                  68

/* WEP Key Type */
#define WLAN_WEP_TYPE_HEX                        1
#define WLAN_WEP_TYPE_ASCII                      2

/* WMM ACK Policy */
#define WLAN_AC_ACK_POLICY                       1
#define WLAC_AC_NO_ACK_POLICY                    2

#define WLAN_AC_AP                               1
#define WLAN_AC_BSS                              2

#define WLAN_AP_ADM_CRTL_ENABLE                   1
#define WLAN_AP_ADM_CRTL_DISABLE                  2

#define WLAN_MAX_COUNTRY_CODE                    20

/* Default values */
#define WLAN_DEF_BEACON_INTERVAL                 100
#define WLAN_DEF_DTIM_PERIOD                     1
#define WLAN_DEF_FRAG_THRESHOLD                  2346 
#define WLAN_DEF_RTS_THRESHOLD                   2347
#define WLAN_DEF_RADIO_CHANNEL                   0
#define WLAN_DEF_WMM_STATUS                      WLAN_WMM_DISABLED
#define WLAN_DEF_RADIO_MODE                      WLAN_RADIO_MODE_BG
#define WLAN_DEF_AC_ACK_POLICY                   WLAN_AC_ACK_POLICY

#define WLAN_DEF_AC0_AP_LOGCW_MIN                4
#define WLAN_DEF_AC0_AP_LOGCW_MAX                10
#define WLAN_DEF_AC0_AP_AIFSN                    3
#define WLAN_DEF_AC0_AP_TXOP                     0
#define WLAN_DEF_AC0_AP_ADM_CTRL                 WLAN_AP_ADM_CRTL_DISABLE
#define WLAN_DEF_AC0_BSS_LOGCW_MIN               4
#define WLAN_DEF_AC0_BSS_LOGCW_MAX               6
#define WLAN_DEF_AC0_BSS_AIFSN                   3
#define WLAN_DEF_AC0_BSS_TXOP                    0
#define WLAN_DEF_AC0_BSS_ADM_CTRL                WLAN_AP_ADM_CRTL_DISABLE
#define WLAN_DEF_AC1_AP_LOGCW_MIN                4
#define WLAN_DEF_AC1_AP_LOGCW_MAX                7
#define WLAN_DEF_AC1_AP_AIFSN                    10
#define WLAN_DEF_AC1_AP_TXOP                     0
#define WLAN_DEF_AC1_AP_ADM_CTRL                 WLAN_AP_ADM_CRTL_DISABLE
#define WLAN_DEF_AC1_BSS_LOGCW_MIN               4
#define WLAN_DEF_AC1_BSS_LOGCW_MAX               7
#define WLAN_DEF_AC1_BSS_AIFSN                   10
#define WLAN_DEF_AC1_BSS_TXOP                    0
#define WLAN_DEF_AC1_BSS_ADM_CTRL                WLAN_AP_ADM_CRTL_DISABLE
#define WLAN_DEF_AC2_AP_LOGCW_MIN                3
#define WLAN_DEF_AC2_AP_LOGCW_MAX                4
#define WLAN_DEF_AC2_AP_AIFSN                    2
#define WLAN_DEF_AC2_AP_TXOP                     94
#define WLAN_DEF_AC2_AP_ADM_CTRL                 WLAN_AP_ADM_CRTL_DISABLE
#define WLAN_DEF_AC2_BSS_LOGCW_MIN               3
#define WLAN_DEF_AC2_BSS_LOGCW_MAX               4
#define WLAN_DEF_AC2_BSS_AIFSN                   1
#define WLAN_DEF_AC2_BSS_TXOP                    94
#define WLAN_DEF_AC2_BSS_ADM_CTRL                WLAN_AP_ADM_CRTL_DISABLE
#define WLAN_DEF_AC3_AP_LOGCW_MIN                2
#define WLAN_DEF_AC3_AP_LOGCW_MAX                3
#define WLAN_DEF_AC3_AP_AIFSN                    2
#define WLAN_DEF_AC3_AP_TXOP                     47 
#define WLAN_DEF_AC3_AP_ADM_CTRL                 WLAN_AP_ADM_CRTL_DISABLE
#define WLAN_DEF_AC3_BSS_LOGCW_MIN               2
#define WLAN_DEF_AC3_BSS_LOGCW_MAX               3
#define WLAN_DEF_AC3_BSS_AIFSN                   1
#define WLAN_DEF_AC3_BSS_TXOP                    47
#define WLAN_DEF_AC3_BSS_ADM_CTRL                WLAN_AP_ADM_CRTL_DISABLE


/* Traces */
#define  WLAN_RPC_TRC                            0x00000100
#define  WLAN_FN_ENTRY                           0x00000200
#define  WLAN_FN_EXIT                            0x00000400
#define  WLAN_CRIT_TRC                           0x00000800
#define  WLAN_FAIL_TRC                           0x00001000
#define  WLAN_DBG_TRC                            0x00002000
#define  WLAN_WD_TRC                             0x00004000
/* Notify related traces */
#define  WLAN_NTFY_DBG_TRC                       0x00000010

#define  WLAN_ALL_TRC                            WLAN_RPC_TRC   |\
                                                 WLAN_FN_ENTRY  |\
                                                 WLAN_FN_EXIT   |\
                                                 WLAN_CRIT_TRC  |\
                                                 WLAN_FAIL_TRC  |\
                                                 WLAN_DBG_TRC   |\
                                                 WLAN_NTFY_DBG_TRC|\
                                                 WLAN_WD_TRC

#define WLAN_LOCK()                              WlanMainLock ()
#define WLAN_UNLOCK()                            WlanMainUnLock ()

PUBLIC INT4  WlanMainLock                        PROTO ((VOID));
PUBLIC INT4  WlanMainUnLock                      PROTO ((VOID));

PUBLIC UINT4 WlanMainTaskInit                    PROTO ((VOID));
PUBLIC VOID  WlanMainTaskDeInit                  PROTO ((VOID));
PUBLIC INT1 WlanGetRadioConfigPrompt             PROTO ((INT1*, INT1*));
PUBLIC UINT4 WlanStaAddStation                   PROTO ((tWlanSta*));
PUBLIC VOID WlanNtfyGetOperStatus                PROTO ((UINT4, UINT1*));
PUBLIC UINT4 WlanNtfyAuthFailure                 PROTO ((tMacAddr));
PUBLIC UINT4 WlanTransmitAuthKeyToAP             PROTO ((UINT1*, UINT4, 
                                                         tMacAddr));
PUBLIC UINT4 WlanRadFillRadioInfo                PROTO ((UINT4, UINT4, UINT4));

#endif /*_WLAN_H*/

