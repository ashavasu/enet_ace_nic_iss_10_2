/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: ipmcnpwr.h,v 1.4 2015/03/18 13:30:15 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              for Multicast wrappers
 *              
 ********************************************************************/
#ifndef _IPMCNP_WR_H_
#define _IPMCNP_WR_H_

#include "pim.h"
#include "ipmcnp.h"
#include "fssyslog.h"

#ifdef FS_NPAPI
#define  FS_NP_IPV4_MC_INIT                                     1
#define  FS_NP_IPV4_MC_DE_INIT                                  2
#define  FS_NP_IPV4_VLAN_MC_INIT                                3
#define  FS_NP_IPV4_VLAN_MC_DE_INIT                             4
#define  FS_NP_IPV4_RPORT_MC_INIT                               5
#define  FS_NP_IPV4_RPORT_MC_DE_INIT                            6
#define  FS_NP_IPV4_MC_ADD_ROUTE_ENTRY                          7
#define  FS_NP_IPV4_MC_DEL_ROUTE_ENTRY                          8
#define  FS_NP_IPV4_MC_CLEAR_ALL_ROUTES                         9
#define  FS_NP_IPV4_MC_ADD_CPU_PORT                             10
#define  FS_NP_IPV4_MC_DEL_CPU_PORT                             11
#define  FS_NP_IPV4_MC_GET_HIT_STATUS                           12
#define  FS_NP_IPV4_MC_UPDATE_OIF_VLAN_ENTRY                    13
#define  FS_NP_IPV4_MC_UPDATE_IIF_VLAN_ENTRY                    14
#define  FS_NP_IPV4_GET_M_ROUTE_STATS                           15
#define  FS_NP_IPV4_GET_M_ROUTE_H_C_STATS                       16
#define  FS_NP_IPV4_GET_M_NEXT_HOP_STATS                        17
#define  FS_NP_IPV4_GET_M_IFACE_STATS                           18
#define  FS_NP_IPV4_GET_M_IFACE_H_C_STATS                       19
#define  FS_NP_IPV4_SET_M_IFACE_TTL_TRESHOLD                    20
#define  FS_NP_IPV4_SET_M_IFACE_RATE_LIMIT                      21
#define  FS_NP_IPV_X_HW_GET_MCAST_ENTRY                         22
#define  FS_NP_DBG_TIME_LEN                                     30
#ifdef PIM_WANTED
#define  FS_PIM_NP_INIT_HW                                      23
#define  FS_PIM_NP_DE_INIT_HW                                   24
#endif /* PIM_WANTED */
#ifdef DVMRP_WANTED
#define  FS_DVMRP_NP_INIT_HW                                    25
#define  FS_DVMRP_NP_DE_INIT_HW                                 26
#endif /* DVMRP_WANTED */
#ifdef IGMPPRXY_WANTED
#define  FS_NP_IGMP_PROXY_INIT                                  27
#define  FS_NP_IGMP_PROXY_DE_INIT                               28
#endif /* IGMPPRXY_WANTED */
#ifdef PIM_WANTED
#define  FS_NP_IPV4_MC_CLEAR_HIT_BIT                            29
#endif
#define  FS_NP_IPV4_MC_RPF_D_F_INFO                             30
#endif
#ifdef MBSM_WANTED
#define  FS_NP_IPV4_MBSM_MC_INIT                                31
#define  FS_NP_IPV4_MBSM_MC_ADD_ROUTE_ENTRY                     32
#ifdef PIM_WANTED
#define  FS_PIM_MBSM_NP_INIT_HW                                 33
#endif /* PIM_WANTED */
#ifdef DVMRP_WANTED
#define  FS_DVMRP_MBSM_NP_INIT_HW                               34
#endif
#endif /* DVMRP_WANTED */

/* Required arguments list for the ipmc NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

#ifdef FS_NPAPI
typedef struct {
    UINT4  u4VlanId;
} tIpmcNpWrFsNpIpv4VlanMcInit;
typedef struct {
    UINT4  u4Index;
} tIpmcNpWrFsNpIpv4VlanMcDeInit;

typedef struct {
    tRPortInfo  PortInfo;
} tIpmcNpWrFsNpIpv4RportMcInit;

typedef struct {
    tRPortInfo  PortInfo;
} tIpmcNpWrFsNpIpv4RportMcDeInit;

typedef struct {
    tMcRtEntry         rtEntry;
    tMcDownStreamIf *  pDownStreamIf;
    UINT4              u4VrId;
    UINT4              u4GrpAddr;
    UINT4              u4GrpPrefix;
    UINT4              u4SrcIpAddr;
    UINT4              u4SrcIpPrefix;
    UINT2              u2NoOfDownStreamIf;
    UINT1              u1CallerId;
    UINT1              au1Pad[1];
} tIpmcNpWrFsNpIpv4McAddRouteEntry;

typedef struct {
    tMcRtEntry         rtEntry;
    tMcDownStreamIf *  pDownStreamIf;
    UINT4              u4VrId;
    UINT4              u4GrpAddr;
    UINT4              u4GrpPrefix;
    UINT4              u4SrcIpAddr;
    UINT4              u4SrcIpPrefix;
    UINT2              u2NoOfDownStreamIf;
    UINT1              au1Pad[2];
} tIpmcNpWrFsNpIpv4McDelRouteEntry;

typedef struct {
    UINT4  u4VrId;
} tIpmcNpWrFsNpIpv4McClearAllRoutes;

typedef struct {
    tMcRtEntry  rtEntry;
    UINT4       u4GrpAddr;
    UINT4       u4SrcAddr;
    UINT2       u2GenRtrId;
    UINT1       au1Pad[2];
} tIpmcNpWrFsNpIpv4McAddCpuPort;

typedef struct {
    tMcRtEntry  rtEntry;
    UINT4       u4GrpAddr;
    UINT4       u4SrcAddr;
    UINT2       u2GenRtrId;
    UINT1       au1Pad[2];
} tIpmcNpWrFsNpIpv4McDelCpuPort;

typedef struct {
    UINT4    u4SrcIpAddr;
    UINT4    u4GrpAddr;
    UINT4    u4Iif;
    UINT4 *  pu4HitStatus;
    UINT2    u2VlanId;
    UINT1       au1Pad[2];
} tIpmcNpWrFsNpIpv4McGetHitStatus;

typedef struct {
    UINT4            u4VrId;
    UINT4            u4GrpAddr;
    UINT4            u4SrcIpAddr;
    tMcRtEntry       rtEntry;
    tMcDownStreamIf  downStreamIf;
} tIpmcNpWrFsNpIpv4McUpdateOifVlanEntry;

typedef struct {
    UINT4       u4VrId;
    UINT4       u4GrpAddr;
    UINT4       u4SrcIpAddr;
    tMcRtEntry  rtEntry;
} tIpmcNpWrFsNpIpv4McUpdateIifVlanEntry;

typedef struct {
    UINT4    u4VrId;
    UINT4    u4GrpAddr;
    UINT4    u4SrcAddr;
    INT4     i4StatType;
    UINT4 *  pu4RetValue;
} tIpmcNpWrFsNpIpv4GetMRouteStats;

typedef struct {
    UINT4                   u4VrId;
    UINT4                   u4GrpAddr;
    UINT4                   u4SrcAddr;
    INT4                    i4StatType;
    tSNMP_COUNTER64_TYPE *  pu8RetValue;
} tIpmcNpWrFsNpIpv4GetMRouteHCStats;

typedef struct {
    INT4     i4OutIfIndex;
    UINT4    u4VrId;
    UINT4    u4GrpAddr;
    UINT4    u4SrcAddr;
    INT4     i4StatType;
    UINT4 *  pu4RetValue;
} tIpmcNpWrFsNpIpv4GetMNextHopStats;

typedef struct {
    INT4     i4IfIndex;
    UINT4    u4VrId;
    INT4     i4StatType;
    UINT4 *  pu4RetValue;
} tIpmcNpWrFsNpIpv4GetMIfaceStats;

typedef struct {
    INT4                    i4IfIndex;
    UINT4                   u4VrId;
    INT4                    i4StatType;
    tSNMP_COUNTER64_TYPE *  pu8RetValue;
} tIpmcNpWrFsNpIpv4GetMIfaceHCStats;

typedef struct {
    INT4   i4IfIndex;
    UINT4  u4VrId;
    INT4   i4TtlTreshold;
} tIpmcNpWrFsNpIpv4SetMIfaceTtlTreshold;

typedef struct {
    INT4   i4IfIndex;
    UINT4  u4VrId;
    INT4   i4RateLimit;
} tIpmcNpWrFsNpIpv4SetMIfaceRateLimit;

typedef struct {
    tNpL3McastEntry *  pNpL3McastEntry;
} tIpmcNpWrFsNpIpvXHwGetMcastEntry;

#ifdef PIM_WANTED
#endif /* PIM_WANTED */
#ifdef DVMRP_WANTED
#endif /* DVMRP_WANTED */
#ifdef IGMPPRXY_WANTED
#endif /* IGMPPRXY_WANTED */
#ifdef PIM_WANTED
typedef struct {
    UINT4  u4GrpAddr;
    UINT4  u4SrcAddr;
    UINT2  u2VlanId;
    UINT1  au1Pad[2];
} tIpmcNpWrFsNpIpv4McClearHitBit;

#endif
typedef struct {
    tMcRpfDFInfo *  pMcRpfDFInfo;
} tIpmcNpWrFsNpIpv4McRpfDFInfo;

#endif
#ifdef MBSM_WANTED
typedef struct {
    tMbsmSlotInfo *  pSlotInfo;
} tIpmcNpWrFsNpIpv4MbsmMcInit;

typedef struct {
    tMcDownStreamIf *  pDownStreamIf;
    tMbsmSlotInfo *    pSlotInfo;
    tMcRtEntry         rtEntry;
    UINT4              u4VrId;
    UINT4              u4GrpAddr;
    UINT4              u4GrpPrefix;
    UINT4              u4SrcIpAddr;
    UINT4              u4SrcIpPrefix;
    UINT2              u2NoOfDownStreamIf;
    UINT1              u1CallerId;
    UINT1          au1Pad[1];
} tIpmcNpWrFsNpIpv4MbsmMcAddRouteEntry;

#ifdef PIM_WANTED
typedef struct {
    tMbsmSlotInfo *  pSlotInfo;
} tIpmcNpWrFsPimMbsmNpInitHw;

#endif /* PIM_WANTED */
#ifdef DVMRP_WANTED
typedef struct {
    tMbsmSlotInfo *  pSlotInfo;
} tIpmcNpWrFsDvmrpMbsmNpInitHw;

#endif
#endif /* DVMRP_WANTED */
typedef struct IpmcNpModInfo {
union {
#ifdef FS_NPAPI
    tIpmcNpWrFsNpIpv4VlanMcInit  sFsNpIpv4VlanMcInit;
    tIpmcNpWrFsNpIpv4VlanMcDeInit  sFsNpIpv4VlanMcDeInit;
    tIpmcNpWrFsNpIpv4RportMcInit  sFsNpIpv4RportMcInit;
    tIpmcNpWrFsNpIpv4RportMcDeInit  sFsNpIpv4RportMcDeInit;
    tIpmcNpWrFsNpIpv4McAddRouteEntry  sFsNpIpv4McAddRouteEntry;
    tIpmcNpWrFsNpIpv4McDelRouteEntry  sFsNpIpv4McDelRouteEntry;
    tIpmcNpWrFsNpIpv4McClearAllRoutes  sFsNpIpv4McClearAllRoutes;
    tIpmcNpWrFsNpIpv4McAddCpuPort  sFsNpIpv4McAddCpuPort;
    tIpmcNpWrFsNpIpv4McDelCpuPort  sFsNpIpv4McDelCpuPort;
    tIpmcNpWrFsNpIpv4McGetHitStatus  sFsNpIpv4McGetHitStatus;
    tIpmcNpWrFsNpIpv4McUpdateOifVlanEntry  sFsNpIpv4McUpdateOifVlanEntry;
    tIpmcNpWrFsNpIpv4McUpdateIifVlanEntry  sFsNpIpv4McUpdateIifVlanEntry;
    tIpmcNpWrFsNpIpv4GetMRouteStats  sFsNpIpv4GetMRouteStats;
    tIpmcNpWrFsNpIpv4GetMRouteHCStats  sFsNpIpv4GetMRouteHCStats;
    tIpmcNpWrFsNpIpv4GetMNextHopStats  sFsNpIpv4GetMNextHopStats;
    tIpmcNpWrFsNpIpv4GetMIfaceStats  sFsNpIpv4GetMIfaceStats;
    tIpmcNpWrFsNpIpv4GetMIfaceHCStats  sFsNpIpv4GetMIfaceHCStats;
    tIpmcNpWrFsNpIpv4SetMIfaceTtlTreshold  sFsNpIpv4SetMIfaceTtlTreshold;
    tIpmcNpWrFsNpIpv4SetMIfaceRateLimit  sFsNpIpv4SetMIfaceRateLimit;
    tIpmcNpWrFsNpIpvXHwGetMcastEntry  sFsNpIpvXHwGetMcastEntry;
#ifdef PIM_WANTED
#endif /* PIM_WANTED */
#ifdef DVMRP_WANTED
#endif /* DVMRP_WANTED */
#ifdef IGMPPRXY_WANTED
#endif /* IGMPPRXY_WANTED */
#ifdef PIM_WANTED
    tIpmcNpWrFsNpIpv4McClearHitBit  sFsNpIpv4McClearHitBit;
#endif
    tIpmcNpWrFsNpIpv4McRpfDFInfo  sFsNpIpv4McRpfDFInfo;
#endif
#ifdef MBSM_WANTED
    tIpmcNpWrFsNpIpv4MbsmMcInit  sFsNpIpv4MbsmMcInit;
    tIpmcNpWrFsNpIpv4MbsmMcAddRouteEntry  sFsNpIpv4MbsmMcAddRouteEntry;
#ifdef PIM_WANTED
    tIpmcNpWrFsPimMbsmNpInitHw  sFsPimMbsmNpInitHw;
#endif /* PIM_WANTED */
#ifdef DVMRP_WANTED
    tIpmcNpWrFsDvmrpMbsmNpInitHw  sFsDvmrpMbsmNpInitHw;
#endif
#endif /* DVMRP_WANTED */
    }unOpCode;

#ifdef FS_NPAPI
#define  IpmcNpFsNpIpv4VlanMcInit  unOpCode.sFsNpIpv4VlanMcInit;
#define  IpmcNpFsNpIpv4VlanMcDeInit  unOpCode.sFsNpIpv4VlanMcDeInit;
#define  IpmcNpFsNpIpv4RportMcInit  unOpCode.sFsNpIpv4RportMcInit;
#define  IpmcNpFsNpIpv4RportMcDeInit  unOpCode.sFsNpIpv4RportMcDeInit;
#define  IpmcNpFsNpIpv4McAddRouteEntry  unOpCode.sFsNpIpv4McAddRouteEntry;
#define  IpmcNpFsNpIpv4McDelRouteEntry  unOpCode.sFsNpIpv4McDelRouteEntry;
#define  IpmcNpFsNpIpv4McClearAllRoutes  unOpCode.sFsNpIpv4McClearAllRoutes;
#define  IpmcNpFsNpIpv4McAddCpuPort  unOpCode.sFsNpIpv4McAddCpuPort;
#define  IpmcNpFsNpIpv4McDelCpuPort  unOpCode.sFsNpIpv4McDelCpuPort;
#define  IpmcNpFsNpIpv4McGetHitStatus  unOpCode.sFsNpIpv4McGetHitStatus;
#define  IpmcNpFsNpIpv4McUpdateOifVlanEntry  unOpCode.sFsNpIpv4McUpdateOifVlanEntry;
#define  IpmcNpFsNpIpv4McUpdateIifVlanEntry  unOpCode.sFsNpIpv4McUpdateIifVlanEntry;
#define  IpmcNpFsNpIpv4GetMRouteStats  unOpCode.sFsNpIpv4GetMRouteStats;
#define  IpmcNpFsNpIpv4GetMRouteHCStats  unOpCode.sFsNpIpv4GetMRouteHCStats;
#define  IpmcNpFsNpIpv4GetMNextHopStats  unOpCode.sFsNpIpv4GetMNextHopStats;
#define  IpmcNpFsNpIpv4GetMIfaceStats  unOpCode.sFsNpIpv4GetMIfaceStats;
#define  IpmcNpFsNpIpv4GetMIfaceHCStats  unOpCode.sFsNpIpv4GetMIfaceHCStats;
#define  IpmcNpFsNpIpv4SetMIfaceTtlTreshold  unOpCode.sFsNpIpv4SetMIfaceTtlTreshold;
#define  IpmcNpFsNpIpv4SetMIfaceRateLimit  unOpCode.sFsNpIpv4SetMIfaceRateLimit;
#define  IpmcNpFsNpIpvXHwGetMcastEntry  unOpCode.sFsNpIpvXHwGetMcastEntry;
#ifdef PIM_WANTED
#endif /* PIM_WANTED */
#ifdef DVMRP_WANTED
#endif /* DVMRP_WANTED */
#ifdef IGMPPRXY_WANTED
#endif /* IGMPPRXY_WANTED */
#ifdef PIM_WANTED
#define  IpmcNpFsNpIpv4McClearHitBit  unOpCode.sFsNpIpv4McClearHitBit;
#endif
#define  IpmcNpFsNpIpv4McRpfDFInfo  unOpCode.sFsNpIpv4McRpfDFInfo;
#endif
#ifdef MBSM_WANTED
#define  IpmcNpFsNpIpv4MbsmMcInit  unOpCode.sFsNpIpv4MbsmMcInit;
#define  IpmcNpFsNpIpv4MbsmMcAddRouteEntry  unOpCode.sFsNpIpv4MbsmMcAddRouteEntry;
#ifdef PIM_WANTED
#define  IpmcNpFsPimMbsmNpInitHw  unOpCode.sFsPimMbsmNpInitHw;
#endif /* PIM_WANTED */
#ifdef DVMRP_WANTED
#define  IpmcNpFsDvmrpMbsmNpInitHw  unOpCode.sFsDvmrpMbsmNpInitHw;
#endif
#endif /* DVMRP_WANTED */
} tIpmcNpModInfo;

#ifdef FS_NPAPI
/* Trace and debug flags */

#ifdef PIM_WANTED
#define  MCAST_NP_DBG_FLAG   gSPimConfigParams.u4GlobalDbg
#define MCAST_NP_DBG(Value, mod, modname, Fmt) \
        NpDbgPrintTime(MCAST_NP_DBG_FLAG); \
 UtlSysTrcLog(SYSLOG_INVAL_LEVEL, Value, MCAST_NP_DBG_NPAPI, "PIM", "PIM-NP", Fmt)

#define MCAST_NP_DBG1(Value, mod, modname, Fmt, Arg) \
        NpDbgPrintTime(MCAST_NP_DBG_FLAG); \
 UtlSysTrcLog(SYSLOG_INVAL_LEVEL, Value, MCAST_NP_DBG_NPAPI, "PIM", "PIM-NP", Fmt, Arg)

#define MCAST_NP_DBG2(Value, mod, modname, Fmt, Arg1, Arg2) \
        NpDbgPrintTime(MCAST_NP_DBG_FLAG); \
 UtlSysTrcLog(SYSLOG_INVAL_LEVEL, Value, MCAST_NP_DBG_NPAPI, "PIM", "PIM-NP", Fmt, Arg1, Arg2)

#define MCAST_NP_DBG3(Value, mod, modname, Fmt, Arg1, Arg2, Arg3) \
 NpDbgPrintTime(MCAST_NP_DBG_FLAG); \
 UtlSysTrcLog(SYSLOG_INVAL_LEVEL, Value, MCAST_NP_DBG_NPAPI, "PIM", "PIM-NP", Fmt, Arg1, Arg2, Arg3)

#define MCAST_NP_DBG4(Value, mod, modname, Fmt, Arg1, Arg2, Arg3, Arg4) \
        NpDbgPrintTime(MCAST_NP_DBG_FLAG); \
 UtlSysTrcLog(SYSLOG_INVAL_LEVEL, Value, MCAST_NP_DBG_NPAPI, "PIM", "PIM-NP", Fmt, Arg1, Arg2, Arg3, Arg4)

#define MCAST_NP_DBG5(Value, mod, modname, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5) \
        NpDbgPrintTime(MCAST_NP_DBG_FLAG); \
 UtlSysTrcLog(SYSLOG_INVAL_LEVEL, Value, MCAST_NP_DBG_NPAPI, "PIM", "PIM-NP", Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)

#define MCAST_NP_DBG6(Value, mod, modname, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6) \
        NpDbgPrintTime(MCAST_NP_DBG_FLAG); \
 UtlSysTrcLog(SYSLOG_INVAL_LEVEL, Value, MCAST_NP_DBG_NPAPI, "PIM", "PIM-NP", Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)
#else
#define MCAST_NP_DBG(Value, mod, modname, Fmt)

#define MCAST_NP_DBG1(Value, mod, modname, Fmt, Arg)

#define MCAST_NP_DBG2(Value, mod, modname, Fmt, Arg1, Arg2)

#define MCAST_NP_DBG3(Value, mod, modname, Fmt, Arg1, Arg2, Arg3)

#define MCAST_NP_DBG4(Value, mod, modname, Fmt, Arg1, Arg2, Arg3, Arg4)

#define MCAST_NP_DBG5(Value, mod, modname, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)

#define MCAST_NP_DBG6(Value, mod, modname, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)
#endif

#define MCAST_NP_DBG_ALL             0xffffffff
#define MCAST_NP_DBG_ERROR           0x00000001
#define MCAST_NP_DBG_FAILURE         0x00000002
#define MCAST_NP_DBG_BUF_IF          0x00000004
#define MCAST_NP_DBG_RX              0x00000008
#define MCAST_NP_DBG_TX              0x00000010
#define MCAST_NP_DBG_CTRL_FLOW       0x00000020
#define MCAST_NP_DBG_ENTRY           0x00000040
#define MCAST_NP_DBG_EXIT            0x00000080
#define MCAST_NP_DBG_SNMP_IF         0x00000100
#define MCAST_NP_DBG_PARAMS          0x00000200
#define MCAST_NP_DBG_PKT_EXTRACT     0x00000400
#define MCAST_NP_DBG_CTRL_IF         0x00000800
#define MCAST_NP_DBG_STATUS_IF       0x00001000
#define MCAST_NP_DBG_MEM_IF          0x00002000
#define MCAST_NP_DBG_TMR_IF          0x00004000
#define MCAST_NP_DBG_IF              0x00008000
#define MCAST_NP_DBG_MEM             0x00010000
#define MCAST_NP_DBG_INIT_SHUTDN     0x00020000
#define MCAST_NP_DBG_NPAPI           0x00040000
#endif

/* Function Prototype for Generic NP OpCode specific wrappers
 * defined in Ipmcnpapi.c */

#ifdef FS_NPAPI
UINT1 IpmcFsNpIpv4McInit PROTO ((VOID));
UINT1 IpmcFsNpIpv4McDeInit PROTO ((VOID));
UINT1 IpmcFsNpIpv4VlanMcInit PROTO ((UINT4 u4VlanId));
UINT1 IpmcFsNpIpv4VlanMcDeInit PROTO ((UINT4 u4Index));
UINT1 IpmcFsNpIpv4RportMcInit PROTO ((tRPortInfo PortInfo));
UINT1 IpmcFsNpIpv4RportMcDeInit PROTO ((tRPortInfo PortInfo));
UINT1 IpmcFsNpIpv4McAddRouteEntry PROTO ((UINT4 u4VrId, UINT4 u4GrpAddr, UINT4 u4GrpPrefix, UINT4 u4SrcIpAddr, UINT4 u4SrcIpPrefix, UINT1 u1CallerId, tMcRtEntry rtEntry, UINT2 u2NoOfDownStreamIf, tMcDownStreamIf * pDownStreamIf));
UINT1 IpmcFsNpIpv4McDelRouteEntry PROTO ((UINT4 u4VrId, UINT4 u4GrpAddr, UINT4 u4GrpPrefix, UINT4 u4SrcIpAddr, UINT4 u4SrcIpPrefix, tMcRtEntry rtEntry, UINT2 u2NoOfDownStreamIf, tMcDownStreamIf * pDownStreamIf));
UINT1 IpmcFsNpIpv4McClearAllRoutes PROTO ((UINT4 u4VrId));
UINT1 IpmcFsNpIpv4McAddCpuPort PROTO ((UINT2 u2GenRtrId, UINT4 u4GrpAddr, UINT4 u4SrcAddr, tMcRtEntry rtEntry));
UINT1 IpmcFsNpIpv4McDelCpuPort PROTO ((UINT2 u2GenRtrId, UINT4 u4GrpAddr, UINT4 u4SrcAddr, tMcRtEntry rtEntry));
UINT1 IpmcFsNpIpv4McGetHitStatus PROTO ((UINT4 u4SrcIpAddr, UINT4 u4GrpAddr, UINT4 u4Iif, UINT2 u2VlanId, UINT4 * pu4HitStatus));
UINT1 IpmcFsNpIpv4McUpdateOifVlanEntry PROTO ((UINT4 u4VrId, UINT4 u4GrpAddr, UINT4 u4SrcIpAddr, tMcRtEntry rtEntry, tMcDownStreamIf downStreamIf));
UINT1 IpmcFsNpIpv4McUpdateIifVlanEntry PROTO ((UINT4 u4VrId, UINT4 u4GrpAddr, UINT4 u4SrcIpAddr, tMcRtEntry rtEntry));
UINT1 IpmcFsNpIpv4GetMRouteStats PROTO ((UINT4 u4VrId, UINT4 u4GrpAddr, UINT4 u4SrcAddr, INT4 i4StatType, UINT4 * pu4RetValue));
UINT1 IpmcFsNpIpv4GetMRouteHCStats PROTO ((UINT4 u4VrId, UINT4 u4GrpAddr, UINT4 u4SrcAddr, INT4 i4StatType, tSNMP_COUNTER64_TYPE * pu8RetValue));
UINT1 IpmcFsNpIpv4GetMNextHopStats PROTO ((UINT4 u4VrId, UINT4 u4GrpAddr, UINT4 u4SrcAddr, INT4 i4OutIfIndex, INT4 i4StatType, UINT4 * pu4RetValue));
UINT1 IpmcFsNpIpv4GetMIfaceStats PROTO ((UINT4 u4VrId, INT4 i4IfIndex, INT4 i4StatType, UINT4 * pu4RetValue));
UINT1 IpmcFsNpIpv4GetMIfaceHCStats PROTO ((UINT4 u4VrId, INT4 i4IfIndex, INT4 i4StatType, tSNMP_COUNTER64_TYPE * pu8RetValue));
UINT1 IpmcFsNpIpv4SetMIfaceTtlTreshold PROTO ((UINT4 u4VrId, INT4 i4IfIndex, INT4 i4TtlTreshold));
UINT1 IpmcFsNpIpv4SetMIfaceRateLimit PROTO ((UINT4 u4VrId, INT4 i4IfIndex, INT4 i4RateLimit));
UINT1 IpmcFsNpIpvXHwGetMcastEntry PROTO ((tNpL3McastEntry * pNpL3McastEntry));
#ifdef PIM_WANTED
UINT1 IpmcFsPimNpInitHw PROTO ((VOID));
UINT1 IpmcFsPimNpDeInitHw PROTO ((VOID));
#endif /* PIM_WANTED */
#ifdef DVMRP_WANTED
UINT1 IpmcFsDvmrpNpInitHw PROTO ((VOID));
UINT1 IpmcFsDvmrpNpDeInitHw PROTO ((VOID));
#endif /* DVMRP_WANTED */
#ifdef IGMPPRXY_WANTED
UINT1 IpmcFsNpIgmpProxyInit PROTO ((VOID));
UINT1 IpmcFsNpIgmpProxyDeInit PROTO ((VOID));
#endif /* IGMPPRXY_WANTED */
#ifdef PIM_WANTED
UINT1 IpmcFsNpIpv4McClearHitBit PROTO ((UINT2 u2VlanId, UINT4 u4GrpAddr, UINT4 u4SrcAddr));
#endif
UINT1 IpmcFsNpIpv4McRpfDFInfo PROTO ((tMcRpfDFInfo * pMcRpfDFInfo));
#endif
#ifdef MBSM_WANTED
UINT1 IpmcFsNpIpv4MbsmMcInit PROTO ((tMbsmSlotInfo * pSlotInfo));
UINT1 IpmcFsNpIpv4MbsmMcAddRouteEntry PROTO ((UINT4 u4VrId, UINT4 u4GrpAddr, UINT4 u4GrpPrefix, UINT4 u4SrcIpAddr, UINT4 u4SrcIpPrefix, UINT1 u1CallerId, tMcRtEntry rtEntry, UINT2 u2NoOfDownStreamIf, tMcDownStreamIf * pDownStreamIf, tMbsmSlotInfo * pSlotInfo));
#ifdef PIM_WANTED
UINT1 IpmcFsPimMbsmNpInitHw PROTO ((tMbsmSlotInfo * pSlotInfo));
#endif /* PIM_WANTED */
#ifdef DVMRP_WANTED
UINT1 IpmcFsDvmrpMbsmNpInitHw PROTO ((tMbsmSlotInfo * pSlotInfo));
#endif
#endif /* DVMRP_WANTED */
#endif
