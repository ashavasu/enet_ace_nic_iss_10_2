/********************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 *  $Id: wsssta.h,v 1.45 2017/11/24 10:36:59 siva Exp $
 *
 *  *********************************************************************/

#ifndef __WSSSTA_INC_H__
#define __WSSSTA_INC_H__

#include "wssmac.h"
#include "rsna.h"

#define WSSSTA_SUPP_RATES_LEN 8
#define WSSSTA_PAIRWISE_SC_LEN 6
#define WSSSTA_KEY_LEN 20
#define WSS_STA_WEBUSER_LEN_MAX 20
#define TCP_IPVER_MASK             0xf0

#define MAX_STA_SUPP_PER_AP     MAX_NUM_OF_STA_PER_AP
#define MAX_STA_SUPP_PER_WLC    MAX_NUM_OF_STA_PER_WLC

#define WSSSTA_ASSOC_ID_DEPLETED 0xFFFF
#ifdef BAND_SELECT_WANTED
#define MAX_STA_PER_AP     MAX_STA_SUPP_PER_AP
#define MAX_STA_PER_WLC         32
#endif

#define WSSSTA_WEP_TIMEOUT 1
#define WSSSTA_AUTH_THRESHOLD 30
#define WSSSTA_ASSOC_THRESHOLD 40
#define MAX_SUITE_SUPPORTED     MAX_STA_SUPP_PER_WLC
#define MAX_WSSSTA_WEPPROCESS   MAX_STA_SUPP_PER_WLC
#define MAX_CAPABILITY 15 /* Capability Field */
#define WSSSTA_CLEAR_CONFIG 1
#define WSSSTA_ETHERNET_HEADER_LEN 14
#define WSSSTA_IP_HEADER_CHECKSUM_OFFSET 6
#define WSSSTA_TCP_CHECKSUM_OFFSET 24
#define WSSSTA_IP_TOTAL_LEN 16
#define WSSSTA_TCP_HEADER_LEN 20
#define WSSSTA_DELSTA_LEN 8
#define WSSSTA_ADDSTA_LEN 10
#define WSSSTA_DOT11N_CONFIG_LEN 34
#define WSSSTA_DOT11N_CHANNEL_WIDTH_20 20
#define WSSSTA_DOT11N_CHANNEL_WIDTH_40 40
#define WSSSTA_DOT11N_SHORT_GI_20_ENABLE 1
#define WSSSTA_DOT11N_SHORT_GI_20_DISABLE 0
#define WSSSTA_DOT11N_SHORT_GI_40_ENABLE 1
#define WSSSTA_DOT11N_SHORT_GI_40_DISABLE 0
#define WSSSTA_RADIO_INDEX 1

#define WSSSTA_VLAN_CREATE    1
#define WSSSTA_VLAN_SET       2
#define WSSSTA_VLAN_GET       3
#define WSSSTA_VLAN_DELETE    4
#define WSSSTA_WPA1_MODE       1
#define WSSSTA_WPA2_MODE       2

#define WSS_DOT11_MGMT_TPC_REQ   19
#define WSSSTA_TPC_REQ_SPLIT     4
#define WSSSTA_SHIFT_8           8
/*#define WSS_DOT11_MGMT_CHANL_SWITCH   0x25*/
#define WSS_DOT11_MGMT_CHANL_SWITCH   20
#define WSS_DOT11_MGMT_EX_CHANL_SWITCH   0x3C 
#define WSS_DOT11_MGMT_MEAS_REQ   22
#define WSSSTA_MEAS_REQ_TOKEN   0x01
#define WSSSTA_MEAS_REQ_TYPE    0x00
#define WSSSTA_MEAS_REQ_MODE    0x16
#define WSSSTA_MEAS_REQ_DURATION   0x1388 
/* constants for timer types */
typedef enum {
 WSSSTA_WEP_PROCESS_DB_TMR,
 WSSSTA_MAX_TMRS
}eWssStaTmrId;

typedef struct{
 tTimerListId        WssStaTmrListId;
 tTmrDesc            aWssStaTmrDesc[WSSSTA_MAX_TMRS];
}tWssStaTimerList;

typedef struct _WSSSTA_TIMER {
 tTmrBlk        WssStaTmrBlk;
 UINT1          u1Status;
 UINT1          au1Pad[3];
} tWssStaTmr;

typedef enum {
 UNAUTH_UNASSOC=1,
 AUTH_UNASSOC,
 AUTH_ASSOC
}eWssStaState;


typedef enum {
 WSSSTA_SUCCESSFUL = 0,
 WSSSTA_UNSPECIFIED = 1,
 WSSSTA_BANDSELECT = 2,
 WSSSTA_UNSUP_CAP = 10,
 WSSSTA_REASS_DENIED = 11,
 WSSSTA_ASS_DENIED_UNKNOWN = 12,
 WSSSTA_AUTH_ALG0_UNSUP = 13,
 WSSSTA_AUTH_OUT_OF_SEQ = 14,
 WSSSTA_AUTH_CHALLENGE_FAIL = 15,
 WSSSTA_AUTH_TIMEOUT = 16,
 WSSSTA_ASS_TOO_MANY_MS = 17,
 WSSSTA_DATARATE_UNSUP = 18,
 WSSSTA_SHORTPREAMBLE_UNSUP,
 WSSSTA_PBCC_UNSUP,
 WSSSTA_CHANNELAGILITY_UNSUP,
 WSSSTA_SPECTRUMMGMT_UNSUP,
 WSSSTA_POW_CAPABILITY_UNSUP,
 WSSSTA_CHANNELS_UNSUP = 24,
 WSSSTA_SHORTSLOTTIME_UNSUP = 25,
 WSSSTA_DSSS_OFDM_UNSUP
}eStatusCode;

typedef enum {
 WSSSTA_REASON_UNSPECIFIED = 1,
 WSSSTA_PREV_AUTH_EXPIRED,
 WSSSTA_DEAUTH_MS_LEAVING,
 WSSSTA_DEAUTH_ROUGE,
 WSSSTA_DISASS_INACTIVITY,
 WSSSTA_DISASS_TOO_MANY_MS,
 WSSSTA_NONAUTH_CLASS2,
 WSSSTA_NONASS_CLASS3,
 WSSSTA_DIASS_MS_LEAVING,
 WSSSTA_NONAUTH_ASS_REQUEST
}eReasonCode;

/* Stores the power capability values */
typedef struct {
 UINT1 u1minTransmitPowerCapability;
 UINT1 u1maxTransmitPowerCapability;
 UINT1 au1pad[2];
}tWssStaPowerCapability;

/* Stores the channel range supported by the station */
typedef struct {
 UINT1 u1firstChannelNumber;
 UINT1 u1channelCount;
 UINT1 au1pad[2];
}tWssStaSuppChannels;

/* Stores the RSNA supported by the station */
typedef struct {
 UINT4 u4PairCipherSuite[RSNA_PAIRWISE_CIPHER_COUNT];
 UINT4 u4AKMCipherSuite[RSNA_AKM_SUITE_COUNT];
 UINT4 u4GrpCipherSuite;
 UINT2 u2Version;
 UINT2 u2PairwiseSuiteCount;
 UINT2 u2AKMSuiteCount;
 UINT2 u2RSNACapabilities;
 UINT2 u2PMKidCount;
 UINT1 au1pad[2];
}tWssStaRSNAInfo;
/* Stores traffic specification info for a station */
typedef struct {
 UINT4 u4TsInfo; /* actually 3 bytes*/
 UINT4 u4MinServiceInterval;
 UINT4 u4MaxServiceInterval;
 UINT4 u4InactInterval;
 UINT4 u4SuspInterval;
 UINT4 u4ServStartTime;
 UINT4 u4MinDataRate;
 UINT4 u4MeanDataRate;
 UINT4 u4PeakDataRate;
 UINT4 u4BurstSize;
 UINT4 u4DelayBound;
 UINT4 u4MinPhyRate;
 UINT2 u2SurpBWAllowance;
 UINT2 u2MsduSize;
 UINT2 u2MaxMsduSize;
 UINT2 u2MediumTime;
}tWssStaTspecInfo;


/* The Association parameters provided by the Station */
typedef struct {
 tWssStaPowerCapability powerCapab; /* power capability */
 tWssStaSuppChannels suppChannels;  /* Channels supported */
 tWssStaRSNAInfo  rsnaInfo;         /* station RSNA Info */
 tWssStaTspecInfo tSpecInfo;
 UINT2          u2CapabilityInfo;      /* Capability Info */
 UINT2          u2ListenInterval;      /* listen interval */
 UINT1          u1QosCapability;       /* QoS Capability */
 UINT1          au1pad[3]; 
}tWssStaParams;


typedef struct {
 tWssStaParams   WssstaParams;
 tTmrBlk         wepProcessDBTmr;
 tRBNodeEmbd     nextWssStaWepProcessDB;
 tMacAddr        stationMacAddress;
 tMacAddr        BssIdMacAddr;
 eWssStaState    stationState;
 UINT4           u4ApIpAddress;
 UINT4           u4StationIpAddress;
 UINT4           u4BssIfIndex;
 UINT4           u4WepChallengeTextLen;
 UINT4           u4RsnIeLength;
 UINT4           u4BandWidth;
 UINT4           u4DLBandWidth;
 UINT4           u4ULBandWidth;
 UINT4           u4WpaIeLength;
 UINT2           u2LastSeqNum; /* Sequence number last sent */
 UINT2           u2AssociationID;
 UINT2           u2Sessid;
 UINT1           au1RsnIE[RSNA_ALIGNED_IE_LEN];
 UINT1           u1ThreshCount;
 BOOL1      bStationAssocFlag;
 BOOL1           bRoamStatus;
 UINT1     u1AssocMode;
 UINT1           au1ChallengeText[WSSMAC_MAX_CHALNG_TXT_LEN];
 UINT1           u1StationWmmEnabled;
 BOOL1           bDeAuthDueToWps;
 UINT1           au1WpaIE[WPA_ALIGNED_IE_LEN];
 UINT1  au1Pad[2];
}tWssStaWepProcessDB;

#ifdef BAND_SELECT_WANTED
typedef struct {
 UINT1           au1Ssid[32];
 tRBNodeEmbd     StaBandSteerDBNode;
 tTmrBlk         ProbeEntryTmr;
 tTmrBlk         StaAssocCountTmr;
 tMacAddr        stationMacAddress;
 tMacAddr        BssIdMacAddr;
 UINT1           u1StaStatus;
 UINT1           u1StaAssocCount;
 UINT1           u1SsidLen;
 UINT1           u1RadioId;
 UINT1           u1WlanId;
 UINT1           au1Pad[3];
}tWssStaBandSteerDB;
#endif

typedef enum {
 WSSSTA_ADD_PROCESS_DB=0,
 WSSSTA_DELETE_PROCESS_DB,
 WSSSTA_CREATE_BAND_STEER_DB,
 WSSSTA_GET_BAND_STEER_DB,
 WSSSTA_SET_BAND_STEER_DB,
 WSSSTA_DESTROY_BAND_STEER_DB,
 WSSSTA_GET_FIRST_BAND_STEER_DB,
 WSSSTA_GET_NEXT_BAND_STEER_DB
}eProcessDB;

typedef enum {
 OPEN_AUTHENTICATION = 0x0000,
 SHARED_KEY_AUTHENTICATION = 0x0100
}eWssStaAlgo;

/* Structure to be filled byWLAN/Radio IF module to
 *  * delete a WLAN or WTP profile */
typedef struct{
 UINT4 u4BssIfIndex;
 UINT1 u1MsgType;
 UINT1 au1pad[3];
}tWssStaDelProfile;

typedef struct{
 BOOL1        isPresent;
 UINT1        u1NumStations;
 UINT2        u2MessageType;
 UINT1        u1MessageLength;
 UINT1        u1RadioId;
 UINT1        u1Length;
 UINT1        au1pad[1];
 tMacAddr     stationMacAddress;
 UINT1        au1pad1[2];
}tWssStaDelStation;

typedef enum {
 /* WSS AUTH module */
 WSS_STA_INIT,
 WSS_MAC_MGMT_AUTH_MSG,
 WSS_MAC_MGMT_ASSOC_REQ,
 WSS_MAC_MGMT_DEAUTH_MSG,
 WSS_MAC_MGMT_REASSOC_REQ,
 WSS_MAC_MGMT_DISASSOC_MSG,
 WSS_STA_WLAN_DELETE,
 WSS_STA_WTP_DELETE,
 WSS_STA_IDLE_TIMEOUT,
 WSS_STA_TIMER_EXPIRY,
 WSS_STA_STACONFIG_RESP,
 WSS_STA_DISCOVERY_MODE,
}eWssStaMsgTypes;

/* For Add/delete station message element */
typedef struct {
 BOOL1        isPresent;
 UINT1        au1pad[1];
 UINT2        u2MessageType;
 UINT2        u2MessageLength;
 UINT1        u1RadioId;
 UINT1        u1MacAddrLen;
 tMacAddr     StaMacAddress;
 UINT1           au1pad1[2];
 UINT1        au1VlanName[512];
 UINT2        u2VlanId; 
 UINT1        au1pad2[2];
}tWssStaAddStation;

typedef struct {
 BOOL1        isPresent;
 UINT1        u1StationType;
 UINT2        u2MessageType;
 UINT2        u2MessageLength;
 UINT1        u1RadioId;
 UINT1        u1MacAddrLen;
 tMacAddr     StaMacAddr;
 UINT1        au1pad1[2];
}tWssStaDeleteStation;

typedef struct {
 tMacAddr     stationMacAddr;
 UINT2        u2MsgEleType;
 UINT4        u4VendorId;
 UINT2        u2MsgEleLen;
 UINT2        u2HiSuppDataRate;
 UINT2        u2AMPDUBuffSize;
 UINT1        u1HtFlag;
 UINT1        u1MaxRxFactor;
 UINT1        u1MinStaSpacing;
 UINT1        u1HTCSupp;
 BOOL1        isOptional;
 UINT1        au1Pad[1];
 UINT1        au1MCSRateSet[16];
}tStaDot11nVendorType;

/* For vendor specific payload message element.*/
typedef struct {
 UINT4  u4VendorId;
    UINT4  u4BandwidthThresh;
 UINT2  u2MsgEleType;
 UINT2  u2MsgEleLen;
 UINT2  u2ElementId;
 BOOL1  isOptional;
 UINT1  u1WmmStaFlag;
 tMacAddr   StaMacAddr;
 UINT2   u2TcpPort;
}tWssStaWMMVendor;

typedef struct {
 tStaDot11nVendorType  Dot11nStaVendor;
 tWssStaWMMVendor      WMMStaVendor;
#ifdef PMF_WANTED
 tVendorPMFPktInfo     PMFStaVendor; /*PMF_WANTED*/
#endif
 UINT4  u4VendorId;
 UINT2  u2MsgEleType;
 UINT2  u2MsgEleLen;
 UINT2  u2ElementId;
 BOOL1  isOptional;
 UINT1  u1Pad;
 UINT1  data[2048];/* Should not exceed 2048 */
 tMacAddr   StaMacAddr;
 UINT1  au1Pad[2];
 UINT4 u4BandWidth; 
 UINT4 u4DLBandWidth; 
 UINT4 u4ULBandWidth; 
}tWssStaVendSpecPayload;

typedef struct{
 tMacAddr    stationMacAddress;
 UINT1       au1pad[2];
}tWssStaIdleTimeout;

/* For IEEE 802.11 Station message element */
/* This message element is sent ot AP when a
 *  *  *  * station gets associated
 *   *   *   */
typedef struct {
 UINT2         u2MessageType;
 UINT2         u2MessageLength;
 UINT2         u2AssocId;
 UINT2         u2Capab;
 UINT1         au1SuppRates[WSSSTA_SUPP_RATES_LEN];
 tMacAddr      stationMacAddress;
 UINT1         u1RadioId;
 UINT1         u1Flags;
 UINT1         u1WlanId;
 BOOL1         isPresent;
 UINT1         au1pad[2];
}tWssStaDot11Station;

/* For 802.11 Station Session Key message element */
/* message should not be sent without IEEE 802.11 Station message */
typedef struct {
 tMacAddr     stationMacAddress;
 tRsnaIEElements   RsnaIEElements;
 UINT2        u2MessageType;
 UINT1        au1PairwiseTSC[WSSSTA_PAIRWISE_SC_LEN];
 UINT2        u2MessageLength;
 UINT1        au1PairwiseRSC[WSSSTA_PAIRWISE_SC_LEN];
 UINT2        u2Flags;
 UINT1        au1Key[256];
 BOOL1        isPresent;
 UINT1         au1pad[3];
}tWssStaDot11Sesskey;

typedef struct {
 tMacAddr     stationMacAddr;
 UINT2        u2MessageType;
 UINT4        u4VendorId;
 UINT2        u2MessageLength;
 UINT2        u2HiSuppDataRate;
 UINT2        u2AMPDUBuffSize;
 UINT1        u1HtFlag;
 UINT1        u1MaxRxFactor;
 UINT1        u1MinStaSpacing;
 UINT1        u1HTCSupp;
 BOOL1        isPresent;
 UINT1        u1pad;
 UINT1        au1ManMCSSet[16]; 
}tWssStaDot11nConfigInfo;


/* For 802.11 Station QoS Profile */
typedef struct {
 UINT2        u2MessageType;
 UINT2        u2MessageLength;
 tMacAddr    stationMacAddress;
 UINT1        u180211Priority;
 BOOL1        isPresent;
}tWssStaDot11QoSProfile;
/* For 802.11 Update Station QoS */
typedef struct {
 UINT2        u2MessageType;
 UINT2        u2QoSElement;
 UINT2        u2MessageLength;
 UINT1        u1RadioId;
 BOOL1        isPresent;
 tMacAddr    stationMacAddress;
 UINT1         au1pad[2];
}tWssStaDot11UpdateStaQoS;

typedef struct {
 tMacAddr     stationMacAddress;
 UINT1        u1WebAuthCompStatus;
 BOOL1        isPresent;
}tWssStaWebAuth;

typedef struct{
 tWssStaAddStation        AddSta;
 tWssStaDeleteStation     DelSta;
 tWssStaVendSpecPayload   VendSpec;
 tWssStaDot11Station      StaMsg;
 tWssStaDot11Sesskey      StaSessKey;
 tWssStaDot11QoSProfile   StaQosProfile;
 tWssStaDot11UpdateStaQoS StaQosUpdate;
 tWssStaWebAuth           WebAuthStatus;
 UINT2                    u2SessId;
 UINT1                    au1pad[2];
 UINT4                    Resultcode;
}tWssStaConfigReqInfo;

/* This Struct union contains the message structures for all tWssMsgTypes */
typedef struct{
 union {
  /* 802.11 Mac Frames */
  tDot11AuthMacFrame          WssMacAuthMacFrame;
  tDot11DeauthMacFrame        WssMacDeauthMacFrame;
  tDot11AssocReqMacFrame      WssMacAssocReqMacFrame;
  tDot11ReassocReqMacFrame    WssMacReassocReqMacFrame;
  tDot11DisassocMacFrame      WssMacDisassocMacFrame;
  /* Called by the Wlan/RadioIf module */
  tWssStaDelProfile        AuthDelProfile;
  /* Called by the WLC HDLR */
  tWssStaDeleteStation        AuthDelStation;
  /* Should be added for Auth Resp Timer expiry */
  /* Should be added for Assoc wait timer expiry */
  tWssStaConfigReqInfo        WssStaConfigReqInfo;
 }unAuthMsg;
}tWssStaMsgStruct;

typedef struct {
 UINT2          u2ACFlag;
 UINT2          u2CapabilityInfo;      /* Capability Info */
 UINT1          au1VlanName[512];
 UINT1          au1Key[256];
 UINT1          au1SuppRates[8];
 UINT1          au1PairwiseTSC[6];
 UINT1          u18021Priority;
 UINT1          u1DscpTag;
 UINT1          au1PairwiseRSC[6];
 UINT1          au1Pad[2];
}tWssStaWtpParams;

/* Stores the station's authentication and association status */
typedef struct {
 tWssStaWtpParams  StaParams;
 tRsnaIEElements   RsnaIEElements;
 tRBNodeEmbd       nextWssStaStateDB;
 tMacAddr          stationMacAddress;
 UINT2             u2AssociationID;
 UINT4             u4IpAddr;
 UINT4             u4BandWidth;
 UINT4             u4DLBandWidth;
 UINT4             u4ULBandWidth;
    UINT4             u4BandwidthThresh;
 UINT2             u2SessId;
 UINT2             u2VlanId; 
  UINT2            u2TcpPort;
 UINT1             au1MCSRateSet[16];
 UINT1             u1RadioId;
 UINT1             u1WlanId;
 UINT1             u1HtFlag;
 UINT1       u1WmmFlag;
 BOOL1             staActiveFlag;
 UINT1           u1DialogToken;
 UINT1             u1WebAuthCompStatus;
 INT1              i1LinkMargin;
 UINT1             u1MeasReqDialogToken;
 UINT1             u1Pad;
}tWssStaStateDB;

/* Stores the VLAN related information in case of local routing */
typedef struct {
    tRBNodeEmbd       nextWssStaVlanDB;
    UINT2             u2VlanId;
    BOOL1             IpCreateFlag;
    BOOL1             AddStaFlag;
    tMacAddr          BssId;
    UINT1             au1IntfName [24];
    UINT1             u1WlanId;
    UINT1             au1Pad[1];
}tWssStaVlanDB;

typedef enum{
 WSS_WTP_STA_INIT = 0,
 WSS_WTP_STA_PKT_RCVD,
 WSS_WTP_STA_CONFIG_REQUEST_AP,
 WSS_WTP_STA_IDLE_TIMEOUT,
 WSS_STA_GET_DB,
 WSS_STA_VALIDATE_DB,
 WSS_STA_DEAUTH_MSG,
 WSS_CONSTRUCT_TPC_REQUEST,
 WSS_CONSTRUCT_MEAS_REQUEST
}eWssMsgTypes;

typedef struct{
 tMacAddr   StaMacAddr;
 UINT1          au1Pad[2];
}tWssStaActiveStatus;

typedef struct{
 tMacAddr    BssId;
 UINT1       u1RadioId;
 UINT1       u1WlanId;
}tWssStaDeauthMsg;

typedef struct{
 UINT1       u1RadioId;
 UINT1       u1TpcRequestSlot;
 UINT1       au1Pad[2];
}tWssSta11hTpcMsg;

typedef union {
 tWssStaConfigReqInfo         StaInfoAp;
 /* Called by APHDLR whenever the idle timer expires */
 tWssStaIdleTimeout           StaIdleTimeExpiry;
 tWssStaActiveStatus          StaActiveInfo;
 tWssStaStateDB               WssStaStateDB;
 tWssStaDeauthMsg             WssStaDeauthMsg;
 tWssSta11hTpcMsg             WssSta11hTpcMsg;
}unWssMsgStructs;

typedef struct {
 tMacAddr   staMacAddr;
 UINT1          au1Pad[2];
}tWssStaDBWalkMac;

typedef struct {
 tWssStaDeleteStation     deletestation;
 UINT2                    u2SessId;
 UINT1          au1Pad[2];
}tStationWtpEventReq;

typedef struct {
 tMacAddr   staMacAddr;
 UINT1          au1Pad[2];
}tWssStaWepProcessDBWalk;

INT4 WssIfProcessWssStaMsg PROTO ((UINT4, tWssStaMsgStruct *));

UINT4 WssStaProcessWssIfMsg  PROTO ((eWssStaMsgTypes ,
   tWssStaMsgStruct *));
UINT4
WssStaGetClientMac PROTO ((tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
                                      void *pArg, void *pOut));

UINT4 WssStaWtpProcessWssIfMsg PROTO ((eWssMsgTypes, unWssMsgStructs *));
INT4 WssIfProcessWssStaWtpMsg PROTO ((UINT4, unWssMsgStructs *));
INT4 WssStaWebAuthRecvMainTaskInit(VOID);

INT4 WssStaWebAuthProcessTask(VOID);
UINT4
WssStaSendRsnaSessionKey PROTO ((tMacAddr staMacAddr,  UINT4 u4BssIfIndex, UINT1 u1KeyType));


#ifdef PMF_WANTED
UINT4
WssStaSendSAQueryResponseMsg PROTO ((tMacAddr staMacAddr, UINT4 u4BssIfIndex,
                                     UINT2 u2TransactionId, UINT2 u2SessId));

UINT4
WssStaSendSAQueryReqMsg PROTO ((tMacAddr staMacAddr, UINT4 u4BssIfIndex,
                                UINT2 u2TransactionId, UINT2 u2SessId));
#endif

UINT4
WssStaDeleteRsnaSessionKey PROTO ((tMacAddr staMacAddr,  UINT4 u4BssIfIndex));
UINT4
WssStaSendBandWidth PROTO ((tMacAddr staMacAddrm,UINT4 u4BandWidth, 
   UINT4 u4DLBandWidth, UINT4 u4ULBandWidth));
UINT1
WSSUserRoleConfigBandwidthForIP PROTO ((tMacAddr StationMacAddr,UINT4 u4IpAddr, UINT4 u4BandWidth,
                    UINT4 u4DLBandWidth, UINT4 u4ULBandWidth,
                    UINT1 *pu1InterfaceName,UINT1 u1DeleteFlag));
INT4 
WssRsnaSendDeAuthMsg PROTO((UINT1 * pu1BssId,UINT1 *pu1StaMac));
UINT1
WssIfDeauthenticationStations PROTO ((UINT4 u4BssIfIndex,UINT1 * pu1Bssid));
UINT1 
WSSUserRoleConfigBandwidth PROTO ((tMacAddr StationMacAddr, UINT4 u4BandWidth,
   UINT4 u4DLBandWidth, UINT4 u4ULBandWidth, 
   UINT1 *pu1InterfaceName, UINT1 u1DeleteFlag));

VOID
WssIfWmmQueueCreation(UINT1 *pu1IntfName);

VOID
WssIfWmmQueueConfiguration PROTO ((UINT1 *pu1InterfaceName));

/* For Web Authentication */

typedef struct{
 UINT4      u4WlanIfIndex;
 UINT4      u4WebAuthUsrIP;
 UINT1      au1WebAuthUName[24];
 UINT1      au1WebAuthUserEmail[128];

}tWssStaWebAuthDB;

/* constants for timer types */
typedef enum {
 WSSSTA_WEBAUTH_TMR =0,
 WSSSTA_WEBAUTH_MAX_TMRS
}eWssStaWebAuthTmrId;

typedef struct{
 tTimerListId        WssStaWebAuthTmrListId;
 tTmrDesc            aWssStaWebAuthTmrDesc[WSSSTA_WEBAUTH_MAX_TMRS];
}tWssStaWebAuthTimerList;

typedef struct {
 UINT4          u4BssIfIndex;
 tMacAddr       staMacAddr;
 UINT1          au1Pad[2];
 eWssStaState   stationState;
}tWssClientSummary;

typedef struct {
 tMacAddr   staMacAddr;
 UINT1      au1Pad[2];
}tWssStaWtpDBWalk;

typedef struct {
    tMacAddr   aStaMacAddr[MAX_STA_SUPP_PER_AP];
    UINT1      u1Count;
    UINT1      u1Pad;
}tWssStaClientWalk;

#ifdef WTP_WANTED
UINT4 WssStaSetStaActiveFlag PROTO ((tWssStaActiveStatus));
#endif

INT4 WssStaProcessTCPPacket PROTO ((tCRU_BUF_CHAIN_HEADER *));

INT4  WssStaWebAuthGetUser(tSNMP_OCTET_STRING_TYPE *);

tWssStaWepProcessDB * WssStaProcessEntryGet PROTO((tMacAddr));

#endif
