/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: l2iwf.h,v 1.147 2018/01/11 11:18:53 siva Exp $
 *
 * Description: This file contains all the function prototypes
 *              exported bythe L2IWF Module.
 *
 *******************************************************************/

#ifndef _L2IWF_H_
#define _L2IWF_H_

#include "pbb.h"
#include "fssnmp.h"

#define L2IWF_SUCCESS   SUCCESS
#define L2IWF_FAILURE   FAILURE

#define L2IWF_INGRESS  2 
#define L2IWF_EGRESS   1

#define L2IWF_RESET    0
#define L2IWF_SET      1

#define L2IWF_NUM_DEFAULT_VLAN 1

#define L2IWF_DEFAULT_CONTEXT 0
#define L2IWF_CONTEXT_ALIAS_LEN      VCM_ALIAS_MAX_LEN

#define L2IWF_REQ_NEXT_INSTANCE_ID     0
#define L2IWF_REQ_INSTANCE_STATUS      1
#define L2IWF_REQ_INSTANCE_MAP_COUNT   2

#define L2IWF_VLAN_LIST_SIZE (((VLAN_DEV_MAX_VLAN_ID + 31)/32) * 4) 

#define L2IWF_GET_TIMESTAMP(pu4Time) \
        *pu4Time = OsixGetSysUpTime () * SYS_TIME_TICKS_IN_A_SEC


#define L2IWF_MAX_CONTEXTS           SYS_DEF_MAX_NUM_CONTEXTS
#define L2IWF_MAX_PORTS_PER_CONTEXT  SYS_DEF_MAX_PORTS_PER_CONTEXT

/* the following macro has added when implementing pseudo wire interfaces.
 * In future, further interfaces can be added in the interface list with 
 * physical + LA ports + PW interfaces + AC interfaces.
 */
#define L2IWF_MAX_PORTS_PER_CONTEXT_EXT (L2IWF_MAX_PORTS_PER_CONTEXT + \
                                         SYS_DEF_MAX_L2_PSW_IFACES + \
                                         SYS_DEF_MAX_L2_AC_IFACES + \
                                         SYS_DEF_MAX_NVE_IFACES + \
                                         MAX_TAP_INTERFACES + \
                                           SYS_DEF_MAX_SBP_IFACES )
  
#define L2IWF_BRG_MAX_PHY_PLUS_LOG_PORTS  BRG_MAX_PHY_PLUS_LOG_PORTS
#define L2IWF_BRG_MAX_PHY_PLUS_LAG_INT_PORTS  BRG_MAX_PHY_PLUS_LAG_INT_PORTS
#define L2MI_SYNC_TAKE_SEM()        L2MiSyncTakeSem () 
#define L2MI_SYNC_GIVE_SEM()        L2MiSyncGiveSem ()

#define L2_PROTO_MAX           23

#define L2_PROTO_CHECK(u2Protocol)     (((u2Protocol != L2_PROTO_DOT1X) && (u2Protocol != L2_PROTO_LACP) && \
                                       (u2Protocol != L2_PROTO_STP) && (u2Protocol != L2_PROTO_GVRP) && \
                                       (u2Protocol != L2_PROTO_GMRP) && (u2Protocol != L2_PROTO_IGMP) && \
                                       (u2Protocol != L2_PROTO_MVRP) && (u2Protocol != L2_PROTO_MMRP) && \
                                       (u2Protocol != L2_PROTO_ELMI) && (u2Protocol != L2_PROTO_LLDP) && \
                                       (u2Protocol != L2_PROTO_ECFM) && (u2Protocol != L2_PROTO_EOAM) && \
                                       (u2Protocol != L2_PROTO_PTP) && (u2Protocol != L2_PROTO_OTHER))\
                                       ?L2IWF_TRUE:L2IWF_FALSE)

#define MEMBER_PORT     1
#define FORBIDDEN_PORT  2
#define NON_MEMBER_PORT 3

typedef struct L2IwfFdbInfo
{
    UINT4 u4ContextId;
    UINT1 au1VlanList[L2IWF_VLAN_LIST_SIZE];
    UINT1 au1FdbList[L2IWF_VLAN_LIST_SIZE];
    UINT2 u2FdbCount;
    UINT1 au1Reserved[2];
}tL2IwfFdbInfo;

enum {
   L2_PROTO_DOT1X=1,
   L2_PROTO_LACP,
   L2_PROTO_STP,
   L2_PROTO_GVRP,
   L2_PROTO_GMRP,
   L2_PROTO_IGMP,
   L2_PROTO_MVRP,
   L2_PROTO_MMRP,
   L2_PROTO_EOAM,
   L2_PROTO_ELMI,
   L2_PROTO_LLDP,
   L2_PROTO_ECFM,
   L2_PROTO_PTP,
   L2_PROTO_OTHER /* Macro added for any new protocols */
};

enum {
    L2IWF_PROTOCOL_ID_PNAC=1,
    L2IWF_PROTOCOL_ID_LA,
    L2IWF_PROTOCOL_ID_XSTP,
    L2IWF_PROTOCOL_ID_VLAN,
    L2IWF_PROTOCOL_ID_GARP,
    L2IWF_PROTOCOL_ID_MRP,
    L2IWF_PROTOCOL_ID_PBB,
    L2IWF_PROTOCOL_ID_ECFM,
    L2IWF_PROTOCOL_ID_ELMI,
    L2IWF_PROTOCOL_ID_SNOOP,
    L2IWF_PROTOCOL_ID_LLDP,
    L2IWF_PROTOCOL_ID_BRIDGE,
    L2IWF_PROTOCOL_ID_QOS,
    L2IWF_PROTOCOL_ID_MAX
};

enum {
    L2IWF_CFA_GET_RS=1,
    L2IWF_CFA_GET_NEXT,
    L2IWF_CFA_SET_RS_CRT,
    L2IWF_CFA_SET_RS_DES,
    L2IWF_CFA_CRT_PORT,
    L2IWF_CFA_DEL_PORT,
    L2IWF_CFA_MAX_ACTION
};

typedef UINT1 tIfTypeDenyProtocolList [L2IWF_PROTOCOL_ID_MAX];

typedef struct L2IwfCfaIfTypeDenyProt
{
    tCfaIfInfo         *pIfInfo;
    UINT4              *pu4NextContextId;
    INT4               *pi4NextIfType;
    INT4               *pi4NextBrgPortType;
    INT4               *pi4NextProtocol;
    INT4               *pi4RowStatus;
    UINT4               u4IfIndex;
    UINT4               u4ContextId;
    UINT4               u4Action;
    INT4                i4IfType;
    INT4                i4BrgPortType;
    INT4                i4Protocol;
    INT4                i4RowStatus;
} tL2IwfCfaIfTypeDenyProt;

typedef struct L2NextPortInfo
{
    UINT4              *pu4NextIfIndex;
                               /* Next IfIndex that can be created in
                                  the module */
    UINT2              *pu2NextLocalPort;
                               /* Next Local Port that can be created 
                                  in the module */
    UINT4               u4ContextId;
                               /* Context Identifier */
    UINT4               u4ModuleId;
                               /* Module Identifier - Module Id which is 
                                  calling L2IWF to get next port */
    UINT2               u2LocalPortId;
                               /* Local Port from the Module */
    UINT1               au1Reserved[2];
}tL2NextPortInfo;

typedef struct LocVlanNameInfo
{
    UINT2      u2VlanId;      /* INDEX: VLAN Id */
    UINT1      au1VlanName[VLAN_STATIC_MAX_NAME_LEN];
    UINT1      u1VlanNameTxEnable; /* Transmission enable or disabled*/
    UINT1      u1Pad;
}tVlanNameInfo;

typedef struct _sStpInstanceInfo
{
    UINT2      u2NextValidInstance;
    UINT2      u2MappingCount; /* number of vlans mapped to this instance */
    UINT1      u1InstanceStatus; /* possible values are 
                                    OSIX_TRUE - if instance is active
                                    OSIX_FALSE - if instance in inactive
                                    */
    UINT1      u1RequestFlag; 
                  /* For passing the data request type. possible
                     values are -
                     L2IWF_REQ_INSTANCE_STATUS - u1InstanceStatus will be
                                                 filled up and returned
                     L2IWF_REQ_INSTANCE_MAP_COUNT - u2MappingCount will be
                                                filled up and returned.
                     if no request is set then by default u2NextValidInstance
                     will be return to maintain the backward compatibility
                   */
    UINT1      au1Reserved[2];
}tStpInstanceInfo;


/* This enum is used to specify of the type of VLAN */
typedef enum
{
    L2_VLAN = 0,   /* By default, the value will be configured as L2_VLAN*/
    FCOE_VLAN      /* This value will be set by FSB module*/
} eFCoEVlanType;


#define L2IWF_802_1AD_BRIDGE() \
   (((L2IWF_BRIDGE_MODE()==L2IWF_PROVIDER_EDGE_BRIDGE_MODE) ||\
    (L2IWF_BRIDGE_MODE()==L2IWF_PROVIDER_CORE_BRIDGE_MODE))?L2IWF_TRUE:L2IWF_FALSE)
   
#define L2IWF_ENABLED   1
#define L2IWF_DISABLED  2

#define L2IWF_TRUE      1
#define L2IWF_FALSE     2
#define L2IWF_PBB_FALSE 3 

#define L2IWF_INIT_VAL 0


#define L2IWF_MAX_CVLAN_CONTEXT    AST_MAX_CEP_IN_PEB

#define L2IWF_PROVIDER_BRIDGE_MODE           VLAN_PROVIDER_BRIDGE_MODE
#define L2IWF_CUSTOMER_BRIDGE_MODE           VLAN_CUSTOMER_BRIDGE_MODE
#define L2IWF_PROVIDER_EDGE_BRIDGE_MODE      VLAN_PROVIDER_EDGE_BRIDGE_MODE
#define L2IWF_PROVIDER_CORE_BRIDGE_MODE      VLAN_PROVIDER_CORE_BRIDGE_MODE
#define L2IWF_PBB_ICOMPONENT_BRIDGE_MODE     VLAN_PBB_ICOMPONENT_BRIDGE_MODE
#define L2IWF_PBB_BCOMPONENT_BRIDGE_MODE     VLAN_PBB_BCOMPONENT_BRIDGE_MODE
#define L2IWF_INVALID_BRIDGE_MODE            VLAN_INVALID_BRIDGE_MODE

#ifdef PB_WANTED
#define L2IWF_DEFAULT_BRIDGE_MODE          L2IWF_INVALID_BRIDGE_MODE
#else
#define L2IWF_DEFAULT_BRIDGE_MODE          L2IWF_CUSTOMER_BRIDGE_MODE
#endif


#define L2IWF_PROVIDER_NETWORK_PORT        CFA_PROVIDER_NETWORK_PORT
#define L2IWF_CNP_PORTBASED_PORT           CFA_CNP_PORTBASED_PORT
#define L2IWF_CNP_STAGGED_PORT             CFA_CNP_STAGGED_PORT
#define L2IWF_CUSTOMER_EDGE_PORT           CFA_CUSTOMER_EDGE_PORT
#define L2IWF_PROP_CUSTOMER_EDGE_PORT      CFA_PROP_CUSTOMER_EDGE_PORT
#define L2IWF_PROP_CUSTOMER_NETWORK_PORT   CFA_PROP_CUSTOMER_NETWORK_PORT
#define L2IWF_PROP_PROVIDER_NETWORK_PORT   CFA_PROP_PROVIDER_NETWORK_PORT
#define L2IWF_CUSTOMER_BRIDGE_PORT         CFA_CUSTOMER_BRIDGE_PORT
#define L2IWF_CNP_CTAGGED_PORT             CFA_CNP_CTAGGED_PORT 
#define L2IWF_VIRTUAL_INSTANCE_PORT        CFA_VIRTUAL_INSTANCE_PORT
#define L2IWF_PROVIDER_INSTANCE_PORT       CFA_PROVIDER_INSTANCE_PORT
#define L2IWF_CUSTOMER_BACKBONE_PORT       CFA_CUSTOMER_BACKBONE_PORT
#define L2IWF_PROVIDER_EDGE_PORT           CFA_PROVIDER_EDGE_PORT
#define L2IWF_STATION_FACING_BRIDGE_PORT   CFA_STATION_FACING_BRIDGE_PORT
#define L2IWF_INVALID_PROVIDER_PORT        CFA_INVALID_BRIDGE_PORT

#define L2IWF_I_COMPONENT                  VLAN_I_COMPONENT
#define L2IWF_B_COMPONENT                  VLAN_B_COMPONENT
#define L2IWF_INVALID_COMPONENT            VLAN_INVALID_COMPONENT

#define L2IWF_C_INTERFACE_TYPE             VLAN_C_INTERFACE_TYPE
#define L2IWF_S_INTERFACE_TYPE             VLAN_S_INTERFACE_TYPE
#define L2IWF_INVALID_INTERFACE_TYPE       VLAN_INVALID_INTERFACE_TYPE

#define L2IWF_ACTIVE                    VLAN_ACTIVE          
#define L2IWF_NOT_IN_SERVICE            VLAN_NOT_IN_SERVICE   
#define L2IWF_NOT_READY                 VLAN_NOT_READY        
#define L2IWF_CREATE_AND_GO             VLAN_CREATE_AND_GO    
#define L2IWF_CREATE_AND_WAIT           VLAN_CREATE_AND_WAIT  
#define L2IWF_DESTROY                   VLAN_DESTROY          


#define  L2_ENET_IP              0x800
#define  L2_MVRP_PROTOCOL_ID      0x21
#define  L2_MMRP_PROTOCOL_ID      0x20
#define  L2_GVRP_PROTOCOL_ID      0x21
#define  L2_GMRP_PROTOCOL_ID      0x20
#define  L2_UDP_CONTROL_PKT      0x11
#define  L2_IGMP_CONTROL_PKT     2

#define  L2_MVRP_ENET_TYPE      0x88f5
#define  L2_MMRP_ENET_TYPE      0x88f6



#define   L2_INVALID_PKT         1 /* Invalid Packet. To be dropped */
#define   L2_NOT_BPDU_PKT        2 /* Not a BPDU  Packet */
#define   L2_STAP_PKT            3 /* STP Packet */
#define   L2_GVRP_PKT            4
#define   L2_GMRP_PKT            5
#define   L2_DATA_PKT            6
#define   L2_ECFM_PACKET         7
#define   L2_ELMI_PKT            8 /* ELMI Packet */
#define   L2_PNAC_CTRL_FRAME     9
#define   L2_LA_CTRL_FRAME       10
#define   L2_ECFM_DATA_PACKET    11
#define   L2_MVRP_PKT            12
#define   L2_MMRP_PKT            13
#define   L2_LLDP_PKT            14
#define   L2_EOAM_PKT            15

#define  L2_DATA_LINK_HDR_LEN    14
#define  L2_ETH_TYPE_OFFSET      12
#define  L2IWF_BIT8              0x80
#define  L2IWF_UINT1_ALL_ONE     0xFF

#define L2IWF_ECFM_MAX_PDU_SIZE       1522
 
#define L2_UNTAGGED_MEMBER_PORT 0
#define L2_TAGGED_MEMBER_PORT   1
#define L2_MEMBER_PORT          2

#define L2IWF_TYPE_CONFIG        1
#define L2IWF_PRIMARY_VID_CONFIG 2

/* LLDP Application details - Start */

/* LLDP Application macros */

#define L2IWF_LLDP_MAX_LEN_OUI         3

#define L2IWF_NORMAL_VLAN   1
#define L2IWF_PRIMARY_VLAN   2
#define L2IWF_ISOLATED_VLAN   3
#define L2IWF_COMMUNITY_VLAN   4
#define L2IWF_INVALID_VLAN_TYPE   5

#define L2IWF_VLAN_TYPE           1
#define L2IWF_MAPPED_VLANS        2
#define    L2IWF_NUM_MAPPED_VLANS 3

/* Request types posted by LLDP applications to L2IWF */
enum {
    L2IWF_LLDP_APPL_REGISTER         = 1, /* Request to register application 
                                             in L2IWF module data base */
    L2IWF_LLDP_APPL_DEREGISTER       = 2, /* Request to de-register application
                                             from L2IWF module database */
    L2IWF_LLDP_APPL_PORT_REGISTER    = 3, /* Request to register application 
                                             on an LLDP port */
    L2IWF_LLDP_APPL_PORT_UPDATE      = 4, /* Request to update application data
                                             on an LLDP port */
    L2IWF_LLDP_APPL_PORT_DEREGISTER  = 5  /* Request to de-register application
                                             from an LLDP port */
};

/* Message types posted to applications from LLDP */
enum {
    L2IWF_LLDP_APPL_TLV_RECV        = 1, /* Message type used by LLDP when TLV is 
                                            posted to an application.*/
    L2IWF_LLDP_APPL_TLV_AGED        = 2, /* Message type used by LLDP when an
                                          * application TLV is aged-out in LLDP
                                          * module */
    L2IWF_LLDP_APPL_RE_REG          = 3, /* Message type posted to applications
                                          * when LLDP module is starting */
    L2IWF_LLDP_APPL_PDU_RECV        = 4, /* Message type used by LLDP when PDU is
                                          * posted to an application */
 L2IWF_LLDP_APPL_NEIGH_EXIST_IND = 5, /* Message type used by LLDP when info 
                                          * from existing neighbour arrives at 
                                          * switch via LLDP PDU */
    L2IWF_LLDP_TX_ENABLE_NOTIFY     = 6,
    L2IWF_LLDP_RX_ENABLE_NOTIFY     = 7,
    L2IWF_LLDP_TX_DISABLE_NOTIFY    = 8,
    L2IWF_LLDP_RX_DISABLE_NOTIFY    = 9,
    L2IWF_LLDP_MULTIPLE_PEER_NOTIFY = 10, /* To send LLDP multiple peer notification
                                          * to DCBX*/
    L2IWF_LLDP_APPL_MAX_MSG         = 11
};

/* Message types posted to LLDP from applications */
enum {
 LLDP_BOTH        = 0, /* Message type used by application to register for both Tx & Rx */
 LLDP_TX          = 1, /* Message type used by application to register for only Tx */
 LLDP_RX          = 2, /* Message type used by application to register for only Rx*/
};

/* LLDP - Application Id
 * Application ID is the unique key assigned to every application
 * running on top of LLDP. It consists of following 4 fields 
 * derived from the application's TLV and registration type.
 * 1. TLV Type - Type of TLV
 * 2. OUI - Organizational Unique Identifier, if used by application
 * 3. SubType - TLV Sub Type 
 * 4. RegType - Registraion type for unique Rx/Tx/Rx-Tx registrations
 *              Set LLDP_BOTH for Rx-Tx, LLDP_TX for Tx alone, 
 *              LLDP_RX for Rx alone.
 */ 
typedef struct _LldpAppId
{
    UINT1    au1OUI[L2IWF_LLDP_MAX_LEN_OUI];
    UINT1    u1SubType;    
    UINT2    u2TlvType;
    UINT1    u1RegType;
    UINT1    au1Padding[1];
}tLldpAppId;


/* LLDP Msg to applications - Argument for application call back functions */
typedef struct _LldpAppTlv
{
    tLldpAppId LldpAppId;         /* Application Id */
    UINT1    *pu1RxAppTlv;        /* TLV received in the pkt buffer.
                                   * Appln should copy it to its memory */
    UINT4    u4MsgType;           /* Event to application */
    UINT4    u4RxAppTlvPortId;    /* TLV received port */
    INT4     i4RemIndex;          /* Remote Peer Id on which the TLV is 
                                   * received */
    UINT4    u4RxAppTlvTimeStamp; /* TLV received time */
    UINT4    u4RemLocalDestMACIndex;
                                  /* Destination MAC index for this TLV*/
    tMacAddr RemLocalDestMacAddr; /* Destination MAC for this TLV */
    tMacAddr RemNodeMacAddr;      /* Source MAC of the remote Node */
    UINT1    *pu1RxAppPdu;        /**Pointer to the received LLDP PDU **/
    UINT2    u2RxAppTlvLen;       /* Application can validate their TLV
                                   * lengths using this object */
    UINT2    u2RxAppPduLen;       /**LLDP PDU length**/
    UINT1    u1RxFrameType;       /**Frame type**/
    UINT1    au1Padding[3];
}tLldpAppTlv;

/* Applications uses this structure to interface with LLDP */
typedef struct _LldpAppPortMsg
{
    tLldpAppId  LldpAppId;          /* Application Id */
    VOID        (*pAppCallBackFn) (tLldpAppTlv *); /*Application callback fn*/
    UINT1       (*pAppCallBackTakeTlvFrmAppFn) (UINT1 *pu1AppendTlv, UINT2 *u2TlvMaxLen, UINT4 u4LocPortNum, UINT1 u1FrameType); /*Call back fn to be called for the application to append its TLV.s to the Tx.ed LLDP PDU*/
    UINT1       *pu1TxAppTlv;       /* Application TLV to be sent in LLDP PDUs */
    UINT4       u4LldpInstSelector; /* LLDP instance to use */ 
    UINT2       u2TxAppTlvLen;      /* Application TX TLV Length */
    BOOL1       bAppTlvTxStatus;    /* Flag to indicate whether the given 
                                     * application TLV should be part of the 
                                     * LLDP PDU or not */
    UINT1       u1AppTlvRxStatus;   /* Flag to indicate whether the given
                                     * application TLV Rx should be indicated to the
                                     * application or not. The value must be OSIX_TRUE 
                                     * to indicate application's TLV Rx. To Disable the 
                                     * indication of application TLV Rx set value as
                                     * OSIX_FALSE */
    UINT1       u1AppPDURecv;       /* To be turned on when app wants the whole recvd LLDP PDU*/
    UINT1       u1TakeTlvFrmApp;    /* To be turned on when the application itself can append its tlv to the LLDP PDU to be transmitted  */
    UINT1       u1AppRefreshFrameNotify; /*To be turned on if the refresh frame also needs to be notified to the registered application  */
    UINT1       u1AppNeighborExistenceNotify; /* To be turned on when app needs to know whenever an info from existing neighbour arrives at the switch*/
    UINT1       au1LldpApplSpecMac[MAC_ADDR_LEN]; /* Application specific Mac
                                                   * address which is being
                                                   * registered with LLDP */
    UINT1       au1Reserved[2]; /* padding */
}tLldpAppPortMsg;

/* Application information maintained in L2IWF database */
typedef struct _L2LldpAppInfo
{
    tLldpAppId     LldpAppId;
    VOID           (*pAppCallBackFn) (tLldpAppTlv *); 
                       /* Application callback fn */
}tL2LldpAppInfo;

typedef struct L2FrameBuf
{
 UINT1       au1Frame[L2IWF_ECFM_MAX_PDU_SIZE];
} tL2FrameBuf;

/* APIs provided by L2IWF for LLDP applications */
PUBLIC INT4
L2IwfLldpHandleApplRequest PROTO ((tL2LldpAppInfo *pL2LldpAppInfo,
                                   UINT1 u1LldpApplRequest));
PUBLIC INT4 
L2IwfLldpHandleApplPortRequest PROTO ((UINT4 u4IfIndex,
                                       tLldpAppPortMsg *pLldpAppPortMsg, 
                                       UINT1 u1LldpApplRequest)); 

/* API provided by L2IWF for LLDP */
PUBLIC VOID
L2IwfLldpPostApplReReg PROTO ((VOID));

PUBLIC INT4
L2IwfLldpApiGetInstanceId(UINT1 *pu1MacAddr,UINT4 *pu4InstanceId);

PUBLIC INT4
L2IwfLldpApiSetDstMac(INT4 i4UapIfIndex, UINT1 *pu1MacAddr,
                                                   UINT1 u1RowStatus);

/* LLDP Application details - End */

#ifdef MI_WANTED
#define L2IWF_PORT_LIST_SIZE              CONTEXT_PORT_LIST_SIZE
#else
#define L2IWF_PORT_LIST_SIZE              BRG_PORT_LIST_SIZE
#endif

#ifdef NPAPI_WANTED
/* Uncomment this if VLAN based port state set is necessary. */
#define MSTP_USE_INSTANCE_PORT_STATE

typedef struct MstInstPortStateInfo {
   UINT2 u2Inst;
   UINT1 u1PortState;
   UINT1 u1Pad; /* FS_DELTA - added newly */
} tMstInstPortStateInfo;

typedef struct MstVlanPortStateInfo {
   UINT2 u2VlanId;
   UINT1 u1PortState;
   UINT1 u1Reserved[1];
} tMstVlanPortStateInfo;

#endif

/* Vid translation table related information exchanged between 
 * L2IWF and vlan garp. */
typedef struct _VidTransEntryInfo {
    UINT2      u2Port;
    tVlanId    LocalVid;
    tVlanId    RelayVid;
    UINT1      u1RowStatus;
    UINT1      u1Reserved[1];
}tVidTransEntryInfo;

typedef struct _L2IwfVcmContextInfo {
    UINT4      u4ContextId;
    UINT2      u2LocalPortId;
    UINT1      au1Pad[2];
}tL2IwfContextInfo;

/* This structure is used to fill the following info while 
 * indicating the following from L2IWF / STP 
 *  - MRP_PORT_ROLE_CHG_MSG
 *  - MRP_PORT_OPER_P2P_MSG
 *  - MRP_TCDETECTED_TMR_STATUS */
typedef struct MrpInfo {
    UINT4 u4IfIndex;
               /* Interface Index */
    UINT2 u2MapId;
               /* Map Identifier */
    UINT1 u1OldRole;
               /* Old Port role that is updating from STP */ 
    UINT1 u1NewRole;
               /* New Port role that is updating from STP */ 
    BOOL1 b1TruthVal;
               /* Truth Value that is passed from STP. It 
                * will convey the following 
                * - PortOperP2PChg
                * - TcDetectedTmrState */
    UINT1 u1OperStatus;
               /* OperStatus used by the port Oper Indication from 
                * L2iwf */
    UINT1 u1PortState;
               /* This is used by STP to pass the port state when there 
                * is a change in topology. This can be
                * - AST_PORT_STATE_FORWARDING
                * - AST_PORT_STATE_DISCARDING */
    UINT1 u1Flag;
               /* This is used to identify the following 
                * - MRP_PORT_ROLE_CHG_MSG
                * - MRP_PORT_OPER_P2P_MSG
                * - MRP_TCDETECTED_TMR_STATUS
                * - (MRP_STAP_PORT_FWD_MSG ||
                *    MRP_STAP_PORT_BLK_MSG ) */
} tMrpInfo;

/* PVLAN structures */
typedef struct _tL2PvlanMappingInfo {   

    UINT4               u4ContextId;
    tVlanId             *pMappedVlans; 
    UINT2               u2NumMappedVlans; 
    tVlanId             InVlanId; 
    UINT1               u1VlanType; 
    UINT1               u1RequestType; 
    UINT1               au1Reserved[2]; 
} tL2PvlanMappingInfo;

#define L2_LOCK()  L2Lock ()
#define L2_UNLOCK()  L2Unlock ()

#define L2IWF_LOCK_REQ(bLockReq) if(bLockReq) {L2Lock ();}
#define L2IWF_UNLOCK_REQ(bLockReq)  if(bLockReq) {L2Unlock ();}

#define L2_DB_LOCK()  L2DBLock ()
#define L2_DB_UNLOCK()  L2DBUnlock ()

#define L2_SYNC_TAKE_SEM()       L2SyncTakeSem () 
#define L2_SYNC_GIVE_SEM()       L2SyncGiveSem ()

#define L2_DHCP_SERVER_PORT      67
#define L2_DHCP_CLIENT_PORT      68
#define L2_UDP_PROTOCOL          17
#define L2_IP_HDRLEN_BITMASK     0x0f
#define L2_BYTE_IN_WORD          4

/*****    APIs to maintain L2 Common database     ********/

INT4 L2Lock (VOID);
INT4 L2Unlock (VOID);
INT4 L2DBLock (VOID);
INT4 L2DBUnlock (VOID);

/* To be called from PNAC */
INT4 L2IwfSetPortPnacAuthControl (UINT4 u4IfIndex, UINT2 u2PortAuthControl);
INT4 L2IwfSetPortPnacAuthStatus (UINT4 u4IfIndex, UINT2 u2PortAuthStatus);
INT4 L2IwfSetPortPnacControlDir (UINT4 u4IfIndex, UINT2 u2PortControlDir);
INT4 L2IwfSetPortPnacAuthMode (UINT4 u4IfIndex, UINT2 u2PortAuthMode);
INT4 L2IwfGetPortPnacAuthMode (UINT4 u4IfIndex, UINT2 *pu2PortAuthMode);
INT4 L2IwfSetPortPnacPaeStatus (UINT4 u4IfIndex, UINT1 u1PortPaeStatus);
INT4 L2IwfGetPortPnacPaeStatus (UINT4 u4IfIndex, UINT1 *pu1PortPaeStatus);
INT4 L2IwfSetSuppMacAuthStatus (tMacAddr SuppMacAddr, UINT2 u2MacAuthStatus);

/* To be called from LA */
INT4 L2IwfDeleteAndAddPortToPortChannel (UINT4 u4IfIndex, UINT2 u2AggId);
INT4 L2IwfRemovePortFromPortChannel (UINT4 u4IfIndex, UINT2 u2AggId);
INT4 L2IwfAddActivePortToPortChannel (UINT4 u4IfIndex, UINT2 u2AggId);
INT4 L2IwfRemoveActivePortFromPortChannel (UINT4 u4IfIndex, UINT2 u2AggId);
INT4 L2IwfDeleteAllPortsFromTrunk (UINT2 u2AggId);
VOID L2IwfSetPortChannelValid (UINT4 u4AggId, UINT1 u1Status);
VOID L2IwfSetPortChannelConfigured (UINT4 u4AggId, UINT1 u1Status);

/* To be called from STP */
INT4 L2IwfAllocInstPortStateMem (UINT4 u4ContextId, UINT2 u2LocalPortId, UINT1);
INT4 L2IwfAllocInstActiveStateMem (UINT4 u4ContextId, UINT1 u1Mode);
INT4 L2IwfSetInstPortState (UINT2 u2MstInst, UINT4 u4IfIndex, 
                            UINT1 u1PortState);
INT4 L2IwfMiSetInstPortState (UINT4 u4ContextId, UINT2 u2MstInst,
                              UINT2 u2LocalPortId, UINT1 u1PortState);
INT4 L2IwfSetVlanInstMapping (UINT4 u4ContextId, tVlanId VlanId,
                              UINT2 u2MstInst);
INT4 L2IwfSetPortOperEdgeStatus (UINT4 u4ContextId, UINT2 u2LocalPortId,
                                 BOOL1 bOperEdge);
INT4 L2IwfVlanToInstMappingInit (UINT4 u4ContextId);
INT4 L2IwfCreateSpanningTreeInstance (UINT4 u4ContextId, UINT2 u2InstanceId);
INT4 L2IwfDeleteSpanningTreeInstance (UINT4 u4ContextId, UINT2 u2InstanceId);
INT4 L2IwfFillConfigDigest (UINT4 u4ContextId, UINT1 au1DigPtr[],
                            INT4 i4NumEntries);
VOID
L2IwfCalculateBuddyMemPoolAdjust(UINT4 u4MinBlockSize,UINT4 u4MaxBlockSize,
                                 UINT4 *pu4MinBlkAdjust,UINT4 *pu4MaxBlkAdjust );

/* To be called from SNOOPING */
INT4 L2IwfIsIgmpTunnelEnabledOnAnyPort (UINT4 u4ContextId);

/* To be called from VLAN */
INT4 L2IwfSetPortVlanTunnelStatus (UINT4 u4ContextId, UINT2 u2LocalPortId,
                                   BOOL1 bIsTunnelPort);
INT4 L2IwfCreateVlan (UINT4 u4ContextId, tVlanId VlanId);
INT4 L2IwfDeleteVlan (UINT4 u4ContextId, tVlanId VlanId);
INT4 L2IwfDeleteAllVlans (UINT4 u4ContextId);
INT4 L2IwfSetVlanFdbId (UINT4 u4ContextId, tVlanId VlanId, UINT4 u4FdbId);
INT4 L2IwfUpdateVlanEgressPortList (UINT4 u4ContextId, tVlanId VlanId, 
                                    UINT1 *pu1VlanName, tLocalPortList AddedPorts, 
                                    tLocalPortList DeletedPorts);
INT4
L2IwfGetUntaggedPortList (UINT4 u4ContextId, tVlanId VlanId,
                          tLocalPortList LocalEgressPorts);
INT4 L2IwfUpdateVlanUntagPortList (UINT4 u4ContextId, tVlanId VlanId,
                               UINT1 *pu1VlanName, tLocalPortList AddedPorts);
INT4 L2IwfSetVlanEgressPort (UINT4 u4ContextId, tVlanId VlanId,
                             UINT1 *pu1VlanName, UINT2 u2LocalPortId, UINT1 u1IsUntagged);
INT4 L2IwfResetVlanEgressPort (UINT4 u4ContextId, tVlanId VlanId,
                               UINT2 u2LocalPortId);
INT4 L2IwfUpdateEnhFilterStatus (UINT4 u4ContextId, BOOL1 b1EnhFilterStatus);

INT4 L2IwfSetPerformanceDataStatus (UINT4 u4ContextId, BOOL1 b1PerfDataStatus);
INT4 L2IwfGetPerformanceDataStatus (UINT4 u4ContextId, BOOL1 *pbPerfDataStatus);

INT4 L2IwfUpdateVlanName (UINT4 u4ContextId, UINT2 u2VlanId, UINT1 *pVlanName);
INT4 L2IwfValidatePortVlanTableIndex (UINT4 u4IfIndex, UINT2 u2VlanId);

INT4 L2IwfGetFirstPortVlanTableIndex (UINT4 *pu4IfIndex, UINT2 *pu2VlanId);
INT4 L2IwfGetNextPortVlanTableIndex (UINT4 u4IfIndex, 
                                     UINT4 *pi4NextIfIndex,
                                     UINT2 u2VlanId,
                                     UINT2 *pu2NextVlanId);
INT4 L2IwfGetVlanName (UINT4 u4IfIndex, UINT2 u2VlanId, UINT1 *pu1VlanName);
INT4 L2IwfGetAllVlanNameIfTxEnabled (UINT4 u4IfIndex,
                                     tVlanNameInfo *pVlanNameInfo,
                          UINT2 *pVlanCount);
INT4 L2IwfGetVlanNameTxStatus (UINT4 u4IfIndex, UINT2 u2VlanId, 
                               UINT1 *pu1VlanNameTxStatus);
INT4 L2IwfSetVlanNameTxStatus (UINT4 u4IfIndex, UINT2 u2VlanId,
                               UINT1 u1VlanNameTxStatus, 
                               UINT1 *pu1ConfTxEnable);
INT4 L2IwfSetVlanPortType (UINT4 u4ContextId, UINT2 u2LocalPortId, 
                           UINT1 u1PortType);
INT4 L2IwfSetVlanPortAccpFrmType (UINT4 u4ContextId, UINT2 u2LocalPortId, 
                           UINT1 u1PortAccpFrmType);
INT4 L2IwfSetVlanPortPvid (UINT4 u4ContextId, UINT2 u2LocalPortId, 
                           tVlanId VlanId);
INT4 L2IwfIsIgmpControlPacket (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pu1Result);

INT4 L2IwfSetProtocolEnabledStatusOnPort (UINT4 u4ContextId, 
                                          UINT2 u2LocalPortId, UINT2 u2Protocol, 
                                          UINT1 u1Status);
INT4 L2IwfGetProtocolEnabledStatusOnPort (UINT4 u4IfIndex, UINT2 u2Protocol, 
                                          UINT1 *pu1Status);
INT4 L2IwfChangeBridgeModeChange (UINT4 u4ContextId);
INT4
L2IwfConfigPvlanMapping (UINT4 u4ContextId, tVlanId VlanId, UINT1 u1PvlanType,
                         tVlanId PrimaryVlanId, UINT1 u1ConfigType);
VOID
L2IwfGetPVlanMappingInfo (tL2PvlanMappingInfo *pL2PvlanMappingInfo);

INT4
L2IwfDeletePvlanMapping (UINT4 u4ContextId, tVlanId VlanId);
INT4
L2IwfConfigPvlanType (UINT4 u4ContextId, tVlanId VlanId, UINT1 u1PvlanType);

/*****    APIs to access L2 Common database     ********/

/* General Data */
INT4 L2IwfGetNextValidPort (UINT4 u4IfIndex, UINT2 *pu2NextPort);
INT4 L2IwfGetNextValidPortForContext (UINT4 u4ContextId, UINT2 u2HLPortIndex,
                                      UINT2 *pu2NextHLPort, 
                                      UINT4 *pu4NextIfIndex);

INT4 L2IwfGetNextValidPortForProtoCxt (tL2NextPortInfo 
                                       *pL2NextPortInfo);

INT4 L2IwfGetPortOperStatus (INT4 i4ModuleId, UINT4 u4IfIndex,
                             UINT1 *pu1OperStatus);
INT4 L2IwfIsPortMappedToContext (UINT4 u4IfIndex);
VOID L2IwfConvToLocalPortList (tPortList IfPortList,
                               tLocalPortList LocalPortList);

/* To be called from GARP */
INT4  L2IwfSetVlanPortGvrpStatus (UINT4 u4ContextId, UINT2 u2LocalPortId,
                                  UINT1 u1GvrpStatus);


INT4 L2IwfGetBridgeMode (UINT4 u4ContextId, UINT4 *pu4BridgeMode);
INT4 L2IwfIsDot1xConfigAllowed (UINT4 u4IfIndex);

INT4 L2IwfSetBridgeMode (UINT4 u4ContextId, UINT4  u4BridgeMode);
INT4
L2IwfGetPbPortType (UINT4 u4IfIndex, UINT1 *pu1PortType);

INT4 L2IwfGetNextCVlanPort(UINT4 u4CepIfIndex, UINT2 u2PrevPort, 
                           UINT2  *pu2NextPort, UINT4 *pu4NextCepIfIndex);

INT4 L2IwfGetPbPortOperStatus(UINT1 u1Module, UINT4 u4CepIfIndex, tVlanId SVlanId,
                             UINT1 *pu1OperStatus);
                             
UINT1 L2IwfGetPbPortState (UINT4 u4CepIfIndex, tVlanId SVlanId);
INT4  L2IwfSetPbPortState (UINT4 u4CepIfIndex, tVlanId SVlanId, UINT1 u1PortState);
INT4 L2IwfPbSetInstCepPortState (UINT4 u4IfIndex, UINT1 u1PortState);


INT4 L2IwfGetPbPortOperEdgeStatus (UINT4 u4CepIfIndex, tVlanId SVlanId, BOOL1 *pbOperEdge);
INT4 L2IwfSetPbPortOperEdgeStatus(UINT4 u4CepIfIndex, tVlanId SVlanId, BOOL1 bOperEdge);

INT4 L2IwfPbGetVlanServiceType(UINT4 u4ContextId, tVlanId VlanId, UINT1 *pu1ServiceType);
INT4 L2IwfPbSetVlanServiceType(UINT4 u4ContextId, tVlanId VlanId, UINT1 u1ServiceType);
INT4 L2IwfSendCustBpduOnPep (UINT4 u4CepIfIndex, tVlanId SVlanId,
                        tCRU_BUF_CHAIN_DESC *pBuf, UINT4 u4PktSize);

INT4 L2IwfGetInterfaceType (UINT4 u4ContextId, UINT1 *pu1InterfaceType);
INT4 L2IwfSetInterfaceType (UINT4 u4ContextId, UINT1 u1InterfaceType);
INT4 L2IwfSetAdminIntfTypeFlag (UINT4 u4ContextId, UINT1 u1IsAdminIntfType);
INT4 L2IwfGetAdminIntfTypeFlag (UINT4 u4ContextId, UINT1 *pu1IsAdminIntfType);
INT4 L2IwfGetInternalPortCount (UINT4 u4ContextId, UINT4 *pu4Count);
INT4 L2IwfGetCnpPortCount (UINT4 u4ContextId, UINT4 *pu4Count);

/* PNAC data */
INT4 L2IwfGetPortPnacAuthControl (UINT4 u4IfIndex, UINT2 *pu2PortAuthControl);
INT4 L2IwfGetPortPnacAuthStatus  (UINT4 u4IfIndex, UINT2 *pu2PortAuthStatus);
INT4 L2IwfGetPortPnacControlDir (UINT4 u4IfIndex, UINT2 *pu2PortControlDir);
INT4 L2IwfIsPnacCtrlFrame (tCRU_BUF_CHAIN_DESC *pFrame);
INT4 L2IwfIsPortAuthorized(UINT4 u4IfIndex);
INT4 L2IwfIsSrcMacAuthorized (UINT1 *pSrcMacAddr);

/* LA data */
INT4 L2IwfGetConfiguredPortsForPortChannel (UINT2 u2AggId, UINT2 au2ConfPorts[],
                                            UINT2 *pu2NumPorts);
INT4 L2IwfGetActivePortsForPortChannel (UINT2 u2AggId, UINT2 au2ActivePorts[],
                                            UINT2 *pu2NumPorts);
INT4 L2IwfGetPortChannelForPort  (UINT4 u4IfIndex, UINT2 *pu2AggId);
INT4 L2IwfIsPortInPortChannel (UINT4 u4IfIndex);
INT4 L2IwfIsLaCtrlFrame (tCRU_BUF_CHAIN_DESC *pFrame, tMacAddr DestMacAddr, UINT4 u4IfIndex);
INT4 L2IwfIsEoamCtrlFrame (tCRU_BUF_CHAIN_DESC * pFrame, UINT4 *pu4LogicalIfIndex);
INT4 L2IwfIsPortActiveInPortChannel (UINT4 u4IfIndex, UINT2 u2AggId); 
INT4 LaGetPortChannelIndex (UINT2 u2PortIndex, UINT2 *pu2AggIndex);
    
INT4 L2IwfL3LAGPortOperIndication (UINT4 u4IfIndex, UINT1 u1OperStatus);
INT4 L2IwfGetIndexForPortChannel (UINT2 u2AggId , UINT4 u4Index);

/* Spanning tree data */
UINT1 L2IwfGetInstPortState (UINT2 u2MstInst, UINT4 u4IfIndex);
UINT1 L2IwfGetVlanPortState (tVlanId VlanId, UINT4 u4IfIndex);
UINT2 L2IwfMiGetVlanInstMapping (UINT4 u4ContextId, tVlanId VlanId);
INT4 L2IwfGetPortOperEdgeStatus (UINT4 u4IfIndex, BOOL1 *pbOperEdge);
INT4 L2IwfMrpApiNotifyStpInfo (tMrpInfo *pMrpInfo);
/* Backward compatibility */
UINT2 L2IwfGetVlanInstMapping (tVlanId VlanId);

/* VLAN data - All port indexes should be Global IfIndexes */
UINT4 L2IwfMiGetVlanFdbId          (UINT4 u4ContextId, tVlanId VlanId);
BOOL1 L2IwfMiIsTunnelPortPresent   (UINT4 u4ContextId);
BOOL1 L2IwfMiIsVlanMemberPort      (UINT4 u4ContextId, tVlanId VlanId,
                                    UINT4 u4IfIndex);
UINT2 L2IwfMiGetVlanMapInInstance (UINT4 u4ContextId, UINT2 u2MstInst,
                                   UINT1 *pu1VlanList);

BOOL1 L2IwfMiIsVlanUntagMemberPort (UINT4 u4ContextId, tVlanId VlanId, 
                                    UINT4 u4IfIndex);
BOOL1 L2IwfMiIsVlanActive          (UINT4 u4ContextId, tVlanId VlanId);
INT4  L2IwfMiGetNextActiveVlan     (UINT4 u4ContextId, UINT2 u2VlanId,
                                    UINT2 *pu2NextVlanId);
UINT2 L2IwfMiGetVlanListInInstance (UINT4 u4ContextId, UINT2 u2MstInst, 
                                    UINT1 *pu1VlanList);
INT4  L2IwfGetPortVlanTunnelStatus (UINT4 u4IfIndex,
                                    BOOL1 *pbIsTunnelPort);
INT4  L2IwfGetVlanPortType         (UINT4 u4Port, UINT1 *pPortType);
INT4  L2IwfGetVlanPortAccpFrmType  (UINT4 u4Port, UINT1 *pPortAccpFrmType);
INT4  L2IwfGetVlanPortPvid         (UINT4 u4Port, tVlanId *pVlanId);
INT4  L2IwfMiGetVlanIdFromFdbId (UINT4 u4ContextId, UINT4 u4FdbId,
                                 tVlanId * pVlanId);
INT4  L2IwfMiGetVlanEgressPorts    (UINT4 u4ContextId, tVlanId VlanId,
                                    tPortList EgressPorts);
UINT4 L2iwfValidatePortType (UINT4 u4BridgeMode, UINT1 u1BrgPortType);

/* This following port list alone is based on HL port IDs */
INT4  L2IwfMiGetVlanLocalEgressPorts  (UINT4 u4ContextId, tVlanId VlanId,
                                    tLocalPortList EgressPorts);
INT4 L2IwfMiGetVlanUnTagPorts (UINT4 u4ContextId, tVlanId VlanId,
                          tPortList UnTagPorts);
INT4 L2IwfIsPvidVlanOfAnyPorts (UINT4 u4ContextId, tVlanId VlanId);
/* Backward compatibilty */
INT4  L2IwfGetNextActiveVlan (UINT2 u2VlanId, UINT2 *pu2NextVlanId);
UINT2 L2IwfGetVlanListInInstance (UINT2 u2MstInst, UINT1 *pu1VlanList);
BOOL1 L2IwfIsTunnelPortPresent (VOID);
BOOL1 L2IwfIsVlanActive (tVlanId VlanId);
UINT4  L2IwfGetVlanFdbId (tVlanId VlanId);
INT4  L2IwfGetVlanEgressPorts (tVlanId VlanId, tPortList EgressPorts);
INT4 L2IwfGetPortIgmpTunnelStatus (UINT2 u2PortIndex, BOOL1 *pbTunnelPort);
INT4 L2IwfSetProviderBridgePortType (UINT2 u2Port, UINT1 u1PortType);
INT4 L2IwfGetProviderBridgePortType (UINT2 u2Port, UINT1* pu1PortType);

/* GARP data */
INT4 L2IwfGetVlanPortGvrpStatus (UINT4 u4IfIndex, UINT1 *pu1GvrpStatus);

/*MRP */

INT4 L2IwfGetVlanPortMvrpStatus (UINT4 u4IfIndex, UINT1 *pu1MvrpStatus);

/* Provider bridge related prototypes used by vlan and garp. */
INT4 L2IwfPbGetPbPortType (UINT4 u4IfIndex, UINT2  *pu2PortType);


/* Vlan Translation status related functions. */

INT4 L2IwfPbGetVidTransStatus (UINT4 u4ContextId, UINT2 u2LocalPort, 
                 UINT1 *pu1Status);
INT4 L2IwfPbSetVidTransStatus (UINT4 u4ContextId, UINT2 u2LocalPortId, 
                  UINT1 u1Status);
INT4
L2IwfPbGetLocalVidListFromVIDTransTable(UINT4 u4IfIndex,UINT1 *pu1LocalVidList);

/* Vid translation table related functions. */
INT4 L2IwfPbCreateVidTransTable (UINT4 u4ContextId);
INT4 L2IwfPbDeleteVidTransTable (UINT4 u4ContextId);
INT4 L2IwfPbSetPortType (UINT4 u4IfIndex, INT4 i4PortType);

INT4 L2IwfPbSetPcPortTypeToPhyPort (UINT4 u4PcIfIndex, UINT4 u4PhyIfIndex);
INT4 L2IwfPbSetDefPortType (UINT4 u4IfIndex);

INT4 L2IwfPbGvrpEnabledStausOnPort (UINT2 u2Port);
INT4 L2IwfPbCreateProviderEdgePort PROTO((UINT4 u4IfIndex,
                                          tVlanId SVlanId));
INT4 L2IwfPbDeleteProviderEdgePort PROTO((UINT4 u4IfIndex, tVlanId SVlanId));

INT4 L2IwfPbPepDeleteIndication PROTO((UINT4 u4IfIndex, tVlanId SVlanId));

VOID  L2IwfPbPepOperStatusIndication PROTO((UINT2 u2Port, tVlanId SVlanId, 
                                     UINT1 u1OperStatus));
VOID  L2IwfPbUpdatePepOperStatus (UINT4 u4IfIndex, tVlanId SVlanId,
                                  UINT1 u1OperStatus);
INT4 L2IwfSetProtocolTunnelStatusOnPort (UINT4 u4ContextId, UINT2 u2Port,
                                           UINT2 u2Protocol,
                                           UINT1 u1TunnelStatus);
INT4 L2IwfSetProtocolTunnelStatusPerVlan (UINT4 u4ContextId, UINT4 u4VlanId,
                                           UINT2 u2Protocol,
                                           UINT1 u1TunnelStatus);
VOID L2IwfGetProtocolTunnelStatusOnPort (UINT4 u4IfIndex,
                                         UINT2 u2Protocol,
                                         UINT1 *pu1TunnelStatus);
VOID L2IwfGetProtocolTunnelStatusPerVlan (UINT4 u4ContextId, UINT4 u4VlanId,
                                         UINT2 u2Protocol,
                                         UINT1 *pu1TunnelStatus);
INT4 L2IwfSetOverrideOption (UINT4 u4Port, UINT1 u1OverrideOption);

VOID L2IwfGetOverrideOption (UINT4 u4Port, UINT1 *pu1OverrideOption);

INT4 L2IwfGetL2ProtocolTunnelStatus (UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId VlanId,
                                UINT4 u4ProtocolId, UINT1 *pu1TunnelStatus,
                                UINT1 *pu1L2CPProtType);
INT4 L2IwfSetTunnelStatusPerVlan (UINT4 u4ContextId, UINT4 u4VlanId, UINT1 u1TunnelStatus);

INT4 L2IwfGetL2CPProtcolType (UINT4 u4ProtocolId, UINT1 *pu1L2CPProtType);

VOID L2IwfPbPepHandleInCustBpdu (tCRU_BUF_CHAIN_DESC * pBuf, UINT4 u4IfIndex,
                            tVlanId SVlanId);

INT4 L2IwfPbGetVidTransEntry (UINT4 u4ContextId, UINT2 u2LocalPort, 
                              tVlanId SearchVid, UINT1 u1IsLocalVid,
                              tVidTransEntryInfo *pOutVidTransEntry);

INT4 L2IwfPbGetNextVidTransTblEntry (UINT4 u4ContextId, UINT2 u2Port,
                                     tVlanId LocalVid, 
                                     tVidTransEntryInfo *pOutVidTransEntryInfo);

INT4 L2IwfPbConfigVidTransEntry (UINT4 u4ContextId, 
                  tVidTransEntryInfo VidTransEntryInfo);

INT4 L2IwfPbGetLocalVidFromRelayVid (UINT4 u4ContextId, UINT2 u2LocalPort,
                                     tVlanId RelayVid, tVlanId *pLocalVid);

INT4 L2IwfPbGetRelayVidFromLocalVid (UINT4 u4ContextId, UINT2 u2LocalPort, 
                                    tVlanId LocalVid, tVlanId *pRelayVid);

/******   APIs to route indications from one module to another  ******/

/* L2 -> CFA */
INT4 L2IwfHandleOutgoingPktOnPort (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                                   UINT4 u4PktSize, UINT2 u2Protocol,
                                   UINT1 u1EncapType);
INT4 L2IwfHandleOutgoingPktOnPortList (tCRU_BUF_CHAIN_HEADER * pBuf,
                                       tPortList PortList,
                                       UINT4 u4PktSize, UINT2 u2Protocol,
                                       UINT1 u1EncapType);

INT4 L2IwfHandleOutgoinPktOnBackPlane (tCRU_BUF_CHAIN_HEADER * pBuf,
           tCfaBackPlaneParams * pBackPlaneParams,
           UINT4  u4IfIndex, UINT4  u4PktSize,
           UINT2 u2Protocol, UINT1 u1EncapType);
/* CFA -> L2 */
INT4 L2IwfHandleIncomingLayer2Pkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex, 
                                   UINT1 u1FrameType);

INT4 L2IwfWrHandleIncomingLayer2Pkt (tCRU_BUF_CHAIN_HEADER * pBuf, tPktHandleInfo * pPktHandleInfo,
                                     UINT1 u1FrameType);
VOID L2IwfPortCreateIndication (UINT4 u4IfIndex);
VOID L2IwfPortDeleteIndication (UINT4 u4IfIndex);
VOID L2IwfPortDelIndication (UINT4 u4IfIndex, UINT1 u1Status);
VOID L2IwfIntfTypeChangeIndication (UINT4 u4IfIndex, UINT1 u1IntfType);
VOID L2IwfPortOperStatusIndication (UINT4 u4IfIndex, UINT1 u1Status);
INT4 L2IwfSetBridgePortOperStatus (UINT4 u4IfIndex, UINT1 u1BridgeOperStatus);
INT4 L2IwfGetBridgePortOperStatus (UINT4 u4IfIndex, 
                                   UINT1 *pu1BridgeOperStatus);
INT4 L2IwfGetEoamTunnelStatus (UINT4 u4IfIndex, UINT1 *pu1EoamTunnelStatus);
INT4 L2IwfPbSetDefBrgPortTypeInCfa (UINT4 u4IfIndex, UINT4 u4BridgeMode, 
                                    UINT1 u1BrgModePortType, UINT1 u1IsCnpCheck);

INT4 L2IwfPreprocessOutgoingFrameFromCfa (tCRU_BUF_CHAIN_HEADER *pBuf, 
                                          UINT4 u4PktLen, UINT4 u4IfIndex, 
                                          UINT2 u2Protocol, UINT1 u1EncapType, 
                                          UINT4 *pu4EgressPort);

INT4 L2IwfWrPreprocessOutgoingFrameFromCfa (tCRU_BUF_CHAIN_HEADER *pBuf, 
                                            UINT4 u4PktLen, UINT4 u4IfIndex, 
                                            UINT2 u2Protocol, UINT1 u1EncapType, 
                                            UINT4 *pu4EgressPort, UINT1 u1IfType);

INT4 L2IwfGetPotentialTxPortForAgg (UINT2 u2AggId, UINT2 *pu2PhyPort);
VOID L2CfaRedSetBridgePortOperStatus (UINT4 u4IfIndex, UINT1 u1OperStatus);

INT4
L2IwfHandleIncominPktOnBackPlane (tCRU_BUF_CHAIN_HEADER * pBuf,
      tCfaBackPlaneParams * pBackPlaneParams, 
      UINT4 u4IfIndex, UINT1  u1FrameType);
/* PNAC -> L2 */
INT4 L2IwfPnacHLPortOperIndication (UINT4 u4IfIndex, UINT1 u1OperStatus);
VOID L2PnacRedSetBridgePortOperStatus (UINT4 u4IfIndex, UINT1 u1OperStatus);

/* LA -> L2 */
VOID L2IwfCreatePortIndicationFromLA (UINT4 u4PortIndex, UINT1 u1PortOperStatus);
INT4 L2IwfLaHLPortOperIndication (UINT4 u4AggPortIndex, UINT1 u1OperStatus);

/* Bridge -> L2 */ 
INT4 L2IwfBrgHLPortOperIndication (UINT4 u4IfIndex, UINT1 u1Status);
INT4 L2IwfUtlBrgHLPortOperIndication (UINT4 u4IfIndex, UINT1 u1Status);


UINT4 L2IwfGetAgeoutInt (VOID);
VOID L2IwfRegisterSTDBRI (VOID);


VOID L2IwfIncrFilterInDiscards (UINT2 u2Port);
VOID L2IwfStartHwAgeTimer (UINT4 u4ContextId, UINT4 u4AgeOutInt);

/* NPAPI -> L2 */
#ifdef NPAPI_WANTED
INT4 L2IwfGetInstPortStateForPort (UINT4 u4PortNum,
                                   tMstInstPortStateInfo *pInstPortStateInfo,
                                   UINT2 *pu2Count);
INT4 L2IwfGetVlanPortStateForPort (UINT2 u2PortNum,
                                   tMstVlanPortStateInfo *pVlanPortStateInfo,
                                   UINT2 *pu2Count);
#endif
/* VCM -> L2 */
INT4
L2IwfContextCreateIndication (UINT4 u4ContextId);

INT4
L2IwfContextDeleteIndication (UINT4 u4ContextId);

INT4
L2IwfMiPortCreateIndication (UINT4 u4ContextId, UINT4 u4IfIndex, 
                             UINT2 u2LocalPortId);
INT4
L2IwfMiPortDeleteIndication (UINT4 u4IfIndex);

/******      APIs to be called from lrmain     *******/
INT4 L2IwfInit (INT1 *pi1Dummy);
VOID L2IwfDeInit (VOID);
VOID L2IwfMemDeInit (VOID);
INT4 L2IwfMemInit (VOID);
INT4 L2SyncTakeSem (VOID);
INT4 L2SyncGiveSem (VOID);

INT4 L2MiSyncTakeSem (VOID);
INT4 L2MiSyncGiveSem (VOID);
INT4 L2IwfPortMapIndication (UINT4 u4ContextId, UINT4 u4IfIndex,
 UINT2 u2LocalPortId);
INT4 L2IwfPortUnmapIndication (UINT4 u4IfIndex);
VOID L2IwfUpdateContextAliasName (UINT4 u4ContextId);

/*MPLS - L2 */
BOOL1 L2IwfIsVlanElan (UINT4 u4ContextId, tVlanId VlanId);

INT4 L2IwfHwPbPepCreate (UINT4 u4IfIndex, tVlanId SVlanId);
INT4
L2IwfGetPortLocVlanNameInfo (UINT4 u4IfIndex, tVlanNameInfo * pVlanNameInfo,
                              UINT2 *pu2VlanCount);


VOID L2IwfGetDefaultVlanId (UINT2 *pDefaultVlanId);
VOID L2IwfSetDefaultVlanId (UINT2 u2DefaultVlanId);
INT4 L2IwfGetFwdPortCount (UINT4 u4ContextId, tVlanId u2VlanId,
                           UINT2 *pFwdPortCount);
INT4
L2IwfGetPortOperPointToPointStatus (UINT4 u4IfIndex,
                                    BOOL1 * pbOperPointToPoint);
INT4
L2IwfSetPortOperPointToPointStatus (UINT4 u4ContextId, UINT2 u2LocalPortId,
                                    BOOL1 bOperPointToPoint);

/* To be called from PBB */
INT4 L2IwfCreateIsid (UINT4 u4ContextId, UINT4 Isid);
INT4 L2IwfDeleteIsid (UINT4 u4ContextId, UINT4 Isid);

INT4
L2IwfGetTagInfoFromFrame (UINT4 u4ContextId, UINT4 u4IfIndex,
                      tCRU_BUF_CHAIN_DESC * pBuf, 
       tVlanTag * pVlanTag, tPbbTag * pPbbTag,
                      UINT1 *pu1IngressAction, UINT4 *pu4InOffset,
       UINT4 *pu4TagOffSet,UINT1 u1FrameType, UINT1 *pu1IsSend);
INT4
L2IwfTransmitPbbFrame (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex, tVlanTag *pVlanTagInfo, 
                       tPbbTag *pPbbTagInfo,UINT1 u1FrameType);
INT4
L2IwfTransmitFrameOnVip (UINT4 u4ContextId, UINT4 u4IfIndex,
                          tCRU_BUF_CHAIN_DESC * pBuf, 
        tVlanTag * pVlanTag, tPbbTag * pPbbTag,
        UINT1 u1FrameType);
INT4
L2IwfTransmitCfmFrame (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4ContextId,
                       UINT4 u4IfIndex, tVlanTag * pVlanTagInfo,
                       tPbbTag * pPbbTagInfo, UINT1 u1FrameType);

INT4
L2IwfPbbProcessDataPacket (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                           UINT4 u4ContextId, UINT1 *pu1Dst);

INT4
L2IwfPbbTxFrameToIComp (UINT4 u4ContextId, UINT4 u4IfIndex,
                      tCRU_BUF_CHAIN_DESC * pBuf,
                      tVlanTag * pVlanTag, tPbbTag * pPbbTag, 
                      UINT4 *pu4InOffset, UINT1 u1FrameType,
                      UINT2 u2LocalPort);

INT4 L2IwfGetIsid (UINT4 u4Vip, UINT4 *pu4Isid);
 
INT4 L2IwfGetVip (UINT4 u4Context, UINT4 u4Isid, UINT2 *pu2Vip);

INT4 L2IwfGetVipIfIndex (UINT4 u4Context, UINT4 u4Isid, UINT4 *pu4Vip);

INT4
L2IwfGetPbbShutdownStatus (VOID);

INT4
L2IwfSetPbbShutdownStatus (UINT1 u1Status);

INT4
L2IwfGetVipOperStatusFlag (UINT4 u4IfIndex, UINT1 *pu1VipOperStatusFlag);

INT4
L2IwfSetVipOperStatusFlag (UINT4 u4IfIndex, UINT1 u1VipOperStatusFlag);

INT4
L2IwfUpdateVipOperStatus (UINT4 u4VipIndex, UINT1 u1OperStatus);

INT4
L2IwfGetVlanFloodingStatus (UINT4 u4ContextId, tVlanId u2VlanId, 
       UINT1 *pu1VlanFloodStatus);
INT4
L2IwfSetVlanFloodingStatus (UINT4 u4ContextId, tVlanId u2VlanId, 
       UINT1 u1VlanFloodStatus);
INT4 
L2IwfUpdatePortDeiBit (UINT4 u4IfIndex,UINT1 u1DeiBitValue);

INT4
L2IwfGetPortVlanListFromIfIndex (UINT4 u4ContextId, 
     tSNMP_OCTET_STRING_TYPE * pVlanIdList,
     UINT4 u4IfIndex, UINT1 u1VlanPortType);

INT4
L2IwfGetPortVlanList  (UINT4 u4ContextId,
                       tSNMP_OCTET_STRING_TYPE * pVlanIdList,
                       UINT2 u2LocalPortId, UINT1 u1VlanPortType);

INT4
L2IwfGetPortVlanMemberList (UINT4 u4IfIndex, UINT1 *pu1VlanList);

INT4
L2IwfSetAllToOneBndlStatus (UINT4 u4ContextId, UINT1 u1Status);

INT4
L2IwfGetAllToOneBndlStatus (UINT4 u4ContextId, UINT1 *pu1Status);

INT1
L2IwfIsBridgeModeChangeAllowed (UINT4 u4ContextId, UINT4 u4BrgMode);

INT4 
L2IwfUpdateSispPortCtrlStatus (UINT4 u4IfIndex, UINT1 u1Status);

INT4
L2IwfGetSispPortCtrlStatus (UINT4 u4IfIndex, UINT1 *pu1Status);

INT4 
L2IwfBlockPortChannelConfig PROTO ((UINT4 u4IfIndex, UINT1 u1Status));

INT4 
L2IwfIsLaConfigAllowed PROTO ((UINT4 u4IfIndex));

INT4 
L2IwfIsPortChannelConfigAllowed PROTO ((UINT4 u4IfIndex));

INT4
L2IwfIfDenyProtocolEntry (tL2IwfCfaIfTypeDenyProt *pL2IwfCfaIfTypeDenyProt);

INT4
L2IwfSendPortCrtIndForProtocol (UINT4 u4IfIndex, tCfaIfInfo * pIfInfo,
                                INT4 i4ProtocolId);
VOID
L2IwfSendPortDelIndForProtocol (UINT4 u4IfIndex, tCfaIfInfo * pIfInfo,
                                INT4 i4ProtocolId);
VOID
L2IwfPortOperStatIndForProtocol (UINT4 u4IfIndex, tCfaIfInfo *pIfInfo,
                                 INT4 i4ProtocolId);
INT4 L2IwfSetPortStateCtrlOwner (UINT4 u4IfIndex, UINT1 u1ProtId, 
                                 UINT1 u1OwnerStatus);

INT4 L2IwfGetPortStateCtrlOwner (UINT4 u4IfIndex, UINT1 *pu1OwnerProtId, BOOL1 bLockReq);

INT4 L2IwfGetInstanceInfo (UINT4 u4ContextId, UINT2 u2Instance, 
                           tStpInstanceInfo *pStpInstInfo);

PUBLIC INT4
L2IwfIsDhcpPacket (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pu1Result, 
                   UINT4 u4IfIndex);

INT4 L2IwfFlushFdbForGivenInst(UINT4 u4ContextId,UINT4 u4IfIndex,UINT2 u2InstanceId,
                           INT2 i2OptimizeFlag,UINT1 u1Module,UINT2 u2FlushFlag);
UINT1
L2IwfGetVlanLearningType (UINT4 u4ContextId);

INT4
L2IwfSetVlanLearningType (UINT4 u4ContextId, UINT1 u1LearnMode);

PUBLIC INT4 
L2IwfIsErpsStartedInContext (UINT4 u4ContextId); 

INT4 L2IwfDeleteEntry (UINT4 u4PortIndex, UINT1 u1IfType, UINT1 u1IsCnpCheck);
INT4 L2IwfDelPort (UINT4 u4PortIndex, UINT1 u1IfType, UINT1 u1IsCnpCheck);
INT4 L2IwfDeletePort (UINT4 u4PortIndex, UINT1 u1IfType, UINT1 u1IsCnpCheck);
INT4 L2IwfIsRapsVlanPresent(UINT4 u4ContextId, UINT2 u2RapsVlanId, UINT2 u2MMstInst);

UINT4 L2IwfGetPortIfIndex(UINT2 u2Index);
INT4 L2IwfSetFCoEVlanType PROTO ((UINT4, tVlanId, UINT1));
VOID
L2IwfSetPoAddLinkProgress (UINT2 u2AggId, BOOL1 bStatus);
BOOL1
L2IwfGetPoAddLinkProgress (UINT2 u2AggId);
PUBLIC INT4
L2IwfApiGetPortType(UINT4 u4IfIndex,UINT4 *u4PortType);
VOID L2IwfMbsmGetNpSyncStatus(UINT1 *pu1Status);
VOID L2IwfMbsmSetNpSyncStatus(UINT1 u1Status);
PUBLIC VOID L2IwfLldpApiUpdateSvidOnUap (UINT4 u4UapIfIndex, UINT4 u4Svid,
                                         UINT4 u4Request);

VOID
L2iwfEcfmNotifyOperStatus PROTO ((UINT4 u4IfIndex, UINT1 u1OperStatus));

PUBLIC VOID
L2IwfMbsmGetNpSyncProgressStatus PROTO ((UINT4 *pu4Status));
INT4
L2IwfGetPortChannelAndOperStatus (UINT4 u4IfIndex, UINT2 *pu2AggId, UINT1 *pu1BridgeOperStatus);

VOID L2IwfUpdateIgmpTagInfo (tCRU_BUF_CHAIN_HEADER * pBuf, 
                             UINT2 u2PbPortType, 
                             tVlanTag *pVlanTag, UINT2 *pu2TagLen);

INT4 L2IwfIsPortInInst(UINT4 u4ContextId ,UINT4 u4IfIndex, UINT2 u2InstId);
#endif  /* _L2IWF_H_ */
