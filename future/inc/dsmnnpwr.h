/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dsmnnpwr.h,v 1.2 2012/08/24 08:58:17 siva Exp $
 *
 * Description:This file contains the structure definitions and
 *             macros of DSMON NP wrapper.
 *
 *******************************************************************/
#ifndef __DSMON_NP_WR_H
#define __DSMON_NP_WR_H

#include "rmon2.h"

/* OPER ID */

#define  FS_DSMON_ADD_FLOW_STATS                            1
#define  FS_DSMON_COLLECT_STATS                             2
#define  FS_DSMON_ENABLE_PROBE                              3
#define  FS_DSMON_DISABLE_PROBE                             4
#define  FS_DSMON_HW_SET_STATUS                             5
#define  FS_DSMON_HW_GET_STATUS                             6
#define  FS_DSMON_REMOVE_FLOW_STATS                         7

/* Required arguments list for the dsmon NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

typedef struct {
    tPktHeader  FlowStatsTuple;
} tDsmonNpWrFsDSMONAddFlowStats;

typedef struct {
    tPktHeader     FlowStatsTuple;
    tRmon2Stats *  pStatsCnt;
} tDsmonNpWrFsDSMONCollectStats;

typedef struct {
    UINT1 *  PortList;
    UINT4      u4InterfaceIdx;
    UINT2      u2VlanId;
    UINT1      au1pad[2];
} tDsmonNpWrFsDSMONEnableProbe;

typedef struct {
    UINT1 *  PortList;
    UINT4      u4InterfaceIdx;
    UINT2      u2VlanId;
    UINT1      au1pad[2];
} tDsmonNpWrFsDSMONDisableProbe;

typedef struct {
    UINT4  u4DSMONStatus;
} tDsmonNpWrFsDSMONHwSetStatus;

typedef struct {
    tPktHeader  FlowStatsTuple;
} tDsmonNpWrFsDSMONRemoveFlowStats;

typedef struct DsmonNpModInfo {
union {
    tDsmonNpWrFsDSMONAddFlowStats  sFsDSMONAddFlowStats;
    tDsmonNpWrFsDSMONCollectStats  sFsDSMONCollectStats;
    tDsmonNpWrFsDSMONEnableProbe  sFsDSMONEnableProbe;
    tDsmonNpWrFsDSMONDisableProbe  sFsDSMONDisableProbe;
    tDsmonNpWrFsDSMONHwSetStatus  sFsDSMONHwSetStatus;
    tDsmonNpWrFsDSMONRemoveFlowStats  sFsDSMONRemoveFlowStats;
    }unOpCode;

#define  DsmonNpFsDSMONAddFlowStats  unOpCode.sFsDSMONAddFlowStats;
#define  DsmonNpFsDSMONCollectStats  unOpCode.sFsDSMONCollectStats;
#define  DsmonNpFsDSMONEnableProbe  unOpCode.sFsDSMONEnableProbe;
#define  DsmonNpFsDSMONDisableProbe  unOpCode.sFsDSMONDisableProbe;
#define  DsmonNpFsDSMONHwSetStatus  unOpCode.sFsDSMONHwSetStatus;
#define  DsmonNpFsDSMONRemoveFlowStats  unOpCode.sFsDSMONRemoveFlowStats;
} tDsmonNpModInfo;


/* Function Prototype for Generic NP OpCode specific wrappers
 * defined in Dsmonnputil.c */

UINT1 DsmonFsDSMONAddFlowStats PROTO ((tPktHeader FlowStatsTuple));
UINT1 DsmonFsDSMONCollectStats PROTO ((tPktHeader FlowStatsTuple, tRmon2Stats * pStatsCnt));
UINT1 DsmonFsDSMONEnableProbe PROTO ((UINT4 u4InterfaceIdx, UINT2 u2VlanId, tPortList PortList));
UINT1 DsmonFsDSMONDisableProbe PROTO ((UINT4 u4InterfaceIdx, UINT2 u2VlanId, tPortList PortList));
UINT1 DsmonFsDSMONHwSetStatus PROTO ((UINT4 u4DSMONStatus));
UINT1 DsmonFsDSMONHwGetStatus PROTO ((VOID));
UINT1 DsmonFsDSMONRemoveFlowStats PROTO ((tPktHeader FlowStatsTuple));

#endif /* __DSMON_NP_WR_H */

