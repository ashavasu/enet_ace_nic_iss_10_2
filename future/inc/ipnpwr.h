/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: ipnpwr.h,v 1.21 2017/11/14 07:31:11 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              for IP wrappers
 *              
 ********************************************************************/
#ifndef __IP_NP_WR_H__
#define __IP_NP_WR_H__

#include "ipnp.h"
#include "rportnp.h"
#include "pim.h"

#ifdef CFA_WANTED
#include "cfanp.h"
#endif   

/* OPER ID */
/* when new NP call is added in IP, the following files 
 * has to be updated 
 * 1. npgensup.h 
 * 2. npbcmsup.h
*/

enum
{
  FS_NP_IP_INIT    =1                                     , 
  FS_NP_OSPF_INIT                                       , 
  FS_NP_OSPF_DE_INIT                                    , 
  FS_NP_DHCP_SRV_INIT                                   , 
  FS_NP_DHCP_SRV_DE_INIT                                , 
  FS_NP_DHCP_RLY_INIT                                   , 
  FS_NP_DHCP_RLY_DE_INIT                                , 
  FS_NP_RIP_INIT                                        , 
  FS_NP_RIP_DE_INIT                                     , 
  FS_NP_IPV4_CREATE_IP_INTERFACE                        , 
  FS_NP_IPV4_MODIFY_IP_INTERFACE                        , 
  FS_NP_IPV4_DELETE_IP_INTERFACE                        , 
  FS_NP_IPV4_UPDATE_IP_INTERFACE_STATUS                 , 
  FS_NP_IPV4_SET_FORWARDING_STATUS                      , 
  FS_NP_IPV4_CLEAR_ROUTE_TABLE                          , 
  FS_NP_IPV4_CLEAR_ARP_TABLE                            , 
  FS_NP_IPV4_UC_DEL_ROUTE                               , 
  FS_NP_IPV4_ARP_ADD                                    , 
  FS_NP_IPV4_ARP_MODIFY                                 , 
  FS_NP_IPV4_ARP_DEL                                    , 
  FS_NP_IPV4_CHECK_HIT_ON_ARP_ENTRY                     , 
  FS_NP_IPV4_SYNC_VLAN_AND_L3_INFO                      , 
  FS_NP_IPV4_UC_ADD_ROUTE                               , 
  FS_NP_IPV4_VRF_ARP_ADD                                , 
  FS_NP_IPV4_VRF_ARP_MODIFY                             , 
  FS_NP_IPV4_VRF_ARP_DEL                                , 
  FS_NP_IPV4_VRF_CHECK_HIT_ON_ARP_ENTRY                 , 
  FS_NP_IPV4_VRF_CLEAR_ARP_TABLE                        , 
  FS_NP_IPV4_VRF_GET_SRC_MOVED_IP_ADDR                  , 
  FS_NP_CFA_VRF_SET_DLF_STATUS                          , 
  FS_NP_L3_IPV4_VRF_ARP_ADD                             , 
  FS_NP_L3_IPV4_VRF_ARP_MODIFY                          , 
  FS_NP_IPV4_VRF_ARP_GET                                , 
  FS_NP_IPV4_VRF_ARP_GET_NEXT                           , 
  FS_NP_IPV4_VRRP_INTF_CREATE_WR                        , 
  FS_NP_IPV4_CREATE_VRRP_INTERFACE                      , 
  FS_NP_IPV4_VRRP_INTF_DELETE_WR                        , 
  FS_NP_IPV4_DELETE_VRRP_INTERFACE                      , 
  FS_NP_IPV4_GET_VRRP_INTERFACE                         , 
  FS_NP_IPV4_VRRP_INSTALL_FILTER                        , 
  FS_NP_IPV4_VRRP_REMOVE_FILTER                         , 
  FS_NP_IPV4_IS_RT_PRESENT_IN_FAST_PATH                 , 
  FS_NP_IPV4_GET_STATS                                  , 
  FS_NP_IPV4_VRM_ENABLE_VR                              , 
  FS_NP_IPV4_BIND_IF_TO_VR_ID                           , 
  FS_NP_IPV4_GET_NEXT_HOP_INFO                          , 
  FS_NP_IPV4_UC_ADD_TRAP                                , 
  FS_NP_IPV4_CLEAR_FOWARDING_TBL                        , 
  FS_NP_IPV4_GET_SRC_MOVED_IP_ADDR                      , 
  FS_NP_IPV4_MAP_VLANS_TO_IP_INTERFACE                  , 
  FS_NP_ISIS_HW_PROGRAM                                  ,
  FS_NP_NAT_DISABLE_ON_INTF                              ,
  FS_NP_NAT_ENABLE_ON_INTF                               ,
  FS_NP_IPV4_INTF_STATUS                                , 
  FS_NP_IPV4_ARP_GET                                    , 
  FS_NP_IPV4_ARP_GET_NEXT                               , 
  FS_NP_IPV4_UC_GET_ROUTE                               , 
  FS_NP_L3_IPV4_ARP_ADD                                 , 
  FS_NP_L3_IPV4_ARP_MODIFY                              , 
  FS_NP_IPV4_L3_IP_INTERFACE                            , 
  FS_NP_VRRP_HW_PROGRAM                                  ,
  FS_NP_MBSM_IP_INIT                                     ,
  FS_NP_MBSM_OSPF_INIT                                   ,
  FS_NP_MBSM_DHCP_SRV_INIT                               ,
  FS_NP_MBSM_DHCP_RLY_INIT                               ,
  FS_NP_MBSM_IPV4_CREATE_IP_INTERFACE                    ,
  FS_NP_MBSM_IPV4_ARP_ADD                                ,
  FS_NP_MBSM_IPV4_UC_ADD_ROUTE                           ,
  FS_NP_MBSM_IPV4_VRM_ENABLE_VR                          ,
  FS_NP_MBSM_IPV4_BIND_IF_TO_VR_ID                       ,
  FS_NP_MBSM_IPV4_UC_ADD_TRAP                            ,
  FS_NP_MBSM_IPV4_MAP_VLANS_TO_IP_INTERFACE              ,
  FS_NP_MBSM_IPV4_VRF_ARP_ADD                            ,
  FS_NP_MBSM_IPV4_CREATE_VRRP_INTERFACE                  ,
  FS_NP_MBSM_IPV4_VRRP_INSTALL_FILTER                    ,
  FS_NP_MBSM_IPV4_ARP_ADDITION                           ,
  FS_NP_MBSM_IPV4_VRF_ARP_ADDITION                       ,
  FS_NP_MBSM_IPV4_UPDATE_IP_INTERFACE_STATUS             ,
  FS_NP_MBSM_ISIS_HW_PROGRAM                             ,
  FS_NP_IPV4_DELETE_SEC_IP_INTERFACE                     ,
  FS_NP_MBSM_IPV4_CREATE_L3SUB_INTERFACE                 ,
  FS_NP_IPV4_GET_ECMP_GROUPS                             ,
  FS_NP_IPV4_CREATE_L3SUB_INTERFACE                      ,
  FS_NP_IPV4_DELETE_L3SUB_INTERFACE                         ,
  FS_NP_IPV4_MAX_NP
};
/* Required arguments list for the ip NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

typedef struct {
    UINT4    u4CfaIfIndex;
    UINT1 *  pu1IfName;
    UINT1 *  au1MacAddr;
    UINT4    u4VrId;
    UINT4    u4IpAddr;
    UINT4    u4IpSubNetMask;
    UINT2    u2VlanId;
    UINT1    au1pad[2];
} tIpNpWrFsNpIpv4CreateIpInterface;

typedef struct {
    UINT4    u4CfaIfIndex;
    UINT1 *  pu1IfName;
    UINT1 *  au1MacAddr;
    UINT4    u4VrId;
    UINT4    u4IpAddr;
    UINT4    u4IpSubNetMask;
    UINT2    u2VlanId;
    UINT1    au1pad[2];
    UINT4    u4ParentIfIndex;
} tIpNpWrFsNpIpv4CreateL3SubInterface;

typedef struct {
    UINT4    u4CfaIfIndex;
    UINT1 *  pu1IfName;
    UINT1 *  au1MacAddr;
    UINT4    u4VrId;
    UINT4    u4IpAddr;
    UINT4    u4IpSubNetMask;
    UINT2    u2VlanId;
    UINT1    au1pad[2];
} tIpNpWrFsNpIpv4ModifyIpInterface;

typedef struct {
    UINT4    u4IfIndex;
    UINT1 *  pu1IfName;
    UINT4    u4VrId;
    UINT2    u2VlanId;
    UINT1    au1pad[2];
} tIpNpWrFsNpIpv4DeleteIpInterface;

typedef struct {
    UINT4    u4IfIndex;
} tIpNpWrFsNpIpv4DeleteL3SubInterface;

typedef struct {
    UINT4    u4IfIndex;                                                                                                                                          UINT1 *  pu1IfName;
    UINT4    u4VrId;
    UINT2    u2VlanId;
    UINT1    au1pad[2];
    UINT4    u4IpAddr;
} tIpNpWrFsNpIpv4DeleteSecIpInterface;



typedef struct {
    UINT4    u4CfaIfIndex;
    UINT1 *  pu1IfName;
    UINT1 *  au1MacAddr;
    UINT4    u4VrId;
    UINT4    u4IpAddr;
    UINT4    u4IpSubNetMask;
    UINT4    u4Status;
    UINT2    u2VlanId;
    UINT1    au1pad[2];
} tIpNpWrFsNpIpv4UpdateIpInterfaceStatus;

typedef struct {
    UINT4  u4VrId;
    UINT1  u1Status;
    UINT1  au1pad[3];
} tIpNpWrFsNpIpv4SetForwardingStatus;

typedef struct {
    tFsNpNextHopInfo  routeEntry;
    UINT4             u4VrId;
    UINT4             u4IpDestAddr;
    UINT4             u4IpSubNetMask;
    INT4 *            pi4FreeDefIpB4Del;
} tIpNpWrFsNpIpv4UcDelRoute;

typedef struct {
    UINT4      u4IfIndex;
    UINT1 *    pMacAddr;
    UINT1 *    pu1IfName;
    UINT4 *    pu4TblFull;
    UINT4      u4IpAddr;
    tNpVlanId  u2VlanId;
    INT1       i1State;
    UINT1      au1pad[1];
} tIpNpWrFsNpIpv4ArpAdd;

typedef struct {
    UINT4      u4IfIndex;
    UINT4      u4PhyIfIndex;
    UINT1 *    pMacAddr;
    UINT1 *    pu1IfName;
    UINT4      u4IpAddr;
    tNpVlanId  u2VlanId;
    INT1       i1State;
    UINT1      au1pad[1];
} tIpNpWrFsNpIpv4ArpModify;

typedef struct {
    UINT1 *  pu1IfName;
    UINT4    u4IpAddr;
    INT1     i1State;
    UINT1    au1pad[3];
} tIpNpWrFsNpIpv4ArpDel;

typedef struct {
    UINT4  u4IpAddress;
    UINT1  u1NextHopFlag;
    UINT1  u1HitBitStatus;
    UINT1  au1pad[2];
} tIpNpWrFsNpIpv4CheckHitOnArpEntry;

typedef struct {
#ifndef NPSIM_WANTED
    tFsNpNextHopInfo  * pRouteEntry;
#else
    tFsNpNextHopInfo   routeEntry;
#endif
     UINT4             u4VrId;
    UINT4             u4IpDestAddr;
    UINT4             u4IpSubNetMask;
    UINT1 *           pbu1TblFull;
} tIpNpWrFsNpIpv4UcAddRoute;

typedef struct {
    UINT4      u4IfIndex;
    UINT4      u4VrId;
    UINT4      u4IpAddr;
    UINT1 *    pMacAddr;
    UINT1 *    pu1IfName;
    UINT4 *    pu4TblFull;
    tNpVlanId  u2VlanId;
    INT1       i1State;
    UINT1      au1pad[1];
} tIpNpWrFsNpIpv4VrfArpAdd;

typedef struct {
    UINT4      u4IfIndex;
    UINT4      u4VrId;
    UINT4      u4IpAddr;
    UINT4      u4PhyIfIndex;
    UINT1 *    pMacAddr;
    UINT1 *    pu1IfName;
    tNpVlanId  u2VlanId;
    INT1       i1State;
    UINT1      au1pad[1];
} tIpNpWrFsNpIpv4VrfArpModify;

typedef struct {
    UINT1 *  pu1IfName;
    UINT4    u4VrId;
    UINT4    u4IpAddr;
    INT1     i1State;
    UINT1    au1pad[3];
} tIpNpWrFsNpIpv4VrfArpDel;

typedef struct {
    UINT4  u4VrId;
    UINT4  u4IpAddress;
    UINT1  u1NextHopFlag;
    UINT1  u1HitBitStatus;
    UINT1  au1pad[2];
} tIpNpWrFsNpIpv4VrfCheckHitOnArpEntry;

typedef struct {
    UINT4  u4VrId;
} tIpNpWrFsNpIpv4VrfClearArpTable;

typedef struct {
    UINT4 *  pu4VrId;
    UINT4 *  pu4IpAddress;
} tIpNpWrFsNpIpv4VrfGetSrcMovedIpAddr;

typedef struct {
    UINT4  u4VrfId;
    UINT1  u1Status;
    UINT1  au1pad[3];
} tIpNpWrFsNpCfaVrfSetDlfStatus;

typedef struct {
    UINT4    u4IfIndex;
    UINT1 *  pMacAddr;
    UINT1 *  pu1IfName;
    UINT1 *  pbu1TblFull;
    UINT4    u4VrfId;
    UINT4    u4IpAddr;
    INT1     i1State;
    UINT1    au1pad[3];
} tIpNpWrFsNpL3Ipv4VrfArpAdd;

typedef struct {
    UINT4    u4IfIndex;
    UINT1 *  pMacAddr;
    UINT1 *  pu1IfName;
    UINT4    u4VrfId;
    UINT4    u4IpAddr;
    INT1     i1State;
    UINT1    au1pad[3];
} tIpNpWrFsNpL3Ipv4VrfArpModify;

typedef struct {
    tNpArpOutput *  pArpNpOutParam;
    tNpArpInput     ArpNpInParam;
} tIpNpWrFsNpIpv4VrfArpGet;

typedef struct {
    tNpArpOutput *  pArpNpOutParam;
    tNpArpInput     ArpNpInParam;
} tIpNpWrFsNpIpv4VrfArpGetNext;

typedef struct {
    UINT4      u4IfIndex;
    UINT1 *    au1MacAddr;
    UINT4      u4IpAddr;
    tNpVlanId  u2VlanId;
    UINT1      au1pad[2];
} tIpNpWrFsNpIpv4VrrpIntfCreateWr;

typedef struct {
    UINT1 *    au1MacAddr;
    UINT4      u4IpAddr;
    tNpVlanId  u2VlanId;
    UINT1      au1pad[2];
} tIpNpWrFsNpIpv4CreateVrrpInterface;

typedef struct {
    UINT4      u4IfIndex;
    UINT1 *    au1MacAddr;
    UINT4      u4IpAddr;
    tNpVlanId  u2VlanId;
    UINT1      au1pad[2];
} tIpNpWrFsNpIpv4VrrpIntfDeleteWr;

typedef struct {
    UINT1 *    au1MacAddr;
    UINT4      u4IpAddr;
    tNpVlanId  u2VlanId;
    UINT1      au1pad[2];
} tIpNpWrFsNpIpv4DeleteVrrpInterface;

typedef struct {
    INT4  i4IfIndex;
    INT4  i4VrId;
} tIpNpWrFsNpIpv4GetVrrpInterface;

typedef struct {
    UINT1 u1NpAction;
    UINT1      au1pad[3];
    tVrrpNwIntf *pVrrpNwIntf;
}tIpNpWrFsNpVrrpHwProgram;

#ifdef ISIS_WANTED
typedef struct {
    UINT1      u1Status;
    UINT1      au1pad[3];
} tIpNpWrFsNpIsisHwProgram;
#endif /*ISIS_WANTED*/

typedef struct {
    UINT4  u4DestAddr;
    UINT4  u4Mask;
} tIpNpWrFsNpIpv4IsRtPresentInFastPath;

typedef struct {
    UINT4 *  pu4RetVal;
    INT4     i4StatType;
} tIpNpWrFsNpIpv4GetStats;

typedef struct {
    UINT4  u4VrId;
    UINT1  u1Status;
    UINT1  au1pad[3];
} tIpNpWrFsNpIpv4VrmEnableVr;

typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4VrId;
} tIpNpWrFsNpIpv4BindIfToVrId;

typedef struct {
    tFsNpNextHopInfo *  pNextHopInfo;
    UINT4               u4VrId;
    UINT4               u4Dest;
    UINT4               u4DestMask;
} tIpNpWrFsNpIpv4GetNextHopInfo;

typedef struct {
    tFsNpNextHopInfo  nextHopInfo;
    UINT4             u4VrId;
    UINT4             u4Dest;
    UINT4             u4DestMask;
} tIpNpWrFsNpIpv4UcAddTrap;

typedef struct {
    UINT4  u4VrId;
} tIpNpWrFsNpIpv4ClearFowardingTbl;

typedef struct {
    UINT4 *  pu4IpAddress;
} tIpNpWrFsNpIpv4GetSrcMovedIpAddr;

typedef struct {
    tNpIpVlanMappingInfo *  pPvlanMappingInfo;
} tIpNpWrFsNpIpv4MapVlansToIpInterface;

#ifdef NAT_WANTED
typedef struct {
    INT4  i4Intf;
} tIpNpWrFsNpNatDisableOnIntf;

typedef struct {
    INT4  i4Intf;
} tIpNpWrFsNpNatEnableOnIntf;

#endif /* NAT_WANTED */
typedef struct {
    UINT4              u4VrId;
    UINT4              u4IfStatus;
    tFsNpIp4IntInfo *  pIpIntInfo;
} tIpNpWrFsNpIpv4IntfStatus;

typedef struct {
    tNpArpOutput *  pArpNpOutParam;
    tNpArpInput     ArpNpInParam;
} tIpNpWrFsNpIpv4ArpGet;

typedef struct {
    tNpArpOutput *  pArpNpOutParam;
    tNpArpInput     ArpNpInParam;
} tIpNpWrFsNpIpv4ArpGetNext;

typedef struct {
    tNpRtmOutput *  pRtmNpOutParam;
    tNpRtmInput     RtmNpInParam;
} tIpNpWrFsNpIpv4UcGetRoute;

typedef struct {
    UINT4    u4IfIndex;
    UINT1 *  pMacAddr;
    UINT1 *  pu1IfName;
    UINT1 *  pbu1TblFull;
    UINT4    u4IpAddr;
    INT1     i1State;
    UINT1    au1pad[3];
} tIpNpWrFsNpL3Ipv4ArpAdd;

typedef struct {
    UINT4    u4IfIndex;
    UINT1 *  pMacAddr;
    UINT1 *  pu1IfName;
    UINT4    u4IpAddr;
    INT1     i1State;
    UINT1    au1pad[3];
} tIpNpWrFsNpL3Ipv4ArpModify;

typedef struct {
    tFsNpL3IfInfo *  pFsNpL3IfInfo;
    tL3Action        L3Action;
} tIpNpWrFsNpIpv4L3IpInterface;

#ifdef MBSM_WANTED
typedef struct {
    tMbsmSlotInfo *  pSlotInfo;
} tIpNpWrFsNpMbsmIpInit;

typedef struct {
    tMbsmSlotInfo *  pSlotInfo;
} tIpNpWrFsNpMbsmOspfInit;

typedef struct {
    tMbsmSlotInfo *  pSlotInfo;
} tIpNpWrFsNpMbsmDhcpSrvInit;

typedef struct {
    tMbsmSlotInfo *  pSlotInfo;
} tIpNpWrFsNpMbsmDhcpRlyInit;

typedef struct {
    tMbsmSlotInfo *  pSlotInfo;                                                                                                                                  UINT1      u1Status;
    UINT1      au1pad[3];
} tIpNpWrFsNpMbsmIsisHwProgram;

typedef struct {
    UINT4            u4CfaIfIndex;
    UINT1 *          au1MacAddr;
    tMbsmSlotInfo *  pSlotInfo;
    UINT4            u4VrId;
    UINT4            u4IpAddr;
    UINT4            u4IpSubNetMask;
    UINT2            u2VlanId;
    UINT1            au1pad[2];
} tIpNpWrFsNpMbsmIpv4CreateIpInterface;

typedef struct {
    UINT4            u4CfaIfIndex;
    UINT1 *          au1MacAddr;
    tMbsmSlotInfo *  pSlotInfo;
    UINT4            u4VrId;
    UINT4            u4IpAddr;
    UINT4            u4IpSubNetMask;
    UINT2            u2VlanId;
    UINT1            au1pad[2];
    UINT4            u4ParentIfIndex;
} tIpNpWrFsNpMbsmIpv4CreateL3SubInterface;

typedef struct {
    UINT1 *          pMacAddr;
    UINT1 *          pbu1TblFull;
    tMbsmSlotInfo *  pSlotInfo;
    UINT4            u4IpAddr;
    tNpVlanId        u2VlanId;
    UINT1            au1pad[2];
} tIpNpWrFsNpMbsmIpv4ArpAdd;

typedef struct {
    UINT1 *           pbu1TblFull;
    tMbsmSlotInfo *   pSlotInfo;
#ifndef NPSIM_WANTED
        tFsNpNextHopInfo  * pRouteEntry;
#else
        tFsNpNextHopInfo   routeEntry;
#endif
    UINT4             u4VrId;
    UINT4             u4IpDestAddr;
    UINT4             u4IpSubNetMask;
} tIpNpWrFsNpMbsmIpv4UcAddRoute;

typedef struct {
    UINT4            u4VrId;
    tMbsmSlotInfo *  pSlotInfo;
    UINT1            u1Status;
    UINT1            au1pad[3];
} tIpNpWrFsNpMbsmIpv4VrmEnableVr;

typedef struct {
    UINT4            u4IfIndex;
    tMbsmSlotInfo *  pSlotInfo;
    UINT4            u4VrId;
} tIpNpWrFsNpMbsmIpv4BindIfToVrId;

typedef struct {
    tMbsmSlotInfo *   pSlotInfo;
    tFsNpNextHopInfo  nextHopInfo;
    UINT4             u4VrId;
    UINT4             u4Dest;
    UINT4             u4DestMask;
} tIpNpWrFsNpMbsmIpv4UcAddTrap;

typedef struct {
    tNpIpVlanMappingInfo *  pPvlanMappingInfo;
    tMbsmSlotInfo *         pSlotInfo;
} tIpNpWrFsNpMbsmIpv4MapVlansToIpInterface;

typedef struct {
    UINT4            u4VrId;
    UINT4            u4IpAddr;
    UINT1 *          pMacAddr;
    UINT1 *          pbu1TblFull;
    tMbsmSlotInfo *  pSlotInfo;
    tNpVlanId        u2VlanId;
    UINT1            au1pad[2];
} tIpNpWrFsNpMbsmIpv4VrfArpAdd;

typedef struct {
    UINT4            u4IpAddr;
    UINT1 *          au1MacAddr;
    tMbsmSlotInfo *  pSlotInfo;
    tNpVlanId        u2VlanId;
    UINT1            au1pad[2];
} tIpNpWrFsNpMbsmIpv4CreateVrrpInterface;

typedef struct {
    tMbsmSlotInfo *  pSlotInfo;
} tIpNpWrFsNpMbsmIpv4VrrpInstallFilter;


typedef struct {
    UINT1 *          pMacAddr;
    UINT1 *          pbu1TblFull;
    UINT1 *          pu1IfName;
    tMbsmSlotInfo *  pSlotInfo;
    UINT4            u4IfIndex;
    UINT4            u4IpAddr;
    tNpVlanId        u2VlanId;
    INT1             i1State;
    UINT1            au1pad[1];
} tIpNpWrFsNpMbsmIpv4ArpAddition;

typedef struct {
    UINT1 *          pMacAddr;
    UINT1 *          pbu1TblFull;
    UINT1 *          pu1IfName;
    tMbsmSlotInfo *  pSlotInfo;
    UINT4            u4IfIndex;
    UINT4            u4VrId;
    UINT4            u4IpAddr;
    tNpVlanId        u2VlanId;
    INT1             i1State;
    UINT1            au1pad[1];
} tIpNpWrFsNpMbsmIpv4VrfArpAddition;
#endif /* MBSM_WANTED */

typedef struct {
    UINT4  *pu4EcmpGroups;
}tIpNpWrFsNpIpv4GetEcmpGroups; 

typedef struct IpNpModInfo {
union {
    tIpNpWrFsNpIpv4CreateIpInterface  sFsNpIpv4CreateIpInterface;
    tIpNpWrFsNpIpv4CreateL3SubInterface sFsNpIpv4CreateL3SubInterface;
    tIpNpWrFsNpIpv4ModifyIpInterface  sFsNpIpv4ModifyIpInterface;
    tIpNpWrFsNpIpv4DeleteIpInterface  sFsNpIpv4DeleteIpInterface;
    tIpNpWrFsNpIpv4DeleteL3SubInterface  sFsNpIpv4DeleteL3SubInterface;
    tIpNpWrFsNpIpv4UpdateIpInterfaceStatus  sFsNpIpv4UpdateIpInterfaceStatus;
    tIpNpWrFsNpIpv4SetForwardingStatus  sFsNpIpv4SetForwardingStatus;
    tIpNpWrFsNpIpv4UcDelRoute  sFsNpIpv4UcDelRoute;
    tIpNpWrFsNpIpv4ArpAdd  sFsNpIpv4ArpAdd;
    tIpNpWrFsNpIpv4ArpModify  sFsNpIpv4ArpModify;
    tIpNpWrFsNpIpv4ArpDel  sFsNpIpv4ArpDel;
    tIpNpWrFsNpIpv4CheckHitOnArpEntry  sFsNpIpv4CheckHitOnArpEntry;
    tIpNpWrFsNpIpv4UcAddRoute  sFsNpIpv4UcAddRoute;
    tIpNpWrFsNpIpv4VrfArpAdd  sFsNpIpv4VrfArpAdd;
    tIpNpWrFsNpIpv4VrfArpModify  sFsNpIpv4VrfArpModify;
    tIpNpWrFsNpIpv4VrfArpDel  sFsNpIpv4VrfArpDel;
    tIpNpWrFsNpIpv4VrfCheckHitOnArpEntry  sFsNpIpv4VrfCheckHitOnArpEntry;
    tIpNpWrFsNpIpv4VrfClearArpTable  sFsNpIpv4VrfClearArpTable;
    tIpNpWrFsNpIpv4VrfGetSrcMovedIpAddr  sFsNpIpv4VrfGetSrcMovedIpAddr;
    tIpNpWrFsNpCfaVrfSetDlfStatus  sFsNpCfaVrfSetDlfStatus;
    tIpNpWrFsNpL3Ipv4VrfArpAdd  sFsNpL3Ipv4VrfArpAdd;
    tIpNpWrFsNpL3Ipv4VrfArpModify  sFsNpL3Ipv4VrfArpModify;
    tIpNpWrFsNpIpv4VrfArpGet  sFsNpIpv4VrfArpGet;
    tIpNpWrFsNpIpv4VrfArpGetNext  sFsNpIpv4VrfArpGetNext;
    tIpNpWrFsNpIpv4VrrpIntfCreateWr  sFsNpIpv4VrrpIntfCreateWr;
    tIpNpWrFsNpIpv4CreateVrrpInterface  sFsNpIpv4CreateVrrpInterface;
    tIpNpWrFsNpIpv4VrrpIntfDeleteWr  sFsNpIpv4VrrpIntfDeleteWr;
    tIpNpWrFsNpIpv4DeleteVrrpInterface  sFsNpIpv4DeleteVrrpInterface;
    tIpNpWrFsNpIpv4GetVrrpInterface  sFsNpIpv4GetVrrpInterface;
    tIpNpWrFsNpVrrpHwProgram  sFsNpVrrpHwProgram;
#ifdef ISIS_WANTED
    tIpNpWrFsNpIsisHwProgram sFsNpIsisHwProgram;
#endif /*ISIS_WANTED */
    tIpNpWrFsNpIpv4IsRtPresentInFastPath  sFsNpIpv4IsRtPresentInFastPath;
    tIpNpWrFsNpIpv4GetStats  sFsNpIpv4GetStats;
    tIpNpWrFsNpIpv4VrmEnableVr  sFsNpIpv4VrmEnableVr;
    tIpNpWrFsNpIpv4BindIfToVrId  sFsNpIpv4BindIfToVrId;
    tIpNpWrFsNpIpv4GetNextHopInfo  sFsNpIpv4GetNextHopInfo;
    tIpNpWrFsNpIpv4UcAddTrap  sFsNpIpv4UcAddTrap;
    tIpNpWrFsNpIpv4ClearFowardingTbl  sFsNpIpv4ClearFowardingTbl;
    tIpNpWrFsNpIpv4GetSrcMovedIpAddr  sFsNpIpv4GetSrcMovedIpAddr;
    tIpNpWrFsNpIpv4MapVlansToIpInterface  sFsNpIpv4MapVlansToIpInterface;
#ifdef NAT_WANTED
    tIpNpWrFsNpNatDisableOnIntf  sFsNpNatDisableOnIntf;
    tIpNpWrFsNpNatEnableOnIntf  sFsNpNatEnableOnIntf;
#endif /* NAT_WANTED */
    tIpNpWrFsNpIpv4IntfStatus  sFsNpIpv4IntfStatus;
    tIpNpWrFsNpIpv4ArpGet  sFsNpIpv4ArpGet;
    tIpNpWrFsNpIpv4ArpGetNext  sFsNpIpv4ArpGetNext;
    tIpNpWrFsNpIpv4UcGetRoute  sFsNpIpv4UcGetRoute;
    tIpNpWrFsNpL3Ipv4ArpAdd  sFsNpL3Ipv4ArpAdd;
    tIpNpWrFsNpL3Ipv4ArpModify  sFsNpL3Ipv4ArpModify;
    tIpNpWrFsNpIpv4L3IpInterface  sFsNpIpv4L3IpInterface;
    tIpNpWrFsNpIpv4GetEcmpGroups  sFsNpIpv4GetEcmpGroups;
#ifdef MBSM_WANTED
    tIpNpWrFsNpMbsmIpInit  sFsNpMbsmIpInit;
    tIpNpWrFsNpMbsmOspfInit  sFsNpMbsmOspfInit;
    tIpNpWrFsNpMbsmDhcpSrvInit  sFsNpMbsmDhcpSrvInit;
    tIpNpWrFsNpMbsmDhcpRlyInit  sFsNpMbsmDhcpRlyInit;
    tIpNpWrFsNpMbsmIpv4CreateIpInterface  sFsNpMbsmIpv4CreateIpInterface;
    tIpNpWrFsNpMbsmIpv4CreateL3SubInterface sFsNpMbsmIpv4CreateL3SubInterface;
    tIpNpWrFsNpMbsmIpv4ArpAdd  sFsNpMbsmIpv4ArpAdd;
    tIpNpWrFsNpMbsmIpv4UcAddRoute  sFsNpMbsmIpv4UcAddRoute;
    tIpNpWrFsNpMbsmIpv4VrmEnableVr  sFsNpMbsmIpv4VrmEnableVr;
    tIpNpWrFsNpMbsmIpv4BindIfToVrId  sFsNpMbsmIpv4BindIfToVrId;
    tIpNpWrFsNpMbsmIpv4UcAddTrap  sFsNpMbsmIpv4UcAddTrap;
    tIpNpWrFsNpMbsmIpv4MapVlansToIpInterface  sFsNpMbsmIpv4MapVlansToIpInterface;
    tIpNpWrFsNpMbsmIpv4VrfArpAdd  sFsNpMbsmIpv4VrfArpAdd;
    tIpNpWrFsNpMbsmIpv4CreateVrrpInterface  sFsNpMbsmIpv4CreateVrrpInterface;
    tIpNpWrFsNpMbsmIpv4VrrpInstallFilter  sFsNpMbsmIpv4VrrpInstallFilter;
    tIpNpWrFsNpMbsmIpv4ArpAddition  sFsNpMbsmIpv4ArpAddition;
    tIpNpWrFsNpMbsmIpv4VrfArpAddition  sFsNpMbsmIpv4VrfArpAddition;
    tIpNpWrFsNpMbsmIsisHwProgram       sFsNpMbsmIsisProgram;
#endif /* MBSM_WANTED */
    tIpNpWrFsNpIpv4DeleteSecIpInterface  sFsNpIpv4DeleteSecIpInterface;
    }unOpCode;

#define  IpNpFsNpIpv4CreateIpInterface  unOpCode.sFsNpIpv4CreateIpInterface;
#define  IpNpFsNpIpv4CreateL3SubInterface unOpCode.sFsNpIpv4CreateL3SubInterface;
#define  IpNpFsNpIpv4ModifyIpInterface  unOpCode.sFsNpIpv4ModifyIpInterface;
#define  IpNpFsNpIpv4DeleteIpInterface  unOpCode.sFsNpIpv4DeleteIpInterface;
#define  IpNpFsNpIpv4DeleteL3SubInterface  unOpCode.sFsNpIpv4DeleteL3SubInterface;
#define  IpNpFsNpIpv4DeleteSecIpInterface  unOpCode.sFsNpIpv4DeleteSecIpInterface;
#define  IpNpFsNpIpv4UpdateIpInterfaceStatus  unOpCode.sFsNpIpv4UpdateIpInterfaceStatus;
#define  IpNpFsNpIpv4SetForwardingStatus  unOpCode.sFsNpIpv4SetForwardingStatus;
#define  IpNpFsNpIpv4UcDelRoute  unOpCode.sFsNpIpv4UcDelRoute;
#define  IpNpFsNpIpv4ArpAdd  unOpCode.sFsNpIpv4ArpAdd;
#define  IpNpFsNpIpv4ArpModify  unOpCode.sFsNpIpv4ArpModify;
#define  IpNpFsNpIpv4ArpDel  unOpCode.sFsNpIpv4ArpDel;
#define  IpNpFsNpIpv4CheckHitOnArpEntry  unOpCode.sFsNpIpv4CheckHitOnArpEntry;
#define  IpNpFsNpIpv4UcAddRoute  unOpCode.sFsNpIpv4UcAddRoute;
#define  IpNpFsNpIpv4VrfArpAdd  unOpCode.sFsNpIpv4VrfArpAdd;
#define  IpNpFsNpIpv4VrfArpModify  unOpCode.sFsNpIpv4VrfArpModify;
#define  IpNpFsNpIpv4VrfArpDel  unOpCode.sFsNpIpv4VrfArpDel;
#define  IpNpFsNpIpv4VrfCheckHitOnArpEntry  unOpCode.sFsNpIpv4VrfCheckHitOnArpEntry;
#define  IpNpFsNpIpv4VrfClearArpTable  unOpCode.sFsNpIpv4VrfClearArpTable;
#define  IpNpFsNpIpv4VrfGetSrcMovedIpAddr  unOpCode.sFsNpIpv4VrfGetSrcMovedIpAddr;
#define  IpNpFsNpCfaVrfSetDlfStatus  unOpCode.sFsNpCfaVrfSetDlfStatus;
#define  IpNpFsNpL3Ipv4VrfArpAdd  unOpCode.sFsNpL3Ipv4VrfArpAdd;
#define  IpNpFsNpL3Ipv4VrfArpModify  unOpCode.sFsNpL3Ipv4VrfArpModify;
#define  IpNpFsNpIpv4VrfArpGet  unOpCode.sFsNpIpv4VrfArpGet;
#define  IpNpFsNpIpv4VrfArpGetNext  unOpCode.sFsNpIpv4VrfArpGetNext;
#define  IpNpFsNpIpv4VrrpIntfCreateWr  unOpCode.sFsNpIpv4VrrpIntfCreateWr;
#define  IpNpFsNpIpv4CreateVrrpInterface  unOpCode.sFsNpIpv4CreateVrrpInterface;
#define  IpNpFsNpIpv4VrrpIntfDeleteWr  unOpCode.sFsNpIpv4VrrpIntfDeleteWr;
#define  IpNpFsNpIpv4DeleteVrrpInterface  unOpCode.sFsNpIpv4DeleteVrrpInterface;
#define  IpNpFsNpIpv4GetVrrpInterface  unOpCode.sFsNpIpv4GetVrrpInterface;
#define IpNpFsNpVrrpHwProgram  unOpCode.sFsNpVrrpHwProgram;
#ifdef  ISIS_WANTED
#define  IpNpFsNpIsisHwProgram unOpCode.sFsNpIsisHwProgram;
#endif /*ISIS_WANTED*/
#define  IpNpFsNpIpv4IsRtPresentInFastPath  unOpCode.sFsNpIpv4IsRtPresentInFastPath;
#define  IpNpFsNpIpv4GetStats  unOpCode.sFsNpIpv4GetStats;
#define  IpNpFsNpIpv4VrmEnableVr  unOpCode.sFsNpIpv4VrmEnableVr;
#define  IpNpFsNpIpv4BindIfToVrId  unOpCode.sFsNpIpv4BindIfToVrId;
#define  IpNpFsNpIpv4GetNextHopInfo  unOpCode.sFsNpIpv4GetNextHopInfo;
#define  IpNpFsNpIpv4UcAddTrap  unOpCode.sFsNpIpv4UcAddTrap;
#define  IpNpFsNpIpv4ClearFowardingTbl  unOpCode.sFsNpIpv4ClearFowardingTbl;
#define  IpNpFsNpIpv4GetSrcMovedIpAddr  unOpCode.sFsNpIpv4GetSrcMovedIpAddr;
#define  IpNpFsNpIpv4MapVlansToIpInterface  unOpCode.sFsNpIpv4MapVlansToIpInterface;
#ifdef NAT_WANTED
#define  IpNpFsNpNatDisableOnIntf  unOpCode.sFsNpNatDisableOnIntf;
#define  IpNpFsNpNatEnableOnIntf  unOpCode.sFsNpNatEnableOnIntf;
#endif /* NAT_WANTED */
#define  IpNpFsNpIpv4IntfStatus  unOpCode.sFsNpIpv4IntfStatus;
#define  IpNpFsNpIpv4ArpGet  unOpCode.sFsNpIpv4ArpGet;
#define  IpNpFsNpIpv4ArpGetNext  unOpCode.sFsNpIpv4ArpGetNext;
#define  IpNpFsNpIpv4UcGetRoute  unOpCode.sFsNpIpv4UcGetRoute;
#define  IpNpFsNpL3Ipv4ArpAdd  unOpCode.sFsNpL3Ipv4ArpAdd;
#define  IpNpFsNpL3Ipv4ArpModify  unOpCode.sFsNpL3Ipv4ArpModify;
#define  IpNpFsNpIpv4L3IpInterface  unOpCode.sFsNpIpv4L3IpInterface;
#define  IpNpFsNpIpv4GetEcmpGroups  unOpCode.sFsNpIpv4GetEcmpGroups;
#ifdef MBSM_WANTED
#define  IpNpFsNpMbsmIpInit  unOpCode.sFsNpMbsmIpInit;
#define  IpNpFsNpMbsmOspfInit  unOpCode.sFsNpMbsmOspfInit;
#define  IpNpFsNpMbsmDhcpSrvInit  unOpCode.sFsNpMbsmDhcpSrvInit;
#define  IpNpFsNpMbsmDhcpRlyInit  unOpCode.sFsNpMbsmDhcpRlyInit;
#define  IpNpFsNpMbsmIpv4CreateIpInterface  unOpCode.sFsNpMbsmIpv4CreateIpInterface;
#define  IpNpFsNpMbsmIpv4CreateL3SubInterface unOpCode.sFsNpMbsmIpv4CreateL3SubInterface;
#define  IpNpFsNpMbsmIpv4ArpAdd  unOpCode.sFsNpMbsmIpv4ArpAdd;
#define  IpNpFsNpMbsmIpv4UcAddRoute  unOpCode.sFsNpMbsmIpv4UcAddRoute;
#define  IpNpFsNpMbsmIpv4VrmEnableVr  unOpCode.sFsNpMbsmIpv4VrmEnableVr;
#define  IpNpFsNpMbsmIpv4BindIfToVrId  unOpCode.sFsNpMbsmIpv4BindIfToVrId;
#define  IpNpFsNpMbsmIpv4UcAddTrap  unOpCode.sFsNpMbsmIpv4UcAddTrap;
#define  IpNpFsNpMbsmIpv4MapVlansToIpInterface  unOpCode.sFsNpMbsmIpv4MapVlansToIpInterface;
#define  IpNpFsNpMbsmIpv4VrfArpAdd  unOpCode.sFsNpMbsmIpv4VrfArpAdd;
#define  IpNpFsNpMbsmIpv4CreateVrrpInterface  unOpCode.sFsNpMbsmIpv4CreateVrrpInterface;
#define  IpNpFsNpMbsmIpv4VrrpInstallFilter  unOpCode.sFsNpMbsmIpv4VrrpInstallFilter;
#define  IpNpFsNpMbsmIpv4ArpAddition  unOpCode.sFsNpMbsmIpv4ArpAddition;
#define  IpNpFsNpMbsmIpv4VrfArpAddition  unOpCode.sFsNpMbsmIpv4VrfArpAddition;
#define  IpNpFsNpMbsmIsisHwProgram      unOpCode.sFsNpMbsmIsisProgram;
#endif /* MBSM_WANTED */
} tIpNpModInfo;


/* Function Prototype for Generic NP OpCode specific wrappers
 * defined in Ipnpapi.c */

UINT1 IpFsNpIpInit PROTO ((VOID));
UINT1 IpFsNpOspfInit PROTO ((VOID));
UINT1 IpFsNpOspfDeInit PROTO ((VOID));
UINT1 IpFsNpDhcpSrvInit PROTO ((VOID));
UINT1 IpFsNpDhcpSrvDeInit PROTO ((VOID));
UINT1 IpFsNpDhcpRlyInit PROTO ((VOID));
UINT1 IpFsNpDhcpRlyDeInit PROTO ((VOID));
UINT1 IpFsNpRipInit PROTO ((VOID));
UINT1 IpFsNpRipDeInit PROTO ((VOID));
UINT1 IpFsNpIpv4CreateIpInterface PROTO ((UINT4 u4VrId, UINT1 * pu1IfName, UINT4 u4CfaIfIndex, UINT4 u4IpAddr, UINT4 u4IpSubNetMask, UINT2 u2VlanId, UINT1 * au1MacAddr));
UINT1 IpFsNpIpv4ModifyIpInterface PROTO ((UINT4 u4VrId, UINT1 * pu1IfName, UINT4 u4CfaIfIndex, UINT4 u4IpAddr, UINT4 u4IpSubNetMask, UINT2 u2VlanId, UINT1 * au1MacAddr));
UINT1 IpFsNpIpv4DeleteIpInterface PROTO ((UINT4 u4VrId, UINT1 * pu1IfName, UINT4 u4IfIndex, UINT2 u2VlanId));
UINT1 IpFsNpIpv4UpdateIpInterfaceStatus PROTO ((UINT4 u4VrId, UINT1 * pu1IfName, UINT4 u4CfaIfIndex, UINT4 u4IpAddr, UINT4 u4IpSubNetMask, UINT2 u2VlanId, UINT1 * au1MacAddr, UINT4 u4Status));
UINT1 IpFsNpIpv4SetForwardingStatus PROTO ((UINT4 u4VrId, UINT1 u1Status));
UINT1 IpFsNpIpv4ClearRouteTable PROTO ((VOID));
UINT1 IpFsNpIpv4ClearArpTable PROTO ((VOID));
UINT1 IpFsNpIpv4UcDelRoute PROTO ((UINT4 u4VrId, UINT4 u4IpDestAddr, UINT4 u4IpSubNetMask, tFsNpNextHopInfo routeEntry, INT4 * pi4FreeDefIpB4Del));
UINT1 IpFsNpIpv4ArpAdd PROTO ((tNpVlanId u2VlanId, UINT4 u4IfIndex, UINT4 u4IpAddr, UINT1 * pMacAddr, UINT1 * pu1IfName, INT1 i1State, UINT4 * pu4TblFull));
UINT1 IpFsNpIpv4ArpModify PROTO ((tNpVlanId u2VlanId, UINT4 u4IfIndex, UINT4 u4PhyIfIndex, UINT4 u4IpAddr, UINT1 * pMacAddr, UINT1 * pu1IfName, INT1 i1State));
UINT1 IpFsNpIpv4ArpDel PROTO ((UINT4 u4IpAddr, UINT1 * pu1IfName, INT1 i1State));
UINT1 IpFsNpIpv4CheckHitOnArpEntry PROTO ((UINT4 u4IpAddress, UINT1 u1NextHopFlag));
UINT1 IpFsNpIpv4SyncVlanAndL3Info PROTO ((VOID));
#ifndef NPSIM_WANTED
UINT1 IpFsNpIpv4UcAddRoute PROTO ((UINT4 u4VrId, UINT4 u4IpDestAddr, UINT4 u4IpSubNetMask, tFsNpNextHopInfo * pRouteEntry, UINT1 * pbu1TblFull)); 
#else   
UINT1 IpFsNpIpv4UcAddRoute PROTO ((UINT4 u4VrId, UINT4 u4IpDestAddr, UINT4 u4IpSubNetMask, tFsNpNextHopInfo routeEntry, UINT1 * pbu1TblFull)); 
#endif
UINT1 IpFsNpIpv4VrfArpAdd PROTO ((UINT4 u4VrId, tNpVlanId u2VlanId, UINT4 u4IfIndex, UINT4 u4IpAddr, UINT1 * pMacAddr, UINT1 * pu1IfName, INT1 i1State, UINT4 * pu4TblFull));
UINT1 IpFsNpIpv4VrfArpModify PROTO ((UINT4 u4VrId, tNpVlanId u2VlanId, UINT4 u4IfIndex, UINT4 u4PhyIfIndex, UINT4 u4IpAddr, UINT1 * pMacAddr, UINT1 * pu1IfName, INT1 i1State));
UINT1 IpFsNpIpv4VrfArpDel PROTO ((UINT4 u4VrId, UINT4 u4IpAddr, UINT1 * pu1IfName, INT1 i1State));
UINT1 IpFsNpIpv4VrfCheckHitOnArpEntry PROTO ((UINT4 u4VrId, UINT4 u4IpAddress, UINT1 u1NextHopFlag));
UINT1 IpFsNpIpv4VrfClearArpTable PROTO ((UINT4 u4VrId));
UINT1 IpFsNpIpv4VrfGetSrcMovedIpAddr PROTO ((UINT4 * pu4VrId, UINT4 * pu4IpAddress));
UINT1 IpFsNpCfaVrfSetDlfStatus PROTO ((UINT4 u4VrfId, UINT1 u1Status));
UINT1 IpFsNpL3Ipv4VrfArpAdd PROTO ((UINT4 u4VrfId, UINT4 u4IfIndex, UINT4 u4IpAddr, UINT1 * pMacAddr, UINT1 * pu1IfName, INT1 i1State, UINT1 * pbu1TblFull));
UINT1 IpFsNpL3Ipv4VrfArpModify PROTO ((UINT4 u4VrfId, UINT4 u4IfIndex, UINT4 u4IpAddr, UINT1 * pMacAddr, UINT1 * pu1IfName, INT1 i1State));
UINT1 IpFsNpIpv4VrfArpGet PROTO ((tNpArpInput ArpNpInParam, tNpArpOutput * pArpNpOutParam));
UINT1 IpFsNpIpv4VrfArpGetNext PROTO ((tNpArpInput ArpNpInParam, tNpArpOutput * pArpNpOutParam));
#ifdef VRRP_WANTED
UINT1 IpFsNpIpv4VrrpIntfCreateWr PROTO ((tNpVlanId u2VlanId, UINT4 u4IfIndex, UINT4 u4IpAddr, UINT1 * au1MacAddr));
UINT1 IpFsNpIpv4CreateVrrpInterface PROTO ((tNpVlanId u2VlanId, UINT4 u4IpAddr, UINT1 * au1MacAddr));
UINT1 IpFsNpIpv4VrrpIntfDeleteWr PROTO ((tNpVlanId u2VlanId, UINT4 u4IfIndex, UINT4 u4IpAddr, UINT1 * au1MacAddr));
UINT1 IpFsNpIpv4DeleteVrrpInterface PROTO ((tNpVlanId u2VlanId, UINT4 u4IpAddr, UINT1 * au1MacAddr));
UINT1 IpFsNpIpv4GetVrrpInterface PROTO ((INT4 i4IfIndex, INT4 i4VrId));
UINT1 IpFsNpIpv4VrrpInstallFilter PROTO ((VOID));
UINT1 IpFsNpIpv4VrrpRemoveFilter PROTO ((VOID));
UINT1 IpFsNpVrrpHwProgram PROTO ((UINT1 u1NpAction,tVrrpNwIntf *pVrrpNwIntf));
#endif /* VRRP_WANTED */

#ifdef ISIS_WANTED
UINT1 IpFsNpIsisProgram PROTO ((UINT1 u1Status));
#endif /* ISIS_WANTED */

UINT1 IpFsNpIpv4IsRtPresentInFastPath PROTO ((UINT4 u4DestAddr, UINT4 u4Mask));
UINT1 IpFsNpIpv4GetStats PROTO ((INT4 i4StatType, UINT4 * pu4RetVal));
UINT1 IpFsNpIpv4VrmEnableVr PROTO ((UINT4 u4VrId, UINT1 u1Status));
UINT1 IpFsNpIpv4BindIfToVrId PROTO ((UINT4 u4IfIndex, UINT4 u4VrId));
UINT1 IpFsNpIpv4GetNextHopInfo PROTO ((UINT4 u4VrId, UINT4 u4Dest, UINT4 u4DestMask, tFsNpNextHopInfo * pNextHopInfo));
UINT1 IpFsNpIpv4UcAddTrap PROTO ((UINT4 u4VrId, UINT4 u4Dest, UINT4 u4DestMask, tFsNpNextHopInfo nextHopInfo));
UINT1 IpFsNpIpv4ClearFowardingTbl PROTO ((UINT4 u4VrId));
UINT1 IpFsNpIpv4GetSrcMovedIpAddr PROTO ((UINT4 * pu4IpAddress));
UINT1 IpFsNpIpv4MapVlansToIpInterface PROTO ((tNpIpVlanMappingInfo * pPvlanMappingInfo));
#ifdef NAT_WANTED
UINT1 IpFsNpNatDisableOnIntf PROTO ((INT4 i4Intf));
UINT1 IpFsNpNatEnableOnIntf PROTO ((INT4 i4Intf));
#endif /* NAT_WANTED */
UINT1 IpFsNpIpv4IntfStatus PROTO ((UINT4 u4VrId, UINT4 u4IfStatus, tFsNpIp4IntInfo * pIpIntInfo));
UINT1 IpFsNpIpv4ArpGet PROTO ((tNpArpInput ArpNpInParam, tNpArpOutput * pArpNpOutParam));
UINT1 IpFsNpIpv4ArpGetNext PROTO ((tNpArpInput ArpNpInParam, tNpArpOutput * pArpNpOutParam));
UINT1 IpFsNpIpv4UcGetRoute PROTO ((tNpRtmInput RtmNpInParam, tNpRtmOutput * pRtmNpOutParam));
UINT1 IpFsNpL3Ipv4ArpAdd PROTO ((UINT4 u4IfIndex, UINT4 u4IpAddr, UINT1 * pMacAddr, UINT1 * pu1IfName, INT1 i1State, UINT1 * pbu1TblFull));
UINT1 IpFsNpL3Ipv4ArpModify PROTO ((UINT4 u4IfIndex, UINT4 u4IpAddr, UINT1 * pMacAddr, UINT1 * pu1IfName, INT1 i1State));
UINT1 IpFsNpIpv4L3IpInterface PROTO ((tL3Action L3Action, tFsNpL3IfInfo * pFsNpL3IfInfo));
UINT1 IpFsNpIpv4GetEcmpGroups PROTO ((UINT4 *pu4EcmpGroups));
#ifdef MBSM_WANTED
UINT1 IpFsNpMbsmIpInit PROTO ((tMbsmSlotInfo * pSlotInfo));
UINT1 IpFsNpMbsmOspfInit PROTO ((tMbsmSlotInfo * pSlotInfo));
UINT1 IpFsNpMbsmDhcpSrvInit PROTO ((tMbsmSlotInfo * pSlotInfo));
UINT1 IpFsNpMbsmDhcpRlyInit PROTO ((tMbsmSlotInfo * pSlotInfo));
UINT1 IpFsNpMbsmIpv4CreateIpInterface PROTO ((UINT4 u4VrId, UINT4 u4CfaIfIndex, UINT4 u4IpAddr, UINT4 u4IpSubNetMask, UINT2 u2VlanId, UINT1 * au1MacAddr, tMbsmSlotInfo * pSlotInfo));
UINT1 IpFsNpMbsmIpv4ArpAdd PROTO ((tNpVlanId u2VlanId, UINT4 u4IpAddr, UINT1 * pMacAddr, UINT1 * pbu1TblFull, tMbsmSlotInfo * pSlotInfo));
UINT1 IpFsNpMbsmIpv4UcAddRoute PROTO ((UINT4 u4VrId, UINT4 u4IpDestAddr, UINT4 u4IpSubNetMask,
#ifndef NPSIM_WANTED
                                       tFsNpNextHopInfo * pRouteEntry,
#else
                                       tFsNpNextHopInfo   routeEntry,
#endif
                                       UINT1 * pbu1TblFull, tMbsmSlotInfo * pSlotInfo));
UINT1 IpFsNpMbsmIpv4VrmEnableVr PROTO ((UINT4 u4VrId, UINT1 u1Status, tMbsmSlotInfo * pSlotInfo));
UINT1 IpFsNpMbsmIpv4BindIfToVrId PROTO ((UINT4 u4IfIndex, UINT4 u4VrId, tMbsmSlotInfo * pSlotInfo));
UINT1 IpFsNpMbsmIpv4UcAddTrap PROTO ((UINT4 u4VrId, UINT4 u4Dest, UINT4 u4DestMask, tFsNpNextHopInfo nextHopInfo, tMbsmSlotInfo * pSlotInfo));
UINT1 IpFsNpMbsmIpv4MapVlansToIpInterface PROTO ((tNpIpVlanMappingInfo * pPvlanMappingInfo, tMbsmSlotInfo * pSlotInfo));
UINT1 IpFsNpMbsmIpv4VrfArpAdd PROTO ((UINT4 u4VrId, tNpVlanId u2VlanId, UINT4 u4IpAddr, UINT1 * pMacAddr, UINT1 * pbu1TblFull, tMbsmSlotInfo * pSlotInfo));
#ifdef VRRP_WANTED
UINT1 IpFsNpMbsmIpv4CreateVrrpInterface PROTO ((tNpVlanId u2VlanId, UINT4 u4IpAddr, UINT1 * au1MacAddr, tMbsmSlotInfo * pSlotInfo));
UINT1 IpFsNpMbsmIpv4VrrpInstallFilter PROTO ((tMbsmSlotInfo * pSlotInfo));
#endif /* VRRP_WANTED */
UINT1 IpFsNpMbsmIpv4ArpAddition PROTO ((tNpVlanId u2VlanId, UINT4 u4IpAddr, UINT1 * pMacAddr, UINT1 * pbu1TblFull, UINT4 u4IfIndex, UINT1 * pu1IfName, INT1 i1State, tMbsmSlotInfo * pSlotInfo));
UINT1 IpFsNpMbsmIpv4VrfArpAddition PROTO ((UINT4 u4VrId, tNpVlanId u2VlanId, UINT4 u4IpAddr, UINT1 * pMacAddr, UINT1 * pbu1TblFull, UINT4 u4IfIndex, UINT1 * pu1IfName, INT1 i1State, tMbsmSlotInfo * pSlotInfo));
#ifdef ISIS_WANTED
UINT1 IpFsNpMbsmIsisProgram PROTO ((tMbsmSlotInfo * pSlotInfo, UINT1 u1Status));
#endif
#endif /* MBSM_WANTED */

#ifdef CFA_WANTED
UINT1 IpFsCfaHwRemoveIpNetRcvdDlfInHash PROTO ((UINT4 u4ContextId,UINT4 u4IpNet, UINT4 u4IpMask));
#endif

#endif /* __IP_NP_WR_H__ */
