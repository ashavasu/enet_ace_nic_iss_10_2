/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: la.h,v 1.83 2018/01/11 11:18:53 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              by  external modules.
 *
 *******************************************************************/
#ifndef  _LA_H
#define  _LA_H

#include "rmgr.h"
#include "cust.h"

#define LA_LOCK() LaLock()
#define LA_UNLOCK() LaUnLock()

#define LA_AGGMAC_DYNAMIC 1
#define LA_AGGMAC_FORCE   2



#define LA_DESTADDR_OFFSET                       0
#define LA_PORT_STANDBY                          1
#define LA_ACTOR                                 1
#define LA_PARTNER                               2 
#define LA_ETH_ADDR_SIZE                         6
#define LA_ETH_ADDR_OFFSET                       6
#define LA_PROTOCOL_TYPE_OFFSET                  12
#define LA_PROTOCOL_SUBTYPE_OFFSET               14
#define LA_TLV_TYPE_OFFSET                       16
#define LA_LACP_SUBTYPE                          0x01
#define LA_MARKER_SUBTYPE                        0x02

#define LA_SELECT_SRC_MAC                        0x00000001 
#define LA_SELECT_DST_MAC                        0x00000002
#define LA_SELECT_SRC_DST_MAC                    0x00000003


/* Configuration Parameters */
#define   LA_PORT_LIST_SIZE  ((LA_MAX_PORTS + 31)/32 * 4)

#define LA_SELECT_SRC_IP                         0x00000004
#define LA_SELECT_DST_IP                         0x00000008
#define LA_SELECT_SRC_DST_IP                     0x0000000C

#define LA_SELECT_VLAN_ID                        0x00000010

#define LA_SELECT_MAC_SRC_VID                    0x00000020
#define LA_SELECT_MAC_DST_VID                    0x00000040
#define LA_SELECT_MAC_SRC_DST_VID                0x00000060

#define LA_SELECT_ENHANCED                       0x00000080

#define LA_SELECT_DST_IP6                        0x00000100
#define LA_SELECT_SRC_IP6                        0x00000200

#define LA_SELECT_L3_PROTOCOL                    0x00000400

#define LA_SELECT_DST_L4_PORT                    0x00000800
#define LA_SELECT_SRC_L4_PORT                    0x00001000
#define LA_SELECT_SA_DA_IP_PORT_PROTO            0x00001C1F 

#define LA_SELECT_MPLS_VC_LABEL                  0x00002000
#define LA_SELECT_MPLS_TUNNEL_LABEL              0x00004000
#define LA_SELECT_MPLS_VC_TUNNEL                 0x00006000

#define LA_SELECT_SERVICE_INSTANCE               0x00008000

#define LA_SELECT_RANDOMIZED                     0x00010000

#define LA_SELECT_MAX_RANGE     (LA_SELECT_SRC_MAC | LA_SELECT_DST_MAC | LA_SELECT_SRC_DST_MAC | LA_SELECT_SRC_IP | \
                                LA_SELECT_DST_IP | LA_SELECT_SRC_DST_IP | LA_SELECT_VLAN_ID | LA_SELECT_SERVICE_INSTANCE | \
                                LA_SELECT_MAC_SRC_VID | LA_SELECT_MAC_DST_VID | LA_SELECT_MAC_SRC_DST_VID | \
                                LA_SELECT_DST_IP6 | LA_SELECT_SRC_IP6 | LA_SELECT_L3_PROTOCOL | LA_SELECT_DST_L4_PORT | \
                                LA_SELECT_SRC_L4_PORT | LA_SELECT_MPLS_VC_LABEL | LA_SELECT_MPLS_TUNNEL_LABEL | \
                                LA_SELECT_MPLS_VC_TUNNEL | LA_SELECT_ENHANCED | LA_SELECT_RANDOMIZED)

typedef tCRU_BUF_CHAIN_HEADER   tLaBufChainHeader;

/* Return values for the LA module */
enum {
   LA_SUCCESS = SUCCESS,
   LA_FAILURE = FAILURE
};

typedef enum {
   LA_START    = 1,
   LA_SHUTDOWN = 2
}tLaSysControl;

typedef enum {
   LA_MODE_LACP = 1,
   LA_MODE_MANUAL = 2,
   LA_MODE_DISABLED  = 3,
   LA_MODE_INVALID
}tLaLacpMode;

typedef enum {
   LA_SA    = 1,
   LA_DA    = 2,
   LA_SA_DA = 3,
   LA_SA_IP = 4,
   LA_DA_IP = 5,
   LA_SA_DA_IP = 6,
   LA_VLAN_ID = 7,
   LA_SA_DA_IP_PORT_PROTO = 8,
   LA_ISID = 9,
   LA_SA_VID = 10,
   LA_DA_VID = 11,
   LA_SA_DA_VID = 12,
   LA_DA_IP6 = 13,
   LA_SA_IP6 = 14,
   LA_L3_PROTOCOL = 15,
   LA_DA_L4_PORT = 16,
   LA_SA_L4_PORT = 17,
   LA_MPLS_VC_LABEL = 18,
   LA_MPLS_TUNNEL_LABEL = 19,
   LA_MPLS_VC_TUNNEL = 20,
   LA_ENHANCED = 21,
   LA_RANDOMIZED = 22,
   LA_OTHER_OPTIONS
}tLaLinkSelectPolicy;


typedef struct {
   UINT1    *pu1DestHwAddr;
   UINT4    u4PktSize;
   UINT2    u2Protocol;
   UINT1    u1EncapType;
   UINT1    u1Reserved;
}tLaPktParams;

typedef enum {
   LA_FALSE = 0,
   LA_TRUE  = 1
}tLaBoolean;

typedef enum {
   LA_PASSIVE = LA_FALSE,
   LA_ACTIVE  = LA_TRUE
} tLaLacpActivity;

typedef enum {
   LA_LONG_TIMEOUT  = LA_FALSE,
   LA_SHORT_TIMEOUT = LA_TRUE
}tLaLacpTimeout; 

/*****************************************************************************/
/* tLaLacPortState                                                           */
/* State Variables for the Actor and the partner                             */
/*****************************************************************************/

typedef struct LaLacPortState {
   tLaLacpActivity     LaLacpActivity;
   tLaLacpTimeout      LaLacpTimeout;
   tLaBoolean            LaAggregation;
   tLaBoolean            LaSynchronization;
   tLaBoolean            LaCollecting;
   tLaBoolean            LaDistributing;
   tLaBoolean            LaDefaulted;
   tLaBoolean            LaExpired;
}tLaLacPortState;


typedef enum {
   LA_TEN_MBPS,
   LA_HUNDRED_MBPS,
   LA_THOUSAND_MBPS,
   LA_TEN_GBPS,
   LA_INVALID_RATE
}tLaPhyMediaDataRate;


typedef enum {
   LA_ENABLED  = 1,
   LA_DISABLED = 2
}tLaEnable;

typedef enum {
   LA_DLAG_ENABLED  = 1,
   LA_DLAG_DISABLED = 2
}tLaDLAGEnable;

typedef enum {
   LA_MCLAG_ENABLED  = 1,
   LA_MCLAG_DISABLED = 2
}tLaMCLAGEnable;

typedef enum {
   LA_ICCH_MASTER  = 1,
   LA_ICCH_SLAVE   = 2
}tLaMCLAGRolePlayed;



typedef enum {
   LA_DLAG_REDUNDANCY_ON  = 1,
   LA_DLAG_REDUNDANCY_OFF = 2
}tLaDLAGRedundancy;

typedef enum {
   LA_DATA  = 1,
   LA_CONTROL = 2,
   LA_DROP = 3,
   LA_IGNORE = 4
}tLaParse;

/* LA call back events */
typedef enum {
    LA_PKT_RX_EVENT,
    LA_PKT_TX_EVENT,
    LA_GET_DISTRIB_PORT_EVENT, 
    LA_GET_ACTIVE_PORT_EVENT,
    LA_MAX_CALLBACK_EVENTS
}tLaCallBackEvents;


enum {

   LA_SLOW_PROT_ADDR_DWORD = 0x0180C200,
   LA_SLOW_PROT_ADDR_WORD = 0x0002,
   LA_SLOW_PROT_TYPE = 0x8809
};

enum {
    LA_AGG_NOT_CAPABLE = 0,
    LA_AGG_CAPABLE     = 1
};

#ifdef L2RED_WANTED
INT4 LaRedRcvPktFromRm (UINT1 u1Event, tRmMsg *pData, UINT2 u2DataLen);
#endif

INT4 LaLock PROTO ((VOID));

INT4 LaUnLock PROTO ((VOID));

INT4 LaUpdatePortStatus     PROTO ((UINT2 u2PortIndex , UINT1 u1PortOperStatus));

INT4 LaDefaultSettings(VOID);
tLaEnable
LaGetLaEnableStatus         PROTO ((VOID));

INT4
LaCreatePort                PROTO ((UINT2 u2PortIndex));

INT4
LaMapPort                PROTO ((UINT2 u2PortIndex));

INT4 
LaDeletePortChannelInterface PROTO ((UINT4 u4IfIndex));

INT4 
LaUpdatePortChannelAdminStatus PROTO ((UINT2 u2IfIndex, UINT1 u1AdminStatus, UINT1 *pu1OperStatus));

INT4 
LaDeletePort                PROTO ((UINT2 u2PortIndex)); 

INT4
LaUnmapPort                PROTO ((UINT2 u2PortIndex)); 

INT4
LaHandleOutFrame            PROTO ((tLaBufChainHeader * pBuf, UINT2 u2AggIndex,
                                    UINT1 * pu1DestHwAddr, UINT4 u4PktSize,
                                    UINT2 u2Protocol, UINT1 u1EncapType));

tLaBoolean LaIsLaStarted PROTO ((VOID));

VOID
LaUpdateRestoreMtu (UINT2 u2IfIndex, UINT4 u4IfMtu);

VOID
LaUpdatePauseAdminMode (UINT2 u2IfIndex, UINT4 i4IfPauseAdminMode);

VOID
LaUpdateAggPauseAdminMode (UINT2 u2IfIndex, UINT4 i4AggPauseAdminMode);

INT4
LaUpdateActorPortChannelMac(UINT2 u2IfIndex, 
                            tMacAddr SetValFsLaPortChannelAdminMacAddress);

VOID LaGetLacpPortState PROTO ((UINT2 u2IfIndex,
                               tLaLacPortState ** pLaLacPortState,
                               UINT4 u4Value));
INT4 LaIsPortInPortChannel (UINT4 u4IfIndex);

INT4 LaGetNextAggPort (UINT2 u2AggIndex, UINT2 u2Port, UINT2 *pu2NextPort);

INT4 LaSetDefaultPropForAggregator (UINT2 u2AggIndex);

INT4 LaCheckMclagSystemId PROTO ((VOID)); 
  
INT4 LaCheckMclagSystemIdForPortChannel (UINT4 u4IfIndex); 


/* Prototypes for LA interworking with other modules */
VOID LaMain  PROTO ((INT1 *pi1Param));

INT4 LaHandleIncomingFrame (tLaBufChainHeader * pBuf, UINT2 u2PortIndex,
                            UINT2 *pu2AggPortIndex);
INT4 LaHandleOutgoingFrame (tLaBufChainHeader * pBuf, UINT2 u2AggIndex,
                            UINT2 u2Protocol, UINT1 u1EncapType, 
                            UINT2 *pu2PortIndex);

INT4
LaCallBackRegister (UINT4 u4Event, tFsCbInfo *pFsCbInfo);

INT1
nmhValidateIndexInstanceDot3adAggPortTable (INT4 i4Dot3adAggPortIndex);

INT1
nmhGetFirstIndexDot3adAggPortTable (INT4 *pi4Dot3adAggPortIndex);
#ifdef MBSM_WANTED
INT4  LaMbsmProcessUpdateMessage PROTO ((tMbsmProtoMsg *,INT4 i4Event));
#endif
INT4 LaShutDown (VOID);
INT4 LaInit (VOID);
INT4 LaRestartModule (VOID);
INT4 LaIsLaCtrlFrame (tCRU_BUF_CHAIN_DESC *pBuf, tMacAddr DestAddr, UINT2 u4IfIndex);

INT4 LaHandleReceivedFrame (tLaBufChainHeader * pBuf, UINT2 u2PortIndex,
                       UINT2 *pu2AggPortIndex,UINT1 u1FrameType);

VOID
LaApiIsMclagInterface (UINT4 u4IfIndex, UINT1 *pu1IsMclagEnabled);

#ifdef EVPN_VXLAN_WANTED
VOID
LaApiGetPortChannelMCLAGSystemID (UINT4 u4IfIndex,tMacAddr *pRetValEvpnMCLAGSystemID);

VOID
LaApiGetPortChannelMCLAGSystemPriority (UINT4 u4IfIndex,
                                                INT4 *pi4EvpnMCLAGSystemPriority);
#endif
INT4
LaApiDisableStpOnMclagInt (VOID);

/* Added for pseudowire redundancy */

#define LA_MAX_DLAG_PORTS       8  
#define LA_AC_OPER_UP           CFA_IF_UP
#define LA_AC_OPER_DOWN         CFA_IF_DOWN

typedef struct _DlagAcStatusMsg
{
          UINT2               au2PortIndex [LA_MAX_DLAG_PORTS];
          UINT1               u1DlagPswStatus;
          UINT1               u1NoOfPorts;
          UINT1               au1Pad[2];
} tDlagAcStatusMsg;

PUBLIC VOID
L2VpnNotifyAcUpDown (tDlagAcStatusMsg   *pDlagAcStatusMsg);
VOID
LaApiMCLAGMasterInit (VOID);
UINT1
LaGetMCLAGSystemStatus (VOID);
VOID LaHbResumeProtocolOperation(VOID);
INT4 LaIcchApiTestICCLParams (INT4 i4Status);
INT4 LaApiIsMemberofICCL (UINT4 u4IfIndex);
INT1 LaAstDisableStpOnPort (UINT4 u4IfIndex);
INT4
LaUpdateActorPortChannelAdminKey (UINT4 u4IfIndex,
                             UINT2 *pu2AdminKey);
INT4
LaGetAggIndexFromAdminKey (UINT2 u2AdminKey, UINT4 *pu4IfIndex);

UINT4
LaApiDisableLldpOnIcclPorts (UINT4 u4AggIndex);

UINT4
LaApiGetOperStatusonAggIndex (UINT4 u4AggIndex, UINT1 *pu1Status);

INT4
LaApiIsMemberofMcLag (UINT4 u4IfIndex);
VOID
LaIcchInitProperties (UINT4 u4IcclIfIndex);
/* end of pseudowire redundancy */
VOID
LaApiIsRemoteMclagUp (UINT4 u4IfIndex, UINT1 *pu1IsRemoteMclagUp);

tLaBoolean LaIsMclagStarted (VOID);
VOID LaNotifyMclagShutdownComplete (VOID);
#endif /* _LA_H */
