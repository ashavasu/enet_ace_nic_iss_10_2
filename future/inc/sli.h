/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: sli.h,v 1.53 2016/07/05 08:22:22 siva Exp $
 *
 * Description: This file contains the exported definitions, macros
 *              and data structures of SLI used by other modules.
 *
 *******************************************************************/
#ifndef __SLI_H__
#define __SLI_H__
/* Definitions to specify the Type of the socket */
#define  SOCK_STREAM   1            
#define  SOCK_DGRAM    2            
#define  SOCK_RAW      3            
#define ETH_P_ALL       0x0003
#define SO_BINDTODEVICE 25
/* Definitions to specify the address Family of the Socket */ 
#define  AF_UNSPEC     0            
#define  AF_INET       2            
#define  AF_INET6      10           
#define  AF_MAX        32           
#define AF_PACKET       17 

#define  PF_UNSPEC     AF_UNSPEC    
#define  PF_INET       AF_INET      
#define  PF_INET6      AF_INET6     
#define  PF_MAX        AF_MAX       
#define PF_PACKET       AF_PACKET
/* socket option level for setsockopt and getsockopt , level should
 * match the protocol number(IPPROTO_XXX) */
#define  SOL_SOCKET            1
#define  SOL_IP                0
#define  SOL_TCP               6
#define  SOL_UDP               17
#define  SOL_IPV6              41
#define  SOL_ICMPV6            58
#define  SOL_RAW               255

/* Socket Level Options for SliGetsockopt & SliSetsockopt calls */
/* These 3 options are flags (set or reset ) */
#define  SO_REUSEADDR      1
#define  SO_DONTROUTE      2
#define  SO_BROADCAST      4

#define  SO_TYPE           3
#define  SO_DEBUG          5
#define  SO_ERROR          6
#define  SO_SNDBUF         7
#define  SO_RCVBUF         8
#define  SO_KEEPALIVE      9
#define  SO_OOBINLINE      10
#define  SO_LINGER         13
#define  SO_TIMEOUT        14
#define  SO_SECURITY       15
#define  MAX_MESG_CNT      100 

/* Tcp Level Options for SliGetsockopt & SliSetsockopt calls, not std */
#define  TCP_MAXSEG             12
#define  TCP_MAX_MESG_CNT       13
#define  TCP_NODELAY            14
#define  TCP_TOS                15
#define  TCP_SECURITY           40 
#define  TCP_ASY_REPORT         17
#define  TCP_TIMEOUT            18
#define  TCP_ENBL_ICMP_MSGS     19
#define  TCP_REUSEADDR          20
#define  TCP_MD5SIG             21
#define  TCP_AO_SIG             22
#define  TCP_AO_NOMKT_MCH       23
#define  TCP_AO_ICMP_ACC        24

/* IP level socket options for getsockopt & setsockopt */
#define  IP_TOS                 1
#define  IP_TTL                 2
#define  IP_HDRINCL             3
#define  IP_OPTIONS             4
#define  IP_ROUTER_ALERT        5
#define  IP_PKTINFO             8
#define  IP_MTU_DISCOVER        10
#define  IP_RECVERR             11

#define  IP_MULTICAST_IF        32
#define  IP_MULTICAST_TTL           33
#define  IP_MULTICAST_LOOP          34
#define  IP_ADD_MEMBERSHIP          35
#define  IP_DROP_MEMBERSHIP         36
#define  IP_UNBLOCK_SOURCE          37
#define  IP_BLOCK_SOURCE            38
#define  IP_ADD_SOURCE_MEMBERSHIP   39
#define  IP_DROP_SOURCE_MEMBERSHIP  40
#define  IP_MSFILTER                41
#define  MCAST_JOIN_GROUP           42
#define  MCAST_BLOCK_SOURCE         43
#define  MCAST_UNBLOCK_SOURCE       44
#define  MCAST_LEAVE_GROUP          45
#define  MCAST_JOIN_SOURCE_GROUP    46
#define  MCAST_LEAVE_SOURCE_GROUP   47
#define  MCAST_MSFILTER             48

#define  IP_RECVIF                  49
#define  IP_PKT_TX_CXTID            50
#define  IP_PKT_RX_CXTID            51
#define  IP_SOCK_MODE               52


/* argument values for IP_MTU_DISCOVER */
#define  IP_PMTUDISC_DONT       0    /* Never send DF frames.  */
#define  IP_PMTUDISC_WANT       1    /* Use per route hints.  */
#define  IP_PMTUDISC_DO         2    /* Always DF.  */

/* IPv6 level socket options for getsockopt & setsockopt */
#define  IPV6_PKTINFO           2
#define  IPV6_UNICAST_HOPS      16
#define  IPV6_MULTICAST_IF      17
#define  IPV6_MULTICAST_HOPS    18
#define  IPV6_MULTICAST_LOOP    19
#define  IPV6_MTU_DISCOVER      23
#define  IPV6_RECVERR           25
#define  IPV6_ADD_MEMBERSHIP    20
#define  IPV6_DROP_MEMBERSHIP   21
#define  IPV6_JOIN_GROUP        22
#define  IPV6_LEAVE_GROUP       23
#define  IPV6_V6ONLY            26
#define  IPV6_RECVHOPLIMIT      27

/* values for IPV6_MTU_DISCOVER */
#define  IPV6_PMTUDISC_DONT     0       /* Never send DF frames.  */
#define  IPV6_PMTUDISC_WANT     1       /* Use per route hints.  */
#define  IPV6_PMTUDISC_DO       2       /* Always DF.  */


/* minimum buffer size required to hold the addresses */
#define INET_ADDRSTRLEN     16
#define INET6_ADDRSTRLEN    46

/* Definition related to IP level Option & Raw Packet Parameter */
#define  IP_PKTINFO_ENA            0
#define  IP_PKTINFO_DIS            1
#define  SLI_PMTU_FLAG_MASK        0xfff0

/* Flags Parameters Used with SliSend, SliRecv & SliFcntl calls */
#define  MSG_ZERO        0
#define  MSG_OOB         1
#define  MSG_PEEK        2
#define  MSG_PUSH        4
#define  MSG_DONTROUTE   8
#define  MSG_DONTWAIT    16
#define  MSG_NOSIGNAL    32

/* defintions required for Non-blocking mode operation */
#define  SLI_F_GETFL     3
#define  SLI_F_SETFL     4
#define  SLI_O_NONBLOCK  0x4000

/* General Definitions that are  required for the FutureSocket */
#define  RESET                 0
#define  SET                   1
#define  INIT_VALUE            -1
#define  NO_FREE_SOCKET        -1
#define  IGNORE_OPTLEN         0
#define  IGNORE_OPTVAL         0
#define  NULL_FLAG             0
#define  NULL_TIMEOUT          0
#define  ASCII_OFFSET          48
#define  UNALLOTTED_PORT       0

/* INADDR_XXX values */
#define  INADDR_ANY            0x00000000
#define  INADDR_LOOPBACK       0x7f000001
#define  INADDR_BROADCAST      0xffffffff
/* MPLS_IPv6 add start*/
#ifdef MPLS_IPV6_WANTED
#define IN6ADDR_ANY         {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}}
#endif
/* MPLS_IPv6 add end*/
/* values for tcp connection shutdown */
#define  SHUTDOWN_RX           0
#define  SHUTDOWN_TX           1
#define  SHUTDOWN_ALL          2

/* fcntl options */
#define  FCNTL_BLK                      0x0                
#define  FCNTL_NOBLK                    0x1                

/* Definiton required for pktinfo option */
#define  MAX_ANCILLARY_DATA_SIZE   20
#define  CMSG_FIRSTHDR(mhdr)       ((struct cmsghdr *)(mhdr)->msg_control)
#define  CMSG_DATA(cmsg)           ((cmsg)->cmsg_data)

/* Definition required for TCP MD5 option */
#define  SS_MAXSIZE            30   /* current support for af_inet/af_inet6 */
                                    /* update for newer address families */

#ifndef  TCP_MD5SIG_MAXKEYLEN
#define  TCP_MD5SIG_MAXKEYLEN  80
#endif


/* Definitions required  for supporting FutureUDP protocol */
#define  IPPROTO_UNSPEC       0
#define  IP_HEADER_LEN        20
#define  MAX_UDP_CONNECTIONS  25
#define  UDP_HEADER_LEN       8
#define  SLI_UDP_TOS          0              /* Type of Service  */
#define  SLI_UDP_TTL          0              /* Time To Live     */
#define  SLI_UDP_ID           0              /* IP ID field      */
#define  SLI_UDP_DF           0              /* Dont Frament     */
#define  SLI_UDP_OLEN         0              /* IP Option length */
#define  SLI_TCP_PORT         0xffff
#define  SLI_UDP_PORT         0xffff
#define  SLI_UDP_DATA_OFFSET  UDP_HEADER_LEN + IP_HEADER_LEN
#define  SLI_UDP_WAIT_EVENT   (0x00000080)
/* Error Code Definition returned by the FutureSLI routine */

#define  SLI_ENOTSOCK                          -11
#define  SLI_ENOTCONN                          -12
#define  SLI_ENOPROTOOPT                       -13
#define  SLI_EMFILE                            -14
#define  SLI_ENOMEM                            -15
#define  SLI_EMEMFAIL                          SLI_ENOMEM
#define  SLI_ENOBUFS                           SLI_ENOMEM
#define  SLI_ECONNREFUSED                      -16
#define  SLI_EOPNOTSUPP                        -17
#define  SLI_EINVAL                            -18
#define  SLI_EMSGSIZE                          -19
#define  SLI_EPROTNOSUPP                       -20
#define  SLI_ESOCKNOSUPP                       -21
#define  SLI_EFAMNOSUPP                        -22
#define  SLI_EFAULT                            -32
#define  SLI_ENFILE                            -33
#define  SLI_ENOPKT                            -34
#define  SLI_EADDRNOTAVAIL                     -35
#define  SLI_EBADADDR                          -36
#define  SLI_EDESTADDRREQ                      -37
#define  SLI_EINTR                             -38
#define  SLI_EWOULDBLOCK                       -39
#define  SLI_EACCES                            -40
#define  SLI_ENETUNREACH                       -41
#define  SLI_ENXIO                             -43
#define  SLI_EAGAIN                            -44
#define  SLI_EALREADY                          -45
#define  SLI_EINPROGRESS                       -46
#define  SLI_EADDRINUSE                        -47
#define  SLI_EISCONN                           -48
#define  SLI_ETIMEDOUT                         -49
#define  SLI_EKATIMEDOUT                       -50
#define  SLI_EPERM                             -51
#define  SLI_ECLOSINGCONN                      -52
#define  SLI_ENOENT                            -53

#define  SLI_SUCCESS             OSIX_SUCCESS 
#define  SLI_FAILURE             -1

/* Definition required  for supporting the select function */
#define  SELECT_READ        0x00000001
#define  SELECT_WRITE       0x00000002
#define  SELECT_EXCEPT      0x00000004
#define  SLI_FD_SET(x,y)    ((*y)[(x)/32])|=(0x80000000 >>(x-(((x)/32)*32)))
#define  SLI_FD_CLR(x,y)    ((*y)[(x)/32])&=~(0x80000000 >>(x-(((x)/32)*32)))
#define  SLI_FD_ISSET(x,y)  ((*y)[(x)/32] &(0x80000000 >>(x-(((x)/32)*32))))
#define  SLI_FD_ZERO(x)     SliSetFdZero(x)
#define  SLI_MAX_FD         1024

#define   V4_FROM_V6_ADDR(a)         (a).u4_addr[3]
#define   V6_ADDR_COPY(a,b)          MEMCPY(a,b,sizeof(tIpAddr))
#define   V6_ADDR_CMP(a,b)           MEMCMP(a,b,sizeof(tIpAddr))
#define   V4_MAPPED_ADDR_COPY(a,b) { (*a).u4_addr[3] = b; \
                                     (*a).u4_addr[2] = 0xffff; \
                                     (*a).u4_addr[1] = 0; \
                                     (*a).u4_addr[0] = 0; }
#define   V6_ADDR_INIT(a)          { (*a).u4_addr[3] = 0; \
                                     (*a).u4_addr[2] = 0; \
                                     (*a).u4_addr[1] = 0; \
                                     (*a).u4_addr[0] = 0; }
#define   IS_ADDR_IN6ADDRANYINIT(a)( (a).u4_addr[3] == 0 && \
                                     (a).u4_addr[2] == 0 && \
                                     (a).u4_addr[1] == 0 && \
                                     (a).u4_addr[0] == 0 )
#define   IN6_IS_ADDR_V4MAPPED(a)  ( (a).u4_addr[2] == 0xffff && \
                                     (a).u4_addr[1] == 0 && \
                                     (a).u4_addr[0] == 0 )

/* Start of DATA STRUCTURES */
enum
{
  IPPROTO_IP   =   0,   /* Dummy protocol for TCP            */
  IPPROTO_ICMP =   1,   /* Internet Control Message Protocol */
  IPPROTO_TCP  =   6,   /* Transmission Control Protocol     */
  IPPROTO_UDP  =  17,   /* User Datagram Protocol            */
  IPPROTO_IPV6 =  41,   /* Ipv6-Over-Ipv6 Tunnel             */
  IPPROTO_RSVP = 46,    /* ReSource reseVation Protocol */
  IPPROTO_ICMPV6 = 58,  /* Internet Control Message Protocol ver 6 */
  IPPROTO_OSPF =  89,   /* open shortest path first */
  IPPROTO_RAW  = 255,   /* Raw IP packets                    */
  IPPROTO_MAX
};

/* Standard well known ports */
enum
{
  IPPORT_ECHO   = 7,        /* Echo service.                   */
  IPPORT_FTP    = 21,       /* File Transfer Protocol.         */
  IPPORT_TELNET = 23,       /* Telnet protocol.                */
  IPPORT_SMTP   = 25,       /* Simple Mail Transfer Protocol.  */
  IPPORT_TFTP   = 69        /* Trivial File Transfer Protocol. */
};

typedef enum
{
  SS_FREE = 0,                  /* Not Allocated                  */
  SS_UNCONNECTED,               /* Unconnected to any Socket      */
  SS_CONNECTING,                /* In process of Connecting       */
  SS_CONNECTED,                 /* Connected to Socket            */
  SS_TX_SHUTDOWN,               /* Transmission has been Shutdown */
  SS_RX_SHUTDOWN,               /* Reception has been Shutdown    */
  SS_DISCONNECTING              /* In process of Disconnecting    */
} socketState;

struct in_addr
{
    UINT4  s_addr;
};

/* The message header structure used by sendmsg and recvmsg */
struct msghdr
{
    void   *msg_name;      /* Ptr to Socket Address Structure */
                           /* Not used in our implementation   */
    UINT4  msg_namelen;    /* Size of Socket Address Structure */
                           /* Not used in our implementation   */
    void   *msg_iov;       /* Scatter/Gather array. This is 
                              caadr_t in  old  BSD  versions
                              and  iovec in RFC 2292.
                              Not used in our implementation   */
    UINT4  msg_iovlen;     /* Elements in msg_iov              */
                           /* Not used in our implementation   */
    void   *msg_control;   /* Ancillary Data                   */
    UINT4  msg_controllen; /* Ancillary Data buffer length     */
    int    msg_flags;      /* Flags on received message        */
                           /* Not used in our implementation   */

};

/* The cmsg header that holds the ancillary data */
struct cmsghdr
{
    UINT4  cmsg_len;               /* #Bytes, including this header */
    int    cmsg_level;            /* Originating protocol */
    int    cmsg_type;             /* Protocol-specific type */
    UINT1  cmsg_data[MAX_ANCILLARY_DATA_SIZE]; /* The ancillary data */
};

struct in6_addr
{
    UINT1  s6_addr[16];
};

#ifndef IP6ADDRESS
#define IP6ADDRESS
typedef struct Ip6Address
{
  union
  {
    UINT1  u1ByteAddr[16];
    UINT2  u2ShortAddr[8];
    UINT4  u4WordAddr[4];
  } ip6_addr_u;

#define  u4_addr  ip6_addr_u.u4WordAddr  
#define  u2_addr  ip6_addr_u.u2ShortAddr 
#define  u1_addr  ip6_addr_u.u1ByteAddr  

} tIpAddr;
#endif

typedef struct
{
    struct in6_addr ipv6mr_multiaddr; /* Multi cast Group Address */
    int             ipv6mr_ifindex;  /* Interface index Mcast group joined on */
}tSliIpv6Mreq;

typedef struct
{
    UINT1  *pu1Option;
    UINT2  u2ReservedWord;
    UINT1  u1OptLen;
    UINT1  u1Padding;
} tSliIpOptions;

typedef tTMO_SLL tSliRawQueue;

typedef UINT4 SliFdSet[SLI_MAX_FD/ (sizeof(UINT4)*BITS_PER_BYTE)];

typedef struct sockaddr_in
{
  INT2             sin_family;  /* Address family   */
  UINT2            sin_port;    /* Port number      */
  struct  in_addr  sin_addr;    /* Internet address */
  UINT1            sin_zero[8]; /* NULL Padding     */
} tSin;

typedef struct in_pktinfo
{
  int    ipi_ifindex;           /* Interface index             */
  struct in_addr ipi_spec_dst;  /* Routing Destination address */
  struct in_addr ipi_addr;      /* Header Destination address  */
} tInPktinfo;

typedef struct in6_pktinfo
{
  struct in6_addr ipi6_addr;    /* Header Destination address  */
  int    ipi6_ifindex;          /* Interface index             */
  int    in6_hoplimit;          /* Hop Limit                   */
} tIn6Pktinfo;

#ifndef __socklen_t_defined
#define __socklen_t_defined
#ifndef FS_SSH_CUST
typedef unsigned int socklen_t;
#else
typedef INT4 socklen_t;
#endif
#endif

typedef struct SLILINGERPARMS
{
    UINT2  u2LingerOnOff;
    UINT2  u2LingerTimer;
} tSliLINGERPARMS;

# ifndef _STRUCT_TIMEVAL
#define _STRUCT_TIMEVAL
typedef struct timeval
{
    UINT4  tv_sec;   /* Time in Seconds */
    UINT4  tv_usec;  /* Time in Microseconds */
} timeval; 
#endif

typedef struct sockaddr
{
    UINT2  sa_family;    /* Address family, AF_xxx */
    INT1   sa_data[14];  /* 14 bytes of Protocol Address */
} tSaddr;

typedef struct sockaddr_in6
{
  INT2            sin6_family;    /* Address family                  */
  UINT2           sin6_port;      /* Port number                     */
  UINT4           sin6_flowinfo;  /* Ipv6 Traffic class & flow label */
  struct in6_addr sin6_addr;      /* Internet address                */
  UINT4           sin6_scope_id;  /* set of interfaces for a scope   */
} tSin6;

typedef struct ifnameindex
{
    INT4    i4IfIndex;
    UINT1*  u1IfName;
} tSliIfnameindex;

typedef struct ip_mreq
{
    struct in_addr imr_multiaddr;   /* IP multicast address of group */
    struct in_addr imr_interface;   /* local IP address of interface */
}tIpMreq;

typedef struct ip_mreqn
{
    struct in_addr  imr_multiaddr;      /* IP multicast address of group */
    struct in_addr  imr_address;        /* local IP address of interface */
    INT4            imr_ifindex;        /* Interface index */
}tIpMreqn;

/* Data Structures used by application to set TCP MD5 Option */ 
struct sli_sockaddr_storage
{
    UINT2   ss_family;                  /* Address Family of Peer IP */
    UINT1   __data[SS_MAXSIZE];         /* Buffer to hold IPV4/IPV6 address */
};

struct tcp_md5sig
{
    struct sli_sockaddr_storage   tcpm_addr;        /* Peer Address */
    UINT2           tcpm_keylen;                    /* MD5 Key Length */
    UINT2           u2Padding;                      /* For padding */
    UINT1           tcpm_key[TCP_MD5SIG_MAXKEYLEN]; /* MD5 Key */
};

/* Internal Data Structures for TCP MD5 Option */
typedef struct
{
    UINT1           au1Key[TCP_MD5SIG_MAXKEYLEN];    /* MD5 Key */
    UINT1           u1Keylen;                           /* MD5 Key Length */
    UINT1           au1Padding[3];                      /* For padding */
}tSliMD5Key;

typedef struct
{
    tTMO_SLL_NODE   TSNext;     /* Next node in list */
    tSliMD5Key      MD5Key;     /* MD5 Key Details */
    tIpAddr         PeerIp;     /* Peer IP address */
}tSliMd5SA;

typedef struct
{
    struct sli_sockaddr_storage   tcpm_addr;      /* Peer Address */
    UINT1 u1SndKeyId;                             /* Send key id in TCP-AO option */
    UINT1 u1RcvKeyId;                             /* Receive key id in TCP-AO option */
    UINT1 u1Algo;                                 /* Digest calculation algorithm HMAC-SHA-1-96(1) / AES 128(2) */
    UINT1 u1TcpOptIgn;
    UINT1 u1KeyLen;                               /* Master key length in bytes */
    UINT1 au1Padding[3];
    UINT1 au1Key[TCP_MD5SIG_MAXKEYLEN];           /* Master authentication key */
}tTcpAoMktAddr;

#ifndef SLI_TCPAO_MKTLIST
#define SLI_TCPAO_MKTLIST
typedef struct
{
    UINT1 u1SendKeyId;
    UINT1 u1RcvKeyId;
    UINT1 u1ShaAlgo;
    UINT1 u1TcpOptIgnore;
    UINT1 au1Key[TCP_MD5SIG_MAXKEYLEN];
}tSliTcpAoMkt;
#endif
typedef struct
{
    tTMO_SLL_NODE   TSNext;
    tSliTcpAoMkt    TcpAoKey;
    tIpAddr         PeerIp;
    UINT1           u1IsMktCfg;
    UINT1           u1NoMktMchPckDsc;
    UINT1           u1IcmpAccpt;
    UINT1           u1Padding;
}tsliTcpAoMktListNode;

typedef struct
{
     struct sli_sockaddr_storage   tcpm_addr;
     UINT1 u1IcmpAccpt;
     UINT1 u1NoMktMchPckDsc;
     UINT2 u2Padding;
}tTcpAoNeighCfg;
/* Definition of socket descriptor entry */
typedef struct
{
    tIpAddr                 LocalIpAddr;    /* local connection addr  */
    tIpAddr                 RemoteIpAddr;   /* remote address of connection */
    tIpAddr                 RecvAddr;       /* Address in which Socket Listen */
    tIn6Pktinfo             in6Pktinfo;     /* used in sendmsg  and  recvmsg
          * for IPV6_PKTINFO option */
    tInPktinfo              inPktinfo;      /* used in sendmsg  and  recvmsg 
          * for IP_PKTINFO option */
    tSliIpOptions           IpOption;       /* IP options */
    tSliRawQueue            RecvQueue;      /* Queue on which RAW socket wait */
    tTMO_SLL                Md5SAList;      /* List to store TCP MD5 SA's */    /* RFC 2385 */
    tTMO_SLL                TcpAoList;      /* List to store TCP-AO MKTs */ /*RFC 5925 */
    socketState             state;          /* CONNECTED, .... */
    UINT4                   u4ValidOptions;    /* Used by get/setsockopt() */
    UINT4                   u4ConnId;          /* Identifies the TCB entry */
    tOsixSemId              ConnSemId;       /* Connection sema4 Id      */
    tOsixSemId              ProtectSemId;    /* Protection sema4 Id      */
    UINT4                   u4MCastAddr;       /* multicast address        */
    UINT4                   u4IfIndex;         /* interface used        */ 
    UINT4                   u4MCastIf;         /* multicast interface      */ 
    UINT4                   u4RxContextId;     /* Context Identifier in 
                                                * reception thread */
    UINT4                   u4TxContextId;     /* context Identifier in 
                                                *  transmission thread */
    INT4                    i4Protocol;        /* TCP Socket ?? UDP Socket ?? */
    INT4                    i4MaxPendingOpens; /* accept Q length       */
    tTMO_SLL                *pAccept;
    VOID                    (*fpSliHandlePmtuChange)(UINT4,INT4,UINT2);
#ifdef TCP_WANTED 
    tTcpToApplicationParms  *pConnect;         /* Connect Response Pkt Holder */
    tTcpToAppAsyncParms     *pAsyncParams;
    tTcpToApplicationParms  *pListen;
    VOID                    (*fpSliTcpHandleAsyncMesg)(INT4, 
                                                tTcpToApplicationParms*);
#endif
    tOsixQId                UdpQId;            /* used for UDP          */

    UINT2                   u2LocalPort;       /* Local port of conn    */
    UINT2                   u2RemotePort;      /* remote port of conn   */
    INT2                    i2SdtFamily;       /* Address family           */
    INT2                    i2SdtSockType;     /* type of the socket       */
    INT4                    i4unicasthlmt;     /* hop limit for ucast pkts */
    INT4                    i4multicasthlmt;   /* hop limit for mcast pkts */
    INT4                    i4recvhoplimit;    /* flag for sending hoplimit to upper layer */
    INT4                    i4multicastloop;   /* should loop back mcast pkt? */
    INT4                    i4IpTtl;           /* TTL filled in the IP header */
    INT4                    i4IpTos;           /* TOS filled in the IP header */
    INT4                    i4PmtuDfFlag;      /* PMTU discovery flag.
                                                  When the IP DF  option  is
                                                  supported by sli, this 
                                                  flag  can be used to store
                                                  the DF bit as well. */
    UINT4                   u4IpPktinfoFlag;   /* IP_PKTINFO option flag */
    INT4                    i4IpHdrIncl;        /* Shud RAW SOCK include 
                                                  IP header */
    UINT1                   u1SockOpns;        /* Enabled socket options */
    UINT1                   u1FcntlOpns;       /* fcntl options          */
    UINT1                   u1FdMode;          /* Usage type of SockFd
                                                * It can be Single Instance or 
                                                *  Multiple instance */
    INT1                    i1ErrorCode;       /* recent error code      */
    UINT1                   u1Padding;
    UINT1                   au1Pad[3];
}tSdtEntry;
/* End  of  DATA STRUCTURES */


/* Start of PROTOTYPES */
VOID SliInit PROTO ((INT1 *pi1Dummy)); 

VOID SliShutDown PROTO ((VOID)); 
 
INT4 SliSocket PROTO ((INT4, INT4, INT4));
 
INT4 SliFcntl PROTO ((INT4, INT4, INT4));

INT4 SliBind PROTO ((INT4, struct sockaddr *, INT4));

INT4 SliRecv PROTO ((INT4, VOID *, INT4, UINT4));

INT4 SliRecvfrom PROTO ((INT4, VOID *, INT4, UINT4, struct sockaddr *, INT4 *));

INT4 SliSetsockopt PROTO ((INT4, INT4, INT4, VOID *, INT4));

INT4 SliSendmsg PROTO ((INT4, const struct msghdr *, UINT4));

INT4 SliRecvmsg PROTO ((INT4, struct msghdr *, UINT4));

INT4 SliSendto PROTO ((INT4, CONST VOID *, INT4, UINT4, struct sockaddr *, INT4));

INT4 SliConnect PROTO ((INT4, struct sockaddr *, INT4));

INT4 SliSelect PROTO ((INT4, SliFdSet*, SliFdSet*, SliFdSet*, struct timeval*));

void SliSetFdZero PROTO ((SliFdSet*));

INT4 SliClose PROTO ((INT4));

INT4 SliShutdown PROTO ((INT4, INT4));

INT4 SliGetFtSrvSockFdTCBTableIndex PROTO ((VOID));

INT4 SliGetTcpSrvSockFdTCBTableIndex PROTO ((VOID));

INT4 SliWrite PROTO  ((INT4, CONST VOID *, INT4));

INT4 SliRead PROTO   ((INT4, VOID *, INT4));

INT4 SliSend PROTO ((INT4, CONST VOID *, INT4, UINT4));

INT4 SliGetpeername PROTO ((INT4, struct sockaddr *, INT4 *));

INT4 SliGetsockopt PROTO ((INT4, INT4, INT4, VOID *, INT4 *));

INT4 SliGetsockname PROTO ((INT4, struct sockaddr *, INT4 *));

#ifdef TCP_WANTED
        
INT4 SliListen PROTO ((INT4, INT4));

INT4 SliAccept PROTO ((INT4, struct sockaddr *, INT4 *));
#endif


#ifdef IP6_WANTED        

INT4 SliIfNametoIndex PROTO ((UINT1 *));
        
UINT1 *SliIfIndextoName PROTO ((UINT2, UINT1*));
        
tSliIfnameindex *SliIfNameIndex PROTO ((VOID));
        
VOID SliIfFreenameindex PROTO ((tSliIfnameindex *));
#endif
        
INT4 SliInetPton PROTO ((INT4, const UINT1 *, VOID *));

CONST UINT1 * SliInetNtop PROTO ((INT4 , CONST VOID *, UINT1 *, INT4));

/* Future SLI implementation specific calls */

INT4 SliHandleChangeInPmtuInCxt PROTO ((UINT4 , UINT4, UINT1, UINT2)); 

INT4 SliRegisterPmtuHandler PROTO ((INT4, VOID (*fpPmtuHandler) (UINT4, INT4,
                                                                 UINT2)));

INT4 SliEnqueueIpv4RawPacketInCxt PROTO ((UINT4 , UINT1 , UINT4 , UINT4 , UINT4 , tCRU_BUF_CHAIN_HEADER *));

INT4 SliEnqueueIpv6RawPacketInCxt PROTO ((UINT4 , UINT1 , UINT4 , UINT4 , UINT1 *, tCRU_BUF_CHAIN_HEADER *));

INT4 SliSelectScanList PROTO ((INT4 , UINT4 ));
INT4 SliCheckSelectScanList PROTO ((INT4 i4SockId, UINT4 u4BitMap));

tIpAddr * V6_HTONL PROTO ((tIpAddr *,const tIpAddr *));
tIpAddr * V6_NTOHL PROTO ((tIpAddr *,const tIpAddr *));

/* End  of  PROTOTYPES */

/* ERRORS */
#define  SLI_ERR_NO_SOCKET_MATCH               -1
#define  SLI_ERR_TASK_SPAWN                    -2
#define  SLI_ERR_COULDNT_OPEN_CONNECTION       -3
#define  SLI_ERR_UNCONNECTED_SOCKET            -4
#define  SLI_ERR_COULDNT_GET_OPTION            -5
#define  SLI_ERR_COULDNT_SET_OPTION            -6
#define  SLI_ERR_COULDNT_SET_MESG_COUNT        -7
#define  SLI_ERR_INVALID_FLAG                  -8
#define  SLI_ERR_COULDNT_WRITE_DATA            -9
#define  SLI_ERR_COULDNT_READ_DATA             -10
#define  SLI_ERR_COULDNT_CLOSE_SOCKET          -11
#define  SLI_ERR_INVALID_OPTION                -12
#define  SLI_ERR_NO_FREE_SOCKET                -13
#define  SLI_ERR_COULDNT_READ_URG_DATA         -14
#define  SLI_ERR_NO_MEMORY                     -15
#define  SLI_ERR_THIS_CONN_ABRTD               -16
#define  SLI_ERR_SOME_CONN_ABRTD               -17
#define  SLI_ERR_INVALID_COMMAND               -18
#define  SLI_ERR_CONN_REFUSED                  -19
#define  SLI_ERR_REG_OPERATION_FAILED          -20
#define  SLI_ERR_STATUS_OPER_FAILED            -21
#define  SLI_ERR_COULDNT_ABORT_CONN            -22
#define  SLI_ERR_OPER_NOT_PERMITTED            -23
#define  SLI_ERR_INVALID_PARAM                 -24
#define  SLI_ERR_BIND_FAILED                   -25
#define  SLI_ERR_INSUFF_MEM                    -26
#define  SLI_ERR_NEGATIVE_BACKLOG_NOT_ALLOWED  -27

#define  t_SIN           tSin
#define  t_SADDR         tSaddr
#define  sli_socket      SliSocket
#define  sli_bind        SliBind
#define  sli_accept      SliAccept
#define  sli_fcntl       SliFcntl
#define  sli_connect     SliConnect
#define  sli_recvfrom    SliRecvfrom
#define  sli_sendto      SliSendto
#define  sli_close       SliClose
#define  sli_select      SliSelect
#define  sli_setsockopt  SliSetsockopt
#define  sli_getsockopt  SliGetsockopt
#define  sli_sendmsg     SliSendmsg
#define  sli_recvmsg     SliRecvmsg
#define  sli_fd_set      SliFdSet

/* Event used by the select event */
#define SELECT_TIMER_EVENT      0x70000000

#endif /* _SLI_H */

