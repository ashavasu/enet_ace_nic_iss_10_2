/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ids.h,v 1.13 2014/01/29 13:19:47 siva Exp $                       
 *
 * Description: Contains definitions and macros to be used
 *              by external modules.
 *
 *******************************************************************/
#ifndef  _IDS_H
#define  _IDS_H

typedef struct {
    INT4   i4CtrlSockFd;       /* socket FD for sending DOS attack packets to
                                * Ids by Security module and send BLACKLIST
                                * verdict or TRAP to Security module by Ids
                                */
    INT4   i4DataSockFd;       /* Socket FD used for sending non DOS attack
                                * packets to Ids by Security module and send 
                                * ALLOW verdict to Security module by Ids 
                                */
    INT4   i4MgmtSockFd;       /* socket FD for sending management information
                                * to Ids by Security module and send response
                                * messages to Security module by Ids
                                */
    UINT1  u1IsSockConnected;  /* Bit for socket connection
                                * BIT 0 -> For control socket FD
                                * BIT 1 -> For Data socket FD 
                                * BIT 2 -> For Mgmt socket FD 
                                */
    UINT1   u1Pad[3];          /* Padding */
} tIdsSockInfo;

typedef struct {
    UINT4  u4OpCode;  /* Indication from Ids either Allow/BlackList/Trap */
    UINT4  u4PktLen;  /* Valid length of this packet */
    UINT4  u4IfIndex; /* Valid Interface Index */
    UINT1  *pu1Buf;   /* Data packet Buffer */
} tIdsMsg;

typedef enum
{
    /* Following enums are applicable for Ids & ISS */
    ISS_IDS_PKT_NONE  = 0x00000000,        
    ISS_IDS_PKT_ALLOW = 0x00000001,             /* Allowed packets by Ids */
    ISS_IDS_PKT_DROP  = 0x00000002,             /* Dropped packets */
    ISS_IDS_PKT_BLACKLIST  = 0x00000004,        /* Blacklisted Packet by Ids */
    ISS_IDS_DATA_PKT = 0x00000008,              /* To identify Data/Attack Packets */
    /* Following enums are applicable for sending mgmt request between ISS to Ids*/
    ISS_IDS_VERSION_REQ     = 0x00000010,       /* To get Ids version info */
    ISS_IDS_GLB_STATS_REQ   = 0x00000020,       /* To get global statistics from Ids */
    ISS_IDS_SET_PKT_DROP_THRESH  = 0x00000040,  /* To set packet dropped threshold limit */
    ISS_IDS_TRAP_PKT_DROP_THRESH = 0x00000080,  /* Indicate Trap msg for packet dropped threshold */
    ISS_IDS_MGMT_RESPONSE    = 0x00000100,      /* To get response message from Ids for config request */
    ISS_IDS_INTF_STATS_REQ   = 0x00000200,      /* To get interface specific statistics from Ids */
    ISS_IDS_SET_LOG_STATUS   = 0x00000400,      /* To set the log status as Enable/Disable */
    ISS_IDS_SET_DEL_INTF     = 0x00000800,      /* To inform about interface delete indication to Ids */
    ISS_IDS_CLEAR_GLB_STATS  = 0x00001000,      /* To clear the global statistics in Ids */
    ISS_IDS_CLEAR_INTF_STATS = 0x00002000,      /* To clear the interface specific stats in Ids */
    ISS_IDS_RESET_IDS        = 0x00004000,      /* To reload ids with new set of rules/configurations */
    ISS_IDS_LOG_DATA         = 0x00008000,      /* To send the log from Ids to ISS */
    ISS_IDS_LOAD_RULES       = 0x00010000,      /* To add new set of rules/configurations to existing set */
    ISS_IDS_UNLOAD_RULES     = 0x00020000,
    ISS_IDS_INIT_COMP        = 0x00040000,      /* To send the snort initialization complete 
                                                   from Ids to ISS */
    ISS_IDS_PKT_MAX     = 0xFFFFFFFF
} tIdsOpcode;

typedef enum
{
    ISS_IDS_SOCK_CONN_NONE = 0x00000000,
    ISS_IDS_SOCK_CONN_CTRLFD = 0x00000001,
    ISS_IDS_SOCK_CONN_DATAFD = 0x00000002,
    ISS_IDS_SOCK_CONN_MGMTFD = 0x00000004,
    ISS_IDS_SOCK_CONN_ALL = (ISS_IDS_SOCK_CONN_CTRLFD | ISS_IDS_SOCK_CONN_DATAFD | ISS_IDS_SOCK_CONN_MGMTFD),
} tSockConnStatus;

/* Gloabal variable used in IDS-IPC */
#define ISS_IDS_SECMOD_USER gi4SecMod /* Global variable to identify place of security module */

/* Macros */
#define IDS_SUCCESS  OSIX_SUCCESS
#define IDS_FAILURE  OSIX_FAILURE

#define ISS_IDS_IPC_ERROR (-1)
#define ISS_IDS_SOCK_INIT (-1)

#define ISS_IDS_SOCK_TIMEOUT 5
#define ISS_IDS_MAX_LISTEN   1
#define IDS_DATA_START_OFFSET 12   /*OpCodeLength(4) + PktLen(4) + Length of IfIndex(4) */

#define ISS_IDS_BASE_IP      0x7f000001 /* 127.0.0.1 */
#define ISS_IDS_SERVER_IP    0x00000000 /* INADDR_ANY */
#define ISS_IDS_DATAFD_PORT  26610
#define ISS_IDS_CTRLFD_PORT  26616
#define ISS_IDS_MGMTFD_PORT  26618

#define IDS_INVALID_VERDICT  2
#define IDS_NO_DATA_READ     10

#define IDS_VERSION_LEN        64
#define IDS_MGMT_MSG_LEN     sizeof(UINT4) 

#define ISS_IDS_MAX_BUF_SIZE (SEC_MAX_PKT_SIZE + sizeof (tIdsMsg) + IDS_MGMT_MSG_LEN)
#define IDS_MSG_DATALEN(MsgPtr) (MsgPtr)->u4PktLen
#define IDS_MSG_OPCODE(MsgPtr)  (MsgPtr)->u4OpCode
#define IDS_MSG_DATA(MsgPtr)    (MsgPtr)->pu1Buf
#define IDS_MSG_INTF(MsgPtr)    (MsgPtr)->u4IfIndex

#define IDS_LOG_ENABLE        1
#define IDS_LOG_DISABLE       2 

#define IDS_ENABLE        1
#define IDS_DISABLE       2 

#define IDS_RELOAD               1
#define IDS_LOAD_RULES           1
#define IDS_UNLOAD_RULES           2

#define IDS_RULES_NOT_LOADED     2
#define IDS_RULES_LOADED         1
#define IDS_RULES_LOAD_IN_PROGRESS 0


/* Related to IDS Logging */
#define IDS_MIN_LOG_FILESIZE      1024 /* 1KB */
#define IDS_MAX_LOG_FILESIZE      (1024 * 1024) /* 1 MB */

#define IDS_MIN_LOG_THRESHOLD     1
#define IDS_MAX_LOG_THRESHOLD     99
#define IDS_DEFAULT_LOG_THRESHOLD 70

#define IDS_FILENAME_LEN          256

/* Related to IDS Trap */
#define IDS_TRAP_EVENT            1
#define IDS_TRAP_ATTACK_PKT       2

#define IDS_TRAP_TIME_STR_LEN     24

#define IDS_TRAP_LOG_SIZE_EXCEEDED      1
#define IDS_TRAP_LOG_SIZE_THRESHOLD_HIT 2

typedef struct {
    UINT1  au1Version[IDS_VERSION_LEN]; /* Current IDS operating Version */ 
    UINT4  u4AllowStats;  /* Valid number of packets allowed by Ids */
    UINT4  u4DropStats; /* counter for invalid packets identified by Ids */
    UINT4  u4IfIndex; /* To get the interface specific statistics from Ids */
} tIdsGetInfo;

/* prototype definition */
PUBLIC UINT4 SecIdsTaskMain PROTO ((VOID));
INT4 SecPortHandlePktFromSecurity PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                                          UINT1 u1IsIpSec, tCfaIfInfo * IfInfo,
                                          UINT2 u2Protocol, t_IP_HEADER *pIpHdr));
INT4 SecPortHandleLogMessages PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf));

INT4 SecIdsGetInfo PROTO ((UINT4 u4MsgType, tIdsGetInfo * pIdsGetInfo));
INT4 SecIdsSetInfo PROTO ((UINT4 u4MsgType, UINT4 u4Value));
VOID SecIdsCheckForIdsFiles PROTO ((UINT1 *pu1FileName));

#endif /* _IDS_H */

