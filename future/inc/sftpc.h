/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: sftpc.h,v 1.11 2015/04/27 12:49:41 siva Exp $
 *
 * Description:This file contains the exported definitions and  
 *             macros of SFTP Client and API module
 *
 *******************************************************************/
#ifndef __SFTPC_H__
#define __SFTPC_H__
#include "utilipvx.h"
typedef enum {
    SFTP_SUCCESS,
    SFTP_FAILURE
}sftpStatus;
#define MSG_BUF_SIZE 80
extern char SftpErrString[MSG_BUF_SIZE];
#define SFTP_IN_PROGRESS 1
#define SFTP_PROCESS_COMPLETE 0

/* This function parses the given tftp_url token value and 
 * if valid,returns the ip address in pu4IpAddr & the Filename in pi1TftpFileName
 * Returns CLI_SUCCESS/CLI_FAILURE
 */

INT4 CliGetSftpParams PROTO ((INT1 *pi1SftpStr, INT1 *pi1SftpFileName,
           INT1 *pi1SftpUserName, INT1 *pi1SftpPassWd, tIPvXAddr *pIpAddress,
    UINT1 *pu1HostName));

INT1 
sftpcRecvFile (UINT1 *pu1SrcUserName, UINT1 *pu1SrcPassWd, tIPvXAddr SrcIpAddress,
               UINT1 *pu1SrcFileName, UINT1 *pu1LocalDstFile);

INT1
sftpcSendFile (UINT1 *pu1SrcUserName, UINT1 *pu1SrcPassWd, tIPvXAddr SrcIpAddress,
               UINT1 *pu1SrcFileName, UINT1 *pu1LocalDstFile);

INT1
sftpcArSendLogBuffer (UINT1 *pu1DstUserName, UINT1 *pu1DstPassWd, tIPvXAddr DstIpAddress,
                    UINT1 *pu1DstFileName, UINT1 *pu1LogBuffer, UINT4 u4LogBufSize);
INT1
sftpcArSendMultipleBuffers (UINT1 *pu1DstUserName, UINT1 *pu1DstPassWd, tIPvXAddr DstIpAddress,
          UINT1 *pu1DstFileName, UINT1 *au1LogBuffer, UINT4 u4LogBufSize, UINT4 u4ConnStat);

INT1
sftpcSendLogBuffer (UINT1 *pu1DstUserName, UINT1 *pu1DstPassWd, tIPvXAddr DstIpAddress,
                    UINT1 *pu1DstFileName, UINT1 *pu1LogBuffer, UINT4 u4LogBufSize);
INT1
sftpcSendMultipleBuffers (UINT1 *pu1DstUserName, UINT1 *pu1DstPassWd, tIPvXAddr DstIpAddress,
          UINT1 *pu1DstFileName, UINT1 *pu1LogBuffer, UINT4 u4LogBufSize, UINT4 u4ConnStat);

INT4 IssSftpUploadLogs (UINT1 u1Status,UINT1 *pu1ErrBuf);
INT4 IssSftpImageDownload (UINT1 *pu1ErrBuf);
VOID SftpcArErrString (CHR1 *pi1SftpcErrString);   
VOID SftpcGetErrorString (CHR1 *pi1SftpErrorMsg); 
VOID SftpcGetCopyStatus (UINT1 *pu1Status);
#endif /* __SFTPC_H__ */ 
