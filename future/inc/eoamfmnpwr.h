
/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: eoamfmnpwr.h,v 1.2 2012/08/08 10:02:24 siva Exp $
 * 
 * Description: This file contains the necessary data strucutures
 *              for Hardware Programming.
 *              <Part of dual node stacking model>
 *
 * *********************************************************************/

#ifndef _EOAMFM_NP_WR_H
#define _EOAMFM_NP_WR_H

#include "eoamfm.h"

/* OPER ID */

#define  FM_NP_REGISTER_FOR_FAILURE_INDICATIONS                 1
#define  NP_FM_FAILURE_INDICATION_CALLBACK_FUNC                 2

/* Required arguments list for the eoamfm NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

typedef struct {
    UINT4  u4IfIndex;
    UINT1  u1EventType;
    UINT1  u1Status;
    UINT1  au1pad[2];
} tEoamfmNpWrNpFmFailureIndicationCallbackFunc;

typedef struct EoamfmNpModInfo {
union {
    tEoamfmNpWrNpFmFailureIndicationCallbackFunc  sNpFmFailureIndicationCallbackFunc;
    }unOpCode;

#define  EoamfmNpNpFmFailureIndicationCallbackFunc  unOpCode.sNpFmFailureIndicationCallbackFunc;
} tEoamfmNpModInfo;


/* Function Prototype for Generic NP OpCode specific wrappers
 * defined in Eoamfmnpapi.c */

UINT1 EoamfmFmNpRegisterForFailureIndications PROTO ((VOID));
UINT1 EoamfmNpFmFailureIndicationCallbackFunc PROTO ((UINT4 u4IfIndex, UINT1 u1EventType, UINT1 u1Status));

#endif /* _EOAMFM_NP_WR_H */
