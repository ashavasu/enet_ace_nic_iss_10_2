/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: lcm.h,v 1.5 2011/04/28 12:21:16 siva Exp $
 *
 * Description: This file contains all global variables used by
 *              RSTP and MSTP modules. 
 *
 *******************************************************************/


#ifndef _LCM_H_
#define _LCM_H_

#define LCM_CE_VLAN_SIZE                            sizeof(tIssVlanList)
#define LCM_MAX_EVC_IDENTIFIER_LENGTH               100

#define LCM_LOCK() LcmLock()
#define LCM_UNLOCK() LcmUnLock()

#define     LCM_FAILURE                         SNMP_FAILURE
#define     LCM_SUCCESS                         SNMP_SUCCESS

typedef struct LcmCliInfo
{
    UINT1    EvcName[LCM_MAX_EVC_IDENTIFIER_LENGTH];
    UINT4    u4ServiceInstanceNo;
}tLcmPromptInfo;

typedef struct BwProfile
{
    UINT2                u2CirMultiplier;
    UINT2                u2EirMultiplier;
    UINT1                u1Cm;
    UINT1                u1Cf;
    UINT1                u1PerCosBit;
    UINT1                u1CirMagnitude;
    UINT1                u1CbsMagnitude;
    UINT1                u1CbsMultiplier;
    UINT1                u1EirMagnitude;
    UINT1                u1EbsMagnitude;
    UINT1                u1EbsMultiplier;
    UINT1                u1PriorityBits;
    UINT1                au1Reserved[2];
}tBwProfile;

typedef struct LcmEvcStatusInfo
{
    tBwProfile           BwProfile[8]; /*Num of Bw elements can be upto 8*/
    UINT4                u4EtherServInstance;
    UINT2                u2EvcReferenceId;
    UINT1                u1EvcStatus;        /*Active/Partially Active...*/
    UINT1                u1EvcType;
    UINT1                au1EvcIdentifier[LCM_MAX_EVC_IDENTIFIER_LENGTH]; 
                                            /*Max should be 100*/
    UINT1                u1NoOfCos;
    UINT1                au1Reserved[3];
}tLcmEvcStatusInfo;

typedef struct LcmEvcCeVlanInfo
{
    UINT2                u2EvcReferenceId;
    UINT2                u2FirstVlanId;
    UINT2                u2LastVlanId;
    UINT2                u2NoOfVlansMapped;
    tIssVlanList         CeVlanMapInfo;
    UINT1                u1UntagPriTag;
    UINT1                u1DefaultEvc;
    UINT1                u1ChangedFlag;
    UINT1                u1Reserved;
}tLcmEvcCeVlanInfo;

typedef struct LcmEvcInfo
{
    tLcmEvcStatusInfo   LcmEvcStatusInfo;
    tLcmEvcCeVlanInfo   LcmEvcCeVlanInfo;
}tLcmEvcInfo;

INT4
LcmGetEvcStatusInfo(UINT2 u2PortNo, UINT2 u2EvcRefId, 
                     tLcmEvcStatusInfo *pLcmEvcStatusInfo);
INT4 LcmGetNextEvcStatusInfo(UINT2 u2PortNo, UINT2 u2EvcRefId, 
            tLcmEvcStatusInfo *pLcmEvcStatusInfo,UINT1 *pu1NoEvcInfo);
INT4 LcmGetEvcCeVlanInfo(UINT2 u2PortNo, UINT2 u2EvcRefId, 
            tLcmEvcCeVlanInfo *pLcmEvcCeVlanInfo);
INT4 LcmGetNextEvcCeVlanInfo(UINT2 u2PortNo, UINT2 u2EvcRefId, 
            tLcmEvcCeVlanInfo *pLcmEvcCeVlanInfo,UINT1 *pu1NoEvcInfo);
INT4
LcmFindEvcId(UINT1 *pu1EvcId);
INT4
LcmCreateEvc(UINT1 *pu1EvcId);
VOID
LcmDeleteEtherServiceInstance
                (UINT2 u2PortNo,UINT2 u2EtherServiceInstance,UINT1 *pu1EvcId);
VOID
LcmDeleteEvc(UINT1 *pu1EvcId);
VOID
LcmUpdateEvcAttributes(UINT2 u2RefId );
INT4
LcmUpdateEvcOamProtocolMap(UINT1 *pu1EvcId, UINT1 u1OamProtocol,UINT2 
u2SVlan,UINT1* pu1DomainName);
INT4
LcmUpdateEvcUniCount(UINT1 *pu1EvcId,UINT2 u2UniCount,UINT4 u4UniType);
INT4
LcmUpdateEvcEtherServiceInstance(UINT2 u2PortNo,
                UINT2 u2EtherServiceInstance,
                UINT1 *pu1EvcId,
                UINT2 u2AssignedEvcRefId);
INT4
LcmUpdateCVlanEvcMap( UINT2 u2PortNo,UINT1 *pu1EvcId,
                 UINT4 u4Vlan,UINT4 u4CommandType);
INT4  
LcmSetEvcBwProfile(UINT1 *pu1EvcId,UINT4 u4Cm,UINT4 u4Cf,UINT4 u4PerCosBit,
                   UINT4 u4CirMagnitude,UINT4 u4CirMultiplier,UINT4 u4CbsMagnitude,
                   UINT4 u4CbsMultiplier,UINT4 u4EirMagnitude,UINT4 u4EirMultiplier,
                   UINT4 u4EbsMagnitude,UINT4 u4EbsMultiplier,UINT4 u4PriorityBits);
VOID
LcmSetDefaultEvc(UINT1 *pu1EvcId);
VOID
LcmEvcStatusChange(UINT1 *pu1EvcId, UINT1 u1Status);
VOID              
LcmElmiOperStatusIndication (UINT2 u2PortNo,UINT1 u1ElmOperStatus);
VOID
LcmPassCeVlanInfo(UINT2 u2PortNo,tLcmEvcCeVlanInfo *pElmCeVlanInfo);
INT4
LcmGetEvcInfo(UINT2 u2PortNo,UINT2 u2EvcReferenceId,tLcmEvcInfo *pLcmEvcInfo);
INT4
LcmGetNextEvcInfo(UINT2 u2PortNo,UINT2 u2EvcReferenceId,tLcmEvcInfo *pLcmEvcInfo);
VOID
LcmEvcStatusChangeIndication(UINT2 u2PortNo,UINT2 u2EvcReferenceId,UINT1 u1EvcStatus);
VOID                      
LcmCreateNewEvcIndication (UINT2 u2PortNo,tLcmEvcStatusInfo *pLcmEvcStatusInfo);
VOID
LcmDeleteEvcIndication(UINT2 u2PortNo,UINT2 u2EvcReferenceId);
VOID
LcmDatabaseUpdateIndication(UINT2 u2PortNo,tLcmEvcStatusInfo *pLcmEvcStatusInfo);
INT4
LcmLock (VOID);
INT4
LcmUnLock (VOID);
INT4 LcmInitialize(INT1 *pi1Dummy);


#endif
