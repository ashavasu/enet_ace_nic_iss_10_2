/* $Id: cxe2130.h,v 1.59 2016/10/03 10:34:41 siva Exp $ */
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : cxe2130.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        :                                                */
/*    MODULE NAME           :                                                */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : SWITCHCORE                                     */
/*    DATE OF FIRST RELEASE : 6 May 2005                                     */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains sizing parameters for the   */
/*                            CXE2130 environment                            */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    06 May 2005            Initial Creation                        */
/*---------------------------------------------------------------------------*/
#ifndef _CXE_SIZE_H
#define _CXE_SIZE_H

#include "cxehal/cxedrv/cxedrv.h"
#include "cxehal/vcxedrv/vcxedrv.h"
#include "cxehal/cxedrv/cxe.h"
#include "cxehal/port/port.h"
#include "cxehal/board/board.h"
#include "cxehal/cxedrv/kernel/cxehlc/cxeforw/cxeforwsegments.h"
#include "cxehal/netglue/netglue.h"
#include "cxehal/netglue/kernel/netglue_packet.h"
#include "user/libs/net/net.h"

#define SYS_DEF_MAX_PHYSICAL_INTERFACES \
(SYS_DEF_MAX_DATA_PORT_COUNT + SYS_DEF_MAX_INFRA_SYS_PORT_COUNT)

#define SYS_DEF_MAX_RADIO_INTERFACES        0
#define SYS_DEF_MAX_ENET_INTERFACES      SYS_DEF_MAX_PHYSICAL_INTERFACES - \
                                         SYS_DEF_MAX_RADIO_INTERFACES

#define SYS_DEF_MAX_DATA_PORT_COUNT 28 /* Max No of Front Panel Ports */
#define SYS_DEF_MAX_INFRA_SYS_PORT_COUNT 0 /* Max No of connecting Ports
                                              used for Dual Unit Stacking*/
#define SYS_DEF_NUM_PHYSICAL_INTERFACES SYS_DEF_MAX_PHYSICAL_INTERFACES

/* Switchcore doesn't support MI, so MAX ports per context will be 
 * SYS_DEF_MAX_PHYSICAL_INTERFACES + AGG Interfaces. Hence map it to 
 * BRG_MAX_PHY_PLUS_LOG_PORTS. */
#define SYS_DEF_MAX_PORTS_PER_CONTEXT   BRG_MAX_PHY_PLUS_LOG_PORTS

#define SYS_DEF_MAX_NUM_CONTEXTS        1

#define MAX_TAP_INTERFACES            0

#ifdef MPLS_WANTED
#define SYS_DEF_MAX_L2_PSW_IFACES       100
#define SYS_DEF_MAX_L2_AC_IFACES        100
#else
#define SYS_DEF_MAX_L2_PSW_IFACES       0
#define SYS_DEF_MAX_L2_AC_IFACES        0
#endif

#ifdef VXLAN_WANTED
#define SYS_DEF_MAX_NVE_IFACES       100
#else
#define SYS_DEF_MAX_NVE_IFACES       0
#endif

#define VLAN_MAX_PORTS_IN_SYSTEM        BRG_MAX_PHY_PLUS_LOG_PORTS 
#define VLAN_MAX_VLAN_ID                VLAN_DEV_MAX_VLAN_ID

/*  No of VLANs are increased with count VLAN_MAX_NUM_VFI_IDS to provide VPLS
 *  visibility to L2 modules.
 *  VLAN ID is increased with VLAN_DEV_MAX_VFI_ID.
 *
 *  Here we can configure 256 VLANs. For these VLANs, VLAN ID should be present
 *  between (1 - 4094).
 *
 *  We can configure VFIs for the count VLAN_MAX_NUM_VFI_IDS. For these VFIs,
 *  VFI ID should be present between (VLAN_VFI_MIN_ID to VLAN_VFI_MAX_ID)
 */
#define VLAN_DEV_MAX_VLAN_ID      (4094  +  VLAN_DEV_MAX_VFI_ID)

#define VLAN_DEV_MAX_CUSTOMER_VLAN_ID   4096

 /* Maximum number of VLANs*/
#define VLAN_DEV_MAX_NUM_VLAN     (256 + VLAN_MAX_NUM_VFI_IDS)

#ifdef MPLS_WANTED
#define VLAN_DEV_MAX_VFI_ID       0
#define VLAN_MAX_NUM_VFI_IDS      0
#else
#define VLAN_DEV_MAX_VFI_ID       0
#define VLAN_MAX_NUM_VFI_IDS      0
#endif

#define VLAN_VFI_MIN_ID           (VLAN_DEV_MAX_VLAN_ID - VLAN_DEV_MAX_VFI_ID)
#define VLAN_VFI_MAX_ID           VLAN_DEV_MAX_VLAN_ID

#define VLAN_DEV_MAX_ST_VLAN_ENTRIES    256  /* Maximum number of static
            * VLANs. This number should be
            * lesser than or equal to 
            * maximum number of VLANs */

#define VLAN_DEV_MAX_NUM_COSQ           8     /* Maximum number of hardware 
            * Queues
            */
/* STP Sizing Params */ 
#define AST_MAX_PORTS_IN_SYSTEM               BRG_MAX_PHY_PLUS_LOG_PORTS 
#define AST_MAX_CEP_IN_PEB              8

#define SNOOP_MAX_IGS_PORTS                   BRG_MAX_PHY_PLUS_LOG_PORTS

#ifdef ISS_METRO_WANTED
#define VLAN_MAX_VID_TRANS_ENTS         4094
#endif /*ISS_METRO_WANTED*/
#define AST_MAX_SERVICES_PER_CUSTOMER   128

#define VLAN_MAX_MAC_MAP_ENTRIES       1 /* applicable for MAC based VLAN */

#define VLAN_MAX_VID_SET_ENTRIES_PER_PORT 10

#ifdef MSTP_WANTED
#define AST_MAX_MST_INSTANCES          (15 + 1) /* no. of instance + CIST */
#else
/*When MSTP_WANTED is disabled more than one ERPS vlan group can not be configured
 *so this macro value is changed to (15 + 1)*/
#define AST_MAX_MST_INSTANCES          (15 + 1) /* no. of instance + CIST */
#endif /*MSTP_WANTED */

#define IP_DEV_MAX_L3VLAN_INTF          256  /* Maximum IP interfaces */
#define IP_DEV_MAX_TNL_INTF             20  /* Maximum IP interfaces */
#define SYS_DEF_MAX_L3SUB_IFACES        64 /* Maximum L3Subinterface */

#define LA_DEV_MAX_TRUNK_GROUP          8    /* Maximum Trunk groups */  
#define LA_MAX_PORTS_PER_AGG            16    /* Maximum no.of ports in 
                  * aggregation */
#define LA_MIN_PORT_INDEX_PER_AGG SYS_DEF_NUM_PHYSICAL_INTERFACES + 1
#define LA_MAX_PORT_INDEX_PER_AGG SYS_DEF_NUM_PHYSICAL_INTERFACES + LA_DEV_MAX_TRUNK_GROUP

/* Vlan filtering entry sizing */

/* Maximum number of L2 static unicast entries -
 * This number includes PBB-TE static unicast entries also */
#define VLAN_DEV_MAX_ST_UCAST_ENTRIES   (50)
/* Maximum number of L2 dynamic unicast entries -
 * Assuming the number of learnt unicast entries to be 1000 */
#define VLAN_DEV_MAX_L2_TABLE_SIZE      (4096)
/* Maximum number of L2 multicast entries -
 * This number includes PBB-TE static multicast entries also */
#define VLAN_DEV_MAX_MCAST_TABLE_SIZE   (100)

#define VLAN_DEF_DYN_UCAST_ENTRIES_PER_VLAN   400 /* Default value of Limit  
                                     * of Dynamic unicast entries that can be 
                                     * learnt per vlan. 10% of the maximum
                                     * L2 unicast entries per switch.
                                     */

#define IP_DEV_MAX_ADDR_PER_IFACE       5   /* Maximum IP address associated 
                                               with the interface */

/* ARP Table table Size  */
#define IP_DEV_MAX_IP_TBL_SZ            1000 

/* Maximum Route entries. Route entries in the FIB(IP_DEV_MAX_ROUTE_TBL_SZ) depends on
 * the routes from different routing protocols.Route entries from various parameter 
 * should be tuned based on the limitation in H/W. */
#define RIP_DEF_MAX_ROUTES              1000
#define OSPF_DEF_MAX_ROUTES             2500
#define BGP_DEF_MAX_ROUTES               500 
#define IP_DEF_MAX_STATIC_ROUTES         100
#define IP_MAX_INTERFACE_ROUTES        (IP_DEV_MAX_L3VLAN_INTF * IP_DEV_MAX_ADDR_PER_IFACE)
                                        /* By default 128 * 5 */
                                        
#define IP_DEV_MAX_ROUTE_TBL_SZ         RIP_DEF_MAX_ROUTES + OSPF_DEF_MAX_ROUTES + \
                                        BGP_DEF_MAX_ROUTES + IP_DEF_MAX_STATIC_ROUTES + IP_MAX_INTERFACE_ROUTES

                                        
/* L3 multicast table size */ 
#define IP_DEV_L3MCAST_TABLE_SIZE       256
/*DIFFSERV*/

/*
 * For Switchcore MinRefresCount = 64 Kbps, MaxRefresCount = 1048576 Kbps
 * */
#define NP_DIFFSRV_MIN_REFRESH_COUNT 64   /* Kb/s */
#define NP_DIFFSRV_MAX_REFRESH_COUNT 1048576 /* (1024*1024)Kb/s */

#define RATE_LIMIT_MAX_VALUE                 0x3ffff

#define ISS_MAX_L2_FILTERS                  50
#define ISS_MAX_L3_FILTERS                  90
#define ISS_MAX_L4S_FILTERS                 20

#define ISS_FILTER_SHADOW_MEM_SIZE          4  /* Filter Shadow Size */

#define   SYS_DEF_MAX_NBRS                  10



/* TCP */
#define   TCP_DEF_MAX_NUM_OF_TCB            500

/* PIM */
#define   PIM_DEF_MAX_SOURCES               100
#define   PIM_DEF_MAX_RPS                   100

/* DVMRP */
#define   DVMRP_DEF_MAX_SOURCES             100


/* OSPF */
#define   OSPF_DEF_MAX_LSA_PER_AREA         100 
#define   OSPF_DEF_MAX_EXT_LSAS             2500 

/* BGP */
#define   BGP_DEF_MAX_PEERS                 50
#define   BGP_DEF_MAX_CAPS_PER_PEER         10
#define   BGP_DEF_MAX_CAP_DATA_SIZE         16
#define   BGP_DEF_MAX_INSTANCES_PER_CAP     5
#define   BGP_DEFAULT_MAX_INPUT_COMM_FILTER_TBL_ENTRIES      1000
#define   BGP_DEFAULT_MAX_OUTPUT_COMM_FILTER_TBL_ENTRIES     1000
#define   BGP_DEFAULT_MAX_INPUT_EXT_COMM_FILTER_TBL_ENTRIES  200
#define   BGP_DEFAULT_MAX_OUTPUT_EXT_COMM_FILTER_TBL_ENTRIES 200

/* DHCP Server */

#define DHCP_SRV_MAX_POOLS                  5
#define DHCP_SRV_MAX_HOST_PER_POOL          20

/*MPLS -> VLAN */
#define VLAN_MAX_L2VPN_ENTRIES             MAX_L2VPN_PW_VC_ENTRIES
/* CLI and Telnet */

#define CLI_MAX_SESSIONS                    10
#define CLI_MAX_USERS                       20
#define CLI_MAX_GROUPS                      10

/* SSL and Web */

#define ISS_MAX_WEB_SESSIONS                10
#define ISS_MAX_SSL_SESSIONS                10

#define QOS_MAX_REGEN_INNER_PRIORITY_VAL    (7)
/*
 * if h/w supports portlist it is set to VLAN_TRUE and otherwise 
 * set to VLAN_FALSE.If portlist is not supported whenever the 
 * vlan member ports are modified we have to set/reset the 
 * added/deleted ports specifically 
*/

#define VLAN_HW_PORTLIST_SUPPORTED()       VLAN_TRUE


#define CXE_ISS_DEVICE_FILE_NAME "/dev/isscxe"
#define CXE_ISS_MAJOR_NUMBER 100
#define CXE_ISS_MINOR_NUMBER 0

#define FNP_VR_ENABLE            1
#define FNP_VR_DISABLE           2

#define FNP_ETHADDR_LEN          6
#define FNP_MAC_ADDR_LEN         FNP_ETHADDR_LEN

/* To be mapped to OS calls */
#define FNP_MEM_ALLOC           MEM_MALLOC
#define FNP_FREE                MEM_FREE  
#define FNP_MEM_CPY             MEMCPY
#define FNP_MEM_SET             MEMSET

#define FNP_INFO                0xE
#define FNP_CRITICAL            0xF

/* The following enable or disable forwarding per VR if
 * supported otherwise its for the whole router
 */
#define FNP_FORW_ENABLE           1
#define FNP_FORW_DISABLE          2

#define FNP_HW_ADDR_EQUAL         0
#define FNP_HW_ADDR_UNEQUAL       1

#define FNP_NOT_PRUNED_INTERFACE  0
#define FNP_MC_COUNTER_SET        2 
#define FNP_NO_ECMP_ROUTE         0 
#define FNP_NO_DS_INTERFACE       0 

#define FNP_DEFAULT_INTERFACE     0
#define FNP_DEFAULT_GRP_ADDR      0
#define FNP_DEFAULT_SRC_ADDR      0

/* Unicast VRID mappings which will be enabled with the respective 
   Macro'w when VR is enabled in Future IP code */


#define OSPF_VRID                0        
#define IP_VRID                  0
#define RTM_VRID                 0
#define BGP_VRID                 0
#define PIM_VRID                 0        
#define ARP_VRID(u4IfIndex)      0

/* Initialises Next Hop information */
#define FSNP_INIT_NEXTHOP_INFO(x) MEMSET(&(x), 0, sizeof (tFsNpNextHopInfo))

#define FNP_SET_DEFAULTS_IN_ROUTE_ENTRY(RtEntry)                           \
{                                                                \
        RtEntry.eCommand = ROUTE ;                                    \
                RtEntry.u2ChkRpfFlag = FNP_TRUE ;                             \
                RtEntry.u2TtlDecFlag   = FNP_TRUE ;                           \
}

#if defined (WLC_WANTED) || defined (WTP_WANTED)
#define MAX_NUM_OF_AP                   4097
#define NUM_OF_AP_SUPPORTED             4
#define MAX_NUM_OF_RADIO_PER_AP         31
#define MAX_NUM_OF_RADIOS  (NUM_OF_AP_SUPPORTED *\
      MAX_NUM_OF_RADIO_PER_AP)
#define MAX_NUM_OF_SSID_PER_WLC         50
#define MAX_NUM_OF_BSSID_PER_RADIO      16
#define MAX_NUM_OF_STA_PER_AP           32
#define MAX_NUM_OF_STA_PER_WLC          (NUM_OF_AP_SUPPORTED *\
                                         MAX_NUM_OF_STA_PER_AP)
#endif

#ifdef WLC_WANTED
#define SYS_DEF_MAX_WSS_IFACES  (MAX_NUM_OF_RADIOS +\
      MAX_NUM_OF_SSID_PER_WLC +\
                     (MAX_NUM_OF_BSSID_PER_RADIO *\
      MAX_NUM_OF_RADIOS))
#elif WTP_WANTED
#define SYS_DEF_MAX_WSS_IFACES  (MAX_NUM_OF_BSSID_PER_RADIO *\
                      SYS_DEF_MAX_RADIO_INTERFACES)
#else
#define SYS_DEF_MAX_WSS_IFACES          0
#endif

/*
 * Hold the size of platform specific structure,
 * Its value could be changed upon platform.
 */
#define VLAN_HW_STATS_ENTRY_LEN                4

#define   CFA_MAX_GIGA_ENET_MTU   9216
#define   CFA_MAX_L3_INTF_ENET_MTU      9216

/* Number of S-Channel interfaces. Referring S-Channel interfaces
 * as SBP interfaces */

#ifdef EVB_WANTED
#define SYS_DEF_MAX_SBP_IFACES          0
#else
#define SYS_DEF_MAX_SBP_IFACES          0
#endif

#endif

