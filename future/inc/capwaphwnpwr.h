#ifndef __CAPWAP_NP_WR_H__
#define __CAPWAP_NP_WR_H__

#ifdef NPAPI_WANTED
#include "npapi.h"
#include "capwap.h"

#define CAPWAP_ENABLE 1
#define CAPWAP_DISABLE 2
#define CAPWAP_CREATE_UNICAST_TUNNEL 4
#define CAPWAP_CREATE_MULTICAST_TUNNEL 5
#define CAPWAP_CREATE_BROADCAST_TUNNEL 6
#define CAPWAP_DELETE_UNICAST_TUNNEL 7
#define CAPWAP_DELETE_MULTICAST_TUNNEL 8
#define CAPWAP_DELETE_BROADCAST_TUNNEL 9
#define CAPWAP_DELETE_ALL_TUNNEL 10
#define CAPWAP_GET_UNICAST_TUNNEL_INFO 11
#define CAPWAP_GET_MULTICAST_TUNNEL_INFO 12
#define CAPWAP_GET_BROADCAST_TUNNEL_INFO 13

typedef struct {
    tCapwapNpParams *ptCapwapNpParams;
} tCapwapNpWrEnable;

typedef struct {
    tCapwapNpParams *ptCapwapNpParams;
} tCapwapNpWrDisable;

typedef struct {
    tCapwapNpParams *ptCapwapNpParams;
} tCapwapNpWrCreateUcTunnel;

typedef struct {
    tCapwapNpParams *ptCapwapNpParams;
} tCapwapNpWrCreateMcTunnel;

typedef struct {
    tCapwapNpParams *ptCapwapNpParams;
} tCapwapNpWrCreateBcTunnel;

typedef struct {
    tCapwapNpParams *ptCapwapNpParams;
} tCapwapNpWrDeleteUcTunnel;

typedef struct {
    tCapwapNpParams *ptCapwapNpParams;
} tCapwapNpWrDeleteMcTunnel;

typedef struct {
    tCapwapNpParams *ptCapwapNpParams;
} tCapwapNpWrDeleteBcTunnel;

typedef struct {
    tCapwapNpParams *ptCapwapNpParams;
} tCapwapNpWrDeleteAllTunnel;

typedef struct {
    tCapwapNpParams *ptCapwapNpParams;
} tCapwapNpWrGetUcTunnelInfo;

typedef struct {
    tCapwapNpParams *ptCapwapNpParams;
} tCapwapNpWrGetMcTunnelInfo;

typedef struct {
    tCapwapNpParams *ptCapwapNpParams;
} tCapwapNpWrGetBcTunnelInfo;

typedef struct CapwapNpModInfo {
union {
    tCapwapNpWrEnable     sCapwapNpEnable;
    tCapwapNpWrDisable    sCapwapNpDisable;
    tCapwapNpWrCreateUcTunnel   sCapwapNpCreateUcTunnel;
    tCapwapNpWrCreateMcTunnel   sCapwapNpCreateMcTunnel;
    tCapwapNpWrCreateBcTunnel   sCapwapNpCreateBcTunnel;
    tCapwapNpWrDeleteUcTunnel   sCapwapNpDeleteUcTunnel;
    tCapwapNpWrDeleteMcTunnel   sCapwapNpDeleteMcTunnel;
    tCapwapNpWrDeleteBcTunnel   sCapwapNpDeleteBcTunnel;
    tCapwapNpWrDeleteAllTunnel  sCapwapNpDeleteAllTunnel;
    tCapwapNpWrGetUcTunnelInfo  sCapwapNpGetUcTunnelInfo;
    tCapwapNpWrGetMcTunnelInfo  sCapwapNpGetMcTunnelInfo;
    tCapwapNpWrGetBcTunnelInfo  sCapwapNpGetBcTunnelInfo;
    }unOpCode;

#define  CapwapNpEnable  unOpCode.sCapwapNpEnable;
#define  CapwapNpDisable  unOpCode.sCapwapNpDisable;
#define  CapwapNpCreateUcTunnel  unOpCode.sCapwapNpCreateUcTunnel;
#define  CapwapNpCreateMcTunnel  unOpCode.sCapwapNpCreateMcTunnel;
#define  CapwapNpCreateBcTunnel  unOpCode.sCapwapNpCreateBcTunnel;
#define  CapwapNpDeleteUcTunnel  unOpCode.sCapwapNpDeleteUcTunnel;
#define  CapwapNpDeleteMcTunnel  unOpCode.sCapwapNpDeleteMcTunnel;
#define  CapwapNpDeleteBcTunnel  unOpCode.sCapwapNpDeleteBcTunnel;
#define  CapwapNpDeleteAllTunnel  unOpCode.sCapwapNpDeleteAllTunnel;
#define  CapwapNpGetUcTunnelInfo  unOpCode.sCapwapNpGetUcTunnelInfo;
#define  CapwapNpGetMcTunnelInfo  unOpCode.sCapwapNpGetMcTunnelInfo;
#define  CapwapNpGetBcTunnelInfo  unOpCode.sCapwapNpGetBcTunnelInfo;

}tCapwapNpModInfo;
#endif
#endif
