#ifndef _L2DS_H
#define _L2DS_H
/*****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2007                                               */
/* $Id: l2ds.h,v 1.4 2013/02/09 08:21:07 siva Exp $                                                                    */
/*****************************************************************************/
/*    FILE  NAME            : l2ds.h                                         */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : L2DHCP Snoop                                   */
/*    MODULE NAME           : L2DHCP snoop globals                           */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains macros, definitions and     */
/*                            prototyes used from other modules              */
/*---------------------------------------------------------------------------*/

VOID L2DSMain (INT1 *);

VOID L2dsPortEnquesDhcpPkts (tCRU_BUF_CHAIN_HEADER *, UINT4, tVlanTag);

VOID L2dsPortVlanDelete (UINT4 u4ContextId, tVlanId);

PUBLIC VOID L2dsApiCreateContext (UINT4);
PUBLIC VOID  L2dsApiDeleteContext (UINT4);

INT4
L2dsUtilIsDhcpSnoopingEnabled (UINT4 u4Instance);

#define L2DS_ENABLED                1
#define L2DS_DISABLED               2
/* Trace Related Macros */
#define L2DS_FN_ENTRY               (0x1 << 1)
#define L2DS_FN_EXIT                (0x1 << 2)
#define L2DS_DBG_TRC                (0x1 << 3)
#define L2DS_FAIL_TRC               (0x1 << 4)
#define L2DS_FN_ARGS                (0x1 << 5)

#define L2DS_ALL_TRC                (L2DS_FN_ENTRY | L2DS_FN_EXIT | \
                                     L2DS_FAIL_TRC | L2DS_DBG_TRC | L2DS_FN_ARGS)
    
#endif /* _L2DS_H */
