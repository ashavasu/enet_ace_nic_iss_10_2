
/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: pnacnpwr.h,v 1.3 2012/10/31 09:40:08 siva Exp $
 *
 * Description:This file contains the necessary data strucutures
 *             for packet handling in NPAPI layer
 *             <Part of dual node stacking model>
 *
 *******************************************************************/

#ifndef __PNAC_NP_WR_H__
#define __PNAC_NP_WR_H__

#include "pnac.h"
#ifdef MBSM_WANTED
#include "mbsm.h"
#endif /* MBSM_WANTED */

/* OPER ID */

#ifdef L2RED_WANTED
#define  FS_PNAC_RED_HW_UPDATE_D_B                              1
#endif /* L2RED_WANTED */
#define  PNAC_HW_ADD_OR_DEL_MAC_SESS                            2
#define  PNAC_HW_DISABLE                                        3
#define  PNAC_HW_ENABLE                                         4
#define  PNAC_HW_GET_AUTH_STATUS                                5
#define  PNAC_HW_GET_SESSION_COUNTER                            6
#define  PNAC_HW_SET_AUTH_STATUS                                7
#define  PNAC_HW_START_SESSION_COUNTERS                         8
#define  PNAC_HW_STOP_SESSION_COUNTERS                          9
#ifdef MBSM_WANTED
#define  PNAC_MBSM_HW_ENABLE                                    10
#define  PNAC_MBSM_HW_SET_AUTH_STATUS                           11
#endif /* MBSM_WANTED */

/* Required arguments list for the pnac NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

#ifdef L2RED_WANTED
typedef struct {
    UINT4    u4Port;
    UINT1 *  pu1SuppAddr;
    UINT1    u1AuthMode;
    UINT1    u1AuthStatus;
    UINT1    u1CtrlDir;
    UINT1    au1pad[1];
} tPnacNpWrFsPnacRedHwUpdateDB;

#endif /* L2RED_WANTED */
typedef struct {
    UINT4    u4Port;
    UINT1 *  pu1SuppAddr;
    UINT1    u1SessStatus;
 UINT1    au1pad[3];
} tPnacNpWrPnacHwAddOrDelMacSess;

typedef struct {
    UINT4    u4PortNum;
    UINT1 *  pu1SuppAddr;
    UINT2 *  pu2AuthStatus;
    UINT2 *  pu2CtrlDir;
    UINT1    u1AuthMode;
 UINT1    au1pad[3];
} tPnacNpWrPnacHwGetAuthStatus;

typedef struct {
    UINT4    u4Port;
    UINT1 *  pu1SuppAddr;
    UINT4 *  pu4HiCounter;
    UINT4 *  pu4LoCounter;
    UINT1    u1CounterType;
 UINT1    au1pad[3];
} tPnacNpWrPnacHwGetSessionCounter;

typedef struct {
    UINT4    u4Port;
    UINT1 *  pu1SuppAddr;
    UINT1    u1AuthMode;
    UINT1    u1AuthStatus;
    UINT1    u1CtrlDir;
 UINT1    au1pad[1];
} tPnacNpWrPnacHwSetAuthStatus;

typedef struct {
    UINT4    u4Port;
    UINT1 *  pu1SuppAddr;
} tPnacNpWrPnacHwStartSessionCounters;

typedef struct {
    UINT4    u4Port;
    UINT1 *  pu1SuppAddr;
} tPnacNpWrPnacHwStopSessionCounters;

#ifdef MBSM_WANTED
typedef struct {
    tMbsmSlotInfo *  pSlotInfo;
} tPnacNpWrPnacMbsmHwEnable;

typedef struct {
    UINT4            u4Port;
    UINT1 *          pu1SuppAddr;
    tMbsmSlotInfo *  pSlotInfo;
    UINT1            u1AuthMode;
    UINT1            u1AuthStatus;
    UINT1            u1CtrlDir;
 UINT1            au1pad[1];
} tPnacNpWrPnacMbsmHwSetAuthStatus;

#endif /* MBSM_WANTED */
typedef struct PnacNpModInfo {
union {
#ifdef L2RED_WANTED
    tPnacNpWrFsPnacRedHwUpdateDB  sFsPnacRedHwUpdateDB;
#endif /*  L2RED_WANTED */
    tPnacNpWrPnacHwAddOrDelMacSess  sPnacHwAddOrDelMacSess;
    tPnacNpWrPnacHwGetAuthStatus  sPnacHwGetAuthStatus;
    tPnacNpWrPnacHwGetSessionCounter  sPnacHwGetSessionCounter;
    tPnacNpWrPnacHwSetAuthStatus  sPnacHwSetAuthStatus;
    tPnacNpWrPnacHwStartSessionCounters  sPnacHwStartSessionCounters;
    tPnacNpWrPnacHwStopSessionCounters  sPnacHwStopSessionCounters;
#ifdef MBSM_WANTED
    tPnacNpWrPnacMbsmHwEnable  sPnacMbsmHwEnable;
    tPnacNpWrPnacMbsmHwSetAuthStatus  sPnacMbsmHwSetAuthStatus;
#endif /* MBSM_WANTED */
    }unOpCode;

#ifdef L2RED_WANTED
#define  PnacNpFsPnacRedHwUpdateDB  unOpCode.sFsPnacRedHwUpdateDB;
#endif /* L2RED_WANTED */
#define  PnacNpPnacHwAddOrDelMacSess  unOpCode.sPnacHwAddOrDelMacSess;
#define  PnacNpPnacHwGetAuthStatus  unOpCode.sPnacHwGetAuthStatus;
#define  PnacNpPnacHwGetSessionCounter  unOpCode.sPnacHwGetSessionCounter;
#define  PnacNpPnacHwSetAuthStatus  unOpCode.sPnacHwSetAuthStatus;
#define  PnacNpPnacHwStartSessionCounters  unOpCode.sPnacHwStartSessionCounters;
#define  PnacNpPnacHwStopSessionCounters  unOpCode.sPnacHwStopSessionCounters;
#ifdef MBSM_WANTED
#define  PnacNpPnacMbsmHwEnable  unOpCode.sPnacMbsmHwEnable;
#define  PnacNpPnacMbsmHwSetAuthStatus  unOpCode.sPnacMbsmHwSetAuthStatus;
#endif /* MBSM_WANTED */
} tPnacNpModInfo;


/* Function Prototype for Generic NP OpCode specific wrappers
 * defined in Pnacnpapi.c */

#ifdef L2RED_WANTED
UINT1 PnacFsPnacRedHwUpdateDB PROTO ((UINT2 u2Port, UINT1 * pu1SuppAddr, UINT1 u1AuthMode, UINT1 u1AuthStatus, UINT1 u1CtrlDir));
#endif /* L2RED_WANTED */
UINT1 PnacPnacHwAddOrDelMacSess PROTO ((UINT2 u2Port, UINT1 * pu1SuppAddr, UINT1 u1SessStatus));
UINT1 PnacPnacHwDisable PROTO ((VOID));
UINT1 PnacPnacHwEnable PROTO ((VOID));
UINT1 PnacPnacHwGetAuthStatus PROTO ((UINT2 u2PortNum, UINT1 * pu1SuppAddr, UINT1 u1AuthMode, UINT2 * pu2AuthStatus, UINT2 * pu2CtrlDir));
UINT1 PnacPnacHwGetSessionCounter PROTO ((UINT2 u2Port, UINT1 * pu1SuppAddr, UINT1 u1CounterType, UINT4 * pu4HiCounter, UINT4 * pu4LoCounter));
UINT1 PnacPnacHwSetAuthStatus PROTO ((UINT2 u2Port, UINT1 * pu1SuppAddr, UINT1 u1AuthMode, UINT1 u1AuthStatus, UINT1 u1CtrlDir));
UINT1 PnacPnacHwStartSessionCounters PROTO ((UINT2 u2Port, UINT1 * pu1SuppAddr));
UINT1 PnacPnacHwStopSessionCounters PROTO ((UINT2 u2Port, UINT1 * pu1SuppAddr));
UINT1 PnacIssHwSetLearningMode PROTO ((UINT4 u4IfIndex, INT4 i4Status));
#ifdef MBSM_WANTED
UINT1 PnacPnacMbsmHwEnable PROTO ((tMbsmSlotInfo * pSlotInfo));
UINT1 PnacPnacMbsmHwSetAuthStatus PROTO ((UINT2 u2Port, UINT1 * pu1SuppAddr, UINT1 u1AuthMode, UINT1 u1AuthStatus, UINT1 u1CtrlDir, tMbsmSlotInfo * pSlotInfo));
#endif /* MBSM_WANTED */

#endif /* __PNAC_NP_WR_H__ */
