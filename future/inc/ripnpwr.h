
/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: ripnpwr.h,v 1.1 2013/03/28 12:08:07 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              for RIP wrappers
 *              
 ********************************************************************/
#ifndef __RIP_NP_WR_H__
#define __RIP_NP_WR_H__

UINT1 RipFsNpRipInit PROTO ((VOID));
UINT1 RipFsNpRipDeInit PROTO ((VOID));

#endif /* __IP_NP_WR_H__ */
