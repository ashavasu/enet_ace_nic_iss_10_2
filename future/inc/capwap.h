/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: capwap.h,v 1.50 2018/02/08 10:07:56 siva Exp $
 *
 * Description:This file contains the exported definitions and
 *             macros of CFA
 *
 *****************************************************************/
#ifndef __CAPWAP_DEFN_H__
#define __CAPWAP_DEFN_H__

#include "lr.h"
#include "radioif.h"
#include "wsswlan.h"
#include "tcp.h"
#include "fsvlan.h"
#include "wsssta.h"
#ifdef RFMGMT_WANTED
#include "rfmgmt.h"
#endif

#define  tSegment         tCRU_BUF_CHAIN_HEADER
#define NO_AP_PRESENT         2
#define _CAPWAP_GLOBAL_VAR
#ifdef WPS_WTP_WANTED
#define RADIO_NAME_MAX_LEN       12
#define WLAN_INDEX_MAX            32
#define WLAN_NAME_MAX_LEN         10
#endif
#define MAX_POOLS    16
#define MAX_IP_ROUTES     16
#define MAX_L3IFACES    16
UINT4 CapwapRecvMainTaskInit (VOID);
UINT4 CapwapServiceTaskInit (VOID);
UINT4 CapwapDiscTaskMain (VOID);

#define   CAP_WANTED 0
#define   SOCKET_ID  -1
#define   OFF_SET -1
#define   DESTINATION_DATA_PORT 5247
#define   DESTINATION_CONTROL_PORT 5246
#define   IPV4_TTL 10
#define   KEEP_ALIVE_COUNT 10
#define   IPV4_WLC_IP_ADDRESS 0x0A000001
#define   TMR 60
#define   IPV4_IP_ADDRESS 0xe0000103
#define   IPV4_IP_ADD     0xe0000104
#define   IPV4_PMTU       0x7ff - 0xc
#define   FRAME_ADDRESS   0x00
#define   WTP_MAC         2
#define   MAC_LENGTH         16
#define   CAPWAP_MSG_ELE_LEN 13
#define   CAPWAP_MSG_OFFSET 13
#define   CAPWAP_CMN_MSG_ELM_HEAD_LEN 3
#define   AC_INFO_AFTER_DISC  5
#define   RADIO_ADMIN_LEN  2
#define   WTP_REBBOT_STATS_LEN  15
#define   HEX_COUNT  0xFF
#define   HEX_VAL    0x08
#define   VAL_HEX    0x80
#define   MSG_LEN  19
#define   REPORT_INTERVAL  60
#define   DECRYP_ERR_REPORT_LEN  3
#define   DECRYP_MSG_LEN  7
#define   RADIO_CONFI_MSG_LEN  12
#define   RADIO_MAC_LEN  6
#define   RADIO_COUNTRY_LEN  3
#define   RADIO_OPER_RATE_LEN  8
#define   RADIO_TX_POWER_LEN  18
#define   KEEP_ALIVE_BYTE  3
#define   CAPWAP_HEADER_LEN 8
#define   CAPWAP_MSGTYPE_LEN 4
#define   CAPWAP_BUF_LEN  200
#define   DATA_KEEPALIVE 4
#define   RECIVED_BUF 6
#define   JOIN_SESSION_LEN 4
#define   FREE_INDEX  10
#define   AC_REF_COUNT 4
#define   STR_LEN  2
#define   Q_DEPTH  30
#define   CAPWAP_Q_DEPTH  100
#define   SKIP_BYTE 2
#define   BUF_MSG_TYPE 4
#define   BUF_VAL 3
#define   VEND_DISC_TYPE  100
#define   VEND_DISC_LEN   9
#define   VEND_DISC_ID    1111
#define   VEND_DOMAIN_LEN  32
#define   VEND_DOMAIN_NAME_LEN  40
#define   VEND_DNS_LEN  36
#define   VEND_NATIVE_LEN  10
#define   VEND_SPEC_NATIVE_LEN  6
#define   VEND_PLD  5
#define   IP_MTU 14
#define   IP_UDP_HEADER_LEN 28
#define   ISSNVRAM_SLOT_NUMBER_LEN 2
#define   DISCOVERY_TYPE_MESSAGE_LEN 13

#define MAX_AC_DISCOVERY_SUPPORTED             10
#define MAX_WTP_SUPPORTED                      NUM_OF_AP_SUPPORTED
#define MAX_CAPWAP_MSG_ELEM_INDEX    200
#define CAPWAP_MAX_DATA_LENGTH                          1024
#define CAPWAP_MAX_WLC_DESCRIPTOR_TYPE           2
#define MAX_AC_IP_LIST_SIZE                           1024
#define CAPWAP_WLC_NAME_SIZE                            512
#define  CAPWAP_VLAN_NAME_LEN                    512
#define CAPWAP_MAX_IMAGEID_DATA_LEN       1024
#define CAPWAP_IMAGE_CHECKSUM_SIZE               1024
#define CAPWAP_LOCATION_DATA_SIZE                       1024
#define CAPWAP_JOIN_SESSIONID_SIZE                      4
#define MAX_WTP_BOARD_INFO                              5
#define CAPWAP_MAX_WTP_DESCRIPTOR_TYPE                  4
#define CAPWAP_WTP_NAME_SIZE                            512
#define CAPWAP_MAX_PADDING_SIZE                         1024 /* TBD */
#define MAX_RETRANSMIT_INTERVAL_TIMEOUT          3
#define PRIMARY_DISC_INTERVAL_TIMEOUT            60
#define CAPWAP_MAX_PKT_LEN             2000
#define CAPWAP_MTU_SIZE                1500
#define CAPWAP_MAX_DATA_FRAG_INSTANCE  10
#define MAX_RADIOS_SUPPORTED           31
#define CAPWAP_TUNNEL_TYPE_WLAN        0
#define CAPWAP_TUNNEL_TYPE_ROAMING     1
#define CAPWAP_NP_MAX_STATION          100
#define MAX_CONFIG_STAT_VEND_MSG       5
#define CAPWAP_IEEE_INFO_ELEMENT       1029
#define VENDOR_SPECIFIC_PAYLOAD        37
#define VENDOR_ROUGE_AP_MSG_LEN        27

#define  ADD_STATION                     8
#define  DELETE_STATION                 18
#define IEEE_STATION                    1036
#define IEEE_STATION_SESSION_KEY        1038
#define CAPWAP_MAX_SEQ_NUM              255
#define CAPWAP_MIN_SEQ_NUM              0

#define CAPWAP_WTP_NAME_MSG_TYPE        45
#define CAPWAP_WTP_LOCATION_MSG_TYPE    28
#define CAPWAP_NO_AP_PRESENT            2
#define CAPWAP_RESPONSE_TIMEOUT         3
#define CAPWAP_ENABLE 1
#define CAPWAP_DISABLE 2

#define CAPWAP_WTP_NAME_MSG_TYPE            45
#define CAPWAP_WTP_LOCATION_MSG_TYPE        28
#define CAPWAP_AC_NAME_WITH_PRIORITTY_MSG_TYPE   5
#define CAPWAP_AC_TIMESTAMP_MSG_TYPE             6
#define CAPWAP_ADD_MAC_ACL_MSG_TYPE              7
#define CAPWAP_TIMERS_MSG_TYPE                   12
#define CAPWAP_DECRYPTION_ERROR_REPORT_MSG_TYPE  16
#define CAPWAP_DEL_MAC_ACL_MSG_TYPE              17
#define CAPWAP_IDLE_TIMEOUT_MSG_TYPE             23
#define CAPWAP_RADIO_ADMIN_STATE_MSG_TYPE        31
#define CAPWAP_STATS_TIMER_MSG_TYPE              36
#define CAPWAP_WTP_FALLBACK_MSG_TYPE             40
#define CAPWAP_WTP_STATIC_IP_INFO_MSG_TYPE       49
#define CAPWAP_IMAGE_IDENTIFIER_MSG_TYPE         25
#define CAPWAP_VEND_SPECIFIC_PAYLOAD_MSG_TYPE    37
#define CAPWAP_IEEE_MSG_TYPE_BASE                3398886

#define CAPWAP_AC_TIMESTAMP_MSG_LEN         4

#define CAPWAP_MSG_ELEM_PLUS_SEQ_LEN        3
#define CAPWAP_CMN_HDR_LEN                  13
#define CAPWAP_CONFIG_RESP_OFFSET           3
#define CAPWAP_WEB_AUTH_COMPLETE_MSG_LEN    7

#define CAP_SHIFT_BIT8 8

/* Information Element - Optional*/
/* 802.11n */
#define DOT11_INFO_ELEM_HTCAPABILITY 45
#define DOT11_INFO_HT_CAP_MSG_LEN 26
#define DOT11_INFO_ELEM_HTOPERATION 61
#define DOT11_INFO_HT_OPER_MSG_LEN  22
#define ENABLE 1
#define DISABLE 0
/* HT 20/40 */
#define DOT11_HT_SCAN_BEACON_UPDATE 200
/* 802.11AC */
#define DOT11AC_INFO_ELEM_VHTCAPABILITY 191
#define DOT11AC_INFO_VHT_CAP_MSG_LEN 12
#define DOT11AC_VHT_CAP_MCS_LEN    8 
#define DOT11AC_MCS_MAP            2
#define DOT11AC_TX_MCS_PART1       4
#define DOT11AC_TX_MCS_PART2       5

#define DOT11AC_INFO_ELEM_VHTOPERATION 192
#define DOT11AC_INFO_VHT_OPER_MSG_LEN 5
#define DOT11AC_VHT_OPER_INFO_LEN 3 
#if defined(WPS_WANTED) || defined(WPS_WTP_WANTED)
#define WPS_VENDOR_ELEMENT_ID 193
#endif
#define MAX_MULTIDOMAIN_INFO_ELEMENT 15
/* 802.11N */
#define DOT11N_HT_OPER_INFO_LEN 3 
#define DOT11N_HT_CAP_MCS_LEN    16 
#define DOT11N_HTOPE_INFO        5
#define DOT11N_BASIC_MCS_SET     16

/* Reset response - Mandatory */
#define  RESULT_CODE_RESET_RESP_INDEX 0
#define  IMAGE_IDENTIFIER_RESET_RESP_INDEX 1


/* CAPWAP multicast Group addresses */
#define CAPWAP_MULTICAST_ADDR           0xE000018C  /* 224.0.1.140 */       
#define CAPWAP_BROADCAST_ADDR           0xFFFFFFFF  /* 255.255.255.255 */ 

/* Clear Config response - Mandatory */
#define  RESULT_CODE_CLEAR_CONFIG_RESP_INDEX 0
#define  VENDOR_SPECIFIC_PAYLOAD_CLEAR_CONFIG_RESP_INDEX 1


/* Config Update request - mandatory message elements */
#define RESULT_CODE_CONF_UPDATE_RESP_INDEX 0
#define RADIO_OPER_STATE_CONF_UPDATE_RESP_INDEX 1
#define VENDOR_SPECIFIC_PAYLOAD_CONF_UPDATE_RESP_INDEX 2

/* Reset response - Mandatory */
#define  RESULT_CODE_RESET_RESP_INDEX 0
#define  IMAGE_IDENTIFIER_RESET_RESP_INDEX 1


/* Clear Config response - Mandatory */
#define  RESULT_CODE_CLEAR_CONFIG_RESP_INDEX 0
#define  VENDOR_SPECIFIC_PAYLOAD_CLEAR_CONFIG_RESP_INDEX 1

/* WTP Event response - Mandatory */
#define VENDOR_SPECIFIC_WTP_EVENT_RESP_INDEX 0


/* WTP Event response - Mandatory */
#define VENDOR_SPECIFIC_WTP_EVENT_RESP_INDEX 0


/* Data request - Mandatory */
#define  VENDOR_SPECIFIC_PAYLOAD_DATA_REQ_INDEX 1
#define  DATA_TRANSFER_MODE_REQ_INDEX 0
#define  DATA_TRANSFER_DATA_REQ_INDEX 0
#define  DATA_TRANSFER_MAX_DATA_SIZE    5900
#define  DATA_TRANSFER_MAX_BUF_LEN      6000
/* Data response - Mandatory */
#define  VENDOR_SPECIFIC_PAYLOAD_DATA_RESP_INDEX 1
#define  RESULT_CODE_DATA_RESP_INDEX 0

#define AC_DISCTYPE_DHCP_V4             138 

/* Maximum number of AC fallback to be supported */
#define MAX_NUM_AC_FALLBACK          3

#define MAX_AID_INDEX                   (MAX_NUM_OF_STA_PER_AP/32 ) + 1
#define MAX_AID_VALUE                   2007

/* CAPWAP Header Fields */
#define CAPWAP_HDR_RID_WID_FLAGS_SPLIT  0x1083
#define CAPWAP_HDR_RID_WID_FLAGS_LOCAL  0x2042
#define CAPWAP_HDR_FLWMK_FLAGS          0x10
#define CAPWAP_HDR_NULL_WIRELESS_INFO   0x00
#define CAPWAP_HDR_MIN_LEN              8
#define CAPWAP_HDR_RADIO_MAC_LEN        1
#define CAPWAP_HDR_RADIO_MAC            6
#define CAPWAP_HDR_WIRELESS_INFO_LEN    1

/* QoS */
#define CAPWAP_CTRL_PKT_DSCP_CODE   0xC0

#define VENDOR_ID 1111
#define CAPWAP_VENDOR_ID_LEN    4
#define CAPWAP_VENDOR_ELEM_LEN  2
#define CAPWAP_VENDOR_BSS_COUNT 1
#define CAPWAP_11N_MCS_RATE_LEN 16

/* Statistics */
#define CAPWAP_WTP_RADIO_STATS_MSG_TYPE  47
#define CAPWAP_DOT11_RADIO_STATS_MSG_TYPE 1039
#define CAPWAP_WTP_REBOOT_STATS_MSG_TYPE 48

#define CAPWAP_WTP_RADIO_STATS_MSG_LEN  20
#define CAPWAP_DOT11_RADIO_STATS_MSG_LEN 80
#define CAPWAP_WTP_REBOOT_STATS_MSG_LEN 15
#define CAPWAP_STATS_MSG_LEN    28
#define CAPWAP_MAC_STATS_MSG_LEN    20
#define AP_STATS_MSG_LEN      25
#define RADIO_CLIENT_STATS_LEN    23
#define CAPWAP_STATS_TMR_INTVL      20

#define CAPWAP_MAX_MSG_ELEMS            26

#define CAPWAP_MSG_ELEM_OPTIONAL        0
#define CAPWAP_MSG_ELEM_MANDATORY       1
#define CAPWAP_MSG_PRESENCE_TYPE        2
#define CAPWAP_MSG_ELEM_NOT_PRESENT     3

#define CAPWAP_WEP_40_KEY_LEN           5
#define CAPWAP_WEP_104_KEY_LEN          13
#define CAPWAP_RSNA_CCMP_KEY_LEN        16
#define CAPWAP_RSNA_TKIP_KEY_LEN        32
#define CAPWAP_RSN_IE_LEN               20
#define PM_STATS_CLEAR_SET  OSIX_TRUE
#define PM_STATS_CLEAR_RESET  OSIX_FALSE
#define NUM_BYTES   1024

#define MAX_FILTERS     10
#define AP_FWL_MAX_FILTER_SET_LEN      252
#define AP_FWL_MAX_RULE_NAME_LEN       36
#define AP_FWL_MAX_FILTER_NAME_LEN     36
#define AP_FWL_MAX_ACL_NAME_LEN        36
#define AP_FWL_MAX_ADDR_LEN            85
#define AP_FWL_MAX_PORT_LEN            12
#define AP_FWL_MIN_PORT_LEN            1

#define CAPWAP_BR_INTERFACE            "br-lan"

typedef enum{
    OPERATION_NO_CHECK,
    OPERATION_LESS_THAN,
    OPERATION_NOT_EQUALTO,
    OPERATION_BETWEEN
}eCapwapOperType;

typedef enum{
    DISCOVERY_TYPE_VENDOR_MSG = 100,
    DOT11N_VENDOR_MSG,
    DOMAIN_NAME_VENDOR_MSG,
    NATIVE_VLAN_VENDOR_MSG,
    MGMT_SSID_VENDOR_MSG,
#ifdef RFMGMT_WANTED
    NEIGH_CONFIG_VENDOR_MSG,
    CLIENT_CONFIG_VENDOR_MSG,
    CH_SWITCH_STATUS_VENDOR_MSG,
    NEIGHBOR_AP_MSG,
    NEIGHBOR_AP_RADIO2_MSG,
    CLIENT_SCAN_MSG,
#endif
    LOCAL_ROUTING_VENDOR_MSG,
    MAC_WHITE_VENDOR_MSG,
    UDP_SERVER_PORT_VENDOR_MSG,
    CONTROL_POLICY_DTLS_VENDOR_MSG,
    SSID_ISOLATION_VENDOR_MSG,
    SSID_RATE_VENDOR_MSG,
    BSSID_STATS_TYPE,
    RADIO_CLIENT_STATS,
    DOT11N_STA_VENDOR_MSG,
    WEBAUTH_COMPLETE_MSG,
    VENDOR_USERROLE_MSG,
    VENDOR_WMM_MSG,
    VENDOR_MULTIDOMAIN_INFO_MSG,

#ifdef RFMGMT_WANTED
    VENDOR_CH_SCANNED_LIST_MSG,
    VENDOR_SPECT_MGMT_TPC_MSG,
    VENDOR_SPECTRUM_MGMT_DFS_MSG,
#endif
    VENDOR_DFS_CHANNEL_MSG,
    VENDOR_DFS_RADAR_STATUS_MSG,
#ifdef WPS_WTP_WANTED
    VENDOR_WPS_PBC_MSG,
#endif    
    MULTICAST_VENDOR_MSG,   
    VENDOR_DIFF_SERV_MSG,
    WLAN_INTERFACE_IP,
    DHCP_POOL,
    DHCP_RELAY,
    FIREWALL_STATUS,
    NAT_STATUS,
    ROUTE_TABLE,
    FIREWALL_FILTER_TABLE,
    FIREWALL_ACL_TABLE,
    VENDOR_CAPW_DIFF_SERV_MSG,
#ifdef PMF_WANTED
    VENDOR_PMF_MSG,
#endif
#ifdef RFMGMT_WANTED
#ifdef ROGUEAP_WANTED
 ROGUE_CONFIG_VENDOR_MSG,
#endif 
#endif 
    L3SUBIF_TABLE,
    VENDOR_DOT11N_CONFIG_MSG,
    VENDOR_BANDSELECT_MSG,
    VENDOR_WLAN_VLAN_MSG,
    VENDOR_WPA_MSG,
    VENDOR_MAX_MSG
}eVendorMsgType;
typedef enum {
    VENDOR_DISC_TYPE = 0,
    VENDOR_DOT11N_TYPE,
    VENDOR_DOMAIN_NAME_TYPE,
    VENDOR_NATIVE_VLAN,
    VENDOR_MGMT_SSID_TYPE,
#ifdef RFMGMT_WANTED
    VENDOR_NEIGH_CONFIG_TYPE,
    VENDOR_CLIENT_CONFIG_TYPE,
    VENDOR_CH_SWITCH_STATUS_TYPE,
    VENDOR_NEIGHBOR_AP_TYPE,
    VENDOR_NEIGHBOR_RADIO2_AP_TYPE,
    VENDOR_CLIENT_SCAN_TYPE,
#endif
    VENDOR_LOCAL_ROUTING_TYPE,
    VENDOR_MAC_WHITE_LIST_TYPE,
    VENDOR_UDP_SERVER_PORT_MSG,
    VENDOR_CONTROL_POLICY_DTLS_MSG,
    VENDOR_SSID_ISOLATION_MSG,
    VENDOR_SSID_RATE_MSG,
    VENDOR_BSSID_STATS_TYPE,
    VENDOR_RADIO_CLIENT_STATS,
    VENDOR_DOT11N_STA_TYPE,
    VENDOR_WEBAUTH_COMPLETE_MSG,
    VENDOR_USERROLE_TYPE,
    VENDOR_WMM_TYPE,
    VENDOR_MULTIDOMAIN_INFO_TYPE,
#ifdef RFMGMT_WANTED
    VENDOR_CH_SCANNED_LIST,
    VENDOR_SPECT_MGMT_TPC_TYPE,
    VENDOR_SPECTRUM_MGMT_DFS_TYPE,
#endif
    VENDOR_DFS_CHANNEL_STATS,
    VENDOR_DFS_RADAR_STATS,
#if defined(WPS_WTP_WANTED) || defined(WPS_WANTED)    
    VENDOR_WPS_TYPE,
#endif    
    VENDOR_MULTICAST_MSG,   
    VENDOR_DIFF_SERV_TYPE,
    VENDOR_WLAN_INTERFACE_IP,
    VENDOR_DHCP_POOL,
    VENDOR_DHCP_RELAY,
    VENDOR_FIREWALL_STATUS,
    VENDOR_NAT_STATUS,
    VENDOR_ROUTE_TABLE,
    VENDOR_FIREWALL_FILTER_TABLE,
    VENDOR_FIREWALL_ACL_TABLE,
    VENDOR_CAPW_DIFF_SERV_TYPE,
#ifdef PMF_WANTED
    VENDOR_PMF_TYPE,
#endif
#ifdef RFMGMT_WANTED 
#ifdef ROGUEAP_WANTED
 VENDOR_ROUGE_AP,
#endif
#endif
    VENDOR_L3SUBIF_TABLE,
    VENDOR_DOT11N_CONFIG_TYPE,
    VENDOR_BANDSELECT_TYPE,
    VENDOR_WLAN_VLAN_TYPE,
    VENDOR_WPA_TYPE,
    VENDOR_MAX_TYPE
}eConfigStatVendortype;

#define    VENDOR_HEADER_LEGTH                    4
#define    VENDOR_DISC_TYPE_LENGTH                0
#define    VENDOR_DOT11N_TYPE_LENGTH              0

#define    VENDOR_DOMAIN_NAME_LENGTH              40
#define    DOMAIN_NAME_VEND_MSG_ELEM_LENGTH       32
#define    DNS_SERVER_VEND_MSG_ELEM_LENGTH         4



#define    VENDOR_NATIVE_VLAN_LENGTH              10
#define    NATIVE_VLAN_VEND_MSG_ELEM_LENGTH       6
#define    VENDOR_MGMT_SSID_TYPE_LENGTH           0
#define    VENDOR_MAC_WHITE_LIST_TYPE_LENGTH      0
#define    VENDOR_UDP_SERVER_PORT_MSG_LENGTH      0
#define    VENDOR_CONTROL_POLICY_DTLS_MSG_LENGTH  0
#define    VENDOR_SSID_ISOLATION_MSG_LENGTH       0
#define    VENDOR_SSID_RATE_MSG_LENGTH            0
#define    VENDOR_BSSID_STATS_TYPE_LENGTH         0
#define    VENDOR_LOCAL_ROUTING_TYPE_LENGTH       0

typedef enum {
    CAPWAP_DISC_INTERVAL_TMR =0, /* Valid only for WTP */
    CAPWAP_MAX_DISC_INTERVAL_TMR,    /* Valid only for WTP */
    CAPWAP_SILENT_INTERVAL_TMR,
    CAPWAP_PRIMARY_DISC_INTERVAL_TMR,
    CAPWAP_DHCP_OFFER_RCV_INTERVAL_TMR,
    CAPWAP_MAX_DISC_TMR,

/* constants for State Machine timer types */
    CAPWAP_WAITDTLS_TMR = CAPWAP_MAX_DISC_TMR+1,
    CAPWAP_WAITJOIN_TMR,
    CAPWAP_CHANGESTATE_PENDING_TMR,
    CAPWAP_DATACHECK_TMR,
    CAPWAP_REASSEMBLE_TMR,
    CAPWAP_IMAGEDATA_START_TMR,
    CAPWAP_CPU_RELINQUISH_TMR,
    CAPWAP_CPU_RECV_RELINQUISH_TMR,
    CAPWAP_CPU_DISC_RELINQUISH_TMR,
    CAPWAP_MAX_FSM_TMR,

/* constants for Run State timer types */
    CAPWAP_RETRANSMIT_INTERVAL_TMR = CAPWAP_MAX_FSM_TMR +1,
    CAPWAP_DATACHNL_KEEPALIVE_TMR,
    CAPWAP_DATACHNL_DEADINTERVAL_TMR,
    CAPWAP_ECHO_INTERVAL_TMR,
    CAPWAP_PMTU_UPDATE_TMR,
    CAPWAP_MAX_RUN_TMR
}eCapwapTmrId;

#if 0
/* cpu relinquish timers */
typedef enum{
    CAPWAP_CPU_RELINQUISH_RX_TMR = CAPWAP_MAX_RUN_TMR + 1,
    CAPWAP_CPU_RELINQUISH_SER_TMR,
    CAPWAP_CPU_RELINQUISH_APHDLR_TMR,
    CAPWAP_CPU_RELINQUISH_WLCHDLR_TMR
}eCapwapCPuRelinquishTmrId;
#endif

typedef enum {
    CAPWAP_CPU_RELINQUISH_RX_TMR = 0,
    CAPWAP_CPU_RELINQUISH_DISC_TMR,
}eCapwapCPuRelinquishTmrId;

/* Enum for CAPWAP transport protocol */
typedef enum{
    CAPWAP_INIT = 0,
    CAPWAP_DISC_REQ = 1,
    CAPWAP_DISC_RSP = 2, 
    CAPWAP_JOIN_REQ =3,
    CAPWAP_JOIN_RSP =4,  
    CAPWAP_CONF_STAT_REQ = 5,
    CAPWAP_CONF_STAT_RSP=6, 
    CAPWAP_CONF_UPD_REQ=7,
    CAPWAP_CONF_UPD_RSP=8,
    CAPWAP_WTP_EVENT_REQ=9, 
    CAPWAP_WTP_EVENT_RSP=10,
    CAPWAP_CHANGE_STAT_REQ=11,
    CAPWAP_CHANGE_STAT_RSP=12,
    CAPWAP_ECHO_REQ=13,
    CAPWAP_ECHO_RSP=14,
    CAPWAP_IMAGE_DATA_REQ =15,
    CAPWAP_IMAGE_DATA_RSP=16, 
    CAPWAP_RESET_REQ=17, 
    CAPWAP_RESET_RSP=18, 
    CAPWAP_PRIMARY_DISC_REQ = 19,
    CAPWAP_PRIMARY_DISC_RSP = 20, 
    CAPWAP_DATA_TRANS_REQ = 21,
    CAPWAP_DATA_TRANS_RSP = 22, 
    CAPWAP_CLEAR_CONF_REQ = 23, 
    CAPWAP_CLEAR_CONF_RSP = 24, 
    CAPWAP_STATION_CONF_REQ = 25,
    CAPWAP_STATION_CONF_RSP = 26, 
    IEEE_WLAN_CONF_REQ = 27, 
    IEEE_WLAN_CONF_RSP = 28,
    CAPWAP_MAX_EVENTS=29
}eCapwapStateEvent;

typedef enum {
 CAPWAP_SUCCESS,    
 CAPWAP_FAILURE,    
 NAT_DETECTED_SUCCESS,  
 JOIN_FAILURE_UNSPECIFIED, 
 JOIN_FAILURE_RES_DEPLETION,  
 JOIN_FAILURE_UNKNOWN_SOURCE,
 JOIN_FAILURE_INCORRECT_DATA,
 JOIN_FAILURE_SESSIONID_INUSE,
 JOIN_FAILURE_HW_NOTSUPPORTED,
 JOIN_FAILURE_BINDING_NOT_SUPPORTED,
 RESET_FAIL_UNKNOWN,
 RESET_FAIL_WRITE_ERROR,
 CONFIG_FAIL_SERVICE_PROVIDED,
 CONFIG_FAIL_SERVICE_NOTPROVIDED,
 IMAGEDATA_FAIL_INVALID_CHECKSUM,
 IMAGEDATA_FAIL_INVALID_DATALEN,
 IMAGEDATA_FAIL_OTHER_ERROR,
 IMAGEDATA_ALREADY_PRESENT,
 UNEXPECTED_MESSAGE_REQUEST,
 UNRECOGNISED_MESSAGE_REQUEST,  
 MISSING_MANDATORY_MSG_ELEMENT, 
 UNRECOGNISED_MESSAGE_ELEMENT,  
    DATATRANSFER_NO_INFO_TO_TRANSFER,
 MAX_CAPWAP_RETURN_CODE
}eCapwapReturnCode;

#ifdef __CAPWAP_DISCOVERY_C__
CONST CHR1 *gcp1CapwapReturnCode [] = {
        "0  Success",
        "1  Failure (AC List Message Element MUST Be Present)",
        "2  Success (NAT Detected)",
        "3  Join Failure (Unspecified)",
        "4  Join Failure (Resource Depletion)",
        "5  Join Failure (Unknown Source)",
        "6  Join Failure (Incorrect Data)",
        "7  Join Failure (Session ID Already in Use)",
        "8  Join Failure (WTP Hardware Not Supported)",
        "9  Join Failure (Binding Not Supported)",
        "10 Reset Failure (Unable to Reset)",
        "11 Reset Failure (Firmware Write Error)",
        "12 Configuration Failure (Unable to Apply Requested Configuration - Service Provided Anyhow)",
        "13 Configuration Failure (Unable to Apply Requested Configuration - Service Not Provided)",
        "14 Image Data Error (Invalid Checksum)",
        "15 Image Data Error (Invalid Data Length)",
        "16 Image Data Error (Other Error)",
        "17 Image Data Error (Image Already Present)",
        "18 Message Unexpected (Invalid in Current State)",
        "19 Message Unexpected (Unrecognized Request)",
        "20 Failure - Missing Mandatory Message Element",
        "21 Failure - Unrecognized Message Element",
        "22 Data Transfer Error (No Information to Transfer)"
};
#else
extern CONST CHR1 *gcp1CapwapReturnCode[];
#endif



typedef enum {
    WTP_MODEL_NUMBER = 0,
    WTP_SERIAL_NUMBER,
    WTP_BOARD_ID,
    WTP_BOARD_RIVISION,
    WTP_BASE_MAC_ADDRESS
}eWtpBoardDataType;

/*WTP Descriptor*/
/* RSNA capabilities */
typedef enum {
    AUTH_CIPHER_CCMP,
    AUTH_CIPHER_TKIP,
    AUTH_CIPHER_TKIP_CCMP,
    AUTH_CIPHER_NONE
}eRsnaSupport;

typedef enum {
    WTP_HW_VERSION = 0,
    WTP_SW_VERSION ,
    WTP_BOOT_VERSION,
    WTP_STANDBY_SWVERSION,
    WLC_HW_VERSION,
    WLC_SW_VERSION, 
    UNKNOWN_VENDOR_INFO
}eVendorInfoType;

typedef struct {
    UINT1   u1Preamble;
    UINT1   u1Reserverd1;
    UINT2   u2Reserved2;
}tCapDtlsHdr;

/* Capwap header */
typedef struct{
    tMacAddr RadioMacAddr;
    UINT1  au1Pad[2];
    UINT4  u4WirelessRadioInfo;
    UINT2  u2FragId;            /* 16 bits fragment Id */
    UINT2  u2FragOffsetRes3;    /* 13 bits fragment offset */
    UINT2  u2HdrLen; /* Length of the CAPWAP header */
    UINT2  u2HlenRidWbidFlagT;  /* contains the 5 bits header length , 5 bits radio Id, 5 bits Wbid, Flags bits - T */
    UINT1  u1Preamble;          /* Capwap preamble-0 for payload type*/
    UINT1  u1FlagsFLWMKResd;    /* Flags F, L,W,M,K , last 3 bits reserved */
    UINT1  u1RadMacAddrLength; /* optional 32 bit radio MAC address field */
    UINT1  u1WirelessInfoLength; /* optional wireless info */
}tCapwapHdr;

/* Capwap Control Header */
typedef struct{
    UINT4  u4MsgType;          /* Control message type- request or response */
    UINT2  u2MsgElementLen;    /* length following the seq number */
    UINT1  u1SeqNum;           /* identifier to match the request and response messages */    
    UINT1  u1Flags;            /* must be set to zero */
}tCapwapCtrlHdr;


/* Capwap message element */
typedef struct {
    UINT2  u2MsgType; 
    UINT2  u2Length;
    UINT2  *pu2Offset;
    UINT2  numMsgElemInstance;
    UINT1  au1Pad[2];
}tCapwapMsgElement;

/* Capwap Control message */
typedef struct{
    tCapwapHdr          capwapHdr;      /* Capwap header */
    tCapwapCtrlHdr      capwapCtrlHdr;  /* Capwap control header */
    tCapwapMsgElement   capwapMsgElm[MAX_CAPWAP_MSG_ELEM_INDEX]; /* List of message elements in the CAPWAP control msg */
 UINT4    u4ReturnCode;
    UINT2               numOptionalElements; /* Number of optional mesg elements */
    UINT2               numMandatoryElements;
    UINT2               u2IntProfileId;
    UINT1               au1Pad[2]; 
}tCapwapControlPacket;

/* Capwap control message elements */

/* Type = 1, AC Descriptor */
typedef struct{
    UINT2    vendorInfoType;
    UINT2    u2VendorInfoLength;
    UINT1    vendorInfoData[CAPWAP_MAX_DATA_LENGTH];
}tVendorDescriptor;

typedef struct{
    tVendorDescriptor VendDesc[CAPWAP_MAX_WLC_DESCRIPTOR_TYPE];
    UINT4             u4VendorInfoCount;
    UINT4             ACVendorId;
    UINT2             u2MsgEleType; /* 1 */
    UINT2             u2MsgEleLen;  /* >=12 */
    UINT2             stations; /* No of stations serviced by AC currently */
    UINT2             stationLimit; /* Max no of stations serviced by AC */
    UINT2             activeWtp;  /* Currently attached wtps */
    UINT2             maxWtp;      /* Maximum WTPS */
    UINT1             security;   /* Auth credentials supported by AC.flagsS,X,R */
    UINT1             radioMacField;
    UINT1             resvd;
    UINT1             dtlsPolicy;
    BOOL1             isOptional;    /* This is used during assembling the packet */
    UINT1             au1Pad[3]; 
}tAcDescriptor;

/* AC IPv4 List, Type =  2 or 3*/
typedef struct{
    tIpAddr ipAddr[MAX_AC_IP_LIST_SIZE]; /* the latest list of AC.s IP available for the */
    UINT2   u2MsgEleType; /* 2 */
    UINT2   u2MsgEleLen;  /* >=4 */
    UINT2   u2Afi;   /* Represents address family */
    BOOL1   isOptional;    /* This is used during assembling the packet */
    UINT1   au1Pad[1]; 
}tACIplist;

/* AC Name , Type =  4 */
typedef struct{
    UINT2 u2MsgEleType; /* 4 */
    UINT2 u2MsgEleLen;  /* >=1 */
    UINT1 wlcName[CAPWAP_WLC_NAME_SIZE];
    BOOL1 isOptional;    /* This is used during assembling the packet */
    UINT1 au1Pad[3];
}tACName;

/* AC Name with Priority, Type =  5 */
typedef struct{
    UINT2 u2MsgEleType; /* 5 */
    UINT2 u2MsgEleLen;  /* >=2 */
    UINT1 u1Priority;
    UINT1 wlcName[CAPWAP_WLC_NAME_SIZE];
    BOOL1 isOptional;    /* This is used during assembling the packet */
    UINT1 au1Pad[2];
}tACNameWithPrio;

/* AC Timestamp, Type =  6*/
/* AC Time Stamp */
typedef struct{
    UINT4 u4Timestamp;
    UINT2 u2MsgEleType; /* message element type - 6*/
    UINT2 u2MsgEleLen; /* message element length - 4 */
    BOOL1 isOptional;    /* This is used during assembling the packet */
    UINT1 au1Pad[3]; 
}tACtimestamp;

/* Add MAC ACL Entry, Type = 7*/
typedef struct{
    UINT2 u2MsgEleType; /* message element type - 7*/
    UINT2 u2MsgEleLen; /* message element length - >=8 */
    UINT2 u2MacAddr;
    UINT1 u1NumEntries;
    UINT1 u1Length;
    BOOL1 isOptional;    /* This is used during assembling the packet */
    UINT1 au1Pad[3];
}tAddMacAclEntry;

/* Add Station, Type = 8 */
typedef struct{
    UINT2 u2MsgEleType; /* message element type - 8*/
    UINT2 u2MsgEleLen; /* message element length - >=8 */
    UINT2 u2LenMacAddr; /* Length of the Mac Address Field */
    UINT2 u2RadMacAddr; /* stations MAC Address */
    UINT1 u2Name[CAPWAP_VLAN_NAME_LEN];
    UINT1 u1RadioId; /* radioId */
    BOOL1 isOptional;    /* This is used during assembling the packet */
    UINT1 au1Pad[2];
}tAddstation;

/* CAPWAP Control IPV4 Address , Type = 10 */
/* CAPWAP Control IPV6 Address, Type = 11 */
typedef struct{
    tIpAddr  ipAddr; /* the IP address of the sender */
    UINT2    u2MsgEleType; /* 30 */
    UINT2    u2MsgEleLen;  /* 4 */
    UINT2    wtpCount;
    BOOL1    isOptional;    /* This is used during assembling the packet */
    UINT1    au1Pad[1];
}tCtrlIpAddr;

/* CAPWAP Timers , Type = 12 */
typedef struct{
     UINT2 u2MsgEleType; /* message element type - 12*/
     UINT2 u2MsgEleLen; /* message element length - 2 */
     UINT1 u1Discovery;
     UINT1 u1EchoRequest;
     BOOL1 isOptional;    /* This is used during assembling the packet */
     UINT1 au1Pad[1];
}tCapwapTimer;

/* Data Transfer Data, Type = 13 */
typedef struct{
    UINT2 u2MsgEleType; /* message element type - 16*/
    UINT2 u2MsgEleLen; /* message element length - 3 */
    UINT1 DataType;
    UINT1 DataMode;
    UINT2 DataLength;
    UINT1 Data[DATA_TRANSFER_MAX_DATA_SIZE];
    BOOL1 isPresent;    /* This is used during assembling the packet */
    UINT1 au1Pad[3];
}tDataTransferData;

/* Data Transfer Mode, Type = 14 */
typedef struct{
    UINT2 u2MsgEleType; /* message element type - 16*/
    UINT2 u2MsgEleLen; /* message element length - 3 */
    UINT1 DataMode;
    BOOL1 isPresent;    /* This is used during assembling the packet */
    UINT1 au1Pad[2];
}tDataTransferMode;

typedef enum
{
    DATA_TRANS_MODE_RESERVED = 0,
    DATA_TRANS_MODE_WTP_CRASH,
    DATA_TRANS_MODE_WTP_MEMORY
}eDataTransferMode;

typedef enum
{
    DATA_TRANS_DATA = 1,
    DATA_TRANS_EOF = 2,
    DATA_TRANS_ERROR = 5
}eDataTransferDataType;

typedef enum
{
    CRASHFILE_GET = 1,
    CRASHFILE_DELETE,
    NO_CRASHMEMORY
}eDataTransferAction;

/* Decryption Error Report, Type =  15*/
typedef struct{
    UINT2 u2MsgEleType; /* message element type - 16*/
    UINT2 u2MsgEleLen; /* message element length - 3 */
    UINT2 u2NumEntries; /* Number of entries */
    UINT2 u2LenMacAddr; /* Length of the Mac Address Field */
    UINT2 u2RadMacAddr; /* stations MAC Address */
    UINT1 u1RadioId;   /* radioId */
    BOOL1 isOptional;    /* This is used during assembling the packet */
}tDecryptErrReport;

/* Decryption Error Report Period, Type = 16 */
typedef struct{
    UINT2 u2MsgEleType; /* message element type - 16*/
    UINT2 u2MsgEleLen; /* message element length - 3 */
    UINT2 u2ReportInterval; /* report Interval */
    UINT1 u1RadioId; /* radioId */
    BOOL1 isOptional;    /* This is used during assembling the packet */
}tDecryptErrReportPeriod;

/* Delete MAC ACL Entry, Type = 17 */
typedef struct{
    UINT2 u2MsgEleType; /* message element type - 17*/
    UINT2 u2MsgEleLen; /* message element length - >=8 */
    UINT2 u2MacAddr;
    UINT1 u1NumEntries;
    UINT1 u1Length;
    BOOL1 isOptional;    /* This is used during assembling the packet */
    UINT1 au1Pad[3];
}tDeleteMacAclEntry;

/* Delete Station, Type =  18 */
typedef struct{
    UINT2 u2MsgEleType; /* message element type - 18*/
    UINT2 u2MsgEleLen; /* message element length - >=8 */
    UINT2 u2LenMacAddr; /* Length of the Mac Address Field */
    UINT2 u2RadMacAddr; /* stations MAC Address */
    UINT1 u1RadioId; /* radioId */
    BOOL1 isOptional;    /* This is used during assembling the packet */
    UINT1 au1Pad[2];
}tDeletestation;   

/* Discovery Type, Type = 20 */
typedef struct{
    UINT2 u2MsgEleType; /* 20 */
    UINT2 u2MsgEleLen;  /* 1 */
    UINT1 u1DiscType;  /* Value field */
    BOOL1 isOptional;    /* This is used during assembling the packet */
    UINT1 au1Pad[2];
}tWtpDiscType;

/* Duplicate IPv4 Address, Type =  21 */
typedef struct{
    UINT4 u4IpAddress;   /* The IP address currently used by the WTP */
    UINT2 u2MsgEleType; /* 21 */
    UINT2 u2MsgEleLen;  /* 12 */
    UINT2 u2MacAddr; /* MAC Address */
    UINT1 u1Status;     /* status of the duplicate IP address */
    UINT1 u1LenMacAddr; /* Length of the Mac Address Field */
    BOOL1 isOptional;    /* This is used during assembling the packet */
    UINT1 au1Pad[3];
}tDupIPV4Addr;

/* Duplicate IPv6 Address, Type =   22 */
typedef struct{
    tIpAddr  IpAddress;   /* The IP address currently used by the WTP */
    UINT2    u2MsgEleType; /* 22 */
    UINT2    u2MsgEleLen;  /* 24 */
    UINT2    u2MacAddr; /* MAC Address */
    UINT1    u1Status;     /* status of the duplicate IP address */
    UINT1    u1LenMacAddr; /* Length of the Mac Address Field */
    BOOL1    isOptional;    /* This is used during assembling the packet */
    UINT1    au1Pad[3];
}tDupIPV6Addr;

/* Idle Timeout, Type = 23 */
typedef struct{
    UINT4 u4Timeout; /* timeout value */
    UINT2 u2MsgEleType; /* message element type - 23*/
    UINT2 u2MsgEleLen; /* message element length - 4 */
    BOOL1 isOptional;    /* This is used during assembling the packet */
    UINT1 au1Pad[3]; 
}tIdleTimeout;

/* Image Data, Type = 24 */
typedef struct {
    UINT2 u2MsgEleType; 
    UINT2 u2MsgEleLen; /* >=1 */
    UINT1 u1DataType;
 UINT1 au1ImageData[1024];
 BOOL1 isOptional;
    UINT1 au1Pad[2];
}tImageData;

/* Image Identifier, Type = 25 */
typedef struct{
    UINT4 u4VendorId; /* 32 .bit vendor id */
    UINT2 u2MsgEleType; /* 25 */
    UINT2 u2MsgEleLen;  /* >=5 */
    UINT1 data[CAPWAP_MAX_IMAGEID_DATA_LEN]; /* UTF-8 encoded string containing the firmware id */
    BOOL1 isOptional;    /* This is used during assembling the packet */
    UINT1 au1Pad[3]; 
}tImageId;


/* Image Information, Type = 26 */
typedef struct {
    UINT1 Checksum[16]; /* 16 Octage MD5 Check sum*/
    UINT4 u4FileSize;
    UINT2 u2MsgEleType; 
    UINT2 u2MsgEleLen; /*20*/
    BOOL1 isOptional;
    UINT1 au1Pad[3];
}tImageInfo;

/* Initiate Download, Type = 27 */
typedef struct {
    UINT2 u2MsgEleType; /* 27 */
    UINT2 u2MsgEleLen;  /* 0 */
 BOOL1 isOptional;
    UINT1 au1Pad[3];
}tImageIntiateDownload;

/* Location Data, Type = 28 */
typedef struct{
    UINT2 u2MsgEleType; /* 28 */
    UINT2 u2MsgEleLen;  /* >=1 */
    UINT1 value[CAPWAP_LOCATION_DATA_SIZE]; /* non-zero terminated string */
    BOOL1 isOptional;    /* This is used during assembling the packet */
    UINT1 au1Pad[3];
}tLocationData;

/* Maximum Message Length, Type = 29*/
typedef struct{
    UINT2    u2MsgEleType; /* 29 */
    UINT2    u2MsgEleLen;  /* 2 */
    UINT2    u2MaxMsglen; /* max capwap mesg that the WTP supports to the AC */
    BOOL1    isOptional;    /* This is used during assembling the packet */
    UINT1 au1Pad[1];
}tMaxMsgLen;

/* CAPWAP Local IPV4 Address, Type = 30 */
/* CAPWAP Local IPV6 Address, Type = 50 */
typedef struct{
    tIpAddr  ipAddr; /* the IP address of the sender */
    UINT2    u2MsgEleType; /* 30 */
    UINT2    u2MsgEleLen;  /* 4 */
    BOOL1    isOptional;    /* This is used during assembling the packet */
    UINT1 au1Pad[3];
}tLocalIpAddr;

/*  Radio Administrative State, Type = 31 */
#if 0
/typedef struct{
     UINT2 u2MsgEleType; /* message element type - 31 */
     UINT2 u2MsgEleLen; /* message element length - 2 */
     UINT1 radioId;
     UINT1 adminState;
     BOOL1 isOptional;    /* This is used during assembling the packet */
}tRadioAdminState;

/*  Radio Operational State, Type = 32 */
typedef struct{
     UINT2 u2MsgEleType; /* message element type - 31 */
     UINT2 u2MsgEleLen; /* message element length - 2 */
     UINT1 u1NumRadios; /* Number of radio's info contained */
     UINT1 u1RadId;
     UINT1 u1State;
     UINT1 u1Cause;
     BOOL1 isOptional;    /* This is used during assembling the packet */
}tRadioOperState;
#endif 

/*  Result Code, Type = 33 */
typedef struct{
    UINT4 u4Value; /* Result code */
    UINT2 u2MsgEleType; /* 33 */
    UINT2 u2MsgEleLen;  /* 4 */
    BOOL1 isOptional;    /* This is used during assembling the packet */
    UINT1 au1Pad[3];    
}tResCode;

/*  Returned Message Element, Type = 34 */
typedef struct{
    UINT2 u2MsgEleType; /* 33 */
    UINT2 u2MsgEleLen;  /* 4 */
    UINT1 *msgElem;
    UINT1 u1Reason; /* Result code */
    UINT1 u1Length;
    BOOL1 isOptional;    /* This is used during assembling the packet */
    UINT1 au1Pad[1];
}tReturnMsgElems;

/*  Session ID, Type = 35 */
typedef struct{
    UINT4 CheckSum[CAPWAP_JOIN_SESSIONID_SIZE]; /* 16-Octate MD5 Check sum */
    UINT2 u2MsgEleType; /* 35 */
    UINT2 u2MsgEleLen;  /* 16 */
    BOOL1 isOptional;    /* This is used during assembling the packet */
    UINT1 au1Pad[3];
}tSessid;

/*  Statistics Timer, Type = 36 */
typedef struct{
 UINT2 u2MsgEleType; /* message element type - 36*/
 UINT2 u2MsgEleLen; /* message element length - 2 */
 UINT2 u2StatsTimer;
 BOOL1 isOptional;  /* This is used during assembling the packet */
    UINT1 au1Pad[1];
}tStatisticsTimer;


typedef struct{
    UINT2    u2MsgEleType;
    UINT2    u2MsgEleLen;
    UINT4    u4WlcStaticIpAddress;
    UINT4    vendorId;
    UINT4    u4WlcIpAddress;
    UINT1    u1Val;
    BOOL1    isOptional;
    UINT1    au1Pad[2];
}tVendorDiscoveryType;


typedef struct{
    UINT2    u2MsgEleType;
    UINT2    u2MsgEleLen;
    UINT4    vendorId;
    UINT4    u4Val;
    BOOL1    isOptional;
    UINT1    au1Pad[3];
}tVendorUdpServerPort;

typedef struct{
    UINT2    u2MsgEleType;
    UINT2    u2MsgEleLen;
    UINT4    vendorId;
    UINT1    u1Val;
    BOOL1    isOptional;
    UINT1    au1Pad[2];
}tVendorControlDTLS;

typedef struct{
    UINT4    u4VendorId;
    UINT4    u4SuppTranBeamformCap;
    UINT2    u2MsgEleType;
    UINT2    u2MsgEleLen;
    UINT2    u2SuppExtCap;
    UINT1    u1RadioId;
    UINT1    u1HtFlag;
    UINT1    u1MaxSuppMCS;
    UINT1    u1MaxManMCS;
    UINT1    u1TxAntenna;
    UINT1    u1RxAntenna;
    UINT2    u2Reserved;
    BOOL1    isOptional;
    UINT1    u1Pad;
}tDot11nVendorType;

typedef struct {
   tMacAddr     stationMacAddr;
   UINT2        u2MsgEleType;
   UINT4        u4VendorId;
   UINT2        u2MsgEleLen;
   UINT2        u2HiSuppDataRate;
   UINT2        u2AMPDUBuffSize;
   UINT1        u1HtFlag;
   UINT1        u1MaxRxFactor;
   UINT1        u1MinStaSpacing;
   UINT1        u1HTCSupp;
   BOOL1        isOptional;
   UINT1        au1Pad[1];
   UINT1        au1ManMCSSet[16];
}tDot11nStaVendorType;

typedef struct{
    UINT4    vendorId;
    UINT2    u2MsgEleType;
    UINT2    u2MsgEleLen;
    UINT1    u1Val;
    UINT1    u1RadioId;
    UINT1    u1WlanId;
    BOOL1    isOptional;
}tVendorSsidIsolation;

#ifdef WLC_WANTED
typedef struct{
    UINT4    vendorId;
    UINT2    u2MsgEleType;
    UINT2    u2MsgEleLen;
    UINT2    u2WlanMulticastMode;
    UINT2    u2WlanSnoopTableLength;
    UINT4    u4WlanSnoopTimer;
    UINT4    u4WlanSnoopTimeout;
    UINT1    u1RadioId;
    UINT1    u1WlanId;
    BOOL1    isOptional;
    UINT1    au1Pad[1];
}tVendorMulticast;
#endif

typedef struct{
    UINT4    vendorId;
    UINT4    u4QosUpStreamCIR;
    UINT4    u4QosUpStreamCBS;
    UINT4    u4QosUpStreamEIR;
    UINT4    u4QosUpStreamEBS;
    UINT4    u4QosDownStreamCIR;
    UINT4    u4QosDownStreamCBS;
    UINT4    u4QosDownStreamEIR;
    UINT4    u4QosDownStreamEBS;
    UINT2    u2MsgEleType;
    UINT2    u2MsgEleLen;
    UINT1    u1PassengerTrustMode;
    UINT1    u1QosRateLimit;
    UINT1    u1RadioId;
    UINT1    u1WlanId;
    BOOL1    isOptional;
    UINT1    au1Pad[3];

}tVendorSsidRateLimit;

typedef struct{
    UINT2    u2MsgEleType;
    UINT2    u2MsgEleLen;
    UINT4    vendorId;
    UINT4    wlanBSSIDBeaconsSentCount;
    UINT4    wlanBSSIDProbeReqRcvdCount;
    UINT4    wlanBSSIDProbeRespSentCount;
    UINT4    wlanBSSIDDataPktRcvdCount;
    UINT4    wlanBSSIDDataPktSentCount;
    BOOL1    isOptional;
    UINT1    au1Pad[3];
}tVendorWlanBssIdStats;

typedef struct{
    UINT2    u2MsgEleType;
    UINT2    u2MsgEleLen;
    UINT4    vendorId;
    UINT1    au1DomainName[32];
    UINT1    au1ServerIp[4];
    BOOL1    isOptional;
    UINT1    au1Pad[3]; 
}tVendorDNS;

typedef struct{
    UINT4    vendorId;
    UINT2    u2MsgEleType;
    UINT2    u2MsgEleLen;
    UINT2    u2VlanId;
    BOOL1    isOptional;
    UINT1    au1Pad[1];
}tVendorNativeVlan;

typedef struct{
    UINT4    vendorId;
    UINT2    u2MsgEleType;
    UINT2    u2MsgEleLen;
    UINT1    au1ManagmentSSID[32];
    BOOL1    isOptional;
    UINT1    au1Pad[3];
}tVendorMgmtSsid;
typedef struct{
    UINT4    vendorId;
    UINT2    u2MsgEleType;
    UINT2    u2MsgEleLen;
    UINT1    u1WtpLocalRouting;
    BOOL1    isOptional;
    UINT1    au1Pad[2];
}tVendorApLocalRouting;
#ifdef WPS_WTP_WANTED
typedef struct{
    UINT4    u4vendorId;
    UINT4    u4RadioIfIndex;
    UINT1    au1RadioIfName[RADIO_NAME_MAX_LEN];
    UINT1    au1WlanIfname [WLAN_INDEX_MAX][WLAN_NAME_MAX_LEN]; 
    UINT2    u2WtpInternalId;
    UINT2    u2MsgEleType;
    UINT2    u2MsgEleLen;
    BOOL1    isOptional;
    UINT1    u1WlanId;
}tVendorWpsConfig;
#endif

#ifdef WLC_WANTED
typedef struct{
    UINT4    u4IpAddr;
    UINT4    u4SubMask;
  UINT4    u4vendorId;
 UINT2    u2MsgEleType;
 UINT2    u2MsgEleLen;
    UINT1    u1AdminStatus;
    BOOL1    isOptional;
    UINT1    au1Pad[2];
}tVendorWlanInterfaceIp;
#endif

typedef struct{
        UINT4    u4IpStartAddr;
        UINT4    u4IpEndAddr;
        UINT4    u4SubMask;
        UINT4    u4LeaseTime;
 UINT2    u2MsgEleType;
 UINT2    u2MsgEleLen;
        UINT1    u1SubnetPool;
        UINT1    u1AdminStatus;
        BOOL1    isOptional;
        UINT1    au1Pad;
}tVendorSubnetPool;

typedef struct{
     UINT1  u1FirewallStatus;
     BOOL1  isOptional;
     UINT1  au1Pad[2];
}tVendorFirewallStatus;

typedef struct{
     UINT1  u1FirewallStatus;
     UINT1  u1Interface;
     BOOL1  isOptional;
     UINT1  au1Pad;
}tVendorNatStatus;

typedef struct{
    UINT4    vendorId;
    UINT2    u2MsgEleType;
    UINT2    u2MsgEleLen;
    tMacAddr StationMac;
    UINT1    u1WebAuthCompleteStatus;
    BOOL1    isOptional;
}tVendorWebAuthStatus;

/* WLAN VLAN Information */
typedef struct{
    UINT4    vendorId;
    UINT2    u2MsgEleType;
    UINT2    u2MsgEleLen;
    UINT2    u2VlanId;
    UINT1    u1RadioId;
    UINT1    u1WlanId;
    BOOL1    isOptional;
    UINT1    au1Pad[3];
}tVendorWlanVlan;


typedef struct{
    UINT2    u2MsgEleLen;
    BOOL1    isOptional;
    UINT1    au1Pad[1];
}tVendorMultiDomainInfo;


/* HT Capabilities */
typedef struct {
    UINT4       u4TranBeamformCap;
    UINT2       u2HTCapInfo;
    UINT2       u2HTExtCap;
    UINT1       u1ElemId;
    UINT1       u1ElemLen;
    UINT1       u1AMPDUParam;
    UINT1       u1ASELCap;
    BOOL1       isOptional;
    UINT1       au1Pad[3];
    UINT1       au1SuppMCSSet[CAPWAP_11N_MCS_RATE_LEN];
}tHTCapability;

/* Information Element, Type = 1029 */

/*VHT Capabilities*/
typedef struct{
    union {
        tHTCapability HTCapability;
 tVHTCapabilityElem VHTCapability;
        tVHTOperationElem VHTOperation;
    }unInfoElem;
    UINT4    u4InfoID;
    UINT2    u2MsgEleType; /* 1029 */
    UINT2    u2MsgEleLen; /* >=4 */
    UINT2    u2EleId; 
    BOOL1    isOptional;
    UINT1    u1RadioID;
    UINT1    u1WlanID;
    UINT1    u1BPflag;
    UINT1    au1Pad[2];
}tInfoElement;

typedef struct{
   UINT4 u4ChannelNum;
   UINT4 u4DFSFlag;

}tDFSInfo;

typedef struct{
    tDFSInfo DFSInfo[16];
    UINT4    u4VendorId;
    UINT2    u2MsgEleType;
    UINT2    u2MsgEleLen;
    UINT1    u1RadioId;
    BOOL1    isOptional;
    UINT1    au1Pad[2];
}tDFSChannelInfoVendorType;

typedef struct{

   UINT4    u4VendorId;
   UINT2    u2MsgEleType;
   UINT2    u2MsgEleLen;
   UINT2    u2ChannelNum;
   UINT1    u1RadioId;
   BOOL1    isOptional;
   BOOL1     isRadarFound;
   UINT1    au1Pad[3];
}tDFSRadarEventVendorInfo;

#ifdef WLC_WANTED
typedef struct {
  UINT4     u4VendorId;
  UINT2     u2MsgEleLen;
  UINT2     u2MsgEleType;
  UINT2     u2WlanProfileId;
  UINT1     u1InPriority;
  UINT1     u1OutDscp;
  UINT1     u1EntryStatus;
  UINT1     u1RadioId;
  BOOL1     isOptional;
  UINT1     au1Pad;
}tVendorSpecDiffServ;
#endif

typedef struct{
    UINT4   u4NextIpAddress;
    UINT4   u4VendorId;
    UINT2   u2MsgEleLen;
    UINT2   u2MsgEleType;
    UINT1   u1RelayStatus;
    BOOL1   b1IsOptional;
    UINT1   au1Pad[2];
}tDhcpRelayConfigUpdateReq;

typedef struct{
    UINT4               u4DhcpPoolId[MAX_POOLS];
    UINT4               u4Subnet[MAX_POOLS];
    UINT4               u4Mask[MAX_POOLS];
    UINT4               u4StartIp[MAX_POOLS];
    UINT4               u4EndIp[MAX_POOLS];
    UINT4               u4LeaseExprTime[MAX_POOLS];
    UINT4               u4DefaultIp[MAX_POOLS];
    UINT4               u4DnsServerIp[MAX_POOLS];
    UINT4               u4VendorId;
    UINT2  u2PoolStatus[MAX_POOLS];
    UINT2               u2MsgEleLen;
    UINT2               u2MsgEleType;
    UINT2  u2NoofPools;
    BOOL1               b1IsOptional;
    UINT1               u1ServerStatus;
}tDhcpPoolConfigUpdateReq;

typedef struct{
    UINT4               u4VendorId;
    UINT2               u2MsgEleLen;
    UINT2               u2MsgEleType;
    UINT1  u1FirewallStatus;
    BOOL1               b1IsOptional;
    UINT1  au1Pad[2];
}tFireWallUpdateReq;

typedef struct{
     UINT4 u4NATConfigIndex;
     INT4 i4NATConfigIndex;
     UINT4      u4VendorId;
     INT2 i2MainWanType;
     INT2 i2RowStatus;
     UINT2      u2MsgEleLen;
     UINT2      u2MsgEleType;     
     INT2 i2WanType;
     BOOL1      b1IsOptional;
     UINT1 au1Pad;
}tNatUpdateReq;

typedef struct{
     UINT4 u4VendorId;
     UINT4 u4Subnet[MAX_IP_ROUTES];
     UINT4 u4NetMask[MAX_IP_ROUTES];
     UINT4 u4Gateway[MAX_IP_ROUTES];
     UINT2 u2MsgEleLen;
     UINT2 u2MsgEleType;
     BOOL1 b1IsOptional;
     UINT1 u1EntryStatus;
     UINT2 u2NoofRoutes;
}tRouteTableUpdateReq;

typedef struct{
     UINT4 u4VendorId;
     UINT2 u2MsgEleLen;
     UINT2 u2MsgEleType;
     UINT2 u2NoOfFilters;
     UINT2 u2AddrType;
     UINT1 au1FilterName[AP_FWL_MAX_FILTER_NAME_LEN + 1];
     UINT1 au1Pad2[3];
     UINT1 au1SrcPort [AP_FWL_MAX_PORT_LEN];
     UINT1 au1DestPort [AP_FWL_MAX_PORT_LEN];
     UINT1 au1SrcAddr[AP_FWL_MAX_ADDR_LEN];
     UINT1 au1DestAddr[AP_FWL_MAX_ADDR_LEN];
     UINT1 au1Pad1[2];
     BOOL1 b1IsOptional;
     UINT1 u1EntryStatus;
     UINT1 u1Proto;
     UINT1 au1Pad;
}tFirewallFilterUpdateReq;

typedef struct{
     UINT4 u4VendorId;
     INT4  i4AclIfIndex;
     UINT2 u2MsgEleLen;
     UINT2 u2MsgEleType;
     UINT2 u2NoOfAcl;
     UINT2 u2SeqNum;
     BOOL1 b1IsOptional;
     UINT1 u1AclDirection;
     UINT1 u1Action;
     UINT1 u1EntryStatus;
     UINT1 au1AclName[AP_FWL_MAX_ACL_NAME_LEN];
     UINT1 au1FilterSet [AP_FWL_MAX_FILTER_SET_LEN];
}tFirewallAclUpdateReq;

typedef struct{
     UINT4 u4VendorId;
     UINT4 u4IpAddr[MAX_L3IFACES];
     UINT4 u4SubnetMask[MAX_L3IFACES];
     UINT4 u4PhyPort[MAX_L3IFACES];
     UINT2 u2MsgEleLen;
     UINT2 u2MsgEleType;
     UINT2 u2VlanId[MAX_L3IFACES];
     UINT2 u2NoOfIfaces;
     UINT1 u1IfType[MAX_L3IFACES];
     UINT1 u1IfNwType[MAX_L3IFACES];
     UINT1 u1IfAdminStatus[MAX_L3IFACES];
     UINT1 u1EntryStatus;
     BOOL1 b1IsOptional;
}tL3SubIfUpdateReq;

typedef struct{
    tDhcpRelayConfigUpdateReq       DhcpRelayConfigUpdateReq;
    tDhcpPoolConfigUpdateReq        DhcpPoolConfigUpdateReq;
    tFireWallUpdateReq              FireWallUpdateReq;
    tNatUpdateReq                   NatUpdateReq;
    tRouteTableUpdateReq            RouteTableUpdateReq;
    tFirewallFilterUpdateReq        FirewallFilterUpdateReq;
    tFirewallAclUpdateReq           FirewallAclUpdateReq;
    tL3SubIfUpdateReq             L3SubIfUpdateReq;
}tBatchCmdUpdateReq;


typedef struct {
  UINT4     u4VendorId;
  UINT2     u2MsgEleLen;
  UINT2     u2MsgEleType;
  UINT1     u1RadioId;
  UINT1     u1WlanId;
  UINT1     u1InPriority;
  UINT1     u1OutDscp;
  UINT1     u1EntryStatus;
  BOOL1     isOptional;
  UINT1     au1Pad[2];
}tVendorCapwSpecDiffServ;

typedef struct{
    UINT4    u4VendorId;
    UINT2    u2MsgEleType;
    UINT2    u2MsgEleLen;
    UINT2    u2AMPDULimit;
    UINT2    u2AMSDULimit;
    UINT1    u1RadioId;
    UINT1    u1AMPDUStatus;
    UINT1    u1AMPDUSubFrame;
    UINT1    u1AMSDUStatus;
    BOOL1    isOptional;
    UINT1    au1Pad[3];
}tVendorDot11nCfg;


#ifdef BAND_SELECT_WANTED
typedef struct{
   UINT4    u4VendorId;
   UINT2    u2MsgEleType;
   UINT2    u2MsgEleLen;
   UINT1    u1RadioId;
   UINT1    u1WlanId;
   UINT1    u1BandSelectGlobalStatus;
   UINT1    u1BandSelectStatus;
   UINT1    u1AgeOutSuppression;
   BOOL1    isOptional;
   UINT1    au1Pad[2];
}tVendorBandSelect;
#endif

/* Vendor Speciic Payload, Type = 37 */
typedef struct{
    UINT4    vendorId;
    UINT2    u2MsgEleType; /* 37 */
    UINT2    u2MsgEleLen;  /* >=7 */
    UINT2    elementId;
    BOOL1    isOptional;    /* This is used during assembling the packet */
    UINT1    u1RFVendorMsgEleType;
    UINT1    data[2048];
    union {
    tVendorDiscoveryType  VendDiscType;
    tVendorUdpServerPort  VendUdpPort;
    tVendorControlDTLS    VendorContDTLS;
    tDot11nVendorType     Dot11nVendor;
    tDot11nStaVendorType  Dot11nStaVendor;
    tVendorSsidIsolation  VendSsidIsolation;
#ifdef WLC_WANTED
    tVendorMulticast   VendorMulticast;
#endif
    tVendorSsidRateLimit  VendSsidRate;
    tVendorWlanBssIdStats VendWlanBssIdStats;
    tVendorDNS            VendDns;
    tVendorNativeVlan     VendVlan;
    tVendorMgmtSsid       VendSsid;
    tVendorApLocalRouting VendLocalRouting;
#ifdef RFMGMT_WANTED
    tVendorNeighborAp    VendNeighAP;
    tVendorClientScan    VendClientScan;
    tVendorNeighConfig   VendorNeighConfig;
    tVendorClientConfig  VendorClientConfig;
    tVendorChSwitchStatus VendorChSwitchStatus;
    tVendorChAllowedList VendorChAllowedList;
    tVendorTpcSpectMgmt   VendorTpcSpectMgmt;
    tVendorDfsParams      VendorDfsParams;
#endif
    tVendorWebAuthStatus  VendorWebAuthStatus;
    tWssStaVendSpecPayload WssStaVendSpecPayload;
    tWssStaWMMVendor    WMMStaVendor;
#ifdef WPS_WTP_WANTED    
    tVendorWpsConfig       VendorWpsConfig;
#endif    
#ifdef WLC_WANTED
    tVendorWlanInterfaceIp      VendorWlanInterfaceIp;
    tVendorSpecDiffServ VendorSpecDiffServ;
#endif
    tVendorSubnetPool          VendorSubnetPool;
    tVendorFirewallStatus VendorFirewallStatus;
    tVendorNatStatus            VendorNatStatus;
    tVendorMultiDomainInfo VendMultiDomain;
    tDFSChannelInfoVendorType  DFSChannelInfo;
    tDFSRadarEventVendorInfo  DFSRadarEventInfo;
    tDhcpRelayConfigUpdateReq       DhcpRelayConfigUpdateReq;
    tDhcpPoolConfigUpdateReq        DhcpPoolConfigUpdateReq;
    tFireWallUpdateReq   FireWallUpdateReq;
    tNatUpdateReq NatUpdateReq;
    tRouteTableUpdateReq  RouteTableUpdateReq;
    tFirewallFilterUpdateReq        FirewallFilterUpdateReq;
    tFirewallAclUpdateReq           FirewallAclUpdateReq;
    tVendorCapwSpecDiffServ  VendorCapwSpecDiffServ;
    tL3SubIfUpdateReq    L3SubIfUpdateReq;
    tBatchCmdUpdateReq  BatchCmdUpdateReq;
#ifdef WTP_WANTED
#ifdef PMF_WANTED
    tVendorPMFPktInfo VendPMFPktNo;
#endif
#endif
#ifdef RFMGMT_WANTED
#ifdef ROGUEAP_WANTED
 tVendorRougeAp        RougeAp;
#endif 
#endif 
    tVendorDot11nCfg VendorDot11nCfg;
#ifdef BAND_SELECT_WANTED
    tVendorBandSelect         VendorBandSelect;
#endif
    tVendorWlanVlan     VendWlanVlan;    
#ifdef WPA_WANTED
    tWpaIEElements  WpaIEElements;
#endif
    }unVendorSpec;
}tVendorSpecPayload;

/* WTP Board Data, Type = 38 */
typedef struct {
    UINT2     u2BoardInfoType;
    UINT2     u2BoardInfoLength;
    UINT1     boardInfoData[CAPWAP_MAX_DATA_LENGTH]; 
 BOOL1     isOptional;
    UINT1     au1Pad[3];
}tWtpBoardInfo;

typedef struct {
    tWtpBoardInfo  wtpBoardInfo[MAX_WTP_BOARD_INFO];
    UINT4          u4VendorSMI;
    UINT2          u2MsgEleType;
    UINT2          u2MsgEleLen;
    UINT1          u1DataInfoCount;
    BOOL1          isOptional;    /* This is used during assembling the packet */
    UINT1          au1Pad[2]; 
}tWtpBoardData;


/* WTP Descriptor, Type = 39*/
typedef struct {
    UINT2    encryptCapability;
    UINT1    u1WBID;
    UINT1    au1Pad[1];
} tEncrCap;

typedef struct {
    tVendorDescriptor  VendDesc[CAPWAP_MAX_WTP_DESCRIPTOR_TYPE];
    tEncrCap           wtpEncrptCap;
    UINT4              u4VendorSMI;
    UINT4              u4VendorInfoCount;
    UINT2              u2MsgEleLen;  /* >=33 */
    UINT2              u2MsgEleType;
    UINT1              u1MaxRadios;  /* No of radios in the WTP */
    UINT1              u1RadiosInUse;  /* Num of radios in use in the WTP */
    UINT1              u1NumEncrypt;
    BOOL1              isOptional;    /* This is used during assembling the packet */
}tWtpDescriptor;

/* WTP Fallback, Type =  40 */
typedef struct{
    UINT2 u2MsgEleType; /* message element type - 40*/
    UINT2 u2MsgEleLen; /* message element length - 1 */
    UINT1 mode; /* WTP Fallback mode */
    BOOL1 isOptional;    /* This is used during assembling the packet */
    UINT1 au1Pad[2];
}tWtpFallback;

/* WTP Frame Tunnel Mode, Type = 41 */
typedef enum {
    TUNNEL_NATIVE_MODE = 8,
    TUNNEL_802_3_MODE = 4,
    TUNNEL_LOCAL_BRIDGE_MODE = 2,
    TUNNEL_RESERVED_MODE = 0
}eFrameTunnelType; 

typedef struct{
    UINT2   u2MsgEleType;/* 41*/
    UINT2   u2MsgEleLen;/*1 */
    UINT1   resvd4NELU; /* 4 bits reserved, Flags N,E,L,U*/
    UINT1   au1Pad[3];
} tWtpFrameTunnel;

/* WTP MAC Type, Type = 44 */
typedef enum{
    LOCAL_MAC_MODE,
    SPLIT_MAC_MODE,
    LOCAL_SPLIT_MAC_MODE
}eMACType;

typedef struct{
    eMACType  macValue;
    UINT2     u2MsgEleType; /* 44 */
    UINT2     u2MsgEleLen;  /* 1 */
    BOOL1     isOptional;    /* This is used during assembling the packet */
    UINT1     au1Pad[3]; 
}tWtpMacType;

/* WTP Name, Type = 45 */
typedef struct{
    UINT2 u2MsgEleType; /* 45 */
    UINT2 u2MsgEleLen;  /* >=1 */
    UINT1 wtpName[CAPWAP_WTP_NAME_SIZE]; /* max capwap mesg that the WTP supports to the AC  */
    BOOL1 isOptional;    /* This is used during assembling the packet */
    UINT1 au1Pad[3];
}tWtpName;


/* WTP Static IP Address Information, Type = 49 */
typedef struct{
    UINT4 u4IpAddr;
    UINT4 u4IpNetMask;
    UINT4 u4IpGateway;
    UINT2 u2MsgEleType; /* 49 */
    UINT2 u2MsgEleLen;  /* 13 */
    BOOL1 bStaticIpBool; 
    BOOL1 isOptional;    /* This is used during assembling the packet */
    UINT1 au1Pad[2];
}tWtpStaticIpAddr;

/* CAPWAP Transport Protocol, Type = 51 */
typedef enum{
    TRANS_PROT_MIN = 0,
    CAPWAP_UDP_LITE, 
    CAPWAP_UDP,
    TRANS_PROT_MAX
}eCapwapTransProtoEnum;

typedef struct{
    eCapwapTransProtoEnum  transportType; /* UDP-Lite or UDP transport protocol */
    UINT2       u2MsgEleType; /* 51 */
    UINT2       u2MsgEleLen;  /* 1 */
    BOOL1       isOptional;    /* This is used during assembling the packet */
    UINT1       au1Pad[3];
}tCapwapTransprotocol;


/* MTU Discovery Padding, Type = 52*/
typedef struct{
    UINT2 u2MsgEleType; /* 52 */
    UINT2 u2MsgEleLen;  /* Variable*/
    UINT1 paddingVal[CAPWAP_MAX_PADDING_SIZE]; /* contains octets of value 0xFF */
    BOOL1 isOptional;    /* This is used during assembling the packet */
    UINT1 au1Pad[3];
}tMtuDiscPad;

/* Ech Support, Type = 53*/
typedef enum {
    CAPWAP_LIMITED_ECN_SUPPORT,
    CAPWAP_FULL_ENC_SUPPORT
}eEcnSupportType;

typedef struct{
    UINT2 u2MsgEleType; /* 53 */
    UINT2 u2MsgEleLen;  /* 1 */
    BOOL1 isOptional;    /* This is used during assembling the packet */
    UINT1 u1EcnSupport;/* 0 . limited ECN Support, 1- Full and limited ECN support */
    UINT1 au1Pad[2];
}tEcnSupport;


/*  IEEE 802.11 WTP Radio Info., Type = 1048*/
typedef struct{
      UINT4 radioType; /* 4 bits reserved, N, G, A, B flags */
      UINT2 u2MsgEleType; /* message element type */
      UINT2 u2MsgEleLen; /* message element length */
      UINT1 radioId; /* The Radio id, between 1 and 31 */
      BOOL1 isOptional;    /* This is used during assembling the packet */
      UINT1 au1Pad[2];
}tWtpRadInfo;

/* WTP Radio Statistics, Type =  47 */
typedef struct{
    UINT2 u2ResetCount;
    UINT2 u2SwFailCount;
    UINT2 u2HwFailCount;
    UINT2 u2OtherFailureCount;
    UINT2 u2UnknownFailureCount;
    UINT2 u2ConfigUpdateCount;
    UINT2 u2ChannelChangeCount;
    UINT2 u2BandChangeCount;
    UINT2 u2CurrentNoiseFloor;
    UINT1 u1RadioId;   /* radioId */
    UINT1 u1LastFailureType;
} tWtpRadioStats;

typedef struct {
    UINT2                u2MsgEleType; /* 47 */
    UINT2                u2MsgEleLen;
    tWtpRadioStats       wtpRadioStats;
    BOOL1                isOptional;    /* This is used during assembling the packet */
    UINT1                au1Pad[3];
}tWtpRadioStatsElement;

/* WTP Reboot Statistics, Type = 48 */
typedef struct{
    UINT2 u2MsgEleType; /* 48 */
    UINT2 u2MsgEleLen;  /* 15 */
    UINT2 u2RebootCount;
    UINT2 u2AcInitiatedCount;
    UINT2 u2LinkFailCount;
    UINT2 u2SwFailCount;
    UINT2 u2HwFailCount;
    UINT2 u2OtherFailCount;
    UINT2 u2UnknownFailCount;
    UINT1 u1LastFailureType;
    BOOL1 isOptional;    /* This is used during assembling the packet */
} tWtpRebootStatsElement;

typedef struct {
UINT4    discReqRxd;
UINT4    discRespSent;
UINT4    discUnsuccReqProcessed;
UINT4    discReasonLastUnsuccAtt;
UINT4    discLastSuccAttTime;
UINT4    discLastUnsuccAttTime;
}tWtpCapwapSessDiscStats;                        /* Session Discovery Statistics */

typedef struct{
UINT4    joinReqRxd;
UINT4    joinRespSent;
UINT4    joinUnsuccReqProcessed;
UINT4    joinReasonForLastUnsuccAtt;
UINT4    joinLastSuccAttTime;
UINT4    joinLastUnsuccAttTime;
}tWtpCapwapSessJoinStats;                        /* Session Join Statistics */

typedef struct{
UINT4    cfgReqSent;
UINT4    cfgRespRcvd;
UINT4    cfgUnsuccReqProcessed;
UINT4    cfgReasonForLastUnsuccAtt;
UINT4    cfgLastSuccAttTime;
UINT4    cfgLastUnsuccAttTime;
}tWtpCapwapSessConfigStats;                      /* Session Config. Statistics */

typedef struct {
    tWtpCapwapSessDiscStats   wtpCapwapSessDiscStats;
    tWtpCapwapSessJoinStats   wtpCapwapSessJoinStats;
    tWtpCapwapSessConfigStats wtpCapwapSessConfigStats;
    UINT4                     wtpKeepAlivePktsSent;
}tWtpCapwapSessStats;                            /* Capwap-Session related Statistics collected from WLC context */

typedef struct {
UINT4    discReqSent;
UINT4    discRespRxd;
UINT4    joinReqSent;
UINT4    joinRespRxd;
UINT4    cfgReqSent; 
UINT4    cfgRespRxd;
UINT4    keepAliveMsgRcvd;
} tWtpCPWPWtpStats;

typedef struct {
    UINT2                u2MsgEleType; /* 47 */
    UINT2                u2MsgEleLen;
    tWtpCPWPWtpStats     capwapStats;
    BOOL1                isOptional;    
    UINT1                au1Pad[3];
} tWtpCapwapSessStatsElement;

typedef struct {
    UINT4       u4TxFragmentCnt;
    UINT4       u4MulticastTxCnt;
    UINT4       u4FailedCount;
    UINT4       u4RetryCount;
    UINT4       u4MultipleRetryCount;
    UINT4       u4FrameDupCount;
    UINT4       u4RTSSuccessCount;
    UINT4       u4RTSFailCount;
    UINT4       u4ACKFailCount;
    UINT4       u4RxFragmentCount;
    UINT4       u4MulticastRxCount;
    UINT4       u4FCSErrCount;
    UINT4       u4TxFrameCount;
    UINT4       u4DecryptionErr;
    UINT4       u4DiscardQosFragmentCnt;
    UINT4       u4AssociatedStaCount;
    UINT4       u4QosCFPollsRecvdCount;
    UINT4       u4QosCFPollsUnusedCount;
    UINT4       u4QosCFPollsUnusableCount;
    UINT1       u1RadioId;        
    UINT1       au1Pad[3];
} tDot11Statistics;

typedef struct {
    UINT2                u2MsgEleType; /* 47 */
    UINT2                u2MsgEleLen;
    tDot11Statistics     dot11Stats;
    BOOL1                isOptional;    
    UINT1                au1Pad[3]; 
}tWtpdot11StatsElement;

typedef struct {
UINT4    wlanBSSIDBeaconsSentCount;
UINT4    wlanBSSIDProbeReqRcvdCount;
UINT4    wlanBSSIDProbeRespSentCount;
UINT4    wlanBSSIDDataPktRcvdCount;
UINT4    wlanBSSIDDataPktSentCount;
BOOL1    isOptional;
UINT1    au1Pad[3];
}tBSSIDMacHndlrStats;             

typedef struct {
    UINT2                u2MsgEleType; /* 47 */
    UINT2                u2MsgEleLen;
    tBSSIDMacHndlrStats  BSSIDMacHndlrStats[16];
    tMacAddr             BssId[16];
    UINT1                numBssCount;
    BOOL1                isOptional;   
    UINT1                au1Pad[2]; 
}tBssIDMacHdlrStatsElement;
typedef struct {
    UINT4 u4IfInOctets;
    UINT4 u4IfOutOctets;
}tWtpRCStats;
typedef struct {
    UINT4       u4ClientStatsBytesSentCount;
    UINT4       u4ClientStatsBytesRecvdCount;
    UINT4       u4StaIpAddr;
    FLT4        f4ClientStatsTotalSentCount;
    FLT4        f4ClientStatsTotalRecvdCount;
    tMacAddr StaMacAddr;
    UINT1 au1Pad[2];
}tWtpStaStats;
typedef struct {
    tWtpRCStats     radioClientStats[SYS_DEF_MAX_RADIO_INTERFACES];
    tWtpStaStats    clientStats[MAX_STA_SUPP_PER_AP];
    UINT2           u2MsgEleType; /* 47 */
    UINT2           u2MsgEleLen;
    UINT1           numRadioCount;
    UINT1           numStaCount;
    BOOL1           isOptional;
    UINT1           u1Pad;
}tRadioClientStats;
typedef struct{
    UINT4 u4GatewayIp;
    UINT4 u4Subnetmask;
    UINT4 u4SentBytes;
    UINT4 u4RcvdBytes;
    UINT4 u4SentTrafficRate;
    UINT4 u4RcvdTrafficRate;
    UINT1 u1TotalInterfaces;
    UINT1 au1Pad[3];
}tApParams;
typedef struct {
    UINT2         u2MsgEleType; /* 47 */
    UINT2         u2MsgEleLen;
    tApParams     ApElement;
    BOOL1         isOptional;
    UINT1         au1Pad[3];
}tApParamsStats;
typedef struct{
    tWtpRebootStatsElement     wtpRebootstatsElement;
    tWtpRadioStatsElement      wtpRadiostatsElement;
    tWtpCapwapSessStatsElement capwapStatsElement;
    tWtpdot11StatsElement      dot11StatsElement;
    tBssIDMacHdlrStatsElement  MacHdlrStatsElement;
    tRadioClientStats        RadioClientStatsElement;
    tApParamsStats        ApParamsElement; 
    tVendorSpecPayload   vendSpec;
}tPmWtpEventReq;


/*****************************************/
/*        Discovery request , Msg Type =1             */
/*****************************************/
/* Discovery type - possible values */
typedef enum{
    UNKNOWN_DISC_TYPE = 0,    
    STATIC_CONF_DISC_TYPE,
    DHCP_DISC_TYPE, 
    DNS_DISC_TYPE,     
    AC_REFERRAL_DISC_TYPE,
    AUTO_DISC_TYPE
}ecapwapDiscReqVal;

typedef enum {
    CAPWAP_BROADCAST_ENABLE = 1,
    CAPWAP_MULTICAST_ENABLE = 2
}eCapwapAcRefOption;

/* Discovery request message */
typedef struct{
   tCapwapHdr          capwapHdr;
   tWtpDiscType        discType;
   tWtpBoardData       wtpBoardData;
   tWtpDescriptor      wtpDescriptor;
   tWtpFrameTunnel     wtpTunnelMode;
   tWtpMacType         wtpMacType;
   tRadioIfInfo        RadioIfInfo;
   /* Optional */
   tMtuDiscPad         mtuDiscPad;
   tVendorSpecPayload  vendSpec;
   UINT4               u4CtrlPort;
   UINT2               u2CapwapMsgElemenLen;/* Length following the sequence num */
   UINT1               u1SeqNum;
   UINT1               u1NumMsgBlocks;
   UINT1               u1AcRefOption;
   UINT1               au1Pad[3]; 
}tDiscReq;

/*****************************************/
/*        Discovery response, MsgType =2            */
/*****************************************/

typedef struct{
    tCapwapHdr          capwapHdr;
    tAcDescriptor       acDesc;
    tACName             acName;
    tRadioIfInfo        RadioIfInfo;
    tCtrlIpAddr         ctrlAddr;
    tACIplist           ipv4List;
    tVendorSpecPayload  vendSpec;
    UINT2               u2CapwapMsgElemenLen;/* Length following the sequence num */
    UINT2               u2WtpInternalId;
    UINT1               u1SeqNum;
    UINT1               u1NumMsgBlocks;
    UINT1               au1Reserved[2];
}tDiscRsp;

/* Primary Discovery request message */ 
typedef struct{     
   tCapwapHdr          capwapHdr;     
   tWtpDiscType        discType;     
   tWtpBoardData       wtpBoardData;     
   tWtpDescriptor      wtpDescriptor;     
   tWtpFrameTunnel     wtpTunnelMode;     
   tWtpMacType         wtpMacType;     
   tRadioIfInfo        RadioIfInfo;     
   /* Optional */     
   tMtuDiscPad         mtuDiscPad;     
   tVendorSpecPayload  vendSpec;     
   UINT4               u4CtrlPort;     
   UINT2               u2CapwapMsgElemenLen;/* Length following the sequence num */     
   UINT1               u1SeqNum;     
   UINT1               u1NumMsgBlocks;     
   UINT1               u1AcRefOption;     
   UINT1               au1Pad[3];     
}tPriDiscReq;     
      
/* Primary Discovery response message */     
typedef struct{     
    tCapwapHdr          capwapHdr;     
    tAcDescriptor       acDesc;     
    tACName             acName;     
    tRadioIfInfo        RadioIfInfo;     
    tCtrlIpAddr         ctrlAddr;     
    tACIplist           ipv4List;     
    tVendorSpecPayload  vendSpec;     
    UINT2               u2CapwapMsgElemenLen;/* Length following the sequence num */     
    UINT2         u2WtpInternalId;
    UINT1               u1SeqNum;     
    UINT1               u1NumMsgBlocks;     
    UINT1               au1Pad[2];     
}tPriDiscRsp; 

/****************************** **************/
/*        Join Request , Msg Type =3                           */
/*********************************************/
typedef struct {
   tCapwapHdr          capwapHdr;
   tLocationData       wtpLocation;
   tWtpBoardData       wtpBoardData;
   tWtpDescriptor      wtpDescriptor;
   tWtpName            wtpName;
   tWtpFrameTunnel     wtpTunnelMode;
   tWtpMacType         wtpMacType;
   tRadioIfInfo        RadioIfInfo;
   tEcnSupport         ecnSupport;
   tLocalIpAddr        incomingAddress; 
   /* Optional */
   tCapwapTransprotocol  transProto;
   tMaxMsgLen          msgLength;
   tWtpRebootStatsElement     rebootStats;
   tVendorSpecPayload  vendSpec;
   UINT4               sessionID[CAPWAP_JOIN_SESSIONID_SIZE];
   UINT2               u2CapwapMsgElemenLen;/* Length following the sequence num */
   UINT1               u1SeqNum;
   UINT1               u1NumMsgBlocks;
} tJoinReq;


/*******************************************/
/*        Join Response, Msg Type = 4                       */
/*******************************************/
typedef struct{
   tCapwapHdr          capwapHdr;
   tResCode            resultCode;
   tAcDescriptor       acDesc;
   tACName             acName;
   tRadioIfInfo        RadioIfInfo;
   tEcnSupport         ecnSupport;
   tCtrlIpAddr         controlIpAddr;
   tLocalIpAddr        localIpAddr;
   tACIplist         ipv4List;
   tCapwapTransprotocol  transProto;
   tImageId            imageId;
   tMaxMsgLen          msgLength;
   tVendorSpecPayload  vendSpec;
   UINT2               u2CapwapMsgElemenLen;/* Length following the sequence num */
   UINT2               u2IntProfileId;    
   UINT1               u1SeqNum;
   UINT1               u1NumMsgBlocks;
   UINT1               au1Reserved[2];
}tJoinRsp;

/***************************************************/
/*        Configuation Status Request , Msg Type = 5               */
/***************************************************/
typedef struct{
   tCapwapHdr          capwapHdr;
   tACName             acName;
   tStatisticsTimer    statsTimer;
   tWtpRebootStatsElement     rebootStats;
   /* Optional */
   tACNameWithPrio     acNameWithPrio;
   tCapwapTransprotocol  transProto;
   tWtpStaticIpAddr    wtpStaticIpAddr;
   tVendorSpecPayload  vendSpec[VENDOR_MAX_TYPE];
   BOOL1               isVendorOptional;
   UINT1                    au1Pad[3]; 
   /* IEEE 802.11 Message Elements */
   tRadioIfAdminStatus     radAdminState;
   tRadioIfAntenna         RadioIfAntenna;
   tRadioIfDSSSPhy         RadioIfDSSPhy;
   tRadioIfInfoElement     RadioIfInfoElem;
   tRadioIfMacOperation    RadioIfMacOperation;
   tRadioIfMultiDomainCap  RadioIfMultiDomainCap;
   tRadioIfOFDMPhy         RadioIfOFDMPhy;
   tRadioIfSupportedRate   RadioIfSupportedRate;
   tRadioIfTxPower         RadioIfTxPower;
   tRadioIfTxPowerLevel    RadioIfTxPowerLevel;
   tRadioIfConfig          RadioIfConfig;
   tRadioIfInfo            RadioIfInfo;
   UINT2               u2CapwapMsgElemenLen;/* Length following the sequence num */
   UINT1               u1SeqNum;
   UINT1               u1NumMsgBlocks;
}tConfigStatusReq;

/****************************************************/
/*        Configuation Status Response , Msg Type = 6               */
/****************************************************/
typedef struct{
   tCapwapHdr          capwapHdr;
   tCapwapTimer capwapTimer;
   tDecryptErrReportPeriod  decryErrPeriod;
   tIdleTimeout    idleTimeout;
   tWtpFallback    wtpFallback;
   tACIplist         ipList;
    /* Optional */
   tWtpStaticIpAddr    wtpStaticIpAddr;
   tVendorSpecPayload  vendSpec[VENDOR_MAX_TYPE];
   tACNameWithPrio          acNameWithPrio[3];
   /* IEEE 802.11 Message Elements */
   tRadioIfAntenna         RadioIfAntenna;
   tRadioIfDSSSPhy         RadioIfDSSPhy;
   tRadioIfInfoElement     RadioIfInfoElem;
   tRadioIfMacOperation    RadioIfMacOperation;
   tRadioIfMultiDomainCap  RadioIfMultiDomainCap;
   tRadioIfOFDMPhy         RadioIfOFDMPhy;
   tRadioIfRateSet         RadioIfRateSet;
   tRadioIfSupportedRate   RadioIfSupportedRate;
   tRadioIfTxPower         RadioIfTxPower;
   tRadioIfQos             RadioIfQos;
   tRadioIfConfig          RadioIfConfig;
   UINT2               u2CapwapMsgElemenLen;/* Length following the sequence num */
   UINT2               u2IntProfileId;
   UINT1               u1NumMsgBlocks;
   UINT1               u1SeqNum;
   UINT1               au1Reserved[2];
}tConfigStatusRsp;

/**************************************************/
/*        Configuation Update Request , Msg Type =  7           */
/**************************************************/
typedef struct {
     tACNameWithPrio          acNameWithPrio[3];
     tCapwapTimer             capwapTimer;
     tIdleTimeout             idleTimeout;
     tWtpFallback             wtpFallback;
     tWtpName                 wtpName;
     tStatisticsTimer         statsTimer;
     tDecryptErrReportPeriod  decryErrPeriod;
     tLocationData            wtpLocation;
     tAddMacAclEntry          addMacEntry;
     tDeleteMacAclEntry       delMacEntry;
     tImageId                 imageId;
     tWtpStaticIpAddr         wtpStaticIpAddr;
     tVendorSpecPayload       vendSpec;
     UINT4                    u4SessId;
     UINT2                    u2InternalId;
     UINT1                    au1Pad[2]; 
}tCapwapConfigUpdateReq;

typedef struct {
    tACtimestamp             acTimestamp;
    UINT4                    u4SessId;
}tClkiwfConfigUpdateReq;


typedef struct {
    tStatisticsTimer         statsTimer;
    UINT4                    u4SessId;
}tPmConfigUpdateReq;


/* Config Update request */
typedef struct{
   tCapwapHdr                      capwapHdr;
   tCapwapConfigUpdateReq          CapwapConfigUpdateReq;
   tClkiwfConfigUpdateReq          ClkiwfConfigUpdateReq;
   tRadioIfConfigUpdateReq         RadioIfConfigUpdateReq;
   tPmConfigUpdateReq              PmConfigUpdateReq;
#ifdef RFMGMT_WANTED
   tRfMgmtConfigUpdateReq          RfMgmtConfigUpdateReq;
#endif
   UINT2                           u2CapwapMsgElemenLen;/* Length following the sequence num */
   UINT1                           u1NumMsgBlocks;
   UINT1                           u1SeqNum;
}tConfigUpdateReq;

/**************************************************/
/*        Configuation Update Response , Msg Type = 8          */
/**************************************************/
typedef struct{
   tCapwapHdr          capwapHdr;
   tResCode            resultCode;
   tRadioIfOperStatus  radOperState;
   tVendorSpecPayload  vendSpec;
   UINT2               u2CapwapMsgElemenLen;/* Length following the sequence num */
   UINT1               u1NumMsgBlocks;
   UINT1               u1SeqNum;
}tConfigUpdateRsp;



/*******************************************/
/*        WTP  Event Request , Msg Type = 9            */
/*******************************************/
typedef struct {
       UINT4               u4SessId;
       tDecryptErrReport  decrypreport;
}tMacHdlrWtpEventReq;

typedef struct {
   UINT4              u4SessId;
   tDupIPV4Addr       ipv4addr;
   tDupIPV6Addr       ipv6addr;
}tArpWtpEventReq;

typedef struct {
    UINT2       u2MessageType;
    UINT2       u2MessageLength;
    UINT1       u1RadioId;
    UINT1       u1WlanId;
    tMacAddr    StaMacAddr;
    BOOL1       isOptional;
    UINT1       au1Pad[3]; 
}tDot11MicCountermeasures;

typedef struct {
    UINT2       u2MessageType;
    UINT2       u2MessageLength;
    tMacAddr    ClientAddr;
    UINT2       u2Reserved;
    tMacAddr    BssId;
    UINT1       u1RadioId;
    UINT1       u1WlanId;
    UINT4       u4TkipICVErr;
    UINT4       u4TkipLocalMicFail;
    UINT4       u4TkipRemoteMicFail;
    UINT4       u4CCMPReplays;
    UINT4       u4CCMPDecryptErr;
    UINT4       u4TkipReplays;
    BOOL1       isOptional;
    UINT1       au1Pad[3]; 
}tDot11RSNAErrReport;

typedef struct 
{
 tCapwapHdr           capwapHdr;
 tMacHdlrWtpEventReq   MacHdlrWtpEventReq;
 tArpWtpEventReq   ArpWtpEventReq;
 tPmWtpEventReq    PmWtpEventReq;
 tStationWtpEventReq  StationWtpEventReq;
    tVendorSpecPayload   vendSpec;
    UINT1                u1SeqNum;
    UINT1                u1NumMsgBlocks;
    UINT2                u2CapwapMsgElemenLen;/* Length following the sequence num */
}tWtpEveReq;


/*********************************************/
/*        WTP Event  Response , Msg Type = 10            */
/*********************************************/
typedef struct{
   tCapwapHdr          capwapHdr;
   tVendorSpecPayload  vendSpec;
   UINT2               u2CapwapMsgElemenLen;/* Length following the sequence num */
   UINT1      u1SeqNum;
   UINT1               u1NumMsgBlocks;
}tWtpEveRsp;

/****************************************************/
/*        Change State Event Request  , Msg Type = 11            */
/****************************************************/
/*typedef struct {
    UINT4                   u4SessId;
    tRadioIfFailAlarm       RadioIfFailAlarm;
    tDot11InfoElement       Dot11InfoElement;
    tRadioOperState         radOperState[31];
}tRadioIfChangeStateEventReq;*/

typedef struct {
    tCapwapHdr              capwapHdr;
    tRadioIfChangeStateEventReq  RadioIfChaStateEvtReq;
    tReturnMsgElems         retMsgElems;
    tVendorSpecPayload      vendSpec;
    tResCode                resultCode;
    UINT4                   u4SessId;
    UINT2                   u2CapwapMsgElemenLen;/* Length following the sequence num */
    UINT1                   u1SeqNum;
    UINT1                   u1NumMsgBlocks;
}tChangeStateEvtReq;


/****************************************************/
/*        Change State Event Response  , Msg Type = 12            */
/****************************************************/
typedef struct{
   tCapwapHdr          capwapHdr;
   /*No Mandatory */
   /* Optional */
   tVendorSpecPayload  vendSpec;
   UINT2               u2CapwapMsgElemenLen;/* Length following the sequence num */
   UINT1               u1SeqNum;
   UINT1               u1NumMsgBlocks;
}tChangeStateEvtRsp;


/**************************************/
/*        Echo Request , Msg Type = 13            */
/**************************************/
typedef struct{
   tCapwapHdr          capwapHdr;
   tVendorSpecPayload  vendSpec;
   UINT2               u2CapwapMsgElemenLen;/* Length following the sequence num */
   UINT1               u1NumMsgBlocks;
   UINT1               u1SeqNum;
}tEchoReq;

/**************************************/
/*        Echo Response , Msg Type = 14         */
/**************************************/
typedef struct{
   tCapwapHdr          capwapHdr;
   tVendorSpecPayload  vendSpec;
   UINT2               u2CapwapMsgElemenLen;/* Length following the sequence num */
   UINT1               u1NumMsgBlocks;
   UINT1               u1SeqNum;
}tEchoRsp;

/*******************************************/
/*        Image Data Request  , Msg Type = 15         */
/*******************************************/
typedef struct {
    tCapwapHdr              capwapHdr;
 tCapwapTransprotocol    TransportProtocol;
 tImageData              ImageData;
 tImageId                ImageId;
 tImageIntiateDownload   InitiatelDownload;
    tVendorSpecPayload      vendSpec;
    UINT4                   u4SessId;
    UINT2                   u2CapwapMsgElemenLen;/* Length following the sequence num */ 
    UINT1                   u1SeqNum;
    UINT1                   u1NumMsgBlocks;
}tImageDataReq;

/*******************************************/
/*        Image Dat  Response , Msg Type = 16         */
/*******************************************/
typedef struct {
    tCapwapHdr              capwapHdr;
 tImageInfo              ImageInfo;
 tResCode                ResultCode;
    tVendorSpecPayload      vendSpec;
    UINT4                   u4SessId;
    UINT2                   u2CapwapMsgElemenLen;/* Length following the sequence num */
    UINT1                   u1SeqNum;
    UINT1                   u1NumMsgBlocks;
}tImageDataRsp;


/**************************************/
/*        Reset Request , Msg Type = 17           */
/**************************************/
typedef struct{
   tCapwapHdr          capwapHdr;
   UINT2               u2CapwapMsgElemenLen;/* Length following the sequence num */
   UINT2               u2SessId;
   tImageId            imageId;
   tVendorSpecPayload  vendSpec;
   UINT1               u1SeqNum;
   UINT1               u1NumMsgBlocks;
   UINT1               au1Pad[2]; 
}tResetReq;

/****************************************/
/*       Reset  Response , Msg Type = 18           */
/****************************************/
typedef struct{
   tCapwapHdr          capwapHdr;
   UINT2               u2CapwapMsgElemenLen;/* Length following the sequence num */ 
   UINT1               u1SeqNum;
   UINT1               u1NumMsgBlocks;
   tResCode            resultCode;
   tVendorSpecPayload  vendSpec;
}tResetRsp;


/*********************************************/
/*        Data Transfer Request, Msg Type = 21            */
/*********************************************/
typedef struct{
   tCapwapHdr          capwapHdr;
   tDataTransferMode   datatransfermode;
   tDataTransferData   datatransferdata;
   /* Optional */
   tVendorSpecPayload  vendSpec;
   UINT2               u2CapwapMsgElemenLen;/* Length following the sequence num */
   UINT1               u1SeqNum;
   UINT1               u1NumMsgBlocks;
   UINT2               u2SessId;
   BOOL1               isPresent;
   UINT1               au1Pad[1]; 
}tDataReq;

/**********************************************/
/*        Data Transfer Response , Msg Type = 22           */
/**********************************************/
typedef struct{
   tCapwapHdr          capwapHdr;
   tResCode            resultCode;
   /* Optional */
   tVendorSpecPayload  vendSpec;
   UINT2               u2CapwapMsgElemenLen;/* Length following the sequence num */
   UINT1               u1NumMsgBlocks;
   UINT1               u1SeqNum;
}tDataRsp;


/**********************************************/
/*        Clear configuration  Request , Msg Type = 23     */
/**********************************************/
typedef struct{
   tCapwapHdr          capwapHdr;
   tVendorSpecPayload  vendSpec;
   UINT2               u2CapwapMsgElemenLen;/* Length following the sequence num */
   UINT1               u1SeqNum;
   UINT1               u1NumMsgBlocks;
   UINT2               u2SessId;
   UINT1               au1Pad[2]; 
}tClearconfigReq;

/****************************************************/
/*        Clear Configuration  Response , Msg Type = 24           */
/****************************************************/
typedef struct{
   tCapwapHdr          capwapHdr;
   tResCode            resultCode;
   tVendorSpecPayload  vendSpec;
   UINT2               u2CapwapMsgElemenLen;/* Length following the sequence num */
   UINT1               u1SeqNum;
   UINT1               u1NumMsgBlocks;
}tClearconfigRsp;

/****************************************************/
/*        Configuation Station Request , Msg Type = 25              */
/****************************************************/
typedef struct {
    tCapwapHdr          capwapHdr;
    /* tAddstation                 addStation;
    tDeletestation              deleteStation;
    tWssStaDot11Station       staMsg;
    tWssStaDot11Sesskey       staSessKey;
    tWssStaDot11QoSProfile    staQosProfile;
    tWssStaDot11UpdateStaQoS  staQosUpdate; */
    tVendorSpecPayload   vendSpec;
    tWssStaConfigReqInfo wssStaConfig;
    tWssStaDot11nConfigInfo Dot11nConfig;
    UINT4               u4SessId;
    UINT2               u2CapwapMsgElemenLen;/* Length following the sequence num */
    UINT1               u1SeqNum;
    UINT1               u1NumMsgBlocks;
}tStationConfReq;

/****************************************************/
/*        Configuation Station Response , Msg Type = 26            */
/****************************************************/
typedef struct{
   tCapwapHdr          capwapHdr;
   tResCode            resultCode;
   /* Optional */
   tVendorSpecPayload  vendSpec;
   UINT2               u2CapwapMsgElemenLen;/* Length following the sequence num */
   UINT1               u1SeqNum;
   UINT1               u1NumMsgBlocks;
}tStationConfRsp;

/*******************************************/
/*        Unknown Response , Msg Type > 26            */
/*******************************************/
typedef struct{
    tCapwapHdr          capwapHdr;
    tResCode            resultCode;
 UINT4    u4MsgType;
    UINT2               u2CapwapMsgElemenLen;/* Length following the sequence num */
    UINT1               u1SeqNum;
    UINT1               u1NumMsgBlocks;
}tUnknownRsp;

/*********************************************************/
/*        IEEE Configuation WLAN Request , Msg Type = 3398913       */
/*********************************************************/
typedef struct {
    union {
    tWssWlanAddReq      WssWlanAddReq;
    tWssWlanUpdateReq   WssWlanUpdateReq;
    tWssWlanDeleteReq   WssWlanDeleteReq;
    }unWlanConfReq;
    tCapwapHdr          capwapHdr;
#ifdef WPS_WANTED
 tRadioIfInfoElement    RadioIfInfoElement;
#endif
 tVendorSpecPayload  vendSpec[VEND_CONF_MAX];
    UINT2               u2SessId;
    UINT2               u2CapwapMsgElemenLen;/* Length following the sequence num */
    UINT1               u1SeqNum;
    UINT1               u1NumMsgBlocks;
    UINT1               u1WlanOption;
    UINT1               u1Pad; 
}tWssWlanConfigReq;

/********************************************************/
/*        Configuation WLAN Response , Msg Type = 3398913            */
/********************************************************/
typedef struct {
    tCapwapHdr          capwapHdr;
    tWssWlanConfRsp     WssWlanConfRsp;
    tResCode            resultCode;
    tVendorSpecPayload  vendSpec;
    UINT4               u4SessId;
    UINT2               u2CapwapMsgElemenLen;/* Length following the sequence num */
    UINT1               u1SeqNum;
    UINT1               u1NumMsgBlocks;
}tWssWlanConfigRsp;

typedef struct{
    tCapwapHdr          capwapHdr;
    UINT4               sessionID[CAPWAP_JOIN_SESSIONID_SIZE];
    UINT2               u2CapwapMsgElemenLen;
    UINT1               au1Pad[2]; 
}tKeepAlivePkt;

typedef struct {
    tAcDescriptor       acDesc;
    UINT1               wlcName[CAPWAP_WLC_NAME_SIZE];
    tIpAddr             ipAddr; /* the IP address of the sender */
    /* tWtpRadInfo     wtpRadioInfo; */
    tRadioIfInfo        RadioIfInfo;
    tVendorSpecPayload  vendSpec;
    tIpAddr             incomingAddr;    
    tACIplist           acIPv4List; 
    UINT4               u4DestPort;
    UINT2               wtpCount;
    UINT1               u1Used;
    UINT1               u1CtrlDtlsStatus; 
} tACInfoAfterDiscovery;

/********************************************************/
/*        Ac Referral DataStructure                     */
/********************************************************/

typedef struct {
        tIpAddr         ipAddr;             /* Refered Ipaddress by AC */ 
        UINT1           u1Used;             /* Index is Used or Not */
        BOOL1           isDiscReqSent;      /* Is Capwap Disc Req Sent */
        BOOL1           isDiscReqRcd;       /* Is Capwap Disc Rsp Rcvd */
        UINT1           au1Pad[1]; 
} tACRefInfoAfterDiscovery;


enum {
/* Message types used for CAPWAP Receiver task */
    CAPWAP_DTLS_MSG =1,
    CAPWAP_CTRL_FRAG_MSG,
    CAPWAP_DISC_MSG,
    CAPWAP_FSM_MSG,
    CAPWAP_ECHO_ALIVE_MSG,
    CAPWAP_DATA_TX_MSG,
    WLCHDLR_DATA_RX_MSG,
    WLCHDLR_DATA_TX_MSG,
    WLCHDLR_CTRL_MSG,
    WLCHDLR_CFA_RX_MSG,
    APHDLR_CTRL_MSG,
    APHDLR_DATA_RX_MSG,
    APHDLR_CFA_RX_MSG,
    APHDLR_DATA_TX_MSG,
    CAPWAP_DATA_FRAG_MSG,
    APHDLR_NEIGHBOUR_AP_CTRL_MSG,
    APHDLR_CLIENT_SCAN_CTRL_MSG,
    WLCHDLR_DATA_RX_PROBE_MSG,
#ifdef WPS_WTP_WANTED    
    APHDLR_WPS_MSG,
#endif    
    WLCHDLR_RELEASE_LAST_TRANSMIT_MSG, 
    CAPWAP_ERR_MSG,
};

/* ------------------------------------------------------------------
 *                Global Information
 * This structure contains all the Global Data required for CAPWAP
 * Receiver task Operation .
 * ----------------------------------------------------------------- */

typedef struct RECV_GLOBAL_INFO{
    tOsixTaskId         capwapRecvTaskId;
    tOsixSemId          capwapReceiveTaskSemId;
    UINT1                au1TaskSemName[8];
    UINT1               u1IsCapwapInitialized;
    UINT1               au1Pad[3]; 
}tRecvTaskGlobals;


typedef struct {
    UINT4                  u4PktType;
    tCRU_BUF_CHAIN_HEADER *pRcvBuf;
    tIpAddr                capwapSessionId;   
}tCapwapRxQMsg;

typedef enum {
    SUPPORT_PRE_SHARED_KEY = 1,
    SUPPORT_X509_CERTIFICATE =2
}eSecurity;
#if 0
typedef enum {
 CAPWAP_START=0,
 CAPWAP_IDLE,
 CAPWAP_DISCOVERY,
  CAPWAP_DTLS,
/* CAPWAP_DTLSSETUP,*/
/* CAPWAP_DTLSCONNECT,*/
/* CAPWAP_AUTHORIZE, */
 CAPWAP_JOIN,
 CAPWAP_CONFIGURE,
 CAPWAP_DATACHECK,
/* CAPWAP_IMAGEDATA, */
 CAPWAP_RUN,
 CAPWAP_RESET,
 CAPWAP_SULKING,
 CAPWAP_DTLSTD,
 CAPWAP_DEAD,
 CAPWAP_MAX_STATE
} tCapwapState;
#endif 

typedef enum {
    MAC_DISCOVERY_MODE = 1,
    AUTO_DISCOVERY_MODE = 2 
}eDiscoveryMode;

typedef enum {
    DEVICE_WTP = 1,
    DEVICE_WLC = 2
}eNodeType;

typedef struct{
    /*CAPWAP Timer List ID */
    tTimerListId        capwapFsmTmrListId;
    tTmrDesc            aCapwapFsmTmrDesc[CAPWAP_MAX_FSM_TMR];

                             /* Timer data struct that contains
                              * func ptrs for timer handling and

                              * offsets to identify the data
                              * struct containing timer block.
                              * Timer ID is the index to this
                              * data structure */
    tTimerListId        capwapRunTmrListId;
    tTmrDesc            aCapwapRunTmrDesc[CAPWAP_MAX_RUN_TMR];

                             /* Timer data struct that contains
                              * func ptrs for timer handling and

                              * offsets to identify the data
                              * struct containing timer block.
                              * Timer ID is the index to this
                              * data structure */
}tCapwapTimerList;

/* ------------------------------------------------------------------
 *                Global Information
 * This structure contains all the Global Data required for CAPWAP
 * Service task Operation .
 * ----------------------------------------------------------------- */


typedef struct{
    tOsixTaskId         capwapSerTaskId;
    tOsixQId            ctrlRxMsgQId;             /* Ctrl Rx Queue */
    tOsixQId            ctrlFragRxMsgQId;         /* Ctrl Frag RX Queue */
    tOsixQId            echoAliveMsgQId;             /* Data Rx Queue */
    tOsixQId            dataTxMsgQId;                /* Data Tx Queue */
    tOsixQId            dataFragRxMsgQId;
    tTmrBlk             PMTUUpdateTmr;
    tOsixSemId          capwapConfSemId;
    tOsixSemId          capwapServiceTaskSemId;  /*Capwap Service Task Semaphore*/
    UINT1               au1TaskSemName[8];       /*Capwap Service Task Name*/
    UINT1               u1IsSerTaskInitialized;
    UINT1               au1Pad[3]; 
}tSerTaskGlobals;


/* ------------------------------------------------------------------
 *                Global Information
 * This structure contains all the Global Data required for CAPWAP
 * discovery task Operation .
 * ----------------------------------------------------------------- */

typedef struct {

    tOsixTaskId    capwapDiscTaskId;
    tOsixSemId     capwapDiscSemId;
    tOsixQId       discMsgQId;
    tTimerListId   capDiscTmrListId;
    tTmrDesc       aCapDiscTmrDesc[CAPWAP_MAX_DISC_TMR];
                             /* Timer data struct that contains

                              * func ptrs for timer handling and
                              * offsets to identify the data
                              * struct containing timer block.
                              * Timer ID is the index to this
                              * data structure */
    tTmrBlk        MaxDiscIntervalTmr;
    tTmrBlk        DiscIntervalTmr;
    tTmrBlk        SilentIntervalTmr;
    tTmrBlk        PrimaryDiscTmr;
    tTmrBlk        DhcpOfferTmr;
    UINT1          au1TaskSemName[8];
    UINT2          u2DiscCount;
    UINT1          u1IsDiscTaskInitialized;
    UINT1          au1Pad[1]; 
}tDiscTaskGloabls;

typedef struct {
    UINT4       ReqRxd;
    UINT4       RespSent;
    UINT4       UnsuccReqProcessed;
    UINT4       ReasonLastUnsuccAttempt;
    UINT4       LastSuccAttemptTime;
    UINT4       LastUnsuccAttemptTime;
} tCapSessStats;

typedef struct {
   tCapSessStats    CapDisc;
   tCapSessStats    CapJoin;
   tCapSessStats    CapConfig;
   tCapSessStats    CapEcho;
   tCapSessStats    CapKeeAlive;
   tCapSessStats    CapRun;
}tCapwapStats;

enum
{
    /* CAPWAP DATA Payload types */
    WSS_CAPWAP_802_11_MGMT_PKT,
    WSS_CAPWAP_802_11_DATA_PKT,
    WSS_CAPWAP_802_11_CTRL_PKT,
    WSS_CAPWAP_802_11_INVALID_PKT
};

enum
{
    /* Events processed by CAPWAP Module */
    WSS_CAPWAP_PARSE_REQ,
    WSS_CAPWAP_PARSE_MEMREL_REQ,
    WSS_CAPWAP_ASSEMBLE_WTP_EVENT_REQ,
    WSS_CAPWAP_ASSEMBLE_EVENT_RSP,
    WSS_CAPWAP_ASSEMBLE_CHANGESTATE_EVENT_REQ,
    WSS_CAPWAP_ASSEMBLE_CHANGESTATE_EVENT_RSP,
    WSS_CAPWAP_ASSEMBLE_WLAN_CONF_REQ,
    WSS_CAPWAP_ASSEMBLE_WLAN_CONF_RSP,
    WSS_CAPWAP_ASSEMBLE_CONF_UPDATE_REQ,
    WSS_CAPWAP_ASSEMBLE_CONF_UPDATE_RSP,
    WSS_CAPWAP_ASSEMBLE_STATION_CONF_REQ,
    WSS_CAPWAP_ASSEMBLE_SATION_CONF_RSP,
    WSS_CAPWAP_ASSEMBLE_RESET_REQ,
    WSS_CAPWAP_ASSEMBLE_RESET_RSP,
    WSS_CAPWAP_ASSEMBLE_PRIMARY_DISCOVERY_REQ,   
    WSS_CAPWAP_ASSEMBLE_PRIMARY_DISCOVERY_RSP,
    WSS_CAPWAP_ASSEMBLE_CLEARCONF_REQ,
    WSS_CAPWAP_ASSEMBLE_CLEARCONF_RSP,
    WSS_CAPWAP_ASSEMBLE_IMAGEDATA_REQ,
    WSS_CAPWAP_ASSEMBLE_IMAGEDATA_RSP,
    WSS_CAPWAP_ASSEMBLE_DATATRANSFER_REQ,
    WSS_CAPWAP_ASSEMBLE_DATATRANSFER_RSP,
    WSS_CAPWAP_TX_CTRL_PKT,
    WSS_CAPWAP_WTP_TX_DATA_PKT,
    WSS_CAPWAP_WLC_TX_DATA_PKT
};

typedef struct {
    tCapwapControlPacket   *pCapwapCtrlMsg;
    tCRU_BUF_CHAIN_HEADER  *pData;
}tCapwapParseReq;

typedef struct {
    UINT1               *pData;
    tWtpEveReq          *pCapwapCtrlMsg;
    UINT2               u2MsgLen;
    UINT1               au1Pad[2]; 
}tCapAssembleWtpEventReq;

typedef struct {
    UINT1               *pData;
    tWtpEveRsp          *pCapwapCtrlMsg;
    UINT2               u2MsgLen;
    UINT1               au1Pad[2]; 
}tCapAssembleWtpEventRsp;


typedef struct {
    UINT1               *pData;
    tChangeStateEvtReq  *pCapwapCtrlMsg;
    UINT2               u2MsgLen;
    UINT1               au1Pad[2]; 
}tCapAssembleChangeStateReq;

typedef struct {
    UINT1               *pData;
    tChangeStateEvtRsp  *pCapwapCtrlMsg;
    UINT2               u2MsgLen;
    UINT1               au1Pad[2]; 
}tCapAssembleChangeStateRsp;

typedef struct {     
    UINT1               *pData;     
    tPriDiscReq         *pCapwapCtrlMsg;     
    UINT2               u2MsgLen;     
    UINT1               au1Pad[2];     
}tCapAssemblePrimaryDiscReq;     
      
typedef struct {     
    UINT1               *pData;     
    tPriDiscRsp         *pCapwapCtrlMsg;     
    UINT2               u2MsgLen;     
    UINT1               au1Pad[2];     
}tCapAssemblePrimaryDiscRsp; 

typedef struct {
   tWtpStaticIpAddr         wtpStaticIpAddr;
}tCfaConfigUpdateReq;


typedef struct {
    UINT1                *pData;
    tStationConfReq      *pCapwapCtrlMsg;
    UINT2                u2MsgLen;
    UINT1                au1Pad[2]; 
}tCapAssembleStaConfReq;

typedef struct {
    UINT1                *pData;
    tStationConfRsp      *pCapwapCtrlMsg;
    UINT2                u2MsgLen;
    UINT1                au1Pad[2]; 
}tCapAssembleStaConfRsp;


typedef struct {
   UINT1                 *pData;
   tConfigUpdateReq      *pCapwapCtrlMsg;
   UINT2                 u2MsgLen;
   UINT1                 au1Pad[2]; 
}tCapAssembleConfUpdateReq;

typedef struct {
   UINT1                 *pData;
   tConfigUpdateRsp      *pCapwapCtrlMsg;
   UINT2                 u2MsgLen;
   UINT1                 au1Pad[2]; 
}tCapAssembleConfUpdateRsp;


typedef struct {
    UINT1                *pData;
    tWssWlanConfigReq    *pCapwapCtrlMsg;
    UINT2                u2MsgLen;
    UINT1                au1Pad[2];
}tCapAssembleWlanUpdateReq;

typedef struct {
    UINT1                *pData;
    tWssWlanConfigRsp    *pCapwapCtrlMsg;
    UINT2                u2MsgLen;
    UINT1                au1Pad[2];
}tCapAssembleWlanUpdateRsp;


typedef struct {
    tCRU_BUF_CHAIN_HEADER  *pData;
    UINT4                   u4pktLen;
    UINT4                   u4IpAddr;
    UINT4                   u4DestPort;
    UINT4                   u4MsgType;
    UINT2                   u2SessId;
    /* tRemoteSessionManager  *pSessEntry; */
    tMacAddr                BssIdMac;
    UINT1                   u1MacType;
    UINT1                   u1DscpValue;
    UINT1                   au1Pad[2];
}tCapwapTxPkt;

typedef struct {
    tResetReq           *pCapwapCtrlMsg;
        UINT1           *pData;
        UINT2           u2MsgLen;
        UINT1           au1Pad[2];
}tCapAssembleResetReq;

typedef struct {
    tResetRsp           *pCapwapCtrlMsg;
        UINT1           *pData;
        UINT2           u2MsgLen;
        UINT1           au1Pad[2];
}tCapAssembleResetRsp;

typedef struct {
    tClearconfigReq       *pCapwapCtrlMsg;
        UINT1             *pData;
        UINT2             u2MsgLen;
        UINT1             au1Pad[2];
}tCapAssembleClearConfReq;

typedef struct {
    tClearconfigRsp       *pCapwapCtrlMsg;
        UINT1             *pData;
        UINT2             u2MsgLen;
        UINT1             au1Pad[2];
}tCapAssembleClearConfRsp;

typedef struct {
    tImageDataReq       *pCapwapCtrlMsg;
        UINT1           *pData;
        UINT2           u2MsgLen;
        UINT1           au1Pad[2];
}tCapAssembleImageDataReq;

typedef struct {
    tImageDataRsp       *pCapwapCtrlMsg;
        UINT1           *pData;
        UINT2           u2MsgLen;
        UINT1           au1Pad[2];
}tCapAssembleImageDataRsp;


typedef struct {
        tDataReq         *pCapwapCtrlMsg;
        UINT1            *pData;
        UINT2            u2MsgLen;
        UINT1            au1Pad[2];
}tCapAssembleDataTransferReq;

typedef struct {
        tDataRsp         *pCapwapCtrlMsg;
        UINT1            *pData;
        UINT2            u2MsgLen;
        UINT1            au1Pad[2];
}tCapAssembleDataTransferRsp;

/* This union contains the message structures for all tWssMsgTypes */
typedef union {

    tCapwapParseReq                 CapwapParseReq;
    tCapAssembleWtpEventReq         CapAssembleWtpEventReq;
    tCapAssembleWtpEventRsp         CapAssembleWtpEventRsp;
    tCapAssembleChangeStateReq      CapAssembleChangeStateReq;
    tCapAssembleChangeStateRsp      CapAssembleChangeStateRsp;
    tCapAssembleStaConfReq          CapAssembleStaConfReq;
    tCapAssembleStaConfRsp          CapAssembleStaConfRsp;
    tCapAssembleConfUpdateReq       CapAssembleConfUpdateReq;
    tCapAssembleConfUpdateRsp       CapAssembleConfUpdateRsp;
    tCapAssembleWlanUpdateReq       CapAssembleWlanUpdateReq;
    tCapAssembleWlanUpdateRsp       CapAssembleWlanUpdateRsp;
    tCapAssembleResetReq            CapAssembleResetReq;
    tCapAssembleResetRsp            CapAssembleResetRsp;
    tCapAssembleClearConfReq        CapAssembleClearConfReq;
    tCapAssembleClearConfRsp        CapAssembleClearConfRsp;
    tCapAssembleImageDataReq        CapAssembleImageDataReq;
    tCapAssembleImageDataRsp        CapAssembleImageDataRsp;
    tCapAssembleDataTransferReq     CapAssembleDataTransferReq;
    tCapAssembleDataTransferRsp     CapAssembleDataTransferRsp;
    tCapAssemblePrimaryDiscReq      CapAssemblePrimaryDiscReq; 
    tCapAssemblePrimaryDiscRsp      CapAssemblePrimaryDiscRsp;
    tCapwapTxPkt                    CapwapTxPkt;
}unCapwapMsgStruct;


typedef struct CapwapDhcpDiscCbInfo
{
INT4 iDhcpOption;
INT4 (*pCapwapDhcDiscoveryCbFn) (UINT1, UINT4 *);
INT4 iTaskId;
INT4 iEventId;
}tCapwapDhcpDiscCbInfo;

typedef struct CapwapDhcpDiscV4AcList
{
INT4 iV4AcIpAddress[10];
INT1 iLength;
UINT1 au1Pad[3];
}tCapwapDhcpDiscV4AcList;

typedef struct CapwapDhcpDiscV6AcList
{
INT4 iV6AcIpAddress[20];
INT1 iLength;
UINT1 au1Pad[3];
}tCapwapDhcpDiscV6AcList;

typedef struct CapwapDhcpAcDiscList
{
tCapwapDhcpDiscV4AcList CapwapV4AcList;
UINT1 uCfgAcDiscMode;
UINT1 uPreAcDiscMode;
UINT1 uCurAcDiscMode;
UINT1 uOfferRcvd;
UINT1 uDhcpOfferTmrExp;
UINT1 au1Pad[3];
}tCapwapDhcpAcDiscList;


typedef struct CapwapDnsAcDiscList
{
INT4 iV4AcIpAddress[10];
UINT4 u4Length; 
UINT4 u4DnsRespRcvd;
UINT4 u4DnsMaxRetries; 
UINT4 u4DnsDiscCount; 
UINT1 au1DnsDomainName[64]; 
UINT4 u4DnsDomainLen; 
UINT2 u2AcDestPort;  
UINT1 au1Pad[2];
}tCapwapDnsAcDiscList;

typedef struct CapwapStaticAcDiscList
{
INT4 iV4AcIpAddress;
UINT1 wlcName[512];
UINT1 u1Used;
UINT1 u1Priority;
UINT1 au1Pad[2];
}tCapwapStaticAcDiscList;

/* Capwap NPAPI */
#define CUST_NP_ACL_LIST_ID1 15
#ifdef NPAPI_WANTED

#define CAPWAP_NP_TUNNEL_START_VLAN 1
#define CAPWAP_NP_MIN_UNICAST_TUNNEL 1
#define CAPWAP_NP_MAX_UNICAST_TUNNEL 512
#define CAPWAP_NP_ADDRESS_IPV4 4
#define CAPWAP_NP_ADDRESS_IPV6 16
#define CAPWAP_NP_POOL_PRIORITY 100
#define CAPWAP_NP_POOL_ID 2
#define CAPWAP_NP_LIST_ID 2
#define CAPWAP_NP_TUNNEL_START_VLAN 1
#define CAPWAP_NP_MAX_BSSID_INDEX 16

/* For Web Authentication */
#define CUST_NP_ACL_POOL_ID 6
#define CUST_NP_ACL_LIST_ID2 16
#define CUST_NP_ACL_LIST_ID3 17
#define CUST_NP_ACL_LIST_ID4 18
#define CUST_NP_ACL_LIST_ID5 19

typedef struct {
    UINT2 u2Afi;        /* Represents address family */
    UINT2 u2AddressLen; /* Represents the length of the prefix present e.g. for IPV4 = 4, IPV6 = 16 */
    tIpAddr ipAddr;
}tNetAddr;

typedef struct {
    tNetAddr remoteIpAddr;
    tNetAddr wlcIpAddr;
    tMacAddr wlcMacAddr;
    UINT1    au1Pad[2];
    UINT4    u4RadioMacIncl;
    UINT4    u4DestControlPort;
    UINT4    u4DestDataPort;
    UINT4    u4LocalControlPort;
    UINT4    u4LocalDataPort;
    UINT4    u4PMTU;
    UINT4    u4svid;
    UINT4    u4cvid;
    UINT4    u4TunnelId;
    UINT4    u4Ttl;
    UINT1    wtpTunnelMode;
    UINT1    transProto;
    UINT1    u1TunnelType;
    UINT1    u1TunnelMode;
    UINT1    u1VlanEnable;
    UINT1    u1StartProfile;
    UINT1    u1SvidOptions;
    UINT1    u1CvidOptions;
}tCapwapNpParams;

typedef struct {
    UINT4 radioId;
    UINT4 u4IfIndex;
    UINT2 u4TunnelId;
    tMacAddr bssIdMacAddr;
    tVlanId  wlanVlanId;
    UINT1    au1Pad[2];
}tWlanParams;

typedef struct {
    tMacAddr clientMacAddr;
    tVlanId  clientVlanId;
    tWlanParams *pWlanNpParams;
}tWlanClientParams;

typedef struct {
    UINT4 u4SipCIR;
    UINT4 u4SipCBS;
    UINT4 u4SipEIR;
    UINT4 u4SipEBS;
    UINT4 u4DipCIR;
    UINT4 u4DipCBS;
    UINT4 u4DipEIR;
    UINT4 u4DipEBS;
    UINT1 u1SipRateLimit;
    UINT1 u1DipRateLimit;
    UINT1 au1Pad[2];
}tWlanSSIDRateParam;

typedef struct {
    UINT4  u4VrfId;
    tNetAddr wlcIpAddr;
}tWlanHostIpv4Key;

/* Added for Web Authentication */
/* Type of rule to be added */
typedef enum{
    WSSSTA_INITIATE_POOL = 0,
    WSSSTA_DNS_FWD_RULE,
    WSSSTA_DHCP_FWD_RULE,
    WSSSTA_EXT_WEB_PORTAL_FWD_RULE,
    WSSSTA_TCP_CPU_RULE,
    WSSSTA_STA_MAC_FWD_RULE,
    WSSSTA_DEFAULT_DROP
}eWssRuleType;

/* CPU code type - for packets lifted to CPU */
typedef enum{
    WSSSTA_TCP_PKT_TO_CPU = 0
}eWssCpuCode;

typedef struct {
    tMacAddr BssId;
    UINT1 au1Pad[2];
    tMacAddr StaMacAddr;
    UINT1 au1Pad1[2];
    UINT4 ExtPortalIpAddr;
    UINT4 u4PolicyListId;
    UINT2 u2RuleId;
    UINT2 u2RuleNum;
    UINT2 u2TcpDPort;
    UINT1 u1RuleType;
    UINT1 u1RulePrio;
}tWlanClientRuleParams;
typedef struct {
    UINT4 u4Meter;
    UINT4 u4Sip;
    UINT4 u4CIR;
    UINT4 u4CBS;
    UINT4 u4EIR;
    UINT4 u4EBS;
}tWlanSSIDMeterParam;

INT4 capwapNpEnable (tCapwapNpParams *);
INT4 capwapNpDisable (VOID);
INT4 capwapNpCreateUcTunnel (tCapwapNpParams *);
INT4 capwapNpCreateBcTunnel (tCapwapNpParams *);
INT4 capwapNpCreateMcTunnel (tCapwapNpParams *);
INT4 capwapNpGetUnicastTunnelInfo (tCapwapNpParams *);
INT4 capwapNpGetMulticastTunnelInfo (tCapwapNpParams *);
INT4 capwapNpGetBroadcastTunnelInfo (tCapwapNpParams *);
INT4 capwapNpDeleteUcTunnel (tCapwapNpParams *);
INT4 capwapNpDeleteBcTunnel (VOID);
INT4 capwapNpDeleteMcTunnel (VOID);
INT4 capwapNpDeleteAllTunnel (VOID);
INT4 capwapNpWlanCreate (tWlanParams *);
INT4 capwapNpWlanGet (tWlanParams *);
INT4 capwapNpWlanDelete (tWlanParams *);
INT4 capwapNpWlanClientAdd (tWlanClientParams *);
INT4 capwapNpWlanClientGet (tWlanClientParams *);
INT4 capwapNpWlanClientDelete (tWlanClientParams *);
INT4 capwapNpWlanSSIDSipRateSet (tWlanHostIpv4Key *pWlanClientKey, tWlanSSIDRateParam *pWlanClientRateParam, UINT4 *u4MeterId);
INT4 capwapNpWlanSSIDDipRateSet (tWlanHostIpv4Key *pWlanClientKey, tWlanSSIDRateParam *pWlanClientRateParam, UINT4 u4MeterId);
INT4 capwapNpCreateClientRule (tWlanClientRuleParams *);
INT4 capwapNpDeleteClientRule (tWlanClientRuleParams *);
INT4 capwapNpUpdateTunnelPmtu (tCapwapNpParams *ptCapwapNpParams);

#endif
INT4 CapwapSetAcIPAddressFromDns (UINT4 ,UINT1 *,UINT4);
INT4 CapwapDnsSrvParamsFromDHCP(UINT4 , UINT1 *, UINT4 );
INT4 WssIfProcessCapwapMsg  (UINT1, unCapwapMsgStruct *);

UINT1
CapwapSendWebAuthStatus (tMacAddr StationMacAddr, UINT1  u1Status,
                UINT2 u2WtpInternalId);

#endif

