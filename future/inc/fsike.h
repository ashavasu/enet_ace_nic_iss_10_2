/**********************************************************************
 * Copyright (C) Future Software Limited, 2002
 *
 * $Id: fsike.h,v 1.25 2014/03/07 11:59:11 siva Exp $
 *
 * Description: This has interface macros and prototypes for ike. 
 *
 ***********************************************************************/

#ifndef _FS_IKE_H_
#define _FS_IKE_H_

#include "lr.h"
#include "ip.h"
#include "ipv6.h"
#include "fips.h"
#include "cust.h"

/* IKE UDP PORT Number */
#define IKE_UDP_PORT         500

/* IKE NATT UDP PORT Number */
#define IKE_NATT_UDP_PORT     4500
#define IKE_PORT_LEN             2
#define IKE_USE_NATT_PORT        1
#define IKE_BEHIND_NAT           2
#define IKE_PEER_BEHIND_NAT      4
#define IKE_PEER_SUPPORT_NATT    8

/* Constants for Ike functions */
#define IKE_SUCCESS              0
#define IKE_FAILURE             -1

#define IKE_DEF_USER_DIR       "./"

/* Constants for IKE/IPSEC KERNEL functions */
#define IOCTL_SUCCESS            0

/* Cmd type used by cfa2 for processing the message from ipsec to ike */  
#define   IKE_IPSEC_MESSAGE      13


/* IKE Encr Algo : RFC 2409 */
#define IKE_DES_CBC               1
#define IKE_3DES_CBC              5
#define IKE_AES                   7

/* IKE Hash Algo: RFC 2409 */
#define IKE_MD5                   1
#define IKE_SHA1                  2
#define IKE_SHA224                3
#define IKE_SHA256                4
#define IKE_SHA384                5
#define IKE_SHA512                6

#define IKE_SIGN_ALGO_MD5         (UINT1 *)"md5WithRSAEncryption"
#define IKE_SIGN_ALGO_SHA1        (UINT1 *)"sha1WithRSAEncryption"
#define IKE_SIGN_ALGO_SHA224      (UINT1 *)"sha224WithRSAEncryption"
#define IKE_SIGN_ALGO_SHA256      (UINT1 *)"sha256WithRSAEncryption"
#define IKE_SIGN_ALGO_SHA384      (UINT1 *)"sha384WithRSAEncryption"
#define IKE_SIGN_ALGO_SHA512      (UINT1 *)"sha512WithRSAEncryption"

/* IPSec mode */
#define IKE_IPSEC_TUNNEL_MODE        1
#define IKE_IPSEC_TRANSPORT_MODE     2
#define IKE_IPSEC_UDP_TUNNEL_MODE    3
#define IKE_IPSEC_UDP_TRANSPORT_MODE 4

/* AH Transform Id */
#define IKE_IPSEC_AH_MD5         2
#define IKE_IPSEC_AH_SHA1        3

/* Esp Transform Id */
#define IKE_IPSEC_ESP_DES_CBC     2
#define IKE_IPSEC_ESP_3DES_CBC    3
#define IKE_IPSEC_ESP_AES        12
#define IKE_IPSEC_ESP_AES_CTR    13
#define IKE_IPSEC_ESP_NULL       11

/* Esp Hash Algo */
#define IKE_IPSEC_HMAC_MD5        1
#define IKE_IPSEC_HMAC_SHA1       2


/*AES default key length*/
#define IKE_AES_DEF_KEY_LEN_VAL    192
#define IKE_AES_KEY_LEN_128        128
#define IKE_AES_KEY_LEN_256        256

#define IPSEC_MANUAL_KEY             0 /* arbitrary value */

/* IKE Authentication methods */
#define IKE_PRESHARED_KEY            1
#define IKE_DSS_SIGNATURE            2
#define IKE_RSA_SIGNATURE            3
#define IKE_RSA_ENCRYPTION           4
#define IKE_REVISED_RSA_ENCRYPTION   5



/* IPSec ID types */
#define IKE_IPSEC_ID_IPV4_ADDR      1
#define IKE_IPSEC_ID_FQDN           2
#define IKE_IPSEC_ID_USER_FQDN      3
#define IKE_IPSEC_ID_IPV4_SUBNET    4
#define IKE_IPSEC_ID_IPV6_ADDR      5
#define IKE_IPSEC_ID_IPV6_SUBNET    6
#define IKE_IPSEC_ID_IPV4_RANGE     7
#define IKE_IPSEC_ID_IPV6_RANGE     8
#define IKE_IPSEC_ID_DER_ASN1_DN    9
#define IKE_IPSEC_ID_KEY_ID        11

/* Ike queue identifier */
#define IKE_TASK_NAME         (const UINT1 *)"ike"
#define IKE_QUEUE_NAME        (const UINT1 *)"IKEQ"
#define IKE_GLOBAL_SEM_NAME   (const UINT1 *)"IKES"

/* IKE Socket Select Task related */
#define    IKE_SOCKET_SELECT_TASK_NAME      (CONST UINT1 *) "ikes"
#define    IKE_SELECT_TASK_PRIORITY         80 
#define    IKE_SELECT_TASK_MODE             OSIX_DEFAULT_TASK_MODE

#define TAKE_IKE_SEM() IkeLock()
#define GIVE_IKE_SEM() IkeUnLock()
#define IKE_LOCK()     IkeLock()
#define IKE_UNLOCK()   IkeUnLock()

/* Constants for Ike events */
#define IKE_TIMER0_EVENT        0x00000001
#define IKE_INPUT_Q_EVENT       0x00000010
#define IKE_CFG_WAIT_EVENT      0x00000100
   
/* Constants for MsgType in the IPSecIfParam structure */
#define IKE_IPSEC_INSTALL_SA              1
#define IKE_IPSEC_DUPLICATE_SPI_REQ       2
#define IKE_IPSEC_PROCESS_DELETE_SA       3
#define IKE_IPSEC_PROCESS_DEL_SA_BY_ADDR  4 
#define  IKE_IPSEC_PROCESS_DEL_SA_FOR_LOCAL_CONF_CHG 5
#define IKE_IPSEC_PROCESS_ADD_TIME_DEL_SA  6 

#define IKE_IPSEC_INSTALL_DYNAMIC_IPSEC_DB      5 /* VPNC ADD */

/* Constants for MsgType in the ConfigIfParam structure */
#define IKE_CONFIG_CHG_POLICY             1
#define IKE_CONFIG_DEL_POLICY             2
#define IKE_CONFIG_CHG_TUNNEL_TERM_ADDR   3
#define IKE_CONFIG_CHG_PRESHARED_KEY      4
#define IKE_CONFIG_DEL_PRESHARED_KEY      5
#define IKE_CONFIG_CHG_CRYPTO_MAP         6
#define IKE_CONFIG_DEL_CRYPTO_MAP         7
#define IKE_CONFIG_CHG_TRANSFORM_SET      8
#define IKE_CONFIG_DEL_TRANSFORM_SET      9
#define IKE_CONFIG_NEW_ENGINE             10
#define IKE_CONFIG_START_ENGINE_LISTENER  11
#define IKE_CONFIG_DEL_ENGINE             12
#define IKE_CONFIG_DELETE_MYCERT          13
#define IKE_CONFIG_DEL_PEER_CERTIFICATE   14
#define IKE_CONFIG_DEL_CACERT             15
#define IKE_CONFIG_DEL_CERT_MAP           16
#define IKE_CONFIG_CHG_RA_INFO            17   
#define IKE_CONFIG_DEL_RA_INFO            18   
#define IKE_CONFIG_CHG_PROTECT_NET        19  
#define IKE_CONFIG_DEL_PROTECT_NET        20  

    
#define IKE_CONFIG_NEW_VPN_POLICY         21   
#define IKE_CONFIG_DEL_VPN_POLICY         22   
#define IKE_CONFIG_DELETE_SA              23   

#define IKE_CONFIG_P1_POLICY_CHG          24
#define IKE_CONFIG_P1_DEL_POLICY          25
#define IKE_CONFIG_P1_ALL_POLICY_CHG      26
#define IKE_CONFIG_P1_DEL_ALL_POLICY      27
   
#define IKE_CONFIG_DEL_TRANSFORM_SET_SPECIFIC      28

#define IKE_VPN_POLICY_NO_ATTR_SET          0x00
#define IKE_VPN_POLICY_ALL_ATTR_SET         0xFF
#define IKE_VPN_POLICY_KEY_MODE_SET         0x01
#define IKE_VPN_POLICY_REMOTE_PEER_SET      0x02
#define IKE_VPN_POLICY_IKE_PROPOSAL_SET     0x04
#define IKE_VPN_POLICY_IPSEC_PROPOSAL_SET   0x08
#define IKE_VPN_POLICY_LOCAL_REMOTE_NW_SET  0x10
   
#define IKE_POLICY_PEER_ID_SET              0x01   
#define IKE_POLICY_LOCAL_ID_SET             0x01   

/* Constants for MsgType in the IkeQueue structure */
#define IPSEC_IF_MSG                  1
#define CONFIG_IF_MSG                 2
#define L2TP_IF_MSG                   3
#define IKE_PKT_ARRIVAL               4

/* Maximum number of SPI that can come in the delete notify */
#define IKE_MAX_NUM_SPI               10

/* Remote gateway for XAUTH */
#define CLI_IKE_VP_GW_ANY        "any"

/* Maximum length of the key in the IPSec SA */
#define IKE_MAX_KEY_LEN     64
/* Length of Nonce of AES-CTR algorithm*/
#define IKE_NONCE_LEN   4

/* Constants for u4MsgType in the structure tIkeIPSecQMsg */
#define IKE_IPSEC_NEW_KEY                 1
#define IKE_IPSEC_REKEY                   2
#define IKE_IPSEC_INVALID_SPI             3
#define IKE_IPSEC_SEND_DELETE_SA          4
#define IKE_IPSEC_DUPLICATE_SPI_REPLY     5

/* Constants for u4MsgType in the structure tIkeL2TPIfParam */
 #define IKE_L2TP_DELETE_SA             1 

#define IKE_DISABLE                     0
#define IKE_ENABLE                      1
#define IKE_INIT_SHUT                   2
#define IKE_MGMT                        3
#define IKE_DATA_PATH                   4
#define IKE_CNTRL_PATH                  5
#define IKE_DUMP                        6
#define IKE_OS_RESOURCE                 7
#define IKE_ALL_FAILURE                 8
#define IKE_BUFFER                      9

#define IKE_BACKUP_ENGINE     "wan2"
#define IKE_WLAN_ENGINE       "wlan"

/* Default engine is a dummy engine just used for socket listening 
 * and certificate storage */
#define IKE_DUMMY_ENGINE      "engine0"
#define IKE_DEFAULT_ENGINE    IKE_DUMMY_ENGINE
#define IKE_ENGINE_NAME       "engine" 

#define IKE_GET_ENGINE_NAME(u4EngineEntryIndex) \
    gaIkeEngToIntMap[u4EngineEntryIndex].au1EngineName
#define IKE_GET_ENGINE_INDEX(u4EngineEntryIndex) \
    gaIkeEngToIntMap[u4EngineEntryIndex].u4EngIndex
#define IKE_GET_ENGINE_IFINDEX(u4EngineEntryIndex) \
    gaIkeEngToIntMap[u4EngineEntryIndex].u4IfIndex

/* VPN POLICY - XAUTH CONFIG */
#define VPN_XAUTH_DISABLE              0
#define VPN_XAUTH_ENABLE               1

#define IKE_XAUTH_GENERIC        0

/* IKE Remote Access Info  macros */
#define IKE_XAUTH_SERVER         1
#define IKE_XAUTH_CLIENT         2
#define IKE_CM_SERVER            1
#define IKE_CM_CLIENT            2
#define IKE_XAUTH_ENABLE         1
#define IKE_XAUTH_DISABLE        2
#define IKE_CM_ENABLE            1
#define IKE_CM_DISABLE           2
#define IKE_IPADDR_ALLOC_STATIC  1  
#define IKE_IPADDR_ALLOC_POOL    2

/* IKE Higher Layer Protocol */
#define IKE_PROTO_TCP            6
#define IKE_PROTO_UDP            17
#define IKE_PROTO_ICMP           1
#define IKE_PROTO_ANY            0

#define IKE_DEFAULT_ENGINE_NAME IKE_DEFAULT_ENGINE
        


#define IKE_MAX_ADDR_STR_LEN 256     /* Display string Maximum value */ 

#define INET_PTON4(src,dst)          INET_PTON (AF_INET, (const char *)src, \
                                                (void *)dst)
#define INET_NTOP4(src,dst)          INET_NTOP (AF_INET, (const void *)src, \
                                                (char *)dst, INET_ADDRSTRLEN)

#define INET_PTON6(src,dst)          INET_PTON (AF_INET6, (const char *)src, \
                                                (void *)dst)
#define INET_NTOP6(src,dst)          INET_NTOP (AF_INET6, (const void *)src, \
                                                (char *)dst, INET6_ADDRSTRLEN)

/*
 * The following structure definitions are to support the different 
 * types of local and remote networks that can be supported in the policy
 */
#define   tIp4Addr UINT4
typedef struct IKEIPV4SUBNET {
    tIp4Addr    Ip4Addr;
    tIp4Addr    Ip4SubnetMask;
} tIkeIpv4Subnet;

typedef struct IKEIPV4RANGE {
    tIp4Addr    Ip4StartAddr;
    tIp4Addr    Ip4EndAddr;
} tIkeIpv4Range;

typedef struct IKEIPV6SUBNET {
    tIp6Addr    Ip6Addr;
    tIp6Addr    Ip6SubnetMask;
} tIkeIpv6Subnet;

typedef struct IKEIPV6RANGE {
    tIp6Addr    Ip6StartAddr;
    tIp6Addr    Ip6EndAddr;
} tIkeIpv6Range;

typedef struct IKENETWORK {
    UINT4    u4Type;
    union
    {
        tIp4Addr       Ip4Addr;
        tIkeIpv4Subnet Ip4Subnet;
        tIkeIpv4Range  Ip4Range;
        tIp6Addr       Ip6Addr;
        tIkeIpv6Subnet Ip6Subnet;
        tIkeIpv6Range  Ip6Range;
    } uNetwork;
    
    UINT2     u2StartPort;  /* Start Port of the Range*/
    UINT2     u2EndPort;    /* End Port of the Range*/
    UINT1     u1HLProtocol; /* IP Protocol (TCP/UDP/ICMP) */
    UINT1     au1Pad[3];
} tIkeNetwork;

/* Table Referred for Converting V6 Prefix to Mask */
extern UINT1               gaIp6PrefixToMask[][IP6_ADDR_SIZE];

/* Structure which supports both IPv4 and IPv6 address formats */
typedef struct IKEIPADDR {
    UINT4 u4AddrType;
    union
    {
        tIp4Addr Ip4Addr;
        tIp6Addr Ip6Addr;
    } uIpAddr;

#define Ipv4Addr uIpAddr.Ip4Addr
#define Ipv6Addr uIpAddr.Ip6Addr
} tIkeIpAddr;

#define MAX_NAME_LENGTH                    64
typedef struct IkePhase1ID {
    INT4    i4IdType;
    UINT4   u4Length;
    union
    {
        tIp4Addr Ip4Addr;
        tIp6Addr Ip6Addr;
        UINT1    au1Email[MAX_NAME_LENGTH];   
        UINT1    au1Fqdn[MAX_NAME_LENGTH]; /* Fully qualified domain 
                                              name */
        UINT1    au1Dn[MAX_NAME_LENGTH];   /* X.500 Distinguished Name */ 
        UINT1    au1KeyId[MAX_NAME_LENGTH];   /* random Key Id  */ 
    } uID;
} tIkePhase1ID;

typedef struct IKENATT
{
    INT2       i2NattSocketFd;
    UINT2      u2RemotePort;
    UINT1      u1NattFlags;
    UINT1      au1Padding[3];
}tIkeNatT;


/* Structure for holding ipsec SA information */
typedef struct IPSECSA {
    tIkeNatT IkeNattInfo;       /* NAT-T Info */
    UINT1 au1AuthKey[IKE_MAX_KEY_LEN];
    UINT1 au1EncrKey[IKE_MAX_KEY_LEN];
    UINT1 au1Nonce[IKE_NONCE_LEN];
    UINT4 u4LifeTime;
    UINT4 u4LifeTimeKB;
    UINT4 u4Spi;
    UINT2 u2HLProtocol;
    UINT2 u2HLPortNumber;
    UINT2 u2AuthKeyLen;
    UINT2 u2EncrKeyLen;
    UINT1 u1IPSecProtocol;
    UINT1 u1Mode;
    UINT1 u1HashAlgo;
    UINT1 u1EncrAlgo;
    UINT1 u1IkeVer;       /* Ike Version */
    UINT1 au1AlignBytes[3];  /* Pad bytes to align 4-byte word boundary */
} tIPSecSA;

/* Structure to be passed to IPSec */
typedef struct IPSECBUNDLE {
    tIPSecSA    aInSABundle[2];    /* Inbound SA's [0]->ESP [1]->AH        */
    tIPSecSA    aOutSABundle[2];   /* Outbound SA's [0]->ESP [1]->AH       */
    tIkeNetwork LocalProxy;        /* Proxy Source Address                 */
    tIkeNetwork RemoteProxy;       /* Proxy Dst Address                    */
    tIkeIpAddr  LocTunnTermAddr;   /* The Local Tunnel Termination address */
    tIkeIpAddr  RemTunnTermAddr;   /* The Remote Tunnel Termination address*/
    UINT4       u4VpnIfIndex;      /* Vpnc Interface index  */
    BOOLEAN     bVpnServer;        /* VPN mode - client or server */
    BOOLEAN     bInstallDynamicIPSecDB; /* Flag used by IPSec to install only 
                                           SAs or all (SA, Policy, ACLs and 
                                           Selectors */
    UINT1       u1IkeVersion;      /* IKE Version */
    UINT1       u1Pad;       /* Pad bytes to align 4-byte word boundary */
} tIPSecBundle;

/* Structure to be used for new key and rekey */
typedef struct IKENEWSA
{
    tIkeNetwork   SrcNet;         /* Src Network of the outbound packet   */
    tIkeNetwork   DestNet;        /* Dest Network of the outbound packet  */
    tIkeIpAddr    LocalTEAddr;    /* Address of the LocalTunnel 
                                     Termination. This is the address of 
                                     the interface on which the local 
                                     peer sends out the outbound 
                                     packets                              */

    tIkeIpAddr    RemoteTEAddr;    /* Address of the Remote Tunnel 
                                     Termination. This is the address of 
                                     the interface on which the local 
                                     peer recieves the inboun  
                                     packets                              */
    UINT4         u4ReKeyingSpi;   /* Inbound SPI which is being rekeyed */
    UINT2         u2PortNumber;   /* The destination Port of the Higher 
                                     layer Protocols (TCP/UDP)            */
    UINT1         u1Protocol;     /* Refers to IP Higher Layer Protocol   */
    BOOLEAN       bRekey;         /* TRUE if rekey else FALSE             */
    UINT1         u1IkeVersion;
    UINT1         au1Padding[3];
}tIkeNewSA;

/* Structure for holding invalid spi information */
typedef struct INVALIDSPI
{
    tIkeIpAddr   LocalAddr;    /* Local tunnel termination address  */
    tIkeIpAddr   RemoteAddr;   /* Remote tunnel termination address */
    UINT4        u4Spi;        /* Invalid Spi                       */
    UINT1        u1Protocol;   /* Protocol AH/ESP                   */
    UINT1        u1IkeVer;       /* Ike Version */
    UINT2        u2Alignment;  /* Added for alignment Purpose       */
    }tIkeInvalidSpi;

/* Structure for holding duplicate spi information */
typedef struct DUPLICATESPI 
{
    tIkeIpAddr   LocalAddr;     /* Local tunnel termination address   */
    tIkeIpAddr   RemoteAddr;    /* Remote tunnel termination address  */
    UINT4        u4EspSpi;      /* Recieved Spi                       */
    UINT4        u4AhSpi;       /* Recieved Spi                       */
    FS_ULONG     u4Context;     /* Recieved from Ike                  */
    BOOLEAN      bDupSpi;       /* TRUE if Duplicate, FALSE otherwise */
    UINT1        u1IkeVersion;
    UINT2        u2Alignment;   /* Added for alignment Purpose        */
}tIkeDupSpi;


/* 
 * This structure is used by ipsec and ike to post messages for 
 * SPI delete request and process request 
 */
typedef struct DELETESPI 
{
   tIkeIpAddr    LocalAddr;      /* Local tunnel termination address  */
   tIkeIpAddr    RemoteAddr;     /* Remote tunnel termination address */
   UINT4         au4SPI[IKE_MAX_NUM_SPI]; /* SPI values to be deleted */
   UINT2         u2NumSPI;       /* Number of SPI to be deleted       */
   UINT1         u1Protocol;     /* Protocol for which this is sent   */
   BOOLEAN         bDelDynIPSecDB; /* Flag used by IPSec to delete only 
                                        SAs or all (SA, Policy, ACLs and 
                                        Selectors */
   UINT1         u1IkeVersion;
   UINT1         au1Padding[3];
}tIkeDelSpi;

/* 
 * This structure is used by IPSec to delete the SA's established 
 * with the RemoteAddr 
 */ 
typedef struct DELSAFROMPEERADDR 
{
   tIkeIpAddr    LocalAddr;      /* Local tunnel termination address  */ 
   tIkeIpAddr    RemoteAddr;     /* Remote tunnel termination address */
   UINT1         u1IkeVersion;       /* Ike Version */
   UINT1         au1Pad [3];
}tIkeDelSAByAddr;

/* 
 * Structure used for sending/receiving information between 
 * IKE & IPSec 
 */
typedef struct IKEIPSECPARAM { 
    UINT4 u4MsgType; 
    union { 
        tIPSecBundle       InstallSA; /* Info for Storing SA in IPSec          */
        tIkeNewSA          SaTriggerInfo; /* Info for ike newkey and rekey     */
        tIkeInvalidSpi     InvalidSpiInfo; /* Info for invalid spi message     */
        tIkeDupSpi         DupSpiInfo;     /* Info for sending duplicate spi   */
        tIkeDelSpi         DelSpiInfo;     /* Info for sending delete spi      */
        tIkeDelSAByAddr    DelSAInfo;      /* Info for sending initial contact */
    }IPSecReq;
}tIkeIPSecIfParam;

/* Structures used by CLI */
typedef struct IKECONFENGINETUNNTERMADDR {
    UINT4 u4IkeEngineIndex;
    tIkeIpAddr LocTunnTermAddr;
} tIkeConfEngineTunnTermAddr;

typedef struct IKECONFPOLICY {
    UINT4 u4IkeEngineIndex;
    tIkePhase1ID PeerId;
} tIkeConfPolicy;

typedef struct IKECONFPRESHAREDKEY {
    UINT4 u4IkeEngineIndex;
    tIkePhase1ID KeyID;
} tIkeConfPreSharedKey;

typedef struct IKECONFCRYPTOMAP {
    UINT4 u4IkeEngineIndex;
    tIkeIpAddr PeerIpAddr;
} tIkeConfCryptoMap;

typedef struct IKECONFRA {
    UINT4 u4IkeEngineIndex;
    tIkeIpAddr PeerIpAddr;
} tIkeConfRA;
typedef struct IKECONFRAPROTECTNET {
    UINT4 u4IkeEngineIndex;
    tIkeIpAddr PeerIpAddr;
} tIkeConfRAProtectNet;


typedef struct IKECONFCREATEENGINE {
    UINT1         au1EngineName[MAX_NAME_LENGTH]; 
} tIkeConfCreateEngine;

typedef struct IKECONFCREATEVPNPOLICY {
    UINT1         au1VpnPolicyName[MAX_NAME_LENGTH]; 
} tIkeConfCreateVpnPolicy;

typedef struct IKECONFCERTINFO {
    UINT4         u4IkeEngineIndex;
    UINT1         au1CertId[MAX_NAME_LENGTH];
} tIkeConfCertInfo;

typedef struct IKEDELCERTINFO {
    UINT4         u4IkeEngineIndex;
    tIkeIpAddr    PeerIpAddr;
} tIkeDelCertInfo;

typedef struct IKEDELPEERCERTINFO {
    UINT4         u4IkeEngineIndex;
    tIkePhase1ID   Phase1ID;
    BOOLEAN        bDynamic;
    UINT1          au1Pad[3];
} tIkeDelPeerCertInfo;

typedef struct IKEMAPCERTINFO {
    UINT4         u4IkeEngineIndex;
    UINT1         au1CertId[MAX_NAME_LENGTH];
    tIkePhase1ID  Phase1ID;
} tIkeMapCertInfo;

typedef tIkeConfCertInfo tIkeDelCaCertInfo;

typedef struct IKECONFIGPARAM {
    UINT4 u4MsgType;
    union {
        UINT4                      u4IkeEngineIndex;
        tIkeConfEngineTunnTermAddr IkeConfEngineTunnTermAddr;
        tIkeConfPreSharedKey       IkeConfPreSharedkey;
        tIkeConfCryptoMap          IkeConfCryptoMap;
        tIkeConfCreateEngine       IkeConfCreateEngine;
        tIkeConfCertInfo           IkeConfCertInfo;
        tIkeDelCertInfo            IkeDelCertInfo;
        tIkeDelPeerCertInfo        IkeDelPeerCertInfo;
        tIkeDelCaCertInfo          IkeDelCaCertInfo;
        tIkeMapCertInfo            IkeMapCertInfo;
        tIkeConfRA                 IkeConfRA;
        tIkeConfRAProtectNet       IkeConfRAProtectNet;
        UINT4                      u4VpnPolicyIndex;
        tIkeConfCreateVpnPolicy    IkeConfCreateVpnPolicy;
        tIkeDelSAByAddr            DelSAByAddr; /* Deleting Phase2 SA */
        tIkeConfPolicy             IkeConfPolicy;
    } ConfigReq;
} tIkeConfigIfParam;

typedef struct  IKEL2TPIFPARAM {
    UINT4 u4MsgType;
    UINT1 u1L2TPSysMode;
    UINT1 au1Pad[3];
    tIkeDelSAByAddr  DelSAByAddr; /* Info for deleting Phase-1 and 
         Phase-2 SA */
}tIkeL2TPIfParam; 

typedef struct IKEPKTBUFF {
    tIkeIpAddr  PeerAddr;             /* IP address of the Peer */
    UINT1       *pu1PktBuffer;        /* Pointer to the Pkt Buffer */
    UINT4       u4PktLen;             /* Length of the Recieved buffer  */
    UINT4       u4IkeEngineIndex;
    UINT2       u2RemotePort;         /* Remote Peer Port */
    UINT2       u2LocalPort;          /* Port on which Connection is received */
}tIkePktBuffer;

/* This structure is used to post message to IKE. Currently IPv4 and
 * IPv6 post message to IKE Queue
 */
typedef struct IKEQMSG{
    UINT4 u4MsgType;                /* To identify the task from which 
                                       the event was received */
    union
    {
        tIkeIPSecIfParam IkeIPSecParam; /* Structure to hold IPSec Q Message */
        tIkeConfigIfParam IkeConfigParam; /* Structure to hold Mesg 
                                             from CLI/SNMP */
        tIkePktBuffer     IkePktBuffer;   /* Structure to hold the IKE
                                             packet received */
 tIkeL2TPIfParam  IkeL2TPIfParam; /* Structure to hold L2TP Q Message */
    }IkeIfParam;
    UINT4 u4TaskId; 
    UINT1 u1IkeVer;
    UINT1 au1Pad[3];
}tIkeQMsg;

/* Structure used by IPSec to get Ike message from IPv4/IPv6 queue */
typedef struct IKEIPSECQMSG{
    UINT4 u4MsgType;

    union
    {
        tIPSecBundle   InstallSA;    /* Information for Storing SA in IPSec */
        tIkeDupSpi     DupSpiInfo;   /* Information for sending duplicate spi*/
        tIkeDelSpi     DelSpiInfo;   /* Information for sending delete spi */
        tIkeDelSAByAddr DelSAInfo;   /* Information for sending initialcontact */
    }IkeIfParam;
}tIkeIPSecQMsg;

/* Structure to hold the required data in the ioctl calls */
typedef struct iface
{
    UINT1  *pBuf;
    UINT4   wLen;
}tIkeIoctlRW;


/* Ike callback entries */
typedef enum IkeCallBackEvents
{
    IKE_WRITE_PRE_SHARED_KEY_TO_NVRAM = 1,
    IKE_ACCESS_SECURE_MEMORY,
    IKE_MAX_CALLBACK_EVENTS
}eIkeCallBackEvents;

/*Mulitple Engine Database */
typedef struct IKEENGTOINTMAP
{
    UINT4 u4IfIndex;
    UINT4 u4EngIndex;
    UINT1 au1EngineName[MAX_NAME_LENGTH];
}tIkeEngToIntMap;

#define IKE_MAX_ENGINE SYS_DEF_MAX_PHYSICAL_INTERFACES

PUBLIC tIkeEngToIntMap     gaIkeEngToIntMap[IKE_MAX_ENGINE];
PUBLIC UINT4               gu4NumIkeEngines;


UINT1 * IkeGetEngNameFromIfIndex PROTO ((UINT4 u4IntIndex));

PUBLIC UINT1 gu1IkeBypassCrypto;

INT4 IkeDeleteAllKeys PROTO ((VOID));
INT4 IkeWritePreSharedKeyToNVRAM PROTO ((VOID));
INT4 IkeUtilRegCallBackforIKEModule (UINT4 , tFsCbInfo * ); 

INT4 IkeUtilCallBack PROTO((UINT4 u4Event, ...));

/* Exported Function Prototypes */
VOID IkeHandlePktFromIpsec       PROTO ((tIkeQMsg *pMsg));
VOID IkeHandlePktFromL2TP        PROTO ((tIkeQMsg *pMsg));
VOID Secv4ProcessIKEMsg          PROTO ((tIkeIPSecQMsg *pMsg));
#ifdef IPSECv6_WANTED
VOID Secv6ProcessIKEMsg          PROTO ((tIkeIPSecQMsg *pMsg));
#endif
UINT4 IkeTaskMain                PROTO ((VOID));
VOID IkeProcessKernelIpsecRequest PROTO ((VOID *));

#ifdef FUTURE_SNMP_WANTED
INT4 RegisterFSIKEwithFutureSNMP PROTO ((VOID));
#endif

INT1 IkeGetBypassCapability PROTO ((VOID));
INT1 IkeSetBypassCapability PROTO ((UINT1 u1BypassStatus));
INT1 IkeSetIkeTrcLevel PROTO ((UINT4 u4IkeTrcLevel));
#endif
