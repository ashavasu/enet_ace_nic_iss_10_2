/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: cfanpwr.h,v 1.20 2018/02/02 09:47:31 siva Exp $
 *
 * Description:This file contains the necessary data strucutures
 *             for packet handling in NPAPI layer
 *             <Part of dual node stacking model>
 *
 *******************************************************************/

#ifndef __CFA_NP_WR_H__
#define __CFA_NP_WR_H__

#include "cfa.h"
#include "cfanp.h"
#include "ethernp.h"
#include "ipnp.h"
#include "rportnp.h"
#include "ip6np.h"
#include "ip6mcnp.h"
#ifdef MBSM_WANTED
#include "mbsm.h"
#endif /* MBSM_WANTED */

/* when new NP call is added in the CFA, the following files 
 * has to be updated 
 * 1. npgensup.h 
 * 2. npbcmsup.h
*/
/* OPER ID */
enum
{
    CFA_NP_GET_LINK_STATUS                                =1, 
    CFA_REGISTER_WITH_NP_DRV                              , 
    FS_CFA_HW_CLEAR_STATS                                 , 
    FS_CFA_HW_GET_IF_FLOW_CONTROL                         , 
    FS_CFA_HW_GET_MTU                                     , 
    FS_CFA_HW_SET_MAC_ADDR                                , 
    FS_CFA_HW_SET_MTU                                     , 
    FS_CFA_HW_SET_WAN_TYE                                 , 
    FS_ETHER_HW_GET_CNTRL                                 , 
    FS_ETHER_HW_GET_COLL_FREQ                             , 
    FS_ETHER_HW_GET_PAUSE_FRAMES                          , 
    FS_ETHER_HW_GET_PAUSE_MODE                            , 
    FS_ETHER_HW_GET_STATS                                 , 
    FS_ETHER_HW_SET_PAUSE_ADMIN_MODE                      , 
    FS_HW_GET_ETHERNET_TYPE                               , 
    FS_HW_GET_STAT                                        , 
    FS_HW_GET_STAT64                                      , 
    FS_HW_SET_CUST_IF_PARAMS                              , 
    FS_HW_UPDATE_ADMIN_STATUS_CHANGE                      , 
    FS_MAU_HW_GET_AUTO_NEG_ADMIN_CONFIG                   , 
    FS_MAU_HW_GET_AUTO_NEG_ADMIN_STATUS                   , 
    FS_MAU_HW_GET_AUTO_NEG_CAP_ADVT_BITS                  , 
    FS_MAU_HW_GET_AUTO_NEG_CAP_BITS                       , 
    FS_MAU_HW_GET_AUTO_NEG_CAP_RCVD_BITS                  , 
    FS_MAU_HW_GET_AUTO_NEG_REM_FLT_ADVT                   , 
    FS_MAU_HW_GET_AUTO_NEG_REM_FLT_RCVD                   , 
    FS_MAU_HW_GET_AUTO_NEG_REMOTE_SIGNALING               , 
    FS_MAU_HW_GET_AUTO_NEG_RESTART                        , 
    FS_MAU_HW_GET_AUTO_NEG_SUPPORTED                      , 
    FS_MAU_HW_GET_FALSE_CARRIERS                          , 
    FS_MAU_HW_GET_JABBER_STATE                            , 
    FS_MAU_HW_GET_JABBERING_STATE_ENTERS                  , 
    FS_MAU_HW_GET_JACK_TYPE                               , 
    FS_MAU_HW_GET_MAU_TYPE                                , 
    FS_MAU_HW_GET_MEDIA_AVAIL_STATE_EXITS                 , 
    FS_MAU_HW_GET_MEDIA_AVAILABLE                         , 
    FS_MAU_HW_GET_TYPE_LIST_BITS                          , 
    FS_MAU_HW_SET_AUTO_NEG_ADMIN_STATUS                   , 
    FS_MAU_HW_SET_AUTO_NEG_CAP_ADVT_BITS                  , 
    FS_MAU_HW_SET_AUTO_NEG_REM_FLT_ADVT                   , 
    FS_MAU_HW_SET_AUTO_NEG_RESTART                        , 
    FS_MAU_HW_SET_MAU_STATUS                              , 
    FS_MAU_HW_GET_STATUS                                  , 
    FS_MAU_HW_SET_DEFAULT_TYPE                            , 
    FS_MAU_HW_GET_DEFAULT_TYPE                            , 
    NP_CFA_FRONT_PANEL_PORTS                              , 
    FS_CFA_HW_MAC_LOOKUP                                  , 
    CFA_NP_SET_HW_PORT_INFO                               , 
    CFA_NP_GET_HW_PORT_INFO                               , 
    FS_CFA_HW_DELETE_PKT_FILTER                           , 
    FS_CFA_HW_CREATE_PKT_FILTER                           , 
    FS_NP_CFA_SET_DLF_STATUS                              , 
    CFA_NP_SET_STACKING_MODEL                             , 
    CFA_NP_GET_HW_INFO                                    , 
    CFA_NP_REMOTE_SET_HW_INFO                             , 
    FS_CFA_HW_REMOVE_IP_NET_RCVD_DLF_IN_HASH             ,
#ifdef IP6_WANTED
    FS_CFA_HW_REMOVE_IP6_NET_RCVD_DLF_IN_HASH             ,
#endif
    CFA_NP_GET_HW_STACKING_INDEX                          , 
    FS_HW_GET_VLAN_INTF_STAT                              , 
    CFA_MBSM_NP_SLOT_DE_INIT                             , 
    CFA_MBSM_NP_SLOT_INIT                                , 
    CFA_MBSM_REGISTER_WITH_NP_DRV                        , 
    CFA_MBSM_UPDATE_PORT_MAC_ADDRESS                     , 
    FS_CFA_MBSM_SET_MAC_ADDR                             , 
    FS_NP_CFA_MBSM_ARP_MODIFY_FILTER                     ,
    FS_NP_CFA_MBSM_OSPF_MODIFY_FILTER                    ,
    FS_HW_MBSM_SET_CUST_IF_PARAMS                        , 
    FS_CFA_HW_L3_SET_MTU                                 ,
    FS_CFA_HW_MAX_NP  /* Max Count should be the Last entry */
}; 

/* Required arguments list for the cfa NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

typedef struct {
    UINT4  u4IfIndex;
    UINT1  u1PortLinkStatus;   
    UINT1  au1Pad[3];   
} tCfaNpWrCfaNpGetLinkStatus;

typedef struct {
    UINT4  u4Port;
} tCfaNpWrFsCfaHwClearStats;

typedef struct {
    UINT4    u4IfIndex;
    UINT4 *  pu4PortFlowControl;
} tCfaNpWrFsCfaHwGetIfFlowControl;

typedef struct {
    UINT4    u4IfIndex;
    UINT4 *  pu4MtuSize;
} tCfaNpWrFsCfaHwGetMtu;

typedef struct {
    UINT4     u4IfIndex;
    UINT1 *   PortMac;
} tCfaNpWrFsCfaHwSetMacAddr;

typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4MtuSize;
} tCfaNpWrFsCfaHwSetMtu;

typedef struct {
    tL3MtuInfo L3Mtu;
}tCfaNpWrFsCfaHwL3SetMtu;

typedef struct {
    tIfWanInfo *  pCfaWanInfo;
} tCfaNpWrFsCfaHwSetWanTye;

typedef struct {
    UINT4   u4IfIndex;
    INT4 *  pElement;
    INT4    i4Code;
} tCfaNpWrFsEtherHwGetCntrl;

typedef struct {
    UINT4    u4IfIndex;
    UINT4 *  pElement;
    INT4     i4dot3CollCount;
} tCfaNpWrFsEtherHwGetCollFreq;

typedef struct {
    UINT4    u4IfIndex;
    UINT4 *  pElement;
    INT4     i4Code;
} tCfaNpWrFsEtherHwGetPauseFrames;

typedef struct {
    UINT4   u4IfIndex;
    INT4 *  pElement;
    INT4    i4Code;
} tCfaNpWrFsEtherHwGetPauseMode;

typedef struct {
    UINT4        u4IfIndex;
    tEthStats *  pEthStats;
} tCfaNpWrFsEtherHwGetStats;

typedef struct {
    UINT4   u4IfIndex;
    INT4 *  pElement;
} tCfaNpWrFsEtherHwSetPauseAdminMode;

typedef struct {
    UINT4    u4IfIndex;
    UINT1 *  pu1EtherType;
} tCfaNpWrFsHwGetEthernetType;

typedef struct {
    UINT4    u4IfIndex;
    UINT4 *  pu4Value;
    INT1     i1StatType;
    UINT1    au1pad[3];
} tCfaNpWrFsHwGetStat;

typedef struct {
    UINT4                   u4IfIndex;
    tSNMP_COUNTER64_TYPE *  pValue;
    INT1                    i1StatType;
    UINT1                   au1pad[3];
} tCfaNpWrFsHwGetStat64;

typedef struct {
    UINT4               u4IfIndex;
    tHwCustIfParamType  HwCustParamType;
    tHwCustIfParamVal   CustIfParamVal;
    tNpEntryAction      EntryAction;
} tCfaNpWrFsHwSetCustIfParams;

typedef struct {
    UINT4  u4IfIndex;
    UINT1  u1AdminEnable;
    UINT1  au1pad[3];
} tCfaNpWrFsHwUpdateAdminStatusChange;

typedef struct {
    UINT4   u4IfIndex;
    INT4 *  pElement;
    INT4    i4IfMauId;
} tCfaNpWrFsMauHwGetAutoNegAdminConfig;

typedef struct {
    UINT4   u4IfIndex;
    INT4 *  pElement;
    INT4    i4IfMauId;
} tCfaNpWrFsMauHwGetAutoNegAdminStatus;

typedef struct {
    UINT4   u4IfIndex;
    INT4 *  pElement;
    INT4    i4IfMauId;
} tCfaNpWrFsMauHwGetAutoNegCapAdvtBits;

typedef struct {
    UINT4   u4IfIndex;
    INT4 *  pElement;
    INT4    i4IfMauId;
} tCfaNpWrFsMauHwGetAutoNegCapBits;

typedef struct {
    UINT4   u4IfIndex;
    INT4 *  pElement;
    INT4    i4IfMauId;
} tCfaNpWrFsMauHwGetAutoNegCapRcvdBits;

typedef struct {
    UINT4   u4IfIndex;
    INT4 *  pElement;
    INT4    i4IfMauId;
} tCfaNpWrFsMauHwGetAutoNegRemFltAdvt;

typedef struct {
    UINT4   u4IfIndex;
    INT4 *  pElement;
    INT4    i4IfMauId;
} tCfaNpWrFsMauHwGetAutoNegRemFltRcvd;

typedef struct {
    UINT4   u4IfIndex;
    INT4 *  pElement;
    INT4    i4IfMauId;
} tCfaNpWrFsMauHwGetAutoNegRemoteSignaling;

typedef struct {
    UINT4   u4IfIndex;
    INT4 *  pElement;
    INT4    i4IfMauId;
} tCfaNpWrFsMauHwGetAutoNegRestart;

typedef struct {
    UINT4   u4IfIndex;
    INT4 *  pElement;
    INT4    i4IfMauId;
} tCfaNpWrFsMauHwGetAutoNegSupported;

typedef struct {
    UINT4    u4IfIndex;
    UINT4 *  pElement;
    INT4     i4IfMauId;
} tCfaNpWrFsMauHwGetFalseCarriers;

typedef struct {
    UINT4   u4IfIndex;
    INT4 *  pElement;
    INT4    i4IfMauId;
} tCfaNpWrFsMauHwGetJabberState;

typedef struct {
    UINT4    u4IfIndex;
    UINT4 *  pElement;
    INT4     i4IfMauId;
} tCfaNpWrFsMauHwGetJabberingStateEnters;

typedef struct {
    UINT4   u4IfIndex;
    INT4 *  pElement;
    INT4    i4ifJackIndex;
    INT4    i4IfMauId;
} tCfaNpWrFsMauHwGetJackType;

typedef struct {
    UINT4    u4IfIndex;
    UINT4 *  pu4Val;
    INT4     i4IfMauId;
} tCfaNpWrFsMauHwGetMauType;

typedef struct {
    UINT4    u4IfIndex;
    UINT4 *  pElement;
    INT4     i4IfMauId;
} tCfaNpWrFsMauHwGetMediaAvailStateExits;

typedef struct {
    UINT4   u4IfIndex;
    INT4 *  pElement;
    INT4    i4IfMauId;
} tCfaNpWrFsMauHwGetMediaAvailable;

typedef struct {
    UINT4   u4IfIndex;
    INT4 *  pElement;
    INT4    i4IfMauId;
} tCfaNpWrFsMauHwGetTypeListBits;

typedef struct {
    UINT4   u4IfIndex;
    INT4 *  pElement;
    INT4    i4IfMauId;
} tCfaNpWrFsMauHwSetAutoNegAdminStatus;

typedef struct {
    UINT4   u4IfIndex;
    INT4 *  pElement;
    INT4    i4IfMauId;
} tCfaNpWrFsMauHwSetAutoNegCapAdvtBits;

typedef struct {
    UINT4   u4IfIndex;
    INT4 *  pElement;
    INT4    i4IfMauId;
} tCfaNpWrFsMauHwSetAutoNegRemFltAdvt;

typedef struct {
    UINT4   u4IfIndex;
    INT4 *  pElement;
    INT4    i4IfMauId;
} tCfaNpWrFsMauHwSetAutoNegRestart;

typedef struct {
    UINT4   u4IfIndex;
    INT4 *  pElement;
    INT4    i4IfMauId;
} tCfaNpWrFsMauHwSetMauStatus;

typedef struct {
    UINT4   u4IfIndex;
    INT4 *  pElement;
    INT4    i4IfMauId;
} tCfaNpWrFsMauHwGetStatus;

typedef struct {
    UINT4  u4IfIndex;
    INT4   i4IfMauId;
    UINT4  u4Val;
} tCfaNpWrFsMauHwSetDefaultType;

typedef struct {
    UINT4    u4IfIndex;
    UINT4 *  pu4Val;
    INT4     i4IfMauId;
} tCfaNpWrFsMauHwGetDefaultType;

typedef struct {
    UINT4  u4MaxFrontPanelPorts;
} tCfaNpWrNpCfaFrontPanelPorts;

typedef struct {
    UINT2 *           pu2Port;
    UINT1 *           pu1MacAddr;
    tVlanIfaceVlanId  VlanId;
    UINT1             au1pad[2];
} tCfaNpWrFsCfaHwMacLookup;

typedef struct {
    tHwPortInfo  HwPortInfo;
} tCfaNpWrCfaNpSetHwPortInfo;

typedef struct {
    tHwPortInfo *  pHwPortInfo;
} tCfaNpWrCfaNpGetHwPortInfo;

typedef struct {
    UINT4  u4FilterId;
} tCfaNpWrFsCfaHwDeletePktFilter;

typedef struct {
    tHwCfaFilterInfo *  pFilterInfo;
    UINT4 *             pu4FilterId;
} tCfaNpWrFsCfaHwCreatePktFilter;

typedef struct {
    UINT1  u1Status;
    UINT1  au1pad[3];
} tCfaNpWrFsNpCfaSetDlfStatus;

typedef struct {
    UINT4  u4StackingModel;
} tCfaNpWrCfaNpSetStackingModel;

typedef struct {
    tHwIdInfo  * pHwIdInfo;
} tCfaNpWrCfaNpGetHwInfo;

typedef struct {
    tHwIdInfo * pHwIdInfo;
} tCfaNpWrCfaNpRemoteSetHwInfo;

typedef struct {
    UINT4 u4ContextId;
    UINT4  u4IpNet;
    UINT4  u4IpMask;
} tCfaNpWrFsCfaHwRemoveIpNetRcvdDlfInHash;

#ifdef IP6_WANTED
typedef struct {
    tIp6Addr    Ip6Addr;
    UINT4       u4Prefix;
    UINT4       u4ContextId;
} tCfaNpWrFsCfaHwRemoveIp6NetRcvdDlfInHash;
#endif

#ifdef MBSM_WANTED 
typedef struct {
    tMbsmSlotInfo *  pSlotInfo;
} tCfaNpWrCfaMbsmNpSlotDeInit;

typedef struct {
    tMbsmSlotInfo *  pSlotInfo;
} tCfaNpWrCfaMbsmNpSlotInit;

typedef struct {
    tMbsmSlotInfo *  pSlotInfo;
} tCfaNpWrCfaMbsmRegisterWithNpDrv;

typedef struct {
    UINT4    u4PortIndex;
    UINT4    u4IfIndex;
    UINT1 *  pu1SwitchMac;
    UINT1    u1SlotId;
    UINT1    au1pad[3];
} tCfaNpWrCfaMbsmUpdatePortMacAddress;

typedef struct {
    UINT4            u4IfIndex;
    UINT1 *          PortMac;
    tMbsmSlotInfo *  pSlotInfo;
} tCfaNpWrFsCfaMbsmSetMacAddr;

typedef struct {
    UINT4               u4IfIndex;
    tMbsmSlotInfo *     pSlotInfo;
    tHwCustIfParamType  HwCustParamType;
    tHwCustIfParamVal   CustIfParamVal;
    tNpEntryAction      EntryAction;
} tCfaNpWrFsHwMbsmSetCustIfParams;

typedef struct {
    UINT1  u1Action;
    UINT1  au1pad[3];
} tCfaNpWrFsNpCfaModifyFilter;



#endif /* MBSM_WANTED */

typedef struct {
    UINT4    u4IfIndex;
    UINT4 *  pu4Value;
    INT1     i1StatType;
    UINT1    au1pad[3];
} tCfaNpWrFsHwGetVlanIntfStats;


typedef struct CfaNpModInfo {
union {
    tCfaNpWrCfaNpGetLinkStatus  sCfaNpGetLinkStatus;
    tCfaNpWrFsCfaHwClearStats  sFsCfaHwClearStats;
    tCfaNpWrFsCfaHwGetIfFlowControl  sFsCfaHwGetIfFlowControl;
    tCfaNpWrFsCfaHwGetMtu  sFsCfaHwGetMtu;
    tCfaNpWrFsCfaHwSetMacAddr  sFsCfaHwSetMacAddr;
    tCfaNpWrFsCfaHwSetMtu  sFsCfaHwSetMtu;
    tCfaNpWrFsCfaHwSetWanTye  sFsCfaHwSetWanTye;
    tCfaNpWrFsEtherHwGetCntrl  sFsEtherHwGetCntrl;
    tCfaNpWrFsEtherHwGetCollFreq  sFsEtherHwGetCollFreq;
    tCfaNpWrFsEtherHwGetPauseFrames  sFsEtherHwGetPauseFrames;
    tCfaNpWrFsEtherHwGetPauseMode  sFsEtherHwGetPauseMode;
    tCfaNpWrFsEtherHwGetStats  sFsEtherHwGetStats;
    tCfaNpWrFsEtherHwSetPauseAdminMode  sFsEtherHwSetPauseAdminMode;
    tCfaNpWrFsHwGetEthernetType  sFsHwGetEthernetType;
    tCfaNpWrFsHwGetStat  sFsHwGetStat;
    tCfaNpWrFsHwGetStat64  sFsHwGetStat64;
    tCfaNpWrFsHwSetCustIfParams  sFsHwSetCustIfParams;
    tCfaNpWrFsHwUpdateAdminStatusChange  sFsHwUpdateAdminStatusChange;
    tCfaNpWrFsMauHwGetAutoNegAdminConfig  sFsMauHwGetAutoNegAdminConfig;
    tCfaNpWrFsMauHwGetAutoNegAdminStatus  sFsMauHwGetAutoNegAdminStatus;
    tCfaNpWrFsMauHwGetAutoNegCapAdvtBits  sFsMauHwGetAutoNegCapAdvtBits;
    tCfaNpWrFsMauHwGetAutoNegCapBits  sFsMauHwGetAutoNegCapBits;
    tCfaNpWrFsMauHwGetAutoNegCapRcvdBits  sFsMauHwGetAutoNegCapRcvdBits;
    tCfaNpWrFsMauHwGetAutoNegRemFltAdvt  sFsMauHwGetAutoNegRemFltAdvt;
    tCfaNpWrFsMauHwGetAutoNegRemFltRcvd  sFsMauHwGetAutoNegRemFltRcvd;
    tCfaNpWrFsMauHwGetAutoNegRemoteSignaling  sFsMauHwGetAutoNegRemoteSignaling;
    tCfaNpWrFsMauHwGetAutoNegRestart  sFsMauHwGetAutoNegRestart;
    tCfaNpWrFsMauHwGetAutoNegSupported  sFsMauHwGetAutoNegSupported;
    tCfaNpWrFsMauHwGetFalseCarriers  sFsMauHwGetFalseCarriers;
    tCfaNpWrFsMauHwGetJabberState  sFsMauHwGetJabberState;
    tCfaNpWrFsMauHwGetJabberingStateEnters  sFsMauHwGetJabberingStateEnters;
    tCfaNpWrFsMauHwGetJackType  sFsMauHwGetJackType;
    tCfaNpWrFsMauHwGetMauType  sFsMauHwGetMauType;
    tCfaNpWrFsMauHwGetMediaAvailStateExits  sFsMauHwGetMediaAvailStateExits;
    tCfaNpWrFsMauHwGetMediaAvailable  sFsMauHwGetMediaAvailable;
    tCfaNpWrFsMauHwGetTypeListBits  sFsMauHwGetTypeListBits;
    tCfaNpWrFsMauHwSetAutoNegAdminStatus  sFsMauHwSetAutoNegAdminStatus;
    tCfaNpWrFsMauHwSetAutoNegCapAdvtBits  sFsMauHwSetAutoNegCapAdvtBits;
    tCfaNpWrFsMauHwSetAutoNegRemFltAdvt  sFsMauHwSetAutoNegRemFltAdvt;
    tCfaNpWrFsMauHwSetAutoNegRestart  sFsMauHwSetAutoNegRestart;
    tCfaNpWrFsMauHwSetMauStatus  sFsMauHwSetMauStatus;
    tCfaNpWrFsMauHwGetStatus  sFsMauHwGetStatus;
    tCfaNpWrFsMauHwSetDefaultType  sFsMauHwSetDefaultType;
    tCfaNpWrFsMauHwGetDefaultType  sFsMauHwGetDefaultType;
    tCfaNpWrNpCfaFrontPanelPorts  sNpCfaFrontPanelPorts;
    tCfaNpWrFsCfaHwMacLookup  sFsCfaHwMacLookup;
    tCfaNpWrCfaNpSetHwPortInfo  sCfaNpSetHwPortInfo;
    tCfaNpWrCfaNpGetHwPortInfo  sCfaNpGetHwPortInfo;
    tCfaNpWrFsCfaHwDeletePktFilter  sFsCfaHwDeletePktFilter;
    tCfaNpWrFsCfaHwCreatePktFilter  sFsCfaHwCreatePktFilter;
    tCfaNpWrFsNpCfaSetDlfStatus  sFsNpCfaSetDlfStatus;
    tCfaNpWrCfaNpSetStackingModel  sCfaNpSetStackingModel;
    tCfaNpWrCfaNpGetHwInfo  sCfaNpGetHwInfo;
    tCfaNpWrCfaNpRemoteSetHwInfo  sCfaNpRemoteSetHwInfo;
#ifdef MBSM_WANTED 
    tCfaNpWrCfaMbsmNpSlotDeInit  sCfaMbsmNpSlotDeInit;
    tCfaNpWrCfaMbsmNpSlotInit  sCfaMbsmNpSlotInit;
    tCfaNpWrCfaMbsmRegisterWithNpDrv  sCfaMbsmRegisterWithNpDrv;
    tCfaNpWrCfaMbsmUpdatePortMacAddress  sCfaMbsmUpdatePortMacAddress;
    tCfaNpWrFsCfaMbsmSetMacAddr  sFsCfaMbsmSetMacAddr;
    tCfaNpWrFsHwMbsmSetCustIfParams  sFsHwMbsmSetCustIfParams;
    tCfaNpWrFsNpCfaModifyFilter  sFsNpCfaModifyFilter;
#endif /* MBSM_WANTED */
    tCfaNpWrFsHwGetVlanIntfStats  sFsHwGetVlanIntfStats;
    tCfaNpWrFsCfaHwRemoveIpNetRcvdDlfInHash  sFsCfaHwRemoveIpNetRcvdDlfInHash;
#ifdef IP6_WANTED
    tCfaNpWrFsCfaHwRemoveIp6NetRcvdDlfInHash  sFsCfaHwRemoveIp6NetRcvdDlfInHash;
#endif
    tCfaNpWrFsCfaHwL3SetMtu  sFsCfaHwL3SetMtu;
    }unOpCode;

#define  CfaNpCfaNpGetLinkStatus  unOpCode.sCfaNpGetLinkStatus;
#define  CfaNpFsCfaHwClearStats  unOpCode.sFsCfaHwClearStats;
#define  CfaNpFsCfaHwGetIfFlowControl  unOpCode.sFsCfaHwGetIfFlowControl;
#define  CfaNpFsCfaHwGetMtu  unOpCode.sFsCfaHwGetMtu;
#define  CfaNpFsCfaHwSetMacAddr  unOpCode.sFsCfaHwSetMacAddr;
#define  CfaNpFsCfaHwSetMtu  unOpCode.sFsCfaHwSetMtu;
#define  CfaNpFsCfaHwSetWanTye  unOpCode.sFsCfaHwSetWanTye;
#define  CfaNpFsEtherHwGetCntrl  unOpCode.sFsEtherHwGetCntrl;
#define  CfaNpFsEtherHwGetCollFreq  unOpCode.sFsEtherHwGetCollFreq;
#define  CfaNpFsEtherHwGetPauseFrames  unOpCode.sFsEtherHwGetPauseFrames;
#define  CfaNpFsEtherHwGetPauseMode  unOpCode.sFsEtherHwGetPauseMode;
#define  CfaNpFsEtherHwGetStats  unOpCode.sFsEtherHwGetStats;
#define  CfaNpFsEtherHwSetPauseAdminMode  unOpCode.sFsEtherHwSetPauseAdminMode;
#define  CfaNpFsHwGetEthernetType  unOpCode.sFsHwGetEthernetType;
#define  CfaNpFsHwGetStat  unOpCode.sFsHwGetStat;
#define  CfaNpFsHwGetStat64  unOpCode.sFsHwGetStat64;
#define  CfaNpFsHwSetCustIfParams  unOpCode.sFsHwSetCustIfParams;
#define  CfaNpFsHwUpdateAdminStatusChange  unOpCode.sFsHwUpdateAdminStatusChange;
#define  CfaNpFsMauHwGetAutoNegAdminConfig  unOpCode.sFsMauHwGetAutoNegAdminConfig;
#define  CfaNpFsMauHwGetAutoNegAdminStatus  unOpCode.sFsMauHwGetAutoNegAdminStatus;
#define  CfaNpFsMauHwGetAutoNegCapAdvtBits  unOpCode.sFsMauHwGetAutoNegCapAdvtBits;
#define  CfaNpFsMauHwGetAutoNegCapBits  unOpCode.sFsMauHwGetAutoNegCapBits;
#define  CfaNpFsMauHwGetAutoNegCapRcvdBits  unOpCode.sFsMauHwGetAutoNegCapRcvdBits;
#define  CfaNpFsMauHwGetAutoNegRemFltAdvt  unOpCode.sFsMauHwGetAutoNegRemFltAdvt;
#define  CfaNpFsMauHwGetAutoNegRemFltRcvd  unOpCode.sFsMauHwGetAutoNegRemFltRcvd;
#define  CfaNpFsMauHwGetAutoNegRemoteSignaling  unOpCode.sFsMauHwGetAutoNegRemoteSignaling;
#define  CfaNpFsMauHwGetAutoNegRestart  unOpCode.sFsMauHwGetAutoNegRestart;
#define  CfaNpFsMauHwGetAutoNegSupported  unOpCode.sFsMauHwGetAutoNegSupported;
#define  CfaNpFsMauHwGetFalseCarriers  unOpCode.sFsMauHwGetFalseCarriers;
#define  CfaNpFsMauHwGetJabberState  unOpCode.sFsMauHwGetJabberState;
#define  CfaNpFsMauHwGetJabberingStateEnters  unOpCode.sFsMauHwGetJabberingStateEnters;
#define  CfaNpFsMauHwGetJackType  unOpCode.sFsMauHwGetJackType;
#define  CfaNpFsMauHwGetMauType  unOpCode.sFsMauHwGetMauType;
#define  CfaNpFsMauHwGetMediaAvailStateExits  unOpCode.sFsMauHwGetMediaAvailStateExits;
#define  CfaNpFsMauHwGetMediaAvailable  unOpCode.sFsMauHwGetMediaAvailable;
#define  CfaNpFsMauHwGetTypeListBits  unOpCode.sFsMauHwGetTypeListBits;
#define  CfaNpFsMauHwSetAutoNegAdminStatus  unOpCode.sFsMauHwSetAutoNegAdminStatus;
#define  CfaNpFsMauHwSetAutoNegCapAdvtBits  unOpCode.sFsMauHwSetAutoNegCapAdvtBits;
#define  CfaNpFsMauHwSetAutoNegRemFltAdvt  unOpCode.sFsMauHwSetAutoNegRemFltAdvt;
#define  CfaNpFsMauHwSetAutoNegRestart  unOpCode.sFsMauHwSetAutoNegRestart;
#define  CfaNpFsMauHwSetMauStatus  unOpCode.sFsMauHwSetMauStatus;
#define  CfaNpFsMauHwGetStatus  unOpCode.sFsMauHwGetStatus;
#define  CfaNpFsMauHwSetDefaultType  unOpCode.sFsMauHwSetDefaultType;
#define  CfaNpFsMauHwGetDefaultType  unOpCode.sFsMauHwGetDefaultType;
#define  CfaNpNpCfaFrontPanelPorts  unOpCode.sNpCfaFrontPanelPorts;
#define  CfaNpFsCfaHwMacLookup  unOpCode.sFsCfaHwMacLookup;
#define  CfaNpCfaNpSetHwPortInfo  unOpCode.sCfaNpSetHwPortInfo;
#define  CfaNpCfaNpGetHwPortInfo  unOpCode.sCfaNpGetHwPortInfo;
#define  CfaNpFsCfaHwDeletePktFilter  unOpCode.sFsCfaHwDeletePktFilter;
#define  CfaNpFsCfaHwCreatePktFilter  unOpCode.sFsCfaHwCreatePktFilter;
#define  CfaNpFsNpCfaSetDlfStatus  unOpCode.sFsNpCfaSetDlfStatus;
#define  CfaNpCfaNpSetStackingModel  unOpCode.sCfaNpSetStackingModel;
#define  CfaNpCfaNpGetHwInfo  unOpCode.sCfaNpGetHwInfo;
#define  CfaNpCfaNpRemoteSetHwInfo  unOpCode.sCfaNpRemoteSetHwInfo;
#ifdef MBSM_WANTED 
#define  CfaNpCfaMbsmNpSlotDeInit  unOpCode.sCfaMbsmNpSlotDeInit;
#define  CfaNpCfaMbsmNpSlotInit  unOpCode.sCfaMbsmNpSlotInit;
#define  CfaNpCfaMbsmRegisterWithNpDrv  unOpCode.sCfaMbsmRegisterWithNpDrv;
#define  CfaNpCfaMbsmUpdatePortMacAddress  unOpCode.sCfaMbsmUpdatePortMacAddress;
#define  CfaNpFsCfaMbsmSetMacAddr  unOpCode.sFsCfaMbsmSetMacAddr;
#define  CfaNpFsHwMbsmSetCustIfParams  unOpCode.sFsHwMbsmSetCustIfParams;
#define  CfaNpFsNpCfaModifyFilter  unOpCode.sFsNpCfaModifyFilter;
#endif /* MBSM_WANTED */
#define  CfaNpFsHwGetVlanIntfStats  unOpCode.sFsHwGetVlanIntfStats;
#define  CfaNpFsCfaHwRemoveIpNetRcvdDlfInHash  unOpCode.sFsCfaHwRemoveIpNetRcvdDlfInHash;
#ifdef IP6_WANTED
#define  CfaNpFsCfaHwRemoveIp6NetRcvdDlfInHash  unOpCode.sFsCfaHwRemoveIp6NetRcvdDlfInHash;
#endif
#define  CfaNpFsCfaHwL3SetMtu  unOpCode.sFsCfaHwL3SetMtu;
} tCfaNpModInfo;


/* Function Prototype for Generic NP OpCode specific wrappers
 * defined in Cfanpapi.c */

UINT1 CfaCfaNpGetLinkStatus PROTO ((UINT4 u4IfIndex));
UINT1 CfaCfaRegisterWithNpDrv PROTO ((VOID));
UINT1 CfaFsCfaHwClearStats PROTO ((UINT4 u4Port));
UINT1 CfaFsCfaHwGetIfFlowControl PROTO ((UINT4 u4IfIndex, UINT4 * pu4PortFlowControl));
UINT1 CfaFsCfaHwGetMtu PROTO ((UINT2 u2IfIndex, UINT4 * pu4MtuSize));
UINT1 CfaFsCfaHwSetMacAddr PROTO ((UINT4 u4IfIndex, tMacAddr PortMac));
UINT1 CfaFsCfaHwSetMtu PROTO ((UINT4 u4IfIndex, UINT4 u4MtuSize));
UINT1 CfaFsCfaHwL3SetMtu PROTO ((tL3MtuInfo L3Mtu));
UINT1 CfaFsCfaHwSetWanTye PROTO ((tIfWanInfo * pCfaWanInfo));
UINT1 CfaFsEtherHwGetCntrl PROTO ((UINT4 u4IfIndex, INT4 * pElement, INT4 i4Code));
UINT1 CfaFsEtherHwGetCollFreq PROTO ((UINT4 u4IfIndex, INT4 i4dot3CollCount, UINT4 * pElement));
UINT1 CfaFsEtherHwGetPauseFrames PROTO ((UINT4 u4IfIndex, UINT4 * pElement, INT4 i4Code));
UINT1 CfaFsEtherHwGetPauseMode PROTO ((UINT4 u4IfIndex, INT4 * pElement, INT4 i4Code));
UINT1 CfaFsEtherHwGetStats PROTO ((UINT4 u4IfIndex, tEthStats * pEthStats));
UINT1 CfaFsEtherHwSetPauseAdminMode PROTO ((UINT4 u4IfIndex, INT4 * pElement));
UINT1 CfaFsHwGetEthernetType PROTO ((UINT4 u4IfIndex, UINT1 * pu1EtherType));
UINT1 CfaFsHwGetStat PROTO ((UINT4 u4IfIndex, INT1 i1StatType, UINT4 * pu4Value));
UINT1 CfaFsHwGetStat64 PROTO ((UINT4 u4IfIndex, INT1 i1StatType, tSNMP_COUNTER64_TYPE * pValue));
UINT1 CfaFsHwSetCustIfParams PROTO ((UINT4 u4IfIndex, tHwCustIfParamType HwCustParamType, tHwCustIfParamVal CustIfParamVal, tNpEntryAction EntryAction));
UINT1 CfaFsHwUpdateAdminStatusChange PROTO ((UINT4 u4IfIndex, UINT1 u1AdminEnable));
UINT1 CfaFsMauHwGetAutoNegAdminConfig PROTO ((UINT4 u4IfIndex, INT4 i4IfMauId, INT4 * pElement));
UINT1 CfaFsMauHwGetAutoNegAdminStatus PROTO ((UINT4 u4IfIndex, INT4 i4IfMauId, INT4 * pElement));
UINT1 CfaFsMauHwGetAutoNegCapAdvtBits PROTO ((UINT4 u4IfIndex, INT4 i4IfMauId, INT4 * pElement));
UINT1 CfaFsMauHwGetAutoNegCapBits PROTO ((UINT4 u4IfIndex, INT4 i4IfMauId, INT4 * pElement));
UINT1 CfaFsMauHwGetAutoNegCapRcvdBits PROTO ((UINT4 u4IfIndex, INT4 i4IfMauId, INT4 * pElement));
UINT1 CfaFsMauHwGetAutoNegRemFltAdvt PROTO ((UINT4 u4IfIndex, INT4 i4IfMauId, INT4 * pElement));
UINT1 CfaFsMauHwGetAutoNegRemFltRcvd PROTO ((UINT4 u4IfIndex, INT4 i4IfMauId, INT4 * pElement));
UINT1 CfaFsMauHwGetAutoNegRemoteSignaling PROTO ((UINT4 u4IfIndex, INT4 i4IfMauId, INT4 * pElement));
UINT1 CfaFsMauHwGetAutoNegRestart PROTO ((UINT4 u4IfIndex, INT4 i4IfMauId, INT4 * pElement));
UINT1 CfaFsMauHwGetAutoNegSupported PROTO ((UINT4 u4IfIndex, INT4 i4IfMauId, INT4 * pElement));
UINT1 CfaFsMauHwGetFalseCarriers PROTO ((UINT4 u4IfIndex, INT4 i4IfMauId, UINT4 * pElement));
UINT1 CfaFsMauHwGetJabberState PROTO ((UINT4 u4IfIndex, INT4 i4IfMauId, INT4 * pElement));
UINT1 CfaFsMauHwGetJabberingStateEnters PROTO ((UINT4 u4IfIndex, INT4 i4IfMauId, UINT4 * pElement));
UINT1 CfaFsMauHwGetJackType PROTO ((UINT4 u4IfIndex, INT4 i4IfMauId, INT4 i4ifJackIndex, INT4 * pElement));
UINT1 CfaFsMauHwGetMauType PROTO ((UINT4 u4IfIndex, INT4 i4IfMauId, UINT4 * pu4Val));
UINT1 CfaFsMauHwGetMediaAvailStateExits PROTO ((UINT4 u4IfIndex, INT4 i4IfMauId, UINT4 * pElement));
UINT1 CfaFsMauHwGetMediaAvailable PROTO ((UINT4 u4IfIndex, INT4 i4IfMauId, INT4 * pElement));
UINT1 CfaFsMauHwGetTypeListBits PROTO ((UINT4 u4IfIndex, INT4 i4IfMauId, INT4 * pElement));
UINT1 CfaFsMauHwSetAutoNegAdminStatus PROTO ((UINT4 u4IfIndex, INT4 i4IfMauId, INT4 * pElement));
UINT1 CfaFsMauHwSetAutoNegCapAdvtBits PROTO ((UINT4 u4IfIndex, INT4 i4IfMauId, INT4 * pElement));
UINT1 CfaFsMauHwSetAutoNegRemFltAdvt PROTO ((UINT4 u4IfIndex, INT4 i4IfMauId, INT4 * pElement));
UINT1 CfaFsMauHwSetAutoNegRestart PROTO ((UINT4 u4IfIndex, INT4 i4IfMauId, INT4 * pElement));
UINT1 CfaFsMauHwSetMauStatus PROTO ((UINT4 u4IfIndex, INT4 i4IfMauId, INT4 * pElement));
UINT1 CfaFsMauHwGetStatus PROTO ((UINT4 u4IfIndex, INT4 i4IfMauId, INT4 * pElement));
UINT1 CfaFsMauHwSetDefaultType PROTO ((UINT4 u4IfIndex, INT4 i4IfMauId, UINT4 u4Val));
UINT1 CfaFsMauHwGetDefaultType PROTO ((UINT4 u4IfIndex, INT4 i4IfMauId, UINT4 * pu4Val));
UINT1 CfaNpCfaFrontPanelPorts PROTO ((UINT4 u4MaxFrontPanelPorts));
UINT1 CfaFsCfaHwMacLookup PROTO ((UINT1 * pu1MacAddr, tVlanIfaceVlanId VlanId, UINT2 * pu2Port));
UINT1 CfaCfaNpSetHwPortInfo PROTO ((tHwPortInfo HwPortInfo));
UINT1 CfaCfaNpGetHwPortInfo PROTO ((tHwPortInfo * pHwPortInfo));
UINT1 CfaFsCfaHwDeletePktFilter PROTO ((UINT4 u4FilterId));
UINT1 CfaFsCfaHwCreatePktFilter PROTO ((tHwCfaFilterInfo * pFilterInfo, UINT4 * pu4FilterId));
UINT1 CfaFsNpCfaSetDlfStatus PROTO ((UINT1 u1Status));
UINT1 CfaCfaNpSetStackingModel PROTO ((UINT4 u4StackingModel));
UINT1 CfaIssHwGetPortSpeed PROTO ((UINT4 u4IfIndex, INT4 * pi4PortSpeed));
UINT1 CfaIssHwSetPortSpeed PROTO ((UINT4 u4IfIndex, INT4 i4Value));
UINT1 CfaIssHwSetPortAutoNegAdvtCapBits PROTO ((UINT4 u4IfIndex, INT4 * pi4MauCapBits, INT4 * pi4IssCapBits));
UINT1 CfaIssHwInitFilter PROTO ((VOID));
UINT1 CfaCfaNpGetHwInfo PROTO ((tHwIdInfo * pHwIdInfo));
UINT1 CfaCfaNpRemoteSetHwInfo PROTO ((tHwIdInfo * pHwIdInfo));
UINT1 CfaFsNpIpv4DeleteIpInterface PROTO ((UINT4 u4VrId, UINT1 *pu1IfName, UINT4 u4IfIndex, UINT2 u2VlanId));
UINT1 CfaFsNpIpv4DeleteSecIpInterface PROTO ((UINT4 u4VrId, UINT1 *pu1IfName, UINT4 u4IfIndex, UINT4 u4IpAddr, UINT2 u2VlanId));
UINT1 CfaFsNpIpv4MapVlansToIpInterface PROTO ((tNpIpVlanMappingInfo * pPvlanMappingInfo));
UINT1 CfaFsNpIpv4IntfStatus PROTO ((UINT4 u4VrId, UINT4 u4IfStatus, tFsNpIp4IntInfo * pIpIntInfo));
UINT1 CfaFsNpIpv4UpdateIpInterfaceStatus PROTO ((UINT4 u4VrId, UINT1 *pu1IfName, UINT4 u4CfaIfIndex, UINT4 u4IpAddr, UINT4 u4IpSubNetMask, UINT2 u2VlanId, UINT1 *au1MacAddr, UINT4 u4Status));
UINT1 CfaFsNpIpv4CreateIpInterface PROTO ((UINT4 u4VrId, UINT1 *pu1IfName, UINT4 u4CfaIfIndex, UINT4 u4IpAddr, UINT4 u4IpSubNetMask, UINT2 u2VlanId, UINT1 *au1MacAddr));
UINT1 CfaFsNpIpv4L3IpInterface PROTO ((tL3Action L3Action, tFsNpL3IfInfo * pFsNpL3IfInfo));
UINT1 CfaFsNpIpv4ModifyIpInterface PROTO ((UINT4 u4VrId, UINT1 *pu1IfName, UINT4 u4CfaIfIndex, UINT4 u4IpAddr, UINT4 u4IpSubNetMask, UINT2 u2VlanId, UINT1 *au1MacAddr));
UINT1
CfaFsNpIpv4CreateL3SubInterface (UINT4 u4VrId, UINT1 *pu1IfName,
                              UINT4 u4CfaIfIndex, UINT4 u4IpAddr,
                              UINT4 u4IpSubNetMask, UINT2 u2VlanId,
                              UINT1 *au1MacAddr, UINT4 u4ParentIfIndex);
UINT1
CfaFsNpIpv4DeleteL3SubInterface (UINT4 u4IfIndex);

#ifdef VRRP_WANTED
UINT1 CfaFsNpIpv4VrrpIntfDeleteWr PROTO ((tNpVlanId u2VlanId, UINT4 u4IfIndex, UINT4 u4IpAddr, UINT1 *au1MacAddr));
#endif /* VRRP_WANTED */
#ifdef IP6_WANTED
UINT1 CfaFsNpIpv6IntfStatus PROTO ((UINT4 u4VrId, UINT4 u4IfStatus, tFsNpIntInfo * pIntfInfo));
#endif /* IP6_WANTED */
#ifdef MBSM_WANTED 
UINT1 CfaCfaMbsmNpSlotDeInit PROTO ((tMbsmSlotInfo * pSlotInfo));
UINT1 CfaCfaMbsmNpSlotInit PROTO ((tMbsmSlotInfo * pSlotInfo));
UINT1 CfaCfaMbsmRegisterWithNpDrv PROTO ((tMbsmSlotInfo * pSlotInfo));
UINT1 CfaCfaMbsmUpdatePortMacAddress PROTO ((UINT1 u1SlotId, UINT4 u4PortIndex, UINT4 u4IfIndex, UINT1 * pu1SwitchMac));
UINT1 CfaFsCfaMbsmSetMacAddr PROTO ((UINT4 u4IfIndex, tMacAddr PortMac, tMbsmSlotInfo * pSlotInfo));
UINT1 CfaFsHwMbsmSetCustIfParams PROTO ((UINT4 u4IfIndex, tHwCustIfParamType HwCustParamType, tHwCustIfParamVal CustIfParamVal, tNpEntryAction EntryAction, tMbsmSlotInfo * pSlotInfo));
UINT1 CfaMbsmNpGetStackMac PROTO ((UINT4 u4SlotId, UINT1 *pu1MacAddr));
UINT1 CfaMbsmNpTxOnStackInterface PROTO ((UINT1 *pu1Pkt, INT4 i4PktSize));
UINT1 CfaFsNpMbsmIpv4CreateIpInterface PROTO ((UINT4 u4VrId, UINT4 u4CfaIfIndex, UINT4 u4IpAddr, UINT4 u4IpSubNetMask, UINT2 u2VlanId, UINT1 *au1MacAddr, tMbsmSlotInfo * pSlotInfo));
UINT1 CfaFsNpMbsmIpv4MapVlansToIpInterface PROTO ((tNpIpVlanMappingInfo * pPvlanMappingInfo, tMbsmSlotInfo * pSlotInfo));
INT4  FsNpCfaModifyFilter PROTO ((UINT1 u1Action, UINT4 u4OpCode));
#endif /* MBSM_WANTED */
UINT1 CfaFsMiRstpNpSetPortState (UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1Status);
UINT1 CfaFsHwGetVlanIntfStats PROTO ((UINT4 u4IfIndex, INT1 i1StatType, UINT4 * pu4Value));
UINT1 CfaFsCfaHwRemoveIpNetRcvdDlfInHash PROTO ((UINT4 u4ContextId,UINT4 u4IpNet, UINT4 u4IpMask));
#ifdef IP6_WANTED
UINT1 CfaFsCfaHwRemoveIp6NetRcvdDlfInHash PROTO ((UINT4 u4ContextId, tIp6Addr Ip6Addr, UINT4 u4Prefix));
#endif
#endif /* __CFA_NP_WR_H__ */
