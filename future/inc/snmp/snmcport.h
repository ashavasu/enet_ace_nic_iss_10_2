/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: snmcport.h,v 1.20 2015/04/28 11:55:07 siva Exp $
 *
 * Description:Contains all the porting related  macros 
 *
 *******************************************************************/

#ifndef _SNMCPORT_H
#define _SNMCPORT_H
#include "fssnmp.h"

typedef struct
{
    tTMO_SLL_NODE     link;
    tSNMP_OCTET_STRING_TYPE CommIndex;
    tSNMP_OCTET_STRING_TYPE CommunityName;
    tSNMP_OCTET_STRING_TYPE SecurityName;
    tSNMP_OCTET_STRING_TYPE ContextEngineID;
    tSNMP_OCTET_STRING_TYPE ContextName;
    tSNMP_OCTET_STRING_TYPE TransTag;
    INT4               i4Storage;
    INT4               i4Status; 
}tCommunityMappingEntry;



tCommunityMappingEntry *SNMPGetCommunityEntryFromName(tSNMP_OCTET_STRING_TYPE *);

#endif /*_SNMCPORT_H*/
