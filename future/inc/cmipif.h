/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cmipif.h,v 1.3 2007/02/01 14:52:30 iss Exp $
 *
 * Description: This file contains exported definitions and functions
 *              of CMIP Interface  module.
 *********************************************************************/
#ifndef _CMIPIF_H
#define _CMIPIF_H

/* CMIP Interface Main Task */
VOID  CmifMainTask PROTO ((INT1 *));

/* Prototype of API to retrieve the value of a variable */
INT4  CmifApiVariableRetrieve PROTO ((UINT4 u4Port, UINT1 u1Branch,
                                      UINT2 u2Leaf, UINT4 u4ReqId));

#endif /* _CMIPIF_H*/
