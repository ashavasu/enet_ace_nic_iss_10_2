/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: ip6mcnpwr.h,v 1.1 2014/12/18 12:24:58 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              for Multicast wrappers
 *              
 ********************************************************************/
#ifndef _IP6MCNP_WR_H_
#define _IP6MCNP_WR_H_

#include "ip6mcnp.h"

#ifdef PIMV6_WANTED
#define  FS_NP_IPV6_MC_INIT                                     1
#define  FS_NP_IPV6_MC_DE_INIT                                  2
#define  FS_NP_IPV6_MC_ADD_ROUTE_ENTRY                          3
#define  FS_NP_IPV6_MC_DEL_ROUTE_ENTRY                          4
#define  FS_NP_IPV6_MC_CLEAR_ALL_ROUTES                         5
#define  FS_NP_IPV6_MC_UPDATE_OIF_VLAN_ENTRY                    6
#define  FS_NP_IPV6_MC_UPDATE_IIF_VLAN_ENTRY                    7
#define  FS_NP_IPV6_MC_GET_HIT_STATUS                           8
#define  FS_NP_IPV6_MC_ADD_CPU_PORT                             9
#define  FS_NP_IPV6_MC_DEL_CPU_PORT                             10
#define  FS_NP_IPV6_GET_M_ROUTE_STATS                           11
#define  FS_NP_IPV6_GET_M_ROUTE_H_C_STATS                       12
#define  FS_NP_IPV6_GET_M_NEXT_HOP_STATS                        13
#define  FS_NP_IPV6_GET_M_IFACE_STATS                           14
#define  FS_NP_IPV6_GET_M_IFACE_H_C_STATS                       15
#define  FS_NP_IPV6_SET_M_IFACE_TTL_TRESHOLD                    16
#define  FS_NP_IPV6_SET_M_IFACE_RATE_LIMIT                      17
#define  FS_PIMV6_NP_INIT_HW                                    18
#define  FS_PIMV6_NP_DE_INIT_HW                                 19
#define  FS_NP_IPV6_MC_CLEAR_HIT_BIT                            20
#ifdef MBSM_WANTED
#define  FS_NP_IPV6_MBSM_MC_INIT                                31
#define  FS_NP_IPV6_MBSM_MC_ADD_ROUTE_ENTRY                     32
#define  FS_PIMV6_MBSM_NP_INIT_HW                                 33
#endif
#endif

/* Required arguments list for the ip6mc NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

#ifdef PIMV6_WANTED
typedef struct {
    UINT4               u4VrId;
    UINT1 *             pGrpAddr;
    UINT4               u4GrpPrefix;
    UINT1 *             pSrcIpAddr;
    UINT4               u4SrcIpPrefix;
    tMc6RtEntry         rtEntry;
    tMc6DownStreamIf *  pDownStreamIf;
    UINT2               u2NoOfDownStreamIf;
    UINT1               u1CallerId;
    UINT1               au1Pad[1];
} tIp6mcNpWrFsNpIpv6McAddRouteEntry;

typedef struct {
    UINT4               u4VrId;
    UINT1 *             pGrpAddr;
    UINT4               u4GrpPrefix;
    UINT1 *             pSrcIpAddr;
    UINT4               u4SrcIpPrefix;
    tMc6RtEntry         rtEntry;
    tMc6DownStreamIf *  pDownStreamIf;
    UINT2               u2NoOfDownStreamIf;
    UINT1               au1Pad[2];
} tIp6mcNpWrFsNpIpv6McDelRouteEntry;

typedef struct {
    UINT4  u4VrId;
} tIp6mcNpWrFsNpIpv6McClearAllRoutes;

typedef struct {
    UINT4             u4VrId;
    UINT1 *           pGrpAddr;
    UINT4             u4GrpPrefix;
    UINT1 *           pSrcIpAddr;
    UINT4             u4SrcIpPrefix;
    tMc6RtEntry       rtEntry;
    tMc6DownStreamIf  downStreamIf;
} tIp6mcNpWrFsNpIpv6McUpdateOifVlanEntry;

typedef struct {
    UINT4             u4VrId;
    UINT1 *           pGrpAddr;
    UINT4             u4GrpPrefix;
    UINT1 *           pSrcIpAddr;
    UINT4             u4SrcIpPrefix;
    tMc6RtEntry       rtEntry;
    tMc6DownStreamIf  downStreamIf;
} tIp6mcNpWrFsNpIpv6McUpdateIifVlanEntry;

typedef struct {
    UINT1 *  pSrcIpAddr;
    UINT1 *  pGrpAddr;
    UINT4    u4Iif;
    UINT4 *  pu4HitStatus;
    UINT2    u2VlanId;
    UINT1    au1Pad[2];
} tIp6mcNpWrFsNpIpv6McGetHitStatus;

typedef struct {
    UINT1 *             pGrpAddr;
    UINT4               u4GrpPrefix;
    UINT1 *             pSrcIpAddr;
    UINT4               u4SrcIpPrefix;
    tMc6RtEntry         rtEntry;
    tMc6DownStreamIf *  pDownStreamIf;
    UINT2               u2NoOfDownStreamIf;
    UINT1               u1GenRtrId;
    UINT1               au1Pad[1];
} tIp6mcNpWrFsNpIpv6McAddCpuPort;

typedef struct {
    UINT1 *      pGrpAddr;
    UINT4        u4GrpPrefix;
    UINT1 *      pSrcIpAddr;
    UINT4        u4SrcIpPrefix;
    tMc6RtEntry  rtEntry;
    UINT1        u1GenRtrId;
    UINT1        au1Pad[3];
} tIp6mcNpWrFsNpIpv6McDelCpuPort;

typedef struct {
    UINT4    u4VrId;
    UINT1 *  pu1GrpAddr;
    UINT1 *  pu1SrcAddr;
    INT4     i4StatType;
    UINT4 *  pu4RetValue;
} tIp6mcNpWrFsNpIpv6GetMRouteStats;

typedef struct {
    UINT4                   u4VrId;
    UINT1 *                 pu1GrpAddr;
    UINT1 *                 pu1SrcAddr;
    INT4                    i4StatType;
    tSNMP_COUNTER64_TYPE *  pu8RetValue;
} tIp6mcNpWrFsNpIpv6GetMRouteHCStats;

typedef struct {
    INT4     i4OutIfIndex;
    UINT4    u4VrId;
    UINT1 *  pu1GrpAddr;
    UINT1 *  pu1SrcAddr;
    INT4     i4StatType;
    UINT4 *  pu4RetValue;
} tIp6mcNpWrFsNpIpv6GetMNextHopStats;

typedef struct {
    INT4     i4IfIndex;
    UINT4    u4VrId;
    INT4     i4StatType;
    UINT4 *  pu4RetValue;
} tIp6mcNpWrFsNpIpv6GetMIfaceStats;

typedef struct {
    INT4                    i4IfIndex;
    UINT4                   u4VrId;
    INT4                    i4StatType;
    tSNMP_COUNTER64_TYPE *  pu8RetValue;
} tIp6mcNpWrFsNpIpv6GetMIfaceHCStats;

typedef struct {
    INT4   i4IfIndex;
    UINT4  u4VrId;
    INT4   i4TtlTreshold;
} tIp6mcNpWrFsNpIpv6SetMIfaceTtlTreshold;

typedef struct {
    INT4   i4IfIndex;
    UINT4  u4VrId;
    INT4   i4RateLimit;
} tIp6mcNpWrFsNpIpv6SetMIfaceRateLimit;

#ifdef PIMV6_WANTED
typedef struct {
    UINT1 *  pSrcIpAddr;
    UINT1 *  pGrpAddr;
    UINT2    u2VlanId;
    UINT1    au1Pad[2];
} tIp6mcNpWrFsNpIpv6McClearHitBit;

#endif
#ifdef MBSM_WANTED
typedef struct {
    tMbsmSlotInfo *  pSlotInfo;
} tIp6mcNpWrFsNpIpv6MbsmMcInit;

typedef struct {
    tMc6DownStreamIf *  pDownStreamIf;
    tMbsmSlotInfo *    pSlotInfo;
    tMc6RtEntry         rtEntry;
    UINT4               u4VrId;
    UINT1 *             pGrpAddr;
    UINT4               u4GrpPrefix;
    UINT1 *             pSrcIpAddr;
    UINT4               u4SrcIpPrefix;
    UINT2               u2NoOfDownStreamIf;
    UINT1               u1CallerId;
    UINT1               au1Pad[1];
} tIp6mcNpWrFsNpIpv6MbsmMcAddRouteEntry;

typedef struct {
    tMbsmSlotInfo *  pSlotInfo;
} tIp6mcNpWrFsPimv6MbsmNpInitHw;

#endif 
#endif /* PIMV6_WANTED */
typedef struct Ip6mcNpModInfo {
union {
#ifdef PIMV6_WANTED
    tIp6mcNpWrFsNpIpv6McAddRouteEntry  sFsNpIpv6McAddRouteEntry;
    tIp6mcNpWrFsNpIpv6McDelRouteEntry  sFsNpIpv6McDelRouteEntry;
    tIp6mcNpWrFsNpIpv6McClearAllRoutes  sFsNpIpv6McClearAllRoutes;
    tIp6mcNpWrFsNpIpv6McUpdateOifVlanEntry  sFsNpIpv6McUpdateOifVlanEntry;
    tIp6mcNpWrFsNpIpv6McUpdateIifVlanEntry  sFsNpIpv6McUpdateIifVlanEntry;
    tIp6mcNpWrFsNpIpv6McGetHitStatus  sFsNpIpv6McGetHitStatus;
    tIp6mcNpWrFsNpIpv6McAddCpuPort  sFsNpIpv6McAddCpuPort;
    tIp6mcNpWrFsNpIpv6McDelCpuPort  sFsNpIpv6McDelCpuPort;
    tIp6mcNpWrFsNpIpv6GetMRouteStats  sFsNpIpv6GetMRouteStats;
    tIp6mcNpWrFsNpIpv6GetMRouteHCStats  sFsNpIpv6GetMRouteHCStats;
    tIp6mcNpWrFsNpIpv6GetMNextHopStats  sFsNpIpv6GetMNextHopStats;
    tIp6mcNpWrFsNpIpv6GetMIfaceStats  sFsNpIpv6GetMIfaceStats;
    tIp6mcNpWrFsNpIpv6GetMIfaceHCStats  sFsNpIpv6GetMIfaceHCStats;
    tIp6mcNpWrFsNpIpv6SetMIfaceTtlTreshold  sFsNpIpv6SetMIfaceTtlTreshold;
    tIp6mcNpWrFsNpIpv6SetMIfaceRateLimit  sFsNpIpv6SetMIfaceRateLimit;
    tIp6mcNpWrFsNpIpv6McClearHitBit  sFsNpIpv6McClearHitBit;
#ifdef MBSM_WANTED
    tIp6mcNpWrFsNpIpv6MbsmMcInit  sFsNpIpv6MbsmMcInit;
    tIp6mcNpWrFsNpIpv6MbsmMcAddRouteEntry  sFsNpIpv6MbsmMcAddRouteEntry;
    tIp6mcNpWrFsPimv6MbsmNpInitHw  sFsPimv6MbsmNpInitHw;
#endif
#endif /* PIMV6_WANTED */
    }unOpCode;

#ifdef PIMV6_WANTED
#define  Ip6mcNpFsNpIpv6McAddRouteEntry  unOpCode.sFsNpIpv6McAddRouteEntry;
#define  Ip6mcNpFsNpIpv6McDelRouteEntry  unOpCode.sFsNpIpv6McDelRouteEntry;
#define  Ip6mcNpFsNpIpv6McClearAllRoutes  unOpCode.sFsNpIpv6McClearAllRoutes;
#define  Ip6mcNpFsNpIpv6McUpdateOifVlanEntry  unOpCode.sFsNpIpv6McUpdateOifVlanEntry;
#define  Ip6mcNpFsNpIpv6McUpdateIifVlanEntry  unOpCode.sFsNpIpv6McUpdateIifVlanEntry;
#define  Ip6mcNpFsNpIpv6McGetHitStatus  unOpCode.sFsNpIpv6McGetHitStatus;
#define  Ip6mcNpFsNpIpv6McAddCpuPort  unOpCode.sFsNpIpv6McAddCpuPort;
#define  Ip6mcNpFsNpIpv6McDelCpuPort  unOpCode.sFsNpIpv6McDelCpuPort;
#define  Ip6mcNpFsNpIpv6GetMRouteStats  unOpCode.sFsNpIpv6GetMRouteStats;
#define  Ip6mcNpFsNpIpv6GetMRouteHCStats  unOpCode.sFsNpIpv6GetMRouteHCStats;
#define  Ip6mcNpFsNpIpv6GetMNextHopStats  unOpCode.sFsNpIpv6GetMNextHopStats;
#define  Ip6mcNpFsNpIpv6GetMIfaceStats  unOpCode.sFsNpIpv6GetMIfaceStats;
#define  Ip6mcNpFsNpIpv6GetMIfaceHCStats  unOpCode.sFsNpIpv6GetMIfaceHCStats;
#define  Ip6mcNpFsNpIpv6SetMIfaceTtlTreshold  unOpCode.sFsNpIpv6SetMIfaceTtlTreshold;
#define  Ip6mcNpFsNpIpv6SetMIfaceRateLimit  unOpCode.sFsNpIpv6SetMIfaceRateLimit;
#define  Ip6mcNpFsNpIpv6McClearHitBit  unOpCode.sFsNpIpv6McClearHitBit;
#ifdef MBSM_WANTED
#define  Ip6mcNpFsNpIpv6MbsmMcInit  unOpCode.sFsNpIpv6MbsmMcInit;
#define  Ip6mcNpFsNpIpv6MbsmMcAddRouteEntry  unOpCode.sFsNpIpv6MbsmMcAddRouteEntry;
#define  Ip6mcNpFsPimv6MbsmNpInitHw  unOpCode.sFsPimv6MbsmNpInitHw;
#endif
#endif /* PIMV6_WANTED */
} tIp6mcNpModInfo;


/* Function Prototype for Generic NP OpCode specific wrappers
 * defined in Ip6mcnpapi.c */

#ifdef PIMV6_WANTED
UINT1 Ip6mcFsNpIpv6McInit PROTO ((VOID));
UINT1 Ip6mcFsNpIpv6McDeInit PROTO ((VOID));
UINT1 Ip6mcFsNpIpv6McAddRouteEntry PROTO ((UINT4 u4VrId, UINT1 * pGrpAddr, UINT4 u4GrpPrefix, UINT1 * pSrcIpAddr, UINT4 u4SrcIpPrefix, UINT1 u1CallerId, tMc6RtEntry rtEntry, UINT2 u2NoOfDownStreamIf, tMc6DownStreamIf * pDownStreamIf));
UINT1 Ip6mcFsNpIpv6McDelRouteEntry PROTO ((UINT4 u4VrId, UINT1 * pGrpAddr, UINT4 u4GrpPrefix, UINT1 * pSrcIpAddr, UINT4 u4SrcIpPrefix, tMc6RtEntry rtEntry, UINT2 u2NoOfDownStreamIf, tMc6DownStreamIf * pDownStreamIf));
UINT1 Ip6mcFsNpIpv6McClearAllRoutes PROTO ((UINT4 u4VrId));
UINT1 Ip6mcFsNpIpv6McUpdateOifVlanEntry PROTO ((UINT4 u4VrId, UINT1 * pGrpAddr, UINT4 u4GrpPrefix, UINT1 * pSrcIpAddr, UINT4 u4SrcIpPrefix, tMc6RtEntry rtEntry, tMc6DownStreamIf downStreamIf));
UINT1 Ip6mcFsNpIpv6McUpdateIifVlanEntry PROTO ((UINT4 u4VrId, UINT1 * pGrpAddr, UINT4 u4GrpPrefix, UINT1 * pSrcIpAddr, UINT4 u4SrcIpPrefix, tMc6RtEntry rtEntry, tMc6DownStreamIf downStreamIf));
UINT1 Ip6mcFsNpIpv6McGetHitStatus PROTO ((UINT1 * pSrcIpAddr, UINT1 * pGrpAddr, UINT4 u4Iif, UINT2 u2VlanId, UINT4 * pu4HitStatus));
UINT1 Ip6mcFsNpIpv6McAddCpuPort PROTO ((UINT1 u1GenRtrId, UINT1 * pGrpAddr, UINT4 u4GrpPrefix, UINT1 * pSrcIpAddr, UINT4 u4SrcIpPrefix, tMc6RtEntry rtEntry, UINT2 u2NoOfDownStreamIf, tMc6DownStreamIf * pDownStreamIf));
UINT1 Ip6mcFsNpIpv6McDelCpuPort PROTO ((UINT1 u1GenRtrId, UINT1 * pGrpAddr, UINT4 u4GrpPrefix, UINT1 * pSrcIpAddr, UINT4 u4SrcIpPrefix, tMc6RtEntry rtEntry));
UINT1 Ip6mcFsNpIpv6GetMRouteStats PROTO ((UINT4 u4VrId, UINT1 * pu1GrpAddr, UINT1 * pu1SrcAddr, INT4 i4StatType, UINT4 * pu4RetValue));
UINT1 Ip6mcFsNpIpv6GetMRouteHCStats PROTO ((UINT4 u4VrId, UINT1 * pu1GrpAddr, UINT1 * pu1SrcAddr, INT4 i4StatType, tSNMP_COUNTER64_TYPE * pu8RetValue));
UINT1 Ip6mcFsNpIpv6GetMNextHopStats PROTO ((UINT4 u4VrId, UINT1 * pu1GrpAddr, UINT1 * pu1SrcAddr, INT4 i4OutIfIndex, INT4 i4StatType, UINT4 * pu4RetValue));
UINT1 Ip6mcFsNpIpv6GetMIfaceStats PROTO ((UINT4 u4VrId, INT4 i4IfIndex, INT4 i4StatType, UINT4 * pu4RetValue));
UINT1 Ip6mcFsNpIpv6GetMIfaceHCStats PROTO ((UINT4 u4VrId, INT4 i4IfIndex, INT4 i4StatType, tSNMP_COUNTER64_TYPE * pu8RetValue));
UINT1 Ip6mcFsNpIpv6SetMIfaceTtlTreshold PROTO ((UINT4 u4VrId, INT4 i4IfIndex, INT4 i4TtlTreshold));
UINT1 Ip6mcFsNpIpv6SetMIfaceRateLimit PROTO ((UINT4 u4VrId, INT4 i4IfIndex, INT4 i4RateLimit));
UINT1 Ip6mcFsPimv6NpInitHw PROTO ((VOID));
UINT1 Ip6mcFsPimv6NpDeInitHw PROTO ((VOID));
UINT1 Ip6mcFsNpIpv6McClearHitBit PROTO ((UINT2 u2VlanId, UINT1 * pSrcIpAddr, UINT1 * pGrpAddr));
#ifdef MBSM_WANTED
UINT1 Ip6mcFsNpIpv6MbsmMcInit PROTO ((tMbsmSlotInfo * pSlotInfo));
UINT1 Ip6mcFsNpIpv6MbsmMcAddRouteEntry PROTO ((UINT4 u4VrId, UINT1 * pGrpAddr, UINT4 u4GrpPrefix, UINT1 * pSrcIpAddr, UINT4 u4SrcIpPrefix, UINT1 u1CallerId, tMc6RtEntry rtEntry, UINT2 u2NoOfDownStreamIf, tMc6DownStreamIf * pDownStreamIf, tMbsmSlotInfo * pSlotInfo));
UINT1 Ip6mcFsPimv6MbsmNpInitHw PROTO ((tMbsmSlotInfo * pSlotInfo));
#endif
#endif /* PIMV6_WANTED */
#endif
