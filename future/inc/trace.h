/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: trace.h,v 1.22 2017/09/20 13:07:38 siva Exp $
 *
 * Description:This file contains the exported definitions and  
 *             macros of Trace                                 
 *
 *******************************************************************/
#ifndef _TRACE_H_
#define _TRACE_H_

/* Common bitmasks for various events. This has to be implemented in all the 
 * modules in the same fashion
 */
#define   INIT_SHUT_TRC       0x00000001 
#define   MGMT_TRC            0x00000002 
#define   DATA_PATH_TRC       0x00000004 
#define   CONTROL_PLANE_TRC   0x00000008 
#define   DATA_CTRL_PLANE_TRC 0xffffffff
#define   NO_TRC              0x00000000
#define   DUMP_TRC            0x00000010
#define   OS_RESOURCE_TRC     0x00000020 
#define   ALL_FAILURE_TRC     0x00000040 
#define   BUFFER_TRC          0x00000080 
#define   ENTRY_TRC           0x00000100
#define   EXIT_TRC            0x00000200 
#define   RX_DUMP_TRC         0x00000400 
#define   TX_DUMP_TRC         0x00000800 
#define   RIP_CRITICAL_TRC    0x00001000

#ifdef TRACE_WANTED
PUBLIC void DumpPkt ARG_LIST((tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4Length));

#define MOD_PKT_DUMP(u4DbgVar, mask, moduleName, pBuf, Length, fmt) \
  { UtlTrcLog(u4DbgVar, mask , moduleName, fmt); \
    if ((u4DbgVar & mask) == DUMP_TRC) \
   DumpPkt(pBuf, Length); \
  }  

#ifdef __GNUC__
#define MOD_FN_ENTRY(u4DbgVar,mask,moduleName) \
          UtlTrcLog (u4DbgVar, mask, moduleName, "Entering Fn : %s\r\n", \
                     __FUNCTION__);

#define MOD_FN_ENTR(u4DbgVar,mask,moduleName,fmt) \
          UtlTrcLog (u4DbgVar, mask, moduleName,"Entering Fn : %s\r\n", \
                     __FUNCTION__);

#define MOD_FN_EXIT(u4DbgVar,mask,moduleName) \
          UtlTrcLog (u4DbgVar, mask, moduleName, "Exiting Fn %s Line No %d\r\n", \
                     __FUNCTION__ , __LINE__);
#else

#define MOD_FN_ENTRY(u4DbgVar,mask,moduleName) \
          UtlTrcLog (u4DbgVar, mask, moduleName, "Entering Fn : %s\r\n", \
                     __FILE__);

#define MOD_FN_EXIT(u4DbgVar,mask,moduleName) \
          UtlTrcLog (u4DbgVar, mask, moduleName, "Exiting Fn %s\r\n", \
                     __FILE__);
#endif

#define MOD_TRC(u4DbgVar,mask,moduleName,fmt)  UtlTrcLog(u4DbgVar, \
                                                mask,               \
                                                moduleName,         \
                                                fmt)


#define MOD_TRC_ARG1(u4DbgVar, mask, moduleName, fmt, arg1)        \
             UtlTrcLog(u4DbgVar, \
             mask,    \
             moduleName,   \
             fmt,    \
             arg1)

#define MOD_TRC_ARG2(u4DbgVar, mask, moduleName,fmt,arg1,arg2)      \
             UtlTrcLog(u4DbgVar, \
             mask,    \
             moduleName,   \
             fmt,    \
             arg1,    \
             arg2)
             
#define MOD_TRC_ARG3(u4DbgVar, mask, moduleName,fmt,arg1,arg2,arg3) \
             UtlTrcLog(u4DbgVar, \
             mask,    \
             moduleName,   \
             fmt,    \
             arg1,    \
             arg2,    \
             arg3)
             
#define MOD_TRC_ARG4(u4DbgVar, mask, moduleName,fmt,arg1,arg2,arg3,arg4) \
             UtlTrcLog(u4DbgVar, \
             mask,    \
             moduleName,   \
             fmt,    \
             arg1,    \
             arg2,    \
             arg3,    \
             arg4)
             
#define MOD_TRC_ARG5(u4DbgVar, mask, moduleName,fmt,arg1,arg2,arg3,arg4,arg5) \
             UtlTrcLog(u4DbgVar, \
             mask,    \
             moduleName,   \
             fmt,    \
             arg1,    \
             arg2,    \
             arg3,    \
             arg4,    \
             arg5)
             
#define MOD_TRC_ARG6(u4DbgVar, mask, moduleName,fmt,arg1,arg2,arg3,arg4,arg5,arg6) \
             UtlTrcLog(u4DbgVar, \
             mask,    \
             moduleName,   \
             fmt,    \
             arg1,    \
             arg2,    \
             arg3,    \
             arg4,    \
             arg5,    \
             arg6)

#define MOD_TRC_ARG7(u4DbgVar, mask, moduleName,fmt,arg1,arg2,arg3,arg4,arg5,arg6,arg7) \
             UtlTrcLog(u4DbgVar, \
             mask,    \
             moduleName,   \
             fmt,    \
             arg1,    \
             arg2,    \
             arg3,    \
             arg4,    \
             arg5,    \
             arg6,    \
             arg7)

#else

#define MOD_FN_ENTRY(u4DbgVar,mask,moduleName)
#define MOD_FN_EXIT(u4DbgVar,mask,moduleName)

#define MOD_PKT_DUMP(u4DbgVar, mask, moduleName, pBuf, Length, fmt) 
#define MOD_TRC(u4DbgVar,mask, moduleName,fmt)

#define MOD_TRC_ARG1(u4DbgVar, mask, moduleName,fmt,arg1)        
#define MOD_TRC_ARG2(u4DbgVar, mask, moduleName,fmt,arg1,arg2)        
#define MOD_TRC_ARG3(u4DbgVar, mask, moduleName,fmt,arg1,arg2,arg3)        
#define MOD_TRC_ARG4(u4DbgVar, mask, moduleName,fmt,arg1,arg2,arg3,arg4)        
#define MOD_TRC_ARG5(u4DbgVar, mask, moduleName,fmt,arg1,arg2,arg3,arg4,arg5)        
#define MOD_TRC_ARG6(u4DbgVar, mask, moduleName,fmt,arg1,arg2,arg3,arg4,arg5,arg6)        
#define MOD_TRC_ARG7(u4DbgVar, mask, moduleName,fmt,arg1,arg2,arg3,arg4,arg5,arg6,arg7) 

#endif /* TRACE_WANTED */
#endif /* _TRACE_H_ */

/****************************************************************************/
/*           How to use the Trace Statement                                 */
/*                                                                          */
/* 1) Define a UINT4 Trace variable. (eg :u4xxx)                            */
/*                                                                          */
/* 2) The Trace statements can be used as                                   */
/*                                                                          */
/*           MOD_TRC(u4DbgVar, mask, module name, fmt)                      */
/*           MOD_TRC(u4xxx, INIT_SHUT_TRC, XXX, "Trace Statement")          */
/*                                                                          */
/****************************************************************************/
