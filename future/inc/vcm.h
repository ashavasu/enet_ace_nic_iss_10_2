/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vcm.h,v 1.61 2017/11/14 07:31:11 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              by external modules.
 ********************************************************************/

#ifndef _VCM_H
#define _VCM_H

#ifdef L2RED_WANTED
#include "rmgr.h"
#endif
#include "mbsm.h"
#include "fsvlan.h"
#include "l2iwf.h"

/* ---------------------------------------------- */
/* Structure declarations used by layer 3 modules */
/* ---------------------------------------------- */

typedef  struct  _VcmRegInfo
{
    VOID (*pIfMapChngAndCxtChng) 
        (UINT4 u4IpIfIndex, UINT4 u4VcmCxtId, UINT1 u1CxtChgType);
                               /* Call back function */
    UINT1       u1InfoMask;
    /* This mask can take the following values */
    #define     VCM_IF_MAP_CHG_REQ               1
    #define     VCM_CXT_STATUS_CHG_REQ           2
    #define     VCM_CONTEXT_CREATE               4
    #define     VCM_CONTEXT_DELETE               2 /* This value is kept as 2 
                                                      ( value same as that of                                          existing macro VCM_CXT_STATUS_CHG_REQ) to
                                                      ensure backward compatability
                                                      for rip and ospf */

    UINT1       u1ProtoId;      /* Registering Protocol's Identifier */
    INT1        ai1Align[2];    /* Padding for alignment */
}tVcmRegInfo;

typedef struct _tContextMapInfo{
    UINT2 u2LocalPortId;
    UINT1 u1IsPrimaryCtx;
    UINT1 au1Reserved[1];
}tContextMapInfo;

typedef struct  _tVrfCounters {
   UINT4 u4VrfId;
   UINT4 *pu4VrfStatsValue;
   UINT1 u1StatsType;
   UINT1 u1pad[3];

}tVrfCounter;

/* ---------------------------------------------- */
/* Prototype declarations called by other modules */
/* ---------------------------------------------- */

#define VCM_FAILURE                  OSIX_FAILURE
#define VCM_SUCCESS                  OSIX_SUCCESS

#define VCM_TRUE                      TRUE
#define VCM_FALSE                     FALSE

#define VCM_SI_MODE                   1
#define VCM_MI_MODE                   2

#define VCM_ONE                       1
#define VCM_ZERO        0

#define SISP_MAX_VLAN_PER_BLOCK 512

#define SISP_NUMBER_OF_VLAN_BLOCK \
     (VLAN_MAX_VLAN_ID + SISP_MAX_VLAN_PER_BLOCK - 1) / SISP_MAX_VLAN_PER_BLOCK

#define SISP_START 1
#define SISP_SHUTDOWN 2

#define SISP_ENABLE L2IWF_ENABLED
#define SISP_DISABLE L2IWF_DISABLED

#define SISP_ADD 1
#define SISP_DELETE 2

/* Macro defines the Maximum value of IfIndex supported by VCM module */
#define VCM_MAX_IFINDEX           CFA_MAX_VIP_IF_INDEX

#define VCM_SISP_MIN_LOGICAL_IFINDEX CFA_MIN_SISP_IF_INDEX
#define VCM_SISP_MAX_LOGICAL_IFINDEX CFA_MAX_SISP_IF_INDEX

#define SISP_IS_LOGICAL_PORT(u4IfIdx) \
    (((u4IfIdx >= VCM_SISP_MIN_LOGICAL_IFINDEX) && \
      (u4IfIdx <= VCM_SISP_MAX_LOGICAL_IFINDEX)) ? VCM_TRUE : VCM_FALSE)

#define VCM_IFACE_MAP                 1
#define VCM_IFACE_UNMAP               2

#define VCM_LOCK()                    VcmLock()
#define VCM_UNLOCK()                  VcmUnLock()

#define VCM_CONF_LOCK()               VcmConfLock()
#define VCM_CONF_UNLOCK()             VcmConfUnLock()

#define VCM_TASK_NAME                 ((const UINT1*)"VcmT")
#define VCM_TASK_PRIORITY             23

#define VCM_CTRL_QUEUE                ((UINT1*) "VcmQ")
#define VCM_LNX_VRF_QUEUE             ((UINT1*) "VcmLnxVrfQ")
#define VCM_CTRLQ_DEPTH               BRG_NUM_PHY_PLUS_LAG_INT_PORTS
#define VCM_CTRLQ_MODE                0

#define VCM_L2_CONTEXT               1
#define VCM_L3_CONTEXT               2
#define VCM_L2_L3_CONTEXT            3


  /* The Macro Is used in VCM and Web Module */
#define VCM_ALIAS_MAX_LEN    36
#define VCMALIAS_MAX_ARRAY_LEN    VCM_ALIAS_MAX_LEN + 4

#define VCM_INVALID_CONTEXT_ID  (L2IWF_MAX_CONTEXTS + 1)
/* Maximum number allowed for context-ID*/
#define MAX_CXT_PER_SYS               SYS_DEF_MAX_NUM_CONTEXTS - 1

#define MAX_PORTS_PER_CXT             (SYS_DEF_MAX_PORTS_PER_CONTEXT + \
                                       SYS_DEF_MAX_L2_PSW_IFACES + \
                                       SYS_DEF_MAX_L2_AC_IFACES + \
                                       SYS_DEF_MAX_NVE_IFACES + \
                                       MAX_TAP_INTERFACES + \
                                        SYS_DEF_MAX_SBP_IFACES)

#define VCM_OWNER_MAX_SIZE         17
#define SISP_ALWAYS                 1

#define VCM_VIRTUAL_INSTANCE_PORT         CFA_VIRTUAL_INSTANCE_PORT
#define VCM_MAX_L3_CONTEXT           32
#define VCM_INVALID_VC       0xffffffff
#define VCM_DEFAULT_CONTEXT  0 

#define VRF_STAT_ENABLE 1
#define VRF_STAT_DISABLE 2
#define VRF_CREATE_COUNTER 1
#define VRF_DELETE_COUNTER 2
#define VRF_RESET_COUNTER 3
#define VRF_GET_UNICAST_STAT 4
#define VRF_GET_MULTICAST_STAT 5
#define VRF_GET_BROADCAST_STAT 6

/*Length of switch mode string - "(config-switch)#" */
#define CLI_VCM_VC_MODE_LEN  18

/* Unique Identifier for each of the protocols. Add L3 protocols ID at the end.
 * Add L2 protocols ID before L3_PROTOCOL_ID. Don't rearrange the existing
 * L3 protocol Ids. L3 Protocol ID must be in the order in which the protocol
 * tasks are created from LR.*/
enum
{
    BRIDGE_PROTOCOL_ID = 1,
    VCM_PROTOCOL_ID,
    CFA_PROTOCOL_ID, 
    L2IWF_PROTOCOL_ID, 
    PNAC_PROTOCOL_ID,
    LA_PROTOCOL_ID,
    VLANMOD_PROTOCOL_ID,
    GARP_PROTOCOL_ID,
    MRP_PROTOCOL_ID,
    SNOOP_PROTOCOL_ID,
    AST_PROTOCOL_ID,
    MSR_PROTOCOL_ID,
    MPLS_PROTOCOL_ID,
    ISS_PROTOCOL_ID,
    ECFM_PROTOCOL_ID,
    PBB_PROTOCOL_ID, 
    EOAM_PROTOCOL_ID,
    CN_PROTOCOL_ID,
    IPDB_PROTOCOL_ID,
    L2DS_PROTOCOL_ID,
    RBRG_PROTOCOL_ID,
    Y1564_PROTOCOL_ID,
    L3_PROTOCOL_ID, 
    RTM6_PROTOCOL_ID,
    IP6_FWD_PROTOCOL_ID,
    PING6_PROTOCOL_ID,
    RTM_PROTOCOL_ID,
    IPFWD_PROTOCOL_ID,
    UDP_PROTOCOL_ID,
    ARP_PROTOCOL_ID,
    PING_PROTOCOL_ID,
    RIP_PROTOCOL_ID,
    OSPF_PROTOCOL_ID,
    OSPF3_PROTOCOL_ID,
    RIP6_PROTOCOL_ID,
    IGMP_PROTOCOL_ID,
    PIM_PROTOCOL_ID,
    DVMRP_PROTOCOL_ID,
    ISIS_PROTOCOL_ID,
    BFD_PROTOCOL_ID,
    LSPP_PROTOCOL_ID,
    RFC6374_PROTOCOL_ID,
    IP_TCP_PROTOCOL_ID,
    BGP_PROTOCOL_ID,
    L3VPN_PROTOCOL_ID,
    DHCP_RLY_PROTOCOL_ID,
 RFC2544_PROTOCOL_ID,
    MAX_PROTOCOL_ID
};

VOID VcmTaskMain PROTO ((INT1 *));

INT4 VcmLock PROTO ((VOID));

INT4 VcmUnLock PROTO ((VOID));

INT4 VcmConfLock PROTO ((VOID));

INT4 VcmConfUnLock PROTO ((VOID));

INT4 VcmInit PROTO ((VOID));

INT4 VcmGetSystemMode PROTO ((UINT2 u2ProtocolId));
INT4 VcmGetSystemModeExt PROTO ((UINT2 u2ProtocolId));

INT4 VcmGetContextInfoFromIfIndex PROTO ((UINT4 u4IfIndex, UINT4 *pu4CxtId, 
                              UINT2 *pu2HlPort));

INT4 VcmGetFirstActiveContext PROTO ((UINT4 *pu4CxtId));

INT4 VcmGetNextActiveContext PROTO ((UINT4 u4CxtId, UINT4 *pu4CxtId));

INT4 VcmGetNextActiveL2Context PROTO ((UINT4 u4CxtId, UINT4 *pu4CxtId));

INT4 VcmGetFirstActiveL3Context PROTO ((UINT4 *pu4ContextId));

INT4 VcmGetNextActiveL3Context PROTO ((UINT4 u4ContextId, 
                                       UINT4 *pu4NextContextId));

INT4 VcmIsSwitchExist PROTO ((UINT1 *pu1Alias, UINT4 *pu4VcNum));

INT4 VcmIsVrfExist PROTO ((UINT1 *pu1Alias, UINT4 *pu4VcNum));

INT4 VcmGetAliasName PROTO ((UINT4, UINT1 *));

INT4 VcmMapPortToContextInHw PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex));

INT4 VcmUnMapPortFromContextInHw PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex));

INT4
VcmUnMapPortFromContext PROTO ((UINT4 u4IfIndex));

INT4
VcmGetIfIndexFromLocalPort PROTO ((UINT4 u4ContextId, UINT2 u2LocalPort, 
              UINT4 *pu4IfIndex));

INT4
VcmUpdateIpPortNumber PROTO ((UINT4 u4CfaIfIndex, UINT4 u4IpPortNum));

INT4
VcmGetL2CxtIdForIpIface PROTO ((UINT4 u4IfIndex, UINT4 *pu4L2CxtId));

INT4
VcmGetIpIfaceCount PROTO ((UINT4 u4ContextId, UINT4 *pu4IfCount));

#ifdef L2RED_WANTED
INT4 VcmHandleUpdateEvents PROTO ((UINT1, tRmMsg *, UINT2));
#endif

#ifdef MBSM_WANTED
INT4 VcmMbsmProcessUpdate PROTO ((tMbsmPortInfo * pPortInfo, 
                                  tMbsmSlotInfo * pSlotInfo, INT4 i4Event));
#endif /* MBSM_WANTED */
INT4
VcmIsVcExist PROTO ((UINT4 ));

INT4
VcmIsL2VcExist PROTO ((UINT4 u4ContextId));

/* Layer 3 support */
INT4 VcmGetContextInfoFromIpIfIndex PROTO ((UINT4 u4IpIfIndex, UINT4 *pu4CxtId));

INT4 VcmGetContextIdFromCfaIfIndex PROTO ((UINT4 u4CfaIfIndex, UINT4 *pu4CxtId));

INT4 VcmRegisterHLProtocol PROTO ((tVcmRegInfo * pVcmRegInfo));

INT4 VcmDeRegisterHLProtocol PROTO ((UINT1 u1ProtoId));

VOID VcmIpInterfaceDelete PROTO ((UINT4 u4IpIfIndex));
 
INT4 VcmGetFirstIpIfaceInCxt PROTO ((UINT4 u4ContextId, UINT4 *pu4IfIndex));

INT4 VcmGetNextIpIfaceInCxt PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, 
                                    UINT4 *pu4IfIndex));

INT4
VcmModuleShutdown PROTO ((VOID));
 
INT4
VcmModuleStart PROTO ((VOID));

VOID VcmCfaCreateIfaceMapping PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex));

INT4
VcmCfaCreateIPIfaceMapping PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex));

INT4
VcmCfaMakeIPIfaceMappingActive PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex));

INT4
VcmCfaDeleteIPIfaceMapping PROTO ((UINT4 u4IfIndex, UINT1 u1Status));


INT4
VcmCfaDeleteInterfaceMapping PROTO ((UINT4 u4IfIndex));

INT4
VcmIsL3VcExist PROTO ((UINT4 u4ContextId));

INT4
VcmIsIfMapExist PROTO ((UINT4 ));

INT4
VcmGetIfMapVcId PROTO ((UINT4 , UINT4 *));

INT4
VcmGetIfMapHlPortId PROTO ((UINT4 , UINT2 *));

INT4
VcmTestv2FsVcIfRowStatus PROTO ((
        UINT4 *pu4ErrorCode,
        INT4 u4IfIndex,
        INT4 u4RowStatus
        ));
INT4
VcmSetFsVcIfRowStatus PROTO ((
        INT4 u4IfIndex,
        INT4 u4RowStatus
        ));
INT1
VcmSetFsVcId PROTO((
            INT4 i4FsVcmIfIndex, 
            INT4 i4SetValFsVcId));


INT1
VcmTestFsVcId PROTO((
            UINT4 *pu4ErrorCode, 
            INT4 i4FsVcmIfIndex, 
            INT4 i4TestValFsVcId));

INT4
VcmGetContextPortList PROTO ((UINT4, tPortList));

INT4 
VcmSispGetContextInfoForPortVlan PROTO ((UINT4 u4PhyIfIndex, 
                                         tVlanId VlanId,
                                         UINT4 *pu4ContextId,
                                         UINT4 *pu4LogicalIfIndex,
                                         UINT2 *pu2LocalPortId));

INT4
VcmSispUpdatePortVlanMapping PROTO ((UINT4 u4ContextId, tVlanId  VlanId,
         tLocalPortList AddedPorts,
         tLocalPortList DeletedPorts));

INT4 
VcmSispUpdatePortVlanMappingOnPort PROTO ((UINT4 u4ContextId, tVlanId  VlanId, 
        UINT4 u4IfIndex, UINT1 u1Status));

INT4
VcmSispDeletePortVlanMapEntriesForPort PROTO ((UINT4 u4IfIndex));

INT4 
VcmSispIsPortVlanMappingValid PROTO ((UINT4 u4ContextId, tVlanId  VlanId, 
          tLocalPortList AddedPorts));

INT4 
VcmSispGetPhysicalPortOfSispPort PROTO ((UINT4 u4LogicalIfIndex, 
      UINT4 *pu4PhysicalIndex));

INT4
VcmSispGetSispPortsInfoOfPhysicalPort PROTO ((UINT4 u4PhyIfIndex, 
           UINT1 u1RetLocalPorts,
           VOID *paSispPorts, 
           UINT4 *pu4PortCnt));
INT1
VcmSispIsSispPortPresentInCtxt PROTO ((UINT4 u4ContextId));

INT4
VcmSispGetSispPortOfPhysicalPortInCtx PROTO ((UINT4 u4PhyIfIndex, 
                                              UINT4 u4ContextId,
                                              UINT4 *pu4SispPort, 
                                              UINT2 * pu2LocalPort));

INT4
VcmSispUpdatePortVlanMappingInHw PROTO ((UINT4 u4ContextId, tVlanId VlanId,
                                         tLocalPortList PortList, 
                                         UINT1 u1Action));

INT4
VcmSispMbsmProcessUpdate (tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo,
     INT4 i4Event);

INT4
VcmSispPortAddedToPortChannel (UINT4 u4IfIndex);

INT4
VcmGetCxtIpIfaceList PROTO ((UINT4 u4ContextId, UINT4 *pu4IfIndexArray));

INT1
VcmSetL2CxtId PROTO ((UINT4 u4IfIndex, UINT4 u4L2CxtId));

INT1 
VcmTestL2CxtId PROTO ((UINT4 *pu4ErrorCode, UINT4 u4IfIndex, UINT4 u4L2CxtId));

INT1
VcmCliGetL2CxtNameForIpInterface PROTO ((UINT4 u4IfIndex, UINT1 *pu1L2CxtName));

INT1
VcmGetIfIndexFrmL2CxtIdAndVlanId PROTO ((UINT4 u4L2ContextId, UINT2 u2VlanId,
                                         UINT4 *pu4IfIndex));

INT4
VcmIsL3Interface PROTO ((UINT4 u4IfIndex));

INT4
VcmGetL2Mode PROTO ((VOID));

INT4
VcmGetL3Mode PROTO ((VOID));

INT4
VcmGetL2ModeExt PROTO ((VOID));

INT4
VcmGetL3ModeExt PROTO ((VOID));

INT4
VcmGetIfMapCfgVcId PROTO ((UINT4 u4IfIndex, UINT4 *pu4CfgCxtId));

INT4
VcmGetVcCxtType PROTO ((UINT4 u4ContextId, INT4 *pi4CxtType));

INT1
VcmGetVCMacAddr PROTO ((INT4 i4VCId, UINT1 *pu1BridgeAddr));

INT1
VcmGetVRMacAddr PROTO ((UINT4 u4VRId, UINT1 *pu1MacAddr));

INT4
VcmMapPortExt (UINT4 u4IfIndex, UINT1 *pu1Alias);

INT4
VcmUnMapPortExt (UINT4 u4IfIndex, UINT1 *pu1Alias);

UINT4
VcmIsL3SubIfParentPort (UINT4 u4IfIndex);

INT4
VcmGetVcNextFreeHlPortIdExt (UINT4 u4ContextId, UINT2 *pu2LocalPortId);
#if defined (LNXIP4_WANTED) && defined (VRF_WANTED) && defined (LINUX_310_WANTED)
INT1
VcmHandleStatusForLnxVrf (UINT4 u4VrfId, UINT1 u1MsgType, UINT4 u4status,UINT4 u4IfIndex);
#endif
INT4 VcmGetVrfCounterStatus(UINT4 u4ContextId, UINT1 * pu1CounterStatus);
INT4 VcmSetVrfCounterStatus(UINT4 u4ContextId, UINT1 u1Status);
#endif /* _VCM_H */
