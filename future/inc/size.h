/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: size.h,v 1.73 2018/01/30 14:25:44 siva Exp $
 *
 * Description:This file contains the system sizing object  
 *             definition for the entire system            
 *
 *******************************************************************/
#ifndef _SIZE_H
#define _SIZE_H

/* Refer to the appropriate file for tuning the system sizing parameters 
 * Any new sizing parameter introduced should be added for all the envirnnments
 * e.g BCM5690 , BCM5695 , Linux etc.
 */

#ifdef ENET_ADAPTOR
#include "enetadaptor.h"
#endif

/* The following macro defines the maximum number of ports that can be present
 * in each of the attached Line Cards. The List of Line Cards supported can be
 * configured in the Chassis header file */
#define  MBSM_MAX_PORTS_PER_SLOT    (MBSM_MAX_POSSIBLE_PORTS_PER_SLOT - IssGetStackPortCountFromNvRam())
#define  MBSM_MAX_POSSIBLE_PORTS_PER_SLOT                 28
#ifdef NPAPI_WANTED
#ifdef MBSM_WANTED /* Multi board */

#ifdef STACKING_WANTED
/* The following parameters are defined as per PIZZABOX stacking concept */

#define  MBSM_MAX_SLOTS                                   4 
#define  MBSM_MAX_LC_SLOTS                                2 
#define  MBSM_MAX_CC_SLOTS                                2
#define  MBSM_SLOT_ALL                   MBSM_MAX_SLOTS + 1

#else /* STACKING_WANTED */
/* The following parameters are defined as per Chassis concept */

#define  MBSM_MAX_SLOTS                                   6 
#define  MBSM_MAX_LC_SLOTS                                4 
#define  MBSM_MAX_CC_SLOTS                                2
#define  MBSM_SLOT_ALL                   MBSM_MAX_SLOTS + 1
#define  MBSM_MAX_POSSIBLE_PORTS_PER_SLOT                 28

#endif /* STACKING_WANTED */

#ifdef LINUXSIM_WANTED
#ifdef METROE_WANTED
#include "isslnxme.h"
#else
#include "isslnx.h"
#endif
#else
#ifdef NPSIM
#include "npsiminc.h"
#else
#ifdef DX167
#include "dx167.h"
#else
#if defined (XCAT) || defined (XCAT3)
#include "xcat.h"
#endif
#ifdef WINTEGRA
#include "wintegra.h"
#endif
#endif
#endif
#endif

#else  /* Single board */

#ifdef LINUXSIM_WANTED
#ifdef METROE_WANTED
#include "isslnxme.h"
#else
#include "isslnx.h"
#endif
#endif

#ifdef QCA_WANTED
#include "qca.h"
#endif

#ifdef WASP_WANTED
#include "wasp.h"
#endif

#ifdef LNXWIRELESS_WANTED
#include "lnxwireless.h"
#endif

#if defined (BCM5690_WANTED) || defined (BCM5695_WANTED) || defined (BCMX_WANTED) || defined (XPLIANT_WANTED)

#define  MBSM_MAX_SLOTS                                   1
#define  MBSM_SLOT_ALL                   MBSM_MAX_SLOTS + 1

#endif

#ifdef FULCRUM_WANTED
#include "fulcrum.h"
#endif

#ifdef XPLIANT_WANTED
#include "xpliant.h"
#endif

#ifdef ENET_ADAPTOR
#include "enetadaptor.h"
#endif

#ifdef EZCHIP_WANTED
#include "ezchip.h"
#endif

#ifdef ALTERA_WANTED
#include "altr4SGX.h"
#endif

#ifdef PETRA_WANTED
#include "petra.h"
#endif

#ifdef SWC
#include "cxe2130.h"
#endif

#ifdef DX260
#include "dx260.h"
#endif

#ifdef DX285
#include "dx285.h"
#endif


#ifdef DX167
#include "dx167.h"
#endif

#ifdef XCAT
#include "xcat.h"
#endif

#ifdef XCAT3
#include "xcat.h"
#endif

#ifdef WINTEGRA
#include "wintegra.h"
#endif

#ifdef LION
#include "lion.h"
#endif

#ifdef LION_DB
#include "lion.h"
#endif

#ifdef MRVLLS
#include "mrvlls.h"
#endif

#endif /* MBSM_WANTED */

#ifdef NPSIM
#include "npsiminc.h"
#endif /* NPSIM */
#else

#ifdef METROE_WANTED
#include "isslnxme.h"
#else
#include "isslnx.h"
#endif

#endif /* NPAPI_WANTED */
#ifdef BCMX_WANTED
#include "bcm.h"
#endif

#if defined (VTSS_SERVAL_1) || defined (VTSS_SERVAL_2)
#include "vtss.h"
#endif

/* The following macro defines the maximum number of ports 
 * supported for a line-card (Any type). */
#define  MBSM_MAX_ALLOWED_PORTS_IN_LC                     48

/* The params file contains some dependant constants and should not be changed
 * as far as possible
 */

#include "params.h"

#endif
