/*******************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: index.h,v 1.3 2010/10/22 18:51:44 prabuc Exp $
 *
 * Description: This file contains required external defn for Index manager.
 *********************************************************************/
#ifndef __INDEX_H_
#define __INDEX_H_

#define MAX_INDEX_MGR_LIST 100

#define INDEX_SUCCESS OSIX_SUCCESS
#define INDEX_FAILURE OSIX_FAILURE


/* Index Manager Type */
#define INDEX_MGR_STRICT_INCR_TYPE  1  /* Index will be allocated only in a 
                                          Incremental order */

#define INDEX_MGR_NON_INCR_TYPE     2  /* Index will be allocated based on
                                          the holes created in the list and 
                                          no incremental order is maintained */

#define INDEX_MGR_INCR_HOLES_TYPE   3  /* Initially it maintains incremental 
                                          allocation. Once the index pool is 
                                          full, then it starts to use the 
                                          holes present in the list */

/* Index Manager Pool ID */
typedef UINT4 tIndexMgrId;

/* Index Manager Init Function  */
UINT1 IndexManagerUtilInit PROTO ((VOID));

/* Function Prototypes */
UINT1 IndexManagerInitList PROTO ((UINT4, UINT4, UINT1 *, UINT1,
                                   tIndexMgrId *pIndexMgrId));
UINT1 IndexManagerDeInitList PROTO ((tIndexMgrId));
UINT1 IndexManagerGetNextFreeIndex PROTO ((tIndexMgrId, UINT4 *));
UINT1 IndexManagerSetNextFreeIndex PROTO ((tIndexMgrId, UINT4 *));
UINT1 IndexManagerCheckIndexIsFree PROTO ((tIndexMgrId, UINT4));
UINT1 IndexManagerSetIndex PROTO ((tIndexMgrId, UINT4));
UINT1 IndexManagerFreeAllIndex PROTO ((tIndexMgrId));
UINT1 IndexManagerFreeIndex PROTO ((tIndexMgrId, UINT4));

#endif /* __INDEX_H_ */
