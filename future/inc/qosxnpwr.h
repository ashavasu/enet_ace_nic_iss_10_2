
/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: qosxnpwr.h,v 1.9 2016/07/13 09:57:32 siva Exp $
 *
 * Description:This file contains the necessary data strucutures
 *             for packet handling in NPAPI layer
 *             <Part of dual node stacking model>
 *
 *******************************************************************/

#ifndef __QOSX_NP_WR_H__
#define __QOSX_NP_WR_H__

#include "qosxtd.h"
#include "qosxnp.h"
#ifdef MBSM_WANTED
#include "mbsm.h"
#endif

/* OPER ID */
enum
{
    QO_S_HW_INIT = 1,
    QO_S_HW_MAP_CLASS_TO_POLICY,
    QO_S_HW_UPDATE_POLICY_MAP_FOR_CLASS,
    QO_S_HW_UNMAP_CLASS_FROM_POLICY,
    QO_S_HW_DELETE_CLASS_MAP_ENTRY,
    QO_S_HW_METER_CREATE,
    QO_S_HW_METER_DELETE,
    QO_S_HW_SCHEDULER_ADD,
    QO_S_HW_SCHEDULER_UPDATE_PARAMS,
    QO_S_HW_SCHEDULER_DELETE,
    QO_S_HW_QUEUE_CREATE,
    QO_S_HW_QUEUE_DELETE,
    QO_S_HW_MAP_CLASS_TO_QUEUE,
    QO_S_HW_MAP_CLASS_TO_QUEUE_ID,
    QO_S_HW_SCHEDULER_HIERARCHY_MAP,
    QO_S_HW_SET_DEF_USER_PRIORITY,
    QO_S_HW_GET_METER_STATS,
    QO_S_HW_GET_CO_S_Q_STATS,
    FS_QOS_HW_CONFIG_PFC,
    QO_S_HW_GET_COUNT_ACT_STATS,
    QO_S_HW_GET_ALG_DROP_STATS,          
    QO_S_HW_GET_RANDOM_DROP_STATS,       
    QO_S_HW_SET_PBIT_PREFERENCE_OVER_DSCP,
    QO_S_HW_SET_CPU_RATE_LIMIT,
    QO_S_HW_MAP_CLASSTO_PRI_MAP,
    QOS_HW_MAP_CLASS_TO_INT_PRIORITY,
    QO_S_HW_GET_DSCP_Q_MAP,
#ifdef MBSM_WANTED
    QO_S_MBSM_HW_UPDATE_POLICY_MAP_FOR_CLASS,
    QO_S_MBSM_HW_QUEUE_CREATE,
    QO_S_MBSM_HW_MAP_CLASS_TO_QUEUE,
    QO_S_MBSM_HW_MAP_CLASS_TO_QUEUE_ID,
    FS_QOS_MBSM_HW_CONFIG_PFC,
    QO_S_MBSM_HW_SCHEDULER_ADD,
    QO_S_MBSM_HW_METER_CREATE,
    QO_S_MBSM_HW_INIT,
    QO_S_MBSM_HW_SET_CPU_RATE_LIMIT,
    QO_S_MBSM_HW_MAP_CLASS_TO_POLICY,
    QO_S_MBSM_HW_SCHEDULER_HIERARCHY_MAP,
    QO_S_MBSM_HW_SCHEDULER_UPDATE_PARAMS,
    QO_S_MBSM_HW_SET_PBIT_PREFERENCE_OVER_DSCP,
    QO_S_MBSM_HW_MAP_CLASSTO_PRI_MAP,
    QOS_MBSM_HW_MAP_CLASS_TO_INT_PRIORITY,
    QO_S_MBSM_HW_SET_DEF_USER_PRIORITY,
#endif /* MBSM_WANTED */
    QO_S_HW_SET_TRAF_CLASS_TO_PCP,
    QO_S_HW_METER_STAT_UPDATE,
    QO_S_HW_METER_STAT_CLEAR,
    QO_S_HW_SET_VLAN_QUEUING_STATUS,
    QOS_HW_EGRESS_TX_STATUS,
    QOS_HW_CONFIGURE_WRED_PROFILE,
    QOS_HW_DISABLE_WRED_PROFILE,
    QOS_HW_CLASSIFIER_DEL_ACTION_FP,
    QOS_HW_CLASSIFIER_ADD_ACTION_FP,
    QOS_SET_IN_PROFILE_ACTION_FP,
    QOS_SET_OUT_PROFILE_ACTION_FP,
    QOS_HW_CLASSIFIER_UPD_ACTION_FP,
    QOS_UPD_OUT_PROFILE_ACTION_FP,
    QOS_UPD_IN_PROFILE_ACTION_FP,
    QOS_SET_EXCEED_PROFILE_ACTION_FP,
    QOS_ADD_L2_L3_FILTER_ACTION_AND_INSTALL,
    QOS_HW_OUT_PRIORITY_REGENERATE,
    QOS_HW_UPDATE_CLASS_TO_INT_PRI,
    QOS_HW_UPDATE_OUT_PKT_PRI,
    QOS_HW_CHECK_PHB_CONFLICT,
    QOS_CHK_AND_VALIDATE_ACTION_FPS,
    QOS_HW_DEFAULT_DSCP_CONFIG,
    QOS_HW_STACK_SCHEDULER_SET,
    QOS_HW_CPU_SCHEDULER_SET,
    QOS_HW_DEFAULT_MPLS_EXP_CONFIG,
    FS_QOS_HW_GET_PFC_STATS,
    FS_QOS_HW_MAX_NP
};

/* Required arguments list for the qosx NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

typedef struct {
    tQoSClassMapEntry *          pClsMapEntry;
    tQoSPolicyMapEntry *         pPlyMapEntry;
    tQoSInProfileActionEntry *   pInProActEntry;
    tQoSOutProfileActionEntry *  pOutProActEntry;
    tQoSMeterEntry *             pMeterEntry;
    UINT1                        u1Flag;
 UINT1                        au1pad[3];
} tQosxNpWrQoSHwMapClassToPolicy;

typedef struct {
    tQoSClassMapEntry *          pClsMapEntry;
    tQoSPolicyMapEntry *         pPlyMapEntry;
    tQoSInProfileActionEntry *   pInProActEntry;
    tQoSOutProfileActionEntry *  pOutProActEntry;
    tQoSMeterEntry *             pMeterEntry;
    UINT1                        u1Flag;
 UINT1                        au1pad[3];
} tQosxNpWrQoSHwUpdatePolicyMapForClass;

typedef struct {
    tQoSClassMapEntry *   pClsMapEntry;
    tQoSPolicyMapEntry *  pPlyMapEntry;
    tQoSMeterEntry *      pMeterEntry;
    UINT1                 u1Flag;
 UINT1                 au1pad[3];
} tQosxNpWrQoSHwUnmapClassFromPolicy;

typedef struct {
    tQoSClassMapEntry *  pClsMapEntry;
} tQosxNpWrQoSHwDeleteClassMapEntry;

typedef struct {
    tQoSMeterEntry *  pMeterEntry;
} tQosxNpWrQoSHwMeterCreate;

typedef struct {
    INT4  i4MeterId;
} tQosxNpWrQoSHwMeterDelete;

typedef struct {
    tQoSMeterEntry *  pMeterEntry;
    UINT4          u4HwFilterId;
} tQosxNpWrQoSHwMeterStatsUpdate;

typedef struct {
    INT4  i4MeterId;
} tQosxNpWrQoSHwMeterClear;

typedef struct {
    tQoSSchedulerEntry *  pSchedEntry;
} tQosxNpWrQoSHwSchedulerAdd;

typedef struct {
    tQoSSchedulerEntry *  pSchedEntry;
} tQosxNpWrQoSHwSchedulerUpdateParams;

typedef struct {
    INT4   i4IfIndex;
    UINT4  u4SchedId;
} tQosxNpWrQoSHwSchedulerDelete;

typedef struct {
    INT4               i4IfIndex;
    UINT4              u4QId;
    tQoSQEntry *       pQEntry;
    tQoSQtypeEntry *   pQTypeEntry;
    tQoSREDCfgEntry * *  papRDCfgEntry;
    INT2               i2HL;
 UINT1              au1pad[2];
} tQosxNpWrQoSHwQueueCreate;

typedef struct {
    INT4   i4IfIndex;
    UINT4  u4Id;
} tQosxNpWrQoSHwQueueDelete;

typedef struct {
    INT4   i4IfIndex;
    INT4   i4ClsOrPriType;
    UINT4  u4ClsOrPri;
    UINT4  u4QId;
    UINT1  u1Flag;
 UINT1  au1pad[3];
} tQosxNpWrQoSHwMapClassToQueue;

typedef struct {
    INT4                 i4IfIndex;
    tQoSClassMapEntry *  pClsMapEntry;
    INT4                 i4ClsOrPriType;
    UINT4                u4ClsOrPri;
    UINT4                u4QId;
    UINT1                u1Flag;
 UINT1                au1pad[3];
} tQosxNpWrQoSHwMapClassToQueueId;

typedef struct {
    INT4   i4IfIndex;
    UINT4  u4SchedId;
    UINT4  u4NextSchedId;
    UINT4  u4NextQId;
    UINT2  u2Sweight;
    INT2   i2HL;
    UINT1  u1Spriority;
    UINT1  u1Flag;
 UINT1  au1pad[2];
} tQosxNpWrQoSHwSchedulerHierarchyMap;

typedef struct {
    INT4  i4Port;
    INT4  i4DefPriority;
} tQosxNpWrQoSHwSetDefUserPriority;

typedef struct {
    UINT4                   u4MeterId;
    UINT4                   u4StatsType;
    tSNMP_COUNTER64_TYPE *  pu8MeterStatsCounter;
} tQosxNpWrQoSHwGetMeterStats;

typedef struct {
    INT4                    i4IfIndex;
    UINT4                   u4QId;
    UINT4                   u4StatsType;
    tSNMP_COUNTER64_TYPE *  pu8CoSQStatsCounter;
} tQosxNpWrQoSHwGetCoSQStats;

typedef struct {
    tQosPfcHwEntry *  pQosPfcHwEntry;
} tQosxNpWrFsQosHwConfigPfc;

typedef struct {
    UINT4                   u4DiffServId;
    UINT4                   u4StatsType;
    tSNMP_COUNTER64_TYPE *  pu8RetValDiffServCounter;
} tQosxNpWrQoSHwGetCountActStats;

typedef struct {
    UINT4                   u4DiffServId;
    UINT4                   u4StatsType;
    tSNMP_COUNTER64_TYPE *  pu8RetValDiffServCounter;
} tQosxNpWrQoSHwGetAlgDropStats;

typedef struct {
    UINT4                   u4DiffServId;
    UINT4                   u4StatsType;
    tSNMP_COUNTER64_TYPE *  pu8RetValDiffServCounter;
} tQosxNpWrQoSHwGetRandomDropStats;

typedef struct {
    INT4  i4Port;
    INT4  i4PbitPref;
} tQosxNpWrQoSHwSetPbitPreferenceOverDscp;

typedef struct {
    INT4   i4CpuQueueId;
    UINT4  u4MinRate;
    UINT4  u4MaxRate;
} tQosxNpWrQoSHwSetCpuRateLimit;

typedef struct {
    tQosClassToPriMapEntry *  pQosClassToPriMapEntry;
} tQosxNpWrQoSHwMapClasstoPriMap;

typedef struct {
    tQosClassToIntPriEntry *  pQosClassToIntPriEntry;
} tQosxNpWrQosHwMapClassToIntPriority;

typedef struct {
            INT4 i4QosDscpVal;
                    INT4 *i4QosQueID;
} tQosxNpWrQosHwGetDscpQueueMap;

typedef struct {
            INT4 i4IfIndex;
            INT4 i4UserPriority;
            INT4 i4TrafficClass;
} tQosxNpWrQosHwSetTrafficClassToPcp;

typedef struct {
    INT4          i4IfIndex;
    tQoSQEntry *  pQEntrySubscriber;
} tQosxNpWrQoSHwSetVlanQueuingStatus;

typedef struct {
    tQosPfcHwStats *pQosPfcHwStats;
} tQosxNpWrFsQosHwGetPfcStats;

#ifdef MBSM_WANTED
typedef struct {
    tQoSClassMapEntry *          pClsMapEntry;
    tQoSPolicyMapEntry *         pPlyMapEntry;
    tQoSInProfileActionEntry *   pInProActEntry;
    tQoSOutProfileActionEntry *  pOutProActEntry;
    tQoSMeterEntry *             pMeterEntry;
    tMbsmSlotInfo *              pSlotInfo;
} tQosxNpWrQoSMbsmHwUpdatePolicyMapForClass;

typedef struct {
    INT4               i4IfIndex;
    UINT4              u4QId;
    tQoSQEntry *       pQEntry;
    tQoSQtypeEntry *   pQTypeEntry;
    tQoSREDCfgEntry * *  papRDCfgEntry;
    tMbsmSlotInfo *    pSlotInfo;
    INT2               i2HL;
 UINT1              au1pad[2];
} tQosxNpWrQoSMbsmHwQueueCreate;

typedef struct {
    INT4             i4IfIndex;
    INT4             i4ClsOrPriType;
    UINT4            u4ClsOrPri;
    UINT4            u4QId;
    tMbsmSlotInfo *  pSlotInfo;
    UINT1            u1Flag;
 UINT1            au1pad[3];
} tQosxNpWrQoSMbsmHwMapClassToQueue;

typedef struct {
    INT4                 i4IfIndex;
    tQoSClassMapEntry *  pClsMapEntry;
    INT4                 i4ClsOrPriType;
    UINT4                u4ClsOrPri;
    UINT4                u4QId;
    tMbsmSlotInfo *      pSlotInfo;
    UINT1                u1Flag;
 UINT1                au1pad[3];
} tQosxNpWrQoSMbsmHwMapClassToQueueId;

typedef struct {
    tQosPfcHwEntry *  pQosPfcHwEntry;
    tMbsmSlotInfo *   pSlotInfo;
} tQosxNpWrFsQosMbsmHwConfigPfc;

typedef struct {
    tQoSSchedulerEntry *  pSchedEntry;
    tMbsmSlotInfo *       pSlotInfo;
} tQosxNpWrQoSMbsmHwSchedulerAdd;


typedef struct {
    tQoSMeterEntry *  pMeterEntry;
    tMbsmSlotInfo *   pSlotInfo;
} tQosxNpWrQoSMbsmHwMeterCreate;

typedef struct {
    tMbsmSlotInfo *  pSlotInfo;
} tQosxNpWrQoSMbsmHwInit;

typedef struct {
    INT4             i4CpuQueueId;
    UINT4            u4MinRate;
    UINT4            u4MaxRate;
    tMbsmSlotInfo *  pSlotInfo;
} tQosxNpWrQoSMbsmHwSetCpuRateLimit;

typedef struct {
    tQoSClassMapEntry *          pClsMapEntry;
    tQoSPolicyMapEntry *         pPlyMapEntry;
    tQoSInProfileActionEntry *   pInProActEntry;
    tQoSOutProfileActionEntry *  pOutProActEntry;
    tQoSMeterEntry *             pMeterEntry;
    UINT1                        u1Flag;
    tMbsmSlotInfo *              pSlotInfo;
} tQosxNpWrQoSMbsmHwMapClassToPolicy;

typedef struct {
    INT4             i4IfIndex;
    UINT4            u4SchedId;
    UINT2            u2Sweight;
    UINT1            u1Spriority;
    UINT4            u4NextSchedId;
    UINT4            u4NextQId;
    INT2             i2HL;
    UINT1            u1Flag;
    tMbsmSlotInfo *  pSlotInfo;
} tQosxNpWrQoSMbsmHwSchedulerHierarchyMap;

typedef struct {
    tQoSSchedulerEntry *  pSchedEntry;
    tMbsmSlotInfo *       pSlotInfo;
} tQosxNpWrQoSMbsmHwSchedulerUpdateParams;

typedef struct {
    INT4             i4Port;
    INT4             i4DefPriority;
    tMbsmSlotInfo *  pSlotInfo;
} tQosxNpWrQoSMbsmHwSetDefUserPriority;

typedef struct {
    INT4             i4Port;
    INT4             i4PbitPref;
    tMbsmSlotInfo *  pSlotInfo;
} tQosxNpWrQoSMbsmHwSetPbitPreferenceOverDscp;

typedef struct {
    tQosClassToPriMapEntry *  pQosClassToPriMapEntry;
    tMbsmSlotInfo *           pSlotInfo;
} tQosxNpWrQoSMbsmHwMapClasstoPriMap;

typedef struct {
    tQosClassToIntPriEntry *  pQosClassToIntPriEntry;
    tMbsmSlotInfo *           pSlotInfo;
} tQosxNpWrQosMbsmHwMapClassToIntPriority;



#endif /* MBSM_WANTED */
typedef struct QosxNpModInfo {
union {
    tQosxNpWrQoSHwMapClassToPolicy  sQoSHwMapClassToPolicy;
    tQosxNpWrQoSHwUpdatePolicyMapForClass  sQoSHwUpdatePolicyMapForClass;
    tQosxNpWrQoSHwUnmapClassFromPolicy  sQoSHwUnmapClassFromPolicy;
    tQosxNpWrQoSHwDeleteClassMapEntry  sQoSHwDeleteClassMapEntry;
    tQosxNpWrQoSHwMeterCreate  sQoSHwMeterCreate;
    tQosxNpWrQoSHwMeterDelete  sQoSHwMeterDelete;
    tQosxNpWrQoSHwSchedulerAdd  sQoSHwSchedulerAdd;
    tQosxNpWrQoSHwSchedulerUpdateParams  sQoSHwSchedulerUpdateParams;
    tQosxNpWrQoSHwSchedulerDelete  sQoSHwSchedulerDelete;
    tQosxNpWrQoSHwQueueCreate  sQoSHwQueueCreate;
    tQosxNpWrQoSHwQueueDelete  sQoSHwQueueDelete;
    tQosxNpWrQoSHwMapClassToQueue  sQoSHwMapClassToQueue;
    tQosxNpWrQoSHwMapClassToQueueId  sQoSHwMapClassToQueueId;
    tQosxNpWrQoSHwSchedulerHierarchyMap  sQoSHwSchedulerHierarchyMap;
    tQosxNpWrQoSHwSetDefUserPriority  sQoSHwSetDefUserPriority;
    tQosxNpWrQoSHwGetMeterStats  sQoSHwGetMeterStats;
    tQosxNpWrQoSHwGetCoSQStats  sQoSHwGetCoSQStats;
    tQosxNpWrFsQosHwConfigPfc  sFsQosHwConfigPfc;
    tQosxNpWrQoSHwGetCountActStats  sQoSHwGetCountActStats;
    tQosxNpWrQoSHwGetAlgDropStats  sQoSHwGetAlgDropStats;
    tQosxNpWrQoSHwGetRandomDropStats  sQoSHwGetRandomDropStats;
    tQosxNpWrQoSHwSetPbitPreferenceOverDscp  sQoSHwSetPbitPreferenceOverDscp;
    tQosxNpWrQoSHwSetCpuRateLimit  sQoSHwSetCpuRateLimit;
    tQosxNpWrQoSHwMapClasstoPriMap  sQoSHwMapClasstoPriMap;
    tQosxNpWrQosHwMapClassToIntPriority  sQosHwMapClassToIntPriority;
    tQosxNpWrQosHwGetDscpQueueMap sQosHwGetDscpQueueMap;
    tQosxNpWrQosHwSetTrafficClassToPcp sQosHwSetTrafficClassToPcp;
    tQosxNpWrQoSHwSetVlanQueuingStatus  sQoSHwSetVlanQueuingStatus;
    tQosxNpWrFsQosHwGetPfcStats  sFsQosHwGetPfcStats;
#ifdef MBSM_WANTED
    tQosxNpWrQoSMbsmHwUpdatePolicyMapForClass  sQoSMbsmHwUpdatePolicyMapForClass;
    tQosxNpWrQoSMbsmHwQueueCreate  sQoSMbsmHwQueueCreate;
    tQosxNpWrQoSMbsmHwMapClassToQueue  sQoSMbsmHwMapClassToQueue;
    tQosxNpWrQoSMbsmHwMapClassToQueueId  sQoSMbsmHwMapClassToQueueId;
    tQosxNpWrFsQosMbsmHwConfigPfc  sFsQosMbsmHwConfigPfc;
    tQosxNpWrQoSMbsmHwSchedulerAdd  sQoSMbsmHwSchedulerAdd;
    tQosxNpWrQoSMbsmHwMeterCreate  sQoSMbsmHwMeterCreate;
    tQosxNpWrQoSMbsmHwInit  sQoSMbsmHwInit;
    tQosxNpWrQoSMbsmHwSetCpuRateLimit  sQoSMbsmHwSetCpuRateLimit;
    tQosxNpWrQoSMbsmHwMapClassToPolicy  sQoSMbsmHwMapClassToPolicy;
    tQosxNpWrQoSMbsmHwSchedulerHierarchyMap  sQoSMbsmHwSchedulerHierarchyMap;
    tQosxNpWrQoSMbsmHwSchedulerUpdateParams  sQoSMbsmHwSchedulerUpdateParams;
    tQosxNpWrQoSMbsmHwSetDefUserPriority  sQoSMbsmHwSetDefUserPriority;
    tQosxNpWrQoSMbsmHwSetPbitPreferenceOverDscp  sQoSMbsmHwSetPbitPreferenceOverDscp;
    tQosxNpWrQoSMbsmHwMapClasstoPriMap  sQoSMbsmHwMapClasstoPriMap;
    tQosxNpWrQosMbsmHwMapClassToIntPriority  sQosMbsmHwMapClassToIntPriority;
#endif /* MBSM_WANTED */
    tQosxNpWrQoSHwMeterStatsUpdate  sQoSHwMeterStatsUpdate;
    tQosxNpWrQoSHwMeterClear  sQoSHwMeterStatsClear;
    }unOpCode;

#define  QosxNpQoSHwMapClassToPolicy  unOpCode.sQoSHwMapClassToPolicy;
#define  QosxNpQoSHwUpdatePolicyMapForClass  unOpCode.sQoSHwUpdatePolicyMapForClass;
#define  QosxNpQoSHwUnmapClassFromPolicy  unOpCode.sQoSHwUnmapClassFromPolicy;
#define  QosxNpQoSHwDeleteClassMapEntry  unOpCode.sQoSHwDeleteClassMapEntry;
#define  QosxNpQoSHwMeterCreate  unOpCode.sQoSHwMeterCreate;
#define  QosxNpQoSHwMeterDelete  unOpCode.sQoSHwMeterDelete;
#define  QosxNpQoSHwSchedulerAdd  unOpCode.sQoSHwSchedulerAdd;
#define  QosxNpQoSHwSchedulerUpdateParams  unOpCode.sQoSHwSchedulerUpdateParams;
#define  QosxNpQoSHwSchedulerDelete  unOpCode.sQoSHwSchedulerDelete;
#define  QosxNpQoSHwQueueCreate  unOpCode.sQoSHwQueueCreate;
#define  QosxNpQoSHwQueueDelete  unOpCode.sQoSHwQueueDelete;
#define  QosxNpQoSHwMapClassToQueue  unOpCode.sQoSHwMapClassToQueue;
#define  QosxNpQoSHwMapClassToQueueId  unOpCode.sQoSHwMapClassToQueueId;
#define  QosxNpQoSHwSchedulerHierarchyMap  unOpCode.sQoSHwSchedulerHierarchyMap;
#define  QosxNpQoSHwSetDefUserPriority  unOpCode.sQoSHwSetDefUserPriority;
#define  QosxNpQoSHwGetMeterStats  unOpCode.sQoSHwGetMeterStats;
#define  QosxNpQoSHwGetCoSQStats  unOpCode.sQoSHwGetCoSQStats;
#define  QosxNpFsQosHwConfigPfc  unOpCode.sFsQosHwConfigPfc;
#define  QosxNpQoSHwGetCountActStats  unOpCode.sQoSHwGetCountActStats;
#define  QosxNpQoSHwGetAlgDropStats  unOpCode.sQoSHwGetAlgDropStats;
#define  QosxNpQoSHwGetRandomDropStats  unOpCode.sQoSHwGetRandomDropStats;
#define  QosxNpQoSHwSetPbitPreferenceOverDscp  unOpCode.sQoSHwSetPbitPreferenceOverDscp;
#define  QosxNpQoSHwSetCpuRateLimit  unOpCode.sQoSHwSetCpuRateLimit;
#define  QosxNpQoSHwMapClasstoPriMap  unOpCode.sQoSHwMapClasstoPriMap;
#define  QosxNpQosHwMapClassToIntPriority  unOpCode.sQosHwMapClassToIntPriority;
#define  QosxNpQosHwGetDscpQueueMap unOpCode.sQosHwGetDscpQueueMap;
#define  QosxNpQosHwSetTrafficClassToPcp unOpCode.sQosHwSetTrafficClassToPcp;
#define  QosxNpQoSHwSetVlanQueuingStatus  unOpCode.sQoSHwSetVlanQueuingStatus;
#define  QosxNpFsQosHwGetPfcStats  unOpCode.sFsQosHwGetPfcStats;
#ifdef MBSM_WANTED
#define  QosxNpQoSMbsmHwUpdatePolicyMapForClass  unOpCode.sQoSMbsmHwUpdatePolicyMapForClass;
#define  QosxNpQoSMbsmHwQueueCreate  unOpCode.sQoSMbsmHwQueueCreate;
#define  QosxNpQoSMbsmHwMapClassToQueue  unOpCode.sQoSMbsmHwMapClassToQueue;
#define  QosxNpQoSMbsmHwMapClassToQueueId  unOpCode.sQoSMbsmHwMapClassToQueueId;
#define  QosxNpFsQosMbsmHwConfigPfc  unOpCode.sFsQosMbsmHwConfigPfc;
#define  QosxNpQoSMbsmHwSchedulerAdd  unOpCode.sQoSMbsmHwSchedulerAdd;
#define  QosxNpQoSMbsmHwMeterCreate  unOpCode.sQoSMbsmHwMeterCreate;
#define  QosxNpQoSMbsmHwInit  unOpCode.sQoSMbsmHwInit;
#define  QosxNpQoSMbsmHwSetCpuRateLimit  unOpCode.sQoSMbsmHwSetCpuRateLimit;
#define  QosxNpQoSMbsmHwMapClassToPolicy  unOpCode.sQoSMbsmHwMapClassToPolicy;
#define  QosxNpQoSMbsmHwSchedulerHierarchyMap  unOpCode.sQoSMbsmHwSchedulerHierarchyMap;
#define  QosxNpQoSMbsmHwSchedulerUpdateParams  unOpCode.sQoSMbsmHwSchedulerUpdateParams;
#define  QosxNpQoSMbsmHwSetDefUserPriority  unOpCode.sQoSMbsmHwSetDefUserPriority;
#define  QosxNpQoSMbsmHwSetPbitPreferenceOverDscp  unOpCode.sQoSMbsmHwSetPbitPreferenceOverDscp;
#define  QosxNpQoSMbsmHwMapClasstoPriMap  unOpCode.sQoSMbsmHwMapClasstoPriMap;
#define  QosxNpQosMbsmHwMapClassToIntPriority  unOpCode.sQosMbsmHwMapClassToIntPriority;
#define  QosxNpQoSMbsmHwSchedulerUpdateParams  unOpCode.sQoSMbsmHwSchedulerUpdateParams;
#endif /* MBSM_WANTED */
#define  QosxNpQoSHwMeterStatsUpdate  unOpCode.sQoSHwMeterStatsUpdate;
#define  QosxNpQoSHwMeterStatsClear  unOpCode.sQoSHwMeterStatsClear;
} tQosxNpModInfo;


/* Function Prototype for Generic NP OpCode specific wrappers
 * defined in Qosxnpapi.c */

UINT1 QosxQoSHwInit PROTO ((VOID));
UINT1 QosxQoSHwMapClassToPolicy PROTO ((tQoSClassMapEntry * pClsMapEntry, tQoSPolicyMapEntry * pPlyMapEntry, tQoSInProfileActionEntry * pInProActEntry, tQoSOutProfileActionEntry * pOutProActEntry, tQoSMeterEntry * pMeterEntry, UINT1 u1Flag));
UINT1 QosxQoSHwUpdatePolicyMapForClass PROTO ((tQoSClassMapEntry * pClsMapEntry, tQoSPolicyMapEntry * pPlyMapEntry, tQoSInProfileActionEntry * pInProActEntry, tQoSOutProfileActionEntry * pOutProActEntry, tQoSMeterEntry * pMeterEntry, UINT1 u1Flag));
UINT1 QosxQoSHwUnmapClassFromPolicy PROTO ((tQoSClassMapEntry * pClsMapEntry, tQoSPolicyMapEntry * pPlyMapEntry, tQoSMeterEntry * pMeterEntry, UINT1 u1Flag));
UINT1 QosxQoSHwDeleteClassMapEntry PROTO ((tQoSClassMapEntry * pClsMapEntry));
UINT1 QosxQoSHwMeterCreate PROTO ((tQoSMeterEntry * pMeterEntry));
UINT1 QosxQoSHwMeterDelete PROTO ((INT4 i4MeterId));
UINT1 QosxQoSHwMeterStatsUpdate PROTO ((UINT4 ,tQoSMeterEntry *));
UINT1 QosxQoSHwMeterStatsClear PROTO ((INT4));
UINT1 QosxQoSHwSchedulerAdd PROTO ((tQoSSchedulerEntry * pSchedEntry));
UINT1 QosxQoSHwSchedulerUpdateParams PROTO ((tQoSSchedulerEntry * pSchedEntry));
UINT1 QosxQoSHwSchedulerDelete PROTO ((INT4 i4IfIndex, UINT4 u4SchedId));
UINT1 QosxQoSHwQueueCreate PROTO ((INT4 i4IfIndex, UINT4 u4QId, tQoSQEntry * pQEntry, tQoSQtypeEntry * pQTypeEntry, tQoSREDCfgEntry * * papRDCfgEntry, INT2 i2HL));
UINT1 QosxQoSHwQueueDelete PROTO ((INT4 i4IfIndex, UINT4 u4Id));
UINT1 QosxQoSHwMapClassToQueue PROTO ((INT4 i4IfIndex, INT4 i4ClsOrPriType, UINT4 u4ClsOrPri, UINT4 u4QId, UINT1 u1Flag));
UINT1 QosxQoSHwMapClassToQueueId PROTO ((tQoSClassMapEntry * pClsMapEntry, INT4 i4IfIndex, INT4 i4ClsOrPriType, UINT4 u4ClsOrPri, UINT4 u4QId, UINT1 u1Flag));
UINT1 QosxQoSHwSchedulerHierarchyMap PROTO ((INT4 i4IfIndex, UINT4 u4SchedId, UINT2 u2Sweight, UINT1 u1Spriority, UINT4 u4NextSchedId, UINT4 u4NextQId, INT2 i2HL, UINT1 u1Flag));
UINT1 QosxQoSHwSetDefUserPriority PROTO ((INT4 i4Port, INT4 i4DefPriority));
UINT1 QosxQoSHwGetMeterStats PROTO ((UINT4 u4MeterId, UINT4 u4StatsType, tSNMP_COUNTER64_TYPE * pu8MeterStatsCounter));
UINT1 QosxQoSHwGetCoSQStats PROTO ((INT4 i4IfIndex, UINT4 u4QId, UINT4 u4StatsType, tSNMP_COUNTER64_TYPE * pu8CoSQStatsCounter));
UINT1 QosxFsQosHwConfigPfc PROTO ((tQosPfcHwEntry * pQosPfcHwEntry));
UINT1 QosxQoSHwGetCountActStats PROTO ((UINT4 u4DiffServId, UINT4 u4StatsType, tSNMP_COUNTER64_TYPE * pu8RetValDiffServCounter));
UINT1 QosxQoSHwGetAlgDropStats PROTO ((UINT4 u4DiffServId, UINT4 u4StatsType, tSNMP_COUNTER64_TYPE * pu8RetValDiffServCounter));
UINT1 QosxQoSHwGetRandomDropStats PROTO ((UINT4 u4DiffServId, UINT4 u4StatsType, tSNMP_COUNTER64_TYPE * pu8RetValDiffServCounter));
UINT1 QosxQoSHwSetPbitPreferenceOverDscp PROTO ((INT4 i4Port, INT4 i4PbitPref));
UINT1 QosxQoSHwSetCpuRateLimit PROTO ((INT4 i4CpuQueueId, UINT4 u4MinRate, UINT4 u4MaxRate));
UINT1 QosxQoSHwMapClasstoPriMap PROTO ((tQosClassToPriMapEntry * pQosClassToPriMapEntry));
UINT1 QosxQosHwMapClassToIntPriority PROTO ((tQosClassToIntPriEntry * pQosClassToIntPriEntry));
UINT1 QosxQoSHwGetDscpQueueMap PROTO((INT4 i4DscpVal,INT4 *i4QueueID));
UINT1 QosxQoSHwSetTrafficClassToPcp PROTO ((INT4 i4IfIndex, INT4 i4UserPriority, INT4 i4TrafficClass));
UINT1 QosxQoSHwSetVlanQueuingStatus PROTO ((INT4 i4IfIndex, tQoSQEntry * pQEntrySubscriber));
UINT1 QosxFsQosHwGetPfcStats PROTO ((tQosPfcHwStats * pQosPfcHwStats));
#ifdef MBSM_WANTED
UINT1 QosxQoSMbsmHwUpdatePolicyMapForClass PROTO ((tQoSClassMapEntry * pClsMapEntry, tQoSPolicyMapEntry * pPlyMapEntry, tQoSInProfileActionEntry * pInProActEntry, tQoSOutProfileActionEntry * pOutProActEntry, tQoSMeterEntry * pMeterEntry, tMbsmSlotInfo * pSlotInfo));
UINT1 QosxQoSMbsmHwQueueCreate PROTO ((INT4 i4IfIndex, UINT4 u4QId, tQoSQEntry * pQEntry, tQoSQtypeEntry * pQTypeEntry, tQoSREDCfgEntry * * papRDCfgEntry, INT2 i2HL, tMbsmSlotInfo * pSlotInfo));
UINT1 QosxQoSMbsmHwMapClassToQueue PROTO ((INT4 i4IfIndex, INT4 i4ClsOrPriType, UINT4 u4ClsOrPri, UINT4 u4QId, UINT1 u1Flag, tMbsmSlotInfo * pSlotInfo));
UINT1 QosxQoSMbsmHwMapClassToQueueId PROTO ((tQoSClassMapEntry * pClsMapEntry, INT4 i4IfIndex, INT4 i4ClsOrPriType, UINT4 u4ClsOrPri, UINT4 u4QId, UINT1 u1Flag, tMbsmSlotInfo * pSlotInfo));
UINT1 QosxFsQosMbsmHwConfigPfc PROTO ((tQosPfcHwEntry * pQosPfcHwEntry, tMbsmSlotInfo * pSlotInfo));
UINT1 QosxQoSMbsmHwMeterCreate PROTO ((tQoSMeterEntry * pMeterEntry, tMbsmSlotInfo * pSlotInfo));

UINT1 QosxQoSMbsmHwSchedulerAdd (tQoSSchedulerEntry * pSchedEntry,
                                 tMbsmSlotInfo * pSlotInfo);
UINT1 QosxQoSMbsmHwInit PROTO ((tMbsmSlotInfo * pSlotInfo));
UINT1 QosxQoSMbsmHwSetCpuRateLimit PROTO ((INT4 i4CpuQueueId, UINT4 u4MinRate, UINT4 u4MaxRate, tMbsmSlotInfo * pSlotInfo));
UINT1 QosxQoSMbsmHwMapClassToPolicy PROTO ((tQoSClassMapEntry * pClsMapEntry, tQoSPolicyMapEntry * pPlyMapEntry, tQoSInProfileActionEntry * pInProActEntry, tQoSOutProfileActionEntry * pOutProActEntry, tQoSMeterEntry * pMeterEntry, UINT1 u1Flag, tMbsmSlotInfo * pSlotInfo));
UINT1 QosxQoSMbsmHwSchedulerHierarchyMap PROTO ((INT4 i4IfIndex, UINT4 u4SchedId, UINT2 u2Sweight, UINT1 u1Spriority, UINT4 u4NextSchedId, UINT4 u4NextQId, INT2 i2HL, UINT1 u1Flag, tMbsmSlotInfo * pSlotInfo));
UINT1 QosxQoSMbsmHwSchedulerUpdateParams PROTO ((tQoSSchedulerEntry * pSchedEntry, tMbsmSlotInfo * pSlotInfo));
UINT1 QosxQoSMbsmHwSetDefUserPriority PROTO ((INT4 i4Port, INT4 i4DefPriority, tMbsmSlotInfo * pSlotInfo));
UINT1 QosxQoSMbsmHwSetPbitPreferenceOverDscp PROTO ((INT4 i4Port, INT4 i4PbitPref, tMbsmSlotInfo * pSlotInfo));
UINT1 QosxQoSMbsmHwMapClasstoPriMap PROTO ((tQosClassToPriMapEntry * pQosClassToPriMapEntry, tMbsmSlotInfo * pSlotInfo));
UINT1 QosxQosMbsmHwMapClassToIntPriority PROTO ((tQosClassToIntPriEntry * pQosClassToIntPriEntry, tMbsmSlotInfo * pSlotInfo));
UINT1 QosxQoSMbsmHwSchedulerUpdateParams PROTO ((tQoSSchedulerEntry * pSchedEntry, tMbsmSlotInfo * pSlotInfo));
#endif /* MBSM_WANTED */

#endif /* __QOSX_NP_WR_H__ */
