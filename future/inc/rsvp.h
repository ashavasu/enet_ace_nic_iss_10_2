/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rsvp.h,v 1.20 2011/10/28 10:36:48 siva Exp $
 *
 * Description:This file contains constants , typedefs & macros
 *             related to TC.
 *
 *******************************************************************/
#ifndef _RSVP_H
#define _RSVP_H

#include "ospfte.h"
 /*
 * !! NOTE : Don't change any of the structures. Becuase based on the Relative
 * position of different fields in the structures.
 */

/* 
 * These structure will be used for RSVP- Service Dependent Function
 * Interface. 
 */
typedef FLT4 FLOAT;

typedef struct {
    UINT1  u1ParamNum;
    UINT1  u1ParamFlags;
    UINT2  u2ParamLen;
}tParamHdr;

typedef struct MsgHdrType{
    UINT1  u1Version;   /* Higher 4 bits represent the version number */
    UINT1  u1Reserved;
    UINT2  u2HdrLen;
}tMsgHdr;

typedef struct SrvHdrType{
    UINT1  u1SrvID;
    UINT1  u1Flag;
    UINT2  u2SrvLength;
}tSrvHdr;

typedef struct TBParamsType{
    FLOAT  fBktRate;
    FLOAT  fBktSize;
    FLOAT  fPeakRate;
    UINT4  u4MinSize;
    UINT4  u4MaxSize;
}tTBParams;

   /* Parameters for Token Bucket Specification */
typedef struct TSpecType{
    tMsgHdr    MsgHdr;
    tSrvHdr    SrvHdr;
    tParamHdr  ParamHdr;
    tTBParams  TBParams;
}tTSpec;

#define   tSenderTspec         tTSpec                    
typedef struct GLRSpecType{
    tSrvHdr  SrvHdr;
    FLOAT    fRSpecRate;
    UINT4    u4Slack;     
}tGsRSpec;

typedef struct GLFlowSpecType{
    tTBParams  TBParams;
    tGsRSpec   GsRSpec;
}tGsFlowSpec;

typedef struct CLFlowSpecType{
    tTBParams  TBParams;
}tClsFlowSpec;

typedef struct FlowSpecType{
    tMsgHdr    MsgHdr;
    tSrvHdr    SrvHdr;
    tParamHdr  ParamHdr;
   union ServiceParamsType{
       tClsFlowSpec  ClsService;
       tGsFlowSpec   GsService;  
   }ServiceParams;
}tFlowSpec;


typedef struct CToTType{
    tParamHdr  ParamHdr;
    UINT4      u4CTot;
   /* End to End composed value of C. C is rate specific error term */
}tCTot;

typedef struct DToTType{
    tParamHdr  ParamHdr;
    UINT4      u4DTot;
   /* End to End composed value of D. D is rate independent error term */
}tDTot;

typedef struct CSumType{
    tParamHdr  ParamHdr;
    UINT4      u4CSum;
   /* Value of C since the last reshaping point */
}tCSum;

typedef struct DSumType{
    tParamHdr  ParamHdr;
    UINT4      u4DSum;
   /* Value of D since the last reshaping point */
}tDSum;

typedef struct HCParamType{
    tParamHdr  ParamHdr;
    UINT4      u4ISHopCount;
   /* Indicates the number of Integrated Services Aware Hops */
}tHCParam;

typedef struct PathBwType{
    tParamHdr  ParamHdr;
    FLOAT      fPathBw;
}tPathBwParam;

typedef struct PathLatType{
    tParamHdr  ParamHdr;
    UINT4      u4MinPathLatency;
}tPathLatParam;

typedef struct MTUParamType{
    tParamHdr  ParamHdr;
    UINT4      u4MTUSize;
   /* MTU - Maximum Transmission Unit */
}tMTUParam;

typedef struct DfltParamsType{
    tSrvHdr        SrvHdr;
    tHCParam       HopCount;
    tPathBwParam   PathBw;
    tPathLatParam  MinPathLatency;
    tMTUParam      MaxTransUnit;
}tDfltParams;

typedef struct GLAdSpecType{
    tSrvHdr  SrvHdr;
    tCTot    CTot;
    tDTot    DTot;
    tCSum    CSum;
    tDSum    DSum;
   /* Service specific values which over-ride default values can follow */
}tGsAdSpec;

typedef struct CLAdSpecType{
    tSrvHdr  SrvHdr;
   /* Service specific values which over-ride default values can follow */
}tClsAdSpec;

typedef struct AdSpecType{
    tMsgHdr      MsgHdr;
    tDfltParams  DfltParams;
    tGsAdSpec    GsAdSpec;    
    tClsAdSpec   ClsAdSpec;   
}tAdSpec;

typedef struct AdSpecWithoutGsAndCls{
    tMsgHdr      MsgHdr;
    tDfltParams  DfltParams;
}tAdSpecWithoutGsAndCls;

/* Default General Parameters */
   /* Guaranteed and Controlled load service fragments follow. Since they
    * may or maynot be present, the presence can be confirmed only after
    * parsing the received frame.
    */
typedef struct FilterSpec {
    UINT4  u4Addr;
    UINT2  u2Reserved;
    UINT2  u2Port;
} tFilterSpec;
VOID
RpteEnqueueMsgToRsvpQFromCspf (tCspfCompInfo *pCspfCompInfo, VOID *aPath);
#endif /* _RSVP_H */
