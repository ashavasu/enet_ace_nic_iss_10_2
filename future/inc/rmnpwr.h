
/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: rmnpwr.h,v 1.1 2013/03/19 12:21:51 siva Exp $
 *
 * Description:This file contains the necessary data strucutures
 *             for packet handling in NPAPI layer
 *             <Part of dual node stacking model>
 *
 *******************************************************************/
#ifndef __RM_NP_WR_H__
#define __RM_NP_WR_H__

#include "rmnp.h"

/* OPER ID */
#define  RM_NP_UPDATE_NODE_STATE                                1
#define  FS_NP_H_R_SET_STDY_ST_INFO                             2

/* Required arguments list for the rm NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

typedef struct {
    UINT1  u1Event;
    UINT1  au1pad[3];
} tRmNpWrRmNpUpdateNodeState;

typedef struct {
    tRmHRAction     eAction;
    tRmHRPktInfo *  pRmHRPktInfo;
} tRmNpWrFsNpHRSetStdyStInfo;

typedef struct RmNpModInfo {
union {
    tRmNpWrRmNpUpdateNodeState  sRmNpUpdateNodeState;
    tRmNpWrFsNpHRSetStdyStInfo  sFsNpHRSetStdyStInfo;
    }unOpCode;

#define  RmNpRmNpUpdateNodeState  unOpCode.sRmNpUpdateNodeState;
#define  RmNpFsNpHRSetStdyStInfo  unOpCode.sFsNpHRSetStdyStInfo;
} tRmNpModInfo;


/* Function Prototype for Generic NP OpCode specific wrappers
 * defined in Rmnpapi.c */

UINT1 RmRmNpUpdateNodeState PROTO ((UINT1 u1Event));
UINT1 RmFsNpHRSetStdyStInfo PROTO ((tRmHRAction eAction, tRmHRPktInfo * pRmHRPktInfo));
#ifdef MBSM_WANTED
#ifdef L2RED_WANTED
UINT1 RmMbsmNpProcRmNodeTransition PROTO ((INT4 i4Event, UINT1 u1PrevState, UINT1 u1State));
#endif /* L2RED_WANTED */
UINT1 RmMbsmNpClearHwTbl PROTO ((INT4 i4SlotId));
UINT1 RmMbsmNpGetStackMac PROTO ((UINT4 u4SlotId, UINT1 *pu1MacAddr));
UINT1 RmMbsmNpHandleNodeTransition PROTO ((INT4 i4Event, UINT1 u1PrevState, UINT1 u1State));
#endif /* MBSM_WANTED */

#endif /* __RM_NP_WR_H__ */
