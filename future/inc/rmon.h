/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rmon.h,v 1.31 2017/01/17 14:10:29 siva Exp $
 *
 * Description:This file contains the exported definitions and
 *             include files of RMON 
 *
 *******************************************************************/
#ifndef _RMON_H
#define _RMON_H

#define ADDRESS_LENGTH             6
#define RMON_INTERFACE_UP          1
#define RMON_INTERFACE_DOWN        2

#define RMON_LOCK() RmonLock ()
#define RMON_UNLOCK() RmonUnLock ()

#define OWN_STR_LENGTH          128
#define COMM_LENGTH             128

#define   tRmonHashTable       tTMO_HASH_TABLE
#define   tRmonHashNode        tTMO_HASH_NODE

typedef enum {
    RMON_HW_ETHER_STATS_BATCH_ONE = 1,
    RMON_HW_ETHER_STATS_BATCH_TWO = 2,
    RMON_HW_ETHER_STATS_BATCH_THREE = 3,
    RMON_HW_ETHER_STATS_BATCH_MAX = 4
}tRmonHwEtherStatsBatch;

typedef enum {
    RMON_VALID = 1,
    RMON_CREATE_REQUEST = 2,
    RMON_UNDER_CREATION = 3,
    RMON_INVALID = 4,
    RMON_MAX_STATE = 5
} tRmonStatus;

typedef enum {
    PORT_INDEX_TYPE = 1,
    VLAN_INDEX_TYPE = 2
}tRmonIndexType;

typedef struct RmonEtherStatsNode {
        
    UINT4                   u4EtherStatsDropEvents;
    UINT4                   u4EtherStatsOctets;
    UINT4                   u4EtherStatsPkts;
    UINT4                   u4EtherStatsBroadcastPkts;
    UINT4                   u4EtherStatsMulticastPkts;
    UINT4                   u4EtherStatsCRCAlignErrors;
    UINT4                   u4EtherStatsUndersizePkts;
    UINT4                   u4EtherStatsOversizePkts;
    UINT4                   u4EtherStatsFragments;
    UINT4                   u4EtherStatsJabbers;
    UINT4                   u4EtherStatsCollisions;
    UINT4                   u4EtherStatsPkts64Octets;
    UINT4                   u4EtherStatsPkts65to127Octets;
    UINT4                   u4EtherStatsPkts128to255Octets;
    UINT4                   u4EtherStatsPkts256to511Octets;
    UINT4                   u4EtherStatsPkts512to1023Octets;
    UINT4                   u4EtherStatsPkts1024to1518Octets;
    UINT4                   u4EtherStatsOutFCSErrors;
    UINT4                   u4EtherStatsPkts1519to1522Octets;
    UINT4                   u4EtherStatsHighCapacityOverflowPkts;
    UINT4                   u4EtherStatsHighCapacityOverflowOctets;
    UINT4                   u4EtherStatsHighCapacityOverflowPkts64Octets;
    UINT4                   u4EtherStatsHighCapacityOverflowPkts65to127Octets;
    UINT4                   u4EtherStatsHighCapacityOverflowPkts128to255Octets;
    UINT4                   u4EtherStatsHighCapacityOverflowPkts256to511Octets;
    UINT4                   u4EtherStatsHighCapacityOverflowPkts512to1023Octets;
    UINT4                   u4EtherStatsHighCapacityOverflowPkts1024to1518Octets;
    UINT4      u4EtherStatsBatchId; /* Since the stats are huge,
                       * collected them batch by 
                             * batch. 1batch=10stats */ 
    tSNMP_COUNTER64_TYPE    u8EtherStatsHighCapacityPkts;
    tSNMP_COUNTER64_TYPE    u8EtherStatsHighCapacityOctets;
    tSNMP_COUNTER64_TYPE    u8EtherStatsHighCapacityPkts64Octets;
    tSNMP_COUNTER64_TYPE    u8EtherStatsHighCapacityPkts65to127Octets;
    tSNMP_COUNTER64_TYPE    u8EtherStatsHighCapacityPkts128to255Octets;
    tSNMP_COUNTER64_TYPE    u8EtherStatsHighCapacityPkts256to511Octets;
    tSNMP_COUNTER64_TYPE    u8EtherStatsHighCapacityPkts512to1023Octets;
    tSNMP_COUNTER64_TYPE    u8EtherStatsHighCapacityPkts1024to1518Octets;
}tRmonEtherStatsNode;

typedef struct Frame_Particulars {
    tMacAddr                 SrcAddress;
    tMacAddr                 DestAddress;
    UINT4                    u4VlanId;
    UINT4                    u4Type;
    UINT2                    u2Length;
    UINT1                    u1DataSource;
    BOOLEAN                  b1WhetherError;
    BOOLEAN                  b1WhetherBroadcast;
    BOOLEAN                  b1WhetherMulticast;
    UINT1                    u1Reserved[2];
} tFrameParticulars;

typedef struct RmonExtnNode {
   UINT4  u4CreateTime;
   UINT4  u4DroppedFrames;
} tRmonExtnNode;

/*  Some typedefs for Interface Status updation */
typedef struct
{
    UINT2  u2port;
    UINT1  u1cmd;
    UINT1  u1Reserved[1];
}tRmonParms;

typedef   tCRU_BUF_CHAIN_HEADER        tRmonBuf;


INT4  RmonInitialize (void);   
INT1  RmonUpdateStatisticsTable (UINT1 *, tFrameParticulars *);
void  RmonUpdateHostModuleTables (tFrameParticulars *);
UINT1 RmonUpdateMatrixTables (tFrameParticulars *);
INT2  RmonGenerateErrorPkt (UINT1 *, UINT4 *);
INT1  RmonUpdateFrameParticulars (UINT1 *, tFrameParticulars *);

/* CFA Interface status updation */
INT4  RmonUpdateInterfaceStatus (UINT4 u4IfIndex, UINT1 u1Status);

VOID  RmonMain (INT1 * pu1Param);
INT4 RmonDeletePort (UINT4 u4IfIndex);
INT4 RmonProcessDeletePort (UINT4 u4IfIndex);
INT4 RmonDeleteVlan (UINT4 u4ContextId, UINT4 u4VlanId);
INT4 RmonPostDeleteVlan (UINT4 u4ContextId, UINT4 u4VlanId);

VOID RmonUtlGetEtherStatsExtn (INT4, tRmonExtnNode*);
VOID RmonUtlGetHistCtlExtn (INT4, tRmonExtnNode*);
VOID RmonUtlGetHostCtlExtn (INT4, tRmonExtnNode*);
VOID RmonUtlGetMtrxCtlExtn (INT4, tRmonExtnNode*);

INT4 RmonLock (VOID);
INT4 RmonUnLock (VOID);
#endif /* _RMON_H */

