/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dhcp.h,v 1.50 2017/09/22 12:19:28 siva Exp $
 *
 * Description:This file contains the exported definitions and  
 *             macros of DHCP                                  
 *
 *******************************************************************/
#ifndef _DHCP_H
#define _DHCP_H

PUBLIC INT4 DhrlApiGetIfCiruitId PROTO ((INT4 i4IfIndex, 
                                         UINT4 *pu4DhcpRelayIfCircuitId));
    
INT4  DhcpRelayInitialize      PROTO((VOID));
INT4  DhcpSrvShutDown          PROTO((VOID));
UINT4 DhcpClientInit           PROTO((VOID));
BOOLEAN DhcpIsSrvEnabled         PROTO((VOID));
BOOLEAN DhcpIsRelayEnabled       PROTO((VOID));
BOOLEAN DhcpIsRelayEnabledInCxt  PROTO((UINT4));
BOOLEAN DhcpIsRlyEnabledInAnyCxt PROTO((VOID));

/* Function Invoked By IP */
VOID DhcpCHandleIfStateChg (UINT4 u4Port, UINT4 u4BitMap);

/* For INIT-SEQ */
VOID  DhcpTaskMain             PROTO ((INT1 *i1pParam));
VOID  DhcpCTaskMain            PROTO((INT1 *));
VOID  DhcpClntSelectTaskMain   PROTO((INT1 *));
#if defined (LNXIP4_WANTED) && !defined (KERNEL_WANTED)
VOID  DhcpClntArpRespTaskMain  PROTO((INT1 *));
#endif
VOID  DhcpRelayMain            PROTO((INT1 *));
/***/
INT4 DhcpCheckOfferedIpOnInterface (UINT4 u4PortNo, UINT4 u4IpAddress);
/*Functions used for Dhcp Lock*/
INT4 DhcpSProtocolLock(VOID);
INT4 DhcpCProtocolLock (VOID);
INT4 DhcpRProtocolLock (VOID);
INT4 DhcpSProtocolUnLock(VOID);
INT4 DhcpCProtocolUnlock(VOID);
INT4 DhcpRProtocolUnlock(VOID);

INT1 DhcpUtilHandleAlloMethodChange (INT4, INT4, INT4, INT4);
 

#define DHCPS_PROTO_LOCK                DhcpSProtocolLock
#define DHCPC_PROTO_LOCK                DhcpCProtocolLock
#define DHCPR_PROTO_LOCK                DhcpRProtocolLock
#define DHCPS_PROTO_UNLOCK              DhcpSProtocolUnLock
#define DHCPC_PROTO_UNLOCK              DhcpCProtocolUnlock
#define DHCPR_PROTO_UNLOCK              DhcpRProtocolUnlock
/*end of Lock functions */

/* Message types used for DHCP server task */

#define DHCPS_RM_MSG       1

/* Message types used for DHCP client task */
#define DHCPC_IF_OPER_CHG  1
#define DHCPC_ACQUIRE_IP   2
#define DHCPC_RELEASE_IP   3
#define DHCPC_PKT_ARRIVED  4
#define DHCPC_RM_MSG       5
#define DHCPC_DISC_OPTION  6

#if defined (LNXIP4_WANTED) && !defined (KERNEL_WANTED)
#define DHCPC_ARP_REPLY    1
#endif

#define  DHCP_SUCCESS      1 
#define  DHCP_FAILURE      0 

#define  DHCP_NETMASK_CLASS_A          0xff000000
#define  DHCP_NETMASK_CLASS_B          0xffff0000
#define  DHCP_NETMASK_CLASS_C          0xffffff00
#define  DHCP_NETMASK_DEFAULT          0xffffffff

#ifdef DHCP_RLY_WANTED
#define DHRL_MAX_RID_LEN 32
#endif
  
#define DHCP_MAX_ADDR_LEN 32  
#define BOOTP_MIN_LEN           300 /*  Minimum length of BOOTP Messages  */
#define DHCP_OPTION_PAD         0   /* Padding */
#define DHCP_UDP_HDR_LEN                  8

/* Definition of DHCP message types */
#define  DHCP_DISCOVER                  1
#define  DHCP_OFFER                     2
#define  DHCP_REQUEST                   3
#define  DHCP_DECLINE                   4
#define  DHCP_ACK                       5
#define  DHCP_NACK                      6
#define  DHCP_RELEASE                   7
#define  DHCP_INFORM                    8
#define  BOOTP_PKT                      9

/* Bootp Message types */
#define  BOOTREQUEST                   1                                     
#define  BOOTREPLY                     2                                     

/* options type */
#define  DHCP_OPT_PAD                  0
#define  DHCP_OPT_SUBNET_MASK          1
#define  DHCP_OPT_ROUTER_OPTION        3
#define  DHCP_OPT_DNS_NS               6
#define  DHCP_OPT_HOST_NAME            12 
#define  DHCP_OPT_DNS_NAME             15
/* 25/11/2008 [RFC-2132, section 8.3]*/
#define  DHCP_OPT_NTP_SERVERS          42
#define  DHCP_OPT_SVENDOR_SPECIFIC     43
#define  DHCP_OPT_REQUESTED_IP         50
#define  DHCP_OPT_LEASE_TIME           51
#define  DHCP_OPT_OVERLOAD             52
#define  DHCP_OPT_MSG_TYPE             53
#define  DHCP_OPT_SERVER_ID            54
#define  DHCP_OPT_PARAMETER_LIST       55
#define  DHCP_OPT_CVENDOR_SPECIFIC     60
#define  DHCP_OPT_CLIENT_ID            61
#define  DHCP_OPT_MAX_MESSAGE_SIZE     57
#define  DHCP_OPT_TFTP_SNAME           66
#define  DHCP_OPT_BOOT_FILE_NAME       67
#define  DHCP_OPT_USER_CLASS           77
#define  DHCP_OPT_SIP_SERVER        120
#define  DHCP_OPT_WLC_AC_DISC_V4       138
#define  DHCP_OPT_240             240
#define  DHCP_OPT_END                  255
#define  DHCP_MAX_OPT_LEN              255
#define  DHCP_MAX_ALLOWED_LEN          250
/* Maximum packet that can be processed by DHCP */
#define  DHCP_MAX_MTU                  CFA_ENET_MTU

#define DHCP_MAX_STR_IP_LEN        16
#define DHCP_MAX_POOLNAME_LEN          20

/* DHCP Message related Macros */
#define  DHCP_DEF_MAX_MSGLEN           312
#define  DHCP_DEF_MESSAGE_SIZE         312
#define  DHCP_LEN_FIXED_HDR            240 /* Include Magic Cookie also */
#define  DHCP_LEN_MAGIC                4
#define  DHCP_MAX_OUTMSG_SIZE          2048
#define  DHCP_MAX_OUT_OPTION_LEN       400 /*The Max option length supported in server 
                                            is 312 (DHCP_DEF_MAX_MSGLEN) the 
                                            current length is buffered to 400. 
                                            It is a Tuneable value in future*/

#define  DHCP_MAX_CID_LEN              64
#define  DHCP_LEN_TYPE_LEN             2

#define  DHCP_FOUND                    1 
#define  DHCP_NOT_FOUND                0 

#define DHCPC_TFTP_SERVER_NAME         66
#define DHCPC_BOOT_FILE_NAME           67
#define DHCPC_SIP_SERVER        120
#define DHCPC_OPTION_IP         0
#define DHCPC_OPTION_STRING        1
/* UDP Ports used by the DHCP Client and Server */
#define  DHCP_PORT_SERVER              67
#define  DHCP_PORT_CLIENT              68

/* DHCP Trace Levels */
#define  BIND_TRC         0x00000010
#define  ALL_TRC          0x0000ffff
#define  EVENT_TRC        0x00000001
#define  FAIL_TRC         ALL_FAILURE_TRC

#define  DHCP_ENABLE_STATUS      1
#define  DHCP_DISABLE_STATUS     0

#define DHCP_ENABLE       1
#define DHCP_DISABLE      2

/* Flags used for DHCP client */
#define  DHCP_BROADCAST_MASK           0x8000

#define DHCP_INIT_COMPLETE(u4Status)    lrInitComplete(u4Status)

#define DHCP_IS_VALID_IP(u4Addr) \
               ( (IP_IS_ADDR_CLASS_A(u4Addr) ? \
                 (IP_IS_ZERO_NETWORK(u4Addr) ? 0: 1) : 0)\
                 || (IP_IS_ADDR_CLASS_B (u4Addr))\
                 || (IP_IS_ADDR_CLASS_C(u4Addr))\
               )

#define DHCP_IS_VALID_NETMASK(u4Mask) \
               ( (~(u4Mask | (u4Mask -1))) == 0 )

#define OFFER_RECEVIED_WITH_OPTION   1
#define OFFER_COMPLETED              2
#define DHCPC_RELEASE_STATUS_SET     1
#define DHCP_MIN_HW_TYPE             1
#define DHCP_MAX_HW_TYPE             255

/* Definition of the rcvd DHCP Pkt - for detailed description see
 * IETF RFC 2131 section 2.*/
typedef struct sdhcpmsghdr {
    UINT1  Op;           /* 1 = BOOTREQUEST, 2 = BOOTREPLY          */
    UINT1  htype;        /* Hardware address type.                  */
    UINT1  hlen;         /* Hardware address length.                */
    UINT1  hops;
    UINT4  xid;          /* Transaction ID - random number          */
    UINT2  secs;
    UINT2  flags;
    UINT4  ciaddr;       /* Client IP address                       */
    UINT4  yiaddr;       /* 'your' (client) IP address              */
    UINT4  siaddr;
    UINT4  giaddr;       /* Relay agent IP address                  */
    UINT1  chaddr[16];   /* client hardware address                 */
    UINT1  sname[64];
    UINT1  file[128];
    UINT1  *pOptions;   /* options - does not includes magic cookie */
} tDhcpMsgHdr;

#define DHCP_OPTION_BLOCKS        8
#define DHCP_OPTION_BLOCK_SIZE   32
typedef struct DhcpCDiscCbInfo
{
    INT4 aiOptionBitMask [DHCP_OPTION_BLOCKS];
    INT4 (*pDhcDiscoveryCbFn) (UINT1, UINT1 *,UINT1);
    INT4 iTaskId;
}tDhcpCDiscCbInfo;

UINT4 DhcpClientUtilCallBackRegister (tDhcpCDiscCbInfo* pCbInfo);
UINT4 DhcpClientUtilFillDhcpOptionBitMask (INT4 *pOptions, INT4 i1Option);

/* Callback Registration */
UINT1 DhcpCRegisterOptions (UINT4, VOID (*pDhcpCOptionCallBack)(UINT4,UINT4, VOID *));
VOID DhcpCUnRegisterOptions (UINT4);
VOID DhcpRlyNotifyContextChange (UINT4, UINT4, UINT1);

/* Testing Function -- Delete after testing is done */
VOID test_func(UINT4, UINT4, VOID *);

/* Assigning appropriate value for cmsg_level based on processor bit*/
#ifdef SIXTY_FOUR_BIT_LONG
#define    DHCP_CMSG_LEVEL  IPPROTO_UDP
#else
#define    DHCP_CMSG_LEVEL  SOL_IP
#endif

#endif /* _DHCP_H */
