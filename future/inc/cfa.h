/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfa.h,v 1.365.2.1 2018/05/10 12:22:48 siva Exp $
 *
 * Description:This file contains the exported definitions and  
 *             macros of CFA                                   
 *
 *******************************************************************/
#ifndef _CFA_H
#define _CFA_H

#include "mbsm.h"
#include "nat.h"
#include "fssnmp.h"
#include "cust.h"
#include "ipv6.h"

#define CFA_ADD     1
#define CFA_DEL     2
#define CFA_DEL_ALL 3

#define CFA_PORTCHANNEL  OSIX_SUCCESS

#define CFA_ZERO    0

#define CLI_MAX_IFTYPES      29
#define CLI_MAX_IFSUBTYPES    2
#define CFA_IP_INTERFACE_TYPE 65554
#define BASE_DECIMAL 10
#define MAX_MOD_EVENTS        2
#ifdef L2RED_TEST_WANTED
#define CFA_STACKSIZE_VALUE 40000
#else
#define CFA_STACKSIZE_VALUE 30000
#endif

#define   CFA_PACKET_QUEUE_MUX        "CFAMUX"                    
#define   CFA_TASK_NAME       "CFA"
#define   LINKUP_TASK_NAME       "Lnk"

#define   CFA_LOCK()            CfaLock ()

#define   CFA_UNLOCK()          CfaUnlock ()

#define   CFA_PKTTX_LOCK()            CfaPktTxLock ()

#define   CFA_PKTTX_UNLOCK()          CfaPktTxUnlock ()
#define   CFA_AST_PKT_LOCK()          CfaPacketTxLock ()
#define   CFA_AST_PKT_UNLOCK()        CfaPacketTxUnLock ()

#define CFA_MAX_NUM_OF_VIP                 SYS_DEF_MAX_VIP_IFACES 

#define CFA_MAX_SISP_INTERFACES            SYS_DEF_MAX_SISP_IFACES 

#define CFA_MAX_L3SUB_IFACES               SYS_DEF_MAX_L3SUB_IFACES 

/* Interface ranges for various interface types */

#if defined (WLC_WANTED) || defined (WTP_WANTED) 

#define CFA_MIN_WSS_IF_INDEX            (SYS_DEF_MAX_PHYSICAL_INTERFACES + \
        LA_MAX_AGG + 1)
#define CFA_MAX_WSS_IF_INDEX            (SYS_DEF_MAX_PHYSICAL_INTERFACES + \
        LA_MAX_AGG + SYS_DEF_MAX_WSS_IFACES)
                                        
#define CFA_MIN_WSS_BSSID           CFA_MIN_WSS_IF_INDEX
#define CFA_MAX_WSS_BSSID           (CFA_MIN_WSS_BSSID + \
                                        (MAX_NUM_OF_BSSID_PER_RADIO * MAX_NUM_OF_RADIOS)\
                                        - 1)

#define CFA_MIN_WSS_DOT11_SSID      (CFA_MAX_WSS_BSSID + 1)
#define CFA_MAX_WSS_DOT11_SSID      (CFA_MAX_WSS_BSSID + \
                                        MAX_NUM_OF_SSID_PER_WLC)

#define CFA_MIN_WSS_VIRT_RADIO      (CFA_MAX_WSS_DOT11_SSID + 1)
#define CFA_MAX_WSS_VIRT_RADIO      (CFA_MAX_WSS_DOT11_SSID + MAX_NUM_OF_RADIOS)

#define CFA_MIN_IVR_IF_INDEX          (CFA_MAX_WSS_IF_INDEX + 1)

#define CFA_MAX_IVR_IF_INDEX          (CFA_MAX_WSS_IF_INDEX + \
                                           IP_DEV_MAX_L3VLAN_INTF)
#else

#define CFA_MIN_IVR_IF_INDEX   (SYS_DEF_MAX_PHYSICAL_INTERFACES + \
    LA_MAX_AGG + 1)

#define CFA_MAX_IVR_IF_INDEX   (SYS_DEF_MAX_PHYSICAL_INTERFACES + \
    LA_MAX_AGG + IP_DEV_MAX_L3VLAN_INTF)

#endif

#define CFA_MIN_TNL_IF_INDEX   CFA_MAX_IVR_IF_INDEX + 1

#define CFA_MAX_TNL_IF_INDEX   CFA_MAX_IVR_IF_INDEX + SYS_DEF_MAX_TUNL_IFACES

#define CFA_MIN_MPLS_IF_INDEX  CFA_MAX_TNL_IF_INDEX + 1

#define CFA_MAX_MPLS_IF_INDEX  CFA_MAX_TNL_IF_INDEX + SYS_DEF_MAX_MPLS_IFACES

#define CFA_MIN_MPLSTNL_IF_INDEX \
        CFA_MAX_MPLS_IF_INDEX + 1

#define CFA_MAX_MPLSTNL_IF_INDEX    \
        CFA_MAX_MPLS_IF_INDEX + SYS_DEF_MAX_MPLS_TNL_IFACES 

#define CFA_MIN_VPNC_IF_INDEX            (CFA_MAX_MPLSTNL_IF_INDEX + 1)

#define CFA_MAX_VPNC_IF_INDEX            (CFA_MAX_MPLSTNL_IF_INDEX+\
                                          SYS_DEF_MAX_NUM_OF_VPNC )

#define CFA_MIN_LOOPBACK_IF_INDEX     CFA_MAX_VPNC_IF_INDEX + 1

#define CFA_MAX_LOOPBACK_IF_INDEX     CFA_MAX_VPNC_IF_INDEX + \
                                        SYS_MAX_LOOPBACK_INTERFACES

#define CFA_IS_PPP_IFINDEX(u4IfIndex)  \
        (((u4IfIndex >= CFA_MIN_PPP_IF_INDEX) &&  \
          (u4IfIndex <= CFA_MAX_PPP_IF_INDEX)) ? (CFA_TRUE) : (CFA_FALSE))

#define CFA_MIN_PPP_IF_INDEX          CFA_MAX_LOOPBACK_IF_INDEX + 1 

#define CFA_MAX_PPP_IF_INDEX          CFA_MAX_LOOPBACK_IF_INDEX + \
                                         SYS_MAX_PPP_INTERFACES

#define CFA_MIN_ILAN_IF_INDEX           (CFA_MAX_PPP_IF_INDEX + 1)

#define CFA_MAX_ILAN_IF_INDEX           (CFA_MAX_PPP_IF_INDEX +\
                                         SYS_DEF_MAX_ILAN_IFACES)
                                         
#define CFA_MIN_INTERNAL_IF_INDEX       (CFA_MAX_ILAN_IF_INDEX + 1)

#define CFA_MAX_INTERNAL_IF_INDEX       (CFA_MAX_ILAN_IF_INDEX +\
                                         SYS_DEF_MAX_INTERNAL_IFACES)

#define CFA_MIN_TELINK_IF_INDEX            (CFA_MAX_INTERNAL_IF_INDEX + 1)
#define CFA_MAX_TELINK_IF_INDEX            (CFA_MAX_INTERNAL_IF_INDEX + \
                                            SYS_DEF_MAX_TELINK_INTERFACES)

#define CFA_MIN_PSW_IF_INDEX          (CFA_MAX_TELINK_IF_INDEX + 1)
#define CFA_MAX_PSW_IF_INDEX          (CFA_MAX_TELINK_IF_INDEX + \
                                          SYS_DEF_MAX_L2_PSW_IFACES)

#define CFA_MIN_AC_IF_INDEX           (CFA_MAX_PSW_IF_INDEX + 1)
#define CFA_MAX_AC_IF_INDEX           (CFA_MAX_PSW_IF_INDEX + \
                                         SYS_DEF_MAX_L2_AC_IFACES)

#define CFA_MIN_NVE_IF_INDEX          (CFA_MAX_AC_IF_INDEX + 1) 

#define CFA_MAX_NVE_IF_INDEX          (CFA_MAX_AC_IF_INDEX + \
                                       SYS_DEF_MAX_NVE_IFACES )
#ifdef HDLC_WANTED
#define CFA_MAX_NUM_OF_HDLC           10
#else
#define CFA_MAX_NUM_OF_HDLC           0
#endif
#define CFA_MIN_HDLC_INDEX            (CFA_MAX_NVE_IF_INDEX + 1)
#define CFA_MAX_HDLC_INDEX            (CFA_MAX_NVE_IF_INDEX +\
                                         CFA_MAX_NUM_OF_HDLC)

#define CFA_MIN_EVB_SBP_INDEX            (CFA_MAX_HDLC_INDEX + 1)
#define CFA_MAX_EVB_SBP_INDEX            (CFA_MAX_HDLC_INDEX + \
                                       SYS_DEF_MAX_SBP_IFACES)

#define CFA_MIN_TAP_IF_INDEX          (CFA_MAX_EVB_SBP_INDEX + 1)

#define CFA_MAX_TAP_IF_INDEX          (CFA_MAX_EVB_SBP_INDEX + \
                                       MAX_TAP_INTERFACES)

#define CFA_MIN_L3SUB_IF_INDEX         (CFA_MAX_TAP_IF_INDEX + 1)

#define CFA_MAX_L3SUB_IF_INDEX         (CFA_MAX_TAP_IF_INDEX +\
                                         CFA_MAX_L3SUB_IFACES)

#define CFA_MIN_SISP_IF_INDEX         (CFA_MAX_L3SUB_IF_INDEX+ 1)

#define CFA_MAX_SISP_IF_INDEX         (CFA_MAX_L3SUB_IF_INDEX + \
                                           CFA_MAX_SISP_INTERFACES)

#define CFA_MIN_VIP_IF_INDEX            (CFA_MAX_SISP_IF_INDEX + 1)

#define CFA_MAX_VIP_IF_INDEX            (CFA_MAX_SISP_IF_INDEX +\
                                         CFA_MAX_NUM_OF_VIP)

#define CFA_GET_DEFAULT_SCHANNEL_IFINDEX(u4UapIfIndex,u4SChIfIndex) \
        u4SChIfIndex = (CFA_MIN_EVB_SBP_INDEX + (VLAN_EVB_MAX_SBP_PER_UAP *    \
                                           (u4UapIfIndex - 1)));


/* MAX valid CIDR subnet mask number */
#define CFA_MAX_CIDR                  32

#ifdef OPENFLOW_WANTED
#define CFA_MAX_NUM_OF_OF               VLAN_DEV_MAX_NUM_VLAN
#else
#define CFA_MAX_NUM_OF_OF               0
#endif 
#define CFA_MIN_OF_IF_INDEX            (CFA_MAX_VIP_IF_INDEX + 1)

#define CFA_MAX_OF_IF_INDEX            (CFA_MAX_VIP_IF_INDEX +\
                                         CFA_MAX_NUM_OF_OF)

#if defined (WLC_WANTED) || defined (WTP_WANTED)                                         
#define CFA_MAX_NUM_OF_RADIO            (NUM_OF_AP_SUPPORTED * \
                                         MAX_NUM_OF_RADIO_PER_AP)

#define CFA_MAX_NUM_OF_WLAN             MAX_NUM_OF_SSID_PER_WLC

#define CFA_MAX_NUM_OF_WLAN_BSS         (CFA_MAX_NUM_OF_RADIO * \
                                         MAX_NUM_OF_BSSID_PER_RADIO)
                                     
#endif

#define INTERNAL_PORTS_PER_BYTE            8

#define   BRG_PORT_INTERNAL_LIST_SIZE   ((CFA_MAX_INTERNAL_IF_INDEX/INTERNAL_PORTS_PER_BYTE) + 1)
#define   BRG_PORT_ILAN_LIST_SIZE       ((CFA_MAX_ILAN_IF_INDEX/INTERNAL_PORTS_PER_BYTE) + 1)

#define   CFA_CLI_MAX_IF_NAME_LEN    CFA_MAX_PORT_NAME_LENGTH + \
    VCM_ALIAS_MAX_LEN + 3

#define INTERNAL_BIT8                      0x80

#ifdef NAT_WANTED 
#define CFA_CHECK_IF_NAT_ENABLE(IfIndex) NatCheckIfNatEnable(IfIndex) 
#else
#define CFA_CHECK_IF_NAT_ENABLE(IfIndex) NAT_DISABLE
#endif

#ifdef ICCH_WANTED
#define CFA_ICCH_DEFAULT_INST ICCH_DEFAULT_INST
#else
#define CFA_ICCH_DEFAULT_INST 0
#endif

#define CFA_FRONTPANEL_INTF         1
#define CFA_BACKPLANE_INTF          2
#define CFA_NP_IPV6_PROTO   16

/* Macros for interface capabilities */
#ifdef NPAPI_WANTED
#define CFA_CLI_MAX_DUPLEX_LENGTH 32
#define CFA_CLI_MAX_SPEED_LENGTH 100
#define CFA_CLI_MAX_TYPE_LENGTH 100
#endif

/* Persistence of Interface */
#define   CFA_PERS_OTHER                 1
#define   CFA_PERS_DEMAND                2
#define   CFA_PERS_AUTO                  3

#define  CFA_NETWORK_TYPE_LAN             1
#define  CFA_NETWORK_TYPE_WAN             2
#ifdef LNXIP4_WANTED
#define CFA_PROXY_ARP_DISABLE      0
#endif

/* CFA Changes for flexible IfIndex */

typedef tRBNodeEmbd tCfaIfNode;
typedef tTMO_DLL_NODE tCfaIfTypeNode;

/* CFA_GAPIF_DB_ENTRY -
 * MACRO to be used in the GAP IF structure wherein 
 * the memory block is part of the GAP IF database */

#define CFA_GAPIF_DB_ENTRY \
        tCfaIfNode         CfaGapIfInfoNode; \
        UINT4              u4CfaGapIfIndex 

/* CFA_IF_DB_ENTRY -
 * Macro which can be used in structures where the
 * memory block  can be part of two different database.
 * Node pointers are used with
 * requisite offsets while adding to the respective
 * DB (IFDB (Interface based database) or IFLISTDB (Interface
 * type based database). Thus the order below should
 * not be changed */

#define CFA_IF_DB_ENTRY \
        tCfaIfNode       CfaIfInfoNode; \
        tCfaIfTypeNode   CfaIfTypeNode; \
        UINT4            u4CfaIfIndex

enum {
    PNAC_MODULE = 1,
    LA_MODULE,
    VLAN_MODULE,
    STP_MODULE,
    BRIDGE_MODULE,
    GARP_MODULE,
    MRP_MODULE,
    SNOOP_MODULE,
    IVR_MODULE,
    MPLS_MODULE,
    CFA_MODULE,
    LLDP_MODULE,
    PBB_MODULE,
    RMON_MODULE,
    ERPS_MODULE,
    PTP_MODULE
};

enum {
    IF_TYPE = 1,
    IF_OPER_STATUS,
    IF_SPEED,
    IF_MAC_ADDRESS,
    IF_NAME,
    IF_DESC,
    IF_MTU,
    IF_VLANID_OF_IVR,
    IF_DUPLEX_STATUS,
    IF_AUTONEG_STATUS,
    IF_BRIDGE_IFACE_STATUS,
    IF_COUNTERS,
    IF_ACTIVE_STATUS,
    IF_VALID_STATUS,
    IF_IP_PORTNUM,
    IF_EOAM_STATUS,
    IF_EOAM_MUX_STATE,
    IF_EOAM_PARSER_STATE, 
    IF_EOAM_UNIDIR_SUPP,
    IF_EOAM_LINK_FAULT,
    IF_EOAM_REMOTE_LB,
    IF_BRG_PORT_TYPE,
    IF_PAUSE_ADMIN_MODE,
    IF_L3VLAN_INTF_STATS,
    IF_IP_IDX_MGR_NUM
};

typedef enum {
   CFA_PROTO_NONE = 0,
   CFA_PROTO_RARP,
   CFA_PROTO_DHCP,
   CFA_PROTO_BOOTP
}tCfaIpAddrAllocProto;

#define CFA_L3_VLAN_COUNTER_ENABLE                              127
#define CFA_L3_VLAN_COUNTER_DISABLE                             126

#define  CFA_MAX_INTERFACES_IN_SYS      SYS_MAX_INTERFACES
#define  CFA_NUM_INTERFACES_IN_SYSTEM   SYS_NUM_INTERFACES
#define  CFA_MAX_USER_DATA_LEN                           4
#define  CFA_TRUE                                        1
#define  CFA_FALSE                                       2

#define CFA_ENABLED                                      1
#define CFA_DISABLED                                     2
#define CFA_SEC_BRIDGE_ENABLED                           1
#define CFA_SEC_BRIDGE_DISABLED                          2
#define  CFA_LINK_UNK                                    4
#define  CFA_ENET                                        6
#define  CFA_TOKENRING                                   9
#define  CFA_FDDI                                        15
#define  CFA_ENET_ADDR_LEN                               6
#define  CFA_VENDOR_OUI_LEN                              3
#define  CFA_SUCCESS                                     0
#define  CFA_IF_UP                                       1
#define  CFA_IF_DOWN                                     2
#define  CFA_IF_DEL                                      3
#define  CFA_IF_DORM                                     5
#define  CFA_IF_NP                                       6                
#define  CFA_IF_CRT                                      11
#define  CFA_IF_UFD_ERR_DISABLED                         3
#define  CFA_LOOPBACK_LOCAL                              7
#define  CFA_NO_LOOPBACK_LOCAL                           8 
#define  CFA_IF_AGEOUT                                   12
#define  QoS_SHAPE_TIMER_EXPIRY_EVENT                 0x20

/* UFD related macros - start */
#define CFA_UFD_GROUP_PORTS_ADD                          1
#define CFA_UFD_GROUP_PORTS_DELETE                       2
#define CFA_PORT_ROLE_UPLINK                             1
#define CFA_PORT_ROLE_DOWNLINK                           2
#define CFA_PORT_ROLE_DESIG_UPLINK                       3
#define CFA_UFD_MAX_GROUP                                8
#define CFA_UFD_GROUP_MAX_NAME_LEN                      32
#define CFA_UFD_GROUP_MAX_UPLINK                        48
#define CFA_UFD_GROUP_MAX_DOWNLINK                      48


#define CFA_UFD_GROUP_UP                                 1
#define CFA_UFD_GROUP_DOWN                               2

/* CFA - LA interaction functions */
#define CFA_LA_ADD_PORT_TO_PORT_CHANNEL_MSG              1
#define CFA_LA_PORT_CHANNEL_OPER_MSG                     2

typedef enum {
   CFA_UFD_START    = 1,
   CFA_UFD_SHUTDOWN = 2
}tUfdSysControl;

typedef enum {
   CFA_UFD_ENABLED  = 1,
   CFA_UFD_DISABLED = 2
}tUfdEnable;

extern tUfdSysControl gUfdSystemControl;
extern tUfdEnable     gUfdModuleStatus;
extern UINT4          gau4ModTrcEvents[MAX_MOD_EVENTS];

/* Dual Oob secondary ip related macros*/
#define CFA_NODE0      0
#define CFA_NODE1      1

/* Dual Oob enabled status initialization macro */
#ifdef DUAL_OOB_WANTED
#define CFA_FETCH_DUAL_OOB_STATUS() gu4DualOob = OSIX_TRUE; 
#else
#define CFA_FETCH_DUAL_OOB_STATUS() gu4DualOob = OSIX_FALSE;
#endif





#define CFA_UFD_SYSTEM_CONTROL()  gUfdSystemControl
#define CFA_UFD_MODULE_STATUS() gUfdModuleStatus



typedef enum {
       CFA_LINKUP_DELAY_START    = 1,
       CFA_LINKUP_DELAY_SHUTDOWN = 2
}tLnkUpSysControl;

#define CFA_LNKUP_SYSTEM_CONTROL()  gLnkUpSystemControl

/* Structure to notify LA updates to CFA module*/
typedef struct CfaLaInfoMsg
{
    UINT4  u4IfIndex;       /* Physical interface index if u1MsgType is 
                               CFA_LA_ADD_PORT_TO_PORT_CHANNEL_MSG.
                               Port channel interface index if u1MsgType is
                               CFA_LA_PORT_CHANNEL_OPER_MSG. */
    UINT1  u1OperStatus;    /* Oper status of port channel */ 
    UINT1  u1MsgType;       /* CFA_LA_ADD_PORT_TO_PORT_CHANNEL_MSG or
                               CFA_LA_PORT_CHANNEL_OPER_MSG. */
    UINT1  u1Reserved[2];   /* Padding */
}
tCfaLaInfoMsg;


/* UFD related macros - end */


/* Split Horizon - Macros */

typedef enum {
    CFA_SH_START    = 1,
    CFA_SH_SHUTDOWN = 2
}tShSysControl;

typedef enum {
    CFA_SH_ENABLED  = 1,
    CFA_SH_DISABLED = 2
}tShEnable;

extern tShSysControl gShSystemControl;
extern tShEnable     gShModuleStatus;

#define CFA_SH_SYSTEM_CONTROL gShSystemControl
#define CFA_SH_MODULE_STATUS gShModuleStatus


/* MPLS_IPv6 add start*/
#define CFA_NOTIFYV6                                            3
/* MPLS_IPv6 add end*/

#define MAU_AUTO_B_OTHER             0x8000
#define MAU_AUTO_B_10_BASE_T         0x4000
#define MAU_AUTO_B_10_BASE_TFD       0x2000
#define MAU_AUTO_B_100_BASE_T4       0x1000
#define MAU_AUTO_B_100_BASE_TX       0x0800
#define MAU_AUTO_B_100_BASE_TXFD     0x0400
#define MAU_AUTO_B_100_BASE_T2       0x0200
#define MAU_AUTO_B_100_BASE_T2FD     0x0100
#define MAU_AUTO_B_FDX_PAUSE         0x0080
#define MAU_AUTO_B_FDXA_PAUSE        0x0040
#define MAU_AUTO_B_FDXS_PAUSE        0x0020
#define MAU_AUTO_B_FDXB_PAUSE        0x0010
#define MAU_AUTO_B_1000_BASE_X       0x0008
#define MAU_AUTO_B_1000_BASE_XFD     0x0004
#define MAU_AUTO_B_1000_BASE_T       0x0002
#define MAU_AUTO_B_1000_BASE_TF      0x0001
#define MAU_AUTO_B_10GIG_BASE_X      0x001F

#define CFA_MAX_CNP_COUNT_TO_TOGGLE   1

/* Constants used for detecting DOS attacks */

#define  CFA_IP_V4                    4
#define  CFA_IP_V4_ADDR_LEN           4
#define  CFA_IP_HDR_LEN               20
#define  CFA_IPV6_HDR_LEN             40
#define CFA_IP_V6                             6
#define  CFA_IP_PROTO_OFFSET_IN_IPHDR   9 
#define  CFA_TCP_RST_MASK             0x04
#define  CFA_TCP_ACK_MASK             0x10
#define  CFA_TCP_FIN_MASK             0x01
#define  CFA_TCP_SYN_MASK             0x02
#define  CFA_TCP_CODE_BIT_OFFSET      13  /* For Ack and Rst bits offset */

#define  CFA_IRDP_ADVERTISEMENT       9
#define  CFA_TCP_URG_MASK             0x20
#define  CFA_TCP_PUSH_MASK            0x08
         
#define  CFA_TCP_LENGTH_OFFSET        12
#define  CFA_HEADER_LEN_MASK          0x0f
#define  CFA_UDP_HEADER_LEN           8  /* Minimum Header length of UDP Pkt */

/* Storage Type Definitions - same as the MIB variable */
#define CFA_STORAGE_TYPE_OTHER       1
#define CFA_STORAGE_TYPE_VOLATILE    2
#define CFA_STORAGE_TYPE_NONVOLATILE 3
#define CFA_STORAGE_TYPE_PERMANENT   4
#define CFA_STORAGE_TYPE_READONLY    5



/******************************************************************************/
/*             IANA assigned Internet Protocol Numbers                        */
/******************************************************************************/
    
#define  CFA_ICMP           1   /*  Internet Control Message Protocol       */
#define  CFA_TCP            6   /*  Transmission Control Protocol           */
#define  CFA_UDP           17   /*  User Datagram protocol                  */
#define IP_FLAGS_BITS      3

/* invalid ifIndex & IpPort */
#define   CFA_MAX_U2_VALUE                (0xffff)         
#define   CFA_INVALID_INDEX           CFA_MAX_U2_VALUE 

/* Enet configuration options - IOCTL */
#define  CFA_ENET_EN_PROMISC                             0
#define  CFA_ENET_DIS_PROMIS                             1

#define  CFA_MAX_DESCR_LENGTH                           36

#define  CFA_MAX_PORT_NAME_LENGTH                       24
#define  CFA_SLOT_1                 "Slot0/1"
#define  CFA_TYPE_WIRELESS              0x08
#define  CFA_OFFSET_1                   1
#define  CFA_OFFSET_2                   2
#define  CFA_OFFSET_4                   4
#define  CFA_OFFSET_6                   6
#define  CFA_OFFSET_8                   8
#define  CFA_OFFSET_10                  10

#ifdef VXLAN_WANTED
#define  CFA_MAX_VXLAN_PORT_NAME_LENGTH                  3
#endif

#ifdef VCPEMGR_WANTED
#define  CFA_MAX_TAP_PORT_NAME_LENGTH                    3
#endif

#define  CFA_MAX_IFALIAS_LENGTH                         64 

#define  CFA_MAX_SISP_NAME_LEN                           4
#define  CFA_SWITCH_ALIAS_LEN      VCM_ALIAS_MAX_LEN
/* Maximum 20 characters can be there in UINT8 value */
#define  CFA_CLI_U8_STR_LENGTH                          21

#define CFA_MAX_BRG_PORT_TYPE_LEN                       36


#define  CFA_MAX_MEDIA_ADDR_LEN                         40

#define  CFA_MAX_PSW_LENGTH                             6

/* type of encaps - same as MIB variable enums */
#define  CFA_ENCAP_NONE                                  0

/* types supported currently - same as IANAifTypes shown in the MIB */
#define  CFA_PPP                                        23
#define  CFA_WLAN                                       71
#define  CFA_MP                                        108
#define  CFA_HDLC                                      118
#define  CFA_PPP_CONFIG                                175


/* Protocol defines used for masking */
#define  CFA_PROT_BPDU                                0x10
#define  CFA_DATA                                     0x01


/* Protocol definitions for bridge encapsulation over atm */
#define  CFA_ENET_BRE                                0x0007
#define  CFA_BPDU                                    0x000E

/* CFA routine's return codes */
#define  CFA_FAILURE                                  (-1)

/* type of encaps - same as MIB variable enums */
#define   CFA_ENCAP_OTHER                                1
#define   CFA_ENCAP_NLPID                                2
#define  CFA_ENCAP_LLC_SNAP                              6
#define  CFA_ENCAP_VC_MUX                                7
#define  CFA_ENCAP_ENETV2                                8
#define  CFA_ENCAP_LLC                                   9

/* types supported currently - same as IANAifTypes shown in the MIB */
#define  CFA_INVALID_TYPE                              255

/*
 * implies the interface is either the highest or lowest layer. Also is used 
 * for IfIndex 
 */
#define  CFA_NONE                                        0
#define  CFA_ASYNC                                      84
#define  CFA_TUNNEL                                    131
#define  CFA_L2VLAN                                    135
#define  CFA_L3IPVLAN                                  136
#define  CFA_IFPWTYPE                                  246
#define  CFA_LOOPBACK                                  24 
#define  CFA_L3SUB_INTF                                137

/* Additional type for fast ethernet & gigabit ethernet */
/* In coldstandby case higig ports will be used stack communication.*/
#define  CFA_LVI_ENET                                   245 /* not standard */
#define  CFA_STACK_ENET                                250
#define  CFA_XE_ENET                                   251 /* not standard */
#define  CFA_FA_ENET                                   252 /* not standard */
#define  CFA_GI_ENET                                   253 /* not standard */
#define  CFA_XL_ENET                                   254 /* not standard */
#define  CFA_ENET_UNKNOWN                              255 /* not standard */
#define  CFA_MPLS                                      166
#define  CFA_LAGG                                      161
#define  CFA_RFC1483                                   159
#define  CFA_MPLS_TUNNEL                               150
#define  CFA_BRIDGED_INTERFACE                         209
#define  CFA_ILAN                                      247
#define  CFA_PIP                                       248
#define  CFA_PROP_VIRTUAL_INTERFACE                    53
#define  CFA_VPNC                                      249    /* not standard */
#define  CFA_TELINK                                    200
#define  CFA_IF_TYPES                                  11
#define  CFA_PSEUDO_WIRE                               246
#define  CFA_OTHER                                     1
#define  CFA_VXLAN_NVE                                 244 /* not stadndard */
#define  CFA_TAP                                       243 /* not standard */
#define  CFA_SLOT_NUM_OFFSET                           4
/* SubType Macros */
#define  CFA_SUBTYPE_SISP_INTERFACE                    0
#define  CFA_SUBTYPE_AC_INTERFACE                      1
#define  CFA_SUBTYPE_OPENFLOW_INTERFACE                2 

#if defined (WLC_WANTED) || defined (WTP_WANTED)
#define  CFA_RADIO                                     71
#define  CFA_CAPWAP_VIRT_RADIO                         154 /* not standard */
#define  CFA_CAPWAP_DOT11_PROFILE                      152 /* not standard */
#endif
#define  CFA_WLAN_RADIO                                3  /* not standard */
#define  CFA_CAPWAP_DOT11_BSS                          153 /* not standard */

/* Bridge port types. */
#define  CFA_PROVIDER_NETWORK_PORT                     1
#define  CFA_CNP_PORTBASED_PORT                        2
#define  CFA_CNP_STAGGED_PORT                          3
#define  CFA_CUSTOMER_EDGE_PORT                        4
#define  CFA_PROP_CUSTOMER_EDGE_PORT                   5
#define  CFA_PROP_CUSTOMER_NETWORK_PORT                6
#define  CFA_PROP_PROVIDER_NETWORK_PORT                7
#define  CFA_CUSTOMER_BRIDGE_PORT                      8
#define  CFA_CNP_CTAGGED_PORT                          9
#define  CFA_VIRTUAL_INSTANCE_PORT                     10
#define  CFA_PROVIDER_INSTANCE_PORT                    11
#define  CFA_CUSTOMER_BACKBONE_PORT                    12
/* NOTE: check whether the PEP and IBP dont have impact */
#define  CFA_UPLINK_ACCESS_PORT                        13
#define  CFA_STATION_FACING_BRIDGE_PORT                14
#define  CFA_PROVIDER_EDGE_PORT                        15 /* Internal port */
#define  CFA_INVALID_BRIDGE_PORT                       16

/* MPLS related definitions. */
#define  CFA_ENET_MPLS                              0x8847
#define  CFA_ENET_PTP                               0x88F7

#define   CFA_ENET_IPV6                             0x86DD

/* ISIS related constants */
#define  CFA_ISIS_PID                      0x0023
#define  CFA_NLPID_ISIS                      0x83
#define  CFA_NLPID_ESIS                      0x82
#define  CFA_NLPID_CLNP                      0x81

/* Port types */
#define  CFA_PORT_TYPE_NONE              0
#define  CFA_PORT_TYPE_UPLINK            1
#define  CFA_PORT_TYPE_DOWNLINK          2

#define  CFA_PORT_STATE_TRUSTED          1
#define  CFA_PORT_STATE_UNTRUSTED        0
/* for Enet ioctl and config commands/opCodes */
#define  CFA_ENET_ADD_MCAST              2
#define  CFA_ENET_DEL_MCAST              3
#define  CFA_ADD_ENET                    2
#define  CFA_DEL_ENET                    4
#define  CFA_ENET_ADD_ALL_MCAST          5
#define  CFA_ENET_DEL_ALL_MCAST          6


/*rport macro added for interface status configuration*/
#ifdef LNXIP4_WANTED
#define CFA_ENET_LNXIP_INT_CFA_UP        7
#define CFA_ENET_LNXIP_INT_CFA_DOWN      8
#endif



/* direction of packet flow */
#define   CFA_INCOMING                0                
#define   CFA_OUTGOING                1                

#define CFA_CLASS_NONE                   0
#define CFA_CBR                          1
#define CFA_UBR                          2
#define CFA_ABR                          3
#define CFA_VBR                          4
#define CFA_ANYCLASS                     5

/* WAN connection types */
#define   CFA_WAN_CONN_TYPE_OTHER        0

#define CFA_WAN_TYPE_ENET_DYNAMIC   7
#define CFA_WAN_TYPE_L2TP           10


/* Value for Interface Flow Control Status */
#define  ENET_FLOW_UNKNOWN               0 
#define  ENET_FLOW_ON                    1
#define  ENET_FLOW_OFF                   2

/* Value for Interface Auto Negotiation Support */
#define  ENET_AUTO_SUPPORT                1
#define  ENET_AUTO_NO_SUPPORT             2

/* Value for Interface Auto Negotiation Status */
#define  ENET_AUTO_ENABLE                 1
#define  ENET_AUTO_DISABLE                2

/* Values for Interface Duplexity Status */
#define ETH_STATS_DUP_ST_UNKNOWN          1
#define ETH_STATS_DUP_ST_HALFDUP          2
#define ETH_STATS_DUP_ST_FULLDUP          3

/* IP MTU over Enet - only for Ethernet v2 encap type */
#define   CFA_100KB             100000
#define   CFA_1MB               1000000
#define   CFA_ENET_SPEED_10M    10000000
#define   CFA_ENET_SPEED_100M   100000000
#define   CFA_ENET_SPEED_1G     1000000000
#define   CFA_ENET_SPEED_2500M  2500000000UL

#define   CFA_ENET_MAX_SPEED_VALUE     0xffffffff
#define   CFA_ENET_HISPEED_10G 10000 /* High Speed is an estimate of the 
                                        interface's current bandwidth 
                                        in units of 1,000,000 
                                        bits per second */
#define   CFA_ENET_HISPEED_40G 40000
#define   CFA_ENET_HISPEED_56G 56000
#define   CFA_ENET_HISPEED_25G  25000
#define   CFA_ENET_HISPEED_100G 100000

/* Default speed (Linux) - 100M */ 
#define   CFA_ENET_SPEED        CFA_ENET_SPEED_100M

/* Initialize to 2 Gbps on the assumption that a link aggregation will have
 * atleast 2 physical links. This is the value that will be used by RSTP/MSTP 
 * to calculate pathcost. Any further changes in speed for the port-channel 
 * will not be reflected in the pathcost */
#define   CFA_LAGG_SPEED        (2 * CFA_ENET_SPEED_1G)
/* Default MTU value */
#define   CFA_ENET_MTU            ISS_CUST_DEFAULT_MTU_SIZE
/* IP Address Allocation methods */
#define   CFA_IP_ALLOC_MAN               1
#define   CFA_IP_ALLOC_NEGO              2
#define   CFA_IP_ALLOC_POOL              3
#define   CFA_IP_ALLOC_NONE              4


#define CFA_BOOTP_ATTEMPT_INITIATE       (1)
#define CFA_BOOTP_ATTEMPT_FAILURE        (0)

#define CFA_MAX_VLAN_ID              VLAN_DEV_MAX_VLAN_ID

#define CFA_MIN_CUSTOMER_VLAN_ID     1
#define CFA_MAX_CUSTOMER_VLAN_ID     4094

#define CFA_MGMT_VLAN_LIST_SIZE  ((CFA_MAX_VLAN_ID + 31)/32 * 4)
#define CFA_SEC_VLAN_LIST_SIZE  ((CFA_MAX_VLAN_ID + 31)/32 * 4)

#define CFA_MAX_SEC_VLAN_LIST_SIZE   512

#define CFA_MGMT_IF_INDEX   CFA_DEFAULT_ROUTER_VLAN_IFINDEX
#define CFA_STACK_VLAN_INTERFACE     "vlan"

#define IF_PREFIX_LEN           30


#define CFA_NP_KNOWN_UCAST_PKT       1
#define CFA_NP_UNKNOWN_UCAST_PKT     2
#define CFA_NP_BCAST_PKT             3
#define CFA_NP_MCAST_PKT             4

#define CFA_NP_UNTAGGED              5
#define CFA_NP_TAGGED                6

#define   CFA_DEFAULT_ROUTER_VLAN_IFINDEX \
                     CFA_MIN_IVR_IF_INDEX /* Default VLAN */
    
/* OOB Interface Index */
#define   CFA_OOB_MGMT_IFINDEX   CFA_DEFAULT_ROUTER_VLAN_IFINDEX
/*This macro is used as a command to set OOB ifname 
  through ioctl*/                                      
#define   CFA_OOB_SET_IFNAME   1
#define   CFA_INVALID_IFINDEX  (-1)
                                 

#ifdef WGS_WANTED
#define   CFA_DEFAULT_MGMT_INTERFACE  "vlanMgmt"
#endif /* WGS_WANTED */

#define   CFA_ENET_IS_TYPE(u2Type)   (u2Type > 1500)

/* Enet encap sizes */
#define   CFA_ENET_V2_HEADER_SIZE            14
#define   CFA_ENET_SNAP_HEADER_SIZE          22
#define   CFA_ENET_LLC_HEADER_SIZE           17
/* 22 - 5 bytes SNAP */

/* LLC SAP Type constants */
#define   CFA_LLC_SNAP_SAP                 0xAA
#define   CFA_LLC_ROUTED_SAP               0xFE
#define   CFA_LLC_STAP_SAP                 0x42
#define   CFA_LLC_CONTROL_UI               0x03

/* type of link layer frame - same as FutureIP/RIP defines  */
#define   CFA_LINK_UCAST                 1
#define   CFA_LINK_MCAST                 2
#define   CFA_LINK_BCAST                 3

/* OSPF Protocol type field in packet */
#define   OSPF_PROTO_BYTE                11

/* EtherTypes for EthernetV2 Protocol type field - from RFC 1700 */
#define   CFA_ENET_IPV4                  0x0800
#define   CFA_ENET_ARP                   0x0806
#define   CFA_ENET_OSPF                  0x5900
#define   CFA_ENET_RARP                  0x8035
#define   CFA_ENET_SLOW_PROTOCOL         0x8809
#define   CFA_PPPOE_DISCOVERY            0x8863
#define   CFA_PPPOE_SESSION              0x8864
#define   CFA_PPPOE_IP_PROTO             0x0021
#define   CFA_MPLS_ROUTER_ALERT_ID       0x01
#define   CFA_IPV4_PROTO_L2TPV3          0x73
   
#define CFA_ETHER_RATE_CONTROL_OFF      1
#define CFA_ETHER_RATE_CONTROL_ON       2
#define CFA_ETHER_RATE_CONTROL_UNKNOWN  3

#define CFA_VLAN_PROTOCOL_ID             0x8100
#define CFA_VLAN_TAG_OFFSET              12
#define CFA_VLAN_PROTOCOL_SIZE           2
#define CFA_VLAN_TAGGED_HEADER_SIZE 16
#define CFA_ENET_TYPE_OR_LEN             2
#define   CFA_ENET_MIN_UNTAGGED_FRAME_SIZE   60
#define   CFA_ENET_MIN_TAGGED_FRAME_SIZE     64

/* RouteInfo & L2Info Status update command Definitions */
#define   CFA_HCT_UPDATE                 5
#define   CFA_HCT_DELETE                 6

/* network interface config commands */
#define   CFA_NET_IF_NEW                  1
#define   CFA_NET_IF_DEL                  3

#define  CFA_IP_ACQUIRED              1
#define  CFA_IP_RELEASED              2
#define  CFA_MEMBER_LINK_UP           3
#define  CFA_MEMBER_LINK_DOWN         4

#define   CFA_VRRP_VRID_OFFSET  CFA_IP_HDR_LEN + CFA_ENET_V2_HEADER_SIZE + 1

#define   CFA_VRRP_IPV6_VRID_OFFSET (CFA_IPV6_HDR_LEN + CFA_ENET_V2_HEADER_SIZE + 1)

/* Flag to identify whether the system has routing enabled over oob interface
 * The value of this flag is set to TRUE, if the system routing enabled 
 * over oob/linux vlan interface, set to FALSE, otherwise. */
#define   CFA_OOB_MGMT_INTF_ROUTING        gu4MgmtIntfRouting 
#define   CFA_LINUX_L3IPVLAN_NAME_PREFIX   "eth"
#define   CFA_LINUX_L3LAGG_NAME_PREFIX   "po"
#define   CFA_LINUX_OOB_PORT_NAME   "eth0"
#define   CFA_SECONDARY_OOB_PREFIX_NAME ":2"
/*Opaque Atrributes related constants*/
#define   CFA_MAX_OPAQUE_ATTRS             4
#define   CFA_MAX_INTF_DESC_LEN        128
          
/*Max value of the system specific portID allowed*/
#define CFA_IF_MAX_SYS_SPECIFIC_PORTID     16384

#define CFA_WAN_TYPE_OTHER                             0
#define CFA_WAN_TYPE_PRIVATE                           1
#define CFA_WAN_TYPE_PUBLIC                            2
/* flags for the FSAP2 trace call - applicable only to CFA modules */
#define   CFA_TRC_NONE              (0x00000000) 
#define   CFA_TRC_ALL               (0xffffffff) 
#define   CFA_TRC_ALL_TRACK         (0x20000000) 
#define   CFA_TRC_ALL_PKT_DUMP      (0x0f000000) 
#define   CFA_TRC_ENET_PKT_DUMP     (0x02000000) 
#define   CFA_TRC_IP_PKT_DUMP       (0x04000000) 
#define   CFA_TRC_ARP_PKT_DUMP      (0x08000000) 
#define   CFA_TRC_ATMARP_PKT_DUMP   (0x08000000) 
#define   CFA_TRC_AAL5_PKT_DUMP     (0x08000000) 
#define   CFA_TRC_ALL_MSG_DUMP      (0x00f00000) 
#define   CFA_TRC_ERROR             (0x10000000) 

#define CFA_TRC_TYPE_ALL      (CFA_TRC_NONE | CFA_TRC_ALL | \
                               CFA_TRC_ALL_TRACK | CFA_TRC_ALL_PKT_DUMP | \
                               CFA_TRC_ENET_PKT_DUMP | CFA_TRC_IP_PKT_DUMP | \
                               CFA_TRC_ARP_PKT_DUMP | \
                               CFA_TRC_ATMARP_PKT_DUMP | \
                               CFA_TRC_AAL5_PKT_DUMP | CFA_TRC_ALL_MSG_DUMP | \
                               CFA_TRC_ERROR | OS_RESOURCE_TRC | \
                               ALL_FAILURE_TRC | BUFFER_TRC)      

#define CFA_INVALID_CONTEXT                0xFFFF

/*Add for CFA_ENET CFA_LAG & CFA_PSEUDOWIRE check*/
#define CFA_IFINDEX_IS_PHY_LAG_PW_IFACE(u4IfIndex)  CfaCheckBrgPortType (u4IfIndex)

/* This will check whether the interface is ENET or LAGG or PW or AC */
#define CFA_IFINDEX_IS_VALID_IFACE(u4IfIndex) CfaCheckBrgPortType (u4IfIndex)

/* link up delay macro */
#define  CFA_LINKUP_DELAY_ENABLED 1
#define  CFA_LINKUP_DELAY_DISABLED 2
#define  CFA_LINKUP_DELAY_MIN_TIMER 1
#define  CFA_LINKUP_DELAY_MAX_TIMER 1000
#define  CFA_LINKUP_DELAY_DEFAULT_TIMER 2
#define  CFA_LINKUP_DELAY_STARTUP_TIMER 15

#define LINKUPDELAY_TMR_SET 1
#define LINKUPDELAY_TMR_RESET 2
#define CFA_LINKUP_DELAY_TIMER 1
#define CFA_LINKUP_STARTUP_TIMER 2

#define  CFA_DEFAULT_ENABLE 0
#define  CFA_DEFAULT_DISABLE 1


#ifdef HVPLS_WANTED
#define CFA_IF_APP_NONE 0x0
#define CFA_IF_APP_ERPS 0x10
#endif

/* Encapsulation types used by a tunnel. These values have
 * been extracted from IANAtunnelType. */
enum
{
    TNL_TYPE_OTHER = 1,        /* none of the following */
    TNL_TYPE_DIRECT,       /* no intermediate header */
    TNL_TYPE_GRE,          /* GRE encapsulation */
    TNL_TYPE_MINIMAL,      /* Minimal encapsultion */
    TNL_TYPE_L2TP,         /* L2TP enacapsulation */
    TNL_TYPE_PPTP,         /* PPTP encapsulation */
    TNL_TYPE_L2F,          /* L2F encapsulation */
    TNL_TYPE_UDP,          /* UDP encapsulation */
    TNL_TYPE_ATMP,         /* ATMP encapsulation */
    TNL_TYPE_MSDP,         /* MSDP encapsulation */
    TNL_TYPE_SIXTOFOUR,    /* 6to4 encapsulation */
    TNL_TYPE_SIXOVERFOUR,  /* 6over4 encapsulation */
    TNL_TYPE_ISATAP,       /* ISATAP encapsulation */
    TNL_TYPE_TEREDO,       /* Teredo encapsulation */
    TNL_TYPE_COMPAT,       /* IPv6 Auto-Compatible encapsulation */
    TNL_TYPE_IPV6IP,       /* IPv6 over IPv4 Configured encapsulation */
    TNL_TYPE_OPENFLOW      /* OFCL Hybrid Communication */
};

/* Value that represents the type of Internet Address -
* as per inet-address-mib */
enum
{
    ADDR_TYPE_UNKNOWN = 0,
    ADDR_TYPE_IPV4 = 1,
    ADDR_TYPE_IPV6 = 2
};

/* ************* IMPORTANT *************************
 * All the modules with ID less than PNAC will have*
 * untagged packets only. This should be taken care*
 * while adding modules.                           *
 ***************************************************/
typedef enum
{
    PACKET_HDLR_PROTO_RSTP_BRG  = 1,
    PACKET_HDLR_PROTO_PVRST_CISCO_ISL1 ,
    PACKET_HDLR_PROTO_PVRST_CISCO_ISL2 , 
    PACKET_HDLR_PROTO_PVRST_DOT1Q ,
    PACKET_HDLR_PROTO_LA_MARKER ,
    PACKET_HDLR_PROTO_LA_LACP ,
    PACKET_HDLR_PROTO_LLDP ,
    PACKET_HDLR_PROTO_EOAM ,
    PACKET_HDLR_PROTO_PNAC, 
    /* This is separate because it is a provider 
     * packet and wil be tagged. So it has to be after PNAC */
    PACKET_HDLR_PROTO_PROVIDER_STP,
    PACKET_HDLR_PROTO_VRP ,
    PACKET_HDLR_PROTO_MRP ,
    PACKET_HDLR_PROTO_PROVIDER_MVRP ,
    PACKET_HDLR_PROTO_SNOOP_CONTROL , 
    PACKET_HDLR_PROTO_MLD_QUERY , 
    PACKET_HDLR_PROTO_MLD_DONE , 
    PACKET_HDLR_PROTO_MLDV1_REPORT , 
    PACKET_HDLR_PROTO_MLDV2_REPORT , 
    PACKET_HDLR_PROTO_IPV4 ,
    PACKET_HDLR_PROTO_IPV6 ,
    PACKET_HDLR_PROTO_ARP ,
    PACKET_HDLR_PROTO_RARP ,
    PACKET_HDLR_PROTO_DOT11,
    PACKET_HDLR_PROTO_MAX
} tPktHdlrModuleId;
/* Node Status */
typedef UINT4 tCfaNodeStatus;

#define CFA_NODE_IDLE                             RM_INIT
#define CFA_NODE_ACTIVE                           RM_ACTIVE
#define CFA_NODE_STANDBY                          RM_STANDBY

/* Default for Pause Admin Mode,4 implies both send and receive on */
#define CFA_DEF_PAUSE_ADMIN_MODE                  4


typedef struct
{
    union
    {
        UINT4 u4Ip4Addr;
        UINT1 au1Ip6Addr[16];
    } uIpAddr;
#define Ip4TnlAddr   uIpAddr.u4Ip4Addr
#define Ip6TnlAddr   uIpAddr.au1Ip6Addr
}tTnlIpAddr;
typedef struct
{
    UINT4 u4AddrType;
    tTnlIpAddr LocalAddr;
    tTnlIpAddr RemoteAddr;
    UINT4 u4EncapsMethod;
    UINT4 u4ConfigId;
    UINT4 u4ContextId; /* Virtual context Id */
    UINT4 u4Mtu;
    UINT4 u4EncapLmt;
    UINT4 u4PhyIfIndex;
    UINT4 u4IfIndex;
    INT4  i4FlowLabel;
    INT4 i4PathMtuSock;
    UINT2 u2HopLimit;
    INT2  i2TOS;
    UINT1 au1IfAlias[CFA_MAX_PORT_NAME_LENGTH];
    UINT1 u1Security;
#define TNL_SECURITY_NONE  1
#define TNL_SECURITY_IPSEC 2
#define TNL_SECURITY_OTHER 3

    UINT1 u1DirFlag;
#define TNL_UNIDIRECTIONAL 1
#define TNL_BIDIRECTIONAL  2

    UINT1 u1Direction;
#define TNL_INCOMING 1
#define TNL_OUTGOING 2

    UINT1 u1EncapOption;
#define TNL_ENCAP_ENABLE  1
#define TNL_ENCAP_DISABLE 2

    UINT1 u1RowStatus;
    UINT1 bCksumFlag;
#define TNL_CKSUM_ENABLE  1
#define TNL_CKSUM_DISABLE 2

    UINT1 bPmtuFlag;
#define TNL_PMTU_ENABLE  1
#define TNL_PMTU_DISABLE 2
    UINT1 u1Reserved;
}tTnlIfEntry;

typedef struct
{
    UINT4               u4IfType;
    UINT1               au1IfName[IF_PREFIX_LEN];
    UINT1               au1Reserved[4 - (IF_PREFIX_LEN % 4)];
}
tCfaIfaceInfo;

typedef struct {
    tTMO_SLL_NODE  NextEntry;
    UINT4          u4IfIndex;
} tCfaILanPortStruct;

extern tCfaIfaceInfo asCfaIfaces[][CLI_MAX_IFSUBTYPES];
extern INT4     IfMainBrgPortTypeSet(tSnmpIndex *, tRetVal *);
                    
/*  Ip Interface Node */
typedef struct {
    tRBNodeEmbd       RBNode;             /* Embeded RBTree node */
    tTMO_SLL          SecondaryIpList;    /* List of secondaty IP address
                                             over the interface */
    UINT1             u1SubnetMask;          /* NetMask associated with primary 
                                                IP address */
    UINT1             u1AddrAllocMethod;  /* Address allocation method for
                                             the interface */
    UINT1             u1AddrAllocProto;   /* Dynamic protocol being used 
                                             for IP acquistion */
    UINT1             u1IpForwardEnable;  /* Forwarding status of the 
                                             interface */
    UINT1             u1AddrType;          /*Type of the IP address*/
    UINT1             u1IpStatus;         /*Status of the IP address*/
    UINT1             u1AddrRowStatus;    /*New address table rowstatus*/
    UINT1             u1IpStorageType;    /*Storage type of IP address*/
    UINT1             u1SecAddrFlag;      /*Secondary address flag */
    UINT1             u1PrefixRowStatus;  /*Prefix table rowstatus */
    UINT2             u2IpPortNum;        /* IP port no */
    UINT4             u4IpCreated;        /*Time at which IP add is created*/ 
    UINT4             u4IfIndex;          /* CFA interface index */
    UINT4             u4IpAddr;           /* Primary IP Address */
    UINT4             u4BcastAddr;        /* Broadcast address associated 
                                             with primary IP Address*/
    UINT4             u4IpLastChanged;    /*Latest time at which IP add is changed*/
    UINT1             u1Origin;           /*Origin of the IP address*/
    UINT1             u1IpCount;          /* Secondary IP address count counting
                                             only manually added IP's */
    UINT2             u2PortVlanId;       /*Holds the VLAN Identifier used for Routerport Implementation */
} tIpIfRecord;

/* Macros for classification of IP addresses */
#define   CFA_IS_ADDR_CLASS_A(u4Addr)   ((u4Addr & 0x80000000) == 0)          
#define   CFA_IS_ADDR_CLASS_B(u4Addr)   ((u4Addr & 0xc0000000) == 0x80000000) 
#define   CFA_IS_ADDR_CLASS_C(u4Addr)   ((u4Addr & 0xe0000000) == 0xc0000000) 
#define   CFA_IS_ADDR_CLASS_D(u4Addr)   ((u4Addr & 0xf0000000) == 0xe0000000) 
#define   CFA_IS_ADDR_CLASS_E(u4Addr)   ((u4Addr & 0xf0000000) == 0xf0000000) 

/*Restriction  for 127.0.0.1 loopback address */
#define   CFA_IS_LOOPBACK(u4Addr)       (u4Addr == 0x7f000001)

/* Macro for Loopback IP Range 127.0.0.0 to 127.255.255.255 */
#define CFA_IS_LOOPBACK_RANGE(u4Addr) (u4Addr >=  0x7f000000) && (u4Addr <=  0x7fffffff)

#define CFA_IPIFTBL_LOCK              CfaIpIfTblLock
#define CFA_IPIFTBL_UNLOCK            CfaIpIfTblUnlock
/* Global inforamtion associated with a IP interface */
typedef struct
{
    tRBTree          pIpIfTable; /* RB Tree containing all the IP 
                                    interfaces */
                                    
    tMemPoolId       IpIntfPoolId; /* Mempool for IP interface record */
    tMemPoolId       SecondaryIpPool; /* MemPool for secondary address */
    tOsixSemId       SemId;           /* Semaphore for protecting IP interface
                                         table */
}tIpIfGlobalInfo;

extern tIpIfGlobalInfo  gIpIfInfo;

typedef struct {
    UINT4  u4Mtu;
    UINT4  u4Pcr;
    UINT4  u4Scr;
    UINT4  u4Mbs;
    UINT1  u1EncapType;
    UINT1  u1TraffType;
    UINT1  u1AtmQosCls;
    UINT1  u1Direction;
    UINT1  u1ConnType;
    UINT1  u1Reserved;  /* Added for packaging */
    UINT2  u2Reserved;  /* Added for packaging */
} tAtmVcConfigStruct;

/* Interface Layering Info structure */
typedef struct {
   UINT4    u4IfIndex;
   UINT1    u1IfType;
   UINT1    u1Command; /* add, delete or ignore */
   UINT1    u1EncapsType;
   UINT1    u1VcType;
} tIfLayerInfo;

/* The Module Data Structure used in CFA */
typedef struct {
    UINT1  u1Command;
    UINT1  u1EncapType;
    UINT1  au1NextHopMediaAddr[CFA_ENET_ADDR_LEN];
    UINT1  u1PktType;
#ifdef MBSM_WANTED
    UINT1  u1SlotCount; /* Used by MBSM for Slot count in a message */
#else
    UINT1  u1Reserved;
#endif

    /* 2 Bytes in the reserved field is used as for RFC-6374*/
    UINT1  au1Reserved[2];
    UINT4  u4NextHopIpAddr;
    /* used by QoS on outgoing side - between FFM & IWF */

    UINT4  u4Protocol;
    /* used by QoS on outgoing side - between FFM & IWF */
    UINT4  u4IfIndex;

} tCfaModuleData;

typedef struct {
    UINT1  u1LinkUpDownTrapEnable; /* To enable/disable Traps when 
                                    * interface status changed */
    
    UINT1  u1PromiscuousMode;      /* To set/reset interface in Promiscous
                                    * mode */  
    
    UINT2  u2Reserved;             /* for structure alignment */ 
    
    UINT4  u4LastChange;           /* System up time since interface 
                                      status changed, specified in ipif.mib */
    
    UINT4  u4Speed;                /* To store interface current bandwidth 
                                    * specified in ipif.mib */
    
    UINT4  u4HighSpeed;            /* To store the speed value > 4,294,967,29  
                                    * specified in ipif.mib */

    UINT1  *pu1Alias;              /* To store interface name */
  
    /* ifDescr and ifAlias are all pointer to strings. 
    * Currently ifName is same as ifAlias
    */
    UINT1  au1Descr[CFA_MAX_DESCR_LENGTH];
} tIfGeneralInfoStruct;

/* statistics' structure */
typedef struct {
    UINT4  u4InOctets;
    UINT4  u4HighInOctets;     /* used for showing the High Octet counter */
    UINT4  u4InUcastPkts;
    UINT4  u4InDiscards;
    UINT4  u4InErrors;
    UINT4  u4InUnknownProtos;
    UINT4  u4OutOctets;
    UINT4  u4HighOutOctets;    /* used for showing the High Octet counter */
    UINT4  u4OutUcastPkts;
    UINT4  u4OutDiscards;
    UINT4  u4OutErrors;

   /* the sum of Mcast and Bcast counters give the NUcast counters */
    UINT4  u4InMulticastPkts;
    UINT4  u4InBroadcastPkts;
    UINT4  u4OutMulticastPkts;
    UINT4  u4OutBroadcastPkts;
} tIfCountersStruct;

/* Async params structure */
typedef struct {
    UINT1  u1TxFcsSize;
    UINT1  u1RxFcsSize;
    UINT2  u2Reserved;
    UINT1  *pTxACCMap;
    UINT1  *pRxACCMap;
} tLlAsyncParams;


/* the global structure which forms the GDD registration Table */
typedef struct {
    UINT1  u1DevType;
    /* device type */

    UINT1  u1PortNum;
    /* port number of the device - not used currently -
     * only for interrupt mode */

   UINT1 u1PhysIfRegValidity;
   /* indicates whether the port is registered or not */

   UINT1 u1HigherIfType;
   /* ifType of the layer which is registered to run over the port */

   INT4  i4PortDescriptor;
   /* handle for accessing the port */

   UINT1 au1PortName [CFA_MAX_PORT_NAME_LENGTH];
   /* device name of the port */

   INT4  (*fn_open)(UINT4 u4IfIndex);
   INT4  (*fn_close)(UINT4 u4IfIndex);
   INT4  (*fn_read)(UINT1 *pBuf,UINT4 u4IfIndex, UINT4 *pu4PktSize);
   INT4  (*fn_write)(UINT1 *pBuf,UINT4 u4IfIndex, UINT4 u4PktSize);
   INT4  (*fn_hl_rx)(tCRU_BUF_CHAIN_HEADER *pBuf,UINT4 u4IfIndex, 
                     UINT4 u4PktSize, UINT2 u2Protocol, UINT1 u1EncapType);
   INT4 (*fn_pm_write)(UINT1 *pBuf,UINT4 u4IfIndex,UINT4 u4PktSize,
         UINT1 u1Priority);
   tLlAsyncParams AsyncParams;
   UINT1 u1Flag;
   UINT1 au1Reserved[3];
} tGddRegStruct;

/*Bcp Configuration structure */
typedef struct {
  UINT4 u4Index;
  UINT4 u4Mtu;
  UINT4 u4IdleTimeout;
  UINT1 u1LocalMediaAddr[CFA_ENET_ADDR_LEN];
  UINT1 u1PeerMediaAddr[CFA_ENET_ADDR_LEN];
  UINT1 Stp;
  UINT1 u1MgmtInlineSupport; 
  UINT1 u1IEEE802TagFrameSupport;
  UINT1 u1BridgeLineId;
  UINT1 u1TinygramFlag;
  UINT1 u1Alignment;
  UINT2 u2Reserved; 
} tBcpConfigStruct;  


/* Wan interfaces Config Structure */
typedef struct {
    UINT1  u1ConnType;
    UINT1  u1CompressionEnable;
    UINT1  u1Persistence;
    UINT1  u1DemandCktStatus;
    UINT1  au1PeerAddress[CFA_MAX_MEDIA_ADDR_LEN];
    UINT4  u4QosIndex;
    UINT4  u4Vpi;
    UINT4  u4CktId;
    UINT4  u4SustainedSpeed;
    UINT4  u4PeakSpeed;
    UINT4  u4MaxBurstSize;
    UINT4  u4IdleTimeout;
    UINT4  u4LastTimeoutCounters;
    UINT4  u4ConfigPeerIpAddr;
    UINT4  u4NegoPeerIpAddr;
    UINT4  u4ConfigMtu;
    INT4   i4AtmVcDesc;                             
    UINT1  u1TraffType;
    UINT1  u1Direction;
    UINT2  u2Reserved;  /* Added for packaging */
    tTMO_SLL  DemandCktBufQ;
} tWanConfigStruct;

typedef UINT2 tVlanIfaceVlanId;
typedef UINT1 tMgmtVlanList [CFA_MGMT_VLAN_LIST_SIZE];
typedef UINT1 tSecVlanList  [CFA_SEC_VLAN_LIST_SIZE];
/* Structure for maintainig the IVR information. Bridging can be enabled on a 
 * per interface basis. VLAN ID will be maintained in CFA for VLAN interfaces 
 * alone.
 */

/* Tunnel If Table */
typedef struct
{
    tTMO_SLL_NODE Next;
    UINT4 u4AddrType;
    tTnlIpAddr LocalAddr;
    tTnlIpAddr RemoteAddr;
    UINT4 u4EncapsMethod;
    UINT4 u4ConfigId;
    UINT4 u4ContextId; /* Virtual context Id */
    UINT4 u4Mtu;
    UINT4 u4EncapLmt;
    UINT4 u4PhyIfIndex;
    UINT4 u4IfIndex;
    INT4  i4FlowLabel;
    INT4 i4PathMtuSock;
    UINT2 u2HopLimit;
    INT2  i2TOS;
    UINT1 au1IfAlias[CFA_MAX_PORT_NAME_LENGTH];
    UINT1 u1Security;
    UINT1 u1DirFlag;
    UINT1 u1Direction;
    UINT1 u1EncapOption;
    UINT1 u1RowStatus;
    UINT1 bCksumFlag;
    UINT1 bPmtuFlag;
    UINT1 u1Reserved;
}tTnlIfNode;

typedef struct{
    UINT4                 au4OpqAttrValue[CFA_MAX_OPAQUE_ATTRS];
    UINT1                 au1OpqAttrStatus[CFA_MAX_OPAQUE_ATTRS];
    tTMO_SLL              IfTlvTable;
}tCustIfInfo;

typedef struct {
    UINT4  u4IpAddr;
    UINT4  u4BcastAddr;
    UINT4  u4DnsPrimaryIpAddr;
    UINT4  u4DnsSecondaryIpAddr;
    UINT2  u2UnnumberedIndex;
    UINT2  u2IpPortNum;        /* handle into the IP's table */
    INT2   i2DefRouteMetric;
    UINT1  u1SubnetMask;
    UINT1  u1IpForwardEnable;
} tIpConfigStruct;



/* main ifTable struct */
typedef struct {
    CFA_GAPIF_DB_ENTRY;
    tIfGeneralInfoStruct  IfGeneralInfo;
    tTMO_SLL              IfStackLowerLayer;
    tTMO_SLL              IfStackHigherLayer;
    tTMO_SLL              IfRcvAddressTable;
    tIpConfigStruct       *pIpEntry;
    tGddRegStruct         *pGddEntry;
    tWanConfigStruct      *pWanEntry;
    tCustIfInfo           CustIfInfo;
    VOID                  *pIwfStruct;
    tTnlIfNode            *pTnlIfEntry;
    UINT4                 u4RegProtMask;
    UINT4                 u4HighIfIndex;
    INT4                  i4IfMainStorageType;
    UINT1                 u1AdminStatus;
    UINT1                 u1RowStatus;
    UINT1                 u1EncapType;
    UINT1                 u1IsBackPlaneIntf;
} tIfInfoStruct;

typedef struct {
    tTMO_SLL_NODE NextNode; 
    UINT4 u4TlvType;
    UINT4 u4TlvLength;
    UINT1 *pu1TlvVlaue;
    INT4  i4TlvRowStatus;
}tIfTlvInfoStruct;

#ifndef PACK_REQUIRED
#pragma pack(1)
#endif
/* ethernet V2 Header Format */
typedef struct {
    UINT1  au1DstAddr[CFA_ENET_ADDR_LEN];
    UINT1  au1SrcAddr[CFA_ENET_ADDR_LEN];
    UINT2  u2LenOrType;
} tEnetV2Header;


/* complete ethernet SNAP Header Format */
typedef struct {
    UINT1  au1DstAddr[CFA_ENET_ADDR_LEN];
    UINT1  au1SrcAddr[CFA_ENET_ADDR_LEN];
    UINT2  u2LenOrType;
    UINT1  u1DstLSap;
    UINT1  u1SrcLSap;
    UINT1  u1Control;
    UINT1  u1Oui1;
    UINT1  u1Oui2;
    UINT1  u1Oui3;
    UINT2  u2ProtocolType;
} tEnetSnapHeader;
#ifndef PACK_REQUIRED
#pragma pack ()
#endif
/* complete ethernet LLC Header Format - for BPDU */
typedef struct {
    UINT1  au1DstAddr[CFA_ENET_ADDR_LEN];
    UINT1  au1SrcAddr[CFA_ENET_ADDR_LEN];
    UINT2  u2LenOrType;
    UINT1  u1DstLSap;
    UINT1  u1SrcLSap;
    UINT1  u1Control;
    UINT1  au1Pad[3];
} tEnetLlcHeader;


typedef struct {
    UINT1  u1Flags;
    UINT1  u1MacType;
    UINT2  u2Reserved;
} tBridgeHeader;



/* The Enet IWF struct.
When supporting both enetV2 and Enet-SNAP encaps, three pre-formed headers
would be maintained in this struct. We use the u2LenOrType to indicate only
type. */

typedef struct {
    tEnetV2Header    EnetV2HeaderIp;
    tEnetSnapHeader  EnetSnapHeaderIp;    
    tEnetV2Header    EnetV2HeaderArp;
    tEnetSnapHeader  EnetSnapHeaderArp;   
    tEnetV2Header    EnetV2HeaderRarp;
    tEnetSnapHeader  EnetSnapHeaderRarp;
#ifdef IP6_WANTED
   tEnetV2Header     EnetV2HeaderIp6;
#endif


   tEnetLlcHeader    EnetLlcHeaderBpdu;

#ifdef ISIS_WANTED
   tEnetLlcHeader    EnetLlcHeaderOsi;
#endif
    UINT1            au1LocalMacAddr[CFA_ENET_ADDR_LEN];
    UINT2            u2Reserved;
} tEnetIwfStruct;

/* EOAM information maintained in CFA */
typedef struct
{
    UINT1 u1EoamStatus;     /* indicates whether EOAM is enabled/disabled */
    UINT1 u1MuxState;       /* Multiplexer action - FWD/DISCARD */
    UINT1 u1ParState;       /* Parser action - FWD/DISCARD/LOOPBACK */
    UINT1 u1UniDirSupp;     /* To indicate the unidirectional Tx capability */
    UINT1 u1LinkFault;      /* denotes that link fault has occured*/
    UINT1 u1RemoteLB;       /* denotes whether Loopback has been enabled at 
                               remote end */
    UINT1 au1Reserved[2];
}tCfaEoamInfo;

typedef struct
{
  INT4   i4Status;           /* ifMauStatus */
  UINT4  u4DefType;          /* IfMauDefaultType */
  INT4   i4ANStatus;         /* IfMauAutoNegAdminStatus */
  INT4   i4ANRestart;        /* ifMauAutoNegRestart */
  INT4   i4ANCapAdvtBits;    /* ifMauAutoNegCapAdvertisedBits */
  INT4   i4ANRemFltAdvt;     /* ifMauAutoNegRemoteFaultAdvertised */
} tIfMauStruct;


/* tIfMauStruct is defined in union because tIfMauStruct is used only for 
 * Physical and lagg interfaces. Any 4 Byte Variable which needs to be 
 * introduced in the future for interfaces other than physical and lagg 
 * can be included in this union*/

typedef union 
{
    tIfMauStruct       *pIfMauInfo;
}tIfMauEntry;


/* Structure to  populate Link Up Delay configuration*/
typedef struct LinkUpTimer {
     tTmrBlk             TmrBlk;
     UINT1               u1TmrStatus;
     UINT1               au1Pad[3];
} tLinkUpTimer;

typedef struct TimerDesc {
    tTmrDesc              aTmrDesc[10];
} tCfaTimerDesc;
typedef struct{
    tLinkUpTimer  PeriodicTmrNode;
    UINT4         u4ConfLinkUpTime;
    UINT4         u4LinkUpStartTime;
    UINT4         u4LinkUpRemainingTime;
    UINT1         u1LinkUpDelayStatus;
    UINT1         au1Pad[3];
}tCfaLinkUpDelay;

typedef struct {
    CFA_IF_DB_ENTRY;          /* Macro which can be used in structures where  
                                 the memory block can be part of two different 
                                 databases. */ 
    tCfaEoamInfo          EoamParams;
    tIfCountersStruct     IfCounters;
    tCfaLinkUpDelay       IfLinkUpDelay;
    INT4                  i4Valid;
    INT4                  i4Active;
    INT4                  i4IsInternalPort;
    INT4                  i4IpPort;
#ifdef LNXIP4_WANTED
    UINT4                 u4IndexMgrPort;
#endif
    UINT4                 u4IfMtu;
    UINT4                 u4IfSpeed;
    UINT4                 u4IfHighSpeed;
    UINT4                 u4SysSpecificPortID;
    UINT4                 u4UnnumAssocIPIf;
 UINT4                 u4ACPort;/* Underlying physical interface for the logical
                                      attachment circuit interface */
    INT4                  i4IfMainStorageType;
    UINT4                 u4IpIntfStats;
    UINT4                 u4UfdGroupId; /* group id is 0
                                           if the port is not mapped with any UFD group*/
    UINT4                 u4UfdDownlinkDisabledCount;
    UINT4                 u4UfdDownlinkEnabledCount;
    UINT2                 u2OperMauType;
    UINT2                 u2AdvtCapability;
    UINT1                 au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1                 au1IfAliasName[CFA_MAX_IFALIAS_LENGTH];
    UINT1                 au1PortName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1                 au1Descr[CFA_MAX_DESCR_LENGTH]; 
    UINT1                 au1MacAddr[CFA_ENET_ADDR_LEN];
    UINT2                 u2VlanId;
    UINT1                 au1PeerMacAddr[CFA_ENET_ADDR_LEN];
    UINT1                 u1DuplexStatus;
    UINT1                 u1IfOperStatus;
    UINT1                 u1IfType;
    UINT1                 u1IfSubType;
    UINT1                 u1EthernetType;
    UINT1                 u1BridgedIface; 
    UINT1                 u1AddrAllocMeth;
    UINT1                 u1PauseAdminNode;
    UINT1                 u1IfAdminStatus;
    UINT1                 u1AutoNegStatus;
    UINT1                 u1BrgPortType;
    UINT1                 u1AutoNegSupport; 
    UINT1                 u1IfRowStatus; 
    UINT1                 u1PortSecState;   /* Port security state: 
                                               Trusted/Untrusted */
   UINT1                 au1Desc[CFA_MAX_INTF_DESC_LEN];
    UINT1                 u1WanType;
    UINT1                 u1NwType;
    UINT2                 u2ACVlan;/* Customer vlan to be configured on logical
                                      Attachment Circuit interface */
    tIfMauEntry           IfMauEntry; /* tIfMauEntry is used only for
                  * Physical and lagg interfaces. 
                  * Any 4 Byte Variable which needs to be
                  * introduced in the future for 
                  * interfaces other than physical and 
                  * lagg can be included in this union */
    tIp6Addr              IfIp6Addr;
    UINT1                 u1PrefixLen;
    UINT1                 u1Ipv4AddressChange;
    UINT1                 u1PortRole;       /* Port Role:
                                               Uplink/Downlink*/
    UINT1                 u1UfdOperStatus;  /* UFD Oper Status:
                                               Up, Down, UfdErrorDisabled*/
    UINT1                 u1DesigUplinkStatus;
    UINT1                 u1IsEncapSet;
    UINT1                 au1Pad[2];
 
}tCfaIfInfo;

 /* Structure for UFD */
typedef struct _tUfdGroupInfo {
    tRBNodeEmbd     GroupIdMapUfdNode;
    UINT1           au1UfdGroupName[CFA_UFD_GROUP_MAX_NAME_LEN];
    tPortList       UfdGroupUplinkPortList; 
    tPortList       UfdGroupDownlinkPortList;
    UINT4           u4UfdGroupId;
    UINT4           u4UfdUplinkCount;
    UINT4           u4UfdDownlinkCount;
    UINT4           u4UfdGroupDesigUplinkPort;
    UINT4           u4UfdRowStatus;
    UINT1           u1UfdGroupStatus; 
    UINT1           au1Pad[3];
}tUfdGroupInfo;



/* Strucuture to populate start up port
 * property values to be used by clear configuration*/

typedef struct{
    UINT4  u4IfSpeed;
    UINT4  u4IfHighSpeed;
    UINT4  u4DuplexStatus;
    UINT4  u4AutoNegStatus;
}tCfaDefaultPortParams;

/* 
 * This structure is used by DHCP/BOOTP Client to 
 * pass the interface specific parameters obtained from DHCP/BOOTP Server. 
 * Currently Ip Address and Mask are used. 
 */ 
typedef struct _tIpInterfaceParams { 
   UINT4 u4IpAddress; 
   UINT4 u4SubnetMask; 
   UINT4 u4DNSPrimaryIp;            /* Set to 0 if the DNS IP is not
                                       present */
   UINT4 u4DNSSecondaryIp;
   UINT4 u4Mtu;
   UINT4 u4StatusFlag;
                                       
} tIpInterfaceParams; 

/* structure used by protocol modules to create a new logical interface */
typedef struct{
    UINT4 u4IfIndex;
    UINT4 *pu4LowIfs; 
    /* List of lower IfIndices over which the new interface has 
     * to be stacked. */

    UINT4 u4LowIfCount; 
    /* No. of ifaces over which the sub-layer has to be stacked */

    UINT4 u4IfSpeed; 
    /* Interface speed */

    UINT4 u4IfMtu; 
    /* MTU of the interface */

    UINT4 u4IfType; 
    /* Type of the interface */

    UINT1 au1AliasName[CFA_MAX_PORT_NAME_LENGTH]; 
    /* Alias name of the interface to be created */

    UINT1 au1HwAddr[CFA_ENET_ADDR_LEN]; 
    /* MAC address of interface*/

    UINT2 u2Reserved;   
}tCfaIfCreateParams;

/* BufInfo to be sent to HL, when a pkt arrives at CFA */
typedef struct
{
    tCRU_BUF_CHAIN_HEADER *pBuf;
    UINT1  au1DstMac[CFA_ENET_ADDR_LEN];
    tEnetV2Header      EthHdr;
    UINT2                  VlanId;
    UINT1  u1PktType;
    UINT1  au1Reserved[1];
}tCfaPktInfo;

/* Tunnel interface infomation change to be given to higher layers */
typedef struct
{
    UINT4 u4LocalAddr;
    UINT4 u4RemoteAddr;
    UINT4 u4TnlType; 
    UINT4 u4TnlMask;
    UINT4 u4TnlDir;
    UINT4 u4TnlDirFlag;
    UINT4 u4TnlEncapOpt;
    UINT4 u4TnlEncapLmt;
    UINT4 u4TnlHopLimit;
#define TNL_DIR_FLAG_CHG        0x01
#define TNL_DIRECTION_CHG       0x02
#define TNL_ENCAP_OPT_CHG       0x04
#define TNL_ENCAP_LIMIT_CHG     0x08
#define TNL_ENCAP_ADDR_CHG      0x10
#define TNL_HOP_LIMIT_CHG       0x20
}tTnlIfInfo;

typedef struct
{
    UINT4 u4IfIndex;      /* Interface Index */
    union
    {
        tCfaIfInfo  CfaInterfaceInfo;
        /* Interface specific info to be sent to HL during
         * IfCreate, IfDelete & IfUpdate */

        tCfaPktInfo CfaBufInfo;
        /* BufInfo to be sent to HL, during pkt reception */

        tTnlIfInfo CfaTnlInfo;
        /* Tunnel interface information, valid only for tunnel interfaces */
    }uCfaRegParams;

#define CfaIntfInfo  uCfaRegParams.CfaInterfaceInfo
#define CfaPktInfo uCfaRegParams.CfaBufInfo
#define CfaTnlIfInfo uCfaRegParams.CfaTnlInfo
}tCfaRegInfo;

typedef struct _CfaRegHLFnPtr
{
    /* Interface create notification from CFA to HL */
    INT4 (*pIfCreate) (tCfaRegInfo *);

    /* Interface delete notification from CFA to HL */
    INT4 (*pIfDelete) (tCfaRegInfo *);

    /* Interface info update notification from CFA to HL */
    INT4 (*pIfUpdate) (tCfaRegInfo *);

    /* Tunnel Interface info update notification from CFA to HL */
    INT4 (*pTnlIfUpdate) (tCfaRegInfo *);

    /* Interface oper status chg notification from CFA to HL */
    INT4 (*pIfOperStChg) (tCfaRegInfo *);

    /* Deliver the pkt rcvd to HL from CFA - pBuf includes ethernet hdr */
    INT4 (*pIfRcvPkt) (tCfaRegInfo *);

    /* Protocol Type field incase of ethernet frame (OR) LLC SAPs:
    * Type/Length field - IP(0x0800), ARP(0x0806), RARP(0x8035), IPV6(0x86DD),
    * PPPoE-session-frame(0x8863), PPPoE-discovery-frame(0x8864) & MPLS(0x8847).
    * LLC SAPs - Bridge control(0x4242)/data frame(0xAAAA), IS-IS(0xFEFE) */
    UINT2 u2LenOrType;

    /* Registration mask */
    UINT2 u2RegMask;
   /* List of Registration masks supported */
#define CFA_IF_CREATE       0x01
#define CFA_IF_DELETE       0x02
#define CFA_IF_UPDATE       0x04
#define CFA_TNL_IF_UPDATE   0x08
#define CFA_IF_OPER_ST_CHG  0x10
#define CFA_IF_RCV_PKT      0x20
    /* Registration flag - set to CFA_REGISTER after registration */
    UINT1 u1RegFlag;

    /* 3 bytes padding added for 4-byte alignment */
    UINT1 au1Reserved[3];
}tCfaRegParams;

typedef struct
{
    UINT2  u2Len;
    UINT2  u2Id;
    UINT1  u1Df;
    UINT1  u1Ttl;
    UINT1  u1Tos;
    UINT1  u1Proto;
    UINT1  u1Olen;
    UINT1  au1Reserved[3];
}t_IP_PARMS;

typedef struct
{
    tCRU_BUF_CHAIN_HEADER *pBuf;  /* Buf to be Txed */
    t_IP_PARMS IpHdr;             /* IpHdr info to be filled in */
    tTnlIpAddr TnlSrcAddr;        /* Tunnel Src IP */
    tTnlIpAddr TnlDestAddr;       /* Tunnel Dest IP */
    UINT4 u4AddrType;             /* AddrType: ADDR_TYPE_V4/ADDR_TYPE_V6 */
    UINT4 u4IfIndex;              /* IfIndex of the output interface */
    UINT4 u4ContextId;            /* Virtual context Id */
    UINT2 u2Len;                  /* Payload Length */
    UINT2 u2Proto;                /* Encapsulated Protocol value */
}tTnlPktTxInfo;
/*Structure used to set OOB IfName*/
typedef struct {
    INT4    i4Cmd;
    UINT1   au1OobIfName[16]; /*Length of IfName is 16 in kernel*/
} tOobIfName;

typedef enum {
    CFA_INFO_TYPE_SYS_SPECIFIC_PORTID
} tCfaInfoType;

typedef struct {
    tCfaInfoType u4Type;
    UINT4  u4IfIndex;
}tCfaInfoInput;

typedef struct {
        UINT4  u4SysSpecificPortId;
}tCfaInfoOutput;

/* This structure is used by L2 Modules to fill the parameters necessary
 * for tx PDU over backplane interfaces
 * */

typedef struct {
    UINT4 u4ContextId;
}tCfaBackPlaneParams;
/* This Structure is used to hold the information of allowed ports
 * for each Slot at NP Level
 * If MBSM is not defined all parameters will be set to default value.*/
typedef struct _HwPortInfo
{
   UINT4    u4StartIfIndex;    /* Starting If-Index for the Slot */
   UINT4    u4EndIfIndex;      /* End If-Index for the Slot */
   UINT4    au4ConnectingPortIfIndex[SYS_DEF_MAX_INFRA_SYS_PORT_COUNT];
                               /* Hardware number of Ports used to interconnect
                                 2 Boards in Dual Unit Stacking Model */
   UINT4    au4ConnectingPort[SYS_DEF_MAX_INFRA_SYS_PORT_COUNT];  
                               /* Hardware number of Ports used to interconnect
                                 2 Boards in Dual Unit Stacking Model */
   UINT4    au4ConnectingUnit[SYS_DEF_MAX_INFRA_SYS_PORT_COUNT];
                                /* Unit ID of connecting Ports */
   UINT4    u4NumConnectingPorts; /* Total Connecting Ports */
   INT4     i4MaxPortSpeed;      /*the default speed for port*/
   UINT4    u4OutPort;
   UINT1    au1Mac[MAC_ADDR_LEN];
   UINT2    u2OutVlanId;
   UINT1    u1MsgType;          /*message type for api to take necessary action*/
   UINT1    au1Reserved[3];     
}tHwPortInfo;

#if defined (WLC_WANTED) || defined (WTP_WANTED) 

#define WSS_CFA_TX_DOT11_PKT 200

typedef struct {
    UINT4       u4IfIndex;
    UINT1       u1IfType;
    UINT1       u1AdminStatus;
    UINT1       u1OperStatus;
    /* Mac address to be set for the WAP interface */
    tMacAddr    CfaWssMacAddr;
    UINT1       au1Reserved[3];
}tCfaCreateIfIndex;

typedef struct {
    tCRU_BUF_CHAIN_DESC     *pTxPktBuf;
    UINT4                   u4IfIndex;
    UINT2                   u2VlanId;
    UINT1                   au1Reserved[2];
}tCfaDot11PktBuf;

typedef struct {
    union {
        tCfaCreateIfIndex   CfaCreateIfIndex;   
        tCfaDot11PktBuf     CfaDot11PktBuf; 
    }unCfaMsg;
}tCfaMsgStruct;

enum {
 CFA_WSS_CREATE_RADIO_INTF_MSG,
 CFA_WSS_CREATE_IFINDEX_MSG,
 CFA_WSS_OPER_STATUS_CHG_MSG,
 CFA_WSS_ADMIN_STATUS_CHG_MSG,
 CFA_WSS_DELETE_IFINDEX_MSG,
 CFA_WSS_TX_DOT11_PKT,
 CFA_WSS_TX_DOT3_PKT,
 CFA_WSS_SET_MAC_ADDR,
 CFA_WSS_WTP_INIT
};
#endif
/**********************   EXTERNS  ******************************/
INT4 CfaGetIfIpPort PROTO ((UINT4 u4IfIndex));
UINT4 CfaGetIfInterface PROTO ((UINT4 u4Port));
VOID CfaGetSecurityBridgeMode PROTO ((UINT4 *pu4SecMode));
UINT4 CfaGetSecIvrIndex PROTO((VOID));
INT4 CfaGetIfType PROTO ((UINT4 u4IfIndex, UINT1 *pu1IfType));
INT4 CfaGetIfBridgedIfaceStatus (UINT4 u4IfIndex, UINT1 *pu1Status);
INT4 CfaGetHLIfFromLLIf PROTO ((UINT4 u4LLIf, UINT4 *pu4HLIf, UINT1 *pu1IfType));

#define   CFA_IF_IPPORT(u4IfIndex) CfaGetIfIpPort (u4IfIndex)

#ifdef LNXIP4_WANTED
#define   CFA_IF_IDXMGRPORT(u4IfIndex) CfaGetIdxMgrPort (u4IfIndex)
#endif


#define   CFA_IF_GET_IFFROM_IPPORT (i4IpPort, pu4IfIndex)\
      CfaGetIfFromIpPort (i4IpPort, pu4IfIndex)
    
/* MACRO to get the Interface Statistics */
#define   CFA_IF_GET_IN_OCTETS(u4IfIndex)     CfaIfGetInOctets (u4IfIndex)

#define   CFA_IF_GET_IN_PKTS(u4IfIndex)       CfaIfGetInPkts (u4IfIndex)

#define   CFA_IF_GET_IN_UCAST(u4IfIndex)      CfaIfGetInUcast (u4IfIndex)

#define   CFA_IF_GET_IN_NUCAST(u4IfIndex)     CfaIfGetInNUcast (u4IfIndex)

#define   CFA_IF_GET_IN_MCAST(u4IfIndex)      CfaIfGetInMCast (u4IfIndex)

#define   CFA_IF_GET_IN_BCAST(u4IfIndex)      CfaIfGetInBCast (u4IfIndex)

#define   CFA_IF_GET_IN_HC_OCTETS(u4IfIndex)  CfaIfGetInHcOctets (u4IfIndex)

#define   CFA_IF_GET_IN_DISCARD(u4IfIndex)    CfaIfGetInDiscard (u4IfIndex)

#define   CFA_IF_GET_IN_ERR(u4IfIndex)        CfaIfGetInErr (u4IfIndex)

#define   CFA_IF_GET_IN_UNK(u4IfIndex)        CfaIfGetInUnk (u4IfIndex)

#define   CFA_IF_GET_OUT_OCTETS(u4IfIndex)    CfaIfGetOutOctets (u4IfIndex)

#define   CFA_IF_GET_OUT_UCAST(u4IfIndex)     CfaIfGetOutUCast (u4IfIndex)

#define   CFA_IF_GET_OUT_NUCAST(u4IfIndex)    CfaIfGetOutNUCast (u4IfIndex)

#define   CFA_IF_GET_OUT_MCAST(u4IfIndex)     CfaIfGetOutMCast (u4IfIndex)

#define   CFA_IF_GET_OUT_BCAST(u4IfIndex)     CfaIfGetOutBCast (u4IfIndex)
  
#define   CFA_IF_GET_OUT_HC_OCTETS(u4IfIndex) CfaIfGetOutHcOctets (u4IfIndex)
 
#define   CFA_IF_GET_OUT_DISCARD(u4IfIndex)   CfaIfGetOutDiscard (u4IfIndex)

#define   CFA_IF_GET_OUT_ERR(u4IfIndex)       CfaIfGetOutErr (u4IfIndex)


/* MACRO to update the Interface Statistics */
#define   CFA_IF_SET_IN_OCTETS(u4IfIndex, u4Octets) \
    CfaIfSetInOctets (u4IfIndex, u4Octets)

#define   CFA_IF_SET_IN_UCAST(u4IfIndex)      CfaIfSetInUCast (u4IfIndex)

#define   CFA_IF_SET_IN_DISCARD(u4IfIndex)    CfaIfSetInDiscard (u4IfIndex)

#define   CFA_IF_SET_IN_ERR(u4IfIndex)        CfaIfSetInErr (u4IfIndex)

#define   CFA_IF_SET_IN_UNK(u4IfIndex)        CfaIfSetInUnk (u4IfIndex)
    
#define   CFA_IF_SET_IN_MCAST(u4IfIndex)      CfaIfSetInMCast (u4IfIndex)

#define   CFA_IF_SET_IN_BCAST(u4IfIndex)      CfaIfSetInBCast (u4IfIndex)

#define   CFA_IF_SET_IN_HC_OCTETS(u4IfIndex, u4Octets) \
           CfaIfSetInHCOctets (u4IfIndex, u4Octets)

#define   CFA_IF_INCR_IN_HC_OCTETS(u4IfIndex) CfaIfIncrInHCOctets (u4IfIndex)

#define   CFA_IF_SET_OUT_OCTETS(u4IfIndex, u4Octets) \
           CfaIfSetOutOctets (u4IfIndex, u4Octets)

#define   CFA_IF_SET_OUT_UCAST(u4IfIndex)     CfaIfSetOutUCast (u4IfIndex)

#define   CFA_IF_SET_OUT_DISCARD(u4IfIndex)   CfaIfSetOutDiscard (u4IfIndex)

#define   CFA_IF_SET_OUT_ERR(u4IfIndex)       CfaIfSetOutErr (u4IfIndex)

#define   CFA_IF_SET_OUT_MCAST(u4IfIndex)     CfaIfSetOutMCast (u4IfIndex)

#define   CFA_IF_SET_OUT_BCAST(u4IfIndex)     CfaIfSetOutBCast (u4IfIndex)

#define   CFA_IF_SET_OUT_HC_OCTETS(u4IfIndex, u4Octets) \
           CfaIfSetOutHCOctets (u4IfIndex, u4Octets)

#define   CFA_IF_INCR_OUT_HC_OCTETS(u4IfIndex) CfaIfIncrOutHCOctets (u4IfIndex)

#define   GET_MODULE_DATA_PTR(pBuf)                 \
          ((tCfaModuleData *) CRU_BUF_Get_ModuleData(pBuf))

#define   CFA_IS_ENET_MAC_MCAST(pHwAddr) \
          (((*((UINT1 *)pHwAddr) & 0xff) == 0x01) ? (1):(0))

#define   CFA_IS_ENET_IPV6_MAC_MCAST(pHwAddr) \
          (((*((UINT2 *)(VOID *)pHwAddr) & 0xffff) == 0x3333) ? (1):(0))

#define   CFA_IS_ENET_MAC_BCAST(pHwAddr) \
          ((*((UINT1 *)pHwAddr) & 0xff) == 0xff ? (1):(0))

#define   CFA_IS_ENET_MAC_UCAST(pHwAddr) \
          ((*((UINT1 *)pHwAddr) & 0x01)? (0):(1))

#define   CFA_IS_VLAN_PRESENT(pBuf) \
          ((((*(pBuf+VLAN_TAG_OFFSET)) == 0x81) && \
           ((*(pBuf+VLAN_TAG_OFFSET+1)) == 0x00)) ? (CFA_TRUE) : (CFA_FALSE))

#ifdef QoS_WANTED
#include "qos.h"

#define   CFA_GET_QOSINFO1(pBuf)                    \
          (GET_MODULE_DATA_PTR(pBuf)->u4NextHopIpAddr)

#define   CFA_SET_QOSINFO1(pBuf, u4Info1)           \
          (GET_MODULE_DATA_PTR(pBuf)->u4NextHopIpAddr = u4Info1)

#define   CFA_GET_QOSINFO2(pBuf)                    \
          (GET_MODULE_DATA_PTR(pBuf)->u4Protocol)

#define   CFA_SET_QOSINFO2(pBuf, u4Info2)           \
          (GET_MODULE_DATA_PTR(pBuf)->u4Protocol = u4Info2)


INT4 CfaHandleIncomingIpPkt PROTO ((tQoSData *,
                                    UINT2,
                                    UINT1));
INT4 CfaHandleOutgoingIpPkt PROTO ((tQoSData *,
                                    UINT2,
                                    INT4));
INT4 CfaHandleQoSPkt PROTO ((tQoSData *,
                             UINT2,
                             UINT4,
                             UINT1 *,
                             UINT2,
                             UINT1,
                             UINT1));
#endif

INT4 CfaInit PROTO((VOID));
INT4  CfaValidateIfIndex PROTO ((UINT4));
INT4  CfaNotifyIpUp PROTO ((VOID));
INT4  CfaIfmEnetConfigMcastAddr PROTO ((UINT1 u1OperCode, 
                                        UINT4 u4IfIndex,
                                        UINT4 u4IpAddr, 
                                        UINT1 *au1HwAddr));
VOID  CfaHandleIpForwardingStatusUpdate PROTO((UINT1 u1IpForwardingStatus));
VOID  CfaSetSystemSizeValue (const char *pu1ModuleName, 
                             const char *pu1ObjectName, 
                            UINT4 u4Value);
INT4  CfaHandlePktFromIp PROTO ((tCRU_BUF_CHAIN_HEADER *, 
                                 UINT4, 
                                 UINT4, 
                                 UINT1 *, 
                                 UINT2, 
                                 UINT1, 
                                 UINT1));

INT4 CfaIfmHandleInterfaceStatusChange PROTO((UINT4 u4IfIndex,
                                              UINT1 u1AdminStatus,
                                              UINT1 u1OperStatus,
                                              UINT1 u1IsFromMib));
/* This function is invoked by DHCP Client */ 
INT4 CfaDynamicInterfaceIpConfig (UINT4 u4IfIndex,
                                  tIpInterfaceParams pIpParams); 

INT4  CfaHandlePktFromIpoa (tCRU_BUF_CHAIN_DESC *pData, 
                            UINT4                u4IfIndex, 
                            UINT2                u2VcIfIndex, 
                            UINT2                u2Protocol);

INT4
CfaGddWrite PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex, 
                   UINT4 u4PktSize, UINT1 u1VlanTagCheck, 
                   tCfaIfInfo * pIfInfo));

INT4   CfaGddConfigPort PROTO((UINT4 u4IfIndex, UINT1 u1ConfigOption,
                              VOID *pConfigParam));

INT1   CfaEnablePromiscuousMode(INT4 i4Index, INT4 i4Value);
INT4   CfaProcessOutgoingIpPkt PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, 
                                          UINT4 u4IfIndex));

INT4   CfaGetIfInfo PROTO((UINT4 u4IfIndex, tCfaIfInfo *pIfInfo));
INT4   CfaSetIfInfo PROTO(( INT4 i4CfaIfParam, UINT4 u4IfIndex, tCfaIfInfo *pIfInfo));
INT4   CfaNotifySpeedToHigherLayer  PROTO(( UINT4 u4IfIndex));
INT4 CfaGetPortName PROTO((UINT4 u4IfIndex, UINT1 *pu1IfName));
INT4 CfaSetPortName PROTO((UINT4 u4IfIndex, UINT1 *pu1IfName));
INT4 CfaGetIfOperStatus PROTO((UINT4 u4IfIndex, UINT1 *pu1OperStatus));
INT4 CfaGetCdbPortAdminStatus (UINT4 u4IfIndex, UINT1 *pu1AdminStatus);
INT4 CfaApiGetPortDesc PROTO ((UINT4 u4IfIndex,tSNMP_OCTET_STRING_TYPE * pPortDesc));
INT4   CfaGetNextActivePort PROTO((UINT4 u4Port, UINT4 *pu4NextPort));
BOOL1  CfaIsThisInterfaceVlan (UINT2 VlanId);
INT4   CfaGetIfCountersFromSoftware (UINT4 u4IfIndex,
                               tIfCountersStruct *pIfCounter); 
INT4
CfaIfmUpdateExtSubType (UINT4 u4IfIndex, UINT2 u2SubType, UINT1 u1Flag);
INT4
CfaApiGetHighIfIndex PROTO ((UINT4 u4IfIndex, UINT4 *pu4HighIfIndex));
INT4
CfaApiCheckIfEntryExist PROTO ((UINT4 u4IfIndex));
INT4
CfaApiGetIfAdminStatus PROTO ((UINT4 u4IfIndex, UINT1 *pu1IfAdminStatus));
INT4
CfaApiGetWanEntryPersist PROTO ((UINT4 u4IfIndex, UINT1 *pu1IfWanPersist));

#ifdef NPAPI_WANTED
VOID   CfaGetIfCountersFromHardware (UINT4 u4IfIndex,
                               tIfCountersStruct *pIfCounter); 
#endif
INT4   CfaGetIfCounters PROTO((UINT4 u4IfIndex, tIfCountersStruct *pIfCounter));
INT4   CfaProcessIncomingIpPkt PROTO((tCRU_BUF_CHAIN_HEADER *pBuf,
                    UINT4 u4IfIndex, UINT1 u1PktType, tEnetV2Header *pEthHdr));
#ifdef MPLS_IPV6_WANTED
INT4   CfaProcessIncomingIpv6Pkt PROTO((tCRU_BUF_CHAIN_HEADER *pBuf,
                    UINT4 u4IfIndex, UINT1 u1PktType, tEnetV2Header *pEthHdr));
#endif
INT4   CfaIwfEnetProcessTxFrame PROTO((tCRU_BUF_CHAIN_HEADER *pBuf,
                                      UINT4 u4IfIndex, UINT1 *au1DestHwAddr,
                                      UINT4 u4PktSize, UINT2 u2Protocol,
                                      UINT1 u1EncapType));

INT4 CfaInterfaceStatusChangeIndication PROTO((UINT4 u4IfIndex,
                                           UINT1 u1OperStatus));
INT4 CfaIvrNotifyMclagOperChangeIndication PROTO((UINT4 u4IfIndex, UINT1 u1OperStatus));

INT4 CfaUfdIsLastDownlinkInGroup PROTO((UINT4 u4IfIndex));
INT4 CfaApiPortChannelUpdateInfo PROTO ((tCfaLaInfoMsg *pCfaLaInfoMsg));

INT4 CfaUpdateVipOperStatus PROTO((UINT4 u4VipIfIndex,
                                   UINT1 u1OperStatus));

VOID CfaUtilUpdateLinkStatus PROTO ((UINT4 u4StartIfIndex, UINT4 u4EndIfIndex));

INT4 CfaUtilClearInterfacesCounters PROTO ((INT4 i4Index));

INT4 CfaSetIfMainAdminStatus PROTO((INT4 i4IfMainIndex, INT4 i4SetValIfMainAdminStatus));

/*This is a callback api that fetches the system specific portID on a particular port*/
INT4 CfaGetInfo PROTO ((tCfaInfoInput CfaInput, tCfaInfoOutput *pCfaOutput));

/* This Function is used to configure IP Information for the Interface */
INT4 CfaInterfaceConfigIpParams (UINT4 u4IfIndex, tIpInterfaceParams IpInfo);

INT4 CfaConfigBaseBridgeMode (UINT1 u1BaseBridgeMode);


INT4 CfaIsMgmtPortEnabled (VOID);

UINT4 CfaIsDualOobEnabled (VOID);

INT4 CfaPostPktFromL2 PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4IfIndex,
                              UINT4 u4PktSize, UINT2 u2Protocol,
                              UINT1 u1EncapType));

INT4 CfaPostPktFromWss PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4IfIndex,
                              UINT4 u4PktSize, UINT2 u2Protocol,
                              UINT1 u1EncapType));

INT4 CfaPostPktFromL2OnPortList (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *PortList,
                                 UINT4 u4PktSize, UINT2 u2Protocol,
                                 UINT1 u1EncapType);


INT4 CfaHandlePktFromEoam PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                    UINT4 u4PktSize, UINT2 u2Protocol,
                                        UINT1 *au1DestHwAddr, UINT1 u1EncapType));


INT4 CfaPostPktOverVlan PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4ContextId,
                                UINT2 u2VlanId));

INT4 CfaDeletePortChannelIndication (UINT4 u4IfIndex);

VOID CfaCheckStatusAndSendTrap (UINT2 u2Port);

INT4 CfaDelDefL3VlanInterface(VOID);

INT4 CfaCheckBrgPortType PROTO ((UINT4 u4IfIndex));

INT4 CfaCheckNonPhysicalInterfaces PROTO ((VOID));
INT4 CfaCreateDefaultIVRInterface PROTO ((VOID));
UINT1 CfaGetCidrSubnetMaskIndex PROTO((UINT4 u4SubnetMask));
INT1 CfaGetIfIpAddr  (INT4 i4IfMainIndex, UINT4 *pu4RetValIfIpAddr);
UINT4 CfaGetCidrSubnetMask (UINT4 u4NetPrefixLen);
INT1 CfaHandleDhcpFallback (UINT4 u4IfIndex);

INT4 CfaSetIfType PROTO((UINT4 u4IfIndex, UINT1 u1IfType));
INT4 CfaGetRmConnectingPortIfIndex (UINT4 *pu4ConnectingPortIfIndex);

#if defined(MPLS_WANTED) || defined(TLM_WANTED)
INT4
CfaIfmDeleteDynamicStackEntry PROTO((UINT4 u4MplsTnlIfIndex,
                                     UINT4  u4MplsIfIndex));
INT4
CfaUtilGetMplsIfFromMplsTnlIf (UINT4 u4MplsTnlIfIndex, UINT4 *pu4MplsIfIndex, BOOL1 bLockReq);
INT4
CfaUtilGetL3IfFromMplsIf (UINT4 u4MplsIfIndex, UINT4 *pu4L3IfIndex, BOOL1 bLockReq);
#endif

#ifdef MPLS_WANTED
INT4   CfaIfmCreateMplsAtmVcInterface PROTO((UINT4 u4IfIndex, UINT2 u2Vpi,
                               UINT2 u2Vci, tAtmVcConfigStruct *pIfAtmInfo));
INT4   CfaIfmDeleteMplsAtmVcInterface PROTO((UINT4 u4IfIndex));
INT4   CfaHandlePktFromMpls PROTO((tCRU_BUF_CHAIN_HEADER * pBuf,
                                 UINT4 u4IfIndex, UINT1 u1Direction,
                                 UINT1 *pu1MediaAddr, UINT1 u1PktType));
INT4 CfaIwfMplsHandleOutGoingPkt (tCRU_BUF_CHAIN_HEADER * pBuf,
                                  UINT4 u4MplsIfIndex, UINT4 u4PktSize);
INT4
CfaIwfMplsProcessOutGoingL2Pkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                                  UINT4 u4VfId, UINT1 u1PktType);


INT4 CfaIfmInitMplsTunnelIfEntry PROTO((UINT4 u4IfIndex, UINT1 *au1PortName));
INT4  CfaUtilGetIfIndexFromMplsTnlIf PROTO((UINT4 u4TnlIfIndex,
                                             UINT4 *pu4L3VlanIf, BOOL1 bLockReq));
INT4
CfaUtilGetMplsIfFromIfIndex (UINT4 u4L3IpIntf, UINT4 *pu4MplsIfIndex, BOOL1 bLockReq);

INT4
CfaUtilGetMplsTnlIfFromMplsIfIndex (UINT4 u4MplsL3IpIntf, UINT4 *pu4MplsIfIndex,BOOL1 bLockReq);


INT4
CfaIfmCreateDynamicMplsTunnelInterface PROTO((UINT4 u4IfIndex,
                                              UINT1 *pu1IfName));

INT4
CfaIfmCreateStackMplsTnlIntfFromTnlIfIndex(UINT4 u4L3IpIntf,
                                           UINT4 u4TnlIfIndex);

INT4 
CfaIfmDeleteDynamicMplsTunnelInterface PROTO((UINT4 u4IfIndex,
                                              UINT1 *pu1IfName,
                                              BOOL1 bLockReq));

INT4
CfaIfmCreateStackMplsTunnelInterface PROTO((UINT4 u4L3IpIntf,
                                            UINT4 *pu4MplsTnlIfIndex));

INT4
CfaIfmDeleteStackMplsTunnelInterface PROTO((UINT4 u4L3IpIntf, 
                                            UINT4 u4MplsTnlIfIndex));
INT4
CfaUtilL3IfStatusHandleForMplsIf (UINT4 u4MplsIfIndex, UINT1 u1OperStatus);
INT4
CfaApiGetTeLinkIfFromMplsIf (UINT4 u4MplsIfIndex, UINT4 *pu4TeLinkIf, BOOL1 bLockReq);
INT4
CfaApiGetTeLinkIfFromMplsTnlIf (UINT4 u4MplsTnlIfIndex, UINT4 *pu4TeLinkIf, BOOL1 bLockReq);
INT4
CfaApiGetFirstMplsIfInfo PROTO ((UINT4 *pu4IfIndex, tCfaIfInfo * pIfInfo));
INT4 
CfaApiGetNextMplsIfInfo PROTO ((UINT4 u4IfIndex, UINT4 *pu4NextIndex,
                                     tCfaIfInfo * pIfInfo));  
#endif /* MPLS_WANTED */

#ifdef TLM_WANTED
INT4
CfaApiGetTeLinkIfFromL3If (UINT4 u4L3IfIndex, UINT4 *pu4TeLinkIf, UINT1 u1Level,
                           BOOL1 bLockReq);
INT4
CfaApiGetL3IfFromTeLinkIf (UINT4 u4TeLinkIf, UINT4 *pu4L3If, BOOL1 bLockReq);
INT4
CfaApiGetMplsIfFromTeLinkIf (UINT4 u4TeLinkIf, UINT4 *pu4MplsIfIndex, BOOL1 bLockReq);
INT4
CfaApiGetHLTeLinkFromLLTeLink (UINT4 u4LLTeLinkIf, UINT4 *pu4HLTeLinkIf,
                               BOOL1 bLockReq);
INT4
CfaApiGetFirstLLTeLinkFromHLTeLink (UINT4 u4HLTeLinkIf, UINT4 *pu4LLTeLinkIf,
                                    BOOL1 bLockReq);
INT4
CfaApiGetNextLLTeLinkFromHLTeLink (UINT4 u4HLTeLinkIf, UINT4 u4CurLLTeLinkIf,
                                   UINT4 *pu4NextLLTeLinkIf, BOOL1 bLockReq);

INT4 
CfaApiDeleteDynamicTeLink (UINT4 u4IfIndex, UINT1 u1DelIfEntry) ;
INT4
CfaApiCreateDynamicTeLinkInterface (UINT4 u4IfIndex, UINT4 u4CompIfIndex);
#endif

#ifdef ISIS_WANTED
INT4   CfaHandlePktFromIsIs PROTO((tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2IfIndex,
                                   UINT2 u2Protocol));
INT4   CfaIfmCreateDynamicAtmVcInterface PROTO((UINT4 u4Port, UINT2 u2Vpi, 
                                                UINT2 u2Vci,
                                                tAtmVcConfigStruct *pIfAtmInfo));
INT4   CfaIfmDeleteDynamicAtmVcInterface PROTO((UINT4 u4IfIndex));
#endif /* ISIS_WANTED */

#ifdef S_A_CONV_WANTED
INT4 CvHandleIncomingPkt PROTO((tCRU_BUF_CHAIN_HEADER  *pBuf, UINT4 u4IfIndex, 
                                UINT4 u4PktSize, UINT2 u2Protocol, 
                                UINT1 u1EncapType));

INT4 CfaIfmSAConvRegisterInterface PROTO((UINT2 u2SyncIfIndex, 
                                          UINT2 u2AsyncIfIndex));
INT4 CfaIfmSAConvDeRegisterInterface PROTO((UINT2 u2SyncIfIndex, 
                                            UINT2 u2AsyncIfIndex));
#endif /* S_A_CONV_WANTED */

VOID CfaUpdateInterfaceStatus  PROTO ((UINT4 u4IfIndex, UINT1 u1IfStatus));
INT4 CfaTxPPPoEPkt PROTO((tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex, 
     UINT1* au1MediaAddr, UINT2 u2Protocol));
VOID CfaIfmAddNewLinkToBundle PROTO((UINT4 u4BundleIndex));
VOID CfaIfmDeletePppLink PROTO((UINT4 u4PppIndex));
INT4 CfaGetFreeSerialInterface PROTO ((VOID));
INT4 CfaHandlePktFromPpp PROTO((tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4IfIndex,
                                UINT1 u1Direction, UINT2 u2Protocol));
INT4 CfaIfmHandleAsyncParamsUpdate PROTO((UINT4 u4IfIndex, VOID *pAsyncParams));
INT4 CfaIfmHandlePppConfigUpdate PROTO((UINT4 u4IfIndex, 
                                        UINT4 u4LocalIpAddr, 
                                        UINT4 u4PeerIpAddr, UINT4 u4Mtu,
                                        UINT4 u4IdleTimeout));

VOID  CfaCreateNewBundleIf (UINT4, UINT4);

INT4 CfaIfmHandleMpBundleUpdate PROTO((UINT2 u2MpIfIndex, UINT2 u2PppIfIndex, 
                                       UINT1 u1Command));
INT4 CfaIfmHandleBcpConfigUpdate PROTO((tBcpConfigStruct PppBcpConfigUpdate)); 
INT4 CfaPppInitForIfType(UINT4 u4IfIndex);
INT4 CfaPppCheckPortBinding(UINT4 u4IfIndex);


#ifdef L2TP_WANTED
INT4 CfaHandlePktFromL2tp PROTO((tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4IfIndex));
#endif

#ifdef IKE_WANTED
INT4 CfaHandlePacketFromIke PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf));
#endif


VOID CfaReleaseInterfaceIndex (UINT4 u4IfIndex);

/* Utility functions */

UINT4 CfaGetInterfaceIndexFromName PROTO ((UINT1 *pu1Alias, 
                                         UINT4 *pu4Index));
UINT4 CfaGetInterfaceIndexFromNameInCxt PROTO ((UINT4 u4L2ContextId,
                                                UINT1 *pu1Alias, 
                                         UINT4 *pu4Index));
INT4
cli_get_if_type (UINT1 *pu1Interface);

UINT4 CfaGetInterfaceNameFromIndex PROTO ((UINT4 u4Index, 
                                           UINT1 *pu1Alias));

UINT4 CfaGetFreeInterfaceIndex PROTO ((UINT4 *pu4Index, UINT4 u4IfType));
UINT4 CfaGetFreeIndexForSubType PROTO ((UINT4 *pu4Index, UINT4 u4IfType, 
                                        UINT4 u4SubType));
 
UINT4 CfaGetFreeVipInterfaceIndex PROTO ((UINT4 *pu4Index));
UINT4 CfaGetNextInterfaceName PROTO ((UINT4 ,UINT4 ,UINT1 *));
VOID CfaConvertIntToStr PROTO ((UINT4, UINT1 *));
UINT4 CfaCliIsDefaultInterface PROTO ((INT4));
UINT4 CfaGetDefaultVlanInterfaceIndex PROTO ((VOID)); 
INT1 CfaGetVlanId PROTO ((UINT4 u4IfIndex, tVlanIfaceVlanId *pVlanId));
INT1 CfaGetLoopbackId PROTO ((UINT4 u4IfIndex, UINT2 *pLoopbackId));
UINT4 CfaGetVlanInterfaceIndex PROTO ((tVlanIfaceVlanId VlanId));
UINT4 CfaGetVlanInterfaceIndexInCxt PROTO ((UINT4 u4L2ContextId,
                                            tVlanIfaceVlanId VlanId));
UINT4 CfaGetL3SubIfVlanIndexInCxt PROTO ((UINT4 u4L2ContextId,
                                            tVlanIfaceVlanId VlanId));
UINT4 CfaGetL3XSubIfVlanIndexInCxt (UINT4 u4L2ContextId, tVlanIfaceVlanId 
                                            VlanId, UINT4 u4ParIfIndex);
UINT4  FsNpIpv4DeleteL3SubInterface (UINT4 u4Index);

UINT4
FsNpIpv4CreateL3SubInterface (UINT4 u4VrId, UINT1 *pu1IfName,
                           UINT4 u4CfaIfIndex,
                           UINT4 u4IpAddr, UINT4 u4IpSubNetMask,
                           UINT2 u2VlanId, UINT1 *au1MacAddr,UINT4 u4ParentIfIndex);

INT4
CfaGetL3SubIfParentIndex (UINT4 u4L3SubIfIndex, UINT4 *pu4IfIndex);

INT4
CfaGetParentPortOperStatus (UINT4 u4L3SubIfIndex, UINT1 *pu1OperStatus);

INT4 CfaIsL3IpVlanInterface PROTO((UINT4 u4IfIndex));
UINT1 CfaIsTunnelInterface PROTO ((UINT4 u4IfIndex));

/* Function to register with the NP driver */
#ifdef NPAPI_WANTED
INT4 CfaRegisterWithNpDrv PROTO ((VOID));
#endif


/* Exportable APIs for accessing Interface Statistics */
UINT4 CfaIfGetInOctets PROTO ((UINT4 u4IfIndex));
UINT4 CfaIfGetInPkts PROTO ((UINT4 u4IfIndex));
UINT4 CfaIfGetInUcast PROTO ((UINT4 u4IfIndex));
UINT4 CfaIfGetInNUcast PROTO ((UINT4 u4IfIndex));
UINT4 CfaIfGetInMCast PROTO ((UINT4 u4IfIndex));
UINT4 CfaIfGetInBCast PROTO ((UINT4 u4IfIndex));
UINT4 CfaIfGetInHcOctets PROTO ((UINT4 u4IfIndex));
UINT4 CfaIfGetInDiscard PROTO ((UINT4 u4IfIndex));
UINT4 CfaIfGetInErr PROTO ((UINT4 u4IfIndex));
UINT4 CfaIfGetInUnk PROTO ((UINT4 u4IfIndex));
UINT4 CfaIfGetOutOctets PROTO ((UINT4 u4IfIndex));
UINT4 CfaIfGetOutUCast PROTO ((UINT4 u4IfIndex));
UINT4 CfaIfGetOutNUCast PROTO ((UINT4 u4IfIndex));
UINT4 CfaIfGetOutMCast PROTO ((UINT4 u4IfIndex));
UINT4 CfaIfGetOutBCast PROTO ((UINT4 u4IfIndex));
UINT4 CfaIfGetOutHcOctets PROTO ((UINT4 u4IfIndex));
UINT4 CfaIfGetOutDiscard PROTO ((UINT4 u4IfIndex));
UINT4 CfaIfGetOutErr PROTO ((UINT4 u4IfIndex));
INT4 CfaGetIfSpeed PROTO((UINT4 u4IfIndex, UINT4 *pu4IfSpeed));
/* Exportable APIs for updating Interface Statistics */
VOID CfaIfSetInOctets PROTO ((UINT4 u4IfIndex, UINT4 u4Octets));
VOID CfaIfSetInUCast PROTO ((UINT4 u4IfIndex));
VOID CfaIfSetInDiscard PROTO ((UINT4 u4IfIndex));
VOID CfaIfSetInErr PROTO ((UINT4 u4IfIndex));
VOID CfaIfSetInUnk PROTO ((UINT4 u4IfIndex));
VOID CfaIfSetInMCast PROTO ((UINT4 u4IfIndex));
VOID CfaIfSetInBCast PROTO ((UINT4 u4IfIndex));
VOID CfaIfSetInHCOctets PROTO ((UINT4 u4IfIndex, UINT4 u4Octets));
VOID CfaIfIncrInHCOctets PROTO ((UINT4 u4IfIndex));
VOID CfaIfSetOutOctets PROTO ((UINT4 u4IfIndex, UINT4 u4Octets));
VOID CfaIfSetOutUCast PROTO ((UINT4 u4IfIndex));
VOID CfaIfSetOutDiscard PROTO ((UINT4 u4IfIndex));
VOID CfaIfSetOutErr PROTO ((UINT4 u4IfIndex));
VOID CfaIfSetOutMCast PROTO ((UINT4 u4IfIndex));
VOID CfaIfSetOutBCast PROTO ((UINT4 u4IfIndex));
VOID CfaIfSetOutHCOctets PROTO ((UINT4 u4IfIndex, UINT4 u4Octets));
VOID CfaIfIncrOutHCOctets PROTO ((UINT4 u4IfIndex));
UINT1 CfaGetIvrStatus PROTO((VOID));
INT1 CfaIvrNotifyVlanIfOperStatusInCxt PROTO((UINT4 u4ContextId, 
                                         tVlanIfaceVlanId VlanId, 
  UINT1 u1IfOperStatus));
INT1
CfaIvrNotifyMclagIfOperStatusInCxt PROTO ((UINT4 u4L2ContextId,
                                   tVlanIfaceVlanId VlanId, UINT1 u1OperStatus));
INT4 CfaIvrConvertStrToVlanList (UINT1 *pu1String, UINT1 *au1PortArray);
VOID CfaCheckSecurityForBridging(tCRU_BUF_CHAIN_HEADER *pBuf,
                                 UINT4 u4IfIndex,
                                 UINT1 *pu1IsSecWantedForBridgedTraffic);
INT1 CfaGetMgmtVlanList (tMgmtVlanList MgmtVlanList);
#ifdef CLI_WANTED
/* Function used by CLI to validate and get the interface prompt */
INT1 CfaGetIfConfigPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1DispStr));
INT1 CfaGetL3SubIfConfigPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1DispStr));
INT1 CfaGetIfRangeConfigPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1DispStr));
INT1 CfaGetIfRouterPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1DispStr));
INT1 CfaGetIfOpenflowPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1DispStr));
INT1 CfaGetIfPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1DispStr));
INT1 CliGetUfdGroupPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1DispStr));
INT1 CfaGetPortChannelPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1DispStr));
INT1 CfaGetVirtualPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1DispStr));
INT1 CfaGetSispPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1DispStr));
INT1 CfaGetTnlPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1DispStr));
INT1 CfaGetIvrPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1DispStr));
INT1 CfaGetLoopbackPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1DispStr));
INT1 CfaGetLinuxIvrPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1DispStr));
VOID CliUfdGroup PROTO ((VOID));
#ifdef PPP_WANTED
INT1 CfaGetPppPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1DispStr));                              
#endif
INT1 CfaGetPswPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1DispStr));
INT1 CfaGetACPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1DispStr));
INT1
CfaGetEvbSbpPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1DispStr));
INT1
CfaGetEvbUapPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1DispStr));
#ifdef VXLAN_WANTED
INT1 CfaGetNvePrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1DispStr));
#endif
#ifdef HDLC_WANTED
INT1 CfaGetHdlcPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1DispStr));
#endif
#ifdef VCPEMGR_WANTED
INT1 CfaGetTapPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1DispStr));
#endif
#endif /*CLI_WANTED*/

VOID
CfaVlanTagFrame (UINT1 *pBuff, tVlanIfaceVlanId VlanId, UINT1 u1Priority);

INT4 CfaGetAddrAllocMethod(UINT4 u4IfIndex);
INT1 CfaGetLinkTrapEnabledStatus (UINT4 , UINT1 *);

INT4 CfaTnlMgrTxFrame PROTO ((tTnlPktTxInfo *pTnlPktInfo));

INT4 CfaGetTnlEntry PROTO ((tTnlIfEntry *pTnlIfEntry, UINT4 u4AddrType,
                            tTnlIpAddr *pTnlSrcAddr, tTnlIpAddr *pTnlDestAddr));

INT4 CfaGetTnlEntryInCxt PROTO ((UINT4 u4ContextId, tTnlIfEntry *pTnlIfEntry,
                                 UINT4 u4AddrType,
                                 tTnlIpAddr *pTnlSrcAddr,
                                 tTnlIpAddr *pTnlDestAddr));

INT4 CfaGetTnlEntryFromIfIndex PROTO ((UINT4 u4IfIndex,
                                       tTnlIfEntry *pTnlIfEntry));
VOID CfaTnlIfaceUnMapping PROTO ((UINT4  u4IfIndex, UINT4 u4ContextId));

VOID CfaMain PROTO((INT1 *));
INT4 CfaIsMacAllZeros PROTO((UINT1 *));
UINT1 CfaIsPhysicalInterface PROTO ((UINT4 u4IfIndex));

INT4 CfaIfmBringupAllInterfaces (INT1 *pi1Dummy);
INT4 CfaIfmMapDefaultInterfaceForDefaultContext (VOID);
INT4 CfaIfmDefaultInterfaceInit PROTO((VOID));

UINT4  CfaIsMgmtPort (UINT4 u4IfIndex);

UINT4  CfaIsLinuxVlanIntf (UINT4 u4IfIndex);
INT4   CfaGetIfaceType (UINT4 u4IfIndex, UINT1 *pu1IfType);

UINT4  CfaIsLoopBackIntf (UINT4 u4IfIndex);
UINT4  CfaIsValidLinuxVlanName (UINT1 *pu1IfName);
INT4 CfaCliGetOobIfIndex PROTO ((UINT4 *pu4IfIndex));

INT4 CfaCliGetIndexFromType PROTO ((UINT4 u4IfType, INT1 *pi1IfNum, UINT4 *pu4IfIndex));
INT4 CfaCliGetIndexFromSubType PROTO ((UINT4 u4IfType, UINT4 u4IfSubType, 
                                       INT1 *pi1IfNum, UINT4 *pu4IfIndex));
INT4 CfaGetIfIndex PROTO ((UINT4 u4IfType,INT4 i4PortNum,UINT4 *pu4IfIndex));
INT4 CfaValidateSlotAndPortNum PROTO ((UINT4 u4IfType, INT4 i4SlotNum, UINT4 u4PortNum));
INT4 CfaGetSlotAndPortFromIfIndex PROTO ((UINT4, INT4 *, INT4 *));
INT4 CfaGetPortNumFromIfIndex PROTO ((UINT4 u4IfType,UINT4 u4IfIndex,
                                      INT4 i4SlotNum, INT4 i4SlotPortNo, INT4 *pi4PortNum));
INT4 CfaGetPhysIfName PROTO ((UINT4 u4IfIndex, INT1 *pi1IfName));
INT4 CfaGetL3SubIfName PROTO ((UINT4 u4IfIndex, INT1 *pi1IfName));
INT4 CfaCliGetIfName PROTO ((UINT4 u4IfIndex, INT1 *pi1IfName));
INT4 CfaMsrGetIfName PROTO ((UINT4 u4IfIndex, INT1 *pi1IfName));
INT4 CfaCliGetIfList PROTO ((INT1 *pi1IfName, INT1 *pi1IfListStr, 
                             UINT1 *pu1IfListArray, UINT4 u4IfListArrayLen));

INT4 CfaCliConfGetIfName PROTO ((UINT4 u4IfIndex, INT1 *pi1IfName));

INT4
CfaCliGetL3SubIfIndex (INT1 *pi1IfName, INT1 *pi1IfNum, UINT4 *pu4IfIndex);

INT4
CfaCliGetPhysicalAndLogicalPortNum (INT1 *pi1IfNum, INT4 *pi4PhyPort,
                                                    INT4 *pi4LPortNum,
                                                    INT4 *pi4SlotNum);
INT1
CfaIsL3SubIfNum (INT1 *pi1IfNum);

INT1
CfaIsL3SubIfIndex (UINT4 u4IfIndex);

INT1
CfaIsL3SubLogicalPort (INT4 i4LPortNum);

INT4
CfaCliGetPortChannelPhysicalAndLogicalPortNum(INT1 *pi1IfNum,
                                                INT4 *pi4AggId, INT4 *pi4LAggId);


#ifdef CLI_WANTED
INT4 cli_get_ifc_index PROTO ((UINT1 *));


INT4 CfaCliValidateInterfaceName PROTO ((INT1 *pi1IfName, INT1 *pi1RetIfName, UINT4 *pu4RetIfType));

INT4 CfaCliValidateXInterfaceName PROTO ((INT1 *pi1IfName, INT1 *pi1RetIfName, UINT4 *pu4RetIfType));
INT4 CfaCliValidateXSubTypeIntName PROTO ((INT1 *pi1IfXName, INT1 *pi1RetIfXName,
                                           UINT4 *pu4RetIfXType, UINT4 *pu4RetIfXSubType));
INT4 CfaCliValidateIpInterfaceName PROTO ((INT1 *pi1IfName, INT1 *pi1RetIfName, UINT4 *pu4RetIfType));
#endif

INT4 CfaCliGetIfIndex PROTO ((INT1 *pi1IfName, INT1 *pi1IfNum, UINT4 *pu4IfIndex));

INT4 *
cli_get_iface_type_name PROTO ((UINT4 u4Type));

INT4 *
CfaUtilGetIfaceTypeName PROTO ((UINT4 u4Type, UINT4 u4SubType));


INT4
CfaGetIfIndexFromIfNameAndIfNum PROTO ((UINT1 *pu1IfName,
                                 UINT1 *pu1IfNum, UINT4 *pu4IfIndex));

INT4
CfaGetIfIndexFromSlotAndPort PROTO ((INT4 i4SlotNum, UINT4 u4IfNum, 
                                     UINT4 *pu4IfIndex));

INT4 CfaUnlock PROTO ((VOID));
INT4 CfaLock PROTO ((VOID));
INT4 CfaPktTxUnlock PROTO ((VOID));
INT4 CfaPktTxLock PROTO ((VOID));
INT4 CfaCommonDSLock PROTO ((VOID));
INT4 CfaCommonDSUnLock PROTO ((VOID));

INT4 CfaTunnelDSLock PROTO ((VOID));
INT4 CfaTunnelDSUnLock PROTO ((VOID));

#ifdef MBSM_WANTED
INT4 CfaIsPortExist PROTO ((UINT4 u4IfIndex));
INT4 CfaMbsmMsgToCfa PROTO ((UINT1 *pMsg, UINT1 u1Cmd));
INT4 CfaMbsmProtoMsgToCfa PROTO ((tMbsmProtoMsg *pMsg, INT4 i4Event));
INT4 CfaMbsmNpInit PROTO((VOID));
VOID CfaNotifyBootComplete PROTO((VOID));
INT4 CfaMbsmInitDefEtherType (INT4 i4SlotNum);
INT4 CfaMbsmCreateInterfacesForAllSlot PROTO ((VOID));
INT4 CfaMbsmDeleteInterfacesForAllSlot PROTO ((VOID));
INT4 CfaUtilIsConnectingPorts PROTO ((UINT4 u4IfIndex));
#endif
VOID CfaSetLocalUnitPortInformation PROTO ((tHwPortInfo *pHwPortInfo));
VOID CfaGetLocalUnitPortInformation PROTO ((tHwPortInfo *pHwPortInfo));
#ifdef L2RED_WANTED
INT4 CfaRmCallBk PROTO((UINT1 u1Event, tCRU_BUF_CHAIN_HEADER * , UINT2 ));
#endif

#ifdef NPAPI_WANTED
INT4 CfaNpProcessPktRcvdOnStackPort (INT4 i4Unit, UINT1 *pu1Pkt, 
                                     UINT4 u4PktSize);
#endif

VOID CfaGetSysMacAddress(tMacAddr pSwitchMac);
VOID
CfaGetSysSuppTimeFormat (UINT1 * u1SysSuppTimeFormat);

INT4
CfaIfGetIpAllocProto PROTO ((UINT4 u4IfIndex, UINT1 *pu1IpAllocProto));
INT4
CfaIfGetIpAllocMethod PROTO ((UINT4 u4IfIndex, UINT1 *pu1IpAllocMethod));
UINT4 CfaGetDefaultRouterIfIndex  PROTO ((VOID));
VOID CfaSetPauseMode PROTO ((INT4 i4IssPortCtrlIndex, INT4  i4PortCtrlMode));
extern UINT4               gu4MgmtPort;
extern UINT4        gu4MgmtIntfRouting;

/* API used by Fault Management & EOAM Parser Module to Tx Layer 2 
 * Loopback Test data*/
INT4 CfaEnetTxLoopbackTestFrame PROTO ((UINT4 u4IfIndex, 
                                        tCRU_BUF_CHAIN_HEADER * pBuf, 
                                        UINT4 u4PktSize));
/* Macros to be used for configuring IP Parameters */
#define  CFA_IP_IF_ALLOC_PROTO     0x00000001
#define  CFA_IP_IF_ALLOC_METHOD    0x00000002
#define  CFA_IP_IF_PRIMARY_ADDR    0x00000008
#define  CFA_IP_IF_NETMASK         0x00000010
#define  CFA_IP_IF_BCASTADDR       0x00000020
#define  CFA_IP_IF_FORWARD         0x00000040
#define  CFA_IP_IF_PORTVID         0x00000080
#define  CFA_IP_IF_NOFORWARD       0x00001000

#define   CFA_IP_IF_SECONDARY_ADDR_ADD   0x00000080
#define   CFA_IP_IF_SECONDARY_ADDR_DEL   0x00001000
#define   CFA_IP_IF_SECONDAY_ROW_STATUS  0x00002000
#define   CFA_DEFAULT_STACK_IFINDEX      CFA_DEFAULT_ROUTER_VLAN_IFINDEX + 1
#define   CFA_DEFAULT_STACK_VLAN_ID      4094
#define   CFA_STACK_PORT_START_INDEX     MBSM_MAX_PORTS_PER_SLOT +1

/* These macros mention that IP Addresses are assigned to the interfaces
 * either manually or virtually */
#define CFA_IP_IF_MANUAL         1
#define CFA_IP_IF_VIRTUAL        2

/* Information associated with the IP interface */
typedef struct {
    UINT4             u4Addr; 
    UINT4             u4NetMask;   
    UINT4             u4BcastAddr;
    UINT1             u1AddrAllocMethod;
    UINT1             u1AddrAllocProto;
    UINT1             u1IpForwardEnable;
    UINT1             u1Reserverd;
    tIp6Addr          Ip6Addr;
    UINT2             u2PortVlanId;   /* Holds the VLAN Identifier used for Routerport Implemenation */ 
    UINT1             u1PrefixLen;
    UINT1             u1Pad;
} tIpConfigInfo;

/* Information required for packet processing*/
typedef struct {
   UINT4              u4IfIndex;
   UINT4              u4ContextId;
   UINT4              u4VlanOffset;
   UINT4              u4ModuleId;
   tEnetV2Header      EnetHdr; /* This structure has 14 bytes.
                                  Allignement needs to be taken care*/
   UINT2              u2EtherProto;
   UINT2              u2LocalPortId;
   UINT1              u1IpProto;
   UINT1              u1IfType;
   UINT1              u1LinkFrameType;
   UINT1              au1Pad[3];
} tPktHandleInfo;

typedef struct {
    UINT4 u4VrfId;
    UINT4 u4Mtu;
    UINT4 u4IfIndex;
} tL3MtuInfo;


/* APIs provide for IP interface */
/* Configure IP related Information associated with the interface */
 INT4 CfaIpIfSetIfInfo PROTO ((UINT4 u4IfIndex, UINT4 u4Flag, tIpConfigInfo *pIpInfo));

 INT4 CfaIpUpdateHostRoute          PROTO ((UINT4, UINT4, INT2, UINT1));

 INT4 CfaIpIfDeleteIpInterface PROTO ((UINT4   u4CfaIfIndex));


/* Get the IP related Information associated with the interface */
INT4 CfaIpIfGetIfInfo PROTO ((UINT4 u4IfIndex, tIpConfigInfo *pIpInfo));

INT4  CfaIpIfGetSrcAddress PROTO ((UINT4 u4Dest, UINT4 *pu4Src));

INT4  CfaIpIfGetSrcAddressOnInterface PROTO ((UINT4 u4CfaIfIndex, 
                                              UINT4 u4Dest, UINT4 *pu4Src));

INT4 CfaIpIfIsOurAddress PROTO ((UINT4 u4IpAddress));

INT4 CfaIpIfIsOurAddressInCxt PROTO ((UINT4 u4ContextId, UINT4 u4IpAddress));

INT4   CfaIpIfGetIfIndexFromIpAddress PROTO ((UINT4 u4IpAddress, 
                                              UINT4  *pu4CfaIfIndex));

INT4   CfaIpIfGetIfIndexFromIpAddressInCxt PROTO ((UINT4 u4ContextId, 
                                                   UINT4 u4IpAddress, 
                                                   UINT4  *pu4CfaIfIndex));

INT4 CfaIpIfCheckLoopbackAddr PROTO ((UINT4 u4IpAddress));

INT4 CfaIpIfValidateIfIpSubnet PROTO ((UINT4 u4CurrIfIndex, UINT4 u4CurrIpAddr,
                                      UINT4 u4CurrNetMask));

INT4 CfaIpIfIsLocalBroadcastInCxt   PROTO ((UINT4 u4ContextId, UINT4 u4IpAddress));

INT4 CfaIpIfCheckInterfaceBcastAddr PROTO ((UINT4 u4CfaIfIndex,
                                            UINT4 u4IpAddress));

INT4 CfaIpIfIsLocalNet PROTO ((UINT4 u4IpAddress));

INT4 CfaIcclIpIfIsLocalNet PROTO ((UINT4 u4IpAddress, UINT4 u4SubnetMask));

UINT4 CfaGetGlobalModTrc PROTO((VOID));

INT4 CfaIpIfIsLocalNetInCxt PROTO ((UINT4 u4ContextId, UINT4 u4IpAddress));

INT4 CfaIpIfIsLocalNetOnInterface PROTO ((UINT4 u4CfaIfIndex, 
                                          UINT4 u4IpAddress));
INT4 CfaIpIfIsIpValidOnLocalNetIf PROTO ((UINT4 u4CfaIfIndex,
                                          UINT4 u4IpAddress));
INT4 CfaIpIfIsSecLocalNetOnInterface PROTO ((UINT4 u4CfaIfIndex, UINT4 u4IpAddress,
                                              UINT4 *pu4NetMask));
INT4
CfaIpIfGetIfIndexForNetInCxt PROTO ((UINT4 u4ContextId, UINT4 u4IpAddress,
                                     UINT4 *pu4IfIndex));

VOID CfaIpIfGetHighestIpAddr PROTO ((UINT4  *pu4IpAddress));
    
VOID CfaIpIfGetHighestIpAddrInCxt PROTO ((UINT4 u4ContextId,
                                          UINT4  *pu4IpAddress));
    
INT4  CfaIpIfGetNextIpAddr PROTO ((UINT4 u4IpAddress, 
                                   UINT4 *pu4NextIpAddress));


INT4  CfaIpIfIsSecondaryAddress PROTO ((UINT4 u4IfIndex, 
                                   UINT4 pu4SecIpAddress));

INT4  CfaIpIfGetNextIpAddrInCxt PROTO ((UINT4 u4ContextId, UINT4 u4IpAddress, 
                                        UINT4 *pu4NextIpAddress));

INT4   CfaIpIfGetNextSecondaryAddress PROTO ((UINT4 u4CfaIfIndex, 
                                              UINT4 u4IpAddress, 
                                              UINT4 *pu4NextIpAddress, 
                                              UINT4 *pu4NetMask));

INT4   CfaIpIfGetIfIndexFromHostIpAddressInCxt PROTO ((UINT4 u4ContextId,
                                                       UINT4 u4IpAddress,
                                                       UINT4  *pu4CfaIfIndex));

/* Get the information associated with the given IP Address */
INT4 CfaIpIfGetIpAddressInfo PROTO ((UINT4 u4IpAddress, UINT4 *pu4NetMask,
                                  UINT4 *pu4BcastAddr, UINT4  *pu4CfaIfIndex));

INT4 CfaIpIfGetIpAddressInfoInCxt PROTO ((UINT4 u4ContextId, UINT4 u4IpAddress, 
                                          UINT4 *pu4NetMask, UINT4 *pu4BcastAddr,
                                          UINT4  *pu4CfaIfIndex));

INT4 CfaSendEventToCfaTask PROTO ((UINT4 u4Event));

INT4 CfaNotifyStartBootupEvent PROTO ((VOID));

INT4 CfaNotifyStartPktProcessEvent PROTO ((VOID));

/* Get the OID for a given object name ( only for cfa module related mibs ) */
INT4 CfaApiGetOIDFromObjName PROTO ((UINT1 *pObjNameStr, 
                                     tSNMP_OID_TYPE *pRetValOidStr));

VOID UtilCalculateBuddyMemPoolAdjust (UINT4 u4MinBlockSize,
                                      UINT4 u4MaxBlockSize,
                                      UINT4 *pu4MinBlkAdjust,
                                      UINT4 *pu4MaxBlkAdjust);
INT4 CfaShutdown PROTO((VOID));
INT4 CfaModuleStart PROTO ((VOID));
VOID CfaIfmMakeDefaultRouterVlanUp PROTO ((VOID));

INT4 CfaTestIfPipName PROTO((INT4 i4IfIndex, tSNMP_OCTET_STRING_TYPE *));
INT4 CfaGetIfPipName PROTO((INT4 i4IfIndex, tSNMP_OCTET_STRING_TYPE *));
INT4 CfaSetIfPipName PROTO((INT4 i4IfIndex, tSNMP_OCTET_STRING_TYPE *));

INT4 CfaGetNumberOfBebPorts PROTO((VOID));

INT4 CfaSetVipOperStatus PROTO((UINT4 u4VipIfIndex, INT4 i4VipOperStatus));

INT4 CfaGetIfHwAddr PROTO((UINT4 u4IfIndex, UINT1 *pu1HwAddr));
INT4 CfaSetIfPipHwAddr PROTO((UINT4 u4IfIndex, UINT1 *pu1HwAddr, UINT2 u2Count));
INT4 CfaTestIfPipHwAddr PROTO((UINT4 u4IfIndex, UINT1 *pu1HwAddr, UINT2 u2Count));

INT4 CfaCreateVip PROTO ((INT4* pi4IfIndex));

INT4 CfaGetIlanPorts PROTO (( UINT4 u4InIfIndex, tTMO_SLL *pOutInfo));

UINT1 CfaIsVirtualInterface PROTO ((UINT4 ));
UINT1 CfaIsVipInterface PROTO ((UINT4 ));
UINT1 CfaIsILanInterface PROTO ((UINT4 ));
INT4  CfaApiUpdtIvrInterface PROTO ((UINT4 u4IfIndex,UINT1 u1Status));
INT4 CfaValidateIfMainTableIndex PROTO ((INT4));

INT4
CfaIPVXSetIpAddress PROTO ((UINT4 u4IfIndex, UINT4 u4IpAddr, 
                            UINT4 u4IpSubnetMask));

INT4
CfaSetIfMainRowStatus (
        INT4 u4IfIndex,
        INT4 u4RowStatus
        );
INT4
CfaTestv2IfMainRowStatus (
        UINT4 *pu4ErrCode,
        INT4  u4IfIndex,
        INT4 u4RowStatus
        );


INT1
CfaSetIfMainBrgPortType (
        INT4 i4IfMainIndex, 
        INT4 i4SetValIfMainBrgPortType);

INT4 CfaGetEthernetType PROTO((UINT4 u4IfIndex, UINT1 *pu1EthernetType));
#ifdef LNXIP4_WANTED
INT4  CfaGddGetPortName (UINT4, UINT1 *);
#endif
#ifdef MBSM_WANTED
INT4 CfaUpdatePortMacAddress (UINT4 u4Switchid, UINT1 *pu1SwitchHwAdd);
INT4 CfaUpdatePortMacAddressForSlot (UINT4 u4Switchid, UINT1 *pu1SwitchHwAdd);
INT4 CfaIfmBringupAllStackInterfaces (VOID);
#endif
#ifdef WTP_WANTED
INT4 CfaIfmBringupAllRadioInterfaces (INT1 *pi1Dummy);
#endif
INT4 CfaGetIfIndexFromSlot PROTO ((INT4 i4SlotNum, UINT4 u4IfType,
                                   INT4 i4PortNum, UINT4 *pu4IfIndex));

INT4 CfaSetIfBridgedIfaceStatus PROTO ((UINT4 u4IfIndex, UINT1 u1Status));
INT4 CfaIsStackVlanIntf (UINT4 u4IfIndex);

INT4 CfaCheckIsHgIndex (UINT2 u2IfIndex);
UINT1 CfaIsIfEntryProgrammingAllowed (UINT4 u4IfIndex);

UINT1 CfaIsSispInterface (UINT4 u4IfIndex);
UINT1  CfaIsSbpInterface (UINT4 u4IfIndex);

INT4 CfaCopyPhyPortPropToLogicalInterface (UINT4 u4IfIndex);

INT4 CfaCopyPhysicalIfaceProperty (UINT4 u4DstIfIndex, UINT4 u4SrcIfIndex);

INT4 CfaCopyBridgePortTypeToSispPorts (UINT4 u4PhyIfIndex, UINT1 u1BrgPortType);

INT4 CfaSispResetInterfaceProp (UINT4 u4SispIfIndex);

INT1
CfaIPVXSetIfIpSubnetMask (INT4 i4IfMainIndex, UINT4 u4SetValIfIpSubnetMask);
INT1
CfaIPVXSetIfIpBroadcastAddr (INT4 i4IfMainIndex, UINT4 u4SetValIfIpBroadcastAddr);
INT1
CfaIPVXSetIfIpAddr (INT4 i4IfMainIndex, UINT4 u4SetValIfIpAddr);
INT4 CfaUpdateIfaceMapping PROTO ((UINT4 u4IfIndex, UINT4 u4ContextId));
INT4 CfaUpdateVrfInterfaceMac PROTO ((UINT4 u4IfIndex, UINT4 u4ContextId));
INT4 CfaIsAliasNameSet PROTO ((UINT4 u4IfIndex));
UINT4 CfaCliGetFirstIfIndexFromName PROTO ((UINT1 *pu1Alias, UINT4 *pu4Index));
UINT4 CfaCliGetNextIfIndexFromName PROTO ((UINT1 *pu1Alias,
                                           UINT4 u4IfIndex, UINT4 *pu4Index));

UINT1 CfaApiGetBackPlaneStatusForIface (UINT4 u4IfIndex);

INT4 CfaPostPktParamsFromL2 (tCfaBackPlaneParams * pBackPlaneParams);

INT4  CfaUtilSetTnlCxt PROTO ((UINT4 u4ContextId));
VOID  CfaUtilResetTnlCxt PROTO ((VOID));
VOID  CfaIndicateConfigRestoreFail PROTO ((VOID));

INT4
CfaGetIfAutoNegEntry PROTO((UINT4 u4IfIndex, UINT4 u4IfMauIndex, tIfMauStruct *pIfMauEntry));
INT4 CfaRegisterHL PROTO ((tCfaRegParams *pCfaRegParams));
INT4 CfaDeregisterHL PROTO ((UINT2 u2LenOrType));
INT4  CfaValidateRxPacket PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                           UINT4 u4PktSize, tPktHandleInfo * pPktHandleInfo,
                           BOOL1 *pbProcessFlag));

INT4 CfaMuxIwfEnetProcessRxFrame PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                                      UINT4 u4IfIndex, UINT4 u4PktSize,
                                      UINT2 u2IfType, UINT1 u1EnetEncapType));
PUBLIC INT4  
CfaGetIfPortSecState PROTO ((UINT4 u4IfIndex, UINT1 *pu1PortSecState));
PUBLIC INT4 
CfaGetUpLinkPortList PROTO ((UINT4 u4ContextId, UINT2 VlanId, 
                             tPortList PortList));
PUBLIC INT4 
CfaGetTrustedPortList PROTO ((UINT4 u4ContextId, UINT2 VlanId, 
                              tPortList PortList));
PUBLIC INT4 
CfaGetIfPortType PROTO ((UINT4 u4IfIndex, UINT1 *pu1PortType));
PUBLIC INT4
CfaApiSetIfPortSecState PROTO ((UINT4 u4IfIndex, UINT1 u1PortSecState));
VOID 
CfaFreeILanListNodes PROTO ((tTMO_SLL * pList));

INT4
CfaGetInterfaceBrgPortType PROTO ((UINT4 u4IfIndex,INT4 *pi4BrgPortType));
INT4
CfaGetIfUnnumPeerMac (UINT4 u4IfIndex, UINT1 *pu1MacAddr);
INT4
CfaGetIfUnnumAssocIPIf (UINT4 u4IfIndex, UINT4 *pu4AssocIfId);

#ifdef VPN_WANTED
/* Prototypes related to VPNC interface create for RAVPN */
INT4  CfaIfmCreateDynamicVpncInterface PROTO ((UINT4 u4DynamicVpncAddr,
                                        UINT4 u4SubnetMask,UINT4 u4GatewayAddr,
                                        UINT4 u4IpRouteDest,UINT4 u4IpRouteMask));
INT4
CfaIfmDeleteVpncInterface PROTO ((UINT4 u4IfIndex, UINT1 u1DelIfEntry));
#endif

/* Interface create indications */
INT4 CfaSetIfNwType PROTO ((UINT4 u4IfIndex, UINT1 u1NwType));
INT4 CfaGetIfMtu PROTO((UINT4 u4IfIndex, UINT4 *u4IfMtu));
INT4 CfaSetIfIpv4AddressChange PROTO((UINT4 u4IfIndex, UINT1 u1Ipv4AddressChange));
INT4 CfaGetIfIpv4AddressChange PROTO((UINT4 u4IfIndex, UINT1 *pu1Ipv4AddressChange));
INT4 CfaGetIfNwType PROTO ((UINT4 u4IfIndex, UINT1 *pu1IfNwType));
INT4 CfaGetIfWanType PROTO ((UINT4 u4IfIndex, UINT1 *pu1IfWanType));
INT4 CfaSetIfWanType PROTO ((UINT4 u4IfIndex, UINT1 u1WanType));
INT4 CfaGetHLIfIndex(UINT4 u4IfIndex, UINT4 *pu4HLIfIndex);
INT4 CfaGetLLIfIndex(UINT4 u4IfIndex, UINT4 *pu4LLIfIndex);
INT4 CfaHandlePktFromSec PROTO((tCRU_BUF_CHAIN_HEADER * pBuf));
INT4
CfaHandlePktFromSecKern PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf));
/* Prototypes related to Secure Firewall */
INT4 CfaUtilRegisterCallBack PROTO ((UINT4 u4Event, tFsCbInfo *pFsCbInfo));
INT4 CfaCheckIfLocalMac PROTO((tEnetV2Header *pEthHdr,UINT4 u4IfIndex));
INT4 CfaUtilGetInterfaceIndexFromAlias PROTO ((UINT1 *pu1IfName, UINT4 *pu4IfIndex));
INT4 CfaGetSecVlanId PROTO((UINT4 u4IvrIndex, tMacAddr pMacAddress, UINT2 *pVlanId));

INT4 CfaCreateAndWaitPhysicalInterface (UINT1 *pu1IfName,
                            UINT1 *pu1IfNum, UINT4 *pu4IfIndex);
INT4 CfaActivatePhysicalInterface (UINT4 u4IfIndex);
INT4 CfaGetInterfaceNameFromIfType (INT1 *pi1IfName, UINT4 u4IfType);
INT4 CfaDeleteInterfaceExt (UINT4 u4IfIndex);
INT4 CfaIwfMainACValidate (UINT4 u4IfIndex, UINT2 u2VlanId, UINT4 *u4ACIfIndex);
INT4 CfaIfmSetIfSpeed (UINT4 u4IfIndex, UINT4 u4Speed, UINT1 u1Flag);
INT4 CfaSetIfMainStorageType PROTO ((UINT4 u4IfIndex, INT4 i4StorageType));
INT4 CfaGetIfMainStorageType PROTO ((UINT4 u4IfIndex, INT4 *pi4IfMainStorageType));
#if defined (WLC_WANTED) || defined (WTP_WANTED) 
INT4 CfaIwfWssProcessRxFrame PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex, 
                            UINT4 u4PktSize));
INT4 CfaIwfWssProcessTxFrame PROTO((tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4IfIndex, 
                                    UINT4 u4PktSize));

UINT4 CfaProcessWssIfMsg PROTO ((UINT1 u1OpCode, tCfaMsgStruct *pCfaMsgStruct));

INT4 CfaWssProcessRxFrame PROTO ((UINT4 u4IfIdx, tCRU_BUF_CHAIN_HEADER * pBuf, 
                                  UINT2 u2VlanId));

#ifdef WTP_WANTED
INT4 CfaTxToWssWlanPorts PROTO ((UINT1 *pu1DataBuf, UINT4 u4IfIndex, 
                                 UINT4 u4PktSize));
INT4
CfaHandlePacketFromWss (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex);
#endif
#endif
#ifdef HDLC_WANTED
INT4
CfaValidateHdlcPacket (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4PktSize,
                       UINT4 *pu4HdlcIndex);
#endif
typedef enum
{
    CFA_CUST_FWL_CHECK_EVENT= 1,
    CFA_CUST_MAX_CALL_BACK_EVENTS

}eCfaUtilCallBackEvents;
/* Registration Ids which applications can use to register with CFA for packet offloading */
enum _CfaPeriodicPktTxApp {
    CFA_APP_RM        = 0,
    CFA_APP_ICCH       ,
    CFA_MAX_APPLNS     
};
typedef struct {
    VOID  (*SendToApplication) (VOID);
}tRegPeriodicTxInfo;

VOID CfaPeriodicPktTxTsk (INT1 *pArg);
VOID CfaPacketTxTask (INT1 *pArg);
INT4 CfaPacketTxLock (VOID);
INT4 CfaPacketTxUnLock (VOID);
VOID CfaRegisterPeriodicPktTx (UINT4 u4AppId,tRegPeriodicTxInfo *pRegnInfo);
VOID CfaDeRegisterPeriodicPktTx (UINT4 u4AppId);
INT4 CfaDetermineSpeed (UINT4 u4IfIndex, UINT4 *pu4IfSpeed, UINT4 *pu4IfHighSpeed);
INT4 CfaGetIfAutoNegStatus PROTO((INT4 i4IfIndex, INT4 i4IfMauIndex, INT4 *i4Status));

INT4 CfaIsOpenflowPort (UINT4 u4IfIndex);
INT4 CfaCreateOpenflowVlan(UINT4 u4VlanId, UINT4 *pu4IfIndex);
INT4 CfaDeleteOpenflowVlan(UINT4 u4VlanId , UINT4 *pu4IfIndex);
BOOL1 CfaIsThisOpenflowVlan (UINT4 u4VlanId);
#ifdef OPENFLOW_WANTED
INT4 CfaHandlePktFromOfc (tCRU_BUF_CHAIN_HEADER *pBuf);
#endif /* OPENFLOW_WANTED */




#ifdef BCMX_WANTED
INT4 CfaOvrwriteEtherTypeBasedOnBridgeMode PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf));
#endif
VOID 
CfaGetAddrAllocMethodForSecIp (UINT4 u4IfIndex, UINT4 u4SecondaryIp,
                               UINT1 *pu1AddrAllocMethod);

INT4
CfaIpIfSetSecondaryAddressInfoWr (UINT4 u4IfIndex, UINT4 u4IpAddr,
                                  UINT4 u4NetMask, UINT4 u4Flag);
INT4 CfaGetStandbyNodeCount (VOID);

INT4 CfaGetPeerNodeCount (VOID);

UINT4 CfaGetRetrieveNodeState (VOID);

UINT4 CfaCheckCentralizedProto (tMacAddr DestAddr);

INT4 CfaIsPortVlanExists PROTO ((UINT2));

INT4
CfaGddPortWrite (UINT1 *pu1DataBuf, UINT2 u2IfIndex, UINT4 u4PktSize);

#ifdef NPAPI_WANTED
INT4
CfaPostPacketToNP( tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                  UINT4 u4PktSize);

#endif

#ifdef VXLAN_WANTED
INT4
CfaHandlePktFromVxlan (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                       UINT1 u1Direction, UINT1 u1PktType);
#endif
#ifdef LNXIP4_WANTED
#ifdef VRF_WANTED
UINT4 CfaUpdatePortDescforIndex(UINT4 u4IfIndex,INT4 i4PortDesc);
UINT4 CfaVrfUpdateInterfaceSockInfo (UINT4 u4IfIndex,INT4 i4PortDesc);
#endif
#endif
INT4 CfaCreateIcchIPInterface (UINT2 u2InstanceId);
VOID CfaDeleteIcchIPInterface (UINT4 u4IcchIfIndex, UINT4 u4L3IfIndex, UINT4 u4VlanId);
INT4 CfaUfdValidateAddPortList PROTO ((tPortList IfPortList , UINT4 u4UfdGroupId));
#ifdef DHCPC_WANTED
VOID CfaDhcpReleaseUpdate (UINT4 u4IfIndex);
#endif
INT4
CfaCreateSChannelInterface (UINT4 u4UapIfIndex, UINT4 u4IfIndex , UINT1 u1Status);
INT4
CfaCreateDynamicSChannelInterface (UINT4 u4UapIfIndex, UINT4 u4SChIfIndex);

INT4
CfaDeleteSChannelInterface (UINT4 u4SChIfIndex);
INT4
CfaDeleteDynamicSChannelInterface (UINT4 u4SChIfIndex);
INT4
CfaUpdateSChannelOperStatus  (UINT4 u4SchIfIndex , UINT1 u1OperStatus);
INT4
CfaGetFreeSChIndexForUap (UINT4 u4UapIfIndex, UINT4 *pu4Index);

INT4 CfaApiSetInterfaceMtu (UINT4 u4IfIndex, UINT4 u4IfMtu);

/* Prototype of function to get/set ipv6 addr for an interface */
INT4
CfaGetIfIp6Addr (UINT4 u4IfIndex, tIpConfigInfo * pIpConfigInfo);
INT4
CfaSetIfIp6Addr (UINT4 u4IfIndex, tIp6Addr * pIp6Addr, INT4 i4PrefixLen, INT4 i4Command);

INT4
CfaFsbGddEthWrite (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 u4PktSize);

VOID
CfaUpdateIcclFdbEntry (UINT1 *pu1HbPeerMac, UINT4 u4IcchIfIndex, UINT1 u1Action);

INT4
CfaIfGetIpAddress (UINT4 u4IfIndex, UINT4 *pu1IpAddress);

UINT4
CfaIsParentPort (UINT4 u4ParentIfIndex);

INT4
CfaNotifyToL3SubIfVlanIndexInCxt(UINT2 u2PortId,UINT4 u4L2ContextId, tVlanIfaceVlanId VlanId,
        UINT1 u1OperStatus);

INT4
CfaVlanIsMemberPort (UINT4 u4VlanId, UINT4 u4Index);

UINT4 
CfaIsL3SubIfVLAN (UINT2 u2VlanId);
 
INT4
CfaVlanValidateL3SubIf (UINT4 u4IfIndex, UINT2 u2VlanId);

UINT4
CfaVlanOperDownForSubIf (UINT2 u2VlanId);


INT4 CfaDuplicateCruBuffer PROTO((tCRU_BUF_CHAIN_HEADER *pBuf,
                                   tCRU_BUF_CHAIN_HEADER **pDupBuf,
                                   UINT4 u4PktSize));


VOID CfaInterfaceHLPortOperIndication (UINT4 u4IfIndex, UINT1 u1OperStatus);

VOID
CfaInterfaceLAPortCreateIndication (UINT4 u4IfIndex, UINT1 u1OperStatus);

VOID CfaSetInterfaceLAPortCreateIndication (UINT4 u4IfIndex, UINT1 u1OperStatus);

INT4 CfaSetInterfaceHLPortOperIndication (UINT4 u4IfIndex, UINT1 u1OperStatus);

INT4
CfaApiSetInterfaceAdminStatus (UINT4 u4IfIndex, UINT4 u4AdminStatus);

#endif /* _CFA_H */
