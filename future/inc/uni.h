/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: uni.h,v 1.2 2007/11/01 11:52:11 iss Exp $
 *
 * Description: This file contains all global variables used by
 *              RSTP and MSTP modules. 
 *
 *******************************************************************/


#ifndef _CFAUNI_H_
#define _CFAUNI_H_


#define   CFA_MAX_LENGTH_UNI_NAME             64

typedef struct CfaUniBwProfile
{
    UINT2                   u2CirMultiplier;
    UINT2                   u2EirMultiplier;
    UINT1                   u1Cm;
    UINT1                   u1Cf;
    UINT1                   u1PerCosBit;
    UINT1                   u1CirMagnitude;
    UINT1                   u1CbsMagnitude;
    UINT1                   u1CbsMultiplier;
    UINT1                   u1EirMagnitude;
    UINT1                   u1EbsMagnitude;
    UINT1                   u1EbsMultiplier;
    UINT1                   u1PriorityBits;
    UINT1                   au1Reserved[2];
}tCfaUniBwProfile;

typedef struct CfaUniInfo
{
    tCfaUniBwProfile           UniBwProfile;
    UINT4                u4IfIndex;
    UINT1                au1UniName[CFA_MAX_LENGTH_UNI_NAME];
    UINT1                u1CeVlanMapType;
    UINT1                u1ChangedFlag;
    UINT1                u1Reserved[2];
}tCfaUniInfo;


INT4 CfaGetUniInfo(UINT2 u2PortNo,tCfaUniInfo *pCfaUniInfo);
VOID CfaUpdateUniInfo(tCfaUniInfo CfaUniInfo);

#endif  /*_CFAUNI_H_*/
