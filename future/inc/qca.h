/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: qca.h,v 1.16 2016/10/03 10:34:42 siva Exp $
 *
 * Description:This file contains the system sizing object  
 *             definition for the entire system            
 *
 *******************************************************************/
#ifndef _QCA_SIZE_H
#define _QCA_SIZE_H

/*****************************************************************************/
/*    FILE  NAME            : qca.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        :                                                */
/*    MODULE NAME           :                                                */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Linux                                          */
/*    DATE OF FIRST RELEASE :                                        */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains sizing parameters for the   */
/*                            Linux environment                              */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/

#define SYSTEM_DEFAULT_CONTEXT  0 
#ifdef MI_WANTED
#define SYS_DEF_MAX_RADIO_INTERFACES        0
#define SYS_DEF_MAX_PHYSICAL_INTERFACES  \
(SYS_DEF_MAX_DATA_PORT_COUNT + SYS_DEF_MAX_INFRA_SYS_PORT_COUNT+SYS_DEF_MAX_RADIO_INTERFACES)
/* #define SYS_DEF_MAX_DATA_PORT_COUNT 1000 *//* Max No of Front Panel Ports */
#define SYS_DEF_MAX_DATA_PORT_COUNT 28 /* Max No of Front Panel Ports */
#define SYS_DEF_MAX_INFRA_SYS_PORT_COUNT 0 /* Max No of connecting Ports$
                                              used for Dual Unit Stacking*/
#define SYS_DEF_LOCAL_CPU_PORT           2
#define SYS_DEF_NUM_PHYSICAL_INTERFACES SYS_DEF_MAX_PHYSICAL_INTERFACES
                                         /* Actual number of interfaces
                                          * available in the system. This
                                          * affects the memory pools in the
                                          * modules
                                          */
#define SYS_DEF_MAX_NUM_CONTEXTS        256
#define SYS_DEF_MAX_PORTS_PER_CONTEXT   BRG_MAX_PHY_PLUS_LOG_PORTS
#define SYS_DEF_MAX_SISP_IFACES         255
#else
#define SYS_DEF_MAX_RADIO_INTERFACES        0
#define SYS_DEF_MAX_PHYSICAL_INTERFACES \
(SYS_DEF_MAX_DATA_PORT_COUNT + SYS_DEF_MAX_INFRA_SYS_PORT_COUNT+SYS_DEF_MAX_RADIO_INTERFACES)
#define SYS_DEF_MAX_DATA_PORT_COUNT 28 /* Max No of Front Panel Ports */
#define SYS_DEF_MAX_INFRA_SYS_PORT_COUNT 0 /* Max No of connecting Ports$
                                              used for Dual Unit Stacking*/
#define SYS_DEF_LOCAL_CPU_PORT           2 
#define SYS_DEF_NUM_PHYSICAL_INTERFACES SYS_DEF_MAX_PHYSICAL_INTERFACES
#define SYS_DEF_MAX_SISP_IFACES         0
#ifdef VRF_WANTED
#define SYS_DEF_MAX_NUM_CONTEXTS        32
#else
#define SYS_DEF_MAX_NUM_CONTEXTS        1
#endif
#define SYS_DEF_MAX_PORTS_PER_CONTEXT   BRG_MAX_PHY_PLUS_LOG_PORTS
#endif

#define SYS_DEF_MAX_ENET_INTERFACES      SYS_DEF_MAX_PHYSICAL_INTERFACES - \
                                        SYS_DEF_MAX_RADIO_INTERFACES

#define VLAN_MAX_MAC_MAP_ENTRIES              200 /* applicable for MAC based VLAN */

/* PBB Specific */
#ifdef PBB_WANTED
#define   SYS_DEF_MAX_ILAN_IFACES        100
#define   SYS_DEF_MAX_INTERNAL_IFACES    100
#define   SYS_DEF_MAX_VIP_IFACES         5000
#define PBB_MAX_NUM_OF_ISID       2048
#define PBB_MAX_NUM_OF_ISID_PER_CONTEXT    1024
#define PBB_MAX_NUM_OF_PORT_PER_ISID_PER_CONTEXT    128
#define PBB_MAX_NUM_OF_PORT_PER_ISID    50
/* Num of Service Instances required for I-Compoents 
 *  * Number of ISIDs for B bridges without I-Components, should be
 *   * reduced from this macro "PBB_MAX_NUM_OF_ICOMP_ISID". */
#define   PBB_MAX_NUM_OF_ICOMP_ISID       PBB_MAX_NUM_OF_ISID

/* Num of Service Instances required for B-Compoents 
 *  * Number of ISIDs for I bridges without B-Component, should be
 *   * reduced from this macro "PBB_MAX_NUM_OF_BCOMP_ISID */
#define   PBB_MAX_NUM_OF_BCOMP_ISID       PBB_MAX_NUM_OF_ISID
#define   PBB_MAX_NUM_OF_VIP              (PBB_MAX_NUM_OF_ICOMP_ISID)
#else
#define   SYS_DEF_MAX_ILAN_IFACES        1
#define   SYS_DEF_MAX_INTERNAL_IFACES    1
#define   SYS_DEF_MAX_VIP_IFACES         1
#endif
/*end of PBB*/


#ifdef MPLS_WANTED
#define VLAN_DEV_MAX_VFI_ID       0
#define VLAN_MAX_NUM_VFI_IDS      0
#else
#define VLAN_DEV_MAX_VFI_ID       0
#define VLAN_MAX_NUM_VFI_IDS      0
#endif

#define VLAN_VFI_MIN_ID           (VLAN_DEV_MAX_VLAN_ID - VLAN_DEV_MAX_VFI_ID)
#define VLAN_VFI_MAX_ID           VLAN_DEV_MAX_VLAN_ID

#define VLAN_MAX_PORTS_IN_SYSTEM        1000 

/* The macro VLAN_DEV_MAX_NUM_VLAN needs to be tuned in order to scale the 
 * number of vlans in the system to a higher value. Along with tuning this
 * macro, the macro VLAN_PORT_MEM_MAX_VLAN present in inc/params.h also 
 * needs to be tuned appropriately to achieve the scaled VLAN number
 */
#define VLAN_DEV_MAX_NUM_VLAN           4094
#define VLAN_MAX_VLAN_ID                VLAN_DEV_MAX_VLAN_ID
#define VLAN_DEV_MAX_VLAN_ID            4094
#define ISS_DEFAULT_STACK_PORT_COUNT    0
#define VLAN_DEV_MAX_CUSTOMER_VLAN_ID   4096

#define VLAN_MAX_PRIMARY_VLANS          256
#define VLAN_MAX_ISOLATED_VLANS         256
#define VLAN_MAX_COMMUNITY_VLANS        256


#define VLAN_DEV_MAX_NUM_COSQ           8     /* Maximum number of hardware 
            * Queues
            */
/*
 * if h/w supports portlist it is set to VLAN_TRUE and otherwise 
 * set to VLAN_FALSE.If portlist is not supported whenever the 
 * vlan member ports are modified we have to set/reset the 
 * added/deleted ports specificallly.
 * Will be used only for NPAPI ported code.
 */

#define VLAN_HW_PORTLIST_SUPPORTED()   VLAN_TRUE

#define VLAN_MAX_MAC_MAP_ENTRIES      200 /* applicable for MAC based VLAN */

#define VLAN_MAX_SUBNET_MAP_ENTRIES   200 /* applicable for Subnet based VLAN */

#define VLAN_MAX_VID_SET_ENTRIES_PER_PORT 10

#ifdef MBSM_WANTED
#define MBSM_SLOT_ID_FILE               "nodeid"
#endif

/* STP Sizing Params */ 
/* 
  This parameter(AST_MAX_PORTS_IN_SYSTEM) indicates the maximum 
  number of ports on which STP protocol can be enabled. This macro 
  is tunable and to  be tuned according to scalability requirements. 

  Few examples: 
    a) In case of customer bridge, this parameter can be tuned as 
       BRG_MAX_PHY_PLUS_LOG_PORTS.
    b) In case of PEB, this parameter can be tuned as 
            (AST_MAX_PORTS_PER_CONTEXT + AST_MAX_CEP_IN_PEB + 
             (AST_MAX_CEP_IN_PEB*AST_MAX_SERVICES_PER_CUSTOMER)).
*/
#define AST_MAX_PORTS_IN_SYSTEM               1000 
#define AST_MAX_CEP_IN_PEB                    8

#ifdef MSTP_WANTED
#define AST_MAX_MST_INSTANCES         (64 + 1) /* no. of instance + CIST */
#else
/*When MSTP_WANTED is disabled more than one ERPS vlan group can not be configured
 *so this macro value is changed to (64 + 1)*/
#define AST_MAX_MST_INSTANCES         (64 + 1) /* no. of instance + CIST */
#endif /*MSTP_WANTED */

#define AST_MAX_PVRST_INSTANCES          VLAN_DEV_MAX_NUM_VLAN  /* Vlans Supported in PVRST */

#define AST_MAX_INSTANCES   (AST_MAX_MST_INSTANCES > AST_MAX_PVRST_INSTANCES)? AST_MAX_MST_INSTANCES : AST_MAX_PVRST_INSTANCES


#define SNOOP_MAX_IGS_PORTS               1000

#ifdef ELPS_WANTED
#define ELPS_SYS_MAX_PG                      1024
#define ELPS_SYS_MAX_NUM_SERVICE_IN_LIST     4094
#define ELPS_SYS_MAX_NUM_SERVICE_PTR_IN_LIST 2048
#endif

#ifdef PB_WANTED

#define  VLAN_MAX_VID_TRANSLATION_ENTRIES   1024
#define  VLAN_MAX_ETHER_TYPE_SWAP_ENTRIES   1024
#define SVLAN_PORT_CVLAN_MAX_ENTRIES          1024
#define     CVLAN_MAX_ENTRIES_PER_PORT SVLAN_PORT_CVLAN_MAX_ENTRIES
#define SVLAN_PORT_SRCIP_DSTIP_MAX_ENTRIES    1024
#define SVLAN_PORT_DSTIP_MAX_ENTRIES          1024
#define SVLAN_PORT_SRCIP_MAX_ENTRIES          1024
#define SVLAN_PORT_DSCP_MAX_ENTRIES           1024
#define SVLAN_PORT_DSTMAC_MAX_ENTRIES         1024
#define SVLAN_PORT_SRCMAC_MAX_ENTRIES         1024
#define SVLAN_PORT_CVLAN_DSTIP_MAX_ENTRIES    1024
#define SVLAN_PORT_CVLAN_DSCP_MAX_ENTRIES     1024
#define SVLAN_PORT_CVLAN_SRCMAC_MAX_ENTRIES   1024
#define SVLAN_PORT_CVLAN_DSTMAC_MAX_ENTRIES   1024

#endif /*PB_WANTED*/
#define AST_MAX_SERVICES_PER_CUSTOMER   128

#define IP_DEV_MAX_TNL_INTF             20  /* Maximum IP interfaces */
#define IP_DEV_MAX_L3VLAN_INTF          100  /* Maximum IP interfaces */
#define SYS_DEF_MAX_L3SUB_IFACES        64 

#define LA_DEV_MAX_TRUNK_GROUP          8    /* Maximum Trunk groups */  

#define LA_MIN_PORT_INDEX_PER_AGG SYS_DEF_NUM_PHYSICAL_INTERFACES + 1
#define LA_MAX_PORT_INDEX_PER_AGG SYS_DEF_NUM_PHYSICAL_INTERFACES + LA_DEV_MAX_TRUNK_GROUP

#ifdef QCA_SILVER_TARGET 
#define LA_MAX_PORTS_PER_AGG            8    /* Maximum no.of ports in 
                         * aggregation 
           */
#else
#define LA_MAX_PORTS_PER_AGG            4
#endif

/* Vlan filtering entry sizing */

/* Maximum number of L2 static unicast entries -
 * This number includes the TE static unicast entries also */
#ifdef QCA_WANTED
#define VLAN_DEV_MAX_ST_UCAST_ENTRIES   1024
#else
#define VLAN_DEV_MAX_ST_UCAST_ENTRIES   1100
#endif
/* Maximum number of L2 dynamic unicast entries -
 * Assuming the number of learnt unicast entries to be 1000 */
#define VLAN_DEV_MAX_L2_TABLE_SIZE      2000
/* Maximum number of L2 multicast entries -
 * This number includes the TE static multiicast entries also */
#define VLAN_DEV_MAX_MCAST_TABLE_SIZE   32

#define VLAN_DEF_DYN_UCAST_ENTRIES_PER_VLAN   150 /* Default value of Limit  
                                     * of Dynamic unicast entries that can be 
                                     * learnt per vlan. 10% of the maximum
                                     * L2 unicast entries per switch.
                                     */

#define ISS_MAX_L2_FILTERS                  256
#define ISS_MAX_L3_FILTERS                  256
#define ISS_MAX_L4S_FILTERS      20
#define ISS_MAX_UDB_FILTERS             500
#define ISS_FILTER_SHADOW_MEM_SIZE       4  /* Filter Shadow Size */
#define RATE_LIMIT_MAX_VALUE            0xfa000
#define IP_DEV_MAX_ADDR_PER_IFACE        5   /* Maximum IP address associated 
                                               with the interface */
#define VLAN_MAX_L2VPN_ENTRIES          MAX_L2VPN_PW_VC_ENTRIES

#define ISS_MAX_METERS                  1024

/* MPLS */
#ifdef MPLS_WANTED
/* Total L3 interfaces available in h/w */
#define IP_DEV_MAX_IP_INTF              4096

/* IP interfaces remaining after usage by L3IPVLAN type interfaces */
#define IP_DEV_MAX_REM_L3_IP_INTF       IP_DEV_MAX_IP_INTF -  \
                                        SYS_DEF_MAX_MPLS_IFACES

#define SYS_DEF_MAX_L2_PSW_IFACES       100

#else
#define IP_DEV_MAX_IP_INTF              IP_DEV_MAX_L3VLAN_INTF + IP_MAX_RPORT
#define SYS_DEF_MAX_L2_PSW_IFACES       0
#endif

/* ARP Table table Size  */
#define IP_DEV_MAX_IP_TBL_SZ            1000 

/* Maximum Route entries. Route entries in the FIB(IP_DEV_MAX_ROUTE_TBL_SZ) depends on
 * the routes from different routing protocols.Route entries from various protocols 
 * should be tuned based on the limitation in H/W. */
#define RIP_DEF_MAX_ROUTES              1000
#define OSPF_DEF_MAX_ROUTES             MAX_OSPF_RT_ENTRIES
#define BGP_DEF_MAX_ROUTES              2500 
#define IP_DEF_MAX_STATIC_ROUTES         100
#define IP_MAX_INTERFACE_ROUTES        ((IP_DEV_MAX_IP_INTF) * IP_DEV_MAX_ADDR_PER_IFACE)
                                        /* By default 128 * 5 */
                                        
#define IP_DEV_MAX_ROUTE_TBL_SZ         RIP_DEF_MAX_ROUTES + OSPF_DEF_MAX_ROUTES + \
                                        BGP_DEF_MAX_ROUTES + IP_DEF_MAX_STATIC_ROUTES + IP_MAX_INTERFACE_ROUTES

                                        
/* L3 multicast table size */ 
#define IP_DEV_L3MCAST_TABLE_SIZE       256
                                        
/*DIFFSERV*/

/*
 * For 5690 MinRefresCount = 1 Mbps, MaxRefresCount = 1023 Mbps
 * For 5695, Firebolt MinRefresCount = 64 Kbps, MaxRefresCount = 1048576 Kbps
 * */
#define NP_DIFFSRV_MIN_REFRESH_COUNT 64   /* Kb/s */
#define NP_DIFFSRV_MAX_REFRESH_COUNT 1048576 /* (1024*1024)Kb/s */

#define   SYS_DEF_MAX_NBRS                  10


/* TCP */
#define   TCP_DEF_MAX_NUM_OF_TCB            500

/* PIM */
#define   PIM_DEF_MAX_SOURCES               100
#define   PIM_DEF_MAX_RPS                   100

/* DVMRP */
#define   DVMRP_DEF_MAX_SOURCES             100

/* OSPF */
#define   OSPF_DEF_MAX_LSA_PER_AREA         100 
#define   OSPF_DEF_MAX_EXT_LSAS             2500 

/* DHCP Server */

#define DHCP_SRV_MAX_POOLS                  5
#define DHCP_SRV_MAX_HOST_PER_POOL          20

/* CLI and Telnet */

#define CLI_MAX_SESSIONS                    10
#define CLI_MAX_USERS                       20
#define CLI_MAX_GROUPS                      10

/* SSL and Web */

#define ISS_MAX_WEB_SESSIONS                10
#define ISS_MAX_SSL_SESSIONS                10

/* Load Sharing */
enum
{
    MBSM_LOAD_SHARING_ENABLE = 1,
    MBSM_LOAD_SHARING_DISABLE = 2
};


#define WLAN_MAX_STATIONS                         64

#define SYS_DEF_MAX_EVC_PER_PORT           64
#define SYS_MAX_NUM_EVCS_SUPPORTED         256


/* LLDP Module specific sizing parameters */
#define LLDP_MAX_LOC_MAN_ADDR               (IP_DEV_MAX_L3VLAN_INTF + IP_MAX_RPORT)
 
#define LLDP_MAX_LOC_PROTOID                1  /* As protocol identity tlv is
                                                * not supported this macro is 
                                                * defined with minimum value.
                                                * Need to increase the value 
                                                * when there is support.i.e,
                                                * it should be LLDP_MAX_PORTS *
                                                * Number of protocols supported
                                                * on a port.
                                                */
#define LLDP_MAX_LOC_PPVID                  240   /* 10 PPVIDs per port for 
                                                     24 port system */
#define LLDP_MAX_LOC_VLAN                   VLAN_DEV_MAX_NUM_VLAN

#define LLDP_MAX_NEIGHBORS                  256  /* Maximum number of neighbors 
                                                    that can be learnt */
#define LLDP_MAX_REM_MAN_ADDR               256  /* Stores at least 1 mgmt 
                                                    address per neighbor */
#define LLDP_MAX_REM_UNKNOWN_TLV            10   /* Stores a total of 50 
                                                    unknown TLV information */
#define LLDP_MAX_REM_ORG_DEF_INFO_TLV       10   /* Stores a total 50 org 
                                                    defined unkown TLVs */
#define LLDP_MAX_REM_PROTOID                1  /* As protocol identity tlv is
                                                * not supported this macro is 
                                                * defined with minimum value.
                                                * Need to increase the value 
                                                * when there is support.i.e,
                                                * the value can be set to
                                                * 10 protocol id entries 
                                                * for each neighbor */
#define LLDP_MAX_REM_PPVID                  256  /* 1 PPVID per neighbor */
#define LLDP_MAX_REM_VLAN                   2560 /* 10 VLAN per neighbor */


/* QoS Tunable Parameters */
/* Refer  QoS Mib and qosxtd.h for MACROS  Values */
#define QOS_PRI_MAP_TBL_MAX_ENTRIES        (100)
#define QOS_CLS_MAP_TBL_MAX_ENTRIES        (100)
#define QOS_CLS2PRI_MAP_TBL_MAX_ENTRIES    (100)
#define QOS_METER_TBL_MAX_ENTRIES          (100)
#define QOS_PLY_MAP_TBL_MAX_ENTRIES        (100)
#define QOS_Q_TEMP_TBL_MAX_ENTRIES         (10)
#define QOS_RD_TBL_MAX_ENTRIES             (10)
#define QOS_SCHED_TEMP_TBL_MAX_ENTRIES     (100)
#define QOS_SHAPE_TEMP_TBL_MAX_ENTRIES     (100)
#ifdef ISS_METRO_WANTED
#define QOS_Q_MAP_TBL_MAX_ENTRIES          (1500)
#define QOS_Q_TBL_MAX_ENTRIES              (1500)
#define QOS_SCHED_TBL_MAX_ENTRIES          (1500)
#define QOS_HIERARCHY_TBL_MAX_ENTRIES      (1500)
#else
#define QOS_Q_MAP_TBL_MAX_ENTRIES          (100)
#define QOS_Q_TBL_MAX_ENTRIES              (100)
#define QOS_SCHED_TBL_MAX_ENTRIES          (100)
#define QOS_HIERARCHY_TBL_MAX_ENTRIES      (100)
#endif

#define QOS_MAX_NUM_OF_CLASSES             (100)
#define QOS_MAX_REGEN_INNER_PRIORITY_VAL    (7)
/*QoS Granularity Value */
#define QOS_RATE_UNIT                      (QOS_RATE_UNIT_KBPS)
#define QOS_RATE_GRANULARITY               (64)

/* Default Values for QoS Table Entries */
#define QOS_CLS_DEFAULT_PRE_COLOR          (QOS_CLS_COLOR_NONE) 
#define QOS_PLY_DEFAULT_PHB_TYPE           (QOS_PLY_PHB_TYPE_NONE)
#define QOS_PLY_DEFAULT_PHB_VAL             0
#define QOS_Q_TEMP_SIZE_DEFAULT             10000
#define QOS_Q_TEMP_DROP_TYPE_DEFAULT        QOS_Q_TEMP_DROP_TYPE_TAIL
#define QOS_Q_TEMP_DROP_ALGO_DEFAULT        QOS_Q_TEMP_DROP_ALGO_DISABLE
#define QOS_Q_TEMP_DROP_ALGO_DEFAULT        QOS_Q_TEMP_DROP_ALGO_DISABLE
#define QOS_Q_TABLE_WEIGHT_DEFAULT          1
#define QOS_Q_TABLE_PRIORITY_DEFAULT        1

#define QOS_RD_CFG_MIN_AVG_TH_DEFAULT       10000 
#define QOS_RD_CFG_MAX_AVG_TH_DEFAULT       50000
#define QOS_RD_CFG_MAX_PKT_SIZE_DEFAULT      1000
#define QOS_RD_CFG_MAX_PROB_DEFAULT             1
#define QOS_RD_CFG_EXP_WEIGHT_DEFAULT           0
#define QOS_RD_CFG_GAIN_DEFAULT                 0
#define QOS_RD_CFG_DROP_THRESH_TYPE_DEFAULT     QOS_RD_CONFIG_DROP_THRESH_TYPE_PKTS
#define QOS_RD_CFG_ECN_THRESH_DEFAULT           0
#define QOS_RD_CFG_ACTION_FLAG_DEFAULT          0

#define QOS_SHAPE_TEMP_CIR_DEFAULT          1024000
#define QOS_SHAPE_TEMP_CBS_DEFAULT          1024000
#define QOS_SHAPE_TEMP_EIR_DEFAULT          1024000
#define QOS_SHAPE_TEMP_EBS_DEFAULT          1024000

#define QOS_SCHED_Q_COUNT_DEFAULT               4
#define QOS_SCHED_ALGO_DEFAULT                  1
#define QOS_QMAP_DEFAULT_QMAPTYPE           QOS_QMAP_TYPE_CLASS

/*Hierarchical scheduling constants */
#define QOS_SCHED_HL_DEFAULT               0
#define QOS_QUEUE_ENTRY_MAX               VLAN_DEV_MAX_NUM_COSQ
#define QOS_QUEUE_MAX_NUM_UCOSQ            8
#define QOS_QUEUE_MAX_NUM_MCOSQ            0
#define QOS_QUEUE_MAX_NUM_SUBQ             0

/* QOS_DEF_HL_SCHED_CONFIG_SUPPORT Indicates whether Hierarchical Scheduling 
 * needs to be setup by default,The following macros take effect only this 
 * is enabled (1) */
#define QOS_DEF_HL_SCHED_CONFIG_SUPPORT    0  
#define QOS_HL_DEF_NUM_LEVELS              3 /*No of Hierarchical Scheduler Levels(Default)*/
#define QOS_HL_DEF_NUM_S3_SCHEDULERS       1 /*No of Level3 Schedulers in Hierarchical Scheduling(Default)*/
#define QOS_HL_DEF_NUM_S2_SCHEDULERS       1 /*No of Level2 Schedulers in Hierarchical Scheduling(Default)*/
#define QOS_HL_DEF_NUM_S1_SCHEDULERS       1 /*No of Level1 Schedulers in Hierarchical Scheduling(Default)*/

/* PFC Maximum Profiles */
#define PFC_MAX_PROFILES                    256

#ifdef IP6_WANTED
#define  RMON2_MAX_TUPLE_FLOW_STATS  448
#else /* IP6 not wanted */
#define  RMON2_MAX_TUPLE_FLOW_STATS  512
#endif

#define ISS_MAX_POSSIBLE_STK_PORTS      4
#define ISS_DEFAULT_STACK_PORT_COUNT    0
#define ISS_DEFAULT_STACK_COLD_STANDBY  ISS_COLDSTDBY_DISABLE
#define ISS_MAX_STK_PORTS               IssGetStackPortCountFromNvRam()


#ifdef IKE_WANTED
#define SYS_DEF_MAX_NUM_OF_VPNC                1 
#else
#define SYS_DEF_MAX_NUM_OF_VPNC                0 
#endif

#ifdef TLM_WANTED
#define SYS_DEF_MAX_TELINK_INTERFACES             300
#else
#define SYS_DEF_MAX_TELINK_INTERFACES             0
#endif


#define ECFM_HW_MA_HANDLER_SIZE                4
#define ECFM_HW_MEP_HANDLER_SIZE               4 
#define ECFM_HW_HANDLER_SIZE                   (ECFM_HW_MEP_HANDLER_SIZE / 2)

#define MAX_NUM_OF_AP                   4097
#define NUM_OF_AP_SUPPORTED             4
#define MAX_NUM_OF_RADIO_PER_AP         31
#define MAX_NUM_OF_RADIOS               (NUM_OF_AP_SUPPORTED *\
                                         MAX_NUM_OF_RADIO_PER_AP)
#define MAX_NUM_OF_SSID_PER_WLC         50
#define MAX_NUM_OF_BSSID_PER_RADIO      5
#define MAX_NUM_OF_STA_PER_AP           32
#define MAX_NUM_OF_STA_PER_WLC          (NUM_OF_AP_SUPPORTED *\
                                         MAX_NUM_OF_STA_PER_AP)

#define SYS_DEF_MAX_WSS_IFACES          (MAX_NUM_OF_RADIOS +\
                                         MAX_NUM_OF_SSID_PER_WLC +\
                                                        (MAX_NUM_OF_BSSID_PER_RADIO *\
                                         MAX_NUM_OF_RADIOS))
#define SYS_DEF_MAX_L2_AC_IFACES         0
#define VLAN_HW_STATS_ENTRY_LEN          4

#define   CFA_MAX_GIGA_ENET_MTU   9216
#define   CFA_MAX_L3_INTF_ENET_MTU      9216
#define ISS_TGT_TELNET_SERVER_PORT     6023
#define ISS_TGT_TELNET_SERVER_PORT1    6024

/* Number of S-Channel interfaces. Referring S-Channel interfaces
 * as SBP interfaces */

#ifdef EVB_WANTED
#define SYS_DEF_MAX_SBP_IFACES          0
#else
#define SYS_DEF_MAX_SBP_IFACES          0
#endif

#endif
