/****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: isspi.h,v 1.10 2015/11/11 08:39:35 siva Exp $
 *
 * Description:  This file contains all the exported definitions of PI
 ****************************************************************************/

/*****************************************************************************/
/*    FILE  NAME            : isspi.h                                        */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : Port Isolation Feature                         */
/*    MODULE NAME           : ISS                                            */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Linux                                          */
/*    DATE OF FIRST RELEASE : 21 July 2010                                   */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           :  This header file contains                     */
/*                             (i) data structure definition,                */
/*                             (ii) Macros definition,                       */
/*                             (iii) prototype declarations,                 */
/*                             (iv) extern declarations                      */
/*                             required for Port Isolation feature.          */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    21 July 2010           Initial Creation                        */
/*---------------------------------------------------------------------------*/

#ifndef ISSPI_H
#define ISSPI_H

#include "fsvlan.h"
#include "lldp.h"


/* Macros to identify the well-defined operations on Port Isolation Table */

#define ISS_PI_ADD      1  /* To Add port(s) to Port Isolation Entry */
#define ISS_PI_DELETE   2  /* To Delete port(s) from Port Isolation Entry */
#define ISS_PI_CONFIGURE 3
#define  ISS_PI_REMOVE   4
#define ISS_PI_MODIFY    5

#define ISS_PI_GET_FWD_STATUS 1 /* To get the Port Isolation forwarding status*/


#define LA_PI_ADD      1  /* To Add port(s) to Port Isolation Entry */
#define LA_PI_DELETE   2  /* To Delete port(s) from Port Isolation Entry */


/* The following structure will be used to update the
 * port isolation table*/

typedef struct _IssUpdtPortIsolation
{
   UINT4    u4IngressPort;    /* Source port for which the allowed egress port
                               * is configured. */
   UINT4   *pu4EgressPorts;   /* List of forwarding allowed ports for a given
                               * ingress port. The egress ports are stored in 
                               * ascending order in the array. */
   tPortList ExcludeList;     /* Ports to be excluded for a given ingress port */
   tVlanId InVlanId;          /* Ingress Vlan Id */
   UINT2   u2NumEgressPorts;  /* Ports present in pu4EgressPorts Array */
   UINT1   u1Action;          /* To ADD/DELETE */
   UINT1   u1Reserved[3];
}tIssUpdtPortIsolation;

/* The following structure is used for notifying the MBSM 
 * related changes. 
 */
typedef struct _sIssMbsmInfo{
    tMbsmSlotInfo * pSlotInfo;    /* pointer to slot information */
    UINT1           u1CardAction; /* MBSM_MSG_CARD_INSERT /
                                     MBSM_MSG_CARD_DELETE. */
    UINT1           au1Reserved[3];
}tIssMbsmInfo;

/* The following structure will be used to update the
 * port isolation information in the hardware. 
 */
typedef struct _IssHwUpdtPortIsolation{
   UINT4   u4IngressPort;       /* Source port for which the allowed egress port
                              * is configured. */
   UINT4   au4EgressPorts[ISS_MAX_UPLINK_PORTS]; /* List of forwarding allowed
                                            * ports for a given ingress port. */
   tPortList ExcludeList;     /* Ports to be excluded for a given ingress port */
   tVlanId InVlanId;          /* Ingress VlanId */
   UINT2   u2NumEgressPorts;  /* Ports present in pu4EgressPorts Array */
   UINT1   u1Action;          /* To ADD/DELETE */
   UINT1   au1Reserved[3];

}tIssHwUpdtPortIsolation;

/* The following structure will be used to get the Port Isolation 
 *  information by other modules. 
 */
typedef struct _IssPortIsoInfo
{
   UINT4    u4IngressPort;    /* Source port information. */
   UINT4    u4EgressPort;     /* Egress  port information */
   tVlanId InVlanId;          /* Ingress Vlan Id */
   UINT1   u1Action;          /* Currently ISS_PI_GET_STATUS is allowed. */
   UINT1   au1Reserved[1];    /* Reserved variable */
}tIssPortIsoInfo;

PUBLIC INT4 IssPIMbsmCardInsertPITable
PROTO ((tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo));

PUBLIC INT4 IssPIMbsmCardRemovePITable
PROTO ((tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo));


/* API to update the Port Isolation table */
INT4 IssApiUpdtPortIsolationEntry PROTO ((tIssUpdtPortIsolation *));
INT4 IssApiGetIsolationEgressPorts PROTO ((tIssPortIsoInfo *, UINT4 *));
INT1 IssPIApiIsPIStorageTypeVolatile PROTO ((UINT4, tVlanId));
INT1 IssApiGetIsolationFwdStatus (tIssPortIsoInfo *);
INT4 IssPIInitPortIsolationTable PROTO((VOID));
VOID IssPIDeInitPortIsolationTable PROTO((VOID));
PUBLIC INT1 EntApiGetInventoryInfo PROTO ((tLldpMedLocInventoryInfo* ));

#endif /* ISSPI_H */
