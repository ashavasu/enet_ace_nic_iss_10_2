
/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: dhrlnpwr.h,v 1.2 2015/06/17 04:48:28 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              for DHCP-RELAY wrappers
 *              
 ********************************************************************/

#ifndef __DHRL_NP_WR_H__
#define __DHRL_NP_WR_H__

UINT1 DhrlFsNpDhcpRlyInit PROTO ((UINT4));
UINT1 DhrlFsNpDhcpRlyDeInit PROTO ((UINT4));

#endif /* __DHRL_NP_WR_H__ */
