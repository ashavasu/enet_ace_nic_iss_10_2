/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mldsnpwr.h,v 1.2 2012/07/20 10:34:21 siva Exp $
 *
 * Description: Contains sructure definitions, macros and functions to be used
 *              by  external modules.
 *
 *******************************************************************/
#ifndef __MLDS_NP_WR_H
#define __MLDS_NP_WR_H

#include "snp.h"


/* OPER ID */

#define  FS_MI_MLDS_HW_DISABLE_IP_MLD_SNOOPING                  1
#define  FS_MI_MLDS_HW_DISABLE_MLD_SNOOPING                     2
#define  FS_MI_MLDS_HW_ENABLE_IP_MLD_SNOOPING                   3
#define  FS_MI_MLDS_HW_ENABLE_MLD_SNOOPING                      4
#define  FS_MI_NP_GET_IP6_FWD_ENTRY_HIT_BIT_STATUS              5
#define  FS_MI_NP_UPDATE_IP6MC_FWD_ENTRIES                      6
#ifdef MBSM_WANTED
#define  FS_MI_MLDS_MBSM_HW_ENABLE_IP_MLD_SNOOPING              7
#define  FS_MI_MLDS_MBSM_HW_ENABLE_MLD_SNOOPING                 8
#endif 

/* Required arguments list for the mlds NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

typedef struct {
    UINT4  u4Instance;
} tMldsNpWrFsMiMldsHwDisableIpMldSnooping;

typedef struct {
    UINT4  u4Instance;
} tMldsNpWrFsMiMldsHwDisableMldSnooping;

typedef struct {
    UINT4  u4Instance;
} tMldsNpWrFsMiMldsHwEnableIpMldSnooping;

typedef struct {
    UINT4  u4Instance;
} tMldsNpWrFsMiMldsHwEnableMldSnooping;

typedef struct {
    UINT4         u4Instance;
    UINT1 *       pGrpAddr;
    UINT1 *       pSrcAddr;
    tSnoopVlanId  VlanId;
    UINT1         au1pad[2];  
} tMldsNpWrFsMiNpGetIp6FwdEntryHitBitStatus;

typedef struct {
    UINT1   *     PortList;
    UINT1   *     UntagPortList;
    UINT4         u4Instance;
    UINT1 *       pGrpAddr;
    UINT1 *       pSrcAddr;
    tSnoopVlanId  VlanId;
    UINT1         u1EventType;
    UINT1         au1pad[1];  
} tMldsNpWrFsMiNpUpdateIp6mcFwdEntries;

#ifdef MBSM_WANTED
typedef struct {
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
} tMldsNpWrFsMiMldsMbsmHwEnableIpMldSnooping;

typedef struct {
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
} tMldsNpWrFsMiMldsMbsmHwEnableMldSnooping;

#endif 
typedef struct MldsNpModInfo {
union {
    tMldsNpWrFsMiMldsHwDisableIpMldSnooping  sFsMiMldsHwDisableIpMldSnooping;
    tMldsNpWrFsMiMldsHwDisableMldSnooping  sFsMiMldsHwDisableMldSnooping;
    tMldsNpWrFsMiMldsHwEnableIpMldSnooping  sFsMiMldsHwEnableIpMldSnooping;
    tMldsNpWrFsMiMldsHwEnableMldSnooping  sFsMiMldsHwEnableMldSnooping;
    tMldsNpWrFsMiNpGetIp6FwdEntryHitBitStatus  sFsMiNpGetIp6FwdEntryHitBitStatus;
    tMldsNpWrFsMiNpUpdateIp6mcFwdEntries  sFsMiNpUpdateIp6mcFwdEntries;
#ifdef MBSM_WANTED
    tMldsNpWrFsMiMldsMbsmHwEnableIpMldSnooping  sFsMiMldsMbsmHwEnableIpMldSnooping;
    tMldsNpWrFsMiMldsMbsmHwEnableMldSnooping  sFsMiMldsMbsmHwEnableMldSnooping;
#endif 
    }unOpCode;

#define  MldsNpFsMiMldsHwDisableIpMldSnooping  unOpCode.sFsMiMldsHwDisableIpMldSnooping;
#define  MldsNpFsMiMldsHwDisableMldSnooping  unOpCode.sFsMiMldsHwDisableMldSnooping;
#define  MldsNpFsMiMldsHwEnableIpMldSnooping  unOpCode.sFsMiMldsHwEnableIpMldSnooping;
#define  MldsNpFsMiMldsHwEnableMldSnooping  unOpCode.sFsMiMldsHwEnableMldSnooping;
#define  MldsNpFsMiNpGetIp6FwdEntryHitBitStatus  unOpCode.sFsMiNpGetIp6FwdEntryHitBitStatus;
#define  MldsNpFsMiNpUpdateIp6mcFwdEntries  unOpCode.sFsMiNpUpdateIp6mcFwdEntries;
#ifdef MBSM_WANTED
#define  MldsNpFsMiMldsMbsmHwEnableIpMldSnooping  unOpCode.sFsMiMldsMbsmHwEnableIpMldSnooping;
#define  MldsNpFsMiMldsMbsmHwEnableMldSnooping  unOpCode.sFsMiMldsMbsmHwEnableMldSnooping;
#endif 
} tMldsNpModInfo;


/* Function Prototype for Generic NP OpCode specific wrappers
 * defined in Mldsnputil.c */

UINT1 MldsFsMiMldsHwDisableIpMldSnooping PROTO ((UINT4 u4Instance));
UINT1 MldsFsMiMldsHwDisableMldSnooping PROTO ((UINT4 u4Instance));
UINT1 MldsFsMiMldsHwEnableIpMldSnooping PROTO ((UINT4 u4Instance));
UINT1 MldsFsMiMldsHwEnableMldSnooping PROTO ((UINT4 u4Instance));
UINT1 MldsFsMiNpGetIp6FwdEntryHitBitStatus PROTO ((UINT4 u4Instance, tSnoopVlanId VlanId, UINT1 * pGrpAddr, UINT1 * pSrcAddr));
UINT1 MldsFsMiNpUpdateIp6mcFwdEntries PROTO ((UINT4 u4Instance, tSnoopVlanId VlanId, UINT1 * pGrpAddr, UINT1 * pSrcAddr, tPortList PortList, tPortList UntagPortList, UINT1 u1EventType));
#ifdef MBSM_WANTED
UINT1 MldsFsMiMldsMbsmHwEnableIpMldSnooping PROTO ((UINT4 u4ContextId, tMbsmSlotInfo * pSlotInfo));
UINT1 MldsFsMiMldsMbsmHwEnableMldSnooping PROTO ((UINT4 u4ContextId, tMbsmSlotInfo * pSlotInfo));
#endif 

#endif /* __IGS_NP_WR_H */
