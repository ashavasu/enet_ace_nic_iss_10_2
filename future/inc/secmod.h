/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: secmod.h,v 1.36 2016/07/08 07:34:14 siva Exp $
 *
 * Description:This file contains the macros, data structures and
 *             functions exported by Security Module.
 *******************************************************************/

#ifndef _SECMOD_H_
#define _SECMOD_H_
#include "fsike.h"

/* Used to send Module name to npapi layer */
enum {
        ISS_SEC_FWL_MODULE = 1,
        ISS_SEC_NAT_MODULE,
        ISS_SEC_VPN_MODULE,
        ISS_SEC_IDS_MODULE
};

/* SrcMac[6] + DstMac[6] + VLANTag[4]+ Proto [2] + MTU [1500] */
#define SEC_MAX_PKT_SIZE       CFA_ENET_MTU + \
                               CFA_VLAN_TAGGED_HEADER_SIZE + \
                               CFA_VLAN_PROTOCOL_SIZE + \
                               CFA_ENET_TYPE_OR_LEN + \
                               500 + \
                               CFA_ENET_TYPE_OR_LEN

/* Maximum size of the message buffer used for reading data from 
 * kernel space. 
 */
#define SEC_MAX_MSG_BUF_SIZE   (SEC_MAX_PKT_SIZE +\
                                sizeof (tSecQueMsg) + sizeof (tSecModuleData))
#define SEC_KERN_MAX_MEMPOOLS 100
#define SEC_MAX_LOG_BUF_SIZE  500
#define SEC_MAX_IP_ADDR_LEN    16

/* Indicates the packets that are enqueued to CFA task
 * after Security Processing. 
 */
#define   SEC_TO_CFA                 19

/* Different types of Messages posted from kernel to user space. 
 */
/* SEC_MSG_TYPE_ETH_FRAME identifies the packet destined to ISS and  
 * posted from kernel to user space. 
 */
#define SEC_MSG_TYPE_ETH_FRAME  1
#define SEC_IDS_MSG_TYPE_ETH_FRAME  2
#define SEC_IP_OFFSET           16 


#define SEC_ADD_SEC_IP 1
#define SEC_DEL_SEC_IP 2
/* Security character device FD for 
 * ISS <-> Security kernel communication. 
 * Npsim <-> Security Kernel communication. 
 * SNORT <-> Security Kernel communication.
 */
extern INT4  gi4SecDevFd; 
extern INT4  gi4SysOperMode; 

extern UINT4 gu4IdsStatus;
extern UINT4 gu4PrevIdsStatus;
extern INT4 gi4IdsRulesStatus;
typedef struct
{
        INT4  i4IfIndex;
        INT4  i4L2tpEnabledStatus;
        INT4  i4L2tpEncapType;
        UINT4 u4L2tpBridgeMode;
        UINT2 u2L2tpPvid;
        UINT1 au1Padding[2];
}tSecL2tpPortInfo;

extern INT4 SecApiUpdateL2tpPortInfo(tSecL2tpPortInfo *L2tpKernelPortInfo);
extern INT4 SecApiDeleteAllL2tpPortInfo(VOID);
 
extern INT4
GddKSecUpdateOutTag(UINT1 u1Direction, UINT2 u2DsaVlanId);

extern INT4
GddKSecUpdateFromCpuOutTag(UINT1 u1Direction,UINT2 u2DsaIfIndex);

#define SYS_MAX_WAN_INTERFACES 11 /*Max Layer3 WAN interfaces*/
#define SYS_MAX_WAN_PHY_INTERFACES 20 /* Max Physical interfaces that can be configured and WAN interface*/

/* Denotes the direction of the packet is inbound. */
#define SEC_INBOUND       1
/* Denotes the direction of the packet is outbound. */
#define SEC_OUTBOUND      2

/* Indicates that the packet is stolen by security Module. */
#define SEC_NOT_STOLEN    0
#define SEC_STOLEN        1

#define MAX_SEC_MODULES  6
enum
{
    SEC_USER,          /* Security module operates in PCI bus architecture */ 
    SEC_KERN_USER,      
    SEC_KERN
};

enum
{
 SEC_PKT_TO_CFA_FROM_KERNEL = 1,
 SEC_LOGMSG_TO_FWL_FROM_KERNEL,
 SEC_TRAPMSG_TO_FWL_FROM_KERNEL,
 SEC_MSG_TO_IKE_FROM_KERNEL,
 SEC_ERRMSG_TO_FWL_FROM_KERNEL,
 SEC_MSG_TO_SECV4_FROM_KERNEL,
 SEC_PKT_TO_IDS_FROM_KERNEL,
 SEC_LOGMSG_TO_UTLTRC_FROM_KERNEL,
 SEC_MSG_TO_SECV4_FROM_KERNEL_DUMMY
};

enum
{
    SEC_USR_MODULE = 1,
    SEC_NPSIM_MODULE, 
    SEC_SNORT_MODULE
};



typedef struct _FwlLogMsg
{
 UINT1 au1Msg [SEC_MAX_LOG_BUF_SIZE];
 UINT4 u4AttackType;
 UINT4 u4IfaceNum;
 UINT4 u4LogLevel;
 UINT4 u4Severity;
}tFwlLogMsg;

typedef struct _FwlTrapMsg
{
 UINT4 u4TrapType;
 UINT4 u4IfaceNum;
 UINT4 u4PktCount;
 UINT1 u1NodeName;
 UINT1 au1BlackListIpAddr[SEC_MAX_IP_ADDR_LEN + 1];
 UINT1 au1Reserved[2];
}tFwlTrapMsg;

typedef struct _FwlErrMsg
{
 UINT4 u4IfIndex;
}tFwlErrMsg;

typedef struct _Secv4ErrMsg
{
 UINT4 u4Mtu;
 INT1  i1Type;
 INT1  i1Code;
 UINT1 au1Rsvd[2];
}tSecv4ErrMsg;

typedef struct _SecLogMsg
{
 UINT1 au1Msg [SEC_MAX_LOG_BUF_SIZE];
 UINT1 au1ModName [SEC_MAX_LOG_BUF_SIZE];
}tSecLogMsg;

enum 
{
  FWL_THRESHOLD_EXCEED_TRAP,
  FWL_GEN_MEM_FAILURE_TRAP,
  FWL_GEN_ATTACK_SUM_TRAP,
  FWL_IDS_ATTACK_PKT_TRAP
};

/* Data communication structure between security user and security kernel. */
typedef struct PktInfo{ 
    INT4 i4InIfIdx;
    INT4 i4OutIfIdx;
    UINT2 u2PktLen;
    INT2  i2ssport;
    UINT1 u1Direction;
    UINT1 u1Reserved[7];
}tPktInfo;

/* Secv4DummyMsg structure for resolving dest IP in kernel */

typedef struct _Secv4DummyMsg{
    UINT4 *pSaEntry;
    tMacAddr   au1MacAddr;
    UINT1      au1Pad[2]; /* Padding */
    UINT4      u4Addr;
    UINT4      u4IfIndex;
}tSecv4DummyMsg;
 
typedef struct {
   UINT1 u1MsgType;
   UINT1 au1Pad[3];
   union
   {
       tPktInfo   PktInfo;        /* For Sending and Recieving packets 
                                     between User and Kernel */
       tIkeQMsg   IkeQMsg;        /* Interaction between IPSec in Kernel to 
                                     IKE in User */
       tFwlLogMsg   FwlLogMsg;    /* To post Log Message from Firewall in kernel 
                                     to User Space  */
       tFwlTrapMsg  FwlTrapMsg;   /* To post Trap Message from Kernel to  
                                     User Space */ 
       tFwlErrMsg  FwlErrMsg;     /* To Post FWL Error Meesage from Kernel to User */ 
      tSecv4ErrMsg Secv4ErrMsg;   /* To Post IPSec Error Msg from Kernel to User */
      tSecLogMsg   SecLogMsg;     /* To Post UtlTrc msg from Kernel to User */
   tSecv4DummyMsg Secv4DummyMsg; /* Interaction b/w user and kernel to resolve the
                                   MAC for the dummy packet dest IP*/
   }ModuleParam;
 }tSecQueMsg;

/* The Module Data Structure used by Security Module. */
typedef struct SecModuleData{
    UINT1  u1Command;            /* The command type of the buffer. Currently
                                  * SEC_TO_CFA is supported.
                                  */ 
    UINT1  au1Reserved[1];        /* Added for Packing Purpose */
    UINT1  u1MsgType;        /* Added for Packing Purpose */
    UINT1  u1Direction;           /* Direction of the packet */
    UINT4  u4IfIndex;            /* Interface Index. */
    UINT1  DestMACAddr[CFA_ENET_ADDR_LEN];
    UINT1  SrcMACAddr[CFA_ENET_ADDR_LEN];
    UINT2  u2LenOrType;
 UINT2  u2SVlanCtrlIdentifier;
 UINT2  u2CVlanCtrlIdentifier;
 UINT2  u2EthType;
    UINT1  u1VerAndType;
    UINT1  u1Code;
    UINT2  u2SessionId;
    UINT2  u2Length;
    UINT2  u2PppProtocol;
}tSecModuleData;

/* This structure is used in SecProcessFrame function to enable 
 * security processing with L3 VLAN interface index or router interface index or
 * VPNC interface index. 
 */
typedef struct SecIntfInfo {
   UINT4 u4PhyIfIndex;  /* Physical interface index. */ 
   UINT4 u4VlanIfIndex; /* Vlan Interface index. */
   UINT4 u4VpncIfIndex; /* VPNC interface index. */
}tSecIntfInfo;


#define   SEC_GET_MODULE_DATA_PTR(pBuf)                 \
          ((tSecModuleData *) CRU_BUF_Get_ModuleData(pBuf))

#define SEC_GET_IFINDEX(pBuf)                           \
          ((tSecModuleData *) CRU_BUF_Get_ModuleData(pBuf))->u4IfIndex

#define SEC_GET_MSGTYPE(pBuf)                           \
          ((tSecModuleData *) CRU_BUF_Get_ModuleData(pBuf))->u1MsgType

#define SEC_GET_DIRECTION(pBuf)                           \
          ((tSecModuleData *) CRU_BUF_Get_ModuleData(pBuf))->u1Direction

#define SEC_SET_IFINDEX(pBuf, u4IfId) \
          (SEC_GET_MODULE_DATA_PTR(pBuf)->u4IfIndex = u4IfId)

#define SEC_SET_DIRECTION(pBuf, u1Dir) \
          (SEC_GET_MODULE_DATA_PTR(pBuf)->u1Direction = u1Dir)

#define SEC_SET_MSGTYPE(pBuf, u1MsgType)                           \
          (SEC_GET_MODULE_DATA_PTR(pBuf)->u1MsgType = u1MsgType)

#define SEC_GET_ETH_HDR(pBuf)                           \
          ((tEnetV2Header *)&(SEC_GET_MODULE_DATA_PTR(pBuf)->DestMACAddr))

#define SEC_GET_PPPOE_HDR(pBuf)                           \
          ((tPPPoEHeader *) &(SEC_GET_MODULE_DATA_PTR(pBuf)->DestMACAddr))
#define  SEC_SET_COMMAND(pBuf, u1Cmd) \
          (SEC_GET_MODULE_DATA_PTR(pBuf)->u1Command = u1Cmd)

#define  SEC_GET_COMMAND(pBuf) \
          (SEC_GET_MODULE_DATA_PTR(pBuf)->u1Command)

#define  SEC_SET_DEST_MEDIA_ADDR(pBuf, au1DestHwAddr) \
          MEMCPY(SEC_GET_MODULE_DATA_PTR(pBuf)->DestMACAddr,\
                 au1DestHwAddr, \
          CFA_ENET_ADDR_LEN)

#define SEC_GET_PROTOCOL(pBuf) (SEC_GET_MODULE_DATA_PTR(pBuf)->u2EthType)

/* Exported API's of security module */

UINT4 SecMainTask PROTO ((VOID));
UINT4 SecProcessFrame PROTO((tCRU_BUF_CHAIN_HEADER * pBuf, 
                             tSecIntfInfo *pSecIntfInfo,
                             UINT1 u1Direction,
                             BOOL1 * pbSecVerdict));
INT4 
CheckingFrInvalidIntfProcess(tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Offset,UINT1 u1NwType);

INT4 CheckingFrIcmpv6Pkt(tCRU_BUF_CHAIN_HEADER * pBuf);

INT4 SecProcessContinueInBound PROTO ((tCRU_BUF_CHAIN_HEADER  *pBuf,
                                UINT4                  u4IfIndex,
                                UINT4                  u4VpncIndex,
                                UINT1                  u1AssocMode,
                                UINT1                  u1IsIpSec,
                                tCfaIfInfo             * IfInfo,
                                UINT2                  u2Protocol));

INT4 SecProcessContinueOutBound PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                                        UINT4 u4IfIndex));

INT4 SecUtilGetIfIndexFromVlanId PROTO ((UINT2 u2VlanId, UINT4 *pu4CfaIfIndex));
INT4
SecUtilGetIfInfo (UINT4 u4IfIndex, tCfaIfInfo * pIfInfo);

INT4
SecUtilGetIfNwType (UINT4 u4IfIndex, UINT1 *pu1IfNwType);

INT4
SecUtilIpIfIsOurAddress (UINT4 u4Addr);

INT4
SecUtilValidateIfIndex (UINT4 u4IfIndex);

INT4
SecUtilIpIfGetIfIndexFromIpAddress (UINT4 u4IpAddress, UINT4 *pu4CfaIfIndex);


INT4
SecUtilGetIfMtu (UINT4 u4IfIdx, UINT4 *pu4Mtu);

UINT4
SecUtilGetInterfaceNameFromIndex (UINT4 u4IfIndex, UINT1 *pu1Alias);

INT4
SecUtilCheckIfLocalMac (tEnetV2Header * pHwAddr, UINT4 u4IfIndex);

INT4
SecUtilIpifIsBroadCastLocal (UINT4 u4Addr);

INT4
SecUtilIpIsLocalNet (UINT4 u4IpAddress, UINT4 *pu4IfIndex);

VOID
SecUtlGetRandom (UINT1 *pu1Rand, UINT4 u4RandLen);

UINT4
SecUtilAtoL (CONST CHR1 *nptr);

INT4 SecMainInit PROTO ((VOID));


VOID
VpnKernelDeInit PROTO ((VOID));
INT1
VpnKernelInit PROTO ((VOID));

INT4 
SecGetSysOperMode PROTO((VOID));
VOID
SecSetSysOperMode PROTO((INT4));

INT4
Sec6UtilIpIfIsOurAddress (tIp6Addr * pIp6Addr, UINT4 *pu4Index);

tIp6Addr *
Sec6UtilGetGlobalAddr (UINT4 u4Index, tIp6Addr * Ip6Addr);

INT4
Sec6UtilIpIsLocalNet (tIp6Addr * Ip6Addr, UINT4 *pu4IfIndex);

INT4
SecUtilGetVlanIdFromIfIndex PROTO((UINT4 u4IfIndex, UINT2 *pu2VlanId));
INT4
SecGetDirectionFromMacAddress (tMacAddr SrcMacAddress,
                              tMacAddr DstMacAddress, INT1 *pi1Dir);
INT4
SecApiPPPoEAddNewSession PROTO ((UINT4 u4PPPIfIndex, UINT4 u4IfIndex, 
                                 UINT2 u2SessionId));

INT4
SecApiPPPoEDelSession PROTO ((UINT4 u4PPPIfIndex, UINT4 u4IfIndex, 
                              UINT2 u2SessionId));
INT4
SecUtilAddPppHdrToPkt PROTO ((tCRU_BUF_CHAIN_HEADER *pCRUBuf, UINT4 u4IfIndex));

INT4 
SecUtilGetPppIdxFromPhyIfdx PROTO ((UINT4 u4IfIndex, UINT4 *pu4PppIndex));

INT4
SecUtilGetPhyIdxFromPPPIfdx PROTO ((UINT4 u4PppIfIndex, UINT4 *pu4IfIndex));

INT4
SecUtilGetIfTypeFromPhyIndex PROTO ((UINT4 u4IfIndex, UINT1 *pu1IfType));

INT4
SecUtilGetVlanIdFromPhyIfIndex PROTO ((UINT4 u4PhyIfIndex, UINT2 *pu2VlanId));

INT4
SecUtilStripPppHdrFromBuf PROTO ((tCRU_BUF_CHAIN_HEADER * pCRUBuf, UINT4 u4IfIndex));

VOID
SecApiSetSecIvrIfIndex PROTO ((INT4 i4SecIvrIfIndex));

VOID
SecApiSetSecVlanList PROTO ((tSecVlanList SecVlanList));

VOID
SecApiResetSecVlanList PROTO ((tSecVlanList SecVlanList));

VOID
SecApiUpdSecBridgingStatus PROTO ((UINT4 u4SecurityBridgingStatus));

UINT4 SecGetBrigdingStatus PROTO ((VOID));

INT4 SecGetSecIvrIndex PROTO ((VOID));

BOOL1 SecIsMemberOfSecVlanList PROTO((UINT2 u2VlanId));
INT4 SecApiUpdateSecIpAddrInfo(UINT4 u4IfIndex,
                            UINT4 u4Addr,
                            UINT1 u1SubnetMask, UINT1 u1Flag);

PUBLIC VOID Secv4KernelLock PROTO ((VOID));
PUBLIC VOID Secv4KernelUnLock PROTO ((VOID));

INT4 SecUtilGetPhyIfNwType (UINT4 u4DsaIfIndex,UINT1 *pu1NwType);

INT4
SecUtilGetL2tpPortInfo (INT4 i4IfIndex,tSecL2tpPortInfo *pL2tpPortInfo);

#endif /* _SECMOD_H_ */
