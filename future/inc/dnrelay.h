/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dnrelay.h,v 1.14 2007/02/01 14:52:30 iss Exp $
 *
 * Description: DNS relay specific.
 */

#define MAX_CONCURRENT_DNS_QUERIES gSystemSize.DnsRelaySystemSize.u4MaxNoOfConcurrentQueries

#define MAX_DNS_SERVERS gSystemSize.DnsRelaySystemSize.u4DnsRelayMaxNameServers

#define MAX_DNS_URL_FILTERS gSystemSize.DnsRelaySystemSize.u4DnsRelayMaxNameServers

#define MAX_DNS_CACHE_ENTRIES gSystemSize.DnsRelaySystemSize.u4MaxCacheEntries
