
/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: ospf3npwr.h,v 1.1 2014/04/10 13:36:13 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              for OSPF3 wrappers
 *              
 ********************************************************************/
#ifndef __OSPF3_NP_WR_H__
#define __OSPF3_NP_WR_H__

UINT1 Ospf3FsNpOspf3Init PROTO ((VOID));
UINT1 Ospf3FsNpOspf3DeInit PROTO ((VOID));

#ifdef MBSM_WANTED
UINT1 Ospf3FsNpMbsmOspf3Init PROTO ((tMbsmSlotInfo * pSlotInfo));
#endif /* MBSM_WANTED */

#endif /* __OSPF3_NP_WR_H__ */
