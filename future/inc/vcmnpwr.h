/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vcmnpwr.h,v 1.5 2017/11/14 07:31:11 siva Exp $
 *
 * Description: Definitions used for IP-CFA Interface
 *
 *******************************************************************/
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2004-2005                         */
/*****************************************************************************/
/*    FILE  NAME            : vcmnpwr.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : VCM module                                     */
/*    MODULE NAME           : VCM module                                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 15 May 2012                                    */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains the type definitions and    */
/*                            data structures for wrappers in VCM module.    */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE                   DESCRIPTION OF CHANGE/                 */
/*                                    FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    15 MAY 2012            Initial Create.                         */
/*---------------------------------------------------------------------------*/

#ifndef __VCM_NP_WR_H_
#define __VCM_NP_WR_H_

#include "vcmnp.h"

/* OPER ID */
enum
{
    FS_VCM_HW_CREATE_CONTEXT =1,
    FS_VCM_HW_DELETE_CONTEXT,    
    FS_VCM_HW_MAP_IF_INDEX_TO_BRG_PORT,
    FS_VCM_HW_MAP_PORT_TO_CONTEXT,
    FS_VCM_HW_MAP_VIRTUAL_ROUTER,
    FS_VCM_HW_UN_MAP_PORT_FROM_CONTEXT,
    FS_VCM_SISP_HW_SET_PORT_CTRL_STATUS,
    FS_VCM_SISP_HW_SET_PORT_VLAN_MAPPING,
#ifdef MBSM_WANTED
    FS_VCM_MBSM_HW_CREATE_CONTEXT,
    FS_VCM_MBSM_HW_MAP_IF_INDEX_TO_BRG_PORT,
    FS_VCM_MBSM_HW_MAP_PORT_TO_CONTEXT,
    FS_VCM_MBSM_HW_MAP_VIRTUAL_ROUTER,
    FS_VCM_SISP_MBSM_HW_SET_PORT_CTRL_STATUS,
    FS_VCM_SISP_MBSM_HW_SET_PORT_VLAN_MAPPING,
#endif /* MBSM_WANTED */
    FS_VCM_HW_VRF_COUNTERS,
    FS_NP_VCM_MAX_NP                     
};

/* Required arguments list for the vcm NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

typedef struct {
    UINT4  u4ContextId;
} tVcmNpWrFsVcmHwCreateContext;

typedef struct {
    UINT4  u4ContextId;
} tVcmNpWrFsVcmHwDeleteContext;

typedef struct {
    UINT4            u4IfIndex;
    UINT4            u4ContextId;
    tContextMapInfo  ContextInfo;
} tVcmNpWrFsVcmHwMapIfIndexToBrgPort;

typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4ContextId;
} tVcmNpWrFsVcmHwMapPortToContext;

typedef struct {
    tFsNpVcmVrMapInfo *  pVcmVrMapInfo;
    tVrMapAction         VrMapAction;
} tVcmNpWrFsVcmHwMapVirtualRouter;

typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4ContextId;
} tVcmNpWrFsVcmHwUnMapPortFromContext;

typedef struct {
    UINT4  u4IfIndex;
    UINT1  u1Status;
    UINT1  au1pad[3];
} tVcmNpWrFsVcmSispHwSetPortCtrlStatus;

typedef struct {
    UINT4    u4IfIndex;
    UINT4    u4ContextId;
    tVlanId  VlanId;
    UINT1    u1Status;
    UINT1    au1pad[1];
} tVcmNpWrFsVcmSispHwSetPortVlanMapping;

typedef struct {
    tVrfCounter VrfCounter;
}tVcmNpWrFsVrfCounters;


#ifdef MBSM_WANTED
typedef struct {
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
} tVcmNpWrFsVcmMbsmHwCreateContext;

typedef struct {
    UINT4            u4IfIndex;
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
    tContextMapInfo  ContextInfo;
} tVcmNpWrFsVcmMbsmHwMapIfIndexToBrgPort;

typedef struct {
    UINT4            u4IfIndex;
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
} tVcmNpWrFsVcmMbsmHwMapPortToContext;

typedef struct {
    tFsNpVcmVrMapInfo *  pVcmVrMapInfo;
    tMbsmSlotInfo *      pSlotInfo;
    tVrMapAction         VrMapAction;
} tVcmNpWrFsVcmMbsmHwMapVirtualRouter;

typedef struct {
    UINT4            u4IfIndex;
    tMbsmSlotInfo *  pSlotInfo;
    UINT1            u1Status;
    UINT1            au1pad[3];
} tVcmNpWrFsVcmSispMbsmHwSetPortCtrlStatus;

typedef struct {
    UINT4            u4IfIndex;
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
    tVlanId          VlanId;
    UINT1            u1Status;
    UINT1            au1pad[1];
} tVcmNpWrFsVcmSispMbsmHwSetPortVlanMapping;

#endif /* MBSM_WANTED */
typedef struct VcmNpModInfo {
union {
    tVcmNpWrFsVcmHwCreateContext  sFsVcmHwCreateContext;
    tVcmNpWrFsVcmHwDeleteContext  sFsVcmHwDeleteContext;
    tVcmNpWrFsVcmHwMapIfIndexToBrgPort  sFsVcmHwMapIfIndexToBrgPort;
    tVcmNpWrFsVcmHwMapPortToContext  sFsVcmHwMapPortToContext;
    tVcmNpWrFsVcmHwMapVirtualRouter  sFsVcmHwMapVirtualRouter;
    tVcmNpWrFsVcmHwUnMapPortFromContext  sFsVcmHwUnMapPortFromContext;
    tVcmNpWrFsVcmSispHwSetPortCtrlStatus  sFsVcmSispHwSetPortCtrlStatus;
    tVcmNpWrFsVcmSispHwSetPortVlanMapping  sFsVcmSispHwSetPortVlanMapping;
#ifdef MBSM_WANTED
    tVcmNpWrFsVcmMbsmHwCreateContext  sFsVcmMbsmHwCreateContext;
    tVcmNpWrFsVcmMbsmHwMapIfIndexToBrgPort  sFsVcmMbsmHwMapIfIndexToBrgPort;
    tVcmNpWrFsVcmMbsmHwMapPortToContext  sFsVcmMbsmHwMapPortToContext;
    tVcmNpWrFsVcmMbsmHwMapVirtualRouter  sFsVcmMbsmHwMapVirtualRouter;
    tVcmNpWrFsVcmSispMbsmHwSetPortCtrlStatus  sFsVcmSispMbsmHwSetPortCtrlStatus;
    tVcmNpWrFsVcmSispMbsmHwSetPortVlanMapping  sFsVcmSispMbsmHwSetPortVlanMapping;
#endif /* MBSM_WANTED */
    tVcmNpWrFsVrfCounters sFsVrfCounters;
    }unOpCode;

#define  VcmNpFsVcmHwCreateContext  unOpCode.sFsVcmHwCreateContext;
#define  VcmNpFsVcmHwDeleteContext  unOpCode.sFsVcmHwDeleteContext;
#define  VcmNpFsVcmHwMapIfIndexToBrgPort  unOpCode.sFsVcmHwMapIfIndexToBrgPort;
#define  VcmNpFsVcmHwMapPortToContext  unOpCode.sFsVcmHwMapPortToContext;
#define  VcmNpFsVcmHwMapVirtualRouter  unOpCode.sFsVcmHwMapVirtualRouter;
#define  VcmNpFsVcmHwUnMapPortFromContext  unOpCode.sFsVcmHwUnMapPortFromContext;
#define  VcmNpFsVcmSispHwSetPortCtrlStatus  unOpCode.sFsVcmSispHwSetPortCtrlStatus;
#define  VcmNpFsVcmSispHwSetPortVlanMapping  unOpCode.sFsVcmSispHwSetPortVlanMapping;
#ifdef MBSM_WANTED
#define  VcmNpFsVcmMbsmHwCreateContext  unOpCode.sFsVcmMbsmHwCreateContext;
#define  VcmNpFsVcmMbsmHwMapIfIndexToBrgPort  unOpCode.sFsVcmMbsmHwMapIfIndexToBrgPort;
#define  VcmNpFsVcmMbsmHwMapPortToContext  unOpCode.sFsVcmMbsmHwMapPortToContext;
#define  VcmNpFsVcmMbsmHwMapVirtualRouter  unOpCode.sFsVcmMbsmHwMapVirtualRouter;
#define  VcmNpFsVcmSispMbsmHwSetPortCtrlStatus  unOpCode.sFsVcmSispMbsmHwSetPortCtrlStatus;
#define  VcmNpFsVcmSispMbsmHwSetPortVlanMapping  unOpCode.sFsVcmSispMbsmHwSetPortVlanMapping;
#endif /* MBSM_WANTED */
#define VcmNpWrFsVrfCounters unOpCode.sFsVrfCounters;
} tVcmNpModInfo;
/* Function Prototype for Generic NP OpCode specific wrappers
 * defined in vcmnpapi.c */

UINT1 VcmFsVcmHwCreateContext PROTO ((UINT4 u4ContextId));
UINT1 VcmFsVcmHwDeleteContext PROTO ((UINT4 u4ContextId));
UINT1 VcmFsVcmHwMapIfIndexToBrgPort PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, tContextMapInfo ContextInfo));
UINT1 VcmFsVcmHwMapPortToContext PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex));
UINT1 VcmFsVcmHwMapVirtualRouter PROTO ((tVrMapAction VrMapAction, tFsNpVcmVrMapInfo * pVcmVrMapInfo));
UINT1 VcmFsVcmHwUnMapPortFromContext PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex));
UINT1 VcmFsVcmSispHwSetPortCtrlStatus PROTO ((UINT4 u4IfIndex, UINT1 u1Status));
UINT1 VcmFsVcmSispHwSetPortVlanMapping PROTO ((UINT4 u4IfIndex, tVlanId VlanId, UINT4 u4ContextId, UINT1 u1Status));
#ifdef MBSM_WANTED
UINT1 VcmFsVcmMbsmHwCreateContext PROTO ((UINT4 u4ContextId, tMbsmSlotInfo * pSlotInfo));
UINT1 VcmFsVcmMbsmHwMapIfIndexToBrgPort PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, tContextMapInfo ContextInfo, tMbsmSlotInfo * pSlotInfo));
UINT1 VcmFsVcmMbsmHwMapPortToContext PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, tMbsmSlotInfo * pSlotInfo));
UINT1 VcmFsVcmMbsmHwMapVirtualRouter PROTO ((tVrMapAction VrMapAction, tFsNpVcmVrMapInfo * pVcmVrMapInfo, tMbsmSlotInfo * pSlotInfo));
UINT1 VcmFsVcmSispMbsmHwSetPortCtrlStatus PROTO ((UINT4 u4IfIndex, UINT1 u1Status, tMbsmSlotInfo * pSlotInfo));
UINT1 VcmFsVcmSispMbsmHwSetPortVlanMapping PROTO ((UINT4 u4IfIndex, tVlanId VlanId, UINT4 u4ContextId, UINT1 u1Status, tMbsmSlotInfo * pSlotInfo));
#endif /* MBSM_WANTED */
UINT1 VcmFsVcmHwVrfCounter PROTO ((tVrfCounter VrfCounter));

#endif /* __VCM_NP_WR_H_ */
