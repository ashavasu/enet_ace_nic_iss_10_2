/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rmgr.h,v 1.131 2018/02/01 11:02:56 siva Exp $
 *
 * Description:
 *      This file contains all the exported definitions of FutureRM.
 *******************************************************************/
#ifndef _RMGR_H
#define _RMGR_H

#include "fssnmp.h"

/* Return values of RM module */
#define RM_SUCCESS       OSIX_SUCCESS 
#define RM_FAILURE       OSIX_FAILURE

#define RM_TRUE          TRUE
#define RM_FALSE         FALSE 

#define RM_ENABLE          1
#define RM_DISABLE         2 
#define RM_INVALID_CHKSUM_VALUE -1

#define RM_TASK_NAME      "RMGR"

#define RM_ONE             1

/* RM module events */
#define RM_PKT_FROM_APP               0x000001
#define MSR_RESTORE_COMPLETE          0x000002
#define RM_GO_ACTIVE                  0x000004
#define RM_GO_STANDBY                 0x000008
#define RM_CTRL_QMSG_EVENT            0x000010
#define RM_COMM_LOST_AND_RESTORED_EVT 0x000020
#define RM_TRIG_FSW_FROM_MGMT         0x000040
#define RM_TCP_NEW_CONN_RCVD          0x000080
#define RM_ACTIVE_FSW_FAILED          0x000100
#define RM_TMR_EXPIRY_EVENT           0x000200
#define RM_STANDBY_FSW_RCVD           0x000400
#define RM_SEND_TO_FILES_PEERS        0x000800
#define CFA_PORT_CREATION_COMPLETED   0x001000
#define STACK_TMR_EXP                 0x004000
#define RM_RESUME_EVENT               0x008000
#define RM_PROCS_RX_BUFF_SELF_EVENT   0x010000
#define RM_RESTART_PROTOCOLS          0x020000
#define RM_REINIT_STANDBY             0x040000
#define RM_INIT_BULK_REQUEST          0x080000
#define RM_PROCESS_PENDING_SYNC_MSG   0x100000
#define RM_TRIGGER_SELF_ATTACH_EVENT  0x200000
#define RM_ACT_TASK_EVENT             0x000001 
#define RM_DYNAMIC_AUDIT_PROCESSED    0x400000 /*Indicate RM the completion of checksum clculation*/
#define RM_SSL_CERT_GENERATED         0x800000
#define RM_V3_CRYPTSEQ_UPD            0x1000000

#define RM_ISSU_MAINT_MODE_EVENT      0x2000000

#define   UDP_RM_PORT                              7219
#define RM_MODULE_EVENTS (RM_PKT_FROM_APP | MSR_RESTORE_COMPLETE | \
                          RM_GO_ACTIVE | RM_GO_STANDBY | RM_CTRL_QMSG_EVENT | \
                          RM_COMM_LOST_AND_RESTORED_EVT | RM_TRIG_FSW_FROM_MGMT |\
                          RM_TCP_NEW_CONN_RCVD | RM_ACTIVE_FSW_FAILED | \
                          RM_TMR_EXPIRY_EVENT | RM_STANDBY_FSW_RCVD | \
                          RM_SEND_TO_FILES_PEERS | RM_PROCESS_PENDING_SYNC_MSG | \
                          STACK_TMR_EXP | CFA_PORT_CREATION_COMPLETED | \
                          RM_PROCS_RX_BUFF_SELF_EVENT | RM_RESTART_PROTOCOLS | \
                          RM_REINIT_STANDBY | RM_INIT_BULK_REQUEST | \
                          RM_TRIGGER_SELF_ATTACH_EVENT | RM_DYNAMIC_AUDIT_PROCESSED | \
        RM_SSL_CERT_GENERATED | RM_V3_CRYPTSEQ_UPD | RM_ISSU_MAINT_MODE_EVENT)

#define L2_SYNC_UP_COMPLETED          0x2000

/* Standard trace messages */
#define RM_INIT_SHUT_TRC      INIT_SHUT_TRC
#define RM_MGMT_TRC           MGMT_TRC
#define RM_DATA_PATH_TRC      DATA_PATH_TRC
#define RM_CTRL_PATH_TRC      CONTROL_PLANE_TRC
#define RM_DUMP_TRC           DUMP_TRC
#define RM_OS_RESOURCE_TRC    OS_RESOURCE_TRC
#define RM_FAILURE_TRC        ALL_FAILURE_TRC
#define RM_BUFF_TRACE         BUFFER_TRC
/* RM specific trace messages */
#define RM_CRITICAL_TRC       0x00010000
#define RM_SEM_TRC            0x00020000
#define RM_TMR_TRC            0x00040000
#define RM_SOCK_TRC           0x00080000
#define RM_FT_TRC             0x00100000
#define RM_SNMP_TRC           0x00200000
#define RM_NOTIF_TRC          0x00400000
#define RM_SYNCUP_TRC         0x00800000
#define RM_EVENT_TRC          0x01000000
#define RM_SWITCHOVER_TRC     0x02000000
#define RM_SOCK_CRIT_TRC      0x04000000
#define RM_SYNC_EVENT_TRC     0x08000000
#define RM_SYNC_MSG_TRC       0x10000000
#define RM_ALL_TRC            0x1FFFFFFF

#ifdef L2RED_TEST_WANTED
/*RM specific module bitlist*/
#define VCM_MOD 0x0001
#define LA_MOD 0x0002
#define VLAN_MOD 0x0004
#define SNTP_MOD 0x0008
#define MBSM_MOD 0x0010
#define CFA_MOD 0x0020
#define ACL_MOD 0x0040
#define OSPF_MOD 0x0080
#define RIP_MOD 0x0100
#define BFD_MOD 0x0200
#define ISIS_MOD 0x0400
#define ARP_MOD 0x0800
#define RTM_MOD 0x1000
#define RTM6_MOD 0x2000
#define ND6_MOD 0x4000
#define OSPF3_MOD 0x8000
#define ALL_MOD 0x7fff
#endif

/* RM enabled module trace message */
#define RM_MOD_RM_TRC          0x00000001
#define RM_MOD_VCM_TRC         0x00000002
#define RM_MOD_MBSM_TRC        0x00000004
#define RM_MOD_MSR_TRC         0x00000008
#define RM_MOD_CFA_TRC         0x00000010
#define RM_MOD_EOAM_TRC        0x00000020
#define RM_MOD_PNAC_TRC        0x00000040
#define RM_MOD_LA_TRC          0x00000080
#define RM_MOD_RSTPMSTP_TRC    0x00000100
#define RM_MOD_VLANGARP_TRC    0x00000200
#define RM_MOD_MRP_TRC         0x00000400
#define RM_MOD_PBB_TRC         0x00000800
#define RM_MOD_ECFM_TRC        0x00001000
#define RM_MOD_ELPS_TRC        0x00002000
#define RM_MOD_PBBTE_TRC       0x00004000
#define RM_MOD_ELMI_TRC        0x00008000
#define RM_MOD_SNOOP_TRC       0x00010000
#define RM_MOD_MPLS_TRC        0x00020000
#define RM_MOD_SNMP_TRC        0x00040000
#define RM_MOD_LLDP_TRC        0x00080000
#define RM_MOD_CLI_TRC         0x00100000
#define RM_MOD_NPAPI_TRC       0x00200000
#define RM_MOD_STATIC_CONF_TRC 0x00400000
#define RM_MOD_ALL_TRC         0x007FFFFF

enum
{
    RM_FM_ACTIVE_IND = 1,
    RM_FM_STANDBY_IND,
    RM_FM_SYNC_SUCCESS_IND,
    RM_FM_SYNC_FAIL_IND
};

/* Traces that are enabled by default */
#define RM_DEFAULT_TRC        (RM_SOCK_CRIT_TRC)

/* Traces that are enabled during switchover */
#define RM_SWITCHOVER_TIME_TRC   RM_ALL_TRC

#ifdef KERNEL_WANTED
#define L2RED_IS_NP_PROGRAMMING_ALLOWED() OSIX_TRUE
#else
#define L2RED_IS_NP_PROGRAMMING_ALLOWED() \
        ((RmGetNodeState () == RM_ACTIVE) ? OSIX_TRUE: OSIX_FALSE)
#endif

#define RM_STANDBY_UP                   RM_PEER_UP
#define RM_STANDBY_DOWN                 RM_PEER_DOWN

/* The Maximum size of sync packet can be received 
 * in a single socket read call */
#define RM_MAX_SYNC_PKT_LEN 1568
#define RM_MAX_BUF_SYNC_PKT_LEN 9000
#define RM_HB_SYNC_PKT_LEN 512
#define RM_STATIC_CONFIG_NOT_RESTORED   1
#define RM_STATIC_CONFIG_IN_PROGRESS    2
#define RM_STATIC_CONFIG_RESTORED       3

#define RM_FSW_OCCURED                  1
#define RM_FSW_NOT_OCCURED              2

#ifdef L2RED_WANTED
#define  RM_GET_SEQ_NUM(pSeqNum) RmApiGetSeqNumForSyncMsg(pSeqNum)
#else
#define  RM_GET_SEQ_NUM(pSeqNum) *pSeqNum = 0
#endif

/* Size of the peer node count msg sent to protocols */
#define RM_NODE_COUNT_MSG_SIZE          1

/*  Error codes */

#define RM_NONE                1
#define RM_MEMALLOC_FAIL       2
#define RM_SENDTO_FAIL         3
#define RM_PROCESS_FAIL        4

#define RM_INV_SOCK_FD -1
   
#define RM_LOCK()              RmLock ()
#define RM_UNLOCK()            RmUnLock ()

#define RM_WR_LOCK() \
    if (gu1RmLockVariable == OSIX_FALSE) \
        RM_LOCK () 

#define RM_WR_UNLOCK() \
    if (gu1RmLockVariable == OSIX_FALSE) \
        RM_UNLOCK () 

/* Allocate CRU buf and set the valid offset to point to Rm data. 
 * i.e., reserve space for RM hdr  */
#ifdef L2RED_WANTED
#define  RM_ALLOC_TX_BUF(size) RmApiAllocTxBuf (size)
#else
#define  RM_ALLOC_TX_BUF(size) NULL
#endif

#define RM_FREE(size)     CRU_BUF_Release_MsgBufChain (size, FALSE)

#define RM_COPY(CruBuf, LinBuf, size)  \
        CRU_BUF_Copy_OverBufChain (CruBuf, LinBuf, 0, size)
   
#define RM_COPY_TO_OFFSET(dest, src, offset, size) \
        CRU_BUF_Copy_OverBufChain (dest, (UINT1 *) src, offset, size)
      
#define RM_GET_DATA_1_BYTE(pMsg, u4Offset, u1Value) \
        CRU_BUF_Copy_FromBufChain(pMsg, (UINT1 *)&u1Value, u4Offset, 1)

#define RM_GET_DATA_4_BYTE(pMsg, u4Offset, u4Value) \
{\
   CRU_BUF_Copy_FromBufChain(pMsg, ((UINT1 *) &u4Value), u4Offset, 4);\
   u4Value = OSIX_NTOHL ((UINT4)u4Value);\
}

#define RM_GET_DATA_2_BYTE(pMsg, u4Offset, u2Value) \
{\
   CRU_BUF_Copy_FromBufChain(pMsg, ((UINT1 *) &u2Value), u4Offset, 2);\
   u2Value = OSIX_NTOHS (u2Value);\
}

#define RM_GET_DATA_N_BYTE(pMsg, pdst, u4Offset, u4size) \
{\
   CRU_BUF_Copy_FromBufChain(pMsg, ((UINT1 *) pdst), u4Offset, u4size);\
}

#define RM_GET_VALID_BYTE_COUNT(pMsg, Count) \
{\
   Count = CRU_BUF_Get_ChainValidByteCount (pMsg); \
}

#define RM_DATA_ASSIGN_1_BYTE(pMsg, u4Offset, u1Value) \
{\
   UINT1  u1LinearBuf = (UINT1) u1Value;\
   CRU_BUF_Copy_OverBufChain(pMsg, ((UINT1 *) &u1LinearBuf), u4Offset, 1);\
}

#define RM_DATA_ASSIGN_2_BYTE(pMsg, u4Offset, u2Value) \
{\
   UINT2  u2LinearBuf = OSIX_HTONS ((UINT2) u2Value);\
   CRU_BUF_Copy_OverBufChain(pMsg, ((UINT1 *) &u2LinearBuf), u4Offset, 2);\
}

#define RM_DATA_ASSIGN_4_BYTE(pMsg, u4Offset, u4Value) \
{\
   UINT4  u4LinearBuf = OSIX_HTONL ((UINT4) u4Value);\
   CRU_BUF_Copy_OverBufChain(pMsg, ((UINT1 *) &u4LinearBuf), u4Offset, 4);\
}
                               
#define RM_PKT_MOVE_TO_DATA(pBuf, u1HLen) \
       { \
        CRU_BUF_Move_ValidOffset (pBuf, u1HLen);\
       }

/* Protocols registered with RM module, uses this macro to strip off 
 * the RM header from the RM packet */
#define RM_STRIP_OFF_RM_HDR(pBuf, u2PktLen) \
{\
    if (pBuf != NULL) \
    {\
        RM_PKT_MOVE_TO_DATA (pBuf, RM_HDR_LENGTH);\
        u2PktLen = (UINT2)(u2PktLen - RM_HDR_LENGTH);\
    }\
}

/* Protocols registered with RM, uses this macro to get the sequence 
 * number from RM Header */
#define RM_PKT_GET_SEQNUM(pBuf, pu4SeqNum) \
{\
    if (pBuf != NULL)\
    {\
        RM_GET_DATA_4_BYTE (pBuf, RM_HDR_OFF_SEQ_NO, *pu4SeqNum);\
    }\
    else\
    {\
        *pu4SeqNum = 0;\
    }\
}

/* Currently CRU BUF is used for RM. In future, this can be 
 * modified to a linear buffer */
typedef tCRU_BUF_CHAIN_HEADER  tRmMsg;

#ifdef L2RED_TEST_WANTED
typedef struct {
    UINT2 u2AppId;
    UINT2 u2ChkSumValue;
}tRmDynAuditChkSumMsg;
#endif

/* Structure used by management modules to send the configuration information
 * to the RM module*/
typedef struct {
    tSnmpIndex            *pMultiIndex; /* Index for the OID */
    tSNMP_MULTI_DATA_TYPE *pMultiData;  /* OID Data to be configured */
    UINT4                 u4SeqNo;  /* Sequence number for this message*/
    INT4                  i4OidLen; /* OID length */
    UINT1                 *pu1ObjectId; /* Actual OID */
    INT1                  i1ConfigSetStatus; /* return value of nmhSet SNMP_SUCCESS/
                                                SNMP_FAILURE */
    INT1                  i1MsgType; /* RM_CONFIG_VALID_MSG/RM_CONFIG_IGNORE_ABORT/
                                        RM_CONFIG_ACCEPT_ABORT/RM_CONFIG_DUMMY_MSG*/
    UINT1                 au1Pad [2];
}tRmNotifConfChg;

/* Structure used by HB module to interface with RM module */
typedef struct {
    UINT4 u4Evt;
    UINT4 u4PeerAddr;
}tRmHbMsg;

/* For sending the standby node count to the protocols*/
typedef struct RmNodeInfo
{
    UINT1 u1NumStandby;
    UINT1 au1Reserved [3];
}
tRmNodeInfo;

/* Protocol events */
typedef struct {
    UINT4 u4AppId;     /* Module Identifier which calls 
                          RmApiHandleProtocolEvents API */
    UINT4 u4Event;     /* Actions to be informed to RM module
                        * RM_PROTOCOL_BULK_UPDT_COMPLETION is sent from protocols
                          to RM to inform RM about the completion of bulk update
                          process in  standby node. i.e., protocols call
                          RmApiHandleProtocolEvents API after receiving the 
                          BULK_UPDATE_TAIL_MSG.
                        * RM_INITIATE_BULK_UPDATE from protocols or RM to initiate 
                          the bulk request.         
                        * RM_BULK_UPDT_ABORT is sent from protocols to RM when the
                          bulk update process fails
                        * RM_STANDBY_EVT_PROCESSED is sent from protocols to RM 
                          on completion of STANDBY state transition by that 
                          protocol.
                        * RM_IDLE_TO_ACTIVE_EVT_PROCESSED is sent by protocols to
        RM on completion of IDLE to ACTIVE state transition
                          by that protocol.
                        * RM_STANDBY_TO_ACTIVE_EVT_PROCESSED is sent by protocols
                          to RM on completion of STANDBY to ACTIVE state transition 
                          by that protocol.
                        * RM_PROTOCOL_SEND_EVENT is sent by the protocols to send an event
                        * to the RM module */
    UINT4 u4SendEvent; /* Event to be send by other protocols to the RM module
   * This event is send to the RM module if u4Event is set to
   * RM_PROTOCOL_SEND_EVENT */
    UINT4 u4Error;     /* Error code 
                        * RM_MEMALLOC_FAIL from protocols 
                        * RM_SENDTO_FAIL from protocols 
                        * RM_PROCESS_FAIL from protocols
                        * RM_NONE */
} tRmProtoEvt;

/* List of events sent to protocols */
enum
{
    RM_EVENT_UNKNOWN = 0,
    GO_ACTIVE = 1,        /* 1 - RM node status sent to protocols */
    GO_STANDBY,           /* 2 -do- */
    RM_PEER_UP,        /* 3 - PEER node status sent to protocols */
    RM_PEER_DOWN,      /* 4 -do- */
    RM_CONFIG_RESTORE_COMPLETE, /* 5-evt sent to protocols (restore complete) */
    CONFIG_CHANGE,        /* 6 - event sent to peer node about the config chg */
    FLASH_CKSUM_REQ,      /* 7 - peer node requests flash cksum */
    FLASH_CKSUM_RESP,     /* 8 - current node resp about the flash cksum */
    RM_MESSAGE,           /* 9 -Evt to protocols - valid buf has to be sent */
    L2_INITIATE_BULK_UPDATES, /* 10 - evt to start bulk updates */
    RM_FILE_TRANSFER_COMPLETE, /* 11 - evt send to inform the protocols  that 
                                  requested file transfer is completed */
    RM_COMPLETE_SYNC_UP, /* 12 - Evt send by RM to L2 protocols to complete
                                  sync up during force switchover */
    L2_COMPLETE_SYNC_UP, /* 13 - Evt send by L2 lower layer module to immediate
                                 higher layer L2 module during force
                                 switchover */
    RM_NOTIFICATION_INFO,/* 14 - Notification information sent from STANDBY to
                                  ACTIVE*/
    RM_INIT_TO_ACTIVE_COMPLETE, /* 15 - init to active processing */
    RM_INIT_HW_AUDIT, /* 16 - Initiate the HW Audit for protocols */
    RM_DYNAMIC_SYNCH_AUDIT, /*17 - Evt to Protocols to initiate checksum calculation
                                   for the show cmd output of dynamic messages*/
#ifdef L2RED_TEST_WANTED
    RM_DYN_AUDIT_CHKSUM_PEER_REQ, /*18 - checksum request msg sent from ACTIVE to
                                    STANDBY*/
    RM_DYN_AUDIT_CHKSUM_PEER_RESP, /*19 - checksum response  msg sent from STANDBY to
                                    ACTIVE*/
#endif
    RM_TRIGGER_SELF_ATTACH, /* 18 Trigger Self Attach to MBSM in case of
                                 Dual Unit Stacking */
    RM_SSL_GET_CERT_REQ_MSG,
    RM_V3_GET_CRYPTSEQ_REQ_MSG
};

enum
{
    RM_DYN_AUDIT_NOT_TRIGGERED = 0,
    RM_DYN_AUDIT_INPROGRESS,
    RM_DYN_AUDIT_ABORTED,
    RM_DYN_AUDIT_SUCCESS,
    RM_DYN_AUDIT_FAILED
};
/* To initiate bulk updates in non-l2 module, a new event with the 
 * same value is created */
#define RM_INITIATE_BULK_UPDATES L2_INITIATE_BULK_UPDATES

/* List of events sent from RM-HB module to RM core module */
enum
{
 /* Following commented values has to be changed 
  * when the associated enum values of these macro are changed.
  * GO_ACTIVE = 1, GO ACTIVE event to RM core module.
    GO_STANDBY = 2, GO STANDBY event to RM core module.
    RM_PEER_UP = 3, PEER UP event to RM core module
    RM_PEER_DOWN = 4,  PEER DOWN event to RM core module*/
    RM_PROC_PENDING_SYNC_MSG = 5, /* Event sent to RM core module to process 
                                     all the pending synchronization messages.*/
    RM_FSW_FAILED_IN_ACTIVE = 6, /* Event sent to notify force-switchover failed, 
                                 upon receiving this event in RM core module, 
                                 releases the management lock and allows the 
                                 dynamic sync messages for the peer.*/
    RM_FSW_RCVD_IN_STANDBY = 7, /* Force switchover is received through HB message at STANDBY,
                                 notify this information to RM core module.*/
    RM_COMM_LOST_AND_RESTORED = 8 /* RM interface is down and up, so initialize 
                                     the Redundancy enabled protocols. */
};

/* The following array should be udpated with proper string to 
 * display proper value in notification messages */
#ifdef _RMMAIN_C_
CONST CHR1  *gapc1RmEvntName [] = {
    /*  0 */ "RM_EVENT_UNKNOWN",
    /*  1 */ "GO_ACTIVE",       
    /*  2 */ "GO_STANDBY",
    /*  3 */ "RM_PEER_UP",
    /*  4 */ "RM_PEER_DOWN",
    /*  5 */ "RM_CONFIG_RESTORE_COMPLETE",
    /*  6 */ "CONFIG_CHANGE", 
    /*  7 */ "FLASH_CKSUM_REQ",
    /*  8 */ "FLASH_CKSUM_RESP",
    /*  9 */ "RM_HEART_BEAT_MSG",
    /* 10 */ "RM_MESSAGE",
    /* 11 */ "L2_INITIATE_BULK_UPDATES",
    /* 12 */ "RM_FILE_TRANSFER_COMPLETE",
    /* 13 */ "RM_COMPLETE_SYNC_UP",
    /* 14 */ "L2_COMPLETE_SYNC_UP",
    /* 15 */ "RM_NOTIFICATION_INFO",
    /* 16 */ "RM_INIT_TO_ACTIVE_COMPLETE",
    /* 17 */ "RM_INIT_HW_AUDIT",
#ifdef L2RED_TEST_WANTED
    "RM_DYN_AUDIT_CHKSUM_PEER_REQ",
    "RM_DYN_AUDIT_CHKSUM_PEER_RESP",
#endif
    /* 18 */ "RM_TRIGGER_SELF_ATTACH",
    /* 19 */ "RM_SSL_GET_CERT_REQ_MSG",
    /* 20 */ "RM_V3_GET_CRYPTSEQ_REQ_MSG"
};
#else
extern CONST CHR1  *gapc1RmEvntName[];
#endif

/* List of events sent from protocols */
enum
{
   RM_INITIATE_BULK_UPDATE = 1, /* To initiate bulk request in case of 
                                   failure/SWAudit mismatch */
   RM_PROTOCOL_BULK_UPDT_COMPLETION, /* Indicate RM about bulk update process
                                      completion by that protocol */
   RM_BULK_UPDT_ABORT, /* Indicates failure of bulk update process */
   RM_STANDBY_TO_ACTIVE_EVT_PROCESSED, /* STANDBY to ACTIVE processed 
                                          successfully */
   RM_IDLE_TO_ACTIVE_EVT_PROCESSED, /* IDLE to ACTIVE processed 
                                          successfully */
   RM_STANDBY_EVT_PROCESSED, /* IDLE/ACTIVE to STANDBY processed successfully */
   RM_PROTOCOL_SEND_EVENT    /* To send an event by protocols to RM module */
};

/* Application identifiers used by RM */
enum
{
    /*  0 */ RM_APP_ID = 0, /* Appln. id used for rm<->rm commn. */
    /*  1 */ RM_VCM_APP_ID,
    /*  2 */ RM_MBSM_APP_ID,
    /*  3 */ RM_MSR_APP_ID, /* Appln. id used for msr<->msr commn. */
    /*  4 */ RM_CFA_APP_ID,
    /*  5 */ RM_EOAM_APP_ID,
    /*  6 */ RM_PNAC_APP_ID,
    /*  7 */ RM_LA_APP_ID,
    /*  8 */ RM_RSTPMSTP_APP_ID,
    /*  9 */ RM_VLANGARP_APP_ID,
    /* 10 */ RM_MRP_APP_ID,
    /* 11 */ RM_PBB_APP_ID,
    /* 12 */ RM_ECFM_APP_ID,
    /* 13 */ RM_ELPS_APP_ID,
    /* 14 */ RM_PBBTE_APP_ID,
    /* 15 */ RM_ELMI_APP_ID,
    /* 16 */ RM_SNOOP_APP_ID,
    /* 17 */ RM_MPLS_APP_ID,
    /* 18 */ RM_LDP_APP_ID,
    /* 19 */ RM_L2VPN_APP_ID,
    /* 20 */ RM_OSPF_APP_ID,  
    /* 21 */ RM_SNMP_APP_ID,
    /* 22 */ RM_LLDP_APP_ID,
    /* 23 */ RM_OSPFV3_APP_ID,
    /* 24 */ RM_PIM_APP_ID,
    /* 25 */ RM_CLI_APP_ID, /* Login prompt is set based on the value of
                               gCliRmRole. So GO_ACTIVE indication has to
                               be given to CLI before MSR */
    /* 26 */ RM_NP_APP_ID,
    /* 27 */ RM_STATIC_CONF_APP_ID,
    /* 28 */ RM_ERPS_APP_ID,
    /* 29 */ RM_ACL_APP_ID,
    /* 30 */ RM_QOS_APP_ID,
    /* 31 */ RM_ISIS_APP_ID,
    /* 32 */ RM_ISS_APP_ID, 
    /* 33 */ RM_BGP_APP_ID,
    /* 34 */ RM_DCBX_APP_ID, 
    /* 35 */ RM_DHCPS_APP_ID,
    /* 36 */ RM_DHCPC_APP_ID,
    /* 37 */ RM_ARP_APP_ID,
    /* 38 */ RM_ND6_APP_ID,
    /* 39 */ RM_SNTP_APP_ID,
    /* 40 */ RM_VRRP_APP_ID,
    /* 41 */ RM_IGMP_APP_ID,
    /* 42 */ RM_BFD_APP_ID,
    /* 43 */ RM_RIP_APP_ID,
    /* 44 */ RM_RIP6_APP_ID,
    /* 45 */ RM_RTM_APP_ID,
    /* 46 */ RM_RTM6_APP_ID,
    /* 47 */ RM_PTP_APP_ID,
    /* 48 */ RM_DHCP6RLY_APP_ID,
    /* 49 */ RM_VXLAN_APP_ID,
    /* 50 */ RM_FSB_APP_ID,
    /* 51 */ RM_MAX_APPS   /* Include new AppIds before this */ 
};

/* Modules must update the appropriate module name, to be
 * displayed in notification messages, in gapc1AppName array pointer */
#ifdef _RMMAIN_C_
CHR1  *gapc1AppName [] = {
    /*  0 */ "RM",
    /*  1 */ "VCM",
    /*  2 */ "MBSM",
    /*  3 */ "MSR",
    /*  4 */ "CFA",
    /*  5 */ "EOAM",
    /*  6 */ "PNAC",
    /*  7 */ "LA",
    /*  8 */ "RSTPMSTP",
    /*  9 */ "VLANGARP",
    /* 10 */ "MRP",
    /* 11 */ "PBB",
    /* 12 */ "ECFM",
    /* 13 */ "ELPS",
    /* 14 */ "PBBTE",
    /* 15 */ "ELMI",
    /* 16 */ "SNOOP",
    /* 17 */ "MPLS",
    /* 18 */ "LDP",
    /* 19 */ "L2VPN",
    /* 20 */ "OSPF",
    /* 21 */ "SNMP",
    /* 22 */ "LLDP",
    /* 23 */ "OSPF3",
    /* 24 */ "PIM",
    /* 25 */ "CLI",
    /* 26 */ "NP_RED",
    /* 27 */ "STATIC_CONF",
    /* 28 */ "ERPS",
    /* 29 */ "ACL",
    /* 30*/ "QOSX",
    /* 31*/ "ISIS", 
    /* 32 */ "ISS", 
    /* 33 */ "BGP",
    /* 34 */ "DCBX", 
    /* 35 */ "DHCPS",
    /* 36 */ "DHCPC",
    /* 37 */ "ARP",
    /* 38 */ "ND6",
    /* 39 */ "SNT",
    /* 40 */ "VRRP",
    /* 41 */ "IGMP",
    /* 42 */ "BFD",
    /* 43 */ "RIP",
    /* 44 */ "RIP6",
    /* 45 */ "RTM",
    /* 46 */ "RTM6",
    /* 47 */ "PTP",
    /* 48 */ "DHCP6RLY",
    /* 49 */ "VXLAN",
    /* 50 */ "FSB",
    /* 51 */ "ALL"
};
#else
extern CHR1  *gapc1AppName[];
#endif

/* Message types used by protocols to send bulk update request, bulk update tail,
and bulk update message. */


#define RM_BULK_UPDT_REQ_MSG   1
#define RM_BULK_UPDATE_MSG            2 
#define RM_BULK_UPDT_TAIL_MSG         3     


/* RM states */
enum {
   RM_INIT = 0,
   RM_ACTIVE,
   RM_STANDBY,
   RM_TRANSITION_IN_PROGRESS, /* Node transition in progress */
   RM_MAX_STATES /* Count of states. If any new states, add before this  */
};

/* Message types used in the static configuration sync-up message */
enum
{
    RM_UNUSED = 0, /* Macro for 0 */

    RM_CONFIG_VALID_MSG = 1, /* Used if the configuration was successful at
                                Active node. */
    RM_CONFIG_DUMMY_MSG = 2,  /* Used if the configuration failed at the Active
                                node. */
    RM_CONFIG_IGNORE_ABORT = 3,  /* Used to ignore abort messages at the standby
                                node. */
    RM_CONFIG_ACCEPT_ABORT = 4  /* Used to the accept abort messages at the standby
                                node. */
};


/* Rm header format */
typedef struct {
   /* RM Version no. */
   UINT2 u2Version;
  
   /* RM pkt Checksum */
   UINT2 u2Chksum;
   
   /* RM pkt total length (RM hdr + data) */
   UINT4 u4TotLen;

   /* REQ/RESP */
   UINT4 u4MsgType;

   /* Source entity id */
   UINT4 u4SrcEntId;

   /* Destination entity id */
   UINT4 u4DestEntId;

   /* Sequence no. */
   UINT4 u4SeqNo;

#define RM_HDR_OFF_VERSION      0
#define RM_HDR_OFF_CKSUM        2
#define RM_HDR_OFF_TOT_LENGTH   4
#define RM_HDR_OFF_MSG_TYPE     8
#define RM_HDR_OFF_SRC_ENTID   12
#define RM_HDR_OFF_DEST_ENTID  16
#define RM_HDR_OFF_SEQ_NO       20 
#define RM_HDR_LENGTH    sizeof(tRmHdr)
} tRmHdr;

/* Struct used by protocols to register with RM */
typedef struct {

   /* Entity Id (i.e., Application id) to be registered with RM */
   UINT4 u4EntId;
   
   /* Rm delivers the pkt rcvd to protocols thru' this FnPtr. 
    * Parameters: u1Event - Valid events are GO_ACTIVE/GO_STANDBY/RM_MESSAGE
    *             pBuf - ptr to buf (valid only if u4Event==RM_MESSAGE)
    *             u2PktLen - Length of pBuf
    */
   INT4 (*pFnRcvPkt) (UINT1 u1Event, tCRU_BUF_CHAIN_HEADER *pBuf, UINT2 u2PktLen);

}tRmRegParams;

#define RCV_FILE_TASK_PRI  12 
#define MAX_FILE_NAME_LEN  32 
#define MAX_TASK_NAME_LEN  20

#define RM_APP_LIST_SIZE   ((RM_MAX_APPS + 31)/32 * 4)

typedef UINT1 tRmAppList [RM_APP_LIST_SIZE];

typedef struct 
{
    UINT1 au1FileName[MAX_FILE_NAME_LEN];
    UINT1 ac1TaskName[MAX_TASK_NAME_LEN];  
    UINT4 u4TskId;
}tRcvFileInfo;

typedef struct
{
   UINT4   u4PeerIp;
   UINT1   au1FileName[MAX_FILE_NAME_LEN];
   UINT1   ac1TaskName[MAX_TASK_NAME_LEN];
   UINT4   u4TskId;

}tRmPeerFileInfo;




VOID  RmSendFileToPeers(VOID);
UINT4 RmGetSwitchIdFromTskId (UINT4);
INT4  RmGetFileIndexFromTskId (UINT4  u4TskId);
INT4  RmGetFileFromPeer (INT4 i4FtSrvSockFd);
VOID RmUdpActTaskCrt (VOID);
VOID RmUdpActSockInit (VOID);
VOID RmSockCallback (INT4);
VOID RmUdpStdTaskCrt (VOID);
VOID RmUdpStdSockInit (VOID);
VOID  RmTaskSendFiles (INT4 i4TskIndex);
/* NOTE: Do not change the value of the file ids.
   The order given in FileInfo struct and the enum values 
   should match.  */

#define RM_MAX_NUM_FILES   7

enum
{
    FILE_MIB_SAVE_CONF_ID = 0,
    FILE_SLOT_INFO_ID,
    FILE_NVRAM_ID,
    FILE_PRIVIL_ID,
    FILE_USERS_INFO_ID,
    FILE_SYSTEM_SIZE_ID,
    FILE_SERVER_KEY_ID1,
    FILE_SERVER_KEY_ID2,
    FILE_SYSLOG_FILE,
 FILE_SSL_SERV_CERT,
  FILE_V3_CRYPT_SEQNUM,
#if ((!defined L2RED_WANTED) && ((defined MBSM_WANTED) && (defined RM_WANTED)))
/* Only for COLDSTDBY case */
    MAX_NO_OF_FILES = (RM_MAX_NUM_FILES * MBSM_MAX_LC_SLOTS)
#else
    MAX_NO_OF_FILES
#endif
};
                                                                               
/* Mandatory number of files transfered during Synchup */
#ifdef MBSM_WANTED
#define MANDATORY_NO_FILES 3
#else
#define MANDATORY_NO_FILES 5
#endif

/* This structure is used by external protocols for sending the 
 * Acknowledgement to RM after processing of sync-up message. */
typedef struct _RmProtoAck
{
   UINT4  u4AppId;    /* Protocol Id known by RM */
   UINT4  u4SeqNumber; /* ACK for this sequence numbered packet */
}tRmProtoAck;

/* RM Notifications */
/* Protocol operation */
#define RM_NOTIF_SYNC_UP             1
#define RM_NOTIF_SWITCHOVER          2
#define RM_COPY_LOG                  7
/* HITLESS RESTART TRAP */
#define RM_HR_START                  5
#define RM_HR_STOP                   6

/* Status of the protocol operation */                                                                 
#define RM_NOT_STARTED         0
#define RM_STARTED             1
#define RM_COMPLETED           2
#define RM_FAILED              3
/* Length of Date and Time string used in trap message. */

#define RM_DATE_TIME_STR_LEN    24
#define RM_ERROR_STR_LEN        256

typedef struct {
   UINT4 u4NodeId;    /* Node Id of the node which generates the trap */                                  UINT4 u4State;     /* Node State(ACTIVE/STANDBY) of the node which generates                                                 the trap */
   UINT4 u4AppId;     /* Module Identifier which calls this API*/
   UINT4 u4Operation; /* Protocol operation (BULK_UPDATE / SWITCHOVER ) */
   UINT4 u4Status; /* The status of  protocol operation ( started/ completed /                                               failure) */
   UINT4 u4Error;    /* Error code (MEMALLOC_FAIL from protocols / SENDTO_                                                                         FAIL from protocols / NONE) */
   CHR1  ac1DateTime[RM_DATE_TIME_STR_LEN];   /* Date and Time when the                                                                                 trap gets generated*/
   CHR1 ac1ErrorStr[RM_ERROR_STR_LEN];
} tRmNotificationMsg;
#define  RM_MAC_ADDR_LEN    6
enum
{
   RM_ATTACH = 0,
   RM_DETACH
};
enum
{
    RM_COPY_STATUS_NONE = 0,
    RM_COPY_STATUS_INITIATE,
    RM_COPY_STATUS_INPROGRESS,
    RM_COPY_STATUS_SUCCESS,
    RM_COPY_STATUS_FAILURE
};
typedef struct
{
    UINT4         u4MsgType;                             /* Query or Response*/
    UINT4         u4Switchid;                            /* slot number*/
    UINT1         au1SwitchBaseMac[RM_MAC_ADDR_LEN];     /* switch hw address*/
    UINT1         u1PrevState;
    UINT1         u1CurrState;
}tStackSwitchInfo;
typedef struct  {
   UINT4         u4PeerId; /*Peer information*/
   UINT4         u4MsgType;                             /* Query or Response*/
   UINT4         u4Switchid;                            /* slot number*/
   UINT1         au1SwitchBaseMac[RM_MAC_ADDR_LEN];     /* switch hw address*/
   UINT1         u1PrevState;
   UINT1         u1CurrState;
   UINT1         u1PeerState;  /*Peer status . Peer up/Peer down*/
   UINT1         au1Rsvd[3] ; /* For padding*/
}tRmPeerCtrlMsg ;

VOID RmGetSelfNodeIp (UINT4 *pu4SelfNodeIp);
VOID RmGetSelfNodeMask (UINT4 *pu4SelfNodeMask);
VOID RmGetNodeSwitchId (UINT4 *pu4NodeId);

VOID  StackUpdateConfigurationFile (VOID);

UINT4  RmHandleLCInsert (INT4 i4SlotId);
                                                                               
/* Exported functions */
VOID   RmTaskMain          (INT1 *pArg);

/* Function used by protocols (Applications) to register/De-Register with RM */
UINT4  RmRegisterProtocols (tRmRegParams *RmReg);
UINT4  RmDeRegisterProtocols (UINT4 u4SrcEntId);

/* Function provided for applications to enq msg to RM */
UINT4  RmEnqMsgToRmFromAppl(tRmMsg *pRmMsg, UINT2 u2DataLen, 
                            UINT4 u4SrcEntId, UINT4 u4DestEntId);

/* Function provided for applications to enq dynamic audit checksum msg to RM */
UINT4  RmEnqChkSumMsgToRmFromApp (UINT2 u2AppId, UINT2 u2ChkSum); 

/* Function used to request for file transfer of the specified file */
UINT4  RmRequestFile (UINT1 u1FileId);

/* Function to receive file by connecting to File Transfer server */
UINT4  RmRcvFile           (const CHR1 *pc1FileName);

/* Function to get the Node state of RMGR. */
UINT4  RmGetNodeState (VOID);
UINT4  RmRetrieveNodeState (VOID);

/* Function to get the Node previous state of RMGR. */
UINT4  RmGetNodePrevState (VOID);

/* Function to get the number of standby nodes that are up. */
UINT1  RmGetStandbyNodeCount (VOID);

/* Function to get the number of peer nodes that are up during ISSU. */
UINT1 RmGetIssuPeerNodeCount (VOID);

/* Function to set the number of peer nodes that are up during ISSU. */
VOID RmApiSetIssuPeerNodeCount(UINT1 u1PeerCount);

UINT1 RmGetPeerNodeCount (VOID);
/* Function to release the memory allocated after processing standby count msg*/
UINT4 RmReleaseMemoryForMsg (UINT1 *pu1Block);

/* Function used to send event to protocols or notification to management
 * application based on the protocol operations. */
UINT1 RmApiHandleProtocolEvent (tRmProtoEvt *pEvt);

/* Function to get the static configuration restoration status */
UINT1  RmGetStaticConfigStatus (VOID);

/* Function to set the static configuration restoration status */
VOID   RmSetStaticConfigStatus (UINT1 u1Status);

/* Function to get the force switchover flag */
UINT1 RmGetForceSwitchOverFlag (VOID);

/* Function to set the force switchover flag */
VOID RmSetForceSwitchOverFlag (UINT1 u1Flag);

/* Function to Send Event to all applications except the called one */
VOID   RmSendEventToAppln (UINT4 u4AppId);

/* API called by  protocols to send MSR_RESTORE_COMPLETE/L2_SYNC_UP_COMPLETED 
 * to RM task */
UINT4 RmSendEventToRmTask (UINT4 u4Event);

INT4 RmLock (VOID);
INT4 RmUnLock (VOID);

VOID RmApiTrapSendNotifications (tRmNotificationMsg * pNotifyInfo);
/* Function to disable sending dynamic sync up message when reload command
 * is executed */
VOID RmDisableDynamicSyncup (VOID);

/* Function used  by the registered protocols to inform the status of
 * bulk updates completion. */
INT4 RmSetBulkUpdatesStatus (UINT4 u4AppId);

/* Function used by the registered protocols to get the status of
 * bulk updates completion. */
INT4 RmGetBulkUpdatesStatus (UINT4 u4AppId);

/* Function to validate whether the given interface name exists or not */
UINT4 RmValidateRmIfName (UINT1 *pu1IfName);

/* API to reserve a sequence number for the sync-up message */
VOID RmApiGetSeqNumForSyncMsg (UINT4 *pu4SeqNum);

/* API to send the ACK for the processed sync-up message */
INT4 RmApiSendProtoAckToRM (tRmProtoAck *pProtoAck);

/* Function for posting the static configuration related information to
 * the RM task*/
INT4 RmApiSyncStaticConfig (tRmNotifConfChg *pRmNotifConfChg);

/* Function for setting the wrapper lock variable */
VOID RmApiSetNoLock (VOID);

VOID RmApiSetLock (VOID);

/* Function to get the active node id */
UINT4 RmGetActiveNodeId PROTO ((VOID));

/* Function to add the SNMP MIB node to failure buffer */
INT4 RmApiConfAddToFailBuff
PROTO ((tSNMP_OID_TYPE *pOID, tSNMP_MULTI_DATA_TYPE * pVal));

/* API used by HB module to send event to RM module */
UINT4 RmApiSendHbEvtToRm (tRmHbMsg *pRmHbMsg);

/* API used by MSR to block set the restoration of OIDs given in gaRmOidList */
INT1 RmApiConfIsOIDInFilterList (tSNMP_OID_TYPE * pOID);

UINT4 RmColdStandByFileTr(VOID);
VOID   RmSendImagetoAttachedPeers (VOID);
VOID   RmApiSystemRestart (VOID);
UINT4  RmEnqCertGenMsgToRmFromApp (VOID);
UINT4  RmEnqCryptSeqMsgToRmFrmApp (VOID);
UINT4  RmApiAllProtocolRestart (VOID);
UINT4  RmApiInitiateBulkRequest (VOID);
tRmMsg *  RmApiAllocTxBuf (UINT4 u4SyncMsgSize);
VOID   RmApiIncrementRebootCntr (VOID);
UINT4 RmApiIssuMaintenanceModeEnable PROTO ((VOID));
UINT1
RmApiBulkUpdtStatus PROTO ((VOID));

/* Hitless Restart structure contains the function pointers of storing and
 * restoring functions for memory/flash/file.
 */
typedef struct {
    INT4 (*pFnRmHRBulkStore) (UINT1 *pu1Data, UINT4 u4Length, UINT4 u4SrcId);
    /* Function pointer to store the bulk updates in protocol's file
     * by default */
    INT4 (*pFnRmHRBulkRestore) (UINT4);
    /* Function pointer to restore the bulk updates from protocol's file
     * by default */
    UINT1 u1StorageType;
    /* whether the dynamic messages are stored in a File, Flash or Memory */
    UINT1 au1Reserved[3]; /* added for padding */
}tRmHRInfo;

/* Enum for HITLESS RESTART - storage types */
enum
{
    RM_HR_FILE_STORAGE = 0,
    RM_HR_FLASH_STORAGE,
    RM_HR_MEMORY_STORAGE,
    RM_HR_MAX_STORAGE
};

enum
{
    RM_HR_STDY_ST_PKT_REQ  = 61,
    RM_HR_STDY_ST_PKT_MSG  = 62,
    RM_HR_STDY_ST_PKT_TAIL = 63
};

#define RM_HR_STATUS_DISABLE           0
#define RM_HR_STATUS_STORE             1
#define RM_HR_STATUS_RESTORE           2

#define RM_HB_MODE  IssGetHeartBeatModeFromNvRam ()
#define RM_RTYPE    IssGetRedundancyTypeFromNvRam ()
#define RM_DTYPE    IssGetRmDataPlaneFromNvRam ()

#define RM_HR_GET_1BYTE_FROM_LIN_BUF(pu1Buf,u4Offset,u1Value) \
{\
    MEMCPY (&u1Value, (pu1Buf + u4Offset), sizeof (UINT1));\
}

#define RM_HR_GET_2BYTE_FROM_LIN_BUF(pu1Buf,u4Offset,u2Value) \
{\
    MEMCPY (&u2Value, (pu1Buf + u4Offset), sizeof (UINT2));\
    u2Value = OSIX_NTOHS (u2Value);\
}

#define RM_HR_GET_4BYTE_FROM_LIN_BUF(pu1Buf,u4Offset,u4Value) \
{\
    MEMCPY (&u4Value, (pu1Buf + u4Offset), sizeof (UINT4));\
    u4Value = OSIX_NTOHL (u4Value);\
}
    
VOID   RmInformOtherModulesBootUp (INT1 *);
VOID  RmHRRegister      (tRmHRInfo *);
VOID  RmHRDeRegister    (VOID);
UINT1 RmGetHRFlag       (VOID);

/* function to check bulk update completion */
INT4  RmIsBulkUpdateComplete(VOID);
VOID  RmApiTcpSrvSockInit (VOID);
VOID RmUtilSetFileTransferStatus (INT4 , UINT1);
INT4 RmUtilGetFileIndexFromName(UINT1*);
VOID RmApiPostPeerEventToProtocols (UINT1 u1Event);

INT4 RmGetFtSrvSockFd(VOID);
INT4 RmGetTcpSrvSockFd(VOID);


#endif /* _RMGR_H */
