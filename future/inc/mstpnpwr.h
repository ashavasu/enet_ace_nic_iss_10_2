/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: mstpnpwr.h,v 1.4 2016/08/20 09:40:09 siva Exp $
 *
 * Description:This file contains the necessary data strucutures
 *             for packet handling in NPAPI layer
 *             <Part of dual node stacking model>
 *
 *******************************************************************/

#ifndef __MSTP_NP_WR_H__
#define __MSTP_NP_WR_H__

#include "mstp.h"
#include "fsvlan.h"
#ifdef MBSM_WANTED
#include "mbsm.h"
#endif

/* OPER ID */
enum 
{
 FS_MI_MSTP_NP_CREATE_INSTANCE = 1,
 FS_MI_MSTP_NP_ADD_VLAN_INST_MAPPING,
 FS_MI_MSTP_NP_ADD_VLAN_LIST_INST_MAPPING,
 FS_MI_MSTP_NP_DELETE_INSTANCE,
 FS_MI_MSTP_NP_DEL_VLAN_INST_MAPPING,
 FS_MI_MSTP_NP_DEL_VLAN_LIST_INST_MAPPING,
 FS_MI_MSTP_NP_SET_INSTANCE_PORT_STATE,
 FS_MI_MSTP_NP_INIT_HW,
 FS_MI_MSTP_NP_DE_INIT_HW,
 FS_MI_MST_NP_GET_PORT_STATE,
#ifdef  MBSM_WANTED
 FS_MI_MSTP_MBSM_NP_CREATE_INSTANCE,
 FS_MI_MSTP_MBSM_NP_ADD_VLAN_INST_MAPPING,
 FS_MI_MSTP_MBSM_NP_SET_INSTANCE_PORT_STATE,
 FS_MI_MSTP_MBSM_NP_INIT_HW,
#endif /* MBSM_WANTED */
 FS_NP_MSTP_MAX_NP 
};
/* Required arguments list for the mstp NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

typedef struct {
    UINT4  u4ContextId;
    UINT2  u2InstId;
    UINT1  au1Pad[2];
} tMstpNpWrFsMiMstpNpCreateInstance;

typedef struct {
    UINT4    u4ContextId;
    tVlanId  VlanId;
    UINT2    u2InstId;
} tMstpNpWrFsMiMstpNpAddVlanInstMapping;

typedef struct {
    UINT4    u4ContextId;
    UINT1 *  pu1VlanList;
    UINT2    u2InstId;
    UINT2    u2NumVlans;
    UINT2 *  pu2LastVlan;
} tMstpNpWrFsMiMstpNpAddVlanListInstMapping;

typedef struct {
    UINT4  u4ContextId;
    UINT2  u2InstId;
    UINT1  au1Pad[2];
} tMstpNpWrFsMiMstpNpDeleteInstance;

typedef struct {
    UINT4    u4ContextId;
    tVlanId  VlanId;
    UINT2    u2InstId;
} tMstpNpWrFsMiMstpNpDelVlanInstMapping;

typedef struct {
    UINT4    u4ContextId;
    UINT1 *  pu1VlanList;
    UINT2    u2InstId;
    UINT2    u2NumVlans;
    UINT2 *  pu2LastVlan;
} tMstpNpWrFsMiMstpNpDelVlanListInstMapping;

typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4ContextId;
    UINT2  u2InstanceId;
    UINT1  u1PortState;
    UINT1  au1Pad[1];
} tMstpNpWrFsMiMstpNpSetInstancePortState;

typedef struct {
    UINT4  u4ContextId;
} tMstpNpWrFsMiMstpNpInitHw;

typedef struct {
    UINT4  u4ContextId;
} tMstpNpWrFsMiMstpNpDeInitHw;

typedef struct {
    UINT4    u4IfIndex;
    UINT4    u4ContextId;
    UINT1 *  pu1Status;
    UINT2    u2InstanceId;
    UINT1    au1Pad[2];
} tMstpNpWrFsMiMstNpGetPortState;

#ifdef  MBSM_WANTED
typedef struct {
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
    UINT2            u2InstId;
    UINT1            au1Pad[2];
} tMstpNpWrFsMiMstpMbsmNpCreateInstance;

typedef struct {
    UINT4            u4ContextId;
    tVlanId          VlanId;
    UINT2            u2InstId;
    tMbsmSlotInfo *  pSlotInfo;
} tMstpNpWrFsMiMstpMbsmNpAddVlanInstMapping;

typedef struct {
    UINT4            u4IfIndex;
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
    UINT2            u2InstanceId;
    UINT1            u1PortState;
    UINT1            au1Pad[1];
} tMstpNpWrFsMiMstpMbsmNpSetInstancePortState;

typedef struct {
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
} tMstpNpWrFsMiMstpMbsmNpInitHw;

#endif /* MBSM_WANTED */
typedef struct MstpNpModInfo {
union {
    tMstpNpWrFsMiMstpNpCreateInstance  sFsMiMstpNpCreateInstance;
    tMstpNpWrFsMiMstpNpAddVlanInstMapping  sFsMiMstpNpAddVlanInstMapping;
    tMstpNpWrFsMiMstpNpAddVlanListInstMapping  sFsMiMstpNpAddVlanListInstMapping;
    tMstpNpWrFsMiMstpNpDeleteInstance  sFsMiMstpNpDeleteInstance;
    tMstpNpWrFsMiMstpNpDelVlanInstMapping  sFsMiMstpNpDelVlanInstMapping;
    tMstpNpWrFsMiMstpNpDelVlanListInstMapping  sFsMiMstpNpDelVlanListInstMapping;
    tMstpNpWrFsMiMstpNpSetInstancePortState  sFsMiMstpNpSetInstancePortState;
    tMstpNpWrFsMiMstpNpInitHw  sFsMiMstpNpInitHw;
    tMstpNpWrFsMiMstpNpDeInitHw  sFsMiMstpNpDeInitHw;
    tMstpNpWrFsMiMstNpGetPortState  sFsMiMstNpGetPortState;
#ifdef  MBSM_WANTED
    tMstpNpWrFsMiMstpMbsmNpCreateInstance  sFsMiMstpMbsmNpCreateInstance;
    tMstpNpWrFsMiMstpMbsmNpAddVlanInstMapping  sFsMiMstpMbsmNpAddVlanInstMapping;
    tMstpNpWrFsMiMstpMbsmNpSetInstancePortState  sFsMiMstpMbsmNpSetInstancePortState;
    tMstpNpWrFsMiMstpMbsmNpInitHw  sFsMiMstpMbsmNpInitHw;
#endif /* MBSM_WANTED */
    }unOpCode;

#define  MstpNpFsMiMstpNpCreateInstance  unOpCode.sFsMiMstpNpCreateInstance;
#define  MstpNpFsMiMstpNpAddVlanInstMapping  unOpCode.sFsMiMstpNpAddVlanInstMapping;
#define  MstpNpFsMiMstpNpAddVlanListInstMapping  unOpCode.sFsMiMstpNpAddVlanListInstMapping;
#define  MstpNpFsMiMstpNpDeleteInstance  unOpCode.sFsMiMstpNpDeleteInstance;
#define  MstpNpFsMiMstpNpDelVlanInstMapping  unOpCode.sFsMiMstpNpDelVlanInstMapping;
#define  MstpNpFsMiMstpNpDelVlanListInstMapping  unOpCode.sFsMiMstpNpDelVlanListInstMapping;
#define  MstpNpFsMiMstpNpSetInstancePortState  unOpCode.sFsMiMstpNpSetInstancePortState;
#define  MstpNpFsMiMstpNpInitHw  unOpCode.sFsMiMstpNpInitHw;
#define  MstpNpFsMiMstpNpDeInitHw  unOpCode.sFsMiMstpNpDeInitHw;
#define  MstpNpFsMiMstNpGetPortState  unOpCode.sFsMiMstNpGetPortState;
#ifdef  MBSM_WANTED
#define  MstpNpFsMiMstpMbsmNpCreateInstance  unOpCode.sFsMiMstpMbsmNpCreateInstance;
#define  MstpNpFsMiMstpMbsmNpAddVlanInstMapping  unOpCode.sFsMiMstpMbsmNpAddVlanInstMapping;
#define  MstpNpFsMiMstpMbsmNpSetInstancePortState  unOpCode.sFsMiMstpMbsmNpSetInstancePortState;
#define  MstpNpFsMiMstpMbsmNpInitHw  unOpCode.sFsMiMstpMbsmNpInitHw;
#endif /* MBSM_WANTED */
} tMstpNpModInfo;


/* Function Prototype for Generic NP OpCode specific wrappers
 * defined in Mstpnpapi.c */

UINT1 MstpFsMiMstpNpCreateInstance PROTO ((UINT4 u4ContextId, UINT2 u2InstId));
UINT1 MstpFsMiMstpNpAddVlanInstMapping PROTO ((UINT4 u4ContextId, tVlanId VlanId, UINT2 u2InstId));
UINT1 MstpFsMiMstpNpAddVlanListInstMapping PROTO ((UINT4 u4ContextId, UINT1 * pu1VlanList, UINT2 u2InstId, UINT2 u2NumVlans, UINT2 * pu2LastVlan));
UINT1 MstpFsMiMstpNpDeleteInstance PROTO ((UINT4 u4ContextId, UINT2 u2InstId));
UINT1 MstpFsMiMstpNpDelVlanInstMapping PROTO ((UINT4 u4ContextId, tVlanId VlanId, UINT2 u2InstId));
UINT1 MstpFsMiMstpNpDelVlanListInstMapping PROTO ((UINT4 u4ContextId, UINT1 * pu1VlanList, UINT2 u2InstId, UINT2 u2NumVlans, UINT2 * pu2LastVlan));
UINT1 MstpFsMiMstpNpSetInstancePortState PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2InstanceId, UINT1 u1PortState));
UINT1 MstpFsMiMstpNpInitHw PROTO ((UINT4 u4ContextId));
UINT1 MstpFsMiMstpNpDeInitHw PROTO ((UINT4 u4ContextId));
UINT1 MstpFsMiMstNpGetPortState PROTO ((UINT4 u4ContextId, UINT2 u2InstanceId, UINT4 u4IfIndex, UINT1 * pu1Status));
#ifdef  MBSM_WANTED
UINT1 MstpFsMiMstpMbsmNpCreateInstance PROTO ((UINT4 u4ContextId, UINT2 u2InstId, tMbsmSlotInfo * pSlotInfo));
UINT1 MstpFsMiMstpMbsmNpAddVlanInstMapping PROTO ((UINT4 u4ContextId, tVlanId VlanId, UINT2 u2InstId, tMbsmSlotInfo * pSlotInfo));
UINT1 MstpFsMiMstpMbsmNpSetInstancePortState PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2InstanceId, UINT1 u1PortState, tMbsmSlotInfo * pSlotInfo));
UINT1 MstpFsMiMstpMbsmNpInitHw PROTO ((UINT4 u4ContextId, tMbsmSlotInfo * pSlotInfo));
#endif /* MBSM_WANTED */

#endif /* __MSTP_NP_WR_H__ */
