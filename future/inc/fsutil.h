/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsutil.h,v 1.22 2015/06/15 06:51:57 siva Exp $
 *
 * Description: Contains common definitions, macros and functions to be 
 *              used by other modules.
 ********************************************************************/
#ifndef _FSUTIL_H
#define _FSUTIL_H

#include "lr.h"
#include "fsvlan.h"
#include "utilsz.h"
#include "dns.h"
/* This MACRO defines the minimum size and block alignment.
 * It needs to be 4 byte aligned for 32 bit and 8 byte for 64 bit
 * processors
 * */

#define BUDDY_MIN_BLK_SIZE  sizeof(FS_ULONG)
#define RAND_SEED_LEN       16
#define RAND_GEN_LEN        32

#define FS_ULONG_MAX       0xffffffff

/* Macro to check the given mac address is a multicast mac or not */
#define FS_UTIL_IS_MCAST_MAC(pMacAddr) \
       (((pMacAddr[0] & 0x01) != 0) ? OSIX_TRUE : OSIX_FALSE)

#define FS_UTIL_MAX_LINE_LEN  256

#define FS_UTIL_SPL_CHAR      '>'

VOID    FsUtilBitListInit PROTO ((UINT4));

UINT1 * FsUtilAllocBitList PROTO ((UINT4));

VOID    FsUtilReleaseBitList PROTO ((UINT1 *));

INT4    FsUtilBitListIsAllZeros PROTO ((UINT1 *, UINT4 ));

VOID    UtilPlstInitLocalPortList PROTO ((UINT4));

UINT1 * UtilPlstAllocLocalPortList PROTO ((UINT4));

VOID    UtilPlstReleaseLocalPortList PROTO ((UINT1 *));

VOID    UtilVlanInitVlanListSize PROTO ((UINT4));

UINT1 * UtilVlanAllocVlanListSize PROTO ((UINT4));

VOID    UtilVlanReleaseVlanListSize PROTO ((UINT1 *));

VOID    UtilVlanInitVlanIdSize PROTO ((UINT4));

UINT1 * UtilVlanAllocVlanIdSize PROTO ((UINT4));

VOID    UtilVlanReleaseVlanIdSize PROTO ((UINT1 *));

INT1    FsUtlReadLine PROTO ((INT4 i4Filefd, INT1 *pi1Buf));
INT1    FsUtlEnhReadLine PROTO ((INT4 i4Filefd, INT1 *pi1Buf, UINT4 u4Len));

INT4    FsUtlResolveHostName PROTO ((UINT1 *pu1HostName, UINT4 *pu4IPAddr));

INT4    FsUtlIPvXResolveHostName PROTO ((UINT1 *, UINT1, tDNSResolvedIpInfo *));

VOID    FsUtlEncryptPasswd (CHR1 *);

VOID    FsUtlDecryptPasswd (CHR1 *);

VOID    FsUtlGetRandom PROTO((UINT1 *pu1Rand, UINT4 u4RandLen));

VOID    FsUtlGetUniqueRandom PROTO((UINT1 *pu1Rand, UINT4 u4RandLen));

UINT1 * UtlShMemAllocTacMsgOutput PROTO ((VOID));

VOID UtlShMemFreeTacMsgOutput PROTO ((UINT1 *pTacMsgOutput));

UINT1 * UtlShMemAllocTacMsgInput PROTO ((VOID));

VOID
UtlShMemFreeTacMsgInput PROTO ((UINT1 *pTacMsgInput));

UINT1 * UtlShMemAllocRadInterface PROTO ((VOID));

VOID
UtlShMemFreeRadInterface PROTO ((UINT1 * pIface));
UINT4 * UtlShMemAllocOifList PROTO ((VOID));
VOID
UtlShMemFreeOifList PROTO ((UINT4 *pu4OifList));

UINT1 * UtlShMemAllocVlanList PROTO ((VOID));
VOID UtlShMemFreeVlanList PROTO ((UINT1 *pVlanList));

VOID UtlShMemInitPoolIds PROTO ((VOID));

VOID UtilMemMove PROTO ((VOID *pDst, CONST VOID *pSrc, UINT4 u4Len));
FS_ULONG UtilStrtoul PROTO ((CHR1 *pc1Str, CHR1 **ppc1EndPtr, INT4 i4Base));
INT4 UtilStrcspn PROTO ((CHR1 *pc1Str, CHR1 *pc1Reject));


UINT1 *UtilWebAllocBufferSize PROTO ((VOID));
VOID UtilWebReleaseBufferSize (UINT1 *);


UINT1*  UtilRadKeyAllocBufferSize PROTO ((VOID));
VOID    UtilRadKeyReleaseBufferSize PROTO ((UINT1 *pu1BufferSize));

PUBLIC UINT4 UtilCalculateCRC32 PROTO ((UINT1 *pData, UINT4 u4Len));
PUBLIC UINT1 UtilStrToLower (UINT1 *);
#if defined (WLC_WANTED) || defined (WTP_WANTED)
UINT1 * UtlShMemAllocWlcBuf PROTO ((VOID));
VOID UtlShMemFreeWlcBuf PROTO ((UINT1 *pWlcBuf));
UINT1 * UtlShMemAllocWssWlanBuf PROTO ((VOID));
VOID UtlShMemFreeWssWlanBuf PROTO ((UINT1 *pWssWlanBuf));
UINT1* UtlShMemAllocAntennaSelectionBuf PROTO ((VOID));
VOID   UtlShMemFreeAntennaSelectionBuf PROTO ((UINT1 *pu1AntennaSelection));
UINT1* UtlShMemAllocWlanDbBuf PROTO ((VOID));
VOID   UtlShMemFreeWlanDbBuf PROTO ((UINT1 *pwssWlanDb));
UINT1* UtlShMemAllocMacMsgStructBuf PROTO ((VOID));
VOID   UtlShMemFreeMacMsgStructBuf PROTO ((UINT1 *pWssMacMsgRspStruct));
UINT1* UtlShMemAllocCapwapPktBuf PROTO ((VOID));
VOID   UtlShMemFreeCapwapPktBuf PROTO ((UINT1 *pu1Frame));
UINT1* UtlShMemAllocConfigStatusRspBuf PROTO ((VOID));
VOID   UtlShMemFreeConfigStatusRspBuf PROTO ((UINT1 *pConfigStatusRsp));
UINT1* UtlShMemAllocWssIfAuthDbBuf PROTO ((VOID));
VOID   UtlShMemFreeWssIfAuthDbBuf PROTO ((UINT1 *pWssIfAuthDb));
UINT1* UtlShMemAllocAcInfoBuf PROTO ((VOID));
VOID   UtlShMemFreeAcInfoBuf PROTO ((UINT1 *pAcInfoAfterDisc));
UINT1* UtlShMemAllocJoinReqBuf PROTO ((VOID));
VOID   UtlShMemFreeJoinReqBuf PROTO ((UINT1 *pJoinReq));
UINT1* UtlShMemAllocDiscRspBuf PROTO ((VOID));
VOID   UtlShMemFreeDiscRspBuf PROTO ((UINT1 *pDiscRsp));
UINT1* UtlShMemAllocDataReqBuf PROTO ((VOID));
VOID   UtlShMemFreeDataReqBuf PROTO ((UINT1 *pDataReq));
UINT1* UtlShMemAllocJoinRspBuf PROTO ((VOID));
VOID   UtlShMemFreeJoinRspBuf PROTO ((UINT1 *pJoinRsp));
UINT1* UtlShMemAllocConfigStatusReqBuf PROTO ((VOID));
VOID   UtlShMemFreeConfigStatusReqBuf PROTO ((UINT1 *pConfigStatusReq));
#endif

#endif /*_FSUTIL_H */
