
#ifndef _CHRDEV_CXE_H_
#define _CHRDEV_CXE_H_

#include "fsvlan.h"

/* IOCTL structures */
typedef struct {
    INT4       i4Cmd;
    HCXE       hCXe;
    UINT4      u4IpAddr;
    UINT4      u4IpSubnetMask;
    UINT1      au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    tVlanId    u2VlanId;
    INT4       i4IpPortNum;
    UINT1      au1MacAddr[CFA_ENET_ADDR_LEN];
} tNpIpIoctlParams;

typedef struct {
    INT4       i4Cmd;
} tNpIpMcIoctlParams;

typedef struct {
    INT4       i4Cmd;
    HCXE       hCXe;
    UINT4      u4IpAddr;
    tVlanId    u2VlanId;
    UINT1      au1MacAddr[CFA_ENET_ADDR_LEN];
} tNpVrrpIoctlParams;

typedef union {
    tNpIpIoctlParams   IpPrms;
    tNpIpMcIoctlParams IpMcPrms;
    tNpVrrpIoctlParams VrrpPrms;
}uNpIoctlParams;

typedef struct { 
    UINT4             u4IfIndex; /* CFA IfIndex on which the packet 
                                    is received */
    UINT4             u4PktLen; /* Packet len according to flags */
}tHandlePacketRxCallBack;

typedef struct _tdata {
   tHeader hdr;
   union {
   tHandlePacketRxCallBack  RxCbk;
   } u;
}tData;

typedef enum {
   PACKET_RX,
}tCallBackEvent;


#define GDD_PORT_OPER_STATUS 1

#define FS_NP_IOCTL_CREATE_IP_INTERFACE        1
#define FS_NP_IOCTL_MODIFY_IP_INTERFACE        2
#define FS_NP_IOCTL_DELETE_IP_INTERFACE        3
#define FS_NP_IOCTL_CREATE_VRRP_INTERFACE      4
#define FS_NP_IOCTL_DELETE_VRRP_INTERFACE      5
#define FS_PIM_NP_INIT_HW                      6
#define FS_PIM_NP_DE_INIT_HW                   7
#define FS_DVMRP_NP_INIT_HW                    8
#define FS_DVMRP_NP_DE_INIT_HW                 9
#define FS_IGMP_HW_ENABLE_IGMP                10
#define FS_IGMP_HW_DISABLE_IGMP               11
#define FS_NP_IGMP_PROXY_INIT                 12
#define FS_NP_IGMP_PROXY_DE_INIT              13

#endif /* _CHRDEV_CXE_H_ */
