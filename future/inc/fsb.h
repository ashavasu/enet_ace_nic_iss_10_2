/*************************************************************************
* Copyright (C) 2007-2015 Aricent Group . All Rights Reserved
*
* $Id: fsb.h,v 1.13 2017/10/09 13:13:59 siva Exp $
*
* Description: This header file contains all the macros and protoype
*              of FIP-snooping Module that is commonly used by other modules.
*
*************************************************************************/
#ifndef _FSB_H_
#define _FSB_H_

#include "cust.h"
#include "mbsm.h"

/* FSB Return Related Macros */
#define FSB_SUCCESS             OSIX_SUCCESS 
#define FSB_FAILURE             OSIX_FAILURE
#define FSB_IN_PROGRESS         2

/* FSB Packet Related Macros */
#define FSB_ETHER_TYPE_LEN   2
#define FSB_ETHER_TYPE_OFFSET 16
#define FSB_SCHANNEL_ETHER_TYPE_OFFSET 20
#define FSB_ETHER_TYPE  0x8914
#define FSB_FCOE_ETHER_TYPE 0x8906
#define VLAN_ETHER_TYPE  0x8100
#define VLAN_ETHER_TYPE_LEN   2
#define VLAN_ID_OFFSET 14
#define FSB_EVB_OPCODE_OFFSET 24  /* Double-tagged VLAN_DISCOVERY_PKT*/
#define FSB_OPCODE_OFFSET 20      /* Single-tagged VLAN_DISCOVERY_PKT*/

/* ACL MODE */
#define FSB_FILTER_PORT_MODE                1
#define FSB_FILTER_GLOBAL_MODE              2

#define FSB_KEEP_ALIVE                      3



/* FSAP Related Macros */
#define FSB_FALSE                           OSIX_FALSE
#define FSB_TRUE                            OSIX_TRUE

/*FSB Status of VLAN Entry*/
#define FSB_VLAN_ENABLED                    1
#define FSB_VLAN_DISABLED                   2

/* The following macros are used as metering values for filter installed to 
 * lift FIP VLAN REQUEST and SOLICITAION MCAST packet 
 * NP_FSB_CIR - Committed Information Rate 
 * NP_FSB_EIR - Exceeded Information Rate */
#define NP_FSB_CIR                          128 
#define NP_FSB_EIR                          128

/* Hw Filter Table - Contains Hw specific attributes for Filter Entry */
typedef struct FsbHwFilterEntry
{
    tPortList       PortList;                     /* Port List for Port Specific Filters */
    UINT4           u4ContextId;                  /* Context Id */
    UINT4           u4AggIndex;                   /* Aggregation Interface index */
    UINT4           u4PinnnedPortIfIndex;         /* Pinned Port Interface index */
    tMacAddr        DstMac;                       /* Destination MAC address */
    tMacAddr        SrcMac;                       /* Source MAC address*/
    UINT2           u2VlanId;                     /* FCoE VLAN identifier*/
    UINT2           u2ClassId;                     /* Class identifier*/
    UINT2           u2Ethertype;                  /* Ether type */
    UINT2           u2OpcodeFilterOffset;         /* Filter offset value*/
    UINT2           u2OpcodeFilterOffsetValue;    /* Filter offset value*/
    UINT2           u2OpcodeFilterOffsetMask;     /* Filter offset mask*/
    UINT2           u2SubOpcodeFilterOffset;      /* Filter offset value*/
    UINT2           u2SubOpcodeFilterOffsetValue; /* Filter offset value*/
    UINT2           u2SubOpcodeFilterOffsetMask;  /* Filter offset mask/ */
    UINT1           u1FilterAction;               /* Filter Action */
    UINT1           u1FilterType;                 /* Specifies the filter type (global/port-based) */
    UINT1           u1Priority;                   /* Specifies the filter priority */
    UINT1           u1DefFilterType;            /* Specifies the Default filter type */
    UINT1           u1Pad[2];               
}tFsbHwFilterEntry;

typedef struct FsbHwSChannelFilterEntry
{
    UINT4           u4IfIndex;                    /* Aggregation Interface index */
    UINT2           u2VlanId;                     /* FCoE VLAN identifier*/
    UINT1           u1Priority;                   /* Specifies the filter priority */
    UINT1           au1Pad[1];                    /* Padding byte*/
}tFsbHwSChannelFilterEntry;

/* Filter Id - Contains Filter ID specific to Remote and Local */
typedef struct FsbLocRemFilterId
{
    UINT4           u4LocalFilterId;              /* Local Filter Id */
    UINT4           u4RemoteFilterId;             /* Remote Filter Id */
}tFsbLocRemFilterId;

typedef struct FsbHwVlanEntry
{
    UINT4           u4ContextId;                  /* Context Id */
    UINT2           u2VlanId;                     /* FCoE VLAN identifier*/
    UINT1           u1EnabledStatus;              /* Value to indicate Enabled/Disabled Status */
    UINT1           u1Pad;
}tFsbHwVlanEntry;

/* This enum is used to specify the Filter Action */
typedef enum {
    FSB_FILTER_ALLOW_AND_COPYTOCPU = 1,
    FSB_FILTER_ALLOW,
    FSB_FILTER_DENY_AND_DONOTLEARN,
}tFsbFilterAction;

/* This enum is used to specify the type of filter */
typedef enum {
    FSB_VLAN_DISC_FILTER = 1,
    FSB_VLAN_RESP_TAGGED_FILTER,
    FSB_DEFAULT_FILTER,
    FSB_SESSION_FILTER
}tFsbDefFilterAction;

/* FSB call back events */
typedef enum {
    FSB_SET_IF_MTU_EVENT,
    FSB_MAX_CALLBACK_EVENTS
}tFsbCallBackEvents;

/* Prototypes for FSB Functions used by other modules */
PUBLIC VOID FsbTaskMain PROTO ((INT1 *));
INT4 FsbLock PROTO ((VOID));
INT4 FsbUnLock PROTO ((VOID));
INT4 FsbSendPacketToFsbQueue PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf, UINT4, UINT4, UINT2));
INT4 FsbPortListUpdateNotification PROTO((UINT4, UINT2, tLocalPortList, tLocalPortList));
INT4 FsbDeleteVlanIndication PROTO((UINT4, UINT2));
INT4 FsbApiNotifyPortDown PROTO ((UINT4, UINT1)); 
INT4 FsbApiNotifyPortDiscarding PROTO ((UINT4)); 
INT4 FsbApiDeleteContext PROTO ((UINT4));
INT4 FsbApiUnmapPort PROTO ((UINT4));
UINT1 FsbApiIsFsbEnabled PROTO ((VOID));
INT4 FsbApiMbsmNotification PROTO ((tMbsmProtoMsg*, INT4));
INT4 FsbCustSetIfMtu (UINT4 u4IfIndex);
INT4 FsbCallBackRegister (UINT4 u4Event, tFsCbInfo *pFsCbInfo);
INT4 FsbApiAddPortToPortChannel PROTO ((UINT4, UINT2));
INT4 FsbApiRemovePortFromPortChannel PROTO ((UINT4, UINT2));
INT4 FsbPortUpdateNotification PROTO  ((UINT4, UINT2, UINT2));
INT4 FsbGetPhyIfIndexforVlan (UINT2, UINT4 *);
INT4 FsbProcessFsbFrame PROTO ((tCRU_BUF_CHAIN_HEADER *, UINT4, UINT2 ));
INT4 FsbGetValidPhyIfIndexforVlan PROTO((UINT2, UINT4 *));
INT4 FsbApiPortRemoveFromVlan PROTO((UINT4, UINT2, UINT4));
INT4 FsbIsFCoEVlan PROTO ((UINT4 u4ContextId, UINT2 u2VlanIndex));
INT4 FsbGetPinnedPortForFCoEVlan PROTO ((UINT4 , UINT2 , UINT4 *));
#endif
