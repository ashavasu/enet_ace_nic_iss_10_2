/*************************************************************************
 * * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * * $Id: fsmid1evbon.h,v 1.4 2016/07/16 11:15:02 siva Exp $
 * * Description: This header file contains all OID information
 * *              related to EVB module.
 * **********************************************************************/
{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"ccitt","0"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"iso","1"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"lldpExtensions","1.0.8802.1.1.2.1.5"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"org","1.3"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"dod","1.3.6"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"internet","1.3.6.1"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"directory","1.3.6.1.1"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"mgmt","1.3.6.1.2"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"mib-2","1.3.6.1.2.1"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"ip","1.3.6.1.2.1.4"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"transmission","1.3.6.1.2.1.10"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"mplsStdMIB","1.3.6.1.2.1.10.166"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"rmon","1.3.6.1.2.1.16"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"statistics","1.3.6.1.2.1.16.1"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"etherStatsHighCapacityEntry","1.3.6.1.2.1.16.1.7.1"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"history","1.3.6.1.2.1.16.2"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"hosts","1.3.6.1.2.1.16.4"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"hostTopN","1.3.6.1.2.1.16.5"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"matrix","1.3.6.1.2.1.16.6"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"filter","1.3.6.1.2.1.16.7"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"capture","1.3.6.1.2.1.16.8"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"tokenRing","1.3.6.1.2.1.16.10"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"protocolDist","1.3.6.1.2.1.16.12"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"nlHost","1.3.6.1.2.1.16.14"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"nlMatrix","1.3.6.1.2.1.16.15"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"alHost","1.3.6.1.2.1.16.16"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"alMatrix","1.3.6.1.2.1.16.17"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"usrHistory","1.3.6.1.2.1.16.18"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"probeConfig","1.3.6.1.2.1.16.19"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"rmonConformance","1.3.6.1.2.1.16.20"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"dot1dBridge","1.3.6.1.2.1.17"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"dot1dStp","1.3.6.1.2.1.17.2"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"dot1dTp","1.3.6.1.2.1.17.4"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"vrrpOperEntry","1.3.6.1.2.1.18.1.3.1"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"dns","1.3.6.1.2.1.32"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"experimental","1.3.6.1.3"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"private","1.3.6.1.4"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"enterprises","1.3.6.1.4.1"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"ARICENT-MPLS-TP-MIB","1.3.6.1.4.1.2076.13.4"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"issExt","1.3.6.1.4.1.2076.81.8"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsDot1dBridge","1.3.6.1.4.1.2076.116"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsDot1dStp","1.3.6.1.4.1.2076.116.2"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsDot1dTp","1.3.6.1.4.1.2076.116.4"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"futureEvb","1.3.6.1.4.1.29601.2.104"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsMIEvb","1.3.6.1.4.1.29601.2.104.1"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsMIEvbSystemGroup","1.3.6.1.4.1.29601.2.104.1.1"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsMIEvbSystemTable","1.3.6.1.4.1.29601.2.104.1.1.1"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsMIEvbSystemEntry","1.3.6.1.4.1.29601.2.104.1.1.1.1"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsMIEvbSystemContextId","1.3.6.1.4.1.29601.2.104.1.1.1.1.1"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsMIEvbSystemControl","1.3.6.1.4.1.29601.2.104.1.1.1.1.2"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsMIEvbSystemModuleStatus","1.3.6.1.4.1.29601.2.104.1.1.1.1.3"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsMIEvbSystemTraceLevel","1.3.6.1.4.1.29601.2.104.1.1.1.1.4"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsMIEvbSystemTrapStatus","1.3.6.1.4.1.29601.2.104.1.1.1.1.5"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsMIEvbSystemStatsClear","1.3.6.1.4.1.29601.2.104.1.1.1.1.6"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsMIEvbSchannelIdMode","1.3.6.1.4.1.29601.2.104.1.1.1.1.7"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsMIEvbSystemRowStatus","1.3.6.1.4.1.29601.2.104.1.1.1.1.8"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsMIEvbConfigGroup","1.3.6.1.4.1.29601.2.104.1.2"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsMIEvbCAPConfigTable","1.3.6.1.4.1.29601.2.104.1.2.1"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsMIEvbCAPConfigEntry","1.3.6.1.4.1.29601.2.104.1.2.1.1"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsMIEvbCAPSChannelID","1.3.6.1.4.1.29601.2.104.1.2.1.1.1"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsMIEvbCAPSChannelIfIndex","1.3.6.1.4.1.29601.2.104.1.2.1.1.2"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsMIEvbCAPSChNegoStatus","1.3.6.1.4.1.29601.2.104.1.2.1.1.3"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsMIEvbPhyPort","1.3.6.1.4.1.29601.2.104.1.2.1.1.4"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsMIEvbSchID","1.3.6.1.4.1.29601.2.104.1.2.1.1.5"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsMIEvbSChannelFilterStatus","1.3.6.1.4.1.29601.2.104.1.2.1.1.6"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsMIEvbUAPConfigTable","1.3.6.1.4.1.29601.2.104.1.2.2"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsMIEvbUAPConfigEntry","1.3.6.1.4.1.29601.2.104.1.2.2.1"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsMIEvbUAPSchCdcpMode","1.3.6.1.4.1.29601.2.104.1.2.2.1.1"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsMIEvbStatsGroup","1.3.6.1.4.1.29601.2.104.1.3"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsMIEvbUAPStatsTable","1.3.6.1.4.1.29601.2.104.1.3.1"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsMIEvbUAPStatsEntry","1.3.6.1.4.1.29601.2.104.1.3.1.1"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsMIEvbTxCdcpCount","1.3.6.1.4.1.29601.2.104.1.3.1.1.1"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsMIEvbRxCdcpCount","1.3.6.1.4.1.29601.2.104.1.3.1.1.2"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsMIEvbSChAllocFailCount","1.3.6.1.4.1.29601.2.104.1.3.1.1.3"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsMIEvbSChActiveFailCount","1.3.6.1.4.1.29601.2.104.1.3.1.1.4"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsMIEvbSVIDPoolExceedsCount","1.3.6.1.4.1.29601.2.104.1.3.1.1.5"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsMIEvbCdcpRejectStationReq","1.3.6.1.4.1.29601.2.104.1.3.1.1.6"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsMIEvbCdcpOtherDropCount","1.3.6.1.4.1.29601.2.104.1.3.1.1.7"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsMIEvbTrapsGroup","1.3.6.1.4.1.29601.2.104.1.4"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"fsMIEvbTraps","1.3.6.1.4.1.29601.2.104.1.4.0"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"security","1.3.6.1.5"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"snmpV2","1.3.6.1.6"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"snmpDomains","1.3.6.1.6.1"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"snmpProxys","1.3.6.1.6.2"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"snmpModules","1.3.6.1.6.3"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"snmpTraps","1.3.6.1.6.3.1.1.5"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"snmpAuthProtocols","1.3.6.1.6.3.10.1.1"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"snmpPrivProtocols","1.3.6.1.6.3.10.1.2"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"ieee802dot1mibs","1.3.111.2.802.1.1"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"lldpV2Xdot1MIB","1.3.111.2.802.1.1.13.1.5.32962"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"lldpXdot1StandAloneExtensions","1.3.111.2.802.1.1.13.1.5.32962.7"},

{{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},"joint-iso-ccitt","2"},

