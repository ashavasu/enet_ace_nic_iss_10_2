
/**********************************************************************
 * Copyright (C) Future Software Limited,1997-98,2001
 *
 * $Id: secv4.h,v 1.16 2012/08/24 09:00:15 siva Exp $
 *
 * Description: This has macros and proto type cmn to ip4 and ipsec 
 *
 ***********************************************************************/



#ifndef _SECV4_H_
#define _SECV4_H_

/* Macros Export to IPv4 */
#define SEC_AH                   51 
#define SEC_ESP                  50
#define SEC_NO_NEXT_HDR          59
#define SEC_UDP                  17 
#define SEC_FAILURE              0
#define SEC_SUCCESS              1
#define SEC_DROP                 2
#define SEC_ENCODE               1 
#define SEC_DECODE               2
#define SEC_AUTH                 1
#define SEC_DECR                 2
#define SEC_ENCR                 3

#define  IP_SEC_COMPLETE         1

#define SEC_IPV4_HEADER_SIZE     20
#define SEC_PORT_NUMBER_OFFSET   22


/* Sub system up notification message */
typedef struct NOTIFYSYSTEMUP
{
    UINT4   u4IfIndex;
}tSecv4NotifySystemUp;

/* Prototypes of the functions exported to  IPv4 */

UINT1 Secv4OutProcess PROTO ((tIP_BUF_CHAIN_HEADER * pBuf, UINT4 u4Port));
UINT1 Secv4InProcess PROTO ((tIP_BUF_CHAIN_HEADER **ppBuf, UINT4 u4Port));

INT1 Secv4Initialize PROTO((VOID));
VOID Secv4DeInit PROTO ((VOID));

PUBLIC UINT4 Secv4HandleNotifyEvent PROTO ((VOID));

PUBLIC VOID Secv4Lock PROTO ((VOID));
PUBLIC VOID Secv4UnLock PROTO ((VOID));
VOID Secv4ProcessTimeOut PROTO((VOID));
INT1 SecUtilGetGlobalStatus PROTO ((VOID));


#ifdef IPSECv4_WANTED
#define IPSECv4_IN_PROCESS(pBuf, u2Port) Secv4InProcess(pBuf, u2Port)
#define IPSECv4_OUT_PROCESS(pBuf, u2Port) Secv4OutProcess(pBuf, u2Port)
#define IPSECv4_INITIALISE()  Secv4Initialize()
#else

#define IPSECv4_IN_PROCESS(pBuf, u2Port)         SEC_SUCCESS 
#define IPSECv4_OUT_PROCESS(pBuf, u2Port) 
#define IPSECv4_INITIALISE()                  SEC_SUCCESS 
#endif

#define SEC_CFA_TO_IP        1
#define SEC_CFA_TO_VPNFP         2
#define SEC_CFA_TO_IPSEC         3
#define SEC_CFA_CONTINUE         4
#endif

