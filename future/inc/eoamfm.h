/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: eoamfm.h,v 1.2 2013/11/05 10:57:46 siva Exp $ 
 *
 * Description: This file contains exported definitions and functions
 *              of Fault Management module.
 *********************************************************************/
#ifndef _EOAMFM_H
#define _EOAMFM_H

/* Trace type  definitions */
#define  FM_FN_ENTRY_TRC    (0x1 << 16)
#define  FM_FN_EXIT_TRC     (0x1 << 17)
#define  FM_CRITICAL_TRC    (0x1 << 18)
#define  FM_LOOPBACK_TRC    (0x1 << 19)
#define  FM_EVENTTRIG_TRC   (0x1 << 20)
#define  FM_EVENTRX_TRC     (0x1 << 21)
#define  FM_VAR_REQRESP_TRC (0x1 << 22)
#define  FM_ALL_TRC         (FM_FN_ENTRY_TRC    |\
                             FM_FN_EXIT_TRC     |\
                             FM_CRITICAL_TRC    |\
                             FM_LOOPBACK_TRC    |\
                             FM_EVENTTRIG_TRC   |\
                             FM_EVENTRX_TRC     |\
                             FM_VAR_REQRESP_TRC |\
                             INIT_SHUT_TRC      |\
                             MGMT_TRC           |\
                             CONTROL_PLANE_TRC  |\
                             DUMP_TRC           |\
                             OS_RESOURCE_TRC    |\
                             ALL_FAILURE_TRC    |\
                             BUFFER_TRC)


#define FM_IS_VARIABLE_INDICATION(u1Val) \
         (u1Val & 0x80 ? OSIX_TRUE : OSIX_FALSE)

/* Length of the variable response(branch/leaf/(width/indication)/value) 
 * display in hex format */
#define FM_CLI_BRANCH_LEN            2
#define FM_CLI_LEAF_LEN              4
#define FM_CLI_WIDTH_LEN             2
#define FM_CLI_VALUE_LEN             512

/* +2 for storing "0x", +1 for storing '\0' */
#define FM_HEXSTR_MIB_RESP_WIDTH_LEN  (FM_CLI_WIDTH_LEN + 2 + 1)

/* Variable retrieval values */
#define FM_CLI_BRANCH                0x01
#define FM_CLI_LEAF                  0x02
#define FM_CLI_LENGTH                0x03
#define FM_CLI_VALUE                 0x04

/* System control status */
enum
{
    FM_START = 1,
    FM_SHUTDOWN
};

/* Module status */
enum
{
    FM_ENABLED = 1,
    FM_DISABLED
};

/* Event Actions */
typedef enum
{
    FM_ACTION_NONE = 1,
    FM_ACTION_WARNING,
    FM_ACTION_BLOCK
} eFmEventAction;

/* FM Main entry point function */
VOID FmMainTask PROTO ((INT1 *));

INT4 FmLock PROTO ((VOID));
INT4 FmUnLock PROTO ((VOID));
PUBLIC INT4 FmApiCallbackFromEoam PROTO ((tEoamCallbackInfo * pCallbackInfo));
/* Protocols should call the above API to nofity the fault to FM module.
 * FM send the trap message and logs the syslog message.*/

#endif /* _EOAMFM_H */
/*-----------------------------------------------------------------------*/
/*                       End of the file  eoamfm.h                       */
/*-----------------------------------------------------------------------*/
