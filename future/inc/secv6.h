
/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secv6.h,v 1.11 2011/10/25 10:44:43 siva Exp $
 *
 * Description: This has macros and proto type cmn to ip6 and ipsec 
 *
 ***********************************************************************/

#ifndef _SECV6_H_
#define _SECV6_H_

/* Macros Export to IPv6 */

#define SEC_AH                   51 
#define SEC_ESP                  50
#define SEC_FAILURE              0
#define SEC_SUCCESS              1

#define SEC_IPV6_PORT_NUMBER_OFFSET   42

#define SECV6_TASK_LOCK IpSecv6Lock
#define SECV6_TASK_UNLOCK IpSecv6UnLock

INT4 IpSecv6Lock PROTO ((VOID));
INT4 IpSecv6UnLock PROTO ((VOID));

/* Export to CLI */

typedef tCRU_BUF_CHAIN_HEADER tBufChainHeader;

/* Prototypes of the functions security functions exported to IPv6 */

INT1    Secv6Initialize    PROTO((VOID));
INT1    Secv6OutProcess    PROTO ((tBufChainHeader * pBuf,
                                   tIp6If *pIf6,UINT4 *pLen));
INT1    Secv6InProcess     PROTO ((tBufChainHeader * pBuf,
                                   tIp6If *pIf6,UINT1 *pNhr,
                                   UINT4 *pu4ProcessedLen));

UINT2   Secv6GetSecAssocOverHead    PROTO((tIp6Addr SrcAddr, tIp6Addr DestAddr,
                                           UINT2 u2Index, UINT1 u1Protocol));
INT1    Secv6AccListExist PROTO ((tIp6Addr SrcAddr, tIp6Addr DestAddr));
INT4    RegisterIPSECv6withFutureSNMP    PROTO((VOID));
INT1    Secv6DeleteAllSecAssocEntries (VOID);
INT1    Secv6UtilGetGlobalStatus PROTO ((VOID));
INT4    Secv6Ioctl (FS_ULONG  P);
#endif
