/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsvxlan.h,v 1.17 2018/01/05 09:57:08 siva Exp $
 *
 * Description:This file contains the exported definitions and
 *             prototypes of MPLS
 *
 *******************************************************************/


#ifndef _VXLAN_H
#define _VXLAN_H
#include "fswebnm.h"
PUBLIC VOID
VxlanTaskSpawnVxlanTask (INT1 *pi1Arg);

INT4
VxlanProcessL2Pkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex);

INT4
VxlanPortCheckMcastVxlanPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1LengthCheck);

INT4
VxlanMbsmUpdateCardStatus (tMbsmProtoMsg * pProtoMsg, INT4 i4Event);

#ifdef NPAPI_WANTED
INT4
VxlanPortCheckVniVlanMapAndUpdateNveDb (tCRU_BUF_CHAIN_HEADER * pBuf);
#endif

VOID
VxlanPortDelNveEntry (UINT4 u4IfIndex, tMacAddr MacAddr, UINT2 u2VlanId);

VOID
VxlanPortDelNveInterface (UINT4 u4IfIndex);
VOID
VxlanPortRemoveVniFromVlan (UINT2 u2IfVlanId);
INT4
VxlanIsIngressReplicaEnabledForVlan (INT4 i4VlanId);
VOID
VxlanPortRemoveDynamicNveEntries (UINT4 u4IfIndex, INT4 i4SetValIfMainAdminStatus);

VOID
VxlanPortOperDownIndication (UINT4 u4IfIndex, UINT1 u2Status);

VOID VxlanDelPortFromL2Table(UINT4 u4IfIndex);
INT4
VxlanIsLoopbackUsed (UINT4 u4IfIndex);
#define VXLAN_SUCCESS        (OSIX_SUCCESS)
#define VXLAN_FAILURE        (OSIX_FAILURE)

#define L2PKT_VXLAN_TO_CFA   50
#define L3PKT_VXLAN_TO_CFA   51
#ifdef WEBNM_WANTED
PUBLIC VOID IssProcessVxlanNvePage    PROTO  ((tHttp * pHttp));
PUBLIC VOID IssProcessVxlanVtepPage   PROTO  ((tHttp * pHttp));
PUBLIC VOID IssProcessVxlanMcastPage   PROTO  ((tHttp * pHttp));
PUBLIC VOID IssProcessVxlanVniVlanPage   PROTO  ((tHttp * pHttp));
#endif /*WEBNM_WANTED*/
#ifdef EVPN_VXLAN_WANTED
INT4
EvpnVxlanPortCheckVniEviMap (UINT4 u4EviId);

INT4
VxlanNotifyESIAndIfStatus(UINT1 * pu1VlanList, tMacAddr MacAddr,
             UINT2 u2SysPriority, UINT1 u1OperStatus);
INT4
VxlanUtilGetAnycastGwmac (tMacAddr *pEvpnAnycastGwMac);
typedef struct
{
    UINT4     u4EviNumber;
    UINT4     u4VxlanVniNumber;
    UINT4     u4L3VniNumber;
    UINT4     u4RtIfIndx; /* In case of IRB, to fill Route IfIndex */
    INT4      i4VxlanRemoteVtepAddressType;
    UINT1     au1VxlanRemoteVtepAddress[16];
    UINT1     au1VxlanDestAddress[16];
    tMacAddr  VxlanDestVmMac;
    UINT1     u1VxlanDestAddressLen;
    UINT1     u1RouteType;
    UINT1     au1FsEvpnMHEviVniESI[10];
    UINT1     u1LBFlag;
    UINT2     u2VrfFlag;
    UINT1     au1Pad[3];
} tEvpnRoute;

INT4
EvpnVxlanAddRouteInDp (tEvpnRoute * pEvpnRoute);
INT4
EvpnVxlanDelRouteInDp (tEvpnRoute * pEvpnRoute);
INT4
EvpnVxlanAddESRouteInMHPeer (tEvpnRoute * pEvpnRoute);
INT4
EvpnVxlanAddADRouteInMHPeer (tEvpnRoute * pEvpnRoute);

#define EVPN_BGP4_ROUTE_PARAMS_REQ       1
#define EVPN_BGP4_VRF_ADMIN_STATUS_REQ   2
#define EVPN_BGP4_NOTIFY_MAC             3
#define EVPN_BGP4_GET_MAC                4
#define EVPN_BGP4_NOTIFY_ESI             5
#define EVPN_BGP4_NOTIFY_ETH_AD          6
#define EVPN_BGP4_NOTIFY_L3VNI           7

#define EVPN_BGP4_STATIC_MAC             1

#define EVPN_BGP4_MAC_VRF_ROUTE         0x01
#define EVPN_BGP4_IP_VRF_ROUTE          0x02

#define EVPN_BGP4_MAC_ADD                1
#define EVPN_BGP4_MAC_DEL                2

typedef struct _EvpnBgpRegEntry
{
    VOID (*pBgp4EvpnNotifyRouteParams) (UINT4 u4EviId, UINT4 u4VniIdx, UINT1 u1ParamType, UINT1 u1Action, UINT1 *pu1RouteParam, UINT1 u1VniType);
    /* u4EviId       : The VRF for which the RD/RT is being informed about */
    /* u4VniIdx      : The VNI for which the RD/RT is being informed about */
    /* u1PamarmType  : The parameter type which is being passed to BGP (RD, Import RT, Export RT) */
    /* u1Action      : ADD/DELETE */
    /* pu1RouteParam : The value of the route-parameter (RD/RT) */
    VOID (*pBgp4EvpnNotifyAdminStatusChange) (UINT4 u4EviId, UINT4 u4VniIdx, UINT1 u1EvpnStatus);
    /* u4EviId     : The VRF for which the RD/RT is being informed about */
    /* u4VniIdx    : The VNI for which the RD/RT is being informed about */
    /* u1EvpnStatus  : The VRF status UP/DOWN/CREATED/DELETED */
    VOID (*pBgp4EvpnNotifyMACUpdate) (UINT4 u4VrfId, UINT4 u4VniIdx, UINT4 u4L3vniId, tMacAddr u1MacAddr, UINT1 u1AddrLen, UINT1 *au1IpAddr, UINT1 u1Action, UINT1  u1EntryStatus);
    /* u4VrfId     : The VRF for which MAC update is being informed about */
    /* u4VniIdx    : The VNI for which MAC update is being informed about */
    /* u4L3vniId   : The L3 VNI for which the MAC update is being sent */
    /* u1MacAddr   : MAC Address learnt from CE */
    /* IpAddr      : IP Address learnt from CE */
    /* u1Action      : ADD/DELETE */
    /* u1EntryStatus : VLAN_FDB_OTHER/VLAN_FDB_INVALID/VLAN_FDB_LEARNT/VLAN_FDB_SELF/VLAN_FDB_MGMT */
    VOID (*pBgp4EvpnGetMACNotification) (UINT4 u4VrfId, UINT4 u4VniIdx, tMacAddr u1MacAddr, UINT1 u1Flag);
    /* u4VrfId     : The VRF for which MAC update is required */
    /* u4VniIdx    : The VNI for which MAC update is required */
    /* u1MacAddr   : The Mac Address for whoch the MAC update is required */
    /* u1Flag      : The Flag to indicate ADD/DELETE */
    VOID (*pBgp4EvpnNotifyESIUpdate) (UINT4 u4VrfId, UINT4 u4VniIdx, UINT1 *pu1EsiId, UINT1 *pu1IpAddr, INT4 i4AddrType, UINT1 u1Action);
    /* u4VrfId     : The VRF for which MAC update is being informed about */
    /* u4VniIdx    : The VNI for which MAC update is being informed about */
    /* pu1EsiId    : ESI Id */
    /* pu1IpAddr   : VTEP IP Address */
    /* i4AddrType  : VTEP IP Address Type */
    /* u1Action    : ADD/DELETE */
    VOID (*pBgp4EvpnNotifyADRoute) (UINT4 u4VrfId, UINT4 u4VniIdx, UINT1 *pu1EsiId, INT4 i4Flag);
    /* u4VrfId     : The VRF for which MAC update is being informed about */
    /* u4VniIdx    : The VNI for which MAC update is being informed about */
    /* pu1EsiId    : ESI Id */
    /* i4Flag      : 0 - all active / 1- single active */
    VOID (*pBgp4EvpnNotifyL3Vni) (UINT4 u4VrfId, UINT4 u4L3Vniidx, UINT1 u1Action);
    UINT2       u2InfoMask;
    /* Possible values:
     * EVPN_BGP4_ROUTE_PARAMS_REQ
     * EVPN_BGP4_VRF_ADMIN_STATUS_REQ
     * EVPN_BGP4_VRF_NOTIFY_MAC_UPDT
     */

    UINT1       u1Padding[2];

}tEvpnBgpRegEntry;

/* Functions for EVPN */
/* API exposed to BGP for regsitering callbacks */
/* This function will be called by BGP module to register the callback functions */
/* Returns: OSIX_SUCCESS/OSIX_FAILURE */
INT4 EvpnRegisterCallback (tEvpnBgpRegEntry *pRegInfo);
INT4 EvpnDeRegisterCallback (tEvpnBgpRegEntry *pRegInfo);
VOID
VxlanHandleL3VniVlanIntAdminStatus(UINT4 u4L3Vni, INT4 i4IfMainAdminStatus);
INT4 EvpnUtlBgpGetMacUpdate (UINT4 u4VniId);
INT4 VxlanIsL3VniMappedWithContext (UINT4 u4IfIndex, UINT4 *u4VrfId, UINT4 *pu4L3VniId);
INT4
VxlanProcessL3Pkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4VrfId, UINT4 u4L3Vni);
INT4 VxlanIsL3VniMappedWithVlan (UINT2 u2VlanId);
INT4 VxlanEvpnIsArpSuppressionEnabled (UINT4 u4IpAddr);
#endif /* EVPN_VXLAN_WANTED*/
#endif
