
/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: natnpwr.h,v 1.2 2014/10/16 13:04:12 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              for NAT wrappers
 *              
 ********************************************************************/
#ifndef __NAT_NP_WR_H__
#define __NAT_NP_WR_H__

UINT1 NatFsNpIpv4ArpAdd PROTO ((tNpVlanId u2VlanId, UINT4 u4IfIndex, UINT4 u4IpAddr, UINT1 *pMacAddr, UINT1 *pu1IfName, INT1 i1State, UINT4 *pu4TblFull));
UINT1 NatFsNpIpv4ArpDel PROTO ((UINT4 u4IpAddr, UINT1 *pu1IfName, INT1 i1State));

#endif /* __NAT_NP_WR_H__ */
