/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pnac.h,v 1.59 2015/06/11 10:01:49 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              by  external modules.
 *
 *******************************************************************/

#ifndef _PNAC_H
#define _PNAC_H

#include "rmgr.h"
enum PnacEvents
{
   PNAC_BRIDGE_DETECTED = 1,
   PNAC_BRIDGE_NOT_DETECTED
};

/* MACROs used across PNAC module, Link Aggregation module and RADIUS Client */

#define PNAC_LOCK() PnacLock ()
#define PNAC_UNLOCK() PnacUnLock ()

#define PNAC_SUCCESS 0
#define PNAC_FAILURE -1
#define PNAC_IGNORE   1

#define   PNAC_DATA    1
#define   PNAC_CONTROL 2
#define   PNAC_DROP    3

#define   PNAC_TRUE  1
#define   PNAC_FALSE 0

#define   PNAC_GREATER  1
#define   PNAC_EQUAL    0
#define   PNAC_LESSER   -1

#define   tPnacCruBufChainHdr  tCRU_BUF_CHAIN_HEADER
/* Hardware API related defines */

#define  PNAC_LOCK_PORT                 1
#define  PNAC_BLOCK_UNKNOWN_PKT         2
#define  PNAC_BLOCK_UNICAST_UNKNOWN_PKT 3
#define  PNAC_BLOCK_ALL_UNKNOWN_PKT     4

#define  PNAC_SET                       1
#define  PNAC_UNSET                     2

#define  SERVER_SUCCESS                 0
#define  SERVER_FAILURE                -1

#define   PNAC_REMOTE_AUTH_SERVER  1
#define   PNAC_LOCAL_AUTH_SERVER   2
#define   PNAC_REMOTE_RADIUS_AUTH_SERVER  1
#define   PNAC_REMOTE_TACACSPLUS_AUTH_SERVER  2
/* Radius related defines */

#define   PNAC_ENET_PORT             CFA_ENET
#define   PNAC_MAX_SERVER_KEY_SIZE        120

#define   PNAC_MAX_NAS_ID_SIZE            16
#define   PNAC_DEFAULT_NAS_ID        "fsNas1"

#define   PNAC_USER_NAME_SIZE             64
 
#define   PNAC_MAX_EAP_PKT_SIZE         1496 
                                        /* PNAC_MAX_ENET_FRAME_SIZE 
                                           - PNAC_ENET_EAPOL_HDR_SIZE */


#define   PNAC_ENETTYPE_OFFSET             12 
#define   PNAC_ENET_TYPE_OR_LEN            2
#define   PNAC_PAE_ENET_TYPE              0x888e
#define   PNAC_PAE_RSN_PREAUTH_ENET_TYPE  0x88c7

#define   PNAC_KEY_VECTOR_LEN             16
#define   PNAC_KEY_SIGNATURE_LEN          16 
#define   PNAC_KEY_REPLAY_LEN              8

#define   PNAC_MAX_STATE_ATTRIB_LEN      255
#define   PNAC_PORTLIST_LEN   ((BRG_MAX_PHY_PORTS + 31)/32 * 4)
#define   PNAC_CNTRLD_DIR_BOTH          0
#define   PNAC_CNTRLD_DIR_IN            1

#define   PNAC_PORTSTATUS_AUTHORIZED    1
#define   PNAC_PORTSTATUS_UNAUTHORIZED  2
#define   PNAC_MAC_SESS_DEL             3

/* The following macros are used in Mac based
   Authentication */
#define   PNAC_UNBLOCK_PORT             PNAC_PORTSTATUS_AUTHORIZED
#define   PNAC_BLOCK_PORT               PNAC_PORTSTATUS_UNAUTHORIZED
#define   PNAC_ADD_ALLOWED_MAC          PNAC_PORTSTATUS_AUTHORIZED
#define   PNAC_REMOVE_ALLOWED_MAC       PNAC_PORTSTATUS_UNAUTHORIZED

#define   PNAC_PORTCNTRL_FORCEUNAUTHORIZED 1
#define   PNAC_PORTCNTRL_AUTO              2
#define   PNAC_PORTCNTRL_FORCEAUTHORIZED   3

 /* Authentication Mode related macros */

#define   PNAC_PORT_AUTHMODE_PORTBASED  1
#define   PNAC_PORT_AUTHMODE_MACBASED    2

#define  PNAC_COUNT_SESS_OCTETS_RX   1
#define  PNAC_COUNT_SESS_OCTETS_TX   2
#define  PNAC_COUNT_SESS_FRAMES_RX   3
#define  PNAC_COUNT_SESS_FRAMES_TX   4
/*Authenticator States related Macros*/
#define  PNAC_ASM_STATE_INITIALIZE         1
#define  PNAC_ASM_STATE_DISCONNECTED       2
#define  PNAC_ASM_STATE_CONNECTING         3
#define  PNAC_ASM_STATE_AUTHENTICATING     4
#define  PNAC_ASM_STATE_AUTHENTICATED      5
#define  PNAC_ASM_STATE_ABORTING           6
#define  PNAC_ASM_STATE_HELD               7
#define  PNAC_ASM_STATE_FORCEAUTH          8
#define  PNAC_ASM_STATE_FORCEUNAUTH        9

/*Constants defined for PNAC_MAC_SESSION_STATS*/
#define   PNAC_SESSION_ID_STRING_SIZE      256

/* Interface Mesage Types */
typedef enum {
    PNAC_CREATE_PORT = 1,
    PNAC_DELETE_PORT = 2,
    PNAC_UPDATE_PORT_STATUS = 3,
    PNAC_EAPOL_FRAME = 4,
    PNAC_AUTHSERVER_RESP = 5,
    PNAC_RM_MSG = 6,
    PNAC_CDSM = 7,
    PNAC_RSNA_MLME_MSG_FROM_APME = 8,  
    PNAC_RSNA_KEY_AVAILABLE = 9,
    PNAC_RSNA_CONFIG_FROM_APME = 10,
    PNAC_RSNA_CHG_CONFIG_FROM_APME = 11,
    PNAC_MAP_PORT = 12,
    PNAC_UNMAP_PORT = 13,
    PNAC_FIRSTDATA_FRAME = 14,
    PNAC_AUTH_MODE_CHG = 15,
    PNAC_AUTH_CTRL_CHG = 16,
    PNAC_CREATE_AUTHORIZE_SESSION = 17,
    PNAC_RESTART_MAC_SESSION = 18,
    PNAC_DEAUTHORIZE_MAC_SESSION = 19,
    PNAC_NP_CALL_BACK = 20,
    PNAC_VLAN_PROP_CHANGE = 21
}tPnacIntfMesgTypes;

/* data structures used across PNAC module and RADIUS Client */
/* MAC Address type */

typedef struct PnacAsIfSendData {

   tMacAddr         macAddr;
   UINT2            u2NasPortNumber;
   UINT2            u2NasIdLen;
   UINT2            u2SuppUserNameLen;
   UINT2            u2EapPktLength;
   UINT2            u2StateLen;
   UINT1            au1NasId[PNAC_MAX_NAS_ID_SIZE];
   UINT1            au1EapPkt[PNAC_MAX_EAP_PKT_SIZE];
   UINT1            au1State[PNAC_MAX_STATE_ATTRIB_LEN];
   UINT1            u1NasPortType;
   UINT1            au1SuppUserName[PNAC_USER_NAME_SIZE + 1];
   UINT1            au1Pad[3];

}tPnacAsIfSendData;


typedef struct PnacAsIfRecvData {
  tMacAddr    macAddr;
  UINT1       au1Reserved[2];
  UINT4       u4SessionTimeout;
  UINT2       u2NasPortNumber;
  UINT2       u2EapPktLength;
  UINT2       u2ServerKeyLength;
  UINT2       u2StateLen;
  UINT1       au1EapPkt [PNAC_MAX_EAP_PKT_SIZE];
  UINT1       au1ServerKey [PNAC_MAX_SERVER_KEY_SIZE];
  UINT1       au1State[PNAC_MAX_STATE_ATTRIB_LEN];
  UINT1       u1Pad;

}tPnacAsIfRecvData;

typedef struct _tVlanChangeInfo {
   UINT2                  u2CurrentVlanId;
   UINT2                  u2Port;
} tVlanChangeInfo;

/* Key Descriptor */
typedef struct PnacKeyDesc {
    UINT1    au1InitVector[PNAC_KEY_VECTOR_LEN];
    UINT1    au1KeySignature[PNAC_KEY_SIGNATURE_LEN];
    UINT1    au1ReplayCounter[PNAC_KEY_REPLAY_LEN];
    UINT1    au1Key[PNAC_MAX_SERVER_KEY_SIZE];
    UINT2    u2KeyLength;
    UINT1    u1DescType;
    UINT1    u1KeyIndex;
}tPnacKeyDesc;

INT4 PnacGetPnacEnableStatus (VOID);
INT4 PnacCreatePort (UINT2);
INT4 PnacDeletePort (UINT2);
INT4 PnacMapPort (UINT2 u2PortNum);
INT4 PnacUnmapPort (UINT2 u2PortNum);
INT4 PnacPortStatusUpdate (UINT2, UINT1);
INT4 PnacHandleInFrame ( tPnacCruBufChainHdr  *, UINT2, UINT1);
INT4 PnacHandleReceivedFrame (tPnacCruBufChainHdr * pMesg,
                UINT2 u2IfIndex,
                              UINT1 u1FrameType);

INT4 PnacWrHandleReceivedFrame (tPnacCruBufChainHdr * pMesg,
          UINT2 u2IfIndex,
                                UINT1 u1FrameType,
                                UINT4 u4ContextId);

INT4 PnacHandleOutFrame (tPnacCruBufChainHdr *pMesg,
                         UINT2  u2IfIndex,
                         UINT1  *au1DestHwAddr,
                         UINT4 u4FrameSize,
                         UINT2 u2Protocol,
                         UINT1 u1EncapType);

INT4 PnacGetLocAuthServPassword    PROTO ((UINT1 *, UINT1 *));
INT4 PnacGetStartedStatus          PROTO ((VOID));
INT4 PnacHandleEvent               PROTO ((UINT2 u2PortNum, UINT4 u4Event));


VOID PnacInterfaceTaskMain PROTO ((INT1 *));

INT4 PnacLock PROTO ((VOID));
INT4 PnacUnLock PROTO ((VOID));

INT4 PnacL2IwfProcPortStatUpdEvent PROTO ((UINT2 u2PortNum, UINT1 u1OperStatus));

VOID PnacHandleRadResponse (VOID *pRadRespRcvd);
INT4 PnacVlanPropChange PROTO ((tVlanChangeInfo *pVlanChangeInfo));

#ifdef L2RED_WANTED
INT4 PnacRedRcvPktFromRm PROTO ((UINT1 , tRmMsg *, UINT2));
#endif

#ifdef MBSM_WANTED
INT4 PnacMbsmProcessUpdateMessage PROTO ((tMbsmProtoMsg *, INT4 i4Event)) ;
#endif
VOID PnacModuleShutDown PROTO ((VOID));
INT4 PnacModuleStart  PROTO ((VOID));
INT4 
PnacIsPnacCtrlFrame (tCRU_BUF_CHAIN_DESC *pMesg);

VOID PnacHandleMacSession (UINT2 u2PortIndex, tMacAddr SrcMacAddr, 
                           UINT2 u2MsgType);
INT4 PnacSetPnacPortAuthMode (UINT2 u2PortNum, UINT2 u2AuthMode);
INT4 PnacSetPnacPortAuthControl (UINT2 u2Port, UINT2 u2AuthControl);
INT4 PnacNotifyAuthFailure (UINT2 u2PortNum, tMacAddr SrcMacAddr, UINT2 u2Status);
INT4 PnacMemRelease(tMemPoolId PoolIdInfo, INT4 u4RelCount, ...);
INT1 PnacSetPnacTraceLevel (UINT4 u4PnacTrcLevel);
#endif /* _PNAC_H */
