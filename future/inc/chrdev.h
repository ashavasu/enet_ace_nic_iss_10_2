/***************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: chrdev.h,v 1.25 2010/11/12 12:56:52 prabuc Exp $
 *
 * Description: This file contains Macro's, Constants, Globals and Structure
 *              Definition's related to the ISS Future Kernel Module
 *
 **************************************************************************/
#ifndef _CHRDEV_H_
#define _CHRDEV_H_

#include "lr.h"
#include "cfa.h"
#include "lnxip.h"

/* For Linux Version related Macro's */
#include "fsapsys.h"

#define GDD_MAJOR_NUMBER 100

#define GDD_NP_IOCTL          _IOWR(GDD_MAJOR_NUMBER, 1, char *)
#define IPAUTH_NMH_IOCTL      _IOWR(GDD_MAJOR_NUMBER, 2, char *)
#define GDD_OOB_IOCTL         _IOWR(GDD_MAJOR_NUMBER, 3, char *)
#define IPAUTH_PORT_IOCTL     _IOWR(GDD_MAJOR_NUMBER, 4, char *)
#ifdef VRRP_WANTED
#define VRRP_ASSOCIP_IOCTL     _IOWR(GDD_MAJOR_NUMBER, 5, char *)
#endif
#define VLAN_FDB_IOCTL     _IOWR(GDD_MAJOR_NUMBER, 6, char *)
#define VLAN_FDB_REMOVE_IOCTL     _IOWR(GDD_MAJOR_NUMBER, 7, char *)

#ifdef MBSM_WANTED
#define STACK_INFO_IOCTL        _IOWR(GDD_MAJOR_NUMBER, 8, char *)
#endif
#define IGMP_MAJOR_NUMBER 101
#define PIM_MAJOR_NUMBER 102
#define DVMRP_MAJOR_NUMBER 103
#define SNTP_MAJOR_NUMBER 104

#define IGMP_DEVICE_FILE_NAME "/dev/FsIgmpChrDev"
#define PIM_DEVICE_FILE_NAME "/dev/FsPimChrDev"
#define DVMRP_DEVICE_FILE_NAME "/dev/FsDvmrpChrDev"
#define SNTP_DEVICE_FILE_NAME "/dev/FsSntpChrDev"

#define IPAUTH_SET_HTTP_PORT   1
#define IPAUTH_SET_TELNET_PORT 2

#ifdef WGS_WANTED
#define MGMT_VLAN_IOCTL       _IOWR(GDD_MAJOR_NUMBER, 9, char *)

#define KERN_SET_MGMT_VLAN_LIST         1
#define KERN_RESET_MGMT_VLAN_LIST       2
#endif

extern INT4 gi4DevFd;
extern INT4 gi4IgmpDevFd;

/*Structure used to set Http/Telnet Port*/
typedef struct {
    INT4    i4Cmd;
    INT4    i4Port;
} tSourcePort;

typedef tLnxIpIntfParams tKernIntfParams;

#ifdef WGS_WANTED
typedef struct _KernMgmtVlanParams{

    UINT4   u4Action;
    tMgmtVlanList MgmtVlanList;
}tKernMgmtVlanParams;
#endif

#ifdef VRRP_WANTED
typedef struct _VrrpAssocIpParams{

    UINT4      u4Action;             /* Specifies the ioctl request */
    UINT4      u4IpAddr;             /* Virtual IP Address */
    UINT1      *pu1MacAddr;          /* Virtual MAC */
    UINT2      u2VlanId;             /* VLAN Id */
    UINT2      u2IpPort;             /* IP Port Number over which a VRRP
                                        Instance runs */
} tVrrpAssocIpParams;

enum {
    VRRP_KERN_ADD = 1,
    VRRP_KERN_DEL,
    VRRP_KERN_IF_CREATE,
    VRRP_KERN_IF_DELETE       
};
#endif /*VRRP_WANTED */

typedef struct _VlanFdbEntryParams{
    UINT4      u4Action;
    UINT2      VlanId;
    tMacAddr   MacAddress;
}tVlanFdbEntryParams;

typedef struct _VlanFdbParams{
    UINT4      u4Action;
}tVlanFdbParams;

enum {
    VLAN_KERN_REMOVE = 1,
    VLAN_KERN_FDB_REMOVE, /* To remove fdb entries from the software */
};

enum {
    GDD_PORT_CREATE = 1,
    GDD_PORT_DELETE,
    GDD_INTERFACE_CREATE,
    GDD_INTERFACE_DELETE,
    GDD_PORT_OPER_STATUS
};

#ifdef MBSM_WANTED
typedef struct _StackingParams{
   UINT1      u1Param;
   UINT1      u1Value;
}tStackingParams;

enum {
    KERN_STACK_PORTS = 1,
    KERN_FP_PORTS,
    KERN_COLD_STDBY_STATE,
    KERN_STACK_PRI
};
#endif

typedef struct { 
   INT4 startpattern;
   INT4 type; 
   INT4 len;
   INT4 endpattern;
}tHeader;

int NmhIoctl (unsigned long p);

#if defined (BCM5690_WANTED) || defined (BCM5695_WANTED) || defined (BCMX_WANTED)
#include "chrdvbcm.h"
#endif

#ifdef SWC
#include "chrdvcxe.h"
#endif

#ifdef MUX_WANTED
/* Includes for MUX Module */
#include "mux.h"
#include "netdev.h"
#endif /* MUX_WANTED */

#endif /* _CHRDEV_H_ */
