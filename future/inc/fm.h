/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fm.h,v 1.22 2016/07/13 12:30:25 siva Exp $ 
 *
 * Description: This file contains exported definitions and functions
 *              of Fault Management module.
 *********************************************************************/
#ifndef _FM_H
#define _FM_H

/* Module Ids For External Modules. gapc1FmModule array pointer needs
 * to be updated if any modules gets newly added.*/
enum
{
    FM_NOTIFY_MOD_ID_LLDP = 1,
    FM_NOTIFY_MOD_ID_RM,
    FM_NOTIFY_MOD_ID_MSR,
    FM_NOTIFY_MOD_ID_SYS,
    FM_NOTIFY_MOD_ID_ELPS,
    FM_NOTIFY_MOD_ID_MRP,
    FM_NOTIFY_MOD_ID_ERPS,
    FM_NOTIFY_MOD_ID_DCBX,
    FM_NOTIFY_MOD_ID_CN,
    FM_NOTIFY_MOD_ID_PTP,
    FM_NOTIFY_MOD_ID_CLK,
    FM_NOTIFY_MOD_ID_MPLS,
    FM_NOTIFY_MOD_ID_MPLS_L2VPN,
    FM_NOTIFY_MOD_ID_LSPP,
    FM_NOTIFY_MOD_ID_BFD,
    FM_NOTIFY_MOD_ID_Y1564,
    FM_NOTIFY_MOD_ID_RFC2544,
    FM_NOTIFY_MOD_ID_RFC6374
};
/* External modules must update the appropriate module name, to be
 * displayed in notification messages, in gapc1FmModule array pointer */
#ifdef FM_API_C
CONST CHR1  *gapc1FmModule [] = {
   NULL,
   "LLDP",
   "RM",
   "MSR",
   "SYS",
   "ELPS",
   "MRP",
   "ERPS",
   "DCBX",
   "CN",
   "PTP",
   "CLOCK",
   "MPLS",
   "MPLSL2VPN",
   "LSPP",
   "BFD",
   "Y1564",
   "RFC2544",
   "RFC6374"
};
#endif

#define ISS_FM_MAX_MODULE_NAME   12   /* Length of the Module Name */
#define  ISS_FM_MAX_MODULES 5 /* Maximum number of modules interested
                                    to receive the critical events */


/* Fault Notification Structure */
typedef struct 
{
    tSNMP_VAR_BIND     *pTrapMsg;
    tSNMP_OID_TYPE     *pEnterpriseOid;
    tSNMP_OCTET_STRING_TYPE *pCxtName; 
    UINT1              *pSyslogMsg; 
    UINT1             ModuleName[ISS_FM_MAX_MODULE_NAME];
    UINT4               u4GenTrapType;
    UINT4               u4SpecTrapType;
    UINT4               u4ModuleId; /* Holds the ID of the module which posted the event */
    UINT4  u4EventType; /* Type of event triggered */
    UINT4  u4CriticalInfo; /* Holds the status of the critical events*/
}tFmFaultMsg;



PUBLIC INT4 FmApiNotifyFaults PROTO ((tFmFaultMsg *pFmMsg));
/* Protocols should call the above API to nofity the fault to FM module.
 * FM send the trap message and logs the syslog message.*/


/* The below enumerator defines the Critical events of the module 
 * which needs to be sent to FM module */
typedef enum
{
 ISS_FM_TASK_INIT_COMPLETE_INDICATION  = 1,
 ISS_FM_MSR_RELOAD_COMPLETE,
 ISS_FM_RM_CRITICAL_EVENT_INDICATION,
} eFmEventTypes;

typedef struct _sFmRegInfo
{
    INT4 (*pFmCallBack) (VOID *);   /* Function pointer for
                                  callback function to register with FM module */
    UINT1   u4Flag;             /* Flag Which Indicates whether entry is used or
                                   not */
    UINT1 au1padding[3];  /* Padding for the structure */
}tFmRegisterInfo;

/* To register with fm module to invoke the call back function on critical
 * event */
INT4 FmRegisterForCriticalEvent(tFmRegisterInfo *pFmRegInfo);
INT4 FmIndicateEventToProto (tFmFaultMsg *pFmMsg);
INT4 FmCallbackRegister(tFsCbInfo * pFsCbInfo);

#endif /* _FM_H */
/*-----------------------------------------------------------------------*/
/*                       End of the file  fm.h                      */
/*-----------------------------------------------------------------------*/
