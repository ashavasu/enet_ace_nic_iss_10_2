#ifndef __WLAN_NP_WR_H__
#define __WLAN_NP_WR_H__
#ifdef NPAPI_WANTED
#include "capwap.h"
#include "npapi.h"

#define WLAN_ENTRY_ADD 1
#define WLAN_ENTRY_GET 2
#define WLAN_ENTRY_DELETE 3

typedef struct {
    tWlanParams *pWlanNpParams;
} tWlanNpWrEntryAdd;

typedef struct {
    tWlanParams *pWlanNpParams;
} tWlanNpWrEntryGet;

typedef struct {
    tWlanParams *pWlanNpParams;
} tWlanNpWrEntryDelete;

typedef struct WlanNpModInfo {
union {
    tWlanNpWrEntryAdd sWlanNpEntryAdd;
    tWlanNpWrEntryGet sWlanNpEntryGet;
    tWlanNpWrEntryDelete sWlanNpEntryDelete;
    }unOpCode;

#define  WlanNpEntryAdd  unOpCode.sWlanNpEntryAdd;
#define  WlanNpEntryGet  unOpCode.sWlanNpEntryGet;
#define  WlanNpEntryDelete  unOpCode.sWlanNpEntryDelete;
} tWlanNpModInfo;
#endif
#endif
