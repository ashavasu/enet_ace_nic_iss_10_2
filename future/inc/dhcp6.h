/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dhcp6.h,v 1.12 2015/03/14 11:49:38 siva Exp $
 *  
 * Description:This file contains the exported definitions and macros
 *   of DHCP6.
 * 
 ********************************************************************/

#ifndef _DHCP6_H
#define _DHCP6_H
#include "ipv6.h"
#define  DHCP6_INIT_COMPLETE(u4Status)    lrInitComplete(u4Status)
/* DHCP6 Trace Levels */

#define  BIND_TRC               0x00000010
#define  ALL_TRC                0x0000ffff
#define  EVENT_TRC              0x00000001
#define  FAIL_TRC               ALL_FAILURE_TRC

#define DHCP6_SNMP_TRUE         1
#define DHCP6_SNMP_FALSE        2

#define DHCP6_INIT_COMPLETE(u4Status)    lrInitComplete(u4Status)
#define BIND_TRC                0x00000010
#define ALL_TRC                 0x0000ffff
#define EVENT_TRC               0x00000001
#define FAIL_TRC                ALL_FAILURE_TRC
#define DHCP6_CLNT_OPTION_SIZE_MAX  256

#define DHCP6_ALL_RELAY_SERVER "FF02::1:2\0"
#define DHCP6_ALL_SERVER       "FF05::1:3\0"

/* Maximum Length of remote-id */
#define DHCP6_RLY_REM_ID_MAX_LEN     128

/* Remote-Id option Type */
#define DHCP6_RLY_REMOTEID_DUID         1   /* DUID */
#define DHCP6_RLY_REMOTEID_SW_NAME      2   /* Switch Name */
#define DHCP6_RLY_REMOTEID_MGMT_IP      3   /* Management IP */
#define DHCP6_RLY_REMOTEID_USERDEFINED  4   /* User defined- string */

/* Maximum packet that can be processed by DHCP6 */
#define DHCP6_MAX_MTU                CFA_ENET_MTU

enum
{
   DHCP6_CLNT_RESERVE_APP_ID = 0, /* No application is given this application ID */
   APPLICATION_DUMMY1,
   APPLICATION_DUMMY2,
   APPLICATION_DUMMY3,
   DHCP6_CLNT_APP_MAX         /* Add new app IDs before this */
};
typedef enum
{
    DHCP6_ENABLED  = 1,
    DHCP6_DISABLED = 2
}eDhcp6Enable;
enum
{
     DHCP6_CLNT_REQUEST_OPTION_CODE = 1,
     DHCP6_CLNT_USER_CLASS_VALUE = 2,
     DHCP6_CLNT_VENDOR_CLASS_VALUE = 3,
     DHCP6_CLNT_VENDOR_SPECF_VALUE = 4,
     DHCP6_CLNT_OPTION_CODE = 5
};
enum
{
     DHCP6_CLNT_APP_INFO_ADD = 1,
     DHCP6_CLNT_APP_INFO_REMOVE = 2
};
typedef struct __Dhcp6ClntAppParam
{

    INT4        (*fpDhcp6ClntCallback) (UINT2 u2AppIdId, 
                                        UINT4 u4IfIndex,
                                        UINT2 *pu2Type,
                                        UINT2 *pu2OptionLength, 
                                        UINT1 *pu1OptionValue);
    UINT2       u2AppId;
    UINT1       au1Pad[2];
} tDhcp6ClntAppParam;
typedef struct __Dhcp6ClntAppOptionParam
{
    UINT4 u4IfIndex;
    UINT1 au1Value[DHCP6_CLNT_OPTION_SIZE_MAX];
    UINT2 u2Code;
    UINT2 u2ValueLength;
    UINT1 u1OptionType;
                     /* Option
                      * User Class
                        Vendor Class
                        Vender Spec */
                                                    
    UINT1 u1Action;     /* ADD
                       * REMOVE   */
    UINT1 au1Pad[2];
}tDhcp6ClntAppOptionParam;

typedef struct __Dhcp6ClntSubOptionInfo
{
   UINT1     *pu1Value;
   UINT2     u2Length;
   UINT2     u2Code;
}tDhcp6ClntSubOptionInfo;

typedef struct __Dhcp6OptionInfo
{
   UINT1     au1Value[DHCP6_CLNT_OPTION_SIZE_MAX];
   UINT2     u2Length;
   UINT2     u2Code;
}tDhcp6OptionInfo;

PUBLIC INT4 D6ClApiRegisterApplication (UINT2 u2AppId, tDhcp6ClntAppParam *pAppParam);
PUBLIC INT4 D6ClApiDeRegisterApplication (UINT2 u2AppId);
PUBLIC INT4 D6ClApiOptionSet(UINT2 u2AppId, tDhcp6ClntAppOptionParam *pOptionParam);
PUBLIC INT4 D6ClApiOptionGet (UINT2 u2AppId, UINT4 u4IfIndex, tDhcp6OptionInfo *pOption);
PUBLIC INT4 D6ClUtlIsClientRowCreated (UINT4 u4InterfaceId );

PUBLIC VOID D6ClMainTask PROTO((INT1 *));
PUBLIC VOID D6SrMainTask PROTO ((INT1 *));
PUBLIC VOID D6RlMainTask PROTO ((INT1 *));

PUBLIC VOID D6RlVlanCliDelIfEntry (tCliHandle CliHandle, UINT4 u4IfIndex);
PUBLIC INT4 D6RlApiIp6IfStatusNotify PROTO ((tNetIpv6HliParams * pNetIpv6HlParams));
PUBLIC INT4 D6SrApiIp6IfStatusNotify PROTO ((tNetIpv6HliParams * pNetIpv6HlParams));
PUBLIC INT4 D6ClApiIp6IfStatusNotify PROTO ((tNetIpv6HliParams * pNetIpv6HlParams));
PUBLIC VOID D6SrApiShowGlobalInfo (tCliHandle CliHandle);
PUBLIC VOID D6ClApiShowGlobalInfo (tCliHandle CliHandle);
PUBLIC VOID D6RlApiShowGlobalInfo (tCliHandle CliHandle);
PUBLIC VOID D6RlApiShowInterfaceInfo (tCliHandle CliHandle,INT4);
PUBLIC VOID D6ClApiShowInterfaceInfo (tCliHandle CliHandle,INT4);
PUBLIC VOID D6SrApiShowInterfaceInfo (tCliHandle CliHandle,INT4);
PUBLIC VOID Dhcp6SrvShowTraps (tCliHandle CliHandle);
PUBLIC VOID Dhcp6ClientShowTraps (tCliHandle CliHandle);
PUBLIC VOID Dhcp6RelayShowTraps (tCliHandle CliHandle);
PUBLIC INT4 D6ClApiLock PROTO ((VOID));
PUBLIC INT4 D6ClApiUnLock PROTO ((VOID));

PUBLIC INT4 D6RlMainLock PROTO ((VOID));
PUBLIC INT4 D6RlMainUnLock PROTO ((VOID));

PUBLIC INT4 D6SrMainLock PROTO ((VOID));
PUBLIC INT4 D6SrMainUnLock PROTO ((VOID));
#endif
