/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: snpsys.h,v 1.1 2015/02/13 11:16:24 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              by  external modules.
 *
 *******************************************************************/
#ifndef _SNPSYS_H
#define _SNPSYS_H

enum {
SYS_LOG_BUFF_MEM_ALLOC_FAIL = 1,
SYS_LOG_BUFF_MEM_RELEASE_FAIL,
SYS_LOG_CRU_BUFF_REL_FAIL,
SYS_LOG_MEM_ALLOC_FAIL,
SYS_LOG_MEM_RELEASE_FAIL,
SYS_LOG_QUEUE_RELEASE_FAIL,
SYS_LOG_IGMP_SNOOP_NP_ENABLE_FAIL,
SYS_LOG_MLD_SNOOP_NP_DISABLE_FAIL,
SYS_LOG_MLD_SNOOP_NP_ENABLE_FAIL,
SYS_LOG_IGS_SNOOP_NP_ENABLE_FAIL,
SYS_LOG_IGS_SNOOP_NP_DISABLE_FAIL,
SYS_LOG_SNP_RB_TREE_ADD_FAIL,
SYS_LOG_DEL_SNOOP_NP_IP_FORWARD_FAIL,
SYS_LOG_ADD_SNOOP_NP_IP_FORWARD_FAIL,
SYS_LOG_CRU_BUFF_RELEASE_FAIL,
SYS_LOG_CRU_BUFF_ALLOC_FAIL,
SYS_LOG_CLEAR_SNOOP_NP_IPMC_FWD_ENTRY_FAIL,
SYS_LOG_IGS_SNOOP_NP_DISABLE_IP_FAIL,
SYS_LOG_IGS_NP_ENAHNCE_ENABLE_FAIL,
SYS_LOG_IGS_NP_PORT_RATE_FAIL,
SYS_LOG_IGS_NP_UPDATE_FAIL,
};

#ifdef __SNPMAIN_C__
CONST CHR1  *SnoopSysErrString [] = {
NULL,
" \r% Buffer allocation Failure\r\n",
" \r% Buffer Memory release Failure\r\n",
" \r% CRU Buffer release Failure\r\n",
" \r% Memory Allocation Failure\r\n",
" \r% Memeory Release Failure\r\n",
" \r% Queue Release Failure\r\n",
" \r% Enabling IGMP Snooping on Hardware Failed\r\n",
" \r% Disabling IGMP Snooping on Hardware Failed\r\n",
" \r% Disabling MLD Snooping on Hardware Failed\r\n",
" \r% Enabling MLD Snooping on Hardware Failed\r\n",
" \r% Enabling IGS Snooping on Hardware Failed\r\n",
" \r% Disabling IGS Snooping on Hardware Failed\r\n",
" \r% Addition of RB Tree Failed\r\n",
" \r% Deleting IP forward entry table from Hardware Failed\r\n",
" \r% Addition of IP forward entry table from Hardware Failed\r\n",
" \r% CRU Buffer release Failure\r\n",
" \r% CRU Buffer Allocation Failure\r\n",
" \r% Clearing IPMC forward entry on Hardware failed\r\n",
" \r% Disabling IGS Snooping on Hardware Failed\r\n",
" \r% Enabling IGS Enhance Mode on Hardware Failed \r\n",
" \r% Failure in setting IGS port Rate on Hardware\r\n",
" \r% IPMC updation on Hardware Failed \r\n",
};
#else
extern CONST CHR1  *SnoopSysErrString [];
#endif

#endif /*_SNPSYS_H */
