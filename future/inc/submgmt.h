/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: submgmt.h,v 1.5 2018/08/29 12:17:04 vijay Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              by  external modules.
 ********************************************************************/
#ifndef _SUBMGMT_H_
#define _SUBMGMT_H_


/*--------------------------------------------------------------------------*/
/*                       submmain.c                                          */
/*--------------------------------------------------------------------------*/
PUBLIC VOID SubMgmtMain           PROTO ((INT1 *));

PUBLIC INT4 SubMgmtLock                     PROTO ((VOID));
PUBLIC INT4 SubMgmtUnLock                   PROTO ((VOID));  

/* System Control*/
#define  SUBM_START                        1
#define  SUBM_SHUTDOWN                     2

/* Module status */
#define  SUBM_ENABLED                      1
#define  SUBM_DISABLED                     2

#define SUBM_PROF_NAME_LEN                 32

#define SUBM_STATE_MC_TRC          (0x1 << 16)
#define SUBM_TMR_TRC               (0x1 << 17)
#define SUBM_SESS_TRC              (0x1 << 18)
#define SUBM_ALL_TRC               (CONTROL_PLANE_TRC| ALL_FAILURE_TRC | ENTRY_TRC | EXIT_TRC| \
                                    INIT_SHUT_TRC| OS_RESOURCE_TRC | BUFFER_TRC | SUBM_STATE_MC_TRC| \
                                    SUBM_TMR_TRC| SUBM_SESS_TRC)

#define SUBM_STATIC							1
#define SUBM_DHCP							2
#define SUBM_IPOE                                                      3
#define SUBM_PPPOE                                                     4



/* The below structure is to get the Cvlan 
 * and Svlan information of the subscriber*/
typedef struct SubmVlanInfo
{
      UINT2     u2OuterVlanId;
      UINT2     u2InnerVlanId;
}tSubmVlanInfo;

/* The below structure will be used by 
 * outside modules to post to submgmt*/
typedef struct SubMgmtEntry
{
        tSubmVlanInfo      VlanId;
                              /*Vlan info (S-vlan and C-vlan) of the host*/
        UINT4              u4SubIfIndex;
                              /*The interface index(sub-interface) of the subscriber*/
        UINT4              u4ParentIfIndex;
                              /*The Parent interface index of sub-interface of the subscriber*/
        UINT4              u4HostIp;
                              /*IP address of the host*/
        UINT4              u4LeaseTime;
                              /*Lease time of the subscriber*/
        tMacAddr           HostMac;
                              /*Mac address of the host*/
        UINT1              u1OperStatus;
                              /*OPER-UP or OPER_DOWN of the subscriber*/
        UINT1              u1EntryType;
                              /*Type of the entry DHCP/IPoE/PPPoE*/
        UINT2              u2SessionId;
                              /*Session id specific for PPPOE*/
        UINT1              au1Pad[2];
}tSubMgmtEntry;

typedef struct SubMgmtProfile
{
     tRBNodeEmbd     SubMgmtProfRbNode;                 /* Link to Traverse the Table */
     UINT1           au1ProfName[SUBM_PROF_NAME_LEN];   /* Name of the table */
     UINT4           u4CIR;                             /* Committed Information Rate */
     UINT4           u4CBS;                             /* Committed Burst Size */
     UINT4           u4EIR;                             /* Excess information Rate */
     UINT4           u4EBS;                             /* Excess Burst Size */
     INT4            i4ProfileId;                       /* Index of Profile Table */
     UINT4           u4ConformAction1Value;              /* InProfile Conform Action1 Value */
     UINT4           u4ConformAction2Value;              /* InProfile Conform Action2 Value */
     UINT4           u4ExceedAction1Value;               /* InProfile Exceed Action1 Value */
     UINT4           u4ExceedAction2Value;               /* InProfile Exceed Action2 Value */
     UINT4           u4ClassId;                         /* CLASS Id for the Profile */
     UINT4           u4MeterId;                         /* Meter Id for the profile*/
     UINT4           u4PolicerId;                       /* Policy-Map Id for the profile */
     UINT1           u1MeterType;                       /* Metering algorithm */
     UINT1           u1ConformActionFlag;               /* Flag indicate Valid Action fields
                                                         * for In profile Conform Actions */
     UINT1           u1ExceedActionFlag;                /* Flag indicate Valid Action fields
                                                         * for In profile Exced Actions */
     UINT1           u1RowStatus;                       /* Row Status of this Entry */
}tSubMgmtProfile;


PUBLIC VOID SubmUpdatePortStatus(tSubMgmtEntry *pSubEntry);
PUBLIC VOID SubmUpdateIpAddress(tSubMgmtEntry *pSubEntry);
PUBLIC VOID SubmUpdateLeaseTime(tSubMgmtEntry *pSubEntry);
PUBLIC INT4 SubmGetInterfaceSessCounter (UINT4 u4IfIndex,UINT4 u4SessionType);
PUBLIC INT4 SubmIsModuleEnable(VOID);
#endif
