/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: issnpwr.h,v 1.1
 *
 ******************************************************************************/

#ifndef ISS_SYS_NP_WR_H
#define ISS_SYS_NP_WR_H

#include "iss.h"
#include "isspi.h"

/* OPER ID */
/* when new NP call is added in ISS, the following files 
 * has to be updated 
 * 1. npgensup.h 
 * 2. npbcmsup.h
*/
enum
{
 ISS_HW_SET_PORT_EGRESS_STATUS                         =1, 
 ISS_HW_SET_PORT_STATS_COLLECTION                      , 
 ISS_HW_SET_PORT_MODE                                  , 
 ISS_HW_SET_PORT_DUPLEX                                , 
 ISS_HW_SET_PORT_SPEED                                 , 
 ISS_HW_SET_PORT_FLOW_CONTROL                          , 
 ISS_HW_SET_PORT_RENEGOTIATE                           , 
 ISS_HW_SET_PORT_MAX_MAC_ADDR                          , 
 ISS_HW_SET_PORT_MAX_MAC_ACTION                        , 
 ISS_HW_SET_PORT_MIRRORING_STATUS                      , 
 ISS_HW_SET_MIRROR_TO_PORT                             , 
 ISS_HW_SET_INGRESS_MIRRORING                          , 
 ISS_HW_SET_EGRESS_MIRRORING                           , 
 ISS_HW_SET_RATE_LIMITING_VALUE                        , 
 ISS_HW_SET_PORT_EGRESS_PKT_RATE                       , 
 ISS_HW_GET_PORT_EGRESS_PKT_RATE                       , 
 ISS_HW_RESTART_SYSTEM                                 , 
 ISS_HW_INIT_FILTER                                    , 
 ISS_HW_GET_PORT_DUPLEX                                , 
 ISS_HW_GET_PORT_SPEED                                 , 
 ISS_HW_GET_PORT_FLOW_CONTROL                          , 
 ISS_HW_SET_PORT_H_O_L_BLOCK_PREVENTION                , 
 ISS_HW_GET_PORT_H_O_L_BLOCK_PREVENTION                , 
 ISS_HW_UPDATE_L2_FILTER                               , 
 ISS_HW_UPDATE_L3_FILTER                               , 
 ISS_HW_UPDATE_L4_S_FILTER                             , 
 ISS_HW_SEND_BUFFER_TO_LINUX                           , 
 ISS_HW_GET_FAN_STATUS                                 , 
 ISS_HW_INIT_MIRR_DATA_BASE                            , 
 ISS_HW_SET_MIRRORING                                  , 
 ISS_HW_MIRROR_ADD_REMOVE_PORT                         , 
 ISS_HW_SET_MAC_LEARNING_RATE_LIMIT                    , 
 ISS_HW_GET_LEARNED_MAC_ADDR_COUNT                     , 
 ISS_HW_UPDATE_USER_DEFINED_FILTER                     , 
 ISS_HW_SET_PORT_AUTO_NEG_ADVT_CAP_BITS                , 
 ISS_HW_GET_PORT_AUTO_NEG_ADVT_CAP_BITS                , 
 ISS_HW_SET_LEARNING_MODE                              , 
 ISS_HW_SET_PORT_MDI_OR_MDIX_CAP                       , 
 ISS_HW_GET_PORT_MDI_OR_MDIX_CAP                       , 
 ISS_HW_SET_PORT_FLOW_CONTROL_RATE                     , 
 ISS_HW_CONFIG_PORT_ISOLATION_ENTRY                    , 
 NP_SET_TRACE                                          , 
 NP_GET_TRACE                                          ,
#ifdef NPAPI_WANTED
 NP_SET_TRACE_LEVEL                                    ,         
 NP_GET_TRACE_LEVEL                                    ,
#endif
 ISS_HW_SET_SET_TRAFFIC_SEPERATION_CTRL                , 
 ISS_HW_ENABLE_HW_CONSOLE                              ,  
 ISS_HW_CPU_MIRRORING          ,
 ISS_MBSM_HW_SET_PORT_EGRESS_STATUS                    , 
 ISS_MBSM_HW_SET_PORT_STATS_COLLECTION                 , 
 ISS_MBSM_HW_SET_PORT_MIRRORING_STATUS                 , 
 ISS_MBSM_HW_SET_MIRROR_TO_PORT                        , 
 ISS_MBSM_HW_UPDATE_L2_FILTER                          , 
 ISS_MBSM_HW_UPDATE_L3_FILTER                          , 
 ISS_P_I_MBSM_HW_CONFIG_PORT_ISOLATION_ENTRY           , 
 ISS_MBSM_HW_CPU_MIRRORING          ,
 ISS_MBSM_HW_SET_MIRRORING,
 ISS_NP_HW_GET_CAPABILITIES          ,
 ISS_HW_SET_SWITCH_MODE_TYPE          ,
 ISS_NP_HW_UPDATE_RESERV_FRAME_ACTION         ,
 FS_NP_ISS_MAX_NP /* Max Count should be the Last entry */
};

/* Required arguments list for the isssys NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

typedef struct {
    UINT4  u4IfIndex;
    UINT1  u1EgressEnable;
    UINT1  au1pad[3];
} tIsssysNpWrIssHwSetPortEgressStatus;

typedef struct {
    UINT4  u4IfIndex;
    UINT1  u1StatsEnable;
    UINT1  au1pad[3];
} tIsssysNpWrIssHwSetPortStatsCollection;

typedef struct {
    UINT4  u4IfIndex;
    INT4   i4Value;
} tIsssysNpWrIssHwSetPortMode;

typedef struct {
    UINT4  u4IfIndex;
    INT4   i4Value;
} tIsssysNpWrIssHwSetPortDuplex;

typedef struct {
    UINT4  u4IfIndex;
    INT4   i4Value;
} tIsssysNpWrIssHwSetPortSpeed;

typedef struct {
    UINT4  u4IfIndex;
    UINT1  u1FlowCtrlEnable;
    UINT1  au1pad[3];
} tIsssysNpWrIssHwSetPortFlowControl;

typedef struct {
    UINT4  u4IfIndex;
    INT4   i4Value;
} tIsssysNpWrIssHwSetPortRenegotiate;

typedef struct {
    UINT4  u4IfIndex;
    INT4   i4Value;
} tIsssysNpWrIssHwSetPortMaxMacAddr;

typedef struct {
    UINT4  u4IfIndex;
    INT4   i4Value;
} tIsssysNpWrIssHwSetPortMaxMacAction;

typedef struct {
    INT4  i4MirrorStatus;
} tIsssysNpWrIssHwSetPortMirroringStatus;

typedef struct {
    UINT4  u4IfIndex;
} tIsssysNpWrIssHwSetMirrorToPort;

typedef struct {
    UINT4  u4IfIndex;
    INT4   i4IngMirroring;
} tIsssysNpWrIssHwSetIngressMirroring;

typedef struct {
    UINT4  u4IfIndex;
    INT4   i4EgrMirroring;
} tIsssysNpWrIssHwSetEgressMirroring;

typedef struct {
    UINT4  u4IfIndex;
    INT4   i4RateLimitVal;
    UINT1  u1PacketType;
    UINT1  au1pad[3];
} tIsssysNpWrIssHwSetRateLimitingValue;

typedef struct {
    UINT4  u4IfIndex;
    INT4   i4PktRate;
    INT4   i4BurstRate;
} tIsssysNpWrIssHwSetPortEgressPktRate;

typedef struct {
    UINT4   u4Port;
    INT4 *  pi4PortPktRate;
    INT4 *  pi4PortBurstRate;
} tIsssysNpWrIssHwGetPortEgressPktRate;

typedef struct {
    INT4 *  pi4PortDuplexStatus;
    UINT4   u4IfIndex;
} tIsssysNpWrIssHwGetPortDuplex;

typedef struct {
    INT4 *  pi4PortSpeed;
    UINT4   u4IfIndex;
} tIsssysNpWrIssHwGetPortSpeed;

typedef struct {
    INT4 *  pi4PortFlowControl;
    UINT4   u4IfIndex;
} tIsssysNpWrIssHwGetPortFlowControl;

typedef struct {
    UINT4  u4IfIndex;
    INT4   i4Value;
} tIsssysNpWrIssHwSetPortHOLBlockPrevention;

typedef struct {
    UINT4   u4IfIndex;
    INT4 *  pi4Value;
} tIsssysNpWrIssHwGetPortHOLBlockPrevention;

typedef struct {
    tIssL2FilterEntry *  pIssL2FilterEntry;
    INT4                 i4Value;
} tIsssysNpWrIssHwUpdateL2Filter;

typedef struct {
    tIssL3FilterEntry *  pIssL3FilterEntry;
    INT4                 i4Value;
} tIsssysNpWrIssHwUpdateL3Filter;

typedef struct {
    tIssL4SFilterEntry *  pIssL4SFilterEntry;
    INT4                  i4Value;
} tIsssysNpWrIssHwUpdateL4SFilter;

typedef struct {
   tIssReservFrmCtrlTable * pIssReservFrmCtrlInfo;
    INT4                  i4Value;
} tIsssysNpWrIssHwSetResrvFrameFilter;


typedef struct {
    UINT1 *  pBuffer;
    UINT4    u4Len;
    INT4     i4ImageType;
} tIsssysNpWrIssHwSendBufferToLinux;

typedef struct {
    UINT4    u4FanIndex;
    UINT4 *  pu4FanStatus;
} tIsssysNpWrIssHwGetFanStatus;

typedef struct {
    tIssHwMirrorInfo *  pMirrorInfo;
} tIsssysNpWrIssHwSetMirroring;

typedef struct {
    UINT4  u4SrcIndex;
    UINT4  u4DestIndex;
    UINT1  u1Mode;
    UINT1  u1MirrCfg;
    UINT1  au1pad[2];
} tIsssysNpWrIssHwMirrorAddRemovePort;

typedef struct {
    INT4  i4IssMacLearnLimitVal;
} tIsssysNpWrIssHwSetMacLearningRateLimit;

typedef struct {
    INT4 i4IssSwitchModeType;
} tIsssysNpWrIssHwSetSwitchModeType;


typedef struct {
    UINT1 *  pu1Buffer;
} tIsssysNpWrIssHwEnableHwConsole;

typedef struct {
    INT4 *  pi4LearnedMacAddrCount;
} tIsssysNpWrIssHwGetLearnedMacAddrCount;

typedef struct {
    tIssUDBFilterEntry *  pAccessFilterEntry;
    tIssL2FilterEntry *   pL2AccessFilterEntry;
    tIssL3FilterEntry *   pL3AccessFilterEntry;
    INT4                  i4FilterAction;
} tIsssysNpWrIssHwUpdateUserDefinedFilter;

typedef struct {
    UINT4   u4IfIndex;
    INT4 *  pi4MauCapBits;
    INT4 *  pi4IssCapBits;
} tIsssysNpWrIssHwSetPortAutoNegAdvtCapBits;

typedef struct {
    UINT4   u4IfIndex;
    INT4 *  pElement;
} tIsssysNpWrIssHwGetPortAutoNegAdvtCapBits;

typedef struct {
    UINT4  u4IfIndex;
    INT4   i4Status;
} tIsssysNpWrIssHwSetLearningMode;

typedef struct {
    UINT4  u4IfIndex;
    INT4   i4Value;
} tIsssysNpWrIssHwSetPortMdiOrMdixCap;

typedef struct {
    INT4 *  pi4PortStatus;
    UINT4   u4IfIndex;
} tIsssysNpWrIssHwGetPortMdiOrMdixCap;

typedef struct {
    INT4   i4PortMaxRate;
    INT4   i4PortMinRate;
    UINT4  u4IfIndex;
} tIsssysNpWrIssHwSetPortFlowControlRate;

typedef struct {
    tIssHwUpdtPortIsolation *  pPortIsolation;
} tIsssysNpWrIssHwConfigPortIsolationEntry;

typedef struct {
    UINT1  u1TraceModule;
    UINT1  u1TraceLevel;
    UINT1  au1pad[2];
} tIsssysNpWrNpSetTrace;

typedef struct {
    UINT1 *  pu1TraceLevel;
    UINT1    u1TraceModule;
    UINT1    au1pad[3];
} tIsssysNpWrNpGetTrace;
#ifdef NPAPI_WANTED
typedef struct {
    UINT2  u2TraceLevel;
    UINT1  au1pad[2];
} tIsssysNpWrNpSetTraceLevel;

typedef struct {
    UINT2 *  pu2TraceLevel;
} tIsssysNpWrNpGetTraceLevel;
#endif

typedef struct {
    INT4  i4CtrlStatus;
 } tIsssysNpWrIssHwSetSetTrafficSeperationCtrl;

#ifdef MBSM_WANTED
typedef struct {
    UINT4            u4IfIndex;
    tMbsmSlotInfo *  pSlotInfo;
    UINT1            u1Status;
    UINT1            au1pad[3];
} tIsssysNpWrIssMbsmHwSetPortEgressStatus;

typedef struct {
    UINT4            u4IfIndex;
    tMbsmSlotInfo *  pSlotInfo;
    UINT1            u1Status;
    UINT1            au1pad[3];
} tIsssysNpWrIssMbsmHwSetPortStatsCollection;

typedef struct {
    INT4             i4MirrorStatus;
    tMbsmSlotInfo *  pSlotInfo;
} tIsssysNpWrIssMbsmHwSetPortMirroringStatus;

typedef struct {
    UINT4            u4MirrorPort;
    tMbsmSlotInfo *  pSlotInfo;
} tIsssysNpWrIssMbsmHwSetMirrorToPort;

typedef struct {
    tIssL2FilterEntry *  pIssL2FilterEntry;
    INT4                 i4Value;
    tMbsmSlotInfo *      pSlotInfo;
} tIsssysNpWrIssMbsmHwUpdateL2Filter;

typedef struct {
    tIssL3FilterEntry *  pIssL3FilterEntry;
    INT4                 i4Value;
    tMbsmSlotInfo *      pSlotInfo;
} tIsssysNpWrIssMbsmHwUpdateL3Filter;

typedef struct {
    tIssHwUpdtPortIsolation *  pHwUpdPortIsolation;
    tIssMbsmInfo *             pIssMbsmInfo;
} tIsssysNpWrIssPIMbsmHwConfigPortIsolationEntry;

typedef struct {
    tIssHwMirrorInfo *  pMirrorInfo;
    tMbsmSlotInfo *      pSlotInfo;
} tIsssysNpWrIssMbsmHwSetMirroring;

#endif

typedef struct {
    tNpIssHwGetCapabilities *  pHwGetCapabilities;
} tIsssysNpWrIssNpHwGetCapabilities;

typedef struct {
    UINT4    u4IssCpuMirrorToPort;
    INT4     i4IssCpuMirrorType;
}tIsssysNpWrIssHwSetCpuMirroring;


typedef struct {
    tMbsmSlotInfo *  pSlotInfo;
    UINT4             u4IssCpuMirrorToPort;
    INT4              i4IssCpuMirrorType;
}tIsssysNpWrIssMbsmHwSetCpuMirroring;

typedef struct IsssysNpModInfo {
union {
    tIsssysNpWrIssHwSetPortEgressStatus  sIssHwSetPortEgressStatus;
    tIsssysNpWrIssHwSetPortStatsCollection  sIssHwSetPortStatsCollection;
    tIsssysNpWrIssHwSetPortMode  sIssHwSetPortMode;
    tIsssysNpWrIssHwSetPortDuplex  sIssHwSetPortDuplex;
    tIsssysNpWrIssHwSetPortSpeed  sIssHwSetPortSpeed;
    tIsssysNpWrIssHwSetPortFlowControl  sIssHwSetPortFlowControl;
    tIsssysNpWrIssHwSetPortRenegotiate  sIssHwSetPortRenegotiate;
    tIsssysNpWrIssHwSetPortMaxMacAddr  sIssHwSetPortMaxMacAddr;
    tIsssysNpWrIssHwSetPortMaxMacAction  sIssHwSetPortMaxMacAction;
    tIsssysNpWrIssHwSetPortMirroringStatus  sIssHwSetPortMirroringStatus;
    tIsssysNpWrIssHwSetMirrorToPort  sIssHwSetMirrorToPort;
    tIsssysNpWrIssHwSetIngressMirroring  sIssHwSetIngressMirroring;
    tIsssysNpWrIssHwSetEgressMirroring  sIssHwSetEgressMirroring;
    tIsssysNpWrIssHwSetRateLimitingValue  sIssHwSetRateLimitingValue;
    tIsssysNpWrIssHwSetPortEgressPktRate  sIssHwSetPortEgressPktRate;
    tIsssysNpWrIssHwGetPortEgressPktRate  sIssHwGetPortEgressPktRate;
    tIsssysNpWrIssHwGetPortDuplex  sIssHwGetPortDuplex;
    tIsssysNpWrIssHwGetPortSpeed  sIssHwGetPortSpeed;
    tIsssysNpWrIssHwGetPortFlowControl  sIssHwGetPortFlowControl;
    tIsssysNpWrIssHwSetPortHOLBlockPrevention  sIssHwSetPortHOLBlockPrevention;
    tIsssysNpWrIssHwGetPortHOLBlockPrevention  sIssHwGetPortHOLBlockPrevention;
    tIsssysNpWrIssHwUpdateL2Filter  sIssHwUpdateL2Filter;
    tIsssysNpWrIssHwUpdateL3Filter  sIssHwUpdateL3Filter;
    tIsssysNpWrIssHwUpdateL4SFilter  sIssHwUpdateL4SFilter;
    tIsssysNpWrIssHwSetResrvFrameFilter sIssHwSetResrvFrameFilter;
    tIsssysNpWrIssHwSendBufferToLinux  sIssHwSendBufferToLinux;
    tIsssysNpWrIssHwGetFanStatus  sIssHwGetFanStatus;
    tIsssysNpWrIssHwSetMirroring  sIssHwSetMirroring;
    tIsssysNpWrIssHwMirrorAddRemovePort  sIssHwMirrorAddRemovePort;
    tIsssysNpWrIssHwSetMacLearningRateLimit  sIssHwSetMacLearningRateLimit;
    tIsssysNpWrIssHwSetSwitchModeType sIssHwSetSwitchModeType;
    tIsssysNpWrIssHwGetLearnedMacAddrCount  sIssHwGetLearnedMacAddrCount;
    tIsssysNpWrIssHwUpdateUserDefinedFilter  sIssHwUpdateUserDefinedFilter;
    tIsssysNpWrIssHwSetPortAutoNegAdvtCapBits  sIssHwSetPortAutoNegAdvtCapBits;
    tIsssysNpWrIssHwGetPortAutoNegAdvtCapBits  sIssHwGetPortAutoNegAdvtCapBits;
    tIsssysNpWrIssHwSetLearningMode  sIssHwSetLearningMode;
    tIsssysNpWrIssHwSetPortMdiOrMdixCap  sIssHwSetPortMdiOrMdixCap;
    tIsssysNpWrIssHwGetPortMdiOrMdixCap  sIssHwGetPortMdiOrMdixCap;
    tIsssysNpWrIssHwSetPortFlowControlRate  sIssHwSetPortFlowControlRate;
    tIsssysNpWrIssHwConfigPortIsolationEntry  sIssHwConfigPortIsolationEntry;
    tIsssysNpWrNpSetTrace sNpSetTrace;
    tIsssysNpWrNpGetTrace sNpGetTrace;
#ifdef NPAPI_WANTED
    tIsssysNpWrNpSetTraceLevel  sNpSetTraceLevel;
    tIsssysNpWrNpGetTraceLevel  sNpGetTraceLevel;
#endif
    tIsssysNpWrIssHwSetSetTrafficSeperationCtrl  sIssHwSetSetTrafficSeperationCtrl;
#ifdef MBSM_WANTED
    tIsssysNpWrIssMbsmHwSetPortEgressStatus  sIssMbsmHwSetPortEgressStatus;
    tIsssysNpWrIssMbsmHwSetPortStatsCollection  sIssMbsmHwSetPortStatsCollection;
    tIsssysNpWrIssMbsmHwSetPortMirroringStatus  sIssMbsmHwSetPortMirroringStatus;
    tIsssysNpWrIssMbsmHwSetMirrorToPort  sIssMbsmHwSetMirrorToPort;
    tIsssysNpWrIssMbsmHwUpdateL2Filter  sIssMbsmHwUpdateL2Filter;
    tIsssysNpWrIssMbsmHwUpdateL3Filter  sIssMbsmHwUpdateL3Filter;
    tIsssysNpWrIssPIMbsmHwConfigPortIsolationEntry  sIssPIMbsmHwConfigPortIsolationEntry;
    tIsssysNpWrIssMbsmHwSetCpuMirroring sIssMbsmHwSetCpuMirroring;
    tIsssysNpWrIssMbsmHwSetMirroring sIssMbsmHwSetMirroring;
#endif
    tIsssysNpWrIssHwEnableHwConsole  sIsssysNpWrIssHwEnableHwConsole; 
 tIsssysNpWrIssNpHwGetCapabilities  sIssNpHwGetCapabilities;
    tIsssysNpWrIssHwSetCpuMirroring sIssHwSetCpuMirroring;
    }unOpCode;

#define  IsssysNpIssHwSetPortEgressStatus  unOpCode.sIssHwSetPortEgressStatus;
#define  IsssysNpIssHwSetPortStatsCollection  unOpCode.sIssHwSetPortStatsCollection;
#define  IsssysNpIssHwSetPortMode  unOpCode.sIssHwSetPortMode;
#define  IsssysNpIssHwSetPortDuplex  unOpCode.sIssHwSetPortDuplex;
#define  IsssysNpIssHwSetPortSpeed  unOpCode.sIssHwSetPortSpeed;
#define  IsssysNpIssHwSetPortFlowControl  unOpCode.sIssHwSetPortFlowControl;
#define  IsssysNpIssHwSetPortRenegotiate  unOpCode.sIssHwSetPortRenegotiate;
#define  IsssysNpIssHwSetPortMaxMacAddr  unOpCode.sIssHwSetPortMaxMacAddr;
#define  IsssysNpIssHwSetPortMaxMacAction  unOpCode.sIssHwSetPortMaxMacAction;
#define  IsssysNpIssHwSetPortMirroringStatus  unOpCode.sIssHwSetPortMirroringStatus;
#define  IsssysNpIssHwSetMirrorToPort  unOpCode.sIssHwSetMirrorToPort;
#define  IsssysNpIssHwSetIngressMirroring  unOpCode.sIssHwSetIngressMirroring;
#define  IsssysNpIssHwSetEgressMirroring  unOpCode.sIssHwSetEgressMirroring;
#define  IsssysNpIssHwSetRateLimitingValue  unOpCode.sIssHwSetRateLimitingValue;
#define  IsssysNpIssHwSetPortEgressPktRate  unOpCode.sIssHwSetPortEgressPktRate;
#define  IsssysNpIssHwGetPortEgressPktRate  unOpCode.sIssHwGetPortEgressPktRate;
#define  IsssysNpIssHwGetPortDuplex  unOpCode.sIssHwGetPortDuplex;
#define  IsssysNpIssHwGetPortSpeed  unOpCode.sIssHwGetPortSpeed;
#define  IsssysNpIssHwGetPortFlowControl  unOpCode.sIssHwGetPortFlowControl;
#define  IsssysNpIssHwSetPortHOLBlockPrevention  unOpCode.sIssHwSetPortHOLBlockPrevention;
#define  IsssysNpIssHwGetPortHOLBlockPrevention  unOpCode.sIssHwGetPortHOLBlockPrevention;
#define  IsssysNpIssHwUpdateL2Filter  unOpCode.sIssHwUpdateL2Filter;
#define  IsssysNpIssHwUpdateL3Filter  unOpCode.sIssHwUpdateL3Filter;
#define  IsssysNpIssHwUpdateL4SFilter  unOpCode.sIssHwUpdateL4SFilter;
#define  IsssysNpIssHwSetResrvFrameFilter  unOpCode.sIssHwSetResrvFrameFilter;
#define  IsssysNpIssHwSendBufferToLinux  unOpCode.sIssHwSendBufferToLinux;
#define  IsssysNpIssHwGetFanStatus  unOpCode.sIssHwGetFanStatus;
#define  IsssysNpIssHwSetMirroring  unOpCode.sIssHwSetMirroring;
#define  IsssysNpIssHwMirrorAddRemovePort  unOpCode.sIssHwMirrorAddRemovePort;
#define  IsssysNpIssHwSetMacLearningRateLimit  unOpCode.sIssHwSetMacLearningRateLimit;
#define  IsssysNpIssHwSetSwitchModeType unOpCode.sIssHwSetSwitchModeType;
#define  IsssysNpIssHwGetLearnedMacAddrCount  unOpCode.sIssHwGetLearnedMacAddrCount;
#define  IsssysNpIssHwUpdateUserDefinedFilter  unOpCode.sIssHwUpdateUserDefinedFilter;
#define  IsssysNpIssHwSetPortAutoNegAdvtCapBits  unOpCode.sIssHwSetPortAutoNegAdvtCapBits;
#define  IsssysNpIssHwGetPortAutoNegAdvtCapBits  unOpCode.sIssHwGetPortAutoNegAdvtCapBits;
#define  IsssysNpIssHwSetLearningMode  unOpCode.sIssHwSetLearningMode;
#define  IsssysNpIssHwSetPortMdiOrMdixCap  unOpCode.sIssHwSetPortMdiOrMdixCap;
#define  IsssysNpIssHwGetPortMdiOrMdixCap  unOpCode.sIssHwGetPortMdiOrMdixCap;
#define  IsssysNpIssHwSetPortFlowControlRate  unOpCode.sIssHwSetPortFlowControlRate;
#define  IsssysNpIssHwConfigPortIsolationEntry  unOpCode.sIssHwConfigPortIsolationEntry;
#define  IsssysNpNpSetTrace unOpCode.sNpSetTrace;
#define  IsssysNpNpGetTrace unOpCode.sNpGetTrace;
#ifdef NPAPI_WANTED
#define  IsssysNpNpSetTraceLevel  unOpCode.sNpSetTraceLevel;
#define  IsssysNpNpGetTraceLevel  unOpCode.sNpGetTraceLevel;
#endif
#define  IsssysNpIssHwSetSetTrafficSeperationCtrl  unOpCode.sIssHwSetSetTrafficSeperationCtrl;
#ifdef MBSM_WANTED
#define  IsssysNpIssMbsmHwSetPortEgressStatus  unOpCode.sIssMbsmHwSetPortEgressStatus;
#define  IsssysNpIssMbsmHwSetPortStatsCollection  unOpCode.sIssMbsmHwSetPortStatsCollection;
#define  IsssysNpIssMbsmHwSetPortMirroringStatus  unOpCode.sIssMbsmHwSetPortMirroringStatus;
#define  IsssysNpIssMbsmHwSetMirrorToPort  unOpCode.sIssMbsmHwSetMirrorToPort;
#define  IsssysNpIssMbsmHwUpdateL2Filter  unOpCode.sIssMbsmHwUpdateL2Filter;
#define  IsssysNpIssMbsmHwUpdateL3Filter  unOpCode.sIssMbsmHwUpdateL3Filter;
#define  IsssysNpIssPIMbsmHwConfigPortIsolationEntry  unOpCode.sIssPIMbsmHwConfigPortIsolationEntry;
#define  IsssysNpIssMbsmHwSetCpuMirroring  unOpCode.sIssMbsmHwSetCpuMirroring
#define  IsssysNpIssMbsmHwSetMirroring  unOpCode.sIssMbsmHwSetMirroring;
#endif
#define  IsssysNpWrIssHwEnableHwConsole  unOpCode.sIsssysNpWrIssHwEnableHwConsole; 
#define  IsssysNpIssNpHwGetCapabilities  unOpCode.sIssNpHwGetCapabilities;
#define  IsssysNpWrIssHwSetCpuMirroring unOpCode.sIssHwSetCpuMirroring;
} tIsssysNpModInfo;


/* Function Prototype for Generic NP OpCode specific wrappers
 * defined in Isssysnputil.c */

UINT1 IsssysIssHwSetPortEgressStatus PROTO ((UINT4 u4IfIndex, UINT1 u1EgressEnable));
UINT1 IsssysIssHwSetPortStatsCollection PROTO ((UINT4 u4IfIndex, UINT1 u1StatsEnable));
UINT1 IsssysIssHwSetPortMode PROTO ((UINT4 u4IfIndex, INT4 i4Value));
UINT1 IsssysIssHwSetPortDuplex PROTO ((UINT4 u4IfIndex, INT4 i4Value));
UINT1 IsssysIssHwSetPortSpeed PROTO ((UINT4 u4IfIndex, INT4 i4Value));
UINT1 IsssysIssHwSetPortFlowControl PROTO ((UINT4 u4IfIndex, UINT1 u1FlowCtrlEnable));
UINT1 IsssysIssHwSetPortRenegotiate PROTO ((UINT4 u4IfIndex, INT4 i4Value));
UINT1 IsssysIssHwSetPortMaxMacAddr PROTO ((UINT4 u4IfIndex, INT4 i4Value));
UINT1 IsssysIssHwSetPortMaxMacAction PROTO ((UINT4 u4IfIndex, INT4 i4Value));
UINT1 IsssysIssHwSetPortMirroringStatus PROTO ((INT4 i4MirrorStatus));
UINT1 IsssysIssHwSetMirrorToPort PROTO ((UINT4 u4IfIndex));
UINT1 IsssysIssHwSetIngressMirroring PROTO ((UINT4 u4IfIndex, INT4 i4IngMirroring));
UINT1 IsssysIssHwSetEgressMirroring PROTO ((UINT4 u4IfIndex, INT4 i4EgrMirroring));
UINT1 IsssysIssHwSetRateLimitingValue PROTO ((UINT4 u4IfIndex, UINT1 u1PacketType, INT4 i4RateLimitVal));
UINT1 IsssysIssHwSetPortEgressPktRate PROTO ((UINT4 u4IfIndex, INT4 i4PktRate, INT4 i4BurstRate));
UINT1 IsssysIssHwGetPortEgressPktRate PROTO ((UINT4 u4Port, INT4 * pi4PortPktRate, INT4 * pi4PortBurstRate));
UINT1 IsssysIssHwRestartSystem PROTO ((VOID));
UINT1 IsssysIssHwInitFilter PROTO ((VOID));
UINT1 IsssysIssHwGetPortDuplex PROTO ((UINT4 u4IfIndex, INT4 * pi4PortDuplexStatus));
UINT1 IsssysIssHwGetPortSpeed PROTO ((UINT4 u4IfIndex, INT4 * pi4PortSpeed));
UINT1 IsssysIssHwGetPortFlowControl PROTO ((UINT4 u4IfIndex, INT4 * pi4PortFlowControl));
UINT1 IsssysIssHwSetPortHOLBlockPrevention PROTO ((UINT4 u4IfIndex, INT4 i4Value));
UINT1 IsssysIssHwGetPortHOLBlockPrevention PROTO ((UINT4 u4IfIndex, INT4 * pi4Value));
UINT1 IsssysIssHwUpdateL2Filter PROTO ((tIssL2FilterEntry * pIssL2FilterEntry, INT4 i4Value));
UINT1 IsssysIssHwUpdateL3Filter PROTO ((tIssL3FilterEntry * pIssL3FilterEntry, INT4 i4Value));
UINT1 IsssysIssHwUpdateL4SFilter PROTO ((tIssL4SFilterEntry * pIssL4SFilterEntry, INT4 i4Value));
UINT1 IsssysIssHwSetResrvFrameFilter PROTO ((tIssReservFrmCtrlTable * , INT4));
UINT1 IsssysIssHwSendBufferToLinux PROTO ((UINT1 * pBuffer, UINT4 u4Len, INT4 i4ImageType));
UINT1 IsssysIssHwGetFanStatus PROTO ((UINT4 u4FanIndex, UINT4 * pu4FanStatus));
UINT1 IsssysIssHwInitMirrDataBase PROTO ((VOID));
UINT1 IsssysIssHwSetMirroring PROTO ((tIssHwMirrorInfo * pMirrorInfo));
UINT1 IsssysIssHwMirrorAddRemovePort PROTO ((UINT4 u4SrcIndex, UINT4 u4DestIndex, UINT1 u1Mode, UINT1 u1MirrCfg));
UINT1 IsssysIssHwSetMacLearningRateLimit PROTO ((INT4 i4IssMacLearnLimitVal));
UINT1 IsssysIssHwGetLearnedMacAddrCount PROTO ((INT4 * pi4LearnedMacAddrCount));
UINT1 IsssysIssHwUpdateUserDefinedFilter PROTO ((tIssUDBFilterEntry * pAccessFilterEntry, tIssL2FilterEntry * pL2AccessFilterEntry, tIssL3FilterEntry * pL3AccessFilterEntry, INT4 i4FilterAction));
UINT1 IsssysIssHwSetPortAutoNegAdvtCapBits PROTO ((UINT4 u4IfIndex, INT4 * pi4MauCapBits, INT4 * pi4IssCapBits));
UINT1 IsssysIssHwGetPortAutoNegAdvtCapBits PROTO ((UINT4 u4IfIndex, INT4 * pElement));
UINT1 IsssysIssHwSetLearningMode PROTO ((UINT4 u4IfIndex, INT4 i4Status));
UINT1 IsssysIssHwSetPortMdiOrMdixCap PROTO ((UINT4 u4IfIndex, INT4 i4Value));
UINT1 IsssysIssHwGetPortMdiOrMdixCap PROTO ((UINT4 u4IfIndex, INT4 * pi4PortStatus));
UINT1 IsssysIssHwSetPortFlowControlRate PROTO ((UINT4 u4IfIndex, INT4 i4PortMaxRate, INT4 i4PortMinRate));
UINT1 IsssysIssHwEnableHwConsole PROTO ((UINT1 *pu1HwCmd)); 
UINT1 IsssysIssHwConfigPortIsolationEntry PROTO ((tIssHwUpdtPortIsolation * pPortIsolation));
UINT1 IsssysNpSetTrace PROTO ((UINT1 u1TraceModule, UINT1 u1TraceLevel));
UINT1 IsssysNpGetTrace PROTO ((UINT1 u1TraceModule, UINT1 * pu1TraceLevel));
UINT1 IsssysNpSetTraceLevel (UINT2 u2TraceLevel);
UINT1 IsssysNpGetTraceLevel (UINT2 *pu2TraceLevel);
UINT1 IsssysIssHwSetSetTrafficSeperationCtrl PROTO ((INT4 i4CtrlStatus));
#ifdef MBSM_WANTED
UINT1 IsssysIssMbsmHwSetPortEgressStatus PROTO ((UINT4 u4IfIndex, UINT1 u1Status, tMbsmSlotInfo * pSlotInfo));
UINT1 IsssysIssMbsmHwSetPortStatsCollection PROTO ((UINT4 u4IfIndex, UINT1 u1Status, tMbsmSlotInfo * pSlotInfo));
UINT1 IsssysIssMbsmHwSetPortMirroringStatus PROTO ((INT4 i4MirrorStatus, tMbsmSlotInfo * pSlotInfo));
UINT1 IsssysIssMbsmHwSetMirrorToPort PROTO ((UINT4 u4MirrorPort, tMbsmSlotInfo * pSlotInfo));
UINT1 IsssysIssMbsmHwUpdateL2Filter PROTO ((tIssL2FilterEntry * pIssL2FilterEntry, INT4 i4Value, tMbsmSlotInfo * pSlotInfo));
UINT1 IsssysIssMbsmHwUpdateL3Filter PROTO ((tIssL3FilterEntry * pIssL3FilterEntry, INT4 i4Value, tMbsmSlotInfo * pSlotInfo));
UINT1 IsssysIssPIMbsmHwConfigPortIsolationEntry PROTO ((tIssHwUpdtPortIsolation * pHwUpdPortIsolation, tIssMbsmInfo * pIssMbsmInfo));
UINT1 IsssysIssMbsmHwSetCpuMirroring (INT4 i4IssCpuMirrorType,  UINT4 u4IssCpuMirrorToPort, tMbsmSlotInfo * pSlotInfo);
UINT1 IsssysIssMbsmHwSetMirroring PROTO ((tIssHwMirrorInfo * pMirrorInfo, tMbsmSlotInfo * pSlotInfo));
#endif
UINT1 IsssysIssNpHwGetCapabilities PROTO ((tNpIssHwGetCapabilities * pHwGetCapabilities));
UINT1 IsssysIssHwSetSwitchModeType PROTO ((INT4 i4IssSwitchModeType));
UINT1  IsssysIssHwSetCpuMirroring PROTO ((INT4 , UINT4));

#endif /* ISS_SYS_NP_WR_H */
