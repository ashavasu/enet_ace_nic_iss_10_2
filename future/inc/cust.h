/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cust.h,v 1.99 2017/12/28 10:40:14 siva Exp $
 *
 * Description:This file contains the exported definitions and  
 *             macros for customer specific purpose
 *
 *******************************************************************/
#ifndef __CUST_H_
#define __CUST_H_

#include "fssnmp.h"

#ifdef DX260
#define ISS_HW_VERSION          "1.3.2"
#else
#ifdef DX285
#define ISS_HW_VERSION          "1.3.71"
#else
#ifdef SWC
#define ISS_HW_VERSION          "0.2.3"
#else
#ifdef MRVLLS
#define ISS_HW_VERSION          "2.6"
#else
#ifdef PETRA_WANTED
#define ISS_HW_VERSION          "7.0.5"
#else
#define ISS_HW_VERSION          "5.5.5"
#endif
#endif
#endif
#endif
#endif
#if defined(OPUS24) || defined (OPUS_NEM)
#define SYS_DEF_MAX_DATA_PORT_COUNT  24 /* Maximum number of ports */
#define ISS_NP_FIRST_40G_PORT   0
#elif defined(OPUS_TOR)
#define SYS_DEF_MAX_DATA_PORT_COUNT  72 /* Maximum number of ports */
#define ISS_NP_FIRST_40G_PORT   0
#elif defined(ALTA_WANTED)
#define SYS_DEF_MAX_DATA_PORT_COUNT   72
#define IPIF_CUST_MAX_MTU           7136
#define PING_CUST_MAX_DATA_SIZE     7136
#define PING_CUST_MAX_INSTANCES       54
#if defined(OPUSR72)
#define ISS_NP_FIRST_40G_PORT   1
#else
#define ISS_NP_FIRST_40G_PORT   49
#endif
#else
#define ISS_NP_FIRST_40G_PORT       0  
#define IPIF_CUST_MAX_MTU           0
#define PING_CUST_MAX_DATA_SIZE     0
#define PING_CUST_MAX_INSTANCES     0
#endif

/* determines the size of the flash array used for remote restore/save of
 * configurations */
#define MSR_MIBENTRY_MEMBLK_SIZE         10000 

#define ISS_CONFIG_CUST_FILE2              "iss_bkp.conf"

#define ISS_CUST_FW_VERSION     "6.7.2"
#define FLASH_CUST              ""
#define FLASH_USERS                    FLASH
#define FLASH_GROUPS                   FLASH
#define FLASH_PRIVILEGES               FLASH

#define ISS_USERS_FILE_NAME            "users"
#define ISS_GROUPS_FILE_NAME           "groups"
#define ISS_PRIVILEGES_FILE_NAME       "privil"
#define ISS_BANNER_FILE_NAME           "banner.txt"

#define SNMP_SYS_DESCR           "SNMPV2"
#define SNMP_SYS_NAME            "Aricent Linux Router Ver 1.0"
#define SNMP_SYS_CONTACT         "Aricent Ltd, India"
#define SNMP_SYS_LOCATION        "Aricent Ltd, India"

#define LA_NOTIFY               1 /*Indicates that the notification
                                    is send by the LA module*/

#define STP_NOTIFY  2 /* Indicates that the notification is 
                             sent by STP module for bpdu guard action*/
                            
#define LA_NOTIFY_LINK_STATUS   1/* Indicates a port channel becoming
                                    oper up or oper down*/
#define LA_NOTIFY_PORT_IN_BUNDLE_STATUS 2 /*Indicates a port
                                                 becoming oper up or oper down
                                                 in a port channel*/
#define LA_NOTIFY_DEFAULTED 3      /*Indicates that a port has entered the
                                     defaulted state*/
#define LA_NOTIFY_PARAMETER_CHANGE 4/*Indicates that the partner LACP PDU
                                      has different parameters than what
                                     is currently stored as the partner
                                     information*/

#define LA_NOTIFY_RECOVERY        5 /* Indicates whether recovery mechanism
                                       is to be triggered */

#define LA_ERROR_REC_THRESHOLD_EXCEEDED  6 /* Indicates that the defaulted state port
                                            has exceeded the threshold for error recovery */


#define ISS_CUST_DEFAULT_MTU_SIZE    1500

#define FWL_FILE_NAME_EXT       "fwl.alert"
#define IDS_FILE_NAME_EXT       "ids.alert"
#define LOG_FILE_NAME_EXT       "log.alert"

#define ISS_CUST_LOG_DIRECTORY  FLASH "LogDir"
#define FWL_LOG_FILE_PATH       ISS_LOG_DIRECTORY
#define AUDIT_LOG_FILE_PATH     ISS_LOG_DIRECTORY
#define IDS_LOG_FILE_PATH       ISS_LOG_DIRECTORY
#define LOG_FILE_PATH           ISS_LOG_DIRECTORY

#define ISS_CUST_SYS_DEF_IP_ADDRESS          0x0a000001
#define ISS_CUST_SYS_DEF_RES_IP_ADDRESS      0x0a000002
#define ISS_CUST_SYS_DEF_RES_STR_IP_ADDRESS  "0x0a000002"
#define ISS_CUST_SYS_DEF_IP_MASK             0xff000000
#define ISS_CUST_SYS_DEF_RM_IP_ADDRESS       0x81000000
#define ISS_CUST_SYS_DEF_RM_SUBNET_MASK      0xff000000
#define ISS_CUST_SYS_DEF_RM_STK_INTERFACE    "STK0"

#define ISS_CUST_SYS_DEF_IPV6_ADDRESS        "0::0" 
#define ISS_CUST_SYS_DEF_IPV6_PREFIX_LEN     0

#define ISS_CUST_IMAGE_FILE     "iss.exe" 
#define ISS_CUST_IMAGE_PATH     FLASH
#define ISS_CUST_SYS_EXTN_STRG_PATH ""
#define ISS_CUST_SYS_DEF_ROOT_USER           "root"
#define ISS_CUST_SYS_DEF_GUEST_USER          "guest"
#define ISS_CUST_SYS_DEF_ROOT_PASSWD         "admin123"
#define ISS_CUST_SYS_DEF_GUEST_PASSWD        "guest123"

#define ISS_CUST_DHCP_MAX_RETRY     4

#define CUST_LOG_MODE CONSOLE_LOG
#define SYSLOG_DEFAULT_MODE         OSIX_FALSE

/* For Customer specific BGP Router-id selection */
#define CUST_BGP_ROUTER_ID_SELECT    OSIX_FALSE 

UINT4 IssCustGetMacAddrLogic PROTO ((VOID));

VOID  IssCustGetSysMonReqStatus PROTO ((VOID));
#define ISS_CUST_CTRL_PLANE_MAC     1
#define ISS_CUST_DATA_PLANE_MAC     2

typedef union FsCbInfo {
#ifdef LA_WANTED
    INT4 (*pLaPktRxCallBack) (UINT2, UINT2 *);
    INT4 (*pLaPktTxCallBack) (UINT2, UINT2 *);
    INT4 (*pLaGetDistribPortCallBack) (UINT2, UINT2 *);
    INT4 (*pLaGetActivePortCallBack) (UINT2, UINT2 *);
#endif
#ifdef SNMP_3_WANTED
    INT4 (*pSnmpUpdateEOIDCallBack) (tSNMP_OID_TYPE *,tSNMP_OID_TYPE *,UINT1);
    INT4 (*pSnmpRevertEOIDCallBack) (tSNMP_OID_TYPE *, tSNMP_OID_TYPE *);
    INT4 (*pSnmpUpdateGetNextEOIDCallBack) (tSNMP_OID_TYPE *, tMbDbEntry **, UINT4 *, tSnmpDbEntry *);
#endif
#ifdef ISS_WANTED
    INT4 (*pIssInitCallBack) (VOID);

    /* For Firmware Cutomize upgradtion */
    INT4 (*pIssCustFirmUpgrade) PROTO(( CONST  UINT1* , UINT1*));
#endif

#ifdef CLI_WANTED
    INT4 (*pCliCustCheckrootLoginAllowed) (INT2);
    INT4 (*pCliCustCheckStrictPasswd) (VOID);
#endif

#ifdef SSL_WANTED
    INT4 (*pIssCustCheckSslCustMode) (VOID);
#endif

#ifdef SSH_WANTED
    INT4 (*pIssCustCheckSshCustMode) (VOID);
    INT4 (*pIssCustSramReadPublicKey) (INT1 *);    
#endif


#ifdef SYSLOG_WANTED
    INT4 (*pSysLogRegister) (CONST UINT1 *, UINT4, UINT4 *);
    INT4 (*pSysLogDeRegister) (UINT4);
    INT4 (*pSysLogMsg) (UINT4, UINT4, const CHR1 *, ...);
#endif

#ifdef IKE_WANTED
    INT4 (*pIkeWritePreSharedKeyToNVRAM) (VOID);
#endif

#ifdef VPN_WANTED
    INT4 (*pVpnReadPreSharedKeyFromNVRAM) (VOID);
#endif

#ifdef RADIUS_WANTED
    INT4 (*pRadUtilReadSecretFromNVRAM) (VOID);
    INT4 (*pRadUtilWriteSecretToNVRAM) (VOID);
#endif

    INT4 (*pCfaCustFwlCheck) PROTO ((UINT4)); /*For CFA  Module*/
    INT4 (*pFwlCustIfCheck) PROTO ((VOID));   /*For Firewall Module*/
    INT4 (*pIssCustShowNvram) PROTO ((INT4 CliHandle));

    /* For Secure storing/restoring of crypto keys and certificates */
    INT4 (*pIssCustSecureProcess) PROTO((UINT4, UINT4, ... ));
    INT4 (*pIssCustValidateConfResFile) PROTO((INT4));

#ifdef CLKIWF_WANTED 
    /* Call back for setting Real Time clock */ 
    INT4 (*pClkIwfSetCustClock) (INT4 i4TimeSource,  
           tUtlSysPreciseTime *pSysPreciseTime, tUtlTm *pUtlTm); 
#endif 
#ifdef FIPS_WANTED
    INT4 (*pIssCustDelSramContents) (VOID);
#endif /* FIPS_WANTED */
    INT4 (*pFmCallBack) (VOID *);   /* Function pointer for
                         callback function to register with FM module */
#ifdef WSS_WANTED
    INT4 (*pWssReadNvRam)(VOID);
#endif
#ifdef FSB_WANTED
    INT4 (*pFsbSetIfMtuCallBack) (UINT4);
#endif
}tFsCbInfo;

VOID IssCustSnmpCallBkRegister PROTO ((VOID));

VOID SNMPUpdateEOIDCB PROTO ((tSNMP_OID_TYPE *, tSNMP_OID_TYPE *,UINT1));

VOID SNMPRevertEOIDCB PROTO ((tSNMP_OID_TYPE *, tSNMP_OID_TYPE *));

VOID SNMPUpdateGetNextEOIDCB PROTO ((tSNMP_OID_TYPE *, tMbDbEntry **, UINT4 *, tSnmpDbEntry *));


#ifdef ISS_WANTED
INT1 IssCustGetConfigPrompt PROTO ((INT1 *, INT1 *));
VOID TgtCustomStartup       PROTO ((INT1 *));
INT4 IssCustRegisterEvtCallBk PROTO ((UINT4 u4ModuleId, UINT4 u4Event,
                                 tFsCbInfo *pFsCbInfo));

VOID IssCustInit PROTO ((int argc, char *argv[]));

INT4 IssCustMsrInit PROTO ((VOID));

VOID IssGetSysCapabilitiesSupported PROTO ((UINT1 *pu1SysCapSupported));
VOID IssGetSysCapabilitiesEnabled PROTO ((UINT1 *pu1SysCapEnabled));
const char *IssGetFirmwareVersion PROTO((VOID));
const char *IssGetSwitchName PROTO((VOID));
UINT1  *IssGetSaveFileName PROTO((VOID));
UINT1  *IssGetOspfConfSaveFileName PROTO((VOID));
UINT1  *IssGetCsrConfSaveFileName PROTO((VOID));
UINT1  *IssGetBgpConfSaveFileName PROTO((VOID));

const char *IssGetDlImageName PROTO((VOID));
const char *IssGetImageName PROTO((VOID));
const char *IssGetUlLogFileName PROTO((VOID));
const char *IssGetSysLocation PROTO((VOID));
const char *IssGetSysContact PROTO((VOID));
INT4 IssCustConfControlWebnm (VOID *);
INT4 IssCustGetSlotMACAddress PROTO ((UINT4 u4SwitchId, UINT1 *pu1SwitchHwAddr));

VOID IssCustRegisterNvramCallBk (VOID);
INT4 IssCustNvramWrite (VOID);
INT4 IssCustNvramRead (VOID);
#endif

INT4
IssCustDetermineSpeed PROTO ((UINT4 u4IfIndex, UINT4 *pu4IfSpeed, UINT4 *pu4IfHighSpeed));
INT4
IssCustGetEthernetType PROTO ((UINT4 u4IfIndex));

typedef struct _IssCustSnmpConfInfo {
    VOID* pMidLevelFuncPtr;
    VOID* pSnmpMultiDataVal;
    VOID* pSnmpIndex;
    VOID* pSnmpOId;
} tIssCustSnmpConfInfo;
INT4 IssCustConfControlSnmp(tIssCustSnmpConfInfo *pIssCustSnmpConfInfo);
INT4 IssCustConfControlSnmpGet(tIssCustSnmpConfInfo *pIssCustSnmpConfInfo);

/* Used to indicate the system ID of the partner*/
typedef struct _tLaSystemId 
{
   tMacAddr        SystemMacAddr;
   UINT2           u2SystemPriority;
}tLaSystemId;

typedef struct _IssCustCliConfInfo {
          INT1*  pi1CliCmd;
        CONST   INT1*  pi1ConfMode;
          INT4     i4PromptInfo;
} tIssCustCliConfInfo;
INT4 IssCustConfControlCli(tIssCustCliConfInfo *pIssCustCliConfInfo);

/*used to store the Interface Index,port channel Index and the System ID*/
typedef struct _tLaParamInfo
{
    tLaSystemId             SystemId;
    UINT2                   u2IfIndex;
    UINT2                   u2IfKey;
}tLaParamInfo;

typedef struct _tLaNotifyProtoToApp
{
    tLaParamInfo LaInfo;    /*Contains the port index and port channel Index
                              when u1Action is LA_NOTIFY_PARAMETER_CHANGE 
                              this structure contains the new parameters*/

    UINT1     u1Action;   /*LA_NOTIFY_LINK_STATUS - To notify the change in 
                            oper status of a port-channel.
    
                            LA_NOTIFY_PORT_IN_BUNDLE_OPER_STATUS - To notify 
                            the change in oper status of the port in bundle. 
                            
                            LA_NOTIFY_DEAFULTED - To notify the port getting 
                            into defaulted state.

                            LA_NOTIFY_PARAMETER_CHANGE - To notify change in 
                            any of the parmeters in the LACP PDU
                            */ 
    UINT1     u1OperStatus; /*when u1Action is LA_NOTIFY_LINK_STATUS ,it gives 
                             the oper status of the port channel and when 
                             u1Action is LA_NOTIFY_PORT_IN_BUNDLE_OPER_STATUS,
                             then it contains the status of the port in the 
                             port channel.When action is LA_NOTIFY_DEAFULTED or                              LA_NOTIFY_PARAMETER_CHANGE , then it is unused*/
    UINT1    u1Reserved[2];
}tLaNotifyProtoToApp;

typedef struct _tSTPNotifyProtoToApp
{
    UINT4    u4IfIndex;
    UINT1    u1AdminStatus; 
    UINT1    u1Reserved[3];
}tSTPNotifyProtoToApp;

typedef union _NotifyProtoToApp
{
    tLaNotifyProtoToApp               LaNotify;
    tSTPNotifyProtoToApp       STPNotify;
}tNotifyProtoToApp;


VOID CfaNotifyProtoToApp (UINT1 u1ModeId, tNotifyProtoToApp NotifyProtoToApp);

INT4 IssCustGetCurrTemperature (INT4 *);
INT4 IssCustGetCurrPowerSupply (UINT4 *);
INT4 IssCustGetFanStatus (UINT4, UINT4 *);
INT4 IssCustValidateAction PROTO((UINT1 *,tSnmpIndex *,tSNMP_MULTI_DATA_TYPE *));

#ifdef WEBNM_WANTED
INT4 IssCustLoadHelpTag (INT4 *, INT1 *, INT1 *);
#endif

/* For Log Related Custom Features */
VOID IssCustCheckLogOption (UINT4 *, UINT4 *);

/* For FIPS related custom features */

#define ISS_FIPS_OPER_MODE      0x01
#define ISS_FIPS_SELF_TEST      0x02

INT4 IssCustSetFipsCurrOperMode PROTO((INT4 i4OperMode));

INT4 IssCustGetFipsCurrOperMode PROTO((INT4 *pi4OperMode));

INT4 IssCustCheckSwitchToFipsMode PROTO((VOID));

INT4 IssCustFipsErrHandler PROTO ((UINT4 u4Event, UINT4 u4Value));

VOID IssCustSystemRestart (VOID);

VOID IssCustConfigRestore PROTO ((VOID));

INT4 IssCustClearConfig PROTO ((UINT1 u1IsClrConfigCompleted,UINT2 u2OidType,  
                                UINT1 *pu1Oid,UINT1 *pu1Instance, 
                                tSNMP_MULTI_DATA_TYPE *pData)); 
      

/* For LED related custom feature */
INT4 IssCustLedSet PROTO ((CHR1 *pc1Result));

/* For SRAM/SECRAM storage of the data */
typedef enum {
    ISS_MIB_OBJECT = 1,
    ISS_FILE,
    ISS_CHAR_BUFFER

}eIssCryptoDataType;

typedef enum
{
    SRAM_READ = 1,
    SRAM_WRITE,
    SRAM_DELETE
}eSramCliCommandAction;

typedef enum {
    ISS_SECURE_MEM_INITIATE = 1,
    ISS_SECURE_MEM_SAVE,
    ISS_SECURE_MEM_RESTORE,
    ISS_SECURE_MEM_DELETE,
    ISS_SRAM_FILENAME_MAPPING

}eIssSecMemAccessOption;
#define ISS_DEFAULT_MODE 3 /* ISS_TRUE = 1 & ISS_FALSE = 2 */

INT4 IssCustLocalLoggingCheck PROTO ((UINT4 u4SeverityLevel));
void IntelCallBack(INT1 *pParam);
void IntlCallBack(INT1 *pParam);

#if defined ALTA_WANTED || defined RRC_WANTED
#define ADVER_INTERVAL_VRRP_V2(x)          (30 * gpVrrpOperTable[x].i4AdvtInterval)
#define ADVER_INTERVAL_VRRP_V3(x)          (30 * gpVrrpOperTable[x].i4RcvdMasterAdvtInterval)
#else
#define ADVER_INTERVAL_VRRP_V2(x)          (3 * gpVrrpOperTable[x].i4AdvtInterval)
#define ADVER_INTERVAL_VRRP_V3(x)          (3 * gpVrrpOperTable[x].i4RcvdMasterAdvtInterval)
#endif 
#endif /* __CUST_H_ */
