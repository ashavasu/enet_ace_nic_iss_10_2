/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: maunp.h,v 1.11 2016/09/17 12:43:08 siva Exp $
 * Description: Exported file for MAU MIB NP-API.
 *
 *******************************************************************/
#ifndef __MAUNP_H
#define __MAUNP_H
#include "npapi.h"

#define MAU_DEF_INDEX                1

#define MAU_STATUS_OTHER             1
#define MAU_STATUS_UNKNOWN           2
#define MAU_STATUS_OPERATIONAL       3
#define MAU_STATUS_STANDBY           4
#define MAU_STATUS_SHUTDOWN          5
#define MAU_STATUS_RESET             6

#define MAU_MEDIA_OTHER              1
#define MAU_MEDIA_UNKNOWN            2
#define MAU_MEDIA_AVAILABLE          3
#define MAU_MEDIA_NOT_AVAILABLE      4
#define MAU_MEDIA_REMOTE_FAULT       5
#define MAU_MEDIA_INVALID_SIGNAL     6
#define MAU_MEDIA_REMOTE_JABBER      7
#define MAU_MEDIA_REMOTE_LINK_LOSS   8
#define MAU_MEDIA_REMOTE_TEST        9
#define MAU_MEDIA_OFFLINE           10
#define MAU_MEDIA_AUTONEG_ERROR     11

#define MAU_JABBER_OTHER             1
#define MAU_JABBER_UNKNOWN           2
#define MAU_JABBER_NO_JABBER         3
#define MAU_JABBER_JABBERING         4

#define MAU_TYPE_OTHER               0
#define MAU_TYPE_AUI                 1
#define MAU_TYPE_10_BASE_5           2          
#define MAU_TYPE_FOIRL               3
#define MAU_TYPE_10_BASE_2           4
#define MAU_TYPE_10_BASE_T           5
#define MAU_TYPE_10_BASE_FP          6
#define MAU_TYPE_10_BASE_FB          7 
#define MAU_TYPE_10_BASE_FL          8
#define MAU_TYPE_10_BROAD_36         9
#define MAU_TYPE_10_BASE_THD        10
#define MAU_TYPE_10_BASE_TFD        11
#define MAU_TYPE_10_BASE_FLHD       12
#define MAU_TYPE_10_BASE_FLFD       13
#define MAU_TYPE_100_BASE_T4        14
#define MAU_TYPE_100_BASE_TXHD      15
#define MAU_TYPE_100_BASE_TXFD      16
#define MAU_TYPE_100_BASE_FXHD      17
#define MAU_TYPE_100_BASE_FXFD      18
#define MAU_TYPE_100_BASE_T2HD      19
#define MAU_TYPE_100_BASE_T2FD      20
#define MAU_TYPE_1000_BASE_XHD      21
#define MAU_TYPE_1000_BASE_XFD      22
#define MAU_TYPE_1000_BASE_LXHD     23
#define MAU_TYPE_1000_BASE_LXFD     24
#define MAU_TYPE_1000_BASE_SXHD     25
#define MAU_TYPE_1000_BASE_SXFD     26
#define MAU_TYPE_1000_BASE_CXHD     27
#define MAU_TYPE_1000_BASE_CXFD     28
#define MAU_TYPE_1000_BASE_THD      29
#define MAU_TYPE_1000_BASE_TFD      30
#define MAU_TYPE_10GIG_BASE_X       31
#define MAU_TYPE_10GIG_BASE_LX4     32
#define MAU_TYPE_10GIG_BASE_R       33
#define MAU_TYPE_10GIG_BASE_ER      34
#define MAU_TYPE_10GIG_BASE_LR      35
#define MAU_TYPE_10GIG_BASE_SR      36
#define MAU_TYPE_10GIG_BASE_W       37
#define MAU_TYPE_10GIG_BASE_EW      38
#define MAU_TYPE_10GIG_BASE_LW      39
#define MAU_TYPE_10GIG_BASE_SW      40
#define MAU_TYPE_10GIG_BASE_CX4     41
#define MAU_TYPE_2_BASE_TL          42
#define MAU_TYPE_10_PASS_TS         43
#define MAU_TYPE_100_BASE_BX10D     44
#define MAU_TYPE_100_BASE_BX10U     45
#define MAU_TYPE_100_BASE_LX10      46
#define MAU_TYPE_1000_BASE_BX10D    47
#define MAU_TYPE_1000_BASE_BX10U    48
#define MAU_TYPE_1000_BASE_LX10     49
#define MAU_TYPE_1000_BASE_PX10D    50
#define MAU_TYPE_1000_BASE_PX10U    51
#define MAU_TYPE_1000_BASE_PX20D    52
#define MAU_TYPE_1000_BASE_PX20U    53
#define MAU_TYPE_40GIG_BASE_X       54
#define MAU_TYPE_40GIG_BASE_X       54
#define MAU_TYPE_56GIG_BASE_X       57
#define MAU_TYPE_25GIG_BASE_X       58
#define MAU_TYPE_100GIG_BASE_X      59
#define MAU_TYPE_2500_BASE_THD      55
#define MAU_TYPE_2500_BASE_TFD      56
#define MAU_TYPE_LIST_LEN           7
#define MAU_OCTET_LEN               8
#define MAU_BYTE_LEN                32
#define MAU_BYTE_LEN_EXT            64

#define MAU_JACK_OTHER               1
#define MAU_JACK_RJ45                2
#define MAU_JACK_RJ25S               3
#define MAU_JACK_DB9                 4
#define MAU_JACK_BNC                 5
#define MAU_JACK_FAUI                6
#define MAU_JACK_MAUI                7
#define MAU_JACK_FIBER_SC            8
#define MAU_JACK_FIBER_MIC           9
#define MAU_JACK_FIBER_ST           10
#define MAU_JACK_TELCO              11
#define MAU_JACK_MTRJ               12
#define MAU_JACK_HSSDC              13

#define MAU_AUTO_SIG_DETECTED        1  
#define MAU_NO_AUTO_SIG_DETECTED     2

#define MAU_AUTO_OTHER               1
#define MAU_AUTO_CONFIGURING         2
#define MAU_AUTO_COMPLETE            3
#define MAU_AUTO_DISABLED            4
#define MAU_AUTO_PLL_DETECT_FAIL     5

#define MAU_AUTO_RESTART             1
#define MAU_AUTO_NO_RESTART          2


#define MAU_AUTO_NO_ERROR            1
#define MAU_AUTO_OFFLINE             2 
#define MAU_AUTO_LINK_FAILURE        3
#define MAU_AUTO_ERROR               4

/* IANAifMauAutoNegCapBits  */
#define MAU_AUTO_NEG_BIT_OTHER           0x1       /* bit position 0 */
#define MAU_AUTO_NEG_BIT_10_BASE_T       0x2       /* bit position 1 */
#define MAU_AUTO_NEG_BIT_10_BASE_TFD     0x4       /* bit position 2 */
#define MAU_AUTO_NEG_BIT_100_BASE_T4     0x8       /* bit position 3 */
#define MAU_AUTO_NEG_BIT_100_BASE_TX     0x10      /* bit position 4 */
#define MAU_AUTO_NEG_BIT_100_BASE_TXFD   0x20      /* bit position 5 */
#define MAU_AUTO_NEG_BIT_100_BASE_T2     0x40      /* bit position 6 */
#define MAU_AUTO_NEG_BIT_100_BASE_T2FD   0x80      /* bit position 7 */
#define MAU_AUTO_NEG_BIT_FDX_PAUSE       0x100     /* bit position 8 */
#define MAU_AUTO_NEG_BIT_FDX_A_PAUSE     0x200     /* bit position 9 */
#define MAU_AUTO_NEG_BIT_FDX_S_PAUSE     0x400     /* bit position 10 */
#define MAU_AUTO_NEG_BIT_FDX_B_PAUSE     0x800     /* bit position 11 */
#define MAU_AUTO_NEG_BIT_1000_BASE_X     0x1000    /* bit position 12 */
#define MAU_AUTO_NEG_BIT_1000_BASE_XFD   0x2000    /* bit position 13 */
#define MAU_AUTO_NEG_BIT_1000_BASE_T     0x4000    /* bit position 14 */
#define MAU_AUTO_NEG_BIT_1000_BASE_TFD   0x8000    /* bit position 15 */

INT4 FsMauHwGetMauType (UINT4, INT4, UINT4 *);
INT4 FsMauHwSetMauStatus (UINT4, INT4, INT4 *);
INT4 FsMauHwGetStatus (UINT4, INT4, INT4 *);
INT4 FsMauHwGetMediaAvailable (UINT4, INT4, INT4 *);
INT4 FsMauHwGetMediaAvailStateExits (UINT4, INT4, UINT4 *);
INT4 FsMauHwGetJabberState (UINT4, INT4, INT4 *);
INT4 FsMauHwGetJabberingStateEnters (UINT4, INT4, UINT4 *);
INT4 FsMauHwGetFalseCarriers (UINT4, INT4, UINT4 *);
INT4 FsMauHwSetDefaultType (UINT4, INT4, UINT4);
INT4 FsMauHwGetDefaultType (UINT4, INT4, UINT4 *);
INT4 FsMauHwGetAutoNegSupported (UINT4, INT4, INT4 *);
INT4 FsMauHwGetTypeListBits (UINT4, INT4, INT4 *);
INT4 FsMauHwGetJackType (UINT4, INT4, INT4, INT4 *);
INT4 FsMauHwSetAutoNegAdminStatus (UINT4, INT4, INT4 *);
INT4 FsMauHwGetAutoNegAdminStatus (UINT4, INT4, INT4 *);
INT4 FsMauHwGetAutoNegRemoteSignaling (UINT4, INT4, INT4 *);
INT4 FsMauHwGetAutoNegAdminConfig (UINT4, INT4, INT4 *);
INT4 FsMauHwSetAutoNegRestart (UINT4, INT4, INT4 *);
INT4 FsMauHwGetAutoNegRestart (UINT4, INT4, INT4 *);
INT4 FsMauHwGetAutoNegCapBits (UINT4, INT4, INT4 *);
INT4 FsMauHwSetAutoNegCapAdvtBits (UINT4, INT4, INT4 *);
INT4 FsMauHwGetAutoNegCapAdvtBits (UINT4, INT4, INT4 *);
INT4 FsMauHwGetAutoNegCapRcvdBits (UINT4, INT4, INT4 *);
INT4 FsMauHwSetAutoNegRemFltAdvt (UINT4, INT4, INT4 *);
INT4 FsMauHwGetAutoNegRemFltAdvt (UINT4, INT4, INT4 *);
INT4 FsMauHwGetAutoNegRemFltRcvd (UINT4, INT4, INT4 *);

#endif   /* __MAUNP_H */
