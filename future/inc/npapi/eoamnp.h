/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: eoamnp.h,v 1.7 2007/02/01 14:52:34 iss Exp $
 *
 * Description: Exported file for EOAM NP-API.
 *
 *******************************************************************/
#ifndef __EOAMNP_H
#define __EOAMNP_H

#define  EOAM_NP_LB_INITIATE       1
#define  EOAM_NP_LB_ENABLE         2
#define  EOAM_NP_LB_DISABLE        3

INT4 EoamNpInit PROTO ((VOID));
INT4 EoamNpDeInit PROTO ((VOID));
INT4 EoamNpGetUniDirectionalCapability PROTO ((UINT4, UINT1 *));

INT4 EoamNpHandleRemoteLoopBack PROTO ((UINT4, tMacAddr, tMacAddr, UINT1));

INT4 EoamNpHandleLocalLoopBack PROTO ((UINT4, tMacAddr, tMacAddr, UINT1));

INT4 EoamLmNpRegisterLinkMonitor PROTO ((VOID));

INT4 EoamLmNpConfigureParams PROTO ((UINT4, UINT2, FS_UINT8, FS_UINT8)); 

INT4 EoamLmNpGetStat PROTO ((UINT4, UINT1, UINT4 *));

INT4 EoamLmNpGetStat64 PROTO ((UINT4, UINT1, FS_UINT8 *));

#ifdef MBSM_WANTED
INT4 EoamMbsmHwInit PROTO ((tMbsmSlotInfo *));
#endif /* MBSM_WANTED */
#endif /*__EOAMNP_H*/
