/**************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: esatnp.h,v 1.1 2014/08/14 12:52:04 siva Exp $ 
 *
 * Description: All prototypes for Network Processor API functions for ESAT 
 *
 **************************************************************************/
#ifndef _ESATNP_H
#define _ESATNP_H

#include"esat.h"
#include"fsvlan.h"

#define ESAT_HW_PAYLOAD_SIZE    16
#define ESAT_NP_SLA_CIR_START   1
#define ESAT_NP_SLA_CIR STOP    2

#define ESAT_NP_EVC_LB_ENABLE   1
#define ESAT_NP_EVC_LB_DISABLE  2
#define ESAT_ENET_ADDR_LEN      6

#define ESAT_NP_TX_PROCESS      1
#define ESAT_NP_RX_PROCESS      2
#define ESAT_NP_RX_START        3

#define ESAT_NP_TX_EVENT        0x10
#define ESAT_NP_RX_PKT_EVENT    0x20
#define ESAT_NP_RX_TMR_EVENT    0x30
#define ESAT_NP_RX_START_EVENT  0x40

#define ESAT_NP_MAX_SERVICE     1
#define ESAT_NP_MAX_EVC_LB      1

#define   ESAT_NP_TX_START      1
#define   ESAT_NP_TX_STOP       2

typedef struct _EsatHwCirStats
{
    FS_UINT8        u8TxBytes; /* Total Bytes Transmitted */
    FS_UINT8        u8RxBytes; /* Received Total Bytes */
    UINT4           u4TxPkts; /* Transmitted packet count */
    UINT4           u4RxPkts; /* Recieved packet count */
} tEsatHwCirStats;

typedef struct {
    UINT1    au1DstAddr[ESAT_ENET_ADDR_LEN];
    UINT1    au1SrcAddr[ESAT_ENET_ADDR_LEN];
    UINT2    u2OuterVlanId;
    UINT2    u2InnerVlanId;
    UINT1    u1OutPriority;
    UINT1    u1InrPriority;
    INT4     i4EsatEvent;
} tEsatPkt;

typedef struct {
    UINT4    u4IfIndex;  /* Interface Index */
    UINT4    u4ContextId; /* Context Id*/
    UINT4    u4SlaIndex; /* SLA Index Value */
    UINT4    u4TxInterval; /* Inter packet gap timei Value */
    UINT1    au1DstAddr[ESAT_ENET_ADDR_LEN]; /* Source MAC Address */
    UINT1    au1SrcAddr[ESAT_ENET_ADDR_LEN]; /* Destination MAC address */
    UINT2    u2OuterVlanId; /* Outer Vlan Id */
    UINT2    u2InnerVlanId;  /* Inner Vlan Id */
    UINT1    u1OutPriority; /* Outer Priority Value */
    UINT1    u1InrPriority; /* Inner Priority Value */
    INT4     i4PktSize; /* Configurred packet size to be sent in Tx */
    INT4     i4PktPrcsTime; /* Packet processing time */
    INT4     i4EsatEvent; /* Esat Tx On or Off */
    INT4     i4EvcId; /* EVC ID */
} tSlaParams;

typedef struct _EsatHwSlaInfo
{
    tMacAddr        TrafProfSrcMac; /* Source MAC address */
    UINT2           u2TrafProfInVlan; /* Inner Vlan Id */
    tMacAddr        TrafProfDestMac; /* Destination MAC address */
    UINT2           u2TrafProfOutVlan; /* Outer Vlan Id */
    UINT4           u4SlaId; /* Sla Identifier */
    UINT4           u4IfIndex; /* Port Identifier */
    UINT4           u4ContextId; /* Context Identifier */
    UINT4           u4SlaDuration; /* Duration for which packet to be transmitted */
    UINT4           u4SlaFreqDelay; /* Inter packet delay */
    UINT4           u4CIR; /*CIR for EVC Index */
    UINT4           u4CBS; /*CBS for EVC Index */
    UINT4           u4TrafProfTagType; /* VLAN Tag type */
    UINT2           u2TrafProfPktSize; /* Packet Size */
    UINT2           u2StepId; /* Step Identifier */
    UINT2           u2EvcId; /* EVC Index */
    UINT1           u1TrafProfInCos; /* Inner Class of service */
    UINT1           u1TrafProfOutCos; /* Inner Class of service */
    UINT1           au1TrafProfPayload[ESAT_HW_PAYLOAD_SIZE + 1];
                                    /* Payload information */
    UINT1           au1Pad[3];
} tEsatHwSlaInfo;

typedef struct _EsatHwInfo
{
    tEsatHwSlaInfo    EsatHwSlaInfo; /* Information about SLA */
    tEsatHwCirStats   EsatHwCirStats; /* CIR test report */
    UINT4             u4InfoType; /* Information type to be set or queried from H/w */
} tEsatHwInfo;

enum {
    ESAT_HW_START_SLA_TEST = 1,
    ESAT_HW_STOP_SLA_TEST,
    ESAT_HW_FETCH_SLA_REPORT
};

INT4 EsatHwGetEvcStats (UINT2 u4EvcId);

INT4 EsatStartTx (tEsatHwSlaInfo EsatHwSlaInfo);

INT4 EsatStopTx (UINT2 u2EvcId);

INT4 FsNpHwConfigEsat (tEsatHwInfo *pEsatHwInfo);

#endif /*_ESATNP_H*/
