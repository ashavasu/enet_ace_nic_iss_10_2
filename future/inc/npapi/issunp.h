/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: issunp.h,v 1.4 2016/08/09 09:41:27 siva Exp $
 * Description: Contains definitions, prototypes and other NP related
 *              header extensions for ISSU module.
 ********************************************************************/
#ifndef _ISSUNP_H
#define _ISSUNP_H

#define ISSU_COLDBOOT_FLAG              "0x000000"
#define ISSU_WARMBOOT_FLAG              "0x200000"
#define ISSU_PATH_MAX_SIZE    128
#define ISSU_INIT_PROCEDURE             1
#define ISSU_LOAD_SOFTWARE              2
#define ISSU_EXIT_PROCEDURE             3
#define ISSU_RESTORE_NP_SHADOW_TABLE    4

#define ISSU_MAX_BUFFER_SIZE            1500
#define ISS_CACHE_FILE                  FLASH "isscache.txt"

enum
{
    ISS_NPC_IGMP_PKT_ENTRY = 1,                 /* gIgmpPktFPEntry */
    ISS_NPC_RAPID_AGING_NUM_PORTS,              /* gi4NpRapidAgingNumPorts */
    ISS_NPC_VLAN_FP_ENTRY,                      /* gaVlanFPEntry */
    ISS_NPC_VLAN_HW_STATS,                      /* gaVlanHwStats */
    ISS_NPC_CFA_NP_SBP_IF_MAP,                  /* gaCfaNpSbpIfMap */
    ISS_NPC_CFA_NP_SBP_HW_IF_MAP,               /* gaCfaNpSbpHwIfMap */
    ISS_NPC_VLAN_MCAST_GROUP,                   /* gaVlanMcastGroup */
    ISS_NPC_CFA_NP_RX_PROTO_FILTER,             /* FilterNode */
    ISS_NPC_VLAN_HW_MCAST_INFO,                 /* tVlanHwMcastInfo */
    ISS_NPC_MAX_INDEX                  
};

typedef struct _HwIssuStatusInfo {
    UINT4        u4IssuMode;
    UINT4        u4Opcode; /* Based on this value 
                     * ISSU Init/LoadSoftware/Exit
              * NP call procedure will triger */
    UINT1        au1IssuLoadSWPath[ISSU_PATH_MAX_SIZE+1];
    UINT1        au1Pad[3];
}tIssuHwStatusInfo;

INT4
FsNpIssuLoadVersionUpgrade (tIssuHwStatusInfo * pIssuHwStatusInfo);
INT4
FsNpIssuStoreNpShadowTable PROTO ((VOID));
INT4
FsIssuSetFilterData PROTO ((INT4, UINT4 ));
INT4
FsNpIssuRestoreNpShadowTable PROTO ((VOID));
INT4
FsIssuGetFilterData PROTO ((CHR1 *, UINT4 ));
INT4
FsIssuWriteToFile PROTO ((INT4 ,UINT4 ,UINT1 *));

#endif /* _ISSUNP_H */
