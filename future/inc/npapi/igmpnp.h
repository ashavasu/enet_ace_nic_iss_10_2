/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igmpnp.h,v 1.4 2014/12/10 12:55:37 siva Exp $
 *
 * Description: Prototypes for IGMP Snooping module's NP-API.
 *
 *******************************************************************/
#ifndef _IGMPNP_H
#define _IGMPNP_H

INT4 FsIgmpHwEnableIgmp PROTO ((VOID));
INT4 FsIgmpHwDisableIgmp PROTO ((VOID));
INT4 FsNpIpv4SetIgmpIfaceJoinRate PROTO ((INT4,INT4));

#ifdef MBSM_WANTED
INT4
FsIgmpMbsmHwEnableIgmp (tMbsmSlotInfo * pSlotInfo);
#endif

#endif

