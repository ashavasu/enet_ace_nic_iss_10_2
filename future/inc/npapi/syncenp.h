/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * $Id: syncenp.h,v 1.2 2013/03/23 14:31:57 siva Exp $
 * Description: Contains definitions, prototypes and other NP related
 *              header extensions for SyncE module.
 ********************************************************************/

#ifndef __SYNCE_NP_H_
#define __SYNCE_NP_H_

/* HW related enums */
enum
{
    SYNCE_HW_SET_SYNCE_STATUS,
    SYNCE_HW_SET_ESMC_MODE,
    SYNCE_HW_SET_HW_CLK
};

typedef struct _SynceHwSynceStatusInfo {
    UINT4             u4ContextId;
    UINT4             u4IfIndex;
    UINT4             u4SynceStatus;
}tSynceHwSynceStatusInfo;

typedef struct _SynceHwSynceEsmcMode {
    UINT4             u4ContextId;
    UINT4             u4IfIndex;
    UINT4             u4EsmcMode;
}tSynceHwSynceEsmcMode;

typedef struct _SynceHwSynceConfigHwClk {
    UINT4             u4ContextId;
    UINT4             u4IfIndex;
}tSynceHwSynceConfigHwClk;

/* Data structure to be passed as argument to the H/w routines */
typedef struct _SynceHwInfo {
    union
    {
        tSynceHwSynceStatusInfo SynceHwSynceStatusInfo;
        tSynceHwSynceEsmcMode SynceHwSynceEsmcMode;
        tSynceHwSynceConfigHwClk SynceHwSynceConfigHwClk;
    }unHwParam;
    UINT4     u4InfoType; /* Information type to be set or queried from H/w */
}tSynceHwInfo;

/* This is an unified NPAPI function which includes functionality of
 * all NPAPIs provided by this module */
PUBLIC INT4
FsNpHwConfigSynceInfo PROTO ((tSynceHwInfo * pSynceHwSynceInfo));

#endif /* SYNCENP_H_ */
