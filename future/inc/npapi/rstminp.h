/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rstminp.h,v 1.10 2010/11/18 13:32:21 siva Exp $ 
 *
 * Description: All prototypes for Network Processor API functions for RSTP 
 * 
 *
 *******************************************************************/
#ifndef _RSTMINP_H
#define _RSTMINP_H

INT1 FsMiStpNpHwInit (UINT4 u4ContextId);
VOID FsMiRstpNpDeInitHw (UINT4 u4ContextId);


INT1 FsMiRstpNpSetPortState (UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1Status);
INT1 FsMiRstNpGetPortState (UINT4 u4ContextId, UINT4 u4IfIndex, UINT1* pu1Status);
VOID FsMiRstpNpInitHw PROTO ((UINT4 u4ContextId ));

#ifdef MBSM_WANTED
INT1 FsMiRstpMbsmNpSetPortState (UINT4,UINT4 , UINT1 , tMbsmSlotInfo *);

INT1 FsMiRstpMbsmNpInitHw (UINT4 , tMbsmSlotInfo * );
#endif

#endif /* _RSTMINP_H */
