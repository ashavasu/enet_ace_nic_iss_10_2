
/********************************************************************
 * Copyright (C) Future Software Limited, 2001
 *
 * Description: This file contains the function implementations  of FS NP-API.
 *
 * $Id: radionp.h,v 1.32 2017/11/30 14:01:49 siva Exp $
 *******************************************************************/
#ifndef _RADIONP_H
#define _RADIONP_H
#include "npapi.h"
#include "lr.h"
#include "osxstd.h"
#include "srmbuf.h"
#include "osxsys.h"
#include "osxprot.h"
#include "srmmem.h"
#include "utlsll.h"
#include "utlslldyn.h"
#include "utldll.h"
#include "srmtmr.h"
#include "utlhash.h"
#include "utltrc.h"
#include "utleeh.h"
#include "utlmacro.h"
#include "utlhdr.h"
#include "redblack.h"

#include "pack.h"
#include "size.h"
#include "trace.h"
#include "utlbit.h"
/* #include "npradio.h"*/

#include "fsutlsz.h"
#include "radioifhwnpwr.h" 


INT4 FsRadioHwInit PROTO ((VOID));
INT4 FsRadioHwDeInit PROTO ((INT4   *));
INT4
FsRadioHwUpdateAdminStatusEnable PROTO ((tRadioInfo ));
INT4
FsRadioHwUpdateAdminStatusDisable PROTO ((tRadioInfo ));
INT4
FsRadioHwSetAntennaMode PROTO ((tRadioInfo  , UINT1 ));
INT4 FsRadioHwGetAntennaMode PROTO ((tRadioInfo  , UINT1 *));
INT4
FsRadioHwGetCurrTxAntenna PROTO ((tRadioInfo  , UINT1 * ));
INT4
FsRadioHwSetAntennaSelection PROTO ((tRadioInfo   , UINT1 u1AntennaIdx, UINT1  u1AntennaSel));
INT4
FsRadioHwGetAntennaSelection PROTO ((tRadioInfo   , UINT1 u1AntennaIdx, UINT1 * pu1AntennaSel));
INT4
FsRadioHwSetAntennaDiversity PROTO  ((tRadioInfo  RadioInfo, UINT4 *u4RcvDivsity));
INT4
FsRadioHwGetAntennaDiversity PROTO ((tRadioInfo  RadioInfo, UINT4 *pu4RcvDivsity));
INT4
FsRadioHwSetDot11RTSThreshold PROTO ((tRadioInfo   , UINT2 u2RTSThshold));
INT4
FsRadioHwGetDot11RTSThreshold PROTO ((tRadioInfo   , UINT2 * pu2RTSThshold));
INT4
FsRadioHwSetDot11FragThreshold PROTO ((tRadioInfo   , UINT2 u2FragThshold));
INT4
FsRadioHwGetDot11FragThreshold PROTO ((tRadioInfo   , UINT2 * pu2FragThshold));
INT4
FsRadioHwGetDot11ShortRetryLimit PROTO ((tRadioInfo   , UINT2 *pu1ShortRetryLimit));
INT4
FsRadioHwSetLongRetryLimit PROTO ((tRadioInfo   , UINT2   u1LongRtryLmt));
INT4
FsRadioHwGetLongRetryLimit PROTO ((tRadioInfo   , UINT1   *u1LongRtryLmt));
INT4
FsRadioHwSetTxCurrPower PROTO ((tRadioInfo   , INT2  i2TxCurrPwrLevel));
INT4
FsRadioHwGetTxCurrPower PROTO ((tRadioInfo   , INT4 *));
INT4
FsRadioHwSetOFDMChannelWidth PROTO ((tRadioInfo   , UINT4 *));
INT4
FsRadioHwGetOFDMChannelWidth PROTO ((tRadioInfo   , UINT4 *));
INT4
FsRadioHwSetBeaconInterval PROTO ((tRadioInfo   , UINT4 *));
INT4
FsRadioHwGetBeaconInterval PROTO ((tRadioInfo   , UINT4 *));
INT4
FsRadioHwSetDot11EDCATableParams (tRadioInfo ,
                                 INT4 *pi4Dot11EDCATbleIndex,
                                 INT4 *pi4ValDot11EDCATbleCWmin,
                                 INT4 *pi4ValDot11EDCATbleCWmax,
                                 INT4 *i4ValDot11EDCATbleAifs,
                                 INT4 *i4ValDot11EDCATbleTxOpLmt,
                                 INT4 *i4ValDot11EDCATbleAdmsnCtrl);

INT4
FsRadioHwSetDot11EDCATableCWmin PROTO ((tRadioInfo   , INT4 * pi4Dot11EDCATbleIndex, 
                                         INT4  *i4ValDot11EDCATbleCWmin));
INT4
FsRadioHwGetDot11EDCATableCWmin PROTO ((tRadioInfo   , INT4 * pi4Dot11EDCATbleIndex, 
                                         INT4  *i4ValDot11EDCATbleCWmin));
INT4
FsRadioHwSetDot11EDCATableCWmax PROTO ((tRadioInfo   , INT4 * pi4Dot11EDCATbleIndex,
                                         INT4  *pi4ValDot11EDCATbleCWmax));
INT4
FsRadioHwGetDot11EDCATableCWmax PROTO ((tRadioInfo   , INT4 * pi4Dot11EDCATbleIndex, 
                                         INT4 * pi4ValDot11EDCATbleCWmax));
INT4
FsRadioHwSetDot11Aifs PROTO ((tRadioInfo   , INT4 * pi4Dot11EDCATbleIndex, 
                                         INT4  * i4ValDot11EDCATbleAifs));
INT4
FsRadioHwGetDot11Aifs PROTO ((tRadioInfo   , INT4 * pi4Dot11EDCATbleIndex, 
                                         INT4 *  pi4ValDot11EDCATbleAifs ));
INT4
FsRadioHwSetDot11EDCATableTxOpLmt PROTO ((tRadioInfo   , INT4 * pi4Dot11EDCATbleIndex, 
                                         INT4 * i4ValDot11EDCATbleTxOpLmt));
INT4
FsRadioHwGetDot11EDCATableTxOpLmt PROTO ((tRadioInfo   , INT4 * pi4Dot11EDCATbleIndex, 
                                         INT4 * pi4ValDot11EDCATbleTxOpLmt));
INT4
FsRadioHwSetDot11EDCATableAdmsnCtrl PROTO ((tRadioInfo   , INT4 * pi4Dot11EDCATbleIndex, 
                                         INT4  * i4ValDot11EDCATbleAdmsnCtrl));
INT4
FsRadioHwGetDot11EDCATableAdmsnCtrl PROTO ((tRadioInfo   , INT4 * pi4Dot11EDCATbleIndex, 
                                         INT4 * pi4ValDot11EDCATbleAdmsnCtrl));
INT4
FsRadioHwSetDot11HideEssid PROTO ((tRadioInfo   , UINT1 *, 
                                         INT4  * ));
INT4
FsRadioHwGetDot11HideEssid PROTO ((tRadioInfo   , UINT1 *, 
                                         INT4 *));
INT4
FsRadioHwSetDot11DtimPrd PROTO ((tRadioInfo   , INT4 *));
INT4
FsRadioHwGetDot11DtimPrd PROTO ((tRadioInfo   , INT4  *));
INT4
FsRadioHwSetDot11StWpKeyEncyptn PROTO ((tRadioInfo   , INT2 i2ValDot11StaWpKyEnType, 
  UINT1 *pu1ValDot11StaWpKyEnVal, INT2 i2ValDot11StaWepKeyEnIdx, 
  INT2 i2ValDot11StWpKeyAuthIdx));
INT4
FsRadioHwGetDot11StWpKeyEncyptn PROTO ((tRadioInfo   ,INT2 * pi2ValDot11StaWpKyEnType,
  INT2 * pi2ValDot11StaWpKyEnVal, INT2 i2ValDot11StaWepKeyEnIdx, INT2 i2ValDot11StWpKeyAuthIdx));
#if 0
INT4
FsRadioHwSetDot11StWpAuthKeyType PROTO ((tRadioInfo   ,INT2 * pi2ValDot11StWpKyAuthType, 
  INT2 i2ValDot11StWpKyAuthStat, INT2 i2ValDot11StWpKeyAuthIdx));
INT4
FsRadioHwGetDot11StWpAuthKeyType (tRadioInfo  RadioInfo,INT4 *pi4ValDot11StWpKyAuthType,
        INT2 i2ValDot11StWpKyAuthStat, INT2 i2ValDot11StWpKeyAuthIdx );
#endif
INT4
FsRadioHwSetDot11StaWepKeyStatus PROTO ((tRadioInfo   , INT2 * pi2ValDot11StaWpKeyStat, 
  INT2 i2ValDot11StaWpKeyIndex));
INT4
FsRadioHwGetDot11StaWepKeyStatus PROTO ((tRadioInfo   ,INT2 * pi2ValDot11StaWpKeyStat, 
  INT2 i2ValDot11StaWpKeyIndex));
INT4
FsRadioHwSetDot11nAMPDUSuppStat PROTO ((tRadioInfo   ,INT2 *pi2ValDot11nAMPDUSupStat)); 
INT4
FsRadioHwGetDot11nAMPDUSuppStat PROTO ((tRadioInfo   , INT2 * pi2ValDot11nAMPDUSupStat)); 
INT4
FsRadioHwSetDot11SecExUnEnStat PROTO ((tRadioInfo   , UINT2 * )); 
INT4
FsRadioHwGetDot11SecExUnEnStat PROTO ((tRadioInfo   , UINT2 *)); 

INT4
FsRadioHwSetDot11Frequency PROTO ((tRadioInfo  , UINT1 ));
INT4
FsRadioHwGetDot11Frequency PROTO ((tRadioInfo  , UINT1 *));
INT4
FsRadioHwDot11DelMac PROTO ((tRadioInfo  , UINT1  *));
INT4
FsRadioHwDot11AddMac PROTO ((tRadioInfo , UINT1  *));
INT4
FsRadioHwSetDot11aSuppMode PROTO ((tRadioInfo  , UINT1  * )); 
INT4
FsRadioHwGetDot11aSuppMode PROTO ((tRadioInfo  , UINT1  * ));
INT4
FsRadioHwGetDot11bSuppMode PROTO ((tRadioInfo  , UINT1  * ));
INT4
FsRadioHwSetDot11bSuppMode PROTO ((tRadioInfo  , UINT1  * ));
INT4
FsRadioHwSetDot11anSuppMode PROTO ((tRadioInfo  , UINT1 *));
INT4
FsRadioHwGetDot11anSuppMode PROTO ((tRadioInfo  , UINT1 *));
INT4
FsRadioHwSetDot11gnSuppMode PROTO ((tRadioInfo  , UINT1 *));
INT4
FsRadioHwGetDot11gSuppMode PROTO ((tRadioInfo  , UINT1 *));
INT4
FsRadioHwSetDot11gSuppMode PROTO ((tRadioInfo  , UINT1 *));
INT4
FsRadioHwGetDot11gnSuppMode PROTO ((tRadioInfo  , UINT1 *));
INT4
FsRadioHwSetDot11acSuppMode PROTO ((tRadioInfo  , UINT1 *));
INT4
FsRadioHwGetDot11acSuppMode PROTO ((tRadioInfo  , UINT1 *));
INT4
FsRadioHwSetDot11StaFwd PROTO ((tRadioInfo  , UINT4  *));

INT4
FsRadioHwGetDot11StaFwd PROTO ((tRadioInfo  , UINT4  *));
INT4
FsRadioHwGetDot11MaxTxPow2G PROTO ((tRadioInfo  , UINT4 *));
INT4
FsRadioHwSetDot11MaxTxPow2G PROTO ((tRadioInfo  , UINT4 ));
INT4
FsRadioHwGetDot11MaxTxPow5G PROTO ((tRadioInfo , UINT4 *));
INT4
FsRadioHwSetDot11MaxTxPow5G PROTO ((tRadioInfo , UINT4 ));
INT4
FsRadioHwSetDot11HwAddr PROTO ((tRadioInfo , UINT1 *));
INT4
FsRadioHwGetDot11HwAddr PROTO ((tRadioInfo , UINT1 *));
INT4
FsRadioHwSetDot11AthHwAddr PROTO ((tRadioInfo  , UINT1 * )); 
INT4
FsRadioHwGetDot11AthHwAddr PROTO ((tRadioInfo  , UINT1 *)); 
INT4
FsRadioHwSetDot11BitRates PROTO ((tRadioInfo  , tRadioIfNpWrSetDot11BitRates *)); 
INT4
FsRadioHwGetDot11BitRates PROTO ((tRadioInfo  , UINT2 *, UINT4 *, UINT4 *)); 
INT4
FsRadioHwDot11StaAuthDeAuth PROTO ((tRadioInfo , UINT1 , UINT1 *)); 
INT4
FsRadioHwDot11MCSRateSet PROTO ((tRadioInfo , UINT1 *, UINT1 *));
INT4
FsRadioHwSetDot11NRatesStat PROTO ((tRadioInfo , UINT4 *)); 
INT4
FsRadioHwGetDot11NRatesStat PROTO ((tRadioInfo , UINT4 *)); 
#ifdef WPS_WTP_WANTED
INT4
FsRadioHwSetDot11WpsButtonPushed PROTO ((VOID));
INT4
FsRadioHwSetDot11WpsButtonPushedDisable PROTO ((VOID));
INT4
FsRadioHwSetDot11WpsIEParams PROTO ((tRadioIfNpWrSetWpsIEParams *));
INT4
FsRadioHwSetDot11WpsAdditionalParams PROTO ((tRadioIfNpWrSetWpsAdditionalParams *));
#endif
INT4
FsRadioHwSetDot11RsnaIEParams PROTO ((tRadioInfo  RadioInfo,
        UINT1 u1RsnaStatus, UINT2 u2PairwiseCipherSuiteCount,
        UINT2 u2AKMSuiteCount, UINT1 *pu1PairwiseCipher,
        UINT1 *pu1AKMSuite, UINT1 u1GroupCipher,
        UINT1 u1Privacy
#ifdef PMF_WANTED
        ,UINT1 u1GroupMgmtCipher,UINT2 u2RsnCapab
#endif
        ));

INT4
FsRadioHwSetDot11RsnaPairwiseParams PROTO ((tRadioInfo  RadioInfo,
        UINT1 u1RsnaStatus, UINT2 u2PairwiseCipherSuiteCount,
        UINT2 u2AKMSuiteCount, UINT1 *pu1PairwiseCipher,
        UINT1 *pu1AKMSuite, UINT1 u1GroupCipher, 
        UINT1 *pu1PtkKey, UINT1 u1KeyLength, 
        UINT1 u1KeyIndex, UINT1 u1Privacy, tMacAddr stationMacAddr));


INT4
FsRadioHwSetDot11RsnaGroupwiseParams PROTO ((tRadioInfo  RadioInfo,
        UINT1 u1GroupCipher, 
        UINT1 *pu1GtkKey, UINT1 u1KeyLength, 
        UINT1 u1KeyIndex));

INT4 FsRadioHwSetDot11Essid (tRadioInfo  RadioInfo, UINT1 *pu1ValDot11Essid);
INT4 FsRadioHwGetDot11Essid (tRadioInfo  RadioInfo, UINT1 *pu1ValDot11Essid);
INT4 FsRadioHwDot11WlanCreate (tRadioInfo RadioInfo );
INT4 FsRadioHwDot11WlanDestroy (tRadioInfo  RadioInfo);
INT4 FsRadioHwDot11AllWlanDestroy PROTO((VOID));
INT4 FsRadioHwSetDot11Channel (tRadioInfo  RadioInfo, UINT2 u2ChannelNum);
INT4 FsRadioHwSetDot11ChannelForWlan (tRadioInfo  RadioInfo, UINT2 u2ChannelNum);
INT4 FsRadioHwGetDot11Channel (tRadioInfo  RadioInfo, UINT2 *pu2ChannelNum);
 
INT4 FsRadioHwAddChanlSwitIE (tRadioInfo  RadioInfo, UINT2 u2ChannelNum);
INT4 FsRadioHwRemoveChanlSwitIE (tRadioInfo  RadioInfo, UINT2 u2ChannelNum);
  
INT4 FsRadioHwSetDot11Country (tRadioInfo  RadioInfo, UINT1 *pu1ValDot11CountryName);
INT4 FsRadioHwGetDot11Country (tRadioInfo  RadioInfo, UINT1 *pu1ValDot11CountryName);

INT4 FsRadioHwGet80211Stats (tRadioInfo  RadioInfo, tStats  *pStats, 
                            tMacStats *pMacStats, tMacMulStats *pMacMulStats);
INT4 FsRadioHwChkAthStatus (UINT1 *pu1AthName, INT4 *pi4RetChk);
INT4 FsRadioHwSetDot11StWpAuthKeyType (tRadioInfo  RadioInfo,INT4 * pi4ValDot11StWpKyAuthType, 
  INT2 i2ValDot11StWpKyAuthStat, INT2 i2ValDot11StWpKeyAuthIdx);

INT4 FsRadioHwGetDot11StWpAuthKeyType (tRadioInfo  RadioInfo,INT4 *pi4ValDot11StWpKyAuthType, 
  INT2 i2ValDot11StWpKyAuthStat, INT2 i2ValDot11StWpKeyAuthIdx );

INT4
FsRadioHwGetDot11ShortGi (tRadioInfo  RadioInfo, UINT4  *u4ValShortGi);

UINT1  CustNpGetWlanIfName (UINT2 u2WlanIfIndex, UINT1 *pu1WlanIfName);
INT4 FsRadioHwSetDot11ShortGi (tRadioInfo RadioInfo, UINT4  *pu4ValShortGi);
INT4
FsRadioHwSetDot11PowerConstraint (tRadioInfo RadioInfo, UINT1 u1PowerConstraint);
INT4
FsRadioHwSetDot11RsnaMgmtGroupwiseParams (tRadioInfo RadioInfo,
                                          UINT1 u1MgmtGroupCipher,
                                          UINT1 *pu1IGtkKey,
                                          UINT1 u1KeyLength, UINT1 u1KeyIndex);
INT4
FsRadioHwGetSeqNum (tRadioInfo *pRadioInfo,INT4 i4KeyIndex,UINT1 *pu1IGTKPktNum);
INT4
FsRadioHwSetDot11Capability (tRadioInfo RadioInfo, UINT2 u2Capability);
INT4
FsRadioHwUpdateWlanAdminStatusEnable PROTO ((tRadioInfo ));
INT4
FsRadioHwUpdateWlanAdminStatusDisable PROTO ((tRadioInfo ));
INT4 FsRadioHwGetInterfaceInOctets(tRadioInfo RadioInfo,UINT4 *u4IfInOctets);
INT4 FsRadioHwGetInterfaceOutOctets(tRadioInfo RadioInfo,UINT4 *u4IfOutOctets);
INT4 FsRadioHwGetClientStatsBytesRecvdCount(tRadioInfo RadioInfo,tMacAddr StationMacAddr,UINT4 *u4ClientStatsBytesRecvdCount);
INT4 FsRadioHwGetClientStatsBytesSentCount(tRadioInfo RadioInfo,tMacAddr StationMacAddr,UINT4 *u4ClientStatsBytesSentCount);
VOID 
FsRadioHwMapVlanToWlanIntf PROTO (( UINT1 u1CreateStatus, 
                     UINT2 u2VlanId, UINT1 *pu1WlanName ,UINT1 u1WlanId));
INT4 FsRadioHwSetDot11N PROTO ((tRadioInfo ,  tRadioIfNpWrConfigSetDot11n *));
#ifdef ROGUEAP_WANTED
INT4 FsRadioHwSetRougeApDetectionGroupId PROTO ((tRadioInfo ,  tRadioIfNpWrConfigSetRougeApDetection *));
#endif 
INT4 FsRadioHwSetDot11AC PROTO ((tRadioInfo ,  tRadioIfNpWrConfigSetDot11ac *));
INT4 FsRadioHwGetDriverMaxMPDULen (tRadioInfo RadioInfo, 
                                       UINT1 *pu1SuppMaxMPDULen);
INT4 FsRadioHwGetDriverRxLdpc (tRadioInfo RadioInfo, 
                                       UINT1 *pu1SuppRxLdpc);
INT4 FsRadioHwGetDriverShortGi80 (tRadioInfo RadioInfo,
                                       UINT1 *pu1SuppShortGi80);
INT4 FsRadioHwGetDriverShortGi160 (tRadioInfo RadioInfo,
                                       UINT1 *pu1ShortGi160);
INT4 FsRadioHwGetDriverTxStbc (tRadioInfo RadioInfo,
                                       UINT1 *pu1TxStbc);
INT4 FsRadioHwGetDriverRxStbc (tRadioInfo RadioInfo,
                                       UINT1 *pu1RxStbc);
INT4 FsRadioHwGetDriverSuBeamFormer (tRadioInfo RadioInfo,
                                       UINT1 *pu1SuBeamFormer);
INT4 FsRadioHwGetDriverSuBeamFormee (tRadioInfo RadioInfo,
                                       UINT1 *pu1SuBeamFormee);
INT4 FsRadioHwGetDriverSuppBeamFormAnt (tRadioInfo RadioInfo,
                                       UINT1 *pu1SuppBeamFormAnt);
INT4 FsRadioHwGetDriverSoundDimension (tRadioInfo RadioInfo,
                                       UINT1 *pu1SoundDimension);
INT4 FsRadioHwGetDriverMuBeamFormer (tRadioInfo RadioInfo,
                                       UINT1 *pu1MuBeamFormer);
INT4 FsRadioHwGetDriverMuBeamFormee (tRadioInfo RadioInfo,
                                       UINT1 *pu1MuBeamFormee);
INT4 FsRadioHwGetDriverVhtTxOpPs (tRadioInfo RadioInfo,
                                       UINT1 *pu1VhtTxOpPs);
INT4 FsRadioHwGetDriverHtcVhtCapable (tRadioInfo RadioInfo,
                                       UINT1 *pu1HtcVhtCapable);
INT4 FsRadioHwGetDriverVhtMaxAMPDU (tRadioInfo RadioInfo,
                                       UINT1 *pu1MaxAMPDU);
INT4 FsRadioHwGetDriverVhtLinkAdaption (tRadioInfo RadioInfo,
                                       UINT1 *pu1LinkAdaption);
INT4 FsRadioHwGetDriverVhtRxAntenna (tRadioInfo RadioInfo,
                                       UINT1 *pu1VhtRxAntenna);
INT4 FsRadioHwGetDriverVhtTxAntenna (tRadioInfo RadioInfo,
                                       UINT1 *pu1VhtTxAntenna);
INT4 FsRadioHwGetDriverSuppChannelWidth (tRadioInfo RadioInfo,
                                       UINT1 *pu1SuppChanWidth);
INT4 FsRadioHwGetDriverVhtSuppMcs (tRadioInfo RadioInfo,
                                       UINT1 *pu4SuppVhtCapInfo);
INT4
FsRadioHwGetSuppRadioType (tRadioInfo RadioInfo, 
                                       UINT4 *pu4SuppRadioType);
INT4
FsRadioHwGetDriverVhtCapablities (tRadioInfo RadioInfo, 
                                       UINT4 *pu4SuppVhtCapInfo);

INT4
FsRadioHwSetLocalRouting (UINT1);

INT4
FsRadioHwUpdateLocalRouting (UINT1);
UINT1
FsRadioHwGetMaxTxPowerLevel(tRadioIfNpWrFsHwGetMultiDomainInfo *pMultiDomainInfo,UINT1 *pu1AChannel,UINT1 *pu1Index,
                                UINT1 *pu1MultiArrayIndex,UINT1 u1NextAChannel,UINT1 u4TxPower,UINT1 u1MaxChannelAIndex);
FsRadioHwUpdateLocalRouting (UINT1);


INT4
FsRadioHwWlanAddVlanIntf (tWlanVlanInfo WlanVlanInf);

VOID
FsRadioHwWlanDelVlanIntf (UINT2 u2VlanId);

INT4
FsRadioHwGetMultiDomainCapabilityInfo (tRadioInfo RadioInfo, 
                                       tRadioIfNpWrFsHwGetMultiDomainInfo *pMultiDomainInfo);

VOID
FsRadioAssembleBeaconFrame (tRadioInfo *pRadioInfo);

INT4
FsRadioHwGetScanDetails (tRadioInfo RadioInfo);

INT4
FsRadioHwGetDriverHtCapablities (tRadioInfo RadioInfo,
                                       tRadioIfNpWrFsGetDot11NCapaParams *);

INT4
FsRadioHwGetDriverHtSuppMcs (tRadioInfo RadioInfo, UINT1 *pu1HtSuppMcs);

INT4
FsRadioHwGetDriverHtAMPDU (tRadioInfo RadioInfo, 
                        tRadioIfNpWrFsGetAmpduParams *AmpduParam);

INT4 FsRadioHwDFSResendBeacon (tRadioInfo  RadioInfo, UINT2 u2ChannelNum);

INT4 
FsRadioHwGetDriverChnlCapablities (tRadioInfo RadioInfo);

INT4
FsRadioHwSetDot11MulticastMode (tRadioInfo RadioInfo,
                        INT2 *pi2ValWlanMulticastMode);

INT4
FsRadioHwSetDot11MulticastLength (tRadioInfo RadioInfo,
                        INT2 *pi2ValWlanMulticastLength);
INT4
FsRadioHwSetDot11MulticastTimer (tRadioInfo RadioInfo,
                        INT4 *pi4ValWlanMulticastTimer);

INT4
FsRadioHwSetDot11MulticastTimeout (tRadioInfo RadioInfo,
                        INT4 *pi4ValWlanMulticastTimeout);
INT4
FsRadioHwSetDot11MaxAmpdu (tRadioInfo RadioInfo, UINT4 *pu4ValAmpdu);
INT4
FsRadioHwGetDot11Ldpc(tRadioInfo RadioInfo, UINT4 *pu4ValueGet);
INT4
FsRadioHwSetDot11Amsdu (tRadioInfo RadioInfo, UINT4 *pu4ValAmsdu);
INT4
FsRadioHwGetDot11Amsdu(tRadioInfo RadioInfo, UINT4 *pu4ValueGet);
INT4
FsRadioHwSetDot11AmpduDensity (tRadioInfo RadioInfo, UINT4 *pu4AmpduDensity);
INT4
FsRadioHwGetDot11AmpduDensity (tRadioInfo RadioInfo, UINT4 *pu4ValueGet);
INT4
FsRadioHwSetDot11TxStbc (tRadioInfo RadioInfo, UINT4 *pu4TxStbc);
INT4
FsRadioHwGetDot11TxStbc(tRadioInfo RadioInfo, UINT4 *pu4ValueGet);
INT4
FsRadioHwSetDot11RxStbc (tRadioInfo RadioInfo, UINT4 *pu4RxStbc);
INT4
FsRadioHwGetDot11RxStbc(tRadioInfo RadioInfo, UINT4 *pu4ValueGet);
INT4
FsRadioHwSetDot11HtIntol (tRadioInfo RadioInfo, UINT4 *pu4HtIntol);
INT4
FsRadioHwGetDot11HtIntol(tRadioInfo RadioInfo, UINT4 *pu4ValueGet);
INT4
FsRadioHwSetDot11VhtmaxAmpdu (tRadioInfo RadioInfo, UINT4 *pu4VhtmaxAmpdu);
INT4
FsRadioHwGetDot11VhtmaxAmpdu(tRadioInfo RadioInfo, UINT4 *pu4ValueGet);
INT4
FsRadioHwSetDot11VhtAmpduFactor (tRadioInfo RadioInfo, UINT4 *pu4VhtAmpduFactor);
INT4
FsRadioHwGetDot11VhtAmpduFactor(tRadioInfo RadioInfo, UINT4 *pu4ValueGet);
INT4
FsRadioHwSetDot11VhtTxMcs (tRadioInfo RadioInfo, UINT4 *pu4VhtTxMcs);
INT4
FsRadioHwGetDot11VhtTxMcs (tRadioInfo RadioInfo, UINT4 *pu4ValueGet);
INT4
FsRadioHwSetDot11VhtRxMcs (tRadioInfo RadioInfo, UINT4 *pu4VhtRxMcs);
INT4
FsRadioHwGetDot11VhtRxMcs (tRadioInfo RadioInfo, UINT4 *pu4ValueGet);
INT4
FsRadioHwSetDot11nParams (tRadioInfo RadioInfo, UINT1 u1AMPDUStatus,
 UINT1 u1AMPDUSubFrame, UINT2 u2AMPDULimit, 
 UINT1 u1AMSDUStatus, UINT2 u2AMSDULimit);
INT4
FsSecNpHwSetRateLimit(UINT1 u1Module, INT4 i4Status);
INT4
FsRadioWlanIsolationSet (tRadioInfo RadioInfo,
                        INT2 *p2ValWlanIsolationEnable);
INT4
FsRadioHwDot11AllWlanCreate (VOID);
INT4
FsRadioHwSetEnableOlStats (tRadioInfo RadioInfo, UINT4 *pu4VhtRxMcs);
INT4
FsRadioHwSetDot11QosInfoTableParams(tRadioInfo RadioInfo, UINT4 *u4QosInfo);
INT4
FsRadioHwSetDot11Ldpc (tRadioInfo RadioInfo, UINT4 *pu4ValLdpc);
UINT1
FsRadioHwDot11BrInterfaceCreate (tRadioIfNpWrConfigDot11CreateBrInterface *pCreateBrInterface);

INT4 
FsRadioHwSetRadarFound (tRadioInfo  RadioInfo);


INT4
FsRadioHwGetDriverBeamFormCapablities (tRadioInfo RadioInfo,
                       UINT4 *pu4SuppBeamFormCapInfo);

INT4
FsRadioHwGetDriverExtHtCapablities (tRadioInfo RadioInfo,
                         UINT2 *pu2SuppExtHtCapInfo);
INT4 FsRadioHwSetBandwidthStatus (tRadioInfo RadioInfo, 
        tRadioIfNpWrFsBandSelectStatus *pEntry);
INT4
FsRadioHwStaDelBandSteer (tRadioInfo RadioInfo,UINT1 *pu1StaMac);
#endif

