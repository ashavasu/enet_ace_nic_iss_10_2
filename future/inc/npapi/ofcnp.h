/*****************************************************************************/
/* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*******************************************************************************
**    FILE  NAME             : ofcnp.h
**    PRINCIPAL AUTHOR       : Aricent Inc.
**    SUBSYSTEM NAME         : OFC
**    MODULE NAME            : TCAM AND NONTCAM NPAPI MODULE
**    LANGUAGE               : ANSI-C
**    DATE OF FIRST RELEASE  :
**    DESCRIPTION            : Definition of constants and structures used
**                             across Hardware Flowtables
* * $Id: ofcnp.h,v 1.1
**---------------------------------------------------------------------------*/
#ifndef _OFCNP_H
#define _OFCNP_H

#define OFC_MAX_ACTIONS                 17
#define OPNFL_LOWEST_FILTER_PRIORITY     1
#define OFC_NP_ONE                       1 
#define OFC_NP_ZERO                      0 
#define OFC_NP_TWO                       2
#define OFC_NP_FOUR                      4
#define OFC_NP_MINUS_ONE                -1 

typedef struct
{
    UINT4 u4InIfIndex;
    UINT1 au1Ip6Src[6];
    UINT1 au1Ip6Dst[6];
    UINT1 au1SrcMacAddr[6];
    UINT1 au1DstMacAddr[6];
    UINT4 u4Ip4Src;
    UINT4 u4Ip4Dst;
    UINT4 u4InPort;
    UINT2 u2EthType;
    UINT2 u2TpSrc;
    UINT2 u2TpDst;
    UINT1 u1IpProto;
    UINT1 au1pad[1];
    UINT1 u1IpTos;
    UINT1 u1IpDscp;
    UINT1 au1pad2[2];
}tOfcHwExactFlowMatch;

typedef struct
{
    UINT4 u4IfIndex;
    UINT4 u4FlowIndex;
}tOfcHwIfInfo;
    
typedef struct OFCHWFLOWENTRY
{
    UINT4                u4FlowIndex;
    UINT2                u2Priority;
    UINT1                u1TableId;
    UINT1                u1Pad[1];
    UINT4                u4Action;                      
    UINT4                u4ActionParam;                      
    tPortList            RedirectPortList;
    tOfcHwExactFlowMatch ExactKey;
    UINT4                u4NumActs;
    UINT1                OfcActs[OFC_MAX_ACTIONS];
    UINT1                au1Pad[3];
    /* Newly added to support 1.3.1 */
    FS_UINT8             u8Cookie;
    FS_UINT8             u8CookieMask;
    UINT2                u2IdleTimeout;
    UINT2                u2HardTimeout;
    tTMO_SLL            *pMatchList;
    tTMO_SLL            *pInstrList;
}tOfcHwFlowInfo;

typedef struct
{
    tTMO_SLL_NODE Node;
    UINT4         u4WatchPort;   /* Only required for fast failover groups */
    UINT4         u4WatchGroup;  /* Only required for fast failover groups */
    UINT2         u2Weight;      /* Only defined for select groups */
    UINT1         u1SelectFlag;
    UINT1         au1pad[1];
    UINT4         u4NumActs;
    tTMO_SLL      ActList;
} tOfcHwGroupBucket;

typedef struct
{
    UINT4     u4FsofcGroupIndex;
    INT4      i4FsofcGroupType;
    UINT4     u4FsofcGroupDurationSec;
    tTMO_SLL *pBucketList;
} tOfcHwGroupInfo;

typedef struct
{
        UINT4 u4FsofcMeterIndex;
        UINT4 u4FsofcMeterFlowCount;
        UINT4 u4FsofcMeterDurationSec;
        UINT2 u2Flags;
        UINT1 au1Pad[2];
        FS_UINT8 u8FsofcMeterPacketInCount;
        FS_UINT8 u8FsofcMeterByteInCount;
        tTMO_SLL             BandList; /* tOfcMeterBand */
} tOfcHwMeterInfo;

typedef enum
{
    OFC_NP_PORT_ADD,
    OFC_NP_PORT_DELETE,
    OFC_NP_PORT_DOWN,
    OFC_NP_PORT_UP,
    OFC_NP_FLOW_ADD,
    OFC_NP_FLOW_DELETE,
    OFC_NP_FLOW_MODIFY,
    OFC_NP_GROUP_ADD,
    OFC_NP_GROUP_DELETE,
    OFC_NP_GROUP_MODIFY,
    OFC_NP_METER_ADD,
    OFC_NP_METER_DELETE,
    OFC_NP_METER_MODIFY,
    OFC_NP_CONFIG_PARAM,
    OFC_NP_GET_FLOW_STATS,
    OFC_NP_GET_GROUP_STATS,
    OFC_NP_GET_METER_STATS,
    OFC_NP_VLAN_ADD,
    OFC_NP_VLAN_DELETE,
    OFC_NP_VLAN_SET,
    OFC_NP_VLAN_RESET,
    OFC_NP_INIT
}eOfcCmd;

typedef enum
{
    OFC_VER_100 = 0,
    OFC_VER_131 = 1,
}eOfcVer;

typedef enum
{
    OFC_IP_REASSEMBLE = 0,
    OFC_PKT_BUFFERING = 1,
    OFC_STP_STATUS = 2
}eOfcCfgParam;

typedef enum
{
    OFC_HW_ACL_FILTERS = 0,
    OFC_HW_METER_ENTRIES = 1,
}eOfcRsrvParam;

typedef struct
{
    eOfcCfgParam OfcCfgParam;
    UINT4 u4ParamValue;
}tOfcHwCfgInfo;

typedef struct
{
    eOfcRsrvParam OfcRsrvParam;
    UINT4 u4Entries;
}tOfcHwRsrvInfo;

typedef struct
{
    FS_UINT8 u8PktCount;
    FS_UINT8 u8ByteCount;
    UINT4    u4DurSec;
    UINT4    u4FlowIndex;
}tOfcFlowStats;

typedef struct
{
    UINT4    u4RefCount;
    FS_UINT8 u8PktCount;
    FS_UINT8 u8ByteCount;
    UINT4    u4DurSec;
}tOfcGroupStats;

typedef struct
{
    FS_UINT8           u8PktCount;          /* Number of packets matched. */
    FS_UINT8           u8ByteCount;         /* Number of bytes matched. */
}tOfcHwMeterStats;

typedef struct
{
    UINT4 u4ContextId;
    UINT4 u4VlanId;
    UINT1 *pu1EgressPorts;
    UINT1 *pu1UntagPorts;
}tOfcHwVlanInfo;

typedef struct
{
    UINT1 au1HwAddr[6];
    UINT1 au1Pad[2];
    UINT1 au1Name[16];
    UINT4 u4Config;
    UINT4 u4State;
    UINT4 u4Curr;
    UINT4 u4Advertised;
    UINT4 u4Supported;
    UINT4 u4Peer;
    UINT4 u4CurrSpeed;
    UINT4 u4MaxSpeed;
} tOfcHwPortInfo;

typedef struct
{
    FS_UINT8 u8RxPkts;
    FS_UINT8 u8TxPkts;
    FS_UINT8 u8RxBytes;
    FS_UINT8 u8TxBytes;
    FS_UINT8 u8RxDropped;
    FS_UINT8 u8TxDropped;
    FS_UINT8 u8RxErrors;
    FS_UINT8 u8TxErrors;
    FS_UINT8 u8RxFrameErr;
    FS_UINT8 u8RxOverErr;
    FS_UINT8 u8RxCrcErr;
    FS_UINT8 u8Collisions;
    UINT4 u4DurSec;
} tOfcHwPortStats;

typedef struct
{
    UINT4 u4PortNum;
    UINT4 u4QueueId;
    UINT4 u4MinRate;
    UINT4 u4MaxRate;
}tOfcHwQueueInfo;

typedef struct
{
    FS_UINT8 u8TxBytes;
    FS_UINT8 u8TxPkts;
    UINT4 u4DurSec;
} tOfcHwQueueStats;

typedef union
{
    tOfcHwIfInfo      OfcHwIfInfo;
    tOfcHwFlowInfo    OfcHwFlowInfo;
    tOfcHwGroupInfo   OfcHwGroupInfo;
    tOfcHwMeterInfo   OfcHwMeterInfo;
    tOfcHwCfgInfo     OfcHwCfgInfo;
    tOfcFlowStats     OfcFlowStats;
    tOfcGroupStats    OfcGroupStats;
    tOfcHwMeterStats  OfcMeterStats;
    tOfcHwRsrvInfo    OfcHwRsrvInfo;
    tOfcHwVlanInfo    OfcHwVlanInfo;
}unHwParam;

typedef struct
{
    unHwParam      OfcHwEntry;
    eOfcVer        OfcVer;
    eOfcCmd        OfcCmd;
}tOfcHwInfo;

INT4
FsOfcHwOpenflowInitAclFilters (UINT4 u4Entries);

INT4
FsOfcHwOpenflowInitMeterEntries (UINT4 u4Entries);

VOID 
FsOfcHwInitFilter PROTO ((UINT4 u4Index, tOfcHwFlowInfo *pFlow));

INT4
FsOfcHwOpenflowClientCfgParamsUpdate PROTO ((eOfcCfgParam OfcCfgParam, UINT4 u4CfgParamValue));

INT4
FsOfcHwOpenflowPortUpdate PROTO ((UINT4 u4Ifindex, eOfcCmd OfcCmd));

INT4
FsOfcHwUpdateExactFlowEntry PROTO ((tOfcHwFlowInfo *pFlow, eOfcCmd OfcCmd));

INT4
FsOfcHwGetExactFlowStats PROTO ((tOfcHwExactFlowMatch *pOfcFlowMatch, tOfcFlowStats *pOfcFlowStats));

INT4
FsOfcHwUpdateGroupEntry PROTO ((tOfcHwGroupInfo *pOfcGroupEntry, eOfcCmd OfcCmd));

INT4
FsOfcHwUpdateMeterEntry PROTO ((tOfcHwMeterInfo *pOfcMeterEntry, eOfcCmd OfcCmd));

INT4
FsOfcHwGetGroupStats PROTO ((tOfcHwGroupInfo *pOfcGroupEntry, tOfcGroupStats *pOfcGroupStats));

INT4
FsOfcHwGetMeterStats PROTO ((tOfcHwMeterInfo *pOfcMeterEntry, tOfcHwMeterStats *pOfcMeterStats));

INT4
FsOfcHwAddVlanEntry PROTO ((UINT4 u4ContextId, UINT4 u4VlanId, UINT1 *pu1EgressPorts , UINT1 *pu1UntagPorts));

INT4
FsOfcHwDelVlanEntry PROTO ((UINT4 u4ContextId, UINT4 u4VlanId));

INT4
FsOfcHwSetVlanMemberPorts PROTO ((UINT4 u4ContextId,UINT4 u4VlanId, UINT1 *pu1EgressPorts , UINT1 *pu1UntagPorts));

INT4
FsOfcHwResetVlanMemberPorts PROTO ((UINT4 u4ContextId, UINT4 VlanId, UINT1 *pu1EgressPorts , UINT1 *pu1UntagPorts));

#ifdef OPENFLOW_TCAM /* || DPA_WANTED */
INT4
FsOfcHwUpdateWCFlowEntry PROTO ((tOfcHwFlowInfo *pFlow, eOfcCmd OfcCmd));
INT4
FsOfcHwGetWCFlowStats PROTO ((tOfcHwFlowInfo *pFlow, tOfcFlowStats *pOfcFlowStats));

#endif /* OPENFLOW_TCAM */

#endif
/*-----------------------------------------------------------------------*/
/*                       End of the file  ofcnp.h                        */
/*-----------------------------------------------------------------------*/


