/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 *
 * Description: All prototypes for Network Processor API functions for 
 * TAC Module 
 *
 * * $Id: tacnp.h,v 1.3 2011/06/10 15:29:30 siva Exp $ 
 *
 *******************************************************************/
#ifndef _TACNP_H
#define _TACNP_H

INT4 FsTacHwUpdateStatus (INT4 i4Status);

#endif /*_TACNP_H*/
