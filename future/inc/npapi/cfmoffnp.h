/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfmoffnp.h,v 1.24 2014/11/11 13:09:11 siva Exp $
 *
 * Description: Exported file for ECFM NP-API.
 *
 *******************************************************************/
#ifndef __CFMOFFNP_H
#define __CFMOFFNP_H


#include "ecfm.h"

/* MACROS values for Generic API */
#define ECFM_NP_START_CCM_TX_ON_PORTLIST 1
#define ECFM_NP_START_CCM_TX             2
#define ECFM_NP_STOP_CCM_TX              3
#define ECFM_NP_START_CCM_RX_ON_PORTLIST 4
#define ECFM_NP_STOP_CCM_RX              5
#define ECFM_NP_START_CCM_RX             6
#define ECFM_NP_MA_CREATION              7
#define ECFM_NP_MA_DELETION              8
#define ECFM_NP_OFFLOAD_STATE            9
#define ECFM_NP_MEP_CREATE               10
#define ECFM_NP_MEP_DELETE               11

#define ECFM_NP_MP_DIR_DOWN              1  /* MP Direction */
#define ECFM_NP_MP_DIR_UP                2   /* MP Direction */

#define ECFM_NP_RMEP_STATE_OK            3   /* State is set on receipt of the
                                              * valid CCM */

INT4
FsEcfmHwStartCcmTx (UINT4 u4IfIndex, 
                    UINT1 *pCCM_PDU, 
                    UINT2 u2Length,
                    UINT4 u4TxInterval, 
                    UINT2 u2TxFilterId);
INT4
FsEcfmHwStartCcmRx( UINT4 u4IfIndex,
                    UINT2 u2MEPID,
                    tEcfmCcOffRxInfo* pEcfmCcOffRxInfo,
                    UINT2 u2RxFilterId);
INT4
FsEcfmHwStartCcmTxOnPortList (tHwPortArray PortList, 
                    tHwPortArray UntagPortList,
                    UINT1 *pCCM_PDU, 
                    UINT2 u2Length,
                    UINT4 u4TxInterval, 
                    UINT4 u4ContextId,
                    UINT2 u2TxFilterId);
INT4
FsEcfmHwStartCcmRxOnPortList( tHwPortArray PortList,
                              tHwPortArray UntagPortList,
                               UINT2 u2MEPID,
                               tEcfmCcOffRxInfo* pEcfmCcOffRxInfo,
                               UINT4 u4ContextId,
                               UINT2 u2RxFilterId);
VOID
FsEcfmHwStopCcmTx (UINT4 u4ContextId,UINT2 u2TxFilterId);

VOID
FsEcfmHwStopCcmRx (UINT4 u4ContextId,UINT2 u2RxFilterId);

INT4
FsEcfmHwGetCcmTxStatistics (UINT4 u4ContextId,
                            UINT2 u2TxFilterId,
                            tEcfmCcOffMepTxStats *pEcfmCcOffMepTxStats);

INT4
FsEcfmHwGetCcmRxStatistics (UINT4 u4ContextId,
                            UINT2 u2RxFilterId,
                            tEcfmCcOffMepRxStats *pEcfmCcOffMepRxStats);

INT4                                   
FsEcfmHwRegister (UINT4 u4ContextId);

                                            
INT4
FsEcfmHwGetPortCcStats (UINT4 u4ContextId,UINT4 u4IfIndex,
                        tEcfmCcOffPortStats *pEcfmCcOffPortStats);                        
/*End of OLD NPAPIs*/

/*Start of New NPAPIs*/
INT4
FsMiEcfmHwStartCcmTx (UINT4 u4ContextId,
                    UINT4 u4IfIndex, 
                    UINT1 *pCCM_PDU, 
                    UINT2 u2Length,
                    UINT4 u4TxInterval, 
                    UINT2 u2TxFilterId);
INT4
FsMiEcfmHwStartCcmRx( UINT4 u4ContextId,
                    UINT4 u4IfIndex,
                    UINT2 u2MEPID,
                    tEcfmCcOffRxInfo* pEcfmCcOffRxInfo,
                    UINT2 u2RxFilterId);
INT4
FsMiEcfmHwSetAisStatus (tEcfmMplstpAisOffParams *pAisOffParams);
INT4
FsMiEcfmHwStrtCcmTxOnPrtLst (UINT4 u4ContextId,
                    tHwPortArray PortList, 
                    tHwPortArray UntagPortList,
                    UINT1 *pCCM_PDU, 
                    UINT2 u2Length,
                    UINT4 u4TxInterval, 
                    UINT2 u2TxFilterId);
INT4
FsMiEcfmHwStrtCcmRxOnPrtLst( UINT4 u4ContextId,
                               tHwPortArray PortList,
                              tHwPortArray UntagPortList,
                               UINT2 u2MEPID,
                               tEcfmCcOffRxInfo* pEcfmCcOffRxInfo,
                               UINT2 u2RxFilterId);
VOID
FsMiEcfmHwStopCcmTx (UINT4 u4ContextId,UINT2 u2TxFilterId);

VOID
FsMiEcfmHwStopCcmRx (UINT4 u4ContextId,UINT2 u2RxFilterId);

INT4
FsMiEcfmHwGetCcmTxStatistics (UINT4 u4ContextId,
                            UINT2 u2TxFilterId,
                            tEcfmCcOffMepTxStats *pEcfmCcOffMepTxStats);

INT4
FsMiEcfmHwGetCcmRxStatistics (UINT4 u4ContextId,
                            UINT2 u2RxFilterId,
                            tEcfmCcOffMepRxStats *pEcfmCcOffMepRxStats);

INT4                                   
FsMiEcfmHwRegister (UINT4 u4ContextId);

                                            
INT4
FsMiEcfmHwGetPortCcStats (UINT4 u4ContextId,UINT4 u4IfIndex,
                        tEcfmCcOffPortStats *pEcfmCcOffPortStats);                        
INT4                        
FsMiEcfmHwSetVlanEtherType(UINT4 u4ContextId, UINT4 u4IfIndex, 
                           UINT2 u2EtherTypeValue, UINT1 u1EtherType);
INT4
FsMiEcfmHwHandleIntQFailure (UINT4 u4ContextId,
                             tEcfmCcOffRxHandleInfo* pEcfmCcOffRxHandleInfo,
                             UINT2 *pu2RxHandle,
                             BOOL1 *pb1More);
VOID
EcfmConvertArrayToPortList (tHwPortArray * pIfPortArray, UINT1 *PortList,
                            INT4 i4Length);
INT4
FsEcfmParseCcmPdu (UINT1 *pCcmPdu, UINT1 *pu1DstAddr, UINT1 *pu1SrcAddr,
                   UINT1 *pu1MAID, UINT1 *pu1Level, UINT2 *pu2Vlan,
                   UINT2 *pu2MEPID);

INT4
FsEcfmFilterForElps(tEcfmHwParams *,UINT1 u1Type);
INT4
FsEcfmFilterForMel(tEcfmHwParams *,UINT1 u1Type);

INT4 
FsMiEcfmHwCallNpApi (UINT1 , tEcfmHwParams *);

INT4 NpFsEcfmStartCcmTx (tEcfmHwParams *);
INT4 NpFsEcfmStartCcmRx (tEcfmHwParams *);
VOID NpFsEcfmStopCcmTx (tEcfmHwParams *);
VOID NpFsEcfmStopCcmRx (tEcfmHwParams *);
INT4 NpFsEcfmMaCreate (tEcfmHwParams *);
VOID NpFsEcfmMaDelete (tEcfmHwParams *);
INT4 NpFsEcfmMaInit (tEcfmHwParams *);
VOID NpFsEcfmCreateDummyGroup (VOID);
VOID NpFsEcfmCreateDummyEndpoint (VOID);
INT4 NpFsEcfmOffloadStatus (tEcfmHwParams * pEcfmHwParams);

#endif
