/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ecfmminp.h,v 1.15 2016/02/08 10:35:58 siva Exp $
 *
 * Description: Exported file for ECFM NP-API.
 *
 *******************************************************************/
#ifndef __ECFMMINP_H
#define __ECFMMINP_H

#if defined (Y1564_WANTED) || defined (RFC2544_WANTED)
#include "y1564.h"
#include "r2544.h"

#include "fsvlan.h"
#endif



INT4 FsMiEcfmHwInit PROTO ((UINT4));
INT4 FsMiEcfmHwDeInit PROTO ((UINT4));
INT4 FsMiEcfmTransmitDmm PROTO ((UINT4 , UINT4 , UINT1 *, UINT2 ,tVlanTag , UINT1 ));
INT4 FsMiEcfmTransmitDmr PROTO ((UINT4 , UINT4 , UINT1 *, UINT2 ,tVlanTag , UINT1 ));
INT4 FsMiEcfmTransmitLmm PROTO ((UINT4 , UINT4 , UINT1 *, UINT2 ,tVlanTagInfo, UINT1 ));
INT4 FsMiEcfmTransmitLmr PROTO ((UINT4 , UINT4 , UINT1 *, UINT2 ,tVlanTagInfo *, UINT1 ));
INT4 FsMiEcfmTransmit1Dm PROTO ((UINT4 , UINT4 , UINT1 *, UINT2 ,tVlanTag , UINT1 ));
INT4 FsMiEcfmStartLbmTransaction PROTO ((tEcfmMepInfoParams *, tEcfmConfigLbmInfo *));
INT4 FsMiEcfmStopLbmTransaction PROTO ((tEcfmMepInfoParams *));
INT4 FsMiEcfmStartTstTransaction PROTO ((tEcfmMepInfoParams *, tEcfmConfigTstInfo *));
INT4 FsMiEcfmStopTstTransaction PROTO ((tEcfmMepInfoParams *));
INT4 FsMiEcfmClearRcvdLbrCounter PROTO ((tEcfmMepInfoParams *));
INT4 FsMiEcfmClearRcvdTstCounter PROTO ((tEcfmMepInfoParams *));
INT4 FsMiEcfmGetRcvdLbrCounter PROTO ((tEcfmMepInfoParams *, UINT4 *));
INT4 FsMiEcfmGetRcvdTstCounter PROTO ((tEcfmMepInfoParams *, UINT4 *));

INT4 FsMiEcfmStartLm PROTO ((tEcfmHwLmParams *pHwLmInfo));
INT4 FsMiEcfmStopLm PROTO ((tEcfmHwLmParams *pHwLmInfo));
#ifdef MBSM_WANTED

/* MACROS values for Generic MBSM API */
#define ECFM_NP_MBSM_START_CCM_TX_ON_PORTLIST 1
#define ECFM_NP_MBSM_START_CCM_TX             2
#define ECFM_NP_MBSM_STOP_CCM_TX              3
#define ECFM_NP_MBSM_START_CCM_RX_ON_PORTLIST 4
#define ECFM_NP_MBSM_STOP_CCM_RX              5
#define ECFM_NP_MBSM_START_CCM_RX             6

INT4 FsMiEcfmMbsmNpInitHw PROTO ((UINT4 , tMbsmSlotInfo * ));
INT4 FsMiEcfmMbsmNpDeInitHw PROTO ((UINT4 , tMbsmSlotInfo * ));
INT4 FsMiEcfmMbsmHwStrtCcmTxOnPrtLst PROTO ((UINT4 , tHwPortArray, tHwPortArray, UINT1 *,
                                               UINT2 ,UINT4 , UINT2 ,
                                              tMbsmSlotInfo * ));
INT4 FsMiEcfmMbsmHwStartCcmTx PROTO (( UINT4 ,UINT4 , UINT1 *, UINT2 ,
                                    UINT4 ,UINT2 , tMbsmSlotInfo * ));
INT4 FsMiEcfmMbsmHwStrtCcmRxOnPrtLst PROTO (( UINT4 ,tHwPortArray, tHwPortArray, UINT2,
                                              tEcfmCcOffRxInfo * ,
                                              UINT2 ,tMbsmSlotInfo * ));
INT4 FsMiEcfmMbsmHwStartCcmRx PROTO (( UINT4 ,UINT4 , UINT2 ,
                                    tEcfmCcOffRxInfo *,UINT2, tMbsmSlotInfo *));
INT4 FsMiEcfmMbsmHwCallNpApi (UINT1 u1Type, tEcfmHwParams *pEcfmHwInfo, 
                              tMbsmSlotInfo *pSlotInfo);
#endif /*MBSM_WANTED*/
/**
 * BIT 0 - LBM transmission and LBR reception capability
 * BIT 1 - DMM and DMR transmission, reception capability
 * BIT 2 - 1DM tranmission and reception capability
 * BIT 3 - LMM and LMR transmission, reception capability
 * BIT 4 - TST transmission, reception capability
 * BIT 5 - CCM Offloading capability
 */
INT4 FsMiEcfmHwGetCapability PROTO ((UINT4, UINT4*));

#if defined (Y1564_WANTED) || defined (RFC2544_WANTED)

#define SLA_FRAME_SIZE        1518

#define SLA_HW_PAYLOAD_SIZE 16
#define SLA_ENET_ADDR_LEN    6


#define SLA_NP_TX_PROCESS      1
#define SLA_NP_RX_PROCESS      2
#define SLA_NP_RX_START        3

#define SLA_NP_TX_EVENT        0x10
#define SLA_NP_RX_PKT_EVENT    0x20
#define SLA_NP_RX_TMR_EVENT    0x800
#define SLA_NP_RX_START_EVENT  0x40

#define SLA_NP_MAX_SERVICE     11
#define SLA_NP_MAX_STEP_ID     10

#define SLA_NP_TX_START      1
#define SLA_NP_TX_STOP       2

typedef struct _HwTestFrameStats
{
    FS_UINT8        u8TxBytes;
                        /* Total Bytes Transmitted */
    FS_UINT8        u8RxBytes;
                        /* Received Total Bytes */
    UINT4           u4TxPkts;
                        /* Transmitted packet count */
    UINT4           u4RxPkts;
                        /* Recieved packet count */
    FS_UINT8        u8YellowFrTxBytes;
                        /* Total Bytes Transmitted */
    FS_UINT8        u8YellowFrRxBytes;
                        /* Received Total Bytes */
    UINT4           u4YellowFrTxPkts;
                        /* Transmitted packet count */
    UINT4           u4YellowFrRxPkts;
                        /* Recieved packet count */
    UINT4           u4CIRPps;
    UINT4           u4EIRPps;
    UINT2           u2PktSize;
    UINT1           u1Pad[2];
} tHwTestFrameStats;

typedef struct _HwSlaInfo
{
    tMacAddr        SlaTxSrcMacAddr;
                        /* Source MAC address */
    tVlanId         InVlanId;
                        /* Inner Vlan Id */
    tMacAddr        SlaTxDstMacAddr;
                        /* Destination MAC address */
    tVlanId         OutVlanId;
                        /* Outer Vlan Id */
    UINT1            *pu1Pdu; /* Pointer to the PDU */
    UINT4           u4SlaId;
                        /* Sla Identifier */
    UINT4           u4IfIndex;
                        /* Port Identifier */
    UINT4           u4ContextId;
                        /* Context Identifier */
    UINT4           u4SlaDuration;
                        /* Duration for which packet to be transmitted */
    UINT4           u4R2544SubTest; 
                        /* Sub test for which the test is running */
    UINT4           u4TrialCount; 
                        /* The Trial Count number for RFC2544 test */
    UINT4           u4CIR; 
                        /* CIR for EVC Index */
    UINT4           u4CBS;
                        /* CBS for EVC Index */
    UINT4           u4EIR;
                        /* CIR for EVC Index */
    UINT4           u4EBS;
                        /* EBS for EVC Index */
    UINT4           u4Rate;
    UINT4           u4YellowFrRate;
    UINT4           u4CIRPps;
    UINT4           u4YellowFrPps;
    UINT4           u4TagType;
    UINT4           u4DwellTime;
                       /* Tag Type */
    UINT2           u2PktSize;
                        /* Packet Size */
    UINT2           u2StepId;
                        /* Step Identifier */
    UINT2           u2EvcId;
                        /* EVC Index or Service Configuration Identifier*/
    UINT1           u1InCos;
                        /* Inner Class of service */
    UINT1           u1OutCos;
                        /* Inner Class of service */
    UINT1           au1TrafProfPayload[SLA_HW_PAYLOAD_SIZE + 1];
                        /* Payload information */
    UINT1           u1TestMode;
                        /* current test mode of the test like Simple
                         * CIR or Step Load CIR or EIR color aware or
                         * EIR non-color aware or Traffic Policing color aware
                         * or Traffic Policing non-color-aware */
    UINT1          u1Pcp;
                       /* PCP Value */
    UINT1          u1SeqNoCheck;
    UINT1          u1Dei;
                       /* Zero for green colored frames and ONE for yellow
                        * colored frames */
    UINT1          u1Pad[3];
}tHwSlaInfo;


typedef struct _HwTestInfo
{
    tHwSlaInfo          HwSlaInfo;
                        /* Information about SLA */
    tHwTestFrameStats   HwTestFrameStats;
                        /* CIR test report */
    UINT4               u4InfoType;
                        /* Information type to be set or queried from H/w */
} tHwTestInfo;

typedef struct _HwLoopbackInfo
{
    tMacAddr        MepMacAddr;      /* Source MAC address */
    UINT2           u2MepId;         /* MEP Identifierfor the MEP Table. */
    UINT4           u4ContextId;     /* Context Identifier */
    UINT4           u4IfIndex;       /* Port Identifier */
    UINT4           u4MdIndex;       /* Md Index of the MD to which this MEP belongs */
    UINT4           u4MaIndex;       /* Ma Index of the MA to which this MEP belongs */
    UINT4           u4PrimaryVidIsid;/* Primary VID assigned to this MEP. Its
                                      * value can be either the Primary VID of
                                      * the MA it is associated or any Vlan ID
                                      * associated to the Primary VID of its
                                      * MA. Also used as index for MEP RBTree
                                      * in portInfo
                                      */
    INT4            i4LoopbackStatus;/* Loopback Status of the Local Port Number of
                                      * the IfIndex.
                                      */
    UINT2           u2PortNum;       /* Local Port Number of the IfIndex */
    UINT1           u1MdLevel;       /* MDLevel at which MEP is configured */
    UINT1           u1Direction;     /* Indicates the direction of the MEP (Up/Down) */
} tHwLoopbackInfo;

enum {
    ECFM_HW_START_SLA_TEST = 1,
    ECFM_HW_STOP_SLA_TEST,
    ECFM_HW_FETCH_SLA_REPORT
};

INT4 FsMiEcfmSlaTest (tHwTestInfo * pHwTestInfo);

INT4
EcfmSlaTestStartTx (tHwSlaInfo HwSlaInfo);

INT4
EcfmSlaTestStopTx (tHwSlaInfo HwSlaInfo);

INT4
EcfmSlaHwGetEvcStats (tHwSlaInfo HwSlaInfo);

UINT1
FsMiEcfmAddHwLoopbackInfo (tHwLoopbackInfo * pHwLoopbackInfo);

UINT1
FsMiEcfmDelHwLoopbackInfo (tHwLoopbackInfo * pHwLoopbackInfo);

#endif
#endif /*__ECFMMINP_H*/
