/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwlnp.h,v 1.4 2015/07/22 09:41:08 siva Exp $
 * 
 * Description: Exported file for FWL NP-API.
 * 
 * *******************************************************************/


#ifndef _FWLNP_H_
#define _FWLNP_H_

VOID FwlNpEnableDosAttack PROTO ((VOID));
INT4 FwlNpEnableDosAttackCust PROTO ((INT4 i4DosAttackType ,INT4 i4Setvalue));
VOID FwlNpDisableDosAttack PROTO ((VOID));
VOID FwlNpEnableIpHeaderValidation PROTO ((VOID));
VOID FwlNpDisableIpHeaderValidation PROTO ((VOID));
VOID FwlNpGetHwFilterCapabilities PROTO ((UINT1 *pFwlFilter));
INT4 FwlNpRateLimitEnable PROTO ((tFwlRateLimit *pFwlRateLimit));
INT4 FwlNpRateLimitDisable PROTO ((tFwlRateLimit *pFwlRateLimit));

#endif
