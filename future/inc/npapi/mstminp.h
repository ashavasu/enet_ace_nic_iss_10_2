/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mstminp.h,v 1.8 2011/02/09 11:56:50 siva Exp $ 
 *
 * Description: All prototypes for Network Processor API functions for MSTP 
 * 
 *
 *******************************************************************/
#ifndef _MSTMINP_H
#define _MSTMINP_H

INT1 FsMiMstpNpCreateInstance PROTO ((UINT4 u4ContextId, UINT2 u2InstId));

INT1 FsMiMstpNpAddVlanInstMapping PROTO ((UINT4 u4ContextId, tVlanId VlanId, UINT2 u2InstId));

INT1 FsMiMstpNpAddVlanListInstMapping PROTO ((UINT4 u4ContextId, UINT1 *pu1VlanList, UINT2 u2InstId, UINT2 u2NumVlans, UINT2 *pu2LastVlan));

INT1 FsMiMstpNpDeleteInstance PROTO ((UINT4 u4ContextId, UINT2 u2InstId));

INT1 FsMiMstpNpDelVlanInstMapping PROTO ((UINT4 u4ContextId, tVlanId VlanId, UINT2 u2InstId));

INT1 FsMiMstpNpDelVlanListInstMapping PROTO ((UINT4 u4ContextId, UINT1 *pu1VlanList, UINT2 u2InstId, UINT2 u2NumVlans, UINT2 *pu2LastVlan));
INT4
FsMiMstpNpSetInstancePortState PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2InstanceId,
                              UINT1 u1PortState));

VOID FsMiMstpNpInitHw PROTO ((UINT4 u4ContextId));
VOID FsMiMstpNpDeInitHw PROTO ((UINT4 u4ContextId));
INT1 FsMiMstNpGetPortState PROTO ((UINT4 u4ContextId, UINT2 u2InstanceId, 
                UINT4 u4IfIndex, UINT1* pu1Status));

#ifdef MBSM_WANTED
INT1 FsMiMstpMbsmNpCreateInstance PROTO ((UINT4,UINT2 , tMbsmSlotInfo *));
INT1 FsMiMstpMbsmNpAddVlanInstMapping PROTO ((UINT4,tVlanId , UINT2,
                                            tMbsmSlotInfo *));
INT1 FsMiMstpMbsmNpSetInstancePortState PROTO ((UINT4,UINT4 , UINT2 , UINT1, 
                                              tMbsmSlotInfo *));
INT1 FsMiMstpMbsmNpInitHw PROTO ((UINT4 , tMbsmSlotInfo *));
#endif

#endif /* _MSTMINP_H */
