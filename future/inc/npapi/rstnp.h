/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rstnp.h,v 1.6 2007/05/26 11:11:57 iss Exp $ 
 *
 * Description: All prototypes for Network Processor API functions for RSTP 
 * 
 *
 *******************************************************************/
#ifndef _RSTNP_H
#define _RSTNP_H

INT1 FsStpNpHwInit (VOID);

INT1 FsRstpNpSetPortState (UINT4 u4IfIndex, UINT1 u1Status);
INT1 FsRstNpGetPortState (UINT4 u4IfIndex, UINT1* pu1Status);
VOID FsRstpNpInitHw PROTO((VOID));

#ifdef MBSM_WANTED
INT1 FsRstpMbsmNpSetPortState (UINT4 , UINT1 , tMbsmSlotInfo *); 
INT1 FsPbRstpMbsmNpSetPortState (UINT4 u4IfIndex, tVlanId Svid, 
                                 UINT1 u1Status, 
                                 tMbsmSlotInfo * pSlotInfo);
INT1 FsRstpMbsmNpInitHw (tMbsmSlotInfo * );
#endif

#endif /* _RSTNP_H */
