/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mldsminp.h,v 1.3 2007/03/23 13:16:35 iss Exp $
 *
 * Description: Prototypes for IGMP Snooping module's NP-API.
 *
 *******************************************************************/
#ifndef _MLDSMINP_H
#define _MLDSMINP_H

/* PRIVATE FUNCTIONS - to be used only by NPAPI code */


/* Functions related to Field Processor programming for Firebolt chips */

INT4 FsMiMldsHwEnableMldSnooping PROTO ((UINT4));
INT4 FsMiMldsHwDisableMldSnooping PROTO ((UINT4));
INT4 FsMiMldsHwDestroyMLDFP PROTO ((UINT4));
INT4 FsMiMldsHwDestroyMLDFilter PROTO ((UINT4));

INT4 FsMiNpGetIp6FwdEntryHitBitStatus PROTO ((UINT4, UINT2, UINT1 *, UINT1 *));

INT4 FsMiMldsHwEnableIpMldSnooping PROTO ((UINT4));
INT4 FsMiMldsHwDisableIpMldSnooping PROTO ((UINT4));

INT4 FsMiNpUpdateIp6mcFwdEntries PROTO ((UINT4, tSnoopVlanId, UINT1 *, UINT1 *,
                                         tPortList, tPortList, UINT1));

#ifdef MBSM_WANTED
INT4 FsMiMldsMbsmHwEnableMldSnooping PROTO ((UINT4 u4ContextId, 
                                             tMbsmSlotInfo * pSlotInfo));
INT4 FsMiMldsMbsmHwEnableIpMldSnooping PROTO ((UINT4 u4ContextId, 
                                               tMbsmSlotInfo * pSlotInfo));
#endif

#endif
