/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ethernp.h,v 1.7 2015/05/21 13:57:54 siva Exp $ 
 *
 * Description: Exported file for etherMIB NP-API.
 *
 *******************************************************************/
#ifndef __ETHERNP_H
#define __ETHERNP_H

typedef struct 
{
        FS_UINT8 u8HCAlignErrors;
        FS_UINT8 u8HCFCSErrors;
        FS_UINT8 u8HCIntMacTxErrors;
        FS_UINT8 u8HCFrameTooLongs;
        FS_UINT8 u8HCIntMacRxErrors;
        FS_UINT8 u8HCSymbolErrors;
}tEthHCStats;

typedef struct 
{
        UINT4 u4AlignErrors;
        UINT4 u4FCSErrors;
        UINT4 u4SingleColFrames;
        UINT4 u4MultipleColFrames;
        UINT4 u4SQETestErrors;
        UINT4 u4DeferredTrans;
        UINT4 u4LateCollisions;
        UINT4 u4ExcessiveCols;
        UINT4 u4IntMacTxErrors;
        UINT4 u4CarrierSenseErrors;
        UINT4 u4FrameTooLongs;
        UINT4 u4IntMacRxErrors;
        UINT4 u4SymbolErrors;
        UINT4 u4DuplexStatus;
        UINT4 u4ReqType;
        UINT2 u2RateControlStatus;
        BOOLEAN bRateControlAbility;
        UINT1 u1pad[1];
        tEthHCStats EthHCStats;
}tEthStats;

#define ETH_CNTRL_FNS_SUPPORTED             1
#define ETH_CNTRL_IN_UNK_OPCODES            2
#define ETH_CNTRL_HC_IN_UNK_OPCODES         3

#define ETH_PAUSE_ADMIN_MODE                1
#define ETH_PAUSE_OPER_MODE                 2

#define ETH_PAUSE_IN_FRAMES                 1
#define ETH_PAUSE_OUT_FRAMES                2
#define ETH_HC_PAUSE_IN_FRAMES              3
#define ETH_HC_PAUSE_OUT_FRAMES             4

#define ETH_PAUSE_DISABLED                  1
#define ETH_PAUSE_ENABLED_XMIT              2
#define ETH_PAUSE_ENABLED_RCV               3
#define ETH_PAUSE_ENABLED_XMIT_AND_RCV      4

#define ETHER_RATE_CONTROL_OFF          1
#define ETHER_RATE_CONTROL_ON           2
#define ETHER_RATE_CONTROL_UNKNOWN      3

INT4 FsEtherHwGetStats PROTO ((UINT4 u4dot3StatsIndex, 
                               tEthStats *pEthStats)); 

INT4 FsEtherHwGetCollFreq PROTO ((UINT4 u4ifIndex, 
                        INT4 i4dot3CollCount, 
                        UINT4 *pElement));

INT4 FsEtherHwGetCntrl PROTO ((UINT4 u4dot3StatsIndex, 
                        INT4 *pElement, 
                        INT4 i4Code));

INT4 FsEtherHwSetPauseAdminMode PROTO ((UINT4 u4dot3StatsIndex, 
                        INT4 *pElement));

INT4 FsEtherHwGetPauseMode PROTO ((UINT4 u4dot3StatsIndex, 
                        INT4 *pElement,
                        INT4 i4Code));

INT4 FsEtherHwGetPauseFrames PROTO ((UINT4 u4dot3StatsIndex, 
                        UINT4 *pElement, 
                        INT4 i4Code));

#endif /* __ETHERNP_H */
