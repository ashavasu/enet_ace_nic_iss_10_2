/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id
 *
 * Description: This file contains the function prototypes of PBB NPAPI.
 ****************************************************************************/
#ifndef _PBBNP_H
#define _PBBNP_H

/* Hardware API function prototypes */
INT4
FsMiPbbNpInitHw (VOID);

VOID
FsMiPbbNpDeInitHw (VOID);

INT4
FsMiPbbHwSetVipAttributes (UINT4 u4ContextId, UINT4 u4VipIndex,
                           tVipAttribute VipAttribute);
INT4
FsMiPbbHwDelVipAttributes (UINT4 u4ContextId, 
                           UINT4 u4VipIfIndex);
INT4
FsMiPbbHwSetVipPipMap (UINT4 u4ContextId, UINT4 u4VipIfIndex,
                       tVipPipMap VipPipMap);
INT4
FsMiPbbHwDelVipPipMap (UINT4 u4ContextId, UINT4 u4VipIfIndex);

INT4
FsMiPbbHwSetBackboneServiceInstEntry (UINT4 u4ContextId, UINT4 u4CbpIfIndex, 
                                      UINT4 u4BSid, 
                          tBackboneServiceInstEntry BackboneServiceInstEntry);
INT4
FsMiPbbHwDelBackboneServiceInstEntry (UINT4 u4ContextId, UINT4 u4CbpIfIndex, 
                                      UINT4 u4BSid);
INT4
FsMiBrgHwCreateControlPktFilter (UINT4 u4ContextId, tFilterEntry FilterEntry);
INT4
FsMiBrgHwDeleteControlPktFilter (UINT4 u4ContextId, tFilterEntry FilterEntry);

INT4
FsMiPbbHwSetAllToOneBundlingService (UINT4 u4ContextId, UINT4 u4IfIndex, 
                                     UINT4 u4Isid);
INT4
FsMiPbbHwDelAllToOneBundlingService (UINT4 u4ContextId, UINT4 u4IfIndex, 
                                     UINT4 u4Isid);
INT4
FsMiPbbHwGetPortIsidStats (UINT4 u4ContextId,
                           UINT4 u4IfIndex, 
                           UINT4 u4Isid, 
                           UINT1 u1StatsType, UINT4 *pu4Stats);
INT4
FsMiPbbHwSetPortIsidLckStatus (UINT4 u4ContextId,
                               UINT4 u4IfIndex, 
                               UINT4 u4Isid, 
                               UINT1 u1Lockstatus);
#endif
