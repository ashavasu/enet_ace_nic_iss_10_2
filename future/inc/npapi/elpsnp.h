/*****************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: elpsnp.h
 *
 * Description: This file contains the function prototypes of ELPS NPAPI.
 ****************************************************************************/
#ifndef _ELPSNP_H
#define _ELPSNP_H

/* Hardware API function prototypes */
INT4
FsMiElpsHwPgSwitchDataPath (UINT4 u4ContextId, UINT4 u4PgId,
                            tElpsHwPgSwitchInfo *pPgHwInfo);
#ifdef MBSM_WANTED
INT4
FsMiElpsMbsmHwPgSwitchDataPath (UINT4 u4ContextId, UINT4 u4PgId,
                                tElpsHwPgSwitchInfo *pPgHwInfo,
                                tMbsmSlotInfo *pSlotInfo);
#endif
#endif
