/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ip6mcnp.h,v 1.14 2015/06/08 10:17:00 siva Exp $ 
 *
 * Description: Exported file for IPMC NP-API.
 *
 *******************************************************************/
#ifndef __IP6MCNP_H__
#define __IP6MCNP_H__


#include "fssnmp.h"
#include "ipmcnp.h"

/* Description : Multicast Route Entry information
   stored per route entry */ 

typedef enum 
{
        TRAP_IP6_TO_CPU,     /* 0 */
        ROUTE_IP6,           /* 1 */
        DISCARD_IP6,         /* 2 */
        PASS_TRANSPARENT_IP6 /* 3 */
}tRoute6EntryCommand;

enum eIpv6StatsType
{
   IPV6MROUTE_PKTS = 1,          /* 1 */
   IPV6MROUTE_DIFF_IF_IN_PKTS,   /* 2 */
   IPV6MROUTE_OCTETS,            /* 3 */
   IPV6MROUTE_HCOCTETS,          /* 4 */
   IPV6MNEXTHOP_PKTS,            /* 5 */
   IPV6MIFACE_IN_MCAST_PKTS,     /* 6 */
   IPV6MIFACE_OUT_MCAST_PKTS,    /* 7 */
   IPV6MIFACE_HCIN_MCAST_PKTS,   /* 8 */
   IPV6MIFACE_HCOUT_MCAST_PKTS   /* 9 */
};


typedef struct _Mc6RtEntry 
{
        tRoute6EntryCommand  eCommand;/* Command to act for this entry */
        UINT2 u2ChkRpfFlag    ; /* u2RpfIf is valid if set to TRUE */
        UINT2 u2RpfIf         ; /* Reverse Path Forwarding Interface.*/
        UINT2 u2TtlDecFlag    ; /* TTL is decremented if set to TRUE */
        UINT2 u2VlanId        ; /* Added for igsv3 compatibility */
        UINT2 u2PreVlanId     ; /* Previous Incoming Interface VLAN ID */
        UINT1 u1RouteType     ;
        UINT1 u1pad;
        tPortList McFwdPortList;
        tPortList UntagPortList;
        tPortList VlanEgressPortList;
} tMc6RtEntry;

/* Description :  Donwn Stream Infomration stored 
   per route entry */

typedef tMcDownStreamIf tMc6DownStreamIf;

PUBLIC UINT4 FsNpIpv6McInit (VOID);

PUBLIC  UINT4 FsNpIpv6McDeInit (VOID);

PUBLIC UINT4
FsNpIpv6McAddRouteEntry (UINT4 u4VrId,
                         UINT1 *pGrpAddr,
                         UINT4 u4GrpPrefix,
                         UINT1 *pSrcIpAddr,
                         UINT4 u4SrcIpPrefix,
                         UINT1 u1CallerId,
                         tMc6RtEntry rtEntry,
                         UINT2 u2NoOfDownStreamIf,
                         tMc6DownStreamIf * pDownStreamIf);


PUBLIC VOID FsNpIpv6McClearAllRoutes (UINT4 u4VrId);

PUBLIC UINT4
FsNpIpv6McAddCpuPort (UINT1 u1GenRtrId,
                      UINT1 *pGrpAddr,
                      UINT4 u4GrpPrefix,
                      UINT1 *pSrcIpAddr,
                      UINT4 u4SrcIpPrefix,
                      tMc6RtEntry rtEntry,
                      UINT2 u2NoOfDownStreamIf,
                      tMc6DownStreamIf * pDownStreamIf);

PUBLIC UINT4
FsNpIpv6McDelCpuPort (UINT1 u1GenRtrId, UINT1 *pGrpAddr, UINT4 u4GrpPrefix,
                      UINT1 *pSrcIpAddr, UINT4 u4SrcIpPrefix,
                       tMc6RtEntry rtEntry);

PUBLIC VOID
FsNpIpv6McGetHitStatus (UINT1 *pSrcIpAddr, UINT1 *pGrpAddr,
                        UINT4 u4Iif, UINT2 u2VlanId, UINT4 *pu4HitStatus);

PUBLIC UINT4
FsNpIpv6McDelRouteEntry (UINT4 u4VrId,
                         UINT1 * pGrpAddr,
                         UINT4 u4GrpPrefix,
                         UINT1 *pSrcIpAddr,
                         UINT4 u4SrcIpPrefix,
                         tMc6RtEntry rtEntry,
                         UINT2 u2NoOfDownStreamIf,
                         tMc6DownStreamIf * pDownStreamIf);

PUBLIC VOID
FsNpIpv6McUpdateOifVlanEntry (UINT4 u4VrId, UINT1 *pGrpAddr, UINT4 u4GrpPrefix,
                                 UINT1 *pSrcIpAddr, UINT4 u4SrcIpPrefix,
                              tMc6RtEntry rtEntry, tMc6DownStreamIf downStreamIf);

PUBLIC VOID
FsNpIpv6McUpdateIifVlanEntry (UINT4 u4VrId, UINT1 *pGrpAddr, UINT4 u4GrpPrefix,
                                 UINT1 *pSrcIpAddr, UINT4 u4SrcIpPrefix,
                              tMc6RtEntry rtEntry, tMc6DownStreamIf downStreamIf);

INT4
FsNpIpv6GetMRouteStats (UINT4, UINT1 *, UINT1 *, INT4, UINT4 *);
INT4
FsNpIpv6GetMRouteHCStats (UINT4, UINT1 *, UINT1 *, INT4,
                          tSNMP_COUNTER64_TYPE *);
INT4
FsNpIpv6GetMNextHopStats (UINT4, UINT1 *, UINT1 *, INT4, INT4, UINT4 *);
INT4
FsNpIpv6GetMIfaceStats (UINT4, INT4, INT4, UINT4 *);

INT4
FsNpIpv6GetMIfaceHCStats (UINT4, INT4, INT4, tSNMP_COUNTER64_TYPE *);
INT4
FsNpIpv6SetMIfaceTtlTreshold (UINT4, INT4, INT4);

INT4
FsNpIpv6SetMIfaceRateLimit (UINT4, INT4, INT4);

PUBLIC UINT4
FsPimv6NpInitHw (VOID);

PUBLIC UINT4
FsPimv6NpDeInitHw (VOID);

UINT4
FsNpIpv6McClearHitBit (UINT2 u2VlanId, UINT1 *pSrcIpAddr, UINT1 *pGrpAddr);

#ifdef MBSM_WANTED
PUBLIC UINT4 
FsNpIpv6MbsmMcInit PROTO ((tMbsmSlotInfo * pSlotInfo));
PUBLIC UINT4 
FsNpIpv6MbsmMcAddRouteEntry PROTO ((UINT4 u4VrId, UINT1 * pGrpAddr, UINT4 u4GrpPrefix, UINT1 * pSrcIpAddr, UINT4 u4SrcIpPrefix, UINT1 u1CallerId, tMc6RtEntry rtEntry, UINT2 u2NoOfDownStreamIf, tMc6DownStreamIf * pDownStreamIf, tMbsmSlotInfo * pSlotInfo));
PUBLIC UINT4 
FsPimv6MbsmNpInitHw PROTO ((tMbsmSlotInfo * pSlotInfo));
#endif
#endif /* __IP6MCNP_H__ */
