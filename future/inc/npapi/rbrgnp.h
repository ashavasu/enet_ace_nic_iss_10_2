/****************************************************************************/
/*  Copyright (C) 2010 Aricent Inc . All Rights Reserved                    */
/*  $Id: rbrgnp.h,v 1.1 2012/01/27 13:05:27 siva Exp $                                                   */
/*                                                                          */
/*  FILE NAME             : rbrgnp.h                                        */
/*  PRINCIPAL AUTHOR      : Aricent                                         */
/*  MODULE                : RBridge                                         */
/*  DESCRIPTION           : This file contains the exported NPAPI functions */
/*                          and macros of Aricent RBridge  Module.          */
/****************************************************************************/
#include "lr.h"
#include "iss.h"
#include "npapi.h"
#include "rbridge.h"

PUBLIC INT4 RbrgNpProgramEntry(tRBrgNpEntry *pRBrgEncapInfo);
