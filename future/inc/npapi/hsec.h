/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: hsec.h,v 1.1 2011/06/10 14:01:00 siva Exp $
 *
 * Description: Contains the prototype for the npapai call defined in hsec
 * ****************************************************************************/
#ifndef _HSEC_H_
#define _HSEC_H_

INT4 NpHwUtilFunction PROTO ((UINT1 u1Algorithm, unUtilAlgo * punUtilAlgo));
INT4 NpSecAlgoInit PROTO ((VOID));

#endif
