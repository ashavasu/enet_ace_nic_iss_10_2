/****************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: asnpsim.h,v 1.5 2010/12/20 04:52:26 siva Exp $
 *
 * Description: This file contains mappings of NPAPIs to their
 *              Async counterparts.
 * *************************************************************/

#ifndef _ASNPSIM_H
#define _ASNPSIM_H

#ifdef NPSIM_ASYNC_WANTED

PUBLIC INT1 AsyncNPInit (VOID);

#define PnacHwSetAuthStatus(u2Port, pu1SuppAddr, u1AuthMode, u1AuthStatus, u1CtrlDir)  AsyncPnacHwSetAuthStatus(u2Port, pu1SuppAddr, u1AuthMode, u1AuthStatus, u1CtrlDir)
#define PnacHwAddOrDelMacSess(u2Port, pu1SuppAddr, u1SessStatus)  AsyncPnacHwAddOrDelMacSess(u2Port, pu1SuppAddr, u1SessStatus)
#define FsLaHwCreateAggGroup(u2AggIndex, pu2HwAggIndex)  AsyncFsLaHwCreateAggGroup(u2AggIndex, pu2HwAggIndex)
#define FsLaHwAddLinkToAggGroup(u2AggIndex, u2PortNumber, pu2HwAggIndex)  AsyncFsLaHwAddLinkToAggGroup(u2AggIndex, u2PortNumber, pu2HwAggIndex)
#define FsLaHwSetSelectionPolicy(u2AggIndex, u1SelectionPolicy)  AsyncFsLaHwSetSelectionPolicy(u2AggIndex, u1SelectionPolicy)
#define FsLaHwRemoveLinkFromAggGroup(u2AggIndex, u2PortNumber)  AsyncFsLaHwRemoveLinkFromAggGroup(u2AggIndex, u2PortNumber)
#define FsLaHwDeleteAggregator(u2AggIndex)  AsyncFsLaHwDeleteAggregator(u2AggIndex)
#define FsLaHwEnableCollection(u2AggIndex, u2PortNumber)  AsyncFsLaHwEnableCollection(u2AggIndex, u2PortNumber)
#define FsLaHwEnableDistribution(u2AggIndex, u2PortNumber)  AsyncFsLaHwEnableDistribution(u2AggIndex, u2PortNumber)
#define FsLaHwDisableCollection(u2AggIndex, u2PortNumber)  AsyncFsLaHwDisableCollection(u2AggIndex, u2PortNumber)
#define FsLaHwSetPortChannelStatus(u2AggIndex, u2Inst, u1StpState)  AsyncFsLaHwSetPortChannelStatus(u2AggIndex, u2Inst, u1StpState)
#define FsMiNpUpdateIpmcFwdEntries(u4ContextId, VlanId, u4GrpAddr, u4SrcAddr, PortList, UntagPortList, u1EventType)  AsyncFsMiNpUpdateIpmcFwdEntries(u4ContextId, VlanId, u4GrpAddr, u4SrcAddr, PortList, UntagPortList, u1EventType)
#define FsMiIgsHwUpdateIpmcEntry(u4Instance, pIgsHwIpFwdInfo, u1Action)  AsyncFsMiIgsHwUpdateIpmcEntry(u4Instance, pIgsHwIpFwdInfo, u1Action)
#define FsMiRstpNpSetPortState(u4ContextId, u4IfIndex, u1Status)  AsyncFsMiRstpNpSetPortState(u4ContextId, u4IfIndex, u1Status)
#define FsMiMstpNpSetInstancePortState(u4ContextId, u4IfIndex, u2InstanceId, u1PortState)  AsyncFsMiMstpNpSetInstancePortState(u4ContextId, u4IfIndex, u2InstanceId, u1PortState)
#define FsMiPvrstNpSetVlanPortState(u4ContextId, u4IfIndex, VlanId, u1PortState)  AsyncFsMiPvrstNpSetVlanPortState(u4ContextId, u4IfIndex, VlanId, u1PortState)
#define FsErpsHwRingConfig(pErpsHwRingInfo)  AsyncFsErpsHwRingConfig(pErpsHwRingInfo)

PUBLIC INT4
AsyncFsMiIgsHwUpdateIpmcEntry (UINT4 u4Instance, 
                               tIgsHwIpFwdInfo * pIgsHwIpFwdInfo,
                               UINT1 u1Action);

PUBLIC INT4
AsyncPnacHwSetAuthStatus PROTO ((UINT2   u2Port,
                          UINT1 * pu1SuppAddr,
                          UINT1   u1AuthMode,
                          UINT1   u1AuthStatus,
                          UINT1   u1CtrlDir));

PUBLIC INT4
AsyncPnacHwAddOrDelMacSess PROTO ((UINT2   u2Port,
                            UINT1 * pu1SuppAddr,
                            UINT1   u1SessStatus));

PUBLIC INT4
AsyncFsLaHwCreateAggGroup PROTO ((UINT2 u2AggIndex, UINT2 * pu2HwAggIndex));

PUBLIC INT4
AsyncFsLaHwAddLinkToAggGroup PROTO ((UINT2   u2AggIndex,
                              UINT2   u2PortNumber,
                              UINT2 * pu2HwAggIndex));

PUBLIC INT4
AsyncFsLaHwSetSelectionPolicy PROTO ((UINT2 u2AggIndex, UINT1 u1SelectionPolicy));

PUBLIC INT4
AsyncFsLaHwRemoveLinkFromAggGroup PROTO ((UINT2 u2AggIndex, UINT2 u2PortNumber));

PUBLIC INT4
AsyncFsLaHwDeleteAggregator PROTO ((UINT2 u2AggIndex));

PUBLIC INT4
AsyncFsLaHwEnableCollection PROTO ((UINT2 u2AggIndex, UINT2 u2PortNumber));

PUBLIC INT4
AsyncFsLaHwEnableDistribution PROTO ((UINT2 u2AggIndex, UINT2 u2PortNumber));

PUBLIC INT4
AsyncFsLaHwDisableCollection PROTO ((UINT2 u2AggIndex, UINT2 u2PortNumber));

PUBLIC INT4
AsyncFsLaHwSetPortChannelStatus PROTO ((UINT2 u2AggIndex,
                                 UINT2 u2Inst,
                                 UINT1 u1StpState));

PUBLIC INT4
AsyncFsMiNpUpdateIpmcFwdEntries PROTO ((UINT4        u4ContextId,
                                 tSnoopVlanId VlanId,
                                 UINT4        u4GrpAddr,
                                 UINT4        u4SrcAddr,
                                 tPortList    PortList,
                                 tPortList    UntagPortList,
                                 UINT1        u1EventType));

PUBLIC INT1
AsyncFsMiRstpNpSetPortState PROTO ((UINT4 u4ContextId,
                             UINT4 u4IfIndex,
                             UINT1 u1Status));

PUBLIC INT4
AsyncFsMiMstpNpSetInstancePortState PROTO ((UINT4 u4ContextId,
                                     UINT4 u4IfIndex,
                                     UINT2 u2InstanceId,
                                     UINT1 u1PortState));

PUBLIC INT4
AsyncFsMiPvrstNpSetVlanPortState PROTO ((UINT4   u4ContextId,
                                  UINT4   u4IfIndex,
                                  tVlanId VlanId,
                                  UINT1   u1PortState));

PUBLIC INT4
AsyncFsErpsHwRingConfig PROTO ((tErpsHwRingInfo * pErpsHwRingInfo));

#endif /* NPSIM_ASYNC_WANTED */
#endif /* _ASNPSIM_H */
