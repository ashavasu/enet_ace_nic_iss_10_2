/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: difsrvnp.h,v 1.7 2011/02/09 11:56:49 siva Exp $
 *
 * Description:This file contains prototypes of Driver API                   
 *             interface functions in  DiffServ NP-API.
 ****************************************************************************/
#ifndef _DSNP_H
#define _DSNP_H

#ifdef SWC
                                                  
/* In Profile Action allowed vlaues */            
#define FS_DS_ACTN_INSERT_PRIO                    0x000001
#define FS_DS_ACTN_SET_COS_QUEUE                  0x000002
#define FS_DS_ACTN_INSERT_TOSP                    0x000004
#define FS_DS_ACTN_COPY_TO_CPU                    0x000008
#define FS_DS_ACTN_DO_NOT_SWITCH                  0x000010
#define FS_DS_ACTN_SET_OUT_PORT_UCAST             0x000020
#define FS_DS_ACTN_COPY_TO_MIRROR                 0x000040
#define FS_DS_ACTN_INCR_FFPPKT_COUNTER            0x000080
#define FS_DS_ACTN_INSERT_PRIO_FROM_TOSP          0x000100
#define FS_DS_ACTN_INSERT_TOSP_FROM_PRIO          0x000200
#define FS_DS_ACTN_DROP_PRECEDENCE                0x4000
#define FS_DS_ACTN_INSERT_DSCP                    0x000400
#define FS_DS_ACTN_SET_OUT_PORT_NON_UCAST         0x000800
#define FS_DS_ACTN_DO_SWITCH                      0x002000
#define FS_DS_ACTN_SET_OUT_PORT_ALL               0x8000

#define DS_NP_MAX_RED_STOPPROB   511
#define DS_NP_DEFAULT_TRAFFICCLASS 0

typedef enum {
 DS_NP_PREC_SWITCHED              = 1,
 DS_NP_PREC_ROUTED                = 2
} tDsNpPrecType;

typedef enum {
 DS_NP_CLSFR_TC_DONTCARE          = 0,
 DS_NP_CLSFR_TC_MAP               = 1,
 DS_NP_CLSFR_TC_UNMAP             = 2
} tDsNpClfrTCFlag;

typedef enum {
 DS_NP_PORT_PARAM_SET_MAXBW       = 1,
 DS_NP_PORT_PARAM_SET_DP1         = 2,
 DS_NP_PORT_PARAM_SET_DP2         = 3,
 DS_NP_PORT_PARAM_SET_DP3         = 4,
 DS_NP_PORT_PARAM_SET_REDCURVE    = 5,
 DS_NP_PORT_PARAM_SET_ALL         = 6,
 DS_NP_PORT_PARAM_SET_DEFAULT_ALL = 7
} tDsNpPortParams;

typedef enum {
 DS_NP_TC_PARAM_SET_PORT         = 11,
 DS_NP_TC_PARAM_SET_MAXBW        = 12,
 DS_NP_TC_PARAM_SET_GUARANTEEDBW = 13,
 DS_NP_TC_PARAM_SET_DP1          = 14,
 DS_NP_TC_PARAM_SET_DP2          = 15,
 DS_NP_TC_PARAM_SET_DP3          = 16,
 DS_NP_TC_PARAM_SET_WGTFACTOR    = 17,
 DS_NP_TC_PARAM_SET_WGTSHIFT     = 18,
 DS_NP_TC_PARAM_SET_NUMFLWGRPS   = 19,
 DS_NP_TC_PARAM_SET_REDCURVE     = 20,
 DS_NP_TC_PARAM_SET_ALL          = 21,
 DS_NP_TC_PARAM_SET_DEFAULT_ALL  = 22
} tDsNpTCParams;
  
typedef enum {
 DS_NP_COUNTER_OFFERED_PKTS  = 1,    
 DS_NP_COUNTER_ACCEPTED_PKTS,
 DS_NP_COUNTER_REJECTED_PKTS,
 DS_NP_COUNTER_OFFERED_BYTES,
 DS_NP_COUNTER_ACCEPTED_BYTES,
 DS_NP_COUNTER_REJECTED_BYTES,
 DS_NP_COUNTER_ACCEPT_WRONGPORT_PKTS,
 DS_NP_COUNTER_DISCARD_WRONGPORT_PKTS,
 DS_NP_COUNTER_ACCEPT_FAIR_GROUP_PKTS,
 DS_NP_COUNTER_DISCARD_UNFAIR_GROUP_PKTS,
 DS_NP_COUNTER_DISCARD_DROP_LEVEL2_PKTS,
 DS_NP_COUNTER_DISCARD_DROP_LEVEL3_PKTS,
 DS_NP_COUNTER_ACCEPT_FAIR_TC_PKTS,
 DS_NP_COUNTER_ACCEPT_DONTCARE_PKTS,
 DS_NP_COUNTER_DISCARD_PORT_RED_PKTS,
 DS_NP_COUNTER_DISCARD_TC_RED_PKTS,
 DS_NP_COUNTER_TC_ACCEPT_BYTES,
 DS_NP_COUNTER_TC_DISCARD_BYTES
} tDsNpCounterType;

typedef enum {
 DS_NP_RED_CURVE_ENABLE  = 1,
 DS_NP_RED_CURVE_DISABLE = 2
} tDsNpREDParams;

extern  
INT4
IssHwUpdateFilter (UINT4 u4FilterId, UINT4 u4FilterType, INT1 i1WriteCam,
                   UINT4 u4Value);

INT4 DsHwEnableWfhbdOnAllPorts (VOID);
INT4 DsHwDisableWfhbdOnAllPorts (VOID);

INT4
DsHwSetClassifierAction (tDiffServClfrEntry * pDsClfrEntry,
                          tDiffServMultiFieldClfrEntry *pDsMultiFieldClfrEntry,
                          tDiffServInProfileActionEntry 
                          *pDsInProfileActionEntry,
                          tDsNpClfrTCFlag ClsfrTcFlag);
INT4
DsHwResetClassifierAction (tDiffServClfrEntry * pDsClfrEntry,
                           tDiffServMultiFieldClfrEntry 
                           *pDsMultiFieldClfrEntry);
INT4
DsHwSetIPTOSQPrecedence (UINT4 u4IPTOSQPrec, tDsNpPrecType PrecType);
INT4
DsHwSetDot1PQPrecedence (UINT4 u4Dot1PQPrec, tDsNpPrecType PrecType);
INT4
DsHwSetDSCPQPrioMap (UINT4 u4DSCPValue, UINT4 u4QPriority);
INT4
DsHwSetPortParams (UINT4 u4PortNum, tDiffServPortEntry *pDsPortEntry,
                   tDsNpPortParams u4Flag);
INT4
DsHwSetTCParams (UINT4 u4TrafficClassId, tDiffServTCEntry *pDsTcEntry,
                 tDsNpTCParams u4ParamFlag);
INT4
DsHwSetREDParams (UINT4 u4REDCurveId, tDiffServREDEntry *pDsREDEntry,
                 tDsNpREDParams u4EnableFlag);
INT4
DsHwGetWFHBDCounter (tDsNpCounterType CounterType, UINT4 *pu4Count);
INT4
DsHwGetTCCounter (UINT4 u4TCId, tDsNpCounterType CounterType, 
                  UINT4 *pu4CountLow, UINT4 *pu4CountHigh);


INT4 DsHwInitWfhbd (VOID);
INT4 DsHwInitPriorityScheme (VOID);
INT4 DsHwInitDefaultTrfClasses (VOID);

#elif MRVLLS



INT4
QoSHwInit PROTO ((VOID));


INT4
QoSHwDeInit PROTO ((VOID));


INT4
QosHwSetRegenUserPriority PROTO ((UINT2 u2IfIndex, INT4 i4UserPriority, 
                                  INT4 i4RegenPriority));


INT4
QosHwMapVlanRegenPriority PROTO ((UINT2 u2VlanId,INT4 i4RegenPriority));


INT4
QosHwMapMacRegenPriority PROTO ((UINT2 u2FdbId, tMacAddr MacAddr, 
                          UINT4 u1Priority));

INT4
QosHwMapVlanPriTrafficClass PROTO ((UINT4 u4PriorityTCInPriority,
                                    UINT4 u4TrafficClass));


INT4
QosHwMapDSCPTrafficClass PROTO ((UINT4 u4DSCPValue, UINT4 u4QPriority));


INT4
QosHwSetVlanPriPriority PROTO ((UINT4 u4FsQoSIfIndex, 
                                INT4 i4SetValFsQoSIEEETagPriority));


INT4
QosHwSetIPDSCPPriority PROTO ((UINT4 u4FsQoSIfIndex, 
                               INT4 i4SetValFsQoSIPTagPriority));


INT4
QosHwSetVlanIPPriority PROTO ((UINT4 u4FsQoSIfIndex, 
                               INT4 i4SetValFsQoSIEEEIPTagPriority));


INT4
QosHwSetDAOverride PROTO ((UINT4 u4FsQoSIfIndex, INT4 i4SetValFsQoSDAPriority));


INT4
QosHwSetSAOverride PROTO ((UINT4 u4FsQoSIfIndex, INT4 i4SetValFsQoSSAPriority));


INT4
QosHwSetVIDOverride PROTO ((UINT4 u4FsQoSIfIndex, INT4 i4SetValFsQoSVIDPriority));


INT4
QoSHwSetSchedulingAlgo PROTO ((UINT4 u4SchedulingAlgo)); 

#else

INT4 DsHwInit PROTO ((VOID));

INT4 DsHwClassifierAdd PROTO ((tDiffServClfrEntry *, 
                tDiffServMultiFieldClfrEntry *, 
                tDiffServInProfileActionEntry *,
                tDiffServOutProfileActionEntry *,
                tDiffServMeterEntry *));

INT4 DsHwClassifierDelete PROTO ((tDiffServClfrEntry *,
                      tDiffServMultiFieldClfrEntry * , tDiffServMeterEntry *));

INT4 DsHwClassifierUpdate PROTO ((tDiffServClfrEntry *, 
                tDiffServMultiFieldClfrEntry *, 
                tDiffServInProfileActionEntry *,
                tDiffServOutProfileActionEntry *,
                tDiffServMeterEntry *));

INT4 DsHwSchedulerAdd PROTO ((tDiffServSchedulerEntry *pDsSchedulerEntry));

INT4
DsHwGetCounters PROTO ((INT4 i4DataPid, INT4 i4ClfrId, 
                        tDiffServCounters *DsCounters));

INT4 DsHwChangeCosqScheduleAlgo PROTO ((UINT4, UINT4, UINT4 *, UINT4 *, UINT4 *, UINT4 *));
INT4 DsHwGetCosqBandwidth PROTO ((UINT4, UINT4 *, UINT4 *, UINT4 *));
INT4 DsGetEasyriderStaus PROTO ((VOID));
        
#ifdef MBSM_WANTED
INT4 DsMbsmHwInit            PROTO ((tMbsmSlotInfo *));
INT4 DsMbsmHwClassifierAdd   PROTO ((tDiffServClfrEntry *, 
                              tDiffServMultiFieldClfrEntry *,
                              tDiffServInProfileActionEntry *, 
                              tDiffServOutProfileActionEntry *,
                              tDiffServMeterEntry *, tMbsmSlotInfo *));

INT4 DsMbsmHwSchedulerAdd     PROTO ((tDiffServSchedulerEntry *, 
                                      tMbsmSlotInfo *));
#endif

/* Classifier Flags */                            
#define FS_DS_CLFR_DST_MAC_ADDR                   0x000001
#define FS_DS_CLFR_SRC_MAC_ADDR                   0x000002
#define FS_DS_CLFR_VLAN_TAG                       0x000004
#define FS_DS_CLFR_ETHER_TYPE                     0x000008
#define FS_DS_CLFR_IP_VERSION                     0x000010
#define FS_DS_CLFR_IP_HLEN                        0x000020
#define FS_DS_CLFR_IPV4_CODEPOINT                 0x000040
#define FS_DS_CLFR_IPV4_PROTOCOL                  0x000080
#define FS_DS_CLFR_IPV4_SRC_ADDR                  0x000100
#define FS_DS_CLFR_IPV4_SRC_PREFIX                0x000200
#define FS_DS_CLFR_IPV4_DST_ADDR                  0x000400
#define FS_DS_CLFR_IPV4_DST_PREFIX                0x000800
#define FS_DS_CLFR_IPV4_L4_SRC_PORT_MIN           0x001000
#define FS_DS_CLFR_IPV4_L4_SRC_PORT_MAX           0x002000
#define FS_DS_CLFR_IPV4_L4_DST_PORT_MIN           0x004000
#define FS_DS_CLFR_IPV4_L4_DST_PORT_MAX           0x008000
#define FS_DS_CLFR_IPV4_L4_SRC_PORT               0x010000
#define FS_DS_CLFR_IPV4_L4_SRC_MASK               0x020000
#define FS_DS_CLFR_IPV4_L4_DST_PORT               0x040000
#define FS_DS_CLFR_IPV4_L4_DST_MASK               0x080000
#define FS_DS_CLFR_PKT_FORMAT                     0x100000
#define FS_DS_CLFR_PKT_TO_802DOT3                 0x200000
                                                  
/* In Profile Action allowed vlaues */            
#define FS_DS_ACTN_INSERT_PRIO                    0x000001
#define FS_DS_ACTN_SET_COS_QUEUE                  0x000002
#define FS_DS_ACTN_INSERT_TOSP                    0x000004
#define FS_DS_ACTN_COPY_TO_CPU                    0x000008
#define FS_DS_ACTN_DO_NOT_SWITCH                  0x000010
#define FS_DS_ACTN_SET_OUT_PORT_UCAST             0x000020
#define FS_DS_ACTN_COPY_TO_MIRROR                 0x000040
#define FS_DS_ACTN_INCR_FFPPKT_COUNTER            0x000080
#define FS_DS_ACTN_INSERT_PRIO_FROM_TOSP          0x000100
#define FS_DS_ACTN_INSERT_TOSP_FROM_PRIO          0x000200
#define FS_DS_ACTN_INSERT_DSCP                    0x000400
#define FS_DS_ACTN_SET_OUT_PORT_NON_UCAST         0x000800
#define FS_DS_ACTN_DO_SWITCH                      0x002000
#define FS_DS_ACTN_DROP_PRECEDENCE                0x4000
#define FS_DS_ACTN_SET_OUT_PORT_ALL               0x8000

/* Out Profile Action allowed vlaues */
#define FS_DS_OUT_ACTN_COPY_TO_CPU                0x01
#define FS_DS_OUT_ACTN_DO_NOT_SWITCH              0x02
#define FS_DS_OUT_ACTN_INSERT_DSCP                0x04
#define FS_DS_OUT_ACTN_DO_SWITCH                  0x10
#define FS_DS_OUT_ACTN_DROP_PRECEDENCE            0x11

/* No Match Action allowed vlaues */
#define FS_DS_NM_ACTN_INSERT_PRIO                 0x001
#define FS_DS_NM_ACTN_SET_COS_QUEUE               0x002
#define FS_DS_NM_ACTN_INSERT_TOSP                 0x004
#define FS_DS_NM_ACTN_COPY_TO_CPU                 0x008
#define FS_DS_NM_ACTN_DO_NOT_SWITCH               0x010
#define FS_DS_NM_ACTN_SET_OUT_PORT_UCAST          0x020
#define FS_DS_NM_ACTN_COPY_TO_MIRROR              0x040
#define FS_DS_NM_ACTN_INSERT_PRIO_FROM_TOSP       0x100
#define FS_DS_NM_ACTN_INSERT_TOSP_FROM_PRIO       0x200 
#define FS_DS_NM_ACTN_INSERT_DSCP                 0x400 
#define FS_DS_NM_ACTN_SET_OUT_PORT_NON_UCAST      0x800 
#define FS_DS_NM_ACTN_DROP_PRECEDENCE             0x1000
#define FS_DS_NM_ACTN_SET_OUT_PORT_ALL            0x2000

/* Meter Token size */
#define FS_DS_METER_512_TOKENS                    0x00
#define FS_DS_METER_1024_TOKENS                   0x01
#define FS_DS_METER_2048_TOKENS                   0x02
#define FS_DS_METER_4096_TOKENS                   0x03
#define FS_DS_METER_8192_TOKENS                   0x04
#define FS_DS_METER_16384_TOKENS                  0x05
#define FS_DS_METER_32768_TOKENS                  0x06
#define FS_DS_METER_65536_TOKENS                  0x07

#endif  /* SWC */

#endif  /* _DSNP_H */
