/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ecfmnp.h,v 1.3 2009/09/29 15:57:01 prabuc Exp $
 *
 * Description: Exported file for ECFM NP-API.
 *
 *******************************************************************/
#ifndef __ECFMNP_H
#define __ECFMNP_H

INT4 FsEcfmHwInit PROTO ((VOID));
INT4 FsEcfmHwDeInit PROTO ((VOID));
INT4
FsMiEcfmTransmitDmm (UINT4 u4ContextId, UINT4 u4IfIndex,
                     UINT1 *pu1DmmPdu, UINT2 u2PduLength,
                     tVlanTag VlanTag, UINT1 u1Direction);

INT4
FsMiEcfmTransmitDmr (UINT4 u4ContextId, UINT4 u4IfIndex,
                     UINT1 *pu1DmrPdu, UINT2 u2PduLength,
                     tVlanTag VlanTag, UINT1 u1Direction);
#endif /*__ECFMNP_H*/
