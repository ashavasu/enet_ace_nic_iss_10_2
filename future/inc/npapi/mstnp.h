/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mstnp.h,v 1.7 2011/02/09 11:56:50 siva Exp $ 
 *
 * Description: All prototypes for Network Processor API functions for MSTP 
 * 
 *
 *******************************************************************/
#ifndef _MSTNP_H
#define _MSTNP_H

INT1 FsMstpNpCreateInstance PROTO ((UINT2 u2InstId));

INT1 FsMstpNpAddVlanInstMapping PROTO ((tVlanId VlanId, UINT2 u2InstId));

INT1 FsMstpNpAddVlanListInstMapping PROTO ((UINT1 *pu1VlanList, UINT2 u2InstId,
                                            UINT2 u2NumVlans, 
                                            UINT2 *pu2LastVlan));

INT1 FsMstpNpDeleteInstance PROTO ((UINT2 u2InstId));

INT1 FsMstpNpDelVlanInstMapping PROTO ((tVlanId VlanId, UINT2 u2InstId));

INT1 FsMstpNpDelVlanListInstMapping PROTO ((UINT1 *pu1VlanList, UINT2 u2InstId,
                                            UINT2 u2NumVlans, 
                                            UINT2 *pu2LastVlan));
INT4
FsMstpNpSetInstancePortState PROTO ((UINT4 u4IfIndex, UINT2 u2InstanceId,
                              UINT1 u1PortState));

VOID FsMstpNpInitHw PROTO ((VOID));
VOID FsMstpNpDeInitHw PROTO ((VOID));
INT1 FsMstNpGetPortState (UINT2 u2InstanceId, 
                UINT4 u4IfIndex, UINT1* pu1Status);

#ifdef MBSM_WANTED
INT1 FsMstpMbsmNpCreateInstance PROTO ((UINT2 , tMbsmSlotInfo *));
INT1 FsMstpMbsmNpAddVlanInstMapping PROTO ((tVlanId , UINT2,
                                            tMbsmSlotInfo *));
INT1 FsMstpMbsmNpSetInstancePortState PROTO ((UINT4 , UINT2 , UINT1, 
                                              tMbsmSlotInfo *));
INT1 FsMstpMbsmNpSetVlanPortState PROTO ((UINT4 , tVlanId, UINT1, 
                                          tMbsmSlotInfo *));
INT1 FsMstpMbsmNpInitHw PROTO ((tMbsmSlotInfo *));
#endif

#endif /* _MSTNP_H */
