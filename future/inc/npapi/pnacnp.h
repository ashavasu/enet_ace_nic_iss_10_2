/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id
 *
 * Description: This file contains the function implementations  of FS NP-API.
 ****************************************************************************/
#ifndef _PNACNP_H
#define _PNACNP_H

/* Hardware API function prototypes */

INT4  PnacHwEnable PROTO ((VOID));
INT4  PnacHwDisable PROTO ((VOID));
INT4  PnacHwSetAuthStatus PROTO ((UINT2, UINT1  *, UINT1, UINT1, UINT1));
INT4  PnacHwAddOrDelMacSess PROTO ((UINT2, UINT1  *, UINT1));
INT4  PnacHwStartSessionCounters PROTO ((UINT2, UINT1  *));
INT4  PnacHwGetSessionCounter PROTO ((UINT2, UINT1  *, UINT1, UINT4  *, 
                                      UINT4  *));
INT4  PnacHwStopSessionCounters PROTO ((UINT2 , UINT1  *));
INT4  PnacHwGetAuthStatus (UINT2 , UINT1 *, UINT1 , UINT2 *,UINT2 *); 

#ifdef MBSM_WANTED
INT4 PnacMbsmHwEnable PROTO ((tMbsmSlotInfo *));
INT4 PnacMbsmHwSetAuthStatus PROTO ((UINT2, UINT1 *, UINT1, UINT1, UINT1, 
                              tMbsmSlotInfo *));
#endif

#ifdef L2RED_WANTED
INT4 FsPnacRedHwUpdateDB PROTO ((UINT2, UINT1 *, UINT1, UINT1, UINT1));
#endif
#endif /* _PNACNP_H */
