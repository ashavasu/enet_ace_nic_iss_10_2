/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fmnp.h,v 1.4 2007/02/01 14:52:34 iss Exp $
 *
 * Description: Exported file for FM NP-API.
 *
 *******************************************************************/
#ifndef __FMNP_H
#define __FMNP_H
INT4 FmNpRegisterForFailureIndications PROTO ((VOID));

INT4 NpFmFailureIndicationCallbackFunc PROTO ((UINT4, UINT1, UINT1));

#endif /*__FMNP_H*/
