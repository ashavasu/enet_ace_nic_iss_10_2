/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ip6np.h,v 1.16 2017/11/14 07:31:12 siva Exp $
 *
 * Description: All prototypes for Network Processor API functions done here
 *
 *******************************************************************/
#ifndef _FSIP6NPAPIPROTO_H
#define _FSIP6NPAPIPROTO_H

/* Description : 
 * Structure for Ipv6 tunnel entry which would be used 
 * to pass tunnel information to IPv6 FSNPAPIs
 */


typedef struct _FsNpTunlInfo
{ 
        tIp6Addr      tunlSrc;  /* Tunnel source endpoint address */
 
        tIp6Addr      tunlDst;   /* Tunnel dest endpoint address */

        UINT1         u1TunlType;    /* Type of tunnel - For Fs Code, Tunnel Type
                                      * is as defined in inc/cfa.h. */
        UINT1         u1TunlFlag;        /* Flag Defining whether tunnel
                                      * is unidirectional or bidirectional.
                                      * if 1 = Unidirectional.
                                      * else if 2 = Bidirectional.
                                      */
        UINT1         u1TunlDir;         /* Incase if tunnel is unidirectional
                                      * flag defining it is out going or incoming.
                                      * if 1 = Incoming
                                      * else if 2 = Outgoing.
                                      */
        UINT1         u1Reserved;

} tFsNpTunlInfo;

/* Description : 
 * Structure for Ipv6 Interface entry which would be used 
 * to pass interface information to IPv6 FSNPAPIs
 */

typedef struct _FsNpIntInfo
{
        UINT4       u4PhyIfIndex;    /* Index of vlan for L3 Interface (or) 
                                      * Index of vlan over which tunnel is configured */
        
        UINT4       u4TnlIfIndex;  /* Index of vlan for L3 Interface (or) 
                                      * Index of vlan over which tunnel is configured */
     
        
        UINT1       au1MacAddr[6];
        UINT2       u2VlanId;     
                     
#ifdef TUNNEL_WANTED
        tFsNpTunlInfo   TunlInfo;               
#endif
        UINT4       u4HwIntfId[2];/*To store Hardware IntfId for active and Standby*/
        UINT1       u1IfType;
        UINT1       u1NDProxyOperStatus;
        UINT1       u1RouteCount;
        UINT1       u1Reserved;

} tFsNpIntInfo;

typedef struct _FsNpRouteInfo
{
       UINT4       u4HwIntfId[2];/*To store Hardware IntfId for active and Standby*/
       UINT1       u1RouteCount;/* Updated for ECMP*/
       UINT1       u1NHType;
       UINT1       u1Reserved[2];

}tFsNpRouteInfo;

typedef struct _NpNDCacheInput
{
    UINT4       u4ContextId;
    tIp6Addr    Ip6Addr;
}tNpNDCacheInput;

typedef struct _NpNDCacheOutput
{
    UINT4       u4ContextId;
    tIp6Addr    Ip6Addr;
    UINT4       u4CfaIndex;
    UINT1       u1ReachState;
    INT1        HwAddress[CFA_ENET_ADDR_LEN];
    INT1        i1Pad;
}tNpNDCacheOutput;

typedef struct _NpRtm6Input
{
    UINT4        u4CxtId;   /* Context Id */
    tIp6Addr     Ip6DestAddr; /* Destination address */
    tIp6Addr     Ip6NextHopAddr; /* Next hop address */
}tNpRtm6Input;

typedef struct _NpRtm6Output
{
    UINT4        u4CxtId;   /* Context Id */
    tIp6Addr     Ip6DestAddr; /* Destination address */
    tIp6Addr     Ip6NextHopAddr; /* Next hop address */
}tNpRtm6Output;

/* Prototype for Ipv6 NPAPI functions. */
INT4 FsNpIpv6Init               PROTO ((VOID));

INT4 FsNpIpv6Deinit             PROTO ((VOID));

INT4 FsNpRip6Init               PROTO ((VOID));

INT4 FsNpOspf3Init              PROTO ((VOID));

VOID FsNpOspf3DeInit            PROTO ((VOID));

INT4 FsNpIpv6NeighCacheAdd      PROTO ((UINT4 u4VrId, UINT1 *pu1Ip6Addr,
                                        UINT4 u4IfIndex, UINT1 *pu1HwAddr,
                                        UINT1 u1HwAddrLen, UINT1 u1ReachStatus,
                                        UINT2 u2VlanId));

INT4 FsNpIpv6NeighCacheDel      PROTO ((UINT4 u4VrId, UINT1 *pu1Ip6Addr,
                                        UINT4 u4IfIndex));
UINT1 FsNpIpv6CheckHitOnNDCacheEntry 
                                PROTO ((UINT4 u4VrId, UINT1 *pu1Ip6Addr, 
                                        UINT4 u4IfIndex));

INT4 FsNpIpv6NeighCacheGet  PROTO ((tNpNDCacheInput Nd6NpInParam,
                                    tNpNDCacheOutput *pNd6NpOutParam));

INT4 FsNpIpv6NeighCacheGetNext  PROTO ((tNpNDCacheInput Nd6NpInParam,
                                        tNpNDCacheOutput *pNd6NpOutParam));

INT4 FsNpIpv6UcRouteAdd         PROTO ((UINT4 u4VrId, UINT1 *pu1Ip6Prefix,
                                        UINT1 u1PrefixLen, UINT1 *pu1NextHop,
                                        UINT4 u4NHType, tFsNpIntInfo *pIntInfo));

INT4 FsNpIpv6UcRouteDelete      PROTO ((UINT4 u4VrId, UINT1 *pu1Ip6Prefix,
                                        UINT1 u1PrefixLen, UINT1 *pu1NextHop,
                                        UINT4 u4IfIndex, tFsNpRouteInfo *pRouteInfo));

INT4 FsNpIpv6UcGetRoute (tNpRtm6Input Rtm6NpInParam,
                          tNpRtm6Output *pRtm6NpOutParam);

INT4 FsNpIpv6RtPresentInFastPath PROTO ((UINT4 u4VrId, UINT1 *pu1Ip6Prefix,
                                         UINT1 u1PrefixLen, UINT1 *pu1NextHop, 
                                         UINT4 u4IfIndex));

INT4 FsNpIpv6UcRouting          PROTO ((UINT4 u4VrId, UINT4 u4RoutingStatus));
    
INT4 FsNpIpv6GetStats           PROTO ((UINT4 u4VrId, UINT4 u4IfIndex,
                                        UINT4 u4StatsFlag,
                                        UINT4 *pu4StatsValue));

INT4 FsNpIpv6IntfStatus         PROTO ((UINT4 u4VrId, UINT4 u4IfStatus, 
                                         tFsNpIntInfo *pIntfInfo));

INT4 FsNpIpv6AddrCreate         PROTO ((UINT4 u4VrId, UINT4 u4IfIndex,
                                        UINT4 u4AddrType, UINT1  *pu1Ip6Addr,
                                        UINT1 u1PrefixLen));

INT4 FsNpIpv6AddrDelete         PROTO ((UINT4 u4VrId, UINT4 u4IfIndex,
                                        UINT4 u4AddrType, UINT1  *pu1Ip6Addr,
                                        UINT1 u1PrefixLen));

INT4 FsNpIpv6TunlParamSet       PROTO ((UINT4 u4VrId, UINT4 u4IfIndex,
                                        UINT4 u4TunlType, UINT4 u4SrcAddr,
                                        UINT4 u4DstAddr));

INT4 FsNpIpv6AddMcastMAC        PROTO ((UINT4 u4VrId, UINT4 u4IfIndex,
                                        UINT1 *pu1MacAddr, UINT1 u1MacAddrLen));

INT4 FsNpIpv6DelMcastMAC        PROTO ((UINT4 u4VrId, UINT4 u4IfIndex,
                                        UINT1 *pu1MacAddr, UINT1 u1MacAddrLen));

INT4 FsNpIpv6HandleFdbMacEntryChange PROTO ((UINT1 *pu1L3Nexthop,
                                             UINT4 u4IfIndex,
                                             UINT1  *pu1MacAddr,
                                             UINT4 u4IsRemoved));
VOID  FsNpIpv6ClearNdCacheTable PROTO((VOID));
VOID  FsNpIpv6ClearRouteTable PROTO((VOID));
INT4  FsNpIpv6Enable           PROTO ((UINT4 u4Index,UINT2  u2VlanId));
INT4  FsNpIpv6Disable           PROTO ((UINT4 u4Index,UINT2  u2VlanId));


#define NP_IP6_ADDR_LEN                128


#ifdef MBSM_WANTED
INT4 FsNpMbsmIpv6Init (tMbsmSlotInfo * pSlotInfo);
INT4 FsNpMbsmRip6Init (tMbsmSlotInfo * pSlotInfo);
INT1 FsNpMbsmOspf3Init (tMbsmSlotInfo * pSlotInfo);
INT4 FsNpMbsmIpv6NeighCacheAdd (UINT4 u4VrId, UINT1 *pu1Ip6Addr, 
           UINT4 u4IfIndex, UINT1 *pu1HwAddr, UINT1 u1HwAddrLen, 
           UINT1 u1ReachStatus,UINT2 u2VlanId, tMbsmSlotInfo * pSlotInfo);
INT4 FsNpMbsmIpv6NeighCacheDel (UINT4 u4VrId, UINT1 *pu1Ip6Addr, 
                UINT4 u4IfIndex, tMbsmSlotInfo * pSlotInfo);
INT4 FsNpMbsmIpv6UcRouteAdd (UINT4 u4VrId, UINT1 *pu1Ip6Prefix, 
         UINT1 u1PrefixLen, UINT1 *pu1NextHop, UINT4 u4NHType, 
         tFsNpIntInfo *pIntInfo, tMbsmSlotInfo * pSlotInfo);
INT4 FsNpMbsmIpv6RtPresentInFastPath (UINT4 u4VrId, UINT1 *pu1Ip6Prefix,
                              UINT1 u1PrefixLen, UINT1 *pu1NextHop, 
                              UINT4 u4Index, tMbsmSlotInfo * pSlotInfo);
INT4 FsNpMbsmIpv6IntfCreate (UINT4 u4VrId, UINT4 u4IfIndex, 
                tMbsmSlotInfo * pSlotInfo);
INT4 FsNpMbsmIpv6IntfStatus (UINT4 u4VrId, UINT4 u4IfStatus,
                tFsNpIntInfo *pIntInfo, tMbsmSlotInfo * pSlotInfo);
INT4 FsNpMbsmIpv6L3EntryAdd PROTO ((UINT4 u4VrId, UINT1 *pu1Ip6Addr, UINT1 *pMacAddr,
                                         UINT4 u4IfIndex, UINT1 u1ReachStatus, 
                                         UINT2 u2VlanId, tMbsmSlotInfo * pSlotInfo));

INT4 FsNpMbsmIpv6L3EntryDel PROTO ((UINT4 u4VrId,UINT1 *pu1Ip6Addr, tMbsmSlotInfo * pSlotInfo));

#endif /* MBSM_WANTED*/



/* Macro Definitions. */
#define NP_IP6_FORW_ENABLE              1
#define NP_IP6_FORW_DISABLE             2

#define NH_DIRECT                       1
#define NH_REMOTE                       2
#define NH_SENDTO_CP                    3
#define NH_DISCARD                      4
#define NH_TUNNEL                       5

#define NP_IP6_IF_UP                    1
#define NP_IP6_IF_DOWN                  2
#define NP_IP6_IF_DELETE                3
#define NP_IP6_IF_UPDATE                4

#define NP_IP6_UNI_ADDR                 1
#define NP_IP6_MULTI_ADDR               2

#define NP_STAT_IP6_TOO_BIG_ERROR       1
#define NP_STAT_IP6_IN_DISCARDS         2
#define NP_STAT_IP6_FORW_DATAGRAMS      3
#define NP_STAT_IP6_IN_RECIEVES         4 
#define NP_STAT_IP6_IN_MCAST_RECIEVES   5 
#define NP_STAT_IP6_OUT_MCAST_RECIEVES  6 

#define NP_IPV6_NH_INCOMPLETE           1
#define NP_IPV6_NH_REACHABLE            2
#define NP_IPV6_NH_STALE                3

#ifdef TUNNEL_WANTED
#define NP_IP6_UNI_TUNL 1
#define NP_IP6_BI_TUNL  2

#define NP_IP6_TUNL_IN 1
#define NP_IP6_TUNL_OUT 2
#endif

#endif /* _FSIP6NPAPIPROTO_H */
