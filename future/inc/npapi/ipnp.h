/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipnp.h,v 1.46 2018/02/02 09:47:32 siva Exp $
 *
 * Description: Exported file for IP NP-API.
 *
 *******************************************************************/
#ifndef _IPNP_H
#define _IPNP_H
#include "fsvlan.h"   
#include "bridge.h"   
#include "npapi.h" 
#include "vrrp.h"

typedef UINT2 tNpVlanId;
#ifdef TUNNEL_WANTED
typedef struct _FsNpIp4TunlInfo
{ 
        UINT4         u4tunlSrc;  /* Tunnel source endpoint address */
 
        UINT4         u4tunlDst;   /* Tunnel dest endpoint address */

        UINT1         u1TunlType;    /* Type of tunnel - For Fs Code, Tunnel Type
                                      * is as defined in inc/cfa.h. */
        UINT1         u1TunlFlag;        /* Flag Defining whether tunnel
                                      * is unidirectional or bidirectional.
                                      * if 1 = Unidirectional.
                                      * else if 2 = Bidirectional.
                                      */
        UINT1         u1TunlDir;         /* Incase if tunnel is unidirectional
                                      * flag defining it is out going or incoming.
                                      * if 1 = Incoming
                                      * else if 2 = Outgoing.
                                      */
        UINT1         u1Reserved;

} tFsNpIp4TunlInfo;
#endif
typedef struct _FsNpIp4IntInfo
{
        UINT4       u4PhyIfIndex;    /* Index of vlan for L3 Interface (or) 
                                      * Index of vlan over which tunnel is configured */
        UINT4       u4TnlIfIndex;  /* Index of vlan for L3 Interface (or) 
                                    * Index of vlan over which tunnel is configured */
        tMacAddr       MacAddr;
        UINT2       u2VlanId;     
     
        
        UINT1       u1IfType;
                     
        UINT1       au1Reserved[3];
#ifdef TUNNEL_WANTED
        tFsNpIp4TunlInfo   TunlInfo;               
#endif

} tFsNpIp4IntInfo;

/* Description : 
 * Structure for unicast Ipv4 route entry which would be used 
 * to pass route inormation to IP FSNPAPIs
 */
typedef struct _FsNpNextHopInfo
{ 
        UINT4     u4NextHopGt; /* Ip addr of the NextHop */
        UINT4     u4IfIndex;   /* Index through which NextHop
                                  is reachable */

        UINT4     u4LLIfIndex; /* LLI Index should be used as Index
                                  to program route incase of stacking */
        UINT4       u4HwIntfId[2];/*To store HwIntfId of Active and Standby Node*/
        tNpVlanId  u2VlanId;
        UINT1      u1RtCount;   /* when u1RtCount is  0, will add local route in Hardware (to trap packet to CPU)
                                 when u1RtCount is 1 or more , will add REACHABLE ROUTE entry in Hardware*/
        UINT1      u1RtType;
        UINT1      u1IfType; /* In case of PPP interface type, roaute shall be
                         a       programme with CPU port */
        UINT1       au1Rsvd[3];
        UINT4       u4ContextId; /* VRF ID */
} tFsNpNextHopInfo;

typedef struct _NpArpInParams
{
    UINT4       u4ContextId;
    UINT4       u4IpAddr;
}tNpArpInParams;

typedef struct _NpArpOutParams
{
    UINT4       u4ContextId;
    UINT4       u4IpAddr;
    UINT4       u4CfaIndex;
    INT1        HwAddress[CFA_ENET_ADDR_LEN];
    INT1        i1Pad[2];
}tNpArpOutParams;

/* 
 * Strucutre used for passing the IVR mapping infromation to the 
 * NP layer
 */

typedef struct _tNpIpVlanMappingInfo 
{
        UINT4                                   u4VrtId;        /* Virtual Router Id for the IVR Vlan Mapping */
        UINT4                                   u4IvrIfIndex; /* IVR interface index */
        tVlanId                         IvrVlanId;    /* VLAN ID of the IVR interface index */
        tVlanId                         MappedVlanId; /* VLAN associated to the IVR interface */
        tVlanList                       MappedVlanList; /* List of VLAN associated to the IVR interface */
        tMacAddr      IvrMac;       /* Vlan Interface MAC address  */
        UINT1                                   u1Action;       /* Flag vlaue indicating entry ADD or Remove */ 
        UINT1                                   au1Reserved[1];
}tNpIpVlanMappingInfo;

typedef struct _NpArpInput
{
    UINT4       u4ContextId;
    UINT4       u4CfaIndex;
    UINT4       u4IpAddr;
}tNpArpInput;

typedef struct _NpArpOutput
{
    UINT4       u4ContextId;
    UINT4       u4IpAddr;
    UINT4       u4CfaIndex;
    INT1        HwAddress[CFA_ENET_ADDR_LEN];
    INT1        i1Pad[2];
}tNpArpOutput;

typedef struct _NpRtmInput
{
    UINT4        u4CxtId;   /* Context Id */
    UINT4        u4DestNet; /* Destination Network */
    UINT4        u4DestMask; /* Destination Ip address Mask */
    UINT4        u4NextHop; /* Next Hop Ip address */
    UINT4        u4HwIntfId[2];/*To store Hardware IntfId for active and Standby*/
    UINT1        u1Preference;
    UINT1        au1Pad[3];
}tNpRtmInput;

typedef struct _NpRtmOutput
{
    UINT4        u4CxtId;   /* Context Id */
    UINT4        u4DestNet; /* Destination Network */
    UINT4        u4DestMask; /* Destination Ip address Mask */
    UINT4        u4NextHop; /* Next Hop Ip address */
    UINT4        u4HwIntfId[2];/*To store Hardware IntfId for active and Standby*/
    UINT1        u1Preference;
    UINT1        au1Pad[3];
}tNpRtmOutput;

typedef struct _NpIpInterface
{

    UINT4   u4VrId;        /* VrfId of a L3 Interface */
    UINT4   u4CfaIfIndex;  /* Cfa Interface Index */
    UINT4   u4IpAddr;      /* IPv4 Address */
    UINT4   u4IpSubNetMask;/* Address mask */
    UINT2   u2VlanId;      /* Outer Vlan of L3 Interface */
    UINT2   u2InnerVlan;   /* Inner Vlan of L3 Interface */
    INT1    pu1IfName[CFA_MAX_PORT_NAME_LENGTH]; /* Alias Name of L3 Interface */
    UINT1   au1MacAddr[MAC_ADDR_LEN]; /* Mac Address of L3 Interface */
    UINT1   u1OperStatus;  /* Oper Status of L3 Interface */
    UINT1   au1Pad[1];

}tNpIpInterface;

#define FNP_SET_DEFAULTS_IN_ROUTE_ENTRY(RtEntry)                           \
{                                                                \
        RtEntry.eCommand = ROUTE ;                                    \
                RtEntry.u2ChkRpfFlag = FNP_TRUE ;                             \
                RtEntry.u2TtlDecFlag   = FNP_TRUE ;                           \
}

/* Unicast VRID mappings which will be enabled with the respective 
   Macro'w when VR is enabled in Future IP code */

#define OSPF_VRID                0        
#define IP_VRID                  0
#define RTM_VRID                 0
#define BGP_VRID                 0
#define PIM_VRID                 0        
#define ARP_VRID(u4IfIndex)      0
#define FNP_FORW_ENABLE           1
#define FNP_FORW_DISABLE          2

#define FNP_IP_DROP               0xF

#define FNP_RT_LOCAL              0x01 /* Indicates that the route needs to
                                           be installed in H/W to get the 
                                           packet to CPU */
#ifdef TUNNEL_WANTED
#define NP_IP_UNI_TUNL 1
#define NP_IP_BI_TUNL  2

#define NP_IP_TUNL_IN 1
#define NP_IP_TUNL_OUT 2
#endif

#define NP_IP_IF_UP                    1
#define NP_IP_IF_DELETE                2

/* Initialises Next Hop information */
#define FSNP_INIT_NEXTHOP_INFO(x) MEMSET(&(x), 0, sizeof (tFsNpNextHopInfo))

VOID   FsNpIpInit (VOID);
INT1   FsNpOspfInit (VOID);
VOID   FsNpOspfDeInit (VOID);
INT1   FsNpDhcpSrvInit (VOID);
VOID   FsNpDhcpSrvDeInit (VOID);
INT1   FsNpDhcpRlyInit (VOID);
VOID   FsNpDhcpRlyDeInit (VOID);
INT1   FsNpRipInit (VOID);
VOID   FsNpRipDeInit (VOID);
UINT4               FsNpCreateDummyVlanId (UINT4 u4IfIndex);
UINT4               FsNpDeleteDummyVlanId (UINT4 u4IfIndex);


PUBLIC VOID FsNpIpv4AddUnresolvedIp PROTO ((UINT4 u4IpAddr, 
                                            UINT2 u2VlanId));
PUBLIC UINT4 FsNpGetDummyVlanId (UINT4 u4IfIndex);

UINT4  FsNpIpv4CreateIpInterface (UINT4  u4VrId, UINT1 *pu1IfName, 
                                  UINT4  u4IfIndex, UINT4  u4IpAddr, 
                                  UINT4  u4IpSubnetMask, UINT2 u2VlanId, 
                                  UINT1 *au1MacAddr);
UINT4  FsNpIpv4ModifyIpInterface (UINT4  u4VrId, UINT1 *pu1IfName, 
                                  UINT4  u4IfIndex, UINT4  u4IpAddr, 
                                  UINT4  u4IpSubnetMask, UINT2 u2VlanId, 
                                  UINT1 *au1MacAddr);
UINT4  FsNpIpv4DeleteIpInterface (UINT4, UINT1 *, UINT4, UINT2);
UINT4  FsNpIpv4DeleteSecIpInterface (UINT4, UINT1 *, UINT4, UINT4, UINT2);

UINT4  FsNpIpv4CreateL3IpInterface (tNpIpInterface *L3Interface); 
UINT4  FsNpIpv4ModifyL3IpInterface (tNpIpInterface *L3Interface); 
UINT4  FsNpIpv4DeleteL3IpInterface (tNpIpInterface *L3Interface); 

UINT4 FsNpIpv4UpdateIpInterfaceStatus (UINT4 u4VrId, UINT1 *pu1IfName,
                                       UINT4 u4CfaIfIndex, UINT4 u4IpAddr,
                                       UINT4 u4IpSubNetMask, UINT2 u2VlanId,
                                       UINT1 *au1MacAddr, UINT4 u4Status);
#ifdef OSPF_WANTED
#if defined (LNXIP4_WANTED) && !defined (KERNEL_WANTED)
INT4
FsNpOspfCreateAndDeleteFilter( UINT4 u4CfaIfIndex, UINT1 u1Status);
#endif
#endif

INT4 FsNpIpv4SetForwardingStatus (UINT4 u4VrId, UINT1 u1Status);
INT4 FsNpCfaModifyFilter (UINT1 u1Action, UINT4 u4OpCode);
INT4   FsNpIpv4ClearRouteTable PROTO((VOID));
INT4   FsNpIpv4ClearArpTable PROTO ((VOID));


UINT4 FsNpIpv4UcDelRoute (UINT4 u4VrId, UINT4 u4IpDestAddr,UINT4 u4IpSubNetMask,
                tFsNpNextHopInfo nextHop, int *pi4FreeDefIpB4Del);

INT4 FsNpIpv4UcGetRoute (tNpRtmInput RtmNpInParam,
                          tNpRtmOutput *pRtmNpOutParam);

UINT4  FsNpIpv4ArpAdd (tNpVlanId u2VlanId, UINT4 u4IfIndex,
                       UINT4 u4IpAddr, UINT1 *pMacAddr, 
                       UINT1 *pu1IfName, INT1 i1State, UINT4 *pu4TblFull);

UINT4
FsNpIpv4ArpModify PROTO  ((tNpVlanId u2VlanId, UINT4 u4IfIndex, UINT4 u4PhyIfIndex,
                           UINT4 u4IpAddr, UINT1 *pMacAddr, UINT1 *pu1IfName,
                           INT1 i1State));

INT4 FsNpIpv4ArpGet (tNpArpInput ArpNpInParam, 
                     tNpArpOutput *pArpNpOutParam);

INT4 FsNpIpv4ArpGetNext (tNpArpInput ArpNpInParam, 
                         tNpArpOutput *pArpNpOutParam);

UINT1    FsNpIpv4CheckHitOnArpEntry PROTO ((UINT4 u4IpAddress, 
                                            UINT1 u1NextHopFlag));

UINT4 FsNpIpv4ArpDel PROTO ((UINT4 u4IpAddr, UINT1 *pu1IfName,
                             INT1 i1State));

INT4   FsNpIpv4GetSrcMovedIpAddr PROTO ((UINT4 *pu4IpAddr));

UINT4  FsNpIpv4VrfArpAdd (UINT4 u4VrId, tNpVlanId u2VlanId, UINT4 u4IfIndex,
                       UINT4 u4IpAddr, UINT1 *pMacAddr, 
                       UINT1 *pu1IfName, INT1 i1State, UINT4 *pu4TblFull);
UINT4
FsNpIpv4VrfArpModify PROTO  ((UINT4 u4VrId, tNpVlanId u2VlanId, UINT4 u4IfIndex,
                           UINT4 u4PhyIfIndex, UINT4 u4IpAddr, UINT1 *pMacAddr, UINT1 *pu1IfName,
                           INT1 i1State));
UINT1    FsNpIpv4VrfCheckHitOnArpEntry PROTO ((UINT4 u4VrId, UINT4 u4IpAddress, 
                                            UINT1 u1NextHopFlag));
UINT4 FsNpIpv4VrfArpDel PROTO ((UINT4 u4VrId, UINT4 u4IpAddr, UINT1 *pu1IfName,
                             INT1 i1State));
INT4 FsNpIpv4VrfArpGet (tNpArpInput ArpNpInParam, 
                        tNpArpOutput *pArpNpOutParam);

INT4 FsNpIpv4VrfArpGetNext (tNpArpInput ArpNpInParam, 
                            tNpArpOutput *pArpNpOutParam);

INT4   FsNpIpv4VrfClearArpTable PROTO ((UINT4 u4VrId));
INT4   FsNpIpv4VrfGetSrcMovedIpAddr PROTO ((UINT4 *u4VrfId, UINT4 *pu4IpAddr));

INT4   FsNpIpv4IntfStatus         PROTO ((UINT4 u4VrId, UINT4 u4IfStatus, 
                                         tFsNpIp4IntInfo *pIpIntfInfo));

VOID FsNpIpv4SyncVlanAndL3Info PROTO((VOID));

UINT4 FsNpIpv4UcAddRoute (UINT4  u4VrId, UINT4  u4IpDestAddr, UINT4 u4IpSubNetMask, 
#ifndef NPSIM_WANTED
                          tFsNpNextHopInfo * nextHopInfo,
#else
                          tFsNpNextHopInfo  nextHopInfo,
#endif
                          UINT1 *pbu1TblFull);

UINT4 FsNpIpv4VrrpIntfCreateWr (tNpVlanId u2VlanId, UINT4 u4IfIndex, UINT4 u4IpAddr,UINT1 *pau1MacAddr);
UINT4 FsNpIpv4VrrpIntfDeleteWr (tNpVlanId u2VlanId, UINT4 u4IfIndex, UINT4 u4IpAddr, UINT1 *pau1MacAddr);
UINT4 FsNpIpv4CreateVrrpInterface (tNpVlanId u2VlanId,UINT4 u4IpAddr,UINT1 *au1MacAddr);
UINT4 FsNpIpv4DeleteVrrpInterface (tNpVlanId u2VlanId,UINT4 u4IpAddr, UINT1 *au1MacAddr);
UINT4 FsNpIpv4GetVrrpInterface (INT4 i4IfIndex, INT4 i4VrId);
INT4  FsNpIpv4VrrpInstallFilter (VOID);
INT4 FsNpIpv4VrrpRemoveFilter (VOID);
INT4 FsNpVrrpHwProgram (UINT1 u1NpAction, tVrrpNwIntf *pVrrpNwIntf);
INT4 FsMbsmVrrpHwProgram(tMbsmSlotInfo *pSlotInfo, tVrrpNwIntf *pVrrpNwIntface);

#ifdef ISIS_WANTED
UINT1 FsNpIsisHwProgram (UINT1 u1Status);
#endif /* ISIS_WANTED */

UINT4 FsNpIpv4IsRtPresentInFastPath (UINT4 u4DestAddr, UINT4 u4Mask);

PUBLIC UINT4 FsNpIpv4GetStats (INT4 i4StatType, UINT4 *pu4RetVal);

UINT4 FsNpIpv4VrmEnableVr (UINT4 u4VrId, UINT1 u1Status); 

UINT4 FsNpIpv4BindIfToVrId (UINT4 u4IfIndex, UINT4 u4VrId );

UINT4 FsNpIpv4GetNextHopInfo (UINT4 u4VrId, UINT4 u4Dest, UINT4 u4DestMask,
                tFsNpNextHopInfo *pNextHopInfo);

UINT4 FsNpIpv4UcAddTrap (UINT4 u4VrId, UINT4 u4Dest, UINT4 u4DestMask,
                tFsNpNextHopInfo nextHopInfo);

UINT4 FsNpIpv4ClearFowardingTbl (UINT4 u4VrId);

INT4 FsNpIpv4MapVlansToIpInterface(tNpIpVlanMappingInfo *pPvlanMappingInfo);

#ifdef MBSM_WANTED
VOID  FsNpIpv4UcModifyEgress(UINT1 u1RmMsg);
UINT4  FsNpMbsmIpv4CreateIpInterface (UINT4  u4VrId, UINT4  u4IfIndex, 
                                      UINT4  u4IpAddr, UINT4  u4IpSubnetMask, 
                                      UINT2 u2VlanId, UINT1 *au1MacAddr, 
                                      tMbsmSlotInfo *pSlotInfo);
VOID   FsNpMbsmIpInit (tMbsmSlotInfo *);
INT1   FsNpMbsmDhcpSrvInit (tMbsmSlotInfo *);
INT1   FsNpMbsmDhcpRlyInit (tMbsmSlotInfo *);
INT1   FsNpMbsmOspfInit (tMbsmSlotInfo *);
UINT1  FsNpMbsmIsisHwProgram (tMbsmSlotInfo *, UINT1 u1Status);

UINT4  FsNpMbsmIpv4ArpAdd (tNpVlanId u2VlanId, UINT4 u4IpAddr,
                           UINT1 *pMacAddr, UINT1 *pbu1TblFull,
                           tMbsmSlotInfo *);
UINT4  FsNpMbsmIpv4VrfArpAdd (UINT4 u4VrId, tNpVlanId u2VlanId, UINT4 u4IpAddr,
                           UINT1 *pMacAddr, UINT1 *pbu1TblFull,
                           tMbsmSlotInfo *);

UINT4 FsNpMbsmIpv4ArpAddition (tNpVlanId u2VlanId, UINT4 u4IpAddr, 
                               UINT1 *pMacAddr, UINT1 *pbu1TblFull,
                               UINT4 u4IfIndex, UINT1 *pu1IfName,
                               INT1 i1State, tMbsmSlotInfo * pSlotInfo);
UINT4 FsNpMbsmIpv4VrfArpAddition (UINT4 u4VrId, tNpVlanId u2VlanId, UINT4 u4IpAddr,
                                  UINT1 *pMacAddr, UINT1 *pbu1TblFull,
                                  UINT4 u4IfIndex, UINT1 *pu1IfName,
                                  INT1 i1State, tMbsmSlotInfo * pSlotInfo);

UINT4 FsNpMbsmIpv4VrmEnableVr (UINT4 u4VrId, UINT1 u1Status, tMbsmSlotInfo *); 

UINT4 FsNpMbsmIpv4BindIfToVrId (UINT4 u4IfIndex, UINT4 u4VrId, tMbsmSlotInfo *);

UINT4 FsNpMbsmIpv4UcAddRoute (UINT4  u4VrId, UINT4  u4IpDestAddr, UINT4 u4IpSubNetMask,
#ifndef NPSIM_WANTED
                              tFsNpNextHopInfo * nextHopInfo,
#else
                              tFsNpNextHopInfo  nextHopInfo,
#endif                              
                              UINT1 *pbu1TblFull, tMbsmSlotInfo *);

UINT4 FsNpMbsmIpv4UcAddTrap (UINT4 u4VrId, UINT4 u4Dest, UINT4 u4DestMask,
                tFsNpNextHopInfo nextHopInfo, tMbsmSlotInfo *);

#ifdef VRRP_WANTED
UINT4 FsNpMbsmIpv4CreateVrrpInterface (tNpVlanId u2VlanId, UINT4 u4IpAddr,
                                       UINT1 *au1MacAddr, tMbsmSlotInfo *);
INT4  FsNpMbsmIpv4VrrpInstallFilter (tMbsmSlotInfo *);
#endif

INT4 FsNpMbsmIpv4MapVlansToIpInterface( 
                tNpIpVlanMappingInfo *pPvlanMappingInfo, tMbsmSlotInfo *pSlotInfo);
#endif

#ifdef NAT_WANTED
VOID FsNpNatDisableOnIntf (INT4 i4Intf);
INT4 FsNpNatEnableOnIntf (INT4 i4Intf);
#endif /*NAT_WANTED */


#ifdef SNTP_WANTED
UINT4 FsSntpStatusInit (INT4 i4SntpStatus);
#endif
#ifdef NPAPI_WANTED
INT4
FsNpOspfCreateAndDeleteFilter(UINT4 u4CfaIfIndex, UINT1 u1Status);
#endif


#endif

