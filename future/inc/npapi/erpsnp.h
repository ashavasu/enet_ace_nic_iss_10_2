/**************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: erpsnp.h,v 1.2 2010/04/01 14:12:10 prabuc Exp $ 
 *
 * Description: All prototypes for Network Processor API functions for ERPS 
 *
 **************************************************************************/
#ifndef _ERPSNP_H
#define _ERPSNP_H

INT4 FsErpsHwRingConfig (tErpsHwRingInfo *pErpsHwRingInfo);

#ifdef MBSM_WANTED
INT4 FsErpsMbsmHwRingConfig (tErpsHwRingInfo *pErpsHwRingInfo, 
                             tErpsMbsmInfo * pErpsMbsmInfo);
#endif

#endif /*_ERPSNP_H*/
