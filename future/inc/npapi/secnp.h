/*****************************************************************************/
/*  FILE NAME             : secnp.h                                          */
/*                                                                           */
/*  $Id: secnp.h,v 1.1 2014/11/05 13:06:08 siva Exp $                                                    */
/*                                                                           */
/*  DESCRIPTION           : This file contains security related              */
/*                          npapi macros,prototypes,etc                      */
/*****************************************************************************/

#ifndef _SECNP_H_
#define _SECNP_H_

INT4
FsSecNpHwSetRateLimit (UINT1 u1Module, INT4 i4Status);

#endif
