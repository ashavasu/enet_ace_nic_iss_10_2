/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mldsnp.h,v 1.3 2007/02/01 14:52:34 iss Exp $
 *
 * Description: Prototypes for IGMP Snooping module's NP-API.
 *
 *******************************************************************/
#ifndef _MLDSNP_H
#define _MLDSNP_H

/* PRIVATE FUNCTIONS - to be used only by NPAPI code */


/* Functions related to Field Processor programming for Firebolt chips */

INT4 FsMldsHwEnableMldSnooping PROTO ((VOID));
INT4 FsMldsHwDisableMldSnooping PROTO ((VOID));
INT4 FsMldsHwDestroyMLDFP PROTO ((VOID));
INT4 FsMldsHwDestroyMLDFilter PROTO ((VOID));

INT4 FsNpGetIp6FwdEntryHitBitStatus PROTO ((UINT2, UINT1 *, UINT1 *));

INT4 FsMldsHwEnableIpMldSnooping PROTO ((VOID));
INT4 FsMldsHwDisableIpMldSnooping PROTO ((VOID));

INT4 FsNpUpdateIp6mcFwdEntries PROTO ((tSnoopVlanId, UINT1 *, UINT1 *, tPortList, tPortList, UINT1));

#ifdef MBSM_WANTED
INT4 FsMldsMbsmHwEnableMldSnooping PROTO ((tMbsmSlotInfo * pSlotInfo));
INT4 FsMldsMbsmHwEnableIpMldSnooping PROTO ((tMbsmSlotInfo * pSlotInfo));
#endif

#endif
