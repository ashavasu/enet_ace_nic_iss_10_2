/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mldnp.h,v 1.4 2014/12/10 12:55:37 siva Exp $
 *
 * Description: Prototypes for IGMP Snooping module's NP-API.
 *
 *******************************************************************/
#ifndef _MLDNP_H
#define _MLDNP_H

/* PRIVATE FUNCTIONS - to be used only by NPAPI code */


/* Functions related to Field Processor programming for Firebolt chips */

INT4 FsMldHwEnableMld PROTO ((VOID));
INT4 FsMldHwDisableMld PROTO ((VOID));
INT4 FsMldHwDestroyMLDFP PROTO ((VOID));
INT4 FsMldHwDestroyMLDFilter PROTO ((VOID));
INT4 FsNpIpv6SetMldIfaceJoinRate PROTO ((INT4, INT4));

#ifdef MBSM_WANTED
INT4 FsMldMbsmHwEnableMld PROTO ((tMbsmSlotInfo * pSlotInfo));
#endif

#endif
