/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipmcnp.h,v 1.21 2017/04/10 14:20:22 siva Exp $
 *
 * Description: Exported file for IPMC NP-API.
 *
 *******************************************************************/
#ifndef __IPMCNP_H__
#define __IPMCNP_H__

#include "mbsm.h"
#include "utilipvx.h"
#include "pim.h"
#include "snp.h"
/* Description: Enumeration for the different type
   of Network Processor Actions that can be performed
   on a matching route */
typedef enum 
{
        TRAP_TO_CPU,     /* 0 */
        ROUTE,           /* 1 */
        DISCARD,         /* 2 */
        PASS_TRANSPARENT /* 3 */
}tRouteEntryCommand;

enum eIpv4StatsType
{
   IPV4MROUTE_PKTS = 1,         /* 1 */ 
   IPV4MROUTE_DIFF_IF_IN_PKTS,  /* 2 */
   IPV4MROUTE_OCTETS,           /* 3 */
   IPV4MROUTE_HCOCTETS,         /* 4 */
   IPV4MNEXTHOP_PKTS,           /* 5 */
   IPV4MIFACE_IN_MCAST_PKTS,    /* 6 */
   IPV4MIFACE_OUT_MCAST_PKTS,   /* 7 */
   IPV4MIFACE_HCIN_MCAST_PKTS,  /* 8 */
   IPV4MIFACE_HCOUT_MCAST_PKTS  /* 9 */
};

typedef struct _McUpStreamIf 
{
        UINT4  u4IfIndex;      /* Interface index            */ 
        UINT4  u4Mtu;          /* MTU for the given interface*/
        UINT2  u2TtlThreshold; /* TTL Threshold value        */
        UINT2  u2VlanId;
        tPortList McFwdPortList;
        tPortList UntagPortList;
        tPortList VlanEgressPortList;
} tMcUpStreamIf;
/* Description : Multicast Route Entry information
   stored per route entry */ 
typedef struct _McRtEntry 
{
        tRouteEntryCommand  eCommand;/* Command to act for this entry */
 tMcUpStreamIf * pUpStreamIf;     /* upstream interface */
        UINT2 u2ChkRpfFlag    ; /* u2RpfIf is valid if set to TRUE */
        UINT2 u2RpfIf         ; /* Reverse Path Forwarding Interface.*/
   
        UINT2 u2TtlDecFlag    ; /* TTL is decremented if set to TRUE */
        UINT2 u2VlanId        ; /* Added for igsv3 compatibility */

        UINT2 u2PreVlanId     ; /* Previous Incoming Interface VLAN ID */
        UINT1 u1RouteType     ; /* Added for STAR,G porting */
        UINT1 u1PimMode  ; /* Added for Pim Bidir Mode */

        tSnoopTag VlanId        ; /* Vlan array. VlanId[0] - Svlan, VlanId[1] - Cvlan */
 UINT2 u2NoOfUpStreamIf;          /* No of Upstream interfaces */
        UINT1 u1CallerId;
        UINT1 u1Pad[1];
   
        tPortList McFwdPortList;
        tPortList UntagPortList;
        tPortList VlanEgressPortList;
} tMcRtEntry ;

/* Description :  Donwn Stream Infomration stored 
   per route entry */
typedef struct _McDownStreamIf 
{
        UINT4  u4IfIndex;      /* Interface index            */ 
        UINT4  u4Mtu;          /* MTU for the given interface*/
        UINT2  u2TtlThreshold; /* TTL Threshold value        */
        UINT2  u2VlanId;
        tPortList McFwdPortList;
        tPortList UntagPortList;
        tPortList VlanEgressPortList;
} tMcDownStreamIf;

typedef struct _NpL3McastEntry
{
       UINT4 u4VrId;
       UINT4 au4OifList[PIMSM_DFL_MAX_INTERFACE];
       UINT4 u4RpfIf;
       UINT1 au1GrpAddr[IPVX_MAX_INET_ADDR_LEN];
       UINT1 au1SrcAddr[IPVX_MAX_INET_ADDR_LEN];
       UINT2 u2OifCnt;
       UINT2 u2VlanId;
       UINT1 u1Afi;
       UINT1 u1CpuPortPresent;
       UINT1 au1Pad[2];
}tNpL3McastEntry;

typedef struct _Ipv4McRouteInfo
{
    tMcRtEntry rtEntry;              /*contains Route parameters like RPF
                                      *interface and flags                   */ 
    tMcDownStreamIf * pDownStreamIf; /*downstream interface                  */
    UINT4 u4VrId;                    /*Virtual Router ID;0 when not supported*/
    UINT4 u4GrpAddr;                 /*multicast group address               */
    UINT4 u4GrpPrefix;               /*Group Prefix                          */
    UINT4 u4SrcIpAddr;               /*Source Address                        */
    UINT4 u4SrcIpPrefix;             /*Source Prefix                         */ 
    UINT2 u2NoOfDownStreamIf;        /*No. of downstream interfaces          */
    UINT1 u1CallerId;                /*protocol updating the multicast route 
                                      * IPMC_MRP/IPMC_IGS/IPMC_MLDS          */
    UINT1 u1Action;                 /* Action for Add/Del to Iif*/
} tIpv4McRouteInfo;

typedef struct _McRpfDFInfo
{
    tIPvXAddr *pIpvXGrpAddr;     /* Group Address List */ 
    UINT4 u4RpAddr;       /* Elected RP Address */
    INT4 i4RpfIndex;      /* Rpf Index */
    INT4 i4DFIndex;      /* DF Index */
    UINT2 u2NoOfGrps;      /* No of Group Entries */
    UINT1 u1Action;      /* Pim RPF Action*/
    UINT1 u1Padding;
} tMcRpfDFInfo;

/* The structure combining both Routerport and Port channel Index (LAG)  info */
typedef struct
{
    UINT4 u4RportIdx; /*L3 interface [router port] */
    UINT4 u4LaPortIdx; /*Port channel [LAGG]*/
} tRPortInfo;

PUBLIC UINT4        FsNpIpv4McInit PROTO ((VOID));
PUBLIC UINT4        FsNpIpv4McDeInit PROTO ((VOID));

PUBLIC UINT4        FsNpIpv4VlanMcInit PROTO ((UINT4));
PUBLIC UINT4        FsNpIpv4VlanMcDeInit PROTO ((UINT4));

#ifdef PIM_WANTED
PUBLIC UINT4 FsPimNpInitHw (VOID);
PUBLIC UINT4 FsPimNpDeInitHw (VOID);
#endif

#ifdef DVMRP_WANTED
PUBLIC UINT4 FsDvmrpNpInitHw (VOID);
PUBLIC UINT4 FsDvmrpNpDeInitHw (VOID);
#endif

#ifdef IGMPPRXY_WANTED
PUBLIC UINT4 FsNpIgmpProxyInit (VOID);
PUBLIC UINT4 FsNpIgmpProxyDeInit (VOID);
#endif


PUBLIC UINT4 FsNpIpv4McAddRouteEntry (UINT4 u4VrId, UINT4 u4GrpAddr,
                                      UINT4 u4GrpPrefix, UINT4 u4SrcIpAddr,
                                      UINT4 u4SrcIpPrefix,UINT1 u1CallerId,
                                      tMcRtEntry rtEntry,
                                      UINT2 u2NoOfDownStreamIf,
                                      tMcDownStreamIf * pDownStreamIf);

PUBLIC UINT4
FsNpIpv4McDelRouteEntry (UINT4 u4VrId, UINT4 u4GrpAddr,
                         UINT4 u4GrpPrefix, UINT4 u4SrcIpAddr,
                         UINT4 u4SrcIpPrefix, tMcRtEntry rtEntry,
                         UINT2 u2NoOfDownStreamIf, 
                         tMcDownStreamIf *pDownStreamIf);

PUBLIC VOID FsNpIpv4McClearAllRoutes (UINT4 u4VrId);

UINT4 FsNpIpv4McAddCpuPort (UINT2 u2GenRtrId,UINT4 u4GrpAddr,
                UINT4 u4SrcAddr, tMcRtEntry rtEntry);

UINT4 FsNpIpv4McDelCpuPort (UINT2 u2GenRtrId,UINT4 u4GrpAddr,
                UINT4 u4SrcAddr, tMcRtEntry rtEntry);

VOID   FsNpIpv4McGetHitStatus (UINT4, UINT4, UINT4, UINT2, UINT4 *);

VOID
FsNpIpv4McUpdateIifVlanEntry (UINT4 u4VrId, UINT4 u4GrpAddr,
                              UINT4 u4SrcIpAddr, tMcRtEntry rtEntry);
VOID
FsNpIpv4McUpdateOifVlanEntry (UINT4 u4VrId, UINT4 u4GrpAddr, UINT4 u4SrcIpAddr,
                              tMcRtEntry rtEntry, tMcDownStreamIf downStreamIf);

INT4 
FsNpIpv4GetMRouteStats (UINT4 u4VrId, UINT4 u4GrpAddr, UINT4 u4SrcAddr,
                        INT4 i4StatType, UINT4 *pi4RetValue);
INT4
FsNpIpv4GetMRouteHCStats (UINT4 u4VrId, UINT4 u4GrpAddr, UINT4 u4SrcAddr,
                          INT4 i4StatType, tSNMP_COUNTER64_TYPE *pi8RetValue);
INT4
FsNpIpv4GetMNextHopStats (UINT4 u4VrId, UINT4 u4GrpAddr, UINT4 u4SrcAddr,
                        INT4 i4OutIfIndex, INT4 i4StatType, UINT4 *pi4RetValue);

INT4
FsNpIpv4GetMIfaceStats (UINT4 u4VrId, INT4 i4IfIndex, INT4 i4StatType,
                        UINT4 *pi4RetValue);

INT4
FsNpIpv4GetMIfaceHCStats (UINT4 u4VrId, INT4 i4IfIndex, INT4 i4StatType,
                          tSNMP_COUNTER64_TYPE *pi8RetValue);
INT4
FsNpIpv4SetMIfaceTtlTreshold (UINT4 u4VrId, INT4 i4IfIndex, INT4 i4TtlTreshold);

INT4
FsNpIpv4SetMIfaceRateLimit (UINT4 u4VrId, INT4 i4IfIndex, INT4 i4RateLimit);

PUBLIC INT4
FsNpIpvXHwGetMcastEntry ( tNpL3McastEntry  *pNpL3McastEntry);

PUBLIC UINT4 FsNpIpv4McRpfDFInfo(tMcRpfDFInfo *pMcRpfDFInfo );

/*For Router port and LAG */
PUBLIC UINT4  FsNpIpv4RportMcInit  (tRPortInfo PortInfo);
PUBLIC UINT4  FsNpIpv4RportMcDeInit (tRPortInfo PortInfo); 

#ifdef MBSM_WANTED
#ifdef PIM_WANTED
PUBLIC UINT4 FsPimMbsmNpInitHw (tMbsmSlotInfo * pSlotInfo);
#endif /* PIM_WANTED */
#ifdef DVMRP_WANTED
PUBLIC UINT4 FsDvmrpMbsmNpInitHw (tMbsmSlotInfo * pSlotInfo);
#endif /* DVMRP_WANTED */

UINT4 FsNpIpv4MbsmMcAddRouteEntry (UINT4, UINT4, UINT4 , UINT4 , UINT4 , UINT1, 
                                   tMcRtEntry , UINT2 , tMcDownStreamIf *,
                                   tMbsmSlotInfo *);
#endif

UINT4 FsNpIpv4McClearHitBit (UINT2 u2VlanId, UINT4 u4GrpAddr,
                                    UINT4 u4SrcAddr);
PUBLIC UINT4
FsNpIpv4MbsmMcInit (tMbsmSlotInfo * pSlotInfo);
#endif /* __IPMCNP_H__ */
