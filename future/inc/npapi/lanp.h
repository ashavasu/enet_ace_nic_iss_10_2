/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: lanp.h,v 1.13 2014/06/13 12:55:52 siva Exp $
 *
 * Description: This file contains the function implementations  of FS NP-API.
 ****************************************************************************/
#ifndef _LANP_H
#define _LANP_H

/* This struct is used for getting the ports in an aggregation
 * and to compare that with that of the information in sw. */
typedef struct _LaHwAggInfo
{  
    /* The list of logical ports attached to this aggregator */
    UINT4               au4LPortList[LA_MAX_PORTS_PER_AGG];
    UINT4               u4SelPolicyBitList;
    /* The Hw Aggregator Index */
    UINT2               u2HwAggIdx;

    /* The Aggregator Index given by Software */
    UINT1               u1SelPolicy;
    UINT1               u1AggStatus;
    /* The Aggregator Status, possible values ENABLED/DISABLED */
    /* The number of ports attached to this aggregator */
    UINT1               u1NumPorts;

    INT1                i1RetVal;
    UINT1               au1Reserved[2];
}tLaHwAggInfo;

typedef tLaHwAggInfo   tLaHwInfo;

INT4 FsLaHwCreateAggGroup(UINT2 u2AggIndex, UINT2 *pu2HwAggIndex);     
INT4 FsLaHwAddLinkToAggGroup (UINT2 u2AggIndex, UINT2 u2PortNumber, 
                              UINT2 *pu2HwAggIndex);

INT4 FsLaHwDlagAddLinkToAggGroup (UINT2 u2AggIndex, UINT2 u2PortNumber, 
                              UINT2 *pu2HwAggIndex);

INT4 FsLaHwSetSelectionPolicy PROTO ((UINT2 u2AggIndex, 
                                      UINT1 u1SelectionPolicy));
INT4 FsLaHwSetSelectionPolicyBitList PROTO ((UINT2 u2AggIndex, 
                                      UINT4 u4SelectionPolicyBitList));
INT4 FsLaHwRemoveLinkFromAggGroup PROTO ((UINT2 u2AggIndex, 
                                          UINT2 u2PortNumber));

INT4 FsLaHwDlagRemoveLinkFromAggGroup PROTO ((UINT2 u2AggIndex, 
                                          UINT2 u2PortNumber));
INT4 FsLaHwDeleteAggregator PROTO ((UINT2 u2AggIndex));
INT4 FsLaHwDeInit PROTO ((VOID));
INT4 FsLaHwInit PROTO ((VOID));
INT4 FsLaHwEnableCollection PROTO ((UINT2, UINT2));
INT4 FsLaHwEnableDistribution PROTO ((UINT2, UINT2));
INT4 FsLaHwDisableCollection PROTO ((UINT2, UINT2));
INT4 FsLaHwSetPortChannelStatus PROTO ((UINT2, UINT2, UINT1));

INT4
FsLaHwAddPortToConfAggGroup    PROTO ((UINT2 u2AggIndex, 
                                       UINT2 u2PortNumber));

INT4
FsLaHwRemovePortFromConfAggGroup PROTO ((UINT2 u2AggIndex, 
                                         UINT2 u2PortNumber));

INT4
FsLaHwDlagStatus PROTO ((UINT1 u1Status)); 

INT4 FsLaHwCleanAndDeleteAggregator (UINT2 u2HwAggIndex);
INT4 FsLaGetNextAggregator (INT4 i4HwAggIndex, tLaHwInfo  *pHwInfo);
INT4 FsLaRedHwInit (VOID);
INT4 FsLaRedHwNpDeleteAggregator (UINT2 u2AggIndex);
INT4 FsLaRedHwUpdateDBForAggregator (UINT2 u2AggIndex ,
                                     tPortList ConfigPorts,
                                     tPortList ActivePorts,
                                     UINT1 u1SelectionPolicy);
INT4 FsLaRedHwNpUpdateDlfMcIpmcPort (UINT2 u2AggIndex);
#ifdef MBSM_WANTED
INT4 FsLaMbsmHwInit (tMbsmSlotInfo *);
INT4 FsLaMbsmHwCreateAggGroup (UINT2 , tMbsmSlotInfo *);
INT4 FsLaMbsmHwAddPortToConfAggGroup (UINT2 , UINT2 , tMbsmSlotInfo *);
INT4 FsLaMbsmHwAddLinkToAggGroup (UINT2 , UINT2 , tMbsmSlotInfo *);
INT4 FsLaMbsmHwSetSelectionPolicy (UINT2 , UINT1 , tMbsmSlotInfo *);
#endif

#endif /* _LANP_H */
