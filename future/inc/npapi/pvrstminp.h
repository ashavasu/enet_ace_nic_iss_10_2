/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pvrstminp.h,v 1.4 2011/02/09 11:56:49 siva Exp $ 
 *
 * Description: All prototypes for Network Processor API functions for PVRST 
 * 
 *
 *******************************************************************/
#ifndef _PVRSTMINP_H
#define _PVRSTMINP_H

INT1 FsMiPvrstNpCreateVlanSpanningTree PROTO ((UINT4 u4ContextId, tVlanId VlanId));
INT1 FsMiPvrstNpDeleteVlanSpanningTree PROTO ((UINT4 u4ContextId, tVlanId VlanId));
INT4
FsMiPvrstNpSetVlanPortState PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, 
                                    tVlanId VlanId, UINT1 u1PortState));
INT4 FsMiPvrstNpInitHw PROTO ((UINT4 u4ContextId));
VOID FsMiPvrstNpDeInitHw PROTO ((UINT4 u4ContextId));
INT1 FsMiPvrstNpGetVlanPortState PROTO ((UINT4 u4ContextId, tVlanId VlanId, 
                                             UINT4 u4IfIndex, UINT1* pu1Status));

#endif /* _PVRSTMINP_H */
