/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mbsnp.h,v 1.11 2013/11/01 10:13:53 siva Exp $
 *
 * Description: Exported file for MBSM NP-API.
 *
 *******************************************************************/
#ifndef __MBSNP_H
#define __MBSNP_H

#define MBSM_STK_VLAN 4094        /*VLAN assigned for STK Traffic */
#define MBSM_MAX_BCM_CMD_LEN 200

INT4
MbsmNpInitHwTopoDisc PROTO ((INT4 i4SlotId, INT1 i1NodeState));
INT4
MbsmNpUpdtHwTblForLoadSharing PROTO ((UINT1 u1Flag));
INT4
MbsmNpProcCardInsertForLoadSharing PROTO ((INT4 i4MsgType));
INT4
MbsNpSetLoadSharingStatus PROTO ((INT4 i4LoadSharingFlag));
INT4
MbsmNpGetCardTypeTable PROTO ((INT4 i4LoopIdx,
                                     tMbsmCardInfo *pMbsmCardInfo));
    
INT4
MbsmNpClearHwTbl (INT4 i4SlotId);

PUBLIC INT4 MbsmNpGetSlotInfo (INT4 i4SlotId,tMbsmHwMsg *pHwMsg);
PUBLIC INT4 MbsmInitNpInfoOnStandbyToActive (tMbsmSlotInfo * pSlotInfo);
INT4 MbsmNpProcRmNodeTransition (INT4 i4Event, UINT1, UINT1);

VOID MbsmNpGetStackMac (UINT4 u4SlotId, UINT1 *pu1MacAddr);
INT4 MbsmNpTxOnStackInterface (UINT1 *pu1Pkt, INT4 i4PktSize);
VOID MbsmNpSetStackPriority (UINT4 u4Priority);
INT4
MbsmNpHandleNodeTransition (INT4 i4Event, UINT1 u1PrevState, UINT1 u1State);
UINT1 MbsmNpGetColdStandby (VOID);
VOID MbsmNpSetColdStandby (UINT1 u1ColdStandby);
#endif /* __MBSNP_H */
