/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mplsnp.h,v 1.56 2016/07/14 13:00:08 siva Exp $
 *
 * Description:This file contains the exported definitions and
 *             prototypes of MPLS
 *
 *******************************************************************/


#ifndef _MPLSNP_H
#define _MPLSNP_H

#include "ipnp.h"
#include "ip6np.h"


#define MPLS_HW_MAX_LBLSTACK       3

#define ISS_MPLS_INVALID_AC_INFO_INDEX 0xffffffff

/* Match criteria for Attachment circuit in a VPN. */
#define MPLS_HW_PWVC_ENET_DEF_PORT_VLAN L2VPN_PWVC_ENET_DEF_PORT_VLAN

#define ISS_MPLS_VPN_PW_MIN_INDEX       1
#define ISS_MPLS_VPN_PW_MAX_INDEX       MAX_L2VPN_PW_VC_ENTRIES
#define MPLS_SPLIT_HORIZON_NETWORK_GROUP_ID 4
#define MPLS_HW_L2VPN_VLANMODE_OTHER         0
#define MPLS_HW_L2VPN_VLANMODE_PORTBASED     1
#define MPLS_HW_L2VPN_VLANMODE_NOCHANGE      2
#define MPLS_HW_L2VPN_VLANMODE_CHANGEVLAN    3
#define MPLS_HW_L2VPN_VLANMODE_ADDVLAN       4
#define MPLS_HW_L2VPN_VLANMODE_REMOVEVLAN    5

#ifdef MPLS_L3VPN_WANTED
#define NP_MPLS_HW_L3_EGR_OBJ_ID_INVALID       0
#define MPLS_L3VPN_ROUTE_ADD                1
#define MPLS_L3VPN_ROUTE_DELETE             2
#endif

/* MPLS actions
 *
 *
 */

typedef enum LabelAction {
    MPLS_HW_ACTION_SWAP = 1,         /* Label swap and fwd to nexthop */
    MPLS_HW_ACTION_SWAP_PUSH,        /* Label swap, push new label & fwd 
                                      * to nexthop */
    MPLS_HW_ACTION_SWAP_NEW_DOMAIN,  /* Label swap, switch to next hop
                                      * & can add 1 or 2 new labels */
    MPLS_HW_ACTION_PHP,              /* Penultimate hop pop & can add 1 label */
    MPLS_HW_ACTION_POP_L2_SWITCH,    /* Pop 1 or 2 labels, L2 switch packet */
    MPLS_HW_ACTION_POP_DST_MOD_PORT, /* Pop 1 or 2 labels, fwd packet
                                      * to MODID/PORT */
    MPLS_HW_ACTION_POP_L3_SWITCH,    /* Pop 1 or 2 labels, L3 switch packet */
    MPLS_HW_ACTION_POP_L3_NEXTHOP,   /* Pop 1 or 2 labels, fwd packet to nexthop */
    MPLS_HW_ACTION_POP_SEARCH,       /* Pop 1 or 2 labels, repeate MPLS actions */
    MPLS_HW_PUSH_LABEL,              /* Ingress Tunnel Action */
    MPLS_HW_POP_LABEL,               /* Egress Tunnel Action*/            
    MPLS_HW_ACTION_POP_L3VPN_LABEL_SWITCH, /** Pop 1 or 2 Labels, L3VPN switch packet**/
    MPLS_HW_ACTION_COUNT             /* Must be last */
} tLabelAction;

/* MPLS_P2MP_LSP_CHANGES - S */
typedef enum Action {
    MPLS_HW_ACTION_L2_SWITCH = 1,
    MPLS_HW_ACTION_L3_SWITCH, 
    MPLS_HW_ACTION_COPY_L3_SWITCH
} tAction;
/* MPLS_P2MP_LSP_CHANGES - E */


typedef enum LabelOpcodes {
    MPLS_HW_PRESERVE_USER_TAG = 1,   /* Keep the user Tag intact */ 
    MPLS_HW_ORIGINAL_PKT,         
    MPLS_HW_INSERT_LABEL1,        
    MPLS_HW_INSERT_LABEL2,        
    MPLS_HW_ONE_LABEL_LOOKUP,     
    MPLS_HW_USE_OUTER_TTL,        

    MPLS_HW_USE_OUTER_EXP,       
    MPLS_HW_NO_CHANGE_INNER_EXP,  
    MPLS_HW_USE_PRI,              /* use QOS priority E-LSP/L-LSP*/
    MPLS_HW_NO_CHANGE_INNER_L2,   
    MPLS_HW_NO_MPLS_DECAP,        
    MPLS_HW_L2_LEARN,             
    MPLS_HW_L2_VPN,               
    MPLS_HW_L3_VPN,               
    MPLS_HW_LSR_SWITCH,           
    MPLS_HW_SRC_TRUNK,              /* Source port is trunk */
    MPLS_HW_NEXT_TRUNK,             /* Next (MPLS) port */
    /* is trunk         */
    MPLS_DST_TRUNK                 /* Dest (termination) */
        /* port is trunk      */
} tLabelOpcodes;

typedef enum LspType
{
    MPLS_HW_LSP_FEC =1,
    MPLS_HW_LSP_ILM =2,
    MPLS_HW_LSP_TUN =3
} tLspType;

typedef UINT4 tMplsHwLspId;

typedef struct {
    UINT4 u4Flag; /*specifies valid params*/
    UINT4 u4MaxRate; /*Max or Peak rate (bps)*/
    UINT4 u4MeanRate; /*Mean rate (bps)*/
    UINT4 u4MaxBurstSize; /*Max Burst size in bytes*/
    UINT4 u4MeanBurstSize; /*Mean Burst size in bytes*/
    UINT4 u4ExBurstSize; /*Excess Burst size in bytes*/
    UINT4 u4Frequency; /*Frequency of token refresh*/
    UINT4 u4Weight; /*Weight associated with tunnel*/
    UINT4 u4TrafficClass; /*Derived from parameters above*/
} tMplsHwTeParams;

typedef enum {
    MPLS_HW_DS_LSP_MODEL_NONE = 0, /* non-Diffserv model */
    MPLS_HW_DS_LSP_MODEL_PIPE = 1, /* Pipe */
    MPLS_HW_DS_LSP_MODEL_SHORTPIPE = 2, /* Short-pipe */
    MPLS_HW_DS_LSP_MODEL_UNIFORM = 3 /* Uniform */
} tDsLspModel;

typedef enum {
    MPLS_HW_DS_LSP_TYPE_NONE = 0, /* non-Diffserv type */
    MPLS_HW_DS_LSP_TYPE_ELSP = 1, /* ELSP */
    MPLS_HW_DS_LSP_TYPE_LLSP = 2 /* LLSP */
} tDsLspType;

typedef enum {
    MPLS_HW_LABEL_TYPE_IF_BASED = 0,
    MPLS_HW_LABEL_TYPE_GENERIC = 1, /* generic/ethernet */
    MPLS_HW_LABEL_TYPE_ATM = 2,
    MPLS_HW_LABEL_TYPE_FR = 3
} tMplsLabelType;

typedef UINT4 tMplsShimLabel; /* generic/ethernet label */

typedef struct {
    tMplsLabelType MplsLabelType; /* Type of label */
    union {
        tMplsShimLabel MplsShimLabel; /* Generic label/ethernet*/
    } u;
} tLabel;

typedef enum {
    MPLS_HW_QOS_POLICY_TYPE = 0, /* Current QOS  type */
    MPLS_HW_QOS_POLICY_GEN = 1, /* Generuc QOS type - extension */
} tQosType;

typedef struct {
    UINT4 u4QosMode;
    UINT4 u4QosPolicyId;
    UINT4 u4LabelPri;
    INT4  i4HwExpMapId;
    INT4  i4HwPriMapId;
} tHwQosPolicy;

typedef struct {
    UINT4  u4StackDepth;
    UINT4  u4LabelMinRange;
    UINT4  u4LabelMaxRange;
} tIntfParams;

typedef enum
{
    GENERIC_INTF = 0
} tIntfParamsType;

typedef struct {
    tIntfParamsType IntfParamsType; /* Type of intf param to parse */
    union {
        tIntfParams  IntfParams; /* ,etc */
    } u;
} tMplsIntfParamsSet;

/* For Entended PolicyType tMplsGenPolicyInfo  */
typedef struct{
    UINT4 u4Weight;
} tWeightPolicy;

typedef struct{
    UINT4 u4Dscp; /* incoming DSCP to select on */
} tDsPolicy;

typedef struct{
    union {
        tWeightPolicy WeightPolicy;
        tDsPolicy     DSPolicy;
    } u;
} tMplsPolicy;

typedef enum {
    NPF_MPLS_POLICYTYPE_NONE = 0,
    NPF_MPLS_POLICYTYPE_WEIGHT = 1,
    NPF_MPLS_POLICYTYPE_ELSP = 2,
    NPF_MPLS_POLICYTYPE_LLSP = 3
} tMplsPolicyType;

typedef struct {
    tQosType           QosType; /* Type of QOS to be parsed */
    union {
        tHwQosPolicy     MplsQosPolicy; /* Curr PolicyInfo */
    } u;
} tQosInfo;

typedef struct FecInfo {
    UINT4 u4SrcIpv4Net;
    UINT4 u4DstIpv4Net; 
    UINT4 u4SrcIpv6Net;
    UINT4 u4DstIpv6Net; 
    UINT4 u4SrcIpv4Host;
    UINT4 u4DstIpv4Host; 
    UINT4 u4SrcIpv6Host;
    UINT4 u4DstIpv6Host; 
    UINT4 u4SrcPort;
    UINT4 u4DstPort;
    UINT4 u4Ipv4Proto;
    UINT4 u4Ipv6NxtHdr;
    UINT4 u4VpnSrcPhyPort;   /*   VPN Src Port */
    UINT4 u4VpnSrcVlan;    /* VPN Src Vlan*/
    UINT4 u4VpnSrcIntf;    /* Src Intf sub to disc */
    UINT1 au1FecDstMac[MAC_ADDR_LEN];
    UINT1 au1FecSrctMac[MAC_ADDR_LEN];
    UINT4 u4FecBitmask; /* Tells which fields are applicable.
                         * (e,g) bit positions 0 and 1 indicates,
                         * u4VpnSrcVlan/Intf and u4VpnSrcPhyPort
                         * should be used for FEC.*/
    INT4  i4VSI; 
    UINT4 u4VplsInstance; 
    UINT4 u4PwEnetPwVlan;     /* Pw Enet Pw Vlan, it is used when
                                 the i1PwEnetVlanMode changevlan/addvlan*/
    UINT4  u4AcIfIndex; /* IfIndex created for the AC in IfTable*/
    UINT2 u2ReqVlanId;
    INT1  i1PwEnetVlanMode;     /* PW Enet Vlan modes */
    UINT1 au1Rsvd[1];
}tFecInfo;

typedef struct MplsHwVcTnlInfo {

    tLspType           LspType; /* Ingress Tnl/Vc  (or) Egress Tnl/Vc */

    tFecInfo           FecInfo; /* (M) */

    tLabelAction       LabelAction; /* how to act on Label(s) (M) */
    tLabelOpcodes      LabelOpcodes; /* what other options (M) */

    UINT4              u4PwL3Intf;   /* (M) */
    UINT4              u4PwOutPort;  /* Outgoing port of the underlying tunnel*/
    UINT4              u4PwIndex;
    UINT4              u4PwIfIndex; /*IfIndex created for the PW in IfTable*/
    tLabel             PwOutVcLabel; /* (M) */
    tLabel             PwInVcLabel; /* (M) */
 tLabel      OutLspLabel;
    tQosInfo           QosInfo[MPLS_DIFFSERV_MAX_EXP]; /* Label Stack QOS (O)*/
    UINT4              u4L3Ttl[MPLS_HW_MAX_LBLSTACK]; /* outer Label Stack TTL (O)*/
    UINT4              u4PwAcId; /*Unique ID for Enet entry*/   
    /* Tnl Label Stack (M) */
    tLabel             MplsLabelList[MPLS_HW_MAX_LBLSTACK]; 

    UINT1              au1PwDstMac[MAC_ADDR_LEN]; /* (M) */
    UINT2              u2OutVlanId;  /* Associated VLAN for the Outgoing port */
    UINT1              u1AppOwner; /* Specifies the applications runningn over 
                                      this PW entry.*/
    /*
     * If the variable is set to PW_HW_AC_UPDATE then AC programming should 
     * be done. 
     * If the variable is set to PW_HW_PW_UPDATE then PW programming should 
     * be done.
     * If the variable is set to PW_HW_VPN_UPDATE then both AC and PW     
     * programming should be done.
     * deleted.
     */
    UINT1              u1VpwsPwHwAction; /* Hardware Action to be Taken */
    INT1               i1CwStatus;

    INT1               i1PwVcType; /*Pw Type*/
    UINT4              u4VplsPwBindType;
} tMplsHwVcTnlInfo;

typedef struct MplsHwL3FTNInfo {
    /* VPN Info*/ 
    tFecInfo           FecInfo; /* (M) */
    UINT4              u4IngRPFIntf; /* Set interface on which RPF check 
                                      *  needs to be done (E)
                                      */
    /* Tunnel Info */
    UINT4              u4EgrL3Intf; /*  (M) */

    tMplsIntfParamsSet MplsIntfParamsSet; /* LabelRange Params Conf (O) */

    /* Label Stack (M) */
    tLabel             MplsLabelList[MPLS_HW_MAX_LBLSTACK]; 
    tQosInfo           QosInfo[MPLS_DIFFSERV_MAX_EXP]; /* Label Stack QOS (O)*/
    tMplsHwTeParams    MplsHwTeParams; /* tunnel/LSP TE Params (E)*/
    UINT4              u4L3Ttl[MPLS_HW_MAX_LBLSTACK]; /* outer Label Stack TTL (O)*/
    UINT1              au1DstMac[MAC_ADDR_LEN]; /* (M) */
    UINT2              u2OutVlanId;
    UINT4              u4OutPort;
    UINT1              u1TnlHwPathType;
    UINT1              au1Pad[3];
    tDsLspModel        DsLspModel;
} tMplsHwL3FTNInfo;


typedef struct MplsHwIlmInfo {
    tLspType           LspType; /* ILM to NHLFE, at LSR  (or) Tunnel
                                 * Endpoint (M) 
                                 */
    UINT4              u4VpnId; /* VPN identifier (M)*/
    tDsLspModel        DsLspModel; /* UNIFORM/PIPE/SHORT-PIPE (M)*/

    tDsLspType         DsLspType; /* ELSP/LLSP/NONE (M)*/

    tQosInfo           QosInfo[MPLS_DIFFSERV_MAX_EXP]; /* PolicyInfo (M) */

    UINT4              u4InVlanId; /* Ingress vlan/intf sub to disc (M)*/

    UINT4              u4InL3Intf; /* Ingress vlan/intf sub to disc (M)*/

    /* Incoming label(s) to lookup (M) - minimum one label */
    tLabel             InLabelList[MPLS_HW_MAX_LBLSTACK]; 

    tLabelAction       LabelAction; /* how to act on Label(s) (M) */
    tLabelOpcodes      LabelOpcodes; /* what other options (M) */

    /* Label stack to push,MplsPushLblList[0] being the first label to be
     * pushed  down (M)
     */
    tLabel             MplsPushLblList[MPLS_HW_MAX_LBLSTACK];  

    tLabel             MplsSwapLabel ; /* (M) */
    tLabel             OutLabelToLearn; /* (M) */
    UINT4              u4OutL3Intf; /* egress vlan/intf sub to disc (M) */


    UINT4              u4NewVlanId; /* Egress vlan -new vlan domain (O) */

    UINT4              u4SrcPort; /* TOREMOVE */
    UINT4              u4OutPort; /* TOREMOVE */

    tMplsHwLspId       MplsHwLspId; /*If the tunnel to be programmed is normal tunnel,
                                      this variable contains its own tunnel interface index.
                                      If the tunnel to be programmed is service tunnel 
                                      stacked over a H-LSP, this variable contains 
                                      the tunnel interface index of the H-LSP tunnel*/
    UINT4              u4MplsHwTtlDecrement; /* where to decrement TTL (E)*/
    tMplsHwTeParams    MplsHwTeParams; /* tunnel/LSP TE Params (E)*/  
    UINT4              u4LspMtu; /* LSP MTU (E)*/
    UINT1              au1DstMac [MAC_ADDR_LEN]; /* (M) */
    INT1               i1PwEnetVlanMode;     /* PW Enet Vlan modes */
    INT1               i1CwStatus;
    UINT4              u4PwEnetPwVlan;     /* Pw Enet Pw Vlan, it is used when
                                              the i1PwEnetVlanMode changevlan/addvlan*/
    UINT1              au1InMacAddr [MAC_ADDR_LEN];
    BOOL1              bIsStackedTunnel ; /* This flag will be set if this service tunnel is
                                             stacked over HLSP. It is added to optimize usuage of
                                             hardware resources during stacking*/
    INT1               i1PwVcType; /*Pw Type*/
    UINT4              u4TTLVal;
} tMplsHwIlmInfo;

 typedef struct MplsHwIntInfo { 
     UINT4    u4CfaIfIndex;
     UINT1    au1MacAddress [MAC_ADDR_LEN];
     UINT2    u2VlanId;
     UINT4    u4L3Intf;
 } tMplsHwMplsIntInfo;


typedef struct FlushMacInfo {
    UINT1              au1FlushMac [MAC_ADDR_LEN];
    UINT1              au1Reserv[2]; 
} tFlushMacInfo;

typedef struct MplsHwVplsInfo {
    tMplsHwVcTnlInfo   mplsHwVplsVcInfo; 
    tFlushMacInfo     *pFlushMacInfo;
    /* VPLS FDB */
#ifdef HVPLS_WANTED
    UINT4              u4VplsHighWaterMark;
    UINT4              u4VplsLowWaterMark;
#endif
    UINT2              u2VplsFdbId;
    UINT1              au1Pad[2];
} tMplsHwVplsInfo;

/* Structure Definitions for NP STATISTICS Collection */

typedef struct _MplsIfParam {
    UINT4              u4IntfNum;      /* Port number */
} tMplsIfParam;

/* Defines the Input Type for which the Statistics is required */

typedef enum _MplsInputType {
    MPLS_GET_VC_LBL_STATS = 1,         /* Pseudo wire Label statistics */
    MPLS_GET_TNL_LBL_STATS,            /* Tunnel Label related statistics */
    MPLS_GET_IF_STATS,                 /* Interface related statistics */
} tMplsInputType;

/* Structure used as input for getting the Statistics from the NP */

typedef struct _MplsInputParams {
    tMplsInputType InputType;          /* type info filled in the union */
    union {
        tMplsShimLabel  MplsLabel;     /* TNL/VC */
        tMplsIfParam    MplsIfParam;
    }u;
    tMplsShimLabel  MplsInLabel;     /* IN TNL LBL */
    UINT4           u4TnlIfIndex;   /* TNL OUT IfIndex */
    INT1           i1NpReset;   /* Flag indicate to reset the hw counters */
    INT1           ai1Pad[3];
} tMplsInputParams;

/* Structure used for returning the Statistics from the NP */
typedef struct _StatsInfo {
    FS_UINT8           u8Value;        /* for High Counter Values */
} tStatsInfo;

typedef enum _StatsType {
    /* Tunnel related Statistics */
    MPLS_HW_STAT_TUNNEL_INPKTS = 1,    /* pkts received on the tunnel */
    MPLS_HW_STAT_TUNNEL_OUTPKTS,       /* pkts forwarded by the tunnel */
    MPLS_HW_STAT_TUNNEL_INBYTES,       /* bytes received on the tunnel*/
    MPLS_HW_STAT_TUNNEL_OUTBYTES,      /* bytes forwarded by the tunnel*/
    MPLS_HW_STAT_TUNNEL_INERR,         /* error pkts rcvd on this segment */
    MPLS_HW_STAT_TUNNEL_OUTERR,        /* pkts dropped because of 
                                          errors or for other reasons */
    MPLS_HW_STAT_TNL_IN_DISCARDS,   /* pkts discarded on this InSegment */
    MPLS_HW_STAT_TNL_OUT_DISCARDS,  /* pkts discarded on this OutSegment */

    /* Current Statistics of a VC */
    MPLS_HW_STAT_VC_CURRENT_INPKTS,    /* pkts received by the VC 
                                          (from the PSN) */
    MPLS_HW_STAT_VC_CURRENT_INBYTES,   /* bytes received by the VC 
                                          (from the PSN) */ 
    MPLS_HW_STAT_VC_CURRENT_OUTPKTS,   /* pkts forwarded by the VC 
                                          (to the PSN) */
    MPLS_HW_STAT_VC_CURRENT_OUTBYTES,  /* bytes forwarded by the VC 
                                          (to the PSN) */

    /* Statistics of a VC for a specified Interval */
    MPLS_HW_STAT_VC_PERINTERVAL_INPKTS,   /* pkts received by this VC 
                                             during the specified interval */
    MPLS_HW_STAT_VC_PERINTERVAL_INBYTES,  /* bytes received by this VC  
                                             during the specified interval */
    MPLS_HW_STAT_VC_PERINTERVAL_OUTPKTS,  /* pkts sent by this VC 
                                             during the specified interval */
    MPLS_HW_STAT_VC_PERINTERVAL_OUTBYTES, /* bytes snet by this VC 
                                             during the specified interval */

    MPLS_HW_STAT_VC_ILLEGAL_VLAN,      /* pkts received (from the PSN) 
                                          on this VC with an illegal
                                          VLAN field */
    MPLS_HW_STAT_VC_ILLEGAL_LENGTH,    /* pkts that were received 
                                          with an illegal Ethernet packet length
                                          on this VC Eg : PKT-SIZE > MTU */

    /* Interface related Statistics */
    MPLS_HW_STAT_IF_IN_LABELS_INUSE,        /* counts of labels that are in
                                               use at this point in time */
    MPLS_HW_STAT_IF_IN_LBL_LOOKUP_FAILURES, /* pkts received with no 
                                               matching cross-connect entry */
    MPLS_HW_STAT_IF_OUT_LABELS_INUSE,
    MPLS_HW_STAT_IF_OUT_FRAGMENTED_PKTS,    /* pkts that require fragmentation 
                                               before transmission */
} tStatsType;

#ifdef MPLS_L3VPN_WANTED
typedef struct MplsHwL3VpnBgp4AddrPrefix
{
    UINT2   u2Afi;        /* Represents address family */
    UINT2   u2AddressLen; /* Represents the length of the prefix present
                           * (e.g. for IPV4 = 4, IPV6 = 16
                           *                            */
    UINT1   au1Address [L3VPN_BGP_MAX_ADDRESS_LEN];
    /* To store Prefix address */
    UINT1   u1PrefixLen; /* Represents the length of the prefix */
    UINT1   au1Pad[3];
}tMplsHwL3VpnBgp4AddrPrefix;

typedef struct MplsHwL3vpnEgressInfo
{
    UINT4              u4OutPort;
    UINT4              u4VrfId;
    tMplsHwL3VpnBgp4AddrPrefix NexHopAddr;
    UINT4               u4Label;
    INT4                i4HwExpMapId;
    INT4                i4HwPriMapId;
    UINT1               au1DstMac[MAC_ADDR_LEN];
    UINT1               u1LabelPolicy; /* Per VRF or Per Route */
    UINT1               u1Action;
}tMplsHwL3vpnEgressInfo;

typedef struct MplsHwL3vpnIgressInfo
{
    UINT4               u4Label;
    UINT4               u4NhAs;
    INT4                i4Metric;
    UINT4               u4VrfId;
    /* NON-TE LSP Info */
    UINT4              u4EgrL3Intf; /*  (M) */
    UINT4              u4OutPort;
    /* Label Stack (M) */
    tLabel             MplsLabelList[MPLS_HW_MAX_LBLSTACK];
    tMplsHwL3VpnBgp4AddrPrefix NextHop;
    tMplsHwL3VpnBgp4AddrPrefix IpPrefix;
    INT4               i4HwExpMapId;
    INT4               i4HwPriMapId;
    UINT1              au1DstMac[MAC_ADDR_LEN]; /* (M) */
    UINT1              u1Action;
    UINT1              u1Pad;
}tMplsHwL3vpnIgressInfo;

#endif



#define MPLS_HW_STATS_INPUT_LABEL(InputParams) InputParams.u.MplsLabel
#define MPLS_HW_STATS_INPUT_IF(InputParams) InputParams.u.MplsIfParam.u4IntfNum

/*Functions Protos */

/* MPLS system/intf wide */
INT4 FsMplsHwEnableMpls(VOID);

INT4 FsMplsHwDisableMpls (VOID);

INT4 FsMplsHwEnableMplsIf (UINT4 u4Intf,tMplsIntfParamsSet *pMplsIntfParamsSet);

INT4 FsMplsHwDisableMplsIf (UINT4 u4Intf);
INT4 FsMplsHwGetPwCtrlChnlCapabilities (UINT1 *pu1PwCCTypeCapabs);

/* L3 MPLS */

INT4 FsMplsHwCreateL3FTN (UINT4 u4VpnId, tMplsHwL3FTNInfo *pMplsHwL3FTNInfo);

#ifdef NP_BACKWD_COMPATIBILITY
INT4 FsMplsHwDeleteL3FTN(UINT4 u4L3EgrIntf);
#else
INT4 FsMplsHwDeleteL3FTN(UINT4 u4VpnId, tMplsHwL3FTNInfo *pMplsHwL3FTNInfo);
#endif
INT4 FsMplsWpHwDeleteL3FTN(UINT4 u4VpnId, tMplsHwL3FTNInfo *pMplsHwL3FTNInfo);

INT4 FsMplsHwCreateILM (tMplsHwIlmInfo  *pMplsHwIlmInfo, UINT4 u4Action);


INT4 FsMplsHwDeleteILM (tMplsHwIlmInfo *pMplsHwDelIlmParams,UINT4 u4Action);

INT4 FsMplsHwTraverseL3FTN (tMplsHwL3FTNInfo * pMplsHwL3FTNInfo, tMplsHwL3FTNInfo * pNextMplsHwL3FTNInfo);
INT4 FsMplsHwGetL3FTN (UINT4 u4VpnId, UINT4 u4IfIndex, tMplsHwL3FTNInfo * pMplsHwL3FTNInfo);

INT4 FsMplsHwTraverseILM (tMplsHwIlmInfo  *pMplsHwIlmInfo,tMplsHwIlmInfo 
        *pNextMplsHwIlmInfo);

INT4 FsMplsHwGetILM (tMplsHwIlmInfo  *pMplsHwIlmInfo,tMplsHwIlmInfo 
        *pNextMplsHwIlmInfo);




/* L2 VPN */

/* VPWS */
INT4 FsMplsHwCreatePwVc (UINT4 u4VpnId, tMplsHwVcTnlInfo *pMplsHwVcInfo, 
        UINT4 u4Action);

INT4
FsMplsHwTraversePwVc (tMplsHwVcTnlInfo * pMplsHwVcInfo,
        tMplsHwVcTnlInfo * pNextMplsHwVcInfo);
INT4
FsMplsHwGetPwVc (tMplsHwVcTnlInfo * pMplsHwVcInfo,
        tMplsHwVcTnlInfo * pNextMplsHwVcInfo);


INT4 FsMplsHwDeletePwVc (UINT4 u4VpnId, tMplsHwVcTnlInfo *pMplsHwVcInfo, 
        UINT4 u4Action);

INT4 FsMplsHwCreatePsnTunnel (UINT4 u4VpnId, tMplsHwVcTnlInfo *pMplsHwTnlInfo, 
        UINT4 u4Action);

INT4 FsMplsHwDeletePsnTunnel (UINT4 u4VpnId, tMplsHwVcTnlInfo *pMplsHwTnlInfo, 
        UINT4 u4Action);


/* VPLS */
INT4 FsMplsHwCreateVplsVpn (UINT4 u4VplsInstance, UINT4 u4VpnId, 
        tMplsHwVplsInfo *pMplsHwVplsVpnInfo);

INT4 FsMplsHwDeleteVplsVpn (UINT4 u4VplsInstance, UINT4 u4VpnId, 
        tMplsHwVplsInfo *pMplsHwVplsVpnInfo);

INT4 FsMplsHwVplsAddPwVc (UINT4 u4VplsInstance, UINT4 u4VpnId, 
        tMplsHwVplsInfo *pMplsHwVplsVcInfo, 
        UINT4 u4Action);
INT4
FsMplsHwModifyPwVc (UINT4 u4VpnId, UINT4 u4OperActVpnId,tMplsHwVcTnlInfo *pMplsHwVcInfo);

INT4 FsMplsHwVplsDeletePwVc (UINT4 u4VplsInstance, UINT4 u4VpnId, 
        tMplsHwVplsInfo *pMplsHwVplsVcInfo, 
        UINT4 u4Action);

INT4 FsMplsHwVplsFlushMac (UINT4 u4VplsInstance, UINT4 u4VpnId, 
        tMplsHwVplsInfo *pMplsHwVplsVpnInfo);

#ifdef HVPLS_WANTED
INT4 FsMplsRegisterFwdAlarm (UINT4 u4VpnId,tMplsHwVplsInfo *pMplsHwVplsVpnInfo);
INT4 FsMplsDeRegisterFwdAlarm (UINT4 u4VpnId);
#endif
INT4
FsMplsHwAddVpnAc (UINT4 u4VplsInstance, UINT4 u4VpnId, 
        tMplsHwVplsInfo * pMplsHwVplsInfo, UINT4 u4Action);
UINT4
NpMplsCreateMplsInterfaceWr (tMplsHwMplsIntInfo *  pMplsHwMplsIntInfo);

UINT4
NpMplsCreateMplsInterface (UINT4 u4CfaIfIndex, UINT2 u2VlanId, UINT1 *au1MacAddr);

UINT4
FsMplsHwAddMplsRoute (UINT4 u4VrId, UINT4 u4IpDestAddr, UINT4 u4IpSubNetMask,
        tFsNpNextHopInfo routeEntry, UINT1 *pbu1TblFull);

UINT4
FsMplsHwAddMplsIpv6Route (UINT4 u4VrId, UINT1 *pu1Ip6Prefix, UINT1 u1PrefixLen,
                    UINT1 *pu1NextHop, UINT4 u4NHType,
                    tFsNpIntInfo * pFsNpIntInfo);

UINT4
FsMplsHwDeleteMplsIpv6Route (UINT4 u4VrId, UINT1 *pu1Ip6Prefix, UINT1 u1PrefixLen,
                       UINT1 *pu1NextHop, UINT4 u4IfIndex, tFsNpRouteInfo *pRouteInfo);

INT4
FsMplsHwDeleteVpnAc (UINT4 u4VplsInstance, UINT4 u4VpnId,
        tMplsHwVplsInfo * pMplsHwVplsInfo, UINT4 u4Action);
INT4
FsMplsHwGetVpnAc (tMplsHwVplsInfo * pMplsHwVplsInfo);

/* MPLS_P2MP_LSP_CHANGES - S */

INT4 FsMplsHwP2mpAddILM (UINT4 u4P2mpId, tMplsHwIlmInfo *pMplsHwP2mpIlmInfo);

INT4 FsMplsHwP2mpRemoveILM (UINT4 u4P2mpId, tMplsHwIlmInfo *pMplsHwP2mpIlmInfo);

INT4
FsMplsHwP2mpTraverseILM (UINT4 u4P2mpId, tMplsHwIlmInfo *pMplsHwP2mpIlmInfo,
        tMplsHwIlmInfo *pNextMplsHwP2mpIlmInfo);

INT4 FsMplsHwP2mpAddBud (UINT4 u4P2mpId, UINT4 u4Action);

INT4 FsMplsHwP2mpRemoveBud (UINT4 u4P2mpId, UINT4 u4Action);
/* MPLS_P2MP_LSP_CHANGES - E */


#ifdef MBSM_WANTED
INT4
FsMplsMbsmHwEnableMpls (tMbsmSlotInfo * pSlotInfo);
INT4
FsMplsMbsmHwDisableMpls (tMbsmSlotInfo * pSlotInfo);
INT4
FsMplsMbsmHwGetPwCtrlChnlCapabilities (UINT1 *pu1PwCCTypeCapabs);
INT4
FsMplsMbsmHwCreatePwVc (UINT4 u4VpnId, tMplsHwVcTnlInfo * pMplsHwVcInfo,
        UINT4 u4Action, tMbsmSlotInfo * pSlotInfo);
INT4
FsMplsMbsmHwDeletePwVc (UINT4 u4VpnId, tMplsHwVcTnlInfo * pMplsHwDelVcInfo,
        UINT4 u4Action, tMbsmSlotInfo * pSlotInfo);
INT4
FsMplsMbsmHwCreateILM (tMplsHwIlmInfo * pMplsHwIlmInfo, 
        UINT4 u4Action, tMbsmSlotInfo *pSlotInfo);
INT4
FsMplsMbsmHwDeleteILM (tMplsHwIlmInfo * pMplsHwDelIlmParams, UINT4 u4Action,
        tMbsmSlotInfo *pSlotInfo);
INT4
FsMplsMbsmHwCreateL3FTN (UINT4 u4VpnId, tMplsHwL3FTNInfo * pMplsHwL3FTNInfo,
        tMbsmSlotInfo *pSlotInfo);
INT4
FsMplsMbsmHwDeleteL3FTN (UINT4 u4L3EgrIntf, tMbsmSlotInfo *pSlotInfo);

INT4
FsMplsMbsmHwCreateVplsVpn (UINT4 u4VplsInstance, UINT4 u4VpnId, 
        tMplsHwVplsInfo *pMplsHwVplsVpnInfo, tMbsmSlotInfo *pSlotInfo);
INT4
FsMplsMbsmHwDeleteVplsVpn (UINT4 u4VplsInstance, UINT4 u4VpnId, 
        tMplsHwVplsInfo *pMplsHwVplsVpnInfo, tMbsmSlotInfo *pSlotInfo);
INT4
FsMplsMbsmHwVplsAddPwVc (UINT4 u4VplsInstance, UINT4 u4VpnId, 
        tMplsHwVplsInfo  *pMplsHwVplsVcInfo, UINT4 u4Action, 
        tMbsmSlotInfo *pSlotInfo);
INT4
FsMplsMbsmHwVplsDeletePwVc (UINT4 u4VplsInstance, UINT4 u4VpnId, 
        tMplsHwVplsInfo *pMplsHwVplsVcInfo, UINT4 u4Action, 
        tMbsmSlotInfo *pSlotInfo);
INT4
FsMplsMbsmHwAddVpnAc (UINT4 u4VplsInstance, UINT4 u4VpnId, 
        tMplsHwVplsInfo *pMplsHwVplsVcInfo, UINT4 u4Action, 
        tMbsmSlotInfo *pSlotInfo);
INT4
FsMplsMbsmHwDeleteVpnAc (UINT4 u4VplsInstance, UINT4 u4VpnId, 
        tMplsHwVplsInfo *pMplsHwVplsVcInfo, UINT4 u4Action, 
        tMbsmSlotInfo *pSlotInfo);
UINT4
NpMplsMbsmCreateMplsInterfaceWr (tMplsHwMplsIntInfo *  pMplsHwMplsIntInfo,
                      tMbsmSlotInfo * pSlotInfo);
UINT4 
NpMplsMbsmCreateMplsInterface (UINT4 u4CfaIfIndex, UINT2 u2VlanId,
                 UINT1 *au1MacAddr, tMbsmSlotInfo * pSlotInfo);

/* MPLS_P2MP_LSP_CHANGES - S */
INT4
FsMplsMbsmHwP2mpAddILM (UINT4 u4P2mpId, tMplsHwIlmInfo * pMplsHwP2mpIlmInfo,
        tMbsmSlotInfo *pSlotInfo);
INT4
FsMplsMbsmHwP2mpRemoveILM (UINT4 u4P2mpId, tMplsHwIlmInfo * pMplsHwP2mpIlmInfo,
        tMbsmSlotInfo *pSlotInfo);
/* MPLS_P2MP_LSP_CHANGES - E */

INT4
FsMplsMbsmHwGetVpnAc (tMplsHwVplsInfo * pMplsHwVplsInfo, tMbsmSlotInfo *pSlotInfo);
INT4
FsMplsMbsmHwGetPwVc (tMplsHwVcTnlInfo * pMplsHwVcInfo,
            tMplsHwVcTnlInfo * pNextMplsHwVcInfo, tMbsmSlotInfo *pSlotInfo);
INT4 
FsMplsMbsmHwGetILM (tMplsHwIlmInfo  *pMplsHwIlmInfo,tMplsHwIlmInfo
        *pNextMplsHwIlmInfo, tMbsmSlotInfo *pSlotInfo);
INT4 
FsMplsMbsmHwGetL3FTN (UINT4 u4VpnId, UINT4 u4IfIndex, tMplsHwL3FTNInfo * pMplsHwL3FTNInfo,
                       tMbsmSlotInfo *pSlotInfo);
#endif



/* Generic API for retreiving Statistics from NP *Exported* */
PUBLIC INT4 FsMplsHwGetMplsStats (tMplsInputParams *pInputParams,
        tStatsType StatsType,
        tStatsInfo *pStatsInfo);
#ifdef MPLS_L3VPN_WANTED

#ifdef MBSM_WANTED
INT4
FsMplsMbsmHwL3vpnIngressMap(tMplsHwL3vpnIgressInfo *pMplsHwL3vpnIgressInfo,tMbsmSlotInfo * pSlotInfo);
INT4
FsMplsHwL3vpnIngressUnMap(tMplsHwL3vpnIgressInfo *pMplsHwL3vpnIgressInfo);

INT4
FsMplsMbsmHwL3vpnEgressMap(tMplsHwL3vpnEgressInfo*pMplsHwL3vpnEgressInfo,tMbsmSlotInfo * pSlotInfo);
INT4
FsMplsHwL3vpnEgressUnMap(tMplsHwL3vpnEgressInfo*pMplsHwL3vpnEgressInfo);
#endif

INT4
FsMplsHwL3vpnIngressMap(tMplsHwL3vpnIgressInfo *pMplsHwL3vpnIgressInfo);
INT4
FsMplsHwL3vpnEgressMap(tMplsHwL3vpnEgressInfo*pMplsHwL3vpnEgressInfo);
#endif
UINT4
FsNpMplsModifyMplsTunnelStatus(UINT4 u4CfaIfIndex, UINT2 u2VlanId,
                                                           UINT4 u4Status);

#endif /* _MPLS_H */
