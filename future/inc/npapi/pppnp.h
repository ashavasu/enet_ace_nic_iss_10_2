/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppnp.h,v 1.2 2014/03/11 14:02:39 siva Exp $
 *
 * Description: Exported file for PPP NP-API.
 *
 *******************************************************************/
#ifndef _PPP_NP_H
#define _PPP_NP_H
#include "fsvlan.h"
typedef enum {
 PPP_PPP= 1,
 PPP_MP = 2 
} tPppIfType;

typedef enum {
    NP_PPP_LCP =1,
    NP_PPP_IPCP=2
} tPppProtoType;

typedef enum {
   PPPoE_ADD_SESSION = 1,
   PPPoE_DELETE_SESSION = 2,
   PPPoE_TRAP_UPDATE = 3,
   PPPoE_UPDATE_NAT = 4,
} ePppNpwFsPPPoESession;

typedef enum {
   PPPoE_ADD_FILTER = 1,
   PPPoE_DELETE_FILTER = 2
} ePppNpwFsFilter;

typedef struct {
   UINT4 u4IfIndex;
   tMacAddr DestMACAddr;
   tMacAddr SrcMACAddr;
   UINT2 u2Port;
   UINT2 u2SessionId;
   UINT4 ACCookieId;
} tPppNpwFsAddSession;

typedef struct {
   UINT4 u4IfIndex;
   UINT2 u2Port;
   UINT2 u2SessionId;
} tPppNpwFsDeleteSession;

typedef struct {
   ePppNpwFsFilter eFilter;
   UINT4 u4IfIndex;
   tMacAddr DestMACAddr;
   tMacAddr SrcMACAddr;
   UINT2 u2Port;
   UINT2 u2SessionId;
   UINT4 ACCookieId;
} tPppNpwFsFilter;

typedef struct {
   UINT4 IfIndex;
   UINT4 u4AckPeerIPAddr;
} tPppNpwFsNATEntry;

typedef union {
   tPppNpwFsAddSession FsAddNewSession;
   tPppNpwFsDeleteSession FsDeleteSession;
   tPppNpwFsFilter FsFilter;
   tPppNpwFsNATEntry FsNATEntry;
} unPppNpwFsPPPoESession;

UINT4 FsNpPppInit(VOID);

UINT4  FsNpPppLCPUp(UINT4 IfIndex, UINT4 u4PeerMRU, UINT4 u4LocalMRU,
                          UINT1 u1LocToRemPFC, UINT1 u1RemToLocPFC ,
                          UINT1 u1LocToRemACFC , UINT1 u1RemToLocACFC);

UINT4 FsNpPppGetLinkStatus(UINT4 u4IfIndex);

UINT4 FsNpPppIPCPUp(UINT4 IfIndex, UINT4 u4AckPeerIPAddr,
                    UINT4 u4AckLocalIPAddr);

UINT4  FsNpPPPoEUpdateNat(UINT4 IfIndex, UINT4 u4AckPeerIPAddr);

UINT4 FsNpPppCPDown(UINT4 u4IfIndex,UINT4 u4CPType);

UINT4 FsNpCreatePppIf (UINT4 u4IfIndex, UINT4 u4PhysIfIndex, UINT4 u4IfType);

UINT4 FsNpDelPppIf (UINT4 u4IfIndex, UINT4 u4PhysIfIndex, UINT4 u4IfType);
UINT4 FsNpUpdPppIf (UINT4 u4IfIndex, UINT4 u4PhysIfIndex, UINT4 u4IfType);

UINT4 FsNpPPPoESession (ePppNpwFsPPPoESession pNpType, unPppNpwFsPPPoESession *pUnion);

UINT4 FsNpPPPoEAddNewSession(UINT4 u4IfIndex, tMacAddr DestMACAddr,
                              tMacAddr SrcMACAddr, UINT2 u2Port, 
                              UINT2 u2SessionId, UINT4 ACCookieId, tVlanTag *pVlanInfo);

UINT4 FsNpPPPoEDelSession (UINT4 u4IfIndex, UINT2 u2Port, UINT2 u2SessionId);

UINT4 FsNpPPPoEAddVlanInfo (UINT4 u4IfIndex, UINT4 u4PhysIfIndex, tVlanTag *pSVlanInfo);

UINT4 FsNpPPPoEDelVlanInfo (UINT4 u4IfIndex, UINT4 u4PhysIfIndex);

UINT4 FsNpCreateMPBundle (UINT4 u4IfIndex);

UINT4 FsNpAddLinkToMPBundle (UINT4 u4MemberIfIndex, UINT4 u4BundleIfIndex);

UINT4 FsNpEnableLinkInMPBundle (UINT4 u4MemberIfIndex, UINT4 u4BundleIfIndex);

UINT4 FsNpDisableLinkInMPBundle (UINT4 u4MemberIfIndex, UINT4 u4BundleIfIndex);

UINT4 FsNpRemoveLinkFromMPBundle (UINT4 u4MemberIfIndex, UINT4 u4BundleIfIndex);

UINT4 FsNpDeleteMPBundle (UINT4 u4IfIndex);

INT4 FsNpGetFlowId PROTO ((UINT2 *pu2FlowId));

INT4 FsNpDelFlowId PROTO ((UINT2 u2FlowId));
INT4 PppNpTinygramCompressSupported PROTO ((VOID));
INT4 PppNpNonEthernetMacTypesSupported PROTO ((VOID));
INT4 PppNpSRouteBridgeSupported PROTO ((VOID));
#endif
