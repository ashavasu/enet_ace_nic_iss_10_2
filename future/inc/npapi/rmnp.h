/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rmnp.h,v 1.4 2014/03/21 14:17:01 siva Exp $
 *
 * Description: This file contains the function implementations  of FS NP-API.
 ****************************************************************************/
#ifndef _RMNP_H
#define _RMNP_H

INT4 RmNpUpdateNodeState (UINT1 u1Event);
/* HITLESS RESTART */

#define RM_STDY_ST_MAX_PDU_SIZE     1568

typedef enum _eRmHRAction
{
    NP_RM_HR_STDY_ST_TRIG_START = 1,
    NP_RM_HR_STDY_ST_TRIG_STOP,
    NP_RM_HR_STORE_STDY_ST_PKT,
    NP_RM_HR_SUSPEND_COPY_TO_CPU_TRAFFIC,
    NP_RM_HR_RESUME_COPY_TO_CPU_TRAFFIC,
    NP_RM_HR_INVALID_ACTION
}tRmHRAction;

typedef struct _sRmHRPktInfo
{
    UINT1   au1LinBuf[RM_STDY_ST_MAX_PDU_SIZE];
    UINT4   u4PktLen;
    UINT4   u4TimeOut;
    UINT2   u2Port;
    UINT1   u1ProtoId;
    UINT1   u1Pad;

}tRmHRPktInfo;

INT4 FsNpHRSetStdyStInfo (tRmHRAction eAction, tRmHRPktInfo *pRmHRPktInfo);

#endif /* _RMNP_H */
