/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: vxlannp.h,v 1.7 2018/01/05 09:57:08 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              for VXLAN NPAPI
 *
 ********************************************************************/

#include "lr.h"
#include "npapi.h"

#ifndef _VXLANNP_H
#define _VXLANNP_H

typedef struct 
{
    INT4         i4NveIfIndex;
    UINT4        u4IfIndex; 
    UINT4        u4VniNumber;
    UINT4        u4VlanId; 
    UINT4        u4VlanIdOutTunnel;   /* ip tunneling must be enabled on dest's vlan */
    UINT4        u4VrId;              /* VRID */
    INT4         i4VtepAddressType;    
    INT4         i4RemoteVtepAddressType;    
    UINT4        u4NetworkPort;
    /* au1VtepAddress must be the same as the switch vxlan_dip!!! */
    UINT1        au1VtepAddress[16]; 
    UINT1        au1RemoteVtepAddress[16];    
    tMacAddr     DestVmMac;    
    BOOL1        b1NveFlushAllVmMacOnly;
    UINT1        u1MacType;
    UINT1        au1NextHopMac[MAC_ADDR_LEN];
    UINT1        u1IsEgIntfTobeDeleted;
    UINT1       au1Pad[1];
} tVxlanHwNveInfo;

/*Multicast database
   1. NVE interface index
   2. VNI Number
   3. VLAN Id
   4. Source VTEP address type (IPV4/IPV6)
   5. Source VTEP address
   6. Multicast group address type IPV4/IPV6)
   7. Multicast group address

*/
typedef struct _VxlanHwMcastInfo {
    INT4        i4NveIfIndex;
    UINT4        u4VniNumber; 
    UINT4        u4VlanId;
    /* au1VtepAddress must be the same as the switch vxlan_dip!!! */
    INT4         i4VtepAddressType;    
    INT4         i4GroupAddressType; 
    UINT1        au1VtepAddress[16];    
    UINT1        au1GroupAddress[16];    
    tMacAddr     McastMac;
    UINT2        u2Rsvd;
    UINT4        u4VlanIdOutTunnel;
    UINT4        u4VrId;              /* VRID */
} tVxlanHwMcastInfo;

/* VLAN - VNI Database
   1. Vlan id
   2. VNI Number
   3. VXLAN packet sent counter
   4. VXLAN packet rcvd counter
   5. VXLAN packet drpd counter
*/

typedef struct _VxlanHwVniVlanInfo {
    UINT4        u4VniNumber;
    UINT4        u4VlanId; 
    UINT4        u4IfIndex; /* Member port of VLAN to be created as access port */
    UINT4        u4PktSent;
    UINT4        u4PktRcvd;
    UINT4        u4PktDrpd;
    UINT4        u4VrId;              /* VRID */
    UINT4        u4AccessPort;  /* Access port created in BCM */
    UINT1        u1IsEgIntfTobeDeleted;
    UINT1        u1IsVlanTagged;
    UINT1        au1Pad[2];    
} tVxlanHwVniVlanInfo;

/* BUM Head-end replica database
   1. NVE interface index
   2. VNI Number
   3. VLAN Id
   5. Source VTEP address
   6. Number of Remote Vteps to replica packets to
   6. Pointer to a list of Remote Vteps IP addresses.

*/
typedef struct _VxlanHwBUMReplicaInfo {
    INT4        i4NveIfIndex;
    UINT4        u4IfIndex;
    UINT4        u4VniNumber;
    UINT4        u4VlanId;
    UINT4        u4VlanIdOutTunnel;
    UINT4        u4VrId;              /* VRID */
    INT4         i4VtepAddressType;
    INT4         i4RemoteVtepAddressType;
    /* au1VtepAddress must be the same as the switch vxlan_dip!!! */
    UINT1        au1VtepAddress[16];
    UINT4        u4NoRemoteVtepsReplicaTo;
    /* all of the remote vteps vlans must be enabled as ip-tunneling already ? */
    UINT1        *pau1RemoteVtepsReplicaTo;     /* pointer to a list Replica Vteps IPs */
    UINT1        au1RemoteVtepAddress[16];
    UINT4         u4NetworkBUMPort;    /* Multicast Network port created in BCM */
    UINT4         u4NetworkPort;    /* Unicast Network port created in BCM */
    UINT1        au1NextHopMac[MAC_ADDR_LEN];
    UINT1        u1ArpSupFlag;
    UINT1        u1IsEgIntfTobeDeleted;
} tVxlanHwBUMReplicaInfo;

typedef struct _VxlanHwBUMMHPeerInfo { 
    UINT4        u4NoBumMHPeers;  /* Denotes number of Multihomed peers */ 
    UINT1        *pau1BumMHPeers; /* Denotes Multihomed peer list */ 
    UINT4        u4Vni;       /*Denotes the multi homed VNI value */
    UINT1        u1DFFlag; /* Dentoes whether the VTEP is DF or not */ 
    UINT1        u1Pad[3];
}tVxlanHwBUMMHPeerInfo; 

typedef struct _VxlanHwVlanPortInfo { 
    UINT4        u4VniNumber;  /* Denotes number of Multihomed peers */ 
    UINT4        u4VlanId;
}tVxlanHwVlanPortInfo; 

#ifdef MBSM_WANTED
typedef struct
{
    tMbsmSlotInfo *              pSlotInfo;
}tVxlanMbsmHwInitInfo;

typedef struct
{
    UINT4                   u4VxlanUdpPortNo;
    tMbsmSlotInfo *              pSlotInfo;
}tVxlanMbsmHwUdpPortInfo;

typedef struct 
{
    INT4         i4NveIfIndex;
    UINT4        u4IfIndex; 
    UINT4        u4VniNumber;
    UINT4        u4VlanId; 
    UINT4        u4VlanIdOutTunnel;   /* ip tunneling must be enabled on dest's vlan */
    UINT4        u4VrId;              /* VRID */
    INT4         i4VtepAddressType;    
    INT4         i4RemoteVtepAddressType;    
    UINT4         u4NetworkPort;    /* Network port created in BCM */
    /* au1VtepAddress must be the same as the switch vxlan_dip!!! */
    UINT1        au1VtepAddress[16]; 
    UINT1        au1RemoteVtepAddress[16];
    tMacAddr     DestVmMac;    
 
    BOOL1        b1NveFlushAllVmMacOnly;
    UINT1        u1MacType;
    UINT1        au1NextHopMac[MAC_ADDR_LEN];
    UINT1       au1Pad[2];
    tMbsmSlotInfo   *pSlotInfo;
} tVxlanMbsmHwNveInfo;

/*Multicast database
   1. NVE interface index
   2. VNI Number
   3. VLAN Id
   4. Source VTEP address type (IPV4/IPV6)
   5. Source VTEP address
   6. Multicast group address type IPV4/IPV6)
   7. Multicast group address

*/
typedef struct {
    INT4        i4NveIfIndex;
    UINT4        u4VniNumber; 
    UINT4        u4VlanId;
    /* au1VtepAddress must be the same as the switch vxlan_dip!!! */
    INT4         i4VtepAddressType;    
    INT4         i4GroupAddressType; 
    UINT1        au1VtepAddress[16];    
    UINT1        au1GroupAddress[16];
    tMbsmSlotInfo   *pSlotInfo;
    tMacAddr     McastMac;
    UINT2        u2Rsvd;
    UINT4        u4VlanIdOutTunnel;
    UINT4        u4VrId;              /* VRID */
} tVxlanMbsmHwMcastInfo;

/* VLAN - VNI Database
   1. Vlan id
   2. VNI Number
   3. VXLAN packet sent counter
   4. VXLAN packet rcvd counter
   5. VXLAN packet drpd counter
*/

typedef struct {
    UINT4        u4VniNumber;
    UINT4        u4VlanId; 
    UINT4        u4IfIndex; /* Member port of VLAN to be created as access port */
    UINT4        u4PktSent;
    UINT4        u4PktRcvd;
    UINT4        u4PktDrpd;
    UINT4        u4VrId;              /* VRID */
    UINT4        u4AccessPort;  /* Access port created in BCM */
    UINT1        u1IsVlanTagged;
    tMbsmSlotInfo   *pSlotInfo;
} tVxlanMbsmHwVniVlanInfo;

/* BUM Head-end replica database
   1. NVE interface index
   2. VNI Number
   3. VLAN Id
   5. Source VTEP address
   6. Number of Remote Vteps to replica packets to
   6. Pointer to a list of Remote Vteps IP addresses.

*/

typedef struct {
    INT4        i4NveIfIndex;
    UINT4        u4IfIndex;
    UINT4        u4VniNumber;
    UINT4        u4VlanId;
    UINT4        u4VlanIdOutTunnel;
    UINT4        u4VrId;              /* VRID */
    INT4         i4VtepAddressType;
    INT4         i4RemoteVtepAddressType;
    /* au1VtepAddress must be the same as the switch vxlan_dip!!! */
    UINT1        au1VtepAddress[16];
    UINT4        u4NoRemoteVtepsReplicaTo;
    /* all of the remote vteps vlans must be enabled as ip-tunneling already ? */
    UINT1        *pau1RemoteVtepsReplicaTo;     /* pointer to a list Replica Vteps IPs */
    UINT1        au1RemoteVtepAddress[16];
    UINT4         u4NetworkBUMPort;    /* Network port created in BCM */
    UINT4         u4NetworkPort;    /* Network port created in BCM */
    UINT1        u1ArpSupFlag;
    UINT1        au1NextHopMac[MAC_ADDR_LEN];
    UINT1        u1Pad;
    tMbsmSlotInfo   *pSlotInfo;
} tVxlanMbsmHwBUMReplicaInfo;

typedef struct { 
    UINT4        u4NoBumMHPeers;  /* Denotes number of Multihomed peers */ 
    UINT1        *pau1BumMHPeers; /* Denotes Multihomed peer list */ 
    UINT4        u4Vni;       /*Denotes the multi homed VNI value */
    tMbsmSlotInfo   *pSlotInfo;
    UINT1        u1DFFlag; /* Dentoes whether the VTEP is DF or not */ 
    UINT1        u1Pad[3];
}tVxlanMbsmHwBUMMHPeerInfo; 

typedef struct { 
    UINT4        u4VniNumber;  /* Denotes number of Multihomed peers */ 
    UINT4        u4VlanId;
    tMbsmSlotInfo   *pSlotInfo;
}tVxlanMbsmHwVlanPortInfo; 

#endif

typedef struct _VxlanHwInfo {
    union
    {
        tVxlanHwNveInfo         VxlanHwNveInfo; /* NVE database */
        tVxlanHwMcastInfo       VxlanHwMcastInfo; /* Multicast database */
        tVxlanHwVniVlanInfo     VxlanHwVniVlanInfo; /* VNI - VLAN mapping */
        UINT4                   u4VxlanUdpPortNo; /* UDP port number */
        tIPvXAddr               SwitchVxlanDipAddr;   /* switch Vxlan DIP */
        tMacAddr                SwitchVxlanMac;       /* switch Vxlan MAC */
        tVxlanHwBUMReplicaInfo  VxlanHwBUMReplicaInfo;/* VNI - VLAN mapping */
        tVxlanHwBUMMHPeerInfo   VxlanHwBUMMHPeerInfo; /* MH Peer database */
        tVxlanHwVlanPortInfo    VxlanHwVlanPortInfo;  /* VLAN - Loopback Port */
#ifdef MBSM_WANTED
        tVxlanMbsmHwNveInfo         VxlanMbsmHwNveInfo; /* NVE database */
        tVxlanMbsmHwMcastInfo       VxlanMbsmHwMcastInfo; /* Multicast database */
        tVxlanMbsmHwVniVlanInfo     VxlanMbsmHwVniVlanInfo; /* VNI - VLAN mapping */
        tVxlanMbsmHwUdpPortInfo     VxlanMbsmHwUdpPorInfo; /* UDP port number */
        tVxlanMbsmHwBUMReplicaInfo  VxlanMbsmHwBUMReplicaInfo;/* VNI - VLAN mapping */
        tVxlanMbsmHwBUMMHPeerInfo   VxlanMbsmHwBUMMHPeerInfo; /* MH Peer database */
        tVxlanMbsmHwVlanPortInfo    VxlanMbsmHwVlanPortInfo;  /* VLAN - Loopback Port */
#endif
    } unHwParam;
    UINT4     u4InfoType; /* Information type to be set or queried from H/w */
#define VxlanNpHwNveInfo        unHwParam.VxlanHwNveInfo
#define VxlanNpHwVniVlanInfo    unHwParam.VxlanHwVniVlanInfo
#define VxlanNpHwBUMReplicaInfo unHwParam.VxlanHwBUMReplicaInfo
#define VxlanNpHwVlanPortInfo   unHwParam.VxlanHwVlanPortInfo
#define u4VxlanNpHwUdpPortNo    unHwParam.u4VxlanUdpPortNo
#define VxlanNpHwMcastInfo      unHwParam.VxlanHwMcastInfo 
#ifdef MBSM_WANTED
#define VxlanNpMbsmHwInitInfo       unHwParam.VxlanMbsmHwInitInfo
#define VxlanNpMbsmHwNveInfo        unHwParam.VxlanMbsmHwNveInfo
#define VxlanNpMbsmHwUdpPortInfo    unHwParam.VxlanMbsmHwUdpPorInfo
#define VxlanNpMbsmHwVniVlanInfo    unHwParam.VxlanMbsmHwVniVlanInfo
#define VxlanNpMbsmHwBUMReplicaInfo unHwParam.VxlanMbsmHwBUMReplicaInfo
#define VxlanNpMbsmHwVlanPortInfo   unHwParam.VxlanMbsmHwVlanPortInfo
#define u4VxlanNpMbsmHwUdpPortNo    unHwParam.u4VxlanMbsmUdpPortNo
#define VxlanNpMbsmHwMcastInfo      unHwParam.VxlanMbsmHwMcastInfo 
#endif
}tVxlanHwInfo;

#define VXLAN_HW_UDP_PORT               1
#define VXLAN_HW_NVE_DATABASE           2
#define VXLAN_HW_MCAST_DATABASE         3
#define VXLAN_HW_VNI_VLAN_MAPPING       4
#define VXLAN_HW_MAC                    5
#define VXLAN_HW_LOCAL_IP               6
#define VXLAN_HW_BUM_REPLICA_SOFTWARE   7
#define VXLAN_HW_INIT                   8
#define VXLAN_HW_DEL_DMAC               9
#define VXLAN_HW_MH_PEER_DATABASE       10
#define VXLAN_HW_VLAN_PORT              11
#define VXLAN_HW_GET_STATS              12

#ifdef MBSM_WANTED
#define VXLAN_MBSM_HW_UDP_PORT               13
#define VXLAN_MBSM_HW_NVE_DATABASE           14
#define VXLAN_MBSM_HW_VNI_VLAN_MAPPING       16
#define VXLAN_MBSM_HW_BUM_REPLICA_SOFTWARE   19
#define VXLAN_MBSM_HW_INIT                   20
#define VXLAN_MBSM_HW_MCAST_DATABASE         21
#define VXLAN_MBSM_HW_NVE_L2_TABLE           22
#define VXLAN_MBSM_HW_BUM_REPLICA_MCAST      23
#define VXLAN_MBSM_HW_VNI_VLAN_PORT_MAPPING  24
#endif

#define VXLAN_HW_NVE_L2_TABLE           25
#define VXLAN_HW_BUM_REPLICA_MCAST           26
#define VXLAN_HW_VNI_VLAN_PORT_MAPPING       27

#define VXLAN_HW_CONFIGURE   1
#define VXLAN_HW_CLEAR       2
#define VXLAN_HW_GET         3
#define VXLAN_HW_MH_PEER_LIST          10
#define VXLAN_HW_MAX_NP      32 /* Used in IPC Wait status for REMOTE NP CALL*/
#define VXLAN_HW_L2VNI_TYPE  1
#define VXLAN_HW_L3VNI_TYPE  2

#define VXLAN_HW_VLAN_TAGGED   1
#define VXLAN_HW_VLAN_UNTAGGED 0
INT4
FsNpHwVxlanInfo (tVxlanHwInfo * pVxlanHwInfo, 
                 UINT1          u1Flag);
INT4
VxlanAddDmacEntry (tMacAddr NpMacAddr, tVlanId VlanId2, UINT4 u4RemoteVTEPIp);
    INT4
VxlanUpdateMacEntryInQueue(UINT4 u4Vni,tMacAddr VxlanMac,INT4 i4Port,UINT4 u4Flag);

VOID
VxlanRemoveDmacEntry (tMacAddr NpMacAddr, tVlanId VlanId2);

VOID
VxlanRemoveDMacFromHw (tVlanId VlanId, tMacAddr pDMac);

VOID
VxlanUpdateMacEntry(UINT4 u4Vni,tMacAddr VxlanMac,UINT4 u4Port,UINT1 u1Flag);

VOID
VxlanUpdateNvePortEntry(UINT1 *pau1Address,INT4 i4AddessType,UINT4 u4NetPort,UINT4 u4BUMPort);
VOID
VxlanUpdateAccessPort(UINT4 u4IfIndex,UINT4 u4AccessPort,
                      UINT4 u4VniId, UINT4 u4VlanId);
#endif

