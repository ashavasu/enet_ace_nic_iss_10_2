/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlnmpbnp.h,v 1.14 2015/04/25 12:26:06 siva Exp $
 *
 * Description: This file contains the function implementations  of FS NP-API.
 ****************************************************************************/
#ifndef _VLNMPBNP_H
#define _VLNMPBNP_H

#include "l2iwf.h"
#include "evcnp.h"

INT4
FsMiVlanHwSetProviderBridgePortType PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex,
                                     UINT4 u4PortType));

INT4
FsMiVlanHwSetPortSVlanTranslationStatus PROTO ((UINT4 u4ContextId,
                                                UINT4 u4IfIndex,
                                                 UINT1 u1Status));
INT4
FsMiVlanHwAddSVlanTranslationEntry PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex,
                                    UINT2 u2LocalSVlan, UINT2 u2RelaySVlan));
INT4
FsMiVlanHwDelSVlanTranslationEntry PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex,
                                    UINT2 u2LocalSVlan, UINT2 u2RelaySVlan));

INT4
FsMiVlanHwSetPortEtherTypeSwapStatus PROTO ((UINT4 u4ContextId,
                                             UINT4 u4IfIndex,
                                             UINT1 u1Status));

INT4
FsMiVlanHwAddEtherTypeSwapEntry PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, 
                                        UINT2 u2LocalEtherType, 
                                        UINT2 u2RelayEtherType));

INT4
FsMiVlanHwDelEtherTypeSwapEntry PROTO ((UINT4 u4ContextId,
                                        UINT4 u4IfIndex,
                                        UINT2 u2LocalEtherType,
                                        UINT2 u2RelayEtherType));

INT4
FsMiVlanHwAddSVlanMap PROTO ((UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap));

INT4
FsMiVlanHwDeleteSVlanMap PROTO ((UINT4 u4ContextId,
                                 tVlanSVlanMap VlanSVlanMap));

INT4
FsMiVlanHwSetPortSVlanClassifyMethod PROTO ((UINT4 u4ContextId,
                                             UINT4 u4IfIndex,
                                             UINT1 u1TableType));
INT4
FsMiVlanHwPortMacLearningLimit PROTO ((UINT4 u4ContextId,
                                       UINT4 u4IfIndex,
                                       UINT4 u4MacLimit));
INT4
FsMiVlanHwMulticastMacTableLimit PROTO ((UINT4 u4ContextId, UINT4 u4MacLimit));

INT4
FsMiVlanHwSetPortCustomerVlan (UINT4 u4ContextId, UINT4 u4IfIndex,
                               tVlanId CVlanId);
INT4
FsMiVlanHwResetPortCustomerVlan (UINT4 u4ContextId, UINT4 u4IfIndex);


INT4
FsMiVlanHwCreateProviderEdgePort (UINT4 u4ContextId, UINT4 u4IfIndex, 
                                  tVlanId SVlanId, tHwVlanPbPepInfo PepConfig);
INT4
FsMiVlanHwDelProviderEdgePort (UINT4 u4ContextId,UINT4 u4IfIndex, 
                               tVlanId SVlanId);
INT4
FsMiVlanHwSetPepPvid (UINT4 u4ContextId,UINT4 u4IfIndex, tVlanId SVlanId, 
                      tVlanId Pvid);

INT4
FsMiVlanHwSetPepAccFrameType (UINT4 u4ContextId,UINT4 u4IfIndex, 
                              tVlanId SVlanId, UINT1 u1AccepFrameType);

INT4
FsMiVlanHwSetPepDefUserPriority (UINT4 u4ContextId,UINT4 u4IfIndex, 
                                 tVlanId SVlanId, INT4 i4DefUsrPri);


INT4
FsMiVlanHwSetPepIngFiltering (UINT4 u4ContextId,UINT4 u4IfIndex, 
                              tVlanId SVlanId, UINT1 u1IngFilterEnable);


INT4
FsMiVlanHwSetPcpEncodTbl (UINT4 u4ContextId,UINT4 u4IfIndex, 
                          tHwVlanPbPcpInfo NpPbVlanPcpInfo);

INT4
FsMiVlanHwSetPcpDecodTbl (UINT4 u4ContextId,UINT4 u4IfIndex, 
                          tHwVlanPbPcpInfo NpPbVlanPcpInfo);

INT4
FsMiVlanHwSetPortUseDei (UINT4 u4ContextId,UINT4 u4IfIndex, UINT1 u1UseDei);


INT4
FsMiVlanHwSetPortReqDropEncoding (UINT4 u4ContextId,UINT4 u4IfIndex, 
                                  UINT1 u1ReqDrpEncoding);

INT4
FsMiVlanHwSetPortPcpSelection (UINT4 u4ContextId,UINT4 u4IfIndex, 
                               UINT2 u2PcpSelection);

INT4
FsMiVlanHwSetServicePriRegenEntry (UINT4 u4ContextId,UINT4 u4IfIndex, 
                                   tVlanId SVlanId, INT4 i4RecvPriority, 
                                   INT4 i4RegenPriority);
INT4
FsMiVlanHwSetTunnelMacAddress (UINT4 u4ContextId, tMacAddr MacAddr, 
                               UINT2 u2Protocol);
INT4
FsMiVlanHwSetCvidUntagPep (UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap);

INT4
FsMiVlanHwSetCvidUntagCep (UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap);

INT4
FsMiVlanHwGetNextSVlanMap (UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap,
                           tVlanSVlanMap *pRetVlanSVlanMap);

INT4
FsMiVlanHwGetNextSVlanTranslationEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                    tVlanId u2LocalSVlan,
                                    tVidTransEntryInfo *pVidTransEntryInfo);

INT4
FsMiVlanHwSetEvcAttribute (UINT4 u4ContextId, INT4 i4Action, tEvcInfo *pIssEvcInfo);

INT4 FsMiVlanHwSetCVlanStat PROTO ((tHwVlanCVlanStat VlanSVlanMap));

INT4 FsMiVlanHwGetCVlanStat PROTO  ((UINT2 u2Port ,UINT2 u2CVlanId,UINT1 u1StatsType, UINT4 *pu4VlanStatsValue));

INT4 FsMiVlanHwClearCVlanStat PROTO (( UINT2 u2Port,
                             UINT2 u2CVlanId));



#ifdef MBSM_WANTED
INT4
FsMiVlanMbsmHwSetProviderBridgePortType (UINT4 u4ContextId, UINT4 u4IfIndex,
                                         UINT4 u4PortType,
                                         tMbsmSlotInfo * pSlotInfo);
INT4
FsMiVlanMbsmHwSetPortSVlanTranslationStatus (UINT4 u4ContextId, UINT4 u4IfIndex,
                                             UINT1 u1Status,
                                             tMbsmSlotInfo * pSlotInfo);
INT4
FsMiVlanMbsmHwAddSVlanTranslationEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                        UINT2 u2LocalSVlan, UINT2 u2RelaySVlan,
                                        tMbsmSlotInfo * pSlotInfo);

INT4
FsMiVlanMbsmHwSetPortEtherTypeSwapStatus (UINT4 u4ContextId, UINT4 u4IfIndex,
                                          UINT1 u1Status,
                                          tMbsmSlotInfo * pSlotInfo);
INT4
FsMiVlanMbsmHwAddEtherTypeSwapEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                     UINT2 u2LocalEtherType,
                                     UINT2 u2RelayEtherType,
                                     tMbsmSlotInfo * pSlotInfo);

INT4
FsMiVlanMbsmHwAddSVlanMap (UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap,
                           tMbsmSlotInfo * pSlotInfo);

INT4
FsMiVlanMbsmHwSetPortSVlanClassifyMethod (UINT4 u4ContextId, UINT4 u4IfIndex,
                                          UINT1 u1TableType,
                                          tMbsmSlotInfo * pSlotInfo);
INT4
FsMiVlanMbsmHwPortMacLearningLimit (UINT4 u4ContextId, UINT4 u4IfIndex,
                                    UINT4 u4MacLimit, tMbsmSlotInfo * pSlotInfo);

INT4
FsMiVlanMbsmHwSetPortCustomerVlan (UINT4 u4ContextId, UINT4 u4IfIndex,
                                   tVlanId CVlanId, tMbsmSlotInfo * pSlotInfo);

INT4
FsMiVlanMbsmHwCreateProviderEdgePort (UINT4 u4ContextId, UINT4 u4IfIndex,
                                      tVlanId SVlanId,
                                      tHwVlanPbPepInfo PepConfig,
                                      tMbsmSlotInfo * pSlotInfo);
INT4
FsMiVlanMbsmHwSetPcpEncodTbl (UINT4 u4ContextId, UINT4 u4IfIndex,
                              tHwVlanPbPcpInfo NpPbVlanPcpInfo,
                              tMbsmSlotInfo * pSlotInfo);

INT4
FsMiVlanMbsmHwSetPcpDecodTbl (UINT4 u4ContextId, UINT4 u4IfIndex,
                              tHwVlanPbPcpInfo NpPbVlanPcpInfo,
                              tMbsmSlotInfo * pSlotInfo);

INT4
FsMiVlanMbsmHwSetPortUseDei (UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1UseDei,
                             tMbsmSlotInfo * pSlotInfo);

INT4
FsMiVlanMbsmHwSetPortReqDropEncoding (UINT4 u4ContextId, UINT4 u4IfIndex,
                                      UINT1 u1ReqDrpEncoding,
                                      tMbsmSlotInfo * pSlotInfo);

INT4
FsMiVlanMbsmHwSetPortPcpSelection (UINT4 u4ContextId, UINT4 u4IfIndex,
                                   UINT2 u2PcpSelection,
                                   tMbsmSlotInfo * pSlotInfo);
INT4
FsMiVlanMbsmHwSetServicePriRegenEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                       tVlanId SVlanId, INT4 i4RecvPriority,
                                       INT4 i4RegenPriority,
                                       tMbsmSlotInfo * pSlotInfo);

INT4
FsVlanMbsmAddCVidEntry (INT4 i4BcmUnit, INT4 i4BcmPort,
                        tVlanSVlanMap VlanSVlanMap,
                        tMbsmSlotInfo * pSlotInfo);
INT4
FsMiVlanMbsmHwMulticastMacTableLimit (UINT4 u4ContextId, UINT4 u4MacLimit,
                                      tMbsmSlotInfo * pSlotInfo);
#endif
#endif

