/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bfdnp.h,v 1.6 2012/12/18 10:36:15 siva Exp $
 *
 * Description: Exported file for BFD NP-API.
 *
 *******************************************************************/
#ifndef __BFDNP_H
#define __BFDNP_H

#ifdef _BFDNP_C_
/* For BDF OffLoading */
UINT1 gu1BfdIsHwInitialized = OSIX_FALSE;
UINT1 gu1BfdIsCallbackRegistered = OSIX_FALSE;
#else
extern UINT1 gu1BfdIsHwInitialized;
extern UINT1 gu1BfdIsCallbackRegistered;
#endif

typedef  VOID (*FsMiBfdHwSessionCb)
             (UINT4, tBfdSessionHandle*, tBfdHwhandle*, tBfdEventType,
                    BOOL1 bActionFlag, tBfdParams*, tBfdOffPktBuf *);


/* Callback provided in the NPAPI */
VOID FsMiBfdHwSessionCallBack 
PROTO ((UINT4     u4ContextId, 
        tBfdSessionHandle  *pBfdSessHandle,tBfdHwhandle  *pBfdHwSessHandle, 
        tBfdEventType    eBfdEventType,BOOL1   bActionFlag, 
        tBfdParams    *pBfdParams, tBfdOffPktBuf *));




/* NPAPI function prototypes */
INT4 FsMiBfdHwStartBfd 
PROTO (( UINT4                     u4ContextId,
            tBfdSessionHandle           *pBfdSessHandle,  
            tBfdSessionPathInfo         *pBfdSessPathInfo, 
            tBfdParams                  *pBfdParams,
            tBfdOffPktBuf               *pBfdTxCtrlPkt,
            tBfdOffPktBuf               *pBfdRxCtrlPkt,
            tBfdSessHwStartBfdCallFlag  eBfdSessHwStartBfdCallFlag,
            tBfdHwhandle                *pBfdSessHwHandle));


INT4 FsMiBfdHwStopBfd 
PROTO (( UINT4                     u4ContextId,
            tBfdSessionHandle      *pBfdSessHandle,  
            tBfdHwhandle           *pBfdSessHwHandle,
            tBfdSessHwStopBfdCallFlag eBfdSessHwStopBfdCallFlag));

INT4 FsMiBfdHwRegisterSessionCb 
PROTO (( UINT4                   u4ContextId, 
            tBfdEventType           eBfdEventType,
            FsMiBfdHwSessionCb BfdOffCallBack)); 

INT4 FsMiBfdGetHwBfdParameters 
PROTO ((
            UINT4                 u4ContextId,
            tBfdSessionHandle     *pBfdSessHandle, 
            tBfdHwhandle        *pBfdSessHwHandle,
            tBfdParams            *pBfdParams)); 

INT4 FsMiBfdGetHwBfdSessionStats 
PROTO ((
            UINT4              u4ContextId,
            tBfdSessionHandle *pBfdSessHandle, 
            tBfdHwhandle      *pBfdSessHwHandle,
            tBfdSessStats  *pSessionStats));

INT4 FsMiBfdHwClearBfdStats 
PROTO ((  
            UINT4                  u4ContextId,
            tBfdSessionHandle      *pBfdSessHandle,
            tBfdHwhandle           *pBfdSessHwHandle, 
            BOOL1                   bClearAllSessStats)); 

#endif /*__BFDNP_H*/
