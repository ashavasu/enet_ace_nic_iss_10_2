/*****************************************************************************/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/*****************************************************************************/
/*    FILE  NAME            : isspinp.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : Port Isolation                                 */
/*    MODULE NAME           : ISS                                            */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Linux                                          */
/*    DATE OF FIRST RELEASE : 23 July 2010                                   */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This header file contains the prototype        */
/*                            and data structure declarations of ISS Port    */
/*                            isolation related NP-API.                      */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    23 July 2010           Initial Creation                        */
/*---------------------------------------------------------------------------*/

#ifndef ISSPINP_H 
#define ISSPINP_H

#include "isspi.h"

INT4 IssHwConfigPortIsolationEntry PROTO ((tIssHwUpdtPortIsolation *));
INT4 IssPIMbsmHwConfigPortIsolationEntry PROTO ((tIssHwUpdtPortIsolation *,
                                         tIssMbsmInfo *));
#endif /* ISSPINP_H */
