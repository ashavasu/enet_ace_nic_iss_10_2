/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ikenp.h,v 1.1 2011/02/16 13:33:03 siva Exp $
 *
 * Description: All prototypes for Network Processor API functions 
 *******************************************************************/
#ifndef __IKENP_H
#define __IKENP_H

VOID FsNpIkeInit PROTO ((VOID));

#endif /* __IKENP_H */
