/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/*                                                                           */
/*  FILE NAME             : rstmpbbnp.h                                      */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                     */
/*  SUBSYSTEM NAME        : RSTP Module                                      */
/*  MODULE NAME           : RSTP                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : -                                                */
/*  AUTHOR                : Aricent Inc.                                     */
/*  DESCRIPTION           : This file contains RSTP NPPBB Function Prototypes*/
/*                                                                           */
/*****************************************************************************/
#ifndef _RSTMPBBNP_H
#define _RSTMPBBNP_H
INT4 FsMiPbbRstHwServiceInstancePtToPtStatus  (UINT4 u4ContextId, 
                                               UINT4 u4VipIndex, 
                                               UINT4 u4Isid, 
                                               UINT1 u1PointToPointStatus);
#endif
