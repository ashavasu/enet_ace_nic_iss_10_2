/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rmonnp.h,v 1.2 2007/02/01 14:52:34 iss Exp $
 *
 * Description: This file contains the function implementations  of 
 *              RMON NP-API.
 ****************************************************************************/

#ifndef _RMONNP_H
#define _RMONNP_H

INT4 FsRmonHwSetEtherStatsTable PROTO ((UINT4, UINT1));
INT4 FsRmonHwGetEthStatsTable PROTO ((UINT4, tRmonEtherStatsNode *));

#endif /* _RMONNP_H */
