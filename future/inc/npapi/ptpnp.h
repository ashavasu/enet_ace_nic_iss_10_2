/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: ptpnp.h,v 1.8 2017/01/12 12:57:32 siva Exp $
 * Description: Contains definitions, prototypes and other NP related
 *              header extensions for PTP module.
 ********************************************************************/
#ifndef _PTPNP_H
#define _PTPNP_H

#include "fsclk.h"

extern UINT2 gu2TimeStamp;

#define PTP_SUCCESS 1
#define PTP_FAILURE 0

typedef struct _PtpHwPtpStatusInfo {
    tMbsmSlotInfo    *pSlotInfo;
    UINT4             u4ContextId;
    UINT4             u4IfIndex;
    UINT4             u4Status;
    ePtpDeviceType    DeviceType;
    UINT1             u1DomainId;
    UINT1             au1Pad[3];
}tPtpHwPtpStatusInfo;

typedef struct _PtpHwTransmitTsOpt {
    tMbsmSlotInfo    *pSlotInfo;
    UINT4             u4ContextId;
    UINT4             u4IfIndex;
    ePtpDeviceType    DeviceType;
    UINT1             u1DomainId;
    UINT1             u1DelayMech;
    BOOL1             bIsTwoStep;
    BOOL1             bSyncOneStep;
    BOOL1             bGetDelayReq;
    BOOL1             bRecalcChkSum;
    BOOL1             bIanaIpTsReq;
    BOOL1             bEnableL2Det;
    BOOL1             bTsEnable;
    BOOL1             bIpV4PtpDetEnable;
    BOOL1             bIpV6PtpDetEnable;
    BOOL1             bL2PtpDetEnable;
    BOOL1             bRecalcCrc;
    BOOL1             bEnableUdpDet;
    UINT1             au1Pad[2];
}tPtpHwTransmitTsOpt;

typedef struct _PtpHwRecvTsOpt {
    tMbsmSlotInfo    *pSlotInfo;
    tIp6Addr          Ip6Addr;
    tPtpHwSrcIdHash   PtpHwSrcIdHash;
    UINT4             u4ContextId;
    UINT4             u4IfIndex;
    UINT4             u4SrcIPAddr;
    UINT4             u4SecondsOff;
    UINT4             u4NanoSecondsOff;
    ePtpDeviceType    DeviceType;
    UINT1             u1DomainId;
    UINT1             u1Version;
    UINT1             u1SecOffset;
    UINT1             u1NanoSecOffset;
    BOOL1             bDomainmatchEnable;
    BOOL1             bUserIpEnable;
    BOOL1             bAltMastTsEnable;
    BOOL1             bL2PtpDetEnable;
    BOOL1             bIpV6PtpDetEnable;
    BOOL1             bIpV4PtpDetEnable;
    BOOL1             bRecvTSEnable;
    BOOL1             bPtpTSAppendEnable;
    BOOL1             bPtpRxTSInsertEnable;
    BOOL1             bPtpTsIanaEnable;
    BOOL1             bCrcErrAcc;
    BOOL1             bUdpChkSumErr;
    BOOL1             bSrcIdHash;
    BOOL1             bIsSlaveOnly;
    UINT1             au1pad[2];
}tPtpHwRecvTsOpt;

typedef struct _PtpHwPtpClk {
    tMbsmSlotInfo    *pSlotInfo;
    FS_UINT8          u8Offset; /*Offset to be programmed*/
    UINT4             u4ContextId;
    UINT4             u4IfIndex;
    ePtpDeviceType    DeviceType;
    UINT4             u4Seconds;
    UINT4             u4NanoSeconds;
    UINT1             u1DomainId;
    INT1              i1OffsetSign; /* Sign which indicates Master 
                                     * leads/lags the slave */
    UINT1             au1Rsvd[2];
}tPtpHwPtpClk;

typedef struct _PtpHwPtpClkRateAdj {
    tMbsmSlotInfo    *pSlotInfo;
    FS_UINT8          u8RatioPerSec;
    UINT4             u4ContextId;
    UINT4             u4IfIndex;
    UINT4             u4RateAdj;
    UINT4             u4NanoSeconds;
    ePtpDeviceType    DeviceType;
    UINT1             u1DomainId;
    UINT1             u1Direction;
    BOOL1             bTempRateAdj;
    UINT1             au1Pad[1];
}tPtpHwPtpClkRateAdj;


typedef struct _PtpHwDmnModeParams {
    tMbsmSlotInfo    *pSlotInfo;
    UINT4             u4ContextId;
    eClkMode          ClkMode; /* Denote the configured clock mode */
    UINT1             u1DomainId;
    UINT1             au1Pad[3];
}tPtpHwDmnModeParams;

/* PTP - PEERDELAY */
typedef struct _PtpHwPeerDelay {
    FS_UINT8          u8PeerMeanDelay;
    UINT4             u4ContextId;
    UINT4             u4IfIndex;
    UINT1             u1DomainId;
    UINT1             au1Pad[3];
} tPtpHwPeerDelay;

/* Data structure to be passed as argument to the H/w routines */
typedef struct _PtpHwInfo {
    union
    {
        tPtpHwPtpStatusInfo PtpHwPtpStatusInfo; 
        tPtpHwTransmitTsOpt PtpHwTransmitTsOpt; 
        tPtpHwRecvTsOpt     PtpHwRecvTsOpt;
        tPtpHwPtpClk        PtpHwPtpClk;
        tPtpHwPtpClkRateAdj PtpHwPtpClkRateAdj;
        tPtpHwDmnModeParams PtpHwDmnModeParams;
        tPtpHwSrcIdHash     PtpHwSrcIdHash;
        tPtpHwLog           PtpHwLog;        /* Pkt Tx/Rx Time stamp Log */
  tPtpHwPeerDelay     PtpHwPeerDelay;  /* PTP - PEERDELAY */
    }unHwParam;
    UINT4     u4InfoType; /* Information type to be set or queried from H/w */
    UINT2     u2PtpTimeStamp; /* Hardware or Software Timestamp */
    UINT1     au1Pad[2];
}tPtpHwInfo;

typedef enum {
   PTP_NP_ETHERTYPE = 1,
   PTP_NP_IPV4_1,
   PTP_NP_IPV4_2,
   PTP_NP_IPV6_1,
   PTP_NP_IPV6_2
  }tPtpHwFilters;
           
/* This is an unified NPAPI function which includes functionality of
 * all NPAPIs provided by this module */
PUBLIC INT4
FsNpHwConfigPtpInfo PROTO ((tPtpHwInfo * pPtpHwPtpInfo));

/* OBSOLETE APIs: START*/
/*  All the below NPAPIs are obsolete and the unified API FsNpHwConfigPtpInfo
 *  recommended to be used for the purpose */

PUBLIC 
INT4 FsNpHwSetPtpStatus PROTO ((tPtpHwPtpStatusInfo * pPtpHwPtpStatusInfo));

PUBLIC INT4 
FsNpHwSetPtpTransmitTSOption PROTO ((tPtpHwTransmitTsOpt * pPtpHwTransmitTsOpt));

PUBLIC 
INT4 FsNpHwSetPtpReceiveTSOption PROTO ((tPtpHwRecvTsOpt * pPtpHwRecvTsOpt));

PUBLIC
INT4 FsNpPtpHwSetClk PROTO ((tPtpHwPtpClk * pPtpHwPtpClk));

PUBLIC
INT4 FsNpPtpHwSetClkAdj PROTO ((tPtpHwPtpClkRateAdj * pPtpHwPtpClkRateAdj));

PUBLIC
INT4 FsNpPtpHwTimeStampCallBack PROTO ((tPtpHwPtpCbParams * pPtpHwPtpCbParams));

PUBLIC INT4
FsNpPtpSetDomainMode PROTO ((tPtpHwDmnModeParams * pPtpHwDmnModeParams));

PUBLIC INT4
FsNpPtpHwSetSrcPortIdHash PROTO ((tPtpHwSrcIdHash * pPtpHwSrcIdHash));

PUBLIC INT4
FsNpPtpHwInit PROTO ((tPtpHwLog  * pPtpHwLog));

PUBLIC INT4
FsNpPtpHwDeInit PROTO ((VOID));
/* OBSOLETE APIs: END*/

#endif /* _PTPNP_H */
