/**************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsbnp.h,v 1.4 2017/10/09 13:14:00 siva Exp $ 
 *
 * Description: All prototypes for Network Processor API functions for ERPS 
 *
 **************************************************************************/
#ifndef _FSBNP_H
#define _FSBNP_H

INT4 FsMiNpFsbHwInit PROTO ((tFsbHwFilterEntry*, tFsbLocRemFilterId *));
INT4 FsFsbHwCreateFilter PROTO ((tFsbHwFilterEntry *, UINT4 *));
INT4 FsFsbHwDeleteFilter PROTO ((tFsbHwFilterEntry *, UINT4));
INT4 FsMiNpFsbHwDeInit PROTO ((tFsbHwFilterEntry *, tFsbLocRemFilterId));
INT4 FsFsbHwCreateSChannelFilter PROTO ((tFsbHwSChannelFilterEntry *,UINT4 *));
INT4 FsFsbHwDeleteSChannelFilter PROTO ((tFsbHwSChannelFilterEntry *,UINT4));
INT4 FsFsbHwSetFCoEParams PROTO ((tFsbHwVlanEntry *pFsbHwVlanEntry));
VOID FsbNpUtlAttachStatsId PROTO ((INT4, UINT4));
VOID FsbNpUtlDetachStatsId PROTO ((INT4, UINT4));

#ifdef MBSM_WANTED
INT4 FsMiNpFsbMbsmHwInit (tFsbHwFilterEntry * pFsbHwFilterEntry , tFsbLocRemFilterId *pFsbLocRemFilterId );
INT4 FsFsbMbsmHwCreateFilter (tFsbHwFilterEntry *pFsbHwFilterEntry , UINT4 *pu4FilterId );
#endif  /* MBSM_WANTED */

#endif /*_FSBNP_H*/
