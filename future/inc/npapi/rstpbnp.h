#ifndef _RSTPBNP_H
#define _RSTPBNP_H

INT1
FsPbRstpNpSetPortState (UINT4 u4IfIndex, tVlanId SVlanId, UINT1 u1Status);
INT1
FsPbRstNpGetPortState (UINT4 u4IfIndex, tVlanId SVlanId, UINT1 *pu1Status);
#ifdef MBSM_WANTED
INT1
FsPbRstpMbsmNpSetPortState (UINT4 u4IfIndex, tVlanId Svid, UINT1 u1Status, 
                            tMbsmSlotInfo *pSlotInfo);
#endif
#endif /*_RSTPBNP_H*/

