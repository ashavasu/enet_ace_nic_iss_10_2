/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: npapi.h,v 1.38 2017/11/14 07:31:12 siva Exp $
 *
 * Description: Exported file for FS NP-API.
 *
 *******************************************************************/
#ifndef _NPAPI_H
#define _NPAPI_H

#include "fsvlan.h"
#include "bridge.h"
#include "snp.h"
#include "erps.h"
#include "asnp.h"
#include "asnpport.h"

#ifdef NPSIM_ASYNC_WANTED
#include "asnpsim.h"
#endif /* NPSIM_ASYNC_WANTED */

#define FNP_SUCCESS              1
#define FNP_FAILURE              0

#define FNP_VLAN_DISABLED_BUT_NOT_DELETED 3
#define FNP_NOT_SUPPORTED        4  /* The NPAPI wrapper should return this
                                       value if the supporting driver API
                                       is not present. The protocol will 
                                       try to provide the same functionality 
                                       by calling some other NPAPI's
                                     */

#define FNP_FALSE                0
#define FNP_TRUE                 1
#define FNP_LOOPBACK    2
#define FNP_NOLOOPBACK  3
#define FNP_BLOCKING    4
#define FNP_FORWARDING  5
#define FNP_LINKUP_STP_FORWARD 6
#define FNP_LINKUP_STP_BLOCK  7
#define FNP_PTP_FOLLOWUP          8

#define FNP_ONE                  1
#define FNP_ZERO                 0

/*Ipv4 Unicasting Related Macros Starts Here */
#define FNP_ALL_BITS_SET        0xFFFFFFFF
#define FNP_ALL_BITS_RESET      0
#define FNP_ACTIVE              1

#define IPMC_MRP                  1
#define IPMC_IGS                  2
#define IPMC_MLDS                  3

/* A change in this should be relatively should be updated in 
 * the StatsArray defined in future/npapi/<target>api.h file & vice-versa.
 */
/* Used by NpBcmStatsArray in npapi/bcmapi.h and equivalents. */
#define NP_STAT_UNSUPPORTED       -1

#define NP_IP_ISIS_ENABLE    2
#define NP_IP_ISIS_DISABLE   1

typedef enum {
    NP_ENTRY_ADD,
    NP_ENTRY_DELETE
}tNpEntryAction;


enum {
    /* Following are the Statistics supported in the BCM */
    /*** RFC 1213 ***/

    NP_STAT_IF_IN_OCTETS = 0,
    NP_STAT_IF_IN_UCAST_PKTS,
    NP_STAT_IF_IN_NUCAST_PKTS,
    NP_STAT_IF_IN_DISCARDS,
    NP_STAT_IF_IN_ERRORS,
    NP_STAT_IF_IN_UNKNOWN_PROTOS,
    NP_STAT_IF_OUT_OCTETS,
    NP_STAT_IF_OUT_UCAST_PKTS,
    NP_STAT_IF_OUT_NUCAST_PKTS,
    NP_STAT_IF_OUT_DISCARDS,
    NP_STAT_IF_OUT_ERRORS,
    NP_STAT_IF_OUT_QLEN,
    NP_STAT_IP_IN_RECIEVES,
    NP_STAT_IP_IN_HDR_ERRORS,
    NP_STAT_IP_IN_FORW_DATAGRAMS,
    NP_STAT_IP_IN_DISCARDS,

    /*** RFC 1493 ***/

    NP_STAT_DOT1D_BASE_PORT_DLY_EXCEEDED_DISCARDS,
    NP_STAT_DOT1D_BASE_PORT_MTU_EXCEEDED_DISCARDS,
    NP_STAT_DOT1D_TP_PORT_IN_FRAMES,
    NP_STAT_DOT1D_TP_PORT_OUT_FRAMES,
    NP_STAT_DOT1D_TP_PORT_IN_DISCARDS,


    /*** RFC 1757 (EtherStat) ***/

    NP_STAT_ETHER_DROP_EVENTS,
    NP_STAT_ETHER_MCAST_PKTS,
    NP_STAT_ETHER_BCAST_PKTS,
    NP_STAT_ETHER_UNDERSIZE_PKTS,
    NP_STAT_ETHER_FRAGMENTS,
    NP_STAT_ETHER_PKTS_64_OCTETS,
    NP_STAT_ETHER_PKTS_65_TO_127_OCTETS,
    NP_STAT_ETHER_PKTS_128_TO_255_OCTETS,
    NP_STAT_ETHER_PKTS_256_TO_511_OCTETS,
    NP_STAT_ETHER_PKTS_512_TO_1023_OCTETS,
    NP_STAT_ETHER_PKTS_1024_TO_1518_OCTETS,
    NP_STAT_ETHER_OVERSIZE_PKTS,
    NP_STAT_ETHER_JABBERS,
    NP_STAT_ETHER_OCTETS,
    NP_STAT_ETHER_PKTS,
    NP_STAT_ETHER_COLLISIONS,
    NP_STAT_ETHER_CRC_ALIGN_ERRORS,
    NP_STAT_ETHER_TX_NO_ERRORS,
    NP_STAT_ETHER_RX_NO_ERRORS,

    /*** RFC 2665 ***/

    NP_STAT_DOT3_ALIGNMENT_ERRORS,
    NP_STAT_DOT3_FCS_ERRORS,
    NP_STAT_DOT3_SINGLE_COLLISION_ERRORS,
    NP_STAT_DOT3_MULTIPLE_COLLISION_ERRORS,
    NP_STAT_DOT3_SQET_TEST_ERRORS,
    NP_STAT_DOT3_DEFERRED_TRANSMISSIONS,
    NP_STAT_DOT3_LATE_COLLISIONS,
    NP_STAT_DOT3_EXCESSIVE_COLLISIONS,
    NP_STAT_DOT3_INTERNAL_MAC_TX_ERRORS,
    NP_STAT_DOT3_CARRIER_SENSE_ERRORS,
    NP_STAT_DOT3_FRAME_TOO_LONGS,
    NP_STAT_DOT3_INTERNAL_MAC_RX_ERRORS,
    NP_STAT_DOT3_SYMBOL_ERRORS,
    NP_STAT_DOT3_CONTROL_IN_UNK_OPCODES,
    NP_STAT_DOT3_IN_PAUSE_FRAMES,
    NP_STAT_DOT3_OUT_PAUSE_FRAMES,
    
    /*** RFC 2233 ***/

    NP_STAT_IF_HC_IN_OCTETS,
    NP_STAT_IF_HC_IN_UCAST_PKTS,
    NP_STAT_IF_HC_IN_MCAST_PKTS,
    NP_STAT_IF_HC_IN_BCAST_PKTS,
    NP_STAT_IF_HC_OUT_OCTETS,
    NP_STAT_IF_HC_OUT_UCAST_PKTS,
    NP_STAT_IF_HC_OUT_MCAST_PKTS,
    NP_STAT_IF_HC_OUT_BCAST_PKTS,

    /*** Additional Broadcom stats ***/

    NP_STAT_IPMC_BRIDGED_PKTS,
    NP_STAT_IPMC_ROUTED_PKTS,
    NP_STAT_IPMC_IN_DROPPED_PKTS,
    NP_STAT_IPMC_OUT_DROPPED_PKTS,
                                                                                             
    NP_STAT_PKTS_1519_TO_1522_OCTETS,
    NP_STAT_PKTS_1522_TO_2047_OCTETS,
    NP_STAT_PKTS_2048_TO_4095_OCTETS,
    NP_STAT_PKTS_4095_TO_9216_OCTETS,


/* Unsupported Statistics in BCM */
    NP_STAT_IF_IN_MCAST_PKTS,
    NP_STAT_IF_IN_BCAST_PKTS,
    NP_STAT_IF_OUT_MCAST_PKTS,
    NP_STAT_IF_OUT_BCAST_PKTS,
    NP_STAT_DOT1D_PORT_LEARNED_ENTRY_DISCARDS,
    NP_STAT_DOT1D_PORT_BCAST_OUT_PKTS,
    NP_STAT_DOT1D_PORT_MCAST_OUT_PKTS,

    NP_STAT_VLAN_PORT_IN_DISCARDS,
    NP_STAT_VLAN_PORT_IN_FRAMES,
    NP_STAT_VLAN_PORT_OUT_FRAMES,
    NP_STAT_VLAN_PORT_IN_DISCARDS_OVERFLOW,
    NP_STAT_VLAN_PORT_OUT_OVERFLOW,
    NP_STAT_VLAN_PORT_IN_OVERFLOW,
    NP_STAT_MAX,
    NP_STAT_IF_IN_BAD_OCTETS,
    NP_STAT_IF_IN_SYMBOLS,

    NP_STAT_VLAN_UCAST_IN_FRAMES,
    NP_STAT_VLAN_MCAST_BCAST_IN_FRAMES,
    NP_STAT_VLAN_UNKNOWN_UCAST_OUT_FRAMES,
    NP_STAT_VLAN_UCAST_OUT_FRAMES,
    NP_STAT_VLAN_BCAST_OUT_FRAMES,
    NP_STAT_IF_HC_IN_DISCARDS,
    NP_STAT_IF_HC_IN_ERRORS,
    NP_STAT_IF_HC_IN_UNKNOWN_PROTOS,
    NP_STAT_IF_HC_OUT_DISCARDS,
    NP_STAT_IF_HC_IN_OUT_ERRORS,
    NP_STAT_ETHER_OUT_FCS_ERRORS,

    /* Vlan Stat Counters for RMON per Vlan */ 
    NP_STAT_VLAN_ETHER_DROP_EVENTS,           
    NP_STAT_VLAN_ETHER_MCAST_PKTS,       
    NP_STAT_VLAN_ETHER_BCAST_PKTS,          
    NP_STAT_VLAN_ETHER_UNDERSIZE_PKTS,       
    NP_STAT_VLAN_ETHER_FRAGMENTS,               
    NP_STAT_VLAN_ETHER_PKTS_64_OCTETS,           
    NP_STAT_VLAN_ETHER_PKTS_65_TO_127_OCTETS,      
    NP_STAT_VLAN_ETHER_PKTS_128_TO_255_OCTETS,       
    NP_STAT_VLAN_ETHER_PKTS_256_TO_511_OCTETS,       
    NP_STAT_VLAN_ETHER_PKTS_512_TO_1023_OCTETS,        
    NP_STAT_VLAN_ETHER_PKTS_1024_TO_1518_OCTETS,         
    NP_STAT_VLAN_ETHER_OVERSIZE_PKTS,                    
    NP_STAT_VLAN_ETHER_JABBERS,                          
    NP_STAT_VLAN_ETHER_OCTETS,                           
    NP_STAT_VLAN_ETHER_PKTS,                             
    NP_STAT_VLAN_ETHER_COLLISIONS,                       
    NP_STAT_VLAN_ETHER_CRC_ALIGN_ERRORS,
    NP_STAT_VLAN_ETHER_FCS_ERRORS,

    NP_STAT_ETHER_RX_PKTS_64_OCTETS,
    NP_STAT_ETHER_RX_PKTS_65_TO_127_OCTETS,
    NP_STAT_ETHER_RX_PKTS_128_TO_255_OCTETS,
    NP_STAT_ETHER_RX_PKTS_256_TO_511_OCTETS,
    NP_STAT_ETHER_RX_PKTS_512_TO_1023_OCTETS,
    NP_STAT_ETHER_RX_PKTS_1024_TO_1518_OCTETS,
    NP_STAT_ETHER_RX_OVERSIZE_PKTS,

    /* Vlan and Interface based counters */
    NP_STAT_IF_IN_PKTS,
    NP_STAT_VLAN_IN_FRAMES,
    NP_STAT_VLAN_IN_BYTES,
    NP_STAT_VLAN_OUT_FRAMES,
    NP_STAT_VLAN_OUT_BYTES,
    NP_STAT_VLAN_DISCARD_FRAMES,
    NP_STAT_VLAN_DISCARD_BYTES,
    /* Customer Vlan  based counters */
 
    NP_STAT_CUSTOMER_VLAN_IN_FRAMES,
    NP_STAT_CUSTOMER_VLAN_IN_BYTES,
    NP_STAT_CUSTOMER_VLAN_OUT_FRAMES,
    NP_STAT_CUSTOMER_VLAN_OUT_BYTES,

    /* Dot3 Stats table RFC 2665. */
    NP_STAT_DOT3_HC_ALIGNMENT_ERRORS,
    NP_STAT_DOT3_HC_FCS_ERRORS,
    NP_STAT_DOT3_HC_INTERNAL_MAC_TX_ERRORS,
    NP_STAT_DOT3_HC_FRAME_TOO_LONGS,
    NP_STAT_DOT3_HC_INTERNAL_MAC_RX_ERRORS,
    NP_STAT_DOT3_HC_SYMBOL_ERRORS,

    NP_STAT_PORT_DUPLEX_STATUS,
    NP_ETH_STATS_BRATE_CONTROLABILITY,
    NP_ETH_STATS_RATE_CONTROL_STATUS,

    /* PFC Stats */
    NP_PFC_STATS_RX_PFC_CONTROL_FRAME,
    NP_PFC_STATS_TX_PFC_CONTROL_FRAME,

    NP_PFC_STATS_RX_PFC_FRAME_XON_PRI0,
    NP_PFC_STATS_RX_PFC_FRAME_XON_PRI1,
    NP_PFC_STATS_RX_PFC_FRAME_XON_PRI2,
    NP_PFC_STATS_RX_PFC_FRAME_XON_PRI3,
    NP_PFC_STATS_RX_PFC_FRAME_XON_PRI4,
    NP_PFC_STATS_RX_PFC_FRAME_XON_PRI5,
    NP_PFC_STATS_RX_PFC_FRAME_XON_PRI6,
    NP_PFC_STATS_RX_PFC_FRAME_XON_PRI7,

    NP_PFC_STATS_RX_PFC_FRAME_PRI0,
    NP_PFC_STATS_RX_PFC_FRAME_PRI1,
    NP_PFC_STATS_RX_PFC_FRAME_PRI2,
    NP_PFC_STATS_RX_PFC_FRAME_PRI3,
    NP_PFC_STATS_RX_PFC_FRAME_PRI4,
    NP_PFC_STATS_RX_PFC_FRAME_PRI5,
    NP_PFC_STATS_RX_PFC_FRAME_PRI6,
    NP_PFC_STATS_RX_PFC_FRAME_PRI7,

    NP_PFC_STATS_TX_PFC_FRAME_PRI0,
    NP_PFC_STATS_TX_PFC_FRAME_PRI1,
    NP_PFC_STATS_TX_PFC_FRAME_PRI2,
    NP_PFC_STATS_TX_PFC_FRAME_PRI3,
    NP_PFC_STATS_TX_PFC_FRAME_PRI4,
    NP_PFC_STATS_TX_PFC_FRAME_PRI5,
    NP_PFC_STATS_TX_PFC_FRAME_PRI6,
    NP_PFC_STATS_TX_PFC_FRAME_PRI7,

   /* VRF statistics*/
    VRF_GET_UNICAST_COUNTERS,
    VRF_GET_MULTICAST_COUNTERS,
    VRF_GET_BROADCAST_COUNTERS

};

typedef enum {
    FNP_NONE = 0,
    FNP_XCAT,
    FNP_LION,
    FNP_LION_DB,
    FNP_DX167
}tNpChipSet;



/* This enum supports the various error codes returned by
 *  * NPAPI layer to the caller.
 *   */

typedef enum NpErrCodes
{
    FNP_RESERVED_VLAN_EXHAUST_ERR = 1 /* Reserved Range for  Router-Port VLAN is exhausted. */
}tNpErrCodes;



extern UINT1               gau1NpPortBitMaskMap[];
#define NP_SET_MEMBER_PORT(au1PortArray, u2Port) \
{\
    UINT2 u2PortBytePos;\
        UINT2 u2PortBitPos;\
        u2PortBytePos = (UINT2)(u2Port / 8);\
        u2PortBitPos  = (UINT2)(u2Port % 8);\
        if (u2PortBitPos  == 0) {u2PortBytePos -= 1;} \
            \
                au1PortArray[u2PortBytePos] = (UINT1)(au1PortArray[u2PortBytePos] \
                                                      | gau1NpPortBitMaskMap[u2PortBitPos]);\
}
    /* Filter Actions */
#define FNP_DROP                    1
#define FNP_COPY_TO_CPU             3
#define FNP_SWITCH                  2
#define FNP_RESERVED                4
#define FNP_SWITCH_AND_COPY_TO_CPU  5
#endif /* _NPAPI_H */
