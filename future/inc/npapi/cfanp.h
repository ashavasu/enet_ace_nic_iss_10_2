/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfanp.h,v 1.67 2018/01/19 12:41:31 siva Exp $
 *
 * Description: Exported file for CFA NP-API.
 *
 *******************************************************************/
#ifndef __CFANP_H
#define __CFANP_H

/* tHwPortArray Definition is in fsvlan.h, but CFA needs it. */
#include "fsvlan.h"
#include "bridge.h"
#include "npapi.h"
#include "ip.h"
#include "isspinp.h"
#ifdef IP6_WANTED
#include "ipv6.h"
#endif

#define CFA_PACKET_QUEUE_BCM   "BCMQ"
#define CFA_NP_CRC_SIZE        4

#define CFA_MAX_NO_OF_PACKET_PER_READ  50
#define MAX_HW_INFO 3

#define CFA_DEF_OAMCCM_UNKNOWN_COMMIT_RATE (100) /* 100 kbps */

#define NP_HB_MSG_ETHERTYPE_OFFSET 16
#define NP_HB_MSG_ETHERTYPE_LEN 2
#define NP_HB_MSG_ETHERTYPE 0x88b6
#define NP_ICCH_MSG_ETHERTYPE 0x88b5


/* This structure is used to get the IfIndex to Unit-Port info from custnp.c 
*/
typedef struct npInterfaceInfo {
  INT4           i4SlotId;  /* Slot Identifier */
  UINT4          u4IfIndex; /* Interface Identifier - IfIndex */
  UINT4          u4SlotStartIfIndex; /* Starting IfIndex of the Slot. */
                                     /* Useful in multi-board support */
  UINT4          u4BoardType; /* Board Type / Identifier of the Target */
  UINT4          u4UnitId; /* Unit Identifier */
  UINT4          u4PortNum; /* Port Identifier */ 
} tNpInterfaceInfo;

/* Data Structures introduced for Handling Hw Notification at a common function
 * and used for Messaging in Dual Unit Stacking */



/* Structure that is used for  L3 VLAN Transmission */


typedef struct _HwL3TxMsgInfo
{
   tPortList TagPorts;   /* Tagged Port List for L3 VLAN Transmission */
   tPortList UnTagPorts; /*UnTagged Port List for L3 VLAN Transmission */
   UINT1    *pu1Data;    /* Message to be communicated */
   UINT2     u2VlanId;   /* VLAN ID */
   UINT2     u2InnerVlan;   /* Inner VLAN ID */
   UINT2     u2PktType;  /* Indicates whether Packet is a Unicast / Multicast
                            or Broadcast Packet */
   UINT2     u2EtherType;/* Ether-type present in the packet */
   UINT1     u1Tag;       /* Indicates whether packet is to be transmitted with VLAN
                           Tag or Not */
   UINT1     au1Pad[3];
}tHwL3TxMsgInfo;

/* Message Structure used to Handle DOS Attack Pkt,
 * Link Status Notification and Packet TX from CPU */

typedef struct _HwGenMsgInfo
{
   UINT1  *pu1Data;    /* Message to be communicated */
   
}tHwGenMsgInfo;

/* Message Structure used to handle MAC Learning Indication */

typedef struct _HwMacLearnMsgInfo
{
   UINT1 *pu1Data;      /* Message to be communicated */
   UINT4  u4InsertFlag; /* Flag to indicate whether corresponding 
                          Mac Entry Is to be Inserted or Removed */
}tHwMacLearnMsgInfo;

/* Message Structure used to handle Packet RX To CPU */

typedef struct _HwRxMsgInfo
{
   UINT1  *pu1Data;    /* Message to be communicated */
   UINT4  u4VlanId;    /* SP VlanID added by Hardware */

}tHwRxMsgInfo;

/* Structure to Handle Remote Client NP Hardware Programming */

typedef struct _HwRemClntMsgInfo
{
   UINT1  *pu1Data;    /* Message to be communicated */
   UINT4  u4Status;    /* Status of the Remote Client NP Programming */
   UINT4  u4SeqNum;     /* Status of the Remote Client NP Programming */

}tHwRemClntMsgInfo;

/* Structure to Handle DISS message communication between nodes */

typedef struct _HwDissMsgInfo
{
   UINT1  *pu1Data;    /* Message to be communicated */
   UINT4  u4AppId;     /* Application id of the module sending DISS message */
   INT4   i4SrcSlot;   /* Source Slot sending DISS message */
}tHwDissMsgInfo;


/* Structure to Handle all Hardware Notifications */

typedef struct _HwInfo
{

    UINT4 u4MessageType;   /* Notification Messsage Type  */   
    UINT4 u4DestSlotId;
    UINT4 u4IfIndex;       /* Interface If-Index */
    UINT4 u4PktSize;       /* Packet Size */
    UINT4 u4Cos;           /* Cos value to be used */
    UINT1 u1IsIPCPkt;
    UINT1 au1Pad[3];
    union 
        {
          /* Message Structure to be used if Message type is    
            MAC_LEARNING_INDICATION*/
            tHwMacLearnMsgInfo HwMacLearnMsgInfo;

          /* Message Structure to be used if Message type is
             PKT_RX_TO_CPU*/

            tHwRxMsgInfo       HwRxMsgInfo;                    

          /* Message Structure to be used if Message type is
             PKT_L3_TX_FROM_CPU*/

            tHwL3TxMsgInfo     HwL3TxMsgInfo;

         /* Message Structure to be used for Remote Clnt NP Programming */
            tHwRemClntMsgInfo  HwRemClntMsgInfo;

         /* Message Structure to be used for all other Message Types */
            tHwGenMsgInfo            HwGenMsgInfo;

         /* Message Structure to be used for all DISS Message Types */
            tHwDissMsgInfo            HwDissMsgInfo;
            
        }uHwMsgInfo;
}tHwInfo;

typedef enum {
    NP_CUST_IF_PRM_TYPE_TLV,
    NP_CUST_IF_PRM_TYPE_OPQ_ATTR
} tHwCustIfParamType;

typedef struct _HwCustIfTLV {
    UINT4 u4Type;
    UINT4 u4Length;
    UINT1 *pu1Value;
} tHwCustIfTLV;

typedef struct _HwCustIfOpqAttrs {
    UINT4 u4AttribID; /* Can be 1 to 4 */
    UINT4 u4Attribute;
} tHwCustIfOpqAttrs;

typedef union _HwCustIfParamVal {
    tHwCustIfTLV TLV;
    tHwCustIfOpqAttrs OpaqueAttrbs;
} tHwCustIfParamVal;

typedef struct _HwCfaFilterInfo
{
    UINT4 u4ContextId; /* Context where the filter needs to be installed.
                        * If u1UseContextId == OSIX_FALSE, then the filter
                        * should not be installed context spefic.
                        */
    UINT4 u4IfIndex;   /* Port on which the filter to be installed. Zero
                        * means the filter to be installed through out the
                        * Context.
                        */
    UINT1 au1Addr1[20];
    UINT1 au1Addr2[20];
    UINT2 u2Addr1Offset; /* Offset from where au1Addr1 starts in packet. */
    UINT2 u2Addr2Offset; /* Offset from where au1Addr2 starts in packet. */
    UINT2 u2Addr1Len;    /* Length of au1Addr1. If the length is zero then 
                            au1Addr1 has to be ignored. */
    UINT2 u2Addr2Len;    /* Length of au1Addr2. If the length is zero then 
                            au1Addr2 has to be ignored.*/
    UINT2 u2Addr1Type;   /* Address Type:CFA_NP_IPV4/CFA_NP_IPV6/CFA_NP_MAC */
    UINT2 u2Addr2Type;   /* Address Type:CFA_NP_IPV4/CFA_NP_IPV6/CFA_NP_MAC */
    UINT2 u2EtherType;
    UINT2 u2CustomDataOffset1;
    UINT2 u2CustomDataOffset2;
    UINT1 au1pad[1];
    UINT1 u1CustomData1[4];
    UINT1 u1CustomData2[4];
    UINT1 u1UseContextId; /* This can take OSIX_TRUE/OSIX_FALSE. If the filter
                           * needs to be installed on a particular context,
                           * then this value will be OSIX_TRUE and the context
                           * id to be taken from u4ContextId.
                           */
}tHwCfaFilterInfo;
typedef struct IfWanInfo
{
    UINT4    u4IfIndex; /* Interface Index of the port */
    tMacAddr MacAddr;   /* Mac Address of the given port*/
    UINT1    u1Opcode;  /* This can take  CFA_SET_TO_WAN/CFA_RESET_TO_LAN.
                           Based on this value the interface will be configured 
                           either as WAN or LAN  */
    UINT1    au1Pad[1]; /* Padding bytes */
}tIfWanInfo;

enum {
       CFA_SET_TO_WAN = 1,
       CFA_RESET_TO_LAN = 2
     };

typedef struct CfaWanInfo
{
    UINT4    u4IfIndex;        /* Interface Index of WAN port */
    INT4     i4EgressFpEntry;  /* Egress FP entry of WAN port */
    INT4     i4IngressFpEntry; /* Ingress FP entry of WAN port */
}tCfaWanInfo;


typedef struct _HwIdInfo
{
    UINT4 au4HwInfo[MAX_HW_INFO];
    UINT4 u4IfIndex;
    UINT1 u1Status;
    UINT1 au1Pad[3];
    
}tHwIdInfo;

#ifdef _CFANP_C_
tCfaWanInfo gaWanInfo[SYS_DEF_MAX_WAN_INTERFACES];
#else
extern tCfaWanInfo gaWanInfo[SYS_DEF_MAX_WAN_INTERFACES];
#endif
INT4 CfaNpSetStackingModel(UINT4 u4StackingModel);
INT4 FsCfaHwSetWanTye PROTO ((tIfWanInfo *pCfaWanInfo));
INT4  CfaNpInit PROTO ((VOID));
PUBLIC VOID  CfaNpUpdateSwitchMac PROTO ((tMacAddr));
INT4  CfaBcmxStatCustomSetInit PROTO ((VOID));
VOID  CfaNpGetHwAddr PROTO ((UINT2, tMacAddr));

INT4 CfaNpPortRead PROTO ((UINT1 *pu1Buf, UINT2 u2IfIndex, UINT4 *pu4PktSize));
INT4 CfaNpPortWrite PROTO ((UINT1 *pu1DataBuf, UINT2 u2IfIndex, UINT4 u4PktSize));
INT4 CfaNpPortOpen PROTO ((UINT2 u2IfIndex));
INT4 CfaNpPortClose PROTO ((UINT2 u2IfIndex));
PUBLIC INT4 FsNpCfaSetDlfStatus PROTO ((UINT1));
PUBLIC INT4 FsNpCfaVrfSetDlfStatus PROTO ((UINT4, UINT1));
INT4 NpResetACLHitCount (INT4 u4RuleNo, UINT1 u1Layer, UINT2 u2Port,
                       UINT1 u1Direction);
INT4 FsCfaHwClearStats PROTO ((UINT4 u4Port));
PUBLIC INT4 FsHwUpdateAdminStatusChange PROTO (( UINT4, UINT1 ));

INT4 FsCfaHwGetIfFlowControl PROTO ((UINT4 u4IfIndex, UINT4 *pu4PortFlowControl));
INT4 CfaHwL3VlanIntfWrite PROTO ((UINT1 *pu1DataBuf, UINT4 u4PktSize,
                                  tCfaVlanInfo VlanInfo));
INT4 CfaHwL3PwVlanIntfWrite PROTO (( UINT4 u4PktSize,
                                   INT2 i2Port));

INT4 FsCfaHwMacLookup PROTO ((UINT1 *pu1MacAddr, tVlanIfaceVlanId VlanId, UINT2 *pu2Port));

PUBLIC INT4 FsHwGetStat PROTO ((UINT4 , INT1, UINT4 *)); 
PUBLIC INT4 FsHwGetStat64 PROTO ((UINT4, INT1, tSNMP_COUNTER64_TYPE *));
PUBLIC INT4 FsHwGetVlanIntfStats PROTO ((UINT4 , INT1, UINT4 *)); 

PUBLIC UINT1 CfaNpGetLinkStatus PROTO ((UINT4 u4IfIndex));
PUBLIC UINT1 CfaNpGetPhyAndLinkStatus PROTO ((UINT2 u2IfIndex));

INT4 CfaNpIfCheckLocalMac PROTO ((UINT4 u4IfIndex, UINT1 *au1HwAddr));
INT4 FsHwNpInitDataBuffers PROTO((VOID));
INT4 CfaHwL2VlanIntfWrite (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 u4PktSize);
#ifdef DX285
INT4 CfaNpAddCPUMacEntry PROTO ((tVlanId VlanId));
INT4 CfaNpDeleteCPUMacEntry PROTO ((tVlanId VlanId));
#endif
INT4 CfaNpUpdatePortMacAddr PROTO ((VOID));
#ifdef DX285

#define CFA_NP_ADD_CPUMAC_ENTRY(VlanId)  CfaNpAddCPUMacEntry (VlanId)
#define CFA_NP_DEL_CPUMAC_ENTRY(VlanId)  CfaNpDeleteCPUMacEntry (VlanId)

#else

/* return success in case of other chipset */

#define CFA_NP_ADD_CPUMAC_ENTRY(VlanId)  (1)
#define CFA_NP_DEL_CPUMAC_ENTRY(VlanId)  (1)

#endif

INT4 FsHwSetCustIfParams PROTO ((UINT4 u4IfIndex, 
                                 tHwCustIfParamType u4HwCustParamType,
                                 tHwCustIfParamVal CustIfParam,
                                 tNpEntryAction EntryAction));

#ifdef MBSM_WANTED
PUBLIC INT4 CfaMbsmNpSlotInit PROTO ((tMbsmSlotInfo * pSlotInfo));
PUBLIC INT4 CfaMbsmNpSlotDeInit PROTO ((tMbsmSlotInfo * pSlotInfo));
PUBLIC INT4 CfaMbsmRegisterWithNpDrv PROTO ((tMbsmSlotInfo *pSlotInfo));
PUBLIC INT4 FsHwMbsmSetCustIfParams PROTO ((UINT4 u4IfIndex, 
                                            tHwCustIfParamType HwCustParamType,
                                            tHwCustIfParamVal CustIfParamVal,
                                            tNpEntryAction EntryAction, 
                                            tMbsmSlotInfo *pSlotInfo));

INT4 FsCfaMbsmSetMacAddr PROTO ((UINT4 u4IfIndex, tMacAddr PortMac,
                                 tMbsmSlotInfo *pSlotInfo));
#endif

INT4 FsCfaHwSetMtu PROTO ((UINT4 u4IfIndex, UINT4 u4MtuSize));
INT4 FsCfaHwL3SetMtu PROTO ((tL3MtuInfo  L3MtuInfo));
INT4 FsCfaHwSetMacAddr PROTO ((UINT4 u4IfIndex, tMacAddr PortMac));
INT4 FsHwGetEthernetType PROTO ((UINT4 u4IfIndex, UINT1 * pu1EtherType));

INT4 FsCfaHwCreateILan (UINT4 u4ILanIndex, tHwPortArray ILanPortArray);
INT4 FsCfaHwDeleteILan (UINT4 u4ILanIndex);
INT4 FsCfaHwAddPortToILan (UINT4 u4ILanIndex, UINT4 u4PortIndex);
INT4 FsCfaHwRemovePortFromILan (UINT4 u4ILanIndex, UINT4 u4PortIndex); 
INT4 FsCfaHwCreateInternalPort (UINT4 u4IfIndex);
INT4 FsCfaHwDeleteInternalPort (UINT4 u4IfIndex);
INT4 FsCfaHwGetMtu PROTO ((UINT2 u2IfIndex, UINT4 *pu4MtuSize));
INT4 FsCfaHwCreatePktFilter (tHwCfaFilterInfo *pFilterInfo, UINT4 *pu4FilterId);
INT4 FsCfaHwDeletePktFilter (UINT4 u4FilterId);
VOID CfaMbsmUpdatePortMacAddress (UINT1 u1SlotId, UINT4 u4PortIndex, 
                                    UINT4 u4IfIndex, UINT1 *pu1SwitchMac);
INT4 NpCfaFrontPanelPorts (UINT4 u4MaxFrontPanelPorts);
INT4 FsCfaHwSendIPCMsg (tHwInfo * pHwInfo);
VOID CfaNpSetHwPortInfo (tHwPortInfo  HwPortInfo);
VOID CfaNpGetHwPortInfo (tHwPortInfo  * pHwPortInfo);
INT4 CfaNpGetHwInfo (tHwIdInfo  * pHwIdInfo);
INT4 CfaNpRemoteSetHwInfo (tHwIdInfo * pHwIdInfo);
INT4  FsCfaHwRemoveIpNetRcvdDlfInHash (UINT4 u4ContextId, UINT4 u4IpNet, UINT4 u4IpMask);
#ifdef IP6_WANTED
INT4  FsCfaHwRemoveIp6NetRcvdDlfInHash (UINT4 u4ContextId,tIp6Addr Ip6Addr, UINT4 u4Prefix);
#endif
#define FS_UINT8_SET_COUNTER(pPortCounters, s, u8Value)       \
{                                                             \
    pPortCounters->s.u4Hi  = u64_H(u8Value);                  \
    pPortCounters->s.u4Lo  = u64_L(u8Value);                  \
}
typedef struct _IssPortCounters
{
    /* Receive Counters */
    /* Number of received packets with CRC error, but proper size. */
    FS_UINT8 cntRxFCSErrors;

    /* Number of received packets with symbol error, but proper size. */
    FS_UINT8 cntRxSymbolErrors;

    /* Number of received packets that are undersized or oversized. */
    FS_UINT8 cntRxFrameSizeErrors;

    /*
     * Number of received packets larger than the configured maximum size
     * with either a CRC or alignment error (16 bits).
     */
    FS_UINT8 cntRxJabberPkts;

    /* Transmit Counters */
    /* Number of transmitted packets with CRC error, but proper size. */
    FS_UINT8 cntTxFCSErroredPkts;

    /*
     * Number of transmitted packets that were marked on ingress as
     * erroneous (either due to an CRC or symbol error, or due to under/over
     * size problems).
     */
    FS_UINT8 cntTxErrorDropPkts;

    /** Number of frames dropped due to the frame timeout mechanism */
    FS_UINT8 cntTxTimeOutPkts;
} tIssPortCounters;
INT4 IssHwGetStat64 (UINT4 u4IfIndex, tIssPortCounters *tPortCounters);
#endif /* __CFANP_H */
