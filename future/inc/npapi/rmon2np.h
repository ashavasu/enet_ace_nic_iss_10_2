/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rmon2np.h,v 1.5 2015/09/13 10:36:22 siva Exp $
 *
 * Description: This file contains the function implementations  of 
 *              RMON NP-API.
 ****************************************************************************/

#ifndef _RMON2NP_H
#define _RMON2NP_H

#define MAX_NP_RMON2_DSMON_CTRL_IF_NODE    50

/* Macro used for RBTree Comparisions */
#define NP_RMON2_DSMON_RB_EQUAL             0
#define NP_RMON2_DSMON_RB_GREATER           1
#define NP_RMON2_DSMON_RB_LESS             -1
#define NP_RMON2_PROT_IDFR_LEN              2
#define NP_RMON2_PROT_TYPE_LEN              1
#define NP_RMON2_IF_IDX_LEN                 4


#define NP_RMON2_ALL_INTFCS_TYPE            1
#define NP_RMON2_ALL_PROTOCOLS_TYPE         2

#define NP_RMON_DSMON_ALL_DATA_COLLECT      1

#define RMON2_MAX_DATA_SOURCE            24
#define RMONV2_MAX_VLAN_NODES            RMON2_MAX_DATA_SOURCE

/* Macro for RMON Feature */
#define RMON2_ENABLE                        1
#define RMON2_DISABLE                       2

/* Macro for DSMON Feature */
#define DSMON_ENABLE                        1
#define DSMON_DISABLE                       2

typedef struct RMONv2ProtocolIdfr {
    UINT4 u4ProtoDirLclIdx;                  /* RMONv2 Protocol Directory Local Index */
    UINT2 u2L3ProtocolId;
    UINT2 u2L4ProtocolId;
    UINT2 u2L5ProtocolId;
    UINT1 au1Reserved[2];
} tRmon2ProtocolIdfr;

INT4 FsRMONv2NPInit PROTO ((VOID));
VOID FsRMONv2NPDeInit PROTO ((VOID));
INT4 FsRMONv2HwSetStatus PROTO ((UINT4));
UINT4 FsRMONv2HwGetStatus PROTO ((VOID));
INT4 FsRMONv2EnableProtocol PROTO ((tRmon2ProtocolIdfr));
INT4 FsRMONv2DisableProtocol PROTO ((UINT4));
INT4 FsRMONv2EnableProbe PROTO ((UINT4, UINT2, tPortList));
INT4 FsRMONv2DisableProbe PROTO ((UINT4, UINT2, tPortList));
INT4 FsRMONv2AddFlowStats PROTO ((tPktHeader));
INT4 FsRMONv2RemoveFlowStats PROTO ((tPktHeader));
INT4 FsRMONv2CollectStats (tPktHeader, tRmon2Stats*);
UINT1 FsRMONv2GetPortID PROTO ((UINT1 *au1Mac,UINT4 u4VlanIndex,INT4 *i4PktPortNum));



#endif /* _RMON2NP_H */
