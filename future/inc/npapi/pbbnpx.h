/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id
 *
 * Description: This file contains the function prototypes of PBB MBSMNPAPI
 ****************************************************************************/
#ifndef _PBBNPX_H
#define _PBBNPX_H
#ifdef MBSM_WANTED 
/* Hardware API function prototypes */
INT4
FsMiPbbMbsmHwNpInit PROTO((tMbsmSlotInfo * pSlotInfo));

INT4
FsMiPbbMbsmHwSetVipPipMap PROTO((UINT4 u4ContextId, UINT4 u4VipIfIndex,
                       tVipPipMap VipPipMap,
                       tMbsmSlotInfo * pSlotInfo));

INT4
FsMiPbbMbsmHwSetVipAttributes PROTO((UINT4 u4ContextId, UINT4 u4VipIndex,
                           tVipAttribute VipAttribute,
                           tMbsmSlotInfo * pSlotInfo));
INT4
FsMiPbbMbsmHwDelVipPipMap PROTO((UINT4 u4ContextId, UINT4 u4VipIfIndex,
        tMbsmSlotInfo * pSlotInfo));
INT4
FsMiPbbMbsmHwDelVipAttributes PROTO((UINT4 u4ContextId, 
                           UINT4 u4VipIfIndex,
                           tMbsmSlotInfo * pSlotInfo));

INT4
FsMiPbbMbsmHwSetBackboneServiceInstEntry PROTO((
            UINT4 u4ContextId, 
            UINT4 u4CbpIfIndex,
            UINT4 u4BSid,
            tBackboneServiceInstEntry BackboneServiceInstEntry,
            tMbsmSlotInfo * pSlotInfo));
INT4
FsMiPbbMbsmHwDelBackBoneServiceInstEntry PROTO((UINT4 u4ContextId, 
        UINT4 u4CbpIfIndex,
        UINT4 u4BSid,
        tMbsmSlotInfo * pSlotInfo));

INT4
FsMiPbbMbsmHwSetAllToOneBundlingService PROTO((
    UINT4 u4ContextId, 
    UINT4 u4IfIndex,
    UINT4 u4Isid,
    tMbsmSlotInfo * pSlotInfo));

INT4
FsMiPbbMbsmHwDelAllToOneBundlingService PROTO((
        UINT4 u4ContextId, 
        UINT4 u4IfIndex,
        UINT4 u4Isid,
        tMbsmSlotInfo * pSlotInfo));
INT4 
FsMiBrgMbsmHwCreateControlPktFilter PROTO((
                      UINT4 u4ContextId,
                      tPbbCtrlPktFilterEntry *pEcfmFilterEntry,
                      tMbsmSlotInfo *pSlotInfo));
    

#endif
#endif
