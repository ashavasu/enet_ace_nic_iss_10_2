/* $Id: qosxnp.h,v 1.34 2018/02/08 10:07:28 siva Exp $*/
/****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                     */
/*                                                                          */
/*  FILE NAME             : qosxnp.h                                        */
/*  PRINCIPAL AUTHOR      : Aricent                                         */
/*  SUBSYSTEM NAME        : QoS                                             */
/*  MODULE NAME           : QoS-NPAPI                                       */
/*  LANGUAGE              : C                                               */
/*  TARGET ENVIRONMENT    : Linux                                           */
/*  DATE OF FIRST RELEASE :                                                 */
/*  AUTHOR                : Aricent                                         */
/*  FUNCTIONS DEFINED(Applicable for Source files) :                        */
/*  DESCRIPTION           : This file contains the exported NPAPI functions */
/*                          and macros of Aricent QoS Module.               */
/****************************************************************************/
/*                                                                          */
/*  Change History                                                          */
/*  Version               :                                                 */
/*  Date(DD/MM/YYYY)      :                                                 */
/*  Modified by           :                                                 */
/*  Description of change :                                                 */
/****************************************************************************/

#ifndef __QOSX_NP_H__
#define __QOSX_NP_H__

#include "lr.h"
#include "iss.h"
#include "npapi.h"
#include "qosxtd.h"


#define QOS_CNC_BLOCK1 0
#define QOS_CNC_BLOCK2 1

/* NPAPI Structers */
/* Priority Map Table Entry */
typedef struct 
{
   tRBNodeEmbd         QosProfileRbNode; /* Link to Traverse the Table*/
   INT4                i4DropPrecedence; /*Drop precedence value*/
   INT4                i4Dscp;           /*Ipdscp value*/
   INT4                i4MplsExp;        /*Mpls Exp value to be remarked*/
   INT4                i4TrafficClass;   /*Queue id */
   INT4                i4VlanPriority;   /*UP(user priority) to be updated */
   INT4                i4ProfileIndex;   /*Profile Index*/
   INT4                i4RefCount;       /*Reference count*/

}tQoSProfileRbEntry;
typedef struct QoSProfileClass
{
    tRBNodeEmbd         QosProfileClassRbNode;/* Link to Traverse the Table*/
    INT4  i4ClassMapIndex;                    /*CLASS index*/
    INT4  i4ProfileIndexGreen;                /*Conform action profile Index*/
    INT4  i4ProfileIndexYellow;               /*Exceed action profile Index*/
    INT4  i4ProfileIndexRed;                  /*Violate action profile Index*/
}tQoSProfileClassRbEntry;


typedef struct QoSPriorityMapEntry
{
    UINT4 u4IfIndex;                  /* Ingress interface         */
    UINT4 u4QoSPriorityMapId;         /* Priority map table id     */
    UINT4 u4HwFilterId;                  /* Hw Index of filter crated for mpls exp*/       
    UINT2 u2VlanId;                   /*  VLAN id                  */
    UINT1 u1InPriority;               /* Incoming priority         */
    UINT1 u1InPriType;                /* Incoming Pri Type         */
    UINT1 u1RegenPri;                 /* Regenerated priority      */
    UINT1 u1RegenInnerPri;            /* Regenerated Inner-VLAN priority */
    UINT1 u1PriorityMapStatus;        /* priority-map entry status       */
    INT1  i1InDEI;                    /* Incoming DEI value       */
    UINT1 u1Color;                    /* Incoming Color value       */
    UINT1 au1Pack[3];                 /* packing                         */

}tQoSPriorityMapEntry;

/* Incoming Vlan map Table */
typedef struct _tQoSInVlanMapEntry
{
    tRBNodeEmbd RbNode;                  /* Link to Traverse the Table       */
    UINT4 u4Id;                          /* Index of the Table               */
    UINT4 u4IfIndex;                     /* Incoming Interface (port)        */
    UINT4 u4VlanMapHwId;                 /* HW ID for scheduler              */
    UINT2 u2VlanId;                      /* Incoming VLAN ID                 */
    UINT1 u1Status;                      /* Status of this Entry             */
    UINT1 au1Pack[1];                    /* packing                          */

} tQoSInVlanMapEntry;



/* Class Map Table Entry */
typedef struct QoSMultiFieldClassMapEntry
{
    tIssL2FilterEntry      *pL2FilterPtr;
    tIssL3FilterEntry      *pL3FilterPtr;
    tQoSPriorityMapEntry   *pPriorityMapPtr;/* Ptr to priority-map entry  */
    tQoSInVlanMapEntry     *pInVlanMapPtr;  /* Ptr to vlan-map entry      */
    UINT4                  u4QoSMFClass;    /* CLASS for the class-map    */
    UINT4                  u4TrafficClass;  /* Traffic class              */
    UINT1                  u1QoSMFCStatus;  /* class-map entry status     */
    UINT1                  u1PreColor;      /* PreColor                   */
    UINT2                  u2PPPSessId;     /* PPP Session identifier     */

}  tQoSClassMapEntry;

/* Policy Map Table Entry */
typedef struct QoSPolicyMapEntry
{
    UINT4   u4QoSClass;       /* CLASS for the class-map */
    INT4    i4IfIndex;        /* ingress port for which policy is applicable */
    INT4    i4HwExpMapId;     /* Hardware MPLS-EXP profiling Id*/
    INT4    i4QoSMeterId;     /* Meter id */
    INT4    i4QoSPolicyMapId; /* Policy-map entry id */
    INT4    i4OutDEI; /* Policy-map entry id */
    INT4    i4Color;
    UINT1   u1PHBType;        /* PHB Type */
    UINT1   u1DefaultPHB;     /* Default PHB  */  
    UINT1   u1PolicyMapStatus; /* entry status */
    UINT1   au1Pack[1];         /* packing                    */

}  tQoSPolicyMapEntry;

/* In Profile Table Entry */
typedef struct QoSInProfileActionEntry
{
    UINT4  u4QoSInProfConfActFlag;           /* Conform action flag        */
    UINT4  u4QoSInProfConfNewClass;          /* Conform action new Class   */
    UINT4  u4QoSInProfConfTrafficClass;      /* Conform action traffic Class   */
    UINT4  u4QoSInProfExcActFlag;            /* Exceed action flag        */
    UINT4  u4QoSInProfExcNewClass;           /* Exceed action new Class   */
    UINT4  u4QoSInProfExcTrafficClass;       /* Exceed action traffic Class   */

    UINT4  u4QoSInProfileActionVlanPrio;    /* Conform action -vlan prio  */
    UINT4  u4QoSInProfileActionVlanDE;      /* Conform action -vlan DE    */
    UINT4  u4QoSInProfileActionInnerVlanPrio;  /* Conform action-Inner  
                                                  vlan prio               */
    UINT4  u4QoSInProfileActionInnerVlanDE;  /* Conform action-Inner Vlan DE */ 
    UINT4  u4QoSInProfileActionIpTOS;       /* Conform action -ip TOS     */
    UINT4  u4QoSInProfileActionDscp;        /* Conform action -dscp       */
    UINT4  u4QoSInProfileActionMplsExp;     /* Conform action -mpls exp   */
    UINT4  u4QoSInProfileExceedActionVlanPrio;  /* Exceed action-vlan prio*/
    UINT4  u4QoSInProfileExceedActionVlanDE;    /* Exceed action -vlan DE */
    UINT4  u4QoSInProfileExceedActionInnerVlanPrio; /* Exceed action .  
                                                       Inner vlan prio      */
    UINT4  u4QoSInProfileExceedActionInnerVlanDE; /* Exceed action inner vlan DE */
    UINT4  u4QoSInProfileExceedActionIpTOS;     /* Exceed action- ip TOS    */
    UINT4  u4QoSInProfileExceedActionDscp;      /* Exceed action - dscp     */
    UINT4  u4QoSInProfileExceedActionMplsExp;    /* Exceed action - mpls exp */
    UINT4  u4QoSInProfileActionPort;            /* InProfile action - Port  */

} tQoSInProfileActionEntry;
 
/* Out Profile Table Entry */
typedef struct QoSOutProfileActionEntry
{   
    /* Violate Action Flag and Values */ 
    UINT4   u4QoSOutProfileActionFlag;
    UINT4   u4QoSOutProfileNewClass;
    UINT4   u4QoSOutProfileTrafficClass;
    UINT4   u4QoSOutProfileActionVlanPrio;
    UINT4   u4QoSOutProfileActionVlanDE;
    UINT4   u4QoSOutProfileActionInnerVlanPrio;
    UINT4   u4QoSOutProfileActionInnerVlanDE;
    UINT4   u4QoSOutProfileActionIpTOS;
    UINT4   u4QoSOutProfileActionDscp;
    UINT4   u4QoSOutProfileActionMplsExp;

}tQoSOutProfileActionEntry;

/* Meter Table Entry */
typedef struct QoSMeterEntry
{
    UINT4  u4QoSMeterFlag;
    INT4   i4QoSMeterId;
    UINT4  u4QoSCIR;
    UINT4  u4QoSCBS;
    UINT4  u4QoSEIR;
    UINT4  u4QoSEBS;
    UINT4  u4NextMeter;
    UINT1  u1QoSMeterStatus;
    UINT1  u1QoSMeterType;
    UINT1  u1QoSInterval;
    UINT1  u1QoSMeterColorMode;
    UINT1  u1QoSMeterPacketBased;
    UINT1  u1MeterStatStatus;
    UINT1  u1Flag;
    UINT1  u1DefCoPPFlag;
    UINT1  au1Pad[4];

}tQoSMeterEntry;


/* RD Table Entry */
typedef struct QoSREDCfgEntry
{
    UINT4           u4MinAvgThresh;
    UINT4           u4MaxAvgThresh;
    UINT4           u4MaxPktSize;
    UINT4           u4Gain;
    UINT4           u4ECNThresh; 
    UINT1           u1MaxProbability;
    UINT1           u1ExpWeight;
    UINT1           u1DropPrecedence;
    UINT1           u1DropThreshType;
    UINT1           u1RDActionFlag;
    UINT1           au1Pack[3];

} tQoSREDCfgEntry;

/* Q Template Table Entry */
typedef struct QoSQtypeEntry
{
    UINT4           u4QueueTypeId;
    UINT4           u4QueueSize;
    UINT1           u1DropAlgo;
    UINT1           u1DropAlgoEnableFlag;
    UINT1           au1Pack[2];

} tQoSQtypeEntry;

/* Shape Table Entry */
typedef struct QoSShapeCfgEntry
{
    UINT4 u4QosCIR;
    UINT4 u4QosCBS;
    UINT4 u4QosEIR;
    UINT4 u4QosEBS;
    UINT2 u2ShaperId;
    UINT1 au1Pack[2];

} tQoSShapeCfgEntry;

/* Scheduler Entry */
typedef struct QoSSchedulerEntry
{
    tQoSShapeCfgEntry  *pShapePtr;
    INT4               i4QosIfIndex;
    INT4               i4QosSchedulerId;
    UINT4              u4QosSchedHwId; /* Hardware Id created in the hardware for 
     * schedulers when  BCM56840 devices are used */
    UINT4              u4QosSchedChildren;
    UINT1              u1QosSchedAlgo;
    UINT1              u1QosSchedulerStatus;
    UINT1              u1HL;
    UINT1              u1Flag;
} tQoSSchedulerEntry;

/* Q Table Entry */
typedef struct QoSQEntry
{
    tQoSShapeCfgEntry   *pShapePtr;
    tQoSSchedulerEntry  *pSchedPtr;
    INT4                i4QosQId;
    INT4                i4QosSchedulerId;
    INT4                i4QosQueueHwId;
    UINT4               u4QueueType;  /* Type of Queue Unicast Queue or Multicast Queue */
    UINT2               u2QosQWeight;
    UINT1               u1QosQPriority;
    UINT1               u1QosQStatus;
} tQoSQEntry;

typedef struct QosClassToPriMapEntry
{
    INT4  i4IfIndex;    /* Interface Index */
    INT4  i4HwExpMapId;    /* Hardware MplsEXP profiling Id  */
    UINT4 u4HwFilterId; /*  Hw Filter Id */
    UINT2 u2VlanId;
    UINT1 u1PriType;    /* Priority type */
    UINT1 u1Priority;   
    UINT1 u1RegenPriority;  /*Regeneration priority */
    UINT1 u1Flag;   /* QOS_NP_ADD/QOS_NP_DEL */
    INT1  i1DEI;        /*Drop Eligible */
    INT1  i1Color;     /*Regenerator Color */
} tQosClassToPriMapEntry;

typedef struct QosClassToIntPriEntry
{
    tIssL2FilterEntry      *pL2FilterPtr;
    tIssL3FilterEntry      *pL3FilterPtr;
    UINT1 u1IntPriority; /* Internal priority for traffic class*/
    UINT1 u1Flag; /* QOS_NP_ADD/QOS_NP_DEL */
    UINT1 u1Pad[2];
} tQosClassToIntPriEntry;

typedef struct QosPfcHwStats
{
    tSNMP_COUNTER64_TYPE *pu8StatsCount; /* 64- bit counter value*/
    UINT4 u4IfIndex;       /* Interface Index */
    UINT4 u4PfcPauseCount; /* PFC Pause Count */
    UINT1 u1PfcStatsType;  /* PFC Statistics Type */
    UINT1 u1PfcStats;  /* PFC Statistics Type */
    UINT1 u1Priority;
    UINT1 u1pad;
}tQosPfcHwStats;

/* NPAPI Functions Prototype */
INT4
QoSHwMapClasstoPriMap PROTO ((tQosClassToPriMapEntry  *pQosClassToPriMapEntry));

INT4
QosHwMapClassToIntPriority PROTO ((tQosClassToIntPriEntry *pQosClassToIntPriEntry));

INT1 QosHwGetDscpQueueMap PROTO (( INT4  i4QosDscpVal, INT4 * i4QosQueID ));
INT1 QoSHwSetTrafficClassToPcp PROTO ((INT4 i4IfIndex, INT4 i4Priority, INT4 i4TrafficClass));

UINT1 FsQosHwGetPfcStats PROTO ((tQosPfcHwStats *pPfcHwStats));

INT4
QoSHwInit PROTO ((VOID));

INT4
QoSHwMapClassToPolicy  PROTO ((tQoSClassMapEntry *pClsMapEntry,
                               tQoSPolicyMapEntry *pPlyMapEntry,
                               tQoSInProfileActionEntry *pInProActEntry,
                               tQoSOutProfileActionEntry *pOutProActEntry,
                               tQoSMeterEntry *pMeterEntry, UINT1 u1Flag));

INT4
QoSHwUpdatePolicyMapForClass PROTO ((tQoSClassMapEntry *pClsMapEntry,
                                     tQoSPolicyMapEntry *pPlyMapEntry,
                                     tQoSInProfileActionEntry *pInProActEntry,
                                     tQoSOutProfileActionEntry *pOutProActEntry,
                                     tQoSMeterEntry *pMeterEntry,
                                     UINT1 u1Flag));

INT4
QoSHwUnmapClassFromPolicy PROTO ((tQoSClassMapEntry *pClsMapEntry,
                                  tQoSPolicyMapEntry *pPlyMapEntry,
                                  tQoSMeterEntry *pMeterEntry,
                                  UINT1 u1Flag));
INT4
QoSHwDeleteClassMapEntry PROTO ((tQoSClassMapEntry *pQoSClassMapEntry));
INT4
QoSHwMeterCreate PROTO ((tQoSMeterEntry *pQoSMeterEntry));
INT4
QoSHwMeterDelete PROTO ((INT4 i4MeterId));

INT4
QoSHwMeterStatsUpdate PROTO ((UINT4 u4HwFilterId,tQoSMeterEntry * pMeterEntry));

INT4
QoSHwMeterStatsClear PROTO((UINT4 u4MeterId));

INT4
QoSHwSchedulerAdd PROTO ((tQoSSchedulerEntry *pSchedEntry));
INT4
QoSHwSchedulerUpdateParams PROTO ((tQoSSchedulerEntry *pSchedEntry));
INT4
QoSHwSchedulerDelete PROTO ((INT4 i4IfIndex, UINT4 u4SchedId));
INT4
QoSHwMapClassToQueue PROTO ((INT4 i4IfIndex, INT4 i4RegenPriType,
                             UINT4 u4ClsOrPri, UINT4 u4QId, UINT1 u1Flag));
INT4
QoSHwMapClassToQueueId PROTO ((tQoSClassMapEntry *pClsMapEntry,
                             INT4 i4IfIndex, INT4 i4RegenPriType,
                             UINT4 u4ClsOrPri, UINT4 u4QId, UINT1 u1Flag));


INT4
QoSHwSchedulerHierarchyMap PROTO ((INT4 i4IfIndex, UINT4 u4SchedulerId,
                                   UINT2 u2Sweight, UINT1 u1Spriority, 
                                   UINT4 u4NextSchedId, UINT4 u4NextQId,
                                   INT2 i2HL, UINT1 u1Flag));

INT4
QoSHwQueueCreate PROTO ((INT4 i4IfIndex, UINT4 u4QId, tQoSQEntry *pQEntry,
                         tQoSQtypeEntry *pQTypeEntry,
                         tQoSREDCfgEntry *papRDCfgEntry[], INT2 i2HL));

INT4
QoSHwQueueDelete PROTO ((INT4 i4IfIndex, UINT4 u4Id));

INT4
QosAddL2L3FilterActionAndInstall PROTO ((INT4 FPHandle,INT4 i4FilterAction));

INT4
QoSHwSetDefUserPriority PROTO ((INT4 i4Port, INT4 i4DefPriority));

INT4
QoSHwSetPbitPreferenceOverDscp PROTO ((INT4 i4Port, INT4 i4PbitPref));

INT4
QoSHwGetMeterStats PROTO ((UINT4 u4FsQoSMeterId, UINT4 u4StatsType,
                           tSNMP_COUNTER64_TYPE *pu8MeterStatsCounter));
INT4 
QoSHwGetCoSQStats PROTO ((INT4 i4IfIndex, UINT4 u4FsQoSCoSQId, 
                          UINT4 u4StatsType, 
                          tSNMP_COUNTER64_TYPE *pu8CoSQStatsCounter));
INT4
QoSHwGetRandomDropStats PROTO ((UINT4 u4DiffServId,
                         UINT4 u4StatsType,
                         tSNMP_COUNTER64_TYPE * pu8RetValDiffServCounter));
INT4
QoSHwGetAlgDropStats PROTO ((UINT4 u4DiffServId,
                      UINT4 u4StatsType,
                      tSNMP_COUNTER64_TYPE * pu8RetValDiffServCounter));
INT4 QoSHwGetCountActStats PROTO ((UINT4 u4DiffServId, UINT4 u4StatsType,
                                   tSNMP_COUNTER64_TYPE * pu8RetValDiffServCounter));

/* DCB PFC NPAPI */
INT4 FsQosHwConfigPfc PROTO ((tQosPfcHwEntry *pQosPfcHwEntry));

INT4
QoSHwSetCpuRateLimit PROTO ((INT4 i4CpuQueueId, UINT4 u4MinRate, UINT4 u4MaxRate));

INT1
QoSHwSetVlanQueuingStatus PROTO ((INT4 i4IfIndex, tQoSQEntry  *pQEntrySubscriber));

INT4
FsDxQosReleaseProfileIndex (INT4 i4ClassMapIndex,INT4 i4Action);
INT4
FsDxQosAllocateProfileIndex(INT4 i4ClassMapIndex,INT4 *i4QosProfileIndex,INT4 i4Action);
INT4
FsDxQosUpdateProfileIndex (INT4 i4ClassMapIndex,INT4 i4Action,INT4 i4TempProfileIndex);
INT4
FsDxQosGetProfileIndex (INT4 i4ClassMapIndex,INT4 i4Action);
PUBLIC INT4
QosProfileRbEntryCmp (tRBElem * prbElem1, tRBElem * prbElem2);
INT4
FsDxQosAddProfileEntry (tQoSProfileRbEntry * pProfileEntry);
tQoSProfileRbEntry *
FsDxQosGetProfileEntry (tQoSProfileRbEntry * pProfileEntry);
tQoSProfileRbEntry *
FsDxQosGetProfileEntryFromProfileIndex(INT4 i4ProfileIndex);
INT4
FsDxQosDeleteProfileEntry (tQoSProfileRbEntry * pProfileEntry);
INT4
FsDxQosFillPriorityMapToProfileEntry(tQoSPriorityMapEntry *pPriMap,
                                                tQoSProfileRbEntry *pQoSProfileRbEntry);
INT4
FsDxQosMapClassToProfileIndex(INT4 i4Class,INT4 i4Action,INT4 *i4ProfileIndex,
                                            tQoSProfileRbEntry *pTempQoSProfileRbEntry);

PUBLIC INT4
QosProfileClassRbEntryCmp (tRBElem * prbElem1, tRBElem * prbElem2);
INT4
FsDxQosAddProfileClassEntry (tQoSProfileClassRbEntry * pProfileEntry);
tQoSProfileClassRbEntry *
FsDxQosGetProfileClassEntry (tQoSProfileClassRbEntry * pProfileEntry);
INT4
FsDxQosDeleteProfileClassEntry (tQoSProfileClassRbEntry * pProfileEntry);
INT4 QoSAllocateFreeProfileIndex(INT4 *i4QosProfileIndex);
INT4 QoSReleaseProfileIndex(INT4 i4QosProfileIndex);
INT4 FsDxQosReleaseProfileClassEntry(INT4 i4ClassMapIndex);

#ifdef MBSM_WANTED
INT4
QoSMbsmHwUpdatePolicyMapForClass PROTO ((tQoSClassMapEntry * pClsMapEntry,
                       tQoSPolicyMapEntry * pPlyMapEntry,
                       tQoSInProfileActionEntry * pInProActEntry,
                       tQoSOutProfileActionEntry * pOutProActEntry,
                       tQoSMeterEntry * pMeterEntry, 
                       tMbsmSlotInfo * pSlotInfo));
INT4 QoSMbsmHwQueueCreate PROTO ((INT4 i4IfIndex, UINT4 u4QId, tQoSQEntry * pQEntry, tQoSQtypeEntry * pQTypeEntry, tQoSREDCfgEntry * papRDCfgEntry[], INT2 i2HL, tMbsmSlotInfo * pSlotInfo));

INT4
QoSMbsmHwMapClassToQueue PROTO ((INT4 i4IfIndex, INT4 i4ClsOrPriType, UINT4 u4ClsOrPri,
                                 UINT4 u4QId, UINT1 u1Flag, tMbsmSlotInfo * pSlotInfo));
INT4
QoSMbsmHwMapClassToQueueId PROTO ((tQoSClassMapEntry * pClsMapEntry,
                                 INT4 i4IfIndex, INT4 i4RegenPriType, UINT4 u4ClsOrPri,
                                 UINT4 u4QId, UINT1 u1Flag, tMbsmSlotInfo * pSlotInfo));

/* DCB PFC MBSM NPAPI */
INT4 FsQosMbsmHwConfigPfc PROTO ((tQosPfcHwEntry *pQosPfcHwEntry, 
                                  tMbsmSlotInfo * pSlotInfo));
INT4 QoSMbsmHwInit PROTO ((tMbsmSlotInfo * pSlotInfo));
INT4 QoSMbsmHwSetCpuRateLimit PROTO ((INT4 i4CpuQueueId, UINT4 u4MinRate, UINT4 u4MaxRate, tMbsmSlotInfo * pSlotInfo));
INT4 QoSMbsmHwMapClassToPolicy PROTO ((tQoSClassMapEntry * pClsMapEntry, tQoSPolicyMapEntry * pPlyMapEntry, tQoSInProfileActionEntry * pInProActEntry, tQoSOutProfileActionEntry * pOutProActEntry, tQoSMeterEntry * pMeterEntry, UINT1 u1Flag, tMbsmSlotInfo * pSlotInfo));
INT4 QoSMbsmHwSchedulerHierarchyMap PROTO ((INT4 i4IfIndex, UINT4 u4SchedId, UINT2 u2Sweight, UINT1 u1Spriority, UINT4 u4NextSchedId, UINT4 u4NextQId, INT2 i2HL, UINT1 u1Flag, tMbsmSlotInfo * pSlotInfo));
INT4 QoSMbsmHwSchedulerUpdateParams PROTO ((tQoSSchedulerEntry * pSchedEntry, tMbsmSlotInfo * pSlotInfo));
INT4 QoSMbsmHwSetDefUserPriority PROTO ((INT4 i4Port, INT4 i4DefPriority, tMbsmSlotInfo * pSlotInfo));
INT4 QoSMbsmHwSetPbitPreferenceOverDscp PROTO ((INT4 i4Port, INT4 i4PbitPref, tMbsmSlotInfo * pSlotInfo));
INT4 QoSMbsmHwMapClasstoPriMap PROTO ((tQosClassToPriMapEntry * pQosClassToPriMapEntry, tMbsmSlotInfo * pSlotInfo));
INT4 QosMbsmHwMapClassToIntPriority PROTO ((tQosClassToIntPriEntry * pQosClassToIntPriEntry, tMbsmSlotInfo * pSlotInfo));
INT4 QoSMbsmHwSchedulerUpdateParams PROTO ((tQoSSchedulerEntry * pSchedEntry, tMbsmSlotInfo * pSlotInfo));
INT4                                                                                       QoSMbsmHwSchedulerAdd (tQoSSchedulerEntry * pSchedEntry,                                           tMbsmSlotInfo * pSlotInfo);                                                       
INT4
QoSMbsmHwMeterCreate (tQoSMeterEntry * pMeterEntry, tMbsmSlotInfo *pSlotInfo);


#endif
#ifdef NPAPI_WANTED
#ifdef QOSX_WANTED
#ifdef __QOS_SYS_C__
UINT4 gu4QueueHwId[SYS_DEF_MAX_PHYSICAL_INTERFACES][VLAN_DEV_MAX_NUM_COSQ];
UINT4 gu4MCQueueHwId[SYS_DEF_MAX_PHYSICAL_INTERFACES][QOS_QUEUE_MAX_NUM_MCOSQ];
UINT4 gau4SubQHwId[SYS_DEF_MAX_PHYSICAL_INTERFACES][QOS_QUEUE_MAX_NUM_SUBQ];
#else
extern UINT4 gu4QueueHwId[SYS_DEF_MAX_PHYSICAL_INTERFACES][VLAN_DEV_MAX_NUM_COSQ];
extern UINT4 gu4MCQueueHwId[SYS_DEF_MAX_PHYSICAL_INTERFACES][QOS_QUEUE_MAX_NUM_MCOSQ];
extern UINT4 gau4SubQHwId[SYS_DEF_MAX_PHYSICAL_INTERFACES][QOS_QUEUE_MAX_NUM_SUBQ];
#endif
#endif
#endif
#endif /* __QOSX_NP_H__ */

