/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pvrstnp.h,v 1.4 2011/02/09 11:56:50 siva Exp $ 
 *
 * Description: All prototypes for Network Processor API functions for PVRST 
 * 
 *
 *******************************************************************/
#ifndef _PVRSTNP_H
#define _PVRSTNP_H

INT1 FsPvrstNpCreateVlanSpanningTree PROTO ((tVlanId VlanId));
INT1 FsPvrstNpDeleteVlanSpanningTree PROTO ((tVlanId VlanId));
INT4 FsPvrstNpInitHw PROTO ((VOID));
VOID FsPvrstNpDeInitHw PROTO ((VOID));
INT4 FsPvrstNpSetVlanPortState PROTO ((UINT4 u4IfIndex, tVlanId VlanId,
                              UINT1 u1PortState));
INT1 FsPvrstNpGetVlanPortState PROTO ((tVlanId VlanId,
                UINT4 u4IfIndex, UINT1* pu1Status));

#endif /* _MSTNP_H */
