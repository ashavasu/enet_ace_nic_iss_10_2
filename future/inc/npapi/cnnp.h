/****************************************************************************/
/*  Copyright (C) 2010 Aricent Inc . All Rights Reserved                     */
/*  $Id: cnnp.h,v 1.1 2010/07/05 06:38:46 prabuc Exp $                                                                   */
/*                                                                          */
/*  FILE NAME             : cnnp.h                                          */
/*  PRINCIPAL AUTHOR      : Aricent                                         */
/*  SUBSYSTEM NAME        : CN                                              */
/*  MODULE NAME           : CN-NPAPI                                        */
/*  LANGUAGE              : C                                               */
/*  TARGET ENVIRONMENT    : Linux                                           */
/*  DATE OF FIRST RELEASE :                                                 */
/*  AUTHOR                : Aricent                                         */
/*  FUNCTIONS DEFINED(Applicable for Source files) :                        */
/*  DESCRIPTION           : This file contains the exported NPAPI functions */
/*                          and macros of Aricent CN  Module.               */
/****************************************************************************/

#ifndef __CN_NP_H__
#define __CN_NP_H__

#include "lr.h"
#include "iss.h"
#include "npapi.h"
#include "cn.h"

/*Congestion point Parameters  */
typedef struct _CpParams
{
   UINT4         u4CpQSizeSetPoint; /* ieee8021CnCpQueueSizeSetPoint */
   UINT4         u4CpMinSampleBase; /* ieee8021CnCpMinSampleBase */
   UINT1         au1CnCpIdentifier[CN_CPID_LEN]; /* Congestion point Identifier */
   UINT1         u1CnCpPriority;  /* ieee8021CnCpPriority */
   UINT1         u1CpMinHeaderOctets; /* ieee8021CnCpMinHeaderOctets */
   INT1          i1CnCpFeedbackWeight;/* ieee8021CnCpFeedbackWeight */
   UINT1         au1Padding[1];
}tCnHwCpParams;



/* NPAPI Functions Prototype */

PUBLIC INT4 FsMiCnHwSetDefenseMode PROTO((UINT4 u4ContextId, /* Context Id*/ 
                                          UINT4 u4PortId,  /*Port Number*/
                                          UINT1 u1CNPV,/* Congestion Notification*/
                                                       /* Priority Value  */
                                        UINT1 u1DefenseMode/* Defense mode to set*/
                                        ));

PUBLIC INT4 FsMiCnHwSetCpParams PROTO((UINT4 u4ContextId,/*Context Id*/
                                       UINT4 u4PortId, /*Port Number*/
                                  UINT4 u4TrafficClass,/*Traffic Class*/  
                                tCnHwCpParams *pCpParams /* Cp Parameters*/
                                ));

PUBLIC INT4 FsMiCnHwSetCnmPriority PROTO((UINT4 u4ContextId, /* Context Id*/
                                          UINT1 u1CnmPri /* CN Message Priority*/
                                         ));

PUBLIC INT4 FsMiCnHwReSetCounters PROTO((UINT4 u4ContextId,/* Context Id*/ 
                                         UINT4 u4PortId,/*Port Number */
                                         UINT1 u1CNPV /* CN Priority Value*/
                                         ));

PUBLIC INT4 FsMiCnHwGetCounters PROTO((UINT4 u4ContextId,/* Context Id */
                                       UINT4 u4PortId,/* Port Number*/
                                       UINT1 u1CNPV, /*CN Priority Value*/
                              FS_UINT8 *pDisFrames, /*Discarded Frames */
                              FS_UINT8 *pTransFrames, /*Trasmitted Frames*/
                              FS_UINT8 *PTransCnms /*Trasmitted CNMs*/
                              ));

PUBLIC INT4 FsMiCnHwCallBkFunc PROTO((UINT1 *pu1CpId,  /* Cp Identifier*/
                                     INT4 i4CnmQOffset,/* CNM offset */
                                     INT4 i4CnmQDelta  /* CNM Delta */
                                     ));


#endif
/*-----------------------------------------------------------------------*/
/*                       End of the file  cnnp.h                         */
/*-----------------------------------------------------------------------*/

