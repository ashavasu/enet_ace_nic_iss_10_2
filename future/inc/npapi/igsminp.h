/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igsminp.h,v 1.7 2010/07/07 12:59:42 prabuc Exp $
 *
 * Description: Prototypes for IGMP Snooping module's NP-API.
 *
 *******************************************************************/
#ifndef _IGSMINP_H
#define _IGSMINP_H

INT4 FsMiIgsHwEnableIgmpSnooping PROTO ((UINT4));
INT4 FsMiIgsHwDisableIgmpSnooping PROTO ((UINT4));

INT4
FsMiNpUpdateMcFwdEntries PROTO ((UINT4 u4Instance, 
                                 tSnoopVlanId VlanId, UINT4 u4GrpAddr, 
                        UINT4 u4SrcAddr, UINT1 *pu1PortList, 
                        UINT1 u1EventType));

INT4 FsMiNpGetFwdEntryHitBitStatus PROTO ((UINT4 u4Instance, UINT2, UINT4,
                                         UINT4));

INT4 FsMiIgsHwAddRtrPort PROTO ((UINT4, tSnoopVlanId, UINT2));
INT4 FsMiIgsHwDelRtrPort PROTO ((UINT4, tSnoopVlanId, UINT2));

INT4 FsMiIgsHwEnableIpIgmpSnooping PROTO ((UINT4));
INT4 FsMiIgsHwDisableIpIgmpSnooping PROTO ((UINT4));

INT4 FsMiNpUpdateIpmcFwdEntries (UINT4, tSnoopVlanId, UINT4, UINT4, tPortList, tPortList, UINT1);

INT4 FsMiIgsHwUpdateIpmcEntry PROTO ((UINT4, tIgsHwIpFwdInfo *, UINT1 u1Action));
INT4 FsMiIgsHwGetIpFwdEntryHitBitStatus PROTO ((UINT4, tIgsHwIpFwdInfo *));
INT4 FsMiIgsHwEnhancedMode PROTO ((UINT4 u4ContextId, UINT1 u1ConfigStatus));
INT4 FsMiIgsHwSparseMode PROTO ((UINT4 u4ContextId, UINT1 u1SparseStatus));
VOID FsMiIgsHwClearIpmcFwdEntries PROTO ((UINT4 u4ContextId));
INT4 FsMiIgsHwPortRateLimit PROTO ((UINT4 u4ContextId,
                                    tIgsHwRateLmt *pIgsHwRateLmt, UINT1 u1Action));
#ifdef L2RED_WANTED
INT4
FsNpGetIpMcastFwdEntry (tSnoopVlanId VlanId, UINT4 u4GrpAddr, UINT4 u4SrcAddr,
                        UINT1 *pu1PortList);
INT4
FsNpGetFirstIpMcastFwdEntry (INT4 *pi4VlanId, UINT4 *pu4McastGrpIpAddr,
                             UINT4 *pu4McastSrcIpAddr);

INT4
FsNpGetNextIpMcastFwdEntry (INT4 i4VlanId, INT4 *pi4VlanId,
                            UINT4 u4McastGrpIpAddr, UINT4 *pu4McastGrpIpAddr,
                            UINT4 u4McastSrcIpAddr, UINT4 *pu4McastSrcIpAddr);
#endif

#ifdef MBSM_WANTED
INT4 FsMiIgsMbsmHwEnableIgmpSnooping PROTO ((UINT4 u4ContextId, 
                                             tMbsmSlotInfo * pSlotInfo));
INT4 FsMiIgsMbsmHwEnableIpIgmpSnooping PROTO ((UINT4 u4ContextId, 
                                               tMbsmSlotInfo * pSlotInfo));
INT4 FsMiIgsMbsmHwEnhancedMode PROTO ((UINT4 u4ContextId, UINT1 u1ConfigStatus,
                                       tMbsmSlotInfo * pSlotInfo));
INT4 FsMiIgsMbsmHwPortRateLimit PROTO ((UINT4, tIgsHwRateLmt *,UINT1,
                                        tMbsmSlotInfo *));
INT4 FsMiIgsMbsmHwSparseMode PROTO ((UINT4 u4ContextId, UINT1 u1ConfigStatus,
                                       tMbsmSlotInfo * pSlotInfo));
#endif

#endif

