/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: poenp.h,v 1.2 2007/02/01 14:52:34 iss Exp $
 *
 * Description: Prototypes for Link Aggregation module's NP-API.
 *
 *******************************************************************/

#ifndef _POE_NP_H
#define _POE_NP_H

INT4 FsPoeHwInit PROTO((VOID)); 
INT4 FsPoeHwDeInit PROTO((VOID)); 
INT4 FsPoeGetPseStatus PROTO((VOID)); 
INT4 FsPoeDetectAndClassifyPD (tPoePsePortEntry * pPoePsePortEntry, INT4 i4ErrStatus);
INT4 FsPoeApplyPowerOnPI (tPoePsePortEntry * pPoePsePortEntry , INT4 i4ErrStatus);
INT4 FsPoeRemovePowerFromPI (tPoePsePortEntry * pPoePsePortEntry , INT4 i4ErrStatus);
#endif /* _POE_NP_H */
