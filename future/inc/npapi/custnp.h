/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: custnp.h,v 1.9 2015/06/10 05:31:51 siva Exp $
 *
 * Description: This file contains the controlling application specific 
 *              function prototypes and data definitions. The file 
 *              can be ported specific to customer requirements.
 *
 *******************************************************************/

#ifndef __CUSTNP_H_
#define __CUSTNP_H_
INT4
CustNpGetIfIndexInfo PROTO ((tNpInterfaceInfo *pNpInterfaceInfo));

INT4 InitializeFm PROTO ((INT1  *pi1Param));
#if defined(OPUS24) || defined (OPUS_NEM)
#define CFA_NP_FM_MAX_GIG_PORTS     0
#define CFA_NP_FM_MAX_SWITCH        1
#elif defined(OPUS_TOR)
#define CFA_NP_FM_MAX_GIG_PORTS     0
#define CFA_NP_FM_MAX_SWITCH        9
#elif defined(ALTA_WANTED) || defined (RRC_WANTED)
#if defined(OPUSR72)
#define CFA_NP_FM_MAX_GIG_PORTS     0
#define CFA_NP_FM_MAX_SWITCH        1
#else
#define CFA_NP_FM_MAX_GIG_PORTS     48
#define CFA_NP_FM_MAX_SWITCH        1
#endif
#else /* vegas by default */
#define CFA_NP_FM_MAX_GIG_PORTS     48
#define CFA_NP_FM_MAX_SWITCH        4
#endif
#endif
