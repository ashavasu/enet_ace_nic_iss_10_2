/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: brgnp.h,v 1.3 2007/02/01 14:52:34 iss Exp $ 
 *
 * Description: All prototypes for Network Processor API functions for BRIDGE 
 * 
 *
 *******************************************************************/
#ifndef _BRGNP_H
#define _BRGNP_H

/* Prototype for Bridging General functionality*/
INT1 
FsBrgGetMaxInfo PROTO ((UINT4 u4Port,INT4 *pi4MaxVal));

/* Prototype for managing Forwarding Database Table as per 802.1D */
INT1
FsBrgSetAgingTime PROTO ((INT4 i4AgingTime));

INT1
FsBrgFindFdbMacEntry PROTO ((tMacAddr macAddr, UINT4 *pu4PortNo));

INT1
FsBrgGetFdbEntryFirst PROTO ((tMacAddr macAddr));

INT1
FsBrgGetFdbNextEntry PROTO ((tMacAddr macAddr,tMacAddr nxtAddr));

INT1
FsBrgFlushPort PROTO ((UINT4));

INT1
FsBrgAddMacEntry PROTO ((UINT4 u4Port,tMacAddr macAddr));

INT1
FsBrgDelMacEntry PROTO ((UINT4 u4Port,tMacAddr macAddr));

INT1
FsBrgSetPortIngStFilt PROTO ((UINT4 u4Port, tMacAddr macAddr, 
                              UINT1 ucMember[BRG_PORT_LIST_SIZE]));

INT1
FsBrgSetPortIngStFiltFlags PROTO ((UINT4 u4ReceivePort, 
                                   tMacAddr staticAddress,
                                   INT4 i4StaticStatus));

/* Prototypes for supporting explicit MAC Multicast distribution groups */
INT1 
FsBrgMcAddEntry PROTO ((INT4 i4GrpID,tMacAddr multiAddr));

INT1 
FsBrgMcDeleteEntry PROTO ((INT4 i4GrpID,tMacAddr multiAddr));

INT1 
FsBrgSetMcGrp2Port PROTO ((UINT4 u4Port, INT4 i4GrpId));

INT1 
FsBrgMcEnAddrInGrp PROTO ((INT4 i4GrpId, tMacAddr multiAddr, INT4 i4Status));

/* Prototypes of egress and ingress counter mechanisms */
INT1 
FsBrgGetLearnedEntryDisc PROTO ((UINT4 *pu4Val));

#ifdef MBSM_WANTED
INT1  
FsBrgMbsmSetAgingTime PROTO ((INT4 , tMbsmSlotInfo *));
#endif

#endif /* _BRGNP_H */
