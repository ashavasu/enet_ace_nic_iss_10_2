/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlnpbnp.h,v 1.11 2013/12/05 12:48:26 siva Exp $
 *
 * Description: This file contains the function implementations  of FS NP-API.
 ****************************************************************************/

#ifndef _VLNPBNP_H
#define _VLNPBNP_H

#include "l2iwf.h"

INT4 
FsVlanHwSetProviderBridgePortType PROTO ((UINT4 u4IfIndex, UINT4 u4PortType));

INT4
FsVlanHwSetPortSVlanTranslationStatus PROTO ((UINT4 u4IfIndex, UINT1 u1Status));

INT4
FsVlanHwAddSVlanTranslationEntry PROTO ((UINT4 u4IfIndex, UINT2 u2LocalSVlan,
                                                 UINT2 u2RelaySVlan));
INT4 
FsVlanHwDelSVlanTranslationEntry PROTO ((UINT4 u4IfIndex, UINT2 u2LocalSVlan,
                                                 UINT2 u2RelaySVlan));
INT4
FsVlanHwSetPortEtherTypeSwapStatus PROTO ((UINT4 u4IfIndex, UINT1 u1Status));

INT4 
FsVlanHwAddEtherTypeSwapEntry PROTO ((UINT4 u4IfIndex, UINT2 u2LocalEtherType,
                                           UINT2 u2RelayEtherType));
INT4 
FsVlanHwDelEtherTypeSwapEntry PROTO ((UINT4 u4IfIndex, UINT2 u2LocalEtherType,
                                      UINT2 u2RelayEtherType));

INT4
FsVlanHwAddSVlanMap PROTO ((tVlanSVlanMap VlanSVlanMap));

INT4
FsVlanHwDeleteSVlanMap PROTO ((tVlanSVlanMap VlanSVlanMap));

INT4
FsVlanHwSetPortSVlanClassifyMethod PROTO((UINT4 u4IfIndex, UINT1 u1TableType));

INT4 
FsVlanHwPortMacLearningLimit PROTO ((UINT4 u4IfIndex, UINT4 u4MacLimit));
INT4 
FsVlanHwMulticastMacTableLimit  PROTO ((UINT4 u4MacLimit));

INT4 
FsVlanHwResetPortCustomerVlan PROTO ((UINT4 u4IfIndex));

INT4 
FsVlanHwSetPortCustomerVlan PROTO ((UINT4 u4IfIndex, tVlanId  CVlanId));

INT4
FsVlanHwCreateProviderEdgePort (UINT4 u4IfIndex, tVlanId SVlanId,
                                tHwVlanPbPepInfo PepConfig);

INT4
FsVlanHwDelProviderEdgePort (UINT4 u4IfIndex, tVlanId SVlanId);

INT4 
FsVlanHwSetPepPvid (UINT4 u4IfIndex, tVlanId SVlanId, tVlanId Pvid);

INT4
FsVlanHwSetPepAccFrameType (UINT4 u4IfIndex, tVlanId SVlanId, 
                            UINT1 u1AccepFrameType);
INT4
FsVlanHwSetPepDefUserPriority (UINT4 u4IfIndex, tVlanId SVlanId, 
                               INT4 i4DefUsrPri);
INT4
FsVlanHwSetPepIngFiltering (UINT4 u4IfIndex, tVlanId SVlanId, 
                            UINT1 u1IngFilterEnable);
INT4
FsVlanHwSetPcpEncodTbl (UINT4 u4IfIndex, tHwVlanPbPcpInfo NpPbVlanPcpInfo);
INT4
FsVlanHwSetPcpDecodTbl (UINT4 u4IfIndex, tHwVlanPbPcpInfo PcpTblEntry);

INT4
FsVlanHwSetPortUseDei (UINT4 u4IfIndex, UINT1 u1UseDei);

INT4
FsVlanHwSetPortReqDropEncoding (UINT4 u4IfIndex, UINT1 u1ReqDrpEncoding);

INT4
FsVlanHwSetPortPcpSelection (UINT4 u4IfIndex, UINT2 u2PcpSelection);

INT4
FsVlanHwSetServicePriRegenEntry (UINT4 u4IfIndex, tVlanId SVlanId, 
                                 INT4 i4RecvPriority, INT4 i4RegenPriority);

INT4
FsVlanHwSetTunnelMacAddress (tMacAddr MacAddr, UINT2 u2Protocol);
INT4
FsVlanHwSetCvidUntagPep (tVlanSVlanMap VlanSVlanMap);
INT4
FsVlanHwSetCvidUntagCep (tVlanSVlanMap VlanSVlanMap);

INT4
FsVlanHwFidMacLearningLimit (UINT2 u2Fid, UINT4 u4MacLimit);

INT4
FsVlanHwFidMacLearningStatus (UINT2 u2Fid, UINT1 u1Status);

INT4
FsVlanHwGetNextSVlanMap (tVlanSVlanMap VlanSVlanMap,
                         tVlanSVlanMap *pRetVlanSVlanMap);

INT4
FsVlanHwGetNextSVlanTranslationEntry (UINT4 u4IfIndex, tVlanId u2LocalSVlan,
                                  tVidTransEntryInfo *pVidTransEntryInfo);


#ifdef MBSM_WANTED
INT4
FsVlanMbsmHwSetProviderBridgePortType (UINT4 u4IfIndex, UINT4 u4PortType,
                                       tMbsmSlotInfo * pSlotInfo);
INT4
FsVlanMbsmHwSetPortSVlanTranslationStatus (UINT4 u4IfIndex, UINT1 u1Status,
                                           tMbsmSlotInfo * pSlotInfo);
INT4
FsVlanMbsmHwAddSVlanTranslationEntry (UINT4 u4IfIndex, UINT2 u2LocalSVlan,
                                  UINT2 u2RelaySVlan,
                                  tMbsmSlotInfo * pSlotInfo);
INT4
FsVlanMbsmHwSetPortEtherTypeSwapStatus (UINT4 u4IfIndex, UINT1 u1Status,
                                        tMbsmSlotInfo * pSlotInfo);
INT4
FsVlanMbsmHwAddEtherTypeSwapEntry (UINT4 u4IfIndex, UINT2 u2LocalEtherType,
                               UINT2 u2RelayEtherType,
                               tMbsmSlotInfo * pSlotInfo);
INT4
FsVlanMbsmHwAddSVlanMap (tVlanSVlanMap VlanSVlanMap,
                         tMbsmSlotInfo * pSlotInfo);

INT4
FsVlanMbsmHwSetPortSVlanClassifyMethod (UINT4 u4IfIndex, UINT1 u1TableType,
                                        tMbsmSlotInfo * pSlotInfo);

INT4
FsVlanMbsmHwPortMacLearningLimit (UINT4 u4IfIndex, UINT4 u4MacLimit,
                                  tMbsmSlotInfo * pSlotInfo);

INT4
FsVlanMbsmHwMulticastMacTableLimit (UINT4 u4MacLimit,
                                    tMbsmSlotInfo * pSlotInfo);

INT4
FsVlanMbsmHwSetPortCustomerVlan (UINT4 u4Port, tVlanId CVlanId,
                                 tMbsmSlotInfo * pSlotInfo);

INT4
FsVlanMbsmHwCreateProviderEdgePort (UINT4 u4IfIndex, tVlanId SVlanId,
                                tHwVlanPbPepInfo PepConfig,
                                tMbsmSlotInfo * pSlotInfo);
INT4
FsVlanMbsmHwSetPcpEncodTbl (UINT4 u4IfIndex, tHwVlanPbPcpInfo NpPbVlanPcpInfo,
                            tMbsmSlotInfo * pSlotInfo);
INT4
FsVlanMbsmHwSetPcpDecodTbl (UINT4 u4IfIndex, tHwVlanPbPcpInfo NpPbVlanPcpInfo,
                            tMbsmSlotInfo * pSlotInfo);

INT4
FsVlanMbsmHwSetPortUseDei (UINT4 u4IfIndex, UINT1 u1UseDei,
                           tMbsmSlotInfo * pSlotInfo);

INT4
FsVlanMbsmHwSetPortReqDropEncoding (UINT4 u4IfIndex, UINT1 u1ReqDrpEncoding,
                                    tMbsmSlotInfo * pSlotInfo);
INT4
FsVlanMbsmHwSetPortPcpSelection (UINT4 u4IfIndex, UINT2 u2PcpSelection,
                                 tMbsmSlotInfo * pSlotInfo);

INT4
FsVlanMbsmHwSetServicePriRegenEntry (UINT4 u4IfIndex, tVlanId SVlanId,
                                 INT4 i4RecvPriority, INT4 i4RegenPriority,
                                 tMbsmSlotInfo * pSlotInfo);

INT4
FsVlanMbsmAddCVidEntry (INT4 i4BcmUnit, INT4 i4BcmPort,
                        tVlanSVlanMap VlanSVlanMap,
                        tMbsmSlotInfo * pSlotInfo);

INT4
FsVlanMbsmHwMulticastMacTableLimit (UINT4 u4MacLimit,
                                    tMbsmSlotInfo * pSlotInfo);

#endif
#endif
