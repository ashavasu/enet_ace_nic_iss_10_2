/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlannp.h,v 1.37 2014/08/14 10:32:51 siva Exp $ * /
 *
 * Description: This file contains the function implementations  of FS NP-API.
 ****************************************************************************/
#ifndef _VLANNP_H
#define _VLANNP_H


INT4 FsVlanHwInit PROTO ((VOID));
INT4 FsVlanHwDeInit PROTO ((VOID));
INT4 FsVlanHwSetBrgMode PROTO ((UINT4 u4BridgeMode));
INT4 FsVlanHwSetAllGroupsPorts PROTO ((tVlanId VlanId, tPortList Ports));
INT4 FsVlanHwResetAllGroupsPorts  PROTO ((tVlanId, tPortList));
INT4 FsVlanHwSetUnRegGroupsPorts PROTO ((tVlanId VlanId, tPortList Ports));
INT4 FsVlanHwResetUnRegGroupsPorts PROTO ((tVlanId, tPortList));
INT4 FsVlanHwAddStaticUcastEntry PROTO ((UINT4 u4Fid, tMacAddr MacAddr, 
                                UINT4 u4RcvPort, tPortList AllowedToGoPorts, 
                                UINT1 u1Status));
INT4 FsVlanHwAddStaticUcastEntryEx PROTO ((UINT4 u4Fid, tMacAddr MacAddr,
                                UINT4 u4RcvPort, tPortList AllowedToGoPorts, 
                                UINT1 u1Status, tMacAddr ConnectionId));
INT4 FsVlanHwDelStaticUcastEntry PROTO ((UINT4 u4Fid, tMacAddr MacAddr, 
                        UINT4 u4RcvPort));
INT4 FsVlanHwAddPortMacVlanEntry PROTO (( UINT4 u4Port,
                                           tMacAddr MacAddr, UINT4 u4VlanId));

INT4 FsVlanHwDeletePortMacVlanEntry PROTO (( UINT4 u4Port,
                                              tMacAddr MacAddr));
INT4 FsVlanHwGvrpEnable PROTO((VOID));
INT4 FsVlanHwGvrpDisable PROTO((VOID));

INT4 FsVlanHwSetSubnetBasedStatusOnPort PROTO ((UINT4 u4IfIndex,
                                                UINT1 u1SubnetBasedVlanEnable));
INT4 FsVlanHwAddPortSubnetVlanEntry PROTO ((UINT4 u4IfIndex, UINT4 SubnetAddr,
                                            tVlanId VlanId, BOOL1 bARPOption));
INT4 FsVlanHwDeletePortSubnetVlanEntry PROTO ((UINT4 u4IfIndex, UINT4 SubnetAddr));

INT4
FsVlanHwSetPortIngressEtherType PROTO ((UINT4 u4IfIndex, UINT2 u2EtherType));

INT4
FsVlanHwSetPortEgressEtherType PROTO ((UINT4 u4IfIndex, UINT2 u2EtherType));

INT4
FsVlanHwSetPortProperty (UINT4 u4ContextId,UINT4 u4IfIndex,
                           tHwVlanPortProperty VlanPortProperty);

#ifndef SW_LEARNING
INT4
FsVlanHwGetFdbCount PROTO ((UINT4 u4FdbId, UINT4 *pu4Count));

INT4
FsVlanHwGetFirstTpFdbEntry PROTO ((UINT4 *pu4FdbId, tMacAddr MacAddr));

INT4
FsVlanHwGetNextTpFdbEntry PROTO ((UINT4 u4FdbId, tMacAddr MacAddr,
                           UINT4 *pu4NextFdbId, tMacAddr NextMacAddr));

INT4 FsVlanHwGetFdbEntry PROTO ((UINT4 u4FdbId, tMacAddr MacAddr, 
                        tHwUnicastMacEntry *pEntry));
    
#endif

INT4 FsVlanHwAddMcastEntry PROTO ((tVlanId VlanId, tMacAddr MacAddr, 
                        tPortList PortList));

INT4 FsVlanHwAddStMcastEntry PROTO ((tVlanId, tMacAddr , INT4, tPortList));

INT4 FsVlanHwSetMcastPort PROTO ((tVlanId      VlanId, tMacAddr MacAddr, 
                        UINT4       u4Port));

INT4 FsVlanHwResetMcastPort PROTO ((tVlanId      VlanId, tMacAddr MacAddr, 
                        UINT4       u4Port));

INT4 FsVlanHwDelMcastEntry PROTO ((tVlanId VlanId, tMacAddr MacAddr));

INT4 FsVlanHwDelStMcastEntry PROTO ((tVlanId, tMacAddr, INT4));

INT4 FsVlanHwAddVlanEntry PROTO ((tVlanId VlanId, tPortList HwPortList, 
                        tPortList UnTagPorts));
INT4 FsVlanHwDelVlanEntry PROTO ((tVlanId VlanId));

INT4 FsVlanHwSetVlanMemberPort PROTO ((tVlanId VlanId, UINT4 u4Port, 
                        UINT1 u1IsTagged));
INT4 FsVlanHwResetVlanMemberPort PROTO ((tVlanId VlanId, UINT4 u4Port));

INT4 FsVlanHwSetPortPvid PROTO ((UINT4 u4Port, tVlanId VlanId));
#ifdef L2RED_WANTED
INT4 FsVlanHwSyncDefaultVlanId (tVlanId VlanId);
INT4 FsVlanRedHwUpdateDBForDefaultVlanId (tVlanId VlanId);
#endif
INT4 FsVlanHwSetDefaultVlanId (tVlanId DefaultVlanId);
INT4 FsVlanHwSetBaseBridgeMode (UINT4 u4Mode);
INT4 FsVlanHwSetPortAccFrameType PROTO ((UINT4 u4Port, INT1 i1AccFrameType));
INT4 FsVlanHwSetPortIngFiltering PROTO ((UINT4 u4Port, UINT1 u1IngFiltering));
INT4 FsVlanHwVlanEnable PROTO ((VOID));
INT4 FsVlanHwVlanDisable PROTO ((VOID));

INT4 FsVlanHwSetVlanLearningType PROTO ((UINT1 u1LearningType));
INT4 FsVlanHwSetMacBasedStatusOnPort PROTO((UINT4, UINT1));
INT4 FsVlanHwEnableProtoVlanOnPort PROTO((UINT4, UINT1));
INT4 FsVlanHwSetDefUserPriority PROTO ((UINT4 u4Port, INT4 i4DefPriority));
INT4 FsVlanHwSetPortNumTrafClasses PROTO ((UINT4 u4InPort, 
                        INT4 i4NumTraffClass));
INT4 FsVlanHwSetRegenUserPriority PROTO ((UINT4 u4Port, INT4 i4UserPriority, 
                        INT4 i4RegenPriority));
INT4 FsVlanHwSetTraffClassMap PROTO ((UINT4 u4InPort, INT4 i4UserPriority, 
                        INT4 i4TraffClass));
INT4 FsVlanHwGmrpEnable PROTO ((VOID));
INT4 FsVlanHwGmrpDisable PROTO ((VOID));

VOID FsNpDeleteAllFdbEntries PROTO((VOID));

INT4 FsVlanHwAddVlanProtocolMap PROTO ((UINT4 u4IfIndex, UINT4 u4GroupId,
                            tVlanProtoTemplate * pProtoTemplate, 
                            tVlanId VlanId));
INT4 FsVlanHwDelVlanProtocolMap PROTO ((UINT4 u4IfIndex, UINT4 u4GroupId,
                            tVlanProtoTemplate * pProtoTemplate));

INT4 FsVlanHwGetPortStats PROTO ((UINT4 u4Port, tVlanId VlanId, 
                          UINT1 u1StatsType,
                          UINT4 *pu4PortStatsValue));

INT4 FsVlanHwGetPortStats64 PROTO ((UINT4 u4Port, tVlanId VlanId, 
                                    UINT1 u1StatsType,
                             tSNMP_COUNTER64_TYPE *pValue));

INT4 FsVlanHwSetPortTunnelMode      PROTO ((UINT4, UINT4));
INT4 FsVlanHwSetTunnelFilter PROTO ((INT4));
INT4 FsVlanHwCheckTagAtEgress PROTO ((UINT1 *));
INT4 FsVlanHwCheckTagAtIngress PROTO ((UINT1 *));

INT4 FsVlanHwCreateFdbId PROTO ((UINT4 u4Fid));
INT4 FsVlanHwDeleteFdbId PROTO ((UINT4 u4Fid));
INT4 FsVlanHwAssociateVlanFdb PROTO ((UINT4 u4Fid, tVlanId VlanId));
INT4 FsVlanHwDisassociateVlanFdb PROTO ((UINT4 u4Fid, tVlanId VlanId));

INT4 FsVlanHwFlushPortFdbId PROTO ((UINT4 u4Port, UINT4 u4Fid, 
                                    INT4 i4OptimizeFlag));

INT4 FsVlanHwFlushPort PROTO ((UINT4 u4Port, INT4 i4OptimizeFlag));
INT4 FsVlanHwFlushFdbId PROTO ((UINT4 u4Fid));

INT4 FsVlanHwSetShortAgeout PROTO ((UINT4 u4PortNum, INT4 i4AgingTime));
INT4 FsVlanHwResetShortAgeout PROTO ((UINT4 u4PortNum, INT4 i4LongAgeout));

INT4 FsVlanHwGetVlanInfo PROTO ((tVlanId VlanId, tHwVlanEntry *pHwEntry));
INT4 FsVlanHwGetMcastEntry PROTO ((tVlanId VlanId, tMacAddr MacAddr, 
                            tPortList HwPortList));
INT4 FsVlanHwTraffClassMapInit PROTO ((UINT1 u1Priority, INT4 i4CosqValue));
INT4 FsVlanHwGetVlanStats PROTO ((tVlanId VlanId,
                        UINT1 u1StatsType, UINT4 *pu4VlanStatsValue));
INT4 FsVlanHwResetVlanStats PROTO ((tVlanId VlanId));
INT4
FsVlanHwMacLearningStatus (tVlanId VlanId, UINT2 u2FdbId, tPortList HwPortList, 
                           UINT1 u1Status);
INT4
FsVlanHwMacLearningLimit (tVlanId VlanId, UINT2 u2FdbId, UINT4 u4MacLimit);

INT4
FsVlanHwPortMacLearningStatus PROTO ((UINT4 u4IfIndex, UINT1 u1Status));
INT4
FsVlanHwSwitchMacLearningLimit (UINT4 u4MacLimit);

INT4
FsVlanHwSetFidPortLearningStatus (UINT4 u4Fid,  tPortList HwPortList,
                                  UINT4 u4Port, UINT1 u1Action);

INT4
FsVlanHwSetPortProtectedStatus (UINT4 u4IfIndex, INT4 i4ProtectedStatus);

INT4
FsVlanHwSetProtocolTunnelStatusOnPort (UINT4 u4IfIndex,
                               tVlanHwTunnelFilters ProtocolId,
                                       UINT4 u4TunnelStatus);

#ifdef L2RED_WANTED
typedef VOID (*FsVlanHwProtoCb)(UINT2 u2Port, UINT4 u4GroupId, 
                                tVlanProtoTemplate *pProto,
                                tVlanId VlanId);

typedef VOID (*FsVlanHwMcastCb)(tVlanId VlanId, tMacAddr  MacAddr, 
                                UINT2 u2RcvPort, tPortList EgressPorts);

typedef VOID (*FsVlanHwUcastCb)(UINT4 u4FdbId, tMacAddr  MacAddr, 
                                UINT2 u2RcvPort, tHwUnicastMacEntry *pEntry, 
                                tPortList AllowedList, UINT1 u1Status);

INT4 FsVlanHwScanProtocolVlanTbl (UINT4 u4Port, 
                                  FsVlanHwProtoCb ProtoVlanCallBack);
INT4 FsVlanHwGetVlanProtocolMap (UINT4 u4IfIndex, UINT4 u4GroupId,
                                 tVlanProtoTemplate * pProtoTemplate, 
                                 tVlanId *pVlanId);
INT4 FsVlanHwScanMulticastTbl (FsVlanHwMcastCb McastCallBack);
INT4 FsVlanHwGetStMcastEntry (tVlanId VlanId, tMacAddr MacAddr, 
                              UINT4 u4RcvPort, tPortList HwPortList);
INT4 FsVlanHwScanUnicastTbl (FsVlanHwUcastCb UcastCallBack);
INT4 FsVlanHwGetStaticUcastEntry (UINT4 u4Fid, tMacAddr MacAddr,
                                  UINT4 u4Port, tPortList AllowedToGoPortBmp,
                                  UINT1 *pu1Status);
INT4 FsVlanHwGetStaticUcastEntryEx (UINT4 u4Fid, tMacAddr MacAddr,
                                  UINT4 u4Port, tPortList AllowedToGoPortBmp,
                                  UINT1 *pu1Status, tMacAddr ConnectionId);
INT4 FsVlanHwGetFirstProtocolVlanEntry (UINT2 *pu2Port, 
                                        tVlanProtoTemplate *pProtoTemplate,
                                        UINT4 *pu4GroupId, 
                                        tVlanId *pVlanId);
INT4 FsVlanHwGetNextProtocolVlanEntry (UINT2 u2CurrPort, 
                                       UINT2 *pu2NextPort, 
                                       tVlanProtoTemplate *pCurrProtoTemplate,
                                       tVlanProtoTemplate *pNextProtoTemplate,
                                       UINT4 u4CurrGroupId, 
                                       UINT4 *pu4NextGroupId, 
                                       tVlanId *pVlanId);
INT4 FsVlanHwGetFirstL2McEntry (tVlanId *pVlanId, tMacAddr MacAddr, 
                                UINT2 *pu2RcvPort, tPortList HwPortList);
INT4 FsVlanHwGetNextL2McEntry (tVlanId CurrVlanId, tVlanId *pNextVlanId, 
                               tMacAddr CurrMacAddr, tMacAddr NextMacAddr,
                               UINT2 u2CurrRcvPort, UINT2 *pu2NextRcvPort, 
                               tPortList HwPortList);
INT4 FsVlanHwGetFirstL2UcastEntry (UINT4 *pu4FdbId, tMacAddr MacAddr, 
                                   tHwUnicastMacEntry *pHwEntry);
INT4 FsVlanHwGetNextL2UcastEntry (UINT4 u4CurrFdbId, UINT4 *pu4NextFdbId, 
                                  tMacAddr CurrMacAddr, 
                                  tMacAddr NextMacAddr, 
                                  tHwUnicastMacEntry *pHwEntry);
INT4
FsVlanHwGetSyncedTnlProtocolMacAddr (UINT2 u2Protocol, tMacAddr MacAddr);
#endif
INT4 FsVlanNpHwRunMacAgeing (VOID);
#ifdef MBSM_WANTED
INT4 FsVlanMbsmHwInit                     (tMbsmSlotInfo *);
INT4 FsVlanMbsmHwSetBrgMode               (UINT4, tMbsmSlotInfo *);
INT4 FsVlanMbsmHwSetDefaultVlanId         (tVlanId, tMbsmSlotInfo *);
INT4 FsVlanMbsmHwSetAllGroupsPorts        (tVlanId, tPortList, tMbsmSlotInfo *);
INT4 FsVlanMbsmHwSetUnRegGroupsPorts      (tVlanId, tPortList, tMbsmSlotInfo *);
INT4 FsVlanMbsmHwAddStaticUcastEntry      (UINT4, tMacAddr, UINT4, tPortList, UINT1, 
                                           tMbsmSlotInfo *);
INT4 FsVlanMbsmHwAddStaticUcastEntryEx     (UINT4, tMacAddr, UINT4, tPortList, 
                                            UINT1, tMacAddr, tMbsmSlotInfo *);

INT4 FsVlanMbsmHwSetUcastPort             (UINT4 u4FdbId, tMacAddr MacAddr,
                                           UINT4 u4RcvPort, UINT4 u4AllowedToGoPort,
                                           UINT1 u1Status, tMbsmSlotInfo * pSlotInfo);
INT4 FsVlanMbsmHwResetUcastPort           (UINT4 u4FdbId, tMacAddr MacAddr,
                                           UINT4 u4RcvPort, UINT4 u4AllowedToGoPort,
                                           tMbsmSlotInfo * pSlotInfo);
INT4 FsVlanMbsmHwDelStaticUcastEntry      (UINT4, tMacAddr, UINT2, tMbsmSlotInfo *);
INT4 FsVlanMbsmHwAddMcastEntry            (tVlanId, tMacAddr, tPortList, tMbsmSlotInfo *);
INT4 FsVlanMbsmHwAddStMcastEntry          (tVlanId, tMacAddr, INT4, tPortList, tMbsmSlotInfo *);
INT4 FsVlanMbsmHwDelStMcastEntry          (tVlanId VlanId, tMacAddr MacAddr,
                                           UINT4 u4RcvPort, tMbsmSlotInfo * pSlotInfo);
INT4 FsVlanMbsmHwSetMcastPort             (tVlanId VlanId, tMacAddr MacAddr, UINT4 u4Port,
                                           tMbsmSlotInfo * pSlotInfo);
INT4 FsVlanMbsmHwResetMcastPort           (tVlanId, tMacAddr, UINT4, tMbsmSlotInfo *);
INT4 FsVlanMbsmHwAddVlanEntry             (tVlanId, tPortList, tPortList, tMbsmSlotInfo *);
INT4 FsVlanMbsmHwSetVlanMemberPort        (tVlanId VlanId, UINT4 u4Port,
                                           UINT1 u1IsTagged, tMbsmSlotInfo * pSlotInfo);
INT4 FsVlanMbsmHwResetVlanMemberPort      (tVlanId, UINT4, tMbsmSlotInfo *);
INT4 FsVlanMbsmHwSetPortPvid              (UINT4, tVlanId, tMbsmSlotInfo *);
INT4 FsVlanMbsmHwSetPortAccFrameType      (UINT4, UINT1, tMbsmSlotInfo *);
INT4 FsVlanMbsmHwSetPortIngFiltering      (UINT4, UINT1, tMbsmSlotInfo *);
INT4 FsVlanMbsmHwSetSubnetBasedStatusOnPort (UINT4, UINT1, tMbsmSlotInfo *);
INT4 FsVlanMbsmHwEnableProtoVlanOnPort    (UINT4, UINT1, tMbsmSlotInfo *);
INT4 FsVlanMbsmHwSetDefUserPriority       (UINT4, INT4, tMbsmSlotInfo *);
INT4 FsVlanMbsmHwSetPortNumTrafClasses    (UINT4, INT4, tMbsmSlotInfo *);
INT4 FsVlanMbsmHwSetRegenUserPriority     (UINT4, INT4, INT4, tMbsmSlotInfo *);
INT4 FsVlanMbsmHwSetTraffClassMap         (UINT4, INT4, INT4, tMbsmSlotInfo *);
INT4 FsVlanMbsmHwSetTunnelFilter (INT4, tMacAddr, UINT2, tMbsmSlotInfo *);
INT4 FsVlanMbsmHwAddPortSubnetVlanEntry (UINT4, UINT4, tVlanId, UINT1, 
                                         tMbsmSlotInfo *);

INT4 FsVlanMbsmHwTraffClassMapInit PROTO ((UINT1 u1Priority, 
                                    INT4 i4CosqValue, 
                                    tMbsmSlotInfo *pSlotInfo));
INT4
FsVlanMbsmHwSetFidPortLearningStatus (UINT4 u4Fid, UINT4 u4Port, 
                                      UINT1 u1Action, 
                                      tMbsmSlotInfo * pSlotInfo);
INT4
FsVlanMbsmHwSwitchMacLearningLimit (UINT4 u4MacLimit, 
                                    tMbsmSlotInfo * pSlotInfo);
INT4
FsVlanMbsmHwMacLearningLimit (tVlanId VlanId, UINT2 u2FdbId, UINT4 u4MacLimit,
                              tMbsmSlotInfo * pSlotInfo);
INT4
FsVlanMbsmHwPortMacLearningStatus (UINT4 u4IfIndex, UINT1 u1Status,
                                   tMbsmSlotInfo * pSlotInfo);
INT4
FsVlanMbsmHwSetPortIngressEtherType (UINT4 u4IfIndex, UINT2 u2EtherType,
                                     tMbsmSlotInfo * pSlotInfo);
INT4
FsVlanMbsmHwSetPortEgressEtherType (UINT4 u4IfIndex, UINT2 u2EtherType,
                                    tMbsmSlotInfo * pSlotInfo);

#endif

#ifndef BRIDGE_WANTED
INT1 FsBrgSetAgingTime PROTO ((INT4 i4AgingTime));
#ifdef MBSM_WANTED
INT1 FsBrgMbsmSetAgingTime PROTO ((INT4 ,tMbsmSlotInfo * ));
#endif /* BRIDGE_WANTED */
#endif /* MBSM_WANTED */

INT4
FsVlanHwSetVlanLoopbackStatus PROTO ((tVlanId VlanId, INT4 i4LoopbackStatus));
#endif
