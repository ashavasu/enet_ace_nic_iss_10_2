/**************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: erpsminp.h,v 1.1 2011/07/26 05:16:51 siva Exp $ 
 *
 * Description: All prototypes for Network Processor API functions for ERPS 
 *
 **************************************************************************/
#ifndef _ERPSMINP_H
#define _ERPSMINP_H

INT4 FsErpsHwRingConfig (tErpsHwRingInfo *pErpsHwRingInfo);

INT1
FsMiErpsNpCreateInstance (UINT4 u4ContextId, UINT2 u2InstId);
INT1
FsMiErpsNpDeleteInstance (UINT4 u4ContextId, UINT2 u2InstId);
INT1
FsMiErpsNpAddVlanInstMapping (UINT4 u4ContextId,tVlanId VlanId, UINT2 u2InstId);
INT1
FsMiErpsNpDelVlanInstMapping (UINT4 u4ContextId, tVlanId VlanId, UINT2 u2InstId);
INT1
FsMiErpsNpSetInstancePortState (UINT4 u4ContextId, UINT4 u4IfIndex,UINT2 u2InstanceId, UINT1 u1PortState);
INT1
FsMiErpsNpSetPortState (UINT4 u4ContextId, UINT4 u4IfIndex,UINT1 u1PortState);

#ifdef MBSM_WANTED
INT4 FsMiErpsMbsmHwRingConfig (tErpsHwRingInfo *pErpsHwRingInfo, 
                             tErpsMbsmInfo * pErpsMbsmInfo);
#endif
#endif /*_ERPSNP_H*/
