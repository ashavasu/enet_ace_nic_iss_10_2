/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vcmnp.h,v 1.10 2017/11/14 07:31:12 siva Exp $ 
 *
 * Description: All prototypes for Network Processor API functions for BRIDGE 
 * 
 *
 *******************************************************************/
#ifndef _VCMNP_H
#define _VCMNP_H

#include "mbsm.h"
#include "vcm.h"
#include "fsvlan.h"


typedef struct _FsNpVcmVrMapInfo
{
   UINT4 u4IfIndex;   /* CFA IfIndex */
   UINT4 u4VrfId;      /* Virtual Router Identifier*/
   UINT4 u4L2CxtId;   /* Virtual Switch Identifier*/
   tMacAddr MacAddr;  /* Mac address of the Virtual Router */
   UINT2 u2VlanId;     /* Vlan Id */
}tFsNpVcmVrMapInfo;

typedef enum VrMapAction
{
    NP_HW_CREATE_VIRTUAL_ROUTER = 1,    /* Action to create virtual router */
    NP_HW_DELETE_VIRTUAL_ROUTER,    /* Action to delete Virtual Router */
    NP_HW_MAP_INTF_TO_VR,    /* Map interface to Virtual Router */
    NP_HW_UNMAP_INTF_TO_VR,   /* Unmap interface from Virtual Router */
    NP_HW_MAP_RPORT_TO_VR,    /* Map Router port interface to Virtual Router */
    NP_HW_UNMAP_RPORT_TO_VR,   /* Unmap Router port interface from Virtual Router */
    NP_HW_MAP_INTF_TO_VIRTUAL_SW, /* Map interface to Virtual Switch */
    NP_HW_UNMAP_INTF_TO_VIRTUAL_SW, /* Unmap interface from Virtual Switch */
    NP_HW_MAP_INVALID /* INVALID action */
} tVrMapAction;

INT4 FsVcmHwCreateContext PROTO ((UINT4 u4ContextId));
INT4 FsVcmHwDeleteContext PROTO ((UINT4 u4ContextId));
INT4 FsVcmHwMapPortToContext PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex));
INT4 FsVcmHwUnMapPortFromContext PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex));
INT4 FsVcmHwMapIfIndexToBrgPort PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, tContextMapInfo ContextInfo));
INT4 FsVcmHwMapVirtualRouter PROTO ((tVrMapAction VrMapAction, 
                                        tFsNpVcmVrMapInfo  * pVcmVrMapInfo ));
INT4 FsVcmSispHwSetPortCtrlStatus PROTO ((UINT4 u4IfIndex, UINT1 u1Status));
INT4 FsVcmSispHwSetPortVlanMapping PROTO ((UINT4 u4IfIndex, tVlanId VlanId,
                                           UINT4 u4ContextId, UINT1 u1Status));

#ifdef MBSM_WANTED
INT4 FsVcmMbsmHwCreateContext PROTO ((UINT4, tMbsmSlotInfo *));
INT4 FsVcmMbsmHwMapPortToContext PROTO ((UINT4, UINT4, tMbsmSlotInfo *));
INT4 FsVcmMbsmHwMapIfIndexToBrgPort PROTO ((UINT4, UINT4, tContextMapInfo, 
                                            tMbsmSlotInfo *));
INT4 FsVcmMbsmHwMapVirtualRouter PROTO ((tVrMapAction VrMapAction, 
                                        tFsNpVcmVrMapInfo  * pVcmVrMapInfo, 
                                        tMbsmSlotInfo *pSlotInfo));
INT4 FsVcmSispMbsmHwSetPortVlanMapping PROTO ((UINT4 , tVlanId, UINT4, UINT1, 
                                             tMbsmSlotInfo * ));
INT4 FsVcmSispMbsmHwSetPortCtrlStatus  PROTO ((UINT4, UINT1,  tMbsmSlotInfo *));
#endif /* MBSM_WANTED */

INT4 FsVcmHwHandleVrfCounter PROTO (( tVrfCounter VrfCounter ));
    
#endif /*_VCMNP_H*/
