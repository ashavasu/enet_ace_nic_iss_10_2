/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: issnp.h,v 1.33 2017/01/27 12:27:27 siva Exp $ 
 *
 * Description: This file contains the function implementations  of FS NP-API.
 ****************************************************************************/
#ifndef _ISSNP_H
#define _ISSNP_H

INT4 IssDeInstallUserDefinedFilter PROTO((tIssUDBFilterEntry *));

INT4 IssHwSetPortEgressStatus     PROTO ((UINT4, UINT1));
INT4 IssHwSetPortStatsCollection  PROTO ((UINT4, UINT1));

INT4 IssHwSetPortMode             PROTO ((UINT4, INT4));
INT4 IssHwSetPortDuplex           PROTO ((UINT4, INT4));
INT4 IssHwSetPortSpeed            PROTO ((UINT4, INT4));
INT4 IssHwSetPortMdiOrMdixCap     PROTO ((UINT4, INT4));
INT4 IssHwSetPortFlowControl      PROTO ((UINT4, UINT1));
INT4 IssHwSetPortRenegotiate      PROTO ((UINT4, INT4));
INT4 IssHwSetPortMaxMacAddr       PROTO ((UINT4, INT4));
INT4 IssHwSetPortMaxMacAction     PROTO ((UINT4, INT4));

INT4 IssHwSetMirrorToPort         PROTO ((UINT4));
INT4 IssHwSetPortMirroringStatus  PROTO ((INT4));
INT4 IssHwSetIngressMirroring     PROTO ((UINT4, INT4));
INT4 IssHwSetEgressMirroring      PROTO ((UINT4, INT4));

INT4 IssHwInitMirrDataBase PROTO ((VOID));
INT4 IssHwSetMirroring PROTO((tIssHwMirrorInfo *));
INT4 IssHwMirrorAddRemovePort PROTO((UINT4 ,UINT4 ,UINT1 ,UINT1 ));
INT4 IssHwSetRateLimitingValue    PROTO ((UINT4, UINT1, INT4));
INT4 IssHwSetMacLearningRateLimit PROTO ((INT4));
INT4 IssHwGetLearnedMacAddrCount  PROTO ((INT4 *));

INT4 IssHwUpdateL2Filter          PROTO ((tIssL2FilterEntry *, INT4));
INT4 IssHwUpdateL3Filter          PROTO ((tIssL3FilterEntry *, INT4));
INT4 IssHwUpdateL4SFilter         PROTO ((tIssL4SFilterEntry *, INT4));
INT4 IssHwSetResrvFrameFilter PROTO ((tIssReservFrmCtrlTable *,INT4));

PUBLIC INT4 IssNpAddAllL2Cache PROTO ((VOID));
PUBLIC INT4 IssNpScanL2Cache PROTO ((INT4 *, tMacAddr ));
PUBLIC INT4 IssNpAddL2Cache PROTO ((tMacAddr));
PUBLIC INT4 IssNpDelAllIndivL2Cache PROTO ((VOID));
PUBLIC INT4 IssHwSetCpuMirroring PROTO ((INT4 , UINT4));


INT4 IssHwInitFilter              PROTO ((VOID));
VOID IssHwRestartSystem           PROTO ((VOID));

INT4 IssHwGetPortDuplex           (UINT4, INT4 *);
INT4 IssHwGetPortSpeed            (UINT4, INT4 *);
INT4 IssHwGetPortMdiOrMdixCap     (UINT4, INT4 *);
INT4 IssHwGetPortFlowControl      (UINT4, INT4 *);
INT4 IssHwGetPortEgressPktRate    (UINT4, INT4 *, INT4 *);
INT4 IssHwSetPortEgressPktRate    PROTO ((UINT4, INT4  , INT4  ));
INT4 IssHwGetCurrTemperature      PROTO ((INT4 *));
INT4 IssHwGetCurrPowerSupply      PROTO ((UINT4 *));
INT4 IssHwGetFanStatus            PROTO ((UINT4, UINT4 *));
INT4 IssHwGetPortAutoNegAdvtCapBits PROTO((UINT4, INT4 *));
INT4 IssHwSetPortAutoNegAdvtCapBits PROTO((UINT4 , INT4 *,INT4 *));
const char * IssHwGetHwVersion PROTO ((VOID));
VOID NpShowDebugging (VOID);
INT4 IssHwSetSetTrafficSeperationCtrl PROTO ((INT4));
VOID IssHwGetPortInfo  PROTO ((INT4, CHR1 *));


#define ISS_NP_COLDSTDBY_ENABLE    ISS_RM_RTYPE_COLD
extern UINT1 gu1ColdStandbyState;
extern UINT1 gu1NumofFPPorts;
extern UINT1 gu1NumofStkPorts;
extern UINT1 gu1StackPriority;
#ifdef MBSM_WANTED
INT4 IssMbsmHwUpdateL2Filter          (tIssL2FilterEntry *, INT4, tMbsmSlotInfo *);
INT4 IssMbsmHwUpdateL3Filter          (tIssL3FilterEntry *, INT4, tMbsmSlotInfo *);
INT4 IssMbsmHwSetPortEgressStatus     (UINT4, UINT1, tMbsmSlotInfo *);
INT4 IssMbsmHwSetPortStatsCollection  (UINT4, UINT1, tMbsmSlotInfo *);

INT4 IssMbsmHwSetMirrorToPort         (UINT4, tMbsmSlotInfo *);
INT4 IssMbsmHwSetPortMirroringStatus  (INT4, tMbsmSlotInfo *);
#endif

INT4 IssHwSetPortHOLBlockPrevention (UINT4 u4IfIndex, INT4 i4Value);
INT4 IssHwGetPortHOLBlockPrevention (UINT4 u4IfIndex, INT4 *pi4Value);
INT4 IssHwUpdateUserDefinedFilter (tIssUDBFilterEntry *pAccessFilterEntry,
                                   tIssL2FilterEntry           *pL2AccessFilterEntry,
                                   tIssL3FilterEntry           *pL3AccessFilterEntry,
                                   INT4                         i4FilterAction);
INT4 IssHwSetLearningMode (UINT4 u4IfIndex, INT4 i4Status);
INT4 IssHwSetPortFlowControlRate (UINT4 u4IfIndex, INT4 i4PortMaxRate,
                                  INT4 i4PortMinRate);
/* This NPAPI is applicable only for xcat */
UINT4 IssHwSetEgressFilterMode (INT4 i4EgressFilterMode);

UINT1 NpUtilAccessHwConsole PROTO ((UINT1 *pu1HwCmd));
VOID
IssNpHwGetCapabilities (tNpIssHwGetCapabilities * pHwGetCapabilities);
INT4 IssHwSetSwitchModeType (INT4 i4SwitchModeType);

#endif /* _ISSNP_H */
