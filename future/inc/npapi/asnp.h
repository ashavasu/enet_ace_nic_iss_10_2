/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: asnp.h,v 1.6 2011/04/08 13:22:09 siva Exp $
 *
 * Description: This file contains typedefs for input arguments and
 *               return values to callbacks for various Async NPAPIs
 * ******************************************************************/

#ifndef _ASNP_H
#define _ASNP_H

typedef enum {  
    AS_PNAC_HW_SET_AUTH_STATUS                       = 1,
    AS_PNAC_HW_ADD_OR_DEL_MAC_SESS                   = 2,
    AS_FS_LA_HW_CREATE_AGG_GROUP                     = 3,
    AS_FS_LA_HW_ADD_LINK_TO_AGG_GROUP                = 4,
    AS_FS_LA_HW_SET_SELECTION_POLICY                 = 5,
    AS_FS_LA_HW_REMOVE_LINK_FROM_AGG_GROUP           = 6,
    AS_FS_LA_HW_DELETE_AGGREGATOR                    = 7,
    AS_FS_LA_HW_ENABLE_COLLECTION                    = 8,
    AS_FS_LA_HW_ENABLE_DISTRIBUTION                  = 9,
    AS_FS_LA_HW_DISABLE_COLLECTION                   = 10,
    AS_FS_LA_HW_SET_PORT_CHANNEL_STATUS              = 11,
    AS_FS_MI_NP_UPDATE_IPMC_FWD_ENTRIES              = 12,
    AS_FS_MI_IGS_HW_UPDATE_IPMC_ENTRY                = 13,
    AS_FS_MI_RSTP_NP_SET_PORT_STATE                  = 14,
    AS_FS_MI_MSTP_NP_SET_INSTANCE_PORT_STATE         = 15,
    AS_FS_MI_PVRST_NP_SET_VLAN_PORT_STATE            = 16,
    AS_FS_ERPS_HW_RING_CONFIG                        = 17
}eNpCallId;

typedef struct {
    UINT2    u2Port;
    tMacAddr  SuppAddr;
    UINT1    u1AuthMode;
    UINT1    u1AuthStatus;
    UINT1    u1CtrlDir;
    UINT1    au1Pad[1];
    INT4     rval;
} tAsNpwPnacHwSetAuthStatus;

typedef struct {
    UINT2    u2Port;
    tMacAddr  SuppAddr;
    UINT1    u1SessStatus;
    UINT1    au1Pad[3];
    INT4     rval;
} tAsNpwPnacHwAddOrDelMacSess;

typedef struct {
    UINT2    u2AggIndex;
    UINT2    u2HwAggIndex;
    INT4     rval;
} tAsNpwFsLaHwCreateAggGroup;

typedef struct {
    UINT2    u2AggIndex;
    UINT2    u2PortNumber;
    UINT2    u2HwAggIndex;
    UINT2    u2Pad;
    INT4     rval;
} tAsNpwFsLaHwAddLinkToAggGroup;

typedef struct {
    UINT2  u2AggIndex;
    UINT1  u1SelectionPolicy;
    UINT1  u1Pad;
    INT4   rval;
} tAsNpwFsLaHwSetSelectionPolicy;

typedef struct {
    UINT2  u2AggIndex;
    UINT2  u2PortNumber;
    INT4   rval;
} tAsNpwFsLaHwRemoveLinkFromAggGroup;

typedef struct {
    UINT2  u2AggIndex;
    UINT2  u2Pad;
    INT4   rval;
} tAsNpwFsLaHwDeleteAggregator;

typedef struct {
    UINT2  u2AggIndex;
    UINT2  u2PortNumber;
    INT4   rval;
} tAsNpwFsLaHwEnableCollection;

typedef struct {
    UINT2  u2AggIndex;
    UINT2  u2PortNumber;
    INT4   rval;
} tAsNpwFsLaHwEnableDistribution;

typedef struct {
    UINT2  u2AggIndex;
    UINT2  u2PortNumber;
    INT4   rval;
} tAsNpwFsLaHwDisableCollection;

typedef struct {
    UINT2  u2AggIndex;
    UINT2  u2Inst;
    UINT1  u1StpState;
    UINT1  au1Pad[3];
    INT4   rval;
} tAsNpwFsLaHwSetPortChannelStatus;

typedef struct {
    UINT4         u4ContextId;
    UINT4         u4GrpAddr;
    UINT4         u4SrcAddr;
    tPortList     PortList;
    tPortList     UntagPortList;
    tSnoopVlanId  VlanId;
    UINT1         u1EventType;
    UINT1         u1Pad;
    INT4          rval;
} tAsNpwFsMiNpUpdateIpmcFwdEntries;

typedef struct {
    UINT4              u4Instance;
    tIgsHwIpFwdInfo    IgsHwIpFwdInfo;
    UINT1              u1Action;
    UINT1              au1Pad[3];
    INT4               rval;
} tAsNpwFsMiIgsHwUpdateIpmcEntry;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
    UINT1  u1Status;
    INT1   rval;
    UINT1  au1Pad[2];
} tAsNpwFsMiRstpNpSetPortState;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
    UINT2  u2InstanceId;
    UINT1  u1PortState;
    UINT1  u1Pad;
    INT4   rval;
} tAsNpwFsMiMstpNpSetInstancePortState;

typedef struct {
    UINT4    u4ContextId;
    UINT4    u4IfIndex;
    tVlanId  VlanId;
    UINT1    u1PortState;
    UINT1    u1Pad;
    INT4     rval;
} tAsNpwFsMiPvrstNpSetVlanPortState;

typedef struct {
    tErpsHwRingInfo    ErpsHwRingInfo;
    INT4               rval;
} tAsNpwFsErpsHwRingConfig;

typedef union {
    tAsNpwPnacHwSetAuthStatus PnacHwSetAuthStatus;
    tAsNpwPnacHwAddOrDelMacSess PnacHwAddOrDelMacSess;
    tAsNpwFsLaHwCreateAggGroup FsLaHwCreateAggGroup;
    tAsNpwFsLaHwAddLinkToAggGroup FsLaHwAddLinkToAggGroup;
    tAsNpwFsLaHwSetSelectionPolicy FsLaHwSetSelectionPolicy;
    tAsNpwFsLaHwRemoveLinkFromAggGroup FsLaHwRemoveLinkFromAggGroup;
    tAsNpwFsLaHwDeleteAggregator FsLaHwDeleteAggregator;
    tAsNpwFsLaHwEnableCollection FsLaHwEnableCollection;
    tAsNpwFsLaHwEnableDistribution FsLaHwEnableDistribution;
    tAsNpwFsLaHwDisableCollection FsLaHwDisableCollection;
    tAsNpwFsLaHwSetPortChannelStatus FsLaHwSetPortChannelStatus;
    tAsNpwFsMiNpUpdateIpmcFwdEntries FsMiNpUpdateIpmcFwdEntries;
    tAsNpwFsMiIgsHwUpdateIpmcEntry FsMiIgsHwUpdateIpmcEntry;
    tAsNpwFsMiRstpNpSetPortState FsMiRstpNpSetPortState;
    tAsNpwFsMiMstpNpSetInstancePortState FsMiMstpNpSetInstancePortState;
    tAsNpwFsMiPvrstNpSetVlanPortState FsMiPvrstNpSetVlanPortState;
    tAsNpwFsErpsHwRingConfig FsErpsHwRingConfig;
}unAsyncNpapi;
#endif /*_ASNP_H */
