/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pbtnp.h,v 1.6 2011/11/14 12:07:26 siva Exp $
 *
 * Description: This file contains the function prototypes of PBB NPAPI.
 ****************************************************************************/
#ifndef _PBTNP_H
#define _PBTNP_H
/* Hardware API function prototypes */
#ifdef NPAPI_WANTED
#ifdef MBSM_WANTED

INT4
FsMiPbbTeMbsmHwSetEspVid (UINT4 u4ContextId, tVlanId EspVlanId, 
                        tMbsmSlotInfo *pSlotInfo);
#endif
#endif
INT4
FsMiPbbTeHwResetEspVid (UINT4 u4ContextId, tVlanId EspVlanId);

INT4
FsMiPbbTeHwSetEspVid (UINT4 u4ContextId, tVlanId EspVlanId);

#endif
