/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mrpnp.h,v 1.3 2009/09/08 05:58:50 prabuc Exp $ 
 *
 * Description: This file contains the function prototypes of MRP NPAPI.
 ****************************************************************************/
#ifndef _MRPNP_H
#define _MRPNP_H
/* Hardware API function prototypes */
INT4
FsMiMrpHwSetMvrpStatus (tMrpHwInfo *pMrpHwInfo);
INT4
FsMiMrpHwSetMmrpStatus (tMrpHwInfo *pMrpHwInfo);

#endif /* _MRPNP_H */
