/*****************************************************************************
  Aricent Inc . All Rights Reserved
*
* $Id: evcnp.h,v 1.1 2014/10/10 12:07:26 siva Exp $
*
* Description: This file contains structure for EVC information.
****************************************************************************/
#ifndef __EVCNP_H
#define __EVCNP_H

typedef struct EvcInfo
{
tRBNodeEmbd  IssEvcInfoRbNode;
UINT4        u4EvcId;
UINT2        SVlanId;
BOOLEAN      bCeVlanIdPreservation;
BOOLEAN      bCeVlanCoSPreservation;
}tEvcInfo;

typedef struct HwEvcInfo
{
UINT4        u4EvcId;
UINT2        SVlanId;
BOOLEAN      bCeVlanIdPreservation;
BOOLEAN      bCeVlanCoSPreservation;
}tHwEvcInfo;

#endif /*__EVCNP_H*/


