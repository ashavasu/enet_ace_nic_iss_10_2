/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igsnp.h,v 1.9 2010/05/19 07:12:30 prabuc Exp $
 *
 * Description: Prototypes for IGMP Snooping module's NP-API.
 *
 *******************************************************************/
#ifndef _IGSNP_H
#define _IGSNP_H

INT4 FsIgsHwEnableIgmpSnooping PROTO ((VOID));
INT4 FsIgsHwDisableIgmpSnooping PROTO ((VOID));

INT4
FsNpUpdateMcFwdEntries PROTO ((tSnoopVlanId VlanId, UINT4 u4GrpAddr, 
                        UINT4 u4SrcAddr, UINT1 *pu1PortList, 
                        UINT1 u1EventType));

INT4 FsNpGetFwdEntryHitBitStatus PROTO ((UINT2, UINT4, UINT4));

INT4 FsIgsHwAddRtrPort PROTO ((tSnoopVlanId, UINT2));
INT4 FsIgsHwDelRtrPort PROTO ((tSnoopVlanId, UINT2));

INT4 FsIgsHwEnableIpIgmpSnooping PROTO ((VOID));
INT4 FsIgsHwDisableIpIgmpSnooping PROTO ((VOID));
INT4 FsIgmpHwEnableIgmp PROTO ((VOID));
INT4 FsIgmpHwDisableIgmp PROTO ((VOID));

INT4 FsNpUpdateIpmcFwdEntries (tSnoopVlanId, UINT4, UINT4, tPortList, tPortList, UINT1);
VOID FsIgsHwClearIpmcFwdEntries PROTO ((VOID));

INT4 FsIgsHwUpdateIpmcEntry PROTO ((tIgsHwIpFwdInfo *, UINT1));
INT4 FsIgsHwGetIpFwdEntryHitBitStatus PROTO ((tIgsHwIpFwdInfo *));
INT4 FsIgsHwEnhancedMode PROTO ((UINT1));
INT4 FsIgsHwSparseMode PROTO ((UINT1));
INT4
FsIgsHwPortRateLimit PROTO ((tIgsHwRateLmt * pIgsHwRateLmt, UINT1 u1Action));
#ifdef L2RED_WANTED
INT4
FsNpGetIpMcastFwdEntry (tSnoopVlanId VlanId, UINT4 u4GrpAddr, UINT4 u4SrcAddr,
                        UINT1 *pu1PortList);
INT4
FsNpGetFirstIpMcastFwdEntry (INT4 *pi4VlanId, UINT4 *pu4McastGrpIpAddr,
                             UINT4 *pu4McastSrcIpAddr);

INT4
FsNpGetNextIpMcastFwdEntry (INT4 i4VlanId, INT4 *pi4VlanId,
                            UINT4 u4McastGrpIpAddr, UINT4 *pu4McastGrpIpAddr,
                            UINT4 u4McastSrcIpAddr, UINT4 *pu4McastSrcIpAddr);
#endif

#ifdef MBSM_WANTED
INT4 FsIgsMbsmHwEnableIgmpSnooping PROTO ((tMbsmSlotInfo * pSlotInfo));
INT4 FsIgsMbsmHwEnableIpIgmpSnooping PROTO ((tMbsmSlotInfo * pSlotInfo));

INT4 FsIgmpMbsmHwEnableIgmp PROTO ((tMbsmSlotInfo * pSlotInfo));

INT4 FsMbsmNpUpdateIpmcFwdEntries PROTO ((tSnoopVlanId VlanId, UINT4 u4GrpAddr, 
                                 UINT4 u4SrcAddr, tPortList PortList,
                                 tPortList UntagPortList,
                                 tMbsmSlotInfo * pSlotInfo));
INT4 FsIgsMbsmHwEnhancedMode PROTO ((UINT1, tMbsmSlotInfo *));
#endif

#endif

