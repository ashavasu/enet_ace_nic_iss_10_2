/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rednp.h,v 1.11 2015/03/05 12:02:47 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              by external modules.
 *
 *******************************************************************/
#ifndef  _REDNP_H
#define  _REDNP_H

/* Sync up Message types */
typedef enum {
   NP_RED_BULK_REQ_MSG = 1,
   NP_RED_BULK_UPD_TAIL_MSG,
   NP_RED_BULK_UPDATES,
   NP_RED_EOAM_FILTER_SYNC,
   NP_RED_PNAC_FILTER_SYNC,
   NP_RED_IGS_FILTER_SYNC,
   NP_RED_IGMP_NP_INIT_SYNC,
   NP_RED_IGS_FLT_CNT_SYNC,
   NP_RED_PIM_FILTER_SYNC,
   NP_RED_PIM_FLT_CNT_SYNC,
   NP_RED_OSPF_FILTER_SYNC,
   NP_RED_OSPF_FLT_CNT_SYNC,
   NP_RED_VLAN_TNL_FILTER_SYNC,
   NP_RED_ACL_FILTER_SYNC,
   NP_RED_ACL_CHAIN_INDEX_SYNC,
   NP_RED_IP_SYNC,
   NP_RED_VLAN_TNL_FILTER_ADDR_SYNC,
   NP_RED_VLAN_BLK_TNL_FILTER_ADDR_SYNC,
   NP_RED_ECFM_FILTER_SYNC,
   NP_RED_CFA_PORT_STATUS_SYNC,
   NP_RED_CFA_BLK_PORT_STATUS_SYNC,
   NP_RED_IPMC_FLT_CNT_SYNC,
   NP_RED_KNET_INTF_SYNC,
   NP_RED_KNET_FILTER_SYNC
}tNpRedRmMsgTypes;

INT4 NpRedInit (VOID);
INT4 NpRedDeInit (VOID);
VOID NpRedTaskMain (INT1 *pi1Param);
#endif /* _REDNP_H */
