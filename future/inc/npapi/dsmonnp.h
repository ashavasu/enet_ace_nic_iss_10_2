/*****************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: dsmonnp.h,v 1.5 2009/12/19 10:42:56 prabuc Exp $
 *
 * Description: This file contains the function implementations  of
 *              DSMON NP-API.
 ****************************************************************************/

#ifndef _DSMONNP_H
#define _DSMONNP_H

INT4 FsDSMONHwSetStatus PROTO ((UINT4));
UINT4 FsDSMONHwGetStatus PROTO ((VOID));
INT4 FsDSMONEnableProbe PROTO ((UINT4, UINT2, tPortList));
INT4 FsDSMONDisableProbe PROTO ((UINT4, UINT2, tPortList));
INT4 FsDSMONAddFlowStats PROTO ((tPktHeader));

INT4 FsDSMONRemoveFlowStats PROTO ((tPktHeader));


INT4 FsDSMONCollectStats PROTO ((tPktHeader,
                          tRmon2Stats*));

#endif /* _DSMONNP_H */

