/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/*                                                                           */
/*  FILE NAME             : rstmpbnp.h                                       */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                  */
/*  SUBSYSTEM NAME        : RSTP Module                                      */
/*  MODULE NAME           : RSTP                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 07 DEC 2006                                      */
/*  AUTHOR                : Aricent Inc.                                  */
/*  DESCRIPTION           : This file contains RSTP NP   Function Prototypes */
/*                                                                           */
/*****************************************************************************/


INT1
FsMiPbRstpNpSetPortState (UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId SVlanId, 
                          UINT1 u1Status);
INT1
FsMiPbRstNpGetPortState (UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId SVlanId,
                         UINT1 *pu1Status);

#ifdef MBSM_WANTED
INT1
FsMiPbRstpMbsmNpSetPortState (UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId Svid,
                              UINT1 u1Status, tMbsmSlotInfo * pSlotInfo);
#endif
