/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rportnp.h,v 1.10 2015/10/29 09:35:28 siva Exp $
 *
 * Description: Exported file for routerport NP-API.
 *******************************************************************/
#ifndef _RPORTNP_H
#define _RPORTNP_H
#include "npapi.h"
#include "cfa.h"

#define MAC_LEARNING_ENABLE 1
#define MAC_LEARNING_DISABLE 2
 
typedef enum L3Action
{
    L3_HW_CREATE_IF_INFO =1,
    L3_HW_MODIFY_IF_INFO,
    L3_HW_DELETE_IF_INFO
}tL3Action;


typedef struct _FsNpL3IfInfo
{
        UINT4 u4VrId;      /* Virtual Route Identifier*/
        UINT4 u4IfIndex;   /* CFA IfIndex */
        UINT4 u4IpAddr;    /* Ip Address of this L3 interface */
        UINT4 u4IpSubnet;  /* Subnet of this L3 interface */
        UINT1 au1IfName[CFA_MAX_PORT_NAME_LENGTH];  /* Interface Name*/
        UINT1 au1MacAddr[MAC_ADDR_LEN]; /*Interface Mac */
        UINT2 u2PortVlanId; /* Holds the VLAN Identifier used for Routerport
    Implementation */
 UINT2 u2ErrCode; /* Error code set by NP */
        UINT1 au1Dummy[2];
}tFsNpL3IfInfo;


UINT4  FsNpL3Ipv4ArpAdd (UINT4 u4IfIndex, UINT4 u4IpAddr,
                         UINT1 *pMacAddr, UINT1 *pu1IfName, INT1 i1State,
                         UINT1 *pbu1TblFull);

UINT4  FsNpL3Ipv4ArpModify (UINT4 u4IfIndex, UINT4 u4IpAddr,
                            UINT1 *pMacAddr, UINT1 *pu1IfName, INT1 i1State);
UINT4  FsNpL3Ipv4VrfArpAdd (UINT4  u4VrId, UINT4 u4IfIndex, UINT4 u4IpAddr,
                         UINT1 *pMacAddr, UINT1 *pu1IfName, INT1 i1State,
                         UINT1 *pbu1TblFull);

UINT4  FsNpL3Ipv4VrfArpModify (UINT4  u4VrId, UINT4 u4IfIndex, UINT4 u4IpAddr,
                            UINT1 *pMacAddr, UINT1 *pu1IfName, INT1 i1State);

UINT4 FsNpIpv4L3IpInterface(tL3Action L3Action,
                                        tFsNpL3IfInfo *pFsNpL3IfInfo);
UINT4 FsNpIpv4GetEcmpGroups (UINT4 *pu4EcmpGroups);

UINT4 NpGetRportIfIndex(UINT4 u4VlanId);

void NpSetMacLearningStatus(UINT2 u2VlanId, UINT4 u4IfIndex, UINT1 u1Status);

UINT4 NpL3Ipv4ArpAdd (UINT4 u4VrfId,UINT4 u4IfIndex, UINT4 u4IpAddr,
                  UINT1 *pMacAddr, UINT1 *pu1IfName,
                  INT1 i1State, UINT1 *pbu1TblFull);

UINT4 NpL3Ipv4ArpModify (UINT4 u4VrfId,UINT4 u4IfIndex, UINT4 u4IpAddr,
                     UINT1 *pMacAddr, UINT1 *pu1IfName, INT1 i1State);

#endif
