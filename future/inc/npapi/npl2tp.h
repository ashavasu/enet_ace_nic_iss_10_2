#ifndef _L2TPNP_H_
#define _L2TPNP_H_
#include "npapi.h"

#define L2TP_MAX_FILTERS 100
#define L2TP_NP_ZERO      0
/* L2TP return Codes */
#define  L2TP_NP_SUCCESS                 0
#define  L2TP_NP_FAILURE                 1

typedef struct
{
    UINT4    u4IfIndex; /* Interface Index of the port */
    UINT4    u4CVlan;   /*Customer VLAN Id */
    UINT4    u4SVlan;   /*Service VLAN Id */
    UINT4    u4XConnectId;  /*XConnect ID for Filter Id manipulation*/
    tMacAddr MacAddr;   /* Mac Address of the given port*/
    UINT1    u1Opcode;  /* This can take  CFA_SET_TO_WAN/CFA_RESET_TO_LAN.
                           Based on this value the interface will be configured
                           either as WAN or LAN  */
    UINT1    u1EncapType; /*Encaptype of the port*/

    UINT1    au1Pad[1]; /* Padding bytes */
}tL2tpFilterInfo;


enum {
       L2TP_NP_ENABLE = 1,
       L2TP_NP_DISABLE = 2
     };

/*Filter Id storing structure based on the attachment circuit*/
typedef struct
{
    UINT4    u4XConnectId; /*XConnect Id*/
    UINT4    u4FilterNo; /*Filter Id */
}tL2tpGlobalFilterInfo;

enum {

    NP_PORT_ENCAPSULATION_TYPE_PORT =1,
    NP_PORT_ENCAPSULATION_TYPE_PORT_VLAN,
    NP_PORT_ENCAPSULATION_TYPE_QINQ,
    NP_PORT_ENCAPSULATION_TYPE_QINANY
};


INT4 
FsHwL2tpFilterCreate PROTO ((tL2tpFilterInfo *));
INT4
L2tpHandleFilterIdFromXConnectId PROTO((UINT4 u4XConnectId,UINT1 u1Opcode,INT4 i4L2tpFilterNo));
VOID
L2tpCalcFilterIdFromXConnectId PROTO((UINT4 u4XConnectId,INT4 *pi4L2tpFilterNo));
INT4
L2tpFetchFilterIdFromXConnectId PROTO((UINT4 u4XConnectId,INT4 *pi4L2tpFilterNo));

#endif
