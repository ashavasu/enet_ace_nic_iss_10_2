/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tcp.h,v 1.44 2017/09/13 13:34:07 siva Exp $
 *
 * Description:This file contains the exported definitions and
 *             macros of TCP
 *
 *******************************************************************/
#ifndef _TCP_H
#define _TCP_H

/* DATA STRUCTURES */
/* --------------- */
#ifndef IP6ADDRESS
#define IP6ADDRESS
typedef struct Ip6Address
{
  union
  {
    UINT1  u1ByteAddr[16];
    UINT2  u2ShortAddr[8];
    UINT4  u4WordAddr[4];
  } ip6_addr_u;

#define  u4_addr  ip6_addr_u.u4WordAddr  
#define  u2_addr  ip6_addr_u.u2ShortAddr 
#define  u1_addr  ip6_addr_u.u1ByteAddr  

} tIpAddr;
#endif

typedef struct {               /* Command Code = SEND_CMD              */
    UINT1  *pBuf;
    UINT4   u4BufLen;
} tRetsend;

typedef struct {               /* Command Code = RECEIVE_CMD           */
    UINT1  *pBuf;
    UINT2  u2BufLen;
    UINT1  u1Flag;             /* urgent/push/normal                   */
    UINT1  padding;
} tRetreceive;

typedef struct {               /* Command Code = STATUS_CMD            */
    tIpAddr  LocalIp;
    tIpAddr  RemoteIp;
    UINT4    u4ParentConnId;       /* Parent connection ID */
    UINT4    u4RcvWin;             /* Local window size of the connection */
    UINT4    u4SendWin;            /* Window size advertised by the remote */
    UINT2    u2LocalPort;          /* Local socket (IP address, Port No.) */
    UINT2    u2RemotePort;         /* Foreign socket (IP address, Port No) */
    UINT2    u2BufWaitAck;         /* # bytes sent, not yet acknowledged */
    UINT2    u2BufPendingReceipt;  /* # bytes in rcv buf to be consumed */
    UINT2    u2Security;           /* Security value, 0 if not specified */
    UINT2    u2TxmtTmout;          /* Retransmission Timeout , in secs */
    UINT1    u1State;              /* Current FSM state of the connection */
    UINT1    u1Urg;                /* 1 if Urgent data present, 0 if not */
    UINT1    u1Tos;                /* Type Of Service field used in IP */
    UINT1    padding;
} tRetstatus;

typedef struct {               /* Command Code = ERROR_MSG             */
    UINT1  u1ErrorCmd;         /* Set to command that caused the error */
    UINT1  u1Error;            /* Error code corresponding to UNIX     */
    INT1   i1ErrorCode;        /* Error code corresponding to TCP      */
    INT1  padding;
} tReterror;

typedef struct {               /* Command Code = CONN_REPORT           */
    UINT1  u1Message;          /* TCPE_ , URGENT_DATA                  */
    UINT1  u1IcmpType;         /* Type and the code fields             */
    UINT1  u1IcmpCode;         /* if u1Message = ICMP_MSG_ARRIVED      */
    UINT1  padding;
} tMessage;

typedef struct {               /* Command Code = NEW_CONN              */
    tIpAddr  LocalIp;
    tIpAddr  RemoteIp;
    UINT4    u4NewConn;
    UINT4    u4IfIndex;
    UINT2    u2LocalPort;      /* Local socket                         */
    UINT2    u2RemotePort;     /* Foreign socket                       */
} tNewConn;

typedef struct {               /* Command Code = OPTIONS_CMD           */
    UINT4  u4Value;
    UINT4  u4Option;
    UINT2  u2Reserved;
    UINT1  u1Optlen;
    UINT1  padding1;
} tRetoption;

typedef struct {
   union {
    tRetsend     send;
    tRetreceive  receive;
    tRetstatus   status;
    tReterror    error;
    tMessage     message;
    tNewConn     conn;
    tRetoption   option;
   } cmdtype;
    UINT4  u4ConnId;             /* The connection Identifier          */
    UINT2  u2Reserved;
    UINT1  u1Cmd;                /* Specifies the type of command      */
    UINT1  u1padding;
} tTcpToApplicationParms;

typedef struct {                 /* Command Code = REG_ASYNC_HANDLER   */
   UINT4 u4ConnId;
   VOID  (*fpTcpHandleAsyncMesg)(INT4, INT1 *);
} tRegAsyncHandler;

#define TCPAO_MASTER_KEY_MAXLEN   (80)
#define TCPAO_TRAFFIC_KEY_MAXLEN  (20)
/* 
 * 4 byte SNE , IPv6 pseudo header 40, tcp header 60
 * bgp max message size 4096 -> 4200
 */
#define TCP_AO_MAC_BUF_LEN        (4200)

/* Context max len 44 + i(1) + label (6) + length (2) - > 53 */
#define TCP_AO_TRFC_BUF_LEN       (53)

#define TCPAO_MKT_INI_IND         (3)
#define TCPAO_MKT_MAX_IND         (TCPAO_MKT_INI_IND)
#ifndef  TCP_MD5SIG_MAXKEYLEN
#define  TCP_MD5SIG_MAXKEYLEN  80
#endif
typedef struct TcpAOAuthMktTcb{
    UINT1       au1TcpAOMasterKey[TCPAO_MASTER_KEY_MAXLEN];
    UINT1       au1TcpTrafficKeySyn[TCPAO_TRAFFIC_KEY_MAXLEN];
    UINT1       au1TrafficKeyReceive[TCPAO_TRAFFIC_KEY_MAXLEN];
    UINT1       au1TrafficKeySend[TCPAO_TRAFFIC_KEY_MAXLEN];
    UINT1       u1SendKeyId;
    UINT1       u1ReceiveKeyId;
    UINT1       u1KDFAlgo;
    UINT1       u1MACAlgo;
    UINT1     u1TcpAOPasswdLength; 
    UINT1       u1TcpOptionIgnore;   
    UINT2       u2Padding;
} tTcpAOAuthMktTcb;
#ifndef SLI_TCPAO_MKTLIST
#define SLI_TCPAO_MKTLIST
typedef struct
{
    UINT1 u1SendKeyId;
    UINT1 u1RcvKeyId;
    UINT1 u1ShaAlgo;
    UINT1 u1TcpOptIgnore;
    UINT1 au1Key[TCP_MD5SIG_MAXKEYLEN];
}tSliTcpAoMkt;
#endif
typedef struct {                  /* Command Code = OPEN_CMD           */
    tIpAddr  LocalIp;
    tIpAddr  RemoteIp;
    UINT4    u4Timeout;
    UINT4    u4ValidOptions;
    UINT4    u4IfIndex;
    INT4     i4MaxPendingMsgs;
    UINT1    *pOptions;
    UINT1    *pMd5Key;
    UINT2    u2LocalPort;
    UINT2    u2RemotePort;
    UINT2    u2Security;
    UINT2    u2Reserved;
    UINT1    u1Flag;               /* Active or Passive                */
    UINT1    u1Tos;
    UINT1    u1Optlen;
    UINT1    u1Md5Keylen;
    tSliTcpAoMkt  *pTcpAoMkt;
    UINT1    u1TcpAoNoMktDisc;
    UINT1    u1TcpAoIcmpAcc;
    UINT2    u2Padding;

} tOpen;

typedef struct {                   /* Command Code = CLOSE_CMD         */
    UINT4  u4ConnId;
} tClose;

typedef struct {                   /* Command Code = SEND_CMD          */
    UINT4  u4ConnId;
    UINT4  u4IfIndex;
    UINT4  u4Timeout;
    UINT1  *pBuf;
    UINT2  u2BufLen;
    UINT1  u1Flag;                 /* Urgent/Push                      */
    UINT1  u1Padding;
} tSend;

typedef struct {                  /* Command Code = RECEIVE_CMD        */
    UINT1  *pBuf;
    UINT4  u4ConnId;
    UINT2  u2BufLen;
    UINT2  padding;
} tReceive;

typedef struct {                  /* Command Code = STATUS_CMD         */
    UINT4  u4ConnId;
} tStatus;

typedef struct {                  /* Command Code = ABORT_CMD          */
    UINT4  u4ConnId;
} tAbort;

typedef struct {                  /* Command Code = OPTIONS_CMD        */
    UINT4    u4ConnId;
    UINT4    u4Option;
    UINT1    u1Flag;                /* Get or Set                        */
    UINT1    u1Optlen;
    UINT1    u1Ttl;
    UINT1    u1padding;
    AR_UINT8 u8Value;
} tOption;

typedef struct {
   union {
    tRegAsyncHandler  RegAsyncHandler;
    tOpen             open;
    tClose            close;
    tSend             send;
    tReceive          receive;
    tAbort            abort;
    tStatus           status;
    tOption           option;
   } cmdtype;
    UINT4    u4ContextId;
    INT4   i4SockId;
    UINT2  u2Reserved; 
    UINT1  u1Cmd;
    UINT1  Padding1;
} tApplicationToTcpParms;

typedef struct FRAG {
    INT4  i4Seq;
    struct FRAG *pNext;
    UINT2  u2Len;
    UINT2  padding;
} tFrag;

typedef tFrag tSack;

typedef struct {
    tTmrAppTimer  node;
    UINT4         u4TcbIndex;
    UINT2         u2Reserved; 
    UINT1         u1Active;   /* Indicates whether the timer is active or not */
    UINT1         Padding1;
} tTcpTimer;

typedef struct RXMT {
    UINT4  u4StartIndex;
    UINT4  u4EndIndex;
    INT4   i4Seq;
    struct  RXMT  *pNext;
    UINT2  u2Reserved; 
    UINT1  u1SackFlag;
    UINT1  u1Padding;
} tRxmt;

typedef struct TCPLINGERPARMS {
    UINT2  u2LingerOnOff;
    UINT2  u2LingerTimer;
} tTcpLingerParms;

/* Macro for TCP MD5 Key Size */
#ifndef  TCP_MD5SIG_MAXKEYLEN
#define  TCP_MD5SIG_MAXKEYLEN  80
#endif

typedef struct TCB {
    tIpAddr  TcbRemoteIp;
    tIpAddr  TcbLocalIp;
    UINT4    u4TcbSwindow;      /* SND.WND rcvr's current window size  */
    UINT4    u4TcbCwnd;         /* congestion window size              */
    UINT4    u4TcbSsthresh;     /* slow start threshold                */
    UINT4    u4TcbTSRecent;     /* TS.Recent. refer 1323               */
    UINT4    u4TcbSrt;          /* smooth round trip time              */
    UINT4    u4TcbRtde;         /* round trip deviation estimator      */
    UINT4    u4TcbPersist;      /* persist timeout value               */
    UINT4    u4TcbRexmt;        /* retransmit timeout value            */
    UINT4    u4TcbSbstart;      /* start of valid data                 */
    UINT4    u4TcbSbcount;      /* send buffer count to be sent        */
    UINT4    u4TcbSbsize;       /* send buffer size                    */
    UINT4    u4TcbRbstart;      /* start of valid data                 */
    UINT4    u4TcbRbcount;      /* receive buffer count                */
    UINT4    u4TcbRbsize;       /* receive buffer size                 */
    UINT4    u4TcbValidOptions; /* Options that are valid              */
    UINT4    u4TcbTimeout;      /* Timeout, optional                   */
    UINT4    u4KaMainTmOut;     /* Main keepalive timeout              */
    tOsixSemId BlockSemId; /* Read block sema4 Id                 */
    tOsixSemId MainSemId;  /* connection Main sema4 Id            */
    UINT4    u4IfIndex;         /* The Interface index    */
    UINT4    u4MaxSwnd;         /* max send window, so for in this connection 
                                   MAX.SND.WND */
    UINT4    u4InMd5Errs;       /* Count of incoming segments dropped due to
                                   MD5 authentication failure */
    UINT4    u4Context;         /* Context associated with this TCB */
    UINT1    pTcbIpOptions[64]; /* IP options, optional                */
    UINT1 au1TcbMd5Key[TCP_MD5SIG_MAXKEYLEN]; /* MD5 key for this connection */
    INT4  i4TcbSuna;            /* SND.UNA lowest unACKed byte in sequence */
    INT4  i4TcbSnext;           /* SND.NXT next sequence num to send       */
    INT4  i4TcbFinSnext;           /* SND.NXT next sequence num to send with 
                                      appliction close is triggered       */
    INT4  i4TcbSupseq;          /* SND.UP send urgent pointer sequence     */
    INT4  i4TcbLwseq;           /* SND.WL1 last window sequence            */
    INT4  i4TcbLwack;           /* SND.WL2 last window acknowledgement     */
    INT4  i4TcbIss;             /* ISS initial send sequence               */
    INT4  i4TcbSlast;           /* Sequence of the FIN sent                */
    INT4  i4TcbTSLastAckSent;   /* Last.Ack.sent. refer 1323                */
    INT4  i4TcbOutOrderSeq;     /* last segment that triggered NAK generation */
    INT4  i4TcbSackLargestSeq;  /* largest SACKed sequence number            */
    INT4  i4TcbRnext;           /* RCV.NXT next sequence to receive          */
    INT4  i4TcbRupseq;          /* RCV.UP receive urgent pointer sequence    */
    INT4  i4SockDesc;           /* socket identifier of this conenction      */
    INT4  i4TcbCwin;            /* sequence of last advertised window      */
    INT4  i4TcbFinseq;          /* FIN sequence number    */
    INT4  i4TcbPushseq;         /* PUSH sequence number   */
    INT4  i4RemoteAddrType;     /* Remote address type                */
    INT4  i4LocalAddrType;     /* Local address type                 */

    VOID       (*fpTcpHandleAsyncMesg)(INT4, INT1 *);
    tSack      *pTcbSackHdr;  /* pointer to SACK SLL                    */
    struct TCB *pTcbPptcb;  /* ptr to parent TCB                      */
    UINT1      *pTcbSndbuf;   /* send buffer                            */
    UINT1      *pTcbRcvbuf;   /* receive buffer                         */
    tFrag      *pTcbFraghdr;  /* Initialise to NULL                     */
    tRxmt      *pTcbRxmtQHdr; /* pointer to RxmtQ to aid retransmission */
    tTcpToApplicationParms  *pParms;      /* Ptr to Return Params.      */

    tTcpTimer  TcbPersistTmr;      /* persist timer                     */
    tTcpTimer  TcbRexmtTmr;        /* retransmit timer                  */
    tTcpTimer  Tcb2mslTmr;         /* twice max. seg. life timer        */
    tTcpTimer  TcbUserTimeout;     /* close if user doesn't close       */
    tTcpTimer  TcbKaTimer;         /* keepalive timer                   */
    tTcpTimer  TcbLingerTimer;     /* So Linger timer                   */
    tTcpTimer  TcbFinWait2RecTimer;   /* FIN_WAIT2 state recovery timer */
    tTcpLingerParms  TcbLingerParms;

    UINT2  u2TcbRemotePort;   /* remote port                       */
    UINT2  u2TcbLocalPort;    /* local port addr                   */
    UINT2  u2MaxMsgCnt;       /* The max no of outstanding messages */
    UINT2  u2TcbSmss;         /* send max segment size              */
    UINT2  u2TcbFlags;        /* various flags                      */
    UINT2  u2TcbOptionsFlag;  /* flags that denote enabled options  */
    UINT2  u2TcbRmss;         /* receive max segment size           */
    UINT2  u2TcbSecurity;     /* security/compartment, optional     */
    UINT2  u2KaRetransTmOut;  /* Keepalive retransmission timeout   */
    UINT1  u1TcbMd5Keylen;    /* TCP MD5 Key Length */
    UINT1  u1TcbState;         /* TCP state                         */
    UINT1  u1TcbOutState;      /* Output state                      */
    UINT1  u1TcbType;          /* SERVER or CLIENT                  */
    UINT1  u1TcbCode;          /* TCP code for next pkt             */
    UINT1  u1TcbRexmtcount;    /* number of rexmt send              */
    INT1   i1TcbError;         /* return error                      */
    UINT1  u1TcbIpOptlen;      /* Length of IP options              */
    UINT1  u1TcbTos;           /* Type of Service value, optional   */
    UINT1  u1KaRetransCnt;     /* Keepalive retransmission count    */
    UINT1  u1KaRetransOverCnt; /* No. of keepalive retransmissions  */
    UINT1  u1TcbHlicmd;        /* The cmd issued and whose reply is */
    UINT1  u1ReadWaiting;      /* indicates of app blocked on read  */
    UINT1  u1Ttl;
    UINT4  u4LastDataSentTime;
    UINT4  u4ProcessID;        /* Process ID associated with the
                                  connection                         */
    UINT2  u2SendWindowScale;  /* The factor by which the Advertised 
                                 scale is to be shifted             */
    UINT2  u2RecvWindowScale;  /* The factor by which the received
                                 scale is to be shifted             */

  /* TCP-AO related parameters */
    tTcpAOAuthMktTcb   TcpAoAuthMktTCBList[TCPAO_MKT_MAX_IND];
    UINT4              u4RcvIsn;  /* initial sequence number received */
    UINT4              u4TcpAoMacVerErrs ;
    UINT4              u4TcpAoSneSnd;
    UINT4              u4TcpAoSneRcv;
    UINT4              u4IcmpIgnoreCtr;
    UINT4              u4SilentAcceptCtr;
    UINT1              u1TcpAoEnabled;
    UINT1              u1TcpAoCurrentKey;
    UINT1              u1TcpAoRNextKey;
    UINT1              u1TcpAoNoMktDisc;
    UINT1              u1TcpAoIcmpAcc;
    UINT1              u1RcvPackKeyId;
    UINT1              u1RcvPackRNextKeyId;
    UINT1              u1ActiveInd;
    UINT1              u1NewInd;
    UINT1              u1BackUpInd;
    UINT2              u2Padding2;
} tTcb;

typedef struct {
    UINT2  u2NumOfUrgBytes;
    UINT2  u2NumOfNorBytes;
} tAsyncClose;

typedef struct {
    UINT2  u2NumOfUrgBytes;
    UINT2  u2NumOfNorBytes;
} tAsyncRead;

typedef struct {
    UINT2  u2NumOfUrgBytes;
    UINT2  u2NumOfNorBytes;
} tAsyncUrgdata;

typedef struct {
    UINT2  u2Reserved; 
    INT1   i1IcmpType;
    INT1   i1IcmpCode;
} tAsyncIcmp;

typedef struct {        /* Command Code = ABORT_CMD */
    UINT4  u4ConnId;
} tAsyncAbort;

typedef struct {        /* Command Code = SEND_CMD */
   union {
    tAsyncClose    AsyncClose;
    tAsyncRead     AsyncRead;
    tAsyncUrgdata  AsyncUrg;
    tAsyncIcmp     AsyncIcmp;
    tAsyncAbort    AsyncAbort;
   } msgtype;
    UINT2  u2Reserved;
    UINT1  u1Message;
    UINT1  Padding1;
} tTcpToAppAsyncParms;


/* EXTERNS */
/* ------- */
extern tTcb   *TCBtable;
extern tMemPoolId gTcpToAppPoolId;

/* MACROS */
/* ------ */
#define   ALLOCATE_TCP_TO_APPLICATION_PARMS() \
          MemAllocMemBlk (gTcpToAppPoolId)

#define   FREE_TCP_TO_APPLICATION_PARMS(ptr) \
          MemReleaseMemBlock (gTcpToAppPoolId, (UINT1 *)ptr)

/* DEFINITIONS */
/* ----------- */

#define  TCPO_TOS                 0x0001   /* Type Of Service, IP */
#define  TCPO_SECURITY            0x0002   /* Security/Compartment option, IP */
#define  TCPO_IPOPTION            0x0004   /* Other IP options, IP */
#define  TCPO_TIMEOUT             0x0008   /* Timeout value, TCP */
#define  TCPO_NODELAY             0x0010   /* Don't delay , TCP*/
#define  TCPO_ASY_REPORT          0x0020   /* Asynchronous reports, TCP*/
#define  TCPO_MSS                 0x0040   /* Sender's MSS option */
#define  TCPO_ENBL_ICMP_MSGS      0x0080   /* Enable ICMP Messages*/
#define  TCPO_OOBINLINE           0x0100   /* send urgent along with normal */
#define  TCPO_SENDBUF             0x0200   /* send buffer size*/
#define  TCPO_RCVBUF              0x0400   /* Recv buffer size*/
#define  TCPO_REG_TSK_ID          0x0800   /* Register task id  */
#define  TCPO_SO_LINGER           0x1000   /* linger time */
#define  TCPO_KEEPALIVE           0x2000   /* TCP Keepalive option */
#define  TCPO_UNICAST_HOPS        0x4000   /* Unicast Hop limit for Ipv6 */
#define  TCPO_SO_REUSEADDR        0x8000   /* Reuse address */
#define  TCPO_MD5SIG             0x10000   /* TCP MD5 Option */
#define  TCPO_AO                 0x20000   /* TCP Authentication option (RFC 5925) */
#define  TCPO_AO_MKTMCH          0x40000   /* TCP-AO  No Mkt-match packet discard  config RFC 5925 */
#define  TCPO_AO_ICMP            0x80000   /* TCP-AO  ICMP Accept config RFC 5925 */

/* TCP HLI command codes */
#define  OPEN_CMD                 (1)     
#define  CLOSE_CMD                (2)     
#define  SEND_CMD                 (3)     
#define  RECEIVE_CMD              (4)     
#define  STATUS_CMD               (5)     
#define  ABORT_CMD                (6)     
#define  OPTIONS_CMD              (7)     
#define  ERROR_MSG                (8)     
#define  CONN_REPORT              (9)     
#define  NEW_CONN                 (10)    
#define  REG_ASYNC_HANDLER        (11)    

/* TCP states */
#define  TCPS_TIMEWAIT            11      
#define  ACTIVE_OPEN              0       
#define  PASSIVE_OPEN             1       
#define  TCP_CONNECTION           2       
#define  URGENT_DATA              0x01        /* Urgent Flag */
#define  PUSH_DATA                0x08        /* Push   Flag */
#define  ERR                      (-1)    
#define  GET                      0       

/* TCP errors */
#define  TCPE_RESET               (4)     
#define  TCPE_TIMEDOUT            (5)     
#define  TCPE_CONN_REFUSED        (16) 
#define  TCPE_TIMINGOUT           (19)    
#define  TCPE_MGMTRESET           (21)        /* SNMP has resetted conn */
#define  TCP_URG_DATA_ARRIVED     (22)    
#define  TCP_NORMAL_DATA_ARRIVED  (23)    
#define  TCPE_ICMP_MSG_ARRIVED    (24)        /* ICMP Message Arrived   */
#define  TCP_CLOSE_ARRIVED        (25)        /* Remote Side Wants to   */
                                              /* Close the Connection   */

/*Address Type*/
#define IPV4_ADD_TYPE      (1)
#define IPV6_ADD_TYPE      (2)
#define  TCP_PROTOCOL      6
    
/* PROTOTYPES */
/* ---------- */
INT4  TcpInit PROTO ((void));

VOID TcpTaskMain PROTO ((INT1 *));
INT4  TcpSetContext (UINT4 u4SetValFsMIContextID);
VOID  TcpReleaseContext(VOID);
VOID  TcpStopAllTimers(VOID);


#endif /* _TCP_H */
