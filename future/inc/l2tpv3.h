/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * Description:This file contains the exported definitions and
 *             macros of FIREWALL
 *
 *******************************************************************/
#ifndef _L2TPV3_H
#define _L2TPV3_H

PUBLIC UINT4 L2tpInit                    PROTO((VOID));
PUBLIC VOID  L2TPHandleKernelInitFailure PROTO((VOID));
PUBLIC VOID  L2TPHandleUserInitFailure   PROTO((VOID));
PUBLIC VOID L2tpCFAMsgProcessEvent PROTO((VOID));
PUBLIC VOID L2tpRtmMsgProcessEvent PROTO((VOID));
VOID L2tpHandleCreatePort PROTO((UINT4));
VOID L2tpHandleDeletePort PROTO((UINT4));
PUBLIC VOID L2tpApiNotifyPortVlanId PROTO((UINT4 , UINT2));

#define L2TP_SUCCESS                        OSIX_SUCCESS
#define L2TP_FAILURE                        OSIX_FAILURE

#define L2TP_ALWAYS                         1 

#define L2TP_MAC_ADDR_LEN                   6
#define L2TP_ETH_HDR_LEN                    18
#define L2TP_IP_HDR_LEN                     20
#define L2TP_HDR_LEN                        16

#define L2TP_TASK_NAME                      "L2TP"
#define L2TP_EVENT_WAIT_FLAGS               OSIX_WAIT

#define L2TP_MIN_XCONNECT_ID                1
#define L2TP_MAX_XCONNECT_ID                65535

#define L2TP_MIN_REMOTE_END_ID              1
#define L2TP_MAX_REMOTE_END_ID              65535

#define L2TP_MIN_PSEUDO_WIRE_ID             1
#define L2TP_MAX_PSEUDO_WIRE_ID             65535

/* The below constants are Range of Inner Vlan Ids */
#define L2TP_MIN_VLANIDS                    0
#define L2TP_MAX_VLANIDS                    4094

/* IP address length for printing as a string */
#define L2TP_IP_ADDR_LEN                    22

#define COOKIE_TYPE_FOUR_BYTES                1
#define COOKIE_TYPE_EIGHT_BYTES               2
#define COOKIE_TYPE_NONE                      3


#define COOKIE_FOUR_BYTES_VALUE               4
#define COOKIE_TYPE_EIGHT_VALUE               8
#define COOKIE_VALUE_NONE                     0

enum {
     PORT_ENCAPSULATION_TYPE_PORT =1,
    PORT_ENCAPSULATION_TYPE_PORT_VLAN,
     PORT_ENCAPSULATION_TYPE_QINQ,
     PORT_ENCAPSULATION_TYPE_QINANY
 };
#ifdef L2TPV3_GLOBAL
#define tL2tpLock tOsixSemId
tL2tpLock  L2tpLck;
#else
#define tL2tpLock tOsixSemId
extern tL2tpLock  L2tpLck;
#endif
#define L2TP_MAX_IF_NAME_LEN                50
#define L2TP_VLANID_TYPE_MASK               0x0fff

#define L2TP_SYS_START                      1
#define L2TP_SYS_SHUT                       2

#define L2TP_IPADDR_TO_ARR(u4Addr,Oct) \
        Oct[0] = (UINT1) ((u4Addr & 0xFF000000) >> 24); \
    Oct[1] = (UINT1) ((u4Addr & 0x00FF0000) >> 16); \
    Oct[2] = (UINT1) ((u4Addr & 0x0000FF00) >> 8); \
    Oct[3] = (UINT1) ((u4Addr & 0x000000FF))

#define L2TP_CUSTOMER_BRIDGE     1
#define L2TP_PROVIDER_BRIDGE     2
#define L2TP_PROVIDER_EDGE_BRIDGE 3
#define L2TP_PROVIDER_CORE_BRIDGE 4

PUBLIC INT4 L2tpLock PROTO ((VOID));
PUBLIC INT4 L2tpUnLock PROTO ((VOID));
INT4 L2tpArpLock PROTO((VOID));
INT4 L2tpArpUnLock PROTO((VOID));

INT4 L2tpProcessPktForOutBound (tCRU_BUF_CHAIN_HEADER * pBuf,UINT4 i4IfIndex,UINT4 *pu4WanIfIndex,INT4 *pi4IsIpsecEnabled);
INT4 L2tpProcessPktForInBound (tCRU_BUF_CHAIN_HEADER * pBuf,UINT4 i4IfIndex);

/* Macros for classification of IP addresses */
#define   L2TP_IS_ADDR_CLASS_A(u4Addr)   ((u4Addr & 0x80000000) == 0)
#define   L2TP_IS_ADDR_CLASS_B(u4Addr)   ((u4Addr & 0xc0000000) == 0x80000000) 
#define   L2TP_IS_ADDR_CLASS_C(u4Addr)   ((u4Addr & 0xe0000000) == 0xc0000000)
#define   L2TP_IS_ADDR_CLASS_D(u4Addr)   ((u4Addr & 0xf0000000) == 0xe0000000)
#define   L2TP_IS_ADDR_CLASS_E(u4Addr)   ((u4Addr & 0xf0000000) == 0xf0000000)

/*Restriction  for 127.0.0.1 loopback address */
#define   L2TP_IS_LOOPBACK(u4Addr)       (u4Addr == 0x7f000001)

#endif /*_L2TPV3_H*/
