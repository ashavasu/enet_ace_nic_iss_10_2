/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: httpssl.h,v 1.16 2014/01/20 12:08:54 siva Exp $
 *
 * Description: This file has the typedefs and prototypes common to HTTP
 *              and SSL
 *
 ***********************************************************************/

#ifndef __HTTPSSL_H__
#define __HTTPSSL_H__
#include "fsssl.h"

/* Prototype declarations */
/* Functions to enable/disable HTTP/HTTPS */
VOID    HttpsEnable          PROTO ((VOID));
VOID    HttpsDisable         PROTO ((VOID));

VOID HttpGetHttpsStatus PROTO((INT4 *pi4HttpsStatus));

#endif /* __HTTPSSL_H__ */
