/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rmap.h,v 1.22 2017/12/12 13:28:30 siva Exp $
 *
 * Description:This file contains the exported definitions and
 *             include files of ROUTE MAP module
 *
 *******************************************************************/
#ifndef _RMAP_H
#define _RMAP_H

#include "utilipvx.h"
#include "ip.h"
#include "bgp.h"

#define RMAP_BGP_MAX_COMM            16
#define RMAP_BGP_AUTO_TAG            0x80000000
#define RMAP_BGP_MAX_TAG_VAL         0x0000ffff

/* unified structure for IPv4/IPv4 routes */
typedef struct {
    tTMO_SLL    TSASPath;
    tIPvXAddr   DstXAddr;
    UINT2       u2DstPrefixLen; /*TODO: must be u1*/
    INT2        i2RouteType; /* common for tRtInfo.u2RtType & tIp6RtEntry.i1Type */
    tIPvXAddr   SrcXAddr; /* route src ip has no prefix/netmask, use one of MATCH node for comparison */
    tIPvXAddr   NextHopXAddr;

    UINT4       u4IfIndex;
    INT4        i4Metric; /* common for tRtInfo.i4Metric1 & tIp6RtEntry.u4Metric */
    UINT4       u4RouteTag;

    UINT4       u4NextHopAS;
    UINT4       u4MatchAs;
    UINT4       au4Community[RMAP_BGP_MAX_COMM];
    UINT4       u4LocalPref;
    UINT4       u4ExtCommCost; /*BGP Extended community cost attribute Cost Value 
                                used for Best route selection process in BGP*/
    INT4        i4Origin;
/*  UINT4       u4OriginAsNum;*/

    UINT2       u2Weight;
    UINT2       u2ExtCommId; /* BGP Extended community cost attribute community Id 
                                used for Best route selection process in BGP,
                                when the u4ExtCommCost is same for the routes, 
                                CommId is used in the selection process.
                                Route with lower CommId is preferred.*/
    UINT2       u2ExtCommPOI;/*BGP Extended community cost attribute Point of Insertion.
                               Specifies the point of insertion for usage of extended
                               cost community in the BGP best route selection  process*/
    UINT1       u1Addflag; /* flag for community additive  */
    UINT1       u1AutoTagEna;
    UINT1       u1MetricType;
    UINT1       u1Level;
    UINT1       u1RmapAccess;
    UINT1       u1AsCnt;
    INT4         i4RMapFlag;    /* Indicates if the route-map is applied on the
                                  route */  
    UINT4       u4SetFlag;   /* This Flag will be set if we are setting any field
                                Route-Map*/
    UINT2       u2RtProto; /* Protocol ID */
    UINT1       u1CommunityCnt;
    UINT1       au1Align[1];
} tRtMapInfo;

    
/* Registration Ids which applications can use to register with Route Map */
/* RTM,RTM6 should be the first */
enum _RMapApp {
    RMAP_APP_RTM        = 0, /*RTM4, redistribution*/
    RMAP_APP_RTM6       = 1, /*RTM6, redistribution*/
    RMAP_APP_RIP        = 2,
    RMAP_APP_RIP6       = 3,
    RMAP_APP_OSPF2      = 4,
    RMAP_APP_OSPF3      = 5,
    RMAP_APP_ISIS       = 6,
    RMAP_APP_BGP4       = 7,
    RMAP_MAX_APPLNS     = 8
};


/* Macros */
#define METRIC_SET_RMAP             1

#define RMAP_MAX_NAME_LEN           20
#define RMAP_COMM_STR_LEN            300
#define RMAP_ROUTE_PERMIT            1 
#define RMAP_ROUTE_DENY              2
#define RMAP_ENTRY_NOT_MATCHED       3
#define RMAP_ENTRY_MATCHED           4
#define RMAP_ENTRY_MISMATCHED        5 /* MATCH configured but not met */
#define PERMIT_PREFIX                6
#define DENY_PREFIX                  7
#define RMAP_AFI_NOT_MATCHED         8

#define RMAP_BGP_COMMUNITY_ADDITIVE  2
#define RMAP_BGP_COMMUNITY_REPLACE   1

#define RMAP_ZERO                    0
#define RMAP_ONE                     1

#define RMAP_BGP_EXT_COMM_POI        129 /*BGP Extended Community Attribute POI */

#define   RMAP_FAILURE      -1
#define   RMAP_SUCCESS       0

/****************** MIB values ************************/
#define RMAP_MAX_TAG_VAL          0x7fffffff  /* 2^31*/                
#define RMAP_MAX_ASPATH_VAL       0x7fffffff /* 2^31*/                 
#define RMAP_MAX_LOCAl_PREF_VAL   0x7fffffff /* 2^31*/                
#define RMAP_MAX_WEIGHT_VAL       0x0000ffff
#define RMAP_MAX_EXT_COMM_ID      255 /* one octet value */

enum rmap_access {
 RMAP_PERMIT =1,
 RMAP_DENY   =2
};

enum metric_type {
    RMAP_METRIC_TYPE_INTRA_AREA     =1,
    RMAP_METRIC_TYPE_INTER_AREA     =2,
    RMAP_METRIC_TYPE_TYPE1EXTERNAL  =3,
    RMAP_METRIC_TYPE_TYPE2EXTERNAL  =4
};

enum route_type {
    RMAP_ROUTE_TYPE_LOCAL   =3,
    RMAP_ROUTE_TYPE_REMOTE  =4
};

#define  RMAP_COMM_INTERNET   1
#define  RMAP_COMM_COMMNUM    2  
#define  RMAP_COMM_LOCALAS    0xFFFFFF03
#define  RMAP_COMM_NO_ADVT    0xFFFFFF02
#define  RMAP_COMM_NO_EXPORT  0xFFFFFF01
#define  RMAP_COMM_NONE       0xFFFFFFFF

#define  RMAP_MAX_COMMUNITY_NUM 0xFFFFFFFE
#define  RMAP_MIN_COMMUNITY_NUM 0x00010000
enum origin{
 RMAP_ORIGIN_IGP         =1,
 RMAP_ORIGIN_EGP         =2,
 RMAP_ORIGIN_INCOMPLETE  =3
};

enum auto_tag_ena {
    RMAP_AUTO_TAG_ENA = 1,
    RMAP_AUTO_TAG_DIS
};

enum level{
 RMAP_LEVEL_1        =1,
 RMAP_LEVEL_2        =2,
 RMAP_LEVEL_1_2      =3,
 RMAP_LEVEL_STUB_AREA=4,
 RMAP_LEVEL_BACKBONE =5
};


/* Common APIs */
INT4  RMapInit PROTO((VOID));
INT4  RMapDeInit PROTO((VOID));
INT4  RMapProcessRouteMapUpdate PROTO((UINT1 *pu1RMapName));

VOID  RMapRegister (UINT1 u1AppId,
        INT4 (*SendToApplication)
        (UINT1 *pu1RMapName, UINT4 u4Status) );
VOID  RMapDeRegister PROTO((UINT1 u1AppId));


INT4 RMapLockWrite (VOID);
INT4 RMapUnLockWrite (VOID);
INT4 RMapLockRead (VOID);
INT4 RMapUnLockRead (VOID);

UINT4
RMapGetInitialMapStatus (UINT1 *pu1RMapName);


UINT1
RMapApplyPrefixRule (tRtMapInfo * pInputRtInfo, tRtMapInfo * pOutputRtInfo,
                     UINT1 *pu1RMapName);

INT4
RMapCheckIfIpPrefixExist (tSNMP_OCTET_STRING_TYPE * pu1RMapName);

UINT1
RMapApplyRule (tRtMapInfo * pInputRtInfo, tRtMapInfo * pOutputRtInfo,
                       UINT1 *pu1RMapName);
UINT1
RMapApplyRule2 ( tRtMapInfo *pInputRtInfo , UINT1 *pu1RMapName );

UINT1
RMapApplyRule3 ( tRtMapInfo *pInputRtInfo , tRtMapInfo * pOutputRtInfo,
                 UINT1 *pu1RMapName );

UINT1
RMapApplyRule4 ( tRtMapInfo *pInputRtInfo , UINT1 *pu1RMapName );

INT4 RMapGetNextValidIpPrefixEntry (UINT1 *pu1IpPrefixName, UINT4 u4SeqNo,
                                    tBgp4OrfEntry *pOrfEntry);

UINT4 RMapIpPrefixExist (tSNMP_OCTET_STRING_TYPE * pIpPrefixNameOct,  UINT1 u1Afi);

/* API for IPv4 */
#ifdef _IPV4_H /*file with tRtInfo*/

VOID
RMapTRtInfo2TRtMapInfo (tRtMapInfo* pOut,
                        tRtInfo*    pIn,
                        UINT4       u4SrcAddr);
VOID
RMapTRtMapInfo2TRtInfo (tRtInfo*    pOut,
                        tRtMapInfo* pIn);

extern UINT1        gaPreference[MAX_ROUTING_PROTOCOLS];
#endif

/* API for IPv6 */
#ifdef _FSIPV6_H /*file with tIp6RtEntry*/

VOID
RMapTIp6RtEntry2TRtMapInfo (tRtMapInfo*  pOut,
                            tIp6RtEntry* pIn,
                            tIp6Addr*    pSrcAddr);

VOID
RMapTRtMapInfo2TIp6RtEntry (tIp6RtEntry* pOut,
                            tRtMapInfo*  pIn);

#endif



/* Validating RMAP AS number */
#define RMAP_MAX_AS_LEN_WITH_COLON 11
#define RMAP_MAX_AS_DIGITS_LEN_IN_STR 6
#define RMAP_MAX_AS_LEN 10
#define RMAP_MAX_AS_DIGITS_LEN 5

#define  RMAP_MAX_TWO_BYTE_AS 0xFFFF

/* Values for tFilteringRMap::u1Status */
#define   FILTERNIG_STAT_ENABLE      1
#define   FILTERNIG_STAT_DISABLE     0
#define   FILTERNIG_STAT_DEFAULT     FILTERNIG_STAT_DISABLE

/* Types of filtering */
#define   FILTERING_TYPE_DISTANCE    1
#define   FILTERING_TYPE_DISTRIB_IN  2
#define   FILTERING_TYPE_DISTRIB_OUT 3

#define   FILTERING_ROUTEMAP_IN 1
#define   FILTERING_ROUTEMAP_OUT 2
#define   INVALID_STATUS  0

#define RMAP_MAX_IPV4_PREFIX_LEN 32
#define RMAP_MAX_IPV6_PREFIX_LEN 128

#define RMAP_IPV4_ADDR_TYPE 1
#define RMAP_IPV6_ADDR_TYPE 2
/* filtering config */
typedef struct FilteringRMap
{
    UINT1     au1DistInOutFilterRMapName[RMAP_MAX_NAME_LEN + 4]; /* route map name */
    UINT1     au1DistInOutFilterTempRMapName[RMAP_MAX_NAME_LEN + 4]; /* Temporary storage req. 
                                                                      * for copying Peergroup to Peer */
    UINT1     u1RMapDistance; /* if seq no is 0 then distance value will be stored here */
    UINT1     u1RMapOldDistance; /* Previous distance value will be stored  here*/
    UINT1     u1Status;        /* status of filtering (FILTERNIG_STAT_ENABLE/FILTERNIG_STAT_DISABLE)*/
    UINT1     u1RowStatus;
} tFilteringRMap;


INT1
CmpFilterRMapName (tFilteringRMap* pFilterRMap, tSNMP_OCTET_STRING_TYPE* pRMapName);

tFilteringRMap*
GetMinFilterRMap (tFilteringRMap* pFilterRMap1 , INT1 i1Type1, tFilteringRMap* pFilterRMap2, INT1 i1Type2);

INT4 
RmapCliParseCommunityStringAsNum (CHR1 *pAsNoinpToken, UINT4 *pu4AsNo);
#endif /* _RMAP_H */

