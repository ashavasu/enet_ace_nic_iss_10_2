#ifndef _PARAMS_H
#define _PARAMS_H
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* $Id: params.h,v 1.518.2.1 2018/03/01 14:05:18 siva Exp $*/
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : params.h                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        :                                                */
/*    MODULE NAME           :                                                */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 29 July 2004                                   */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains parameters/constants        */
/*                            which are dependant on other sizing parameters */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    29 July 2004           Initial Creation                        */
/*---------------------------------------------------------------------------*/

/* Number of router ports.
 * Router port implementation requires reserved vlan. Therefore, VLAN-id above
 * VLAN_MAX_VLAN_ID below VLAN_DEV_MAX_VLAN_ID is reserved for router port
 * and those VLANs will not available for L2 modules.
 */
#define IP_MAX_RPORT                       SYS_DEF_MAX_PHYSICAL_INTERFACES 

/* VLAN Sizing Params */
#define VLAN_MIN_VLAN_ID                   1
#define VLAN_DEV_MIN_VLAN_ID               1

#define VLAN_MAX_PROTO_GROUP_ENTRIES          100
#define VLAN_PB_MAX_LOGICAL_PORT_ENTRIES      1024 

#define VLAN_MAX_WILDCARD_ENTRIES             128
#define VLAN_CFG_Q_DEPTH                      4096
/* Number of vlans allowed after the reserved range (4008 to 4089)
 *  * for router port support. currently its 4090 to 4094
 *   */
#define VLAN_AFTER_END_RSV_VLAN_START        4090
/* reserved vlan starts from (4080 to 4089)*/
#define VLAN_START_RPORT_VLAN_ID                (VLAN_AFTER_END_RSV_VLAN_START - IP_MAX_RPORT)
/* This macro VLAN_PORT_MEM_MAX_VLAN is used for maintaining the Per-Port 
 * Per-VLAN statistics. Therefore, the value of this macro has to be tuned 
 * keeping in mind the value of the VLAN_DEV_MAX_NUM_VLAN macro and the 
 * BRG_MAX_PHY_PLUS_LAG_INT_PORTS macro present in the target specific 
 * sizing files during scaling.
 */
#define  VLAN_MAX_PORT_VLAN_STAT_ENTRIES     50

/* The following macros are added VFI entries. These should be used only in 
 * VPLS supported modules. Rest of the modules should use the macros that are 
 * defined without *EXT notation.
 */
#define VLAN_DEV_MAX_VLAN_ID_EXT            (4094 + VLAN_DEV_MAX_VFI_ID)
#define VLAN_DEV_MAX_NUM_VLAN_EXT           (4096 + VLAN_MAX_NUM_VFI_IDS)
#define VLAN_MAX_VLAN_ID_EXT                 VLAN_DEV_MAX_VLAN_ID_EXT
#define ELPS_SYS_MAX_NUM_SERVICE_IN_LIST_EXT VLAN_DEV_MAX_VLAN_ID_EXT

/* SNOOPING Sizing Params */
#if defined (IGS_WANTED) || defined (MLDS_WANTED)
#define   SNOOP_MAX_MULTICAST_VLANS           10 /* Multicast VLAN Configurations */
#define   SNOOP_MAX_MCAST_GROUPS              255  
#define   SNOOP_MAX_MCAST_SRCS                255 /* MAX number of Sources */
#define   SNOOP_MAX_HW_ENTRIES                255 /* MAX Hardware entries  */
#define   SNOOP_MAX_HOSTS                     255
#define   SNOOP_MAX_IGMP_PKTS_PROCESSED_IN_A_SEC 100
#endif

#define SNOOP_MAX_IGS_GROUPS               10000
#define SNOOP_MAX_IGS_VLANS                   256
#define SNOOP_MAX_IGS_RTR_PORTS               64
#define SNOOP_MAX_IGS_HOST_ENTRY           100
#define SNOOP_MAX_IGS_MAC_FWD_ENTRIES       1000
#define SNOOP_MAX_IGS_CONSO_GRP_ENTRIES       1000


/* ECFM Sizing Params */
#define ECFM_MAX_MA_PER_PORT                  8
#define ECFM_MAX_MEP_PER_PORT                 8
#define ECFM_MAX_MIP_PER_PORT                 4
#define ECFM_MAX_DEF_MD_PER_CONTEXT           16
#define ECFM_MAX_MD                           32 /* Maximum number of MDs per
                                                  * bridge */
#define ECFM_MAX_DEF_MD                       1000

#define ECFM_MAX_VLAN                         8192
#define ECFM_MAX_VLAN_IN_SYSTEM               4094
#define ECFM_MAX_PORTS                        (BRG_MAX_PHY_PLUS_LOG_PORTS + \
                                               SYS_DEF_MAX_SISP_IFACES + \
                                               SYS_DEF_MAX_INTERNAL_IFACES)
#define ECFM_MAX_PORTS_IN_SYSTEM              (BRG_NUM_PHY_PLUS_LOG_PORTS + \
                                               SYS_DEF_MAX_SISP_IFACES + \
                                               SYS_DEF_MAX_INTERNAL_IFACES)
#define ECFM_MAX_MA                           (ECFM_MAX_PORTS * \
                                               ECFM_MAX_MA_PER_PORT)
#define ECFM_MAX_MA_IN_SYSTEM                 (ECFM_MAX_PORTS_IN_SYSTEM * \
                                               ECFM_MAX_MA_PER_PORT)
#define ECFM_MAX_MEP                          (ECFM_MAX_PORTS * \
                                               ECFM_MAX_MEP_PER_PORT) 
#define ECFM_MAX_MEP_IN_SYSTEM                (ECFM_MAX_PORTS_IN_SYSTEM * \
                                               ECFM_MAX_MEP_PER_PORT)
#define ECFM_MAX_MPTP_MEP_IN_SYSTEM           100
#define ECFM_MAX_MIP                          (ECFM_MAX_PORTS * \
                                               ECFM_MAX_MIP_PER_PORT)
#define ECFM_MAX_MIP_IN_SYSTEM                (ECFM_MAX_PORTS_IN_SYSTEM * \
                                               ECFM_MAX_MIP_PER_PORT)
#define ECFM_MPLSTP_MAX_INPARAMS               2
#define ECFM_MPLSTP_MAX_OUTPARAMS              2
#define ECFM_MPLSTP_MAX_PATH_IN_SYSTEM         2
/* Macro needed for MPLSTP servive pointer */
#define ECFM_MPLSTP_MAX_SERVICE_PTRS_IN_SYSTEM 3

/*START -DHCP Snooping sizing params */
/*tL2DhcpSnpQMsg*/
#define MAX_L2DS_Q_MESG                50
/*tL2DhcpSnpIfaceEntry*/
#define MAX_L2DS_IFACE_ENTRIES         VLAN_DEV_MAX_NUM_VLAN
#define MAX_L2DS_PDU_COUNT             1
#define MAX_L2DS_PKT_INFO              2
/*END -DHCP Snooping sizing params */

/*START - IP Source Guard sizing params */
/*tIpDbQMsg*/
#define MAX_IPDB_Q_MESG                  10
/*tIpDbStaticBindingEntry*/
#define MAX_IPDB_STATIC_BINDING_ENTRIES  50
/*tIpDbBindingEntry*/
#define MAX_IPDB_BINDING_ENTRIES         254
/*tIpDbIfaceEntry*/
#define MAX_IPDB_IFACE_ENTRIES           VLAN_DEV_MAX_NUM_VLAN
/*tIpDbGatewayEntry*/
#define MAX_IPDB_GATEWAY_ENTRIES         (MAX_IPDB_BINDING_ENTRIES * \
                                          1)
/*tPortCtrlEntry*/
#define MAX_IPDB_PORT_CTRL_ENTRIES       SYS_DEF_MAX_PHYSICAL_INTERFACES
/*tVlanCtrlEntry*/
#define MAX_IPDB_VLAN_CTRL_ENTRIES       VLAN_DEV_MAX_VLAN_ID
/*END - IP Source Guard sizing params */
/*START - ELPS Sizing params */
/*tElpsQMsg*/
#define MAX_ELPS_Q_MESG                       (2 * L2IWF_MAX_PORTS_PER_CONTEXT)
/*tElpsContextInfo*/
#define MAX_ELPS_CONTEXTS                     SYS_DEF_MAX_NUM_CONTEXTS
/*ElpsPgInfo*/
#define MAX_ELPS_PROTECTION_GROUP_INFO        ELPS_SYS_MAX_PG
/*tElpsServiceListInfo*/
#define MAX_ELPS_SERVICE_LIST_INFO            ELPS_SYS_MAX_NUM_SERVICE_IN_LIST
/*tElpsServiceListPointerInfo*/
#define MAX_ELPS_SYS_NUM_SERVICE_PTR_IN_LIST      ELPS_SYS_MAX_NUM_SERVICE_PTR_IN_LIST
/*tElpsCfmConfigInfo*/
#define MAX_ELPS_CFM_CONFIG_INFO              (ELPS_SYS_MAX_PG * 2)
/*tElpsApsTxInfo*/
#define MAX_ELPS_APS_TX_INFO                  1024
#define MAX_ELPS_SIMULTANEOUS_CFM_SIGNAL      1024
/*tElpsRedNpSyncEntry*/
#define MAX_ELPS_RED_NP_SYNC_ENTRIES          1
/*tMplsApiInInfo*/
#define MAX_ELPS_MPLS_IN_API_COUNT            2
/*tMplsApiOutInfo*/
#define MAX_ELPS_MPLS_OUT_API_COUNT           2
/*tMplsPAthId*/
#define MAX_ELPS_MPLS_PATH_ID_INFO            1

#define ELPS_MIN_VLAN_GROUP_ID          0
#define ELPS_MAX_VLAN_GROUP_ID          (AST_MAX_MST_INSTANCES - 1)

/*END - ELPS Sizing params */


/*L3VPN Params */
#ifdef MPLS_L3VPN_WANTED
#define MAX_L3VPN_VRF_TABLE_ENTRIES             128
#define MAX_L3VPN_MPLSL3VPNVRFTABLE_ISSET       10
#define MAX_L3VPN_IF_CONF_TABLE_ENTRIES         1280
#define MAX_L3VPN_MPLSL3VPNIFCONFTABLE_ISSET    10
#define MAX_L3VPN_RT_TABLE_ENTRIES              128
#define MAX_L3VPN_MPLSL3VPNVRFRTTABLE_ISSET     10
#define MAX_L3VPN_SEC_TABLE_ENTRIES             128
#define MAX_L3VPN_MPLSL3VPNVRFSECTABLE_ISSET    10
#define MAX_L3VPN_PERF_TABLE_ENTRIES            128
#define MAX_L3VPN_MPLSL3VPNVRFPERFTABLE_ISSET   10
#define MAX_L3VPN_ROUTE_TABLE_ENTRIES           1000
#define MAX_L3VPN_MPLSL3VPNVRFRTETABLE_ISSET    10
#define MAX_L3VPN_Q_MSG                         256
#define MAX_L3VPN_BGP_LSP_INTEREST_LIST         100
#define MAX_L3VPN_EGRESS_ROUTE_TABLE_ENTRIES    100
#define MAX_L3VPN_OID_POOL_SIZE                 20
#define MAX_L3VPN_RSVP_MAP_LABEL_TABLE_ENTRIES  100
#endif
/* L3VPN params */


/*START - Snooping Sizing params */
#ifdef PVRST_WANTED
#define MAX_SNOOP_Q_MSG      (4*SYS_DEF_MAX_INTERFACES + \
                              SYS_DEF_MAX_SISP_IFACES)
#else
#define MAX_SNOOP_Q_MSG      (4*SYS_DEF_MAX_INTERFACES + \
                               SYS_DEF_MAX_SISP_IFACES)
#endif
#define MAX_SNOOP_QUE_PKT_MSG           SNOOP_MAX_IGMP_PKTS_PROCESSED_IN_A_SEC 
#define MAX_SNOOP_GLOBAL_INSTANCES      SYS_DEF_MAX_NUM_CONTEXTS 
#define MAX_SNOOP_MISC_BLK_COUNT        1 
#define MAX_SNOOP_PORT_CFG_ENTRIES      SNOOP_MAX_IGS_PORTS
#define MAX_SNOOP_EXT_PORT_CFG_ENTRIES  SNOOP_MAX_IGS_PORTS 
#define MAX_SNOOP_CONSO_GROUP_ENTRIES   SNOOP_MAX_IGS_CONSO_GRP_ENTRIES 
#define MAX_SNOOP_GROUP_ENTRIES         SNOOP_MAX_IGS_GROUPS 
#define MAX_SNOOP_PORT_ENTRIES          SNOOP_MAX_IGS_PORTS 
#define MAX_SNOOP_VLAN_ENTRIES          SNOOP_MAX_IGS_VLANS 
#define MAX_SNOOP_VLAN_STATS_ENTRIES    SNOOP_MAX_IGS_VLANS 
#define MAX_SNOOP_VLAN_CONFIG_ENTRIES   SNOOP_MAX_IGS_VLANS
#define MAX_SNOOP_PORT_INST_ENTRIES     SNOOP_MAX_PORT_INST_ENTRIES
#define MAX_SNOOP_GROUP_STATUS_ENTRIES  SNOOP_MAX_GROUP_STATUS_ENTRIES
#define MAX_SNOOP_LPORT_ENTRIES         SNOOP_MAX_LPORT_ENTRIES
#define MAX_SNOOP_PORT_NODES            SNOOP_MAX_PORT_NODES
#define MAX_SNOOP_ROUTER_PORT_ENTRIES   SNOOP_MAX_IGS_RTR_PORTS 
#define MAX_SNOOP_MAC_FWD_ENTRIES       SNOOP_MAX_IGS_MAC_FWD_ENTRIES
#define MAX_SNOOP_SRC_BMP_NODES     (SNOOP_MAX_IGS_PORTS + SNOOP_MAX_IGS_HOST_ENTRY) 
#define MAX_SNOOP_HOST_ENTRIES          SNOOP_MAX_IGS_HOST_ENTRY 
#define MAX_SNOOP_IP_FWD_ENTRIES        SNOOP_MAX_HW_ENTRIES 
#define MAX_SNOOP_RED_HW_ENTRIES        SNOOP_MAX_HW_ENTRIES
#define MAX_SNOOP_MCAST_VLAN_MAPPING    SNOOP_MAX_MULTICAST_VLANS 
#define MAX_SNOOP_MVLAN_GRP_SRC_STATUS_ENTRIES 370
#define MAX_SNOOP_SRC_BLOCK_COUNT      1
#define MAX_SNOOP_DATA_BLOCK_COUNT     1
#define MAX_SNOOP_PVLAN_LIST_COUNT     5 
#define MAX_SNOOP_MCAST_SRCS_ENTRIES   2
#define MAX_SNOOP_HW_IP_FW_INFO_COUNT  1

/*END - Snooping Sizing params */

/* QOS - Sizing params */
#define MAX_QOS_STD_CLFR_PORT_ENTRIES       QOS_STD_CLFR_PORT_ENTRIES
#define MAX_QOS_VLAN_MAP_ENTRIES            64 /*QOS_VLAN_Q_MAP_MAX_NUM_VLAN_SUPPORTED*/

/* END - QOS - Sizing params */

/* LA Sizing Params */
#define LA_MIN_PORTS                    1
#define LA_MAX_PORTS                    SYS_DEF_MAX_PHYSICAL_INTERFACES
#ifdef LA_WANTED
#define LA_MAX_AGG                             LA_DEV_MAX_TRUNK_GROUP
#else
#define LA_MAX_AGG                             0
#endif
#define LA_MAX_PORTS_IN_SYSTEM                 SYS_DEF_NUM_PHYSICAL_INTERFACES
#ifdef LA_WANTED
#define LA_MAX_AGG_INTF                        LA_MAX_AGG
#else
#define LA_MAX_AGG_INTF                        0
#endif

/*tLaCtrlMesg*/
#define MAX_LA_CONTROL_MESG             40
/*tLaIntfMesg*/
#define MAX_LA_INTF_MESG                (LA_MAX_PORTS_IN_SYSTEM * 4)
/*tLaLacPortEntry*/
#define MAX_LA_PORTS_IN_SYS             LA_MAX_PORTS_IN_SYSTEM
/*tLaLacAggEntry*/
#define MAX_LA_AGG_IN_SYS               LA_MAX_AGG
/*tLaCacheEntry*/
#define MAX_LA_CACHE_ENTRIES            256
/*tLaMarkerEntry*/
#define MAX_LA_MARKER_ENTRIES           LA_MAX_PORTS_IN_SYSTEM

#define  MAX_RTM6_RT_MSG_HDR           300
#define  MAX_RTM6_ST_RT_MSG_HDR        1500


/*tLaDLAGRemoteAggEntry : default size to be allocated is limited to
 * LA_MAX_AGG, but for performance and scalability testing this should
 * be changed to max required value (LA_MAX_AGG * (LA_MAX_AGG - 1)) */
#define MAX_LA_DLAG_REMOTE_AGG_ENTRIES  LA_MAX_AGG
/*tLaDLAGRemotePortEntry : default size to be allocated is limited to
 * LA_MAX_PORTS_IN_SYSTEM, but for performance and scalability testing this
 * should be changed to max required value
 * ( LA_MAX_AGG * (LA_MAX_AGG - 1) * LA_MAX_PORTS_IN_SYSTEM )*/
#define MAX_LA_DLAG_REMOTE_PORT_ENTRIES LA_MAX_PORTS_IN_SYSTEM

/* tLaDLAGConsPortEntry : Default size to be allocated is
   Maximum number of remote port-channels and remote entries.
   Say for 1 port-channel 8 remote is there in which 8 ports are there.
   Calculation: (1*8*8) = 64 port entries allocated per port-channel.
*/
#define MAX_LA_DLAG_CONS_PORT_ENTRIES   (LA_MAX_AGG * MAX_LA_DLAG_REMOTE_AGG_ENTRIES * 24)

/*START- ASTP Sizing Params */
#define  AST_CFG_Q_DEPTH     \
           ((AST_MAX_PORTS_IN_SYSTEM) * 10 * 4)
    
#define  AST_QUEUE_DEPTH          (AST_MAX_PORTS_IN_SYSTEM)* 2
/*tAstContextInfo*/
#define   MAX_AST_CONTEXTS           SYS_DEF_MAX_NUM_CONTEXTS
/*tAstPortArray*/
#define   MAX_AST_PORT_ARRAY         MAX_AST_CONTEXTS
/*tAstPerStPortArray*/
#ifdef PVRST_WANTED
#define   MAX_AST_PERST_PORT_ARRAY   AST_MAX_INSTANCES
#else
#define   MAX_AST_PERST_PORT_ARRAY   (MAX_AST_CONTEXTS * \
                                      AST_MAX_MST_INSTANCES)
#endif
/*tAstPortEntry*/
#define   MAX_AST_PORTS_IN_SYS       AST_MAX_PORTS_IN_SYSTEM

/*tRstPortInfo*/
#define   MAX_AST_PORTS_IN_SYSTEM       AST_MAX_PORTS_IN_SYSTEM

/*tMstInstInfo*/
#define MAX_MST_INSTANCES               (MAX_AST_CONTEXTS * \
                                         AST_MAX_MST_INSTANCES)
/*tAstFrameSize*/
#define MAX_AST_ETHFRAME_SIZE        1

/*tMstVlanMap*/
#define MAX_MST_VLANMAP              4

/*tMstVlanList*/
#define MAX_MST_VLANLIST             7

/*tAstBpduType */

#define MAX_AST_BPDU_TYPE             5


#ifdef PVRST_WANTED
/*tAstPerStInfo*/
#define   MAX_AST_PERST_INFO_IN_SYS  AST_MAX_INSTANCES

/* Maximum no. of instances may be from PVRST or MSTP. Hence considering the Max no. for allocation*/
/*tAstPerStPortInfo*/
#define   MAX_AST_PERST_PORT_INFO_IN_SYS (MAX_AST_PORTS_IN_SYS* \
                                          (AST_MAX_MST_INSTANCES > AST_MAX_PVRST_INSTANCES \
                                          ? AST_MAX_MST_INSTANCES : AST_MAX_PVRST_INSTANCES))
#else

/* Maximum no. of instances may be from PVRST or MSTP. Hence considering the Max no. for allocation*/
/*tAstPerStInfo*/
#define   MAX_AST_PERST_INFO_IN_SYS  ((MAX_AST_CONTEXTS* \
                                      (AST_MAX_MST_INSTANCES > AST_MAX_PVRST_INSTANCES \
                                       ? AST_MAX_MST_INSTANCES : AST_MAX_PVRST_INSTANCES))\
                                       + AST_MAX_CVLAN_COMP)
/*tAstPerStPortInfo*/
#define   MAX_AST_PERST_PORT_INFO_IN_SYS (MAX_AST_PORTS_IN_SYS* \
                                          (AST_MAX_MST_INSTANCES > AST_MAX_PVRST_INSTANCES \
                                           ? AST_MAX_MST_INSTANCES : AST_MAX_PVRST_INSTANCES))
#endif

#ifdef PVRST_WANTED
/*tPerStPvrstBridgeInfo*/
#define   MAX_AST_PVRST_BRIGE_INFO   AST_MAX_PVRST_INSTANCES
/*tPerStPvrstRstPortInfo*/
#define   MAX_AST_PVRST_RST_PORT_INFO  (SYS_DEF_MAX_PORTS_PER_CONTEXT * \
                                        MAX_AST_PVRST_BRIGE_INFO)

#define   MAX_AST_PVRST_VLAN_TO_INDEX_MAPPING_COUNT \
                      (AST_MAX_PVRST_INSTANCES * 2) 
        
/*tPerStPvrstBridgeInfo*/
#define   MAX_PVAST_PVRST_BRIGE_INFO   AST_MAX_PVRST_INSTANCES
/*tPerStPvrstRstPortInfo*/
#define   MAX_PVAST_PVRST_RST_PORT_INFO  (SYS_DEF_MAX_PORTS_PER_CONTEXT * \
                                        MAX_PVAST_PVRST_BRIGE_INFO)

#define   MAX_PVAST_PVRST_VLAN_TO_INDEX_MAPPING_COUNT \
                      (AST_MAX_PVRST_INSTANCES * 2)
#define   MAX_PVAST_RED_PVRST_PER_PORT_INST_INFO  MAX_AST_PORTS_IN_SYS
#else

/*tPerStPvrstBridgeInfo*/
#define   MAX_AST_PVRST_BRIGE_INFO   1
/*tPerStPvrstRstPortInfo*/
#define   MAX_AST_PVRST_RST_PORT_INFO  1

#define   MAX_AST_PVRST_VLAN_TO_INDEX_MAPPING_COUNT  1

/*tPerStPvrstBridgeInfo*/
#define   MAX_PVAST_PVRST_BRIGE_INFO   1
/*tPerStPvrstRstPortInfo*/
#define   MAX_PVAST_PVRST_RST_PORT_INFO  1
#define   MAX_PVAST_PVRST_VLAN_TO_INDEX_MAPPING_COUNT  1
#define   MAX_PVAST_RED_PVRST_PER_PORT_INST_INFO  1
#endif /*PVRST_WANTED*/

#ifdef L2RED_WANTED
/*tAstRedPortInfoArray*/
#define   MAX_AST_RED_PORT_INFO_ARRAY  MAX_AST_CONTEXTS
/*tAstRedPortInfo*/
#define   MAX_AST_RED_PORT_INFO   MAX_AST_PORTS_IN_SYS
/*tAstRedPerPortInstInfoSize*/
#define   MAX_AST_RED_PER_PORT_INST_INFO  MAX_AST_PORTS_IN_SYS
/*tAstRedContextInfo*/
#define   MAX_AST_RED_CVLAN_CONTEXT_INFO  AST_MAX_CVLAN_COMP
/*tAstRedPerPortInstInfo*/
#define   MAX_AST_RED_CVLAN_PER_PORT_INST_INFO   \
                       (AST_MAX_SERVICES_PER_CUSTOMER * \
                        AST_MAX_CVLAN_COMP)


#else
/*tAstRedPortInfoArray*/
#define   MAX_AST_RED_PORT_INFO_ARRAY  1
/*tAstRedPortInfo*/
#define   MAX_AST_RED_PORT_INFO   1
/*tAstRedPerPortInstInfoSize*/
#define   MAX_AST_RED_PER_PORT_INST_INFO  1
/*tAstRedContextInfo*/
#define   MAX_AST_RED_CVLAN_CONTEXT_INFO  1
/*tAstRedPerPortInstInfo*/
#define   MAX_AST_RED_CVLAN_PER_PORT_INST_INFO   1

#endif
/*tAstContextInfo*/
#define   MAX_AST_PB_CONTEXT_INFO    AST_MAX_CVLAN_COMP 
/*tAstPerStInfo*/
#define   MAX_AST_PB_PERST_INFO      AST_MAX_CVLAN_COMP
/*tAstPbCVlanInfo*/
#define   MAX_AST_PB_CVLAN_INFO      AST_MAX_CVLAN_COMP
/*tAstPbPortInfo*/
#define   MAX_AST_PB_PORT_INFO      (AST_MAX_CVLAN_COMP * \
                                     (AST_MAX_SERVICES_PER_CUSTOMER +1)) 
/*tAstPort2IndexMap*/
#define   MAX_AST_PB_PORT_INDEX_MAP  (AST_MAX_CVLAN_COMP * \
                                      (AST_MAX_SERVICES_PER_CUSTOMER +1))
/*tAstTimer*/
/* Calculated for 5 per-inst-per-port timers and 3 per-port timers */
#ifdef PVRST_WANTED
#define   MAX_AST_TIMER_BLOCKS    ((MAX_AST_PORTS_IN_SYS * \
                                    AST_MAX_PVRST_INSTANCES * 5) +\
                                   (MAX_AST_PORTS_IN_SYS * 3) + \
           (AST_MAX_MST_INSTANCES * \
         MAX_AST_CONTEXTS))
#else
#define   MAX_AST_TIMER_BLOCKS    ((MAX_AST_PORTS_IN_SYS *  \
                                    AST_MAX_MST_INSTANCES * 5) + \
                                   (MAX_AST_PORTS_IN_SYS * 3) + \
           (AST_MAX_MST_INSTANCES * \
         MAX_AST_CONTEXTS))
#endif /*PVRST_WANTED*/
/*tAstMsgNode*/
#define   MAX_AST_LOCAL_MESG      (AST_CFG_Q_DEPTH + \
                                   AST_AVG_LOCAL_MSGS)
/*tAstQMsg*/
#define   MAX_AST_Q_MESG           (AST_QUEUE_DEPTH)
/*tAstQMsg*/
#define   MAX_AST_CONFIG_MESG      (AST_CFG_Q_DEPTH)
/*tAstMsgDigestsize*/
#define   MAX_AST_MST_DIGEST_MESG   MAX_AST_CONTEXTS
#define   MAX_AST_MSTP_DIGEST_MESG   MAX_AST_CONTEXTS 

/* END- ASTP Sizing Params */

/* ELMI Sizing Params */
#define ELM_MAX_NUM_PORTS_SUPPORTED     (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG)
#define ELM_MAX_NUM_PORTS_SUPPORTED_IN_SYSTEM  \
                                        (SYS_DEF_NUM_PHYSICAL_INTERFACES + LA_MAX_AGG)
/* Maximum number of ports supported*/
#define ELM_MAX_PORT_INFO               (ELM_MAX_NUM_PORTS_SUPPORTED)
/* Maximum number of timer nodes supported */
#define ELM_MAX_NUM_TMRNODES        (ELM_MAX_NUM_PORTS_SUPPORTED_IN_SYSTEM * 2)
/* Maximum size of Asynchronous list */
#define ELM_MAX_ASYNCH_LIST            64

#define SYS_MAX_LOOPBACK_INTERFACES    10

#ifdef PPP_WANTED
#define SYS_MAX_PPP_INTERFACES         128
#else
#define SYS_MAX_PPP_INTERFACES         0
#endif
#ifdef HDLC_WANTED
#define SYS_MAX_HDLC_INTERFACES        10
#else
#define SYS_MAX_HDLC_INTERFACES        0
#endif

/* START - ELMI Sizing Params */
#define MAX_ELMI_Q_MESG             ELM_MAX_NUM_PORTS_SUPPORTED_IN_SYSTEM * 2
#define MAX_ELMI_LOCAL_MESG         (ELM_MAX_NUM_PORTS_SUPPORTED_IN_SYSTEM \
                                                                    * 10)
#define MAX_ELMI_CONFIG_Q_MESG      ELM_MAX_NUM_PORTS_SUPPORTED_IN_SYSTEM * 2
#define MAX_ELMI_PORT_INFO_ARRAY    1
#define MAX_ELMI_PORT_INFO          ELM_MAX_PORT_INFO
#define MAX_ELMI_ASYNC_MESG_NODES   ELM_MAX_ASYNCH_LIST
#define MAX_ELMI_TMR_NODES          ELM_MAX_NUM_TMRNODES
#define MAX_ELM_EVC_STATUS_INFO     10
#define MAX_ELM_EVC_VLAN_INFO       10
#define MAX_CFA_UNI_INFO            10
#define MAX_LCM_EVC_INFO            10
#define MAX_LCM_EVC_CE_VLAN_INFO    10
#define MAX_ELM_ETH_FRAME_BUF       10
/* END - ELMI Sizing Params */

/* EOAM Sizing Params */
#define EOAM_MIN_PORTS                1
#define EOAM_MAX_PORTS                SYS_DEF_NUM_PHYSICAL_INTERFACES
#define MAX_EOAM_Q_MESG                             EOAM_QMSG_COUNT 
#define MAX_EOAM_PORTS                              EOAM_MAX_PORTS 
#define MAX_EOAM_ENABLED_PORTS                      EOAM_ENABLED_PORTS 
#define MAX_EOAM_VAR_REQUESTS                       EOAM_MAX_VAR_REQS 
#define MAX_EOAM_VAR_REQ_RBT_NODES                  EOAM_BRANCH_LEAF_COUNT 
#define MAX_EOAM_EVENT_LOG_ENTRIES                  (EOAM_ENABLED_PORTS*EOAM_MAX_LOG_ENTRIES) 
#define MAX_EOAM_DLL_NODES_FOR_EOAM_ENABLED_PORTS   EOAM_ENABLED_PORTS
#define MAX_EOAM_PDU_BUF                            1
#define MAX_EOAM_DATA_BUF                           1

/* PTP Sizing Params */
#define MAX_PTP_Q_MESG              128
#define MAX_PTP_CONTEXTS            128
#define MAX_PTP_DOMAINS             128
#define MAX_PTP_PORTS               128
#define MAX_PTP_FOREIGN_MASTERS              256
#define MAX_PTP_GRAND_MASTER_TABLE           1
#define MAX_PTP_ACCEPTABLE_MASTER_TABLE      256
#define MAX_PTP_ACCEP_UCAST_MASTER_TABLE     1
#define MAX_PTP_SECURITY_ASSOC               1
#define MAX_PTP_SECURITY_KEY_LIST            1
#define MAX_PTP_VERSION_COMP_DB_ENTRIES      128
#define MAX_PTP_ALT_TIME_SCALE_ENTRIES       256
#define MAX_PTP_UCAST_NEG_GRANTEE_TABLE      1
#define MAX_PTP_UCAST_NEG_GRANTOR_TABLE      1
#define MAX_PTP_UTILITY_ENTRY                10
#define MAX_PTP_G8261_DELAY_SYSTEM_ENTRIES  2
#define MAX_PTP_G8261_DELAY_PARAM_ENTRIES 2
#define MAX_PTP_DELAY_OUTPUT_ENTRIES 2
/* END -  PTP SIZING PARAMS */

/*PPP - Sizing Params*/

#define MAX_IPCP_NODE_BLOCKS            128 
#define MAX_IPCP_IPCPIF_BLOCKS          128 
#define MAX_IPCP_ADDR_BLOCKS            128
#define MAX_LCP_AUTH_BLOCKS             128 
#define MAX_LCP_LQM_BLOCKS              128
#define MAX_LCP_MSG_DESC_BLOCKS         128
#define MAX_LCP_PPPIF_BLOCKS            128
#define MAX_PPPOE_PADOLIST_BLOCKS       16
#define MAX_PPPOE_SESSION_BLOCKS        128
#define MAX_PPPOE_CLIENT_DIS_BLOCKS     16
#define MAX_PPPOE_BINDING_BLOCKS        128
#define MAX_PPPOE_VLAN_BLOCKS           16
#define MAX_PPPOE_SERVICE_BLOCKS        16
#define MAX_PPP_SERVICES_BLOCKS         128   
#define MAX_PPP_SECRET_BLOCKS           128  
#define MAX_PPP_BUF_BLOCKS              10
#define MAX_PPP_LINEAR_BUF_BLOCKS       10   
/* END - PPP Sizing Params*/

/* CFA sizing params */
#define   SYS_DEF_MAX_FFM_ENTRIES       1000
#define   SYS_DEF_MAX_TUNL_IFACES    20
#define   CFA_MAX_TLVS_PERPORT     10
/* IFType Protocol Deny related macros */                                   
/* Max number of bridge port type that can be restricted to a protocol */ 
#define CFA_MAX_BRGPORTTYPE_PROT_DENY CFA_INVALID_BRIDGE_PORT             
/* Max number of protocols that can restrict IFTYPES and BRGPORT Types */ 
#define CFA_MAX_PROT_DENY    L2IWF_PROTOCOL_ID_MAX                  
/* Max number of entries in IFTypeProtoDeny Table. */                     
#define L2IWF_MAX_IFTYPE_PROT_ENTRIES   100   


/* VCM Sizing params */
#define VCM_MAX_PORTS_IN_SYS            BRG_NUM_PHY_PLUS_LAG_INT_PORTS

/* Security - IDSIPC Sizing params */
#ifdef IDS_WANTED
#define MAX_IDS_BUFF_MSG_SIZE                  100 
#define MAX_IDS_ATTACK_BUFF_MSG_SIZE           6 
#endif /* IDS_WANTED */

/* Security - Sizing params */
#define  MAX_PPPOE_DEF_SESSION                 100

/* Security module RB Tree (Unicast & broadcast IP address sizing params) 
   (unicast + Broadcast)2 * (No. of LAN + WAN interfaces) * 5 (Max. Secondary IP per interface)*/
#define  MAX_SEC_INTERFACE_IPADDR        (SYS_MAX_LAN_INTERFACES +SYS_MAX_WAN_INTERFACES) * 10

/* SISP Sizing params */
#define SISP_MAX_SISP_ENABLED_PORTS     10

/* PVLAN Sizing params */


#define VLAN_MAX_SECONDARY_VLANS (VLAN_MAX_ISOLATED_VLANS + \
                                   VLAN_MAX_COMMUNITY_VLANS)
/* MRP Sizing params */
                                        
/* If SYS_DEF_MAX_NUM_CONTEXTS is less than 32,
 * then use SYS_DEF_MAX_NUM_CONTEXTS else use 32.
 */
/*tMrpContextInfo*/
#define MRP_MAX_NUM_CONTEXTS     SYS_DEF_MAX_NUM_CONTEXTS

/* START - MRP Sizing params */
/*tMrpPortEntry*/
/* If BRG_MAX_PHY_PLUS_LOG_PORTS is less than 1000,
 * then use BRG_MAX_PHY_PLUS_LOG_PORTS else use 1000.
 */
#define MAX_MRP_PORT_ENTRIES_IN_SYSTEM   MRP_MAX_MVRP_PORTS
                                      
#define MRP_MAX_PORTS_PER_CONTEXT        L2IWF_MAX_PORTS_PER_CONTEXT

#define MRP_MAX_PEER_MAC_ADDR_ENTRIES 256 /* Assumption: There are 256 peers 
                                             are connected in a LAN. If needed 
                                             this can be tuned based on the 
                                             number of peers. */

/* VLAN_DEV_MAX_NUM_VLAN - is the maximum number of VLANs supported in the 
 * system.
 * If both GARP and MRP are made YES during compilation, then Garp
 * can run on some context and MRP on some other contexts. Hence
 * the number of attributes (no. of vlans, mac) should be divided between
 * GARP and MRP.
 * Hence the number of vlan supported by MRP is made VLAN_DEV_MAX_NUM_VLAN/2.
 * Similar change is present for GARP too.
 * This can be tuned as per the requirement.
 * Example: If we compile with GARP=NO, then we can make 
 * MRP_VLAN_DEV_MAX_NUM_VLAN   equal to VLAN_DEV_MAX_NUM_VLAN.
 */
#define MRP_VLAN_DEV_MAX_NUM_VLAN   (VLAN_DEV_MAX_NUM_VLAN / 2)

#define MRP_MAX_MVRP_ATTR_IN_SYS  MRP_VLAN_DEV_MAX_NUM_VLAN
#define MRP_MAX_MMRP_ATTR_IN_SYS  256
                                       /* This includes the multicast & unicast
                                        * MAC addresses that needs to be 
                                        * register in the MMRP.
                                        * Tunable by user */ 


 /* IVR sizing params */
  
#define MAX_CFA_IVR_MAPPING_ENTRIES (VLAN_MAX_ISOLATED_VLANS+VLAN_MAX_COMMUNITY_VLANS)

/*Memory Block Count */
/*tMrpQMsg*/
#define MAX_MRP_MSG_Q_DEPTH        (MRP_MAX_PORTS_PER_CONTEXT * 4)
/*tMrpBulkQMsg*/
#define MAX_MRP_BULK_MSG           4
/*tMrpRxPduQMsg*/
#define MAX_MRP_PDU_Q_DEPTH        (MRP_MAX_PORTS_PER_CONTEXT * 2)

/*tMrpContextInfo*/
#define MAX_MRP_CONTEXTS          (SYS_DEF_MAX_NUM_CONTEXTS+1)

/*tMrpVidMapQMsg*/
#define MAX_MRP_BULK_VID_MAP_QMSG    (AST_MAX_MST_INSTANCES * \
                                      (VLAN_DEV_MAX_NUM_VLAN/VLAN_MAP_INFO_PER_POST))

/*tMrpAppPortEntry*/
#define MAX_MRP_MEM_APP_PORT_ENTRIES_IN_SYSTEM   (MAX_MRP_PORT_ENTRIES_IN_SYSTEM*2)

/*tMrpStatsEntry*/
#define MAX_MRP_STATS_ENTRIES_IN_SYSTEM   (MAX_MRP_PORT_ENTRIES_IN_SYSTEM*2)

/*tMrpMapEntry*/
#define MAX_MRP_MAP_ENTRIES_IN_SYS ((AST_MAX_MST_INSTANCES * \
                                    MRP_MAX_NUM_CONTEXTS) + \
                                    MRP_VLAN_DEV_MAX_NUM_VLAN)
/*tMrpMvrpPortAttrInfoList*/
#define MAX_MRP_MVRP_MAP_PORTS_IN_SYS     (MAX_MRP_PORT_ENTRIES_IN_SYSTEM * \
                                   AST_MAX_MST_INSTANCES)
/*tMrpMmrpPortAttrInfoList*/
#define MAX_MRP_MMRP_MAP_PORTS_IN_SYS     (MRP_MAX_MMRP_PORTS * \
                                    MRP_MAX_MMRP_MAPS_PER_PORT)
/*tMrpMapPortEntry*/
#define MAX_MRP_MAP_PORT_ENTRIES   (MAX_MRP_MVRP_MAP_PORTS_IN_SYS + \
                                    MAX_MRP_MMRP_MAP_PORTS_IN_SYS)
/*tMrpAttrEntry*/
#define MAX_MRP_ATTR_ENTRIES_IN_SYS (MRP_MAX_MVRP_ATTR_IN_SYS + \
                                     MRP_MAX_MMRP_ATTR_IN_SYS)
/*UINT1[MRP_MAX_PDU_LEN]*/
#define MAX_MRP_PDU_BUFFER_COUNT    1
/*tMrpMvrpAttrArrayEntry*/
#define MAX_MRP_MVRP_MAPS_IN_SYS    (AST_MAX_MST_INSTANCES * \
                                       MRP_MAX_NUM_CONTEXTS)
                                        /* This is the Max. MSTP 
                                           instances in the system.
                                           The MVRP attrs are propagated
                                           to the ports belongs to this
                                           Instances */

/* No. of MMRP attributes is always less the no. of MMRP Maps.
 * Actual formula:
 * #define MAX_MRP_MMRP_MAPS_IN_SYS \
 * {\
 * (MRP_MAX_MMRP_ATTR_IN_SYS < MRP_VLAN_DEV_MAX_NUM_VLAN)?\
 * MRP_MAX_MMRP_ATTR_IN_SYS: MRP_VLAN_DEV_MAX_NUM_VLAN.
                                        */
/*tMrpMmrpAttrArrayEntry*/
#define MAX_MRP_MMRP_MAPS_IN_SYS   (VLAN_DEV_MAX_NUM_VLAN / 2)

/*tMrpPortMvrpPeerMacTable*/
#define MAX_MRP_MVRP_PEER_MAC_TABLE  MAX_MRP_PORT_ENTRIES_IN_SYSTEM
/*tMrpPortMmrpPeerMacTable*/
#define MAX_MRP_MMRP_PEER_MAC_TABLE  MRP_MAX_MMRP_PORTS
/*tMrpPortMvrpAttrList*/
#define MAX_MRP_PORT_ATTR_TABLE      MAX_MRP_PORT_ENTRIES_IN_SYSTEM
/*tMrpMvrpLvBitListTbl*/
#define MAX_MRP_MVRP_LV_BIT_LIST_COUNT (MAX_MRP_MVRP_MAP_PORTS_IN_SYS*3)
/*tMrpMmrpLvBitListTbl*/
#define MAX_MRP_MMRP_LV_BIT_LIST_COUNT (MRP_MAX_MMRP_PORTS*3)
/*tMrpMvrpMapTable*/
#define MAX_MRP_MVRP_MAPS_BLKS_IN_SYS  MRP_MAX_NUM_CONTEXTS
/*tMrpMmrpMapTable*/
#define MAX_MRP_MMRP_MAPS_BLKS_IN_SYS  MRP_MAX_NUM_CONTEXTS
/*tMrpMmrpAttrIndexList*/
#define MAX_MRP_MMPR_APP_ATTR_ARRAY_BLKS    MRP_MAX_NUM_CONTEXTS

/* END - MRP SIZING PARAMS */

/* MPLS sizing params */
#ifdef MPLS_WANTED
/* MPLS , since the MPLS can be enabled on as many L3 interfaces */
#define SYS_DEF_MAX_MPLS_IFACES      IP_DEV_MAX_L3VLAN_INTF
/* MPLS TNL */
#define SYS_DEF_MAX_MPLS_TNL_IFACES  IP_DEV_MAX_REM_L3_IP_INTF  

#ifdef LSPP_WANTED

/* The number of blocks for MAX_LSPP_FSLSPPGLOBALCONFIGTABLE is 
 * incremented by one for handling memory allocation for Set ,IsSet 
 * and LsppFsLsppGlobalConfigTableCreateApi incase of 
 * SYS_DEF_MAX_NUM_CONTEXTS set to 1.
 */
 
#define MAX_LSPP_FSLSPPGLOBALCONFIGTABLE    (SYS_DEF_MAX_NUM_CONTEXTS + 2)
#define MAX_LSPP_FSLSPPGLOBALSTATSTABLE     (SYS_DEF_MAX_NUM_CONTEXTS + 2)
#define MAX_LSPP_FSLSPPPINGTRACETABLE       100
#define MAX_LSPP_FSLSPPECHOSEQUENCETABLE    500
#define MAX_LSPP_FSLSPPHOPTABLE             800
#define MAX_LSPP_ECHOMSG_BUFFER_BLK         2
#define MAX_LSPP_QUEUE_DEPTH                100
#define MAX_LSPP_OUTPUT_PARAMS              2
#define MAX_LSPP_INPUT_PARAMS               2
#define MAX_LSPP_PDU_BUF                    2
#define MAX_LSPP_MPLS_API_INPUT_BUF         2
#define MAX_LSPP_MPLS_API_OUTPUT_BUF        2
#define MAX_LSPP_BFD_REQ_PARAM_BUF          2

#endif


#else
#define SYS_DEF_MAX_MPLS_TNL_IFACES              0                
#define SYS_DEF_MAX_MPLS_IFACES                  0                
#endif

#ifdef RFC6374_WANTED
#define RFC6374_MAX_SEQUENCE_COUNT_PER_SESSION            5
#define RFC6374_MAX_SESSION_COUNT_PER_SERVICE             10
#define RFC6374_MAX_SERVICE_COUNT                         150
#define RFC6374_MAX_ENTRIES_IN_BUFFER_TABLE               RFC6374_MAX_SERVICE_COUNT \
    * RFC6374_MAX_SESSION_COUNT_PER_SERVICE\
    * RFC6374_MAX_SEQUENCE_COUNT_PER_SESSION
#define RFC6374_MAX_ENTRIES_IN_STATS_TABLE               RFC6374_MAX_SERVICE_COUNT \
    * RFC6374_MAX_SESSION_COUNT_PER_SERVICE
#define RFC6374_MAX_QUEUE_DEPTH                100
#define RFC6374_MAX_PDU_BUF                    2
#define RFC6374_MAX_OUTPUT_PARAMS              2
#define RFC6374_MAX_INPUT_PARAMS               2
#define RFC6374_MAX_MPLS_API_INPUT_BUF         2
#define RFC6374_MAX_MPLS_API_OUTPUT_BUF        2
#define RFC6374_MAX_MPLS_PATH_INFO             2
#endif

/* ARP */
#define   MAX_ARP_ENTRIES                          1000
#define   MAX_ARP_CONTEXTS        SYS_DEF_MAX_NUM_CONTEXTS
#define   MAX_ARP_PKT_QUE_SIZE                     1000

#define   MAX_ARP_RM_QUE_SIZE                    100
#define   MAX_ARP_ICCH_QUE_SIZE                    100
#define   MAX_ARP_VLAN_QUE_SIZE                    100
#define   MAX_ARP_DYN_MSG_SIZE                   100
#define   MAX_ARP_ENET_ENTRIES                   ARP_MAX_ENET_ENTRIES
/* The following MACROS are not related to mempool sizing */
#define   MAX_ARP_ENTRIES_LIMIT               SYS_DEF_MAX_NUM_CONTEXTS 

/* IP */
#define   MAX_IP_CONTEXTS                SYS_DEF_MAX_NUM_CONTEXTS
#define   MAX_IP_REASM_LISTS             20
#define   MAX_IP_FRAG_PKTS               200 
#define   MAX_IP_IPIF_QUE_DEPTH          (IP_DEV_MAX_L3VLAN_INTF*2)
#define   MAX_IP_BROADCAST_Q_DEPTH       10
#define   MAX_IP_PMTU_ENTRIES            50
#define   MAX_IP_LOGICAL_IFACES          150
#define   MAX_IP_IF_STATS                150
#define   MAX_IP_AGG_ROUTES              50
#define   MAX_IP_CIDR_EXPL_ROUTES        5 
#define   MAX_IP_TUNNEL_INTERFACES       20
#define   MAX_IP_INTERFACES              IP_IFACE_MAX_NODE_COUNT 
#define   MAX_UTIL_IP_LOCAL_IFACES_COUNT 1

/* Following MACROS donot have direct mapping to mempools. Need not be tuned for sizing */
#define   MAX_IP_CONTEXT_LIMIT          SYS_DEF_MAX_NUM_CONTEXTS
#define   IP_DEF_IGMP_MAX_MCAST_GROUPS  IP_DEV_L3MCAST_TABLE_SIZE
#define   IP_DEF_MAX_ROUTES             IP_DEV_MAX_ROUTE_TBL_SZ

/* RARP */
#define MAX_RARP_SERVER_ADDRESS_ENTRIES         1
/* UDP */
#define   MAX_UDP_CONTEXTS               SYS_DEF_MAX_NUM_CONTEXTS
#define   MAX_UDP_CB_TAB_SIZE            50
#define   MAX_UDP_CFG_MSGS               32
#define   MAX_UDP_HL_PARMS_MSGS          32

/* RTM */
#define   MAX_RTM_CONTEXTS               SYS_DEF_MAX_NUM_CONTEXTS
#define   MAX_RTM_RRD_CTRL_INFO          128
#define   MAX_RTM_ROUTE_TABLE_ENTRIES    7000
#define   MAX_RTM_COMM                   2 

/* Default memory reservation for routing protocols*/

#define   MAX_RTM_ROUTE_ENTRIES (FsRTMSizingParams[MAX_RTM_ROUTE_TABLE_ENTRIES_SIZING_ID].u4PreAllocatedUnits)

#define   DEF_RTM_BGP_ROUTE_ENTRIES      ((MAX_RTM_ROUTE_ENTRIES - SYS_DEF_MAX_INTERFACES)/5)
#define   DEF_RTM_OSPF_ROUTE_ENTRIES      ((MAX_RTM_ROUTE_ENTRIES - SYS_DEF_MAX_INTERFACES)/5)
#define   DEF_RTM_RIP_ROUTE_ENTRIES      ((MAX_RTM_ROUTE_ENTRIES - SYS_DEF_MAX_INTERFACES)/5)
#define   DEF_RTM_STATIC_ROUTE_ENTRIES      ((MAX_RTM_ROUTE_ENTRIES -SYS_DEF_MAX_INTERFACES)/5)
#define   DEF_RTM_ISIS_ROUTE_ENTRIES      ((MAX_RTM_ROUTE_ENTRIES -SYS_DEF_MAX_INTERFACES)/5)


#define   MAX_RTM6_ROUTE_ENTRIES (FsRTM6SizingParams[MAX_RTM6_ROUTE_TABLE_ENTRIES_SIZING_ID].u4PreAllocatedUnits)

#define   DEF_RTM6_BGP_ROUTE_ENTRIES      ((MAX_RTM6_ROUTE_ENTRIES - SYS_DEF_MAX_INTERFACES)/5)
#define   DEF_RTM6_OSPF_ROUTE_ENTRIES      ((MAX_RTM6_ROUTE_ENTRIES - SYS_DEF_MAX_INTERFACES)/5)
#define   DEF_RTM6_RIP_ROUTE_ENTRIES      ((MAX_RTM6_ROUTE_ENTRIES - SYS_DEF_MAX_INTERFACES)/5)
#define   DEF_RTM6_STATIC_ROUTE_ENTRIES      ((MAX_RTM6_ROUTE_ENTRIES -SYS_DEF_MAX_INTERFACES)/5)
#define   DEF_RTM6_ISIS_ROUTE_ENTRIES      ((MAX_RTM6_ROUTE_ENTRIES -SYS_DEF_MAX_INTERFACES)/5)


#define   MAX_RTM_REDIST_NODES           128
#define   MAX_NEXTHOP_ROUTES             400
#define   MAX_RTM_PEND_NEXTHOP           25 + MAX_NEXTHOP_ROUTES
#define   MAX_RTM_RESLV_NEXTHOP          25
#define   MAX_RTM_PEND_RT_ENTRIES        700  
#define   MAX_RTM_ROUTE_MSG              1000
#define   MAX_RTM_APP_SPEC_INFO          3
                                        /* MAX_RTM_ROUTE_TABLE_ENTRIES/10 */

#define   MAX_RTM_RM_QUE_SIZE            100
#define   MAX_RTM_DYN_MSG_SIZE           200
/* The following MACROS are not related to mempool sizing */
#define  RTM_MAX_CONTEXT_LMT              SYS_DEF_MAX_NUM_CONTEXTS 
#define   IP_DEF_IGMP_MAX_MCAST_GROUPS   IP_DEV_L3MCAST_TABLE_SIZE
#define   IP_DEF_MAX_ROUTES              IP_DEV_MAX_ROUTE_TBL_SZ

/* The following macros are used in PW supported modules to have
 * interfaces count included with PW and AC ifaces.
 * Later this definition has to be modified with sizing variable.
 * NOTE: While doing so, it is to be taken care for sizing macros. */
#define CFA_SYS_DEF_MAX_INTERFACES          SYS_DEF_MAX_INTERFACES_EXT
#define BRG_SYS_DEF_MAX_INTERFACES_EXT      SYS_DEF_MAX_INTERFACES_EXT
/* for following macro, SYS_DEF_MAX_INTERFACES_EXT + SYS_DEF_MAX_SISP_IFACES */
#define SNOOP_SYS_DEF_MAX_INTERFACES        256
/* for follwoing macro, value is SYS_DEF_MAX_INTERFACES_EXT */
#define RMAP_SYS_DEF_MAX_INTERFACES         SYS_DEF_MAX_INTERFACES
#define NPSIM_SYS_DEF_MAX_INTERFACES        SYS_DEF_MAX_INTERFACES_EXT
#define ERPS_SYS_DEF_MAX_INTERFACES         SYS_DEF_MAX_INTERFACES_EXT

/* The following macros are used in PW supported modules to have
 * interfaces count included wih PW and AC ifaces.
 * This macro should be used only in arrays.*/
#define CFA_STATIC_SYS_DEF_MAX_INTERFACES   SYS_DEF_MAX_INTERFACES_EXT
#define SNOOP_STATIC_SYS_DEF_MAX_INTERFACES (SYS_DEF_MAX_INTERFACES_EXT + \
                                             SYS_DEF_MAX_SISP_IFACES)


/* WSSSTA */
#define MAX_WSSSTA_ASSOC_REJECTCOUNT      10

/* WEBAUTH */
#define MAX_USER_COUNT    MAX_STA_SUPP_PER_WLC

/* WSSCFG */

#define MAX_MULTIDOMAIN_INFO_ELEMENT 15

#define MAX_WSSCFG_TEMP_DB_COUNT           10

#define MAX_WSSCFG_DOT11STATIONCONFIGTABLE       10

#define MAX_WSSCFG_DOT11STATIONCONFIGTABLE_ISSET       10

#define MAX_WSSCFG_DOT11AUTHENTICATIONALGORITHMSTABLE       10

#define MAX_WSSCFG_DOT11AUTHENTICATIONALGORITHMSTABLE_ISSET       10

#define MAX_WSSCFG_DOT11WEPDEFAULTKEYSTABLE       10

#define MAX_WSSCFG_DOT11WEPDEFAULTKEYSTABLE_ISSET       10

#define MAX_WSSCFG_DOT11WEPKEYMAPPINGSTABLE       10

#define MAX_WSSCFG_DOT11WEPKEYMAPPINGSTABLE_ISSET       10

#define MAX_WSSCFG_DOT11PRIVACYTABLE       10

#define MAX_WSSCFG_DOT11PRIVACYTABLE_ISSET       10

#define MAX_WSSCFG_DOT11MULTIDOMAINCAPABILITYTABLE       10

#define MAX_WSSCFG_DOT11MULTIDOMAINCAPABILITYTABLE_ISSET       10

#define MAX_WSSCFG_DOT11SPECTRUMMANAGEMENTTABLE       10

#define MAX_WSSCFG_DOT11SPECTRUMMANAGEMENTTABLE_ISSET       10

#define MAX_WSSCFG_DOT11REGULATORYCLASSESTABLE       10

#define MAX_WSSCFG_DOT11REGULATORYCLASSESTABLE_ISSET       10

#define MAX_WSSCFG_DOT11OPERATIONTABLE       10

#define MAX_WSSCFG_DOT11OPERATIONTABLE_ISSET       10

#define MAX_WSSCFG_DOT11COUNTERSTABLE       10

#define MAX_WSSCFG_DOT11GROUPADDRESSESTABLE       10

#define MAX_WSSCFG_DOT11GROUPADDRESSESTABLE_ISSET       10

#define MAX_WSSCFG_DOT11EDCATABLE       10

#define MAX_WSSCFG_DOT11EDCATABLE_ISSET       10

#define MAX_WSSCFG_DOT11QAPEDCATABLE       10

#define MAX_WSSCFG_DOT11QAPEDCATABLE_ISSET       10

#define MAX_WSSCFG_DOT11QOSCOUNTERSTABLE       10

#define MAX_WSSCFG_DOT11RESOURCEINFOTABLE       10

#define MAX_WSSCFG_DOT11RESOURCEINFOTABLE_ISSET       10

#define MAX_WSSCFG_DOT11PHYOPERATIONTABLE       10

#define MAX_WSSCFG_DOT11PHYOPERATIONTABLE_ISSET       10

#define MAX_WSSCFG_DOT11PHYANTENNATABLE       10

#define MAX_WSSCFG_DOT11PHYANTENNATABLE_ISSET       10

#define MAX_WSSCFG_DOT11PHYTXPOWERTABLE       10

#define MAX_WSSCFG_DOT11PHYTXPOWERTABLE_ISSET       10

#define MAX_WSSCFG_DOT11PHYFHSSTABLE       10

#define MAX_WSSCFG_DOT11PHYFHSSTABLE_ISSET       10

#define MAX_WSSCFG_DOT11PHYDSSSTABLE       10

#define MAX_WSSCFG_APGROUPTABLE          55

#define MAX_WSSCFG_APGROUPWTPTABLE       512

#define MAX_WSSCFG_APGROUPWLANTABLE      3000

#define MAX_WSSCFG_DOT11PHYDSSSTABLE_ISSET       10

#define MAX_WSSCFG_DOT11PHYIRTABLE       10

#define MAX_WSSCFG_DOT11PHYIRTABLE_ISSET       10

#define MAX_WSSCFG_DOT11REGDOMAINSSUPPORTEDTABLE       10

#define MAX_WSSCFG_DOT11REGDOMAINSSUPPORTEDTABLE_ISSET       10

#define MAX_WSSCFG_DOT11ANTENNASLISTTABLE       10

#define MAX_WSSCFG_DOT11ANTENNASLISTTABLE_ISSET       10

#define MAX_WSSCFG_DOT11SUPPORTEDDATARATESTXTABLE       10

#define MAX_WSSCFG_DOT11SUPPORTEDDATARATESTXTABLE_ISSET       10

#define MAX_WSSCFG_DOT11SUPPORTEDDATARATESRXTABLE       10

#define MAX_WSSCFG_DOT11SUPPORTEDDATARATESRXTABLE_ISSET       10

#define MAX_WSSCFG_DOT11PHYOFDMTABLE       10

#define MAX_WSSCFG_DOT11PHYOFDMTABLE_ISSET       10

#define MAX_WSSCFG_DOT11PHYHRDSSSTABLE       10

#define MAX_WSSCFG_DOT11PHYHRDSSSTABLE_ISSET       10

#define MAX_WSSCFG_DOT11HOPPINGPATTERNTABLE       10

#define MAX_WSSCFG_DOT11HOPPINGPATTERNTABLE_ISSET       10

#define MAX_WSSCFG_DOT11PHYERPTABLE       10

#define MAX_WSSCFG_DOT11PHYERPTABLE_ISSET       10

#define MAX_WSSCFG_DOT11RSNACONFIGTABLE       10

#define MAX_WSSCFG_DOT11RSNACONFIGTABLE_ISSET       10

#define MAX_WSSCFG_DOT11RSNACONFIGPAIRWISECIPHERSTABLE       10

#define MAX_WSSCFG_DOT11RSNACONFIGPAIRWISECIPHERSTABLE_ISSET       10

#define MAX_WSSCFG_DOT11RSNACONFIGAUTHENTICATIONSUITESTABLE       10

#define MAX_WSSCFG_DOT11RSNACONFIGAUTHENTICATIONSUITESTABLE_ISSET       10

#define MAX_WSSCFG_DOT11RSNASTATSTABLE       10

#define MAX_WSSCFG_DOT11RSNASTATSTABLE_ISSET       10

#define MAX_WSSCFG_CAPWAPDOT11WLANTABLE       MAX_NUM_OF_SSID_PER_WLC + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_WSSCFG_CAPWAPDOT11WLANTABLE_ISSET       MAX_NUM_OF_SSID_PER_WLC + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_WSSCFG_CAPWAPDOT11WLANBINDTABLE       NUM_OF_AP_SUPPORTED * \
                                                  (MAX_NUM_OF_SSID_PER_WLC + MAX_WSSCFG_TEMP_DB_COUNT)  

#define MAX_WSSCFG_CAPWAPDOT11WLANBINDTABLE_ISSET       NUM_OF_AP_SUPPORTED * \
                                                  (MAX_NUM_OF_SSID_PER_WLC + MAX_WSSCFG_TEMP_DB_COUNT)  

#define MAX_WSSCFG_FSDOT11STATIONCONFIGTABLE       MAX_NUM_OF_SSID_PER_WLC + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_WSSCFG_FSDOT11STATIONCONFIGTABLE_ISSET       MAX_NUM_OF_SSID_PER_WLC + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_WSSCFG_FSDOT11CAPABILITYPROFILETABLE       25

#define MAX_WSSCFG_FSDOT11CAPABILITYPROFILETABLE_ISSET       25

#define MAX_WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE       10

#define MAX_WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_ISSET       10

#define MAX_WSSCFG_FSSECURITYWEBAUTHGUESTINFOTABLE       100

#define MAX_WSSCFG_FSSECURITYWEBAUTHGUESTINFOTABLE_ISSET       100

#define MAX_WSSCFG_FSSECURITYWEBAUTHCREDENTIALSTABLE       10

#define MAX_WSSCFG_FSSECURITYWEBAUTHCREDENTIALSTABLE_ISSET       10

#define MAX_WSSCFG_FSSTATIONQOSPARAMSTABLE       10

#define MAX_WSSCFG_FSSTATIONQOSPARAMSTABLE_ISSET       10

#define MAX_WSSCFG_FSVLANISOLATIONTABLE       10

#define MAX_WSSCFG_FSVLANISOLATIONTABLE_ISSET       10

#define MAX_WSSCFG_FSDOT11RADIOCONFIGTABLE       MAX_NUM_OF_RADIOS + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_WSSCFG_FSDOT11RADIOCONFIGTABLE_ISSET       MAX_NUM_OF_RADIOS + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_WSSCFG_FSDOT11QOSPROFILETABLE       17

#define MAX_WSSCFG_FSDOT11QOSPROFILETABLE_ISSET       17

#define MAX_WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE       MAX_NUM_OF_SSID_PER_WLC + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_ISSET       MAX_NUM_OF_SSID_PER_WLC + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE       MAX_NUM_OF_SSID_PER_WLC + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_ISSET       MAX_NUM_OF_SSID_PER_WLC + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_WSSCFG_FSDOT11WLANQOSPROFILETABLE       MAX_NUM_OF_SSID_PER_WLC + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_WSSCFG_FSDOT11WLANQOSPROFILETABLE_ISSET       MAX_NUM_OF_SSID_PER_WLC + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_WSSCFG_FSDOT11QAPTABLE       10

#define MAX_WSSCFG_FSDOT11QAPTABLE_ISSET       10

#define MAX_WSSCFG_FSQAPPROFILETABLE       100

#define MAX_WSSCFG_FSQAPPROFILETABLE_ISSET       100

#define MAX_WSSCFG_FSDOT11RADIOQOSTABLE       MAX_NUM_OF_RADIOS * 4  + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_WSSCFG_FSDOT11RADIOQOSTABLE_ISSET       MAX_NUM_OF_RADIOS * 4 + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE       MAX_NUM_OF_SSID_PER_WLC + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_ISSET       MAX_NUM_OF_SSID_PER_WLC + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_WSSCFG_FSDOT11AUTHMAPPINGTABLE       MAX_NUM_OF_SSID_PER_WLC + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_WSSCFG_FSDOT11AUTHMAPPINGTABLE_ISSET       MAX_NUM_OF_SSID_PER_WLC + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_WSSCFG_FSDOT11QOSMAPPINGTABLE       MAX_NUM_OF_SSID_PER_WLC + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_WSSCFG_FSDOT11QOSMAPPINGTABLE_ISSET       MAX_NUM_OF_SSID_PER_WLC + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_WSSCFG_FSDOT11CLIENTSUMMARYTABLE    200

#define MAX_WSSCFG_FSDOT11EXTERNALWEBAUTHPROFILETABLE_ISSET 3

#define  MAX_WSSCFG_FSDOT11EXTERNALWEBAUTHPROFILETABLE 3

#define MAX_WSSCFG_FSDOT11ANTENNASLISTTABLE       10

#define MAX_WSSCFG_FSDOT11ANTENNASLISTTABLE_ISSET       10

#define MAX_WSSCFG_FSDOT11WIRELESSBINDINGTABLE       10

#define MAX_WSSCFG_FSDOT11WIRELESSBINDINGTABLE_ISSET       10

#define MAX_WSSCFG_FSDOT11WLANTABLE      MAX_NUM_OF_SSID_PER_WLC + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_WSSCFG_FSDOT11WLANTABLE_ISSET      MAX_NUM_OF_SSID_PER_WLC + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_WSSCFG_FSDOT11WLANBINDTABLE       NUM_OF_AP_SUPPORTED * \
                                             (MAX_NUM_OF_SSID_PER_WLC + MAX_WSSCFG_TEMP_DB_COUNT)  

#define MAX_WSSCFG_FSDOT11WLANBINDTABLE_ISSET       NUM_OF_AP_SUPPORTED * \
                                             (MAX_NUM_OF_SSID_PER_WLC + MAX_WSSCFG_TEMP_DB_COUNT)  

#define MAX_WSSCFG_FSDOT11NCONFIGTABLE       100

#define MAX_WSSCFG_FSDOT11NCONFIGTABLE_ISSET       100

#define MAX_WSSCFG_FSDOT11NMCSDATARATETABLE        100

#define MAX_WSSCFG_FSDOT11NMCSDATARATETABLE_ISSET       100

#define MAX_WSSCFG_FSWTPIMAGEUPGRADETABLE       10

#define MAX_WSSCFG_FSWTPIMAGEUPGRADETABLE_ISSET       10

#define MAX_WSSCFG_FSRRMCONFIGTABLE       10

#define MAX_WSSCFG_FSRRMCONFIGTABLE_ISSET       10

#define MAX_WSSCFG_DOT11SUPPORTEDCOUNTRYTABLE   200

/* CAPWAP */
#define MAX_CAPWAP_FSWTPMODELTABLE       8

#define MAX_CAPWAP_FSWTPMODELTABLE_ISSET       8

#define MAX_CAPWAP_FSWTPRADIOTABLE       350

#define MAX_CAPWAP_FSWTPRADIOTABLE_ISSET       350

#define MAX_CAPWAP_FSCAPWAPWHITELISTTABLE       25

#define MAX_CAPWAP_FSCAPWAPWHITELISTTABLE_ISSET       25

#define MAX_CAPWAP_FSCAPWAPBLACKLIST       25

#define MAX_CAPWAP_FSCAPWAPBLACKLIST_ISSET       25

#define MAX_CAPWAP_FSCAPWAPWTPCONFIGTABLE       NUM_OF_AP_SUPPORTED  + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_CAPWAP_FSCAPWAPWTPCONFIGTABLE_ISSET       NUM_OF_AP_SUPPORTED  + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE       (NUM_OF_AP_SUPPORTED * 2) + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_ISSET       (NUM_OF_AP_SUPPORTED * 2)  + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE       10

#define MAX_CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_ISSET       10

#define MAX_CAPWAP_FSCAPWAPDNSPROFILETABLE       NUM_OF_AP_SUPPORTED  + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_CAPWAP_FSCAPWAPDNSPROFILETABLE_ISSET       NUM_OF_AP_SUPPORTED  + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_CAPWAP_FSWTPNATIVEVLANIDTABLE       NUM_OF_AP_SUPPORTED  + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_CAPWAP_FSWTPNATIVEVLANIDTABLE_ISSET       NUM_OF_AP_SUPPORTED  + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_CAPWAP_FSWTPLOCALROUTINGTABLE       NUM_OF_AP_SUPPORTED  + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_CAPWAP_FSWTPLOCALROUTINGTABLE_ISSET       NUM_OF_AP_SUPPORTED  + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_CAPWAP_FSCAWAPDISCSTATSTABLE       NUM_OF_AP_SUPPORTED  + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_CAPWAP_FSCAWAPDISCSTATSTABLE_ISSET       NUM_OF_AP_SUPPORTED  + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_CAPWAP_FSCAWAPJOINSTATSTABLE       NUM_OF_AP_SUPPORTED  + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_CAPWAP_FSCAWAPJOINSTATSTABLE_ISSET       NUM_OF_AP_SUPPORTED  + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_CAPWAP_FSCAWAPCONFIGSTATSTABLE       NUM_OF_AP_SUPPORTED  + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_CAPWAP_FSCAWAPCONFIGSTATSTABLE_ISSET       NUM_OF_AP_SUPPORTED  + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_CAPWAP_FSCAWAPRUNSTATSTABLE       NUM_OF_AP_SUPPORTED  + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_CAPWAP_FSCAWAPRUNSTATSTABLE_ISSET       NUM_OF_AP_SUPPORTED  + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE       MAX_NUM_OF_RADIOS + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_ISSET       MAX_NUM_OF_RADIOS + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_CAPWAP_FSCAPWAPSTATIONWHITELIST       10

#define MAX_CAPWAP_FSCAPWAPSTATIONWHITELIST_ISSET       10

#define MAX_CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE       NUM_OF_AP_SUPPORTED  + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_ISSET       NUM_OF_AP_SUPPORTED  + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_CAPWAP_FSCAPWAPWTPRADIOSTATISTICSTABLE       MAX_NUM_OF_RADIOS + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_CAPWAP_FSCAPWAPWTPRADIOSTATISTICSTABLE_ISSET       MAX_NUM_OF_RADIOS + MAX_WSSCFG_TEMP_DB_COUNT

#if defined (WLC_WANTED) || defined (WTP_WANTED)
#define SYS_DEF_MAX_INTERFACES          (SYS_DEF_MAX_PHYSICAL_INTERFACES \
                                  + LA_MAX_AGG_INTF + \
                         IP_DEV_MAX_L3VLAN_INTF + \
                                        SYS_DEF_MAX_TUNL_IFACES + \
                                        SYS_DEF_MAX_MPLS_IFACES + \
                                        SYS_DEF_MAX_MPLS_TNL_IFACES + \
                                        SYS_MAX_LOOPBACK_INTERFACES +\
                                        SYS_MAX_PPP_INTERFACES + \
                                        SYS_DEF_MAX_ILAN_IFACES + \
                                        SYS_DEF_MAX_INTERNAL_IFACES + \
                                         SYS_DEF_MAX_NUM_OF_VPNC + \
                                         SYS_DEF_MAX_TELINK_INTERFACES + \
                                         SYS_DEF_MAX_L2_PSW_IFACES + \
                                        SYS_DEF_MAX_L2_AC_IFACES +\
                                         SYS_DEF_MAX_NVE_IFACES +\
                                         MAX_TAP_INTERFACES +\
                                        SYS_MAX_HDLC_INTERFACES + \
                                        SYS_DEF_MAX_SBP_IFACES +\
                                        SYS_DEF_MAX_L3SUB_IFACES+\
     SYS_DEF_MAX_WSS_IFACES)

#else
#define SYS_DEF_MAX_INTERFACES          (SYS_DEF_MAX_PHYSICAL_INTERFACES \
                                  + LA_MAX_AGG_INTF + \
                         IP_DEV_MAX_L3VLAN_INTF + \
                                        SYS_DEF_MAX_TUNL_IFACES + \
                                        SYS_DEF_MAX_MPLS_IFACES + \
                                        SYS_DEF_MAX_MPLS_TNL_IFACES + \
                                        SYS_MAX_LOOPBACK_INTERFACES +\
                                        SYS_MAX_PPP_INTERFACES + \
                                        SYS_DEF_MAX_ILAN_IFACES + \
                                        SYS_DEF_MAX_INTERNAL_IFACES + \
                                         SYS_DEF_MAX_NUM_OF_VPNC + \
                                         SYS_DEF_MAX_TELINK_INTERFACES + \
                                         SYS_DEF_MAX_L2_PSW_IFACES + \
                                         SYS_DEF_MAX_L2_AC_IFACES + \
                                         SYS_DEF_MAX_NVE_IFACES + \
     MAX_TAP_INTERFACES + \
                                        SYS_MAX_HDLC_INTERFACES + \
                                        SYS_DEF_MAX_L3SUB_IFACES +\
                                        SYS_DEF_MAX_SBP_IFACES)
#endif

/* Macro to calculate the maximum interfaces using the actual number of
 * physical interfaces in system
 */
#if defined (WLC_WANTED) || defined (WTP_WANTED)
#define SYS_DEF_NUM_INTERFACES          (SYS_DEF_NUM_PHYSICAL_INTERFACES \
                                         + LA_MAX_AGG_INTF + \
                                        IP_DEV_MAX_L3VLAN_INTF + \
                                        SYS_DEF_MAX_TUNL_IFACES + \
                                        SYS_DEF_MAX_MPLS_IFACES + \
                                        SYS_DEF_MAX_MPLS_TNL_IFACES + \
                                        SYS_MAX_LOOPBACK_INTERFACES +\
                                        SYS_MAX_PPP_INTERFACES + \
                                        SYS_DEF_MAX_ILAN_IFACES + \
                                        SYS_DEF_MAX_INTERNAL_IFACES + \
                                         SYS_DEF_MAX_NUM_OF_VPNC + \
                                         SYS_DEF_MAX_TELINK_INTERFACES + \
                                         SYS_DEF_MAX_L2_PSW_IFACES + \
                                        SYS_DEF_MAX_L2_AC_IFACES + \
                                         SYS_DEF_MAX_NVE_IFACES + \
                                         MAX_TAP_INTERFACES + \
                                        SYS_MAX_HDLC_INTERFACES + \
                                        SYS_DEF_MAX_SBP_IFACES +\
                                        SYS_DEF_MAX_L3SUB_IFACES +\
     SYS_DEF_MAX_WSS_IFACES) 
#else
#define SYS_DEF_NUM_INTERFACES          (SYS_DEF_NUM_PHYSICAL_INTERFACES \
                                         + LA_MAX_AGG_INTF + \
                                        IP_DEV_MAX_L3VLAN_INTF + \
                                        SYS_DEF_MAX_TUNL_IFACES + \
                                        SYS_DEF_MAX_MPLS_IFACES + \
                                        SYS_DEF_MAX_MPLS_TNL_IFACES + \
                                        SYS_MAX_LOOPBACK_INTERFACES +\
                                        SYS_MAX_PPP_INTERFACES + \
                                        SYS_DEF_MAX_ILAN_IFACES + \
                                        SYS_DEF_MAX_INTERNAL_IFACES + \
                                         SYS_DEF_MAX_NUM_OF_VPNC + \
                                         SYS_DEF_MAX_TELINK_INTERFACES + \
                                         SYS_DEF_MAX_L2_PSW_IFACES + \
                                         SYS_DEF_MAX_L2_AC_IFACES + \
                                         SYS_DEF_MAX_NVE_IFACES + \
      MAX_TAP_INTERFACES + \
                                         SYS_MAX_HDLC_INTERFACES + \
                                         SYS_DEF_MAX_SBP_IFACES +\
                                         SYS_DEF_MAX_L3SUB_IFACES)
#endif
#define  MAX_NUM_TRIE               1030

#define BRG_MAX_PHY_PLUS_LAG_INT_PORTS  (SYS_DEF_MAX_INTERFACES +\
      SYS_DEF_MAX_VIP_IFACES + \
      SYS_DEF_MAX_SISP_IFACES)

#define BRG_NUM_PHY_PLUS_LAG_INT_PORTS  (SYS_DEF_NUM_INTERFACES +\
      SYS_DEF_MAX_VIP_IFACES + \
      SYS_DEF_MAX_SISP_IFACES)

#if defined (WLC_WANTED) || defined (WTP_WANTED)
#define BRG_MAX_PHY_PLUS_LOG_PORTS  (SYS_DEF_MAX_PHYSICAL_INTERFACES \
                                       + LA_MAX_AGG_INTF \
                                       + (MAX_NUM_OF_BSSID_PER_RADIO * MAX_NUM_OF_RADIOS))

#define BRG_NUM_PHY_PLUS_LOG_PORTS  (SYS_DEF_NUM_PHYSICAL_INTERFACES \
                                     + LA_MAX_AGG_INTF \
                                     + (MAX_NUM_OF_BSSID_PER_RADIO * MAX_NUM_OF_RADIOS))
#else
#define BRG_MAX_PHY_PLUS_LOG_PORTS  (SYS_DEF_MAX_PHYSICAL_INTERFACES \
                                       + LA_MAX_AGG_INTF)

#define BRG_NUM_PHY_PLUS_LOG_PORTS  (SYS_DEF_NUM_PHYSICAL_INTERFACES \
                                     + LA_MAX_AGG_INTF)
#endif

#define SYS_DEF_MAX_INTERFACES_EXT      (SYS_DEF_MAX_INTERFACES + \
                                         SYS_DEF_MAX_L2_PSW_IFACES + \
                                         SYS_DEF_MAX_L2_AC_IFACES + \
                                         SYS_DEF_MAX_NVE_IFACES + \
                                         MAX_TAP_INTERFACES)

#define BRG_MAX_PHY_PLUS_LAG_INT_PORTS_EXT  (BRG_SYS_DEF_MAX_INTERFACES_EXT +\
      SYS_DEF_MAX_VIP_IFACES + \
      SYS_DEF_MAX_SISP_IFACES)

#define   BRG_PORT_LIST_SIZE_EXT ((BRG_MAX_PHY_PLUS_LAG_INT_PORTS_EXT + 31)/32 * 4)

#define BRG_NUM_PHY_PLUS_LAG_INT_PORTS_EXT  (SYS_DEF_MAX_INTERFACES_EXT +\
      SYS_DEF_MAX_VIP_IFACES + \
      SYS_DEF_MAX_SISP_IFACES)


#ifdef PBB_WANTED
#define PBB_MAX_PORTS_IN_SYSTEM                     1000
#endif

/* PBB-TE params */
#ifdef PBBTE_WANTED
/* Maximum number of ESPs that have an entry in the TESID table */
#define  PBBTE_MAX_TESID_ENTRIES          1500
/* Maximum number of unique {Destination Address, Vlan ID} pairs
 * needed for ESPs in the TESID table.
 * By default it is assumed to be same as the total TESID entries
 * If the number of unique DA, VID pairs is less than this
 * then this parameter can be reduced to save some memory */
#define  PBBTE_MAX_TESID_ESP_DA_VID_PAIRS 1500

#else /* Dummy values when PBB-TE is not compiled */
#define  PBBTE_MAX_TESID_ENTRIES         0
#define  PBBTE_MAX_TESID_ESP_DA_VID_PAIRS 0
#endif


#define   BRG_MAX_PHY_PORTS       (SYS_DEF_MAX_PHYSICAL_INTERFACES)

/* Actual number of physical interfaces present in the system */
#define   BRG_NUM_PHY_PORTS       (SYS_DEF_NUM_PHYSICAL_INTERFACES)

#define   BRG_PORT_LIST_SIZE ((BRG_MAX_PHY_PLUS_LAG_INT_PORTS + 31)/32 * 4)
    
#define   BRG_PHY_PORT_LIST_SIZE  ((BRG_MAX_PHY_PORTS + 31)/32 * 4)

/* Added with Attachment Circuit interfaces */
#define BRG_SYS_DEF_MAX_INTERFACES          SYS_DEF_MAX_INTERFACES
#if defined (WLC_WANTED) || defined (WTP_WANTED)
#define   CONTEXT_PORT_LIST_SIZE  (((SYS_DEF_MAX_PORTS_PER_CONTEXT + \
                                     SYS_DEF_MAX_L2_PSW_IFACES + \
                                     SYS_DEF_MAX_L2_AC_IFACES + \
                                     SYS_DEF_MAX_NVE_IFACES + \
          MAX_TAP_INTERFACES + \
                                     SYS_DEF_MAX_SBP_IFACES  + \
                                     SYS_DEF_MAX_L3SUB_IFACES +\
                                     MAX_NUM_OF_RADIOS +\
                                     MAX_NUM_OF_SSID_PER_WLC) + 31)/32 * 4)
#define   CONTEXT_PORT_LIST_SIZE_EXT  (((SYS_DEF_MAX_PORTS_PER_CONTEXT + \
                                     SYS_DEF_MAX_L2_PSW_IFACES + \
                                     SYS_DEF_MAX_L2_AC_IFACES + \
                                     SYS_DEF_MAX_NVE_IFACES + \
          MAX_TAP_INTERFACES + \
                                     SYS_DEF_MAX_SBP_IFACES  + \
                                     SYS_DEF_MAX_L3SUB_IFACES +\
                                     MAX_NUM_OF_RADIOS +\
                                     MAX_NUM_OF_SSID_PER_WLC) + 31)/32 * 4)

#define   UTIL_LOCAL_PORT_LIST_SIZE  (((SYS_DEF_MAX_PORTS_PER_CONTEXT + \
                                        SYS_DEF_MAX_L2_PSW_IFACES + \
                                        SYS_DEF_MAX_L2_AC_IFACES + \
                                        SYS_DEF_MAX_NVE_IFACES + \
          MAX_TAP_INTERFACES + \
                                        SYS_DEF_MAX_SBP_IFACES  + \
                                        SYS_DEF_MAX_L3SUB_IFACES +\
                                        MAX_NUM_OF_RADIOS + \
                                        MAX_NUM_OF_SSID_PER_WLC) + 31)/32 * 4)
#else
#define   CONTEXT_PORT_LIST_SIZE  (((SYS_DEF_MAX_PORTS_PER_CONTEXT + \
                                     SYS_DEF_MAX_L2_PSW_IFACES + \
                                     SYS_DEF_MAX_L2_AC_IFACES + \
                                     SYS_DEF_MAX_NVE_IFACES  + \
                                     SYS_DEF_MAX_L3SUB_IFACES +\
          MAX_TAP_INTERFACES + \
                                     SYS_DEF_MAX_SBP_IFACES) + 31)/32 * 4)
#define CONTEXT_PORT_LIST_SIZE_EXT  (((SYS_DEF_MAX_PORTS_PER_CONTEXT + \
                                    SYS_DEF_MAX_L2_PSW_IFACES + \
                                    SYS_DEF_MAX_L2_AC_IFACES + \
                                    SYS_DEF_MAX_NVE_IFACES  + \
                                    SYS_DEF_MAX_L3SUB_IFACES+\
         MAX_TAP_INTERFACES + \
                                    SYS_DEF_MAX_SBP_IFACES) + 31)/32 * 4)
#define   UTIL_LOCAL_PORT_LIST_SIZE  (((SYS_DEF_MAX_PORTS_PER_CONTEXT + \
                                        SYS_DEF_MAX_L2_PSW_IFACES + \
                                        SYS_DEF_MAX_L2_AC_IFACES + \
                                        SYS_DEF_MAX_NVE_IFACES  + \
                                        SYS_DEF_MAX_L3SUB_IFACES +\
          MAX_TAP_INTERFACES + \
                                        SYS_DEF_MAX_SBP_IFACES ) + 31)/32 * 4)
#endif

typedef UINT1 tPortList [BRG_PORT_LIST_SIZE];
typedef UINT1 tLocalPortList [CONTEXT_PORT_LIST_SIZE];

typedef UINT1 tPortListExt [BRG_PORT_LIST_SIZE_EXT];
typedef UINT1 tLocalPortListExt [CONTEXT_PORT_LIST_SIZE_EXT];
/* The following macro should ONLY be used for memory pool creation of 
 * tLocalPortList. This is included with PW and AC interfaces count.
 * This array should not be used anywhere other than this util memory
 * pool.
 */
#define   UTIL_BRG_PORT_LIST_SIZE   (((BRG_SYS_DEF_MAX_INTERFACES_EXT +\
                                    SYS_DEF_MAX_VIP_IFACES +  \
                                    SYS_DEF_MAX_SISP_IFACES)+ 31)/32 * 4)


typedef UINT1 tUtilPortList [UTIL_BRG_PORT_LIST_SIZE];
typedef UINT1 tUtilLocalPortList [UTIL_LOCAL_PORT_LIST_SIZE];

/* ERPS Sizing params */
#define ERPS_MIN_VLAN_GROUP_ID          0
#define ERPS_MAX_VLAN_GROUP_ID          (AST_MAX_MST_INSTANCES - 1)

#define MAX_ERPS_Q_MESG                     64
#define MAX_ERPS_CONTEXT_INFO               SYS_DEF_MAX_NUM_CONTEXTS
#define MAX_ERPS_RING_INFO                  32
#define MAX_ERPS_RING_VLAN_INFO             32
#define MAX_ERPS_RAPS_TX_INFO               1024
#define MAX_ERPS_SIMULTANEOUS_CFM_SIGNAL    1024
#define MAX_ERPS_MPLS_API_IN_INFO           1

#ifdef MBSM_WANTED
#define MAX_ERPS_MBSM_MESG                  8
#else
#define MAX_ERPS_MBSM_MESG                  1
#endif

#define MAX_ERPS_RED_NP_SYNC_ENTRIES        32
#define MAX_ERPS_SUB_RING_ENTRIES           31


#define MAX_ERPS_MPLS_API_OUT_INFO          2
#define MAX_ERPS_CLI_BUF_ENTRIES            5
#define MAX_ERPS_SUB_PORTLIST_COUNT         1
#define MAX_ERPS_SUB_PORT_ENTRIES           ERPS_MAX_SUB_PORT_ENTRIES

/*Memory needs to be allocated for two Subport lists, since there are two ring ports
 *  * in a single ring*/

#define MAX_ERPS_SUB_PORT_LIST_ENTRIES     (2*MAX_ERPS_RING_INFO)

/* Since 32 rings are supported in ERPS 32 blocks of this structure is needed*/

#define MAX_ERPS_LSP_PW_INFO_ENTRIES           32


/* Minimum number of sub ports required for ring. */ 
#define MIN_ERPS_DEF_SUB_PORTS           2

#define MAX_ERPS_SUB_PORT_LIST_SIZE     (MIN_ERPS_DEF_SUB_PORTS + \
                                         SYS_DEF_MAX_L2_PSW_IFACES)

#define MAX_ERPS_UTIL_PORT_ARRAY_COUNT   5

/* END - ERPS SIZING PARAMS */

/* START - Y1564 SIZING PARAMS*/
#define MAX_Y1564_SLA_REPORT_CONF_ENTRIES    15
#define MAX_Y1564_SLA_ENTRIES                10 * SYS_DEF_MAX_NUM_CONTEXTS
#define MAX_Y1564_TRAFFIC_PROFILES           10 * SYS_DEF_MAX_NUM_CONTEXTS
#define MAX_Y1564_SAC_ENTRIES                10 * SYS_DEF_MAX_NUM_CONTEXTS
#define MAX_Y1564_SERV_CONF_ENTRIES          10 * SYS_DEF_MAX_NUM_CONTEXTS
#define MAX_Y1564_CONF_TEST_REPORT_ENTRIES   10 * SYS_DEF_MAX_NUM_CONTEXTS * MAX_Y1564_SLA_REPORT_CONF_ENTRIES 
#define MAX_Y1564_PERF_TEST_REPORT_ENTRIES   10 * SYS_DEF_MAX_NUM_CONTEXTS
#define MAX_Y1564_PERF_TEST_ENTRIES          10 * SYS_DEF_MAX_NUM_CONTEXTS
#define MAX_Y1564_CONTEXT_INFO               SYS_DEF_MAX_NUM_CONTEXTS
#define MAX_Y1564_SLA_LIST_INFO              10
#define MAX_Y1564_STAT_ENTRIES               10
#define MAX_EXT_IN_PARAMS                    100
#define MAX_EXT_OUT_PARAMS                   100
#define MAX_EXT_REQ_PARAMS                   100
#define MAX_Y1564_Q_MESG                     10
/* END - Y1564 SIZING PARAMS */

/* START - ESAT SIZING PARAMS*/
#define MAX_ESAT_SLA_ENTRIES                10
#define MAX_ESAT_TRAFFIC_PROFILES           10
#define MAX_ESAT_SAC_ENTRIES                10
#define MAX_ESAT_STAT_ENTRIES               100
#define MAX_ESAT_Q_MESG                     10
/* END - ESAT SIZING PARAMS */


/* START - RFC2544 SIZING PARAMS*/

#define MAX_RFC2544_CONTEXT_ENTRIES            SYS_DEF_MAX_NUM_CONTEXTS
#define MAX_RFC2544_FRAMESIZES                 9
#define MAX_RFC2544_SLA_ENTRIES                10 * MAX_RFC2544_CONTEXT_ENTRIES
#define MAX_RFC2544_TRAF_PROF_ENTRIES          10 * MAX_RFC2544_CONTEXT_ENTRIES
#define MAX_RFC2544_SAC_ENTRIES                10 * MAX_RFC2544_CONTEXT_ENTRIES
#define MAX_RFC2544_STATS_ENTRIES              100 * MAX_RFC2544_CONTEXT_ENTRIES
#define MAX_RFC2544_TH_TEST_STATS              20 * MAX_RFC2544_SLA_ENTRIES * MAX_RFC2544_FRAMESIZES
#define MAX_RFC2544_FL_TEST_STATS              200 * MAX_RFC2544_SLA_ENTRIES * MAX_RFC2544_FRAMESIZES
#define MAX_RFC2544_LA_TEST_STATS              20 * MAX_RFC2544_SLA_ENTRIES * MAX_RFC2544_FRAMESIZES
#define MAX_RFC2544_BB_TEST_STATS              100 * MAX_RFC2544_SLA_ENTRIES * MAX_RFC2544_FRAMESIZES
#define MAX_RFC2544_Q_MESG                     10 * MAX_RFC2544_CONTEXT_ENTRIES
#define MAX_RFC2544_EXTINPARAMS                10 * MAX_RFC2544_CONTEXT_ENTRIES
#define MAX_RFC2544_EXTOUTPARAMS                10 * MAX_RFC2544_CONTEXT_ENTRIES

/* END - RFC2544 SIZING PARAMS */


/*PNAC*/
#define   PNAC_MAXSUPP                        256

/* This tunable parameter is used to control the maximum number
   of Supplicants that can be connected to a port
   in Mac based Authentication */

#define   PNAC_MAX_SUPP_FOR_PORT  PNAC_MAXSUPP

#if defined (RSNA_WANTED) || defined (WLC_WANTED)
#define   PNAC_MAXPORTS             (BRG_NUM_PHY_PORTS + LA_MAX_AGG_INTF + CFA_MAX_WSS_BSSID + 1)
           /* Max Port Increased by
            1 to accomodate Virtual
            Interface Created to 
            Handle PreAuth 
            Sessions + Wap interfaces in WSS */
#else
#define   PNAC_MAXPORTS             BRG_NUM_PHY_PORTS
#endif

/* Since PNAC_MAX_AUTHSESS is used only in the macros which are in turn used
 * for the creation os memory pools, PNAC_MAX_AUTHSESS is modified to use
 * PNAC_MAXPORTS_IN_SYSTEM
 */
#ifdef WLAN_WANTED
#define   PNAC_MAX_SESS_IN_PROGRESS PNAC_MAXPORTS + WLAN_MAX_STATIONS

#define  PNAC_MAX_AUTHSESS  PNAC_MAXSUPP
    
#else
#define  PNAC_MAX_AUTHSESS PNAC_MAXSUPP

#define   PNAC_MAX_SESS_IN_PROGRESS PNAC_MAXPORTS
#endif /*WLAN_WANTED*/

/* currently, failed authsessions are mapped to maximum sessions supported by
 * PNAC module */
#define   PNAC_MAX_FAILED_AUTHSESS  PNAC_MAX_AUTHSESS

/* PNAC Sizing Params */

#define MAX_PNAC_AUTH_SESSION_NODE              PNAC_MAX_AUTHSESS
#define MAX_PNAC_SUPP_SESSION_NODE              PNAC_MAXPORTS
#define MAX_PNAC_NP_FAILED_MAC_SESSION_NODE     PNAC_MAX_FAILED_AUTHSESS
#define MAX_PNAC_SERV_USER_INFO                 64
#define MAX_PNAC_AUTH_SERVER_CACHE              PNAC_MAX_AUTHSESS
#define MAX_PNAC_INTF_QUEUE_MESG                50
#define MAX_PNAC_CONFIG_QUEUE_MESG              (PNAC_MAXPORTS * 4)
#define MAX_PNAC_TIMER_BLOCKS                   (7 * PNAC_MAX_AUTHSESS)
#define MAX_PNAC_EAPOL_FRAMES                   1522
#define MAX_PNAC_TAC_MSG_INPUT                  1
#define MAX_PNAC_OID_LENGTH                     5
#define MAX_PNAC_ENET_EAP_ARRAY_SIZE            2
#define MAX_PNAC_IF_RECV_DATA                   2
#define MAX_PNAC_IF_SEND_DATA                   1
#define MAX_PNAC_USER_INFO                      1

#ifdef MBSM_WANTED
#define MAX_DPNAC_SLOTS_INFO                    MBSM_MAX_SLOTS
#else
#define MAX_DPNAC_SLOTS_INFO                    2
#endif

#define MAX_DPNAC_SLOT_ALL                     4294967295U 

#define MAX_DPNAC_PORTS_INFO                   (MAX_DPNAC_SLOTS_INFO* PNAC_MAXPORTS)

#define MAX_RSNA_AUTHSESSNODE_INFO              MAX_NUM_OF_STA_PER_WLC
#define MAX_RSNA_PORT_INFO                     (MAX_NUM_OF_SSID_PER_WLC+1)  /* The tuning contain two parts
                                                        * Part-1: 50 - Value of WLAN_IFINDEX_DB_MAX. 
                                                        * This shall be proportional to number of SSIDs
                                                        * Part-2: 1 - Used for internal implementation purpose. 
                                                        * Hence always +1, must be maintained when tuning this parameter*/
#define MAX_RSNA_PMKSA_NODE_INFO                MAX_NUM_OF_STA_PER_WLC
#define MAX_RSNA_SUPP_INFO                      MAX_NUM_OF_STA_PER_WLC
#define MAX_RSNA_SUPP_STATS_INFO                MAX_NUM_OF_STA_PER_WLC /*RSNA_MAX_SUPP_STATS*/
#define MAX_RSNA_WSS_INFO                       MAX_NUM_OF_STA_PER_WLC /*RSNA_MAX_SUPP*/

#define MAX_WSS_USERGROUP_INFO              5
#define MAX_WSS_USERROLE_INFO             256 /* TBD */
#define MAX_WSS_USERNAME_ACC_LIST_INFO    100
#define MAX_WSS_USERMAC_ACC_LIST_INFO     100
#define MAX_WSS_USERMAPPING_LIST_INFO     100
#define MAX_WSS_USERSESSION_INFO          MAX_NUM_OF_STA_PER_WLC /* TBD */
#define MAX_WSS_USERNAME_INFO             256 
#define MAX_WSS_USER_NOTIFY_INFO          100

/* END - PNAC SIZING PARAMS */

/* FSB SIZING PARAMS*/
#define MAX_FSB_FCOE_VLAN_ENTRIES         32
#define MAX_FSB_FIP_SESS_ENTRIES          256
#define MAX_FSB_FCOE_ENTRIES              256

#define MAX_FSB_CONTEXT                   SYS_DEF_MAX_NUM_CONTEXTS
#define MAX_FSB_INTF_ENTRIES              (MAX_FSB_FCOE_VLAN_ENTRIES * (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG_INTF + SYS_DEF_MAX_SBP_IFACES))
#define MAX_FSB_FCF_PER_VLAN              4
#define MAX_FSB_FCF_ENTRIES               (MAX_FSB_FCOE_VLAN_ENTRIES * MAX_FSB_FCF_PER_VLAN)
#define MAX_FSB_NOTIFY_PARAMS_INFO        1024 
#define MAX_FSB_FCOE_MAC_COUNT            2

#define MAX_FSB_DEFAULT_FILTERS           7
#define MAX_FSB_DEFAULT_VLAN_FILTER       2
#define MAX_FSB_SESSION_FILTERS           8
#define MAX_FSB_FILTER_ENTRIES            ((MAX_FSB_FCOE_VLAN_ENTRIES * MAX_FSB_DEFAULT_FILTERS)\
                                           + MAX_FSB_DEFAULT_VLAN_FILTER +\
                                           (MAX_FSB_FIP_SESS_ENTRIES * MAX_FSB_SESSION_FILTERS))

#define MAX_FSB_FCOE_MAC_ADDR             256
#define MAX_FSB_FCOE_MAC_SIZE             (MAX_FSB_FCOE_MAC_ADDR * MAC_ADDR_LEN)

#define MAX_FSB_SCHANNEL_FILTER_ENTRIES   1024
#ifdef MBSM_WANTED
#define MAX_FSB_MBSM_MSG                  MBSM_MAX_LC_SLOTS 
#else
#define MAX_FSB_MBSM_MSG                  1
#endif

/* RIP */
#define  MAX_RIP_IPIF_LOGICAL_IFACES     1000
#define  MAX_RIP_AGG_RT_ENTRIES          1000
#define  MAX_RIP_ROUTE_ENTRIES           1000
#define  MAX_RIP_LOCAL_ROUTE_ENTRIES     500
#define  MAX_RIP_ROUTE_NODES             1000
#define  MAX_RIP_QUE_DEPTH               1000
#define  MAX_RIP_INTERFACES              1000
#define  MAX_RIP_RESP_MSG_NUM            1000
#define  MAX_RIP_CONTEXT                 SYS_DEF_MAX_NUM_CONTEXTS
#define  MAX_RIP_PEERS                    100
/*Redistributed routes - set to the value of max routes learnt from RTM*/
#define  MAX_RIP_REDISTRIBUTE_ROUTES     1000
/*Max neighbors on each interface is 16 - added an approximtae value */
#define  MAX_RIP_NBRS                    1600
/* Max of 256 auth keys can be configured on each interface - added an approximtae value*/
#define  MAX_RIP_MD5_AUTH_KEYS           1000
/* Max of 256 auth keys can be configured on each interface - added an approximtae value*/
#define  MAX_RIP_CRYPTO_AUTH_KEYS           1000
/* 3 types of Routemap filters can be configured in each context*/
#define  MAX_RIP_RMAP_FILTERS            1000

#define  MAX_RIP_SUMMARY_ENTRIES         250
/* The following macros donot map to mempool */
#define MAX_RIP_CONTEXT_LIMIT      SYS_DEF_MAX_NUM_CONTEXTS 
#define MAX_RIP_RM_QUE_DEPTH       20
#define MAX_RIP6_RM_QUEUE_DEPTH    10

#ifdef NPAPI_WANTED

#define   BRIDGE_DEF_MAX_FDB            1    /* Learning is done in hardware
                                              * hence only one entry is
                                              * allocated
                                              */

#define   BRIDGE_DEF_MAX_FILTERS        1    /* Bridge Filters are not be used
                                              * Hence only one entry is
                                              * allocated
                                              */
#else

#define   BRIDGE_DEF_MAX_FDB            2000
#define   BRIDGE_DEF_MAX_FILTERS        200


#endif

/* The expected time duration that CFA will trigger higher layer
   modules about oper up indication. Unit (in seconds). */
#define   SYS_DEF_IF_UP_TIME            10 

/************************ Constants required for future use ******************/

/* MPLS */
#define   MPLS_DEF_MAX_TUNNELS                    10000

/* IP6 */
#define  MAX_IP6_INTERFACES_LIMIT  (SYS_DEF_MAX_PHYSICAL_INTERFACES \
                                    + LA_MAX_AGG_INTF + \
                                    MAX_IP_LOGICAL_IFACES + \
                                    SYS_DEF_MAX_TUNL_IFACES)
#define  MAX_IP6_ADDR_PROFILES_LIMIT   200
#define  MAX_IP6_CONTEXT_LIMIT         128
#define  MAX_IP6_REASM_ENTRIES_LIMIT   46 
#define  MAX_PING6_MAX_DST_LIMIT   5
#define  MAX_RTM6_CONTEXT_LIMIT        128
#define  MAX_IP6_INTERFACES            100
#define  MAX_IP6_RA_ENTRY              MAX_IP6_INTERFACES
#define  MAX_IP6_ICMP6_STATS_NODE      100
#define  MAX_IP6_TUNNEL_INTERFACES     20
#define  MAX_IP6_REASM_ENTRIES         46 * MAX_IP6_CONTEXT_LIMIT
#define  MAX_IP6_ADDR_PROFILES         200
#define  MAX_IP6_FRAG_QUEUES           46
#define  MAX_IP6_PARAMS                200
#define  MAX_IP6_LINK_LOCAL_ADDRESS    100
#define  MAX_IP6_UNICAST_ADDRESS       300
#define  MAX_IP6_MCAST_ADDRESS         500
#define  MAX_IP6_MAC_FILTER_NODE       800
#define  MAX_IP6_PREFIX_ADDRESS        300
#define  MAX_IP6_ND6_CACHE_ENTRIES     80 
#define MAX_IP6_ND6_HW_CACHE_ENTRIES   80
#define  MAX_IP6_ND6_CACHE_LAN_ENTRIES IP_DEV_MAX_L3VLAN_INTF + IP_MAX_RPORT
#define  MAX_IP6_ND6_PROXYLIST_ENTRIES 10
#define  MAX_IP6_PMTU_ENTRIES          10
#define  MAX_IP6_CONTEXT               128
#define  MAX_IP6_UDP6_PORT             25
#define  MAX_IP6_POLICY_TABLE_ENTRIES  50
#define  MAX_IP6_SRC_ADDR_ENTRIES      200
#define  MAX_IP6_DST_ADDR_ENTRIES      100
#define  PING6_MAX_DATA_SIZE           2080 
#define  MAX_ND6_QUEUE_MSG       100
#define  MAX_ND6_DYN_MSG         1000
#define  MAX_IP6_IF_RA_ENTRIES   IP6_MAX_IF_RA_ENTRIES
#define  MAX_IP6_ND6_NS_SRC     20
#define  MAX_ND6_SECURE_LBUF_SIZE      1024
#define  MAX_ND6_SECURE_LBUF           100
#define  MAX_IP6_ROUTE_INFO            100 

/* Buffer size added for allocating memory for unfragmented buffer processing
   and for allocating memory for linear buffer while copying contents from one
   CRU buf to another CRU buf */
#define  MAX_IP6_BUFFER_ENTRIES           3
#define  IP6_MAX_BUF_SIZE              1500
#define  MAX_IP6_UNFRAG_BUFFER         1
#define  IP6_MAX_UNFRAGMENTED_SIZE     1500
/* Added for mem pool configuration for RFC4007 */
#define  MAX_IP6_SCOPE_ZONES          450 
#define  MAX_IP6_INTERFACE_SCOPE_ZONE 450
/*Every ipv6 interface have link-local zone and interface-zone by default. 
 * For other scope zone, need to tune this macro. By default this can be 
 * 2*MAX_IP6_INTERFACES. L3 context is added for global scope zone at worst 
 * case it can be (11*MAX_IP6_INTERFACES + L3 context.)*/
#define  MAX_IP6_SCOPE_ZONES_VALID    ((MAX_IP6_INTERFACES * 2) + VCM_MAX_L3_CONTEXT)
#define  MAX_IP6_ANYCAST_DST               10
#define  MAX_IP6_ANYCAST_ND_CACHE_ENTRIES 10*MAX_IP6_ANYCAST_DST


#define  MAX_IP6_TUNL_ACCESS_LIST      15

#define  MAX_IP6_MC_REG_MSG            200


#define  MAX_PING6_MAX_DST         5
#define  MAX_IP6_ND_REDIRECT_DATA      1
#define  MAX_IP6HOST_ND6_ROUTES       10

#define  MAX_RTM6_ROUTE_TABLE_ENTRIES  (SYS_DEF_MAX_INTERFACES + 1500)
#define  MAX_RTM6_RRD6_CONTROL_INFO    128
#define  MAX_RTM6_REDIST_NODE          128
#define  MAX_RTM6_CONTEXT              128
#define  MAX_RTM6_PEND_NEXTHOP         125
#define  MAX_RTM6_RESLV_NEXTHOP        125
#define  MAX_RTM6_PEND_RT_ENTRIES      250
#define  MAX_RTM6_ALL_NEXTHOP_ENTRIES  400
#define  MAX_RTM6_COMM                 2 

#define  MAX_RTM6_ROUTE_MSG            15000
#define   MAX_RTM6_ND_MSG               2500


#define   MAX_RTM_FAILED_ROUTE           400
#define   MAX_RTM6_RM_QUE_SIZE            100
#define   MAX_RTM6_DYN_MSG_SIZE            100
#define   MAX_RTM6_FAILED_ROUTE         250

/* IPSECv6 */
#define  MAX_IPSEC6_MAX_SA             100
#define  MAX_IPSEC6_MAX_SELECTOR       (2* MAX_IPSEC6_MAX_SA)
#define  MAX_IPSEC6_MAX_ACCESS_LIST    MAX_IPSEC6_MAX_SA
#define  MAX_IPSEC6_MAX_POLICY         MAX_IPSEC6_MAX_SA
#define  MAX_IPSEC6_MAX_ASSOC_ENTRY    MAX_IPSEC6_MAX_SA
#define  MAX_IPSEC6_MAX_AH_KEYS        (2* MAX_IPSEC6_MAX_SA)
#define  MAX_IPSEC6_MAX_ESP_KEYS       (4* MAX_IPSEC6_MAX_SA)
#define  MAX_IPSEC6_BLOCK_SIZE         1000
#define  MAX_IPSEC6_MAX_DATA_BLOCKS    1000


/* RIPNG */
#define   MAX_RIP6_ROUTE_ENTRIES                  4096
#define   MAX_RIP6_RRD_ROUTES                     5000
#define   MAX_RIP6_IF_INFO_ENTRIES                RIP6_MAX_IF_INFO_ENTRIES
#define   MAX_RIP6_INST_IF_ENTRIES                RIP6_MAX_INST_IF_ENTRIES
#define   MAX_RIP6_IFACES             (BRG_MAX_PHY_PLUS_LOG_PORTS + MAX_IP6_INTERFACES + IP_MAX_RPORT)
#define   MAX_RIP6_INSTANCE                          1
#define   MAX_RIP6_IFACES_INSTANCE_MAP       MAX_RIP6_IFACES 
#define   MAX_RIP6_PROFILES                         100
#define   MAX_RIP6_PEERS                            10
/* Following MACROS are not directly related to mempool creation */
#define   MAX_RIP6_IFACES_LIMIT                    SYS_DEF_MAX_INTERFACES
#define   MAX_RIP6_PROFILES_LIMIT                  100
#define   MAX_RIP6_CONTEXTS_LIMIT                  SYS_DEF_MAX_NUM_CONTEXTS 
#define   MAX_RIP6_BUF                            2
/*  This parameter is used to allocate leaf nodes in trie.Trie is used by RTM
 *  and RIP module.In RTM Trie, all the routes from various protocols are there.
 *  In RIP Trie, routes learnt from RIP is stored. Aggregated routes are also 
 *  stored  in trie*/
#define   TRIE_MAX_RTS               (IP_DEV_MAX_ROUTE_TBL_SZ + (RIP_DEF_MAX_ROUTES * 2))
/* OSPF */
/*Maximum number of interfaces on a router on which OSPF is expected to be enabled
 * Example to support OSPF on 160 interfaces as MAX_OSPF_INTERFACES 160*/ 
 
#define MAX_OSPF_INTERFACES          512
#define MAX_OSPF_MD5_AUTH_KEYS       10
#define MAX_OSPF_NBRS_LIMIT          512
/* 1. Maximum number of OSPF neighbors that can be configured or learned
 * 2.Neighbors in OSPF can be (1) configured on interfaces like PTOMP and NBMA 
 * (2) dynamically learned on interfaces like PTOP, Broadcast and 
 * virtual neigbors on virtual links
 * 3. It is possible to learn more than one neighbor on an interface 
 * like in the case of broadcast, PTOMP and NBMA
 * 4. The maximum number of the neighbors that can be tuned by system.size 
 * is limited by a macro MAX_OSPF_NBRS_LIMIT. The default value of this macro is 512. 
 * In case more than 512 neighbors are required, then tuning of two macros 
 * would be required (1) MAX_OSPF_NBRS_LIMIT and (2) MAX_OSPF_NBRS */
#define MAX_OSPF_NBRS                100
/* 1. This is required for tuning th maximum number of LSA's description information. 
 * LSA description information is an internal structure to OSPF. 
 * 2. This is applicable only for the self originated LSAs
 * 3. On ASBR, mainly external LSAs will contribute to this number
 * 4. On ABR, mainly summary LSAs (Type3 and Type4) will contribute to this number
 * 5. On non-ABR and non-ASBR self originated router LSAs and network LSAs will contribute*/ 
#define MAX_OSPF_LSA_DESCRIPTORS     1000
/* 1. This shall be tuned based on the maximum number of self orginated 
 * summary LSAs (3 and 4) a router want to generate
 * 2. This is an implementaion parameter
 * 3. It applies only for the self originated LSAs
 * Example to support 500 OSPF_SUMMARY_PARAMS 500*/
#define MAX_OSPF_SUMMARY_PARAMS      500
/*Maximum number of OSPF route entries expected to be learned by a router
 * which includes Network route entries(Intra area route, Inter area route,
 * Type1/Type2 external route),ABR route entries and ASBR route entries
 * Example to support 10K OSPF routes as MAX_OSPF_RT_ENTRIES  20000*/
#define MAX_OSPF_RT_ENTRIES          2500
/* 1. Maximum number of paths
 * 2. This shall be minimum equal to = MAX_OSPF_RT_ENTRIES,
 * because each route entry will have at least one path
 * 3. ECMP routes are stored in next hop array in the path structure.
 * Hence no need to change this parameter based on number of ECMP routes OSPF want to calculate
 * 4. In few peculiar cases, it is possible that ABR route 
 * entries can be multiple paths through different areas.
 * In that case it is possible that more than one path will be there in a route entry.
 * But this is ignorable or require very small number to be added to the list
 * Example to support 500 OSPF route paths as MAX_OSPF_ROUTE_PATHS  500*/
#define MAX_OSPF_ROUTE_PATHS         5000
/* 1. Maximum number of external route entries re-distrubted 
 * to OSPF module from RTM on a router
 * 2. This is applicable only on ASBR router
 * Example to support 10K OSPF external routes as 
 * MAX_OSPF_EXT_ROUTES  10000*/
#define MAX_OSPF_EXT_ROUTES          2500
/* 1. Maximum number of candidate nodes
 * 2. This shall be minimum = MAX_AREAS * maximum number of nodes 
 * in an area assume all are point to point links
 * 3.  This shall be minimum = MAX_AREAS * 2 * maximum number of nodes 
 * in an area assume all are broadcat links
 * 4. This pool used during SPF calculation
 * Example to support 500 OSPF_CANDT_NODES as MAX_OSPF_CANDT_NODES  500*/
#define MAX_OSPF_CANDT_NODES         1000
/* 1. Maximum number of request nodes
 * 2. This pool is used during adjacency bring up time. 
 * Typically happens when new router is connted or interface goes down and comes up
 * 3. The request nodes shall be tuned based on maximum number of LSAs
 * 4. Do note that once adjancey is done, there will not be any request list used
 * Example to support 500 OSPF_CANDT_NODES as MAX_OSPF_REQ_NODES  500*/
#define MAX_OSPF_REQ_NODES           5000
#define MAX_OSPF_CONTEXTS            SYS_DEF_MAX_NUM_CONTEXTS
/*Maximum number of OSPF areas supported on a router
 * MAX_OSPF_AREAS               64*/
#define MAX_OSPF_AREAS               64
/* 1. Maximum number of host route entries
 * 2. On an interface it is possible to add "n" number of host route 
 * entries as defined in standard MIB
 * 3. This memory pool is a global pool
 * 4. It is possible that on many interfaces we may not have any host entries
 * 5. Hence tuning of this parameter shall be based on budgetting 
 * that we want to do for this parameter for the whole router
 * 6. This is required only on the router where want to configure the hosts
 * Example to support 500 OSPF_CANDT_NODES as MAX_OSPF_HOSTS  500*/
#define MAX_OSPF_HOSTS               100
/* 1. Maximum number of OSPF quueue messages
 * 2. The recommended value - 2* MAX_OSPF_INTERFACES
 * Example to support 2000 OSPF_CANDT_NODES as MAX_OSPF_QUE_MSGS  2000*/
#define MAX_OSPF_QUE_MSGS            1000
/* 1. The maximum number of opaque application supported. 
 * 2. Default value is 50
 * 3. It is required only when would like to run applications like OSPF-TE*/
#define MAX_OSPF_OPQ_APPS            50
/* 1. Maximum number of external address ranges want to be configured on a router
 * 2. This mainly applicable on ASBR router
 * 3. This corresponding to the MIB table - 
 * fsospf.mib - futOspfAsExternalAggregationTable
 * Example to support 2000 MAX_OSPF_EXT_ADDR_RANGES  as MAX_OSPF_EXT_ADDR_RANGES 2000*/
#define MAX_OSPF_EXT_ADDR_RANGES     1500
/* 1. Maximum number of DB nodes for external LSAs
 * 2. This shall be equal to the maximum number of external LSAs - MAX_OSPF_EXT_LSAS
 * Example to support 2000 MAX_OSPF_EXT_LSA_DB_NODES  as MAX_OSPF_EXT_LSA_DB_NODES 2000*/
#define MAX_OSPF_EXT_LSA_DB_NODES    5000
/* This is required for tuning the maximum number of OSPF redistribute routes
 * Example to support 3000 MAX_OSPF_REDISTRIBUTE_ROUTES  as MAX_OSPF_REDISTRIBUTE_ROUTES 3000*/
#define MAX_OSPF_REDISTRIBUTE_ROUTES 2500
/* 1. Maximum route cache size
 * 2. This is implementation specific pool
 * 3. When routes are distributed from RTM, this cache number is useful buffer
 * 4. Default value is 100
 * 5. Based on target platform situation, this number shall be tuned
 * Example to support 3000 MAX_OSPF_RTM_ROUTE_CACHE as MAX_OSPF_RTM_ROUTE_CACHE 3000*/
#define MAX_OSPF_RTM_ROUTE_CACHE     100
#define MAX_OSPF_LSAINFOS            5000
/* 1. Maximum number of opaque LSAs - Type9, Type10, Type11 LSAs
 * 2. This is applicable only when opaque applications are enabled
 * Example to support 300 MAX_OSPF_OPQ_LSAS as MAX_OSPF_OPQ_LSAS 300*/
#define MAX_OSPF_OPQ_LSAS            200
/* 1. Maximum number of low priority queue messages. 
 * 2. This queue is for the purpose of handling low priority queues
 * Example to support 700 MAX_OSPF_LOWPRIO_QUE_MSGS as MAX_OSPF_LOWPRIO_QUE_MSGS 700*/
#define MAX_OSPF_LOWPRIO_QUE_MSGS    500
/* 1. This is required for tuning the maximum number of router LSA info
 * 2. This is used graceful restart implementation
 * Example to support 10K MAX_OSPF_RTR_LSA_INFO as MAX_OSPF_RTR_LSA_INFO 10000*/
#define MAX_OSPF_RTR_LSA_INFO        2500
/* 1. This is required for tuning the maximum number of router LSA info
 * 2. This is used graceful restart implementation
 * Example to support 10K MAX_OSPF_NW_LSA_INFO as MAX_OSPF_NW_LSA_INFO 10000*/
#define MAX_OSPF_NW_LSA_INFO         2500
/* OSPF-Rmap Filter Sizing Params - This macro should be set to value of  MAX_OSPF_CONTEXTS */
/* Maximum number of route map filters supported*/
#define MAX_OSPF_RMAP_FILTER        256
/* OSPF LSA's Sizing Params - This macro should be set to MAX_LSAS_PER_AREA * MAX_AREAS*/
/* 1. Maximum number of LSAs that a router can learn in a topology
 * 2. This includes both self originated LSAs and LSAs flooded by other routers
 * 2. This shall be calculated based on all type of LSAs
 * 3. This allocation will happen from this pool, 
 * provided the LSA size is less than 1024 bytes
 * 4. Do note that type-3 , Type-4 summary LSAs, external LSAs, 
 * Type-7 LSAs size would be less than 1024 bytes
 * 5. The router LSA size can vary based on the number of links information + 
 * number of hosts information that a router LSA is carrying
 * 6. The network LSA size can vary based on the number of routers 
 * connected to a broadcast of NBMA network
 * 7. The related macros are
 * MAX_OSPF_LSAS_SZ256
 * MAX_OSPF_LSAS_SZ512
 * MAX_OSPF_LSAS_SZ1024
 * MAX_OSPF_LSAS_SZ2048
 * MAX_OSPF_LSAS_SZ4096
 * As name suggests the allocation happens based on the size of the LSA
 * 8. Type 7 NSSA LSAs also falls under this category
 * Example to support 10K OSPF route entries as 
 * MAX_OSPF_LSAS 10000*/
#define MAX_OSPF_LSAS               500
/* Added for scaling purpose */ 
/* This is required for tuning the maximum number of LSA with size of 256
 * Example to support 300 LSA with size of 256 as MAX_OSPF_LSAS_SZ256 300*/
#define MAX_OSPF_LSAS_SZ256         300
/* This is required for tuning the maximum number of LSA with size of 512.
 * Example to support 300 LSA with size of 512 as MAX_OSPF_LSAS_SZ512 300*/
#define MAX_OSPF_LSAS_SZ512         300
/* This is required for tuning the maximum number of LSA with size of 1024.
 * Example to support 300 LSA with size of 1024 as MAX_OSPF_LSAS_SZ1024 300*/
#define MAX_OSPF_LSAS_SZ1024        270
/* This is required for tuning the maximum number of LSA with size of 2048.
 * Example to support 100 LSA with size of 2048 as MAX_OSPF_LSAS_SZ2048 100*/
#define MAX_OSPF_LSAS_SZ2048        2
/*This is required for tuning the maximum number of LSA with size of 4096.
 * Example to support 100 LSA with size of 4096 as MAX_OSPF_LSAS_SZ4096 100*/
#define MAX_OSPF_LSAS_SZ4096        2 /* 1 for Router LSA allocation */
#define MAX_OSPF_VRF_SOCK           32 
/* This is required for tuning the maximum number of external LSA's flooded by other routers
 * This is applicable on all routers except the routers which are internal to STUB area
 * Example to support 1000 external LSAS as MAX_OSPF_EXT_LSAS    1000*/
#define MAX_OSPF_EXT_LSAS           2500
/* Redist-Registration Sizing Params - This macro should be set to value of  MAX_OSPF_CONTEXTS */
#define MAX_OSPF_REDIST_REG        256

/* Routing Table Sizing Params - This macro should be set to value of  MAX_OSPF_CONTEXTS */
/* 1. The maximum number ofrouting table instances. 
 * 2. This shall be set to OSPF_MAX_CONTEXTS
 * 3. This is applicable when OSPF is enabled on multiple router instances
 * Example to support 500 MAX_OSPF_ROUTING_TABLE_INSTANCE as 
 * MAX_OSPF_ROUTING_TABLE_INSTANCE 500*/
#define MAX_OSPF_ROUTING_TABLE_INSTANCE   256

/* The following macros donot map to mempool */
#define MAX_OSPF_CONTEXTS_LIMIT      SYS_DEF_MAX_NUM_CONTEXTS 
/* This is required for tuning the OSPF Que size*/
#define  OSPF_QUEUE_SIZE             400        /*  50 + MAX_IP_LOGICAL_IFACES * 2 */ 
/* This is required for tuning the OSPF low priority  Que size*/
#define  OSPF_LOW_PRIORITY_QUEUE_SIZE    (2 * OSPF_QUEUE_SIZE)

/* OSPFV3 */
#define  MAX_OSPFV3_AUTH_KEYS              20
#define  MAX_OSPFV3_AREAS                  30    /* areas */
#define  MAX_OSPFV3_CONTEXTS               SYS_DEF_MAX_NUM_CONTEXTS 
                                                 /* contexts */
#define  MAX_OSPFV3_FWD_ADDR               20    /* forwarding addresses */
#define  MAX_OSPFV3_ADDR_RANGES            10    /* area address range */
#define  MAX_OSPFV3_EXT_AGGR_RANGES        10    /* external address range */
#define  MAX_OSPFV3_QUEUE_SIZE             500   /* OSPF main queue size */
#define  MAX_OSPFV3_RM_QUEUE_SIZE          100   /* OSPF RM queue size */
#define  MAX_OSPFV3_PREFIXES               2500  /* ipv6 prefixes */
#define  MAX_OSPFV3_NBRS                   300   /* OSPF neighbors */
#define  MAX_OSPFV3_DB_NODES               6000  /* external LSA data base
                                                  * nodes */
#define  MAX_OSPFV3_NP_QUEUE_SIZE          1000
#define  MAX_OSPFV3_IFLEAK_STATUS_NODE     (MAX_OSPFV3_INTERFACES/2)

#define  MAX_OSPFV3_LP_QUEUE_SIZE          200
#define  MAX_OSPFV3_LSAS                   6000  /* LSA */
#define  MAX_OSPFV3_INTERFACES             100   /* normal and virtual
                                                  * interfaces */
#define  MAX_OSPFV3_PREFS_PER_INTERFACE    10    /* max prefix per interface */
#define  MAX_OSPFV3_PREFS_IN_AS            200   /* max prefix per AS */
#define  MAX_OSPFV3_NBRS_PER_IFACE         8     /* max nbrs per interface */
#define  MAX_OSPFV3_HOSTS                  8     /* max hosts */
#define  MAX_OSPFV3_LSA_REQ_NODES          6000  /* max LSA requests */
#define  MAX_OSPFV3_SPF_NODES              600   /* max SPF nodes for route
                                                  * calc */
#define  MAX_OSPFV3_ROUTES                 1500  /* max OSPF routes */
#define  MAX_OSPFV3_PATHS                  3000  /* max OSPF paths */
#define  MAX_OSPFV3_INTERNAL_ROUTES        500   /* max OSPF internal routes */
#define  MAX_OSPFV3_EXT_ROUTES             1000  /* max OSPF external routes */
#define  MAX_OSPFV3_REDISTR_CONFIG_RECS    50    /* max redistribution config
                                                  * info */
#define  MAX_OSPFV3_RXMT_NODES             6000  /* max LSA rxmt nodes */
#define  MAX_OSPFV3_RTR_LSA_INFO           1000  /* max router LSA for GR */
#define  MAX_OSPFV3_NW_LSA_INFO            1000  /* max network LSA for GR */
#define  MAX_OSPFV3_NO_OF_LINK_SCOPE_LSAS  100   /* max link scope LSA */
#define  MAX_OSPFV3_SELF_ORG_LSAS_EXT_LSAS 6000  /* max self orig external
                                                  * LSA */
#define  MAX_OSPFV3_SELF_ORG_SUM_LSAS      400   /* max self orig summary LSA */
#define  MAX_OSPFV3_ECMP_PATHS             64    /* Extra paths for ECMP routes 
                                                  * in addition to normal
                                                  * Routes */
#define  MAX_OSPFV3_LSAID_INFO             100   /* max LSA id for GR */
#define  MAX_OSPFV3_RTM_ROUTES             1000  /* max route info from RTM */
#define  MAX_OSPFV3_INT_LSAS               6000  /* max LSAs*/
#define  MAX_OSPFV3_EXT_LSAS               6000  /* max LSAs*/
#define  MAX_OSPFV3_INT_LSA_SIZE           1024  /* max LSA size for INT Lsa mempool*/
#define  MAX_OSPFV3_EXT_LSA_SIZE           500   /* max LSA size for EXT Lsa mempool*/

/* Route map message array size which will preserve rmap messages during
 * staggered state
 */
#define  MAX_OSPFV3_RMAP_MSGS               10    /* maximum route map
                                                   * messages */

/* Max ospf v3 msg = max ospf interfaces (to hold last sent DDP pkt) +
 *                   max ospf nbrs (to hold ack pkt) +
 *                   1 (normal packet processing)
 */
#define  MAX_OSPFV3_MSG                     (MAX_OSPFV3_INTERFACES + MAX_OSPFV3_NBRS + 1)
                                                  /* max no of msgs used for
                                                   * packet allocation */


/* The following macros do not correspond to memory pools */
/* Macro to define the maximum number of contexts supported by OSPFv3
 * This macro is mapped to system default max contexts since context is
 * maintained as an array in OSPFv3
 */
#define  OSPFV3_MAX_LSA_SIZE                640                 
#define  OSPFV3_MIN_LSA_SIZE                20      
#define  OSPFV3_MAX_LSAS_PER_AREA           128
#define  OSPFV3_MAX_LEARNT_EXT_ROUTES       500
#define  OSPFV3_MAX_REDIST_EXT_ROUTES       500
#define  OSPFV3_MAX_VIRTUAL_IFS             5
#define  OSPFV3_MAX_MSG_SIZE                (1500 + IPV6_HEADER_LEN)

/* TLM */
#define MAX_TLM_LINKS                       SYS_DEF_MAX_TELINK_INTERFACES
#define MAX_TLM_IF_DESCRS                   1
#define MAX_TLM_TE_IF_DESCR                 (MAX_TLM_LINKS * MAX_TLM_IF_DESCRS)
#define MAX_TLM_SRLG_NO                     8
#define MAX_TLM_TE_SRLG                     (MAX_TLM_LINKS * MAX_TLM_SRLG_NO)
#define MAX_TLM_COMPONENT_LINKS             SYS_DEF_MAX_PHYSICAL_INTERFACES
#define TLM_MAX_COMPONENT_IF_DESCRS         1
#define MAX_TLM_COMP_IF_DESCR               (MAX_TLM_COMPONENT_LINKS * \
                                             TLM_MAX_COMPONENT_IF_DESCRS)
#define MAX_TLM_TE_HASH_INFO                 MAX_TLM_LINKS
#define MAX_TLM_APPS                         TLM_MAX_APPS
#define MAX_TLM_QUEUE_SIZE                   (MAX_TLM_LINKS * 2)

/* MPLS */
#ifndef ISS_WANTED
#define MAX_MPLS_INTERFACES                 16
#else
#define MAX_MPLS_INTERFACES                 SYS_DEF_MAX_MPLS_TNL_IFACES
#endif

#define MPLS_INTERFACE_MIN      CFA_MIN_MPLS_IF_INDEX
#define MPLS_INTERFACE_MAX      CFA_MAX_MPLS_IF_INDEX

#define MAX_MPLS_STATIC_LSPS                128
#define MAX_DS_EXP                          8

/* LDP */
/* NOTE: This value LDP_QDEPTH indicates the maximum number of messages that 
 * can be enqueued into the LDP's message queue. IF required this has to be
 * modified at the time of porting based on the target support. */
#define MAX_LDP_QDEPTH                      256
#define MAX_LDP_ENTITIES                    16
#define MAX_LDP_LSPS                        100
#define MAX_LDP_IFACES                      SYS_DEF_MAX_PHYSICAL_INTERFACES + \
                                            LA_MAX_AGG + \
                                            IP_DEV_MAX_L3VLAN_INTF + \
                                            SYS_DEF_MAX_TUNL_IFACES + \
                                            SYS_DEF_MAX_MPLS_IFACES + \
                                            SYS_DEF_MAX_MPLS_TNL_IFACES
#define MAX_LDP_LOCAL_PEERS                 32
#define MAX_LDP_REMOTE_PEERS                16
#define MAX_LDP_PEERS                       (MAX_LDP_LOCAL_PEERS + MAX_LDP_REMOTE_PEERS)
#define MAX_LDP_PEER_IF                     200
#define MAX_LDP_MSGS                        4
#define MAX_LDP_ADJS                        64
#define MAX_LDP_SESSIONS                    64
#define MAX_LDP_BFD_SESSIONS                64
#define MAX_LDP_CRLSP_TUNNELS               16
#define MAX_LDP_CRLSP_TNL_INFO              MAX_LDP_CRLSP_TUNNELS
#define MAX_LDP_TNL_INCARN_INFO             MAX_LDP_CRLSP_TUNNELS
#define MAX_LDP_LSP_LCB                    (MAX_LDP_LSPS + MAX_LDP_CRLSP_TUNNELS)
#define MAX_LDP_LSP_UPSTR_LCB               MAX_LDP_LSPS
#define MAX_LDP_LSP_TRIG_LCB               (MAX_LDP_LSPS + MAX_LDP_CRLSP_TUNNELS)
#define MAX_LDP_PEER_PASSWD_INFO            MAX_LDP_PEERS

#define MAX_LDP_ATM_RANGE_BLKS              4
#define MAX_LDP_ATM_LBL_RANGE               4
#define MAX_LDP_ATM_PARAMS                  4
#define MAX_LDP_GEN_RANGE_BLKS              128
/* NOTE: This value MAX_LDP_IP_RT_ENTRIES indicates the number of route
 * change indications(from IP) that can be handled by LDP simultaneously. */
#define MAX_LDP_IP_RT_ENTRIES               64
/* NOTE: This value LDP_MAX_INT_LSP indicates the number of route
 * entries for which LSP Establishment can be initiated at a time by LDP. */
#define MAX_LDP_INT_LSP                     32
#define MAX_LDP_TEMP_TE_HOP_INFO            32
#define MAX_LDP_DEF_NUM_BLKS                1
#define MAX_LDP_APPLICATIONS                2
#define MAX_LDP_L2VPN_MSGS                  64
#define MAX_LDP_L2VPN_EVT_INFO              MAX_LDP_L2VPN_MSGS
/* NOTE: This has to be changed at the time of porting if required to support
 * more than 256 sessions. This also depends on the underlying SLI and TCP
 * support in the target. Maximum TCP session connections supported by the TASK */
#define MAX_LDP_TCP_UDP_SSN_SUPRTD          256
#define MAX_LDP_OA_TRFC_PROFILE            (MAX_LDP_CRLSP_TUNNELS * MAX_DS_EXP)
#define MAX_LDP_FEC_TABLE                  MAX_LDP_DEF_NUM_BLKS
#define MAX_LDP_RECV_BUF                   MAX_LDP_DEF_NUM_BLKS
#define MAX_LDP_UDP_SEND_BUF               MAX_LDP_DEF_NUM_BLKS
#define MAX_LDP_TCP_SEND_BUF               MAX_LDP_DEF_NUM_BLKS
#define MAX_LDP_UDP_RECV_BUF               MAX_LDP_DEF_NUM_BLKS
#define MAX_LDP_ATM_LBL_KEY_INFO           MAX_LDP_DEF_NUM_BLKS
#define MAX_LDP_TCP_UDP_SOCK_INFO          MAX_LDP_DEF_NUM_BLKS
#define MAX_LDP_SOCK_RECV_BUF              20
#define MAX_LDP_TCP_PEND_WRITE_BUF         20
#define MAX_LDP_L2VPN_PWVC_EVT_INFO        MAX_LDP_L2VPN_EVT_INFO
#define MAX_LDP_L2VPN_IF_STR_INFO          MAX_LDP_L2VPN_EVT_INFO

#define MAX_LBl_GRPS_SUPRTD             (MAX_LDP_ENTITIES + 1)
#define MAX_KEY2_GRP_PER_KEY2_GRP_SPRTD 6250

/* TE */
/* The below macro should be kept as a multiple of 4 whenever it gets
 * fine tuned. This is because this macro is used as index to an array
 * in some structure. */
#define  MAX_TE_TUNNEL_INFO                 52
#define  MAX_TE_DS_AVG_PHBS                  4
#define  MAX_TE_DS_ELSPS                    MAX_TE_TUNNEL_INFO
#define  MAX_TE_DS_LLSPS                    MAX_TE_TUNNEL_INFO

/* These values should be less than the MAX_TE_TUNNEL_INFO and hence 
 * the max value of 20 is chosen for H-LSP and S-LSP tunnels.*/
#define MAX_TE_HLSP                         20
#define MAX_TE_SLSP                         20
#define MAX_TE_DEF_STACKED_TNLS             12
/* The MAX_TE_DEF_HLSP specifies the maximum number of blocks to be 
 * allocated for maintaining H-LSP and S-LSP specific parameters.
 * The same mempool is used for H-LSP and S-LSP tunnels.*/
#define MAX_TE_DEF_HLSP                     (MAX_TE_HLSP + MAX_TE_SLSP) 
#define MAX_TE_LSP_MAP                      (MAX_TE_HLSP + MAX_TE_SLSP) 
/* Hop related constants */
#define  MAX_TE_HOP_LIST                    (2 * MAX_TE_TUNNEL_INFO)
#define  MAX_TE_PO_PER_HOP_LIST              2
#define  MAX_TE_HOP_PER_PO                 16

/* ArHop related constants */
#define  MAX_TE_ARHOP_LIST                  (2 * MAX_TE_TUNNEL_INFO)
#define  MAX_TE_ARHOP_PER_LIST               16

/* Maximum number of Computed-Hop list used */
#define  MAX_TE_CHOP_LIST                   MAX_TE_TUNNEL_INFO
#define  MAX_TE_CHOP_PER_LIST              16
/* Maximum number of Attribute list used */
#define  MAX_TE_ATTR_LIST                   20
#define MAX_TE_MPLS_API_INPUT_INFO_ENTRIES  2
#define MAX_TE_MPLS_API_OUTPUT_INFO_ENTRIES 2


/* Traffic Param related constants */
#define  MAX_TE_TRAFFIC_PARAMS                 ((4 * MAX_TE_TUNNEL_INFO) + 1)
#define  MAX_TE_L2VPN_IF_QDEPTH              128

#define  MAX_TE_DIFFSERV_ELSP_INFO          (MAX_TE_TUNNEL_INFO * MAX_TE_DS_AVG_PHBS)
#define  MAX_TE_DIFFSERV_TNL_INFO           (MAX_TE_DS_ELSPS + MAX_TE_DS_LLSPS)
#define  MAX_TE_PATH_INFO                   (MAX_TE_HOP_LIST * MAX_TE_PO_PER_HOP_LIST)
#define  MAX_TE_HOP_INFO                    (MAX_TE_PATH_INFO * MAX_TE_HOP_PER_PO)
#define  MAX_TE_AR_HOP_INFO                 (MAX_TE_ARHOP_LIST * MAX_TE_ARHOP_PER_LIST)
#define  MAX_TE_CHOP_INFO                   (MAX_TE_CHOP_LIST * MAX_TE_CHOP_PER_LIST)
#define  MAX_TE_RSVP_TRFC_PARAMS            MAX_TE_TRAFFIC_PARAMS
#define  MAX_TE_CRLD_TRFC_PARAMS            MAX_TE_TRAFFIC_PARAMS
#define  MAX_TE_MPLS_TE_TNL_INDEX           MAX_TE_L2VPN_IF_QDEPTH
#define  MAX_TE_FRR_CONST_INFO              20
#define  MAX_TE_TNL_SRLG                    10
#define  MAX_TE_ATTR_SRLG                   10
#define  MAX_TE_DIFFSERV_ELSP_LIST          1
#define  MAX_TE_TNL_INDEX_LIST              1
#define  MAX_TE_PATH_LIST                   1
#define  MAX_TE_AR_HOP_LIST_INFO            1
#define  MAX_TE_CHOP_LIST_INFO              1 
#define  MAX_TE_ATTR_LIST_INFO              1
#define  MAX_TE_TRFC_PARAMS                 1
#define  MAX_TE_TNL_ERROR_TABLE_INFO        (MAX_TE_TUNNEL_INFO/2)
#define  MAX_TE_TEMP_TNL_INFO               20

/* TC */
#define MAX_TC_ALLOC_RES_NODE               MAX_TE_TUNNEL_INFO
#define MAX_TC_FILTER_SPEC_NODE             MAX_TE_TUNNEL_INFO

/*RSVPTE*/
#define  MAX_RSVPTE_RT_HANDLE                   50
#define  MAX_RSVPTE_NEW_SUBOBJ_VAL               8

#define  MAX_RSVPTE_MSG_IDS_PER_SREFRESH         400
#define  MAX_RSVPTE_MSG_ID_PER_TNL               8
#define  MAX_RSVPTE_MAX_TIMER_BLKS_PER_TNL      4
#define  MAX_RSVPTE_ACKS_NACKS_PER_MSG          200

#define  MAX_RSVPTE_TUNNEL                      MAX_TE_TUNNEL_INFO
#define  MAX_RSVPTE_NEWSUBOBJ_INFO              MAX_RSVPTE_TUNNEL * MAX_RSVPTE_NEW_SUBOBJ_VAL
#define  MAX_RSVPTE_HOP_INFO                    32
#define  MAX_RSVPTE_AR_HOP_INFO                 32
#define  MAX_RSVPTE_IF_ENTRY                    MAX_MPLS_INTERFACES
#define  MAX_RSVPTE_NBR_ENTRY                   50
#define  MAX_RSVPTE_PSB                         MAX_RSVPTE_TUNNEL
#define  MAX_RSVPTE_RSB                         MAX_RSVPTE_TUNNEL
#define  MAX_RSVPTE_RT_ENTRY_INFO               MAX_RSVPTE_RT_HANDLE
#define  MAX_RSVPTE_CSPF_MSG                    50
#define  MAX_RSVPTE_MSGID_OBJNODE               ((MAX_RSVPTE_MSG_IDS_PER_SREFRESH * \
                                                 MAX_RSVPTE_NBR_ENTRY) + \
                                                 MAX_RSVPTE_ACKS_NACKS_PER_MSG)
#define  MAX_RSVPTE_TIMER_BLOCK                 (MAX_RSVPTE_TUNNEL * \
                                                 MAX_RSVPTE_MAX_TIMER_BLKS_PER_TNL)
#define  MAX_RSVPTE_TRIE_INFO                   (MAX_RSVPTE_TUNNEL * \
                                                 RPTE_MAX_MSG_ID_PER_TNL)
#define  MAX_RSVPTE_64_BIT_TRIE_NODE             2 * ( 2 * MAX_RSVPTE_TUNNEL)
#define  MAX_RSVPTE_64_BIT_TRIE_LEAF_NODE       (2 * MAX_RSVPTE_TUNNEL)
#define  MAX_RSVPTE_32_BIT_TRIE_NODE             2 * ( 4 * MAX_RSVPTE_TUNNEL)
#define  MAX_RSVPTE_32_BIT_TRIE_LEAF_NODE       (4 * MAX_RSVPTE_TUNNEL)
#define  MAX_RSVPTE_RAW_PKT                     1
#define  MAX_RSVPTE_UDP_DATA_BUF                1
#define  MAX_RSVPTE_RAW_IP_DATA_BUF             1
#define  MAX_RSVPTE_PROCESSS_UDP_DATA_BUF       1
#define  MAX_RSVPTE_RSVP_PKT                    2
#define  MAX_RSVPTE_IP_PKT                      2
#define  MAX_RSVPTE_GBL_TRIE                    1
#define  MAX_RSVPTE_AR_HOP_TEMP                 32
#define  MAX_RSVPTE_CSPF_COMP_INFO              2
#define  MAX_RSVPTE_PKT_MAP_ENTRIES             2
#define  MAX_RSVPTE_NOTIFY_RECEIPIENT           20
#define  MAX_RSVPTE_NOTIFY_TUNNELS              MAX_RSVPTE_TUNNEL
#define  MAX_RSVPTE_REC_NTFY_TUNNEL             MAX_RSVPTE_TUNNEL
#define  MAX_RSVPTE_INTERNAL_EVNT               3000

/* With this value maximum 32 * MAX_RSVPTE_L3_INF_ARRAY_INDEX
 * interfaces can be protected for Frr tunnel */
#define  MAX_RSVPTE_L3_INF_ARRAY_INDEX          10

/* MPLS_P2MP_LSP_CHANGES - S */
#define  MAX_DEST_PER_P2MP_LSP                  20
#define  MAX_OUTSEGMENT_PER_P2MP_TUNNEL         5
#define  MAX_TE_P2MP_OUTSEGMENT                 (MAX_TE_P2MP_TUNNEL_INFO * \
                                                 (MAX_OUTSEGMENT_PER_P2MP_TUNNEL - 1))
/* MPLS_P2MP_LSP_CHANGES - E */

/* MPLSDB */
#define MAX_MPLSDB_LABELS_PER_ENTRY         7
#define MAX_MPLSDB_LSR_IFENTRY              MAX_MPLS_INTERFACES
#define MAX_MPLSDB_FTN_ENTRY                (MAX_MPLS_STATIC_LSPS + MAX_LDP_LSPS)
#define MAX_MPLSDB_FTN_MAP_ENTRY            MAX_MPLS_STATIC_LSPS
#define MAX_MPLSDB_LSR_INSEGMENT           (MAX_MPLS_STATIC_LSPS + MAX_TE_TUNNEL_INFO + \
                                            MAX_LDP_LSPS)
#define MAX_MPLSDB_LSR_XCENTRY             (MAX_MPLS_STATIC_LSPS + MAX_TE_TUNNEL_INFO + \
                                            MAX_LDP_LSPS)

/* MPLS_P2MP_LSP_CHANGES - S */
/* Single out-segment is assigned to each tunnel. P2MP tunnel will have 
 * additional out-segments */
#define MAX_MPLSDB_LSR_OUTSEGMENT          (MAX_MPLS_STATIC_LSPS + MAX_TE_TUNNEL_INFO + \
                                            MAX_TE_P2MP_OUTSEGMENT + MAX_LDP_LSPS)
/* MPLS_P2MP_LSP_CHANGES - E */

#define MAX_MPLSDB_LSR_LBLSTKENTRY          MAX_MPLS_STATIC_LSPS
#define MAX_MPLSDB_LABEL_ENTRY              (MAX_MPLSDB_LSR_LBLSTKENTRY * \
                                             MAX_MPLSDB_LABELS_PER_ENTRY)

#define MAX_MPLSDB_LBLSTK_ENTRIES     MAX_MPLSDB_LSR_LBLSTKENTRY

#ifdef LDP_GR_WANTED
#define MAX_MPLSDB_HW_LIST_FTN_ENTRY        MAX_LDP_LSPS
#define MAX_MPLSDB_HW_LIST_ILM_ENTRY        MAX_LDP_LSPS
#endif

/* MPLS_P2MP_LSP_CHANGES - S */
#define  MAX_TE_P2MP_TUNNEL_INFO            MAX_TE_TUNNEL_INFO/2
#define  MAX_TE_P2MP_BRANCH_INFO            MAX_MPLSDB_LSR_OUTSEGMENT
#define  MAX_TE_P2MP_DEST_INFO             (MAX_TE_P2MP_TUNNEL_INFO * \
                                            MAX_DEST_PER_P2MP_LSP)
#define MAX_TE_P2MP_TUNNEL_DEST_TABLE       5
/* MPLS_P2MP_LSP_CHANGES - E */

/*L2VPN*/
#define MAX_L2VPN_Q_MSG                         256

/*The MAX_L2VPN_PW_VC_ENTRIES is increased by 1 to allocate memory during 
 * the deletion when all 128 PW are created */

/* Default Pw that can be configured are 64*/
#define MAX_L2VPN_PW_VC_ENTRIES                 (64 + 1)
#define MAX_MPLSDB_PW_VC_ENTRIES               MAX_L2VPN_PW_VC_ENTRIES
/* Considering one IF Descriptor entry for each PW the below value
 * is kept as MAX_L2VPN_PW_VC_ENTRIES */
#define MAX_L2VPN_IF_DESCRIPTOR                 MAX_L2VPN_PW_VC_ENTRIES

/* Dormant PW Entries is currently kept as 1/4th of MAX_L2VPN_PW_VC_ENTRIES,
 * this can be fine tuned based on the need. */
#define MAX_L2VPN_PW_VC_DORMANT_ENTRIES         (MAX_L2VPN_PW_VC_ENTRIES / 4)

/* Label Message Entries temporarily stored by L2VPN Module till local PW
 * configuration is done is currently kept as 64, this can be fine tuned 
 * based on the need */
#define MAX_L2VPN_PW_VC_LBLMSG_ENTRIES          64

/* The below value represents the maximum number of peer LDP session entries
 * required to be maintained in L2VPN module. Currently, it is kept as 32
 * (equal to Max T-LDP Peers supported in LDP module), this can be fine tuned
 * based on the need. */
#define MAX_L2VPN_ACTIVE_PEER_SSN_ENTRY         32

/* The below value represents the medium in which PW operates on. Since,
 * all PW operate only over MPLS medium the below value is mapped to
 * MAX_L2VPN_PW_VC_ENTRIES */
#define MAX_L2VPN_PW_VC_MPLS_ENTRY              MAX_L2VPN_PW_VC_ENTRIES

/* The below value represents the underlying PSN TE Incoming tunnel + LSP
 * entries that are maintained by L2VPN Module. Currently, it is kept as
 * 64 (equal to maximum tunnel entries supported), this can be fine tuned
 * based on the need. */
#define MAX_L2VPN_MPLS_INMAPPING_ENTRIES        64

/* The below value represents the underlying PSN TE Outgoing tunnel + LSP
 * entries that are maintained by L2VPN Module. Currently, it is kept as
 * 64 (equal to maximum tunnel entries + LSP supported), this can be fine tuned
 * based on the need. */
#define MAX_L2VPN_PW_VC_MPLS_MAPPING_ENTRIES    64

/* Currently each PW is mapped to one service */
#define MAX_L2VPN_ENET_SERV_SPEC_ENTRY          MAX_L2VPN_PW_VC_ENTRIES

/* Currently each PW is mapped to one ethernet entry in case of VPWS */
#define MAX_L2VPN_ENET_ENTRIES                  MAX_L2VPN_PW_VC_ENTRIES

/* Currently one priority mapping entry is possible for each PW,
 * This table is obsoleted in the latest PW MIB, need to remove this mempool */
#define MAX_L2VPN_ENET_PRIMAPPING_ENTRIES       MAX_L2VPN_PW_VC_ENTRIES

#define MAX_L2VPN_VPLS_ENTRIES                  100

#ifdef VPLSADS_WANTED
#define MAX_L2VPN_VPLS_RD_ENTRIES               MAX_L2VPN_VPLS_ENTRIES
#define MAX_L2VPN_VPLS_RT_ENTRIES               MAX_L2VPN_VPLS_ENTRIES * MAX_L2VPN_VPLS_ENTRIES
#define MAX_L2VPN_VPLS_VE_ENTRIES               MAX_L2VPN_VPLS_ENTRIES
#define MAX_L2VPN_VPLS_AC_ENTRIES               MAX_L2VPN_VPLS_ENTRIES
#define MAX_L2VPN_VPLS_LB_ENTRIES               MAX_L2VPN_VPLS_ENTRIES
#define MAX_L2VPN_VPLS_LB_BLOCK_SIZE            sizeof(tVPLSLBInfo) * L2VPN_VPLS_MAX_LB
#endif
#ifdef HVPLS_WANTED                              
#define MAX_L2VPN_VPLS_BIND_ENTRIES             MAX_L2VPN_PW_VC_ENTRIES
#endif

#define MAX_L2VPN_PW_VC_INFO                    1
#define MAX_L2VPN_VPLS_INFO                     1
#define MAX_L2VPN_DATA_BUFFER                   1
#define MAX_L2VPN_VPN_ENTRIES                   MAX_L2VPN_PW_VC_ENTRIES
#define MAX_L2VPN_PWID_ENTRIES                  MAX_L2VPN_PW_VC_ENTRIES
#define MAX_L2VPN_IFINDEX_ENTRIES               SYS_DEF_MAX_L2_PSW_IFACES
/* Pw Redundancy */
#define MAX_L2VPN_REDUNDANCY_ENTRIES            10
#define MAX_L2VPN_REDUNDANCY_NODE_ENTRIES       4
#define MAX_L2VPN_REDUNDANCY_PW_ENTRIES         8
#define MAX_L2VPN_ICCP_MESSAGES                 16
#define MAX_L2VPN_ICCP_PW_ENTRIES               (MAX_L2VPN_REDUNDANCY_ENTRIES * \
                                                 MAX_L2VPN_REDUNDANCY_NODE_ENTRIES * \
                                                 MAX_L2VPN_REDUNDANCY_PW_ENTRIES)
#define MAX_L2VPN_ICCP_PWVC_REQUESTS            (MAX_L2VPN_ICCP_MESSAGES * \
                                                 MAX_L2VPN_REDUNDANCY_ENTRIES * \
                                                 MAX_L2VPN_REDUNDANCY_NODE_ENTRIES * \
                                                 (MAX_L2VPN_REDUNDANCY_PW_ENTRIES + 1))
#define MAX_L2VPN_ICCP_PWVC_DATA_ENTRIES        (MAX_L2VPN_ICCP_MESSAGES * \
                                                 MAX_L2VPN_REDUNDANCY_ENTRIES * \
                                                 MAX_L2VPN_REDUNDANCY_NODE_ENTRIES * \
                                                 MAX_L2VPN_REDUNDANCY_PW_ENTRIES)
#define MAX_L2VPN_MPLS_API_IN_INFO_ENTRIES      2
#define MAX_L2VPN_MPLS_API_OUT_INFO_ENTRIES     2
#define MAX_L2VPN_MPLS_PORT_ENTRY_INFO          MAX_L2VPN_ENET_ENTRIES

/* The below value should be equal to maximum number of contexts.
 * But MPLS-TP supports only default context. So, setting this value to 1 */
#define MAX_MPLSOAM_FSMPLSTPGLOBALCONFIGTABLE       10

/* The below value is sum of "Maximum number of NodeMapTable Entries" + 2.
 *
 * * Because 2 memory chunks are used by cli for holding temporary information.
 *
 * * Refer to oamclig.c - function cli_process_Oam_cmd() for use */
#define MAX_MPLSOAM_FSMPLSTPNODEMAPTABLE            25

/* Currently MEG is created only for TE LSP and PW. 
 * So, Max number of MEG entries is equal to Max Number of Tunnels +
 * Max Number of PW VC Entries. */
#define MAX_MPLSOAM_FSMPLSTPMEGTABLE               (MAX_TE_TUNNEL_INFO + \
                                                    MAX_L2VPN_PW_VC_ENTRIES)
#define MAX_MPLSOAM_FSMPLSTPMETABLE                 MAX_MPLSOAM_FSMPLSTPMEGTABLE

#define MAX_MPLSOAM_APIININFO                       2
#define MAX_MPLSOAM_APIOUTINFO                      2



/* MS-PW */
#define MAX_L2VPN_MS_PW_ENTRIES (MAX_L2VPN_PW_VC_ENTRIES/2)

/* MPLSRTR */
#define MAX_MPLS_LABELS_PER_IF                  32

#define MAX_MPLSRTR_RM_FRAME   10
#ifdef LANAI_WANTED
#define MAX_LANAI_VC_TABLE_ENTRIES  MAX_MPLS_INTERFACES * MAX_MPLS_LABELS_PER_IF
#define MAX_LANAI_ATM_INFO   8
#endif
#define MAX_MPLSRTR_RT_CHG_ENTRIES  60
#define MAX_MPLSRTR_IF_CHG_ENTRIES      30
#define MAX_MPLSRTR_DIFFSERV_PARAMS_TABLE 1
#define MAX_MPLSRTR_DIFFSERV_PARAMS_TOKEN_STACK 1
#define MAX_MPLSRTR_ELSPMAP_TABLE  1
#define MAX_MPLSRTR_ELSPMAP_TOKEN_STACK  1
#define MAX_MPLSRTR_ELSPMAP_ROW_ENTRIES  128
#define MAX_MPLSRTR_MPLS_DIFFSERV_PARAMS 128
#define MAX_MPLSRTR_MPLS_APPLICATIONS    MPLS_MAX_APP_ID
#define MAX_MPLSRTR_MPLS_API_IN_INFO        3
#define MAX_MPLSRTR_MPLS_API_OUT_INFO       3
#define MAX_MPLSRTR_OAM_FSMPLS_TP_ME_TABLE  3
#define MAX_MPLSRTR_MPLS_EVENT_NOTIF        2
#define MAX_MPLSRTR_BFD_REQ_PARAMS          2
#define MAX_CFA_ENET_MTU                    2
/* Label Manager Sizing Values */
#define MAX_LBLMGR_LBL_SPACE_GRP  1
#define MAX_LBL_GRPS_SUPPORTED                  MAX_LBl_GRPS_SUPRTD
#define MAX_KEY1_GRP_PER_LBL_GRP_SPRTD          1 
#define MAX_LBLMGR_LBL_MGR_KEY1_INFO  (MAX_LBl_GRPS_SUPRTD * \
                                             MAX_KEY1_GRP_PER_LBL_GRP_SPRTD)
#define MAX_LBLMGR_LBL_MGR_KEY2_INFO  (MAX_LBl_GRPS_SUPRTD * \
                                             MAX_KEY1_GRP_PER_LBL_GRP_SPRTD * \
                                             MAX_KEY2_GRP_PER_KEY2_GRP_SPRTD)

/* Sizing of MPLS Index Manager */

/* Value of the sizing value MAX_INDEXMGR_INDEX_MGR_GRP in the file system.size
 * should be (Max Number Of Groups Supported -> MAX_INDEXMGR_GRPS_SPRTD). This
 * is to ensure MemPools gets created correctly for the required number of 
 * groups. */

#define MAX_INDEXMGR_INDEX_MGR_GRP     1



/* Currently MPLS Index Manager supports the 12 groups to handle indices for 
 * the following entities. 
 * Group 1.  PW               -> MAX_L2VPN_PW_VC_ENTRIES
 * Group 2.  InSegment        -> MAX_MPLSDB_LSR_INSEGMENT
 * Group 3.  OutSegment       -> MAX_MPLSDB_LSR_OUTSEGMENT
 * Group 4.  XC               -> MAX_MPLSDB_LSR_XCENTRY
 * Group 5.  LabelStackEntry  -> MAX_MPLSDB_LSR_LBLSTKENTRY
 * Group 6.  FTN              -> MAX_MPLSDB_FTN_ENTRY
 * Group 7.  LDP Entities     -> MAX_LDP_ENTITIES
 * Group 8.  LDP LSPs         -> MAX_LDP_LSPS
 * Group 9.  VPN              -> MAX_L2VPN_VPN_ENTRIES
 * Group 10. MEG              -> MAX_MPLSOAM_FSMPLSTPMEGTABLE 
 * Group 11. ACID             -> MAX_L2VPN_ENET_ENTRIES
 */

/* Value of the sizing value MAX_INDEXMGR_INDEX_MGR_CHUNK in the file system.size
 * should be (Number of Groups Supported * Max of Max Number of Chunks 
 * Required in all the groups -> MAX_INDEXMGR_MAX_OF_CHUNKS_PER_GROUP).
 *
 * For example: Assume there are three groups each of them require 2 chunks, 
 * 3 chunks and 4 chunks respectively. The sizing value MAX_INDEXMGR_INDEX_MGR_CHUNK
 * in system.size file should be (3 * 4) = 12. No need to update this in file params.h.
 *
 * By default, max of max number of chunks required in all the 11 groups is 1. So,
 * This value is hardcoded in params.h as (11 * 1) = 11. */

#define MAX_INDEXMGR_INDEX_MGR_CHUNK  11

/* Number of chunks required for each group mentioned above is computed as
 * 
 * If X is the Maximum entries required for that group, then
 *    Number of chunks = ((X % 8192) == 0) ? (X / 8192) : ((X / 8192) + 1)
 *
 * Example: For Group 1: PW, X = MAX_L2VPN_PW_VC_ENTRIES -> 128,
 *    Number of chunks = ((128 % 8192) == 0) ? (128 / 8192) : ((128 / 8192) + 1)
 *                     = 1
 *
 * The value MAX_INDEXMGR_INDEX_TBL_CHUNK is equal to sum of number of chunks
 * all the groups. It is currently equal to 11. When Maximum number of entries
 * required for any of the group described above exceed 8192, this value should
 * be recomputed.
 */
#define MAX_INDEXMGR_INDEX_TBL_CHUNK           11

/* MPLS Sizing Parameters ENDS */

/* MLD */
#define   MLD_DEF_MAX_CACHE_ENTRIES                  50
#define   MLD_DEF_MAX_ROUTINGPROTO                   10

/*IPOA*/
#define   IPOA_DEF_MAX_AT_ENTRIES                     3
#define   IPOA_DEF_MAX_IPOA_CONNECTIONS               3

/* NAT */
#define   NAT_DEF_MAX_TYPICAL_ENTRIES              2500

/* FWL */
#define   FWL_DEF_MAX_FILTERS                       100
#define   FWL_DEF_MAX_RULES                          50

/* QoS */
#define   QoS_DEF_MAX_CLFR                          100
#define   QoS_DEF_MAX_METER                         100
#define   QoS_DEF_MAX_ACTION                        100
#define   QoS_DEF_MAX_BUFF                          100
#define   QoS_DEF_MAX_FLOWS                         100

/* DCBX */ /* DCBX Maximum Ports Size */
#define DCBX_MAX_PORT_LIST_SIZE              SYS_DEF_MAX_PHYSICAL_INTERFACES

/*START - DCBX sizing params */
/*tDcbxQueueMsg*/
#define MAX_DCBX_QUEUE_MESG                  (DCBX_MAX_PORT_LIST_SIZE * 14)
/*tDcbxPortEntry*/
#define MAX_DCBX_PORT_TABLE_ENTRIES          DCBX_MAX_PORT_LIST_SIZE
/*tDcbxAppEntry*/
#define MAX_DCBX_APP_TABLE_ENTRIES           6
/*tETSPortEntry*/
#define MAX_DCBX_ETS_PORT_ENTRIES            DCBX_MAX_PORT_LIST_SIZE
/*tPFCPortEntry*/
#define MAX_DCBX_PFC_PORT_ENTRIES            DCBX_MAX_PORT_LIST_SIZE
/*tMbsmProtoMsg*/
#ifdef MBSM_WANTED
#define MAX_DCBX_MBSM_MAX_LC_SLOTS           MBSM_MAX_LC_SLOTS
#else
#define MAX_DCBX_MBSM_MAX_LC_SLOTS           1
#endif 

/*Application Prioirty MACROS */
/*tAppPriPortEntry*/
#define MAX_DCBX_APP_PRI_PORTS             DCBX_MAX_PORT_LIST_SIZE
/* Max number of selectors used*/
#define DCBX_MAX_APP_PRI_SELECTORS         4
/* Max number of Application Priority Mapping Entries in a port */
#define DCBX_MAX_APP_PRI_ENTRIES_PER_PORT 15
/* Max number of application priority mapping entries allowed in a system */
#define MAX_DCBX_APP_PRI_ENTRIES          (DCBX_MAX_APP_PRI_ENTRIES_PER_PORT * MAX_DCBX_APP_PRI_PORTS * 2)

/*END - DCBX sizing params */

/*START - CN sizing params */
/*tCnQMsg*/
#define MAX_CN_Q_MESG                      ((BRG_MAX_PHY_PLUS_LOG_PORTS)/4)
/*tCnCompTblInfo*/
#define MAX_CN_COMP_TABLE_INFO             (SYS_DEF_MAX_NUM_CONTEXTS + 1)
/*tCnCompPriTbl*/
#define MAX_CN_COMP_PRIORITY_TABLE         ((SYS_DEF_MAX_NUM_CONTEXTS + 1)* 7)
/*tCnPortTblInfo*/
#define MAX_CN_PORT_TABLE_INFO              BRG_MAX_PHY_PLUS_LOG_PORTS
/*tCnPortPriTblInfo*/
#define MAX_CN_PORT_PRIORITY_TABLE_INFO    (BRG_MAX_PHY_PLUS_LOG_PORTS * 7)
/*tCnCompTblInfo*/
#define MAX_CN_COMP_TABLE_PTRS              (SYS_DEF_MAX_NUM_CONTEXTS + 1)
/*END - CN sizing params */

/* RBRG sizing changes - START */

/* This is for temporary usage. No need to scale the below set of  macros */
/* MAcros used for temporary storage in RBRG module - START */
#define MAX_RBRG_FSRBRIDGEGLOBALTABLE_ISSET       3 
#define MAX_RBRG_FSRBRIDGENICKNAMETABLE_ISSET     3
#define MAX_RBRG_FSRBRIDGEUNIFDBTABLE_ISSET       3
#define MAX_RBRG_FSRBRIDGEPORTTABLE_ISSET         3
#define MAX_RBRG_FSRBRIDGEUNIFIBTABLE_ISSET       3
#define MAX_RBRG_FSRBRIDGEMULTIFIBTABLE_ISSET     2
#define MAX_RBRG_FSRBRIDGEPORTCOUNTERTABLE_ISSET  2
#define MAX_RBRG_FSRBRIDGEUNIFDBTABLE             6
#define MAX_RBRG_FSRBRIDGEUNIFIBTABLE             6
#define MAX_RBRG_FSRBRIDGEMULTIFIBTABLE           5
#define MAX_RBRG_FSRBRIDGEPORTCOUNTERTABLE        3
/* MAcros used for temporary storage in RBRG module - END */


#define MAX_RBRG_FSRBRIDGEGLOBALTABLE             MAX_RBRG_CONTEXTS
#define MAX_RBRG_FSRBRIDGENICKNAMETABLE           MAX_RBRG_NICKNAMES
#define MAX_RBRG_FSRBRIDGEPORTTABLE               MAX_RBRG_PORTS
#define MAX_RBRG_QUEUE_MSG                        50
#define MAX_RBRG_MULTI_FIB_ENTRIES                2
#define MAX_RBRG_NICKNAMES                        4
#define MAX_RBRG_PORTS                           BRG_MAX_PHY_PLUS_LOG_PORTS
#define MAX_RBRG_FSFDB_ENTRY                     10
#define MAX_RBRG_FSFIB_ENTRY                     10 
#define MAX_RBRG_FSFDB_DATA                      20 /* This should be greater than MAX_RBRG_FSFDB_ENTRY
                                                       At the max it can hold (MAX_RBRG_FSFDB_ENTRY * 2) */
#define MAX_RBRG_FSFIB_DATA                      20 /* This should be greater than MAX_RBRG_FSFIB_ENTRY
                                                       At the max it can hold (MAX_RBRG_FSFIB_ENTRY * 2) */
#define MAX_RBRG_CONTEXTS                        (SYS_DEF_MAX_NUM_CONTEXTS + 4)
#ifdef MBSM_WANTED
#define MAX_RBRG_MBSM_LC_SLOTS                  MBSM_MAX_LC_SLOTS
#else
#define MAX_RBRG_MBSM_LC_SLOTS                  1
#endif 
/* RBRG sizing changes   - END*/

/* RADIUS */
#define   Q_TABLE_SIZE                              256
#define   MAX_RAD_USER_ENTRIES                       10
#define   MAX_RAD_TX_PKTS                            10
#define   MAX_RAD_SERVERS                             5
#define   MAX_RAD_INTERFACE_ENTRIES                  10
/* tRad2Byte Sizing params - Maximum number of 65536 byte blocks used in Radius*/
#define   MAX_U2_BLOCKS                               2
/*Following are Sizing params for members in tRADIUS_INPUT_AUTH */
#define   MAX_RAD_SERVICES                          270
#define   MAX_RAD_USER_CHAP                          10
#define   MAX_RAD_USER_EAP                           10
#define   MAX_RAD_USER_PAP                           10
#define   MAX_RAD_USER_OTHERS                        10
#define   MAX_RAD_USER_MS_CHAP                       10
#define   MAX_RAD_USER_MS_CHAP_CPW1                  10
#define   MAX_RAD_USER_MS_CHAP_CPW2                  10
#define   MAX_RAD_KEY_BLOCKS                          5
#define   MAX_RAD_TIMER_BLOCKS                       Q_TABLE_SIZE
#define   MAX_RAD_CONCAT                              3
#define   MAX_RAD_PACKET                              3

/* Following sizing param not related to mempool */
#define   MAX_RAD_SERVERS_LIMIT                       5

/* SNTP */
#define  MAX_SNTP_SERVERS                             2
#define  MAX_SNTP_Q_DEPTH                            10

/* TACACS */
#define MAX_TAC_AUTHEN_SESSION                       16
#define MAX_TAC_AUTHOR_SESSION                       16
#define MAX_TAC_ACCT_SESSION                         16
#define MAX_TAC_MSG_OUTPUT                           16
#define MAX_TAC_AUTHOR_OUTPUT                         2
#define MAX_TAC_AUTHEN_OUTPUT                         2
#define MAX_TACACS_PKT_SIZE                          1500 
#define MAX_TAC_PKT_COUNT                         5

/* Sizing params which are not related to MemPool*/
#define TAC_MAX_AUTHEN_SESSION_LIMIT                     16
#define TAC_MAX_AUTHOR_SESSION_LIMIT                     16
#define TAC_MAX_ACCT_SESSION_LIMIT                       16

/* RSVP */
#define   RSVP_DEF_MAX_SESSION                      100
#define   RSVP_DEF_MAX_SENDER                       100
#define   RSVP_DEF_MAX_RSB                          100
#define   RSVP_DEF_MAX_TCSB                         100
#define   RSVP_DEF_MAX_RESV_FWD                     100
#define   RSVP_DEF_MAX_FLOW_DESCR                   100
#define   RSVP_DEF_MAX_TMR_PARAM                    100

/* IPSEC */
#define IPSEC_DEF_MAX_NO_OF_SA                      100
#define IPSEC_DEF_MAX_TBL_SIZE                      1000
#define MAX_SEC_CRU_BUF                             1000

/* DNS_RELAY */
#define DNS_RELAY_DEF_MAX_NS                        100
#define DNS_RELAY_DEF_MAX_CQ                        50
#define DNS_RELAY_DEF_MAX_CACHE                     1000
#define DNS_RELAY_DEF_MAX_URL_FIL                   100

/* MFWD */
#ifdef MFWD_WANTED
#define MFWD_DEF_MAX_MRPS                           260
#define MAX_MFWD_CMD_DATA_PKT                       MFWD_DEF_MAX_MRPS
#define MAX_MFWD_OWNER_INFO_TABLE                   1
#define MAX_MFWD_NUM_MRPS                           7
#define MAX_MFWD_OWNER_NODE                         MAX_MFWD_NUM_MRPS
#define MAX_MFWD_CACHE_MISS_DB_NODES                3
#define MAX_MFWD_Q_DEPTH                            30
#define MAX_MFWD_INTERFACES_NODE                    30 /*((MAX_IP_TUNNEL_INTERFACES + \
                                                    IP_DEV_MAX_L3VLAN_INTF + \
                                                    IP_MAX_RPORT) * MAX_MFWD_OWNER_NODE)*/

#define MAX_MFWD_GRP_NODE                           30
                                                    /*(MAX_PIM_GRPS  * MAX_MFWD_OWNER_NODE)                                                    maximum of MAX_IGP_MCAST_GRPS
                                                                   MAX_GROUP_DVMRP &
                                                                   MAX_PIM_GRPS */
#define MAX_MFWD_SRC_NODE                           30
                                                   /*(MAX_MFWD_GRP_NODE * MAX_PIM_GRP_SRC*/
                                                            /*maximum of
                                                             MAX_IGP_MCAST_SRCS,
                                                              MAX_SOURCE_DVMRP &
                                                      (MAX_PIM_GRP_SRC + MAX_PIM_RPS)*/

#define MAX_MFWD_OWNER_RTENTRIES                      MAX_MFWD_GRP_NODE

#define MAX_MFWD_OWNER_OIFS                          (MAX_MFWD_INTERFACES_NODE - 1)

#define MAX_MFWD_OWNER_IIFS                          (MAX_MFWD_INTERFACES_NODE - 1)

#define MFWD_MAX_NEXTHOPS_PER_RTENTRY                3

#define MAX_MFWD_OWNER_OIF_NEXTHOP                   MAX_MFWD_SRC_NODE * \
                                                     MFWD_MAX_NEXTHOPS_PER_RTENTRY

#endif

/*VRRP*/
#define MAX_ASSO_ADDR_ENTRY      10 /*Maximum number of associated ip address
                                     *per VRRP instance*/
#define MAX_TRACK_IF_ENTRY       5  /* Maximum number of Tracked Interface per
                                       Track Group. */

#define MAX_VRRP_OPER_ENTRY      10
#define MAX_VRRP_AUTHKEY_ENTRY   MAX_VRRP_OPER_ENTRY /*The Max length defined in std mib is 16*/
#define MAX_VRRP_TIMER_ENTRY     10
#define VRRP_MAX_OPER_ENTRY      MAX_VRRP_OPER_ENTRY
#define MAX_VRRP_QUEUE_DEPTH     IP_DEV_MAX_L3VLAN_INTF * 4
#define MAX_VRRP_RM_QUE_DEPTH    10
#define MAX_VRRP_DYN_MSG_SIZE    20

/* The below denotes the maximum number of Associated IP Addresses
 * supported in the system.
 *
 * Memory required for Transmit Buffer for IPv4 and IPv6 is computed based
 * MAX_VRRP_OPER_ENTRY and MAX_ASSO_ADDR_ENTRY by using formulae.
 *
 * Size required for Transmit Buffer of IPv4 is 
 * VRRP_HEADER_LEN (8) + TEXT_AUTH_SIZE (8) + 
 * ((Number of Associated IP Addresses supported per VRRP Instance *
 *   Number of VRRP Oper Entries supported in the system) * 4).
 * Number of blocks required Transmit Buffer of IPv4 is
 *   Number of VRRP Oper Entries supported in the system
 *
 * Size required for Transmit Buffer of IPv6 is
 * VRRP_HEADER_LEN (8) +
 * ((Number of Associated IP Addresses supported per VRRP Instance *
 *   Number of VRRP Oper Entries supported in the system) * 16).
 * Number of blocks required Transmit Buffer of IPv6 is
 *   Number of VRRP Oper Entries supported in the system.
 *
 * The macro MAX_ASSO_ADDR_ENTRY should be mentioned sizing file for
 * changes to get reflected.
 */
#define MAX_VRRP_ASSO_IP_ENTRY   (MAX_VRRP_OPER_ENTRY * MAX_ASSO_ADDR_ENTRY)

#define MAX_VRRP_TX_IPV4_BUF     MAX_VRRP_OPER_ENTRY
#define MAX_VRRP_TX_IPV6_BUF     MAX_VRRP_OPER_ENTRY
#define MAX_VRRP_TX_RX_IPV4_PKT  2
#define MAX_VRRP_TX_RX_IPV6_PKT  2
#define MAX_VRRP_TRACK_GROUP     10

#define MAX_VRRP_TRACK_IF_GROUP  (MAX_VRRP_TRACK_GROUP * MAX_TRACK_IF_ENTRY)

/*NAT*/
#define MAX_NAT_NO_OF_FRAGMENT_STREAM        100
#define MAX_NAT_FRAGMENTS_SIZE               2000
#define MAX_NAT_LOC_OUT_HASH_NODE            18000
#define MAX_NAT_LID_LIST_NODE                9000
#define MAX_NAT_LID_OID_ARR_DYN_LIST_NODE    9000
#define MAX_NAT_DNS_TABLE_BLOCKS             100
#define MAX_NAT_DYNAMIC_ENTRIES              9000
#define MAX_NAT_GBL_HASH_NODE                9000
#define MAX_NAT_FREE_GIP_LIST_NODE           9000
#define MAX_NAT_PARTIAL_LINKS_LIST_BLOCKS    2000
#define MAX_NAT_ONLY_LIST_BLOCKS             100
#define MAX_NAT_FREE_PORT_NODE               9000
#define MAX_NAT_TCP_DEL_NODE                 9000
#define NAT_SIP_MEM_POOL_NUM_BLK             1000 
#define MAX_NAT_LINEAR_BUF_BLOCKS            2
#define MAX_NAT_IPSEC_OUT_HASH_NODE          9000
#define NAT_SIP_HASH_POOL_NUM_BLK            500
#define MAX_NAT_IPSEC_IN_HASH_NODE           9000
#define MAX_NAT_IPSEC_PEND_HASH_NODE         9000
#define MAX_NAT_IPSEC_LIST_NODE              9000
#define MAX_NAT_IPSEC_PEND_LIST_NODE         9000
#define MAX_NAT_IKE_HASH_NODE                9000
#define MAX_NAT_IKE_LIST_NODE                9000
#define MAX_NAT_LOCAL_ADDR_TABLE_BLOCKS      10
#define MAX_NAT_STATIC_BLOCKS                100
#define MAX_NAT_STATIC_NAPT_BLOCKS           60
#define MAX_NAT_NUM_IF                       4497
#define MAX_NAT_GLOBAL_ADDR_TABLE_BLOCKS     100
#define MAX_NAT_POLICY_NODE                  20

/* Firewall */
#define MAX_FWL_INTERFACE_INFO          CFA_MAX_INTERFACES_IN_SYS 
#define MAX_FWL_RULES_INFO              50
#define MAX_FWL_FILTERS_INFO            100
#define MAX_FWL_ACL_INFO                50 
#define MAX_FWL_LOG_BUFFER              20 
#define MAX_FWL_STATEFUL_SESSION        9000 
#define MAX_FWL_TCP_INIT_FLOWS          (MAX_FWL_STATEFUL_SESSION * 80/100)
#define MAX_FWL_PARTIAL_LINKS           (MAX_FWL_STATEFUL_SESSION * 50/100)
#define MAX_FWL_IN_USER_SESSION         (MAX_FWL_STATEFUL_SESSION * 25/100)
#define MAX_FWL_OUT_USER_SESSION        (MAX_FWL_STATEFUL_SESSION * 10/100)
#define MAX_FWL_DMZ_INFO                5
#define MAX_FWL_IPV6_DMZ_INFO           5
#define MAX_FWL_URL_FILTERS             25
#define MAX_FWL_URL_LENGTH_NUM          MAX_FWL_URL_FILTERS
#define MAX_FWL_WEB_LOG_BUFFER          1
#define MAX_FWL_PARTIAL_LOG_BLOCK       1
#define MAX_FWL_BLACK_LIST_ENTRIES      1024
#define MAX_FWL_WHITE_LIST_ENTRIES      1024
#define MAX_FWL_IPV6_ADDR_DMZ_BLOCKS    5
#define MAX_FWL_ACCESS_LIST_BLOCKS      1
#define MAX_FWL_FILTERS_BLOCKS          1
#define MAX_FWL_SNORK_ATTACK_BLOCKS     10
#define MAX_FWL_RPF_CHECK_BLOCKS        IP_DEV_MAX_IP_INTF

/* L2TP */

#define MAX_L2TP_PW_CLASS_INFO          100
#define MAX_L2TP_SESSION_INFO           100
#define MAX_L2TP_XCONNECT_INFO          100

#define MAX_L2TP_PORT_BLOCKS            MAX_L2TP_XCONNECT_INFO
#define MAX_L2TP_PORT_INFO              MAX_L2TP_XCONNECT_INFO
#define MAX_L2TP_ENCAP_INFO             MAX_L2TP_SESSION_INFO 
#define MAX_L2TP_DECAP_INFO             MAX_L2TP_SESSION_INFO
#define MAX_L2TP_SESSION_STATS_INFO     MAX_L2TP_SESSION_INFO

/* Ike */
#define MAX_IKE_PAYLOAD_BLOCKS          18
#define MAX_IKE_PROPOSAL_BLOCKS         2 
#define MAX_IKE_TRNSFORM_BLOCKS         3
#define MAX_IKE_ENGINE_BLOCKS           1000
#define MAX_IKE_AUTH_BLOCKS             1000
#define MAX_IKE_POLICY_BLOCKS           1000
#define MAX_IKE_KEY_BLOCKS              1000
#define MAX_IKE_TRANSFORMSET_BLOCKS     1000
#define MAX_IKE_CRYPTOMAP_BLOCKS        1000
#define MAX_IKE_RAINFO_BLOCKS           1000
#define MAX_IKE_PROTECTNET_BLOCKS       1000
#define MAX_IKE_RSAKEY_BLOCKS           1000
#define MAX_IKE_VPN_POLICY_BLOCKS       1000
#define MAX_IKE_SESSION_INFO_BLOCKS     1000  
#define MAX_IKE_SA_INFO_BLOCKS          1000
#define MAX_IKE_NOTIFY_INFO_BLOCKS      1000
#define MAX_IKE_CERT_INFO_BLOCKS        1000
#define MAX_IKE_AUTHDB_BLOCKS           1000
#define MAX_IKE_CERTDB_BLOCKS           1000
#define MAX_IKE_CERTRECORD_BLOCKS       10
#define MAX_IKE_CERTMAP_BLOCKS          1000
#define MAX_IKE_HEXNUM_BLOCKS           1000
#define MAX_IKE_UINT_BLOCKS             1000
#define MAX_IKE_IFPARAM_BLOCKS          5
#define IKE_SIZE_OF_BLK                 16384
#define MAX_IKE_STATISTICS_BLOCKS           1
#define MAX_IKE_V2_TRANSFORM_BLOCKS     1000
#define MAX_IKE_QUEUE_MESSAGE           1000

/*VPN */
#define  MAX_VPN_ID_BLOCKS            1000
#define  MAX_VPN_CERT_BLOCKS          1000
#define  MAX_VPN_CA_CERT_BLOCKS       1000 
#define  MAX_VPN_POLICY_BLOCKS        1000
#define  MAX_VPN_RA_ADDR_NODE_BLOCKS  1000
#define  MAX_RA_VPN_ADDRESS_POOL_BLOCKS 5
#define  MAX_RA_VPN_USERS_BLOCKS       30 
#define  MAX_WEB_IKE_POLICY_BLOCKS     5
#define  MAX_WEB_IPSEC_POLICY_BLOCKS   5
#define  MAX_WEB_POL_BUFF_BLOCKS       10
#define  VPN_MAX_Q_SIZE                128
/* DHCP Client*/
#define MAX_DHC_INTERFACE            148
#define MAX_DHC_CLIENTID             MAX_DHC_INTERFACE            
#define MAX_DHC_OPTION_REG           10
/* (148*2) - currently two options are supported*/
#define MAX_DHC_OPT_ENTRIES          296 
/* DHCP Relay*/
#define MAX_DHRL_INTERFACE           128
#define MAX_DHRL_FREE_SERVER_POOL    16
#define MAX_DHC_BEST_OFFER           148
#define MAX_DHC_QUEUE_MSG            MAX_DHC_INTERFACE
#define MAX_DHC_MSG_OPTIONS      25
#define MAX_DHC_OUT_PKT_INFO         25
#define MAX_DHC_CLIENT_DEF_OPTIONS   25
#define MAX_DHRL_PKT_INFO_SIZE            2
#define MAX_DHRL_CLNT_INFO_POOL     320 
#define MAX_DHRL_RLY_CXT           SYS_DEF_MAX_NUM_CONTEXTS

/* DHCP Server */
#define MAX_DHSRV_FREE_BIND_REC      (DHCP_SRV_MAX_POOLS * DHCP_SRV_MAX_HOST_PER_POOL )/*Max memory blocks */
#define MAX_DHSRV_FREE_HOST_REC      (DHCP_SRV_MAX_POOLS * DHCP_SRV_MAX_HOST_PER_POOL )/* Max memory blocks */
#define MAX_DHSRV_FREE_POOL_REC      DHCP_SRV_MAX_POOLS   /*Max memory blocks*/
#define MAX_DHSRV_EXCL_NODE          DHCP_SRV_MAX_POOLS   /*Max memory blocks*/
#define MAX_DHSRV_OPTION             20   /*Max memory blocks*/
#define MAX_DHSRV_OUT_PKT            10 /* Max Dhcp_srv outgoing pkt memory block*/
#define MAX_DHSRV_TEMP_OPT_PKT       MAX_DHSRV_OUT_PKT /*Max Dhcp_srv Temp pkt memory block*/
#define MAX_DHSRV_ICMP_NODE          100
#define MAX_DHSRV_QUEUE_MSG          50
#define MAX_DHCP_MAX_OUTMSG_SIZE        2
#define MAX_DHCP_MAX_MTU                2

/*DHCP6 Server*/
#define MAX_D6SR_SRV_CLIENT_ENTRY      64
#define MAX_D6SR_SRV_POOL_ENTRY        64
#define MAX_D6SR_SRV_REALM_ENTRY       64
#define MAX_D6SR_SRV_OPTION_ENTRY      256
#define MAX_D6SR_SRV_IF_ENTRY          32
#define MAX_D6SR_SRV_INPREFIX_ENTRY    64
#define MAX_D6SR_SRV_SUBOPTION_ENTRY   64
#define MAX_D6SR_SRV_QMSGS             64
#define MAX_D6SR_SRV_KEY_ENTRY         64

/*DHCP6 Client*/
#define MAX_D6CL_CLNT_OPTION           512
#define MAX_D6CL_CLNT_IFACES           128
#define MAX_D6CL_CLNT_APPS             64
#define MAX_D6CL_CLNT_QMSGS            384
#define MAX_D6CL_CLNT_PKTINFO          2
/* Macro doesnot map to mempool. 
 * Provides an upper limit on the number of APPS */
#define MAX_D6CL_CLNT_APPS_LIMIT       64

/*DHCP6 Relay*/
#define MAX_D6RL_RLY_ENTRIES           32
#define MAX_D6RL_RLY_IF_ENTRIES        32
#define MAX_D6RL_RLY_QMSGS             96
#define MAX_D6RL_RLY_OUT_ENTRIES       32
#define MAX_D6RL_RLY_PD_ROUTES        100
#define MAX_D6RL_RM_QUE_DEPTH          64

/*ISIS*/
#define MAX_ISIS_INSTANCES                10
#define MAX_ISIS_CIRCUITS                 10
#define MAX_ISIS_CKT_LEVELS               20
#define MAX_ISIS_AREA_ADDRESSES            5
#define MAX_ISIS_MANUAL_AREA_ADDRESSES     5
#define DEF_ISIS_AREA_ADDRESSES            3
#define MAX_ISIS_ADJACENCIES             320
#define MAX_ISIS_ADJ_DIR_ENTRIES         320
#define MAX_ISIS_IPRAS                  2500
#define MAX_ISIS_IP_IFACES                40
#define MAX_ISIS_SUMMARY_ADDRESSES        20
#define MAX_ISIS_EVENTS                   10
#define MAX_ISIS_SPT_NODES              3000
#define MAX_ISIS_TMR_NODES                30
#define MAX_ISIS_HASH_TABLE_NODES       2500
#define MAX_ISIS_LSP_ENTRIES            1000
#define MAX_ISIS_MESSAGES                 50
/* MAX LSPs that could be present in the transmission Queue*/
#define MAX_ISIS_LSPS_TXQ               1000
/* mempool to hold all the received acknowledgment packets*/
#define MAX_ISIS_ACK_RX                  255
/* mempool to hold all the received CSNP packets*/
#define MAX_ISIS_CSNP_RX                 255
/*a max of 3 route map filters can be configured on a context*/
#define MAX_ISIS_LSPS_RMAP_FILTERS       3*(MAX_ISIS_INSTANCES)
/* Following MACROS are not directly related to mempool creation */
#define   MAX_ISIS_INSTANCES_LIMIT        SYS_DEF_MAX_NUM_CONTEXTS 
#define   MAX_ISIS_GR_INFO        10
#define   MAX_ISIS_GR_CXT        10
#define MAX_GR_CXT_INFO_BLOCK_SIZE  (MAX_ISIS_INSTANCES*10)
#define   MAX_ISIS_128_BLKS      500 
#define   MAX_ISIS_1500_BLKS     500
#define   MAX_ISIS_RRD_ROUTES    1000
#define   MAX_HOST_NME_ENTRIES    255

/* Assumptions for arriving at MSR_INCREMENTAL_SAVE_BUFF_SIZE :
       * 1. An average of 40 to 50 bytes needed for storing the
       *   index, row status, OID, type and value of each object.
    * 2. assuming max entries allowed in incremental file or buffer 
    *    is 100 
    *    then buffer size is 50*100=5000 bytes */
#define MSR_INCREMENTAL_SAVE_BUFF_SIZE  ((IssGetColdStandbyFromNvRam()== \
                                          ISS_COLDSTDBY_ENABLE)?1000:5000)

/* The RB Tree mempool is allocated 20000 Rb nodes so that
 *  * it is equivalent to 1 MB */
#define MSR_INCR_RBNODES_COUNT          20000

/* TAC */
#ifdef TAC_WANTED
/* Maximum number of profiles and filter rules per system */
#define TACM_MAX_PROFILES                           100
#define TACM_MAX_FILTERS                            1000
#endif

/* RM */
/* Some CLI command/nmhSet call can take different time in different target 
 * enviornment, if it involves in h/w api calls. If the 
 * RM_SEQ_RECOV_TMR_INTERVAL is less than the required time then it will
 * produce sequence recovery timer expiry in standby node. This parameter 
 * needs to be tuned based on the target enviornment.
 */
#define RM_SEQ_RECOV_TMR_INTERVAL (30*1000) /* 30 sec - Default - 15 sec */
#define RM_ACK_RECOV_TMR_INTERVAL (30*1000) /* 30 sec - Default 5 sec */
#define RM_CONN_RETRY_TMR_INTERVAL (2*1000) /* 2 sec */

#ifdef ICCH_WANTED
/* ICCH */
/* Some CLI command/nmhSet call can take different time in different target
 * enviornment, if it involves in h/w api calls. If the
 * ICCH_SEQ_RECOV_TMR_INTERVAL is less than the required time then it will
 * produce sequence recovery timer expiry in standby node. This parameter
 * needs to be tuned based on the target enviornment.
 */
#define ICCH_SEQ_RECOV_TMR_INTERVAL (5*1000) /* 5 sec - Default - 5 sec */
#define ICCH_ACK_RECOV_TMR_INTERVAL (5*1000)  /* 5 sec - Default 5 sec */
#define ICCH_CONN_RETRY_TMR_INTERVAL (2*1000) /* 2 sec */
#endif

#ifdef RMON2_WANTED

#define RMON2_MAX_OID_LENGTH       30
/* Max Data Entries */
#define RMON2_MAX_DATA_PER_CTL     20
#define RMON2_MAX_ADDRMAP_ENTRY    50 
#define RMON2_MAX_TOPN_ENTRY       20
#define RMON2_MAX_USR_HISTORY_BUCKETS_GRANT     10
#endif

#ifdef DSMON_WANTED

/* Max Data entries */
#define DSMON_MAX_DATA_PER_CTL  20
#define DSMON_MAX_TOPN_ENTRY  20
#endif

                                               
/* LLDP application specific macros */
#define LLDP_MAX_APPL        6    /* Number of application that can 
                                     use the LLDP services simultaneously */
#define LLDP_MAX_APP_TLV_LEN 512  /* Maximum length of the LLDP application 
                                     TLV */
/*START - LLDP sizing params */
#define MAX_LLDP_Q_MESG                    (2 * SYS_DEF_MAX_PHYSICAL_INTERFACES)
#define MAX_LLDP_RX_PDU_Q_MESG               60
#define MAX_LLDP_LOCAL_PORT_INFO             SYS_DEF_MAX_PHYSICAL_INTERFACES
#define MAX_LLDP_LOCAL_MGMT_ADDR_TABLE       LLDP_MAX_LOC_MAN_ADDR
#define MAX_LLDP_REMOTE_NODES                (LLDP_MAX_NEIGHBORS + 1)
#define MAX_LLDP_REMOTE_MGMT_ADDR_TABLE      LLDP_MAX_REM_MAN_ADDR
#define MAX_LLDP_REMOTE_UNKNOWN_TLV_TABLE    LLDP_MAX_REM_UNKNOWN_TLV
#define MAX_LLDP_REMOTE_ORG_DEF_INFO_TABLE   LLDP_MAX_REM_ORG_DEF_INFO_TLV
#define MAX_LLDP_LOCAL_PROTO_IDENTIFY_INFO   LLDP_MAX_LOC_PROTOID
#define MAX_LLDP_LOCAL_PROTO_VLAN_INFO       LLDP_MAX_LOC_PPVID
#define MAX_LLDP_REMOTE_VLAN_NAME_INFO       LLDP_MAX_REM_VLAN
#define MAX_LLDP_REMOTE_PROTO_VLAN_INFO      LLDP_MAX_REM_PPVID
#define MAX_LLDP_REMOTE_PROTO_IDENTIFY_INFO  LLDP_MAX_REM_PROTOID
#define MAX_LLDP_REMOTE_PORT_INFO            (LLDP_MAX_NEIGHBORS + 1)
#define MAX_LLDP_APP_INFO                    (SYS_DEF_MAX_PHYSICAL_INTERFACES * LLDP_MAX_APPL * 0.5)
#define MAX_LLDP_APPL_TLV_COUNT              (MAX_LLDP_APP_INFO + 60)
#define MAX_LLDP_NEIGH_INFO                   1
#define MAX_LLDP_ARRAY_SIZE_INFO              8
#define MAX_LLDP_LEN_MAN_OID                  1
#define MAX_LLDP_PORT_TABLE                   ((SYS_DEF_MAX_PHYSICAL_INTERFACES) + 10)
#define MAX_LLDP_LOC_PORT_TABLE               256
#define MAX_LLDP_DEST_MAC_ADDR_TABLE          256
#define MAX_LLDP_AGENT_TO_LOC_PORT_TABLE      256
#define MAX_LLDP_MED_APP_TYPES                8
#define MAX_LLDP_MED_LOCATION_SUB_TYPES        3
#define MAX_LLDP_MED_LOC_NW_POLICY_TABLE     (SYS_DEF_MAX_PHYSICAL_INTERFACES * MAX_LLDP_MED_APP_TYPES)
#define MAX_LLDP_MED_REM_NW_POLICY_TABLE     (LLDP_MAX_NEIGHBORS * MAX_LLDP_MED_APP_TYPES)
#define MAX_LLDP_MED_LOC_LOCATION_TABLE      (SYS_DEF_MAX_PHYSICAL_INTERFACES * MAX_LLDP_MED_LOCATION_SUB_TYPES)
#define MAX_LLDP_MED_REM_LOCATION_TABLE      (LLDP_MAX_NEIGHBORS * MAX_LLDP_MED_LOCATION_SUB_TYPES)
#define MAX_LLDP_PDU_COUNT                    ((2 * SYS_DEF_MAX_PHYSICAL_INTERFACES) + 1)

/*End - LLDP sizing params */

/* - FPAM sizing parameters */
/* Maximum 20 users are allowed*/
#define MAX_FPAM_FSUSRMGMTTABLE       15
/* - FPAM End of Sizing parameters */

/* ROUTEMAP */
#define MAX_ROUTEMAP_NODES        100
#define MAX_ROUTEMAP_MATCH_NODES  1000 
#define MAX_ROUTEMAP_MATCH_LISTS  1000
#define MAX_ROUTEMAP_SET_NODES    100
#define MAX_IP_PREFIX_NODES       10

/* BGP */
#define MAX_BGP_PEER_GROUPS                  10 
#define MAX_BGP_PEERS                        50 
#define MAX_BGP_ROUTES                       5000 
#define MAX_BGP_ROUTE_INFO_ENTRIES           5000
#define MAX_BGP_RCVD_PATH_ATTRIBS            2500
#define MAX_BGP_ADVT_PATH_ATTRIBS            2500
#define MAX_BGP_AS_PATHS                     5000
#define MAX_BGP_LINK_NOCDES                  5000
#define MAX_BGP_ADVT_LINK_NODES              1250
#define MAX_BGP_Q_MSGS                       500
#define MAX_BGP_IGP_METRIC_ENTRIES           3750
#define MAX_BGP_IF_INFOS                     100
#define MAX_BGP_RT_REF_DATA                  100
#define MAX_BGP_MD5_AUTH_PASSWDS             100
#define MAX_BGP_TCPAO_AUTH_PASSWDS          256
#define MAX_BGP_SUPP_CAP_INFOS               2500
#define MAX_BGP_SNPA_INFO_NODES              5000
#define MAX_BGP_AFI_SAFI_SPEC_INFOS          200
#define MAX_BGP_EXT_COMM_IN_FILTER_INFOS     2000
#define MAX_BGP_EXT_COMM_OUT_FILTER_INFOS    2000
#define MAX_BGP_RT_CONF_EXT_COMMUNITIES      2500
#define MAX_BGP_EXT_COMM_PROFILES            25000
#define MAX_BGP_PEER_LINK_BANDWIDTHS         50
#define MAX_BGP_RT_EXT_COMM_SET_CONFIGS      2500
#define MAX_BGP_COMM_IN_FILTER_INFOS         10000
#define MAX_BGP_COMM_OUT_FILTER_INFOS        10000
#define MAX_BGP_RT_CONF_COMMUNITIES          2500
#define MAX_BGP_COMM_PROFILES                25000
#define MAX_BGP_RT_COMM_SET_CONFIGS          2500  
#define MAX_BGP_RT_DAMP_HISTORIES            2500
#define MAX_BGP_PEER_DAMP_HISTORIES          50
#define MAX_BGP_REUSE_PEERS                  50
/* Max number of blocks for the pool tBufNodeMsg  */  
#ifdef RM_WANTED
/* Max number of blocks for the pool tBufNodeMsg  */
 #define MAX_BGP_BUFNODE_MSGS                 300 
/* Max number of blocks for the pool tBufNode  */
 #define MAX_BGP_BUF_NODES                    1000 
#else 
/* Max number of blocks for the pool tBufNodeMsg  */
 #define MAX_BGP_BUFNODE_MSGS                 30
/* Max number of blocks for the pool tBufNode  */
 #define MAX_BGP_BUF_NODES                    100
#endif
/* Max number of blocks for the pool tClusterList  */  
#define MAX_BGP_CLUSTER_LIST                 500
/* Max number of blocks for the pool tAfiSafiNode  */  
#define  MAX_BGP_NETWORK_ROUTES              128
#define MAX_BGP_AFI_SAFI_NODE                500
/* Max number of blocks for the pool tCommunity  */  
#define MAX_BGP_COMMUNITY_NODE               500
/* Max number of blocks for the pool tExtCommunity  */  
#define MAX_BGP_EXT_COMM_NODE                500
/* Max number of blocks for the pool tAggregator  */  
#define MAX_BGP_AGGR_NODE                    500
/* Max number of blocks for the pool variable attributes with block size MAX_BGP_ATTR_LEN  */  
#define MAX_BGP_ATTRIBUTE_SIZE               500
/* Max number of blocks for the pool tPeernode  */  
#define MAX_BGP_PEER_NODE                    50
/* Max number of blocks for the pool tBufNodeMsg  */  
#define BGP_AS_SEG_LEN_LIMIT                 20
#define BGP_AS4_SEG_LEN_LIMIT                40
/*Maximum number of BGP instances */
#define MAX_BGP_CXT_NODES                    SYS_DEF_MAX_NUM_CONTEXTS
#define MAX_BGP_GLOBAL_CXT                   MAX_BGP_CXT_NODES
/* Max number of blocks for the pool  RFD_DECAY_ARRAY */
#define MAX_BGP_RFD_DECAY_ARRAY_BLOCKS       MAX_BGP_CXT_NODES
#define MAX_BGP_RFD_DECAY_ARRAY_SIZE         (8 * (3600 + 1))  /* MAX_BGP_RFD_DECAY_ARRAY_SIZE - 3600 
                                                                  if the value of the max RFD decay array size
                                                                  is to be increased, MAX_BGP_RFD_DECAY_ARRAY_SIZE
                                                                  also has to be increased */
/* RFD_DEF_MAX_HOLD_DOWN_TIME - 3600 and Float - 8 
 * if the value of RFD_DEF_MAX_HOLD_DOWN_TIME is 
 * increased this value should also be increased accordingly*/

/* Max number of blocks for the pool  RFD_REUSE_INDEX_ARRAY */
#define MAX_BGP_RFD_REUSE_INDEX_ARRAY_BLOCKS MAX_BGP_CXT_NODES
#define MAX_BGP_RFD_REUSE_INDEX_ARRAY_SIZE   ((1024+1) * 2) /* RFD_DEF_REUSE_INDEX_ARRAY_SIZE - 1024 
                                                               if the value of MAX_BGP_RFD_REUSE_INDEX_ARRAY_SIZE                                                                 is increased via system.size this value should 
                                                               also increased*/
/* Arraay size of au1ASSegs in  tAsPath Structure  */  
#define BGP4_AS_SEG_LEN                      BGP_AS_SEG_LEN_LIMIT
#define BGP4_AS4_SEG_LEN                     BGP_AS4_SEG_LEN_LIMIT
#define MAX_BGP_CLI_MEM_BUF                  20

/* Max number of blocks for the pool tBgp4PeerDataList  */
#define MAX_BGP_DATA_PEERS                   MAX_BGP_CXT_NODES
#define MAX_BGP_TIMERS                       (5 * MAX_BGP_CXT_NODES)
#define MAX_BGP_RM_MSGS                      50
#define MAX_BGP_ORF_ENTRIES                  100
#define MAX_BGP_RR_IMPORT_TARGETS            100
#define MAX_BGP_MAC_DUP_TIMERS               MAX_VXLAN_FSVXLANNVETABLE
#define MAX_BGP_LSPS                         MAX_LDP_LSPS

#define MAX_BGP_VRF_NODES                    MAX_BGP_CXT_NODES

#define MAX_BGP_MULTI_PATHS                       64
#define MAX_BGP_ADV_ROUTES_INFOS_SET              5000
/* IGMP */
#define MAX_IGMP_QUERIES  138
#define MAX_IGMP_LOGICAL_IFACES  138
#define MAX_IGMP_Q_DEPTH         150 
#define MAX_IGMP_MCAST_GRPS  255 
#define MAX_MLD_MCAST_GRPS  255 
#define MAX_IGMP_MCAST_SRCS  255
#define MAX_MLD_MCAST_SRCS  255 
#define MAX_IGMP_SRCS_FOR_MRP_PER_GROUP 255
#define MAX_IGMP_MCAST_SRS_REPORTERS  255
#define MAX_IGMP_MCAST_SCHED_QUERIES 255
#define MAX_IGMP_SCHED_QUERY_SRCS 255
#define MAX_IGMP_ADDR_ARRAY_STR  6
#define MAX_SRCS_TO_PKT_BLKS_U4 2
#define MAX_SRCS_TO_PKT_BLKS_U1 2
#define MAX_IGMP_RECVBUF_BLKS 2
#define MAX_IGMP_RM_QUE_DEPTH 10
#define MAX_IGMP_GROUPLIST_NODES 50
#define MAX_IGMP_SSM_MAP_SRC 5
#define MAX_IGMP_SSM_MAPPINGS 10

#define MAX_IGP_DYN_MSG 10
#define MAX_IGP_MCAST_GRPS 255
#define MAX_IGP_FWD_ENTRIES 255
#define MAX_IGP_RTR_IFACES 255
#define MAX_IGP_MAX_OIFS   35190
#define MAX_IGP_REP_BUF_MEMBLK 1
#define MAX_IGP_MCAST_SRCS 255
#define MAX_IGP_REP_BUF_MEMBLK_SIZE 1500

/* DVMRP */
#define MAX_DP_FWD_TBL_ENTRIES 100
#define MAX_DP_FWD_CACHE_TBL_ENTRIES 200
#define MAX_DP_HOST_MBR_TBL_ENTRIES 100
#define MAX_DP_NBR_TBL_ENTRIES 64
#define MAX_DP_ROUTE_TBL_ENTRIES 100
#define MAX_DP_NEXT_HOP_TBL_ENTRIES 100
#define MAX_DP_IFACE_FREE_POOL_TBL_ENTRIES 200
#define MAX_DP_NBR_FREE_POOL_TBL_ENTRIES 200
#define MAX_DP_SRC_FREE_POOL_TBL_ENTRIES 200
#define MAX_DP_MAX_IFACES 8
#define MAX_DP_Q_DEPTH 100
#define MAX_DP_MISC_BLOCKS 20
#define MAX_DP_MISC_BLOCK_SIZE 2048
#define MAX_DP_FWD_TBL_PTR             1
#define MAX_DP_FWD_CACHE_PTR           1
#define MAX_DP_HOST_MBR_PTR            1
#define MAX_DP_NBR_TBL_PTR             1
#define MAX_DP_ROUTE_TBL_PTR           1
#define MAX_DP_NEXT_HOP_PTR            1
#define MAX_DP_IFACE_LIST_PTR          1
#define MAX_DP_NBR_LIST_PTR            1
/*The MAX_MSDP_FSMSDPPEERTABLE is increased by 2 to allocate memory during creation of MSDP PEER TABLE for avoiding stack over flow of size 1024 bytes */
#define MAX_MSDP_FSMSDPPEERTABLE              (15 + 2)
#define MAX_MSDP_FSMSDPSACACHETABLE           15
#define MAX_MSDP_FSMSDPMESHGROUPTABLE         15
#define MAX_MSDP_FSMSDPRPTABLE                15
#define MAX_MSDP_FSMSDPPEERFILTERTABLE        15
#define MAX_MSDP_FSMSDPSAREDISTRIBUTIONTABLE  15
#define MAX_MSDP_QUEUE_MSGS                   50
#define MAX_MSDP_MESH_GROUPS                  10
  /*twice the MSDP_MAX_PEERS_ALLOWED*/
#define MAX_MSDP_PEER_RX_TX_BUFFER            64

/* This is for temporary usage. No need to scale the below set of  macros */
/* Macros used for temporary storage in MSDP module - START */
#define MAX_MSDP_FSMSDPPEERTABLE_ISSET              1
#define MAX_MSDP_FSMSDPSACACHETABLE_ISSET           1
#define MAX_MSDP_FSMSDPMESHGROUPTABLE_ISSET         1
#define MAX_MSDP_FSMSDPRPTABLE_ISSET                1
#define MAX_MSDP_FSMSDPPEERFILTERTABLE_ISSET        1
#define MAX_MSDP_FSMSDPSAREDISTRIBUTIONTABLE_ISSET  1
/* Macros used for temporary storage in MSDP module - END */

 /* PIM */
#define MAX_PIM_NBRS        24
#define MAX_PIM_Q_DEPTH     85
#define MAX_PIM_COMPONENTS   5
#define MAX_PIM_INTERFACES   8
 /*(MAX_PIM_COMPONENT_NBRS = MAX_PIM_NBRS * MAX_PIM_COMPONENTS)*/
#define MAX_PIM_COMPONENT_NBRS      120
 /*(MAX_PIM_COMPONENT_IFACE = MAX_PIM_INTERFACES * MAX_PIM_COMPONENTS)*/
#define MAX_PIM_COMPONENT_IFACES   40
#define MAX_PIM_SCOPES_PER_IF      11
   /*(MAX_PIM_SCOPES_PER_IF * MAX_PIM_INTERFACES) */
#define MAX_PIM_IF_SCOPE           88
#define MAX_PIM_MISC_OIFS       16 /*MAX_PIM_INTERFACES *2 */           
#define MAX_PIM_MISC_IIFS       16 /*MAX_PIM_INTERFACES *2 */
#define MAX_PIM_SEC_IP_ADDRS  132
#define MAX_PIM_HA_FPST_NODES  2500
#define MAX_PIM_BIDIR_DF_NODES 200/* MAX_PIM_RPS * MAX_PIM_INTERFACES */
#define MAX_PIM_GRP_MBRS       1275
#define MAX_PIM_GRP_SRC        1325
#define MAX_PIM_SRCS_INF      100
#define MAX_PIM_GRPS          1275
#define MAX_PIM6_GRPS        1275
#define MAX_PIM_ROUTE_ENTRY   12750
#define MAX_PIM6_ROUTE_ENTRY   12750
#define MAX_PIM_OIFS          102000
#define MAX_PIM_IIFS          102000
#define MAX_PIM_DATA_RATE_GRPS  1275
#define MAX_PIM_DATA_RATE_SRCS   12750
#define MAX_PIM_REG_RATE_MON     1275
#define MAX_PIM_CRPS             100
#define MAX_PIM_GRP_MASK         500
#define MAX_PIM_CRP_CONFIG       40
#define MAX_PIM_GRP_PFX          100
#define MAX_PIM_STATIC_GRP_RP    100
#define MAX_PIM_RPS              50
#define MAX_PIM_FRAG_GRPS        50
#define MAX_PIM_RP_GRPS          12750
#define MAX_PIM_REG_CHKSUM_NODE  50
#define MAX_PIM_MAX_ACTIVE_SRC_NODE  500
#define MAX_PIM_DM_GRAFT_RETX    25
#define MAX_PIM_DM_GRAFT_SRC    125
#define MAX_PIM_GRP_ADDR 1
#define MAX_PIM_RP_ADDR 1000

/* Syslog */

#define MAX_SYSLOG_FILE_ENTRY           200
#define MAX_SYSLOG_FWD_ENTRY            200
#define MAX_SYSLOG_MAIL_ENTRY           200
#define MAX_SYSLOG_LOGGING_ENTRY        8
#define MAX_SYSLOG_SMTPQ_DEPTH          10
#define MAX_SYSLOG_TCP_QUE_DEPTH        5 
#define MAX_SYSLOG_LOG_BUFF_USERS       CLI_MAX_USERS+ 2
#define MAX_SYSLOG_UINT_PKT             5
#define MAX_SYSLOG_STD_PKT              50
#define MAX_SYSLOG_CHR_PKT              3
#define MAX_SYSLOG_AUDIT_INFO           2


#define MAX_SYSLOG_USERS_LIMIT          CLI_MAX_USERS

/* RM */

#define MAX_RM_TX_BUF_NODES               25000
#define MAX_RM_CTRL_MSG_NODES             1000
#define MAX_RM_APPS                       RM_MAX_APPS
#define MAX_RM_NODE_INFO_BLOCKS           2 * MAX_RM_APPS
#define MAX_RM_NOTIFICATION_INFO          10
#define MAX_RM_RX_BUF_NODES               50000
#define MAX_RM_RX_BUF_PKT_NODES           50000
#define MAX_RM_PKTQ_MSG                   4000
#define MAX_RM_CONF_Q_DEPTH               50
#define MAX_RM_CONF_FAIL_BUF_BLOCKS       350
#define MAX_RM_PKT_MSG_BLOCKS             MAX_RM_TX_BUF_NODES + 16
#define MAX_RM_SYNC_SSNS                  16
#ifdef L2RED_TEST_WANTED
#define MAX_RM_APP_MSG                    16
#endif

#define RM_MAX_NOTIFICATION_INFO_LIMIT    10
#ifdef L2RED_WANTED
#define RM_MAX_SYNC_SSNS_LIMIT            1
#else
#define RM_MAX_SYNC_SSNS_LIMIT            16
#endif

/* ICCH */
#define ICCH_MAX_SYNC_SSNS_LIMIT          1
#define MAX_ICCH_CTRL_MSG_NODES           1000
#define MAX_ICCH_PKTQ_MSG                 4000
/* The below 3 parameters need to be scaled to a maximum value according to the 
 * maximum vlan FDB entries*/ 
#define MAX_ICCH_TX_BUF_NODES             2500
#define MAX_ICCH_RX_BUF_NODES             5000
#define MAX_ICCH_RX_BUF_PKT_NODES         5000
#define MAX_ICCH_PKT_MSG_BLOCKS           MAX_ICCH_TX_BUF_NODES + 16
#define MAX_ICCH_APPS                     ICCH_MAX_APPS
#define MAX_ICCH_NODE_INFO_BLOCKS         2 * MAX_ICCH_APPS
#define MAX_VLAN_MCAG_CONFIG_Q_MESG       100
/* In hit bit request packet, maximum entries carried is 30, so setting it to 40*/
#define MAX_VLAN_MCAG_MAC_AGING_ENTRIES    40

#define MAX_VLAN_PORT_VLAN_LIST           VLAN_MAX_PORTS 

/* UTIL CMN */
#define MAX_CMN_OIF_BLOCKS               192

/* RMON */

#define RMON_MAX_INTERFACE_LIMIT        (SYS_DEF_NUM_PHYSICAL_INTERFACES + 1) 

#define MAX_RMON_EVENT_NODES            50
#define MAX_RMON_ALARM_NODES            50
#define MAX_RMON_HISTORY_NODES          74
#define MAX_RMON_ETHER_STATS_NODES     100
#define MAX_RMON_TOPN_CONTROL_ENTRY     16
#define MAX_RMON_MATRIX_CTRL_BLOCKS     RMON_MAX_INTERFACE_LIMIT 
#define MAX_RMON_HOST_CTRL_BLOCKS       RMON_MAX_INTERFACE_LIMIT
#define MAX_RMON_LOG_EVENT_BLOCKS       MAX_RMON_EVENT_NODES
#define MAX_RMON_EVENT_COMM_BLOCKS      MAX_RMON_EVENT_NODES   
#define MAX_RMON_LOG_BUCKET_BLOCKS      MAX_RMON_EVENT_NODES
#define MAX_RMON_ALARM_VAR_BLOCKS       MAX_RMON_ALARM_NODES
#define MAX_RMON_ETHER_HIS_BLOCKS       MAX_RMON_HISTORY_NODES
#define MAX_RMON_HIS_BUCKETS_NODES      MAX_RMON_HISTORY_NODES
#define MAX_RMON_HOST_TABLE_BLOCKS      RMON_MAX_INTERFACE_LIMIT
#define MAX_RMON_MATRIX_BUCKETS_BLOCKS  RMON_MAX_INTERFACE_LIMIT
#define MAX_RMON_TOPN_REPORT_BLOCKS     MAX_RMON_TOPN_CONTROL_ENTRY
#define MAX_RMON_VLAN_BLOCKS            (VLAN_MAX_VLAN_ID + SYS_DEF_MAX_PHYSICAL_INTERFACES) 


#define RMON_MAX_TOPN_CONTROL_ENTRY_LIMIT  16
#define MAX_RMON_ALARM_ENTRY_NODES      1

/* RMON2 */
#define MAX_RMON2_PKTINFO                   400 /* Packet Information Memory Pool */ 
#define MAX_RMON2_QUEUE_SIZE                50
#define MAX_RMON2_PROTOCOLS                 50  /* Protocol Dir Table Memory Pool */
#define MAX_RMON2_CTRL                      50  /* Control Table Memory Pool */
#define MAX_RMON2_DATA                      100 /* Data Table Memory Pool */
#define MAX_RMON2_TOPNCTRL                  20  /* TopN Control Table Memory Pool */
#define MAX_RMON2_TOPN                      50  /* TopN Data Table Memory Pool */
#define MAX_RMON2_USRHISTCTRL               20  /* Usr Hist Control Table Memory Pool */
#define MAX_RMON2_USRHISTOBJ                50  /* Usr Hist Object Memory Pool */
#define MAX_RMON2_PDISTCTRL_BLOCKS          MAX_RMON2_CTRL
#define MAX_RMON2_PDISTSTATS_BLOCKS         MAX_RMON2_DATA
#define MAX_RMON2_ADDRMAPCTRL_BLOCKS        MAX_RMON2_CTRL
#define MAX_RMON2_ADDRMAP_BLOCKS            MAX_RMON2_DATA
#define MAX_RMON2_HLHOSTCTRL_BLOCKS         MAX_RMON2_CTRL
#define MAX_RMON2_NLHOST_BLOCKS             MAX_RMON2_DATA
#define MAX_RMON2_ALHOST_BLOCKS             MAX_RMON2_DATA
#define MAX_RMON2_HLMATRIXCTRL_BLOCKS       MAX_RMON2_CTRL
#define MAX_RMON2_NLMATRIXSD_BLOCKS         MAX_RMON2_DATA
#define MAX_RMON2_ALMATRIXSD_BLOCKS         MAX_RMON2_DATA
#define MAX_RMON2_NLMATRIX_TOPNCTRL_BLOCKS  MAX_RMON2_TOPNCTRL
#define MAX_RMON2_NLMATRIX_TOPN_BLOCKS      MAX_RMON2_TOPN
#define MAX_RMON2_ALMATRIX_TOPNCTRL_BLOCKS  MAX_RMON2_TOPNCTRL
#define MAX_RMON2_ALMATRIX_TOPN_BLOCKS      MAX_RMON2_TOPN
#define MAX_RMON2_HISOIDLIST_BLOCKS         MAX_RMON2_USRHISTOBJ * MAX_RMON2_USRHISTCTRL
#define MAX_RMON2_HISCTRLBUCKETS_BLOCKS     MAX_RMON2_USRHISTOBJ * MAX_RMON2_USRHISTCTRL

#define RMON2_MAX_PROTOCOLS_LIMIT           50
#define MAX_RMON2_ALMATRIX_SAMPLE_TOPN_BLOCKS 1
#define MAX_RMON2_NLMATRIX_SAMPLE_TOPN_BLOCKS 1

/* DSMON */
#define MAX_DSMON_PKTINFO              400 /* Packet Information Memory Pool */
#define MAX_DSMON_AGGCTLS              10  /* Aggregation Control Memory Pool */
#define MAX_DSMON_AGGCTL_GROUPS        100 /* Aggregation Group Memory Pool */
#define MAX_DSMON_CTLS                 50  /* Control Table Memory Pool */
#define MAX_DSMON_DATA                 100 /* Data Table Memory Pool */
#define MAX_DSMON_TOPNCTLS             20  /* TopN Control Table Memory Pool */
#define MAX_DSMON_TOPN                 50  /* TopN Report Memory Pool */
#define MAX_DSMON_STATSCTL             MAX_DSMON_CTLS
#define MAX_DSMON_STATS                MAX_DSMON_DATA
#define MAX_DSMON_PDISTCTL             MAX_DSMON_CTLS
#define MAX_DSMON_PDIST                MAX_DSMON_DATA
#define MAX_DSMON_PDIST_TOPNCTL        MAX_DSMON_TOPNCTLS
#define MAX_DSMON_PDIST_TOPN           MAX_DSMON_TOPN
#define MAX_DSMON_HOSTCTL              MAX_DSMON_CTLS 
#define MAX_DSMON_HOST                 MAX_DSMON_DATA
#define MAX_DSMON_HOST_TOPNCTL         MAX_DSMON_TOPNCTLS
#define MAX_DSMON_HOST_TOPN            MAX_DSMON_TOPN
#define MAX_DSMON_MATRICCTL            MAX_DSMON_CTLS
#define MAX_DSMON_MATRIXSD             MAX_DSMON_DATA
#define MAX_DSMON_MATRIX_TOPNCTL       MAX_DSMON_TOPNCTLS
#define MAX_DSMON_MATRIX_TOPN          MAX_DSMON_TOPN
#define MAX_DSMON_HOST_SAMPLE_TOPN     1
#define MAX_DSMON_MATRIX_SAMPLE_TOPN   1


/* TCP */
#define MAX_NUM_OF_TCB_LIMIT           2500

#define MAX_TCP_TCB_TBL_BLOCKS         500
#define MAX_TCP_SND_BUFF_BLOCKS        TCP_DEF_MAX_NUM_OF_TCB
#define MAX_TCP_RCV_BUFF_BLOCKS        TCP_DEF_MAX_NUM_OF_TCB
#define MAX_TCP_NUM_OF_FRAG            200
#define MAX_TCP_NUM_OF_SACK            200
#define MAX_TCP_NUM_OF_RXMT            200
#define MAX_TCP_TO_APP_BLOCKS          50
#define MAX_TCP_NUM_CONTEXT            SYS_DEF_MAX_NUM_CONTEXTS
#define MAX_TCP_NUM_PARAMS             10
#define MAX_TCP_IP_HL_PARMS            200
#define MAX_TCP_ICMP_PARAMS            25
#define MAX_TCP_CXT_STRUCTS            MAX_TCP_NUM_CONTEXT

/* Macros for TCP receive buffer size */
#define TCP_MIN_RECV_BUF_SIZE  (9 * 1024) /* 9 KB */
#define TCP_MAX_RECV_BUF_SIZE  0xffff  /* 64 KB */


/* SLI */
#ifdef TCP_WANTED
 #define  MAX_TCP_CONNECTIONS  TCP_DEF_MAX_NUM_OF_TCB
 #define  MAX_NO_OF_SOCKETS    (MAX_UDP_CONNECTIONS + MAX_TCP_CONNECTIONS)
#else
 #define  MAX_NO_OF_SOCKETS    MAX_UDP_CONNECTIONS
#endif


#define MAX_SLI_DESC_TBL_BLOCKS         1
#define MAX_SLI_SLL_NODES               1500
#define MAX_SLI_BUFF_BLOCKS             2
#define MAX_SLI_FD_ARR_BLOCKS           2
#define MAX_SLI_SDT_BLOCKS              MAX_NO_OF_SOCKETS
#define MAX_SLI_RAW_HASH_NODES          MAX_NO_OF_SOCKETS
#define MAX_SLI_RAW_RCV_Q_NODES 4000
#define MAX_SLI_ACCEPT_LIST_BLOCKS      MAX_NO_OF_SOCKETS
#define MAX_SLI_UDP4_BLOCKS             (MAX_NO_OF_SOCKETS / 2)
#define MAX_SLI_UDP6_BLOCKS             (MAX_NO_OF_SOCKETS / 2)
#define MAX_SLI_IP_OPT_BLOCKS           MAX_NO_OF_SOCKETS
#define MAX_SLI_MD5_SLL_NODES           (MAX_NO_OF_SOCKETS / 2)
#define MAX_SLI_TCPAO_SLL_NODES         (MAX_NO_OF_SOCKETS / 2)

/* MBSM */

#define MAX_MBSM_SLOT_INFO_BLOCKS            MBSM_MAX_SLOTS
#define MAX_MBSM_PORT_INFO_BLOCKS            MBSM_MAX_SLOTS
#define MAX_MBSM_LC_TYPES                    12 /* Number of differnt Line Card
                                              Supported */
/* define MAX_MBSM_CTRLQ_MSG_MEMBLK_COUNT     (MBSM_MAX_PORTS_PER_SLOT * 20) */
#define MAX_MBSM_CTRLQ_MSG_MEMBLK_COUNT      100
#define MAX_MBSM_MSG_QUEUE_DEPTH             40


/* MSR */
#define MSR_MAX_UPDATE_QUEUE_SIZE          50

#define MAX_MSR_MIBENTRY_BLOCKS            1
#define MAX_MSR_Q_DEPTH                    255
#define MAX_MSR_UPDATE_QUEUE_SIZE          MSR_MAX_UPDATE_QUEUE_SIZE + 1
#define MAX_MSR_CONFIG_DATA_BLOCKS         MSR_MAX_UPDATE_QUEUE_SIZE + 1
#define MAX_MSR_OID_ARR_LIST_BLOCKS        MAX_MSR_UPDATE_QUEUE_SIZE * 5
#define MAX_MSR_OID_ARR_BLOCKS             MAX_MSR_UPDATE_QUEUE_SIZE
#define MAX_MSR_OID_LIST_BLOCKS            MAX_MSR_UPDATE_QUEUE_SIZE
#define MAX_MSR_RM_QUEUE_MSG               10
#define MAX_MSR_RM_HDR_BLOCKS              20
#define MAX_MSR_RM_PKT_BLOCKS              20
#define MAX_MSR_INCR_RB_NODES              50000
#define MAX_MSR_INCR_OID_BLOCKS            50000
#define MAX_MSR_DATA_BLOCKS                10
/* WebNM */

#define MAX_HTTP_DUP_MSG_NODES             ISS_MAX_WEB_SESSIONS
#define MAX_HTTP_MSG_NODES                 ISS_MAX_WEB_SESSIONS
#define MAX_HTTP_PROCCESSING_TASKS         3
#define MAX_HTTP_REDIRECTION_ENTRY_BLOCKS  10
#define MAX_HTTP_CLT_INFO_BLOCKS           4 * (ISS_MAX_SSL_SESSIONS + \
                                                ISS_MAX_WEB_SESSIONS)
#define MAX_HTTP_WEBNM_RESP_BLOCKS         10
#define MAX_HTTP_Q_DEPTH                   10
#define MAX_HTTP_WEBNM_USER_NODES          12 /*To Accomodate 3 paralell http tasks, this value
                                                should be always HTTP_MAX_LISTEN + 2*/
#define MAX_WEBNM_TACACS_BLOCKS            1
#define MAX_ENM_BLOCKS                     2

#define HTTP_MAX_NUM_PROCESSING_TASKS_LIMIT    3

/* System */

#define MAX_ISS_CONFIG_ENTRIES             SYS_DEF_MAX_PHYSICAL_INTERFACES + 1
#define MAX_ISS_PORT_ENTRIES               SYS_DEF_MAX_PHYSICAL_INTERFACES + 1
#define MAX_ISS_MIRR_DEST_RECORD_BLOCKS    100
#define MAX_ISS_MIRR_SRC_RECORD_BLOCKS     100
#define MAX_ISS_L4SFILTERENTRY_BLOCKS      ISS_MAX_L4S_FILTERS
/* This macro identifies the maximum number of Port Isolation entries
 * supported by the system. 
 * The value of the macro is derived as follows.
 * MAX_ISS_PORT_ISOLATION_ENTRIES = ISS_MAX_PORT_ISOLATION_SUPP_PORTS +
 *                                  (x*ISS_MAX_ISOLATED_VLANS))
 * where, x - number of the trunk ports in the switch.
 * MAX_ISS_PORT_ISOLATION_ENTRIES = 1000 + (1*256) = 1256                   
 */
#define MAX_ISS_PORT_ISOLATION_ENTRIES     1256 
#define MAX_ISS_PI_EGRESS_PORT_ENTRIES     1254 
#define MAX_ISS_FIRMWARE_BLOCKS            1
#define MAX_ISS_ENT_PHY_ENTRIES            2
#define MAX_ISS_CONFIG_ENTRIES_LIMIT       SYS_DEF_MAX_PHYSICAL_INTERFACES + 1
#define MAX_ISS_PORT_ENTRIES_LIMIT         SYS_DEF_MAX_PHYSICAL_INTERFACES + 1

/* Snmpv3 */
#define  SNMP_PROXY_CACHE_HASH_SIZE             1000
#define  SNMP_PROXY_CACHE_ELEMENT_SIZE          1000
#define  SNMP_PROXY_CACHE_ELEMENT_EXHAUSTED     1000

#define MAX_SNMP_GET_VARBIND_BLOCKS          2
#define MAX_SNMP_PKT_POOL_BLOCKS             12
#define MAX_SNMP_NORMAL_PDU_BLOCKS           10
#define MAX_SNMP_TRAP_PDU_BLOCKS             2
#define MAX_SNMP_V3PDU_BLOCKS                2
#define MAX_SNMP_INFORM_NODE_BLOCKS          50
#define MAX_SNMP_INFORM_MSG_NODE_BLOCKS      50
#define MAX_SNMP_TCP_RCV_BUF_BLOCKS          2
#define MAX_SNMP_TCP_TX_NODE_BLOCKS          10
#define MAX_SNMP_VARBIND_BLOCKS              500
#define MAX_SNMP_OID_TYPE_BLOCKS             500
#define MAX_SNMP_OID_LIST_BLOCKS             500
#define MAX_SNMP_OCTET_STR_BLOCKS            500
#define MAX_SNMP_OCTET_LIST_BLOCKS           500
#define MAX_SNMP_MULTI_DATA_BLOCKS           500
#define MAX_SNMP_INDEX_BLOCKS                500
#define MAX_SNMP_MULTI_DATA_INDEX_BLOCKS     500
#define MAX_SNMP_MULTI_OID_BLOCKS            500
#define MAX_SNMP_TCP_CLIENTS                 5 
#define MAX_SNMP_TCP_SSN_RCV_BUF_BLOCKS      MAX_SNMP_TCP_CLIENTS
#define MAX_SNMP_TCP_CLIENT_Q_DEPTH          10
#define MAX_SNMP_RED_Q_DEPTH                 100
#define MAX_SNMP_DATA_BLOCKS                 10
#define MAX_SNMP_AUDIT_INFO_BLOCKS           10
#define MAX_SNMP_TAG_LIST_BLOCKS             10


#define MAX_SNMP_TCP_CLIENTS_LIMIT           5
#define MAX_SNMP_AGENTX_OID_BLOCKS           500
#define MAX_SNMP_AGENTX_VARBIND_BLOCKS       500
#define MAX_SNMP_SEARCH_RANGE_BLOCKS         500
#define MAX_SNMP_TRAP_FILTER_TABLE_ENTRIES   20
#define MAX_SNMP_AGENT_PARAM_BLOCKS          5

/* CLI */

#define MAX_CLI_USERS_BLOCKS                (CLI_MAX_USERS + 1)
#define MAX_CLI_GROUPS_BLOCKS               (CLI_MAX_GROUPS + 1)
#define MAX_CLI_PRIV_BLOCKS                 17
#define MAX_CLI_ALIAS_BLOCKS                40
#define MAX_CLI_CMD_LIST_BLOCKS             1200
#define MAX_CLI_CMD_HELP_BLOCKS             1200
#define MAX_CLI_TOKENS                      14550
#define MAX_CLI_TOKEN_RANGE_BLOCKS          MAX_CLI_TOKENS
#define MAX_CLI_CMD_TOKENS_BLOCKS           (3*MAX_CLI_TOKENS)
#define MAX_CLI_CMD_MODES                   140
#define MAX_CLI_CTX_BUF_ARR_BLOCKS          CLI_MAX_SESSIONS
#define MAX_CLI_CTX_OP_BUF_BLOCKS           CLI_MAX_SESSIONS
#define MAX_CLI_CTX_PEND_BUF_BLOCKS         CLI_MAX_SESSIONS * 2
#define MAX_CLI_AMB_PTR_BLOCKS              CLI_MAX_SESSIONS
#define MAX_CLI_CTX_HELP_BLOCKS             CLI_MAX_SESSIONS
#define MAX_CLI_AMB_ARR_BLOCKS              CLI_MAX_SESSIONS * 400
#define MAX_CLI_HELP_BLOCKS                 1
#define MAX_CLI_AMB_MEM_BLOCKS              2  /* One for Application context
                                                    and other for the cli
                                                    context sessions */
#define MAX_CLI_MAX_LINE_BLOCKS             120
#define MAX_CLI_USRGRP_BLOCKS               207
#define MAX_CLI_CMD_HELP_PRIV_BLOCKS        (2 * MAX_CLI_CMD_HELP_BLOCKS)
#define MAX_CLI_RED_Q_DEPTH                 100
#define MAX_CLI_TAC_MSG_INPUT               2
#define MAX_CLI_AUDIT_INFO                  2
#define MAX_CLI_OUTPUT_BUF_BLOCKS           10

/* CFA */
#define MAX_CFA_PORT_ARRAY                    1
#define MAX_CFA_IPIF_RECORDS_IN_SYSTEM       (IP_DEV_MAX_L3VLAN_INTF + IP_MAX_RPORT + SYS_MAX_PPP_INTERFACES)
                                                /* Macro used to allocate memory for
                                                 * * tIpIfRecord */
#define MAX_CFA_IPADDR_INFO_IN_SYSTEM        ((IP_DEV_MAX_ADDR_PER_IFACE - 1) * \
                                              (IP_DEV_MAX_L3VLAN_INTF + IP_MAX_RPORT \
                                               + LA_DEV_MAX_TRUNK_GROUP))
                                                 /* Macro used to allocate memory for                                                            * tIpAddrInfo */ 
#if defined (IGS_WANTED) || defined (MLDS_WANTED)
#define MAX_CFA_PKT_PORTLIST_Q_DEPTH          (SNOOP_MAX_IGMP_PKTS_PROCESSED_IN_A_SEC + CFA_NUM_INTERFACES_IN_SYSTEM)
#else
#define MAX_CFA_PKT_PORTLIST_Q_DEPTH           CFA_NUM_INTERFACES_IN_SYSTEM
#endif

#define MAX_CFA_EXT_TRIGGER_QUEUE_DEPTH        (2 * CFA_NUM_INTERFACES_IN_SYSTEM)
#define MAX_CFA_DEF_INTF_IN_SYS                 (SYS_DEF_MAX_INTERFACES + \
                                                 CFA_MAX_NUM_OF_VIP + \
                                                 CFA_MAX_SISP_INTERFACES)
                                                  /* Macro used to allocate memory for
                                                   * gaCfaCommonIfInfo */
#define MAX_CFA_MBSM_SLOT_MSG_BLOCKS             2
#define MAX_CFA_MBSM_PROTO_MSG_BLOCKS            2
#define MAX_CFA_PKT_POOL_BLOCKS                  2
#define MAX_CFA_DRIVER_MTU_BLOCKS                2 
#define MAX_CFA_STACK_BLOCKS                     SYS_DEF_MAX_INTERFACES * 2
#define MAX_CFA_SISP_INTERFACES                  SYS_DEF_MAX_SISP_IFACES + 1
#define MAX_CFA_RCV_ADDR_BLOCKS                  SYS_DEF_MAX_INTERFACES * 2
#define MAX_CFA_IFSTATUS_PROP_BLOCKS             SYS_DEF_MAX_INTERFACES
#define MAX_CFA_TUNL_CONTEXT                     SYS_DEF_MAX_NUM_CONTEXTS
#define MAX_CFA_TUNL_INTERFACES                  SYS_DEF_MAX_TUNL_IFACES
#define MAX_CFA_RED_BUF_ENTRIES                  15
#define MAX_CFA_HL_APPLS                         16
#define MAX_CFA_IFINFO_INTF_IN_SYS               CFA_MAX_INTERFACES_IN_SYS
#ifdef WTP_WANTED
#define MAX_CFA_GDD_REG_BLOCKS                 SYS_DEF_MAX_PHYSICAL_INTERFACES \
                                               + 1 + MAX_NUM_OF_BSSID_PER_RADIO
#elif WLC_WANTED
#define MAX_CFA_GDD_REG_BLOCKS                 SYS_DEF_MAX_PHYSICAL_INTERFACES \
                                               + 1 \
                                               + (MAX_NUM_OF_BSSID_PER_RADIO * \
                                                  MAX_NUM_OF_RADIOS)
#else
#define MAX_CFA_GDD_REG_BLOCKS                   SYS_DEF_MAX_PHYSICAL_INTERFACES + 1 \
                                                 + SYS_MAX_HDLC_INTERFACES 
#endif
#define MAX_CFA_TLV_INFO_IN_SYSTEM               BRG_NUM_PHY_PLUS_LOG_PORTS
#define MAX_CFA_ENET_IWF_BLOCKS                  SYS_DEF_MAX_INTERFACES
#define MAX_CFA_MAU_INTERFACES           BRG_NUM_PHY_PLUS_LOG_PORTS

#define MAX_CFA_PKTGEN                           10
#define MAX_CFA_PKTGEN_RX_INFO                    5
#define MAX_CFA_PKTGEN_TX_INFO                    5

#define MAX_CFA_HL_APPLS_LIMIT                   16
#define MAX_CFA_IPIF_BLOCKS                      CFA_MAX_INTERFACES_IN_SYS
#define MAX_CFA_WAN_BLOCKS                       CFA_MAX_INTERFACES_IN_SYS
#define MAX_CFA_UFD_GROUP_INFO_BLOCKS                8
/* Maximum number of Uplink Ports supported for Port Isolation.*/
#if ((defined ICCH_WANTED) && !(defined NPSIM_WANTED))
/* When ICCH is enabled the maximum number of egress ports in a 
 * Port Isolation entry corresponds to the number of physical interfaces
 * in the System + MAX LA interfaces. 
 */
#define ISS_MAX_UPLINK_PORTS               30
#else
#define ISS_MAX_UPLINK_PORTS               10
#endif
/* Maximum number of Port Isolation Supported ports.
 * It defines the number of ingress ports in the Port Isolation table. 
 * This macro value is derived from the below formulae. 
 * ISS_MAX_PORT_ISOLATION_SUPP_PORTS  = (SYS_DEF_NUM_PHYSICAL_INTERFACES + \
 *                                      LA_MAX_AGG + SYS_DEF_MAX_L2_PSW_IFACES - \
 *                                      ISS_MAX_UPLINK_PORTS)
 * ISS_MAX_PORT_ISOLATION_SUPP_PORTS = 1000 + 8 + 2 - 10
 */                                      
#define ISS_MAX_PORT_ISOLATION_SUPP_PORTS  1000    
/* This macro identifies the maximum number of Port Isolation entries
 * supported by the system.
 * The value of the macro is derived as follows.
 * ISS_MAX_PORT_ISOLATION_ENTRIES = ISS_MAX_PORT_ISOLATION_SUPP_PORTS +
 *                                  (x*ISS_MAX_ISOLATED_VLANS))
 * where, x - number of the trunk ports in the switch.
 * ISS_MAX_PORT_ISOLATION_ENTRIES = 998 + (1*256) = 1254
 */
#define ISS_MAX_PORT_ISOLATION_ENTRIES     1254

/* NETIPVX 
 * Maximum number of Trace Route Table instances */
#define MAX_TRACE_MAX_INSTANCES              11
#define MAX_TRACE_INFO_ENTRIES               (MAX_TRACE_MAX_INSTANCES * 99 * 3)
 
/* BFD */

/* The number of blocks for MAX_BFD_FSMISTDBFDGLOBALCONFIGTABLE is 
 * incremented by one for handling memory allocation for Set ,IsSet 
 * and BfdGblCreateGlobalConfig incase of SYS_DEF_MAX_NUM_CONTEXTS set to 1.
 */
#define MAX_BFD_FSMISTDBFDGLOBALCONFIGTABLE       (BFD_MAX_CONTEXT + 2)
#define MAX_BFD_FSMISTDBFDSESSTABLE       1024
#define MAX_BFD_FSMISTDBFDSESSDISCMAPTABLE       MAX_BFD_FSMISTDBFDSESSTABLE
#define MAX_BFD_FSMISTDBFDSESSIPMAPTABLE       MAX_BFD_FSMISTDBFDSESSTABLE
#define MAX_BFD_QUEMSG               100
#define MAX_BFD_MPLSPATHINFOSTRUCT   2
#define MAX_BFD_EXTOUTPARAMS         5
#define MAX_BFD_EXTINPARAMS          5 
#define MAX_BFD_PKTBUF               2
#define MAX_BFD_OFFLOAD_PARAMS       2

#define MAX_DNS_PENDING_QUERIES    10
#define MAX_DNS_NAME_SERVERS       8
#define MAX_DNS_DOMAINS            4
#define MAX_DNS_BITMAP_SIZE        10
#define MAX_DNS_INDICES            MAX_DNS_NAME_SERVERS
#define MAX_DNS_CACHE_ENTRIES      20
#define MAX_DNS_PKT_LEN            10
#define MAX_DNS_Q_MSG          50 
#define MAX_DNS_SRV    22

/* Max WAN interfaces in the System */
#define SYS_DEF_MAX_WAN_INTERFACES          (SYS_DEF_MAX_PHYSICAL_INTERFACES +\
                                             SYS_MAX_PPP_INTERFACES - 1)

/* START -PBB-TE Sizing Params */  
#define   MAX_PBBTE_RED_BUFER_ENTRIES       50  
#define   MAX_PBBTE_CONFIG_Q_MESG           20   
#define   MAX_PBBTE_CONTEXT_INFO            SYS_DEF_MAX_NUM_CONTEXTS 
#define   MAX_PBBTE_SERVICE_ID_ESP_ENTRIES  PBBTE_MAX_TESID_ENTRIES  
#define   MAX_PBBTE_ESP_DMAC_VID_INFO       PBBTE_MAX_TESID_ESP_DA_VID_PAIRS  
/* END -  PBBTE SIZING PARAMS */  

/* START - EVCPRO Sizing Params */ 
#define MAX_EVCPRO_LOCAL_MESG           ((SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG )* 10 ) 
#define MAX_EVCPRO_Q_MESG               ((SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG) * 2 )
#define MAX_EVCPRO_CONFIG_MESG          ((SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG) * 2 )
/* END - EVCPRO SIZING PARAMS */

/* GARP Sizing Params */
#define MAX_GARP_CONFIG_Q_MESG        (L2IWF_MAX_PORTS_PER_CONTEXT * 4)
#define MAX_GARP_BULK_MSG_COUNT       48
#define MAX_GARP_CONTEXT_INFO         SYS_DEF_MAX_NUM_CONTEXTS
#define MAX_GARP_PORT_ENTRIES         (L2IWF_MAX_PORTS_PER_CONTEXT * 2)
/* MAX_GARP_PORT_STAT_ENTRIES can be set to (L2IWF_MAX_PORTS_PER_CONTEXT ) if the GARP Statistics are needed
 * or can be set to ONE (minimum, in this case memory will be allocated to only 1st port)
 * if GARP stats are not needed to be accumulated */
#define MAX_GARP_PORT_STAT_ENTRIES    L2IWF_MAX_PORTS_PER_CONTEXT  
#define MAX_GARP_ATTR_ENTRIES         ((((BRG_MAX_PHY_PLUS_LOG_PORTS) *(VLAN_MAX_CURR_ENTRIES))+ \
                                        (((VLAN_DEV_MAX_MCAST_TABLE_SIZE)+2) * GARP_MAX_PORTS)) / 2)

#define MAX_GARP_GIP_ENTRIES        ((VLAN_DEV_MAX_VLAN_ID * (BRG_MAX_PHY_PLUS_LOG_PORTS)) +\
                                    (AST_MAX_MST_INSTANCES * BRG_MAX_PHY_PLUS_LOG_PORTS))
/* Instance-VLAN Map:
 * Single post includes information for VLAN_MAP_INFO_PER_POST (b) here 250 VLANs.
 * For one command VLAN_DEV_MAX_VLAN_ID (a) can be mapped.
 * So (a)/(b) msg posts.
 * AST_MAX_MST_INSTANCES (c) can be mapped at the max.
 * So MemPool is allocated for (c)*[(a)/(b)]
 */
#define MAX_GARP_VLAN_MAP_MSG_COUNT ((AST_MAX_MST_INSTANCES) *(VLAN_DEV_MAX_NUM_VLAN / VLAN_MAP_INFO_PER_POST)) 

#define MAX_GARP_MAX_APP_HASH_ARRAY_COUNT  (2 * SYS_DEF_MAX_NUM_CONTEXTS)
#define MAX_GARP_REMAP_ATTR_ENTRIES        (((VLAN_DEV_MAX_MCAST_TABLE_SIZE) + \
                                             2) * L2IWF_MAX_PORTS_PER_CONTEXT)
#define MAX_GARP_GLOBAL_PORT_ENTRIES       L2IWF_MAX_PORTS_PER_CONTEXT
#define MAX_GARP_MSG_COUNT                 (SYS_DEF_MAX_NUM_CONTEXTS + 2)
#define MAX_GARP_GLOBAL_PVLAN_ENTRIES      1
#define MAX_GARP_GVRP_ATTR_HASH_BUF        (MAX_GARP_PORT_ENTRIES * SYS_DEF_MAX_NUM_CONTEXTS) 
#define MAX_GARP_GMRP_ATTR_HASH_BUF        (MAX_GARP_PORT_ENTRIES * SYS_DEF_MAX_NUM_CONTEXTS)
/* END - GARP SIZING PARAMS */

/* PBB Sizing Params */
#define  MAX_PBB_Q_MESG                       (L2IWF_MAX_PORTS_PER_CONTEXT*4)
#define  MAX_PBB_CONTEXT_INFO                 SYS_DEF_MAX_NUM_CONTEXTS
#define  MAX_PBB_ISID_RBTREE_NODES            (PBB_MAX_NUM_OF_ICOMP_ISID + PBB_MAX_NUM_OF_BCOMP_ISID)
#define  MAX_PBB_BACKBONE_SERV_INST_ENTRIES   PBB_MAX_NUM_OF_BCOMP_ISID
#define  MAX_PBB_PORT_RBTREE_NODES            PBB_MAX_PORTS_IN_SYSTEM
#define  MAX_PBB_ICOMP_VIP_RBTREE_NODES       PBB_MAX_NUM_OF_ICOMP_ISID
#define  MAX_PBB_CBP_PORT_LIST_COUNT          PBB_MAX_PORTS_IN_SYSTEM
#define  MAX_PBB_CNP_PORT_LIST_COUNT          PBB_MAX_PORTS_IN_SYSTEM
#define  MAX_PBB_ISID_PIP_VIP_NODES           (PBB_MAX_NUM_OF_ICOMP_ISID + PBB_MAX_NUM_OF_BCOMP_ISID)
#define  MAX_PBB_PCP_DATA_COUNT               PBB_MAX_PORTS_IN_SYSTEM
#define  MAX_PBB_INSTANCE_INFO                SYS_DEF_MAX_NUM_CONTEXTS
#define  MAX_PBB_CONTEXTS                     SYS_DEF_MAX_NUM_CONTEXTS
#define  MAX_PBB_RED_BUFFER_ENTRIES           55
#define  MAX_PBB_PORTS_PER_ISID_ENTRIES       2
/* END -  PBB  SIZING PARAMS */

/*START - FSAP Sizing params*/
#define MAX_FSAP_SYS_MEMPOOLS_SIZE                  1000
#define MAX_FSAP_CRU_BUF_CHAIN_FREE_QUE_DESC        1
#define MAX_FSAP_CRU_BUF_DATABLK_FREE_QUE_DESC      2
#define MAX_FSAP_CRU_BUF_DATADESC_FREE_QUE_DESC     1
#define MAX_FSAP_CRU_BUF_CHAIN_DESC                 800
#define MAX_FSAP_CRU_BUF_DATA_DESC                  4000
#define MAX_FSAP_CRU_BUF_DATA_BLOCKS                800
#define MAX_FSAP_TIMER_LISTS                        75
#define MAX_FSAP_STEPS_SIZE                         100
#define MAX_FSAP_BUDDDY_INST                        20
/*END - FSAP Sizing params*/

/*Start ECFM Sizing Params */
#define  MAX_ECFM_CC_Q_MESG             ((ECFM_MAX_PORTS_IN_SYSTEM*5)+\
                                         (ECFM_MAX_MEP_IN_SYSTEM/5)+\
                                         (ECFM_MAX_PORTS_IN_SYSTEM*3))
#define  MAX_ECFM_CC_CONTEXT_INFO       SYS_DEF_MAX_NUM_CONTEXTS 
#define  MAX_ECFM_CC_VLAN_INFO          ECFM_MAX_VLAN_IN_SYSTEM
#define  MAX_ECFM_LBLT_VLAN_INFO          ECFM_MAX_VLAN_IN_SYSTEM
#define  MAX_ECFM_CC_PORT_INFO          ECFM_MAX_PORTS_IN_SYSTEM
#define  MAX_ECFM_CC_STACK_INFO         (ECFM_MAX_MEP +\
                                        (ECFM_MAX_MIP*2))
#define  MAX_ECFM_CC_MD_INFO            ECFM_MAX_MD  
#define  MAX_ECFM_CC_DEF_MD_INFO        ECFM_MAX_DEF_MD  
#define  MAX_ECFM_CC_MA_INFO            ECFM_MAX_MA_IN_SYSTEM
#ifdef MI_WANTED
#define  MAX_ECFM_CC_MEP_LIST_INFO      ((ECFM_MAX_MA_IN_SYSTEM * \
                                         4) \
                                        + ECFM_MAX_MEP_IN_SYSTEM)

#define  MAX_ECFM_CC_RMEP_DB_INFO       (ECFM_MAX_MA_IN_SYSTEM * \
                                         4)

#define  MAX_ECFM_LBLT_RMEP_DB_INFO     (ECFM_MAX_MA_IN_SYSTEM * 4)
#else
#define  MAX_ECFM_CC_MEP_LIST_INFO      ((ECFM_MAX_MA_IN_SYSTEM * \
                                         2) \
                                        + ECFM_MAX_MEP_IN_SYSTEM)

#define  MAX_ECFM_CC_RMEP_DB_INFO       (ECFM_MAX_MA_IN_SYSTEM * \
                                         2)
#define  MAX_ECFM_LBLT_RMEP_DB_INFO     (ECFM_MAX_MA_IN_SYSTEM * 2)
#endif
#define  MAX_ECFM_CC_MEP_INFO           ECFM_MAX_MEP_IN_SYSTEM
#define  MAX_ECFM_CC_MIP_INFO           ECFM_MAX_MIP_IN_SYSTEM
#define  MAX_ECFM_CC_MIP_PREVENT_INFO   ECFM_MAX_MIP_IN_SYSTEM
#define  MAX_ECFM_CC_CONFIG_ERR_INFO    (ECFM_MAX_PORTS_IN_SYSTEM * \
                                            2)
#define  MAX_ECFM_CC_REG_APPS           3 
#define  MAX_ECFM_CC_PORT_ARRAY_COUNT   2
#define  MAX_ECFM_LBLT_Q_MESG           ECFM_MAX_PORTS_IN_SYSTEM + \
                                        (ECFM_MAX_PORTS_IN_SYSTEM*8)
#define  MAX_ECFM_LBLT_CONTEXT_INFO     SYS_DEF_MAX_NUM_CONTEXTS 
#define  MAX_ECFM_LBLT_PORT_INFO        ECFM_MAX_PORTS_IN_SYSTEM
#define  MAX_ECFM_LBLT_MEP_INFO         ECFM_MAX_MEP_IN_SYSTEM
#define  MAX_ECFM_LBLT_MIP_INFO         ECFM_MAX_MIP_IN_SYSTEM
#define  MAX_ECFM_LBLT_STACK_INFO       (ECFM_MAX_MEP +\
                                        (ECFM_MAX_MIP*2))
#define  MAX_ECFM_LBLT_LTM_REPLY_LIST_INFO      (10 * ECFM_MAX_MEP)
#define  MAX_ECFM_LBLT_LBM_INFO                 (10 * ECFM_MAX_MEP)
#define  MAX_ECFM_LBLT_MA_INFO                   ECFM_MAX_MA_IN_SYSTEM
#define  MAX_ECFM_LBLT_MD_INFO                   ECFM_MAX_MD 
#define  MAX_ECFM_LBLT_DELAY_QUEUE_NODES         128
#define  MAX_ECFM_LBLT_DEF_MD_INFO               ECFM_MAX_DEF_MD 
#define  MAX_ECFM_LBLT_REG_APPS                  3
#define  MAX_ECFM_AIS_MEP_IN_SYSTEM              ECFM_MAX_MEP_IN_SYSTEM
#define  MAX_ECFM_LCK_MEP_IN_SYSTEM              ECFM_MAX_MEP_IN_SYSTEM
#define MAX_ECFM_CC_MEP_MPTP_PARAMS_SIZE         100
#define MAX_ECFM_MPLSTP_OUTPARAMS_SIZE           2
#define MAX_ECFM_MPLSTP_INPARAMS_SIZE            2
#define MAX_ECFM_MPLSTP_PATHINFO_SIZE            2
#define MAX_ECFM_MPTP_LBLT_OUTPARAMS_SIZE        2
#define MAX_ECFM_MPTP_LBLT_INPARAMS_SIZE         2
#define MAX_ECFM_MPTP_LBLT_PATHINFO_SIZE         2
#define MAX_ECFM_MPLSTP_SERVICE_PTR_SIZE        ECFM_MPLSTP_MAX_SERVICE_PTRS_IN_SYSTEM
#define MAX_ECFM_MAX_CHASSISID_LEN              3072
#define MAX_ECFM_MAX_MGMT_DOMAIN_LEN            3072 
#define MAX_ECFM_MAX_MAN_ADDR_LEN               3072
#define MAX_ECFM_MAX_LBM_PDU_SIZE               10
#define MAX_ECFM_LBM_DATA_TLV_LEN_MAX           10
#define MAX_ECFM_LTR_CACHE_STRUCT_SIZE          1
#define MAX_ECFM_AVLBLTY_INFO_SIZE              3072
#define MAX_ECFM_CC_MEP_ERR_CCM_SIZE            192
#define MAX_ECFM_CC_MEP_XCON_CCM_SIZE           192
#define MAX_ECFM_LBR_RCVD_INFO_SIZE             50
#define MAX_ECFM_ACTIVE_MA_LEVEL                1
#define MAX_ECFM_ACTIVE_MD_LEVEL                1

/* END - ECFM SIZING PARAMS */
/* OSPFTE */
#define  MAX_OSPF_TE_INTF            10
#define  MAX_OSPF_TE_OSINT_INFO      10
#define OSPF_TE_MAX_LSA_SIZE          512 /* This is equivalen to MAX_LSA_SIZE*/
#define  MAX_OSPF_TE_DESC            1530 /* OSPF_TE_MAX_DESC */
#define OSPF_TE_MAX_ROUTERS            50
#define OSPF_TE_MAX_IF_DESC            3
#define MAX_OSPF_TE_Q_DEPTH            20
#define MAX_OSPF_TE_LSAS               800
#define MAX_OSPF_TE_LSAS_NODE              800
#define MAX_OSPF_TE_CONS               100
#define MAX_OSPF_TE_CSPF_REQUEST       100
#define MAX_OSPF_TE_CANDTE_NODE        OSPF_TE_MAX_ROUTERS
#define  MAX_OSPF_TE_AREAS           5
#define  MAX_OSPF_TE_DB_NODE         50
#define MAX_OSPF_TE_LINK_TLV_NODE      (OSPF_TE_MAX_ROUTERS * MAX_OSPF_TE_INTF)
#define MAX_OSPF_TE_PATH_NODE          (OSPF_TE_MAX_ROUTERS * MAX_OSPF_TE_INTF)
#define MAX_OSPF_TE_RM_LINKS            40
#define MAX_OSPF_TE_RM_IF_DESCRS        10
#define MAX_OSPF_TE_SRLG_NO             16
#define MAX_OSPF_TE_LINK_TLV_SIZE       (4 + 120 + 4 + MAX_OSPF_TE_SRLG_NO * 4 \
                          + (4 + 44) * OSPF_TE_MAX_IF_DESC)
#define MAX_OSPF_TE_RM_LINK_INFO        800
#define MAX_OSPF_TE_RM_LINK_SRLG_INFO   800 
#define MAX_OSPF_TE_RM_LINK_MSG_BLOCKS  800
#define MAX_OSPF_TE_MAX_NET_NBRS        MAX_OSPF_TE_LSAS
#define  MAX_OSPF_TE_LINK_TLV        10
/* LCM */
#define  MAX_LCM_EVC_ENT_IN_PRT      ((SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG)* SYS_DEF_MAX_EVC_PER_PORT)
#define  MAX_LCM_LCL_EVC_INF         SYS_MAX_NUM_EVCS_SUPPORTED
#define  MAX_LCM_PRT_ENT_IN_EVC      ((SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG) * SYS_DEF_MAX_EVC_PER_PORT)

/*START - VCM Sizing params */
#define MAX_VCM_CTRLQ_MSG_MEMBLK_SIZE   BRG_NUM_PHY_PLUS_LAG_INT_PORTS
#define MAX_VCM_VCINFO_SIZE             SYS_DEF_MAX_NUM_CONTEXTS
#define MAX_VCM_IFMAPENTRY_SIZE         VCM_MAX_PORTS_IN_SYS
#define MAX_VCM_VCPORTENTRY_SIZE        VCM_MAX_PORTS_IN_SYS
#define MAX_SISP_BLOCKS                    10
#define MAX_SISP_VLAN_IF_MAP_SIZE       90
#define MAX_SISP_IF_CXT_INFO_SIZE       SISP_MAX_SISP_ENABLED_PORTS          
#define MAX_VCM_IPIFENTRY_SIZE          (128 + SYS_MAX_PPP_INTERFACES)
#define MAX_VCM_PROTO_REG_SIZE          20
#define MAX_VCM_LNX_VRF_STATUS_SIZE     40
/*END - VCM Sizing params */

/*START - L2IWF Sizing params */
#define MAX_L2IWF_PORTS_PER_CONTEXT               SYS_DEF_MAX_PORTS_PER_CONTEXT
#define MAX_L2IWF_TEMP_PORTS_PER_CONTEXT          1
#define MAX_L2IWF_VLAN_INFO_SIZE                  VLAN_DEV_MAX_NUM_VLAN
#define MAX_L2IWF_PORTCHANNEL_SIZE                LA_DEV_MAX_TRUNK_GROUP
#define MAX_L2IWF_VLAN_FID_MST_INST_INFO          VLAN_DEV_MAX_VLAN_ID
#define MAX_L2IWF_CONTEXTS                        SYS_DEF_MAX_NUM_CONTEXTS
#define MAX_L2IWF_PNAC_AUTHSESS                   PNAC_MAX_AUTHSESS
#define MAX_L2IWF_VLAN_VID_TRANSLATION_ENTRIES    VLAN_MAX_VID_TRANSLATION_ENTRIES
#define MAX_L2IWF_NUM_PEP                     (AST_MAX_SERVICES_PER_CUSTOMER * AST_MAX_CEP_IN_PEB)
#define MAX_L2IWF_PORT_VLAN_MEMBLK_COUNT       VLAN_DEV_MAX_NUM_VLAN
#define MAX_L2IWF_IFTYPE_PROT_ENTRIES          100
#define MAX_L2IWF_LLDP_APPL                    5
#define MAX_L2IWF_PVLAN_ENTRIES                (VLAN_MAX_PRIMARY_VLANS + VLAN_MAX_ISOLATED_VLANS + VLAN_MAX_COMMUNITY_VLANS)
#define MAX_L2IWF_SECONDARY_VLAN_INFO          (VLAN_MAX_ISOLATED_VLANS + VLAN_MAX_COMMUNITY_VLANS)
#define MAX_L2IWF_PORT_VLAN_MAP_ENTRIES        L2IWF_PORT_VLAN_MAP_ENTRIES
#define MAX_L2IWF_PORT_VLAN_LIST_ENTRIES       L2IWF_PORT_VLAN_LIST_ENTRIES
#define MAX_L2IWF_MST_INST_VLAN_MAP_ENTRIES    L2IWF_MST_INST_VLAN_MAP_ENTRIES
/*END - L2IWF Sizing params */

/* VLAN Sizing Params */
#define  MAX_VLAN_CONFIG_Q_MESG              4096
#define  MAX_VLAN_PKTS_IN_QUE                100
#define  MAX_VLAN_FID_ENTRIES                VLAN_DEV_MAX_NUM_VLAN
#define  MAX_VLAN_PORT_ENTRIES               VLAN_MAX_PORTS_IN_SYSTEM
#define  MAX_VLAN_CURR_ENTRIES               VLAN_DEV_MAX_NUM_VLAN
#ifdef NPAPI_WANTED
#ifndef SW_LEARNING
#ifdef WSS_SW_LEARNING
#define MAX_VLAN_FDB_ENTRIES VLAN_DEV_MAX_L2_TABLE_SIZE /* In case of WTP, the FDB table wont get updated in the hardware, hnce having an entry in software FDB table */
#else
#define MAX_VLAN_FDB_ENTRIES 1 /* In case hw learning is enabled then there is no software entries */
#endif /* WSS_SW_LEARNING */
#else
#define MAX_VLAN_FDB_ENTRIES VLAN_DEV_MAX_L2_TABLE_SIZE
#endif
#else
#define MAX_VLAN_FDB_ENTRIES VLAN_DEV_MAX_L2_TABLE_SIZE
#endif
#define  MAX_VLAN_FDB_INFO                   MAX_VLAN_FDB_ENTRIES
#ifdef FSB_WANTED
#define  MAX_VLAN_ST_UCAST_ENTRIES           700 
#else
#define  MAX_VLAN_ST_UCAST_ENTRIES           VLAN_DEV_MAX_ST_UCAST_ENTRIES
#endif
#define  MAX_VLAN_ST_MCAST_ENTRIES           VLAN_DEV_MAX_MCAST_TABLE_SIZE
#define  MAX_VLAN_GROUP_ENTRIES              VLAN_DEV_MAX_MCAST_TABLE_SIZE
#define  MAX_VLAN_MAC_MAP_ENTRIES            VLAN_MAX_MAC_MAP_ENTRIES
/*Ideally this macro (MAX_VLAN_PER_PORT_PER_VLAN_STATS) is suppose to 
hold the value (SYS_DEF_MAX_PORTS_PER_CONTEXT * VLAN_DEV_MAX_NUM_VLAN) 
which is a worst case scenario in which you have the maximum number of VLANs 
and ports created in the system and all the ports are member of all the VLANs. 
For practical purpose, the value of 4096 is sufficient and it can be scaled 
on need basis.*/
#define  MAX_VLAN_PER_PORT_PER_VLAN_STATS    4096
#define  MAX_VLAN_STATS_ENTRIES              VLAN_DEV_MAX_NUM_VLAN
#define  MAX_VLAN_PROTO_GROUP_ENTRIES        100
#define  MAX_VLAN_PORT_PROTOCOL_ENTRIES      100
#define  MAX_VLAN_MAC_CONTROL_ENTRIES        VLAN_DEV_MAX_NUM_VLAN
#define  MAX_VLAN_WILD_CARD_ENTRIES          128
#define  MAX_VLAN_CONTEXT_INFO               SYS_DEF_MAX_NUM_CONTEXTS
#define  MAX_VLAN_NUM_PORT_ARRARY            7
#define  MAX_VLAN_TEMP_PORT_LIST_COUNT       2000
#define  MAX_VLAN_L2VPN_MAP_ENTRIES          VLAN_MAX_L2VPN_ENTRIES
#define  MAX_VLAN_PB_PORT_ENTRIES            (VLAN_MAX_PORTS_IN_SYSTEM + 1)
#define  MAX_VLAN_RED_CACHE_ENTRIES          15
#define  MAX_VLAN_RED_ST_UCAST_CACHE_ENTRIES          15
#define  MAX_VLAN_RED_MCAST_CACHE_ENTRIES          15
#define  MAX_VLAN_RED_PROTO_VLAN_ENTRIES           15

#ifdef ISS_METRO_WANTED
#define  MAX_VLAN_PB_CVID_ENTRIES             SVLAN_PORT_CVLAN_MAX_ENTRIES
#define  MAX_VLAN_PB_CVLAN_SRCMAC_SVLAN_ENTRIES  SVLAN_PORT_CVLAN_SRCMAC_MAX_ENTRIES
#define  MAX_VLAN_PB_CVLAN_DSTMAC_SVLAN_ENTRIES  SVLAN_PORT_CVLAN_DSTMAC_MAX_ENTRIES
#define  MAX_VLAN_PB_CVLAN_DSCP_SVLAN_ENTRIES    SVLAN_PORT_CVLAN_DSCP_MAX_ENTRIES
#define  MAX_VLAN_PB_CVLAN_DSTIP_SVLAN_ENTRIES   SVLAN_PORT_CVLAN_DSTIP_MAX_ENTRIES
#define  MAX_VLAN_PB_SRCMAC_SVLAN_ENTRIES        SVLAN_PORT_SRCMAC_MAX_ENTRIES
#define  MAX_VLAN_PB_DSTMAC_SVLAN_ENTRIES        SVLAN_PORT_DSTMAC_MAX_ENTRIES
#define  MAX_VLAN_PB_DSCP_SVLAN_ENTRIES          SVLAN_PORT_DSCP_MAX_ENTRIES
#define  MAX_VLAN_PB_SRCIP_SVLAN_ENTRIES         SVLAN_PORT_SRCIP_MAX_ENTRIES
#define  MAX_VLAN_PB_DSTIP_SVLAN_ENTRIES         SVLAN_PORT_DSTIP_MAX_ENTRIES
#define  MAX_VLAN_PB_SRC_DSTIP_SVLAN_ENTRIES     SVLAN_PORT_SRCIP_DSTIP_MAX_ENTRIES
#define  MAX_VLAN_PB_ETHER_TYPE_SWAP_ENTRIES     VLAN_MAX_ETHER_TYPE_SWAP_ENTRIES
#else
#define  MAX_VLAN_PB_CVID_ENTRIES                1
#define  MAX_VLAN_PB_CVLAN_SRCMAC_SVLAN_ENTRIES  1
#define  MAX_VLAN_PB_CVLAN_DSTMAC_SVLAN_ENTRIES  1
#define  MAX_VLAN_PB_CVLAN_DSCP_SVLAN_ENTRIES    1
#define  MAX_VLAN_PB_CVLAN_DSTIP_SVLAN_ENTRIES   1
#define  MAX_VLAN_PB_SRCMAC_SVLAN_ENTRIES        1
#define  MAX_VLAN_PB_DSTMAC_SVLAN_ENTRIES        1
#define  MAX_VLAN_PB_DSCP_SVLAN_ENTRIES          1
#define  MAX_VLAN_PB_SRCIP_SVLAN_ENTRIES         1
#define  MAX_VLAN_PB_DSTIP_SVLAN_ENTRIES         1
#define  MAX_VLAN_PB_SRC_DSTIP_SVLAN_ENTRIES     1
#define  MAX_VLAN_PB_ETHER_TYPE_SWAP_ENTRIES     1
#endif /*ISS_METRO WANTED*/

#define  MAX_VLAN_PB_PEP_ENTRIES                 1024
#define  MAX_VLAN_RED_GVRP_DELETED_ENTRIES       1
#define  MAX_VLAN_RED_GMRP_DELETED_ENTRIES       1
#define  MAX_VLAN_RED_GVRP_LEARNT_ENTRIES        1
#define  MAX_VLAN_RED_GMRP_LEARNT_ENTRIES        1
#define  MAX_VLAN_STATIC_ENTRIES                 VLAN_DEV_MAX_NUM_VLAN
#define  MAX_VLAN_SUBNET_MAP_ENTRIES             200
#define  MAX_VLAN_PVLAN_ENTRIES                  3
#define  MAX_VLAN_BASE_PORT_ENTRIES              VLAN_MAX_PORTS_IN_SYSTEM
/* the following macro should be defined as
 * VLAN_MAX_PORTS * VLAN_DEV_MAX_NUM_VLAN */
#define  MAX_VLAN_PORT_VLAN_MAP_ENTRIES          VLAN_PORT_VLAN_MAP_ENTRIES
#define  MAX_VLAN_PORT_INFO_ENTRIES              VLAN_PORT_INFO_ENTRIES
#define  MAX_VLAN_MAC_PORT_ENTRIES               VLAN_MAC_PORT_ENTRIES
#define  MAX_VLAN_TEMP_PORT_ENTRIES              VLAN_TEMP_PORT_ENTRIES
#define  MAX_VLAN_INFO_ENTRIES                   VLAN_INFO_ENTRIES
/* END -  VLAN SIZING PARAMS */
/* EOAMFM  Sizing */
#define MAX_EOAM_CM_IP_INTF_MESG               50 
/* EOAM - CMIPIF Sizing */
#define MAX_EOAM_FM_Q_MESG                     50
#define MAX_EOAM_FM_DATA_BLOCKS               (10 * EOAM_MAX_PORTS) 
#define MAX_EOAM_FM_VAR_REQ_RESP_BLOCKS       (MAX_EOAM_FM_DATA_BLOCKS + EOAM_MAX_PORTS) 
/* END - EOAM SIZING PARAMS */
/* TAC Sizing Params */
#define MAX_TACM_FILTERS     TACM_MAX_FILTERS
#define MAX_TACM_PROFILES    TACM_MAX_PROFILES
#define MAX_TACM_PRF_FILTER_HASH_ENTRIES    (TACM_MAX_FILTERS * 10)

/* START - UTIL Sizing Params */
#define MAX_UTIL_BITLIST_COUNT             20
#define MAX_UTIL_LOCAL_PORTLIST_COUNT      20
#define MAX_UTIL_VLAN_ID_SIZE_COUNT        20
#define MAX_UTIL_VLAN_LIST_SIZE_COUNT      20
#define MAX_UTIL_DATA_LEN           5
#define MAX_L2IWF_DUPL_BUF_SIZE     10
#define MAX_WEB_BUFFER_SIZE_COUNT 5

#define MAX_TRIE2_NUM_RADIX_NODES          25026
#define MAX_TRIE2_NUM_LEAF_NODES           25026
#define MAX_TRIE2_NUM_TRIE2_KEYS           (2 * 25026)

#define MAX_TRIE_NUM_RADIX_NODES           (28580)
#define MAX_TRIE_NUM_LEAF_NODES            (28580)
#define MAX_TRIE_NUM_TRIE_KEYS             (28580)
#define  MAX_VLAN_LIST_COUNT               10
#define MAX_UTIL_RAD_KEY_BLOCKS             MAX_RAD_KEY_BLOCKS

#if defined (WLC_WANTED) || defined (WTP_WANTED)
#define  MAX_WLC_BUF_SIZE                  10
#define  MAX_WSS_WLAN_BUF_SIZE             10
#define MAX_UTIL_ANTENNA_SELECTION         50
#define MAX_WLAN_DB_BUF_SIZE               10
#define MAX_WSS_MAC_MSG_STRUCT_BUF_SIZE    10
#define MAX_CAPWAP_PKT_COUNT               10
#define MAX_CONFIG_STATUS_RSP_BUF_SIZE     10
#define MAX_WSSIF_AUTH_DB_BUF_SIZE         10
#define MAX_AC_INFO_BUF_SIZE               10
#define MAX_JOIN_REQ_BUF_SIZE              10
#define MAX_DISC_RSP_BUF_SIZE              10
#define MAX_JOIN_RSP_BUF_SIZE              10
#define MAX_CONFIG_STATUS_REQ_BUF_SIZE     10
#define MAX_DATA_REQ_BUF_SIZE              10
#endif
/* END - UTIL Sizing Params */

/* START - BEEP sizing params */
#define BEEP_MAX_FRAME_SIZE    2500
#define BEEP_MAX_SESSIONS      3
#define BEEP_MAX_FRAME_BLOCKS  3 
#define BEEP_MAX_CHANNELS     256
#define BPCLT_Q_DEPTH          30
#define BPCLT_PENDING_LOG_Q_DEPTH         30
/* START - BEEP CLNT sizing params */
#define BPCLT_MAX_SSN_NODE         BEEP_MAX_SESSIONS
#define BPCLT_MAX_CHNL_NODE       (BEEP_MAX_SESSIONS * BEEP_MAX_CHANNELS)
#define BPCLT_MAX_SYSLOG_MSG       BPCLT_Q_DEPTH
#define BPCLT_MAX_MSG              BEEP_MAX_SESSIONS
#define BPCLT_MAX_PENDING_MSG      (BPCLT_PENDING_LOG_Q_DEPTH * BEEP_MAX_SESSIONS) 
#define BPCLT_MAX_MGMT_CHNL_NODE   BEEP_MAX_SESSIONS
#define BPCLT_MAX_MSG_BLOCKS       BEEP_MAX_SESSIONS
#define BPCLT_MAX_FRAME_BLOCKS     10 

/* START - BEEP SRV sizing params */
#define BPSRV_MAX_SRV_MSG_DB       BEEP_MAX_SESSIONS
#define BPSRV_MAX_MD5_CHNL_DATA    BEEP_MAX_SESSIONS
#define BPSRV_MAX_CHNL_DB          BEEP_MAX_SESSIONS
#define BPSRV_MAX_SRV_SESS_DB      BEEP_MAX_SESSIONS
#define BPSRV_MAX_CHNL_DATA        BEEP_MAX_SESSIONS
#define BPSRV_MAX_BUFFER_BLOCKS    10
/* END - BEEP sizing params */




/* START - MPLS VFI sizing params */

/* The Minimum and Maximum VLAN Space for VFI */
#define MIN_VLAN_VFI_ID           VLAN_VFI_MIN_ID
#define MAX_VLAN_VFI_ID           VLAN_VFI_MAX_ID

/*This macro SNMP_MAX_OCTETSTRING_SIZE is used for different purposes.
 * For example,
 * * Differnet kind of Namings - set/get/test via SNMP configuration
 * * Vlan List size - set/get/test via SNMP configuration
 *
 * For naming purposes, The minimum value should be 512 bytes.
 *
 * When it is used for Vlan List size, the size should be tuned based on the
 * VLAN_DEV_MAX_VFI_ID.
 *
 * When VLAN_DEV_MAX_VLAN_ID <= 4094 and VLAN_DEV_MAX_VFI_ID = 0,
 * the value of SNMP_MAX_OCTETSTRING_SIZE = 512.
 *
 * When VLAN_DEV_MAX_VLAN_ID <= 4094 and VLAN_DEV_MAX_VFI_ID > 0,
 * value of SNMP_MAX_OCTETSTRING_SIZE = (4094 + VLAN_DEV_MAX_VFI_ID + 31)/32 * 4.
 * When It is used for PortList size,the Size should be tuned  based on the 
 * BRG_PORT_LIST_SIZE.
 *  SNMP_MAX_OCTETSTRING_SIZE = BRG_PORT_LIST_SIZE. */

#define SNMP_MAX_OCTETSTRING_SIZE             620
#define SNMP_MAX_OCTETLIST_SIZE               900

/* The Number of VFI Ids in the system */
#define MAX_NUM_VLAN_VFI_ID       (MAX_VLAN_VFI_ID - MIN_VLAN_VFI_ID)

/* The Minimum and Maximum VLAN Space for VPWS VFI */
#define MIN_VLAN_VPWS_VFI_ID     MIN_VLAN_VFI_ID 
#define MAX_VLAN_VPWS_VFI_ID     (MIN_VLAN_VFI_ID + \
                                 ((MAX_NUM_VLAN_VFI_ID)/2))

/* The Minimum and Maximum VLAN Space for VPLS VFI */
#define MIN_VLAN_VPLS_VFI_ID     (MAX_VLAN_VPWS_VFI_ID + 1)
#define MAX_VLAN_VPLS_VFI_ID     MAX_VLAN_VFI_ID

/* END - MPLS VFI sizing params */
/****************************************************************************/
/************ CAPWAP CLI ************/
#define MAX_CAPWAP_CAPWAPBASEACNAMELISTTABLE       10

#define MAX_CAPWAP_CAPWAPBASEACNAMELISTTABLE_ISSET       10

#define MAX_CAPWAP_CAPWAPBASEMACACLTABLE       10

#define MAX_CAPWAP_CAPWAPBASEMACACLTABLE_ISSET       10

#define MAX_CAPWAP_CAPWAPBASEWTPPROFILETABLE       NUM_OF_AP_SUPPORTED  + MAX_WSSCFG_TEMP_DB_COUNT 

#define MAX_CAPWAP_CAPWAPBASEWTPPROFILETABLE_ISSET      NUM_OF_AP_SUPPORTED + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_CAPWAP_CAPWAPBASEWTPSTATETABLE       NUM_OF_AP_SUPPORTED + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_CAPWAP_CAPWAPBASEWTPSTATETABLE_ISSET       NUM_OF_AP_SUPPORTED + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_CAPWAP_CAPWAPBASEWTPTABLE       NUM_OF_AP_SUPPORTED + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_CAPWAP_CAPWAPBASEWTPTABLE_ISSET       NUM_OF_AP_SUPPORTED + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_CAPWAP_CAPWAPBASEWIRELESSBINDINGTABLE       MAX_NUM_OF_RADIOS + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_CAPWAP_CAPWAPBASESTATIONTABLE       60

#define MAX_CAPWAP_CAPWAPBASESTATIONTABLE_ISSET       60

#define MAX_CAPWAP_CAPWAPBASEWTPEVENTSSTATSTABLE       NUM_OF_AP_SUPPORTED + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_CAPWAP_CAPWAPBASERADIOEVENTSSTATSTABLE       NUM_OF_AP_SUPPORTED + MAX_WSSCFG_TEMP_DB_COUNT

#define MAX_WSS_TEMP_IF_DB_COUNT     15
#define MAX_WSS_NEIGHBOR_SCAN_COUNT     2


/********END of CAPWAP CLI *********/

/* START - MEF sizing params */
#define  MAX_MEF_UNI_ENTRIES                   SYS_DEF_NUM_PHYSICAL_INTERFACES
#define  MAX_MEF_EVC_ENTRIES                   VLAN_DEV_MAX_NUM_VLAN
#define  MAX_MEF_FILTER_ENTRIES                ISS_MAX_L3_FILTERS
#define  MAX_MEF_EVC_FILTER_ENTRIES            ISS_MAX_L2_FILTERS
#define  MAX_MEF_CLS_MAP_TBL_MAX_ENTRIES       QOS_CLS_MAP_TBL_MAX_ENTRIES
#define  MAX_MEF_MAX_NUM_OF_CLASSES            QOS_MAX_NUM_OF_CLASSES
#define  MAX_MEF_METER_TBL_MAX_ENTRIES         QOS_METER_TBL_MAX_ENTRIES
#define  MAX_MEF_PLY_MAP_TBL_MAX_ENTRIES       QOS_PLY_MAP_TBL_MAX_ENTRIES
#define  MAX_MEF_CVLAN_EVC_MAP_ENTIES          MAX_VLAN_PB_CVID_ENTRIES
#define  MAX_MEF_MAX_CONTEXTS                  SYS_DEF_MAX_NUM_CONTEXTS
#define  MAX_MEF_Q_MSGS                        10
/*  For scaling MAX_MEF_UNI_LIST_ENTRIES the following formula 
 *  has to be used (VLAN_DEV_MAX_NUM_VLAN * (SYS_DEF_NUM_PHYSICAL_INTERFACES + MAX_IP_LOGICAL_IFACES)) */
#define  MAX_MEF_UNI_LIST_ENTRIES              100
/* END - MEF sizing params */

/*HOTSPOT2.0*/
#define  MAX_HOTSPOT2_INFOLISTS                      10
#define  MAX_HOTSPOT2_GAS_DATA                       10
#define  MAX_HOTSPOT2_CAP_LIST                       10
#define  MAX_HOTSPOT2_CONN_CAPABILITY                10
#define  MAX_HOTSPOT2_NAIH_REALM_QUERY               10
#define  MAX_HOTSPOT2_OPER_CLASSINDICATION           10
#define  MAX_HOTSPOT2_OP_FRIENDLY_NAME               10 
#define  MAX_HOTSPOT2_QUERYLIST                      10 
#define  MAX_HOTSPOT2_WAN_METRICS                    10 
#define  MAX_HOTSPOT2_ADV_NMA_DATA                   10 
#define  MAX_HOTSPOT2_DATA_BUF_INFO                  10 
#define  MAX_HOTSPOT2_FDMACINFO                      10 
#define  MAX_HOTSPOT2_INTERWORKING_NMS_DATA          10 
#define  MAX_HOTSPOT2_ANQP_3GPP_NET_INFO             10   
#define  MAX_HOTSPOT2_ANQP_CAPLIST                   10
#define  MAX_HOTSPOT2_ANQP_DOMAINNAMELIST            10
#define  MAX_HOTSPOT2_ANQP_IPADDTYPE                 10
#define  MAX_HOTSPOT2_ANQP_NAIREALMLIST              10
#define  MAX_HOTSPOT2_ANQP_AUTHTYPE                  10
#define  MAX_HOTSPOT2_ANQP_QUERYLIST                 10
#define  MAX_HOTSPOT2_ANQP_ROAMCONS                  10
#define  MAX_HOTSPOT2_ANQP_VENUE_NAME                10
#define  MAX_HOTSPOT2_AUTH_PARAM                     10
#define  MAX_HOTSPOT2_EAP                            10
#define  MAX_HOTSPOT2_NAI_REALMDATA                  10
#define  MAX_HOTSPOT2_Q_DEPTH                        10
#define  MAX_HOTSPOT2_ROAM_CONS                      10
#define  MAX_HOTSPOT2_SOCKET_MSG                     10
#define  MAX_HOTSPOT2_CONTEXT_INFO                   10
/*END - HOTSPOT Sizing params */

/* START - SYNCE sizing params */
#define MAX_SYNCE_CONTEXT                       1
#define MAX_SYNCE_PORTS                         128
#define MAX_SYNCE_FSSYNCETABLE_UNIT             (MAX_SYNCE_CONTEXT + 3)
#define MAX_SYNCE_FSSYNCETABLE_ISSET_UNIT       (MAX_SYNCE_CONTEXT + 3)
#define MAX_SYNCE_FSSYNCEIFTABLE_UNIT           (MAX_SYNCE_PORTS + 3)
#define MAX_SYNCE_FSSYNCEIFTABLE_ISSET_UNIT     (MAX_SYNCE_PORTS + 3)
#define MAX_SYNCE_MESSAGEQUEUE_UNIT             (MAX_SYNCE_PORTS * 2)
#define MAX_SYNCE_ESMC_PKT_POOL_UNIT            1
/* END - SYNCE sizing params */

/* START - OFCL sizing params */

#define MAX_JUMBO_PDU_SIZE  9216          /* Maximum size for Jumbo packets*/

#define MAX_OFC_VERSION                              2

/* SIZING PARAMs for Common for Both OFC V1.0.0 and OFC V1.3.1 */
#define MAX_OFC_FSOFCCFGTABLE                        10
#define MAX_OFC_FSOFCCFGTABLE_ISSET                  10
#define MAX_OFC_FSOFCCONTROLLERCONNTABLE             10
#define MAX_OFC_FSOFCCONTROLLERCONNTABLE_ISSET       10
#define MAX_OFC_FSOFCIFTABLE                         110
#define MAX_OFC_FSOFCIFTABLE_ISSET                   110
#define MAX_OFC_FSOFCFLOWTABLE                       1000
#define MAX_OFC_FSOFCFLOWTABLE_1                     1000
#define MAX_OFC_FSOFCFLOWTABLE_2                     1000
#define MAX_OFC_FSOFCFLOWTABLE_3                     1000
#define MAX_OFC_FSOFCFLOWTABLE_4                     1000
#define MAX_OFC_FSOFCFLOWTABLE_5                     1000
#define MAX_OFC_FSOFCFLOWTABLE_6                     1000
#define MAX_OFC_FSOFCFLOWTABLE_7                     1000
#define MAX_OFC_FSOFCFLOWTABLE_8                     1000
#define MAX_OFC_FSOFCFLOWTABLE_9                     1000
#define MAX_OFC_FLOW_INDEX                           1000000 /* Million */
#define MAX_OFC_QUEUE_MSG                            20
#define MAX_OFC_PORT_STATUS                          20
#define MAX_OFC_NONTCAM_FLOWMSGS                     1000

/* SIZING PARAMs for Specific to OFC V1.3.1 Only */
#define MAX_OFC_FSOFCGROUPTABLE                      100
#define MAX_OFC_FSOFCMETERTABLE                      100
#define MAX_OFC_MULTIPARTREPLY_MSG                   10
#define MAX_OFC_INSTRUCTIONS                         1000
#define MAX_OFC_INSTRUCTIONS_1                       1000
#define MAX_OFC_INSTRUCTIONS_2                       1000
#define MAX_OFC_INSTRUCTIONS_3                       1000
#define MAX_OFC_INSTRUCTIONS_4                       1000
#define MAX_OFC_INSTRUCTIONS_5                       1000
#define MAX_OFC_INSTRUCTIONS_6                       1000
#define MAX_OFC_INSTRUCTIONS_7                       1000
#define MAX_OFC_INSTRUCTIONS_8                       1000
#define MAX_OFC_INSTRUCTIONS_9                       1000
#define MAX_OFC_ACTIONS                              1000
#define MAX_OFC_ACTIONS_1                            1000
#define MAX_OFC_ACTIONS_2                            1000
#define MAX_OFC_ACTIONS_3                            1000
#define MAX_OFC_ACTIONS_4                            1000
#define MAX_OFC_ACTIONS_5                            1000
#define MAX_OFC_ACTIONS_6                            1000
#define MAX_OFC_ACTIONS_7                            1000
#define MAX_OFC_ACTIONS_8                            1000
#define MAX_OFC_ACTIONS_9                            1000
#define MAX_OFC_ACTION_BUCKET                        110
#define MAX_OFC_SET_FIELDS                           1000
#define MAX_OFC_SET_FIELDS_1                         1000
#define MAX_OFC_SET_FIELDS_2                         1000
#define MAX_OFC_SET_FIELDS_3                         1000
#define MAX_OFC_SET_FIELDS_4                         1000
#define MAX_OFC_SET_FIELDS_5                         1000
#define MAX_OFC_SET_FIELDS_6                         1000
#define MAX_OFC_SET_FIELDS_7                         1000
#define MAX_OFC_SET_FIELDS_8                         1000
#define MAX_OFC_SET_FIELDS_9                         1000
#define MAX_OFC_FLOW_MATCH                           1000
#define MAX_OFC_FLOW_MATCH_1                         1000
#define MAX_OFC_FLOW_MATCH_2                         1000
#define MAX_OFC_FLOW_MATCH_3                         1000
#define MAX_OFC_FLOW_MATCH_4                         1000
#define MAX_OFC_FLOW_MATCH_5                         1000
#define MAX_OFC_FLOW_MATCH_6                         1000
#define MAX_OFC_FLOW_MATCH_7                         1000
#define MAX_OFC_FLOW_MATCH_8                         1000
#define MAX_OFC_FLOW_MATCH_9                         1000
#define MAX_OFC_KEY_FLOW_MATCH                       50
#define MAX_OFC_FLOW_MATCH_TLV                       10
#define MAX_OFC_PKTIN                                10
#define MAX_OFC_FLOW_REM_MSG                         10
#define MAX_OFC_FLOW_TABLES                          (30 * MAX_OFC_FSOFCCFGTABLE)
#define MAX_OFC_FLOWMSGS                             MAX_OFC_FSOFCFLOWTABLE
#define MAX_OFC_METER_BAND                           300
#define MAX_OFC_BUFFER                               30
#define MAX_OFC_DESC_STATS                           5
#define MAX_OFC_EXACT_ENTRY                          10000
#define MAX_OFC_FLOW_TYPE                            1  /* Exact Supported */
#define MAX_OFC_EXACT_LENGTH                         64 /* to be scaled */
#define MAX_OFC_OUT_PORT                             1000

/* Index Manager MACROs */
#define MAX_OFC_INDEXMGR_INDEX_MGR_CHUNK             21
#define MAX_OFC_INDEXMGR_INDEX_MGR_GRP               1
#define MAX_OFC_INDEXMGR_INDEX_TBL_CHUNK             21
/* END - OFCL sizing params */

/* VXLAN Table Macros */
#define MAX_VXLAN_FSVXLANVTEPTABLE             2051
#define MAX_VXLAN_FSVXLANVTEPTABLE_ISSET       2051
#define MAX_VXLAN_FSVXLANNVETABLE              8192
#define MAX_VXLAN_FSVXLANNVETABLE_ISSET        8192
#define MAX_VXLAN_FSVXLANMCASTTABLE            100
#define MAX_VXLAN_FSVXLANMCASTTABLE_ISSET      10
#define MAX_VXLAN_FSVXLANVNIVLANMAPTABLE       2051
#define MAX_VXLAN_FSVXLANVNIVLANMAPTABLE_ISSET 2051
#define MAX_VXLAN_FSVXLANINREPLICATABLE        2051
#define MAX_VXLAN_FSVXLANINREPLICATABLE_ISSET  2051
#define MAX_VXLAN_FSVXLANECMPNVETABLE              100 
#define MAX_VXLAN_FSEVPNVXLANARPTABLE          100
#define MAX_VXLAN_DECAPPKTSLOGTABLE            50
#define MAX_VXLAN_RED_MSGS                     50
/* EVPN-VXLAN Table Macros */
#define MAX_VXLAN_FSEVPNVXLANEVIVNIMAPTABLE               2051
#define MAX_VXLAN_FSEVPNVXLANEVIVNIMAPTABLE_ISSET         2051
#define MAX_VXLAN_FSEVPNVXLANBGPRTTABLE                   100
#define MAX_VXLAN_FSEVPNVXLANBGPRTTABLE_ISSET             10
#define MAX_VXLAN_FSEVPNVXLANVRFTABLE                     100
#define MAX_VXLAN_FSEVPNVXLANVRFTABLE_ISSET               10
#define MAX_VXLAN_FSEVPNVXLANVRFRTTABLE                   100
#define MAX_VXLAN_FSEVPNVXLANVRFRTTABLE_ISSET             10
#define MAX_VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE          10
#define MAX_VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE_ISSET    5
#define MAX_VXLAN_QUEUE_MSG                    MAX_VXLAN_FSVXLANNVETABLE
#define MAX_VXLAN_UDP_RECV_BUF                 1
#define MAX_VXLAN_UDP_SEND_BUF                 1
#define MAX_BGP_EVPN_VRF_VNI_NODES             MAX_VXLAN_FSEVPNVXLANEVIVNIMAPTABLE
#define MAX_VXLAN_EVPN_VNI                     MAX_VXLAN_FSEVPNVXLANEVIVNIMAPTABLE

#define MAX_VXLAN_EVPN_VRF                     MAX_VXLAN_FSEVPNVXLANVRFTABLE
/* EVB related sizing macros */
#define MAX_VLAN_EVB_CONTEXT_ENTRIES            SYS_DEF_MAX_NUM_CONTEXTS
#define MAX_VLAN_EVB_UAP_ENTRIES                10
/*sizing params for EVB S-channel interface */
#define MAX_VLAN_EVB_SCH_ENTRIES                40
#define MAX_VLAN_EVB_QUE_ENTRIES                20
/* Max SBP ports per UAP*/
#define VLAN_EVB_MAX_SBP_PER_UAP                4


/****************************************************************************/

typedef struct sCfaSystemSize {
    UINT4  u4SystemMaxIface;
    UINT4  u4SystemMaxPhysicalIface;
    UINT4  u4SystemMaxTunlIface;
    UINT4  u4SystemMaxFFMEntries;
}tCfaSystemSize;

#ifdef IP_WANTED
typedef struct sIpSystemSize {
    UINT4  u4IpMaxStaticRoutes;
    UINT4  u4IpMaxAggregateRoutes;
    UINT4  u4IpMaxReasmLists;
    UINT4  u4IpMaxFragPerList;
    UINT4  u4IpMaxRoutes;
}tIpSystemSize;
#endif /* IP_WANTED */

#if defined  IGMP_WANTED || defined (MLD_WANTED)
typedef struct sIgmpSystemSize
{
    UINT4  u4MaxGroups;
    UINT4  u4MaxSrcs;
}tIgmpSystemSize;
#endif


typedef struct sRtmSystemSize {
    UINT4  u4RtmMaxRoutes;
}tRtmSystemSize;

#if defined (IP6_WANTED) || defined (LINUX_IPV6_WANTED)
typedef struct sRtm6SystemSize {
     UINT4  u4Rtm6MaxRoutes;
}tRtm6SystemSize;
#endif

#ifdef RIP6_WANTED
typedef struct sRip6SystemSize {
    UINT4  u4Rip6MaxRoutes;
    UINT4  u4Rip6MaxPeerEntries;
}tRip6SystemSize;
#endif /* RIP6_WANTED */

#ifdef MPLS_WANTED
typedef struct sMplsSystemSize {
    UINT4  u4MplsMaxTunnels;
    UINT4  u4MplsMaxPwId;
    UINT4  u4MinLdpLblRange;
    UINT4  u4MaxLdpLblRange;
    UINT4  u4MinRsvpTeLblRange;
    UINT4  u4MaxRsvpTeLblRange;
    UINT4  u4MinL2VpnLblRange;
    UINT4  u4MaxL2VpnLblRange;
    UINT4  u4MinStaticLblRange;
    UINT4  u4MaxStaticLblRange;
    UINT4  u4MinL3VPNLblRange;
    UINT4  u4MaxL3VPNLblRange;
    UINT4  u4MinBgpVplsLblRange;
    UINT4  u4MaxBgpVplsLblRange;
}tMplsSystemSize;
#endif /* MPLS_WANTED */

#ifdef IP6_WANTED
typedef struct sIp6SystemSize {
    UINT4 u4Ip6MaxRoutes;
    INT4 i4Ip6MaxLogicalIfaces;
    UINT4 u4Ip6MaxTunnelIfaces;
   UINT4 u4Ip6MaxAddr;
    UINT4 u4Ip6MaxFragReasmList;
    UINT4 u4Ip6MaxCacheEntries;
    UINT4 u4Ip6MaxPmtuEntries;
}tIp6SystemSize;
#endif /* IP6_WANTED */

#ifdef RIP_WANTED
typedef struct sRipSystemSize {
    UINT4  u4RipMaxRoutes;
    UINT4  u4RipMaxPeerEntries;
}tRipSystemSize;
#endif /* RIP_WANTED */

#ifdef TCP_WANTED
typedef struct sTcpSystemSize {
    UINT4  u4TcpMaxNumOfTCB;
}tTcpSystemSize;
#endif /* TCP_WANTED */

#ifdef MLD_WANTED
typedef struct sMldSystemSize {
    UINT4  u4MldMaxCacheEntries;
    UINT4  u4MldMaxRoutingProtocol;
}tMldSystemSize;
#endif /* MLD_WANTED */

#ifdef IPOA_WANTED
typedef struct sIpoaSystemSize {
    UINT4  u4IpoaMaxATEntries;
    UINT4  u4IpoaMaxIpoaConnections;
    UINT4  u4IpoaAarpMaxCacheEntries;
}tIpoaSystemSize;
#endif /* IPOA_WANTED */

#ifdef MFWD_WANTED
typedef struct sMfwdSystemSize {
    UINT2  u2MfwdMaxMrps;
    UINT2  u2Reserved;
}tMfwdSystemSize;
#endif 

#ifdef NAT_WANTED
typedef struct sNatSystemSize {
    UINT4  u4NatTypicalNumOfEntries;
}tNatSystemSize;
#endif /* NAT_WANTED */

#ifdef FIREWALL_WANTED
typedef struct sFwlAclSystemSize {
    UINT4  u4FwlMaxNumOfFilters;
    UINT4  u4FwlMaxNumOfRules;
}tFwlAclSystemSize;
#endif /* FIREWALL_WANTED */

#ifdef QoS_WANTED
typedef struct sQoSSystemSize {
    UINT4  u4QoSMaxNbrOfClassifiers;
    UINT4  u4QoSMaxNbrOfMeters;       
    UINT4  u4QoSMaxNbrOfActions;
    UINT4  u4QoSMaxNbrOfBuffers;
#ifdef QoS_INTSRV_WANTED
    UINT4  u4QoSMaxNbrOfFlows;
#endif /* QoS_INTSRV_WANTED */
}tQoSSystemSize;
#endif /* QoS_WANTED */

#if defined (PIM_WANTED) || defined (PIMV6_WANTED)
typedef struct sPimSystemSize {
    UINT4  u4PimMaxSources;
    UINT4  u4PimMaxRps;
}tPimSystemSize;
#endif /* PIM_WANTED */

#ifdef DVMRP_WANTED
typedef struct sDvmrpSystemSize {
    UINT4  u4DvmrpMaxSources;
}tDvmrpSystemSize;
#endif /* DVMRP_WANTED */

#ifdef RADIUS_WANTED
typedef struct sRadiusSystemSize {
    UINT4  u4RadiusMaxUserEntries;
}tRadiusSystemSize;
#endif /* RADIUS_WANTED */

#ifdef OSPF_WANTED
typedef struct sOspfSystemSize {
    UINT4  u4OspfMaxAreas;
    UINT4  u4OspfMaxLSAperArea;
    UINT4  u4OspfMaxExtLSAs;
    UINT4  u4OspfMaxSelfOrgLSAs;
    UINT4  u4OspfMaxRoutes;
    UINT4  u4OspfMaxLsaSize;
}tOspfSystemSize;
#endif /* OSPF_WANTED */

#ifdef BGP_WANTED
typedef struct sBgpSystemSize {
    UINT4  u4BgpMaxPeerEntries;
    UINT4  u4BgpMaxNumOfRoutes;
    UINT4  u4BgpMaxCapsPerPeer;
    UINT4  u4BgpMaxCapDataSize;
    UINT4  u4BgpMaxInstancesPerCap;
    UINT4  u4BgpCommMaxInFTblEntries;
    UINT4  u4BgpCommMaxOutFTblEntries;
    UINT4  u4BgpExtCommMaxInFTblEntries;
    UINT4  u4BgpExtCommMaxOutFTblEntries;
    UINT4  u4BgpRfdCutOffThreshold;
    UINT4  u4BgpRfdReuseThreshold;
    UINT4  u4BgpRfdMaxHoldDownTime;
    UINT4  u4BgpRfdDecayHalfLifeTime;
    UINT4  u4BgpRfdDecayTimerGranularity;
    UINT4  u4BgpRfdReuseTimerGranularity;
    UINT4  u4BgpRfdReuseArraySize;
}tBgpSystemSize;
#endif /* BGP_WANTED */


#ifdef RSVP_WANTED
typedef struct sRsvpSystemSize {
    UINT4  u4RsvpMaxSessionEntries;
    UINT4  u4RsvpMaxSenderEntries;
    UINT4  u4RsvpMaxRsbEntries;
    UINT4  u4RsvpMaxTcsbEntries;
    UINT4  u4RsvpMaxResvFwdEntries;
    UINT4  u4RsvpFlowDescPoolSize;
    UINT4  u4RsvpTmrParamPoolSize;
}tRsvpSystemSize;
#endif /* RSVP_WANTED */

#if defined(IPSECv4_WANTED ) || defined(IPSECv6_WANTED)
typedef struct sSecSystemSize {
   UINT4  u4SecMaxNoSA;
   UINT4  u4SecMaxContextTableSize;
}tSecSystemSize;
#endif

#ifdef DNS_RELAY_WANTED
typedef struct sDnsRelaySystemSize {
   UINT4  u4DnsRelayMaxNameServers;
   UINT4  u4MaxCacheEntries;
   UINT4  u4MaxNoOfConcurrentQueries;
   UINT4  u4MaxNoOfUrlFilters;
}tDnsRelaySystemSize;

#endif
#ifdef LNXIP4_WANTED
#ifdef TUNNEL_WANTED

#define  IP6_MAX_LOGICAL_IF_INDEX  (SYS_DEF_MAX_PHYSICAL_INTERFACES \
                                    +SYS_DEF_MAX_WSS_IFACES\
                                    + LA_MAX_AGG_INTF + \
                                    IP_DEV_MAX_L3VLAN_INTF + \
                                    SYS_DEF_MAX_TUNL_IFACES + \
                                    SYS_DEF_MAX_MPLS_IFACES + \
                                    SYS_DEF_MAX_MPLS_TNL_IFACES + \
                                    SYS_MAX_LOOPBACK_INTERFACES + \
                                    SYS_DEF_MAX_L3SUB_IFACES) 
#else
#define  IP6_MAX_LOGICAL_IF_INDEX  (SYS_DEF_MAX_PHYSICAL_INTERFACES \
                                    + LA_MAX_AGG_INTF + \
                                    IP_DEV_MAX_L3VLAN_INTF + \
                                    SYS_DEF_MAX_MPLS_IFACES + \
                                    SYS_DEF_MAX_MPLS_TNL_IFACES + \
                                    SYS_MAX_LOOPBACK_INTERFACES + \
                                    SYS_DEF_MAX_L3SUB_IFACES) 
#endif
#endif


/*************************************************************************/

typedef struct sSystemSize  {
   tCfaSystemSize   CfaSystemSize;
#ifdef IP_WANTED
   tIpSystemSize    IpSystemSize;
#endif /* IP_WANTED */
   tRtmSystemSize    RtmSystemSize;
#if defined (IP6_WANTED) || defined (LINUX_IPV6_WANTED)
   tRtm6SystemSize   Rtm6SystemSize;
#endif
#ifdef RIP6_WANTED
   tRip6SystemSize    Rip6SystemSize;
#endif /* RIP6_WANTED */
#ifdef MPLS_WANTED
   tMplsSystemSize   MplsSystemSize;
#endif /* MPLS_WANTED */

#ifdef IP6_WANTED
   tIp6SystemSize    Ip6SystemSize;
#endif /* IP6_WANTED */

#ifdef MFWD_WANTED
   tMfwdSystemSize  MfwdSystemSize;
#endif

#ifdef RIP_WANTED
   tRipSystemSize    RipSystemSize;
#endif /* RIP_WANTED */

#ifdef TCP_WANTED
   tTcpSystemSize   TcpSystemSize;
#endif /* TCP_WANTED */

#ifdef MLD_WANTED
   tMldSystemSize   MldSystemSize;
#endif /* MLD_WANTED */

#ifdef IPOA_WANTED
   tIpoaSystemSize  IpoaSystemSize;
#endif /* IPOA_WANTED */

#ifdef NAT_WANTED
   tNatSystemSize   NatSystemSize;
#endif /* NAT_WANTED */

#ifdef FIREWALL_WANTED
   tFwlAclSystemSize  FwlAclSystemSize;
#endif /* FIREWALL_WANTED */

#ifdef QoS_WANTED
   tQoSSystemSize     QoSSystemSize;
#endif /* QoS_WANTED */

#if defined (PIM_WANTED) || defined (PIMV6_WANTED)
   tPimSystemSize     PimSystemSize;
#endif /* PIM_WANTED */

#ifdef IGMP_WANTED
   tIgmpSystemSize     IgmpSystemSize;
#endif /* IGMP_WANTED */

#ifdef DVMRP_WANTED
   tDvmrpSystemSize   DvmrpSystemSize;
#endif /* DVMRP_WANTED */
   
#ifdef RADIUS_WANTED
   tRadiusSystemSize  RadiusSystemSize;
#endif /* RADIUS_WANTED */

#ifdef OSPF_WANTED 
   tOspfSystemSize    OspfSystemSize;
#endif /* OSPF_WANTED */

#ifdef BGP_WANTED 
   tBgpSystemSize     BgpSystemSize;
#endif /* BGP_WANTED */

#ifdef BRIDGE_WANTED
   tBridgeSystemSize  BridgeSystemSize;
#endif /* BRIDGE_WANTED */

#ifdef RSVP_WANTED
   tRsvpSystemSize    RsvpSystemSize;
#endif /* RSVP_WANTED */

#if defined(IPSECv4_WANTED ) || defined(IPSECv6_WANTED)
   tSecSystemSize     SecSystemSize;
#endif /* IPSECv4_WANTED */

#ifdef DNS_RELAY_WANTED
   tDnsRelaySystemSize    DnsRelaySystemSize;
#endif /* DNS_RELAY_WANTED */
}tSystemSize;

/***********************************************************************/

extern tSystemSize   gSystemSize;

#define   CFA_SET_SYSTEM_SIZE_VALUE(Val1, Val2, Val3) \
          CfaSetSystemSizeValue(Val1, Val2, Val3)

#ifdef BGP_WANTED
VOID GetBgpSizingParams (tBgpSystemSize *pParams);
VOID SetBgpSizingParams (tBgpSystemSize *pParams);
#endif

#ifdef MPLS_WANTED
VOID GetMplsSizingParams (tMplsSystemSize *pParams);
VOID SetMplsSizingParams (tMplsSystemSize *pParams);
#endif


#ifdef CFA_WANTED
VOID GetCfaSizingParams (tCfaSystemSize *pParams);
VOID SetCfaSizingParams (tCfaSystemSize *pParams);
#endif

#ifdef DNS_RELAY_WANTED
VOID GetDnsRelaySizingParams (tDnsRelaySystemSize *pParams);
VOID SetDnsRelaySizingParams (tDnsRelaySystemSize *pParams);
#endif

#ifdef DVMRP_WANTED
VOID GetDvmrpSizingParams (tDvmrpSystemSize *pParams);
VOID SetDvmrpSizingParams (tDvmrpSystemSize *pParams);
#endif

#ifdef FWLACL_WANTED
VOID GetFwlAclSizingParams (tFwlAclSystemSize *pParams);
VOID SetFwlAclSizingParams (tFwlAclSystemSize *pParams);
#endif

#ifdef IP_WANTED
VOID GetIpSizingParams (tIpSystemSize *pParams);
VOID SetIpSizingParams (tIpSystemSize *pParams);
#endif

VOID GetRtmSizingParams (tRtmSystemSize *pParams);

#ifdef RIP6_WANTED
VOID GetRip6SizingParams (tRip6SystemSize *pParams);
VOID SetRip6SizingParams (tRip6SystemSize *pParams);
#endif


#if defined (IP6_WANTED) || defined (LINUX_IPV6_WANTED)
VOID GetRtm6SizingParams (tRtm6SystemSize * pParams);
#endif

#ifdef IP6_WANTED
VOID GetIp6SizingParams (tIp6SystemSize *pParams);
VOID SetIp6SizingParams (tIp6SystemSize *pParams);
#endif

#ifdef MFWD_WANTED
PUBLIC VOID 
GetMfwdSizingParams (tMfwdSystemSize *pParams);
PUBLIC VOID 
SetMfwdSizingParams (tMfwdSystemSize *pParams);
#endif

#ifdef IPOA_WANTED
VOID GetIpoaSizingParams (tIpoaSystemSize *pParams);
VOID SetIpoaSizingParams (tIpoaSystemSize *pParams);
#endif

#ifdef NAT_WANTED
VOID GetNatSizingParams (tNatSystemSize *pParams);
VOID SetNatSizingParams (tNatSystemSize *pParams);
#endif

#ifdef OSPF_WANTED
VOID GetOspfSizingParams (tOspfSystemSize *pParams);
VOID SetOspfSizingParams (tOspfSystemSize *pParams);
#endif

#if defined (PIM_WANTED) || defined (PIMV6_WANTED)
VOID GetPimSizingParams (tPimSystemSize *pParams);
VOID SetPimSizingParams (tPimSystemSize *pParams);
#endif

#ifdef QOS_WANTED
VOID GetQoSSizingParams (tQoSSystemSize *pParams);
VOID SetQoSSizingParams (tQoSSystemSize *pParams);
#endif

#ifdef RADIUS_WANTED
VOID GetRadiusSizingParams (tRadiusSystemSize *pParams);
VOID SetRadiusSizingParams (tRadiusSystemSize *pParams);
#endif

#ifdef RIP_WANTED
VOID GetRipSizingParams (tRipSystemSize *pParams);
VOID SetRipSizingParams (tRipSystemSize *pParams);
#endif

#ifdef RSVP_WANTED
VOID GetRsvpSizingParams (tRsvpSystemSize *pParams);
VOID SetRsvpSizingParams (tRsvpSystemSize *pParams);
#endif

#ifdef TCP_WANTED
VOID GetTcpSizingParams (tTcpSystemSize *pParams);
VOID SetTcpSizingParams (tTcpSystemSize *pParams);
#endif

#ifdef MLD_WANTED
VOID GetMldSizingParams (tMldSystemSize *pParams);
VOID SetMldSizingParams (tMldSystemSize *pParams);
#endif

#if defined(IPSECv4_WANTED) || defined(IPSECv6_WANTED)
VOID GetSecSizingParams (tSecSystemSize *pParams);
VOID SetSecSizingParams (tSecSystemSize *pParams);
#endif
#endif

 
