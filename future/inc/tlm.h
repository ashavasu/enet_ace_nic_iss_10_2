/********************************************************************
 * Copyright (C) 2011 Aricent Group . All Rights Reserved
 *
 * $Id: tlm.h,v 1.9 2012/12/11 15:10:36 siva Exp $
 *
 * Description:This file contains the exported definitions and
 *             prototypes of TLM
 *
 *******************************************************************/

#ifndef _TLM_H_
#define _TLM_H_

#include "ospfte.h"

#define TLM_TRUE 1
#define TLM_FALSE 0

#define TLM_MIN_PRIORITY_LVL              0
#define TLM_MAX_PRIORITY_LVL              8
#define TLM_MAX_TE_CLASS                  8
#define TLM_MAX_TE_CLASS_TYPE             8

#define TLM_MAX_BW_THRESHOLD_SUPPORTED    16

#define TLM_MAX_BW_LEN                    16
#define TLM_MAX_NAME_LEN                  20

#define TLM_MAX_SRLG_NO         OSPF_TE_MAX_SRLG_NO
#define TLM_MAX_DESCRIPTOR_NO   OSPF_TE_MAX_DESCRIPTOR_NO

#define IPV4_ADD_LEN                    4
#define IPV6_ADD_LEN                    16
#define TLM_DEFAULT_MASK                0xffff0000
                    /* uint4 value corresponds to 255.0.0.0 */
#define TLM_MAX_IP_LEN                  IPV4_ADD_LEN


/* Specifies the Protection type supported by the TE-Link */
#define  TLM_TE_LINK_PROTECTION_TYPE_EXTRATRAFFIC    OSPF_TE_LINK_PROTECTION_TYPE_EXTRATRAFFIC
#define  TLM_TE_LINK_PROTECTION_TYPE_UNPROTECTED     OSPF_TE_LINK_PROTECTION_TYPE_UNPROTECTED
#define  TLM_TE_LINK_PROTECTION_TYPE_SHARED          OSPF_TE_LINK_PROTECTION_TYPE_SHARED
#define  TLM_TE_LINK_PROTECTION_TYPE_DEDICATED1FOR1  OSPF_TE_LINK_PROTECTION_TYPE_DEDICATED1FOR1
#define  TLM_TE_LINK_PROTECTION_TYPE_DEDICATED1PLUS1 OSPF_TE_LINK_PROTECTION_TYPE_DEDICATED1PLUS1
#define  TLM_TE_LINK_PROTECTION_TYPE_ENHANCED        OSPF_TE_LINK_PROTECTION_TYPE_ENHANCED

/* Used to Set the flag to indicate the parameters
 * sent as input to the API to get the data from tlm
 * and also to send info to OSPF-TE */
#define  TLM_TE_LINK_IF_INDEX           0x1
#define  TLM_TE_LINK_ADDRESS_TYPE       0x2
#define  TLM_TE_LINK_NAME               0x4
#define  TLM_TE_LINK_LOCAL_IP_ADD       0x8
#define  TLM_TE_LINK_REMOTE_IP_ADD      0x10
#define  TLM_TE_LINK_REMOTE_ROUTER_ID   0x20
#define  TLM_TE_LINK_METRIC             0x40
#define  TLM_TE_LINK_LOCAL_IDENTIFIER   0x80
#define  TLM_TE_LINK_REMOTE_IDENTIFIER  0x100
#define  TLM_TE_LINK_MAX_RES_BAND       0x200
#define  TLM_TE_LINK_MAX_BAND           0x400
#define  TLM_TE_LINK_PROTECTION_TYPE    0x800
#define  TLM_TE_LINK_SRLG               0x20000

/* Address Types */
#define TLM_ADDRESSTYPE_UNKNOWN         0
#define TLM_ADDRESSTYPE_NUMBERED_IPV4   1

#define TLM_LOCK()   TlmLock();
#define TLM_UNLOCK() TlmUnLock()

PUBLIC INT4 TlmLock PROTO ((VOID));
PUBLIC INT4 TlmUnLock PROTO ((VOID));


enum
{
    /*  0 */ TLM_APP_ID = 0, /* Appln. id used for rm<->rm commn. */
    /*  1 */ TLM_OSPF_TE_APP_ID,
    /*  2 */ TLM_RSVP_TE_APP_ID,
    /*  3 */ TLM_MAX_APPS   /* Include new AppIds before this */ 
};

enum
{
  TLM_GET_TELINK_PARAMS = 0,
  TLM_GET_UNRESV_BW,
  TLM_UPDATE_RESV_BW,
  TLM_GET_TELINK_NAME,
  TLM_REG_PROTOCOL,
  TLM_DEREG_PROTOCOL
};

/* For Control and Data Channel Separation */
#define TLM_TE_LINK_AREAID_INFO           OSPF_TE_RM_AREAID_INFO
#define TLM_TE_LINK_DATALINK_INFO         OSPF_TE_RM_DATALINK_INFO
#define TLM_TE_LINK_DATACONTROLLINK_INFO  OSPF_TE_RM_DATACONTROLLINK_INFO


/* Event IDs for providing the notifications */
#define TE_LINK_CREATED            OSPF_TE_LINK_CREATE
#define TE_LINK_DELETED            OSPF_TE_LINK_DELETE
#define TE_LINK_UPDATED            OSPF_TE_LINK_UPDATE

#define TLM_MAX_RESOURCE_CLASS_NAME_LEN       20

/* Only TLM_TE_PSC_1 is currently supported */
#define TLM_TE_PSC_1  OSPF_TE_PSC_1
#define TLM_TE_PSC_2  OSPF_TE_PSC_2
#define TLM_TE_PSC_3  OSPF_TE_PSC_3
#define TLM_TE_PSC_4  OSPF_TE_PSC_4
#define TLM_TE_L2SC   OSPF_TE_L2SC
#define TLM_TE_TDM    OSPF_TE_TDM
#define TLM_TE_LSC    OSPF_TE_LSC
#define TLM_TE_FSC    OSPF_TE_FSC

#define TLM_SUCCESS 1
#define TLM_FAILURE 0

/* TLM MODULE Specific Trace Categories */
#define  TLM_CRITICAL_TRC          0x00000001
#define  TLM_FN_ENTRY_TRC          0x00000002
#define  TLM_FN_EXIT_TRC           0x00000004
#define  TLM_FAILURE_TRC           0x00000008
#define  TLM_RESOURCE_TRC          0x00000010
#define  TLM_MGMT_TRC              0x00000020

#define TELINK_IF_COUNT(u4IfIndex, u4CurValue) { \
    UINT4 u4IndexMin = CFA_MIN_TELINK_IF_INDEX; \
    u4CurValue = (u4IfIndex % u4IndexMin) + \
    (u4IndexMin * ( (u4IfIndex / u4IndexMin) - 1)); }\

/* Care must be taken to change this 
   value when new TRC type is introduced */
#define  TLM_ALL_TRC               (TLM_CRITICAL_TRC    |\
                                    TLM_FN_ENTRY_TRC    |\
                                    TLM_FN_EXIT_TRC     |\
                                    TLM_FAILURE_TRC     |\
                                    TLM_RESOURCE_TRC    |\
                                    TLM_MGMT_TRC)

#ifndef IP6ADDRESS
#define IP6ADDRESS
typedef struct Ip6Address
{
  union
  {
    UINT1  u1ByteAddr[16];
    UINT2  u2ShortAddr[8];
    UINT4  u4WordAddr[4];
  } ip6_addr_u;

#define  u4_addr  ip6_addr_u.u4WordAddr  
#define  u2_addr  ip6_addr_u.u2ShortAddr 
#define  u1_addr  ip6_addr_u.u1ByteAddr  

} tIpAddr;
#endif
typedef struct TlmEventNotification
{
    /* Maximum Bandwidth           */
    tBandWidth  maxBw;               
    /* Maximum Reservable Bandwidth*/
    tBandWidth  maxResBw;            
    /* Unreserved Bandwidth at Priority        */
    tBandWidth  aUnResBw [TLM_MAX_PRIORITY_LVL];
    /* Local IP address of the interface */
    tIpAddr     LocalIpAddr;     
    /* Remote IP address of the interface */
    tIpAddr     RemoteIpAddr;      
    /* Pointer to interface Descriptors */ 
    tOsTeIfDesc  aIfDescriptors[OSPF_TE_MAX_DESCRIPTOR_NO];
    /* Array of SRLG Information */
    tOsTeSrlg    Srlg;

    /* Remote Router address of the interface */
    UINT4       u4RemoteRtrId;        
    /* Interface Index of TE link  */
    UINT4       u4IfIndex; 
    /* MPLS Interface IfIndex      */
    UINT4       u4MplsIfIndex;      
    /* TE Metric value             */
    UINT4       u4TeMetric;
    /* Administrative Group        */
    UINT4       u4RsrcClassColor;
     /* Local Identifier value in case of un-numbered interface */
    UINT4       u4LocalIdentifier;   
     /* Remote Identifier value in case of un-numbered interface */
    UINT4       u4RemoteIdentifier;
     /* Represents a priority value */
    UINT4       u4WorkingPriority;
     /* Interface Descriptor count on an interface  */
    UINT4       u4IfDescCnt;
    /*Event Type*/
    UINT4       u4EventType;
    /*TE Link interface type 1.Point to point 2.Multiaccess*/
    UINT4       u4TeLinkIfType;
    /* CFA Port Index. To be filled if the TE Link is 
     * the FA TE Link */
    UINT4       u4LowerCompIfCfaPortIndex;
    /*Address Type*/
    INT4        i4AddressType;
    /*Protection type*/
    INT4        i4ProtectionType;
    /* Information Type Data or Control Channel or FA Channel */
    UINT1       u1InfoType;
    /* Link information -- Create -- Modify -- Delete */
    UINT1       u1LinkStatus;
    /*Module Status Up -- Module Status Down -- LinkInfo*/
    UINT1       u1MsgSubType;
    UINT1       au1Pad[1];
}tTlmEventNotification;

typedef struct TlmRegParams
{
   /* Module ID(app Id) of the application that wants to get registered */
   UINT4 u4ModId; 
   /* Bit set for the events with which an application wants to be notified
    * The Bits sets are as follows 
    * TE_LINK_CREATED   00000000000000001
    * TE_LINK_DELETED   00000000000000010
    * TE_LINK_UPDATED   00000000000000100 
    */
   UINT4 u4EventsId;
   /* TLM provides the notification through this function pointer */
   INT1 (*pFnRcvPkt) (tTlmEventNotification *pTlmEventNotification);
}tTlmRegParams;


typedef tOsTeIfDesc tTlmIfDesc;
/* Input structure for API Provided by TLM */
typedef struct _InTlmApiInfo {
    /*Applications registered */
    tTlmRegParams    AppRegParams [TLM_MAX_APPS];
    /* Local IP Address of the interface */
    tIpAddr          LocalIpAddr;
    /* Remote IP Address of the interface */
    tIpAddr          RemoteIpAddr;
    /* Requested Reserved bandwidth 
     * In case of FA TE-Link creation call, RSVP-TE
     * fills the maximum LSP Bandwidth of the FA TE-Link */
    tBandWidth       reqResBw;
    /* Remote router ID of the interface */
    UINT4            u4RemoteRtrId;
    /* Interface Index of teLink */
    UINT4             u4IfIndex;
    /* Interface Index of component link 
     * In case of FA TE-Link creation call, RSVP-TE fills 
     * mplsTunnel IfIndex in this variable */
    UINT4            u4CompIfIndex;
    /* Interface Descriptor Id*/
    UINT4            u4DescId;
    /* Local Identifier value */
    UINT4            u4LocalIdentifier;
    /* Remote Identifier value */
    UINT4            u4RemoteIdentifier; 
    /* TE class*/
    UINT4            u4TeClass;
    /* TE Metric */
    UINT4            u4TeMetric;
    /* Resource color of the Te Link */
    UINT4            u4RsrcClassColor;
    /* Encoding Type. It can hold Packet, ethernet, lambda or any other
     * as specified in stdtlm MIB*/
    INT4             i4EncodingType;
    /* Switching capability. It can hold PSC1, PSC2, tdm or any other 
     * as specified in stdtlm MIB*/
    INT4             i4SwitchingCap;
    /* TE - Link to be advertised status */
    INT4             i4IsTeLinkAdvtReqd;
    /* TE-Link Name */
    UINT1            au1TeLinkName[TLM_MAX_NAME_LEN];

} tInTlmTeLink;

/* Structure returned by the API Provided by TLM */
typedef struct _OutTlmApiInfo {
    /* Local IP Address of the interface */
    tIpAddr          LocalIpAddr;
    /* Remote IP Address of the interface */
    tIpAddr          RemoteIpAddr;
    /*Max reservable bandwidth of the interface*/
    tBandWidth       maxResBw;
    /* Unreserved bandwidth of the interface */
    tBandWidth       unResBw;
    /* Available bandwidth of the interface */
    tBandWidth       availBw;
    /* Remote router ID of the interface */
    UINT4            u4RemoteRtrId;
    /* Interface Index of teLink. */
    UINT4            u4IfIndex;  
    /* Interface Index of component */
    UINT4            u4CompIfIndex;
    /* Local Identifier value */
    UINT4            u4LocalIdentifier;
    /* Remote Identifier value */
    UINT4            u4RemoteIdentifier;
    /* Administrative Group */
    UINT4            u4RsrcClassColor;
    /* In link Bundling Case fills the u4IfIndex of the Parent Te-Link */
    UINT4            u4PTeLink;
    /* Bundled or un bundled teLink  */
    INT4             i4TeLinkType; 
    /* ipv4 or unknown */
    INT4             i4AddressType;
    /* Switching Capability of teLink */
    INT4             i4SwitchingCap;
    /* Encoding Type of teLink */
    INT4             i4EncodingType;
    /* TE-Link Name */
    UINT1            au1TeLinkName[TLM_MAX_NAME_LEN];
    /* Denotes the appropriate OperStatus of the ifindex specified 
     * in tInTlmTeLink */
    UINT1            u1OperStatus;
    UINT1            au1Pad[3];
} tOutTlmTeLink;

typedef struct _TlmBwInfo {
    tBandWidth              aBwInfo[TLM_MAX_PRIORITY_LVL];
} tTlmBwInfo;

/* This structure holds the information about mapping of Class Types
 * to Priority */
typedef struct _TlmTeClass{
    UINT1   u1ClassType;
    /* Preemption Priority of the Class-Type */
    UINT1   u1PremptPrio;

    /* Denotes whether class-type is set or not */
    UINT1   u1IsSet;
    /* For Padding */
    UINT1   u1Rsvd;
}tTlmTeClassMap;

/* This structure holds the information about bandwidth percentage value of 
 * each Class Types
 */
typedef struct _TlmClassType{
    /* bandwidth percentage value. BC0..BC7 values will be calculated using this value */
    UINT4   u4BwPercentage;
}tTlmClassType;

typedef struct TlmDiffservInfo
{
    tTlmClassType    aTlmClassType [TLM_MAX_TE_CLASS_TYPE];
    tTlmTeClassMap   aTlmTeClassMap [TLM_MAX_TE_CLASS];
    UINT1            u1DsTeStatus;
    UINT1            au1Rsvd[3];
}tTlmDiffservInfo;

VOID TlmTaskMain PROTO ((INT1 *pArg));

/* Registration and Degistration of Applications*/
UINT4 TlmApiRegisterProtocols PROTO ((tTlmRegParams *));
UINT4 TlmApiDeRegisterProtocols PROTO ((UINT4));

/* RSVP Interface Function prototypes */
PUBLIC INT4 TlmApiGetTeLinkParams            PROTO ((UINT4, tInTlmTeLink *, tOutTlmTeLink *));
PUBLIC INT4  TlmApiGetTeLinkUnResvBw         PROTO ((tInTlmTeLink *, tOutTlmTeLink *));
PUBLIC INT4  TlmApiUpdateTeLinkUnResvBw      PROTO ((UINT1, tInTlmTeLink *, tOutTlmTeLink *));
PUBLIC INT4  TlmApiGetIfIndexFromTeLinkName PROTO ((UINT1 *, UINT4 *));
PUBLIC INT4  TlmApiCreateFATeLink           PROTO ((tInTlmTeLink *, tOutTlmTeLink *));
PUBLIC INT4  TlmApiDeleteFATeLink           PROTO ((tInTlmTeLink *));
PUBLIC INT4  TlmApiAssociateCtrlIfAndDataIf PROTO ((UINT4 u4CtrlMplsIfIndex, UINT4 u4IfIndex));

/* MPLS-RTR */
PUBLIC INT4 TlmApiDsTeStatusChangeNotify PROTO ((UINT1));
PUBLIC INT4 TlmApiSetClassTypeSupported      PROTO ((UINT1 , UINT1));
PUBLIC INT4 TlmApiSetTeClassForTPrio         PROTO ((UINT1, UINT1, UINT1,UINT4));
PUBLIC UINT1 TlmApiIsTeLinkConfigured        PROTO ((VOID));

VOID
TlmApiSetDiffservParams (tTlmDiffservInfo *pTlmDiffservInfo);

/* Called in cfa for stacking telink and component */
INT4 TlmApiIfStatusChange PROTO ((UINT4 u4IfIndex, UINT1 u1PortState));

PUBLIC INT4 TlmApiIfDeleteCallBack (tCfaRegInfo * pCfaRegInfo);
PUBLIC INT4 TlmApiIfOperChgCallBack (tCfaRegInfo * pCfaRegInfo);


#endif /* _TLM_H_ */
