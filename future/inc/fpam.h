/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* $Id: fpam.h,v 1.16 2015/10/12 11:09:28 siva Exp $
*
* Description: This file contains the default values for Fpam module.
*********************************************************************/


#ifndef __FPAM_H__
#define __FPAM_H__


#define FPAM_SUCCESS        (OSIX_SUCCESS)
#define FPAM_FAILURE        (OSIX_FAILURE)
#define  FPAM_TASK_PRIORITY              100
#define  FPAM_TASK_NAME                  "FPAM1"
#define  FPAM_QUEUE_NAME                 (const UINT1 *) "FPAMQ"
#define  FPAM_MUT_EXCL_SEM_NAME          (const UINT1 *) "FPAMM"
#define  FPAM_QUEUE_DEPTH                100
#define  FPAM_SEM_CREATE_INIT_CNT        1

#define FPAM_USER_STATUS_NA              -1
#define  FPAM_ENABLED                    1
#define  FPAM_DISABLED                   2
#define FPAM_USER_ENABLE 1
#define FPAM_USER_DISABLE 0
#define FPAM_USER_LOGGED_IN 1
#define FPAM_USER_LOGGED_OUT 0

#define  FPAM_TIMER_EVENT             0x00000001
#define  FPAM_QUEUE_EVENT             0x00000002
#define  MAX_FPAM_DUMMY                   1

#define FPAM_RESET_LOGIN_ATTEMPT        0
#define FPAM_INC_FAILED_LOGIN_ATTEMPT   1

/*********** macros for scalar objects*********************/


/*********** macros for tabular objects*********************/

#define FPAM_DEF_FSUSRMGMTUSERPASSWORD               "Password123#"
#define FPAM_DEF_FSUSRMGMTUSERPASSWORD_LEN               STRLEN(FPAM_DEF_FSUSRMGMTUSERPASSWORD)
#define FPAM_DEF_FSUSRMGMTUSERPRIVILEGE             1
#define FPAM_DEF_FSUSRMGMTUSERSTATUS             FPAM_USER_ENABLE
#define FPAM_DEF_FSUSRMGMTUSERLOCKRELTIME             0
#define FPAM_PRIVILEGE_ID_MASK  0x0000ffff   /* last two bytes are used to store the privilege id value <= 15,
                                                first two bytes can be used for future enhancement */

/* To enable the active user can change his own password or 
 * privileged user can change the active user password */
#define FPAM_CHECK_ACTIVE_USER_TO_EDIT_PSWD 0x00001 
/* To enable the disable user feature */
#define FPAM_CHECK_DISABLE_USER_EDIT_PSWD     0x00010
/* To enable root user can edit his own password */
#define FPAM_CHECK_EDITPASSWORD_ROOT_USER   0x0100
/* To enable confirm password feature */
#define FPAM_CONFIRM_PASSWORD_FEATURE 0x01000
/* Feature which will not allow the user to set its user status "enable/disable"*/
#define FPAM_USER_STATUS_FEATURE 0x10000
/* Feature which will forcefully logout all the active sessions of users after
 * they update the password */
#define FPAM_USER_FORCED_LOGOUT 0x100000

enum
{
    CLI_SHOW_PASSWORD_VALIDATE_RULES,
    CLI_SET_PASSWORD_VALIDATE_MASK,
    CLI_SET_PASSWORD_LOWERCASE_CHAR_COUNT,
    CLI_SET_PASSWORD_UPPERCASE_CHAR_COUNT,
    CLI_SET_PASSWORD_NUMBER_COUNT,
    CLI_SET_PASSWORD_SYMBOL_COUNT
};

enum
{
    FPAM_NO_MASK = 0x0,
    FPAM_LOWERCASE_MASK = 0x1,
    FPAM_UPPERCASE_MASK = 0x2,
    FPAM_NUMBER_MASK    = 0x4,
    FPAM_SYMBOL_MASK    = 0x8,
    FPAM_DEFAULT_MASK = 0xf
};

typedef struct 
{
 INT1 *pi1NewUsrName;
 INT1 *pi1NewPasswd;
 INT4 i4PrivilegeLevel; 
 INT1 *pi1ConfirmPasswd;
 INT1 i1UserStatus;
    UINT1 au1Pad[3];
}tFpamAddUser;

PUBLIC VOID FpamInit PROTO((INT1 *pi1Arg));

PUBLIC INT4
FpamUtlAddUser PROTO ((tFpamAddUser *pFpamAddUser));
PUBLIC INT4
FpamUtlDelUser PROTO ((INT1 *pi1NewUsrName));
PUBLIC INT1
FpamGetPrivilege PROTO ((CHR1 * pi1UserName, INT2 *pi2UserPrivilege));
PUBLIC INT4
FpamUtlSearchUserName PROTO ((tSNMP_OCTET_STRING_TYPE *
                                        pFpamFsUsrMgmtEntry));
PUBLIC INT1
FpamCheckPassword PROTO ((CHR1 *pi1UserName, CONST INT1 *pi1UserPassword));

PUBLIC INT1
FpamSetMinPasswordLen (INT4 i4MinPassLen);

PUBLIC INT4
FpamDisplayMinPasswordLen(UINT4 *i4MinPassLen);

PUBLIC INT1
FpamChangePassword PROTO ((INT1 *pi1UserName, INT1 *pi1Passwd));
PUBLIC INT1
FpamPasswordValidateRules PROTO ((UINT4 u4Case, UINT4 u4Params));

PUBLIC INT1 
FpamShowPasswdMaxLifeTime PROTO ((UINT4 *pu4LifeTime));
PUBLIC INT1 
FpamSetPasswdMaxLifeTime PROTO ((UINT4 u4LifeTime));
PUBLIC INT1
FpamPasswordExpiryCheck PROTO ((INT1 *pi1UserName));

PUBLIC INT4
FpamUtlListUsers PROTO ((void));
PUBLIC INT1
FpamGetReleaseTime PROTO ((CHR1 * pi1UserName, UINT4 * pu4BlockTime));
PUBLIC INT1
FpamSetReleaseTime PROTO ((CHR1 * pi1UserName, UINT4 u4RetValFsusrMgmtUserLockRelTime));
PUBLIC INT4
FpamUtlGetNumOfUsers PROTO ((UINT4 *pu4FsusrMgmtStatsNumOfUsers));

PUBLIC INT4
FpamUtlGetUserMode PROTO ((CONST CHR1 * pi1UserName, INT1 *pi1UserMode));
PUBLIC INT1
FpamGetUserStatus PROTO ((CHR1 * pi1UserName, INT4 *pi4UserStatus));
PUBLIC INT4
FpamUtlUpdateActiveUsers PROTO ((CHR1 * pi1UserName, UINT4 i4LoginLogoutDetail));

PUBLIC INT4
FpamUserPasswdConfCheck PROTO ((tSNMP_OCTET_STRING_TYPE * pCheckFsusrMgmtUserName ,tSNMP_OCTET_STRING_TYPE * pCheckFsusrMgmtUserPassword));
PUBLIC INT1
FpamGetUserPasswordCreationTime PROTO((INT1 *pi1UserName, tUtlTm *pTmUserPasswordLastChanged));

PUBLIC INT4
FpamUtlDelAllUsers PROTO((VOID));
PUBLIC UINT4
FpamIsSetUserMgmtFeature PROTO((UINT4 u4CheckUserMgmtFeature));
PUBLIC UINT4
FpamSetUserMgmtFeature PROTO((UINT4 u4SetUserMgmtFeature));
PUBLIC VOID
FpamClrCfgWriteUsersDefault PROTO ((VOID));
PUBLIC INT4
FpamGetLoginAttempts (CHR1 * pc1UserName, INT4 *pi4LoginAttempts);
PUBLIC INT4
FpamSetLoginAttempts (CHR1 * pc1UserName, INT4 i4LoginAttempt);


INT1 * FpamGetDefaultRootUser PROTO ((VOID));
INT1 * FpamGetDefaultRootUsrDefaultPwd PROTO ((VOID));
VOID FpamSetDefaultRootUser PROTO ((INT1 *pi1RootUserName));
VOID FpamSetDefaultRootUserPasswd PROTO ((INT1 *pi1RootUserPasswd));

#ifdef RM_WANTED
VOID
FpamReInit PROTO ((VOID));
#endif 

#endif  /* __FPAM_H__*/
/*-----------------------------------------------------------------------*/
/*                       End of the file fpamdefn.h                      */
/*-----------------------------------------------------------------------*/

