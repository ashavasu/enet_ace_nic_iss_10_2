/*****************************************************************************/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved
 ******************************************************************************
 * $Id: mplsapi.h,v 1.34 2018/01/03 11:31:19 siva Exp $
 * FILE  NAME             : mplsapi.h
 * PRINCIPAL AUTHOR       : Aricent Inc. 
 * SUBSYSTEM NAME         : MPLS
 * MODULE NAME            : MPLS FM
 * LANGUAGE               : ANSI-C
 * TARGET ENVIRONMENT     : Linux (Portable)                         
 * DATE OF FIRST RELEASE  :
 * DESCRIPTION            : This file contains the structures used by 
 *                          external modules of MPLS.
 *----------------------------------------------------------------------------*/

#ifndef _MPLSAPI_H
#define _MPLSAPI_H
#include "mpls.h"
#include "tcp.h"

/* Main request type */
enum 
{
    MPLS_OAM_HANDLE_PATH_STATUS_CHG = 0,
    MPLS_OAM_UPDATE_SESSION_PARAMS,
    MPLS_OAM_REGISTER_MEG_FOR_NOTIF,
    MPLS_PACKET_HANDLE_FROM_APP,
    MPLS_OAM_GET_MEG_INFO,
    MPLS_GET_TUNNEL_INFO,
    MPLS_GET_PW_INFO,
    MPLS_GET_LSP_INFO,
    MPLS_GET_NODE_ID,
    MPLS_GET_PATH_FROM_INLBL_INFO,
    MPLS_GET_REV_PATH_FROM_FWD_PATH,
    MPLS_GET_SERVICE_POINTER_OID,
    MPLS_OAM_MEG_ASSOC_WITH_PW,
    MPLS_OAM_UPDATE_PW_MEG_OAM_STATUS,
    MPLS_OAM_UPDATE_LPS_STATUS, /* LPS (linear protection switching) module updates 
                                  working and protection path and the 
                                  status - protection available/
                                  protection not available/protection in use */
    MPLS_OAM_UPDATE_ACH_CHNL_CODE, 
    MPLS_OAM_REG_APP_FOR_NOTIF,
    MPLS_GET_PACKET_COUNT, /* Get PDU Transmitted and received count */
    MPLS_INCR_OUT_PACKET_COUNT, /* Increment the Transmitted MPLSTP OAM PDU count */
    MPLS_INCR_IN_PACKET_COUNT, /* Increment the Receiveded MPLSTP OAM PDU count */
    MPLS_UPDATE_APP_OWNER_FOR_PW, /*Application owner for the pseudowire*/
    MPLS_REGISTER_PW_FOR_NOTIF /*Registering ERPS with MPLS-OAM to receive PW UP/ DOWN Event*/
};

/* Sub request type */
enum 
{
    MPLS_GET_PW_INFO_FROM_PW_INDEX = 0,
    MPLS_GET_PW_INFO_FROM_VCID,
    MPLS_GET_PW_INFO_FROM_GENTYPE2,
    MPLS_GET_PW_INFO_FROM_GENFEC,
    MPLS_GET_PW_INFO_FROM_PWIDFEC,
    MPLS_GET_FTN_INDEX_FROM_FEC,
    MPLS_GET_LSP_INFO_FROM_FTN_INDEX,
    MPLS_GET_LSP_INFO_FROM_XC_INDEX,
    MPLS_GET_MEG_ID_FROM_MEG_NAME,
    MPLS_GET_MEG_NAME_FROM_MEG_ID,
    MPLS_GET_MEG_INFO_FROM_MEG_ID,
    MPLS_GET_MEG_FROM_SRC_ICC_MEP,
    MPLS_GET_ROUTER_ID,
    MPLS_GET_GLOBAL_NODE_ID,
    MPLS_GET_GLBNODEID_FROM_LOCALNUM,
    MPLS_GET_LOCALNUM_FROM_GLBNODEID,
    MPLS_GET_BASE_OID_FROM_MEG_NAME,
    MPLS_GET_BASE_OID_FROM_TNL_INDEX,
    MPLS_GET_BASE_OID_FROM_FEC,
    MPLS_GET_BASE_OID_FROM_VCID,
    MPLS_GET_BASE_PW_OID,
    MPLS_GET_BASE_FTN_OID,
    MPLS_GET_BASE_TNL_OID,
    MPLS_GET_BASE_MEG_OID,
    MPLS_APP_REGISTER,
    MPLS_APP_DEREGISTER,
    MPLS_LPS_PROT_NOT_APPLICABLE,
    MPLS_LPS_PROT_AVAILABLE,
    MPLS_LPS_PROT_NOT_AVAILABLE,
    MPLS_LPS_PROT_IN_USE,    
    MPLS_APP_REG_CALL_BACK,
    MPLS_APP_REG_PATH_FOR_NOTIF   
};

enum
{
    BFD_MODULE = 0,
    LSPPING_MODULE,
    ELPS_MODULE,
    MPLSDB_MODULE, /* Internal notification purpose */
    L2VPN_MODULE,  /* Internal notification purpose */
    MPLS_OAM_MODULE, /* Internal notification purpose */
    MPLS_MEG_APPS, /* External notification purpose */
    Y1731_MODULE,  /* External notification purpose */  
    RFC6374_MODULE,  /* External notification purpose */  
    ERPS_MODULE_APP 
};
enum
{
    MPLS_ADDR_TYPE_IPV4 = 0,
    MPLS_ADDR_TYPE_IPV6,
    MPLS_ADDR_TYPE_GLOBAL_NODE_ID,
    MPLS_PATH_TYPE_MEG_ID,
    MPLS_PATH_TYPE_MEG_NAME,
    MPLS_PATH_TYPE_NONTE_LSP,
    MPLS_PATH_TYPE_TUNNEL,
    MPLS_PATH_TYPE_PW
};
enum
{
    MPLS_PATH_STATUS_UP = 0,
    MPLS_PATH_STATUS_DOWN,
    MPLS_PATH_STATUS_UNKNOWN
};

enum
{
    GET_MPLS_IN_STATS = 1,
    GET_MPLS_OUT_STATS,
    GET_MPLS_BOTH_STATS
};

enum
{
    MPLS_MEG_UP = 0,
    MPLS_MEG_DOWN,
    MPLS_MEG_OAM_UNKNOWN,
    MPLS_TNL_UP,
    MPLS_TNL_DOWN,
    MPLS_PW_UP,
    MPLS_PW_DOWN,
};

/* MPLS events */
#define MPLS_MEG_UP_EVENT              0x01
#define MPLS_MEG_DOWN_EVENT            0x02
#define MPLS_BFD_PACKET                0x04
#define MPLS_LSPPING_PACKET            0x08
#define MPLS_PSC_PACKET                0x10
#define MPLS_AIS_CONDITION_ENCOUNTERED 0x20
#define MPLS_AIS_CONDITION_CLEARED     0x40
#define MPLS_TNL_UP_EVENT              0x80
#define MPLS_TNL_DOWN_EVENT            0x100
#define MPLS_PW_UP_EVENT               0x200
#define MPLS_PW_DOWN_EVENT             0x400
#define MPLS_Y1731_PACKET              0x800  
#define MPLS_BULK_PATH_STATUS_IND      0x1000 /* This constant can be used 
                                               * whenever OAM requires bulk path  
                                               * status update from MPLS 
                                               * instead of individual updates.
                                               * Currently bulk update is not
                                               * supported in MPLS.
                                               */
#define MPLS_RFC6374_PACKET            0x2000

#ifdef HVPLS_WANTED
#define MPLS_ECFM_PACKET                0x2000
#define MPLS_CPU_HW_TRANS_INTERVAL 1 
#endif
#define MPLS_PW_IF_UP   0x4000
#define MPLS_PW_IF_DOWN            0x8000
enum
{
    MPLS_PW_TYPE_TE = 0,
    MPLS_PW_TYPE_NONTE,
    MPLS_PW_TYPE_PWONLY

};

enum
{
    MPLS_OAM_TYPE_NONE = 0,
    MPLS_OAM_TYPE_BFD,
    MPLS_OAM_TYPE_Y1731,
    MPLS_PM_TYPE_RFC6374
 
};

#define MPLS_GAL_LABEL      13
#define MPLS_TC_BEST_EFFORT 0 /* MPLS best effort traffic class */
#define MPLS_DEF_PW_TTL     2
#define MPLS_DEF_LSP_TTL    64

#define MPLS_ACH_TYPE       1 /* First nibble - 00010000 */
#define MPLS_ACH_VERSION    0
#define MPLS_ACH_RSVD       0

#define MPLS_ACH_BFD_CHANNEL_TYPE  0x7 

/*List of module Ids of valid modules 
  to register with MPLS OAM*/

typedef enum {
 MPLS_ELPS_APP_ID = 0,
 MPLS_BFD_APP_ID,
 MPLS_LSP_PING_APP_ID,
 MPLS_Y1731_APP_ID,
 MPLS_ERPS_APP_ID, /* New modules that wish to register must be added below this*/
 MPLS_RFC6374_APP_ID,
 MPLS_MAX_APP_ID
}eMplsRegdApplicationIdentifier;

#define MPLS_MAX_L2HEADER_LEN 4
#define MPLS_MAX_GAL_GACH_HEADER_LEN 16
#define MPLS_MEG_NAME_LEN 48
#define MPLS_ME_NAME_LEN 48
#define MPLS_ICC_LENGTH 6
#define MPLS_UMC_LENGTH 7
#define MPLS_MEG_ID_LENGTH 13
#define MPLS_SERVICE_OID_LEN 56
#define MPLS_MAX_LABEL_STACK MPLS_MAX_LABELS_PER_ENTRY /* 7 labels stack */

#define MPLS_XC_FWD_IN_SEG_INFO  0x1
#define MPLS_XC_REV_IN_SEG_INFO  0x2
#define MPLS_XC_FWD_OUT_SEG_INFO 0x4
#define MPLS_XC_REV_OUT_SEG_INFO 0x8

/************ API typedefinitions starts **************/
typedef struct
{
    UINT4  u4GlobalId;  /* Global Identifier. */
    UINT4  u4NodeId;   /* Node Identifier. 
                          MPLS case, Router-id is filled here */
}tMplsGlobalNodeId;

typedef struct 
{
    UINT4 u4NodeType; /* Peer Address type - IPv4 (Known IP format
                         /Unknown format but 4 byte value)/IPv6/GlobalId-NodeId */
    union {
        tIpAddr  IpAddr;     /* IPv4/IPv6 address */
        tMplsGlobalNodeId GlobalNodeId; /* Global Identifier & Node Identifier */
    }unNodeIdInfo;
#define MplsRouterId unNodeIdInfo.IpAddr
#define MplsGlobalNodeId unNodeIdInfo.GlobalNodeId
}tMplsNodeId;

typedef struct
{
    tMplsNodeId SrcNodeId; /* Source NodeId - Use the Nodetype as IPv4 
                              if we are not sure of LSRId format */
    UINT4 u4SrcTnlNum; /* Source Tunnel number for MPLS-TP, 
                          TunnelIndex for MPLS */
    tMplsNodeId DstNodeId; /* Destination NodeId - Use the Nodetype as IPv4
                              if we are not sure of LSRId format */
    UINT4 u4DstTnlNum; /* Destination tunnel number for MPLS-TP */
    UINT4 u4LspNum; /* LSP number for MPLS-TP, Source Tunnel instance for MPLS */
    UINT4 u4DstLspNum; /* LSP number for MPLS-TP, Destination Tunnel instance for MPLS */
    UINT4 u4SrcLocalMapNum; /* Not Applicable for Input API information, 
          used only in the output API information if tunnel 
          LSRId is mapped with node map table */
    UINT4 u4DstLocalMapNum; /* Not Applicable for Input API information,
          used only in the output API information if tunnel 
          LSRId is mapped with node map table */
}tMplsTnlLspId;

typedef struct 
{
    tIpAddr PeerAddr; /* Peer Address */ 
    UINT4 u4AddrType; /* IPv4/IPv6 */
    UINT4 u4AddrLen; /* Prefix length */
    UINT4 u4FtnIndex; /* FEC to NHLFE index */
    UINT4 u4XCIndex; /* Cross Connect index */
}tMplsLspId; 

typedef struct 
{
    UINT4 u4Label; /* MPLS Label */
    UINT1 u1Ttl; /* Time to live value */
    UINT1 u1Exp; /* Traffic Class value */
    UINT1 u1SI; /* Stack Indicator */
    UINT1 u1Protocol; /* Protocol which has distributed this label */
}tMplsLblInfo;

typedef struct 
{
    tMplsLblInfo LblInfo [MPLS_MAX_LABEL_STACK]; /* Seven layer label stack is
                                                    supported 
                                                    Outer label info. 
                                                    is at index 0, subsequent 
                                                    label infos present in the
                                                    subsequent array index of 
                                                    this array. */
    UINT4 u4InIf; /* Incoming interface */
    eDirection Direction;      /* Direction of the In Segment */
    UINT2 u2AddrFmly; /* IPv4/IPv6 */
    UINT1 u1LblAction; /* SWAP/POP */
    UINT1 u1LblStkCnt; /* Label stack count in the LblInfo array */   
}tMplsInSegInfo;

typedef struct 
{
    tMplsLblInfo LblInfo [MPLS_MAX_LABEL_STACK]; /* Seven layer label stack is
                                                    supported 
                                                    Outer label info. 
                                                    is at index 0, subsequent 
                                                    label infos present in the
                                                    subsequent array index of 
                                                    this array. */
    tIpAddr NHAddr;  /* NextHop Interface IP address */ 
    UINT4   u4OutIf; /* Outgoing interface */
    eDirection Direction;   /* Direction of the Out Segment */
    UINT1   au1NextHopMac [MAC_ADDR_LEN]; /* NextHop MAC address */
    UINT1   u1NHAddrType; /* Applicable for NextHop Interface 
                             address type - IPv4/IPv6 */
    UINT1   u1LblStkCnt; /* Label stack count in the LblInfo array */   
    UINT4   u4PhyPort; /* Outgoing  Physical Port  */
    UINT2   u2VlanId; /* Vlan ID of L3 interface associated to MPLS tunnel interface */
    UINT1   au1Pad[2];
}tMplsOutSegInfo;

typedef struct 
{
    UINT4 u4InIf; /* Incoming interface */
    UINT4 u4Inlabel; /* Incoming MPLS label */
}tMplsInLblInfo;

/* Reverse informations applicable only for 
 * co-routed bi-directional */
/* Forward informations will be used for all types of tunnels */
typedef struct 
{
    tMplsInSegInfo  FwdInSegInfo; /* Forward In Segment information */
    tMplsInSegInfo  RevInSegInfo; /* Reverse In Segment information */
    tMplsOutSegInfo FwdOutSegInfo; /* Forward Out Segment information */
    tMplsOutSegInfo RevOutSegInfo; /* Reverse Out Segment information */
    UINT4 u4XcSegInfo; /* MPLS_XC_FWD_IN_SEG_INFO/MPLS_XC_REV_IN_SEG_INFO/
                          MPLS_XC_FWD_OUT_SEG_INFO/MPLS_XC_REV_OUT_SEG_INFO*/
    UINT1 u1OperStatus; /* XC operational status */
    UINT1 au1Pad[3]; /* Padding */
}tMplsXcApiInfo;

typedef struct 
{
    tMplsLspId MplsLspId; /* LSP identifier */
    tMplsXcApiInfo XcApiInfo; /* Cross Connect Information */
    UINT4 u4ProactiveSessIndex; /* BFD session index */
}tNonTeApiInfo;

/************************* MEG API information starts ***********************/
typedef struct 
{
    UINT4 u4MegIndex; /* Maintenance Entity Group index */
    UINT4 u4MeIndex; /* Maintenance Entity index */
    UINT4 u4MpIndex; /* Maintenace Point index [MEP/MIP] */
}tMplsMegId;

typedef struct 
{
    UINT1 au1MegName [MPLS_MEG_NAME_LEN]; /* Maintenance Entity Group name */
    UINT1 au1MeName [MPLS_ME_NAME_LEN]; /* Maintenance Entity name */
}tMplsMegMeName;

typedef struct 
{
    tMplsMegId MplsMegId; /* Maintenance Entity Group identifier */
    tMplsMegMeName MplsMegMeName; /* MEG and ME names */
    union {
        tMplsTnlLspId ServiceTnlId; /* Tunnel identifiers */
        UINT4       u4PwIndex; /* Pseudowire Index */
    }unServiceInfo;
    UINT4 u4OperatorType; /* IP/ICC operator */
    tMplsGlobalNodeId MplsNodeId; /* MIP Global_ID::Node_ID */
    UINT4 u4MpIfIndex; /* MEP/MIP interface index */
    UINT4 u4IccSrcMepIndex; /* ICC based Source MEP index */
    UINT4 u4IccSinkMepIndex; /* ICC based Sink MEP index */
    UINT4 u4MpType; /* MEP/MIP */
    UINT4 u4MepDirection; /* DOWN/UP */
    UINT4 u4ProactiveSessIndex; /* BFD session index */
    UINT4 u4ProactiveTCValue; /* Traffic Class value for BFD session */
    UINT4 u4OnDemandTCValue; /* Traffic value for LSP ping */
    UINT4 u4MeOperStatus; /* MEG Down - MEG associated with this ME is down
                             ME down - ME is down
                             PATH down - Path associated with the MEG is down
                             OAM down - Proactive session status for the MEG */ 
    UINT1 au1Icc[MPLS_ICC_LENGTH+1]; /* ICC based MEG-ID-1 */
    UINT1 u1ServiceType; /* LSP/Pseudowire/Section */
    UINT1 u1ServiceLocation; /* Per Node/Per Interface */
    UINT1 au1Umc[MPLS_UMC_LENGTH+1]; /* ICC based MEG-ID-2 */
    UINT1 au1Pad[3]; /* Padding */
#define MegMplsTnlId unServiceInfo.ServiceTnlId
#define MegPwId unServiceInfo.u4PwIndex
}tMplsMegApiInfo;
/************************* MEG API information ends ***********************/

/************************* Tunnel API information starts ******************/
typedef struct 
{
    tMplsTnlLspId TnlLspId; /* Tunnel identifier */
    tMplsXcApiInfo XcApiInfo; /* Cross connect information */
    tMplsMegId MplsMegId;
    UINT4       u4TnlPrimaryInstance; /* Primary tunnel instance - 
                                     This value should be taken when the tunnel 
                                     changes the lsp dynamically */
    UINT4       u4TnlIfIndex; /* Tunnel logical interface */
    UINT4       u4ProactiveSessIndex; /* BFD session index that is 
                                         monitoring this Tunnel */       
    UINT4       u4TnlType; /* Tunnel type - mpls, mpls-tp, gmpls, h-lsp */
    tIpAddr     ExtendedTnlId; /* Same as IngressId applicable for MPLS */
    UINT1       au1TnlName[LSP_TNLNAME_LEN]; /* Tunnel name */
    UINT1       au1NextHopMac[MAC_ADDR_LEN]; /* Nexthop MAC address */
    UINT1       u1TnlRole; /* head/transit/tail */
    UINT1       u1OperStatus; /* up/down */
    UINT1       u1TnlOwner; /* snmp/ldp/crldp/rsvpTe */
    UINT1       u1TnlMode; /* unidirectional/co-routed bi-directional/
                              associated bi-directional */
    UINT1       u1LsrIdMapInfo; /* Provides the information that the 
                                   IngressId/EgressId are not complete, 
                                   should refer external Node map table */
    BOOL1       bIsUnNumberedIf; /* True if the u4TnlIfIndex is unnumbered */
    
    UINT1       u1TnlSgnlPrtcl; /* LDP/RSVP/Static */
    UINT1       u1TnlHwStatus; /*Tunnel hardware status*/
    UINT1       au1Pad[2]; /* Padding */
}tTeTnlApiInfo;
/************************* Tunnel API information ends ******************/

typedef struct
{
    UINT4       u4VcId; /* Virtual Circuit Identifier */
    UINT4       u4PwIndex; /* Pseudowire Index */
    tMplsNodeId SrcNodeId; /* Source node identifer */
    UINT4       u4SrcAcId; /* Source attachment circuit identifier */
    tMplsNodeId DstNodeId; /* Destination node identifier */
    UINT4       u4DstAcId; /* Destination attachment circuit identifier */
    UINT1       au1Agi[L2VPN_MAX_AI_LEN]; /* 129 FEC Group Attachment ID */
    UINT1       au1Saii[L2VPN_MAX_AI_LEN]; /* 129 FEC Source Attachment 
                                             Individual ID */
    UINT1       au1Taii[L2VPN_MAX_AI_LEN]; /* 129 FEC Target Attachment 
                                             Individual ID */
    UINT2       u2PwVcType; /* Ethernet/Ethernet Tagged */
    UINT1       u1PwVcOwner; /* manual/pwIdFecSignaling/genFecSignaling */
    UINT1       u1AgiType; /* Type 1 or Type 2*/
    UINT1       u1LocalAIIType; /* Type 1 or Type 2*/
    UINT1       u1RemoteAIIType; /* Type 1 or Type 2*/
    UINT1       u1AgiLen;
    UINT1       u1SaiiLen;
    UINT1       u1TaiiLen;
    UINT1       au1Pad[3]; /* Padding */ 
    UINT4 u4MegIndex; /* Maintenance Entity Group index */
    UINT4 u4MeIndex; /* Maintenance Entity index */
    UINT4 u4MpIndex; /* Maintenace Point index [MEP/MIP] */
    UINT4 u4PwIfIndex; /*Pseudo-wire If Index*/ 
}tPwPathId;

/************************* Pseudowire API information starts **************/
typedef struct 
{
    tPwPathId MplsPwPathId;
    union {
        tMplsTnlLspId MplsTnlId; /* TE Tunnel identifier */
        UINT4 u4NonTeXcIndex; /* NON-TE Tunnel identifierr */
        UINT4 u4OutIfIndex; /* pwOnly */
    }unOutTnlInfo;
    tMplsMegId MplsMegId;
    UINT4 u4OutVcLabel; /* Outgoing VC label */
    UINT4 u4InVcLabel; /* Incoming VC label */
    UINT4 u4LocalGroupID; /* 128 FEC Local Group Identifier */
    UINT4 u4RemoteGroupID; /* 128 FEC Remote Group Identifier */
    UINT4 u4ProactiveSessIndex; /* BFD Session index */
    /* VPWS is mentioned below, TODO VPLS i.e multiple AC ethernet 
     * entries to be done */
    UINT4 u4PortVlan; /* Attachment Circuit vlan */
 UINT4 u4PhyPort; /* Physical Port */
 UINT4 u4OutIf; /* Tunnel Interface Index */
    INT4  i4PortIfIndex; /* Attachment Circuit port */
 UINT4 u4PwIfIndex; /* Pseudo-Wire Interface Index */
 UINT4 u4PwVcIndex; /* Pseudo-Wire Index */
 UINT4 u4VpnId; /* VPN Identifier */
    UINT2 u2VlanId; /* Vlan ID of the L3 interface associated to MPLS tunnel interface */
    UINT2 u2VplsFdbId; /* Field to store the VPLS FDB Id*/
    UINT1 au1PwVcName[L2VPN_PWVC_MAX_LEN]; /* Pseudowire Name */
    UINT1 u1CcSelected; /* VCCV: ACH, TTL expiry and router alert label */
    UINT1 u1CvSelected; /* VCCV: ICMP ping, LSP ping and BFD */
    UINT1 u1PwVcMode; /* VPWS (1)- default, VPLS (2), IPLS (3) */
    UINT1 u1MplsType; /* TE/NON-TE/PwOnly */
    UINT1 au1NextHopMac[MAC_ADDR_LEN]; /* NextHop MAC address for PwOnly case */
    UINT1 u1Ttl; /* TTL value for the outgoing VC label */
    UINT1 u1PwType; /* MPLS/MPLS-TP */
    INT1  i1OperStatus; /* UP/DOWN/Lower Layer down */
    INT1  i1VlanMode;
    BOOL1 bOamEnable; /* TRUE/FALSE */
    UINT1 u1IpVersion; /* IP version of PW with LDP signalling */
#define TeTnlId unOutTnlInfo.MplsTnlId
#define NonTeTnlId unOutTnlInfo.u4NonTeXcIndex 
#define PwOnlyIfIndex unOutTnlInfo.u4OutIfIndex
}tPwApiInfo;

/************************* Pseudowire API information ends ****************/

/************************* MPLS LSR information starts ********************/
/************************* MPLS LSR information ends ******************/
typedef struct
{
    UINT2 u2MepIndex;
    UINT1 au1Icc[MPLS_ICC_LENGTH+1]; /* ICC based MEG-ID-1 */
    UINT1 au1Umc[MPLS_UMC_LENGTH+1]; /* ICC based MEG-ID-2 */
    UINT1 au1Pad[3]; /* Padding */
}tMplsIccMepTlv;

typedef tMplsIccMepTlv tMplsIccMep;

typedef struct 
{ 
    UINT4 u4PathType; /* MEG ID/MEG Name/NON-TE LSP/Tunnel/Pseudowire */
    union {
        tMplsMegId MplsMegId; /* MEG identifiers */
        tMplsMegMeName MplsMegMeName; /* MEG and ME names */
        tMplsIccMep MplsIccMep; /* ICC based MEP */
        tMplsLspId MplsLspId; /* LSP Identifier */
        tPwPathId MplsPwId; /* Pseudowire Identifier */
        tMplsTnlLspId MplsTnlId; /* Tunnel Identifier */
    }unPathId;
#define MegId unPathId.MplsMegId
#define MegMeName unPathId.MplsMegMeName    
#define LspId unPathId.MplsLspId
#define PwId  unPathId.MplsPwId
#define TnlId unPathId.MplsTnlId    
}tMplsPathId;

typedef struct 
{
    UINT1 u1PathStatus; /* Path (TNL/PW) status - UP/DOWN */
    UINT1 au1Pad[3]; /* Padding */
}tMplsOamPathStatus;

typedef struct
{
   UINT4 u4ProactiveSessIndex; /* BFD session index */
}tMplsOamSessionParams;

typedef struct 
{ 
    UINT4  au4ServiceOidList[MPLS_SERVICE_OID_LEN]; /* Service MEG/PW/LSP/TNL
                                                      indices */
    UINT2  u2ServiceOidLen; /* Service OID length */
    UINT1  au1Pad[2]; /* Padding */
}tMplsServiceOid;

/******************* For Packet transmission *********************************/
typedef struct 
{
   tCRU_BUF_CHAIN_HEADER *pBuf; /* MPLS transmit packet buffer - 
                                   except L2 and Eth headers */
   tIpAddr NextHopIpAddr; /* Nexthop IP address */ 
   UINT4 u4OutIfIndex; /* Outgoing logical interface */
   UINT1 au1DstMac [MAC_ADDR_LEN]; /* Destination MAC for Eth Header */
   UINT1 NextHopIpAddrType; /* IPv4/IPv6 */
   BOOL1 bIsMplsLabelledPacket; /* True - MPLS labelled packet, 
                                   False - Packet without MPLS encapsulation  */
   BOOL1 bIsUnNumberedIf; /* True if the u4OutIfIndex is unnumbered */
   UINT1 au1Pad[3];
}tMplsPktInfo;

/******************* For events/packet reception ****************************/
typedef struct {
    tCRU_BUF_CHAIN_HEADER *pBuf; /* This points to valid address 
                                    for packet events, 
                                    pBuf contains MPLS header (s) + Payload */
    tMplsPathId PathId; /* MEG/LSP/TNL/PW path information */
    UINT4 u4ContextId; /* Default context - 0 */
    UINT4 u4InIfIndex; /* Incoming interface index */
    UINT4 u4ProactiveSessIndex; /* Used to notify BFD module */
    UINT2 u2Event; /* MPLS_MEG_UP, MPLS_MEG_DOWN, 
                      MPLS_PSC_PACKET */
    UINT1 u1PayloadOffset; /* Application payload offset in the pBuf */
    UINT1 u1ServiceType; /* LSP/Pseudowire/Section - 
                            MPLS_PATH_TYPE_TUNNEL/MPLS_PATH_TYPE_PW */
}tMplsEventNotif;

/* Struct used by protocols to register with MPLS OAM MEG */
typedef struct 
{
    UINT4 u4ModId; /* Module ID(app Id) of the application that
                    * wants to get registered
                    */
    UINT4 u4Events; /* The events that are needed by the application -
                       MPLS_MEG_UP, MPLS_MEG_DOWN, 
                       MPLS_PSC_PACKET */
    INT4 (*pFnRcvPkt) (UINT4 u4SrcModId, VOID *pMplsEventNotif);
    /* Application specific callback function */  
}tMplsRegParams;

/* Structure for transmitted and 
 * received PDU count */
typedef struct
{
    UINT4 u4TxCnt;
    UINT4 u4RxCnt;
    UINT1 u1StatsType;
    INT1  ai1Pad[3];
}tMplsPktCnt;

typedef struct
{
    UINT4        u4PwId;       /* Pw Id */
    UINT4        u4LmmInterval; /* Lmm Interval - used to find 
                                   day counter reset*/
    tMplsPktCnt  MplsPrevCount;  /* Previous Day Total PDU Count */
}tMplsPktCntInfo;

typedef struct
{
    tMplsPktCnt MplsPktCnt;    /* PLS Total Current Pkt Count */
    tMplsPktCnt MplsTotDayCnt; /* MPLS Total Day Pkt Count */
}tMplsPktCntRes;

/****************** MPLS API type definitions **********************/
typedef struct 
{
    UINT4 u4SubReqType; /* Sub request type */
    UINT4 u4ContextId; /* Default context - 0 */
    UINT4 u4SrcModId; /* BFD, LSP Ping, MPLS-DB (static/control plane), 
                         L2VPN (static/control plane) */
    union {
        tMplsPathId MplsPathId; /* MEG name and ME name/MEG Id, ME Id, MP Id/
                                   LSP/TNL/PW */
        tMplsInLblInfo MplsInLblInfo; /* Incoming label information */ 
        tMplsPktInfo MplsPktInfo; /* Packet information used by application 
                                   for transmission */
        tMplsNodeId MplsNodeId; /* Applications:
                                   1. GlobalIdNodeId to get LocalMapNumber
                                   2. Source address information to get the 
                                      reverse path information */
        tMplsPktCntInfo MplsPktCntInfo; /* Information to get PDU count 
                                           for current interval */
        UINT4 u4LocalMapNum; /* Local map number for GlobalId::NodeId*/
    }unIntInfo;
    union {
        tMplsPathId ServicePathId; /* Tnl/Pw path identifiers */
        tMplsOamSessionParams MplsOamSessionParams; /* Session parameters */
        tMplsRegParams MplsRegParams; /* Registration parameters */
        UINT2 u2AchChnlCode;
        BOOL1 bY1731OamEnabled; /* TRUE/FALSE */ 
        UINT1 u1ElpsMode; /* 1+1 or 1:1 */
        BOOL1 bRFC6374Enabled; /* TRUE/FALSE */
        UINT1 au1Pad[3];
    }unOamInfo;
    tMplsOamPathStatus MplsOamPathStatus; /* Path status */
#define InPathId unIntInfo.MplsPathId
#define InLocalNum unIntInfo.u4LocalMapNum
#define InNodeId unIntInfo.MplsNodeId    
#define InInLblInfo unIntInfo.MplsInLblInfo
#define InPktInfo unIntInfo.MplsPktInfo
#define InRegParams unOamInfo.MplsRegParams
#define InOamPathStatus MplsOamPathStatus
#define InOamSessionParams unOamInfo.MplsOamSessionParams
#define InServicePathId unOamInfo.ServicePathId
#define InAchChnlCode unOamInfo.u2AchChnlCode    
#define InY1731OamStatus unOamInfo.bY1731OamEnabled  
#define InElpsMode unOamInfo.u1ElpsMode
#define InRFC6374Status unOamInfo.bRFC6374Enabled
}tMplsApiInInfo; 

typedef struct 
{
    UINT4 u4ContextId; /* Default context - 0 */
    UINT4 u4PathType; /* Used to provide information about the path selected -
                         tTeTnlApiInfo/tNonTeApiInfo/tPwApiInfo */
    UINT4 u4SrcModId; /* BFD, LSP Ping, MPLS-DB (static/control plane),
                         L2VPN (static/control plane) */

    union {
        tMplsPathId MplsPathId; /* Path (MEG/LSP/TNL/PW) identifiers */
        tMplsServiceOid MplsServiceOid; /* First accessible columnar object of 
                                       path table */
        tMplsGlobalNodeId MplsNodeId; /* MPLS Node identifier */
        UINT4 u4LocalMapNum; /* Local map number for GlobalId::NodeId*/
        tMplsMegApiInfo MplsMegApiInfo; /* MEG information */
        tTeTnlApiInfo MplsTeTnlApiInfo; /* Tunnel information */
        tNonTeApiInfo MplsNonTeApiInfo; /* NON-TE Tunnel information */
        tPwApiInfo MplsPwApiInfo; /* Pseudowire information */
        tMplsPktCntRes MplsPktCntRes;   /* No Of PDUs Transmitted and 
                                           Received Result */
    }unOutInfo;
#define  OutPathId  unOutInfo.MplsPathId
#define  OutServiceOid unOutInfo.MplsServiceOid
#define  OutNodeId unOutInfo.MplsNodeId    
#define  OutLocalNum unOutInfo.u4LocalMapNum 
#define  OutMegInfo unOutInfo.MplsMegApiInfo
#define  OutTeTnlInfo unOutInfo.MplsTeTnlApiInfo
#define  OutNonTeTnlInfo unOutInfo.MplsNonTeApiInfo
#define  OutPwInfo unOutInfo.MplsPwApiInfo
}tMplsApiOutInfo; 

/* MPLS ACH header - to be defined in *tdfs.h */
typedef struct
{
    UINT4               u4AchType; /* First nibble value - 0001 */
    UINT1               u1Version; /* Version */
    UINT1               u1Rsvd; /* Reserved */
    UINT2               u2ChannelType; /* Channel code */
} tMplsAchHdr;

typedef struct
{
    UINT2               u2TlvHdrLen; /* ACH TLV header TLV */
    UINT2               u2Rsvd; /* Reserved */
} tMplsAchTlvHdr;

/* MPLS ACH TLV header - to be defined in *tdfs.h */
typedef struct
{
  UINT4   u4NodeId;
  UINT4   u4IfNum;
}tMplsIfId;

typedef struct
{
    tMplsGlobalNodeId GlobalNodeId; /* Global Identifier & Node Identifier */
    UINT2 u2TnlNum;
    UINT2 u2LspNum;
}tMplsIpLspMepTlv;

typedef struct
{
    UINT4 u4GlobalId;
    UINT4 u4NodeId;
    UINT4 u4AcId;
    UINT1 au1Agi[L2VPN_MAX_AI_LEN]; /* 129 FEC Group Attachment ID */
    UINT1 u1AgiLen;
    UINT1 u1AgiType;
    UINT1 au1Pad[2];
}tMplsIpPwMepTlv;

typedef struct
{
    union {
        tMplsIccMepTlv IccMepTlv; /* ICC based MEP TLV for LSP & PW */
        tMplsIpLspMepTlv IpLspMepTlv; /* IP based LSP MEP TLV */
        tMplsIpPwMepTlv IpPwMepTlv; /* IP based PW MEP TLV */
    }unMepTlv;
}tMplsMepTlv;

typedef struct
{
    tMplsGlobalNodeId GlobalNodeId; /* Global Identifier & Node Identifier */
    UINT4 u4IfNum;
}tMplsMipTlv;

typedef struct
{
  UINT1 u1AuthType;
  UINT1 u1AuthLen;
  UINT2 u2AuthData; /* TODO Check the Authentication data value size */
}tAchAuthTlv;

#define MPLS_ACH_NULL_TLV             0
#define MPLS_ACH_SOURCE_V4_ADDR_TLV   1
#define MPLS_ACH_SOURCE_V6_ADDR_TLV   2
#define MPLS_ACH_ICC_ID_TLV           3
#define MPLS_ACH_GLOBAL_ID_TLV        4
#define MPLS_ACH_IF_ID_TLV            5
#define MPLS_ACH_AUTH_TLV             6
#define MPLS_ACH_MIP_TLV              7
#define MPLS_ACH_IP_LSP_MEP_TLV       8
#define MPLS_ACH_IP_PW_MEP_TLV        9
#define MPLS_ACH_ICC_MEP_TLV         10

typedef struct
{
    UINT2 u2TlvType; /* NULL TLV, IPv4/IPv6 Source address TLV, 
                        ICC TLV, Global_ID TLV, IF_ID TLV, Auth TLV */
    UINT2 u2TlvLen; /* Length of the ACH TLV */
    union {
        tIpAddr SrcIpAddr;
        UINT4   u4GlobalId;
        tMplsIfId IfId;
        tAchAuthTlv AuthTlv;
        tMplsMipTlv MipTlv;
        tMplsMepTlv MepTlv;
        UINT1 au1Icc[MPLS_ICC_LENGTH]; /* ICC based MEG-ID */
        UINT1 au1Pad[2];
    }unAchValue;
}tMplsAchTlv;

/* Function declaration */
INT4 MplsApiHandleExternalRequest (UINT4 u4MainReqType,
                                   tMplsApiInInfo *pInMplsApiInfo,
                                   tMplsApiOutInfo *pOutMplsApiInfo);
INT4 L2VpnApiHandleExternalRequest (UINT4 u4MainReqType,
                                    tMplsApiInInfo *pInMplsApiInfo,
                                    tMplsApiOutInfo *pOutMplsApiInfo);

BOOL1 L2VpnApiIsPwStatic (UINT4 u4PwIndex);
INT4
L2VpnApiGetPwIndexFromPwIfIndex (UINT4 u4PwIfIndex, UINT4 *pu4PwIndex);

/* Structure to Hold Ach Channel Types */
typedef struct _MplsAchChannelType {
    UINT2  u2AchChnlTypeCcBfd;
    UINT2  u2AchChnlTypeCvBfd;
    UINT2  u2AchChnlTypeCcIpv4;
    UINT2  u2AchChnlTypeCvIpv4;
    UINT2  u2AchChnlTypeCcIpv6;
    UINT2  u2AchChnlTypeCvIpv6;
} tMplsAchChannelType; /*Ach Channel Type */


VOID
MplsApiUpdateChannelTypes (tMplsAchChannelType * pMplsAchChannelTypeInfo, 
                           BOOL1 bLockReq);

VOID
L2VpnApiGetVplsStorageType (UINT4 u4VplsIndex, UINT1 *pu1StorageType);

#ifdef MPLS_L3VPN_WANTED
/* Functions for MPLS L3VPN */
/* API exposed to BGP for regsitering callbacks */
/* This function will be called by BGP module to register the callback functions */
/* Returns: OSIX_SUCCESS/OSIX_FAILURE */
UINT4 MplsL3VpnRegisterCallback (tMplsL3VpnRegInfo *pRegInfo);

UINT4 MplsL3VpnDeRegisterCallback (tMplsL3VpnRegInfo *pRegInfo);

/* This API is called to get the LSP infomation for a particular prefix */
/* Returns: OSIX_SUCCESS/OSIX_FAILURE */
UINT4 MplsGetNonTeLspForDest (UINT4 u4Prefix, UINT4 *pu4LspLabel, UINT1 *pu1LspStatus);

/* This API is called to get the RSVP-TE LSP infomation for a particular prefix */
/* Returns: OSIX_SUCCESS/OSIX_FAILURE */
UINT4
MplsGetRsvpTeLspForDest (UINT4 u4Prefix, UINT4 *pu4LspLabel, UINT1 *pu1LspStatus);


/* This API is called to add/delete a Route Entry in MplsL3VpnRteTable */
/* Returns: OSIX_SUCCESS/OSIX_FAILURE */
UINT4 MplsL3VpnBgp4RouteUpdate (tMplsL3VpnBgp4RouteInfo *pRouteInfo);

/* This API is called to inform MPLS whether the given LSP is of interest to BGP or not */
/* u1Status: OSIX_TRUE- If BGP is interted to use the LSP, OSIX_FALSE- If BGP is not interested */
/* Returns: OSIX_SUCCESS/OSIX_FAILURE */
UINT4 MplsL3VpnBgp4LspUsageStatusUpdate (UINT4 u4Prefix, UINT4 u4LspLabel, BOOL1 u1Status);

UINT4 MplsBgp4ASNInUse (BOOL1 *pbInUse);

INT4 L3VpnCliParseAndGenerateRdRt (UINT1* pu1RandomString, UINT1* pu1RdRt);

UINT4 L3VpnUtilFindNextFreeRtIndex(UINT4 contextId, UINT4 *pu4FreeIndex);



#endif

#ifdef HVPLS_WANTED
INT4
MplsApiFillRegInfo (tMplsApiInInfo * pInMplsApiInfo);
#endif

#endif /*_MPLSAPI_H*/
/*---------------------------------------------------------------------------*/
/*                        End of file mplsapi.h                              */
/*---------------------------------------------------------------------------*/
