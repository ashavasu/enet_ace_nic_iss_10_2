/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: radius.h,v 1.29 2015/06/27 11:40:16 siva Exp $
 *
 * Description:This file contains the exported definitions and  
 *             macros of RADIUS                                
 *
 *******************************************************************/
#ifndef _RADIUS_H
#define _RADIUS_H
#include "dns.h"

#define RADIUS_LOCK()   RadiusLock ()
#define RADIUS_UNLOCK() RadiusUnLock ()

/* RADIUS Packet types */

#define  ACCESS_REQUEST                1
#define  ACCESS_ACCEPT                 2
#define  ACCESS_REJECT                 3
#define  ACCOUNTING_REQUEST            4
#define  ACCOUNTING_RESPONSE           5
#define  ACCESS_CHALLENGE             11

#define  RAD_TIMEOUT                   0
#define  MAX_SECRET_SIZE               255
/* RADIUS Packet Attribute Types */

#define  A_USER_NAME                   1
#define  A_USER_PASSWORD               2
#define  A_CHAP_PASSWORD               3
#define  A_NAS_IP_ADDRESS              4
#define  A_NAS_PORT                    5
#define  A_SERVICE_TYPE                6
#define  A_FRAMED_PROTOCOL             7
#define  A_FRAMED_IP_ADDRESS           8
#define  A_FRAMED_IP_NETMASK           9
#define  A_FRAMED_ROUTING             10
#define  A_FILTER_ID                  11
#define  A_FRAMED_MTU                 12
#define  A_FRAMED_COMPRESSION         13
#define  A_LOGIN_IP_HOST              14
#define  A_LOGIN_SERVICE              15
#define  A_LOGIN_TCP_PORT             16

    /* Attribute Type 17 NOT ASSIGNED */

#define  A_REPLY_MESSAGE              18
#define  A_CALLBACK_NUMBER            19
#define  A_CALLBACK_ID                20

    /* Attribute Type 21 NOT ASSIGNED */

#define  A_FRAMED_ROUTE               22
#define  A_FRAMED_IPX_NETWORK         23
#define  A_STATE                      24
#define  A_CLASS                      25
#define  A_VENDOR_SPECIFIC            26
#define  A_SESSION_TIMEOUT            27
#define  A_IDLE_TIMEOUT               28
#define  A_TERMINATION_ACTION         29
#define  A_CALLED_STATION_ID          30
#define  A_CALLING_STATION_ID         31
#define  A_NAS_IDENTIFIER             32
#define  A_PROXY_STATE                33
#define  A_LOGIN_LAT_SERVICE          34
#define  A_LOGIN_LAT_NODE             35
#define  A_LOGIN_LAT_GROUP            36
#define  A_FRAMED_APPLETALK_LINK      37
#define  A_FRAMED_APPLETALK_NETWORK   38
#define  A_FRAMED_APPLETALK_ZONE      39
#define  A_ACCT_STATUS_TYPE           40
#define  A_ACCT_DELAY_TIME            41
#define  A_ACCT_INPUT_OCTETS          42
#define  A_ACCT_OUTPUT_OCTETS         43
#define  A_ACCT_SESSION_ID            44
#define  A_ACCT_AUTHENTIC             45
#define  A_ACCT_SESSION_TIME          46
#define  A_ACCT_INPUT_PACKETS         47
#define  A_ACCT_OUTPUT_PACKETS        48
#define  A_ACCT_TERMINATE_CAUSE       49
#define  A_ACCT_MULTI_SESSION_ID      50
#define  A_ACCT_LINK_COUNT            51
#define  A_ACCT_INPUT_GIGAWORDS       52
#define  A_ACCT_OUTPUT_GIGAWORDS      53
#define  A_EVENT_TIMESTAMP            55
#define  A_CHAP_CHALLENGE             60
#define  A_NAS_PORT_TYPE              61
#define  A_PORT_LIMIT                 62
#define  A_LOGIN_LAT_PORT             63
#define  A_TUNNEL_TYPE                64
#define  A_TUNNEL_MEDIUM_TYPE         65
#define  A_TUNNEL_CLIENT_ENDPOINT     66
#define  A_TUNNEL_SERVER_ENDPOINT     67
#define  A_TUNNEL_PASSWORD            69
#define  A_ARAP_PASSWORD              70
#define  A_ARAP_FEATURES              71
#define  A_ARAP_ZONE_ACCESS           72
#define  A_ARAP_SECURITY              73
#define  A_ARAP_SECURITY_DATA         74
#define  A_PASSWORD_RETRY             75
#define  A_PROMPT                     76
#define  A_CONNECT_INFO               77
#define  A_CONFIGURATION_TOKEN        78
#define  A_EAP_MESSAGE                79
#define  A_MESSAGE_AUTHENTICATOR      80
#define  A_TUNNEL_PRIVATE_GROUP_ID    81
#define  A_TUNNEL_ASSIGNMENT_ID       82
#define  A_TUNNEL_PREFERENCE          83
#define  A_ARAP_CHALLENGE_RESPONSE    84
#define  A_ACCT_INTERIM_INTERVAL      85
#define  A_NAS_PORT_ID                87
#define  A_FRAMED_POOL                88
#define  A_TUNNEL_CLIENT_ATUH_ID      90
#define  A_TUNNEL_SERVER_AUTH_ID      91

/* Service Type for attribute A_SERVICE_TYPE */

#define  SERT_LOGIN                    1
#define  SERT_FRAMED                   2
#define  SERT_CALLBACK_LOGIN           3
#define  SERT_CALLBACK_FRAMED          4
#define  SERT_OUTBOUND                 5
#define  SERT_ADMINISTRATIVE           6
#define  SERT_NAS_PROMPT               7
#define  SERT_AUTHENTICATE_ONLY        8
#define  SERT_CALLBACK_NAS_PROMPT      9
#define  SERT_CALL_CHECK               10
#define  SERT_CALLBACK_ADMINISTRATIVE  11
#define  RAD_BANDWIDTH           1
#define  RAD_DLBANDWIDTH         2
#define  RAD_ULBANDWIDTH         3
#define  RAD_VOLUME              4
#define  RAD_TIME                5

/* Framed Protocol Types for attribute A_FRAMED_PROTOCOL */
#define  FP_PPP                        1
#define  FP_SLIP                       2
#define  FP_ARAP                       3
#define  FP_GANDALF                    4
#define  FP_XYLOGICS                   5

/* Routing methods for attribute A_FRAMED_ROUTING */

#define  SEND_RUTE_PKT                 1
#define  LISTEN_RUTE_PKT               2
#define  SEND_LISTEN                   3

/* Types of compression for attribute A_FRAMED_COMPRESSION */
#define  COMP_NONE                     0
#define  COMP_VJ_TCP_IP                1
#define  COMP_IPX                      2
#define  COMP_STAC_LZS                 3

/* Login Service Types for attribute A_LOGIN_SERVICE */

#define  LOG_TELNET                    0
#define  LOG_RLOGIN                    1
#define  LOG_TCP_CLEAR                 2
#define  LOG_POSTMASTER                3
#define  LOG_LAT                       4

/* Termination actions for attribute A_TERMINATION_ACTION */

#define  TERA_DEFAULT                  1
#define  TERA_RADIUS_REQ               2

/* NAS Port types for attribute A_NAS_PORT_TYPE */

#define  NASP_ASYNC                    0
#define  NASP_SYNC                     1
#define  NASP_ISDN_SYNC                2
#define  NASP_ISDN_ASYNC_V120          3
#define  NASP_ISDN_ASYNC_V110          4
#define  NASP_VIRTUAL                  5
#define  NASP_PIAFS                    6
#define  NASP_HDLC_CLEAR_CHANNEL       7
#define  NASP_X_25                     8
#define  NASP_X_75                     9
#define  NASP_G_3_FAX                 10
#define  NASP_SDSL                    11
#define  NASP_ADSL_CAP                12
#define  NASP_ADSL_DMT                13
#define  NASP_IDSL                    14
#define  NASP_ETHERNET                15
#define  NASP_XDSL                    16
#define  NASP_CABLE                   17
#define  NASP_WIRELESS_OTHER          18
#define  NASP_WIRELESS_IEEE_802_11    19 

/* Accounting status type for attribute A_ACCT_STATUS_TYPE */

#define  ACCT_START                    1
#define  ACCT_STOP                     2
#define  ACCT_INTERIM_UPDATE           3
#define  ACCT_ON                       7
#define  ACCT_OFF                      8

/* Accounting Authentication type for attribute A_ACCT_AUTHENTIC */

#define  AUTH_RADIUS                   1
#define  AUTH_LOCAL                    2
#define  AUTH_REMOTE                   3

/* Accounting terminate cause for attribute A_ACCT_TERMINATE_CAUSE */

#define  TERC_USER_REQ                 1
#define  TERC_LOST_CARRIER             2
#define  TERC_LOST_SERVICE             3
#define  TERC_IDLE_TIMEOUT             4
#define  TERC_SESSION_TIMEOUT          5
#define  TERC_ADMIN_RESET              6
#define  TERC_ADMIN_REBOOT             7
#define  TERC_PORT_ERROR               8
#define  TERC_NAS_ERROR                9
#define  TERC_NAS_REQ                 10
#define  TERC_NAS_REBOOT              11
#define  TERC_PORT_UNNEEDED           12
#define  TERC_PORT_PREEMPTED          13
#define  TERC_PORT_SUSPENDED          14
#define  TERC_SERVICE_UNAVAILABLE     15
#define  TERC_CALLBACK                16
#define  TERC_USER_ERROR              17
#define  TERC_HOST_REQUEST            18
/* Protocol Types used in the Radius input */

#define  PRO_CHAP                           1
#define  PRO_PAP                            2
#define  PRO_OTHERS                         3
#define  PRO_MS_CHAP                        4
#define  MS_CHAP_CPW1                       5
#define  MS_CHAP_CPW2                       6
#define  PRO_EAP                            7

/*************************** SOFTWARE DEFINITIONS ************************/

/* Socket related constants */
#ifndef OK
#define OK        0 
#endif

#ifndef NOT_OK
#define NOT_OK   -1 
#endif

/* Different length values used */

#define  LEN_NAS_ID                        64
#define  LEN_USER_NAME                     64
#define  LEN_UTF8_USER_NAME                64
#define  LEN_CHAP_CHALLENGE                64
#define  LEN_CHAP_RESPONSE                 16
#define  LEN_PASSWD                        64
#define  LEN_PWD_PAD                       16
#define  LEN_SESSION_ID                    52
#define  LEN_SECRET                        48
#define  LEN_REQ_AUTH_AUTH                 16
#define  LEN_REQ_AUTH_ACC                  16
#define  LEN_RES_AUTH                      16
#define  LEN_SERVICE_STRING                256
/* This macro is related with creation of MemPool .
 * So Change in value of this macro must be properly updated in radsize.txt*/
#define  LEN_TX_PKT                      2000
#define  LEN_RX_PKT                      4096
#define  LEN_PKT_MIN                       20
#define  LEN_PKT_MAX                     4096
#define  LEN_DIGEST                        16
#define  NO_OF_USER_ENTRIES                10
#define  NO_OF_SERVERS                      5
/* Attribute lengths */

#define  LEN_MIN_PROXY_STATE                3
#define  MAX_STATE_LEN                    255
#define  MAX_RESPONSE_LEN                  16 
#define  LEN_MESSAGE_AUTHENTICATOR         16
#define  LEN_RAD_ATTRIBUTE                  2    /** Type + Len **/     
#define  LEN_EAP_MESSAGE    LEN_RAD_ATTRIBUTE

#define  LEN_CHAP_PASSWORD                 19
#define  LEN_NAS_IP_ADDRESS                 6
#define  LEN_NAS_PORT                       6
#define  LEN_SERVICE_TYPE                   6
#define  LEN_FRAMED_PROTOCOL                6
#define  LEN_FRAMED_IP_ADDRESS              6
#define  LEN_FRAMED_IP_NETMASK              6
#define  LEN_FRAMED_ROUTING                 6
#define  LEN_FRAMED_MTU                     6
#define  LEN_FRAMED_COMPRESSION             6
#define  LEN_LOGIN_IP_HOST                  6
#define  LEN_LOGIN_SERVICE                  6
#define  LEN_LOGIN_TCP_PORT                 6
#define  LEN_FRAMED_IPX_NETWORK             6
#define  LEN_SESSION_TIMEOUT                6
#define  LEN_IDLE_TIMEOUT                   6
#define  LEN_TERMINATION_ACTION             6
#define  LEN_LOGIN_LAT_GROUP               34
#define  LEN_FRAMED_APPLETALK_LINK          6
#define  LEN_FRAMED_APPLETALK_NETWORK       6
#define  LEN_ACCT_STATUS_TYPE               6
#define  LEN_ACCT_DELAY_TIME                6
#define  LEN_ACCT_INPUT_OCTETS              6
#define  LEN_ACCT_OUTPUT_OCTETS             6
#define  LEN_ACCT_INPUT_GIGAWORDS           6
#define  LEN_ACCT_OUTPUT_GIGAWORDS           6
#define  LEN_ACCT_AUTHENTIC                 6
#define  LEN_ACCT_SESSION_TIME              6
#define  LEN_ACCT_INPUT_PACKETS             6
#define  LEN_ACCT_OUTPUT_PACKETS            6
#define  LEN_ACCT_TERMINATE_CAUSE           6
#define  LEN_ACCT_LINK_COUNT                6
#define  LEN_NAS_PORT_TYPE                  6
#define  LEN_PORT_LIMIT                     6

#define  LEN_NUM_ATTR                       6


/* EAP related definitions */

#define  EAP_RESPONSE                       2
#define  EAP_MD5_MAX_CHALLENGE_LENGTH      64
#define  EAP_MD5_MAX_RESPONSE_LENGTH       16
#define  EAP_TYPE_IDENTITY                  1
#define  EAP_TYPE_MD5                       4
#define  EAP_HDR_LEN                        4
#define  EAP_TYPE_FIELD_LEN                 1
#define  EAP_CODE_FIELD_LEN                 1
#define  EAP_ID_FIELD_LEN                   1
#define  EAP_TYPE_TLS                       13
#define  EAP_PKT_DATA_MAX_LENGTH            1496
#define  EAP_FRAGMENT_SIZE                  254

#define  VALUE_SIZE_FIELD_LEN               1

/* Vandor ID to be used in Access-Accept message*/
#define  ARICENT_VENDOR_ID                 29601
#define  CALLED_STATION_ID_ATTRIBUTE        30
#define  CALLING_STATION_ID_ATTRIBUTE       31

/* MS CHAP related definitions */

#define  MS_CHAP_VENDOR_ID                311

/* MS CHAP attributes */

#define  SA_MS_CHAP_CHALLENGE              11
#define  SA_MS_CHAP_RESPONSE                1
#define  SA_MS_CHAP_DOMAIN                 10
#define  SA_MS_CHAP_ERROR                   2
#define  SA_MS_CHAP_CPW1                    3
#define  SA_MS_CHAP_CPW2                    4
#define  SA_MS_CHAP_LM_ENC_PW               5
#define  SA_MS_CHAP_NT_ENC_PW               6
#define  SA_MS_CHAP_MPPE_KEYS              12
#define  SA_MS_MPPE_SEND_KEY               16
#define  SA_MS_MPPE_RECV_KEY               17

/* MS CHAP lengths */

#define  LEN_MS_CHAP_CHALLENGE             64
#define  LEN_MS_CHAP_RESPONSE_LM           24
#define  LEN_MS_CHAP_RESPONSE_NT           24
#define  LEN_MS_CHAP_OLD_PW_LM             16
#define  LEN_MS_CHAP_OLD_PW_NT             16
#define  LEN_MS_CHAP_NEW_PW_LM             16
#define  LEN_MS_CHAP_NEW_PW_NT             16
#define  LEN_MS_CHAP_OLD_HASH_LM           16
#define  LEN_MS_CHAP_OLD_HASH_NT           16
#define  LEN_MS_CHAP_ENC_PW_LM            516
#define  LEN_MS_CHAP_ENC_PW_NT            516

#define  LEN_MS_CHAP_ENC_PW_FRAG          172
#define  NO_OF_FRAGS_MS_CHAP_ENC_PW         3

/* MS CHAP attribute lengths */

#define  LEN_SA_MS_CHAP_RESPONSE                            52
#define  LEN_SA_MS_CHAP_CPW1                                72
#define  LEN_SA_MS_CHAP_CPW2                                86
#define  LEN_SA_MS_CHAP_MPPE_KEYS                           34
#define  LEN_SA_MS_CHAP_LM_ENC_PW    LEN_MS_CHAP_ENC_PW_FRAG+6
#define  LEN_SA_MS_CHAP_NT_ENC_PW    LEN_MS_CHAP_ENC_PW_FRAG+6

#define  LEN_SA_MS_MPPE_SALT               2 
/* MS CHAP attribue flags and codes */

#define  MS_CHAP_CPW1_CODE                                   5
#define  MS_CHAP_CPW2_CODE                                   6
#define  MS_CHAP_LM_ENC_PW_CODE              MS_CHAP_CPW2_CODE
#define  MS_CHAP_NT_ENC_PW_CODE              MS_CHAP_CPW2_CODE
#define  MS_CHAP_RESPONSE_FLAG                               0
#define  MS_CHAP_CPW1_FLAG                                   0
#define  MS_CHAP_CPW2_FLAG                                   0
 


#define  NO_ATTRIBUTE                                       -1
#define  RAD_INTERFACE_DEFVAL                       0xffffffff
/* definition of DEFAULT Values: */
#define  RAD_NAS_IDENTIFIER_STRING                 "FutureNAS"

#define  IPV4_TUNNEL_MEDIUM           1 

#define L2TP_TUNNEL_TYPE              1

#define  RADIUS_KEY_SAVE_FILE                  "radiuskey"

#define RAD_IF_MEMBER_MAX_SIZE          256   
#define RAD_MAX_U2_VAR_SIZE            65536
#define RAD_KEY_SIZE                     256
#define RAD_USER_NAME_SIZE               32

/* Data Structure for RADIUS input for Authentication */

typedef struct chapuser {
   UINT4   a_u4Utf8UserName[LEN_UTF8_USER_NAME]; /* Used when multilingual characters are available 
                                                  * '\0' is must at the end of multilingual string
                                                  */ 
   UINT1   a_u1UserName[LEN_USER_NAME];          /* CHAP User */
   UINT1   a_u1Challenge[LEN_CHAP_CHALLENGE];    /* CHAP Challenge */
   UINT1   a_u1Response[LEN_CHAP_RESPONSE];      /* CHAP response */
   UINT1   u1_Identifier;                        /* CHAP Identifier */
   UINT1   u1_IsUtf8EncodingReqd;                /* When a_u4Utf8UserName is used at 
                                                  * that time this flag is must to be set
                                                  * 1:UTF-8 Encoding is required
                                                  * 0:UTF-8 Encoding isn't required
         */
   UINT1   u1Align[2];
}tUSER_INFO_CHAP;

typedef struct eapuser {
   UINT4   a_u4Utf8UserName[LEN_UTF8_USER_NAME]; /* Used when multilingual characters are available 
                                                  * '\0' is must at the end of multilingual string
                                                  */ 
   UINT1   a_u1UserName[LEN_USER_NAME];           /* EAP User */
   UINT1   a_u1Response[EAP_PKT_DATA_MAX_LENGTH]; /* EAP response */
   UINT1   u1_IsUtf8EncodingReqd;                /* When a_u4Utf8UserName is used at 
                                                  * that time this flag is must to be set
                                                  * 1:UTF-8 Encoding is required
                                                  * 0:UTF-8 Encoding isn't required
         */
   UINT1   u1Align[3];
}tUSER_INFO_EAP;

typedef struct papuser {
   UINT4   a_u4Utf8UserName[LEN_UTF8_USER_NAME]; /* Used when multilingual characters are available
                                                  * '\0' is must at the end of multilingual string
                                                  */ 
   UINT1   a_u1UserName[LEN_USER_NAME];           /* PAP user name */
   UINT1   a_u1UserPasswd[LEN_PASSWD];            /* PAP password */
   UINT1   u1_IsUtf8EncodingReqd;                /* When a_u4Utf8UserName is used at 
                                                  * that time this flag is must to be set
                                                  * 1:UTF-8 Encoding is required
                                                  * 0:UTF-8 Encoding isn't required
         */
   UINT1   u1Align[3];
}tUSER_INFO_PAP;

typedef struct otheruser {
   UINT4   a_u4Utf8UserName[LEN_UTF8_USER_NAME]; /* Used when multilingual characters are available 
                                                  * '\0' is must at the end of multilingual string
                                                  */ 
   UINT1   a_u1UserName[LEN_USER_NAME];           /* Other user name */
   UINT1   a_u1UserPasswd[LEN_PASSWD];            /* Other password */
   UINT1   u1_IsUtf8EncodingReqd;                /* When a_u4Utf8UserName is used at 
                                                  * that time this flag is must to be set
                                                  * 1:UTF-8 Encoding is required
                                                  * 0:UTF-8 Encoding isn't required
         */
   UINT1   u1Align[3];
}tUSER_INFO_OTHERS;

typedef struct mschapuser {
   UINT4   a_u4Utf8UserName[LEN_UTF8_USER_NAME]; /* Used when multilingual characters are available
                                                  * '\0' is must at the end of multilingual string
                                                  */ 
   UINT1   a_u1UserName[LEN_USER_NAME];
   UINT1   a_u1UserPasswd[LEN_PASSWD];
   UINT1   a_u1Challenge[LEN_MS_CHAP_CHALLENGE];
   UINT1   a_u1ResponseLM[LEN_MS_CHAP_RESPONSE_LM];
   UINT1   a_u1ResponseNT[LEN_MS_CHAP_RESPONSE_NT];
   UINT1   u1_Identifier;
   UINT1   u1_IsUtf8EncodingReqd;                /* When a_u4Utf8UserName is used at 
                                                  * that time this flag is must to be set
                                                  * 1:UTF-8 Encoding is required
                                                  * 0:UTF-8 Encoding isn't required
         */
   UINT1   u1Pad[2];
}tUSER_INFO_MS_CHAP;

typedef struct mschapcpw1 {
   UINT4   a_u4Utf8UserName[LEN_UTF8_USER_NAME]; /* Used when multilingual characters are available
                                                  * '\0' is must at the end of multilingual string
                                                  */ 
   UINT1   a_u1UserName[LEN_USER_NAME];
   UINT1   a_u1UserPasswd[LEN_PASSWD];
   UINT1   a_u1OldPwLM[LEN_MS_CHAP_OLD_PW_LM];
   UINT1   a_u1NewPwLM[LEN_MS_CHAP_NEW_PW_LM];
   UINT1   a_u1OldPwNT[LEN_MS_CHAP_OLD_PW_NT];
   UINT1   a_u1NewPwNT[LEN_MS_CHAP_NEW_PW_NT];
   UINT2   u2_NewLMPwLength;
   UINT1   u1_Identifier;
   UINT1   u1_IsUtf8EncodingReqd;                /* When a_u4Utf8UserName is used at 
                                                  * that time this flag is must to be set
                                                  * 1:UTF-8 Encoding is required
                                                  * 0:UTF-8 Encoding isn't required
                                 */
   
}tMS_CHAP_CPW1;

typedef struct mschapcpw2 {
   UINT4   a_u4Utf8UserName[LEN_UTF8_USER_NAME]; /* Used when multilingual characters are available
                                                  * '\0' is must at the end of multilingual string
                                                  */ 
   UINT1   a_u1UserName[LEN_USER_NAME];
   UINT1   a_u1UserPasswd[LEN_PASSWD];
   UINT1   a_u1OldLMHash[LEN_MS_CHAP_OLD_HASH_LM];
   UINT1   a_u1OldNTHash[LEN_MS_CHAP_OLD_HASH_NT];
   UINT1   a_u1ResponseLM[LEN_MS_CHAP_RESPONSE_LM];
   UINT1   a_u1ResponseNT[LEN_MS_CHAP_RESPONSE_NT];
   UINT1   a_u1EncriptedPwLM[LEN_MS_CHAP_ENC_PW_LM];
   UINT1   a_u1EncriptedPwNT[LEN_MS_CHAP_ENC_PW_NT];
   UINT1   u1_Identifier;
   UINT1   u1_IsUtf8EncodingReqd;                /* When a_u4Utf8UserName is used at 
                                                  * that time this flag is must to be set
                                                  * 1:UTF-8 Encoding is required
                                                  * 0:UTF-8 Encoding isn't required
         */
   UINT1   au1Align[2];
}tMS_CHAP_CPW2;

typedef struct RAD_MS_MPPE_KEYS {
   UINT1 *pu1MppeRecvKey;
   UINT2 u2MppeRecvKeyLen;
   UINT2 u2Pad;
} tRAD_MS_MPPE_KEYS;

typedef struct serviceaaa {
   struct serviceaaa   *p_NextService;              /* Pointer to a next service */
   UINT4   u4_NumericalValue;                      /* The value of the attribute 
                                                     if value is a number */
   UINT4   a_u4Utf8StringVal[LEN_SERVICE_STRING];   /* Used when multilingual characters are available
                                                     * '\0' is must at the end of multilingual service 
                                                     * string
                                                     */ 
   UINT1   a_u1StringValue[LEN_SERVICE_STRING];     /* The value of the attribute
                                                     if value is a string   */
  
   UINT1   u1_ServiceType;                       /* Type of the attribute */
   UINT1   u1_IsUtf8EncodingReqd;                /* When a_u4Utf8UserName is used at
                                                  * that time this flag is must to be set
                                                  * 1:UTF-8 Encoding is required
                                                  * 0:UTF-8 Encoding isn't required
                                                  */
   UINT1   au1Align[2];
}tSERVICES;
typedef    struct radinputauth {
   tUSER_INFO_EAP  *p_UserInfoEAP;            /* Pointer to a structure
                                                 which has info. for EAP user */
   tUSER_INFO_CHAP   *p_UserInfoCHAP;         /* Pointer to a structure
                                                 which has info. for CHAP user */
   tUSER_INFO_PAP   *p_UserInfoPAP;           /* Pointer to a structure which has info for PAP user */
   tUSER_INFO_OTHERS   *p_UserInfoOTHERS;     /* Pointer to a structure which has
                                                 info for other users */
   tUSER_INFO_MS_CHAP   *p_UserInfoMschap;    /* Pointer to a structure has info
                                                   for MS CHAP user */
   tMS_CHAP_CPW1   *p_MsChapChgPw1;           /* Pointer to a strucutr which has
                                                 info to change the user password
                                                 if it has expired (version 1) */
   tMS_CHAP_CPW2   *p_MsChapChgPw2;           /* Pointer to a strucutr which has
                                                 info to change the user password
                                                 if it has expired (version 2) */
   tSERVICES   *p_ServicesAuth;                /* Pointer to a structure which has
                                                   info for services that can be
                                                   hinted to server */
   tOsixTaskId   TaskId;
   UINT4   u4_NasIPAddress;                  /* IP Address of the Network Access*/
   INT4   u4_NasPort;                      /* Network Access Server port */
   INT4   u4_NasPortType;                  /* Network Access Server Port Type */
   UINT1   a_u1NasId[LEN_NAS_ID];              /* Identifier of NAS */
   UINT1  a_u1State[MAX_STATE_LEN];
   UINT1  au1CalledStationId[LEN_USER_NAME];  /* Called Station id attribute*/
   UINT1  au1CallingStationId[LEN_USER_NAME]; /* Calling Station id attribute*/
   UINT1  au1StaMac[MAC_ADDR_LEN];            /* Station mac address */
   UINT1   u1_ProtocolType;                 /* The protocol type used for
                                               authentication of the user. It
                                               takes value like PRO_CHAP, PRO_PAP,
                                               PRO_OTHERS */
   UINT1  u1_StateLen;                         /* State attribute length */
   BOOL1  bCalledStationFlag;                /* Called station id Flag */
   BOOL1  bCallingStationFlag;                /* Calling station id Flag */
   UINT1  au1Pad[3];                          /* Padding */
}tRADIUS_INPUT_AUTH;

typedef struct acctstat {
   UINT4   u4_InputOctets;           /* Total no of bytes received from the user */
   UINT4   u4_OutputOctets;          /* Total no of bytes sent to the user */
   UINT4   u4_SessionTime;           /* Duration of the session */
   UINT4   u4_InputPackets;          /* Total no of packets received from the user */
   UINT4   u4_OutputPackets;         /* Total no of packets sent to user */
   UINT4   u4_InputGigaWords;          /* Total no of packets received from the user */
   UINT4   u4_OutputGigaWords;         /* Total no of packets sent to user */
   UINT4   u4_TerminateCause;        /* The cause of termination of the session */ 
}tACCOUNT_STAT;

typedef struct radinputacc {
   tACCOUNT_STAT   *p_AccountStat;                    /* Accounting statistics to be 
                                                            recorded */
   tSERVICES   *p_ServicesAcc;                        /* Other attributes */
   UINT4   u4_NasIPAddress;                       /* IP Address of the Network Access*/
   UINT4  u4_AuthType;                       
                                                    /* Type of authentication  Server */
   UINT4   u4_AccStatusType;                        /* Accounting Status Type */
                                                    /* Type of authentication  Server */
                                                    
   INT4   u4_NasPort;                             /* Network Access Server port */
   INT4   u4_NasPortType;                       /* Network Access Server Port Type */
   UINT1   a_u1UserName[LEN_USER_NAME];                /* User name */
   UINT1   a_u1SessionId[LEN_SESSION_ID];            /* Session Id */
   UINT1   a_u1NasId[LEN_NAS_ID];                   /* Identifier of NAS */
   UINT1  au1CalledStationId[LEN_USER_NAME];  /* Called Station id attribute*/
   UINT1  au1CallingStationId[LEN_USER_NAME]; /* Calling Station id attribute*/
   BOOL1  bCalledStationFlag;                /* Called station id Flag */
   BOOL1  bCallingStationFlag;                /* Calling station id Flag */
    UINT1  au1Pad[2];
}tRADIUS_INPUT_ACC;

typedef struct serverinfo {
   UINT4   ifIndex;
   struct serverinfo  *p_NextServer; 
   UINT4   u4_ServerType;
   tIPvXAddr ServerAddress;
   UINT4   u4_DestinationPortAuth;
   UINT4   u4_DestinationPortAcc;
   UINT4   u4_ResponseTime;
   UINT4   u4_AccessReqs;
   UINT4   u4_AccessReTxs;
   UINT4   u4_AccessAccepts;
   UINT4   u4_AccessChallenges;
   UINT4   u4_MalAccessResps;
   UINT4   u4_AuthBadAuths;
   UINT4   u4_AccessRejects;
   UINT4   u4_AcctReqs;
   UINT4   u4_AcctReTxs;
   UINT4   u4_AcctResps;
   UINT4   u4_AcctBadAuths;
   UINT4   u4_TimeOuts;
   UINT4   u4_RoundTripTime;
   UINT4   u4_PendingRequests;
   UINT4   u4_UnknownTypes;
   UINT4   u4_PktsDropped;
   UINT4   u4_MalAcctAccessResps;
   UINT1   a_u1Secret[LEN_SECRET];
   UINT1   u1_ServerEnabled;
   UINT1   Status;
   UINT1   u1_MaxRetransmissions;
   BOOL1   b1HostFlag;
   UINT1   au1HostName[DNS_MAX_QUERY_LEN];
   UINT1   au1Pad[1];
}tRADIUS_SERVER;

typedef struct sinterface {
    UINT4    ifIndex;
    tRAD_MS_MPPE_KEYS MsMppeKeys;
    UINT4    u4Bandwidth;
    UINT4    u4DLBandwidth;
    UINT4    u4ULBandwidth;
    UINT4    u4Volume;
    UINT4    u4Timeout;
    UINT1    au1Username[RAD_USER_NAME_SIZE+1];
    UINT1    au1Pad[3];
    UINT1    au1EapPkt[EAP_PKT_DATA_MAX_LENGTH];

    UINT1    Reply_Message[RAD_IF_MEMBER_MAX_SIZE];
    UINT1    Framed_Route[RAD_IF_MEMBER_MAX_SIZE]; 
                    /* This Attribute provides routing information
                     * to be configured for the user on the NAS */
    UINT1    Filter_Id[RAD_IF_MEMBER_MAX_SIZE];   
                    /* This Attribute indicates the name of 
                     * the filter list for this user */
    UINT1    Callback_Number[RAD_IF_MEMBER_MAX_SIZE]; 
    UINT1    State[RAD_IF_MEMBER_MAX_SIZE] ;
    UINT1    Class[RAD_IF_MEMBER_MAX_SIZE];
    UINT1    Callback_ID[RAD_IF_MEMBER_MAX_SIZE]; 
                    /* This Attribute indicates the name of a 
                     *place to be called, to be interpreted by the NAS */
    UINT4    Access;
    UINT4    Utf8ReplyMsg[RAD_IF_MEMBER_MAX_SIZE];
                    /* Used to store decoded multilingual characters of Reply
                     *  Message when it is encoded into UTF8 at transmit time */
    UINT4    Utf8Framed_Route[RAD_IF_MEMBER_MAX_SIZE]; 
                    /* Used to store decoded multilingual characters of Frame 
                    * route when it is encoded into UTF8 at transmit time */

    UINT4    Utf8Filter_Id[RAD_IF_MEMBER_MAX_SIZE]; 
                    /* Used to store decoded multilingual characters of filter Id
                     *  when it is encoded into UTF8 at transmit time */
                    /* access response params */
    UINT4    Service_Type;
    UINT4    Framed_Protocol;
    UINT4    Framed_IP_Address;
    UINT4    Framed_IP_Netmask;
    UINT4    Framed_MTU;
    UINT4    Framed_Compression;
    UINT4    Session_Timeout;
    UINT4    Idle_Timeout;
    UINT4    Termination_Action;
    UINT4    Framed_Routing; /* This Attribute indicates the routing 
                           * method for the user, when the
                              * user is a router to a network
         */
    UINT4    Login_IP_Host;  /* This Attribute indicates the system with 
                           * which to connect the user, when the Login-Service 
         * Attribute is included
                           */
    UINT4    Login_TCP_Port; /* This Attribute indicates the TCP port with 
                           * which the user is to be connected, 
         * when the Login-Service Attribute is also present
                           */
    UINT4    Framed_IPX_Network; /* This Attribute indicates the IPX Network 
                               * number to be configured for the user 
                               */
    UINT4    Login_LAT_Group; /* This Attribute indicates the system with 
                            * which the user is to be connected by LAT
                            */ 
    UINT4    Framed_Appletalk_Link; /* This Attribute indicates the AppleTalk 
                                  * network number which should be used 
          * for the serial link to the user, 
          * which is another AppleTalk router
                                  */ 
    UINT4    Port_Limit; /* This Attribute sets the maximum number 
                          * of ports to be provided to the user by the NAS
                       */
    INT4    i4NasPort;  /* NAS port identifer*/
    tOsixTaskId TaskId;
/* access response for l2tp */
#ifdef L2TP_WANTED
    UINT1    Tunnel_Password[RAD_IF_MEMBER_MAX_SIZE];
    UINT1    Tunnel_Private_Group_ID[RAD_IF_MEMBER_MAX_SIZE];
    UINT1    Tunnel_Client_Auth_ID[RAD_IF_MEMBER_MAX_SIZE];
    UINT1    Tunnel_Server_Auth_ID[RAD_IF_MEMBER_MAX_SIZE];
    UINT4    Tunnel_Type;
    UINT4    Tunnel_Medium_Type;
    UINT4    Tunnel_Client_Endpoint;
    UINT4    Tunnel_Server_Endpoint;
    UINT4    Tunnel_Assignment_ID;
    UINT4    Tunnel_Preference;
    UINT2    Tunnel_Password_Salt;
    UINT1    Tunnel_Type_Tag;
    UINT1    Tunnel_Medium_Type_Tag;
    UINT1    Tunnel_Client_Endpoint_Tag;
    UINT1    Tunnel_Server_Endpoint_Tag;
    UINT1    Tunnel_Password_Tag;
    UINT1    Tunnel_Private_Group_ID_Tag;
    UINT1    Tunnel_Assignment_ID_Tag;
    UINT1    Tunnel_Preference_Tag;
    UINT1    Tunnel_Client_Auth_ID_Tag;
    UINT1    Tunnel_Server_Auth_ID_Tag;
#endif /* for l2tp */
    UINT1    u1StateLen;
    UINT1    u1EAPType;
    UINT1    u1UsernameLen;
    UINT1    RequestId;
    UINT1    au1StaMac[MAC_ADDR_LEN];
    BOOL1    bAccessAcceptRcvd;         /* To denote whether access-accept packet is recieved */
    UINT1    u1Reserved;
}tRadInterface;

/* Radius callback entries */
typedef enum RadCallBackEvents
{
    RADIUS_READ_SECRET_FROM_NVRAM =1,
    RADIUS_WRITE_SECRET_TO_NVRAM,
    RAD_ACCESS_SECURE_MEMORY,
    RADIUS_MAX_CALLBACK_EVENTS
}eRadCallBackEvents;

typedef struct Rad2Byte{
    UINT1    u1Key[RAD_MAX_U2_VAR_SIZE]; 
}tRad2Byte;
typedef struct RadKey{
    UINT1    u1Key[RAD_KEY_SIZE];
}tRadKey;



typedef INT4 (*tRadHandleRadAuthCB) (tRADIUS_INPUT_AUTH * pRadiusInputAuth,
                              tRADIUS_SERVER * pRadiusServerAuth,
                              VOID (*CallBackfPtr) (VOID *),
                               UINT4 u4IfIndex, UINT1 u1ReqId);

typedef VOID (*tRadHandleRadResCB) (VOID *pRadRespRcvd);




INT4 RadDeleteSharedSecretKey PROTO ((VOID));
INT4 radiusAuthentication(tRADIUS_INPUT_AUTH  *, tRADIUS_SERVER *, void (*CallBackfPtr)(void *), UINT4, UINT1 );

INT4 radiusAccounting(tRADIUS_INPUT_ACC *, tRADIUS_SERVER *, void (*CallBackfPtr)(void *), UINT4 );

VOID RadInitInterface( tRadInterface *pInterface );
VOID
radiusProxyMakeResAuth (UINT1 *p_u1RadiusReceivedPacket, 
                        UINT1 a_u1ResponseAuth[]);
VOID
radiusMakeProxyStringVal (UINT1 *p_u1RadiusReceivedPacket,
                          UINT1 a_u1Concatenated[], INT4 *i4_Length);

INT4 RadiusStart (VOID);
#define RAD_SUCCESS 0
#define RAD_FAILURE -1

VOID RadiusMain (INT1 *);

INT4 RadiusLock (VOID);
INT4 RadiusUnLock (VOID);
INT4 RadUtilReadSecretFromNVRAM (VOID);
INT4 RadUtilRegCallBackforRADModule (UINT4 , tFsCbInfo * );
INT4 RadApiRestoreSharedSecretFromNvRam (VOID);


tRadHandleRadAuthCB
RadApiHandleRadAuthCB  (tRADIUS_INPUT_AUTH * pRadiusInputAuth,
                        tRADIUS_SERVER * pRadiusServerAuth,
                        UINT4 u4IfIndex, UINT1 u1ReqId, INT4 *pi4Ret);

VOID RadApiRegisterHandleRadAuthResCB (tRadHandleRadAuthCB  RadAuthCB, tRadHandleRadResCB RadResCB);

VOID RadApiDeRegisterHandleRadAuthResCB (VOID);


UINT4 GetNewRadServerIndex PROTO ((VOID));
UINT4 RadGetServerIndex PROTO ((tIPvXAddr * pRadServerAddr));
UINT4 RadGetServerHostIndex PROTO ((UINT1 *));
INT4 RadUtilWriteSecretToNVRAM (VOID);
#endif /* _RADIUS_H */
