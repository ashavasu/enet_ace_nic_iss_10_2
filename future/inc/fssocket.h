/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: fssocket.h,v 1.39 2015/06/26 02:41:55 siva Exp $
 *
 * Description: This file contains the inclusion of system
 *              header files and mappings of  socket calls
 *              for the respective Operating Systems.
 *
 *******************************************************************/
#ifndef _SOCKET_H
#define _SOCKET_H

#if __GLIBC__ >= 2 && __GLIBC_MINOR__ >= 9
/* Inclusion of System Files and Socket Call Mappings for Linux OS */
#if defined (LNXIP4_WANTED) || defined (LNXIP6_WANTED)
#include <linux/if_tun.h>
#include <linux/if_vlan.h>
#include <errno.h>
#include <linux/socket.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/icmp6.h>
#include <sys/param.h>
#include <net/route.h>
#include <sys/ioctl.h>
#include <linux/in_route.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <net/if.h>
#include <linux/sockios.h>
#include <arpa/inet.h>
#include <sys/times.h>
#include <netinet/ip6.h>
#include <rpc/types.h>
#include <linux/mroute6.h>
#include <net/if_arp.h>
#include <net/if.h>
#include <fcntl.h>
#include <linux/mroute.h>
#if __GLIBC__ >= 2 && __GLIBC_MINOR__ >= 1
#include <netpacket/packet.h>
#include <net/ethernet.h>     /* the L2 protocols */
#else
#include <asm/types.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>   /* The L2 protocols */
#endif
#include <linux/unistd.h>
#include <linux/types.h>
#include <sys/sysctl.h>
#include <unistd.h>
#endif

#else

/* Inclusion of System Files and Socket Call Mappings for Linux OS */
#if defined (LNXIP4_WANTED) || defined (LNXIP6_WANTED)
#include <linux/if_tun.h>
#include <linux/if_vlan.h>
#include <errno.h>
#include <linux/socket.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/select.h>
#include <sys/param.h>
#include <net/route.h>
#include <sys/ioctl.h>
#include <linux/in_route.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <net/if.h>
#include <linux/sockios.h>
#include <arpa/inet.h>
#include <sys/times.h>
#include <rpc/types.h>
#include <net/if_arp.h>
#include <net/if.h>
#include <fcntl.h>

#if __GLIBC__ >= 2 && __GLIBC_MINOR__ >= 1
#include <netpacket/packet.h>
#include <net/ethernet.h>     /* the L2 protocols */
#else
#include <asm/types.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>   /* The L2 protocols */
#endif
#include <linux/unistd.h>
#include <sys/sysctl.h>
#include <unistd.h>
#endif

#endif

#ifdef BSDCOMP_SLI_WANTED
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <errno.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <net/if.h>
#include "selutil.h"
#else
  #ifdef TCP_WANTED
  #include "tcp.h"
  #endif
#include "utlbit.h"
#include "sli.h"
#endif

#ifdef VRF_WANTED
#ifdef LNXIP4_WANTED
#include <linux/sched.h>
#endif
#endif

#ifdef CLI_LNXIP_WANTED
#define    SSH_DEFAULT_PORT        6022 /* SSH Server listening port */
#define    CLI_TELNET_SERVER_PORT  ISS_TGT_TELNET_SERVER_PORT /* Telent Server listening port*/
#define    CLI_TELNET_SERVER_PORT1 ISS_TGT_TELNET_SERVER_PORT1 /* if telnet port is already in use,
                                            use this */
#define    SSH_PORT_2              6025 /* if ssh port 22 is already in use,
                                            use this */
#else
#define    SSH_DEFAULT_PORT        22  /* SSH Server listening port */
#define    CLI_TELNET_SERVER_PORT  23  /* Telent Server listining port*/

/* Following are the usage of TCP local ports */
#define    CLI_TELNET_SERVER_PORT1 6023 /* if telnet port 23 is already in use,
                                            use this */
#define    SSH_PORT_2              6024 /* if ssh port 22 is already in use, 
                                            use this */
#endif /* CLI_LNXIP_WANTED */

/* Prototypes of Future Socket Wrapper Function Calls for FS Stack */
INT4 SocketWrapper
        (
          INT4, 
          INT4, 
          INT4
        );
 
INT4 FcntlWrapper
        (
          INT4, 
          INT4, 
          INT4
        );

INT4 BindWrapper
        (
          INT4, 
          struct sockaddr *, 
          INT4
        );

INT4 RecvWrapper
        (
          INT4, 
          VOID *,
          INT4, 
          UINT4
        );

INT4 RecvfromWrapper
        (
          INT4, 
          VOID *,
          INT4,
          UINT4, 
          struct sockaddr *,
          INT4 *
        );

INT4 SetsockoptWrapper
        (
          INT4,
          INT4,
          INT4,
          VOID *,
          INT4
        );

INT4 SendmsgWrapper
        (
          INT4, 
          const struct msghdr *, 
          UINT4
        );

INT4 RecvmsgWrapper
        (
          INT4, 
          struct msghdr *,
          UINT4
        );

INT4 SendtoWrapper
        (
          INT4, 
          CONST VOID *,
          INT4,
          UINT4, 
          struct sockaddr *,
          INT4
        );

INT4 ConnectWrapper
        (
          INT4, 
          struct sockaddr *, 
          INT4
        );

/* Remap types suitably.
 * In general we try to map standard types to * FS types (as in fd_set below).
 *
 * Applications are written using standard types.
 * tInPktinfo and tIn6Pktinfo are exceptions.
 */

#ifdef BSDCOMP_SLI_WANTED
#define TCP_SET_NODELAY(a,b,c,d,e) setsockopt (a,b,c,d,e)
#define MSG_PUSH_FLAG 0
#define tInPktinfo struct in_pktinfo
#define tIn6Pktinfo struct in6_pktinfo
#else
#define fd_set SliFdSet
#define  SelAddFd(a,b)           SelAddFsFd(a,b) 
#define  SelRemoveFd(a)          SelRemoveFsFd(a)

#define  SelAddWrFd(a,b)         SelAddFsWrFd(a,b)
#define  SelRemoveWrFd(a)        SelRemoveFsWrFd(a)
/* TCP_NODELAY is a FLAG used to flush the input at the sockets
 * without buffering, which is enabled by setsockopt() supported in Linux.
 * MSG_PUSH is an alternative to the above flag supported by FUTURE_SLI,
 * which is enabled by send().
 */
#define TCP_SET_NODELAY(a,b,c,d,e) UNUSED_PARAM(d)
#define MSG_PUSH_FLAG MSG_PUSH

#endif
INT4 SelectWrapper
        (
          INT4, 
          fd_set *, 
          fd_set *, 
          fd_set *, 
          struct timeval*
        );

INT4 CloseWrapper
        (
          INT4
        );

INT4 WriteWrapper
        (
          INT4,
          CONST VOID *,
          INT4
        );

INT4 ReadWrapper
        (
          INT4,
          VOID *,
          INT4
        );

INT4 SendWrapper
        (
          INT4,
          CONST VOID *,
          INT4,
          UINT4
        );

INT4 GetpeernameWrapper
        (
          INT4,
          struct sockaddr *,
          INT4 *
        );

INT4 GetsockoptWrapper
        (
          INT4,
          INT4,
          INT4,
          VOID *,
          INT4 *
        );

INT4 GetsocknameWrapper
        (
          INT4,
          struct sockaddr *,
          INT4 *
        );
        
INT4 InetPtonWrapper
        (
   INT4,
   const CHR1 *,
   VOID *
 );

const UINT1 * InetNtopWrapper
       (
   INT4 ,
   const void *,
          char *,
          INT4
       );

INT4 ListenWrapper
        (
          INT4,
          INT4
        );

INT4 AcceptWrapper
        (
          INT4, 
          struct sockaddr *, 
          INT4 *
        );
 
INT4 SelAddFsFd 
        (
   INT4, 
          VOID (*pCallBk)(INT4) 
 );  
INT4  SelRemoveFsFd
        (
   INT4
 );
INT4 SelAddFsWrFd 
        (
   INT4, 
     VOID (*pCallBk)(INT4)
 );  
INT4  SelRemoveFsWrFd
        (
   INT4
 );

INT4 ShutdownWrapper
        (
          INT4,
          INT4
        );


/* Standard Socket Call - Future Socket Wrapper Mapping Definition */
#ifndef IS_SLI_WRAPPER_MODULE
#define  socket(x,y,z)           SocketWrapper(x,y,z)          
#define  bind(x,y,z)             BindWrapper(x,y,z)            
#define  connect(x,y,z)          ConnectWrapper(x,y,z)         
#define  listen(x,y)             ListenWrapper(x,y)            
#define  read(x,y,z )            ReadWrapper(x,y,z)            
#define  accept(x,y,z )          AcceptWrapper(x,y,(INT4 *)(z))          
#define  recv(w,x,y,z)           RecvWrapper(w,x,y,z)          
#define  recvfrom(a,b,c,d,e,f)   RecvfromWrapper(a,b,c,d,e,(INT4 *)(f) ) 
#define  recvmsg(a,b,c)          RecvmsgWrapper(a,b,c)         
#define  write(a,b,c)            WriteWrapper(a,b,c)           
#define  send(a,b,c,d)           SendWrapper(a,b,c,d)          
#define  sendto(a,b,c,d,e,f)     SendtoWrapper(a,b,c,d,e,f)    
#define  sendmsg(a,b,c)          SendmsgWrapper(a,b,c)         
#define  close(a)                CloseWrapper(a)               
#define  getsockname(a,b,c)      GetsocknameWrapper(a,b,(INT4 *)(c))     
#define  getpeername(a,b,c)      GetpeernameWrapper(a,b,(INT4 *)(c))     
#define  getsockopt(a,b,c,d,e)   GetsockoptWrapper(a,b,c,d,(INT4 *)(e))  
#define  setsockopt(a,b,c,d,e)   SetsockoptWrapper(a,b,c,d,(INT4)(e))  
#define  select(a,b,c,d,e)       SelectWrapper(a,b,c,d,e)      
#define  fcntl(a,b,c)            FcntlWrapper(a,b,c)
#define  inet_pton(a,b,c)        InetPtonWrapper(a,b,c)
#define  inet_ntop(a,b,c,d)      InetNtopWrapper(a,b,c,d)
#define  shutdown(a,b)           ShutdownWrapper(a,b)
#endif /* IS_SLI_WRAPPER_MODULE */

 
/* FutureSLI Select Call Specific Definiton Required for FS  Stack */
#ifndef BSDCOMP_SLI_WANTED
#undef   FD_ZERO
#undef   FD_SET
#undef   FD_CLR
#undef   FD_ISSET
#undef   F_SETFL
#undef   F_GETFL
#undef   O_NONBLOCK
#undef   SHUT_RD
#undef   SHUT_WR
#undef   SHUT_RDWR
#undef   EWOULDBLOCK
#undef   ENOTCONN 
#undef   EINPROGRESS
#undef   EALREADY
#undef   EISCONN 
#undef   EINTR 
#undef   ENOTSOCK
#undef   ENOBUFS
#undef   EAGAIN

#define  FD_ZERO(x)              SLI_FD_ZERO(x)
#define  FD_SET(x,y)             SLI_FD_SET(x,y)
#define  FD_CLR(x,y)             SLI_FD_CLR(x,y)
#define  FD_ISSET(x,y)           SLI_FD_ISSET(x,y)
#define  F_SETFL                 SLI_F_SETFL
#define  F_GETFL                 SLI_F_GETFL
#define  O_NONBLOCK              SLI_O_NONBLOCK
#define  SHUT_RD                 SHUTDOWN_RX
#define  SHUT_WR                 SHUTDOWN_TX
#define  SHUT_RDWR               SHUTDOWN_ALL
#define  EWOULDBLOCK             SLI_EWOULDBLOCK
#define  EINPROGRESS             SLI_EINPROGRESS
#define  EALREADY                SLI_EALREADY
#define  EISCONN                 SLI_EISCONN
#define  EINTR                   SLI_EINTR
#define  ENOTSOCK                SLI_ENOTSOCK
#define  ENOBUFS                 SLI_ENOBUFS
#define  ENOTCONN                SLI_ENOTCONN
#define  EAGAIN                  SLI_EAGAIN
#endif

/* Ignore 0,1,2 because to avoid stdin, stdout, and stderr */
#define SLI_FIRST_SOCKET_FD      3

#endif /* _SOCKET_H */
