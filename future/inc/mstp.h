/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mstp.h,v 1.27 2013/03/06 12:23:28 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              by  external modules.
 *
 *******************************************************************/
#ifndef _MSTP_H
#define _MSTP_H
#include "l2iwf.h"
#include "rstp.h"

/*********************************************
 *         definiton of Macros               *
 *********************************************/   

#define MST_SUCCESS                    0
#define MST_FAILURE                    1

#define MST_ENABLED                    1
#define MST_DISABLED                   2

#define MST_CIST_CONTEXT               0
#define AST_TE_MSTID                   4094
/* The Macro is used in Web Module also*/
#define MST_PORT_ROLE_MASTER     5

#define MST_SNMP_START                 1
#define MST_SNMP_SHUTDOWN              2

#define MST_UPDATE_INST_VLAN_MAP_MSG   1 
#define MST_POST_INST_VLAN_MAP_MSG     2

#define MST_MAP_INST_VLAN_MSG          1 
#define MST_UNMAP_INST_VLAN_MSG        2
#define MST_MAP_INST_VLAN_LIST_MSG     3
#define MST_UNMAP_INST_VLAN_LIST_MSG   4

#define MST_INST_VLAN_MAP_MSG_UPDATED  1 
#define MST_INST_VLAN_MAP_MSG_POSTED   2

#define MST_CONFIG_NAME_LEN               32

#define AST_MAX_SISP_INTF          CFA_MAX_SISP_INTERFACES

#define AST_MIN_SISP_IF_INDEX         CFA_MIN_SISP_IF_INDEX
#define AST_MAX_SISP_IF_INDEX         CFA_MAX_SISP_IF_INDEX

typedef tVlanId tMstVlanId;

/*********************************************
 *   Prototypes for exported functions       *
 *********************************************/   
INT4  MstMiDeleteAllVlans     (UINT4 u4ContextId);

INT4 MstMiVlanCreateIndication (UINT4 u4ContextId, tVlanId VlanId);

INT4 MstMiVlanDeleteIndication (UINT4 u4ContextId, tVlanId VlanId);

INT4  MstGetEnabledStatus        ARG_LIST((VOID));

INT4 MstModuleShutdown (VOID);
INT4 AstIsMstStartedInContext (UINT4 u4ContextId);
INT4 AstIsMstEnabledInContext (UINT4 u4ContextId);

INT4
MstpMapVidToMstId (UINT4 u4ContextId, tVlanId VlanId, UINT2 u2MstId);

INT4 MstUpdateVlanPortList (UINT4 u4ContextId, tVlanId VlanId,
                            tLocalPortList  AddedPorts,
                            tLocalPortList  DeletedPorts);

INT4 MstSispValidateInstRestriction (UINT4 u4ContextId, UINT4 u4IfIndex,
                                     tVlanId VlanId);

INT4 MstUpdateSispStatusOnPort (UINT4 u4IfIndex, UINT4 u4ContextId,
                                tAstBoolean bStatus);
#endif /* _MSTP_H */
