/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: ospfnpwr.h,v 1.2 2018/02/02 09:47:32 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              for OSPF wrappers
 *              
 ********************************************************************/
#ifndef __OSPF_NP_WR_H__
#define __OSPF_NP_WR_H__

UINT1 OspfFsNpOspfInit PROTO ((VOID));
UINT1 OspfFsNpOspfDeInit PROTO ((VOID));
#ifdef MBSM_WANTED
UINT1 OspfFsNpMbsmOspfInit PROTO ((tMbsmSlotInfo * pSlotInfo));
#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
UINT1 OspfFsNpCfaModifyFilter PROTO ((UINT1 u1Action));
#endif /* NPAPI_WANTED */

#endif /* MBSM_WANTED */

#endif /* __OSPF_NP_WR_H__ */
