/********************************************************************
* Copyright (C) Future Software Limited,2006
*
* $Id: ecfm.h,v 1.65 2016/02/02 12:53:20 siva Exp $
*
* Description: This file contains exported definitions and functions
*              of ECFM module.
*********************************************************************/

#ifndef _ECFM_H
#define _ECFM_H

#include "l2iwf.h"
#include "rmgr.h"
#include "cli.h"
#include "ip.h"
#include "ipv6.h"
#include "vcm.h"
#include "mplsapi.h"
#include "r2544.h"
#include "y1564.h"

/* Trace type  definitions */
#define   ECFM_INIT_SHUT_TRC      INIT_SHUT_TRC
#define   ECFM_MGMT_TRC           MGMT_TRC
#define   ECFM_DATA_PATH_TRC      DATA_PATH_TRC
#define   ECFM_CONTROL_PLANE_TRC  CONTROL_PLANE_TRC
#define   ECFM_DUMP_TRC           DUMP_TRC
#define   ECFM_OS_RESOURCE_TRC    OS_RESOURCE_TRC
#define   ECFM_BUFFER_TRC         BUFFER_TRC
#define   ECFM_ALL_FAILURE_TRC    ALL_FAILURE_TRC

#define  ECFM_FN_ENTRY_TRC      (0x1 << 16)
#define  ECFM_FN_EXIT_TRC       (0x1 << 17)
#define  ECFM_CRITICAL_TRC      (0x1 << 18)
#define  ECFM_ALL_TRC          (ECFM_FN_ENTRY_TRC    |\
                                ECFM_FN_EXIT_TRC     |\
                                ECFM_CRITICAL_TRC    |\
                                ECFM_INIT_SHUT_TRC   |\
                                ECFM_MGMT_TRC        |\
                                ECFM_DATA_PATH_TRC   |\
                                ECFM_CONTROL_PLANE_TRC|\
                                ECFM_DUMP_TRC        |\
                                ECFM_OS_RESOURCE_TRC |\
                                ECFM_ALL_FAILURE_TRC |\
                                ECFM_BUFFER_TRC)

#define ECFM_SUCCESS OSIX_SUCCESS
#define ECFM_FAILURE OSIX_FAILURE

#define ECFM_SNMP_TRUE         1
#define ECFM_SNMP_FALSE        2

/* MA Selector Types */
#define ECFM_SERVICE_SELECTION_VLAN         1
#define ECFM_SERVICE_SELECTION_ISID         2
#define ECFM_SERVICE_SELECTION_LSP          3  
#define ECFM_SERVICE_SELECTION_PW           4 

/* Constants for Trap Mask*/
#define ECFM_RDI_CCM_TRAP                   0x40
#define ECFM_MAC_STATUS_TRAP                0x20  
#define ECFM_REMOTE_CCM_TRAP                0x10
#define ECFM_ERR_CCM_TRAP                   0x08
#define ECFM_XCONN_CCM_TRAP                 0x04
#define ECFM_ALL_TRAP                       0x7c

/* Constants for MHF Creation Criteria*/
#define ECFM_MHF_CRITERIA_NONE               1
#define ECFM_MHF_CRITERIA_DEFAULT            2
#define ECFM_MHF_CRITERIA_EXPLICIT           3
#define ECFM_MHF_CRITERIA_DEFER              4
                                                                                                                             
/* MEP ID Validation constants. This macros are needed by 
 * ERPS & ECFM module so it is placed here */
#define ECFM_MEPID_MAX                      8191 
#define ECFM_MEPID_MIN                      1
/* Constants for Sender-Id Permission*/
#define ECFM_SENDER_ID_NONE                  1
#define ECFM_SENDER_ID_CHASSIS               2
#define ECFM_SENDER_ID_MANAGE                3
#define ECFM_SENDER_ID_CHASSID_MANAGE        4
#define ECFM_SENDER_ID_DEFER                 5

/*Type value to be illed in the Eth hdr*/
#define ECFM_PDU_TYPE_CFM     0x8902

#define ECFM_MAX_PDU_SIZE       1522  /* Maximum size of the different CFM PDU 
                                       */
#define ECFM_CC_LOCK()  EcfmCcLock()
#define ECFM_CC_UNLOCK()EcfmCcUnLock()
#define ECFM_LBLT_LOCK() EcfmLbLtLock()
#define ECFM_LBLT_UNLOCK()EcfmLbLtUnLock()

#define ECFM_MIN_SISP_INDEX   CFA_MIN_SISP_IF_INDEX
#define ECFM_MAX_SISP_INDEX   CFA_MAX_SISP_IF_INDEX

/* Event IDs for providing the notifications */
#define ECFM_FRAME_LOSS_EXCEEDED_THRESHOLD            0x00000001   
#define ECFM_FRAME_DELAY_EXCEEDED_THRESHOLD           0x00000002   
#define ECFM_DEFECT_CONDITION_ENCOUNTERED             0x00000004   
#define ECFM_CHECKSUM_COMPARISON_FAILED               0x00000008   
#define ECFM_AIS_CONDITION_ENCOUNTERED                0x00000010   
#define ECFM_AIS_CONDITION_CLEARED                    0x00000020 
#define ECFM_LCK_CONDITION_ENCOUNTERED                0x00000040   
#define ECFM_LCK_CONDITION_CLEARED                    0x00000080  
#define ECFM_RDI_CONDITION_ENCOUNTERED                0x00000100 
#define ECFM_RDI_CONDITION_CLEARED                    0x00000200
#define ECFM_MCC_FRAME_RECEIVED                       0x00000400
#define ECFM_EXM_FRAME_RECEIVED                       0x00000800
#define ECFM_EXR_FRAME_RECEIVED                       0x00001000
#define ECFM_VSM_FRAME_RECEIVED                       0x00002000
#define ECFM_VSR_FRAME_RECEIVED                       0x00004000
#define ECFM_APS_FRAME_RECEIVED                       0x00008000
#define ECFM_DEFECT_CONDITION_CLEARED                 0x00010000
#define ECFM_RAPS_FRAME_RECEIVED                      0x00020000


/* Event IDs from VOE */
#define ECFM_HW_CCM_PERIOD_BIT                        (1<<7)
#define ECFM_HW_CCM_PRIORITY_BIT                      (1<<6)
#define ECFM_HW_CCM_ZERO_PERIOD_BIT                   (1<<5)
#define ECFM_HW_CCM_RX_RDI_BIT                        (1<<4)
#define ECFM_HW_CCM_LOC_BIT                           (1<<3)
#define ECFM_HW_CCM_MEP_ID_BIT                        (1<<2)
#define ECFM_HW_CCM_MEG_ID_BIT                        (1<<1)
#define ECFM_HW_CCM_MEG_LEVEL_BIT                     (1<<0)
#define ECFM_HW_ALL_EVENT_BITS                        (ECFM_HW_CCM_PERIOD_BIT | \
                                                       ECFM_HW_CCM_PRIORITY_BIT | \
                                                       ECFM_HW_CCM_ZERO_PERIOD_BIT | \
                                                       ECFM_HW_CCM_RX_RDI_BIT | \
                                                       ECFM_HW_CCM_LOC_BIT | \
                                                       ECFM_HW_CCM_MEP_ID_BIT | \
                                                       ECFM_HW_CCM_MEG_ID_BIT | \
                                                       ECFM_HW_CCM_MEG_LEVEL_BIT)
/*Interrupt Queue overflow Handling*/
#define ECFM_RXHANDLES_PER_SLOT_MAX                    100
/* Module status*/
typedef enum
{
    ECFM_ENABLE  = 1,
    ECFM_DISABLE = 2
}eEcfmEnable;

typedef enum
{
    ECFM_CCM_OFFLOAD_ENABLE  = 1,
    ECFM_CCM_OFFLOAD_DISABLE = 2
}eEcfmCcmOffEnable;

typedef enum
{
    ECFM_MPLSTP_AIS_OFF_ENABLE  = 1,
    ECFM_MPLSTP_AIS_OFF_DISABLE = 2
}eEcfmMplsAisoffEnable;
/* Vlan Egress or Ingress Ether Type */
typedef enum
{
   ECFM_VLAN_INGRESS_ETHER_TYPE = 1,
   ECFM_VLAN_EGRESS_ETHER_TYPE = 2
}eEcfmVlanEgressType;

enum
{
   ECFM_UPDATE_CHASSISID = 1,
   ECFM_UPDATE_PORTID
};
/* Registration of Applications with ECFM */
/* Application Identifiers used by ECFM */

enum
{
   ECFM_RESERVE_APP_ID = 0, /* No application is given this application ID */
   ECFM_ELPS_APP_ID, 
   ECFM_ERPS_APP_ID,
   ECFM_Y1564_APP_ID,
   ECFM_R2544_APP_ID,
   ECFM_MAX_APPS            /* Add new app IDs before this */
};
typedef enum
{
   ECFM_START = 1,
   ECFM_SHUTDOWN
}eEcfmStatusControl;

/* Types of cli session of ECFM which will block the CLI task till the
 * transaction is fully executed.*/
enum
{
   ECFM_CLI_SESSION_LBM = 0,
   ECFM_CLI_SESSION_LTM,
   ECFM_CLI_SESSION_TST,
   ECFM_CLI_SESSION_TH
};

enum
{
   ECFM_MEP_INDEX_TYPE_INVALID = 0,
   ECFM_MEP_INDEX_TYPE_1,
   ECFM_MEP_INDEX_TYPE_2
};


typedef tOsixSysTime  tEcfmSysTime;
typedef tCRU_BUF_CHAIN_HEADER     tEcfmBufChainHeader;
/* Structure for passing the information to the registered application */
typedef struct
{
    /* Information required to send the following PDUs:
     * 1. MCC PDU 
     * 2. APS PDU
     * 3. EXM PDU
     * 4. EXR PDU
     * 5. VSM PDU
     * 6. VSR PDU
     */
   struct 
   {
       tEcfmSysTime TimeStamp;
       tMacAddr     SrcMacAddr;
       UINT2 u2PortNum;
       UINT4 u4Event;
       /* The Possible Event IDs are:
        * ECFM_FRAME_LOSS_EXCEEDED_THRESHOLD
        * ECFM_FRAME_DELAY_EXCEEDED_THRESHOLD
        * ECFM_DEFECT_OCCURED_AT_MEP
        * ECFM_CHECKSUM_COMPARISON_FAILED
        * ECFM_AIS_CONDITION_ENCOUNTERED
        * ECFM_AIS_CONDITION_CLEARED
        * ECFM_LCK_CONDITION_ENCOUNTERED
        * ECFM_LCK_CONDITION_CLEARED
        * ECFM_RDI_CONDITION_ENCOUNTERED
        * ECFM_RDI_CONDITION_CLEARED
        * ECFM_MCC_FRAME_RECEIVED
        * ECFM_EXM_FRAME_RECEIVED
        * ECFM_EXR_FRAME_RECEIVED
        * ECFM_VSM_FRAME_RECEIVED
        * ECFM_VSR_FRAME_RECEIVED
        * ECFM_APS_FRAME_RECEIVED
        */
       UINT4 u4VarInfo;
       UINT2 u2VlanId;
       UINT1 u1Direction;
       UINT1 u1MdLevel;
   }EventInfo;
   /* Information required to send the following PDUs:
    * 1. MCC PDU 
    * 2. APS PDU
    * 3. EXM PDU
    * 4. EXR PDU
    * 5. VSM PDU
    * 6. VSR PDU
    */
   struct 
   {
        tEcfmBufChainHeader *pu1Data;
        UINT1               *pu1Oui;
        UINT1               u1CfmPduOffset;
        UINT1 u1Flags;
        UINT1 u1TlvOffset;
        UINT1 u1SubOpCode;
    }PduInfo;

    tMacAddr     SrcMacAddr;
    UINT2    u2MepId;
    tEcfmSysTime TimeStamp;

    UINT4    u4ContextId;
    UINT4    u4IfIndex;
    UINT4    u4VlanIdIsid;
    UINT4    u4MdIndex;
    UINT4    u4MaIndex;
    UINT4    u4VarInfo;
    UINT4    u4Event;
    UINT4    u4SlaId; 
               /* Added as part of RFC2544 and Y1564
                * to get the Fault status using
                * SLA Id itself*/
    UINT1    u1MdLevel;
    UINT1    u1Direction;
    /* Commmon Information for all the events */
       /* The Possible Event IDs are:
        * ECFM_FRAME_LOSS_EXCEEDED_THRESHOLD
        * ECFM_FRAME_DELAY_EXCEEDED_THRESHOLD
        * ECFM_DEFECT_CONDITION_ENCOUNTERED
        * ECFM_CHECKSUM_COMPARISON_FAILED
        * ECFM_AIS_CONDITION_ENCOUNTERED
        * ECFM_AIS_CONDITION_CLEARED
        * ECFM_LCK_CONDITION_ENCOUNTERED
        * ECFM_LCK_CONDITION_CLEARED
        * ECFM_RDI_CONDITION_ENCOUNTERED
        * ECFM_RDI_CONDITION_CLEARED
        * ECFM_MCC_FRAME_RECEIVED
        * ECFM_EXM_FRAME_RECEIVED
        * ECFM_EXR_FRAME_RECEIVED
        * ECFM_VSM_FRAME_RECEIVED
        * ECFM_VSR_FRAME_RECEIVED
        * ECFM_APS_FRAME_RECEIVED
        * ECFM_DEFECT_CONDITION_CLEARED
        */
    UINT1    au1Pad[2];
}tEcfmEventNotification;
typedef struct EcfmRegParams
{
   UINT4 u4ModId; /* Module ID(app Id) of the application that 
                   * wants to get registered 
                   */
   UINT4 u4EventsId; /* Bit set for the events with which an 
                      * application wants to be notified
                      */
   UINT4 u4VlanInd; /* Indicates the SF notification mechanism.
         If the value equals
         ECFM_INDICATION_FOR_ALL_VLANS - SF will be provided
                                         to all the vlans that
             are part of this MA
                ECFM_INDICATION_SPECIFIC_VLAN - SF will be provided on
                                         the primary vlan alone
                     */             
   /* The Bits sets are as follows 
    * ECFM_FRAME_LOSS_EXCEEDED_THRESHOLD   00000000000000001
    * ECFM_FRAME_DELAY_EXCEEDED_THRESHOLD  00000000000000010
    * ECFM_DEFECT_CONDITION_ENCOUNTERED   00000000000000100
    * ECFM_CHECKSUM_COMPARISON_FAILED      00000000000001000
    * ECFM_AIS_CONDITION_ENCOUNTERED       00000000000010000
    * ECFM_AIS_CONDITION_CLEARED           00000000000100000
    * ECFM_LCK_CONDITION_ENCOUNTERED       00000000001000000
    * ECFM_LCK_CONDITION_CLEARED           00000000010000000
    * ECFM_RDI_CONDITION_ENCOUNTERED       00000000100000000
    * ECFM_RDI_CONDITION_CLEARED           00000001000000000
    * ECFM_MCC_FRAME_RECEIVED              00000010000000000
    * ECFM_EXM_FRAME_RECEIVED              00000100000000000
    * ECFM_EXR_FRAME_RECEIVED              00001000000000000
    * ECFM_VSM_FRAME_RECEIVED              00010000000000000
    * ECFM_VSR_FRAME_RECEIVED              00100000000000000
    * ECFM_APS_FRAME_RECEIVED              01000000000000000
    * ECFM_DEFECT_CONDITION_CLEARED       10000000000000000
    */
   /* ECFM provides the notification through this function pointer */
   VOID (*pFnRcvPkt) (tEcfmEventNotification *pEcfmEventNotification);

   /* The below pointer to Application Info is provided by the registered 
    * protocols like ERPS to update ECFM specific info by ECFM module in the 
    * information pointed by pAppInfo for ERPS packet tx optimization in HW.
    *
    * Aricent ECFM stack is currently not updating this. It is for integration 
    * with third party stack.
    */
   VOID    *pAppInfo;

}tEcfmRegParams;

enum {
    ECFM_Y1564_REQ_SLA_CONF_TEST_START = 14,
    ECFM_Y1564_REQ_SLA_PERF_TEST_START,
    ECFM_Y1564_REQ_CFM_GET_TEST_REPORT,
    ECFM_Y1564_REQ_CFM_CONF_TEST_STOP_TEST,
    ECFM_Y1564_REQ_CFM_PERF_TEST_STOP_TEST
};

#define    ECFM_R2544_REQ_START_THROUGHPUT_TEST     R2544_REQ_START_THROUGHPUT_TEST
#define    ECFM_R2544_REQ_START_LATENCY_TEST        R2544_REQ_START_LATENCY_TEST
#define    ECFM_R2544_REQ_START_FRAMELOSS_TEST      R2544_REQ_START_FRAMELOSS_TEST
#define    ECFM_R2544_REQ_START_BACKTOBACK_TEST     R2544_REQ_START_BACKTOBACK_TEST
#define    ECFM_R2544_REQ_STOP_BENCHMARK_TEST       R2544_REQ_STOP_BENCHMARK_TEST
#define    ECFM_R2544_REQ_SLA_TEST_RESULT           R2544_REQ_SLA_TEST_RESULT


typedef enum {
    ECFM_DIRECTION_EXTERNAL = 1,
    ECFM_DIRECTION_INTERNAL = 2
}eEcfmDirection;


#define ECFM_Y1564_AND_RFC2544_PAYLOAD_SIZE  16

/* This Structure is the input for the Entry Function
 * (Function to interact with the external modules) */
typedef struct EcfmReqParams
{
    UINT1           au1Alias[VCM_ALIAS_MAX_LEN];
    tMacAddr        TxSrcMacAddr;
    tVlanId         InVlanId;
    tMacAddr        TxDstMacAddr;
    tVlanId         OutVlanId;
    UINT4           u4IfIndex;
    UINT4           u4TagType;
    UINT4           u4FrameSize;
    UINT4           u4MsgType;
    UINT4           u4ContextId;
    UINT4           u4PortSpeed;
    UINT4           u4PortType;
    UINT4           u4IfMtu;
    UINT4           u4YellowFrRate;
    UINT4           u4CIRPps;
    UINT4           u4YellowFrPps;
    UINT2           u2EvcId;
    UINT1           au1Reserved[2];
    struct
    {
        UINT4               u4SlaId;
        UINT4               u4MEGId;
        UINT4               u4MEPId;
        UINT4               u4MEId;
        UINT4               u4TransId;
    }EcfmMepInfo;
    struct
    {
        eEcfmDirection  TrafProfDirection;
        UINT4           u4TrialCount;  
        UINT4           u4Rate;
        UINT4           u4TrafProfDwellTime;
        UINT1           au1TrafProfPayload[ECFM_Y1564_AND_RFC2544_PAYLOAD_SIZE];
        UINT2           u2FrameSize;
        UINT1           u1TrafProfPCP;
        UINT1           u1R2544SubTest;
        UINT1           u1TrafProfSeqNoCheck;
        UINT1           au1Pad[3];
    }TrafficProfileParam;
    union
    {
        struct
        {
            UINT4               u4TestDuration;
            UINT4               u4CIR;
            UINT4               u4EIR;
            UINT2               u2SlaRateStep;
            UINT2               u2SlaCurrentStep;
            UINT1               u1ColorMode;
            UINT1               u1CoupFlag;
            UINT1               u1TestMode;
            UINT1               u1TrafProfInCos;
            UINT1               u1TrafProfOutCos;
            UINT1               au1Pad[3];
        }TestParam;
        union
        {
            struct
            {
                UINT4     u4TrialDuration;
                UINT4     u4TrialCount;
                UINT4     u4Rate;
            }ThroughputParams;
            struct
            {
                UINT4     u4TrialDuration;
                UINT4     u4TrialCount;
                UINT4     u4DelayInterval;
                UINT4     u4Rate;
            }LatencyParams;
            struct
            {
                UINT4     u4TrialDuration;
                UINT4     u4TrialCount;
                UINT4     u4Rate;
            }FrameLossParams;
            struct
            {
                UINT4     u4TrialDuration;
                UINT4     u4TrialCount;
                UINT4     u4Rate; 
            }BackToBackParams;
         }unR2544ReqParams;
     }unReqParams;
#define TestParam  unReqParams.TestParam
#define ThroughputParam unReqParams.unR2544ReqParams.ThroughputParams
#define LatencyParam unReqParams.unR2544ReqParams.LatencyParams
#define FrameLossParam unReqParams.unR2544ReqParams.FrameLossParams
#define BackToBackParam unReqParams.unR2544ReqParams.BackToBackParams
}tEcfmReqParams;

typedef struct EcfmRespParams
{
    UINT1      u1ErrorCode;
    UINT1      au1Pad[3];
}tEcfmRespParams;

typedef struct EcfmExtInParams
{
    UINT4 u4ContextId;
    union
    {
       tR2544ReqParams R2544ReqParam;
       tY1564ReqParams Y1564ReqParam;
       
    }unResultInfo;

#define R2544ReqParams unResultInfo.R2544ReqParam
#define Y1564ReqParams unResultInfo.Y1564ReqParam
}tEcfmExtInParams;

typedef struct EcfmExtOutParams
{
   tR2544RespParams R2544RespParams;
   tY1564RespParams Y1564RespParams;
}tEcfmExtOutParams;


/* Structure for information required to select a MEP.
 * This is used by the modules to call APIs provided by ECFM
 */
typedef struct EcfmMepInfoParams
{
   UINT4 u4ContextId;
   UINT4 u4IfIndex;
   UINT4 u4VlanIdIsid;
   UINT4 u4MdIndex;
   UINT4 u4MaIndex;
   UINT4 u4SlaId;
   /* The below pointer to Application Info is provided by the registered 
    * protocols like ERPS during packet tx for optimized pkt tx in HW.
    *
    * Aricent ECFM stack is currently not using this for packet tx. It is for 
    * integration with third party stack.
    */
   VOID  *pAppInfo;
   UINT2 u2MepId;
   UINT1 u1MdLevel;
   UINT1 u1Direction;
   UINT1 u1Version;  /* Version is added here for filling Version Field in
                      * ECFM header for R-APS packets based on the version 
                      * passed by ERPS to support G.8032 Versions: 1 and 2.
                      */
   UINT1 au1Pad[3];
}tEcfmMepInfoParams;

/* Structure required to configure LBM parameters*/
typedef struct EcfmConfigLbmInfo
{
   tMacAddr  DestMacAddr; /* Target MAC address */
   UINT2     u2TstTlvPatterSize; /* Size of Test Pattern */
   UINT1     *pu1DataTlvData; /* Pointer to Data for Data TLV */
   UINT4     u4DataTlvSize; /* Size of Data TLV */
   UINT4     u4Deadline; /* Duration fof LBM transmission */
   UINT4     u4LbmInterval; /* Interval between successive LBM frames */
   UINT2     u2TxLbmMessages; /* No of LB Messages to be transmitted */
   UINT1     u1Status; /* Status of LBM transmission */
   UINT1     u1TstTlvPatterType; /* Type of Test Pattern */
   UINT1     u1TlvOrNone; /* LBM type to be included in LBM  frame */
   UINT1     u1LbmMode; /* LBM to be initiated in burst/req-response mode */
   BOOL1     b1VariableByte; /* Variable byte in TST PDU */
   BOOL1     b1DropEnable; /* Drop eligibility of LBM frames */
}tEcfmConfigLbmInfo;

/* Structure required to configure TST parameters*/
typedef struct EcfmConfigTstInfo
{
   tMacAddr  DestMacAddr; /* Target MAC address */
   UINT2     u2TstTlvPatterSize; /* Size of Test Pattern */
   UINT4     u4Deadline; /* Duration fof TST transmission */
   UINT4     u4TxTstMessages; /* No of TST Messages to be transmitted */
   UINT4     u4TstInterval; /* Interval between successive TST frames */
   UINT1     u1Status; /* Status of TST transmission */
   UINT1     u1TstTlvPatterType; /* Type of Test Pattern */
   BOOL1     b1VariableByte; /* Variable byte in TST PDU */
   BOOL1     b1DropEnable; /* Drop eligibility of TST frames */
}tEcfmConfigTstInfo;

/* For display of LTR cache for each LTM transaction according to hops */
typedef struct EcfmLtrCacheIndices 
{
   UINT2 u2RcvOrder;
   UINT1 u1Ttl;
   UINT1 u1Pad;
}tEcfmLtrCacheIndices;

/********************** MPLS TP Identifiers ***************************/
/* Tunnel Identifier structure */
typedef struct EcfmMplsLspParams {
    UINT4           u4TunnelId; /* Tunnel identifier. This identifies the 
                                 * tunnel associated with the ME
                                 */ 
    UINT4           u4TunnelInst; /* Instance of the tunnel identifier.*/
   
    UINT4           u4SrcLer;     /* Identifier of the source LER */

    UINT4           u4DstLer;     /* Identifier of the destination LER */
} tEcfmMplsLspParams;

/* MPLS Path identification union (Union of LSP & PW) */
typedef union EcfmMplsPathParams {
    tEcfmMplsLspParams   MplsLspParams; /* LSP Tunnel path identifier */

    UINT4                u4PswId;       /* pwID in pwTable (VC-ID) */
} unEcfmMplsPathParams;

/** MPLS Path Identification parameters *** */
typedef struct EcfmMplsParams {
     unEcfmMplsPathParams  MplsPathParams; /* Union containing the MPLS LSP or
                                            * pseudowire parameters
                                            * */
     UINT1                 u1MplsPathType; /* Determines the type of the path
                                            * either LSP or PW
                                            * */
     UINT1                 au1Pad[3];      /* Array used for padding */
}tEcfmMplsParams;

/** RFC2544 and Y1564 Identification parameters *** */
typedef struct EcfmSlaParams {
    UINT4                 u4SlaId; /* Determines the SLA Id
                                    * */
}tEcfmSlaParams;

/* MPLS path Process information parameters */
typedef struct EcfmMepProcInfo
{
    tEcfmMplsParams    *pMplsParms; /* MPLS-TP Path Identification variable */
    UINT1               u1Priority; /* Priority (e.g. CCM/LBM/LBR priority) to 
                                     * be filled in the EXP field of the MPLS 
                                     * header.
                                     */
    UINT1               u1Ttl;      /* TTL to be filled in MPLS header */ 
    UINT1               au1Pad[2];
} tEcfmMepProcInfo;

typedef struct _EcfmCcOffRxHandleInfo 
{
   UINT2   u2StartRxHandle;
   UINT2   u2Length; 
}tEcfmCcOffRxHandleInfo;


typedef struct _EcfmCcOffRxInfo 
{
    UINT4   u4StartSeqNo;
    UINT4   u4CcmRxTimeout;
    UINT4   u4VlanIdIsid;
    UINT2   u2RMepId;
    UINT1   u1MdLevel;
    UINT1   au1MAID[48];
    UINT1   u1RDI;
}tEcfmCcOffRxInfo;
typedef struct _EcfmCcOffMepTxStats
{
   UINT4 u4CcmTx;
   UINT4 u4LastSeqNum;
   UINT4 u4CcmTxFailure;
}tEcfmCcOffMepTxStats;

typedef struct _EcfmCcOffMepRxStats
{
   UINT4 u4CcmRx;
   UINT4 u4ExpectedSeqNum;
}tEcfmCcOffMepRxStats;

typedef struct _EcfmCcOffPortStats
{
   UINT4    u4NoOfCcTx;
   UINT4    u4NoOfCcRx;
   UINT4    u4NoOfCcFw;
}tEcfmCcOffPortStats;

typedef struct _EcfmMplstpCcOffTxInfo {
    tEcfmMplsParams  *pEcfmMplsParams;    /* Contains the LSP/PW Path 
                                           * information
                                           */
    VOID       *pSlotInfo;                /* Pointer to IfIndex/SlotInfo 
                                           * to identify the slot
                                           */
    VOID       *pBuf;                     /* Pkt that can be used by Offloaded
                                           * module, if required.
                                           */
    UINT4      u4ContextId;               /* Context Identifier */
    UINT4      u4CcInterval;              /* Interval for the periodic 
                                           * transmission of CC Messages
                                           */
    UINT4      u4OutIfIndex;              /* Outgoing Interface Index 
                                           * in the MPLS-TP path only
                                           * if pBuf is used.
                                           */
    UINT2      u2TxFilterHandle;          /* Tx Filter Handle */
    UINT2      u2MepId;                   /* Mep Id*/
    UINT2      u2PduSize;                 /* Pdu Size */
    UINT1      au1NHopMac[MAC_ADDR_LEN];  /* NextHop in MPLS-TP Path */
}tEcfmMplstpCcOffTxInfo;

typedef struct _EcfmMplstpCcOffRxInfo
{
    tEcfmMplsParams  *pEcfmMplsParams; /* Contains the LSP/PW Path */
    VOID    *pSlotInfo;                /* Pointer to IfIndex/SlotInfo 
                                        * to identify the slot
                                        */
    UINT4   u4ContextId;               /* Context Identifier */
    UINT4   u4StartSeqNo;              /* Expeccted Start Sequence Number */
    UINT4   u4CcmRxTimeout;            /* Remote MEP CC interval */
    UINT2   u2RxFilterHandle;          /* Rx Filter Handle */
    UINT2   u2RMepId;                  /* Remote MEP Id */
    UINT1   au1MAID[48];               /* MA ID */
    UINT1   u1MdLevel;                 /* MD Level */
    UINT1   au1Pad[3];                 /* For Word alignment */
}tEcfmMplstpCcOffRxInfo;
/* AIS OFFLOADING */
typedef struct _EcfmMplstpAisOffParams {
UINT4                    u4ContextId;     /* Virtual Context Identifier */
tEcfmMplsParams          EcfmMplsParams;  /* Contains the information
                                           * regarding the path can be
                                           * either LSP or PWMpls.
                                           */
    VOID                 *pSlotInfo;      /* Pointer to IfIndex/SlotInfo 
                                           * to identify the slot.
                                           */
UINT4                    u4Period;        /* AIS Period */
UINT4                    u4AisInterval;   /* Interval for the periodic 
                                           * transmission of AIS
                                           */
UINT1                    u1MdLevel;       /* Level in which the AIS PDU 
                                           * needs to be transmitted 
                                           * or received.
                                           */
UINT1                    u1PhbValue;      /* Value of the Per hop behavior */
BOOL1                    b1AisCondition;  /* TRUE(1)  - indicates AIS Cond Entry
                                           * FALSE(0) - indicates AIS Cond Exit
                                           */
UINT1                    au1Pad[1];       /* Added for Padding */
} tEcfmMplstpAisOffParams;

typedef enum {

    ECFM_INDICATION_FOR_ALL_VLANS = 0,

    ECFM_INDICATION_SPECIFIC_VLAN = 1

}eFaultIndication;

typedef enum
{
    ECFM_HW_EVENT_INIT     = 0,
    ECFM_HW_EVENT_CCM_PERIOD       = ECFM_HW_CCM_PERIOD_BIT,       /* CCM: Interval Value Change Event */
    ECFM_HW_EVENT_CCM_PRIORITY     = ECFM_HW_CCM_PRIORITY_BIT,     /* CCM: Priority Field Value Change */
    ECFM_HW_EVENT_CCM_ZERO_PERIOD  = ECFM_HW_CCM_ZERO_PERIOD_BIT,  /* CCM: Interval Zero Received Event  */
    ECFM_HW_EVENT_CCM_RX_RDI       = ECFM_HW_CCM_RX_RDI_BIT,       /* CCM: RDI Flag Event */
    ECFM_HW_EVENT_CCM_LOC          = ECFM_HW_CCM_LOC_BIT,          /* CCM: Loss Of Continuity Event */
    ECFM_HW_EVENT_CCM_MEP_ID       = ECFM_HW_CCM_MEP_ID_BIT,       /* CCM: Invalid RMEP ID Event */
    ECFM_HW_EVENT_CCM_MEG_ID       = ECFM_HW_CCM_MEG_ID_BIT,       /* CCM: Mismatch in MEG ID Event */
    ECFM_HW_EVENT_MEG_LEVEL        = ECFM_HW_CCM_MEG_LEVEL_BIT     /* CCM: Unexpected MEG Level Event */
}tEcfmHwEvents;

/* The structure containing Slot information used for messages 
 * passed to ecfm */
extern tMbsmSlotInfo gEcfmSlotInfo;

extern tMacAddr gu1CfmMacAddr;
extern tMacAddr gu1CfmTunnelMacAddr;

#ifdef NPAPI_WANTED
extern UINT4 gu4HwCapability;
#endif

typedef void (* mep_event_callback_t)(tEcfmHwEvents  EcfmHwEvents,
                                      UINT1          au1HwHandler[]);
VOID
FsEcfmHwCallBackFromVoe (tEcfmHwEvents EcfmHwEvents,
                         UINT1         au1HwHandler[]);
PUBLIC VOID EcfmCcMain PROTO ((INT1*));
PUBLIC VOID EcfmLbLtMain PROTO ((INT1*));

PUBLIC INT4 EcfmCcLock PROTO ((VOID));
PUBLIC INT4 EcfmCcUnLock PROTO ((VOID));
PUBLIC INT4 EcfmLbLtLock PROTO ((VOID));
PUBLIC INT4 EcfmLbLtUnLock PROTO ((VOID));
PUBLIC INT4 EcfmLbLtCliLock PROTO ((VOID));
PUBLIC INT4 EcfmLbLtCliUnLock PROTO ((VOID));

/* Following APIs should not be used for new porting. These are kept
   for the backward compatibility only */
/* Old APIS -- Start */
PUBLIC INT4 EcfmCcCreatePort PROTO ((UINT4, UINT4, UINT2));
PUBLIC INT4 EcfmCcDeletePort PROTO ((UINT4));
PUBLIC INT4 EcfmCcUpdatePortStatus PROTO ((UINT4, UINT1));
PUBLIC INT4 EcfmLbLtCreatePort PROTO ((UINT4, UINT4,UINT2 ));
PUBLIC INT4 EcfmLbLtDeletePort PROTO ((UINT4 ));
PUBLIC INT4 EcfmLbLtUpdatePortStatus PROTO ((UINT4 , UINT1 ));

PUBLIC INT4 EcfmCcCreateContext PROTO ((UINT4));
PUBLIC INT4 EcfmCcDeleteContext PROTO ((UINT4));
PUBLIC INT4 EcfmCcMapPort PROTO ((UINT4 , UINT4 , UINT2));
PUBLIC INT4 EcfmCcUnMapPort PROTO ((UINT4));

PUBLIC INT4
EcfmCcmOffHandleVoeEvents (tEcfmHwEvents EcfmHwEvents,
                           UINT1         au1HwHandler[]);

PUBLIC INT4 EcfmLbLtCreateContext PROTO ((UINT4));
PUBLIC INT4 EcfmLbLtDeleteContext PROTO ((UINT4));
PUBLIC INT4 EcfmLbLtMapPort PROTO ((UINT4 , UINT4 , UINT2));
PUBLIC INT4 EcfmLbLtUnMapPort PROTO ((UINT4));
PUBLIC INT4 EcfmCcHandleInFrameFromPort PROTO ((tCRU_BUF_CHAIN_HEADER *,
UINT4));
PUBLIC INT4 EcfmLbLtHandleInFrameFromPort PROTO ((tCRU_BUF_CHAIN_HEADER*,
                                                  UINT4));
/* Old APIS -- End */

PUBLIC INT4
Y1731ApiHandleExtRequest (tEcfmReqParams * pEcfmReqParams,
                          tEcfmRespParams * pEcfmRespParams);

PUBLIC VOID EcfmShowTraps PROTO ((tCliHandle CliHandle,UINT1 u1Module));
PUBLIC INT4 EcfmCreatePort PROTO ((UINT4, UINT4,UINT2 ));
PUBLIC INT4 EcfmDeletePort PROTO ((UINT4 ));
PUBLIC INT4 EcfmUpdatePortStatus PROTO ((UINT4 , UINT1 ));

PUBLIC INT4 EcfmCreateContext PROTO ((UINT4));
PUBLIC INT4 EcfmDeleteContext PROTO ((UINT4));
PUBLIC INT4 EcfmMapPort PROTO ((UINT4 , UINT4 , UINT2));
PUBLIC INT4 EcfmUnMapPort PROTO ((UINT4));

PUBLIC INT4 EcfmHandleInFrameFromPort PROTO ((tCRU_BUF_CHAIN_HEADER *,
UINT4));
PUBLIC INT4 EcfmCcAndLbLtStartModule PROTO ((VOID));
PUBLIC INT4 EcfmCcAndLbLtShutDownModule PROTO ((VOID));

PUBLIC INT4 EcfmCcmOffloadCreateVlanIndication PROTO ((UINT4, UINT2));
PUBLIC INT4 EcfmCcmOffloadDeleteVlanIndication PROTO ((UINT4, UINT2));
PUBLIC INT4 EcfmPortStateChangeIndication PROTO ((UINT2, UINT4, UINT1));
PUBLIC INT4 EcfmPbPortStateChangeIndication PROTO ((UINT4, UINT2, UINT1));
PUBLIC INT4 EcfmVlanPortMembershipIndication PROTO ((UINT4,UINT2));
PUBLIC INT4 EcfmVlanEtherTypeChngInd PROTO ((UINT4, UINT4, UINT1, UINT2));
PUBLIC INT4 EcfmMstpEnableIndication PROTO ((UINT4));
PUBLIC INT4 EcfmMstpDisableIndication PROTO ((UINT4));
PUBLIC INT4 EcfmCcmOffHandleTxFailureCallBack PROTO ((UINT4,UINT2));
PUBLIC INT4 EcfmCcmOffHandleRxCallBack PROTO ((UINT4, UINT2, UINT4));
PUBLIC INT4 EcfmCcmHwHandleTxFailureCallBack PROTO ((UINT4, UINT1*));
PUBLIC INT4 EcfmCcmHwHandleRxCallBack PROTO ((UINT4, UINT1 *, UINT4));
PUBLIC INT4 EcfmPortDeiBitChangeIndication PROTO ((UINT4,UINT1));
PUBLIC INT4 EcfmMplstpOffHandleAisCond PROTO 
                        ((tEcfmMplstpAisOffParams *pAisOffParams));
PUBLIC INT4 EcfmMplstpOffSetMepAisStatus PROTO
                        ((tEcfmMplstpAisOffParams *pAisOffParams));
PUBLIC INT1 EcfmGetLckStatus PROTO ((UINT2));
PUBLIC INT4 EcfmSetRdiCapability PROTO ((tEcfmMepInfoParams * ,
                                         UINT1 ,UINT1 ,UINT4));
PUBLIC INT4 EcfmGetLmCounters PROTO ((tEcfmMepInfoParams *, 
                                      UINT4 *, UINT4 *, UINT4 *));
PUBLIC INT4 EcfmConfigLm PROTO((tEcfmMepInfoParams *,tMacAddr,UINT1,
                                UINT2,UINT4));
PUBLIC INT4 EcfmSetLmThreshold PROTO ((tEcfmMepInfoParams *,
                                       UINT4,UINT4));
PUBLIC INT4 EcfmSetAisCapability PROTO ((tEcfmMepInfoParams *,
                                         UINT1 ,UINT1 ,UINT4));
PUBLIC INT4 EcfmSetOutOfService PROTO ((tEcfmMepInfoParams *,
                                        BOOL1 ,UINT1 ,UINT4 ));
PUBLIC INT4 EcfmInitiateExPdu PROTO ((tEcfmMepInfoParams *,UINT1 ,
                                      UINT1 ,UINT1 * ,UINT4,tMacAddr,UINT1));
PUBLIC INT4 EcfmConfigDm PROTO ((tEcfmMepInfoParams *,UINT1,
                                 tMacAddr ,UINT1 ,UINT2 ,UINT4 ,UINT4 ,UINT1));
PUBLIC INT4 EcfmSetDmThreshold PROTO ((tEcfmMepInfoParams *,UINT4));
PUBLIC INT4 EcfmConfigTst PROTO ((tEcfmMepInfoParams *,tEcfmConfigTstInfo *));
PUBLIC INT4 EcfmConfigLbm PROTO ((tEcfmMepInfoParams *,tEcfmConfigLbmInfo *));
PUBLIC INT4 EcfmUpdateLocalSysInfo PROTO ((UINT1, UINT4, UINT1, UINT1 *, UINT2));
/* Callback for IPv4 Module */
PUBLIC VOID EcfmNotifyIpv4IfStatusChange PROTO ((tNetIpv4IfInfo *, UINT4));
/* Callback for IPv6 Module */
PUBLIC VOID EcfmNotifyIpv6IfStatusChange PROTO ((tNetIpv6HliParams *));
PUBLIC INT1 EcfmGetMepConfigPrompt PROTO ((INT1 *,INT1*));
PUBLIC INT1 EcfmTpOamGetMepConfigPrompt PROTO ((INT1 *,INT1*));

PUBLIC INT4 EcfmRegisterProtocols PROTO ((tEcfmRegParams *, UINT4 ));
PUBLIC INT4 EcfmDeRegisterProtocols PROTO ((UINT4,UINT4));
PUBLIC INT4
EcfmMepRegisterAndGetFltStatus (tEcfmRegParams * pEcfmReg,
                                tEcfmEventNotification * pMepInfo);
PUBLIC INT4
EcfmMepDeRegister (tEcfmRegParams * pEcfmReg, tEcfmEventNotification * pMepInfo);
PUBLIC INT4 EcfmConfigureCliSession PROTO ((UINT4, UINT4,UINT4,UINT2, tCliHandle, UINT1));

PUBLIC INT1 EcfmGetDomainConfigPrompt PROTO ((INT1 *,INT1*));
PUBLIC INT1 EcfmTpOamGetDomainConfigPrompt PROTO ((INT1 *,INT1*));
#ifdef L2RED_WANTED
INT4 EcfmRedRcvPktFromRm PROTO ((UINT1, tRmMsg *, UINT2));
INT4 EcfmRestartModule PROTO ((VOID));
#endif
PUBLIC INT4 EcfmCcmOffloadCreateIsidIndication PROTO ((UINT4, UINT4));
PUBLIC INT4 EcfmCcmOffloadDeleteIsidIndication PROTO ((UINT4, UINT4));
PUBLIC BOOL1 EcfmMiIsVlanIsidMemberPort PROTO ((UINT4, UINT4, UINT2));
PUBLIC BOOL1 EcfmPbbMiIsIsidActive PROTO ((UINT4, UINT4));
PUBLIC INT4 EcfmPbbGetFwdPortList PROTO ((UINT4, UINT2, tMacAddr, tMacAddr, UINT4, tLocalPortList));
PUBLIC INT4 EcfmGetFwdPortList PROTO ((UINT4, UINT2, tMacAddr, tMacAddr, UINT4, tLocalPortList));
PUBLIC UINT1 EcfmPbbGetIsidPortState PROTO ((UINT4, UINT2));
PUBLIC UINT1 EcfmGetPortState PROTO ((UINT4, UINT2));
PUBLIC INT4 EcfmPbbMiIsIsidUntagMemberPort PROTO ((UINT4, UINT4, UINT4));
PUBLIC INT4 EcfmMiIsUntagMemberPort PROTO ((UINT4, UINT4, UINT4));
PUBLIC INT4 EcfmMiGetEgressPorts PROTO ((UINT4, UINT4, tPortList));
PUBLIC INT4 EcfmUpdateIntfType PROTO ((UINT4, UINT1));
/*ECFM API for LA*/
PUBLIC INT4 EcfmAddPortToPortChannel PROTO ((UINT4));
PUBLIC INT4 EcfmRemovePortFromPortChannel PROTO ((UINT4));
PUBLIC INT4 EcfmStartedInContext PROTO ((UINT4));
#ifdef MBSM_WANTED
PUBLIC INT4 EcfmMbsmPostMessage PROTO ((tMbsmProtoMsg *, INT4 ));
#endif /*MBSM_WANTED*/
PUBLIC INT4 EcfmApiMplsCallBack (UINT4, VOID *);
PUBLIC tEcfmLtrCacheIndices * EcfmAllocateLtrMemBlk(VOID);
PUBLIC VOID EcfmFreeLtrMemBlk(UINT1 *pu1Block);
INT4 NpEcfmProcessPkt PROTO ((UINT4, UINT4, UINT1, UINT2));
PUBLIC VOID EcfmGetMacAddrOfPort PROTO ((UINT4 u4IfIndex, UINT1 * MacAddr));
PUBLIC INT4 EcfmApiValidateEvcToMepMapping (UINT4 u4VlanId);
PUBLIC INT4 EcfmApiValMepIndex (tEcfmMepInfoParams * pMepInfo);

#if defined (Y1564_WANTED) || defined (RFC2544_WANTED)

PUBLIC tEcfmReqParams  * EcfmSlaGetPerfTestParams (UINT4 u4ContextId, 
                          UINT4 u4SlaId, UINT4 u4MegId, UINT4 u4MeId, 
                          UINT4 u4MepId);
INT4 
EcfmSlaStartSlaTest (tEcfmReqParams * pEcfmReqParams, UINT4 u4MsgType);

INT4
EcfmSlaStopSlaTest (tEcfmReqParams * pEcfmReqParams, UINT4 u4MsgType);

INT4
EcfmSlaFetchSlaReport (UINT4 u4MsgType, tEcfmReqParams * pEcfmReqParams,
                        tEcfmExtInParams * pEcfmExtInParams);

#endif
/* Structure used to pass/get the information to/from H/w routines */
typedef struct _EcfmHwCcTxParams
{
    tHwPortArray          PortList; /* Tagged Port list */
    tHwPortArray          UntagPortList; /* Untagged Port List */
    UINT4                 u4ContextId; /* Virtual Context Id */
    UINT4                 u4IfIndex; /* Interface Index */
    UINT2                 u2TxFilterId; /* Handler */
    UINT2                 u2PduLength; /* Size of the PDU */
    UINT1                 *pu1Pdu; /* Pointer to the PDU */
}tEcfmHwCcTxParams;

typedef struct _EcfmHwCcRxParams
{
    tHwPortArray        PortList; /* Tagged Port List */
    tHwPortArray        UntagPortList; /* Untagged Port List */
    tEcfmCcOffRxInfo   *pEcfmCcRxInfo; /* Structure pointer to RxInfo */
    UINT4               u4ContextId; /* Virtual Context Id */
    UINT4               u4IfIndex; /* Interface Index */
    UINT2               u2RxFilterId; /* Handler */
    UINT1               au1Pad [2]; /* Structure Padding */
}tEcfmHwCcRxParams;
    
typedef struct _EcfmHwMepParams
{
    UINT1            au1HwHandler [ECFM_HW_MEP_HANDLER_SIZE];    
    tMacAddr         DstMacAddr;
    tMacAddr         MepMacAddr;  /* Mac address of the MEP */
    UINT2            u2MepId;
    UINT1            u1MdLevel;
    UINT1            u1Direction; /* Indicates the direction of the MEP
                                   * (Up/Down) */
    UINT1            u1CcmPriority;
    UINT1            au1Pad [3];
}tEcfmHwMepParams;

typedef struct _EcfmHwRMepParams
{
    UINT1            au1HwHandler [ECFM_HW_MEP_HANDLER_SIZE];
    UINT2            u2MepId;
    UINT2            u2RMepId;
    UINT1            u1MdLevel;
    UINT1            u1Direction; /* Indicates the direction of the MEP */
    UINT1            u1State;     /* RMEP State */
    UINT1            au1Pad [1];
}tEcfmHwRMepParams;

typedef struct _EcfmHwMaParams
{
    UINT4              u4VlanIdIsid;
    UINT4              u4CcmInterval;
    UINT1              au1MAID[48];
    UINT1              au1HwHandler [ECFM_HW_MA_HANDLER_SIZE];
}tEcfmHwMaParams;

typedef struct _EcfmHwParams
{
    tEcfmHwMaParams         EcfmHwMaParams;
    tEcfmHwMepParams        EcfmHwMepParams;
    tEcfmHwRMepParams       EcfmHwRMepParams;
    union {
        tEcfmHwCcTxParams   EcfmHwCcTxParams;
        tEcfmHwCcRxParams   EcfmHwCcRxParams;
    }unParam;
    tEcfmHwEvents           EcfmHwEvents;
    mep_event_callback_t    *EcfmEventCallBack;
    UINT4                   *pu4HwCapability;
    UINT1                   au1HwHandler [ECFM_HW_MEP_HANDLER_SIZE];
    UINT1                   u1EcfmOffStatus;
    UINT1                   u1Priority;
    UINT1                   u1EcfmPerPortPerMepOffStatus;
    UINT1                   au1Pad [1];
}tEcfmHwParams;



typedef struct _ECfmHwLmParams
{
  UINT4  u4PortId;              /* Interface to which MEP is created */
  UINT2  u2VlanId;              /* VlanId to which MEP is created  */
  UINT2  u2Filler;             /* */
  UINT4  u4TxStatsHwId;       /* variable  to store hardware id of the */
                                /* egrees filter Id */

  UINT4  u4RxStatsHwId;       /* variable to store hardware id of the */
                                /* Ingress filter Id*/

}tEcfmHwLmParams;

#endif /* _ECFM_H */

/*-----------------------------------------------------------------------*/
/*                       End of the file  ecfm.h                         */
/*-----------------------------------------------------------------------*/

