/********************************************************************
 * Copyright (C) 2014 Aricent Inc . All Rights Reserved
 *
 * $Id: hb.h,v 1.24 2017/10/04 14:27:00 siva Exp $
 *
 * Description:
 *      This file contains all the exported definitions of FutureHB.
 *******************************************************************/
#ifndef _HB_H
#define _HB_H

#include "fssnmp.h"

/* Currently CRU BUF is used for HB. In future, this can be
 * modified to a linear buffer */
typedef tCRU_BUF_CHAIN_HEADER  tHbMsg;

/* Hb header format */
typedef struct {
   /* HB Version no. */
   UINT2 u2Version;

   /* HB pkt Checksum */
   UINT2 u2Chksum;

   /* HB pkt total length (HB hdr + data) */
   UINT4 u4TotLen;

   /* REQ/RESP */
   UINT4 u4MsgType;

   /* Source entity id */
   UINT4 u4SrcEntId;

   /* Destination entity id */
   UINT4 u4DestEntId;

   /* Sequence no. */
   UINT4 u4SeqNo;

#define HB_HDR_OFF_VERSION      0
#define HB_HDR_OFF_CKSUM        2
#define HB_HDR_OFF_TOT_LENGTH   4
#define HB_HDR_OFF_MSG_TYPE     8
#define HB_HDR_OFF_SRC_ENTID   12
#define HB_HDR_OFF_DEST_ENTID  16
#define HB_HDR_OFF_SEQ_NO       20
#define HB_HDR_LENGTH    sizeof(tHbHdr)
} tHbHdr;

typedef enum {
       HB_FDB_ADD = 1,
       HB_FDB_DEL = 2
}tHbFdbAction;

/* Heart Beat interval - in terms of msecs */
/* To convert into STUPS, divide by NO_OF_MSECS_PER_UNIT
   10msec = 1STUPS,
   1000msecs = 1000/10 = 100 STUPS
   Increasing default HB interval results in increasing 4 times of
   the change in the peer dead interval. Hence switchover time
   will be increased and may lead to timer miscalulation when
   standby becomes active.*/

#define HB_DEFAULT_INTERVAL 500 /* 1000 msecs = 1/2 sec */
#define HB_MIN_INTERVAL     10   /* 10 msecs */
#define HB_MAX_INTERVAL     5000 /* 5 secs */

/* peer dead interval multiplier */
#define HB_MIN_PEER_DEAD_INT_MULTIPLIER     4
#define HB_MAX_PEER_DEAD_INT_MULTIPLIER     10
#define HB_DEFAULT_PEER_DEAD_INT_MULTIPLIER \
    HB_MIN_PEER_DEAD_INT_MULTIPLIER

/* Peer node dead interval = 4 times of HB interval */
#define HB_DEFAULT_PEER_DEAD_INTERVAL \
    (HB_DEFAULT_PEER_DEAD_INT_MULTIPLIER * HB_DEFAULT_INTERVAL)

#define HB_MIN_PEER_DEAD_INTERVAL     40    /* 40 msecs */
#define HB_MAX_PEER_DEAD_INTERVAL     20000 /* 20 secs */

/* Level of trace for HB */
#define HB_INIT_SHUT_TRC         0x00000001
#define HB_MGMT_TRC              0x00000002
#define HB_CTRL_PATH_TRC         0x00000004
#define HB_CRITICAL_TRC          0x00000008
#define HB_PEER_DISC_SM_TRC      0x00000010
#define HB_SOCKET_API_TRC        0x00000020
#define HB_NOTIFICATION_TRC      0x00000040
#define HB_ALL_FAILURE_TRC       0x00000080
#define HB_BUFFER_TRC            0x00000100
#define HB_EVENT_TRC             0x00000200
#define HB_PKT_DUMP_TRC          0x00000400
#define HB_SNMP_TRC              0x00000800
#define HB_SWITCHOVER_TRC        0x00001000
#define HB_ALL_TRC               (HB_INIT_SHUT_TRC | \
                                  HB_MGMT_TRC | \
                                  HB_CTRL_PATH_TRC | \
                                  HB_CRITICAL_TRC | \
                                  HB_PEER_DISC_SM_TRC | \
                                  HB_SOCKET_API_TRC | \
                                  HB_NOTIFICATION_TRC | \
                                  HB_ALL_FAILURE_TRC | \
                                  HB_BUFFER_TRC | \
                                  HB_EVENT_TRC | \
                                  HB_PKT_DUMP_TRC | \
                                  HB_SNMP_TRC | \
                                  HB_SWITCHOVER_TRC)

/* HB flags set in the HB messages */
#define  HB_FORCE_SWITCHOVER            0x0001
#define  HB_FORCE_SWITCHOVER_ACK        0x0002
#define  HB_TRANS_IN_PROGRESS_FLAG      0x0004 /* Transition in progress flag */
#define  HB_STANDBY_REBOOT              0x0008
#define  HB_STDBY_PROTOCOL_RESTART      0x0010
#define  HB_STDBY_PROTOCOL_RESTART_ACK  0x0020
#define  HB_STDBY_PEER_UP               0x0040
#define  HB_STDBY_PEER_UP_ACK           0x0080
#define  HB_SLAVE_PEER_UP               0x0100
#define  HB_SLAVE_PEER_UP_ACK           0x0200
#define  HB_ICCH_INIT_STATE             0x0400
#define  HB_ICCH_MASTER_STATE           0x0800
#define  HB_ICCH_SLAVE_STATE            0x1000
#define  HB_COUNTER_ENABLE              0x2000
#define  HB_COUNTER_DISABLE             0x4000

/* HB module events */
#define HB_TMR_EXPIRY_EVENT         0x0001
#define HB_PKT_RCVD                 0x0002
#define HB_RESUME_EVENT             0x0004
#define HB_FSW_EVENT                0x0008
#define HB_SYNC_MSG_PROCESSED       0x0010
#define HB_APP_INIT_COMPLETED       0x0020
#define HB_CHG_TO_TRANS_IN_PRGS     0x0040
#define HB_PKT_RCVD_ON_ETH_PORT     0x0080
#define HB_ICCH_PKT_RCVD            0x0100
#define HB_ICCH_SYNC_MSG_PROCESSED  0x0200
#define HB_MCLAG_START_HB_MSG       0x0400
#define HB_PKT_RCVD_ON_ICCL_PORT    0x0800
#define HB_MCLAG_STOP_HB_MSG        0x1000
#define HB_ISSU_FORCE_STANDBY       0x2000
#define HB_MCLAG_START              0x4000
#define HB_MCLAG_SHUTDOWN           0x8000

#define HB_MODULE_EVENTS (HB_TMR_EXPIRY_EVENT |\
                          HB_PKT_RCVD |HB_ICCH_PKT_RCVD |\
                          HB_FSW_EVENT | \
                          HB_ICCH_SYNC_MSG_PROCESSED | \
                          HB_SYNC_MSG_PROCESSED |\
                          HB_APP_INIT_COMPLETED | \
                          HB_CHG_TO_TRANS_IN_PRGS | \
                          HB_PKT_RCVD_ON_ETH_PORT | \
                          HB_MCLAG_START_HB_MSG | \
                          HB_MCLAG_STOP_HB_MSG | \
                          HB_PKT_RCVD_ON_ICCL_PORT | \
                          HB_ISSU_FORCE_STANDBY | \
                          HB_MCLAG_START | \
                          HB_MCLAG_SHUTDOWN)

enum
{
    HB_RM_APP = 1,
    HB_ICCH_APP,
    HB_MAX_APPS
};

/*Sync pkt length*/
#define HB_SYNC_PKT_LEN 512
#define HB_ICCH_PKT_LEN 174

/* HB packet ethernet header information */
#define HB_MSG_ETHERTYPE  0x88b6      /* Ether type */
#define HB_MSG_ETHERTYPE_LEN 2        /* Length of Ethertype field */
#define HB_MSG_VLAN_PROTOID_OFFSET 12 /* Offset of vlan protocol ID */
#define HB_MSG_VLAN_TAG_OFFSET 14     /* Offset of vlan tag information */
#define HB_ICCH_MCAST_IPADDR   0xE00000FB
#define HB_MSG_ETHERTYPE_OFFSET 16    /* Offset of Ethertype field */
#define HB_ICCH_PROTO     251
#define HB_MSG_ETH_HEADER_SIZE 18     /* Size of Ethernet header */
#define HB_MSG_ETH_DOUBLETAG_HEADER_SIZE 22 /* Size of Ethernet header with double tag */

#define HB_MSG_VLAN_HIGHEST_PRIORITY VLAN_HIGHEST_PRIORITY

/* Return values of HB module */
#define HB_SUCCESS       OSIX_SUCCESS
#define HB_FAILURE       OSIX_FAILURE

INT4 HbLock (VOID);
INT4 HbUnLock (VOID);

VOID HbRegisterApps (UINT4 u4AppId);

INT4  RmHbCallBackFn    (tCRU_BUF_CHAIN_HEADER *pBuf);
INT4  HbIcchPostHBPktToIcch  (tCRU_BUF_CHAIN_HEADER *pBuf);
INT4  HbIcchResumeProtocolOperation (VOID);
INT4  HbIcchStopProtocolOperation (VOID);
VOID  HbTaskMain        (INT1 *pArg);
/* API used by RM module to send event to HB module */
UINT4 HbApiSendRmEventToHb (UINT4 u4Event);

UINT4 HbApiSendIcchEventToHb (UINT4 u4Event);

UINT4  HbApiSetFlagAndTxHbMsg (UINT4 u4Flag);

VOID   HbInformOtherModulesBootUp (INT1 *);

VOID  HbApiSetKeepAliveFlag (UINT1 u1RmKeepAliveFlag);

VOID  HbApiSetIcchKeepAliveFlag (UINT1 u1IcchKeepAliveFlag);
VOID HbSetTrcLevel (UINT4 u4TrcLevel);

VOID HbApiSendEvtFromMCLAG (UINT4 u4Event);
/* API used by RM module to fetch HB global variable*/
UINT4 HbGetPeerNodeId (VOID);

UINT4 HbGetNodeState (VOID);

UINT4 HbGetActiveNodeId (VOID);

INT1 HbSetHbInterval (UINT4);

INT1 HbSetHbPeerDeadInterval (UINT4);

INT1 HbSetHbPeerDeadIntMultiplier (UINT4);

UINT4 HbGetHbInterval (VOID);

UINT4 HbGetHbPeerDeadInterval (VOID);

UINT4 HbGetHbPeerDeadIntMultiplier (VOID);
UINT4
HbApiSendEventToHb (UINT4 u4Event);

UINT4 HbApiIssuForceStandBy PROTO ((VOID));
UINT2 HbApiGetPortMaskValue PROTO ((VOID));
INT4 HbApiMclagEnable PROTO ((VOID));
INT4 HbApiMclagDisable PROTO ((VOID));
#ifdef ICCH_WANTED

VOID
HbUpdateIcclFdbEntryForVlan (UINT2 u2VlanId, UINT1 u1Action);
UINT1 HbIcchGetMclagEnableFlagStatus(VOID);
#endif

INT1 HbApiIssuHandleTimer PROTO ((UINT1));
VOID
HbGetSysMacAddr (UINT1* au1DestMac);
#endif /* _HB_H */

