/*******************************************************************
 * Copyright (C) 2015 Aricent Inc . All Rights Reserved
 *
 * $Id: rtmx.h,v 1.1 2015/10/13 13:11:22 siva Exp $
 *
 * Description: Function declarations for RTMx functions 
 *******************************************************************/

#ifndef _RTMX_H_
#define _RTMX_H_

INT4
RtmxGetBestRouteInCxt (void *pInputRt, void **ppOutputRt, 
                       UINT1 u1QueryFlag, UINT1 u1Protocol);

#endif /* _RTMX_H */
