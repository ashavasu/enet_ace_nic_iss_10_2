/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsl2tpcli.h,v 1.1 2016/07/08 07:41:04 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 L2tpSystemControl[12];
extern UINT4 L2tpGlobalEnable[12];
extern UINT4 L2tpClearGlobalStats[12];
extern UINT4 L2tpClearSessionStats[12];
extern UINT4 L2tpTrcFlag[12];
extern UINT4 L2tpSetTraps[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetL2tpSystemControl(i4SetValL2tpSystemControl)	\
	nmhSetCmn(L2tpSystemControl, 12, L2tpSystemControlSet, NULL, NULL, 0, 0, 0, "%i", i4SetValL2tpSystemControl)
#define nmhSetL2tpGlobalEnable(i4SetValL2tpGlobalEnable)	\
	nmhSetCmn(L2tpGlobalEnable, 12, L2tpGlobalEnableSet, NULL, NULL, 0, 0, 0, "%i", i4SetValL2tpGlobalEnable)
#define nmhSetL2tpClearGlobalStats(i4SetValL2tpClearGlobalStats)	\
	nmhSetCmn(L2tpClearGlobalStats, 12, L2tpClearGlobalStatsSet, NULL, NULL, 0, 0, 0, "%i", i4SetValL2tpClearGlobalStats)
#define nmhSetL2tpClearSessionStats(u4SetValL2tpClearSessionStats)	\
	nmhSetCmn(L2tpClearSessionStats, 12, L2tpClearSessionStatsSet, NULL, NULL, 0, 0, 0, "%u", u4SetValL2tpClearSessionStats)
#define nmhSetL2tpTrcFlag(i4SetValL2tpTrcFlag)	\
	nmhSetCmn(L2tpTrcFlag, 12, L2tpTrcFlagSet, NULL, NULL, 0, 0, 0, "%i", i4SetValL2tpTrcFlag)
#define nmhSetL2tpSetTraps(i4SetValL2tpSetTraps)	\
	nmhSetCmn(L2tpSetTraps, 12, L2tpSetTrapsSet, NULL, NULL, 0, 0, 0, "%i", i4SetValL2tpSetTraps)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 L2tpPortIfIndex[14];
extern UINT4 L2tpEnabledStatus[14];
extern UINT4 L2tpPortEncapType[14];
extern UINT4 L2tpPortRowStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetL2tpEnabledStatus(i4L2tpPortIfIndex ,i4SetValL2tpEnabledStatus)	\
	nmhSetCmn(L2tpEnabledStatus, 14, L2tpEnabledStatusSet, NULL, NULL, 0, 0, 1, "%i %i", i4L2tpPortIfIndex ,i4SetValL2tpEnabledStatus)
#define nmhSetL2tpPortEncapType(i4L2tpPortIfIndex ,i4SetValL2tpPortEncapType)	\
	nmhSetCmn(L2tpPortEncapType, 14, L2tpPortEncapTypeSet, NULL, NULL, 0, 0, 1, "%i %i", i4L2tpPortIfIndex ,i4SetValL2tpPortEncapType)
#define nmhSetL2tpPortRowStatus(i4L2tpPortIfIndex ,i4SetValL2tpPortRowStatus)	\
	nmhSetCmn(L2tpPortRowStatus, 14, L2tpPortRowStatusSet, NULL, NULL, 0, 1, 1, "%i %i", i4L2tpPortIfIndex ,i4SetValL2tpPortRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 L2tpPwIndex[14];
extern UINT4 L2tpIPSecEnabledStatus[14];
extern UINT4 L2tpPwLoopBack[14];
extern UINT4 L2tpRemoteIpAddress[14];
extern UINT4 L2tpPwRowStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetL2tpIPSecEnabledStatus(u4L2tpPwIndex ,i4SetValL2tpIPSecEnabledStatus)	\
	nmhSetCmn(L2tpIPSecEnabledStatus, 14, L2tpIPSecEnabledStatusSet, NULL, NULL, 0, 0, 1, "%u %i", u4L2tpPwIndex ,i4SetValL2tpIPSecEnabledStatus)
#define nmhSetL2tpPwLoopBack(u4L2tpPwIndex ,pSetValL2tpPwLoopBack)	\
	nmhSetCmn(L2tpPwLoopBack, 14, L2tpPwLoopBackSet, NULL, NULL, 0, 0, 1, "%u %s", u4L2tpPwIndex ,pSetValL2tpPwLoopBack)
#define nmhSetL2tpRemoteIpAddress(u4L2tpPwIndex ,u4SetValL2tpRemoteIpAddress)	\
	nmhSetCmn(L2tpRemoteIpAddress, 14, L2tpRemoteIpAddressSet, NULL, NULL, 0, 0, 1, "%u %p", u4L2tpPwIndex ,u4SetValL2tpRemoteIpAddress)
#define nmhSetL2tpPwRowStatus(u4L2tpPwIndex ,i4SetValL2tpPwRowStatus)	\
	nmhSetCmn(L2tpPwRowStatus, 14, L2tpPwRowStatusSet, NULL, NULL, 0, 1, 1, "%u %i", u4L2tpPwIndex ,i4SetValL2tpPwRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 L2tpRemoteEndId[14];
extern UINT4 L2tpLocalSessionId[14];
extern UINT4 L2tpRemoteSessionId[14];
extern UINT4 L2tpSessionPwIndex[14];
extern UINT4 L2tpSessionCookieSize[14];
extern UINT4 L2tpSessionLocalCookie[14];
extern UINT4 L2tpSessionRemoteCookie[14];
extern UINT4 L2tpSessionClearStatistics[14];
extern UINT4 L2tpSessionRowStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetL2tpLocalSessionId(u4L2tpRemoteEndId ,u4SetValL2tpLocalSessionId)	\
	nmhSetCmn(L2tpLocalSessionId, 14, L2tpLocalSessionIdSet, NULL, NULL, 0, 0, 1, "%u %u", u4L2tpRemoteEndId ,u4SetValL2tpLocalSessionId)
#define nmhSetL2tpRemoteSessionId(u4L2tpRemoteEndId ,u4SetValL2tpRemoteSessionId)	\
	nmhSetCmn(L2tpRemoteSessionId, 14, L2tpRemoteSessionIdSet, NULL, NULL, 0, 0, 1, "%u %u", u4L2tpRemoteEndId ,u4SetValL2tpRemoteSessionId)
#define nmhSetL2tpSessionPwIndex(u4L2tpRemoteEndId ,i4SetValL2tpSessionPwIndex)	\
	nmhSetCmn(L2tpSessionPwIndex, 14, L2tpSessionPwIndexSet, NULL, NULL, 0, 0, 1, "%u %i", u4L2tpRemoteEndId ,i4SetValL2tpSessionPwIndex)
#define nmhSetL2tpSessionCookieSize(u4L2tpRemoteEndId ,i4SetValL2tpSessionCookieSize)	\
	nmhSetCmn(L2tpSessionCookieSize, 14, L2tpSessionCookieSizeSet, NULL, NULL, 0, 0, 1, "%u %i", u4L2tpRemoteEndId ,i4SetValL2tpSessionCookieSize)
#define nmhSetL2tpSessionLocalCookie(u4L2tpRemoteEndId ,pSetValL2tpSessionLocalCookie)	\
	nmhSetCmn(L2tpSessionLocalCookie, 14, L2tpSessionLocalCookieSet, NULL, NULL, 0, 0, 1, "%u %s", u4L2tpRemoteEndId ,pSetValL2tpSessionLocalCookie)
#define nmhSetL2tpSessionRemoteCookie(u4L2tpRemoteEndId ,pSetValL2tpSessionRemoteCookie)	\
	nmhSetCmn(L2tpSessionRemoteCookie, 14, L2tpSessionRemoteCookieSet, NULL, NULL, 0, 0, 1, "%u %s", u4L2tpRemoteEndId ,pSetValL2tpSessionRemoteCookie)
#define nmhSetL2tpSessionClearStatistics(u4L2tpRemoteEndId ,i4SetValL2tpSessionClearStatistics)	\
	nmhSetCmn(L2tpSessionClearStatistics, 14, L2tpSessionClearStatisticsSet, NULL, NULL, 0, 0, 1, "%u %i", u4L2tpRemoteEndId ,i4SetValL2tpSessionClearStatistics)
#define nmhSetL2tpSessionRowStatus(u4L2tpRemoteEndId ,i4SetValL2tpSessionRowStatus)	\
	nmhSetCmn(L2tpSessionRowStatus, 14, L2tpSessionRowStatusSet, NULL, NULL, 0, 1, 1, "%u %i", u4L2tpRemoteEndId ,i4SetValL2tpSessionRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 L2tpXconnectIfIndex[14];
extern UINT4 L2tpXconnectId[14];
extern UINT4 L2tpXconnectRemoteEndId[14];
extern UINT4 L2tpXconnectInnerVlanId[14];
extern UINT4 L2tpXconnectOuterVlanId[14];
extern UINT4 L2tpXconnectRowStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetL2tpXconnectRemoteEndId(i4L2tpXconnectIfIndex , u4L2tpXconnectId ,u4SetValL2tpXconnectRemoteEndId)	\
	nmhSetCmn(L2tpXconnectRemoteEndId, 14, L2tpXconnectRemoteEndIdSet, NULL, NULL, 0, 0, 2, "%i %u %u", i4L2tpXconnectIfIndex , u4L2tpXconnectId ,u4SetValL2tpXconnectRemoteEndId)
#define nmhSetL2tpXconnectInnerVlanId(i4L2tpXconnectIfIndex , u4L2tpXconnectId ,i4SetValL2tpXconnectInnerVlanId)	\
	nmhSetCmn(L2tpXconnectInnerVlanId, 14, L2tpXconnectInnerVlanIdSet, NULL, NULL, 0, 0, 2, "%i %u %i", i4L2tpXconnectIfIndex , u4L2tpXconnectId ,i4SetValL2tpXconnectInnerVlanId)
#define nmhSetL2tpXconnectOuterVlanId(i4L2tpXconnectIfIndex , u4L2tpXconnectId ,i4SetValL2tpXconnectOuterVlanId)	\
	nmhSetCmn(L2tpXconnectOuterVlanId, 14, L2tpXconnectOuterVlanIdSet, NULL, NULL, 0, 0, 2, "%i %u %i", i4L2tpXconnectIfIndex , u4L2tpXconnectId ,i4SetValL2tpXconnectOuterVlanId)
#define nmhSetL2tpXconnectRowStatus(i4L2tpXconnectIfIndex , u4L2tpXconnectId ,i4SetValL2tpXconnectRowStatus)	\
	nmhSetCmn(L2tpXconnectRowStatus, 14, L2tpXconnectRowStatusSet, NULL, NULL, 0, 1, 2, "%i %u %i", i4L2tpXconnectIfIndex , u4L2tpXconnectId ,i4SetValL2tpXconnectRowStatus)

#endif
