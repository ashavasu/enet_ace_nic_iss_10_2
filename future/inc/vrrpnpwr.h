
/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: vrrpnpwr.h,v 1.2 2014/10/16 13:04:12 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              for VRRP wrappers
 *              
 ********************************************************************/
#ifndef __VRRP_NP_WR_H__
#define __VRRP_NP_WR_H__

UINT1 VrrpFsNpIpv4VrrpInstallFilter PROTO ((VOID));
UINT1 VrrpFsNpIpv4VrrpRemoveFilter PROTO ((VOID));
UINT1 VrrpFsNpIpv4VrrpIntfDeleteWr PROTO ((tNpVlanId u2VlanId , UINT4 u4IfIndex , UINT4 u4IpAddr , UINT1 *au1MacAddr ));
UINT1 VrrpFsNpIpv4VrrpIntfCreateWr PROTO ((tNpVlanId u2VlanId , UINT4 u4IfIndex , UINT4 u4IpAddr , UINT1 *au1MacAddr ));
UINT1 VrrpFsNpIpv4VrfArpDel PROTO ((UINT4 u4VrId , UINT4 u4IpAddr , UINT1 *pu1IfName , INT1 i1State ));
UINT1 VrrpFsNpIpv4VrfArpAdd PROTO ((UINT4 u4VrId , tNpVlanId u2VlanId , UINT4 u4IfIndex , UINT4 u4IpAddr , UINT1 *pMacAddr , UINT1 *pu1IfName , INT1 i1State , UINT4 *pu4TblFull ));
UINT1 VrrpFsNpIpv4GetVrrpInterface PROTO ((INT4 i4IfIndex , INT4 i4VrId ));

#endif /* __VRRP_NP_WR_H__ */
