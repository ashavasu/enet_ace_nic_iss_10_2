/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: pbb.h,v 1.24 2013/05/06 11:56:14 siva Exp $
 *
 * Description: This file contains all the function prototypes
 *              exported bythe PBB Module.
 *
 *******************************************************************/
#ifndef _PBB_H
#define _PBB_H

#include "fsvlan.h"
#include "fssnmp.h"

#ifdef NPAPI_WANTED
INT1
PbbCopyPortPropertiesToHw PROTO((UINT4 u4DstPort, UINT4 u4SrcPort));

INT1
PbbRemovePortPropertiesFromHw PROTO((UINT4 u4DstPort, UINT4 u4SrcPort));
#endif

INT1 PbbGetDerivedInstanceMacAddress PROTO(( UINT4 u4InstanceId,
        UINT1 *u1InstanceMacAddrType));
INT1 PbbGetContextListForInstance PROTO((UINT4 u4InstanceId, 
        tSNMP_OCTET_STRING_TYPE *pContextList));
INT1 PbbGetFreeInstanceId PROTO(( UINT4 *pu4InstanceId));
INT1 PbbGetInstanceIdFromAlias PROTO((tSNMP_OCTET_STRING_TYPE *pu1InstanceName,
            UINT4 *pu4InstanceId));

INT1 PbbGetVIPValue PROTO((
            UINT4 u4ContextId, 
            UINT4 u4Isid, 
            UINT2 *pu2Vip));
INT1
PbbGetVipFromIsid PROTO ((UINT4 u4Context,
                          UINT4 u4Isid,
                          UINT2 *pu2Vip));

INT4
PbbSetVipPortType PROTO((UINT4 u4ContextId, 
            UINT4 u4IfIndex, 
            UINT2 u2LocalPort));

INT4
PbbGetPipIsidWithVip PROTO((UINT4 u4ContextId, UINT2 u2Vip,
                            UINT4 *pu4PipIndex, UINT4 *pu4Isid,
                            tMacAddr *pBDaAddr));

INT1
PbbStartPbbModule (VOID);

INT1
PbbShutdownPbbModule (VOID);

INT4
PbbGetCbpPortListForIsid PROTO((UINT4 u4ContextId, UINT2 u2LocalPort,
        UINT4 u4Isid, UINT1* OutPortList ));

INT4
PbbCreateContext  PROTO  ((UINT4 u4ContextId));

INT4
PbbDeleteContext PROTO  ((UINT4 u4ContextId));

INT4
PbbUpdateContextName PROTO  ((UINT4 u4ContextId));

INT4
PbbDeletePort PROTO  ((INT4 i4IfIndex));

INT1
PbbGetVipISid PROTO ((INT4 i4ifIndex, UINT4 *u4Isid));

 INT1
PbbUpdatePortStatus PROTO ((INT4 i4IfIndex,UINT1 u1PortStatus));

INT4
PbbMapPortIndication PROTO ((UINT4 u4ContextId, INT4 i4IfIndex, UINT2 u2LocalPort));

INT4
PbbUnMapPortIndication PROTO ((INT4 i4IfIndex));


INT4
PbbCreatePort PROTO  ((UINT4 u4ContextId, INT4 i4IfIndex, UINT2 u2LocalPort));
VOID
PbbMain PROTO((INT1 *pi1Param));

INT1
PbbMbsmPostMessage   PROTO((tMbsmProtoMsg * pProtoMsg, INT4 i4Event));

/* PBB Scalability parameters cannot exceed the following 
   Operator cannot configure any value for the scaling paramters 
   which exceeds the below mentioned values
 */
#define PBB_MAX_ISID                      131072
#define PBB_MAX_ISID_PER_CONTEXT    65536
#define PBB_MAX_PORT_PER_ISID_PER_CONTEXT 128
#define PBB_MAX_PORTS_PER_ISID            128

/* PBB SUCCESS/FAILURE */
#define PBB_SUCCESS                SUCCESS  
#define PBB_FAILURE                FAILURE


#define PBB_PORT_STATUS_UP 1
#define PBB_PORT_STATUS_DOWN 2

/*bridge mode*/
#define PBB_CUSTOMER_BRIDGE_MODE      1
#define PBB_PROVIDER_BRIDGE_MODE      2
#define PBB_PROVIDER_EDGE_BRIDGE_MODE 3
#define PBB_PROVIDER_CORE_BRIDGE_MODE 4
#define PBB_ICOMPONENT_BRIDGE_MODE    5
#define PBB_BCOMPONENT_BRIDGE_MODE    6
#define PBB_INVALID_BRIDGE_MODE       7


/* PCP encoding/decoding related values */
#define PBB_MAX_NUM_PCP_SEL_ROW      4
#define PBB_MAX_NUM_PCP              8
#define PBB_MAX_NUM_PRIORITY         8
#define PBB_MAX_NUM_DROP_ELIGIBLE    2

#define PBB_8P0D_SEL_ROW             1
#define PBB_7P1D_SEL_ROW             2
#define PBB_6P2D_SEL_ROW             3
#define PBB_5P3D_SEL_ROW             4
#define PBB_INVALID_SEL_ROW          0

#define PBB_INVALID_DE              -1
#define PBB_DE_FALSE                 0
#define PBB_DE_TRUE                  1

#define PBB_VLAN_SNMP_TRUE           1
#define PBB_VLAN_SNMP_FALSE          2

#define PBB_MAX_PCP_VAL              7
#define PBB_MIN_PCP_VAL              0
#define PBB_MAX_PRIORITY             7
#define PBB_MIN_PRIORITY             0


#define PBB_NODE_IDLE                      RM_INIT
#define PBB_NODE_ACTIVE                    RM_ACTIVE
#define PBB_NODE_STANDBY                   RM_STANDBY
#define PBB_NODE_SWITCHOVER_IN_PROGRESS    RM_TRANSITION_IN_PROGRESS

/*Component Type*/

#define PBB_INVALID_COMP        0
#define PBB_ICOMPONENT          1
#define PBB_BCOMPONENT          2

/*Service Type for a ICOMPONENT*/
#define PBB_VIP_TYPE_EGRESS   1
#define PBB_VIP_TYPE_INGRESS  2
#define PBB_VIP_TYPE_BOTH     3

/*Service Type for a BCOMPONENT*/
#define PBB_EGRESS              1
#define PBB_INGRESS             2
#define PBB_EGRESS_INGRESS      3

/* Provider Bridge Port Types. */
#define PBB_PROVIDER_NETWORK_PORT         CFA_PROVIDER_NETWORK_PORT
#define PBB_CNP_PORTBASED_PORT            CFA_CNP_PORTBASED_PORT
#define PBB_CNP_STAGGED_PORT              CFA_CNP_STAGGED_PORT
#define PBB_CNP_CTAGGED_PORT              CFA_CNP_CTAGGED_PORT                         
#define PBB_CUSTOMER_EDGE_PORT            CFA_CUSTOMER_EDGE_PORT
#define PBB_PROP_CUSTOMER_EDGE_PORT       CFA_PROP_CUSTOMER_EDGE_PORT
#define PBB_PROP_CUSTOMER_NETWORK_PORT    CFA_PROP_CUSTOMER_NETWORK_PORT
#define PBB_PROP_PROVIDER_NETWORK_PORT    CFA_PROP_PROVIDER_NETWORK_PORT
#define PBB_CUSTOMER_BRIDGE_PORT          CFA_CUSTOMER_BRIDGE_PORT
#define PBB_PROVIDER_EDGE_PORT            CFA_PROVIDER_EDGE_PORT
#define PBB_VIRTUAL_INSTANCE_PORT         CFA_VIRTUAL_INSTANCE_PORT
#define PBB_PROVIDER_INSTANCE_PORT        CFA_PROVIDER_INSTANCE_PORT            
#define PBB_CUSTOMER_BACKBONE_PORT        CFA_CUSTOMER_BACKBONE_PORT           
#define PBB_INVALID_BRIDGE_PORT           CFA_INVALID_BRIDGE_PORT

/* PBB backbone instance macros used by PBB and CLI*/
#define PBB_INSTANCE_ALIAS_LEN            PBB_CONTEXT_ALIAS_LEN
#define PBB_DEF_INSTANCE_ID               0
#define PBB_INSTANCE_MAC_DERIVED         0
#define PBB_INSTANCE_MAC_USER_DEF        1

#define    PBB_PORT_IN_FRAMES 1
#define    PBB_PORT_OUT_FRAMES 2
enum {
    PBB_TUNNEL_PROTOCOL_PEER=1,
    PBB_TUNNEL_PROTOCOL_TUNNEL,
    PBB_TUNNEL_PROTOCOL_DISCARD,
    PBB_TUNNEL_PROTOCOL_INVALID
};

enum {
    PBB_VLAN_AUDIT_COMPLETED=1,
    PBB_VLAN_AUDIT_BRG_MODE_CHG
};

typedef struct _tIsidTagInfo {
   UINT4 u4Isid;
   UINT1 u1TagType;      /* VLAN_TAGGED / VLAN_UNTAGGED / 
                          * VLAN_PRIORITY_TAGGED */
   UINT1 u1Priority;     /* The priority associated with the incoming frame */
   UINT1 u1DropEligible; /* Drop eligible value associated with the incoming frame */
   UINT1 u1UcaBitValue;
  tMacAddr  CSAMacAddr;
  tMacAddr  CDAMacAddr;
}tIsidTagInfo;

typedef struct _tPbbTag {
   tVlanTagInfo  OuterVlanTag;
   tIsidTagInfo  InnerIsidTag;
  tMacAddr  BSAMacAddr;
  tMacAddr  BDAMacAddr;
} tPbbTag;

typedef struct _tPbbVlanAuditStatus {
    UINT4    u4VlanAudStatus;
    UINT4    u4ContextId;
} tPbbVlanAuditStatus;


#define ISID_UCA_BIT_MASK      0x08000000
#define PBB_ISID_IS_UCA_BIT_SET(u4TempIsidTag)((u4TempIsidTag&(ISID_UCA_BIT_MASK))?(PBB_TRUE):(PBB_FALSE))
#define PBB_ISID_PROTOCOL_ID      0x8100
#define PBB_PROVIDER_PROTOCOL_ID  0x88a8
#define PBB_ISID_TAG_PID_LEN      6
#define ISID_TAG_SIZE    4
#define ISID_TAG_PRIORITY_SHIFT   28
#define ISID_TAGGED_HEADER_SIZE        18
#define MAC_ADDRESS_SIZE        6
#define ISID_TAG_PRIORITY_DEI_SHIFT    28
#define ISID_ID_MASK            0x00FFFFFF

#define PBB_CDA_OFFSET_IN_ITAG_PKT  18
#define PBB_CDA_OFFSET_IN_BITAG_PKT  22


INT4
PbbGetIsidInfoFromFrame PROTO ((UINT4 u4ContextId, UINT2 u2LocalPort,
                          tCRU_BUF_CHAIN_DESC * pBuf, tPbbTag * pPbbTag, 
        UINT4 u4IsidTagOffSet));
INT4 PbbGetPipWithPortList PROTO ((UINT4 u4ContextId, UINT2 u2LocalPort, UINT1 *InPortList, UINT1 *OutPortList ));

INT4 PbbGetForwardPortList PROTO  ((UINT4 u4ContextId, UINT2 u2LocalPort,
      UINT4 u4Isid, UINT1 OutPortList ));
INT4
PbbGetBvidForIsid PROTO ((UINT4 u4ContextId, UINT2 u2LocalPort,UINT4 u4Isid, tVlanId *pBvid));

INT4
PbbGetIsidOfVip(UINT4 *pu4Isid,UINT4 u4IfIndex,UINT4 u4ContextId);

INT1 PbbIsShutDown  PROTO((VOID));

INT4
PbbCliGetVipIndexOfIsid PROTO ((UINT4 u4ContextId,UINT4 u4Isid, UINT4 *pu4Value));

INT1
PbbGetIsidForVip  PROTO((UINT4 u4Vip, UINT4 *pu4Isid));

INT1
PbbGetRelayIsidFromLocalIsid PROTO((UINT4 u4ContextId, UINT2 u2LocalPort, 
        UINT4 u4LocalIsid,UINT4 *pu4RelayIsid));

INT1
PbbGetLocalIsidFromRelayIsid  PROTO((UINT4 u4ContextId, UINT2 u2LocalPort, 
       UINT4 u4RelayIsid,UINT4 *pu4LocalIsid));

 INT1
PbbGetVipIsidWithPortList PROTO((UINT4 u4ContextId, UINT2 u2LocalPort,
        UINT1 *InPortList, UINT2 *pu2Vip,UINT2 *pu2Pip,
        UINT4 *pu4Isid, tMacAddr *pBDaAddr));
INT4
PbbSendFrameToPort PROTO ((tCRU_BUF_CHAIN_DESC * pFrame, 
     tVlanOutIfMsg * pVlanOutIfMsg,
     UINT4 u4ContextId, UINT2 u2Port, 
     UINT4  u4Isid, UINT1 u1RegenPri));
INT1
PbbIsIsidMemberPort PROTO ((UINT4 u4ContextId, UINT4 u4Isid, UINT2 u2LocalPort));

   INT1
PbbGetPipVipWithIsid(UINT4 u4ContextId, UINT2 u2LocalPort,
         UINT4 u4Isid,UINT2 *pu2Vip, UINT2 *pu2Pip);

 INT1
PbbGetCbpListForIsid PROTO((UINT4 u4ContextId, UINT4 u4Isid,
                  tSNMP_OCTET_STRING_TYPE *pRetValCbpList));

INT4
PbbGetMemberPortsForIsid PROTO (( UINT4 u4ContextId,
                          UINT4 u4Isid , tSNMP_OCTET_STRING_TYPE *pPortList));
INT4
PbbGetCnpMemberPortsForIsid ( UINT4 u4ContextId,
                          UINT4 u4Isid , tSNMP_OCTET_STRING_TYPE *pPortList);
INT4
PbbIsPortTypePip (INT4 i4IfIndex);
VOID PbbGetEncodingPcpVal (UINT1 u1PcpSelRow, UINT1 u1Priority, UINT1 u1DropEligible, INT4 *pi4PcpVal);
VOID PbbGetDecodingDropEligible (UINT1 u1PcpSelRow, UINT1 u1PcpVal, INT4 *pi4DropEligible);
VOID PbbGetDecodingPriority (UINT1 u1PcpSelRow, UINT1 u1PcpVal, INT4 *pi4Priority);
VOID PbbGetBCompBDA PROTO ((UINT4, UINT2, UINT4, tMacAddr, UINT1, BOOL1));
INT4
PbbL2IwfSetProtocolTunnelStatusOnPort (UINT4 u4ContextId, UINT2 u2Port,
                                        UINT2 u2Protocol, UINT1 u1TunnelStatus);

INT4
PbbL2IwfDeleteIsid (UINT4 u4ContextId, UINT4 u4Isid);
INT4
PbbConfPortIsidLckStatus (UINT4 u4IfIndex, UINT4 u4Isid, BOOL1 b1Status);
VOID
PbbGetPortIsidStats  (UINT4 u4IfIndex, UINT4 u4Isid,
                          UINT4 *pu4TxFCl, UINT4 *pu4RxFCl);
INT1
PbbGetNextIsidForCBP (UINT4 u4ContextId, UINT2 u2LocalPort, UINT4  u4CurrIsid, UINT4 * pu4NextIsid);

INT4
PbbVlanAuditStatusInd PROTO ((tPbbVlanAuditStatus *pPbbVlanStatus));

#ifdef L2RED_WANTED 
INT4 PbbRedHandleUpdateEvents PROTO ((UINT1 u1Event, tRmMsg *pData, 
                                      UINT2 u2DataLen));
#endif /* L2RED_WANTED */

#endif


