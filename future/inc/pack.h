/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pack.h,v 1.16 2011/01/27 10:15:58 siva Exp $
 *
 * Description: Used for enabling the PACK option
 *
 *******************************************************************/
#ifndef _PACK_H
#define _PACK_H

#ifdef  PACK_REQUIRED
#pragma pack  (1)
#endif

#endif/*_PACK_H*/
