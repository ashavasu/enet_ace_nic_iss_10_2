/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: pvrst.h,v 1.10 2017/10/26 12:21:00 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              by  external modules.
 *
 *******************************************************************/
#ifndef _PVRST_H
#define _PVRST_H

/*********************************************
 *         definiton of Macros               *
 *********************************************/   

#define PVRST_TRUE    1
#define PVRST_FALSE    0

#define PVRST_SUCCESS                    0
#define PVRST_FAILURE                    1

#define PVRST_ENABLED                    1
#define PVRST_DISABLED                   2
#define PVRST_SNMP_START                 1
#define PVRST_SNMP_SHUTDOWN              2
#define PVRST_TRUNK_PORT   2
#define PVRST_ACCESS_PORT   1
#define PVRST_HYBRID_PORT                3
#define STP_DST_MAC_OFFSET               0
#define PVRST_COMP_ADDR_LEN              3
#define ISL_HEADER_SIZE                  26

typedef enum {
   PVRST_DOT1Q = 0,
   PVRST_ISL = 1
}tEncapsulationType;

typedef enum {
   PVRST_NP_DOT1Q_ISL = 0,
   PVRST_NP_ISL = 1,
   PVRST_NP_MAX_FILTERS = 2
}tHwFilterType;

#define  PVRST_NP_INVALID_FILTER_TYPE          0

/*********************************************
 *   Prototypes for exported functions       *
 *********************************************/   
/*********************************************
 *   Prototypes for exported functions       *
 *********************************************/   

INT4 PvrstModuleInit ( VOID );

INT4 PvrstMiVlanCreateIndication (UINT4 u4ContextId, tVlanId VlanId);
INT4 PvrstMiVlanDeleteIndication (UINT4 u4ContextId, tVlanId VlanId);
INT4 PvrstCreateVlanIndication (UINT4 u4ContextId, tVlanId VlanId);
INT4 PvrstDeleteVlanIndication (UINT4 u4ContextId, tVlanId VlanId);
INT4 PvrstSetNativeVlan (UINT4 u4IfIndex, tVlanId OldVlanId, tVlanId NewVlanId );
INT4 PvrstSetPortType (UINT2 u2PortNum, UINT1 u1PortType,tVlanId VlanId);
INT4 PvrstSetTaggedPort (tLocalPortList pu1Addedports, 
                                  tLocalPortList pu1Deletedports,tVlanId VlanId);
INT4 PvrstMiDeleteAllVlanIndication (UINT4 u4ContextId);
INT4 AstIsPvrstStartedInContext (UINT4 u4ContextId);
INT4 AstIsPvrstEnabledInContext (UINT4 u4ContextId);
INT4 PvrstModuleStartUp (VOID);
INT4 PvrstModuleShutdown (VOID);
INT4 AstIsPvrstInstExist(VOID);
UINT1 PvrstIsPathcostConfigured (UINT2 u2PortNum, UINT2 u2InstanceId);
#endif /* _PVRST_H */
