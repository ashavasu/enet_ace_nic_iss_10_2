/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igs.h,v 1.33 2007/02/12 06:27:53 iss Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              by  external modules.
 *
 *******************************************************************/
#ifndef _IGS_H
#define _IGS_H

#define   IGS_SUCCESS                     0
#define   IGS_FAILURE                    -1
#define   IGS_TRUE                        1
#define   IGS_FALSE                       0
#define   IGS_ENABLE                      1
#define   IGS_DISABLE                     2
#define   IGS_STARTED                     1
#define   IGS_SHUTDOWN                    2
#define   IGS_VLAN_ADD                    1
#define   IGS_VLAN_DELETE                 2
#define   IGS_CREATE_FWD_ENTRY            1
#define   IGS_DELETE_FWD_ENTRY            2
#define   IGS_ADD_PORT                    3
#define   IGS_DEL_PORT                    4

#define   IGS_ENABLED                     0
#define   IGS_DISABLED                    -1

#define   IGS_MAX_PHY_PORTS              BRG_MAX_PHY_PORTS
#define   IGS_MAX_PORTS                  BRG_MAX_PHY_PLUS_LOG_PORTS
#define   IGS_MAX_PORTS_PER_INSTANCE     IGS_MAX_PORTS
#define   IGS_PORT_LIST_SIZE             BRG_PORT_LIST_SIZE
#define   IGS_INSTANCE_ID                1
#define   IGS_BIT8                       0x80

#define IGS_MCAST_FWD_MODE_IP            1
#define IGS_MCAST_FWD_MODE_MAC           2

#define IGS_RED_MCAST_ENTRY_ONLY_IN_HW   1
#define IGS_RED_MCAST_PORTS_ONLY_IN_HW   2
#define IGS_RED_MCAST_ENTRY_ONLY_IN_SW   3
#define IGS_RED_MCAST_PORTS_ONLY_IN_SW   4

#define   IGS_IP_PKT_TYPE                0x800 
#define   IGS_IGMP_PROTOCOL              2

/* NPAPI definitions */
#define   IGS_HW_CREATE_ENTRY            1
#define   IGS_HW_DELETE_ENTRY            2
#define   IGS_HW_ADD_PORT                3
#define   IGS_HW_DEL_PORT                4

/* These definitons needs to be removed
 * Kept for avoiding compilations errors so do not use these defintions */
#define   IGS_NO_FORWARD                 1
#define   IGS_FORWARD_ALL                2
#define   IGS_FORWARD_SPECIFIC           3
#define   IGS_LOOKUP_MCAST_FWD_TBL       4
#define   tIgsMsgAtt                     tMSG_ATTR


/* Igs Port list size */
typedef UINT1 tIgsPortList[IGS_PORT_LIST_SIZE];

#define   tIgsTimerListId                tTimerListId
#define   tIgsVlanId                     tVlanId
#define   tIgsPortBmp                    tIgsPortList

#define   IGS_LOCK() IgsLock ()
#define   IGS_UNLOCK() IgsUnLock ()

#define   IGS_IS_IGS_ENABLED() IgsIsIgmpSnoopingEnabled ()

INT4 IgsLock (VOID);
INT4 IgsUnLock (VOID);

VOID IgsMain  (INT1 *pi1Param);

INT4 RegisterPrpIgswithFutureSNMP PROTO ((void));

INT4
IgsProcessMcastFrame (tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2IfIndex,
                      tIgsVlanId VlanId);
INT4
IgsDeletePort (UINT2 u2Port);

INT4
IgsIsIgmpSnoopingEnabled (VOID);

INT4
IgsPortOperIndication (UINT2 u2Port, UINT1 u1Status);

INT4
IgsDelMcastFwdEntryForVlan (tIgsVlanId VlanId);

INT4
IgsGetMcFwdPorts (tIgsVlanId VlanId, UINT4 u4GrpAddr, UINT4 u4SrcAddr, 
                  tIgsPortList PortList, tIgsPortList UntagPortList);

INT4 
IgsUpdatePortList PROTO ((tIgsVlanId VlanId, tMacAddr McastAddr,
                          tIgsPortBmp AddPortBitmap, tIgsPortBmp DelPortBitmap,
                          UINT1 u1PortType));

#ifdef MBSM_WANTED
INT4
IgsMbsmProcessUpdateMessage (tMbsmProtoMsg * pProtoMsg, INT4 i4Cmd);
#endif /*MBSM_WANTED */

/* These functions needs to be removed 
 * Dependency removal. Its kept here for avoiding Compilation Errors. */
INT4
IgsUpdateVlanStatus (BOOL1 b1Status);

VOID
IgsCreatePort (UINT2 u2Port);

INT4
IgsProcessMultiCastFrame (tCRU_BUF_CHAIN_HEADER * pBuf,
                          tBrgIf * pBrgIf,
                          tIgsVlanId VlanId,
                          UINT2 *pu2Result, tIgsPortList PortList);
INT4
IgsProcessMcastDataPkt (tCRU_BUF_CHAIN_HEADER * pBuf, tIgsMsgAtt* pMsgAttr);


INT4 IgsRedMcastAuditSyncMsg (UINT1 u1Type, tVlanId VlanId,
                              tMacAddr MacGroupAddr, 
                              tPortList AuditPortBmp);

INT1  IgsGetForwardingType (VOID);

INT4  IgsRedRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen);

INT4 IgsIsHwProgrammingAllowed (VOID);

#endif /*_IGS_H */
