/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 * 
 * $Id: issu.h,v 1.12 2016/06/30 10:05:53 siva Exp $
 * 
 * Description: ISSU main functions.
 ***********************************************************************/

#ifndef __ISSU_H__
#define __ISSU_H__

/* ISSU lock and unlock parameters*/
#define ISSU_LOCK()   IssuLock()
#define ISSU_UNLOCK()   IssuUnLock()

/* Constants */
#define ISSU_SUCCESS     OSIX_SUCCESS
#define ISSU_FAILURE     OSIX_FAILURE

#define ISSU_COMP_CHECK_ENABLE   1
#define ISSU_COMP_CHECK_DISABLE  2

#define ISSU_AUTO_MODE                      0
#define ISSU_FULL_COMPATIBLE_MODE           1
#define ISSU_BASE_COMPATIBLE_MODE           2
#define ISSU_INCOMPATIBLE_MODE              3
#define ISSU_DEFAULT_MODE                   ISSU_AUTO_MODE

#define ISSU_TRAP_ENABLE                    1
#define ISSU_TRAP_DISABLE                   2

#define ISSU_MAINTENANCE_MODE_ENABLE        1
#define ISSU_MAINTENANCE_MODE_DISABLE       2
#define ISSU_DEFAULT_STARTUP_MODE           ISSU_MAINTENANCE_MODE_DISABLE 

#define ISSU_MAINTENANCE_OPER_ENABLE        1
#define ISSU_MAINTENANCE_OPER_DISABLE       2
#define ISSU_MAINTENANCE_OPER_INPROGRESS    3

#define ISSU_NODE_ROLE_AUTO                 0
#define ISSU_NODE_ROLE_ACTIVE               1
#define ISSU_NODE_ROLE_STANDBY              2
#define ISSU_DEFAULT_NODE_ROLE              ISSU_NODE_ROLE_AUTO

#define ISSU_STRTOUL(s,e,b)                 strtoul ((char *)(s), (char **)(e), (int)(b))

#define ISSU_PATH_MAX_SIZE                  128    /*ISSU image path max size*/
#define ISSU_IMAGE_NAME_MAX_SIZE            32     /*ISSU image name max size*/
#define ISSU_MAX_FILES                      1      /*Number of ISSU files need to move New path*/

#define ISSU_TIMER_VALUE                    10

#define ISSU_INIT_SCRIPT                    "/local/issinit/issload.sh"  

enum
{
    ISSU_NOT_INITIATED = 0,
    ISSU_COMPATIBLE,
    ISSU_BASE_COMPATIBLE,
    ISSU_NOT_COMPATIBLE,
    ISSU_CHECK_IN_PROGRESS,
    ISSU_COMP_FAILED
};

enum
{
    ISSU_CMD_NOT_STARTED = 0,
    ISSU_CMD_IN_PROGRESS,
    ISSU_CMD_SUCCESSFUL,
    ISSU_CMD_FAILED
};

enum
{
    ISSU_CMD_LOAD_VERSION = 1,
    ISSU_CMD_FORCE_STANDBY
};

enum
{
    ISSU_PROCEDURE_NOT_INITIATED = 0,
    ISSU_PROCEDURE_IN_PROGRESS,
    ISSU_PROCEDURE_SUCCESSFUL,
    ISSU_PROCEDURE_FAILED
};

/* Standard Trace messages */
#define ISSU_INIT_SHUT_TRC              0x00000001
#define ISSU_MGMT_TRC                   0x00000002
#define ISSU_CONTROL_PATH_TRC           0x00000004
#define ISSU_ALL_FAILURE_TRC            0x00000008

/* ISSU specific trace messages */
#define ISSU_CRITICAL_TRC               0x00000010

#define ISSU_ALL_TRC     (ISSU_INIT_SHUT_TRC | ISSU_MGMT_TRC | ISSU_CONTROL_PATH_TRC \
           | ISSU_ALL_FAILURE_TRC | ISSU_CRITICAL_TRC)

/* By default enable critical & failure trace for ISSU */
#define ISSU_DEFAULT_TRACE (ISSU_ALL_FAILURE_TRC | ISSU_CRITICAL_TRC) 

VOID 
IssuModuleInit PROTO ((VOID));

INT4 IssuLock PROTO ((VOID));
INT4 IssuUnLock PROTO ((VOID));

UINT4 IssuGetMaintModeOperation PROTO ((VOID));
VOID IssuSetOperMaintenanceMode PROTO ((UINT1));
INT4  IssuGetMaintenanceMode PROTO ((VOID));
UINT4 IssuGetIssuMode PROTO ((VOID));
INT4  IssuIsOidAllowed PROTO ((tSNMP_OID_TYPE OID));
INT4  IssuIsOidAllowedInStandby PROTO ((tSNMP_OID_TYPE OID));
VOID  IssuSetLoadVersionCommandStatus PROTO ((UINT4, UINT4));
INT4  IssuIsCliCmdAllowed PROTO ((INT1 *));
INT4  IssuIsCliCmdRestricted PROTO ((INT1 *));
VOID IssuUpdateProcedureStatus PROTO((VOID));
UINT4 IssuIsHwProgrammingAllowed PROTO ((VOID));
VOID IssuApiForceStandbyCompleted PROTO ((VOID));
UINT1 IssuApiIsLoadVersionTriggered PROTO ((VOID));
VOID IssuApiRestoreCacheFile PROTO ((VOID));
INT4 IssuApiPerformLoadVersionNPCmds PROTO ((VOID));
#endif

