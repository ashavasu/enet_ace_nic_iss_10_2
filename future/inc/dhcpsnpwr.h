
/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: dhcpsnpwr.h,v 1.1 2013/03/28 11:56:14 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              for DHCP-SRV wrappers
 *              
 ********************************************************************/

#ifndef __DHCPS_NP_WR_H__
#define __DHCPS_NP_WR_H__

UINT1 DhcpsFsNpDhcpSrvInit PROTO ((VOID));
UINT1 DhcpsFsNpDhcpSrvDeInit PROTO ((VOID));

#endif /* __DHCPS_NP_WR_H__ */
