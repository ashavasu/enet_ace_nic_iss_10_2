/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: arsec.h,v 1.15 2016/07/08 07:34:12 siva Exp $
 *
 * Description:This file contains the exported definitions and
 *             macros of security module / firewall
 *
 *******************************************************************/

#ifndef   _ARSEC_H
#define   _ARSEC_H


#define SEC_DEVICE_FILE_NAME "ksec"
#define SEC_MAJOR_NUMBER 200

#define SECMOD_SUCCESS 0
#define SECMOD_FAILURE -1

#define SEC_MODULE_NAME "ISSSec.ko"
#define SEC_RM_MODULE_NAME "ISSSec"
#define SEC_DEF_XMIT_DEV "eth0"
#define SEC_DEF_WAN_DEV_1 "eth0"
#define SEC_DEF_WAN_DEV_2 "eth1"
#define SEC_DEF_LAN_DEV_1 "eth2"
#define SEC_DEF_LAN_DEV_2 "eth3"

#define   CFA_PACKET_ARRIVAL_EVENT          0x00000001

#define UNALLOCATED -1

#define GDD_INTERFACE         _IOWR(SEC_MAJOR_NUMBER, 1, char *)
#define GDD_NP_IOCTL          _IOWR(SEC_MAJOR_NUMBER, 2, char *)
#define IPAUTH_NMH_IOCTL      _IOWR(SEC_MAJOR_NUMBER, 3, char *)
#define GDD_OOB_IOCTL         _IOWR(SEC_MAJOR_NUMBER, 4, char *)
#define IPAUTH_PORT_IOCTL     _IOWR(SEC_MAJOR_NUMBER, 5, char *)
#define GDD_CLI_IOCTL         _IOWR(SEC_MAJOR_NUMBER, 6, char *)
#define FWL_NMH_IOCTL         _IOWR(SEC_MAJOR_NUMBER, 8, char *)
#define NAT_NMH_IOCTL         _IOWR(SEC_MAJOR_NUMBER, 9, char *)
#define VPN_NMH_IOCTL         _IOWR(SEC_MAJOR_NUMBER, 10, char *)
#define IKE_IPSEC_IOCTL       _IOWR(SEC_MAJOR_NUMBER, 11, char *)
#define SYSLOG_MSG_IOCTL      _IOWR(SEC_MAJOR_NUMBER, 12, char *)
#define SYSLOG_CONFIG_IOCTL   _IOWR(SEC_MAJOR_NUMBER, 13, char *)
#define FLOW_MGR_IOCTL        _IOWR(SEC_MAJOR_NUMBER, 14, char *)
#define DSL_NMH_IOCTL         _IOWR(SEC_MAJOR_NUMBER, 15, char *)
#define SEC_MOD_INIT_IOCTL    _IOWR(SEC_MAJOR_NUMBER, 16, char *)
#define SEC_CFA_IF_CREATE     _IOWR(SEC_MAJOR_NUMBER, 17, char *)
#define SEC_CFA_IF_DELETE     _IOWR(SEC_MAJOR_NUMBER, 18, char *)
#define SEC_CFA_IF_UPDATE     _IOWR(SEC_MAJOR_NUMBER, 19, char *)
#define SEC_CFA_IF_UPDATE_VLANID     _IOWR(SEC_MAJOR_NUMBER, 20, char *)
#define SEC_UT_IOCTL     _IOWR(SEC_MAJOR_NUMBER, 21, char *)
#define FIPS_IPSEC_IOCTL     _IOWR(SEC_MAJOR_NUMBER, 22, char *)
#define SEC_CFA_ADD_PPP_SESSION_INFO _IOWR(SEC_MAJOR_NUMBER, 23, char *)
#define SEC_CFA_DEL_PPP_SESSION_INFO _IOWR(SEC_MAJOR_NUMBER, 24, char *)
#define SEC_IP6_ADDR_ADD     _IOWR(SEC_MAJOR_NUMBER, 25, char *)
#define SEC_IP6_ADDR_DEL     _IOWR(SEC_MAJOR_NUMBER, 26, char *)
#define IPSECV6_IOCTL        _IOWR(SEC_MAJOR_NUMBER, 27, char *)
#define SEC_IVR_UPDT_IOCTL   _IOWR(SEC_MAJOR_NUMBER, 28, char *)
#define SEC_BRIDGING_STAT_UPDT_IOCTL  _IOWR(SEC_MAJOR_NUMBER, 29, char *)
#define SEC_SET_VLAN_LIST             _IOWR(SEC_MAJOR_NUMBER, 30, char *)
#define SEC_RESET_VLAN_LIST           _IOWR(SEC_MAJOR_NUMBER, 31, char *)
#define SEC_CFA_SECIP_ADD    _IOWR(SEC_MAJOR_NUMBER, 32, char *)
#define SEC_CFA_SECIP_DELETE _IOWR(SEC_MAJOR_NUMBER, 33, char *)
#define IPSECv4_DUMMY_IOCTL _IOWR(SEC_MAJOR_NUMBER, 34, char *)
#define SEC_CFA_WAN_PHY_IF_UPDATE     _IOWR(SEC_MAJOR_NUMBER, 35, char *)
#define SEC_SET_DEBUG_TRACE     _IOWR(SEC_MAJOR_NUMBER, 36, char *)
#define SEC_L2TP_UPDATE_PORT_INFO     _IOWR(SEC_MAJOR_NUMBER, 37, char *)
#define SEC_L2TP_UPDATE_ENCAP_INFO     _IOWR(SEC_MAJOR_NUMBER, 38, char *)
#define SEC_L2TP_UPDATE_DECAP_INFO     _IOWR(SEC_MAJOR_NUMBER, 39, char *)
#define SEC_L2TP_DELETE_ALL_ENCAP_DECAP_INFO _IOWR(SEC_MAJOR_NUMBER, 40, char *)
#define SEC_L2TP_DELETE_PORT_INFO     _IOWR(SEC_MAJOR_NUMBER, 41, char *)
#define SEC_L2TP_GET_STATS     _IOWR(SEC_MAJOR_NUMBER, 42, char *)
#define SEC_L2TP_CLEAR_STATS     _IOWR(SEC_MAJOR_NUMBER, 43, char *)
#define SEC_L2TP_SET_DEBUG     _IOWR(SEC_MAJOR_NUMBER, 44, char *)

enum
{
    SECUSR_MODULE = 1,
    NPSIM_MODULE, 
    SNORT_MODULE
};

#endif
