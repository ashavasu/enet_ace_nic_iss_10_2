/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: rbridge.h,v 1.3 2012/02/07 12:13:54 siva Exp $
*
* Description: Contains definitions, macros and functions to be used
*              by  external modules.
********************************************************************/
#ifndef _RBRG_H
#define _RBRG_H
#include "cli.h"
enum
{
     RBRG_ENABLE_ACCESS_PORT,
     RBRG_DISABLE_ACCESS_PORT,
     RBRG_ENABLE_TRUNK_PORT,
     RBRG_DISABLE_TRUNK_PORT,
     RBRG_FIB_CREATE,
     RBRG_CREATE_UCAST_DEST,
     RBRG_TRILL_ENABLE,
     RBRG_TRILL_DISABLE,
     RBRG_UCAST_DSET_DELETE,
     RBRG_TRILL_PORT_UPDATE,
     RBRG_EGRESS_UPDATE,
     RBRG_FIB_DELETE,
     RBRG_GET_PORT_STATS,
     RBRG_CLEAR_PORT_STATS
    };

typedef struct RBrgNpRbrgEntry{
    tMacAddr   unicast_mac;
    tMacAddr   egress_mac;
    tMacAddr   next_hop_mac;
    UINT1      u1TrillPortType;
    UINT1      au1Pad[1];
    UINT4      u4IfIndex;
    UINT4      u4UnicastVlan;
    UINT4      u4EgressVlan;
    UINT4      u4TrillPortHopCount;
    UINT4      u4TrillPortMTU;
    UINT4      u4TrillPortName;
    UINT4      u4TrillPortIfIndex;
    /* Out Parameters */
    INT4      i4L3_Intf_Id;
    INT4      i4L3EgressId;
    INT4      i4AccessTrillPort;
    INT4      i4NetworkTrillPort;
}tRBrgNpRbrgEntry;
/* Data structures used by Rbridge APIs. */
typedef struct RBrgFdb
{
    tMacAddr   UnicastFdbMac;
    UINT1      u1FdbConfidence;
    UINT1      au1Pad[1];
    UINT4      u4FdbId;
    UINT4      u4FdbEgressNickname;
    UINT4      u4FdbPort;
}tRBrgFdb;
typedef struct RBrgFib
{
   tMacAddr    NextHopMac;
   UINT1       au1Pad[2];
   UINT4       u4EgressNickname;
   UINT4       u4EgressPort;
   UINT4       u4NextHopNickname;
   UINT4       u4HopCount;
   UINT4       u4Mtu;
}tRBrgFib;
typedef struct RBrgConfigPort
{
    UINT4      u4Port;
    UINT4      u4DesigVlan;
    BOOL1      bPortDisable;
    BOOL1      bTrunkPort;
    BOOL1      bAccessPort;
    BOOL1      bDisableMacLearning;
}tRBrgConfigPort;
typedef struct RBrgConfig
{
    tRBrgFdb        *pRBrgConfigFdb;
    tRBrgFib        *pRBrgConfigFib; 
    tRBrgConfigPort *pRBrgConfigPort;
    UINT4            u4Command;
    UINT4            u4ContextId;
}tRBrgConfig;
typedef struct RBrgNpPortStats
{
    FS_UINT8          u8TrillInFrames;        /* Trill In Frames Counter */
    FS_UINT8          u8TrillOutFrames;       /* Trill Out Frames Counter */
    UINT4             u4RBrgPortId;           /* Port Index */
    UINT4             u4RPFChecksFailedCount; /* RPFChecksFailedCounter */
    UINT4             u4HopCountExceededCount;/* HopCountExceededCounter */
    UINT4             u4PortOptionsCount;     /* UnsupportedPortOptions
                                                 Counter */
}tRBrgNpPortStats;
typedef struct RBrgNpEntry{
    UINT4      u4Command;
    union {
    tRBrgNpPortStats RbrgNpPortStats;
    tRBrgNpRbrgEntry RbrgNpRbrgEntry;
    }unRBrgData;
}tRBrgNpEntry;
#define RbrgNpRbrgEntry unRBrgData.RbrgNpRbrgEntry 
#define RbrgNpPortStats unRBrgData.RbrgNpPortStats 
enum
{
    RBRG_CREATE_CONTEXT = 1,
    RBRG_DELETE_CONTEXT,
    RBRG_CREATE_PORT,
    RBRG_DELETE_PORT,
    RBRG_MAP_PORT,
    RBRG_UNMAP_PORT,
    RBRG_ADD_LAG_PORT,
    RBRG_REM_LAG_PORT,
    RBRG_MBSM_MSG_CARD_INSERT
};
enum
{
    RBRG_ADD_FDB = 1,
    RBRG_DEL_FDB,
    RBRG_UPDATE_FDB,
    RBRG_ADD_FIB,
    RBRG_DEL_FIB,
    RBRG_UPDATE_FIB,
    RBRG_EDIT_PORT
};
enum
{
    RBRG_ACCESS_PORT = 1,
    RBRG_TRUNK_PORT
};
/**************************rbrgtask.c*******************************/

PUBLIC VOID
RbrgTaskSpawnRbrgTask (INT1 *pi1Arg);
PUBLIC INT4
RbrgApiConfigRbridge PROTO ((tRBrgConfig *pRBrgInfo));
PUBLIC INT4 RbrgTaskRegisterRbrgMibs (VOID);
PUBLIC INT4
RbrgApiContextRequest PROTO ((UINT4 u4ContextId, UINT1 u1Request));
PUBLIC INT4
RbrgApiPortRequest PROTO ((UINT4 u4ContextId, UINT4 u4PortId, 
                           UINT1 u1Request));
PUBLIC INT4
RbrgApiPortToPortChannelRequest PROTO ((UINT4 u4ContextId, UINT4 u4PortId,
                                        UINT4 u4PortChId, UINT1 u1Request));
INT4 RbrgApiEditFdbEntry PROTO ((tRBrgFdb   *pRbrgFdbData,
                                 UINT4 u4ContextId, UINT4 u4Type));
INT4 RbrgApiEditFibEntry PROTO ((tRBrgFib *pRbrgFibData, UINT4 u4ContextId,
                                 UINT4 u4Type));
INT4 RbrgApiEditPortEntry PROTO ((tRBrgConfigPort *pRBrgConfigPort));
PUBLIC INT4
RbrgHwWrProgramEntry PROTO ((tRBrgNpEntry *pRbrgHwInfo));
/****************************rbrgmbsm.c**********************************/
#ifdef MBSM_WANTED
PUBLIC INT4 RbrgApiMbsmNotification PROTO ((tMbsmProtoMsg * pProtoMsg,
                                            INT4 i4Event));
PUBLIC VOID RbrgMbsmProcessMsg PROTO ((tMbsmProtoMsg * pProtoMsg));
PUBLIC VOID
RbrgMbsmConfigTrill PROTO ((tMbsmPortInfo * pMbsmPortInfo,
                            tMbsmSlotInfo * pSlotInfo));
#endif
#endif
