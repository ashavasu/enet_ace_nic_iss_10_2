/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tacacs.h,v 1.6 2015/04/28 11:56:01 siva Exp $
 *
 * Description:This file contains the exported definitions and
 *             macros of TACACS
 *
 *******************************************************************/
#ifndef _TACACS_H
#define _TACACS_H

/* TACACS+ packet types */

#define   TAC_AUTHEN   0x01
#define   TAC_AUTHOR   0x02
#define   TAC_ACCT     0x03

#define   TAC_AUTHEN_LOGIN   0x01
#define   TAC_AUTHEN_STATUS_PASS   0x01
#define   TAC_AUTHEN_STATUS_TIMEOUT   0x03
#define   TAC_AUTHEN_STATUS_FAIL   0x02
#define   TAC_AUTHEN_SVC_LOGIN   0x01
#define   TAC_AUTHEN_TYPE_PAP   0x02
#define   TAC_AUTHEN_TYPE_CHAP   0x03

#define   TAC_USER_NAME_LEN   32
#define   TAC_USER_MSG_LEN   256
#define   TAC_USER_DATA_LEN   256
#define   TAC_USER_PORT_LEN   16
#define   TAC_USER_REM_ADDR_LEN   16

#define   TAC_USER_PASSWD_LEN   256
#define   TAC_CHAP_CHALLENGE_LEN   256
#define   TAC_CHAP_RESPONSE_LEN   16
#define   TAC_MSCHAP_CHALLENGE_LEN   256
#define   TAC_MSCHAP_RESPONSE_LEN   49
#define   TAC_ARAP_CHALLENGE_LEN 8
#define   TAC_ARAP_RESPONSE_LEN 8
#define   TAC_AUTHOR_MAX_AVP   25
#define   TAC_AUTHOR_AVP_MAX_SIZE   100

#define   TAC_MAX_SEQNO  0xff 

#define   TAC_ACCT_MAX_AVP   15       /* If this is increased, 
                                       * TAC_ACCT_PKT_SIZE should also be
                                       * adjusted accordingly */

#define   TAC_ACCT_AVP_MAX_SIZE   128 /* If this is increased, 
                                       * TAC_ACCT_PKT_SIZE should also be
                                       * adjusted accordingly */

#define   TAC_SRV_MSG_LEN   TAC_USER_MSG_LEN
#define   TAC_SRV_DATA_LEN   TAC_USER_DATA_LEN


#define   TAC_CLNT_RES_OK   0x00
/* Data structures definition for authentication */

/* Trace Levels */

#define   TAC_TRACE_INFO   0x01
#define   TAC_TRACE_ERROR   0x02
#define   TAC_TRACE_PKT_TX   0x04
#define   TAC_TRACE_PKT_RX   0x08
#define   TAC_TRACE_ALL   0xFFFFFFFF

typedef struct _tacauthenuserpap {
    CHR1   au1Password[TAC_USER_PASSWD_LEN]; /* PAP user password */
} tTacAuthenUserPAP;

typedef struct _tacauthenuserchap {
    UINT1   au1Challenge[TAC_CHAP_CHALLENGE_LEN];
    UINT1   au1Response[TAC_CHAP_RESPONSE_LEN + 1];
    UINT1   u1Identifier;                    /* PPP identifier */
    UINT1   au1Padding[2];
} tTacAuthenUserCHAP;

typedef struct _tacauthenusermschap {
    UINT1   au1Challenge[TAC_MSCHAP_CHALLENGE_LEN];
    UINT1   au1Response[TAC_MSCHAP_RESPONSE_LEN + 1];
    UINT1   u1Identifier;                   /* PPP identifier */
    UINT1   au1Padding[1];
} tTacAuthenUserMSCHAP;

typedef struct _tacauthenuserarap {
    UINT1   au1ChallengeToRemote[TAC_ARAP_CHALLENGE_LEN + 1];
    UINT1   au1ChallengeFromRemote[TAC_ARAP_CHALLENGE_LEN + 1];
    UINT1   au1ResponseFromRemote[TAC_ARAP_RESPONSE_LEN + 1];
    UINT1   au1OldPassword[TAC_USER_PASSWD_LEN];
    UINT1   au1NewPassword1[TAC_USER_PASSWD_LEN];
    UINT1   au1NewPassword2[TAC_USER_PASSWD_LEN];
    UINT1   au1Padding[1];
} tTacAuthenUserARAP;

typedef struct _tacappinfo {
    INT4    (*fpAppCallBackFn)(VOID *);
    UINT4   u4AppEvent;     /* The event to be sent to application */
    tOsixTaskId TaskId; 
    tOsixQId  QueueId;
} tTacAppInfo;

typedef struct _tacauthorarguments {
    UINT1   au1AVP[TAC_AUTHOR_MAX_AVP][TAC_AUTHOR_AVP_MAX_SIZE];
    UINT1   u1NoOfArgs;
    UINT1   au1Padding[3];
} tTacAuthorArgs;

typedef struct _tacacctarguments {
    UINT1   au1AVP[TAC_ACCT_MAX_AVP][TAC_ACCT_AVP_MAX_SIZE];
    UINT1   u1NoOfArgs;
    UINT1   au1Padding[3];
} tTacAcctArgs;

typedef struct _tacautheninput {
    union
    {
        tTacAuthenUserPAP   UserPAP;
        tTacAuthenUserCHAP  UserCHAP;
        tTacAuthenUserMSCHAP   UserMSCHAP;
        tTacAuthenUserARAP  UserARAP;
    }UserInfo;
    tTacAppInfo   AppInfo;             /* The application info. */
    UINT4         u4AppReqId;         /* Application Request Identifier */
    CHR1          au1UserName[TAC_USER_NAME_LEN];
    CHR1          au1UserMessage[TAC_USER_MSG_LEN];
    CHR1          au1UserData[TAC_USER_DATA_LEN]; /* Used for abort reason */
    CHR1          au1Port[TAC_USER_PORT_LEN];     /* Port name */
    CHR1          au1RemAddr[TAC_USER_REM_ADDR_LEN];
    UINT1         u1PrivilegeLevel;
    UINT1         u1AuthenType;
    UINT1         u1Service;
    UINT1         u1Action;
} tTacAuthenInput;

typedef struct _tacauthenoutput {
    UINT4   u4AppReqId;         /* Application Request Identifier */
    INT4    i4AuthenStatus;
    INT4    i4ErrorValue;
    CHR1    au1ServerMessage[TAC_SRV_MSG_LEN];
    UINT1   au1ServerData[TAC_SRV_DATA_LEN];
    UINT1   u1NoEchoFlag;
    UINT1   au1Padding[3];
} tTacAuthenOutput;

typedef struct _tacauthorinput {
    tTacAuthorArgs   AuthorArgs;   /* Arguments to be authorized */
    tTacAppInfo   AppInfo;   /* Application resource information */
    UINT4   u4AppReqId;                 /* Application request identifier */
    CHR1    au1UserName[TAC_USER_NAME_LEN];
    CHR1    au1Port[TAC_USER_PORT_LEN];
    CHR1    au1RemAddr[TAC_USER_REM_ADDR_LEN];
    UINT1   u1PrivilegeLevel;
    UINT1   u1AuthenMethod;
    UINT1   u1AuthenType;
    UINT1   u1Service;
} tTacAuthorInput;

typedef struct _tacauthoroutput {
    tTacAuthorArgs   AuthorArgs;
    UINT4   u4AppReqId;                 /* Application request identifier */
    INT4    i4AuthorStatus;
    INT4    i4ErrorValue;
    CHR1    au1ServerMessage[TAC_SRV_MSG_LEN];
    UINT1   au1ServerData[TAC_SRV_DATA_LEN];
} tTacAuthorOutput;

typedef struct _tacacctinput {
    tTacAcctArgs   AcctArgs;
    tTacAuthorArgs   AuthorArgs;
    tTacAppInfo   AppInfo;  /* Application resource information */
    UINT4   u4AppReqId;                 /* Application request identifier */
    CHR1    au1UserName[TAC_USER_NAME_LEN];
    CHR1    au1Port[TAC_USER_PORT_LEN];
    CHR1    au1RemAddr[TAC_USER_REM_ADDR_LEN];
    UINT1   u1PrivilegeLevel;
    UINT1   u1AuthenMethod;
    UINT1   u1AuthenType;
    UINT1   u1Service;
    UINT1   u1AcctFlag;
    UINT1   au1Padding[3];
} tTacAcctInput;

typedef struct _tacacctoutput {
    UINT4   u4AppReqId;                 /* Application request identifier */
    INT4    i4AcctStatus;
    INT4    i4ErrorValue;
    CHR1    au1ServerMessage[TAC_SRV_MSG_LEN];
    UINT1   au1ServerData[TAC_SRV_DATA_LEN];
} tTacAcctOutput;

typedef struct _tacmsginput {
    UINT4   u4MsgType;
    union
    {
        tTacAuthenInput   AuthenInput;
        tTacAuthorInput   AuthorInput;
        tTacAcctInput     AcctInput;
    } TacInput;
} tTacMsgInput;

typedef struct _tacmsgoutput {
    UINT4   u4MsgType;
    tOsixTaskId  TaskId;
    union
    {
        tTacAuthenOutput   AuthenOutput;
        tTacAuthorOutput   AuthorOutput;
        tTacAcctOutput     AcctOutput;
    } TacOutput;
} tTacMsgOutput;


VOID TacacsClientMain ARG_LIST((INT1 *));
VOID TacNotifyTask (UINT4);

INT4 TacacsAuthenticateUser ARG_LIST((tTacAuthenInput *));
INT4 TacacsPlusProcessPacket ARG_LIST((tTacMsgInput *));
INT4 TacacsAuthorizeUser ARG_LIST((tTacAuthorInput *));
INT4 TacacsAccountUser ARG_LIST((tTacAcctInput *));
INT1 IsTacacsServerPort (UINT2);
#endif /* _TACACS_H */
