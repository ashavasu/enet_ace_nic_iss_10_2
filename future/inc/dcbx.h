/*************************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
* $Id: dcbx.h,v 1.11 2016/05/25 10:06:48 siva Exp $
* Description: This header file contains all the macros and protoype
*              of DCBX Module that is commonly used by other modules.
****************************************************************************/
#include "fswebnm.h"

#define DCBX_ZERO            0

/* DCBX Return Macros */
#define DCBX_SUCCESS         1
#define DCBX_FAILURE         0
#define DCBX_IGNORE          2

/* DCBX System control Macros */
#define DCBX_START           1
#define DCBX_SHUTDOWN        2

/* ETS System control Macros */
#define ETS_START            1
#define ETS_SHUTDOWN         2

/* ETS Enabled status Macros */
#define ETS_ENABLED          1
#define ETS_DISABLED         2

/* ETS Traffic Class Group Id Macros  */
#define ETS_TCGID0           0
#define ETS_TCGID1           1
#define ETS_TCGID2           2
#define ETS_TCGID3           3
#define ETS_TCGID4           4
#define ETS_TCGID5           5
#define ETS_TCGID6           6
#define ETS_TCGID7           7

/* Reserved TCGID for future use*/
#define ETS_TCGID8           8
#define ETS_TCGID9           9
#define ETS_TCGID10          10
#define ETS_TCGID11          11
#define ETS_TCGID12          12
#define ETS_TCGID13          13
#define ETS_TCGID14          14

/* Default TCGID  */
#define ETS_TCGID15          15

/* Default Traffic class group Id */
#define ETS_DEF_TCGID        ETS_TCGID15

/* This Macro is the maximum configurable or supported
 * traffic class group ID. This will be used in CLI,
 * SNMP and TLV Processing. In future, if Maximum 
 * supported traffic class has been changed then this
 * macro needs to be changed.*/
#define ETS_MAX_TCGID_CONF   ETS_TCGID8
#define ETS_MAX_S3_SCHID_CONF 11

/* PFC System control Macros */
#define PFC_START            1
#define PFC_SHUTDOWN         2

/* PFC Enabled status Macros */
#define PFC_ENABLED          1
#define PFC_DISABLED         2

/* TSA Macro declaration */

#define ETS_TC0           0
#define ETS_TC1           1
#define ETS_TC2           2
#define ETS_TC3           3
#define ETS_TC4           4
#define ETS_TC5           5
#define ETS_TC6           6
#define ETS_TC7           7

/* These macros are used to fill and update the Profile
 * entry of PFC when the dcbx API is called from QOSX 
 * module during hardware Audit process of PFC NPAPAI */

#define DCBX_PFC_FILL_PROFILE_ENTRY 1
#define DCBX_PFC_UPDATE_PROFILE_ID  2

/* Application Priority System control Macros */
#define APP_PRI_START            1
#define APP_PRI_SHUTDOWN         2

/* Application Priority Enabled status Macros */
#define APP_PRI_ENABLED          1
#define APP_PRI_DISABLED         2

enum {
    APP_PRI_HW_CREATE = 1,
    APP_PRI_HW_DELETE = 2
};

/* Credit based shaper flag */
#define ETS_CBS_SUPPORTED          1
#define ETS_CBS_NOT_SUPPORTED      0

/* Structure to be used by external applications that 
 * tend to access an application to priority mapping entry */
typedef struct _tAppPriApiMappingInfo
{
    UINT4    u4IfIndex;   /* Port Number*/
    INT4     i4Selector;  /* Selector Number*/
    INT4     i4Protocol;  /* Protocol Id */
    INT4     i4ErrorCode; /*Error code to indicate if any 
                            error occured during Addition of Application
                            to Priority mapping Entry*/
                          /* Error Codes to be defined */
    UINT1    u1Priority;  /*Priority */
    UINT1    au1Padding[3]; /*Padding Field*/
}tAppPriApiMappingInfo;

/* Enum for Dcbx Message Type used in L2IWF */
enum
{
    DCBX_CREATE_IF_MSG = 1,
    DCBX_DELETE_IF_MSG,
    DCBX_MAP_IF_MSG,
    DCBX_UNMAP_IF_MSG,
    DCBX_ADD_LA_MEM_MSG,
    DCBX_REM_LA_MEM_MSG,
};

/*Enum for Dcbx Error Messgaes used in External APIs*/
enum
{
  DCBX_APP_PRI_MAPPING_CREATION_FAILED = 1,
  DCBX_APP_PRI_MAPPING_NOT_FOUND,
  DCBX_APP_PRI_MAPPING_DELETION_FAILED
};

/* DCBX Main Task Prototype */
PUBLIC VOID DcbxMainTask (INT1 *pi1Arg);

/* DCBX Lock function Prototype */
PUBLIC INT4 DcbxLock       PROTO ((VOID));
PUBLIC INT4 DcbxUnLock     PROTO ((VOID));

/* DCBX API Prototype */
PUBLIC VOID DcbxApiApplCallbkFunc PROTO((tLldpAppTlv *pLldpAppTlv));
PUBLIC INT4 DcbxApiPortRequest PROTO((UINT4 u4PortId,UINT1 u1Status));
PUBLIC INT4 DcbxApiPortToPortChannelRequest PROTO ((UINT4 u4PortId,
                                  UINT4 u4PortChId, UINT1 u1Status));
PUBLIC VOID
DcbxApiGetPfcInfo PROTO ((UINT1 u1PfcProfile,UINT1 *pu1PfcHwCfgFlag, UINT4
                   *pu4PfcMinThreshold, UINT4 *pu4PfcMaxThreshold,
                   INT4 *pi4PfcHwProfileId,UINT1 u1Type));
PUBLIC INT4 DcbxApiVerifyTlvisDcbx PROTO ((UINT2 u2TlvType,
                                           UINT1 u1SubType, UINT1 *pau1OUI));

PUBLIC INT4 DcbxApiModuleStart PROTO ((VOID));
PUBLIC VOID DcbxApiModuleShutDown PROTO ((VOID));
PUBLIC UINT1 DcbxApiIsDcbxEnabled PROTO ((VOID));
PUBLIC UINT1 DcbxApiIsDcbxOperStateUp PROTO ((UINT4 u4IfIndex));
PUBLIC UINT1 DcbxApiIsDcbxAdminStateUp PROTO ((UINT4 u4IfIndex));
#ifdef DCBX_ETS_QOS_WANTED
PUBLIC UINT1 EtsApiIsEtsAdminModeUp PROTO ((UINT4 u4IfIndex));
#endif

#ifdef MBSM_WANTED
PUBLIC INT4 DcbxApiMbsmNotification PROTO 
                 ((tMbsmProtoMsg * pProtoMsg, INT4 i4Event));
#endif /* MBSM_WANTED */

#ifdef WEBNM_WANTED
/*DCB HTML Pages proto types*/
PUBLIC VOID DcbIssProcessHomePage PROTO ((tHttp * pHttp)); 
/*ETS HTML Pages proto types*/
PUBLIC VOID ETSIssProcessPriorityPage PROTO ((tHttp * pHttp));
PUBLIC VOID ETSIssProcessBandwidthPage PROTO ((tHttp * pHttp));
PUBLIC VOID ETSIssProcessLocalInfoPage PROTO ((tHttp * pHttp));
PUBLIC VOID ETSIssProcessRemoteInfoPage PROTO ((tHttp * pHttp));
PUBLIC VOID ETSIssProcessAdminInfoPage PROTO ((tHttp * pHttp));
PUBLIC VOID ETSIssProcessTsaInfoPage PROTO ((tHttp *pHttp));

/*PFC HTML pages Prototypes*/
PUBLIC VOID PFCIssProcessPage PROTO ((tHttp * pHttp));
PUBLIC VOID PFCIssProcessLocalPage PROTO ((tHttp * pHttp));
PUBLIC VOID PFCIssProcessRemotePage PROTO ((tHttp * pHttp));
PUBLIC VOID PFCIssProcessAdminInfoPage PROTO ((tHttp * pHttp));
/*PFC HTML pages Prototypes*/
PUBLIC VOID AppPriIssProcessPage PROTO ((tHttp * pHttp));
PUBLIC VOID AppPriIssProcessLocalPageGet PROTO ((tHttp * pHttp));
PUBLIC VOID AppPriIssProcessRemotePageGet PROTO ((tHttp * pHttp));
PUBLIC VOID AppPriIssProcessAdminInfoPage PROTO ((tHttp * pHttp));

#endif
/*A pplication Priority APIs */
PUBLIC INT4
AppPriApiAddAppPriMapping PROTO 
((tAppPriApiMappingInfo *pAppPriApimappingInfo));
PUBLIC INT4
AppPriApiRemoveAppPriMapping PROTO
((tAppPriApiMappingInfo *pAppPriApimappingInfo));
PUBLIC INT4
AppPriApiGetAppPriMapping PROTO 
((tAppPriApiMappingInfo *pAppPriApimappingInfo));

PUBLIC INT4 DcbxApiVerifyTlvIsCee PROTO ((UINT1 *, UINT4));
PUBLIC INT4 DcbxApiVerifyTlvIsIeee PROTO ((UINT1 *, UINT4));
