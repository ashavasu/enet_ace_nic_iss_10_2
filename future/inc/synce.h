/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * $Id: synce.h,v 1.2 2013/03/23 14:31:53 siva Exp $
 * Description: Contains definitions, macros and functions to be used
 *              by external modules.
 ********************************************************************/
#ifndef __SYNCE_H__
#define __SYNCE_H__

#define  CFA_ENET_SYNCE     0x8809

/* trace related macros */
#define  SYNCE_NONE_TRC           NO_TRC
#define  SYNCE_INIT_SHUT_TRC      INIT_SHUT_TRC
#define  SYNCE_MGMT_TRC           MGMT_TRC
#define  SYNCE_DATA_PATH_TRC      DATA_PATH_TRC
#define  SYNCE_CONTROL_PLANE_TRC  CONTROL_PLANE_TRC
#define  SYNCE_DUMP_TRC           DUMP_TRC
#define  SYNCE_OS_RESOURCE_TRC    OS_RESOURCE_TRC
#define  SYNCE_BUFFER_TRC         BUFFER_TRC
#define  SYNCE_ALL_FAILURE_TRC    ALL_FAILURE_TRC

#define  SYNCE_FN_ENTRY_TRC       (0x1 << 16)
#define  SYNCE_FN_EXIT_TRC        (0x1 << 17)
#define  SYNCE_CRITICAL_TRC       (0x1 << 18)

#define  SYNCE_ALL_TRC   (SYNCE_FN_ENTRY_TRC        |\
                          SYNCE_FN_EXIT_TRC         |\
                          SYNCE_INIT_SHUT_TRC       |\
                          SYNCE_MGMT_TRC            |\
                          SYNCE_DATA_PATH_TRC       |\
                          SYNCE_CONTROL_PLANE_TRC   |\
                          SYNCE_DUMP_TRC            |\
                          SYNCE_OS_RESOURCE_TRC     |\
                          SYNCE_BUFFER_TRC          |\
                          SYNCE_ALL_FAILURE_TRC     |\
                          SYNCE_CRITICAL_TRC        )

/* function prototypes */
VOID SynceMainTask (INT1 *);

/* synceapi.c */
INT4 SynceDetectEsmcPdu(UINT1 *pu1Pdu, UINT2 u2PduLen);
INT4 SynceApiEnqIncomingPdu(tCRU_BUF_CHAIN_HEADER *pBuf, UINT2 u2PduLen, UINT4 u4IfIndex);
INT4 SynceApiIsSyncePDU (tCRU_BUF_CHAIN_HEADER *pBuf);
INT4 SynceApiIfDeleteCb (tCfaRegInfo * pCfaRegInfo);

#endif
