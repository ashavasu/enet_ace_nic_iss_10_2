/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 *
 *
 * Description:This file contains the exported definitions and
 *             prototypes of TAP interfaces
 *
 *******************************************************************/


#ifndef _TAP_H
#define _TAP_H
#include "fswebnm.h"

#define TAP_MAX_FRAME_SIZE      1522

#define TAP_FILE                "/dev/net/tun"

#define TAP_INT_QUEUE (UINT1 *) "TapIfQ"
#define TAP_INT_QUEUE_DEPTH     MAX_TAP_INTERFACES
#define TAP_IN_MEM_BLOCKS       100

typedef struct TapIfDs{
    INT4 i4TapFd;
    UINT1 u1TapIfName[24];
    UINT4 u4CfaIfIdx;
    UINT1 u1IfType;
    UINT1 u1Pad[3];
}tTapIfDs;

typedef struct TapIfMsg{
    INT4          i4TapFd;
    INT4          i4CfaIfIdx;
    UINT1         u1TapIfName[24];
    UINT1         *pu1PktBuf;
    UINT2         u2PktLen;
    UINT1         u1MsgType;
    UINT1         u1Padding;
} tTapIfMsg;

extern tTapIfDs TapIfInfo[MAX_TAP_INTERFACES]; 

extern tOsixQId gTapIfQId;

extern tMemPoolId gTapIfMemPoolId;
PUBLIC VOID
TapIfCallbackFn (INT4 i4TaprdFd);

PUBLIC INT4
CfaGddTunTapWrite  (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4CfaIfIdx);

PUBLIC VOID
CfaGddTunTapRead (INT4 i4Taprdfd);

/* Tap interface functions */
INT4
GetTapIfInfoFrmCfaIdx (UINT4 u4CfaIdx, tTapIfDs ** pTapIfInfo);
INT4
GetTapIfInfoFrmFileDesc (INT4 i4FileDesc, tTapIfDs ** pTapIfInfo);
INT4
GetTapIfInfoFrmIfName (UINT1 *pu1IfName, tTapIfDs ** pTapIfInfo);
INT4
CfaProcessPacketfromTap (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex, UINT4 u4PktLen);

#endif
