/* $Id: utlbit.h,v 1.6 2013/07/03 12:22:36 siva Exp $*/

#ifndef _UTLBIT_H
#define _UTLBIT_H

/* BIT manipulation macros */

#define BITS_PER_BYTE 8
extern UINT1 gau1BitMaskMap[BITS_PER_BYTE]; 

#define OSIX_ADD_PORT_LIST(au1List1, au1List2, u4Size1, u4Size2) \
              {\
                 UINT4 u4TmpSize; \
                 UINT2 u2ByteIndex; \
                 \
                 (u4Size1 < u4Size2) ? \
                            (u4TmpSize = u4Size1) : (u4TmpSize = u4Size2); \
                 \
                 for (u2ByteIndex = 0;\
                      u2ByteIndex < u4TmpSize;\
                      u2ByteIndex++) \
                 {\
                    au1List1[u2ByteIndex] |= au1List2[u2ByteIndex];\
                 }\
              }
#define OSIX_RESET_PORT_LIST(au1List1, au1List2, u4Size1, u4Size2) \
              {\
                 UINT4 u4TmpSize; \
                 UINT2 u2ByteIndex;\
                 \
                 (u4Size1 < u4Size2) ? \
                            (u4TmpSize = u4Size1) : (u4TmpSize = u4Size2); \
                 for (u2ByteIndex = 0;\
                      u2ByteIndex < u4TmpSize;\
                      u2ByteIndex++) {\
                    au1List1[u2ByteIndex] &= ~au1List2[u2ByteIndex];\
                 }\
              }
 
 #define OSIX_BITLIST_IS_BIT_SET(au1BitArray, u2BitNumber, i4ArraySize, bResult) \
        {\
           UINT2 u2BitNumberBytePos;\
           UINT2 u2BitNumberBitPos;\
           u2BitNumberBytePos = (UINT2)(u2BitNumber / BITS_PER_BYTE);\
           u2BitNumberBitPos  = (UINT2)(u2BitNumber % BITS_PER_BYTE);\
           bResult = OSIX_FALSE;      \
           if (u2BitNumberBitPos  == (UINT2) 0) {u2BitNumberBytePos = (UINT2)(u2BitNumberBytePos - 1);} \
           \
              if (u2BitNumberBytePos < i4ArraySize) \
              {                                     \
                  if ((au1BitArray[u2BitNumberBytePos] \
                       & gau1BitMaskMap[u2BitNumberBitPos]) != 0) {\
                      \
                          bResult = OSIX_TRUE;\
                  }\
              } \
        }


 /* Warning!!! - Do not call the macro with u2BitNumber as 0 or Invalid bit 
  * number */
#define OSIX_BITLIST_SET_BIT(au1BitArray, u2BitNumber, i4ArraySize) \
           {\
              UINT2 u2BitNumberBytePos;\
              UINT2 u2BitNumberBitPos;\
              u2BitNumberBytePos = (UINT2)(u2BitNumber / BITS_PER_BYTE);\
              u2BitNumberBitPos  = (UINT2)(u2BitNumber % BITS_PER_BYTE);\
            if (u2BitNumberBitPos  == 0) {u2BitNumberBytePos = (UINT2) (u2BitNumberBytePos - 1);} \
              \
              if (u2BitNumberBytePos < i4ArraySize) \
              {                                     \
                au1BitArray[u2BitNumberBytePos] = (UINT1)(au1BitArray[u2BitNumberBytePos] \
                               | gau1BitMaskMap[u2BitNumberBitPos]);\
              }                                     \
           }


/* 
 * The macro BITLIST_RESET_BIT (), removes u2BitNumber from the au1BitArray. 
 * Warning!!! - Do not call the macro with u2BitNumber as 0 or Invalid 
 * bit number.
 */
#define OSIX_BITLIST_RESET_BIT(au1BitArray, u2BitNumber, i4ArraySize) \
              {\
                 UINT2 u2BitNumberBytePos;\
                 UINT2 u2BitNumberBitPos;\
                 u2BitNumberBytePos = (UINT2)(u2BitNumber / BITS_PER_BYTE);\
                 u2BitNumberBitPos  = (UINT2)(u2BitNumber % BITS_PER_BYTE);\
             if (u2BitNumberBitPos  == 0) {u2BitNumberBytePos = (UINT2)(u2BitNumberBytePos - 1);} \
                 \
                 if (u2BitNumberBytePos < i4ArraySize) \
                 {                                     \
                     au1BitArray[u2BitNumberBytePos] = (UINT1)(au1BitArray[u2BitNumberBytePos] \
                               & ~gau1BitMaskMap[u2BitNumberBitPos]);\
                 }                                     \
              }

#define OSIX_COPY_STRING(pu1Dst, pu1Src, u4DstLen)      \
        if (STRLEN (pu1Src) < u4DstLen)                 \
        {                                               \
            STRCPY (pu1Dst, pu1Src);                    \
        }                                               \
        else                                            \
        {                                               \
            MEMCPY (pu1Dst, pu1Src, u4DstLen - 1);      \
            pu1Dst [u4DstLen - 1] = '\0';               \
        }





#define OSIX_ENABLED  1
#define OSIX_DISABLED 2

#endif
