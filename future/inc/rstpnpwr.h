/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: rstpnpwr.h,v 1.4 2016/08/20 09:40:10 siva Exp $
 *
 * Description:This file contains the necessary data strucutures
 *             for packet handling in NPAPI layer
 *             <Part of dual node stacking model>
 *
 *******************************************************************/

#ifndef __RSTP_NP_WR_H__
#define __RSTP_NP_WR_H__

#include "rstp.h"
#include "fsvlan.h"
#ifdef MBSM_WANTED
#include "mbsm.h"
#endif /* MBSM_WANTED */

/* OPER ID */
enum
{
 FS_MI_STP_NP_HW_INIT = 1,
 FS_MI_RSTP_NP_INIT_HW,
 FS_MI_RSTP_NP_SET_PORT_STATE,
#ifdef MBSM_WANTED
 FS_MI_RSTP_MBSM_NP_SET_PORT_STATE,
 FS_MI_RSTP_MBSM_NP_INIT_HW,
#endif /* MBSM_WANTED */
 FS_MI_RST_NP_GET_PORT_STATE,
#ifdef PB_WANTED
 FS_MI_PB_RSTP_NP_SET_PORT_STATE,
 FS_MI_PB_RST_NP_GET_PORT_STATE,
#ifdef MBSM_WANTED
 FS_MI_PB_RSTP_MBSM_NP_SET_PORT_STATE,
#endif /* MBSM_WANTED */
#endif /*PB_WANTED */
#ifdef PBB_WANTED
 FS_MI_PBB_RST_HW_SERVICE_INSTANCE_PT_TO_PT_STATUS,
#endif /*PBB_WANTED */
 FS_NP_RSTP_MAX_NP
};
/* Required arguments list for the rstp NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

typedef struct {
    UINT4  u4ContextId;
} tRstpNpWrFsMiStpNpHwInit;

typedef struct {
    UINT4  u4ContextId;
} tRstpNpWrFsMiRstpNpInitHw;

typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4ContextId;
    UINT1  u1Status;
    UINT1  au1Pad[7];
} tRstpNpWrFsMiRstpNpSetPortState;

#ifdef MBSM_WANTED
typedef struct {
    UINT4            u4IfIndex;
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
    UINT1            u1Status;
 UINT1            au1Pad[3];
} tRstpNpWrFsMiRstpMbsmNpSetPortState;

typedef struct {
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
} tRstpNpWrFsMiRstpMbsmNpInitHw;

#endif /* MBSM_WANTED */
typedef struct {
    UINT4    u4IfIndex;
    UINT4    u4ContextId;
    UINT1 *  pu1Status;
} tRstpNpWrFsMiRstNpGetPortState;

#ifdef PB_WANTED
typedef struct {
    UINT4    u4IfIndex;
    UINT4    u4ContextId;
    tVlanId  SVid;
    UINT1    u1Status;
 UINT1    au1Pad[1];
} tRstpNpWrFsMiPbRstpNpSetPortState;

typedef struct {
    UINT4    u4IfIndex;
    UINT4    u4ContextId;
    UINT1 *  pu1Status;
    tVlanId  SVlanId;
 UINT1    au1Pad[2];
} tRstpNpWrFsMiPbRstNpGetPortState;

#ifdef MBSM_WANTED
typedef struct {
    UINT4            u4IfIndex;
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
    tVlanId          Svid;
    UINT1            u1Status;
 UINT1            au1Pad[1];
} tRstpNpWrFsMiPbRstpMbsmNpSetPortState;

#endif /* MBSM_WANTED */
#endif /*PB_WANTED */
#ifdef PBB_WANTED
typedef struct {
    UINT4  u4VipIndex;
    UINT4  u4ContextId;
    UINT4  u4Isid;
    UINT1  u1PointToPointStatus;
 UINT1  au1Pad[3];
} tRstpNpWrFsMiPbbRstHwServiceInstancePtToPtStatus;

#endif /*PBB_WANTED */
typedef struct RstpNpModInfo {
union {
    tRstpNpWrFsMiStpNpHwInit  sFsMiStpNpHwInit;
    tRstpNpWrFsMiRstpNpInitHw  sFsMiRstpNpInitHw;
    tRstpNpWrFsMiRstpNpSetPortState  sFsMiRstpNpSetPortState;
#ifdef MBSM_WANTED
    tRstpNpWrFsMiRstpMbsmNpSetPortState  sFsMiRstpMbsmNpSetPortState;
    tRstpNpWrFsMiRstpMbsmNpInitHw  sFsMiRstpMbsmNpInitHw;
#endif /* MBSM_WANTED */
    tRstpNpWrFsMiRstNpGetPortState  sFsMiRstNpGetPortState;
#ifdef PB_WANTED
    tRstpNpWrFsMiPbRstpNpSetPortState  sFsMiPbRstpNpSetPortState;
    tRstpNpWrFsMiPbRstNpGetPortState  sFsMiPbRstNpGetPortState;
#ifdef MBSM_WANTED
    tRstpNpWrFsMiPbRstpMbsmNpSetPortState  sFsMiPbRstpMbsmNpSetPortState;
#endif /* MBSM_WANTED */
#endif /*PB_WANTED */
#ifdef PBB_WANTED
    tRstpNpWrFsMiPbbRstHwServiceInstancePtToPtStatus  sFsMiPbbRstHwServiceInstancePtToPtStatus;
#endif /*PBB_WANTED */
    }unOpCode;

#define  RstpNpFsMiStpNpHwInit  unOpCode.sFsMiStpNpHwInit;
#define  RstpNpFsMiRstpNpInitHw  unOpCode.sFsMiRstpNpInitHw;
#define  RstpNpFsMiRstpNpSetPortState  unOpCode.sFsMiRstpNpSetPortState;
#ifdef MBSM_WANTED
#define  RstpNpFsMiRstpMbsmNpSetPortState  unOpCode.sFsMiRstpMbsmNpSetPortState;
#define  RstpNpFsMiRstpMbsmNpInitHw  unOpCode.sFsMiRstpMbsmNpInitHw;
#endif /* MBSM_WANTED */
#define  RstpNpFsMiRstNpGetPortState  unOpCode.sFsMiRstNpGetPortState;
#ifdef PB_WANTED
#define  RstpNpFsMiPbRstpNpSetPortState  unOpCode.sFsMiPbRstpNpSetPortState;
#define  RstpNpFsMiPbRstNpGetPortState  unOpCode.sFsMiPbRstNpGetPortState;
#ifdef MBSM_WANTED
#define  RstpNpFsMiPbRstpMbsmNpSetPortState  unOpCode.sFsMiPbRstpMbsmNpSetPortState;
#endif /* MBSM_WANTED */
#endif /*PB_WANTED */
#ifdef PBB_WANTED
#define  RstpNpFsMiPbbRstHwServiceInstancePtToPtStatus  unOpCode.sFsMiPbbRstHwServiceInstancePtToPtStatus;
#endif /* PBB_WANTED */
} tRstpNpModInfo;


/* Function Prototype for Generic NP OpCode specific wrappers
 * defined in Rstpnpapi.c */

UINT1 RstpFsMiStpNpHwInit PROTO ((UINT4 u4ContextId));
UINT1 RstpFsMiRstpNpInitHw PROTO ((UINT4 u4ContextId));
UINT1 RstpFsMiRstpNpSetPortState PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1Status));
#ifdef MBSM_WANTED
UINT1 RstpFsMiRstpMbsmNpSetPortState PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1Status, tMbsmSlotInfo * pSlotInfo));
UINT1 RstpFsMiRstpMbsmNpInitHw PROTO ((UINT4 u4ContextId, tMbsmSlotInfo * pSlotInfo));
#endif /* MBSM_WANTED */
UINT1 RstpFsMiRstNpGetPortState PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 * pu1Status));
#ifdef PB_WANTED
UINT1 RstpFsMiPbRstpNpSetPortState PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId SVid, UINT1 u1Status));
UINT1 RstpFsMiPbRstNpGetPortState PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId SVlanId, UINT1 * pu1Status));
#ifdef MBSM_WANTED
UINT1 RstpFsMiPbRstpMbsmNpSetPortState PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId Svid, UINT1 u1Status, tMbsmSlotInfo * pSlotInfo));
#endif /* MBSM_WANTED */
#endif /*PB_WANTED */
#ifdef PBB_WANTED
UINT1 RstpFsMiPbbRstHwServiceInstancePtToPtStatus PROTO ((UINT4 u4ContextId, UINT4 u4VipIndex, UINT4 u4Isid, UINT1 u1PointToPointStatus));
#endif /*PBB_WANTED */

#endif /* __RSTP_NP_WR_H__ */
