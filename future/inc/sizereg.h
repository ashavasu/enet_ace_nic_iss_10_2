/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: sizereg.h,v 1.94 2017/05/29 13:38:21 siva Exp $
 *
 * Description : This file contains the Macors and Structures that    
 *               required for passing data across User & Kernel Space
 *               via ioctl () requests
 *******************************************************************/
#ifndef _SIZEREG_H_
#define _SIZEREG_H_

#include "cli.h"
/* Typedefs */
#define   ISS_SIZING_FILE_NAME    FLASH "system.size"


#ifdef _ISSSZ_C
/* Prototypes */
#ifdef ISS_WANTED
extern INT4  SystemSzRegisterModuleSizingParams( CHR1 *pu1ModName);
extern INT4  MsrSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif
#ifdef IGS_WANTED
extern INT4  SnpSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif
#ifdef DCBX_WANTED
extern INT4  DcbxSzRegisterModuleSizingParams( CHR1 *pu1ModName);
extern INT4  EtsSzRegisterModuleSizingParams( CHR1 *pu1ModName);
extern INT4  PfcSzRegisterModuleSizingParams( CHR1 *pu1ModName);
extern INT4  AppSzRegisterModuleSizingParams( CHR1 *pu1ModName);
extern INT4  CeeSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif
#ifdef CN_WANTED
extern INT4  CnSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif
#ifdef RBRG_WANTED
extern INT4  RbrgSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif
#ifdef LLDP_WANTED
extern INT4  LldpSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif
#ifdef ELPS_WANTED
extern INT4  ElpsSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif
#ifdef LA_WANTED
extern INT4 LaSzRegisterModuleSizingParams(CHR1 *pu1ModName);
#if defined (DLAG_WANTED) || defined (ICCH_WANTED)
extern INT4 LaDlagSzRegisterModuleSizingParams(CHR1 *pu1ModName);
#endif
#endif
#ifdef CFA_WANTED
extern INT4  IpdbSzRegisterModuleSizingParams( CHR1 *pu1ModName);
extern INT4  L2dsSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif
#ifdef SNTP_WANTED
extern INT4   SntpSzRegisterModuleSizingParams(CHR1 *pu1ModName);
#endif 
#ifdef VCM_WANTED
extern INT4   VcmSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif
#ifdef OSPF_WANTED
extern INT4   OspfSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif
#ifdef OSPFTE_WANTED
extern INT4 OspfteSzRegisterModuleSizingParams (CHR1 *pu1ModName);
extern INT4 OstermSzRegisterModuleSizingParams (CHR1 *pu1ModName);
#endif
#ifdef HOTSPOT2_WANTED
extern INT4 Hotspot2SzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif


#ifdef ISIS_WANTED
extern INT4   IsisSzRegisterModuleSizingParams(CHR1 *pu1ModName);
#endif
  
#ifdef RIP_WANTED
extern INT4   RipSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif

#ifdef TACACS_WANTED
extern INT4   TacacsSzRegisterModuleSizingParams(CHR1 *pu1ModName);
#endif

#ifdef RADIUS_WANTED
extern INT4   RadSzRegisterModuleSizingParams(CHR1 *pu1ModName);
#endif  

#ifdef VRRP_WANTED
extern INT4   VrrpSzRegisterModuleSizingParams(CHR1 *pu1ModName);
#endif  

#ifdef PTP_WANTED
extern INT4   PtpSzRegisterModuleSizingParams (CHR1 *pu1ModName);
#endif

#ifdef NAT_WANTED
extern INT4   NatSzRegisterModuleSizingParams(CHR1 *pu1ModName);
#endif  
#ifdef FIREWALL_WANTED
extern INT4   FirewallSzRegisterModuleSizingParams(CHR1 *pu1ModName);
#endif
#ifdef IKE_WANTED
extern INT4   IkeSzRegisterModuleSizingParams (CHR1 * pu1ModName);
#endif 
#ifdef VPN_WANTED
extern INT4   VpnSzRegisterModuleSizingParams (CHR1 * pu1ModName);
#endif 
#ifdef EOAM_WANTED 
extern INT4  EoamSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif 
#ifdef DHCPC_WANTED
extern INT4   DhcSzRegisterModuleSizingParams(CHR1 *pu1ModName);
#endif

#ifdef MRP_WANTED
extern INT4   MrpSzRegisterModuleSizingParams (CHR1 *pu1ModName);
#endif

#ifdef DHCP_RLY_WANTED
extern INT4   DhrlSzRegisterModuleSizingParams(CHR1 *pu1ModName);
#endif

#ifdef DHCP_SRV_WANTED
extern INT4   DhcpsrvSzRegisterModuleSizingParams(CHR1 *pu1ModName);
#endif

#ifdef ROUTEMAP_WANTED
extern INT4   RmapSzRegisterModuleSizingParams(CHR1 *pu1ModName);
#endif

#ifdef RMON_WANTED
extern INT4 RmonSzRegisterModuleSizingParams (CHR1 *pu1ModName);
#endif

#ifdef RMON2_WANTED
extern INT4 Rmon2SzRegisterModuleSizingParams (CHR1 *pu1ModName);
#endif

#ifdef BGP_WANTED
extern INT4   BgpSzRegisterModuleSizingParams(CHR1 *pu1ModName);
#endif

#ifdef OSPF3_WANTED
extern INT4   Ospfv3SzRegisterModuleSizingParams(CHR1 *pu1ModName);
#endif

#ifdef RSTP_WANTED
extern INT4   AstSzRegisterModuleSizingParams (CHR1 *pu1ModName);
#endif

#ifdef IGMP_WANTED
extern INT4   IgmpSzRegisterModuleSizingParams(CHR1 *pu1ModName);
#endif

#ifdef IGMPPRXY_WANTED
extern INT4   IgpSzRegisterModuleSizingParams(CHR1 *pu1ModName);
#endif

#ifdef DVMRP_WANTED
extern INT4   DvmrpSzRegisterModuleSizingParams(CHR1 *pu1ModName);
#endif

#ifdef RIP6_WANTED
extern INT4   Rip6SzRegisterModuleSizingParams(CHR1 *pu1ModName);
#endif

#ifdef ARP_WANTED
extern INT4   ArpSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif

#ifdef IP_WANTED
extern INT4   RtmSzRegisterModuleSizingParams(CHR1 *pu1ModName);
#endif

#ifdef IP_WANTED  
extern INT4   IpSzRegisterModuleSizingParams(CHR1 *pu1ModName);
extern INT4   RarpSzRegisterModuleSizingParams( CHR1 *pu1ModName);
extern INT4   UdpSzRegisterModuleSizingParams(CHR1 *pu1ModName);
#endif

#ifdef DHCP6_CLNT_WANTED
INT4   D6clSzRegisterModuleSizingParams(CHR1 *pu1ModName);
#endif

#ifdef DHCP6_SRV_WANTED
INT4   D6srSzRegisterModuleSizingParams(CHR1 *pu1ModName);
#endif

#ifdef DHCP6_RLY_WANTED
INT4   D6rlSzRegisterModuleSizingParams(CHR1 *pu1ModName);
#endif

#ifdef PNAC_WANTED
extern INT4  PnacSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif

#ifdef PIM_WANTED
extern INT4   PimSzRegisterModuleSizingParams(CHR1 *pu1ModName);
#endif

#ifdef IP6_WANTED
extern INT4   Ip6SzRegisterModuleSizingParams (CHR1 *pu1ModName);
extern INT4   Rtm6SzRegisterModuleSizingParams(CHR1 *pu1ModName);
extern INT4   Ping6SzRegisterModuleSizingParams(CHR1 *pu1ModName);
#ifdef IPSECv6_WANTED
extern INT4   Secv6SzRegisterModuleSizingParams(CHR1 *pu1ModName);
#endif /* IPSECv6_WANTED */ 
#endif /* IP6_WANTED */ 
#ifdef MFWD_WANTED
extern INT4  MfwdSzRegisterModuleSizingParams (CHR1 *pu1ModName);
#endif
#endif

#ifdef OPENFLOW_WANTED
extern INT4    OfcIndexmgrSzRegisterModuleSizingParams (CHR1 *pu1ModName);
extern INT4    OfcSzRegisterModuleSizingParams (CHR1 *pu1ModName);
#endif
#ifdef TLM_WANTED
extern INT4   TlmSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif

#ifdef MPLS_WANTED
extern INT4   MplsdbSzRegisterModuleSizingParams( CHR1 *pu1ModName);
extern INT4   TeSzRegisterModuleSizingParams( CHR1 *pu1ModName);
extern INT4   L2vpnSzRegisterModuleSizingParams( CHR1 *pu1ModName);
extern INT4   LdpSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
extern INT4   RsvpteSzRegisterModuleSizingParams( CHR1 *pu1ModName);
extern INT4   MplsrtrSzRegisterModuleSizingParams( CHR1 *pu1ModName);
extern INT4   IndexmgrSzRegisterModuleSizingParams( CHR1 *pu1ModName);
extern INT4   LblmgrSzRegisterModuleSizingParams( CHR1 *pu1ModName);
extern INT4   TcSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#ifdef LSPP_WANTED
extern INT4   LsppSzRegisterModuleSizingParams (CHR1 *pu1ModName);
#endif
#ifdef RFC6374_WANTED
extern INT4   R6374SzRegisterModuleSizingParams (CHR1 *pu1ModName);
#endif
#ifdef LANAI_WANTED
extern INT4   LanaiSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif
extern INT4   MplsoamSzRegisterModuleSizingParams ( CHR1 *pu1ModName);
#endif

#ifdef IPVX_WANTED
extern INT4  TrcrtSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif

#ifdef BFD_WANTED
extern INT4  BfdSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif

#ifdef ECFM_WANTED
extern INT4  EcfmSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif

#ifdef ELMI_WANTED
extern INT4  ElmiSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif

#ifdef IDS_WANTED
extern INT4 SecidsSzRegisterModuleSizingParams(CHR1 *pu1ModName);
#endif

#ifdef PBBTE_WANTED 
extern INT4  PbbteSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif 

#ifdef EVCPRO_WANTED 
extern INT4  EvcproSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif

#ifdef ERPS_WANTED
extern INT4  ErpsSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif

#ifdef ESAT_WANTED
extern INT4  EsatSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif

#ifdef GARP_WANTED
extern INT4  GarpSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif

#ifdef PBB_WANTED
extern INT4  PbbSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif

#ifdef DNS_WANTED
extern INT4  DnsSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif 

#ifdef MSDP_WANTED
extern INT4  MsdpSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif

#ifdef VLAN_WANTED
extern INT4  VlanSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#ifdef L2RED_WANTED
extern INT4  VlanredSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif
#endif

#ifdef SLI_WANTED
extern INT4  SliSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif

#ifdef TCP_WANTED
extern INT4  TcpSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif

#ifdef BEEP_SERVER_WANTED
extern INT4 Beep_serverSzRegisterModuleSizingParams( CHR1 *pu1ModName );
#endif

#ifdef BEEP_CLIENT_WANTED
extern INT4 Beep_clientSzRegisterModuleSizingParams( CHR1 *pu1ModName );
#endif


extern INT4  TriSzRegisterModuleSizingParams( CHR1 *pu1ModName);

extern INT4  TrieSzRegisterModuleSizingParams( CHR1 *pu1ModName);

extern INT4  UtilSzRegisterModuleSizingParams( CHR1 *pu1ModName);

#ifdef RM_WANTED
extern INT4  RmSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif
#ifdef SYSLOG_WANTED
extern INT4  SyslogSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif
#ifdef CLI_WANTED
extern INT4  CliSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif
#ifdef SNMP_3_WANTED
extern INT4  Snmpv3SzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif
#ifdef WINTEGRA
extern INT4  WintegraSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif
#ifdef CFA_WANTED
extern INT4 CfaSzRegisterModuleSizingParams (CHR1 *pu1ModName);
#endif
#ifdef SYNCE_WANTED
extern INT4  SynceSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif
#ifdef MEF_WANTED
extern INT4  MefSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif
#ifdef BCMX_WANTED
extern INT4  BcmxSzRegisterModuleSizingParams ( CHR1 *pu1ModName);
#endif
#ifdef QOSX_WANTED
extern INT4  QosxSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif
#ifdef MBSM_WANTED
extern INT4  MbsmSzRegisterModuleSizingParams (CHR1 *pu1ModName);
#endif
#ifdef VXLAN_WANTED
extern INT4  VxlanSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif

#ifdef Y1564_WANTED
extern INT4 Y1564SzRegisterModuleSizingParams (CHR1 *pu1ModName);
#endif

UINT4 IssSzGetSizingParamsForModule (CHR1 * pu1ModName, CHR1 * pu1MacroName);
UINT4 IssSzGetSizingMacroValue (CHR1 *pu1MacroName);
INT4 IssSzRegisterAllModuleSizingParams(VOID );
INT4 IssSzRegisterModuleSizingParams( CHR1 *pu1ModName, tFsModSizingParams *pModSizingParams);
INT4 IssSzRegisterModulePoolId( CHR1 *pu1ModName, tMemPoolId *pModPoolId);
INT4 IssSzUpdateDefaultSizingInfo (VOID );
void IssSzUpdateSizingParams( FILE *fp);
FILE *IssSzOpenFile( CHR1 *fname);
void IssSzGenerateCsvFiles ( tCliHandle CliHandle);
void IssSzShowModuleMemory ( tCliHandle CliHandle, CHR1 * pu1ModName);
void IssSzShowIssMemoryStatus ( tCliHandle CliHandle);
void IssSzShowModuleName ( tCliHandle CliHandle);
/*Generate Hitless Restart CSV File*/
VOID IssSzGenerateHRCsvFiles (tCliHandle CliHandle);
INT4 IssSzUpdateSizingInfoForHR (CHR1 *pu1ModName, CHR1 *pu1StName, UINT4 u4BulkUnitSize);
INT4 IssSzInitSizingInfoForHR (VOID);

#ifdef WSSUSER_WANTED
extern INT4 WssuserSzRegisterModuleSizingParams(CHR1 *pu1ModName);
#endif

#ifdef CAPWAP_WANTED
extern INT4  CapwapSzRegisterModuleSizingParams(CHR1 *);
#endif

#if ((defined (WLC_WANTED)) || (defined (WTP_WANTED)))
extern INT4 WsspmSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif

#ifdef WTP_WANTED
extern INT4  AphdlrSzRegisterModuleSizingParams(CHR1 *);
extern INT4  WssifstawtpSzRegisterModuleSizingParams  (CHR1 *pu1ModName);
extern INT4  WssifwtpwlanSzRegisterModuleSizingParams (CHR1 *pu1ModName);
#endif
#ifdef WLC_WANTED
extern INT4  WlchdlrSzRegisterModuleSizingParams( CHR1 *pu1ModName);
extern INT4  WssifauthSzRegisterModuleSizingParams ( CHR1 *pu1ModName );
extern INT4  WssifwebauthSzRegisterModuleSizingParams ( CHR1 *pu1ModName );
extern INT4  WssifwlcwlanSzRegisterModuleSizingParams ( CHR1 *pu1ModName );
#endif

#ifdef RSNA_WANTED
extern INT4   RsnaSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif

#ifdef BCNMGR_WANTED
extern INT4   BcnmgrSzRegisterModuleSizingParams ( CHR1 *pu1ModName );
#endif

#ifdef RFMGMT_WANTED
#ifdef LNXWIRELESS_WANTED
extern INT4  NpEventSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif
#ifdef WLC_WANTED
extern INT4  RfmgmtacSzRegisterModuleSizingParams (CHR1 *pu1ModName);
#endif
extern INT4  RfmgmtSzRegisterModuleSizingParams ( CHR1 *pu1ModName );
#endif

#ifdef WSSAUTH_WANTED
extern INT4  WssauthSzRegisterModuleSizingParams ( CHR1 *pu1ModName );
#endif

#ifdef WSSCFG_WANTED
extern INT4  WsscfgSzRegisterModuleSizingParams ( CHR1 *pu1ModName );
#endif

#ifdef WSSIF_WANTED
extern INT4  WssifcapSzRegisterModuleSizingParams ( CHR1 *pu1ModName );
extern INT4  WssifradioSzRegisterModuleSizingParams ( CHR1 *pu1ModName );
#endif

#ifdef WSSSTA_WANTED
extern INT4  WssstawlcSzRegisterModuleSizingParams ( CHR1 *pu1ModName );
extern INT4  WssstawebSzRegisterModuleSizingParams ( CHR1 *pu1ModName );
#endif

#ifdef WEBNM_WANTED
extern INT4 WebnmSzRegisterModuleSizingParams (CHR1 * pu1ModName);
#endif
#endif /* _SIZEREG_H_ */

/* END OF FILE */
