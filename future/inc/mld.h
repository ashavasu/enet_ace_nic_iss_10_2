
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mld.h,v 1.21 2015/02/13 11:13:57 siva Exp $
 *
 * Description:
 *
 *******************************************************************/
#ifndef _MLD_H_
#define _MLD_H_

#define MLD_IO_MODULE               1
#define MLD_QRY_MODULE              2
#define MLD_GRP_MODULE              4
#define MLD_TMR_MODULE              8
#define MLD_NP_MODULE      16
#define MLD_INIT_SHUT_MODULE     32
#define MLD_OS_RES_MODULE     64
#define MLD_BUFFER_MODULE     128 
#define MLD_ENTRY_MODULE     256
#define MLD_EXIT_MODULE      512
#define MLD_MGMT_MODULE             1024
#define MLD_ALL_MODULES            0x00ffffff
#define MLD_MAX_SOURCES            10

#define MLD_DATA_PATH_MODULE        1
#define MLD_CONTROL_PLANE_MODULE    2
#define MLD_DUMPRx_MODULE           4
#define MLD_DUMPTx_MODULE           8


typedef struct {
    UINT4      u4IfIndex;                  /* I/f Index                        */
    tIp6Addr   GrpAddr;                    /* Group Address                    */
    tIp6Addr   SrcAddr[MLD_MAX_SOURCES]; /* Array of Sources                 */
    UINT1      u1Flag;                     /* possible states Join/Leave       */
    UINT1      u1GrpFilterMode;            /* Include/Exclude                  */
    UINT1      u1NumOfSrcs;                /* Actual Num. Sources in the Array */
    UINT1      u1AlignmentByte;
} tGrp6NoteMsg;

typedef VOID (*MLDHlRegFn)(UINT2, tIp6Addr, UINT4);
typedef VOID (*MLDv2HlRegFn)(tGrp6NoteMsg *pGrpInfo);

VOID MLDTaskMain       PROTO((INT1 *pDummy ));

INT1 MLDInit           PROTO((VOID));

INT1 MLDHlReg          PROTO((UINT2 u2Proto, MLDHlRegFn fnptr));

INT1 MLDv2HlReg        PROTO((UINT2 u2Proto, MLDv2HlRegFn fnptr));

INT1 MLDHlDeReg        PROTO((UINT2 u2Proto));

INT4 MLDStartListening PROTO((UINT4 u4IfIndex, tIp6Addr *McastAddr));

INT4 MLDStopListening  PROTO((UINT4 u4IfIndex, tIp6Addr *McastAddr));

VOID MLDInput PROTO((tNetIpv6HliParams * pIp6HliParams));

INT1 MLDIsCacheEntryPresent PROTO((tIp6Addr * Ip6Addr, UINT4 u4IfIndex));

VOID
MldPortHandleIncomingPkt (tNetIpv6HliParams * pParams );
INT1
MldPortRegisterMRP  (UINT1 u1PktType,
                     VOID (*pLJNoteFuncPtr) (UINT2, tIp6Addr, UINT4));
INT1
MldPortDeRegisterMRP (UINT1 u1PktType);

INT1
MLDGetGrpInterfacesList(tIp6Addr * pSrvAddr ,UINT4 *au1InterfaceList );


#endif
