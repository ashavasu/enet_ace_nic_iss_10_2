/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cli.h,v 1.254 2017/11/30 14:02:06 siva Exp $
 *
 * Description:
 *     This file contains the exported definitions and  macros of CLI.
 *
 *******************************************************************/

#ifndef __CLI_H__
#define __CLI_H__ 

#include "fsutil.h"
#include "fssnmp.h"
#include "cust.h"
#ifdef SECURITY_KERNEL_WANTED
#include "seckgen.h"
#endif


#define CLI_SUCCESS              OSIX_SUCCESS
#define CLI_FAILURE              OSIX_FAILURE

#define CLI_NO_ERROR                  SUCCESS
#define CLI_ERROR                     FAILURE
#define CLI_SESSION_TIMEOUT           1800  /* Default timeout 30 mins */
#define CLI_MAX_SESSION_TIMEOUT       18000 /* 5 Hours */
#define CLI_ROOT_PRIVILEGE_ID         "15"
#define CLI_MAX_LEN_PASSWD            20
#define CLI_MAX_LEN_USERNAME          CLI_MAX_LEN_PASSWD

/* Macros to Define CLI Session Mode */
#define    CLI_MODE_CONSOLE           1
#define    CLI_MODE_TELNET            2
#define    CLI_MODE_SSH               3
#define    CLI_MODE_APP               4
#define    CLI_HW_MODE                "hw-console"
#define    MAX_HW_CMD_LEN             255
#define    CLI_PW_MODE                "pw-console"
#define    MAX_PW_CMD_LEN             255
#define    CLI_SESSION_MODE           "session-console"
#define    MAX_SESSION_CMD_LEN        255
#define    CLI_XCONNECT_MODE          "xconnect-console"
#define    MAX_XCONNECT_CMD_LEN       255

/* max no of variable inputs to cli parser */
#define CLI_MAX_ARGS                       10

#define MAC_LEN                             6
#define MMI_GEN_PASSWD                     44
#define MAX_PROMPT_LEN                    112
#if defined MAX_ADDR_LEN
#undef MAX_ADDR_LEN
#endif
#define MAX_ADDR_LEN                       32
#define MAX_TFTP_FILENAME_LEN             128
#define MAX_FLASH_FILENAME_LEN            128
#define MAX_FILEPATH_LEN            128
#define PORT_FAILURE                       CLI_FAILURE
#define PORT_SUCCESS                       CLI_SUCCESS
#ifdef EVPN_WANTED
#define MAX_LEN_ETH_SEG_ID                  10 
#endif
#define CLI_ENABLE                          1
#define CLI_DISABLE                         2
#define CLI_FALSE                           1
#define CLI_TRUE                            2
#define HEX_FAILURE                        -1

#define CLI_PORTS_PER_BYTE                  8

#define CLI_RM_HB_MODE_INTERNAL             1
#define CLI_RM_HB_MODE_EXTERNAL             2
#define CLI_RM_RTYPE_HOT                    1
#define CLI_RM_RTYPE_COLD                   2
#define CLI_RM_DTYPE_SHARED                 1
#define CLI_RM_DTYPE_SEPARATE               2

/* Default parameters for Ping */
#define CLI_PING_DEFAULT_RETRY_COUNT        3
#define CLI_PING_DEFAULT_SIZE              32
#define CLI_PING_DEFAULT_TIMEOUT            1
#define CLI_PING_DEFAULT_INTERVAL           1
#define CLI_IP_ADDR_MANUAL                "1"
#define CLI_IP_ADDR_NEGOTIATE             "2"
#define CLI_IP_ADDR_LOCAL                 "3"

/* ppp ipcp compression methods */
#define CLI_IPCP_COMPRESSION_NONE         "1"
#define CLI_IPCP_COMPRESSION_VJ           "2"

#define CLI_PPP_MAX_SECURITY_LEN          256

/* ppp security secrets direction */
#define CLI_LOCAL_TO_REMOTE                 1
#define CHAP_PROTOCOL                  0xc223
#define PAP_PROTOCOL                   0xc023
#define REMOTE_TO_LOC_DIRECTION             2
#define MSCHAP_PROTOCOL                0x0001     /* <- proprietary  */
#define EAP_PROTOCOL                   0xc227
#define PPP_MAX_NAME_LEN               30
#define PPP_MAX_PASS_LEN               30

#define CLI_MAX_WAIT_TIME_FOR_SELECT 1000        /* in micro seconds */
#define CLI_WINDOW_SIZE_MESSAGE      255

/* ppp security status */
#define CLI_SECURITY_INVALID                1
#define CLI_SECURITY_VALID                  2

#define MAX_INPUT_SIZE                  5
#define   PROTO(x)            x
/* Bit-mask to indicate the Ctrl-C bit in tCliParam's u4Flags */
#define CLI_CTRL_BRK_MASK  0x00000001
/* Bit-mask to ioctl call with TIOCGWINSZ has failed or not and 
 * the correspondig bit in tCliParam's u4Flags */
#define CLI_TIOCGWINSZ_MASK  0x00000002


/* Bit-mask to indicate the Ctrl-\ or Ctrl-4 bit in tCliParam's u4Flags */
#define CLI_CTRL_QUIT_MASK  0x00000004

/* Checks whether the Ctrl-C bit in 'x' is set or not */
#define CLI_ISCTRL_BRK_SET(x)  ((x) & CLI_CTRL_BRK_MASK)
/* Checks whether the ioctl call with TIOCGWINSZ failed or not bit
 * in 'x' is set or not */
#define CLI_ISTIOCGWINSZ_SET(x)  ((x) & CLI_TIOCGWINSZ_MASK)

#define CLI_MASK_CONSOLE_SESSION 0x00000001
#define CLI_MASK_TELNET_SESSION  0x00000010
#define CLI_TIME_STR_BUF_SIZE    30
#define RMON_MAX_OID_LEN         100

#define CLI_NUM_SSH_CLIENT_WATCH 5

/* Definition of System Debug level (0-7) with 0 being the higest level */

#define DEBUG_DEBUG_LEVEL         7   /* Used for logging debug messages */

#define DEBUG_INFO_LEVEL          6   /* Used for logging informational
                                         *  messages
                                         */

#define DEBUG_NOTICE_LEVEL        5   /* Used for logging messages that
                                       *  require attention but are not errors
                                       */

#define DEBUG_WARN_LEVEL          4   /* Used for logging warning messages */

#define DEBUG_ERROR_LEVEL         3   /* Used for error messages */

#define DEBUG_CRITICAL_LEVEL      2   /* Used for logging critical errors */

#define DEBUG_ALERT_LEVEL         1   /* Used for logging messages that
                                         * require immediate attention.
                                         */

#define DEBUG_EMERG_LEVEL         0   /* Used for logging messages that are
                                         * equivalent to a panic condition.
                                         */
#define  DEBUG_DEF_LVL_FLAG       8



/* Common interface message between FutureCLI and ProtCLI */
typedef struct 
{
   UINT4 u4Command;        /* Command identifier */ 
   UINT4 u4RespStatus;     /* Response flag */
   UINT1 *pBuffer;         /* Data buffer */
} tCliIntfMsg;

/* CliHandle to be used by other modules */

typedef INT4 tCliHandle;

/* Common CLI parameters used by other modules */
typedef struct
{
   INT1  ai1CliCurPrompt[MAX_PROMPT_LEN];  /* CLI Prompt String */

   /* Bits in the Flag will indicate the present status of the CLI session
    * u4Flags's CLI_CTRL_BRK_MASK bit is identified by CLI_CTRL_BRK_MASK */
   UINT4  u4Flags;

} tCliParams;

#define  CLI_GET_COMMAND(u4Cmd,pMesg) \
                   u4Cmd = ((tCliIntfMsg*)pMesg)->u4Command
                   
#define CLI_SET_COMMAND(pMesg,u4Cmd) \
                   ((tCliIntfMsg*)pMesg)->u4Command = u4Cmd 
   
#define CLI_GET_RESP_STATUS(u4RspStatus,pMesg) \
                   u4RspStatus = ((tCliIntfMsg*)(VOID *)pMesg)->u4RespStatus
                   
#define CLI_SET_RESP_STATUS(pMesg,u4RspStatus) \
                   ((tCliIntfMsg*)(VOID *)pMesg)->u4RespStatus = u4RspStatus

#define CLI_GET_DATA_PTR(pMesg)  ( (UINT1 *)pMesg + 2 * sizeof (UINT4) )       
                  
/* size constants */                   
#define CLI_MAC_ADDR_SIZE             6

#define CLI_MEMCPY(dest,src,len)       MEMCPY( (dest), (src), (len) )
#define CLI_MEMCMP(dest,src,len)       MEMCMP( (dest), (src), (len) )
#define CLI_MEMSET                     MEMSET

#define CLI_STRCPY                     STRCPY
#define CLI_STRNCPY                    STRNCPY
#define CLI_STRCAT                     STRCAT
#define CLI_STRLEN                     STRLEN
#define CLI_STRSTR(x,y)                STRSTR(x,y)

#define CLI_STRCMP                     STRCMP
#define CLI_STRNCMP                    STRNCMP
#define CLI_STRCASECMP                 STRCASECMP

#define CLI_ATOI                       ATOI 
#define CLI_ATOL                       ATOL
#define CLI_ISDIGIT                    ISDIGIT
#define CLI_ISXDIGIT                   ISXDIGIT

#define   CLI_IS_ADDR_CLASS_A(u4Addr)   ((u4Addr &  0x80000000) == 0) 
#define   CLI_IS_ADDR_CLASS_B(u4Addr)   ((u4Addr &  0xc0000000) == 0x80000000)    
#define   CLI_IS_ADDR_CLASS_C(u4Addr)   ((u4Addr &  0xe0000000) == 0xc0000000) 
#define   CLI_IS_ADDR_CLASS_D(u4Addr)   ((u4Addr &  0xf0000000) == 0xe0000000)
#define   CLI_IS_ADDR_CLASS_E(u4Addr)   ((u4Addr &  0xf0000000) == 0xf0000000)
#define   CLI_IS_ADDR_LOOPBACK(u4Addr)  ((u4Addr &  0x7f000000) == 0x7f000000)         
#define CLI_PTR_TO_U4(x) PTR_TO_U4(x)
#define CLI_PTR_TO_I4(x) PTR_TO_I4(x)


#define IP6_INIT_OCT(Ostr,buf)\
    (Ostr).pu1_OctetList = buf;\
    MEMSET( buf , 0 , sizeof(buf) );\
    (Ostr).i4_Length=0

/* clear octet string */
#define IP6_CLEAR_OCT(pOstr,len)\
    MEMSET( (pOstr)->pu1_OctetList , 0 , len );\
    (pOstr)->i4_Length=0


/* convert asciiz string to octet string */
#define IP6_ASCIIZ_2_OCT(dst,src)\
    (dst)->i4_Length = (INT4)STRLEN(src);\
    MEMCPY( (dst)->pu1_OctetList , (src) , (dst)->i4_Length )

/* convert octet (could be no space for ending zero) string to asciiz */
#define IP6_OCT_2_ASCIIZ(dst,src)\
    MEMSET( dst , 0 , sizeof(dst) );\
    MEMCPY( dst , (src)->pu1_OctetList , MEM_MAX_BYTES( IP6_DOMAIN_NAME_LEN,(UINT4) (src)->i4_Length ))


/*Converts hexa string to unsigned long*/
#define CLI_HEXSTRTOUL(x)              strtoul(x,0,16)

#define CLI_NTOHL                      OSIX_NTOHL
#define CLI_INET_ADDR(ipaddr)          CLI_NTOHL( INET_ADDR(ipaddr) )
#define CLI_INET_ATON(s, pin)          UtlInetAton((const CHR1 *)s, (tUtlInAddr *)pin)
#define CLI_INET_NTOA(ipaddr)          UtlInetNtoa(ipaddr)

#define CLI_SHOW_STATUS(Status)        (Status==1)?"Enabled":"Disabled"

#define IS_STANDARD_VLAN_ID(u4Vfi) \
        ((u4Vfi == 4095) || (u4Vfi == 4096) || (u4Vfi == 4097) || \
         (u4Vfi == 65535))

#define CLI_CONVERT_IPADDR_TO_STR(pString, u4Value)\
{\
    tUtlInAddr          IpAddr;\
\
         IpAddr.u4Addr = OSIX_NTOHL (u4Value);\
\
         pString = (CHR1 *)CLI_INET_NTOA (IpAddr);\
\
}

#define CLI_CONVERT_OID_TO_STR(pc1DispOIDStr, OIDValue) \
{\
    /* NOTE: pc1DispOIDStr should contain at least 
     * (OIDValue.u4_Length * 2)Bytes of memory 
     */ \
    UINT4 u4Count      = 0;\
    CHR1 *pc1PrintStr = pc1DispOIDStr;\
    \
    for (u4Count = 0; u4Count < OIDValue.u4_Length; u4Count++) \
    {\
        SPRINTF (pc1PrintStr, "%d.", OIDValue.pu4_OidList[u4Count]); \
        pc1PrintStr = pc1PrintStr + STRLEN(pc1PrintStr);\
    }\
    if(OIDValue.u4_Length != 0)\
    {\
     *(pc1PrintStr-1) = '\0';\
    }\
}\


#define CLI_REVERSE_IP_STR(pInStr) \
{ \
    INT1 i1Char; \
    INT1 i,j ; \
    \
     for (i = 0,j = 3; i < j; i++, j--) \
    { \
       i1Char = pInStr[i];\
       pInStr[i] = pInStr[j];\
       pInStr[j] = i1Char; \
    } \
}

#define CLI_MAX_COLUMN_WIDTH           80
#define CLI_MAX_HEADER_SIZE            (CLI_MAX_COLUMN_WIDTH * 10)

#define CLI_CONVERT_VAL_TO_STR(pString, u4Value) \
MEMCPY(pString,&u4Value,sizeof(UINT4));

/* maximum number of chars written on the cli console */
#define   MAX_CLI_OUTPUT_BUF_SIZE         4098
/*
 * NOTE: MAX_CLI_OUTPUT_PEND_BUF_SIZE MUST be greater or equal 
 *       to MAX_CLI_OUTPUT_BUF_SIZE
 */
#define   MAX_CLI_OUTPUT_PEND_BUF_SIZE    MAX_CLI_OUTPUT_BUF_SIZE

/* The following macros are used in inc/cli/<mod>cli.h file.
 * The reason for having unique starting identifier for enum is,
 * MEF is interacting with different modules low level routines.
 * So the CLI error message that is set in the module's low level
 * routine should be visible to MEF CLI process handle. And hence,
 * MEF can able to fetch the CLI error string from the module's 
 * global error string array.
 */
#define   CLI_ERR_START_ID_CFA          1000
#define   CLI_ERR_START_ID_VLAN         2000
#define   CLI_ERR_START_ID_VLAN_PB      3000
#define   CLI_ERR_START_ID_ECFM         4000
#define   CLI_ERR_START_ID_MPLS_TE      5000
#define   CLI_ERR_START_ID_MPLS         6000
#define   CLI_ERR_START_ID_MPLS_PWRED   7000
#define   CLI_ERR_START_ID_MPLS_PWVPLS  7500
#define   CLI_ERR_START_ID_MPLS_OAM     8000
#define   CLI_ERR_START_ID_QOS          9000
#define   CLI_ERR_START_ID_ISS          10000
#define   CLI_ERR_START_ID_VCM          11000
#define   CLI_ERR_START_ID_MPLS_LDP     12000

/*For Trace Levels for IP,firewall,dns,bridge modules*/
#define   CLI_INIT_SHUT_TRC       0x00000001
#define   CLI_MGMT_TRC            0x00000002
#define   CLI_DATA_PATH_TRC       0x00000004 
#define   CLI_CONTROL_PLANE_TRC   0x00000008 
#define   CLI_DUMP_TRC            0x00000010 
#define   CLI_OS_RESOURCE_TRC     0x00000020 
#define   CLI_ALL_FAILURE_TRC     0x00000040 
#define   CLI_BUFFER_TRC          0x00000080 
#define   CLI_ALL_TRC             0xffffffff
#define   CLI_NONE_TRC            0x00000000 

#define   CLI_IP_TRC              0x00010000 
#define   CLI_ICMP_TRC            0x00020000 
#define   CLI_IGMP_TRC            0x00040000 
#define   CLI_RIP_TRC             0x00080000 
#define   CLI_ARP_TRC             0x00100000 
#define   CLI_BOOTP_TRC           0x00200000 
#define   CLI_UDP_TRC             0x00400000 

/*Trace Levels for dhcp modules*/

#define CLI_DEBUG_NONE        "0"
#define CLI_DEBUG_ALL         "1"
#define CLI_DEBUG_EVENTS      "2"
#define CLI_DEBUG_PACKETS     "3"
#define CLI_DEBUG_FAILURE     "4"
#define CLI_DEBUG_BIND        "5"


/*Trace Levels for ppp modules*/
#define CLI_PPP_INIT_DEBUG        "2"
#define CLI_PPP_MGMT_DEBUG        "3"
#define CLI_PPP_DATA_DEBUG        "4"
#define CLI_PPP_CONTROL_DEBUG     "5"
#define CLI_PPP_DUMP_DEBUG        "6"
#define CLI_PPP_OS_DEBUG          "7"
#define CLI_PPP_ALLFAIL_DEBUG     "8"
#define CLI_PPP_BUFFER_DEBUG      "9"
#define CLI_PPP_ALL_DEBUG         "1"
#define CLI_PPP_NONE_DEBUG        "0"

/* max lines displayed per page */
#define CLI_MAX_ROWS              23
#define CLI_MAX_COLS              81
#define CLI_MAX_CONSOLE          (CLI_MAX_ROWS * CLI_MAX_COLS)

#define MAX_ALIAS_LEN             64
#define MAX_LINE_LEN             900

#define  CLI_BLANKSPACE_ASCII    0x20
/* Macro to check the given mac address is a multicast mac or not */
#define CLI_IS_MCASTMAC(pMacAddr) \
  ((FS_UTIL_IS_MCAST_MAC(pMacAddr) == OSIX_TRUE) ? CLI_SUCCESS : CLI_FAILURE)

/* Macro to check the given mac address is a zero mac or not */
#define CLI_IS_NULLMAC(pMacAddr,pZeroMacAddr) \
                       ((CLI_MEMCMP(pMacAddr,pZeroMacAddr,CLI_MAC_ADDR_SIZE))\
                       ? CLI_FAILURE : CLI_SUCCESS)
                           


/* The following macros are provided the mapping for new functions.
 * The protocol modules using these macros need to use the new functions
 */

#define CLI_CONVERT_DOT_STR_TO_MAC     CliDotStrToMac
#define CLI_CONVERT_DOT_STR_TO_ARRAY   CliDotStrToStr
#define CliConvertDotStrToStr          CliDotStrToStr
#define StrToMac                       CliStrToMac
#define OctetLen                       CliOctetLen
#define PrintMacAddress                CliMacToStr
#define AclPrintMacAddress             AclCliMacToStr
#define PrintFourBytesMacAddress       CliMacToFourOctetStr 
#define ConvertHexToDecimal            CliHexStrToDecimal 
#define cli_get_app_port               CliGetServPortByName
#define CLI_CONVERT_MAC_TO_DOT_STR     CliMacToDotStr
#define str_to_ip6addr                 CliStrToIp6Addr
#define mmi_strncmp_without_case       CliStrNCaseCmp

#define CLI_COMMAND_MODE_LEN 50
#define CLI_COMMAND_LEN 900


/* Macros used to get/set the Current CliContext's ErrorCode object parameter */

#ifdef CLI_WANTED
#define CLI_GET_ERR(ErrCode)           CliGetErrorCode (ErrCode)  
#define CLI_SET_ERR(ErrCode)           CliSetErrorCode (ErrCode)
#define CLI_GET_CMD_STATUS(CmdStatus)    CliGetCmdStatus(CmdStatus) 
#define CLI_SET_CMD_STATUS(CmdStatus)    CliSetCmdStatus(CmdStatus)

#else

#ifdef SECURITY_KERNEL_WANTED
#define CLI_GET_ERR(ErrCode)         gi4CliWebSetError;
#define CLI_SET_ERR(ErrCode)          gi4CliWebSetError = ErrCode;
#else
#define CLI_GET_ERR(ErrCode)           CLI_SUCCESS
#define CLI_SET_ERR(ErrCode)           UNUSED_PARAM(ErrCode)
#endif
 
#define CLI_GET_CMD_STATUS(CmdStatus)    CLI_SUCCESS 
#define CLI_SET_CMD_STATUS(CmdStatus)    

#endif

#ifdef CLI_WANTED
#define CLI_FATAL_ERROR(CliHandle)     CliPrintf (CliHandle, "%% Fatal Error: Command Failed\r\n")
#else
#define CLI_FATAL_ERROR(CliHandle)     UNUSED_PARAM(CliHandle)
#endif

/* utility to fetch IfIndex from interface type & interace number */
#if defined(CLI_WANTED) && defined(CFA_WANTED)
#define CliGetIfIndexFromTypeAndPortNum(u4IfType, pi1IfNum, pu4IfIndex)  \
               CfaCliGetIndexFromType(u4IfType, pi1IfNum, pu4IfIndex)
#define CliGetIfIndexFromSubTypeAndPortNum(u4IfType, u4IfSubType, pi1IfNum, pu4IfIndex)  \
               CfaCliGetIndexFromSubType(u4IfType, u4IfSubType, pi1IfNum, pu4IfIndex)
#else
/* Function to be ported by interface manager */
#define CliGetIfIndexFromTypeAndPortNum(u4IfType, pi1IfNum, pu4IfIndex) \
               OSIX_FAILURE
#define CliGetIfIndexFromSubTypeAndPortNum(u4IfType, u4IfSubType, pi1IfNum, pu4IfIndex) \
               OSIX_FAILURE
#endif
#define CLI_ERR_INVALID_INTERFACE_NUM    2

/* Macros used to get/set the Current CliContext's i4PromptInof Object parameter 
 * which will be used by protocol action rotines to keep track of certain Mode info
 */
#define CLI_GET_IFINDEX()                   CliGetModeInfo()  
#define CLI_SET_IFINDEX(IfIndex)            CliSetModeInfo(IfIndex)  

#define CLI_GET_L2TP_PWINDEX()                   CliGetModeInfo()  
#define CLI_SET_L2TP_PWINDEX(IfIndex)            CliSetModeInfo(IfIndex)  


#define CLI_GET_L2TP_SESSIONINDEX()                   CliGetModeInfo()  
#define CLI_SET_L2TP_SESSIONINDEX(IfIndex)            CliSetModeInfo(IfIndex)  

#define CLI_GET_L2TP_XCONNECTINDEX()                   CliGetModeInfo()  
#define CLI_SET_L2TP_XCONNECTINDEX(IfIndex)            CliSetModeInfo(IfIndex)  

#define CLI_GET_PO_INDEX()                  CliGetModeInfo()  
#define CLI_SET_PO_INDEX(IfIndex)           CliSetModeInfo(IfIndex)  

#define CLI_GET_VR_INDEX()                  CliGetModeInfo()  
#define CLI_SET_VR_INDEX(IfIndex)           CliSetModeInfo(IfIndex)  

#define CLI_GET_SISP_INDEX()                  CliGetModeInfo()  
#define CLI_SET_SISP_INDEX(IfIndex)           CliSetModeInfo(IfIndex)  

#define CLI_GET_VLANID()                    CliGetModeInfo()  
#define CLI_SET_VLANID(IfIndex)             CliSetModeInfo(IfIndex)  

#define CLI_GET_UFD_GROUPID()                    CliGetModeInfo()
#define CLI_SET_UFD_GROUPID(IfIndex)             CliSetModeInfo(IfIndex)

#define CLI_GET_TESID()                     CliGetModeInfo()
#define CLI_SET_TESID(TeSid)                CliSetModeInfo(TeSid)

#define CLI_GET_ISID()                    CliGetModeInfo()  
#define CLI_SET_ISID(IfIndex)             CliSetModeInfo(IfIndex) 

#define CLI_GET_PG_ID()                     CliGetModeInfo()
#define CLI_SET_PG_ID(PgId)                 CliSetModeInfo(PgId)

#define CLI_GET_SESS_ID()                     CliGetModeInfo()
#define CLI_SET_SESS_ID(SessId)               CliSetModeInfo(SessId)

#define CLI_GET_LOOPBACKID()                CliGetModeInfo()  
#define CLI_SET_LOOPBACKID(IfIndex)         CliSetModeInfo(IfIndex)  

#define CLI_GET_RING_ID()                     CliGetModeInfo()
#define CLI_SET_RING_ID(RingId)               CliSetModeInfo(RingId)

#define CLI_GET_DHCPPOOLID()                CliGetModeInfo()  
#define CLI_SET_DHCPPOOLID(IfIndex)         CliSetModeInfo(IfIndex)  

#define CLI_GET_STDACLID()                  CliGetModeInfo()  
#define CLI_SET_STDACLID(IfIndex)           CliSetModeInfo(IfIndex)  

#define CLI_GET_EXTACL()                    CliGetModeInfo()  
#define CLI_SET_EXTACL(IfIndex)             CliSetModeInfo(IfIndex)  

#define CLI_GET_IPV6ACL()                   CliGetModeInfo()  
#define CLI_SET_IPV6ACL(IfIndex)            CliSetModeInfo(IfIndex)  

#define CLI_GET_MACACL()                    CliGetModeInfo()  
#define CLI_SET_MACACL(IfIndex)             CliSetModeInfo(IfIndex)  

#define CLI_GET_USERDEFFACL()              CliGetModeInfo()
#define CLI_SET_USERDEFFACL(IfIndex)       CliSetModeInfo(IfIndex)


#define CLI_GET_CLASSMAP_ID()               CliGetModeInfo()  
#define CLI_SET_CLASSMAP_ID(IfIndex)        CliSetModeInfo(IfIndex)  

#define CLI_GET_POLICYMAP_ID()              CliGetModeInfo()  
#define CLI_SET_POLICYMAP_ID(IfIndex)       CliSetModeInfo(IfIndex)  

#define CLI_GET_POCL_MAP_ID()               CliGetModeInfo()  
#define CLI_SET_POCL_MAP_ID(IfIndex)        CliSetModeInfo(IfIndex)  

#define CLI_GET_TRAFFICCLASS_ID()           CliGetModeInfo()
#define CLI_SET_TRAFFICCLASS_ID(TCId)       CliSetModeInfo(TCId)
    
#define CLI_GET_COMPID()                   CliGetModeInfo()  
#define CLI_SET_COMPID(CompId)             CliSetModeInfo(CompId)  

#define CLI_GET_RIP_CXTID()                CliGetModeInfo()  
#define CLI_SET_RIP_CXTID(CxtId)                CliSetModeInfo(CxtId)

#define CLI_GET_CXT_ID()                    CliGetContextIdInfo()
#define CLI_SET_CXT_ID(CxtId)               CliSetContextIdInfo(CxtId)
#define CLI_GET_POOL_INDEX()                 CliGetContextIdInfo()
#define CLI_SET_POOL_INDEX(u4PoolId)         CliSetContextIdInfo(u4PoolId)

#define CLI_GET_VENDOR_INDEX()               CliGetModeInfo()
#define CLI_SET_VENDOR_INDEX(u4VendorId)     CliSetModeInfo(u4VendorId)

#define CLI_GET_SRV_CLNT_INDEX()               CliGetModeInfo()   
#define CLI_SET_SRV_CLNT_INDEX(u4ClientId)     CliSetModeInfo(u4ClientId) 

#define CLI_GET_MPLS_TUNNEL_ID()          CliGetModeInfo()
#define CLI_SET_MPLS_TUNNEL_ID(u4Index)    CliSetModeInfo(u4Index)

#define CLI_GET_MPLS_VFI_ID()              CliGetModeInfo()
#define CLI_SET_MPLS_VFI_ID(i4VfiId)       CliSetModeInfo(i4VfiId)

#define CLI_GET_MPLS_LDP_ENTITY_MODE()            CliGetModeInfo()
#define CLI_SET_MPLS_LDP_ENTITY_MODE(u4EntityIdx) CliSetModeInfo(u4EntityIdx)

#ifdef MPLS_L3VPN_WANTED
#define CLI_GET_MPLS_L3VPN_VRF_MODE()            CliGetModeInfo()
#define CLI_SET_MPLS_L3VPN_VRF_MODE(u4ContextId) CliSetModeInfo(u4ContextId)
#endif

#define CLI_GET_DOMAIN_ID()                   CliGetModeInfo()
#define CLI_SET_DOMAIN_ID(i4DomainId)         CliSetModeInfo(i4DomainId)

/* functions for PW Redundancy */
#define CLI_GET_PW_RG_ID()                    CliGetModeInfo()
#define CLI_SET_PW_ICCP_ID(u4PwRedGrpId)      CliSetModeInfo(u4PwRedGrpId)
#define CLI_SET_PW_RG_ID(u4PwRedGrpId)        CliSetModeInfo(u4PwRedGrpId)
#define CLI_GET_PW_ICCP_ID()                  CliGetModeInfo()



/* Functions for ECFM */
#define CLI_SET_MDINDEX(IfIndex)            CliSetModeInfo(IfIndex)
#define CLI_GET_MDINDEX()                   CliGetModeInfo()
               
/* Macros used by TAC module */
#define CLI_GET_TACM_PROFILE_ID()             CliGetContextIdInfo()
#define CLI_SET_TACM_PROFILE_ID(u4ProfileId)  CliSetContextIdInfo(u4ProfileId)

#define CLI_GET_TACM_ADDRESS_TYPE()           CliGetModeInfo()
#define CLI_SET_TACM_ADDRESS_TYPE(i4AddrType) CliSetModeInfo(i4AddrType)

#define CLI_GET_BLACK_OR_WHITE_LIST_TYPE()           CliGetModeInfo()
#define CLI_SET_BLACK_OR_WHITE_LIST_TYPE(i4ListType) CliSetModeInfo(i4ListType)

#define CLI_GET_GROUP_ID()     CliGetModeInfo()   
#define CLI_SET_GROUP_ID(u4GroupId)  CliSetModeInfo (u4GroupId)
               
#ifdef ISS_WANTED
#define CLI_GET_ISS_SWITCH_NAME()            IssSysGetSwitchName() 
#define CLI_GET_ISS_CLI_PROMPT()             IssGetCustomCliPrompt()
#define CLI_GET_ISS_BANNER(x)                IssGetCliDisplayBanner ((x))
#define CLI_MAX_BANNER_LEN       512 
#else
#define CLI_GET_ISS_SWITCH_NAME()            "iss"
#define CLI_GET_ISS_CLI_PROMPT()             "iss"
#endif /*ISS_WANTED*/

/* Macros added as part of ESAT module */
#define CLI_SET_SLA_ID(i4SlaId)             CliSetModeInfo(i4SlaId)
#define CLI_SET_TRAF_PROF_ID(i4TrafProfId)  CliSetModeInfo(i4TrafProfId)
#define CLI_SET_SERV_CONFIG_ID(i4ServConfId)  CliSetModeInfo(i4ServConfId)
#define CLI_GET_MODE_ID()                   CliGetModeInfo()

/* Macros added as part of RFC2544 module */
#define CLI_SET_RFC2544_SLA_ID(i4SlaId)     CliSetModeInfo(i4SlaId)
#define CLI_SET_RFC2544_TRAFPROF_ID(i4TrafProfId) CliSetModeInfo(i4TrafProfId)
#define CLI_SET_RFC2544_SAC_ID(i4SacId)     CliSetModeInfo(i4SacId)
#define CLI_GET_RFC2544_MODE_ID()           CliGetModeInfo()   

#define CLI_SET_CAPABID(ModeName)           CliSetCurMode(ModeName)  
#define CLI_GET_CAPABID(ModeName)           CliGetCurMode(ModeName)  
#define CLI_SET_RADIUSID(ModeName)           CliSetCurMode(ModeName)  
#define CLI_GET_RADIUSID(ModeName)           CliGetCurMode(ModeName)  
#define CLI_SET_QOSID(ModeName)             CliSetCurMode(ModeName)  
#define CLI_GET_QOSID(ModeName)             CliGetCurMode(ModeName)  
#define CLI_SET_AUTHID(ModeName)            CliSetCurMode(ModeName)  
#define CLI_GET_AUTHID(ModeName)            CliGetCurMode(ModeName)  


#define CLI_SET_POOLID(ModeName)            CliSetPoolId(ModeName)  
#define CLI_GET_POOLID(ModeName)            CliGetPoolId(ModeName)  

#define CLI_SET_NATID(ModeName)            CliSetNatId(ModeName)  
#define CLI_GET_NATID(ModeName)            CliGetNatId(ModeName)  

#define CLI_SET_MODELNAME(ModelName)           CliSetCurMode(ModelName)
#define CLI_GET_MODELNAME(ModelName)           CliGetCurMode(ModelName)
#ifdef HDLC_WANTED
#define CLI_GET_HDLC_ID()                  CliGetModeInfo()
#define CLI_SET_HDLC_ID(HdlcIndex)         CliSetModeInfo(HdlcIndex)
#endif

   enum {
      ISS_DHCP_SHOW_RUNNING_CONFIG = 1,
      ISS_DIFFSERV_SHOW_RUNNING_CONFIG,
      ISS_SNMP_SHOW_RUNNING_CONFIG,
      ISS_SSH_SHOW_RUNNING_CONFIG,
      ISS_SSL_SHOW_RUNNING_CONFIG,
      ISS_SYSLOG_SHOW_RUNNING_CONFIG,
      ISS_INTERFACE_SHOW_RUNNING_CONFIG,
      ISS_IP_SHOW_RUNNING_CONFIG,
      ISS_IGMP_SHOW_RUNNING_CONFIG,
      ISS_IGP_SHOW_RUNNING_CONFIG,
      ISS_DOT1X_SHOW_RUNNING_CONFIG,
      ISS_IGS_SHOW_RUNNING_CONFIG,
      ISS_MLDS_SHOW_RUNNING_CONFIG,
      ISS_RAD_SHOW_RUNNING_CONFIG,
      ISS_RMON_SHOW_RUNNING_CONFIG,
      ISS_STP_SHOW_RUNNING_CONFIG,
      ISS_EOAM_SHOW_RUNNING_CONFIG,
      ISS_FM_SHOW_RUNNING_CONFIG,
      ISS_LA_SHOW_RUNNING_CONFIG,
      ISS_VLAN_SHOW_RUNNING_CONFIG,
      ISS_RIP_SHOW_RUNNING_CONFIG,
      ISS_VRRP_SHOW_RUNNING_CONFIG,
      ISS_DVMRP_SHOW_RUNNING_CONFIG,
      ISS_PIM_SHOW_RUNNING_CONFIG,
      ISS_PIMV6_SHOW_RUNNING_CONFIG,
      ISS_OSPF_SHOW_RUNNING_CONFIG,
      ISS_ISIS_SHOW_RUNNING_CONFIG,
      ISS_BGP_SHOW_RUNNING_CONFIG,
      ISS_IPV6_SHOW_RUNNING_CONFIG,
      ISS_RIP6_SHOW_RUNNING_CONFIG,
      ISS_RM_SHOW_RUNNING_CONFIG,
      ISS_MBSM_SHOW_RUNNING_CONFIG,
      ISS_ACL_SHOW_RUNNING_CONFIG,
      ISS_OSPF3_SHOW_RUNNING_CONFIG,
      ISS_MPLS_SHOW_RUNNING_CONFIG,
      ISS_CONTEXT_SHOW_RUNNING_CONFIG,
      ISS_ECFM_SHOW_RUNNING_CONFIG,
      ISS_ELMI_SHOW_RUNNING_CONFIG,
      ISS_RMAP_SHOW_RUNNING_CONFIG,
      ISS_QOSXTD_SHOW_RUNNING_CONFIG,
      ISS_TACACS_SHOW_RUNNING_CONFIG,
      ISS_TACM_SHOW_RUNNING_CONFIG,
      ISS_BEEP_SERVER_SHOW_RUNNING_CONFIG,
      ISS_SNTP_SHOW_RUNNING_CONFIG,
      ISS_HTTP_SHOW_RUNNING_CONFIG,
      ISS_NAT_SHOW_RUNNING_CONFIG,
      ISS_VRF_SHOW_RUNNING_CONFIG,
      ISS_ELPS_SHOW_RUNNING_CONFIG,
      ISS_ENT_SHOW_RUNNING_CONFIG,
      ISS_DHCP6_SHOW_RUNNING_CONFIG,
      ISS_POE_SHOW_RUNNING_CONFIG,
      ISS_ERPS_SHOW_RUNNING_CONFIG,
      ISS_PBB_SHOW_RUNNING_CONFIG,
      ISS_DCB_SHOW_RUNNING_CONFIG,
      ISS_SHOW_MRP_RUNNING_CONFIG,
      ISS_CN_SHOW_RUNNING_CONFIG,
      ISS_PTP_SHOW_RUNNING_CONFIG,
      ISS_CLKIWF_SHOW_RUNNING_CONFIG,
      ISS_MLD_SHOW_RUNNING_CONFIG,
      ISS_PPP_SHOW_RUNNING_CONFIG,
      ISS_MSDP_SHOW_RUNNING_CONFIG,
      ISS_MSDPV6_SHOW_RUNNING_CONFIG,
      ISS_DNS_SHOW_RUNNING_CONFIG,
      ISS_LLDP_SHOW_RUNNING_CONFIG,
      ISS_FIREWALL_SHOW_RUNNING_CONFIG,
      ISS_SYS_INFO_SHOW_RUNNING_CONFIG,
      ISS_OSPFTE_SHOW_RUNNING_CONFIG,
      ISS_IPSOURCEGUARD_SHOW_RUNNING_CONFIG,
      ISS_TLM_SHOW_RUNNING_CONFIG,
      ISS_RBRG_SHOW_RUNNING_CONFIG,
      ISS_L2DHCSNP_SHOW_RUNNING_CONFIG,
      ISS_MEF_SHOW_RUNNING_CONFIG,
      ISS_WSS_SHOW_RUNNING_CONFIG,
      ISS_SYNCE_SHOW_RUNNING_CONFIG,
   ISS_HS_SHOW_RUNNING_CONFIG,
   ISS_DSMON_SHOW_RUNNING_CONFIG,
      ISS_BFD_SHOW_RUNNING_CONFIG,
   ISS_VPN_SHOW_RUNNING_CONFIG,
   ISS_SECV6_SHOW_RUNNING_CONFIG,
      ISS_OFCL_SHOW_RUNNING_CONFIG,
      ISS_RSNA_SHOW_RUNNING_CONFIG,
      ISS_RFMGMT_SHOW_RUNNING_CONFIG,
      ISS_Y1564_SHOW_RUNNING_CONFIG,
      ISS_VXLAN_SHOW_RUNNING_CONFIG,
      ISS_RMON2_SHOW_RUNNING_CONFIG,
      ISS_WSSUSER_SHOW_RUNNING_CONFIG,
      ISS_WEBAUTH_SHOW_RUNNING_CONFIG,
      ISS_HB_SHOW_RUNNING_CONFIG,
      ISS_ICCH_SHOW_RUNNING_CONFIG,
      ISS_UFD_SHOW_RUNNING_CONFIG,
      ISS_SH_SHOW_RUNNING_CONFIG,
      ISS_FSB_SHOW_RUNNING_CONFIG,
      ISS_EVB_SHOW_RUNNING_CONFIG,
   ISS_RFC2544_SHOW_RUNNING_CONFIG,
      ISS_WPS_SHOW_RUNNING_CONFIG,
      ISS_L2TP_SHOW_RUNNING_CONFIG,
      ISS_WSSLR_SHOW_RUNNING_CONFIG,
      ISS_SHOW_ALL_RUNNING_CONFIG
   };

    
typedef struct
{
    INT1 *pi1NewUsrName;
    INT1 *pi1NewPasswd;
    UINT4 u4EncryptionType;
    INT4 i4PrivilegeLevel;
    INT1 *pi1ConfirmPasswd;
    INT1 i1UsrAdd;
    INT1 i1UserStatus;
    UINT1 au1Pad[2];
}tCliUserInfo;

/* Function prototypes */

/*  Spawns a separate CLI Task - called by the SSH Module */
VOID  CliTaskStart PROTO ((INT1 *));
VOID  CliTelnetTaskMain (INT1 *pi1Param);
VOID  CliSshTaskMain (INT1 *);
VOID  CliRmMain PROTO ((INT1 *pArg));

/*  Checks whether the user belongs to the specified group 
 * - called by the SSH and WEBNM modules
 */
INT1  CliIsUserInGroup PROTO ((INT1 *pi1UserName, INT1 *pi1GrpName));

/*  Reads a Certificate key as input to the CLI - called by IKE module */
INT1  CliReadLine PROTO ((INT1 *pi1Prompt, INT1 *pi1Str, UINT4 u4Len));

/* API provided for other modules to execute CLI commands */
UINT1  CliExecuteCliCmd PROTO ((CONST CHR1 *pc1UserCmd));

/*Function Prototypes for CLI Application Context */
UINT1  CliExecuteAppCmd PROTO ((CONST CHR1 *pc1UserCmd));
INT2 CliApplicationContextInit (VOID);
INT2 CliGiveAppContext(VOID);
INT2 CliTakeAppContext(VOID);
INT2 CliResetConsoleContextTaskId (VOID);
INT2 CliSetConsoleContextTaskId (VOID);

/* API provided for other modules to get the current CLI prompt */
INT1  CliGetCurPrompt PROTO ((INT1 *pi1CliCurPrompt));

INT1 CliSetCurMode PROTO ((INT1 *pi1CliCurPrompt));
INT1 CliSetPoolId PROTO ((UINT4 u4PoolId));
INT1 CliGetPoolId PROTO ((UINT4 *u4PoolId));

INT1 CliSetNatId PROTO ((UINT4 u4PoolId));
INT1 CliGetNatId PROTO ((UINT4 *u4PoolId));

INT1  CliGetCurMode PROTO ((INT1 *pi1CliCurPrompt));

/* API provided for other modules to set the current Modes Index info 
 * This may be used by modules which needs to keep track of some index information
 * (like interface index, Dhcp pool config index ...)
 * Returns CLI_ERROR/i4IndexInfo.
 */
INT4 CliSetModeInfo PROTO ((INT4 i4IndexInfo));

/* API provided for other modules to get the current Modes Index Info
 * Returns CLI_ERROR/i4IndexInfo from CLI Context.
 */
INT4 CliGetModeInfo PROTO ((VOID));

/* API provided for other modules to get the current Session Mode.
 * Returns CLI_ERROR/gu4CliMode from CLI Context.*/

UINT4 CliUtilGetSessionMode PROTO ((VOID));

/* API provided to set the current context Id
 * This is used by vcm to set the current context of the system.
 */
UINT4 CliSetContextIdInfo PROTO ((UINT4 u4Context));

/* API provided for other modules to get the current Context index info */
UINT4 CliGetContextIdInfo PROTO ((VOID));

/* API provided for other modules to change the current CLI path */
INT4 CliChangePath (CONST CHR1 *pu1Path);

/*  Validates the given password against the specified user 
 * - called by the WEBNM module
 */
INT2  CliCheckUserPasswd PROTO ((INT1 *pi1UserName, CONST INT1 *pi1UserPasswd));
INT1 CliGetUserPrivilege (CONST CHR1 * pi1UserName, INT2 *pi2UserPrivilege);

/* This function is used to check the allow user is blocked until the time release. 
 */
INT4 CliUtilCheckAllowUserLogin (CHR1 * pi1Name);

/* This function is used to block the user when 'n' tries to attempt wrong password
 */
INT4 CliUtilCheckBlockUserOrNot (CHR1 * pi1Name);

/* This function is used to set the Current CliContext's ErrorCode object parameter
 */
INT4 CliSetErrorCode PROTO ((UINT4 u4ErrCode));

/* This function is used to get the Current CliContext's ErrorCode object parameter
 */
INT4 CliGetErrorCode PROTO ((UINT4 *pu4ErrCode));

/* This function is used to set the Current CliContext's Command Status object parameter
 */
INT4 CliSetCmdStatus PROTO ((UINT4 u4CmdStatus));

/* This function is used to get the Current CliContext's Command Status object parameter
 */
INT4 CliGetCmdStatus PROTO ((UINT4 *pu4CmdStatus));

/*
 * Convert a numeric TraceLevel (u4TrcLvl) to a string representation (pc1Str),
 * as determined by the pac1Trc (array of pointers to trace types). u4StrSize
 * is the sizeof(pc1Str). This is used to avoid buffer overflows.
 */
VOID  CliTrcStr PROTO((UINT4 u4TrcLvl, const CHR1 *pac1Trc[],
                       CHR1 *pc1Str, UINT4 u4StrSize));

/* API provided for other modules to display the output buffer by passing a
 * Handle to the Active CliContext 
 * Returns : CLI_SUCCESS for valid CliHandle
 *           CLI_FAILURE for Invalid CliHandle
 */

INT4  CliPrintf(tCliHandle, CONST CHR1 *, ...);
INT4 CliNpPrintf(CONST CHR1 *,va_list);
VOID ISSCliCheckAndThrowFatalError (tCliHandle CliHandle);
VOID  CliFlush (tCliHandle);
INT4  CliFlushWithRetCheck (tCliHandle, UINT1 *);
VOID  CliGetBangStatus (tCliHandle ,UINT1 *);
VOID  CliSetBangStatus (tCliHandle ,UINT1 );

VOID
CliRegisterLock (tCliHandle CliHandle, 
                 INT4       (* fpLock) (VOID), 
                 INT4       (* fpUnLock) (VOID));

VOID
CliUnRegisterLock (tCliHandle CliHandle);

VOID
CliUnRegisterAndRemoveLock (tCliHandle CliHandle);

/* Message buffer for MMI text */
extern CONST CHR1        *mmi_gen_messages[];

/* Function used to display a variable argument list string */
VOID mmi_printf PROTO ((const char *fmt, ...));

INT4 mmi_more PROTO ((VOID));

/* Functions used to display the given String and also takes care of paging */
INT4 mmi_more_printf PROTO ((INT1 *));
INT4 mmi_multi_more_printf PROTO ((INT1 *, INT1));

/* Searches whether an user exits or not and 
 * returns the index of the user if exists else CLI_ERROR.
 */
INT4 mmi_search_user PROTO ((INT1 *));

/*
 * This function reads the Input Octet string and returns its length. 
 */
INT4 CliDotStrLength PROTO ((UINT1 *pDotStr));

/* This function converts string to port list */
INT4 ConvertStrToPortList PROTO ((UINT1 *pu1Str, UINT1 *pu1PortArray,
                                  UINT4 u4PortArrayLen, UINT4 u4MaxPorts));

INT4 ConvertStrToPortArray PROTO ((UINT1 *pu1Str, UINT4 *pu4PortArray,
                                   UINT4 u4PortArrayLen, UINT4 u4MaxPorts));


/* This function is used to get the CLI parameters
 * used by other protocol modules
 * Returns : CLI_SUCCESS/CLI_FAILURE
 */
INT4 CliGetContextInfo PROTO ((tCliParams *));

/* This function checks if the given character i1Char matches with 
 * any of the character in the delimiter string pi1Delimit string
 * if matches will return i1Char, else 0.
 */
INT1 CliIsDelimit PROTO ((INT1 i1Char, CONST CHR1 *pi1Delimit));

/* Converts the given mac address string in to array of UINT1 values */
VOID CliDotStrToMac PROTO ((UINT1 *pu1DotStr, UINT1 *pu1Mac));

/* Converts the given hex string to array of UINT1 values of size i4Len */
VOID CliHexStrToOctet PROTO ((UINT1 *pu1HexStr, UINT1 *pu1Octet, INT4 i4Len));

/* Converts the mac address in to string of octets seperated by ':' 
 * ex. pMacAddr = {10,20,30,40,50,60} --> "10:20:30:40:50:60"*/
VOID CliMacToStr PROTO ((UINT1 *pMacAddr, UINT1 *pu1Str));
VOID AclCliMacToStr PROTO ((UINT1 *pMacAddr, UINT1 *pu1Str));
#ifdef EVPN_WANTED
/* Converts Ethernet Segment Identifier to string */
VOID CliEthSegIdToStr PROTO ((UINT1 *pMacAddr, UINT1 *pu1Str));
#endif
/* Converts the mac address in to string of octets seperated by ':' 
 * ex. pMacAddr = {10,20,30,40,50,60} --> "1020.3040.5060"*/
VOID CliMacToFourOctetStr PROTO ((UINT1 *pMacAddr, UINT1 *pu1Str));

/* Converts the given array of UINT1 values of size CLI_MAC_ADDR_SIZE 
 * in to string of octets seperated by '.' 
 * ex. pu1Mac [] = {12,34,56,78,90,22} --> "12.34.56.78.90.22" */
VOID CliMacToDotStr PROTO ((UINT1 *pu1Mac, UINT1 *pu1DotStr));

/* Converts the mac address string to array of UINT1 value. */
VOID CliStrToMac PROTO ((UINT1 *pu1String, UINT1 *pu1Mac));

/* Converts the given Octet string seperated with '.'to
 * an array of UINT1 values */
VOID CliDotStrToStr PROTO ((UINT1 *pu1DotStr, UINT1 *pu1Val));

/* Returns the length of the given string of octets
 * ex. "12.34.56.78" --> 4 
 */
UINT4 CliOctetLen PROTO ((UINT1 *));

/* Converts String of hex. value in to decimal value
 * ex. "0x12ff" --> 4863 */
INT4 CliHexStrToDecimal PROTO ((UINT1 *pu1Str));

/* Returns the port number of the given application, 
 * if it's a known application name, else -1 */
INT4 CliGetServPortByName (UINT1 *pu1App);

/* Compares the two strings, ignoring the case of the characters,
 * but compares only 'i4Len' characters of the two strings. */
INT1 CliStrNCaseCmp PROTO ((INT1 *pi1Str1, INT1 *pi1Str2, INT4 i4Len));

/* The function is used to set port u4Port bit of the
 * port list array pu1PortArray of size u4PortArrayLen
 */
INT4  CliSetMemberPort PROTO ((UINT1 *pu1PortArray, 
                               UINT4 u4PortArrayLen, UINT4 u4Port));

/* The function is used to check if the 'u4Port' bit of the 
 * port list array 'pu1PortArray' of size 'u4PortArrayLen' is set or not.
 * if set will return OSIX_SUCCESS, else OSIX_FAILURE.
 */
INT4 CliIsMemberPort PROTO ((UINT1 *pu1PortArray, 
                             UINT4 u4PortArrayLen, UINT4 u4Port));

/* The function will convert the given string to port list.
 * The bit position in pu1PortArray for the numbers in string pu1Str will
 * be set to 1. 
 */
INT4 CliStrToPortList PROTO ((UINT1 *pu1Str, UINT1 *pu1PortArray,
                              UINT4 u4PortArrayLen, UINT4 u4IfType));

/* The function will convert the given string to port list.
 * The bit position in pu1PortArray for the numbers in string pu1Str will
 * be set to 1. This should be called when having subtype classification.
 */

INT4 CliStrToPortListExt PROTO ((UINT1 *pu1Str, UINT1 *pu1PortArray,
                                UINT4 u4PortArrayLen, UINT4 u4IfType,
                                UINT4 u4SubType));
/* The function will convert the given string to Iface list.
 * The bit position in pu1PortArray for the numbers in string pu1Str will
 * be set to 1. 
 */
INT4 CliGetRange PROTO ((UINT1 *pu1Str, UINT4 *pu4StartIndex, UINT4 *pu4StopIndex));
INT4
CliStrToIfaceList PROTO ((UINT1 *pu1Str, UINT1 *pu1PortArray,
                   UINT4 u4PortArrayLen, UINT4 u4MaxPorts));

/* This function converts octet of port bits to port list string.
 * Checks for each bit pos. in pOctet, if set will add that port number
 * to pu1Temp. The list will be seperated by a ','.
 */
VOID CliOctetToPort PROTO ((UINT1 *pOctet, UINT4 u4OctetLen, UINT1 *pu1Temp, 
                            UINT4 u4TempBufLen, UINT4 u4MaxPorts));

/* This function converts the given array of UINT1 values of pOctet to 
 * string of octets seperated by ':' in pu1Temp
 */
VOID CliOctetToStr PROTO ((UINT1 *pOctet, UINT4 u4OctetLen, 
                           UINT1 *pu1Temp, UINT4 u4BufLen));

/* This function converts the given array of UINT1 values of pOctet to 
 * hex string in pu1Temp
 */
VOID CliOctetToHexStr PROTO ((UINT1 *pOctet, UINT4 u4OctetLen, 
                           UINT1 *pu1Temp));
/* This function parses the given flash_url token value and 
 * if valid,returns the Flash filename in pi1FlashFileName
 * Returns CLI_SUCCESS/CLI_FAILURE
 */
INT4 CliGetFlashFileName PROTO ((INT1 *pi1FlashStr, INT1 *pi1FlashFileName)); 

/* This function parses the given cust_url token value and
 * if valid,returns the Customized filename in pi1CustFileName along with Customized path
 * Returns CLI_SUCCESS/CLI_FAILURE
 */

INT4 CliGetCustFileName PROTO ((INT1 *pi1CustStr, INT1 *pi1CustFileName, INT1 *pi1CustFilePath));

INT4 CliUtilValidateHostName PROTO ((INT1 * pi1HostName));

/* This function  is used to convert net mask value in to number of mask bits */
UINT4 CliGetMaskBits PROTO ((UINT4 u4SubnetMask));

/* Functions for range Command */ 

INT4 CliInitIfRangeType PROTO((VOID));

INT4 CliGetIfRangeType PROTO((UINT1 *));

INT4 CliSetIfRangeType PROTO((UINT1 *));

INT4 CliInitRangeIndex PROTO((VOID));

INT4 CliSetRangeIndex PROTO((UINT4 , UINT4));

INT4 CliInitRangeList PROTO((VOID));

INT4 CliSetRangeList PROTO((UINT1 *));

INT4 CliSetIfRangeMode PROTO((VOID));

INT4 CliGetIfRangeMode PROTO ((INT4 *));

INT4 CliReSetIfRangeMode PROTO ((VOID));

INT1 CliRemoveCommand PROTO ((INT1 * ,INT1 *));

INT1 CliGetCommandPriv PROTO ((CHR1 ai1ModeName[CLI_COMMAND_MODE_LEN], 
                              CHR1 ai1CmdName[CLI_COMMAND_LEN], INT4 *pi4CmdPriv));

INT1 CliGetExtensiveSearch PROTO ((UINT1 *pu1CommandName, INT4 *pi4PrivId));

/* Function for Setting DefaultMore Flag*/
VOID CliModifyMoreFlag PROTO ((tCliHandle, INT1 *));

/* Function for changing the Privilege of Cli Command */
INT1 CliCommandPrivChange PROTO ((INT1 * ,INT1 *, INT1));

INT1
CliRemoveCmdOpt (INT1 * pModeName, INT1 * pCmdName,
                 INT4 *i4Option_position, INT1 *i1Option_name);

INT2 CliAuthenticateUserPasswd PROTO ((INT1 *pi1UserName, CONST INT1 *pi1UserPasswd));
/* Functions for Range commands End */

extern VOID CliConvertStrToMac (UINT1 *pu1String, tMacAddress * pMac);

VOID CliLogout PROTO ((VOID));

INT1 CliSplitTimeToken (INT1 *pi1TempToken, UINT1 *pu1Hrs, UINT1 *pu1Mins, UINT1 *pu1Secs);

INT4
CliOctetToIfName (tCliHandle CliHandle, CHR1 *pu1FirstLine, 
                  tSNMP_OCTET_STRING_TYPE *pOctetStr,
                  UINT4 u4MaxPorts, UINT4 u4MaxLen,
                  UINT1 u1Column, UINT4 *pu4PagingStatus,
       UINT1 u1MaxPortsPerLine);

INT4
CliOctetPortChannelToIfName (tCliHandle CliHandle,
                  tSNMP_OCTET_STRING_TYPE *pOctetStr,
                  UINT4 u4MaxPorts, UINT4 u4MaxLen,
                  UINT4 *pu4PagingStatus);
INT4
CliOctetToMirrorList (tCliHandle CliHandle, CHR1 * pu1FirstLine,
                  tSNMP_OCTET_STRING_TYPE * pOctetStr,
                  UINT4 u4MaxPorts, UINT4 u4MaxLen,
                  UINT1 u1Column, UINT4 *pu4PagingStatus,
                  UINT1 u1MaxPortsPerLine);
INT4
CliConfOctetToIfName (tCliHandle CliHandle, CHR1 * pu1FirstLine, 
                      CHR1 * pu1DefaultStr, 
                      tSNMP_OCTET_STRING_TYPE * pOctetStr, 
                      UINT4 *pu4PagingStatus);

extern UINT1 CfaIsLaggInterface (UINT4 u4IfIndex);

VOID CliWaitForConfRestoration PROTO ((VOID));
VOID  CliInformRestorationComplete PROTO ((VOID));

INT1 CliGetPromptFromMode PROTO ((CONST CHR1 *, INT1 *));
VOID CliSetSessionBitMap PROTO ((UINT4 u4SessionType));

INT4 CliGetTelnetPort (VOID);
INT4 CliCloseAllConnections PROTO ((VOID));

VOID CliSetLoginAttempts (UINT1);
VOID CliSetLoginLockOutTime (UINT4);
VOID CliSetSshClientWindowSize PROTO ((INT2 i2Row, INT2 i2Col));
#ifdef RM_WANTED
VOID  CliInformRmSwitchover PROTO ((VOID));
VOID  CliRmRoleToInit (VOID);
VOID  CliBlockConsole PROTO ((VOID));
#endif /*RM_WANTED*/
#ifdef ECFM_WANTED
INT4 CliEcfmRegisterSignalHandler PROTO ((INT4, VOID
(*pfSignalHandler)(INT4)));
VOID CliEcfmUnRegisterSignalHandler PROTO ((INT4 ));
INT4 CliDisableMore PROTO ((tCliHandle));
INT4 CliEnableMore PROTO ((tCliHandle));
#endif

/* Register Call Back Function for Customization in CLI */
INT4 CliUtilRegCallBackforCLIModule (UINT4 u4Event, tFsCbInfo * pFsCbInfo);

INT4 CliGetInputFd (VOID);
INT4 CliReadFromCliConsole (UINT1 *);
INT4 CliWriteInCliConsole (UINT1 *pMessage, INT4 i4len);
INT4 CliGetSessionId (VOID);
UINT4 CliGetSSHSessionId(VOID);
INT4 CliSerialSelect (VOID);
VOID CliLogAuditDisableStatus (VOID);
VOID CliUtilMgmtLockStatus (BOOL1 );
VOID CliUtilMgmtClearLockStatus (VOID);
typedef enum
{
        CLI_USER_PRIVILEGE_CHK = 1,
        CLI_STRICT_PASSWD_CHK,
        CLI_MAX_CALLBACK_EVENTS
}eCliCallBackEvents;

INT4 CliGetTelnetServerStatus PROTO(( INT4 *pi4CliTelnetStatus));
INT4 CliSetTelnetServerStatus PROTO((INT4 i4CliTelnetStatus));

INT4 CliSetSshServerStatus PROTO((INT4 i4CliSshStatus));
typedef struct
{
    INT4                i4CliTelnetStatus;
}
tCliTelnetServerParams;

INT4 CliUtilDeleteAllUsersInfo PROTO ((VOID));

typedef enum
{
    CLI_TELNET_CLEAR = 1,
    CLI_SSH_CLEAR,
    CLI_TELNET_SSH_CLEAR
}eCliDestroyOptions;

 enum
{
    CLI_NO_HANDLE_BANG = 0, 
    CLI_HANDLE_BANG, 
    CLI_HANDLE_AND_RESET_BANG
};

VOID CliDestroyConnections PROTO ((UINT4 u4DestroyOption,INT1 i1ClearFlagStatus));
VOID CliDestroyDefaultConnections (INT1 i1ClearFlagStatus);

/* Struct containing Choice of Privilege Id based or Username based Authentication in CLI */
typedef struct  
{
    BOOL1 b1UsernameBased;
    BOOL1 b1PrivilegeBased;
    UINT1   u1PrivilegeId;
    UINT1   au1UserName[CLI_MAX_LEN_USERNAME];
}tCliSpecialPriv;

VOID CliSetSpecialPriv (tCliSpecialPriv * pCliSpecialPriv);
VOID CliGetSpecialPriv (tCliSpecialPriv * pCliSpecialPriv);
INT4 CliCheckSpecialPriv PROTO ((VOID));
INT4 CliContextInitToDefault PROTO ((VOID));
VOID CliSetSessionTimer (INT4 i4SessionTimer);
VOID CliIssGetDefaultExecTimeOut (INT4 *pi4DefExecTimeOut);
CHR1 *CliLogOutMessage PROTO ((VOID));
VOID CliLogOutUser PROTO ((INT1 *pi1UsrName));
UINT4 CliGetActiveSessions PROTO ((VOID));
INT4 CliSetSkipDoubleBang PROTO ((UINT1));
INT4 CliGetShowCmdOutputToFile (UINT1 *pu1FileName, UINT1 *pu1CmdName);
INT4 CliGetConnectionId (VOID);
INT4 CliCloseConnections (UINT4 u4IpAddr, UINT4 u4NetMask);
INT4 CliCommandMatchSubset PROTO ((CONST UINT1 *pCommand, UINT1 *pString));
BOOL1
CliCompareExactSubset PROTO ((INT1 *i1pUserInput, CONST INT1 *i1pTestInput));
INT4 CliExecuteCmdFromFile PROTO ((UINT1 *pu1CmdFile));
INT4
CliDisplayMessageAndUserPromptResponse (CHR1 *pi1InputMessage, INT1 i1RepeatFlag,
                                        INT4(*pLockPointer) (VOID),
                                        INT4 (*pUnlockPointer) (VOID));
#endif /* __CLI_H__ */
