/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppoe.h,v 1.3 2014/03/11 14:02:40 siva Exp $
 *
 * Description: This header file contains the data structures having 
 *               pppoe header information
 *******************************************************************/
#ifndef _PPPOE_H_
#define _PPPOE_H_

#define  HOST_ADDR_LEN             6

#define  PPPOE_FULL_HDR_LEN        20     
#define  PPPOE_VER_AND_TYPE        0x11   
#define  PPPOE_SESSION_CODE        0x00   

#define     t_MSG_DESC              tCRU_BUF_CHAIN_DESC

typedef struct PPPoEHeader{
    UINT1   DestMACAddr[HOST_ADDR_LEN];
    UINT1   SrcMACAddr[HOST_ADDR_LEN];
    UINT2   u2EthType;
    UINT1   u1VerAndType;
    UINT1   u1Code;
    UINT2   u2SessionId;
    UINT2   u2Length;
    UINT2   u2VlanId;
} tPPPoEHeader;

VOID PPPoEExtractHeader  ARG_LIST( (t_MSG_DESC *,tPPPoEHeader *) );

INT4
PPPoEGetDestMacFromPPPIfIdx ARG_LIST( (UINT4 u4ifIndex, UINT1 *au1DestMac) );
#endif
