/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bridge.h,v 1.36 2011/08/24 06:06:16 siva Exp $
 *
 * Description:This file contains the exported definitions and  
 *             macros of BRIDGE                                
 *
 *******************************************************************/
#ifndef _BRIDGE_H
#define _BRIDGE_H


#define   BRG_NO_FORWARD          1 
#define   BRG_FORWARD             2 

#define   BRG_DATA_FRAME          1
#define   BRG_CNTL_FRAME          2
#define   BRG_MCAST_FRAME         3
#define   BRG_IGMP_FRAME   4
#define   BRG_MRP_FRAME           5

/* START and SHUTDOWN services */
#define BRG_START                 1                                     
#define BRG_SHUTDOWN              2                                     

#define BRG_UP                    1                                     
#define BRG_DOWN                  2                                     

#define   ETHERNET_ADDR_SIZE      6

#if  defined (RSTP_WANTED) || defined(MSTP_WANTED) 
#define  BRIDGE_TMR_TASK_NAME    ((const UINT1 *)("BTMR"))
#else
#define  BRIDGE_TMR_TASK_NAME    ((const UINT1 *)("STAP"))
#endif

#define  BRIDGE_STAP_TASK_NAME   BRIDGE_TMR_TASK_NAME 
#define  BRG_TIMER_EXP_EVENT        (0x00000001)       
#define  BRG_PKT_ARVL_EVENT         (0x00000008)
#define  BRIDGE_INVALID           3
#define  BRIDGE_DOWN_ALL_IFS      3

#define  BRG_QUEUE_NAME ((const UINT1*) "BRGQ")
#define  BRG_QUEUE_DEPTH          40
#define  BRG_QUEUE_ID             gBrgQId

#define   SEC_TO_STAP(x)           ((x) / 100 )
#define   NORMAL_AGEOUT_TIME       SEC_TO_STAP(30000)

#define   BRG_ENET_TEN_MBPS       10000000 
#define   BRG_ENET_HUNDRED_MBPS   100
#define   BRG_ENET_THOUSAND_MBPS  1000
#define   BRG_ENET_TEN_GBPS       10000

#define  BRG_PORTS_PER_BYTE      8
#define  BRG_STP_DEFAULT_CONTEXT  0

typedef UINT4 tBOOLEAN;

typedef tMacAddress tMACADDRESS;

typedef struct BrgInInfo {
   tMacAddr  SrcAddr;
   UINT2        u2Protocol;
   tMacAddr  DestAddr;
   UINT2        u2InPort;
   UINT2        u2Length;
   UINT2        u2Reserved;
} tBrgIfInInfo;

typedef struct BrgOutInfo {
   UINT2        u2OutPort;
   UINT2        u2Protocol;
   UINT4        u4FrameSize;
   UINT1       *pau1DestHwAddr;
   UINT1        u1FrameType;
   UINT1        u1EncapType;
   UINT2        u2Reserved;
} tBrgIfOutInfo;

/* 
 * This structure is used as interface structure between 
 * Vlan & Igmp snooping and Bridge.
 */

typedef struct BrgIf {
   UINT4        u4TimeStamp;
   union {
      tBrgIfInInfo  InInfo; /* accessed in the input direction */
      tBrgIfOutInfo OutInfo;/* accessed in the output direction */
   } uInfo;
} tBrgIf;


/*
 *  This structure specifies the attribute of the message either for
 *  forwarding or while receiving.
 */

typedef struct msgAttr 
{
    tBOOLEAN     bMember;
    UINT4        u4TimeStamp; /* For transit delay handling */
    UINT2        u2LogicalPortNo;
    UINT2        u2Protocol;
    UINT2        u2Index;
    UINT2        u2Length;
    tMacAddr     destAddr;
    tMacAddr     srcAddr;
} tMSG_ATTR;

typedef struct {
   tPortList *pTagPorts;
   tPortList *pUnTagPorts;
} tCfaTxPorts;

typedef struct {
   UINT2     u2TxPort;
   UINT2     u2EtherType;
   UINT1     u1Tag;
   UINT1     au1Pad[3];
} tCfaTxUcastPort;


typedef struct {
       UINT2 u2VlanId;
       UINT2 u2PktType;
       UINT2 u2InnerVlan;
       struct {
             tCfaTxPorts TxPorts;
             tCfaTxUcastPort TxUcastPort;
       } unPortInfo;
       UINT1 au1Pad[2];
}tCfaVlanInfo;

/* to be mapped with the respective BRIDGE defines */
#define   BRIDGE_SUCCESS           0                          
#define   BRIDGE_FAILURE          -1                          
#define   BRIDGE_IGNORE            2                          
#define   CFA_IS_BRIDGE_ENABLED   (GetBridgeStatus() == 1) 

#define BRG_SHORT_AGEOUT_TIME()       BrgGetShortAgeoutTime()
#define BRG_GET_AGEOUT_TIME()              BrgGetAgeoutTime()

/* Macros for accessing the Bridge IF message */
#define BRG_IFMSG_TIME_STAMP(pBrgIf)      (pBrgIf)->u4TimeStamp

/* Macros for accessing the Bridge IN IF message */
#define BRG_IN_IFMSG_SRC_ADDR(pBrgIf)     (pBrgIf)->uInfo.InInfo.SrcAddr
#define BRG_IN_IFMSG_DST_ADDR(pBrgIf)     (pBrgIf)->uInfo.InInfo.DestAddr
#define BRG_IN_IFMSG_PROTOCOL(pBrgIf)     (pBrgIf)->uInfo.InInfo.u2Protocol
#define BRG_IN_IFMSG_PORT(pBrgIf)         (pBrgIf)->uInfo.InInfo.u2InPort
#define BRG_IN_IFMSG_LEN(pBrgIf)          (pBrgIf)->uInfo.InInfo.u2Length

/* Macros for accessing the Bridge OUT IF message */
#define BRG_OUT_IFMSG_PORT(pBrgIf)        (pBrgIf)->uInfo.OutInfo.u2OutPort
#define BRG_OUT_IFMSG_PROTOCOL(pBrgIf)    (pBrgIf)->uInfo.OutInfo.u2Protocol
#define BRG_OUT_IFMSG_LEN(pBrgIf)         (pBrgIf)->uInfo.OutInfo.u4FrameSize
#define BRG_OUT_IFMSG_DST_ADDR(pBrgIf)    (pBrgIf)->uInfo.OutInfo.pau1DestHwAddr
#define BRG_OUT_IFMSG_FRAME_TYPE(pBrgIf)  (pBrgIf)->uInfo.OutInfo.u1FrameType
#define BRG_OUT_IFMSG_ENCAP_TYPE(pBrgIf)  (pBrgIf)->uInfo.OutInfo.u1EncapType

INT4  BridgeInitializeProtocol          PROTO ((VOID));

VOID  BridgeFwdHandleIncomingDatagram   PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf,
                                                UINT2 , UINT2));

UINT1 GetBridgeStatus                   PROTO ((VOID));
UINT1 GetBridgeSystemControl            PROTO ((VOID));

INT4  BridgeDeletePort                  PROTO ((UINT2 u2IfIndex));

INT4  AddToStapTaskQueue                PROTO ((tCRU_BUF_CHAIN_HEADER* pBuf,
                                                UINT2 u2IfIndex));

tBOOLEAN
TpIsPortOkForForwarding                 PROTO ((UINT4 u4Port, tMSG_ATTR * msgAttr,
                                                tBOOLEAN bBroadcast));

VOID TpBroadcastFrame                   PROTO ((tCRU_BUF_CHAIN_HEADER * pMsg,
                                                tMSG_ATTR * msgAttr,
                                                tBOOLEAN bMulticast));

INT4 BridgeCreatePort                   PROTO ((UINT2 u2IfIndex, UINT2 u2IfType,
                                                UINT4 u4IfSpeed));


/* Bridge interface layer functions */
VOID  BridgePortCreateIndication        PROTO ((UINT2 u2IfIndex,
                                                tCfaIfInfo  *pCfaIfInfo));

VOID BridgePortDeleteIndication         PROTO ((UINT2 u2IfIndex));

VOID BridgePortOperStatusIndication     PROTO ((UINT2 u2IfIndex, UINT1 u1Status));

VOID  BridgeHandleInFrame               PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf,
                                                UINT2 u2IfIndex, UINT2 u2Protocol,
                                                UINT1 u1FrameType));
      
INT1
BridgeHandOverInFrame (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                      UINT2 u2Protocol, UINT1 u1FrameType, INT4 i4IsReservAddr);
      
VOID  BridgeHandleOutFrame              PROTO ((tCRU_BUF_CHAIN_HEADER  *pBuf,
                                                UINT2  u2Ifndex,
                                                UINT4  u4FrameSize,
                                                UINT2  u2Protocol,
                                                UINT1  u1EncapType ));
      

VOID  BridgeForwardMcastFrame           PROTO ((tCRU_BUF_CHAIN_HEADER  *pBuf,
                                                UINT4  u4FrameSize,
                                                UINT2  u2IfIndex,
                                                UINT4  u4InTime));
      
VOID  BrgDeleteFwdEntryForPort          PROTO ((UINT2 u2Port));

VOID  BrgDeleteFwdEntryForMac           PROTO ((tMacAddr *pDestAddr, UINT2 u2Port));

VOID  BrgIncrStatsAtMcastTx             PROTO ((UINT2 u2Port));

UINT4 BrgGetAgeoutTime                  PROTO ((VOID )) ;


INT4  BrgCanForward                     PROTO ((tCRU_BUF_CHAIN_HEADER *pFrame,
                                                UINT2 u2Port));

VOID  BrgIncrFilterInDiscards           PROTO ((UINT2 u2Port));

VOID  BridgeConfigInfoUpdate            PROTO ((UINT2 u2IfIndex,
                                                UINT1 LocalMacAddress[6], 
                                                UINT1 PeerMacAddress[6],
                                                UINT1 u1Stp)); 


VOID BrgTmrTask PROTO ((INT1 *pi1InParam));

#ifdef MBSM_WANTED
extern INT4 BridgeMbsmUpdateCardInsertion PROTO (( tMbsmPortInfo *,
                                                   tMbsmSlotInfo *));

INT4 BridgeMbsmProcessUpdateMessage PROTO ((tMbsmProtoMsg *, INT4));
INT4  CfaMbsmProtoMsgToBrg PROTO ((tMbsmProtoMsg *, INT4 i4Event));
#endif

#endif /* _BRIDGE_H */
