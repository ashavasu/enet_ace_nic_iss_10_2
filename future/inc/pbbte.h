/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pbbte.h,v 1.6 2013/05/06 11:56:14 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              by  external modules.
 *
 *******************************************************************/
#ifndef _PBBTE_H_
#define _PBBTE_H_

#include "rmgr.h"

#define PBBTE_SUCCESS                SUCCESS  /* 0 */
#define PBBTE_FAILURE                FAILURE  /* -1 */

#define PBBTE_TRUE                      1
#define PBBTE_FALSE                     0

#define PBBTE_ENABLED                   1
#define PBBTE_DISABLED                  2

#define PBBTE_CREATE                    1
#define PBBTE_DELETE                    2

/* The constants defined below are used to decide the number of hash tables
 * and the number of buckets in each table used to store the ESP's 
 * {Destination Address, Vlan ID} information. Changing these values affects
 * the ESP set-up, tear-down and MSR restoration performance. */

#define PBBTE_NUM_VLANS_PER_HASH 40 /* Indicates the number of Vlan IDs that
           share the same Hash table. Decreasing
           this number will increase the number
           of hash tables and so the performance.
           But it also increases the memory
           requirements.*/
#define PBBTE_MAX_BUCKETS 100    /* Number of buckets in each hash table */

#define PBBTE_MAX_HASH_TABLE_PER_CONTEXT \
          ((VLAN_DEV_MAX_NUM_VLAN + (PBBTE_NUM_VLANS_PER_HASH - 1))\
           /PBBTE_NUM_VLANS_PER_HASH)

#define PBBTE_MAX_VLAN_ID              VLAN_MAX_VLAN_ID
#define PBBTE_VLAN_LIST_SIZE           ((PBBTE_MAX_VLAN_ID + 31)/32 * 4)

#define PBBTE_LOCK()    PbbTeLock()
#define PBBTE_UNLOCK()  PbbTeUnLock()

#define PBBTE_MSTID                       4094
#define PBBTE_CISTID                      0

#define PBBTE_ESP_TUPULE_SIZE      14

#define PBBTE_TRACE_ENABLED               255
#define PBBTE_TRACE_DISABLED               0

#define PBBTE_NODE_IDLE                      RM_INIT
#define PBBTE_NODE_ACTIVE                    RM_ACTIVE
#define PBBTE_NODE_STANDBY                   RM_STANDBY

typedef enum
{
    PBBTE_START = 1,
    PBBTE_SHUTDOWN
}ePbbTeStatusControl;

/* Enumerations */
/* Config ESP Status */
typedef enum {
    PBBTE_SET = 1,
    PBBTE_RESET
}eConfigEspVidStatus;

/* Storage Types */
enum {
       PBBTE_STORAGE_OTHER = 1,
       PBBTE_STORAGE_VOLATILE,
       PBBTE_STORAGE_NONVOLATILE,
       PBBTE_STORAGE_PERMANENT,
       PBBTE_STORAGE_READONLY 
};

typedef struct EspPathInfo
{
    UINT4      u4ContextId;
    UINT4      u4TeSid;
    tMacAddr   DestMac;
    tMacAddr   SrcMac;
    UINT4      u4EgressIfIndex;
    tPortList  EgressIfIndexList;
    tVlanId    EspVlanId;
    UINT1      au1Reserved[2];
}tEspPathInfo;

/* ESP-Information structure, used while retrieval from TE-SID table */
typedef struct EspInfo
{
    UINT4             u4TeSid;
    UINT4             u4EspIndex;
    UINT4             u4ContextId;
    tMacAddr          SourceAddr;
    tMacAddr          DestAddr;
    tVlanId           EspVlan;
    UINT1             u1RowStatus;
    UINT1             u1StorageType;
}tEspInfo;

typedef UINT4 tPbbTeNodeState;

VOID
PbbTeInit PROTO ((INT1 *pi1Param));

INT4
PbbTeVidIsEspVid PROTO ((UINT4 u4ContextId, tVlanId VlanId));

INT4
PbbTeConfigureEsp PROTO ((tEspPathInfo *pCfgEspInfo, UINT1 u1Action));

INT4
PbbTeGetEspInfo PROTO ((tEspInfo * pEspInfo));

INT4 
PbbTeDeleteContext PROTO ((UINT4));
#ifdef MBSM_WANTED
INT4
PbbTeMbsmPostMessage (tMbsmProtoMsg *pMsg, INT4 i4Event);
#endif

INT4
PbbTeStartModule PROTO ((VOID));

INT4
PbbTeShutdownModule PROTO ((VOID));
#endif
