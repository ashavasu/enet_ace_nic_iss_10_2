/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igmp.h,v 1.20 2016/06/30 10:08:07 siva Exp $
 *
 * Description:This file contains the exported definitions and  
 *             macros of IGMP                                    
 *
 *******************************************************************/
#ifndef _IGMP_H
#define _IGMP_H
#include "utltrc.h"

#ifdef MBSM_WANTED
#include "mbsm.h"
#endif
#define IGMP_IO_MODULE               1 
#define IGMP_QRY_MODULE              2 
#define IGMP_GRP_MODULE              4
#define IGMP_TMR_MODULE              8 
#define IGMP_NP_MODULE       16 
#define IGMP_INIT_SHUT_MODULE      32 
#define IGMP_OS_RES_MODULE      64 
#define IGMP_BUFFER_MODULE      128
#define IGMP_ENTRY_MODULE      256 
#define IGMP_EXIT_MODULE      512 
#define IGMP_MGMT_MODULE             1024
#define IGMP_PACKET_MODULE      2048
#define IGMP_ALL_MODULES             0x00ffff

#define IGMP_DATA_MODULE      4096
#define IGMP_CNTRL_MODULE      8192
#define IGMP_Rx_MODULE             16384
#define IGMP_Tx_MODULE              32768

#define   IGMP_NOT_OK                -1 
#define   IGMP_OK                    0
#define   IGMP_MAX_SOURCES           8
/* Macros used in Web Module also*/
#define   IGMP_VERSION_0             0
#define   IGMP_VERSION_1             1
#define   IGMP_VERSION_2             2
#define   IGMP_VERSION_3      3

#define   IGMP_IANA_MPROTOCOL_ID     10

#define   IGMP_ALL_V3_ROUTERS_GROUP  0xE0000016
#define   IGMP_ALL_V2_ROUTERS_GROUP  0xE0000002

enum {
   IGMP_JOIN = 1,
   IGMP_LEAVE,
   IGMP_EXCLUDE,
   IGMP_INCLUDE
};

typedef struct {
    UINT4      u4IfIndex;                  /* I/f Index                        */
    UINT4  u4GrpAddr;                    /* Group Address                    */
    UINT4  au4SrcAddr[IGMP_MAX_SOURCES]; /* Array of Sources                 */
    UINT1      u1Flag;                     /* possible states Join/Leave       */
    UINT1      u1GrpFilterMode;            /* Include/Exclude                  */
    UINT1      u1NumOfSrcs;                /* Actual Num. Sources in the Array */
    UINT1      u1SrcSSMMapped;  /* Each bit corresponds to a source in au4SrcAddr array */
} tGrpNoteMsg;

typedef tIPvXAddr tIgmpIPvXAddr;

#ifdef FSAP_SEM_DEBUG
#define   IGMP_LOCK() IgmpLockDbg (__FILE__, __LINE__)
#else
#define   IGMP_LOCK() IgmpLock ()
#endif

#define   IGMP_UNLOCK() IgmpUnLock ()

#ifdef FSAP_SEM_DEBUG
INT4 IgmpLockDbg (UINT1 *pu1File, UINT4 u4Line);
#endif

INT4 IgmpLock (VOID);
INT4 IgmpUnLock (VOID);
VOID IgpMainStatusDisable (VOID);
/* FUNCTION PROTOTYPES */
/* ------------------- */
/* IGMP task */
INT4 IgmpCreateTask                  PROTO ((VOID));
VOID IgmpTaskMain PROTO ((INT1 *pi1Param));

VOID IgmpShutDown PROTO ((VOID));
INT4  IgmpUtilIsStaticEntry (UINT4  u4IpAddr, UINT4 u4IfIndex,
                             UINT1 u1AddrType, UINT1 u1CheckVersion);
INT4
IgmpUtilIsSourceConfigured (UINT4 u4IgmpGrpAddress,
                           UINT4 u4IgmpSrcAddress,
                           INT4 i4IfIndex,
                           INT4 i4AddrType);

/*  This fucntion is called by Multicast Routing protocols to Register with 
 *  IGMP module.
 */
INT1
IgmpRegisterMRP PROTO ((UINT1 u1PktType, 
                        VOID (*pInfn) (tCRU_BUF_CHAIN_HEADER *),
                        VOID (*pUpdIfs) (tNetIpv4IfInfo *, UINT4),
                        VOID (*pFwdIfList)(VOID), VOID (*pLJNoteFuncPtr)(tGrpNoteMsg)));

/*  This fucntion is called by Multicast Routing protocols to De-Register from 
 *  IGMP module 
 */
INT1 IgmpDeRegisterMRP                  PROTO ((UINT1 protid));

INT4
IgmpIsEnableOnInterface (UINT4 u4IfIndex);

VOID
IgmpSendReportOrLeave (tIPvXAddr u4GrpAddr, UINT1 u1Leave, UINT4 u4IfIndex);

#ifdef MBSM_WANTED
#define MBSM_IGMP    "MbsmIgmp"
INT4 IgmpMbsmPostMessage (tMbsmProtoMsg * pProtoMsg, INT4 i4Event);
VOID IgmpMbsmHandleEvent (tMbsmProtoMsg *pProtoMsg, UINT4 u4Cmd);
#endif

typedef struct _IgmpSSMMapGrpEntry
{
    tRBNodeEmbd     RbNode;
    tIgmpIPvXAddr   SrcAddr[MAX_IGMP_SSM_MAP_SRC];
    tIgmpIPvXAddr   StartGrpAddr;
    tIgmpIPvXAddr   EndGrpAddr;
    UINT4           u4SrcCnt;
}tIgmpSSMMapGrpEntry;

#ifdef IGMPPRXY_WANTED
VOID IgpPortBmpChgNotify (tMacAddr GrpMacAddr, UINT2 u2VlanId);
UINT1 IgpPortIsIgmpPrxyEnabled (VOID);

#define IGP_SUCCESS  0
#define IGP_FAILURE (-1)

typedef struct IgpNextHopTblIndex {
   UINT4 u4GrpAddr;
   UINT4 u4SrcAddr;
   UINT4 u4SrcMask;
   INT4 i4OIfIndex;
   UINT4 u4NextHopAddr;
}tIgpNextHopTblIndex;

enum eIgpMriObjType
{
   IGP_IIFACE = 1,
   IGP_UPTIME,
   IGP_EXPIRY_TIME,
   IGP_FWD_PACKETS,
   IGP_DIFF_IIFACE,
   IGP_FWD_OCTETS,
   IGP_MCAST_PROTOCOL,
   IGP_RT_PROTOCOL,
   IGP_ROUTE_TYPE,
   IGP_FWD_HCOCTETS,
   IGP_STATE,
   IGP_IFACE_TTL,
   IGP_IFACE_RLIMIT,
   IGP_IFACE_PROTO,
   IGP_IFACE_IN_MOCTETS,
   IGP_IFACE_OUT_MOCTETS,
   IGP_IFACE_HC_IN_MOCTETS,
   IGP_IFACE_HC_OUT_MOCTETS
};


/* IGMP Proxy API for Muticast MIB */
INT4  IgmpProxyValidateRouteTblIndex (UINT4, UINT4, UINT4);
INT4  IgmpProxyGetNextRouteIndex (UINT4, UINT4, UINT4, UINT4 *,
                                  UINT4 *, UINT4 *);
INT4  IgmpProxyGetRouteEntryObjectByName (UINT4, UINT4, UINT4, UINT4,
                                          tSNMP_COUNTER64_TYPE *);
INT4  IgmpProxyValidateNextHopTblIndex (tIgpNextHopTblIndex *);
INT4  IgmpProxyGetNextNextHopIndex (tIgpNextHopTblIndex *,
                                    tIgpNextHopTblIndex *);
INT4  IgmpProxyGetNextHopEntryObjectByName (tIgpNextHopTblIndex *,
                                            UINT4, UINT4*);
INT4  IgmpProxyValidateInterfaceTblIndex (INT4);
INT4  IgmpProxyGetNextInterfaceIndex (INT4, INT4 *);
INT4  IgmpProxyGetInterfaceEntryObjetByName (INT4, UINT4,
                                              tSNMP_COUNTER64_TYPE *);
INT4  IgmpProxySetInterfaceEntryObjectByName (INT4 , UINT4, INT4);
INT4  IgmpProxyTestInterfaceEntryObjectValueByName (UINT4 *, INT4,
                                                     UINT4 , INT4);
#endif

#endif /* _IGMP_H */
