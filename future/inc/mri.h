/****************************************************************************/
/* Copyright (C) 2009 Aricent Inc . All Rights Reserved                     */
/*                                                                          */
/*  FILE NAME             : mri.h                                           */
/*  PRINCIPAL AUTHOR      : Aricent                                         */
/*  SUBSYSTEM NAME        :                                                 */
/*  MODULE NAME           : MRI                                             */
/*  LANGUAGE              : C                                               */
/*  TARGET ENVIRONMENT    :                                                 */
/*  DATE OF FIRST RELEASE :                                                 */
/*  AUTHOR                : Aricent                                         */
/*  FUNCTIONS DEFINED(Applicable for Source files) :                        */
/*  DESCRIPTION           : This file contains the exported definitions and */
/*                          macros of Aricent MRI Module.                   */
/****************************************************************************/
/*                                                                          */
/*  Change History                                                          */
/*  Version               :                                                 */
/*  Date(DD/MM/YYYY)      :                                                 */
/*  Modified by           :                                                 */
/*  Description of change :                                                 */
/****************************************************************************/
#ifndef _MRI_H_
#define _MRI_H_

#define   MRI_MIN_INTERFACE_TTL         0 
#define   MRI_MAX_INTERFACE_TTL         255

#define   MRI_MIN_RATELIMIT_IN_KBPS     0
#define   MRI_MAX_RATELIMIT_IN_KBPS     0

#define   MRI_ROUTE_TYPE_UNICAST        1
#define   MRI_ROUTE_TYPE_MULTICAST      2

#define   MRI_OIF_STATE_PRUNED          1
#define   MRI_OIF_STATE_FORWARDING      2

VOID  MriInit (VOID);
INT4  MriLock (VOID);
INT4  MriUnLock (VOID);

#endif
