/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: raddefs.h,v 1.21 2015/04/28 11:53:23 siva Exp $
 *
 * Description:This file includes all the  
 *             #define's required for RADIUS
 *
 *******************************************************************/
#ifndef RADDEFS_H
#define RADDEFS_H

#include "radius.h"

#define   MAX_USER_QUEUE_SIZE       10

#define   RAD_MAX_TIMER_BLOCKS      MAX_USER_QUEUE_SIZE

#define   RAD_DEBUG_ERROR           0x0001

/************************* PROTOCOL DEFINITIONS *************************/

/*************************** SOFTWARE DEFINITIONS ************************/
#define  RAD_SERVER_DISABLED                1

#define  RADIUS_ENABLED                     1
#define  RADIUS_DISABLED                    2 
#define  RAD_EXT_SRV_DESTROY                3


#define  PRIMARY_SERVER_DISABLED            1
#define  PRIMARY_SERVER_ENABLED             2

#define  RADIUS_CLIENT_PORT_AUTH         1812
#define  RADIUS_CLIENT_PORT_ACC          1813

#define  SERVER_AUTH                     0x01
#define  SERVER_ACC                      0x02
#define  SERVER_BOTH                     0x03

/* Packet Offsets */

#define  PKT_TYPE                           0
#define  PKT_ID                             1
#define  PKT_LEN                            2
#define  PKT_REQA                           4
#define  PKT_RESA                           4
#define  PKT_ATTR                          20

#define  MAX_NAS_PORT                   65535
#define  MAX_RESP_TIME                    120
#define  MIN_RESP_TIME                      1

#define  MAX_RETRANSMIT                    254
#define  MIN_RETRANSMIT                      1

#define  RAD_RETRANS_TIMER                 27

#define  FAULT_MEM_ALLOC                                     1
#define  FAULT_MEM_RELEASE                                   2
#define  FAULT_SOCKET_BINDING                                3
#define  FAULT_TIMER_START                                   4
#define  FAULT_TIMER_STOP                                    5
#define  FAULT_INSUFF_PARAM                                  6
#define  FAULT_DEPENDENCY                                    7
#define  FAULT_MEM_POOL_CREATE                            0x08
#define  MAX_INTEGER                                     10000


#ifndef SYS_NUM_OF_TIME_UNITS_IN_A_SEC
#define  SYS_NUM_OF_TIME_UNITS_IN_A_SEC                      1
#endif

#define  DEFAULT_SECRET                         "AricentRADIUS"
#define  DEFAULT_SERVER_IP_ADDRESS                  0x00000000
#define  DEFAULT_RESPONSE_TIME                              10
#define  DEFAULT_MAX_RETRANS                                 3



#define BYTE_LEN_1     1
#define BYTE_LEN_2     2
#define BYTE_LEN_3     3
#define BYTE_LEN_4     4
#define BYTE_LEN_5     5
#define BYTE_LEN_6     6

#define RAD_ATTR_LEN   7
/*Radius Proxy Related Macros*/
#define  RADIUS_PROXY_ENABLED                     1
#define  RADIUS_PROXY_DISABLED                    2
#define  LEN_PROXY_STATE_ATTR                     24


#endif   /* RADDEFS_H  */

