/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: radproto.h,v 1.24 2015/04/28 11:53:23 siva Exp $
 *
 * Description:This file contains the prototype
 *             declarations of all the functions. 
 *
 *******************************************************************/
#ifndef  RAD_PROTO_H

#define RAD_PROTO_H

#include "radius.h"

/* RAD_AUTH_C  */

INT1 radiusEnterUserQAuth(tRADIUS_INPUT_AUTH *, UINT1 *, tRADIUS_REQ_ENTRY **,                           tRADIUS_SERVER *,void (*CallBackfPtr)(void *), UINT4 );

INT1 radiusValidateInputAuth(tRADIUS_INPUT_AUTH *);
INT1 radiusFramePrePacketAuth(UINT1 *, UINT2 * , UINT1);
VOID radiusGenReqAuthenticatorAuth(UINT1 *);
VOID radiusHidePassword(UINT1 *, UINT1 *, UINT1 *, INT4 *, tRADIUS_SERVER *);
VOID radiusMakeStringAuth(UINT1 *, UINT1 *, INT4 *, tRADIUS_SERVER *);
VOID radiusFillAttributesAuth(tRADIUS_INPUT_AUTH *, UINT1 *, INT4,  UINT2 *,
                              UINT1 *, tRADIUS_SERVER *);
VOID radiusFillAttrEapuser (tUSER_INFO_EAP *, UINT1 *, UINT2 *);
VOID radiusFillAttrMschapuser(tUSER_INFO_MS_CHAP *, UINT1 *, UINT2 *);
VOID radiusFillAttrMschapcpw1(tMS_CHAP_CPW1 *, UINT1 *, UINT2 *);
VOID radiusFillAttrMschapcpw2(tMS_CHAP_CPW2 *, UINT1 *, UINT2 *);

INT4 RadiusTransmitAuth PROTO ((tRADIUS_INPUT_AUTH *p_RadiusInputAuth,
                                UINT1 *a_u1RadiusPacketAuth,
                                tRADIUS_SERVER *p_RadiusServerAuth,
                                VOID (*CallBackfPtr) (VOID *), 
                                UINT4 ifIndex, UINT4 u4NumServers));

INT1 RadiusCopyUserInfoAuth PROTO ((tRADIUS_INPUT_AUTH *p_RadiusInputAuth,
                                    tRADIUS_INPUT_AUTH  *p_UserInfoAuth));

VOID RadiusReleaseUserInfoAuth PROTO ((tRADIUS_INPUT_AUTH *p_UserInfoAuth));
	
/* RAD_ACCT_C  */

INT1 radiusEnterUserQAcc(tRADIUS_INPUT_ACC *, UINT1 *, tRADIUS_REQ_ENTRY **,                             tRADIUS_SERVER *,void (*CallBackfPtr)(void *), UINT4);

INT1 radiusValidateInputAcc(tRADIUS_INPUT_ACC *);
VOID radiusFramePrePacketAcc(UINT1 *, UINT2  *);
VOID radiusFillAttributesAcc(tRADIUS_INPUT_ACC *, UINT1 *, UINT2 *);
VOID radiusMakeFinalPacketAcc(UINT1 *, UINT2  *, tRADIUS_SERVER *); 
VOID radiusMakeReqAuthAcc(UINT1 *, UINT1 *, tRADIUS_SERVER *);
VOID radiusMakeStringAcc(UINT1 *, UINT1 *, INT4 *, tRADIUS_SERVER *);
VOID radiusFillReqAuthAcc(UINT1 *, UINT1 *);

/* RAD_RECV_C  */

INT1 radiusTakeAction(UINT1 *  );
INT1 radiusValidatePacket(UINT1 *, tRADIUS_REQ_ENTRY **);
INT1 radiusValidateIdentifier(UINT1 *, tRADIUS_REQ_ENTRY **); 
VOID radiusMakeResAuth(UINT1 *, tRADIUS_REQ_ENTRY  **, UINT1 *);
VOID radiusMakeStringVal(UINT1 *, tRADIUS_REQ_ENTRY  **, UINT1 *, INT4 *);
INT1 radiusValidateResAuth(UINT1 *, UINT1 *, tRADIUS_REQ_ENTRY **);  
INT1 radiusValidateAttributes(UINT1 *, tRADIUS_REQ_ENTRY  **);
VOID radiusProcessPacket(UINT1 *, tRADIUS_REQ_ENTRY **); 
VOID radiusCheckIncomingPacketAuth(UINT1* pBuf, tIPvXAddr * pSrcServerAddr);
VOID radiusCheckIncomingPacketAcc(UINT1* pBuf, tIPvXAddr * pSrcServerAddr);


UINT1 *
RadDecryptMsMppeKey PROTO ((UINT1 *pu1Key, UINT4 u4KeyLen, UINT1 *pu1ReqAuth,
                     UINT1 *pu1SharedSecret, UINT4 u4SharedSecretLen,
                     UINT2 *pu2DecryptedKeyLen));
UINT1              *
RadRecvEncryptMsMppeKey PROTO ((UINT1 *pu1Key, UINT4 u4KeyLen, UINT1 *pu1ReqAuth,
                                UINT1 *pu1SharedSecret, UINT4 u4SharedSecretLen,
                                UINT2 *pu2EncryptedKeyLen,UINT1 *pu1Salt));

/* RAD_TXMT_C  */

INT4 radiusTransmitPacket(UINT1 *, INT4, INT4, tRADIUS_SERVER *);

VOID radiusTimeoutHandler(tTimerListId);

INT1 radiusTerminateAuthentication(UINT1);

/* RAD_TIMER_C */

INT4 rad_tmrh_stop_timer(tTmrAppTimer*);
INT4 rad_tmrh_start_timer(tTmrAppTimer*, UINT4);


/* RAD_MEM_C */

tRADIUS_REQ_ENTRY * radiusCreateUserEntry(INT4);
UINT1 * radiusCreatePacket(INT4);
INT4 radiusReleaseUserEntry(INT4, UINT1 *);
INT4 radiusReleasePacket(INT4, UINT1 *);
tRADIUS_SERVER * radiusCreateRadiusServer(INT4);
INT4 radiusReleaseRadiusServer(INT4, UINT1 *);

/* RAD_FAULT_C */

VOID radiusInformFault(FS_ULONG);

/* RAD_INIT_C */

INT4 radiusInitTimer(VOID);
INT4 radiusInitMemPool(VOID);
INT4 radiusInitRadiusServerPool(VOID);
INT4 radiusInitGlobals(VOID);
INT4 radiusInitAuth(VOID);
INT4 radiusInitAcc(VOID);

VOID md5_calc(unsigned char *, unsigned char *, unsigned int);
VOID hmac_md5(unsigned char *, int, unsigned char *, int, unsigned char *);

VOID RadInitRadServer(tRADIUS_SERVER *);
tRadInterface    *RadFillInterfaceStructure PROTO ((UINT1 *, 
                                                    tRADIUS_REQ_ENTRY *));
tRADIUS_SERVER   *RadCreateServerEntry PROTO ((UINT4 ifIndex));
tRADIUS_SERVER   *RadGetServerEntry PROTO ((UINT4 ifIndex , 
                                            UINT4 *u4TableIndex));
tRADIUS_SERVER   *RadGetNextServerEntry PROTO ((UINT4 ifIndex));

VOID RadiusMain(INT1 *);
INT4 RadiusStart(VOID);
tRADIUS_SERVER *RadGetAuthServer( VOID );
tRADIUS_SERVER  *RadGetAcctServer( VOID );
UINT4  RadGetNumAuthServers( VOID );
UINT4 RadGetServerIndex PROTO ((tIPvXAddr * pRadServerAddr));

/* RAD_PROXY_C */
VOID RadProxyProcessPktFromClient PROTO ((UINT1 *, struct sockaddr_in));
VOID RadProxyProcessPktFromServer PROTO ((UINT1 *pRadiusPkt));
VOID RadServerToProxyCalculateMsgAuth PROTO ((UINT1 *p_u1RadiusReceivedPacket, 
                                              UINT1*));
VOID RadClientToProxyCalculateMsgAuth PROTO ((UINT1 *p_u1RadiusReceivedPacket, 
                                              UINT1 *));
UINT4  RadProxyGetRadSrvAddrAndSecret PROTO ((tIPvXAddr * pIpAddress, UINT1 **));
VOID RadFormNewPacket PROTO ((UINT1 *pu1RadiusReceivedPacket , 
                              UINT1 *pu1NewRadiusPacket, UINT2 *pu2DestPort, 
                              UINT4 *pu4ClientIpAddr, UINT1 **ppu1ReqAuth));
VOID RadProxyFillInterfaceStructure PROTO ((UINT1 *pu1RespPkt, UINT1*, UINT1*));
/* RAD_PROXY_C */
#endif /*  RAD_PROTO_H */

