/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: radtdfs.h,v 1.22 2015/04/28 11:53:23 siva Exp $
 *
 * Description:This file includes all the type definitions for   
 *             RADIUS Client Software 
 *                    
 *******************************************************************/
/* Data Structure for RADIUS input for Authentication */

#include "radius.h"


typedef struct userq {
   tRADIUS_SERVER   *ptRadiusServer;              /* Pointer of the server used
                                                     provided by the user
                                                     module.*/
   tRADIUS_INPUT_AUTH userInfoAuth;               /* Structure to store the                     		                                                   Authentication info of 
						                             the user*/
   tRADIUS_INPUT_ACC userInfoAcc;                 /* Structure to store the     						                                                   Accouting info of the 
						                             user*/
   void (*CallBackfPtr)(void *);                  /* Call Back function pointer
                                                      for this user */
   UINT1   *p_u1PacketTransmitted;                /* Pointer to the RADIUS
                                                     packet transmitted */
    UINT4    ifIndex;                             /* ifIndex or the identificati                                                                           on number of the user
                                                     module*/
   UINT4    u4SendTime;                           /* send time of the request*/

   UINT2     Reserved;
   UINT1   a_u1UserName[LEN_USER_NAME];           /* Name of the user, alligned 
                                                     to 4 bytes boundary*/
   UINT1   a_u1RequestAuth[LEN_REQ_AUTH_AUTH];    /* The request authenticator
                                                     transmitted, aligned to 4
                                                     bytes boundary */
   UINT1   u1_ReTxCount;                          /* No of maximum re
                                                     transmissions allowed */
   UINT1   u1_PacketIdentifier;                   /* Identifier of the packet
                                                     transmitted */
   UINT4   u4NumServersContacted;                   /* Number of Radius Servers
                                                     contacted to transmit the
                                                     request */
}tRADIUS_REQ_ENTRY;



typedef struct errorlog {
   UINT4   u4_ErrorPacketIdentifier;
   UINT4   u4_ErrorPacketLength;
   UINT4   u4_ErrorQueueCreationAuth;
   UINT4   u4_ErrorQueueCreationAcc;
   UINT4   u4_ErrorPacketCreationAuth;
   UINT4   u4_ErrorPacketCreationAcc;
   UINT4   u4_ErrorServerDisabledAuth;
   UINT4   u4_ErrorServerDisabledAcc;
   UINT4   u4_ErrorInputInsufficientAuth;
   UINT4   u4_ErrorInputInsufficientAcc;
   UINT4   u4_ErrorTransmissionAuth;
   UINT4   u4_ErrorTransmissionAcc;
   UINT4   u4_ErrorTimerNotStartedAuth;
   UINT4   u4_ErrorTimerNotStartedAcc;
   UINT4   u4_ErrorTimerNotStoped;
   UINT4   u4_ErrorMemPoolCreation;
   UINT4   u4_ErrorAuthSocketOpening;
   UINT4   u4_ErrorAccSocketOpening;
   UINT4   u4_ErrorAuthSocketBinding;
   UINT4   u4_ErrorAccSocketBinding;
}tRADIUS_ERROR_LOG;

typedef struct faultoperand {
   UINT4   u4_Item1;
   UINT4   u4_Item2;
   UINT4   u4_Item3;
   UINT4   u4_Item4;
   UINT4   u4_Item5;
   UINT4   u4_Item6;
   UINT4   u4_Item7;
   UINT4   u4_Item8;
   UINT4   u4_Item9;
   UINT4   u4_Item10;
   UINT4   u4_Item11;
   UINT4   u4_Item12;
   UINT4   u4_Item13;
   UINT4   u4_Item14;
   UINT4   u4_Item15;
   UINT4   u4_Item16;
   UINT4   u4_Item17;
   UINT4   u4_Item18;
   UINT4   u4_Item19;
   UINT4   u4_Item20;
   UINT4   u4_Item21;
   UINT2   u2_Item22;
   UINT2   u2Pad;
}tFAULT_OPERANDS;


    
/*************************** END OF FILE "radtdfs.h" ***********************/


