/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: aphdlr.h,v 1.8 2017/05/23 14:21:47 siva Exp $
 *
 * Description:This file contains the exported definitions and
 *             macros of AP Handler module
 *
 *******************************************************************/
#ifndef _APHDLR_H
#define _APHDLR_H

#include "wssmac.h"

#define LOCAL_ROUTING_TYPE    3

typedef struct
{
    tCRU_BUF_CHAIN_DESC     *pRxPktBuf;

}tApHdlrPktBuf;

typedef struct
{
    UINT4       u4MsgType;  /* Message Type */
    UINT4       u4IfIndex;
    tCRU_BUF_CHAIN_HEADER *pRcvBuf;
    tIpAddr capwapSessionId;
    tMacAddr BssIdMac;
    UINT1 u1MacType;
    UINT1 u1DscpValue;
    UINT1 u1PBitValue;
    UINT1    u1WlanId;
    UINT1 au1Pad[2];

}tApHdlrQueueReq;

enum {
    /* Events processed by AP HDLR Module */
    WSS_APHDLR_CHANGE_STATE_EVENT_REQ,
    WSS_APHDLR_ARP_WTP_EVENT_REQ,
    WSS_APHDLR_PM_WTP_EVENT_REQ,
    WSS_APHDLR_STAION_WTP_EVENT_REQ,
    WSS_APHDLR_PRIMARY_DISC_REQ,
    WSS_APHDLR_MACHDLR_WTP_EVENT_REQ,
    WSS_APHDLR_RADIO_WTP_EVENT_REQ,
    WSS_APHDLR_CAPWAP_QUEUE_REQ,
    WSS_APHDLR_RF_WTP_EVENT_REQ,

    /* Event from CFA */
    WSS_APHDLR_CFA_QUEUE_REQ,

    /* Packet Buf To CFA */
    WSS_APHDLR_CFA_TX_BUF,

    /* WSS MAC Messages */
    WSS_APHDLR_MAC_PROBE_RSP,
    WSS_APHDLR_MAC_DEAUTH_MSG,
    WSS_APHDLR_MAC_DATA_MSG,

    /* WSS WLAN Messages */
    WSS_APHDLR_WLAN_PROBE_REQ,

#ifdef WPS_WTP_WANTED
    /*WPS messages */
    WSS_APHDLR_WPS_WTP_EVENT_REQ,
    WSS_APHDLR_WPS_QUEUE_REQ,
#endif        
    /* CAPWAP MAC Messages */
    WSS_APHDLR_CAPWAP_MAC_MSG

};

typedef union {
    tRadioIfChangeStateEventReq         RadioIfCheStateEvtReq;
    tMacHdlrWtpEventReq                 MacHdlrWtpEventReq;
    tArpWtpEventReq                     ArpWtpEventReq;
    tPmWtpEventReq                      PmWtpEventReq;
    tStationWtpEventReq                 StationWtpEventReq;
    tApHdlrQueueReq                     ApHdlrQueueReq;
    tWssMacMsgStruct                    WssMacMsgStruct;
    tPriDiscReq                         PriDiscReq;

}unApHdlrMsgStruct;


UINT4 ApHandlerMainTaskInit (VOID);

INT4 WssIfProcessApHdlrMsg  (UINT1 MsgType, unApHdlrMsgStruct *pWssIfMsg);

INT4 CapwapConstructPrimaryDiscReq (tPriDiscReq *);

INT4 ApHdlrProcessWssIfMsg (UINT1 MsgType, unApHdlrMsgStruct *);
UINT1 ApHdlrStatsGetNumOfInterfaces (VOID);
UINT4 ApHdlrGetApSentBytes(UINT1 *);
UINT4 ApHdlrGetApRcvdBytes(UINT1 *);
UINT4 ApHdlrGetApGatewayIpAddr (VOID);
UINT4 ApHdlrGetApNetMask(VOID);
VOID  ApHdlrGetApAndRadioStats (VOID);


#endif

