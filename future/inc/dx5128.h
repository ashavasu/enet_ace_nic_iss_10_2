/* $Id: dx5128.h,v 1.28 2016/03/09 11:39:31 siva Exp $ */
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : marvel.h                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        :                                                */
/*    MODULE NAME           :                                                */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Linux                                          */
/*    DATE OF FIRST RELEASE : 29 July 2004                                   */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains sizing parameters for the   */
/*                            Linux environment                              */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    29 July 2004           Initial Creation                        */
/*---------------------------------------------------------------------------*/


/************************************************************************/
/*                Board-dependent constants                             */
/************************************************************************/

/* maximum value for vidx */
#define MAX_VIDX_NUMBER             4095 /* or 4096?? see usage in vlannp.c */

/* number of gigabit ports per device(PP) */
#define DEV_GPORT_NUMBER            24

/* number of XG ports per device(PP) */
#define DEV_XGPORT_NUMBER           4

#define MAX_TAP_INTERFACES            0

/* maximum number of ports on device - used to check port parameter */
/* = 28 */
#   define DEV_MAX_PORTS_NUMBER     (DEV_GPORT_NUMBER + DEV_XGPORT_NUMBER)


#ifdef DX285_24G_4XG /*********************** DB-DX285-24G-4XG ***************/

/* number of devices (PP) on the board */
#   define BOARD_DEV_NUMBER         1

/* maximum value for interface index without trunks */
/* = 28 */
#   define MAX_PHYSICAL_IFIDX       (DEV_GPORT_NUMBER)

#else /*********************** DB-DX285-48G-4XG ******************************/

/* number of devices (PP) on the board */
#   define BOARD_DEV_NUMBER         2

/* maximum value for interface index without trunks */
/* ISS IfIndx does not address internal cascade ports,
 *  * therfore need to substract 2 internal ports for each PP */
/* = 52 */
#   define MAX_PHYSICAL_IFIDX     ((DEV_MAX_PORTS_NUMBER - 2)*BOARD_DEV_NUMBER)

/* number of internal cascade ports in device - back to back link */
#   define DEV_INTERNAL_PORT1_NUM    24
#   define DEV_INTERNAL_PORT2_NUM    25

#endif /* #ifdef DX285_24G_4XG */


/* max allowed number of STP group */
#define NP_MAX_STG      256

/* max allowed number of user priority */
#define NP_MAX_UP       8

/* max allowed number of traffic class */
#define NP_MAX_TC       8

#define SYS_DEF_MAX_PHYSICAL_INTERFACES \
(SYS_DEF_MAX_DATA_PORT_COUNT + SYS_DEF_MAX_INFRA_SYS_PORT_COUNT)
#define SYS_DEF_MAX_DATA_PORT_COUNT 24 /* Max No of Front Panel Ports */
#define SYS_DEF_MAX_INFRA_SYS_PORT_COUNT 0 /* Max No of connecting Ports$
                                              used for Dual Unit Stacking*/

#define SYS_DEF_MAX_PORTS_PER_CONTEXT   BRG_MAX_PHY_PLUS_LOG_PORTS

#ifdef MPLS_WANTED
#define SYS_DEF_MAX_L2_PSW_IFACES      100
#define SYS_DEF_MAX_L2_AC_IFACES       100
#else
#define SYS_DEF_MAX_L2_PSW_IFACES      0
#define SYS_DEF_MAX_L2_AC_IFACES       0
#endif


#ifdef VXLAN_WANTED
#define SYS_DEF_MAX_NVE_IFACES       100
#else
#define SYS_DEF_MAX_NVE_IFACES       0
#endif

/*  No of VLANs are increased with count VLAN_MAX_NUM_VFI_IDS to provide VPLS
 *  visibility to L2 modules.
 *  VLAN ID is increased with VLAN_DEV_MAX_VFI_ID.
 *
 *  Here we can configure 4096 VLANs. For these VLANs, VLAN ID should be present
 *  between (1 - 4095).
 *
 *  We can configure VFIs for the count VLAN_MAX_NUM_VFI_IDS. For these VFIs,
 *  VFI ID should be present between (VLAN_VFI_MIN_ID to VLAN_VFI_MAX_ID)
 */
#define VLAN_DEV_MAX_VLAN_ID      (4095  +  VLAN_DEV_MAX_VFI_ID)
#define VLAN_DEV_MAX_CUSTOMER_VLAN_ID   4096


 /* Maximum number of VLANs*/
#define VLAN_DEV_MAX_NUM_VLAN     (4096 + VLAN_MAX_NUM_VFI_IDS)

#ifdef MPLS_WANTED
#define VLAN_DEV_MAX_VFI_ID       0
#define VLAN_MAX_NUM_VFI_IDS      0
#else
#define VLAN_DEV_MAX_VFI_ID       0
#define VLAN_MAX_NUM_VFI_IDS      0
#endif

#define VLAN_VFI_MIN_ID           (VLAN_DEV_MAX_VLAN_ID - VLAN_DEV_MAX_VFI_ID)
#define VLAN_VFI_MAX_ID           VLAN_DEV_MAX_VLAN_ID

#define VLAN_DEV_MAX_ST_VLAN_ENTRIES   256  /* SL> OK */ /* Maximum number of static
                                                                   * VLANs. This number should be
                                                                   *        * lesser than or equal to
                                                                   *               * maximum number of VLANs
                                                                   *                      */

#define VLAN_DEV_MAX_NUM_COSQ           NP_MAX_TC /* Maximum number of hardware
                                                                                    * Queues
                                                                                    *                                                    */
/* xCat TCAM table usage: */
/* TCAM table is shared between:
 - LPM entries (IPv4/IPv6 UC/MC route & IP4 ARP table & Ip6 neighbour table. LPM ==longest prefix match 
 - mac-based vlan entries (mac-vlan, or mac-map)
 - IP tunnel (tunnel terminate) entries
Note: for correspondance with HW - update function "getPpLogicalInitParams" in file:
  CPSS_ISS\cpssEnabler\mainSysConfig\src\appDemo\boardConfig\gtRd_xCat_24GE_4HGS.c */

/* TODO: write assert, this index range not used by LPM Db */
/* use last 80 indexes (xCat) of LPM TCAM for Tunnel Termination entries */
/* Note: cpssDxChTtiRuleSet comment claims Tcam index 0..3k-1,
         but actually there are 4k rows */
#define MIN_TCAM_TUNNEL_TERM_INDEX      2992
#define MAX_TCAM_TUNNEL_TERM_INDEX      3071

#define MIN_TCAM_MAC_VLAN_INDEX      2791
#define MAX_TCAM_MAC_VLAN_INDEX      2990

  /* 200 - applicable for MAC based VLAN */
#define VLAN_MAX_MAC_MAP_ENTRIES      (MAX_TCAM_MAC_VLAN_INDEX - MIN_TCAM_MAC_VLAN_INDEX + 1)
                                         

#define VLAN_MAX_VID_SET_ENTRIES_PER_PORT 10

#ifdef MSTP_WANTED
#define AST_MAX_MST_INSTANCES         (16 + 1) /* SL> was 256, 16 is enough */
/*NSK : 16 => 16 + 1 */                        /* no. of instance + CIST */
#else
/*When MSTP_WANTED is disabled more than one ERPS vlan group can not be configured
 *so this macro value is changed to (16 + 1)*/
#define AST_MAX_MST_INSTANCES         (16 + 1) /* SL> was 256, 16 is enough */
#endif /*MSTP_WANTED */

/*
 * Only 17 STGs are supported in BCM and the first STG (with id as zero can
 * not be used for PVRST as Vlanid and instance are same.
 */
#define AST_MAX_PVRST_INSTANCES          (AST_MAX_MST_INSTANCES - 1)

#define AST_MAX_INSTANCES   (AST_MAX_MST_INSTANCES > AST_MAX_PVRST_INSTANCES)? AST_MAX_MST_INSTANCES : AST_MAX_PVRST_INSTANCES

#define AST_MAX_CEP_IN_PEB              8

#ifdef ISS_METRO_WANTED
#define VLAN_MAX_VID_TRANS_ENTS         4094
#endif /*ISS_METRO_WANTED*/
#define AST_MAX_SERVICES_PER_CUSTOMER   128

#define VLAN_DEV_MAX_L2_TABLE_SIZE      16384   /* L2 unicast table size */
#define VLAN_DEV_MAX_MCAST_TABLE_SIZE   512 /*256   SL>  512 is OK */ /* L2 multicast table size */

#define IP_DEV_MAX_TNL_INTF             20  /* Maximum IP interfaces */
#define IP_DEV_MAX_L3VLAN_INTF          128  /* Maximum IP interfaces */
#define IP_DEV_L3MCAST_TABLE_SIZE       256  /* L3 multicast table size */
#define IP_DEV_MAX_ROUTE_TBL_SZ         1024 /* Maximum route entries */
#define IP_DEV_MAX_IP_TBL_SZ            1024 /* Maximum ARP table size */


/* cpssDxChTrunk.c: NUM_TRUNKS_127_CNS=127*/
#define LA_DEV_MAX_TRUNK_GROUP          8   /* SL> 8 is enough */ /* Maximum Trunk groups */
/*NSK : ISSL:Regression change : Trunk port from 127 to 8 */
/* cpssDxChTrunk.c: DESIGNATED_NUM_ENTRIES_8_CNS=8*/
#define LA_MAX_PORTS_PER_AGG            8     /* Maximum no.of ports in aggregation */
#define LA_MIN_PORT_INDEX_PER_AGG SYS_DEF_NUM_PHYSICAL_INTERFACES + 1
#define LA_MAX_PORT_INDEX_PER_AGG SYS_DEF_NUM_PHYSICAL_INTERFACES + LA_DEV_MAX_TRUNK_GROUP

/* Maximum number of ports that can be configured as members of a
 *  * single aggregator;
 *   * Notes: Max num of active ports in an aggreagtor = LA_MAX_PORTS_PER_AGG */
#define LA_MAX_CONFIGURED_PORTS_PER_AGG     MAX_PHYSICAL_IFIDX

/* maximum value for interface index with trunks */
/* 24G+4XG = 28 + 127 = 155; 48G+4XG = 52 +127 = 179 */
                                    /* SL> remove to npfile. This file contain const which user can customize */
#   define MAX_IFIDX                (MAX_PHYSICAL_IFIDX +       \
                                                                          LA_DEV_MAX_TRUNK_GROUP)

/* [TBD] - define correct values for IP  -IP_DEV_MAX_L3VLAN_INTF */
#define MIN_IP_VLAN_IFIDX            (MAX_IFIDX + 1)
#define MAX_IP_VLAN_IFIDX            (MIN_IP_VLAN_IFIDX + IP_DEV_MAX_L3VLAN_INTF - 1)

#define MIN_IP_IFIDX                 1
#define MAX_IP_IFIDX                 MAX_IP_VLAN_IFIDX

#define VLAN_DEV_MAX_ST_UCAST_ENTRIES   256 /* SL>  Ok  Maximum number of L2 static
                                                   * unicast entries 1% of the
                                                   *     * maximum L2 entries
                                                   *         */
#define VLAN_DEV_MAX_MCAST_ENTRIES      512 /* SL>  Ok Maximum number of L2 static
                                                   * multicast entries. 50% of the
                                                   *     * maximum L2 multicast entries
                                                   *         */

#define ISS_MAX_L2_FILTERS (ACL_MAX_L2_FILTERS)
#define ISS_MAX_L3_FILTERS (ACL_MAX_L3_STD_FILTERS + ACL_MAX_L3_EXT_FILTERS)
#define ISS_MAX_L4S_FILTERS 20

#define ISS_FILTER_SHADOW_MEM_SIZE      4  /* Filter Shadow Size */

#define IP_DEV_MAX_ADDR_PER_IFACE       5   /* Maximum IP address associated
                                                                                              with the interface */
#define RATE_LIMIT_MAX_VALUE                0x3ffff

#define IP_DEF_MAX_STATIC_ROUTES 100 /* NSK should it changed as params.h*/

#define SYS_DEF_MAX_NUM_CONTEXTS        256


/* ACL */

/*
 *  * There are 36 ACL filters available. They must be divided between
 *   * L2 filters, standard L3 filters and extended L3 filters.
 *    */

#define ACL_MAX_L2_FILTERS 12
#define ACL_MAX_L3_STD_FILTERS 12
#define ACL_MAX_L3_EXT_FILTERS 12

/*DIFFSERV*/

/*
 *  * Refresh count should be updated based on the target support.
 *   * As of now these values are un-supported.
 *    * */
#define NP_DIFFSRV_MIN_REFRESH_COUNT 64   /* Kb/s */
#define NP_DIFFSRV_MAX_REFRESH_COUNT 1048576 /* (1024*1024)Kb/s */

#define   SYS_DEF_MAX_NBRS                  10
#define QOS_MAX_REGEN_INNER_PRIORITY_VAL    (7)

/* RIP */
#define   RIP_DEF_MAX_ROUTES                100

/* TCP */
#define   TCP_DEF_MAX_NUM_OF_TCB            500

/* PIM */
#define   PIM_DEF_MAX_SOURCES               100
#define   PIM_DEF_MAX_RPS                   100

/* DVMRP */
#define   DVMRP_DEF_MAX_SOURCES             100


/* OSPF */
#define   OSPF_DEF_MAX_LSA_PER_AREA         128
#define   OSPF_DEF_MAX_EXT_LSAS             512
#define   OSPF_DEF_MAX_ROUTES               256

/* BGP */
#define   BGP_DEF_MAX_PEERS                 50
#define   BGP_DEF_MAX_ROUTES                500
#define   BGP_DEF_MAX_CAPS_PER_PEER         10
#define   BGP_DEF_MAX_CAP_DATA_SIZE         16
#define   BGP_DEF_MAX_INSTANCES_PER_CAP     5
#define   BGP_DEFAULT_MAX_INPUT_COMM_FILTER_TBL_ENTRIES      10000
#define   BGP_DEFAULT_MAX_OUTPUT_COMM_FILTER_TBL_ENTRIES     10000
#define   BGP_DEFAULT_MAX_INPUT_EXT_COMM_FILTER_TBL_ENTRIES  2000
#define   BGP_DEFAULT_MAX_OUTPUT_EXT_COMM_FILTER_TBL_ENTRIES 2000

/* DHCP Server */

#define DHCP_SRV_MAX_POOLS                  5
#define DHCP_SRV_MAX_HOST_PER_POOL          20

/* CLI and Telnet */

#define CLI_MAX_SESSIONS                    10
#define CLI_MAX_USERS                       20
#define CLI_MAX_GROUPS                      10

/* SSL and Web */

#define ISS_MAX_WEB_SESSIONS                10
#define ISS_MAX_SSL_SESSIONS                10

/*MPLS -> VLAN */
#define VLAN_MAX_L2VPN_ENTRIES              MAX_L2VPN_PW_VC_ENTRIES
/*
 *  * if h/w supports portlist it is set to VLAN_TRUE and otherwise
 *   * set to VLAN_FALSE.If portlist is not supported whenever the
 *    * vlan member ports are modified we have to set/reset the
 *     * added/deleted ports specificallly
 *     */
#define VLAN_HW_PORTLIST_SUPPORTED()      VLAN_TRUE

/* LLDP Module specific sizing parameters */
#define LLDP_MAX_LOC_MAN_ADDR               (IP_DEV_MAX_L3VLAN_INTF + IP_MAX_RPORT)

#define LLDP_MAX_LOC_PROTOID                1  /* As protocol identity tlv is
                                                * not supported this macro is
                                                * defined with minimum value.
                                                * Need to increase the value
                                                * when there is support.i.e,
                                                * it should be LLDP_MAX_PORTS *
                                                * Number of protocols supported
                                                * on a port.
                                                */
#define LLDP_MAX_LOC_PPVID                  240   /* 10 PPVIDs per port for
                                                     24 port system */
#define LLDP_MAX_LOC_VLAN                   VLAN_DEV_MAX_NUM_VLAN

#define LLDP_MAX_NEIGHBORS                  256  /* Maximum number of neighbors
                                                    that can be learnt */
#define LLDP_MAX_REM_MAN_ADDR               256  /* Stores at least 1 mgmt
                                                    address per neighbor */
#define LLDP_MAX_REM_UNKNOWN_TLV            10   /* Stores a total of 50
                                                    unknown TLV information */
#define LLDP_MAX_REM_ORG_DEF_INFO_TLV       10   /* Stores a total 50 org
                                                    defined unkown TLVs */
#define LLDP_MAX_REM_PROTOID                1  /* As protocol identity tlv is
                                                * not supported this macro is
                                                * defined with minimum value.
                                                * Need to increase the value
                                                * when there is support.i.e,
                                                * the value can be set to
                                                * 10 protocol id entries
                                                * for each neighbor */
#define LLDP_MAX_REM_PPVID                  256  /* 1 PPVID per neighbor */
#define LLDP_MAX_REM_VLAN                   2560 /* 10 VLAN per neighbor */

/*
 * Hold the size of platform specific structure,
 * Its value could be changed upon platform.
 */
#define VLAN_HW_STATS_ENTRY_LEN                4

#define   CFA_MAX_GIGA_ENET_MTU   9216
#define   CFA_MAX_L3_INTF_ENET_MTU      9216


#if defined (WLC_WANTED) || defined (WTP_WANTED)
#define MAX_NUM_OF_AP                   4097
#define NUM_OF_AP_SUPPORTED             4
#define MAX_NUM_OF_RADIO_PER_AP         31
#define MAX_NUM_OF_RADIOS  (NUM_OF_AP_SUPPORTED *\
      MAX_NUM_OF_RADIO_PER_AP)
#define MAX_NUM_OF_SSID_PER_WLC         50
#define MAX_NUM_OF_BSSID_PER_RADIO      16
#define MAX_NUM_OF_STA_PER_AP           32
#define MAX_NUM_OF_STA_PER_WLC          (NUM_OF_AP_SUPPORTED *\
                                         MAX_NUM_OF_STA_PER_AP)
#endif

#ifdef WLC_WANTED
#define SYS_DEF_MAX_WSS_IFACES  (MAX_NUM_OF_RADIOS +\
      MAX_NUM_OF_SSID_PER_WLC +\
                     (MAX_NUM_OF_BSSID_PER_RADIO *\
      MAX_NUM_OF_RADIOS))
#elif WTP_WANTED
#define SYS_DEF_MAX_WSS_IFACES  (MAX_NUM_OF_BSSID_PER_RADIO *\
                      SYS_DEF_MAX_RADIO_INTERFACES)
#else
#define SYS_DEF_MAX_WSS_IFACES          0
#endif

/* Number of S-Channel interfaces. Referring S-Channel interfaces
 * as SBP interfaces */

#ifdef EVB_WANTED
#define SYS_DEF_MAX_SBP_IFACES          0
#else
#define SYS_DEF_MAX_SBP_IFACES          0
#endif



