/* *******************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * 
 *  Id: wssmac.h,v 1.2 2013/07/10 13:22:01 siva Exp $
 *
 * Description: This file contains the exported definitions and
 *              macros of WSSMAC Module.
 * *******************************************************************/
#ifndef _WSSMAC_H_
#define _WSSMAC_H_

typedef struct UINT8 {
   UINT4 u4HiWord;
   UINT4 u4LoWord;
}tUINT8;
#define WSSMAC_SSID_ELEMENT_ID               0x00
#define WSSMAC_SUPP_RATE_ELEMENT_ID          0x01
#define WSSMAC_EXT_SUPP_RATE_ELEMENTID       0x32    /* Decimal 50 */
#define WSSMAC_REQINFO_ELEMENTID             0x0a    /* Decimal 10 */
#define WSSMAC_VENDOR_INFO_ELEMENTID         0xdd    /* Decimal 221 */
#define WSSMAC_DS_PARAM_ELEMENT_ID           0x03
#define WSSMAC_TIM_ELEMENT_ID                0x05
#define WSSMAC_COUNTRY_ELEMENT_ID            0x07
#define WSSMAC_POWCONST_ELEMENT_ID           0x20    /* Decimal 32 */
#define WSSMAC_CHSWITCHANN_ELEMENT_ID        0x25    /* Decimal 37 */
#define WSSMAC_QUIET_ELEMENT_ID              0x28    /* Decimal 40 */
#define WSSMAC_TPC_REPORT_ELEMENT_ID         0x23    /* Decimal 35 */
#define WSSMAC_BSS_LOAD_ELEMENT_ID           0x0b    /* Decimal 11 */
#define WSSMAC_EDCA_PARAM_ELEMENT_ID         0x0c    /* Decimal 12 */
#define WSSMAC_POW_CAP_ELEMENT_ID            0x21    /* Decimal 33 */
#define WSSMAC_SUPP_CH_ELEMENT_ID            0x24    /* Decimal 36 */
#define WSSMAC_QOS_CAP_ELEMENT_ID            0x2e    /* Decimal 46 */
#define WSSMAC_CHALNG_TXT_ELEMENT_ID         0x10    /* Decimal 16 */  
#define WSSMAC_HT_CAPAB_ELEMENT_ID           0x2d    /* Decimal 45 */
#define WSSMAC_BSS_COEXIST_ELEMENT_ID        0x48    /* Decimal 72 */
#define WSSMAC_EXT_CAPAB_ELEMENT_ID          0x7f    /* Decimal 127 */
#define WSSMAC_HT_OPE_ELEMENT_ID             0x3d    /* Decimal 61 */
#define WSSMAC_OBSS_SCAN_PARAM_ELEMENT_ID    0x4a    /* Decimal 74 */     
#define WSSMAC_RSN_ELEMENT_ID                0x30    /* Decimal 48 */ 
#define WSSMAC_WPA_ELEMENT_ID                0xdd    /* Decimal 221 */ 
#define WSSMAC_VHT_CAPAB_ELEMENT_ID          0xBF    /* Decimal 191*/
#define WSSMAC_VHT_OPE_ELEMENT_ID            0xC0    /* Decimal 192*/
#define WSSMAC_VHT_WIDE_BANDWIDTH_ID         0xC2    /* Decimal 194*/
#define WSSMAC_VHT_TRANSMIT_POWER_ID         0xC3    /* Decimal 195*/
#define WSSMAC_VHT_CHANNEL_WRAPPER_ID        0xC4    /* Decimal 196*/
#define WSSMAC_VHT_OPERATING_MODE_NOTIFICATION      0xC7    /* Decimal 199*/
#define WSSMAC_TPC_REQUEST_ELEMENT_ID     0x22   /*Decimal 34*/
#ifdef PMF_WANTED
#define WSSMAC_TIMEOUT_INTERVAL_ELEMENT_ID     0x38   /*Decimal 56*/
#endif
#define WSSMAC_HT_SCAN_BEACON_UPDATE         0XC8    /* Decimal 200*/

#define WSS_RF_GROUP_ID_MAX_LEN         19
#define WSSRF_GROUP_ID_OUI              0xf25001
#define MAX_SSID_LEN                    32
#define WSSMAC_TPC_REPORT_ELEMENT_LEN     2 
#define WSSMAC_POWCONST_ELEMENT_LEN       1

/* Macros used by WSS MAC */
#define WSSMAC_MAC_ADDR_LEN             6
#define WSSMAC_TIMESTAMP_LEN            8
#define WSSMAC_VEND_INFO_ELEM           10
#define WSSMAC_MAX_SSID_LEN             32
#define WSSMAC_MAX_SUPP_RATE_LEN        8
#define WSSMAC_MAX_VIRT_BITMAP_LEN      251
#define WSSMAC_MAX_COUNTRY_STR_LEN      3
#define WSSMAC_MAX_RAND_TABLE_LEN       1   /* N, Need to update the value */
#define WSSMAC_MAX_NUM_OF_CHANL_SUPP    1
#define WSSMAC_MAX_EXT_SUPP_RATES       255
#define WSSMAC_MAX_CIPH_SUIT_CNT        1   /* m, Need to update the value */
#define WSSMAC_MAX_AKM_SUIT_CNT         1   /* n, Need to update the value */
#define WSSMAC_MAX_PKMID_CNT            1   /* s, Need to update the value */
#define WSSMAC_MAX_OUI_SIZE             3
#define WSSMAC_WMM_OUI                  0xf25000
#define WSSMAC_WPA_OUI                  0x01f25000
#ifdef WPS_WANTED
#define WSSMAC_WPS_OUI                  0x04f25000
#endif
#define WSSMAC_AKM_SUITE_PSK            2
#define WSSMAC_AKM_SUITE_8021x          1
#define WSSMAC_MAX_REQ_ELEM_ID          35
#define WSSMAC_MAX_CHALNG_TXT_LEN       254
#define WSSMAC_MAX_VENDOR_CONTENT       252
#define WSSMAC_MAX_ACTION_ELEM          100
#define WSSMAC_MAX_SUB_BANDS            128
#define WSSMAC_SUPP_MCS_SET             16 
#define WSSMAC_EXT_CAP                  8
#define WSSMAC_HTOPE_INFO               5
#define WSSMAC_BASIC_MCS_SET            16
#define WSSMAC_TIM_LEN                  6 
#define WSSMAC_HT_CAPAB_ELEMENT_LEN     26
#define WSSMAC_HT_OPE_ELEMENT_LEN       22
#define WSSMAC_CAPABILITY_INFO_LEN      2
#define WSSMAC_LISTEN_INTERVAL_LEN      2
#define WSSMAC_VHT_CAPAB_ELEMENT_LEN    12
#define WSSMAC_VHT_OPE_ELEMET_LEN       5
#define WSSMAC_VHT_POWER_ELEM_LEN       2
#define WSSMAC_OPER_NOTIFY_ELEM_LEN     1
#define WSSMAC_VHT_OPMODE_NOTIFY        18
#define WSSMAC_VHT_STA_CAPABILITY       0x601
#define WSSMAC_VHT_CENTER_FREQUENCY     5210
#define WSSMAC_HTCAP_SUPP_CHAN          0x0200
#define WSSMAC_ACM_BE_DISABLE           0x03
#define WSSMAC_HT_OPER_CHAN_WIDTH       0x05
#define WSSMAC_HT_OPER_SUBSET_1         0
#define WSSMAC_VHT_WIDE_BANDWIDTH_LEN   3
#ifdef PMF_WANTED
#define WSSMAC_TIMEOUT_INTERVAL_ELEMENT_LEN    5
#endif


#define DOT11_PUBLIC_ACTION             0x04
#define DOT11_EX_CHAN_ACTION            0x04
#define DOT11_ECSA                      0x04
#define WSSMAC_MEAS_REQUEST_ELEMENT_ID  0x26 /*Decimal 38*/
#define DOT11_MEAS_REQUEST              0x00 /*Decimal 0*/
#define DOT11_MEAS_REPORT               0x01

/* ***** 802.11 Frame Related ******** */
#define DOT11_HDR_FRAME_CTRL_LEN        2
#define DOT11_HDR_DURN_LEN              2
#define DOT11_HDR_MAC_ADDR_LEN          6        
#define DOT11_HDR_SEQ_CTRL_LEN          2
#define DOT11_HDR_QOS_CTRL_LEN          2

#define DOT11_ELEM_BYTE_0               0
#define DOT11_ELEM_BYTE_1               1

#define DOT11_FC_BYTE0_TYPE_ALL         0x0c
#define DOT11_FC_BYTE0_VERSION  0x00 
#define DOT11_FC_BYTE0_TYPE_MGMT        0x00
#define DOT11_FC_BYTE0_TYPE_CTRL        0x04
#define DOT11_FC_BYTE0_TYPE_DATA        0x08

#define DOT11_FC_BYTE0_SUBTYPE_ALL      0xf0
#define DOT11_FC_BYTE0_SUBTYPE_DATA     0x00
#define DOT11_FC_BYTE0_SUBTYPE_QOS      0x80

#define DOT11_FC_BYTE1_DIR_ALL          0x03
#define DOT11_FC_BYTE1_DIR_ST_ST        0x00    /* STA->STA */   
#define DOT11_FC_BYTE1_DIR_ST_AP        0x01    /* STA->AP  */
#define DOT11_FC_BYTE1_DIR_AP_ST        0x02    /* AP->STA  */
#define DOT11_FC_BYTE1_DIR_AP_AP        0x03    /* AP->AP   */

#define DOT11_FLAG_YES                  1
#define DOT11_FLAG_NO                   0

#define DOT11_MAX_PKT_SIZE  2350  

#define DOT3_ETH_HDR_SIZE       14
#define DOT2_LLC_HDR_SIZE       8
#define DOT2_LLC_DSAP_SSAP_SIZE 2
#define DOT2_LLC_SNAP_HDR_INDEX 4
#define DOT3_IP_PROTOCOL_OFFSET 10
#define DOT3_UDP_PROTO          0x11

#define DOT11_REG_HDR_SIZE      24
#define DOT11_FULL_FRAM_HDR_SIZE  30
#define DOT11_FULL_QOS_HDR_SIZE   32
#define DOT11_REG_QOS_HDR_SIZE    26

#define WSSSTA_WMM_FRAME_LENGTH             24
#define WSSSTA_WMM_OUI_TYPE                 0x02
#define WSSSTA_WMM_OUI_SUB_TYPE             0x01
#define WSSSTA_WMM_VERSION                  0x01
#define WSSSTA_WMM_POWER_SAVE_ENABLE        0x01
#define WSSSTA_WMM_POWER_SAVE_DISABLE       0x00
#define WSSSTA_WMM_RESERVED                 0x00
#define WSSSTA_WMM_ASSOC_REQ_TAG_LENGTH     0x07
#define DOT11_SPECTRUM_MGMT     0x00
#ifdef PMF_WANTED
#define DOT11_SA_QUERY       0x08
#endif
#define DOT11_QOS           0x01

#define DOT11_TPC_REPORT     0x03
#define DOT11_TPC_REQUEST     0x02
#define DOT11_CHANL_SWITCH                  0x04

#ifdef PMF_WANTED
#define DOT11_SA_QUERY_REQ 0x00
#define DOT11_SA_QUERY_RESP 0x01
#endif



/* ***** 802.3 Frame Related ********* */
#define DOT3_HDR_ADDR_LEN               6
#define DOT3_MAX_LEN   1536    /* 0x600 - Max Ethernet II */ 

/* ***** 802.2 Frame Related ********* */
#define DOT2_ORG_CODE_LEN               3
#define DOT2_UI                         0x3
#define DOT2_SNAP_LSAP                  0xaa
#define DOT2_SNAP_ORGCODE               0x00
#define DOT2_SNAP_BRTUNL_ORGCODE        0xf8
#define DOT2_ETHTYPE_AARP               0x80f3
#define DOT2_ETHTYPE_IPX                0x8137

#define DOT1X_ETHTYPE_VLAN  0x8100

#define DOT1X_ETHTYPE       0x888E
#define ETY_TYPE_OFFSET     12

#define LOCAL_ROUTING_ENABLED  1
#define LOCAL_ROUTING_DISABLED 2
#define LOCAL_BRIDGING_ENABLED  3
#define LOCAL_BRIDGING_DISABLED 2

#define WSSMAC_EXT_SUPPORT_ENABLE 1
#define DOT11N_CHANNEL_WIDTH40 40
#define DOT11N_CHANNEL_WIDTH20 20
#define DOT11N_GI_SHORT20_ENABLE    1
#define DOT11N_GI_SHORT40_ENABLE    1
#define DOT11N_SUPPORT_ENABLE       1
#define DOT11N_MCS_DEFAULT_VALUE    0xFF
#define DOT11N_AN_HT40_CHANNEL_ABOVE    0x05
#define DOT11N_AN_HT40_CHANNEL_BELOW    0x07
#define DOT11_AN40_CHANNELS    11
#define DOT11_AN_START_CHANNEL 36
#define VALUE_0                0
#define VALUE_1                1
#define VALUE_2                2
#define VALUE_3                3 
#define VALUE_4                4 
#define VALUE_8                8 
#define VALUE_10               10 
#define WSSMAC_VALUE_3         3 
#define WSSMAC_VALUE_4         4 
#define WSSMAC_VALUE_5         5 
#define BIT_1     1 
#define BIT_2     2 
#define BIT_4     4 
#define BIT_8     8 
#define DOT11AC_SUPPORT_ENABLE       1
#define DOT11N_TX_SUPP_MCS_ENABLE    1
#define DOT11_NONGREEN_FIELD_STA     0x4
#define DOT11_GREEN_FIELD_STA    0
/* MAC Data Handler Related Macros */
#define CHECK_FOR_SNAP(pLlcHdr) \
                                ( (pLlcHdr->u1LlcDsap == DOT2_SNAP_LSAP) && \
                                  (pLlcHdr->u1LlcSsap == DOT2_SNAP_LSAP) && \
                                  (pLlcHdr->unLlcType.SnapHdr.u1Control == DOT2_UI) && \
                                  (pLlcHdr->unLlcType.SnapHdr.u1OrgCode[0] == DOT2_SNAP_ORGCODE) && \
                                  (pLlcHdr->unLlcType.SnapHdr.u1OrgCode[1] == DOT2_SNAP_ORGCODE) && \
                                  (pLlcHdr->unLlcType.SnapHdr.u1OrgCode[2] == DOT2_SNAP_ORGCODE) && \
                                  (!( (pLlcHdr->unLlcType.SnapHdr.u2EthType == OSIX_HTONS(DOT2_ETHTYPE_AARP)) || \
                                      (pLlcHdr->unLlcType.SnapHdr.u2EthType == OSIX_HTONS(DOT2_ETHTYPE_IPX)) )) )

#define CHECK_FOR_SNAP_BRTUNL(pLlcHdr) \
                                ( (pLlcHdr->u1LlcDsap == DOT2_SNAP_LSAP) && \
                                  (pLlcHdr->u1LlcSsap == DOT2_SNAP_LSAP) && \
                                  (pLlcHdr->unLlcType.SnapHdr.u1Control == DOT2_UI) && \
                                  (pLlcHdr->unLlcType.SnapHdr.u1OrgCode[0] == DOT2_SNAP_ORGCODE) && \
                                  (pLlcHdr->unLlcType.SnapHdr.u1OrgCode[1] == DOT2_SNAP_ORGCODE) && \
                                  (pLlcHdr->unLlcType.SnapHdr.u1OrgCode[2] == DOT2_SNAP_BRTUNL_ORGCODE) )

#define WSSMAC_SWAP_2BYTES(u2Val)        \
        do {                                    \
           u2Val = (u2Val<<8) | (u2Val>>8); \
        } while(0)

#define WSSMAC_SWAP_4BYTES(x) \
                              x = (UINT4)(((x & 0xFF000000)>>24) | \
                              ((x & 0x00FF0000)>>8)  | \
                              ((x & 0x0000FF00)<<8 ) | \
                              ((x & 0x000000FF)<<24)   \
                             )

/* ***********************************************************
 * *************802.11 MAC Frame Message Elements*************
 * ***********************************************************/

typedef struct
{
  tUINT8                u8MESReqType_StartTime;
  UINT2                 u2MESReqType_Duration;
  UINT1                 u1MESReqType_ChannelNum;
  UINT1                 au1Reserved[1];
}tWssMeasRequestElem;
typedef struct
{
  tWssMeasRequestElem   WssMESReqMesRequest;
  UINT1                         u1MacFrameElemId;
  UINT1                         u1MacFrameElemLen;
  UINT1                 u1MacFrameMESReq_MesToken;
  UINT1                 u1MacFrameMESReq_MesReqMode;
  UINT1                 u1MacFrameMESReq_MesType;
  UINT1                 u1DialogToken;
  UINT1                 au1Reserved[2];
}tWssMacMeasRequest;

typedef struct
{
  FS_UINT8              u8MESRepType_StartTime;
  UINT2                 u2MESRepType_Duration;
  UINT1                 u1MESRepType_ChannelNum;
  UINT1                 u1MESRepType_Map;
}tWSSMeasReportElem;

typedef struct
{
  tWSSMeasReportElem    u1MacFrameMESRep_MesReport;
  UINT1                         u1MacFrameElemId;
  UINT1                         u1MacFrameElemLen;
  UINT1                 u1MacFrameMESRep_MesToken;
  UINT1                 u1MacFrameMESRep_MesRepMode;
  UINT1                 u1MacFrameMESRep_MesType;
  UINT1                 au1Reserved[3];
}tWssMacMeasReport;

/* Authentication Algorithm Number */
typedef struct {
        UINT2           u2MacFrameAuthAlgoNum;
        UINT1           au1Pad[2];
}tWssMacAuthAlgoNum;

/* Authentication Transaction Sequence Number */
typedef struct {
        UINT2           u2MacFrameAuthTransSeqNum;
        UINT1           au1Pad[2];
}tWssMacAuthTransSeqNum;

/* Status Code */
typedef struct {
        UINT2           u2MacFrameStatusCode;
        UINT1           au1Pad[2];
}tWssMacStatusCode;

/* Challenge Text */
typedef struct {
        UINT1           u1MacFrameElemId;
        UINT1           u1MacFrameElemLen;
        UINT1           au1MacFrameChalngTxt[WSSMAC_MAX_CHALNG_TXT_LEN];                /* MAX_CHALNG_TXT_LEN=254 */
}tWssMacChalngTxt;

/* Vendor Specific Information */
typedef struct {
        UINT1           u1MacFrameElemId;
        UINT1           u1MacFrameElemLen;
        UINT1           au1Pad[2];
        UINT1           au1MacFrameVend_OUI[WSSMAC_MAX_OUI_SIZE];       
        UINT1           au1Pad1[1];
        UINT1           au1MacFrameVend_Content[WSSMAC_MAX_VENDOR_CONTENT];     
}tWssMacVendInfo;

/* Reason Code */
typedef struct {
        UINT2           u2MacFrameReasonCode;
        UINT1           au1Pad[2];
}tWssMacReasonCode;

/* Timestamp */
typedef struct {
        UINT1           au1MacFrameTimestamp[WSSMAC_TIMESTAMP_LEN];
}tWssMacTimestamp;

/* 802.11 MAC Frame Message Elements - Beacon Interval */
typedef struct {
        UINT2           u2MacFrameBeaconInt;
        UINT1           au1Pad[2];
}tWssMacBeaconInt;

/* Capability */
typedef struct {
        UINT2           u2MacFrameCapability;
        UINT1           au1Pad[2];
}tWssMacCapability;

/* SSID */
typedef struct {
        UINT1           u1MacFrameElemId;
        UINT1           u1MacFrameElemLen;
        UINT1      au1Pad[2];
        UINT1           au1MacFrameSSID[WSSMAC_MAX_SSID_LEN];   /* SSID, MAX_SSID_LEN=32 */
}tWssMacSSID;

/* Supported Rates */
typedef struct {
        UINT1           u1MacFrameElemId;
        UINT1           u1MacFrameElemLen;
     UINT1      au1Pad[2];
        UINT1           au1MacFrameSuppRate[WSSMAC_MAX_SUPP_RATE_LEN];
}tWssMacSuppRate;

/* FH Parameter Set */
typedef struct {
        UINT2           u2MacFrameFH_DwellTime;
        UINT1           u1MacFrameElemId;
        UINT1           u1MacFrameElemLen;
        UINT1           u1MacFrameFH_HopSet;
        UINT1           u1MacFrameFH_HopPattern;
        UINT1           u1MacFrameFH_HopIndex;
        UINT1           u1Pad;
}tWssMacFHParamSet;

/* DS Parameter Set */
typedef struct {
        UINT1           u1MacFrameElemId;
        UINT1           u1MacFrameElemLen;
        UINT1           u1MacFrameDS_CurChannel;
        UINT1           u1Pad;
}tWssMacDSParamSet;

/* CF Parameter Set */
typedef struct {
        UINT2           u2MacFrameCFP_MaxDur;
        UINT2           u2MacFrameCFP_DurRem;
        UINT1           u1MacFrameElemId;
        UINT1           u1MacFrameElemLen;
        UINT1           u1MacFrameCFP_Count;
        UINT1           u1MacFrameCFP_Period;
}tWssMacCFParamSet;

/* IBSS Parameter Set */
typedef struct {
        UINT2           u2MacFrameIBSS_AtimWind;
        UINT1           u1MacFrameElemId;
        UINT1           u1MacFrameElemLen;
}tWssMacIBSSParamSet;

/* Traffic Indication Map (TIM) */
typedef struct {
        UINT1           u1MacFrameElemId;
        UINT1           u1MacFrameElemLen;
        UINT1           u1MacFrameTIM_DtimCnt;
        UINT1           u1MacFrameTIM_DtimPer;
        UINT1           u1MacFrameTIM_BitCtrl;
        UINT1  au1Pad[3];
        UINT1           au1MacFrameTIM_PartVirtBitmap[WSSMAC_MAX_VIRT_BITMAP_LEN];  /* MAX_VIRT_BITMAP_LEN=251 */
 UINT1  au1Pad1[1];
}tWssMacTIMParamSet;

/* Country */
typedef struct {
        UINT1           u1MacFrameElemId;
        UINT1           u1MacFrameElemLen;
        UINT1  au1Pad[2];
        UINT1           au1MacFrameCountryStr[WSSMAC_MAX_COUNTRY_STR_LEN];
 UINT1  au1Pad1[1];
        UINT1           u1MacFrameFirChanlNum_RegExtnId;
        UINT1           u1MacFrameNumOfChanl_RegClass;
        UINT1           u1MacFrameMaxTxPowLev_CovClass;
        UINT1           u1MacFramePad;
}tWssMacCountry;

/* FH Pattern Parameters */
typedef struct {
        UINT1           u1MacFrameElemId;
        UINT1           u1MacFrameElemLen;
        UINT1           u1MacFrameFHPatt_PrimRadix;
        UINT1           u1MacFrameFHPatt_NumOfChanl;
}tWssMacFHPattern;

/* FH Pattern Table */
typedef struct {
        UINT1           u1MacFrameElemId;
        UINT1           u1MacFrameElemLen;
        UINT1           u1MacFrameFHTab_Flag;
        UINT1           u1MacFrameFHTab_NumOfSet;
        UINT1           u1MacFrameFHTab_Mod;
        UINT1           u1MacFrameFHTab_Offset;
        UINT1           au1MacFrameFHTab_RandTab[WSSMAC_MAX_RAND_TABLE_LEN];    /* MAX_RAND_TABLE_LEN=N */
        UINT1           u1Pad;
}tWssMacFHPattTable;

/* Power Constraint */
typedef struct {
        UINT1           u1MacFrameElemId;
        UINT1           u1MacFrameElemLen;
        UINT1           u1MacFrameLocPowConst;
        UINT1           u1Pad;
}tWssMacPowConstraint;

/* Channel Switch Announcement */
typedef struct {
        UINT1           u1MacFrameElemId;
        UINT1           u1MacFrameElemLen;
        UINT1           u1MacFrameChanlSwit_Mode;
        UINT1           u1MacFrameChanlSwit_NewChanlNum;
        UINT1           u1MacFrameChanlSwit_Cnt;
        UINT1           u1OperClass;
        UINT1           au1Pad[2];
}tWssMacChanlSwitAnnounce;

/* Quiet */
typedef struct {
        UINT2           u2MacFrameQuiet_Duration;
        UINT2           u2MacFrameQuiet_Offset;
        UINT1           u1MacFrameElemId;
        UINT1           u1MacFrameElemLen;
        UINT1           u1MacFrameQuiet_Count;
        UINT1           u1MacFrameQuiet_Period;
}tWssMacQuiet;

/* IBSS DFS Channel map */
typedef struct {
        UINT1           u1MacFrameIBSS_ChanlMap_ChanlNum;
        UINT1           u1MacFrameIBSS_ChanlMap_Map;
        UINT1           au1Pad[2];
}tWssMacIBSS_ChanlMap;

/* IBSS DFS */
typedef struct {
        UINT1                   u1MacFrameElemId;
        UINT1                   u1MacFrameElemLen;
        UINT1                   au1MacFrameIBSS_DFSOwner[WSSMAC_MAC_ADDR_LEN];  /* MAC_ADDR_LEN=6 */
        UINT1                   u1MacFrameIBSS_DFSRecInt;
        UINT1           au1Pad[3];
        tWssMacIBSS_ChanlMap    aMacFrameIBSS_ChanlMap[2*WSSMAC_MAX_NUM_OF_CHANL_SUPP]; /* Need to update value for WSSMAC_MAX_NUM_OF_CHANL_SUPP */
}tWssMacIBSS_DFS;

/* TPC Report */
typedef struct {
        UINT1           u1MacFrameElemId;
        UINT1           u1MacFrameElemLen;
        UINT1           u1MacFrameTPCRep_TxPow;
        UINT1           u1MacFrameTPCRep_LnkMargin;
}tWssMacTPCReport;

/*TPC Request*/
typedef struct {
 UINT1  u1MacFrameElemId;
 UINT1  u1MacFrameElemLen;
 UINT1  u1DialogToken;
 UINT1  u1Pad;
}tWssMacTPCRequest;

/* ERP Information */
typedef struct {
        UINT1           u1MacFrameElemId;
        UINT1           u1MacFrameElemLen;
        UINT1           u1MacFrameERPInfo;
        UINT1           u1Pad; 
}tWssMacERPInfo;

/* Extended Supported Rates */
typedef struct {
        UINT1           u1MacFrameElemId;
        UINT1           u1MacFrameElemLen;
        UINT1           au1Pad[2];
        UINT1           au1MacFrameExtSuppRates[WSSMAC_MAX_EXT_SUPP_RATES];     /* MAX_EXT_SUPP_RATES=255*/
        UINT1           au1Pad1[1];
}tWssMacExtSuppRates;

typedef struct {
        UINT1           au1MacFrameRSN_CiphSuit_OUI[WSSMAC_MAX_OUI_SIZE];       /* MAX_OUI_SIZE=3 */
        UINT1           u1MacFrameRSN_CiphSuit_Type;
}tWssMacRSN_CiphSuit;

typedef struct {
        UINT1           au1MacFrameRSN_AkmSuit_OUI[WSSMAC_MAX_OUI_SIZE];       /* MAX_OUI_SIZE=3 */
        UINT1           u1MacFrameRSN_AkmSuit_Type;
}tWssMacRSN_AkmSuit;


/* RSN Information */
#pragma pack (1)
typedef struct {
        UINT1                   u1MacFrameElemId;
        UINT1                   u1MacFrameElemLen;
        UINT2                   u2MacFrameRSN_Version;
        UINT4                   u4MacFrameRSN_GrpCipherSuit;
        UINT2                   u2MacFrameRSN_PairCiphSuitCnt;
        tWssMacRSN_CiphSuit     aMacFrameRSN_PairCiphSuitList[WSSMAC_MAX_CIPH_SUIT_CNT];
        UINT2                   u2MacFrameRSN_AKMSuitCnt;
        tWssMacRSN_AkmSuit      aMacFrameRSN_AKMSuitList[WSSMAC_MAX_AKM_SUIT_CNT];
        UINT2                   u2MacFrameRSN_Capability;
        UINT2                   u2MacFrameRSN_PMKIdCnt;
        UINT1                   au1MacFrameRSN_PMKIdList[16 * WSSMAC_MAX_PKMID_CNT];      /* MAX_PKMID_CNT=s*/
 UINT1   u1KeyStatus;   
 UINT1   au1Pad[3];
}tWssMacRSNInfo;
#pragma pack ()
/* BSS Load */
typedef struct {
        UINT2           u2MacFrameBSSLoad_AdmnCap;
        UINT2           u2MacFrameBSSLoad_StatnCnt;
        UINT1           u1MacFrameElemId;
        UINT1           u1MacFrameElemLen;
        UINT1           u1MacFrameBSSLoad_ChanlUtil;
        UINT1           u1Pad;
}tWssMacBSSLoad;

typedef struct {
        UINT2           u2MacFrameEDCA_AC_TxOpLimit;
        UINT1           u1MacFrameEDCA_AC_ACI_AFSN;
        UINT1           u1MacFrameEDCA_AC_ECWMin_Max;
}tWssMacEDCA_AC;

/* WMM Parameter set */
typedef struct {
        UINT1 au1OUI[WSSMAC_MAX_OUI_SIZE];
        UINT1 u1MacFrameElemId;
        UINT1 u1MacFrameElemLen;
        UINT1 u1OUIType;
        UINT1 u1OUISubType;
        UINT1 u1WMMVersion;
        UINT1 u1QOSInfo;
        UINT1 u1Rsvrd;
        UINT1 au1Pad[2];
}tWssMacWmmParam;

/* EDCA Parameter Set */
typedef struct {
        tWssMacEDCA_AC          MacFrameEDCA_AC_BE;
        tWssMacEDCA_AC          MacFrameEDCA_AC_BK;
        tWssMacEDCA_AC          MacFrameEDCA_AC_VI;
        tWssMacEDCA_AC          MacFrameEDCA_AC_VO;
        UINT1                   u1MacFrameElemId;
        UINT1                   u1MacFrameElemLen;
        UINT1                   u1MacFrameEDCA_QosInfo;
        UINT1                   u1MacFrameEDCA_Rsrvd;
}tWssMacEDCAParam;

/* QoS Capability */
typedef struct {
        UINT1           u1MacFrameElemId;
        UINT1           u1MacFrameElemLen;
        UINT1           u1MacFrameQosInfo;
        UINT1           u1Pad;   
}tWssMacQosCap;

/* Request Information */
typedef struct {
        UINT1           u1MacFrameElemId;
        UINT1           u1MacFrameElemLen;
        UINT1           au1MacFrameReqID[WSSMAC_MAX_REQ_ELEM_ID];     /* WSSMAC_MAX_REQ_ELEM_ID= N */
        UINT1           au1Pad[3];
}tWssMacReqInfo;

/* Listen Interval */
typedef struct {
        UINT2           u2MacFrameListnInt;
        UINT1           au1Pad[2];
}tWssMacListenInt;

/* Power Capability */
typedef struct {
        UINT1           u1MacFrameElemId;
        UINT1           u1MacFrameElemLen;
        UINT1           u1MacFramePowCap_MinTxPow;
        UINT1           u1MacFramePowCap_MaxTxPow;
}tWssMacPowCapability;

typedef struct {
        UINT1           u1MacFrameSuppChanl_FirChanlNum;
        UINT1           u1MacFrameSuppChanl_NumOfChanl;
}tWssMacSubBandDes;

/* Supported Channels */
typedef struct {
        UINT1           u1MacFrameElemId;
        UINT1           u1MacFrameElemLen;
        tWssMacSubBandDes  SubBandDes[WSSMAC_MAX_SUB_BANDS];
        UINT1           au1Pad[2];
}tWssMacSuppChanls;

/* AID */
typedef struct {
        UINT2           u2MacFrameAID;
        UINT1           au1Pad[2];
}tWssMacAID;

/* Current AP Address */
typedef struct {
        UINT1           au1MacFrameCurrAPAddr[WSSMAC_MAC_ADDR_LEN];
        UINT1           au1Pad[2];
}tWssMacCurrAPAddr;

/* MAC Address */
typedef struct {
 UINT1   u1SrcMacAddr[WSSMAC_MAC_ADDR_LEN];
 UINT1  u1DestMacAddr[WSSMAC_MAC_ADDR_LEN];
 UINT1   u1BSSIDMacAddr[WSSMAC_MAC_ADDR_LEN];
    UINT1           au1Pad[2];
}tWssMacMacAddr;

/* HT Capabilities */
typedef struct {
    UINT4       u4TranBeamformCap;
    UINT2       u2HTCapInfo;
    UINT2       u2HTExtCap;
    UINT1       au1SuppMCSSet[WSSMAC_SUPP_MCS_SET];
    UINT1       u1MacFrameElemId;
    UINT1       u1MacFrameElemLen;
    UINT1       u1AMPDUParam;
    UINT1       u1ASELCap;
}tHTCapabilities;

/*rouge ap detection*/
typedef struct {
    UINT1       au1RougeGroupId[WSS_RF_GROUP_ID_MAX_LEN];
    UINT1       au1OUI[3];
    UINT1       u1OUIType;
    UINT1       u1MacFrameElemId;
    UINT1       u1MacFrameElemLen;
    UINT1       au1Pad[3];
}tRougeApDetection;

typedef struct {
         UINT1      au1OUI[3];
         UINT1      u1OUIType;
         UINT1      u1MacFrameElemId;
         UINT1      u1RsnaStatus;
         UINT1      u1MacFrameElemLen;
         UINT1      u1WpaInfoElement;
         UINT1      u2WpaVersion;
         UINT1      u1MulticastCipherType;
         UINT1      u2MulticastCipherCount;
         UINT1      u1UnicastCipherAES;
         UINT1      u2UnicastSuiteCount;
         UINT1      u1UnicastCipherTkip[2];
         UINT1      u1KeyManagementType;
}tWpaProtection;

typedef struct {
    UINT4 u4VhtCapaInfo;
    UINT1 au1SuppMCS[8];
    UINT1 u1ElemId;
    UINT1 u1ElemLen;
    UINT1 au1Pad[2];
}tVHTCapability;

typedef struct {
    UINT1 au1VhtOperInfo[3];
    UINT1 u1ElemId;
    UINT2 u2BasicMCS;
    UINT1 u1ElemLen;
    UINT1 u1Pad;
}tVHTOperation;

typedef struct {
    UINT1 u1ElemId;
    UINT1 u1ElemLen;
    UINT1 u1VhtOperModeElem;
    BOOL1 isOptional;
}tOperModeNotify;

typedef struct {
 UINT1  u1ElemLen;
 UINT1  u1ElemId;
 BOOL1  isOptional;
 UINT1  u1ChanWidth;
 UINT1  u1CenterFcy0;
 UINT1  u1CenterFcy1;
 UINT1  au1Pad[2];
}tChanwidthSwitch;

typedef struct {
 UINT1 u1ElemLen;
 UINT1 u1ElemId;
 BOOL1 isOptional;
 UINT1 u1TransPower;
 UINT1 u1TxPower20;
 UINT1 u1TxPower40;
 UINT1 u1TxPower80;
 UINT1 u1TxPower160;
 UINT1 u1ChanWidth;
 UINT1 au1Pad[3];
}tVhtTxPower;

typedef struct {
 BOOL1 isOptional;
 UINT1 u1ElemId;
 UINT1 au1Pad[2];
}tNewCountry;

typedef struct  {
 tChanwidthSwitch ChanwidthSwitch;
 tVhtTxPower VhtTxPower;
 tNewCountry NewCountry;
}tWrapperSubElem;

typedef struct {
 UINT1 u1ElemLen; /**/
 UINT1 u1ElemId;
 BOOL1 isOptional;
 UINT1 u1Pad;
 tWrapperSubElem WrapperSubElem;
}tChanSwitchWrapper;

/* Bss Coexistence */
typedef struct {
    UINT1       u1MacFrameElemId;
    UINT1       u1MacFrameElemLen;
    UINT1       u1BSSCoexistInfoFields;
    UINT1       u1Pad;
}tBssCoexistence;

/* Ext Capabilities */
typedef struct {
    UINT1       u1MacFrameElemId;
    UINT1       u1MacFrameElemLen;
    UINT1       au1Pad[2];
    UINT1       au1Capabilities[WSSMAC_EXT_CAP];
}tExtCapabilities;

#ifdef PMF_WANTED
/* Timeout Interval Element */

typedef struct {
   UINT1 u1MacFrameElemId;
   UINT1 u1MacFrameElemLen;
   UINT1 u1TimeoutIntervalType;
   UINT1 au1Pad[1];
   UINT4 u4TimeoutIntervalValue; 
}tTimeoutInterval;
#endif

/* HT Operation */
typedef struct {
    UINT1       u1MacFrameElemId;
    UINT1       u1MacFrameElemLen;
    UINT1       u1PrimaryCh;
    UINT1       au1HTOpeInfo[WSSMAC_HTOPE_INFO];
    UINT1       au1BasicMCSSet[WSSMAC_BASIC_MCS_SET];
}tHTOperation;

/* OBSS Scan Params */
typedef struct {
    UINT2       u2OBSSScanPassiveDwell;
    UINT2       u2OBSSScanActiveDwell;
    UINT2       u2BSSChaWidTriScanInt;
    UINT2       u2OBSSScanPassiveTotalPerCh;
    UINT2       u2OBSSScanActiveTotalPerCh;
    UINT2       u2BSSWidChTransDelayFactor;
    UINT2       u2OBSSScanActivityThreshold;
    UINT1       u1MacFrameElemId;
    UINT1       u1MacFrameElemLen;
}tOBSSScanParams;

typedef struct {
    UINT2 u2FirstChannelNo;
    UINT2 u2NoOfChannels;
    UINT2 u2MaxTxPowerLevel;
    UINT1 au1Reserved[2]; /* Padding */
}tMultiDomainInfo;
/* ********************************************************* *
 * ********************802.11 MAC Frame********************* *
 * ********************************************************* */

/* 802.11 MAC Header */
typedef struct {
        UINT1           u1MacFrameAddr4[WSSMAC_MAC_ADDR_LEN]; 
        UINT2           u2MacFrameSeqCtrl; 
        UINT1           u1MacFrameAddr2[WSSMAC_MAC_ADDR_LEN];
        UINT2           u2MacFrameQosCtrl;
        UINT1           u1MacFrameAddr1[WSSMAC_MAC_ADDR_LEN];
        UINT2           u2MacFrameCtrl;
        UINT1           u1MacFrameAddr3[WSSMAC_MAC_ADDR_LEN]; 
        UINT2           u2MacFrameDurId;

}tWssMacHeader;

/* 802.11 MAC frame */
typedef struct {
        tWssMacHeader           MacFrameHdr;
        UINT1                   *pu1MacFrameBody; 
        UINT4                   u4MacFrameFCS; 

}tWssMacDot11Frame;

/* 802.11 Management frame MAC Header */
typedef struct {
        UINT1           u1DA[WSSMAC_MAC_ADDR_LEN];
        UINT2           u2MacFrameCtrl; 
        UINT1           u1SA[WSSMAC_MAC_ADDR_LEN]; 
        UINT2           u2MacFrameDurId; 
        UINT1           u1BssId[WSSMAC_MAC_ADDR_LEN];
        UINT2           u2MacFrameSeqCtrl;

}tWssMacMgmtFrmHdr;

/* 802.11 MAC Frame Raw Buffer */
typedef struct {
        UINT4                   u4IfIndex;
        tCRU_BUF_CHAIN_DESC     *pDot11MacPdu; 
        tMacAddr                BssIdMac;
        UINT2                   u2SessId;
        UINT2                   u2VlanId;
        UINT1                   u1MacType;
        UINT1                   au1Pad[1];
}tWssMacDot11PktBuf;

/* *********************************************************** *
 * ***********802.11 Management Frame Structures************** * 
 * *********************************************************** */

/* Represents 802.11 MAC beacon frame */
typedef struct {
        tWssMacVendInfo             aVendSpecInfo[WSSMAC_VEND_INFO_ELEM];
        tWssMacExtSuppRates         ExtSuppRates;
        tWssMacTIMParamSet          TIMParamSet;
        tWssMacRSNInfo              RSNInfo;
     tWssMacSSID              Ssid; 
        tWssMacMgmtFrmHdr           MacMgmtFrmHdr;
        tWssMacEDCAParam            EDCAParam;
        tWssMacCountry              Country;
     tWssMacSuppRate             SuppRate; 
     tWssMacTimestamp         TimeStamp;
        tWssMacFHParamSet           FHParamSet;
        tWssMacCFParamSet           CFParamSet;
        tWssMacFHPattTable          FHPattTable;
        tWssMacChanlSwitAnnounce    ChanlSwitAnnounce;
        tWssMacQuiet                Quiet;
        tWssMacBSSLoad              BSSLoad;
      tWssMacBeaconInt         BeaconInt;
     tWssMacCapability         Capability; 
        tWssMacDSParamSet           DSParamSet;
        tWssMacFHPattern            FHPattern;
        tWssMacPowConstraint        PowConstraint;
        tWssMacTPCReport            TPCReport;
        tWssMacERPInfo              ERPInfo;
        tWssMacQosCap               QosCap;
        tHTCapabilities             HTCapabilities;
 tVHTCapability          VHTCapabilities;
 tVHTOperation           VHTOperation;
    tVhtTxPower             VhtTxPower;
    tChanSwitchWrapper      ChanSwitchWrapper;
    tOperModeNotify         OperModeNotify;
        tBssCoexistence             BssCoexistence;
        tExtCapabilities            ExtCapabilities;
        tHTOperation                HTOperation;
        tOBSSScanParams             OBSSScanParams;

}tDot11BeaconMacFrame;

/* Represents 802.11 MAC Probe Request frame */
typedef struct {
        tWssMacMgmtFrmHdr       MacMgmtFrmHdr;
        tWssMacSSID             Ssid;
        tWssMacSuppRate         SuppRate;
        tWssMacReqInfo          ReqInfo;
        tWssMacExtSuppRates     ExtSuppRates;
        tHTCapabilities         HTCapabilities;
        tVHTCapability          VHTCapabilities;
        tOperModeNotify         OperModeNotify;
        tBssCoexistence         BssCoexistence;
        tExtCapabilities        ExtCapabilities;
        tWssMacVendInfo         aVendSpecInfo[WSSMAC_VEND_INFO_ELEM];
        UINT2                   u2SessId;
        UINT1                   au1Pad[38];

}tDot11ProbReqMacFrame;


/* Represents 802.11 MAC probe response frame body  */
typedef struct {
        tWssMacMgmtFrmHdr           MacMgmtFrmHdr;
     tWssMacTimestamp         TimeStamp;
     tWssMacBeaconInt         BeaconInt; 
     tWssMacCapability         Capability; 
     tWssMacSSID              Ssid;
     tWssMacSuppRate          SuppRate;
        tWssMacFHParamSet           FHParamSet;
        tWssMacDSParamSet           DSParamSet;
        tWssMacCFParamSet           CFParamSet;
        tWssMacCountry              Country;
        tWssMacFHPattern            FHPattern;
        tWssMacFHPattTable          FHPattTable;
        tWssMacPowConstraint        PowConstraint;
        tWssMacChanlSwitAnnounce    ChanlSwitAnnounce;
        tWssMacQuiet                Quiet;
        tWssMacTPCReport            TPCReport;
        tWssMacERPInfo              ERPInfo;
        tWssMacExtSuppRates         ExtSuppRates;
        tWssMacRSNInfo              RSNInfo;
        tWssMacBSSLoad              BSSLoad;
        tWssMacEDCAParam            EDCAParam;
        tHTCapabilities             HTCapabilities;
        tVHTCapability              VHTCapabilities;
        tVHTOperation               VHTOperation;
        tVhtTxPower                 VhtTxPower;
        tChanSwitchWrapper          ChanSwitchWrapper;
        tOperModeNotify             OperModeNotify;
        tBssCoexistence             BssCoexistence;
        tExtCapabilities            ExtCapabilities;
        tHTOperation                HTOperation;
        tOBSSScanParams             OBSSScanParams;
        tWssMacVendInfo             aVendSpecInfo[WSSMAC_VEND_INFO_ELEM];
        tWssMacReqInfo              aReqInfo[2];
        UINT2                       u2SessId;
        UINT1                       au1Pad[2];

}tDot11ProbRspMacFrame;

/* Represents 802.11 MAC Authentication frame */
typedef struct {
        tWssMacMgmtFrmHdr       MacMgmtFrmHdr;
        tWssMacAuthAlgoNum      AuthAlgoNum;
        tWssMacAuthTransSeqNum  AuthTransSeqNum;
        tWssMacStatusCode       StatCode;
        tWssMacChalngTxt        ChalngTxt;
        tWssMacVendInfo         aVendSpecInfo[WSSMAC_VEND_INFO_ELEM];
        UINT2                   u2SessId;
        UINT1                   au1Pad[2];

}tDot11AuthMacFrame;

/* Represents 802.11 MAC Deauthentication frame */
typedef struct {
        tWssMacMgmtFrmHdr       MacMgmtFrmHdr;
        tWssMacReasonCode       ReasonCode;
        tWssMacVendInfo         aVendSpecInfo[WSSMAC_VEND_INFO_ELEM];
        UINT2                   u2SessId;
        UINT1                   au1Pad[2];

}tDot11DeauthMacFrame;
#ifdef WPS_WANTED
typedef struct {
      UINT1 elemId;
      UINT1 Len;
      UINT1 Oui[4];
      UINT1 Version[5];
      UINT1 ReqTypeTag[2];
      UINT1 ReqTypeLen[2];
      UINT1 ReqType;
      UINT1 extVal[10];
      UINT1 extLen;
      UINT1 au1Pad[1];
}tWpsCapability;
#endif
#ifdef WPA_WANTED
typedef struct {
         UINT1 elemId;
         UINT1 Len;
         UINT1 Oui[4];
         UINT2 u2WpaVersion;
         UINT1 au1MultiCastCipherSuite[4];
         UINT2 u2UniCastCipherCount;
         UINT1 au1UniCastCipherSuite[4];
         UINT2 u2ManagementKeyCount;
         UINT1 au1ManagementCastCipherSuite[4];
         UINT2 u2MacFrameRSN_Capability;
         UINT1 au1Pad[2];
}tWpaInfo;
#endif


/* Represents 802.11 MAC Association Request frame */
typedef struct {
        tWssMacVendInfo         aVendSpecInfo[WSSMAC_VEND_INFO_ELEM];
        tWssMacExtSuppRates     ExtSuppRates;
        tWssMacSuppChanls       SuppChanls;
        tWssMacRSNInfo          RSNInfo;
#ifdef WPA_WANTED
        tWpaInfo                WpaInfo;
#endif
        tWssMacSSID             Ssid;
        tWssMacWmmParam           WMMParam;
        tWssMacMgmtFrmHdr       MacMgmtFrmHdr;
        tWssMacSuppRate         SuppRate;
        tWssMacCapability       Capability;
        tWssMacListenInt        ListenInt;
        tWssMacPowCapability    PowCapability;
        tWssMacQosCap           QosCap;
        tHTCapabilities         HTCapabilities;
        tHTOperation            HTOperation;
        tVHTCapability          VHTCapabilities;
        tVHTOperation           VHTOperation;
        tOperModeNotify         OperModeNotify;
        tBssCoexistence         BssCoexistence;
        tExtCapabilities        ExtCapabilities;
#ifdef WPS_WANTED        
        tWpsCapability          WpsCapability;
#endif        
        UINT2                   u2SessId;
        UINT1                   au1Pad[2];

}tDot11AssocReqMacFrame;


/* Represents 802.11 MAC Association Response frame body */
typedef struct {
        tWssMacMgmtFrmHdr       MacMgmtFrmHdr; 
     tWssMacCapability     Capability; 
     tWssMacStatusCode     StatCode; 
     tWssMacAID          Aid; 
     tWssMacSuppRate      SuppRate;
     tWssMacWmmParam           WMMParam;
        tWssMacExtSuppRates     ExtSuppRates;
        tWssMacEDCAParam        EDCAParam;
        tHTCapabilities         HTCapabilities;
        tBssCoexistence         BssCoexistence;
        tExtCapabilities        ExtCapabilities;
#ifdef PMF_WANTED
        tTimeoutInterval        TimeoutInterval;
#endif
        tHTOperation            HTOperation;
        tVHTCapability      VHTCapabilities;
        tVHTOperation       VHTOperation;
        tOperModeNotify         OperModeNotify;
        tOBSSScanParams         OBSSScanParams;
        tWssMacVendInfo         aVendSpecInfo[WSSMAC_VEND_INFO_ELEM];
#ifdef WPS_WANTED        
        tWpsCapability          WpsCapability;
#endif        
        UINT2                   u2SessId;
        UINT1                   au1Pad[2];
}tDot11AssocRspMacFrame;

/* Represents 802.11 MAC Reassociation Request frame */
typedef struct {
        tWssMacVendInfo         aVendSpecInfo[WSSMAC_VEND_INFO_ELEM];
        tWssMacExtSuppRates     ExtSuppRates;
        tWssMacSuppChanls       SuppChanls;
        tWssMacRSNInfo          RSNInfo;
#ifdef WPA_WANTED        
        tWpaInfo                WpaInfo;
#endif 
        tWssMacSSID             Ssid;
        tWssMacMgmtFrmHdr       MacMgmtFrmHdr;
        tWssMacSuppRate         SuppRate;
        tWssMacWmmParam         WMMParam;
        tWssMacCurrAPAddr       CurrAPaddr;
        tWssMacCapability       Capability;
        tWssMacListenInt        ListenInt;
        tWssMacPowCapability    PowCapability;
        tWssMacQosCap           QosCap;
        tHTCapabilities         HTCapabilities;
        tBssCoexistence         BssCoexistence;
        tExtCapabilities        ExtCapabilities;
        tVHTCapability          VHTCapabilities;
        tVHTOperation           VHTOperation;
        tOperModeNotify         OperModeNotify;
        UINT2                   u2SessId;
        UINT1                   au1Pad[2];

}tDot11ReassocReqMacFrame;

/* Represents 802.11 MAC Reassociation Response frame body */
typedef struct {
        tWssMacMgmtFrmHdr       MacMgmtFrmHdr; 
     tWssMacCapability     Capability; 
     tWssMacStatusCode     StatCode; 
     tWssMacAID          Aid; 
     tWssMacSuppRate      SuppRate; 
     tWssMacWmmParam           WMMParam;
        tWssMacExtSuppRates     ExtSuppRates;
        tWssMacEDCAParam        EDCAParam;
        tHTCapabilities         HTCapabilities;
        tVHTCapability          VHTCapabilities;
        tVHTOperation           VHTOperation;
        tOperModeNotify         OperModeNotify;
        tBssCoexistence         BssCoexistence;
        tExtCapabilities        ExtCapabilities;
#ifdef PMF_WANTED
        tTimeoutInterval        TimeoutInterval;
#endif
        tHTOperation            HTOperation;
        tOBSSScanParams         OBSSScanParams;
        tWssMacVendInfo         aVendSpecInfo[WSSMAC_VEND_INFO_ELEM];
        UINT2                   u2SessId;
        UINT1                   au1Pad[2];

}tDot11ReassocRspMacFrame;

/* Represents 802.11 MAC Disassociation frame */
typedef struct {
        tWssMacMgmtFrmHdr       MacMgmtFrmHdr;
        tWssMacReasonCode       ReasonCode;
        tWssMacVendInfo         aVendSpecInfo[WSSMAC_VEND_INFO_ELEM];
        UINT2                   u2SessId;
        UINT1                   au1Pad[2];

}tDot11DisassocMacFrame;

/* Represents 802.11 MAC Action Frame */
typedef struct {
    tWssMacMgmtFrmHdr       MacMgmtFrmHdr;
    tWssMacTPCRequest     TPCRequest;
    tWssMacVendInfo         aVendSpecInfo[WSSMAC_VEND_INFO_ELEM];
    UINT1                   au1ActionME[WSSMAC_MAX_ACTION_ELEM];
    UINT1              u1CategoryCode;
    UINT1              u1ActionField;
    UINT1                   au1Pad[2];
}tDot11ActionReqFrame;

/* Represents 802.11 MAC Action frame body */
typedef struct {
    tWssMacMgmtFrmHdr       MacMgmtFrmHdr;
    tWssMacTPCReport     TPCReport;
    tWssMacVendInfo         aVendSpecInfo[WSSMAC_VEND_INFO_ELEM];
    UINT1                au1ActionME[WSSMAC_MAX_ACTION_ELEM];
    UINT1                 u1CategoryCode; 
    UINT1              u1ActionField;
    UINT1                   au1Pad[2];
}tDot11ActionRspFrame;

typedef struct {
    tWssMacMgmtFrmHdr       MacMgmtFrmHdr;
    tWssMacMeasRequest      MeasRequest;
    tWssMacVendInfo         aVendSpecInfo[WSSMAC_VEND_INFO_ELEM];
    UINT1                   au1ActionME[WSSMAC_MAX_ACTION_ELEM];
    UINT1                   u1CategoryCode;
    UINT1                   u1ActionField;
    UINT1                   au1Pad[2];
}tDot11ActionMeasReqFrame;

typedef struct {
     tWssMacMgmtFrmHdr       MacMgmtFrmHdr;
    tWssMacMeasReport        MeasReport;
     tWssMacVendInfo      aVendSpecInfo[WSSMAC_VEND_INFO_ELEM];
     UINT1           au1ActionME[WSSMAC_MAX_ACTION_ELEM];
     UINT1                   u1CategoryCode;
     UINT1                   u1ActionField;
    UINT1                   au1Pad[2];

}tDot11ActionMeasRspFrame;


#ifdef PMF_WANTED
/* Represents 802.11 SA Query Action frame body */

typedef struct {
    tWssMacMgmtFrmHdr       MacMgmtFrmHdr;
    UINT1                   u1CategoryCode; 
    UINT1                   u1ActionField;
    UINT2                   u2TransactionId;
    UINT2                   u2SessId;
    UINT1                   au1Pad[2];
}tDot11ActionStationQueryFrame;
#endif

typedef struct {
    UINT1 u1ElemId;
    UINT1 u1Len;
    BOOL1 isOptional;
    UINT1 u1Pad;
}tCountry;

/* Represents 802.11 MAC Action Frame for Channel Switch*/
typedef struct {
    tWssMacMgmtFrmHdr         MacMgmtFrmHdr;
    UINT1                     au1ActionME[WSSMAC_MAX_ACTION_ELEM];
    tWssMacVendInfo           aVendSpecInfo[WSSMAC_VEND_INFO_ELEM];
    tWssMacChanlSwitAnnounce  ChanlSwitAnnounce;
    tChanwidthSwitch          ChanwidthSwitch;
    tVhtTxPower               VhtTxPower;
    tCountry                  Country;
    UINT1                     u1CategoryCode;
    UINT1                     u1ActionField;
    UINT1                     au1Pad[2];
}tDot11ActionChanlSwitchFrame;

enum {
     WSS_MAC_FRAME_TYPE_MGMT = 0,
     WSS_MAC_FRAME_TYPE_CTRL,
     WSS_MAC_FRAME_TYPE_DATA
};

#ifdef PMF_WANTED
/* Timeout Interval Type */

typedef enum {
     WSS_MAC_TIMEOUT_RESERVED = 0,
     WSS_MAC_REASSOC_DEADLINE_INTERVAL,
     WSS_MAC_KEY_LIFETIME_INTERVAL,
     WSS_MAC_ASSOC_COMEBACK_TIME
}eTimeoutIntervalType;      
#endif

/* ********** Data Handler Structure Definitions ************** */

/* ****** Regular IEEE 802.11 Frame with 3 addr fields ***** */
typedef struct {

    UINT1           au1FrameCtrl[DOT11_HDR_FRAME_CTRL_LEN];
    UINT1           au1Duration[DOT11_HDR_DURN_LEN];
    UINT1           au1FrameAddr1[DOT11_HDR_MAC_ADDR_LEN];
    UINT1           au1FrameAddr2[DOT11_HDR_MAC_ADDR_LEN];
    UINT1           au1FrameAddr3[DOT11_HDR_MAC_ADDR_LEN];
    UINT1           au1FrameSeqCtrl[DOT11_HDR_SEQ_CTRL_LEN];

}tDot11RegFrameHdr;


/* ****** Regular IEEE 802.11 Frame with QoS Control ******* */
typedef struct {

    UINT1           au1FrameCtrl[DOT11_HDR_FRAME_CTRL_LEN];
    UINT1           au1Duration[DOT11_HDR_DURN_LEN];
    UINT1           au1FrameAddr1[DOT11_HDR_MAC_ADDR_LEN];
    UINT1           au1FrameAddr2[DOT11_HDR_MAC_ADDR_LEN];
    UINT1           au1FrameAddr3[DOT11_HDR_MAC_ADDR_LEN];
    UINT1           au1FrameSeqCtrl[DOT11_HDR_SEQ_CTRL_LEN];
    UINT1           au1FrameQosCtrl[DOT11_HDR_QOS_CTRL_LEN];
    UINT1           au1Pad[2];

}tDot11RegQosFrameHdr;


/* ****** Full IEEE 802.11 Frame with 4 addr fields ******** */
typedef struct {

    UINT1           au1FrameCtrl[DOT11_HDR_FRAME_CTRL_LEN];
    UINT1           au1Duration[DOT11_HDR_DURN_LEN];
    UINT1           au1FrameAddr1[DOT11_HDR_MAC_ADDR_LEN];
    UINT1           au1FrameAddr2[DOT11_HDR_MAC_ADDR_LEN];
    UINT1           au1FrameAddr3[DOT11_HDR_MAC_ADDR_LEN];
    UINT1           au1FrameSeqCtrl[DOT11_HDR_SEQ_CTRL_LEN];
    UINT1           au1FrameAddr4[DOT11_HDR_MAC_ADDR_LEN];
    UINT1           au1Pad[2];

}tDot11FullFrameHdr;


/* ****** Full IEEE 802.11 Frame with QoS Control ********** */
typedef struct {

    UINT1           au1FrameCtrl[DOT11_HDR_FRAME_CTRL_LEN];
    UINT1           au1Duration[DOT11_HDR_DURN_LEN];
    UINT1           au1FrameAddr1[DOT11_HDR_MAC_ADDR_LEN];
    UINT1           au1FrameAddr2[DOT11_HDR_MAC_ADDR_LEN];
    UINT1           au1FrameAddr3[DOT11_HDR_MAC_ADDR_LEN];
    UINT1           au1FrameSeqCtrl[DOT11_HDR_SEQ_CTRL_LEN];
    UINT1           au1FrameAddr4[DOT11_HDR_MAC_ADDR_LEN];
    UINT1           au1FrameQosCtrl[DOT11_HDR_QOS_CTRL_LEN];

}tDot11FullQosFrameHdr;


/* ************* IEEE 802.3 Ethernet Frame Header ********** */
typedef struct {

    UINT1           au1FrameDAddr[DOT3_HDR_ADDR_LEN];
    UINT1           au1FrameSAddr[DOT3_HDR_ADDR_LEN];
    UINT2           u2FrameEthType;
    UINT1           au1Pad[2];

}tDot3EthFrameHdr;


/* **************** 802.2 LLC Frame Header ***************** */
typedef struct {

    UINT1           u1LlcDsap;
    UINT1           u1LlcSsap;
    UINT1           au1Pad[2];

    union {

        struct {

            UINT1       u1Control;
            UINT1       u1OrgCode[DOT2_ORG_CODE_LEN];
            UINT2       u2EthType;
            UINT1       au1Pad[2];

        }SnapHdr;     

    }unLlcType;

}tDot2LlcFrameHdr;

/* ******************** Inter Module Communication ***************** */

enum {

        /* 802.11 MAC Frames */
        WSS_DOT11_MGMT_BEACON_MSG = 8,
        WSS_DOT11_MGMT_PROBE_REQ = 4,
        WSS_DOT11_MGMT_PROBE_RSP = 5,
        WSS_DOT11_MGMT_AUTH_MSG = 11,
        WSS_DOT11_MGMT_DEAUTH_MSG = 12,
        WSS_DOT11_MGMT_ASSOC_REQ = 0,
        WSS_DOT11_MGMT_ASSOC_RSP = 1,
        WSS_DOT11_MGMT_REASSOC_REQ = 2,
        WSS_DOT11_MGMT_REASSOC_RSP = 3,
        WSS_DOT11_MGMT_DISASSOC_MSG = 10,
        WSS_DOT11_MGMT_ACTION_MSG = 13,
        WSS_DOT11_CTRL_MSG,
        WSS_DOT11_DATA_MSG,

        /* CFA Msg to WSS MAC */
        WSS_MAC_CFA_MSG,

        /* CAPWAP Msg to WSS MAC */
        WSS_MAC_CAPWAP_MSG,

        /* APHDLR Msg to WSS MAC */
        WSS_MAC_APHDLR_MSG,
        WSS_DOT11_MGMT_TPC_REQ_MSG,
        WSS_DOT11_MGMT_MEAS_REQ_MSG=22,
#ifdef PMF_WANTED
        WSS_DOT11_MGMT_SAQUERY_ACTION_RSP,
        WSS_DOT11_MGMT_SAQUERY_ACTION_REQ,
#endif
        WSS_MAC_MAX_MSG
};
typedef struct {

        union {

            /* 802.11 Frame Structures */
            tDot11BeaconMacFrame        BeaconMacFrame;
            tDot11ProbReqMacFrame       ProbReqMacFrame;
            tDot11ProbRspMacFrame       ProbRspMacFrame;
            tDot11AuthMacFrame          AuthMacFrame;
            tDot11DeauthMacFrame        DeauthMacFrame;
            tDot11AssocReqMacFrame      AssocReqMacFrame;
            tDot11AssocRspMacFrame      AssocRspMacFrame;
            tDot11ReassocReqMacFrame    ReassocReqMacFrame;
            tDot11ReassocRspMacFrame    ReassocRspMacFrame;
            tDot11DisassocMacFrame      DisassocMacFrame;
            tDot11ActionRspFrame        ActionRspFrame;
         tDot11ActionReqFrame ActionReqFrame;
     tDot11ActionChanlSwitchFrame ActionChanlSwitchFrame; 
            tDot11ActionMeasRspFrame        ActionMeasRspFrame;
             tDot11ActionMeasReqFrame        ActionMeasReqFrame;
#ifdef PMF_WANTED
            tDot11ActionStationQueryFrame StationQueryActionFrame;
#endif
            /* WSS MAC Module Structures */
            tWssMacDot11PktBuf          MacDot11PktBuf;
            tWssMacDot11Frame           MacDot11Frame;

        }unMacMsg;
       UINT1 msgType;
       UINT1           au1Pad[3];

}tWssMacMsgStruct;              

/* WSS MAC Interface API Prototype */
UINT1
WssIfProcessWssMacMsg(UINT4, tWssMacMsgStruct *);

UINT1
WssIfConstructProbeRspStruct(tWssMacMsgStruct *, tWssMacMsgStruct *);

/* WSS MAC Data Handler API Prototypes */
INT4
WssMacGetDot11HdrSize(UINT1*);

UINT1*
WssMacPullPtr (UINT1**, INT4);

UINT1
WssMacDot11ToDot3 (UINT1**, UINT2 *);

UINT1
WssMacDot3ToDot11 (UINT1**, UINT1*, UINT2*);

#endif /* _WSSMAC_H_ */
