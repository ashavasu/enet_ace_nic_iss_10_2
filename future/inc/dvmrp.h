/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dvmrp.h,v 1.24 2012/03/01 10:06:14 siva Exp $
 *
 * Description:This file contains the exported definitions and  
 *                          macros of DVMRP                                 
 *
 *******************************************************************/
#ifndef _DVMRP_H
#define _DVMRP_H

#ifdef MBSM_WANTED
#include "mbsm.h"
#endif

/* DVMRP return Codes */
#define   DVMRP_OK    0

#ifdef FS_NPAPI

#define   DVMRP_ENABLED                           1
#define   DVMRP_DISABLED                          2

#endif

#define DVMRP_STATUS_ENABLED 1
#define DVMRP_STATUS_DISABLED 2
#define DVMRP_IS_DVMRP_ENABLED   DvmrpIsDvmrpEnabled ()

INT4 DvmrpIhmMainInit ARG_LIST ((void));
VOID DvmrpTaskMain ARG_LIST ((INT1 *));

INT4 DvmrpMutexLock (VOID);
INT4 DvmrpMutexUnLock (VOID);
INT4 DvmrpIsDvmrpEnabled (VOID);
VOID DvmrpProcessMcastDataPkt (tCRU_BUF_CHAIN_DESC * pBuf);
void DvmrpIhmHandleShutdown ARG_LIST ((void));
#define DVMRP_MUTEX_LOCK    DvmrpMutexLock 
#define DVMRP_MUTEX_UNLOCK  DvmrpMutexUnLock
#ifdef FS_NPAPI
typedef UINT2 tDvmrpVlanId;    
typedef struct _IpmcMcastVlanPbmpData {
    tMacAddr       MacAddr;
    tDvmrpVlanId   VlanId; 
}tIpmcMcastVlanPbmpData; 

VOID DvmrpVlanPortBmpChgNotify (tMacAddr MacAddr, tDvmrpVlanId VlanId);

#endif

#ifdef MBSM_WANTED
#define MBSM_DVMRP    "MbsmDvmrp"
VOID DvmrpMbsmUpdateMrouteTable (tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4Cmd);
INT4 DvmrpMbsmUpdateCardStatus ARG_LIST ((tMbsmProtoMsg * pProtoMsg, INT4 i4Event));
INT4 DvmrpMbsmPostMessage (tMbsmProtoMsg * pProtoMsg, INT4 i4Event);
#endif
#endif /* _DVMRP_H */
