/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: lr.h,v 1.51 2018/02/20 10:52:54 siva Exp $
 *
 * Description:This file contains the common definitions    
 *             for the entire system for the Linux Router   
 *             Environment.                                
 *
 *******************************************************************/
#ifndef _LR_H
#define _LR_H

/* FSAP2 includes */
/* -------------- */
#include "osxstd.h"
#include "srmbuf.h"
#include "osxsys.h"
#include "osxprot.h"
#include "srmmem.h"
#include "utlsll.h"
#include "utlslldyn.h"
#include "utldll.h"
#include "srmtmr.h"
#include "utlhash.h"
#include "utltrc.h"
#include "utleeh.h"
#include "utlmacro.h"
#include "utlhdr.h"
#include "redblack.h"

#include "pack.h"
#include "size.h"
#include "trace.h"
#include "utlbit.h"

#include "fsutlsz.h"

#ifdef MSTP_WANTED
#ifndef RSTP_WANTED
#error "MSTP cannot be enabled when RSTP is disabled"
#endif
#endif


#ifdef   __STDC__
#define  PROTO(x)     x
#else
#define  PROTO(x)     ()
#endif
#include "index.h"
#include "custfsap2.h" /*This file inclusion should not be moved to anywhere 
                         else in this file. 
                         It should be included at the end only.*/

/****************************************************************************/

/***************** Values used for Starting the System ***********************/
#define   SYS_MAX_TASKS      150
#define   SYS_MAX_QUEUES     150
#define   SYS_MAX_SEMS      3300
/* The value SYS_NUM_OF_TIME_UNITS_IN_A_SEC below indicates the system's timer 
 * granularity and hence the polling frequency from the device drivers.
 *  100  --> 10ms polling
 *   10  --> 100ms polling
 *   1   --> 1sec polling
 */
 extern UINT4            gu4SysTimeTicks;
#if defined(DX260) || defined (DX285) || defined(BCM53415_WANTED)
#define   SYS_TIME_TICKS_IN_A_SEC            SYS_NUM_OF_TIME_UNITS_IN_A_SEC
#define   SYS_NUM_OF_TIME_UNITS_IN_A_SEC     50
#else
#define   SYS_TIME_TICKS_IN_A_SEC            gu4SysTimeTicks
#define   SYS_NUM_OF_TIME_UNITS_IN_A_SEC    SYS_TIME_TICKS_IN_A_SEC 
#endif

#define   SYS_MAX_BUF_DESC                   800
#define   SYS_MAX_BUF_BLOCK_SIZE            2048
#define   SYS_MAX_NUM_OF_BUF_BLOCK          4000
#define   SYS_MAX_NUM_OF_TIMER_LISTS         100
#define   SYS_MAX_INTERFACES                 SYS_DEF_MAX_INTERFACES
#define   SYS_NUM_INTERFACES                 SYS_DEF_NUM_INTERFACES


/****************************************************************************/

/************** common definitions used in all modules **********************/
#define   SUCCESS     (0)
#define   FAILURE    (-1)
/****************************************************************************/

#define   UNUSED_PARAM(x)   ((void)x)

/************** Fix for klockwork's false positive warnings *****************
 *
 * False leaks reported by klocwork can be fixed by assigning the
 * resources to these dummy globals. Care must be taken to use
 * the macros at return point where warning is reported, after ensuring 
 * that the allocated pointer's ownership is passed to global scope.
 *
 * Reassignment of another warned resource to the same variable in the
 * same function, like 
 * KW_FALSEPOSITIVE_FIX(a); 
 * KW_FALSEPOSITIVE_FIX(b); 
 * will fix b, but not a. Scenarios like these require multiple
 * globals. Hence the following multiple definitions
 *
 ****************************************************************************/
#ifdef LRMAIN_C
VOID     *gpKwFp;
VOID     *gpKwFp1;
VOID     *gpKwFp2;
INT4     gi4KwFp;
#else
PUBLIC    VOID     *gpKwFp;
PUBLIC    VOID     *gpKwFp1;
PUBLIC    VOID     *gpKwFp2;
PUBLIC    INT4     gi4KwFp;
#endif


#define   KW_FALSEPOSITIVE_FIX(x)   gpKwFp = ((void*) x)
#define   KW_FALSEPOSITIVE_FIX1(x)  gpKwFp1 = ((void*) x)
#define   KW_FALSEPOSITIVE_FIX2(x)  gpKwFp2 = ((void*) x)
#define   KW_FALSEPOSITIVE_FIX3(x)  gi4KwFp = (x)
/****************************************************************************/

/* RowStatus values */
#define   ACTIVE             0x01
#define   NOT_IN_SERVICE     0x02
#define   NOT_READY          0x03
#define   CREATE_AND_GO      0x04
#define   CREATE_AND_WAIT    0x05
#define   DESTROY            0x06

#ifdef __64BIT__
#define PTR_TO_U4(x) ((UINT4)(FS_ULONG)(x))
#define PTR_TO_I4(x) ((INT4)(FS_ULONG)(x))
#else
#define PTR_TO_U4(x) ((UINT4)(x))
#define PTR_TO_I4(x) ((INT4)(x))
#endif

#ifndef CLI_WANTED
#define nmhSetCmn          MsrSetCmn
#define nmhSetCmnNew       MsrSetCmnNew
#define nmhSetCmnWithLock  MsrSetCmnWithLock
#endif

#ifndef PACK_REQUIRED
#pragma pack(1)
#endif
 typedef struct tMacAddress {
    UINT4  u4Dword;
    UINT2  u2Word;
} tMacAddress;
#ifndef PACK_REQUIRED
#pragma pack ()
#endif

#define MAC_ADDR_LEN 6
typedef UINT1 tMacAddr [MAC_ADDR_LEN];

int
LrMain (int argc, char *argv[]);

VOID lrInitComplete PROTO((UINT4 u4Status));

UINT1 LrGetRestoreDefConfigFlag PROTO ((VOID));

VOID LrSetRestoreDefConfigFlag PROTO ((UINT1 u1Status));

#ifdef MSR_WANTED
UINT4 LrSendEventToLrTask PROTO ((UINT4 u4Event));
#endif
#ifdef WTP_WANTED
#define OSIX_SEM_REQUIRED 0
#else
#define OSIX_SEM_REQUIRED 1
#endif
#define IS_OSIX_SEM_REQ(x)       (x | OSIX_SEM_REQUIRED) ? TRUE : FALSE 
#endif /* _LR_H */

