/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mpls.h,v 1.103 2018/01/03 11:31:19 siva Exp $
 *
 * Description:This file contains the exported definitions and
 *             prototypes of MPLS
 *
 *******************************************************************/


#ifndef _MPLS_H
#define _MPLS_H

#include "cfa.h"
#ifdef MBSM_WANTED
#include "mbsm.h"
#endif
typedef struct VplsInfo {
    UINT4  u4ContextId;         /* VSI Index (Virtual Switch Identifier)*/
    UINT4  u4IfIndex  ;         /* Ethernet Port IfIndex */
    UINT4  u4VplsIndex;         /* VPLS Identifier (VPLS contains set of
                                   pseudowires)*/
    tVlanIfaceVlanId VlanId;    /* Vlan Identifier (1-4094)*/
    UINT2  u2MplsServiceType;   /* MPLS Service type for this VPLS interface can
                                   be Port Based / Vlan Based / 
                                   (Port+Vlan) Based*/
    UINT1 u1PwVcMode;           /* Mode of the PW: VPLS or VPWS */
    UINT1 au1Pad[3];

} tVplsInfo;

typedef struct
{
    UINT4               u4Lbl;
    UINT4               Ttl;
    UINT4               Exp;
    UINT4               SI;
} tMplsHdr;

typedef enum
{
    MPLS_DIRECTION_ANY = 0,
    MPLS_DIRECTION_FORWARD = 1,
    MPLS_DIRECTION_REVERSE = 2
} eDirection;


/* L2VPN Debug Trace Macro Definitions */

/*
    Conventions followed are as follows :

    |<------------ 32 bits ------------>|

    +--------+--------+--------+--------+
    |xxx00000|00000000|00000xxx|00000xxx| (Present scenario)
    +--------+--------+--------+--------+

    |CPT     |        |<-Func->|<Levels>|
    |<Byte 4>|<Byte 3>|<Byte 2>|<Byte 1>|

    Byte 1 : Used for the debug levels
    Byte 2 : Used for the functional debugs
    Byte 3 : (Presently unused)
    Byte 4 : 3 bits used...
             C  = reserved for conditional debugging
             PT = Packet Tracing
*/


/* major debug levels */
#define L2VPN_DBG_LVL_CRT_FLAG    0x00000001 /* Level 1 */
#define L2VPN_DBG_LVL_ERR_FLAG    0x00000003 /* Includes Level 1 */
#define L2VPN_DBG_LVL_DBG_FLAG    0x00000007 /* Includes Level 1 & 2 */
#define L2VPN_DBG_LVL_ALL_FLAG    0x000000ff /* to be set externally */

/* major functional debugs */
#define L2VPN_DBG_FNC_SIG_FLAG    0x00000100
#define L2VPN_DBG_FNC_SSN_FLAG    0x00000200
#define L2VPN_DBG_FNC_VC_FLAG     0x00000400
#define L2VPN_DBG_FNC_CRT_FLAG    0x00000800 /* Used for Critical Functional Issue */
#define L2VPN_DBG_FNC_ALL_FLAG    0x0000ff00 /* to be set externally */

/* packet tracing */
#define L2VPN_DBG_IN_PKT_FLAG     0x40000000
#define L2VPN_DBG_OUT_PKT_FLAG    0x20000000
#define L2VPN_DBG_ALL_PKT_FLAG    0x60000000 /* to be set externally */

/* combinations */
#define L2VPN_DBG_ERR_SIG_FLAG    0x00000103
#define L2VPN_DBG_DBG_SIG_FLAG    0x00000107
#define L2VPN_DBG_ERR_SSN_FLAG    0x00000203
#define L2VPN_DBG_DBG_SSN_FLAG    0x00000207
#define L2VPN_DBG_ERR_VC_FLAG     0x00000403
#define L2VPN_DBG_DBG_VC_FLAG     0x00000407

#define L2VPN_DBG_NONE_FLAG              0x00000000
#define L2VPN_DBG_VCCV_CR_TRC            0x00010000 
#define L2VPN_DBG_VCCV_MGMT_TRC          0x00020000 
#define L2VPN_DBG_VCCV_CAPAB_EXG_TRC     0x00040000 
#define L2VPN_DBG_GRACEFUL_RESTART       0x00080000 
#define L2VPN_DBG_ALL_FLAG               0x6fffffff

#define L2VPN_MPLS_TYPE_TE               0x80
#define L2VPN_MPLS_TYPE_NONTE            0x40
#define L2VPN_MPLS_TYPE_VCONLY           0x20

#define  MPLS_HDR_LEN               4

#define  MPLS_DEF_CONTEXT_ID        0

#define  MPLS_TO_CFA                11
#define  MPLS_TO_L2                 12
#define  MPLS_TO_L3                 13
#define  RPTE_TO_CFA                14
#define  PPP_TO_CFA                 15
#ifdef HVPLS_WANTED
#define  MPLS_TO_MPLS               16
#endif

#define  CFA_TO_MPLS                21
#define  L2_TO_MPLS                 22
#define  L3_TO_MPLS                 23
#ifdef HVPLS_WANTED
#define MPLS_TO_MPLS      16
#define MPLS_DOUBLE_HEADER_LEN       8  
#endif
#define  RPTE_TO_MPLSRTR            24
#define  L2PKT_MPLS_TO_CFA          25
#define  L3_TO_MPLS_L3VPN           26
#define  MPLSL3VPN_TO_CFA   27
/* MPLS_IPv6 add start */
#ifdef MPLS_IPV6_WANTED
#define IPV6_L3_TO_MPLS          28
#define IPV6_MPLS_TO_L3          29
#endif
/* MPLS_IPv6 add end */

/* Rsvpte RMD Policy  */

#define RPTE_RMD_PATH_MSG                 0x0080
#define RPTE_RMD_RESV_MSG                 0x0040
#define RPTE_RMD_PATH_ERR_MSG             0x0020
#define RPTE_RMD_RESV_ERR_MSG             0x0010
#define RPTE_RMD_PATH_TEAR_MSG            0x0008
#define RPTE_RMD_RESV_TEAR_MSG            0x0004
#define RPTE_RMD_RECOVERY_PATH_MSG        0x0002
#define RPTE_RMD_ALL_MSGS                 0x00FE

/* Common Data base Owner */
#define MPLS_OWNER_UNKNOWN   1
#define MPLS_OWNER_OTHER     2
#define MPLS_OWNER_SNMP      3
#define MPLS_OWNER_LDP       4
#define MPLS_OWNER_CRLDP     5
#define MPLS_OWNER_RSVPTE    6
#define MPLS_OWNER_POLICYAGT 7

/* Pseudowire Owner - Protocol responsible for establishing the PW */
#define L2VPN_PWVC_OWNER_MANUAL         1
#define L2VPN_PWVC_OWNER_PWID_FEC_SIG   2
#define L2VPN_PWVC_OWNER_GEN_FEC_SIG    3
#define L2VPN_PWVC_OWNER_L2TP_PROTOCOL  4
#define L2VPN_PWVC_OWNER_OTHER          5

/* Label action */
#define MPLS_LABEL_ACTION_SWAP 1
#define MPLS_LABEL_ACTION_POP  2

/* Nexthop address type */
#define MPLS_LSR_ADDR_UNKNOWN       0
#define MPLS_LSR_ADDR_IPV4          1
#define MPLS_LSR_ADDR_IPV6          2

#define MPLS_MAX_LABELS_PER_ENTRY   7
#define LSP_TNLNAME_LEN             32
#define L2VPN_IP_ADDR_LENGTH        4
/* Function return values */
#define MPLS_SUCCESS               1
#define MPLS_FAILURE               0

#define TE_SUCCESS                 1
#define TE_FAILURE                 0

#define TE_EQUAL       1
#define TE_NOT_EQUAL      0
#define TE_REOPT_MANUAL_TRIGGER    1
/* Macro for VPV ID */
#define MPLS_OUI_VPN_ID            "Ari"

/* Pseudo Wire VC admin status */
#define L2VPN_PWVC_ADMIN_UP             1
#define L2VPN_PWVC_ADMIN_DOWN           2
#define L2VPN_PWVC_ADMIN_TESTING        3
#define L2VPN_PWVC_ADMIN_NIS            4

#define MPLS_FTN_TE_TABLE_DEF_OFFSET        17
#define MPLS_TE_RSRC_TABLE_MAX_RATE_OFFSET  14
#define MPLS_TE_XC_TABLE_OFFSET             28
#define MPLS_TE_TNL_XC_TABLE_DEF_OFFSET     14
#define MPLS_XC_INDEX_VALUE_LEN              5
#define LBL_STACK_TABLE_MAX_OFFSET          13

#define MPLS_INDEX_LENGTH                   4
#define MPLS_IPV4_ADDR_TYPE                 1
#define MPLS_IPV6_ADDR_TYPE                 2
#define MPLS_IPV4_ADDR_LEN                  4
#define MPLS_TRUE                           1
#define MPLS_FALSE                          0
#ifdef MPLS_IPV6_WANTED
/*MPLS_IPv6 add start*/
#define MPLS_IPV6_MAX_PREFIX       16
#define MPLS_IPV6_ADDR_LEN           16
/*MPLS_IPv6 add end*/
#endif

/* Below macros are used for SNMP based configuration */
#define MPLS_SNMP_TRUE    1
#define MPLS_SNMP_FALSE   2

#ifdef HVPLS_WANTED
#define MPLS_SPLIT_STATUS_ENABLE  1
#define MPLS_SPLIT_STATUS_DISABLE 0
#endif
       
/* Tunnel Role */
#define MPLS_TE_INGRESS                     1
#define MPLS_TE_INTERMEDIATE                2
#define MPLS_TE_EGRESS                      3
#define MPLS_TE_INGRESS_EGRESS              4

/* Tunnel Modes */
#define TE_TNL_MODE_UNIDIRECTIONAL           0
#define TE_TNL_MODE_COROUTED_BIDIRECTIONAL   1
#define TE_TNL_MODE_ASSOCIATED_BIDIRECTIONAL 2

/*Tunnel Model */
#define MPLS_TNL_UNIFORM_MODEL            1
#define MPLS_TNL_PIPE_MODEL               2
#define MPLS_TNL_SHORTPIPE_MODEL          3

/* Tunnel SRLG Values */
#define TE_TNL_SRLG_INCLUDE_ANY_AFFINITY  1
#define TE_TNL_SRLG_INCLUDE_ALL_AFFINITY  2
#define TE_TNL_SRLG_EXCLUDE_ALL_AFFINITY  3

/*MPLS tunnel ELSP Types*/
#define MPLS_DIFFSERV_PRECONF_ELSP         0
#define MPLS_DIFFSERV_SIG_ELSP             1
/*MPLS Diffserv Class Types*/
#define MPLS_DIFFSERV_DF_DSCP  0x00
#define MPLS_DIFFSERV_CS1_DSCP 0x08
#define MPLS_DIFFSERV_CS2_DSCP 0x10
#define MPLS_DIFFSERV_CS3_DSCP 0x18
#define MPLS_DIFFSERV_CS4_DSCP 0x20
#define MPLS_DIFFSERV_CS5_DSCP 0x28
#define MPLS_DIFFSERV_CS6_DSCP 0x30
#define MPLS_DIFFSERV_CS7_DSCP 0x38
#define MPLS_DIFFSERV_EF_DSCP  0x2e
#define MPLS_DIFFSERV_EF1_DSCP  0x3e
#define MPLS_DIFFSERV_EF2_DSCP MPLS_DIFFSERV_EF_DSCP
#define MPLS_DIFFSERV_AF11_DSCP 0x0a
#define MPLS_DIFFSERV_AF12_DSCP 0x0c
#define MPLS_DIFFSERV_AF13_DSCP 0x0e
#define MPLS_DIFFSERV_AF21_DSCP 0x12
#define MPLS_DIFFSERV_AF22_DSCP 0x14
#define MPLS_DIFFSERV_AF23_DSCP 0x16
#define MPLS_DIFFSERV_AF31_DSCP 0x1a
#define MPLS_DIFFSERV_AF32_DSCP 0x1c
#define MPLS_DIFFSERV_AF33_DSCP 0x1e
#define MPLS_DIFFSERV_AF41_DSCP 0x22
#define MPLS_DIFFSERV_AF42_DSCP 0x24
#define MPLS_DIFFSERV_AF43_DSCP 0x26
#define MPLS_DIFFSERV_AF1_PSC_DSCP MPLS_DIFFSERV_AF11_DSCP
#define MPLS_DIFFSERV_AF2_PSC_DSCP MPLS_DIFFSERV_AF21_DSCP
#define MPLS_DIFFSERV_AF3_PSC_DSCP MPLS_DIFFSERV_AF31_DSCP
#define MPLS_DIFFSERV_AF4_PSC_DSCP MPLS_DIFFSERV_AF41_DSCP

/*Tunnel E2E Protection Types*/
#define MPLS_TE_UNPROTECTED       0x80
#define MPLS_TE_FULL_REROUTE      0x40
#define MPLS_TE_DEDICATED_ONE2ONE 0x10

#define TE_TNL_WORKING_PATH          2
#define TE_TNL_PROTECTION_PATH       1
#define WORKING_PATH_INITIATED       3
#define BACKUP_PATH_INITIATED        4

/*Tunnel Protection Modes*/
#define MPLS_TE_REVERTIVE     1
#define MPLS_TE_NON_REVERTIVE 2

/*Make Before Break capability*/
#define MPLS_TE_MBB_ENABLED   1
#define MPLS_TE_MBB_DISABLED  2

/*MPLS Qos vales*/
#define MPLS_QOS_STD_IP       0
#define MPLS_QOS_RFC_1349     1
#define MPLS_QOS_DIFFSERV     2

/*MPLS Lsp Types*/
#define MPLS_TE_GEN_LSP        0
#define MPLS_TE_DIFF_SERV_ELSP 1
#define MPLS_TE_DIFF_SERV_LLSP 2
/*DiffServ Status*/
#define MPLS_DSTE_STATUS_ENABLE 1
#define MPLS_DSTE_STATUS_DISABLE 0

/*Tunnel Signalling Protocol*/
#define MPLS_TE_SIGPROTO_NONE                1
#define MPLS_TE_SIGPROTO_RSVP                2
#define MPLS_TE_SIGPROTO_LDP                 3
#define MPLS_TE_SIGPROTO_OTHER               4

#define TNL_SRLG_INCLUDE_ANY    1
#define TNL_SRLG_INCLUDE_ALL    2
#define TNL_SRLG_EXCLUDE_ANY    3

/* MPLS tunnel types */
#define MPLS_TE_TNL_TYPE_MPLS   0x80
#define MPLS_TE_TNL_TYPE_MPLSTP 0x40
#define MPLS_TE_TNL_TYPE_GMPLS  0x20
#define MPLS_TE_TNL_TYPE_HLSP   0x10
/* MPLS_P2MP_LSP_CHANGES - S */
#define MPLS_TE_TNL_TYPE_P2MP   0x08
/* MPLS_P2MP_LSP_CHANGES - E */
#define MPLS_TE_TNL_TYPE_SLSP   0x04

#define TE_AFFINITY_UNCONFIGURED   1

#define MPLS_OPER_UP                      1
#define MPLS_OPER_DOWN                    2
#define MPLS_OPER_TESTING                 3
#define MPLS_OPER_UNKNOWN                 4
#define MPLS_OPER_DORMANT                 5
#define MPLS_OPER_NOT_PRESENT             6
#define MPLS_OPER_LOWER_LAYER_DOWN        7

#define MPLS_ROUTER_ALERT_LABEL        1
#define MPLS_IPV4_EXPLICIT_NULL_LABEL  0
#define MPLS_IMPLICIT_NULL_LABEL       3
#define MPLS_INVALID_LABEL             0xffffffff

#define L2VPN_PWVC_ENET_DEF_PORT_VLAN       4097

#define MPLS_PWVC_TYPE_ETH_VLAN  0x0004
#define MPLS_PWVC_TYPE_ETH       0x0005

#define MPLS_AFFINITY_LENGTH               64
/* PwStatusIndication/VCCV Status */
#define L2VPN_PW_STATUS_INDICATION   0x80 /* Bit 0 : PwStatusIndication */
#define L2VPN_PW_VCCV_CAPABLE        0x40 /* Bit 1 : pwVCCV */

/* VCCV Control Channel Types  - Bits 3 to 7: Reserved */
#define L2VPN_VCCV_CC_NONE      0x00 /* None of the bits set */
#define L2VPN_VCCV_CC_ACH       0x80 /* Bit 0: Type 1: PW-ACH */
#define L2VPN_VCCV_CC_RAL       0x40 /* Bit 1: Type 2: MPLS Router Alert Label */
#define L2VPN_VCCV_CC_TTL_EXP   0x20 /* Bit 2: Type 3: MPLS PW Label with TTL ==1 */
#define L2VPN_VCCV_CC_ALL      L2VPN_VCCV_CC_ACH +\
                               L2VPN_VCCV_CC_RAL +\
                               L2VPN_VCCV_CC_TTL_EXP /* All of the CC bits set */
#define L2VPN_VCCV_RAL_TTL     L2VPN_VCCV_CC_RAL +\
                               L2VPN_VCCV_CC_TTL_EXP/*Router alert label and TTL CC bits set*/

/* VCCV Connectivity Verification Types */
#define L2VPN_VCCV_CV_NONE                 0x00 /* None of the bits set */
#define L2VPN_VCCV_CV_ICMPP                0x80 /* Bit 0: ICMP Ping */
#define L2VPN_VCCV_CV_LSPP                 0x40 /* Bit 1: LSP Ping */ 
#define L2VPN_VCCV_CV_BFD_IP_FAULT_ONLY    0x20 /* Bit 2: BFD IP/UDP-encap, PW Fault Detection only */
#define L2VPN_VCCV_CV_BFD_IP_FAULT_STATUS  0x10 /* Bit 3: BFD IP/UDP-encap, PW Fault Dectection & AC/PW Fault Status Signaling */
#define L2VPN_VCCV_CV_BFD_ACH_FAULT_ONLY   0x08 /* Bit 4: BFD PW-ACH-encap, PW Fault Detection only */
#define L2VPN_VCCV_CV_BFD_ACH_FAULT_STATUS 0x04 /* Bit 5: BFD PW-ACH-encap, PW Fault Detection & AC/PW Fault Status Signaling */

/* Pseudowire OAM Status */
#define L2VPN_PW_OAM_ENABLE  1 /* OAM enabled */
#define L2VPN_PW_OAM_DISABLE 2 /* OAM disabled */

#define MPLS_XC_LSP_ID_INDEX_START_OFFSET 13
#define MPLS_XC_INDEX_START_OFFSET MPLS_XC_LSP_ID_INDEX_START_OFFSET+1
#define MPLS_IN_INDEX_START_OFFSET MPLS_XC_LSP_ID_INDEX_START_OFFSET+6
#define MPLS_OUT_INDEX_START_OFFSET MPLS_XC_LSP_ID_INDEX_START_OFFSET+11

#define MPLS_OCTETSTRING_TO_OID(au4XCTableOid,pOctetString,u4XcTblIndex) { \
              au4XCTableOid[u4XcTblIndex] = pOctetString->pu1_OctetList [0];\
              au4XCTableOid[u4XcTblIndex+1] = pOctetString->pu1_OctetList [1];\
              au4XCTableOid[u4XcTblIndex+2] = pOctetString->pu1_OctetList [2];\
              au4XCTableOid[u4XcTblIndex+3] = pOctetString->pu1_OctetList [3];}\
              
#define MPLS_OID_TO_INTEGER(pOid,u4Index,u4Len) { \
        UINT1 au1XcIndex [4] = { 0 }; \
            au1XcIndex [0] = (UINT1)pOid->pu4_OidList[u4Len];\
            au1XcIndex [1] = (UINT1)pOid->pu4_OidList[u4Len+1];\
            au1XcIndex [2] = (UINT1)pOid->pu4_OidList[u4Len+2];\
            au1XcIndex [3] = (UINT1)pOid->pu4_OidList[u4Len+3];\
            MEMCPY((UINT1 *)&u4Index, au1XcIndex, MPLS_INDEX_LENGTH);\
            u4Index = OSIX_NTOHL (u4Index);}\

#define MPLS_INTEGER_TO_OID(au4Oid,u4Index,u4Len) { \
        UINT1 au1LsrIndex [4] = { 0 }; \
            u4Index = OSIX_HTONL (u4Index);\
            MEMCPY (au1LsrIndex, (UINT1 *)&u4Index, MPLS_INDEX_LENGTH);\
            au4Oid[u4Len] = au1LsrIndex[0];\
            au4Oid[u4Len+1] = au1LsrIndex[1];\
            au4Oid[u4Len+2] = au1LsrIndex[2];\
            au4Oid[u4Len+3] = au1LsrIndex[3];}\

#define MPLS_IF_COUNT(u4IfIndex, u4CurValue) { \
    UINT4 u4IndexMin = CFA_MIN_MPLS_IF_INDEX; \
    u4CurValue = (u4IfIndex % u4IndexMin) + \
    (u4IndexMin * ( (u4IfIndex / u4IndexMin) - 1)); }\

#define MPLS_TNL_IF_COUNT(u4IfIndex, u4CurValue) { \
    UINT4 u4IndexMin = CFA_MIN_MPLSTNL_IF_INDEX; \
    u4CurValue = (u4IfIndex % u4IndexMin) + \
    (u4IndexMin * ( (u4IfIndex / u4IndexMin) - 1)); }\

#define MPLS_MIN_TNL_IF_VALUE   CFA_MIN_MPLSTNL_IF_INDEX

#define MPLS_MAX_TNL_IF_VALUE   CFA_MAX_MPLSTNL_IF_INDEX

#define GET_SEG_FROM_TUNNEL_XC(au1SegmentIndex,pSegmentIndex,pIndexOidList,u4Offset) { \
    UINT4 u4Index ; \
        pSegmentIndex->i4_Length = pIndexOidList->pu4_OidList[u4Offset]; \
        if (pSegmentIndex->i4_Length != 0) { \
        for (u4Index = 0  ; u4Index <= ((UINT4)(pSegmentIndex->i4_Length-1)); u4Index++) { \
        u4Offset = u4Offset + 1; \
                au1SegmentIndex[u4Index] = (UINT1) pIndexOidList->pu4_OidList[u4Offset]; }\
                pSegmentIndex->pu1_OctetList = au1SegmentIndex ; }}

/* MPLS OAM specific macros */
#define OAM_MODULE_STATUS_ENABLED          1
#define OAM_MODULE_STATUS_DISABLED         2

#define OAM_MEG_MP_LOCATION_PER_NODE       1
#define OAM_MEG_MP_LOCATION_PER_INTERFACE  2

#define OAM_OPERATOR_TYPE_IP               1
#define OAM_OPERATOR_TYPE_ICC              2

#define OAM_SERVICE_TYPE_LSP               1
#define OAM_SERVICE_TYPE_PW                2
#define OAM_SERVICE_TYPE_SECTION           3
#define OAM_SERVICE_TYPE_PROTECT_LSP       4

#define OAM_ME_MP_TYPE_MEP                 1
#define OAM_ME_MP_TYPE_MIP                 2

#define OAM_MEP_DIRECTION_UP               1
#define OAM_MEP_DIRECTION_DOWN             2

#define  OAM_ME_OPER_STATUS_UP             0 
#define  OAM_ME_OPER_STATUS_MEG_DOWN       0x00000001
#define  OAM_ME_OPER_STATUS_ME_DOWN        0x00000002
#define  OAM_ME_OPER_STATUS_OAM_DOWN       0x00000004
#define  OAM_ME_OPER_STATUS_PATH_DOWN      0x00000008

/* Index Manager Macros - Start */
#define MAX_INDEXMGR_BYTE_BLOCK_SIZE            8 /* 1 byte = 8 bits */
#define MAX_INDEXMGR_ENT_PER_CHUNK                1536 /* UINT4 entries per chunk */
#define MAX_INDEXMGR_INDICES_PER_ENTRY           (MAX_INDEXMGR_BYTE_BLOCK_SIZE * 4) /* 8 * 4 = 32 */

/* Number of UINT4 entries in a chunk...4*256=1024 i.e. 1K bytes */
#define MAX_INDEXMGR_CHUNK_SIZE                  (MAX_INDEXMGR_ENT_PER_CHUNK * 4)

/* 1K bytes * 8 = 8192 indices per chunk */
#define MAX_INDEXMGR_INDICES_PER_CHUNK           (MAX_INDEXMGR_ENT_PER_CHUNK * \
                                              MAX_INDEXMGR_INDICES_PER_ENTRY)
/* Index Manager Macros - End */

#define MAX_LDP_L2VPN_IF_STR_LEN           256
#define L2VPN_PWVC_MAX_LEN                 256
#define L2VPN_MAX_AI_LEN                   256

#define TE_SNMP_TRUE                  1
#define TE_SNMP_FALSE                 2

/* TE ER HOP type definitions - snmp mib values */
#define TE_STRICT_ER                   1
#define TE_LOOSE_ER                    2

#define TE_INCLUDE_HOP                 1
#define TE_EXCLUDE_HOP                 2
#define TE_INCLUDE_ANY_HOP             3

/* GMPLS related Macros */

/* Signaling capability of MPLS module. */
#define GMPLS_SIG_CAP_UNKNOWN_BIT  0x80
#define GMPLS_SIG_CAP_RSVP_BIT     0x40
#define GMPLS_SIG_CAP_CRLDP_BIT    0x20
#define GMPLS_SIG_CAP_OTHER_BIT    0x10
#define GMPLS_SIG_CAP_ALL_BIT      0xf0

/* Label Set Feature */
#define GMPLS_LABEL_SET_ENABLED  1
#define GMPLS_LABEL_SET_DISABLED 2

#define GMPLS_UNNUM_TRUE         1
#define GMPLS_UNNUM_FALSE        2

#define MPLS_DIFFSERV_MAX_EXP 8
#define MPLS_TE_MAX_HOPS 16

#define RPTE_GR_RECOVERY_PATH_SREFRESH 0x80
#define RPTE_GR_RECOVERY_PATH_RX       0x40
#define RPTE_GR_RECOVERY_PATH_TX       0x20
#define RPTE_GR_RECOVERY_PATH_DEFAULT  0x00
#define RPTE_GR_RECOVERY_PATH_FULL     0xe0

/* Pseudowire Hardware Status. Applicable only for VPWS */
#define PW_HW_AC_PRESENT  0x10 /* fifth bit set indicates that AC 
                                programming in in H/W.*/
#define PW_HW_PW_PRESENT  0x20 /* sixth bit set indicates that VC 
                                programming in in H/W.*/
#define PW_HW_AC_UPDATE   0x01 /* first bit set indicates that AC
                                   to be added in H/W */
#define PW_HW_PW_UPDATE   0x02 /* second bit set indicates that VC
                                   to be added in H/W */

#define PW_HW_VPN_PRESENT 0x30 
#define PW_HW_VPN_UPDATE  0x03

#define   MPLS_IS_ADDR_CLASS_A(u4Addr)   ((u4Addr &  0x80000000) == 0)
#define   MPLS_IS_ADDR_CLASS_B(u4Addr)   ((u4Addr &  0xc0000000) == 0x80000000)
#define   MPLS_IS_ADDR_CLASS_C(u4Addr)   ((u4Addr &  0xe0000000) == 0xc0000000)
#define   MPLS_IS_ADDR_CLASS_D(u4Addr)   ((u4Addr &  0xf0000000) == 0xe0000000)
#define   MPLS_IS_ADDR_CLASS_E(u4Addr)   ((u4Addr &  0xf0000000) == 0xf0000000)
#define   MPLS_IS_ADDR_LOOPBACK(u4Addr)  ((u4Addr &  0x7f000000) == 0x7f000000)
#define   MPLS_IS_BROADCAST_ADDR(u4Addr) ((u4Addr &  0xffffffff) == 0xffffffff)

/* Currently MPLS Index Manager supports the 11 groups to handle indices for 
 * the following entities. 
 * Group 1.  PW               -> MAX_L2VPN_PW_VC_ENTRIES
 * Group 2.  InSegment        -> MAX_MPLSDB_LSR_INSEGMENT
 * Group 3.  OutSegment       -> MAX_MPLSDB_LSR_OUTSEGMENT
 * Group 4.  XC               -> MAX_MPLSDB_LSR_XCENTRY
 * Group 5.  LabelStackEntry  -> MAX_MPLSDB_LSR_LBLSTKENTRY
 * Group 6.  FTN              -> MAX_MPLSDB_FTN_ENTRY
 * Group 7.  LDP Entities     -> MAX_LDP_ENTITIES
 * Group 8.  LDP LSPs         -> MAX_LDP_LSPS
 * Group 9.  VPN              -> MAX_L2VPN_VPN_ENTRIES
 * Group 10. MEG              -> MAX_MPLS_OAM_MEG_ENTRIES
 * Group 11. ACID             -> MAX_L2VPN_ENET_ENTRIES
 */
#define MAX_INDEXMGR_GRPS_SPRTD                11

#define MAX_INDEXMGR_MAX_OF_CHUNKS_PER_GROUP    1

#ifdef MPLS_L3VPN_WANTED
/* MACROS for MPLS L3VPN */
#ifdef MPLS_IPV6_WANTED
#define L3VPN_BGP_MAX_ADDRESS_LEN         12
#else
#define L3VPN_BGP_MAX_ADDRESS_LEN         4
#endif

#define L3VPN_BGP4_RD                     1
#define L3VPN_BGP4_IMPORT_RT              2
#define L3VPN_BGP4_EXPORT_RT              3
#define L3VPN_BGP4_BOTH_RT                4

#define L3VPN_BGP4_ROUTE_PARAM_ADD        1
#define L3VPN_BGP4_ROUTE_PARAM_DEL        2

#define L3VPN_BGP4_ROUTE_ADD        1
#define L3VPN_BGP4_ROUTE_DEL        2

#define L3VPN_BGP4_LSP_STATUS_UP          1
#define L3VPN_BGP4_LSP_STATUS_DOWN        2
/*For RSVP-TE*/
#define L3VPN_BGP4_RSVPTE_LSP_STATUS_UP   1
#define L3VPN_BGP4_RSVPTE_LSP_STATUS_DOWN 2


#define L3VPN_BGP4_VRF_UP                 1
#define L3VPN_BGP4_VRF_DOWN               2
#define L3VPN_BGP4_VRF_CREATED    3
#define L3VPN_BGP4_VRF_DELETED     4

#define L3VPN_BGP4_LABEL_PUSH             1
#define L3VPN_BGP4_LABEL_POP              2

#define L3VPN_BGP4_ROUTE_ADD              1
#define L3VPN_BGP4_ROUTE_DEL              2

#define L3VPN_BGP4_ROUTE_PARAMS_REQ       1
#define L3VPN_BGP4_LSP_STATUS_REQ         2
#define L3VPN_BGP4_VRF_ADMIN_STATUS_REQ   4

#define L3VPN_BGP4_POLICY_PER_VRF         1
#define L3VPN_BGP4_POLICY_PER_ROUTE       2

#define L3VPN_VCM_IF_MAP                  1
#define L3VPN_VCM_IF_UNMAP                2


#define L3VPN_MPLS_INET_CIDR_TYPE_OTHER      1
#define L3VPN_MPLS_INET_CIDR_TYPE_REJECT     2
#define L3VPN_MPLS_INET_CIDR_TYPE_LOCAL      3 
#define L3VPN_MPLS_INET_CIDR_TYPE_REMOTE     4
#define L3VPN_MPLS_INET_CIDR_TYPE_BLACKHOLE  5        



#define L3VPN_MAX_RT_LEN                           8
#define L3VPN_MAX_RD_LEN                           8
#define L3VPN_MAX_RD_RT_STRING_LEN                 32
#define L3VPN_RD_SUBTYPE                           0
#define L3VPN_RT_SUBTYPE                           2


#define L3VPN_MPLS_ROUTE_PROTO_BGP           14
/* RSVP-TE Common lock to access Rsvp global TnlInfo array. */
#define L3VPN_BIND_LOCK()  L3vpnBindTaskLock ()
#define L3VPN_BIND_UNLOCK()  L3vpnBindTaskUnLock ()

PUBLIC INT4 L3vpnBindTaskLock          PROTO ((VOID));
PUBLIC INT4 L3vpnBindTaskUnLock        PROTO ((VOID));

#define L3VPN_LOCK  L3vpnMainTaskLock ()
#define L3VPN_UNLOCK  L3vpnMainTaskUnLock ()

#define MPLS_IS_MCASTADDR(pMacAddr) \
    ((FS_UTIL_IS_MCAST_MAC(pMacAddr) == OSIX_TRUE) ? MPLS_TRUE : MPLS_FALSE)

PUBLIC INT4 L3vpnMainTaskLock          PROTO ((VOID));
PUBLIC INT4 L3vpnMainTaskUnLock        PROTO ((VOID));

/*Structures for MPLS L3VPN */
typedef  struct  _MplsL3VpnRegInfo
{
    VOID (*pBgp4MplsL3VpnNotifyRouteParams) (UINT4 u4VrfId, UINT1 u1ParamType, UINT1 u1Action, UINT1 *pu1RouteParam);
    /* u4VrfId       : The VRF for which the RD/RT is being informed about */
    /* u1PamarmType  : The parameter type which is being passed to BGP (RD, Import RT, Export RT) */
    /* u1Action      : ADD/DELETE */
    /* pu1RouteParam : The value of the route-parameter (RD/RT) */

    VOID (*pBgp4MplsL3VpnNotifyLspStatusChange) (UINT4 u4LspLabel, UINT1 u1LspStatus);
    /* u4LspLabel  : Label of the LSP whose status has changed (BGP database is based on the label)*/
    /* u1LspStatus : Status of the LSP (UP/DOWN) */

    VOID (*pBgp4MplsL3vpnNotifyVrfAdminStatusChange) (UINT4 u4VrfId, UINT1 u1VrfStatus);
    /* u4VrfId    : The VRF id for the VRF whose status has changed */
    /* u1VrfStatus: The VRF status UP/DOWN/CREATED/DELETED */

    UINT2       u2InfoMask;
    /* Possible values:
     * MPLS_L3VPN_BGP4_ROUTE_PARAMS_REQ
     * MPLS_L3VPN_BGP4_LSP_STATUS_REQ
     * MPLS_L3VPN_BGP4_VRF_ADMIN_STATUS_REQ
     */

    UINT1       u1Padding[2];
} tMplsL3VpnRegInfo;

typedef  struct  _MplsL3VpnBgp4AddrPrefix
{
    UINT2   u2Afi;        /* Represents address family */
    UINT2   u2AddressLen; /* Represents the length of the prefix present
                           * (e.g. for IPV4 = 4, IPV6 = 16
                           *                            */
    UINT1   au1Address [L3VPN_BGP_MAX_ADDRESS_LEN];
                          /* To store Prefix address */
} tMplsL3VpnBgp4AddrPrefix;


typedef  struct  _MplsL3VpnBgp4RouteInfo
{
tMplsL3VpnBgp4AddrPrefix  IpPrefix;  /* For Advertising End: The Ip of the local CE router for which the route is advertised. */
                                     /* For Receving End: The Ip of the remote CE router for which advertisement is received. */

tMplsL3VpnBgp4AddrPrefix  NextHop;   /* For Adertising end: NHop means the Router in CE domain through which the Destination is reachable. */
                                     /* For the end who receives advertisement: Next hop will be the remote PE router which has sent the BGP adv. */

UINT4 u4Label;                       /* Label for the route to be programmed in H/w. */
UINT4 u4IfIndex;                     /* The information of the interface through which the route is reachable to MPLS. */
UINT4 u4VrfId;                       /* The VRF for which route is to be programmed. */
UINT4 u4NHAS;                        /* The Autonomous System Number of the next hop. */
UINT4 u4Metric;                      /* BGP uses only a single metric value for a route */
UINT2 u2PrefixLen;                   /* Length of the destination/prefix */
UINT1 u1ILMAction;                   /* Route Addition-1, Route Deletion-2 */
UINT1 u1LabelAction;                 /* 1-Label Push, 2-Label Pop*/
UINT1 u1LabelAllocPolicy;            /* 1-Per Label, 2-Per Route */
UINT1 u1Pad[3];
} tMplsL3VpnBgp4RouteInfo;

VOID
L3VpnIfConfMapUnmapEventHandler(UINT4 u4Event, INT4 i4IfIndex, UINT4 u4Context);

#define L3VPN_MAX_VRF_NAME_LEN   36

typedef struct _L3VpnBgpRoutePerRoute
{
    UINT4              u4ContextId;
    tMplsL3VpnBgp4AddrPrefix NextHop;
    UINT4              u4IfIndex;
    UINT1              au1MplsL3VpnVrfName[L3VPN_MAX_VRF_NAME_LEN];
    UINT1              au1DstMac [MAC_ADDR_LEN];
    UINT1              u1Pad[2];
}tL3VpnBgpRoutePerRoute;

typedef struct _L3VpnBgpRoutePerVrf
{
    UINT4                   u4VrfId;
    UINT1                   au1MplsL3VpnVrfName[L3VPN_MAX_VRF_NAME_LEN];
}tL3VpnBgpRoutePerVrf;


typedef union _L3VpnBgpRouteTerminationEntry
{
    tL3VpnBgpRoutePerVrf   L3VpnBgpRoutePerVrf;
    tL3VpnBgpRoutePerRoute L3VpnBgpRoutePerRoute;
}tuL3VpnBgpRouteTerminationEntry;

typedef struct _L3VpnBgpRouteLabelEntry
{
    tRBNodeEmbd     L3VpnBgpRouteLabelNode;
    UINT4           u4Label;
    tuL3VpnBgpRouteTerminationEntry L3VpnBgpRouteTerminationEntry;
    UINT4           u4RowStatus;
    UINT1           u1LabelPolicy;
    UINT1           u1ArpResolveStatus;
    UINT1           u1Pad[2];
}tL3VpnBgpRouteLabelEntry;

#define L3VPN_P_BGPROUTELABEL_LABEL(x) x->u4Label
#define L3VPN_P_BGPROUTELABEL_POLICY(x) x->u1LabelPolicy

#define L3VPN_BGPROUTELABEL_LABEL(x) x.u4Label
#define L3VPN_BGPROUTELABEL_POLICY(x)   x.u1LabelPolicy

#define L3VPN_P_BGPROUTELABELVRF_VRFID(x) x->L3VpnBgpRouteTerminationEntry.L3VpnBgpRoutePerVrf.u4VrfId
#define L3VPN_P_BGPROUTELABELVRF_VNAME(x) x->L3VpnBgpRouteTerminationEntry.L3VpnBgpRoutePerVrf.au1MplsL3VpnVrfName

#define L3VPN_BGPROUTELABELVRF_VRFID(x)  x.L3VpnBgpRouteTerminationEntry.L3VpnBgpRoutePerVrf.u4VrfId
#define L3VPN_BGPROUTELABELVRF_VNAME(x)  x.L3VpnBgpRouteTerminationEntry.L3VpnBgpRoutePerVrf.au1MplsL3VpnVrfName

#define L3VPN_BGPROUTEROW_STATUS(x)  x.u4RowStatus
#define L3VPN_P_ROUTEROW_STATUS(x)   x->u4RowStatus


tL3VpnBgpRouteLabelEntry *
L3vpnGetBgpRouteLabelTable(tL3VpnBgpRouteLabelEntry * pL3VpnBgpRouteLabelEntry);

tL3VpnBgpRouteLabelEntry *
L3vpnGetFirstBgpRouteLabelTable(VOID);

tL3VpnBgpRouteLabelEntry *
L3vpnGetNextBgpRouteLabelTable (tL3VpnBgpRouteLabelEntry * pCurrentL3VpnBgpRouteLabelEntry);

UINT4
L3vpnUtlEgressRteTableRowStatusHdl (UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
                INT4 i4SetValFsMplsL3VpnVrfEgressRteInetCidrStatus);

UINT4
L3vpnUtlTestEgressRteTableRowStatus (UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
                INT4 i4SetValFsMplsL3VpnVrfEgressRteInetCidrStatus,UINT4 *pu4Error);

#endif


#ifdef MPLS_L3VPN_WANTED /** vishal_1 : to remove this later on**/

#define L3VPN_MAX_VRF_NAME_LEN   36
#define L3VPN_MAX_VRF_DESCRIPTION_LEN 256
#define L3VPN_MAX_VRF_VPN_ID_LEN 7

enum
{
  MPLS_L3VPN_RT_IMPORT = 1,
  MPLS_L3VPN_RT_EXPORT,
  MPLS_L3VPN_RT_BOTH,
  MPLS_L3VPN_MAX_RT_TYPE
};

#define MPLS_MAX_L3VPN_RD_LEN        8
#define MPLS_MAX_L3VPN_RT_LEN        8

#define MPLS_L3VPN_RD_TYPE_MIN                     MPLS_L3VPN_RD_TYPE_0
#define MPLS_L3VPN_RD_TYPE_0                       0
#define MPLS_L3VPN_RD_TYPE_1                       1
#define MPLS_L3VPN_RD_TYPE_2                       2
#define MPLS_L3VPN_RD_TYPE_MAX                     MPLS_L3VPN_RD_TYPE_2

#define MPLS_L3VPN_RT_BOTH                         3
#define MPLS_L3VPN_RT_TYPE_MIN                     MPLS_L3VPN_RT_TYPE_0
#define MPLS_L3VPN_RT_TYPE_0                       0
#define MPLS_L3VPN_RT_TYPE_1                       1
#define MPLS_L3VPN_RT_TYPE_2                       2
#define MPLS_L3VPN_RT_TYPE_MAX                     MPLS_L3VPN_RT_TYPE_2

#define MPLS_L3VPN_RD_SUBTYPE                      0
#define MPLS_L3VPN_RT_SUBTYPE                      2

#endif



/* If any new group needs to be inserted into MPLS Index manager for new
 * feature enhancements,
 *
 * 1. Determine max chunks required for that group as mentioned above.
 * 2. Update MAX_INDEXMGR_MAX_OF_CHUNKS_PER_GROUP.
 * 3. Update MAX_INDEXMGR_INDEX_TBL_CHUNK in params.h
 * 4. Increment MAX_INDEXMGR_GRPS_SPRTD
 * 5. Add max entries required for that group in au4GrpMaxIndices[].
 *
 * This will determine complete memory required for index manager. */

/* Inet Address types for GMPLS Tunnels */
typedef  enum {
    GMPLS_UNKNOWN = 0,
    GMPLS_IPV4    = 1,
    GMPLS_IPV6    = 2,
    GMPLS_IPV4Z   = 3,
    GMPLS_IPV6Z   = 4,
    GMPLS_DNS     = 16
}eInetAddrType ;

typedef enum {
    GMPLS_TUNNEL_LSP_NOT_GMPLS        = 0,
    GMPLS_TUNNEL_LSP_PACKET           = 1,
    GMPLS_TUNNEL_LSP_ETHERNET         = 2, 
    GMPLS_TUNNEL_LSP_ANSI_ETSI_PDH    = 3,
    GMPLS_TUNNEL_LSP_ENCODING_VAL_4   = 4,   /* the value 4 is deprecated */
    GMPLS_TUNNEL_LSP_SDH_SONET        = 5,  
    GMPLS_TUNNEL_LSP_ENCODING_VAL_6   = 6,   /* the value 6 is deprecated */
    GMPLS_TUNNEL_LSP_DIGITAL_WRAPPER  = 7,
    GMPLS_TUNNEL_LSP_LAMBDA           = 8, 
    GMPLS_TUNNEL_LSP_FIBER            = 9,
    GMPLS_TUNNEL_LSP_ENCODING_VAL_10  = 10,  /* the value 10 is deprecated */
    GMPLS_TUNNEL_LSP_FIBER_CHANNEL    = 11,
    GMPLS_TUNNEL_DIGITAL_PATH         = 12,
    GMPLS_TUNNEL_OPTICAL_CHANNEL      = 13,
    GMPLS_TUNNEL_LINE                 = 14
}eGmplsEncType;

typedef enum {
    GMPLS_LSP_UNPROTECTED                   = 0x00, /* Unprotected */
    GMPLS_LSP_FULL_REROUTE                  = 0x01, /* (Full) Rerouting */
    GMPLS_LSP_REROUTE_WITHOUT_EXTRA_TRAFFIC = 0x02, /* Rerouting without Extra-Traffic */
    GMPLS_LSP_DEDICATED_ONE_TO_ONE          = 0x04, /* 1:1 Protection with Extra-Traffic */
    GMPLS_LSP_DEDICATED_UNI_ONE_PLUS_ONE    = 0x08, /* 1+1 Unidirectional Protection */
    GMPLS_LSP_DEDICATED_BI_ONE_PLUS_ONE     = 0x10  /* 1+1 Bidirectional Protection */
}eGmplsLSPProtectionType;

typedef enum {
    GMPLS_UNKNOWN_SWITCHING = 0,   /* none of the following, or not known */
    GMPLS_PSC1              = 1,   /* Packet-Switch-Capable 1 */
    GMPLS_PSC2              = 2,   /* Packet-Switch-Capable 2 */
    GMPLS_PSC3              = 3,   /* Packet-Switch-Capable 3 */
    GMPLS_PSC4              = 4,   /* Packet-Switch-Capable 4 */
    GMPLS_L2SC              = 51,  /* Layer-2-Switch-Capable */
    GMPLS_TDM               = 100, /* Time-Division-Multiplex */
    GMPLS_LSC               = 150, /* Lambda-Switch-Capable */                             
    GMPLS_FSC               = 200  /* Fiber-Switch-Capable */
}eGmplsSwitchingType;

typedef enum {
    GMPLS_FORWARD     = TE_TNL_MODE_UNIDIRECTIONAL,
    GMPLS_BIDIRECTION = TE_TNL_MODE_COROUTED_BIDIRECTIONAL,
    GMPLS_INVALID = 4
}eGmplsDirection;

typedef enum {
    GMPLS_SEGMENT_FORWARD = 1,
    GMPLS_SEGMENT_REVERSE = 2
}eGmplsSegmentDirection;

/* Path Computation types */
typedef enum {
    GMPLS_INVALID_PATH_COMP         = 0,
    GMPLS_PATH_COMP_DYNAMIC_FULL    = 1,
    GMPLS_PATH_COMP_EXPLICIT        = 2,
    GMPLS_PATH_COMP_DYNAMIC_PARTIAL = 3
}eGmplsTnlPathComp;

/* Admin status bits */
typedef enum {
    GMPLS_ADMIN_REFLECT             = 0x80000000,
    GMPLS_ADMIN_TESTING             = 0x00000004,
    GMPLS_ADMIN_ADMIN_DOWN          = 0x00000002,
    GMPLS_ADMIN_DELETE_IN_PROGRESS  = 0x00000001,
    GMPLS_ADMIN_UNKNOWN             = 0X00000000
}eGmplsAdminStatus;

typedef enum {                                                                                       
    GMPLS_GPID_UNKNOWN      = 0,
    GMPLS_GPID_ETHERNET     = 33,
    GMPLS_GPID_SDHSONET     = 34,
    GMPLS_GPID_FIBERCHANNEL = 43
}eGmplsTunnelGpid;

enum {
    LDP_GR_CAPABILITY_NONE = 1,
    LDP_GR_CAPABILITY_FULL,
    LDP_GR_CAPABILITY_HELPER
};
enum {
    RPTE_GR_CAPABILITY_NONE = 1,
    RPTE_GR_CAPABILITY_FULL,
    RPTE_GR_CAPABILITY_HELPER
};
/*********** MPLS-TP specific updations starts ***************/
#define MPLS_APPLICATION_NONE 0x00
#define MPLS_APPLICATION_ELPS 0x01
#define MPLS_APPLICATION_BFD 0x02
#define MPLS_APPLICATION_LSP_PING 0x04
#define MPLS_APPLICATION_Y1731 0x08
#define MPLS_APPLICATION_ERPS  0x10
#define MPLS_APPLICATION_STP 0x20
#ifdef HVPLS_WANTED
#define MPLS_APPLICATION_ECFM 0x40
#endif
#ifdef RFC6374_WANTED
#define MPLS_APPLICATION_RFC6374 0x80
#endif
/*********** MPLS-TP specific updations ends ***************/


/* Prototypes of MPLS exported to external modules */
/* ----------------------------------------------- */

/* Function invoked from LR to start the MPLS related tasks */

VOID MplsFmMain ARG_LIST((INT1 *pDummy));
VOID L2VpnTaskMain ARG_LIST((INT1 *pDummy));
VOID LdpTaskMain ARG_LIST((INT1 *pDummy));
VOID RpteRsvpTaskMain ARG_LIST((INT1 *pDummy));
PUBLIC VOID
L3vpnTaskSpawnL3vpnTask (INT1 *pi1Arg);


INT4 MplsStartModule (VOID);
INT4 MplsShutdownModule (VOID);
/* Function invoked by CFA-MUX to deliver the incoming packets */

/* If the ifType is Ethernet presently all the incoming ethernet packets 
 * are handed over to MPLS.
 * MPLS-FM decides, whether it requires MPLS processing or not */

UINT1 mplsProcessIncomingEthPkt
                     ARG_LIST ((tCRU_BUF_CHAIN_HEADER  *pBuf,
                                UINT4                  u4IfIndex));

/* If the ifType is ATM_PVC, only the atm packets of VC's registered by 
 * MPLS-FM are delivered by CFA-MUX */

UINT1 mplsProcessIncomingAtmPkt 
                     ARG_LIST ((tCRU_BUF_CHAIN_HEADER  *pBuf,
                                UINT4                  u4ConnHandle,
                                UINT1                  u1EncapsType));

/* Function used to process the outgoing IP packets for FEC classification and
 * MPLS processing */

INT1 mplsProcessOutgoingIpPkt 
                     ARG_LIST ((tCRU_BUF_CHAIN_HEADER    *pBuf,
                                UINT4                    u4IfIndex,
                                tMacAddr                 *pMacInfo));

INT4
MplsProcessL2Pkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                  UINT4 u4VfId, UINT1 u1PktType);
INT4
MplsProcessIpPkt ARG_LIST ((tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4IfIndex,
                            UINT4 u4DestIp, BOOL1 bCfaLockReqd));

/*MPLS_IPv6 add start*/
#ifdef MPLS_IPV6_WANTED
INT4
MplsProcessIpv6Pkt ARG_LIST ((tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4IfIndex,
                            tIp6Addr V6DestIp, BOOL1 bCfaLockReqd));
#endif
/*MPLS_IPv6 add end*/
INT4
MplsIsIpPktForL3Vpn(UINT4 u4Index, UINT4 *pu4ContextId);

INT4
MplsProcessL3VpnPacket(tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4IfIndex,
                        UINT4 u4DestIp, UINT4 u4ContextId);

INT4
MplsPktHandleFromCfa ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2IfIndex, 
                      UINT4 u4PktSize));
INT4
L2VpnGetPwIndexFromLabel ARG_LIST ((UINT4 u4Label,
                                    UINT4 *pu4PwIndex));
#ifdef HVPLS_WANTED
INT4
MplsProcessMplsPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
        UINT4 u4VfId, UINT1 u1PktType, UINT4 u4PwVcIndex);
INT4
L2VpnVlanIncFDBCounter ARG_LIST ((UINT4 u4PwIndex));

VOID
L2VpnVlanGetFDBLowThreshold ARG_LIST ((UINT4 u4PwIndex,UINT4 *pu4RetValFsMplsVplsFdbHighWatermark));

VOID
L2VpnVlanGetFDBHighThreshold ARG_LIST((UINT4 u4PwIndex,UINT4 *pu4RetValFsMplsVplsFdbLowWatermark));


INT4 L2VpnVlanDecFDBCounter ARG_LIST ((UINT4 u4PwIndex));
VOID
L2VpnVlanGetVpnIdFromPwIndex ARG_LIST ((UINT4 u4PwIndex,UINT4 *pu4VpnId));

INT4
L2VpnSendVplsFDBAlarmRaised ARG_LIST ((UINT4 u4VpnId));

 INT4
L2VpnSendVplsFDBAlarmCleared ARG_LIST ((UINT4 u4VpnId));

#endif

VOID MplsShutdown ARG_LIST((VOID));
INT4 L2vpnLock ARG_LIST((VOID));
INT4 L2vpnUnLock ARG_LIST((VOID));
INT4 MplsDsLock (VOID);
INT4 MplsDsUnlock (VOID);
INT4 MplsConfLock (VOID);
INT4 MplsConfUnlock (VOID);
VOID MplsHandleIfStatusChange 
                   ARG_LIST((UINT4 u4IfIndex, UINT1 u1OperStatus));
UINT4
MplsCheckIsAdminCreatedMplsTnlIf (UINT4 u4IfIndex);
UINT1
MplsTeTunnelIsRsrcRoleIngress (UINT4 u4TrfcParamIndex);
UINT1
MplsTeTunnelIsHopListRoleIngress (UINT4 u4MplsTunnelHopListIndex);
INT4 MplsCreateMplsIfOrMplsTnlIf (UINT4 u4L3IpIntf, UINT4 u4MplsIfOrMplsTnlIf,
                                  INT4 i4IfType);
INT4 MplsDeleteMplsIfOrMplsTnlIf (UINT4 u4L3IpIntf, UINT4 u4MplsIfOrMplsTnlIf,
                                  INT4 i4IfType, BOOL1 bFlag);
INT4 MplsStackMplsIfOrMplsTnlIf (UINT4 u4L3IpIntf, UINT4 u4MplsIfOrMplsTnlIf,
                                 INT4 i4IfType);
INT4
MplsCreateMplsTnlIf (UINT4 u4MplsTunnelId, UINT4 u4IfIndex);

VOID
MplsGetSegFromTunnelXC(tSNMP_OID_TYPE *XCIndex, tSNMP_OCTET_STRING_TYPE *XCSegmentIndex,
                        tSNMP_OCTET_STRING_TYPE *InSegmentIndex,tSNMP_OCTET_STRING_TYPE *OutSegmentIndex,
                        UINT4 *u4XCInd, UINT4 *u4InIndex, UINT4 *u4OutIndex);

/*STATIC_HLSP*/
INT4
MplsCreateTnlIntfOverHLSP (UINT4 u4HlspIntf, UINT4 u4IfIndex);
INT4
MplsDeleteTnlIntfOverHLSP (UINT4 u4HlspIntf, UINT4 u4IfIndex, UINT1 u1Flag);

PUBLIC VOID RpteGrShutDownProcess PROTO ((VOID));
PUBLIC INT4 RpteStartRsvpTask  PROTO ((VOID));

PUBLIC VOID RpteGrReStoreDynamicInfo PROTO ((VOID));

PUBLIC VOID LdpGrShutDownProcess PROTO ((VOID));
PUBLIC INT4 LdpStartLdpTask PROTO ((VOID));
PUBLIC VOID LdpNotifyCsrCompleted PROTO ((VOID));
PUBLIC VOID
RpteApiIfStatusChgNotify PROTO ((INT4 i4CtrlMplsIfIndex, INT4 i4DataTeIfIndex,
                                 UINT1 u1OperStatus));

#define MPLS_L2VPN_LOCK() L2vpnLock( )
#define MPLS_L2VPN_UNLOCK() L2vpnUnLock( )
#ifdef MBSM_WANTED
INT4 MplsNpMbsmInitProtocol PROTO ((tMbsmSlotInfo *));
INT4 MplsMbsmUpdateCardStatus PROTO ((tMbsmProtoMsg * pProtoMsg, UINT1 u1Cmd));
VOID MplsMbsmUpdateLCStatus PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1Cmd));
#endif

#define L2VPN_SUCCESS                     1
#define L2VPN_FAILURE                     0
#define L2VPN_MAX_VPLS_NAME_LEN           32
#define L2VPN_MAX_VPLS_VPNID_LEN          7

#ifdef VPLSADS_WANTED
#define L2VPN_MAX_VPLS_MODE_LEN                    32
#define L2VPN_MAX_VPLS_MTU_LEN                     8
#define L2VPN_MAX_VPLS_CONTROL_WORD_LEN            8
#define L2VPN_MAX_VPLS_RD_LEN                      8
#define L2VPN_MAX_VPLS_RT_LEN                      8

#define L2VPN_BGP_VPLS_MSG_TYPE(x)        (x)->u4MsgType
#define L2VPN_BGP_VPLS_VPLSINDEX(x)       (x)->u4VplsIndex
#define L2VPN_BGP_VPLS_LB(x)              (x)->u4LabelBase
#define L2VPN_BGP_VPLS_IP_ADDR(x)         (((x)->u4IpAddr).uIpAddr.Ip4Addr)
#define L2VPN_BGP_VPLS_IPV6_ADDR(x)       (((x)->u4IpAddr).uIpAddr.Ip6Addr.u1_addr)
#define L2VPN_BGP_VPLS_IP_ADDR_TYPE(x)    (((x)->u4IpAddr).u4AddrType)
#define L2VPN_BGP_VPLS_VE_ID(x)           (x)->u2VeId
#define L2VPN_BGP_VPLS_VBO(x)             (x)->u2VeBaseOffset
#define L2VPN_BGP_VPLS_VBS(x)             (x)->u2VeBaseSize
#define L2VPN_BGP_VPLS_MTU(x)             (x)->u2Mtu
#define L2VPN_BGP_VPLS_RD(x)              (x)->au1RouteDistinguisher
#define L2VPN_BGP_VPLS_RT(x)              (x)->au1RouteTarget
#define L2VPN_BGP_VPLS_VPLS_NAME(x)       (x)->au1VplsName
#define L2VPN_BGP_VPLS_CONTROL_FLAG(x)    (x)->bControlWordFlag
#ifdef VPLS_GR_WANTED
#define L2VPN_BGP_GR_TMR_INTERVAL(x)   (x)->u4TimerInterval
#endif
enum
{
    L2VPN_BGP_ADVERTISE_MSG = 1,
    L2VPN_BGP_WITHDRAW_MSG,
#if 0
    L2VPN_BGP_SESSION_UP,
    L2VPN_BGP_SESSION_DOWN,
#endif
    L2VPN_BGP_ADMIN_UP,
    L2VPN_BGP_ADMIN_DOWN,
#ifndef VPLS_GR_WANTED
    L2VPN_BGP_MODULE_DOWN
#endif
#ifdef VPLS_GR_WANTED
    L2VPN_BGP_MODULE_DOWN,
 L2VPN_BGP_GR_START,
 L2VPN_BGP_GR_IN_PROGRESS
#endif
};

#define  tIp4Addr                           UINT4

typedef struct _L2VpnIpAddr
{
    union
    {
        tIp4Addr Ip4Addr;
        tIp6Addr Ip6Addr;
    }uIpAddr;
    
    UINT4 u4AddrType; /*BGP4_INET_AFI_IPV4 of BGP4_INET_AFI_IPV6*/
}tL2VpnIpAddr;

typedef struct _L2VpnBgpEvtInfo
{
    UINT4 u4MsgType; /*Refer enums defined for BGP and L2VPN*/
    UINT4 u4VplsIndex;
    UINT4 u4LabelBase;
    tL2VpnIpAddr u4IpAddr; /*Peer Ip address */
#ifdef VPLS_GR_WANTED
    UINT4 u4TimerInterval;
#endif
 UINT2 u2VeId;
    UINT2 u2VeBaseOffset;
    UINT2 u2VeBaseSize;
    UINT2 u2Mtu;
    UINT1 au1RouteDistinguisher[L2VPN_MAX_VPLS_RD_LEN];
    UINT1 au1RouteTarget[L2VPN_MAX_VPLS_RT_LEN];
    UINT1 au1VplsName[L2VPN_MAX_VPLS_NAME_LEN];
    BOOL1 bControlWordFlag;
    UINT1 au1pad[3];
} tL2VpnBgpEvtInfo;

UINT4
L2VpnBgpEventHandler (tL2VpnBgpEvtInfo *pL2VpnBgpEvtInfo);

#endif 

 
#define MPLS_IS_MCASTADDR(pMacAddr) \
    ((FS_UTIL_IS_MCAST_MAC(pMacAddr) == OSIX_TRUE) ? MPLS_TRUE : MPLS_FALSE)
 typedef union _GenAddr
 {
       UINT1             au1Ipv4Addr[4];
       tIp6Addr          Ip6Addr;   /* Support for Ipv6 address */
 } uGenAddr;
 
 typedef struct _GenAddress
 {
      uGenAddr Addr;
      UINT2    u2AddrType;
      UINT1    u1Pad[2];
 }tGenAddr;
 
 typedef union _GenU4Addr
 {
        UINT4             u4Addr;
        tIp6Addr          Ip6Addr;   /* Support for Ipv6 address */
 } uGenU4Addr;
 
 typedef struct _GenU4Address
 {
        uGenU4Addr  Addr;
        UINT2    u2AddrType;
        UINT1    u1Pad[2];
 }tGenU4Addr;

UINT1
MplsIsFtnExist (tGenU4Addr * pPrefix, UINT4 u4Mask);
#endif 
