/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: elps.h,v 1.18 2015/10/05 12:19:05 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              by external modules.
 ********************************************************************/
#ifndef _ELPS_H
#define _ELPS_H

#include "ecfm.h"
#include "mplsapi.h"
#include "rstp.h"

/* System-Control status */
#define  ELPS_START       1
#define  ELPS_SHUTDOWN    2

/* Module status */
#define  ELPS_ENABLED     1
#define  ELPS_DISABLED    2

/* Protection Group Protection Type */
#define ELPS_PG_PROT_TYPE_ONE_ISTO_ONE_BIDIR_APS 1
#define ELPS_PG_PROT_TYPE_ONE_PLUS_ONE_BIDIR_APS 2
#define ELPS_PG_PROT_TYPE_ONE_PLUS_ONE_UNIDIR_APS 3
#define ELPS_PG_PROT_TYPE_ONE_PLUS_ONE_UNIDIR_NO_APS 4

/* Protection Group Oper Type */
#define ELPS_PG_OPER_TYPE_REVERTIVE    1
#define ELPS_PG_OPER_TYPE_NONREVERTIVE 2

/* Protection Group Service Type */
#define ELPS_PG_SERVICE_TYPE_VLAN     1
#define ELPS_PG_SERVICE_TYPE_VLAN     1
#define ELPS_PG_SERVICE_TYPE_LSP      2
#define ELPS_PG_SERVICE_TYPE_PW       3

/* Protection Group Monitor Type */
#define ELPS_PG_MONITOR_MECH_CFM                     1
#define ELPS_PG_MONITOR_MECH_MPLSOAM                 2
#define ELPS_PG_MONITOR_MECH_NONE                    3

/* Entity Type */
#define ELPS_ENTITY_WORKING       1
#define ELPS_ENTITY_PROTECT       2

/* Entity Direction */
#define CLI_ELPS_PG_DIR_FWD   1
#define CLI_ELPS_PG_DIR_REV   2

/* Address Type */
#define ELPS_ADDR_TYPE_UCAST MPLS_ADDR_TYPE_IPV4
#define ELPS_ADDR_TYPE_LOCAL_MAP_NUM MPLS_ADDR_TYPE_GLOBAL_NODE_ID

/* Protection Group Configuration Type */
#define ELPS_PG_TYPE_INDIVIDUAL       1
#define ELPS_PG_TYPE_LIST             2
#define ELPS_PG_TYPE_ALL              3

/* ELPS Module Semaphore */
#define  ELPS_LOCK()      ElpsApiLock ()
#define  ELPS_UNLOCK()    ElpsApiUnLock ()

#define ELPS_MAX_SRV_ID              VLAN_MAX_VLAN_ID_EXT
#define ELPS_SRV_LIST_SIZE           ((ELPS_MAX_SRV_ID + 31)/32 * 4)
#define ELPS_MAX_LIST                ELPS_MAX_SRV_ID

#define ELPS_PG_STATUS_INVALID       0
#define ELPS_PG_CREATE               1
#define ELPS_PG_DELETE               2
#define ELPS_PG_SWITCH_TO_WORKING    3
#define ELPS_PG_SWITCH_TO_PROTECTION 4
#define ELPS_PG_FAR_END_WTR          5
#define ELPS_PG_LO_WO_RECOVERY       6
#define ELPS_PG_LO_PRO_RECOVERY      7

#define ELPS_TRC_MAX_SIZE              255
#define ELPS_TRC_MIN_SIZE              1

/* Protection Architecture Types */
#define ELPS_PG_ARCH_1_PLUS_1        1
#define ELPS_PG_ARCH_1_TO_1          2

/* Protection Communication Channel */
#define ELPS_PG_COMM_CHANNEL_APS     1
#define ELPS_PG_COMM_CHANNEL_PSC     2
#define ELPS_PG_COMM_CHANNEL_NO_APS  3
#define ELPS_PG_COMM_CHANNEL_NO_PSC  4


/* Protection Switching Direction */
#define ELPS_PG_UNIDIRECTIONAL       1
#define ELPS_PG_BIDIRECTIONAL        2

#define ELPS_ECFM_APP_ID     ELPS_PG_MONITOR_MECH_CFM
#define ELPS_MPLS_APP_ID     ELPS_PG_MONITOR_MECH_MPLSOAM 

/* PG CFM table Rowstatus */
#define ELPS_CFM_TABLE_ROWSTATUS 7

/* Vlan group mgr for ELPS */
#define ELPS_VLAN_GROUP_MANAGER_MSTP 1
#define ELPS_VLAN_GROUP_MANAGER_ELPS 2
#define ELPS_PORT_STATE_BLOCKING      AST_PORT_STATE_DISCARDING
#define ELPS_PORT_STATE_UNBLOCKING    AST_PORT_STATE_FORWARDING
#define ELPS_MAX_MST_INSTANCES        AST_MAX_MST_INSTANCES

#define ELPS_DEFAULT_VLAN_GROUP_ID       0
#define ELPS_NP_MAX_IF_INDEX   SYS_DEF_MAX_PHYSICAL_INTERFACES

typedef struct ElpsHwPgProtType {
    UINT1 u1ArchType;     /* 1:1  or 1+1 Protection Architecture Type */
    UINT1 u1CommChannel;  /* APS or No APS Channel Channel Type */
    UINT1 u1Direction;    /* UniDirectional or Bi-Directional  
                             Switching Type */
    UINT1 au1Reserved[1];
} tElpsHwPgProtType;

typedef struct ElpsHwPgSwitchInfo {
    tElpsHwPgProtType   PgProtType;
    UINT4               u4PgIngressPortId;
    UINT4               u4PgWorkingPortId;
    UINT4               u4PgProtectionPortId;
    UINT4               u4PgWorkingReversePathPortId;
    UINT4               u4PgProtectionReversePathPortId;
    UINT4               u4PgWorkingServiceValue;
    UINT4               u4PgProtectionServiceValue;
    UINT4               u4PgWorkingInstanceId;
    UINT4               u4PgProtectionInstanceId;
    UINT4               u4PgWorkingReverseServiceValue;
    UINT4               u4PgProtectionReverseServiceValue;
 UINT4    u4WorkingPwIfIndex; 
 UINT4    u4ProtectionPwIfIndex;
 UINT4    u4WorkingPwVcIndex;
 UINT4    u4ProtectionPwVcIndex;
 UINT4    u4WorkingVpnId;
 UINT4    u4ProtectionVpnId;

    UINT4               u4PgWorkingPhyPort;
                        /* Physical port of working path if the service
                         * being protected is MPLS TP LSP and PW */  
    UINT4               u4PgProtectionPhyPort;
                        /* Physical port of protection path if the service
                         * being protected is MPLS TP LSP and PW */  
    UINT2          u2PgWorkingVlan;
                        /* Vlan ID of working path if the service
                         * being protected is MPLS TP LSP */  
    UINT2               u2PgProtectionVlan;
                        /* Vlan ID of protection path if the service
                         * being protected is MPLS TP LSP */  
    INT4                i4PgVlanGroupManager;
    UINT4               u4WorkingTnlHwId;
    UINT4               u4ProtectionTnlHwId;
    UINT1               u1PgIngressBrgPortType; 
    UINT1               u1PgAction; 
                            /* The possible value for this variable are  
                             * ELPS_PG_CREATE / ELPS_PG_DELETE / 
                             * ELPS_PG_SWITCH_TO_PROT_PATH / 
                             * ELPS_PG_SWITCH_TO_WORK_PATH */
    UINT1               au1WorkingNextHopMac[MAC_ADDR_LEN];
    UINT1               au1ProtectionNextHopMac[MAC_ADDR_LEN];
    UINT1               u1ServiceType;
      /* protection service type.
       * The possible values for this variable are
       * ELPS_PG_SERVICE_TYPE_VLAN / Vlan Protection /
       * ELPS_PG_SERVICE_TYPE_LSP / LSP Protection/
       * ELPS_PG_SERVICE_TYPE_PW /Pseudo-wire Protection */
    UINT1               u1WorkTnlHwStatus; /*Hw Status of Working entity Tunnel*/
    UINT1               u1ProtTnlHwStatus; /*HW Status of Protection entity Tunnel*/
    UINT1               u1PortVlanMembership; /* Flag to indicate whether the
                        working port is a membser of working service or not */
    UINT1               au1Reserved[2];
} tElpsHwPgSwitchInfo;

/* SEM State */
enum {
    ELPS_SEM_STATE_NR_W         =  0, 
    ELPS_SEM_STATE_NR_P         =  1,
    ELPS_SEM_STATE_LO           =  2,
    ELPS_SEM_STATE_FS           =  3,
    ELPS_SEM_STATE_SF_W         =  4,
    ELPS_SEM_STATE_SF_P         =  5,
    ELPS_SEM_STATE_MS           =  6,
    ELPS_SEM_STATE_MS_W         =  7,
    ELPS_SEM_STATE_WTR          =  8,
    ELPS_SEM_STATE_DNR          =  9,
    ELPS_SEM_STATE_EXER_W       =  10,
    ELPS_SEM_STATE_EXER_P       =  11,
    ELPS_SEM_STATE_RR_W         =  12,
    ELPS_SEM_STATE_RR_P         =  13,
    ELPS_SEM_STATE_INVALID      =  14
};



/*--------------------------------------------------------------------------*/
/*                       elpsmain.c                                         */
/*--------------------------------------------------------------------------*/
PUBLIC VOID ElpsMainTask PROTO ((INT1 *));
PUBLIC VOID ElpsMainApsTxTask PROTO ((INT1 *));

/*--------------------------------------------------------------------------*/
/*                       elpsapi.c                                          */
/*--------------------------------------------------------------------------*/
PUBLIC INT4 ElpsApiLock PROTO ((VOID));
PUBLIC INT4 ElpsApiUnLock PROTO ((VOID));
PUBLIC INT4 ElpsApiCreateContext PROTO ((UINT4));
PUBLIC INT4 ElpsApiDeleteContext PROTO ((UINT4));
PUBLIC VOID ElpsApiEcfmIndCallBack PROTO ((tEcfmEventNotification *));
PUBLIC INT4 ElpsApiNotificationRoutine PROTO ((UINT4, VOID *));
PUBLIC VOID ElpsApiModuleShutDown PROTO ((VOID));
PUBLIC INT4 ElpsApiModuleStart PROTO ((VOID));
PUBLIC INT1 ElpsApiIsElpsStartedInContext PROTO ((UINT4));
PUBLIC VOID ElpsApiGetPscChannelCode PROTO ((UINT4 *));


#ifdef MBSM_WANTED
/*--------------------------------------------------------------------------*/
/*                       elpsmbsm.c                                         */
/*--------------------------------------------------------------------------*/
PUBLIC INT4 ElpsMbsmPostMessage PROTO ((tMbsmProtoMsg *, INT4));

#endif /* MBSM_WANTED */

/*--------------------------------------------------------------------------*/
/*                       elpsport.c                                         */
/*--------------------------------------------------------------------------*/
/* HITLESS RESTART */
PUBLIC INT4
ElpsPortSendStdyStPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4PktSize,
        UINT4 u4IfIndex);

#endif /* _ELPS_H */

/*-----------------------------------------------------------------------*/
/*                       End of the file  elps.h                         */
/*-----------------------------------------------------------------------*/
