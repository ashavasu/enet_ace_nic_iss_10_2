/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: eoam.h,v 1.27 2014/08/22 11:10:33 siva Exp $
 * 
 * Description: This file contains exported definitions and functions
 *              of EOAM module.
 *********************************************************************/
#ifndef _EOAM_H
#define _EOAM_H

#include "rmgr.h"

#define EOAM_SLOW_PROT_TYPE                0x8809
#define EOAM_PROTOCOL_SUBTYPE              0x03

/* Variable Retrieval Branch Values */
#define EOAM_ATTRIBUTE                     0x07
#define EOAM_PACKAGE                       0x04
#define EOAM_OBJECT                        0x03

/* Length of the Organization Unique Identifier */
#define EOAM_OUI_LENGTH               3

/* Trace type  definitions */
#define  EOAM_FN_ENTRY_TRC    (0x1 << 16)
#define  EOAM_FN_EXIT_TRC     (0x1 << 17)
#define  EOAM_CRITICAL_TRC    (0x1 << 18)
#define  EOAM_DISCOVERY_TRC   (0x1 << 19)
#define  EOAM_LOOPBACK_TRC    (0x1 << 20)
#define  EOAM_LM_TRC          (0x1 << 21)
#define  EOAM_VAR_REQRESP_TRC (0x1 << 22)
#define  EOAM_RFI_TRC         (0x1 << 23)
#define  EOAM_MUX_PAR_TRC     (0x1 << 24)
#define  EOAM_RED_TRC         (0x1 << 25)
#define  EOAM_ALL_TRC         (EOAM_FN_ENTRY_TRC    |\
                               EOAM_FN_EXIT_TRC     |\
                               EOAM_CRITICAL_TRC    |\
                               EOAM_DISCOVERY_TRC   |\
                               EOAM_LOOPBACK_TRC    |\
                               EOAM_LM_TRC          |\
                               EOAM_VAR_REQRESP_TRC |\
                               EOAM_RFI_TRC         |\
                               EOAM_MUX_PAR_TRC     |\
                               EOAM_RED_TRC         |\
                               INIT_SHUT_TRC        |\
                               MGMT_TRC             |\
                               CONTROL_PLANE_TRC    |\
                               DUMP_TRC             |\
                               OS_RESOURCE_TRC      |\
                               ALL_FAILURE_TRC      |\
                               BUFFER_TRC)




#define EOAM_UNIDIRECTIONAL_SUPPORT   0x01
#define EOAM_LOOPBACK_SUPPORT         0x02
#define EOAM_EVENT_SUPPORT            0x04
#define EOAM_VARIABLE_SUPPORT         0x08

/* The below macros are used to encode the bits into Octet String as per the
 * RFC3417 Chapter 8.3 
 */
#define EOAM_SNMP_UNIDIRECTIONAL_SUPPORT   0x80
#define EOAM_SNMP_LOOPBACK_SUPPORT         0x40
#define EOAM_SNMP_EVENT_SUPPORT            0x20
#define EOAM_SNMP_VARIABLE_SUPPORT         0x10


#define EOAM_LINK_FAULT_BITMASK          0x0001
#define EOAM_DYING_GASP_BITMASK          0x0002
#define EOAM_CRITICAL_EVENT_BITMASK      0x0004
#define EOAM_LOCAL_EVAL_BITMASK          0x0008
#define EOAM_LOCAL_STABLE_BITMASK        0x0010
#define EOAM_LOCAL_EVAL_STABLE_BITMASK   0x0018
#define EOAM_REMOTE_EVAL_BITMASK         0x0020
#define EOAM_REMOTE_STABLE_BITMASK       0x0040
#define EOAM_REMOTE_EVAL_STABLE_BITMASK  0x0060

/* 
 * To convert value in millions multiply the given value by 1000000
 *
 */
#define EOAM_CLI_CONV_MILLIONS_TO_U8(pU8Val,pMillion)  \
FSAP_U8_ASSIGN_HI(pMillion,0); \
FSAP_U8_ASSIGN_LO(pMillion,1000000);\
FSAP_U8_MUL(pU8Val,pU8Val,pMillion)

/* 
 * To convert value to millions divide the value by 1000000
 *
 */
#define EOAM_CLI_CONV_U8_TO_MILLIONS(pU8Val,pMillion)  \
FSAP_U8_ASSIGN_HI(pMillion,0); \
FSAP_U8_ASSIGN_LO(pMillion,1000000);\
FSAP_U8_DIV(pU8Val,pMillion, pU8Val, pMillion)

/* Max FS_UINT8 Value string display length (18446744073709551615) */
#define EOAM_CLI_U8_STR_LEN            21

/* System control status */
typedef enum {
    EOAM_START    = 1,
    EOAM_SHUTDOWN = 2
}eEoamSysControl;

/* Module status */
typedef enum {
    EOAM_ENABLED  = 1,
    EOAM_DISABLED = 2
}eEoamEnable;

/* Truth value */
typedef enum {
    EOAM_TRUE = 1,
    EOAM_FALSE  = 2,
}eEoamTruthValue;

typedef enum {
    EOAM_LM_ENABLED  = 1,
    EOAM_LM_DISABLED = 2
 }eEoamLMEnable;

/* Loopback Status */
enum
{
    EOAM_NO_LOOPBACK=1,
    EOAM_INITIATING_LOOPBACK,
    EOAM_REMOTE_LOOPBACK,
    EOAM_TERMINATING_LOOPBACK,
    EOAM_LOCAL_LOOPBACK,
    EOAM_LB_STATUS_UNKNOWN
};

#define  EOAM_SEND_LOOPBACK_ENABLE         1
#define  EOAM_SEND_LOOPBACK_DISABLE        2

/* Draft Mib Admin State values - used only in LOW level routines */
typedef enum {
    EOAM_SNMP_ENABLED  = 1,
    EOAM_SNMP_DISABLED = 2
}eEoamAdminStatus;

typedef enum {
    EOAM_MODE_PASSIVE = 1,
    EOAM_MODE_ACTIVE  = 2,
    EOAM_MODE_PEER_UNKNOWN = 3,
    EOAM_MAX_MODE
}eEoamAdminMode;

/* Enum to denote MUX & PARSER action */
typedef enum
{
    EOAM_FWD = 1,
    EOAM_DISCARD,
    EOAM_LOOPBACK,
    EOAM_REMOTE_LB,
    EOAM_MAX_MUX_PARSE
} eEoamMuxParAction;

typedef enum {
    EOAM_FM_APPL_ID = 1,
    EOAM_CMIP_APPL_ID,
    EOAM_CUSTOM_APPL_ID_1,      /* Application ID reserved for custom applications */
    EOAM_MAX_APPL_ID        /* Max number of modules that can register with
                               EOAM. This should be always at the end */
} eEoamRegistrationId;

/* Error event types - These values will be by Fault
 * management and Link Monitoring module to report
 * events to EOAM. EOAM also uses these values to log
 * the events to EventLog table 
 * Reference: In mib refer the object dot3OamEventLogType  */
enum
{
    EOAM_ERRORED_SYMBOL_EVENT=1,
    EOAM_ERRORED_FRAME_PERIOD_EVENT,
    EOAM_ERRORED_FRAME_EVENT,
    EOAM_ERRORED_FRAME_SECONDS_EVENT,
    EOAM_ORG_SPEC_LINK_EVENT = 254,
    EOAM_LINK_FAULT = 256,
    EOAM_DYING_GASP = 257,
    EOAM_CRITICAL_EVENT = 258,
    EOAM_MAX_EVENT
};

/* Link Event TLVs
 * Reference: In IEEE 802.3ah-2004 Table 57.12 Link Event TLV type value */
enum
{
 EOAM_ERRORED_SYMBOL_EVENT_TLV=1,
 EOAM_ERRORED_FRAME_EVENT_TLV,
 EOAM_ERRORED_FRAME_PERIOD_EVENT_TLV,
 EOAM_ERRORED_FRAME_SECONDS_EVENT_TLV,
 EOAM_ORG_SPEC_LINK_EVENT_TLV = 254,
};

/* Error event log location */
enum
{
    EOAM_LOCAL_ENTRY=1,
    EOAM_REMOTE_ENTRY
};

typedef struct _EoamExtData
{
    UINT4 u4DataLen;
    UINT1 *pu1Data;
}tEoamExtData;

/* Loopback response status */
enum
{
    EOAM_IGNORE_LB_CMD=1,
    EOAM_PROCESS_LB_CMD
};

/* Event Actions */
typedef enum
{
    EOAM_ACTION_NONE = 1,
    EOAM_ACTION_WARNING,
    EOAM_ACTION_BLOCK
} eEmEventAction;

/* LM to OAM link monitoring error event notification */
typedef struct 
{
     FS_UINT8        u8EventWindow;       /* Window size defined for the event*/
     FS_UINT8        u8EventThreshold;    /* Threshold limit for the event*/
     FS_UINT8        u8EventValue;        /* value of the parameter within the 
                                             given window that generated this 
                                             event */
     FS_UINT8        u8ErrorRunningTotal; /* total no. of times this occurence 
                                             has happened since last reset */
     UINT4           u4EventRunningTotal; /* Total Events generated since 
                                             OAM reset */ 
     UINT4           u4EventLogTimestamp; /* System reference time */
     UINT1           u1EventType;         /* Error Event type */
     UINT1           au1Pad[3];

}tEoamThresEventInfo;                      

/* EOAM to CMIP interaction */
typedef struct _EoamVarReqData
{
    UINT4         u4ReqId;    /* unique for each request maintained by OAM */
    UINT2         u2Leaf;     /* CMIP leaf in the request */
    UINT1         u1Branch;   /* CMIP branch in the request */
    UINT1         u1Pad;
} tEoamVarReqData;  

/* OAM to FM callback information */
/* Data structure to report Error events */
typedef struct 
{
    UINT4 u4Port;                        /* Interface number */
    UINT4 u4EventType;                   /* Type of Event*/ 
    union
    {
        UINT1               u1State;     /* State denoting 'ENABLE'
                                            or 'DISABLE'. Used for Dying gasp,
                                            Link fault, critical events*/ 
        tEoamThresEventInfo LMEvents;    /* Contains structure of threshold 
                                            crossing events' information*/ 
        tEoamExtData        EoamExtData; /* contains a data pointer used for 
                                            Org Specific data and variable
                                            request */
        tEoamVarReqData     EoamVarReq;  /* contains a data pointer for 
                                            variable request */
        tCRU_BUF_CHAIN_HEADER *pBuf;     /* Contains pointer to loopback test
                                            data */
    }uEventInfo;
#define EventState     uEventInfo.u1State
#define ThresEventInfo uEventInfo.LMEvents    
#define ExtData        uEventInfo.EoamExtData
#define VarRequest     uEventInfo.EoamVarReq
#define LBTestData     uEventInfo.pBuf
    UINT2 u2Location;                    /* EOAM_LOCAL_ENTRY or 
                                            EOAM_REMOTE_ENTRY */
    UINT1 au1Pad[2];
}tEoamCallbackInfo;                      

/* CMIP to OAM interaction */
typedef struct _EoamVarReqResp
{
    tEoamExtData  RespData;
    UINT4         u4ReqId;    /* unique for each request maintained by OAM */
    UINT2         u2Leaf;     /* CMIP leaf in the request */
    UINT1         u1Branch;   /* CMIP branch in the request */
    UINT1         u1Pad;
} tEoamVarReqResp;  

typedef struct
{
    INT4 (*pNotifyCbFunc) (tEoamCallbackInfo *pInfo);
                     /* EOAM delivers the information to the registered module 
                      * thru this FnPtr. */
    UINT4 u4EntId;   /* Entity Id (i.e., Application id) to be registered 
                        with EOAM */
    UINT4 u4Bitmap;  /* Bitmap to denote which are events registered with */
/* Following are the event type/bitmap that the external module can register
 * with Ethernet OAM to receive notifications */
#define    EOAM_NOTIFY_ERRORED_SYMBOL         0x00000001
#define    EOAM_NOTIFY_ERRORED_FRAME          0x00000002
#define    EOAM_NOTIFY_ERRORED_FRAME_PERIOD   0x00000004
#define    EOAM_NOTIFY_ERRORED_FRAME_SECONDS  0x00000008
#define    EOAM_NOTIFY_ORG_SPECIFIC_EVENT     0x00000010
#define    EOAM_NOTIFY_LINK_FAULT             0x00000020
#define    EOAM_NOTIFY_DYING_GASP             0x00000040
#define    EOAM_NOTIFY_CRITICAL_EVENT         0x00000080
#define    EOAM_NOTIFY_ORG_INFO_RCVD          0x00000100 
#define    EOAM_NOTIFY_VAR_REQ_RCVD           0x00000200
#define    EOAM_NOTIFY_VAR_RESP_RCVD          0x00000400
#define    EOAM_NOTIFY_LB_ACK_RCVD            0x00000800 
#define    EOAM_NOTIFY_LB_ACK_NOT_RCVD        0x00001000 
#define    EOAM_NOTIFY_LB_TESTDATA_RCVD       0x00002000
#define    EOAM_NOTIFY_PORT_CREATE            0x00004000
#define    EOAM_NOTIFY_PORT_DELETE            0x00008000
} tEoamRegParams;

/* EOAM Main task entry point function */
VOID   EoamMainTask PROTO ((INT1 *));

/* Link monitoring task entry point function */
VOID   EoamLmMainTask PROTO ((INT1 *));

/* API to send error events (LINK_FAULT, DYING GASP & CRITICAL EVENT) 
 * to peer */
PUBLIC INT4 EoamApiSendEventsToPeer PROTO ((UINT4 u4Port, 
                                  UINT2 u2RequestType, UINT1 u1Status));

/* API to send Loopback command (enable/disable) to peer */
PUBLIC INT4 EoamApiSendLBCommandToPeer PROTO ((UINT4 u4Port, 
                                  UINT1 u1RequestType));

/* API to send variable request to peer */
PUBLIC INT4 EoamApiSendVarReqToPeer PROTO ((UINT4 u4Port, 
                                  tEoamExtData * pVarReq));

/* API to send Organization specific PDU to peer */
PUBLIC INT4 EoamApiSendOrgSpecPduToPeer PROTO ((UINT4 u4Port, 
                                  tEoamExtData *pOrgSpecData));

/* API to send Organization specific Event TLV to peer */
PUBLIC INT4 EoamApiSendOrgSpecEventToPeer PROTO ((UINT4 u4Port,
                                   tEoamExtData * pOrgSpecData));
/* API to send Link monitoring threshold error events to peer */
PUBLIC INT4 EoamApiSendThresholdEvtToPeer PROTO ((UINT4 u4Port, 
                                  tEoamThresEventInfo * pEventInfo));

/* API to send variable response to peer */
PUBLIC INT4 EoamApiSendVarResponseToPeer PROTO ((UINT4 u4Port, 
                                  tEoamVarReqResp *pRespData));

/* API called by CFA to enq received PDU to EOAM task */
PUBLIC INT4 EoamApiEnqIncomingPdu PROTO ((tCRU_BUF_CHAIN_HEADER *, 
                                  UINT4 u4PortIndex));

/* API called by CFA to notify interface creation */
PUBLIC INT4 EoamApiNotifyIfCreate PROTO ((UINT4 u4IfIndex));

/* API called by CFA to notify interface deletion */
PUBLIC INT4 EoamApiNotifyIfDelete PROTO ((UINT4 u4IfIndex));

/* API called by CFA to notify interface oper sts chg */
PUBLIC INT4 EoamApiNotifyIfOperChg PROTO ((UINT4 u4IfIndex, 
                                           UINT1 u1OperStatus));

PUBLIC INT4 EoamApiIfShutDownNotify PROTO ((UINT4 u4IfIndex,
                                           UINT1 u1OperStatus));

/* API to check if the received PDU is OAMPDU */
PUBLIC INT4 EoamApiIsOAMPDU PROTO ((tCRU_BUF_CHAIN_HEADER *));

/* API to register with EOAM module */
PUBLIC INT4 EoamApiRegisterModules PROTO ((tEoamRegParams *pEoamRegParams));

/* API to de-register from EOAM module */
PUBLIC INT4 EoamApiDeRegisterModule PROTO ((UINT4 u4EntId));

/* API to check if EOAM is enabled on port */
PUBLIC INT4 EoamApiIsEnabledOnPort PROTO ((UINT4 u4IfIndex));

/* API called by CFA during pkt tx, to get the PARSER action
 * (FWD/DISCARD/LB/REMOTE_LB) based on the EOAM parameters of the interface */
PUBLIC INT4 EoamMuxParActOnRx PROTO ((UINT4 u4IfIndex, 
                                     tCRU_BUF_CHAIN_HEADER *, 
                                     UINT4 u4PktSize));

/* API called by CFA during pkt rx, to get the MUX action (FWD/DISCRAD) 
 * based on EOAM parameters of the interface. */
PUBLIC INT4 EoamMuxParActOnTx PROTO ((UINT4 u4IfIndex, 
                                      tCRU_BUF_CHAIN_HEADER *));

/* API to get Mac address of the OAM Peer */
PUBLIC VOID EoamApiGetPeerMac PROTO ((UINT4 u4IfIndex, UINT1 *pMacAddr));

PUBLIC INT4 EoamLock       PROTO ((VOID));
PUBLIC INT4 EoamUnLock     PROTO ((VOID));

PUBLIC INT4 EoamSyncTakeSem PROTO ((VOID));
PUBLIC INT4 EoamSyncGiveSem PROTO ((VOID));

#ifdef MBSM_WANTED
/* API called from MBSM to post the Line Card updation status */
PUBLIC INT4 EoamMbsmPostMessage PROTO ((tMbsmProtoMsg *, INT4));
#endif/* MBSM_WANTED */

#ifdef L2RED_WANTED
PUBLIC INT4 EoamRedHandleUpdateEvent PROTO ((UINT1 , tRmMsg *, UINT2));
#endif /* L2RED_WANTED */
PUBLIC VOID EoamModuleShutDown PROTO ((VOID));
PUBLIC INT4 EoamRestartModule PROTO ((VOID));
PUBLIC INT4 EoamModuleStart PROTO ((VOID));

PUBLIC INT4 EoamSetIfInfo PROTO ((INT4 i4CfaIfParam, 
                                      UINT4 u4IfIndex, 
                                      UINT4 u4Value));
PUBLIC INT4 EoamGetActionFromFm (UINT4 ,UINT4);

#endif /* _EOAM_H */
/*-----------------------------------------------------------------------*/
/*                       End of the file  eoam.h                         */
/*-----------------------------------------------------------------------*/
