/* $Id: pimsys.h,v 1.1 2015/02/13 11:16:24 siva Exp $   */

#ifndef PIMSYS_H
#define PIMSYS_H

enum {
    SYS_LOG_PIM_MEMALLOC_FAIL = 1,
    SYS_LOG_PIM_MEMREL_FAIL,
    SYS_LOG_PIM_RBTREE_ADD_FAIL,
    SYS_LOG_PIM_RBTREE_DEL_FAIL,
    SYS_LOG_PIM_BUF_ALLOC_FAIL,
    SYS_LOG_PIM_BUF_REL_FAIL,
    SYS_LOG_PIM_EVNT_SEND_FAIL,
    SYS_LOG_PIM_BLK_UPDATE_FAIL,
    SYS_LOG_PIM_QUE_SEND_FAIL
};

#ifdef   __SPIM_INPUT_C__
CONST CHR1 *PimSysErrString [] = {
    NULL,
    "Pim Mem Alloc Failed\r\n",
    "Pim Mem Release Failed\r\n",
    "Pim RBTree Addition Failed\r\n",
    "Pim RBTree Deletion Failed\n",
    "Pim Buffer Allocation Failed\r\n",
    "Pim Buffer Release Failed\r\n",
    "Pim Event Send Failed\r\n",
    "Pim Bulk Update Failed\r\n",
    "Pim Queue Send Failed\r\n",
    "\r\n"
};
#else
extern CONST CHR1 *PimSysErrString [];
#endif

#endif   /* PIMSYS_H */
