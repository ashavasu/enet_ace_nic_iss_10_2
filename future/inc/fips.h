/********************************************************************  
*  Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
*  $Id: fips.h,v 1.15 2017/09/12 13:27:05 siva Exp $
*
*  Description: This file contains the exported definitions,
*               macros and prototypes of FIPS module
*             
********************************************************************/
#ifndef __FIPS_H_
#define __FIPS_H_

/********************************************************************
 *                  Macros and Constants
 ********************************************************************/

#define FIPS_FAILURE           OSIX_FAILURE
#define FIPS_SUCCESS           OSIX_SUCCESS

#define FIPS_INIT_COMPLETE(u4Status) lrInitComplete(u4Status)

typedef enum
{
    FIPS_MODE = 1,
    LEGACY_MODE,
    CNSA_MODE
}eFipsOperMode;

typedef enum
{
    BYPASS_ENABLED = 1,
    BYPASS_DISABLED
}eFipsBypassStatus;

enum
{
    FIPS_CUST_DEL_SRAM_CONTENTS,
    FIPS_CUST_MAX_CALL_BACK_EVENTS
};

#define FIPS_KNOWN_ANS_TEST_NONE   0x00000000
#define FIPS_KNOWN_ANS_TEST_SHA1   0x00000001
#define FIPS_KNOWN_ANS_TEST_SHA2   0x00000002
#define FIPS_KNOWN_ANS_TEST_HMAC   0x00000004
#define FIPS_KNOWN_ANS_TEST_AES    0x00000008
#define FIPS_KNOWN_ANS_TEST_DES    0x00000010
#define FIPS_KNOWN_ANS_TEST_RAND   0x00000020
#define FIPS_KNOWN_ANS_TEST_RSA    0x00000040
#define FIPS_KNOWN_ANS_TEST_DSA    0x00000080

#define FIPS_KNOWN_ANS_TEST_ALL    0x000000FF
#define FIPS_ROOT_USER             "admin"

/* Required for FIPS KAT */
enum
{
    FIPS_KAT_DISABLE_ALL_FLAGS           = 0x0,
    FIPS_KAT_POWER_ON_STATUS             = 0x1,
    FIPS_KAT_CONDITIONAL_SELF_TEST       = 0x2,
    FIPS_KAT_UNSET_POWER_ON_STATUS       = 0x4,
    FIPS_KAT_UNSET_CONDITIONAL_SELF_TEST = 0x8
};


/********************************************************************
 *                  Function Prototypes
 ********************************************************************/

PUBLIC VOID FipsInitialize PROTO((VOID));

INT4 FipsUtilRunKnownAnsTest PROTO ((INT4, INT4 *));

INT4 FipsGetKnownAnsTestFlags PROTO ((INT4 i4KatFlag));

VOID FipsSetKnownAnsTestFlags PROTO ((INT4 i4KatFlagValue));

VOID FipsUpdtaeFirstGenRandNum PROTO ((INT4));

PUBLIC VOID FipsDeinitialize PROTO((VOID));

PUBLIC VOID FipsApiSetDefaultFipsModeConfig PROTO((UINT1));

PUBLIC INT4 FipsGetFipsCurrOperMode PROTO((VOID));

PUBLIC INT4 FipsApiSwitchFipsToLegacy PROTO ((VOID));

PUBLIC INT4 FipsGetFirstGenRandNum PROTO((VOID));

INT4 FipsUtilSwitchFipsToLegacy PROTO ((VOID));

INT4 FipsApiRunKnownAnsTest PROTO ((INT4 i4FipsTestAlgo, INT4 *pi4ErrCode));

PUBLIC INT4 FipsSetSwitchMode PROTO ((INT4 i4FipsOperMode));

PUBLIC INT4 FipsSelfTestRsa PROTO ((VOID));

PUBLIC INT4 FipsSelfTestDsa PROTO ((VOID));
#ifdef EXT_CRYPTO_WANTED
extern INT4 ExtWrUtilSetSwitchMode (INT4 i4FipsOperMode);
#endif
INT1 FpamWriteUsersDefault (VOID);
#endif /* __FIPS_H_ */
