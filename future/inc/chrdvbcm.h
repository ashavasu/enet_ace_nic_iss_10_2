/***************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: chrdvbcm.h,v 1.30 2011/04/27 05:14:29 siva Exp $
 *
 * Description: This file contains BCM platform specific Macro's, Constants,
 *              Globals and Structure Definition's of ISS Future Kernel Module
 *
 **************************************************************************/
#ifndef _CHRDEV_BCM_H_
#define _CHRDEV_BCM_H_

#include "fsvlan.h"

#define GDD_DEVICE_FILE_NAME "/dev/FsGddChrDev"

#define GDD_MINOR_NUMBER 0

#define GDD_INTERFACE         _IOWR(GDD_MAJOR_NUMBER, 25, char *)
#define GDD_CLI_IOCTL         _IOWR(GDD_MAJOR_NUMBER, 26, char *)

#ifdef KERNEL_WANTED
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,0)
#define KERNEL_MODULE_NAME        "FutureKernel.ko"
#else
#define KERNEL_MODULE_NAME        "FutureKernel.o"
#endif
#endif

typedef struct { 
    tMacAddr MacAddr;
    UINT2 VlanId;
}tHandleVlanPortPbmpChange;

typedef struct { 
    UINT2 u2AggIndex;
}tHandleChangeForPortMcInfo;

typedef struct { 
    UINT4 u4IfIndex;
    UINT4 u4LinkStatus;
}tHandlePortStateCallBack;

typedef struct { 
    UINT4             u4IfIndex; /* CFA IfIndex on which the packet 
                                    is received */
    UINT4             u4PktLen; /* Packet len according to flags */
}tHandlePacketRxCallBack;

#ifdef MBSM_WANTED
typedef struct { 
    INT4              i4EventType; /* Event identifier */
    UINT4             u4MsgLen;    /* Messge length */
}tHandleMbsmEventFromDriver;
#endif

#ifdef L2RED_WANTED
typedef struct {
    UINT4  u4PktLen;
}tNpRedBulkUpdate;

typedef struct {
    UINT1  u1FilterDevice;  
    UINT4  u4PortNo;
    UINT4  u4FpEntry0;
    UINT4  u4FpEntry1;
}tPnacFilterSync;

typedef struct {
    UINT4  u4PortNo;
    UINT4  u4BlkFilter;
    UINT4  u4LBFilter;
    UINT4  u4BlkFP;
    UINT4  u4LBFP;
}tEoamFilterSync;

typedef struct {
    UINT1  u1FilterDevice;  
    UINT4  u4FilterId;
}tIgsFilterSync;

typedef struct {
    UINT4  u4NpInitCount;
}tIgmpNpInitSync;

typedef struct {
    UINT1  u1FilterDevice;  
    UINT4  u4FilterId;
}tPimFilterSync;

typedef struct {
    UINT1  u1FilterDevice;  
    UINT4  u4FilterId;
}tOspfFilterSync;

typedef struct {
    UINT4  u4MsgType;
    INT4   i4SlotId;
    UINT4  u4FltCount;
}tFltCntSync;

typedef struct {
    UINT1  u1FilterDevice;  
    UINT4  u4FpEntry;
}tEcfmFilterSync;

typedef struct {
    UINT1  u1FilterDevice;  
    UINT4  u4StpFilterId;
    UINT4  u4GvrpFilterId;
    UINT4  u4GmrpFilterId;
    UINT4  u4Dot1XFilterId;
    UINT4  u4LacpFilterId;
}tVlanFilterSync;

typedef struct {
    UINT2    u2SyncProtocolId;  
    tMacAddr MacAddr;
}tVlanFilterAddrSync;

typedef struct {
    INT4   i4FilterNo;
    UINT4  u4FltrHandle;
    UINT4  u4BcmFPEntry[2];
    INT4   i4Action;
    INT2   i2NpFPChainId;
    UINT1  u1AclFltrType;
}tAclFilterSync;

typedef struct {
    INT2   i2FirstFreeChainIdx;
    INT2   i2ChainIdxFreed;
}tAclChainIdxSync;

typedef struct {
    UINT4 u4IfIndex;
    UINT4 u4RedL3IntfId;
}tIpSync;

typedef struct {
    UINT1  u1FilterDevice;  
    UINT4  u4PvrstNpDot1qIslFilterId;
    UINT4  u4PvrstNpIslFilterId;
}tPvrstFilterSync;

typedef struct {
    UINT4  u4MsgType;
    tMbsmSlotInfo MbsmSlotInfo;
}tCfaPortStatusSync;

typedef struct {
    UINT2  u2MsgType; /* Synchorization msg type */
    union {
        tNpRedBulkUpdate BulkUpdate;
        tPnacFilterSync  PnacFltSync;
        tEoamFilterSync  EoamFltSync;
        tIgsFilterSync   IgsFltSync;
        tIgmpNpInitSync  IgmpNpInitSync;
        tPimFilterSync   PimFltSync;
        tOspfFilterSync  OspfFltSync;
        tFltCntSync      FltCntSync;
        tEcfmFilterSync  EcfmFltSync;
        tVlanFilterSync  VlanFltSync;
        tAclFilterSync   AclFltSync;
        tAclChainIdxSync AclChainIdxSync;
        tIpSync          IpSync;
        tVlanFilterAddrSync VlanFltAddrSync;
        tPvrstFilterSync  PvrstFltSync;
        tCfaPortStatusSync   PortStatusSync;
    }uNpRedMsg;
}tHandleNpRedCallBack;
#endif

typedef struct _tdata {
   tHeader hdr;
   union {
   tHandlePortStateCallBack PsCbk;
   tHandlePacketRxCallBack  RxCbk;
#ifdef MBSM_WANTED
   tHandleMbsmEventFromDriver MbsmEventFromDriver;
#endif
   tHandleChangeForPortMcInfo PortMcInfo;
   tHandleVlanPortPbmpChange  VlanPortPbmpChng;
#ifdef L2RED_WANTED
   tHandleNpRedCallBack NpRedCbk;
#endif
   tVlanSrcRelearnTrap VlanSrcRelearnTrap;
   } u;
}tData;

typedef enum {
   PORT_STATE_CHANGE,
   PACKET_RX,
#ifdef MBSM_WANTED
   MBSM_EVENT,
#endif
#ifdef ARP_WANTED
   L2_SRC_MOVEMENT,
#endif
   COPY_PORT_MCAST_INFO,
   REMOVE_PORT_MCAST_INFO,
   PIM_VLAN_PORTPBMP_CHANGE,
   DVMRP_VLAN_PORTPBMP_CHANGE,
   IGP_VLAN_PORTPBMP_CHANGE,
#ifdef L2RED_WANTED
   NP_RED_SYNC_UP,
#endif
   L2_MAC_RELEARN,
   ECFM_RX_EXPIRY,
   ECFM_TX_FAIL,
   ATTACK_PACKET /* Event name to identify the attack packet 
     while reading from character device*/
}tCallBackEvent;

/* FD used for performing ioctl ()'s from user land */
#define GDD_CHAR_DEV_FD       gi4DevFd

typedef union {
    tOobIfName      IfName;
    tSourcePort     SourcePort;
    tKernIntfParams KernIntfParams;
#ifdef WGS_WANTED
    tKernMgmtVlanParams KernMgmtVlanParams;
#endif
#ifdef VRRP_WANTED
    tVrrpAssocIpParams VrrpAssocIpParams;
#endif
    tVlanFdbEntryParams VlanFdbEntryParams;
    tVlanFdbParams VlanFdbParams;
#ifdef MBSM_WANTED
    tStackingParams KernStackParams;
#endif
}tKernCmdParam;

int NpIoctl (unsigned long p);

INT4 KAPIUpdateInfo (INT4 i4Command, tKernCmdParam *pCmdParam);

INT4  KernEnetProcessRxFrame PROTO ((UINT1 *pu1Data, UINT4 u4PktSize, UINT2 u2IfIndex, UINT2 VlanId));

#ifdef MBSM_WANTED
VOID KernPostMbsmEventFromDriver PROTO ((VOID *pHwMsg, 
                                         INT4 i4EventType, UINT4 u4MsgLen));
#endif
VOID KernPostIfStatusToUser PROTO ((UINT4 u4IfIndex, UINT4 u4IfStatus));

#ifdef L2RED_WANTED
VOID KernPostNpRedSyncUpToUser (UINT2 u2SyncUpMsgTye, UINT1 *pu1Data,
                                UINT4 u4PktSize,
                                tHandleNpRedCallBack NpSyncMsg); 
#endif

VOID KernVlanFdbTableAdd PROTO ((UINT2 u2Port, tMacAddr MacAddr, 
                                 UINT2 VlanId, UINT1 u1EntryType, 
                                 tMacAddr ConnectionId));

VOID KernVlanFdbTableRemove PROTO ((UINT2 VlanId, tMacAddr MacAddr));

VOID KernPostVlanPortMcastPropertiesChng PROTO ((INT4 i4EventType, 
                                                 UINT2 u2AggIndex));
VOID KernPostVlanPortPbmpChange PROTO ((INT4 i4EventType, 
                                        tMacAddr MacAddr, UINT2 VlanId));

INT4 GddKernSetIfOperStatus  PROTO ((UINT4 u4IfIndex, UINT1 u1OperStatus));

INT4 KernVlanFdbUpdateIoctl PROTO ((UINT4 u4IoctlParam));

#ifdef ARP_WANTED
VOID KernPostSrcMovementToUser PROTO ((VOID));
#endif
VOID
KernPostMacRelearnToUser PROTO ((UINT2 u2NewPort,UINT2 u2OldPort,
                                 tVlanId VlanId, UINT2 u2FdbId,
                                 tMacAddr MacAddr));
#ifdef ECFM_WANTED
VOID KernPostEcfmRxExpiry PROTO ((VOID));
VOID KernPostEcfmTxFail PROTO ((VOID));
#endif
#endif
