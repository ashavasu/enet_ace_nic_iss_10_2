/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rmonnpwr.h,v 1.1 2012/07/10 06:03:02 siva Exp $
 *
 * Description:This file contains the structure definitions and
 *              macros for NP wrapper of RMON
 *
 *******************************************************************/

#ifndef __RMON_NP_WR_H
#define __RMON_NP_WR_H

#include "rmon.h"


/* OPER ID */

#define  FS_RMON_HW_GET_ETH_STATS_TABLE                         1
#define  FS_RMON_HW_SET_ETHER_STATS_TABLE                       2

/* Required arguments list for the rmon NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

typedef struct {
    UINT4                  u4IfIndex;
    tRmonEtherStatsNode *  pEthStatsEntry;
} tRmonNpWrFsRmonHwGetEthStatsTable;

typedef struct {
    UINT4  u4IfIndex;
    UINT1  u1EtherStatsEnable;
    UINT1  au1pad[3];
} tRmonNpWrFsRmonHwSetEtherStatsTable;

typedef struct RmonNpModInfo {
union {
    tRmonNpWrFsRmonHwGetEthStatsTable  sFsRmonHwGetEthStatsTable;
    tRmonNpWrFsRmonHwSetEtherStatsTable  sFsRmonHwSetEtherStatsTable;
    }unOpCode;

#define  RmonNpFsRmonHwGetEthStatsTable  unOpCode.sFsRmonHwGetEthStatsTable;
#define  RmonNpFsRmonHwSetEtherStatsTable  unOpCode.sFsRmonHwSetEtherStatsTable;
} tRmonNpModInfo;


/* Function Prototype for Generic NP OpCode specific wrappers
 * defined in Rmonnputil.c */

UINT1 RmonFsRmonHwGetEthStatsTable PROTO ((UINT4 u4IfIndex, tRmonEtherStatsNode * pEthStatsEntry));
UINT1 RmonFsRmonHwSetEtherStatsTable PROTO ((UINT4 u4IfIndex, UINT1 u1EtherStatsEnable));

#endif /* _RMON_NP_WR_H */

