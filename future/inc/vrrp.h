/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vrrp.h,v 1.28 2017/12/16 11:57:10 siva Exp $
 *
 * Description: Vrrp's exported file.
 */

#ifndef _VRRP_H
#define _VRRP_H
#include "utilipvx.h"
#include "fswebnm.h"
#include "la.h"
/* Possible States of  State Event Machine */
# define         INITIAL_STATE           1
# define         BACKUP_STATE            2
# define         MASTER_STATE            3
# define         NO_STATE_CHANGE         4
# define         STATE_CHANGE            5

#define     VRRP_VERSION_2               1
#define     VRRP_VERSION_2_3             2
#define     VRRP_VERSION_3               3

#define    IPADDRESS_SIZE                4
#define    IPV6_ADDRESS_SIZE            16
#define    VRRP_HEADER_LEN               8

#define    TEXT_AUTHKEY_SIZE             8 /* Maximum size of text 
                                              authentication key */

#define VRRP_MAX_TX_IPV4_BUF_SIZE       (VRRP_HEADER_LEN + \
                                         (MAX_ASSO_ADDR_ENTRY * IPADDRESS_SIZE) + \
                                         TEXT_AUTHKEY_SIZE)

#define VRRP_MAX_TX_IPV6_BUF_SIZE       (VRRP_HEADER_LEN + \
                                         (MAX_ASSO_ADDR_ENTRY * IPV6_ADDRESS_SIZE))

#define VRRP_MAX_TX_RX_IPV4_PKT_SIZE    (IP_HDR_LEN + VRRP_MAX_TX_IPV4_BUF_SIZE)

/* For Ipv6, VRRP do not construct IPv6 header or expect IPv6 header when packet
 * is received */
#define VRRP_MAX_TX_RX_IPV6_PKT_SIZE    (VRRP_MAX_TX_IPV6_BUF_SIZE) 

#define VRRP_OK        0
#define VRRP_NOT_OK   -1

#define VRRP_LOCK()    VrrpLock ()
#define VRRP_UNLOCK()  VrrpUnLock ()

#define VRRP_NW_INTF_CREATE               1
#define VRRP_NW_INTF_SECONDARY_CREATE     2
#define VRRP_NW_INTF_DELETE               3
#define VRRP_NW_INTF_SECONDARY_DELETE     4
#define VRRP_NW_INTF_MCAST_CREATE         5
#define VRRP_NW_INTF_MCAST_DELETE         6

/* The below network actions are only for Lnxip */
#define VRRP_NW_INTF_ACCEPT_ADD           7
#define VRRP_NW_INTF_ACCEPT_DEL           8

#define VRRP_NP_CREATE_INTERFACE   1
#define VRRP_NP_DELETE_INTERFACE   2
#define VRRP_NP_GET_INTERFACE      3

/* Macros for validating Decrement Priority against Admin Priority */
#define VRRP_VAL_PRIO_OPER_IP_ADD              1
#define VRRP_VAL_PRIO_OPER_IP_DEL              2
#define VRRP_VAL_PRIO_OPER_IP_MODIFY           3

#define VRRPMODULE                           112

typedef  INT1 (*VrrpFuncPtr)(INT4, INT4); 

INT1  VrrpRegisterwithIp PROTO ((UINT1 u1ModuleId,
                                       VrrpFuncPtr pLinkStatusFunc));

INT1 VrrpGetMacAddress PROTO ((UINT4 u4IfIndex, INT4 i4VrId, INT4 i4AddrType,
                               UINT1  *pu1MacAddress));

INT1 VrrpEnabledOnInterface PROTO ((INT4 i4Portno));
INT4 VrrpApiValIfIpChange PROTO ((UINT4 u4IfIndex,tIPvXAddr CurIpAddr,UINT4 u4IpSubnetMask));
INT4 VrrpApiValIfTrackIfExists PROTO ((INT4 i4RecvdIfIndex));

INT4 VrrpApiValDecPriority PROTO ((UINT4 u4IfIndex, UINT1 u1Operation,
                                   tIPvXAddr PrevIpAddr, tIPvXAddr CurIpAddr));

UINT4 VrrpCreateTask PROTO ((VOID));
INT4 VrrpGetIfIpAddr  PROTO ((INT4 i4IfIndex));
INT4  VrrpLock PROTO ((VOID));
INT4  VrrpUnLock PROTO ((VOID));

INT1 VrrpGetVrIdFromMacAddr PROTO ((UINT1 *u1MacAddr, INT4 *i4VrId ));

INT1 VrrpGetState PROTO ((UINT4 u4IfIndex, INT4 i4VrId, INT4 i4AddrType));

INT1 IsVrrpMasterIpAddress PROTO ((UINT4 u4IfIndex, UINT4 u4IpAddr, INT4 *i4VrId));
INT1 IsVrrpVirtualIpAddress (UINT4 u4IfIndex, UINT4 u4IpAddr, INT4 *i4VrId);

INT1 VrrpGetVridFromAssocIpAddress PROTO ((UINT4 u4IfIndex, tIPvXAddr IpAddr,
                                           INT4 *pi4VrId));

VOID  VrrpMain PROTO ((INT1 *));
VOID  VrrpShutDown ARG_LIST((VOID));

INT4 VrrpHandleIfDelete ARG_LIST((UINT4 u4PortNo));

INT4 VrrpIpIfHandleCardAttach (tMbsmSlotInfo *pSlotInfo,INT4 i4AddrType);
INT4 VrrpHandleCardAttach (tMbsmSlotInfo *pSlotInfo,INT4 i4AddrType);
INT4 VrrpIpIfMbsmHwWr (tMbsmSlotInfo *pSlotInfo,INT4 i4OperIndex);
INT1
VrrpAcceptModeEnabledOnInterface (INT4 i4IfIndex, INT4 i4VrId, INT4 i4AddrType, tIPvXAddr IpAddr);
VOID VrrpHandleMclagIvrOperStatus (INT4 i4PortIndex, UINT1 u1OperStatus);
VOID VrrpSetLocalAffinity (UINT1 u1LocalAffinity);

#ifndef _VRRP_WEB_C_
#define _VRRP_WEB_C_
VOID VrrpWebConfPageGet (tHttp *pHttp);
VOID VrrpWebConfPageSet (tHttp *pHttp);
VOID VrrpWebAssocPageGet (tHttp *pHttp);
VOID VrrpWebAssocPageSet (tHttp *pHttp);
VOID VrrpWebBasicGet (tHttp *pHttp);
VOID VrrpWebBasicSet (tHttp *pHttp);
VOID VrrpWebTrackPageGet (tHttp *pHttp);
VOID VrrpWebTrackPageSet (tHttp *pHttp);
VOID VrrpWebStatsPage (tHttp *pHttp);
#endif

#endif /* _VRRP_H */
