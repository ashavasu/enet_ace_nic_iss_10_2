/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: rfhwnpwr.h,v 1.7 2017/12/12 13:28:30 siva Exp $
 *
 * Description: This file contains type npapi type definitions 
 *              for rfmgmt module 
 ********************************************************************/

#ifndef __RFMGMT_NP_WR_H__
#define __RFMGMT_NP_WR_H__

#ifdef NPAPI_WANTED

#define FS_RFMGMT_SCAN_NEIGHBR_MSG           1
#define FS_RFMGMT_GET_NEIGHBOR_INFO          2
#define FS_RFMGMT_SCAN_CLIENT_MSG            3
#define FS_RFMGMT_GET_CLIENT_INFO            4

#define FS_RFMGMT_MAX_NEIGH_PKT_LEN          14

#define FS_RFMGMT_SCAN_CHANNEL_OFFSET        10
#define FS_RFMGMT_RSSI_OFFSET                12

#define FS_RFMGMT_MAX_CLIENT_PKT_LEN         12
#define FS_RFMGMT_SNR_OFFSET                 10


#define WSSMAC_MAX_SSID_LEN                  32
#define FS_RF_GROUP_ID_MAX_LEN              19 
#define AP_DISCOVERED                        1
 
/* #define RFMGMT_MAX_CHANNELA       37 */
typedef struct{
   UINT4    u4Frequency;
   UINT4    u4ChannelNum;
   UINT4    u4DFSFlag;
   UINT4    u4DFSCacTime;
}tRfMgmtDFSInfo;

typedef struct
{
  tRadioInfo   RadInfo;
  tRfMgmtDFSInfo DfsInfo[RADIOIF_MAX_CHANNEL_A];
}tRfMgmtDFSChannelInfo;

typedef struct {
    tRadioInfo  RadInfo;
    UINT2       u2ScannedChannel;
    INT2        i2Rssi;
    tMacAddr    NeighborAPMac;
    UINT1       au1HtOperation[6];
    UINT4       u4RogueApLastReportedTime;
    UINT1       au1RfGroupName[CAPWAP_RF_GROUP_NAME_SIZE];
    UINT1       u1StationCount;
    BOOL1       isRogueApProtectionType;
    BOOL1       isRogueApStatus;
    UINT1       u1NonGFPresent;
    UINT1       u1OBSSNonGFPresent;
    UINT1       u1SecChannelOffset;
    UINT1       bssload[8];
    UINT1       au1Pad[2];
}tRfMgmtNeighborAPInfo;

typedef struct
{
  tRadioInfo   RadInfo;
  UINT2        u2HTEnabled;
  UINT1        u1CurrentChannel;
  UINT1        u1Bandwidth;
  UINT1        u1VHTEnabled;
  UINT1        u1CentreFreq0;
  UINT1        u1CentreFreq1;

}tRfMgmtCacInfo;

typedef struct {
    tRadioInfo  RadInfo;
    UINT2       u2SNR;
    tMacAddr    ClientMac;
}tRfMgmtClientInfo;

typedef VOID (*tNeighAPScanFn) (tRfMgmtNeighborAPInfo *);
typedef VOID (*tClientScanFn)(tRfMgmtClientInfo *);

typedef struct {
    tRadioInfo       RadInfo;
    tNeighAPScanFn   *pNeighAPScanFn;
    UINT1  u1ChannelToBeScanned;
}tRfMgmtNpWrHwScanNeighInfo;

typedef struct {
    tRadioInfo       RadInfo;
    /* tNeighAPScanFn   *pNeighAPScanFn;*/
    UINT2      u2QuietDuration;
    UINT1            u1QuietPeriod;
}tRfMgmtNpWrHwQuietIEInfo;

typedef struct {
    tRadioInfo       RadInfo;
    tClientScanFn    *pClientScanFn;
}tRfMgmtNpWrHwScanClientInfo;

typedef struct {
    tRfMgmtClientInfo   RfMgmtClientInfo;
}tRfMgmtNpWrHwGetClientInfo;

typedef struct {
    tRfMgmtNeighborAPInfo   RfMgmtNeighborAPInfo;
}tRfMgmtNpWrHwGetNeighInfo;

typedef struct RfMgmtNpModInfo {
 union {
  tRfMgmtDFSChannelInfo         sConfigDFSChannelInfo;
  tRfMgmtNpWrHwScanNeighInfo    sConfigScanNeighInfo; 
  tRfMgmtNpWrHwGetNeighInfo     sConfigGetNeighInfo; 
  tRfMgmtNpWrHwScanClientInfo   sConfigScanClientInfo; 
  tRfMgmtNpWrHwGetClientInfo     sConfigGetClientInfo; 
 }unOpCode;

#define  RadioIfNpWrHwGetNeighInfo          unOpCode.sConfigGetNeighInfo;
#define  RadioIfNpWrHwScanNeighInfo         unOpCode.sConfigScanNeighInfo;
#define  RadioIfNpWrHwGetClientInfo         unOpCode.sConfigGetClientInfo;
#define  RadioIfNpWrHwScanClientInfo        unOpCode.sConfigScanClientInfo;
#define  RadioIfNpWrHwGetDFSChannelInfo     unOpCode.sConfigDFSChannelInfo;
}tRfMgmtNpModInfo;

#endif
#endif
