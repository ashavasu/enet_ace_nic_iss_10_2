/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dscxe.h,v 1.7 2007/05/09 10:43:44 iss Exp $
 *
 * Description:This file contains the definitions and  
 *             macros of FS DiffServer                                   
 *
 *******************************************************************/
#ifndef _DSCXE_H_
#define _DSCXE_H_

#define DFS_LOCK()                   DfsLock ()
#define DFS_UNLOCK()                 DfsUnLock ()

/* Return values for the DS module */
#define DS_FAILURE                   FAILURE
#define DS_SUCCESS                   SUCCESS

#define DS_SNMP_TRUE                 1     
#define DS_SNMP_FALSE                2

#define DS_TRUE                      TRUE
#define DS_FALSE                     FALSE

#define DS_MAC_FILTER                1     
#define DS_IP_FILTER                 2

#define DS_MAX_PORTS                SYS_DEF_MAX_PHYSICAL_INTERFACES
/* Port List size */
#define DS_PORT_LIST_SIZE           ((DS_MAX_PORTS + 31)/32 * 4)


typedef tTMO_SLL_NODE       tDsSllNode;
typedef UINT1 tDsPortList[DS_PORT_LIST_SIZE];

/* Enum for Interface Direction */
typedef enum
{
   DS_INGRESS = 1,
   DS_EGRESS 
}tDsIfDirection;

/* Diffserv Classifier Entry */
typedef struct DiffServClfrEntry
{
    tDsSllNode     DsNextNode;
    INT4           i4DsClfrMFClfrId;
    INT4           i4DsClfrInProActionId;
    INT4           i4DsClfrTrafficClassId;
    INT4           i4DsClfrId; 
    UINT1          u1DsClfrTrafficClassFlag; /* Indicates whether any traffic 
                                                class has been configured for
                                                this Classifier entry */
    UINT1          u1DsClfrStatus;
    UINT1          u1Pack[2];
}tDiffServClfrEntry;

/* Diffserv MF Classifier Entry - MF Filter */
typedef struct DiffServMultiFieldClfrEntry
{
    tDsSllNode     DsNextNode;
    INT4           i4DsMultiFieldClfrId;
    UINT4          u4DsMultiFieldClfrFilterId;
    UINT1          u1DsMultiFieldClfrFilterType;
    UINT1          u1DsMultiFieldClfrStatus;
    tDsPortList    DsPorts;
    UINT1          u1Pack[2];
}tDiffServMultiFieldClfrEntry;

/* In Profile Action Entry */
typedef struct DiffServInProfileActionEntry
{
    tDsSllNode     DsNextNode;
    UINT4          u4DsInProfileActionFlag;
    UINT4          u4DsInProfileActionNewPrio;
    UINT4          u4DsInProfileActionIpTOS;
    UINT4          u4DsInProfileActionPort;
    UINT4          u4DsInProfileActionDscp;
    INT4           i4DsInProfileActionId;               
    UINT1          u1DsInProfileActionStatus;
    UINT1          u1Pack[3];
}tDiffServInProfileActionEntry;

/* Diffserv Classifier Data values - Used for WEB Get */
typedef struct DiffServWebClfrData
{
    UINT1          au1DsClfrInProfActionType[28];
    UINT1          au1DsClfrInProfActionValue[16];
}tDiffServWebClfrData;

/* Diffserv Classifier Data values - Used for WEB Set */
typedef struct DiffServWebSetClfrEntry
{
    INT4           i4DsClfrMFClfrId;
    INT4           i4DsClfrInProActionId;
    UINT4          u4InProfActType;
    UINT4          u4InDscp;
    UINT4          u4InPrec;
    UINT4          u4CosValue;
    INT4           i4DsClfrTrafficClassId;    
    INT4           i4DsClfrId;
}tDiffServWebSetClfrEntry;


/* Prototype required for other modules */
INT4 DsGetStartedStatus (VOID);
INT4 DsCheckDiffservPort (INT4 i4TrunkPort);
INT4 DsCheckMFClfrTable (INT4 i4FilterNo, INT4 i4FilterType);

INT4 DsInit  (VOID);
INT4 DsStart (VOID);

INT4 DfsLock (VOID);
INT4 DfsUnLock (VOID);


/* ************** Switchcore CXE specific QoS information *************** */

#define DS_MAX_TC               1023

#define DS_MIN_REDCURVES           1 /* Valid RED curves are from 1-126 */
#define DS_MAX_REDCURVES         126 /* RED curves 0 and 127 are reserved */

#define DS_MIN_PORT_RED_CURVE     64 /* Port RED curves range from 64-126 */

#define DS_DEF_PORT_MAX_BW     15625 /* Default value for port's max. bandwidth
                                      * - 15625 * 64000bps = 1Gbps */ 

#define DS_DEF_WEIGHT_FACTOR   4
#define DS_DEF_WEIGHT_SHIFT    4

/* Drop levels apply only when the traffic rate exceeds the Max. bandwidth 
 * (for the port or Traffic Class) */

#define DS_MIN_DROP_LEVEL1    14 /* Corresponds to 2^14 = 16KB */
#define DS_MAX_DROP_LEVEL1    27 /* Corresponds to 2^27 = 128MB */

#define DS_MIN_DROP_LEVEL2    17 /* Corresponds to 2^17 = 128KB above level1 */
#define DS_MAX_DROP_LEVEL2    24 /* Corresponds to 2^24 = 16MB above level1 */

#define DS_MIN_DROP_LEVEL3    17
#define DS_MAX_DROP_LEVEL3    24

#define DS_MIN_RED_QRANGE     14 /* Corresponds to 2^14 = 16KB */
#define DS_MAX_RED_QRANGE     28 /* Corresponds to 2^28 = 256MB */

#define DS_MIN_RED_STOPPROB    0
#define DS_MAX_RED_STOPPROB  200

typedef struct DiffServREDConfig
{
   UINT4  u4REDStart;
   UINT4  u4REDStop;
   UINT4  u4REDQRange;
   UINT4  u4REDStopProbability;
   UINT4  u4REDCurveStatus;  /* Conceptual row status */
}tDiffServREDEntry;

typedef struct DiffServPortEntry
{
    UINT4             u4MaxBw;
    UINT4             u4REDCurveId;  /* Index into the RED curve array */
    UINT4             u4DropAtMaxBw; /* True/ False */
    UINT4             u4DropLevel1;
    UINT4             u4DropLevel2;
    UINT4             u4DropLevel3;
}tDiffServPortEntry;

typedef struct DiffServTCEntry 
{
    UINT4             u4PortNum;    /* The port on which this TC is attached */ 
    UINT4             u4MaxBw;      /* Max Bw allowed for this TC */
    UINT4             u4GuaranteedBw;/* Guaranteed BW for this TC */
    UINT4             u4DropLevel1; 
    UINT4             u4DropLevel2; /* Water-mark levels for dropping packets*/ 
    UINT4             u4DropLevel3;  
    UINT4             u4DropAtMaxBw; /* True/ False */
    UINT4             u4WeightFactor; 
    UINT4             u4WeightShift;  /* Trafffic class weights */
    UINT4             u4REDCurveId;  /* Index into the RED curve array */
    UINT4             u4FirstFlowGroup; 
    UINT4             u4NumFlowGroups;   /* Number of fair-queues */
    UINT4             u4PortIndependent; /* A value of TRUE indicates that this
                                            traffic class is not attached to 
                                            any port. Max. bandwidth alone can
                                            be configured on such traffic 
                                            classes */
    UINT4             u4EntryStatus; /* Conceptual row status object */
}tDiffServTCEntry;


/* ********************************************************** */


#endif /* _DSCXE_H_ */
