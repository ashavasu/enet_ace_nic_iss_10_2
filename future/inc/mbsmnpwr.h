
/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: mbsmnpwr.h,v 1.1 2013/03/19 12:25:43 siva Exp $
 *
 * Description:This file contains the necessary data strucutures
 *             for packet handling in NPAPI layer
 *             <Part of dual node stacking model>
 *
 *******************************************************************/

#ifndef __MBSM_NP_WR_H__
#define __MBSM_NP_WR_H__

#include "mbsnp.h"

/* OPER ID */
#ifdef L2RED_WANTED
#define  MBSM_NP_PROC_RM_NODE_TRANSITION                        1
#define  MBSM_INIT_NP_INFO_ON_STANDBY_TO_ACTIVE                 2
#define  MBSM_NP_GET_SLOT_INFO                                  3
#endif /* L2RED_WANTED */
#define  MBSM_NP_INIT_HW_TOPO_DISC                              4
#define  MBSM_NP_CLEAR_HW_TBL                                   5
#define  MBSM_NP_PROC_CARD_INSERT_FOR_LOAD_SHARING              6
#define  MBSM_NP_UPDT_HW_TBL_FOR_LOAD_SHARING                   7
#define  MBS_NP_SET_LOAD_SHARING_STATUS                         8
#define  MBSM_NP_GET_CARD_TYPE_TABLE                            9
#define  MBSM_NP_HANDLE_NODE_TRANSITION                         10
#define  MBSM_NP_TX_ON_STACK_INTERFACE                          11
#define  MBSM_NP_GET_STACK_MAC                                  12

/* Required arguments list for the mbsm NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

#ifdef L2RED_WANTED
typedef struct {
    INT4   i4Event;
    UINT1  u1PrevState;
    UINT1  u1State;
    UINT1  au1pad[2];
} tMbsmNpWrMbsmNpProcRmNodeTransition;

typedef struct {
    tMbsmSlotInfo *  pSlotInfo;
} tMbsmNpWrMbsmInitNpInfoOnStandbyToActive;

typedef struct {
    INT4          i4SlotId;
    tMbsmHwMsg *  pHwMsg;
} tMbsmNpWrMbsmNpGetSlotInfo;

#endif /* L2RED_WANTED */
typedef struct {
    INT4  i4SlotId;
    INT1  i1NodeState;
    UINT1 au1pad[3];
} tMbsmNpWrMbsmNpInitHwTopoDisc;

typedef struct {
    INT4  i4SlotId;
} tMbsmNpWrMbsmNpClearHwTbl;

typedef struct {
    INT4  i4MsgType;
} tMbsmNpWrMbsmNpProcCardInsertForLoadSharing;

typedef struct {
    UINT1  u1Flag; 
    UINT1  au1pad[3];
} tMbsmNpWrMbsmNpUpdtHwTblForLoadSharing;

typedef struct {
    INT4  i4LoadSharingFlag;
} tMbsmNpWrMbsNpSetLoadSharingStatus;

typedef struct {
    INT4             i4LoopIdx;
    tMbsmCardInfo *  pMbsmCardInfo;
} tMbsmNpWrMbsmNpGetCardTypeTable;

typedef struct {
    INT4   i4Event;
    UINT1  u1PrevState;
    UINT1  u1State;
    UINT1  au1pad[2];
} tMbsmNpWrMbsmNpHandleNodeTransition;

typedef struct {
    UINT1 *  pu1Pkt;
    INT4     i4PktSize;
} tMbsmNpWrMbsmNpTxOnStackInterface;

typedef struct {
    UINT4    u4SlotId;
    UINT1 *  pu1MacAddr;
} tMbsmNpWrMbsmNpGetStackMac;

typedef struct MbsmNpModInfo {
union {
#ifdef L2RED_WANTED
    tMbsmNpWrMbsmNpProcRmNodeTransition  sMbsmNpProcRmNodeTransition;
    tMbsmNpWrMbsmInitNpInfoOnStandbyToActive  sMbsmInitNpInfoOnStandbyToActive;
    tMbsmNpWrMbsmNpGetSlotInfo  sMbsmNpGetSlotInfo;
#endif /* L2RED_WANTED */
    tMbsmNpWrMbsmNpInitHwTopoDisc  sMbsmNpInitHwTopoDisc;
    tMbsmNpWrMbsmNpClearHwTbl  sMbsmNpClearHwTbl;
    tMbsmNpWrMbsmNpProcCardInsertForLoadSharing  sMbsmNpProcCardInsertForLoadSharing;
    tMbsmNpWrMbsmNpUpdtHwTblForLoadSharing  sMbsmNpUpdtHwTblForLoadSharing;
    tMbsmNpWrMbsNpSetLoadSharingStatus  sMbsNpSetLoadSharingStatus;
    tMbsmNpWrMbsmNpGetCardTypeTable  sMbsmNpGetCardTypeTable;
    tMbsmNpWrMbsmNpHandleNodeTransition  sMbsmNpHandleNodeTransition;
    tMbsmNpWrMbsmNpTxOnStackInterface  sMbsmNpTxOnStackInterface;
    tMbsmNpWrMbsmNpGetStackMac  sMbsmNpGetStackMac;
    }unOpCode;

#ifdef L2RED_WANTED
#define  MbsmNpMbsmNpProcRmNodeTransition  unOpCode.sMbsmNpProcRmNodeTransition;
#define  MbsmNpMbsmInitNpInfoOnStandbyToActive  unOpCode.sMbsmInitNpInfoOnStandbyToActive;
#define  MbsmNpMbsmNpGetSlotInfo  unOpCode.sMbsmNpGetSlotInfo;
#endif /* L2RED_WANTED */
#define  MbsmNpMbsmNpInitHwTopoDisc  unOpCode.sMbsmNpInitHwTopoDisc;
#define  MbsmNpMbsmNpClearHwTbl  unOpCode.sMbsmNpClearHwTbl;
#define  MbsmNpMbsmNpProcCardInsertForLoadSharing  unOpCode.sMbsmNpProcCardInsertForLoadSharing;
#define  MbsmNpMbsmNpUpdtHwTblForLoadSharing  unOpCode.sMbsmNpUpdtHwTblForLoadSharing;
#define  MbsmNpMbsNpSetLoadSharingStatus  unOpCode.sMbsNpSetLoadSharingStatus;
#define  MbsmNpMbsmNpGetCardTypeTable  unOpCode.sMbsmNpGetCardTypeTable;
#define  MbsmNpMbsmNpHandleNodeTransition  unOpCode.sMbsmNpHandleNodeTransition;
#define  MbsmNpMbsmNpTxOnStackInterface  unOpCode.sMbsmNpTxOnStackInterface;
#define  MbsmNpMbsmNpGetStackMac  unOpCode.sMbsmNpGetStackMac;
} tMbsmNpModInfo;


/* Function Prototype for Generic NP OpCode specific wrappers
 * defined in Mbsmnpapi.c */

#ifdef L2RED_WANTED
UINT1 MbsmMbsmNpProcRmNodeTransition PROTO ((INT4 i4Event, UINT1 u1PrevState, UINT1 u1State));
UINT1 MbsmMbsmInitNpInfoOnStandbyToActive PROTO ((tMbsmSlotInfo * pSlotInfo));
UINT1 MbsmMbsmNpGetSlotInfo PROTO ((INT4 i4SlotId, tMbsmHwMsg * pHwMsg));
#endif /* L2RED_WANTED */
UINT1 MbsmMbsmNpInitHwTopoDisc PROTO ((INT4 i4SlotId, INT1 i1NodeState));
UINT1 MbsmMbsmNpClearHwTbl PROTO ((INT4 i4SlotId));
UINT1 MbsmMbsmNpProcCardInsertForLoadSharing PROTO ((INT4 i4MsgType));
UINT1 MbsmMbsmNpUpdtHwTblForLoadSharing PROTO ((UINT1 u1Flag));
UINT1 MbsmMbsNpSetLoadSharingStatus PROTO ((INT4 i4LoadSharingFlag));
UINT1 MbsmMbsmNpGetCardTypeTable PROTO ((INT4 i4LoopIdx, tMbsmCardInfo * pMbsmCardInfo));
UINT1 MbsmMbsmNpHandleNodeTransition PROTO ((INT4 i4Event, UINT1 u1PrevState, UINT1 u1State));
UINT1 MbsmMbsmNpTxOnStackInterface PROTO ((UINT1 * pu1Pkt, INT4 i4PktSize));
UINT1 MbsmMbsmNpGetStackMac PROTO ((UINT4 u4SlotId, UINT1 * pu1MacAddr));

#endif /* __MBSM_NP_WR_H__ */
