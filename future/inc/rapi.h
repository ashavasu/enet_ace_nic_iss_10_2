/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rapi.h,v 1.14 2007/02/01 14:52:30 iss Exp $
 *
 * Description:This file contains the exported definitions and  
 *             macros of RAPI                                  
 *
 *******************************************************************/
#ifndef _RAPI_H
#define _RAPI_H

/* RAPI Event Types  */
#define   RAPI_PATH_EVENT     1 
#define   RAPI_RESV_EVENT     2 
#define   RAPI_PATH_ERROR     3 
#define   RAPI_RESV_ERROR     4 
#define   RAPI_RESV_CONFIRM   5 

#endif /* _RAPI_H */
