/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: esat.h,v 1.1 2014/08/14 12:52:03 siva Exp $
*
* Description: This file contains the function prototypes and macros of 
*       ESAT module*
*******************************************************************/
#ifndef __ESAT_H__
#define __ESAT_H__


#define  ESAT_LOCK()      EsatApiLock ()
#define  ESAT_UNLOCK()    EsatApiUnLock ()

/* Trace Options */
#define ESAT_NO_TRC                 0x00000000
#define ESAT_INIT_SHUT_TRC          0x00000001
#define ESAT_MGMT_TRC               0x00000002
#define ESAT_DATA_PATH_TRC          0x00000004
#define ESAT_CTRL_TRC               0x00000008
#define ESAT_PKT_DUMP_TRC           0x00000010
#define ESAT_RESOURCE_TRC           0x00000020
#define ESAT_ALL_FAIL_TRC           0x00000040
#define ESAT_BUFFER_TRC             0x00000080
#define ESAT_CRITICAL_TRC           0x00000100
#define ESAT_ALL_TRC                0x000001ff


typedef enum {
    ESAT_DIRECTION_EXTERNAL = 1,
    ESAT_DIRECTION_INTERNAL = 2
}eEsatDirection;


VOID
EsatMainTask (INT1 *pi1Arg);

/***ESAT API functions ***/
INT4
EsatApiDeleteContext (UINT4 u4ContextId);

INT4
EsatApiHandleOperDownIndication (UINT4 u4IfIndex);

INT4
EsatApiLock (VOID);

INT4
EsatApiUnLock (VOID);

#endif /*__ESAT_H__*/
