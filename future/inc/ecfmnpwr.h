/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ecfmnpwr.h,v 1.6 2016/02/04 11:56:43 siva Exp $
 *
 * Description: Contains type definitions and data structure for
                wrappers in ECFM module
 *
 *******************************************************************/
#ifndef __ECFM_NP_WR_H_
#define __ECFM_NP_WR_H_

#include "ecfm.h"
#if defined (Y1564_WANTED) || defined (RFC2544_WANTED)
#include "ecfmminp.h"
#endif
/* OPER ID */

#define  FS_MI_ECFM_CLEAR_RCVD_LBR_COUNTER                      1
#define  FS_MI_ECFM_CLEAR_RCVD_TST_COUNTER                      2
#define  FS_MI_ECFM_GET_RCVD_LBR_COUNTER                        3
#define  FS_MI_ECFM_GET_RCVD_TST_COUNTER                        4
#define  FS_MI_ECFM_HW_CALL_NP_API                              5
#define  FS_MI_ECFM_HW_DE_INIT                                  6
#define  FS_MI_ECFM_HW_GET_CAPABILITY                           7
#define  FS_MI_ECFM_HW_GET_CCM_RX_STATISTICS                    8
#define  FS_MI_ECFM_HW_GET_CCM_TX_STATISTICS                    9
#define  FS_MI_ECFM_HW_GET_PORT_CC_STATS                        10
#define  FS_MI_ECFM_HW_HANDLE_INT_Q_FAILURE                     11
#define  FS_MI_ECFM_HW_INIT                                     12
#define  FS_MI_ECFM_HW_REGISTER                                 13
#define  FS_MI_ECFM_HW_SET_VLAN_ETHER_TYPE                      14
#define  FS_MI_ECFM_START_LBM_TRANSACTION                       15
#define  FS_MI_ECFM_START_TST_TRANSACTION                       16
#define  FS_MI_ECFM_STOP_LBM_TRANSACTION                        17
#define  FS_MI_ECFM_STOP_TST_TRANSACTION                        18
#define  FS_MI_ECFM_TRANSMIT1_DM                                19
#define  FS_MI_ECFM_TRANSMIT_DMM                                20
#define  FS_MI_ECFM_TRANSMIT_DMR                                21
#define  FS_MI_ECFM_TRANSMIT_LMM                                22
#define  FS_MI_ECFM_TRANSMIT_LMR                                23
#define  FS_MI_ECFM_START_LM                                    24
#define  FS_MI_ECFM_STOP_LM                                     25
#define  FS_MI_ECFM_SET_HIT_ME_ONCE                             26
#define  FS_MI_ECFM_ENABLE_HW_EVENTS                            27
#define  FS_MI_ECFM_GET_HW_MEP_STATUS                           28
#ifdef MBSM_WANTED
#define  FS_MI_ECFM_MBSM_HW_CALL_NP_API                         29
#define  FS_MI_ECFM_MBSM_NP_INIT_HW                             30
#endif
#if defined (Y1564_WANTED) || defined (RFC2544_WANTED)
#define  FS_MI_ECFM_SLA_TEST                                    31
#define  FS_MI_ECFM_ADD_HW_LOOPBACK_INFO                        32
#define  FS_MI_ECFM_DEL_HW_LOOPBACK_INFO                        33
#endif

/* Required arguments list for the ecfm NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

typedef struct {
    tEcfmMepInfoParams *  pEcfmMepInfoParams;
} tEcfmNpWrFsMiEcfmClearRcvdLbrCounter;

typedef struct {
    tEcfmMepInfoParams *  pEcfmMepInfoParams;
} tEcfmNpWrFsMiEcfmClearRcvdTstCounter;

typedef struct {
    tEcfmMepInfoParams *  pEcfmMepInfoParams;
    UINT4 *               pu4LbrIn;
} tEcfmNpWrFsMiEcfmGetRcvdLbrCounter;

typedef struct {
    tEcfmMepInfoParams *  pEcfmMepInfoParams;
    UINT4 *               pu4TstIn;
} tEcfmNpWrFsMiEcfmGetRcvdTstCounter;

typedef struct {
    tEcfmHwParams *  pEcfmHwInfo;
    UINT1            u1Type;
    UINT1            au1pad[3];
} tEcfmNpWrFsMiEcfmHwCallNpApi;

typedef struct {
    UINT4  u4ContextId;
} tEcfmNpWrFsMiEcfmHwDeInit;

typedef struct {
    UINT4    u4ContextId;
    UINT4 *  pu4HwCapability;
} tEcfmNpWrFsMiEcfmHwGetCapability;

typedef struct {
    UINT4                   u4ContextId;
    tEcfmCcOffMepRxStats *  pEcfmCcOffMepRxStats;
    UINT2                   u2RxFilterId;
    UINT1                   au1pad[2];
} tEcfmNpWrFsMiEcfmHwGetCcmRxStatistics;

typedef struct {
    UINT4                   u4ContextId;
    tEcfmCcOffMepTxStats *  pEcfmCcOffMepTxStats;
    UINT2                   u2TxFilterId;
    UINT1                   au1pad[2];
} tEcfmNpWrFsMiEcfmHwGetCcmTxStatistics;

typedef struct {
    UINT4                  u4IfIndex;
    UINT4                  u4ContextId;
    tEcfmCcOffPortStats *  pEcfmCcOffPortStats;
} tEcfmNpWrFsMiEcfmHwGetPortCcStats;

typedef struct {
    UINT4                     u4ContextId;
    tEcfmCcOffRxHandleInfo *  pEcfmCcOffRxHandleInfo;
    UINT2 *                   pu2RxHandle;
    BOOL1 *                   pb1More;
} tEcfmNpWrFsMiEcfmHwHandleIntQFailure;

typedef struct {
    UINT4  u4ContextId;
} tEcfmNpWrFsMiEcfmHwInit;

typedef struct {
    UINT4  u4ContextId;
} tEcfmNpWrFsMiEcfmHwRegister;

typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4ContextId;
    UINT2  u2EtherTypeValue;
    UINT1  u1EtherType;
    UINT1  au1pad[1];
} tEcfmNpWrFsMiEcfmHwSetVlanEtherType;

typedef struct {
    tEcfmMepInfoParams *  pEcfmMepInfoParams;
    tEcfmConfigLbmInfo *  pEcfmConfigLbmInfo;
} tEcfmNpWrFsMiEcfmStartLbmTransaction;

typedef struct {
    tEcfmMepInfoParams *  pEcfmMepInfoParams;
    tEcfmConfigTstInfo *  pEcfmConfigTstInfo;
} tEcfmNpWrFsMiEcfmStartTstTransaction;

typedef struct {
    tEcfmMepInfoParams *  pEcfmMepInfoParams;
} tEcfmNpWrFsMiEcfmStopLbmTransaction;

typedef struct {
    tEcfmMepInfoParams *  pEcfmMepInfoParams;
} tEcfmNpWrFsMiEcfmStopTstTransaction;

typedef struct {
    UINT4     u4IfIndex;
    UINT4     u4ContextId;
    UINT1 *   pu1DmPdu;
    UINT2     u2PduLength;
    tVlanTag  VlanTag;
    UINT1     u1Direction;
    UINT1     au1pad[1];
} tEcfmNpWrFsMiEcfmTransmit1Dm;

typedef struct {
    UINT4     u4IfIndex;
    UINT4     u4ContextId;
    UINT1 *   pu1DmmPdu;
    UINT2     u2PduLength;
    tVlanTag  VlanTag;
    UINT1     u1Direction;
    UINT1     au1pad[1];
} tEcfmNpWrFsMiEcfmTransmitDmm;

typedef struct {
    UINT4     u4IfIndex;
    UINT4     u4ContextId;
    UINT1 *   pu1DmrPdu;
    UINT2     u2PduLength;
    tVlanTag  VlanTag;
    UINT1     u1Direction;
    UINT1     au1pad[1];
} tEcfmNpWrFsMiEcfmTransmitDmr;

typedef struct {
    UINT4         u4IfIndex;
    UINT4         u4ContextId;
    UINT1 *       pu1LmmPdu;
    UINT2         u2PduLength;
    tVlanTagInfo  VlanTag;
    UINT1         u1Direction;
    UINT1         au1pad[1];
} tEcfmNpWrFsMiEcfmTransmitLmm;

typedef struct {
    UINT4           u4IfIndex;
    UINT4           u4ContextId;
    UINT1 *         pu1LmrPdu;
    tVlanTagInfo *  pVlanTag;
    UINT2           u2PduLength;
    UINT1           u1Direction;
    UINT1           au1pad[1];
} tEcfmNpWrFsMiEcfmTransmitLmr;

typedef struct {
    tEcfmHwLmParams *  pHwLmInfo;
} tEcfmNpWrFsMiEcfmStartLm;

typedef struct {
    tEcfmHwLmParams *  pHwLmInfo;
} tEcfmNpWrFsMiEcfmStopLm;

#ifdef MBSM_WANTED
typedef struct {
    tEcfmHwParams *  pEcfmHwInfo;
    tMbsmSlotInfo *  pSlotInfo;
    UINT1            u1Type;
    UINT1            au1pad[3];
} tEcfmNpWrFsMiEcfmMbsmHwCallNpApi;

typedef struct {
    UINT4            u4ContextId;
    tMbsmSlotInfo *  pSlotInfo;
} tEcfmNpWrFsMiEcfmMbsmNpInitHw;
#endif

#if defined (Y1564_WANTED) || defined (RFC2544_WANTED)
typedef struct {
    tHwTestInfo * pHwTestInfo;
}tEcfmNpWrFsMiEcfmSlaTest;

typedef struct {
    tHwLoopbackInfo *  pHwLoopbackInfo;
} tEcfmNpWrFsMiEcfmAddHwLoopbackInfo;

typedef struct {
    tHwLoopbackInfo *  pHwLoopbackInfo;
} tEcfmNpWrFsMiEcfmDelHwLoopbackInfo;
#endif
typedef struct EcfmNpModInfo {
union {
    tEcfmNpWrFsMiEcfmClearRcvdLbrCounter  sFsMiEcfmClearRcvdLbrCounter;
    tEcfmNpWrFsMiEcfmClearRcvdTstCounter  sFsMiEcfmClearRcvdTstCounter;
    tEcfmNpWrFsMiEcfmGetRcvdLbrCounter  sFsMiEcfmGetRcvdLbrCounter;
    tEcfmNpWrFsMiEcfmGetRcvdTstCounter  sFsMiEcfmGetRcvdTstCounter;
    tEcfmNpWrFsMiEcfmHwCallNpApi  sFsMiEcfmHwCallNpApi;
    tEcfmNpWrFsMiEcfmHwDeInit  sFsMiEcfmHwDeInit;
    tEcfmNpWrFsMiEcfmHwGetCapability  sFsMiEcfmHwGetCapability;
    tEcfmNpWrFsMiEcfmHwGetCcmRxStatistics  sFsMiEcfmHwGetCcmRxStatistics;
    tEcfmNpWrFsMiEcfmHwGetCcmTxStatistics  sFsMiEcfmHwGetCcmTxStatistics;
    tEcfmNpWrFsMiEcfmHwGetPortCcStats  sFsMiEcfmHwGetPortCcStats;
    tEcfmNpWrFsMiEcfmHwHandleIntQFailure  sFsMiEcfmHwHandleIntQFailure;
    tEcfmNpWrFsMiEcfmHwInit  sFsMiEcfmHwInit;
    tEcfmNpWrFsMiEcfmHwRegister  sFsMiEcfmHwRegister;
    tEcfmNpWrFsMiEcfmHwSetVlanEtherType  sFsMiEcfmHwSetVlanEtherType;
    tEcfmNpWrFsMiEcfmStartLbmTransaction  sFsMiEcfmStartLbmTransaction;
    tEcfmNpWrFsMiEcfmStartTstTransaction  sFsMiEcfmStartTstTransaction;
    tEcfmNpWrFsMiEcfmStopLbmTransaction  sFsMiEcfmStopLbmTransaction;
    tEcfmNpWrFsMiEcfmStopTstTransaction  sFsMiEcfmStopTstTransaction;
    tEcfmNpWrFsMiEcfmTransmit1Dm  sFsMiEcfmTransmit1Dm;
    tEcfmNpWrFsMiEcfmTransmitDmm  sFsMiEcfmTransmitDmm;
    tEcfmNpWrFsMiEcfmTransmitDmr  sFsMiEcfmTransmitDmr;
    tEcfmNpWrFsMiEcfmTransmitLmm  sFsMiEcfmTransmitLmm;
    tEcfmNpWrFsMiEcfmTransmitLmr  sFsMiEcfmTransmitLmr;
    tEcfmNpWrFsMiEcfmStartLm  sFsMiEcfmStartLm;
    tEcfmNpWrFsMiEcfmStopLm  sFsMiEcfmStopLm;
#ifdef MBSM_WANTED
    tEcfmNpWrFsMiEcfmMbsmHwCallNpApi  sFsMiEcfmMbsmHwCallNpApi;
    tEcfmNpWrFsMiEcfmMbsmNpInitHw  sFsMiEcfmMbsmNpInitHw;
#endif
#if defined (Y1564_WANTED) || defined (RFC2544_WANTED)
    tEcfmNpWrFsMiEcfmSlaTest  sFsMiEcfmSlaTest;
    tEcfmNpWrFsMiEcfmAddHwLoopbackInfo  sFsMiEcfmAddHwLoopbackInfo;
    tEcfmNpWrFsMiEcfmDelHwLoopbackInfo  sFsMiEcfmDelHwLoopbackInfo;
#endif
    }unOpCode;

#define  EcfmNpFsMiEcfmClearRcvdLbrCounter  unOpCode.sFsMiEcfmClearRcvdLbrCounter;
#define  EcfmNpFsMiEcfmClearRcvdTstCounter  unOpCode.sFsMiEcfmClearRcvdTstCounter;
#define  EcfmNpFsMiEcfmGetRcvdLbrCounter  unOpCode.sFsMiEcfmGetRcvdLbrCounter;
#define  EcfmNpFsMiEcfmGetRcvdTstCounter  unOpCode.sFsMiEcfmGetRcvdTstCounter;
#define  EcfmNpFsMiEcfmHwCallNpApi  unOpCode.sFsMiEcfmHwCallNpApi;
#define  EcfmNpFsMiEcfmHwDeInit  unOpCode.sFsMiEcfmHwDeInit;
#define  EcfmNpFsMiEcfmHwGetCapability  unOpCode.sFsMiEcfmHwGetCapability;
#define  EcfmNpFsMiEcfmHwGetCcmRxStatistics  unOpCode.sFsMiEcfmHwGetCcmRxStatistics;
#define  EcfmNpFsMiEcfmHwGetCcmTxStatistics  unOpCode.sFsMiEcfmHwGetCcmTxStatistics;
#define  EcfmNpFsMiEcfmHwGetPortCcStats  unOpCode.sFsMiEcfmHwGetPortCcStats;
#define  EcfmNpFsMiEcfmHwHandleIntQFailure  unOpCode.sFsMiEcfmHwHandleIntQFailure;
#define  EcfmNpFsMiEcfmHwInit  unOpCode.sFsMiEcfmHwInit;
#define  EcfmNpFsMiEcfmHwRegister  unOpCode.sFsMiEcfmHwRegister;
#define  EcfmNpFsMiEcfmHwSetVlanEtherType  unOpCode.sFsMiEcfmHwSetVlanEtherType;
#define  EcfmNpFsMiEcfmStartLbmTransaction  unOpCode.sFsMiEcfmStartLbmTransaction;
#define  EcfmNpFsMiEcfmStartTstTransaction  unOpCode.sFsMiEcfmStartTstTransaction;
#define  EcfmNpFsMiEcfmStopLbmTransaction  unOpCode.sFsMiEcfmStopLbmTransaction;
#define  EcfmNpFsMiEcfmStopTstTransaction  unOpCode.sFsMiEcfmStopTstTransaction;
#define  EcfmNpFsMiEcfmTransmit1Dm  unOpCode.sFsMiEcfmTransmit1Dm;
#define  EcfmNpFsMiEcfmTransmitDmm  unOpCode.sFsMiEcfmTransmitDmm;
#define  EcfmNpFsMiEcfmTransmitDmr  unOpCode.sFsMiEcfmTransmitDmr;
#define  EcfmNpFsMiEcfmTransmitLmm  unOpCode.sFsMiEcfmTransmitLmm;
#define  EcfmNpFsMiEcfmTransmitLmr  unOpCode.sFsMiEcfmTransmitLmr;
#define  EcfmNpFsMiEcfmStartLm  unOpCode.sFsMiEcfmStartLm;
#define  EcfmNpFsMiEcfmStopLm  unOpCode.sFsMiEcfmStopLm;
#ifdef MBSM_WANTED
#define  EcfmNpFsMiEcfmMbsmHwCallNpApi  unOpCode.sFsMiEcfmMbsmHwCallNpApi;
#define  EcfmNpFsMiEcfmMbsmNpInitHw  unOpCode.sFsMiEcfmMbsmNpInitHw;
#endif
#if defined (Y1564_WANTED) || (RFC2544_WANTED)
#define EcfmNpFsMiEcfmSlaTest unOpCode.sFsMiEcfmSlaTest;
#define  EcfmNpFsMiEcfmAddHwLoopbackInfo  unOpCode.sFsMiEcfmAddHwLoopbackInfo;
#define  EcfmNpFsMiEcfmDelHwLoopbackInfo  unOpCode.sFsMiEcfmDelHwLoopbackInfo;
#endif
} tEcfmNpModInfo;


/* Function Prototype for Generic NP OpCode specific wrappers
 * defined in Ecfmnputil.c */

UINT1 EcfmFsMiEcfmClearRcvdLbrCounter PROTO ((tEcfmMepInfoParams * pEcfmMepInfoParams));
UINT1 EcfmFsMiEcfmClearRcvdTstCounter PROTO ((tEcfmMepInfoParams * pEcfmMepInfoParams));
UINT1 EcfmFsMiEcfmGetRcvdLbrCounter PROTO ((tEcfmMepInfoParams * pEcfmMepInfoParams, UINT4 * pu4LbrIn));
UINT1 EcfmFsMiEcfmGetRcvdTstCounter PROTO ((tEcfmMepInfoParams * pEcfmMepInfoParams, UINT4 * pu4TstIn));
UINT1 EcfmFsMiEcfmHwCallNpApi PROTO ((UINT1 u1Type, tEcfmHwParams * pEcfmHwInfo));
UINT1 EcfmFsMiEcfmHwDeInit PROTO ((UINT4 u4ContextId));
UINT1 EcfmFsMiEcfmHwGetCapability PROTO ((UINT4 u4ContextId, UINT4 * pu4HwCapability));
UINT1 EcfmFsMiEcfmHwGetCcmRxStatistics PROTO ((UINT4 u4ContextId, UINT2 u2RxFilterId, tEcfmCcOffMepRxStats * pEcfmCcOffMepRxStats));
UINT1 EcfmFsMiEcfmHwGetCcmTxStatistics PROTO ((UINT4 u4ContextId, UINT2 u2TxFilterId, tEcfmCcOffMepTxStats * pEcfmCcOffMepTxStats));
UINT1 EcfmFsMiEcfmHwGetPortCcStats PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, tEcfmCcOffPortStats * pEcfmCcOffPortStats));
UINT1 EcfmFsMiEcfmHwHandleIntQFailure PROTO ((UINT4 u4ContextId, tEcfmCcOffRxHandleInfo * pEcfmCcOffRxHandleInfo, UINT2 * pu2RxHandle, BOOL1 * pb1More));
UINT1 EcfmFsMiEcfmHwInit PROTO ((UINT4 u4ContextId));
UINT1 EcfmFsMiEcfmHwRegister PROTO ((UINT4 u4ContextId));
UINT1 EcfmFsMiEcfmHwSetVlanEtherType PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2EtherTypeValue, UINT1 u1EtherType));
UINT1 EcfmFsMiEcfmStartLbmTransaction PROTO ((tEcfmMepInfoParams * pEcfmMepInfoParams, tEcfmConfigLbmInfo * pEcfmConfigLbmInfo));
UINT1 EcfmFsMiEcfmStartTstTransaction PROTO ((tEcfmMepInfoParams * pEcfmMepInfoParams, tEcfmConfigTstInfo * pEcfmConfigTstInfo));
UINT1 EcfmFsMiEcfmStopLbmTransaction PROTO ((tEcfmMepInfoParams * pEcfmMepInfoParams));
UINT1 EcfmFsMiEcfmStopTstTransaction PROTO ((tEcfmMepInfoParams * pEcfmMepInfoParams));
UINT1 EcfmFsMiEcfmTransmit1Dm PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 * pu1DmPdu, UINT2 u2PduLength, tVlanTag VlanTag, UINT1 u1Direction));
UINT1 EcfmFsMiEcfmTransmitDmm PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 * pu1DmmPdu, UINT2 u2PduLength, tVlanTag VlanTag, UINT1 u1Direction));
UINT1 EcfmFsMiEcfmTransmitDmr PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 * pu1DmrPdu, UINT2 u2PduLength, tVlanTag VlanTag, UINT1 u1Direction));
UINT1 EcfmFsMiEcfmTransmitLmm PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 * pu1LmmPdu, UINT2 u2PduLength, tVlanTagInfo VlanTag, UINT1 u1Direction));
UINT1 EcfmFsMiEcfmTransmitLmr PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 * pu1LmrPdu, UINT2 u2PduLength, tVlanTagInfo * pVlanTag, UINT1 u1Direction));
UINT1 EcfmFsMiEcfmStartLm PROTO ((tEcfmHwLmParams * pHwLmInfo));
UINT1 EcfmFsMiEcfmStopLm PROTO ((tEcfmHwLmParams * pHwLmInfo));
#ifdef MBSM_WANTED
UINT1 EcfmFsMiEcfmMbsmHwCallNpApi PROTO ((UINT1 u1Type, tEcfmHwParams * pEcfmHwInfo, tMbsmSlotInfo * pSlotInfo));
UINT1 EcfmFsMiEcfmMbsmNpInitHw PROTO ((UINT4 u4ContextId, tMbsmSlotInfo * pSlotInfo));
#endif
#if defined (Y1564_WANTED) || defined (RFC2544_WANTED)
INT4 EcfmFsMiEcfmSlaTest PROTO ((tHwTestInfo * pHwTestInfo));
UINT1 EcfmFsMiEcfmAddHwLoopbackInfo PROTO ((tHwLoopbackInfo * pHwLoopbackInfo));
UINT1 EcfmFsMiEcfmDelHwLoopbackInfo PROTO ((tHwLoopbackInfo * pHwLoopbackInfo));
#endif

#endif /* __ECFM_NP_WR_H_ */

