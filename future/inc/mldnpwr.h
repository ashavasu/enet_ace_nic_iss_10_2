/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: mldnpwr.h,v 1.1 2014/12/18 12:24:58 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              for MLD wrappers
 *              
 ********************************************************************/
#ifndef _MLDNP_WR_H_
#define _MLDNP_WR_H_

#include "mldnp.h"

#define  FS_MLD_HW_ENABLE_MLD                                   1
#define  FS_MLD_HW_DISABLE_MLD                                  2
#define  FS_MLD_HW_DESTROY_M_L_D_FILTER                         3
#define  FS_MLD_HW_DESTROY_M_L_D_F_P                            4
#define  FS_NP_IPV6_SET_MLD_IFACE_JOIN_RATE                     5
#define  FS_MLD_MBSM_HW_ENABLE_MLD                              6

/* Required arguments list for the mld NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

typedef struct {
    INT4  i4IfIndex;
    INT4  i4RateLimit;
} tMldNpWrFsNpIpv6SetMldIfaceJoinRate;

typedef struct {
    tMbsmSlotInfo *  pSlotInfo;
} tMldNpWrFsMldMbsmHwEnableMld;

typedef struct MldNpModInfo {
union {
    tMldNpWrFsNpIpv6SetMldIfaceJoinRate  sFsNpIpv6SetMldIfaceJoinRate;
    tMldNpWrFsMldMbsmHwEnableMld  sFsMldMbsmHwEnableMld;
    }unOpCode;

#define  MldNpFsNpIpv6SetMldIfaceJoinRate  unOpCode.sFsNpIpv6SetMldIfaceJoinRate;
#define  MldNpFsMldMbsmHwEnableMld  unOpCode.sFsMldMbsmHwEnableMld;
} tMldNpModInfo;


/* Function Prototype for Generic NP OpCode specific wrappers
 * defined in Mldnpapi.c */

UINT1 MldFsMldHwEnableMld PROTO ((VOID));
UINT1 MldFsMldHwDisableMld PROTO ((VOID));
UINT1 MldFsMldHwDestroyMLDFilter PROTO ((VOID));
UINT1 MldFsMldHwDestroyMLDFP PROTO ((VOID));
UINT1 MldFsNpIpv6SetMldIfaceJoinRate PROTO ((INT4 i4IfIndex, INT4 i4RateLimit));
#ifdef MBSM_WANTED
UINT1 MldFsMldMbsmHwEnableMld PROTO ((tMbsmSlotInfo * pSlotInfo));
#endif
#endif
