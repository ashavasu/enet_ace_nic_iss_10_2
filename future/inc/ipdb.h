#ifndef _IPDB_H
#define _IPDB_H
/*****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2007                                               */
/* $Id: ipdb.h,v 1.5 2010/07/31 10:53:47 prabuc Exp $                                                                    */
/*****************************************************************************/
/*    FILE  NAME            : ipdb.h                                         */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : IP Binding Management                          */
/*    MODULE NAME           : IP binding global Structures and definitions   */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains Data Structures and         */
/*                            definitions needed by other modules for        */
/*                            interfacing with IP binding management module  */
/*---------------------------------------------------------------------------*/

/* Gateway/AR entries; */
typedef struct _IpDbGateway
{
    UINT4            u4Network;      /* The network for which the gateway
                                        is given */
    UINT4            u4NetMask;      /* The mask for the network to which
                                        the gateway is given */
    UINT4            u4GatewayIp;    /* Gateway Ip */
} tIpDbGateway;

typedef struct _IpDbEntry
{
    tMacAddr         HostMac;       /* Mac address of the host */
    tVlanId          VlanId;        /* VLAN to which the host belongs */
    UINT4            u4HostIp;      /* IP address assigned to the host */ 
    UINT4            u4InIfIndex;   /* The Port through which the host is
                                       connected */
    UINT4            u4BindingId;   /* An integer value identifying the
                                       binding */
    UINT4            u4LeaseTime;   /* Lease time of the binding */
    UINT4            u4ContextId;   /* Context Identifier */
    UINT1            u1BindingType; /* Binding type (static or DHCP or PPP) */
    UINT1            au1Reserved [3];
} tIpDbEntry;


#define IPDB_CREATE_ENTRY           1
#define IPDB_DELETE_ENTRY           2

/* Trace Related definitions */
#define IPDB_FN_ENTRY              (0x1 << 1)
#define IPDB_FN_EXIT               (0x1 << 2)
#define IPDB_DBG_TRC               (0x1 << 3)
#define IPDB_FAIL_TRC              (0x1 << 4)
#define IPDB_FN_ARGS               (0x1 << 5)

#define IPDB_ALL_TRC               (IPDB_FN_ENTRY | IPDB_FN_EXIT | \
                                    IPDB_FAIL_TRC | IPDB_DBG_TRC | IPDB_FN_ARGS)

/* different Binding types */
#define IPDB_STATIC_BINDING         1
#define IPDB_DHCP_BINDING           2
#define IPDB_PPP_BINDING            3
#define IPDB_ALL_BINDING            4

#define IPDB_SUCCESS                0
#define IPDB_FAILURE                1 
#define IPDB_NO_ENTRY               2

/* ENABLE/DISABLE */
#define IPDB_ENABLED                1
#define IPDB_DISABLED               2

#define IPDB_ARP_ALLOW              1
#define IPDB_ARP_DROP               2

#define IPDB_IPSG_DISABLE           1 
#define IPDB_IPSG_IP_MODE           2
#define IPDB_IPSG_IP_MAC_MODE       3

PUBLIC VOID IPDBMain                      PROTO ((INT1 *pi1Param));

PUBLIC INT4 IpdbApiUpdateBindingEntry     PROTO ((tIpDbEntry * pIpDbEntry, 
                                                  UINT4 u4NoOfGWEntries,
                                                  tIpDbGateway * pIpDbGateway, 
                                                  UINT1 u1Status));

PUBLIC INT4 IpdbApiDeleteBindingEntries   PROTO ((UINT1 u1BindingType, 
                                                  UINT4 u4ContextID, 
                                                  tVlanId VlanId));

PUBLIC VOID IpdbPortVlanDelete            PROTO ((UINT4 u4ContextID, 
                                                  tVlanId VlanId));

PUBLIC VOID IpdbEnqueueArpPkt             PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf, 
                                                  UINT4 u4InPort, 
                                                  tVlanTag VlanTag));

PUBLIC INT4 IpdbCreatePort                PROTO ((UINT4 u4IfIndex));

PUBLIC INT4 IpdbDeletePort                PROTO ((UINT4 u4IfIndex));
 
PUBLIC INT4 IpdbApiProcessPktWithPortList PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf, 
                                                  UINT4 u4ContextId, 
                                                  tVlanTag VlanTag, 
                                                  UINT4 u4InPort, 
                                                  tMacAddr DstMacAddr, 
                                                  tMacAddr SrcMacAddr, 
                                                  tPortList PortList));

PUBLIC INT4 IpdbApiDoIpSrcGuardValidation PROTO ((UINT4 u4ContextId, 
                                                  tVlanId VlanId, 
                                                  tMacAddr HostMac, 
                                                  UINT4 u4HostIp, 
                                                  UINT4 u4InPort));
#endif
