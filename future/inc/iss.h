/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: iss.h,v 1.329 2017/12/28 10:40:15 siva Exp $
 *
 * Description:This file contains the exported definitions and  
 *             macros of ISS
 *
 *******************************************************************/
#ifndef _ISS_H
#define _ISS_H

#include "size.h"
#include "mbsm.h"
#include "cfa.h"
#include "cust.h"
#include "redblack.h"
#include "qosxtd.h"
#include "ipv6.h"
#include "utilipvx.h"

#define ISS_GET_STACKING_MODEL() \
          IssGetStackingModel()
#define ISS_LOCK()     IssLock ()
#define ISS_UNLOCK()   IssUnLock ()

#define ISS_ALL_STP_INSTANCES   65535
#define ISFWDSLASH(ch) \
        (ch == '/')

/* Macros used in Web and system Modules*/
#define ISS_UP                  1
#define ISS_DOWN                2
#define ISS_MAC_LEN             6
#define ISS_MEM_DUMP_DEF_LEN    4

#define ISS_UDB_MAX_OFFSET              128
#ifndef ISS_FILTER_SHADOW_MEM_SIZE
#define ISS_FILTER_SHADOW_MEM_SIZE 4  /* Filter Shadow Size */
#endif

/*Macros related to Http */
#define ISS_CTRL_HTTP_STATUS 1
#define ISS_CTRL_PORT                   7000            /*Control port used for ISS Control messages socket*/
#define ISS_INADDR_LOOPBACK           0x7f000001

/* Macro related to MSR and Show running config Indication */

#define ISS_MAX_PROTOCOLS  5

#define ISS_TELNET_CLIENT_DISABLE    0
#define ISS_TELNET_CLIENT_ENABLE     1

#define ISS_ACTIVE_LOG  1
#define ISS_STANDBY_LOG 2

#define ISS_STANDBY_LOG_FILE            "fsir.peer"

#define ISS_SSH_CLIENT_DISABLE       0
#define ISS_SSH_CLIENT_ENABLE        1

#define ISS_CONFIG_FILE_LEN          128
#define ISS_PATH_LEN                 128

#define GR_CSR_IN_PROGRESS  1
#define GR_CSR_COMPLETED    2

#define ISS_CONFIG_FILE_NAME_LEN      128

#define STORE_MAX_SIZE_TABLE        12000

#define CRU_BUF_NAME_LEN 12

/* Definition of NPAPI Log Modules */
enum {
    CLI_ISS_NP_MOD_CFA = 1,
    CLI_ISS_NP_MOD_VLAN,
    CLI_ISS_NP_MOD_BFD,
    CLI_ISS_NP_MOD_BCMX,
    CLI_ISS_NP_MOD_BRG,
    CLI_ISS_NP_MOD_ECFM,
    CLI_ISS_NP_MOD_ELPS,
    CLI_ISS_NP_MOD_ERPS,
    CLI_ISS_NP_MOD_MPLS,
    CLI_ISS_NP_MOD_MAU,
    CLI_ISS_NP_MOD_IP,
    CLI_ISS_NP_MOD_IP6,
    CLI_ISS_NP_MOD_EOAM,
    CLI_ISS_NP_MOD_ETHER,
    CLI_ISS_NP_MOD_DIFFSERV,
    CLI_ISS_NP_MOD_DSMON,
    CLI_ISS_NP_MOD_FMN,
    CLI_ISS_NP_MOD_PVRST,
    CLI_ISS_NP_MOD_QOS,
    CLI_ISS_NP_MOD_ISS,
    CLI_ISS_NP_MOD_LA,
    CLI_ISS_NP_MOD_MLDS,
    CLI_ISS_NP_MOD_RMON,
    CLI_ISS_NP_MOD_SRCMV,
    CLI_ISS_NP_MOD_PNAC,
    CLI_ISS_NP_MOD_IGMP,
    CLI_ISS_NP_MOD_MBS,
    CLI_ISS_NP_MOD_MRP,
    CLI_ISS_NP_MOD_MSTP,
    CLI_ISS_NP_MOD_NP,
    CLI_ISS_NP_MOD_OFC,
    CLI_ISS_NP_MOD_PBB,
    CLI_ISS_NP_MOD_PTP,
    CLI_ISS_NP_MOD_RSTP,
    CLI_ISS_NP_MOD_RPORT,
    CLI_ISS_NP_MOD_RED,
    CLI_ISS_NP_MOD_SYNCE,
    CLI_ISS_NP_MOD_TAC,
    CLI_ISS_NP_MOD_VCM,
    CLI_ISS_NP_MOD_POE,
    CLI_ISS_NP_MOD_PPP,
    CLI_ISS_NP_MOD_MLD,
    CLI_ISS_NP_MOD_RM,
    CLI_ISS_NP_MOD_RBR,
    CLI_ISS_NP_MOD_CPSS,
    CLI_ISS_NP_MOD_FSD,
    CLI_ISS_NP_MOD_LION,
    CLI_ISS_NP_MOD_MAX
};

#ifdef _NPUTILS_C

CONST CHR1  *NpDebugModule[] = {
    NULL,
    "\r\n NP debugging is on for CFA module ",
    "\r\n NP debugging is on for VLAN module ",
    "\r\n NP debugging is on for BFD module ",
    "\r\n NP debugging is on for BCMX module ",
    "\r\n NP debugging is on for BRG module ",
    "\r\n NP debugging is on for ECFM module ",
    "\r\n NP debugging is on for ELPS module ",
    "\r\n NP debugging is on for ERPS module ",
    "\r\n NP debugging is on for MPLS module ",
    "\r\n NP debugging is on for MAU module ",
    "\r\n NP debugging is on for IP module ",
    "\r\n NP debugging is on for IP6 module ",
    "\r\n NP debugging is on for EOAM module ",
    "\r\n NP debugging is on for ETHER module ",
    "\r\n NP debugging is on for DIFFSERV module ",
    "\r\n NP debugging is on for DSMON module ",
    "\r\n NP debugging is on for FMN module ",
    "\r\n NP debugging is on for PVRST module ",
    "\r\n NP debugging is on for QOS module ",
    "\r\n NP debugging is on for ISS module ",
    "\r\n NP debugging is on for LA module ",
    "\r\n NP debugging is on for MLDS module ",
    "\r\n NP debugging is on for RMON module ",
    "\r\n NP debugging is on for SRCMV module ",
    "\r\n NP debugging is on for PNAC module ",
    "\r\n NP debugging is on for IGMP module ",
    "\r\n NP debugging is on for MBS module ",
    "\r\n NP debugging is on for MRP module ",
    "\r\n NP debugging is on for MSTP module ",
    "\r\n NP debugging is on for NP module ",
    "\r\n NP debugging is on for OFC module ",
    "\r\n NP debugging is on for PBB module ",
    "\r\n NP debugging is on for PTP module ",
    "\r\n NP debugging is on for RSTP module ",
    "\r\n NP debugging is on for RPORT module ",
    "\r\n NP debugging is on for RED module ",
    "\r\n NP debugging is on for SYNCE module ",
    "\r\n NP debugging is on for TAC module ",
    "\r\n NP debugging is on for VCM module ",
    "\r\n NP debugging is on for POE module ",
    "\r\n NP debugging is on for PPP module ",
    "\r\n NP debugging is on for MLD module ",
    "\r\n NP debugging is on for RM module ",
    "\r\n NP debugging is on for RBR module ",
    "\r\n NP debugging is on for CPSS module ",
    "\r\n NP debugging is on for FSD module ",
    "\r\n NP debugging is on for LION module ",
    "\r\n"
};

#else

extern CONST CHR1  *NpDebugModule[];

#endif


/* Definition of NPAPI Log level (0-8) with 8 being the higest level and 0 being off level */

#define CLI_ISS_NP_LOG_DEBUG_LEVEL         8        /* Used for logging debug messages      */

#define CLI_ISS_NP_LOG_INFO_LEVEL          7        /* Used for logging informational
                                                     *  messages
                                                     *                                       */

#define CLI_ISS_NP_LOG_NOTICE_LEVEL        6        /* Used for logging messages that
                                                     *  require attention but are not errors
                                                     *                                       */

#define CLI_ISS_NP_LOG_WARN_LEVEL          5        /* Used for logging warning messages    */

#define CLI_ISS_NP_LOG_ERROR_LEVEL         4        /* Used for error messages              */

#define CLI_ISS_NP_LOG_CRITICAL_LEVEL      3        /* Used for logging critical errors     */

#define CLI_ISS_NP_LOG_ALERT_LEVEL         2        /* Used for logging messages that
                                                     *  require immediate attention.
                                                     *                                       */

#define CLI_ISS_NP_LOG_EMERG_LEVEL         1        /* Used for logging messages that are
                                                     *  equivalent to a panic condition.
                                                     *                                       */
#define CLI_ISS_NP_LOG_OFF_LEVEL           0        /* Used for turning the logging 
                                                     *  messages off                         */



/*following enum contains the message type macros which are
 *passed to api calls for corresponding action
 */
enum
{
     ISS_NP_GET_DEFAULT_SPEED=1,      /*used for getting the maximum port speed */
     ISS_NP_GET_PORT_INFO=2
};

/* System Capabilities */
/* System Capabilities are stored as a BITLIST, and the 
 * following enum show the bit position of a system
 * capability in that bitlist. This is based on textual
 * convension BITS */
enum
{
    ISS_SYS_CAPAB_OTHER          = 1, /* 0th Bit */
    ISS_SYS_CAPAB_REPEATER       = 2, /* 1st Bit */
    ISS_SYS_CAPAB_BRIDGE         = 3, /* 2nd Bit */
    ISS_SYS_CAPAB_WLAN_AP        = 4, /* 3rd Bit */
    ISS_SYS_CAPAB_ROUTER         = 5, /* 4th Bit */
    ISS_SYS_CAPAB_TELEPHONE      = 6, /* 5th Bit */
    ISS_SYS_CAPAB_DOCSIS_CABLE   = 7, /* 6th Bit */
    ISS_SYS_CAPAB_STATION_ONLY   = 8  /* 7th Bit */
};

typedef enum {
    ISS_USERDEFINED_ACCESSFILTER_ADD  = 1, /* Macro to Add UDB Filter */
    ISS_USERDEFINED_L2FILTER_APPLY_AND_OP, /* Macro to AND two L2 Filter */
    ISS_USERDEFINED_L2FILTER_APPLY_OR_OP, /* Macro to OR Two L2 Filter */
    ISS_USERDEFINED_L2FILTER_APPLY_NOT_OP, /* Macro to NOT on  L2 Filter */
    ISS_USERDEFINED_L3FILTER_APPLY_AND_OP, /* Macro to AND two L3 Filter */
    ISS_USERDEFINED_L3FILTER_APPLY_OR_OP, /* Macro to OR  two L3 Filter */
    ISS_USERDEFINED_L3FILTER_APPLY_NOT_OP,/* Macro to NOT  L3 Filter */
    ISS_USERDEFINED_L2L3FILTER_APPLY_AND_OP, /* Macro to AND L2L3 Filter */
    ISS_USERDEFINED_L2L3FILTER_APPLY_OR_OP, /* Macro to OR L2L3 Filter */
    ISS_USERDEFINED_FILTER_MODIFY, /* Macro to Modify UDB Filter */
    ISS_USERDEFINED_ACCESSFILTER_DELETE,  /* Macro to Delete UDB Filter */
    ISS_USERDEFINED_L2L2FILTER_DELETE, /* Macro to Delete L2L2 Filter  */
    ISS_USERDEFINED_L3L3FILTER_DELETE, /* Macro to Delete L3L3 Filter */
    ISS_USERDEFINED_L2L3FILTER_DELETE, /* Macro to Delete L2L3 Filter */
    ISS_USERDEFINED_L2L3FILTER_STAT_ENABLE, /* Macro to Enable L2L3 Hw Stats */
    ISS_USERDEFINED_L2L3FILTER_STAT_DISABLE, /* Macro to Disable L2L3 Hw Stats */
    ISS_USERDEFINED_L2L3FILTER_STAT_GET, /* Macro to Get L2L3 Hw Stats */
    ISS_USERDEFINED_L2L3FILTER_STAT_CLEAR /* Macro to Clear L2L3 Hw Stats */
}tIssUserDefFilterTypeOperation;

/* Below Enum represents the Stacking Model */
/* Global variable (gu4StackingModel)  defined in lrmain.c is used to
 * identify the Stacking Model. By default it is ISS_STACKING_MODEL_NONE */
enum
{
   ISS_STACKING_MODEL_NONE        = 1,  /* Default model in Single Slot Switch */
   ISS_CTRL_PLANE_STACKING_MODEL,       /* Stack task of Hardware will not be used */  
   ISS_DATA_PLANE_STACKING_MODEL,       /* Stack task of Hardware will  be used */
   ISS_DISS_STACKING_MODEL              /* stack task of Hardware will not be used, and used for DISS */
                                   
};

/* Macros used in Web and system Modules*/
#define ISS_UP                  1
#define ISS_DOWN                2
#define ISS_MAC_LEN             6
#define ISS_MAX_CMD_LEN         255

#define ISS_MAX_CIDR            32
#define ISS_MAX_PORTS           SYS_DEF_MAX_PHYSICAL_INTERFACES
#define ISS_MIN_VLAN_ID         VLAN_DEV_MIN_VLAN_ID
#define ISS_MAX_VLAN_ID         VLAN_DEV_MAX_VLAN_ID
#define ISS_MAX_CONTEXTS        SYS_DEF_MAX_NUM_CONTEXTS

#define ISS_SUCCESS                               1
#define ISS_FAILURE                               0
#define ISS_SYS_INFO_LEN                         50
#define ISS_SHOW_SYS_INFO_MAX_LEN                50
#define ISS_STR_LEN                              ISS_SHOW_SYS_INFO_MAX_LEN
#define ISS_ISSU_MAX_PATH_LEN                    128
#define ISS_ISSU_IMAGE_NAME_MAX_SIZE             32  
#define ISS_NPAPI_MODE_LEN                       15
#define ISS_RM_INTERFACE_LEN                     8
#define ISS_RESTORE_VERSION_LEN                  12
#define ISS_RESTORE_FORMAT_LEN                   12
#define ISS_SYS_CAPABILITIES_LEN                 2
#define ISS_MIN_PORTS                            1
#define ISS_MAX_DATE_LEN                         40 
#define ISS_SNMP_MASK                            0x01
#define ISS_TELNET_MASK                          0x02
#define ISS_HTTP_MASK                            0x04   
#define ISS_HTTPS_MASK                           0x08
#define ISS_SSH_MASK                             0x10
#define ISS_ALL_MASK                             0x1f
#define ISS_DEF_HTTP_PORT                        80
#define ISS_HTTP_STATUS_ENABLED                  1
#define ISS_HTTP_STATUS_DISABLED                 2
#define ISS_FLASH_INIT                           1
#define ISS_MAX_AUTH_MGR_PROTOCOLS               5

#define ISS_LOGIN_PROMPT_ENABLE         1
#define ISS_LOGIN_PROMPT_DISABLE        2

#define ISS_DEF_VAL_SAVE_ENABLED        1
#define ISS_DEF_VAL_SAVE_DISABLED       2

#define ISS_NPAPI_MODE_SYNCHRONOUS      1
#define ISS_NPAPI_MODE_ASYNCHRONOUS     2

#define ISS_INCR_SAVE_ENABLED           1
#define ISS_INCR_SAVE_DISABLED          2

#define ISS_AUTO_SAVE_ENABLED           1
#define ISS_AUTO_SAVE_DISABLED          2

#define ISS_MSR_RESTORE                 1
#define ISS_CSR_RESTORE                 2

#define ISS_AUTO_PORT_CREATE_ENABLED    1
#define ISS_AUTO_PORT_CREATE_DISABLED   2
#define ISS_ROLLBACK_DISABLED 1
#define ISS_ROLLBACK_ENABLED 2

#define ISS_COLDSTDBY_ENABLE            ISS_RM_RTYPE_COLD
#define ISS_COLDSTDBY_DISABLE           ISS_RM_RTYPE_HOT
#define ISS_MAX_POSSIBLE_STK_PORTS      4
#define ISS_DEFAULT_STACK_PORT_COUNT    0
#define ISS_DEFAULT_STACK_COLD_STANDBY  ISS_COLDSTDBY_DISABLE
#define ISS_MAX_STK_PORTS               IssGetStackPortCountFromNvRam()
#define ISS_DEFAULT_RM_IP_ADDRESS       ISS_CUST_SYS_DEF_RM_IP_ADDRESS
#define ISS_DEFAULT_RM_SUBNET_MASK      ISS_CUST_SYS_DEF_RM_SUBNET_MASK
#define ISS_DEFAULT_RM_STK_INTERFACE    ISS_CUST_SYS_DEF_RM_STK_INTERFACE

#define ISS_SOFTWARE_TIME_STAMP         1
#define ISS_HARDWARE_TIME_STAMP         2
#define ISS_HARDWARE_TRANS_TIME_STAMP   3
#define ISS_DEFAULT_TIME_STAMP_METHOD   ISS_SOFTWARE_TIME_STAMP

#define ISS_MIN_VALUE_MIN_THRESHOLD     (-15)
#define ISS_MAX_VALUE_MIN_THRESHOLD     30
#define ISS_MIN_VALUE_MAX_THRESHOLD     35
#define ISS_MAX_VALUE_MAX_THRESHOLD     40
#define ISS_DEFAULT_MIN_THRESHOLD       10
#define ISS_DEFAULT_MAX_THRESHOLD       ISS_MAX_VALUE_MAX_THRESHOLD 
#define ISS_SWITCH_MAX_CPU_THRESHOLD    100
#define ISS_SWITCH_MIN_CPU_THRESHOLD    1
#define ISS_SWITCH_MAX_POWER_SUPPLY     230
#define ISS_SWITCH_MIN_POWER_SUPPLY     100
#define ISS_SWITCH_MAX_RAM_USAGE        100
#define ISS_SWITCH_MIN_RAM_USAGE        1
#define ISS_SWITCH_MAX_FLASH_USAGE      100
#define ISS_SWITCH_MIN_FLASH_USAGE      1
#define ISS_SWITCH_MAX_FAN              5
#define ISS_FAN_UP                      1
#define ISS_FAN_DOWN                    0
#define ISS_VRF_UNQ_MAC_ENABLE          1
#define ISS_VRF_UNQ_MAC_DISABLE         0 

/* HITLESS RESTART */
#define ISS_HITLESS_RESTART_DISABLE     0
#define ISS_HITLESS_RESTART_ENABLE      1

#define ISS_LOGIN_ATTEMPT_DEFVAL       3
#define ISS_MIN_CLI_LOGIN_ATTEMPTS     1
#define ISS_MAX_CLI_LOGIN_ATTEMPTS     10
#define ISS_MIN_CLI_LOCK_OUT_TIME      30
#define ISS_MAX_CLI_LOCK_OUT_TIME      600

/* This defaults are just used for Linux compilation package alone
 * since no h/w api's can be queried to get correct values
 */ 
#define ISS_SWITCH_DEFAULT_TEMPERATURE  ISS_MAX_VALUE_MAX_THRESHOLD
#define ISS_SWITCH_DEFAULT_POWER_SUPPLY ISS_SWITCH_MAX_POWER_SUPPLY

INT4 IssUpdateDownLoadStatus (UINT4);
INT4 IssGetDownLoadStatus (VOID);
INT4 IssGetTransferMode (VOID);

#define ISS_TFTP_TRANSFER_MODE 1
#define ISS_SFTP_TRANSFER_MODE 2

VOID IssCustSelectPack PROTO ((UINT1* pu1ImageName,UINT1* pu1RemoteFile));
INT4 IssGetAuditStatus          PROTO (( VOID ));
#define ISS_IS_AUDIT_ENABLED    IssGetAuditStatus
#define AUDIT_MIN_FILE_SIZE     1024 /* 1 KB */
#define AUDIT_MAX_FILE_SIZE     (1024 * 1024)/* 1 MB */
#define AUDIT_DEFAULT_FILE      FLASH "config.txt"
#define LOG_MIN_FILE_SIZE       1024 /* 1 KB */
#define LOG_MAX_FILE_SIZE       (1024 * 1024)/* 1 MB */

#define AUDIT_MIN_THRESHOLD_VALUE  1       /* 1% */
#define AUDIT_MAX_THRESHOLD_VALUE  99      /* 99%*/
#define AUDIT_DEFAULT_THRESHOLD_VALUE 70     /* 70%*/
#define LOG_MIN_THRESHOLD_VALUE  1       /* 1% */
#define LOG_MAX_THRESHOLD_VALUE  99      /* 99%*/
#define LOG_DEFAULT_THRESHOLD_VALUE 70     /* 70%*/

#define LOG_FILE_NAME_LENGTH    (256)

#define LOG_FILE_STORAGE               1
#define LOG_DIR_STORAGE                2

#define ISS_ONE_KILO_BYTE               1024

/* Macros for Mirror Operations */
#define ISS_MIRR_MAX_SESSIONS           20
#define ISS_MAX_MIRR_DEST_PORTS         100

#define ISS_MIRR_MAX_DEST_RECORD        ISS_MAX_MIRR_DEST_PORTS
#define ISS_MIRR_MAX_SRC_RECORD         100
#define ISS_MIRR_HW_DEST_RECD           ISS_MAX_MIRR_DEST_PORTS
#define ISS_MIRR_HW_SRC_RECD            20

#define ISS_INTF_BREAKOUT_BASE_IDX 4

/* Event used by "ROOT" task to spawn MSR task only after
 * all protocols/modules completed it's initializations.
 * Currently spawning MSR task after CFA handles IP_UP_EVENT.
 */
#define ISS_SPAWN_MSR_TASK                       1
#define ISS_SPAWN_MSR_RM_TASK                    2

/* maximum mac addresses that can be learnt over a port */
#define ISS_MAXMAC_PER_PORT                      100

/* Size of buffer used for logging the trace messages */
#define ISS_LOG_BUFFER_SIZE  (UTL_MAX_LOGS * UTL_MAX_LOG_LEN) 

/* Interface alias by default kept as "Slot", only in case of
 * linux where we actually use this alias to open the device
 * This alias to phyInterfaceName  mapping maintained (gddapi.c).
 *
 * For OOB port(s) , the actual phyinterface name is read
 * from NVRAM.
 */

#define ISS_ALIAS_PREFIX       ((const char *)"Slot")
#define OOB_ALIAS_PREFIX       ((const char *)"cpu")
#define ISS_RPORT_ALIAS_PREFIX ((const char *)"rport")


#define OOB_ALIAS_NAME         ((const char *)"cpu0")

/* Configuration  Restore options */
#define ISS_RESTORE_LOCAL                         2
#define ISS_RESTORE_REMOTE                        3

/* flash data types */
#define FLASH_ALL_CONFIG                          1
#define FLASH_STARTUP_CONFIG                      2
#define FLASH_FIRMWARE                            3

#if defined(FLASH_FS_WANTED) && defined(OS_VXWORKS)
#define FLASH                 "flash:/"
#else
#if defined(SWC)
#define FLASH                 "/usr/local/etc/"
#else
#if defined(DX260)
#define FLASH                 "/mnt/fs/"
#else
#if defined(DX285)
#define FLASH                 "/mnt/flash/"
#else
#if defined(LION)
#define FLASH                 "/flash/"
#else
#define FLASH                 FLASH_CUST
#endif
#endif
#endif
#endif
#endif

#define ISS_LOG_DIRECTORY               ISS_CUST_LOG_DIRECTORY
#define ISS_FIRMWARE_NORMAL "normal"
#define ISS_FIRMWARE_FALLBACK "fallback"
#define ISS_L4SFILTER_ADD  5
#define ISS_L4SFILTER_DELETE  6
#define ISS_L4SFILTER_LIST              gIssGlobalInfo.IssL4SFilterListHead
#define ISS_L4SFILTER_LIST_HEAD         (&ISS_L4SFILTER_LIST)->Tail

/* Maximum Image size that can be downloaded */ 
#define ISS_IMAGE_SIZE                     0x400000

/* Specifies the path where the opensource htmlpages exists */
#if defined(DX285)
#define ISS_DEF_OPENSRC_HTML_PATH       FLASH
#else
#define ISS_DEF_OPENSRC_HTML_PATH       ((const char *) ".")
#endif

#define ISS_ERR_BUF_LEN       256

/* Port List size */
#define ISS_PORT_LIST_SIZE           ((ISS_MAX_PORTS + 31)/32 * 4)


#define ISS_PORT_CHANNEL_LIST_SIZE      ((ISS_MAX_PORTS + LA_MAX_AGG_INTF + 31)/32 * 4)
#define ISS_AUTH_PORT_LIST_SIZE      ((ISS_MAX_PORTS + LA_MAX_AGG_INTF + 31)/32 * 4)

#define ISS_VLAN_LIST_SIZE           ((ISS_MAX_VLAN_ID + 31)/32 * 4)

#define ISS_STATIC_MAX_NAME_LEN         32
#define ISS_IP_DEF_NET_MASK             0xffffffff

#define ISS_L2FILTER_ADD                1
#define ISS_L3FILTER_ADD                3
#define ISS_L2FILTER_DELETE             2
#define ISS_L3FILTER_DELETE             4
#define ISS_L2FILTER_MODIFY             5
#define ISS_L3FILTER_MODIFY             6
#define ISS_L2FILTER_STAT_ENABLE        7
#define ISS_L2FILTER_STAT_DISABLE       8
#define ISS_L2FILTER_STAT_GET           9
#define ISS_L2FILTER_STAT_CLEAR         10
/* The following two flags added for S-Channel interface filters */
#define ISS_ACL_FILTER_ENABLE           11
#define ISS_ACL_FILTER_DISABLE          12
#define ISS_L3FILTER_STAT_ENABLE        ISS_L2FILTER_STAT_ENABLE
#define ISS_L3FILTER_STAT_DISABLE       ISS_L2FILTER_STAT_DISABLE
#define ISS_L3FILTER_STAT_GET           ISS_L2FILTER_STAT_GET
#define ISS_L3FILTER_STAT_CLEAR         ISS_L2FILTER_STAT_CLEAR
#define ISS_MAX_UDB_INDEX               6
#define ISS_UDB_MAX_OFFSET              128
#define ISS_UDB_BYTE_SET                0xff
#define ISS_L2FILTER_SRCMAC_DENY        7


/* Macros of Reserve frame Actions */
#define ISS_RESERV_FRAME_ADD            1
#define ISS_RESERV_FRAME_DELETE         2
#define ISS_RESERV_FRAME_MODIFY    3

/* Reserve frame PDUs */
enum {
        ISS_RESERVED_FRAME_BPDU = 1,
        ISS_RESERVED_FRAME_LACPDU,
        ISS_RESERVED_FRAME_EAP,
        ISS_RESERVED_FRAME_LLDPDU,
        ISS_RESERVED_FRAME_OTHER,
        ISS_RESERVED_FRAME_ALL,
};

/*Macros for reserved frame Minimum and Maximum control ID values */
#define ISS_MIN_RESERV_FRM_CTRL_ID 0
#define ISS_MAX_RESERV_FRM_CTRL_ID 32

#define ISS_RESERV_FRM_MAX_GROUPS 2

#define FF_VALUE 0xFF
#define FE_VALUE 0xFE
#define FC_VALUE 0xFC
#define F8_VALUE 0xF8
#define F0_VALUE 0xF0
#define E0_VALUE 0xE0

#define ISS_INCR                        1
#define ISS_DECR                        2

#define ISS_ACTION_INVALID              -1

/* Packet type which the filter will be applied on */
#define ISS_FILTER_SINGLE_TAG           1
#define ISS_FILTER_MAX_PRIO             255
#define ISS_FILTER_DOUBLE_TAG           2
#define QOS_IPV4                        1
#define QOS_IPV6                        2

#define ACL_PRINT_ANY                "ANY"

#define ISS_ZERO_ENTRY                  0
#define ISS_DEF_PREFIX_LEN              128
#define ISS_DSCP_INVALID                -1
#define ISS_MIN_PORT_VALUE              0
#define ISS_MAX_PORT_VALUE              65535
#define ISS_L3_MIN_PROTOCOL             0
#define ISS_L3_MAX_PROTOCOL             255
#define ISS_MIN_ETHER_TYPE_VAL          0
#define ISS_MAX_ETHER_TYPE_VAL          65535
#define ISS_MIN_MSG_TYPE                0
#define ISS_MAX_MSG_TYPE                65535
#define ISS_MIN_MSG_CODE                0
#define ISS_MAX_MSG_CODE                65535
#define ISS_DEFAULT_MSG_TYPE            -1
#define ISS_DEFAULT_MSG_CODE            -1
#define ISS_IP_PROTO_DEF  -1
#define ISS_FLOWID_INVALID              16777215       /* 0x00ffffff*/
#define ISS_DEF_SVLAN_PRIO              -1
#define ISS_DEF_CVLAN_PRIO              -1


#define ISS_DEFAULT_FILTER_PRIORITY     1
#define ISS_ANY          255     
#define ISS_SYSTEM_ANY          0
#define ISS_LAST_BYTE_IN_PORTLIST(au1PortArray, u4Length, u4Size) \
        {                                                          \
            UINT2               u2NoOfBits;\
            u2NoOfBits = (UINT2) (u4Length % BITS_PER_BYTE);    \
            if (u2NoOfBits == 0)                                          \
            {                                                             \
                au1PortArray[u4Size - 1] = (UINT1) 0xff;                  \
            }                                                             \
            else                                                          \
            {                                                             \
                au1PortArray[u4Size - 1] =                                \
                (UINT1) (0xff << (BITS_PER_BYTE - u2NoOfBits));\
            }\
        }

#define ISS_IS_MEMBER_OF_PORTLIST(au1PortList, u2InParam, u1Result) \
         {\
            UINT2 u2BytePos = 0;\
            UINT2 u2BitPos  = 0;\
            u2BytePos = (UINT2)(u2InParam / BITS_PER_BYTE);\
            u2BitPos  = (UINT2)(u2InParam % BITS_PER_BYTE);\
            if (u2BitPos  == 0)\
            {\
                u2BytePos = (UINT2)(u2BytePos - 1);\
            }\
            if ((au1PortList[u2BytePos] \
                 & gau1BitMaskMap[u2BitPos]) != 0)\
            {\
                u1Result = ISS_TRUE;\
            }\
            else \
            {\
                u1Result = ISS_FALSE; \
            }\
         }
        
#define ISS_SET_MEMBER_PORT(au1PortArray, u2Port) \
           {\
              UINT2 u2PortBytePos = 0;\
              UINT2 u2PortBitPos = 0;\
              u2PortBytePos = (UINT2)(u2Port / BITS_PER_BYTE);\
              u2PortBitPos  = (UINT2)(u2Port % BITS_PER_BYTE);\
          if (u2PortBitPos  == 0) {u2PortBytePos = (UINT2)(u2PortBytePos - 1);} \
           \
              au1PortArray[u2PortBytePos] = (UINT1)(au1PortArray[u2PortBytePos]\
                             | gau1BitMaskMap[u2PortBitPos]);\
           }

#define   ISS_IS_ADDR_CLASS_A(u4Addr)\
          ((u4Addr != 0 ) && ((u4Addr & 0x80000000) == 0))
#define   ISS_IS_ADDR_CLASS_B(u4Addr)   ((u4Addr & 0xc0000000) == 0x80000000)
#define   ISS_IS_ADDR_CLASS_C(u4Addr)   ((u4Addr & 0xe0000000) == 0xc0000000)
#define   ISS_IS_ADDR_CLASS_D(u4Addr)   ((u4Addr & 0xf0000000) == 0xe0000000) 
#define   ISS_IS_ADDR_CLASS_E(u4Addr)   ((u4Addr & 0xf8000000) == 0xc0000000) 

#define   ISS_IS_ADDR_VALID(u4Addr)\
   (((u4Addr & 0x000000ff) != 0) && ((u4Addr & 0x000000ff) != 255))

#ifdef RM_WANTED
#define   ISS_IS_NP_PROGRAMMING_ALLOWED() \
          ((L2RED_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE) ? \
            ISS_TRUE: ISS_FALSE)
#else
#define   ISS_IS_NP_PROGRAMMING_ALLOWED() ISS_TRUE
#endif

/* RowStatus values */
#define ISS_ACTIVE                      ACTIVE
#define ISS_NOT_IN_SERVICE              NOT_IN_SERVICE 
#define ISS_NOT_READY                   NOT_READY
#define ISS_CREATE_AND_GO               CREATE_AND_GO
#define ISS_CREATE_AND_WAIT             CREATE_AND_WAIT
#define ISS_DESTROY                     DESTROY

#define ISS_DEF_TELNET_PORT                      23
#define ISS_FLASH_NORMAL               1
#define ISS_FLASH_FALLBACK             2

#define ISS_NODEID_FILE          FLASH "nodeid"
#define ISS_PREFERRED_MASTER     100
#define ISS_BACKUP_MASTER        50
#define ISS_PREFERRED_SLAVE      25

#define ISS_L2_ACL              1
#define ISS_L3_ACL              2


#define ISS_ENT_PHY_DESCR_LEN        255
#define ISS_ENT_PHY_TYPE_LEN         255
#define ISS_ENT_PHY_NAME_LEN         255
#define ISS_ENT_PHY_HW_REV_LEN       255
#define ISS_ENT_PHY_FW_REV_LEN       255
#define ISS_ENT_PHY_SW_REV_LEN       255
#define ISS_ENT_PHY_SER_NUM_LEN      32
#define ISS_ENT_PHY_MFG_NAME_LEN     255
#define ISS_ENT_PHY_MODEL_NAME_LEN   255
#define ISS_ENT_PHY_ALIAS_LEN        32
#define ISS_ENT_PHY_ASSET_ID_LEN     32
#define ISS_ENT_PHY_MFG_DATE_LEN     255
#define ISS_ENT_PHY_URIS_LEN         255
#define ISS_ENT_PHY_NUM              32

#define ISS_RM_IF_TYPE_ETH           "eth"
#define ISS_RM_IF_TYPE_LO            "lo"
#define ISS_RM_IF_TYPE_STACK         "stack"


#define ISS_DISS_ROLE_NONE 0
#define ISS_DISS_ROLE_MASTER 1
#define ISS_DISS_ROLE_SLAVE 2

/* Status of Hardware Support Capability*/
#define ISS_HW_NOT_SUPPORTED  0
#define ISS_HW_SUPPORTED      1 

/* List of front panel ports and SFP ports */

#define ISS_HW_FRONT_PORTS_P1_To_P4   4
#define ISS_HW_SFP_PORTS_P7_P8        7

#define ISS_PAM_MIN_PRIVILEGE         1
#define ISS_PAM_MAX_PRIVILEGE        15

/* Below enum represents the list of features for which 
 * hardware support needs to be checked and updated in global variable */
typedef enum IssHwParamsSupported
{
    ISS_HW_KNET_IFACE_SUPPORT = 1,
    ISS_HW_LA_WITH_DIFF_PORT_SPEED,   /* Creation of LAG between ports that are operating in different speeds */
    ISS_HW_UNTAGGED_PORTS_FOR_VLANS,  /* Keeping a port as untagged in more than one VLAN */
    ISS_HW_UNICAST_MAC_LEARNING_LIMIT,/* Support for Unicase mac learing limit */
    ISS_HW_QUEUE_CONFIG_ON_LA_PORT,   /* Support for Queue configurations for aggregated ports */
    ISS_HW_LA_CREATE_ON_ALL_PORT,     /* Support for creation of port channel for all ports */
    ISS_HW_LAG_ON_CEP_PORT,           /* Support for LAG on CEP ports   */
    ISS_HW_SELF_MAC_IN_MAC_ADDR_TAB,  /* Support for Self MAC in MAC address table   */
    ISS_HW_MORE_SCHEDULER_SUPPORT,    /* Support for More than one scheduler(In addition to Default schedular)*/
    ISS_HW_DWRR_SUPPORT,              /* Support for DWRR in hardware */
    ISS_HW_HIGH_CAP_CNTR,             /* Support for HCRmon in hardware */
    ISS_HW_CP_TPID_ALLOW,             /* Support for Setting TPID configurations For customer Ports */ 
    ISS_HW_SHAPE_PARAM_EIR_SUPPORT,     /* Support for EIR meter Param */
    ISS_HW_SHAPE_PARAM_EBS_SUPPORT,    /* Support for EBS meter Param */
 ISS_HW_METER_COLOR_BLIND,          /* Support for Color Blind Meter in hardware */
    ISS_HW_UNTAG_CEP_PEP,             /* Support for untagged CEP and untagged PRP as false in hardware */
    ISS_HW_QOS_CLASS_TO_POLICER_AND_Q /* In some platform like CPSS, the Traffic Class represents QueueID
                                         in CPSS Qos Profile. Assigning Traffic Class to the QOS profile
                                         should be done using the CLASS to QueueId mapping in queue-map table. */
}tIssHwParamsSupported;

/* MGMT_LOCK */
INT4 MgmtLock (VOID);
INT4 MgmtUnLock (VOID);

#define MGMT_PROTOCOL_SEM                ((const UINT1 *)"MGMSEM")
#define MGMT_LOCK()                      MgmtLock ()
#define MGMT_UNLOCK()                    MgmtUnLock ()

typedef tTMO_SLL_NODE       tIssSllNode;
typedef UINT1 tIssPortList[ISS_PORT_LIST_SIZE];
typedef UINT1 tIssAuthPortList[ISS_AUTH_PORT_LIST_SIZE];
typedef UINT1 tIssVlanList[ISS_VLAN_LIST_SIZE];
typedef UINT1 tIssPortChannelList[ISS_PORT_CHANNEL_LIST_SIZE];

/* Constants relating to login authentication mechanism 
 * Calling IssSetLoginMode with LOCAL_LOGIN causes authentication
 * to happen with the local user database.
 * Calling with REMOTE_LOGIN causes authentication to happen with
 * RADIUS.
 */
enum eLoginModes {
    LOCAL_LOGIN = 1,
    REMOTE_LOGIN_RADIUS,
    REMOTE_LOGIN_TACACS,
    REMOTE_LOGIN_RADIUS_FALLBACK_TO_LOCAL,
    REMOTE_LOGIN_TACACS_FALLBACK_TO_LOCAL,
    PAM_LOGIN
};

/* Satus of the Download operation */
typedef enum DlStatus{
 MIB_DOWNLOAD_IN_PROGRESS= 1,
 LAST_MIB_DOWNLOAD_SUCCESSFUL,
 LAST_MIB_DOWNLOAD_FAILED,
 MIB_DOWNLOAD_NOT_INITIATED
}tDlStatus;

/* Status of the Remote Save operation */
typedef enum RSStatus{
 MIB_REMOTESAVE_IN_PROGRESS= 1,
 LAST_MIB_REMOTESAVE_SUCCESSFUL,
 LAST_MIB_REMOTESAVE_FAILED
}tRSStatus;


/* PIM Mode */
typedef enum {

   ISS_DM = 1,
   ISS_SM

}tIssPimMode;


typedef enum {
    ISS_UDB_OFFSET_L2,                         /* Offset from start of mac (L2) Header*/
    ISS_UDB_OFFSET_L3,                         /* Offset from start of L3 Header*/
    ISS_UDB_OFFSET_L4,                         /* Offset from start of L4 Header*/
    ISS_UDB_OFFSET_IPV6_EXT_HDR,               /* Offset from start of IPv6 Extension Header*/
    ISS_UDB_OFFSET_L3_MINUS_2,                 /* Offset from start of L3 header minus 2.Ethernet Type of IP*/
    ISS_UDB_OFFSET_MPLS_MINUS_2               /* Offset from ethernet type of MPLS minus 2*/
}tIssUserDefFilterOffsetType;


/* Bridge Mode*/
typedef enum {

   ISS_CUSTOMER_BRIDGE_MODE = 1,
   ISS_PROVIDER_BRIDGE_MODE,
   ISS_PROVIDER_EDGE_BRIDGE_MODE,
   ISS_PROVIDER_CORE_BRIDGE_MODE,
   ISS_PBB_ICOMPONENT_BRIDGE_MODE,
   ISS_PBB_BCOMPONENT_BRIDGE_MODE  
}tIssBridgeMode;

/* SNOOP Forward Mode */
typedef enum {
   ISS_SNOOP_IP = 1,
   ISS_SNOOP_MAC
}tIssSnoopFwdMode;

/* ISS system Tables */
typedef enum {

   ISS_ALL_TABLES = 1,
   ISS_CONFIGCTRL_TABLE,
   ISS_MIRRCTRL_TABLE,
   ISS_RATECTRL_TABLE,
   ISS_PORTCTRL_TABLE

}tIssTableName;

/* IP Config Mode */
typedef enum {

   ISS_CFG_MANUAL = 1,
   ISS_CFG_DYNAMIC

}tIssCfgMode;

/* Config Save Option */
typedef enum {

   ISS_CONFIG_NO_SAVE = 1,
   ISS_CONFIG_FLASH_SAVE,
   ISS_CONFIG_REMOTE_SAVE,
   ISS_CONFIG_STARTUP_SAVE
       

}tIssConfigSaveOption;

/* Config Restore Option */
typedef enum {

   ISS_CONFIG_NO_RESTORE = 1,
   ISS_CONFIG_RESTORE,
   ISS_CONFIG_REMOTE_RESTORE

}tIssConfigRestoreOption;

/* Status Value */
typedef enum {

   ISS_ENABLE = 1,
   ISS_DISABLE,
   ISS_ENABLE_IN_PROGRESS,
   ISS_DISABLE_IN_PROGRESS
}tIssStatus;

/* Status Value */
typedef enum {

   ISS_MIRRORDISABLE = 1,
   ISS_MIRRORENABLE

}tIssMirrorStatus;

typedef enum {
    ISS_MIRR_PORT_BASED = 1,
    ISS_MIRR_MAC_FLOW_BASED,
    ISS_MIRR_VLAN_BASED,
    ISS_MIRR_INVALID,
    ISS_MIRR_IP_FLOW_BASED
}tIssMirroringType;

typedef enum {
    ISS_MIRR_RSPAN_SOURCE = 1,
    ISS_MIRR_RSPAN_DEST,
    ISS_MIRR_RSPAN_DISABLED
}tIssMirrRSpanType;

typedef enum {
    ISS_MIRR_INGRESS = 1,
    ISS_MIRR_EGRESS,
    ISS_MIRR_BOTH,
    ISS_MIRR_DISABLED
}tIssMirrMode;

typedef enum {
    ISS_MIRR_ADD = 1,
    ISS_MIRR_DELETE
}tIssMirrSrcConfig;

typedef enum {

   ISS_MIRR_LOCAL_DEL = 1,
   ISS_MIRR_RANGE_DEL,
   ISS_MIRR_SES_DEL

}tIssMirrorDelCfg;

typedef enum {
   ISS_MIRR_ADD_IN_PROGRESS = 1,
   ISS_MIRR_ADD_DONE,
   ISS_MIRR_REMOVE_IN_PROGRESS,
   ISS_MIRR_REMOVE_DONE

}tIssMirrHwStatus;

typedef enum {
    ISS_MIRR_COMPATIBILITY_MODE = 1,
    ISS_MIRR_EXTENDED_MODE,
    ISS_MIRR_TEMP_MODE
}tIssMirrConfMode;

typedef enum {
    ISS_MIRR_L2_ACL=1,
    ISS_MIRR_L3_ACL=2
        
}tIssMirrAclMode;
/* Control Mode */
typedef enum {

   ISS_AUTO = 1,
   ISS_NONEGOTIATION

}tIssPortCtrlMode;

/* HOL Blocking prevention*/
typedef enum {

   ISS_DISABLE_HOL = 1,
   ISS_ENABLE_HOL
}tIssPortHolStatus;

/* Duplex Value */
typedef enum {

   ISS_FULLDUP = 1,
   ISS_HALFDUP

}tIssPortCtrlDuplex;

typedef enum {

   ISS_AUTO_MDIX = 1,
   ISS_MDI,
   ISS_MDIX
}tIssPortCtrlMdiOrMdixCap;

typedef enum {

   ISS_CUT_THROUGH = 1,
   ISS_STORE_FORWARD
}tIssSwitchModeType;

 

/* Speed Value */
typedef enum {

   ISS_10MBPS= 1,
   ISS_100MBPS,
   ISS_1GB,
   ISS_10GB,
   ISS_40GB,
   ISS_56GB,
   ISS_2500MBPS,
   ISS_25GB,
   ISS_100GB
}tIssPortCtrlSpeed;

/* Flow Control*/
typedef enum {

    ISS_FLOW_CTRL_DISABLE = 1,
    ISS_FLOW_CTRL_TX,
    ISS_FLOW_CTRL_RX,
    ISS_FLOW_CTRL_BOTH,
    ISS_FLOW_CTRL_ANY
}tIssFlowCtrl;

/* RM Heart Beat Mode */
typedef enum {

    ISS_RM_HB_MODE_INTERNAL = 1,
    ISS_RM_HB_MODE_EXTERNAL

}tIssRmHBMode;

/* RM Redundancy Type */
typedef enum {

    ISS_RM_RTYPE_HOT = 1,
    ISS_RM_RTYPE_COLD

}tIssRmRType;

/* RM Hardware Type */
typedef enum {

    ISS_RM_DTYPE_SHARED = 1,
    ISS_RM_DTYPE_SEPARATE

}tIssRmDType;

/* RM Stacking Interface Type */
typedef enum {

    ISS_RM_STACK_INTERFACE_TYPE_OOB = 1,
    ISS_RM_STACK_INTERFACE_TYPE_INBAND

}tIssRmType;

/* CPU controlled learning status */
typedef enum {

   ISS_CPU_CNTRL_LEARN_DISABLE = 1,
   ISS_CPU_CNTRL_LEARN_ENABLE
}tIssPortCpuCntrlLearning;

/* Rate Control Packet Type */
#ifdef MRVLLS
typedef enum {

   ISS_RATE_BCAST = 1,
   ISS_RATE_MCAST_BCAST,
   ISS_RATE_DLF_MCAST_BCAST,
   ISS_RATE_ALL

}tIssRateCtrlPacketType;

#define ISS_STORM_CONTROL_128K     1
#define ISS_STORM_CONTROL_256K     2
#define ISS_STORM_CONTROL_512K     3
#define ISS_STORM_CONTROL_1M       4
#define ISS_STORM_CONTROL_2M       5
#define ISS_STORM_CONTROL_4M       6
#define ISS_STORM_CONTROL_8M       7
#define ISS_STORM_CONTROL_16M      8
#define ISS_STORM_CONTROL_32M      9
#define ISS_STORM_CONTROL_64M      10
#define ISS_STORM_CONTROL_128M     11
#define ISS_STORM_CONTROL_256M     12

/* Egress Rate Limit Values */
#define ISS_RATE_LIMIT_128K        1
#define ISS_RATE_LIMIT_256K        2
#define ISS_RATE_LIMIT_512K        3
#define ISS_RATE_LIMIT_1M          4
#define ISS_RATE_LIMIT_2M          5
#define ISS_RATE_LIMIT_4M          6
#define ISS_RATE_LIMIT_8M          7
#define ISS_RATE_LIMIT_16M         8
#define ISS_RATE_LIMIT_32M         9
#define ISS_RATE_LIMIT_64M         10
#define ISS_RATE_LIMIT_128M        11
#define ISS_RATE_LIMIT_256M        12

#define DST_MAC_BASED_ACL    0
#define SRC_MAC_BASED_ACL    1


#define ISS_ARE_MAC_ADDR_EQUAL(pMacAddr1, pMacAddr2) \
        ((MEMCMP(pMacAddr1,pMacAddr2,ISS_ETHERNET_ADDR_SIZE)) ? \
          ISS_FALSE : ISS_TRUE)

/* Backward compatibility */
typedef enum {

   ISS_RATE_DLF = 1,
   ISS_RATE_MCAST

}tIssExRateCtrlPacketType;

#else
typedef enum {

   ISS_RATE_DLF = 1,
   ISS_RATE_BCAST,
   ISS_RATE_MCAST,
   ISS_RATE_KNOWN_UCAST_MCAST

}tIssRateCtrlPacketType;
#endif



#define ISS_IMMEDIATE                         1
#define ISS_CONSOLIDATED                      2
#define ISS_COMMIT_ACTION_TRUE                1
#define ISS_COMMIT_ACTION_FALSE               0

/* Traffic Seperation related macros */
#define  ACL_TRAFFICSEPRTN_CTRL_SYSTEM_DEFAULT          (1)
#define  ACL_TRAFFICSEPRTN_CTRL_USER_DEFINED            (2)
#define  ACL_TRAFFICSEPRTN_CTRL_NONE                    (3)

/* Filter Action */
typedef enum {

   ISS_ALLOW = 1,
   ISS_DROP,
   ISS_REDIRECT_TO,
   ISS_SWITCH_COPYTOCPU,
   ISS_DROP_COPYTOCPU,
   ISS_FORQOS,
   ISS_TOMIRRORPORT,
   ISS_AND,
   ISS_OR,
   ISS_NOT
}tIssFilterAction;     

/* L3 Filter Protocol */
typedef enum {

   ISS_ICMP = 1,
   ISS_IGMP = 2,
   ISS_GGP = 3,
   ISS_IP = 4,
   ISS_TCP = 6,
   ISS_EGP = 8,
   ISS_IGP = 9, 
   ISS_NVP = 11,
   ISS_UDP = 17,
   ISS_IRTP = 28,
   ISS_IDPR = 35,
   ISS_RSVP = 46,
   ISS_MHRP = 48,
   ISS_ICMP6 = 58,
   ISS_IGRP = 88,
   ISS_OSPFIGP = 89,
   ISS_PROTO_ANY = 255

}tIssFilterProtocol;

/* ACK Bit */
typedef enum {

   ISS_ACK_ESTABLISH = 1,
   ISS_ACK_NOT_ESTABLISH,
   ISS_ACK_ANY

}tIssAckBit;

/* RST Bit */
typedef enum {

   ISS_RST_SET = 1,
   ISS_RST_NOT_SET,
   ISS_RST_ANY

}tIssRstBit;

/* TOS : This is a single byte integer of which the last three bits 
 *       (least significant bits) indicate Delay, Throughput and 
 *       Reliability i.e 'uuuuudtr', u-unused, d-delay, t-throughput, 
 *       r-reliability.
 */
typedef enum {

   ISS_TOS_NONE = 0,
              /* No TOS set */
   ISS_TOS_HI_REL,          
              /* High Reliability */ 
   ISS_TOS_HI_THR,          
              /* High Throughput */
   ISS_TOS_HI_REL_HI_THR,   
              /* High Reliability and High Throughput */ 
   ISS_TOS_LO_DEL,          
              /* Low Delay */
   ISS_TOS_LO_DEL_HI_REL,   
              /* Low Delay and High Reliability */
   ISS_TOS_LO_DEL_HI_THR,   
              /* Low Delay and High Throughput */ 
   ISS_TOS_LO_DEL_HI_THR_HI_REL,
              /* Low Delay, High Throughput and High Reliability */
   ISS_TOS_INVALID
              /* Invalid TOS, TOS not set for filtering */
}tIssTos;

typedef enum {
    /* System created ACL filter mode */
    ISS_ACL_CREATION_INTERNAL = 1,
    /* User created ACL filter mode from CLI/SNMP etc */
    ISS_ACL_CREATION_EXTERNAL
}tIssCrtnMode;

#define FSB_ACL_STATS_DISABLE  0
#define FSB_ACL_STATS_ENABLE   1
typedef struct IssSwitchThreshold
{
    INT4      i4MinThresholdTemp;
    INT4      i4MaxThresholdTemp;
    UINT4     u4MinPowerSupply;
    UINT4     u4MaxPowerSupply;
    UINT4     u4MaxCPUThreshold;
    UINT4     u4MaxRAMThreshold;
    UINT4     u4MaxFlashThreshold;
}tIssSwitchThreshold;    

typedef struct IssSwitchInfo
{
    INT4      i4CurrTemperature;
    UINT4     u4CurrCPUUsage;
    UINT4     u4CurrRAMUsage;
    UINT4     u4CurrFlashUsage;
    UINT4     u4CurrPowerSupply;
    UINT1     au1CPUSerialNum[ISS_ENT_PHY_SER_NUM_LEN];
    UINT1     au1CPUAlias[ISS_ENT_PHY_ALIAS_LEN];
    UINT1     au1CPUAssetId[ISS_ENT_PHY_ASSET_ID_LEN];
    UINT1     au1CPUUris[ISS_ENT_PHY_URIS_LEN+1];
    UINT1     au1PSSerialNum[ISS_ENT_PHY_SER_NUM_LEN];
    UINT1     au1PSAlias[ISS_ENT_PHY_ALIAS_LEN];
    UINT1     au1PSAssetId[ISS_ENT_PHY_ASSET_ID_LEN];
    UINT1     au1PSUris[ISS_ENT_PHY_URIS_LEN+1];
}tIssSwitchInfo;

typedef struct IssSwitchFanState
{
   UINT4      u4FanIndex;
   UINT4      u4CurrFanStatus;
}tIssSwitchFanState;

typedef struct IssBlockPrompt
{
       UINT4 u4LoginLockOutTime;
       UINT1 u1LoginAttempts;
       UINT1 au1Pad[3];
}tIssBlockPrompt;

/***********************************************************
* Data Structure for ISS System parameters                 * 
************************************************************/

typedef struct SysGroupInfo
{
   INT4                   i4ProvisionMode; /*This object takes values immediate/consolidated
                                              with default value set to immediate*/
   INT4                   i4TriggerCommit; /*Action specifies TRUE / FALSE*/
   UINT4                  u4IssDefaultIpAddress; 
   UINT4                  u4IssDefaultIpSubnetMask;
   UINT4                  u4VlanLearningMode; /* VLAN Learning mode */
   UINT4                  u4FrontPanelPortCount;
   tIssCfgMode            IssCfgMode; /* IP config mode as Manual or Dynamic */
   tIssPimMode            IssPimMode; /* PIM mode as DM or SM */
   tIssBridgeMode         IssBridgeMode; /*Bridge Mode as CB/PB/PEB/PCB*/
   tIssSnoopFwdMode       IssSnoopFwdMode; /* Snoop Forward mode as Mac or IP*/
   tIssRmHBMode           IssRmHBMode; /* RM Heart Beat mode */
   tIssRmRType            IssRmRType; /* RM Redundancy Type */
   tIssRmDType            IssRmDType; /* Hardware Type in RM */
   tMacAddr               BaseMacAddr; /* Base Mac Address of Switch */
   UINT2                  u2StackPortCount;
   UINT2                  u2IssDefaultVlanId;   
   UINT2                  u2IssSwitchId;   
   UINT2                  u2IssUserPreference;   
   UINT2                  u2IssGetColdStandby;
   UINT1                  au1SysCapabilitiesSupported[ISS_SYS_CAPABILITIES_LEN];
   UINT1                  au1SysCapabilitiesEnabled[ISS_SYS_CAPABILITIES_LEN];
   UINT1                  au1IssSwitchName[ISS_STR_LEN+1];
   UINT1                  au1IssHardwareVersion[ISS_STR_LEN+1];
   UINT1                  au1IssFirmwareVersion[ISS_STR_LEN+1];
   UINT1                  au1IssRestoreFileVersion[ISS_RESTORE_VERSION_LEN];
   UINT1                  au1IssRestoreFileFormat[ISS_RESTORE_FORMAT_LEN];
   UINT1                  au1IssDefaultInterface[CFA_MAX_PORT_NAME_LENGTH+1];
   UINT1                  au1IssContact[ISS_SYS_INFO_LEN+1];
   UINT1                  au1IssLocation[ISS_SYS_INFO_LEN+1];
   UINT1                  au1IssSysNpapiMode[ISS_NPAPI_MODE_LEN+1];
   UINT1                  au1IssDefaultRmIfName[CFA_MAX_PORT_NAME_LENGTH];   
   tIssBool    IssIncrSaveFlag;
   tIssSwitchThreshold    IssSwitchThreshold; /* Switch Threshold Range */
   tIssSwitchInfo         IssSwitchInfo; /* Current Switch Information */
   tIssSwitchFanState     IssSwitchFanState;
   tIssBlockPrompt        IssBlockPrompt;
   tIssBool               IssAutomaticPortCreate; 
       /* TRUE - Ports are Automatically created .
        * FALSE - Ports are created through RSTP module */     
   UINT1                  au1SerialNum[ISS_ENT_PHY_SER_NUM_LEN];
   UINT1                  au1Alias[ISS_ENT_PHY_ALIAS_LEN];
   UINT1                  au1AssetId[ISS_ENT_PHY_ASSET_ID_LEN];
   UINT1                  au1Uris[ISS_ENT_PHY_URIS_LEN];
   UINT1                  u1IssDefaultIpAddrAllocProto; 
   BOOL1                  b1IssDefValSaveFlag;
   BOOL1                  b1VrfUnqMacFlag; /* TRUE - Assign unique mac to each virtual router
                                            * FALSE - Assign switch mac to all virtual routers */
   INT1                   ai1Pack[2]; /* packing */
}tIssSysGroupInfo;

/***********************************************************
*Data Structure related to L4 SWITCHING TABLE              *
************************************************************/

typedef struct IssL4SFilterEntry {

   tIssSllNode          IssNextNode;
   INT4                 i4IssL4SFilterNo;
   tIssFilterProtocol   IssL4SProtocol;
   UINT4                u4IssL4SPortNo;
   UINT4                u4L4SCopyToPort;
   UINT1                u1IssL4SFilterStatus;
   UINT1                au1Reserved[3];
}tIssL4SFilterEntry;

#ifdef SWC
/***********************************************************
*Data Structure related to L2 FILTER TABLE                 *
************************************************************/

typedef struct IssL2FilterEntry {

   tIssSllNode          IssNextNode;
   INT4                 i4IssL2FilterNo;
   INT4                 i4IssL2FilterEtherType;
   UINT4                u4IssL2FilterProtocolType;
   INT4                 i4IssL2FilterVlanId;
   tIssFilterAction     IssL2FilterAction;
   UINT4                u4IssL2FilterMatchCount;
   tMacAddr             IssL2FilterDstMacAddr;
   tMacAddr             IssL2FilterSrcMacAddr;
   INT4                 i4IssL2FilterInPort;
   INT4                 i4IssL2FilterOutPort;
   UINT1                u1IssL2FilterStatus;
   UINT1                u1Pack[3];

}tIssL2FilterEntry;

/***********************************************************
*Data Structure related to L3 FILTER TABLE                 *
************************************************************/

typedef struct IssL3FilterEntry {

   tIssSllNode          IssNextNode;
   tTMO_DLL_NODE        PriFilterNode;
   INT4                 i4IssL3FilterNo;
   tIssFilterProtocol   IssL3FilterProtocol;
   INT4                 i4IssL3FilterMessageType;
   INT4                 i4IssL3FilterMessageCode;
   UINT4                u4IssL3FilterDstIpAddr;
   UINT4                u4IssL3FilterSrcIpAddr;
   UINT4                u4IssL3FilterDstIpAddrMask;
   UINT4                u4IssL3FilterSrcIpAddrMask;
   UINT4                u4IssL3FilterMinDstProtPort;
   UINT4                u4IssL3FilterMaxDstProtPort;
   UINT4                u4IssL3FilterMinSrcProtPort;
   UINT4                u4IssL3FilterMaxSrcProtPort;
   tIssAckBit           IssL3FilterAckBit;
   tIssRstBit           IssL3FilterRstBit;
   tIssTos              IssL3FilterTos;
   INT4                 i4IssL3FilterDscp;
   tIssFilterAction     IssL3FilterAction;
   UINT4                u4IssL2FilterMatchCount;
   UINT4                u4IssL3FilterMatchCount;
   INT4                 i4IssL3FilterVlan;
   UINT1                u1IssL3FilterStatus;
   UINT1                u1IssL3FilterHitCount;
   UINT1                u1Pack[2];

}tIssL3FilterEntry;

#define ISS_DEFAULT_FILTER_PRIORITY     1
#define ISS_RATEENTRY_MEMBLK_COUNT      (BRG_MAX_PHY_PLUS_LOG_PORTS + 1)
#define ISS_L2FILTER_LIST               gIssExGlobalInfo.IssL2FilterListHead
#define ISS_L2FILTER_LIST_HEAD          (&ISS_L2FILTER_LIST)->Tail
#define ISS_L3FILTER_LIST               gIssExGlobalInfo.IssL3FilterListHead
#define ISS_L3FILTER_LIST_HEAD          (&ISS_L3FILTER_LIST)->Tail

typedef tTMO_SLL            tIssExSll;

typedef struct IssRateCtrlEntry {

    UINT4                u4IssRateCtrlDLFLimitValue;
    UINT4                u4IssRateCtrlBCASTLimitValue;
    UINT4                u4IssRateCtrlMCASTLimitValue;
    INT4                 i4IssRateCtrlPortLimitRate;
    INT4                 i4IssRateCtrlPortBurstRate;


}tIssRateCtrlEntry;

/* Iss Extension Global Info */
typedef struct { 

    tMemPoolId              IssRateCtrlPoolId;
    tMemPoolId              IssL2FilterPoolId;
    tMemPoolId              IssL3FilterPoolId;
    tMemPoolId              IssUdbTablePoolId;
    tMemPoolId              IssUdbFilterPoolId;
    tIssRateCtrlEntry      *apIssRateCtrlEntry[ISS_RATEENTRY_MEMBLK_COUNT];
    tIssExSll               IssL2FilterListHead;
    tIssExSll               IssL3FilterListHead;
    tIssExSll               IssUdbFilterTableHead;
}tIssExtGlobalInfo;
#ifdef _ISSEXSYS_C
tIssExtGlobalInfo                gIssExGlobalInfo;

#else
extern tIssExtGlobalInfo         gIssExGlobalInfo;
#endif

#else

/* L3 Filter Direction */
typedef enum {

   ISS_DIRECTION_IN = 1,
   ISS_DIRECTION_OUT

}tIssDirection;

/* Vlan Action */
typedef enum {

   ISS_NONE = 0,
   ISS_MODIFY_VLAN,
   ISS_NESTED_VLAN,
   ISS_STRIP_OUTER_HDR,
   ISS_MODIFY_CFI_DEI,
   ISS_MODIFY_DP,
   ISS_MODIFY_TC
}tIssSubAction;

/* L2 CfiDEi Value */
typedef enum {

   ISS_CFI_DEI_RESET = 0,
   ISS_CFI_DEI_SET

}tIssCfiDei;

/* Drop Precedence */
typedef enum {
    ISS_GREEN = 1,
    ISS_YELLOW,
    ISS_RED
}tIssDropPrecedence;

/*IssHealthChkStatus*/
enum
{
    ISS_UP_AND_RUNNING = 1,
    ISS_DOWN_AND_NONRECOVERABLE_ERR,
    ISS_UP_RECOVERABLE_RUNTIME_ERR
};

enum
{
   NON_RECOV_TASK_INIT_FAILURE = 1,
   NON_RECOV_INSUFFICIENT_STARTUP_MEMORY,
   RECOV_CRU_BUFF_EXHAUSTED,
   RECOV_CONFIG_RESTORE_FAILED,
   RECOV_PROTOCOL_MEMPOOL_EXHAUSTED
};

enum
{
   ISS_CONFIG_RESTORE_SUCCESS = 1,
   ISS_CONFIG_RESTORE_FAILED,
   ISS_CONFIG_RESTORE_IN_PROGRESS,
   ISS_CONFIG_RESTORE_DEFAULT
};

/*Module ID for ISSHealthChkClear countets*/
#define   ISS_ALL_CLEAR_COUNTER   0xff
#define   ISS_BGP_CLEAR_COUNTER   0x01
#define   ISS_OSPF_CLEAR_COUNTER        0x02
#define   ISS_RIP_CLEAR_COUNTER         0x04
#define   ISS_RIP6_CLEAR_COUNTER        0x08
#define   ISS_OSPF3_CLEAR_COUNTER       0x10
#define   ISS_IPV4_CLEAR_COUNTER        0x20
#define   ISS_IPV6_CLEAR_COUNTER        0x40

/***********************************************************
*Data Structure related to Redirec Group Info Pass to Npapi*
************************************************************/
typedef struct _IssRedirectInterfaceGrpInfo {
     UINT4             u4RedirectGrpId;    /*Identifies the Interface Group uniquely */
     UINT4             u4EgressIfIndex;    /* CFA IF Index of Egress interface or Trunk */
     UINT2             u2TunnelIfIndex;    /* CFA If Index of Tunnel interface 
                                              valid when Egress Type tunnel */
     UINT2             u2TrafficDistByte;    /*Virtual trunk traffic 
                                               distribution byte . */
     UINT1             u1UdbPosition;    /*Position of UserDefined Byte*/ 
     UINT1             u1EgressIfType;      /*  PORT, TRUNK or TUNNEL_IF. 
                                              Used to set the tunnel start 
           related information */
     UINT1             u1TrafficDistMaskMsb;   /* Traffic Distribution Mask of MSB */
     UINT1             u1TrafficDistValueMsb;  /* Traffic Distribution Value  of MSB*/
     UINT1             u1TrafficDistMaskLsb;   /* Traffic Distribution Mask of LSB*/
     UINT1             u1TrafficDistValueLsb;  /* Traffic Distribution Value  of LSB*/
     UINT1             u1PriorityFlag;
     UINT1             au1Pad[1];
             
}tIssRedirectIfGrp;

/************************************************
*   Reserved frame Control Table
*************************************************/

typedef struct IssReservFrmCtrlTable {
    tMacAddr       MacAddr;                /* Mac Address to be specified in case of other control frames */
    UINT1          u1ControlAction;        /* Action to be performed whether dropped or forwarded */
    UINT1          u1CtrlStatsStatus;      /* Control frame statistics status */
    UINT4          u4ReservedFrameCtrlId;  /* Reserve Frame Control Id */
    UINT4          u4ControlPktType;       /* Control Frame Type */
    UINT4          u4MatchCount;           /* No of control frames to be suppressed or forwarded based on the action */
    UINT4          u4MacMask;              /* Mask to be applied to acquire the range of mac address to be suppressed or forwarded based on the action */
    UINT1          u1RowStatus;
    UINT1     u1OperationStatus;
    UINT1          u1Pad[2];
}tIssReservFrmCtrlTable;



#define MAX_PORTS 24
typedef struct IssFilterRestore {
 UINT2 au2Restore[MAX_PORTS];
        INT4  *pi4RestoreFlag;
}tIssFilterRestore;

typedef struct IssPriorityFilterTable {
    tTMO_DLL           PriFilterList;             /*  Pointer to next node of Single Linked list*/
}tIssPriorityFilterTable;


typedef struct IssPriorityFilterEntry {
    tTMO_DLL_NODE      PriNode;             /*  Node points to next filter node (L2/L3/UDB)*/
    UINT1              u1FilterType;    /* Specifies the next type of filter (L2/L3 or UDB) */
    UINT1              au1Pad[3];
}tIssPriorityFilterEntry;

typedef struct IssFilterShadowTable {
UINT4 au4HwHandle[ISS_FILTER_SHADOW_MEM_SIZE];
  /* Array of Hardware Handle for the filter */
}tIssFilterShadowTable;

/***********************************************************
*Data Structure related to L2 FILTER TABLE                 *
************************************************************/

typedef struct IssL2FilterEntry {

   tIssSllNode          IssNextNode;
   tIssPriorityFilterEntry   PriorityNode;
   tIssSllNode          IssSortedNextNode;
   INT4                 i4IssL2FilterNo;
   INT4                 i4IssL2NextFilterNo; /*Acl Filter number of L2/L3/UDB type*/
   INT4                 i4IssL2NextFilterType; /*Acl Filter of Type L2/L3/UDB*/
   tIssFilterShadowTable *pIssFilterShadowInfo; /* ACL Filter Shadow info to 
                                                   store Hw Handle information */
   INT4                 i4IssL2FilterPriority;
   INT4                 i4IssL2FilterEncapType;
   INT4                 i4IssL2FilterStatsEnabledStatus; /* Hw Statistics */
   INT4                 i4IssClearL2FilterStats; /* Clear Hw Statistics */
   UINT4                u4IssL2FilterProtocolType;
   UINT4                u4IssL2FilterCustomerVlanId;
   UINT4                u4IssL2FilterServiceVlanId;
   UINT4                u4IssL2FilterMatchCount;
   UINT4                u4IssL2FilterRedirectPort;
   UINT4                u4StatsTransitFlag;
   UINT4          u4IssL2RedirectId;
   UINT4                u4RefCount;              /* RefCount - How Many time 
                                                    used by qosxtd module 
                                                    for Classification*/
   UINT4                u4SChannelIfIndex;   /*S-Channel Interface Index*/
   tIssFilterAction     IssL2FilterAction;
   tIssRedirectIfGrp    RedirectIfGrp;
   tMacAddr             IssL2FilterDstMacAddr;
   tMacAddr             IssL2FilterSrcMacAddr;
   tIssPortList         IssL2FilterInPortList;
   tIssPortList         IssL2FilterOutPortList;
   tIssPortChannelList         IssL2FilterInPortChannelList;
   tIssPortChannelList         IssL2FilterOutPortChannelList;
   tIssFilterRestore    FilterRestore;
   UINT2                u2InnerEtherType;
   UINT2                u2OuterEtherType;
   UINT2                u2IssL2SubActionId; /* Vlan Id to be used for Vlan Action */
   INT1                 i1IssL2FilterCVlanPriority;
   INT1                 i1IssL2FilterSVlanPriority;
   UINT1                u1IssL2FilterTagType;    /* Single Tag or Double tag */
   UINT1                u1FilterDirection;
   UINT1                u1IssL2FilterStatus;
   UINT1                u1IssL2HwStatus; /*ISS_OK: Implies Hw entry exists
                                            ISS_NOTOK implies Hw entry does not exist*/
   UINT1                u1IssL2SubAction; /* Action to Perform Nested/Modify/None */
   UINT1                u1PriorityFlag;
   UINT1                u1IssL2FilterCreationMode; /* Internal or External */
   INT1                 i1DropPrecedence; /* Drop precedence of the packet */
   INT1                 i1CfiDei;         /* CFI/DEI of the packet */
   UINT1                u1IssL2FilterUserPriority;
   UINT1                u1IssL2FilterTrapSent;
   BOOL1                b1IsL2AclConfFromExt; /* Configuration through ACL/Other protocol Flag */
   UINT1                 u1IssL2OutFilterCount;
   UINT1                au1Pad[3];
}tIssL2FilterEntry;

/***********************************************************
*Data Structure related to L3 FILTER TABLE                 *
************************************************************/

typedef struct IssL3FilterEntry {

   tIssSllNode          IssNextNode;
   tIssPriorityFilterEntry   PriorityNode;
   tIssSllNode          IssSortedNextNode;
   INT4                 i4IssL3FilterNo;
   INT4                 i4IssL3FilterPriority;
   tIssFilterProtocol   IssL3FilterProtocol;
   tIssFilterShadowTable *pIssFilterShadowInfo;   /* ACL Filter Shadow info to 
                                                   store Hw Handle information */
   INT4                 i4IssL3FilterMessageType;
   INT4                 i4IssL3FilterMessageCode;
   UINT4                u4CVlanId;
   UINT4                u4SVlanId;
   UINT4                u4IssL3FilterDstIpAddr;
   UINT4                u4IssL3FilterSrcIpAddr;
   UINT4                u4IssL3FilterDstIpAddrMask;
   UINT4                u4IssL3FilterSrcIpAddrMask;
   UINT4                u4IssL3FilterMinDstProtPort;
   UINT4                u4IssL3FilterMaxDstProtPort;
   UINT4                u4IssL3FilterMinSrcProtPort;
   UINT4                u4IssL3FilterMaxSrcProtPort;
   UINT4                u4IssL3FilterRedirectPort;
   UINT4                u4SChannelIfIndex; /* S-Channel Interface Index */
   tIssAckBit           IssL3FilterAckBit;
   tIssRstBit           IssL3FilterRstBit;
   tIssTos              IssL3FilterTos;
   INT4                 i4IssL3FilterDscp;
   tIssDirection        IssL3FilterDirection;
   tIssFilterAction     IssL3FilterAction;
   tIssRedirectIfGrp    RedirectIfGrp;
   UINT4                u4IssL3FilterMatchCount;
   UINT4                u4StatsTransitFlag;
   UINT4                u4RefCount;              /* RefCount - How Many time 
                                                    used by qosxtd module 
                                                    for Classification*/
   tIssPortList         IssL3FilterInPortList;
   tIssPortList         IssL3FilterOutPortList;
   tIssPortChannelList  IssL3FilterInPortChannelList;
   tIssPortChannelList  IssL3FilterOutPortChannelList;
   tIssFilterRestore    FilterRestore;
   UINT4                u4IssL3FilterMultiFieldClfrDstPrefixLength;
                                                    /*Ref:stdqos.mib*/
   UINT4                u4IssL3FilterMultiFieldClfrSrcPrefixLength;/*Ref:stdqos.mib*/
   UINT4                u4IssL3MultiFieldClfrFlowId;           /*Ref:stdqos.mib*/
   UINT4             u4IssL3RedirectId;
   INT4                 i4IssL3MultiFieldClfrAddrType;         /*Ref:stdqos.mib*/
   INT4                 i4StorageType;                       /*Ref:stdqos.mib*/
   INT4                 i4IssL3FilterStatsEnabledStatus; /* Hw Statistics */
   INT4                 i4IssClearL3FilterStats; /* Clear Hw Statistics */
   tIp6Addr             ipv6SrcIpAddress;    /*Ref:stdqos.mib ace IP address */
   tIp6Addr             ipv6DstIpAddress;
   UINT2                u2IssL3SubActionId;  /* Vlan Id to be used for Vlan Action */
   
   INT1                 i1CVlanPriority;
   INT1                 i1SVlanPriority;
   UINT1                u1IssL3FilterTagType;    /* Single Tag or Double tag */
   UINT1                u1IssL3FilterStatus;
   UINT1                u1IssL3HwStatus; /*ISS_OK: Implies Hw entry exists
                                            ISS_NOTOK implies Hw entry does not exist*/
   UINT1                u1IssL3SubAction;  /* Action to Perform Nested/Modify/None */
   UINT1                u1PriorityFlag;
   UINT1                u1IssL3FilterCreationMode; /* Internal or External */
   UINT1                u1IssL3FilterTrapSent;
   BOOL1                b1IsL3AclConfFromExt;  /* Configuration through ACL/Other protocol Flag */
   UINT1                u1IssL3OutFilterCount;
   UINT1                au1Pad[3];
}tIssL3FilterEntry;


/* Data structure for L2 Protocols */
typedef struct _tQoSAcLInfoL2
{
    tQoSId            QosIds;          /* QoS parameters */
    tIssL2FilterEntry L2FilterInfo;    /* L2 Filter ACL Entry */
    UINT4             u4ProtocolRate;  /* Rate of protocol control packets */
    UINT2             u2L2Protocol;    /* internal defined protocol macro no */
    UINT1             u1CpuQueueNo;    /* CPU queue no,packets are directed */
    UINT1             au1dummy [1];    /* Padding */
} tQoSAcLInfoL2Protocols;

/* Data structure for L3 Protocols */
typedef  struct _tQoSAcLInfoL3
{
    tQoSId            QosIds;           /* QoS parameters */
    tIssL3FilterEntry L3FilterInfo;     /* L2 Filter ACL Entry */
    UINT4             u4ProtocolRate;   /* Rate of protocol control packets */
    UINT2             u2L3Protocol;     /* internal defined protocol macro no */
    UINT1             u1CpuQueueNo;     /* CPU queue no,packets are directed */
    UINT1             au1dummy [1];     /* Padding */
} tQoSAcLInfoL3Protocols;

/***********************************************************
*Data Structure related to User Defined Filter FILTER TABLE *
************************************************************/
typedef enum {
 ISS_UDB_PKT_TYPE_IPV4_TCP, 
   ISS_UDB_PKT_TYPE_IPV4_UDP,
   ISS_UDB_PKT_TYPE_MPLS,
   ISS_UDB_PKT_TYPE_FRAG, 
   ISS_UDB_PKT_TYPE_IPV4, 
   ISS_UDB_PKT_TYPE_IPV6_TCP, /* Not Supported by CPSS */
   ISS_UDB_PKT_TYPE_USER_DEF, 
   ISS_UDB_PKT_TYPE_IPV6, 
   ISS_UDB_PKT_TYPE_IPV6_UDP
}tIssUserDefFilterPktType;

typedef enum {
    ISS_L2_FILTER = 1,
    ISS_L3_FILTER,
}tIssUserDefFilterType;

typedef  struct  _IssUserDefinedFilterEntry {
   tIssFilterAction       IssAccessFilterAction;   /* PERMIT/DENY/REDIRECT */
   tIssRedirectIfGrp      RedirectIfGrp;       
   tIssPortList           IssUdbFilterInPortList;
   tIssFilterRestore       FilterRestore;
   UINT4                  u4FilterShadowInfo;    /*variable to store Hw Handle information*/
   UINT1                  au1AccessFilterOffsetValue[ISS_UDB_MAX_OFFSET];  /*  Offset Value */
   UINT1                  au1AccessFilterOffsetMask[ISS_UDB_MAX_OFFSET];   /*   Offset Mask */
   UINT4                  u4UDBFilterId;
   UINT4                  u4IssUdbFilterMatchCount; /* Filter Hits */
   INT4                   i4IssUdbFilterStatsEnabledStatus; /* Hw Statistics */
   INT4                   i4IssClearUserDefinedFilterStats; /* Clear Hw Statistics */
   UINT2                  u2IssUdbSubActionId;  /* Vlan Id to be used for Vlan Action */

   UINT2                  u2AccessFilterOffset;  /*  Offset from start of packet header */
   UINT1                  u1AccessFilterPktType;  /* TCP, UDP, MPLS, IPv4/v6, FRAG_IP, 
                                                    Ethernet */
   UINT1                   u1AccessFilterPriority;
   UINT1                   u1IssUdbSubAction;  /* Action to Perform Nested/Modify/None */

   UINT1                   u1Pad;
}tIssUDBFilterEntry;

typedef  struct  _IssUserDefinedFilterTable {
  tTMO_SLL_NODE                IssNextNode; 
  tIssPriorityFilterEntry      PriorityNode;
  tIssUDBFilterEntry           *pAccessFilterEntry;
  UINT4                        u4BaseAclOne;
  UINT4                        u4BaseAclTwo;
  UINT1                        u1Operation; /* AND/OR / NOT */
  UINT1                        u1AclOneType; /* ISS_L2_FILTER/ ISS_L3_FILTER */
  UINT1                        u1AclTwoType; /* ISS_L2_FILTER/ ISS_L3_FILTER */
  UINT1                        u1RowStatus; /* Row Status of User define filter entry */
  UINT1                        u1PriorityFlag;
  UINT1                        au1Pad[3];
}tIssUserDefinedFilterTable;

typedef struct AclFilterInfo {
    tPortList             PortList;       /* Incoming port list */
    UINT4                 u4HostIp;       /* source IP address */
    tMacAddr              HostMac;        /* Source MAC address */
    UINT2                 u2VlanId;       /* Outer VLAN ID */
    UINT1                 u1ProtocolId;   /* Protocol ID */
    UINT1                 u1Action;       /* Filter Action(Allow/Deny) */
    UINT1                 u1Priority;     /* Filter priority */
    UINT1                 u1FilterType;   /* Filter type */
}tAclFilterInfo; 

/***********************************************************
*Data Structure related to REDIRECT INTERFACE FROUP TABLE  *
************************************************************/
/* Redirect Group Filter Type */

#define ISS_REDIRECT_PORT_LIST_SIZE        BRG_PORT_LIST_SIZE
 
#define ISS_REDIRECT_MAX_PORTS  SYS_DEF_MAX_PORTS_PER_CONTEXT

#define ISS_REDIRECT_PORTS_PER_BYTE          8 

#define ISS_REDIRECT_BIT8                     0x80

#define ISS_DEFAULT_PRIORITY                 1

typedef enum {
   ISS_L2FILTER  = 1,
   ISS_L3FILTER,      
   ISS_UDBFILTER,     
   ISS_NOFILTER,       
   ISS_L2_REDIRECT,
   ISS_L3_REDIRECT,
   ISS_UDB_REDIRECT,
   ISS_L3_IPV6_REDIRECT
}tIssRedirectGrpFilterType;

typedef enum {
   ISS_REDIRECT_TO_PORT = 1,
   ISS_REDIRECT_TO_PORTLIST
}tIssRedirectGrpIntfType;

typedef enum {
   ISS_REDIRECT_TO_ETHERNET = 1,
   ISS_REDIRECT_TO_TRUNK,
   ISS_REDIRECT_TO_MPLS
}tIssRedirectGrpIntfPortType;


typedef enum {
   ISS_REDIRECT_UDB_FIELD  = 128,  
   ISS_TRAFF_DIST_BYTE_SRCIP, 
   ISS_TRAFF_DIST_BYTE_DSTIP,  
   ISS_TRAFF_DIST_BYTE_SMAC,  
   ISS_TRAFF_DIST_BYTE_DMAC,  
   ISS_TRAFF_DIST_BYTE_STCPPORT,  
   ISS_TRAFF_DIST_BYTE_DTCPPORT,
   ISS_TRAFF_DIST_BYTE_SUDPPORT,  
   ISS_TRAFF_DIST_BYTE_DUDPPORT,  
   ISS_TRAFF_DIST_BYTE_VLANID,  
   ISS_TRAFF_DIST_BYTE_ETHERTYPE,  
   ISS_TRAFF_DIST_BYTE_IPPROTOCOL,  
   ISS_TRAFF_DIST_BYTE_INNER_SRCIP,
   ISS_TRAFF_DIST_BYTE_INNER_DESTIP
}tIssRedirectGrpDistField;



#endif

/***************************************************
 * Mirroring NPAPI requirements
 * ************************************************/
typedef struct 
{
        UINT4  u4SourceNo;      /*it can be a VLAN ID,Port Number, ACL Id*/
        UINT4  u4ContextId;     /* In case source is VLAN, context Id to which 
                                VLAN belong*/
        UINT1  u1Mode;          /*it can be Ingress/Egress/Both*/
        UINT1  u1FilterDirection;  /* Flag to check if Ingress or Egress ACL(Applicable only if
                                       u1MirrType is ISS_MIRR_MAC_FLOW_BASED/ISS_MIRR_IP_FLOW_BASED)*/
        UINT1  au1Pad[2];
} tIssHwSourceId;

typedef struct HwMirrorInfo
{
   tIssHwSourceId SourceList[ISS_MIRR_HW_SRC_RECD];   /*List of Source 
entries*/
   UINT4       u4DestList[ISS_MIRR_HW_DEST_RECD];      /*List of Destination 
entries*/
   UINT4       u4RSpanContextId;    /*Context Id to which RSpan VLAN belongs*/
   UINT2       u2RSpanVlanId;       /*VLAN ID for Remote SPAN session*/
   UINT2       u2SessionId;         /*Session ID to which this destination and 
                                     source entities belong*/
   UINT1       u1Mode;              /* Can be Egress/ingress/Both*/
   UINT1       u1MirrType;          /*Used to configure Mirroring type for this 
call*/
   UINT1       u1RSpanSessionType;  /*RSPAN Session type . Source/Destination*/
   UINT1       u1Action;            /*Used to configure Mirroring in hardware or remove
                                      mirroring configuration from hardware .It can take values 
                                      as ADD_IN_PROGRESS/ADD_DONE/REM_IN_PROGRESS/REM_DONE*/
}tIssHwMirrorInfo;

typedef struct {
    UINT1              u1HwKnetIntf;
    UINT1              u1HwLaWithDiffPortSpeed;
    UINT1              u1HwUntaggedPortsForVlans;
    UINT1              u1HwUnicastMacLearningLimit;
    UINT1              u1HwQueueConfigOnLaPort;
    UINT1              u1HwLagOnAllPorts;
    UINT1              u1HwLagOnCep;
    UINT1              u1HwSelfMacInMacTable;
    UINT1              u1HwMoreScheduler;
    UINT1              u1HwDwrrSupport;
    UINT1              u1HwHighCapacityCntr;
    UINT1              u1HwCustomerTpidAllow;
    UINT1              u1ShapeParamEIR;
    UINT1              u1ShapeParamEBS;
    UINT1              u1HwMeterColorBlind;
    UINT1              u1HwUntagCepPep;
    UINT1              u1HwQoSClassToPolicerAndQ;
    UINT1              u1Pad[3];
}tNpIssHwGetCapabilities;

/* This structure is shared between HTTP and ISS, and is used to store the
 * control socket 
 */
typedef struct ISSHTTPCFG {
    INT4      i4IssCtrlSockFd; /* The Control socket used*/
} tISSHttpCfg;

/* ISS Modules */
typedef enum {
   ISS_SYSTEM_MODULE = 1,
   ISS_SNMP_MODULE,
   ISS_LA_MODULE,
   ISS_SYSLOG_MODULE,
   ISS_CLI_MODULE,
   ISS_CFA_MODULE,
   ISS_FWL_MODULE,
   ISS_SSL_MODULE,
   ISS_SSH_MODULE,
   ISS_NVRAM_MODULE,
   ISS_VPN_MODULE,
   ISS_RAD_MODULE,
   ISS_IKE_MODULE,
   ISS_MSR_MODULE,
   ISS_CLKIWF_MODULE,
   ISS_FM_MODULE,
   ISS_FIPS_MODULE,
   ISS_FSB_MODULE,
   ISS_MAX_MODULE
}tIssModule;

/* Enum of Module ID used to start and shut the process */
enum {
    OSPF_MODULE_ID = 1,
    OSPF3_MODULE_ID,
    BGP_MODULE_ID,
    ISIS_MODULE_ID,
    RSVPTE_MODULE_ID,
    LDP_MODULE_ID,
    MAX_PROTOCOLS
};

enum {
      OSPF_NF_QUEUE,
      OSPF3_NF_QUEUE
};

/* Module System control state */
#define MODULE_IDLE     0
#define MODULE_SHUTDOWN 1
#define MODULE_START    2

#define ISS_FIPS_MODE                1
#define ISS_DEFAULT_FIPS_OPER_MODE   2
#define ISS_CNSA_MODE                3

extern tIssSysGroupInfo       gIssSysGroupInfo;
/* Prototypes */

PUBLIC VOID  IssSysMainTask        PROTO ((INT1 *));
INT4  IssInit                      PROTO ((INT1 *));
INT4  IssCreatePort                PROTO ((UINT2, tIssTableName));
VOID  IssCreateVlan                PROTO ((UINT2, UINT2));
VOID  IssMirrCreateAcl             PROTO ((UINT4, INT4));
INT4  RegisterISSwithFutureSNMP    PROTO ((VOID));
INT4  IssDeletePort                PROTO ((UINT2, tIssTableName));
UINT4 IssGetPimModeFromNvRam       PROTO ((VOID));
VOID  IssSetPimModeToNvRam         PROTO ((UINT4));
UINT4 IssGetBridgeModeFromNvRam    PROTO ((VOID));
UINT2 IssGetDefaultVlanIdFromNvRam    PROTO ((VOID));
UINT4 IssGetFrontPanelPortCountFromNvRam PROTO ((VOID));
UINT1 * IssGetNpapiModeFromNvRam    PROTO ((VOID));
UINT4 IssGetSnoopFwdModeFromNvRam    PROTO ((VOID));
VOID  IssSetSnoopFwdModeToNvRam      PROTO ((UINT4));
UINT4 IssGetConfigModeFromNvRam    PROTO ((VOID));
UINT4 IssGetIpAddrFromNvRam        PROTO ((VOID));
void  IssGetIp6AddrFromNvRam    PROTO ((tIp6Addr *plocalIp6Addr));
UINT1  IssGetIp6PrefixLenFromNvRam  PROTO ((VOID));
VOID  IssSystemRestart             PROTO ((VOID));
VOID  CustomStartup                PROTO ((int argc, char *argv[]));
INT4  IssMirrorStatusCheck         PROTO ((UINT2));
INT4  IssIsPortListValid            PROTO ((UINT1 *pu1Ports, INT4 i4Len));
UINT1 * IssSysGetSwitchName        PROTO ((VOID));
VOID  IssSetSwitchNameToNvRam PROTO((UINT1 *pu1SwitchName));
UINT4 IssSysGetDefSwitchIp    PROTO ((VOID));
INT4  IssGetAggregatorMac          PROTO ((UINT2, tMacAddr));
VOID  IssFreeAggregatorMac         PROTO ((UINT4));
UINT4 IssGetRestoreOptionFromNvRam PROTO ((VOID));
INT1 IssGetRestoreFlagFromNvRam PROTO ((VOID));
UINT4 IssGetVlanLearningModeFromNvRam PROTO ((VOID));
INT1 *IssGetInterfaceFromNvRam PROTO ((VOID));
INT1 *IssGetRmIfNameFromNvRam  PROTO ((VOID));
INT1 *IssGetSoftwareVersionFromNvRam  PROTO ((VOID));
UINT4 IssGetSubnetMaskFromNvRam PROTO ((VOID));
UINT4 IssGetIpAddrAllocProtoFromNvRam PROTO ((VOID));
UINT4 IssGetLoginPromptFromNvRam   PROTO ((VOID));
VOID  IssSetIpAddrAllocProtoToNvRam PROTO ((UINT4 u4IpAddrAllocProto));
VOID IssSetBridgeModeToNvRam       PROTO((UINT4));
VOID IssSetDefaultVlanIdToNvRam    PROTO ((UINT2));
VOID IssSetFrontPanelPortCountToNvRam PROTO ((UINT4 u4FrontPanelPortCount));
INT4 IssIsFrontPanelPortValid PROTO ((UINT2 u2IfIndex));
VOID IssSetNpapiModeToNvRam    PROTO ((UINT1 *));
VOID IssSetRestoreOptionToNvRam      PROTO ((UINT4 ));
VOID IssSetConfigRestoreOption  PROTO ((UINT4 ));
INT4 IssGetIncrementalSaveStatus PROTO ((VOID));
INT4 IssGetAutoSaveStatus PROTO ((VOID));
VOID IssEraseConfigurations PROTO ((VOID));
INT4 IssSaveConfigurations PROTO ((VOID));
BOOL1 IssL2IwfMiIsVlanActive PROTO ((UINT4 u4ContextId, UINT2 u2VlanId));
INT1 IssGetDefValSaveFlagFromNvRam PROTO ((VOID));
VOID IssSetDefValSaveFlagToNvRam PROTO ((INT1 i1DefValSaveFlag));
VOID IssSetConfigSaveOption PROTO ((UINT4));
INT4 IssGetAutoPortCreateFlag PROTO ((VOID));
/* Prototype for custom CLI banner and cli prompt */
INT1 IssGetCliDisplayBanner PROTO((CHR1 *pu1BannerMsg));
const char * IssGetCustomCliPrompt PROTO((VOID));
VOID CliTftpDownloadStatus(INT4 i4Data, UINT4 u4FileSize);    
/* Prototype for Custom System Informations */
CONST CHR1  * IssGetHardwareVersion PROTO ((VOID));
CONST CHR1  * IssGetHardwarePartNumber PROTO ((VOID));
CONST CHR1  * IssGetSoftwareVersion PROTO ((VOID));
CONST CHR1  * IssGetSoftwareSerialNumber PROTO ((VOID));

INT1* IssGetSnmpEngineID PROTO ((VOID));
UINT4 IssGetSnmpEngineBoots PROTO ((VOID));
VOID IssSetSnmpEngineIDToNvRam PROTO ((INT1 *));
VOID IssSetSnmpEngineBootsToNvRam PROTO ((UINT4));
VOID IssSetVlanLearningModeToNvRam PROTO((UINT4));
INT4  IssExInit                    PROTO ((VOID));
INT4  IssExCreatePortRateCtrl      PROTO ((UINT2, tIssTableName));
INT4  IssExDeletePortRateCtrl      PROTO ((UINT2));
#ifdef SWC
INT4  IssExGetOutPortFromACL (UINT4 u4FilterId);
#endif

INT4
IssApplyFilter PROTO ((UINT2 u2InPort, UINT2 InVlan, UINT4 u4SrcIp, 
                       UINT1 u1L3Proto, UINT2 u2L4DstPort,BOOL1 bOOBFlag));
INT4 IssIpAuthMgrInit PROTO ((VOID));
INT4 IssCallBackRegister           PROTO ((UINT4 u4Event, 
                                           tFsCbInfo *pFsCbInfo));
INT4 IssGetVRMacAddr   PROTO ((UINT4 u4ContextId, tMacAddr * pVRMac));
/* HITLESS RESTART */
INT1  IssGetHRFlagFromNvRam       PROTO ((VOID));
VOID  IssSetHRFlagToNvRam         PROTO ((INT1));

/*ISS Health Check*/
PUBLIC INT4  ISSSetHealthChkStatus   PROTO ((UINT1 u1IssHealthStatus));
PUBLIC INT4  ISSSetHealthChkErrReason   PROTO ((UINT1 u1IssHealthErrReason));
PUBLIC INT4  ISSSetHealthChkMemAllocErrPoolId PROTO ((UINT4 PoolId));
PUBLIC INT4  ISSSetHealthConfigRestoreStatus PROTO ((UINT1 u1ConfigRestoreStatus));
PUBLIC VOID  BgpHealthChkClearCtr   PROTO ((VOID));
PUBLIC VOID  OspfHealthChkClearCtr   PROTO ((VOID));

PUBLIC VOID  RipHealthChkClearCtr   PROTO ((VOID));
PUBLIC VOID  Rip6HealthChkClearCtr   PROTO ((VOID));
PUBLIC VOID  Ospf3HealthChkClearCtr  PROTO ((VOID));
PUBLIC VOID  Ipv4HealthChkClearCtr   PROTO ((VOID));
PUBLIC VOID  Ipv6HealthChkClearCtr  PROTO ((VOID));

PUBLIC VOID PopulateOspfCounters PROTO ((VOID));
PUBLIC VOID PopulateBgpCounters PROTO ((VOID));
PUBLIC VOID PopulateRipCounters PROTO ((VOID));
PUBLIC VOID PopulateRip6Counters PROTO ((VOID));
PUBLIC VOID PopulateOspf3Counters PROTO ((VOID));
PUBLIC VOID PopulateIpv4Counters PROTO ((VOID));
PUBLIC VOID PopulateIpv6Counters PROTO ((VOID));

#define ISS_LOOP_DURATION        1

#define ISS_SERIAL_CONSOLE_TRUE   1
#define ISS_SERIAL_CONSOLE_FALSE  2
#ifdef WEBNM_WANTED
INT1  IssSystemInit                PROTO ((VOID));
VOID IssProcessCtrlMsgs            PROTO ((VOID));
#endif
INT4 IssLock (VOID);
INT4 IssUnLock (VOID);
PUBLIC VOID IssGetPortCtrlMode (INT4   i4IfIndex, INT4 *pi4PortCtrlMode);
PUBLIC UINT4 IssGetPortCtrlDuplex (INT4 i4IssPortCtrlIndex,
                                   INT4 *pi4PortCtrlDuplex);

INT4 IssGetAsyncMode PROTO ((INT4 i4ProtoId));
UINT1 IssGetDissRolePlayed PROTO ((VOID));

#ifdef MBSM_WANTED
/* Prototypes for MBSM */

PUBLIC INT4  IssMbsmL2FilterCardInsert PROTO  
((tMbsmPortInfo* pPortInfo, tMbsmSlotInfo* pSlotInfo));

PUBLIC INT4 IssMbsmL2FilterCardRemove PROTO 
((tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo));

PUBLIC INT4 IssMbsmL3FilterCardInsert PROTO 
((tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo));

PUBLIC INT4 IssMbsmL3FilterCardRemove PROTO 
((tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo));

PUBLIC INT4 IssMbsmCardInsertConfigCtrlTable 
PROTO ((tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo));

PUBLIC INT4 IssMbsmCardInsertRateCtrlTable 
PROTO ((tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo));

PUBLIC INT4 IssMbsmCardInsertPortCtrlTable 
PROTO ((tMbsmPortInfo * pPortInfo,tMbsmSlotInfo * pSlotInfo));

PUBLIC INT4 IssMbsmCardInsertMirrorCtrlTable 
PROTO ((tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo));


PUBLIC INT4 IssMbsmCardRemoveMirrorCtrlTable 
PROTO ((tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo));

PUBLIC INT4 IssMbsmDiffservCardInsert 
PROTO ((tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo));

PUBLIC INT4
IssMbsmCardInsertCpuMirroring PROTO ((tMbsmSlotInfo * pSlotInfo));

PUBLIC INT4 IssMbsmIGSCardInsert 
PROTO ((tMbsmPortInfo* pPortInfo, tMbsmSlotInfo* pSlotInfo));

PUBLIC INT4 IssMbsmIGSCardRemove 
PROTO ((tMbsmPortInfo* pPortInfo, tMbsmSlotInfo* pSlotInfo));

PUBLIC VOID IssSetMbsmMaxPortsPerSlotToNvRam 
PROTO ((UINT4 u4MbsmMaxPortsPerSlot));

PUBLIC VOID IssSetMbsmMaxSlotsToNvRam 
PROTO ((UINT4 u4MbsmMaxSlots));

PUBLIC VOID IssSetMbsmMaxLcSlotsToNvRam 
PROTO ((UINT4 u4MbsmMaxLcSlots));

PUBLIC VOID IssGetMbsmInfoFromNvRam
PROTO ((tMbsmSysInfo *pMbsmSysInfo));
#endif

VOID IssGetContextMacAddress (UINT4 u4ContextId, 
                              tMacAddr pRetValIssContextMacAddress);
VOID
IssGetVrfUnqMacAddress (UINT4 u4VrId,
                         tMacAddr * pRetValIssVrfMacAddress);

INT4  IssAppendFilePath (UINT1 *pu1FileName);
UINT2 IssGetStackPortCountFromNvRam (VOID);
UINT2 IssGetTimeStampMethodFromNvRam (VOID);
VOID  IssSetStackPortCountToNvRam (UINT2 u2StackPortCount);
INT4 IssHwSendBufferToLinux PROTO ((UINT1 *pBuffer, UINT4 u4Len, INT4 i4ImageType));
INT4 IssCustReadFile PROTO ((UINT1 *u1FirmwareImageName));
INT4 IssCustAccessHwConsole PROTO ((UINT1 *pu1HwCmd)); 
INT4 IssGetLoginAttempts PROTO ((VOID));
INT4 IssMirrIsSrcConfigured(UINT4 u4SessionNo, UINT4 u4ContextId, UINT4 u4SrcNum, UINT1 u1MirrorType);
INT4 IssMirrIsDestConfigured(UINT4 u4SessionNo, UINT4 u4DestNum);

/*Reserved frame proto type which is defined here commonly since it is getting used in both control plane and npapi*/
PUBLIC INT4 IssFetchMacAddressesFrmMask PROTO ((tMacAddr ResrvMacAddresses[], tMacAddr MacAddr, UINT4 u4MacMask, INT4 *pi4TotalMacAddresses));

#define MSR_UPDATE_EVENT  0x10000

/*SNMP update data structure added */
#define MSR_RBNODE_OFFSET(StructType,rbnode)  FSAP_OFFSETOF(StructType,rbnode)
#define MSR_FAILURE_OID_LEN       10
/*SNMP nessage queue name added*/
#define MSR_SNMPQ_NAME   "MSRSNMPQ"

/* MSR invalid context id */
#define MSR_INVALID_CNTXT         -1

typedef struct SnmpUpdatedata {
    UINT1   *pOid;
    tSnmpIndex             *pSnmpindx;
    tSNMP_MULTI_DATA_TYPE  *pSnmpData;
    BOOL1                isRowStatusPresent;
    UINT1                   u1IsDataToBeSaved;
                                            /* Flag to indicate whether the
                                             * needs to be set to non-volatile
                                             * storage.
                                             * MSR_TRUE  - Needs to be stored
                                             * MSR_FALSE - Should not be stored
                                             */
    UINT1                   au1Reserved[2];
} tMSRUpdatedata;

/*Data Structure used to store ISS Control messages*/
typedef struct ISSCTRLMSG {
    UINT4 u4MsgType;
    union {
        BOOLEAN bHttpEnable;
    } uMsg;
    UINT1     au1Pad[3];
} tISSCtrlMsg;

/*Data Structure used to store ISS control socketid*/
typedef struct ISSGLBCFG {
    INT4      i4CliCtrlSockFd;
    BOOLEAN   bCliCtrlSockCreated;
    UINT1     au1Pad[3];
} tISSGlbCfg;

INT4 
issSnmpInitParams (BOOL1 initParams);
/* Truth Value */
typedef enum {
   SNMP_ROLLBACK_DISABLED = 1,
   SNMP_ROLLBACK_ENABLED = 2
}tRollbackFlag;

/* Filter Action - This is currently used by DCBX - Application Priority
 * Module */
typedef enum {
    ISS_FILTER_CREATE = 1,
    ISS_FILTER_UPDATE,
    ISS_FILTER_DELETE
}tIssAclFilterAction; 

/*Data Structure used to store the DCBx Application Priority entries 
 * needed for ACL module */
typedef struct _IssAclHwFilterInfo {
    UINT4 u4FilterId;   /* This filter id will be returned from ACL module */
    UINT4 u4Protocol;   /* Protocol Id passed for L3 filter from App Pri 
                           module */
    UINT4 u4PortNo;     /* Port No passed for L2 filter from App Pri
                           module */
    UINT4 u4L3PortNo;     /* Port No passed for L3 filter from App Pri 
                           module */
    UINT1 u1Priority;   /* Priority to be mapped with queue in QoS module */
    UINT1 u1FilterAction; /* This indicates whether it is a new filter created 
                           or if it is an existing filter being updated.
                           This will be updated in ACL module. */
    UINT1 u1TagType;  /*Single or Double Tag */
    UINT1 au1Padding[1]; /* Padding for the structure */
} tIssAclHwFilterInfo;
typedef struct _ShowRunningConfigRegParams
{
    /* Show Running Config  notification  */
 INT4     (*pShowRunningConfig) (INT4,INT4); 
    /* Registration flag - Will be set after registration */
 UINT1   bFlag;    
 UINT1     au1Pad [3]; /* Padding for the structure */

}tShowRunningConfigRegParams;
typedef  struct  _IssRegInfo
{
    INT4 (*pHostNmeChng) (UINT1 *pHstNme);
                               /* Call back function */
    UINT1       u1InfoMask;
    /* This mask can take the following values */
    #define     ISS_HOSTNME_CHG_REQ               1

    UINT1       u1ProtoId;      /* Registering Protocol's Identifier */
    INT1        ai1Align[2];    /* Padding for alignment */
}tIssRegInfo;

tIssL2FilterEntry *
IssGetL2FilterTableEntry (INT4 i4IssL2FilterNo);

tIssL2FilterEntry *
IssExtGetL2FilterTableEntry (INT4 i4IssL2FilterNo);

tIssL3FilterEntry *
IssGetL3FilterTableEntry (INT4 i4IssL3FilterNo);

tIssL3FilterEntry *
IssExtGetL3FilterTableEntry (INT4 i4IssL3FilterNo);

INT4
IssUpdateFilterRefCount (UINT4 u4FilterId, UINT4 u4FilterType, UINT4 u4Action);

INT4
IssExtUpdateFilterRefCount (UINT4 u4FilterId, UINT4 u4FilterType, UINT4 u4Action);

INT4 IssSetModuleSystemControl (INT4 i4ModuleId, INT4 i4State);
INT4 IssGetCsrRestoreFlag (VOID);
INT4 IssGetModuleSystemControl (INT4 i4ModuleId);
INT4 IssGetCsrRestoresFlag (INT4 i4ModuleId);


UINT4 IssGetSwitchIdFromNvRam PROTO ((VOID));
UINT4 IssGetColdStandbyFromNvRam PROTO ((VOID));

UINT4 IssGetUserPreferenceFromNvRam PROTO ((VOID));

VOID  IssSetSwitchIdToNvRam PROTO ((UINT4 ));

VOID  IssSetUserPreferenceToNvRam PROTO ((UINT4 ));
VOID  IssSetColdStandbyToNvRam PROTO ((UINT4));

UINT4
IssGetMemInfo PROTO ((VOID));

UINT4
IssGetCPUInfo PROTO ((VOID));

UINT4
IssGetFlashInfo PROTO ((VOID));

INT4  IssReadNodeidFile (UINT1 *pu1FileName, UINT1 *pu1SearchStr,
                                             UINT1 *pu1StrValue);
INT4  IssReadNodeid (VOID);
VOID  IssWriteNodeidFile (VOID);
INT4  IssGetSwitchid (VOID);
INT4  IssGetUserPreference (VOID);
VOID  IssSetUserPreference (UINT4 u4Priority);
INT4  IssGetIssuStartupMode PROTO ((VOID));
VOID  IssSetIssuStartupMode PROTO ((UINT1));
INT4  IssGetIssuMode PROTO ((VOID));
VOID  IssSetIssuMode PROTO ((UINT4));
INT4  IssGetIssuNodeRole PROTO ((VOID));
VOID  IssSetIssuNodeRole PROTO ((UINT1));
UINT1* IssGetRollBackIssVersion PROTO((VOID));
VOID  IssSetRollBackIssVersion PROTO ((UINT1[]));
UINT4 IssGetIssuLastUpgradeTime PROTO ((VOID));
VOID  IssSetIssuLastUpgradeTime PROTO ((UINT4));
UINT1* IssGetRollBackSwPath PROTO ((VOID));
VOID  IssSetRollBackSwPath PROTO ((UINT1 []));
UINT1* IssGetCurrentSwPath PROTO ((VOID));
VOID  IssSetCurrentSwPath PROTO ((UINT1 []));
VOID  IssSetSwitchid (UINT4 u4Switchid);
VOID  IssSetRMIpAddress PROTO ((UINT4 u4RmIpAddress));
INT4  IssGetRMIpAddress PROTO ((VOID));
INT4  IssGetRMSubnetMask PROTO ((VOID));
VOID  IssSetRMSubnetMask PROTO ((UINT4 u4RmSubnetMask));
UINT1* IssGetRMStackInterface PROTO ((VOID));
VOID   IssSetRMStackInterface PROTO ((UINT1 * pu1IssRmStackInterface));
VOID IssUtlConvertMaskToPrefix PROTO ((UINT4 u4Mask, UINT4 *pu4Prefix));
VOID IssUtlConvertPrefixToMask PROTO ((UINT4 u4Prefix, UINT4 *pu4Mask));
INT1 IssSetL3FilterAddrType PROTO ((INT4 i4IssL3FilterNo, INT4 i4AddrType));
INT1
IssSetL3FilterDstPrefixLength PROTO ((INT4 i4IssL3FilterNo, INT4 i4DstPrefixLength));
INT1
IssSetL3FilterSrcPrefixLength PROTO ((INT4 i4IssL3FilterNo, INT4 i4SrcPrefixLength));
INT1
IssSetL3FilterControlFlowId PROTO ((INT4 i4IssL3FilterNo, UINT4 u4FlowId));
UINT1
IssGetL3FilterAddrType PROTO ((UINT4 u4IssL3FilterNo, INT4 *pElement));
UINT1
IssGetL3FilterSrcPrefixLength PROTO ((INT4 i4IssL3FilterNo, INT4 *pElement));
INT1 IssGetPortANCapBits PROTO ((INT4, INT4 *));
UINT1
IssGetL3FilterDstPrefixLength PROTO ((INT4 i4IssL3FilterNo, INT4 *pElement));
UINT1
IssGetL3FilterStorageType PROTO ((INT4 i4IssL3FilterNo, UINT4 *pElement));
INT1
IssSetL3FilterStorageType PROTO ((INT4 i4IssL3FilterNo, INT4 i4StorageType));
UINT1
IssGetL3FilterControlFlowId PROTO ((INT4 i4IssL3FilterNo, UINT4 *pElement));
INT4 IssMirrAddRemovePort PROTO((UINT4 u4IfIndex, UINT4 u4AggId,
         UINT1 u1MirrCfg));
INT1 nmhTestv2IssFrontPanelPortCount ARG_LIST((UINT4 *  ,INT4 ));
INT1 nmhSetIssFrontPanelPortCount ARG_LIST((INT4 ));
INT4 AclStart(VOID);
INT4 AclShutdown(VOID);
UINT1 AclCheckDenyRule PROTO ((UINT2 VlanId, tMacAddr MacAddr, UINT1 MacType));
VOID IssCpssHwReset PROTO((VOID));
VOID IssUpdatePortLinkStatus PROTO ((UINT4 u4IfIndex, UINT1 u1IfType, UINT1 u1OperStatus));
UINT1 IssGetVrfUnqMacOptionFromNvRam PROTO ((VOID));
VOID  IssSetVrfUnqMacOptionToNvRam PROTO ((UINT1));
INT4 IssACLCreateFilter PROTO ((tAclFilterInfo *, 
                                UINT4 *, UINT4 *, UINT4 *));
VOID IssACLDeleteFilter PROTO ((UINT1, UINT4, UINT4, UINT4));
INT4 IssACLApiModifyFilterEntry PROTO ((tIssAclHwFilterInfo *pAclFilterEntry,
                                        UINT1 u1FilterType, 
                                        UINT1 u1Status));
INT4 IssACLApiGetFilterEntry PROTO ((tIssAclHwFilterInfo *pAclFilterEntry,
                                        UINT1 u1FilterType, 
                                        UINT1 u1Status));
VOID IssACLApiValidateL2AclConfigFlag PROTO ((INT4 i4L2AclFilterId, BOOL1 *));
VOID IssACLApiValidateL3AclConfigFlag PROTO ((INT4 i4L3AclFilterId, BOOL1 *));

INT4 IssRedInit PROTO((VOID));
VOID IssRedDeInit PROTO((VOID));

INT4 IssGetLocalDirSize PROTO ((CONST CHR1 *, UINT4 *));
INT4 issDeleteLocalFile PROTO((UINT1 *pu1Src));
INT4 IssDeleteLocalDir PROTO ((CONST CHR1 *));
INT4 IssGetFipsOperModeFromNvRam PROTO((VOID));
VOID IssSetFipsOperModeToNvRam PROTO((INT4 i4FipsOperMode));
UINT1* IssGetSwitchMacFromNvRam PROTO ((VOID));
INT4 IssGetWebMaxSession PROTO ((VOID));
INT4 IssGetWebMaxSessionTimeOut PROTO ((VOID));
UINT4 IssGetHeartBeatModeFromNvRam PROTO ((VOID));
UINT4 IssGetRedundancyTypeFromNvRam PROTO ((VOID));
UINT4 IssGetRmDataPlaneFromNvRam PROTO ((VOID));
VOID IssInitSystemParams PROTO (( VOID ));
VOID L2tpUtilClearConfigAllTables PROTO (( VOID ));
INT4 IssCPUControlledLearning PROTO ((UINT2 u2Port));
INT4 IssGetRMTypeFromNvRam PROTO ((VOID));

/* Prototypes for CLI based Save and Restore */
INT4 IssGetRestoreTypeFromNvRam PROTO ((VOID));
VOID IssSetRestoreTypeToNvRam PROTO ((UINT1 u1RestoreType));

/* Prototypes for Peer Message Exchanges*/
VOID IssProcessPktFromCfa PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf));

/* Prototypes for Filters */
INT4 IssAclInstallL2Filter (tIssL2FilterEntry *pIssL2Filter);
INT4 IssAclUnInstallL2Filter (tIssL2FilterEntry *pIssL2Filter);
INT4 IssAclInstallL3Filter  (tIssL3FilterEntry *pIssL3Filter);
INT4 IssAclUnInstallL3Filter (tIssL3FilterEntry *pIssL3Filter);
INT4 IssACLInstallFilter (UINT4 u4FilterType,
                          tIssL2FilterEntry *pIssL2Filter,
                                  tIssL3FilterEntry *pIssL3Filter);
INT4 IssAclQosProcessL2Proto (VOID);
INT4 IssAclQosProcessL3Proto (VOID);
INT4 IssAclQosProcessL2L3Proto (VOID);
INT4 IssAclUninstallFilter (UINT4  u4FilterType,
                            tIssL2FilterEntry *pIssL2Filter,
                                   tIssL3FilterEntry *pIssL3Filter);
INT4 IssAclQosDeleteL2L3Filter (VOID);
INT4 IssAclInstallUserDefFilter (tIssUserDefinedFilterTable *);
INT4 IssAclUnInstallUserDefFilter (tIssUserDefinedFilterTable *);
INT4 IssAclUpdateL2FilterEntry (tAclFilterInfo *, UINT4);
INT4 IssAclUpdateL3FilterEntry (tAclFilterInfo *, UINT4);
INT4 IssAclUpdateUDBFilterEntry (tAclFilterInfo *, UINT4, UINT4, UINT4);
VOID IssAclGetValidL2FilterId (UINT4 *);
VOID IssAclGetValidL3FilterId (UINT4 *);
VOID IssAclGetValidUDBFilterId (UINT4 *);
INT4 IssGetStackingModel PROTO (( VOID ));
INT4 IssIsClearConfigInProgress PROTO ((VOID));
INT4 IssExtValidateRateCtrlEntry (INT4 i4RateCtrlIndex);
INT4 IssValidateSubnetMask PROTO ((UINT4 u4SubnetMask));
VOID
IssPrintStrIpAddress (UINT1 *pIpAddr, UINT1 *pu1Temp);
#ifdef NPAPI_WANTED
INT1 IssSetPortCtrlDuplex (INT4, INT4 );
INT1 IssSetPortCtrlSpeed (INT4, INT4 );
#endif
VOID IpMgrBlockTableInsert(UINT4 u4IpAddr, UINT4 u4Port);
VOID IpMgrBlockTableDelete(UINT4 u4IpAddr, UINT4 u4Port);
VOID IssSysConvertCfaSpeedToIssSpeed PROTO ((UINT4 u4Speed, UINT4 u4HiSpeed, INT4 * pu4PortCtrlSpeed));
INT4 AclValidateMemberPort (UINT4 u4IfIndex);
INT4 IssAclVlanSetSChFilterStatus (UINT4 u4SChIfIndex,UINT4 u4RetValFsMIEvbSChannelFilterStatus);
UINT1 IssGetHwCapabilities (UINT1 u1HwSupport);
VOID IssInitPortSpeedParams (VOID);
VOID
IssPIUtilUpdatePIEntry PROTO ((UINT2 u2IfIndex, UINT2 u2AggId, UINT1   u1Action ));
INT4 AclValidateIfMemberoOfPortChannel  PROTO  ((UINT4  u4IfIndex));
INT4 AclValidatePortFromPortChannel PROTO ((UINT4  u4IfIndex));
PUBLIC VOID IssRegisterProtocol (tIssRegInfo * pIssRegInfo);
PUBLIC VOID IssDeRegisterProtocol (VOID);
INT4 IssPortGetMirrDestPortForSrcPort (UINT4, UINT4*);
/* Prototypes used for updating Secondary Linux OOB */
UINT4 IssGetSecIpAddrFromNodeId PROTO ((VOID));
VOID  IssGetSecIp6AddrFromNodeId PROTO ((tIp6Addr *));
UINT4 IssGetSecSubnetMaskFromNodeId PROTO ((VOID));
UINT1 IssGetSecIp6PrefixLenFromNodeId PROTO ((VOID));
VOID IssSetSecIpAddrForNodeId PROTO ((UINT4));
VOID IssSetSecIp6AddrForNodeId PROTO ((tIp6Addr *));
VOID IssSetSecSubnetMaskForNodeId PROTO ((UINT4));
VOID IssSetSecIp6PrefixLenForNodeId PROTO ((UINT1));
INT4 IssSetSyslogFileSize PROTO ((INT4));
INT4 IssGetSyslogFileSize PROTO ((VOID));


#endif /* _ISS_H */

