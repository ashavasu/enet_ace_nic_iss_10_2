/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: sshfs.h,v 1.27 2016/03/12 10:44:43 siva Exp $
 *
 * Description: This has interface macros and prototypes for ssh. 
 *
 ***********************************************************************/

#ifndef _SSH_FS_H_
#define _SSH_FS_H_

/* Macro definitions used by both CLI & Low levels */

/* Trace level values */
#define SSH_INIT_SHUT_TRC_MASK          0x00000001
#define SSH_MGMT_TRC_MASK               0x00000002
#define SSH_DATA_PATH_TRC_MASK          0x00000004
#define SSH_CNTRL_PLANE_TRC_MASK        0x00000008
#define SSH_DUMP_TRC_MASK               0x00000010
#define SSH_OS_RESOURCE_TRC_MASK        0x00000020
#define SSH_ALL_FAILURE_TRC_MASK        0x00000040
#define SSH_BUFFER_TRC_MASK             0x00000080
#define SSH_DISABLE_ALL_TRC_MASK        0x00000000
#define SSH_SERVER_HANDLE_MASK          0x00000100
#define SSH_ENABLE_ALL_TRC_MASK         (SSH_INIT_SHUT_TRC_MASK   |\
                                         SSH_MGMT_TRC_MASK        |\
                                         SSH_DATA_PATH_TRC_MASK   |\
                                         SSH_CNTRL_PLANE_TRC_MASK |\
                                         SSH_DUMP_TRC_MASK        |\
                                         SSH_OS_RESOURCE_TRC_MASK |\
                                         SSH_ALL_FAILURE_TRC_MASK |\
                                         SSH_BUFFER_TRC_MASK      |\
                                         SSH_DISABLE_ALL_TRC_MASK |\
                                         SSH_SERVER_HANDLE_MASK)


#define SSH_ENABLE                      1
#define SSH_DISABLE                     2


#define SSH_SERVER_WAKEUP_PORT          6125

#define SSH_SUCCESS                     0
#define SSH_FAILURE                     -1
#define SSH_TRUE                        1
#define SSH_FALSE                       0

#define SSH_VERSION_1               0x0001
#define SSH_VERSION_2               0x0002

#define SSH_DEF_PORT                    22
#define SSH_3DES_CBC                    0x0001
#define SSH_DES_CBC                     0x0002
#define SSH_AES_CBC_128   0x0004
#define SSH_AES_CBC_256          0x0008
#define MAX_SSH_CIPHER_LIST  (SSH_3DES_CBC | SSH_DES_CBC | \
                                         SSH_AES_CBC_128 | SSH_AES_CBC_256)

#define SSH_HMAC_SHA1                   0x0001
#define SSH_HMAC_MD5                    0x0002
#define SSH_CALLBACK_FN               gaSshCallBack
#define MAX_SSH_CONNECTIONS                  8

#define SSH_MAX_PORT_NUMBER  65535

/* To Initialise SSH Global Variables */
VOID SshArInit PROTO ((tOsixTaskId TaskId));

/* To Enable & Disable  SSH  */
INT4 SshArEnable PROTO((VOID));

INT4 SshArDisable PROTO((VOID));

/* Returns the SSH Listen Socket Descriptor */
INT4 SshArStartListener PROTO((VOID));

VOID SshArSendTaskId PROTO((INT4 i4ConnIndex, tOsixTaskId TaskId));

/* Starts SSH Negotiations */
VOID SshArSessionStart PROTO((INT4 i4ConnIndex, tOsixTaskId TaskId, UINT1 *pu1TaskName,UINT1 * u1PubUsrName));

/* Returns the Socket Descriptor of the Accept Call */
INT4 SshArAccept PROTO((INT4 SockFd,INT4 i4Family));

INT4 SshArGetServSockId PROTO((VOID));

INT4 SshArGetServ6SockId PROTO((VOID));

VOID SshArUpdateSessionTimeOut PROTO((INT4 i4ConnIndex, INT4 i4SessionTimeOut));

/*Returns the socket descriptor for the given connection index */
INT4 SshArGetSockfd PROTO((INT4 i4ConnIndex));

/* For a given connection, i4ConnIndex, returns the Character Read */
UINT1 SshArRead PROTO((INT4 i4ConnIndex));

/* Writes the Message to the Remote Console */
INT4 SshArWrite PROTO((INT4 i4ConnIndex, UINT1 *pMessage, UINT4 u4Len));

INT4 SshArConnectPassiveSocket PROTO ((VOID));

INT4 SshArConnectPassiveV6Socket PROTO ((VOID));

VOID SshArSetVersionCompatibility PROTO((UINT1 u1VersionCompatibility));

INT1 SshArSetCipherAlgoList(UINT2 u2CipherAlgos);

INT1 SshArSetMacAlgoList(UINT2 u2MacAlgos);

VOID
SshArGetVersionCompatibility (UINT1 * pu1VersionCompatibility);

INT1
SshArGetCipherAlgoList (UINT2 * u2CipherAlgos);


INT1
SshArGetMacAlgoList (UINT2 * u2MacAlgos);

VOID
SshArGetSshTrace (UINT2 * );

VOID
SshArSetSshTrace (UINT2 u2Trace);

VOID
SshArGetSshServerStatus (UINT1 * pu1ServerStatus);

VOID
SshArSetSshServerStatus (UINT1 u1ServerStatus);
VOID
SshArUpdateMaxPktLen(INT4 i4AlloweByte);
VOID
SshArGetSshPacketByte(UINT2 *pu2ReciveByte);


VOID SshArTearDownConnection (INT4 i4ConnIndex);

/* Closes a Connection referred to by i4ConnIndex */

VOID SshArClose (INT4 i4ConnIndex);

INT4 SshArCreateDummySocket (VOID);

/* Deletes the CLI Context Related to SSH */
INT4                CliDeleteSshContext PROTO((UINT1 *pu1TaskName));
INT4                CliDeleteSshContextByTaskId (tOsixTaskId TaskId);
VOID                CliClearSshTaskId(VOID);

VOID SshArKeyGen (tOsixTaskId TaskId);

INT4 SshArReadServerKey (VOID);


INT4 SshRegisterCallBackforSSHModule (UINT4 u4Event, tFsCbInfo * pFsCbInfo);

typedef enum
{
    SSH_CUST_MODE_CHK = 1,
    SSH_CUST_SERVER_KEYS_SAVE_PROCESS,
    SSH_CUST_SERVER_PUBLIC_KEY,
    SSH_CUST_SRAM_READ_PUBLIC_KEY,
    SSH_MAX_CALLBACK_EVENTS
}eSshCallBackEvents;

typedef union SshCallBackEntry
{
    INT4 (* pSshCustCheckCustMode) (VOID );
    INT4 (* pSshCustSaveServerKeys) (UINT4, UINT4, ...);
    INT4 (* pSshCustSavePublicKey) (UINT4, UINT4, ...);
    INT4 (* pSshCustSramReadPublicKey) (INT1 *);
}unSshCallBackEntry;

extern unSshCallBackEntry          gaSshCallBack[SSH_MAX_CALLBACK_EVENTS];

VOID SSHDeleteKeys PROTO((VOID));

#define SSH_KEY_SAVE_FILE_1              FLASH "server_key_1"
#define SSH_KEY_SAVE_FILE_2              FLASH "server_key_2"

#endif
