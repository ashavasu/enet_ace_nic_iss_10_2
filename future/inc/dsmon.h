/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 *
 * Description: This file contains the DSMON function prototypes
 *******************************************************************/
#ifndef _DSMON_H_
#define _DSMON_H_
PUBLIC VOID DsmonMainTaskMain ARG_LIST((INT1 *));
VOID DsmonCfaIfaceUpdateChgs ARG_LIST ((UINT4 u4IfIndex, UINT1 u1OperStatus));
#endif
