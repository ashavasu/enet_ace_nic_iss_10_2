
/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: pppnpwr.h,v 1.2 2014/10/28 11:11:41 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              for PPP wrappers
 *              
 ********************************************************************/
#ifndef __PPP_NP_WR_H__
#define __PPP_NP_WR_H__

#include "ipnp.h"

UINT1 PppFsNpIpv4ArpAdd PROTO ((tNpVlanId u2VlanId, UINT4 u4IfIndex, UINT4 u4IpAddr, UINT1 *pMacAddr, UINT1 *pu1IfName, INT1 i1State, UINT4 *pbu4Tb1Full));
UINT1 PppFsNpIpv4ArpDel PROTO ((UINT4 u4IpAddr, UINT1 *pu1IfName, INT1 i1State));

#endif /* __PPP_NP_WR_H__ */
