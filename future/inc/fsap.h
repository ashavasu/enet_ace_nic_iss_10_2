/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsap.h,v 1.35 2017/12/07 10:07:34 siva Exp $
 *
 * Description:This file contains the macros for fsap, which can be diffrent
 * for diffrent set of modules.
 *
 *******************************************************************/
#ifndef _FSAP_H_
#define _FSAP_H_

/* If the default stack size is less than 16000, pthread
 * library will allocate stack size of 2044000 */

#define OSIX_PTHREAD_MIN_STACK_SIZE         16000 
#define OSIX_PTHREAD_DEF_STACK_SIZE         2094000

#define U1MAX   254
#ifdef ISS_METRO_WANTED
/* SEM count for metro system:
 * Approximately 1521 Semaphores are used by protocols while 
 * bringing up ISS for metro package.
 * Approximately around 27 sems are used per context.
 * 4 sems are used per Customer Edge Port (in Provider edge bridge) and
 * 1 sem per port -- for GVRP.
 * So considering 1000 ports, 255 contexts, 100 CEPs the caculation is:
 * 1521 + 6885(27 * 255) + (4 * 100) + 1000 = Approx 11000
 */
#define   OSIX_MAX_SEMS                     11000

/* Mempools count for metro system:
 * Default count (400) + 100 CEP in system * 4 ( each CEP needs 4 mempools)
 * 400 + 100 * 4 = 800
 */
#define   SYS_MAX_MEMPOOLS                   1800
#else

#define   OSIX_MAX_SEMS                     11000
#define   SYS_MAX_MEMPOOLS                  1550

#endif
#define   MAX_RBTREE_INST  10000

/* This is the timer used for TimedWait Sema4 Locks (in seconds) */
#define  OSIX_MAX_WAIT_FOR_SEM_LOCK  6

#if defined (RM_WANTED) && defined (L2RED_WANTED)
/* In the L2 redundancy solution, we are spawning certain tasks dynamically
 * to do some low priority job. These tasks are used to do the following
 *   1. Hardware Audit - Seperate task for each protocol having redundancy
 *                       support during standby to active node
 *                       transition :- (Approx) 5
 *   2. File transfer - Seperate task will be used to transfer the ISS 
 *                      system level files from active to standby node.
 *                      For e.g. issnvram.txt,privil,users,
 *                      SlotModule.conf,etc. :- (Approx) 6
 */

/* OSIX_MAX_TSKS macro has ben increased inorder to provide 
 * LINUX-VRF support.*/
#define  OSIX_MAX_TSKS          250
#else
#ifdef RM_WANTED
#define  OSIX_MAX_TSKS          350 /* no of task increased to provide
                                       ColdStandbySupport */
#else
#define  OSIX_MAX_TSKS          240
#endif
#endif

#ifdef RM_WANTED
#define UTL_GET_LOG_FILE_NAME(au1LogFile)  SPRINTF ((CHR1 *) au1LogFile, "fsir.log")
#else
#define UTL_GET_LOG_FILE_NAME(au1LogFile)  SPRINTF ((CHR1 *) au1LogFile, "fsir.log.%d", getpid ())
#endif

/* Following macros are used in utltrc.c for printing the Trace message */
#ifndef NPSIM_WANTED
#define UTL_TRC_LOG_MSG(pi1Name, ai1LogMsgBuf) \
        SYS_LOG_TRC(pi1Name,ai1LogMsgBuf); \
        UtlTrcPrint ((const char *) ai1LogMsgBuf);
#else
#define UTL_TRC_LOG_MSG(pi1Name, ai1LogMsgBuf) \
        UtlTrcPrint ((const char *) ai1LogMsgBuf);
#endif

#ifndef NPSIM_WANTED
#define UTL_SYS_TRC_LOG_MSG(u4SysLevel, pi1Name, ai1LogMsgBuf) \
        SYS_LOG_TRC1(u4SysLevel,pi1Name,ai1LogMsgBuf); \
        UtlTrcPrint ((const char *) ai1LogMsgBuf);
#else
#define UTL_SYS_TRC_LOG_MSG(u4SysLevel, pi1Name, ai1LogMsgBuf)
#endif

#define UTL_TRC_LOG_ADD_MOD_NAME(pi1Name, pos,  ai1LogMsgBuf) {\
        time_t              t;\
        struct tm          *tm;\
    /* If the message is to be directed to syslog, there is no
     * need to add the module name
     */ \
    if (gu4InCoreLog != SYSLOG) \
    { \
        /* Set the current wall clock time. and the Module Name.
         * To save on calls to sprintf, try to push all parameters
         * into one single sprintf call whenever possible.
         */\
        if (gu4ShowTime == ISS_DEBUG_TIMESTAMP_ENABLED)\
        {\
            t = time (NULL);\
            tm = localtime (&t);\
            if (tm != NULL)\
            {\
                pos +=\
                   SNPRINTF (ai1LogMsgBuf, MAX_LOG_STR_LEN,\
                             "%d:%d:%d: %d:%d:%d:%s ", tm->tm_hour, tm->tm_min,\
                             tm->tm_sec, tm->tm_mday, tm->tm_mon + 1,\
                             tm->tm_year + 1900, pi1Name);\
            }\
        }\
        else\
        {\
            /* Time not needed, Tag the Module Name alone.
            * Idea is if module name is null, we don't want the ':'  */\
            if (strcmp (pi1Name, ""))\
            {\
                pos += SPRINTF (ai1LogMsgBuf + pos, "%s: ", pi1Name);\
            }\
        }\
    } \
}
/* Following macros are used in utltrc.c for printing the Trace message */
#define  DECL_FLOWDATA()
#define  SET_SYSTEM_TIME(tm) {\
    (void) tm;\
}        
/* Set RHS to FSAP_ON, to make unaligned memory accesses safe.    */
#define ALIGN_SAFE              FSAP_OFF
#endif /*End of _FSAP_H_ */
