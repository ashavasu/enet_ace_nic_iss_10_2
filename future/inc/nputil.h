    /********************************************************************
     * Copyright (C) 2012 Aricent Inc . All Rights Reserved
     *
     * $Id: nputil.h,v 1.46 2018/02/13 09:11:17 siva Exp $
     *
     * Description:This file contains the necessary data strucutures
     *             for packet handling in NPAPI layer
     *             <Part of dual node stacking model>
     *
     *******************************************************************/
    #ifndef __NP_UTIL_H__
    #define __NP_UTIL_H__

    #include "lr.h"
    #include "osxstd.h"
    #include "srmbuf.h"
    #include "osxsys.h"
    #include "utlmacro.h"
    #include "cfa.h"
    #ifdef NPAPI_WANTED
    #include "npapi.h"
    #endif

    /* Module Includes */
    #include "vlannpwr.h"
    #include "mstpnpwr.h"
    #include "rstpnpwr.h"
    #include "pvrstnpwr.h"
    #include "lanpwr.h"
    #include "pnacnpwr.h"
    #include "vcmnpwr.h"
    #include "rmonnpwr.h"
    #include "rmn2npwr.h"
    #include "igsnpwr.h"
    #include "mldsnpwr.h"
    #include "dsmnnpwr.h"
    #include "cfanpwr.h"
    #include "eoamnpwr.h"
    #include "eoamfmnpwr.h"
    #include "issnpwr.h"
    #include "elpsnpwr.h"
    #include "ecfmnpwr.h"
    #include "erpsnpwr.h"
    #include "qosxnpwr.h"
    #include "lanpwr.h"
    #include "rmnpwr.h"
    #include "mbsmnpwr.h"
    #include "syenpwr.h"
    #include "ofcnpwr.h"
    #include "ipnpwr.h"
    #include "ospfnpwr.h"
    #include "dhcpsnpwr.h"
    #include "dhrlnpwr.h"
    #include "cfanpwr.h"
    #include "arpnpwr.h"
    #include "vrrpnpwr.h"
    #include "ripnpwr.h"
    #include "mplsnpwr.h"
    #include "pppnpwr.h"
    #include "natnpwr.h"
    #include "ipv6npwr.h"
    #include "pimnpwr.h"
    #include "ospf3npwr.h"
    #include "rip6npwr.h"
    #include "ipmcnpwr.h"
    #include "igmpnpwr.h"
    #include "mldnpwr.h"
    #include "ip6mcnpwr.h"
    #include "fsbnpwr.h"
    #include "vxlannp.h"

    #define NPUTIL_TRUE                   1
    #define NPUTIL_FALSE                  0

    #ifdef RM_WANTED
    #define  NPUTIL_IS_NP_PROGRAMMING_ALLOWED() \
            ((RmRetrieveNodeState () == RM_STANDBY) ? NPUTIL_FALSE: NPUTIL_TRUE)
    #else
    #define  NPUTIL_IS_NP_PROGRAMMING_ALLOWED() NPUTIL_TRUE
    #endif /* RM_WANTED */

    #if defined (WLC_WANTED) || defined (WTP_WANTED)
    #include "wssmac.h"
    #endif
    #ifdef WTP_WANTED
    #include "capwaphwnpwr.h"
    #include "wsswlanhwnpwr.h"
    #include "wssstahwnpwr.h"
    #include "radioifhwnpwr.h"
    #endif

    /* Module ID */
    typedef enum {
     NP_VLAN_MOD = 1,
     NP_MSTP_MOD,
     NP_RSTP_MOD,
     NP_PVRST_MOD,
     NP_LA_MOD,
     NP_PNAC_MOD,
     NP_VCM_MOD,
     NP_RMON_MOD,
     NP_RMONv2_MOD,
     NP_IGS_MOD,
     NP_MLDS_MOD,
     NP_DSMON_MOD,
     NP_CFA_MOD,
     NP_EOAM_MOD,
     NP_EOAMFM_MOD,
     NP_ISSSYS_MOD,
     NP_ELPS_MOD,
     NP_ECFM_MOD,
     NP_ERPS_MOD,
     NP_QOSX_MOD,
     NP_RM_MOD,
     NP_MBSM_MOD,
     NP_SYNCE_MOD,
     NP_OFC_MOD,
     NP_IP_MOD,
     NP_CAPWAP_MODULE,
     NP_WLAN_MODULE,
     NP_WLAN_CLIENT_MODULE,
     NP_RADIO_MODULE,
     NP_IPV6_MOD,
     NP_MPLS_MOD,
     NP_IGMP_MOD,
     NP_IPMC_MOD,
     NP_MLD_MOD,
     NP_IP6MC_MOD,
     NP_FSB_MOD,
     NP_VXLAN_MOD,
     NP_MAX_MOD
    }tNpModule;

    /* Generic NP Param Structure */

    typedef struct __FS_HARDWARE_NP__ {
     tNpModule NpModuleId;          /* Module ID */
     UINT4     u4Opcode;            /* Opcode for specifying the NP call */
     UINT4     u4Type;              /* Reserved for future use */
     UINT4     u4IfIndex;           /* Interface Index to be used for 
                                       Interface based APIs. Otherwise set 0 */
     UINT4     u4NumPortsParams;    /* No. of parameters which are IfIndex */
     UINT4     u4NumPortlistParams; /* No. of parameters which are Portlists */

     /* The Ports and Port Lists should always be defined as the 
      * first set of parameters in all the NPAPI structure */

     union {
    #ifdef VLAN_WANTED
      tVlanNpModInfo    VlanNpModInfo;  /* vlan specific h/w parameters */
    #endif /* VLAN_WANTED */
    #ifdef MSTP_WANTED 
      tMstpNpModInfo    MstpNpModInfo;  /* mstp specific h/w parameters */
    #endif /* MSTP_WANTED */
    #ifdef RSTP_WANTED
      tRstpNpModInfo    RstpNpModInfo;  /* rstp specific h/w parameters */
    #endif /* RSTP_WANTED */
    #ifdef PVRST_WANTED 
      tPvrstNpModInfo   PvrstNpModInfo;  /* pvrst specific h/w parameters */
    #endif /* PVRST_WANTED */
    #ifdef LA_WANTED
      tLaNpModInfo      LaNpModInfo;  /* la specific h/w parameters */
    #endif /* LA_WANTED */
    #ifdef PNAC_WANTED 
      tPnacNpModInfo    PnacNpModInfo;  /* pnac specific h/w parameters */
    #endif /* PNAC_WANTED */
    #ifdef VCM_WANTED 
      tVcmNpModInfo     VcmNpModInfo;  /* vcm specific h/w parameters */
    #endif /* VCM_WANTED */
    #ifdef RMON_WANTED
      tRmonNpModInfo    RmonNpModInfo;  /* rmon specific h/w parameters */
    #endif /* RMON_WANTED */
    #ifdef RMON2_WANTED
      tRmonv2NpModInfo  Rmonv2NpModInfo;  /* rmon2 specific h/w parameters */
    #endif /* RMON2_WANTED */
    #ifdef IGS_WANTED 
      tIgsNpModInfo     IgsNpModInfo;  /* igs specific h/w parameters */
    #endif /* IGS_WANTED */
    #ifdef MLDS_WANTED
      tMldsNpModInfo    MldsNpModInfo;  /* mlds specific h/w parameters */
    #endif /* MLDS_WANTED */
    #ifdef DSMON_WANTED
      tDsmonNpModInfo   DsmonNpModInfo;  /* dsmon specific h/w parameters */
    #endif /* DSMON_WANTED */
    #ifdef CFA_WANTED
      tCfaNpModInfo     CfaNpModInfo;  /* cfa specific h/w parameters */
    #endif /* CFA_WANTED */
    #ifdef EOAM_WANTED
      tEoamNpModInfo   EoamNpModInfo;  /* eoam specific h/w parameters */
    #endif /* EOAM_WANTED */
    #ifdef EOAM_FM_WANTED
      tEoamfmNpModInfo   EoamfmNpModInfo;  /* eoamfm specific h/w parameters */
    #endif /* EOAM_FM_WANTED */
    #ifdef ISS_WANTED
      tIsssysNpModInfo  IsssysNpModInfo;  /* iss specific h/w parameters */
    #endif /* ISS_WANTED */
    #ifdef ELPS_WANTED
      tElpsNpModInfo  ElpsNpModInfo;  /* elps specific h/w parameters */
    #endif /* ELPS_WANTED */
    #ifdef ECFM_WANTED
      tEcfmNpModInfo  EcfmNpModInfo;  /* ecfm specific h/w parameters */
    #endif /* ECFM_WANTED */
    #ifdef ERPS_WANTED
      tErpsNpModInfo  ErpsNpModInfo;  /* erps specific h/w parameters */
    #endif /* ERPS_WANTED */
    #ifdef QOSX_WANTED
      tQosxNpModInfo  QosxNpModInfo;  /* qosx specific h/w parameters */
    #endif /* QOSX_WANTED */
    #ifdef RM_WANTED
      tRmNpModInfo  RmNpModInfo;  /* rm specific h/w parameters */
    #endif /* RM_WANTED */
    #ifdef MBSM_WANTED
      tMbsmNpModInfo  MbsmNpModInfo;  /* mbsm specific h/w parameters */
    #endif /* MBSM_WANTED */
    #ifdef SYNCE_WANTED
      tSynceNpModInfo SynceNpModInfo; /* synce specific h/w parameters */
    #endif
    #ifdef OPENFLOW_WANTED
      tOfcNpModInfo OfcNpModInfo; /* Openflow Client specific h/w parameters */
    #endif
    #if defined (IP_WANTED) || defined (LNXIP4_WANTED) || defined (VRRP_WANTED) || defined (MPLS_WANTED) || defined (NAT_WANTED)
      tIpNpModInfo  IpNpModInfo;  /* ip specific h/w parameters */
    #endif /* IP_WANTED || LNXIP4_WANTED || VRRP_WANTED || MPLS_WANTED || NAT_WANTED */
    #if defined (WLC_WANTED) || (defined WTP_WANTED)
    #ifdef NPAPI_WANTED
    tCapwapNpModInfo  CapwapNpModInfo;  /* CAPWAP specific h/w parameters */
    tWlanNpModInfo  WlanNpModInfo;  /* WLAN specific h/w parameters */
    tWlanClientNpModInfo  WlanClientNpModInfo;  /* Station specific h/w parameters */
    tRadioIfNpModInfo   RadioIfNpModInfo; /* radio specific h/w parameters */
    #endif
    #endif /*(WLC_WANTED || WTP_WANTED ) */
    #if defined (IP6_WANTED) || defined (LNXIP6_WANTED)
      tIpv6NpModInfo  Ipv6NpModInfo;  /* ipv6 specific h/w parameters */
    #endif /* IP6_WANTED || LNXIP6_WANTED */ 

    #if defined MPLS_WANTED
      tMplsNpModInfo    MplsNpModInfo; /*Mpls specific h/w parameters */
    #endif /*(MPLS_WANTED ) */
    #if defined IGMP_WANTED
      tIgmpNpModInfo    IgmpNpModInfo; /*IGMP specific h/w parameters */
    #endif /*(IGMP_WANTED ) */
    #if defined (IP_WANTED) || defined (LNXIP4_WANTED) || defined (PIM_WANTED) || defined (DVMRP_WANTED) || defined (IGMPPRXY_WANTED)
      tIpmcNpModInfo  IpmcNpModInfo;  /* ip multicast specific h/w parameters */
    #endif /* IP_WANTED || LNXIP4_WANTED || PIM_WANTED || DVMRP_WANTED || IGMPPRXY_WANTED*/ 
    #if defined MLD_WANTED
      tMldNpModInfo    MldNpModInfo; /*MLD specific h/w parameters */
    #endif /*(MLD_WANTED ) */
    #if defined (IP6_WANTED) || defined (LNXIP6_WANTED) || defined (PIMV6_WANTED) 
      tIp6mcNpModInfo  Ip6mcNpModInfo;  /* ip multicast specific h/w parameters */
    #endif /* IP6_WANTED || LNXIP6_WANTED || PIMV6_WANTED */ 
    #ifdef FSB_WANTED
      tFsbNpModInfo FsbNpModInfo;
    #endif /* FSB_WANTED */
    #ifdef VXLAN_WANTED
     tVxlanHwInfo VxlanNpModInfo;
    #endif
     }unModuleInfo;

    #ifdef VLAN_WANTED
    #define VlanNpModInfo  unModuleInfo.VlanNpModInfo
    #endif /* VLAN_WANTED */
    #ifdef MSTP_WANTED
    #define MstpNpModInfo  unModuleInfo.MstpNpModInfo
    #endif /* MSTP_WANTED */
    #ifdef RSTP_WANTED
    #define RstpNpModInfo  unModuleInfo.RstpNpModInfo
    #endif /* RSTP_WANTED */
    #ifdef PVRST_WANTED
    #define PvrstNpModInfo unModuleInfo.PvrstNpModInfo
    #endif /* PVRST_WANTED */
    #ifdef LA_WANTED
    #define LaNpModInfo    unModuleInfo.LaNpModInfo
    #endif /* LA_WANTED */
    #ifdef PNAC_WANTED
    #define PnacNpModInfo  unModuleInfo.PnacNpModInfo
    #endif /* PNAC_WANTED */
    #ifdef VCM_WANTED
    #define VcmNpModInfo   unModuleInfo.VcmNpModInfo
    #endif /* VCM_WANTED */
    #ifdef RMON_WANTED
    #define RmonNpModInfo  unModuleInfo.RmonNpModInfo
    #endif /* RMON_WANTED */
    #ifdef RMON2_WANTED
    #define Rmonv2NpModInfo unModuleInfo.Rmonv2NpModInfo
    #endif /* RMON2_WANTED */
    #ifdef IGS_WANTED
    #define IgsNpModInfo   unModuleInfo.IgsNpModInfo
    #endif /* IGS_WANTED */
    #ifdef MLDS_WANTED
    #define MldsNpModInfo  unModuleInfo.MldsNpModInfo
    #endif /* MLDS_WANTED */
    #ifdef DSMON_WANTED
    #define DsmonNpModInfo unModuleInfo.DsmonNpModInfo
    #endif /* DSMON_WANTED */
    #ifdef CFA_WANTED
    #define CfaNpModInfo unModuleInfo.CfaNpModInfo
    #endif /* CFA_WANTED */
    #ifdef EOAM_WANTED
    #define EoamNpModInfo unModuleInfo.EoamNpModInfo
    #endif /* EOAM_WANTED */
    #ifdef EOAM_FM_WANTED
    #define EoamfmNpModInfo unModuleInfo.EoamfmNpModInfo
    #endif /* EOAM_FM_WANTED */
    #ifdef ISS_WANTED
    #define IsssysNpModInfo unModuleInfo.IsssysNpModInfo
    #endif /* ISS_WANTED */
    #ifdef ELPS_WANTED
    #define ElpsNpModInfo unModuleInfo.ElpsNpModInfo
    #endif /* ELPS_WANTED */
    #ifdef ECFM_WANTED
    #define EcfmNpModInfo unModuleInfo.EcfmNpModInfo
    #endif /* Ecfm_WANTED */
    #ifdef ERPS_WANTED
    #define ErpsNpModInfo unModuleInfo.ErpsNpModInfo
    #endif /* ERPS_WANTED */
    #ifdef QOSX_WANTED
    #define QosxNpModInfo unModuleInfo.QosxNpModInfo
    #endif /* QOSX_WANTED */
    #ifdef RM_WANTED
    #define RmNpModInfo unModuleInfo.RmNpModInfo
    #endif /* RM_WANTED */
    #ifdef MBSM_WANTED
    #define MbsmNpModInfo unModuleInfo.MbsmNpModInfo
    #endif /* MBSM_WANTED */
    #ifdef SYNCE_WANTED
    #define SynceNpModInfo unModuleInfo.SynceNpModInfo
    #endif /* SYNCE_WANTED */
    #ifdef OPENFLOW_WANTED
    #define OfcNpModInfo unModuleInfo.OfcNpModInfo
    #endif /* OPENFLOW_WANTED */
    #ifdef MPLS_WANTED
    #define MplsNpModInfo unModuleInfo.MplsNpModInfo
    #endif /* MPLS_WANTED */
    #if defined (IP_WANTED) || defined (LNXIP4_WANTED) || defined (VRRP_WANTED) || defined (MPLS_WANTED) || defined (NAT_WANTED)
    #define IpNpModInfo unModuleInfo.IpNpModInfo
    #endif /* IP_WANTED || LNXIP4_WANTED || VRRP_WANTED || MPLS_WANTED || NAT_WANTED */
    #if (defined WLC_WANTED) || (defined WTP_WANTED)
    #define CapwapNpModInfo unModuleInfo.CapwapNpModInfo
    #define WlanNpModInfo unModuleInfo.WlanNpModInfo
    #define WlanClientNpModInfo unModuleInfo.WlanClientNpModInfo
    #define RadioIfNpModInfo unModuleInfo.RadioIfNpModInfo
    #endif /*(WLC_WANTED || WTP_WANTED ) */
    #if defined (IP6_WANTED) || defined (LNXIP6_WANTED)
    #define Ipv6NpModInfo unModuleInfo.Ipv6NpModInfo
    #endif
    #if defined (IGMP_WANTED) 
    #define IgmpNpModInfo unModuleInfo.IgmpNpModInfo
    #endif
    #if defined (IP_WANTED) || defined (LNXIP4_WANTED) || defined (PIM_WANTED) || defined (DVMRP_WANTED) || defined (IGMPPRXY_WANTED)
    #define IpmcNpModInfo unModuleInfo.IpmcNpModInfo
    #endif /* IP_WANTED || LNXIP4_WANTED || PIM_WANTED || DVMRP_WANTED || IGMPPRXY_WANTED*/ 
    #if defined (MLD_WANTED) 
    #define MldNpModInfo unModuleInfo.MldNpModInfo
    #endif
    #if defined (IP6_WANTED) || defined (LNXIP6_WANTED) || defined (PIMV6_WANTED) 
    #define Ip6mcNpModInfo unModuleInfo.Ip6mcNpModInfo
    #endif /* IP6_WANTED || LNXIP6_WANTED || PIMV6_WANTED */ 
    #ifdef FSB_WANTED
    #define FsbNpModInfo unModuleInfo.FsbNpModInfo
    #endif  /* FSB_WANTED */
    #ifdef VXLAN_WANTED
    #define VxlanNpModInfo unModuleInfo.VxlanNpModInfo
    #endif  /* VXLAN_WANTED */
    }tFsHwNp;
   
    /*maximum stacking port frame size*/
    #define STACKING_MTU_SIZE (16360 - ISS_CUST_NP_BUF_OFFSET)
        
    /* Supported Opcodes/Message Types between 
       Active and Standby  CPU communication in Dual Unit Stacking */
    /* Opcodes are also used to Mention the Hardware Notifications to CPU */
      enum{

         PKT_TX_FROM_CPU= 1,    /* Packet Transmission on a Physical Port
                                    From CPU */
         PKT_RX_TO_CPU,         /* Packet Reception to CPU */
         OPER_STATUS_INDICATION,/* Link Status Notification */
         MAC_LEARNING_INDICATION,/* MAC Address Learning Indication */
         FAULT_INDICATION,       /* ECFM Fault Indication */
         DOS_ATTACK_PKT_TO_CPU,  /* DOS Attack Packet Reception to CPU */
         PKT_L3_TX_FROM_CPU,      /* Packet Transmission over a L3 VLAN
                                    From CPU */

         NPWRAP_CALL_FROM_ACT_TO_STBY, /* Pkt from Active to Standby */
         NPWRAP_CALL_OUTPUT_FROM_STBY_TO_ACT, /* Reply Pkt from Standby to Active */
         RM_HB_PKT,  /* RM Heartbeat Messages*/
         RM_SYNC_PKT, /* RM Synchronization Messages */
         ISS_PEER_MESSAGE, /* Active - Standby card Info Messages*/
         NP_INVALID_OPCODE,
         RM_STDBY_MSG,
         MAC_INDICATION_TO_CHECK_HITBIT, /* Message triggered from remote CPU to 
                                        check HIT BIT Status for LAG 
                                        with local and remote ports */
         DISS_MSG, /* DISS message between DISS nodes */
         CPU_MIRROR_TX_PKT /* Mirrored Egress Packet of CPU to be
                                    transmitted from monitoring port on
                                    the standby node */
    };
    typedef struct NpMapTable {
            UINT1 u1NpSupport; /* Whether NP call is supported in the 
                                * build platform. */
            UINT1 u1WaitStatus;   /* FNP_TRUE/FNP_FALSE...
                                * Acknowledgement required for NP call */
            UINT2 u2Resvd;     /* Reserved for future use */
    } tNpMapTable;


    #define NP_UTIL_FILL_PARAMS(FsHwNp, ModuleId, Opcode, IfIndex, NumPortsParams, NumPortlistParams) \
    { \
     MEMSET (&FsHwNp, 0, sizeof(tFsHwNp)); \
     FsHwNp.NpModuleId = ModuleId; \
     FsHwNp.u4Opcode = Opcode; \
     FsHwNp.u4IfIndex = IfIndex; \
     FsHwNp.u4NumPortsParams = NumPortsParams; \
     FsHwNp.u4NumPortlistParams = NumPortlistParams; \
    } 
    /* Enum to identify place of execution of NP Call */ 
    enum {
     NPUTIL_NO_NP_INVOKE = 1, /* No execution needed for the NP Call */
     NPUTIL_INVOKE_LOCAL_NP, /* NP to be executed only in Local Unit */
     NPUTIL_INVOKE_REMOTE_NP, /* NP to be executed only in remote Unit */
     NPUTIL_INVOKE_LOCAL_AND_REMOTE_NP /* NP to be executed in both Local and Remote Units */
    };
    /* Single NP wrapper to convert PortList and Port Params 
     * and invoke appropriate module based NP wrappers implemented
     * in nputil.c */

    PUBLIC UINT1 NpUtilHwProgram PROTO ((tFsHwNp * pFsHwNp));

    INT4 NpUtilGetStackingPortIndex (tHwPortInfo * pLocalUnitHwPortRange, INT4 i4Index);

    INT4 NpUtilGetRemoteStackingPortIndex(tHwPortInfo * pLocalUnitHwPortRange);

    /* Module Based NP wrappers implemented in appropriate modules */
    #ifdef VLAN_WANTED
    PUBLIC UINT1 VlanNpWrHwProgram PROTO ((tFsHwNp * pFsHwNp));
    #endif /* VLAN_WANTED */
    #ifdef MSTP_WANTED 
    PUBLIC UINT1 MstpNpWrHwProgram PROTO ((tFsHwNp * pFsHwNp));
    #endif /* MSTP_WANTED */
    #ifdef RSTP_WANTED
    PUBLIC UINT1 RstpNpWrHwProgram PROTO ((tFsHwNp * pFsHwNp));
    #endif /* RSTP_WANTED */
    #ifdef PVRST_WANTED
    PUBLIC UINT1 PvrstNpWrHwProgram PROTO ((tFsHwNp * pFsHwNp));
    #endif /* PVRST_WANTED */
    #ifdef LA_WANTED 
    PUBLIC UINT1 LaNpWrHwProgram PROTO ((tFsHwNp * pFsHwNp));
    #endif /* LA_WANTED */
    #ifdef PNAC_WANTED
    PUBLIC UINT1 PnacNpWrHwProgram PROTO ((tFsHwNp * pFsHwNp));
    #endif /* PNAC_WANTED */
    #ifdef VCM_WANTED
    PUBLIC UINT1 VcmNpWrHwProgram PROTO ((tFsHwNp * pFsHwNp));
    #endif /* VCM_WANTED */
    #ifdef RMON_WANTED
    PUBLIC UINT1 RmonNpWrHwProgram PROTO ((tFsHwNp * pFsHwNp));
    #endif /* RMON_WANTED */
    #ifdef RMON2_WANTED
    PUBLIC UINT1 Rmonv2NpWrHwProgram PROTO ((tFsHwNp * pFsHwNp));
    #endif /* RMON2_WANTED */
    #ifdef IGS_WANTED 
    PUBLIC UINT1 IgsNpWrHwProgram PROTO ((tFsHwNp * pFsHwNp));
    #endif /* IGS_WANTED */
    #ifdef MLDS_WANTED
    PUBLIC UINT1 MldsNpWrHwProgram PROTO ((tFsHwNp * pFsHwNp));
    #endif /* MLDS_WANTED */
    #ifdef DSMON_WANTED
    PUBLIC UINT1 DsmonNpWrHwProgram PROTO ((tFsHwNp * pFsHwNp));
    #endif /* DSMON_WANTED */
    #ifdef CFA_WANTED 
    PUBLIC UINT1 CfaNpWrHwProgram PROTO ((tFsHwNp * pFsHwNp));
    #endif /* CFA_WANTED */
    #ifdef EOAM_WANTED
    PUBLIC UINT1 EoamNpWrHwProgram PROTO ((tFsHwNp * pFsHwNp));
    #endif /* EOAM_WANTED */
    #ifdef EOAM_FM_WANTED
    PUBLIC UINT1 EoamfmNpWrHwProgram PROTO ((tFsHwNp * pFsHwNp));
    #endif /* EOAM_FM_WANTED */
    #ifdef ISS_WANTED
    PUBLIC UINT1 IsssysNpWrHwProgram PROTO ((tFsHwNp * pFsHwNp));
    #endif /* ISS_WANTED */
    #ifdef ELPS_WANTED
    PUBLIC UINT1 ElpsNpWrHwProgram PROTO ((tFsHwNp * pFsHwNp));
    #endif /* ELPS_WANTED */
    #ifdef ECFM_WANTED
    PUBLIC UINT1 EcfmNpWrHwProgram PROTO ((tFsHwNp * pFsHwNp));
    #endif /* ECFM_WANTED */
    #ifdef ERPS_WANTED
    PUBLIC UINT1 ErpsNpWrHwProgram PROTO ((tFsHwNp * pFsHwNp));
    #endif /* ERPS_WANTED */
    #ifdef QOSX_WANTED
    PUBLIC UINT1 QosxNpWrHwProgram PROTO ((tFsHwNp * pFsHwNp));
    #endif /* QOSX_WANTED */
    #ifdef RM_WANTED
    PUBLIC UINT1 RmNpWrHwProgram PROTO ((tFsHwNp * pFsHwNp));
    #endif /* RM_WANTED */
    #ifdef MBSM_WANTED
    PUBLIC UINT1 MbsmNpWrHwProgram PROTO ((tFsHwNp * pFsHwNp));
    #endif /* MBSM_WANTED */
    #ifdef SYNCE_WANTED
    PUBLIC UINT1 SynceNpWrHwProgram PROTO ((tFsHwNp * pFsHwNp));
    #endif /* SYNCE_WANTED */
    #ifdef OPENFLOW_WANTED
    PUBLIC UINT1 OfcNpWrHwProgram PROTO ((tFsHwNp * pFsHwNp));
    #endif /* OPENFLOW_WANTED */
    #ifdef MPLS_WANTED
    PUBLIC UINT1 MplsNpWrHwProgram PROTO ((tFsHwNp * pFsHwNp));
    #endif /*MPLS_WANTED */
    #if defined (IP_WANTED) || defined (LNXIP4_WANTED)
    PUBLIC UINT1 IpNpWrHwProgram PROTO ((tFsHwNp * pFsHwNp));
    #endif /* IP_WANTED || LNXIP4_WANTED */
    #if defined (WLC_WANTED) || defined (WTP_WANTED)
    PUBLIC UINT1 CapwapNpWrHwProgram PROTO ((tFsHwNp * pFsHwNp));
    PUBLIC UINT1 WlanNpWrHwProgram PROTO ((tFsHwNp * pFsHwNp));
    PUBLIC UINT1 WlanClientNpWrHwProgram PROTO ((tFsHwNp * pFsHwNp));
    /* Module Based NP wrappers implemented in appropriate modules */
    PUBLIC UINT1 RadioNpWrHwProgram PROTO ((tFsHwNp * pFsHwNp));
    VOID RadioNpWrHwVlantoWlanMapping PROTO ((UINT1 u1CreateStatus, 
       UINT2 u2VlanId, UINT1 *pu1WlanName ,UINT1 u1WlanId));
    #endif /*(WLC_WANTED || WTP_WANTED )*/
    #if defined (IP6_WANTED) || defined (LNXIP6_WANTED)
    PUBLIC UINT1 Ipv6NpWrHwProgram PROTO ((tFsHwNp * pFsHwNp));
    #endif /* IP6_WANTED || LNXIP6_WANTED */
    #if defined (IGMP_WANTED) 
    PUBLIC UINT1 IgmpNpWrHwProgram PROTO ((tFsHwNp * pFsHwNp));
    #endif /* IGMP_WANTED */
    #if defined (IP_WANTED) || defined (LNXIP4_WANTED) || defined (PIM_WANTED) || defined (DVMRP_WANTED) || defined (IGMPPRXY_WANTED)
    PUBLIC UINT1 IpmcNpWrHwProgram PROTO ((tFsHwNp * pFsHwNp));
    #endif /* IP_WANTED || LNXIP4_WANTED || PIM_WANTED || DVMRP_WANTED || IGMPPRXY_WANTED*/ 
    #if defined (MLD_WANTED) 
    PUBLIC UINT1 MldNpWrHwProgram PROTO ((tFsHwNp * pFsHwNp));
    #endif /* MLD_WANTED */
    #if defined (IP6_WANTED) || defined (LNXIP6_WANTED) || defined (PIMV6_WANTED) 
    PUBLIC UINT1 Ip6mcNpWrHwProgram PROTO ((tFsHwNp * pFsHwNp));
    #endif /* IP6_WANTED || LNXIP6_WANTED || PIMV6_WANTED */ 
    #ifdef FSB_WANTED
    PUBLIC UINT1 FsbNpWrHwProgram PROTO ((tFsHwNp * pFsHwNp));
    #endif  /* FSB_WANTED */
    #ifdef VXLAN_WANTED
    PUBLIC UINT1 VxlanNpWrHwProgram PROTO ((tFsHwNp * pFsHwNp));
    #endif  /* FSB_WANTED */

    PUBLIC UINT1 NpUtilHwProgramLocalUnit PROTO ((tFsHwNp * pFsHwNp));
    PUBLIC UINT1 NpUtilHwProgramLocalAndRemoteUnit PROTO ((tFsHwNp * pFsHwNp));

    INT4 NpUtilProcessActiveNpIPCMsg (tHwInfo *pHwInfo);
    INT4 NpUtilProcessWrapperFn (tHwInfo *pHwRemoteInfo);
    UINT1 NpUtilCheckIsValidRemotePort (UINT4 u4IfIndex);
    INT4 NpUtilSetIPCSyncStatus(UINT1 u1IPCSyncStatus);
    INT4 NpUtilGetIPCSyncStatus(UINT1 * pu1IPCSyncStatus);
    PUBLIC VOID NpDbgPrintTime PROTO ((UINT4 u4Flag));
    VOID NpUtilCalculateTotalIPCMsg(UINT4 *pu4TotalCnt, UINT4 *pu4FailCnt);
    VOID NpUtilPrintDbgInfo(VOID);
    VOID NpUtilPrintModOpcodeInfo(INT1 i1ModIdx);
    INT4 NpUtilGetNoOfRetransmitIPCCount(VOID);
    INT4 NpUtilGetNoOfRetransmitFailsIPCCount(VOID);

#define NP_MAX_OPCODES 255

    typedef struct DbgInfo{
 UINT4 au4HwModSuccessCnt[NP_MAX_MOD];
 UINT4 au4HwOpCodeSuccessCnt[NP_MAX_MOD][NP_MAX_OPCODES];
 UINT4 au4HwModFailCnt[NP_MAX_MOD];
 UINT4 au4HwOpCodeFailCnt[NP_MAX_MOD][NP_MAX_OPCODES];
 UINT4 u4NoOfRetransmits;
 UINT4 u4NoOfRetransmitFails;
    }tNpDbgInfo;

#define AST_VLAN_LIST_SIZE                ((VLAN_DEV_MAX_VLAN_ID + 31)/32 * 4)
#define NP_UTIL_DBG_COUNTER(mod, opcode, retval)     \
    do{                                              \
 if (retval==FNP_SUCCESS)                         \
 {                                                \
     gNpDbgInfo.au4HwOpCodeSuccessCnt[mod][opcode]++;  \
 }                                                \
 else                                             \
 {                                                \
     gNpDbgInfo.au4HwOpCodeFailCnt[mod][opcode]++;     \
 }                                                \
    }while(0)

#define NP_UTIL_DBG_IPC_COUNTER(mod, opcode, retval)     \
    do{                                              \
 if (retval==FNP_SUCCESS)                         \
 {                                                \
     gNpDbgInfo.au4HwModSuccessCnt[mod]++;        \
     gNpDbgInfo.au4HwOpCodeSuccessCnt[mod][opcode]++;  \
 }                                                \
 else                                             \
 {                                                \
     gNpDbgInfo.au4HwModFailCnt[mod]++;           \
     gNpDbgInfo.au4HwOpCodeFailCnt[mod][opcode]++;     \
 }                                                \
    }while(0)

    #endif /* __NP_UTIL_H__ */
