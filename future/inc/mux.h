/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: mux.h,v 1.5 2012/02/09 11:11:25 siva Exp $
 *
 * Description : This file contains the Macors and Structures that    
 *               required for passing data across User & Kernel Space
 *               via ioctl () requests
 *******************************************************************/
#ifndef _MUX_H_
#define _MUX_H_

#include "chrdev.h"

/* Macros related to Character Device Interface b/w MUX & ISS */
#define MUX_DEVICE_FILE_NAME      "/dev/IssMuxDev"

#define MUX_MAJOR_NUMBER          GDD_MAJOR_NUMBER

#define MUX_PROT_REG_IOCTL        _IOWR(MUX_MAJOR_NUMBER, 25, char *)
#define MUX_NP_IOCTL              GDD_NP_IOCTL

#ifdef NPSIM_WANTED
#define NPSIM_DEVICE_FILE_NAME    "/dev/NpSimMuxDev"
#define NPSIM_DEVICE_MAJOR_NUMBER 110
#define NPSIM_ISS_DEV_WRITE       _IOWR(NPSIM_DEVICE_MAJOR_NUMBER, 1, char *)
#endif

/* Registration Table related Macro that indicates where to post an incoming
 * packet. One or more destinations (e.g. Linux IP and ISS) can be specified */

#define MUX_REG_SEND_TO_ISS         0x00000001
#define MUX_REG_SEND_TO_IP          0x00000002

/* Enum's for Handling ioctl () requests from User Land */
enum
{
    MUX_PROT_REG_HDLR = 1,
    MUX_PROT_DEREG_HDLR,
    MUX_DEV_WRITE
};

/* Structure of the buffer passed to Kernel when ISS wants to send a packet 
 * to the MUX Module */

typedef struct _MuxDevWriteParams
{
    INT4                i4Command;
    UINT1              *pu1DataBuf;
    UINT4               u4PktSize;
    INT4                i4RetVal;
    UINT4               u4IfIndex;
    UINT2               u2VlanId;
    UINT2               u2Align;
} tMuxDevWriteParams;

/* This is the protocol callback functions to 
 * be registered by each module for receiving packets 
 * from the MUX  */
typedef INT4 (*tCallBackFn)(
                            tCRU_BUF_CHAIN_HEADER *pBuf,
                            UINT4 u4IfIndex,
                            UINT4 u4PktSize);

/* Structure of MAC Qualifier present in tProtRegTuple */

typedef struct _MacTuple
{
    tMacAddr            MacAddr;    /*  Ethernet MAC Address  */
    tMacAddr            MacMask;    /*     MAC Address Mask   */
} tMacTuple;

/* Structure of Ethernet and IP Qualifier's present in tProtRegTuple */

typedef struct _ProtInfo
{
    UINT2               u2Protocol;    /*  L2/L3 Protocol Number */
    UINT2               u2Mask;    /*         Mask           */
} tProtInfo;

/* Structure for holding Protocol Registration Qualifiers */

typedef struct _ProtRegTuple
{
   tMacTuple       MacInfo;           /* MAC Information to be matched */
   tProtInfo       EthInfo;           /* Ethernet Protocol to be matched */      
   tProtInfo       IPInfo;            /* IP Protocol to be matched */  
} tProtRegTuple;


typedef struct ProtocolInfo {
    tCallBackFn    ProtocolFnPtr; /* Function call back Registered*/
    tProtRegTuple  ProtRegTuple;  /* Strcture containing the qualifiers 
                                     for the packet */
    INT4           i4Command;     /* Register/Deregister */
    INT4           i4Enabled;     /* Currrent Status */
    INT1           i1ModuleId;    /* Module Identifier */
    UINT1          u1SendToFlag;  /* Packet to be sent to ISS/Linux*/
    UINT1          u1RegCount;    /* Count indicating the number of times the
                                     protocol has registered */
    UINT1          u1Pad;         /* Padding */
}tProtocolInfo;

INT4 CfaMuxRegisterProtocolCallBack (INT1 i1ProtocolId, tProtocolInfo * pProtocolInfo);
INT4 CfaMuxUnRegisterProtocolCallBack (INT1 i1ProtocolId);

INT4 CfaMuxHandleIncomingFrame PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, 
                                 UINT4 u4ModuleId, UINT4 u4IfIndex,
                                 UINT4 u4PktLen, UINT2 u2IfType,
                                 UINT1 u1EnetEncapType));
/* Structure to pass Protocol Registration Information */
typedef struct _MuxProtRegParams {
   INT4                i4Command;     /* MUX_PROT_REG_HDLR or            
                                         MUX_PROT_DEREG_HDLR */    
   UINT4               u4ModuleId;    /* Unique Module ID */
   tProtRegTuple       ProtRegTuple;  /* 3 Tuple identifier for each Module*/ 
   INT4                i4RetVal;      /* Command execution status */
   UINT1               u1SendToFlag; /* Indicates to which module this 
                                        packet needs to be handed over. 
                                        This can be set to 
                                        MUX_REG_SEND_TO_ISS and/or 
                                        MUX_REG_SEND_TO_LNXIP */         
   UINT1               au1Align[3];   /* For Alignement Purpose */

} tMuxProtRegParams;
#ifndef OS_VXWORKS
#ifndef BCMX_WANTED
#ifndef OS_QNX
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,0)
#define KERNEL_MODULE_NAME        "ISSmux.ko"
#else
#define KERNEL_MODULE_NAME        "ISSmux.o"
#endif
#endif

typedef union {
    tOobIfName      IfName;
    tSourcePort     SourcePort;
    tKernIntfParams KernIntfParams;
#ifdef WGS_WANTED
    tKernMgmtVlanParams KernMgmtVlanParams;
#endif
#ifdef VRRP_WANTED
    tVrrpAssocIpParams VrrpAssocIpParams;
#endif
    tVlanFdbEntryParams VlanFdbEntryParams;
    tVlanFdbParams VlanFdbParams;
#ifdef MBSM_WANTED
    tStackingParams KernStackParams;
#endif
    tMuxProtRegParams MuxProtRegParams;    
    tMuxDevWriteParams  MuxDevWriteParams;    
}tKernCmdParam;

typedef struct { 
    UINT4             u4IfIndex; /* CFA IfIndex on which the packet 
                                    is received */
    UINT4             u4PktLen; /* Packet len according to flags */
    UINT4             u4ModuleId;  /* Unique Module ID */
} tHandlePacketRxCallBack;

typedef enum
{
    PACKET_RX = 1,
} tCallBackEvent;

/* Structure of the Information passed from Kernel to User Space incase of
 * any events like Packet RX, Port State etc.. */

typedef struct _tdata
{
    tHeader                    Hdr;
    union
    {
        tHandlePacketRxCallBack RxCbk;
    } u;
} tData;

#define GDD_DEVICE_FILE_NAME      MUX_DEVICE_FILE_NAME
#endif /*BCMX_WANTED*/
/* --------------------- User Space Exported API's  ------------------------*/
/* User space API to configure kernel module */
INT4 KAPIUpdateInfo (INT4 i4Command, tKernCmdParam *pCmdParam);
#endif
#ifdef __KERNEL__
/* ------------------- Kernel Space Exported API's  ----------------------- */

/* MUX Module's Exported TX Routine to transmit a packet to the underlying
 * Switch Driver [eg: NPSIM or Marvell Chip]. This API gets called from ISS
 * NetDevice Module */
INT4 MuxTxDevPacket PROTO ((tSkb * pSkb, tNetDevice * pDev));

#ifdef VRRP_WANTED
VOID MuxVrrpInitIntfTable PROTO ((UINT4 u4IfIndex));
#endif /* VRRP_WANTED */

#endif /* __KERNEL__ */

#endif /* _MUX_H_ */

/* END OF FILE */
