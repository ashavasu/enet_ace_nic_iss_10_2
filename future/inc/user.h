/********************************************************************
 * Copyright (C) Future Software Limited, 2001
 *
 * Description: This file contains exported definitions and macros
 *              of wssuser module
 * $Id: user.h,v 1.1 2014/10/20 10:51:02 siva Exp $
 *******************************************************************/
#ifndef __USER_H__
#define __USER_H__

/* Creates MemPools WssUser Module */
UINT4 WssUserInitModuleStart (VOID);
INT4 UserLock PROTO ((VOID));
INT4 UserUnLock PROTO ((VOID));
#endif /* _USER_H */
/********** END OF USER.H *****************/


