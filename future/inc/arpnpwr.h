
/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: arpnpwr.h,v 1.8 2018/02/02 09:47:31 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              for ARP wrappers
 *              
 ********************************************************************/
#ifndef __ARP_NP_WR_H__
#define __ARP_NP_WR_H__

UINT1 ArpFsNpIpv4VrfArpDel PROTO ((UINT4 u4VrId , UINT4 u4IpAddr , UINT1 *pu1IfName , INT1 i1State ));
UINT1 ArpFsNpIpv4VrfArpAdd PROTO ((UINT4 u4VrId , tNpVlanId u2VlanId , UINT4 u4IfIndex , UINT4 u4IpAddr , UINT1 *pMacAddr , UINT1 *pu1IfName , INT1 i1State , UINT4 *pu4TblFull ));
UINT1 ArpFsNpIpv4SyncVlanAndL3Info PROTO ((VOID));
UINT1 ArpFsNpL3Ipv4VrfArpAdd PROTO ((UINT4 u4VrfId , UINT4 u4IfIndex , UINT4 u4IpAddr , UINT1 *pMacAddr , UINT1 *pu1IfName , INT1 i1State , UINT1 *pbu1TblFull ));
UINT1 ArpFsNpIpv4VrfArpModify PROTO ((UINT4 u4VrId , tNpVlanId u2VlanId , UINT4 u4IfIndex , UINT4 u4PhyIfIndex, UINT4 u4IpAddr , UINT1 *pMacAddr , UINT1 *pu1IfName , INT1 i1State ));
UINT1 ArpFsNpL3Ipv4VrfArpModify PROTO ((UINT4 u4VrfId , UINT4 u4IfIndex , UINT4 u4IpAddr , UINT1 *pMacAddr , UINT1 *pu1IfName , INT1 i1State ));
UINT1 ArpFsNpIpv4VrfArpGetNext PROTO ((tNpArpInput ArpNpInParam , tNpArpOutput * pArpNpOutParam ));
UINT1 ArpFsNpIpv4VrfArpGet PROTO ((tNpArpInput ArpNpInParam , tNpArpOutput * pArpNpOutParam ));
UINT1 ArpFsNpIpv4VrfCheckHitOnArpEntry PROTO ((UINT4 u4VrId , UINT4 u4IpAddress , UINT1 u1NextHopFlag ));
UINT1 ArpFsNpIpv4ArpAdd PROTO ((tNpVlanId u2VlanId , UINT4 u4IfIndex , UINT4 u4IpAddr , UINT1 *pMacAddr , UINT1 *pu1IfName , INT1 i1State , UINT1 *pbu1TblFull ));
UINT1 ArpFsNpIpv4ArpModify PROTO ((tNpVlanId u2VlanId , UINT4 u4IfIndex , UINT4 u4IpAddr , UINT1 *pMacAddr , UINT1 *pu1IfName , INT1 i1State ));
UINT1 ArpFsNpIpv4ArpDel PROTO ((UINT4 u4IpAddr , UINT1 *pu1IfName , INT1 i1State ));
UINT1 ArpFsNpIpv4ArpGet PROTO ((tNpArpInput ArpNpInParam , tNpArpOutput * pArpNpOutParam ));
UINT1 ArpFsNpIpv4ArpGetNext PROTO ((tNpArpInput ArpNpInParam , tNpArpOutput * pArpNpOutParam ));
UINT1 ArpFsNpIpv4CheckHitOnArpEntry PROTO ((UINT4 u4IpAddress , UINT1 u1NextHopFlag ));
UINT1 ArpFsNpIpv4ClearArpTable PROTO ((VOID));
UINT1 ArpFsNpIpv4GetSrcMovedIpAddr PROTO ((UINT4 *pu4IpAddress ));
UINT1 ArpFsNpL3Ipv4ArpAdd PROTO ((UINT4 u4IfIndex , UINT4 u4IpAddr , UINT1 *pMacAddr , UINT1 *pu1IfName , INT1 i1State , UINT1 *pbu1TblFull ));
UINT1 ArpFsNpL3Ipv4ArpModify PROTO ((UINT4 u4IfIndex , UINT4 u4IpAddr , UINT1 *pMacAddr , UINT1 *pu1IfName , INT1 i1State ));
UINT1 ArpFsNpIpv4VrfGetSrcMovedIpAddr PROTO ((UINT4 *pu4VrId , UINT4 *pu4IpAddress ));
#ifdef MBSM_WANTED
UINT1 ArpFsNpMbsmIpv4VrfArpAdd PROTO ((UINT4 u4VrId , tNpVlanId u2VlanId , UINT4 u4IpAddr , UINT1 *pMacAddr , UINT1 *pbu1TblFull , tMbsmSlotInfo * pSlotInfo ));
UINT1 ArpFsNpMbsmIpv4VrfArpAddition PROTO ((UINT4 u4VrId, tNpVlanId u2VlanId, UINT4 u4IpAddr, UINT1 * pMacAddr, UINT1 * pbu1TblFull, UINT4 u4IfIndex, UINT1 * pu1IfName, INT1 i1State, tMbsmSlotInfo * pSlotInfo));
#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
UINT1 ArpFsNpCfaModifyFilter PROTO ((UINT1 u1Action));
#endif /* NPAPI_WANTED && CFA_WANTED*/

#endif /* MBSM_WANTED */
UINT1
ArpFsNpHwGetPortFromFdb (UINT2 u2VlanId, UINT1 *i1pHwAddr, UINT4 *pu4Port);
#ifdef CFA_WANTED
UINT1 ArpFsCfaHwRemoveIpNetRcvdDlfInHash PROTO ((UINT4 u4ContextId,UINT4 u4IpNet, UINT4 u4IpMask));
UINT1 ArpFsCfaHwRemoveIpNetRcvdDlf PROTO ((UINT4 u4IpNet, UINT4 u4IpMask, UINT4 u4VrId));
#endif
#endif /* __ARP_NP_WR_H__ */
