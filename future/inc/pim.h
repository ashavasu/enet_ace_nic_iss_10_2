/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pim.h,v 1.36 2017/05/05 11:55:42 siva Exp $
 *
 * Description:This file contains the exported definitions and  
 *             macros of PIM                                    
 *
 *******************************************************************/
#ifndef _PIM_H
#define _PIM_H

#include "utilipvx.h"

#ifdef MBSM_WANTED
#include "mbsm.h"
#endif

/* PIM return Codes */
#define   PIM_SUCCESS    0
#define  PIMSM_MAX_COMPONENT    255
#define  PIMSM_NUM_COMPONENT    5 
#define  PIMSM_DFL_MAX_INTERFACE    8
#define    PIMSM_MAX_CHARACTER      16 /* MAX CHARACTERS FOR
                                          THE PIM VERSION STRING */
#define  PIM_MAX_SEMNAME_LEN         8

typedef struct PimSemDesc {
UINT1      au1SemName[PIM_MAX_SEMNAME_LEN];
tOsixSemId semId;
tOsixSemId PimDSemId;
tOsixSemId PimNbrSemId;
} tPimSemDesc;

INT4 PimCreateTask ARG_LIST ((UINT4 u4PimMode));
VOID SparsePimMain ARG_LIST ((INT1 *pi1Param));
VOID SparsePimHelloMain ARG_LIST ((INT1 *pi1Param));
VOID PimModuleShut(VOID);
INT1 PimModuleStart(VOID);

typedef struct _SPimConfigParam {
   struct PimSemDesc   PimMutex;
   tRBTree    pBidirPimDFTbl;
   tRBTree    pRpSetList;
   UINT4      u4IPMRouteEntryCnt;
   UINT4      u4SPTGrpThreshold;
   UINT4      u4SPTSrcThreshold;
   UINT4      u4SPTSwitchingPeriod;
   UINT4      u4SPTRpThreshold;
   UINT4      u4SPTRpSwitchingPeriod;
   UINT4      u4RegStopRateLimitPeriod;
   UINT4      u4RegThresholdForRateLimitRegStop;
   UINT4      u4GlobalTrc;
   UINT4      u4GlobalDbg;
   UINT1      au1PimVersionString [PIMSM_MAX_CHARACTER];
   UINT1      au1PimTrcMode[20];
   UINT4      u4PmbrBit;
   UINT4      u4RtrMode;
   UINT4      u4StaticRpEnabled;
   UINT4      u4BsrStaticPreference;
   UINT4      u4StaticRPPriority;
   UINT4      u4JPInterval;
   UINT4      u4ComponentCount;
   UINT4      u4MemAllocFailCount;
   UINT4      u4MaxInterfaces;
   UINT4      u4MaxGrpMbrs;
   UINT4      u4MaxSrcs;
   UINT4      u4MaxRps;
   INT4       i4IpRegnId;
   INT4       i4Ip6RegnId;
   INT4       i4BidirOfferLimit;
   INT4       i4BidirOfferInterval;
   INT4       i4SRTInterval; /*Origination interval -1 signifies
                               *generation of SRM is stopped*/
   UINT4      u4SATInterval; /*Source Active time*/
#ifdef LNXIP4_WANTED
   INT4       i4PimSockId;
#endif
#ifdef LNXIP6_WANTED
   INT4       i4Pimv6SockId;
#endif
   UINT1      u1MfwdStatus;
   UINT1      u1PimStatus;
   UINT1      u1PimV6Status;
   UINT1      u1SRProcessingStatus; /* This variable is to control
                                       the processing and forwarding
                                       of SRM*/
   UINT1      u1PimFeatureFlg;
   UINT1      au1Pad[3];
} tPimConfigParam;
#ifdef __SPIM_INPUT_C__
tPimConfigParam gSPimConfigParams;
#else
extern tPimConfigParam gSPimConfigParams;
#endif

#define   PIMSM_FAILURE                       1
#define   PIMSM_SUCCESS                       0
#define   PIMSM_ENABLED                        1
#define   PIMSM_DISABLED                       2
#define   PIMSM_MODE                           2
#define   PIMBM_MODE                           4
#define   PIMBM_GLOB_WIN                       2 
#define   PIMBM_GLOB_LOSE                      4
#define   PIMBM_GLOB_OFFER                     1
#define PIM_IS_PIM_ENABLED   PimIsPimEnabled ()

PUBLIC INT4 PimIsPimEnabled ARG_LIST ((VOID));


typedef UINT2 tPimVlanId;    
typedef struct _McastVlanPbmpData {
    tMacAddr       MacAddr;
    tPimVlanId     VlanId; 
}tMcastVlanPbmpData;       

INT4 SparsePimHandleIgmpStatus ARG_LIST ((UINT1 u1AddrType));
INT4 SparsePimHandleIgmpIfDown ARG_LIST ((UINT4 u4Port, UINT1 u1AddrType));
INT4 SPimPortCheckSGVlanId (UINT2 u2VlanId, tIPvXAddr SrcAddr, tIPvXAddr GrpAddr);
INT4 SPimPortGetSGFromVlanId (UINT2 u2VlanId, tIPvXAddr *pSrcAddr, tIPvXAddr *pGrpAddr);
INT4 SPimPortCheckDROnInterface (UINT4 u4IfIndex);
INT4 SPimPortCheckOnInterface (UINT4 u4IfIndex);
INT4 SPimPortCheckOnIpv6Interface (UINT4 u4IfIndex);
INT4 PimApiIfShutDownIpChgNotify (UINT4 u4IfIndex);
INT4
SPimPortMCinitInterface (UINT4 u4CfaIfIndex,UINT1 u1CfaIfType,
                           UINT4 u4Port,UINT2 u2VlanId);
INT4
SPimPortMCDeinitInterface (UINT4 u4CfaIfIndex,UINT1 u1CfaIfType,
                           UINT4 u4Port,UINT2 u2VlanId);

INT4 PimUtlCheckOnInterface(UINT4 u4IfIndex);

extern INT4 PimMutexLock PROTO ((VOID));
extern INT4 PimMutexUnLock PROTO ((VOID));

#ifdef MBSM_WANTED
#define MBSM_PIM    "MbsmPim"
VOID PimMbsmUpdateMrouteTable (tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4Cmd);
INT4 PimMbsmUpdateCardStatus ARG_LIST ((tMbsmProtoMsg * pProtoMsg, INT4 i4Event));
INT4 PimMbsmPostMessage (tMbsmProtoMsg * pProtoMsg, INT4 i4Event);
#endif
#endif /* _PIM_H */
