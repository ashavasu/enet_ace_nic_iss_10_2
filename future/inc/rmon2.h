/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 *
 *  $Id: rmon2.h,v 1.10 2015/09/13 10:36:23 siva Exp $
 *
 * Description: This file contains the exported definitions and
 *              include files of RMON2 
 *
 *******************************************************************/

#ifndef _RMN2_H_
#define _RMN2_H_

/* CFA Packet processing Macros */
#define RMON2_MAC_ADDRESS_LEN   6
#define RMON2_ETH_TYPE_LEN      2
#define RMON2_VLAN_HEADER_LEN   4
#define RMON2_IPV4_VER_LEN      1
#define RMON2_IPV4_DSCP_LEN     1
#define RMON2_IPV4_PROTO_LEN    1
#define RMON2_IPV4_ADDR_LEN     4
#define RMON2_L4PORT_LEN        2
#define RMON2_VLANID_LEN 2

#define RMON2_IPV4_PROTO_FROM_DSCP_OFFSET       8
#define RMON2_IPV4_SIP_FROM_PROTO_OFFSET        3

#define RMON2_IPV4_VERSION      4
#define RMON2_IPV6_VERSION      6
#define RMON2_IP_PROTO_TCP      6
#define RMON2_IP_PROTO_UDP      17

#define RMON2_IPV6_TC_LEN       2
#define RMON2_IPV6_NEXT_HEADER_OFFSET   6
#define RMON2_IPV6_SIP_FROM_PROTO_OFFSET 2
#define RMON2_IPV6_ADDR_LEN     16

#define RMON2_INNER_VLAN_ID   0x8100
#define RMON2_OUTER_VLAN_ID   0x9100
#define RMON2_ENET_IPV4         0x0800
#define RMON2_ENET_IPV6         0x86DD
#define RMON2_ENET_ICMP         0x0001
#define RMON2_IPV6_DSCP         0x0ff0
#define RMON2_VLANID  0x0fff

#define UINT8_ADD(varA, value) \
   if ((0xffffffff - varA.u4LoWord) >= value){ \
      (varA.u4LoWord)+= value; \
   }                           \
   else {                      \
      varA.u4LoWord = value - (0xffffffff - varA.u4LoWord) - 1;\
      (varA.u4HiWord)++; \
   }

#define UINT8_SUB(varA,varB,varC) \
  if (varA.u4LoWord < varB.u4LoWord){ \
      (varA.u4HiWord)--; \
      varC.u4LoWord = (varA.u4LoWord - varB.u4LoWord); \
  } \
  else { \
      varC.u4LoWord = varA.u4LoWord - varB.u4LoWord; \
  } \
  varC.u4HiWord = varA.u4HiWord - varB.u4HiWord;

#define UINT8_CMP(varA, varB, retVal) \
    if (varA.u4HiWord > varB.u4HiWord){ \
        retVal = 1; \
    } \
    else if (varA.u4HiWord < varB.u4HiWord){ \
        retVal = -1; \
    } \
    else if (varA.u4HiWord == varB.u4HiWord){ \
        if (varA.u4LoWord > varB.u4LoWord){ \
            retVal = 1; \
        }       \
        else if (varA.u4LoWord < varB.u4LoWord){ \
            retVal = -1; \
        } \
        else if (varA.u4LoWord == varB.u4LoWord){ \
            retVal = 0; \
        } \
    }

#define UINT8_RESET(varA) \
   varA.u4HiWord = 0; \
   varA.u4LoWord = 0;

typedef struct Uint8 {
   UINT4 u4HiWord;
   UINT4 u4LoWord;
}tUint8;

typedef struct Rmon2Stats {
   tUint8 u8PktCnt;
   tUint8 u8OctetCnt;
}tRmon2Stats;

typedef struct Rmn2IpAddress
{
  union
  {
    UINT1  u1ByteAddr[16];
    UINT2  u2ShortAddr[8];
    UINT4  u4WordAddr[4];
  } ip6_addr_u;

#define  u4_addr  ip6_addr_u.u4WordAddr
#define  u2_addr  ip6_addr_u.u2ShortAddr
#define  u1_addr  ip6_addr_u.u1ByteAddr

} tRmn2IpAddr;

typedef struct PktHdr {
    tMacAddr  SrcMACAddress;   /* Source MAC Address */
    tMacAddr  DstMACAddress;   /* Destination MAC */
    tRmn2IpAddr  SrcIp;        /* Source IP Address */
    tRmn2IpAddr  DstIp;        /* Destination IP Address */
    UINT4 u4IfIndex;           /* Interface Index */
    UINT4 u4VlanIfIndex;       /* L3 Vlan Interface Index */
    UINT2 u2EtherType;         /* L2 Ethertype Field - Ip or Ipvx*/
    UINT2 u2SrcPort;           /* L4 Source Port */
    UINT2 u2DstPort;           /* L4 Destination Port Field - Telnet or FTP */
    UINT2 u2VlanId;            /* L2 VLAN Id */
    UINT1 u1IpProtocol;        /* L3 Protocol Field - TCP / UDP */
    UINT1 u1DSCP;              /* DSCP */
    UINT1 u1IpVersion;         /* IP Header Version */
    UINT1 u1Rsvd;           /* Reserved */
} tPktHeader;

typedef struct PktHdrInfo
{
    tPktHeader PktHdr;        /* Packet Header */
    UINT4 u4PktSize;        /* Incoming Packet size */
} tPktHdrInfo;

typedef struct LinkStatInfo
{
    UINT4 u4IfIndex;
    UINT4  u4SockId;
    UINT1 u1OperStatus;
    UINT1 au1Pad[3];
} tLinkStatInfo;


PUBLIC VOID Rmon2MainTask ARG_LIST((INT1 *));
PUBLIC INT4 Rmon2UtilUpdatePktHdr ARG_LIST((UINT1 *, tPktHeader *));
PUBLIC VOID DsmonMainProcessPktInfo ARG_LIST((tPktHdrInfo *, UINT4, UINT4, UINT4));
PUBLIC VOID DsmonMainUpdateProtoChgs ARG_LIST((UINT4));
PUBLIC VOID Rmon2CfaIndicateIfUpdate ARG_LIST ((UINT4 u4IfIndex, UINT1 u1OperStatus));

#endif

