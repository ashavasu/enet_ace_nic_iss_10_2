/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ip.h,v 1.181 2018/01/03 11:32:33 siva Exp $
 *
 * Description:This file contains the exported definitions and
 *             macros of IP
 *
 *******************************************************************/
#ifndef   _IPV4_H
#define   _IPV4_H

#include "size.h"
#include "mbsm.h"
#include "tcp.h"
#include "pim.h"
#include "fssnmp.h"
#include "utilipvx.h"

/* DEFINITIONS */
/* ----------- */
/* For Removing Warnings in target */
#ifdef LOCAL
#undef LOCAL
#endif

/* Macros for checksum */
#define CKSUM_LASTDATA      1
#define CKSUM_DATA          0
#define MAX_DATA_LEN        1024

/* Macros for AD */
#define   MAX_AD            255
#define   IP_DEFAULT_WEIGHT 1

#define   MAX_ECMP_PATHS         64
#define   IP_MAX_PROTOCOLS         150
#define   IP6_PTCL                 41
#define   IP_BUF_FAILURE           CRU_FAILURE
#define   LOCAL_RT_INDEX           1
#define   MAX_APPLICATIONS         64
#define   IP_PKT_RXED_EVENT        (0x80000000)
#define   UDP_INPUT_Q_EVENT        0x00000001
#define   IP_UCAST                 4
#define   IP_PKT_OFF_DEST          16
#define   IP_PKT_OFF_PROTO          9
#define   IP_MAX_OPT_LEN           40
#define   IP_DEFAULT_CONTEXT               0
#define   ICMP_TYPES                       41
#define   ICMP_MAX_DOMAIN_NAME_SIZE        256
#define   IP_DEF_MAX_PMTU_ENTRY   FsIPSizingParams[MAX_IP_PMTU_ENTRIES_SIZING_ID].u4PreAllocatedUnits
#define   IP_DEF_MAX_FRAGS        FsIPSizingParams[MAX_IP_FRAG_PKTS_SIZING_ID].u4PreAllocatedUnits
#define   IP_DEF_MAX_REASM_LISTS  FsIPSizingParams[MAX_IP_REASM_LISTS_SIZING_ID].u4PreAllocatedUnits
#define   IP_TRUE                          1 
#define   IP_FALSE                         0 


/* Mode of receive unicast, broadcast, multicast */
#define   IP_FOR_THIS_NODE    3
#define   IP_ADDR_LOOP_BACK   5       /*Default Loopback interface support*/

                                        /* Protocol ids as per RFC 2096 */
#define   OTHERS_ID               0x01 /* Aggregate Routes */
#define   LOCAL_ID                0x02  /* Local Interface */
#define   STATIC_ID                0x03 /* Configured Static Route */
#define   RIP_ID                   0x08 
#define   ISIS_ID                  0x09
#define   OSPF_ID                  0x0d
#define   BGP_ID                   0x0e
#define   LLDP_ID                  0x8a /* 140-252 are unassigned protocol 
                                           numbers in /etc/protocols. First 
                                           unassigned number 138 is used for
                                           LLDP Protocol Id here */
#define  ECFM_ID                   0x8b /*139 is used as ECFM protocol id */
#define  SECURITY_ID               0x8c /*140 is used as Security module id */
#define  VPN_ID                    0x8d /*141 is used as IKE module id */
#define  SNOOP_ID                  0x8e /*142 is used as SNOOP module id */
#define  VXLAN_ID                  0x8f /*143 is used as VXLAN module id */

#define  IP_ID                     0x97 /* Till 150 is the standard values.
                                           This is proprietary for IP. */

#define  CIDR_LOCAL_ID              0x02
#define  CIDR_STATIC_ID             0x03
#define  INVALID_PROTO              -1

/* Protocol IDs of modules registerd with IP */
#define   PIM_ID                   103
#define   DVMRP_ID                  0x13 /* Standard ProtocolId of DVMRP */
#define   IP_ARP_REG_ID            140
#define   IGMP_ID                  0x02
#define   IP_L2TP_REG_ID           115
#define   IP_PROTOCOL            0x0800

#define   STATIC_RT_INDEX             2
#define   RIP_RT_INDEX                7
#define   ISIS_RT_INDEX               8
#define   OSPF_RT_INDEX              12
#define   BGP_RT_INDEX               13
#define   OTHERS_RT_INDEX             0
#define   ALL_ROUTING_PROTOCOL       -1
#define   RT_NOT_FOUND               -1
#define   MPLS_IP_PROTOCOL_ID      137 /* As per RFC 5332 */

/* Protocol Preference *//* moved from code/future/netip/rtm/inc/rtmdefns.h */
#define   STATIC_DEFAULT_PREFERENCE  1
#define   OSPF_DEFAULT_PREFERENCE    110
#define   ISIS_DEFAULT_PREFERENCE    120
#define   RIP_DEFAULT_PREFERENCE     120
#define   BGP_DEFAULT_PREFERENCE     122

#define   ONE_LT_TWO                           0
#define   ONE_EQ_TWO                           1
#define   ONE_GT_TWO                           2

#ifdef    BZERO
#undef    BZERO
#define   BZERO                    bzero
#endif

#define   IP_ZERO                  0
#define   IP_ONE                   1
#define   IP_TWO                   2
#define   IP_THREE                 3
#define   IP_FOUR                  4
#define   IP_FIVE                  5
#define   IP_SIX                   6
#define   IP_EIGHT                 8
#define   IP_SIXTEEN               16
#define   IP_HUNDRED               100
#define   IP_THOUSAND              1000
#define   IP_BYTE_ALL_ONE          0xFF

#define   BYTE_LENGTH              8
#define IPROUTE_LOCAL_TYPE           3
#define IPROUTE_REMOTE_TYPE          4

#define  IP_LOCAL_RT_METRIC            0     /*Local Route Metric */

#define   CIDR_REMOTE_ROUTE_TYPE   0x04
#define   MAX_ROUTING_PROTOCOLS    16

#define   ENABLED                  1 /* enabled/disabled status of the port */ 
#define   DISABLED                 0

#define   ZERO                     0
#define   UP                       1
#define   DOWN                     2
#define   CRU_IP                   4
#define   IP_LAYER4_DATA           6
#define   IP_ICMP_DATA             7
#define   IP_RAW_DATA              9

#define   IP_HTONS(x)              OSIX_HTONS((x))
#define   IP_NTOHS(x)              OSIX_NTOHS((x))

#define   IP_HTONL(x)              OSIX_HTONL((x))
#define   IP_NTOHL(x)              OSIX_NTOHL((x))

#ifndef   CRU_HTONL
#define   CRU_HTONL                IP_HTONL
#endif

#ifndef   CRU_NTOHL
#define   CRU_NTOHL                IP_NTOHL
#endif

#ifndef   CRU_HTONS
#define   CRU_HTONS                IP_HTONS
#endif

#ifndef   CRU_NTOHS
#define   CRU_NTOHS                IP_NTOHS
#endif


#define   STAT                     stat
#define   IP_DF                    0x4000


/******************* IP imports start **************************/
/* to be mapped with the respective IP defines */
#define   IP_SUCCESS                 0
#define   IP_FAILURE                -1
#define   IP_NO_ROOM                 1
#define   IP_ROUTE_FOUND             2
#define   IP_EXACT_ROUTE             1
#define   IP_BEST_ROUTE              2
#define   IP_ROUTE_NOT_FOUND         3
#define   IP_SUCCESS_NOT_ADD_TO_HW   4
#define   IPVX_FAILURE               0
#define   IPVX_SUCCESS               1

/* Linux IP defns */
#define   IP_PORT_NAME_LEN           24      /* CFA_MAX_PORT_NAME_LENGTH */

#define   IGMP_TIMER_UNITS           10

#define   ADMIN_STATE         0x00000002
#define   IP_ADDR             0x00000004
#define   IFACE_DELETED       0x00000020
#define   IF_SECIP_CREATED    0x00000200
#define   IF_SECIP_DELETED    0x00000400
#define  ISS_PKT_RECV_IGMP  1
#define  ISS_PKT_RECV_IGS   2

#define   TMO_OK                     IP_SUCCESS
#define   IPFWD_PACKET_ARRIVAL_EVENT 1

/* value for IP_SOCK_MODE */
#define  SOCK_UNIQUE_MODE       1         /* Context specific socket */ 
#define  SOCK_GLOBAL_MODE       2         /* Global socket for all contexts */

#define   IPIF_MAX_LOGICAL_IFACES   (IP_DEV_MAX_L3VLAN_INTF + IP_MAX_RPORT +\
                                     IP_DEV_MAX_TNL_INTF + SYS_MAX_PPP_INTERFACES)

#define   MAX_NUM_AGGR_RTS    FsIPSizingParams[MAX_IP_AGG_ROUTES_SIZING_ID].u4PreAllocatedUnits

#define   IPIF_INVALID_INDEX         0xffff
#define   IPIF_INVALID_NEXTHOP       0xffff
                                                                                                                             
/* Route type as per RFC 2096 */
#define   OTHERS                    0x01
#define   REJECT                    0x02
#define   FSIP_LOCAL                0x03
#define   REMOTE                    0x04

/* Row status */
#define   IPFWD_ROW_DOES_NOT_EXIST          0
#define   IPFWD_ACTIVE                      1
#define   IPFWD_NOT_IN_SERVICE              2
#define   IPFWD_NOT_READY                   3
#define   IPFWD_CREATE_AND_GO               4
#define   IPFWD_CREATE_AND_WAIT             5
#define   IPFWD_DESTROY                     6
                                                                                                                             
#define   IPFWD_ROUTE_IFINDEX               1
#define   IPFWD_ROUTE_TYPE                  2
#define   IPFWD_ROUTE_PROTO                 3
#define   IPFWD_ROUTE_AGE                   4
#define   IPFWD_ROUTE_INFO                  5
#define   IPFWD_ROUTE_NEXTHOPAS             6
#define   IPFWD_ROUTE_METRIC1               7
#define   IPFWD_ROUTE_METRIC2               8
#define   IPFWD_ROUTE_METRIC3               9
#define   IPFWD_ROUTE_METRIC4              10
#define   IPFWD_ROUTE_METRIC5              11
#define   IPFWD_ROUTE_ROW_STATUS           12
#define   IPFWD_ROUTE_PREFERENCE           13
#define   IPFWD_REDIS_CTRL_FLAG            14
#define   IPFWD_ROUTE_STATUS             15

#define RTM_RT_REACHABLE               0x01 /*Indicates whether the route is
                                              reachable best route */
        
#define RTM_NOTIFIED_RT                0x02 /*In ECMP case,this flag indicates whether
                                              the route is  notified route*/

#define RTM_RT_IN_PRT                  0x04 /* Indicates whether route exists */
                                              
#define RTM_ECMP_RT                    0x08 /* Indicates whether the route is an
                                               ECMP route */

#define RTM_RT_IN_ECMPPRT              0x10 /* Indicates whether the route is 
                                               available in ECMPPRT */
                                              
#define RTM_GR_RT                      0x20 /* Indicates whether the route is 
                                               added after graceful restart */
#define RTM_BEST_RT                    0x40 /* Indicates whether the route is best route */ 
#define RTM_RT_HWSTATUS                0x80 /* Indicates whether the route is added in HardwareStatus */ 
#define RTM_REACHABLE                  0x100 /* Indicates whether the route is reachable */
#define RTM_RT_DROP_ROUTE              0x200 /* Indicates the  drop route  */


#define RTM_RT_COUNT_INC               1
#define RTM_RT_COUNT_DEC               0

/* To identify if the route is VRF Leak route */
#define RTM_VRF_LEAKED_ROUTE           1


/* Default Values. */
#define   IPFWD_ROUTE_AGE_DEFAULT           0
#define   IPFWD_ROUTE_INFO_DEFAULT          0
#define   IPFWD_ROUTE_NEXTHOPAS_DEFAULT     0
#define   IPFWD_ROUTE_METRIC_DEFAULT       -1
                                                                                                                             
#define   IP_ACTIVE_TO_NOT_IN_SERVICE       1
#define   IP_NOT_READY_TO_ACTIVE            2
                                                                                                                             
#define   IP_PREFERENCE_INFINITY          255

#define   IP_DONT_REDIS_ROUTE             1 
#define   IP_REDIS_ROUTE                  2


/* Included for multicast routing protocol's PIM and DVMRP */

#define   IP_GEN_BCAST_ADDR                        0xffffffff
#define   IP_LOOPBACK_ADDRESS                      0x7f000000

#define   IP_PKT_OFF_LEN                           2
#define   IP_PKT_OFF_HLEN                          0
#define   IP_HDR_LEN                               20    /* Default hdr len */
/* Minimum value of IP header in Header Length field */
#define   MIN_IP_HDR_LEN_IN_32BIT_WORD             5
/* IP header version field size in bits */
#define   IP_HDR_VER_OFFSET_BITS                   4
#define   IP_PKT_OFF_SRC                           12
#define   IP_VERSION_4                             4
#define   IP_DEF_TTL                               64
#define   OPER_STATE                               1
#define   IP_ADDR_BIT_MASK                         4
#define   IF_MTU_BIT_MASK                          0x00000040
#define   IF_SPEED_BIT_MASK                        0x00000080
#define   IP_ADDR_31BIT_MASK                       0xfffffffe


#define   IP_MCAST_DATA_PACKET_FROM_MRM            10
#define   IP_BCAST                                 1 /* receiving mode */
#define   IP_MCAST                                 2

#define   IP_ANY_ADDR                              0
#define   IPPMTUDISC_DONT                          2 /* Never send DF frames */
#define   IPPMTUDISC_WANT                          4 /* Use per route hints  */
#define   IPPMTUDISC_DO                            8 /* Always DF            */
#define   IPPMTUDISC_ALLOW_FRAG                    32 /* Always DF           */
#define   IPPMTUDISC                               18

#define   CIDR_ROUTE_TABLE                         1
#define   IP_MAX_PHYS_LENGTH                       6


#define   ICMP_TTL_EXCEED                          0
#define   ICMP_DEST_UNREACH                        3 /* Dest unreachable   */
#define   ICMP_ECHO                     8  /* Echo Request            */
#define   ICMP_ECHO_REPLY               0  /* Echo Reply              */
#define   ICMP_REDIRECT                 5  /* Redirect  */
#define   ICMP_MASK_RQST                17 /* Information Request     */
#define   ICMP_MASK_REPLY               18 /* Information Reply       */
#define   ICMP_TIMESTAMP                13 /* Timestamp               */
#define   ICMP_TIME_REPLY               14 /* Timestamp Reply         */


/* ICMP type and codes for errors */ 

#define   ICMP_NET_UNREACH                         0 /* Net unreachable    */
#define   ICMP_HOST_UNREACH                        1 /* Address unreachable */
#define   ICMP_PROTO_UNREACH                       2 /* Protocol unreachable */
#define   ICMP_PORT_UNREACH                        3 /* Port unreachable   */

#define   ICMP_FRAG_NEEDED                         4
#define   ICMP_QUENCH                              4 /* Source Quench      */
#define   ICMP_TIME_EXCEED                         11
#define   ICMP_PARAM_PROB                          12 /* Parameter Problem */

#define   tIP_INTERFACE                            tCRU_INTERFACE
#define   tIP_BUF_CHAIN_HEADER                     tCRU_BUF_CHAIN_HEADER
#define   tIP_APP_TIMER                            tTMO_APP_TIMER
#define   tIP_SLL_NODE                             tTMO_SLL_NODE

#define   UDP_RIP_PORT                             520
/* UDP PTP PORTS */
#define   UDP_PTP_EVENT_PORT                       319
#define   UDP_PTP_GENERAL_PORT                     320
/* UDP packet destination port offset */
#define   UDP_DST_PORT_OFFSET                      2

#define   IPIF_OPER_ENABLE                    1
#define   IPIF_OPER_DISABLE                   2
#define   IPIF_OPER_INVALID                   3

#define   IPIF_ADMIN_ENABLE                   1
#define   IPIF_ADMIN_DISABLE                  2
#define   IPIF_ADMIN_INVALID                  3

#define   MAX_IP_OPT_LEN                           64

#define   IP_INTERFACE_DELETE                      3

#define   IPIF_CREATE_INTERFACE                      1
#define   IPIF_UPDATE_INTERFACE                      2
#define   IPIF_DELETE_INTERFACE                      3
#define   IP_INTERFACE_UP                            4 /* Ctrl msg primitives */
#define   IP_INTERFACE_DOWN                          5
#define   IP_CONTEXT_CREATE                          6
#define   IP_CONTEXT_DELETE                          7
#define   IP_INTERFACE_MAP                           8
#define   IP_INTERFACE_UNMAP                         9
#define   IP_PORT_SPEED_CHANGE                       10
#define   IP_PORT_MTU_CHANGE                         11
#define   IP_SEC_CREATE                          12
#define   IP_SEC_DELETE                          13
#define   IPV4_ENABLE                            14
#define   IPV4_DISABLE                           15
#define   IP_PROXYARP_ADMIN_STATUS_ENABLE        16
#define   IP_PROXYARP_ADMIN_STATUS_DISABLE       17

#define   IP_PROP_ENABLE                           1 /* assigned at start up */
#define   IP_PROP_DISABLE                          2
#define   IP_FORW_ENABLE                           1
#define   IP_FORW_DISABLE                          2

#define   IP_IF_TYPE_ETHERNET                     (6)
#define   IP_IF_TYPE_OTHER                        (CFA_OTHER)
#define   IP_IF_TYPE_PPP                          (23)
#define   IP_IF_TYPE_FR                           (32)
#define   IP_IF_TYPE_X25                          (38)
#define   IP_IF_TYPE_ATM                          (114)

#define   IP_IF_TYPE_L2VLAN                       (135)
#define   IP_IF_TYPE_L3IPVLAN                     (136)
#define   IP_IF_TYPE_LAGG                         (161)
#define   IP_IF_TYPE_PSEUDO_WIRE                  (246)
#define   IP_IF_TYPE_INVALID                       (0)
#define   IP_IF_TYPE_L3SUBIF                      (137)

#define MIN_AS_NUM              0
#define MAX_AS_NUM              65535

/* **************************************
 * The maximum number of configuratable *
 * aggregate addressess and Explicitly  *
 * advertised routes                    *
 * **************************************
 */

#define   MAX_NUM_EXPL_RTS                          10
#define   RMAP_MAX_COMM                             16 /*This should be same as As RMAP_BGP_MAX_COMM*/

/* these definitions are used by IpRtChgNotify () */
#define   IP_BIT_TOS          0x00000001    /* Type of Service */
#define   IP_BIT_ALL          0xFFFF    /* the parameters  */
#define   IP_HEADER_LEN_MASK  0x0f
#define   IP_HEADER_VER_MASK  0xf0  /* IP header version field mask */
/* Changed the values of the following hash defines to be in sync. with RTM 
 * hash defines. */
#define   IP_BIT_RT_TYPE      0x04          /* Route Type      */
#define   IP_BIT_NH_AS        0x08          /* NextHop AS      */
#define   IP_BIT_STATUS       0x10          /* Route status    */
#define   IP_BIT_METRIC       0x20          /* Metric          */
#define   IP_BIT_INTRFAC      0x40          /* Interface       */
#define   IP_BIT_REDISCTRL    0x0100        /* RedisCtrlFlag   */
#define   IP_BIT_GR           0x0200        /* GraceFulRestart */
#define   IP_BIT_NXTHOP       0x0400        /* Next hop        */


#define   DEFIP_LOCAL_RT_CLASS_ID                  5
#define   SET_BIT_FOR_CHANGED_PARAM(u4BitMask, ChangedBit) \
          (u4BitMask) |=  (ChangedBit)       /* do logical OR and *
                                             *  take the result  */
                                            /* - Row Status      */
#define   GET_BIT_FROM_CHANGED_PARAM(u2BitMask, ChangedBit) \
          (u2BitMask) &= (ChangedBit)       /* do logical AND and *
                                             *  take the result  */

#define   MBSM_IP       "MbsmIp"

 /************************************
  * can have less than               * 
  * 10.0.0.1 = 0x1000001 = 16777217  *
  * Unnumbered interface             *
  ************************************/
#define   IPIF_NUMBER_OF_UNNUMBERED_IFACES    100

#define   IP_V4OVERV6_TUNNEL_INTERFACE_TYPE   201
#define   IP_TUNNEL_INTERFACE_TYPE            0x83
#define   IP_PROTO_ID                         4
#define   IP_ROUTER_ALERT_OPTION              148
#define   IP_ROUTER_ALERT_VALUE_OFFSET        2  /* Router alert option value *
                                                  * offset after IP header */

#define   IP_DEFAULT_ROUTER_ID \
          CFA_DEFAULT_ROUTER_VLAN_IFINDEX - (CFA_MIN_IVR_IF_INDEX)

#define   IP_ALLOCATE_BUF(u4Size, u4ValidOffset)                 \
          CRU_BUF_Allocate_MsgBufChain((u4Size), (u4ValidOffset))

#define   IP_RELEASE_BUF(pBuf, u1ForceFlag)                      \
          CRU_BUF_Release_MsgBufChain((pBuf), (u1ForceFlag))

#define   IP_COPY_TO_BUF(pBuf, pSrc, u4Offset, u4Size)           \
          CRU_BUF_Copy_OverBufChain((pBuf),(UINT1 *)(pSrc),(u4Offset),(u4Size))

#define   IP_OLEN(Ver4Hlen4)\
          (((Ver4Hlen4 & 0x0f) * 4) - IP_HDR_LEN)

#define   IP_ALLOCATE_MEM_BLOCK(PoolId) MemAllocMemBlk(PoolId)

#define   IP_PARMS_ALLOC_UDP_SEND_PARMS(pUdpSendParms)     \
          IP_ALLOCATE_MEM_BLOCK(Ip_mems.ParmPoolId, &(pUdpSendParms))

/* check CLASS for IP Address */
#define   IP_IS_ADDR_CLASS_A(u4Addr)   ((u4Addr &  0x80000000) == 0)
#define   IP_IS_ADDR_CLASS_B(u4Addr)   ((u4Addr &  0xc0000000) == 0x80000000)
#define   IP_IS_ADDR_CLASS_C(u4Addr)   ((u4Addr &  0xe0000000) == 0xc0000000)
#define   IP_IS_ADDR_CLASS_D(u4Addr)   ((u4Addr &  0xf0000000) == 0xe0000000)
#define   IP_IS_ADDR_CLASS_E(u4Addr)   ((u4Addr &  0xf0000000) == 0xf0000000)
#define   IP_IS_ADDR_LOOPBACK_RANGE(u4Addr)  ((u4Addr >=  0x7f000000) && (u4Addr <=  0x7fffffff))
#define  IP_ADDRESS_ABOVE_CLASS_C(u4Addr)    (u4Addr > 0xdfffffff)
#define   IP_IS_LOOPBACK(u4Addr)       ((u4Addr &  0x7f000000) == 0x7f000000)

/*  Check for Ip multicast address range */
#define   IP_IS_ADDR_MCAST(u4Addr)\
   (((u4Addr < 0xe0000000) || (u4Addr > 0xefffffff))\
     ? IP_FAILURE : IP_SUCCESS )
  
/*  Check for Resevered multicast address range */
#define   IP_IS_ADDR_RESV_MCAST(u4Addr)\
   (((u4Addr < 0xe0000000) || (u4Addr > 0xe00001ff))\
    ? IP_FAILURE : IP_SUCCESS )

/*  Check for Resevered SSM multicast range */
#define   IP_IS_ADDR_SSM_MCAST(u4Addr)\
   (((u4Addr < 0xe8000000) || (u4Addr > 0xe8ffffff))\
    ? IP_FAILURE : IP_SUCCESS )

#define   IP_PARMS_RELEASE_ICMP_MSG_PARMS(pParms)\
          IP_RELEASE_MEM_BLOCK(Ip_mems.ParmPoolId, (pParms))

#define   IP_RELEASE_MEM_BLOCK(PoolId, pBlk)                     \
          MemReleaseMemBlock((PoolId), (UINT1 *)(pBlk))

#define   IP_PARMS_GET_IFACE_FROM_UDP_TO_APP_MSG_PARMS(pParms)\
          pParms->Parms.Data.u2Port

#define   IP_COPY_FROM_BUF(pBuf, pDst, u4Offset, u4Size)         \
          CRU_BUF_Copy_FromBufChain((pBuf),(UINT1 *)(pDst),(u4Offset), (u4Size))

#define   IP_PKT_GET_SRC(pBuf,u4Src)                                           \
          {                                                                    \
          IP_COPY_FROM_BUF(pBuf,(UINT1 *)&u4Src,IP_PKT_OFF_SRC, sizeof(UINT4));\
          u4Src = IP_NTOHL(u4Src);                                            \
          }
#define   IP_PKT_GET_DEST(pBuf,u4Dst)\
          {                                                                  \
         IP_COPY_FROM_BUF(pBuf, (UINT1 *)&u4Dst, IP_PKT_OFF_DEST, sizeof(UINT4)); \
          u4Dst = IP_NTOHL(u4Dst);                                            \
          }

#define   IP_BUF_MOVE_VALID_OFFSET(pBuf, u4Size) \
          CRU_BUF_Move_ValidOffset((pBuf), (u4Size))

#define   IP_PKT_MOVE_TO_DATA(pBuf)                                          \
          {                                                                   \
          INT1 Ver4Hlen4;                                                    \
          IP_COPY_FROM_BUF(pBuf, &Ver4Hlen4, IP_PKT_OFF_HLEN, sizeof(INT1)); \
          IP_BUF_MOVE_VALID_OFFSET (pBuf, (UINT4) ((Ver4Hlen4 & 0x0f) << 2));        \
          }

#define   IP_VERS_AND_HLEN(i1Version, i2Opt_len) \
          ((i1Version << 4) | (UINT1)((i2Opt_len + IP_HDR_LEN)>>2))



#define   IP_GET_BUF_LENGTH(pBuf) \
          CRU_BUF_Get_ChainValidByteCount((pBuf))

#define   IP_GET_MODULE_DATA_PTR(pBuf) \
          &(pBuf->ModuleData)

#define   IP_CFG_GET_TTL(u4CxtId)         \
             gIpGlobalInfo.apIpCxt[u4CxtId]->Ip_cfg.u1Ttl
#define   IP_CFG_GET_IF_TBL_CHANGE_TIME() \
             (gIpGlobalInfo.u4Ip4IfTableLastChange)
#define   IP_CFG_SET_IF_TBL_CHANGE_TIME()  \
             OsixGetSysTime (&(gIpGlobalInfo.u4Ip4IfTableLastChange))


#define   IP_IS_ZERO_NETWORK(u4Addr)  ((u4Addr & 0xff000000) == 0)

#ifdef TUNNEL_WANTED
#define IP_TNL_CHG_MASK_DIR            0x01
#define IP_TNL_CHG_MASK_DIR_FLAG       0x02
#define IP_TNL_CHG_MASK_ENCAP_OPT      0x04
#define IP_TNL_CHG_MASK_ENCAP_LMT      0x08
#define IP_TNL_CHG_MASK_END_POINT      0x10
#define IP_TNL_CHG_MASK_HOP_LIMIT      0x20
#endif

typedef   tMemPoolId                   tIpMemPoolId;
typedef   tTmrAppTimer                 tTMO_APP_TIMER;
typedef   tCRU_INTERFACE               tIfId;
typedef   tCRU_BUF_CHAIN_HEADER        tIpBuf;

enum eNetIpMcStatsType
{
   NETIPMC_ROUTE_PKTS = 1,
   NETIPMC_ROUTE_DIFF_IF_IN_PKTS,
   NETIPMC_ROUTE_OCTETS,
   NETIPMC_IFACE_IN_OCTETS,
   NETIPMC_IFACE_OUT_OCTETS
};

/* DATA STRUCTURES */
/* --------------- */
/*
 * Whenever IP receives a packet from CFA or from higher level modules
 * it constructs the header in this structure.  Then this header is
 * written into message. This avoids the multiple access to message
 * buffer for same fields.
 * The header length of IP can be up to a maximum of 60 octets. Of this
 * 20 octects are used by other IP fields and 40 octects can be used by
 * options.
 */
typedef struct
{
    UINT4  u4Src;          /* Source address */
    UINT4  u4Dest;         /* Destination address */
    UINT2  u2Len;          /* Total length  IP header + DATA */
    UINT2  u2Id;           /* Identification */
    UINT2  u2Fl_offs;      /* Flags + fragment offset */
    UINT2  u2Cksum;        /* Checksum value */
    UINT2  u2Olen;         /* Length of options field, bytes */
    UINT1  u1Version;      /* IP version number */
    UINT1  u1Hlen;         /* IP header length */
    UINT1  au1Options[IP_MAX_OPT_LEN]; /* Options field */
    UINT1  u1Tos;          /* Preference + Type of service */
    UINT1  u1Ttl;          /* Time to live */
    UINT1  u1Proto;        /* Protocol */
    UINT1  u1Align;        /* For 4 Byte Word Alignment */
} t_IP;

/*
 * IP Header structure. Used for extracting parameters from buffer
 * by directly typecasting to this structure.
 */
typedef struct
{
    UINT1               u1Ver_hdrlen;  /* Version + Header Length */
    UINT1               u1Tos;         /* Type of service */
    UINT2               u2Totlen;      /* Total length  IP header + DATA */
    UINT2               u2Id;          /* Identification */
    UINT2               u2Fl_offs;     /* Flags + fragment offset */
    UINT1               u1Ttl;         /* Time to live */
    UINT1               u1Proto;       /* Protocol */
    UINT2               u2Cksum;       /* Checksum value */
    UINT4               u4Src;         /* Source address */
    UINT4               u4Dest;        /* Destination address */
    UINT1               u1Options[4];  /* Options field */
} t_IP_HEADER;
 
typedef struct sIpCxt tIpCxt;


/*
 * IP allows for a set of filters to be configured
 * for an interface. This allows for allowing or blocking the
 * packets for/from specific hosts on that interface.
 * Filters are devided into lists and each of the interface is
 * assigned a filterlist to be used.
 */

typedef struct
{
    tIP_SLL_NODE  Link;
    UINT4         u4Dest;
    UINT4         u4Dest_mask;
    UINT4         u4Src;
    UINT4         u4Src_mask;
    UINT2         u2Status;
    UINT2         u2AlignmentByte;
}t_IP_FILTER;

typedef struct _NetIpRmapComm
{
    UINT4 au4Community[RMAP_MAX_COMM];
} tRtRmapComm;


/* ***************************************************************************
 * This is a common structure used to retrieve the routing Info from TRIE    *
 * the order of the structure is very important and none shuld be changed    *
 * This is as per RFC 2096.                                                  *
 * We have added u4Gw and have not implemented RouteInfo desrcibed in RFC    *
 * 2096.                                                                     *
 * ***************************************************************************
 */
typedef struct _RtInfo {
   tTMO_SLL_NODE  NextRtInfo;
   VOID          *pNextAlternatepath;
   UINT4        u4DestNet;      /* The destination address                   */
   UINT4        u4DestMask;     /* The net mask to be applied to get the
                                   non-host portion for the destination      */
   UINT4        u4Tos;          /* always zero for RIP                       */
   UINT4        u4NextHop;      /* The nexthop address to which any datagrams
                                   destined to the destination, to be
                                   forwarded. (In some special cases)        */
   UINT4        u4RtIfIndx;     /* The interface through which the route is
                                   learnt                                    */
   UINT2        u2RtType;       /* The route status                          */
   UINT2        u2RtProto;      /* The protocol id                 here      */
   UINT4        u4RtAge;        /* The number of second since this route
                                 * was last updated.
                                 */
   UINT4        u4RtNxtHopAS;   /* The autonomous system number of next hop  */
   INT4         i4Metric1;      /* The reachability cost for the destination */
   UINT4        u4RowStatus;    /* status of the row */
   UINT4        u4RouteTag;
   UINT2        u2RedisMask;    /* Info about the protocols to which this route
                                 * has been redistributed */
   UINT1        u1BitMask;
   UINT1        u1Preference;  /* Contains prefernce */
   UINT4        u4Community;   /* BGP community value                        */
   UINT4        u4LocalPref;   /* Preference value for the Local autonomous  */
                               /* system                                     */
   INT4         i4MetricType;  /* Metric Type, Same is used for ISIS Level   */
   INT4         i4Origin;      /* BGP origin code                            */
   UINT4        u4OriginAsNum; /* BGP Origin Autonomous System Number */
   UINT4        u4Flag;        /* Indicates reachablity of route,Best route & Hardware status */
   UINT4        u4HwIntfId[2]; /*To store Hardware IntfId for active and Standby*/
   INT4         i4RMapFlag;    /* Indicates if the route-map is applied on the 
                                  route */
   UINT1        u1OldPreference;
   UINT1        u1ECMPRt;
   UINT2        u2Weight;
   tRtRmapComm  *pCommunity;/*pointer to route-map community structure*/
   UINT4        au4Community[RMAP_MAX_COMM];
   UINT1        u1LeakRoute;   /* To identify if the route is VRF Leak route */
   UINT1        u1Community;   /* BGP community value                        */
   UINT1        u1CountFlag;
   UINT1        u1Pad[1];
} tRtInfo;

typedef struct
{
  UINT4  u4IpAddr;      
  UINT4  u4SubnetMask;      
  UINT4  u4BcastAddr;      
  UINT4  u4Mtu;      
  UINT4  u4IfSpeed;      
  UINT4  u4PeerIpAddr;      
  UINT4  u4ReleaseStatusFlag; /* Flag is set for release */
#ifdef LNXIP4_WANTED
  UINT4  u4IdxMgrPort;
#endif
  UINT2  u2IfIndex;      
  UINT2  u2Port;      
  UINT2  u2VlanId;      
  UINT1  u1IfType;      
  UINT1  u1Persistence;      
  UINT1  u1EncapType;      
  UINT1  au1Reserved[3];
  UINT1    au1IfName[IP_PORT_NAME_LEN];/* IVR interface name */
  tMacAddr MacAddress;
  UINT2    u2Reserved;
}t_IFACE_PARAMS;


typedef struct
{
    UINT1  u1Cmd;
    UINT1  u1LinkType;
    UINT2  u2Port;
    UINT4  u4ContextId; /* Context Identifier of VRF */ 
}tIpParms;

/*
 * Whenever a packet is to be sent to IP, The higher level 
 * protocols use this structure to pass parameters. The IP 
 * module, after dequeue(iptask.c) releases this message 
 * buffer. See iptask.c, udptask.c.
 */
typedef struct
{
    UINT1  u1Cmd;
    UINT1  u1Df;
    UINT1  u1Ttl;
    UINT1  u1Tos;
    UINT1  u1Proto;
    UINT1  u1Olen;
    UINT2  u2Port;
    UINT2  u2Len;
    UINT2  u2Id;
    UINT4  u4Src;
    UINT4  u4Dest;
    UINT4  u4ContextId;   /* Context Identifier of VRF */  
}t_IP_SEND_PARMS;

/* Added for PIM and DVMRP Multicasting Protocol */
typedef struct McastIpSendParams {
    UINT1  u1Cmd;
    UINT1  u1Proto;
    UINT1  u1Tos;
    UINT1  u1Unused;
    UINT4  u4SrcAddr;
    UINT4  u4DestAddr;
    UINT4  *pu4OIfList;
    UINT2  u2Len;
    UINT1  u1Ttl;
    UINT1  u1Df;
}tMcastIpSendParams;

/*
 * The per interface address information for IP
 * is maintained in this record.  The same record 
 * can be used by multiple interfaces. User 
 * configures an IP address for an interface at 
 * the time of adding it.
 */
typedef struct
{
    UINT4  u4Addr;
    UINT4  u4Net_mask;
    UINT4  u4Bcast_addr;
}t_IP_ADDR_RECORD;

/*
 * Status and statistics information per Interface/Group.
 */
typedef struct
{
    UINT4  u4In_rcvs;         /* Total pkts received on interfaces */
    UINT4  u4In_len_err;      /* Less length */
    UINT4  u4In_cksum_err;    /* Checksum problem */
    UINT4  u4In_hdr_err;
    UINT4  u4In_ver_err;      /* Version mismatch */
    UINT4  u4In_ttl_err;      /* Time to live expired */
    UINT4  u4In_addr_err;     /* Address problems */
    UINT4  u4In_opt_err;      /* Problems in options */
    UINT4  u4In_iface_err;    /* Received If is not in IP's table */
    UINT4  u4In_deliveries;   /* Pkts sent to high level protocols */
    UINT4  u4In_bcasts;       /* Total broadcast pkts received */
    UINT4  u4Out_pkts;        /* Total Number of packets sent */
    UINT4  u4Out_requests;    /* Total send requests made to IP */
    UINT4  u4Out_no_routes;   /* Pkt discard due to no route */
    UINT4  u4Out_gen_err;     /* Iface DOWN , Not forwarding etc */
    UINT4  u4Out_frag_err;    /* To be fragmented and DF flag */
    UINT4  u4Total_filtered;  /* Total datagrams filtered out */
    UINT4  u4Forw_attempts;   /* Pkts considered eligible for forw */
    UINT4  u4Bad_proto;       /* Invalid high level protocol */
    UINT4  u4Reasm_reqs;      /* Total reassembly requests */
    UINT4  u4Reasm_oks;       /* Total reassembly successes */
    UINT4  u4Reasm_fails;     /* Reassembly failures */
    UINT4  u4Reasm_timeout;   /* NOTE have to be processed */
    UINT4  u4Out_frag_pkts;   /* No of outgoing pkts fragmented */
    UINT4  u4Frag_fails;      /* NOTE have to be processed */
    UINT4  u4Frag_creates;    /* NOTE have to be processed */

   /* Routines Added to support the fsip_mib 17/11/98 */
    UINT4  u4InMulticastPkts;
    UINT4  u4OutMulticastPkts;
    UINT4  u4OutBroadcastPkts;
    
    /* Ipvx Counters  */    
    UINT4  u4OutHCBCastPkts;
    UINT4 u4OutForwds;        /* Pkts considered eligible for fwd form 
                                 local IP */
    UINT4 u4HCOutForwds;      /* HC counter for Pkts considered eligible for 
                                 fwd form local IP */
    UINT4 u4InOctets;         /* Total no of IP Octets Rx */
    UINT4 u4HCInOctets;       /* HC counter for Total no of IP Octets Rx */
    UINT4 u4HCIn_rcvs;        /* HC counter for Total no of IP Pkts Rx */
    UINT4 u4In_no_routes;     /* No route for input pkt to the IP Module  */
    UINT4 u4In_Trun_Pkts;     /* datagrams discarded because the datagram
                                 frame didn't carry enough data */ 
    UINT4 u4HCInForwDgrams;   /* HC counter for Pkts considered eligible 
                                 for fwd */
    UINT4 u4InDiscards;       /* No of Pkts discarded  */
    UINT4 u4HCIn_deliveries;  /* HC counter for No of Pkts discarded */   
    UINT4 u4HCOut_requests;   /*  HC counter for Total send requests made
                                  to IP */
    UINT4 u4OutNoRoutes;      /* Local IP Pkt discard due to no 
                                 route (IPvx MIB)*/
    UINT4 u4OutDiscards;      /* No of Pkts discarded form the local IP */ 
    UINT4 u4Out_frag_Reqs;    /* No of Pkts required fragmentaions 
                                 form the local IP */
    UINT4 u4OutFragOKs;       /* Pkts considered eligible for forw  */
    UINT4 u4OutTrans;         /* Total no of IP Pkts to LL for Tx */
    UINT4 u4HCOutTrans;       /* HC counter for Total no of IP Pkts to 
                                 LL for Tx  */
    UINT4 u4OutOctets;        /* Total no of IP Octets to LL for Tx */
    UINT4 u4HCOutOctets;      /* HC counter for Total no of IP Octets 
                                 to LL for Tx  */
    UINT4 u4HCInMcastPkts;    /* HC counter for Number of IP multicast
                                 Pkts Rx */
    UINT4 u4InMcastOctets;    /* Number of IP multicast Octets Rx */ 
    UINT4 u4OutHCMCastPkts;   /* HC counter for Number of IP multicast
                                 Octets Rx */
    UINT4 u4InHCBCastPkts;    /* HC counter for Number of IP Bcast Pkts */ 
    UINT4 u4HCInMcastOctets;  /* HC counter for Number of IP multicast 
                                 Octets Rx */
    UINT4 u4OutMcastOctets;   /* Number of IP multicast Octets Tx */
    UINT4 u4HCOutMcastOctets; /* HC counter for Number of IP multicast 
                               Octets Tx */

}t_IP_IF_STAT;

/*
 * For each of the interfaces (LAN or WAN) the iface structure
 * maintains the information specific to that interface.
 */
typedef struct
{
    UINT4  u4PhysLength;
    UINT4  u4Reasm_max_size;           /* Maximum size of the pkt            */
    UINT2  u2Filter_list;              /* Filter list to be used             */
    UINT1  u1IcmpRedirectEnable;       /* Flag to enable IcmpRedirect        */
    UINT1  u1AlignmentByte;
    UINT1  au1PhysAddr[IP_MAX_PHYS_LENGTH];  /* multilple of 4 bytes          */
    UINT2  u2AlignmentByte;
}t_IP_IFACE_INFO;

typedef struct
{
    tIpCxt           *pIpCxt;            /* Pointer to the ip context to which
                                            this interface is mapped   */
    t_IP_IFACE_INFO  *pIf;               /* Interface config record    */
    t_IP_IF_STAT     *pStat;             /* Interface statistics       */
    UINT2             u2Next;            /* Next member of same group */
    UINT2             u2AlignmentBytes;  /* alignment bytes           */
}t_IP_IFACE_RECORD;



/* This is the shared structure moved from Ipif Table and accessed by 
 * other modules so a separate data structure lock will be provided for this 
 * structure. This is done to provide mutual exclusion */


typedef struct 
{
  tIP_INTERFACE InterfaceId;    
  tIpCxt    *pIpCxt;                    /* Pointer to the ip context to which
                                           this interface is mapped           */
  UINT4  u4Mtu;                     /* Maximum transmittable units        */
  UINT4  u4PeerAddress;             /* Peer Ip Address                    */
  UINT4  u4IfSpeed;                 /* Speed of the interface             */
  UINT2  u2Routing_Protocol;        /* Routing Protocol configured        */
  UINT2  u2Next;                    /* Next member of same group          */
  UINT1  u1Admin;                   /* Admin status                       */
  UINT1  u1Oper;                    /* Operational status                 */
  UINT1  u1IfaceType;               /* Interface Type                     */
  UINT1  u1EncapType;               /* Encapsulation Type                 */
  UINT1  u1IpForwardingEnable;      /* Ip Forwarding Enable Flag          */
  UINT1  u1IpDrtdBcastFwdingEnable; /* Direct Broadcast Forwarding Enable */
  UINT1  u1Persistence;             /* Persistance value */
  UINT1  u1Ipv4EnableStatus;        /* IPv4 is Enabled or Not             */
  UINT1  u1ProxyArpAdminStatus;     /* Proxy ARP Enable/Disable Flag      */
  UINT1  u1LocalProxyArpStatus;     /* Local Proxy ARP Enable/Disable Flag */  
  UINT1  au1Reserved[2];                /* reserved bytes for padding */  
#ifdef TUNNEL_WANTED
  struct _IP_TUNNEL_INTERFACE    *pIpTunlIf;
                                            /* If interface type is Tunnel,
                                             * then this pointer holds
                                             * additional configuration
                                             * value for the tunnel. */
#endif
}t_IPIF_CONFIG;

#ifdef  TUNNEL_WANTED
typedef struct _IP_TUNNEL_INTERFACE
{
    /* configurable parameters */
    UINT4  u4tunlSrc;        /* Tunnel source endpoint address */
    UINT4  u4tunlDst;        /* Tunnel dest endpoint address */
#define  IPV4_GRE_TUNNEL          TNL_TYPE_GRE      /* Configured GRE Tunnel */

    UINT2 u2HopLimit;        /*Hop Limit for tunnel*/
    UINT1 u1TunlType;        /* Type of tunnel - For Fs Code, Tunnel Type
                              * is as defined in inc/cfa.h. */
    UINT1 u1TunlFlag;        /* Flag Defining whether tunnel
                              * is unidirectional or bidirectional.
                              * if 1 = Unidirectional.
                              * else if 2 = Bidirectional.
                              */
    UINT1 u1TunlDir;         /* Incase if tunnel is unidirectional 
                              * flag defining it is out going or incoming.
                              * if 1 = Incoming
                              * else if 2 = Outgoing.
                              */
    UINT1 u1TunlEncaplmt;    /* Encapsulation limit for tunnels*/
    UINT1 u1EncapFlag;       /* Flag indicating if Encapsulation option needs
                              * to be added or not in a tunnel packet
                              */
#define IPV4_ENCAP_OPT_YES  1
#define IPV4_ENCAP_OPT_NO   2
#define IPV4_DEF_ENCAP_LMT  4
   
    UINT1 u1Reserved[1];
}
tIpTunlIf;
#endif /* TUNNEL_WANTED */


typedef struct
{
    tIP_INTERFACE InterfaceId;         /* Interface related information */
    UINT4  u4Addr;                     /* Ip Address of the interface */
    UINT4  u4Net_mask;                 /* Subnetmask */ 
    UINT4  u4Bcast_addr;               /* Broadcast Address */
    UINT4  u4Mtu;                      /* Maximum transmittable units */
    UINT4  u4PeerAddress;             /* Peer Ip Address                    */
    UINT4  u4IfSpeed;                  /* Speed of the interface */
    UINT4  u4ContextId;                /* The context identified to which the 
                                          interface is mapped */
    UINT2  u2Routing_Protocol;         /* Routing Protocol configured */
    UINT1  u1Admin;                    /* Admin status */
    UINT1  u1Oper;                     /* Operational status */
    UINT1  u1InterfaceType;            /* Interface Type */
    UINT1  u1EncapType;               /* Encapsulation Type                 */
    UINT1  u1IpForwardingEnable;       /* Ip Forwarding Enable Flag */
    UINT1  u1IpDrtdBcastFwdingEnable;  /* Direct Broadcast Forwarding enabled */
    UINT1  u1Persistence;             /* Persistance value */
    UINT1  u1ProxyArpAdminStatus;      /* Proxy ARP Enable/Disable Flag      */
    UINT1  u1LocalProxyArpStatus;     /* Local Proxy ARP Enable/Disable Flag */
    UINT1  au1Reserved[1];                /* reserved bytes for padding */
} tIfConfigRecord;


typedef union
{
   struct {
      UINT1 u1NumAddr;      /* IRDP -- Number of Router Addresses    */
      UINT1 u1AddrEntrySize;/* IRDP -- Size of each tuple            */
      UINT2 u2LifeTime;     /* IRDP -- Advertisement Life Time       */
   } Irdp;
   UINT4 u4Unused;         /* Must be zero for source quench message */
   struct {
       UINT2 u2Reserved;   /* Must be zero                           */
       UINT2 u2Pointer;    /* Points to the offending SPI            */
   } SecFailure;
   UINT1 u1Pointer;        /* Indicates the position of the error    */
   UINT4 u4Address;        /* Gateway address for ICMP redirect      */
   struct {
       INT2 i2Id;
       INT2 i2Seq;
   } Identification;       /* Used for identifying ICMP echo messages */
} tIcmpArgs;

/* Internal format of an ICMP header (checksum is missing) */

typedef struct
{
   tIcmpArgs args;
   UINT4     u4Mtu;
   UINT4     u4ContextId;
   INT1      i1Type;
   INT1      i1Code;
   INT1      i1Flag;
   INT1      i1Reserved;
}t_ICMP;
/*
 * UDP control block structure.
 * Along with port it contains information to send message and errors
 * to applications.
 * The control block is a node in pUdp_cb_table which is a hash table.
 */
typedef struct
{
    tTMO_SLL_NODE   NextCBNode;    /* control block node*/
    INT4            i4SockDesc;      /* socket identifier of UDP CB */
    INT4            i4LocalAddrType; /*Local IP address type*/
    INT4            i4RemoteAddrType;/*Remote IP address type*/

    UINT4           u4Local_addr;    /* IP Address */
    UINT4           u4Remote_addr;   /*Remote IP address*/
    UINT4           u4Instance;      /*Instance for the connection*/
    UINT4           u4RcvQEvent;     /* Event to be posted to the task when a
                                      * message is delivered to the ErrQ */
    UINT4           u4ErrQEvent;     /* Event to be posted to the task when a
                                      * message is delivered to the ErrQ */
    UINT2           u2U_port;        /* Port enrolled by user */
    UINT2           u2Remote_port;   /*Remote Port of connection*/
    UINT1           u1Give_errors;   /* Is it necessary pass ICMP errors to
                                      * appln */
    UINT1           u1Padding[3];       /* For byte alignment */
    tOsixSemId      SemId;       /* Sem node Id in multi processor  */
    tOsixTaskId     TaskId;  /* Name of the task making this call */
    tOsixQId        RcvQId;  /* Queue on which data to be sent */
    tOsixQId        u4ErrQId;  /* Queue on which Error to be sent */
} t_UDP_CB;

/* Statistics maintained by UDP module */
typedef struct
{
    UINT4       u4OutPkts;      /* Total number of packets sent from UDP */
    UINT4       u4InPkts;       /* Total number of packets received by UDP 
                                   for which port is found */
    UINT4       u4InSuccess;    /* Number of packets for which port is found */
    UINT4       u4InBcast;      /* Number of broadcast packets received */
    UINT4       u4InTotalErr;   /* Number of broadcast packets received */
    UINT4       u4InErrNoport;  /* Packets dropped due to no applications */
    UINT4       u4InErrCksum;   /* UDP checksum failures */
    UINT4       u4InNoCksum;    /* UDP No checksum packets */
    UINT4       u4InIcmpErr;    /* Number of ICMP error messages received */
    FS_UINT8    u8HcInPkts;     /* High capacity counters to store total 
                                   number of packets received by UDP 
                                   for which port is found */
    FS_UINT8    u8HcOutPkts;    /* High capacity counters to store total 
                                   number of packets sent from UDP */
} t_UDP_STAT;


typedef struct
{
    tTMO_SLL         UdpCxtCbEntry; /* UDP context CB entries list */
    t_UDP_STAT       UdpStat;       /* UDP contexts structures */
    UINT4            u4ContextId;   /* context identifier */
}tUdpCxt;

/*
 * Any IP packet destined to some high level protocols 
 * (UDP,TCP,OSPF)\IP_MEM_GET_BUF (Ip_mems.u2Parm_q_id)
 * is sent with these parameters.
 */
typedef struct
{
    tCRU_BUF_CHAIN_HEADER * pBuf; /* Pointer to Packet buffer */
    UINT1  u1Cmd;
    UINT1  u1Flag;        /* Mode of Receive .. Broadcast , Multicast, etc */
    UINT2  u2Port;
    UINT2  u2Len;
    UINT2  u2ReservedWord;
    UINT4    u4ContextId;   /* Context Identifier */  
}t_IP_TO_HLMS_PARMS;

/*
 * The ICMP error flows to hhigh level modules using this message structure.
 */
typedef struct
{
    tCRU_BUF_CHAIN_HEADER * pBuf; /* Pointer to Packet buffer */
    UINT1  u1Cmd;
    INT1   i1Type;
    INT1   i1Code;
    UINT1  u1ReservedByte;
    UINT4  u4Src;
    UINT4  u4ContextId;   /* Context Identifier of VRF */  
}t_ICMP_ERR_TO_HLMS_PARMS;

typedef struct {
    tIpMemPoolId  IpIfPoolId;
    tIpMemPoolId  ReasmPoolId;
    tIpMemPoolId  FragPoolId;
    tIpMemPoolId  IfacePoolId;
    tIpMemPoolId  IpCxtPoolId;
    tIpMemPoolId  PmtuPoolId;
    tIpMemPoolId  IpIfInfoPoolId;
    tIpMemPoolId  IpIfStatPoolId;
    tIpMemPoolId  AggRtPoolId;
    tIpMemPoolId  AggAdvRtPoolId;
    tIpMemPoolId  IpTnlIfPoolId;
} t_IP_MEMORY;

/*
 * The UDP passes these parameters to the applications while 
 * sending. The applications should release the message.
 */
typedef struct
{
   tCRU_BUF_CHAIN_HEADER * pBuf; /* Pointer to Packet buffer */
   UINT1 u1Cmd;
   UINT1 u1Type_of_msg;
   UINT2 u2SrcUdpPort;
   UINT2 u2DstUdpPort;
   UINT2 u2IpIdentification;
   UINT2 u2Len;
   union
   {
      struct  DATA_PARMS
      {
         UINT2  u2Port;  /* Interface Index ; IP Handle */
      }Data;
      struct  ERR_PARMS
      {
         INT1  i1Type;  /* Error Type  */
         INT1  i1Code;  /* Error Code  */
      }Err;
   }Parms;
   UINT4 u4SrcIpAddr;
   UINT4 u4DstIpAddr;
   UINT4 u4ContextId;         /* context identifier of VRF */
}t_UDP_TO_APP_MSG_PARMS;

typedef struct _CreateParams {
    UINT4  u4Type;
    UINT1  u1KeySize;
    UINT1  u1AppId;
    UINT1  au1Reserved[2];
} tCreateParams;


typedef struct sIpCfg
{
    tIpCxt *pIpCxt;            /* Pointer to ip context                  */
    INT1   i1Forwarding;       /* Is forwarding enabled                  */
    UINT1  u1Ttl;              /* Default time-to-live for IP datagrams  */
    INT1   i1Preference;       /* Prefered route : STATIC or DYNAMIC     */
    INT1   i1Propagate;        /* Allow static route to be propagated ?? */
    INT1   i1Routing_proto;    /* Currently Active Routing Protocol      */
    INT1   i1IpOptProcEnable;  /* To enable or disable Option Processing */
    INT2   i2Reasm_time;       /* Reassembly Timer value                 */
    INT1   i1LoadShareEnable;
    UINT1  u1NumOfMultiPath;
    UINT2  u2AlignmentByte;
} t_IP_CFG;

typedef struct
{
    tCRU_BUF_CHAIN_HEADER * pBuf; /* Pointer to Packet buffer */
    UINT1  u1Cmd;
    UINT1  u1_Cksum_wanted;
    UINT1  u1Tos;
    UINT1  u1Ttl;
    UINT1  u1Df;
    UINT1  u1ReservedByte;
    UINT2  u2Src_u_port;
    UINT2  u2Dest_u_port;
    UINT2  u2Olen;
    UINT2  u2Id;
    UINT2  u2Port;
    UINT4  u4Src;
    UINT4  u4Dest;
    UINT4  u4ContextId; /* Context identifier of VRF */
}t_UDP_SEND_PARMS;

/*
 * Statistics information maintained by IP.
 */
typedef struct
{
    tIpCxt  *pIpCxt;          /* Pointer to IP context to which this statistic
                                 information belongs to */
    UINT4  u4In_rcvs;       /* Total pkts received on interfaces */
    UINT4  u4In_len_err;    /* Less length                       */
    UINT4  u4In_cksum_err;  /* Checksum problem                  */
    UINT4  u4In_hdr_err;
    UINT4  u4In_ver_err;    /* Version mismatch                  */
    UINT4  u4In_ttl_err;    /* Time to live expired              */
    UINT4  u4In_addr_err;   /* Address problems                  */
    UINT4  u4In_opt_err;    /* Problems in options               */
    UINT4  u4In_iface_err;  /* Received If is not in IP's table  */
    UINT4  u4In_deliveries; /* Pkts sent to high level protocols */
    UINT4  u4In_bcasts;     /* Total broadcast pkts received     */
    UINT4  u4Out_pkts;      /* Total Number of packets sent      */
    UINT4  u4Out_requests;  /* Total send requests made to IP    */
    UINT4  u4Out_no_routes; /* Pkt discard due to no route       */
    UINT4  u4Out_gen_err;   /* Iface DOWN , Not forwarding etc   */
    UINT4  u4Out_frag_err;  /* To be fragmented and DF flag      */
    UINT4  u4Total_filtered;/* Total datagrams filtered out      */
    UINT4  u4Forw_attempts; /* Pkts considered eligible for forw */
    UINT4  u4Bad_proto;     /* Invalid high level protocol       */
    UINT4  u4Reasm_reqs;    /* Total reassembly requests         */
    UINT4  u4Reasm_oks;     /* Total reassembly successes        */
    UINT4  u4Reasm_fails;   /* Reassembly failures               */
    UINT4  u4Reasm_timeout; /* NOTE have to be processed         */
    UINT4  u4Out_frag_pkts; /* No of outgoing pkts fragmented    */
    UINT4  u4Frag_fails;    /* NOTE have to be processed         */
    UINT4  u4Frag_creates;  /* NOTE have to be processed         */
    /* IPvx Counters */
    UINT4 u4OutForwds;        /* Pkts considered eligible for fwd form
                                 local IP */
    UINT4 u4HCOutForwds;      /* HC counter for Pkts considered eligible for
                                 fwd form local IP */
    UINT4 u4InOctets;         /* Total no of IP Octets Rx */
    UINT4 u4HCInOctets;       /* HC counter for Total no of IP Octets Rx */
    UINT4 u4HCIn_rcvs;        /* HC counter for Total no of IP Pkts Rx */
    UINT4 u4In_no_routes;     /* No route for input pkt to the IP Module  */
    UINT4 u4In_Trun_Pkts;     /* datagrams discarded because the datagram
                                 frame didn't carry enough data */
    UINT4 u4HCInForwDgrams;   /* HC counter for Pkts considered eligible
                                 for fwd */
    UINT4 u4InDiscards;       /* No of Pkts discarded  */
    UINT4 u4HCIn_deliveries;  /* HC counter for No of Pkts discarded */
    UINT4 u4HCOut_requests;   /* HC counter for Total send requests made
                                 to IP */
    UINT4 u4OutNoRoutes;      /* Local IP Pkt discard due to no
                                 route (IPvx MIB) */
    UINT4 u4OutDiscards;      /* No of Pkts discarded form the local IP */
    UINT4 u4Out_frag_Reqs;    /* No of Pkts required fragmentaions
                                 form the local IP */
    UINT4 u4OutFragOKs;       /* Pkts considered eligible for forw  */
    UINT4 u4OutTrans;         /* Total no of IP Pkts to LL for Tx */
    UINT4 u4HCOutTrans;       /* HC counter for Total no of IP Pkts to
                                 LL for Tx  */
    UINT4 u4OutOctets;        /* Total no of IP Octets to LL for Tx */
    UINT4 u4HCOutOctets;      /* HC counter for Total no of IP Octets
                                 to LL for Tx  */
    UINT4 u4InMcastPkts;      /* Number of IP multicas  Pkts Rx */ 
    UINT4 u4HCInMcastPkts;    /* HC counter for Number of IP multicast
                                 Pkts Rx */
    UINT4 u4InMcastOctets;    /* Number of IP multicast Octets Rx */
    UINT4 u4HCInMcastOctets;  /* HC counter for Number of IP multicast
                                 Octets Rx */
    UINT4 u4OutMcastPkts;     /* Number of IP multicast pkts Tx */
    UINT4 u4HCOutMcastPkts;   /* HC counter for Number of IP multicast 
                                 pkts Tx */
    UINT4 u4OutMcastOctets;   /* Number of IP multicast Octets Tx */ 
    UINT4 u4HCOutMcastOctets; /* HC counter for Number of IP multicast
                                 Octets Tx */
    UINT4 u4HCInBcastPkts;    /* HC counter for Number of IP bcast Pkts Rx */
    UINT4 u4OutBcastPkts;     /* Number of IP bcast pkts Tx */
    UINT4 u4HCOutBcastPkts;   /* HC counter for Number f IP bcast pkts Tx */

}t_IP_STATS;

/*
 * The generic timer structure used by all the modules in IP
 */

 /*
  * Note: t_IP_TIMER order of the fields should not be changed and no
  * alignment should be added because this structure is typecasted by
  * all the Timer related module in IP ( ARP, INARP, IP
  * Reassembly timer, RIP)
  */

typedef struct
{
    tIP_APP_TIMER  Timer_node;
    UINT1          u1Id;             /* To demultiplex between handlers */
    UINT1          u1AlignmentByte;
    UINT2          u2AlignmentByte;
    VOID           *pArg;            /* Pointer to the data structure if any */
} t_IP_TIMER;

typedef struct
{
    tIpAddr   SrcAddr;
    tIpAddr   NbrAddr;
    UINT4     u4IfIndex;
    UINT4     u4PathStatus;
    UINT1     u1AddrType;
    BOOL1     bIsSessCtrlPlaneInDep;
    UINT1     u1Pad [2];
}tClientNbrIpPathInfo;


#ifdef DHCPC_WANTED
#define   FROM_DHCPC_TO_CFA                15
#endif
#define   FROM_IP_TO_CFA                   13

#define   IP_MODULE_ID                           1

/* EXTERNS */
/* ------- */
extern    UINT2                  u2Start_index;

/* MACROS for updating stats */
#define   UPD_IP_IN_RCV()        IP4SYS_INC_IN_RECEVES() 
#define   UPD_IP_IN_ADDR_ERR()   IP4SYS_INC_IN_ADDR_ERRS() 
#define   UPD_IP_FWD()           IP4SYS_INC_IN_FORW_DGRAMS() 
#define   UPD_IP_IN_DISC()       (Ip_stats.u4In_iface_err++)
#define   UPD_IP_OUT_DISC()      (Ip_stats.u4Out_gen_err++)
#define   UPD_IP_FRAG_FAIL()     IP4SYS_INC_OUT_FRAG_FAILS() 

    /** RARP MACROS and prototypes **/

#define   RARP_PROTOCOL                   0x8035
#define   RARP_SUCCESS               SUCCESS   /* as done in the RARP module */
#define   RARP_CLIENT_ENABLE               17
   
    /* timer id for RARP Client */
#define   RARP_CLIENT_TIMER_ID    27

#ifdef IP_WANTED
VOID RarpInit PROTO ((VOID));
VOID RarpDeInit PROTO ((VOID));
INT4 RarpInput PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2Port));
INT1 RarpClientSendRequest PROTO ((UINT2 u2Port));
VOID RarpClientTimerExpiryHandler PROTO ((VOID));
#endif
/* FUNCTION PROTOTYPES */
/* ------------------- */
INT1  IpRegisterHigherLayerProtocol   PROTO ((UINT1 u1ProtocolId,
                                              VOID  (*pAppRcv)
                                              (tIP_BUF_CHAIN_HEADER *, 
                                               UINT2, UINT2, 
                                               tIP_INTERFACE, UINT1),
                                              VOID  (*pIfStChg)(UINT4, UINT4), 
                                              VOID  (*pRtChg)(tRtInfo *, UINT4)));
INT1  IpDeRegisterHigherLayerProtocol PROTO ((UINT1 u1ProtocolId));
VOID  IpifGetHighIpAddress            PROTO ((UINT4 *pu4IfAddress));
VOID  IpifGetHighIpAddressInCxt       PROTO ((UINT4 u4CxtId, UINT4 *pu4IfAddress));

INT4  IpIsLocalNet                   PROTO ((UINT4 u4Addr));
INT4  IpIsLocalNetInCxt              PROTO ((UINT4 u4ContextId, UINT4 u4Addr));
VOID  IpGetFwdTableRouteNum          PROTO ((UINT4 *pu4IpFwdTblRouteNum));
VOID  IpifGetIfaceEntryCount          PROTO ((UINT4 *pu4IfaceEntryCount));
VOID  IpJoinMcastGroup               PROTO ((UINT4 * pMcastGroupAddr, 
                                             UINT4 u4IfIndex));
VOID  IpLeaveMcastGroup              PROTO ((UINT4 * pMcastGroupAddr, 
                                             UINT4 u4IfIndex));


INT4  IpGetRoute                      PROTO ((UINT4 u4Dest,
                                              UINT4 u4Src,
                                              UINT1 u1Proto,
                                              UINT1 u1Tos,
                                              UINT2 *pu2RtPort,
                                              UINT4 *pu4RtGw));

INT4  IpGetRouteInCxt                 PROTO ((UINT4 u4ContextId,
                                              UINT4 u4Dest,
                                              UINT4 u4Src,
                                              UINT1 u1Proto,
                                              UINT1 u1Tos,
                                              UINT2 *pu2RtPort,
                                              UINT4 *pu4RtGw));

INT4 IpGetAdvancedRouteInfo         PROTO  ((UINT4 u4Dest, 
                                             tRtInfo * pOutRtInfo,
                                             UINT4 *pu4Preference));

INT4 IpGetAdvancedRouteInfoInCxt    PROTO  ((UINT4 u4ContextId, UINT4 u4Dest, 
                                             tRtInfo * pOutRtInfo,
                                             UINT4 *pu4Preference));

VOID  IpRtChgNotifyInCxt             PROTO ((UINT4 u4ContextId,
                                             tRtInfo * pRtEntry, 
                                             UINT4 u4BitMap));
VOID IpRtChngNotifyInCxt PROTO ((UINT4 u4ContextId, tRtInfo * pRtEntry, 
                                 tRtInfo * pRtEntry1, UINT1 u1CmdType));

INT4  IP_TSK_Initialize_Protocol(VOID);
PUBLIC INT4 IpEnquePktToIpFromHL      (tIP_BUF_CHAIN_HEADER * pPkt);
PUBLIC INT4 IpEnquePktToIpFromHLWithCxtId      (tIP_BUF_CHAIN_HEADER * pPkt);

INT4  udp_send_srcaddr_in_cxt                PROTO ((UINT4 u4ContextId,
                                              UINT4 u4SrcAddr,
                                              UINT2 u2Src_u_port,
                                              UINT4 u4Dest,
                                              UINT2 u2Dest_u_port,
                                              tCRU_BUF_CHAIN_HEADER *pBuf,
                                              INT2  i2Len,
                                              INT1  i1Tos,
                                              INT1  i1Ttl,
                                              INT2  i2Id,
                                              INT1  i1Df,
                                              INT2  i2Olen,
                                              UINT2 u2Port,
                                              UINT1 u1_Cksum_wanted));

INT4  udp_sli_open_in_cxt              PROTO((UINT1 u1FdUsage,
                                             UINT4 u4contextId,
                                             INT4  i4SockDesc,
                                             UINT2 *pu2UPort,
                                             UINT4 u4Local_addr,
                                             UINT2 *pu2RPort,
                                             UINT4 u4Remote_addr,
                                             tOsixSemId SemId,
                                             tOsixQId u4RcvQId));
INT4  udp_close_srcaddr_in_cxt         PROTO ((UINT4 u4ContextId,
                                              UINT2 u2U_port,
                                              UINT4 u4LocalAddr));
INT4
udpCxtGetFirstIndexEndpointTable (tSNMP_OCTET_STRING_TYPE *pFirstLocalAddress,
                                  UINT4 *pu4FirstPort,
                                  tSNMP_OCTET_STRING_TYPE *pFirstRemoteAddress,
                                  UINT4 *pu4FirstRemotePort,
                                  UINT4 *pu4FirstInstance);
INT4
udpCxtGetNextIndexEndpointTable (tSNMP_OCTET_STRING_TYPE *pUdpLocalAddress,
                             tSNMP_OCTET_STRING_TYPE *pNextLocalAddress,
                             UINT4 u4UdpLocalPort,
                             UINT4 *pu4NextPort,
                             tSNMP_OCTET_STRING_TYPE *pUdpRemoteAddress,
                             tSNMP_OCTET_STRING_TYPE *pNextRemoteAddress,
                             UINT4 u4UdpRemotePort,
                             UINT4 *pu4NextRemotePort,
                             UINT4 u4Instance,
                             UINT4 *pu4Instance);
INT4
UdpMiGetFirstIndexEndpointTable (INT4 *pi4UdpEndpointLocalAddressType,
                   tSNMP_OCTET_STRING_TYPE * pUdpEndpointLocalAddress,
                   UINT4 *pu4UdpEndpointLocalPort,
                   INT4 *pi4UdpEndpointRemoteAddressType,
                   tSNMP_OCTET_STRING_TYPE * pUdpEndpointRemoteAddress,
                   UINT4 *pu4UdpEndpointRemotePort,
                   UINT4 *pu4UdpEndpointInstance);
INT4
UdpMiGetNextIndexEndpointTable
    (INT4 i4UdpEndpointLocalAddressType,
     INT4 *pi4NextUdpEndpointLocalAddressType,
     tSNMP_OCTET_STRING_TYPE * pUdpEndpointLocalAddress,
     tSNMP_OCTET_STRING_TYPE * pNextUdpEndpointLocalAddress,
     UINT4 u4UdpEndpointLocalPort,
     UINT4 *pu4NextUdpEndpointLocalPort,
     INT4 i4UdpEndpointRemoteAddressType,
     INT4 *pi4NextUdpEndpointRemoteAddressType,
     tSNMP_OCTET_STRING_TYPE * pUdpEndpointRemoteAddress,
     tSNMP_OCTET_STRING_TYPE * pNextUdpEndpointRemoteAddress,
     UINT4 u4UdpEndpointRemotePort,
     UINT4 *pu4NextUdpEndpointRemotePort,
     UINT4 u4UdpEndpointInstance,
     UINT4 *pu4NextUdpEndpointInstance);


INT4  icmp_error_msg                        (tCRU_BUF_CHAIN_HEADER *pBuf, 
                                            t_ICMP *pIcmp);

INT4   IpForwardingTableAddRoute      PROTO ((tRtInfo *pIpRoute));
INT4   IpForwardingTableModifyRoute   PROTO ((tRtInfo *pIpRoute));
INT4   IpForwardingTableDeleteRoute   PROTO ((tRtInfo *pIpRoute));

INT4   IpForwardingTableGetRouteInfo  PROTO ((UINT4     u4Dest,
                                              UINT4     u4Mask,
                                              INT1      i1ProtoId,
                                              tRtInfo   **pIpRoutes));
UINT1   IpRegHLProtocolForMCastPkts    PROTO ((VOID (*pAppRcv)
                                              (tCRU_BUF_CHAIN_HEADER *)));
INT4   IpDeRegHLProtocolForMCastPkts  PROTO ((UINT1 u1AppId));
VOID   IpIfaceMapping PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, 
                              UINT1 u1MapType));
INT1 IpIsCxtExist PROTO ((UINT4 u4ContextId));


/* NETIPV4 Macros */

#define NETIPV4_ADD_ROUTE          1
#define NETIPV4_DELETE_ROUTE       2
#define NETIPV4_MODIFY_ROUTE       3

#define NETIPV4_EXACT_ROUTE        1
#define NETIPV4_BEST_ROUTE         2
#define NETIPV4_NO_ROUTE           3

#define NETIPV4_SUCCESS            0
#define NETIPV4_FAILURE           -1

#define MAX_IFNAME_SIZE            36

#define IP_ISIS_LEVEL1            7
#define IP_ISIS_LEVEL2            8
#define IP_ISIS_INTER_AREA      9
#define ARP_MAX_RETRIES           3


/* UNKNOWN Administrative Distance */
#define IP_UNKNOWN_AD                255

/* Route Info Query structure */
typedef struct RouteInfoQueryMsg
{
   UINT4 u4DestinationIpAddress ;
   UINT4 u4DestinationSubnetMask ;
   UINT4 u4ContextId ;
   UINT2 u2AppIds ;
   UINT1 u1QueryFlag ;
   UINT1 u1Pad ;
} tRtInfoQueryMsg ;

/* NETIPV4 STRUCTURES
 * This NetIP structures is used to while acessing the IP Interface related
 * Information .Whenever this record is referred,u4IfIndex refers to
 * Ip Interface Number */
typedef struct
{
    UINT4    u4IfIndex; /* IP Port number/Index number */ 
    UINT4    u4CfaIfIndex; /* Cfa IfIndex */
    UINT4    u4ContextId; /* Vrf context id */
    UINT4    u4Mtu; /* Maximum Transmittable Units */
    UINT4    u4Addr; /* IP Address of the Interface */
    UINT4    u4NetMask; /*  Address Mask */
    UINT4    u4BcastAddr; /* BroadCast Address */
    UINT4    u4IfSpeed; /* Speed of the Interface */
    UINT4    u4IfHighSpeed; /* High Speed of the Interface */
    UINT4    u4RoutingProtocol; /* Routing Protocol Configured */
    UINT4    u4Admin; /* Admin Status */
    UINT4    u4Oper; /* Operational Status */
    UINT4    u4IfType; /* Interface Type */
    UINT4    u4DhcpReleaseStatusFlag; /* Flag is set for release */
    UINT2    u2VlanId; /* Vlan Id for the interface idx */ 
    UINT1    u1CfaIfType; /* CFA interface type. */ 
    UINT1    u1Pad;                  /* reserved bytes for padding */
    UINT1    au1IfName[MAX_IFNAME_SIZE]; /* Interface Name */
    UINT1    u1EncapType;
    UINT1    u1ProxyArpAdminStatus;     /* Proxy ARP Enable/Disable Flag      */
    UINT1    u1LocalProxyArpStatus;    /* Local Proxy ARP Enable/Disable Flag */
    UINT1    au1Align[1];                  /* reserved bytes for padding */
} tNetIpv4IfInfo;

typedef struct
{
    UINT4    u4DestNet;     /* Destination Address */
    UINT4    u4DestMask;    /* Net Mask */
    UINT4    u4Tos;         /* Type of Service */
    UINT4    u4NextHop;     /* NextHop Address */
    UINT4    u4RtIfIndx;    /* Route Interface Index */ 
    UINT4    u4ContextId;   /* Vrf context id */
    UINT4    u4RtAge;       /* The no. of seconds since this route was last
                               updated */
    UINT4    u4RtNxtHopAs;  /* Autonomous System no. of NextHop */
    UINT4    u4RowStatus;   /* Status of the Row */
    UINT4    u4RouteTag;    /* Contents of Route Domain field in Route Entry */
    UINT4    u4SetFlag;     /* This Flag will be set if we are setting any field
                               in Route-Map */
    INT4     i4Metric1;     /* Reachability Cost for the Destination */
    UINT2    u2RtType;      /* Type of Route */
    UINT2    u2RtProto;     /* Routing Mechanism via which the Route is
                             * learnt */
    UINT2    u2ChgBit;      /* Indicates the parameters that are modified. */
    UINT1    u1BitMask;     /* Contains protocol info */
    UINT1    u1Preference;  /* Contains prefernce */
    UINT1    u1OldPreference; /* Contains previous preference value */
    UINT1    u1MetricType;  /* OSPF path type  The same is used 
                               for isis level-1 or level-2*/
    UINT2    u2Weight;
    INT4     i4RMapFlag;    /* Indicates if the route-map is applied on the 
          route */
    UINT4    u4Flag;        /* Indicates reachablity of route,Best route & Hardware status */
    tRtRmapComm  *pCommunity;/*pointer to route-map community structure*/
    UINT1    u1LeakRoute;   /* To identify if the route is VRF Leak route */
    UINT1    u1CommunityCnt; /* Community Count */
    UINT1    u1Pad[2];
} tNetIpv4RtInfo;

typedef  struct  _NetIpRegInfo
{
    VOID (*pIfStChng) (tNetIpv4IfInfo *pNetIpIfInfo, UINT4 u4Mask);
    VOID (*pRtChng) (tNetIpv4RtInfo *pNetRtInfo, tNetIpv4RtInfo *pNetRtInfo1, UINT1 u1CmdType);
    VOID (*pProtoPktRecv) (tIP_BUF_CHAIN_HEADER *, UINT2, UINT4,
                           tIP_INTERFACE, UINT1);
    UINT2       u2InfoMask;
    /* This mask can take the following values */
    #define     NETIPV4_IFCHG_REQ               1
    #define     NETIPV4_ROUTECHG_REQ            2
    #define     NETIPV4_PROTO_PKT_REQ           4
    UINT1       u1ProtoId;      /* Registering Protocol's Identifier */
    INT1        ai1Align[1];    /* Padding for alignment */
    UINT4       u4ContextId;    /* Vrf context id */
} tNetIpRegInfo;

typedef struct _NetIpMcastInfo
{
    UINT4 u4IpPort;           /*Ip Port */
    UINT4 u4Ratelimit;        /*IP Port Rate limit */
    UINT1 u1McastProtocol;    /*Multicast Protocol:PIM_ID, DVMRP_ID */ 
    UINT1 u1TtlThreshold;     /* TTl threshold */
    UINT1 u1BytePad[2];
}tNetIpMcastInfo;     

typedef struct _NetIpOif
{
        UINT4  u4IfIndex;      /* Interface index            */
        UINT2  u2TtlThreshold; /* TTL Threshold value        */
        UINT1  u1BytePad[2];
} tNetIpOif;

typedef struct _NetIpIif
{
        UINT4  u4IfIndex;      /* Interface index            */
        UINT2  u2TtlThreshold; /* TTL Threshold value        */
        UINT1  u1BytePad[2];
} tNetIpIif;

typedef struct _NetIpMcRouteInfo
{
    tNetIpOif *pOIf;  /* Downstram Outgoing interface */
    tNetIpIif *pIIf;  /* Upstream Incoming interface */
    UINT4 u4OifCnt;         /* No. of Oifs */ 
    UINT4 u4IifCnt;         /* No. of Incoming interfaces */ 
    UINT4 u4SrcAddr;        /* Multicast Src Ip */
    UINT4 u4GrpAddr;        /* Multicast Grp Address */
    UINT4 u4Iif;            /* Incoming Interface */
}tNetIpMcRouteInfo;    

/* Structure to register for Multicast packets */
typedef struct _NetIpMcRegInfo
{
   VOID    (*pControlPktRcv) (tCRU_BUF_CHAIN_HEADER *pBuf);
   VOID    (*pDataPktRcv) (tCRU_BUF_CHAIN_HEADER *pBuf);
   UINT4   u4ProtocolId; /* PIM_ID/DVMRP_ID/IPPROTO_IGMP */
}tNetIpMcRegInfo;

extern INT1                 gi1RegTableUp;

/* NETIPV4 Fn. ProtoTypes */

INT4 
NetIpv4GetIfInfo PROTO ((UINT4 u4IfIndex, tNetIpv4IfInfo *pNetIpIfInfo));

#ifdef LNXIP4_WANTED
INT4
NetIpv4GetLinuxIfName PROTO ((UINT4 u4IfIndex, UINT1 *pu1IfName));
#endif

INT4 
NetIpv4GetFirstIfInfo PROTO ((tNetIpv4IfInfo *pNetIpIfInfo));
INT4 
NetIpv4GetFirstIfInfoInCxt PROTO ((UINT4 u4ContextId, tNetIpv4IfInfo *pNetIpIfInfo));

INT4 
NetIpv4GetNextIfInfo PROTO ((UINT4 u4IfIndex,
                             tNetIpv4IfInfo *pNextNetIpIfInfo));
INT4 
NetIpv4GetNextIfInfoInCxt PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex,
                             tNetIpv4IfInfo *pNextNetIpIfInfo));

INT4
NetIpv4GetIfIndexFromAddr PROTO ((UINT4 u4IpAddr, UINT4 *pu4IfIndex));

INT4
NetIpv4GetIfIndexFromAddrInCxt PROTO ((UINT4 u4ContextId, 
                                       UINT4 u4IpAddr, UINT4 *pu4IfIndex));

INT4
NetIpv4LeakRoute PROTO ((UINT1 u1CmdType, tNetIpv4RtInfo *pNetRtInfo));

INT4
NetIpv4RegisterHigherLayerProtocol (tNetIpRegInfo   *pRegInfo);

INT4
NetIpv4DeRegisterHigherLayerProtocol PROTO ((UINT1 u1ProtoId));

INT4
NetIpv4DeRegisterHigherLayerProtocolInCxt PROTO ((UINT4 u4ContextId, 
                                                  UINT1 u1ProtoId));

INT4
NetIpv4GetFirstFwdTableRouteEntry PROTO ((tNetIpv4RtInfo *pNetRtInfo));

INT4
NetIpv4GetFirstFwdTableRouteEntryInCxt (UINT4 u4CxtId, tNetIpv4RtInfo * pNetRtInfo);

INT4
NetIpv4GetNextFwdTableRouteEntry PROTO ((tNetIpv4RtInfo *pNetRtInfo,
                                         tNetIpv4RtInfo *pNextNetRtInfo));

INT4
NetIpv4GetIfIndexFromName PROTO ((UINT1 *pu1IfName, UINT4 *pu4IfIndex));

INT4
NetIpv4GetIfIndexFromNameInCxt PROTO ((UINT4 u4L2ContextId, UINT1 *pu1IfName, 
                                       UINT4 *pu4IfIndex));

INT4
NetIpv4GetIpForwarding (UINT1 *pu1IpForward);

INT4
NetIpv4GetIpForwardingInCxt (UINT4 u4ContextId, UINT1 *pu1IpForward);

INT4
NetIpv4GetIfCount PROTO ((UINT4 *pu4IfCount));

INT4
NetIpv4GetHighestIpAddr PROTO ((UINT4 *pu4IpAddress));

INT4
NetIpv4GetHighestIpAddrInCxt PROTO ((UINT4 u4ContextId, 
                                     UINT4 *pu4IpAddress));

INT4
NetIpv4IfIsOurAddress PROTO ((UINT4 u4IpAddress));

INT4
NetIpv4IfIsOurAddressInCxt PROTO ((UINT4 u4ContextId, UINT4 u4IpAddress));

INT4
NetIpv4IsLoopbackAddress PROTO ((UINT4 u4IpAddress));

INT4
NetIpv4IpIsLocalNet PROTO ((UINT4 u4IpAddress));

INT4
NetIpv4IpIsLocalNetInCxt PROTO ((UINT4 u4ContextId, UINT4 u4IpAddress));

INT4
NetIpv4IpIsLocalNetOnInterface PROTO ((UINT4 u4IfIndex, UINT4 u4IpAddress));

INT4
NetIpv4GetPortFromIfIndex PROTO ((UINT4 u4IfIndex, UINT4 *pu4IfIndex));

INT4
NetIpv4GetCfaIfIndexFromPort PROTO ((UINT4 u4IfIndex, UINT4 *pu4IfIndex));

#ifdef LNXIP4_WANTED
UINT4 
NetIpv4GetIndexFromIndexManager PROTO ((UINT1 u1GroupId));

INT4
NetIpv4ReleaseIndexToIndexManger PROTO ((UINT1 u1GroupId, UINT4 u4Index));
#endif


INT4
NetIpv4GetSrcAddrToUseForDest PROTO ((UINT4 u4Dest, UINT4 *u4pSrc_addr_to_use));

INT4
NetIpv4GetSrcAddrToUseForDestInCxt PROTO ((UINT4 u4ContextId, UINT4 u4Dest, 
                                           UINT4 *u4pSrc_addr_to_use));

INT4
NetIpv4GetRoute  PROTO ((tRtInfoQueryMsg  *pRtQuery,
                         tNetIpv4RtInfo   *pNetIpRtInfo));

INT4
NetIpv4GetNextSecondaryAddress(UINT2   u2Port, UINT4 u4IpAddr, 
                               UINT4 *pu4NextIpAddr, UINT4 *pu4NetMask);

INT4
NetIpv4GetCxtId (UINT4 u4IfIndex, UINT4 *pu4CxtId);

INT4
NetIpv4RegisterMcPacket (tNetIpMcRegInfo *pNetIpMcRegInfo);

INT4
NetIpv4DeRegisterMcPacket (UINT4 u4ProtocolId);

INT4
NetIpv4SetMcStatusOnPort (tNetIpMcastInfo *pNetIpMcastInfo, 
                          UINT4 u4McastStatus);
                          /*u4McastStatus : ENABLED/DISABLED*/
INT4
NetIpv4McastRouteUpdate (tNetIpMcRouteInfo *pIpMcastRoute, UINT4 u4RouteFlag);
                      /*u4RouteFlag: NETIPV4_ADD_ROUTE/NETIPV4_DELETE_ROUTE*/ 
INT4 
NetIpv4McastUpdateCpuPortStatus (UINT4 u4CpuPortStatus);
                      /* u4CpuPortStatus : ENABLED/DISABLED*/
INT4
NetIpv4McastUpdateRouteCpuPort  (tNetIpMcRouteInfo *pIpMcastRoute,
                                  UINT4 u4CpuPortStatus);
                      /*u4CpuPortStatus : ENABLED/DISABLED*/
INT4
NetIpv4McGetRouteStats (UINT4 u4GrpAddr, UINT4 u4SrcAddr, UINT4 u4StatType,
                        UINT4 *pu4StatVal);
INT4
NetIpv4McGetIfaceStats (INT4 i4IfIndex, UINT4 u4StatType, UINT4 *pu4StatVal);

#ifdef VRRP_WANTED
INT4
NetIpv4CreateVrrpInterface (tVrrpNwIntf *pVrrpNwIntf);

INT4
NetIpv4DeleteVrrpInterface (tVrrpNwIntf *pVrrpNwIntf);

INT4
NetIpv4GetVrrpInterface (tVrrpNwIntf *pVrrpNwInIntf, tVrrpNwIntf *pVrrpNwOutIntf);
#endif

VOID RtmTaskMain PROTO ((INT1 * pi1Arg));
#if  defined (IP_WANTED) || defined(LNXIP4_WANTED)
VOID PingTaskMain PROTO ((INT1 *pi1TaskParam));
#endif
VOID Ip_Udp_Task_Main PROTO ((INT1 * pi1Arg));
VOID IPFWDTaskMain PROTO ((INT1 * pi1Arg));

#ifdef MBSM_WANTED
INT4 IpFwdMbsmUpdateCardStatus (tMbsmProtoMsg * pProtoMsg, INT4 i4Event);
VOID IpMbsmUpdateLCStatus (tCRU_BUF_CHAIN_HEADER *pBuf, UINT1 u1Cmd);
INT4 IpRtmMbsmUpdateCardStatus (tMbsmProtoMsg * pProtoMsg, INT4 i4Event);
VOID RtmMbsmUpdateRouteTbl (tCRU_BUF_CHAIN_HEADER *pBuf, UINT1 u1Cmd);
VOID IpMbsmInitProtocols (tMbsmSlotInfo *pSlotInfo);
#endif

/* List of APIs available through future/util. */
UINT2
UtlIpCSumLinBuf   PROTO ((const INT1 *pi1Buf, UINT4 u4Size));
 
UINT2
UtlIpCSumCruBuf   PROTO ((tCRU_BUF_CHAIN_DESC *pBuf, UINT4 u4Size,
                          UINT4 u4Offset));
VOID
UtilConvMcastIP2Mac PROTO ((UINT4 u4GrpAddr, UINT1 *pu1McMacAddr));

VOID UtilGetPortArrayFromPortList PROTO ((UINT1 *pu1PortList, UINT2 u2ListSize,
                                          UINT2 u2MaxPorts, UINT2 *pu2PortArray, 
                                          UINT2 *pu2Count));
VOID UtilDectoHexStr PROTO ((UINT4 u4Value, UINT1 *pu1HexStr));
INT4 UtilHexStrToDecimal PROTO ((UINT1 *pu1Str));
INT4 UtilGetDecValue PROTO ((UINT1 u1Char));


VOID UtilConvertMacToDotStr PROTO ((UINT1 *pu1Mac, UINT1 *pu1DotStr));
VOID UtilConvertIpAddrToStr PROTO ((CHR1 **pString, UINT4 u4Value));

/* Prototypes for Utility Functions */
UINT4 IpGetDefaultBroadcastAddr PROTO ((UINT4 u4Host_Addr));
UINT4 IpGetDefaultNetmask PROTO ((UINT4 u4Host_Addr));
INT1
IpGetProxyArpSubnetOption (INT4 *pi4ProxyArpSubnetOption);
INT4  IpVerifyAddr PROTO ((UINT4 u4Addr, UINT1 u1Flag));
INT4  IpExtractHdr  PROTO ((t_IP *pIp, tCRU_BUF_CHAIN_HEADER *pBuf));

/* Prototypes for Exported Functions from IP */
INT4 
IpGenerateIcmpErrorMesg (tCRU_BUF_CHAIN_HEADER *pBuf, 
                         t_ICMP Icmp, INT1 i1Flag); 
INT4
IpGetIfConfigRecord (UINT2 u2Port, tIfConfigRecord *pIfConfigRecord);
VOID
IpGetIfIdFromIfIndex (UINT4 u4Index, tIP_INTERFACE *pIfId);
INT4
IpGetPortFromIfIndex (UINT4 u4IfIndex, UINT2 *pu2Port);
VOID
ipif_get_iface_id (UINT2 u2Port, tIP_INTERFACE * pIfId);
INT4
IpGetIfIndexFromAddr (UINT4 u4Addr, UINT4 *pu4IfaceIndex);
INT4
IpGetIfIndexFromAddrInCxt (UINT4 u4ContextId, UINT4 u4Addr, UINT4 *pu4IfaceIndex);
INT4
IpGetPortFromAddr (UINT4 u4Addr);

INT4
IpGetPortFromAddrInCxt (UINT4 u4ContextId, UINT4 u4Addr);

INT4
IpifIsOurAddress (UINT4 u4Addr);

INT4
IpifIsOurAddressInCxt (UINT4 u4ContextId, UINT4 u4Addr);

INT4
IpIsLocalAddr (UINT4 u4Addr, UINT2 *pu2Port);

INT4
IpIsLocalAddrInCxt (UINT4 u4ContextId, UINT4 u4Addr, UINT2 *pu2Port);

INT4
IpifIsBroadCastLocal (UINT4 u4Addr);

INT4
IpifIsBroadCastLocalInCxt (UINT4 u4ContextId, UINT4 u4Addr);

INT4 
IpIfSetSecondaryAddressInfo (UINT4 u4IfIndex, UINT4 u4IpAddr, UINT4 u4NetMask,
                             UINT4 u4Flag);

INT4
ip_src_addr_to_use_for_dest (UINT4 u4Dest, UINT4 *u4pSrc_addr_to_use);

INT4
ip_src_addr_to_use_for_dest_InCxt (UINT4 u4ContextId, UINT4 u4Dest, 
                                   UINT4 *u4pSrc_addr_to_use);

UINT4
IpifGetPortFromIfId (tIP_INTERFACE InterfaceId);
INT1
IpifGetFirstPort (UINT2 *pu2Port);
INT1
IpifGetNextPort (UINT2 u2Port, UINT2 *pu2Port);
INT4
IpUpdateInterfaceForwardingStatus (UINT2 u2Port, UINT1 u1IpForwardingState);
INT1
IpGetLoadShareEnable (VOID);
INT4  
IpifIsUnnumbered (UINT2 u2Port, UINT1 *pu1IfType);
INT4   IpifValidateIfIndex (UINT4 u4IfIndex);
UINT2  IpGetFreePort (UINT4);

VOID IpUpdateInHdrErrInCxt (UINT4);

VOID IpUpdateInIfaceErrInCxt (UINT4);

VOID IpUpdateOutIfaceErrInCxt (UINT4);
#ifdef IP_WANTED
/* API to Enable Rarp Client */
INT4 RarpEnableRarpClient (UINT2 u2IfIndex);
INT4 IPvxDeleteIpv4LeakedStaticRouteInCxt PROTO ((INT1 , UINT4, UINT2));
#endif

/* APIs to Take/Release Semaphore UDP Protocol SemaPhore*/
INT4 UdpProtocolLock(VOID);
INT4 UdpProtocolUnLock(VOID);

#ifdef IP_WANTED
#define  UDP_PROT_LOCK     UdpProtocolLock   
#define  UDP_PROT_UNLOCK   UdpProtocolUnLock 
#endif /* IP_WANTED */

#ifdef LNXIP4_WANTED
/* This lock is not needed when LNXIP is enabled,
 * as Linux UDP will be used. */
#define  UDP_PROT_LOCK()
#define  UDP_PROT_UNLOCK()

#define LNXIP_PROXYARP_FILE_NAME "/proc/sys/net/ipv4/conf"


#endif /* LNXIP4_WANTED */

/* IP FWD task lock Semaphore */

/* Prototypes for Semaphore routines */
INT4 IpFwdProtocolLock (VOID);
INT4 IpFwdProtocolUnlock (VOID);

#ifdef IP_WANTED 
#define IPFWD_PROT_LOCK         IpFwdProtocolLock   
#define IPFWD_PROT_UNLOCK       IpFwdProtocolUnlock
#endif /* IP_WANTED */

#ifdef LNXIP4_WANTED
/* This lock is not needed when LNXIP is enabled,
 * as Linux IP stack will be used. */ 
#define IPFWD_PROT_LOCK()
#define IPFWD_PROT_UNLOCK()
#endif /* LNXIP4_WANTED */

/* RTM protocol Lock */
INT4 RtmProtocolLock (VOID);
INT4 RtmProtocolUnlock (VOID);

#define RTM_PROT_LOCK         RtmProtocolLock   
#define RTM_PROT_UNLOCK       RtmProtocolUnlock

INT4
IpHandleInterfaceMsgFromCfa (t_IFACE_PARAMS *pIfaceParams, UINT1 u1Command);

#ifdef LNXIP4_WANTED
UINT1
NetIpv4IsIndexAvailable (UINT1 u1GrpID);
#endif

PUBLIC INT4 IpUpdateInterfaceStatus PROTO((UINT2 u2Port,UINT1* pu1IfName,
                               UINT1 u1OperStatus));

PUBLIC INT4 IpUpdatePortParams PROTO((t_IFACE_PARAMS *pIfaceParams, 
                               UINT2 u2UpdateMask));
/* The APIs Test, Set & Get are being used to do the SNMP operation
   on IpCommonRoutingTable and IpCidrRouteTable and hence being used in
   netip/fsip & netip/ipmgmt */
INT4 IpFwdTblTestObjectInCxt PROTO ((UINT4 u4ContextId,
                                UINT4 u4Dest, UINT4 u4Mask, 
                                INT4 u4Tos, UINT4 u4NextHop, 
                                INT4 i4Proto, INT4 * pi4Val, 
                                UINT1 u1ObjType, UINT4 * pu4ErrorCode));

INT4 IpFwdTblSetObjectInCxt PROTO ((UINT4 u4ContextId, UINT4 u4Dest,
                                    UINT4 u4Mask, INT4 i4Tos, UINT4 u4NextHop,
                                    INT4 i4Proto, INT4 * pi4Val,
                               UINT1 u1ObjType));

INT4 IpFwdTblGetObjectInCxt PROTO ((UINT4 u4ContextId, UINT4 u4Dest,
                                    UINT4 u4Mask, INT4 i4Tos, UINT4 u4NextHop,
                                    INT4 i4Proto, INT4 * pi4Val,
                               UINT1 u1ObjType));



INT4 IpEnableRouting PROTO ((UINT4 u4VrId, UINT1 u1Status));
VOID IpAddAllRtEntries PROTO ((VOID));

#ifdef REMOVE_ON_STDIP_DEPR_OBJ_DELETE
VOID RegisterSTDIP PROTO ((VOID));
#endif

#ifdef LNXIP4_WANTED
INT1 LnxIpSetIfForwarding PROTO ((UINT1 *pu1IfName, INT4 i4SetVal));
INT4 LnxIpNotifyIfInfo (UINT1 u1State, t_IFACE_PARAMS * pIfaceParams);
INT4 LnxIpGetProxyArpAdminStatus(INT4 i4IpifIndex, INT4 *pi4RetValIpifLocalProxyArpAdminStatus);
INT4 LnxIpSetProxyArpAdminStatus (INT4 i4IpifIndex, INT4 i4SetValIpifProxyArpAdminStatus);
INT4 LnxIpTestProxyArpAdminStatus (UINT4 *pu4ErrorCode, INT4 i4IpifIndex,INT4 i4TestValIpifProxyArpAdminStatus);
INT4 LnxGetNextIndexFsMIStdIpifTable (INT4 i4FsMIStdIpifIndex , INT4 *pi4NextFsMIStdIpifIndex);
#endif /* LNXIP4_WANTED */

UINT4 IPvxGetIpv4IfTblLastChange (VOID);
INT4 UdpSelectContext PROTO ((UINT4));
VOID UdpReleaseContext PROTO ((VOID));
INT4 UdpIsValidCxtId PROTO ((UINT4 u4UdpCxtId));
INT4 UdpGetFirstCxtId PROTO ((UINT4 *pu4UdpCxtId));
INT4 UdpGetNextCxtId PROTO ((UINT4 u4UdpCxtId, UINT4 *pu4NextUdpCxtId));
INT4 UdpGetCxtIdFromCxtName PROTO ((UINT1 *pu1CxtName, UINT4 *pu4CxtId));
INT4 UtilIpSetCxt PROTO ((UINT4 u4ContextId));
VOID UtilIpReleaseCxt PROTO ((VOID));


VOID UtilCalcCheckSum (UINT1 *pData, UINT4 u4len,
                                 UINT4 *pu4Sum, UINT2 *u4cSum, UINT4 u4type);
VOID
UtilCompCheckSum (UINT1 *pData, UINT4 *pu4Sum,
                  UINT4 *pu4Last, UINT2 *pu2cSum, UINT4 u4len, UINT4 u4type);
VOID
UtilSetChkSumLastStat (  UINT4 u4InitVal );
PUBLIC INT4
UtilSelectRouterId (UINT4 u4CxtId, UINT4 *pu4RouterId);
INT1 IpAllocateMemory (tSNMP_OCTET_STRING_TYPE **,tSNMP_OCTET_STRING_TYPE **,tSNMP_OID_TYPE **,
                       tSNMP_OCTET_STRING_TYPE **,tSNMP_OCTET_STRING_TYPE **,tSNMP_OID_TYPE **);
VOID IpDeAllocateMemory (tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE *,tSNMP_OID_TYPE *,
                         tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE *,tSNMP_OID_TYPE *);

PUBLIC INT1 IpHandlePathStatusChange PROTO((UINT4 u4ContextId,
                                            tClientNbrIpPathInfo *));

#if defined (LINUX_310_WANTED)
INT4 LnxIpCheckInterfaceStatus (UINT4 u4IpAddr, UINT4 u4ContextId);
#endif

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
INT4 NetIpv4GetIfIndexFromAddrUsingCxt (UINT4 u4IpAddr, UINT4 *pu4IfIndex, UINT4 u4ContextId);
INT4 NetIpv4GetFirstIfInfoUsingCxt (tNetIpv4IfInfo *pNetIpIfInfo, UINT4 u4ContextId);
INT4 NetIpv4GetNextIfInfoUsingCxt (UINT4 u4IfIndex, tNetIpv4IfInfo *pNextNetIpIfInfo, UINT4 u4ContextId);
INT4 NetIpv4GetHighestIpAddrUsingCxt (UINT4 *pu4IpAddress, UINT4 u4ContextId);
INT4 NetIpv4DeRegisterHigherLayerProtocolUsingCxt (UINT1 u1ProtoId, UINT4 u4ContextId);
INT4 NetIpv4GetIpForwardingUsingCxt (UINT1 *pu1IpForward, UINT4 u4ContextId);
#endif

#endif /* _IP_H */
