/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: bgp.h,v 1.49 2018/01/03 11:31:19 siva Exp $
 *
 * Description:This file contains the exported definitions and  
 *             macros of BGP                                   
 *
 *******************************************************************/
#ifndef _BGP_H
#define _BGP_H

#define  BGP4_QUE         "BGPQ"
#define  BGP4_RM_PKT_QUE  "BGPRMQ"
#define  BGP4_Q_NAME_SIZE 5
#define  BGP4_TASKNAME    "Bgp"
#define  BGP_ID           0x0e
#define  BGP4_SUCCESS     0  
#define  BGP4_FAILURE     (-1)  
#define  BGP4_IGNORE    2

#define BGP4_TRUE                1
#define BGP4_FALSE               2
#define BGP4_DFLT_VRFID          0
#define BGP4_RTM6_ROUTE_PROCESS_EVENT           (0x00000020)

/*  Bit masks used for REDISTRIBUTION and IMPORTING of routes
 *  from other protocols. */
#ifdef RRD_WANTED
#define  BGP4_IMPORT_DIRECT       RTM_DIRECT_MASK
#define  BGP4_IMPORT_STATIC       RTM_STATIC_MASK
#define  BGP4_IMPORT_RIP          RTM_RIP_MASK
#define  BGP4_IMPORT_OSPF         RTM_OSPF_MASK
#define  BGP4_IMPORT_ISISL1       RTM_ISISL1_MASK
#define  BGP4_IMPORT_ISISL2       RTM_ISISL2_MASK
#define  BGP4_IMPORT_ISISL1L2     RTM_ISISL1L2_MASK
#define  BGP4_IMPORT_ALL          0xd086
#else
/* Ensure that these values are same as that of the 
 * value provided by RTM */
#define  BGP4_IMPORT_DIRECT       0x0002
#define  BGP4_IMPORT_STATIC       0x0004
#define  BGP4_IMPORT_RIP          0x0080
#define  BGP4_IMPORT_OSPF         0x1000
#define  BGP4_IMPORT_ALL          0x1086
#endif
#define  BGP4_DEFAULT_CXT_ID      0
#define  BGP4_MATCH_EXT           RTM_MATCH_EXT_TYPE
#define  BGP4_MATCH_INT           RTM_MATCH_INT_TYPE
#define  BGP4_MATCH_NSSA          RTM_MATCH_NSSA_TYPE
#define  BGP4_MATCH_ALL           RTM_MATCH_ALL_TYPE


/* OSPF route types */

#define BGP4_OSPF_INTRA_AREA    1
#define BGP4_OSPF_INTER_AREA    2
#define BGP4_OSPF_TYPE_1_EXT    3
#define BGP4_OSPF_TYPE_2_EXT    4
#define BGP4_OSPF_NSSA_1_EXT    5
#define BGP4_OSPF_NSSA_2_EXT    6

/* PEER Type Internally used by various modules */
#define  BGP4_EXTERNAL_PEER    0x01
#define  BGP4_EXT_OR_INT_PEER  0x02
#define  BGP4_INTERNAL_PEER    0x03

/* Install both/more-specifc/less-specific routes */
#define BGP4_INSTALL_MORESPEC_RT  0x01
#define BGP4_INSTALL_LESSSPEC_RT  0x02
#define BGP4_INSTALL_BOTH_RT      0x03

/* Direction */
#define BGP4_INCOMING             0x01
#define BGP4_OUTGOING             0x02

/*ORF Modes */
#define BGP4_CLI_ORF_MODE_RECEIVE 1
#define BGP4_CLI_ORF_MODE_SEND    2
#define BGP4_CLI_ORF_MODE_BOTH    3

#define  BGP4_ORF_PERMIT               0
#define  BGP4_ORF_DENY                 1

/* Filtering Actions */
#define BGP4_ALLOW                0x01
#define BGP4_DENY                 0x02

#define       COMM_SET             2
#define       COMM_SET_NONE        3
#define       COMM_MODIFY          4

#define       EXT_COMM_NONE        1
#define       EXT_COMM_SET         2
#define       EXT_COMM_SET_NONE    3
#define       EXT_COMM_MODIFY      4

#define       BGP4_INET_AFI_IPV4   1
#define       BGP4_INET_AFI_IPV6   2
#define       BGP4_INET_AFI_L2VPN  25

/*TCP-AO neighbor options*/
#define      BGP4_TCP_AO_ICMP_ACCEPT  1
#define      BGP4_TCP_AO_PKT_DISC     2
#define BGP4_VPN4_MAX_VRF_NAME_SIZE  36

#if defined(L3VPN) || defined(VPLSADS_WANTED) \
    || defined (EVPN_WANTED)
#define BGP4_IMPORT_ROUTE_TARGET_TYPE       1
#define BGP4_EXPORT_ROUTE_TARGET_TYPE       2
#endif

#ifdef L3VPN
#define BGP4_BOTH_ROUTE_TARGET_TYPE         3
#define BGP4_ROUTE_TARGET_ADD               1
#define BGP4_ROUTE_TARGET_DEL               2
#define BGP4_EXT_COM_ASNO_LEN               2
#define BGP4_EXT_COM_IPADDR_LEN             4

#define BGP4_VPN4_NORMAL_PEER               0
#define BGP4_VPN4_CE_PEER                   1
#define BGP4_VPN4_PE_PEER                   2


#define BGP4_PEER_LABEL_SEND                1   
#define BGP4_PEER_LABEL_DONOT_SEND          2   
#endif

/* BFD status options */
#define BGP4_BFD_ENABLE   1
#define BGP4_BFD_DISABLE  2

/* Macro for default vrfid *
 * Should be changed once VCM_DEFAULT_CONTEXT gets
 * introduced */
#define BGP_DEF_CONTEXTID                 0 


/*
 *  Map
 *   if ipv6
 *       |- L3VPN+L2VPN
 *       |- L3VPN
 *       |- L2VPN
 *       |- None
 *   else if L3VPN
 *       |- L2VPN
 *       |- None
 *   else if L2VPN
 *       |- None
 */

#ifdef BGP4_IPV6_WANTED

    /* BOTH IPV4 and IPV6 IS SUPPORTED */
    /* Max number of prefix length. ( Maximum of all supported address families) */

    /* Only for IPV4 & IPV6 -- This value needs to be updated accordingly when
     * support for other address families will get added */
    #ifdef VPLSADS_WANTED
        #define BGP4_MAX_INET_ADDRESS_LEN       17 /*BGP4_VPLS_NLRI_LEN 19 Bytes*/
    #else
        #define BGP4_MAX_INET_ADDRESS_LEN       16
    #endif

    #ifdef L3VPN
        #ifdef VPLSADS_WANTED

            #ifdef EVPN_WANTED
            /* Maximum number of address families supported
             * <IPV4, UNICAST>, <VPNv4, UNICAST> <IPV4, LABEL> <IPV6, UNICAST> 
             * <L2VPN,VPLS> and <L2VPN, EVPN> */
                #define BGP4_MPE_ADDR_FAMILY_MAX_INDEX  0x06
            #else
            /* Maximum number of address families supported
             * <IPV4, UNICAST>, <VPNv4, UNICAST> <IPV4, LABEL> <IPV6, UNICAST> 
             * and <L2VPN,VPLS>*/
                #define BGP4_MPE_ADDR_FAMILY_MAX_INDEX  0x05
            #endif
            
        #else
            #ifdef EVPN_WANTED
                /* Maximum number of address families supported
                 * <IPV4, UNICAST>, <VPNv4, UNICAST> <IPV4, LABEL>
                 * <IPV6, UNICAST>and <L2VPN, EVPN> */
                 #define BGP4_MPE_ADDR_FAMILY_MAX_INDEX  0x05
            #else
                /* Maximum number of address families supported
                 * <IPV4, UNICAST>, <VPNv4, UNICAST> <IPV4, LABEL>
                 * and <IPV6, UNICAST> */
                 #define BGP4_MPE_ADDR_FAMILY_MAX_INDEX  0x04
            #endif

        #endif /*End of VPLSADS_WANTED*/

    #else /*Else of L3VPN*/
        #ifdef VPLSADS_WANTED

            #ifdef EVPN_WANTED
                /* Maximum number of address families supported
                * <IPV4, UNICAST> <IPV6, UNICAST> <L2VPN,VPLS> <L2VPN, EVPN> */
                #define BGP4_MPE_ADDR_FAMILY_MAX_INDEX  0x04
            #else
                /* Maximum number of address families supported
                * <IPV4, UNICAST> <IPV6, UNICAST> <L2VPN,VPLS> */
                #define BGP4_MPE_ADDR_FAMILY_MAX_INDEX  0x03
            #endif

        #else 
            
            #ifdef EVPN_WANTED
                /* Maximum number of address families supported
                * <IPV4, UNICAST> <IPV6, UNICAST> <L2VPN, EVPN> */
                #define BGP4_MPE_ADDR_FAMILY_MAX_INDEX  0x03
            #else
                /* Maximum number of address families supported
                * <IPV4, UNICAST> <IPV6, UNICAST>*/
                #define BGP4_MPE_ADDR_FAMILY_MAX_INDEX  0x02
            #endif

        #endif
    #endif /*End of L3VPN*/

#else /*Else of BGP4_IPV6_WANTED*/
    #ifdef L3VPN
        #ifdef VPLSADS_WANTED

            /* VPNv4, IPV4, and L2vpn-vpls IS SUPPORTED */
            #define BGP4_MAX_INET_ADDRESS_LEN      17/* BGP4_VPLS_NLRI_LEN 19 Bytes*/

            #ifdef EVPN_WANTED
                /* Maximum number of address families supported
                * <IPV4, UNICAST> <IPV4, LABEL> <VPNv4, UNICAST> <L2VPN, VPLS> <L2VPN, EVPN> */
                #define BGP4_MPE_ADDR_FAMILY_MAX_INDEX  0x05
            #else
                /* Maximum number of address families supported
                * <IPV4, UNICAST> <IPV4, LABEL> <VPNv4, UNICAST> <L2VPN, VPLS>*/
                #define BGP4_MPE_ADDR_FAMILY_MAX_INDEX  0x04
            #endif

        #else
            /* VPNv4 and IPV4 IS SUPPORTED */
            #define BGP4_MAX_INET_ADDRESS_LEN       12

            #ifdef EVPN_WANTED
                /* Maximum number of address families supported
                * <IPV4, UNICAST> <IPV4, LABEL> <VPNv4, UNICAST> <L2VPN, EVPN>*/
                #define BGP4_MPE_ADDR_FAMILY_MAX_INDEX  0x04
            #else
                /* Maximum number of address families supported
                * <IPV4, UNICAST> <IPV4, LABEL> <VPNv4, UNICAST>*/
                #define BGP4_MPE_ADDR_FAMILY_MAX_INDEX  0x03
            #endif
            
        #endif
    #else /*Else of L3VPN */

        #ifdef VPLSADS_WANTED
            /* ONLY IPV4 IS SUPPORTED */
            #define BGP4_MAX_INET_ADDRESS_LEN      17 /* BGP4_VPLS_NLRI_LEN 19 Bytes*/

            #ifdef EVPN_WANTED
                /* Maximum number of address families supported
                 * <IPV4, UNICAST> <L2VPN, VPLS> <L2VPN, EVPN>*/
                #define BGP4_MPE_ADDR_FAMILY_MAX_INDEX  0x03
            #else
                /* Maximum number of address families supported
                 * <IPV4, UNICAST> <L2VPN, VPLS>*/
                #define BGP4_MPE_ADDR_FAMILY_MAX_INDEX  0x02
            #endif

        #else /*Else of VPLSADS_WANTED*/
            /* ONLY IPV4 IS SUPPORTED */
            #define BGP4_MAX_INET_ADDRESS_LEN       4

            #ifdef EVPN_WANTED
                /* Maximum number of address families supported
                 * <IPV4, UNICAST> <L2VPN, EVPN>*/
                #define BGP4_MPE_ADDR_FAMILY_MAX_INDEX  0x02
            #else
                /* Maximum number of address families supported
                 * <IPV4, UNICAST> */
                #define BGP4_MPE_ADDR_FAMILY_MAX_INDEX  0x01
            #endif

        #endif /*End of VPLSADS_WANTED*/

    #endif /*End of L3VPN */
#endif /* End of BGP4_IPV6_WANTED*/

typedef struct _tAddrPrefix
{
    UINT2   u2Afi;        /* Represents address family */
    UINT2   u2AddressLen; /* Represents the length of the prefix present
                           * (e.g. for IPV4 = 4, IPV6 = 16
                           */
    UINT1   au1Address [BGP4_MAX_INET_ADDRESS_LEN];
                           /* To store Prefix address */
#ifdef VPLSADS_WANTED
    UINT1   au1Pad[3] ;  /*With current code BGP4_MAX_INET_ADDRESS_LEN is 17 bytes*/
#endif
}
tAddrPrefix;

typedef struct _tNetAddress
{
    tAddrPrefix NetAddr;     /* Represents the Prefix address */
    UINT2       u2PrefixLen; /* Represents the Prefix Length */
    UINT2       u2Safi;      /* Represents the sub-sequent address family */
}
tNetAddress;

typedef struct _tBgp4VrfName {
    UINT1          au1VrfName[BGP4_VPN4_MAX_VRF_NAME_SIZE]; /* Vrf Name */
    INT4           i4Length; /* Length of Vrf Name */
} tBgp4VrfName;

/* Structure for storing the BGP params during shutdown */
typedef struct BgpGrSaveConfig
{
    UINT4 u4RestartTime;
    UINT4 u4ShutTime;
}tBgpGrSaveConfig;

typedef struct t_ASPath
{
    tTMO_SLL_NODE       sllNode;
    UINT1               au1ASSegs[BGP4_AS4_SEG_LEN];
    UINT1               u1Type;
    UINT1               u1Length;
    UINT1               au1Pad[2];
}
tAsPath;

typedef struct t_BgpOrfEntry
{
    tRBNodeEmbd   RBNode;
    tAddrPrefix   PeerAddr;
    tAddrPrefix   AddrPrefix;
    UINT4         u4CxtId;
    UINT4         u4SeqNo;
    UINT2         u2Afi;
    UINT1         u1Safi;
    UINT1         u1OrfType;
    UINT1         u1PrefixLen;
    UINT1         u1MinPrefixLen;
    UINT1         u1MaxPrefixLen;
    UINT1         u1Match;
    UINT1         u1IsStatic;
    UINT1         au1Pad [3];
}tBgp4OrfEntry;

#if defined (VPLSADS_WANTED) || defined (EVPN_WANTED)
#define MAX_LEN_RD_VALUE            8  /* route  distinguisher length value */
#define MAX_LEN_RT_VALUE            8  /* route  target length  value */
#define MAX_LEN_ETH_SEG_ID          10 /* Ethernet Segment Identifier */
#define EVPN_AD_ROUTE               1
#define EVPN_MAC_ROUTE              2
#define EVPN_ETH_SEGMENT_ROUTE      4
#endif

#ifdef VPLSADS_WANTED
#ifdef MPLS_WANTED
       /*VPLS-ADS related interfaces with MPLS*/

#define BGP4_VPLS_MAX_VPLS_NAME_SIZE    32

typedef enum
{
      BGP_L2VPN_ADVERTISE_MSG = 1,
      BGP_L2VPN_WITHDRAWN_MSG,
      BGP_L2VPN_VPLS_CREATE,
      BGP_L2VPN_VPLS_UP,
      BGP_L2VPN_VPLS_DOWN,
      BGP_L2VPN_VPLS_DELETE,
      BGP_L2VPN_DOWN,
      BGP_L2VPN_IMPORT_RT_ADD,
      BGP_L2VPN_IMPORT_RT_DELETE,
      BGP_L2VPN_EXPORT_RT_ADD,
      BGP_L2VPN_EXPORT_RT_DELETE,
      BGP_L2VPN_BOTH_RT_ADD,
#ifndef VPLS_GR_WANTED
      BGP_L2VPN_BOTH_RT_DELETE
#endif
#ifdef VPLS_GR_WANTED
      BGP_L2VPN_BOTH_RT_DELETE,
     BGP_L2VPN_EOVPLS
#endif
}l2vpnBgpMsgType;

typedef struct _Bgp4L2VpnEvtInfo
{
    UINT4 u4MsgType; /*Refer enums defined for BGP and L2VPN*/
    UINT4 u4VplsIndex;
    UINT4 u4LabelBase;
    UINT2 u2Mtu;
    UINT2 u2VeId;  
    UINT2 u2VeBaseOffset;
    UINT2 u2VeBaseSize;
    UINT1 au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE];
    UINT1 au1RouteDistinguisher[MAX_LEN_RD_VALUE];
    UINT1 au1RouteTarget[MAX_LEN_RT_VALUE];
    BOOL1 bControlWordFlag;
    UINT1 u1pad[3];
} tBgp4L2VpnEvtInfo;

INT4 Bgp4L2vpnEventHandler (tBgp4L2VpnEvtInfo *pEvtInfo);
#endif   /*MPLS_WANTED*/
#endif   /* VPLSADS_WANTED */

#if (defined(MPLS_WANTED) && defined(VPLSADS_WANTED)) || defined(EVPN_VXLAN_WANTED) || defined(EVPN_WANTED)
typedef struct _tBgp4EvpnRegisterASNInfo
{
 INT4 (*pBgp4NotifyASN) (UINT1 u1ASNType, UINT4 u4ASNValue);
}tBgp4RegisterASNInfo;

VOID Bgp4ASNRegister ( tBgp4RegisterASNInfo *pBgpRegisterInfo);
#endif
#ifdef EVPN_WANTED
VOID
Bgp4EvpnASNRegister (tBgp4RegisterASNInfo * pBgp4EvpnRegisterASNInfo);
VOID
Bgp4IndicateEvpnASNUpdate (UINT1 u1ASNType, UINT4 u4ASNValue);
#endif

INT4
Bgp4TrieQueryIndirectNextHOP (tSNMP_OCTET_STRING_TYPE * ,  UINT2 , UINT1 *);
INT4
Bgp4TrieQueryIndirectNextHOPInCxt (UINT4, tSNMP_OCTET_STRING_TYPE * ,  UINT2 , UINT1 *);
/* Prototype Declaration */
VOID    Bgp4TaskMain (INT1 *);
INT4    Bgp4TaskInitializeProtocol (void);
INT4    Bgp4DeInit (VOID);
#ifdef L3VPN
VOID    BgpMplsLspStatusChangeNotification (UINT4 , UINT1 );
VOID    BgpMplsVrfAdminStatusChangeNotification (UINT4, UINT1);
VOID    BgpMplsRsvpTeLspStatusChangeNotification (UINT4,UINT1);

VOID    BgpMplsRouteParamsNotification (UINT4, UINT1, UINT1, UINT1 *);
#endif /* L3VPN */
#ifdef EVPN_WANTED
VOID    BgpVxlanRouteParamsNotification (UINT4, UINT4, UINT1, UINT1, UINT1 *);
VOID    BgpVxlanVrfAdminStatusChangeNotification (UINT4, UINT4, UINT1);
VOID    Bgp4VxlanEvpnNotifyMac(UINT4, UINT4, tMacAddr,UINT1,UINT1 *,UINT1, UINT1);
VOID    Bgp4VxlanEvpnGetMACNotification (UINT4, UINT4);
VOID    Bgp4VxlanEvpnESINotification (UINT4, UINT4, UINT1 *, UINT1 *, INT4, UINT1);
VOID    Bgp4VxlanEvpnEthADNotification (UINT4, UINT4, UINT1 *, INT4);
#endif
PUBLIC INT4   BgpLock (VOID);
PUBLIC INT4   BgpUnLock (VOID);
PUBLIC VOID   Bgp4GRPlannedShutdownProcess (VOID);
PUBLIC INT4   BgpValidateIpAddr (const CHR1 * pc1Addr, tUtlInAddr * pInAddr);
PUBLIC INT4   BgpValidateIpv4Addr (const CHR1 * pc1Addr, tUtlInAddr * pInAddr);
PUBLIC INT4  Bgp4ValidatePeerGroupName (UINT1 *pu1PeerGroupName);
PUBLIC VOID   Bgp4ApiModuleDisable (VOID);
PUBLIC INT4   Bgp4ApiModuleEnable (VOID);
PUBLIC INT4   Bgp4L3VpnRegister (INT4 (*Bgp4L3VpnNotifyASN) (UINT1 u1ASNType, UINT4 u4ASNValue));
#endif /* _BGP_H */
