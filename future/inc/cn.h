/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: cn.h,v 1.2 2010/07/14 07:34:00 prabuc Exp $
*
* Description: Contains definitions, macros and functions to be used
*              by  external modules.
********************************************************************/
#ifndef _CN_H_
#define _CN_H_

#include "lldp.h"
#include "fswebnm.h"
#include "l2iwf.h"

#define CN_CPID_LEN  8

/* CN system control status */
#define CN_STARTED 1
#define CN_SHUTDOWN 2

/*CN Master Enable/Disable */
#define CN_MASTER_ENABLE  1
#define CN_MASTER_DISABLE 2

/* CN TRAP values */
#define CN_ERROR_PORT_TRAP      1
#define CN_CNM_TRAP             2
#define CN_DEFAULT_TRAP         3 /*Default trap value*/

/* CN Trace levels */
#define   CN_MGMT_TRC            0x00000001
#define   CN_SEM_TRC             0x00000002
#define   CN_TLV_TRC             0x00000004
#define   CN_RESOURCE_TRC        0x00000008
#define   CN_RED_TRC             0x00000010
#define   CN_ALL_FAILURE_TRC     0x00000020
#define   CN_CONTROL_PLANE_TRC   0x00000040
#define   CN_CRITICAL_TRC        0x00000080

#define CN_ALL_TRC               (CN_MGMT_TRC | \
                                  CN_SEM_TRC  | \
                                  CN_TLV_TRC  | \
                                  CN_RESOURCE_TRC | \
                                  CN_RED_TRC | \
                                  CN_ALL_FAILURE_TRC | \
                                  CN_CONTROL_PLANE_TRC | \
                                  CN_CRITICAL_TRC)


#define  CN_PORT_PRI_SHOW       1
#define  CN_COMP_PRI_SHOW       2


/* CN State Event Machine STATES 
 * WARNING: Changing the values of states has direct impact 
 *          on the gau1CnSem definition. So cross check before
 *          modifying these values. */
enum
{
    CN_DISABLED        = 1,
    CN_INTERIOR        = 2,
    CN_INTERIOR_READY  = 3,
    CN_EDGE            = 4,
    CN_MAX_STATES      = 5
};

/*CN Defense Mode Choice */
enum
{
    CN_ADMIN =1,
    CN_AUTO,
    CN_COMP
};

/* CN COMP PRI CREATION */
enum
{
    CN_AUTO_ENABLE = 1,
    CN_AUTO_DISABLE
};

/*CN LLDP CHOICE */
enum
{
    CN_LLDP_NONE = 1,
    CN_LLDP_ADMIN,
    CN_LLDP_COMP
};

enum
{
    CN_CREATE_CXT_MSG = 1,
    CN_DELETE_CXT_MSG,
    CN_CREATE_IF_MSG,
    CN_DELETE_IF_MSG,
    CN_MAP_IF_MSG,
    CN_UNMAP_IF_MSG,
    CN_ADD_LA_MEM_MSG,
    CN_REM_LA_MEM_MSG,

};
/*Congestion Notification Message Parameters*/
typedef struct _CnHwCnmGen
{
    INT4  i4CnmQOffset; /*CNM Offset */
    INT4  i4CnmQDelta;  /*CN Message Delta */
    UINT1 au1CpIdentifier[CN_CPID_LEN]; /*Congestion point Identifier*/
}tCnCnmGen;

/*--------------------------------------------------------------------------*/
/*                       cnmain.c                                           */
/*--------------------------------------------------------------------------*/
PUBLIC VOID CnMainTask           PROTO ((INT1 *pArg));

/*--------------------------------------------------------------------------*/
/*                        cnapi.c                                           */
/*--------------------------------------------------------------------------*/

/* WEB related APIs */

PUBLIC INT4 CnApiContextRequest PROTO ((UINT4 u4ContextId, UINT1 u1Status));

PUBLIC INT4 CnApiPortRequest PROTO ((UINT4 u4ContextId, UINT4 u4PortId, UINT1 u1Status));

PUBLIC INT4 CnApiPortToPortChannelRequest PROTO ((UINT4 u4ContextId, UINT4 u4PortId,
                                           UINT4 u4PortChId, UINT1 u1Status));

PUBLIC VOID CnApiApplCallbkFunc PROTO ((tLldpAppTlv *pLldpAppTlv));

PUBLIC INT4 CnApiNpapiNotifyCnm PROTO ((UINT1 *pu1CpId, INT4 i4CnmQOffset,
                                         INT4 i4CnmQDelta));
PUBLIC INT4 CnApiCopyPortPropertiesToHw PROTO ((UINT4 u4PoIndex));

#ifdef WEBNM_WANTED
PUBLIC VOID CnProcessCnPortSettingsPage    PROTO  ((tHttp * pHttp));
PUBLIC VOID CnProcessCnCpSettingsPage      PROTO  ((tHttp * pHttp));
PUBLIC VOID CnProcessCnStatisticsPage      PROTO  ((tHttp * pHttp));
PUBLIC VOID CnProcessCnDebugPage           PROTO  ((tHttp * pHttp));
#endif /* WEBNM_WANTED */


#endif /* _CN_H_ */

