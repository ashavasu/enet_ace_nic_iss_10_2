
/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: ipv6npwr.h,v 1.9 2017/11/14 07:31:11 siva Exp $
 *
 * Description:This file contains the necessary data strucutures
 *             for packet handling in NPAPI layer
 *             <Part of dual node stacking model>
 *
 *******************************************************************/

#ifndef __IPV6_NP_WR_H__
#define __IPV6_NP_WR_H__

#include "ip6np.h"

/* OPER ID */

#define  FS_NP_IPV6_INIT                                        21
#define  FS_NP_IPV6_DEINIT                                      22
#define  FS_NP_IPV6_NEIGH_CACHE_ADD                             23
#define  FS_NP_IPV6_NEIGH_CACHE_DEL                             24
#define  FS_NP_IPV6_UC_ROUTE_ADD                                25
#define  FS_NP_IPV6_UC_ROUTE_DELETE                             26
#define  FS_NP_IPV6_RT_PRESENT_IN_FAST_PATH                     27
#define  FS_NP_IPV6_UC_ROUTING                                  28
#define  FS_NP_IPV6_GET_STATS                                   29
#define  FS_NP_IPV6_INTF_STATUS                                 30
#define  FS_NP_IPV6_ADDR_CREATE                                 31
#define  FS_NP_IPV6_ADDR_DELETE                                 32
#define  FS_NP_IPV6_TUNL_PARAM_SET                              33
#define  FS_NP_IPV6_ADD_MCAST_M_A_C                             34
#define  FS_NP_IPV6_DEL_MCAST_M_A_C                             35
#define  FS_NP_IPV6_HANDLE_FDB_MAC_ENTRY_CHANGE                 36
#ifdef OSPF3_WANTED
#define  FS_NP_OSPF3_INIT                                       37
#define  FS_NP_OSPF3_DE_INIT                                    38
#endif /* OSPF3_WANTED */
#ifdef RIP6_WANTED
#define  FS_NP_RIP6_INIT                                        39
#endif /* RIP6_WANTED */
#define  FS_NP_IPV6_NEIGH_CACHE_GET                             40
#define  FS_NP_IPV6_NEIGH_CACHE_GET_NEXT                        41
#define  FS_NP_IPV6_UC_GET_ROUTE                                42
#ifdef MBSM_WANTED
#define  FS_NP_MBSM_IPV6_INIT                                   43
#ifdef RIP6_WANTED
#define  FS_NP_MBSM_RIP6_INIT                                   44
#endif /* RIP6_WANTED */
#ifdef OSPF3_WANTED
#define  FS_NP_MBSM_OSPF3_INIT                                  45
#endif /* OSPF3_WANTED */
#define  FS_NP_MBSM_IPV6_NEIGH_CACHE_ADD                        46
#define  FS_NP_MBSM_IPV6_NEIGH_CACHE_DEL                        47
#define  FS_NP_MBSM_IPV6_UC_ROUTE_ADD                           48
#define  FS_NP_MBSM_IPV6_RT_PRESENT_IN_FAST_PATH                49
#define  FS_NP_MBSM_IPV6_INTF_CREATE                            50
#define  FS_NP_MBSM_IPV6_INTF_STATUS                            51
#endif /* MBSM_WANTED */
#define  FS_NP_IPV6_CHECK_HIT_ON_NDC_ENTRY                      52
#define  FS_NP_IPV6_ENABLE                                      53
#define  FS_NP_IPV6_DISABLE                                     54

/* Required arguments list for the ipv6 NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

typedef struct {
    UINT4    u4IfIndex;
    UINT4    u4VrId;
    UINT1 *  pu1Ip6Addr;
    UINT1 *  pu1HwAddr;
    UINT2    u2VlanId;
    UINT1    u1HwAddrLen;
    UINT1    u1ReachStatus;
} tIpv6NpWrFsNpIpv6NeighCacheAdd;

typedef struct {
    UINT4    u4IfIndex;
    UINT4    u4VrId;
    UINT1 *  pu1Ip6Addr;
} tIpv6NpWrFsNpIpv6NeighCacheDel;

typedef struct {
    UINT4    u4IfIndex;
    UINT2    u2VlanId;
    UINT1    au1pad[2];
} tIpv6NpWrFsNpIpv6Enable;

typedef struct {
    UINT4    u4IfIndex;
    UINT2    u2VlanId;
    UINT1    au1pad[2];
} tIpv6NpWrFsNpIpv6Disable;

typedef struct {
    UINT4           u4VrId;
    UINT1 *         pu1Ip6Prefix;
    UINT1 *         pu1NextHop;
    UINT4           u4NHType;
    tFsNpIntInfo *  pIntInfo;
    UINT4           u4HwIntfId[2];
    UINT1           u1PrefixLen;
    UINT1           au1pad[3];
} tIpv6NpWrFsNpIpv6UcRouteAdd;

typedef struct {
    tFsNpRouteInfo *  pRouteInfo;
    UINT4    u4IfIndex;
    UINT4    u4VrId;
    UINT1 *  pu1Ip6Prefix;
    UINT1 *  pu1NextHop;
    UINT1    u1PrefixLen; 
    UINT1    au1pad[3];
} tIpv6NpWrFsNpIpv6UcRouteDelete;

typedef struct {
    UINT4    u4IfIndex;
    UINT4    u4VrId;
    UINT1 *  pu1Ip6Prefix;
    UINT1 *  pu1NextHop;
    UINT1    u1PrefixLen;
    UINT1    au1pad[3];
} tIpv6NpWrFsNpIpv6RtPresentInFastPath;

typedef struct {
    UINT4  u4VrId;
    UINT4  u4RoutingStatus;
} tIpv6NpWrFsNpIpv6UcRouting;

typedef struct {
    UINT4    u4IfIndex;
    UINT4    u4VrId;
    UINT4    u4StatsFlag;
    UINT4 *  pu4StatsValue;
} tIpv6NpWrFsNpIpv6GetStats;

typedef struct {
    UINT4           u4VrId;
    UINT4           u4IfStatus;
    tFsNpIntInfo *  pIntfInfo;
} tIpv6NpWrFsNpIpv6IntfStatus;

typedef struct {
    UINT4    u4IfIndex;
    UINT4    u4VrId;
    UINT4    u4AddrType;
    UINT1 *  pu1Ip6Addr;
    UINT1    u1PrefixLen;
    UINT1    au1pad[3];
} tIpv6NpWrFsNpIpv6AddrCreate;

typedef struct {
    UINT4    u4IfIndex;
    UINT4    u4VrId;
    UINT4    u4AddrType;
    UINT1 *  pu1Ip6Addr;
    UINT1    u1PrefixLen;
    UINT1    au1pad[3];
} tIpv6NpWrFsNpIpv6AddrDelete;

typedef struct {
    UINT4  u4IfIndex;
    UINT4  u4VrId;
    UINT4  u4TunlType;
    UINT4  u4SrcAddr;
    UINT4  u4DstAddr;
} tIpv6NpWrFsNpIpv6TunlParamSet;

typedef struct {
    UINT4    u4IfIndex;
    UINT4    u4VrId;
    UINT1 *  pu1MacAddr;
    UINT1    u1MacAddrLen;
    UINT1    au1pad[3];
} tIpv6NpWrFsNpIpv6AddMcastMAC;

typedef struct {
    UINT4    u4IfIndex;
    UINT4    u4VrId;
    UINT1 *  pu1MacAddr;
    UINT1    u1MacAddrLen;
    UINT1    au1pad[3];
} tIpv6NpWrFsNpIpv6DelMcastMAC;

typedef struct {
    UINT4    u4IfIndex;
    UINT1 *  pu1L3Nexthop;
    UINT1 *  pu1MacAddr;
    UINT4    u4IsRemoved;
} tIpv6NpWrFsNpIpv6HandleFdbMacEntryChange;

typedef struct {
    tNpNDCacheInput     Nd6NpInParam;
    tNpNDCacheOutput *  pNd6NpOutParam;
} tIpv6NpWrFsNpIpv6NeighCacheGet;

typedef struct {
    tNpNDCacheInput     Nd6NpInParam;
    tNpNDCacheOutput *  pNd6NpOutParam;
} tIpv6NpWrFsNpIpv6NeighCacheGetNext;

typedef struct {
    tNpRtm6Input     Rtm6NpInParam;
    tNpRtm6Output *  pRtm6NpOutParam;
} tIpv6NpWrFsNpIpv6UcGetRoute;

typedef struct {
    UINT4       u4IfIndex;
    UINT4       u4VrId;
    UINT1      *pu1Ip6Addr;
    UINT1       u1HitBitStatus;
    UINT1       au1pad[3];
}
tIpv6NpWrFsNpIpv6CheckHitOnNDCacheEntry;

#ifdef MBSM_WANTED 
typedef struct {
    tMbsmSlotInfo *  pSlotInfo;
} tIpv6NpWrFsNpMbsmIpv6Init;

#ifdef RIP6_WANTED
typedef struct {
    tMbsmSlotInfo *  pSlotInfo;
} tIpv6NpWrFsNpMbsmRip6Init;
#endif /* RIP6_WANTED */

#ifdef OSPF3_WANTED
typedef struct {
    tMbsmSlotInfo *  pSlotInfo;
} tIpv6NpWrFsNpMbsmOspf3Init;
#endif /* OSPF3_WANTED */

typedef struct {
    UINT4            u4IfIndex;
    UINT4            u4VrId;
    UINT1 *          pu1Ip6Addr;
    UINT1 *          pu1HwAddr;
    tMbsmSlotInfo *  pSlotInfo;
    UINT1            u1HwAddrLen;
    UINT1            u1ReachStatus;
    UINT2            u2VlanId;
    UINT1            au1pad[3];
} tIpv6NpWrFsNpMbsmIpv6NeighCacheAdd;

typedef struct {
    UINT4            u4IfIndex;
    UINT4            u4VrId;
    UINT1 *          pu1Ip6Addr;
    tMbsmSlotInfo *  pSlotInfo;
} tIpv6NpWrFsNpMbsmIpv6NeighCacheDel;

typedef struct {
    UINT4            u4VrId;
    UINT1 *          pu1Ip6Prefix;
    UINT1 *          pu1NextHop;
    UINT4            u4NHType;
    tFsNpIntInfo *   pIntInfo;
    tMbsmSlotInfo *  pSlotInfo;
    UINT1            u1PrefixLen;
    UINT1            au1pad[3];
} tIpv6NpWrFsNpMbsmIpv6UcRouteAdd;

typedef struct {
    UINT4            u4Index;
    UINT4            u4VrId;
    UINT1 *          pu1Ip6Prefix;
    UINT1 *          pu1NextHop;
    tMbsmSlotInfo *  pSlotInfo;
    UINT1            u1PrefixLen;
    UINT1            au1pad[3];
} tIpv6NpWrFsNpMbsmIpv6RtPresentInFastPath;

typedef struct {
    UINT4            u4IfIndex;
    UINT4            u4VrId;
    tMbsmSlotInfo *  pSlotInfo;
} tIpv6NpWrFsNpMbsmIpv6IntfCreate;

typedef struct {
    UINT4            u4VrId;
    UINT4            u4IfStatus;
    tFsNpIntInfo *   pIntInfo;
    tMbsmSlotInfo *  pSlotInfo;
} tIpv6NpWrFsNpMbsmIpv6IntfStatus;

#endif /* MBSM_WANTED */
typedef struct Ipv6NpModInfo {
union {
    tIpv6NpWrFsNpIpv6NeighCacheAdd    sFsNpIpv6NeighCacheAdd;
    tIpv6NpWrFsNpIpv6NeighCacheDel    sFsNpIpv6NeighCacheDel;
    tIpv6NpWrFsNpIpv6CheckHitOnNDCacheEntry   sFsNpIpv6CheckHitOnNDCacheEntry;
    tIpv6NpWrFsNpIpv6UcRouteAdd    sFsNpIpv6UcRouteAdd;
    tIpv6NpWrFsNpIpv6UcRouteDelete    sFsNpIpv6UcRouteDelete;
    tIpv6NpWrFsNpIpv6RtPresentInFastPath   sFsNpIpv6RtPresentInFastPath;
    tIpv6NpWrFsNpIpv6UcRouting    sFsNpIpv6UcRouting;
    tIpv6NpWrFsNpIpv6GetStats            sFsNpIpv6GetStats;
    tIpv6NpWrFsNpIpv6IntfStatus    sFsNpIpv6IntfStatus;
    tIpv6NpWrFsNpIpv6AddrCreate    sFsNpIpv6AddrCreate;
    tIpv6NpWrFsNpIpv6AddrDelete    sFsNpIpv6AddrDelete;
    tIpv6NpWrFsNpIpv6TunlParamSet    sFsNpIpv6TunlParamSet;
    tIpv6NpWrFsNpIpv6AddMcastMAC    sFsNpIpv6AddMcastMAC;
    tIpv6NpWrFsNpIpv6DelMcastMAC    sFsNpIpv6DelMcastMAC;
    tIpv6NpWrFsNpIpv6HandleFdbMacEntryChange   sFsNpIpv6HandleFdbMacEntryChange;
    tIpv6NpWrFsNpIpv6NeighCacheGet    sFsNpIpv6NeighCacheGet;
    tIpv6NpWrFsNpIpv6NeighCacheGetNext   sFsNpIpv6NeighCacheGetNext;
    tIpv6NpWrFsNpIpv6UcGetRoute    sFsNpIpv6UcGetRoute;
    tIpv6NpWrFsNpIpv6Enable    sFsNpIpv6Enable;
    tIpv6NpWrFsNpIpv6Disable    sFsNpIpv6Disable;
#ifdef MBSM_WANTED
    tIpv6NpWrFsNpMbsmIpv6Init            sFsNpMbsmIpv6Init;
#ifdef RIP6_WANTED
    tIpv6NpWrFsNpMbsmRip6Init            sFsNpMbsmRip6Init;
#endif /* RIP6_WANTED */
#ifdef OSPF3_WANTED 
    tIpv6NpWrFsNpMbsmOspf3Init    sFsNpMbsmOspf3Init;
#endif /* OSPF3_WANTED */
    tIpv6NpWrFsNpMbsmIpv6NeighCacheAdd   sFsNpMbsmIpv6NeighCacheAdd;
    tIpv6NpWrFsNpMbsmIpv6NeighCacheDel   sFsNpMbsmIpv6NeighCacheDel;
    tIpv6NpWrFsNpMbsmIpv6UcRouteAdd    sFsNpMbsmIpv6UcRouteAdd;
    tIpv6NpWrFsNpMbsmIpv6RtPresentInFastPath   sFsNpMbsmIpv6RtPresentInFastPath;
    tIpv6NpWrFsNpMbsmIpv6IntfCreate    sFsNpMbsmIpv6IntfCreate;
    tIpv6NpWrFsNpMbsmIpv6IntfStatus    sFsNpMbsmIpv6IntfStatus;
#endif /* MBSM_WANTED */
    }unOpCode;

#define  Ipv6NpFsNpIpv6NeighCacheAdd    unOpCode.sFsNpIpv6NeighCacheAdd;
#define  Ipv6NpFsNpIpv6NeighCacheDel    unOpCode.sFsNpIpv6NeighCacheDel;
#define  Ipv6NpFsNpIpv6CheckHitOnNDCacheEntry   unOpCode.sFsNpIpv6CheckHitOnNDCacheEntry
#define  Ipv6NpFsNpIpv6UcRouteAdd    unOpCode.sFsNpIpv6UcRouteAdd;
#define  Ipv6NpFsNpIpv6UcRouteDelete    unOpCode.sFsNpIpv6UcRouteDelete;
#define  Ipv6NpFsNpIpv6RtPresentInFastPath   unOpCode.sFsNpIpv6RtPresentInFastPath;
#define  Ipv6NpFsNpIpv6UcRouting    unOpCode.sFsNpIpv6UcRouting;
#define  Ipv6NpFsNpIpv6GetStats    unOpCode.sFsNpIpv6GetStats;
#define  Ipv6NpFsNpIpv6IntfStatus    unOpCode.sFsNpIpv6IntfStatus;
#define  Ipv6NpFsNpIpv6AddrCreate    unOpCode.sFsNpIpv6AddrCreate;
#define  Ipv6NpFsNpIpv6AddrDelete    unOpCode.sFsNpIpv6AddrDelete;
#define  Ipv6NpFsNpIpv6TunlParamSet    unOpCode.sFsNpIpv6TunlParamSet;
#define  Ipv6NpFsNpIpv6AddMcastMAC    unOpCode.sFsNpIpv6AddMcastMAC;
#define  Ipv6NpFsNpIpv6DelMcastMAC    unOpCode.sFsNpIpv6DelMcastMAC;
#define  Ipv6NpFsNpIpv6HandleFdbMacEntryChange  unOpCode.sFsNpIpv6HandleFdbMacEntryChange;
#define  Ipv6NpFsNpIpv6NeighCacheGet    unOpCode.sFsNpIpv6NeighCacheGet;
#define  Ipv6NpFsNpIpv6NeighCacheGetNext   unOpCode.sFsNpIpv6NeighCacheGetNext;
#define  Ipv6NpFsNpIpv6UcGetRoute    unOpCode.sFsNpIpv6UcGetRoute;
#define  Ipv6NpFsNpIpv6Enable    unOpCode.sFsNpIpv6Enable;
#define  Ipv6NpFsNpIpv6Disable    unOpCode.sFsNpIpv6Disable;
#ifdef MBSM_WANTED 
#define  Ipv6NpFsNpMbsmIpv6Init    unOpCode.sFsNpMbsmIpv6Init;
#ifdef RIP6_WANTED
#define  Ipv6NpFsNpMbsmRip6Init    unOpCode.sFsNpMbsmRip6Init;
#endif /* RIP6_WANTED */
#ifdef OSPF3_WANTED
#define  Ipv6NpFsNpMbsmOspf3Init    unOpCode.sFsNpMbsmOspf3Init;
#endif /* OSPF3_WANTED */
#define  Ipv6NpFsNpMbsmIpv6NeighCacheAdd   unOpCode.sFsNpMbsmIpv6NeighCacheAdd;
#define  Ipv6NpFsNpMbsmIpv6NeighCacheDel   unOpCode.sFsNpMbsmIpv6NeighCacheDel;
#define  Ipv6NpFsNpMbsmIpv6UcRouteAdd    unOpCode.sFsNpMbsmIpv6UcRouteAdd;
#define  Ipv6NpFsNpMbsmIpv6RtPresentInFastPath  unOpCode.sFsNpMbsmIpv6RtPresentInFastPath;
#define  Ipv6NpFsNpMbsmIpv6IntfCreate    unOpCode.sFsNpMbsmIpv6IntfCreate;
#define  Ipv6NpFsNpMbsmIpv6IntfStatus    unOpCode.sFsNpMbsmIpv6IntfStatus;
#endif /* MBSM_WANTED */
} tIpv6NpModInfo;


/* Function Prototype for Generic NP OpCode specific wrappers
 * defined in Ipv6npapi.c */

UINT1 Ipv6FsNpIpv6Init PROTO ((VOID));
UINT1 Ipv6FsNpIpv6Deinit PROTO ((VOID));
UINT1 Ipv6FsNpIpv6NeighCacheAdd PROTO ((UINT4 u4VrId, UINT1 * pu1Ip6Addr, UINT4 u4IfIndex, UINT1 * pu1HwAddr, UINT1 u1HwAddrLen, UINT1 u1ReachStatus, UINT2 u2VlanId));
UINT1 Ipv6FsNpIpv6NeighCacheDel PROTO ((UINT4 u4VrId, UINT1 * pu1Ip6Addr, UINT4 u4IfIndex));
UINT1 Ipv6FsNpIpv6CheckHitOnNDCacheEntry PROTO ((UINT4 u4VrId, UINT1 *pu1Ip6Addr, UINT4 u4IfIndex));

UINT1 Ipv6FsNpIpv6UcRouteAdd PROTO ((UINT4 u4VrId, UINT1 * pu1Ip6Prefix, UINT1 u1PrefixLen, UINT1 * pu1NextHop, UINT4 u4NHType, tFsNpIntInfo * pIntInfo));
UINT1 Ipv6FsNpIpv6UcRouteDelete PROTO ((UINT4 u4VrId, UINT1 * pu1Ip6Prefix, UINT1 u1PrefixLen, UINT1 * pu1NextHop, UINT4 u4IfIndex, tFsNpRouteInfo * pRouteInfo));
UINT1 Ipv6FsNpIpv6RtPresentInFastPath PROTO ((UINT4 u4VrId, UINT1 * pu1Ip6Prefix, UINT1 u1PrefixLen, UINT1 * pu1NextHop, UINT4 u4IfIndex));
UINT1 Ipv6FsNpIpv6UcRouting PROTO ((UINT4 u4VrId, UINT4 u4RoutingStatus));
UINT1 Ipv6FsNpIpv6GetStats PROTO ((UINT4 u4VrId, UINT4 u4IfIndex, UINT4 u4StatsFlag, UINT4 * pu4StatsValue));
UINT1 Ipv6FsNpIpv6IntfStatus PROTO ((UINT4 u4VrId, UINT4 u4IfStatus, tFsNpIntInfo * pIntfInfo));
UINT1 Ipv6FsNpIpv6AddrCreate PROTO ((UINT4 u4VrId, UINT4 u4IfIndex, UINT4 u4AddrType, UINT1 * pu1Ip6Addr, UINT1 u1PrefixLen));
UINT1 Ipv6FsNpIpv6AddrDelete PROTO ((UINT4 u4VrId, UINT4 u4IfIndex, UINT4 u4AddrType, UINT1 * pu1Ip6Addr, UINT1 u1PrefixLen));
UINT1 Ipv6FsNpIpv6TunlParamSet PROTO ((UINT4 u4VrId, UINT4 u4IfIndex, UINT4 u4TunlType, UINT4 u4SrcAddr, UINT4 u4DstAddr));
UINT1 Ipv6FsNpIpv6AddMcastMAC PROTO ((UINT4 u4VrId, UINT4 u4IfIndex, UINT1 * pu1MacAddr, UINT1 u1MacAddrLen));
UINT1 Ipv6FsNpIpv6DelMcastMAC PROTO ((UINT4 u4VrId, UINT4 u4IfIndex, UINT1 * pu1MacAddr, UINT1 u1MacAddrLen));
UINT1 Ipv6FsNpIpv6HandleFdbMacEntryChange PROTO ((UINT1 * pu1L3Nexthop, UINT4 u4IfIndex, UINT1 * pu1MacAddr, UINT4 u4IsRemoved));
UINT1 Ipv6FsNpIpv6Enable PROTO ((UINT4 u4IfIndex, UINT2  u2VlanId));
UINT1 Ipv6FsNpIpv6Disable PROTO ((UINT4 u4IfIndex, UINT2  u2VlanId));

#ifdef OSPF3_WANTED
UINT1 Ipv6FsNpOspf3Init PROTO ((VOID));
UINT1 Ipv6FsNpOspf3DeInit PROTO ((VOID));
#endif /* OSPF3_WANTED */
#ifdef RIP6_WANTED
UINT1 Ipv6FsNpRip6Init PROTO ((VOID));
#endif /* RIP6_WANTED */
UINT1 Ipv6FsNpIpv6NeighCacheGet PROTO ((tNpNDCacheInput Nd6NpInParam, tNpNDCacheOutput * pNd6NpOutParam));
UINT1 Ipv6FsNpIpv6NeighCacheGetNext PROTO ((tNpNDCacheInput Nd6NpInParam, tNpNDCacheOutput * pNd6NpOutParam));
UINT1 Ipv6FsNpIpv6UcGetRoute PROTO ((tNpRtm6Input Rtm6NpInParam, tNpRtm6Output * pRtm6NpOutParam));
#ifdef MBSM_WANTED
UINT1 Ipv6FsNpMbsmIpv6Init PROTO ((tMbsmSlotInfo * pSlotInfo));
#ifdef RIP6_WANTED
UINT1 Ipv6FsNpMbsmRip6Init PROTO ((tMbsmSlotInfo * pSlotInfo));
#endif /* RIP6_WANTED */
#ifdef OSPF3_WANTED
UINT1 Ipv6FsNpMbsmOspf3Init PROTO ((tMbsmSlotInfo * pSlotInfo));
#endif /* OSPF3_WANTED */
UINT1 Ipv6FsNpMbsmIpv6NeighCacheAdd PROTO ((UINT4 u4VrId, UINT1 * pu1Ip6Addr, UINT4 u4IfIndex, UINT1 * pu1HwAddr, UINT1 u1HwAddrLen, UINT1 u1ReachStatus, UINT2 u2VlanId, tMbsmSlotInfo * pSlotInfo));
UINT1 Ipv6FsNpMbsmIpv6NeighCacheDel PROTO ((UINT4 u4VrId, UINT1 * pu1Ip6Addr, UINT4 u4IfIndex, tMbsmSlotInfo * pSlotInfo));
UINT1 Ipv6FsNpMbsmIpv6UcRouteAdd PROTO ((UINT4 u4VrId, UINT1 * pu1Ip6Prefix, UINT1 u1PrefixLen, UINT1 * pu1NextHop, UINT4 u4NHType, tFsNpIntInfo * pIntInfo, tMbsmSlotInfo * pSlotInfo));
UINT1 Ipv6FsNpMbsmIpv6RtPresentInFastPath PROTO ((UINT4 u4VrId, UINT1 * pu1Ip6Prefix, UINT1 u1PrefixLen, UINT1 * pu1NextHop, UINT4 u4Index, tMbsmSlotInfo * pSlotInfo));
UINT1 Ipv6FsNpMbsmIpv6IntfCreate PROTO ((UINT4 u4VrId, UINT4 u4IfIndex, tMbsmSlotInfo * pSlotInfo));
UINT1 Ipv6FsNpMbsmIpv6IntfStatus PROTO ((UINT4 u4VrId, UINT4 u4IfStatus, tFsNpIntInfo * pIntInfo, tMbsmSlotInfo * pSlotInfo));
#endif /* MBSM_WANTED */
#ifdef CFA_WANTED
UINT1 Ipv6FsCfaHwRemoveIp6NetRcvdDlfInHash PROTO ((UINT4 u4ContextId,tIp6Addr Ip6Addr, UINT4 u4Prefix));
#endif
#endif /* __IPv6_NP_WR_H__ */
