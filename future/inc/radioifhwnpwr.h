/********************************************************************
 * Copyright (C) Future Software Limited, 2001
 *
 * $Id: radioifhwnpwr.h,v 1.41 2017/12/21 10:20:17 siva Exp $
 *******************************************************************/

#include "nputil.h"

#ifndef __RADIOIF_NP_WR_H__
#define __RADIOIF_NP_WR_H__

typedef enum {
        FS_RADIO_HW_INIT, 
        FS_RADIO_HW_DE_INIT,            
        FS_RADIO_HW_UPDATE_ADMIN_STATUS_ENABLE, 
        FS_RADIO_HW_UPDATE_ADMIN_STATUS_DISABLE,   
        FS_RADIO_HW_SET_ANTENNA_MODE,              
        FS_RADIO_HW_GET_ANTENNA_MODE,              
        FS_RADIO_HW_GET_CURR_TX_ANTENNA,           
        FS_RADIO_HW_SET_ANTENNA_SELECTION,         
        FS_RADIO_HW_GET_ANTENNA_SELECTION,         
        FS_RADIO_HW_SET_ANTENNA_DIVERSITY,         
        FS_RADIO_HW_GET_ANTENNA_DIVERSITY,         
        FS_RADIO_HW_SET_RTS_THRESHOLD,            
        FS_RADIO_HW_GET_RTS_THRESHOLD,            
        FS_RADIO_HW_SET_FRAG_THRESHOLD,           
        FS_RADIO_HW_GET_FRAG_THRESHOLD,           
        FS_RADIO_HW_GET_SHORT_RETRY_LIMIT,        
        FS_RADIO_HW_SET_LONG_RETRY_LIMIT,         
        FS_RADIO_HW_GET_LONG_RETRY_LIMIT,         
        FS_RADIO_HW_SET_TX_CURR_POWER,            
        FS_RADIO_HW_GET_TX_CURR_POWER,             
        FS_RADIO_HW_SET_OFDM_CHANNEL_WIDTH,        
        FS_RADIO_HW_GET_OFDM_CHANNEL_WIDTH,        
        FS_RADIO_HW_SET_BEACON_PERIOD,             
        FS_RADIO_HW_GET_BEACON_PERIOD,             
        FS_RADIO_HW_SET_EDCA_CW_MIN,               
        FS_RADIO_HW_GET_EDCA_CW_MIN,               
        FS_RADIO_HW_SET_EDCA_CW_MAX,               
        FS_RADIO_HW_GET_EDCA_CW_MAX,               
        FS_RADIO_HW_SET_EDCA_AIFS,                 
        FS_RADIO_HW_GET_EDCA_AIFS,                 
        FS_RADIO_HW_SET_EDCA_TXOP_LIMIT,           
        FS_RADIO_HW_GET_EDCA_TXOP_LIMIT,           
        FS_RADIO_HW_SET_EDCA_ADMN_CTRL,            
        FS_RADIO_HW_GET_EDCA_ADMN_CTRL,            
        FS_RADIO_HW_SET_HIDE_ESSID,                
        FS_RADIO_HW_GET_HIDE_ESSID,                
        FS_RADIO_HW_SET_DTIM_PERIOD,               
        FS_RADIO_HW_GET_DTIM_PERIOD,               
        FS_RADIO_HW_SET_STA_WEP_KEY_EN,            
        FS_RADIO_HW_GET_STA_WEP_KEY_EN,            
        FS_RADIO_HW_SET_STA_WEP_KEY_AUTH,          
        FS_RADIO_HW_GET_STA_WEP_KEY_AUTH,          
        FS_RADIO_HW_SET_STA_WEP_KEY_STAT,          
        FS_RADIO_HW_GET_STA_WEP_KEY_STAT,          
        FS_RADIO_HW_SET_WLAN_SEC_PRIV_STAT,        
        FS_RADIO_HW_GET_WLAN_SEC_PRIV_STAT,        
        FS_RADIO_HW_SET_WLAN_SEC_EX_UN_EN_STAT,    
        FS_RADIO_HW_GET_WLAN_SEC_EX_UN_EN_STAT,    
        FS_RADIO_HW_SET_WLAN_AN_SUPP_MODE,         
        FS_RADIO_HW_GET_WLAN_AN_SUPP_MODE,         
        FS_RADIO_HW_SET_WLAN_N_AMPDU_SUP_STAT,     
        FS_RADIO_HW_GET_WLAN_N_AMPDU_SUP_STAT,     
        FS_RADIO_HW_SET_CWM_STATUS,                
        FS_RADIO_HW_GET_CWM_STATUS,                
        FS_RADIO_HW_SET_COUNTRY,                   
        FS_RADIO_HW_GET_COUNTRY,                   
        FS_RADIO_HW_SET_FREQUENCY,                 
        FS_RADIO_HW_GET_FREQUENCY,                 
        FS_RADIO_HW_SET_ESSID,                     
        FS_RADIO_HW_GET_ESSID,                     
        FS_RADIO_HW_SET_WLAN_G_SUPP_MODE,          
        FS_RADIO_HW_GET_WLAN_G_SUPP_MODE,          
        FS_RADIO_HW_SET_WLAN_GN_SUPP_MODE,         
        FS_RADIO_HW_GET_WLAN_GN_SUPP_MODE,         
        FS_RADIO_HW_SET_WLAN_A_SUPP_MODE,          
        FS_RADIO_HW_GET_WLAN_A_SUPP_MODE,          
        FS_RADIO_HW_SET_WLAN_B_SUPP_MODE,          
        FS_RADIO_HW_GET_WLAN_B_SUPP_MODE,          
        FS_RADIO_HW_WLAN_ADD_MAC,                  
        FS_RADIO_HW_WLAN_DEL_MAC,                  
        FS_RADIO_HW_WLAN_CREATE_ATH,               
        FS_RADIO_HW_WLAN_DESTROY_ATH,              
        FS_RADIO_HW_SET_CHANNEL_NUM,               
        FS_RADIO_HW_GET_CHANNEL_NUM,               
        FS_RADIO_HW_SET_SHORT_GI,                  
        FS_RADIO_HW_GET_SHORT_GI,                  
        FS_RADIO_HW_SET_MAX_TX_POW_2G,             
        FS_RADIO_HW_GET_MAX_TX_POW_2G,             
        FS_RADIO_HW_SET_MAX_TX_POW_5G,             
        FS_RADIO_HW_GET_MAX_TX_POW_5G,             
        FS_RADIO_HW_SET_WIFI_HW_ADDR,              
        FS_RADIO_HW_GET_WIFI_HW_ADDR,              
        FS_RADIO_HW_SET_ATH_HW_ADDR,               
        FS_RADIO_HW_GET_ATH_HW_ADDR,               
        FS_RADIO_HW_GET_BIT_RATES,                 
        FS_RADIO_HW_STA_AUTH_DEAUTH,               
        FS_RADIO_HW_SET_STA_FWD,                   
        FS_RADIO_HW_GET_STA_FWD,                   
        FS_RADIO_HW_GET_RAD_STATS,                 
        FS_RADIO_HW_SET_11NRATES_STAT,             
        FS_RADIO_HW_GET_11NRATES_STAT,             
        FS_RADIO_HW_SET_RADIO_DEFAULT,             
        FS_RADIO_HW_GET_RADIO_DEFAULT,             
        FS_RADIO_HW_SET_RSNA_IE_PARAMS,           
        FS_RADIO_HW_GET_RSNA_IE_PARAMS,            
        FS_RADIO_HW_SET_RSNA_PAIRWISE_PARAMS,      
        FS_RADIO_HW_GET_RSNA_PAIRWISE_PARAMS,      
        FS_RADIO_HW_SET_BIT_RATES,                 
        FS_RADIO_HW_SET_RSNA_GROUPWISE_PARAMS,     
        FS_RADIO_HW_GET_RSNA_GROUPWISE_PARAMS,     
        FS_RADIO_HW_GET_RADIO_CLIENT_STATS,   
        FS_RADIO_HW_GET_INT_OUT_OCTETS,    
        FS_RADIO_HW_GET_CLIENT_RECVD_COUNT,   
        FS_RADIO_HW_GET_CLIENT_SENT_COUNT,   
        FS_RADIO_HW_STA_DOT11N_MCS_RATESET,        
        FS_RADIO_HW_SET_DOT11N,                    
        FS_RADIO_HW_SET_EDCA_PARAMS,                
#ifdef HOSTAPD_WANTED 
        FS_RADIO_HW_HOSTAPD_RESET,           
#endif
        FS_RADIO_HW_SET_DOT11AC,                       
        FS_RADIO_HW_GET_SUPP_RADIO_TYPE,               
        FS_RADIO_HW_GET_DRIVER_VHT_CAPABLITIES,        
        FS_RADIO_HW_GET_DRIVER_MAX_MPDU_LEN,           
        FS_RADIO_HW_GET_DRIVER_RX_LDPC,                
        FS_RADIO_HW_GET_DRIVER_SHORT_GI_80,            
        FS_RADIO_HW_GET_DRIVER_SHORT_GI_160,           
        FS_RADIO_HW_GET_DRIVER_TX_STBC,                
        FS_RADIO_HW_GET_DRIVER_RX_STBC,                
        FS_RADIO_HW_GET_DRIVER_SU_BEAMFORMER,          
        FS_RADIO_HW_GET_DRIVER_SU_BEAMFORMEE,          
        FS_RADIO_HW_GET_DRIVER_SUPP_BEAM_FROM_ANT,     
        FS_RADIO_HW_GET_DRIVER_SOUND_DIMENSION,        
        FS_RADIO_HW_GET_DRIVER_MU_BEAMFORMER,          
        FS_RADIO_HW_GET_DRIVER_MU_BEAMFORMEE,          
        FS_RADIO_HW_GET_DRIVER_VHT_TX_OP_PS,           
        FS_RADIO_HW_GET_DRIVER_HTC_VHTCAPABLE,         
        FS_RADIO_HW_GET_DRIVER_VHT_MAXAMPDU,           
        FS_RADIO_HW_GET_DRIVER_VHT_LINK_ADAPTION,      
        FS_RADIO_HW_GET_DRIVER_VHT_RX_ANTENNA,         
        FS_RADIO_HW_GET_DRIVER_VHT_TX_ANTENNA,         
        FS_RADIO_HW_GET_DRIVER_SUPP_CHANNEL_WIDTH,     
        FS_RADIO_HW_GET_DRIVER_VHT_SUPP_MCS,           
        FS_RADIO_HW_SET_WLAN_AC_SUPP_MODE,             
        FS_RADIO_HW_GET_WLAN_AC_SUPP_MODE,             
        FS_RADIO_HW_SET_LOCAL_ROUTING,                 
        FS_RADIO_HW_UPDATE_LOCAL_ROUTING,              
        FS_RADIO_HW_GET_MULTIDOMAIN_INFO,              
        FS_RADIO_HW_ASSEMBLE_BEACON_FRAMES,            
        FS_RADIO_HW_GET_SCAN_DETAILS,                  
        FS_RADIO_HW_SET_POWER_CONSTRAINT,              
        FS_RADIO_HW_SET_CAPABILITY,                    
        FS_RADIO_HW_GET_DRIVER_HT_CAPABLITIES,         
        FS_RADIO_HW_GET_DRIVER_HT_SUPP_MCS,            
        FS_RADIO_HW_GET_DRIVER_HT_AMPDU,               
#ifdef WPS_WTP_WANTED
        FS_RADIO_HW_SET_WPS_IE_PARAMS,                 
        FS_RADIO_HW_GET_WPS_IE_PARAMS,                 
        FS_RADIO_HW_SET_WPS_ADDITIONAL_PARAMS,         
        FS_RADIO_HW_GET_WPS_ADDITIONAL_PARAMS,         
        FS_RADIO_HW_DISABLE_WPS_PUSH_BUTTON,           
#endif
        FS_RADIO_HW_GET_DRIVER_CHNL_CAPABLITIES,      
        FS_RADIO_DFS_ADD_BEACON_FRAMES,                
        FS_RADIO_DFS_UPDATE_RADAR_STAT,                
        FS_RADIO_HW_ADD_CHANNEL_SWIT_IE,               
        FS_RADIO_HW_REMOVE_CHANNEL_SWIT_IE,            

        FS_RADIO_HW_GET_DRIVER_EXT_HT_CAPABLITIES,     
        FS_RADIO_HW_GET_DRIVER_BEAMFORM_CAPABLITIES,   
#ifdef ROGUEAP_WANTED
        FS_RADIO_HW_SET_ROUGE_AP_DETECTION,            
#endif 

#ifdef BAND_SELECT_WANTED
        FS_RADIO_DEL_STA_BANDSTEER,                    
        FS_RADIO_HW_SET_BANDSELECT,                    
#endif

        FS_RADIO_HW_SET_CHANNEL_NUM_WLAN,              
        FS_RADIO_HW_QOS_INFO,    
        FS_RADIO_HW_MULTICAST_MODE,   
        FS_RADIO_HW_MULTICAST_LENGTH,  
        FS_RADIO_HW_MULTICAST_TIMER,   
        FS_RADIO_HW_MULTICAST_TIMEOUT,  
        FS_RADIO_WLAN_ISOLATION,                      
        FS_RADIO_HW_WLAN_CREATE_BR_INTERFACE,        
#ifdef PMF_WANTED
        FS_RADIO_HW_GET_RSNA_SEQ_NO,                  
        FS_RADIO_HW_SET_RSNA_MGMT_GROUPWISE_PARAMS,   
        FS_RADIO_HW_GET_RSNA_MGMT_GROUPWISE_PARAMS,   
#endif
        FS_RADIO_HW_SET_WPA_PROTECTION,
        FS_RADIO_HW_SET_DOT11N_PARAMS,               
        FS_RADIO_HW_GET_DOT11N_PARAMS,
        FS_RADIO_HW_GET_WLAN_IFNAME,
        FS_RADIO_HW_WLAN_ADD_VLAN 
}eRadioOpCodes;


/* Structure to receive the radio/Wlan details from User 
and will be used for passing it to the NPAPI */

/* MACRO definations related to structure */

#define WLAN_DEF_INTERFACE        1
#define RADIO_NAME_MAX_LEN       12
#define WLAN_INDEX_MAX            32
#define WLAN_INDEX_MAX_PER_RADIO  16
#define WLAN_NAME_MAX_LEN         10
#define RADIO_TYPEA                2
#define RADIO_TYPEB                1
#define RADIO_TYPEG                4
#define RADIO_TYPEAN              10
#define RADIO_TYPEGN              13
#define RADIO_TYPEBG              5
#define RADIO_TYPEAC              0x80000000
#define RADIO_TYPE_MAX            14
#define RSNA_KEY_LEN              32
#define RSNA_IGTK_PKT_NUM_LEN     6
#define WLAN_SSID_NAME_LEN    32

#define DELETE_BR_INTERFACE 1
#define CREATE_BR_INTERFACE 0 
#define CH_WIDTH_20 0

#define HT_SHORT_GI_20 0x20
#define HT_SHORT_GI_40 0x40
#define HT_CHAN_WIDTH 0x02
#define HT_TX_STBC    0x80
#define HT_RX_STBC    0x300
#define HT_AMSDU      0x800
#define HT_LDPC       0x01
#define HT_40_INTOL   0x010000
#define HT_MAX_AMPDU  0x03
#define HT_AMPDU_SPACING 0x01c
#define VHT_SHORT_GI   0x20
#define VHT_CHAN_WIDTH  0x0c
#define VHT_MAX_AMPDU   0x03
#define VHT_AMPDU_FACTOR 0x03800000

#define SHIFT_FIVE 5
#define SHIFT_ONE 1
#define SHIFT_TWO 2
#define SHIFT_SEVEN 7
#define SHIFT_EIGHT 8
#define SHIFT_ELEVEN 11
#define SHIFT_SIXTEEN 16
#define SHIFT_SIX 6
#define SHIFT_TWENTYTHREE 23

#define CAPWAP_RF_GROUP_NAME_SIZE 19
 
#ifdef WPS_WTP_WANTED
#define WPS_MAX_AUTHORIZED_MACS 5
#define WPS_ETH_ALEN 6
#endif

typedef struct Dot11Stats
{
    FS_UINT8    rx_badversion;     /* rx frame with bad version */
    FS_UINT8    rx_tooshort;       /* rx frame too short */
    FS_UINT8    rx_wrongbss;       /* rx from wrong bssid */
    FS_UINT8    rx_dup;            /* rx discard due to it's a dup */
    FS_UINT8    rx_wrongdir;       /* rx w/ wrong direction */
    FS_UINT8    rx_mcastecho;      /* rx discard due to of mcast echo */
    FS_UINT8    rx_notassoc;       /* rx discard due to sta !assoc */
    FS_UINT8    rx_noprivacy;      /* rx w/ wep but privacy off */
    FS_UINT8    rx_unencrypted;    /* rx w/o wep and privacy on */
    FS_UINT8    rx_wepfail;        /* rx wep processing failed */
    FS_UINT8    rx_decap;          /* rx decapsulation failed */
    FS_UINT8    rx_mgtdiscard;     /* rx discard mgt frames */
    FS_UINT8    rx_ctl;            /* rx discard ctrl frames */
    FS_UINT8    rx_beacon;         /* rx beacon frames */
    FS_UINT8    rx_rstoobig;       /* rx rate set truncated */
    FS_UINT8    rx_elem_missing;   /* rx required element missing*/
    FS_UINT8    rx_elem_toobig;    /* rx element too big */
    FS_UINT8    rx_elem_toosmall;  /* rx element too small */
    FS_UINT8    rx_elem_unknown;   /* rx element unknown */
    FS_UINT8    rx_badchan;        /* rx frame w/ invalid chan */
    FS_UINT8    rx_chanmismatch;   /* rx frame chan mismatch */
    FS_UINT8    rx_nodealloc;      /* rx frame dropped */
    FS_UINT8    rx_ssidmismatch;   /* rx frame ssid mismatch  */
    FS_UINT8    rx_auth_unsupported;/* rx w/ unsupported auth alg */
    FS_UINT8    rx_auth_fail;      /* rx sta auth failure */
    FS_UINT8    rx_auth_countermeasures;/* rx auth discard due to CM */
    FS_UINT8    rx_assoc_bss;      /* rx assoc from wrong bssid */
    FS_UINT8    rx_assoc_notauth;  /* rx assoc w/o auth */
    FS_UINT8    rx_assoc_capmismatch;/* rx assoc w/ cap mismatch */
    FS_UINT8    rx_assoc_norate;   /* rx assoc w/ no rate match */
    FS_UINT8    rx_assoc_badwpaie; /* rx assoc w/ bad WPA IE */
    FS_UINT8    rx_deauth;         /* rx deauthentication */
    FS_UINT8    rx_disassoc;       /* rx disassociation */
    FS_UINT8    rx_badsubtype;     /* rx frame w/ unknown subtype*/
    FS_UINT8    rx_nobuf;          /* rx failed for lack of buf */
    FS_UINT8    rx_decryptcrc;     /* rx decrypt failed on crc */
    FS_UINT8    rx_ahdemo_mgt;     /* rx discard ahdemo mgt frame*/
    FS_UINT8    rx_bad_auth;       /* rx bad auth request */
    FS_UINT8    rx_unauth;         /* rx on unauthorized port */
    FS_UINT8    rx_badkeyid;       /* rx w/ incorrect keyid */
    FS_UINT8    rx_ccmpreplay;     /* rx seq# violation (CCMP) */
    FS_UINT8    rx_ccmpformat;     /* rx format bad (CCMP) */
    FS_UINT8    rx_ccmpmic;        /* rx MIC check failed (CCMP) */
    FS_UINT8    rx_tkipreplay;     /* rx seq# violation (TKIP) */
    FS_UINT8    rx_tkipformat;     /* rx format bad (TKIP) */
    FS_UINT8    rx_tkipmic;        /* rx MIC check failed (TKIP) */
    FS_UINT8    rx_tkipicv;        /* rx ICV check failed (TKIP) */
    FS_UINT8    rx_badcipher;      /* rx failed due to of key type */
    FS_UINT8    rx_nocipherctx;    /* rx failed due to key !setup */
    FS_UINT8    rx_acl;            /* rx discard due to of acl policy */
    FS_UINT8    rx_ffcnt;          /* rx fast frames */
    FS_UINT8    rx_badathtnl;      /* driver key alloc failed */
    FS_UINT8    tx_nobuf;          /* tx failed for lack of buf */
    FS_UINT8    tx_nonode;         /* tx failed for no node */
    FS_UINT8    tx_unknownmgt;     /* tx of unknown mgt frame */
    FS_UINT8    tx_badcipher;      /* tx failed due to of key type */
    FS_UINT8    tx_nodefkey;       /* tx failed due to no defkey */
    FS_UINT8    tx_noheadroom;     /* tx failed due to no space */
    FS_UINT8    tx_ffokcnt;        /* tx fast frames sent success */
    FS_UINT8    tx_fferrcnt;       /* tx fast frames sent success */
    FS_UINT8    scan_active;       /* active scans started */
    FS_UINT8    scan_passive;      /* passive scans started */
    FS_UINT8    node_timeout;      /* nodes timed out inactivity */
    FS_UINT8    crypto_nomem;      /* no memory for crypto ctx */
    FS_UINT8    crypto_tkip;       /* tkip crypto done in s/w */
    FS_UINT8    crypto_tkipenmic;  /* tkip en-MIC done in s/w */
    FS_UINT8    crypto_tkipdemic;  /* tkip de-MIC done in s/w */
    FS_UINT8    crypto_tkipcm;     /* tkip counter measures */
    FS_UINT8    crypto_ccmp;       /* ccmp crypto done in s/w */
    FS_UINT8    crypto_wep;                /* wep crypto done in s/w */
    FS_UINT8    crypto_setkey_cipher;/* cipher rejected key */
    FS_UINT8    crypto_setkey_nokey;/* no key index for setkey */
    FS_UINT8    crypto_delkey;     /* driver key delete failed */
    FS_UINT8    crypto_badcipher;  /* unknown cipher */
    FS_UINT8    crypto_nocipher;   /* cipher not available */
    FS_UINT8    crypto_attachfail; /* cipher attach failed */
    FS_UINT8    crypto_swfallback; /* cipher fallback to s/w */
    FS_UINT8    crypto_keyfail;    /* driver key alloc failed */
    FS_UINT8    crypto_enmicfail;  /* en-MIC failed */
    FS_UINT8    ibss_capmismatch;  /* merge failed-cap mismatch */
    FS_UINT8    ibss_norate;       /* merge failed-rate mismatch */
    FS_UINT8    ps_unassoc;                /* ps-poll for unassoc. sta */
    FS_UINT8    ps_badaid;         /* ps-poll w/ incorrect aid */
    FS_UINT8    ps_qempty;         /* ps-poll w/ nothing to send */
    UINT4 u4ProbeReqRx;      /* Probe req received counter*/
    UINT4 u4ProbeRespTx;      /* Probe resp transmit counter*/
} tStats;

typedef struct Dot11MacStats
{
    FS_UINT8    tx_packets; /* frames successfully transmitted */
    FS_UINT8    rx_packets; /* frames successfully received */
    FS_UINT8    tx_bytes;   /* bytes successfully transmitted */
    FS_UINT8    rx_bytes;   /* bytes successfully received */

    /* Decryption errors */
    FS_UINT8    rx_unencrypted; /* rx w/o wep and privacy on */
    FS_UINT8    rx_badkeyid;    /* rx w/ incorrect keyid */
    FS_UINT8    rx_decryptok;   /* rx decrypt okay */
    FS_UINT8    rx_decryptcrc;  /* rx decrypt failed on crc */
    FS_UINT8    rx_wepfail;     /* rx wep processing failed */
    FS_UINT8    rx_tkipreplay;  /* rx seq# violation (TKIP) */
    FS_UINT8    rx_tkipformat;  /* rx format bad (TKIP) */
    FS_UINT8    rx_tkipmic;     /* rx MIC check failed (TKIP) */
    FS_UINT8    rx_tkipicv;     /* rx ICV check failed (TKIP) */
    FS_UINT8    rx_ccmpreplay;  /* rx seq# violation (CCMP) */
    FS_UINT8    rx_ccmpformat;  /* rx format bad (CCMP) */
    FS_UINT8    rx_ccmpmic;     /* rx MIC check failed (CCMP) */
    FS_UINT8    rx_wpireplay;  /* rx seq# violation (WPI) */
    FS_UINT8    rx_wpimic;     /* rx MIC check failed (WPI) */
    FS_UINT8    tx_discard;     /* tx dropped by NIC */
    FS_UINT8    rx_discard;     /* rx dropped by NIC */
    FS_UINT8    rx_countermeasure; /* rx TKIP countermeasure activation count */

} tMacStats;

typedef struct Dot11MacMulStats
{
    FS_UINT8    tx_packets; /* frames successfully transmitted */
    FS_UINT8    rx_packets; /* frames successfully received */
    FS_UINT8    tx_bytes;   /* bytes successfully transmitted */
    FS_UINT8    rx_bytes;   /* bytes successfully received */

    /* Decryption errors */
    FS_UINT8    rx_unencrypted; /* rx w/o wep and privacy on */
    FS_UINT8    rx_badkeyid;    /* rx w/ incorrect keyid */
    FS_UINT8    rx_decryptok;   /* rx decrypt okay */
    FS_UINT8    rx_decryptcrc;  /* rx decrypt failed on crc */
    FS_UINT8    rx_wepfail;     /* rx wep processing failed */
    FS_UINT8    rx_tkipreplay;  /* rx seq# violation (TKIP) */
    FS_UINT8    rx_tkipformat;  /* rx format bad (TKIP) */
    FS_UINT8    rx_tkipmic;     /* rx MIC check failed (TKIP) */
    FS_UINT8    rx_tkipicv;     /* rx ICV check failed (TKIP) */
    FS_UINT8    rx_ccmpreplay;  /* rx seq# violation (CCMP) */
    FS_UINT8    rx_ccmpformat;  /* rx format bad (CCMP) */
    FS_UINT8    rx_ccmpmic;     /* rx MIC check failed (CCMP) */
    FS_UINT8    rx_wpireplay;  /* rx seq# violation (WPI) */
    FS_UINT8    rx_wpimic;     /* rx MIC check failed (WPI) */
    FS_UINT8    tx_discard;     /* tx dropped by NIC */
    FS_UINT8    rx_discard;     /* rx dropped by NIC */
    FS_UINT8    rx_countermeasure; /* rx TKIP countermeasure activation count */
} tMacMulStats;

typedef struct 
{
    UINT4   u4Dot11RadioType;
    UINT1   au1RadioMode[9];
    UINT1   au1Pad[3];
}tRadioTypeMapping;

typedef struct RadioInfo
{
   UINT4 u4RadioIfIndex;    
   UINT4 u4Dot11RadioType;
   UINT1 au1RadioIfName[RADIO_NAME_MAX_LEN];
   UINT2 au2WlanIfIndex[WLAN_INDEX_MAX];
   UINT1 au1WlanIfname [WLAN_INDEX_MAX][WLAN_NAME_MAX_LEN]; 
   UINT1 au1SSID[WLAN_SSID_NAME_LEN];
   UINT1 au1RfGroupID[CAPWAP_RF_GROUP_NAME_SIZE];
   UINT1 au1OUI[WSSMAC_MAX_OUI_SIZE];
   UINT1 u1WlanId;
   UINT1 u1CurrentChannel;
}tRadioInfo;

typedef struct WlanVlanInfo
{
    UINT2   u2VlanId;
    UINT1   au1RadioIfName[RADIO_NAME_MAX_LEN];
    UINT2   au2WlanIfIndex[WLAN_INDEX_MAX];
    UINT1   au1WlanIfname [WLAN_INDEX_MAX][WLAN_NAME_MAX_LEN];
    UINT1   u1WlanId;
    UINT1   au1Pad[1];
}tWlanVlanInfo;

typedef struct {
 tRadioInfo          RadInfo;
 UINT2 u2SuppHtCapInfo;
 UINT2 u2HtCapInfo;
 UINT1 u1LdpcCoding;
 UINT1 u1ChannelWidth;
 UINT1 u1SmPowerSave; 
 UINT1 u1HtGreenField;
 UINT1 u1ShortGi20;
 UINT1 u1ShortGi40;
 UINT1 u1TxStbc; 
 UINT1 u1RxStbc; 
 UINT1 u1DelayedBlockack;
 UINT1 u1MaxAmsduLen;
 UINT1 u1Dssscck40;
 UINT1 u1HtReserved;
 UINT1 u1FortyInto;
 UINT1 u1LsigTxopProt;
 UINT1 au1Pad[2];
}tRadioIfNpWrFsGetDot11NCapaParams;

typedef struct {
 tRadioInfo    RadInfo;
 UINT2         u2ExtSuppHtCapInfo;
 UINT1         au1Pad[2];
}tRadioIfNpWrFsGetDot11NExtCapaParams;

typedef struct {
 tRadioInfo    RadInfo;
 UINT4         u4SuppBeamFormHtCapInfo;
}tRadioIfNpWrFsGetDot11NBeamFormCapaParams;


typedef struct {
 tRadioInfo          RadInfo;
 UINT1 u1SuppMaxAmpduLen;
 UINT1 u1SuppMinAmpduStart;
 UINT1 u1MaxAmpduLen;
 UINT1 u1MinAmpduStart;
 UINT1 u1Reserved;
 UINT1 au1Pad[3];
}tRadioIfNpWrFsGetAmpduParams;

typedef struct {
 tRadioInfo          RadInfo;
 UINT1 au1SuppHtCapaMcs[16];
 UINT1 u1RxMcsBitmask[10];
 UINT2 u2HighSuppDataRate;
 UINT1 u1Reserved1;
 UINT1 u1Reserved2;
 UINT1 u1TxMcsSetDefined; 
 UINT1 u1TxRxMcsSetNotEqual;
 UINT1 u1TxMaxNoSpatStrm;
 UINT1 u1TxUnequalModSupp;
 UINT1 au1Pad[2];
 UINT1 u1Reserved3[4];
}tRadioIfNpWrFsGetSuppMcsParams;

typedef struct {
 tRadioInfo  RadInfo;
} tRadioIfNpWrFsHwInit;

typedef struct {
 tRadioInfo  RadInfo;
} tRadioIfNpWrFsHwDeInit;

typedef struct {
 tRadioInfo  RadInfo;
} tRadioIfNpWrFsHwUpdateAdminStatusEnable;

typedef struct {
 tRadioInfo  RadInfo;
} tRadioIfNpWrFsHwUpdateAdminStatusDisable;

typedef struct {
 tRadioInfo  RadInfo;
 UINT1 u1AntennaMode;
 UINT1 au1Pad[3];
} tRadioIfNpWrFsHwSetAntennaMode;

typedef struct {
 tRadioInfo  RadInfo;
 UINT1 *pu1AntennaMode;
} tRadioIfNpWrFsHwGetAntennaMode;

typedef struct {
 tRadioInfo  RadInfo;
 UINT1  *pu1AntennaCount; 
} tRadioIfNpWrFsHwGetCurrentTxAntenna;

typedef struct {
 tRadioInfo  RadInfo;
 UINT1 u1AntennaIndex;
 UINT1 u1AntennaSelection;
 UINT1 au1pad[2];
} tRadioIfNpWrFsHwSetAntennaSelection;

typedef struct {
 tRadioInfo  RadInfo;
 UINT1  *pu1AntennaSelection;
 UINT1  u1AntennaIndex;
 UINT1 au1Pad[3];
} tRadioIfNpWrFsHwGetAntennaSelection;

typedef struct {
 tRadioInfo  RadInfo;
 UINT4   u4RcvDivsity;
} tRadioIfNpWrFsHwSetAntennaDiversity;

typedef struct {
 tRadioInfo  RadInfo;
 UINT4    u4RcvDivsity;
} tRadioIfNpWrFsHwGetAntennaDiversity;

typedef struct {
 tRadioInfo  RadInfo;
 UINT2 u2RTSThreshold;
 UINT1 au1Pad[2];
} tRadioIfNpWrConfigSetDot11RTSThreshold;

typedef struct {
 tRadioInfo  RadInfo;
 UINT2 u2RTSThreshold;
 UINT1 au1Pad[2];
} tRadioIfNpWrConfigGetDot11RTSThreshold;

typedef struct {
 tRadioInfo  RadInfo;
 UINT2 u2FragThreshold;
 UINT1 au1Pad[2];
} tRadioIfNpWrConfigSetDot11FragThreshold;

typedef struct {
 tRadioInfo  RadInfo;
 UINT2 u2FragThreshold;
 UINT1 au1Pad[2];
} tRadioIfNpWrConfigGetDot11FragThreshold;

typedef struct {
 tRadioInfo  RadInfo;
 UINT1  *pu1ShortRetryLimit;
} tRadioIfNpWrFsHwGetDot11ShortRetryLimit;

typedef struct {
 tRadioInfo  RadInfo;
 UINT1  u1LongRetryLimit;
 UINT1 au1Pad[3];

} tRadioIfNpWrFsHwSetDot11LongRetryLimit;

typedef struct {
 tRadioInfo  RadInfo;
 UINT1  *pu1LongRetryLimit;
} tRadioIfNpWrFsHwGetDot11LongRetryLimit;

typedef struct {
 tRadioInfo  RadInfo;
 UINT4 u4ChannelWidth;
} tRadioIfNpWrFsHwSetOFDMChannelWidth;

typedef struct {
 tRadioInfo  RadInfo;
 UINT4 u4ChannelWidth;
} tRadioIfNpWrFsHwGetOFDMChannelWidth;

typedef struct {
 tRadioInfo  RadInfo;
 INT4 i4TxCurrPower;
} tRadioIfNpWrFsHwSetTxCurrPower;

typedef struct {
 tRadioInfo  RadInfo;
 tRadioIfNpWrFsGetDot11NCapaParams  sDot11nCapaParams;
 tRadioIfNpWrFsGetSuppMcsParams   sDot11nSuppMcsParams;
 tRadioIfNpWrFsGetAmpduParams     sDot11nAmpduParams;
 UINT1 u1AntennaSelection;
 UINT1 u1CurrentCCA;
 UINT1 u1ShortRetry;
 UINT1 u1LongRetry;
 UINT4 u4TxMSDULifeTime;
 UINT4 u4RxMSDULifeTime;
 UINT2 u2FirstChanNo;
 UINT2 u2NoOfChan;
 UINT2 u2MaxTxPowerLvl;
 UINT2 u2HTCapInfo;
 UINT1 u1BandSupport;
 UINT1 u1ShortPreamble;
 UINT1 u1NoOfBSSID;
    UINT1 u1TxAntenna;
    UINT1 u1AntennaDiveristy;
    UINT1 u1AntennaMode;
    UINT1 u1ShortGi20;
    UINT1 u1ShortGi40;
    UINT1 u1ChannelWidth;
    UINT1 u1BPFlag;
    UINT1 u1HTInfoSecChanAbove;
    UINT1 u1HTInfoSecChanBelow;
    UINT1 u1AMPDUParam;
    UINT1 u1TxSupportMcsSet;
    UINT1 au1Pad[2];
    UINT1 au1MCSRateSet[32];
    UINT1 au1SuppMCSSet[16];
    UINT1 au1ManMCSSet[16];
} tRadioIfNpWrFsHwGetRadioDefault;

typedef struct {
 tRadioInfo  RadInfo;
        UINT4 u4TxMSDULifeTime;
        UINT4 u4RxMSDULifeTime;
        UINT2 u2FirstChanNo;
        UINT2 u2NoOfChan;
        UINT2 u2MaxTxPowerLvl;
  UINT1 u1AntennaMode;
        UINT1 u1AntennaSelection;
        UINT1 u1AntennaDiveristy;
        UINT1 u1TxAntenna;
        UINT1 u1CurrentCCA;
        UINT1 u1ShortRetry;
        UINT1 u1LongRetry;
        UINT1 u1BandSupport;
        UINT1 u1ShortPreamble;
        UINT1 u1NoOfBSSID;
} tRadioIfNpWrFsHwSetRadioDefault;

typedef struct {
 tRadioInfo  RadInfo;
 INT4 i4TxCurrPower;
} tRadioIfNpWrFsHwGetTxCurrPower;

typedef struct {
 tRadioInfo  RadInfo;
 UINT4 u4BeaconInterval;
} tRadioIfNpWrFsHwSetBeaconInterval;

typedef struct {
 tRadioInfo  RadInfo;
 UINT4 u4BeaconInterval;
} tRadioIfNpWrFsHwGetBeaconInterval;

typedef struct {
 tRadioInfo  RadInfo;
 INT4 i4Dot11EDCATableIndex;
 INT4 i4ValDot11EDCATableCWmin;
 INT4 i4ValDot11EDCATableCWmax;
 INT4 i4ValDot11EDCATableAifs;
 INT4 i4ValDot11EDCATableTxOpLmt;
 INT4 i4ValDot11EDCATableAdmsnCtrl;
} tRadioIfNpWrConfigSetDot11EDCATableParams; 

typedef struct {
 tRadioInfo  RadInfo;
 UINT4       u4QosInfo;
} tRadioIfNpWrConfigSetDot11QosInfoTableParams;
typedef struct {
 tRadioInfo  RadInfo;
 INT4 i4Dot11EDCATableIndex;
 INT4 i4ValDot11EDCATableCWmax;
} tRadioIfNpWrConfigSetDot11EDCATableCWmax;

typedef struct {
 tRadioInfo  RadInfo;
 INT4 i4Dot11EDCATableIndex;
 INT4  i4ValDot11EDCATableCWmax;
} tRadioIfNpWrConfigGetDot11EDCATableCWmax;

typedef struct {
 tRadioInfo  RadInfo;
 INT4 i4Dot11EDCATableIndex;
 INT4 i4ValDot11EDCATableCWmin;
} tRadioIfNpWrConfigSetDot11EDCATableCWmin;

typedef struct {
 tRadioInfo  RadInfo;
 INT4 i4Dot11EDCATableIndex;
 INT4 i4ValDot11EDCATableCWmin;
} tRadioIfNpWrConfigGetDot11EDCATableCWmin;


typedef struct {
 tRadioInfo  RadInfo;
 INT4 i4Dot11EDCATableIndex;
 INT4 i4ValDot11EDCATableAifs;
} tRadioIfNpWrConfigSetDot11EDCATableAifs;

typedef struct {
 tRadioInfo  RadInfo;
 INT4 i4Dot11EDCATableIndex;
 INT4 i4ValDot11EDCATableAifs;
} tRadioIfNpWrConfigGetDot11EDCATableAifs;

typedef struct {
 tRadioInfo  RadInfo;
 INT4 i4Dot11EDCATableIndex;
 INT4 i4ValDot11EDCATableTxOpLmt;
} tRadioIfNpWrConfigSetDot11EDCATableTxOpLmt;

typedef struct {
 tRadioInfo  RadInfo;
 INT4 i4Dot11EDCATableIndex;
 INT4 i4ValDot11EDCATableTxOpLmt;
} tRadioIfNpWrConfigGetDot11EDCATableTxOpLmt;

typedef struct {
 tRadioInfo  RadInfo;
 INT4 i4Dot11EDCATableIndex;
 INT4 i4ValDot11EDCATableAdmsnCtrl;
} tRadioIfNpWrConfigSetDot11EDCATableAdmsnCtrl;

typedef struct {
 tRadioInfo  RadInfo;
 INT4 i4Dot11EDCATableIndex;
 INT4 i4ValDot11EDCATableAdmsnCtrl;
} tRadioIfNpWrConfigGetDot11EDCATableAdmsnCtrl;


typedef struct {
 tRadioInfo  RadInfo;
 INT4 i4ValDot11HideEssidVal;
 UINT1 u1ValDot11Essid;
 UINT1 au1Pad[3]; 
} tRadioIfNpWrConfigSetDot11HideEssid;

typedef struct {
 tRadioInfo  RadInfo;
 INT4 i4ValDot11HideEssidVal;
 UINT1 u1ValDot11Essid;
 UINT1 au1Pad[3];
} tRadioIfNpWrConfigGetDot11HideEssid;

typedef struct {
 tRadioInfo  RadInfo;
 INT4 i4ValDot11DtimPrd;
} tRadioIfNpWrConfigSetDot11DtimPrd;

typedef struct {
 tRadioInfo  RadInfo;
 INT4 i4ValDot11DtimPrd;
} tRadioIfNpWrConfigGetDot11DtimPrd;

typedef struct {
 tRadioInfo  RadInfo;
 INT2  i2ValDot11StaWepKeyEnType;
 UINT1 au1ValDot11StaWepKeyEnVal[14];        
 INT2  i2ValDot11StaWepKeyEnIndex;
 INT2  i2ValDot11StaWepKeyAuthIndex;
} tRadioIfNpWrConfigSetDot11StaWepKeyEncryptn;

typedef struct {
 tRadioInfo  RadInfo;
 INT2  i2ValDot11StaWepKeyEnType;
 UINT1  au1ValDot11StaWepKeyEnVal[14];
 INT2  i2ValDot11StaWepKeyEnIndex;
 INT2   i2ValDot11StaWepKeyAuthIndex;
} tRadioIfNpWrConfigGetDot11StaWepKeyEncryptn;

typedef struct {
 tRadioInfo  RadInfo;
 INT2  i2ValDot11StWpKeyAuthType;
 INT2  i2ValDot11StWpKeyAuthStat;
 INT2  i2ValDot11StWpKeyAuthIndex;
 UINT1 au1Pad[2];
} tRadioIfNpWrConfigSetDot11StWpAuthKeyType;

typedef struct {
 tRadioInfo  RadInfo;
 INT4  i4ValDot11StWpKeyAuthType;
 INT2  i2ValDot11StWpKeyAuthStat;
 INT2  i2ValDot11StWpKeyAuthIndex;
} tRadioIfNpWrConfigGetDot11StWpAuthKeyType;

typedef struct {
 tRadioInfo  RadInfo;
 INT2  i2ValDot11StaWepKeyStat;
 INT2  i2ValDot11StaWepKeyIndex;
} tRadioIfNpWrConfigSetDot11StaWepKeyStatus;

typedef struct {
 tRadioInfo  RadInfo;
 INT2  i2ValDot11StaWepKeyStat;
 INT2  i2ValDot11StaWepKeyIndex;
} tRadioIfNpWrConfigGetDot11StaWepKeyStatus;

typedef struct {
 tRadioInfo  RadInfo;
 INT2  i2ValDot11SecPrivStatus;
 UINT1 au1Pad[2];
} tRadioIfNpWrConfigSetDot11SecPrivStatus;

typedef struct {
 tRadioInfo  RadInfo;
 INT2 * pi2ValDot11SecPrivStatus;
} tRadioIfNpWrConfigGetDot11SecPrivStatus;

typedef struct {
 tRadioInfo  RadInfo;
 INT2  i2ValDot11SecExUnEnStat;
 UINT1 au1Pad[2];
} tRadioIfNpWrConfigSetDot11SecExUnEnStat;

typedef struct {
 tRadioInfo  RadInfo;
 INT2  * pi2ValDot11SecExUnEnStat;
} tRadioIfNpWrConfigGetDot11SecExUnEnStat;

typedef struct {
 tRadioInfo  RadInfo;
 UINT1  au1ValDot11anSuppMode[10];
 UINT1 au1Pad[2];
} tRadioIfNpWrConfigSetDot11anSuppMode;

typedef struct {
 tRadioInfo  RadInfo;
 UINT1  au1ValDot11anSuppMode[10];
 UINT1 au1Pad[2];
} tRadioIfNpWrConfigGetDot11anSuppMode;

typedef struct {
 tRadioInfo  RadInfo;
 INT2  i2ValDot11nAMPDUSuppStat;
 UINT1 au1Pad[2];
} tRadioIfNpWrConfigSetDot11nAMPDUSuppStat;

typedef struct {
 tRadioInfo  RadInfo;
 INT2  i2ValDot11nAMPDUSuppStat;
 UINT1 au1Pad[2];
} tRadioIfNpWrConfigGetDot11nAMPDUSuppStat;

typedef struct {
 tRadioInfo  RadInfo;
 INT2  i2ValDot11CWMStatus;
 UINT1 au1Pad[2];
} tRadioIfNpWrConfigSetDot11CWMStatus;

typedef struct {
 tRadioInfo  RadInfo;
 INT2  *i2ValDot11CWMStatus;
} tRadioIfNpWrConfigGetDot11CWMStatus;

typedef struct {
 tRadioInfo  RadInfo;
 UINT1 au1ValDot11CountryName[3];
 UINT1 au1Pad;
} tRadioIfNpWrConfigSetDot11Country;

typedef struct {
 tRadioInfo  RadInfo;
 UINT1 au1ValDot11CountryName[3];
 UINT1 au1Pad;
} tRadioIfNpWrConfigGetDot11Country;
typedef struct {
 tRadioInfo  RadInfo;
 UINT1 au1ValGroupId[CAPWAP_RF_GROUP_NAME_SIZE];
 UINT1 u1RougeApDetectionEnable;
} tRadioIfNpWrConfigSetRougeApDetection;
typedef struct {
 tRadioInfo  RadInfo;
 UINT1   u1ValDot11Frequency;
 UINT1 au1Pad[3];
} tRadioIfNpWrConfigSetDot11Frequency;

typedef struct {
 tRadioInfo  RadInfo;
 UINT1 u1ValDot11Frequency;
 UINT1 au1Pad[3];
} tRadioIfNpWrConfigGetDot11Frequency;

typedef struct {
 tRadioInfo  RadInfo;
 UINT1 au1ValDot11Essid[16];
} tRadioIfNpWrConfigGetDot11Essid;
typedef struct {
 tRadioInfo  RadInfo;
 UINT1 au1ValDot11Essid[16];
} tRadioIfNpWrConfigSetDot11Essid;

typedef struct {
 tRadioInfo  RadInfo;
 UINT1       u1PowerConstraint;
 UINT1      au1Pad[3];
} tRadioIfNpWrSetDot11PowerConstraint;
typedef struct {
 tRadioInfo  RadInfo;
 UINT2       u2Capability;
 UINT1      au1Pad[2];
} tRadioIfNpWrSetDot11Capability;

typedef struct {
 tRadioInfo  RadInfo;
 UINT2       u2WlanMulticastMode;
 UINT2       u2WlanSnoopTableLength;
 UINT4       u4WlanSnoopTimer;
 UINT4       u4WlanSnoopTimeout;
} tRadioIfNpWrSetDot11Multicast;

typedef struct {
 tRadioInfo  RadInfo;
 UINT2       u2WlanIsolationiEnable;
 UINT1       au1Pad[2];
} tRadioIfNpWrSetWlanIsolation;

typedef struct {
 tRadioInfo  RadInfo;
 UINT2       u2WlanMulticastMode;
 UINT2       u2WlanSnoopTableLength;
 UINT4       u4WlanSnoopTimer;
 UINT4       u4WlanSnoopTimeout;
} tRadioIfNpWrGetDot11Multicast;

typedef struct {
 tRadioInfo  RadInfo;
 UINT1  au1ValDot11gSuppMode[4];
} tRadioIfNpWrConfigSetDot11gSuppMode;
typedef struct {
 tRadioInfo  RadInfo;
 UINT1  au1ValDot11gSuppMode[4];
} tRadioIfNpWrConfigGetDot11gSuppMode;

typedef struct {
 tRadioInfo  RadInfo;
 UINT1  au1ValDot11SuppMode[9];
 UINT1 au1Pad[3];
} tRadioIfNpWrConfigSetDot11gnSuppMode;

typedef struct {
 tRadioInfo  RadInfo;
 UINT1  au1ValDot11SuppMode[9];
 UINT1 au1Pad[3];
} tRadioIfNpWrConfigGetDot11gnSuppMode;

typedef struct {
 tRadioInfo  RadInfo;
 UINT1  au1ValDot11SuppMode[9];
 UINT1 au1Pad[3];
} tRadioIfNpWrConfigSetDot11acSuppMode;

typedef struct {
 tRadioInfo  RadInfo;
 UINT1  au1ValDot11SuppMode[9];
 UINT1 au1Pad[3];
} tRadioIfNpWrConfigGetDot11acSuppMode;

typedef struct {
 tRadioInfo  RadInfo;
 UINT1  au1ValDot11SuppMode[4];
} tRadioIfNpWrConfigSetDot11aSuppMode;
typedef struct {
 tRadioInfo  RadInfo;
 UINT1  au1ValDot11SuppMode[4];
} tRadioIfNpWrConfigGetDot11aSuppMode;
typedef struct {
 tRadioInfo  RadInfo;
 UINT1  au1ValDot11SuppMode;
 UINT1 au1Pad[3];
} tRadioIfNpWrConfigSetDot11bSuppMode;
typedef struct {
 tRadioInfo  RadInfo;
 UINT1  au1ValDot11SuppMode[4];
} tRadioIfNpWrConfigGetDot11bSuppMode;

typedef struct {
 tRadioInfo  RadInfo;
 UINT1  au1ValDot11AddMac[7];
 UINT1 au1Pad;
} tRadioIfNpWrConfigDot11AddMac;

typedef struct {
 tRadioInfo  RadInfo;
 UINT1  au1ValDot11DelMac[7];
 UINT1 au1Pad;
} tRadioIfNpWrConfigDot11DelMac;

typedef struct {
 tRadioInfo  RadInfo;
} tRadioIfNpWrConfigDot11WlanCreate;

typedef struct {
 tRadioInfo  RadInfo;
 UINT1  u1BrInterfaceStatus;
 UINT1 au1brInterfacename[24];
 UINT1  au1Pad[3];
} tRadioIfNpWrConfigDot11CreateBrInterface;

typedef struct {
 tRadioInfo  RadInfo;
} tRadioIfNpWrConfigDot11WlanDestroy;

typedef struct {
 tRadioInfo  RadInfo;
 UINT2     u2ChannelNum;
 UINT1 au1Pad[2];
} tRadioIfNpWrConfigSetDot11Channel;

typedef struct {
 tRadioInfo  RadInfo;
 UINT2     u2ChannelNum;
 UINT1 au1Pad[2];
} tRadioIfNpWrConfigGetDot11Channel;

typedef struct {
 tRadioInfo  RadInfo;
 UINT4    u4ValShortGi ;
} tRadioIfNpWrConfigSetDot11ShortGi;

typedef struct {
 tRadioInfo  RadInfo;
 UINT4    u4ValShortGi ;
} tRadioIfNpWrConfigGetDot11ShortGi;

typedef struct {
 tRadioInfo  RadInfo;
 UINT4     u4MaxTxPow;
} tRadioIfNpWrConfigSetDot11MaxTxPow2G;

typedef struct {
 tRadioInfo  RadInfo;
 UINT4     u4MaxTxPow;
} tRadioIfNpWrConfigGetDot11MaxTxPow2G;

typedef struct {
 tRadioInfo  RadInfo;
 UINT4     u4MaxTxPow;
} tRadioIfNpWrConfigSetDot11MaxTxPow5G;

typedef struct {
 tRadioInfo RadInfo;
 UINT4     u4MaxTxPow;
} tRadioIfNpWrConfigGetDot11MaxTxPow5G;

typedef struct {
 tRadioInfo  RadInfo;
 UINT1      au1WifiHwAddr[18]; /* A string with : 17 character long*/
 UINT1 au1Pad[2];
}tRadioIfNpWrConfigSetDot11HwAddr;

typedef struct {
 tRadioInfo  RadInfo;
 UINT1      au1WifiHwAddr[18];
 UINT1 au1Pad[2];
} tRadioIfNpWrConfigGetDot11HwAddr;

typedef struct {
 tRadioInfo  RadInfo;
 UINT1      au1AthHwAddr[7]; /* MAC addr in HEX 6 bytes long*/
 UINT1 au1Pad;
} tRadioIfNpWrSetDot11AthHwAddr;

typedef struct {
 tRadioInfo  RadInfo;
 UINT1      au1AthHwAddr[7];
 UINT1 au1Pad;
} tRadioIfNpWrGetDot11AthHwAddr;

typedef struct {
 tRadioInfo  RadInfo;
        UINT4      u4CurrBitRate;
        UINT2      u2BitRateNum;
        UINT1      au1pad[2];
        UINT4      au4BitRate[16];
} tRadioIfNpWrSetDot11BitRates;

typedef struct {
 tRadioInfo  RadInfo;
        UINT4      u4CurrBitRate;
        UINT2      u2BitRateNum;
  UINT1      au1Pad[2];
  UINT4      au4BitRate[16];
} tRadioIfNpWrGetDot11BitRates;

typedef struct {
 tRadioInfo  RadInfo;
 UINT2       u2VlanId;
 UINT1       u1AuthStat;
 UINT1      au1StaAuthMac[7];
 UINT1      au1IntfName[24];
 UINT1 au1pad[2];
} tRadioIfNpWrDot11StaAuthDeAuth;

typedef struct {
        tRadioInfo  RadInfo;
        UINT1      au1StaAuthMac[7];
 UINT1         u1Pad;
        UINT1     au1MCSRateSet[16];
} tRadioIfNpWrDot11StaMCSRate;

typedef struct {
 tRadioInfo  RadInfo;
 UINT4      u4SetStaFwd;
} tRadioIfNpWrSetDot11StaFwd;

typedef struct {
 tRadioInfo  RadInfo;
 UINT4      u4GetStaFwd;
} tRadioIfNpWrGetDot11StaFwd;

typedef struct {
 tRadioInfo     RadInfo;
        tStats         Stats;
        tMacStats      MacStats;
        tMacMulStats   MacMulStats;
} tRadioIfNpWrGet80211Stats;

typedef struct {
 tRadioInfo  RadInfo;
 UINT4     u4SetRateStatus;
} tRadioIfNpWrSetDot11NRatesStat;

typedef struct {
 tRadioInfo  RadInfo;
 UINT4      u4GetRateStatus;
}tRadioIfNpWrGetDot11NRatesStat;
#ifdef WPS_WTP_WANTED
typedef struct {
    tRadioInfo  RadInfo;
    UINT2       u2WtpInternalId;
    UINT1       u1WpsStatus;
    UINT1       wscState;
    UINT1 uuid_e[16];
    UINT1 configMethods; 
    UINT1          au1WtpModelNumber[32];
    UINT1               au1WtpSerialNumber[32];
    UINT1               au1WtpName[32];
    UINT1 au1NoOfRadio;
    UINT1 radioType[31];
    UINT1       au1Pad[3];
} tRadioIfNpWrGetWpsIEParams;

typedef struct {
    tRadioInfo  RadInfo;
    UINT2       u2WtpInternalId;
    UINT1       u1WpsStatus;
    UINT1       u1WpsAdditionalIe;
    UINT1       wscState;
    UINT1       noOfAuthMac;
    UINT1 authorized_macs[WPS_MAX_AUTHORIZED_MACS][WPS_ETH_ALEN];
    UINT1 uuid_e[16];
    UINT1 configMethods; 
    UINT1          au1WtpModelNumber[32];
    UINT1               au1WtpSerialNumber[32];
    UINT1               au1WtpName[32];
    UINT1 au1NoOfRadio;
    UINT1 radioType[31];
    UINT1       au1Pad[3];
} tRadioIfNpWrSetWpsAdditionalParams;

typedef struct {
    tRadioInfo  RadInfo;
    UINT2       u2WtpInternalId;
    UINT1       u1WpsStatus;
    UINT1       u1WpsAdditionalIe;
    UINT1       wscState;
    UINT1       noOfAuthMac;
    UINT1 authorized_macs[WPS_MAX_AUTHORIZED_MACS][WPS_ETH_ALEN];
    UINT1 uuid_e[16];
    UINT1 configMethods; 
    UINT1          au1WtpModelNumber[32];
    UINT1               au1WtpSerialNumber[32];
    UINT1               au1WtpName[32];
    UINT1 au1NoOfRadio;
    UINT1 radioType[31];
    UINT1       au1Pad[3];
} tRadioIfNpWrGetWpsAdditionalParams;

typedef struct {
    tRadioInfo  RadInfo;
    UINT2       u2WtpInternalId;
    UINT1       u1WpsStatus;
    UINT1       wscState;
    UINT1 uuid_e[16];
    UINT1 configMethods; 
    UINT1          au1WtpModelNumber[32];
    UINT1               au1WtpSerialNumber[32];
    UINT1               au1WtpName[32];
    UINT1 au1NoOfRadio;
    UINT1 radioType[31];
    UINT1       au1Pad[3];
} tRadioIfNpWrSetWpsIEParams;
#endif
typedef struct {
    tRadioInfo  RadInfo;
    UINT2       u2PairwiseCipherSuiteCount;
    UINT2       u2AKMSuiteCount;
    UINT2       u2RsnCapab;
    UINT1       au1PairwiseCipher[2];
    UINT1       au1AKMSuite[2];
    UINT1       u1RsnaStatus;
    UINT1       u1GroupCipher;
    UINT1       u1GroupMgmtCipher; /*PMF_WANTED*/
    UINT1       u1Privacy;
    UINT1       au1Pad [2];
} tRadioIfNpWrGetRsnaIEParams;

typedef struct {
    tRadioInfo  RadInfo;
    UINT2       u2PairwiseCipherSuiteCount;
    UINT2       u2AKMSuiteCount;
    UINT2       u2RsnCapab;
    UINT1       au1PairwiseCipher[2];
    UINT1       au1AKMSuite[2];
    UINT1       u1RsnaStatus;
    UINT1       u1GroupCipher;
    UINT1       u1GroupMgmtCipher; /*PMF_WANTED*/
    UINT1       u1Privacy;
    UINT1       au1Pad [2];
} tRadioIfNpWrSetRsnaIEParams;
typedef struct {
    tRadioInfo  RadInfo;
    UINT2       u2PairwiseCipherSuiteCount;
    UINT2       u2AKMSuiteCount;
    UINT2       u2RsnCapab;
    UINT1       au1PairwiseCipher[2];
    UINT1       u1AKMSuite;
    UINT1       u1WpaStatus;
    UINT1       u1GroupCipher;
    UINT1       u1GroupMgmtCipher; /*PMF_WANTED*/
    UINT1       u1Privacy;
    UINT1       u1WpaLen;
    UINT1       au1Pad [2];
} tRadioIfNpWrSetWpaIEParams;
typedef struct {
    tRadioInfo  RadInfo;
    tMacAddr    stationMacAddress;
    UINT2       u2PairwiseCipherSuiteCount;
    UINT2       u2AKMSuiteCount;
    UINT2       u2KeyLength;
    UINT1       au1PairwiseCipher[2];
    UINT1       au1AKMSuite[2];
    UINT1       au1PtkKey[RSNA_KEY_LEN];
    UINT1       u1KeyIndex;
    UINT1       u1RsnaStatus;
    UINT1       u1GroupCipher;
    UINT1       u1Privacy;
} tRadioIfNpWrGetRsnaPairwiseParams;

typedef struct {
    tRadioInfo  RadInfo;
    tMacAddr    stationMacAddress;
    UINT2       u2PairwiseCipherSuiteCount;
    UINT2       u2AKMSuiteCount;
    UINT2       u2KeyLength;
    UINT1       au1PairwiseCipher[2];
    UINT1       au1AKMSuite[2];
    UINT1       au1PtkKey[RSNA_KEY_LEN];
    UINT1       u1KeyIndex;
    UINT1       u1RsnaStatus;
    UINT1       u1GroupCipher;
    UINT1       u1Privacy;
    UINT1       u1ElemId;
    UINT1       u1Pad[3];
} tRadioIfNpWrSetRsnaPairwiseParams;

typedef struct {
    tRadioInfo  RadInfo;
    UINT2       u2KeyLength;
    UINT1       au1GtkKey[RSNA_KEY_LEN];
    UINT1       u1KeyIndex;
    UINT1       u1GroupCipher;
    UINT1       u1ElemId;
    UINT1       u1Pad[3];
} tRadioIfNpWrGetRsnaGroupwiseParams;


typedef struct {
    tRadioInfo  RadInfo;
    UINT2       u2KeyLength;
    UINT1       au1GtkKey[RSNA_KEY_LEN];
    UINT1       u1KeyIndex;
    UINT1       u1GroupCipher;
} tRadioIfNpWrSetRsnaGroupwiseParams;

#ifdef PMF_WANTED
typedef struct {
    tRadioInfo  RadInfo;
    UINT1       au1IGtkKey[RSNA_KEY_LEN];
    UINT2       u2KeyLength;
    UINT1       u1KeyIndex;
    UINT1       u1MgmtGroupCipher;
} tRadioIfNpWrGetRsnaMgmtGroupwiseParams;


typedef struct {
    tRadioInfo  RadInfo;
    UINT1       au1IGtkKey[RSNA_KEY_LEN];
    UINT2       u2KeyLength;
    UINT1       u1KeyIndex;
    UINT1       u1MgmtGroupCipher;
} tRadioIfNpWrSetRsnaMgmtGroupwiseParams;
#endif

typedef struct {
    tRadioInfo  RadInfo;
    UINT4 u4IfInOctets;
}tRadioIfNpWrGetInterfaceInOctets;


typedef struct {
    tRadioInfo  RadInfo;
    UINT4 u4IfOutOctets;
}tRadioIfNpWrGetInterfaceOutOctets;


typedef struct {
    tRadioInfo  RadInfo;
    tMacAddr    stationMacAddress;
    UINT1 au1pad[2];
    UINT4 u4ClientStatsBytesRecvdCount;
}tRadioIfNpWrGetClientStatsBytesRecvdCount;

typedef struct {
    tRadioInfo  RadInfo;
    tMacAddr    stationMacAddress;
    UINT1 au1pad[2];
    UINT4 u4ClientStatsBytesSentCount;
}tRadioIfNpWrGetClientStatsBytesSentCount;

typedef struct {
 tRadioInfo          RadInfo;
 tHTCapabilities     HTCapabilities;
 tHTOperation        HTOperation;
 UINT1               u1Dot11nSupport;
 UINT1       u1HTCapEnable;
 UINT1       u1HTOpeEnable;
 UINT1               au1Pad[1];
} tRadioIfNpWrConfigSetDot11n;

typedef struct {
 tRadioInfo          RadInfo;
 tHTCapabilities     HTCapabilities;
 tHTOperation        HTOperation;
 UINT1               u1Dot11nSupport;
 UINT1       u1HTCapEnable;
 UINT1       u1HTOpeEnable;
 UINT1               au1Pad[1];
} tRadioIfNpWrConfigGetDot11n;

typedef struct {
 tRadioInfo           RadInfo;
 tVHTCapability   VHTCapabilities;
 tVHTOperation    VHTOperation;
 tVhtTxPower      VhtTxPower;
 tChanSwitchWrapper ChanSwitchWrapper;
 tOperModeNotify  OperModeNotify;
 UINT1                u1Dot11acSupport;
 UINT1                au1Pad[3];
}tRadioIfNpWrConfigSetDot11ac;

typedef struct {
 tRadioInfo           RadInfo;
 tVHTCapability   VHTCapabilities;
 tVHTOperation    VHTOperation;
 tVhtTxPower        VhtTxPower;
 tChanSwitchWrapper ChanSwitchWrapper;
 tOperModeNotify  OperModeNotify;
 UINT1                u1Dot11acSupport;
 UINT1                au1Pad[3];
}tRadioIfNpWrConfigGetDot11ac;

typedef struct {
 tRadioInfo          RadInfo;
        UINT4 u4VhtCapInfo;
        UINT1 au1SuppVhtCapaMcs[8];
        UINT1 u1MaxMPDULen;
        UINT1 u1RxLpdc;
        UINT1 u1ShortGi80;
        UINT1 u1ShortGi160;
        UINT1 u1TxStbc;
        UINT1 u1RxStbc;
        UINT1 u1SuBeamFormer;
        UINT1 u1SuBeamFormee;
        UINT1 u1SuppBeamFormAnt;
        UINT1 u1SoundDimension;
        UINT1 u1MuBeamFormer;
        UINT1 u1MuBeamFormee;
        UINT1 u1VhtTxOpPs;
        UINT1 u1HtcVhtCapable;
        UINT1 u1VhtMaxAMPDU;
        UINT1 u1VhtLinkAdaption;
        UINT1 u1VhtRxAntenna;
        UINT1 u1VhtTxAntenna;
        UINT1 u1SuppChanWidth;
 UINT1 au1VhtReserved[1];
}tRadioIfNpWrFsGetDot11AcCapaParams;


typedef struct {
     tRadioInfo  RadInfo;
      UINT1 u1WtpLocalRouting;
       UINT1 au1Pad[3];
} tRadioIfNpWrFsHwSetLocalRouting;

typedef struct {
    tRadioInfo  RadInfo;
    tMultiDomainInfo MultiDomainInfo[MAX_MULTIDOMAIN_INFO_ELEMENT];
}tRadioIfNpWrFsHwGetMultiDomainInfo;

typedef struct {
     tRadioInfo  RadInfo;
}tRadioIfNpWrFsHwGetScanDetails;

typedef struct {
    tRadioInfo          RadInfo;
    UINT4               u4SuppRadioType; 
}tRadioIfNpWrFsGetDot11SuppRadioType;


typedef struct {
    tWlanVlanInfo    WlanVlanInf;
} tRadioIfNpWrWlanAddVlanIntf;


typedef struct{

   UINT4   u4Frequency;
   UINT4   u4Channel;
   UINT4   u4DFSFlag;
   UINT4   u4DFSCacTime;
}tChnlData;

typedef struct {
    tRadioInfo          RadInfo;
    tChnlData          channel_data[37];
}tRadioIfNpWrFsGetChannelInfo;
typedef struct{
 tRadioInfo          RadInfo;
 UINT1               u1RadarStatus;
 UINT1               au1Pad[3];
}tRadioIfNpWrFsHwUpdateRadarStatus;

#ifdef PMF_DEBUG_WANTED
typedef struct {
 tRadioInfo  RadInfo;
 INT4        i4KeyIndex;
 UINT1       au1IGTKPktNum[RSNA_IGTK_PKT_NUM_LEN];
 UINT1       au1Pad[2];
}tRadioIfNpWrFsGetSeqNum;
#endif

typedef struct {
 tRadioInfo  RadInfo;
 UINT2       u2AMPDULimit;
 UINT2       u2AMSDULimit;
 UINT1       u1AMPDUStatus;
 UINT1       u1AMPDUSubFrame;
 UINT1       u1AMSDUStatus;
 UINT1       au1Pad;
} tRadioIfNpWrFsHwSetDot11nParams;

typedef struct {
 tRadioInfo  RadInfo;
 UINT2       u2AMPDULimit;
 UINT2       u2AMSDULimit;
 UINT1       u1AMPDUStatus;
 UINT1       u1AMPDUSubFrame;
 UINT1       u1AMSDUStatus;
 UINT1       au1Pad;
} tRadioIfNpWrFsHwGetDot11nParams;


typedef struct{
 tRadioInfo          RadInfo;
 UINT1               u1BandSelectStatus;
 UINT1               u1BsGlobalStatus;
 UINT1               au1Pad[2];
}tRadioIfNpWrFsBandSelectStatus;

typedef struct {
 tRadioInfo  RadInfo;
 INT4        i4KeyIndex;
 UINT1       au1IGTKPktNum[6];
 UINT1       au1Pad[2];
}tRadioIfNpWrFsGetSeqNum;

typedef struct {
 UINT1   u1RadioId;
 UINT1   u1WlanIdInRadio;
 UINT1   au1WlanIntfName[10];
 UINT1   u1WlanId;
 UINT1   au1Pad[3];
}tRadioIfNpWrGetWlanIfName;


#ifdef BAND_SELECT_WANTED
typedef struct {
 tRadioInfo  RadInfo;
 tMacAddr    stationMacAddress;
 UINT1 au1pad[2];
}tRadioIfNpWrFsDelStaBandSteer;
#endif

typedef struct RadioIfNpModInfo {

 union {
  tRadioIfNpWrFsHwInit                  sConfigHwInit; 
  tRadioIfNpWrFsHwDeInit                  sConfigHwDeInit;
  tRadioIfNpWrFsHwUpdateAdminStatusEnable      sFsHwUpdateAdminStatusEnable;
  tRadioIfNpWrFsHwUpdateAdminStatusDisable     sFsHwUpdateAdminStatusDisable;
  tRadioIfNpWrFsHwSetAntennaMode  sFsHwSetAntennaMode;
  tRadioIfNpWrFsHwGetAntennaMode  sFsHwGetAntennaMode;
  tRadioIfNpWrFsHwGetCurrentTxAntenna  sFsHwGetCurrentTxAntenna;
  tRadioIfNpWrFsHwSetAntennaSelection  sFsHwSetAntennaSelection;
  tRadioIfNpWrFsHwGetAntennaSelection  sFsHwGetAntennaSelection;
  tRadioIfNpWrFsHwSetAntennaDiversity  sFsHwSetAntennaDiversity;
  tRadioIfNpWrFsHwGetAntennaDiversity  sFsHwGetAntennaDiversity;
  tRadioIfNpWrConfigSetDot11RTSThreshold  sConfigSetDot11RTSThreshold;
  tRadioIfNpWrConfigGetDot11RTSThreshold  sConfigGetDot11RTSThreshold;
  tRadioIfNpWrConfigSetDot11FragThreshold  sConfigSetDot11FragThreshold;
  tRadioIfNpWrConfigGetDot11FragThreshold  sConfigGetDot11FragThreshold;
  tRadioIfNpWrFsHwGetDot11ShortRetryLimit   sFsHwSGetDot11ShortRetryLimit;     
  tRadioIfNpWrFsHwSetDot11LongRetryLimit    sFsHwSetDot11LongRetryLimit;     
  tRadioIfNpWrFsHwGetDot11LongRetryLimit    sFsHwGetDot11LongRetryLimit;     
  tRadioIfNpWrFsHwSetOFDMChannelWidth    sFsHwSetOFDMChannelWidth; 
  tRadioIfNpWrFsHwGetOFDMChannelWidth    sFsHwGetOFDMChannelWidth; 
  tRadioIfNpWrFsHwSetTxCurrPower  sFsHwSetTxCurrPower;
  tRadioIfNpWrFsHwGetTxCurrPower  sFsHwGetTxCurrPower;
  tRadioIfNpWrFsHwSetBeaconInterval  sFsHwSetBeaconInterval;
  tRadioIfNpWrFsHwGetBeaconInterval  sFsHwGetBeaconInterval;
  tRadioIfNpWrConfigSetDot11QosInfoTableParams sConfigSetDot11QosInfoTableParams;
  tRadioIfNpWrConfigSetDot11EDCATableParams sConfigSetDot11EDCATableParams;
  tRadioIfNpWrConfigSetDot11EDCATableCWmax  sConfigSetDot11EDCATableCWmax;
  tRadioIfNpWrConfigGetDot11EDCATableCWmax  sConfigGetDot11EDCATableCWmax;
  tRadioIfNpWrConfigSetDot11EDCATableCWmin  sConfigSetDot11EDCATableCWmin;
  tRadioIfNpWrConfigGetDot11EDCATableCWmin  sConfigGetDot11EDCATableCWmin;
  tRadioIfNpWrConfigSetDot11EDCATableAifs  sConfigSetDot11EDCATableAifs;
  tRadioIfNpWrConfigGetDot11EDCATableAifs  sConfigGetDot11EDCATableAifs;
  tRadioIfNpWrConfigSetDot11EDCATableTxOpLmt   sConfigSetDot11EDCATableTxOpLmt;
  tRadioIfNpWrConfigGetDot11EDCATableTxOpLmt   sConfigGetDot11EDCATableTxOpLmt;
  tRadioIfNpWrConfigSetDot11EDCATableAdmsnCtrl      sConfigSetDot11EDCATableAdmsnCtrl;
  tRadioIfNpWrConfigGetDot11EDCATableAdmsnCtrl      sConfigGetDot11EDCATableAdmsnCtrl;
  tRadioIfNpWrConfigSetDot11HideEssid               sConfigSetDot11HideEssid;
  tRadioIfNpWrConfigGetDot11HideEssid               sConfigGetDot11HideEssid;
  tRadioIfNpWrConfigSetDot11DtimPrd                 sConfigSetDot11DtimPrd;
  tRadioIfNpWrConfigGetDot11DtimPrd                 sConfigGetDot11DtimPrd;
  tRadioIfNpWrConfigSetDot11StaWepKeyEncryptn     sConfigSetStaWepKeyEncryptn;
  tRadioIfNpWrConfigGetDot11StaWepKeyEncryptn     sConfigGetStaWepKeyEncryptn;
  tRadioIfNpWrConfigSetDot11StWpAuthKeyType         sConfigSetStWpKeyAuthStatus;
  tRadioIfNpWrConfigGetDot11StWpAuthKeyType         sConfigGetStWpKeyAuthStatus;
  tRadioIfNpWrConfigSetDot11StaWepKeyStatus     sConfigSetDot11StaWepKeyStatus;
  tRadioIfNpWrConfigGetDot11StaWepKeyStatus     sConfigGetDot11StaWepKeyStatus;
  tRadioIfNpWrConfigSetDot11SecPrivStatus          sConfigSetDot11SecPrivStatus;
  tRadioIfNpWrConfigGetDot11SecPrivStatus          sConfigGetDot11SecPrivStatus;
  tRadioIfNpWrConfigSetDot11SecExUnEnStat         sConfigSetDot11SecExUnEnStat;
  tRadioIfNpWrConfigGetDot11SecExUnEnStat         sConfigGetDot11SecExUnEnStat;
  tRadioIfNpWrConfigSetDot11anSuppMode               sConfigSetDot11anSuppMode;
  tRadioIfNpWrConfigGetDot11anSuppMode               sConfigGetDot11anSuppMode;
  tRadioIfNpWrConfigSetDot11nAMPDUSuppStat            sConfigSetDot11nAMPDUSuppStat;
  tRadioIfNpWrConfigGetDot11nAMPDUSuppStat            sConfigGetDot11nAMPDUSuppStat;
  tRadioIfNpWrConfigSetDot11CWMStatus                 sConfigSetDot11CWMStatus;    
  tRadioIfNpWrConfigGetDot11CWMStatus                 sConfigGetDot11CWMStatus;   
  tRadioIfNpWrConfigSetDot11Country                   sConfigSetDot11Country; 
  tRadioIfNpWrConfigGetDot11Country                   sConfigGetDot11Country;
  tRadioIfNpWrConfigSetDot11Frequency                 sConfigSetDot11Frequency; 
  tRadioIfNpWrConfigGetDot11Frequency                 sConfigGetDot11Frequency;
  tRadioIfNpWrConfigGetDot11Essid                     sConfigGetDot11Essid;
  tRadioIfNpWrConfigSetDot11Essid                     sConfigSetDot11Essid;
  tRadioIfNpWrSetDot11PowerConstraint                 sConfigSetDot11PowerConstraint;
  tRadioIfNpWrSetDot11Capability                      sConfigSetDot11Capability;
  tRadioIfNpWrConfigSetDot11gnSuppMode                 sConfigSetDot11gnSuppMode;
  tRadioIfNpWrConfigGetDot11gnSuppMode                 sConfigGetDot11gnSuppMode;
  tRadioIfNpWrConfigSetDot11gSuppMode                 sConfigSetDot11gSuppMode;
  tRadioIfNpWrConfigGetDot11gSuppMode                 sConfigGetDot11gSuppMode;
  tRadioIfNpWrConfigSetDot11aSuppMode                 sConfigSetDot11aSuppMode;
  tRadioIfNpWrConfigGetDot11aSuppMode                 sConfigGetDot11aSuppMode;
  tRadioIfNpWrConfigSetDot11bSuppMode                 sConfigSetDot11bSuppMode;
  tRadioIfNpWrConfigGetDot11bSuppMode                 sConfigGetDot11bSuppMode;
  tRadioIfNpWrConfigSetDot11acSuppMode                 sConfigSetDot11acSuppMode;
  tRadioIfNpWrConfigGetDot11acSuppMode                 sConfigGetDot11acSuppMode;
 tRadioIfNpWrConfigSetRougeApDetection                sConfigSetRougeApDetection;
                tRadioIfNpWrConfigDot11AddMac                       sConfigDot11AddMac;
                tRadioIfNpWrConfigDot11DelMac                       sConfigDot11DelMac;
                tRadioIfNpWrConfigDot11WlanCreate                   sConfigDot11WlanCreate;
                tRadioIfNpWrConfigDot11CreateBrInterface            sConfigDot11CreateBrInterface;
                tRadioIfNpWrConfigDot11WlanDestroy                  sConfigDot11WlanDestroy;
  tRadioIfNpWrConfigSetDot11Channel                   sConfigSetDot11Channel;
  tRadioIfNpWrConfigGetDot11Channel                   sConfigGetDot11Channel;
                tRadioIfNpWrConfigSetDot11ShortGi                   sConfigSetDot11ShortGi;
                tRadioIfNpWrConfigGetDot11ShortGi                   sConfigGetDot11ShortGi;
                tRadioIfNpWrConfigSetDot11MaxTxPow2G                sConfigSetDot11MaxTxPow2G;
                tRadioIfNpWrConfigGetDot11MaxTxPow2G                sConfigGetDot11MaxTxPow2G;
                tRadioIfNpWrConfigSetDot11MaxTxPow5G                sConfigSetDot11MaxTxPow5G;
                tRadioIfNpWrConfigGetDot11MaxTxPow5G                sConfigGetDot11MaxTxPow5G;
                tRadioIfNpWrConfigSetDot11HwAddr                    sConfigSetDot11HwAddr;
                tRadioIfNpWrConfigGetDot11HwAddr                    sConfigGetDot11HwAddr;
                tRadioIfNpWrSetDot11AthHwAddr                       sConfigSetDot11AthHwAddr;
                tRadioIfNpWrGetDot11AthHwAddr                       sConfigGetDot11AthHwAddr;
                tRadioIfNpWrGetDot11BitRates                        sConfigGetDot11BitRates;
                tRadioIfNpWrSetDot11BitRates                        sConfigSetDot11BitRates;
                tRadioIfNpWrDot11StaAuthDeAuth                      sConfigDot11StaAuthDeAuth;
                tRadioIfNpWrSetDot11StaFwd                          sConfigSetDot11StaFwd;
                tRadioIfNpWrGetDot11StaFwd                          sConfigGetDot11StaFwd;
  tRadioIfNpWrGet80211Stats                           sConfigGet80211Stats;
                tRadioIfNpWrSetDot11NRatesStat                      sConfigSetDot11NRatesStat;
                tRadioIfNpWrGetDot11NRatesStat                      sConfigGetDot11NRatesStat;
  tRadioIfNpWrFsHwSetRadioDefault       sConfigSetRadioDefault;
  tRadioIfNpWrFsHwGetRadioDefault       sConfigGetRadioDefault;
#ifdef WPS_WTP_WANTED
                tRadioIfNpWrSetWpsAdditionalParams                 sConfigSetWpsAdditionalParams;
                tRadioIfNpWrGetWpsAdditionalParams         sConfigGetWpsAdditionalParams;
                tRadioIfNpWrSetWpsIEParams                 sConfigSetWpsIEParams;
                tRadioIfNpWrGetWpsIEParams                 sConfigGetWpsIEParams;
#endif                
                tRadioIfNpWrSetRsnaIEParams                 sConfigSetRsnaIEParams;
                tRadioIfNpWrGetRsnaIEParams                 sConfigGetRsnaIEParams;
                tRadioIfNpWrSetRsnaPairwiseParams           sConfigSetRsnaPairwiseParams;
                 tRadioIfNpWrGetRsnaPairwiseParams          sConfigGetRsnaPairwiseParams;
                tRadioIfNpWrSetRsnaGroupwiseParams          sConfigSetRsnaGroupwiseParams;
                tRadioIfNpWrGetRsnaGroupwiseParams          sConfigGetRsnaGroupwiseParams;
#ifdef PMF_WANTED
                tRadioIfNpWrSetRsnaMgmtGroupwiseParams      sConfigSetRsnaMgmtGroupwiseParams;
                tRadioIfNpWrGetRsnaMgmtGroupwiseParams      sConfigGetRsnaMgmtGroupwiseParams;
#endif
   tRadioIfNpWrGetInterfaceInOctets  sConfigGetInterfaceInOctets;
   tRadioIfNpWrGetInterfaceOutOctets  sConfigGetInterfaceOutOctets;
   tRadioIfNpWrGetClientStatsBytesRecvdCount sConfigGetClientStatsBytesRecvdCount;
   tRadioIfNpWrGetClientStatsBytesSentCount sConfigGetClientStatsBytesSentCount;
   tRadioIfNpWrDot11StaMCSRate                 sConfigDot11StaMCSRate;
   tRadioIfNpWrConfigSetDot11n  sConfigSetDot11n;
   tRadioIfNpWrConfigGetDot11n  sConfigGetDot11n;
   tRadioIfNpWrConfigSetDot11ac  sConfigSetDot11ac;
   tRadioIfNpWrConfigGetDot11ac  sConfigGetDot11ac;
   tRadioIfNpWrFsGetDot11AcCapaParams sConfigDot11AcCapaParams; 
   tRadioIfNpWrFsGetDot11SuppRadioType sConfigDot11SuppRadioType; 
   tRadioIfNpWrFsHwSetLocalRouting sConfigSetLocalRouting; 
   tRadioIfNpWrFsHwGetMultiDomainInfo sConfigGetMultiDomainInfo; 
   tRadioIfNpWrFsHwGetScanDetails sConfigGetScanDetails; 
   tRadioIfNpWrFsGetDot11NCapaParams  sConfigDot11NCapaParams;
   tRadioIfNpWrFsGetDot11NExtCapaParams  sConfigDot11NExtCapaParams;
   tRadioIfNpWrFsGetDot11NBeamFormCapaParams  sConfigDot11NBeamFormCapaParams;
   tRadioIfNpWrFsGetSuppMcsParams   sConfigDot11NsuppMcsParams;
   tRadioIfNpWrFsGetAmpduParams     sConfigDot11NampduParams;
   tRadioIfNpWrFsGetChannelInfo    sChannelInfo;
   tRadioIfNpWrFsHwUpdateRadarStatus  sRadarStatus;
   tRadioIfNpWrSetDot11Multicast                      sConfigSetDot11Multicast;
   tRadioIfNpWrGetDot11Multicast                      sConfigGetDot11Multicast;
   tRadioIfNpWrSetWlanIsolation         sConfigSetWlanIsolation;
#ifdef PMF_WANTED
   tRadioIfNpWrFsGetSeqNum           sConfigGetSeqNum;
#endif
#ifdef BAND_SELECT_WANTED
   tRadioIfNpWrFsDelStaBandSteer     sConfigDelStaBandSteer;
   tRadioIfNpWrFsBandSelectStatus  sBandSelectStatus;
#endif
   tRadioIfNpWrGetWlanIfName       sConfigGetWlanIfName;
  tRadioIfNpWrFsHwSetDot11nParams  sFsHwSetDot11nParams;
  tRadioIfNpWrFsHwGetDot11nParams  sFsHwGetDot11nParams;
  tRadioIfNpWrWlanAddVlanIntf  sConfigWlanAddVlanIntf;
  tRadioIfNpWrSetWpaIEParams        sConfigSetWpaIEParams;
 }unOpCode;

#define  RadioIfNpWrHwSetRadioDefault              unOpCode.sConfigSetRadioDefault;
#define  RadioIfNpWrHwGetRadioDefault              unOpCode.sConfigGetRadioDefault;
#define  RadioIfNpWrFsHwInit                    unOpCode.sConfigHwInit;
#define  RadioIfNpWrFsHwDeInit       unOpCode.sConfigHwDeInit;
#define  RadioIfNpFsHwUpdateAdminStatusEnable    unOpCode.sFsHwUpdateAdminStatusEnable;
#define  RadioIfNpFsHwUpdateAdminStatusDisable   unOpCode.sFsHwUpdateAdminStatusDisable;
#define  RadioIfNpFsHwSetAntennaMode    unOpCode.sFsHwSetAntennaMode;
#define  RadioIfNpFsHwGetAntennaMode  unOpCode.sFsHwGetAntennaMode;
#define  RadioIfNpFsHwGetCurrentTxAntenna  unOpCode.sFsHwGetCurrentTxAntenna;
#define  RadioIfNpFsHwSetAntennaSelection  unOpCode.sFsHwSetAntennaSelection;
#define  RadioIfNpFsHwGetAntennaSelection  unOpCode.sFsHwGetAntennaSelection;
#define  RadioIfNpFsHwSetAntennaDiversity  unOpCode.sFsHwSetAntennaDiversity;
#define  RadioIfNpFsHwGetAntennaDiversity  unOpCode.sFsHwGetAntennaDiversity;
#define  RadioIfNpConfigSetDot11RTSThreshold  unOpCode.sConfigSetDot11RTSThreshold;
#define  RadioIfNpConfigGetDot11RTSThreshold  unOpCode.sConfigGetDot11RTSThreshold;
#define  RadioIfNpConfigSetDot11FragThreshold  unOpCode.sConfigSetDot11FragThreshold;
#define  RadioIfNpConfigGetDot11FragThreshold  unOpCode.sConfigGetDot11FragThreshold;
#define  RadioIfNpFsHwGetDot11ShortRetryLimit   unOpCode.sFsHwSGetDot11ShortRetryLimit;     
#define  RadioIfNpFsHwSetDot11LongRetryLimit    unOpCode.sFsHwSetDot11LongRetryLimit; 
#define  RadioIfNpConfigSetDot11QosInfoTableParams    unOpCode.sConfigSetDot11EDCATableParams; 
#define  RadioIfNpFsHwGetDot11LongRetryLimit    unOpCode.sFsHwGetDot11LongRetryLimit; 
#define  RadioIfNpFsHwSetOFDMChannelWidth    unOpCode.sFsHwSetOFDMChannelWidth; 
#define  RadioIfNpFsHwGetOFDMChannelWidth    unOpCode.sFsHwGetOFDMChannelWidth; 
#define  RadioIfNpFsHwSetTxCurrPower  unOpCode.sFsHwSetTxCurrPower;
#define  RadioIfNpFsHwGetTxCurrPower  unOpCode.sFsHwGetTxCurrPower;
#define  RadioIfNpFsHwSetBeaconInterval  unOpCode.sFsHwSetBeaconInterval;
#define  RadioIfNpFsHwGetBeaconInterval  unOpCode.sFsHwGetBeaconInterval;
#define  RadioIfNpFsHwSetDot11nConfig  unOpCode.sFsHwSetDot11nConfig;
#define  RadioIfNpFsHwGetDot11nConfig  unOpCode.sFsHwGetDot11nConfig;
#define  RadioIfNpConfigSetDot11EDCATableParams  unOpCode.sConfigSetDot11EDCATableParams;
#define  RadioIfNpConfigSetDot11EDCATableCWmax  unOpCode.sConfigSetDot11EDCATableCWmax;
#define  RadioIfNpConfigGetDot11EDCATableCWmax  unOpCode.sConfigGetDot11EDCATableCWmax;
#define  RadioIfNpConfigSetDot11EDCATableCWmin  unOpCode.sConfigSetDot11EDCATableCWmin;
#define  RadioIfNpConfigGetDot11EDCATableCWmin  unOpCode.sConfigGetDot11EDCATableCWmin;
#define  RadioIfNpConfigSetDot11EDCATableAifs  unOpCode.sConfigSetDot11EDCATableAifs;
#define  RadioIfNpConfigGetDot11EDCATableAifs  unOpCode.sConfigGetDot11EDCATableAifs;
#define  RadioIfNpConfigSetDot11EDCATableTxOpLmt  unOpCode.sConfigSetDot11EDCATableTxOpLmt;
#define  RadioIfNpConfigGetDot11EDCATableTxOpLmt  unOpCode.sConfigGetDot11EDCATableTxOpLmt;
#define  RadioIfNpConfigSetDot11EDCATableAdmsnCtrl   unOpCode.sConfigSetDot11EDCATableAdmsnCtrl;
#define  RadioIfNpConfigGetDot11EDCATableAdmsnCtrl   unOpCode.sConfigGetDot11EDCATableAdmsnCtrl;
#define  RadioIfNpConfigSetDot11HideEssid            unOpCode.sConfigSetDot11HideEssid;
#define  RadioIfNpConfigGetDot11HideEssid            unOpCode.sConfigGetDot11HideEssid;
#define  RadioIfNpWrConfigSetDot11DtimPrd            unOpCode.sConfigSetDot11DtimPrd;
#define  RadioIfNpWrConfigGetDot11DtimPrd            unOpCode.sConfigGetDot11DtimPrd;
#define  RadioIfNpWrConfigSetDot11StaWepKeyEncryptn unOpCode.sConfigSetStaWepKeyEncryptn;
#define  RadioIfNpWrConfigGetDot11StaWepKeyEncryptn unOpCode.sConfigGetStaWepKeyEncryptn;
#define  RadioIfNpConfigSetDot11StWpAuthKeyType   unOpCode.sConfigSetStWpKeyAuthStatus;
#define  RadioIfNpConfigGetDot11StWpAuthKeyType   unOpCode.sConfigGetStWpKeyAuthStatus;
#define  RadioIfNpConfigSetDot11StaWepKeyStatus    unOpCode.sConfigSetDot11StaWepKeyStatus;
#define  RadioIfNpConfigGetDot11StaWepKeyStatus    unOpCode.sConfigGetDot11StaWepKeyStatus;
#define  RadioIfNpConfigSetDot11SecPrivStatus      unOpCode.sConfigSetDot11SecPrivStatus;
#define  RadioIfNpConfigGetDot11SecPrivStatus      unOpCode.sConfigGetDot11SecPrivStatus;
#define  RadioIfNpConfigSetDot11SecExUnEnStat      unOpCode.sConfigSetDot11SecExUnEnStat;
#define  RadioIfNpConfigGetDot11SecExUnEnStat      unOpCode.sConfigGetDot11SecExUnEnStat;
#define  RadioIfNpConfigSetDot11anSuppMode        unOpCode.sConfigSetDot11anSuppMode;
#define  RadioIfNpConfigGetDot11anSuppMode        unOpCode.sConfigGetDot11anSuppMode;
#define  RadioIfNpConfigSetDot11nAMPDUSuppStat  unOpCode.sConfigSetDot11nAMPDUSuppStat;
#define  RadioIfNpConfigGetDot11nAMPDUSuppStat  unOpCode.sConfigGetDot11nAMPDUSuppStat;
#define  RadioIfNpConfigSetDot11CWMStatus       unOpCode.sConfigSetDot11CWMStatus;
#define  RadioIfNpConfigGetDot11CWMStatus       unOpCode.sConfigGetDot11CWMStatus;
#define  RadioIfNpConfigSetDot11Country         unOpCode.sConfigSetDot11Country;
#define  RadioIfNpConfigGetDot11Country         unOpCode.sConfigGetDot11Country;
#define  RadioIfNpConfigSetDot11Frequency       unOpCode.sConfigSetDot11Frequency;
#define  RadioIfNpConfigGetDot11Frequency       unOpCode.sConfigGetDot11Frequency;
#define  RadioIfNpConfigSetDot11Essid           unOpCode.sConfigSetDot11Essid;
#define  RadioIfNpConfigSetDot11PowerConstraint unOpCode.sConfigSetDot11PowerConstraint;
#define  RadioIfNpConfigSetDot11Capability      unOpCode.sConfigSetDot11Capability;
#define  RadioIfNpConfigGetDot11Essid           unOpCode.sConfigGetDot11Essid;
#define  RadioIfNpConfigSetDot11gSuppMode        unOpCode.sConfigSetDot11gSuppMode;
#define  RadioIfNpConfigGetDot11gSuppMode        unOpCode.sConfigGetDot11gSuppMode;
#define  RadioIfNpConfigSetDot11gnSuppMode        unOpCode.sConfigSetDot11gnSuppMode;
#define  RadioIfNpConfigGetDot11gnSuppMode        unOpCode.sConfigGetDot11gnSuppMode;
#define  RadioIfNpConfigSetDot11acSuppMode        unOpCode.sConfigSetDot11acSuppMode;
#define  RadioIfNpConfigGetDot11acSuppMode        unOpCode.sConfigGetDot11acSuppMode;
#define  RadioIfNpConfigGetDot11Rad_Stats       unOpCode.sConfigGetDot11Rad_Stats;
#define  RadioIfNpConfigSetDot11aSuppMode        unOpCode.sConfigSetDot11aSuppMode;
#define  RadioIfNpConfigGetDot11aSuppMode        unOpCode.sConfigGetDot11aSuppMode;
#define  RadioIfNpConfigSetDot11bSuppMode        unOpCode.sConfigSetDot11bSuppMode;
#define  RadioIfNpConfigGetDot11bSuppMode        unOpCode.sConfigGetDot11bSuppMode;
#define  RadioIfNpConfigDot11AddMac              unOpCode.sConfigDot11AddMac;
#define  RadioIfNpConfigDot11DelMac              unOpCode.sConfigDot11DelMac;
#define  RadioIfNpConfigDot11WlanCreate          unOpCode.sConfigDot11WlanCreate;
#define  RadioIfNpConfigDot11WlanDestroy         unOpCode.sConfigDot11WlanDestroy;
#define  RadioIfNpConfigDot11BrInterfaceCreate   unOpCode.sConfigDot11CreateBrInterface;
#define  RadioIfNpConfigSetDot11Channel          unOpCode.sConfigSetDot11Channel;
#define  RadioIfNpConfigGetDot11Channel           unOpCode.sConfigGetDot11Channel;
#define  RadioIfNpConfigSetDot11ShortGi          unOpCode.sConfigSetDot11ShortGi; 
#define  RadioIfNpConfigGetDot11ShortGi          unOpCode.sConfigGetDot11ShortGi;
#define  RadioIfNpConfigSetDot11MaxTxPow2G     unOpCode.sConfigSetDot11MaxTxPow2G;
#define  RadioIfNpConfigGetDot11MaxTxPow2G     unOpCode.sConfigGetDot11MaxTxPow2G;
#define  RadioIfNpConfigSetDot11MaxTxPow5G     unOpCode.sConfigSetDot11MaxTxPow5G;
#define  RadioIfNpConfigGetDot11MaxTxPow5G     unOpCode.sConfigGetDot11MaxTxPow5G;
#define  RadioIfNpConfigSetDot11HwAddr         unOpCode.sConfigSetDot11HwAddr;
#define  RadioIfNpConfigGetDot11HwAddr         unOpCode.sConfigGetDot11HwAddr;
#define  RadioIfNpSetDot11AthHwAddr            unOpCode.sConfigSetDot11AthHwAddr;
#define  RadioIfNpGetDot11AthHwAddr            unOpCode.sConfigGetDot11AthHwAddr;
#define  RadioIfNpGetDot11BitRates             unOpCode.sConfigGetDot11BitRates;
#define  RadioIfNpSetDot11BitRates             unOpCode.sConfigSetDot11BitRates;
#define  RadioIfNpWrDot11StaAuthDeAuth         unOpCode.sConfigDot11StaAuthDeAuth;
#define  RadioIfNpSetDot11StaFwd               unOpCode.sConfigSetDot11StaFwd;
#define  RadioIfNpGetDot11StaFwd               unOpCode.sConfigGetDot11StaFwd;
#define  RadioIfNpGet80211Stats              unOpCode.sConfigGet80211Stats;
#define  RadioIfNpSetDot11NRatesStat         unOpCode.sConfigSetDot11NRatesStat;
#define  RadioIfNpGetDot11NRatesStat         unOpCode.sConfigGetDot11NRatesStat;
#ifdef WPS_WTP_WANTED
#define  RadioIfNpSetDot11WpsAdditionalParams      unOpCode.sConfigSetWpsAdditionalParams;
#define  RadioIfNpGetDot11WpsAdditionalIEParams     unOpCode.sConfigGetWpsAdditionalParams;
#define  RadioIfNpSetDot11WpsIEParams         unOpCode.sConfigSetWpsIEParams;
#define  RadioIfNpGetDot11WpsIEParams         unOpCode.sConfigGetWpsIEParams;
#endif
#define  RadioIfNpSetDot11RsnaIEParams         unOpCode.sConfigSetRsnaIEParams;
#define  RadioIfNpGetDot11RsnaIEParams         unOpCode.sConfigGetRsnaIEParams;
#define  RadioIfNpSetDot11RsnaPairwiseParams  unOpCode.sConfigSetRsnaPairwiseParams;
#define  RadioIfNpGetDot11RsnaPairwiseParams  unOpCode.sConfigGetRsnaPairwiseParams;
#define  RadioIfNpSetDot11RsnaGroupwiseParams  unOpCode.sConfigSetRsnaGroupwiseParams;
#define  RadioIfNpGetDot11RsnaGroupwiseParams  unOpCode.sConfigGetRsnaGroupwiseParams;
#ifdef PMF_WANTED
#define  RadioIfNpSetDot11RsnaMgmtGroupwiseParams  unOpCode.sConfigSetRsnaMgmtGroupwiseParams;
#define  RadioIfNpGetDot11RsnaMgmtGroupwiseParams  unOpCode.sConfigGetRsnaMgmtGroupwiseParams;
#endif
#define  RadioIfNpGetInterfaceInOctets         unOpCode.sConfigGetInterfaceInOctets;
#define  RadioIfNpGetInterfaceOutOctets         unOpCode.sConfigGetInterfaceOutOctets;
#define  RadioIfNpGetClientStatsBytesRecvdCount unOpCode.sConfigGetClientStatsBytesRecvdCount;
#define  RadioIfNpGetClientStatsBytesSentCount  unOpCode.sConfigGetClientStatsBytesSentCount;
#define  RadioIfNpWrDot11StaMCSRate         unOpCode.sConfigDot11StaMCSRate;
#define  RadioIfNpConfigSetDot11n           unOpCode.sConfigSetDot11n;
#define  RadioIfNpConfigGetDot11n           unOpCode.sConfigGetDot11n;
#define  RadioIfNpConfigGetDot11nExtCap     unOpCode.sConfigDot11NExtCapaParams;
#define  RadioIfNpConfigGetDot11nBeamFormCap unOpCode.sConfigDot11NBeamFormCapaParams;
#define  RadioIfNpConfigSetDot11ac          unOpCode.sConfigSetDot11ac;
#define  RadioIfNpConfigGetDot11ac          unOpCode.sConfigGetDot11ac;
#define  RadioIfNpWrGetDot11AcCapaParams  unOpCode.sConfigDot11AcCapaParams;
#define  RadioIfNpWrGetDot11SuppRadioType  unOpCode.sConfigDot11SuppRadioType;
#define  RadioIfNpWrSetLocalRouting        unOpCode.sConfigSetLocalRouting;
#define  RadioIfNpWrGetMultiDomainInfo     unOpCode.sConfigGetMultiDomainInfo;
#define  RadioIfNpWrGetScanDetails        unOpCode.sConfigGetScanDetails;
#define  RadioIfNpWrGetDot11NCapaParams  unOpCode.sConfigDot11NCapaParams;
#define  RadioIfNpWrGetDot11NsuppMcsParams  unOpCode.sConfigDot11NsuppMcsParams;
#define  RadioIfNpWrGetDot11NampduParams  unOpCode.sConfigDot11NampduParams;
#define  RadioIfNpWrGetChannelInfo         unOpCode.sChannelInfo;
#define  RadioIfNpWrUpdateRadarStatus         unOpCode.sRadarStatus;
#define  RadioIfNpWrGetDot11Multicast         unOpCode.sConfigGetDot11Multicast;
#define  RadioIfNpWrSetDot11Multicast         unOpCode.sConfigSetDot11Multicast;
#define  RadioIfNpWrSetWlanIsolation         unOpCode.sConfigSetWlanIsolation;
#ifdef PMF_WANTED
#define  RadioIfNpWrGetSeqNum             unOpCode.sConfigGetSeqNum;
#endif
#define  RadioIfNpWrSetRougeApDetectionGroupId  unOpCode.sConfigSetRougeApDetection;
#ifdef BAND_SELECT_WANTED
#define  RadioIfNpWrFsBandSelectStatus         unOpCode.sBandSelectStatus;
#define  RadioIfNpWrDelStaBandSteer             unOpCode.sConfigDelStaBandSteer;
#endif
#define  RadioIfNpWrGetWlanIfName          unOpCode.sConfigGetWlanIfName;
#define  RadioIfNpWrSetDot11nParams         unOpCode.sFsHwSetDot11nParams;
#define  RadioIfNpWrGetDot11nParams         unOpCode.sFsHwGetDot11nParams;
#define  RadioIfNpWrWlanAddVlanIntf        unOpCode.sConfigWlanAddVlanIntf;
#define  RadioIfNpWrSetWpaIEParams  unOpCode.sConfigSetWpaIEParams;
}tRadioIfNpModInfo;



#endif /* __RADIOIF_NP_WR_H__ */
