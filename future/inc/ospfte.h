/********************************************************************
 * Copyright (C) 2007 Aricent Inc.
 *
 * $Id: ospfte.h,v 1.15 2016/02/26 09:48:51 siva Exp $
 *
 * Description:This file contains the exported definitions and  
 *             macros of OSPF-TE.
 *
 *******************************************************************/
#ifndef _OSPF_TE_H_
#define _OSPF_TE_H_

#define OSPF_TE_MAX_NEXT_HOPS    16
/* For DiffServ case Max Te class level can be mapped to
 * OSPF_TE_MAX_PRIORITY_LVL */
#define OSPF_TE_MAX_PRIORITY_LVL  8

#define  OSPF_TE_INVALID_INDICATION 255

/* The Below Macros corresponds to u1PathInfo in CspfcompInfo and
 * u1Flag in CspfReq */
#define  OSPF_TE_RESOURCE_AFFINITY_BIT_MASK         1    /* 00000001 */
#define  OSPF_TE_BI_DIRECTIONAL_PATH_BIT_MASK       2    /* 00000010 */
#define  OSPF_TE_BACKUP_PATH_BIT_MASK               4    /* 00000100 */
#define  OSPF_TE_MPLS_LINK_BIT_MASK                 8    /* 00001000 */
#define  OSPF_TE_GMPLS_LINK_BIT_MASK               16    /* 00010000 */
#define  OSPF_TE_SEGMENT_PROTECTION_PATH_BIT_MASK  32    /* 00100000 */
#define  OSPF_TE_MAKE_BEFORE_BREAK_PATH_BIT_MASK   64    /* 01000000 */
#define  OSPF_TE_PRIMARY_PATH_BIT_MASK            128    /* 10000000 */

/* Below Macros corresponds to the u1Diversity in CspfCompInfo 
 * and CspfReq */
#define  OSPF_TE_NODE_DISJOINT_BIT_MASK    1
#define  OSPF_TE_LINK_DISJOINT_BIT_MASK    2
#define  OSPF_TE_SRLG_DISJOINT_BIT_MASK    4

/* Below Macros corresponds to the srlgtype of CspfReq */
#define  OSPF_TE_SRLG_TYPE_INCLUDE_ANY     1
#define  OSPF_TE_SRLG_TYPE_INCLUDE_ALL     2 
#define  OSPF_TE_SRLG_TYPE_EXCLUDE_ANY     3

/* Below Macros corresponds to the srlgtype of CspfReq */
#define  OSPF_TE_RSRC_TYPE_INCLUDE_ALL     1 
#define  OSPF_TE_RSRC_TYPE_INCLUDE_ANY     2
#define  OSPF_TE_RSRC_TYPE_EXCLUDE_ANY     3


#define OSPF_TE_PSC_1  1 
#define OSPF_TE_PSC_2  2 
#define OSPF_TE_PSC_3  3 
#define OSPF_TE_PSC_4  4 
#define OSPF_TE_L2SC   51  
#define OSPF_TE_TDM    100 
#define OSPF_TE_LSC    150 
#define OSPF_TE_FSC    200 

#define OSPF_TE_SUCCESS           0
#define OSPF_TE_FAILURE          -1
/* To Return the error code of Path Computation */
#define CSPF_PATH_FOUND           0
#define CSPF_PATH_NOT_FOUND       1

#define  OSPF_TE_LINK_PROTECTION_TYPE_EXTRATRAFFIC    1
#define  OSPF_TE_LINK_PROTECTION_TYPE_UNPROTECTED     2
#define  OSPF_TE_LINK_PROTECTION_TYPE_SHARED          3
#define  OSPF_TE_LINK_PROTECTION_TYPE_DEDICATED1FOR1  4
#define  OSPF_TE_LINK_PROTECTION_TYPE_DEDICATED1PLUS1 5
#define  OSPF_TE_LINK_PROTECTION_TYPE_ENHANCED        6

#define OSPF_TE_RM_AREAID_INFO           1
#define OSPF_TE_RM_DATALINK_INFO         2
#define OSPF_TE_RM_DATACONTROLLINK_INFO  3

#define OSPF_TE_LINK_CREATE       1
#define OSPF_TE_LINK_UPDATE       2
#define OSPF_TE_LINK_DELETE       3

/* This is filled in u2Info of explicit route or alternate route */
#define OSPF_TE_PRIMARY_PATH           1
#define OSPF_TE_BACKUP_PATH            2
#define OSPF_TE_STRICT_EXP_ROUTE       3
#define OSPF_TE_LOOSE_EXP_ROUTE        4
#define OSPF_TE_FAILED_LINK            5
#define OSPF_TE_STRICT_LOOSE_EXP_ROUTE 6
#define OSPF_TE_INCLUDE_ANY            7

#define OSPF_TE_ROUTER_LSA_INFO  1
#define OSPF_TE_NEIGHBOR_INFO    2
#define OSPF_TE_PASSIVE_INT_INFO 3

#define OSPF_TE_ENCODING_TYPE_PACKET    1
#define OSPF_TE_ENCODING_TYPE_ETHERNET  2
/* OSPF_TE MODULE Specific Trace Categories */
#define  OSPF_TE_CRITICAL_TRC          0x00000001
#define  OSPF_TE_FN_ENTRY              0x00000002
#define  OSPF_TE_FN_EXIT               0x00000004
#define  OSPF_TE_CSPF_TRC              0x00000008
#define  OSPF_TE_FAILURE_TRC           0x00000010
#define  OSPF_TE_RESOURCE_TRC          0x00000020
#define  OSPF_TE_CONTROL_PLANE_TRC     0x00000040
#define  OSPF_TE_ALL_TRC               0x0000007f 
          /* Care must be taken to change this 
          value when new TRC type is introduced */
#define OSPF_TE_MAX_SRLG_NO            16
#define OSPF_TE_MAX_DESCRIPTOR_NO      4

#define OSPF_TE_TLM_MODULE_UP   1
#define OSPF_TE_TLM_MODULE_DOWN 2
#define OSPF_TE_TLM_LINK_INFO   3
typedef float tBandWidth;


typedef struct _OsTeNextHop {
    UINT4    u4RouterId;
    UINT4    u4NextHopIpAddr;
    UINT4    u4RemoteIdentifier;
} tOsTeNextHop;

typedef struct _OsTeSrlg {
    UINT4    aSrlgNumber[OSPF_TE_MAX_SRLG_NO];
                    /* Array Holding the SRLG numbers of the interface */
    UINT4    u4NoOfSrlg;
                    /* Number of SRLG numbers */
} tOsTeSrlg;

typedef struct _OsTeExpRoute {
    tOsTeNextHop    aNextHops[OSPF_TE_MAX_NEXT_HOPS];
    tOsTeSrlg       aSrlg[OSPF_TE_MAX_NEXT_HOPS];
    UINT2           u2Info;
    UINT2           u2HopCount;
} tOsTeExpRoute;


typedef struct _OsTeAppPath {
    tOsTeNextHop    aNextHops[OSPF_TE_MAX_NEXT_HOPS];
    tOsTeSrlg       aSrlg[OSPF_TE_MAX_NEXT_HOPS];
    UINT4           u4AreaId;
    UINT4           u4TeMetric;
    UINT2           u2PathType;
    UINT2           u2HopCount;
    UINT1           u1EncodingType;
    UINT1           u1PathReturnCode;
    UINT1           au1Unused [2];
} tOsTeAppPath;


/* ----------------------------------------------- */
/* Resource Manager to OSPF Link Message structure */
/* ----------------------------------------------- */
/* Holds the Link information given by the Resource Manager to OSPF-TE*/
typedef struct _OsTeLinkMsg {
    UINT4      u4LocalIpAddr;       /* Local IP address of the 
                                     * interface                   */
    UINT4      u4IfIndex;           /* Interface Index */
    UINT4      u4RemoteIpAddr;      /* Remote IP address of the 
                                     * interface                   */
    UINT4      u4RemoteRtrId;       /* Remote Routers RtrId        */ 
    UINT4      u4TeMetric;          /* TE Metric value             */
    UINT4      u4RsrcClassColor;    /* Administrative Group        */
    UINT4      u4LocalIdentifier;   /* Local Identifier value      */
    UINT4      u4RemoteIdentifier;  /* Remote Identifier value     */
    tBandWidth maxBw;               /* Maximum Bandwidth */
    tBandWidth maxResBw;            /* Maximum Unreserved Bandwidth*/
    tBandWidth aUnResBw [OSPF_TE_MAX_PRIORITY_LVL];
                                    /* Unreserved Bandwidth        */
    UINT4      u4IfDescCnt;         /* Interface descriptor count  */
    UINT1      *pIfDescriptors;     /* Pointer to interface 
                                     * descriptors                 */
    UINT4      u4IfSrlgCnt;         /* SRLG count on an interface  */
    UINT1      *pSrlgs;             /* SRLG information            */
    UINT4      u4TeLinkIfType;      /*TE Link interface type 1.Point
                                      to point 2.Multiaccess*/
    UINT4      u4AreaId;            /* Area Id                     */
    UINT4      u4ModuleId;          /* Module Id that fills this 
                                       structure. May hold RM or 
                                       TLM                         */
    UINT4      u4CfaPortIndex;      /* Cfa Port Index. To be filled
                                       by TLM in case of FA TE Link*/
    UINT1      u1ProtectionType;    /* Protection Type             */   
    UINT1      u1InfoType;          /* Information Type            */
    UINT1      u1LinkStatus;        /* Link information --     */
                                    /* Create -- Modify -- Delete  */
    UINT1      u1MsgSubType;
} tOsTeLinkMsg;

/* -------------------------------------- */
/* Application Interface descriptor structure */
/* -------------------------------------- */
typedef struct _OsTeIfDesc {
    /* Descriptor Min LSP bandwidth */
    tBandWidth         minLSPBw;
    /* Descriptor Max LSP bandwidth at Priority */
    tBandWidth         aDescMaxLSPBw [OSPF_TE_MAX_PRIORITY_LVL];
    /* Descriptor identifier  */ 
    UINT4              u4DescrId;
    /* MTU value  */
    UINT2              u2MTU;
    /* Descriptor Switching capability */
    UINT1              u1SwitchingCap;
    /* Descriptor Encoding type */
    UINT1              u1EncodingType; 
    /* Descriptor Indication - used only for TDM */
    UINT1              u1Indication;
    /* Padding */
    UINT1              u1Rsvd[3];
} tOsTeIfDesc;

/* To Provide Backward Compatability */
typedef tOsTeLinkMsg tRmOsTeLinkMsg;


/* Structure filled by external modules for CSPF Computation */
typedef struct _CspfCompInfo {
    /* Based on the u1SrlgType, include any srlg or include all 
     * srlg or exclude any srlg is considered */
    tOsTeSrlg             osTeSrlg;
    /* Explicit route path. In Case of MBB, will hold the primary path */
    tOsTeExpRoute         osTeExpRoute;
    /* In Case of MBB, MAY hold the Alternate Path if Specified 
     * The Alternate path specified may be include or exclude */
    tOsTeExpRoute         osTeAlternateRoute;
    /* Bandwidth Desired */
    tBandWidth            bandwidth;
    /* In Case of MBB, holds the bandwidth of the tunnel mentioned
     * in Primary Path */
    tBandWidth            oldbandwidth;
    /* Source to be considered by CSPF */
    UINT4                 u4SrcIpAddr; 
    /* Destination to be considered by CSPF */
    UINT4                 u4DestIpAddr; 
    /* Tunnel Index */
    UINT4                 u4TnlIndex; 
    /* Tunnel Instance */
    UINT4                 u4TnlInst; 
    /* Ingress id of the tunnel */
    UINT4                 u4TnlSrcAddr;
    /* Egress id of the tunnel */
    UINT4                 u4TnlDestAddr;
    /* Administrative groups to be considered if any one of the value matches */
    UINT4               u4IncludeAnySet;
    /* Administrative groups to be considered for exact match */
    UINT4                u4IncludeAllSet;
    /* Administrative group to be avoided if any one of the value matches */
    UINT4                 u4ExcludeAnySet;
    /* Protection type desired */
    UINT1                 u1ProtectionType;
    /* Switching capability desired */
    UINT1                 u1SwitchingCap;
    /* Encoding type desired */
    UINT1                 u1EncodingType;
    /* Priority desired - In case of Diff-serv, should be considered as
     * TE-CLASS Priority */
    UINT1                 u1Priority; 
    /* May hold the flags: OSPF_TE_NODE_DISJOINT_BIT_MASK or
     * OSPF_TE_LINK_DISJOINT_BIT_MASK or OSPF_TE_SRLG_DISJOINT_BIT_MASK or 
     * ALL */
    UINT1                 u1Diversity;
    /* primary path, backup path ,make beforebreak, direction, explicit route*/
    UINT1                 u1PathInfo; 
    /* May hold OSPF_TE_SRLG_TYPE_INCLUDE_ALL or OSPF_TE_SRLG_TYPE_INCLUDE_ANY
     * or OSPF_TE_SRLG_TYPE_EXCLUDE_ANY  */
    UINT1                 u1SrlgType;
    /* May hold OSPF_TE_RSRC_TYPE_INCLUDE_ALL or OSPF_TE_RSRC_TYPE_INCLUDE_ANY
     * or OSPF_TE_RSRC_TYPE_EXCLUDE_ANY  */
    UINT1                 u1RsrcSetType;
}tCspfCompInfo;


PUBLIC INT4 OspfTeStart         (VOID);
PUBLIC VOID OspfTeShutDown      (VOID);
PUBLIC INT4 OspfTeLock (VOID);
PUBLIC INT4 OspfTeUnLock (VOID);
PUBLIC INT4 OspfTeRmgrRegister (VOID);
PUBLIC INT4 OspfTeRmgrDeRegister (VOID);
PUBLIC INT4 OspfTeRmgrSendLinkInfo (tRmOsTeLinkMsg * pRmOsTeLinkMsg);
INT4
OspfTeEnqueueMsgFromRsvpTe (tCspfCompInfo *pCspfCompInfo);
PUBLIC VOID        RegisterOSPFTEMibs               PROTO ((void));
VOID OspfTeTaskMain      (INT1 *pTaskId);

/*API Provided to other modules to check OSPF-TE Admin Status */
PUBLIC BOOLEAN OspfTeIsEnabled (VOID);
#endif  /* _OSPF_TE_H_ */
