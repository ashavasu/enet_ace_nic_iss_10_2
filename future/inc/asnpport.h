/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: asnpport.h,v 1.3 2010/12/20 04:40:04 siva Exp $
 *
 * Description: This file contains the function prototypes of the 
                callback functions for Async NPAPIs
 * *******************************************************************/

#ifndef _ASNPPORT_H
#define _ASNPPORT_H

VOID IssAsyncNpUpdateStatusCb PROTO ((UINT4 u4NpCallId, unAsyncNpapi *lv));

VOID PnacNpCallBack PROTO ((UINT4 u4NpCallId, unAsyncNpapi *lv));
VOID LaAsyncNpUpdateStatus PROTO ((UINT4 u4NpCallId, unAsyncNpapi *lv));
VOID AstAsyncNpUpdateStatus PROTO ((UINT4 u4NpCallId, unAsyncNpapi *lv));
VOID IgsNpCallBack PROTO ((UINT4 u4NpCallId, unAsyncNpapi *lv));
VOID ErpsAsyncNpUpdateStatus (UINT4 u4NpCallId, unAsyncNpapi * NpParams);
#endif
