/* $Id: bcm.h,v 1.154.2.1 2018/03/16 13:39:40 siva Exp $ */
#ifndef _BCM_SIZE_H
#define _BCM_SIZE_H

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : bcm.h                                          */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        :                                                */
/*    MODULE NAME           :                                                */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : BCM 5690/5695, BCMX                            */
/*    DATE OF FIRST RELEASE : 5 May 2005                                     */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains sizing parameters for the   */
/*                            BCM 5690/5695, BCMX environment                */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    05 May 2005            Initial Creation                        */
/*---------------------------------------------------------------------------*/

/* In BCM 5650x family, no of filters per FP is available,
 * in the factor of 64. So this macro should be multiples of 64,
 * (i.e.) it should be 64, 128, 192, 256, etc.
 */

#define MAX_TAP_INTERFACES            0

/* Max no. FPs for RMONV2 */
#ifdef IP6_WANTED

#define RMON2_MAX_TUPLE_FLOW_STATS        384

#else /* IP6 Not Wanted  */

#define RMON2_MAX_TUPLE_FLOW_STATS        448

#endif

/* Supported Line Card types. To support a card type, change the corresponding
 * macro value to OSIX_TRUE.
 * Note: MBSM_MAX_PORTS_PER_SLOT in size.h should be updated as the maximum
 * of the number of ports present in the supported card type.
 * For example, when MBSM_BCM95695LM and MBSM_BCM956504LM Cards are 
 * supported MBSM_MAX_PORTS_PER_SLOT should be defined as 48.
 * Similarly when MBSM_BCM95695PB and MBSM_BCM956504PB alone are supported,
 * it should be defined as 24. */
#define  MBSM_BCM95695LM    OSIX_TRUE /* 5695 LM CARD */
#define  MBSM_BCM956504LM   OSIX_TRUE /* Firebolt LM CARD */
#define  MBSM_BCM956501LM   OSIX_TRUE /* Firebolt 501 LM CARD */
#define  MBSM_BCM95695PB    OSIX_TRUE  /* 5695 PIZZABOX CARD */
#define  MBSM_BCM956504PB   OSIX_TRUE  /* Firebolt PIZZABOX CARD */
#define  MBSM_BCM956314PB   OSIX_TRUE  /* PIZZABOX CARD */
#define  MBSM_BCM56601PB    OSIX_TRUE  /* Easyrider PIZZABOX CARD */
#define  MBSM_BCM56800PB    OSIX_TRUE  /* Bradley 56800 PIZZABOX CARD */

#ifdef MBSM_WANTED
#define SYS_DEF_MAX_RADIO_INTERFACES        0
#define SYS_DEF_MAX_ENET_INTERFACES      SYS_DEF_MAX_PHYSICAL_INTERFACES - \
                                         SYS_DEF_MAX_RADIO_INTERFACES
#define SYS_DEF_MAX_PHYSICAL_INTERFACES   \
(SYS_DEF_MAX_DATA_PORT_COUNT + SYS_DEF_MAX_INFRA_SYS_PORT_COUNT + \
                               SYS_DEF_MAX_RADIO_INTERFACES)

#define SYS_DEF_MAX_DATA_PORT_COUNT (MBSM_MAX_LC_SLOTS * MBSM_MAX_POSSIBLE_PORTS_PER_SLOT)
/* Max No of Front Panel Ports */
#define SYS_DEF_MAX_INFRA_SYS_PORT_COUNT 0/* Max No of connecting Ports
                                              used for Dual Unit Stacking*/

/* Load Sharing */
enum
{
    MBSM_LOAD_SHARING_ENABLE = 1,
    MBSM_LOAD_SHARING_DISABLE = 2
};

#else
#define SYS_DEF_MAX_RADIO_INTERFACES        0
#define SYS_DEF_MAX_ENET_INTERFACES      SYS_DEF_MAX_PHYSICAL_INTERFACES - \
                                         SYS_DEF_MAX_RADIO_INTERFACES
#define SYS_DEF_MAX_PHYSICAL_INTERFACES \
(SYS_DEF_MAX_DATA_PORT_COUNT + SYS_DEF_MAX_INFRA_SYS_PORT_COUNT + \
                               SYS_DEF_MAX_RADIO_INTERFACES)
#define SYS_DEF_MAX_DATA_PORT_COUNT 24 /* Max No of Front Panel Ports */
                                       /* For Elkhound chipset the value should be modified to 28*/
#define SYS_DEF_MAX_INFRA_SYS_PORT_COUNT 0 /* Max No of connecting Ports$
                                              used for Dual Unit Stacking*/
#endif
#define SYS_DEF_NUM_PHYSICAL_INTERFACES SYS_DEF_MAX_PHYSICAL_INTERFACES

#define ISS_MAX_UDB_FILTERS            128
#define ISS_MAX_POSSIBLE_STK_PORTS     4
#define ISS_MAX_STK_PORTS             IssGetStackPortCountFromNvRam()
#define ISS_DEFAULT_STACK_PORT_COUNT   0

/* BCM doesn't support MI, so MAX ports per context will be 
 * SYS_DEF_MAX_PHYSICAL_INTERFACES + AGG Interfaces. Hence map it to 
 * BRG_MAX_PHY_PLUS_LOG_PORTS. */
#define SYS_DEF_MAX_PORTS_PER_CONTEXT   BRG_MAX_PHY_PLUS_LOG_PORTS

#if (defined(MI_WANTED) && defined(VRF_WANTED))
#define SYS_DEF_MAX_NUM_CONTEXTS        8
#else
#define SYS_DEF_MAX_NUM_CONTEXTS        1
#endif

#define VLAN_MAX_PORTS_IN_SYSTEM        (BRG_MAX_PHY_PLUS_LOG_PORTS + \
                                          SYS_DEF_MAX_SBP_IFACES)

#define VLAN_MAX_PRIMARY_VLANS          256
#define VLAN_MAX_ISOLATED_VLANS         256
#define VLAN_MAX_COMMUNITY_VLANS        256


#define VLAN_MAX_VLAN_ID               (VLAN_DEV_MAX_VLAN_ID)

/* This macro will be used for creating dummy vlans in the bcm H/W 
 * Value should not be above 4094, as there is a restriction in the H/W */
#if (defined MBSM_WANTED) || (defined ICCH_WANTED)
#define IP_RPORT_MIN_ID              (VLAN_DEV_MAX_ETH_VLAN_ID - IP_MAX_RPORT - 1)
#else
#define IP_RPORT_MIN_ID              (VLAN_DEV_MAX_ETH_VLAN_ID - IP_MAX_RPORT)
#endif

#define IP_RPORT_MAX_ID               VLAN_DEV_MAX_ETH_VLAN_ID       

/*  No of VLANs are increased with count VLAN_MAX_NUM_VFI_IDS to provide VPLS 
 *  visibility to L2 modules.
 *  VLAN ID is increased with VLAN_DEV_MAX_VFI_ID.
 *
 *  Here we can configure 128 VLANs. For these VLANs, VLAN ID should be present
 *  between (1 - 2048).
 *
 *  We can configure VFIs for the count VLAN_MAX_NUM_VFI_IDS. For these VFIs, 
 *  VFI ID should be present between (VLAN_VFI_MIN_ID to VLAN_VFI_MAX_ID)
 */

/* When tuning the value for VLAN_DEV_MAX_VFI_ID, SNMP_MAX_OCTETSTRING_SIZE
 * also should be tuned. Because, memory pool and arrays for VLAN LIST are
 * based on the size of SNMP_MAX_OCTETSTRING_SIZE.*/

#define VLAN_DEV_MAX_ETH_VLAN_ID   4094
#define VLAN_DEV_MAX_CUSTOMER_VLAN_ID   4096
#define VLAN_DEV_MAX_VLAN_ID      (VLAN_DEV_MAX_ETH_VLAN_ID  +  VLAN_DEV_MAX_VFI_ID)

 /* Maximum number of VLANs*/
#define VLAN_DEV_MAX_NUM_VLAN     (VLAN_MAX_NUM_VFI_IDS +\
                                   VLAN_MAX_NUM_VLAN_IDS)

#ifdef MPLS_WANTED
#define VLAN_DEV_MAX_VFI_ID       64
#define VLAN_MAX_NUM_VFI_IDS      64
#define VLAN_MAX_NUM_VLAN_IDS     128
#else
#define VLAN_DEV_MAX_VFI_ID       0
#define VLAN_MAX_NUM_VFI_IDS      0
#define VLAN_MAX_NUM_VLAN_IDS     256
#endif

#define VLAN_VFI_MIN_ID           (VLAN_DEV_MAX_VLAN_ID - VLAN_DEV_MAX_VFI_ID)
#define VLAN_VFI_MAX_ID           VLAN_DEV_MAX_VLAN_ID

#define VLAN_DEV_MAX_ST_VLAN_ENTRIES    256  /* Maximum number of static
            * VLANs. This number should be
            * lesser than or equal to 
            * maximum number of VLANs */

#define VLAN_DEV_MAX_NUM_COSQ           8     /* Maximum number of hardware 
            * Queues
            */
#define QOS_DEV_MAX_MULTICAST_COSQ      8     /* Maximum number of multicast queues */

/*
 * if h/w supports portlist it is set to VLAN_TRUE and otherwise 
 * set to VLAN_FALSE.If portlist is not supported whenever the 
 * vlan member ports are modified we have to set/reset the 
 * added/deleted ports specificallly 
*/

#define VLAN_HW_PORTLIST_SUPPORTED()   VLAN_TRUE

#define VLAN_MAX_MAC_MAP_ENTRIES       1 /* applicable for MAC based VLAN */

#define VLAN_MAX_SUBNET_MAP_ENTRIES   128 /*applicable for Subnet based VLAN */

#define VLAN_MAX_VID_SET_ENTRIES_PER_PORT 10

/* STP Sizing Params */ 
#define AST_MAX_PORTS_IN_SYSTEM               BRG_MAX_PHY_PLUS_LOG_PORTS + \
                                    (AST_MAX_CEP_IN_PEB*AST_MAX_SERVICES_PER_CUSTOMER) 
#define  AST_MAX_CEP_IN_PEB                 8

#ifdef MSTP_WANTED
#define AST_MAX_MST_INSTANCES          (16 + 1) /* no. of instance + CIST */
#else
#ifdef ERPS_WANTED
#define AST_MAX_MST_INSTANCES          (64 + 1) /* no. of instance + CIST */
#else
#define AST_MAX_MST_INSTANCES          (16 + 1) /* no. of instance + CIST */
#endif
#endif /*MSTP_WANTED */

#define SNOOP_MAX_IGS_PORTS               BRG_MAX_PHY_PLUS_LOG_PORTS

#ifdef ELPS_WANTED
#define ELPS_SYS_MAX_PG                      1024
#define ELPS_SYS_MAX_NUM_SERVICE_IN_LIST     4094
#define ELPS_SYS_MAX_NUM_SERVICE_PTR_IN_LIST 2048
#endif

/* 
 * Only 17 STGs are supported in BCM and the first STG (with id as zero can
 * not be used for PVRST as Vlanid and instance are same.
 */
#define AST_MAX_PVRST_INSTANCES          16 

#define AST_MAX_INSTANCES   (AST_MAX_MST_INSTANCES > AST_MAX_PVRST_INSTANCES)? AST_MAX_MST_INSTANCES : AST_MAX_PVRST_INSTANCES

#ifdef PB_WANTED

#define     SVLAN_PORT_CVLAN_DSTIP_MAX_ENTRIES   1
#define     SVLAN_PORT_CVLAN_DSCP_MAX_ENTRIES    1
#define     SVLAN_PORT_CVLAN_SRCMAC_MAX_ENTRIES 1
#define     SVLAN_PORT_CVLAN_DSTMAC_MAX_ENTRIES  1
#define     SVLAN_PORT_CVLAN_MAX_ENTRIES 256
#define     CVLAN_MAX_ENTRIES_PER_PORT SVLAN_PORT_CVLAN_MAX_ENTRIES
#define     SVLAN_PORT_SRCIP_DSTIP_MAX_ENTRIES 1
#define     SVLAN_PORT_DSTIP_MAX_ENTRIES 1
#define     SVLAN_PORT_SRCIP_MAX_ENTRIES 1
#define     SVLAN_PORT_DSCP_MAX_ENTRIES 1
#define     SVLAN_PORT_DSTMAC_MAX_ENTRIES  1
#define     SVLAN_PORT_SRCMAC_MAX_ENTRIES  1

/* Vid Translation Table and Ethertype swap table are not supported in bcm.
 * for testing purpose the following two macros are kept a constant value 
 * (10). */
#define  VLAN_MAX_VID_TRANSLATION_ENTRIES   20 
#define  VLAN_MAX_ETHER_TYPE_SWAP_ENTRIES   10

#endif /*PB_WANTED*/

#define AST_MAX_SERVICES_PER_CUSTOMER   128

/* Vlan filtering entry sizing */

/* Maximum number of L2 static unicast entries -
 * This number includes PBB-TE static unicast entries also */
#define VLAN_DEV_MAX_ST_UCAST_ENTRIES   (3192) 
/* Maximum number of L2 dynamic unicast entries -
 * Assuming the number of learnt unicast entries to be 1000 */
#ifdef MBSM_WANTED
#define VLAN_DEV_MAX_L2_TABLE_SIZE      (32768 + MAX_FDB_INFO)
#else
#define VLAN_DEV_MAX_L2_TABLE_SIZE      (32768)
#endif /* MBSM_WANTED */
/* Maximum number of L2 multicast entries -
 * This number includes PBB-TE static multicast entries also */
#define VLAN_DEV_MAX_MCAST_TABLE_SIZE   (100)

#define VLAN_DEF_DYN_UCAST_ENTRIES_PER_VLAN   128 /* Default value of Limit  
                                     * of Dynamic unicast entries that can be 
                                     * learnt per vlan. 10% of the maximum
                                     * L2 unicast entries per switch.
                                     */

#define IP_DEV_MAX_L3VLAN_INTF          (128 - IP_MAX_RPORT) 
#define SYS_DEF_MAX_L3SUB_IFACES        64 /* Maximum L3Subinterface */
                                        /* Maximum IP interfaces */

#define IP_DEV_MAX_TNL_INTF             20  /* Maximum IP interfaces */

#define LA_DEV_MAX_TRUNK_GROUP          8    /* Maximum Trunk groups */  
#define LA_MAX_PORTS_PER_AGG            8    /* Maximum no.of ports in 
                  * aggregation */
#define LA_MIN_PORT_INDEX_PER_AGG SYS_DEF_NUM_PHYSICAL_INTERFACES + 1
#define LA_MAX_PORT_INDEX_PER_AGG SYS_DEF_NUM_PHYSICAL_INTERFACES + LA_DEV_MAX_TRUNK_GROUP

#define VLAN_MAX_L2VPN_ENTRIES          MAX_L2VPN_PW_VC_ENTRIES
#ifdef MPLS_WANTED           
/* Total L3 interfaces available in h/w */
#define IP_DEV_MAX_IP_INTF              512

/* IP interfaces remaining after usage by L3IPVLAN type interfaces */
#define IP_DEV_MAX_REM_L3_IP_INTF       IP_DEV_MAX_IP_INTF -  \
                                        IP_DEV_MAX_L3VLAN_INTF - \
                                        SYS_DEF_MAX_MPLS_IFACES
#define SYS_DEF_MAX_L2_PSW_IFACES       100
#define SYS_DEF_MAX_L2_AC_IFACES        100

#else

#define IP_DEV_MAX_IP_INTF              IP_DEV_MAX_L3VLAN_INTF + IP_MAX_RPORT
#define SYS_DEF_MAX_L2_PSW_IFACES       0
#define SYS_DEF_MAX_L2_AC_IFACES        0

#endif

#ifdef VXLAN_WANTED
#define SYS_DEF_MAX_NVE_IFACES       1
#else
#define SYS_DEF_MAX_NVE_IFACES       0
#endif

 #define IP_DEV_MAX_ADDR_PER_IFACE       7   /* Maximum IP address associated 
                                               with the interface */
                                       

#define RATE_LIMIT_MAX_VALUE                 0x3ffff

/* ARP Table table Size  */
#ifdef MBSM_WANTED
#define IP_DEV_MAX_IP_TBL_SZ            512
#else
#define IP_DEV_MAX_IP_TBL_SZ            4000
#endif
 

/* Maximum Route entries. Route entries in the FIB(IP_DEV_MAX_ROUTE_TBL_SZ) depends on
 * the routes from different routing protocols.Route entries from various protocols 
 * should be tuned based on the limitation in H/W. */
#ifdef RIP_WANTED
#define RIP_DEF_MAX_ROUTES              1000
#else
#define RIP_DEF_MAX_ROUTES              0
#endif

#ifdef OSPF_WANTED
#ifdef MBSM_WANTED
#define OSPF_DEF_MAX_ROUTES             2500
#else
#define OSPF_DEF_MAX_ROUTES             2000
#endif
#else
#define OSPF_DEF_MAX_ROUTES             0
#endif

#ifdef BGP_WANTED
#ifdef MBSM_WANTED
#define BGP_DEF_MAX_ROUTES              2500
#else 
#define BGP_DEF_MAX_ROUTES              2000 
#endif
#else
#define BGP_DEF_MAX_ROUTES              0
#endif


#define IP_DEF_MAX_STATIC_ROUTES         100
#define IP_MAX_INTERFACE_ROUTES        ((IP_DEV_MAX_IP_INTF) * IP_DEV_MAX_ADDR_PER_IFACE)
                                        /* By default 128 * 5 */
                                        
#define IP_DEV_MAX_ROUTE_TBL_SZ         RIP_DEF_MAX_ROUTES + OSPF_DEF_MAX_ROUTES + \
                                        BGP_DEF_MAX_ROUTES + IP_DEF_MAX_STATIC_ROUTES + IP_MAX_INTERFACE_ROUTES

#define ISS_BCM_L3VPN_MAX_ROUTES       (BGP_DEF_MAX_ROUTES + IP_DEF_MAX_STATIC_ROUTES)
                                        /* Max MP-BGP & statically configured routes */
                                        
/* L3 multicast table size */ 
#define IP_DEV_L3MCAST_TABLE_SIZE       256
/* This macro specifies the number of egress ports that an IPMC entry
 * can have (Number of OIFs or VLANs * Number of ports in each OIF or VLAN) */
#define IP_DEV_L3MCAST_EGRESS_PER_IPMC 40
                                        
/*DIFFSERV*/

/*
 * For 5690 MinRefresCount = 1 Mbps, MaxRefresCount = 1023 Mbps
 * For 5695, Firebolt MinRefresCount = 64 Kbps, MaxRefresCount = 1048576 Kbps
 * */
#define NP_DIFFSRV_MIN_REFRESH_COUNT 64   /* Kb/s */
#ifdef MBSM_WANTED
#define NP_DIFFSRV_MAX_REFRESH_COUNT 1048576 /* (1023*1000)Kb/s */
#else
#define NP_DIFFSRV_MAX_REFRESH_COUNT 1023000 /* (1023*1000)Kb/s */
#endif


/* Maximum of 2K FP Rules for 8 Slices are allowed,  
 * and since L2 and L3 Filters are Dual Wide Slices 
 * a maximum of 128 L2 and L3 FP rules can be created. */
#define ISS_MAX_L2_FILTERS                  128 
#define ISS_MAX_L3_FILTERS                  128 
#define ISS_MAX_L4S_FILTERS      20
#define ISS_MAX_L2_L3_OUT_FILTERS                  256
/* Max range of Filter IDs created in control plane */

#define ISS_MAX_L2_FILTERS_ID               65535
#define ISS_MAX_L3_FILTERS_ID               65535

#define ISS_FILTER_SHADOW_MEM_SIZE          34  /* Filter Shadow Size */

#define   SYS_DEF_MAX_NBRS                  10



/* TCP */
#define   TCP_DEF_MAX_NUM_OF_TCB            500

/* PIM */
#define   PIM_DEF_MAX_SOURCES               100
#define   PIM_DEF_MAX_RPS                   100

/* DVMRP */
#define   DVMRP_DEF_MAX_SOURCES             100


/* OSPF */
#define   OSPF_DEF_MAX_LSA_PER_AREA         100 
#ifdef MBSM_WANTED
#define   OSPF_DEF_MAX_EXT_LSAS             2500
#else 
#define   OSPF_DEF_MAX_EXT_LSAS             2000 
#endif

/* BGP */
#define   BGP_DEF_MAX_PEERS                 10
#define   BGP_DEF_MAX_CAPS_PER_PEER         10
#define   BGP_DEF_MAX_CAP_DATA_SIZE         16
#define   BGP_DEF_MAX_INSTANCES_PER_CAP     5
#define   BGP_DEFAULT_MAX_INPUT_COMM_FILTER_TBL_ENTRIES      1000
#define   BGP_DEFAULT_MAX_OUTPUT_COMM_FILTER_TBL_ENTRIES     1000
#define   BGP_DEFAULT_MAX_INPUT_EXT_COMM_FILTER_TBL_ENTRIES  1000
#define   BGP_DEFAULT_MAX_OUTPUT_EXT_COMM_FILTER_TBL_ENTRIES 1000

/* DHCP Server */

#define DHCP_SRV_MAX_POOLS                  5
#define DHCP_SRV_MAX_HOST_PER_POOL          20

/* CLI and Telnet */

#define CLI_MAX_SESSIONS                    10
#define CLI_MAX_USERS                       20
#define CLI_MAX_GROUPS                      10

/* SSL and Web */

#define ISS_MAX_WEB_SESSIONS                10
#define ISS_MAX_SSL_SESSIONS                10

#define SYS_DEF_MAX_EVC_PER_PORT            1
#define SYS_MAX_NUM_EVCS_SUPPORTED          5 
                                        

/* LLDP Module specific sizing parameters */
#define LLDP_MAX_LOC_MAN_ADDR              (IP_DEV_MAX_L3VLAN_INTF + IP_MAX_RPORT)
 
#define LLDP_MAX_LOC_PROTOID                1  /* As protocol identity tlv is
                                                * not supported this macro is 
                                                * defined with minimum value.
                                                * Need to increase the value 
                                                * when there is support.i.e,
                                                * it should be LLDP_MAX_PORTS *
                                                * Number of protocols supported
                                                * on a port.
                                                */
#define LLDP_MAX_LOC_PPVID                  (VLAN_MAX_VID_SET_ENTRIES_PER_PORT * SYS_DEF_NUM_PHYSICAL_INTERFACES)
                                             /* 2 PPVID for maximum number of ports in system */

#define LLDP_MAX_LOC_VLAN                   VLAN_DEV_MAX_NUM_VLAN

#define LLDP_MAX_NEIGHBORS                  256 /* Maximum number of neighbors 
                                                  that can be learnt */
#define LLDP_MAX_REM_MAN_ADDR               50 /* Stores at least 1 mgmt 
                                                  address per neighbor */
#define LLDP_MAX_REM_UNKNOWN_TLV            10 /* unknown TLV information */
#define LLDP_MAX_REM_ORG_DEF_INFO_TLV       10 /* org defined unkown TLVs */
#define LLDP_MAX_REM_PROTOID                1  /* As protocol identity tlv is
                                                * not supported this macro is 
                                                * defined with minimum value.
                                                * Need to increase the value 
                                                * when there is support.i.e,
                                                * the value can be set to
                                                * 10 protocol id entries 
                                                * for each neighbor */
#define LLDP_MAX_REM_PPVID                  50  /* 1 PPVID per neighbor */
#define LLDP_MAX_REM_VLAN                   50

#ifdef STACKING_WANTED
#define MBSM_SLOT_ID_FILE               "nodeid"
#else
#define MBSM_SLOT_ID_FILE               "/proc/slotid"
#endif

#define QOS_PRI_MAP_TBL_MAX_ENTRIES        (100 + QOS_MAX_PCP_PRI_MAP_ENTRIES)
#define QOS_CLS_MAP_TBL_MAX_ENTRIES        (100 + QOS_MAX_PCP_CLS_MAP_ENTRIES)
#define QOS_CLS2PRI_MAP_TBL_MAX_ENTRIES    (100)
#define QOS_METER_TBL_MAX_ENTRIES          (100)
#define QOS_PLY_MAP_TBL_MAX_ENTRIES        (100 + QOS_MAX_PCP_PLY_MAP_ENTRIES)
#define QOS_Q_TEMP_TBL_MAX_ENTRIES         (10)
#define QOS_VLAN_MAP_TBL_MAX_ENTRIES       (64)
/* Each queue supports 6 WRED Profiles, and each port has VLAN_DEV_MAX_NUM_COSQ
 * numuber of queues */
#define QOS_RD_TBL_MAX_ENTRIES             (SYS_DEF_NUM_PHYSICAL_INTERFACES * \
                                            VLAN_DEV_MAX_NUM_COSQ * \
                                            (QOS_RD_CONFG_DP_MAX_VAL + 1))

#define QOS_SCHED_TEMP_TBL_MAX_ENTRIES     (100)
#define QOS_SHAPE_TEMP_TBL_MAX_ENTRIES     (100)
#ifdef PB_WANTED
#define QOS_Q_MAP_TBL_MAX_ENTRIES          (1500 + QOS_MAX_PCP_QMAP_ENTRIES)
#define QOS_Q_TBL_MAX_ENTRIES              (1500)
#define QOS_SCHED_TBL_MAX_ENTRIES          (1500)
#define QOS_HIERARCHY_TBL_MAX_ENTRIES      (1500)
#else
#define QOS_Q_MAP_TBL_MAX_ENTRIES          (100 + QOS_MAX_PCP_QMAP_ENTRIES)
#define QOS_Q_TBL_MAX_ENTRIES              (100)
#define QOS_SCHED_TBL_MAX_ENTRIES          (100)
#define QOS_HIERARCHY_TBL_MAX_ENTRIES      (100)
#endif

#define QOS_MAX_NUM_OF_CLASSES             (100 + QOS_MAX_PCP_CLASSES)
#define QOS_MAX_PCP_PRI_MAP_ENTRIES        (2112)
#define QOS_MAX_PCP_CLS_MAP_ENTRIES        (2112)
#define QOS_MAX_PCP_PLY_MAP_ENTRIES        (2112)
#define QOS_MAX_PCP_QMAP_ENTRIES           (2112)
#define QOS_MAX_PCP_CLASSES                (2112)

/* Hierarchical scheduling is supported only on Trident-56840/Triden2-56850/
 * Triumph3-56640 chipsets currently */
#if defined (BCM56840_WANTED) || defined (BCM56850_WANTED) || defined (BCM56640)
#define QOS_SCHED_HL_DEFAULT               1
#else
#define QOS_SCHED_HL_DEFAULT               0
#endif

#if defined (BCM56840_WANTED)

#define QOS_MAX_REGEN_INNER_PRIORITY_VAL    (15)
#define QOS_QUEUE_ENTRY_MAX                12
#define QOS_QUEUE_MAX_NUM_UCOSQ            8
#define QOS_QUEUE_MAX_NUM_MCOSQ            4
#define QOS_QUEUE_MAX_NUM_SUBQ             0
#define QOS_SCHED_ALGO_DEFAULT             1
#define QOS_DEF_HL_SCHED_CONFIG_SUPPORT    0  
#define QOS_HL_DEF_NUM_LEVELS              3 /*No of Hierarchical Scheduler Levels(Default)*/
#define QOS_HL_DEF_NUM_S3_SCHEDULERS       2 /*No of Level3 Schedulers in Hierarchical Scheduling(Default)*/
#define QOS_HL_DEF_NUM_S2_SCHEDULERS       1 /*No of Level2 Schedulers in Hierarchical Scheduling(Default)*/
#define QOS_HL_DEF_NUM_S1_SCHEDULERS       1 /*No of Level1 Schedulers in Hierarchical Scheduling(Default)*/

#elif defined (BCM56850_WANTED) || defined (BCM56640)

#define QOS_MAX_REGEN_INNER_PRIORITY_VAL    (15)
#define QOS_QUEUE_ENTRY_MAX               16
#define QOS_QUEUE_MAX_NUM_UCOSQ            8
#define QOS_QUEUE_MAX_NUM_MCOSQ            8
#define QOS_QUEUE_MAX_NUM_SUBQ             8
/* QOS_DEF_HL_SCHED_CONFIG_SUPPORT Indicates whether Hierarchical Scheduling 
 * needs to be setup by default,The following macros take effect only this 
 * is enabled (1) */
#define QOS_DEF_HL_SCHED_CONFIG_SUPPORT    1  
#define QOS_HL_DEF_NUM_LEVELS              3 /*No of Hierarchical Scheduler Levels(Default)*/
#define QOS_HL_DEF_NUM_S3_SCHEDULERS       3 /*No of Level3 Schedulers in Hierarchical Scheduling(Default)*/
#define QOS_HL_DEF_NUM_S2_SCHEDULERS       1 /*No of Level2 Schedulers in Hierarchical Scheduling(Default)*/
#define QOS_HL_DEF_NUM_S1_SCHEDULERS       1 /*No of Level1 Schedulers in Hierarchical Scheduling(Default)*/

/* Configuring BCM_COSQ_ROUND_ROBIN as the default scheduling Scheme for
 *   BCM56850 */
#if defined (BCM56850_WANTED)
#define QOS_SCHED_ALGO_DEFAULT             2
#else
#define QOS_SCHED_ALGO_DEFAULT             1
#endif

#else

#define QOS_MAX_REGEN_INNER_PRIORITY_VAL    (7)
#define QOS_QUEUE_MAX_NUM_UCOSQ            8
#define QOS_QUEUE_ENTRY_MAX                8
#define QOS_QUEUE_MAX_NUM_MCOSQ            0
#define QOS_SCHED_ALGO_DEFAULT             1
#if defined (BCM56450)
#define QOS_DEF_HL_SCHED_CONFIG_SUPPORT    1
#define QOS_QUEUE_MAX_NUM_SUBQ             8
#else
#define QOS_DEF_HL_SCHED_CONFIG_SUPPORT    0  
#define QOS_QUEUE_MAX_NUM_SUBQ             0
#endif
#define QOS_SCHED_ALGO_DEFAULT             1
#define QOS_HL_DEF_NUM_LEVELS              3 /*No of Hierarchical Scheduler Levels(Default)*/
#define QOS_HL_DEF_NUM_S3_SCHEDULERS       2 /*No of Level3 Schedulers in Hierarchical Scheduling(Default)*/
#define QOS_HL_DEF_NUM_S2_SCHEDULERS       1 /*No of Level2 Schedulers in Hierarchical Scheduling(Default)*/
#define QOS_HL_DEF_NUM_S1_SCHEDULERS       1 /*No of Level1 Schedulers in Hierarchical Scheduling(Default)*/
#endif


/*QoS Granularity Value */
#define QOS_RATE_UNIT                      (QOS_RATE_UNIT_KBPS)
#define QOS_RATE_GRANULARITY               (64)

/* Default Values for QoS Table Entries */
#define QOS_CLS_DEFAULT_PRE_COLOR          (QOS_CLS_COLOR_NONE) 
#define QOS_PLY_DEFAULT_PHB_TYPE           (QOS_PLY_PHB_TYPE_NONE)
#define QOS_PLY_DEFAULT_PHB_VAL             0
#define QOS_Q_TEMP_SIZE_DEFAULT             50000
#define QOS_Q_TEMP_DROP_TYPE_DEFAULT        QOS_Q_TEMP_DROP_TYPE_TAIL
#define QOS_Q_TEMP_DROP_ALGO_DEFAULT        QOS_Q_TEMP_DROP_ALGO_DISABLE
#define QOS_Q_TEMP_DROP_ALGO_DEFAULT        QOS_Q_TEMP_DROP_ALGO_DISABLE
#define QOS_Q_TABLE_WEIGHT_DEFAULT          1
#define QOS_Q_TABLE_PRIORITY_DEFAULT        1

#define QOS_RD_CFG_MIN_AVG_TH_DEFAULT       10000 
#define QOS_RD_CFG_MAX_AVG_TH_DEFAULT       50000
#define QOS_RD_CFG_MAX_PKT_SIZE_DEFAULT      1000
#define QOS_RD_CFG_MAX_PROB_DEFAULT           100
#define QOS_RD_CFG_EXP_WEIGHT_DEFAULT           0
#define QOS_RD_CFG_GAIN_DEFAULT                 0
#define QOS_RD_CFG_DROP_THRESH_TYPE_DEFAULT     QOS_RD_CONFIG_DROP_THRESH_TYPE_PKTS
#define QOS_RD_CFG_ECN_THRESH_DEFAULT           0
#define QOS_RD_CFG_ACTION_FLAG_DEFAULT          0

#define QOS_SHAPE_TEMP_CIR_DEFAULT          10000
#define QOS_SHAPE_TEMP_CBS_DEFAULT          10000
#define QOS_SHAPE_TEMP_EIR_DEFAULT          10000
#define QOS_SHAPE_TEMP_EBS_DEFAULT          10000

#define QOS_SCHED_Q_COUNT_DEFAULT               4
#define QOS_QMAP_DEFAULT_QMAPTYPE           QOS_QMAP_TYPE_CLASS

/* PFC Maximum Profiles */
#define PFC_MAX_PROFILES                    256

#define SYS_DEF_MAX_SISP_IFACES          0 

/* PBB Specific */
#ifdef PBB_WANTED
#define SYS_DEF_MAX_ILAN_IFACES                   60
#define SYS_DEF_MAX_INTERNAL_IFACES               60
#define SYS_DEF_MAX_VIP_IFACES                    100
#define PBB_MAX_NUM_OF_ISID                       100
#define PBB_MAX_NUM_OF_ISID_PER_CONTEXT           50
#define PBB_MAX_NUM_OF_PORT_PER_ISID_PER_CONTEXT  128
#define PBB_MAX_NUM_OF_PORT_PER_ISID              50
/* Num of Service Instances required for I-Compoents 
 *  * Number of ISIDs for B bridges without I-Components, should be
 *   * reduced from this macro "PBB_MAX_NUM_OF_ICOMP_ISID". */
#define   PBB_MAX_NUM_OF_ICOMP_ISID       PBB_MAX_NUM_OF_ISID

/* Num of Service Instances required for B-Compoents 
 *  * Number of ISIDs for I bridges without B-Component, should be
 *   * reduced from this macro "PBB_MAX_NUM_OF_BCOMP_ISID */
#define   PBB_MAX_NUM_OF_BCOMP_ISID       PBB_MAX_NUM_OF_ISID
#define   PBB_MAX_NUM_OF_VIP              (PBB_MAX_NUM_OF_ICOMP_ISID)

#else                                             
#define SYS_DEF_MAX_ILAN_IFACES                   0
#define SYS_DEF_MAX_INTERNAL_IFACES               0
#define SYS_DEF_MAX_VIP_IFACES                    0
#endif

#ifdef IKE_WANTED
#define SYS_DEF_MAX_NUM_OF_VPNC                1 
#else
#define SYS_DEF_MAX_NUM_OF_VPNC                0 
#endif
/*end of PBB*/

#ifdef TLM_WANTED
#define SYS_DEF_MAX_TELINK_INTERFACES             300
#else
#define SYS_DEF_MAX_TELINK_INTERFACES             0
#endif

#define ISS_BCM_VP_VLAN_MIN_HW_ID 1140850689
#define ISS_BCM_VP_VLAN_MAX_HW_ID 1140867071


#ifdef VXLAN_WANTED
#define ISS_BCM_VP_VXLAN_MIN_HW_ID          2147485689
#define ISS_BCM_VP_VXLAN_MAX_HW_ID          2147500031
#define ISS_BCM_VP_VXLAN_MAX_NUM_OF_VP      \
    (ISS_BCM_VP_VXLAN_MAX_HW_ID - ISS_BCM_VP_VXLAN_MIN_HW_ID)
#define ISS_BCM_VPN_VXLAN_DUMMY_HW_ID       28673 /*First VPN id is used for all VXLAN n/w ports */
#define ISS_BCM_VPN_VXLAN_MIN_HW_ID         28674
#define ISS_BCM_VPN_VXLAN_MAX_HW_ID         36864
#define ISS_BCM_VPN_VXLAN_MAX_NUM_OF_VPN    \
    (ISS_BCM_VPN_VXLAN_MAX_HW_ID -  ISS_BCM_VPN_VXLAN_MIN_HW_ID )
#define ISS_BCM_MIN_VNI_VALUE               4096
#define ISS_BCM_MIN_MULTICAST_VALUE         201330688 /* 4096 => 1000, 
                                                        so for VXLAN, C001000 (starts from 4096) */
#endif

#define VXLAN_GET_VNI_VPN(u4VniNumber) \
    (u4VniNumber + (ISS_BCM_VPN_VXLAN_MIN_HW_ID - ISS_BCM_MIN_VNI_VALUE))

#define VXLAN_GET_VNI_MCAST_GROUP(u4VniNumber) \
    (u4VniNumber + (ISS_BCM_MIN_MULTICAST_VALUE - ISS_BCM_MIN_VNI_VALUE))

#define VXLAN_GET_VNI_FROM_VPN(u4VpnId) \
    (ISS_BCM_MIN_VNI_VALUE +(u4VpnId - ISS_BCM_VPN_VXLAN_MIN_HW_ID))

#define VXLAN_GET_VNI_FROM_VPN_MULTICAST(u4Multicast) \
    (ISS_BCM_MIN_VNI_VALUE +(u4Multicast - ISS_BCM_MIN_MULTICAST_VALUE))


/*************************************************************************
                     Base HW ID for the VPN and virtual ports
*************************************************************************/
#ifndef SDK631_WANTED

#define ISS_BCM_VPN_VPWS_BASE_HW_ID         8192

#define ISS_BCM_VPN_VPLS_BASE_HW_ID         12288

#else

#define ISS_BCM_VPN_VPWS_BASE_HW_ID         12288

#define ISS_BCM_VPN_VPLS_BASE_HW_ID         28672

#endif /* SDK631_WANTED */

#define ISS_BCM_VPN_VP_BASE_HW_ID           402653184


/*************************************************************************
                      Attachment Circuit Without IfIndex           
*************************************************************************/
#define ISS_BCM_VPN_MAX_NUM_OF_AC_WOIDX  MAX_L2VPN_ENET_ENTRIES

#define ISS_BCM_VPN_AC_VP_HW_MIN_ID     ISS_BCM_VPN_VP_BASE_HW_ID

#define ISS_BCM_VPN_AC_VP_HW_MAX_ID \
        (ISS_BCM_VPN_AC_VP_HW_MIN_ID + ISS_BCM_VPN_MAX_NUM_OF_AC_WOIDX)


/*************************************************************************
                      Pseudowire Without IfIndex (with PW Index)          
*************************************************************************/

/* Range of Pseudowire created without entry in ifTable. */ 
#define ISS_BCM_VPN_PW_MIN_INDEX          ISS_MPLS_VPN_PW_MIN_INDEX

#define ISS_BCM_VPN_PW_MAX_INDEX          ISS_MPLS_VPN_PW_MAX_INDEX

#define ISS_BCM_VPN_MAX_NUM_OF_PW_WOIDX  \
    (ISS_BCM_VPN_PW_MAX_INDEX - ISS_BCM_VPN_PW_MIN_INDEX)


/* Range of Pseudowire identifier in chipset. */ 
#define ISS_BCM_VPN_PW_VP_WOIDX_HW_MIN_ID  \
        (ISS_BCM_VPN_AC_VP_HW_MAX_ID + 1)

#define ISS_BCM_VPN_PW_VP_WOIDX_HW_MAX_ID  \
        (ISS_BCM_VPN_PW_VP_WOIDX_HW_MIN_ID + ISS_BCM_VPN_MAX_NUM_OF_PW_WOIDX) 

/* Delta between the HW pseudowire id and the control plane 
 * pseudowire id which is used to adjust the pseudowire Index to get the 
 * hardware id for pseudowire.
 * */
#define ISS_BCM_VPN_PW_VP_WOIDX_DIFF_WITH_HWID \
        (ISS_BCM_VPN_PW_VP_WOIDX_HW_MIN_ID -  ISS_BCM_VPN_PW_MIN_INDEX)


/*************************************************************************
                      Pseudowire With IfIndex           
*************************************************************************/

/* Range of pseudowire with entry created in ifTable with ifIndex. */ 
#define ISS_BCM_VPN_MAX_NUM_OF_PW         \
        (CFA_MAX_PSW_IF_INDEX - CFA_MIN_PSW_IF_INDEX)

/* Range of Pseudowire in chipset. */ 
#define ISS_BCM_VPN_PW_VP_WIDX_HW_MIN_ID  \
        (ISS_BCM_VPN_PW_VP_WOIDX_HW_MAX_ID + 1)

#define ISS_BCM_VPN_PW_VP_WIDX_HW_MAX_ID  \
        (ISS_BCM_VPN_PW_VP_WIDX_HW_MIN_ID + ISS_BCM_VPN_MAX_NUM_OF_PW) 

/* Delta between the HW pseudowire Id and the control plane 
 * pseudowire id used to adjust the pseudowire ifIndex to get the 
 * hardware id for pseudowire circuit.
 * */
#define ISS_BCM_VPN_PW_VP_WIDX_DIFF_WITH_HWID \
        (ISS_BCM_VPN_PW_VP_WIDX_HW_MIN_ID - CFA_MIN_PSW_IF_INDEX)



/*************************************************************************
                      VPWS hardware id map           
*************************************************************************/

/* Range of VPWS VFI Ids supported in the control plane.*/
#define ISS_BCM_VPN_VPWS_MAX_NUM_OF_VFI           \
        ((MAX_VLAN_VPWS_VFI_ID  - MIN_VLAN_VPWS_VFI_ID))


/* Range of VPWS VFI Ids supported in the chipset.*/
#define ISS_BCM_VPN_VPWS_HW_MIN_ID           ISS_BCM_VPN_VPWS_BASE_HW_ID
#define ISS_BCM_VPN_VPWS_HW_MAX_ID           (ISS_BCM_VPN_VPWS_HW_MIN_ID + \
                                             ISS_BCM_VPN_VPWS_MAX_NUM_OF_VFI) 


/* Delta between the HW VPWS VFI id and the control plane VPWS VFI id 
 * which is used to adjust the VPWS VFI id to get the hardware id for 
 * VPWS VFI.
 * */
#define ISS_BCM_VPN_VPWS_VFI_DIFF_WITH_HWID  \
        (ISS_BCM_VPN_VPWS_HW_MIN_ID - MIN_VLAN_VPWS_VFI_ID - 1)



/*************************************************************************
                      VPLS hardware id map           
*************************************************************************/

/* Range of VPLS VFI Ids supported in the control plane.*/
#define ISS_BCM_VPN_VPLS_MAX_NUM_OF_VFI           \
        (MAX_VLAN_VPLS_VFI_ID - MIN_VLAN_VPLS_VFI_ID)

/* Range of VPLS VFI Ids supported in the chipset.*/
#define ISS_BCM_VPN_VPLS_HW_MIN_ID           ISS_BCM_VPN_VPLS_BASE_HW_ID
#define ISS_BCM_VPN_VPLS_HW_MAX_ID           (ISS_BCM_VPN_VPLS_HW_MIN_ID + \
                                             ISS_BCM_VPN_VPLS_MAX_NUM_OF_VFI) 

/* Delta between the HW VPLS VFI id and the control plane VPLS VFI id 
 * which is used to adjust the VPLS VFI id to get the hardware id for 
 * VPLS VFI.
 * */
#define ISS_BCM_VPN_VPLS_VFI_DIFF_WITH_HWID  \
        (ISS_BCM_VPN_VPLS_HW_MIN_ID - MIN_VLAN_VPLS_VFI_ID)

/*============================================================================
                         Common Macros for VPWS/VPLS
=============================================================================*/

/* Following are the combinations that are valid for using this MACRO. 
 *
 * To get VP for a PW when IfIndex is created for a PW,
 *     - u4IfIndex should be non-zero
 *     - u4Index should be zero
 *     - u4Port should be zero
 *     - u2Vlan should be zero
 *
 * To get VP for a PW when IfIndex is not created for a PW,
 *     - u4IfIndex should be zero
 *     - u4Index should be non-zero
 *     - u4Port should be zero
 *     - u2Vlan should be zero
 *
 * To get VP for an AC when IfIndex is not-created for a AC,
 *     - u4IfIndex should be zero
 *     - u4Index should be zero
 *     - u4Port should be non-zero
 *     - u2Vlan may or may not be zero
 *
 * */

#define ISS_BCM_GET_VP_FROM_INDEX(u4IfIndex, u4Index, u4AcPort, u2Vlan) \
{ \
   if ((u4IfIndex >= CFA_MIN_PSW_IF_INDEX) && \
       (u4IfIndex <= CFA_MAX_PSW_IF_INDEX)) \
   { \
       u4IfIndex += ISS_BCM_VPN_PW_VP_WIDX_DIFF_WITH_HWID;\
   } \
   else if ((u4Index >= ISS_MPLS_VPN_PW_MIN_INDEX) && \
            (u4Index <= ISS_MPLS_VPN_PW_MAX_INDEX)) \
   { \
       u4Index += ISS_BCM_VPN_PW_VP_WOIDX_DIFF_WITH_HWID;\
   }\
   else if (u4AcPort != 0)\
   {\
       tMplsAcVpMapInfo   *pMplsAcVpMapInfo = NULL; \
    pMplsAcVpMapInfo = NpUtilMplsGetAcVpMapPortVlanInfo (u4AcPort, u2Vlan); \
    if (pMplsAcVpMapInfo != NULL) \
    { \
        u4AcPort = pMplsAcVpMapInfo->u4VirtualPort; \
    } \
   }\
   else\
   {\
       /* Since it does not fall under virtual port range, 
        * the incoming information will be used further.*/\
   }\
}


/* Following are the combinations that are valid for using this MACRO. 
 *
 * To get PW IfIndex for a PW VP,
 *     - u4IfIndex should contain a VP Id as incoming parameter. 
 *       PW IfIndex will be returned as output paramter in u4IfIndex and 
 *       all other below variables will be zero.
 *         - u4Index, u4Port and u2Vlan.
 *
 * To get PW Index for a PW VP,
 *     - u4IfIndex should contain a VP Id as incoming parameter. 
 *       PW Index will be returned as output paramter in u4Index and 
 *       all other below variables will be zero.
 *         - u4Port and u2Vlan.
 *
 * To get AC Port and VLAN for a AC VP,
 *     - u4IfIndex should contain a VP Id as incoming parameter. 
 *       Port and VLAN will be returned as output paramter in u4Port and 
 *       u2Vlan respectively. All other below variables will be zero.
 *         - u4Index.
 *
 */

#define ISS_BCM_GET_INDEX_FROM_VP(u4IfIndex, u4Index, u4AcPort, u2Vlan) \
{ \
   u4AcPort = 0; \
   u2Vlan = 0; \
\
   if ((u4IfIndex >= ISS_BCM_VPN_PW_VP_WIDX_HW_MIN_ID) && \
       (u4IfIndex <= ISS_BCM_VPN_PW_VP_WIDX_HW_MAX_ID)) \
   { \
       u4Index = u4IfIndex - ISS_BCM_VPN_PW_VP_WIDX_DIFF_WITH_HWID;\
   } \
   else if ((u4IfIndex >= ISS_BCM_VPN_PW_VP_WOIDX_HW_MIN_ID) && \
            (u4IfIndex <= ISS_BCM_VPN_PW_VP_WOIDX_HW_MAX_ID)) \
   { \
       u4Index = u4IfIndex - ISS_BCM_VPN_PW_VP_WOIDX_DIFF_WITH_HWID;\
   } \
   else \
   { \
       tMplsAcVpMapInfo   *pMplsAcVpMapInfo = NULL; \
    pMplsAcVpMapInfo = NpUtilMplsGetAcVpMapVirPortInfo (u4IfIndex); \
    if (pMplsAcVpMapInfo != NULL) \
    { \
        u4AcPort = pMplsAcVpMapInfo->u4Port; \
        u2Vlan = pMplsAcVpMapInfo->u2VlanId; \
    } \
   } \
}


/* These macros will be removed once VFI to VPN association support 
 * is available in MPLS module in control plane. */
#define ISS_BCM_ADJUST_VPWS_VPN(u4VpnId) (u4VpnId += MIN_VLAN_VPWS_VFI_ID)
#define ISS_BCM_ADJUST_VPLS_VPN(u4VpnId) (u4VpnId += MIN_VLAN_VPLS_VFI_ID)

#define ISS_BCM_INVALID_VPN_ID 0

#define ISS_BCM_GET_HW_VPN_ID_FROM_VPN_ID(u4VpnId) \
{ \
    if ((u4VpnId >= MIN_VLAN_VPWS_VFI_ID) && \
        (u4VpnId <= MAX_VLAN_VPWS_VFI_ID)) \
 { \
        u4VpnId += ISS_BCM_VPN_VPWS_VFI_DIFF_WITH_HWID; \
    } \
    else if ((u4VpnId >= MIN_VLAN_VPLS_VFI_ID) && \
             (u4VpnId <= MAX_VLAN_VPLS_VFI_ID)) \
    { \
        u4VpnId += ISS_BCM_VPN_VPLS_VFI_DIFF_WITH_HWID; \
    }\
    else \
    { \
       /* Since it does not fall under VPN ID range, 
        * the incoming information will be used further.*/ \
 u4VpnId = ISS_BCM_INVALID_VPN_ID; \
    } \
}


#define ISS_BCM_GET_HW_VPN_FROM_VFI_VLAN   ISS_BCM_GET_HW_VPN_ID_FROM_VPN_ID




#define ISS_BCM_GET_VPN_ID_FROM_HW_VPN_ID(u4VpnId) \
{ \
    if ((u4VpnId >= ISS_BCM_VPN_VPWS_HW_MIN_ID) && \
        (u4VpnId <= ISS_BCM_VPN_VPWS_HW_MAX_ID)) \
    { \
        u4VpnId -= ISS_BCM_VPN_VPWS_VFI_DIFF_WITH_HWID; \
    } \
    else if ((u4VpnId >= ISS_BCM_VPN_VPLS_HW_MIN_ID) && \
             (u4VpnId <= ISS_BCM_VPN_VPLS_HW_MAX_ID)) \
    { \
        u4VpnId -= ISS_BCM_VPN_VPLS_VFI_DIFF_WITH_HWID; \
    }\
    else \
    { \
       /* Since it does not fall under VPN HW ID range  
        * the incoming information will be used further.*/\
    } \
}


#define ISS_BCM_GET_VFI_VLAN_FROM_HW_VPN   ISS_BCM_GET_VPN_ID_FROM_HW_VPN_ID

#define SYS_DEF_MAX_IFACES    SYS_DEF_MAX_PORTS_PER_CONTEXT +\
     SYS_DEF_MAX_L2_AC_IFACES + \
     SYS_DEF_MAX_L2_PSW_IFACES

/* VPLS Multicast group related MACROs. */

#define ISS_BCM_MCAST_MIN_HW_ID            50331650

#define ISS_BCM_VPN_MCAST_MIN_HW_ID        (ISS_BCM_MCAST_MIN_HW_ID + \
                                            IP_DEF_IGMP_MAX_MCAST_GROUPS)

#define ISS_BCM_VPN_MCAST_MAX_HW_ID        (ISS_BCM_VPN_MCAST_MIN_HW_ID + \
                                            ISS_BCM_VPN_VPLS_MAX_NUM_OF_VFI)

#define ISS_BCM_VPN_MCAST_DIFF_WITH_HWID   (ISS_BCM_VPN_MCAST_MIN_HW_ID - \
                                            MIN_VLAN_VPLS_VFI_ID)

#define ISS_BCM_GET_VPN_MCAST_GRP_HW_ID(u4VpnId)  \
{ \
    if ((u4VpnId >= MIN_VLAN_VPLS_VFI_ID) && \
             (u4VpnId <= MAX_VLAN_VPLS_VFI_ID)) \
    { \
       u4VpnId += ISS_BCM_VPN_MCAST_DIFF_WITH_HWID; \
    } \
}

#define ECFM_HW_MA_HANDLER_SIZE                (4 + 1)/*1 byte for NULL character*/
#define ECFM_HW_MEP_HANDLER_SIZE               (8 + 2)/*2 bytes for NULL character*/
#define ECFM_HW_HANDLER_SIZE                   (ECFM_HW_MEP_HANDLER_SIZE / 2)

/*
 * Hold the size of platform specific structure,
 * Its value could be changed upon platform.
 * Bcmx used tVlanHwStatsCntrId structure and size is 8 bytes for 
 * ingress.
 */
#define VLAN_HW_STATS_ENTRY_LEN         8
#define   CFA_MAX_GIGA_ENET_MTU   9216
#define   CFA_MAX_L3_INTF_ENET_MTU      9216



#if defined (WLC_WANTED) || defined (WTP_WANTED)
#define MAX_NUM_OF_AP                   4097
#define NUM_OF_AP_SUPPORTED             4
#define MAX_NUM_OF_RADIO_PER_AP         31
#define MAX_NUM_OF_RADIOS  (NUM_OF_AP_SUPPORTED *\
      MAX_NUM_OF_RADIO_PER_AP)
#define MAX_NUM_OF_SSID_PER_WLC         50
#define MAX_NUM_OF_BSSID_PER_RADIO      16
#define MAX_NUM_OF_STA_PER_AP           32
#define MAX_NUM_OF_STA_PER_WLC          (NUM_OF_AP_SUPPORTED *\
                                         MAX_NUM_OF_STA_PER_AP)
#endif

#ifdef WLC_WANTED
#define SYS_DEF_MAX_WSS_IFACES  (MAX_NUM_OF_RADIOS +\
      MAX_NUM_OF_SSID_PER_WLC +\
                     (MAX_NUM_OF_BSSID_PER_RADIO *\
      MAX_NUM_OF_RADIOS)) 
#elif WTP_WANTED
#define SYS_DEF_MAX_WSS_IFACES  (MAX_NUM_OF_BSSID_PER_RADIO *\
                      SYS_DEF_MAX_RADIO_INTERFACES) 
#else
#define SYS_DEF_MAX_WSS_IFACES          0
#endif

#define ISS_TGT_TELNET_SERVER_PORT     6023
#define ISS_TGT_TELNET_SERVER_PORT1    6024

/* Number of S-Channel interfaces. Referring S-Channel interfaces
 * as SBP interfaces */

#ifdef EVB_WANTED
/* No of UAP ports (60) * No of SBP ports per UAP (16) */
#define SYS_DEF_MAX_SBP_IFACES          1020
#else
#define SYS_DEF_MAX_SBP_IFACES          0
#endif

/* Maximum FP Groups that can be created */
#if defined (BCM56800_WANTED) || defined (BCM56314_WANTED) || defined (BCM56302_WANTED)
#define CFA_MAX_NP_FP_GROUPS      8
#else
#define CFA_MAX_NP_FP_GROUPS      12
#endif
#endif

