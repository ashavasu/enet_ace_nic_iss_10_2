/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: arp.h,v 1.51 2018/02/19 09:56:23 siva Exp $
 *
 * Description:This file contains the exported definitions and
 *             macros of ARP 
 *
 *******************************************************************/
#ifndef   _ARP_H
#define   _ARP_H

#include "ip.h"

#define   ARP_SUCCESS                0
#define   ARP_FAILURE               -1

#define   ARP_OTHER                  1
#define   ARP_INVALID                2
#define   ARP_DYNAMIC                3
#define   ARP_STATIC                 4
#define   ARP_PENDING                5
#define   ARP_AGEOUT                 7
#define   ARP_STATIC_NOT_IN_SERVICE  8
#define   ARP_EVPN                   6
#define   ARP_PROXY_EVPN             6
#define   ARP_ADDITION               1
#define   ARP_DELETION               2

/*used in arp and ipmgmt*/
#define ARP_DEF_REQ_RETRIES          3

/* Time out  for sending on ARP request for one second to a destination */
#define   ARP_REQUEST_RETRY_TIMEOUT    1

#define   ARP_DEFAULT_CONTEXT          VCM_DEFAULT_CONTEXT
#define   ARP_INVALID_CXT_ID           0xFFFFFFFF

/* Used to set all 1's in a byte  */
#define ARP_UINT1_ALL_ONE              0xFF

#define ARP_TWO                        2

/* ARP opcode */
#define   ARP_REQUEST                1
#define   ARP_RESPONSE               2

#define   FROM_CFA_TO_ARP           0x0002
#define   ARP_APP_IF_MSG            0x0004 
#define   ARP_IF_DELETED            0x0008
#define   ARP_IF_OPER_CHG           0x0010
#define   IP_PKT_FOR_RESOLVE        0x0020
#define   ARP_STATIC_RT_CHANGE      0x0040
#define   ARP_CONTEXT_CREATE        0x0100
#define   ARP_CONTEXT_DELETE        0x0200
#define   CFA_GRAT_ARP_REQ          0x0400
/* Event to handle L2 interface deletion */
#define   ARP_L2_IF_DELETED         0x0800
/* To be used and implemented in future in rtm*/
#define   NEXTHOP_ENTRY               1

#define   ARP_L2_MOVEMENT_EVENT          0x00000080
#define   ARP_L3_DATA_PKT_EVENT          0x00000100

#define   ARP_ENET_AUTO_ENCAP        1
#define   ARP_ENET_V2_ENCAP          8

#define   ARP_ENET_TYPE              1
#define   ARP_IFACE_TYPE(u1IfaceType)   arp_map_cru_hwtype ((u1IfaceType))

#define   ARP_PROXY_ENABLE             1
#define   ARP_PROXY_DISABLE            0

#define ARP_PROT_LOCK ArpProtocolLock
#define ARP_PROT_UNLOCK ArpProtocolUnLock 

/****** common to ARP and RARP - begin ************/
#define   ARP_STANDARD_HDR_SIZE      8
#define   ARP_MAX_PROTO_ADDR_LEN     4
#define  ARP_PACKET_LEN (ARP_STANDARD_HDR_SIZE +         \
                        (CFA_ENET_ADDR_LEN * 2) +     \
                        (ARP_MAX_PROTO_ADDR_LEN * 2))
/****** common to ARP and RARP - end ************/

 /* LinkLocalIp range 169.254.1.0 - 169.254.254.255*/
#define   IS_LINK_LOCAL_ADDR(u4Addr) \
          (u4Addr >= 0xa9fe0100 && u4Addr <= 0xa9fefeff)

 /* Maximum Arp Instances supported */
#define   MAX_ARP_INSTANCES      SYS_DEF_MAX_NUM_CONTEXTS

#define   ARP_MAX_HW_TYPES       10
 /*
  * Note: t_ARP_TIMER order of the fields should not be changed and no
  * alignment should be added because this structure is typecasted by
  * all the Timer related module. 
  */
extern UINT4 gu4MaxArpEntries;
#define   ARP_MAX_ARP_ENTRIES       gu4MaxArpEntries
typedef struct
{
    tTmrAppTimer   Timer_node;
    UINT1          u1Id;             /* To demultiplex between handlers */
    UINT1          u1AlignmentByte;
    UINT2          u2AlignmentByte;
    VOID           *pArg;            /* Pointer to the data structure if any */
} t_ARP_TIMER;

typedef struct
{
    INT4  i4Cache_timeout;  /* Life of a cache entry */
    INT4  i4Cache_pend_time;  /* Maximum waiting time allowed */
    INT4  i4Max_retries;  /* No of times that ARP req can be sent */
    INT4  i4Flush_status; /* global arp flush status */
} t_ARP_CONFIG;

/*
 * ARP statistics.
 */
typedef struct
{
    UINT4  u4Total_in_pkts;  /* Number of ARP packets received */
    UINT4  u4Bad_type;       /* Requests for unsupported hardware */
    UINT4  u4Bad_length;     /* Rejects due to bad length */
    UINT4  u4Bad_address;    /* Rejects due to bogus address */
    UINT4  u4Req_discards;   /* Rejected requests */
    UINT4  u4In_requests;    /* Number of requests received */
    UINT4  u4Replies;        /* Number of response packets received */
    UINT4  u4Out_requests;   /* ARP requests sent */
    UINT4  u4Out_drops;      /* No of packets dropped by ARP */
    UINT4  u4Out_Replies;    /* No of packets dropped by ARP */
} t_ARP_STAT;

/* The Arp Context contains the information of the ARP configurations 
 * in the given virtual router */

typedef struct
{
    t_ARP_CONFIG         Arp_config;
    t_ARP_STAT           Arp_stat;
    UINT4                u4PendingEntryCount;
    UINT4                u4ArpEntryCount;
    UINT4                u4ContextId;
    UINT4                u4ArpDbg;
} tArpCxt;

/*
 * The ARP table is a hash table of t_ARP_CACHE entries.
 * each of the entries contain information to map an ip address
 * to a hardware address.  There is a timer associated with each
 * entry. The timer is used to remove obsolete entries.
 * If a packet is waiting for address resolution it will be
 * queued into pPending buffer list.
 */

typedef struct
{
    tRBNodeEmbd     RbNode;  /* RbNode for the cache entry */
    t_ARP_TIMER      Timer; /* For ARP timeouts  */
    tCRU_BUF_CHAIN_HEADER *pPending;  /* Pending data list of messages */
    tArpCxt        *pArpCxt;  /* Pointer to the Arp Cxt information */
    UINT4           u4Ip_addr; /* Ip address */
    UINT4           u4Time;  /* Time at which this entry is learnt  */
    UINT4           u4LastUpdatedTime;
    UINT4           u4IfIndex;  /* IfIndex for this Arp entry */
    UINT4           u4PhyIfIndex;  /* Physical interface index corresponding to the 
                                    MAC address and VLAN of this ARP cache entry.*/
    INT4            i4Retries;  /* Number of retries for this entry */
    UINT2           u2Port;  /* Interface for this ARP*/
    INT2            i2Hardware; /* Type of this interface*/
    UINT1           u1EncapType;  /* Encap type SNAP or ETHv2 */
    UINT1           u1BitMapFlag;
    UINT1           u1RowStatus; 
    UINT1           u1HwStatus; /* Hardware status. Whether present in 
                                 * hardware or not */
    INT1            i1State; /* status of this entry */
    INT1            i1Hw_addr[CFA_ENET_ADDR_LEN];
    INT1            i1Hwalen;  /* Hardware Address Len */
    INT1            i1Palen;  /* Protocol addr len */
    INT1            i1Pad[3];
} t_ARP_CACHE;

typedef struct
{
    INT2      i2Hardware;        /* Hardware type                  */
    INT2      i2Protocol;        /* High level protocol used       */
    INT4 (*get_hw_address)    /* Function to obtain hw address  */
    PROTO ((UINT2 u2Port, INT1 * i1pAddr, INT1 * pHwalen, INT1 * pPalen));    
    /* Function to get hardware address */
    INT4 (*pWrite) (VOID);    /* function to write to lower layer   */
    INT1      i1State;            /* Is this entry free             */
    UINT1     u1AlignmentByte;    /* Byte for word Alignment        */
    UINT2     u2AlignmentByte;    /* Byte for word Alignment        */
} t_ARP_HW_TABLE;

typedef struct
{
    UINT4          u4IpAddr;
    UINT2          u2Port;  /* Interface for this ARP*/
    INT2           i2Hardware; /* Type of this interface*/
    INT1           i1Hw_addr[CFA_ENET_ADDR_LEN];/*HardWare Address */
    UINT1          u1EncapType;  /* Encap type SNAP or ETHv2 */
    INT1           i1Hwalen;  /* Hardware Address Len */
    INT1           i1State;   /* Arp State */
    UINT1          u1BitMapFlag;
    UINT1          u1RowStatus;   /* For Alignment */
    UINT1          u1Align;   /* For Alignment */
}tARP_DATA;

/*
 * Format of an ARP request or reply packet.
 * Packets are constructed in this and are copied to buffer at the end.
 */
typedef struct
{
    UINT4  u4Tproto_addr;                   /* Target protocol address field */
    UINT4  u4Sproto_addr;                   /* Sender Protocol address field */
    INT2   i2Hardware;                      /* Hardware type */
    INT2   i2Protocol;                      /* Protocol type */
    INT2   i2Opcode;                        /* ARP opcode (request/reply) */
    INT1   i1Hwalen;                        /* Hardware address length. */
    INT1   i1Palen;                         /* Length of protocol address */
    INT1   i1Shwaddr[CFA_ENET_ADDR_LEN];  /* Sender hardware address field */
    INT1   i1Thwaddr[CFA_ENET_ADDR_LEN];  /* Target hardware address field */
} t_ARP_PKT;

typedef struct
{
    tCRU_BUF_CHAIN_HEADER *pBuf;
    UINT4  u4MsgType;
    UINT4  u4EncapType; 
    UINT4  u4IpAddr;
    UINT4  u4NetMask;
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
    UINT4  u4PhyIfIndex;
    UINT2  u2Port;
    UINT2  u2Protocol; 
    UINT1  u1SrcModId;
    UINT1  u1BitMapFlag;
    UINT2  u2Align;
    UINT4  u4Status;
}tArpQMsg;

typedef struct 
{
 UINT2           u2Port;
        UINT2  VlanId;
        tMacAddr    pQMacAddr;
        INT1            i1Pad[2];
}tArpQFlushMac;
        

typedef struct _tArpEthernetEntry
{
    tRBNodeEmbd     RbNode;                       /* RbNode for ArpEnetTable */
    UINT1           au1HwAddr[CFA_ENET_ADDR_LEN]; /* hardware address */
    UINT2           u2Port;                       /* RBTree index */
    UINT1           u1HwAddrValidFlag;            /* Validation flag */  
    UINT1           au1AlignmentByte[3];          /* alignment byte */
} tArpEthernetEntry;

/* As Rarp uses Arp Timers, GlobalInformation in defined here */
typedef struct _ArpGblInfo
{
    UINT4               u4ArpDbg;
    /* Semaphore for protecting ARPCACHE Table */
    tOsixSemId          ArpDSem;
    /* Protocol Semaphore for  ARP*/
    tOsixSemId          ArpSem;
    tOsixTaskId         ArpTaskId;
    tOsixQId            ArpPktQId;
    tOsixQId            ArpNewPktQId;
    tOsixQId            ArpIpPktQId;
    tOsixQId            ArpRmPktQId;
#ifdef ICCH_WANTED
    tOsixQId            ArpIcchPktQId;
#endif
#if defined (LNXIP4_WANTED) && defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    tOsixQId            ArpVrfQId;
#endif
    tTimerListId        ArpTimerListId;
    tArpCxt            *apArpCxt [MAX_ARP_ENTRIES_LIMIT];
    tArpCxt            *pArpCurrCxt;
    tRBTree             ArpTable;
    tRBTree             ArpRedTable;
    t_ARP_HW_TABLE      Arp_hw_table[ARP_MAX_HW_TYPES];    /* Hardware table */
#ifndef ARP_ARRAY_TO_RBTREE_WANTED
    tArpEthernetEntry   aEnet[IPIF_MAX_LOGICAL_IFACES];    /* for ethernet */
#else
    tRBTree             ArpEnetTable;/* To have dynamic entries flexibility,
                                        this RBTree is added instead of aEent*/
#endif
}tArpGblInfo;

/* This structure is used in ARP call back structure tArpRegTbl */
typedef struct _tArpBasicInfo {
    UINT4       u4IpAddr;   /* IP address */
    tMacAddr    MacAddr;    /* Mac address for UPDATE_MAC */
    UINT1       u1Action;   /* Action: ARP_CB_ADD_ARP_ENTRY, 
                             * ARP_CB_REMOVE_ARP_ENTRY */
    UINT1       u1Reserved; /* Structure padding */
}tArpBasicInfo;

enum
{
   ARP_CB_ADD_ENTRY  = 1,
   ARP_CB_REMOVE_ENTRY,
   ARP_CB__INVALID    /* CAUTION: Ensure this element shall be the last
                       * element always */

};
enum
{
   ARP_INDICATION_REGISTER    = 1,
   ARP_INDICATION_DEREGISTER,
   ARP_INDICATION_INVALID /* CAUTION: Ensure this element shall be the last
                           * element always */

};

enum {
   ARP_WSS_PROTO_ID = 0,
   ARP_VXLAN_EVPN_PROTO_ID,
   ARP_MAX_REG_MODULES  /* CAUTION: Ensure this element shall be the last
                         * element always */
};

/***********  Providing registration mechanism from ARP *********/
typedef struct ArpRegTbl {
    VOID (*pArpBasicInfo) (tArpBasicInfo *pArpBasicInfo);
    /* Call back function that a module can register with ARP */
    UINT1                  u1ProtoId;
    /* The protocol Id as defined the in enumeration */
    UINT1                  au1Reserved[3];
    /* Structure padding */
}tArpRegTbl;


INT4
ArpRegDeRegProtocol PROTO ((UINT4 u4Type, tArpRegTbl *pArpRegTbl));


#define ARP_CACHE_SEM_ID        (gArpGblInfo.ArpDSem)
#define ARP_PROTOCOL_SEM_ID     (gArpGblInfo.ArpSem)
#define ARP_TASK_ID             (gArpGblInfo.ArpTaskId)
#define ARP_PKT_Q_ID            (gArpGblInfo.ArpPktQId)
#define ARP_IP_PKT_Q_ID         (gArpGblInfo.ArpIpPktQId)
#define ARP_NEW_PKT_Q_ID     (gArpGblInfo.ArpNewPktQId)
#define ARP_RM_PKT_Q_ID         (gArpGblInfo.ArpRmPktQId)
#define ARP_ICCH_PKT_Q_ID       (gArpGblInfo.ArpIcchPktQId)
#if defined (LNXIP4_WANTED) && defined (VRF_WANTED) && defined (LINUX_310_WANTED)
#define ARP_VRF_IP_PKT_Q_ID     (gArpGblInfo.ArpVrfQId)
#endif
#define ARP_TIMER_LIST_ID       (gArpGblInfo.ArpTimerListId)
#define ARP_POOL_ID             (ARPMemPoolIds[MAX_ARP_ENTRIES_SIZING_ID])
#define ARP_Q_MSG_POOL_ID       (ARPMemPoolIds[MAX_ARP_PKT_QUE_SIZE_SIZING_ID])
#define ARP_CXT_POOL_ID         (ARPMemPoolIds[MAX_ARP_CONTEXTS_SIZING_ID])
#define ARP_RM_MSG_POOL_ID      (ARPMemPoolIds[MAX_ARP_RM_QUE_SIZE_SIZING_ID])
#define ARP_DYN_MSG_POOL_ID     (ARPMemPoolIds[MAX_ARP_DYN_MSG_SIZE_SIZING_ID])
#define ARP_ENET_POOL_ID        (ARPMemPoolIds[MAX_ARP_ENET_ENTRIES_SIZING_ID])
#define ARP_ICCH_MSG_POOL_ID    (ARPMemPoolIds[MAX_ARP_ICCH_QUE_SIZE_SIZING_ID])
#define ARP_VLAN_MAC_POOL_ID    (ARPMemPoolIds[MAX_ARP_VLAN_QUE_SIZE_SIZING_ID])

/* Function prototypes */
VOID ArpTaskMain PROTO ((INT1 *));

INT4 ArpTaskInit PROTO ((VOID));

VOID ArpDeInit PROTO ((VOID));


/* APIs used by ipmgmt, issweb, msr while accessing nmh routines */
INT4 ArpProtocolLock PROTO ((VOID));
INT4 ArpProtocolUnLock PROTO ((VOID));

/* APIs used by dhcprelay, dhcpsrv, bootp, rarp, ipmgmt */
INT1 arp_add    PROTO ((tARP_DATA));

/* APIs used by ipmgmt */
INT2 arp_map_cru_hwtype PROTO ((INT2 i2CruType));
INT4 ArpModifyWithIndex PROTO ((UINT4, UINT4, INT1));
INT4 ArpLookupWithIndex PROTO ((UINT4, UINT4, t_ARP_CACHE *));
INT4 ArpEvpnLookupWithIpAddr PROTO ((UINT4, t_ARP_CACHE *));
INT4 ArpFlushWithMac PROTO ((tArpQFlushMac *pMacAddr));
INT1 ArpGetPendingCacheEntryCountInCxt PROTO ((UINT4, UINT4 *));
INT1 ArpGetCacheEntryCountInCxt PROTO ((UINT4, UINT4 *));
tArpCxt  * ArpGetGlblCxt(UINT4 u4ContextId);
#if defined (LNXIP4_WANTED)
INT4 ArpUpdateKernelEntry PROTO ((INT1, UINT1 *, INT4 , UINT4 , UINT1 *));
INT4 ArpUpdateKernelEntrywithCOM PROTO ((INT1, UINT1 *, INT4 , UINT4 , UINT1 *));
#endif
INT4 ArpGetNextFromArpcacheTable PROTO ((INT4 i4IfIndex, 
                                         INT4 *pi4NextIfIndex,
                                         UINT4 u4IpAddress, 
                                         UINT4 *pu4NextIpAddress));

/* API used by dhcpclient */
INT1 ArpResolve (UINT4 u4IpAddr, INT1 *pi1Hw_addr,
                               UINT1 *pu1EncapType);
INT1
ArpResolveInCxt (UINT4 u4IpAddr, INT1 *pi1Hw_addr, UINT1 *pu1EncapType, 
                 UINT4 u4ContextId);


/* API used by MI supported protocols */
INT1 ArpResolveWithIndex PROTO ((UINT4, UINT4, INT1 *, UINT1 *));

/* API used by VCM to notify the context creation / deletion status */
VOID ArpNotifyContextStatus PROTO ((UINT4, UINT4, UINT1));

/* API used to get the number of entries in the ARP table*/
UINT4 ArpGetEntryCount (VOID);

/* Used by Ip,rtm,dhcp,mpls,etc.*/
INT4  ArpEnqueuePkt (tArpQMsg *pRecvdQMsg);
INT4  ArpDeleteByMac PROTO ((UINT1 *pMacAddr,UINT2 VlanId,UINT2 u2Port));
 
INT4 ArpDeleteArpCacheWithIndex PROTO ((UINT4, UINT4));

/* Used by npapi/bcmx/ip6np.c */
#ifdef    IP6_WANTED
INT4  IpGetIpv4AddrFromArpTable PROTO ((INT4 i4CfaIfIndex, UINT4 *pu4IpAddr));
#endif

/* API used to notify interface status change */
INT4 ArpNotifyIfStChg PROTO ((UINT2 u2Port, UINT4 u4BitMap, UINT4 u4Status));

/* API used by DHCP/VRRP to send ARP req/resp */
INT4 ArpSendReqOrResp PROTO ((UINT2 u2Port, UINT1 u1EncapType, t_ARP_PKT *pArpPkt));

INT4 ArpInitiateAddressResolution(UINT4  u4AddrtoResolve, UINT4  u4IpIfIndex, 
                                  INT1  *pi1ArpState,
                                  tCRU_BUF_CHAIN_HEADER * pBuf);
VOID ArpAddAllArpEntries PROTO ((VOID));
VOID ArpDelAllArpEntries PROTO ((VOID));
VOID ArpAddAllArpEntriesInCxt PROTO ((UINT4 u4ContextId));
VOID ArpDelAllArpEntriesInCxt PROTO ((UINT4 u4ContextId));
INT4  ArpEnqueueIpPkt (tArpQMsg *pRecvdQMsg);

VOID ArpEventSend PROTO ((UINT4 u4Event));
#ifdef MBSM_WANTED
INT4 IpArpMbsmUpdateCardStatus PROTO ((tMbsmProtoMsg * pProtoMsg, INT4 i4Event));
VOID ArpMbsmUpdateArpTable PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf, UINT1 u1Cmd));
#endif
#ifdef EVPN_VXLAN_WANTED
VOID ArpDeleteEvpnArpCache (tARP_DATA ArpData);
VOID EvpnUtlBgpGetARPUpdate (UINT4 u4ContextId);
#endif
/* API used by Netip */
INT4 ArpSetContext PROTO ((UINT4));
VOID ArpResetContext PROTO ((VOID));

/* API used by NetIp */
INT4 ArpAddArpEntry PROTO ((tARP_DATA, tCRU_BUF_CHAIN_HEADER *, UINT1));

VOID ArpSetArpDebug PROTO ((UINT4, UINT4));

INT4 ArpGetIntfForAddr PROTO ((UINT4 u4ContextId, UINT4 u4IpAddress,
                               UINT4 *pu4IfIndex));
INT4  ArpGetCacheWithIndex PROTO ((UINT4, UINT4, t_ARP_CACHE **));
#ifdef LNXIP4_WANTED
VOID ArpNotifyArpTask (INT4);
#endif
#ifdef ICCH_WANTED
INT4
ArpIsMclagEnabled (UINT4 u4IfIndex);
VOID
ArpApiIcchInitBulkReq (VOID);
#endif
INT4
ArpNotifyL2IfStChg (UINT4 u4IfIndex, UINT4 u4PhyIfIndex);
#endif /*  _ARP_H */
