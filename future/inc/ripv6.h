/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: ripv6.h,v 1.11 2017/12/26 13:34:20 siva Exp $
 *
 * Description:This file contains the exported definitions,  
 *             macros and funtion prototypes of RIP6  
 *
 *******************************************************************/
#ifndef _RIP6_H
#define _RIP6_H


/* ---------------------- */
/* Prototype declarations */
/* ---------------------- */

VOID Rip6Init   PROTO ((VOID));

/* ---------------------- */
/* Macro declarations */
/* ---------------------- */

#define  RIP6_SUCCESS                    0
#define  RIP6_FAILURE                   -1

#define  RIP6_IF_CREATE                  1
#define  RIP6_IF_DELETE                  2
#define  RIP6_IF_UP                      3
#define  RIP6_IF_DOWN                    4
#define  RIP6_IF_ENABLE                  5
#define  RIP6_IF_DISABLE                 6
#define  RIP6_ROUTE_ADD                  7
#define  RIP6_ROUTE_DEL                  8
#define  RIP6_RTM6_ROUTE_PROCESS_EVENT  (0x00004000)

VOID         Rip6TaskMain (INT1 *);
VOID         Rip6DataNotify (INT1 *);

INT4    Rip6Lock                 PROTO ((VOID));
INT4    Rip6UnLock               PROTO ((VOID));

#define  RIP6_TASK_LOCK                  Rip6Lock
#define  RIP6_TASK_UNLOCK                Rip6UnLock
#ifdef MBSM_WANTED
INT4  Rip6FwdMbsmUpdateCardStatus    PROTO ((tMbsmProtoMsg * pProtoMsg, 
                                             UINT1 u1Cmd));
VOID Rip6MbsmUpdateLCStatus          PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                                             UINT1 u1Cmd));
#endif
#endif /* _RIP6_H */
