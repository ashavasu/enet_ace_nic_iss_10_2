/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: beepclt.h,v 1.2 2009/01/09 13:40:32 prabuc-iss Exp $
 *
 * Description: Header file contains all Beep client typedefs, apis
 * used by other modules
 *
 *******************************************************************/

#ifndef _BEEPCLT_H_
#define _BEEPCLT_H_

/* Syslog to Beep Client message types */

#define   SYSLOG_ROLE_CHG_EVENT      1
#define   SYSLOG_PROFILE_CHG_EVENT   2
#define   SYSLOG_MSG_EVENT   3

/* Syslog to Beep Profile */
#define RAW_PROFILE   1
#define BPCLT_SYSLOG_MSG_LEN  1500
typedef struct BpCltSyslog
{
    UINT4  u4MsgType;
    UINT4  u4Role;
    UINT4  i4Profile;
    UINT4   i4Port;
    tIPvXAddr  SrvAddr;
    UINT1 au1LogMsg[BPCLT_SYSLOG_MSG_LEN];
}tBpCltSyslogMsg;

typedef struct BpCltSrv
{
    INT4    i4SockId;
}tBpCltSrvMsg;

INT4  BpCltLock PROTO((VOID));

INT4  BpCltUnLock PROTO((VOID));

INT4 BpCltTaskInit PROTO((VOID));

VOID BpCltShutdown PROTO((VOID));

VOID BpCltTaskMain PROTO((INT1 *pi1TaskParam));

INT4 BpCltSyslogMsgNotify PROTO((tBpCltSyslogMsg *pSyslogMsg, UINT1 *pu1Msg));

VOID BpCltSrvMsgNotify PROTO((INT4 i4SockId));

#endif
