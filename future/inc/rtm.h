/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rtm.h,v 1.74 2016/12/27 12:35:59 siva Exp $
 *
 * Description: This file contains #definitions used in the 
 *              RTM module. 
 *
 *******************************************************************/
#ifndef _RTM_DEFN_H
#define _RTM_DEFN_H

/* Contains definitions used in RTM */

#define  RTM_REGISTRATION_MESSAGE                   1
#define  RTM_REGISTRATION_ACK_MESSAGE               2
#define  RTM_REDISTRIBUTE_ENABLE_MESSAGE            3
#define  RTM_ROUTE_UPDATE_MESSAGE                   4
#define  RTM_ROUTE_CHANGE_NOTIFY_MESSAGE            5
#define  RTM_REDISTRIBUTE_DISABLE_MESSAGE           6
#define  RTM_DEREGISTRATION_MESSAGE                 7
#define  RTM_CREATE_INT_MSG                         8
#define  RTM_DELETE_INT_MSG                         9
#define  RTM_OPER_UP_MSG                           10
#define  RTM_OPER_DOWN_MSG                         11 
#define  RTM_INTF_ROUTE_ADD_MSG                    12
#define  RTM_INTF_ROUTE_MODIFY_MSG                 13
#define  RTM_INTF_ROUTE_DEL_MSG                    14
#define  RTM_ROUTEMAP_UPDATE_MSG                   15
#define  RTM_GR_NOTIFY_MSG                         16
#define  RTM_VCM_MSG_RCVD                          17
#define  RTM_MTU_UPDATE_MSG                        18
#define  RTM_SPEED_UPDATE_MSG                      19
#define  RTM_IBGP_ROUTE_REDISTRIBUTE_ENABLE       20
#define  RTM_IBGP_ROUTE_REDISTRIBUTE_DISABLE      21
#define  RTM_ROUTE_UPDATE_ACK_MESSAGE              22
#define  RTM_INTERFACE_UP_REDISTRIBUTE             23
#define  RTM_ACTIVE                                 1
#define  RTM_NOT_IN_SERVICE                         2
#define  RTM_NOT_READY                              3
#define  RTM_CREATE_AND_WAIT                        5
#define  RTM_DESTROY                                6

#define  RTM_AGGR_MASK                              0x0001
#define  RTM_DIRECT_MASK                            0x0002
#define  RTM_STATIC_MASK                            0x0004
#define  RTM_RIP_MASK                               0x0080
#define  RTM_OSPF_MASK                              0x1000
#define  RTM_BGP_MASK                               0x2000
#define  RTM_ISISL1_MASK                            0x4000
#define  RTM_ISISL2_MASK                            0x8000
#define  RTM_ISISL1L2_MASK                          0xc000
#define  RTM_ISIS_MASK                              0x0100 
#define  RTM_INVALID_CXT_ID                         0xFFFFFFFF
#define  RTM_DEF_CXT                                0 
#define  RTM_MIN_THRESHOLD_LIMIT                    1

#define  RTM_OSPF_AND_RIP_MASK                      0x1080
#define  RTM_BGP_AND_OSPF_MASK                      0x3000
#define  RTM_BGP_AND_RIP_MASK                       0x2080
#define  RTM_ALL_RPS_MASK                           0xf180

/*MACROS FOR REDISTRIBUTE MATCH TYPE FOR OSPF */
/* the third bit of redistribute proto mask has been used for Match type of OSPF */
#define RTM_MATCH_EXT_TYPE      0x0200 /* external ospf routes */
#define RTM_MATCH_INT_TYPE      0x0400 /* internal ospf routes */
#define RTM_MATCH_NSSA_TYPE     0x0800 /* NSSA ospf routes */
#define RTM_MATCH_ALL_TYPE      0x0e00 /* both internal & external */

#define  RTM_FILTER_BASED_ON_AS_MASK                0x80
#define  RTM_PRIV_RT_MASK                           0x01

#define   RTM_FORCE_ENABLE                       1
#define   RTM_FORCE_DISABLE                      2

#define  RTM_IFINDEX_MASK                           IP_BIT_INTRFAC
#define  RTM_METRIC_MASK                            IP_BIT_METRIC
#define  RTM_ROW_STATUS_MASK                        IP_BIT_STATUS
#define  RTM_NEXT_HOP_AS_MASK                       IP_BIT_NH_AS
#define  RTM_ROUTE_TYPE_MASK                        IP_BIT_RT_TYPE

#define  RTM_MAX_TASK_NAME_LEN                      4
#define  RTM_MAX_Q_NAME_LEN                         4

#define   RTM_SUCCESS                               IP_SUCCESS
#define   RTM_FAILURE                               IP_FAILURE
#define   RTM_NO_ROOM                               IP_NO_ROOM
#define   RTM_NULL_FUN_PTR                          -2
#define   RTM_NO_RESOURCE                           -3
#define   RTM_ACK_SEND_FAIL                         -4

#define   RTM_ADD_ROUTE                             NETIPV4_ADD_ROUTE
#define   RTM_DELETE_ROUTE                          NETIPV4_DELETE_ROUTE
#define   RTM_MODIFY_ROUTE                          NETIPV4_MODIFY_ROUTE
#define   RTM_DEREGN_MESSAGE                        4
#define   RTM_REGN_MESSAGE                          5
#define   RTM_OPER_UP_MESSAGE                       6

#define RTM_SET_BIT(u2Mask, u2SrcId)  \
        (u2Mask) = (UINT2) ((u2Mask) | (0x0001 << ((UINT1)(u2SrcId-1))))

#define RTM_CLEAR_BIT(u2Mask, u2SrcId) \
        (u2Mask) = (UINT2) ((u2Mask) & (~(0x0001 << ((UINT1)u2SrcId-1))))

#define  RTM_ACK_REQUIRED                           0x80

#define  RTM_QUERIED_FOR_NEXT_HOP                   0x01
#define  RTM_QUERIED_FOR_SYNC                       0x02
#define  RTM_QUERIED_FOR_EXACT_DEST                 0x03

#define  RTM_MSG_HDR_LEN                            sizeof(tRtmMsgHdr)


#define  RTM_REGACK_ASNO_OFFSET                     0
#define  RTM_REGACK_MAXMSGSIZE_OFFSET               8
#define  RTM_REGACK_RTRID_OFFSET                    4
#define  RTM_RTUPDATE_MSG_SIZE                      40
#define  RTM_RTUPD_DESTMASK_OFFSET                  4
#define  RTM_RTUPD_DESTNET_OFFSET                   0
#define  RTM_RTUPD_MSG_OFFSET                       1
#define  RTM_RTUPD_NXTHOPAS_OFFSET                  28
#define  RTM_RTUPD_NXTHOP_OFFSET                    8
#define  RTM_RTUPD_RTAGE_OFFSET                     24
#define  RTM_RTUPD_RTBITMSK_OFFSET                  37
#define  RTM_RTUPD_RTMETR_OFFSET                    20
#define  RTM_RTUPD_RTSTATUS_OFFSET                  32
#define  RTM_RTUPD_RTTAG_OFFSET                     16
#define  RTM_RTUPD_RTTOS_OFFSET                     36
#define  RTM_RTUPD_RTTYP_OFFSET                     38

#define   RTM_MAX_PROTOMASK_ROUTES                 1000
#define RTM_LINK_LOCAL_NETWORK                     0xa9fe0000
#define RTM_DEFAULT_CXT_ID              VCM_DEFAULT_CONTEXT

#define RTM_RTRID_CONFIG_TYPE_DYNAMIC               0
#define RTM_RTRID_CONFIG_TYPE_USER                  1

#define RTM_GET_REACHABLE_BEST         0x01
#define RTM_GET_BST_WITH_PARAMS        0x02 

typedef struct _ExportRtInfo
{
    tTMO_SLL_NODE   NextRt;
    tNetIpv4RtInfo  RtInfo;
}tExpRtNode;

/********************************************************************
 * Structure definitions
 *******************************************************************/
/* RTM Registration Identifier structure. */
typedef struct RtmRegnId
{
    UINT4           u4ContextId;        /* RTM Context Id */
    UINT2           u2ProtoId;          /* Protocol Id */
    UINT2           u2AlignmentBytes;
}
tRtmRegnId;
    
/* Message Header */
typedef struct RtmMsgHdr
{
    tRtmRegnId  RegnId;         /* Registration Information */
    UINT2       u2MsgLen;       /* Length of this message excluding Header */
    UINT1       u1MessageType;  /* Identifies the type of message */
    UINT1       u1Pad;
} tRtmMsgHdr;

typedef tNetIpv4RtInfo tRtmRtUpdateMsg;

   /* Registration ack message structure. */
typedef struct RtmRegnAckMsg
{
    UINT4  u4RouterId;
    UINT4  u4MaxMessageSize;
    UINT4  u4ContextId;        /* RTM Context ID */
    UINT2  u2ASNumber;
    UINT2  u2Reserved;
    INT4   i4Status;
} tRtmRegnAckMsg;

/* To get the Local route change notification from cfa*/
typedef struct RtmRouteUpdateInfo
{
    UINT4 u4OldIpAddr;
    UINT4 u4OldNetMask;
    UINT4 u4NewIpAddr;
    UINT4 u4NewNetMask;
    UINT4 u4IfIndex;
    UINT4 u4ContextId;       /* RTM Context ID */
} tRtmRouteUpdateInfo;

   /* RTM Response message info */
typedef struct RtmRespInfo
{
    tRtmRegnAckMsg      *pRegnAck;
    tNetIpv4RtInfo      *pRtInfo;
} tRtmRespInfo;

   /* Registration message structure. */
typedef struct RtmRegnMsg
{
    tRtmRegnId  RegnId;
    INT4   (*DeliverToApplication)(tRtmRespInfo *, tRtmMsgHdr *);
    UINT1  au1TaskName[RTM_MAX_TASK_NAME_LEN];
    UINT1  au1QName[RTM_MAX_Q_NAME_LEN];
    UINT4  u4Event;
    UINT1  u1BitMask;
    UINT1  au1Reserved[3];
} tRtmRegnMsg;

  /* RRD Enable/Disable Message */
typedef struct RtmRrdMsg
{
    tRtmRegnId  RegnId;
    UINT2       u2MsgLen;
    UINT2       u2DestProtoMask;
    UINT1       au1RMapName[24];
} tRtmRrdMsg;

   /* Redistribution enable and disable message are of 4 byte length.
      The protocol mask will be the first two bytes of the message.
      Hence can be directly extracted to a UINT2 Variable. */

/* RTM Route Info Response structure */
typedef struct RouteInfoResponseMsg
{
    UINT4 u4DestinationIpAddress ;
    UINT4 u4DestinationSubnetMask ;
    UINT4 u4NextHopIpAddress ;
    UINT4 u4RouteInterfaceIndex ;
    UINT4  u4RouteTag;
    UINT4  u4RtAge;
    UINT4  u4RtNxtHopAS;
    UINT4  u4RowStatus;
    UINT4  u4Tos;
    INT4   i4Metric1;
    UINT2  u2RtType;
    UINT2 u2RouteProtocolId ;
    UINT2 u2AppIds ;
    UINT1 u1QueryFlag ;
    UINT1 u1Pad ;
} tRtmRtInfoResponseMsg ;

#define   RTM_COPY_ROUTE_INFO_TO_UPDATE_MSG(pRt, RtUpdate) \
                       RtUpdate.u4DestNet  = pRt->u4DestNet; \
                       RtUpdate.u4DestMask = pRt->u4DestMask ; \
                       RtUpdate.u4NextHop = pRt->u4NextHop ; \
                       RtUpdate.u4RtIfIndx = pRt->u4RtIfIndx ; \
                       RtUpdate.u4RouteTag = pRt->u4RouteTag ; \
                       RtUpdate.i4Metric1 = pRt->i4Metric1 ; \
                       RtUpdate.u4RtAge = pRt->u4RtAge ; \
                       RtUpdate.u4RtNxtHopAs = pRt->u4RtNxtHopAS ; \
                       RtUpdate.u4RowStatus = pRt->u4RowStatus ; \
                       RtUpdate.u4Tos = pRt->u4Tos ; \
                       RtUpdate.u1BitMask = pRt->u1BitMask ; \
                       RtUpdate.u2RtType = pRt->u2RtType ; \
                       RtUpdate.u2RtProto = pRt->u2RtProto ;\
if (pRt->pCommunity != NULL) \
{ \
    MEMCPY(RtUpdate.pCommunity,pRt->pCommunity,(pRt->u1Community *sizeof(UINT4)));\
    RtUpdate.u1CommunityCnt = pRt->u1Community; \
} \
                       RtUpdate.u1MetricType =(UINT1)pRt->i4MetricType ;
                       

/********************************************************************
 * Function Prototypes                              
 *******************************************************************/

/* Contains prototypes of functions defined in RTM module */


INT4    RtmInit                 PROTO ((VOID));
VOID    RtmDeInit               PROTO ((VOID));
INT1    RpsEnqueuePktToRtm      PROTO ((tIpBuf * pBuf));
INT1    RtmTaskInit             PROTO ((VOID));

INT4    RtmRegister             PROTO ((tRtmRegnId *pRegnId, UINT1 u1BitMask,
                                       INT4 (*SendToApplication)
                                            (tRtmRespInfo   *pRespInfo,
                                             tRtmMsgHdr *RtmHeader)));
INT4    RtmNetIpv4GetRoute      PROTO ((tRtInfoQueryMsg  *pRtQuery,
                                        tNetIpv4RtInfo   *pNetIpRtInfo));
INT4    RtmDeregister           PROTO ((tRtmRegnId *pRegnId));
INT4    RtmIpv4LeakRoute        PROTO ((UINT1 u1CmdType,
                                        tNetIpv4RtInfo *pNetRtInfo));
VOID    IpGetFwdTableRouteNumInCxt   PROTO ((UINT4 u4ContextId,
                                             UINT4 *pu4IpFwdTblRouteNum));

INT1
RtmNetIpv4GetNextBestRtEntryInCxt (UINT4 u4ContextId,
                              tRtInfo InRtInfo, tRtInfo * pOutRtInfo);
INT1
IpGetNextBestRouteEntry (tRtInfo InRtInfo, tRtInfo ** ppOutRtInfo);

INT4    IpTableGetObjectInCxt   PROTO  ((UINT4 u4ContextId, UINT4 u4Dest,
                                         UINT4 u4Mask, INT4 i4Tos,
                                         UINT4 u4NextHop, INT4 i4Proto,
                                         INT4 *pi4Val, UINT1 u1ObjType));

INT4    IpForwardingTableGetRouteInfoInCxt PROTO ((UINT4 u4ContextId, UINT4 u4Dest,
                                              UINT4 u4Mask,
                                              INT1 i1ProtoId,
                                              tRtInfo ** pIpRoutes));

VOID   RtmProcessRouteForIfStatChange PROTO ((UINT2 u2Index, INT1 i1Status));
VOID   RtmUtilProcessRtForIfStatChange PROTO ((UINT2 u2Index, INT1 i1Status));
INT4   RtmActOnStaticRoutesForIfDeleteInCxt PROTO ((UINT4 u4ContextId,
                                                    UINT4 u4IfIndex));
INT4   RtmUtilActOnStaticRtForIfDelete PROTO ((UINT4 u4ContextId,
                                                    UINT4 u4IfIndex));

VOID   RtmProcessGRRouteCleanUp PROTO ((tRtmRegnId *pRegnId, INT1 i1GRCompFlag));
INT4   IpCidrTblSetObjectInCxt PROTO ((UINT4 u4ContextId, UINT4 u4Dest, 
                                       UINT4 u4Mask, INT4 i4Tos,
                                       UINT4 u4NextHop, INT4 i4Proto,
                                       INT4 *pi4Val, UINT1 u1ObjType));
VOID   RtmGRStaleRtMark PROTO ((tRtmRegnId *pRegnId));
INT4   RtmIsForwPlanPreserved (tRtmRegnId * pRegnId);

INT4 IpCidrTblTestObjectInCxt PROTO ((UINT4 u4ContextId, UINT4 u4Dest,
                                      UINT4 u4Mask, INT4 i4Tos,
                                      UINT4 u4NextHop, INT4 i4Proto,
                                      INT4 *pi4Val, UINT1 u1ObjType,
                                      UINT4 *pu4ErrorCode));

INT4 RtmApiGetRouterId(INT4 i4RtmContextId, UINT4 *pu4RouterId);
INT4 RtmGetRtrIdConfigType(INT4 i4RtmContextId, UINT1 *pu1Type);

INT4 RtmApiCheckInvalidRtExists PROTO ((UINT4 u4ContextId, UINT4 u4DestNet,
                                 UINT4 u4DestMask, INT4 i4Tos, UINT4 u4NextHop));
INT4  RtmUtilGetNextFwdTableRtEntry PROTO ((tNetIpv4RtInfo * pNetRtInfo,
                                  tNetIpv4RtInfo * pNextNetRtInfo));
INT4
RtmUtilGetNextInactiveRtEntry (tNetIpv4RtInfo * pNetRtInfo,
                               tNetIpv4RtInfo * pNextNetRtInfo);
UINT1
RtmUtilCheckIsRouteGreater(tNetIpv4RtInfo * pRtInfo1 ,tNetIpv4RtInfo * pRtInfo2);

PUBLIC INT4
RtmApiGetNextBestRouteEntryInCxt (UINT4 u4RtmCxtId,                                                                      tRtInfo InRtInfo, tRtInfo ** ppOutRtInfo);
PUBLIC INT4
RtmApiGetBestRouteEntryInCxt (UINT4 u4RtmCxtId,                                                                      tRtInfo InRtInfo, tRtInfo ** ppOutRtInfo);
VOID
RtmNetIpv4HandleProtoDeRegInCxt PROTO ((UINT4 u4RtmCxtId, UINT1 u1ProtocolId));

PUBLIC VOID RtmAddAllRtEntriesInCxt PROTO ((UINT4 u4ContextId));
PUBLIC VOID RtmDelAllRtEntriesInCxt PROTO ((UINT4 u4ContextId));
PUBLIC INT4 RtmGetAndProgrammeBestRoute PROTO ((tRtInfoQueryMsg *pRtQuery));

/* API to invoke ARP resolution based on route look-up */
PUBLIC INT4  RtmTriggerArpResolutionInCxt PROTO ((UINT4 u4ContextId, UINT4 u4Destination));
PUBLIC INT4 RtmApiTriggerArpResolutionInCxt PROTO ((UINT4 u4ContextId, UINT4 u4Destination,
                                                    tCRU_BUF_CHAIN_HEADER * pBuf));
PUBLIC INT4 RtmApiHandlePRTRoutesForArpUpdationInCxt PROTO ((UINT4 u4ContextId,
                                          UINT4 u4NextHopAddr,
                                          UINT1 u1Operation, INT1 i1Flag));
UINT1 RtmApiIsReachableNextHopInCxt PROTO ((UINT4 u4ContextId, UINT4 u4NextHopAddr));
INT1 RtmApiConfigRRDAdminStatus PROTO ((UINT4 u4RtmCxtId));
INT1 RtmConfigRrdRouterId PROTO ((UINT4 u4Context, UINT4 u4SetValFsbgp4Identifier));
INT1 RtmResetRouterId PROTO ((UINT4 u4Context, UINT4 u4SetValFsbgp4Identifier));
INT4 RtmApiHandleOperUpIndication (UINT2 u2Port);
INT4
RtmCheckEcmpRouteCountForDest PROTO ((UINT4 u4ContextId, UINT4 u4IpRouteDest,
                                      UINT4 u4IpRouteMask, INT4 i4Metric));


/* Common Routing table Lock */
INT4 RouteTableLock (VOID);
INT4 RouteTableUnlock (VOID);
#define ROUTE_TBL_LOCK               RouteTableLock
#define ROUTE_TBL_UNLOCK             RouteTableUnlock


PUBLIC INT4 RtmIntfNotifyRoute PROTO ((UINT1 u1State,  
                                 tRtmRouteUpdateInfo *pRtmRouteUpdateInfo));
INT4    RtmUtilIpv4GetRoute (tRtInfoQueryMsg * pRtQuery,
                             tNetIpv4RtInfo * pNetIpRtInfo);
extern INT4      UtilRtmSetContext PROTO  ((UINT4 u4RtmCxtId));
extern VOID      UtilRtmResetContext PROTO  ((VOID));
#endif /* _RTM_PROTOS_H */
