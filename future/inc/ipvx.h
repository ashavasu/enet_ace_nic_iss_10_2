/* $Id: ipvx.h,v 1.31 2016/05/19 10:49:41 siva Exp $ */
#ifndef  __IPVX_H__ 
#define  __IPVX_H__ 

#include "ipv6.h"
typedef enum {

    IANAIP_ROUTE_PROTO_OTHER      = 1,
    IANAIP_ROUTE_PROTO_LOCAL      = 2,
    IANAIP_ROUTE_PROTO_NETMGMT    = 3,
    IANAIP_ROUTE_PROTO_ICMP       = 4,
    IANAIP_ROUTE_PROTO_EGP        = 5,
    IANAIP_ROUTE_PROTO_GGP        = 6,
    IANAIP_ROUTE_PROTO_HELLO      = 7,
    IANAIP_ROUTE_PROTO_RIP        = 8,
    IANAIP_ROUTE_PROTO_ISIS       = 9,
    IANAIP_ROUTE_PROTO_ESIS       = 10,
    IANAIP_ROUTE_PROTO_CISCOIGRP  = 11,
    IANAIP_ROUTE_PROTO_BBNSPFIGP  = 12,
    IANAIP_ROUTE_PROTO_OSPF       = 13,
    IANAIP_ROUTE_PROTO_BGP        = 14,
    IANAIP_ROUTE_PROTO_IDPR       = 15,
    IANAIP_ROUTE_PROTO_CISCOEIGRP = 16,
    IANAIP_ROUTE_PROTO_DVMRP      = 17 

} tIANAIpRouteProt;



typedef enum {

    IPVX_ADDR_TYPE_UNICAST   = 1,
    IPVX_ADDR_TYPE_ANYCAST   = 2,
    IPVX_ADDR_TYPE_BROADCAST = 3

} tIPvxAddrType;


typedef enum {

    IPVX_STORAGE_TYPE_OTHER       = 1,
    IPVX_STORAGE_TYPE_VOLATILE    = 2,
    IPVX_STORAGE_TYPE_NONVOLATILE = 3,
    IPVX_STORAGE_TYPE_PERMANENT   = 4,
    IPVX_STORAGE_TYPE_READONLY    = 5

} tIPvxStorageType;

typedef enum {

    IPVX_ADDR_STATUS_PREFERRED    = 1,
    IPVX_ADDR_STATUS_DEPRECATED   = 2,
    IPVX_ADDR_STATUS_INVALID      = 3,
    IPVX_ADDR_STATUS_INACCESSIBLE = 4,
    IPVX_ADDR_STATUS_UNKNOWN      = 5,
    IPVX_ADDR_STATUS_TENTATIVE    = 6,
    IPVX_ADDR_STATUS_DUPLICATE    = 7,
    IPVX_ADDR_STATUS_OPTIMISTIC   = 8

} tIPvxAddrStatus;

typedef enum {

    IPVX_IPV4_ENABLE_STATUS_UP   = 1,
    IPVX_IPV4_ENABLE_STATUS_DOWN = 2

} tIPvxIPv4EnableStatus;

typedef enum {

    IPVX_NET_TO_PHY_STATE_REACHABLE  = 1,
    IPVX_NET_TO_PHY_STATE_STALE      = 2,
    IPVX_NET_TO_PHY_STATE_DELAY      = 3,
    IPVX_NET_TO_PHY_STATE_PROBE      = 4,
    IPVX_NET_TO_PHY_STATE_INVALID    = 5,
    IPVX_NET_TO_PHY_STATE_UNKNOWN    = 6,
    IPVX_NET_TO_PHY_STATE_INCOMPLETE = 7 

} tIPvxNetToPhyState;



typedef enum {

    IPVX_NET_TO_PHY_TYPE_OTHER   = 1,
    IPVX_NET_TO_PHY_TYPE_INVALID = 2,
    IPVX_NET_TO_PHY_TYPE_DYNAMIC = 3,
    IPVX_NET_TO_PHY_TYPE_STATIC  = 4,
    IPVX_NET_TO_PHY_TYPE_LOCAL   = 5

} tIPvxNetToPhyTypes;

typedef enum {

    IPVX_TRUTH_VALUE_TRUE     = 1,
    IPVX_TRUTH_VALUE_FALSE    = 2

} tIPvxTruthValue;


typedef enum {

    IPVX_ADDR_PREFIX_ORIGIN_OTHER     = 1,
    IPVX_ADDR_PREFIX_ORIGIN_MANUAL    = 2,
    IPVX_ADDR_PREFIX_ORIGIN_WELLKNOWN = 3,
    IPVX_ADDR_PREFIX_ORIGIN_DHCP      = 4,
    IPVX_ADDR_PREFIX_ORIGIN_ROUTERADV = 5

} tIPvxAddrPrefixOrigin;

typedef enum {

    IPVX_RTR_ADV_FLAG_OTHER   = 1,
    IPVX_RTR_ADV_FLAG_MANAGED = 2

} tIPvxRAFlag;

typedef enum {

    IPVX_STATS_DIR_IN      = 1,
    IPVX_STATS_DIR_OUT     = 2

} tIPvxStatsDir;

typedef enum {

    IPVX_IP6_FORW_ENABLE   = 1,
    IPVX_IP6_FORW_DISABLE  = 2

} tIPvxIP6FwdStatus;


typedef enum {

    IPVX_STATS_IN_RECEIVES    = 1,
    IPVX_STATS_HC_IN_RECEIVES = 2,
    IPVX_STATS_IN_OCTETS      = 3,
    IPVX_STATS_HC_IN_OCTETS   = 4,
    IPVX_STATS_IN_HDR_ERRS    = 5,

    IPVX_STATS_IN_NO_ROUTE       = 6,
    IPVX_STATS_IN_ADDR_ERRS      = 7,
    IPVX_STATS_IN_UNKNOWN_PROTOS = 8,
    IPVX_STATS_IN_TRUN_PKTS      = 9,
    IPVX_STATS_IN_FWD_DGRAMS     = 10,

    IPVX_STATS_HC_IN_FWD_DGRAMS = 11,
    IPVX_STATS_REASM_REQDS      = 12,
    IPVX_STATS_REASM_OK         = 13,
    IPVX_STATS_REASM_FAILS      = 14,
    IPVX_STATS_IN_DISCARDS      = 15,

    IPVX_STATS_IN_DELIVERS     = 16,
    IPVX_STATS_HC_IN_DELIVERS  = 17,
    IPVX_STATS_OUT_REQUESTS    = 18,
    IPVX_STATS_HC_OUT_REQUESTS = 19,
    IPVX_STATS_OUT_NO_ROUTES   = 20,

    IPVX_STATS_OUT_FWD_DGRAMS    = 21,
    IPVX_STATS_HC_OUT_FWD_DGRAMS = 22,
    IPVX_STATS_OUT_DISCARDS      = 23,
    IPVX_STATS_OUT_FRAG_REQDS    = 24,
    IPVX_STATS_OUT_FRAG_OK       = 25,

    IPVX_STATS_OUT_FRAG_FAILS    = 26,
    IPVX_STATS_OUT_FRAG_CRET     = 27,
    IPVX_STATS_OUT_TRANS         = 28,
    IPVX_STATS_HC_OUT_TRANS      = 29,
    IPVX_STATS_OUT_OCTETS        = 30,

    IPVX_STATS_HC_OUT_OCTETS       = 31,
    IPVX_STATS_IN_MCAST_PKTS       = 32,
    IPVX_STATS_HC_IN_MCAST_PKTS    = 33,
    IPVX_STATS_IN_MCAST_OCTETS     = 34,
    IPVX_STATS_HC_IN_MCAST_OCTETS  = 35,

    IPVX_STATS_OUT_MCAST_PKTS       = 36,
    IPVX_STATS_HC_OUT_MCAST_PKTS    = 37,
    IPVX_STATS_OUT_MCAST_OCTETS     = 38,
    IPVX_STATS_HC_OUT_MCAST_OCTETS  = 39,
    IPVX_STATS_IN_BCAST_PKTS        = 40,

    IPVX_STATS_HC_IN_BCAST_PKTS  = 41,
    IPVX_STATS_OUT_BCAST_PKTS    = 42,
    IPVX_STATS_HC_OUT_BCAST_PKTS = 43,
    IPVX_STATS_MAX

} tIPvxStatsObj;


typedef enum 
{
    INET_VERSION_UNKNOWN = 0,
    INET_VERSION_IPV4    = 1,
    INET_VERSION_IPV6    = 2

} tInetVersion;

typedef enum {

    IPVX_IPV6_RT_IF_INDEX     = 1,
    IPVX_IPV6_RT_ROUTE_TYPE   = 2,
    IPVX_IPV6_RT_ROUTE_PROTO  = 3,
    IPVX_IPV6_RT_ROUTE_AGE    = 4,
    IPVX_IPV6_RT_ROUTE_TAG    = 5,
    IPVX_IPV6_RT_METRIC_1     = 6,
    IPVX_IPV6_RT_ROWSTATUS    = 7,
    IPVX_IPV6_RT_ADDR_TYPE    = 8,
    IPVX_IPV6_RT_PREFERENCE   = 9,
    IPVX_IPV6_RT_MAX          = 10

} tIPvxIpv6Objs;

typedef enum {

    INET_ADDR_TYPE_UNKNOWN = 0,
    INET_ADDR_TYPE_IPV4    = 1,
    INET_ADDR_TYPE_IPV6    = 2,
    INET_ADDR_TYPE_IPV4Z   = 3,
    INET_ADDR_TYPE_IPV6Z   = 4

} tInetAddrType;

typedef enum {

    IPVX_RT_OBJ_IFINDEX    = 1,
    IPVX_RT_OBJ_ROUTE_TYPE = 2,
    IPVX_RT_OBJ_NXT_HOP_AS = 3,
    IPVX_RT_OBJ_METRIC_1   = 4,
    IPVX_RT_OBJ_METRIC_2   = 5,
    IPVX_RT_OBJ_METRIC_3   = 6, 
    IPVX_RT_OBJ_METRIC_4   = 7, 
    IPVX_RT_OBJ_METRIC_5   = 8, 
    IPVX_RT_OBJ_ROWSTATUS  = 9

} tIPvxRTObjName;

#define  IPVX_CPY_U4_TYPE_TO_OCT_STR_TYPE(pOctStr,u4Val) \
    u4Val= OSIX_HTONL(u4Val); \
    if (pOctStr != NULL) \
    { \
        MEMSET ((pOctStr)->pu1_OctetList, 0, IP6_ADDR_SIZE); \
        MEMCPY((pOctStr)->pu1_OctetList, &u4Val, (sizeof(u4Val))); \
        (pOctStr)->i4_Length  = sizeof(u4Val); \
    }

#define  IPVX_CPY_OCT_STR_TYPE_TO_U4_TYPE(u4Val,pOctStr) \
    if (pOctStr != NULL) \
    { \
        MEMCPY(&u4Val,(pOctStr)->pu1_OctetList, (sizeof(u4Val))); \
        u4Val= OSIX_HTONL(u4Val); \
    }


#define IP_OCTETSTRING_TO_INTEGER(pOctetString,u4Index) { \
    u4Index = 0;\
        if(pOctetString->i4_Length > 0 && pOctetString->i4_Length <= 4){\
            MEMCPY((UINT1 *)&u4Index,pOctetString->pu1_OctetList,\
                   pOctetString->i4_Length);\
                u4Index = OSIX_NTOHL(u4Index);}}

#define IP_INTEGER_TO_OCTETSTRING(u4Index,pOctetString) {\
    UINT4 u4LocalIndex = OSIX_HTONL(u4Index);\
        MEMCPY(pOctetString->pu1_OctetList,(UINT1*)&u4LocalIndex,\
               sizeof(UINT4));\
        pOctetString->i4_Length=sizeof(UINT4);}




#define IPVX_GET_NULL_ROUTE_POLICY(pOctStr) \
    (pOctStr)->u4_Length = 2; \
    (pOctStr)->pu4_OidList[0] = 0; \
    (pOctStr)->pu4_OidList[1] = 0

#define IPVX_UTIL_GET_IPV4_IDX(u4RDst, pOctRtDest, u4RtMask, u4RtPfxLen, u4RNH, pOctRNH) \
{ \
    IPVX_CPY_OCT_STR_TYPE_TO_U4_TYPE (u4RDst, pOctRtDest); \
    u4RtMask = CfaGetCidrSubnetMask (u4RtPfxLen); \
    i4RtTos = 0; \
    IPVX_CPY_OCT_STR_TYPE_TO_U4_TYPE (u4RNH, pOctRNH); \
} \


#define IPVX_ZERO                    0   
#define IPVX_ONE                     1  
#define IPVX_TWO                     2   
#define IPVX_THREE                   3
#define IPVX_SIX                     6
#define IPVX_SEVEN                   7
#define IPVX_OID_LEN_TEN             10
#define IPVX_OID_LEN_ELEVEN          11  
#define IPVX_OID_LEN_TWELVE          12  
#define IPVX_THOUSAND                1000  
#define IPVX_IPV6_RT_TAG_MASK        0Xffff0000  
#define IPVX_IPV6_NH_AS_MASK         0X0000ffff  
#define IPVX_MAX_IPV4_ADDR           0xffffffff   
#define IPVX_IPV6_INVALID_RTR_IDX    0x7fffffff   
#define IPVX_IPV6_INVALID_IF_IDX     0x7fffffff   
#define IPVX_IPV4_INVALID_IF_IDX     0x7fffffff   
#define IPVX_UINT1_ALL_ONE           0xff

#define IPVX_METRIC_NOT_USED    -1

#define IPVX_INVALID_IFIDX   (-1)


#define IPVX_IPV4_PFX_PRE_LIFE_TIME  0xffffffff
#define IPVX_IPV4_PFX_VAL_LIFE_TIME  0xffffffff

#define ICMP_TYPE_MIN_VAL   0 
#define ICMP_TYPE_MAX_VAL   41
/* ICMPv6 Message Types */

#define  ICMP6_PKT_TOO_BIG            2
#define  ICMP6_TIME_EXCEEDED          3
#define  ICMP6_PKT_PARAM_PROBLEM      4
#define  ICMP6_ECHO_REQUEST           128
#define  ICMP6_ECHO_REPLY             129
#define  ICMP6_INFO_MSG_TYPE_MIN_VAL  128
#define  ICMP6_FORMAT                 58
#define  ICMP6_OFFSET                 6

/* Neighbour Discovery Message Types */

#define  ICMP6_MLD_QUERY                130
#define  ICMP6_MLD_REPORT               131 
#define  ICMP6_MLD_DONE                 132
#define  ICMP6_MLDV2_REPORT             143 
#define  ICMP6_ROUTER_SOLICITATION      133
#define  ICMP6_ROUTER_ADVERTISEMENT     134
#define  ICMP6_NEIGHBOUR_SOLICITATION   135
#define  ICMP6_NEIGHBOUR_ADVERTISEMENT  136
#define  ICMP6_ROUTE_REDIRECT           137
#define  ICMP6_RATE_LIMIT               138
#define  ICMP6_NS_NA_OFFSET             40
/* ICMPv6 Message Codes */
#define ICMP6_DEST_UNREACH                0
#define ICMP6_DST_ADMIN_PROHIB            1
#define ICMP6_BEYOND_SCOPE_SADDR          2
#define ICMP6_ADDR_UNREACH                3
#define ICMP6_PORT_UNREACH                4
#define ICMP6_SADDR_FAIL_INGR_EGR_POLICY  5
#define ICMP6_REJECT_ROUTE_TO_DEST        6
#define ICMP6_ERR_SRC_RT_HEADER           7


#define IPVX_IPV4_STATS_DISCON_TIME     0
#define IPVX_IPV6_STATS_DISCON_TIME     0
#define IPVX_IPV4_STATS_REFRESH_RATE    1000
#define IPVX_IPV6_STATS_REFRESH_RATE    1000

/* It has to be replaced For IPv4 routers or IPv6 routers that are using the
 * updated router advertisement format as described in he Default Router
 * Preferences document. 
 */
#define IPVX_DEF_ROUTER_PREF_MEDIUM  0
#define IPVX_IP6_IF_DEF_HOPLMT       64 



VOID
IPvxUpdateIfStatsTblLstChange (VOID);

INT4 IpvxGetCurrContext (VOID);

INT4
UtilIpvxSetCxt (UINT4 u4ContextId);

INT4
UtilIpvxReleaseCxt (VOID);

/* IPv4 IPvx interfaces */

#ifdef LNXIP4_WANTED
INT1
IPvxValidateIdxInsIpv4IfTable (INT4 i4Ipv4InterfaceIfIndex);

INT1
IPvxGetNextIndexIpv4InterfaceTable (INT4 i4Ipv4InterfaceIfIndex, 
                                    INT4 *pi4NextIpv4InterfaceIfIndex);
#endif

INT1
IPvxGetIpv4IfEnableStatus (INT4 i4Ipv4IfIndex, INT4 *pi4Ipv4IfEnableStatus);


INT1
IPvxSetIpv4IfEnableStatus (INT4 i4Ipv4IfIndex, INT4 i4Ipv4IfEnableStatus);


INT1
IPvxGetIpv4Stats (INT4 i4Ipv4IfIdx, UINT4 u4StatsType, UINT1 *pu1Ipv4StatsObj);

INT1
IPvxValIdxInstIpv4DefRtrTbl (UINT4 u4Ipv4DefRtrAddr, INT4  i4Ipv4DefRtrIfIndex);

INT1
IPvxGetNxtIdxIpv4DefRtrTbl (UINT4 u4Ipv4DefRtrAddr, 
                            UINT4 *pu4NxtIpv4DefRtrAddr,
                            INT4  i4Ipv4DefRtrIfIndex, 
                            INT4  *pi4NextIpv4DefRtrIfIndex);

INT1
IPvxGetIpv4DefRtrLifetime (UINT4 u4DefRtrAddr, INT4 i4IpDefRtrIfIndex,
                           UINT4 *pu4IpDefRtrLifetime);
INT1
IPvxGetIpv4DefRtrPreference  (UINT4 u4DefRtrAddr, INT4 i4Ipv4DefRtrIfIndex,
                              UINT4 *pu4IpDefRtrPref);
INT1
IPvxIpv4GetIcmpMsgStatsInPkts (INT4 i4IcmpMsgStatsType, 
                               UINT4 *pu4RetValIcmpMsgStatsInPkts);
INT1
IPvxIpv4GetIcmpMsgStatsOutPkts (INT4 i4IcmpMsgStatsType, 
                                UINT4 *pu4RetValIcmpMsgStatsOutPkts);
INT1
IpvxGetNextIndexIpNetToPhyTable (INT4 i4IpNetToPhyIfIndex,
                                 INT4 *pi4NxtIpNetToPhyIfIndex,
                                 INT4 i4IpNetToPhyNetAddrType,
                                 INT4 *pi4NxtIpNetToPhyNetAddrType,
                                 tSNMP_OCTET_STRING_TYPE
                                 *pIpNetToPhyNetAddr,
                                 tSNMP_OCTET_STRING_TYPE 
                                 *pNxtIpNetToPhyNetAddr);
INT1
IPvxTestIpv4NetToPhyRowStatus (UINT4 *pu4ErrorCode, INT4 i4Ipv4IfIndex, 
                               UINT4  u4Ipv4Addr, INT4 i4Ipv4TestRowStatus);
INT1
IPvxTestIPv4NetToPhyType (UINT4 *pu4ErrorCode, INT4 i4Ipv4IfIndex, 
                          UINT4  u4Ipv4Addr, INT4 i4TestValIpNetToPhysicalType);
INT1
IPvxSetIpv4NetToPhyRowStatus (INT4 i4Ipv4IfIndex, UINT4 u4Ipv4Addr, 
                              INT4 i4Ipv4SetRowStatus);
INT1
IPvxSetIPv4NetToPhyType (INT4  i4Ipv4IfIndex, UINT4  u4Ipv4Addr,
                         INT4 i4TestValIpNetToPhysicalType);
INT1
IPvxSetIPv4NetToPhyPhyAddr (INT4  i4Ipv4IfIndex, UINT4 u4Ipv4Addr,
                            tSNMP_OCTET_STRING_TYPE *pPhysAddr);
INT1
IPvxGetIpv4NetToPhyRowStatus (INT4 i4Ipv4IfIndex, UINT4 u4Ipv4Addr, 
                              INT4 *pi4Ipv4GetRowStatus);
INT1
IPvxGetIPv4NetToPhyLastUpdated (INT4  i4IPv4IfIndex, UINT4 u4Ipv4Addr,
                                UINT4 *pu4IPv4ARPTblLastUpdated);

/*ISSHealthClearChk*/

VOID
IPvxClearIpv4SystemStats(UINT4 u4ContextId , INT4 i4Ipv4IfIndex);
VOID
IPvxClearIpv6SystemStats(UINT4 u4ContextId , INT4 i4Ipv6IfIndex);
VOID
IPvxClearIpv4IcmpStats(UINT4 u4ContextId);
VOID
IPvxClearIpv6IcmpStats(UINT4 u4ContextId);
VOID
ClearInetCidrRoute(UINT4 u4ContextId);
VOID
ClearInetCidrIpv6Route(UINT4 u4ContextId);
VOID
ClearUdpDatagrams(UINT4 u4ContextId);
VOID
ClearUDPIpv6Datagrams(UINT4 u4ContextId);
VOID
ClearMIIpv6IfStatsTable(INT4 i4FsMIStdIpIfStatsIfIndex);
VOID
ClearFsMIIpv6IcmpStatsTable(INT4 i4FsMIStdIpContextId);
VOID
ClearFsIpv6IfIcmpTable(VOID);


VOID
IPvxTstPopulateIpv4SystemStats(UINT4 u4ContextId , INT4 i4Ipv4IfIndex);
VOID
IPvxTstPopulateIpv6SystemStats(UINT4 u4ContextId , INT4 i4Ipv6IfIndex);
VOID
IPvxTstPopulateIpv4IcmpStats(UINT4 u4ContextId);
VOID
IPvxTstPopulateIpv6IcmpStats(UINT4 u4ContextId);
VOID
RtmTstPopulateInetCidrRoute(UINT4 u4ContextId);
VOID
Rtm6TstPopulateInetCidrIpv6Route(UINT4 u4ContextId);
VOID
UdpTstPopulateDatagrams(UINT4 u4ContextId);
VOID
UdpTstPopulateIpv6Datagrams(UINT4 u4ContextId);
VOID
Ipv6TstPopulateMIIfStatsTable(INT4 i4FsMIStdIpIfStatsIfIndex);
VOID
Ipv6TstPopulateFsMIIcmpStatsTable(INT4 i4FsMIStdIpContextId);




/* IPv6 IPvx interfaces */
INT1
IPvxGetIpv6Stats (INT4 i4Ipv6IfIndex, INT4 i4ContextId, 
                  UINT4 u4IPvxStatsType, UINT1 *pu1Ipv6StatsObj);
INT1
IPvxValIdxInstIpv6DefRtrTbl (tSNMP_OCTET_STRING_TYPE *pIpv6DefRtrAddr,
                             INT4  i4Ipv6DefRtrIfIndex);
INT1
IPvxGetNxtIdxIpv6DefRtrTbl (tSNMP_OCTET_STRING_TYPE *pIpv6DefRtrAddr,
                            tSNMP_OCTET_STRING_TYPE *pNextIpv6DefRtrAddr,
                            INT4 i4Ipv6DefRtrIfIndex,
                            INT4 *pi4NextIpv6DefRtrIfIndex);

INT1
IPvxGetIpv6DefRtrLifetime (tSNMP_OCTET_STRING_TYPE *pIpv6DefRtrAddr,
                           INT4 i4Ipv6DefRtrIfIndex,
                           UINT4 *pu4Ipv6DefRtrLifetime);
INT1
IPvxGetIpv6DefRtrPreference (tSNMP_OCTET_STRING_TYPE *pIpv6DefRtrAddr,
                             INT4 i4Ipv6DefRtrIfIndex,
                             UINT4 *pu4Ipv6DefRtrPref);

INT1
IPvxValidateIpv6IcmpMsgType (INT4 i4IcmpMsgStatsType);
INT1
IPvxGetNxtIpv6IcmpMsgType (INT4 i4IcmpMsgType, INT4 *pi4NextIcmpMsgType);

INT1
IPvxIpv6GetIcmpMsgStatsPkts (UINT4 u4ContextId, INT4 i4IcmpMsgStatsType,
                             UINT1 u1Dir, UINT4 *pu4RetValIcmpMsgStatsInPkts);
INT1
IPvxGetIPv6NetToPhyLastUpdated (INT4  i4Ipv6IfIndex,
                                tSNMP_OCTET_STRING_TYPE *pIpv6Addr,
                                UINT4 *pu4IPv6NDCTblLastUpdated);
INT1
IPvxGetIPv6NetToPhyType (INT4  i4Ipv6IfIndex,
                         tSNMP_OCTET_STRING_TYPE *pIpv6Addr,
                         INT4 *pi4IpNetToPhysicalType);
INT1
IPvxGetIpv6NetToPhyState (INT4  i4Ipv6IfIndex,
                          tSNMP_OCTET_STRING_TYPE *pIpv6Addr,
                          INT4 *pi4IPv6NDCTblState);

INT1
IPvxGetIpv6NetToPhyRowStatus (INT4 i4Ipv6IfIndex, 
                              tSNMP_OCTET_STRING_TYPE *pIpv6Addr,
                              INT4 *pi4Ipv6GetRowStatus);
INT1
IPvxSetIPv6NetToPhyPhyAddr (INT4  i4Ipv6IfIndex,
                            tSNMP_OCTET_STRING_TYPE *pIpv6Addr,
                            tSNMP_OCTET_STRING_TYPE *pPhysAddr);
INT1
IPvxSetIPv6NetToPhyType (INT4  i4Ipv6IfIndex,
                         tSNMP_OCTET_STRING_TYPE *pIpv6Addr,
                         INT4 i4TestValIpNetToPhysicalType);
INT1
IPvxSetIpv6NetToPhyRowStatus (INT4 i4Ipv6IfIndex, 
                              tSNMP_OCTET_STRING_TYPE *pIpv6Addr,
                              INT4 i4Ipv6SetRowStatus);
INT1
IPvxTestIPv6NetToPhyType (UINT4 *pu4ErrorCode, INT4  i4Ipv6IfIndex,
                          tSNMP_OCTET_STRING_TYPE *pIpv6Addr,
                          INT4 i4TestValIpNetToPhysicalType);
INT1
IPvxTestIpv6NetToPhyRowStatus (UINT4 *pu4ErrorCode, INT4 i4Ipv6IfIndex, 
                               tSNMP_OCTET_STRING_TYPE *pIpv6Addr, 
                               INT4 i4Ipv6TestRowStatus);


INT1
IpvxUtilClearNetToPhysicalTable (INT4 i4NxtIfIndex, INT4 i4NxtAddrType, tSNMP_OCTET_STRING_TYPE *NxtAddr);
INT1
IpvxUtilClearIp6RouterAdvtTable (INT4 i4IfIndex);
INT1
IpvxUtilClearAddrPrefixTable (INT4 i4NxtIfIndex, INT4 i4NxtAddrType, tSNMP_OCTET_STRING_TYPE *NxtAddr, UINT4 u4NxtPrefLen);

INT1
IpvxUtilGetFirstIndexFsMIStdIpifTable ARG_LIST((INT4 *));

INT1
IpvxUtilGetNextIndexFsMIStdIpifTable ARG_LIST((INT4 , INT4 *));

INT1
IpvxUtilGetFsMIStdIpProxyArpAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
IpvxUtilGetFsMIStdIpLocalProxyArpAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
IpvxUtilSetFsMIStdIpProxyArpAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
IpvxUtilSetFsMIStdIpLocalProxyArpAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
IpvxUtilTestv2FsMIStdIpProxyArpAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
IpvxUtilTestv2FsMIStdIpLocalProxyArpAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));


INT1
IpvxUtilGetFsMIStdIpProxyArpSubnetOption ARG_LIST((INT4 *));


INT1
IpvxUtilSetFsMIStdIpProxyArpSubnetOption ARG_LIST((INT4 ));

INT1
IpvxUtilTestv2FsMIStdIpProxyArpSubnetOption ARG_LIST((UINT4 *  ,INT4 ));


INT1
Ip6ValidateIndexInstanceRtrAdvtTable (INT4 i4RtrAdvtIfIndex);

INT1
Ip6GetFirstIndexRtrAdvtTable (INT4 *pi4RtrAdvtIfIndex);

INT1
Ip6GetNextIndexRtrAdvtTable (INT4 i4RtrAdvtIfIndex,
                             INT4 *pi4NextRtrAdvtIfIndex);
INT1
Ip6GetRtrAdvtSendAdverts (INT4 i4RtrAdvtIfIndex, INT4 *pi4RtrAdvtSendAdverts);

INT1
Ip6GetRtrAdvtMaxInterval (INT4 i4RtrAdvtIfIndex, UINT4 *pu4RtrAdvtMaxInterval);

INT1
Ip6GetRtrAdvtMinInterval (INT4 i4RtrAdvtIfIndex, UINT4 *pu4RtrAdvtMinInterval);

INT1
Ip6GetRtrAdvtMFlag (INT4 i4RtrAdvtIfIndex, INT4 *pi4RtrAdvtMFlag);

INT1
Ip6GetRtrAdvtOFlag (INT4 i4RtrAdvtIfIndex, INT4 *pi4RtrAdvtOFlag);

INT1
Ip6GetRtrAdvtLinkMTU (INT4 i4RtrAdvtIfIndex, UINT4 *pu4RtrAdvtLinkMTU);

INT1
Ip6GetRtrAdvtReachTime (INT4 i4RtrAdvtIfIndex, UINT4 *pu4RtrAdvtReachTime);

INT1
Ip6GetRtrAdvtTxTime (INT4 i4RtrAdvtIfIndex, UINT4 *pu4RtrAdvtTxTime);

INT1
Ip6GetRtrAdvtCurHopLimit (INT4 i4RtrAdvtIfIndex, UINT4 *pu4RtrAdvtCurHopLimit);

INT1
Ip6GetRtrAdvtDefLifeTime (INT4 i4RtrAdvtIfIndex, UINT4 *pu4RtrAdvtDefLifeTime);

INT1
Ip6GetRtrAdvtRowStatus (INT4 i4RtrAdvtIfIndex, INT4 *pi4RtrAdvtRowStatus);

INT1
Ip6SetRtrAdvtSendAdverts (INT4 i4RtrAdvtIfIndex, INT4 i4RtrAdvtSendAdverts);

INT1
Ip6SetRtrAdvtMaxInterval (INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvtMaxInterval);

INT1
Ip6SetRtrAdvtMinInterval (INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvtMinInterval);

INT1
Ip6SetRtrAdvtMFlag (INT4 i4RtrAdvtIfIndex, INT4 i4RtrAdvtMFlag);

INT1
Ip6SetRtrAdvtOFlag (INT4 i4RtrAdvtIfIndex, INT4 i4RtrAdvtOFlag);

INT1
Ip6SetRtrAdvtLinkMTU (INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvtLinkMTU);

INT1
Ip6SetRtrAdvtReachTime (INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvtReachTime);

INT1
Ip6SetRtrAdvtTxTime (INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvtTxTime);

INT1
Ip6SetRtrAdvtCurHopLimit (INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvtCurHopLimit);

INT1
Ip6SetRtrAdvtDefLifeTime (INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvtDefLifeTime);

INT1
Ip6SetRtrAdvtRowStatus (INT4 i4RtrAdvtIfIndex, INT4 i4RtrAdvtRowStatus);

INT1
Ip6TestRtrAdvtSendAdverts (UINT4 *pu4ErrorCode,
                           INT4 i4RtrAdvtIfIndex, INT4 i4RtrAdvtSendAdverts);

INT1
Ip6TestRtrAdvtMaxInterval (UINT4 *pu4ErrorCode,
                           INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvtMaxInterval);

INT1
Ip6TestRtrAdvtMinInterval (UINT4 *pu4ErrorCode,
                           INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvtMinInterval);

INT1
Ip6TestRtrAdvtMFlag (UINT4 *pu4ErrorCode,
                     INT4 i4RtrAdvtIfIndex, INT4 i4RtrAdvtMFlag);

INT1
Ip6TestRtrAdvtOFlag (UINT4 *pu4ErrorCode,
                     INT4 i4RtrAdvtIfIndex, INT4 i4RtrAdvtOFlag);

INT1
Ip6TestRtrAdvtLinkMTU (UINT4 *pu4ErrorCode,
                       INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvtLinkMTU);

INT1
Ip6TestRtrAdvtReachTime (UINT4 *pu4ErrorCode,
                         INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvtReachTime);

INT1
Ip6TestRtrAdvtTxTime (UINT4 *pu4ErrorCode,
                      INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvtTxTime);

INT1
Ip6TestRtrAdvtCurHopLimit (UINT4 *pu4ErrorCode,
                           INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvtCurHopLimit);

INT1
Ip6TestRtrAdvtDefLifeTime (UINT4 *pu4ErrorCode,
                           INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvtDefLifeTime);

INT1
Ip6TestRtrAdvtRowStatus (UINT4 *pu4ErrorCode,
                         INT4 i4RtrAdvtIfIndex, INT4 i4RtrAdvtRowStatus);

INT4 Ip6ifGetNextIndex PROTO ((UINT4 *pu4Index));

INT4
IpvxClearIpInterfaceEntries PROTO ((UINT4 u4IfIndex));

INT4
IpvxClearIp6InterfaceEntries PROTO ((UINT4 u4IfIndex));

INT4
IpvxClearIpContextEntries PROTO ((UINT4 u4ContextId));

INT4
IpvxClearIp6ContextEntries PROTO ((UINT4 u4ContextId));

INT1
Ip6SetRtrAdvertRDNSSOpen (INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvertRDNSSOpen);

INT1
Ip6SetRtrAdvertRDNSS (INT4 i4RtrAdvtIfIndex, UINT4 u4RtrAdvertRDNSS);

INT1
Ip6SetRaRDNSSAddress (INT4 i4RtrAdvtIfIndex,tIp6Addr Ip6RdnssAddr);

INT1
Ip6SetRtrAdvtRDNSSLife ( INT4 i4RtrAdvtIfIndex,
                           UINT4 u4RtrRDNSSLife);
INT1
Ip6SetRtrAdvtRDNSSLifeOne (INT4 i4RtrAdvtIfIndex,
                           UINT4 u4RtrRDNSSLifeOne);
INT1
Ip6SetRtrAdvtRDNSSLifeTwo (INT4 i4RtrAdvtIfIndex,
                           UINT4 u4RtrRDNSSLifeTwo);
INT1
Ip6SetRtrAdvtRDNSSLifeThree (INT4 i4RtrAdvtIfIndex,
                             UINT4 u4RtrRDNSSLifeThree);
INT1
Ip6SetRtrAdvtRDNSSPref ( INT4 i4RtrAdvtIfIndex,
                           UINT4 u4RtrRDNSSPref);
INT1
Ip6GetRADNSStatus (INT4 i4IfIndex, INT4 * pShowIp6Interface);
INT4  IpvxLock PROTO ((VOID));
INT4  IpvxUnLock PROTO ((VOID));
INT4  CheckIp6IsFwdEnabled PROTO ((INT4 i4Index));
INT1 Ip6SetAdvDefaultPreference(INT4 i4AdvDefPrefIfIndex, INT4 i4DefRouterPreference);
#if defined (IP_WANTED)
INT4
IPvxHandleIpv4IfEnableStatus (INT4 i4Ipv4IfIndex, INT4 i4Ipv4EnableStatus);
INT1
IPvxSetProxyArpAdminStatus (INT4 i4FsIpifIndex, INT4 i4SetValFsIpifProxyArpAdminStatus);
#endif
#endif /*  __IPVX_H__  */
