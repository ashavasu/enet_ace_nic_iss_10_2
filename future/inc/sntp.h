/********************************************************************
 *  * Copyright (C) Future Sotware,1997-98,2001
 *  *
 *  * $Id: sntp.h,v 1.12 2014/06/24 11:32:29 siva Exp $
 *  *
 *  * Description: This file contains the constants, gloabl structures, 
 *    typedefs, enums uased in  SNTP  module.
 *  *
 *  *******************************************************************/

#ifndef _SNTP_H
#define _SNTP_H



#define SNTP_POLL_INTERVAL 6 /* 64 secs */
#define SNTP_MAX_POLL_INTERVAL 14 
#define SNTP_MIN_POLL_INTERVAL 4 
/* MANGO: unused */
/* #define SNTP_TIMEOUT_INTERVAL 5 */
#define SNTP_MD5_KEY_LENGTH 16
#define SNTP_DES_KEY_LENGTH 8
#define SNTP_SERVER_PORT 123


#define SNTP_TASK_NAME "SNT"
#define SNTP_TASK_PRIORITY 130


#define SNTP_FORWARD_TIME_ZONE '+' 
#define SNTP_BACK_TIME_ZONE    '-'

#define SNTP_SUCCESS_DHCPC 1

#define SNTP_DST_ENABLE      1
#define SNTP_DST_DISABLE     2
#define SNTP_MAX_KEY_LEN     16




#define SECS_IN_YEAR(yr) (IS_LEAP((yr)) ? 31622400 : 31536000);
#define IS_LEAP(yr)      ((yr) % 4 == 0 && ((yr) % 100 != 0 || (yr) % 400 == 0))



/* 
 * This structure is used by DHCP/BOOTP Client to 
 * pass the ntp server  parameters obtained from DHCP/BOOTP Server. 
 */ 
typedef struct _tSntpParams {
    UINT4 u4SntpPriAddr;  
    UINT4 u4SntpSecAddr;
} tSntpParams;

        
VOID SntpMain(INT1 *);

/*lrmain.c */
VOID SntpSystemTimeInit (VOID);
INT4 SntpLock (VOID);
INT4 SntpUnLock (VOID);
PUBLIC INT4  SntpServerParamsFromDhcpc (tSntpParams *pSntpParams);
PUBLIC INT1  SntpClientStatus (INT4 *pi4RetValFsSntpEntStatusCurrentModeVal);
INT4 SntpGetTimeZoneDiffInSec (VOID);
UINT4 SntpGetTimeSinceEpoch (VOID);
#if LNXIP4_WANTED
UINT4 FsSntpStatusInit (INT4); 
#endif
#define   SNTP_LOCK() SntpLock ()
#define   SNTP_UNLOCK() SntpUnLock ()
#define SNTP_MAX_TIME_LEN  32

UINT4 SntpTmToSec (tUtlTm *);
VOID SntpSecToTm(UINT4, tUtlTm *);
#endif
