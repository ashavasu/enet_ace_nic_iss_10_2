/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: wps.h,v 1.4 2017/12/15 09:50:50 siva Exp $
 *
 * Description: WPS File.
 * *********************************************************************/

#ifndef __WPS_H__
#define __WPS_H__

#define WPS_IE_LEN 255
#define WPS_MAX_AUTHORIZED_MACS 5
#define WPS_ETH_ALEN 6
#define WPS_UUID_LEN 16
#define WPS_MAX_LEN  32
#define ZERO 0

#define WPS_ENABLED 1
#define WPS_DISABLED 2
#define WPS_IE_ELEMENT_LENGTH 1
#define WPS_IE_CAPAB_LENGTH 1
#define WPS_IE_STATE_LENGTH 1
#define WPS_IE_ADDITIONALIE_LENGTH 1
#define WPS_IE_LENGTH_FIELD 1
#define WPS_IE_CONFIG_METHOD_LENGTH 2
#define WPS_VENDOR_EXTENSION_TAG 0x1049
#define WPS_UUID_E_TAG 0x1047
#define WPS_DEVICE_PASSWORD_ID_TAG 0x1012

#define  STRCPY(d,s)       strcpy ((char *)(d),(const char *)(s))
#define  STRNCPY(d,s,n)    strncpy ((char *)(d), (const char *)(s), (size_t) (n))

#define WPS_OUI 0x04
#define WPS_ALL_AP                      0xFF
#define WPS_VERSION_TAG 0x104a
#define WPS_STATE_TAG  0x1044
#define WPS_SELECTED_REGISTRAR_TAG 0x1041

#define WPS_DEVICE_PASSWORD_ID_TAG  0x1012
#define WPS_RESPONSE_TAG 0x103b
#define WPS_UUID_E_TAG  0x1047
#define WPS_MANUFACT_TAG  0x1021
#define WPS_MODEL_NAME_TAG  0x1023
#define WPS_MODEL_NUM_TAG  0x1024
#define WPS_SERIAL_NUM_TAG  0x1042
#define WPS_PRIM_DEV_TYPE_TAG  0x1054
#define WPS_PRIM_DEV_TYPE_LEN  8
#define WPS_PRIM_DEV_NW_INFRASTRUCTURE      6
#define WPS_SEC_DEV_TYPE_AP  1
#define WPS_DEVICE_NAME_TAG  0x1011
#define WPS_CONFIG_METHODS_TAG  0x1008
#define WPS_SELECTED_REGISTRAR_CONFIG_METHODS_TAG  0x1053
#define WPS_RF_BANDS_TAG  0X103C
#define WPS_VENDOR_EXTENSION_TAG  0x1049
#define WFA_ELEM_VERSION2_TAG 0x00
#define WFA_ELEM_AUTH_MAC_TAG 0x01
#define WFA_VERSION2_LEN 1
#define WFA_VERSION2_VAL 0x20




typedef struct wpsIeElemets {
    UINT2 u2WtpInternalId;
    UINT2 configMethods;
    UINT1 u1ElemId;
    UINT1 u1Len;
    BOOL1 bWpsEnabled;
    UINT1 wscState;
    UINT1 authorized_macs[WPS_MAX_AUTHORIZED_MACS][WPS_ETH_ALEN];
    BOOL1 bWpsAdditionalIE;
    UINT1 noOfAuthMac;
    UINT1 uuid_e[16];
    UINT1 statusOrAddIE;
    BOOL1 isOptional;
    UINT1          u1WtpNoofRadio;
    UINT1   au1Pad[1];
#ifdef WPS_WTP_WANTED
    UINT1          au1WtpModelNumber[WPS_MAX_LEN];
    UINT1               au1WtpSerialNumber[WPS_MAX_LEN];
    UINT1               au1WtpName[WPS_MAX_LEN];
#endif
}tWpsIEElements;

typedef enum {
    WSSWPS_PROFILEIF_ADD = 0,
    WSSWPS_PROFILEIF_DEL,
    WSSWPS_ASSOC_IND,
    WSSWPS_REASSOC_IND,
    WSSWPS_DISSOC_IND,
    WSSWPS_PROBREQ_IND,
    PNACWPS_EAP_REQ_IND,
    PNACWPS_EAP_MSG_IND,
    WSSWPS_PBC_IND
} enWssWpsNotifyType;

enum wps_status
{
    WPS_STATUS_SUCCESS,
    WPS_STATUS_FAILURE
};

enum pbc_status
{
    WPS_PBC_STATUS_SUCCESS,
    WPS_PBC_STATUS_OVERLAP,
    WPS_PBC_STATUS_INPROGRESS,
    WPS_PBC_STATUS_INACTIVE,
    WPS_PBC_STATUS_ERROR
};

typedef struct WPS_PROBREQIND  {
    size_t ssid_len;
    UINT2 dev_password_id;
    UINT1 au1StaMac[MAC_ADDR_LEN];
    UINT1 au1OldApMac[MAC_ADDR_LEN];
    UINT1 bWpsPresent;
    UINT1 uuid_e[16];
    BOOL1 request_to_enroll;
    UINT1 ssid[32];                         
    UINT1 au1Pad[16];
}tWpsProbeReqInd;

typedef struct  WPS_DISSOC {
    UINT2 u2reasonCode;
    UINT2 u2aligned;
    UINT1 au1StaMac[MAC_ADDR_LEN];
    UINT1 au1Pad[2];
}tWpsDissoc ;


typedef struct  WPS_ASSOCIND  {
    UINT2 u2SessId;
    UINT2 u2WtpId;
    UINT1  au1StaMac[MAC_ADDR_LEN];
    UINT1  au1ApMac[MAC_ADDR_LEN];
    UINT1  au1WpsIe[WPS_IE_LEN];
    UINT1  u1WpsIELen;
    UINT1 radioType;
    UINT1 u1RadioId;
    UINT1 bWpsPresent;
    UINT1 bRsnaPresent;
} tWpsAssocInd;

typedef struct WPS_REAASOCIND  {
    UINT1  au1StaMac[MAC_ADDR_LEN];
    UINT1  au1OldApMac[MAC_ADDR_LEN];
    UINT1  au1WpsIe[WPS_IE_LEN];
    UINT1  u1WpsIELen;
}tWpsReassocInd;

typedef struct  WPS_AUTHIND  {
    UINT1  ifIndex;
    UINT1  authIndex;
    UINT1  authType;
    UINT1 au1Pad[1];
} tWpsAuthInd;

typedef struct  WPS_ENCRYPIND  {
    UINT1  ifIndex;
    UINT1  encryIndex;
    UINT1  encryType;
    UINT1 au1Pad[1];
} tWpsEncrypInd;

typedef struct  WPS_PSKIND  {
    UINT1  ifIndex;
    UINT1  psk;
    UINT1  type;
    UINT1 au1Pad[1];
} tWpsPskInd;

typedef struct  WPS_PNACIND  {
    UINT1  au1StaMac[MAC_ADDR_LEN];
    UINT2  u2PortNum;
    UINT1 *pu1PktPtr;
    UINT2 u2PktLen;
    UINT1 au1Pad[2];
} tWpsPnacInd;

typedef struct  WPS_PBCIND  {
    UINT2 u2WtpInternalId;
    UINT1 WlanId;
    UINT1 au1Pad[1];
} tWpsPbcInd;

typedef struct WssWPSNotifyParams{
    UINT4 u4IfIndex ; /* this is the CFA index of the wireless device
                         on which the request will be sent
                         */
    enWssWpsNotifyType eWssWpsNotifyType;
    union {
        tWpsAssocInd      wpsAssocInd;
        tWpsReassocInd    wpsReassocInd;
        tWpsDissoc        wpsDissocInd;
        tWpsPnacInd        wpsPnacInd;
        tWpsProbeReqInd   wpsProbeReqInd;
        tWpsAuthInd       wpsAuthInd;
        tWpsEncrypInd     wpsEncrypInd;
        tWpsPskInd        wpsPskInd;
        tWpsPbcInd        wpsPbcInd;
    } unWpsIndication;
#define WPSPNACIND       unWpsIndication.wpsPnacInd
#define WPSASSOCIND       unWpsIndication.wpsAssocInd
#define WPSREASSOCIND     unWpsIndication.wpsReassocInd
#define WPSDISSOCIND      unWpsIndication.wpsDissocInd
#define WPSDEAUTHIND      unWpsIndication.wpsDeauthInd
#define WPSPROBEREQIND      unWpsIndication.wpsProbeReqInd
#define WPSAUTHIND      unWpsIndication.wpsAuthInd
#define WPSENCRYPIND      unWpsIndication.wpsEncrypInd
#define WPSPSKIND      unWpsIndication.wpsPskInd
#define WPSPBCIND      unWpsIndication.wpsPbcInd
} tWssWpsNotifyParams;

typedef struct WpsStat {
    tRBNodeEmbd WpsStatsNode;
    UINT4           u4StatsIndex; 
    enum wps_status e1Status;
    enum pbc_status e1PbcStatus;
    UINT2           u2Port;
    UINT1 au1SuppMacAddr[WPS_ETH_ALEN];
}tWpsStat;


UINT4 WpsInitModuleStart                       PROTO ((VOID));
PUBLIC VOID WpsTaskMain PROTO ((INT1 *));
INT1 WpsGetWPSEnabled(UINT4 u4IfIndex, 
        UINT2 u2WtpInternalId,
        INT4 *pi4RetValFsWPSStatus);

UINT1 WpsGetWpsIEParams (UINT4 u4WlanProfileIfIndex,
        UINT2 u2WtpInternalId, 
        tWpsIEElements* pWpsIEElements);
VOID wpsSendUpdateFromRsna(INT4 i4WlanIfIndex);
INT1 wpsSendEncrypUpdateFromRsna (INT4 i4IfIndex,
        UINT4
        u4Dot11RSNAConfigPairwiseCipherIndex,
        INT4
        i4SetValDot11RSNAConfigPairwiseCipherEnabled);
VOID wpsSendPskUpdateFromRsna(INT4 i4IfIndex);
INT1 WpsGetNextIndexFsWPSAuthSTAInfoTable(INT4 i4IfIndex, INT4 *pi4NextIfIndex,
        UINT4 u4FsWPSStatsIndex,
        UINT4 *pu4NextFsWPSStatsIndex);
VOID WpsUtilGetSuppStatsInfo (UINT2 u2PortNum, UINT4 u4SuppStatsIndex,
        tWpsStat ** ppWpsStat);
UINT4 WpsProcessWssNotification (tWssWpsNotifyParams * pWssWpsNotifyParams);
VOID wpsRecvIdentity(UINT2 u2PortNo, tMacAddr macAddr);
VOID wpsRecvEapPkt(UINT2 u2PortNo,
        tMacAddr macAddr,
        UINT1 *pu1PktPtr,
        UINT2 u2PktLen);
INT4 WssWpsSendDeAuthMsg (UINT1 *u1BssId, UINT1 *u1StaMac, UINT2 u2SessId);
UINT1 WssIfProcessWpsMsg(UINT4 eMsgType, tWssWpsNotifyParams *WssWpsNotifyParams);
VOID
WssWlanWpsDisablePushButton(VOID);
UINT1
WssWlanWpsDisablePushButtonAfterTimerExp(VOID);

#endif
