/*****************************************************************************/
/* Copyright (C) 2008 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2008                                               */
/* $Id: tac.h,v 1.7 2010/07/13 11:46:23 prabuc Exp $                                                                    */
/*****************************************************************************/
/*    FILE  NAME            : tac.h                                          */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : Transmission and Admission Control (TAC)       */
/*    MODULE NAME           : TAC module global Structures and definitions   */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains Data Structures, prototypes */
/*                            and definitions needed by other modules for    */
/*                            interfacing with TAC module                    */
/*---------------------------------------------------------------------------*/

#ifndef _TAC_H
#define _TAC_H

#include "utilipvx.h"

/* Failure and success */
#define TACM_SUCCESS                SUCCESS
#define TACM_FAILURE                FAILURE

/* Lock and UnLock */
#define TACM_LOCK()                 TacLock()
#define TACM_UNLOCK()               TacUnLock()

/* Permit and Deny */
#define TACM_PERMIT                 1
#define TACM_DENY                   2

/* Macro to be sent by other modules while adding/removing
 * profile mapping to an interface */
#define TACM_MAP                    1
#define TACM_UNMAP                  2

/* Maximum length for profile description */
#define TACM_MAX_DESCR_LEN          128
#define TACM_MAX_DESCR_ARRAY_LEN    TACM_MAX_DESCR_LEN + 4 

/* Macros to be used by other modules while filtering packet */
/* IGMP related macros */
#define TACM_IGMP_VERSION1          1
#define TACM_IGMP_VERSION2          2
#define TACM_IGMP_VERSION3          3

#define TACM_IGMP_GENERAL_QUERY     1
#define TACM_IGMP_GRP_QUERY         2
#define TACM_IGMP_GRP_SRC_QUERY     3
#define TACM_IGMP_REPORT            4
#define TACM_IGMP_LEAVE             5

/* Macros to distinguish the filter modes for group records in IGMPv3 reports */
#define TACM_IGMP_INCLUDE           1
#define TACM_IGMP_EXCLUDE           2
#define TACM_IGMP_ALLOW             3
#define TACM_IGMP_BLOCK             4

/* Macros for filter mode in filter entry */
#define TACM_INCLUDE                1
#define TACM_EXCLUDE                2
#define TACM_ANY                    3

/* True and False */
#define TACM_TRUE                   OSIX_TRUE
#define TACM_FALSE                  OSIX_FALSE

#define tTacmSll                    tTMO_SLL
#define tTacmSllNode                tTMO_SLL_NODE
                                          
/* Macros to enable or disable specific debug options */
#define TACM_FN_ENTRY               0x00000100
#define TACM_FN_EXIT                0x00000200
#define TACM_FILTER_TRC             0x00000400
#define TACM_CRITICAL_TRC           0x00000800
#define TACM_ALL_TRC               (TACM_FN_ENTRY     | \
                                    TACM_FN_EXIT      | \
                                    TACM_FILTER_TRC   | \
                                    TACM_CRITICAL_TRC | \
                                    INIT_SHUT_TRC     | \
                                    MGMT_TRC          | \
                                    CONTROL_PLANE_TRC | \
                                    OS_RESOURCE_TRC   | \
                                    ALL_FAILURE_TRC)

/* Packet types to be sent through the filter packet */
/* Structure used by other modules to filter the incoming packet */
typedef struct _TacFilterPkt
{
    tIPvXAddr           GroupAddr;       /* Group address along with the
                                          * internet address type in the
                                          * incoming packet               */
    UINT4               u4ProfileId;     /* Profile Id to be searched     */
    UINT4               u4NumSrc;        /* Number of sources             */
    UINT1               u1Version;       /* Packet version type           */
    UINT1               u1PktType;       /* Packet type                   */
    UINT1               u1Mode;          /* Packet sub type
                                          * For IGMPv3/MLDv2 this will
                                          * contain allow/include/exclude
                                          * or block                      */
    UINT1               au1Reserved[1];  /* Included for 4 byte alignment */
}tTacFilterPkt;

typedef struct _GrpRecords
{
    tTacmSll           *pGrpRecList;
    UINT4               u4NumOfMatchFound;
                                         /* Represents number of entries
                                          * matched in this profile       */
}tGrpRecords;

typedef struct _GrpSrcStatusEntry
{
    tTacmSllNode        NextGrpSrcStatusEntry;
    UINT4               u4GroupAddress;
    UINT4               u4SourceAddress;
    UINT1               u1RecordType;
    UINT1               u1Status;        /* This will be set to TRUE,
                                          * if the particular entry is
                                          * matched in the profile        */
    UINT1               au1Reserved[2];
}tGrpSrcStatusEntry;

typedef struct _TacmPrfStats
{
   UINT4                u4VlanRefCnt;    /* No of VLANs for which the
                                          * profile is mapped             */
   UINT4                u4IntfRefCnt;    /* No of ports for which the
                                          * profile is mapped             */
   UINT1                u1VlanInstRefCnt;/* No of VLANs in the given
                                          * instance for which the profile
                                          * is mapped                     */
   UINT1                au1Reserved[3];
}tTacmPrfStats;

typedef enum {
    TAC_ENABLED  = 1,
    TAC_DISABLED = 2
}tTacStatus;

/* Function prototypes */
/************************** tacmain.c ***************************************/
VOID TACMain (INT1 *);


/************************** tacapi.c ****************************************/
INT4 TacApiFilterChannel PROTO ((tTacFilterPkt * pTacFilterPkt,
                                 UINT1 * pu1SrcList));

INT4 TacApiUpdatePortRefCount
PROTO ((UINT4 u4ProfileId, UINT1 u1AddressType, UINT1 u1UpdtFlag));

INT4 TacApiUpdateVlanRefCount PROTO ((UINT4 u4Instance, UINT4 u4ProfileId,
                                      UINT1 u1AddressType, UINT1 u1UpdtFlag));

INT4
TacSearchGrpSrcRecordInProfile PROTO ((UINT4 u4ProfileId, tGrpRecords * pGrpRecords)); 

INT4 
TacSearchGrpAddressInProfile PROTO ((UINT4 u4ProfileId, UINT4 u4GrpAddr));

INT4 TacApiGetPrfStats PROTO ((UINT4 u4Instance, UINT4 u4ProfileId,
                               UINT1 u1AddressType,
                               tTacmPrfStats * pTacmPrfStats));

INT4 
TacApiGetPrfAction PROTO ((UINT4 u4ProfileId, UINT1 u1AddressType, 
                           UINT1  *pu1ProfileAction));


/************************** tacutil.c ***************************************/
INT4 TacLock PROTO ((VOID));

INT4 TacUnLock PROTO ((VOID));

#endif
