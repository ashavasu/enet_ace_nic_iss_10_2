/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: elm.h,v 1.8 2011/09/08 07:20:04 siva Exp $
 *
 * Description: This file contains all global variables used by
 *              RSTP and MSTP modules. 
 *
 *******************************************************************/


#ifndef _ELMGLOB_H_
#define _ELMGLOB_H_


#define  ELM_TASK_NAME                 "ElmT"
#define  ELM_TASK_PRIORITY             10
#define  ELM_QUEUE_NAME                ((UINT1*)"ElmQ")
#define  ELM_CFG_QUEUE                 ((UINT1*)"ElmC")
#define  ELM_FAILURE                         SNMP_FAILURE
#define  ELM_SUCCESS                         SNMP_SUCCESS
#define  ELM_NETWORK_SIDE                    2 
#define  ELM_CUSTOMER_SIDE                   1 
#define  ELM_SNMP_START                      1
#define  ELM_SNMP_SHUTDOWN                   2

#define  ELM_DATA                            L2_DATA_PKT

#define ELM_QUEUE_DEPTH  ELM_MAX_NUM_PORTS_SUPPORTED_IN_SYSTEM * 2
#define ELM_CFG_Q_DEPTH  ELM_MAX_NUM_PORTS_SUPPORTED_IN_SYSTEM * 2

#define ELM_LOCK()     ElmLock()
#define ELM_UNLOCK()   ElmUnLock()

INT4    ElmUpdatePortStatus (UINT4 u4IfIndex, UINT1 u1Status);

VOID    ElmTaskMain(INT1 *pi1Param);
INT4    ElmEvcStatusChangeIndication (UINT2 u2IfIndex,UINT2 u2EvcRefId, UINT1 u1Status);
INT4    ElmEvcInformationChangeIndication (UINT2 u2IfIndex);
INT4    ElmUniInformationChangeIndication (UINT2 u2IfIndex);
INT4    ElmCreatePort (UINT4 u4IfIndex);
INT4    ElmDeletePort (UINT4 u4IfIndex);
INT4    ElmHandleInFrame (tCRU_BUF_CHAIN_HEADER * pCruBuf, UINT2 u2IfIndex);
INT4    ElmGetPortMode (UINT4 u4IfIndex);
INT4    ElmLock(VOID);
INT4    ElmUnLock(VOID);
INT4    ElmMapPort(UINT2 u2IfIndex);
INT4    ElmUnMapPort(UINT2 u2IfIndex);
INT4    ElmModuleInit(VOID);
VOID    ElmModuleShutdown (VOID);
#ifdef L2RED_WANTED
INT4    ElmRedRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen);
INT4    ElmRestartModule (VOID);
#endif
/******************************************************************************/
/*            Required for CFM Stubs                                          */
/******************************************************************************/
#define CFM_MAX_LENGTH_DOMAIN_NAME     100

INT4    CfmCreateMA(UINT1 *pu1EvcId,UINT2 u2SVlan,UINT1 *pu1DomainName);
INT4    CfmCreateMeps(UINT1 *pu1EvcId,UINT2 u2NumberOfMeps);
INT4    CfmMepDown(VOID);
INT4    CfmFindDomain(UINT1 *pu1DomainName);
INT4    CfmInitialize(VOID);
/******************************************************************************/

#endif  /*_ELMGLOB_H_*/
