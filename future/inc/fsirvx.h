/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsirvx.h,v 1.17 2009/04/09 13:18:34 prabuc-iss Exp $ 
 *
 * Description:This file contains the common definitions    
 *             for the entire system for the vxworks env..
 *******************************************************************/
#ifndef _FSIRVX_H
#define _FSIRVX_H

/*Changed for porting to use vxWorks IpStack */
/* vxworks header files */
#include "stdio.h"
#include "sys/stat.h"

/* for strings */
#include <string.h>

/* FSAP2 includes */
/* -------------- */
#include "osxstd.h"
#include "srmbuf.h"
#include "osxsys.h"
#include "osxprot.h"
#include "srmmem.h"
#include "utlsll.h"
#include "utldll.h"
#include "srmtmr.h"
#include "utlhash.h"
#include "utltrc.h"
#include "utleeh.h"
#include "utlmacro.h"
#include "utlhdr.h"

/* SYSTEM SIZING includes */
/* ---------------------- */
#include "pack.h"
#include "size.h"
#include "trace.h"

#ifdef   __STDC__
#define  PROTO(x)     x
#else
#define  PROTO(x)     ()
#endif

#ifdef NONE
#undef NONE
#endif

#ifdef LOCAL
#undef LOCAL
#endif

/************** version info for the Linux Router Release *******************/
#define   MAJOR_VER    4
#define   MINOR_VER    0
/****************************************************************************/


/***************** Values used for Starting the System ***********************/

#define   SYS_MAX_TASKS       50
#define   SYS_MAX_QUEUES     100
#define   SYS_MAX_SEMS       200
/* The value SYS_NUM_OF_TIME_UNITS_IN_A_SEC below indicates the system's timer 
 * granularity and hence the polling frequency from the device drivers.
 *  100  --> 10ms polling
 *   10  --> 100ms polling
 *   1   --> 1sec polling
 */
#define   SYS_NUM_OF_TIME_UNITS_IN_A_SEC      sysClkRateGet()
#define   SYS_TIME_TICKS_IN_A_SEC             SYS_NUM_OF_TIME_UNITS_IN_A_SEC

#define   SYS_MAX_BUF_DESC                   100
#define   SYS_MAX_BUF_BLOCK_SIZE            2048
#define   SYS_MAX_NUM_OF_BUF_BLOCK           400
#define   SYS_MAX_NUM_OF_TIMER_LISTS          75 
#define   SYS_MAX_INTERFACES                  15

#define   SYS_SYSTEM_SIZING_FILENAME         "system.size"
/****************************************************************************/

/************** common definitions used in all modules **********************/
#define   SUCCESS     (0)
#define   FAILURE    (-1)
/****************************************************************************/

#define   UNUSED_PARAM(x)   ((void)x)

/* RowStatus values */
#define   ACTIVE             0x01
#define   NOT_IN_SERVICE     0x02
#define   NOT_READY          0x03
#define   CREATE_AND_GO      0x04
#define   CREATE_AND_WAIT    0x05
#define   DESTROY            0x06

/* SRM BUF Extension calls used by other protocols */
/*
 * Macros to access CRU BUF fields
 */

#define   CB_FDD(pMsg)            (pMsg->pFirstValidDataDesc)          
#define   CB_FB(pMsg)             ((CB_FDD(pMsg))->pu1_FirstByte)      
#define   CB_FVB(pMsg)            ((CB_FDD(pMsg))->pu1_FirstValidByte) 
#define   CB_VBC(pMsg)            (CB_FDD(pMsg)->u4_ValidByteCount)    
#define   CB_FBC(pMsg)            (CB_FDD(pMsg)->u4_FreeByteCount)     
#define   CB_READ_OFFSET(pMsg)    (pMsg->ModuleData.u4Reserved1)       
#define   CB_WRITE_OFFSET(pMsg)   (pMsg->ModuleData.u4Reserved2)       

/*
 * ASSIGN
 * updates VBC, FBC
 * does OSIX_HTON
 */
#define ASSIGN_1_BYTE(pMsg, u4Offset, u1Val)            \
do {                                                    \
    UINT4 u4OldVbc;                                     \
    UINT4 u4Added;                                      \
    *(UINT1 *)((UINT1 *)CB_FVB(pMsg)+u4Offset) = u1Val; \
    u4OldVbc = CB_VBC(pMsg);                            \
    if(u4Offset >= u4OldVbc) {                          \
        u4Added       = (u4Offset - u4OldVbc) + 1;      \
        CB_VBC(pMsg) += u4Added;                        \
        CB_FBC(pMsg)  = CB_FBC(pMsg) - u4Added;         \
   }                                                    \
}while(0)


#define ASSIGN_2_BYTE(pMsg, u4Offset, u2Val)                            \
do {                                                                    \
        UINT4 u4OldVbc;                                                 \
        UINT4 u4Added;                                                  \
        *(UINT2 *)((UINT1 *)CB_FVB(pMsg)+u4Offset) = OSIX_HTONS(u2Val); \
        u4OldVbc = CB_VBC(pMsg);                                        \
        if(u4Offset >= u4OldVbc) {                                      \
            u4Added       = (u4Offset - u4OldVbc) + 2;                  \
            CB_VBC(pMsg) += u4Added;                                    \
            CB_FBC(pMsg)  = CB_FBC(pMsg) - u4Added;                     \
   }                                                                    \
}while(0)

#define ASSIGN_4_BYTE(pMsg, u4Offset, u4Val)                        \
do {                                                                \
    UINT4 u4OldVbc;                                                 \
    UINT4 u4Added;                                                  \
    *(UINT4 *)((UINT1 *)CB_FVB(pMsg)+u4Offset) = OSIX_HTONL(u4Val); \
    u4OldVbc = CB_VBC(pMsg);                                        \
    if(u4Offset >= u4OldVbc) {                                      \
        u4Added       = (u4Offset - u4OldVbc) + 4;                  \
        CB_VBC(pMsg) += u4Added;                                    \
        CB_FBC(pMsg)  = CB_FBC(pMsg) - u4Added;                     \
   }                                                                \
}while(0)

#define ASSIGN_N_BYTE(pMsg, pu1Src, u4Offset, u4Len)              \
do{                                                               \
    UINT4 u4OldVbc;                                               \
    UINT4 u4Added;                                                \
    MEMCPY((VOID *)CB_FVB(pMsg)+u4Offset, (VOID *)pu1Src, u4Len); \
    u4OldVbc = CB_VBC(pMsg);                                      \
    if(u4Offset >= u4OldVbc) {                                    \
        u4Added       = (u4Offset - u4OldVbc) + u4Len;            \
        CB_VBC(pMsg) += u4Added;                                  \
        CB_FBC(pMsg)  = CB_FBC(pMsg) - u4Added;                   \
    }                                                             \
}while(0)

/*
 * APPEND
 * updates VBC, FBC
 * does OSIX_HTON
 */

#define APPEND_1_BYTE(pMsg, u1Val)                                   \
do {                                                                 \
    *(UINT1 *)((UINT1 *)CB_FVB(pMsg)+CB_WRITE_OFFSET(pMsg)) = u1Val; \
    if(CB_WRITE_OFFSET(pMsg) >= CB_VBC(pMsg)) {                      \
        CB_VBC(pMsg) ++;                                             \
        CB_FBC(pMsg) --;                                             \
   }                                                                 \
}while(0)

#define APPEND_2_BYTE(pMsg, u2Val)                                \
do {                                                              \
        *(UINT2 *)((UINT1 *)CB_FVB(pMsg)+CB_WRITE_OFFSET(pMsg)) = \
            OSIX_HTONS(u2Val);                                    \
        if(CB_WRITE_OFFSET(pMsg) >= CB_VBC(pMsg)) {               \
            CB_VBC(pMsg) += 2;                                    \
            CB_FBC(pMsg) -= 2;                                    \
   }                                                              \
}while(0)

#define APPEND_4_BYTE(pMsg, u4Val)                            \
do{                                                           \
    *(UINT4 *)((UINT1 *)CB_FVB(pMsg)+CB_WRITE_OFFSET(pMsg)) = \
        OSIX_HTONL(u4Val);                                    \
    if(CB_WRITE_OFFSET(pMsg) >= CB_VBC(pMsg)) {               \
        CB_VBC(pMsg) += 4;                                    \
        CB_FBC(pMsg) -= 4;                                    \
    }                                                         \
}while(0)

#define APPEND_N_BYTE(pMsg, pu1Src, u4Len)                        \
do{                                                               \
    MEMCPY((VOID *)((UINT1 *)CB_FVB(pMsg)+CB_WRITE_OFFSET(pMsg)), \
           (VOID *)pu1Src, u4Len);                                \
    if(CB_WRITE_OFFSET(pMsg) >= CB_VBC(pMsg)) {                   \
        CB_VBC(pMsg) += u4Len;                                    \
        CB_FBC(pMsg) -= u4Len;                                    \
    }                                                             \
}while(0)


/*
 * GET
 * does OSIX_NTOH.
 * no changes to state fields.
 */
#define GET_1_BYTE(pMsg, u4Offset, u1Val)\
        u1Val = *(UINT1 *)((UINT1 *)(CB_FVB(pMsg))+u4Offset)

#define GET_2_BYTE(pMsg, u4Offset, u2Val)                     \
do{                                                           \
        u2Val = *(UINT2 *)((UINT1 *)(CB_FVB(pMsg))+u4Offset); \
        u2Val = OSIX_NTOHS((u2Val));                            \
}while(0)

#define GET_4_BYTE(pMsg, u4Offset, u4Val)                     \
do{                                                           \
        u4Val = *(UINT4 *)((UINT1 *)(CB_FVB(pMsg))+u4Offset); \
        u4Val = OSIX_NTOHL(u4Val);                            \
}while(0)


#define GET_N_BYTE(pMsg, pu1Dest, u4Offset, u4Len)              \
        MEMCPY((VOID *)pu1Dest,                                 \
               (const VOID *)((UINT1 *)CB_FVB(pMsg)+u4Offset),  \
               u4Len)


/*
 * EXTRACT
 *
 * CAVEAT:
 * Users *SHOULD* update readoffset, for true fsap1 behaviour
 */

#define EXTRACT_1_BYTE(pMsg, u1Val) \
        u1Val = *(UINT1 *)((UINT1 *)CB_FVB(pMsg)+CB_READ_OFFSET(pMsg));

#define EXTRACT_2_BYTE(pMsg, u2Val)                                  \
do{                                                                  \
    u2Val = *(UINT2 *)((UINT1 *)CB_FVB(pMsg)+CB_READ_OFFSET(pMsg)) ; \
    u2Val = OSIX_NTOHS(u2Val);                                       \
}while(0)


#define EXTRACT_4_BYTE(pMsg, u4Val)                                  \
do{                                                                  \
    u4Val = *(UINT4 *)((UINT1 *)CB_FVB(pMsg)+CB_READ_OFFSET(pMsg)) ; \
    u4Val = OSIX_NTOHL(u4Val);                                       \
}while(0)

#define EXTRACT_N_BYTE(pMsg, pu1Dest, u4Len)                            \
do{                                                                     \
    MEMCPY((VOID *)pu1Dest,                                             \
    (const VOID *)((UINT1 *)CB_FVB(pMsg)+CB_READ_OFFSET(pMsg)), u4Len); \
}while(0)

#define CB_IS_LINEAR_READ(pBuf, u4Offset, u4Size)                \
    ((CRU_BUF_MoveToReadOffset(pBuf, u4Offset, &pTmpSrcDataDesc, \
                               &pu1SrcData) == CRU_SUCCESS) ? 1 : 0)

/* Returns true if pBuf has space for u4Size bytes of data starting from offset u4Offset.
*/
#define CB_IS_LINEAR_WRITE(pBuf, u4Offset, u4Size)          \
((CRU_BUF_MoveToWriteOffset(pBuf,u4Offset,&pTmpDstDataDesc, \
                            &pu1DstData,u4Size) == CRU_SUCCESS) ? 1 : 0)

typedef struct tMacAddress {
    UINT4  u4Dword;
    UINT2  u2Word;
} tMacAddress;

typedef UINT1 tMacAddr [6];

#endif
