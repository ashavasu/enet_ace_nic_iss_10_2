/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: custkern.h,
 *
 * Description: This file contains the controlling application specific 
 *              function implementations. The file can be ported specific to
 *              customer requirements.
 ****************************************************************************/

#ifndef __CUSTKERN_H_
#define __CUSTKERN_H_

INT4 CustKernInit PROTO ((VOID));
VOID CustKernDeInit PROTO ((VOID));

#endif
