/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: wlchdlr.h,v 1.10 2017/11/24 10:36:59 siva Exp $
 *
 * Description:This file contains the exported definitions and
 *             macros of CFA
 *
 *******************************************************************/
#ifndef _WLCHDLR_H
#define _WLCHDLR_H

#include "capwap.h"
#include "radioif.h"
#include "wssmac.h"

#define WLCHDLR_PKT_SIZE                1500
#define WLCHDLR_TASK_NAME                              "WLCH"
#define WLCHDLR_GET_SEQ_NUM_BIT    12
#define WLCHDLR_GET_MSG_TYPE_BIT   11
#define WLCHDLR_STATMR_EXP_EVENT            0x00000010
#define STA_REDIRECTION_URL_LENGTH 1100
#define WTP_DHCP_VEND_FIXED_LEN  34
#define WLCHDLR_CMN_ACCEPT_EVENT                0x00000040
#define WLCHDLR_CMN_REJECT_EVENT                0x00000080



#define WLCHDLR_STA_MEM_ALLOC(p) \
              p = (tWlcHdlrStaConfigDB *) MemAllocMemBlk (WLCHDLR_STA_NUM_POOLID); \
   /*PRINTF ("+++Function = %s ; Line No = %d Alloc - WLCHDLR_STA_MEM_ALLOC\n", __FUNCTION__, __LINE__);\*/

             
#define WLCHDLR_STA_MEM_RELEASE(p) MemReleaseMemBlock(WLCHDLR_STA_NUM_POOLID, (UINT1 *) p); \
/*  PRINTF ("---Function = %s ; Line No = %d Release - WLCHDLR_STA_NUM_POOLID\n", __FUNCTION__, __LINE__);*/


typedef struct {
    tCRU_BUF_CHAIN_DESC     *pRxPktBuf;

}tWlcHdlrPktBuf;

typedef struct{
    tRBNodeEmbd             nextWlcHdlrStaConfigDB;
    UINT4                   u4StaBufLen;
    UINT2                   u2WtpProfileId;
    UINT1                   u1SeqNum;
    UINT1                   au1Pad[1];
    UINT1                   au1StaBuf[WLCHDLR_PKT_SIZE];
}tWlcHdlrStaConfigDB;

typedef struct
{
    tCRU_BUF_CHAIN_HEADER  *pRcvBuf;
    UINT1                       *linearBuf;
    tIpAddr    capwapSessionId;
    UINT4    u4MsgType; /* Message Type */
    UINT4                       u4DestIp;
    UINT4                       u4DestPort;
    tMacAddr                    BssId;
    UINT2                       u2VlanId;
    UINT4                       u4IfIndex;
    UINT4                       u4BufSize;
    UINT2                       u2SessId;
    UINT1                       u1MacType; 
    UINT1                       u1Pad;
}tWlcHdlrQueueReq;

enum {

    /* Events processed by WLC HDLR Module */
    WSS_WLCHDLR_CAPWAP_CONF_UPDATE_REQ,
    WSS_WLCHDLR_CLKIWF_CONF_UPDATE_REQ,
    WSS_WLCHDLR_RADIO_CONF_UPDATE_REQ,
    WSS_WLCHDLR_PM_CONF_UPDATE_REQ,
    WSS_WLCHDLR_WLAN_CONF_REQ,
    WSS_WLCHDLR_STATION_CONF_REQ,
    WSS_WLCHDLR_CAPWAP_QUEUE_REQ,
    WSS_WLCHDLR_DB_INIT,
    WSS_WLCHDLR_DATA_TRANSFER_REQ,
    WSS_WLCHDLR_RESET_REQ,
    WSS_WLCHDLR_CLEAR_CONF_REQ,

    /* WSS MAC Messages */
    WSS_WLCHDLR_MAC_AUTH_MSG,
    WSS_WLCHDLR_MAC_DEAUTH_MSG,
    WSS_WLCHDLR_MAC_ASSOC_RSP,
    WSS_WLCHDLR_MAC_REASSOC_RSP,
    WSS_WLCHDLR_MAC_DISASSOC_MSG,
    WSS_WLCHDLR_MAC_ACTION_RSP,
#ifdef PMF_WANTED
    WSS_WLCHDLR_MAC_SAQUERY_ACTION_RSP,
    WSS_WLCHDLR_MAC_SAQUERY_ACTION_REQ,
#endif

    /* CAPWAP MAC Messages */
    WSS_WLCHDLR_CAPWAP_MAC_MSG,

    /* CFA MAC Messages */
    WSS_WLCHDLR_CFA_QUEUE_REQ,
    WSS_WLCHDLR_CFA_TX_BUF,

    /* WSS WLAN Messages */
    WSS_WLCHDLR_WLAN_PROBE_REQ,

    /* WSS STA Messages */
    WSS_WLCHDLR_STA_AUTH_MSG,
    WSS_WLCHDLR_STA_DEAUTH_MSG,
    WSS_WLCHDLR_STA_ASSOC_REQ,
    WSS_WLCHDLR_STA_REASSOC_REQ,
    WSS_WLCHDLR_STA_DISASSOC_MSG,
    WSS_WLCHDLR_STA_ACTION_REQ,

#ifdef RFMGMT_WANTED
    WSS_WLCHDLR_RFMGMT_CONF_UPDATE_REQ,
#endif
    WSS_WLCHDLR_DHCP_POOL_REQ

};

typedef union {
    tCapwapConfigUpdateReq         CapwapConfigUpdateReq;
    tClkiwfConfigUpdateReq         ClkiwfConfigUpdateReq;
    tRadioIfConfigUpdateReq        RadioIfConfigUpdateReq;
    tPmConfigUpdateReq             PmConfigUpdateReq;
    tWssWlanConfigReq              WssWlanConfigReq;
    tStationConfReq                StationConfReq;
    tWlcHdlrQueueReq               WlcHdlrQueueReq;
    tWssMacMsgStruct               WssMacMsgStruct; 
    tDataReq                       DataReq;
    tResetReq                      ResetReq;
    tClearconfigReq                ClearConfigReq;
#ifdef RFMGMT_WANTED
    tRfMgmtConfigUpdateReq         RfMgmtConfigUpdateReq;
#endif
}unWlcHdlrMsgStruct;


UINT4 WlcHandlerMainTaskInit (VOID);

INT4 WssIfProcessWlcHdlrMsg  (UINT1 MsgType, unWlcHdlrMsgStruct *pWssIfMsg);

INT4
WssIfBssidVlanMemberAction  (UINT4 u4IfIndex, UINT2 u2VlanId, UINT1 u1Action);

INT4
WlcHdlrProcessWssCFAMsg (UINT1 MsgType,unWlcHdlrMsgStruct *pWlcHdlrMsgStruct);

INT4 
WssGetSessIDFromIndex  (UINT4 u4IfIndex, UINT2 *pu2SessId);

INT4 WlchdlrGetStaRedirectionUrl(tMacAddr StaMacAddr , UINT1 *pu1StaRedirectionURL);
INT4  WssStaCompareSeqProfIdDBRBTree PROTO((tRBElem *, tRBElem *));
tWlcHdlrStaConfigDB * WlcHdlrStationDetailsGet (UINT2 u2ProfId, UINT1 u1SeqNum);
#ifdef CLKIWF_WANTED
INT4 WlcHdlrProcessWssClkUpdateMsg (INT4 i4TimeSource,
           tUtlSysPreciseTime *pSysPreciseTime, tUtlTm *pUtlTm);
#endif
INT4
CapwapUpdateKernelStaTable PROTO ((tMacAddr));


#endif

