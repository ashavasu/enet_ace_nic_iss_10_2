
/*  $Id: msdp.h,v 1.2 2011/01/20 09:42:26 siva Exp $ */
PUBLIC VOID MsdpTaskSpawnMsdpTask (INT4 *);
typedef struct
{
    tCRU_BUF_CHAIN_HEADER    *pDataPkt;
    tCRU_BUF_CHAIN_HEADER    *pGrpSrcAddrList;  /* GrpAddr & SrcAddr as tIPvXAddr */
    UINT2     u2SrcCount; /* pair count */
    UINT2     u2DataPktLen;
    UINT1     u1AddrType;
    UINT1     u1CompId;  /*mapping component ID */
    UINT1     u1Action; /* Add or delete */
    UINT1     u1DataTtl;
}tMsdpMrpSaAdvt;

typedef struct {
       tIPvXAddr    GrpAddr;
       UINT1        u1AddrType;
       UINT1        u1CompId;
       UINT1        au1Pad[2];
}tMsdpSAReqGrp;

PUBLIC INT4 MsdpPortHandleSAInfo (tMsdpMrpSaAdvt *);
PUBLIC INT4 MsdpPortHandleSARequest (tMsdpSAReqGrp *);
PUBLIC INT4 MsdpPortRegisterMrp (UINT1 u1ProtId, VOID (*) (tMsdpMrpSaAdvt *));
PUBLIC INT4 MsdpPortDeRegisterMrp (UINT1 u1ProtId);
