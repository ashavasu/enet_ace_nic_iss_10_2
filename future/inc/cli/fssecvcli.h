/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssecvcli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsipv6SecGlobalStatus[10];
extern UINT4 Fsipv6SecGlobalDebug[10];
extern UINT4 Fsipv6SecMaxSA[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsipv6SecGlobalStatus(i4SetValFsipv6SecGlobalStatus)	\
	nmhSetCmn(Fsipv6SecGlobalStatus, 10, Fsipv6SecGlobalStatusSet, IpSecv6Lock, IpSecv6UnLock, 0, 0, 0, "%i", i4SetValFsipv6SecGlobalStatus)
#define nmhSetFsipv6SecGlobalDebug(i4SetValFsipv6SecGlobalDebug)	\
	nmhSetCmn(Fsipv6SecGlobalDebug, 10, Fsipv6SecGlobalDebugSet, IpSecv6Lock, IpSecv6UnLock, 0, 0, 0, "%i", i4SetValFsipv6SecGlobalDebug)
#define nmhSetFsipv6SecMaxSA(i4SetValFsipv6SecMaxSA)	\
	nmhSetCmn(Fsipv6SecMaxSA, 10, Fsipv6SecMaxSASet, IpSecv6Lock, IpSecv6UnLock, 0, 0, 0, "%i", i4SetValFsipv6SecMaxSA)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsipv6SelIfIndex[12];
extern UINT4 Fsipv6SelProtoIndex[12];
extern UINT4 Fsipv6SelAccessIndex[12];
extern UINT4 Fsipv6SelPort[12];
extern UINT4 Fsipv6SelPktDirection[12];
extern UINT4 Fsipv6SelFilterFlag[12];
extern UINT4 Fsipv6SelPolicyIndex[12];
extern UINT4 Fsipv6SelIfIpAddress[12];
extern UINT4 Fsipv6SelStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsipv6SelFilterFlag(i4Fsipv6SelIfIndex , i4Fsipv6SelProtoIndex , i4Fsipv6SelAccessIndex , i4Fsipv6SelPort , i4Fsipv6SelPktDirection ,i4SetValFsipv6SelFilterFlag)	\
	nmhSetCmn(Fsipv6SelFilterFlag, 12, Fsipv6SelFilterFlagSet, IpSecv6Lock, IpSecv6UnLock, 0, 0, 5, "%i %i %i %i %i %i", i4Fsipv6SelIfIndex , i4Fsipv6SelProtoIndex , i4Fsipv6SelAccessIndex , i4Fsipv6SelPort , i4Fsipv6SelPktDirection ,i4SetValFsipv6SelFilterFlag)
#define nmhSetFsipv6SelPolicyIndex(i4Fsipv6SelIfIndex , i4Fsipv6SelProtoIndex , i4Fsipv6SelAccessIndex , i4Fsipv6SelPort , i4Fsipv6SelPktDirection ,i4SetValFsipv6SelPolicyIndex)	\
	nmhSetCmn(Fsipv6SelPolicyIndex, 12, Fsipv6SelPolicyIndexSet, IpSecv6Lock, IpSecv6UnLock, 0, 0, 5, "%i %i %i %i %i %i", i4Fsipv6SelIfIndex , i4Fsipv6SelProtoIndex , i4Fsipv6SelAccessIndex , i4Fsipv6SelPort , i4Fsipv6SelPktDirection ,i4SetValFsipv6SelPolicyIndex)
#define nmhSetFsipv6SelIfIpAddress(i4Fsipv6SelIfIndex , i4Fsipv6SelProtoIndex , i4Fsipv6SelAccessIndex , i4Fsipv6SelPort , i4Fsipv6SelPktDirection ,pSetValFsipv6SelIfIpAddress)	\
	nmhSetCmn(Fsipv6SelIfIpAddress, 12, Fsipv6SelIfIpAddressSet, IpSecv6Lock, IpSecv6UnLock, 0, 0, 5, "%i %i %i %i %i %s", i4Fsipv6SelIfIndex , i4Fsipv6SelProtoIndex , i4Fsipv6SelAccessIndex , i4Fsipv6SelPort , i4Fsipv6SelPktDirection ,pSetValFsipv6SelIfIpAddress)
#define nmhSetFsipv6SelStatus(i4Fsipv6SelIfIndex , i4Fsipv6SelProtoIndex , i4Fsipv6SelAccessIndex , i4Fsipv6SelPort , i4Fsipv6SelPktDirection ,i4SetValFsipv6SelStatus)	\
	nmhSetCmn(Fsipv6SelStatus, 12, Fsipv6SelStatusSet, IpSecv6Lock, IpSecv6UnLock, 0, 1, 5, "%i %i %i %i %i %i", i4Fsipv6SelIfIndex , i4Fsipv6SelProtoIndex , i4Fsipv6SelAccessIndex , i4Fsipv6SelPort , i4Fsipv6SelPktDirection ,i4SetValFsipv6SelStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsipv6SecAccessIndex[12];
extern UINT4 Fsipv6SecAccessStatus[12];
extern UINT4 Fsipv6SecSrcNet[12];
extern UINT4 Fsipv6SecSrcAddrPrefixLen[12];
extern UINT4 Fsipv6SecDestNet[12];
extern UINT4 Fsipv6SecDestAddrPrefixLen[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsipv6SecAccessStatus(i4Fsipv6SecAccessIndex ,i4SetValFsipv6SecAccessStatus)	\
	nmhSetCmn(Fsipv6SecAccessStatus, 12, Fsipv6SecAccessStatusSet, IpSecv6Lock, IpSecv6UnLock, 0, 1, 1, "%i %i", i4Fsipv6SecAccessIndex ,i4SetValFsipv6SecAccessStatus)
#define nmhSetFsipv6SecSrcNet(i4Fsipv6SecAccessIndex ,pSetValFsipv6SecSrcNet)	\
	nmhSetCmn(Fsipv6SecSrcNet, 12, Fsipv6SecSrcNetSet, IpSecv6Lock, IpSecv6UnLock, 0, 0, 1, "%i %s", i4Fsipv6SecAccessIndex ,pSetValFsipv6SecSrcNet)
#define nmhSetFsipv6SecSrcAddrPrefixLen(i4Fsipv6SecAccessIndex ,i4SetValFsipv6SecSrcAddrPrefixLen)	\
	nmhSetCmn(Fsipv6SecSrcAddrPrefixLen, 12, Fsipv6SecSrcAddrPrefixLenSet, IpSecv6Lock, IpSecv6UnLock, 0, 0, 1, "%i %i", i4Fsipv6SecAccessIndex ,i4SetValFsipv6SecSrcAddrPrefixLen)
#define nmhSetFsipv6SecDestNet(i4Fsipv6SecAccessIndex ,pSetValFsipv6SecDestNet)	\
	nmhSetCmn(Fsipv6SecDestNet, 12, Fsipv6SecDestNetSet, IpSecv6Lock, IpSecv6UnLock, 0, 0, 1, "%i %s", i4Fsipv6SecAccessIndex ,pSetValFsipv6SecDestNet)
#define nmhSetFsipv6SecDestAddrPrefixLen(i4Fsipv6SecAccessIndex ,i4SetValFsipv6SecDestAddrPrefixLen)	\
	nmhSetCmn(Fsipv6SecDestAddrPrefixLen, 12, Fsipv6SecDestAddrPrefixLenSet, IpSecv6Lock, IpSecv6UnLock, 0, 0, 1, "%i %i", i4Fsipv6SecAccessIndex ,i4SetValFsipv6SecDestAddrPrefixLen)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsipv6SecPolicyIndex[12];
extern UINT4 Fsipv6SecPolicyFlag[12];
extern UINT4 Fsipv6SecPolicyMode[12];
extern UINT4 Fsipv6SecPolicySaBundle[12];
extern UINT4 Fsipv6SecPolicyOptionsIndex[12];
extern UINT4 Fsipv6SecPolicyStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsipv6SecPolicyFlag(i4Fsipv6SecPolicyIndex ,i4SetValFsipv6SecPolicyFlag)	\
	nmhSetCmn(Fsipv6SecPolicyFlag, 12, Fsipv6SecPolicyFlagSet, IpSecv6Lock, IpSecv6UnLock, 0, 0, 1, "%i %i", i4Fsipv6SecPolicyIndex ,i4SetValFsipv6SecPolicyFlag)
#define nmhSetFsipv6SecPolicyMode(i4Fsipv6SecPolicyIndex ,i4SetValFsipv6SecPolicyMode)	\
	nmhSetCmn(Fsipv6SecPolicyMode, 12, Fsipv6SecPolicyModeSet, IpSecv6Lock, IpSecv6UnLock, 0, 0, 1, "%i %i", i4Fsipv6SecPolicyIndex ,i4SetValFsipv6SecPolicyMode)
#define nmhSetFsipv6SecPolicySaBundle(i4Fsipv6SecPolicyIndex ,pSetValFsipv6SecPolicySaBundle)	\
	nmhSetCmn(Fsipv6SecPolicySaBundle, 12, Fsipv6SecPolicySaBundleSet, IpSecv6Lock, IpSecv6UnLock, 0, 0, 1, "%i %s", i4Fsipv6SecPolicyIndex ,pSetValFsipv6SecPolicySaBundle)
#define nmhSetFsipv6SecPolicyOptionsIndex(i4Fsipv6SecPolicyIndex ,i4SetValFsipv6SecPolicyOptionsIndex)	\
	nmhSetCmn(Fsipv6SecPolicyOptionsIndex, 12, Fsipv6SecPolicyOptionsIndexSet, IpSecv6Lock, IpSecv6UnLock, 0, 0, 1, "%i %i", i4Fsipv6SecPolicyIndex ,i4SetValFsipv6SecPolicyOptionsIndex)
#define nmhSetFsipv6SecPolicyStatus(i4Fsipv6SecPolicyIndex ,i4SetValFsipv6SecPolicyStatus)	\
	nmhSetCmn(Fsipv6SecPolicyStatus, 12, Fsipv6SecPolicyStatusSet, IpSecv6Lock, IpSecv6UnLock, 0, 1, 1, "%i %i", i4Fsipv6SecPolicyIndex ,i4SetValFsipv6SecPolicyStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsipv6SecAssocIndex[12];
extern UINT4 Fsipv6SecAssocDstAddr[12];
extern UINT4 Fsipv6SecAssocProtocol[12];
extern UINT4 Fsipv6SecAssocSpi[12];
extern UINT4 Fsipv6SecAssocMode[12];
extern UINT4 Fsipv6SecAssocAhAlgo[12];
extern UINT4 Fsipv6SecAssocAhKey[12];
extern UINT4 Fsipv6SecAssocEspAlgo[12];
extern UINT4 Fsipv6SecAssocEspKey[12];
extern UINT4 Fsipv6SecAssocEspKey2[12];
extern UINT4 Fsipv6SecAssocEspKey3[12];
extern UINT4 Fsipv6SecAssocLifetimeInBytes[12];
extern UINT4 Fsipv6SecAssocLifetime[12];
extern UINT4 Fsipv6SecAssocAntiReplay[12];
extern UINT4 Fsipv6SecAssocStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsipv6SecAssocDstAddr(i4Fsipv6SecAssocIndex ,pSetValFsipv6SecAssocDstAddr)	\
	nmhSetCmn(Fsipv6SecAssocDstAddr, 12, Fsipv6SecAssocDstAddrSet, IpSecv6Lock, IpSecv6UnLock, 0, 0, 1, "%i %s", i4Fsipv6SecAssocIndex ,pSetValFsipv6SecAssocDstAddr)
#define nmhSetFsipv6SecAssocProtocol(i4Fsipv6SecAssocIndex ,i4SetValFsipv6SecAssocProtocol)	\
	nmhSetCmn(Fsipv6SecAssocProtocol, 12, Fsipv6SecAssocProtocolSet, IpSecv6Lock, IpSecv6UnLock, 0, 0, 1, "%i %i", i4Fsipv6SecAssocIndex ,i4SetValFsipv6SecAssocProtocol)
#define nmhSetFsipv6SecAssocSpi(i4Fsipv6SecAssocIndex ,i4SetValFsipv6SecAssocSpi)	\
	nmhSetCmn(Fsipv6SecAssocSpi, 12, Fsipv6SecAssocSpiSet, IpSecv6Lock, IpSecv6UnLock, 0, 0, 1, "%i %i", i4Fsipv6SecAssocIndex ,i4SetValFsipv6SecAssocSpi)
#define nmhSetFsipv6SecAssocMode(i4Fsipv6SecAssocIndex ,i4SetValFsipv6SecAssocMode)	\
	nmhSetCmn(Fsipv6SecAssocMode, 12, Fsipv6SecAssocModeSet, IpSecv6Lock, IpSecv6UnLock, 0, 0, 1, "%i %i", i4Fsipv6SecAssocIndex ,i4SetValFsipv6SecAssocMode)
#define nmhSetFsipv6SecAssocAhAlgo(i4Fsipv6SecAssocIndex ,i4SetValFsipv6SecAssocAhAlgo)	\
	nmhSetCmn(Fsipv6SecAssocAhAlgo, 12, Fsipv6SecAssocAhAlgoSet, IpSecv6Lock, IpSecv6UnLock, 0, 0, 1, "%i %i", i4Fsipv6SecAssocIndex ,i4SetValFsipv6SecAssocAhAlgo)
#define nmhSetFsipv6SecAssocAhKey(i4Fsipv6SecAssocIndex ,pSetValFsipv6SecAssocAhKey)	\
	nmhSetCmn(Fsipv6SecAssocAhKey, 12, Fsipv6SecAssocAhKeySet, IpSecv6Lock, IpSecv6UnLock, 0, 0, 1, "%i %s", i4Fsipv6SecAssocIndex ,pSetValFsipv6SecAssocAhKey)
#define nmhSetFsipv6SecAssocEspAlgo(i4Fsipv6SecAssocIndex ,i4SetValFsipv6SecAssocEspAlgo)	\
	nmhSetCmn(Fsipv6SecAssocEspAlgo, 12, Fsipv6SecAssocEspAlgoSet, IpSecv6Lock, IpSecv6UnLock, 0, 0, 1, "%i %i", i4Fsipv6SecAssocIndex ,i4SetValFsipv6SecAssocEspAlgo)
#define nmhSetFsipv6SecAssocEspKey(i4Fsipv6SecAssocIndex ,pSetValFsipv6SecAssocEspKey)	\
	nmhSetCmn(Fsipv6SecAssocEspKey, 12, Fsipv6SecAssocEspKeySet, IpSecv6Lock, IpSecv6UnLock, 0, 0, 1, "%i %s", i4Fsipv6SecAssocIndex ,pSetValFsipv6SecAssocEspKey)
#define nmhSetFsipv6SecAssocEspKey2(i4Fsipv6SecAssocIndex ,pSetValFsipv6SecAssocEspKey2)	\
	nmhSetCmn(Fsipv6SecAssocEspKey2, 12, Fsipv6SecAssocEspKey2Set, IpSecv6Lock, IpSecv6UnLock, 0, 0, 1, "%i %s", i4Fsipv6SecAssocIndex ,pSetValFsipv6SecAssocEspKey2)
#define nmhSetFsipv6SecAssocEspKey3(i4Fsipv6SecAssocIndex ,pSetValFsipv6SecAssocEspKey3)	\
	nmhSetCmn(Fsipv6SecAssocEspKey3, 12, Fsipv6SecAssocEspKey3Set, IpSecv6Lock, IpSecv6UnLock, 0, 0, 1, "%i %s", i4Fsipv6SecAssocIndex ,pSetValFsipv6SecAssocEspKey3)
#define nmhSetFsipv6SecAssocLifetimeInBytes(i4Fsipv6SecAssocIndex ,i4SetValFsipv6SecAssocLifetimeInBytes)	\
	nmhSetCmn(Fsipv6SecAssocLifetimeInBytes, 12, Fsipv6SecAssocLifetimeInBytesSet, IpSecv6Lock, IpSecv6UnLock, 0, 0, 1, "%i %i", i4Fsipv6SecAssocIndex ,i4SetValFsipv6SecAssocLifetimeInBytes)
#define nmhSetFsipv6SecAssocLifetime(i4Fsipv6SecAssocIndex ,i4SetValFsipv6SecAssocLifetime)	\
	nmhSetCmn(Fsipv6SecAssocLifetime, 12, Fsipv6SecAssocLifetimeSet, IpSecv6Lock, IpSecv6UnLock, 0, 0, 1, "%i %i", i4Fsipv6SecAssocIndex ,i4SetValFsipv6SecAssocLifetime)
#define nmhSetFsipv6SecAssocAntiReplay(i4Fsipv6SecAssocIndex ,i4SetValFsipv6SecAssocAntiReplay)	\
	nmhSetCmn(Fsipv6SecAssocAntiReplay, 12, Fsipv6SecAssocAntiReplaySet, IpSecv6Lock, IpSecv6UnLock, 0, 0, 1, "%i %i", i4Fsipv6SecAssocIndex ,i4SetValFsipv6SecAssocAntiReplay)
#define nmhSetFsipv6SecAssocStatus(i4Fsipv6SecAssocIndex ,i4SetValFsipv6SecAssocStatus)	\
	nmhSetCmn(Fsipv6SecAssocStatus, 12, Fsipv6SecAssocStatusSet, IpSecv6Lock, IpSecv6UnLock, 0, 1, 1, "%i %i", i4Fsipv6SecAssocIndex ,i4SetValFsipv6SecAssocStatus)

#endif
