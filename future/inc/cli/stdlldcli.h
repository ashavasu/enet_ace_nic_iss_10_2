/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdlldcli.h,v 1.3 2013/05/24 13:36:02 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 LldpMessageTxInterval[9];
extern UINT4 LldpMessageTxHoldMultiplier[9];
extern UINT4 LldpReinitDelay[9];
extern UINT4 LldpNotificationInterval[9];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetLldpMessageTxInterval(i4SetValLldpMessageTxInterval) \
 nmhSetCmn(LldpMessageTxInterval, 9, LldpMessageTxIntervalSet, LldpLock, LldpUnLock, 0, 0, 0, "%i", i4SetValLldpMessageTxInterval)
#define nmhSetLldpMessageTxHoldMultiplier(i4SetValLldpMessageTxHoldMultiplier) \
 nmhSetCmn(LldpMessageTxHoldMultiplier, 9, LldpMessageTxHoldMultiplierSet, LldpLock, LldpUnLock, 0, 0, 0, "%i", i4SetValLldpMessageTxHoldMultiplier)
#define nmhSetLldpReinitDelay(i4SetValLldpReinitDelay) \
 nmhSetCmn(LldpReinitDelay, 9, LldpReinitDelaySet, LldpLock, LldpUnLock, 0, 0, 0, "%i", i4SetValLldpReinitDelay)
#define nmhSetLldpNotificationInterval(i4SetValLldpNotificationInterval) \
 nmhSetCmn(LldpNotificationInterval, 9, LldpNotificationIntervalSet, LldpLock, LldpUnLock, 0, 0, 0, "%i", i4SetValLldpNotificationInterval)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 LldpPortConfigPortNum[11];
extern UINT4 LldpPortConfigAdminStatus[11];
extern UINT4 LldpPortConfigNotificationEnable[11];
extern UINT4 LldpPortConfigTLVsTxEnable[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetLldpPortConfigAdminStatus(i4LldpPortConfigPortNum ,i4SetValLldpPortConfigAdminStatus) \
 nmhSetCmn(LldpPortConfigAdminStatus, 11, LldpPortConfigAdminStatusSet, LldpLock, LldpUnLock, 0, 0, 1, "%i %i", i4LldpPortConfigPortNum ,i4SetValLldpPortConfigAdminStatus)
#define nmhSetLldpPortConfigNotificationEnable(i4LldpPortConfigPortNum ,i4SetValLldpPortConfigNotificationEnable) \
 nmhSetCmn(LldpPortConfigNotificationEnable, 11, LldpPortConfigNotificationEnableSet, LldpLock, LldpUnLock, 0, 0, 1, "%i %i", i4LldpPortConfigPortNum ,i4SetValLldpPortConfigNotificationEnable)
#define nmhSetLldpPortConfigTLVsTxEnable(i4LldpPortConfigPortNum ,pSetValLldpPortConfigTLVsTxEnable) \
 nmhSetCmn(LldpPortConfigTLVsTxEnable, 11, LldpPortConfigTLVsTxEnableSet, LldpLock, LldpUnLock, 0, 0, 1, "%i %s", i4LldpPortConfigPortNum ,pSetValLldpPortConfigTLVsTxEnable)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 LldpConfigManAddrPortsTxEnable[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetLldpConfigManAddrPortsTxEnable(i4LldpLocManAddrSubtype , pLldpLocManAddr ,pSetValLldpConfigManAddrPortsTxEnable) \
 nmhSetCmn(LldpConfigManAddrPortsTxEnable, 11, LldpConfigManAddrPortsTxEnableSet, LldpLock, LldpUnLock, 0, 0, 2, "%i %s %s", i4LldpLocManAddrSubtype , pLldpLocManAddr ,pSetValLldpConfigManAddrPortsTxEnable)

#endif
