/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsoteacli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsOspfTeSasConstraintId[13];
extern UINT4 FsOspfTeSasConstraintSourceIpAddr[13];
extern UINT4 FsOspfTeSasConstraintDestinationIpAddr[13];
extern UINT4 FsOspfTeSasConstraintWPSourceIpAddr[13];
extern UINT4 FsOspfTeSasConstraintWPDestinationIpAddr[13];
extern UINT4 FsOspfTeSasConstraintMaxPathMetric[13];
extern UINT4 FsOspfTeSasConstraintMaxHopsInPath[13];
extern UINT4 FsOspfTeSasConstraintBw[13];
extern UINT4 FsOspfTeSasConstraintIncludeAllSet[13];
extern UINT4 FsOspfTeSasConstraintIncludeAnySet[13];
extern UINT4 FsOspfTeSasConstraintExcludeAnySet[13];
extern UINT4 FsOspfTeSasConstraintPriority[13];
extern UINT4 FsOspfTeSasConstraintExplicitRoute[13];
extern UINT4 FsOspfTeSasConstraintSwitchingCapability[13];
extern UINT4 FsOspfTeSasConstraintEncodingType[13];
extern UINT4 FsOspfTeSasConstraintLinkProtectionType[13];
extern UINT4 FsOspfTeSasConstraintDiversity[13];
extern UINT4 FsOspfTeSasConstraintIndication[13];
extern UINT4 FsOspfTeSasConstraintFlag[13];
extern UINT4 FsOspfTeSasConstraintStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsOspfTeSasConstraintSourceIpAddr(i4FsOspfTeSasConstraintId ,u4SetValFsOspfTeSasConstraintSourceIpAddr)	\
	nmhSetCmn(FsOspfTeSasConstraintSourceIpAddr, 13, FsOspfTeSasConstraintSourceIpAddrSet, NULL, NULL, 0, 0, 1, "%i %p", i4FsOspfTeSasConstraintId ,u4SetValFsOspfTeSasConstraintSourceIpAddr)
#define nmhSetFsOspfTeSasConstraintDestinationIpAddr(i4FsOspfTeSasConstraintId ,u4SetValFsOspfTeSasConstraintDestinationIpAddr)	\
	nmhSetCmn(FsOspfTeSasConstraintDestinationIpAddr, 13, FsOspfTeSasConstraintDestinationIpAddrSet, NULL, NULL, 0, 0, 1, "%i %p", i4FsOspfTeSasConstraintId ,u4SetValFsOspfTeSasConstraintDestinationIpAddr)
#define nmhSetFsOspfTeSasConstraintWPSourceIpAddr(i4FsOspfTeSasConstraintId ,u4SetValFsOspfTeSasConstraintWPSourceIpAddr)	\
	nmhSetCmn(FsOspfTeSasConstraintWPSourceIpAddr, 13, FsOspfTeSasConstraintWPSourceIpAddrSet, NULL, NULL, 0, 0, 1, "%i %p", i4FsOspfTeSasConstraintId ,u4SetValFsOspfTeSasConstraintWPSourceIpAddr)
#define nmhSetFsOspfTeSasConstraintWPDestinationIpAddr(i4FsOspfTeSasConstraintId ,u4SetValFsOspfTeSasConstraintWPDestinationIpAddr)	\
	nmhSetCmn(FsOspfTeSasConstraintWPDestinationIpAddr, 13, FsOspfTeSasConstraintWPDestinationIpAddrSet, NULL, NULL, 0, 0, 1, "%i %p", i4FsOspfTeSasConstraintId ,u4SetValFsOspfTeSasConstraintWPDestinationIpAddr)
#define nmhSetFsOspfTeSasConstraintMaxPathMetric(i4FsOspfTeSasConstraintId ,i4SetValFsOspfTeSasConstraintMaxPathMetric)	\
	nmhSetCmn(FsOspfTeSasConstraintMaxPathMetric, 13, FsOspfTeSasConstraintMaxPathMetricSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsOspfTeSasConstraintId ,i4SetValFsOspfTeSasConstraintMaxPathMetric)
#define nmhSetFsOspfTeSasConstraintMaxHopsInPath(i4FsOspfTeSasConstraintId ,i4SetValFsOspfTeSasConstraintMaxHopsInPath)	\
	nmhSetCmn(FsOspfTeSasConstraintMaxHopsInPath, 13, FsOspfTeSasConstraintMaxHopsInPathSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsOspfTeSasConstraintId ,i4SetValFsOspfTeSasConstraintMaxHopsInPath)
#define nmhSetFsOspfTeSasConstraintBw(i4FsOspfTeSasConstraintId ,i4SetValFsOspfTeSasConstraintBw)	\
	nmhSetCmn(FsOspfTeSasConstraintBw, 13, FsOspfTeSasConstraintBwSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsOspfTeSasConstraintId ,i4SetValFsOspfTeSasConstraintBw)
#define nmhSetFsOspfTeSasConstraintIncludeAllSet(i4FsOspfTeSasConstraintId ,i4SetValFsOspfTeSasConstraintIncludeAllSet)	\
	nmhSetCmn(FsOspfTeSasConstraintIncludeAllSet, 13, FsOspfTeSasConstraintIncludeAllSetSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsOspfTeSasConstraintId ,i4SetValFsOspfTeSasConstraintIncludeAllSet)
#define nmhSetFsOspfTeSasConstraintIncludeAnySet(i4FsOspfTeSasConstraintId ,i4SetValFsOspfTeSasConstraintIncludeAnySet)	\
	nmhSetCmn(FsOspfTeSasConstraintIncludeAnySet, 13, FsOspfTeSasConstraintIncludeAnySetSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsOspfTeSasConstraintId ,i4SetValFsOspfTeSasConstraintIncludeAnySet)
#define nmhSetFsOspfTeSasConstraintExcludeAnySet(i4FsOspfTeSasConstraintId ,i4SetValFsOspfTeSasConstraintExcludeAnySet)	\
	nmhSetCmn(FsOspfTeSasConstraintExcludeAnySet, 13, FsOspfTeSasConstraintExcludeAnySetSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsOspfTeSasConstraintId ,i4SetValFsOspfTeSasConstraintExcludeAnySet)
#define nmhSetFsOspfTeSasConstraintPriority(i4FsOspfTeSasConstraintId ,i4SetValFsOspfTeSasConstraintPriority)	\
	nmhSetCmn(FsOspfTeSasConstraintPriority, 13, FsOspfTeSasConstraintPrioritySet, NULL, NULL, 0, 0, 1, "%i %i", i4FsOspfTeSasConstraintId ,i4SetValFsOspfTeSasConstraintPriority)
#define nmhSetFsOspfTeSasConstraintExplicitRoute(i4FsOspfTeSasConstraintId ,pSetValFsOspfTeSasConstraintExplicitRoute)	\
	nmhSetCmn(FsOspfTeSasConstraintExplicitRoute, 13, FsOspfTeSasConstraintExplicitRouteSet, NULL, NULL, 0, 0, 1, "%i %s", i4FsOspfTeSasConstraintId ,pSetValFsOspfTeSasConstraintExplicitRoute)
#define nmhSetFsOspfTeSasConstraintSwitchingCapability(i4FsOspfTeSasConstraintId ,i4SetValFsOspfTeSasConstraintSwitchingCapability)	\
	nmhSetCmn(FsOspfTeSasConstraintSwitchingCapability, 13, FsOspfTeSasConstraintSwitchingCapabilitySet, NULL, NULL, 0, 0, 1, "%i %i", i4FsOspfTeSasConstraintId ,i4SetValFsOspfTeSasConstraintSwitchingCapability)
#define nmhSetFsOspfTeSasConstraintEncodingType(i4FsOspfTeSasConstraintId ,i4SetValFsOspfTeSasConstraintEncodingType)	\
	nmhSetCmn(FsOspfTeSasConstraintEncodingType, 13, FsOspfTeSasConstraintEncodingTypeSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsOspfTeSasConstraintId ,i4SetValFsOspfTeSasConstraintEncodingType)
#define nmhSetFsOspfTeSasConstraintLinkProtectionType(i4FsOspfTeSasConstraintId ,i4SetValFsOspfTeSasConstraintLinkProtectionType)	\
	nmhSetCmn(FsOspfTeSasConstraintLinkProtectionType, 13, FsOspfTeSasConstraintLinkProtectionTypeSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsOspfTeSasConstraintId ,i4SetValFsOspfTeSasConstraintLinkProtectionType)
#define nmhSetFsOspfTeSasConstraintDiversity(i4FsOspfTeSasConstraintId ,i4SetValFsOspfTeSasConstraintDiversity)	\
	nmhSetCmn(FsOspfTeSasConstraintDiversity, 13, FsOspfTeSasConstraintDiversitySet, NULL, NULL, 0, 0, 1, "%i %i", i4FsOspfTeSasConstraintId ,i4SetValFsOspfTeSasConstraintDiversity)
#define nmhSetFsOspfTeSasConstraintIndication(i4FsOspfTeSasConstraintId ,i4SetValFsOspfTeSasConstraintIndication)	\
	nmhSetCmn(FsOspfTeSasConstraintIndication, 13, FsOspfTeSasConstraintIndicationSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsOspfTeSasConstraintId ,i4SetValFsOspfTeSasConstraintIndication)
#define nmhSetFsOspfTeSasConstraintFlag(i4FsOspfTeSasConstraintId ,i4SetValFsOspfTeSasConstraintFlag)	\
	nmhSetCmn(FsOspfTeSasConstraintFlag, 13, FsOspfTeSasConstraintFlagSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsOspfTeSasConstraintId ,i4SetValFsOspfTeSasConstraintFlag)
#define nmhSetFsOspfTeSasConstraintStatus(i4FsOspfTeSasConstraintId ,i4SetValFsOspfTeSasConstraintStatus)	\
	nmhSetCmn(FsOspfTeSasConstraintStatus, 13, FsOspfTeSasConstraintStatusSet, NULL, NULL, 0, 1, 1, "%i %i", i4FsOspfTeSasConstraintId ,i4SetValFsOspfTeSasConstraintStatus)

#endif
