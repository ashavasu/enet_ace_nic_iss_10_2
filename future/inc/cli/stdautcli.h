/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: stdautcli.h,v 1.1 2014/12/16 10:51:24 siva Exp $
*
* Description: Header file for extern declarations of the mib objects in stdauth.mib
*********************************************************************/
#ifndef __STDAUTCLI_H__
#define __STDAUTCLI_H__

/* extern declaration corresponding to OID variable present in protocol stdauthdb.h */
extern UINT4 PppSecuritySecretsLink[12];
extern UINT4 PppSecuritySecretsIdIndex[12];
extern UINT4 PppSecuritySecretsDirection[12];
extern UINT4 PppSecuritySecretsProtocol[12];
extern UINT4 PppSecuritySecretsIdentity[12];
extern UINT4 PppSecuritySecretsSecret[12];
extern UINT4 PppSecuritySecretsStatus[12];

#endif /*__STDAUTCLI_H__*/
