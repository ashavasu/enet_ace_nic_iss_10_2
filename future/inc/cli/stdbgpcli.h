/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdbgpcli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 BgpPeerAdminStatus[10];
extern UINT4 BgpPeerRemoteAddr[10];
extern UINT4 BgpPeerConnectRetryInterval[10];
extern UINT4 BgpPeerHoldTimeConfigured[10];
extern UINT4 BgpPeerKeepAliveConfigured[10];
extern UINT4 BgpPeerMinASOriginationInterval[10];
extern UINT4 BgpPeerMinRouteAdvertisementInterval[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetBgpPeerAdminStatus(u4BgpPeerRemoteAddr ,i4SetValBgpPeerAdminStatus)	\
	nmhSetCmn(BgpPeerAdminStatus, 10, BgpPeerAdminStatusSet, BgpLock, BgpUnLock, 0, 0, 1, "%p %i", u4BgpPeerRemoteAddr ,i4SetValBgpPeerAdminStatus)
#define nmhSetBgpPeerConnectRetryInterval(u4BgpPeerRemoteAddr ,i4SetValBgpPeerConnectRetryInterval)	\
	nmhSetCmn(BgpPeerConnectRetryInterval, 10, BgpPeerConnectRetryIntervalSet, BgpLock, BgpUnLock, 0, 0, 1, "%p %i", u4BgpPeerRemoteAddr ,i4SetValBgpPeerConnectRetryInterval)
#define nmhSetBgpPeerHoldTimeConfigured(u4BgpPeerRemoteAddr ,i4SetValBgpPeerHoldTimeConfigured)	\
	nmhSetCmn(BgpPeerHoldTimeConfigured, 10, BgpPeerHoldTimeConfiguredSet, BgpLock, BgpUnLock, 0, 0, 1, "%p %i", u4BgpPeerRemoteAddr ,i4SetValBgpPeerHoldTimeConfigured)
#define nmhSetBgpPeerKeepAliveConfigured(u4BgpPeerRemoteAddr ,i4SetValBgpPeerKeepAliveConfigured)	\
	nmhSetCmn(BgpPeerKeepAliveConfigured, 10, BgpPeerKeepAliveConfiguredSet, BgpLock, BgpUnLock, 0, 0, 1, "%p %i", u4BgpPeerRemoteAddr ,i4SetValBgpPeerKeepAliveConfigured)
#define nmhSetBgpPeerMinASOriginationInterval(u4BgpPeerRemoteAddr ,i4SetValBgpPeerMinASOriginationInterval)	\
	nmhSetCmn(BgpPeerMinASOriginationInterval, 10, BgpPeerMinASOriginationIntervalSet, BgpLock, BgpUnLock, 0, 0, 1, "%p %i", u4BgpPeerRemoteAddr ,i4SetValBgpPeerMinASOriginationInterval)
#define nmhSetBgpPeerMinRouteAdvertisementInterval(u4BgpPeerRemoteAddr ,i4SetValBgpPeerMinRouteAdvertisementInterval)	\
	nmhSetCmn(BgpPeerMinRouteAdvertisementInterval, 10, BgpPeerMinRouteAdvertisementIntervalSet, BgpLock, BgpUnLock, 0, 0, 1, "%p %i", u4BgpPeerRemoteAddr ,i4SetValBgpPeerMinRouteAdvertisementInterval)

#endif
