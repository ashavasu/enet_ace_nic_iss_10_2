/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsissdcli.h,v 1.1.1.1 2009/01/08 14:34:38 prabuc-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDsSystemControl[11];
extern UINT4 FsDsStatus[11];
#define nmhSetFsDsSystemControl(i4SetValFsDsSystemControl)	\
	nmhSetCmn(FsDsSystemControl, 11, FsDsSystemControlSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsDsSystemControl)
#define nmhSetFsDsStatus(i4SetValFsDsStatus)	\
	nmhSetCmn(FsDsStatus, 11, FsDsStatusSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsDsStatus)


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDiffServMultiFieldClfrId[13];
extern UINT4 FsDiffServMultiFieldClfrFilterId[13];
extern UINT4 FsDiffServMultiFieldClfrFilterType[13];
extern UINT4 FsDiffServMultiFieldClfrStatus[13];
#define nmhSetFsDiffServMultiFieldClfrFilterId(i4FsDiffServMultiFieldClfrId ,u4SetValFsDiffServMultiFieldClfrFilterId)	\
	nmhSetCmn(FsDiffServMultiFieldClfrFilterId, 13, FsDiffServMultiFieldClfrFilterIdSet, NULL, NULL, 0, 0, 1, "%i %u", i4FsDiffServMultiFieldClfrId ,u4SetValFsDiffServMultiFieldClfrFilterId)
#define nmhSetFsDiffServMultiFieldClfrFilterType(i4FsDiffServMultiFieldClfrId ,i4SetValFsDiffServMultiFieldClfrFilterType)	\
	nmhSetCmn(FsDiffServMultiFieldClfrFilterType, 13, FsDiffServMultiFieldClfrFilterTypeSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsDiffServMultiFieldClfrId ,i4SetValFsDiffServMultiFieldClfrFilterType)
#define nmhSetFsDiffServMultiFieldClfrStatus(i4FsDiffServMultiFieldClfrId ,i4SetValFsDiffServMultiFieldClfrStatus)	\
	nmhSetCmn(FsDiffServMultiFieldClfrStatus, 13, FsDiffServMultiFieldClfrStatusSet, NULL, NULL, 0, 1, 1, "%i %i", i4FsDiffServMultiFieldClfrId ,i4SetValFsDiffServMultiFieldClfrStatus)


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDiffServClfrId[13];
extern UINT4 FsDiffServClfrMFClfrId[13];
extern UINT4 FsDiffServClfrInProActionId[13];
extern UINT4 FsDiffServClfrOutProActionId[13];
extern UINT4 FsDiffServClfrStatus[13];
#define nmhSetFsDiffServClfrMFClfrId(i4FsDiffServClfrId ,i4SetValFsDiffServClfrMFClfrId)	\
	nmhSetCmn(FsDiffServClfrMFClfrId, 13, FsDiffServClfrMFClfrIdSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsDiffServClfrId ,i4SetValFsDiffServClfrMFClfrId)
#define nmhSetFsDiffServClfrInProActionId(i4FsDiffServClfrId ,i4SetValFsDiffServClfrInProActionId)	\
	nmhSetCmn(FsDiffServClfrInProActionId, 13, FsDiffServClfrInProActionIdSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsDiffServClfrId ,i4SetValFsDiffServClfrInProActionId)
#define nmhSetFsDiffServClfrOutProActionId(i4FsDiffServClfrId ,i4SetValFsDiffServClfrOutProActionId)	\
	nmhSetCmn(FsDiffServClfrOutProActionId, 13, FsDiffServClfrOutProActionIdSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsDiffServClfrId ,i4SetValFsDiffServClfrOutProActionId)
#define nmhSetFsDiffServClfrStatus(i4FsDiffServClfrId ,i4SetValFsDiffServClfrStatus)	\
	nmhSetCmn(FsDiffServClfrStatus, 13, FsDiffServClfrStatusSet, NULL, NULL, 0, 1, 1, "%i %i", i4FsDiffServClfrId ,i4SetValFsDiffServClfrStatus)


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDiffServInProfileActionId[13];
extern UINT4 FsDiffServInProfileActionFlag[13];
extern UINT4 FsDiffServInProfileActionNewPrio[13];
extern UINT4 FsDiffServInProfileActionIpTOS[13];
extern UINT4 FsDiffServInProfileActionPort[13];
extern UINT4 FsDiffServInProfileActionDscp[13];
extern UINT4 FsDiffServInProfileActionStatus[13];
#define nmhSetFsDiffServInProfileActionFlag(i4FsDiffServInProfileActionId ,u4SetValFsDiffServInProfileActionFlag)	\
	nmhSetCmn(FsDiffServInProfileActionFlag, 13, FsDiffServInProfileActionFlagSet, NULL, NULL, 0, 0, 1, "%i %u", i4FsDiffServInProfileActionId ,u4SetValFsDiffServInProfileActionFlag)
#define nmhSetFsDiffServInProfileActionNewPrio(i4FsDiffServInProfileActionId ,u4SetValFsDiffServInProfileActionNewPrio)	\
	nmhSetCmn(FsDiffServInProfileActionNewPrio, 13, FsDiffServInProfileActionNewPrioSet, NULL, NULL, 0, 0, 1, "%i %u", i4FsDiffServInProfileActionId ,u4SetValFsDiffServInProfileActionNewPrio)
#define nmhSetFsDiffServInProfileActionIpTOS(i4FsDiffServInProfileActionId ,u4SetValFsDiffServInProfileActionIpTOS)	\
	nmhSetCmn(FsDiffServInProfileActionIpTOS, 13, FsDiffServInProfileActionIpTOSSet, NULL, NULL, 0, 0, 1, "%i %u", i4FsDiffServInProfileActionId ,u4SetValFsDiffServInProfileActionIpTOS)
#define nmhSetFsDiffServInProfileActionPort(i4FsDiffServInProfileActionId ,u4SetValFsDiffServInProfileActionPort)	\
	nmhSetCmn(FsDiffServInProfileActionPort, 13, FsDiffServInProfileActionPortSet, NULL, NULL, 0, 0, 1, "%i %u", i4FsDiffServInProfileActionId ,u4SetValFsDiffServInProfileActionPort)
#define nmhSetFsDiffServInProfileActionDscp(i4FsDiffServInProfileActionId ,u4SetValFsDiffServInProfileActionDscp)	\
	nmhSetCmn(FsDiffServInProfileActionDscp, 13, FsDiffServInProfileActionDscpSet, NULL, NULL, 0, 0, 1, "%i %u", i4FsDiffServInProfileActionId ,u4SetValFsDiffServInProfileActionDscp)
#define nmhSetFsDiffServInProfileActionStatus(i4FsDiffServInProfileActionId ,i4SetValFsDiffServInProfileActionStatus)	\
	nmhSetCmn(FsDiffServInProfileActionStatus, 13, FsDiffServInProfileActionStatusSet, NULL, NULL, 0, 1, 1, "%i %i", i4FsDiffServInProfileActionId ,i4SetValFsDiffServInProfileActionStatus)


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDiffServOutProfileActionId[13];
extern UINT4 FsDiffServOutProfileActionFlag[13];
extern UINT4 FsDiffServOutProfileActionDscp[13];
extern UINT4 FsDiffServOutProfileActionMID[13];
extern UINT4 FsDiffServOutProfileActionStatus[13];
#define nmhSetFsDiffServOutProfileActionFlag(i4FsDiffServOutProfileActionId ,u4SetValFsDiffServOutProfileActionFlag)	\
	nmhSetCmn(FsDiffServOutProfileActionFlag, 13, FsDiffServOutProfileActionFlagSet, NULL, NULL, 0, 0, 1, "%i %u", i4FsDiffServOutProfileActionId ,u4SetValFsDiffServOutProfileActionFlag)
#define nmhSetFsDiffServOutProfileActionDscp(i4FsDiffServOutProfileActionId ,u4SetValFsDiffServOutProfileActionDscp)	\
	nmhSetCmn(FsDiffServOutProfileActionDscp, 13, FsDiffServOutProfileActionDscpSet, NULL, NULL, 0, 0, 1, "%i %u", i4FsDiffServOutProfileActionId ,u4SetValFsDiffServOutProfileActionDscp)
#define nmhSetFsDiffServOutProfileActionMID(i4FsDiffServOutProfileActionId ,i4SetValFsDiffServOutProfileActionMID)	\
	nmhSetCmn(FsDiffServOutProfileActionMID, 13, FsDiffServOutProfileActionMIDSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsDiffServOutProfileActionId ,i4SetValFsDiffServOutProfileActionMID)
#define nmhSetFsDiffServOutProfileActionStatus(i4FsDiffServOutProfileActionId ,i4SetValFsDiffServOutProfileActionStatus)	\
	nmhSetCmn(FsDiffServOutProfileActionStatus, 13, FsDiffServOutProfileActionStatusSet, NULL, NULL, 0, 1, 1, "%i %i", i4FsDiffServOutProfileActionId ,i4SetValFsDiffServOutProfileActionStatus)


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDiffServMeterId[13];
extern UINT4 FsDiffServMetertokenSize[13];
extern UINT4 FsDiffServMeterRefreshCount[13];
extern UINT4 FsDiffServMeterStatus[13];
#define nmhSetFsDiffServMetertokenSize(i4FsDiffServMeterId ,u4SetValFsDiffServMetertokenSize)	\
	nmhSetCmn(FsDiffServMetertokenSize, 13, FsDiffServMetertokenSizeSet, NULL, NULL, 0, 0, 1, "%i %u", i4FsDiffServMeterId ,u4SetValFsDiffServMetertokenSize)
#define nmhSetFsDiffServMeterRefreshCount(i4FsDiffServMeterId ,u4SetValFsDiffServMeterRefreshCount)	\
	nmhSetCmn(FsDiffServMeterRefreshCount, 13, FsDiffServMeterRefreshCountSet, NULL, NULL, 0, 0, 1, "%i %u", i4FsDiffServMeterId ,u4SetValFsDiffServMeterRefreshCount)
#define nmhSetFsDiffServMeterStatus(i4FsDiffServMeterId ,i4SetValFsDiffServMeterStatus)	\
	nmhSetCmn(FsDiffServMeterStatus, 13, FsDiffServMeterStatusSet, NULL, NULL, 0, 1, 1, "%i %i", i4FsDiffServMeterId ,i4SetValFsDiffServMeterStatus)


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDiffServSchedulerId[13];
extern UINT4 FsDiffServSchedulerDPId[13];
extern UINT4 FsDiffServSchedulerQueueCount[13];
extern UINT4 FsDiffServSchedulerWeight[13];
extern UINT4 FsDiffServSchedulerStatus[13];
#define nmhSetFsDiffServSchedulerDPId(i4FsDiffServSchedulerId ,i4SetValFsDiffServSchedulerDPId)	\
	nmhSetCmn(FsDiffServSchedulerDPId, 13, FsDiffServSchedulerDPIdSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsDiffServSchedulerId ,i4SetValFsDiffServSchedulerDPId)
#define nmhSetFsDiffServSchedulerQueueCount(i4FsDiffServSchedulerId ,u4SetValFsDiffServSchedulerQueueCount)	\
	nmhSetCmn(FsDiffServSchedulerQueueCount, 13, FsDiffServSchedulerQueueCountSet, NULL, NULL, 0, 0, 1, "%i %u", i4FsDiffServSchedulerId ,u4SetValFsDiffServSchedulerQueueCount)
#define nmhSetFsDiffServSchedulerWeight(i4FsDiffServSchedulerId ,pSetValFsDiffServSchedulerWeight)	\
	nmhSetCmn(FsDiffServSchedulerWeight, 13, FsDiffServSchedulerWeightSet, NULL, NULL, 0, 0, 1, "%i %s", i4FsDiffServSchedulerId ,pSetValFsDiffServSchedulerWeight)
#define nmhSetFsDiffServSchedulerStatus(i4FsDiffServSchedulerId ,i4SetValFsDiffServSchedulerStatus)	\
	nmhSetCmn(FsDiffServSchedulerStatus, 13, FsDiffServSchedulerStatusSet, NULL, NULL, 0, 1, 1, "%i %i", i4FsDiffServSchedulerId ,i4SetValFsDiffServSchedulerStatus)


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDiffServPortId[13];
extern UINT4 FsDiffServCoSqAlgorithm[13];
#define nmhSetFsDiffServCoSqAlgorithm(i4FsDiffServPortId ,i4SetValFsDiffServCoSqAlgorithm)	\
	nmhSetCmn(FsDiffServCoSqAlgorithm, 13, FsDiffServCoSqAlgorithmSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsDiffServPortId ,i4SetValFsDiffServCoSqAlgorithm)


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDiffServBaseCoSqPortId[13];
extern UINT4 FsDiffServPortCoSqId[13];
extern UINT4 FsDiffServCoSqWeight[13];
extern UINT4 FsDiffServCoSqBwMin[13];
#define nmhSetFsDiffServCoSqWeight(i4FsDiffServBaseCoSqPortId , i4FsDiffServPortCoSqId ,i4SetValFsDiffServCoSqWeight)	\
	nmhSetCmn(FsDiffServCoSqWeight, 13, FsDiffServCoSqWeightSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4FsDiffServBaseCoSqPortId , i4FsDiffServPortCoSqId ,i4SetValFsDiffServCoSqWeight)
#define nmhSetFsDiffServCoSqBwMin(i4FsDiffServBaseCoSqPortId , i4FsDiffServPortCoSqId ,u4SetValFsDiffServCoSqBwMin)	\
	nmhSetCmn(FsDiffServCoSqBwMin, 13, FsDiffServCoSqBwMinSet, NULL, NULL, 0, 0, 2, "%i %i %u", i4FsDiffServBaseCoSqPortId , i4FsDiffServPortCoSqId ,u4SetValFsDiffServCoSqBwMin)
