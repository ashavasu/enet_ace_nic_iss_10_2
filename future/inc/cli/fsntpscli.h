/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsntpscli.h,v 1.1 2017/04/03 15:16:26 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsNtpsGlobalStatus[10];
extern UINT4 FsNtpsServerExecute[10];
extern UINT4 FsNtpsDebug[10];
extern UINT4 FsNtpsAuthStatus[10];
extern UINT4 FsNtpsSourceInterface[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsNtpsGlobalStatus(i4SetValFsNtpsGlobalStatus)	\
	nmhSetCmn(FsNtpsGlobalStatus, 10, FsNtpsGlobalStatusSet, NtpsLock, NtpsUnLock, 0, 0, 0, "%i", i4SetValFsNtpsGlobalStatus)
#define nmhSetFsNtpsServerExecute(i4SetValFsNtpsServerExecute)	\
	nmhSetCmn(FsNtpsServerExecute, 10, FsNtpsServerExecuteSet, NtpsLock, NtpsUnLock, 0, 0, 0, "%i", i4SetValFsNtpsServerExecute)
#define nmhSetFsNtpsDebug(i4SetValFsNtpsDebug)	\
	nmhSetCmn(FsNtpsDebug, 10, FsNtpsDebugSet, NtpsLock, NtpsUnLock, 0, 0, 0, "%i", i4SetValFsNtpsDebug)
#define nmhSetFsNtpsAuthStatus(i4SetValFsNtpsAuthStatus)	\
	nmhSetCmn(FsNtpsAuthStatus, 10, FsNtpsAuthStatusSet, NtpsLock, NtpsUnLock, 0, 0, 0, "%i", i4SetValFsNtpsAuthStatus)
#define nmhSetFsNtpsSourceInterface(i4SetValFsNtpsSourceInterface)	\
	nmhSetCmn(FsNtpsSourceInterface, 10, FsNtpsSourceInterfaceSet, NtpsLock, NtpsUnLock, 0, 0, 0, "%i", i4SetValFsNtpsSourceInterface)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsNtpsServerAddrType[12];
extern UINT4 FsNtpsServerAddr[12];
extern UINT4 FsNtpsServerVersion[12];
extern UINT4 FsNtpsServerKeyId[12];
extern UINT4 FsNtpsServerBurstMode[12];
extern UINT4 FsNtpsServerMinPollInt[12];
extern UINT4 FsNtpsServerMaxPollInt[12];
extern UINT4 FsNtpsServerRowStatus[12];
extern UINT4 FsNtpsServerPrefer[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsNtpsServerVersion(i4FsNtpsServerAddrType , pFsNtpsServerAddr ,i4SetValFsNtpsServerVersion)	\
	nmhSetCmn(FsNtpsServerVersion, 12, FsNtpsServerVersionSet, NtpsLock, NtpsUnLock, 0, 0, 2, "%i %s %i", i4FsNtpsServerAddrType , pFsNtpsServerAddr ,i4SetValFsNtpsServerVersion)
#define nmhSetFsNtpsServerKeyId(i4FsNtpsServerAddrType , pFsNtpsServerAddr ,i4SetValFsNtpsServerKeyId)	\
	nmhSetCmn(FsNtpsServerKeyId, 12, FsNtpsServerKeyIdSet, NtpsLock, NtpsUnLock, 0, 0, 2, "%i %s %i", i4FsNtpsServerAddrType , pFsNtpsServerAddr ,i4SetValFsNtpsServerKeyId)
#define nmhSetFsNtpsServerBurstMode(i4FsNtpsServerAddrType , pFsNtpsServerAddr ,i4SetValFsNtpsServerBurstMode)	\
	nmhSetCmn(FsNtpsServerBurstMode, 12, FsNtpsServerBurstModeSet, NtpsLock, NtpsUnLock, 0, 0, 2, "%i %s %i", i4FsNtpsServerAddrType , pFsNtpsServerAddr ,i4SetValFsNtpsServerBurstMode)
#define nmhSetFsNtpsServerMinPollInt(i4FsNtpsServerAddrType , pFsNtpsServerAddr ,u4SetValFsNtpsServerMinPollInt)	\
	nmhSetCmn(FsNtpsServerMinPollInt, 12, FsNtpsServerMinPollIntSet, NtpsLock, NtpsUnLock, 0, 0, 2, "%i %s %u", i4FsNtpsServerAddrType , pFsNtpsServerAddr ,u4SetValFsNtpsServerMinPollInt)
#define nmhSetFsNtpsServerMaxPollInt(i4FsNtpsServerAddrType , pFsNtpsServerAddr ,u4SetValFsNtpsServerMaxPollInt)	\
	nmhSetCmn(FsNtpsServerMaxPollInt, 12, FsNtpsServerMaxPollIntSet, NtpsLock, NtpsUnLock, 0, 0, 2, "%i %s %u", i4FsNtpsServerAddrType , pFsNtpsServerAddr ,u4SetValFsNtpsServerMaxPollInt)
#define nmhSetFsNtpsServerRowStatus(i4FsNtpsServerAddrType , pFsNtpsServerAddr ,i4SetValFsNtpsServerRowStatus)	\
	nmhSetCmn(FsNtpsServerRowStatus, 12, FsNtpsServerRowStatusSet, NtpsLock, NtpsUnLock, 0, 1, 2, "%i %s %i", i4FsNtpsServerAddrType , pFsNtpsServerAddr ,i4SetValFsNtpsServerRowStatus)
#define nmhSetFsNtpsServerPrefer(i4FsNtpsServerAddrType , pFsNtpsServerAddr ,i4SetValFsNtpsServerPrefer)	\
	nmhSetCmn(FsNtpsServerPrefer, 12, FsNtpsServerPreferSet, NtpsLock, NtpsUnLock, 0, 0, 2, "%i %s %i", i4FsNtpsServerAddrType , pFsNtpsServerAddr ,i4SetValFsNtpsServerPrefer)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsNtpsAuthKeyId[12];
extern UINT4 FsNtpsAuthAlgorithm[12];
extern UINT4 FsNtpsAuthKey[12];
extern UINT4 FsNtpsAuthTrustedKey[12];
extern UINT4 FsNtpsAuthRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsNtpsAuthAlgorithm(i4FsNtpsAuthKeyId ,i4SetValFsNtpsAuthAlgorithm)	\
	nmhSetCmn(FsNtpsAuthAlgorithm, 12, FsNtpsAuthAlgorithmSet, NtpsLock, NtpsUnLock, 0, 0, 1, "%i %i", i4FsNtpsAuthKeyId ,i4SetValFsNtpsAuthAlgorithm)
#define nmhSetFsNtpsAuthKey(i4FsNtpsAuthKeyId ,pSetValFsNtpsAuthKey)	\
	nmhSetCmn(FsNtpsAuthKey, 12, FsNtpsAuthKeySet, NtpsLock, NtpsUnLock, 0, 0, 1, "%i %s", i4FsNtpsAuthKeyId ,pSetValFsNtpsAuthKey)
#define nmhSetFsNtpsAuthTrustedKey(i4FsNtpsAuthKeyId ,i4SetValFsNtpsAuthTrustedKey)	\
	nmhSetCmn(FsNtpsAuthTrustedKey, 12, FsNtpsAuthTrustedKeySet, NtpsLock, NtpsUnLock, 0, 0, 1, "%i %i", i4FsNtpsAuthKeyId ,i4SetValFsNtpsAuthTrustedKey)
#define nmhSetFsNtpsAuthRowStatus(i4FsNtpsAuthKeyId ,i4SetValFsNtpsAuthRowStatus)	\
	nmhSetCmn(FsNtpsAuthRowStatus, 12, FsNtpsAuthRowStatusSet, NtpsLock, NtpsUnLock, 0, 1, 1, "%i %i", i4FsNtpsAuthKeyId ,i4SetValFsNtpsAuthRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsNtpsAccessAddrType[12];
extern UINT4 FsNtpsAccessIpAddr[12];
extern UINT4 FsNtpsAccessIpMask[12];
extern UINT4 FsNtpsAccessRowStatus[12];
extern UINT4 FsNtpsAccessFlag[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsNtpsAccessRowStatus(i4FsNtpsAccessAddrType , pFsNtpsAccessIpAddr , i4FsNtpsAccessIpMask ,i4SetValFsNtpsAccessRowStatus)	\
	nmhSetCmn(FsNtpsAccessRowStatus, 12, FsNtpsAccessRowStatusSet, NtpsLock, NtpsUnLock, 0, 1, 3, "%i %s %i %i", i4FsNtpsAccessAddrType , pFsNtpsAccessIpAddr , i4FsNtpsAccessIpMask ,i4SetValFsNtpsAccessRowStatus)
#define nmhSetFsNtpsAccessFlag(i4FsNtpsAccessAddrType , pFsNtpsAccessIpAddr , i4FsNtpsAccessIpMask ,i4SetValFsNtpsAccessFlag)	\
	nmhSetCmn(FsNtpsAccessFlag, 12, FsNtpsAccessFlagSet, NtpsLock, NtpsUnLock, 0, 0, 3, "%i %s %i %i", i4FsNtpsAccessAddrType , pFsNtpsAccessIpAddr , i4FsNtpsAccessIpMask ,i4SetValFsNtpsAccessFlag)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsNtpsInterfaceIndex[12];
extern UINT4 FsNtpsInterfaceNtpStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsNtpsInterfaceNtpStatus(i4FsNtpsInterfaceIndex ,i4SetValFsNtpsInterfaceNtpStatus)	\
	nmhSetCmn(FsNtpsInterfaceNtpStatus, 12, FsNtpsInterfaceNtpStatusSet, NtpsLock, NtpsUnLock, 0, 0, 1, "%i %i", i4FsNtpsInterfaceIndex ,i4SetValFsNtpsInterfaceNtpStatus)

#endif
