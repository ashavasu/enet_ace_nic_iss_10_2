/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsesatcli.h,v 1.1 2014/08/14 12:52:03 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsEsatSystemControl[10];
extern UINT4 FsEsatTraceOption[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsEsatSystemControl(i4SetValFsEsatSystemControl)	\
	nmhSetCmnWithLock(FsEsatSystemControl, 10, FsEsatSystemControlSet, EsatApiLock, EsatApiUnLock, 0, 0, 0, "%i", i4SetValFsEsatSystemControl)
#define nmhSetFsEsatTraceOption(i4SetValFsEsatTraceOption)	\
	nmhSetCmnWithLock(FsEsatTraceOption, 10, FsEsatTraceOptionSet, EsatApiLock, EsatApiUnLock, 0, 0, 0, "%i", i4SetValFsEsatTraceOption)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsEsatSlaId[12];
extern UINT4 FsEsatSlaIfIndex[12];
extern UINT4 FsEsatSlaEvcIndex[12];
extern UINT4 FsEsatSlaMEG[12];
extern UINT4 FsEsatSlaME[12];
extern UINT4 FsEsatSlaMEP[12];
extern UINT4 FsEsatSlaRateStep[12];
extern UINT4 FsEsatSlaFreqDelay[12];
extern UINT4 FsEsatSlaDuration[12];
extern UINT4 FsEsatSlaDirection[12];
extern UINT4 FsEsatSlaTrafProfileId[12];
extern UINT4 FsEsatSlaSacId[12];
extern UINT4 FsEsatSlaStatus[12];
extern UINT4 FsEsatSlaRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsEsatSlaIfIndex(u4FsEsatSlaId ,i4SetValFsEsatSlaIfIndex)	\
	nmhSetCmnWithLock(FsEsatSlaIfIndex, 12, FsEsatSlaIfIndexSet, EsatApiLock, EsatApiUnLock, 0, 0, 1, "%u %i", u4FsEsatSlaId ,i4SetValFsEsatSlaIfIndex)
#define nmhSetFsEsatSlaEvcIndex(u4FsEsatSlaId ,i4SetValFsEsatSlaEvcIndex)	\
	nmhSetCmnWithLock(FsEsatSlaEvcIndex, 12, FsEsatSlaEvcIndexSet, EsatApiLock, EsatApiUnLock, 0, 0, 1, "%u %i", u4FsEsatSlaId ,i4SetValFsEsatSlaEvcIndex)
#define nmhSetFsEsatSlaMEG(u4FsEsatSlaId ,u4SetValFsEsatSlaMEG)	\
	nmhSetCmnWithLock(FsEsatSlaMEG, 12, FsEsatSlaMEGSet, EsatApiLock, EsatApiUnLock, 0, 0, 1, "%u %u", u4FsEsatSlaId ,u4SetValFsEsatSlaMEG)
#define nmhSetFsEsatSlaME(u4FsEsatSlaId ,u4SetValFsEsatSlaME)	\
	nmhSetCmnWithLock(FsEsatSlaME, 12, FsEsatSlaMESet, EsatApiLock, EsatApiUnLock, 0, 0, 1, "%u %u", u4FsEsatSlaId ,u4SetValFsEsatSlaME)
#define nmhSetFsEsatSlaMEP(u4FsEsatSlaId ,u4SetValFsEsatSlaMEP)	\
	nmhSetCmnWithLock(FsEsatSlaMEP, 12, FsEsatSlaMEPSet, EsatApiLock, EsatApiUnLock, 0, 0, 1, "%u %u", u4FsEsatSlaId ,u4SetValFsEsatSlaMEP)
#define nmhSetFsEsatSlaRateStep(u4FsEsatSlaId ,i4SetValFsEsatSlaRateStep)	\
	nmhSetCmnWithLock(FsEsatSlaRateStep, 12, FsEsatSlaRateStepSet, EsatApiLock, EsatApiUnLock, 0, 0, 1, "%u %i", u4FsEsatSlaId ,i4SetValFsEsatSlaRateStep)
#define nmhSetFsEsatSlaFreqDelay(u4FsEsatSlaId ,i4SetValFsEsatSlaFreqDelay)	\
	nmhSetCmnWithLock(FsEsatSlaFreqDelay, 12, FsEsatSlaFreqDelaySet, EsatApiLock, EsatApiUnLock, 0, 0, 1, "%u %i", u4FsEsatSlaId ,i4SetValFsEsatSlaFreqDelay)
#define nmhSetFsEsatSlaDuration(u4FsEsatSlaId ,i4SetValFsEsatSlaDuration)	\
	nmhSetCmnWithLock(FsEsatSlaDuration, 12, FsEsatSlaDurationSet, EsatApiLock, EsatApiUnLock, 0, 0, 1, "%u %i", u4FsEsatSlaId ,i4SetValFsEsatSlaDuration)
#define nmhSetFsEsatSlaDirection(u4FsEsatSlaId ,i4SetValFsEsatSlaDirection)	\
	nmhSetCmnWithLock(FsEsatSlaDirection, 12, FsEsatSlaDirectionSet, EsatApiLock, EsatApiUnLock, 0, 0, 1, "%u %i", u4FsEsatSlaId ,i4SetValFsEsatSlaDirection)
#define nmhSetFsEsatSlaTrafProfileId(u4FsEsatSlaId ,i4SetValFsEsatSlaTrafProfileId)	\
	nmhSetCmnWithLock(FsEsatSlaTrafProfileId, 12, FsEsatSlaTrafProfileIdSet, EsatApiLock, EsatApiUnLock, 0, 0, 1, "%u %i", u4FsEsatSlaId ,i4SetValFsEsatSlaTrafProfileId)
#define nmhSetFsEsatSlaSacId(u4FsEsatSlaId ,i4SetValFsEsatSlaSacId)	\
	nmhSetCmnWithLock(FsEsatSlaSacId, 12, FsEsatSlaSacIdSet, EsatApiLock, EsatApiUnLock, 0, 0, 1, "%u %i", u4FsEsatSlaId ,i4SetValFsEsatSlaSacId)
#define nmhSetFsEsatSlaStatus(u4FsEsatSlaId ,i4SetValFsEsatSlaStatus)	\
	nmhSetCmnWithLock(FsEsatSlaStatus, 12, FsEsatSlaStatusSet, EsatApiLock, EsatApiUnLock, 0, 0, 1, "%u %i", u4FsEsatSlaId ,i4SetValFsEsatSlaStatus)
#define nmhSetFsEsatSlaRowStatus(u4FsEsatSlaId ,i4SetValFsEsatSlaRowStatus)	\
	nmhSetCmnWithLock(FsEsatSlaRowStatus, 12, FsEsatSlaRowStatusSet, EsatApiLock, EsatApiUnLock, 0, 1, 1, "%u %i", u4FsEsatSlaId ,i4SetValFsEsatSlaRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsEsatTrafProfId[12];
extern UINT4 FsEsatTrafProfDir[12];
extern UINT4 FsEsatTrafProfTagType[12];
extern UINT4 FsEsatTrafProfInVlan[12];
extern UINT4 FsEsatTrafProfOutVlan[12];
extern UINT4 FsEsatTrafProfInCos[12];
extern UINT4 FsEsatTrafProfOutCos[12];
extern UINT4 FsEsatTrafProfPktSize[12];
extern UINT4 FsEsatTrafProfSrcMac[12];
extern UINT4 FsEsatTrafProfDestMac[12];
extern UINT4 FsEsatTrafProfPayload[12];
extern UINT4 FsEsatTrafProfRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsEsatTrafProfDir(u4FsEsatTrafProfId ,i4SetValFsEsatTrafProfDir)	\
	nmhSetCmnWithLock(FsEsatTrafProfDir, 12, FsEsatTrafProfDirSet, EsatApiLock, EsatApiUnLock, 0, 0, 1, "%u %i", u4FsEsatTrafProfId ,i4SetValFsEsatTrafProfDir)
#define nmhSetFsEsatTrafProfTagType(u4FsEsatTrafProfId ,i4SetValFsEsatTrafProfTagType)	\
	nmhSetCmnWithLock(FsEsatTrafProfTagType, 12, FsEsatTrafProfTagTypeSet, EsatApiLock, EsatApiUnLock, 0, 0, 1, "%u %i", u4FsEsatTrafProfId ,i4SetValFsEsatTrafProfTagType)
#define nmhSetFsEsatTrafProfInVlan(u4FsEsatTrafProfId ,i4SetValFsEsatTrafProfInVlan)	\
	nmhSetCmnWithLock(FsEsatTrafProfInVlan, 12, FsEsatTrafProfInVlanSet, EsatApiLock, EsatApiUnLock, 0, 0, 1, "%u %i", u4FsEsatTrafProfId ,i4SetValFsEsatTrafProfInVlan)
#define nmhSetFsEsatTrafProfOutVlan(u4FsEsatTrafProfId ,i4SetValFsEsatTrafProfOutVlan)	\
	nmhSetCmnWithLock(FsEsatTrafProfOutVlan, 12, FsEsatTrafProfOutVlanSet, EsatApiLock, EsatApiUnLock, 0, 0, 1, "%u %i", u4FsEsatTrafProfId ,i4SetValFsEsatTrafProfOutVlan)
#define nmhSetFsEsatTrafProfInCos(u4FsEsatTrafProfId ,i4SetValFsEsatTrafProfInCos)	\
	nmhSetCmnWithLock(FsEsatTrafProfInCos, 12, FsEsatTrafProfInCosSet, EsatApiLock, EsatApiUnLock, 0, 0, 1, "%u %i", u4FsEsatTrafProfId ,i4SetValFsEsatTrafProfInCos)
#define nmhSetFsEsatTrafProfOutCos(u4FsEsatTrafProfId ,i4SetValFsEsatTrafProfOutCos)	\
	nmhSetCmnWithLock(FsEsatTrafProfOutCos, 12, FsEsatTrafProfOutCosSet, EsatApiLock, EsatApiUnLock, 0, 0, 1, "%u %i", u4FsEsatTrafProfId ,i4SetValFsEsatTrafProfOutCos)
#define nmhSetFsEsatTrafProfPktSize(u4FsEsatTrafProfId ,i4SetValFsEsatTrafProfPktSize)	\
	nmhSetCmnWithLock(FsEsatTrafProfPktSize, 12, FsEsatTrafProfPktSizeSet, EsatApiLock, EsatApiUnLock, 0, 0, 1, "%u %i", u4FsEsatTrafProfId ,i4SetValFsEsatTrafProfPktSize)
#define nmhSetFsEsatTrafProfSrcMac(u4FsEsatTrafProfId ,SetValFsEsatTrafProfSrcMac)	\
	nmhSetCmnWithLock(FsEsatTrafProfSrcMac, 12, FsEsatTrafProfSrcMacSet, EsatApiLock, EsatApiUnLock, 0, 0, 1, "%u %m", u4FsEsatTrafProfId ,SetValFsEsatTrafProfSrcMac)
#define nmhSetFsEsatTrafProfDestMac(u4FsEsatTrafProfId ,SetValFsEsatTrafProfDestMac)	\
	nmhSetCmnWithLock(FsEsatTrafProfDestMac, 12, FsEsatTrafProfDestMacSet, EsatApiLock, EsatApiUnLock, 0, 0, 1, "%u %m", u4FsEsatTrafProfId ,SetValFsEsatTrafProfDestMac)
#define nmhSetFsEsatTrafProfPayload(u4FsEsatTrafProfId ,pSetValFsEsatTrafProfPayload)	\
	nmhSetCmnWithLock(FsEsatTrafProfPayload, 12, FsEsatTrafProfPayloadSet, EsatApiLock, EsatApiUnLock, 0, 0, 1, "%u %s", u4FsEsatTrafProfId ,pSetValFsEsatTrafProfPayload)
#define nmhSetFsEsatTrafProfRowStatus(u4FsEsatTrafProfId ,i4SetValFsEsatTrafProfRowStatus)	\
	nmhSetCmnWithLock(FsEsatTrafProfRowStatus, 12, FsEsatTrafProfRowStatusSet, EsatApiLock, EsatApiUnLock, 0, 1, 1, "%u %i", u4FsEsatTrafProfId ,i4SetValFsEsatTrafProfRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsEsatSacId[12];
extern UINT4 FsEsatSacInfoRate[12];
extern UINT4 FsEsatSacFrLossRatio[12];
extern UINT4 FsEsatSacFrTxDelay[12];
extern UINT4 FsEsatSacFrDelayVar[12];
extern UINT4 FsEsatSacRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsEsatSacInfoRate(u4FsEsatSacId ,i4SetValFsEsatSacInfoRate)	\
	nmhSetCmnWithLock(FsEsatSacInfoRate, 12, FsEsatSacInfoRateSet, EsatApiLock, EsatApiUnLock, 0, 0, 1, "%u %i", u4FsEsatSacId ,i4SetValFsEsatSacInfoRate)
#define nmhSetFsEsatSacFrLossRatio(u4FsEsatSacId ,i4SetValFsEsatSacFrLossRatio)	\
	nmhSetCmnWithLock(FsEsatSacFrLossRatio, 12, FsEsatSacFrLossRatioSet, EsatApiLock, EsatApiUnLock, 0, 0, 1, "%u %i", u4FsEsatSacId ,i4SetValFsEsatSacFrLossRatio)
#define nmhSetFsEsatSacFrTxDelay(u4FsEsatSacId ,i4SetValFsEsatSacFrTxDelay)	\
	nmhSetCmnWithLock(FsEsatSacFrTxDelay, 12, FsEsatSacFrTxDelaySet, EsatApiLock, EsatApiUnLock, 0, 0, 1, "%u %i", u4FsEsatSacId ,i4SetValFsEsatSacFrTxDelay)
#define nmhSetFsEsatSacFrDelayVar(u4FsEsatSacId ,i4SetValFsEsatSacFrDelayVar)	\
	nmhSetCmnWithLock(FsEsatSacFrDelayVar, 12, FsEsatSacFrDelayVarSet, EsatApiLock, EsatApiUnLock, 0, 0, 1, "%u %i", u4FsEsatSacId ,i4SetValFsEsatSacFrDelayVar)
#define nmhSetFsEsatSacRowStatus(u4FsEsatSacId ,i4SetValFsEsatSacRowStatus)	\
	nmhSetCmnWithLock(FsEsatSacRowStatus, 12, FsEsatSacRowStatusSet, EsatApiLock, EsatApiUnLock, 0, 1, 1, "%u %i", u4FsEsatSacId ,i4SetValFsEsatSacRowStatus)

#endif
