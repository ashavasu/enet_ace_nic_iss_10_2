
/* INCLUDE FILE HEADER :
 *
 * $Id: rsnacli.h,v 1.15 2017/11/24 10:36:59 siva Exp $
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : rsnacli.h                                        |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR      :                                                  |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : RSNA                                             |
 * |                                                                           |
 * |  MODULE NAME           : CLI interface for RSNA configuration             |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    :                                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION           : This file contains command idenfiers for         |
 * |                          definitions in rsnacmd.def, function prototypes  |
 * |                          for RSNA CLI and corresponding error code        |
 * |                          definitions                                      |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 *
 */

#ifndef RSNACLI_H
#define RSNACLI_H

#include "cli.h"
/* RSNA CLI command constants. To add a new command ,a new definition needs to
 * be added to the list.
 */



enum
{
    CLI_RSNA_TRACE_CHANGE = 1,        /* Changes the trace*/
    CLI_RSNA_SECURITY,                /* Shows RSNA security associations*/
    CLI_RSNA_PREAUTH_MODE,            /*Shows RSNA security associations*/
    CLI_RSNA_GROUPWISE_CIPHER,        /*Show RSNA Authetication and key suite association */
    CLI_RSNA_GROUP_REKEY_METHOD,      /*shows RSNA GroupKey Methods*/
    CLI_RSNA_GROUP_REKEY_TIME,         /*shows RSNA GTK rekey interval*/
    CLI_RSNA_GROUP_REKEY_PKT,           /*Gtk rekeying based on packets sent */
    CLI_RSNA_GROUP_REKEY_STRICT,    /* Strict GTK Rekey mode */    
    CLI_RSNA_PSK_CONFIG,               /* To configure either PSK Passphrase or Value */
    CLI_RSNA_GROUP_UPDATE_COUNT,      /*Retry count value of group handshake */
    CLI_RSNA_PAIRWISE_UPDATE_COUNT,      /*Retry count value of group handshake */
    CLI_RSNA_PMKLIFETIME,            /* sets the maximum time period for PMK value to be present in the cache */
    CLI_RSNA_PMKREAUTHTHRESHOLD,     /*sets the percentage of the PMK lifetime that should expire before an IEEE 802.1X reauthentication occurs */
    CLI_RSNA_SATIMEOUT,              /* The maximum time a security association shall take to set up */
    CLI_RSNA_OKC_STATUS,             /* Opportunistic key caching status */
    CLI_RSNA_PAIRWISE_CIPHER_STATUS,  /*Show RSNA Authentication and key suite association */    /* TODO - dot11RSNAConfigPairwiseCipherEnabled */
    CLI_RSNA_AUTHSUITE_MODE,               /*Show RSNA Auhentication mode parameters*/
    CLI_RSNA_SHOW_CONFIG_DETAILS,      /*Shows RSNA Config details */
    CLI_RSNA_SHOW_PAIRWISE_CIPHER,              /* The maximum time a security association shall take to set up */
    CLI_RSNA_SHOW_AUTHSUITE_DETAILS,       /*Shows Authentication Suite Related details*/  
    CLI_RSNA_SHOW_RSNA_STATS,         /* Shows RSNA Client Statistics*/
    CLI_RSNA_SHOW_RSNA_HANDSHAKE_STATS,         /* Shows RSNA handshake Statistics*/

    CLI_RSNA_SHOW_WPA2_STATUS,        /* Shows WPA2 Status */
    CLI_RSNA_SHOW_PREAUTH_STATUS,     /* Shows WPA2 Pre-authentication status*/
    CLI_RSNA_CLEAR_STATS,             /* Clears the RSNA Stats info and counters in config file*/
    CLI_RSNA_CLEAR_COUNTERS_WLANID ,  /* Clears the counters for particular WLAN ID*/
    CLI_RSNA_NO_DEBUG,                /* disables tracing*/
    CLI_RSNA_DEBUG,                   /* enables tracing*/
#ifdef PMF_WANTED
    CLI_RSNA_MFPR_CONFIG,              /* To Configure PMF Mandatory */
    CLI_RSNA_MFPC_CONFIG,              /* To Configure PMF capable */
    CLI_RSNA_MFP_ASSOC_COMEBACK_TIME, /* To Configure PMF Assoc ComeBack Time */
    CLI_RSNA_MFP_SAQUERY_RETRY_TIME,  /* To Configure SA Query Retry Time */
    CLI_RSNA_MFP_SAQUERY_MAX_TIME_OUT,/* To Configure SA Query Max Time Out*/
    CLI_RSNA_SHOW_MFP_CONFIG,         /* show Management Frame Protection Configurations */
#endif
#ifdef RSNA_TEST_WANTED
    CLI_RSNA_TEST_PAIRWISE_UPDATE_COUNT,           /* for testing the functionality*/
    CLI_RSNA_TEST_GROUPWISE_UPDATE_COUNT,          /* for testing the functionality*/
    CLI_RSNA_SHOW_PMK_CACHE,                  /* for displaying the PMK Cache Table*/
#endif
#ifdef WPA_WANTED
    CLI_WPA_SECURITY,
    CLI_WPA_GROUPWISE_CIPHER,
    CLI_WPA_GROUP_REKEY_TIME,
    CLI_WPA_PSK_CONFIG,
    CLI_WPA_GROUP_UPDATE_COUNT,
    CLI_WPA_PAIRWISE_UPDATE_COUNT,
    CLI_WPA_PMKLIFETIME,
    CLI_WPA_SATIMEOUT,
    CLI_WPA_AUTHSUITE_MODE,
    CLI_WPA_CLEAR_STATS,
    CLI_WPA_CLEAR_COUNTERS_WLANID,
    CLI_WPA_OKC_STATUS, 
    CLI_SHOW_WPA_STATUS,
    CLI_WPA_SHOW_CONFIG_DETAILS, 
    CLI_WPA_SHOW_PAIRWISE_CIPHER,
    CLI_WPA_SHOW_AUTHSUITE_DETAILS,
    CLI_WPA_MIX_MODE,
    CLI_SHOW_MIX_MODE,
#endif
    CLI_RSNA_MAX_CMDS
};

#define RSNA_CLEAR_FLAG_SET              1
#define RSNA_CLEAR_FLAG_NOSET            0


#define RSNA_TRUE                        1    /* Always true*/
#define RSNA_CLI_TERMINATE               1    /* For \0*/
#define RSNA_CLI_FIRST_ARG               1
#define RSNA_CLI_SECOND_ARG              2
#define RSNA_CLI_THIRD_ARG               3
#define RSNA_CLI_TRACE_ADD               1 /* Macro to add trace*/
#define RSNA_CLI_TRACE_DEL               2 /* Macro to delete trace*/
#define RSNA_CLI_UINT8                   (2 * sizeof (UINT4))
#define RSNA_CLI_SUITE_LEN               4    /* Specifies the number of 
                                                 bytes to be copied*/
#define RSNA_CLI_DISPLAY_SUITE           6 
#define RSNA_INIT_SHUT_TRC               INIT_SHUT_TRC     
#define RSNA_MGMT_TRC                    MGMT_TRC          
#define RSNA_DATA_PATH_TRC               DATA_PATH_TRC     
#define RSNA_CONTROL_PATH_TRC            CONTROL_PLANE_TRC 
#define RSNA_DUMP_TRC                    DUMP_TRC          
#define RSNA_OS_RESOURCE_TRC             OS_RESOURCE_TRC   
#define RSNA_ALL_FAILURE_TRC             ALL_FAILURE_TRC   
#define RSNA_BUFFER_TRC                  BUFFER_TRC        
#define RSNA_CLI_MAX_TRACE_LEN           200
#define RSNA_CLI_MAC_ADDR_LEN            21
#define RSNA_CLI_PROTOCOL_TYPE_LENGTH    10




/* Error codes 
 * Error code values and strings are maintained inside the protocol module itself.
 */
enum {
    CLI_RSNA_INVALID_SSID = 1,
    CLI_RSNA_NO_STATIONS,
    CLI_RSNA_NO_ENTRIES_TO_IFINDEX,
    CLI_RSNA_NOT_ENABLED,
    CLI_RSNA_PREAUTH_NOT_ENABLED,
    CLI_RSNA_PAIRWISE_CIPHER_NOT_ENABLED,
    CLI_RSNA_AUTHSUITE_NOT_ENABLED,
    CLI_RSNA_INVALID_PSK_VALUE,
    CLI_RSNA_INVALID_PASSPHRASE,
    CLI_RSNA_INVALID_REKEY_TIME,
    CLI_RSNA_INVALID_REKEY_PKT,
    CLI_RSNA_INVALID_GROUP_UPDATE_COUNT,
    CLI_RSNA_INVALID_PAIRWISE_UPDATE_COUNT,
    CLI_RSNA_INVALID_PMK_LIFETIME,
    CLI_RSNA_INVALID_PMK_REAUTH_THRESHOLD,
    CLI_RSNA_INVALID_SA_TIMEOUT,
    CLI_RSNA_INVALID_WLAN_ID,
    CLI_RSNA_INVALID_INVALID_GRP_CIPHER,
    CLI_RSNA_INVALID_INVALID_GRP_METHOD,
    CLI_RSNA_INVALID_INVALID_GRP_STRICT,
    CLI_RSNA_INVALID_CLEAR_FIELD,
    CLI_RSNA_WLAN_NOT_CREATED,
    CLI_RSNA_INCOMPATIBLE_PAIRWISE_AND_GROUPWISE_CIPHER,
    CLI_RSNA_COMMAND_NOT_SUPPORTED,
#ifdef PMF_WANTED
    CLI_RSNA_INVALID_SAQUERY_RETRY_TIME,
    CLI_RSNA_INVALID_ASSOC_COMEBACK_TIME,
#endif
    
    
    CLI_RSNA_MAX_ERR
};
/* The error strings should be places under the switch so as to avoid redifinition
 * This will be visible only in modulecli.c file
 */
#ifdef RSNACLI_C

CONST CHR1  *RsnaCliErrString [] = {
    NULL,
    "% No such SSID exists \r\n",
    "% No stations exists \r\n",
    "% No entries corresponding to Ifindex \r\n",
    "% Wpa2 is not enabled \r\n",
    "% Wpa2 pre-authentication not enabled \r\n",
    "% Wpa2 pairwise cipher not enabled \r\n",
    "% Wpa2 authentication not enabled \r\n",
    "% Invalid psk value \r\n",
    "% Invalid PassPhrase \r\n",
    "% Invalid rekey time interval \r\n",
    "% Invalid rekey packet count \r\n",
    "% Invalid group update count \r\n",
    "% Invalid pairwise update count \r\n",
    "% Invalid pmk lifetime \r\n",
    "% Invalid pmk reauth-threshold \r\n",
    "% Invalid SA timeout \r\n",
    "% Invalid WLAN id \r\n",
    "% Invalid Group Cipher \r\n",
    "% Invalid Group Method \r\n",
    "% Invalid Group Strict \r\n",
    "% Invalid Clear field\r\n",
    "% WLAN is not created\r\n",
    "% Incompatible Pairwise and Group Cipher Suites\r\n",
    "% Command Not Supported",
#ifdef PMF_WANTED
    "% Invalid SA Query Retry Time\r\n ",
    "% Invalid Association Come Back Time \r\n"
#endif
};

#endif /*RSNACLI_C*/
/*extern declarations */


INT4 cli_process_rsna_command PROTO ((tCliHandle CliHandle,
                                      UINT4 u4Command, ...));

INT4 RsnaSetTrace             PROTO ((tCliHandle CliHandle, INT4 i4TraceStats, 
                                      INT4 i4Trace));
INT4
RsnaCliSetDebugs ( tCliHandle CliHandle,
                   INT4 i4CliTraceVal  ,
                   INT4 u1TraceFlag    );


/* Util APIs for config commands */
INT4 RsnaCliSetSecurity      PROTO ((tCliHandle CliHandle, UINT1 u1Status, UINT4 u4WlanIndex));
INT4 RsnaCliSetPreAuthMode                   PROTO ((tCliHandle CliHandle, UINT1 u1Status, UINT4 u4WlanIndex));
INT4 RsnaCliSetGroupwiseCipher          PROTO ((tCliHandle CliHandle, UINT1 u1CipherType, UINT4 u4WlanIndex));
INT4 RsnaCliSetGroupRekeyMethod          PROTO ((tCliHandle CliHandle, UINT1 u1GroupRekeyMethod, UINT4 u4WlanIndex));
INT4 RsnaCliSetGroupRekeyinterval    PROTO ((tCliHandle CliHandle, UINT4 u4GroupRekeyInterval, UINT4 u4WlanIndex));
INT4 RsnaCliSetGroupRekeyPackets PROTO ((tCliHandle CliHandle, UINT4 u4NumberOfPackets, UINT4 u4WlanId  ));
INT4 RsnaCliSetStrictGtkMode         PROTO  ((tCliHandle CliHandle, UINT1 u1Status, UINT4 u4WlanId));
INT4 RsnaCliSetAkmPsk               PROTO ((tCliHandle CliHandle, UINT1 u1PskFormat, UINT1 *pu1Key , UINT4 u4WlanId));
INT4 RsnaCliSetGroupUpdateCount PROTO ((tCliHandle CliHandle, UINT4 u4RetryCount, UINT4 u4WlanId ));
INT4 RsnaCliSetPairwiseUpdateCount  PROTO ((tCliHandle CliHandle, UINT4 u4RetryCount, UINT4 u4WlanId));
INT4 RsnaCliSetPMKLifeTime  PROTO ((tCliHandle CliHandle, UINT4 u4LifeTime, UINT4 u4WlanId));
INT4 RsnaCliSetPMKReauthThreshold   PROTO ((tCliHandle CliHandle, UINT4 u4ThresholdValue, UINT4 u4WlanId));
INT4 RsnaCliSetSATimeOut         PROTO ((tCliHandle CliHandle, UINT4 u4TimeValue, UINT4 u4WlanId));
INT4 RsnaCliSetPairwiseCipherStatus    PROTO ((tCliHandle CliHandle, UINT1 u1CipherType, UINT1 u1Status, UINT4 u4WlanIndex));
INT4 RsnaCliSetAuthSuiteStatus   PROTO ((tCliHandle CliHandle, UINT1 u1AuthType, UINT1 u1Status, UINT4 u4ProfileId));
INT4 RsnaCliClearStats (tCliHandle CliHandle);
INT4 RsnaCliClearCountersDetail (tCliHandle CliHandle, UINT4 WlanId);
INT4 RsnaCliSetOkcStatus PROTO ((tCliHandle CliHandle, UINT1 u1Status, UINT4 u4WlanId));
#ifdef PMF_WANTED
INT4 RsnaCliSetAssocComeBackTime PROTO ((tCliHandle CliHandle, UINT4 u4AssocComeBackTime, UINT4 u4ProfileId ));
INT4 RsnaCliSetMgmtFrameProtectionRequired PROTO ((tCliHandle CliHandle, UINT1 u1MFPType, UINT4 u4ProfileId )); 
INT4 RsnaCliSetMgmtFrameProtectionCapable PROTO ((tCliHandle CliHandle, UINT1 u1MFPType, UINT4 u4ProfileId )); 
INT4 RsnaCliSetSAQueryRetryTime PROTO ((tCliHandle CliHandle, UINT4 u4SaQueryRetryTime, UINT4 u4ProfileId ));
INT4 RsnaCliSetSAQueryMaxTimeOut PROTO ((tCliHandle CliHandle, UINT4 u4SaQueryMaxTimeOut, UINT4 u4ProfileId ));
INT4
RsnaCliShowMFPConfig PROTO ((tCliHandle CliHandle, UINT4 u4ProfileId));
#endif
INT4 RsnaShowRunningConfig (tCliHandle CliHandle, UINT4 u4WlanIndex);
/* Util APIs for show commands */
INT4 RsnaCliShowWpa2Status      PROTO ((tCliHandle CliHandle,UINT4 u4WlanIndex));
INT4 RsnaCliShowPreAuthStatus  PROTO ((tCliHandle CliHandle,UINT4 u4WlanIndex));
INT4 RsnaCliShowConfig         PROTO ((tCliHandle CliHandle,UINT4 u4WlanIndex));
INT4 RsnaCliShowWpa2Cipher     PROTO ((tCliHandle CliHandle, UINT4 u4WlanId));
INT4 RsnaCliShowAuthSuiteInfo  PROTO ((tCliHandle CliHandle, UINT4));
INT4 RsnaCliShowStats    PROTO ((tCliHandle CliHandle, UINT4 u4WlanIndex));
INT4 RsnaCliShowHandshakeStats PROTO ((tCliHandle CliHandle, UINT4 u4ProfileId));
VOID RsnaCliShowHandshakeStatsDisplay PROTO ((tCliHandle CliHandle, 
                                      UINT4 u4NextConfigIndex, UINT4 u4WlanId));

INT4 RsnaShowRunningConfigTables PROTO ((tCliHandle CliHandle, UINT4 u4WlanIndex));

INT4 RsnaShowRunningConfigPrivacyTableDisplay PROTO((tCliHandle CliHandle,UINT4 u4ProfileId,UINT4 u4WlanId));
INT4 RsnaShowRunningConfigTableDisplay PROTO((tCliHandle CliHandle,UINT4 u4ProfileId ,UINT4 u4WlanId));
INT4 RsnaShowRunningConfigCipherTableDisplay PROTO ((tCliHandle CliHandle, UINT4 u4ProfileId,
                                             UINT4 u4RSNAConfigPairwiseCipherIndex,UINT4 u4WlanId));
INT4 RsnaShowRunningConfigAuthSuiteTableDisplay PROTO ((tCliHandle CliHandle,
      UINT4, UINT4 u4AuthSuiteIndex, UINT4));
VOID RsnaCliShowConfigDisplay    PROTO ((tCliHandle CliHandle, UINT4 u4NextConfigIndex,UINT4 u4WlanId));
VOID RsnaCliShowWpa2CipherDisplay PROTO ((tCliHandle CliHandle, UINT4 u4NextWlanId, UINT4 u4NextRSNAConfigPairwiseCipherIndex,UINT4 u4WlanId));
VOID
RsnaCliShowAuthSuiteInfoDisplay PROTO ((tCliHandle CliHandle, UINT4 u4NextWlanId,
                              UINT4 u4CurrConfigPairwiseCipherIndex,
                              UINT4 u4WlanId));
VOID RsnaCliShowStatsDisplay PROTO ((tCliHandle CliHandle, UINT4 u4WlanIndexValue, 
                                     UINT4 u4RSNAStatsIndexValue,UINT4 u4WlanId));

#ifdef RSNA_TEST_WANTED
INT4 RsnaCliTestPairwiseUpdateCount  PROTO ((tCliHandle CliHandle));
INT4 RsnaCliTestGroupwiseUpdateCount PROTO ((tCliHandle CliHandle));
INT4 RsnaCliShowPmkCacheEntry        PROTO ((tCliHandle CliHandle));
INT4 RsnaCliPmkCacheWalkFn           PROTO ((tRBElem *pRBEle, 
                                             eRBVisit visit, 
                                             UINT4 u4Level,
                                             VOID *pArg, 
                                             VOID *pOut));
#endif

#endif /*RSNACLI_H*/
