/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fscfmecli.h,v 1.4 2011/01/25 15:11:27 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIEcfmExtDefaultMdPrimarySelectorType[14];
extern UINT4 FsMIEcfmExtDefaultMdPrimarySelector[14];
extern UINT4 FsMIEcfmExtDefaultMdLevel[14];
extern UINT4 FsMIEcfmExtDefaultMdMhfCreation[14];
extern UINT4 FsMIEcfmExtDefaultMdIdPermission[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMIEcfmExtDefaultMdLevel(u4FsMIEcfmContextId , i4FsMIEcfmExtDefaultMdPrimarySelectorType , u4FsMIEcfmExtDefaultMdPrimarySelector ,i4SetValFsMIEcfmExtDefaultMdLevel)	\
	nmhSetCmnWithLock(FsMIEcfmExtDefaultMdLevel, 14, FsMIEcfmExtDefaultMdLevelSet,EcfmCcLock, EcfmCcUnLock, 0, 0, 3, "%u %i %u %i", u4FsMIEcfmContextId , i4FsMIEcfmExtDefaultMdPrimarySelectorType , u4FsMIEcfmExtDefaultMdPrimarySelector ,i4SetValFsMIEcfmExtDefaultMdLevel)
#define nmhSetFsMIEcfmExtDefaultMdMhfCreation(u4FsMIEcfmContextId , i4FsMIEcfmExtDefaultMdPrimarySelectorType , u4FsMIEcfmExtDefaultMdPrimarySelector ,i4SetValFsMIEcfmExtDefaultMdMhfCreation)	\
	nmhSetCmnWithLock(FsMIEcfmExtDefaultMdMhfCreation, 14,FsMIEcfmExtDefaultMdMhfCreationSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 3, "%u %i %u %i", u4FsMIEcfmContextId , i4FsMIEcfmExtDefaultMdPrimarySelectorType , u4FsMIEcfmExtDefaultMdPrimarySelector ,i4SetValFsMIEcfmExtDefaultMdMhfCreation)
#define nmhSetFsMIEcfmExtDefaultMdIdPermission(u4FsMIEcfmContextId , i4FsMIEcfmExtDefaultMdPrimarySelectorType , u4FsMIEcfmExtDefaultMdPrimarySelector ,i4SetValFsMIEcfmExtDefaultMdIdPermission)	\
	nmhSetCmnWithLock(FsMIEcfmExtDefaultMdIdPermission, 14,FsMIEcfmExtDefaultMdIdPermissionSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 3, "%u %i %u %i", u4FsMIEcfmContextId , i4FsMIEcfmExtDefaultMdPrimarySelectorType , u4FsMIEcfmExtDefaultMdPrimarySelector ,i4SetValFsMIEcfmExtDefaultMdIdPermission)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIEcfmExtMaIndex[14];
extern UINT4 FsMIEcfmExtMaPrimarySelectorType[14];
extern UINT4 FsMIEcfmExtMaPrimarySelectorOrNone[14];
extern UINT4 FsMIEcfmExtMaFormat[14];
extern UINT4 FsMIEcfmExtMaName[14];
extern UINT4 FsMIEcfmExtMaMhfCreation[14];
extern UINT4 FsMIEcfmExtMaIdPermission[14];
extern UINT4 FsMIEcfmExtMaCcmInterval[14];
extern UINT4 FsMIEcfmExtMaRowStatus[14];
extern UINT4 FsMIEcfmExtMaCrosscheckStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMIEcfmExtMaFormat(u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , i4FsMIEcfmExtMaPrimarySelectorType , u4FsMIEcfmExtMaPrimarySelectorOrNone ,i4SetValFsMIEcfmExtMaFormat)	\
	nmhSetCmnWithLock(FsMIEcfmExtMaFormat, 14, FsMIEcfmExtMaFormatSet, EcfmCcLock,EcfmCcUnLock, 0, 0, 5, "%u %u %u %i %u %i", u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , i4FsMIEcfmExtMaPrimarySelectorType , u4FsMIEcfmExtMaPrimarySelectorOrNone ,i4SetValFsMIEcfmExtMaFormat)
#define nmhSetFsMIEcfmExtMaName(u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , i4FsMIEcfmExtMaPrimarySelectorType , u4FsMIEcfmExtMaPrimarySelectorOrNone ,pSetValFsMIEcfmExtMaName)	\
	nmhSetCmnWithLock(FsMIEcfmExtMaName, 14, FsMIEcfmExtMaNameSet, EcfmCcLock,EcfmCcUnLock, 0, 0, 5, "%u %u %u %i %u %s", u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , i4FsMIEcfmExtMaPrimarySelectorType , u4FsMIEcfmExtMaPrimarySelectorOrNone ,pSetValFsMIEcfmExtMaName)
#define nmhSetFsMIEcfmExtMaMhfCreation(u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , i4FsMIEcfmExtMaPrimarySelectorType , u4FsMIEcfmExtMaPrimarySelectorOrNone ,i4SetValFsMIEcfmExtMaMhfCreation)	\
	nmhSetCmnWithLock(FsMIEcfmExtMaMhfCreation, 14, FsMIEcfmExtMaMhfCreationSet,EcfmCcLock, EcfmCcUnLock, 0, 0, 5, "%u %u %u %i %u %i", u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , i4FsMIEcfmExtMaPrimarySelectorType , u4FsMIEcfmExtMaPrimarySelectorOrNone ,i4SetValFsMIEcfmExtMaMhfCreation)
#define nmhSetFsMIEcfmExtMaIdPermission(u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , i4FsMIEcfmExtMaPrimarySelectorType , u4FsMIEcfmExtMaPrimarySelectorOrNone ,i4SetValFsMIEcfmExtMaIdPermission)	\
	nmhSetCmnWithLock(FsMIEcfmExtMaIdPermission, 14, FsMIEcfmExtMaIdPermissionSet,EcfmCcLock, EcfmCcUnLock, 0, 0, 5, "%u %u %u %i %u %i", u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , i4FsMIEcfmExtMaPrimarySelectorType , u4FsMIEcfmExtMaPrimarySelectorOrNone ,i4SetValFsMIEcfmExtMaIdPermission)
#define nmhSetFsMIEcfmExtMaCcmInterval(u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , i4FsMIEcfmExtMaPrimarySelectorType , u4FsMIEcfmExtMaPrimarySelectorOrNone ,i4SetValFsMIEcfmExtMaCcmInterval)	\
	nmhSetCmnWithLock(FsMIEcfmExtMaCcmInterval, 14, FsMIEcfmExtMaCcmIntervalSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 5, "%u %u %u %i %u %i", u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , i4FsMIEcfmExtMaPrimarySelectorType , u4FsMIEcfmExtMaPrimarySelectorOrNone ,i4SetValFsMIEcfmExtMaCcmInterval)
#define nmhSetFsMIEcfmExtMaRowStatus(u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , i4FsMIEcfmExtMaPrimarySelectorType , u4FsMIEcfmExtMaPrimarySelectorOrNone ,i4SetValFsMIEcfmExtMaRowStatus)	\
	nmhSetCmnWithLock(FsMIEcfmExtMaRowStatus, 14, FsMIEcfmExtMaRowStatusSet, EcfmCcLock,EcfmCcUnLock, 0, 1, 5, "%u %u %u %i %u %i", u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , i4FsMIEcfmExtMaPrimarySelectorType , u4FsMIEcfmExtMaPrimarySelectorOrNone ,i4SetValFsMIEcfmExtMaRowStatus)
#define nmhSetFsMIEcfmExtMaCrosscheckStatus(u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , i4FsMIEcfmExtMaPrimarySelectorType , u4FsMIEcfmExtMaPrimarySelectorOrNone ,i4SetValFsMIEcfmExtMaCrosscheckStatus)	\
	nmhSetCmnWithLock(FsMIEcfmExtMaCrosscheckStatus, 14, FsMIEcfmExtMaCrosscheckStatusSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 5, "%u %u %u %i %u %i", u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , i4FsMIEcfmExtMaPrimarySelectorType , u4FsMIEcfmExtMaPrimarySelectorOrNone ,i4SetValFsMIEcfmExtMaCrosscheckStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIEcfmExtMipIfIndex[14];
extern UINT4 FsMIEcfmExtMipMdLevel[14];
extern UINT4 FsMIEcfmExtMipSelectorType[14];
extern UINT4 FsMIEcfmExtMipPrimarySelector[14];
extern UINT4 FsMIEcfmExtMipActive[14];
extern UINT4 FsMIEcfmExtMipRowStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMIEcfmExtMipActive(i4FsMIEcfmExtMipIfIndex , i4FsMIEcfmExtMipMdLevel , i4FsMIEcfmExtMipSelectorType , u4FsMIEcfmExtMipPrimarySelector ,i4SetValFsMIEcfmExtMipActive)	\
	nmhSetCmnWithLock(FsMIEcfmExtMipActive, 14, FsMIEcfmExtMipActiveSet,EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%i %i %i %u %i", i4FsMIEcfmExtMipIfIndex , i4FsMIEcfmExtMipMdLevel , i4FsMIEcfmExtMipSelectorType , u4FsMIEcfmExtMipPrimarySelector ,i4SetValFsMIEcfmExtMipActive)
#define nmhSetFsMIEcfmExtMipRowStatus(i4FsMIEcfmExtMipIfIndex , i4FsMIEcfmExtMipMdLevel , i4FsMIEcfmExtMipSelectorType , u4FsMIEcfmExtMipPrimarySelector ,i4SetValFsMIEcfmExtMipRowStatus)	\
	nmhSetCmnWithLock(FsMIEcfmExtMipRowStatus, 14, FsMIEcfmExtMipRowStatusSet, EcfmCcLock, EcfmCcUnLock, 0, 1, 4, "%i %i %i %u %i", i4FsMIEcfmExtMipIfIndex , i4FsMIEcfmExtMipMdLevel , i4FsMIEcfmExtMipSelectorType , u4FsMIEcfmExtMipPrimarySelector ,i4SetValFsMIEcfmExtMipRowStatus)

#endif
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIEcfmExtMepIdentifier[14];
extern UINT4 FsMIEcfmExtMepIfIndex[14];
extern UINT4 FsMIEcfmExtMepDirection[14];
extern UINT4 FsMIEcfmExtMepPrimaryVidOrIsid[14];
extern UINT4 FsMIEcfmExtMepActive[14];
extern UINT4 FsMIEcfmExtMepCciEnabled[14];
extern UINT4 FsMIEcfmExtMepCcmLtmPriority[14];
extern UINT4 FsMIEcfmExtMepLowPrDef[14];
extern UINT4 FsMIEcfmExtMepFngAlarmTime[14];
extern UINT4 FsMIEcfmExtMepFngResetTime[14];
extern UINT4 FsMIEcfmExtMepTransmitLbmStatus[14];
extern UINT4 FsMIEcfmExtMepTransmitLbmDestMacAddress[14];
extern UINT4 FsMIEcfmExtMepTransmitLbmDestMepId[14];
extern UINT4 FsMIEcfmExtMepTransmitLbmDestIsMepId[14];
extern UINT4 FsMIEcfmExtMepTransmitLbmMessages[14];
extern UINT4 FsMIEcfmExtMepTransmitLbmDataTlv[14];
extern UINT4 FsMIEcfmExtMepTransmitLbmVlanIsidPriority[14];
extern UINT4 FsMIEcfmExtMepTransmitLbmVlanIsidDropEnable[14];
extern UINT4 FsMIEcfmExtMepTransmitLtmStatus[14];
extern UINT4 FsMIEcfmExtMepTransmitLtmFlags[14];
extern UINT4 FsMIEcfmExtMepTransmitLtmTargetMacAddress[14];
extern UINT4 FsMIEcfmExtMepTransmitLtmTargetMepId[14];
extern UINT4 FsMIEcfmExtMepTransmitLtmTargetIsMepId[14];
extern UINT4 FsMIEcfmExtMepTransmitLtmTtl[14];
extern UINT4 FsMIEcfmExtMepTransmitLtmEgressIdentifier[14];
extern UINT4 FsMIEcfmExtMepRowStatus[14];
extern UINT4 FsMIEcfmExtMepCcmOffload[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMIEcfmExtMepIfIndex(u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,i4SetValFsMIEcfmExtMepIfIndex)	\
	nmhSetCmnWithLock(FsMIEcfmExtMepIfIndex, 14, FsMIEcfmExtMepIfIndexSet, EcfmCcLock,EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,i4SetValFsMIEcfmExtMepIfIndex)
#define nmhSetFsMIEcfmExtMepDirection(u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,i4SetValFsMIEcfmExtMepDirection)	\
	nmhSetCmnWithLock(FsMIEcfmExtMepDirection, 14, FsMIEcfmExtMepDirectionSet,EcfmCcLock,EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,i4SetValFsMIEcfmExtMepDirection)
#define nmhSetFsMIEcfmExtMepPrimaryVidOrIsid(u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,u4SetValFsMIEcfmExtMepPrimaryVidOrIsid)	\
	nmhSetCmnWithLock(FsMIEcfmExtMepPrimaryVidOrIsid, 14, FsMIEcfmExtMepPrimaryVidOrIsidSet, EcfmCcLock,EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %u", u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,u4SetValFsMIEcfmExtMepPrimaryVidOrIsid)
#define nmhSetFsMIEcfmExtMepActive(u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,i4SetValFsMIEcfmExtMepActive)	\
	nmhSetCmnWithLock(FsMIEcfmExtMepActive, 14, FsMIEcfmExtMepActiveSet, EcfmCcLock,EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,i4SetValFsMIEcfmExtMepActive)
#define nmhSetFsMIEcfmExtMepCciEnabled(u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,i4SetValFsMIEcfmExtMepCciEnabled)	\
	nmhSetCmnWithLock(FsMIEcfmExtMepCciEnabled, 14, FsMIEcfmExtMepCciEnabledSet,EcfmCcLock,EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,i4SetValFsMIEcfmExtMepCciEnabled)
#define nmhSetFsMIEcfmExtMepCcmLtmPriority(u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,u4SetValFsMIEcfmExtMepCcmLtmPriority)	\
	nmhSetCmnWithLock(FsMIEcfmExtMepCcmLtmPriority, 14,FsMIEcfmExtMepCcmLtmPrioritySet,EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %u", u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,u4SetValFsMIEcfmExtMepCcmLtmPriority)
#define nmhSetFsMIEcfmExtMepLowPrDef(u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,i4SetValFsMIEcfmExtMepLowPrDef)	\
	nmhSetCmnWithLock(FsMIEcfmExtMepLowPrDef, 14, FsMIEcfmExtMepLowPrDefSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,i4SetValFsMIEcfmExtMepLowPrDef)
#define nmhSetFsMIEcfmExtMepFngAlarmTime(u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,i4SetValFsMIEcfmExtMepFngAlarmTime)	\
	nmhSetCmnWithLock(FsMIEcfmExtMepFngAlarmTime, 14, FsMIEcfmExtMepFngAlarmTimeSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,i4SetValFsMIEcfmExtMepFngAlarmTime)
#define nmhSetFsMIEcfmExtMepFngResetTime(u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,i4SetValFsMIEcfmExtMepFngResetTime)	\
	nmhSetCmnWithLock(FsMIEcfmExtMepFngResetTime, 14, FsMIEcfmExtMepFngResetTimeSet,EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,i4SetValFsMIEcfmExtMepFngResetTime)
#define nmhSetFsMIEcfmExtMepTransmitLbmStatus(u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,i4SetValFsMIEcfmExtMepTransmitLbmStatus)	\
	nmhSetCmnWithLock(FsMIEcfmExtMepTransmitLbmStatus, 14, FsMIEcfmExtMepTransmitLbmStatusSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,i4SetValFsMIEcfmExtMepTransmitLbmStatus)
#define nmhSetFsMIEcfmExtMepTransmitLbmDestMacAddress(u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,SetValFsMIEcfmExtMepTransmitLbmDestMacAddress)	\
	nmhSetCmnWithLock(FsMIEcfmExtMepTransmitLbmDestMacAddress, 14, FsMIEcfmExtMepTransmitLbmDestMacAddressSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %m", u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,SetValFsMIEcfmExtMepTransmitLbmDestMacAddress)
#define nmhSetFsMIEcfmExtMepTransmitLbmDestMepId(u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,u4SetValFsMIEcfmExtMepTransmitLbmDestMepId)	\
	nmhSetCmnWithLock(FsMIEcfmExtMepTransmitLbmDestMepId, 14, FsMIEcfmExtMepTransmitLbmDestMepIdSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %u", u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,u4SetValFsMIEcfmExtMepTransmitLbmDestMepId)
#define nmhSetFsMIEcfmExtMepTransmitLbmDestIsMepId(u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,i4SetValFsMIEcfmExtMepTransmitLbmDestIsMepId)	\
	nmhSetCmnWithLock(FsMIEcfmExtMepTransmitLbmDestIsMepId, 14, FsMIEcfmExtMepTransmitLbmDestIsMepIdSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,i4SetValFsMIEcfmExtMepTransmitLbmDestIsMepId)
#define nmhSetFsMIEcfmExtMepTransmitLbmMessages(u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,i4SetValFsMIEcfmExtMepTransmitLbmMessages)	\
	nmhSetCmnWithLock(FsMIEcfmExtMepTransmitLbmMessages, 14, FsMIEcfmExtMepTransmitLbmMessagesSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,i4SetValFsMIEcfmExtMepTransmitLbmMessages)
#define nmhSetFsMIEcfmExtMepTransmitLbmDataTlv(u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,pSetValFsMIEcfmExtMepTransmitLbmDataTlv)	\
	nmhSetCmnWithLock(FsMIEcfmExtMepTransmitLbmDataTlv, 14, FsMIEcfmExtMepTransmitLbmDataTlvSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %s", u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,pSetValFsMIEcfmExtMepTransmitLbmDataTlv)
#define nmhSetFsMIEcfmExtMepTransmitLbmVlanIsidPriority(u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,i4SetValFsMIEcfmExtMepTransmitLbmVlanIsidPriority)	\
	nmhSetCmnWithLock(FsMIEcfmExtMepTransmitLbmVlanIsidPriority, 14, FsMIEcfmExtMepTransmitLbmVlanIsidPrioritySet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,i4SetValFsMIEcfmExtMepTransmitLbmVlanIsidPriority)
#define nmhSetFsMIEcfmExtMepTransmitLbmVlanIsidDropEnable(u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,i4SetValFsMIEcfmExtMepTransmitLbmVlanIsidDropEnable)	\
	nmhSetCmnWithLock(FsMIEcfmExtMepTransmitLbmVlanIsidDropEnable, 14, FsMIEcfmExtMepTransmitLbmVlanIsidDropEnableSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,i4SetValFsMIEcfmExtMepTransmitLbmVlanIsidDropEnable)
#define nmhSetFsMIEcfmExtMepTransmitLtmStatus(u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,i4SetValFsMIEcfmExtMepTransmitLtmStatus)	\
	nmhSetCmnWithLock(FsMIEcfmExtMepTransmitLtmStatus, 14, FsMIEcfmExtMepTransmitLtmStatusSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,i4SetValFsMIEcfmExtMepTransmitLtmStatus)
#define nmhSetFsMIEcfmExtMepTransmitLtmFlags(u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,pSetValFsMIEcfmExtMepTransmitLtmFlags)	\
	nmhSetCmnWithLock(FsMIEcfmExtMepTransmitLtmFlags, 14, FsMIEcfmExtMepTransmitLtmFlagsSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %s", u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,pSetValFsMIEcfmExtMepTransmitLtmFlags)
#define nmhSetFsMIEcfmExtMepTransmitLtmTargetMacAddress(u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,SetValFsMIEcfmExtMepTransmitLtmTargetMacAddress)	\
	nmhSetCmnWithLock(FsMIEcfmExtMepTransmitLtmTargetMacAddress, 14, FsMIEcfmExtMepTransmitLtmTargetMacAddressSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %m", u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,SetValFsMIEcfmExtMepTransmitLtmTargetMacAddress)
#define nmhSetFsMIEcfmExtMepTransmitLtmTargetMepId(u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,u4SetValFsMIEcfmExtMepTransmitLtmTargetMepId)	\
	nmhSetCmnWithLock(FsMIEcfmExtMepTransmitLtmTargetMepId, 14, FsMIEcfmExtMepTransmitLtmTargetMepIdSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %u", u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,u4SetValFsMIEcfmExtMepTransmitLtmTargetMepId)
#define nmhSetFsMIEcfmExtMepTransmitLtmTargetIsMepId(u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,i4SetValFsMIEcfmExtMepTransmitLtmTargetIsMepId)	\
	nmhSetCmnWithLock(FsMIEcfmExtMepTransmitLtmTargetIsMepId, 14, FsMIEcfmExtMepTransmitLtmTargetIsMepIdSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,i4SetValFsMIEcfmExtMepTransmitLtmTargetIsMepId)
#define nmhSetFsMIEcfmExtMepTransmitLtmTtl(u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,u4SetValFsMIEcfmExtMepTransmitLtmTtl)	\
	nmhSetCmnWithLock(FsMIEcfmExtMepTransmitLtmTtl, 14, FsMIEcfmExtMepTransmitLtmTtlSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %u", u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,u4SetValFsMIEcfmExtMepTransmitLtmTtl)
#define nmhSetFsMIEcfmExtMepTransmitLtmEgressIdentifier(u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,pSetValFsMIEcfmExtMepTransmitLtmEgressIdentifier)	\
	nmhSetCmnWithLock(FsMIEcfmExtMepTransmitLtmEgressIdentifier, 14, FsMIEcfmExtMepTransmitLtmEgressIdentifierSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %s", u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,pSetValFsMIEcfmExtMepTransmitLtmEgressIdentifier)
#define nmhSetFsMIEcfmExtMepRowStatus(u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,i4SetValFsMIEcfmExtMepRowStatus)	\
	nmhSetCmnWithLock(FsMIEcfmExtMepRowStatus, 14,FsMIEcfmExtMepRowStatusSet,EcfmCcLock, EcfmCcUnLock, 0, 1, 4, "%u %u %u %u %i", u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,i4SetValFsMIEcfmExtMepRowStatus)
#define nmhSetFsMIEcfmExtMepCcmOffload(u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,i4SetValFsMIEcfmExtMepCcmOffload)	\
	nmhSetCmnWithLock(FsMIEcfmExtMepCcmOffload, 14,FsMIEcfmExtMepCcmOffloadSet,EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIEcfmContextId , u4FsMIEcfmMdIndex , u4FsMIEcfmExtMaIndex , u4FsMIEcfmExtMepIdentifier ,i4SetValFsMIEcfmExtMepCcmOffload)

#endif
