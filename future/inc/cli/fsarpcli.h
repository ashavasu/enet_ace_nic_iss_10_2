/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsarpcli.h,v 1.4 2015/09/13 09:21:49 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsArpCacheTimeout[10];
extern UINT4 FsArpCachePendTime[10];
extern UINT4 FsArpMaxRetries[10];
extern UINT4 FsArpGlobalDebug [10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsArpCacheTimeout(i4SetValFsArpCacheTimeout) \
 nmhSetCmn(FsArpCacheTimeout, 10, FsArpCacheTimeoutSet, ArpProtocolLock, ArpProtocolUnLock, 0, 0, 0, "%i", i4SetValFsArpCacheTimeout)
#define nmhSetFsArpCachePendTime(i4SetValFsArpCachePendTime) \
 nmhSetCmn(FsArpCachePendTime, 10, FsArpCachePendTimeSet, ArpProtocolLock, ArpProtocolUnLock, 0, 0, 0, "%i", i4SetValFsArpCachePendTime)
#define nmhSetFsArpMaxRetries(i4SetValFsArpMaxRetries) \
 nmhSetCmn(FsArpMaxRetries, 10, FsArpMaxRetriesSet, ArpProtocolLock, ArpProtocolUnLock, 0, 0, 0, "%i", i4SetValFsArpMaxRetries)
#define nmhSetFsArpGlobalDebug(i4SetValFsArpGlobalDebug) \
        nmhSetCmn(FsArpGlobalDebug, 10, FsArpGlobalDebugSet, ArpProtocolLock, ArpProtocolUnLock, 0, 0, 0, "%i", i4SetValFsArpGlobalDebug)

#endif
