/********************************************************************
 * Copyright (C) 2015 Aricent Inc . All Rights Reserved
 *
 * $Id: r2544cli.h,v 1.14 2016/06/14 12:29:05 siva Exp $
 *
 * Description: This file contains RFC2544 CLI command constants, error
 *              strings and function prototypes.
 *
 *******************************************************************/


#ifndef __R2544CLI_H__
#define __R2544CLI_H__

#include "cli.h"

/* 
 * R2544 CLI command constants. To add a new command ,a new definition needs to
 * be added to the list.
 */


enum {
    CLI_RFC2544_SHUTDOWN = 1,
    CLI_RFC2544_START,
    CLI_RFC2544_ENABLE,
    CLI_RFC2544_DISABLE,
    CLI_RFC2544_SLA_CREATE,
    CLI_RFC2544_SLA_DELETE,
    CLI_RFC2544_TRAF_PROF_CREATE,
    CLI_RFC2544_TRAF_PROF_DELETE,
    CLI_RFC2544_SAC_CREATE,
    CLI_RFC2544_SAC_DELETE,
    CLI_RFC2544_NOTIFY_ENABLE,
    CLI_RFC2544_NOTIFY_DISABLE,
    CLI_RFC2544_SLA_TEST,
    CLI_RFC2544_DELETE_REPORT_SLA,
    CLI_RFC2544_SAVE_SLA_REPORT,
    CLI_RFC2544_TFTP_SLA_REPORT,
    CLI_RFC2544_SHOW_GLOBAL_INFO,
    CLI_RFC2544_SHOW_SAC,
    CLI_RFC2544_SHOW_TRAF_PROF,
    CLI_RFC2544_SHOW_SLA,
    CLI_RFC2544_SHOW_SLA_REPORT,
    CLI_RFC2544_DEBUG,
    CLI_RFC2544_NO_DEBUG,
    CLI_RFC2544_MAP_TRAF_PROFILE,
    CLI_RFC2544_UNMAP_TRAF_PROFILE,
    CLI_RFC2544_MAP_SAC,
    CLI_RFC2544_UNMAP_SAC,
    CLI_RFC2544_MAP_CFM_CFG,
    CLI_RFC2544_UNMAP_CFM_CFG,
    CLI_RFC2544_TRAF_PROF_NAME,
    CLI_RFC2544_THROUGHPUT_ENABLE,
    CLI_RFC2544_THROUGHPUT_DISABLE,
    CLI_RFC2544_LATENCY_ENABLE,
    CLI_RFC2544_LATENCY_DISABLE,
    CLI_RFC2544_FRAMELOSS_ENABLE,
    CLI_RFC2544_FRAMELOSS_DISABLE,
    CLI_RFC2544_BACKTOBACK_ENABLE,
    CLI_RFC2544_BACKTOBACK_DISABLE,
    CLI_RFC2544_TRAF_PROF_FRAMESIZE,
    CLI_RFC2544_TRAF_PROF_SEQCHK_ENABLE,
    CLI_RFC2544_TRAF_PROF_PCP,
    CLI_RFC2544_TRAF_PROF_DWELL_TIME,
    CLI_RFC2544_TRAF_PROF_SEQCHK_DISABLE,
    CLI_RFC2544_SAC_THROUGHPUT_PARAMS,
    CLI_RFC2544_SAC_LATENCY_PARAMS,
    CLI_RFC2544_SAC_FRAMELOSS_PARAMS,
    CLI_RFC2544_SHOW_SLA_DETAIL,
    CLI_RFC2544_SHOW_SLA_BRIEF,
    CLI_RFC2544_MAX_COMMANDS
};

enum
{
    CLI_RFC2544_SLA_START = 1,
    CLI_RFC2544_SLA_STOP    
};

enum {
    CLI_RFC2544_UNKNOWN_ERR = 1,
    CLI_RFC2544_INVALID_SYSTEM_CONTROL,
    CLI_RFC2544_INVALID_CONTEXT_ID,
    CLI_RFC2544_CONTEXT_NOT_PRESENT,
    CLI_RFC2544_ERR_MODULE_SHUTDOWN,
    CLI_RFC2544_ERR_MODULE_DISABLED,
    CLI_RFC2544_INVALID_MODULE_STATUS,
    CLI_RFC2544_SERVICE_NOT_PRESENT,
    CLI_RFC2544_SERVICE_NOT_CONFIGURED,
    CLI_RFC2544_SLA_TRAFPROF_NOT_PRESENT,
    CLI_RFC2544_SLA_TRAFPROF_NOT_CONFIGURED,
    CLI_RFC2544_SLA_SAC_NOT_PRESENT,
    CLI_RFC2544_SLA_SAC_NOT_CONFIGURED,
    CLI_RFC2544_ERR_SLA_TRAF_PROF_MAP,
    CLI_RFC2544_ERR_SLA_SAC_MAP,
    CLI_RFC2544_INVALID_SLA_MEG,
    CLI_RFC2544_INVALID_SLA_ME,
    CLI_RFC2544_INVALID_SLA_MEP,
    CLI_RFC2544_INVALID_SLA_MEGLEVEL,
    CLI_RFC2544_INVALID_SLA_ID,
    CLI_RFC2544_INVALID_SAC_ID,
    CLI_RFC2544_INVALID_TRAF_PROF_ID,
    CLI_RFC2544_INVALID_TRAF_PROF_SEQ_NO_CHECK,
    CLI_RFC2544_INVALID_TRAF_PROF_DWELL_TIME,
    CLI_RFC2544_INVALID_TRAF_PROF_FRAME_SIZE,
    CLI_RFC2544_INVALID_TRAF_PROF_PCP,
    CLI_RFC2544_INVALID_TH_TEST_STATUS,
    CLI_RFC2544_INVALID_FL_TEST_STATUS,
    CLI_RFC2544_INVALID_LA_TEST_STATUS,
    CLI_RFC2544_INVALID_BB_TEST_STATUS,
    CLI_RFC2544_INVALID_TH_TRIAL_DURATION,
    CLI_RFC2544_INVALID_LA_TRIAL_DURATION,
    CLI_RFC2544_INVALID_FL_TRIAL_DURATION,
    CLI_RFC2544_INVALID_BB_TRIAL_DURATION,
    CLI_RFC2544_INVALID_LA_DELAY_MEASURE_INTERVAL,
    CLI_RFC2544_INVALID_BB_TRIAL_COUNT,
    CLI_RFC2544_INVALID_TH_FRAMELOSS,
    CLI_RFC2544_INVALID_LA_FRAMELOSS,
    CLI_RFC2544_INVALID_FL_FRAMELOSS,
    CLI_RFC2544_INVALID_TEST_STATUS,
    CLI_RFC2544_ERR_TRAF_PROF_MAPPED,
    CLI_RFC2544_ERR_SAC_MAPPED,
    CLI_RFC2544_INVALID_MIN_RATE,
    CLI_RFC2544_INVALID_MAX_RATE,
    CLI_RFC2544_INVALID_RATE_STEP,
    CLI_RFC2544_ERR_TH_MIN_RATE_GREATER_THAN_MAX_RATE,
    CLI_RFC2544_INVALID_MIN_RATE_ERR_GREATER,
    CLI_RFC2544_INVALID_MIN_RATE_ERR,
    CLI_RFC2544_ERR_TEST_IN_PROGRESS,
    CLI_RFC2544_SLA_NOT_PRESENT,    
    CLI_RFC2544_SAC_NOT_PRESENT,    
    CLI_RFC2544_TRAF_PROF_NOT_PRESENT,    
    CLI_RFC2544_ERR_TEST_START,
    CLI_RFC2544_ERR_TEST_STOP,
    CLI_RFC2544_ERR_TEST_COMPLETED,
    CLI_RFC2544_ERR_SLA_VALIDATION,
    CLI_RFC2544_TRAF_PROF_NAME_ERR,
    CLI_RFC2544_TRAF_PROF_NAME_CONF_ERR,
    CLI_RFC2544_TRAF_PROF_DWELL_TIME_CONF_ERR,
    CLI_RFC2544_TRAF_PROF_SEQ_NO_CHECK_CONF_ERR,
    CLI_RFC2544_TRAF_PROF_FRAME_SIZE_CONF_ERR,
    CLI_RFC2544_TRAF_PROF_PCP_CONF_ERR,
    CLI_RFC2544_TH_TEST_STATUS_CONF_ERR,
    CLI_RFC2544_FL_TEST_STATUS_CONF_ERR,
    CLI_RFC2544_LA_TEST_STATUS_CONF_ERR,
    CLI_RFC2544_BB_TEST_STATUS_CONF_ERR,
    CLI_RFC2544_TH_TRIAL_DURATION_CONF_ERR,
    CLI_RFC2544_TH_MAX_RATE_CONF_ERR,
    CLI_RFC2544_TH_MIN_RATE_CONF_ERR,
    CLI_RFC2544_LA_TRIAL_DURATION_CONF_ERR,
    CLI_RFC2544_LA_DELAY_MEASURE_INTERVAL_CONF_ERR,
    CLI_RFC2544_FL_TRIAL_DURATION_CONF_ERR,
    CLI_RFC2544_FL_MAX_RATE_CONF_ERR,
    CLI_RFC2544_FL_MIN_RATE_CONF_ERR,
    CLI_RFC2544_FL_RATE_STEP_CONF_ERR,
    CLI_RFC2544_BB_TRIAL_DURATION_CONF_ERR,
    CLI_RFC2544_BB_TRIAL_COUNT_CONF_ERR,
    CLI_RFC2544_TH_FRAMELOSS_CONF_ERR,
    CLI_RFC2544_LA_FRAMELOSS_CONF_ERR,
    CLI_RFC2544_FL_FRAMELOSS_CONF_ERR,
    CLI_RFC2544_MEG_MEP_ME_MAPPED,
    CLI_RFC2544_ECFM_NOT_SETTLED,
    CLI_RFC2544_DELETE_SLA_REPORT,
    CLI_RFC2544_SLA_SAC_NOT_MAPPED,
    CLI_RFC2544_SLA_TRAF_PROF_NOT_MAPPED,
    CLI_RFC2544_SLA_CONF_ERR,
    CLI_RFC2544_TRAF_PROF_CONF_ERR,
    CLI_RFC2544_SAC_CONF_ERR,
    CLI_RFC2544_ERR_MAP_TEST_IN_PROGRESS,
    CLI_RFC2544_INVALID_MIN_FRAME_SIZE,
    CLI_RFC2544_INVALID_FRAME_SIZE,
    CLI_RFC2544_INVALID_REPEAT_FRAME_SIZE,
    CLI_RFC2544_REMOTE_MEP_NOT_PRESENT,
    CLI_RFC2544_TEST_PARAMETERS_NOT_PRESENT,
    CLI_RFC2544_ME_MEG_MEP_CONF_ERR,
    CLI_RFC2544_SUBTEST_NOT_ENABLED,
    CLI_RFC2544_NON_ZERO_RATE_STEP_ERR, 
    CLI_RFC2544_ZERO_RATE_STEP_ERR, 
    CLI_RFC2544_MAX_RATE_ERR, 
    CLI_RFC2544_MIN_RATE_ERR,     
    CLI_RFC2544_MAX_ERR
};

#ifdef  _R2544CLI_C_
CONST CHR1  *gaR2544CliErrString [] = {
    NULL,
    "Unknown Error\r\n",
    "% Invalid System Control \r\n",
    "% Invalid Context ID \r\n",
    "% Context does not exist \r\n",
    "% RFC2544 Module is shutdown in context \r\n",
    "% RFC2544 Module is Disabled. Enable Rf2544 Module to start the test\r\n",
    "% Invalid Module status \r\n",
    "% MEP/ME/MEG configured for the SLA does not exist \r\n",
    "% MEP/ME/MEG is not configured for the SLA \r\n",
    "% Traffic Profile Entry does not exist for the traffic profile Id configured to SLA \r\n",
    "% Traffic Profile is not configured for the SLA \r\n",
    "% Sac Entry  does not exist for the Sac Id configured to SLA \r\n",
    "% Sac is not configured for the SLA \r\n",
    "% Traffic profile is already mapped. Unmap the existing traffic profile Id and map the new Traffic Profile Id\r\n",
    "% Sac is already mapped. Unmap the existing Sac Id and map the new Sac Id\r\n",
    "% Invalid SLA MEG value\r\n",
    "% Invalid SLA ME value\r\n",
    "% Invalid SLA MEP value\r\n",
    "% Invalid SLA MEGLEVEL value\r\n",
    "% Invalid SLA Identifier \r\n",
    "% Invalid SAC Identifier\r\n",
    "% Invalid Traffic profile Identifier \r\n",
    "% Invalid Traffic profile sequence number check \r\n",
    "% Invalid Traffic profile Dwell time\r\n",
    "% Invalid Traffic profile Frame size\r\n",
    "% Invalid Traffic profile PCP\r\n",
    "% Invalid Throughput test status\r\n",
    "% Invalid frameloss test status\r\n",
    "% Latency test should be enabled only when throughput test is enabled\r\n",
    "% Invalid Back-to-Back test status\r\n",
    "% Invalid Throughput test trial duration\r\n",
    "% Invalid Latency test trial duration\r\n",
    "% Invalid frameloss test trial duration\r\n",
    "% Invalid Back-to-BacK test trial duration\r\n",
    "% Invalid latency test delay measure interval\r\n",
    "% Invalid Back-to-Back test trial count. The value configured for  is not within the range\r\n",
    "% Invalid frameloss value for Throughput test. The value configured for frame loss is not within the range\r\n",
    "% Invalid frameloss value for Latency test. The value configured for frame loss is not within the range\r\n",
    "% Invalid frameloss value for frameloss test. The value configured for frame loss is not within the range\r\n",
    "% Invalid test status. Test status should be start or stop\r\n",
    "% Traffic Profile is mapped to SLA. Unmap Traffic profile from SLA to delete Traffic Profile\r\n",
    "% SAC is mapped to SLA. Unmap SAC from SLA to delete SAC\r\n",
    "% Invalid Minimum Rate. The value for Maximum rate configured is not within the range<\r\n",
    "% Invalid Maximum Rate. The value for Minimum rate configured is not within the range\r\n",
    "% Invalid Rate Step. The value for rate configured in not within the range\r\n",
    "% Min rate should not be greater than max rate for Throughput test\r\n",
    "% Min rate should not be greater than or equal to max rate for FrameLoss test \r\n",
    "% Configured Min/Max should support  atleast one trial for Throughput Test and two trials for FrameLoss test\r\n",
    "% Test is in-progress. Hence test cannot be initiated again.\r\n",
    "% Sla Entry not present.\r\n",
    "% Sac Entry not present.\r\n",
    "% Traffic Profile Entry not present.\r\n",
    "% RFC2544 Module is Disabled in the context. Enable Rf2544 Module to start the test.\r\n",
    "% RFC2544 Module is Disabled in the context. Test will not be running when the module is disabled.\r\n",
    "% Test is not in-progress. Hence need not stop it again.\r\n",
    "% Mandatory Parameters are not filled. Either MEP/ME/MEG/TP/SAC are not configured or MEP/ME/MEG/TP/SAC configured to the SLA does not exist.\r\n",
    "% Only characters  (A-Z, a-z) and numeric values (0-9) are allowed to configure traffic profile name .\r\n",
    "% Traffic profile name cannot be modified as Test is in progress. \r\n",
    "% Test is in progress hence Dwell Time cannot be modified. \r\n",
    "% Test is in progress hence Sequence no cannot be modified. \r\n",
    "% Test is in progress hence Frame size cannot be modified. \r\n",
    "% Test is in progress hence PCP cannot be modified. \r\n",
    "% Test is in progress hence Throughput Test Status cannot be modified. \r\n",
    "% Test is in progress hence Frame Loss Test Status cannot be modified. \r\n",
    "% Test is in progress hence Latency Test Status cannot be modified. \r\n",
    "% Test is in progress hence Back to back Test Status cannot be modified. \r\n",
    "% Test is in progress hence Throughput Test Trail Duration cannot be modified. \r\n",
    "% Test is in progress hence Throughput Test max rate cannot be modified. \r\n",
    "% Test is in progress hence Throughput Test min rate cannot be modified. \r\n",
    "% Test is in progress hence Latency Test Trail Duration cannot be modified. \r\n",
    "% Test is in progress hence Latency Test Delay Measurement Interval cannot be modified . \r\n",
    "% Test is in progress hence Frameloss Test Trail Duration cannot be modified. \r\n",
    "% Test is in progress hence Frameloss Test Trail max rate cannot be modified. \r\n",
    "% Test is in progress hence Frameloss Test Trail min rate cannot be modified. \r\n",
    "% Test is in progress hence Frameloss Test Trail rate step cannot be modified. \r\n",
    "% Test is in progress hence Back to back Test Trail Duration cannot be modified. \r\n",
    "% Test is in progress hence Back to back Test Trail Count cannot be modified. \r\n",
    "% Test is in progress hence Throughput Test Allowed Frameloss cannot be modified. \r\n",
    "% Test is in progress hence Latency Test Allowed Frameloss cannot be modified. \r\n",
    "% Test is in progress hence Frameloss Test Allowed Frameloss cannot be modified. \r\n",
    "% MEG MEP and MEP are already mapped to SLA. Hence cannot be mapped again. \r\n",
    "% ECFM is not settled .Thus test initiation failed. \r\n",
    "% Test reports were deleted.\r\n",
    "% No SAC is mapped to SLA.\r\n",
    "% No Traffic Profile is mapped to SLA.\r\n",
    "% Test is in progress hence SLA cannot be deleted. \r\n",
    "% Test is in progress hence Traffic Profile cannot be unmapped. \r\n",
    "% Test is in progress hence SAC cannot be unmapped. \r\n",
    "% Test is in progress hence cannot be UnMapped/Mapped. \r\n",
    "% At least five frame sizes should specify\r\n",
    "% Invalid frame size format for Traffic profile\r\n",
    "% Same frame-size in the frame size string for Trafficprofile is not allowed.\r\n",
    "% Remote MEP is not present hence test cannot be initiated. \r\n",
    "% Failed to fetch the test parameters hence test cannot be initiated. \r\n",
    "% Test is in progress hence MEP/ME/MEG cannot be modified. \r\n",
    "% All subtest are disabled. Hence test cannot be initiated on this SLA. \r\n",
    "% Only zero rate step is configurable when max and min rate are equal\r\n", 
    "% Only non-zero rate step is configurable when min rate is less than max rate\r\n", 
    "% Max rate should be in multiples of 10\r\n", 
    "% Min rate should be in multiples of 10\r\n", 
    "\r\n",
};
#else
extern CONST CHR1  *gaR2544CliErrString[];
#endif



#define RFC2544_CLI_MAX_ARGS          15
#define RFC2544_CLI_INVALID_CONTEXT   0xFFFFFFFF

#define CLI_RFC2544_ZERO                  0
#define CLI_RFC2544_FIVE                  5  
#define CLI_RFC2544_TEN                   10 
#define CLI_RFC2544_INVALID_VALUE        -1 
#define CLI_RFC2544_DEF_DWELL_TIME             5
#define CLI_RFC2544_DEF_PCP                    1
#define CLI_RFC2544_DEF_TH_TRIALDURATION       60
#define CLI_RFC2544_DEF_LA_TRIALDURATION       120
#define CLI_RFC2544_DEF_FL_TRIALDURATION       60
#define CLI_RFC2544_DEF_LA_DELAY_INTERVAL      10
#define CLI_RFC2544_DEF_BB_TRIALDURATION       2000 
#define CLI_RFC2544_DEF_BB_TRIALCOUNT          50  
#define CLI_RFC2544_DEF_TH_ALL_FRAME_LOSS  0
#define CLI_RFC2544_DEF_LA_ALL_FRAME_LOSS  0
#define CLI_RFC2544_DEF_FL_ALL_FRAME_LOSS  0
#define CLI_RFC2544_DEF_TH_RATESTEP       10
#define CLI_RFC2544_DEF_FL_RATESTEP       10  
#define CLI_RFC2544_DEFAULT_FRAMESIZE     {"64,128,256,512,1024,1280,1518"}
#define CLI_RFC2544_TRAF_PROF_SEQNOCHECK_ENABLE 1
#define CLI_RFC2544_TRAF_PROF_SEQNOCHECK_DISABLE 2
#define CLI_RFC2544_MAX_RATE            100
#define CLI_RFC2544_MIN_RATE            80


INT4 cli_process_r2544_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);
INT4 cli_process_r2544_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);
INT4 cli_process_r2544_test_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);


INT4 R2544CliSetSystemControl (tCliHandle CliHandle, UINT4 u4ContextId, INT4 i4SystemControl);

INT4 R2544CliSetModuleStatus (tCliHandle CliHandle, UINT4 u4ContextId, INT4 i4ModuleStatus);

INT4 R2544CliSetSlaCreate (tCliHandle CliHandle,UINT4 u4ContextId,
                            UINT4 u4SlaId);

INT4 R2544CliSetSlaDelete (tCliHandle CliHandle, UINT4 u4ContextId,  UINT4 u4SlaId);

INT4 R2544CliSetTrafProfCreate (tCliHandle CliHandle,UINT4 u4ContextId,
                            UINT4 u4TrafficProfileId);

INT4 R2544CliSetTrafProfDelete (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4TrafficProfileId);


INT4 R2544CliSetSacCreate (tCliHandle CliHandle, UINT4 u4ContextId,
                            UINT4 u4SacId);

INT4 R2544CliSetSacDelete (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4SacId);

INT4 R2544CliSetTrapNotification (tCliHandle CliHandle, UINT4 u4ContextId, INT4 i4TrapNotification);

INT4 R2544CliSetSlaMapTrafficProfile (tCliHandle CliHandle, UINT4 u4ContextId,
                            UINT4 u4SlaId, UINT4 u4TrafProfId);

INT4 R2544CliSetSlaUnMapTrafficProfile (tCliHandle CliHandle, UINT4 u4ContextId,
                                        UINT4 u4SlaId);

INT4 R2544CliSetSlaMapSac (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4SlaId, UINT4 u4SacId);

INT4 R2544CliSetSlaUnMapSac (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4SlaId);

INT4 R2544CliSetSlaMapCfmEntries (tCliHandle CliHandle, UINT4 u4ContextId,
                            UINT4 u4SlaId, UINT4 u4SlaMeg, UINT4 u4SlaMe,
                            UINT4 u4SlaMep);

INT4 R2544CliSetSlaUnMapCfmEntries (tCliHandle CliHandle, UINT4 u4ContextId,
                        UINT4 u4SlaId);

INT4 R2544CliSetThroughputParams (tCliHandle CliHandle, UINT4 u4ContextId,
                        UINT4 u4TrafficProfileId, INT4 i4ThroughputStatus,
                        UINT4 u4ThTrialDuration,UINT4 u4ThMinRate,
                        UINT4 u4ThMaxRate, UINT4 u4ThRateStep);

INT4 R2544CliSetLatencyParams (tCliHandle CliHandle, UINT4 u4ContextId,
                          UINT4 u4TrafficProfileId, INT4 i4LatencyStatus,
                          UINT4 u4LaTrialDuration,UINT4 u4LaDelayInterval);

INT4 R2544CliSetFrameLossParams (tCliHandle CliHandle, UINT4 u4ContextId,
                        UINT4 u4TrafficProfileId, INT4 i4FrameLossStatus,
                        UINT4 u4FlTrialDuration,UINT4 u4FlMinRate,
                        UINT4 u4FlMaxRate, UINT4 u4FlRateStep);

INT4 R2544CliSetBackToBackParams (tCliHandle CliHandle, UINT4 u4ContextId,
                        UINT4 u4TrafficProfileId, INT4 i4BackToBackStatus,
                        UINT4 u4BbTrialDuration,UINT4 u4BbTrialCount);

INT4 R2544CliSetFrameSize (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4TrafficProfileId,
                        UINT1 *pu1FrameSize);

INT4 R2544CliSetSeqNumCheck (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4TrafficProfileId,
                        INT4 i4SeqNumCheck);

INT4 R2544CliSetPCP (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4TrafficProfileId,
                        INT4 i4Pcp);

INT4 R2544CliSetDwellTime (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4TrafficProfileId,
                                INT4 i4DwellTime);

INT4 R2544CliSetThAllowedFrameLoss (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4SacId,
                                UINT4 u4ThAllowedFrameLoss);

INT4 R2544CliSetLaAllowedFrameLoss (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4SacId,
                                UINT4 u4LaAllowedFrameLoss);

INT4 R2544CliSetFlAllowedFrameLoss (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4SacId,
                                UINT4 u4FlAllowedFrameLoss);

INT4 R2544CliSetTraceLevel (tCliHandle CliHandle, UINT4 u4ContextId, 
                            UINT4 u4TraceInput,
                             UINT1 u1TraceStatus);

INT4 R2544CliSetTestStatus (tCliHandle CliHandle, UINT4 u4ContextId, 
                            UINT4 u4SlaId,
                            INT4 i4TestStatus);

INT4
R2544CliGetContextInfoForShowCmd (tCliHandle CliHandle, 
                                  UINT1 *pu1ContextName,
                                  UINT4 u4CurrContextId, 
                                   UINT4 *pu4ContextId);
INT4
R2544CliShowGlobalInfo (tCliHandle CliHandle, UINT4 u4ContextId);

INT4
R2544CliShowSac (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4SacId, UINT4 u4SwFlag);

INT4
R2544CliShowTrafficProfile (tCliHandle CliHandle, UINT4 u4ContextId, UINT4
                               u4TrafficProfileID, UINT4 u4SwFlag);

INT4
R2544CliShowSla (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4SlaId,
                       UINT4 u1DisplayType);

INT4
R2544CliShowSlaReport (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4SlaId);

INT4
R2544ShowRunningConfigCxtCmds (tCliHandle CliHandle);

INT4
R2544ShowRunningConfigSlaCmds (tCliHandle CliHandle);

INT4
R2544ShowRunningConfigSacCmds (tCliHandle CliHandle);

INT4
R2544ShowRunningConfigTrafProfCmds (tCliHandle CliHandle);

INT1
R2544GetSlaCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

INT1
R2544GetTrafficProfileCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

INT1
R2544GetSacCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

INT1
R2544GetVcmSlaCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

INT1
R2544GetVcmTrafficProfileCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

INT1
R2544GetVcmSacCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

VOID 
R2544CliShowDebug (tCliHandle CliHandle);


INT4
R2544CliSetTrafProfName (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4TrafficProfileId,
                UINT1 *pu1TrafProfName);

INT4
R2544ShowRunningConfig (tCliHandle CliHandle);

INT4
R2544CliSaveSlaReport (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4Command,
                                CHR1 * pu1FileName, UINT4 u4SlaId);

INT4
R2544CliDeleteReport (tCliHandle CliHandle, CHR1 * pu1FileName);


#ifdef RFC2544_TEST_WANTED
#define CLI_R2544_UT 1
#endif


#endif /* __RFC2544CLI_H__ */
