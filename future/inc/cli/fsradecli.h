/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsradecli.h,v 1.1 2015/04/28 11:45:05 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsRadExtDebugMask[12];
extern UINT4 FsRadExtMaxNoOfUserEntries[12];
extern UINT4 FsRadExtPrimaryServerAddressType[12];
extern UINT4 FsRadExtPrimaryServer[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsRadExtDebugMask(i4SetValFsRadExtDebugMask)	\
	nmhSetCmnWithLock(FsRadExtDebugMask, 12, FsRadExtDebugMaskSet, RadiusLock, RadiusUnLock, 0, 0, 0, "%i", i4SetValFsRadExtDebugMask)
#define nmhSetFsRadExtMaxNoOfUserEntries(i4SetValFsRadExtMaxNoOfUserEntries)	\
	nmhSetCmnWithLock(FsRadExtMaxNoOfUserEntries, 12, FsRadExtMaxNoOfUserEntriesSet, RadiusLock, RadiusUnLock, 0, 0, 0, "%i", i4SetValFsRadExtMaxNoOfUserEntries)
#define nmhSetFsRadExtPrimaryServerAddressType(i4SetValFsRadExtPrimaryServerAddressType)	\
	nmhSetCmnWithLock(FsRadExtPrimaryServerAddressType, 12, FsRadExtPrimaryServerAddressTypeSet, RadiusLock, RadiusUnLock, 0, 0, 0, "%i", i4SetValFsRadExtPrimaryServerAddressType)
#define nmhSetFsRadExtPrimaryServer(pSetValFsRadExtPrimaryServer)	\
	nmhSetCmnWithLock(FsRadExtPrimaryServer, 12, FsRadExtPrimaryServerSet, RadiusLock, RadiusUnLock, 0, 0, 0, "%s", pSetValFsRadExtPrimaryServer)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsRadExtServerIndex[14];
extern UINT4 FsRadExtServerAddrType[14];
extern UINT4 FsRadExtServerAddress[14];
extern UINT4 FsRadExtServerType[14];
extern UINT4 FsRadExtServerSharedSecret[14];
extern UINT4 FsRadExtServerEnabled[14];
extern UINT4 FsRadExtServerResponseTime[14];
extern UINT4 FsRadExtServerMaximumRetransmission[14];
extern UINT4 FsRadExtServerEntryStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsRadExtServerAddrType(i4FsRadExtServerIndex ,i4SetValFsRadExtServerAddrType)	\
	nmhSetCmnWithLock(FsRadExtServerAddrType, 14, FsRadExtServerAddrTypeSet, RadiusLock, RadiusUnLock, 0, 0, 1, "%i %i", i4FsRadExtServerIndex ,i4SetValFsRadExtServerAddrType)
#define nmhSetFsRadExtServerAddress(i4FsRadExtServerIndex ,pSetValFsRadExtServerAddress)	\
	nmhSetCmnWithLock(FsRadExtServerAddress, 14, FsRadExtServerAddressSet, RadiusLock, RadiusUnLock, 0, 0, 1, "%i %s", i4FsRadExtServerIndex ,pSetValFsRadExtServerAddress)
#define nmhSetFsRadExtServerType(i4FsRadExtServerIndex ,i4SetValFsRadExtServerType)	\
	nmhSetCmnWithLock(FsRadExtServerType, 14, FsRadExtServerTypeSet, RadiusLock, RadiusUnLock, 0, 0, 1, "%i %i", i4FsRadExtServerIndex ,i4SetValFsRadExtServerType)
#define nmhSetFsRadExtServerSharedSecret(i4FsRadExtServerIndex ,pSetValFsRadExtServerSharedSecret)	\
	nmhSetCmnWithLock(FsRadExtServerSharedSecret, 14, FsRadExtServerSharedSecretSet, RadiusLock, RadiusUnLock, 0, 0, 1, "%i %s", i4FsRadExtServerIndex ,pSetValFsRadExtServerSharedSecret)
#define nmhSetFsRadExtServerEnabled(i4FsRadExtServerIndex ,i4SetValFsRadExtServerEnabled)	\
	nmhSetCmnWithLock(FsRadExtServerEnabled, 14, FsRadExtServerEnabledSet, RadiusLock, RadiusUnLock, 0, 0, 1, "%i %i", i4FsRadExtServerIndex ,i4SetValFsRadExtServerEnabled)
#define nmhSetFsRadExtServerResponseTime(i4FsRadExtServerIndex ,i4SetValFsRadExtServerResponseTime)	\
	nmhSetCmnWithLock(FsRadExtServerResponseTime, 14, FsRadExtServerResponseTimeSet, RadiusLock, RadiusUnLock, 0, 0, 1, "%i %i", i4FsRadExtServerIndex ,i4SetValFsRadExtServerResponseTime)
#define nmhSetFsRadExtServerMaximumRetransmission(i4FsRadExtServerIndex ,i4SetValFsRadExtServerMaximumRetransmission)	\
	nmhSetCmnWithLock(FsRadExtServerMaximumRetransmission, 14, FsRadExtServerMaximumRetransmissionSet, RadiusLock, RadiusUnLock, 0, 0, 1, "%i %i", i4FsRadExtServerIndex ,i4SetValFsRadExtServerMaximumRetransmission)
#define nmhSetFsRadExtServerEntryStatus(i4FsRadExtServerIndex ,i4SetValFsRadExtServerEntryStatus)	\
	nmhSetCmnWithLock(FsRadExtServerEntryStatus, 14, FsRadExtServerEntryStatusSet, RadiusLock, RadiusUnLock, 0, 1, 1, "%i %i", i4FsRadExtServerIndex ,i4SetValFsRadExtServerEntryStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsRadExtAuthServerIndex[14];
extern UINT4 FsRadExtAuthClientServerPortNumber[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsRadExtAuthClientServerPortNumber(i4FsRadExtAuthServerIndex ,i4SetValFsRadExtAuthClientServerPortNumber)	\
	nmhSetCmnWithLock(FsRadExtAuthClientServerPortNumber, 14, FsRadExtAuthClientServerPortNumberSet, RadiusLock, RadiusUnLock, 0, 0, 1, "%i %i", i4FsRadExtAuthServerIndex ,i4SetValFsRadExtAuthClientServerPortNumber)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsRadExtAccServerIndex[14];
extern UINT4 FsRadExtAccClientServerPortNumber[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsRadExtAccClientServerPortNumber(i4FsRadExtAccServerIndex ,i4SetValFsRadExtAccClientServerPortNumber)	\
	nmhSetCmnWithLock(FsRadExtAccClientServerPortNumber, 14, FsRadExtAccClientServerPortNumberSet, RadiusLock, RadiusUnLock, 0, 0, 1, "%i %i", i4FsRadExtAccServerIndex ,i4SetValFsRadExtAccClientServerPortNumber)

#endif
