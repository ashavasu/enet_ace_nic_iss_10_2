/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: fsmefcli.h,v 1.15 2015/07/16 04:48:02 siva Exp $
 *
 * Description:
 *********************************************************************/

#ifndef __FSMEFCLI_H__
#define __FSMEFCLI_H__
#include "cli.h"

/**************************Globle Defines*******************************/
enum
{
   MEF_CLI_TRANSMODE,
   MEF_CLI_UNI_ID,
   MEF_CLI_NO_UNI_ID,
   MEF_CLI_UNI_TYPE,
   MEF_CLI_NO_UNI_TYPE, 
   MEF_CLI_DEF_UNI_CEVLAN,
   MEF_CLI_UNI_L2CP,
   MEF_CLI_EVC_ID,
   MEF_CLI_NO_EVC_ID,
   MEF_CLI_EVC_TYPE,
   MEF_CLI_EVC_PRESERVE,
   MEF_CLI_EVC_NO_PRESERVE,
   MEF_CLI_EVC_LOOPBACK,
   MEF_CLI_CEVLAN_EVC_MAP,
   MEF_CLI_NO_CEVLAN_EVC_MAP,
   MEF_CLI_FILTER_PRIORITY,
   MEF_CLI_FILTER_DSCP,
   MEF_CLI_NO_FILTER,
   MEF_CLI_CLASSMAP,
   MEF_CLI_NO_CLASSMAP,
   MEF_CLI_CLASS,
   MEF_CLI_NO_CLASS,
   MEF_CLI_METER,
   MEF_CLI_NO_METER,
   MEF_CLI_POLICY_MAP,
   MEF_CLI_NO_POLICY_MAP,
   MEF_CLI_MAP_FILTER,
   MEF_CLI_SHOW_UNI,
   MEF_CLI_SHOW_EVC,
   MEF_CLI_SHOW_CEVLAN_EVC_MAP,
   MEF_CLI_ETREE,
   MEF_CLI_NO_ETREE,
   MEF_CLI_CLEAR_FRAME_LOSS,
   MEF_CLI_CLEAR_FRAME_DELAY,
   MEF_CLI_CLEAR_L2_TUNNEL_COUNTERS,
   MEF_CLI_FRAME_DELAY,
   MEF_CLI_FRAME_LOSS,
   MEF_CLI_AVAILABILITY,
   MEF_CLI_SHOW_FRAME_LOSS,
   MEF_CLI_SHOW_FRAME_DELAY,
   MEF_CLI_SHOW_AVAILABILITY,
   MEF_CLI_SHOW_FILTER,
   MEF_CLI_SHOW_CLASSMAP,
   MEF_CLI_SHOW_CLASS,
   MEF_CLI_SHOW_METER,
   MEF_CLI_SHOW_POLICYMAP,
   MEF_CLI_SHOW_MEF_INFO,
   MEF_CLI_SHOW_MEF_ETREE,
   MEF_CLI_EVC_FILTER,
   MEF_CLI_NO_EVC_FILTER,
   MEF_CLI_UNILIST,
   MEF_CLI_NO_UNILIST,
   MEF_CLI_SHOW_UNILIST,
   MEF_CLI_UNI_L2CP_OVERRIDE,
   MEF_CLI_EVC_L2CP,
   MEF_CLI_SHOW_EVC_STATS
};

enum {
   MEF_CLI_L2CP_PROTOCOL_DOT1X,
   MEF_CLI_L2CP_PROTOCOL_LACP,
   MEF_CLI_L2CP_PROTOCOL_STP,
   MEF_CLI_L2CP_PROTOCOL_GVRP,
   MEF_CLI_L2CP_PROTOCOL_GMRP,
   MEF_CLI_L2CP_PROTOCOL_MVRP,
   MEF_CLI_L2CP_PROTOCOL_MMRP,
   MEF_CLI_L2CP_PROTOCOL_ELMI,
   MEF_CLI_L2CP_PROTOCOL_LLDP,
   MEF_CLI_L2CP_PROTOCOL_ECFM,
   MEF_CLI_L2CP_PROTOCOL_IGMP,
   MEF_CLI_L2CP_PROTOCOL_EOAM
};

enum {
   MEF_CLI_L2CP_PROTO_ACTION_DISCARD,
   MEF_CLI_L2CP_PROTO_ACTION_PEER,
   MEF_CLI_L2CP_PROTO_ACTION_TUNNEL
};

enum {
/*1*/  MEF_CLI_UNI_NOT_EXIST = 1,
/*2*/  MEF_CLI_EVC_NOT_EXIST,
/*3*/  MEF_CLI_CVLAN_EVC_NOT_EXIST,
/*4*/  MEF_CLI_CVLAN_EVC_EXIST,
/*5*/  MEF_CLI_PORT_MAP_ERR,
/*6*/  MEF_CLI_UNI_ID_EXIST,
/*7*/  MEF_CLI_FILTER_UNI_ERR,
/*8*/  MEF_CLI_ENTRY_ALREADY_EXIT,
/*9*/  MEF_CLI_INVALID_UNI_L2CP_STATUS,
/*10*/ MEF_CLI_TUNNEL_PROTOCOL_ENA_ERR,
/*11*/ MEF_CLI_GVRP_PEER_ERROR,
/*12*/ MEF_CLI_TUNNEL_PEP_ERR,
/*13*/ MEF_CLI_BRG_PORT_TYPE_MAP_ERR,
/*14*/ MEF_CLI_BRG_PORT_TYPE_MODE_ERR,
/*15*/ MEF_CLI_POLICYMAP_NOT_EXIST,
/*16*/ MEF_CLI_POLICYMAP_EXIST,
/*17*/ MEF_CLI_METER_NOT_EXIST,
/*18*/ MEF_CLI_METER_EXIST,
/*19*/ MEF_CLI_CLASSMAP_NOT_EXIST,
/*20*/ MEF_CLI_CLASSMAP_EXIST,
/*21*/ MEF_CLI_CLASS_NOT_EXIST,
/*22*/ MEF_CLI_CLASS_EXIST,
/*23*/ MEF_CLI_FILTER_NOT_EXIST,
/*24*/ MEF_CLI_FILTER_EXIST,
/*25*/ MEF_CLI_METER_RFE_ERR,
/*26*/ MEF_CLI_CLS_MAP_RFE_ERR,
/*27*/ MEF_CLI_CLASS_RFE_ERR,
/*28*/ MEF_CLI_CVID_ENTRY_ERR,
/*29*/ MEF_CLI_INVALID_LMM_INTERVAL_ERR,
/*30*/ MEF_CLI_INVALID_AVAILABILITY_INTERVAL_ERR,
/*31*/ MEF_CLI_ERR_PLY_MULTIMAP,
/*32*/ MEF_CLI_ERR_NO_FILTER,
/*33*/ MEF_CLI_MEP_NOT_EXIST,
/*34*/ MEF_CLI_PORT_LIST_ERR,
/*35*/ MEF_CLI_INVALID_CONTEXT,
/*36*/ MEF_CLI_UNI_EVC_ID_EXIST,
/*37*/ MEF_CLI_UNI_LIST_NOT_EXIST,
/*38*/ MEF_CLI_EVC_PEER_NO_SUPPORT,
/*39*/ MEF_CLI_EVC_TUNNEL_NO_SUPPORT,
/*40*/ MEF_CLI_EVC_ID_EXIST,
/*41*/ MEF_CLI_MEP_EVC_MAP_EXISTS,
/*42*/ MEF_CLI_METER_COLOR_BLIND_MODE_NOT_SUPPORTED,
       MEF_CLI_MAX_ERR
     };
enum {
    MEF_UNI_OVERRIDE_ENABLE =1,
    MEF_UNI_OVERRIDE_DISABLE
};

#ifdef  __FSMEFCLI_C__
CONST CHR1  *gapc1MefCliErrString [] = {
       NULL,
/*1*/  "UNI does not exists \r\n",
/*2*/  "EVC does not exists \r\n",
/*3*/  "CVlan EVC Entry Does Not Exist \r\n",
/*4*/  "Provided CVlan for this Interface is already mapped with some other EVC \r\n",
/*5*/  "Port is not mapped to any virtual context \r\n",
/*6*/  "uni-id already exist \r\n",
/*7*/  "No such filter is configured on this UNI \r\n",
/*8*/  "Entry Already Exist \r\n",
/*9*/  "Invalid UNI L2CP Status,check mutiplexing/bundling status or UNI configuration.\r\n",
/*10*/ "Protocol status must be disabled to set the tunnel status \r\n",
/*11*/ "Peering option is not supported for GVRP \r\n",
/*12*/ "Tunneling can be enabled on CEP only when there is only one PEP associated with CEP and the S-VLAN service type is E-Line \r\n",
/*13*/ "uni-id cannot be set for a port that is not mapped to any virtual context.\r\n",
/*14*/ "Bridge port type invalid for this bridge mode \r\n",
/*15*/ "Mef Policy Map Entry Does Not Exists \r\n",
/*16*/ "Mef Policy Map Entry Already Exists \r\n",
/*17*/ "Mef Meter Entry Does Not Exists \r\n",
/*18*/ "Mef Meter Entry Already Exists \r\n",
/*19*/ "Mef CLass Map Entry Does Not Exists \r\n",
/*20*/ "Mef Class Map Entry Already Exists \r\n",
/*21*/ "Mef Class Entry Does Not Exists \r\n",
/*22*/ "Mef Class Entry Already Exists \r\n",
/*23*/ "Mef Filter Entry Does Not Exists \r\n",
/*24*/ "Mef Filter Entry Already Exists \r\n",
/*25*/ "Meter Table Entry is referenced by  Policy Entry or other Meter.\r\n",
/*26*/ "Class Map Entry referenced by Policy Map Table Entry.\r\n",
/*27*/ "Class Table Entry is referenced by  Policy Entry or other Class Map Entry.\r\n",
/*28*/ "C-VID Registration table entries can be configured only for CustomerEdgePort\r\n",
/*29*/ "Invalid LMM Interval value.\r\n",
/*30*/ "Invalid Availability Interval value.\r\n",
/*31*/ "CLASS is already Mapped with another PolicyMapEntry.\r\n",
/*32*/ "Filter not configured for the CLASS.\r\n",
/*33*/ "No MEP exists for the indices.\r\n",
/*34*/ "Ingress/Egress port-list is not correct.\r\n",
/*35*/ "Invalid Context Id.\r\n",
/*36*/  "UNI-EVC Id already exist \r\n",
/*37*/  "UNI List does not exists \r\n",
/*38*/ "Protocols GVRP, GMRP, MVRP, MMRP and IGMP do not support Peering\r\n",
/*39*/ "Protocol PTP does not support Tunneling\r\n",
/*40*/  "EVC Id already exist \r\n",
/*41*/ "EVC cannot be deleted, One or More MEPs associated with this EVC.\r\n",
/*42*/ "Color-blind policer mode not supported.\r\n",
       "\r\n"
};
#endif

/**************************Globle Prototypes*******************************/
INT4
cli_process_mef_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);

INT4
cli_process_mef_sh_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);

#ifdef MEF_TEST_WANTED
INT4 cli_process_mef_test_cmd (tCliHandle CliHandle,...);
#endif

#endif
