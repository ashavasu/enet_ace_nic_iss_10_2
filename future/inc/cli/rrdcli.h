
/* $Id: rrdcli.h,v 1.15 2016/02/08 10:40:13 siva Exp $*/
/* Description: This file contains RRD cli definitions */

#ifndef __RRDCLI_H__
#define __RRDCLI_H__

#include "cli.h"
#include "ip.h"

#define CLI_RRD_PERMIT               1
#define CLI_RRD_DENY                 2

#define CLI_RRD_OSPF_AREA_RT         1
#define CLI_RRD_OSPF_EXT_RT          2

#define     RRD_CLI_DEF_DEST         0
#define     RRD_CLI_DEF_RANGE        0xffffffff 

#define RRD_CLI_IP_PROTO_LOCAL       2
#define RRD_CLI_IP_PROTO_STATIC      3
#define RRD_CLI_IP_PROTO_RIP         8 
#define RRD_CLI_IP_PROTO_OSPF        13 
#define RRD_CLI_IP_PROTO_BGP         14 

#define RRD_CLI_MAX_ARGS             10
#define RRD_MAX_ADDR_BUFFER          256

/* Command Identifier */

enum {
    CLI_IP_SHOW_PROTOCOLS = 1,
    CLI_RRD_SHOW_CONTROL_INFO,
    CLI_RRD_SHOW_STATUS, 
    CLI_RRD_AS_NUM,      
    CLI_RRD_ROUTER_ID,
    CLI_RRD_EXPORT_OSPF,
    CLI_RRD_NO_EXPORT_OSPF,
    CLI_RRD_DEF_CONTROL_STATUS,  
    CLI_RRD_REDISTRIBUTE_POLICY,
    CLI_RRD_NO_REDISTRIBUTE_POLICY,
    CLI_RRD_THROT_VALUE,
    CLI_RRD_AS_ROUTERID_FORCE,
    CLI_RRD_MAX_ROUTE,
    CLI_RRD_SHOW_MAX_ROUTE,
    CLI_RRD_SHOW_ECMP_ACROSS_PROTOCOL,
    CLI_RRD_RED_DYN_INFO,
    CLI_ECMP_ACROSS_PROTOCOL,
    CLI_VRF_ROUTE_LEAK,
    CLI_RRD_MAX_COMMANDS
};


/* Error codes */
enum {
    CLI_RRD_INVALID_RID = 1,
    CLI_RTM_WRONG_THROTTLE_LIMIT,
    CLI_RRD_INVALID_INPUT,
    CLI_RRD_ONLY_ENABLE,
    CLI_RRD_DISABLED,
    CLI_RRD_RID_AS_NOTSET,
    CLI_RRD_NOENTRY_CTRL_TABLE,
    CLI_RRD_EXPORT_FLAG_NOTSET,
    CLI_RRD_INVALID_SRCPROTO,
    CLI_RRD_INVALID_DESTPROTO,
    CLI_RRD_NO_REDISTRIBUTE,
    CLI_RRD_PROTO_NOREG,
    CLI_RRD_MEMORY_OVERRUN,
    CLI_RRD_ROUTE_EXIST,
    CLI_RRD_MAX_ERR
   }; 


/* Error strings */

#ifdef __RRDCLI_C__

CONST CHR1 *RrdCliErrString [] = {
    NULL,
    "\r% Invalid Router ID\r\n",
    "\r% Wrong Throttle Limit Value\r\n",
    "\r% Invalid Input\r\n",
    "\r% Router Redistribution can only be enabled\r\n",
    "\r% Force cannot be enabled if Router Redistribution is disabled\r\n",
    "\r% AS Number not Set\r\n",
    "\r% No Such Entry in RRD Control Table\r\n",
    "\r% Global Export Flag Not Enabled\r\n",
    "\r% Invalid Source Protocol\r\n",
    "\r% Invalid Destination Protocol\r\n",
    "\r% Redistibution not enabled\r\n",
    "\r% Protocol not registered with RRD\r\n",
    "\r% Memory Unavailable\r\n",
    "\r% Memory Reservation Failure. Free Existing protocol routes\r\n",
};    

#endif


/* Prototypes added for rrdcli.c */

INT4 cli_process_rrd_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Comamnd, ...));  
INT4 CliRrdAsNum (tCliHandle,INT4);
INT4 CliRrdRouterId (tCliHandle,UINT4);
INT4 CliRrdAddControlTable (tCliHandle,UINT1,UINT4,UINT4,INT4,INT4);
INT4 CliRrdDelControlTable (tCliHandle,UINT4,UINT4);
INT4 CliRrdOspfAreaStatus (tCliHandle,INT4,INT4);
INT4 CliRrdOspfExtStatus (tCliHandle,INT4,INT4);
INT4 CliRrdDefControlEntry (tCliHandle, INT4);
INT4 CliRrdSetThrottleLimit (tCliHandle, UINT4);
INT4 CliRrdForceAsNumRouterId (tCliHandle, UINT4 , UINT4);
INT4 ShowRtmControlInfoInCxt (tCliHandle, UINT4);
INT4 ShowRrdStatusInCxt (tCliHandle, UINT4);
INT4 ShowRoutingProtocolInfoInCxt (tCliHandle, UINT4);
INT4 ShowRipProtocolInfo (tCliHandle);
INT4 ShowOspfProtocolInfo (tCliHandle);
INT4 ShowRipProtocolInfoInCxt (tCliHandle, INT4);
INT4 ShowOspfProtocolInfoInCxt (tCliHandle, UINT4);
VOID CliRrdModDefCtrlEntry (INT4);
INT4 RrdShowRunningConfig(tCliHandle CliHandle, INT4 i4ContextId, UINT4);
INT4 RrdShowRunningConfigScalars (tCliHandle CliHandle, INT4 i4ContextId, UINT4);
INT4 RrdShowRunningConfigTable (tCliHandle CliHandle, INT4 i4ContextId);
INT4 RtmCliGetCommandType (UINT4, INT1 *);
INT4 RtmSetMaxRoute (UINT1 u1ProtoType, UINT4 u4MaxRTMRoute);
INT4 RtmShowRouteMemReservation ( tCliHandle );
INT4 CliEcmpAcrossProtocols (tCliHandle CliHandle, UINT4 u4AdminStatus);
INT4 CliVrfRouteLeaking (tCliHandle CliHandle, UINT4 u4AdminStatus);
VOID RtmShowEcmpAcrossProtocols (tCliHandle CliHandle);

#ifdef RM_WANTED
INT4 ShowRrdRedDynInfo (tCliHandle CliHandle);
#endif

#endif /* __RRDCLI_H__ */
