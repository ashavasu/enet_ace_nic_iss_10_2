/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lsppcli.h,v 1.11 2013/09/17 13:07:19 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              in clicmds.def and cli.c file.
 ********************************************************************/


#ifndef __LSPPCLI_H__
#define __LSPPCLI_H__
#include "cli.h"

enum
{
    CLI_LSPP_SHOW_GLOBAL_STATS = 1,
    CLI_LSPP_SHOW_GLOBAL_CFGS,
    CLI_LSPP_SHOW_PING_TABLE,
    CLI_LSPP_SHOW_TRACE_TABLE
};

enum
{
    LSPP_CLI_CONTEXT_NOT_EXIST = 0,
    LSPP_CLI_NO_GLOBAL_STATS_TABLE,
    LSPP_CLI_NO_GLOBAL_CONFIG_TABLE,
    LSPP_CLI_NO_PING_TRACE_ENTRY,
    LSPP_CLI_MAX_ERR
};

#ifdef __LSPPCLI_C__

CONST CHR1  *gaLsppCliErrorString [] = {

    "\r\nContext with the given context name not exist\r\n",
    "\r\nNo Global Stats entry is present to display\r\n",
    "\r\nNo Global Config entry is present to display\r\n",
    "\r\nNo Ping Trace entry is present to display\r\n",
};

#endif

#define LSPP_CLI_MAX_ARGS                          15

#define CLI_LSPP_PATH_TYPE_LDP_IPV4                 1
#define CLI_LSPP_PATH_TYPE_LDP_IPV6                 2
#define CLI_LSPP_PATH_TYPE_TNL                      3
#define CLI_LSPP_PATH_TYPE_PW                       5
#define CLI_LSPP_PATH_TYPE_MEP                      7

#define CLI_LSPP_ADDR_TYPE_IPV4                     0  
#define CLI_LSPP_ADDR_TYPE_IPV6                     1

#define CLI_LSPP_DEFAULT_CONTEXT                    0

#define CLI_LSPP_CREATE_AND_GO                      0x04

#define CLI_LSPP_REQ_TYPE_PING                      1
#define CLI_LSPP_REQ_TYPE_TRACE_ROUTE               2

#define CLI_LSPP_TIMER_UNIT_MILLI_SECONDS           1
#define CLI_LSPP_TIMER_UNIT_SECONDS                 2
#define CLI_LSPP_TIMER_UNIT_MINUTES                 3

#define CLI_LSPP_ENABLE                             1
#define CLI_LSPP_DISABLE                            2

#define CLI_LSPP_ENCAP_IP                           1
#define CLI_LSPP_ENCAP_ACH                          2
#define CLI_LSPP_ENCAP_ACH_IP                       3
#define CLI_LSPP_ENCAP_VCCV                         4

#define CLI_LSPP_NO_REPLY                           1
#define CLI_LSPP_REPLY_IP_UDP                       2
#define CLI_LSPP_REPLY_IP_UDP_ROUTER_ALERT          3
#define CLI_LSPP_REPLY_APPLICATION_CC               4


#define CLI_LSPP_DBG_INIT                           0x0001
#define CLI_LSPP_DBG_MGMT                           0x0002
#define CLI_LSPP_DBG_DATA                           0x0004
#define CLI_LSPP_DBG_CTRL                           0x0008
#define CLI_LSPP_DBG_PKT_DUMP                       0x0010
#define CLI_LSPP_DBG_RESOURCE                       0x0020
#define CLI_LSPP_DBG_FAIL                           0x0040
#define CLI_LSPP_DBG_BUF                            0x0080
#define CLI_LSPP_DBG_TLV                            0x0100
#define CLI_LSPP_DBG_ERROR                          0x0200
#define CLI_LSPP_DBG_EVENT                          0x0400   
#define CLI_LSPP_DBG_PKT_ERROR                      0x0800
#define CLI_LSPP_DBG_PATH                           0x1000 
#define CLI_LSPP_DBG_ALL                            0x1FFF


#define CLI_LSPP_PING_COMPLETION_TRAP               0x1
#define CLI_LSPP_TRACE_ROUTE_COMPLETION_TRAP        0x2
#define CLI_LSPP_BFD_BOOTSTRAP_TRAP                 0x4
#define CLI_LSPP_ENABLE_ALL_TRAP                    0x7
                                                     
#define CLI_LSPP_CLEAR_GLOBAL_STATS                 1         
                                                     
#define CLI_LSPP_TRUE                               1                           
#define CLI_LSPP_FALSE                              2                             
#define CLI_LSPP_MAX_OID_LENGTH                     56

#define CLI_LSPP_MAX_IPV4_MASK_LEN                  32
#define CLI_LSPP_MAX_IPV6_MASK_LEN                  128

#define CLI_LSPP_MAX_PAD_PATTERN_LEN                16


INT4 cli_process_Lspp_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);


INT4 LsppCliShowGlobalStats (tCliHandle CliHandle, UINT4 *,  UINT1 *);
INT4 LsppCliShowGlobalConfig (tCliHandle CliHandle,UINT4 *);
INT4 LsppCliShowPingTraceTable (tCliHandle CliHandle,
                                UINT4 *,  UINT1 *, UINT1 *, UINT1 );

INT4 LsppCliGetTrapLevel (UINT4, INT4 *);
INT4 LsppCliGetDebugLevel (UINT4, INT4 *);
INT4 LsppCliGetCxtIdFromName (UINT1 *, UINT4 *);
INT4 LsppCliGetSendersHandle (UINT4, UINT4 *);
VOID LsppCliConvertStrToHex (UINT1 *, UINT1 *);


INT4 LsppCliGetLdpIpv4FecAndMask (tCliHandle, UINT1 *, tLsppPathId *);
INT4 LsppCliGetLdpIpv6FecAndMask (tCliHandle, UINT1 *, tLsppPathId *);

VOID IssMplsOamEchoShowDebugging (tCliHandle CliHandle);

#ifdef LSPP_TEST_WANTED
#define CLI_LSPP_UT_TEST 1
#endif

#ifdef LSPP_TEST_WANTED
INT4 cli_process_lspp_test_cmd (tCliHandle CliHandle, UINT4 u4Command,...);
#endif
#endif
