/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssnpcli.h,v 1.8 2015/02/13 11:13:58 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsSnoopInstanceGlobalInstId[12];
extern UINT4 FsSnoopInstanceGlobalMcastFwdMode[12];
extern UINT4 FsSnoopInstanceGlobalSystemControl[12];
extern UINT4 FsSnoopInstanceGlobalLeaveConfigLevel[12];
extern UINT4 FsSnoopInstanceGlobalEnhancedMode[12];
extern UINT4 FsSnoopInstanceGlobalSparseMode[12];
extern UINT4 FsSnoopInstanceGlobalReportProcessConfigLevel[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsSnoopInstanceGlobalMcastFwdMode(i4FsSnoopInstanceGlobalInstId ,i4SetValFsSnoopInstanceGlobalMcastFwdMode) \
 nmhSetCmn(FsSnoopInstanceGlobalMcastFwdMode, 12, FsSnoopInstanceGlobalMcastFwdModeSet, SnpLock, SnpUnLock, 0, 0, 1, "%i %i", i4FsSnoopInstanceGlobalInstId ,i4SetValFsSnoopInstanceGlobalMcastFwdMode)
#define nmhSetFsSnoopInstanceGlobalSystemControl(i4FsSnoopInstanceGlobalInstId ,i4SetValFsSnoopInstanceGlobalSystemControl) \
 nmhSetCmn(FsSnoopInstanceGlobalSystemControl, 12, FsSnoopInstanceGlobalSystemControlSet, SnpLock, SnpUnLock, 0, 0, 1, "%i %i", i4FsSnoopInstanceGlobalInstId ,i4SetValFsSnoopInstanceGlobalSystemControl)
#define nmhSetFsSnoopInstanceGlobalLeaveConfigLevel(i4FsSnoopInstanceGlobalInstId ,i4SetValFsSnoopInstanceGlobalLeaveConfigLevel) \
 nmhSetCmn(FsSnoopInstanceGlobalLeaveConfigLevel, 12, FsSnoopInstanceGlobalLeaveConfigLevelSet, SnpLock, SnpUnLock, 0, 0, 1, "%i %i", i4FsSnoopInstanceGlobalInstId ,i4SetValFsSnoopInstanceGlobalLeaveConfigLevel)
#define nmhSetFsSnoopInstanceGlobalEnhancedMode(i4FsSnoopInstanceGlobalInstId ,i4SetValFsSnoopInstanceGlobalEnhancedMode) \
 nmhSetCmn(FsSnoopInstanceGlobalEnhancedMode, 12, FsSnoopInstanceGlobalEnhancedModeSet, SnpLock, SnpUnLock, 0, 0, 1, "%i %i", i4FsSnoopInstanceGlobalInstId ,i4SetValFsSnoopInstanceGlobalEnhancedMode)
#define nmhSetFsSnoopInstanceGlobalSparseMode(i4FsSnoopInstanceGlobalInstId ,i4SetValFsSnoopInstanceGlobalSparseMode) \
 nmhSetCmn(FsSnoopInstanceGlobalSparseMode, 12, FsSnoopInstanceGlobalSparseModeSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsSnoopInstanceGlobalInstId ,i4SetValFsSnoopInstanceGlobalSparseMode)
#define nmhSetFsSnoopInstanceGlobalReportProcessConfigLevel(i4FsSnoopInstanceGlobalInstId ,i4SetValFsSnoopInstanceGlobalReportProcessConfigLevel) \
 nmhSetCmn(FsSnoopInstanceGlobalReportProcessConfigLevel, 12, FsSnoopInstanceGlobalReportProcessConfigLevelSet, SnpLock, SnpUnLock, 0, 0, 1, "%i %i", i4FsSnoopInstanceGlobalInstId ,i4SetValFsSnoopInstanceGlobalReportProcessConfigLevel)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsSnoopInstanceConfigInstId[12];
extern UINT4 FsSnoopInetAddressType[12];
extern UINT4 FsSnoopStatus[12];
extern UINT4 FsSnoopProxyReportingStatus[12];
extern UINT4 FsSnoopRouterPortPurgeInterval[12];
extern UINT4 FsSnoopPortPurgeInterval[12];
extern UINT4 FsSnoopReportForwardInterval[12];
extern UINT4 FsSnoopRetryCount[12];
extern UINT4 FsSnoopGrpQueryInterval[12];
extern UINT4 FsSnoopReportFwdOnAllPorts[12];
extern UINT4 FsSnoopTraceOption[12];
extern UINT4 FsSnoopDebugOption[12];
extern UINT4 FsSnoopSendQueryOnTopoChange[12];
extern UINT4 FsSnoopSendLeaveOnTopoChange[12];
extern UINT4 FsSnoopFilterStatus[12];
extern UINT4 FsSnoopMulticastVlanStatus[12];
extern UINT4 FsSnoopProxyStatus[12];
extern UINT4 FsSnoopQueryFwdOnAllPorts[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsSnoopStatus(i4FsSnoopInstanceConfigInstId , i4FsSnoopInetAddressType ,i4SetValFsSnoopStatus) \
 nmhSetCmn(FsSnoopStatus, 12, FsSnoopStatusSet, SnpLock, SnpUnLock, 0, 0, 2, "%i %i %i", i4FsSnoopInstanceConfigInstId , i4FsSnoopInetAddressType ,i4SetValFsSnoopStatus)
#define nmhSetFsSnoopProxyReportingStatus(i4FsSnoopInstanceConfigInstId , i4FsSnoopInetAddressType ,i4SetValFsSnoopProxyReportingStatus) \
 nmhSetCmn(FsSnoopProxyReportingStatus, 12, FsSnoopProxyReportingStatusSet, SnpLock, SnpUnLock, 0, 0, 2, "%i %i %i", i4FsSnoopInstanceConfigInstId , i4FsSnoopInetAddressType ,i4SetValFsSnoopProxyReportingStatus)
#define nmhSetFsSnoopRouterPortPurgeInterval(i4FsSnoopInstanceConfigInstId , i4FsSnoopInetAddressType ,i4SetValFsSnoopRouterPortPurgeInterval) \
 nmhSetCmn(FsSnoopRouterPortPurgeInterval, 12, FsSnoopRouterPortPurgeIntervalSet, SnpLock, SnpUnLock, 1, 0, 2, "%i %i %i", i4FsSnoopInstanceConfigInstId , i4FsSnoopInetAddressType ,i4SetValFsSnoopRouterPortPurgeInterval)
#define nmhSetFsSnoopPortPurgeInterval(i4FsSnoopInstanceConfigInstId , i4FsSnoopInetAddressType ,i4SetValFsSnoopPortPurgeInterval) \
 nmhSetCmn(FsSnoopPortPurgeInterval, 12, FsSnoopPortPurgeIntervalSet, SnpLock, SnpUnLock, 1, 0, 2, "%i %i %i", i4FsSnoopInstanceConfigInstId , i4FsSnoopInetAddressType ,i4SetValFsSnoopPortPurgeInterval)
#define nmhSetFsSnoopReportForwardInterval(i4FsSnoopInstanceConfigInstId , i4FsSnoopInetAddressType ,i4SetValFsSnoopReportForwardInterval) \
 nmhSetCmn(FsSnoopReportForwardInterval, 12, FsSnoopReportForwardIntervalSet, SnpLock, SnpUnLock, 0, 0, 2, "%i %i %i", i4FsSnoopInstanceConfigInstId , i4FsSnoopInetAddressType ,i4SetValFsSnoopReportForwardInterval)
#define nmhSetFsSnoopRetryCount(i4FsSnoopInstanceConfigInstId , i4FsSnoopInetAddressType ,i4SetValFsSnoopRetryCount) \
 nmhSetCmn(FsSnoopRetryCount, 12, FsSnoopRetryCountSet, SnpLock, SnpUnLock, 0, 0, 2, "%i %i %i", i4FsSnoopInstanceConfigInstId , i4FsSnoopInetAddressType ,i4SetValFsSnoopRetryCount)
#define nmhSetFsSnoopGrpQueryInterval(i4FsSnoopInstanceConfigInstId , i4FsSnoopInetAddressType ,i4SetValFsSnoopGrpQueryInterval) \
 nmhSetCmn(FsSnoopGrpQueryInterval, 12, FsSnoopGrpQueryIntervalSet, SnpLock, SnpUnLock, 0, 0, 2, "%i %i %i", i4FsSnoopInstanceConfigInstId , i4FsSnoopInetAddressType ,i4SetValFsSnoopGrpQueryInterval)
#define nmhSetFsSnoopReportFwdOnAllPorts(i4FsSnoopInstanceConfigInstId , i4FsSnoopInetAddressType ,i4SetValFsSnoopReportFwdOnAllPorts) \
 nmhSetCmn(FsSnoopReportFwdOnAllPorts, 12, FsSnoopReportFwdOnAllPortsSet, SnpLock, SnpUnLock, 0, 0, 2, "%i %i %i", i4FsSnoopInstanceConfigInstId , i4FsSnoopInetAddressType ,i4SetValFsSnoopReportFwdOnAllPorts)
#define nmhSetFsSnoopTraceOption(i4FsSnoopInstanceConfigInstId , i4FsSnoopInetAddressType ,i4SetValFsSnoopTraceOption) \
 nmhSetCmn(FsSnoopTraceOption, 12, FsSnoopTraceOptionSet, SnpLock, SnpUnLock, 0, 0, 2, "%i %i %i", i4FsSnoopInstanceConfigInstId , i4FsSnoopInetAddressType ,i4SetValFsSnoopTraceOption)
#define nmhSetFsSnoopDebugOption(i4FsSnoopInstanceConfigInstId , i4FsSnoopInetAddressType ,i4SetValFsSnoopDebugOption) \
 nmhSetCmn(FsSnoopDebugOption, 12, FsSnoopDebugOptionSet, SnpLock, SnpUnLock, 0, 0, 2, "%i %i %i", i4FsSnoopInstanceConfigInstId , i4FsSnoopInetAddressType ,i4SetValFsSnoopDebugOption)
#define nmhSetFsSnoopSendQueryOnTopoChange(i4FsSnoopInstanceConfigInstId , i4FsSnoopInetAddressType ,i4SetValFsSnoopSendQueryOnTopoChange) \
 nmhSetCmn(FsSnoopSendQueryOnTopoChange, 12, FsSnoopSendQueryOnTopoChangeSet, SnpLock, SnpUnLock, 0, 0, 2, "%i %i %i", i4FsSnoopInstanceConfigInstId , i4FsSnoopInetAddressType ,i4SetValFsSnoopSendQueryOnTopoChange)
#define nmhSetFsSnoopSendLeaveOnTopoChange(i4FsSnoopInstanceConfigInstId , i4FsSnoopInetAddressType ,i4SetValFsSnoopSendLeaveOnTopoChange) \
 nmhSetCmn(FsSnoopSendLeaveOnTopoChange, 12, FsSnoopSendLeaveOnTopoChangeSet, SnpLock, SnpUnLock, 0, 0, 2, "%i %i %i", i4FsSnoopInstanceConfigInstId , i4FsSnoopInetAddressType ,i4SetValFsSnoopSendLeaveOnTopoChange)
#define nmhSetFsSnoopFilterStatus(i4FsSnoopInstanceConfigInstId , i4FsSnoopInetAddressType ,i4SetValFsSnoopFilterStatus) \
 nmhSetCmn(FsSnoopFilterStatus, 12, FsSnoopFilterStatusSet, SnpLock, SnpUnLock, 0, 0, 2, "%i %i %i", i4FsSnoopInstanceConfigInstId , i4FsSnoopInetAddressType ,i4SetValFsSnoopFilterStatus)
#define nmhSetFsSnoopMulticastVlanStatus(i4FsSnoopInstanceConfigInstId , i4FsSnoopInetAddressType ,i4SetValFsSnoopMulticastVlanStatus) \
 nmhSetCmn(FsSnoopMulticastVlanStatus, 12, FsSnoopMulticastVlanStatusSet, SnpLock, SnpUnLock, 0, 0, 2, "%i %i %i", i4FsSnoopInstanceConfigInstId , i4FsSnoopInetAddressType ,i4SetValFsSnoopMulticastVlanStatus)
#define nmhSetFsSnoopProxyStatus(i4FsSnoopInstanceConfigInstId , i4FsSnoopInetAddressType ,i4SetValFsSnoopProxyStatus) \
 nmhSetCmn(FsSnoopProxyStatus, 12, FsSnoopProxyStatusSet, SnpLock, SnpUnLock, 0, 0, 2, "%i %i %i", i4FsSnoopInstanceConfigInstId , i4FsSnoopInetAddressType ,i4SetValFsSnoopProxyStatus)
#define nmhSetFsSnoopQueryFwdOnAllPorts(i4FsSnoopInstanceConfigInstId , i4FsSnoopInetAddressType ,i4SetValFsSnoopQueryFwdOnAllPorts) \
 nmhSetCmn(FsSnoopQueryFwdOnAllPorts, 12, FsSnoopQueryFwdOnAllPortsSet, SnpLock, SnpUnLock, 0, 0, 2, "%i %i %i", i4FsSnoopInstanceConfigInstId , i4FsSnoopInetAddressType ,i4SetValFsSnoopQueryFwdOnAllPorts)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsSnoopVlanFilterInstId[12];
extern UINT4 FsSnoopVlanFilterVlanId[12];
extern UINT4 FsSnoopVlanFilterInetAddressType[12];
extern UINT4 FsSnoopVlanSnoopStatus[12];
extern UINT4 FsSnoopVlanCfgOperVersion[12];
extern UINT4 FsSnoopVlanFastLeave[12];
extern UINT4 FsSnoopVlanCfgQuerier[12];
extern UINT4 FsSnoopVlanStartupQueryCount[12];
extern UINT4 FsSnoopVlanStartupQueryInterval[12];
extern UINT4 FsSnoopVlanQueryInterval[12];
extern UINT4 FsSnoopVlanOtherQuerierPresentInterval[12];
extern UINT4 FsSnoopVlanRtrPortList[12];
extern UINT4 FsSnoopVlanRowStatus[12];
extern UINT4 FsSnoopVlanQuerierIpAddress[12];
extern UINT4 FsSnoopVlanQuerierIpFlag[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsSnoopVlanSnoopStatus(i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,i4SetValFsSnoopVlanSnoopStatus) \
 nmhSetCmn(FsSnoopVlanSnoopStatus, 12, FsSnoopVlanSnoopStatusSet, SnpLock, SnpUnLock, 0, 0, 3, "%i %i %i %i", i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,i4SetValFsSnoopVlanSnoopStatus)
#define nmhSetFsSnoopVlanCfgOperVersion(i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,i4SetValFsSnoopVlanCfgOperVersion) \
 nmhSetCmn(FsSnoopVlanCfgOperVersion, 12, FsSnoopVlanCfgOperVersionSet, SnpLock, SnpUnLock, 1, 0, 3, "%i %i %i %i", i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,i4SetValFsSnoopVlanCfgOperVersion)
#define nmhSetFsSnoopVlanFastLeave(i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,i4SetValFsSnoopVlanFastLeave) \
 nmhSetCmn(FsSnoopVlanFastLeave, 12, FsSnoopVlanFastLeaveSet, SnpLock, SnpUnLock, 0, 0, 3, "%i %i %i %i", i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,i4SetValFsSnoopVlanFastLeave)
#define nmhSetFsSnoopVlanCfgQuerier(i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,i4SetValFsSnoopVlanCfgQuerier) \
 nmhSetCmn(FsSnoopVlanCfgQuerier, 12, FsSnoopVlanCfgQuerierSet, SnpLock, SnpUnLock, 0, 0, 3, "%i %i %i %i", i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,i4SetValFsSnoopVlanCfgQuerier)
#define nmhSetFsSnoopVlanStartupQueryCount(i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,i4SetValFsSnoopVlanStartupQueryCount) \
 nmhSetCmn(FsSnoopVlanStartupQueryCount, 12, FsSnoopVlanStartupQueryCountSet, NULL, NULL, 0, 0, 3, "%i %i %i %i", i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,i4SetValFsSnoopVlanStartupQueryCount)
#define nmhSetFsSnoopVlanStartupQueryInterval(i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,i4SetValFsSnoopVlanStartupQueryInterval) \
 nmhSetCmn(FsSnoopVlanStartupQueryInterval, 12, FsSnoopVlanStartupQueryIntervalSet, NULL, NULL, 0, 0, 3, "%i %i %i %i", i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,i4SetValFsSnoopVlanStartupQueryInterval)
#define nmhSetFsSnoopVlanQueryInterval(i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,i4SetValFsSnoopVlanQueryInterval) \
 nmhSetCmn(FsSnoopVlanQueryInterval, 12, FsSnoopVlanQueryIntervalSet, SnpLock, SnpUnLock, 0, 0, 3, "%i %i %i %i", i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,i4SetValFsSnoopVlanQueryInterval)
#define nmhSetFsSnoopVlanOtherQuerierPresentInterval(i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,i4SetValFsSnoopVlanOtherQuerierPresentInterval) \
 nmhSetCmn(FsSnoopVlanOtherQuerierPresentInterval, 12, FsSnoopVlanOtherQuerierPresentIntervalSet, NULL, NULL, 0, 0, 3, "%i %i %i %i", i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,i4SetValFsSnoopVlanOtherQuerierPresentInterval)
#define nmhSetFsSnoopVlanRtrPortList(i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,pSetValFsSnoopVlanRtrPortList) \
 nmhSetCmn(FsSnoopVlanRtrPortList, 12, FsSnoopVlanRtrPortListSet, SnpLock, SnpUnLock, 0, 0, 3, "%i %i %i %s", i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,pSetValFsSnoopVlanRtrPortList)
#define nmhSetFsSnoopVlanRowStatus(i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,i4SetValFsSnoopVlanRowStatus) \
 nmhSetCmn(FsSnoopVlanRowStatus, 12, FsSnoopVlanRowStatusSet, SnpLock, SnpUnLock, 0, 1, 3, "%i %i %i %i", i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,i4SetValFsSnoopVlanRowStatus)

#define nmhSetFsSnoopVlanQuerierIpAddress(i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,u4SetValFsSnoopVlanQuerierIpAddress)      \
  nmhSetCmn(FsSnoopVlanQuerierIpAddress, 12, FsSnoopVlanQuerierIpAddressSet, SnpLock, SnpUnLock, 0, 0, 3, "%i %i %i %p", i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,u4SetValFsSnoopVlanQuerierIpAddress)                                         
#define nmhSetFsSnoopVlanQuerierIpFlag(i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,i4SetValFsSnoopVlanQuerierIpFlag)    \
        nmhSetCmnWithLock(FsSnoopVlanQuerierIpFlag, 12, FsSnoopVlanQuerierIpFlagSet, SnpLock, SnpUnLock, 0, 0, 3, "%i %i %i %i", i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,i4SetValFsSnoopVlanQuerierIpFlag)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsSnoopVlanBlkRtrPortList[12];
extern UINT4 FsSnoopVlanFilterMaxLimitType[12];
extern UINT4 FsSnoopVlanFilterMaxLimit[12];
extern UINT4 FsSnoopVlanFilter8021pPriority[12];
extern UINT4 FsSnoopVlanFilterDropReports[12];
extern UINT4 FsSnoopVlanMulticastProfileId[12];
extern UINT4 FsSnoopVlanMaxResponseTime[12];
extern UINT4 FsSnoopVlanRtrLocalPortList[12];
extern UINT4 FsSnoopVlanBlkRtrLocalPortList[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsSnoopVlanBlkRtrPortList(i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,pSetValFsSnoopVlanBlkRtrPortList) \
 nmhSetCmn(FsSnoopVlanBlkRtrPortList, 12, FsSnoopVlanBlkRtrPortListSet, SnpLock, SnpUnLock, 0, 0, 3, "%i %i %i %s", i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,pSetValFsSnoopVlanBlkRtrPortList)
#define nmhSetFsSnoopVlanFilterMaxLimitType(i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,i4SetValFsSnoopVlanFilterMaxLimitType) \
 nmhSetCmn(FsSnoopVlanFilterMaxLimitType, 12, FsSnoopVlanFilterMaxLimitTypeSet, SnpLock, SnpUnLock, 0, 0, 3, "%i %i %i %i", i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,i4SetValFsSnoopVlanFilterMaxLimitType)
#define nmhSetFsSnoopVlanFilterMaxLimit(i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,u4SetValFsSnoopVlanFilterMaxLimit) \
 nmhSetCmn(FsSnoopVlanFilterMaxLimit, 12, FsSnoopVlanFilterMaxLimitSet, SnpLock, SnpUnLock, 0, 0, 3, "%i %i %i %u", i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,u4SetValFsSnoopVlanFilterMaxLimit)
#define nmhSetFsSnoopVlanFilter8021pPriority(i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,i4SetValFsSnoopVlanFilter8021pPriority) \
 nmhSetCmn(FsSnoopVlanFilter8021pPriority, 12, FsSnoopVlanFilter8021pPrioritySet, SnpLock, SnpUnLock, 0, 0, 3, "%i %i %i %i", i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,i4SetValFsSnoopVlanFilter8021pPriority)
#define nmhSetFsSnoopVlanFilterDropReports(i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,i4SetValFsSnoopVlanFilterDropReports) \
 nmhSetCmn(FsSnoopVlanFilterDropReports, 12, FsSnoopVlanFilterDropReportsSet, SnpLock, SnpUnLock, 0, 0, 3, "%i %i %i %i", i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,i4SetValFsSnoopVlanFilterDropReports)
#define nmhSetFsSnoopVlanMulticastProfileId(i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,u4SetValFsSnoopVlanMulticastProfileId) \
 nmhSetCmn(FsSnoopVlanMulticastProfileId, 12, FsSnoopVlanMulticastProfileIdSet, SnpLock, SnpUnLock, 0, 0, 3, "%i %i %i %u", i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,u4SetValFsSnoopVlanMulticastProfileId)
#define nmhSetFsSnoopVlanMaxResponseTime(i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,i4SetValFsSnoopVlanMaxResponseTime) \
 nmhSetCmn(FsSnoopVlanMaxResponseTime, 12, FsSnoopVlanMaxResponseTimeSet, SnpLock, SnpUnLock, 0, 0, 3, "%i %i %i %i", i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,i4SetValFsSnoopVlanMaxResponseTime)
#define nmhSetFsSnoopVlanRtrLocalPortList(i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,pSetValFsSnoopVlanRtrLocalPortList) \
 nmhSetCmn(FsSnoopVlanRtrLocalPortList, 12, FsSnoopVlanRtrLocalPortListSet, SnpLock, SnpUnLock, 0, 0, 3, "%i %i %i %s", i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,pSetValFsSnoopVlanRtrLocalPortList)
#define nmhSetFsSnoopVlanBlkRtrLocalPortList(i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,pSetValFsSnoopVlanBlkRtrLocalPortList) \
 nmhSetCmn(FsSnoopVlanBlkRtrLocalPortList, 12, FsSnoopVlanBlkRtrLocalPortListSet, SnpLock, SnpUnLock, 0, 0, 3, "%i %i %i %s", i4FsSnoopVlanFilterInstId , i4FsSnoopVlanFilterVlanId , i4FsSnoopVlanFilterInetAddressType ,pSetValFsSnoopVlanBlkRtrLocalPortList)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsSnoopPortIndex[12];
extern UINT4 FsSnoopPortInetAddressType[12];
extern UINT4 FsSnoopPortLeaveMode[12];
extern UINT4 FsSnoopPortRateLimit[12];
extern UINT4 FsSnoopPortMaxLimitType[12];
extern UINT4 FsSnoopPortMaxLimit[12];
extern UINT4 FsSnoopPortProfileId[12];
extern UINT4 FsSnoopPortMaxBandwidthLimit[12];
extern UINT4 FsSnoopPortDropReports[12];
extern UINT4 FsSnoopPortRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsSnoopPortLeaveMode(i4FsSnoopPortIndex , i4FsSnoopPortInetAddressType ,i4SetValFsSnoopPortLeaveMode) \
 nmhSetCmn(FsSnoopPortLeaveMode, 12, FsSnoopPortLeaveModeSet, SnpLock, SnpUnLock, 0, 0, 2, "%i %i %i", i4FsSnoopPortIndex , i4FsSnoopPortInetAddressType ,i4SetValFsSnoopPortLeaveMode)
#define nmhSetFsSnoopPortRateLimit(i4FsSnoopPortIndex , i4FsSnoopPortInetAddressType ,u4SetValFsSnoopPortRateLimit) \
 nmhSetCmn(FsSnoopPortRateLimit, 12, FsSnoopPortRateLimitSet, SnpLock, SnpUnLock, 0, 0, 2, "%i %i %u", i4FsSnoopPortIndex , i4FsSnoopPortInetAddressType ,u4SetValFsSnoopPortRateLimit)
#define nmhSetFsSnoopPortMaxLimitType(i4FsSnoopPortIndex , i4FsSnoopPortInetAddressType ,i4SetValFsSnoopPortMaxLimitType) \
 nmhSetCmn(FsSnoopPortMaxLimitType, 12, FsSnoopPortMaxLimitTypeSet, SnpLock, SnpUnLock, 0, 0, 2, "%i %i %i", i4FsSnoopPortIndex , i4FsSnoopPortInetAddressType ,i4SetValFsSnoopPortMaxLimitType)
#define nmhSetFsSnoopPortMaxLimit(i4FsSnoopPortIndex , i4FsSnoopPortInetAddressType ,u4SetValFsSnoopPortMaxLimit) \
 nmhSetCmn(FsSnoopPortMaxLimit, 12, FsSnoopPortMaxLimitSet, SnpLock, SnpUnLock, 0, 0, 2, "%i %i %u", i4FsSnoopPortIndex , i4FsSnoopPortInetAddressType ,u4SetValFsSnoopPortMaxLimit)
#define nmhSetFsSnoopPortProfileId(i4FsSnoopPortIndex , i4FsSnoopPortInetAddressType ,u4SetValFsSnoopPortProfileId) \
 nmhSetCmn(FsSnoopPortProfileId, 12, FsSnoopPortProfileIdSet, SnpLock, SnpUnLock, 0, 0, 2, "%i %i %u", i4FsSnoopPortIndex , i4FsSnoopPortInetAddressType ,u4SetValFsSnoopPortProfileId)
#define nmhSetFsSnoopPortMaxBandwidthLimit(i4FsSnoopPortIndex , i4FsSnoopPortInetAddressType ,u4SetValFsSnoopPortMaxBandwidthLimit) \
 nmhSetCmn(FsSnoopPortMaxBandwidthLimit, 12, FsSnoopPortMaxBandwidthLimitSet, SnpLock, SnpUnLock, 0, 0, 2, "%i %i %u", i4FsSnoopPortIndex , i4FsSnoopPortInetAddressType ,u4SetValFsSnoopPortMaxBandwidthLimit)
#define nmhSetFsSnoopPortDropReports(i4FsSnoopPortIndex , i4FsSnoopPortInetAddressType ,i4SetValFsSnoopPortDropReports) \
 nmhSetCmn(FsSnoopPortDropReports, 12, FsSnoopPortDropReportsSet, SnpLock, SnpUnLock, 0, 0, 2, "%i %i %i", i4FsSnoopPortIndex , i4FsSnoopPortInetAddressType ,i4SetValFsSnoopPortDropReports)
#define nmhSetFsSnoopPortRowStatus(i4FsSnoopPortIndex , i4FsSnoopPortInetAddressType ,i4SetValFsSnoopPortRowStatus) \
 nmhSetCmn(FsSnoopPortRowStatus, 12, FsSnoopPortRowStatusSet, SnpLock, SnpUnLock, 0, 1, 2, "%i %i %i", i4FsSnoopPortIndex , i4FsSnoopPortInetAddressType ,i4SetValFsSnoopPortRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsSnoopEnhPortIndex[12];
extern UINT4 FsSnoopEnhPortInnerVlanId[12];
extern UINT4 FsSnoopEnhPortInetAddressType[12];
extern UINT4 FsSnoopEnhPortLeaveMode[12];
extern UINT4 FsSnoopEnhPortRateLimit[12];
extern UINT4 FsSnoopEnhPortMaxLimitType[12];
extern UINT4 FsSnoopEnhPortMaxLimit[12];
extern UINT4 FsSnoopEnhPortProfileId[12];
extern UINT4 FsSnoopEnhPortMaxBandwidthLimit[12];
extern UINT4 FsSnoopEnhPortDropReports[12];
extern UINT4 FsSnoopEnhPortRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsSnoopEnhPortLeaveMode(i4FsSnoopEnhPortIndex , u4FsSnoopEnhPortInnerVlanId , i4FsSnoopEnhPortInetAddressType ,i4SetValFsSnoopEnhPortLeaveMode) \
 nmhSetCmn(FsSnoopEnhPortLeaveMode, 12, FsSnoopEnhPortLeaveModeSet, SnpLock, SnpUnLock, 0, 0, 3, "%i %u %i %i", i4FsSnoopEnhPortIndex , u4FsSnoopEnhPortInnerVlanId , i4FsSnoopEnhPortInetAddressType ,i4SetValFsSnoopEnhPortLeaveMode)
#define nmhSetFsSnoopEnhPortRateLimit(i4FsSnoopEnhPortIndex , u4FsSnoopEnhPortInnerVlanId , i4FsSnoopEnhPortInetAddressType ,u4SetValFsSnoopEnhPortRateLimit) \
 nmhSetCmn(FsSnoopEnhPortRateLimit, 12, FsSnoopEnhPortRateLimitSet, SnpLock, SnpUnLock, 0, 0, 3, "%i %u %i %u", i4FsSnoopEnhPortIndex , u4FsSnoopEnhPortInnerVlanId , i4FsSnoopEnhPortInetAddressType ,u4SetValFsSnoopEnhPortRateLimit)
#define nmhSetFsSnoopEnhPortMaxLimitType(i4FsSnoopEnhPortIndex , u4FsSnoopEnhPortInnerVlanId , i4FsSnoopEnhPortInetAddressType ,i4SetValFsSnoopEnhPortMaxLimitType) \
 nmhSetCmn(FsSnoopEnhPortMaxLimitType, 12, FsSnoopEnhPortMaxLimitTypeSet, SnpLock, SnpUnLock, 0, 0, 3, "%i %u %i %i", i4FsSnoopEnhPortIndex , u4FsSnoopEnhPortInnerVlanId , i4FsSnoopEnhPortInetAddressType ,i4SetValFsSnoopEnhPortMaxLimitType)
#define nmhSetFsSnoopEnhPortMaxLimit(i4FsSnoopEnhPortIndex , u4FsSnoopEnhPortInnerVlanId , i4FsSnoopEnhPortInetAddressType ,u4SetValFsSnoopEnhPortMaxLimit) \
 nmhSetCmn(FsSnoopEnhPortMaxLimit, 12, FsSnoopEnhPortMaxLimitSet, SnpLock, SnpUnLock, 0, 0, 3, "%i %u %i %u", i4FsSnoopEnhPortIndex , u4FsSnoopEnhPortInnerVlanId , i4FsSnoopEnhPortInetAddressType ,u4SetValFsSnoopEnhPortMaxLimit)
#define nmhSetFsSnoopEnhPortProfileId(i4FsSnoopEnhPortIndex , u4FsSnoopEnhPortInnerVlanId , i4FsSnoopEnhPortInetAddressType ,u4SetValFsSnoopEnhPortProfileId) \
 nmhSetCmn(FsSnoopEnhPortProfileId, 12, FsSnoopEnhPortProfileIdSet, SnpLock, SnpUnLock, 0, 0, 3, "%i %u %i %u", i4FsSnoopEnhPortIndex , u4FsSnoopEnhPortInnerVlanId , i4FsSnoopEnhPortInetAddressType ,u4SetValFsSnoopEnhPortProfileId)
#define nmhSetFsSnoopEnhPortMaxBandwidthLimit(i4FsSnoopEnhPortIndex , u4FsSnoopEnhPortInnerVlanId , i4FsSnoopEnhPortInetAddressType ,u4SetValFsSnoopEnhPortMaxBandwidthLimit) \
 nmhSetCmn(FsSnoopEnhPortMaxBandwidthLimit, 12, FsSnoopEnhPortMaxBandwidthLimitSet, SnpLock, SnpUnLock, 0, 0, 3, "%i %u %i %u", i4FsSnoopEnhPortIndex , u4FsSnoopEnhPortInnerVlanId , i4FsSnoopEnhPortInetAddressType ,u4SetValFsSnoopEnhPortMaxBandwidthLimit)
#define nmhSetFsSnoopEnhPortDropReports(i4FsSnoopEnhPortIndex , u4FsSnoopEnhPortInnerVlanId , i4FsSnoopEnhPortInetAddressType ,i4SetValFsSnoopEnhPortDropReports) \
 nmhSetCmn(FsSnoopEnhPortDropReports, 12, FsSnoopEnhPortDropReportsSet, SnpLock, SnpUnLock, 0, 0, 3, "%i %u %i %i", i4FsSnoopEnhPortIndex , u4FsSnoopEnhPortInnerVlanId , i4FsSnoopEnhPortInetAddressType ,i4SetValFsSnoopEnhPortDropReports)
#define nmhSetFsSnoopEnhPortRowStatus(i4FsSnoopEnhPortIndex , u4FsSnoopEnhPortInnerVlanId , i4FsSnoopEnhPortInetAddressType ,i4SetValFsSnoopEnhPortRowStatus) \
 nmhSetCmn(FsSnoopEnhPortRowStatus, 12, FsSnoopEnhPortRowStatusSet, SnpLock, SnpUnLock, 0, 1, 3, "%i %u %i %i", i4FsSnoopEnhPortIndex , u4FsSnoopEnhPortInnerVlanId , i4FsSnoopEnhPortInetAddressType ,i4SetValFsSnoopEnhPortRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsSnoopRtrPortIndex[12];
extern UINT4 FsSnoopRtrPortVlanId[12];
extern UINT4 FsSnoopRtrPortInetAddressType[12];
extern UINT4 FsSnoopRtrPortCfgOperVersion[12];
extern UINT4 FsSnoopOlderQuerierInterval[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsSnoopRtrPortCfgOperVersion(i4FsSnoopRtrPortIndex , u4FsSnoopRtrPortVlanId , i4FsSnoopRtrPortInetAddressType ,i4SetValFsSnoopRtrPortCfgOperVersion) \
 nmhSetCmn(FsSnoopRtrPortCfgOperVersion, 12, FsSnoopRtrPortCfgOperVersionSet, SnpLock, SnpUnLock, 0, 0, 3, "%i %u %i %i", i4FsSnoopRtrPortIndex , u4FsSnoopRtrPortVlanId , i4FsSnoopRtrPortInetAddressType ,i4SetValFsSnoopRtrPortCfgOperVersion)
#define nmhSetFsSnoopOlderQuerierInterval(i4FsSnoopRtrPortIndex , u4FsSnoopRtrPortVlanId , i4FsSnoopRtrPortInetAddressType ,i4SetValFsSnoopOlderQuerierInterval) \
 nmhSetCmn(FsSnoopOlderQuerierInterval, 12, FsSnoopOlderQuerierIntervalSet, SnpLock, SnpUnLock, 0, 0, 3, "%i %u %i %i", i4FsSnoopRtrPortIndex , u4FsSnoopRtrPortVlanId , i4FsSnoopRtrPortInetAddressType ,i4SetValFsSnoopOlderQuerierInterval)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsXSnoopRtrPortRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsXSnoopRtrPortRowStatus(i4FsSnoopRtrPortIndex , u4FsSnoopRtrPortVlanId , i4FsSnoopRtrPortInetAddressType ,i4SetValFsXSnoopRtrPortRowStatus) \
 nmhSetCmn(FsXSnoopRtrPortRowStatus, 12, FsXSnoopRtrPortRowStatusSet, NULL, NULL, 0, 1, 3, "%i %u %i %i", i4FsSnoopRtrPortIndex , u4FsSnoopRtrPortVlanId , i4FsSnoopRtrPortInetAddressType ,i4SetValFsXSnoopRtrPortRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsSnoopVlanStaticMcastGrpInstId[12];
extern UINT4 FsSnoopVlanStaticMcastGrpVlanId[12];
extern UINT4 FsSnoopVlanStaticMcastGrpAddressType[12];
extern UINT4 FsSnoopVlanStaticMcastGrpSourceAddress[12];
extern UINT4 FsSnoopVlanStaticMcastGrpGroupAddress[12];
extern UINT4 FsSnoopVlanStaticMcastGrpPortList[12];
extern UINT4 FsSnoopVlanStaticMcastGrpRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsSnoopVlanStaticMcastGrpPortList(i4FsSnoopVlanStaticMcastGrpInstId , i4FsSnoopVlanStaticMcastGrpVlanId , i4FsSnoopVlanStaticMcastGrpAddressType , pFsSnoopVlanStaticMcastGrpSourceAddress , pFsSnoopVlanStaticMcastGrpGroupAddress ,pSetValFsSnoopVlanStaticMcastGrpPortList) \
 nmhSetCmn(FsSnoopVlanStaticMcastGrpPortList, 12, FsSnoopVlanStaticMcastGrpPortListSet, SnpLock, SnpUnLock, 0, 0, 5, "%i %i %i %s %s %s", i4FsSnoopVlanStaticMcastGrpInstId , i4FsSnoopVlanStaticMcastGrpVlanId , i4FsSnoopVlanStaticMcastGrpAddressType , pFsSnoopVlanStaticMcastGrpSourceAddress , pFsSnoopVlanStaticMcastGrpGroupAddress ,pSetValFsSnoopVlanStaticMcastGrpPortList)
#define nmhSetFsSnoopVlanStaticMcastGrpRowStatus(i4FsSnoopVlanStaticMcastGrpInstId , i4FsSnoopVlanStaticMcastGrpVlanId , i4FsSnoopVlanStaticMcastGrpAddressType , pFsSnoopVlanStaticMcastGrpSourceAddress , pFsSnoopVlanStaticMcastGrpGroupAddress ,i4SetValFsSnoopVlanStaticMcastGrpRowStatus) \
 nmhSetCmn(FsSnoopVlanStaticMcastGrpRowStatus, 12, FsSnoopVlanStaticMcastGrpRowStatusSet, SnpLock, SnpUnLock, 0, 1, 5, "%i %i %i %s %s %i", i4FsSnoopVlanStaticMcastGrpInstId , i4FsSnoopVlanStaticMcastGrpVlanId , i4FsSnoopVlanStaticMcastGrpAddressType , pFsSnoopVlanStaticMcastGrpSourceAddress , pFsSnoopVlanStaticMcastGrpGroupAddress ,i4SetValFsSnoopVlanStaticMcastGrpRowStatus)

#endif
