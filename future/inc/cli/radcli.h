/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: radcli.h,v 1.8 2015/04/28 11:45:05 siva Exp $
 *
 * Description:This file contains command idenfiers for definitions in
 *             radiuscmd.def,function prototypes for RADIUS CLI
 *
 *******************************************************************/

/* INCLUDE FILE HEADER :
 *
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : radcli.h                                         |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR      : Jeeva Sethuraman                                 |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : RADIUS                                           |
 * |                                                                           |
 * |  MODULE NAME           : CLI interface for RADIUS configuration           |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    :                                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION           : This file contains command idenfiers for         |
 * |                          definitions in radiuscmd.def,function prototypes |
 * |                          for RADIUS CLI and corresponding error code      |
 * |                          definitions                                      |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 *
 */

#ifndef __RADCLI_H__
#define __RADCLI_H__

#include "cli.h"
#include "utilipvx.h"
/* RADIUS CLI command constants. To add a new command ,a new definition needs to
 * be added to the list.
 */

enum
{
 CLI_RADIUS_SERVER_HOST= 1,
 CLI_RADIUS_NO_SERVER_HOST,
 CLI_RADIUS_DEBUG,
 CLI_RADIUS_NO_DEBUG,
 CLI_SHOW_RADIUS_SERVER,              
 CLI_SHOW_RADIUS_STATS,
 CLI_ENABLE_RADIUS_PROXY,
 CLI_DISABLE_RADIUS_PROXY,
 CLI_SHOW_RADIUS_PROXY
};

#define RAD_CLI_MAX_COMMANDS     12
#define RAD_CLI_MAX_ARGS         9
#define RAD_CLI_NAME_SIZE        20

#define RAD_CLI_NO_DEBUG        0x00000000
#define RAD_CLI_DEBUG_ALL       0x000fffff
#define RAD_CLI_ERR_MASK        0x00000001
#define RAD_CLI_EVENT_MASK      0x00000010
#define RAD_CLI_PACKET_MASK     0x00000008
#define RAD_CLI_RESP_MASK       0x00000004
#define RAD_CLI_TIMER_MASK      0x00000002

/* Error codes 
 * Error code values and strings are maintained inside the protocol 
 * module itself.
 */
enum {
    CLI_RAD_MAX_SERVER_ERR = 1,
    CLI_RAD_INVALID_SERVER,
    CLI_RAD_INVALID_SERVER_IP,
    CLI_RAD_NVT_ERR,
    CLI_RAD_FATAL_ERR,
    CLI_RAD_ENTRY_IN_USE,
    CLI_RAD_MAX_ERR
};
/* The error strings should be places under the switch so as to avoid 
 * redifinition. This will be visible only in modulecli.c file
 */
#ifdef __RADCLI_C__

CONST CHR1  *RadCliErrString[] = {
    NULL,
    "% Cannot configure more than 5 RADIUS servers \r\n" ,
    "% Invalid RADIUS server \r\n" ,
    "% Invalid RADIUS server IP address \r\n" ,
    "% Invalid charaters present in the encryption key \r\n" ,
    "% FATAL ERROR : Command FAILED \r\n" ,
    "% Cannot delete : awaiting response from this radius server \r\n"
};

#endif

#ifdef UTF8_TEST_WANTED
INT4 cli_process_utf8_test_cmd (tCliHandle, UINT4,...);
#endif

INT4 cli_process_radius_cmd PROTO ((tCliHandle CliHandle,UINT4, ...));
INT4
RadSetServerHost PROTO ((tCliHandle, UINT1*, UINT4, UINT4, UINT1 *, INT4, INT4, INT4 , UINT4));
INT4
RadSetNoServerHost PROTO ((tCliHandle, UINT1 *, INT4,UINT4));
INT4
RadSetDebug PROTO ((tCliHandle, UINT4));
INT4
RadShowServerInfo PROTO ((tCliHandle CliHandle, tIPvXAddr * pServerAddress, UINT1 *));
INT4
RadShowStats PROTO ((tCliHandle CliHandle));

VOID IssRadiusShowDebugging (tCliHandle);
INT4 RadiusShowRunningConfig(tCliHandle CliHandle);
INT4 RadSetProxyStatus (tCliHandle CliHandle, UINT1 u1StatusVal);
INT4 RadShowProxyInfo (tCliHandle CliHandle);

INT4 FsRadExtDebugMaskSet(tSnmpIndex *, tRetVal *);
INT4 FsRadExtMaxNoOfUserEntriesSet(tSnmpIndex *, tRetVal *);
INT4 FsRadExtPrimaryServerAddressTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsRadExtPrimaryServerSet(tSnmpIndex *, tRetVal *);
INT4 FsRadExtServerResponseTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsRadExtServerAddrTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsRadExtServerAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsRadExtServerTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsRadExtServerSharedSecretSet(tSnmpIndex *, tRetVal *);
INT4 FsRadExtServerEnabledSet(tSnmpIndex *, tRetVal *);
INT4 FsRadExtServerResponseTimeSet(tSnmpIndex *, tRetVal *);
INT4 FsRadExtServerMaximumRetransmissionSet(tSnmpIndex *, tRetVal *);
INT4 FsRadExtServerEntryStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsRadExtServerResponseTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRadExtAuthClientServerPortNumberSet(tSnmpIndex *, tRetVal *);
INT4 FsRadExtAccClientServerPortNumberSet(tSnmpIndex *, tRetVal *);

#endif
