/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdsnccli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 SnmpCommunityIndex[11];
extern UINT4 SnmpCommunityName[11];
extern UINT4 SnmpCommunitySecurityName[11];
extern UINT4 SnmpCommunityContextEngineID[11];
extern UINT4 SnmpCommunityContextName[11];
extern UINT4 SnmpCommunityTransportTag[11];
extern UINT4 SnmpCommunityStorageType[11];
extern UINT4 SnmpCommunityStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetSnmpCommunityName(pSnmpCommunityIndex ,pSetValSnmpCommunityName)	\
	nmhSetCmn(SnmpCommunityName, 11, SnmpCommunityNameSet, NULL, NULL, 0, 0, 1, "%s %s", pSnmpCommunityIndex ,pSetValSnmpCommunityName)
#define nmhSetSnmpCommunitySecurityName(pSnmpCommunityIndex ,pSetValSnmpCommunitySecurityName)	\
	nmhSetCmn(SnmpCommunitySecurityName, 11, SnmpCommunitySecurityNameSet, NULL, NULL, 0, 0, 1, "%s %s", pSnmpCommunityIndex ,pSetValSnmpCommunitySecurityName)
#define nmhSetSnmpCommunityContextEngineID(pSnmpCommunityIndex ,pSetValSnmpCommunityContextEngineID)	\
	nmhSetCmn(SnmpCommunityContextEngineID, 11, SnmpCommunityContextEngineIDSet, NULL, NULL, 0, 0, 1, "%s %s", pSnmpCommunityIndex ,pSetValSnmpCommunityContextEngineID)
#define nmhSetSnmpCommunityContextName(pSnmpCommunityIndex ,pSetValSnmpCommunityContextName)	\
	nmhSetCmn(SnmpCommunityContextName, 11, SnmpCommunityContextNameSet, NULL, NULL, 0, 0, 1, "%s %s", pSnmpCommunityIndex ,pSetValSnmpCommunityContextName)
#define nmhSetSnmpCommunityTransportTag(pSnmpCommunityIndex ,pSetValSnmpCommunityTransportTag)	\
	nmhSetCmn(SnmpCommunityTransportTag, 11, SnmpCommunityTransportTagSet, NULL, NULL, 0, 0, 1, "%s %s", pSnmpCommunityIndex ,pSetValSnmpCommunityTransportTag)
#define nmhSetSnmpCommunityStorageType(pSnmpCommunityIndex ,i4SetValSnmpCommunityStorageType)	\
	nmhSetCmn(SnmpCommunityStorageType, 11, SnmpCommunityStorageTypeSet, NULL, NULL, 0, 0, 1, "%s %i", pSnmpCommunityIndex ,i4SetValSnmpCommunityStorageType)
#define nmhSetSnmpCommunityStatus(pSnmpCommunityIndex ,i4SetValSnmpCommunityStatus)	\
	nmhSetCmn(SnmpCommunityStatus, 11, SnmpCommunityStatusSet, NULL, NULL, 0, 1, 1, "%s %i", pSnmpCommunityIndex ,i4SetValSnmpCommunityStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 SnmpTargetAddrTMask[11];
extern UINT4 SnmpTargetAddrMMS[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetSnmpTargetAddrTMask(pSnmpTargetAddrName ,pSetValSnmpTargetAddrTMask)	\
	nmhSetCmn(SnmpTargetAddrTMask, 11, SnmpTargetAddrTMaskSet, NULL, NULL, 0, 0, 1, "%s %s", pSnmpTargetAddrName ,pSetValSnmpTargetAddrTMask)
#define nmhSetSnmpTargetAddrMMS(pSnmpTargetAddrName ,i4SetValSnmpTargetAddrMMS)	\
	nmhSetCmn(SnmpTargetAddrMMS, 11, SnmpTargetAddrMMSSet, NULL, NULL, 0, 0, 1, "%s %i", pSnmpTargetAddrName ,i4SetValSnmpTargetAddrMMS)

#endif
