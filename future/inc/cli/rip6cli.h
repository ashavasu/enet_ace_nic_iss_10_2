/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: rip6cli.h,v 1.11 2014/06/24 12:44:11 siva Exp $
 **********************************************************************/

#ifndef __RIP6CLI_H__
#define __RIP6CLI_H__

#include "cli.h"
#include "ipv6.h"


/* command identification */

enum CliCommands {
    CLI_RIP6_DEBUG = 1 ,
    CLI_RIP6_SHOW_RIP,
    CLI_RIP6_IF_STATS_SHOW ,
    CLI_RIP6_PEERADV_TABLE_SHOW,
    CLI_RIP6_ROUTER,
    CLI_RIP6_NO_ROUTER,
    CLI_RIP6_SPLIT_HORIZON,
    CLI_RIP6_NO_SPLIT_HORIZON,
    CLI_RIP6_IF,
    CLI_RIP6_POISON_REVERSE,
    CLI_RIP6_DEFROUTE,
    CLI_RIP6_METRIC,
    CLI_RIP6_REDISTRIBUTE,
    CLI_RIP6_PEER_FLTR_STATUS_SHOW,
    CLI_RIP6_TRIG_UPDATE_INTERVAL_SHOW,
    CLI_RIP6_PEER_FILTER_SET,
    CLI_RIP6_DELAY_TRIG_INTERVAL_SET,
    CLI_RIP6_FILTER,
    CLI_RIP6_DISTRIBUTE_LIST,
    CLI_RIP6_ROUTE_DISTANCE,
    CLI_RIP6_NO_ROUTE_DISTANCE,
    CLI_RIP6_MAX_COMMANDS
};

/* Error codes */

enum
{
     CLI_RIP6_INV_RIP6IF=1,
     CLI_RIP6_IP6_FORWAD_DIS,
     CLI_RIP6_INV_ENTRY,
            CLI_RIP6_INV_VALUE,
     CLI_RIP6_INV_DEF_ROUTEADV,
     CLI_RIP6_INV_METRIC,
     CLI_RIP6_INV_RRD_PROTOMASK,
     CLI_RIP6_INV_PEER_FLAG,
     CLI_RIP6_INV_PEER_ADDR,
     CLI_RIP6_INV_PEER_STATUS,
     CLI_RIP6_INV_ADV_FLAG,
     CLI_RIP6_INV_ADV_ADDR,
     CLI_RIP6_INV_ADV_STATUS,
     CLI_RIP6_FILTER_TBL_EMPTY,
     CLI_RIP6_ROUTE_EMPTY,
     CLI_RIP6_MEM_ALLOC_FAIL,
     CLI_RIP6_PROFILE_TBL_EMPTY,
     CLI_RIP6_INSTANCE_DOWN,
     CLI_RIP6_NO_IF,
            CLI_RIP6_INV_ASSOC_RMAP,
            CLI_RIP6_INV_DISASSOC_RMAP,
            CLI_RIP6_RMAP_ASSOC_FAILED,
            CLI_RIP6_MAX_ERR
};

/* Error strings */

#ifdef __RIP6CLI_C__

CONST CHR1 *Rip6CliErrString[] = {
         NULL,
         "% Invalid RIP6 Interface\r\n",
  "% Ip6 Forwarding Disabled\r\n",
  "% Entry does not exists\r\n",
         "% Invalid Value\r\n",
  "% Invalid Default Route Advt Value\r\n",
  "% Invalid Metric\r\n",
  "% FutureRIP6 Redistribution not supported for this protocol\r\n",
  "% Invalid PeerFilter flag value\r\n",
  "% PeerFilter Address SHOULD be LinkLocal only\r\n",
  "% Invalid PeerFilter Entry\r\n",
  "% Invalid AdvFilter flag value\r\n",
  "% AdvFilter Address SHOULD NOT be LinkLocal or Multicast \r\n",
  "% Invalid AdvFilter Entry\r\n",
  "No Entries In The Filter Table\r\n",
  "Route Entry is not Present in the RIP6 Route Table\r\n",
  "% RIP6 Route Display Fails - No Memory\r\n",
                "% No Entries in The Profile Table\r\n",
                "% RIP6 Instance Is Down\r\n",
                "% RIP6 is not enabled on any Interface\r\n",
                "% Route Map Not Associated\r\n",
                "% Route Map Not Disassociated\r\n",
                "% Route-map association failed (probably another route map is used)\r\n"
         "\r\n"
};
#endif


#define CLI_RIP6_ROUTER_MODE "Rip6Router"

#define         RIP6_CLI_MIN_COMMAND             0
#define         RIP6_MIN_ARRAY_ELEMENT           0
#define         RIP6_DEFAULT_INDEX               0
#define         RIP6_STATUS_DOWN                 0
#define         RIP6_DFLT_COST                   1
#define         RIP6_CLI_TRUE                    1
#define         RIP6_FIRST_ARRAY_ELEMENT         1
#define         RIP6_CLI_LAST_ARG_NO             8
#define         RIP6_CLI_MAX_ARGS               10
#define         RIP6_ADDR_BYTES                 16

#define         RIP6_CLI_IFACE_ATTACHED        "1"
#define         RIP6_CLI_IFACE_DETACHED        "0"
#define         RIP6_CLI_INST_UP               "1"
#define         RIP6_CLI_INST_DOWN             "0"

#define         RIP6_CLI_ALLOW                 "1"
#define         RIP6_CLI_DENY                  "2"
#define         RIP6_CLI_VALID                 "1"
#define         RIP6_CLI_INVALID               "2"

#define  RIP6_ALLOW          1
#define  RIP6_DENY              2

#define  RIP6_ENABLE            1
#define  RIP6_DISABLE           2

#define         RIP6_INVALID_INST               -1

#define         CLI_RIP6_ENABLE                1
#define         CLI_RIP6_DISABLE               2
#define         CLI_RIP6_TRUE                  1
#define         CLI_RIP6_FALSE                 0
#define         RIP6_ENABLE_IF                 1
#define         RIP6_DISABLE_IF                2

#define         RIP6_CLI_DEFROUTE_ENABLE       1
#define         RIP6_CLI_DEFROUTE_DISABLE      0   

#define         RIP6_CLI_STATIC_DYNAMIC        1
#define         RIP6_CLI_DYNAMIC               2
#define         RIP6_CLI_MAX_COMMANDS           30
#define         RIP6_CLI_IF_STATS_OBJECTS      200

#define         RIP6_CLI_PROF_NO_HORIZON       "1"
#define         RIP6_CLI_PROF_SPLIT_HORIZON    "2"
#define         RIP6_CLI_PROF_POISON_REVERSE   "3"

#define RIP6_IMPORT_DIRECT         0x0002
#define RIP6_IMPORT_STATIC         0x0004
#define RIP6_IMPORT_BGP            0x0040
#define RIP6_IMPORT_OSPF           0x0020
#define RIP6_IMPORT_ISISL1         0x0100
#define RIP6_IMPORT_ISISL2         0x0200
#define RIP6_IMPORT_ISISL1L2       0x0300
#define RIP6_ALL_PROTO_MASK        0x0366

#define CLI_RIP6_PROFILE_SHOW 0
#define CLI_RIP6_ROUTE_SHOW 1
#define RIP6_PEER_FILTER 0
#define RIP6_ADV_FILTER 1

#define CLI_RIP6_IN_FILTER                  0
#define CLI_RIP6_OUT_FILTER                 1

enum RouteType {
    RIP6_STATIC_DYNAMIC = 1,
    RIP6_DYNAMIC
};

enum CliTrace {
    RIP6_DATA_PATH = 1,
    RIP6_CONTROL_PLANE,
    RIP6_ALL_TRC,     
    RIP6_NO_TRC 
};


#define RIP6_MAX_PEER_ENTRIES       10

#define RIP6_CLI_ROUTE_SIZE             \
        ((10 * CLI_MAX_COLUMN_WIDTH) +  CLI_MAX_HEADER_SIZE)

#define RIP6_CLI_PROFILE_SIZE           \
        ((25 * CLI_MAX_COLUMN_WIDTH) +  CLI_MAX_HEADER_SIZE)

#define RIP6_CLI_IF_STATS_SIZE          \
        ((20 * CLI_MAX_COLUMN_WIDTH) +  CLI_MAX_HEADER_SIZE)

#define RIP6_CLI_INSTANCE_TABLE_SIZE    \
        ((10 * CLI_MAX_COLUMN_WIDTH) +  CLI_MAX_HEADER_SIZE)

#define RIP6_CLI_INST_IF_MAP_STATS_SIZE \
        ((10 * CLI_MAX_COLUMN_WIDTH) + CLI_MAX_HEADER_SIZE)

#define RIP6_CLI_PEER_TABLE_SIZE        \
        ((10 * CLI_MAX_COLUMN_WIDTH) + CLI_MAX_HEADER_SIZE)

#define RIP6_CLI_ADV_TABLE_SIZE         \
        ((10 * CLI_MAX_COLUMN_WIDTH) + CLI_MAX_HEADER_SIZE)

#define REDISTRIBUTION_OBJ 1
#define FSRIP6_CLI_REDISTRIBUTION_SIZE  \
        (CLI_MAX_COLUMN_WIDTH + REDISTRIBUTION_OBJ)
 
/*****************************************************************************
 *                       "show rip6 routes" interface structure                    *
 *****************************************************************************/
#define RIP6_SHOW_MAX_ROUTE          10  /* Collect a maximum of specified
                                         * routes to be displayed per cycle */

typedef struct t_ShowRip6Route
{
    tIp6Addr    PrefixAddr;      /* network prefix              */
    tIp6Addr    NextHop;         /* next hop                    */
    UINT4       u4IfIndex;       /* Interface Index             */
    UINT1       u1Metric;        /* metric                      */
    UINT1       u1PrefixLen;     /* Route's Prefix Length       */
    INT1        i1Protocol;      /* Protocol                    */
    UINT1       u1Pad; 
} tShowRip6Route;

/* the following information is used as a cookie in tShowRip6Rt structure */
typedef struct t_ShowRip6RtCookie
{
    tIp6Addr    PrefixAddr;     /* Route's Prefix                           */
    tIp6Addr    NextHop;        /* Next Hop                                 */
    UINT1       u1PrefixLen;    /* Route's Prefix Length                    */
    INT1        i1Protocol;     /* Protocol from which the route is learnt. */
                                /* all routes                               */
    UINT1       au1Pad[2];
} tShowRip6RtCookie;

typedef struct t_ShowRip6Rt
{
    UINT4 u4NoOfRoutes;          /* number of routes presented in this return 
                                  * message
                                  */
    tShowRip6Route aRoutes[1];  /* 'u4NoOfRoutes' follow here           */
} tShowRip6Rt;

INT4 cli_process_rip6_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command, ...));
INT1 cli_process_rip6_test_cmd PROTO ((tCliHandle CliHandle, ...));

INT1 Rip6GetRouterCfgPrompt PROTO ((INT1 *, INT1 *));

#endif /* __RIP6CLI_H__ */
