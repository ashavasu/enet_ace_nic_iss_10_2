
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipcli.h,v 1.51 2016/05/31 10:01:54 siva Exp $
 *
* Description: This file contains ip cli definitions
*/

#ifndef __IPCLI_H__
#define __IPCLI_H__

#include "cli.h"
#include "ip.h"

 /* Default Router VLAN Interface Index */
#define   DEFAULT_ROUTER_VLAN_IFINDEX   CfaGetDefaultVlanInterfaceIndex()

/* SIZE constants */
#define MAX_ADDR_BUFFER 256
#define MAX_MALLOC_SIZE 1000
#define MAX_ROUTES_SHOW 200
#define MAX_ROUTES_SHOW_BUFF  (MAX_ROUTES_SHOW * CLI_MAX_COLUMN_WIDTH) +\
                                CLI_MAX_HEADER_SIZE 
#define ARPCONF_OBJECTS  5  
#define IP_CLI_ARPCONF_SIZE ( CLI_MAX_COLUMN_WIDTH * ARPCONF_OBJECTS )

#define ARPSTAT_OBJECTS  10  
#define IP_CLI_ARPSTAT_SIZE ( CLI_MAX_COLUMN_WIDTH * ARPSTAT_OBJECTS )

#define IPTRAFF_OBJECTS  20  
#define IP_CLI_IPTRAFF_SIZE ( CLI_MAX_COLUMN_WIDTH * IPTRAFF_OBJECTS )

#define MAX_ARP_CACHE_OBJECTS  100  
#define IP_CLI_ARPCACHE_SIZE (MAX_ARP_CACHE_OBJECTS * CLI_MAX_COLUMN_WIDTH) +\
                                CLI_MAX_HEADER_SIZE

#define IP_CLI_RARP_CACHE_SIZE \
        (RARP_MAX_SERVER_TABLE_ENTRIES * CLI_MAX_COLUMN_WIDTH) + \
         CLI_MAX_HEADER_SIZE

#define INARP_OBJECTS   8  
#define IP_CLI_INARP_SIZE ( CLI_MAX_COLUMN_WIDTH * INARP_OBJECTS )

#define ICMPCONF_OBJECTS   12  
#define IP_CLI_ICMPCONF_SIZE ( CLI_MAX_COLUMN_WIDTH * ICMPCONF_OBJECTS )

#define ICMPSTAT_OBJECTS   20  
#define IP_CLI_ICMPSTAT_SIZE ( CLI_MAX_COLUMN_WIDTH * ICMPSTAT_OBJECTS )

#define MAX_IPPATHMTU_ENTRIES   10  
#define IP_CLI_IPPATHMTU_SIZE  (MAX_IPPATHMTU_ENTRIES * CLI_MAX_COLUMN_WIDTH) +\
                                CLI_MAX_HEADER_SIZE

#define IPGLOBAL_OBJECTS   20
#define IP_CLI_SHOW_IPGBL_CFG    ( CLI_MAX_COLUMN_WIDTH * IPGLOBAL_OBJECTS )


#define IP_DEF_TOS       0 
#define IP_CLI_MAX_ARGS  10

/* Command types for show ip arp command 
*/
#define     SHOW_IP_ARP                           1
#define     SHOW_IP_ARP_IPADDR                    2
#define     SHOW_IP_ARP_MACADDR                   3
#define     SHOW_IP_ARP_SUMMARY                   4
#define     SHOW_IP_ARP_INFO                      5
#define     SHOW_IP_ARP_INTF                      6  
#define     SHOW_IP_ARP_STATS                     7


/* Command types for show route command 
*/
#define     SHOW_ROUTE                            1
#define     SHOW_ROUTE_IPADDR                     2
#define     SHOW_ROUTE_BGP                        3
#define     SHOW_ROUTE_CONNECTED                  4
#define     SHOW_ROUTE_OSPF                       5
#define     SHOW_ROUTE_RIP                        6
#define     SHOW_ROUTE_STATIC                     7
#define     SHOW_ROUTE_SUMMARY                    8
#define     SHOW_ROUTE_DETAILS                    9
#define     SHOW_ROUTE_ISIS                      10
#define     SHOW_ROUTE_FAILED                    11


/* Command types for ip route command 
*/
#define     IP_ROUTE_NEXT_HOP                     1
#define     IP_ROUTE_IFACE_INDEX                  2


/* Command types for ip rarp client request command
*/
#define     RARP_CLIENT_REQ_RETRIES               1
#define     RARP_CLIENT_REQ_INTERVAL              2

/* Macros defined for default values */
#define IP_DEF_TTL                                64 
#define IP_DEF_AGGREGATE_ROUTES                   10 
#define IP_DEF_ARP_TIMEOUT                        300 
#define IP_DEF_MAXIMUM_PATHS                      10
#define IP_DEF_ROUTE_AGE                          180  
#define IP_DEF_REQ_RETRIES                        4
#define IP_DEF_REQ_INTERVAL                       100
  

#define     RRD_CLI_ENABLE                      "1"
#define     RRD_CLI_DISABLE                     "2"
#define     RRD_CLI_DEF_DEST                     0
#define     RRD_CLI_DEF_RANGE                    0xffffffff 
#define     RRD_CLI_ACTIVE                       1
#define     RRD_CLI_CREATEWAIT                   5
#define     RRD_CLI_DESTROY                      6
#define     RRD_CLI_PERMIT                       1
#define     RRD_CLI_DENY                         2
#define     RRD_CLI_RIP_MASK                     0x00000080
#define     RRD_CLI_OSPF_MASK                    0x00001000
#define     RRD_CLI_BGP_MASK                     0x00002000
#define     RRD_CLI_ALL_MASK                     0x00003080
#define     MAX_PHY_ADDR_LEN                     32 

#define     RRD_CLI_BGP                          14
#define     RRD_CLI_RIP                          8

#define CLI_NET2MEDIA_STATIC_TYPE    4
#define CLI_VAL_INVALID_ENTRY        2 
#define IP_ENABLE_STATUS             1 
#define IP_DISABLE_STATUS            2 
#define IP_PING_DUMMY_EVT            0x10000100
                                
#define   IP_MIN_DEF_TTL           1
#define   IP_MAX_DEF_TTL         255

/*CONSTANTS FOR 0,1 and -1*/
#define  IP_MINUS_ONE         -1
#define  IP_ZERO               0
#define  IP_ONE                1

#define  IP_CLI_HC_DISABLED    0

#define IP_ENABLE_IF       "1"
#define IP_DISABLE_IF      "2"

/* Buffer size for reading arp entries from /proc/net/arp */
#define LNX_MAX_ARP_ENTRY_SIZE           100
#define LNX_MAX_ARP_LINE_SIZE            200
#define LNX_STATIC_ARP                   0x6
#define LNX_DYNAMIC_ARP                  0x2
#define LNX_PENDING_ARP                  0x0

/* Macro to enable all trace levels in ARP module */
#define ARP_ALL_TRC            0x000000ff

#define IP_OCTETSTRING_TO_INTEGER(pOctetString,u4Index) { \
    u4Index = 0;\
        if(pOctetString->i4_Length > 0 && pOctetString->i4_Length <= 4){\
            MEMCPY((UINT1 *)&u4Index,pOctetString->pu1_OctetList,\
                   pOctetString->i4_Length);\
                u4Index = OSIX_NTOHL(u4Index);}}

#define IP_INTEGER_TO_OCTETSTRING(u4Index,pOctetString) {\
    UINT4 u4LocalIndex = OSIX_HTONL(u4Index);\
        MEMCPY(pOctetString->pu1_OctetList,(UINT1*)&u4LocalIndex,\
               sizeof(UINT4));\
        pOctetString->i4_Length=sizeof(UINT4);}


                                
/* Structure for Cli*/

typedef struct 
{
    UINT4  u4In_rcvs;      
    UINT4  u4In_hdr_err;
    UINT4  u4In_len_err;    
    UINT4  u4In_cksum_err; 
    UINT4  u4In_ver_err; 
    UINT4  u4In_ttl_err;
    UINT4  u4In_addr_err;
    UINT4  u4Reasm_reqs;
    UINT4  u4Reasm_oks;
    UINT4  u4Reasm_timeout;
    UINT4  u4Out_frag_pkts;
    UINT4  u4Frag_fails;
    UINT4  u4In_bcasts;
    UINT4  u4Out_requests;
    UINT4  u4Forw_attempts;
    UINT4  u4Out_no_routes;
    UINT4  u4Out_gen_err;
    UINT4  u4Bad_proto;
    UINT4  u4In_opt_err;
    INT4   i4IpForwEnable;
    INT4   i4DefaultTtl;
    INT4   i4AggRoutes;
    INT4   i4Multipath;
    INT4   i4IpLoadShareEnable;
    INT4   i4IpEnablePMTUD;
}t_CLI_IP_STATS;       

typedef struct 
{
    UINT4  u4IcmpInMsgs;
    UINT4  u4IcmpInErr;
    UINT4  u4IcmpInDestUnreach;
    UINT4  u4IcmpInRedirects;
    UINT4  u4IcmpInTimeExceeds;
    UINT4  u4IcmpInParmProbs;
    UINT4  u4IcmpInSrcQuench;
    UINT4  u4IcmpInEchos;
    UINT4  u4IcmpInEchosReps;
    UINT4  u4IcmpInAddrMasks;
    UINT4  u4IcmpInAddrMaskReps;
    UINT4  u4IcmpInTimeStamps;
    UINT4  u4IcmpInTimeStampsReps;
    UINT4  u4IcmpInDomainNameReq;
    UINT4  u4IcmpInDomainNameRep;
    UINT4  u4IcmpInSecurityFail;
    UINT4  u4IcmpOutMsgs;
    UINT4  u4IcmpOutErr;
    UINT4  u4IcmpOutDestUnreach;
    UINT4  u4IcmpOutRedirects;
    UINT4  u4IcmpOutTimeExceeds;
    UINT4  u4IcmpOutParmProbs;
    UINT4  u4IcmpOutSrcQuench;
    UINT4  u4IcmpOutEchos;
    UINT4  u4IcmpOutEchosReps;
    UINT4  u4IcmpOutAddrMasks;
    UINT4  u4IcmpOutAddrMaskReps;
    UINT4  u4IcmpOutTimeStamps;
    UINT4  u4IcmpOutTimeStampsReps;
    UINT4  u4IcmpOutDomainNameReq;
    UINT4  u4IcmpOutDomainNameRep;
    UINT4  u4IcmpOutSecurityFail;
    INT4   i4IcmpSendRedirectEnable;
    INT4   i4IcmpSendUnreachEnable;
    INT4   i4IcmpSendEchoReplyEnable;
    INT4   i4IcmpSendNetmaskReplyEnable;
}t_CLI_ICMP_STATS;       

typedef struct 
{
    UINT4  u4ArpInReq;
    UINT4  u4ArpInResp;
    UINT4  u4ArpInPkts;
    UINT4  u4ArpInDiscards;
    UINT4  u4ArpOutReq;
    UINT4  u4ArpOutResp;
    INT4   i4ArpProxy;
}t_CLI_ARP_STATS;       

typedef struct 
{
    t_CLI_IP_STATS    IpStats;
    t_CLI_ICMP_STATS  IcmpStats;
    t_CLI_ARP_STATS   ArpStats;
}tIpCliInfo;       
        
/* Command Identifier */

enum {
    CLI_IP_SHOW_ARP = 1,
    CLI_IP_SHOW_TRAFFIC,       
    CLI_IP_SHOW_RARP,       
    CLI_IP_SHOW_INFO,       
    CLI_IP_SHOW_ROUTE,       
    CLI_IP_SHOW_FAILED_ROUTE,
    CLI_IP_SHOW_PMTU,
    CLI_IP_ARP,
    CLI_IP_NO_ARP,
    CLI_IP_ARP_MAX_RETRIES,
    CLI_IP_NO_ARP_MAX_RETRIES,
    CLI_IP_ROUTE,
    CLI_IP_NO_ROUTE,
    CLI_IP_ROUTING,
    CLI_IP_NO_ROUTING,
    CLI_IP_MAXIMUM_PATHS,
    CLI_IP_NO_MAXIMUM_PATHS,
    CLI_IP_RARP_CLIENT_REQUEST,
    CLI_IP_NO_RARP_CLIENT_REQUEST,
    CLI_IP_DEFAULT_TTL,
    CLI_IP_NO_DEFAULT_TTL,
    CLI_IP_AGGREGATE_ROUTE,
    CLI_IP_NO_AGGREGATE_ROUTE,
    CLI_IP_TRAFFIC_SHARE,
    CLI_IP_NO_TRAFFIC_SHARE,
    CLI_IP_PATH_MTU_DISCOVER,
    CLI_IP_NO_PATH_MTU_DISCOVER,
    CLI_IP_PATH_MTU_ADD,
    CLI_IP_PATH_MTU_DEL,
    CLI_IP_ARP_TIMEOUT,
    CLI_IP_NO_ARP_TIMEOUT,
    CLI_IP_REDIRECTS,
    CLI_IP_NO_REDIRECTS,
    CLI_IP_UNREACHABLES,
    CLI_IP_NO_UNREACHABLES,
    CLI_IP_MASK_REPLY,
    CLI_IP_NO_MASK_REPLY,
    CLI_IP_ECHO_REPLY,
    CLI_IP_NO_ECHO_REPLY,
    CLI_IP_DIRECTED_BROADCAST,
    CLI_IP_NO_DIRECTED_BROADCAST,
    CLI_IP_RARP_CLIENT,
    CLI_IP_NO_RARP_CLIENT,
    CLI_IP_INTERFACE,
    CLI_IP_TRACEROUTE_TTL,
    CLI_IP_TRACEROUTE,
    CLI_IP_PROXY_ARP,
    CLI_IP_NO_PROXY_ARP,
    CLI_IP_SHOW_PROXY_ARP,
    CLI_IP_PROXY_ARP_SUBNET_OPTION,
    CLI_IP_NO_PROXY_ARP_SUBNET_OPTION,
    CLI_IP_LOCAL_PROXY_ARP,
    CLI_IP_NO_LOCAL_PROXY_ARP,
    CLI_IP_CLEAR_ARP,
    CLI_IP_ROUTE_DEF_DISTANCE,
    CLI_IP_SHOW_DEF_DISTANCE,
    CLI_ARP_DEBUG,
    CLI_IP_MAX_COMMANDS
};


/* Error codes */
enum {
    CLI_IP_INVALID_ARP_ENTRY_ERR = 1,
    CLI_IP_INVALID_IP_MASK_ERR,
    CLI_IP_INVALID_NEXTHOP_ERR,
    CLI_IP_INVALID_INTF_ERR,
    CLI_IP_INVALID_MAC_ERR,
    CLI_IP_RARP_ERR,
    CLI_IP_INV_ARPCACHE_TIMEOUT,
    CLI_IP_NO_DEST_ROUTE_ERR,
    CLI_IP_NO_NETWORK_ERR,
    CLI_IP_ARP_MAX_ENTRIES_ERR,
    CLI_IP_PMTU_MAX_ENTRIES_ERR,
    CLI_IP_MAX_STATIC_RT_ERR,
    CLI_IP_INVALID_ADMIN_STATUS,
    CLI_IP_INVALID_LPARP_STATUS,
    CLI_IP_INVALID_PARP_STATUS,
    CLI_IP_LOCAL_IPADDR,    
    CLI_IP_CONNECTED_ROUTE_EXISTS,    
    CLI_IP_INVALID_FLUSH_STATUS,
    CLI_IP_INVALID_DEF_DISTANCE,
    CLI_IP_DISTANCE_SET_ERR,
    CLI_IP_DISTANCE_GET_ERR,
    CLI_IP_ROUTE_LEAK_ERR,
    CLI_IP_MAX_ERR
   }; 

#ifdef __IPCLI_C__ 
CONST CHR1         *StdipCliErrString[] = {
    NULL,
    "\r% No matching ARP entry to delete\r\n",
    "\r% Inconsistent address and mask\r\n",
    "\r% Invalid next hop address specified\r\n",
    "\r% Invalid interface specified\r\n",
    "\r% Invalid mac address specified\r\n",
    "\r% RARP allowed on ethernet interfaces only\r\n",
    "\r% Invalid arpcache timeout value\r\n",
    "\r% Route entry not Present\r\n",
    "\r% Network not in table\r\n",
    "\r% Max number of ARP entries already exist\r\n",
    "\r% Max number of Pmtu entries already exist\r\n",
    "\r% Exceeded maximum static routes allowed\r\n",
    "\r% Invalid admin status specified\r\n",
    "\r% Enable Proxy arp in this interface\r\n",
    "\r% Disable local proxy arp in this interface\r\n",
    "\r% Ip address  is local address\r\n",
    "\r% Connected route for same network exists\r\n",
    "\r% Invalid Cache Flush Status\r\n",
    "\r% Invalid Default Administrative Distance value\r\n",
    "\r% Default Administrative Distance value not set\r\n",
    "\r% Default Administrative Distance value get failed\r\n",
    "\r% Enable VRF route leaking to configure Optional Next-hop IP address\r\n"
};
#endif
/* Prototypes added for ipcli.c */


INT4 cli_process_ip_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Comamnd, ...));  
INT4 cli_process_stdip_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Comamnd, ...));  
INT4 cli_process_arp_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Comamnd, ...));  
INT4 cli_process_rarp_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Comamnd, ...));  
INT4 cli_process_rtm_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Comamnd, ...));  
INT4 IpShowArpInfoInCxt PROTO ((tCliHandle, UINT4));
INT4 IpShowArpStatsInCxt PROTO ((tCliHandle, UINT4));
INT4 IpShowArpInCxt PROTO ((tCliHandle, UINT4));
INT4 IpSetClearArp PROTO ((tCliHandle, UINT4));
VOID IpShowLinuxVlanArp PROTO ((tCliHandle, INT1));
VOID IpShowLinuxVlanArpIpAddress PROTO ((tCliHandle, UINT4, INT1));
VOID IpShowLinuxVlanArpMacAddress PROTO ((tCliHandle, UINT1 *, INT1));
VOID IpGetLinuxArpCount PROTO ((UINT4 *, UINT4 *));
INT4 IpShowArpInterfaceInCxt PROTO ((tCliHandle, UINT4, INT4));
INT4 IpShowArpIpAddressInCxt PROTO ((tCliHandle, UINT4, UINT4));
INT4 IpShowArpMacAddressInCxt PROTO ((tCliHandle, UINT4, UINT1 *));
INT4 IpShowArpSummaryInCxt PROTO ((tCliHandle, UINT4));
INT4 IpShowTrafficInCxt PROTO ((tCliHandle, UINT1, UINT4));
INT4 IpShowIfTrafficInCxt PROTO ((tCliHandle, UINT4, UINT1, UINT4));
INT1 IpSetInterfaceAdminStatus PROTO ((tCliHandle, UINT4, INT4));
INT4 IpShowRarp PROTO ((tCliHandle));
INT4 IpShowInfoInCxt PROTO ((tCliHandle, UINT4));
INT1 CliIpGetNextRouteInCxt PROTO ((UINT4 , UINT4, UINT4, tRtInfo **));
INT1 CliIpGetNextRoute PROTO ((UINT4 , UINT4, tRtInfo **));
INT4 IpShowFrtEntry(tCliHandle CliHandle);
INT4 IpShowRouteInCxt PROTO ((tCliHandle, INT1, UINT4));
INT4 IpShowRouteProtoInCxt PROTO ((tCliHandle, INT1, UINT4));
INT4 IpShowRouteSummaryInCxt PROTO ((tCliHandle, UINT4));
INT4 IpShowRouteIpAddrInCxt PROTO ((tCliHandle , INT1, INT4, INT4, UINT4));
INT4 IpShowIgmpAllInterface PROTO ((tCliHandle));
INT4 IpShowIgmpInterface PROTO ((tCliHandle, INT4));
INT4 IpIgmpShowMroute PROTO ((tCliHandle));
INT4 IpSetArp PROTO ((tCliHandle, INT4, UINT4 , UINT1 *, UINT4));
INT4 IpSetNoArp PROTO ((tCliHandle, UINT4, UINT4));
INT4 IpSetArpCacheTimeout PROTO ((tCliHandle, INT4));
INT4 IpSetProxyArp PROTO ((tCliHandle, INT4));
INT4 IpSetIcmpRedirects PROTO ((tCliHandle, INT4));
INT4 IpSetIcmpUnreachables PROTO ((tCliHandle, INT4));
INT4 IpSetIcmpMaskReply PROTO ((tCliHandle, INT4));
INT4 IpSetIcmpEchoReply PROTO ((tCliHandle, INT4));
INT4 IpSetArpMaxRetries PROTO ((tCliHandle, INT4));
INT4 IpSetRoute PROTO ((tCliHandle, UINT4,INT4, UINT4, UINT4, UINT4, 
                                                  INT4 , INT4));
INT4 IpSetNoRoute PROTO ((tCliHandle, UINT4,UINT4, UINT4, UINT4, INT4));
INT4 IpSetNoRouteInterface PROTO ((tCliHandle, INT4, UINT4, UINT4, UINT4));
INT4 IpSetRouting PROTO ((tCliHandle, INT4));
INT4 IpSetMaximumPaths PROTO ((tCliHandle, INT4));
INT4 IpSetRarpReqTimeout PROTO ((tCliHandle, INT4));
INT4 IpSetRarpReqRetries PROTO ((tCliHandle, INT4));
INT4 IpSetDefaultTTL PROTO ((tCliHandle, INT4));
INT4 IpSetAggregateRoutes PROTO ((tCliHandle, INT4));
INT4 IpSetTrafficShare PROTO ((tCliHandle, INT4));
INT4 IpSetPathMtuDiscover PROTO ((tCliHandle, INT4));
INT4 IpSetIgmpStatus PROTO ((tCliHandle, INT4));
INT4 IpSetIgmpMroute PROTO ((tCliHandle, INT4, INT4));
INT4 IpSetNoIgmpMroute PROTO ((tCliHandle, INT4, INT4));
INT4 IpSetDirectedBcast PROTO ((tCliHandle, INT4, INT4 ));
INT4 IpSetRarpClientEnable PROTO ((tCliHandle, INT4 ));
INT4 IpSetRarpClientDisable PROTO ((tCliHandle, INT4 ));
INT4 IpSetIgmpVersion PROTO ((INT4, INT4));
INT4 IpSetIgmpQueryInterval PROTO ((INT4, INT4));
INT4 IpSetIgmpQueryResponseTime PROTO ((INT4, INT4));
INT4 IpIgmpIfAdd PROTO ((tCliHandle, INT4, UINT4, INT4));
INT4 IpIgmpDelete PROTO ((tCliHandle, INT4)); 
INT4 IpPathMtuAdd PROTO ((tCliHandle,UINT4,INT4,INT4));
INT4 IpPathMtuDel PROTO ((tCliHandle,UINT4,INT4));
INT4 ShowIpPmtuInCxt (tCliHandle CliHandle, UINT4);
INT4 FsIpShowInfoInCxt (tCliHandle CliHandle, INT4);
INT4 IpSetTraceRouteMinTtl (tCliHandle, UINT4, UINT4);
INT4 IpSetTraceRouteMaxTtl (tCliHandle, UINT4, UINT4);
INT4 IpConfigProxyArp PROTO ((tCliHandle , INT4 , UINT1 ));
INT4 IpConfigProxyArpSubnetOption PROTO ((tCliHandle , UINT1 ));
INT4 IpShowProxyArpInfoInCxt PROTO ((tCliHandle, UINT4 ));
INT4 IpArpSetDebugLevel PROTO((tCliHandle, UINT4, UINT4, UINT4));
VOID IssIpArpShowDebugging PROTO((tCliHandle));  
INT4 IpConfigLocalProxyArp PROTO ((tCliHandle , INT4 , UINT1 ));
INT4 IpAddrMatch PROTO ((UINT4, UINT4, UINT4));
INT4 IpSetDefAdminDistance PROTO ((tCliHandle, UINT4, INT4 ));
INT4 IpGetDefAdminDistance PROTO ((UINT4, INT4 *));
INT4 IpShowDefAdminDistance PROTO ((tCliHandle, UINT4 ));


#ifdef IP_WANTED
INT4 FsIpShowRunningConfigInCxt(tCliHandle CliHandle, UINT4 u4Module, INT4);
INT4 FsIpShowRunningConfigInterfaceInCxt(tCliHandle CliHandle, INT4, UINT4);
INT4 FsIpShowRunningConfigInterfaceDetails(tCliHandle CliHandle, INT4 i4Index);
INT4 FsIpShowRunningConfigTablesInCxt (tCliHandle CliHandle, INT4);
INT4 FsIpShowRunningConfigScalarsInCxt (tCliHandle CliHandle, INT4);
#endif

VOID IpvxShowRunningConfigIfDetails (tCliHandle CliHandle, INT4 i4Index);

INT4 IpShowRunningConfigInCxt (tCliHandle CliHandle, UINT4);
INT4 IpShowRunningConfigTablesInCxt (tCliHandle CliHandle, UINT4);
INT4 IpShowRunningConfigScalarsInCxt (tCliHandle CliHandle, UINT4);

extern INT1 nmhGetIfAdminStatus (INT4, INT4 *);
extern INT1 nmhGetIfOperStatus (INT4, INT4 *);
extern INT1 nmhGetFsIpArpMaxRetries (INT4 *);
extern INT1 nmhGetFsIpArpCacheTimeout (INT4 *);
/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetIpSystemStatsInReceives ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpSystemStatsHCInReceives ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetIpSystemStatsInOctets ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpSystemStatsHCInOctets ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetIpSystemStatsInHdrErrors ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpSystemStatsInNoRoutes ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpSystemStatsInAddrErrors ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpSystemStatsInUnknownProtos ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpSystemStatsInTruncatedPkts ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpSystemStatsInForwDatagrams ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpSystemStatsHCInForwDatagrams ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetIpSystemStatsReasmReqds ARG_LIST((INT4 ,UINT4 *));


extern INT1
nmhGetIpSystemStatsReasmOKs ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpSystemStatsReasmFails ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpSystemStatsInDiscards ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpSystemStatsInDelivers ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpSystemStatsHCInDelivers ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetIpSystemStatsOutRequests ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpSystemStatsHCOutRequests ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetIpSystemStatsOutNoRoutes ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpSystemStatsOutForwDatagrams ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpSystemStatsHCOutForwDatagrams ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetIpSystemStatsOutDiscards ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpSystemStatsOutFragReqds ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpSystemStatsOutFragOKs ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpSystemStatsOutFragFails ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpSystemStatsOutFragCreates ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpSystemStatsOutTransmits ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpSystemStatsHCOutTransmits ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetIpSystemStatsOutOctets ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpSystemStatsHCOutOctets ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetIpSystemStatsInMcastPkts ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpSystemStatsHCInMcastPkts ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetIpSystemStatsInMcastOctets ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpSystemStatsHCInMcastOctets ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetIpSystemStatsOutMcastPkts ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpSystemStatsHCOutMcastPkts ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetIpSystemStatsOutMcastOctets ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpSystemStatsHCOutMcastOctets ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetIpSystemStatsInBcastPkts ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpSystemStatsHCInBcastPkts ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetIpSystemStatsOutBcastPkts ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpSystemStatsHCOutBcastPkts ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetIpSystemStatsDiscontinuityTime ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpSystemStatsRefreshRate ARG_LIST((INT4 ,UINT4 *));


/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetIpIfStatsTableLastChange ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for IpIfStatsTable. */
extern INT1
nmhValidateIndexInstanceIpIfStatsTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for IpIfStatsTable  */

extern INT1
nmhGetFirstIndexIpIfStatsTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexIpIfStatsTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetIpIfStatsInReceives ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIpIfStatsHCInReceives ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetIpIfStatsInOctets ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIpIfStatsHCInOctets ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetIpIfStatsInHdrErrors ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIpIfStatsInNoRoutes ARG_LIST((INT4  , INT4 ,UINT4 *));


extern INT1
nmhGetIpIfStatsInAddrErrors ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIpIfStatsInUnknownProtos ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIpIfStatsInTruncatedPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIpIfStatsInForwDatagrams ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIpIfStatsHCInForwDatagrams ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetIpIfStatsReasmReqds ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIpIfStatsReasmOKs ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIpIfStatsReasmFails ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIpIfStatsInDiscards ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIpIfStatsInDelivers ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIpIfStatsHCInDelivers ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetIpIfStatsOutRequests ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIpIfStatsHCOutRequests ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetIpIfStatsOutForwDatagrams ARG_LIST((INT4  , INT4 ,UINT4 *));


extern INT1
nmhGetIpIfStatsHCOutForwDatagrams ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetIpIfStatsOutDiscards ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIpIfStatsOutFragReqds ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIpIfStatsOutFragOKs ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIpIfStatsOutFragFails ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIpIfStatsOutFragCreates ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIpIfStatsOutTransmits ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIpIfStatsHCOutTransmits ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetIpIfStatsOutOctets ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIpIfStatsHCOutOctets ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetIpIfStatsInMcastPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIpIfStatsHCInMcastPkts ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetIpIfStatsInMcastOctets ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIpIfStatsHCInMcastOctets ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));


extern INT1
nmhGetIpIfStatsOutMcastPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIpIfStatsHCOutMcastPkts ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetIpIfStatsOutMcastOctets ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIpIfStatsHCOutMcastOctets ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetIpIfStatsInBcastPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIpIfStatsHCInBcastPkts ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetIpIfStatsOutBcastPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIpIfStatsHCOutBcastPkts ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetIpIfStatsDiscontinuityTime ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIpIfStatsRefreshRate ARG_LIST((INT4  , INT4 ,UINT4 *));

/* Proto Validate Index Instance for IcmpStatsTable. */
extern INT1
nmhValidateIndexInstanceIcmpStatsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IcmpStatsTable  */

extern INT1
nmhGetFirstIndexIcmpStatsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexIcmpStatsTable ARG_LIST((INT4 , INT4 *));


/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetIcmpStatsInMsgs ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIcmpStatsInErrors ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIcmpStatsOutMsgs ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIcmpStatsOutErrors ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for IcmpMsgStatsTable. */
extern INT1
nmhValidateIndexInstanceIcmpMsgStatsTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for IcmpMsgStatsTable  */

extern INT1
nmhGetFirstIndexIcmpMsgStatsTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexIcmpMsgStatsTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetIcmpMsgStatsInPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIcmpMsgStatsOutPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

/* Proto Validate Index Instance for Ipv4InterfaceTable. */
extern INT1
nmhValidateIndexInstanceIpv4InterfaceTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Ipv4InterfaceTable  */

extern INT1
nmhGetFirstIndexIpv4InterfaceTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexIpv4InterfaceTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetIpv4InterfaceReasmMaxSize ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIpv4InterfaceEnableStatus ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIpv4InterfaceRetransmitTime ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetIpv4InterfaceEnableStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2Ipv4InterfaceEnableStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
/* Low Level DEP Routines for.  */

extern INT1
nmhDepv2Ipv4InterfaceTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));


#endif /* __IPCLI_H__ */
