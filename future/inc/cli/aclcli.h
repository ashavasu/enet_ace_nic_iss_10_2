/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: aclcli.h,v 1.54 2017/09/12 13:24:33 siva Exp $
 *
 * Description:
 * 
 *******************************************************************/
#ifndef __ACLCLI_H__
#define __ACLCLI_H__
#include "cli.h"
#include "fssnmp.h"

#define ISS_MAX_LEN 255
#define ISS_ADDR_LEN 22
#define HOST_MASK "255.255.255.255"
#define CLI_IPV6ACL_MODE    "IPV6Acl"
enum {
    CLI_IP_ACL =1,
    CLI_NO_IP_ACL,
    CLI_MAC_ACL,
    CLI_USR_DEF_ACL,
    CLI_NO_MAC_ACL,
    CLI_NO_USR_DEF_ACL,
    CLI_IPV6_ACL,
    CLI_NO_IPV6_ACL,
    CLI_ACL_PERMIT,
    CLI_ACL_DENY,
    CLI_USR_DEFANDORNOT_FILTER, 
    CLI_ACL_UDB_PERMIT,
    CLI_ACL_UDB_DENY,
    CLI_ACL_PERMIT_PROTO,
    CLI_ACL_REDIRECT_PROTO,
    CLI_ACL_DENY_PROTO,
    CLI_ACL_PERMIT_TCP,
    CLI_ACL_DENY_TCP,
    CLI_ACL_PERMIT_UDP,
    CLI_ACL_DENY_UDP,
    CLI_ACL_PERMIT_ICMP,
    CLI_ACL_DENY_ICMP,
    CLI_ACL_PERMIT_IPV6_PROTO,
    CLI_ACL_DENY_IPV6_PROTO,
    CLI_ACL_PERMIT_IPV6_TCP,
    CLI_ACL_DENY_IPV6_TCP,
    CLI_ACL_PERMIT_IPV6_UDP,
    CLI_ACL_DENY_IPV6_UDP,
    CLI_ACL_PERMIT_IPV6_ICMP,
    CLI_ACL_DENY_IPV6_ICMP,
    CLI_ACL_PERMIT_UP_CFI_DEI,
    CLI_ACL_PERMIT_DP,
    CLI_ACL_IP_ACCESS_GRP,
    CLI_ACL_IP_NO_ACCESS_GRP,
    CLI_PERMIT_MAC_ACL,
    CLI_REDIRECT_MAC_ACL,
    CLI_DENY_MAC_ACL,
    CLI_ACL_MAC_ACCESS_GRP,
    CLI_ACL_MAC_NO_ACCESS_GRP,
    CLI_ACL_IPV6_ACCESS_GRP,
    CLI_ACL_IPV6_NO_ACCESS_GRP,
    CLI_ACL_UDB_ACCESS_GRP,
    CLI_ACL_UDB_NO_ACCESS_GRP,
    CLI_ACL_PB_PERMIT_PROTO,
    CLI_ACL_PB_DENY_PROTO,
    CLI_ACL_PB_PERMIT_TCP,
    CLI_ACL_PB_DENY_TCP,
    CLI_ACL_PB_PERMIT_UDP,
    CLI_ACL_PB_DENY_UDP,
    CLI_ACL_PB_PERMIT_ICMP,
    CLI_ACL_PB_DENY_ICMP,
    CLI_PB_PERMIT_MAC_ACL,
    CLI_PB_DENY_MAC_ACL,
    CLI_UDB_ACL,
    CLI_ACL_SHOW,
    CLI_ACL_L2_RESET_COUNT,
    CLI_ACL_L3_RESET_COUNT,
    CLI_TRAFFIC_SEPARATION_CONTROL,
    CLI_ACL_COPYTOCPU,
    CLI_ACL_COPYTOCPU_PROTO,
    CLI_ACL_COPYTOCPU_IPV6_PROTO,
    CLI_ACL_COPYTOCPU_TCP,
    CLI_ACL_COPYTOCPU_UDP,
    CLI_ACL_COPYTOCPU_ICMP,
    CLI_COPYTOCPU_MAC_ACL,
    CLI_ACL_PB_COPYTOCPU_PROTO,
    CLI_ACL_PB_COPYTOCPU_TCP,
    CLI_ACL_PB_COPYTOCPU_UDP,
    CLI_ACL_PB_COPYTOCPU_ICMP,
    CLI_PB_COPYTOCPU_MAC_ACL,
    CLI_ACL_COPYTOCPU_IPV6_ICMP,
    CLI_EG_ACL_MODE,
    CLI_EG_ACL_SHOW,
    CLI_CLR_MAC_ACL_COUNT,
    CLI_CLR_IP_ACL_COUNT,
    CLI_CLR_USR_DEF_ACL_COUNT,
    CLI_RESR_FRM_TRNS,
    CLI_NO_RESR_FRM_TRNS,
    CLI_ACL_RESRV_FRAME_SHOW,
    CLI_ACL_MAX_COMMANDS
};

/* Command identifiers for ip access-list command */
#define ACL_STANDARD 1
#define ACL_EXTENDED 2

/* Macro for maximum value of standard access list number */
#define ACL_STANDARD_MAX_VALUE 1000
#define ACL_STD_MAX_VALUE 1000

#define ACL_STANDARD_MIN_VALUE 1

/* Command identifiers for permit/deny command */
#define ACL_ANY                  1
#define ACL_HOST_IP              2
#define ACL_HOST_IP_MASK         3 

#define ACL_SRC                  1
#define ACL_DST                 2
/* Command identifiers for access list commands */
#define ACL_HOST_MAC             2

#define ISS_BIT8                        0x80

/* Command identifier for permit tcp command */
#define ACL_GREATER_THAN_PORT        1
#define ACL_LESSER_THAN_PORT         2
#define ACL_EQUAL_TO_PORT            3
#define ACL_RANGE_PORT               4

/* Command identifier to distinguish ack or rst in permit tcp command */
#define ACL_ACK                  1
#define ACL_RST                  2

/* Command identifier to distinguish in or out in access group command */
#define ACL_ACCESS_IN            1
#define ACL_ACCESS_OUT           2

/* Command identifier to enable or disable hardware statistics */
#define ACL_STAT_ENABLE          1
#define ACL_STAT_DISABLE         2

#define ACL_SCH_FILTER_ENABLE   1
#define ACL_SCH_FILTER_DISABLE  2

/*ACL dscp values*/
#define ACL_DSCP_AF11       10
#define ACL_DSCP_AF12       12
#define ACL_DSCP_AF13       14
#define ACL_DSCP_AF21       18
#define ACL_DSCP_AF22       20
#define ACL_DSCP_AF23       22
#define ACL_DSCP_AF31       26
#define ACL_DSCP_AF32       28
#define ACL_DSCP_AF33       30
#define ACL_DSCP_AF41       34
#define ACL_DSCP_AF42       36
#define ACL_DSCP_AF43       38
#define ACL_DSCP_CS1        8          
#define ACL_DSCP_CS2        16  
#define ACL_DSCP_CS3        24
#define ACL_DSCP_CS4        32      
#define ACL_DSCP_CS5        40
#define ACL_DSCP_CS6        48
#define ACL_DSCP_CS7        56
#define ACL_DSCP_EF         46
#define ACL_DSCP_DEFAULT    0

/* Command identifier to distinguish in or out in access group command */
#define AARP                    0x80F3 
#define AMBER                   0x6008 
#define DEC_SPANNING            0x8138
#define DECNET_IV               0x6003
#define DIAGNOSTIC              0x6005
#define DSM                     0x8039
#define ETYPE_6000              0x6000
#define ETYPE_8042              0x8042
#define LAT                     0x6004
#define LAVC_SCA                0x6007
#define MOP_CONSOLE             0x6002
#define MOP_DUMP                0x6001
#define MSDOS                   0x8041
#define MUMPS                   0x6009 
#define NET_BIOS                0x8040 
#define VINES_ECHO              0x0BAF 
#define VINES_IP                0x0BAD
#define XNS_ID                  0x0807

#define ISS_PROT_ICMP      1
#define ISS_PROT_IGMP      2
#define ISS_PROT_GGP       3
#define ISS_PROT_IP        4
#define ISS_PROT_TCP       6
#define ISS_PROT_EGP       8
#define ISS_PROT_IGP       9
#define ISS_PROT_NVP       11
#define ISS_PROT_UDP       17
#define ISS_PROT_IRTP      28
#define ISS_PROT_IDPR      35
#define ISS_PROT_RSVP      46
#define ISS_PROT_MHRP      48
#define ISS_PROT_ICMP6     58
#define ISS_PROT_IGRP      88
#define ISS_PROT_OSPFIGP   89
#define ISS_PROT_PIM       103
#define ISS_PROT_ANY       255


/* ICMP message type - IP filter */
#define ISS_ECHO_REPLY     0
#define ISS_DEST_UNREACH   3
#define ISS_SRC_QUENCH     4
#define ISS_REDIRECT       5
#define ISS_ECHO_REQ       8
#define ISS_TIME_EXCEED    11
#define ISS_PARAM_PROB     12
#define ISS_TIMEST_REQ     13
#define ISS_TIMEST_REP     14
#define ISS_INFO_REQ       15
#define ISS_INTO_REP       16
#define ISS_ADD_MK_REQ     17
#define ISS_ADD_MK_REP     18
#define ISS_NO_ICMP_TYPE   255

/* ICMP message code - IP filter */
#define ISS_NET_UNREACH    0
#define ISS_HOST_UNREACH   1
#define ISS_PROT_UNREACH   2
#define ISS_PORT_UNREACH   3
#define ISS_FRG_NEED       4
#define ISS_SRC_RT_FAIL    5
#define ISS_DST_NET_UNK    6
#define ISS_DST_HOST_UNK   7
#define ISS_SRC_HOST_ISO   8
#define ISS_DST_NET_ADMP   9
#define ISS_DST_HOST_ADMP  10
#define ISS_NET_UNRE_TOS   11
#define ISS_HOST_UNRE_TOS  12
#define ISS_NO_ICMP_CODE   255

#define ISS_PORTLIST_LEN   BRG_PHY_PORT_LIST_SIZE

#define ISS_DEFAULT_INDEX 0
#define DEFAULT_ENABLE 1
#define ISS_ERROR  -1
#define CLI_ACL_MAX_ARGS 33 
#define ACL_MAX_NAME_LENGTH 16
#define  CLI_STDACL_MODE "StdAcl"
#define  CLI_EXTACL_MODE "ExtAcl"
#define  CLI_MACACL_MODE "MacAcl"
#define  CLI_USRDEFACL_MODE "UdbAcl"
#define CLI_IPV6_EQUAL_ZERO(ipv6)   ((ipv6.u4_addr[0] == 0) &&\
                                     (ipv6.u4_addr[1] == 0) &&\
                                     (ipv6.u4_addr[2] == 0) &&\
                                     (ipv6.u4_addr[3] == 0))
/******* Traffic Separation ************/
#define ACL_SWITCH     1
#define ACL_NO_SWITCH  2

/* Egress Filter Mode */
#define EG_IP_ACL  1
#define EG_MAC_ACL 2
/* Error codes  *
 * Error code values and strings are maintained inside the protocol module itself.
 * */
enum {
        CLI_ACL_ERR = 0,
        CLI_ACL_FILTER_NO_CHANGE,
        CLI_ACL_PROVIDERBRIDGE_ERR,
        CLI_ACL_FILTER_NO_MEMBERS,
        CLI_ACL_FILTER_CREATION_FAILED,
        CLI_ACL_TCP_ERR,
        CLI_ACL_NO_FILTER,
        CLI_ACL_FILTER_EXISTS,
        CLI_ACL_FILTER_MAX,
        CLI_ACL_HW_UPDATE_FAILED,
        CLI_ACL_FILTER_NOT_ACTIVE,
        CLI_ACL_FILTER_DATA_INSUFFICIENCY,
        CLI_ACL_DELETION_FAILED,
        CLI_ACL_FILTER_ACTIVE_FAILED,
        CLI_ACL_SRC_DST_IP_ADDR_CONF_ERR,
        CLI_IPV6_MULTIPLE_PORTS,
        CLI_ACL_FILTER_NO_PORT,
 CLI_ACL_FILTER_MAPPED_TO_CLSMAP,
 CLI_ACL_INVALID_FILTER_ID,
        CLI_ACL_UNABLE_TO_SET,
         CLI_ACL_FILTER_ACTION_FAILS,
         CLI_ACL_FILTER_SRC_PORT_ERR,
        CLI_ACL_OUT_MULTIPLE_PORTS,
 CLI_ACL_INVALID_RESER_FRAME_CTRL_ID,
 CLI_ACL_INVALID_RESERV_FRAME_PKT_TYPE,
 CLI_ACL_INVALID_RESERV_FRAME_MAC_ADDRESS,
 CLI_ACL_INVALID_RESERV_FRAME_MAC_MASK,
 CLI_ACL_INVALID_RESERV_FRAME_CONTROL_ACTION,
 CLI_ACL_RESERV_FRAME_ENTRY_EXIST,
 CLI_ACL_RESERV_FRAME_ENTRY_NOT_FOUND,
 CLI_ACL_MAX_RESERV_FRAME_ENTRY,
 CLI_ACL_ALL_PROTO_ACT_DONE,
CLI_ACL_RESRV_FRM_ENTRY_EXIST,
         CLI_ACL_INVALID_COPY_ERR,
         CLI_ACL_SCH_FILTER_ERROR,
         CLI_ACL_SET_ERROR_ON_PORT,
         CLI_ACL_SET_ERROR_ON_PORT_CHANNEL,
         CLI_ACL_FILTER_INVALID_L4_PORT_RANGE,
         CLI_ACL_EGRESS_FILTER_ALREADY_MAPPED,
        CLI_ACL_MAX_OUT_DIRECTION,
        CLI_ACL_MAX_ERR
};

/*Reserve frame transmission protocols*/
enum {
        RESERVED_FRAME_BPDU = 1,
        RESERVED_FRAME_LACPDU,
        RESERVED_FRAME_EAP,
        RESERVED_FRAME_LLDPDU,
        RESERVED_FRAME_OTHER,
        RESERVED_FRAME_ALL,
};    

/*Reserved Frame transmissions action */
#define RESERVED_FRAME_ACTION_FWD   1
#define RESERVED_FRAME_ACTION_DROP   2

/* The error strings should be places under the switch so as to avoid redifinition
 *  * This will be visible only in modulecli.c file
 *   */
#ifdef __ACLCLI_C__

CONST CHR1  *AclCliErrString [] = {
        NULL,
        "% This filter is associated with some class-map in Diffserv\r\n",
        "% Bridge is not in provider bridge mode\r\n",
        "% Filter will be activated after any port association.\r\n",
        "% Filter creation failed\r\n",
        "% The Filter port values are out of range.\r\n",
        "% Filter does not exist\r\n",
        "% Filter already exists\r\n",
        "% Number of filter creation reached maximum\r\n",
        "% Hardware Update failed\r\n",
        "% Filter is not active\r\n",
        "% Filter Data is not sufficient\r\n",
        "% Filter Deletion Failed\r\n",
        "% Filter status should be in NotInService\r\n",
        "% FlowId cannot be configured if src/dst ipv6 address is configured\r\n", 
        "% Only a single port can be configured in an IPv6 ACL\r\n", 
        "% Filter status cannot be made active without any ports\r\n",
 "% Filter is mapped to class-map entry. Unmap filter & Class Map to Modify/delete filter.\r\n",
 "% Invalid Filter ID\r\n",
        "% Unable to mark the status of ACL as active after changing the rule\r\n",
          "% Filter creation failed.L2,L3 Filters action should be same.\r\n",
          "% Source port is not in valid range.\r\n",
        "% Only a single port can be configured in an Out ACL\r\n", 
  "% Invalid Reserve frame control table index.\r\n",
 "% Invalid Reserve frame Packet Type.\r\n",
 "% Invalid Reserve frame Mac address.\r\n",
 "% Invalid Reserve frame Mac address mask.\r\n",
 "% Invalid Reserve frame control action.\r\n",
 "% Reserve Frame entry already Exist.\r\n", 
 "% Reserve Frame entry not found.\r\n",
 "% Maximum Reserve Frame Entries reached.\r\n",
 "% Specific rule configuration is not allowed, if the reserved frame entry is configured for ALL protocols.\r\n",
 "% Conflicting rule with existing entry.\r\n",
 "% Copy-to-cpu Action Is Not Applicable For Egress ACL.\r\n",
 "% L2 Filter cannot be Applied for this S-Channel Interface.\r\n ", 
 "% Member of port-channel is already configured with ACL.Configuration not allowed.\r\n",
 "% port-channel is already configured with ACL.Configuration not allowed.\r\n",
 "% Invalid L4 Source/Destination port range.\r\n",
 "% Filter is already mapped to another interface in Egress direction.\r\n",
 "% Number of egress filter reached maximum.\r\n",
        "\r\n"
};
#endif


INT4 cli_process_acl_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Comamnd, ...));

INT1 StdAclGetCfgPrompt PROTO ((INT1 *, INT1 *));

INT1 ExtAclGetCfgPrompt PROTO ((INT1 *, INT1 *));

INT1 MacAclGetCfgPrompt PROTO ((INT1 *, INT1 *));

INT1 UserDefAclGetCfgPrompt PROTO ((INT1 *, INT1 *));

INT1 AclIPv6GetCfgPrompt PROTO ((INT1 *, INT1 *));


INT4 AclResetCount PROTO ((tCliHandle, UINT1));


#ifdef __ACLCLI_C__
#define ACL_PORTS_PER_BYTE 8
UINT1               gau1UtlAclPortBitMask[ACL_PORTS_PER_BYTE] =
    { 0x01, 0x80, 0x40, 0x20, 0x10,
    0x08, 0x04, 0x02
};

#define ISS_PORT_SIZE(u2Port) \
        ((u2Port > ISS_PORTLIST_LEN)? \
           ISS_PORT_CHANNEL_LIST_SIZE: ISS_PORTLIST_LEN)

#define ACL_ADD_PORT_LIST(au1PortArray, u2Port) \
{\
    UINT2 u2PortBytePos;\
        UINT2 u2PortBitPos;\
        u2PortBytePos = (UINT2)(u2Port / ACL_PORTS_PER_BYTE);\
        u2PortBitPos  = (UINT2)(u2Port % ACL_PORTS_PER_BYTE);\
        if (u2PortBitPos  == 0) {u2PortBytePos = (UINT2)(u2PortBytePos - 1);} \
        if (u2PortBytePos < ISS_PORT_SIZE(u2Port)) \
        {\
            au1PortArray[u2PortBytePos] = (UINT1)(au1PortArray[u2PortBytePos] \
                                                       | gau1UtlAclPortBitMask[u2PortBitPos]);\
        }\
}
#define ACL_DEL_PORT_LIST(au1PortArray, u2Port) \
{\
    UINT2 u2PortBytePos;\
        UINT2 u2PortBitPos;\
        u2PortBytePos = (UINT2)(u2Port / ACL_PORTS_PER_BYTE);\
        u2PortBitPos  = (UINT2)(u2Port % ACL_PORTS_PER_BYTE);\
        if (u2PortBitPos  == 0) {u2PortBytePos = (UINT2)(u2PortBytePos - 1);} \
        if (u2PortBytePos < ISS_PORT_SIZE(u2Port))\
        {\
          au1PortArray[u2PortBytePos] = (UINT1)(au1PortArray[u2PortBytePos]                                            & ~ gau1UtlAclPortBitMask[u2PortBitPos]);\
        }\
}
#endif

#endif
