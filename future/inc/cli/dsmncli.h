/********************************************************************
 *                                                                  *
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved             *
 *            *
 * $Id: dsmncli.h,v 1.5 2013/08/22 15:24:49 siva Exp $     *
 *            *
 * Description: This file contains the data structure and macros    *
 *              for DSMON CLI module                                *
 *                                                                  *
 *******************************************************************/

#ifndef _DSMNCLI_H_
#define _DSMNCLI_H_

#include "lr.h"
#include "cli.h"


#define DSMON_MAX_ARGS 5



/* Command Identifiers */
enum{
    DSMON_CLI_STATUS = 1,                /*1*/
    DSMON_CLI_SHOW_TRACE,                /*2*/
    DSMON_CLI_TRACE,                     /*3*/
    DSMON_CLI_RESET_TRACE,               /*4*/
    DSMON_CLI_MAX_TRACE                  /*5*/
};

PUBLIC INT4 cli_process_dsmon_cmd (tCliHandle , UINT4 , ...);
PUBLIC INT4 DsmonCliSetModuleStatus (tCliHandle , INT4);
PUBLIC INT4 DsmonCliSetTrace (tCliHandle , INT4 );
PUBLIC INT4 DsmonCliResetTrace (tCliHandle);
PUBLIC VOID DsmonCliShowTrace (tCliHandle);
PUBLIC VOID DsmonShowRunningConfig (tCliHandle); 
 

#define DSMON_ENABLE               1
#define DSMON_DISABLE              2
#define DSMON_CLI_TRACE_FUNC_ENTRY 0x00000001
#define DSMON_CLI_TRACE_FUNC_EXIT  0x00000002
#define DSMON_CLI_TRACE_CRITICAL   0x00000004
#define DSMON_CLI_TRACE_MEM_FAIL   0x00000008
#define DSMON_CLI_TRACE_DEBUG      0x00000010
#define DSMON_CLI_TRACE_ALL        0x0000001F


/* Error Strings */
enum{
        CLI_DS_RMON2_NOT_ENABLED = 1,
     CLI_DSMON_MAX_ERR
};

#ifdef __DSMNCLI_C__ 
CONST CHR1  *DsmonCliErrString [] = {
    NULL,
    "RMON2 Not Enabled\r\n",
    "\r\n"
};
#endif


#endif /* _DSMNCLI_H_ */
