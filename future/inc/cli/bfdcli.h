/********************************************************************
 * Copyright (C) Future Software Limited,1997-98,2001
 *
 * $Id: bfdcli.h,v 1.22 2014/03/18 12:08:35 siva Exp $
 *
 * Description:This file contains prototypes 
 *******************************************************************/
/* Macro and prototypes for cli.h */
#ifndef __BFDCLI_H__
#define __BFDCLI_H__ 

INT4
cli_process_bfd_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);

INT4
BfdCliShowGlobalInfo (tCliHandle CliHandle, UINT4 u4ContextId);

INT4
BfdCliShowSessInfo (tCliHandle CliHandle, UINT1 u1Output, UINT4 u4SessIndex,
                 UINT4 u4ContextId);

INT4
BfdCliPrintGlobalInfo (tCliHandle CliHandle, UINT4 u4ContextId);

INT4
BfdCliShowStatisticsInfo (tCliHandle CliHandle,
        UINT4 u4Display, UINT4 u4Type, 
                               UINT4 u4SessIndex, UINT4 u4ContextId);

INT4
BfdCliPrintGlobalStatsInfo (tCliHandle CliHandle,
        UINT4 u4ContextId);

INT4
BfdCliShowSessDiscMapping (tCliHandle CliHandle,
        UINT4 u4Index,UINT4 u4Value,UINT4 u4ContextId);

INT4
BfdCliPrintSessDiscMapping (tCliHandle CliHandle,
        UINT4 u4ContextId, UINT4 u4SessDisc);

INT4
BfdCliChangeSessMode (tCliHandle CliHandle, UINT4 u4SessId, UINT4 u4ContextId);

INT1
BfdGetBfdSessPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

INT1 BfdGetBfdCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

INT1 BfdGetVcmBfdCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

INT4
BfdCliGetCxtId (UINT1 *pau1VrfName, UINT4 *pu4ContextId);

INT4
BfdCliGetOldTraceLevel (UINT4 u4ContextId,INT4 *pu4OldDebugLevel);

INT4
BfdCliGetOldTrapLevel (UINT4 u4ContextId,UINT4 *pu4OldTraps);

INT4
BfdUtilIsBfdSessTableExist PROTO ((UINT4, UINT4));

VOID
IssBfdShowDebugging (tCliHandle CliHandle, UINT4 u4ContextId);

INT4
BfdCliGetCxtName (UINT4 u4ContextId, UINT1 *pu1Contextname);

INT4
BfdShowRunningConfig (tCliHandle CliHandle, UINT4 u4ContextId);

INT4
BfdCliShowNeighbors (tCliHandle CliHandle, UINT4 u4CxtId, UINT1 u1Client,
                     UINT1 u1AddrType, UINT1 *pu1Addr, UINT1);
INT4
BfdCliIsDirectlyConnected (UINT4 u4ContextId, UINT1 *pu1Address, INT4 i4AddrType);

#define CLI_BFD_MEG_NAME_LEN          20
#define CLI_BFD_ME_NAME_LEN           20

typedef struct
{
 UINT1 au1MegName [CLI_BFD_MEG_NAME_LEN];
 UINT1 au1MeName [CLI_BFD_ME_NAME_LEN];
}tBfdSessMegParams;

typedef struct
{
 tBfdPathType ePathType;
 union
 {
  tBfdSessNonTeParams  BfdSessLdpLspParams;
  tBfdSessTeParams     BfdSessTunnelParams;
  tBfdSessPwParams     BfdSessPwParams;
  tBfdSessMegParams    BfdSessMegParams;
 }unSessInfo;
#define SessMegInfo unSessInfo.BfdSessMegParams
#define SessPwInfo unSessInfo.BfdSessPwParams
#define SessTunnelInfo unSessInfo.BfdSessTunnelParams
#define SessLdpLspInfo unSessInfo.BfdSessLdpLspParams
}tBfdCliSessPathParams;

INT4
BfdCliGetRowPointer (UINT4 , tBfdCliSessPathParams *, UINT4 *, UINT4 *);

enum
{
          CLI_BFD_SHOW_GLOB_CONF = 1,
          CLI_BFD_SHOW_SESS_INFO,
          CLI_BFD_SHOW_SESS_STATS,
          CLI_BFD_SHOW_BFD_SESS_DISC_MAPPING,
          CLI_BFD_SHOW_BFD_NEIGHBORS
};


#define CLI_BFD_MAX_ARGS   15


/* Header file for bfdcmd.def file */
#define CLI_BFD_SESSION_CREATE CREATE_AND_WAIT
#define CLI_BFD_SESSION_DELETE DESTROY
#define CLI_BFD_SESSION_ACTIVE ACTIVE
#define CLI_BFD_SESSION_NOT_IN_SERVICE NOT_IN_SERVICE



#define CLI_BFD_GET_MODE_ID() CliGetModeInfo()
#define CLI_BFD_GET_CXT_ID()  CliGetContextIdInfo ()

#define CLI_BFD_CONVERT_SECONDS 1000

#define CLI_BFD_DETECT_MULT 3

#define CLI_BFD_SESS_MODE "bfdsess"

#define CLI_BFD_MAX_OID               20

#define CLI_BFD_IPV4_ADDR_LEN    4
#define CLI_BFD_IPV6_ADDR_LEN    16
#define CLI_BFD_MAX_IP_ADDR_LEN 16

#define CLI_BFD_ADMIN_START   2
#define CLI_BFD_ADMIN_STOP    1


#define CLI_BFD_CONTROL_PLANE_FLAG_ENABLE          1
#define CLI_BFD_CONTROL_PLANE_FLAG_DISABLE         2

#define CLI_BFD_MULTI_POINT_FLAG_ENABLE            1
#define CLI_BFD_MULTI_POINT_FLAG_DISABLE           2

#define CLI_BFD_OFFLOAD_FLAG_ENABLE                1
#define CLI_BFD_OFFLOAD_FLAG_DISABLE               2

#define CLI_BFD_TIMER_NEGO_FLAG_ENABLE             1
#define CLI_BFD_TIMER_NEGO_FLAG_DISABLE            2

#define CLI_BFD_AUTH_PRES_FLAG_ENABLE              1
#define CLI_BFD_AUTH_PRES_FLAG_DISABLE             2

#define BFD_NO_TRAP                         0x0000
#define BFD_SESS_UP_DOWN_TRAP               0x0001
#define BFD_BTSTRAP_FAIL_TRAP               0x0002
#define BFD_NEG_TX_CHANGE_TRAP              0x0004
#define BFD_ADMIN_CTRL_ERR_TRAP             0x0008
#define BFD_ALL_TRAP                        0x001F    

#define CLI_BFD_NOTIFICATION_TRUE                1
#define CLI_BFD_NOTIFICATION_FALSE               2             

#define CLI_BFD_NO_DEBUG                      0x00000000
#define CLI_BFD_DEBUG_ALL                     0x00003FFF 
#define CLI_BFD_DEBUG_INIT_SHUT               0x00000001
#define CLI_BFD_DEBUG_MGMT                    0x00000002
#define CLI_BFD_DEBUG_DATA_PATH               0x00000004
#define CLI_BFD_DEBUG_ALL_CTRL                0x00000008
#define CLI_BFD_DEBUG_ALL_PKT_DUMP            0x00000010
#define CLI_BFD_DEBUG_ALL_RESOURCE            0x00000020
#define CLI_BFD_DEBUG_ALL_FAIL                0x00000040
#define CLI_BFD_DEBUG_BUF                     0x00000080 
#define CLI_BFD_DEBUG_SESS_ESTB               0x00000100
#define CLI_BFD_DEBUG_SESS_DOWN               0x00000200  
#define CLI_BFD_DEBUG_POLL_SEQ                0x00000400
#define CLI_BFD_DEBUG_CRITICAL                0x00000800
#define CLI_BFD_DEBUG_REDUNDANCY              0x00001000
#define CLI_BFD_DEBUG_OFFLOAD                 0x00002000


#define CLI_BFD_SHOW_DETAILS          1
#define CLI_BFD_SHOW_SUMMARY          2 


#define CLI_BFD_GLOBAL_STATS          1
#define CLI_BFD_SESSION_STATS         2 

#define CLI_BFD_SHOW_DEFECT_STATS     1
#define CLI_BFD_SHOW_PKT_STATS        2
#define CLI_BFD_SHOW_GENERIC_STATS    3
#define CLI_BFD_SHOW_ALL_SESS_STATS   4 

#define CLI_BFD_SHOW_ALL              1
#define CLI_BFD_SHOW_SPECIFIC_MAP     2


INT4
BfdPortGetCxtId (UINT1 *pau1VrfName, UINT4 *pu4ContextId);

enum
{
 CLI_BFD_FSMISTDBFDGLOBALCONFIGTABLE,
 CLI_BFD_FSMISTDBFDSESSTABLE,
};

#define BFD_CLI_MAX_ARGS  50 

INT4 cli_process_Bfd_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);

#ifdef BFD_TEST_WANTED
#define CLI_BFD_UT 1
#endif

#ifdef BFD_TEST_WANTED
INT4 cli_process_bfd_test_cmd (tCliHandle CliHandle, UINT4 u4Command,...);
#endif


#ifdef BFD_HA_TEST_WANTED
enum
{
 CLI_BFD_HA_TEST_ABORT_BLK_UPDT = 1000,
 CLI_BFD_HA_TEST_NO_ABORT_BLK_UPDT,
 CLI_BFD_HA_TEST_SESS_SUMMARY,
 CLI_BFD_HA_TEST_SESS_TMR
};
#endif


#endif   /* __BFDCLI_H__ */

/*-----------------------------------------------------------------------*/
/*                       End of the file  bfdcli.h                       */
/*-----------------------------------------------------------------------*/
