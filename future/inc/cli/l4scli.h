#ifndef __L4SCLI_H__
#define __L4SCLI_H__

#include "cli.h"
#include "fssnmp.h"

#define ISS_PROT_TCP 6
#define ISS_PROT_UDP 17

#define CLI_L4S_MAX_ARGS 16


enum {
	CLI_L4S_ADD =1,
	CLI_L4S_DELETE,
	CLI_L4S_SHOW
};



/* Error codes  *
 * Error code values and strings are maintained inside the protocol module itself.
 * */
enum {
        CLI_L4S_ERR = 0,
        CLI_L4S_FILTER_NO_CHANGE,
        CLI_L4S_MAX_ERR
};


INT4 cli_process_l4s_command PROTO ((tCliHandle CliHandle, UINT4 u4Comamnd, ...));


INT4  AclCreateIPFilter PROTO ((tCliHandle, UINT4, INT4 ));

INT4 L4SCreateFilter PROTO ((tCliHandle, INT4, UINT4, UINT4, UINT4 ));

INT4 L4SDeleteFilter PROTO ((tCliHandle CliHandle, INT4));

INT4 L4SShowFilter PROTO ((tCliHandle CliHandle, INT4));

#endif



