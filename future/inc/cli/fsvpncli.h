/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsvpncli.h,v 1.1 2014/06/29 11:04:42 siva Exp $
 *
 * Description: Header file for nmhSet update trigger
 *
 **********************************************************************/



/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsVpnGlobalStatus [10];
extern UINT4 FsIkeTraceOption [10];
extern UINT4 FsVpnIkeDstPortRange [12];
extern UINT4 FsVpnSecurityProtocol [12];
extern UINT4 FsVpnRaServer [10];
extern UINT4 FsVpnDummyPktGen [10];
extern UINT4 FsVpnDummyPktParam [10];
extern UINT4 FsVpnPolicyType [12];
extern UINT4 FsVpnPolicyPriority [12];
extern UINT4 FsVpnTunTermAddrType [12]; 
extern UINT4 FsVpnLocalTunTermAddr [12];
extern UINT4 FsVpnRemoteTunTermAddr [12];
extern UINT4 FsVpnCertAlgoType [12];
extern UINT4 FsVpnProtectNetworkType [12];
extern UINT4 FsVpnLocalProtectNetwork [12];
extern UINT4 FsVpnLocalProtectNetworkPrefixLen [12];
extern UINT4 FsVpnRemoteProtectNetwork [12];
extern UINT4 FsVpnIkeSrcPortRange [12 ];
extern UINT4 FsVpnRemoteProtectNetworkPrefixLen [12];
extern UINT4 FsVpnInboundSpi[12];
extern UINT4 FsVpnOutboundSpi [12];
extern UINT4 FsVpnMode [12];
extern UINT4 FsVpnAuthAlgo [12];
extern UINT4 FsVpnAhKey [12];
extern UINT4 FsVpnEncrAlgo [12];
extern UINT4 FsVpnEspKey [12];
extern UINT4 FsVpnAntiReplay [12];
extern UINT4 FsVpnPolicyFlag [12];
extern UINT4 FsVpnProtocol [12];
extern UINT4 FsVpnPolicyIntfIndex [12];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsVpnIkePhase1HashAlgo [12];
extern UINT4 FsVpnIkePhase1EncryptionAlgo [12];
extern UINT4 FsVpnIkePhase1DHGroup [12];
extern UINT4 FsVpnIkePhase1LocalIdType [12];
extern UINT4 FsVpnIkePhase1LocalIdValue [12];
extern UINT4 FsVpnIkePhase1PeerIdType [12];
extern UINT4 FsVpnIkePhase1PeerIdValue [12];
extern UINT4 FsVpnIkePhase1LifeTimeType [12];
extern UINT4 FsVpnIkePhase1LifeTime [12];
extern UINT4 FsVpnIkePhase1Mode [12];
extern UINT4 FsVpnIkePhase2AuthAlgo [12];
extern UINT4 FsVpnIkePhase2EspEncryptionAlgo [12];
extern UINT4 FsVpnIkePhase2LifeTimeType [12];
extern UINT4 FsVpnIkePhase2LifeTime [12];
extern UINT4 FsVpnIkePhase2DHGroup [12];
extern UINT4 FsVpnIkeVersion [12];
extern UINT4 FsIpsecTraceOption[10];
extern UINT4 FsVpnPolicyRowStatus [12];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsVpnRaUserName [12];
extern UINT4 FsVpnRaUserSecret [12];
extern UINT4 FsVpnRaUserRowStatus [12];
extern UINT4 FsVpnRaAddressPoolName [12];
extern UINT4 FsVpnRaAddressPoolAddrType [12];
extern UINT4 FsVpnRaAddressPoolStart [12];
extern UINT4 FsVpnRaAddressPoolEnd [12];
extern UINT4 FsVpnRaAddressPoolPrefixLen [12];
extern UINT4 FsVpnRaAddressPoolRowStatus [12];
extern UINT4 FsVpnRemoteIdType [12];
extern UINT4 FsVpnRemoteIdValue [12];
extern UINT4 FsVpnRemoteIdKey [12];
extern UINT4 FsVpnRemoteIdAuthType [12];
extern UINT4 FsVpnRemoteIdStatus [12];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsVpnCertKeyString [12];
extern UINT4 FsVpnCertKeyType [12];
extern UINT4 FsVpnCertKeyFileName [12];
extern UINT4 FsVpnCertFileName [12];
extern UINT4 FsVpnCertEncodeType [12];
extern UINT4 FsVpnCertStatus [12];
extern UINT4 FsVpnCaCertKeyString [12];
extern UINT4 FsVpnCaCertFileName [12];
extern UINT4 FsVpnCaCertEncodeType [12];
extern UINT4 FsVpnCaCertStatus [12];



