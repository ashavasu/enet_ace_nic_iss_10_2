/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fs6374cli.h,v 1.2 2017/07/25 12:00:35 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fs6374ContextId[13];
extern UINT4 Fs6374ServiceName[13];
extern UINT4 Fs6374MplsPathType[13];
extern UINT4 Fs6374FwdTnlIdOrPwId[13];
extern UINT4 Fs6374RevTnlId[13];
extern UINT4 Fs6374SrcIpAddr[13];
extern UINT4 Fs6374DestIpAddr[13];
extern UINT4 Fs6374EncapType[13];
extern UINT4 Fs6374TrafficClass[13];
extern UINT4 Fs6374RowStatus[13];
extern UINT4 Fs6374DyadicMeasurement[13];
extern UINT4 Fs6374TSFNegotiation[13];
extern UINT4 Fs6374DyadicProactiveRole[13];
extern UINT4 Fs6374QueryTransmitRetryCount[13];
extern UINT4 Fs6374SessionIntervalQueryStatus[13];
extern UINT4 Fs6374MinReceptionIntervalInMilliseconds[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFs6374MplsPathType(u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374MplsPathType) \
 nmhSetCmnWithLock(Fs6374MplsPathType, 13, Fs6374MplsPathTypeSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %i", u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374MplsPathType)
#define nmhSetFs6374FwdTnlIdOrPwId(u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374FwdTnlIdOrPwId) \
 nmhSetCmnWithLock(Fs6374FwdTnlIdOrPwId, 13, Fs6374FwdTnlIdOrPwIdSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %u", u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374FwdTnlIdOrPwId)
#define nmhSetFs6374RevTnlId(u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374RevTnlId) \
 nmhSetCmnWithLock(Fs6374RevTnlId, 13, Fs6374RevTnlIdSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %u", u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374RevTnlId)
#define nmhSetFs6374SrcIpAddr(u4Fs6374ContextId , pFs6374ServiceName ,pSetValFs6374SrcIpAddr) \
 nmhSetCmnWithLock(Fs6374SrcIpAddr, 13, Fs6374SrcIpAddrSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %s", u4Fs6374ContextId , pFs6374ServiceName ,pSetValFs6374SrcIpAddr)
#define nmhSetFs6374DestIpAddr(u4Fs6374ContextId , pFs6374ServiceName ,pSetValFs6374DestIpAddr) \
 nmhSetCmnWithLock(Fs6374DestIpAddr, 13, Fs6374DestIpAddrSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %s", u4Fs6374ContextId , pFs6374ServiceName ,pSetValFs6374DestIpAddr)
#define nmhSetFs6374EncapType(u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374EncapType) \
 nmhSetCmnWithLock(Fs6374EncapType, 13, Fs6374EncapTypeSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %i", u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374EncapType)
#define nmhSetFs6374TrafficClass(u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374TrafficClass) \
 nmhSetCmnWithLock(Fs6374TrafficClass, 13, Fs6374TrafficClassSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %i", u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374TrafficClass)
#define nmhSetFs6374RowStatus(u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374RowStatus) \
 nmhSetCmnWithLock(Fs6374RowStatus, 13, Fs6374RowStatusSet, R6374TaskLock, R6374TaskUnLock, 0, 1, 2, "%u %s %i", u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374RowStatus)
#define nmhSetFs6374DyadicMeasurement(u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374DyadicMeasurement) \
 nmhSetCmnWithLock(Fs6374DyadicMeasurement, 13, Fs6374DyadicMeasurementSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %i", u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374DyadicMeasurement)
#define nmhSetFs6374TSFNegotiation(u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374TSFNegotiation) \
 nmhSetCmnWithLock(Fs6374TSFNegotiation, 13, Fs6374TSFNegotiationSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %i", u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374TSFNegotiation)
#define nmhSetFs6374DyadicProactiveRole(u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374DyadicProactiveRole) \
 nmhSetCmnWithLock(Fs6374DyadicProactiveRole, 13, Fs6374DyadicProactiveRoleSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %i", u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374DyadicProactiveRole)
#define nmhSetFs6374QueryTransmitRetryCount(u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374QueryTransmitRetryCount) \
 nmhSetCmnWithLock(Fs6374QueryTransmitRetryCount, 13, Fs6374QueryTransmitRetryCount, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %i", u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374QueryTransmitRetryCount)

#define nmhSetFs6374SessionIntervalQueryStatus(u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374SessionIntervalQueryStatus) \
 nmhSetCmnWithLock(Fs6374SessionIntervalQueryStatus, 13, Fs6374SessionIntervalQueryStatus, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %i", u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374SessionIntervalQueryStatus)

#define nmhSetFs6374MinReceptionIntervalInMilliseconds(u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374MinReceptionIntervalInMilliseconds) \
 nmhSetCmnWithLock(Fs6374MinReceptionIntervalInMilliseconds, 13, Fs6374MinReceptionIntervalInMilliseconds, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %i", u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374MinReceptionIntervalInMilliseconds)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fs6374LMType[13];
extern UINT4 Fs6374LMMethod[13];
extern UINT4 Fs6374LMMode[13];
extern UINT4 Fs6374LMNoOfMessages[13];
extern UINT4 Fs6374LMTimeIntervalInMilliseconds[13];
extern UINT4 Fs6374LMTimeStampFormat[13];
extern UINT4 Fs6374LMTransmitStatus[13];
extern UINT4 Fs6374DMType[13];
extern UINT4 Fs6374DMMode[13];
extern UINT4 Fs6374DMTimeIntervalInMilliseconds[13];
extern UINT4 Fs6374DMNoOfMessages[13];
extern UINT4 Fs6374DMTimeStampFormat[13];
extern UINT4 Fs6374DMTransmitStatus[13];
extern UINT4 Fs6374CmbLMDMType[13];
extern UINT4 Fs6374CmbLMDMMethod[13];
extern UINT4 Fs6374CmbLMDMMode[13];
extern UINT4 Fs6374CmbLMDMNoOfMessages[13];
extern UINT4 Fs6374CmbLMDMTimeIntervalInMilliseconds[13];
extern UINT4 Fs6374CmbLMDMTimeStampFormat[13];
extern UINT4 Fs6374CmbLMDMTransmitStatus[13];
extern UINT4 Fs6374DMPaddingSize[13];
extern UINT4 Fs6374CmbLMDMPaddingSize[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFs6374LMType(u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374LMType) \
 nmhSetCmnWithLock(Fs6374LMType, 13, Fs6374LMTypeSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %i", u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374LMType)
#define nmhSetFs6374LMMethod(u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374LMMethod) \
 nmhSetCmnWithLock(Fs6374LMMethod, 13, Fs6374LMMethodSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %i", u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374LMMethod)
#define nmhSetFs6374LMMode(u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374LMMode) \
 nmhSetCmnWithLock(Fs6374LMMode, 13, Fs6374LMModeSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %i", u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374LMMode)
#define nmhSetFs6374LMNoOfMessages(u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374LMNoOfMessages) \
 nmhSetCmnWithLock(Fs6374LMNoOfMessages, 13, Fs6374LMNoOfMessagesSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %u", u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374LMNoOfMessages)
#define nmhSetFs6374LMTimeIntervalInMilliseconds(u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374LMTimeIntervalInMilliseconds) \
 nmhSetCmnWithLock(Fs6374LMTimeIntervalInMilliseconds, 13, Fs6374LMTimeIntervalInMillisecondsSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %u", u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374LMTimeIntervalInMilliseconds)
#define nmhSetFs6374LMTimeStampFormat(u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374LMTimeStampFormat) \
 nmhSetCmnWithLock(Fs6374LMTimeStampFormat, 13, Fs6374LMTimeStampFormatSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %i", u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374LMTimeStampFormat)
#define nmhSetFs6374LMTransmitStatus(u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374LMTransmitStatus) \
 nmhSetCmnWithLock(Fs6374LMTransmitStatus, 13, Fs6374LMTransmitStatusSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %i", u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374LMTransmitStatus)
#define nmhSetFs6374DMType(u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374DMType) \
 nmhSetCmnWithLock(Fs6374DMType, 13, Fs6374DMTypeSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %i", u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374DMType)
#define nmhSetFs6374DMMode(u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374DMMode) \
 nmhSetCmnWithLock(Fs6374DMMode, 13, Fs6374DMModeSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %i", u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374DMMode)
#define nmhSetFs6374DMTimeIntervalInMilliseconds(u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374DMTimeIntervalInMilliseconds) \
 nmhSetCmnWithLock(Fs6374DMTimeIntervalInMilliseconds, 13, Fs6374DMTimeIntervalInMillisecondsSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %u", u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374DMTimeIntervalInMilliseconds)
#define nmhSetFs6374DMNoOfMessages(u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374DMNoOfMessages) \
 nmhSetCmnWithLock(Fs6374DMNoOfMessages, 13, Fs6374DMNoOfMessagesSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %u", u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374DMNoOfMessages)
#define nmhSetFs6374DMTimeStampFormat(u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374DMTimeStampFormat) \
 nmhSetCmnWithLock(Fs6374DMTimeStampFormat, 13, Fs6374DMTimeStampFormatSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %i", u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374DMTimeStampFormat)
#define nmhSetFs6374DMTransmitStatus(u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374DMTransmitStatus) \
 nmhSetCmnWithLock(Fs6374DMTransmitStatus, 13, Fs6374DMTransmitStatusSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %i", u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374DMTransmitStatus)
#define nmhSetFs6374CmbLMDMType(u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374CmbLMDMType) \
 nmhSetCmnWithLock(Fs6374CmbLMDMType, 13, Fs6374CmbLMDMTypeSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %i", u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374CmbLMDMType)
#define nmhSetFs6374CmbLMDMMethod(u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374CmbLMDMMethod) \
 nmhSetCmnWithLock(Fs6374CmbLMDMMethod, 13, Fs6374CmbLMDMMethodSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %i", u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374CmbLMDMMethod)
#define nmhSetFs6374CmbLMDMMode(u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374CmbLMDMMode) \
 nmhSetCmnWithLock(Fs6374CmbLMDMMode, 13, Fs6374CmbLMDMModeSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %i", u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374CmbLMDMMode)
#define nmhSetFs6374CmbLMDMNoOfMessages(u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374CmbLMDMNoOfMessages) \
 nmhSetCmnWithLock(Fs6374CmbLMDMNoOfMessages, 13, Fs6374CmbLMDMNoOfMessagesSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %u", u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374CmbLMDMNoOfMessages)
#define nmhSetFs6374CmbLMDMTimeIntervalInMilliseconds(u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374CmbLMDMTimeIntervalInMilliseconds) \
 nmhSetCmnWithLock(Fs6374CmbLMDMTimeIntervalInMilliseconds, 13, Fs6374CmbLMDMTimeIntervalInMillisecondsSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %u", u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374CmbLMDMTimeIntervalInMilliseconds)
#define nmhSetFs6374CmbLMDMTimeStampFormat(u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374CmbLMDMTimeStampFormat) \
 nmhSetCmnWithLock(Fs6374CmbLMDMTimeStampFormat, 13, Fs6374CmbLMDMTimeStampFormatSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %i", u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374CmbLMDMTimeStampFormat)
#define nmhSetFs6374CmbLMDMTransmitStatus(u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374CmbLMDMTransmitStatus) \
 nmhSetCmnWithLock(Fs6374CmbLMDMTransmitStatus, 13, Fs6374CmbLMDMTransmitStatusSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %i", u4Fs6374ContextId , pFs6374ServiceName ,i4SetValFs6374CmbLMDMTransmitStatus)
#define nmhSetFs6374DMPaddingSize(u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374DMPaddingSize) \
 nmhSetCmnWithLock(Fs6374DMPaddingSize, 13, Fs6374DMPaddingSizeSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %u", u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374DMPaddingSize)
#define nmhSetFs6374CmbLMDMPaddingSize(u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374CmbLMDMPaddingSize) \
 nmhSetCmnWithLock(Fs6374CmbLMDMPaddingSize, 13, Fs6374CmbLMDMPaddingSizeSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %u", u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374CmbLMDMPaddingSize)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fs6374StatsLmmOut[13];
extern UINT4 Fs6374StatsLmmIn[13];
extern UINT4 Fs6374StatsLmrOut[13];
extern UINT4 Fs6374StatsLmrIn[13];
extern UINT4 Fs6374Stats1LmOut[13];
extern UINT4 Fs6374Stats1LmIn[13];
extern UINT4 Fs6374Stats1DmOut[13];
extern UINT4 Fs6374Stats1DmIn[13];
extern UINT4 Fs6374StatsDmmOut[13];
extern UINT4 Fs6374StatsDmmIn[13];
extern UINT4 Fs6374StatsDmrOut[13];
extern UINT4 Fs6374StatsDmrIn[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFs6374StatsLmmOut(u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374StatsLmmOut) \
 nmhSetCmnWithLock(Fs6374StatsLmmOut, 13, Fs6374StatsLmmOutSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %u", u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374StatsLmmOut)
#define nmhSetFs6374StatsLmmIn(u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374StatsLmmIn) \
 nmhSetCmnWithLock(Fs6374StatsLmmIn, 13, Fs6374StatsLmmInSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %u", u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374StatsLmmIn)
#define nmhSetFs6374StatsLmrOut(u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374StatsLmrOut) \
 nmhSetCmnWithLock(Fs6374StatsLmrOut, 13, Fs6374StatsLmrOutSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %u", u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374StatsLmrOut)
#define nmhSetFs6374StatsLmrIn(u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374StatsLmrIn) \
 nmhSetCmnWithLock(Fs6374StatsLmrIn, 13, Fs6374StatsLmrInSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %u", u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374StatsLmrIn)
#define nmhSetFs6374Stats1LmOut(u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374Stats1LmOut) \
 nmhSetCmnWithLock(Fs6374Stats1LmOut, 13, Fs6374Stats1LmOutSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %u", u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374Stats1LmOut)
#define nmhSetFs6374Stats1LmIn(u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374Stats1LmIn) \
 nmhSetCmnWithLock(Fs6374Stats1LmIn, 13, Fs6374Stats1LmInSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %u", u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374Stats1LmIn)
#define nmhSetFs6374Stats1DmOut(u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374Stats1DmOut) \
 nmhSetCmnWithLock(Fs6374Stats1DmOut, 13, Fs6374Stats1DmOutSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %u", u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374Stats1DmOut)
#define nmhSetFs6374Stats1DmIn(u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374Stats1DmIn) \
 nmhSetCmnWithLock(Fs6374Stats1DmIn, 13, Fs6374Stats1DmInSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %u", u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374Stats1DmIn)
#define nmhSetFs6374StatsDmmOut(u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374StatsDmmOut) \
 nmhSetCmnWithLock(Fs6374StatsDmmOut, 13, Fs6374StatsDmmOutSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %u", u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374StatsDmmOut)
#define nmhSetFs6374StatsDmmIn(u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374StatsDmmIn) \
 nmhSetCmnWithLock(Fs6374StatsDmmIn, 13, Fs6374StatsDmmInSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %u", u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374StatsDmmIn)
#define nmhSetFs6374StatsDmrOut(u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374StatsDmrOut) \
 nmhSetCmnWithLock(Fs6374StatsDmrOut, 13, Fs6374StatsDmrOutSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %u", u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374StatsDmrOut)
#define nmhSetFs6374StatsDmrIn(u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374StatsDmrIn) \
 nmhSetCmnWithLock(Fs6374StatsDmrIn, 13, Fs6374StatsDmrInSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 2, "%u %s %u", u4Fs6374ContextId , pFs6374ServiceName ,u4SetValFs6374StatsDmrIn)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fs6374SystemControl[13];
extern UINT4 Fs6374ModuleStatus[13];
extern UINT4 Fs6374DMBufferClear[13];
extern UINT4 Fs6374LMBufferClear[13];
extern UINT4 Fs6374DebugLevel[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFs6374SystemControl(u4Fs6374ContextId ,i4SetValFs6374SystemControl) \
 nmhSetCmnWithLock(Fs6374SystemControl, 13, Fs6374SystemControlSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 1, "%u %i", u4Fs6374ContextId ,i4SetValFs6374SystemControl)
#define nmhSetFs6374ModuleStatus(u4Fs6374ContextId ,i4SetValFs6374ModuleStatus) \
 nmhSetCmnWithLock(Fs6374ModuleStatus, 13, Fs6374ModuleStatusSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 1, "%u %i", u4Fs6374ContextId ,i4SetValFs6374ModuleStatus)
#define nmhSetFs6374DMBufferClear(u4Fs6374ContextId ,i4SetValFs6374DMBufferClear) \
 nmhSetCmnWithLock(Fs6374DMBufferClear, 13, Fs6374DMBufferClearSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 1, "%u %i", u4Fs6374ContextId ,i4SetValFs6374DMBufferClear)
#define nmhSetFs6374LMBufferClear(u4Fs6374ContextId ,i4SetValFs6374LMBufferClear) \
 nmhSetCmnWithLock(Fs6374LMBufferClear, 13, Fs6374LMBufferClearSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 1, "%u %i", u4Fs6374ContextId ,i4SetValFs6374LMBufferClear)
#define nmhSetFs6374DebugLevel(u4Fs6374ContextId ,u4SetValFs6374DebugLevel) \
 nmhSetCmnWithLock(Fs6374DebugLevel, 13, Fs6374DebugLevelSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 1, "%u %u", u4Fs6374ContextId ,u4SetValFs6374DebugLevel)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fs6374LMDMTrapControl[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFs6374LMDMTrapControl(u4Fs6374ContextId ,pSetValFs6374LMDMTrapControl) \
 nmhSetCmnWithLock(Fs6374LMDMTrapControl, 13, Fs6374LMDMTrapControlSet, R6374TaskLock, R6374TaskUnLock, 0, 0, 1, "%u %s", u4Fs6374ContextId ,pSetValFs6374LMDMTrapControl)

#endif
