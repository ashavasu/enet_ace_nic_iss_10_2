
/* INCLUDE FILE HEADER :
 *
 * $Id: usercli.h,v 1.9 2015/06/27 11:40:19 siva Exp $
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : usercli.h                                        |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR      :                                                  |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : WSSUSER                                          |
 * |                                                                           |
 * |  MODULE NAME           : CLI interface for WSSUSER configuration          |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    :                                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION           : This file contains command idenfiers for         |
 * |                          definitions in usercmd.def, function prototypes  |
 * |                          for WSSUSER CLI and corresponding error code     |
 * |                          definitions                                      |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 *
 */

#ifndef USERCLI_H
#define USERCLI_H


#include "cli.h"
/* WSSUSER CLI command constants. To add a new command ,a new definition needs to
 * be added to the list.
 */
enum
{
 CLI_WSSUSER_GROUP_CREATE =1,
 CLI_WSSUSER_GROUP_DELETE,
 CLI_WSSUSER_GROUP_NAME,
 CLI_WSSUSER_GROUP_BANDWIDTH,
 CLI_WSSUSER_GROUP_DLBANDWIDTH,
 CLI_WSSUSER_GROUP_ULBANDWIDTH,
 CLI_WSSUSER_GROUP_TIME,
 CLI_WSSUSER_GROUP_VOLUME,
 CLI_WSSUSER_ROLE_ADD,
 CLI_WSSUSER_ROLE_DELETE,
 CLI_WSSUSER_NAME_RESTRICT,
 CLI_WSSUSER_NAME_NO_RESTRICT,
 CLI_WSSUSER_MAC_RESTRICT,
 CLI_WSSUSER_MAC_NO_RESTRICT,
 CLI_WSSUSER_NAME_MAC_MAP_ADD,
 CLI_WSSUSER_NAME_MAC_MAP_DELETE,
 CLI_WSSUSER_ENABLED,
 CLI_WSSUSER_DISABLED,
 CLI_WSSUSER_SHOW_USER_GROUP,
 CLI_WSSUSER_SHOW_USER_ID,
 CLI_WSSUSER_SHOW_USER_SESSION,
 CLI_WSSUSER_SHOW_MAC_MAP,
 CLI_WSSUSER_SHOW_RESTRICTED_USER,
 CLI_WSSUSER_SHOW_RESTRICTED_STATION,
 CLI_WSSUSER_SHOW_SUMMARY,
 CLI_WSSUSER_SHOW_MODULE_STATUS,
 CLI_WSSUSER_CLEAR_COUNTERS,
 CLI_WSSUSER_DEBUG,
 CLI_WSSUSER_NO_DEBUG,
 CLI_WSSUSER_TRAP_ENABLE,
 CLI_WSSUSER_TRAP_DISABLE,
 CLI_WSSUSER_GROUP_MAX_CMDS
};
#define WSSUSER_CLI_FIRST_ARG               1
#define WSSUSER_CLI_SECOND_ARG              2
#define WSSUSER_CLI_THIRD_ARG               3
#define CLI_WSSUSER_GROUP_MODE              "user_group-"
#define WSSUSER_INIT_SHUT_TRC               INIT_SHUT_TRC
#define WSSUSER_MGMT_TRC                    MGMT_TRC
#define WSSUSER_DATA_PATH_TRC               DATA_PATH_TRC
#define WSSUSER_CONTROL_PATH_TRC            CONTROL_PLANE_TRC
#define WSSUSER_DUMP_TRC                    DUMP_TRC
#define WSSUSER_OS_RESOURCE_TRC             OS_RESOURCE_TRC
#define WSSUSER_ALL_FAILURE_TRC             ALL_FAILURE_TRC
#define WSSUSER_BUFFER_TRC                  BUFFER_TRC

/* Error codes 
 * Error code values and strings are maintained inside the protocol module itself.
 */
enum 
{
 CLI_WSSUSER_NO_STATION = 1,
 CLI_WSSUSER_NO_GROUP_NAME,
 CLI_WSSUSER_GROUP_NOT_CREATED,
 CLI_WSSUSER_GROUP_ALREADY_CREATED,
 CLI_WSSUSER_INVALID_GROUP_ID,
 CLI_WSSUSER_INVALID_GROUP_NAME,
 CLI_WSSUSER_GROUPNAME_LEN_EXCEEDS,
 CLI_WSSUSER_INVALID_BANDWIDTH,
 CLI_WSSUSER_INVALID_VOLUME,
 CLI_WSSUSER_INVALID_TIME,
 CLI_WSSUSER_GROUP_CANNOT_DELETE,
 CLI_WSSUSER_GROUP_CANNOT_DELETE_DEFAULT,
 CLI_WSSUSER_GROUP_MEM_ALLOCATE_FAIL,
 CLI_WSSUSER_USER_PRESENT,
 CLI_WSSUSER_ROLE_NOT_CREATED,
 CLI_WSSUSER_SESSION_NOT_CREATED,
 CLI_WSSUSER_ROLE_ALREADY_CREATED,
 CLI_WSSUSER_INVALID_ROLE_INDEX,
 CLI_WSSUSER_WLAN_NOT_CREATED,
 CLI_WSSUSER_ROLE_CANNOT_DELETE,
 CLI_WSSUSER_INVALID_USER_NAME,
 CLI_WSSUSER_INVALID_MAC,
 CLI_WSSUSER_NO_USER_NAME,
 CLI_WSSUSER_NO_MAC_ADDR,
 CLI_WSSUSER_MAPPING_ALREADY_CREATED,
 CLI_WSSUSER_MAPPING_NOT_CREATED,
 CLI_WSSUSER_MAPPING_CANNOT_DELETE,
 CLI_WSSUSER_INVALID_MAPPING,
 CLI_WSSUSER_GROUP_MAX_LIMIT,
 CLI_WSSUSER_ROLE_MAX_LIMIT,
 CLI_WSSUSER_RESTRICTED_USER_MAX_LIMIT,
 CLI_WSSUSER_RESTRICTED_MAC_MAX_LIMIT,
 CLI_WSSUSER_MACMAP_MAX_LIMIT,
 CLI_WSSSUER_MAC_RESTRICTED,
 CLI_WSSUSER_USER_NAME_RESTRICTED,
 CLI_WSSUSER_NOT_ENABLED,
 CLI_WSSUSER_INVALID_TRAP_STATUS,
 CLI_WSSUSER_MAX_ERR
};
/* The error strings should be places under the switch so as to avoid redefinition
 * This will be visible only in modulecli.c file
 */
#ifdef WSSUSERCLI_C

CONST CHR1  *WssUserCliErrString [] = {
 NULL,
 "% No stations exists \r\n",
 "% No such Group Name exists \r\n",
 "% User Group Entry is not present \r\n",
 "% User Group Entry is already present\r\n",
 "% Invalid Group Id \r\n",
 "% Invalid Group Name \r\n",
 "% Length of Group Name should not exceed 32\r\n",
 "% Invalid Bandwidth Range\r\n",
 "% Invalid Volume Range\r\n",
 "% Invalid Time Range\r\n",
 "% Cannot Delete User Group \r\n",
 "% Cannot Delete Default User Group \r\n",
 "% Memory Allocation failed for User Group \r\n",
 "% Cannot delete,User is already associated with some group \r\n",
 "% User Role Entry is not present \r\n",
 "% User Session Entry is not present \r\n",
 "% User Role Entry is already present\r\n",
 "% Invalid User Name and Wlan Index\r\n",
 "% WLAN is not created \r\n",
 "% Cannot Delete User Role\r\n",
 "% Invalid User Name\r\n",
 "% Invalid Mac Address\r\n",
 "% User Name is not Present\r\n",
 "% User Mac Address is not Present\r\n",
 "% User-MAC Mapping is already present \r\n",
 "% User-MAC Mapping is not present \r\n",
 "% Cannot delete User-MAC Mapping \r\n",
 "% Invalid Mapping \r\n",
 "% Only limited number of User-Groups can be created - Exceeds the limit\r\n",
 "% Only limited number of Users can be created - Exceeds the limit\r\n",
 "% Only limited number of Users can be restricted - Exceeds the limit\r\n",
 "% Only limited number of MAC can be restricted - Exceeds the limit\r\n",
 "% Only limited number of User-MAC Mapping can be done - Exceeds the limit\r\n",
 "% User MAC address Already restricted\r\n",
 "% User Name already Restricted \r\n",
 "% User Role is disabled\r\n",
 "% Invalid Trap Status\r\n",
 "\r\n"
};

#endif /*USERCLI_C*/

/**********************/
/*extern declarations */
/**********************/
INT4 cli_process_wssuser_command (tCliHandle CliHandle,
                                      UINT4 u4Command, ...);
INT1 WssUserGetGroupCfgPrompt (INT1*,INT1*);

/**********************************/
/* Util APIs for config commands  */  
/*********************************/

/* User Group Table */
INT4 WssUserCliCreateGroup (tCliHandle , UINT4);
INT4 WssUserCliDeleteGroup (tCliHandle , UINT4);
INT4 WssUserCliSetGroupName (tCliHandle , UINT4, tSNMP_OCTET_STRING_TYPE*);
INT4 WssUserCliSetGroupBandWidth (tCliHandle , UINT4, UINT4);
INT4 WssUserCliSetGroupDLBandWidth (tCliHandle , UINT4, UINT4);
INT4 WssUserCliSetGroupULBandWidth (tCliHandle , UINT4, UINT4);
INT4 WssUserCliSetGroupVolume (tCliHandle , UINT4, UINT4);
INT4 WssUserCliSetGroupTime (tCliHandle , UINT4, UINT4);

/* User Role Table*/
INT4 WssUserCliAddRole (tCliHandle, tSNMP_OCTET_STRING_TYPE *, UINT4, UINT4);
INT4 WssUserCliDeleteRole (tCliHandle, tSNMP_OCTET_STRING_TYPE*, UINT4);

/* User Name Access List Table*/
INT4 WssUserCliRestrictUserName (tCliHandle,tSNMP_OCTET_STRING_TYPE *);
INT4 WssUserCliNoRestrictUserName (tCliHandle,tSNMP_OCTET_STRING_TYPE *);

/* User Mac Access List Table */
INT4 WssUserCliRestrictUserMac (tCliHandle,tMacAddr);
INT4 WssUserCliNoRestrictUserMac (tCliHandle,tMacAddr);

/*User Mapping Tabe */
INT4 WssUserCliAddMacMap (tCliHandle,tMacAddr,tSNMP_OCTET_STRING_TYPE *);
INT4 WssUserCliDelMacMap (tCliHandle,tMacAddr,tSNMP_OCTET_STRING_TYPE *);

/*User Role Status */
INT4 WssUserCliRoleStatus (tCliHandle, INT4);


/******************************/
/* Util APIs for Show Command */
/******************************/
INT4 WssUserCliShowGroupDetails (tCliHandle, UINT4);
INT4 WssUserCliShowGroupDetailsAll (tCliHandle);
INT4 WssUserCliShowUserDetails (tCliHandle,tSNMP_OCTET_STRING_TYPE *,UINT4);
INT4 WssUserCliShowUserDetailsAll (tCliHandle);
INT4 WssUserCliShowSessionDetails (tCliHandle,tSNMP_OCTET_STRING_TYPE *, tMacAddr);
INT4 WssUserCliShowSessionAll (tCliHandle CliHandle);
INT4 WssUserCliShowMacMapAll (tCliHandle);
INT4 WssUserCliShowRestrictedUser (tCliHandle);
INT4 WssUserCliShowRestrictedStation (tCliHandle);
INT4 WssUserCliShowUserInfo (tCliHandle,tSNMP_OCTET_STRING_TYPE *);
INT4 WssUserCliShowModuleStatus (tCliHandle);
INT4 WssUserCliClearCounters (tCliHandle);
/******************************/
/* Util APIs for Show Command */
/******************************/
INT4 WssUserShowRunningConfig (tCliHandle CliHandle);
VOID WssUserShowRunningUserGroupConfig (tCliHandle);
VOID WssUserShowRunningUserRoleConfig (tCliHandle);
VOID WssUserShowRunningUserNameAccessListConfig (tCliHandle);
VOID WssUserShowRunningUserMacAccessListConfig (tCliHandle);
VOID WssUserShowRunningUserMacMappingConfig (tCliHandle);
VOID WssUserShowRunningUserRoleModuleStatusConfig (tCliHandle);
VOID WssUserShowRunningTrapStatus (tCliHandle CliHandle);


/*******************************/
/* Util APIs for Debug Command */
/*******************************/
INT4 WssUserCliSetDebugs (tCliHandle, INT4, INT4);
#endif /*WSSUSERCLI_H*/
