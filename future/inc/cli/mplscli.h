/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: mplscli.h,v 1.104 2017/06/08 11:40:30 siva Exp $
 *
 * Description: This file contains the MPLS CLI related defines
 *              enums, prototypes & structures..
 *******************************************************************/
#ifndef __MPLSCLI_H__
#define __MPLSCLI_H__

#include "cli.h"
#include "l2vpdefs.h"
#include "mplsdbg.h"

/* ALL DEBUG LEVEL RELATED MACROS */
#define CLI_FM_ZERO 0
#define CLI_ALL_MEMORY_RELATED_DEBUG_STATEMENTS MPLSFM_DBG_MEM
#define CLI_ALL_TIMER_MESSAGES MPLSFM_DBG_TIMER
#define CLI_ALL_SEMAPHORE_RELATED_MESSAGES MPLSFM_DBG_SEM
#define CLI_ALL_PROCESS_RELATED_MESSAGES MPLSFM_DBG_PRCS
#define CLI_ALL_SNMP_RELATED_MESSAGES MPLSFM_DBG_SNMP
#define CLI_ALL_MISCELLANEOUS_MESSAGES MPLSFM_DBG_MISC
#define CLI_ALL_ENTRY_EXIT_MESSAGES MPLSFM_DBG_ETEXT
#define CLI_ALL_INTERFACE_RELATED_MESSAGES MPLSFM_DBG_IF
#define CLI_DISPLAY_MESSAGES_FROM_MAIN_MODULE MPLSFM_MAIN_DEBUG
#define CLI_DISPLAY_INTERFACE_RELATED_MESSAGES MPLSFM_IF_DEBUG
#define CLI_DISPLAY_PROCESS_RELATED_DEBUG_MESSAGES MPLSFM_PRCS_DEBUG
#define CLI_MPLSFM_DBG_ALL MPLSFM_DBG_MEM | MPLSFM_DBG_TIMER | MPLSFM_DBG_SEM | MPLSFM_DBG_PRCS | MPLSFM_DBG_SNMP | MPLSFM_DBG_MISC | MPLSFM_DBG_ETEXT | MPLSFM_DBG_IF | MPLSFM_MAIN_DEBUG | MPLSFM_IF_DEBUG | MPLSFM_PRCS_DEBUG

#ifdef MPLS_TEST_WANTED
#define CLI_MPLS_TP_UT    1
#define CLI_VCCV_UT       2
#define CLI_MPLS_P2MP_UT  3
#define CLI_VPLS_ADS_UT   4
#define CLI_TLDP_PW_GR_UT   5
#define CLI_LDP_GR_UT       6
#define CLI_L2VPN_HA_UT     7
#define CLI_LDP_HA_UT       8
#define CLI_VPLSADS_GR_UT   9
#define CLI_LDP_BFD_UT   5
#endif

#ifdef L2VPN_TEST_WANTED
#define CLI_PWRED_UT 1
#endif

/* Packet dump directions */
#define  MPLS_CLI_IN                  1
#define  MPLS_CLI_OUT                 2
#define  MPLS_CLI_ALL                 3

/* Show command o/p type */
#define  MPLS_CLI_BRIEF               1
#define  MPLS_CLI_DETAIL              2

/* Tunnel signalling protocol */
#define  MPLS_CLI_NONE                 1
#define  MPLS_CLI_RSVP                 2
#define  MPLS_CLI_CRLDP                3
#define  MPLS_CLI_OTHER                4

#define  MPLS_CLI_DEF_GR_MAX_WAIT_TMR  600


#define  MAX_CLI_TIME_STRING           9
#define  MAX_U8_STRING_SIZE           21

#define  CLI_TNL_STATUS(Status)       (Status == 1) ? "up" : "down"

/*-----------------------------------TE-MODULE-------------------------------*/

#define  TE_CLI_MAX_ARGS               10

enum 
{
    MPLS_CLI_TE_NO_ERROR = CLI_ERR_START_ID_MPLS_TE,                        
    MPLS_CLI_TE_ERR_INSEG_LABEL,
    MPLS_CLI_TE_ERR_INSEG_IF,
    MPLS_CLI_TE_ERR_INSEG_NPOP,
    MPLS_CLI_TE_ERR_INSEG_ADDR_FAMILY,
    MPLS_CLI_TE_ERR_OUTSEG_LABEL,
    MPLS_CLI_TE_ERR_OUTSEG_PUSH_TOP_LABEL,
    MPLS_CLI_TE_ERR_NEXT_HOP_ADDR_TYPE,
    MPLS_CLI_TE_ERR_NEXT_HOP_ADDR,
    MPLS_CLI_TE_ERR_OUTSEG_IF,
    MPLS_CLI_TE_ERR_OUTSEG_DIR,
    MPLS_CLI_TE_ERR_INSEG_DIR,
    MPLS_CLI_OUTSEG_ENTRY_GET, /* STATIC_HLSP */
    MPLS_CLI_TE_ERR_LBLSTK_LBL,
    MPLS_CLI_TE_ERR_LBLSTK_RS,
    MPLS_CLI_TE_ERR_LSP_MAP,
    MPLS_CLI_TE_ERR_MAP_NUM_NOT_EXIST,
    MPLS_CLI_TE_ERR_LOCAL_MAP_NUM_NOT_CONF,
    MPLS_CLI_TE_ERR_P2MP_DEST_LOCAL_MAP_NUM,
    MPLS_CLI_TE_ERR_P2MP_DEST_EXISTS,
    MPLS_CLI_TE_ERR_WRONG_SUBNET,
    MPLS_CLI_TE_ERR_LOCAL_INTF_ADDR,
    MPLS_CLI_TE_HLSP_STK_TNL,
    MPLS_CLI_TE_OAM_BFD_EXISTS,
    MPLS_CLI_TE_ELPS_EXISTS,
    MPLS_CLI_TE_ERR_NO_FREE_TUNNEL_INTERFACE,
    MPLS_CLI_TE_ERR_CANNOT_SET_AFFINITY,
    MPLS_CLI_TE_ERR_CANNOT_SET_ATTRIBUTE,
    MPLS_CLI_TE_ERR_UNABLE_TO_SET_CT_BW,
    MPLS_CLI_TE_ERR_ALL_CT_BW_SUM_EXCEED_MAX_LIMIT,
    MPLS_CLI_TE_ERR_PATH_OPT,
    MPLS_CLI_TE_ERR_HOLD_PRO,
    MPLS_CLI_TE_MAX_ERROR_MSGS
};
enum 
{
    MPLS_CLI_DIRECTION_FORWARD = 0,
    MPLS_CLI_DIRECTION_REVERSE,
    MPLS_CLI_TE_UNIDIRECTIONAL,
    MPLS_CLI_TE_COROUTED_BIDIRECTIONAL,
    MPLS_CLI_TE_ASSOCIATED_BIDIRECTIONAL,
    MPLS_CLI_TE_DESTINATION_TUNNEL,
    MPLS_CLI_TE_DESTINATION_LSP
};

/*FRR MODULE*/
#define  CLI_MPLS_FRR_BACKUP_SENDER_TEMPLATE       0x0100
#define  CLI_MPLS_FRR_BACKUP_PATH_SPECIFIC         0x0200
#define  CLI_MPLS_HOP_TYPE_STRICT                  5
#define  CLI_MPLS_HOP_TYPE_LOOSE                   6
#define  CLI_MPLS_FRR_DE_MER_ENABLE                8
#define  CLI_MPLS_FRR_DE_MER_DISABLE               9
#define  CLI_MPLS_FRR_DE_ENABLE                    10
#define  CLI_MPLS_FRR_DE_DISABLE                   11
#define  CLI_MPLS_FRR_SHOW_ONETOONE                12
#define  CLI_MPLS_FRR_SHOW_MANYTOONE               13
#define  CLI_MPLS_FRR_SHOW_ALL                     14
#define  CLI_MPLS_FRR_MAKE_AFTER_BREAK_ENABLE      15
#define  CLI_MPLS_FRR_MAKE_AFTER_BREAK_DISABLE     16
#define  CLI_MPLS_FRR_BKP_UP_SETUP_PRIO            7
#define  CLI_MPLS_FRR_BKP_UP_HOP_LIMIT             32

#define  CLI_MPLS_FRR_BW_PROT                      0x0001
#define  CLI_MPLS_FRR_NODE_PROT                    0x0002
#define  CLI_MPLS_FRR_LINK_PROT                    0x0004  
#define  CLI_MPLS_FRR_ONE_TO_ONE                   0x0008
#define  CLI_MPLS_FRR_MANY_TO_ONE                  0x0010 
#define  CLI_MPLS_FRR_PRIOR                        0x0020
#define  CLI_MPLS_FRR_HOP_LIMIT                    0x0040 
#define  CLI_MPLS_FRR_AFFINITY                     0x0080

#define   MPLS_CLI_TE_TYPE_MPLS   0x1
#define   MPLS_CLI_TE_TYPE_MPLSTP 0x2
#define   MPLS_CLI_TE_TYPE_GMPLS  0x4
#define   MPLS_CLI_TE_TYPE_HLSP   0x8
/* MPLS_P2MP_LSP_CHANGES - S */
#define   MPLS_CLI_TE_TYPE_P2MP   0x10
/* MPLS_P2MP_LSP_CHANGES - E */
#define   MPLS_CLI_TE_TYPE_SLSP   0x20

#define CLI_MPLS_TE_SESS_ATTR 1
#define CLI_MPLS_TE_TNL_TYPE  1

enum
{
    CLI_MPLS_FRR_CONST_INFO = 1,
    CLI_MPLS_FRR_NO_CONST_INFO,
    CLI_MPLS_TE_PRIORITY,
    CLI_MPLS_TE_NO_PRIORITY,
    CLI_MPLS_TE_AFFINITY,
    CLI_MPLS_TE_NO_AFFINITY,
    CLI_MPLS_FRR_BKPUP_ID_METHOD,
    CLI_MPLS_FRR_NO_BKPUP_ID_METHOD,
    CLI_MPLS_TE_RECORD_ROUTE,
    CLI_MPLS_TE_NO_RECORD_ROUTE,
    CLI_MPLS_TE_HOP_ENTRY,
    CLI_MPLS_TE_NO_HOP_ENTRY,
    CLI_MPLS_TE_GLOBAL_REVERTIVE_TIME,
    CLI_MPLS_TE_HOP_TABLE,
    CLI_MPLS_TE_NO_HOP_TABLE,
    CLI_MPLS_FRR_SHOW,
    CLI_MPLS_TE_SNMP_TRAP_ENABLE,
    CLI_MPLS_TE_SNMP_TRAP_DISABLE,
    CLI_MPLS_TE_ADMIN_STATUS_FLAGS,
    CLI_MPLS_TE_GPID,
    CLI_MPLS_TE_SWITCHING_ENCODING_TYPE,
    CLI_MPLS_TE_TNL_SRLG_TYPE,
    CLI_MPLS_TE_TNL_SRLG_VALUE,
    CLI_MPLS_TE_TNL_NO_SRLG_VALUE,
    CLI_MPLS_TE_LSP_ATTR,
    CLI_MPLS_TE_NO_LSP_ATTR,
    CLI_MPLS_TE_IP_EXPL_PATH,
    CLI_MPLS_TE_NO_IP_EXPL_PATH,
    CLI_MPLS_TE_PATH_HOP,
    CLI_MPLS_TE_NO_PATH_HOP,
    CLI_MPLS_TE_ATTR_PRIORITY,
    CLI_MPLS_TE_ATTR_NO_PRIORITY,
    CLI_MPLS_TE_ATTR_AFFINITY,
    CLI_MPLS_TE_ATTR_NO_AFFINITY,
    CLI_MPLS_TE_ATTR_BW,
    CLI_MPLS_TE_ATTR_NO_BW,
    CLI_MPLS_TE_ATTR_RECORD_ROUTE,
    CLI_MPLS_TE_ATTR_NO_RECORD_ROUTE,
    CLI_MPLS_TE_ATTR_SRLG_TYPE,
    CLI_MPLS_TE_ATTR_NO_SRLG_TYPE,
    CLI_MPLS_TE_ATTR_SRLG_VALUE,
    CLI_MPLS_TE_ATTR_NO_SRLG_VALUE,
    CLI_MPLS_TE_ATTR_CLASSTYPE_VALUE,
    CLI_MPLS_TE_ATTR_NO_CLASSTYPE_VALUE,
    CLI_MPLS_TUNNEL_CLASS_TYPE,
    CLI_MPLS_TE_END_TO_END_PROTECTION,
    CLI_MPLS_TE_PROTECTION_OPER_TYPE,
    CLI_MPLS_TE_MBB_STATUS, 
    CLI_MPLS_QOS_POLICY,
    CLI_MPLS_DELETE_QOS_POLICY,
    CLI_MPLS_DIFFSERV_LSP_TYPE,
    CLI_MPLS_CREATE_EXP_PHB_MAP,
    CLI_MPLS_DELETE_EXP_PHB_MAP,
    CLI_MPLS_CREATE_ELSP_INFO_LIST_INDEX,
    CLI_MPLS_DELETE_ELSP_INFO_LIST_INDEX,
    CLI_MPLS_PRECONFIG_EXP_PHB_INDEX,
    CLI_MPLS_UNMAP_PRECONFIG_EXP_PHB_INDEX,
    CLI_MPLS_DSTE_STATUS,
    CLI_MPLS_DSTE_CLASS_TYPE,
    CLI_MPLS_DSTE_NO_CLASS_TYPE,
    CLI_MPLS_CONFIGURE_EXP_PHB_MAP,
    CLI_MPLS_REMOVE_EXP_PHB_MAP,
    CLI_MPLS_CONFIGURE_ELSP_INFO_PHB_MAP,
    CLI_MPLS_REMOVE_ELSP_INFO_PHB_MAP,
    CLI_MPLS_DSTE_TRAFFIC_CLASS,
    CLI_MPLS_DSTE_NO_TRAFFIC_CLASS,
    CLI_MPLS_DSTE_TE_CLASS,
    CLI_MPLS_DSTE_NO_TE_CLASS,
    CLI_MPLS_DSTE_SHOW_STATUS,
    CLI_MPLS_DSTE_SHOW_CLASS,
    CLI_MPLS_DIFFSERV_ELSP_TYPE,
    CLI_MPLS_ASSOCIATE_EXP_PHB_MAP,
    CLI_MPLS_DIFFSERV_LLSP_PSC,
    CLI_MPLS_TE_SIG_PROTO,
    CLI_MPLS_TE_EXIT,
    CLI_MPLS_TE_TNL_CREATE,                
    CLI_MPLS_TE_TNL_IF_CREATE,             
    CLI_MPLS_TE_TNL_NO_IF_CREATE,
    CLI_MPLS_TE_TRAFFIC,                   
    CLI_MPLS_TE_RSRC,                      
    CLI_MPLS_TE_ST_BIND,                   
    CLI_MPLS_TE_NO_SHUT,                   
    CLI_MPLS_TE_SHUT,                      
    CLI_MPLS_TE_TYPE,                      
    CLI_MPLS_TE_MODE,                      
    CLI_MPLS_TE_DEST_TUNNEL,               
    CLI_MPLS_TE_NO_DEST_TUNNEL,            
    CLI_MPLS_TE_SUMMARY,                                    
    CLI_MPLS_TE_BRIEF,                     
    CLI_MPLS_TE_NAME,                      
    CLI_MPLS_TE_SRC_ADDR,                  
    CLI_MPLS_TE_DEST_ADDR,                 
    CLI_MPLS_TE_ROLE_ALL,                  
    CLI_MPLS_TE_ROLE_HEAD,                 
    CLI_MPLS_TE_ROLE_MIDDLE,               
    CLI_MPLS_TE_ROLE_TAIL,                 
    CLI_MPLS_TE_ROLE_REMOTE,               
    CLI_MPLS_TE_DETAIL,                    
    CLI_MPLS_TE_ALL,                       
    CLI_MPLS_TE_DBG,                       
    CLI_MPLS_TE_SRC_LOCAL_MAP_NUM,         
    CLI_MPLS_TE_DEST_LOCAL_MAP_NUM,        
    CLI_MPLS_TE_TNL_DELETE,                
    CLI_MPLS_TE_UNIDIRECTIONAL,            
    CLI_MPLS_TE_COROUTED_BIDIR,            
    CLI_MPLS_TE_ASSOCIATED_BIDIR,          
    CLI_MPLS_TE_CONFIG_HLSP,                
    CLI_MPLS_TE_CONFIG_NO_HLSP,            
    CLI_MPLS_HLSP_SHOW,                    
    CLI_MPLS_HLSP,                         
    CLI_MPLS_HLSP_STACKED_TNL,             
    CLI_MPLS_P2MP_TE_TNL_CREATE,           
    CLI_MPLS_P2MP_TE_TNL_ING_DST_ADD,      
    CLI_MPLS_P2MP_TE_TNL_BR_ST_BIND,       
    CLI_MPLS_P2MP_TE_ST_BIND_EGRESS,       
    CLI_MPLS_P2MP_TE_BUD_NODE_SET,         
    CLI_MPLS_P2MP_TE_NO_ST_BIND,           
    CLI_MPLS_TE_P2MP_BRANCH_STATS,         
    CLI_MPLS_P2MP_TE_TNL_DELETE,           
    CLI_MPLS_SLSP_SHOW,                     
    CLI_MPLS_SLSP_TNLS,                    
    CLI_MPLS_SLSP_TNL,             
    CLI_MPLS_TE_BACKUPHOP_ENTRY,
    CLI_MPLS_TE_NO_BACKUPHOP_ENTRY,
 CLI_MPLS_TE_TNL_NO_SRLG_TYPE,
 CLI_MPLS_REOPTIMIZE_TNL_LIST,
 CLI_MPLS_REOPTIMIZE_NO_TNL_LIST,
 CLI_MPLS_REOPTIMIZE_TUNNEL_SHOW,
 CLI_MPLS_REOPTIMIZE_MANUAL_TRIGGER
};

/* Accessing the ErrString with ErrCode value.
 * Since the ErrCode's starting Id is changed, this forumla is used
 * to access the Err String */
#define CLI_ERR_OFFSET_MPLS_TE(u4ErrCode) \
        (u4ErrCode - CLI_ERR_START_ID_MPLS_TE)

/*END OF FRR*/
#ifdef __TEECCLI_C__
CONST CHR1         *TE_CLI_ERROR_MSGS[] = {
    "\r\n Invalid TE error code\r\n",
    "\r\n%% Unable to Set Insegment Label\r\n",
    "\r\n%% Unable to Set Insegment Interface\r\n",
    "\r\n%% Unable to Set Insegment POP N Labels\r\n",
    "\r\n%% Unable to Set Insegment Address Family\r\n",
    "\r\n%% Unable to Set Outsegment Label\r\n",
    "\r\n%% Unable to Set Outsegment PushLabel\r\n",
    "\r\n%% Unable to Set Outsegment Next Hop Addr Type\r\n",
    "\r\n%% Unable to Set Outsegment Next Hop Address\r\n",
    "\r\n%% Unable to Set Outsegment Interface\r\n",
    "\r\n%% Unable to Set Outsegment Direction\r\n",
    "\r\n%% Unable to Set Insegment Direction\r\n",
    "\r\n%% Unable to Get HLSP Outsegment \r\n",
    "\r\n%% Unable to Set Label Stack Label \r\n",
    "\r\n%% Unable to Set Label Stack Row Status \r\n",
    "\r\n%% Unable to Set LSP Map operation \r\n",
    "\r\n%% Map Number not present in Node Map Table \r\n",
    "\r\n%% Local Map Number of this node is not configured \r\n",
    "\r\n%% P2MP Destination cannot be a Local Map Number of this node \r\n",
    "\r\n%% P2MP Destination Entry already exists \r\n",
    "\r\n%% Invalid Subnet Error \r\n",
    "\r\n%% P2MP Destination overlaps with existing Interface IP Address \r\n",
    "\r\n%% Client tunnel is associated with the HLSP tunnel \r\n",
    "\r\n%% Tunnel is Associated with OAM/BFD \r\n",
    "\r\n%% ELPS is associated with the tunnel \r\n",
    "\r\n%% No Free Mpls-Tunnel Interface Index \r\n",
    "\r\n%% Cannot configure Affinity value without tunnel shutdown with MBB disabled \r\n",
    "\r\n%% Cannot configure the required tunnel attribute with tunnel up with MBB disabled \r\n",
    "\r\n%% Unable to Set Class Type Bandwidth Percentage \r\n",
    "\r\n%% Class Type Bandwidth Percentage exceed Maximum limit."
            " Sum of all Class Type Bandwidth should not be greater than 100\r\n",
    "\r\n%% Path-Option not configured in Explicit Route \r\n",
    "\r\n%% Hold-priority cannot be changed for MBB \r\n"
    "\r\n%% \r\n"    
};
#else
extern CONST CHR1         *TE_CLI_ERROR_MSGS[];
#endif

INT4 cli_process_te_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command, ...));


/*----------------------------------RSVP-TE-----------------------------------*/

#define  MPLS_CLI_RSVP_MAX_ARGS           8

#define  MPLS_RSVP_MODE      "rsvp-cfg"
#define  MPLS_RSVP_IF_MODE   "rsvp-if-cfg-"

#define  MPLS_CLI_HELLO_REL(rel)      (rel == RPTE_NBR_HELLO_REL_ACTIVE) ?\
                                      "Active" : "Passive"

/* List of rsvp command macros */                                      
enum {
    MPLS_CLI_RPTE_GLBL_STATUS = 1,
    MPLS_CLI_RPTE_INTF_CREATE,
    MPLS_CLI_RPTE_INTF_DELETE,
    MPLS_CLI_RPTE_SET_RR_STATUS,
    MPLS_CLI_RPTE_SET_HELLO_INTVL,
    MPLS_CLI_RPTE_SET_INTF_RFRSH_INTVL,
    MPLS_CLI_RPTE_SET_INTF_RFRSH_NMISS,
    MPLS_CLI_RPTE_SET_RTRID,
    MPLS_CLI_RPTE_SET_GEN_LBL_SPACE,
    MPLS_CLI_RPTE_RESET_GEN_LBL_SPACE,
    MPLS_CLI_RPTE_SET_SOCK_SUPPORT,
    MPLS_CLI_RPTE_SET_HELLO_SUPPORT,
    MPLS_CLI_RPTE_SET_MSGID_SUPPORT,
    MPLS_CLI_RPTE_SET_MAX_TNLS,
    MPLS_CLI_RPTE_SET_MAX_TNL_ERHOPS,
    MPLS_CLI_RPTE_SET_MAX_RSVP_INTFS,
    MPLS_CLI_RPTE_SET_MAX_RSVP_NBRS,
    MPLS_CLI_RPTE_SET_RMDPOLICY,
    MPLS_CLI_RPTE_SET_ROUTE_DELAY,
    MPLS_CLI_RPTE_SET_UDP_ENCAP,
    MPLS_CLI_RPTE_SET_HELLO_ONIF,
    MPLS_CLI_RPTE_SET_LINK_ATTR,
    MPLS_CLI_RPTE_SET_INTF_MTU,
    MPLS_CLI_RPTE_SET_INTF_TTL,
    MPLS_CLI_RPTE_SET_INTF_STATUS,
    MPLS_CLI_RPTE_DBG_DUMP_MSGS,
    MPLS_CLI_RPTE_DBG_HELLO,
    MPLS_CLI_RPTE_DBG_INTF,
    MPLS_CLI_RPTE_DBG_PACKET,
    MPLS_CLI_RPTE_DBG_PATH,
    MPLS_CLI_RPTE_DBG_PROCESS,
    MPLS_CLI_RPTE_DBG_RESV,
    MPLS_CLI_RPTE_SHOW_COUNTERS,
    MPLS_CLI_RPTE_CLEAR_COUNTERS,
    MPLS_CLI_RPTE_SHOW_HELLO_INFO,
    MPLS_CLI_RPTE_SHOW_NBR_INFO,
    MPLS_CLI_RPTE_SHOW_PATH_REQ,
    MPLS_CLI_RPTE_FRR_DETOUR_MERG,
    MPLS_CLI_RPTE_FRR_DETOUR,
    MPLS_CLI_RPTE_FRR_REVERTIVE_LOCAL,
    MPLS_CLI_RPTE_FRR_REVERTIVE_GLOBAL,
    MPLS_CLI_RPTE_FRR_CSPF,
    MPLS_CLI_RPTE_FRR_NO_CSPF,
    MPLS_CLI_RPTE_FRR_BKPUP_TUNNEL,
    MPLS_CLI_RPTE_FRR_NO_BKPUP_TUNNEL,
    MPLS_CLI_RPTE_FRR_MAKE_AFTER_BREAK,
    MPLS_CLI_RPTE_PLR_AND_AVOID_NODE_ID,
    MPLS_CLI_RPTE_SET_DS_OVERRIDE,
    MPLS_CLI_RPTE_DBG_NEIGHBOUR,
    MPLS_CLI_RPTE_DBG_FRR,
    MPLS_CLI_RPTE_ADMIN_STATUS_CAP,
    MPLS_CLI_RPTE_PATH_STATE_REMOVE,
    MPLS_CLI_RPTE_LABEL_SET_SUPPORT,
    MPLS_CLI_RPTE_ADMIN_GRACEFUL_TIMER,
    MPLS_CLI_RPTE_GR_MODE,
    MPLS_CLI_RPTE_GR_RESTART_TIMER,
    MPLS_CLI_RPTE_GR_RECOVERY_TIMER,
    MPLS_CLI_RPTE_SHOW_GR_INFO,
    MPLS_CLI_RPTE_DBG_GR,
    MPLS_CLI_RPTE_SET_NBRID,
    MPLS_CLI_RPTE_DEL_NBRID,
    MPLS_CLI_RPTE_DBG_DUMP_MSG_LVL,
 MPLS_CLI_RPTE_REOPTIMIZE_TIMER,
 MPLS_CLI_RPTE_ERO_CACHE_TIMER,
 MPLS_CLI_RPTE_REOPT_LINK_MAINTENANCE,
 MPLS_CLI_RPTE_REOPT_NODE_MAINTENANCE,
 MPLS_CLI_RPTE_SHOW_REOPT_INFO,
 MPLS_CLI_RPTE_DBG_REOPT
};

/* List of rsvp message types */
enum {
    MPLS_CLI_RPTE_MSG_ALL = 1,
    MPLS_CLI_RPTE_MSG_ACK,
    MPLS_CLI_RPTE_MSG_BUNDLE,
    MPLS_CLI_RPTE_MSG_CNFRM,
    MPLS_CLI_RPTE_MSG_HELLO,
    MPLS_CLI_RPTE_MSG_PATH,
    MPLS_CLI_RPTE_MSG_PERR,
    MPLS_CLI_RPTE_MSG_PTEAR,
    MPLS_CLI_RPTE_MSG_RESV,
    MPLS_CLI_RPTE_MSG_RERR,
    MPLS_CLI_RPTE_MSG_RTEAR,
    MPLS_CLI_RPTE_MSG_SRFRSH,
    MPLS_CLI_RPTE_MSG_NOTIFY,
    MPLS_CLI_RPTE_MSG_RECOVERY_PATH
};

INT4 cli_process_rsvp_cmd ARG_LIST ((tCliHandle CliHandle, UINT4 u4Command,
                                     ...));
INT4
RpteCliConfigMode ARG_LIST ((tCliHandle CliHandle));

INT1
RpteCliGetRsvpCfgPrompt ARG_LIST ((INT1 *pi1ModeName, INT1 *pi1DispStr));

INT1
RpteCliGetRsvpIfCfgPrompt ARG_LIST ((INT1 *pi1ModeName, INT1 *pi1DispStr));


/* RSVP-TE ERROR MSGS & it's ENUM*/
enum
{
RPTE_CLI_NO_ERROR=0,
RPTE_CLI_MAX_ER_HOP,
RPTE_CLI_RSVP_ENABLE,
RPTE_CLI_RSVP_NO_REOPT_TUNNEL,
RPTE_CLI_MAX_ERROR_MSGS
};

#ifdef __RPTEECCLI_C__
CONST CHR1         *RPTE_CLI_ERROR_MSGS[] = {
"\n \n",
"% Unable to set. Some of the Tunnels are already using greater Erhops.\n",
"% RSVP must be disbled before set/reset maximum erhops per tunnel.\n",
"% There is no tunnel with reoptimization enabled.\n",
"\n \n"
};
#else
extern CONST CHR1         *RPTE_CLI_ERROR_MSGS[];
#endif

/*------------------------------L2VPN-MODULE---------------------------------*/
/* MPLS CLI Command Constants. To Add A New Command,A New Definition Needs */
/* To Be Added To The List.                                                */
                                           
#define  CLI_MPLS_VPWS_VLAN_BASED_COMMAND       0x01
#define  CLI_MPLS_VPWS_PORT_BASED_COMMAND       0x02
#define  CLI_MPLS_TE_CREATE                     0x04
#define  CLI_MPLS_VCONLY_CREATE                 0x08
#define  CLI_MPLS_NON_TE_CREATE                 0x10
#define  CLI_MPLS_VPWS_GLBL_COMMAND             0x20
#define  CLI_MPLS_VLAN_MODE                0x80

/*MS-PW */
#define  CLI_MPLS_VPWS_SWITCH_BASED_COMMAND   0x40

#define  CLI_MPLS_L2VPN_DETAIL       1
#define  CLI_MPLS_L2VPN_SUMMARY      2
#define  CLI_MPLS_L2VPN_PEER_DETAIL  3
#define  CLI_MPLS_L2VPN_PEER_SUMMARY 4

#define CLI_L2VPN_SHOW_PW_OPER_STATUS(i4PwOperStatus) \
                                     (i4PwOperStatus == 1 ? "up" : \
                                     (i4PwOperStatus == 2 ? "down" : \
                                     (i4PwOperStatus == 3 ? "testing" : \
                                     (i4PwOperStatus == 4 ? "unknown" : \
                                     (i4PwOperStatus == 5 ? "dormant" : \
                                     (i4PwOperStatus == 6 ? "not present" : \
                                      "lower layer down"))))))

#define CLI_L2VPN_SHOW_PW_OWNER(i4PwOwner) \
                                (i4PwOwner == CLI_L2VPN_PW_OWNER_MANUAL ? \
                                 "manual" : \
                                 (i4PwOwner == CLI_L2VPN_PW_OWNER_PW_ID_FEC \
                                 ? "pwid" : "genfec"))

#define CLI_L2VPN_SHOW_BRIDGE_DOMAIN(u4BridgeMode) \
                                     (u4BridgeMode == 1 ? "customer" : \
                                     (u4BridgeMode == 2 ? "provider" : \
                                     (u4BridgeMode == 3 ? "provider edge" : \
                                     (u4BridgeMode == 4 ? "provider core" : \
                                      "invalid bridge"))))


 /* VCCV Related MACROS*/

 #define CLI_MPLS_VCCV_INACTIVE_ENABLE 1
 #define CLI_MPLS_VCCV_INACTIVE_DISABLE 0

 /*VCCV/OAM Related Command Enum*/
 enum {
      CLI_MPLS_OAM_CONFIG = 1,
      CLI_MPLS_OAM_MEP_CONFIG,
      CLI_MPLS_OAM_MEP_DECONFIG,
      CLI_MPLS_NOTIF,
      CLI_MPLS_NO_NOTIF,
      CLI_MPLS_SHOW_VC_LABEL_BINDING,
      CLI_MPLS_SHOW_HW_CAPABS,
      CLI_CONFIG_VCCV_GLOBAL_CC,
      CLI_CONFIG_VCCV_GLOBAL_CV,
      CLI_MPLS_SHOW_GBL_INFO,   
      CLI_MPLS_MAP_PWID,
      CLI_MPLS_UNMAP_PWID,
      CLI_VCCV_NO_GLOBAL_CC,
      CLI_VCCV_NO_GLOBAL_CV, 
 };


/* VPWS Related command enum */
enum 
{
    CLI_MPLS_VPWS_CREATE = 1,
    CLI_MPLS_VPWS_DELETE,
    CLI_MPLS_L2VPN_DBG,
    CLI_MPLS_NO_L2VPN_DBG,
    CLI_MPLS_L2VPN_SHOW
};
/* VPLS Related Command enum */                                     
                                    
#define CLI_MPLS_VPLS_RT_IMPORT         L2VPN_VPLS_RT_IMPORT
#define CLI_MPLS_VPLS_RT_EXPORT         L2VPN_VPLS_RT_EXPORT
#define CLI_MPLS_VPLS_RT_BOTH           L2VPN_VPLS_RT_BOTH

#define CLI_MPLS_L2VPN_MAX_VPLS_RD_LEN  L2VPN_MAX_VPLS_RD_LEN
#define CLI_MPLS_L2VPN_MAX_VPLS_RT_LEN  L2VPN_MAX_VPLS_RT_LEN
#define CLI_MPLS_L2VPN_MAX_VPLS_VE_LEN  L2VPN_MAX_VPLS_NAME_LEN

#define CLI_MPLS_L2VPN_VPLS_RD_SUBTYPE  L2VPN_VPLS_RD_SUBTYPE
#define CLI_MPLS_L2VPN_VPLS_RT_SUBTYPE  L2VPN_VPLS_RT_SUBTYPE

#define CLI_MPLS_L2VPN_ENABLED          L2VPN_ENABLED
#define CLI_MPLS_L2VPN_DISABLED         L2VPN_DISABLED

#define CLI_MPLS_DEFAULT_MTU            L2VPN_VPLS_DEFAULT_MTU

enum {
    CLI_MPLS_VFI_CREATE = 1,
    CLI_MPLS_VFI_DELETE,
    CLI_MPLS_VPN_CREATE,
    CLI_MPLS_VPN_DELETE,
    CLI_MPLS_NEIGHBOR_CREATE,
    CLI_MPLS_NEIGHBOR_DELETE,
    CLI_MPLS_XCONNECT_CREATE,
    CLI_MPLS_XCONNECT_DELETE,
    CLI_MPLS_SHOW_VFI,
    CLI_MPLS_SHOW_ALL,

#ifdef VPLSADS_WANTED
    CLI_MPLS_RD_CREATE,
    CLI_MPLS_RD_DELETE,
    CLI_MPLS_RT_CREATE,
    CLI_MPLS_RT_DELETE,
    CLI_MPLS_SET_CONTROL_WORD,
    CLI_MPLS_SET_MTU,
    CLI_MPLS_VE_CREATE,
    CLI_MPLS_VE_DELETE,
#endif
    /* represents the Max L2Vpn commands */
    CLI_MPLS_MAX_L2VPN_CMDS
};

/* PW Redundancy class related commands */
enum {
    CLI_MPLS_RED_GRP_ENABLE = 1,
    CLI_MPLS_RED_GRP_DISABLE,
    CLI_MPLS_RED_GRP_SYNC_NOTIF_ENABLE,
    CLI_MPLS_RED_GRP_SYNC_NOTIF_DISABLE,
    CLI_MPLS_RED_GRP_PWSTATUS_NOTIF_ENABLE,
    CLI_MPLS_RED_GRP_PWSTATUS_NOTIF_DISABLE,
    CLI_MPLS_RED_GRP_NEG_TIMEOUT,
    CLI_MPLS_RED_GRP_CREATE,
    CLI_MPLS_RED_GRP_DELETE,
    CLI_MPLS_RED_GRP_NAME,
    CLI_MPLS_RED_GRP_PROT_TYPE_CONFIG,
    CLI_MPLS_RED_GRP_REVERSION_ENABLE,
    CLI_MPLS_RED_GRP_REVERSION_DISABLE,
    CLI_MPLS_RED_GRP_WTR_VAL,
    CLI_MPLS_RED_GRP_MODE_CONFIG,
    CLI_MPLS_RED_GRP_STATUS_CONFIG,
    CLI_MPLS_RED_GRP_LOCK_PROTECT,
    CLI_MPLS_RED_GRP_LOCK_PROTECT_DISABLE,
    CLI_MPLS_RED_GRP_FS,
    CLI_MPLS_RED_GRP_MULTI_HOMING_APPS_CONFIG,
    CLI_MPLS_RED_GRP_ACTIVATE,
    CLI_MPLS_RED_GRP_DEACTIVATE,
    CLI_MPLS_PW_RED_ICCP_GRP,
    CLI_MPLS_RED_GRP_ICCP_NEIGHBOR,
    CLI_MPLS_RED_GRP_ICCP_NEIGHBOR_ACTIVATE,
    CLI_MPLS_RED_GRP_NO_ICCP_NEIGHBOR,
    CLI_MPLS_RED_PW_PREF_CONFIG,
    CLI_MPLS_PSW_RED_SHOW_GBL_INFO,
    CLI_MPLS_PSW_RED_SHOW_GRP_INFO,
    CLI_MPLS_SHOW_RED_PWS,
    CLI_MPLS_SHOW_RED_NODES,
    CLI_MPLS_SHOW_ICCP_PWS
#ifdef MPLS_TEST_WANTED
#if defined(LDP_GR_WANTED) || (VPLS_GR_WANTED)
    ,CLI_MPLS_SHOW_HW_LIST
#endif
#endif
};
#define L2VPN_CLI_IPV4_ADDR_TYPE         1
#define L2VPN_CLI_IPV6_ADDR_TYPE         2

#define L2VPN_CLI_PWRED_PREF_PRIMARY     1
#define L2VPN_CLI_PWRED_PREF_SECONDARY   2

#define L2VPN_CLI_PSWRED_MODE_MASTERSLAVE 2
#define L2VPN_CLI_PSWRED_MODE_INDEPENDENT 1

#define L2VPN_CLI_PSWRED_LOCKOUT_ENABLE  1
#define L2VPN_CLI_PSWRED_LOCKOUT_DISABLE 0

#define L2VPN_CLI_PSWRED_MODE_MASTER  1
#define L2VPN_CLI_PSWRED_MODE_SLAVE   2

#define L2VPN_CLI_PW_BACKUP             1

#define L2VPN_CLI_PW_ACTIVATE         1
#define L2VPN_CLI_PW_DEACTIVATE       2

#define  L2VPN_CLI_MAX_CMDS            (CLI_MPLS_MAX_L2VPN_CMDS - 1)
#define  L2VPN_CLI_MAX_ARGS             50
#define  L2VPN_PWRED_CLI_MAX_ARGS       10

#define L2VPN_GLOB_CONFIG_SIZE (CLI_MAX_HEADER_SIZE + (CLI_MAX_COLUMN_WIDTH * \
                     L2VPN_CLI_MAX_GLOBAL_CONFIG_OBJECTS))

#define L2VPN_CLI_MAX_GLOBAL_CONFIG_OBJECTS             0x8
#define  CLI_L2VPN_ZERO                       0

/* SIG CLI RELATED MACRO */
#define CLI_L2VPN_PW_OWNER_MANUAL       1
#define CLI_L2VPN_PW_OWNER_PW_ID_FEC    2
#define CLI_L2VPN_PW_OWNER_GEN_FEC      3
#define CLI_L2VPN_PW_OWNER_L2TP         4
#define CLI_L2VPN_PW_OWNER_OTHER        5

#define CLI_L2VPN_TE                    1
#define CLI_L2VPN_NON_TE                2
#define CLI_L2VPN_VCONLY                3

#define CLI_L2VPN_PW_TYPE_ETH_TAGGED    4
#define CLI_L2VPN_PW_TYPE_ETH           5

#define CLI_L2VPN_VLAN_MODE_OTHER       0
#define CLI_L2VPN_VLAN_MODE_PORT_BASED  1
#define CLI_L2VPN_VLAN_MODE_NO_CHANGE   2
#define CLI_L2VPN_VLAN_MODE_CHANGE_VLAN 3
#define CLI_L2VPN_VLAN_MODE_ADD_VLAN    4
#define CLI_L2VPN_VLAN_MODE_REM_VLAN    5
#define CLI_L2VPN_PWVC_ENET_DEF_PW_VLAN    4097

#define L2VPN_CLI_VLAN                  0x1
#define L2VPN_CLI_MANUAL                0x2
#define L2VPN_CLI_CNTWRD                0x4
#define L2VPN_CLI_MTU                   0x8
#define L2VPN_CLI_INBOUND               0x10
#define L2VPN_CLI_MPLSTYPE              0x20
#define L2VPN_CLI_GRP                   0x40 

#define L2VPN_CLI_OWNER_MAN_DEF         0x2
#define L2VPN_CLI_NO_CNTWRD_DEF         0x1

#define L2VPN_CLI_VLAN_MODE             0x4
#define L2VPN_CLI_ETH_MODE              0x5  
#define L2VPN_CLI_MPLSTYPE_TE           0x80
#define L2VPN_CLI_MPLSTYPE_NON_TE       0x40
#define L2VPN_CLI_MPLSTYPE_VC_ONLY      0x20

#define L2VPN_CLI_INBOUND_MODE_LOOSE    0x1
#define L2VPN_CLI_INBOUND_MODE_STRICT   0x2

#define L2VPN_PWVC_DEF_EXP_MODE 1
#define L2VPN_PWVC_DEF_EXP_BITS 0
#define L2VPN_PWVC_DEF_TTL      2  

#define L2VPN_PWVC_ENET_VLAN 4097
#define PWVC_VLAN_MODE_PORT_BASED 1

#define PWVC_PRI_MAP_VALUE  65408

#define  IP_STR_MAX_LEN              1024
#define  IPV6_ADDR_LENGTH            16

#define CLI_MPLS_L2VPN_MAX_AGI_LEN             L2VPN_PWVC_MAX_AGI_LEN

#define CLI_MPLS_L2VPN_PWVC_AGI_TYPE0          L2VPN_PWVC_AGI_TYPE0
#define CLI_MPLS_L2VPN_PWVC_AGI_TYPE1          L2VPN_PWVC_AGI_TYPE1
#define CLI_MPLS_L2VPN_PWVC_AGI_TYPE2          L2VPN_PWVC_AGI_TYPE2

#define CLI_MPLS_L2VPN_AGI_SUBTYPE             10

#define CLI_MPLS_L2VPN_AGI_TYPE0_ASN_OFFSET    L2VPN_PWVC_AGI_TYPE0_ASN_OFFSET
#define CLI_MPLS_L2VPN_AGI_TYPE1_ASN_OFFSET    L2VPN_PWVC_AGI_TYPE1_ASN_OFFSET
#define CLI_MPLS_L2VPN_AGI_TYPE2_ASN_OFFSET    L2VPN_PWVC_AGI_TYPE2_ASN_OFFSET

#define CLI_MPLS_L2VPN_AGI_TYPE0_ASN_LENGTH    L2VPN_PWVC_AGI_TYPE0_ASN_LENGTH
#define CLI_MPLS_L2VPN_AGI_TYPE1_ASN_LENGTH    L2VPN_PWVC_AGI_TYPE1_ASN_LENGTH
#define CLI_MPLS_L2VPN_AGI_TYPE2_ASN_LENGTH    L2VPN_PWVC_AGI_TYPE2_ASN_LENGTH

#define CLI_MPLS_L2VPN_AGI_TYPE0_ID_OFFSET     L2VPN_PWVC_AGI_TYPE0_ID_OFFSET
#define CLI_MPLS_L2VPN_AGI_TYPE1_ID_OFFSET     L2VPN_PWVC_AGI_TYPE1_ID_OFFSET
#define CLI_MPLS_L2VPN_AGI_TYPE2_ID_OFFSET     L2VPN_PWVC_AGI_TYPE2_ID_OFFSET

#define CLI_MPLS_L2VPN_AGI_TYPE0_ID_LENGTH     L2VPN_PWVC_AGI_TYPE0_ID_LENGTH
#define CLI_MPLS_L2VPN_AGI_TYPE1_ID_LENGTH     L2VPN_PWVC_AGI_TYPE1_ID_LENGTH
#define CLI_MPLS_L2VPN_AGI_TYPE2_ID_LENGTH     L2VPN_PWVC_AGI_TYPE2_ID_LENGTH


/* error handling */
enum {
L2VPN_CLI_ERR_INVALID_PARAM = 1,
L2VPN_CLI_ERR_ADM_DOWN,
L2VPN_CLI_ERR_VPN_ADMIN_SET_FAILED,
L2VPN_CLI_ERR_PWVC_ADMIN_SET_FAILED,
L2VPN_CLI_ERR_PWVC_ID_SET_FAILED,
L2VPN_CLI_ERR_PWVC_TYPE_SET_FAILED,
L2VPN_CLI_ERR_PWVC_ADDR_SET_FAILED,
L2VPN_CLI_ERR_PWVC_CW_SET_FAILED,
L2VPN_CLI_ERR_PWVC_MTU_SET_FAILED,
L2VPN_CLI_ERR_PWVC_INBND_SET_FAILED,
L2VPN_CLI_ERR_PWVC_OWNER_SET_FAILED,
L2VPN_CLI_ERR_PWVC_OUTLBL_SET_FAILED,
L2VPN_CLI_ERR_PWVC_INLBL_SET_FAILED,
L2VPN_CLI_ERR_PWVC_GRPID_SET_FAILED,
L2VPN_CLI_ERR_MPLS_TYPE_SET_FAILED,
L2VPN_CLI_ERR_MPLS_EXP_BITS_MODE_FAILED,
L2VPN_CLI_ERR_MPLS_EXP_BITS_FAILED,
L2VPN_CLI_ERR_MPLS_TTL_FAILED,
L2VPN_CLI_ERR_LCL_LDPID_FAILED,
L2VPN_CLI_ERR_LCL_LDP_ENT_ID_FAILED,
L2VPN_CLI_ERR_OUTB_ROWSTATUS_FAILED,
L2VPN_CLI_ERR_OUTB_TNL_INDEX_FAILED,
L2VPN_CLI_ERR_OUTB_TNL_INST_FAILED,
L2VPN_CLI_ERR_OUTB_TNL_LCL_LSR_FAILED,
L2VPN_CLI_ERR_OUTB_TNL_PEER_LSR_FAILED,
L2VPN_CLI_ERR_OUTB_TNL_XC_FAILED,
L2VPN_CLI_ERR_OUTB_TNL_IFINDEX_FAILED,
L2VPN_CLI_ERR_INB_ROWSTATUS_FAILED,
L2VPN_CLI_ERR_INB_TNL_INDEX_FAILED,
L2VPN_CLI_ERR_INB_TNL_INST_FAILED,
L2VPN_CLI_ERR_INB_TNL_LCL_LSR_FAILED,
L2VPN_CLI_ERR_INB_TNL_PEER_LSR_FAILED,
L2VPN_CLI_ERR_INB_TNL_XC_FAILED,
L2VPN_CLI_ERR_INB_TNL_IFINDEX_FAILED,
L2VPN_CLI_ERR_PRI_ROWSTATUS_FAILED,
L2VPN_CLI_ERR_PRI_MAP_FAILED,
L2VPN_CLI_ERR_PWVC_DELETE_FAILED,
L2VPN_CLI_ERR_PWVC_ROW_SET_FAILED,
L2VPN_CLI_ERR_VPN_ADMIN_ALREADY_UP,
L2VPN_CLI_ERR_VPN_ADMIN_ALREADY_DOWN,
L2VPN_CLI_ERR_GET_IFINDEX_FAILED,
L2VPN_CLI_ERR_LDP_GET_ENT_FAILED,
L2VPN_CLI_ERR_PWVC_ENTRY_EXISTS,
L2VPN_CLI_MAX_ERROR_MSGS    
};

#ifdef L2VPN_TEST_WANTED
INT4 cli_process_pwred_test_cmd  ARG_LIST ((tCliHandle CliHandle,
                                     UINT4 u4Command, ...));
#endif

#ifdef VPLSADS_WANTED
/* VPLS command related proto's */
INT4
L2VpnCliParseAndGenerateRdRt ARG_LIST ((UINT1* pu1RandomString, 
                                        UINT1* pu1RdRt));
#endif

INT4 L2VpnIpValidateAndGenerate ARG_LIST ((UINT1* pu1RandomString,
                                           UINT1* pu1addrType,
                                           UINT1* pu1IpAddress));

INT4 cli_process_vpls_cmd ARG_LIST ((tCliHandle CliHandle, 
                                     UINT4 u4Command, ...));
/* PW OAM CLI RELATED PROTOTYPES */
 INT4 cli_process_l2vpn_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command, ...));

INT1 
MplsGetMplsVfiCfgPrompt ARG_LIST ((INT1 *pi1ModeName, INT1 *pi1DispStr));

INT4 PwCreateEnetEntry ARG_LIST ((tCliHandle CliHandle,UINT4 u4PwIndex, 
                                  UINT4 u4EnetInstance, UINT4 u4PwEnetPwVlan,
                                  UINT4 u4PwEnetPortVlan, UINT4 u4IfIndex, 
                                  UINT4 u4Mode));

/* VPWS CLI RELATED PROTOTYPES */
INT4 cli_process_vpws_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command, ...));

VOID
CliMplsConvertTimetoString (UINT4 u4Time, INT1 *pi1Time);

/* VPWS CLI PW REDUNDANCY RELATED COMMANDS */
INT4
cli_process_pwred_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command, ...));

INT4
L2VpCliChangePwRedGrpMode ARG_LIST ((tCliHandle CliHandle, UINT4 u4PwRedGrpIndex));

INT1
L2VpGetPwRedClassCfgPrompt ARG_LIST ((INT1 *pi1ModeName, INT1 *pi1DispStr));

INT4
L2VpCliChangeRedPwNodeMode ARG_LIST ((tCliHandle CliHandle, UINT4 u4PwRedGrpIndex));

INT1
L2VpGetPwRedNodePrompt ARG_LIST ((INT1 *pi1ModeName, INT1 *pi1DispStr));



/*--------------------------MPLS STATIC CLI MODULE-------------------*/

/* MPLS CLI Command Constants. To Add A New Command,A New Definition Needs */ 
/* To Be Added To The List.                                                */

#define  MPLS_CLI_MAX_ARGS                20 

enum 
{
MPLS_CLI_NO_ERROR = CLI_ERR_START_ID_MPLS, 
MPLS_CLI_INVALID_PARAM,                   
MPLS_CLI_NO_ENTRY,
MPLS_CLI_NO_FREE_INDEX,
MPLS_CLI_FTN_ENTRY_CREATION,
MPLS_CLI_FTN_ENTRY_DELETION,
MPLS_CLI_INSEG_ENTRY_CREATION,
MPLS_CLI_INSEG_ENTRY_DELETION,
MPLS_CLI_OUTSEG_ENTRY_CREATION,
MPLS_CLI_OUTSEG_ENTRY_DELETION,
MPLS_CLI_XC_ENTRY_CREATION,
MPLS_CLI_XC_ENTRY_DELETION,
MPLS_CLI_ERR_ACTION_TYPE,
MPLS_CLI_ERR_ACTION_POINTER,
MPLS_CLI_ERR_ADDR_TYPE,
MPLS_CLI_ERR_DEST_ADDR,
MPLS_CLI_ERR_NEXT_HOP_ADDR,
MPLS_CLI_ERR_NEXT_HOP_ADDR_TYPE,
MPLS_CLI_ERR_INSEG_LABEL,
MPLS_CLI_ERR_INSEG_NPOP,
MPLS_CLI_ERR_FTN_MASK,
MPLS_CLI_ERR_INSEG_ADDR_FAMILY,
MPLS_CLI_ERR_OUTSEG_LABEL,
MPLS_CLI_ERR_OUTSEG_PUSH_TOP_LABEL,
MPLS_CLI_ERR_IFMAIN_TYPE,
MPLS_CLI_ERR_IFENTRY_CREATION,
MPLS_CLI_ERR_IFENTRY_DELETION,
MPLS_CLI_ERR_IFADMIN_STATUS,
MPLS_CLI_ERR_IFALIAS,
MPLS_CLI_ERR_OUTSEG_IF,
MPLS_CLI_ERR_FTN_SET_STATIC_BINDING_FAIL,             
MPLS_CLI_ERR_FTN_REMOVE_STATIC_BINDING_FAIL,       
MPLS_CLI_ERR_FTN_CONFIGURE_CROSSCONNECT,        
MPLS_CLI_ERR_FTN_REMOVE_CROSSCONNECT,        
MPLS_CLI_ERR_INSTACK_ENTRY_CREATION,
MPLS_CLI_ERR_OUTSTACK_ENTRY_CREATION,
MPLS_CLI_ERR_INVALID_ROUTER_ID,
MPLS_CLI_ERR_INVALID_TUNNEL_MODEL,
MPLS_CLI_ERR_INVALID_TTL_VALUE,
MPLS_CLI_SAME_IP,
MPLS_CLI_SAME_SUBNET,
MPLS_CLI_SAME_DEST,
MPLS_CLI_INVALID_SUBNET,
MPLS_CLI_MAX_ERROR_MSGS,
};

/* Accessing the ErrString with ErrCode value.
 * Since the ErrCode's starting Id is changed, this forumla is used
 * to access the Err String */
#define CLI_ERR_OFFSET_MPLS(u4ErrCode)   (u4ErrCode - CLI_ERR_START_ID_MPLS)

#ifdef __MPLSCLI_C__
CONST CHR1         *MPLS_CLI_ERROR_MSGS[] = {
    "\n \n",
    "% Invalid Parameter value\n",
    "% No Entry Present\n",
    "% No Free Index\n",
    "% Unable to create an FTN entry\n",
    "% Unable to delete an FTN entry\n",
    "% Unable to create an INSEG entry\n",
    "% Unable to delete an INSEG entry\n",
    "% Unable to create an OUTSEG entry\n",
    "% Unable to delete an OUTSEG entry\n",
    "% Unable to create an XC entry\n",
    "% Unable to delete an XC entry\n",
    "% Unable to set action type\n",
    "% Unable to set action pointer\n",
    "% Unable to set address type\n",
    "% Unable to set destination address\n",
    "% Unable to set next hop address\n",
    "% Unable to set next hop address type\n",
    "% Unable to set in-label\n",
    "% Unable to set insegment NPop\n",
    "% Unable to set ftn mask\n",
    "% Unable to set insegment address family\n",
    "% Unable to set out-label\n",
    "% Unable to set out segment push top label\n",
    "% Unable to set ifmain type new mpls interface\n",
    "% Unable to create new interface entry\n",
    "% Unable to delete the interface entry\n",
    "% Unable to set ifadmin status\n",
    "% Unable to set ifalias name\n",
    "% Unable to set outseg interface\n",
    "% Unable to set Ftn Static Binding of labels to IPv4\n",
    "% Unable to remove Ftn Static Binding of labels\n",
    "% Unable to configure crossconnect \n",
    "% Unable to remove crossconnect \n",
    "% Please enable MPLS on Incoming interface\n",
    "% Please enable MPLS on Outgoing interface\n",
    "% Unable to set the router id\n",
    "% Unable to set the MPLS Tunnel Model \n",
    "% Unable to set the MPLS TTL Value \n",
    "% Destination and Next Hop  have same ip \n",
    "% Destination and Next Hop fall in same subnet \n",
    "% Destination is same as self ip address \n",
    "% Invalid subnet \n",
    "\n \n"
};
#else
extern CONST CHR1         *MPLS_CLI_ERROR_MSGS[];
#endif
/* LDP ERROR MSGS & it's ENUM*/
enum
{
LDP_CLI_NO_ERROR= CLI_ERR_START_ID_MPLS_LDP,
LDP_CLI_INVALID_ENTITY, 
LDP_CLI_WRONG_ADDR_TYPE,
LDP_CLI_WRONG_ADDR_VALUE,
LDP_CLI_INVALID_ENTITY_TYPE,
LDP_CLI_ADDR_IN_USE,
LDP_CLI_INVALID_TLDP_CONFIG,
LDP_CLI_WRONG_IPV6_TARG_ADDR_TYPE,
LDP_CLI_INVALID_BFD_STATUS_VALUE,
LDP_CLI_INVALID_SESSION,
LDP_CLI_MAX_ERROR_MSGS
};

#define CLI_ERR_OFFSET_MPLS_LDP(u4ErrCode) \
        (u4ErrCode - CLI_ERR_START_ID_MPLS_LDP)

#ifdef __LDPECCLI_C__
CONST CHR1         *LDP_MPLS_CLI_ERROR_MSGS[] = {
"\n \n",
"% LDP Entity doesn't exist\n",
"% Entity Address type is not IPV4\n",
"% Wrong Address Value\n",
"% Entity type is not targeted\n",
"% Targeted IP address already in use\n",
"% Unable to set admin UP.\n  PHP, Hop count limit, Path vector are not valid for tLDP. Also Transport address TLV should be set as loopback for tLDP\n",
"% Target Ipv6 address is not Global Unique Address\n",
"% Unable to set bfd status\n",
"% Ldp Session does not exist\n"
"\n \n"
};
#else
extern CONST CHR1         *LDP_MPLS_CLI_ERROR_MSGS[];
#endif

/* PW RED ERROR MSGS & it's ENUM*/
enum
{
L2VPN_PWRED_CLI_NO_ERROR = CLI_ERR_START_ID_MPLS_PWRED,
L2VPN_PWRED_CLI_INVALID_PWRED_STATUS,
L2VPN_PWRED_CLI_INVALID_SYNC_NOTIFY_STATUS,
L2VPN_PWRED_CLI_INVALID_PWSTATUS_NOTIFY,
L2VPN_PWRED_CLI_INVALID_PWNEG_TIMEOUT,
L2VPN_PWRED_CLI_GRP_CREATE_FAILURE,
L2VPN_PWRED_CLI_GRP_SET_FAILURE,
L2VPN_PWRED_CLI_GRP_ACTIVATE_FAILURE,
L2VPN_PWRED_CLI_GRP_ACTIVATE_SET_FAILURE,
L2VPN_PWRED_CLI_GRP_DELETE_FAILURE,
L2VPN_PWRED_CLI_INVALID_REVERTIVE_TYPE,
L2VPN_PWRED_CLI_REVERTIVE_SET_FAILURE,
L2VPN_PWRED_CLI_INVALID_PROTECTION_TYPE,
L2VPN_PWRED_CLI_PROTECTION_SET_FAILURE,
L2VPN_PWRED_CLI_INVALID_RG_NAME_LEN,
L2VPN_PWRED_CLI_RG_NAME_SET_FAILURE,
L2VPN_PWRED_CLI_INVALID_WTR_VALUE,
L2VPN_PWRED_CLI_WTR_SET_FAILURE,
L2VPN_PWRED_CLI_INVALID_CONTENTION_RES_METHOD,
L2VPN_PWRED_CLI_CONTENTION_RES_SET_FAILURE,
L2VPN_PWRED_CLI_LOCKOUT_PROTECTION_FAILURE,
L2VPN_PWRED_CLI_INVALID_MASTER_SLAVE_MODE,
L2VPN_PWRED_CLI_MASTER_SLAVE_SET_FAILURE,
L2VPN_PWRED_CLI_FORCE_ACTIVE_PW_FAILURE,
L2VPN_PWRED_CLI_FORCE_ACTIVE_PW_SET_FAILURE,
L2VPN_PWRED_CLI_INVALID_DUAL_HOMS_VALUE,
L2VPN_PWRED_CLI_DUAL_HOME_APPS_SET_FAILURE,
L2VPN_PWRED_CLI_REDPW_ICCP_GRP_FAILURE,
L2VPN_PWRED_CLI_REDPW_ICCP_GRP_NEIGH_EXIST,
L2VPN_PWRED_CLI_REDPW_ICCP_GRP_NEIGH_NOT_EXIST,
L2VPN_PWRED_CLI_GRP_NOT_EXIST,
L2VPN_PWRED_CLI_GRP_STATUS_ACTIVE,
L2VPN_PWRED_CLI_GRP_NON_REVERTIVE,
L2VPN_PWRED_CLI_GRP_NAME_IN_USE,
L2VPN_PWRED_CLI_MAX_PWRED_ERROR_MSGS
};

/* Accessing the ErrString with ErrCode value.
 * Since the ErrCode's starting Id is changed, this forumla is used
 * to access the Err String */
#define CLI_ERR_OFFSET_MPLS_PWRED(u4ErrCode) \
        (u4ErrCode - CLI_ERR_START_ID_MPLS_PWRED)

#ifdef __L2VPCLI_C__
CONST CHR1         *L2VPN_MPLS_CLI_PWRED_ERROR_MSGS[] = {
    "\n \n",
    "% Invalid value for pseudowire redundancy status\n",
    "% Invalid value for pseudowire redundancy sync notification\n",
    "% Invalid value for pseudowire status notification \n",
    "% Invalid value for pseudowire negotiation timeout \n",
    "% Couldnot create the RG group \n",
    "% Could not set the RG group values\n",
    "% Couldnot activate the RG group \n",
    "% Could not set the RG group as active\n",
    "% Couldnot delete the RG group \n",
    "% Invalid type of revertion for the RG group \n",
    "% Could not set the revertive type value \n",
    "% Invalid type of protection for the RG group \n",
    "% Could not set the protection type value \n",
    "% Redundancy Group Name Length exceeds the limit \n",
    "% Could not set the protection group name \n",
    "% Invalid WTR value configured \n",
    "% Could not set the WTR value \n",
    "% Invalid Contention resolution Method value configured \n",
    "% Could not set the value for contention resolution \n",
    "% Invalid value for master/slave mode \n",
    "% Could not set the value for master/slave mode \n",
    "% Forceful switching of pseudowire to Active failed\n",
    "% Could not set the pseudowire to Active \n",
    "% Invalid value for the dual home apps configured\n",
    "% could not set the Dual Home Apps Value \n",
    "% Given ICCP GRP not exist \n",
    "% Given ICCP neighbor already exist\n",
    "% Given ICCP neighbor does not exist\n",
    "% RG Group does not exist \n",
    "% Cannot configure the RG Group values when the status is active \n",
    "% Deactivate the Redundancy group before enabling/disabling lockout or reversion \n",
    "% Cannot Configure WTR timer when reversion is not enabled \n",
    "% The configured RG group name is already associated with another RG \n",
    "\n \n" 
};
#else
extern CONST CHR1         *L2VPN_MPLS_CLI_PWRED_ERROR_MSGS[];
#endif

#ifdef VPLSADS_WANTED
/* PW VPLS ERROR MSGS & it's ENUM*/
enum
{
L2VPN_PW_VPLS_CLI_NO_ERROR = CLI_ERR_START_ID_MPLS_PWVPLS,
L2VPN_PW_VPLS_CLI_RD_ALREADY_ASSOCIATED,
L2VPN_PW_VPLS_CLI_RD_CREATE_FAILED,
L2VPN_PW_VPLS_CLI_RD_DELETE_FAILED,
L2VPN_PW_VPLS_CLI_INVALID_RD_VALUE,
L2VPN_PW_VPLS_CLI_DUPLICATE_RD_VALUE,
L2VPN_PW_VPLS_CLI_DUPLICATE_RT_VALUE,
L2VPN_PW_VPLS_CLI_RT_CREATE_FAILED,
L2VPN_PW_VPLS_CLI_INVALID_RT_VALUE,
L2VPN_PW_VPLS_CLI_INVALID_RT_TYPE,
L2VPN_PW_VPLS_CLI_RT_DELETE_FAILED,
L2VPN_PW_VPLS_CLI_VE_ID_INVALID,
L2VPN_PW_VPLS_CLI_VE_DUPLICATE,
L2VPN_PW_VPLS_CLI_VE_CREATE_FAILED,
L2VPN_PW_VPLS_CLI_VE_ALREADY_EXISTS,
L2VPN_PW_VPLS_CLI_VE_NOT_EXISTS,
L2VPN_PW_VPLS_CLI_VE_DELETE_FAILED,
L2VPN_PW_VPLS_CLI_AC_NOT_FOUND,
L2VPN_PW_VPLS_CLI_AC_DUPLICATE,
L2VPN_PW_VPLS_CLI_AC_CREATE_FAILED,
L2VPN_PW_VPLS_CLI_AC_DELETE_FAILED,
L2VPN_PW_VPLS_CLI_AC_WRONG_INDEX,
L2VPN_PW_VPLS_CLI_AC_WRONG_VLAN,
L2VPN_PW_VPLS_CLI_MAX_ERROR_MSGS
};

/* Accessing the ErrString with ErrCode value.
 * Since the ErrCode's starting Id is changed, this forumla is used
 * to access the Err String */
#define CLI_ERR_OFFSET_MPLS_PW_VPLS(u4ErrCode) \
        (u4ErrCode - CLI_ERR_START_ID_MPLS_PWVPLS)

#ifdef __L2VPCLI_C__
CONST CHR1         *L2VPN_MPLS_CLI_PW_VPLS_ERROR_MSGS[] = {
    "\n \n",
    "% A Rd entry already associated with this VPLS \n",
    "% Could not create RD entry \n",
    "% Could not delete RD entry \n",
    "% RD value is not valid. ASN value and type mismatch with BGP\n",
    "% The RD value already associated with some other VPLS \n",
    "% The RT value already associated with some VPLS \n",
    "% Could not create RT entry \n",
    "% RT value is not valid. ASN value and type mismatch with BGP\n",
    "% RT type is not valid. Per VPLS only one RT can have export policy. \n",
    "% Could not delete RT entry \n",
    "% VE id is not valid. Ve id should be in range 1 - Max sites \n",
    "% Same VE id Entry already mapped to this VPLS \n",
    "% Could not create VE entry \n",
    "% One VE id Entry already mapped to this VPLS \n",
    "% VE id Entry not mapped to this VPLS \n",
    "% Could not delete VE entry \n",
    "% Could not find AC Corresponding to VPLS \n",
    "% This AC Entry is already mapped to a VPLS \n",
    "% Could not create AC entry \n",
    "% Could not delete AC entry \n",
    "% Wrong Port given for AC \n",
    "% Wrong Vlan given for AC \n",
    "\n \n" 
};
#else
extern CONST CHR1         *L2VPN_MPLS_CLI_PW_VPLS_ERROR_MSGS[];
#endif
#endif


#define  MPLS_CLI_FTN_EGRS_LBL_BINDING         1
#define  MPLS_CLI_FTN_INGRS_LBL_BINDING        2
#define  MPLS_CLI_FTN_NO_EGRS_LBL_BINDING      3
#define  MPLS_CLI_FTN_NO_INGRS_LBL_BINDING     4
#define  MPLS_CLI_FTN_NO_LBL_BINDING           5
#define  MPLS_CLI_SHOWSTATICBINDING            6
#define  MPLS_CLI_SHOWSTATICCROSSCONNECT       7
#define  MPLS_CLI_SHOWFORWARDINGTABLE          8
#define  MPLS_CLI_FTN_STATIC_CROSSCONNECT      9
#define  MPLS_CLI_FTN_NO_STATIC_CROSSCONNECT   10


#define  CLI_MPLS_ENABLE                       11
#define  CLI_MPLS_DISABLE                      12

#define  MPLS_CLI_FTN_TE_BINDING               13
#define  MPLS_CLI_NO_FTN_TE_BINDING            14

#define  MPLS_CLI_COMMON_DB_TRACE              15

#define  MPLS_CLI_ROUTER_ID                    16
#define  MPLS_CLI_SIG_CAPABILITY               17
#define  MPLS_CLI_GR_MAX_WAIT_TIME             18
#define  MPLS_CLI_GR_NO_MAX_WAIT_TIME          19 
#define  MPLS_CLI_TUNNEL_MODE                  20
#define  MPLS_CLI_TTL_VALUE                    21
#define  MPLS_CLI_SHOW_TUNNEL_MODE             22
#define  MPLS_CLI_SHOW_TTL_VALUE               23
#define  MPLS_CLI_ACH_CODE_POINT               24
#define  MPLS_CLI_SHOW_MPLS_INTERFACES         25
#ifdef MPLS_IPV6_WANTED           
#define  MPLS_CLI_IPV6_SHOWSTATICBINDING       26 
#define  MPLS_CLI_IPV6_FTN_EGRS_LBL_BINDING    27
#define  MPLS_CLI_IPV6_FTN_INGRS_LBL_BINDING   28
#define  MPLS_CLI_IPV6_FTN_NO_EGRS_LBL_BINDING 29
#define  MPLS_CLI_IPV6_FTN_NO_INGRS_LBL_BINDING 30
#define  MPLS_CLI_IPV6_FTN_NO_LBL_BINDING       31
#define  MPLS_CLI_IPV6_FTN_STATIC_CROSSCONNECT 32
#define  MPLS_CLI_IPV6_FTN_NO_STATIC_CROSSCONNECT 33
#endif
#define  LDP_TEST_SH_FTN_HW_LIST               34
#define  LDP_TEST_SH_ILM_HW_LIST               35
#define  CLI_FM_DEBUG_LEVEL_ENABLE             36
#define  CLI_FM_DEBUG_LEVEL_DISABLE            37

#define  MPLS_TE_TNL_INTERMEDIATE              1
#define  MPLS_TE_TNL_EGRESS                    2
#define  MPLS_TE_TNL_INGRESS                   3
/* STATIC_HLSP */
#define  MPLS_TE_TNL_INTERMEDIATE_STACK        4
#define  MPLS_TE_TNL_INGRESS_STACK             5
#define  MPLS_TE_TNL_INTERMEDIATE_STITCH       6
#define  MPLS_TE_TNL_END_POINT_STITCH          7 

#define  MPLS_TE_TNL_SLSP_INGRESS              1
#define  MPLS_TE_TNL_SLSP_EGRESS               2

#define  MPLS_SB_PREFIX   0x08
#define  MPLS_SB_LOCAL    0x04
#define  MPLS_SB_REMOTE   0x02
#define  MPLS_SB_NEXTHOP  0x01

#define  MPLS_SB_LLABEL  0x01
#define  MPLS_SB_HLABEL  0x02
#define  MPLS_L2VPNRED_MULTIHOMINGAPP_LAGG  0x80

/*Generic function for IP Address type finding and ascii to integer conversion*/
INT1
MplsGetIpv4Ipv6AddrfromCliStr(tCliHandle CliHandle, CONST CHR1 *pInArgs,
                            tGenU4Addr *pCLIAddress);
/* MPLS show debugging related prototypes */
VOID IssMplsCommonShowDebugging (tCliHandle CliHandle);

VOID IssMplsTeShowDebugging (tCliHandle CliHandle);

VOID IssMplsLdpShowDebugging (tCliHandle CliHandle);

VOID IssRsvpShowDebugging (tCliHandle CliHandle);

VOID IssMplsOamShowDebugging (tCliHandle CliHandle);

VOID IssMplsL2vpnShowDebugging (tCliHandle CliHandle);


INT4
cli_process_mpls_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command, ...));
INT4
MplsCliShowMplsInterfaces (tCliHandle CliHandle);

#if LDP_GR_WANTED 
INT1
MplsDisplayFTNHwList (tCliHandle CliHandle);


INT1
MplsDisplayILMHwList (tCliHandle CliHandle);
#endif
INT1
MplsCliSetFsMplsEnable (tCliHandle CliHandle, UINT4 u4IfaceIndex);
INT1
MplsCliSetFsMplsDisable (tCliHandle CliHandle, UINT4 u4IfaceIndex);
INT1
MplsCliInFTNEntry (tSNMP_OCTET_STRING_TYPE *InSegmentIndex, UINT4 u4InLabel,
                   UINT4 u4InIfIndex, UINT2 u2AddrType, BOOL1 bFtnEntryExists);
INT1
MplsCliOutFTNEntry (tSNMP_OCTET_STRING_TYPE * OutSegmentIndex,
                    UINT4 u4OutLabel,
                    tGenU4Addr NextHop, BOOL1 bFtnEntryExists, UINT1 *pu1IsNhExist,
                    UINT4 u4OutIfIndex);
VOID
MplsCliDelTables (tSNMP_OCTET_STRING_TYPE *InSegmentIndex,
                  tSNMP_OCTET_STRING_TYPE *OutSegmentIndex,
                  tSNMP_OCTET_STRING_TYPE *XCIndex, UINT4 u4InIfIndex,
                  UINT4 u4FtnIndex);
INT1
MplsCliFtnLabelBinding (tCliHandle CliHandle,
                        tGenU4Addr Prefix,
                        UINT4 u4Mask, tGenU4Addr NextHop, UINT4 u4InIfIndex,
                        UINT4 u4InLabel, UINT4 u4OutLabel, INT4 i4FtnActType,
                        UINT4 u4OutIfIndex);
INT1
MplsCliFtnNoIngrsLB (tCliHandle CliHandle, tGenU4Addr Prefix, UINT4 u4Mask,
                     tGenU4Addr NextHop, UINT4 u4Label, INT4 i4FtnActType);
INT1
MplsCliFtnNoEgrsLB (tCliHandle CliHandle, tGenU4Addr Prefix,
                    UINT4 u4Label, INT4 i4FtnActType);            
INT1
MplsCliFtnNoLabelBinding (tCliHandle CliHandle, UINT2 u2AddrType);

INT1 
MplsCliFtnTeBinding (tCliHandle CliHandle,
                     UINT4 u4Prefix, UINT4 u4Mask, UINT4 u4TunnelId,
                     INT4 i4FtnActType, UINT4 u4TnlLspNum, UINT4 u4TnlIngressLSRId,
                     UINT4 u4TnlEgressLSRId);
INT1
MplsCliNoFtnTeBinding (tCliHandle CliHandle, UINT4 u4Prefix, UINT4 u4Mask,
                       INT4 i4FtnActType);

INT1
MplsCliShowStaticBinding (tCliHandle CliHandle, UINT4 u4Flag,
                          UINT4 u4SBPrefix, UINT4 u4SBMask, UINT4 u4NextHop,
                          UINT4 u4StaticFlag);

INT1
MplsCliShowIpv6StaticBinding (tCliHandle CliHandle, UINT4 u4Flag,
                                tGenU4Addr SBPrefix, UINT4 u4SBMask,
                                tGenU4Addr NextHop, UINT4 u4StaticFlag);

INT1
MplsCliShowStaticCrossConnect (tCliHandle CliHandle, UINT4 u4Flag,
                               UINT4 u4LowLabel, UINT4 u4HighLabel, 
                               UINT4 u4StaticFlag);
INT1
MplsCliStaticXC (tCliHandle CliHandle, UINT4 u4InIfIndex, UINT4 u4InLabel,
                                  tGenU4Addr NextHop, UINT4 u4OutLabel,
                                  UINT4 u4OutIfIndex);
INT1
MplsCliNoStaticXC (tCliHandle CliHandle, UINT4 u4InLabel,
                                        tGenU4Addr u4NextHop, 
                                        UINT4 u4OutLabel);
INT1
MplsCliGetOrCreateFTNEntry (tGenU4Addr Prefix, UINT4 u4Mask,
                            UINT4 *pu4FtnIndex, BOOL1 * bFtnEntryExists,
                            INT4 i4FtnActType, UINT4 *pu4ErrorCode);


INT1 MplsCliIngressFTNEntry (tSNMP_OCTET_STRING_TYPE *OutSegmentIndex,
                             UINT4 u4OutLabel,
                             UINT4 u4NextHop,
                             BOOL1 bFtnEntryExists);

INT1 MplsCliSignallingCaps (tCliHandle CliHandle, UINT1 u1SigCaps);
INT1 MplsCliTunnelModel (tCliHandle CliHandle, UINT1 u1TnlMdl);
INT1 MplsCliTTLValue    (tCliHandle CliHandle, INT4 u4TTLval);
INT1 MplsCliSetMaxWaitTimers (tCliHandle CliHandle, UINT1 u1Flag, 
                              INT4 i4GrMaxWaitTime);
INT4 MplsCliShowTunnelMode (tCliHandle CliHandle);
INT4 MplsCliShowTTL (tCliHandle CliHandle);

INT1
MplsCliSetAchCodePoint (tCliHandle CliHandle, INT4 i4AchCodePoint);

/* show running related prototypes */
INT4
MplsShowRunningConfig (tCliHandle CliHandle);

INT4
MplsStaticBindingShowRunningConfig(tCliHandle CliHandle);

INT1
MplsStaticCrossConnectShowRunningConfig(tCliHandle CliHandle);

INT4
MplsTunnelShowRunningConfig(tCliHandle CliHandle);

INT4
MplsLspReoptimizationRunningConfig (tCliHandle CliHandle);

INT4
RpteCliRsvpTeShowRunnningConfig(tCliHandle CliHandle);

INT4
RpteCliRsvpTeGenShowRunningConfig(tCliHandle CliHandle);

INT4
RpteCliRsvpTeDebugShowRunningConfig(tCliHandle CliHandle);

INT4
RpteCliRsvpTeGenSubDebugSRC (tCliHandle CliHandle);

INT4
RpteCliRsvpTeIfShowRunningConfig(tCliHandle CliHandle);

INT4
RpteFrrFacShowRunningConfig (tCliHandle CliHandle);

INT4
LdpCliShowRunningConfig(tCliHandle CliHandle);

INT1
MplsTTLShowRunningConfig(tCliHandle CliHandle);

INT1
MplsTnlmodelShowRunningConfig(tCliHandle CliHandle);

INT4
LdpCliFsMplsShowRunningConfig (tCliHandle CliHandle);


INT4
LdpCliEntityShowRunningConfig (tCliHandle CliHandle);


INT4
LdpCliEntityGLRShowRunningConfig (tCliHandle CliHandle,
                                  tSNMP_OCTET_STRING_TYPE *pLdpEntityLdpId,
                                  UINT4 u4LdpEntityIndex);


INT4 
MplsVfiShowRunningConfig (tCliHandle);

INT4 
MplsVpwsShowRunningConfig (tCliHandle, INT4, UINT4, INT4, UINT4, UINT4,
                           UINT1 *, UINT1 *);
void
MplsTunnelShowEROConfig (tCliHandle , UINT4 , UINT4, UINT4, UINT4, UINT4);

INT4 
MplsPwXConnectShowRunningConfig (tCliHandle, INT4, UINT4, INT4, UINT1 *, UINT1 *);

INT4 MplsDsTeShowRunningConfig (tCliHandle CliHandle);

VOID MplsPwGlobalShowRunningConfig (tCliHandle CliHandle);

INT4
MplsL2vpnShow(tCliHandle CliHadle);

INT4 MplsAdmnStatGet(INT4 *);

UINT4
L2vpnCliGetCxtId (VOID);

extern INT1
nmhGetIfAlias (INT4 i4IfIndex, tSNMP_OCTET_STRING_TYPE * pRetValIfAlias);
extern INT1
nmhTestv2IfMainRowStatus (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                          INT4 i4TestValIfMainRowStatus);
extern INT1
nmhSetIfStackStatus (INT4 i4IfStackHigherLayer, INT4 i4IfStackLowerLayer,
                     INT4 i4SetValIfStackStatus);
extern INT1
nmhSetIfMainRowStatus (INT4 i4IfMainIndex, INT4 i4SetValIfMainRowStatus);

extern INT1
nmhSetIfMainStorageType (INT4 i4IfMainIndex, INT4 i4StorageType);

extern INT4
CfaValidateCfaIfIndex (UINT2 u2IfIndex);
extern INT1
nmhTestv2IfMainType (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                                          INT4 i4TestValIfMainType);
extern INT1
nmhSetIfMainType (INT4 i4IfMainIndex, INT4 i4SetValIfMainType);
extern INT1
nmhTestv2IfMainAdminStatus (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                            INT4 i4TestValIfMainAdminStatus);
extern INT1
nmhSetIfMainAdminStatus (INT4 i4IfMainIndex, 
                         INT4 i4SetValIfMainAdminStatus);
extern INT1
nmhTestv2IfStackStatus (UINT4 *pu4ErrorCode, INT4 i4IfStackHigherLayer,
                        INT4 i4IfStackLowerLayer,
                        INT4 i4TestValIfStackStatus);
extern INT4
CfaIfmAddDynamicStackEntry (UINT2 u2IfStackHigherLayer,
                            UINT2 u2IfStackLowerLayer);

#define MPLS_TUNNEL_MODE       "config_if_"
#define MPLS_TUNNEL_LSP_MODE   "config_if_lsp"
#define MPLS_TUNNEL_LSP_PROMPT   "(config-if-lsp)#"
#define MPLS_VFI_MODE          "mpls_vfi_"

#define MPLS_TUNNEL_ELSP_INFO_PHB_MAP_MODE "config_elsp_info_phb"
#define MPLS_TUNNEL_ELSP_INFO_PHB_MAP_PROMPT "(config-elsp-info-phb)#"

#define MPLS_TUNNEL_ELSP_MODE     "config_if_elsp"
#define MPLS_TUNNEL_ELSP_PROMPT   "(config-if-elsp)#"

#define MPLS_TUNNEL_LLSP_MODE     "config_if_llsp"
#define MPLS_TUNNEL_LLSP_PROMPT   "(config-if-llsp)#"

#define MPLS_TUNNEL_CLASS_MODE     "config_class"
#define MPLS_TUNNEL_CLASS_PROMPT   "(config-class)#"

#define MPLS_TUNNEL_EXP_PHB_MAP_MODE  "config_exp_phb_map"
#define MPLS_TUNNEL_EXP_PHB_MAP_PROMPT "(config-exp-phb-map)#"

#define MPLS_OUI_VPN_ID  "Ari"

#define MPLS_TE_ATTR_MODE            "config_lsp_attr"
#define MPLS_TE_ATTR_PROMPT          "(config-lsp-attr)#"
#define MPLS_TE_EXPLICIT_PATH_MODE   "cfg_ip_expl_path"
#define MPLS_TE_EXPLICIT_PATH_PROMPT "(cfg-ip-expl-path)#"
#define MPLS_TUNNEL_PROMPT     "(config-if)#"

INT1
MplsGetMplsTunnelCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

INT1
MplsGetMplsTunnelLspCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

INT1
MplsGetMplsTunnelElspCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);
INT1
MplsGetMplsTunnelLlspCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);
INT1
MplsGetMplsTunnelClassCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);
INT1
MplsGetMplsTunnelExpToPhbMapCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

INT1
MplsGetMplsTunnelElspInfoPhbMapCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

INT1
TeGetExplicitPathCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

INT1
TeGetAttrCfgPrompt ARG_LIST ((INT1 *pi1ModeName, INT1 *pi1DispStr));

/*--------------------------------LDP-MODULE----------------------------------*/

#define  LDP_CLI_MAX_ARGS              8

/* LDP Entity cli related macros */
#define CLI_MPLS_LDP_ONE           1
#define CLI_MPLS_LDP_TWO           2
#define CLI_MPLS_LDP_FOUR          4
#define CLI_MPLS_LDP_EIGHT         8

#define CLI_MPLS_LDP_ZERO          0

#define CLI_MPLS_LDP_LABEL(i4LabelType)      (i4LabelType == 1) ? "Generic" \
                                              : "ATM"
#define CLI_MPLS_LDP_DIST(i4Mode)            (i4Mode == 1) ? "On Demand" \
                                              : "Unsolicited"
#define CLI_MPLS_LDP_RET(i4Mode)             (i4Mode == 1) ? "Conservative" \
                                              : "Liberal"
#define CLI_MPLS_LDP_ALLOC(i4Mode)           (i4Mode == 1) ? "Ordered" \
                                              : "Independent"

#define CLI_MPLS_LDP_SHOW_ADMIN_STATUS(i4Status)  (i4Status == 1) ? "Up" : \
                                                  "Down"

#define CLI_MPLS_LDP_PHP(i4Mode)            (i4Mode == 0) ? "Exp-Null" \
                                              : ( (i4Mode == 1) ? "Disabled" \
                                                  : "Imp-Null")
#define CLI_MPLS_LDP_SESSION_ROLE(i4Mode)   (i4Mode == 1) ? "UnKnown" \
                                              : ((i4Mode == 2) ? "active" \
                                                 : "passive")
#define CLI_MPLS_SESSION_STATE(i4State)     (i4State == 1) ? "Nonexistent" \
                                             : ((i4State == 2) ? "init" \
                                             : ((i4State == 3) ? "openrec" \
                                             : ((i4State == 4) ? "opensent" \
                                             : "oper")))
#define CLI_MPLS_LDP_ADJACENCY(i4AdjacencyType) (i4AdjacencyType == 1) ? \
                                           "Local Peer" : "Remote Peer"
                                           
#define LDP_MODE             "mpls_ldp_"
#define LDP_ENTITY_MODE      "mpls_ldp_entity_"

/* LDP Label Allocation Modes */
#define MPLS_LDP_ORDERED_MODE        1
#define MPLS_LDP_INDEPENDENT_MODE    2

/* LDP Label Distribution Modes */
#define MPLS_LDP_ON_DEMAND_MODE      1
#define MPLS_LDP_UNSOLICITED_MODE    2

/* LDP Label Retention Modes */
#define MPLS_LDP_CONSERVATIVE_MODE   1
#define MPLS_LDP_LIBERAL_MODE        2

/* LDP PHP Modes */
#define MPLS_LDP_EXPLICIT_NULL_LABEL  0
#define MPLS_LDP_IMPLICIT_NULL_LABEL  3

/* LDP Transport Address Type */
#define MPLS_LDP_INTERFACE_ADDRESS_TYPE  1
#define MPLS_LDP_LOOPBACK_ADDRESS_TYPE   2

#define MPLS_LDP_LOOP_DETECT_NOT_CAPABLE    1
#define MPLS_LDP_LOOP_DETECT_CAPABLE_OTHER  2
#define MPLS_LDP_LOOP_DETECT_CAPABLE_HC     3
#define MPLS_LDP_LOOP_DETECT_CAPABLE_PV     4
#define MPLS_LDP_LOOP_DETECT_CAPABLE_HCPV   5

/* Ldp Bfd status */
#ifdef MPLS_LDP_BFD_WANTED

#define LDP_BFD_ENABLED   1
#define LDP_BFD_DISABLED  2

#endif

/* LDP commands list */
enum 
{
    CLI_MPLS_LDP=1,
    CLI_MPLS_LDP_POPULATE_HW_LIST,
    CLI_MPLS_LDP_ENTITY,
    CLI_MPLS_LDP_NO_ENTITY,    
    CLI_MPLS_LDP_HELLO,
    CLI_MPLS_LDP_NO_HELLO,
    CLI_MPLS_LDP_KEEPALIVE,
    CLI_MPLS_LDP_NO_KEEPALIVE,
    CLI_MPLS_LDP_MAX_HOPS,
    CLI_MPLS_LDP_NO_MAX_HOPS,
    CLI_MPLS_LDP_TARGETED_HELLO,
    CLI_MPLS_LDP_NO_TARGETED_HELLO,
    CLI_MPLS_LDP_LABEL_DIST_METHOD,
    CLI_MPLS_LDP_NO_LABEL_DIST_METHOD,
    CLI_MPLS_LDP_LABEL_RETENTION_MODE,
    CLI_MPLS_LDP_NO_LABEL_RETENTION_MODE,
    CLI_MPLS_LDP_LABEL_ALLOCATION_MODE,
    CLI_MPLS_LDP_NO_LABEL_ALLOCATION_MODE,
    CLI_MPLS_LDP_LSR_ID,
    CLI_MPLS_LDP_PHP_MODE,
    CLI_MPLS_LDP_NO_PHP_MODE,
    CLI_MPLS_LDP_INTERFACE_ENABLE,
    CLI_MPLS_LDP_INTERFACE_DISABLE,
    CLI_MPLS_LDP_ENTITY_SHUT,
    CLI_MPLS_LDP_ENTITY_NO_SHUT,
    CLI_MPLS_LDP_TRANSPORT_ADDRESSIPV6,
    CLI_MPLS_LDP_TRANSPORT_ADDRESS,
    CLI_MPLS_LDP_DBG_ADVT,
    CLI_MPLS_LDP_DBG_INTERFACE,
    CLI_MPLS_LDP_DBG_MEM,
    CLI_MPLS_LDP_DBG_MESSAGE,
    CLI_MPLS_LDP_DBG_SESSION,
    CLI_MPLS_LDP_DBG_PRCS,
    CLI_MPLS_LDP_DBG_TIMER,
    CLI_MPLS_LDP_DBG_GRACEFUL_RESTART,
    CLI_MPLS_LDP_DBG_DUMP_MSGS,
    CLI_MPLS_LDP_LABEL_RANGE_SHOW,
    CLI_MPLS_LDP_DISCOVERY_SHOW,
    CLI_MPLS_LDP_NEIGHBOR_SHOW,
    CLI_MPLS_LDP_DATABASE_SHOW,
    CLI_MPLS_LDP_PARAM_SHOW,
    CLI_MPLS_LDP_POLICY,
    CLI_MPLS_LDP_GR_MODE,
    CLI_MPLS_LDP_GR_NO_MODE,
    CLI_MPLS_LDP_NBR_LIVENESS_TIMER,
    CLI_MPLS_LDP_FWD_HOLDING_TIMER,
    CLI_MPLS_LDP_RECOVERY_TIMER,
    CLI_MPLS_LDP_NO_NBR_LIVENESS_TIMER,
    CLI_MPLS_LDP_NO_FWD_HOLDING_TIMER,
    CLI_MPLS_LDP_NO_RECOVERY_TIMER,
    CLI_MPLS_LDP_GR_SHOW,
    CLI_MPLS_CONFIG_SEQ_TLV,
    CLI_MPLS_LDP_PATH_VECTOR,
 CLI_MPLS_LDP_IPV6_TRANSPORT_ADDRESS,

#ifdef MPLS_LDP_BFD_WANTED
    CLI_MPLS_LDP_BFD_STATUS
#endif 
};


/* Dumping messages */
enum
{
    CLI_MPLS_LDP_DUMP_ADDRESS=1,
    CLI_MPLS_LDP_DUMP_ALL,
    CLI_MPLS_LDP_DUMP_HELLO,
    CLI_MPLS_LDP_DUMP_INIT,
    CLI_MPLS_LDP_DUMP_KA,
    CLI_MPLS_LDP_DUMP_MAP,
    CLI_MPLS_LDP_DUMP_NOTIF,
    CLI_MPLS_LDP_DUMP_REL,
    CLI_MPLS_LDP_DUMP_RQ,
    CLI_MPLS_LDP_DUMP_WITHDRAW    
};
/* LDP Mode related Prompt prototypes */

INT4 cli_process_ldp_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command, ...));

INT1 MplsGetMplsLdpCfgPrompt ARG_LIST ((INT1 *pi1ModeName,  
                                      INT1 *pi1DispStr));
INT1 MplsGetMplsLdpEntityCfgPrompt ARG_LIST ((INT1 *pi1ModeName, 
                                     INT1 *pi1DispStr));

/************** OAM-MODULE START ***********************/

#define MPLS_MEG_MODE          "meg-cfg"
#define MPLS_MEP_MODE          "mep-cfg"

#define OAM_CLI_MAX_ARGS       20

#define CLI_OAM_DEFAULT_CXT_ID  0

extern UINT4 MplsTunnelName [13];

enum
{
CLI_MPLS_SHOW_NODE_MAP_INFO_GID_NID,
CLI_MPLS_SHOW_NODE_MAP_INFO_LCL_MAP_NUM,
CLI_MPLS_DISPLAY_SIMPLE,
CLI_MPLS_DISPLAY_DETAIL,
CLI_MPLS_OAM_SHOW_MEG_ALL,
CLI_MPLS_OAM_SHOW_MEG_NAME,
CLI_MPLS_OAM_SHOW_SERVICE_ALL,
CLI_MPLS_OAM_SHOW_MEG_NAME_SERVICE_NAME,
CLI_MPLS_OAM_SHOW_MEG_NAME_ALL,
CLI_MPLS_OAM_MP_DISPLAY_MODE_SIMPLE,
CLI_MPLS_OAM_MP_DISPLAY_MODE_DETAIL,
CLI_MPLS_SHOW_NODE_MAP_INFO_ALL,
CLI_MPLS_SHOW_GLOBAL_INFO
};

enum
{
    CLI_OAM_FSMPLSTPGLOBALCONFIGTABLE = 0,
    CLI_OAM_FSMPLSTPNODEMAPTABLE,
    CLI_OAM_FSMPLSTPMEGTABLE,
    CLI_OAM_FSMPLSTPMETABLE
};

enum
{
    CLI_OAM_UNKNOWN_ERROR = CLI_ERR_START_ID_MPLS_OAM,
    CLI_OAM_ENTRY_NOT_FOUND,
    CLI_OAM_INVALID_GLOBAL_ID,
    CLI_OAM_INVALID_ICC_ID,
    CLI_OAM_INVALID_NODE_ID,
    CLI_OAM_INVALID_UMC_ID,
    CLI_OAM_MP_LOCATION_NOT_SUPPORTED,
    CLI_OAM_INVALID_MEP_INDEX,
    CLI_OAM_MEP_DIRECTION_NOT_SUPPORTED,
    CLI_OAM_INVALID_PHB_VALUE,
    CLI_OAM_INVALID_TRACE_VALUE,
    CLI_OAM_INVALID_LOCAL_MAP_NUM,
    CLI_OAM_INVALID_SERVICE_TYPE,
    CLI_OAM_DEL_MEG_FAILED_ME_EXISTS,
    CLI_OAM_LOCAL_MAP_NUM_ENTRY_EXISTS,
    CLI_OAM_GLOBAL_NODE_ID_ENTRY_EXISTS,
    CLI_OAM_INVALID_SERVICE_INDEX,
    CLI_OAM_MEP_INDEX_NOT_CONFIGURED,
    CLI_OAM_MEG_ROWSTATUS_NOT_ACTIVE,
    CLI_OAM_SERVICE_NOT_EXIST,
    CLI_OAM_ELPS_PROACTIVE_SESSION_EXISTS,
    CLI_OAM_ELPS_ASSOCIATION_EXISTS,
    CLI_OAM_PROACTIVE_SESSION_EXISTS,
    CLI_OAM_ACTIVE_ME_EXISTS,
    CLI_OAM_TUNNEL_ASSOC_WITH_Y1731,
    CLI_OAM_MAX_ERR
};

#define  CLI_OAM_FN_ENTRY_EXIT_TRC       0x100000
#define  CLI_OAM_MGMT_TRC                0x010000
#define  CLI_OAM_MAIN_TRC                0x001000
#define  CLI_OAM_UTIL_TRC                0x000100
#define  CLI_OAM_RESOURCE_TRC            0x000010
#define  CLI_OAM_ALL_FAIL_TRC            0x000001
#define  CLI_OAM_ALL_TRC                 CLI_OAM_FN_ENTRY_EXIT_TRC |\
                                         CLI_OAM_MGMT_TRC  |\
                                         CLI_OAM_MAIN_TRC |\
                                         CLI_OAM_UTIL_TRC  |\
                                         CLI_OAM_RESOURCE_TRC |\
                                         CLI_OAM_ALL_FAIL_TRC

/* Accessing the ErrString with ErrCode value.
 * Since the ErrCode's starting Id is changed, this forumla is used
 * to access the Err String */
#define CLI_ERR_OFFSET_MPLS_OAM(u4ErrCode) \
        (u4ErrCode - CLI_ERR_START_ID_MPLS_OAM)

#ifdef __OAMCLI_C__
CONST CHR1  *OamCliErrString [] = {
    "%% Unknown Error\r\n\n",
    "%% Entry Not Found\r\n\n",
    "%% Invalid Global Id\r\n\n",
    "%% Invalid ICC Id. ICC Id should be 1 to 6 characters, each character being uppercase alphabet or numeric.\r\n\n",
    "%% Invalid Node Id\r\n\n",
    "%% Invalid UMC Id. UMC Id should be 1 to 7 characters, each character being uppercase alphabet or numeric.\r\n\n",
    "%% Invalid MP Location. Currently Per-node only Supported.\r\n\n",
    "%% Invalid MEP Index\r\n\n",
    "%% Invalid MEP Direction. Currently Down direction only Supported.\r\n\n",
    "%% Invalid PHB Value\r\n\n",
    "%% Invalid Trace Value\r\n\n",
    "%% Invalid Local Map Number\r\n\n",
    "%% Invalid Service type. Currently service types LSP and PW only supported.\r\n\n",
    "%% Cannot delete MEG entry. Associated ME entry exists.\r\n\n",
    "%% Entry already exists for Local Map Number.\r\n\n",
    "%% Entry already exists for Global ID and Node Id.\r\n\n",
    "%% Invalid service Index.\r\n\n",
    "%% MEP indices not configured.\r\n\n",
    "%% MEG not Active.\r\n\n",
    "%% Service entry does not exist.\r\n\n",
    "%% ELPS and Proactive Session exists.\r\n\n",
    "%% ELPS Association exists.\r\n\n",
    "%% Proactive Session exists.\r\n\n",
    "%% ACTIVE ME exists.\r\n\n",
    "%% The tunnel is already associated with Y1731 OAM \r\n"
    "\r\n"
};
#endif

#ifdef MPLS_TEST_WANTED
INT4
cli_process_mplstp_test_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command,...));
INT4
cli_process_vccv_test_cmd (tCliHandle CliHandle, UINT4 u4Command,...);
INT4
cli_process_mpls_p2mp_test_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command,...));
INT4
cli_process_vplsads_test_cmd (tCliHandle CliHandle, UINT4 u4Command,...);
INT4
cli_process_ldp_gr_test_cmd (tCliHandle CliHandle, UINT4 u4Command,...);
INT4
cli_process_ldp_ha_test_cmd (tCliHandle CliHandle, UINT4 u4Command,...);
INT4
cli_process_tldp_pw_gr_test_cmd(tCliHandle CliHandle, UINT4 u4Command,...);
INT4
cli_process_l2vpn_ha_test_cmd(tCliHandle CliHandle, UINT4 u4Command,...);
INT4
cli_process_ldp_test_cmd (tCliHandle CliHandle, UINT4 u4Command,...);
#endif

INT4 cli_process_oam_show_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Comamnd, ...));

INT4 cli_process_Oam_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command, ...));

INT1 MplsGetMplsOamMegPrompt ARG_LIST ((INT1 *pi1ModeName, INT1 *pi1DispStr));

INT4 OamCliChangeMegMode PROTO ((tCliHandle CliHandle));

INT4 OamUtilGetMegIndexFromMegName PROTO ((UINT4 u4ContextId, UINT1 *pu1MegName,
                                           UINT4 *pu4MegIndex));

INT4 OamCliGetMegIndexFromMegName PROTO ((UINT4 u4ContextId, UINT1 *pu1MegName,
                                           UINT4 *pu4MegIndex));

INT4
OamUtilGetMeRowStatus PROTO ((UINT4 u4ContextId, UINT4 u4MegIndex,
                             UINT1 u4MeIndex, UINT4 u4MpIndex,
                             UINT4 *pu4MeRowStatus));
INT4
OamUtilGetMegRowStatus PROTO ((UINT4 u4ContextId, UINT4 u4MegIndex,
                               UINT4 *pu4MegRowStatus));

INT4 OamUtilGetMeIndexFromMegIndex PROTO ((UINT4 u4ContextId, UINT4 u4MegIndex,
                                           UINT1 *pu1MeName, UINT4 *pu4MeIndex,
                                           UINT4 *pu4MpIndex));

VOID OamCliSetMegIndex PROTO ((tCliHandle CliHandle, UINT4 u4ContextId, 
                                                   UINT4 u4MegIndex));

VOID OamCliGetMegIndex PROTO ((tCliHandle CliHandle, UINT4 *pu4ContextId, 
                                                   UINT4 *pu4MegIndex));

INT4 OamCliGetCxtIdFromCxtName PROTO ((tCliHandle CliHandle, UINT1 *pu1CxtName,
                                       UINT4 *pu4ContextId));
UINT4 OamCliGetFreeMegId PROTO ((VOID));
UINT4 OamCliGetFreeMeId PROTO ((UINT4 u4ContextId, UINT4 u4MegIndex));
UINT4 OamCliGetFreeMpId PROTO ((UINT4 u4ContextId, UINT4 u4MegIndex, 
                                UINT4 u4MeIndex));

INT4 OamCliGetPwIndexFromVcId PROTO ((UINT4 u4VcId, UINT4 *pu4PwIndex));

INT4 CliGetDebugLevel PROTO ((tCliHandle CliHandle, UINT4 u4ContextId, 
                              UINT4 *pu4DebugLevel));
extern INT1
nmhTestv2IfAlias (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex,
                  tSNMP_OCTET_STRING_TYPE * pSetValIfAlias);
extern INT1
nmhSetIfAlias (INT4 i4IfIndex, tSNMP_OCTET_STRING_TYPE * pRetValIfAlias);
/************** OAM-MODULE END ***********************/
/*MS-PW */

enum
{
 CLI_L2VPN_FSMSPWMAXENTRIES,
 CLI_L2VPN_FSMSPWCONFIGTABLE_CREATE,
        CLI_L2VPN_FSMSPWCONFIGTABLE_DESTROY,
        CLI_L2VPN_FSMSPWCONFIGTABLE_SHOW,
};
enum
{
    CLI_L2VPN_ERR_PWVC_NOT_FOUND=1,
    CLI_L2VPN_ERR_MSPW_CONFIG=2,
    CLI_L2VPN_MAX_ERR 
};

#ifdef LDP_TEST_WANTED
enum
{
    LDP_TEST_TCP_OPEN = 1,
    LDP_TEST_TCP_CLOSE = 2,
    LDP_TEST_TCP_SEND = 3
};

INT4 cli_process_ldp_tst_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);
#endif

INT4 cli_process_MSPW_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);

INT4
MplsCommonCliSetDebugLevel (tCliHandle CliHandle, INT4 i4CliDebugLevel);

/*MS-PW */

#ifdef FM_WANTED
#ifdef MPLS_WANTED
VOID IssMplsFmShowDebugging (tCliHandle CliHandle);
#endif
#endif

#endif /* __MPLSCLI_H__ */
