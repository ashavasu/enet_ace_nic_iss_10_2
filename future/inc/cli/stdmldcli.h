/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdmldcli.h,v 1.3 2010/12/22 11:44:47 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 MldInterfaceIfIndex[11];
extern UINT4 MldInterfaceQueryInterval[11];
extern UINT4 MldInterfaceStatus[11];
extern UINT4 MldInterfaceVersion[11];
extern UINT4 MldInterfaceQueryMaxResponseDelay[11];
extern UINT4 MldInterfaceRobustness[11];
extern UINT4 MldInterfaceLastListenQueryIntvl[11];
extern UINT4 MldInterfaceProxyIfIndex[11];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 MldCacheAddress[11];
extern UINT4 MldCacheIfIndex[11];
extern UINT4 MldCacheSelf[11];
extern UINT4 MldCacheStatus[11];
