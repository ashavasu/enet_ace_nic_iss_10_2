/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsrmon2cli.h,v 1.3 2013/10/23 09:35:25 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsRmon2Trace[10];
extern UINT4 FsRmon2AdminStatus[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsRmon2Trace(u4SetValFsRmon2Trace) \
    nmhSetCmnWithLock(FsRmon2Trace, 10, FsRmon2TraceSet, Rmon2MainLock, Rmon2MainUnLock, 0, 0, 0, "%u", u4SetValFsRmon2Trace)
#define nmhSetFsRmon2AdminStatus(i4SetValFsRmon2AdminStatus) \
    nmhSetCmnWithLock(FsRmon2AdminStatus, 10, FsRmon2AdminStatusSet, Rmon2MainLock, Rmon2MainUnLock, 0, 0, 0, "%i", i4SetValFsRmon2AdminStatus)
#endif
