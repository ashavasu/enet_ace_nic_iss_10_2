#ifndef __POECLI_H__
#define __POECLI_H__

#include "lr.h"
#include "cli.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include "snmccons.h"

#define POE_CLI_STATE                     1
#define POE_ADD_PD_MAC                    2
#define POE_DEL_PD_MAC                    3
#define POE_CLI_INT_STATE                 4
#define POE_PORT_PRIORITY                 5
#define SHOW_PSE_STATUS                   6
#define SHOW_ALL_POE_INTERFACE            7
#define SHOW_POE_INTERFACE                8
#define SHOW_POE_MAC_LIST_INFO            9

#define ADD_PD_MAC                        1
#define DEL_PD_MAC                        2

#define POE_CLI_CRITICAL                  1
#define POE_CLI_HIGH                      2
#define POE_CLI_LOW                       3

#define POE_CLI_MAX_COMMANDS              8

INT4 cli_process_poe_cmd    PROTO ((tCliHandle, UINT4, ...));

/* Error codes 
 * Error code values and strings are maintained inside the protocol module itself.
 */
enum {
    CLI_POE_MAC_ADDR_ERR = 1,
    CLI_POE_PORT_ENABLE_ERR,
    CLI_POE_PORT_PRIORITY_ERR,
    CLI_POE_PD_MAC_LEARNT_ERR,
    CLI_POE_MAC_ADDR_EXIST_ERR,
    CLI_POE_NO_PD_MAC_ERR,
    CLI_POE_MAX_ERR
};
/* The error strings should be places under the switch so as to avoid redifinition
 * This will be visible only in modulecli.c file
 */
#ifdef __POECLI_C__

CONST CHR1  *PoeCliErrString [] = {
    NULL,
    "% Broadcast address is not supported\r\n",
    "% POE is not enabled on this port\r\n",
    "% Not sufficient power available to set priority to high\r\n",
    "% This powered device MAC address is not yet learnt\r\n",
    "% This powered device MAC address is already existing\r\n",
    "% This powered device MAC address is not existing\r\n"
};

#endif /* __POECLI_C__ */

#endif /* __POECLI_H__ */
