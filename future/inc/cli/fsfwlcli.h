/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsfwlcli.h,v 1.1 2011/10/25 12:10:28 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FwlGlobalMasterControlSwitch[10];
extern UINT4 FwlGlobalICMPControlSwitch[10];
extern UINT4 FwlGlobalIpSpoofFiltering[10];
extern UINT4 FwlGlobalSrcRouteFiltering[10];
extern UINT4 FwlGlobalTinyFragmentFiltering[10];
extern UINT4 FwlGlobalTcpIntercept[10];
extern UINT4 FwlGlobalTrap[10];
extern UINT4 FwlGlobalTrace[10];
extern UINT4 FwlGlobalDebug[10];
extern UINT4 FwlGlobalUrlFiltering[10];
extern UINT4 FwlGlobalNetBiosFiltering[10];
extern UINT4 FwlGlobalNetBiosLan2Wan[10];
extern UINT4 FwlGlobalICMPv6ControlSwitch[10];
extern UINT4 FwlGlobalIpv6SpoofFiltering[10];
extern UINT4 FwlGlobalLogFileSize[10];
extern UINT4 FwlGlobalLogSizeThreshold[10];
extern UINT4 FwlGlobalIdsLogSize[10];
extern UINT4 FwlGlobalIdsLogThreshold[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFwlGlobalMasterControlSwitch(i4SetValFwlGlobalMasterControlSwitch) \
 nmhSetCmnWithLock(FwlGlobalMasterControlSwitch, 10, FwlGlobalMasterControlSwitchSet, FwlLock, FwlUnLock, 0, 0, 0, "%i", i4SetValFwlGlobalMasterControlSwitch)
#define nmhSetFwlGlobalICMPControlSwitch(i4SetValFwlGlobalICMPControlSwitch) \
 nmhSetCmnWithLock(FwlGlobalICMPControlSwitch, 10, FwlGlobalICMPControlSwitchSet, FwlLock, FwlUnLock, 0, 0, 0, "%i", i4SetValFwlGlobalICMPControlSwitch)
#define nmhSetFwlGlobalIpSpoofFiltering(i4SetValFwlGlobalIpSpoofFiltering) \
 nmhSetCmnWithLock(FwlGlobalIpSpoofFiltering, 10, FwlGlobalIpSpoofFilteringSet, FwlLock, FwlUnLock, 0, 0, 0, "%i", i4SetValFwlGlobalIpSpoofFiltering)
#define nmhSetFwlGlobalSrcRouteFiltering(i4SetValFwlGlobalSrcRouteFiltering) \
 nmhSetCmnWithLock(FwlGlobalSrcRouteFiltering, 10, FwlGlobalSrcRouteFilteringSet, FwlLock, FwlUnLock, 1, 0, 0, "%i", i4SetValFwlGlobalSrcRouteFiltering)
#define nmhSetFwlGlobalTinyFragmentFiltering(i4SetValFwlGlobalTinyFragmentFiltering) \
 nmhSetCmnWithLock(FwlGlobalTinyFragmentFiltering, 10, FwlGlobalTinyFragmentFilteringSet, FwlLock, FwlUnLock, 1, 0, 0, "%i", i4SetValFwlGlobalTinyFragmentFiltering)
#define nmhSetFwlGlobalTcpIntercept(i4SetValFwlGlobalTcpIntercept) \
 nmhSetCmnWithLock(FwlGlobalTcpIntercept, 10, FwlGlobalTcpInterceptSet, FwlLock, FwlUnLock, 0, 0, 0, "%i", i4SetValFwlGlobalTcpIntercept)
#define nmhSetFwlGlobalTrap(i4SetValFwlGlobalTrap) \
 nmhSetCmnWithLock(FwlGlobalTrap, 10, FwlGlobalTrapSet, FwlLock, FwlUnLock, 0, 0, 0, "%i", i4SetValFwlGlobalTrap)
#define nmhSetFwlGlobalTrace(i4SetValFwlGlobalTrace) \
 nmhSetCmnWithLock(FwlGlobalTrace, 10, FwlGlobalTraceSet, FwlLock, FwlUnLock, 0, 0, 0, "%i", i4SetValFwlGlobalTrace)
#define nmhSetFwlGlobalDebug(i4SetValFwlGlobalDebug) \
 nmhSetCmnWithLock(FwlGlobalDebug, 10, FwlGlobalDebugSet, FwlLock, FwlUnLock, 0, 0, 0, "%i", i4SetValFwlGlobalDebug)
#define nmhSetFwlGlobalUrlFiltering(i4SetValFwlGlobalUrlFiltering) \
 nmhSetCmnWithLock(FwlGlobalUrlFiltering, 10, FwlGlobalUrlFilteringSet, FwlLock, FwlUnLock, 0, 0, 0, "%i", i4SetValFwlGlobalUrlFiltering)
#define nmhSetFwlGlobalNetBiosFiltering(i4SetValFwlGlobalNetBiosFiltering) \
 nmhSetCmnWithLock(FwlGlobalNetBiosFiltering, 10, FwlGlobalNetBiosFilteringSet, FwlLock, FwlUnLock, 0, 0, 0, "%i", i4SetValFwlGlobalNetBiosFiltering)
#define nmhSetFwlGlobalNetBiosLan2Wan(i4SetValFwlGlobalNetBiosLan2Wan) \
 nmhSetCmnWithLock(FwlGlobalNetBiosLan2Wan, 10, FwlGlobalNetBiosLan2WanSet, FwlLock, FwlUnLock, 0, 0, 0, "%i", i4SetValFwlGlobalNetBiosLan2Wan)
#define nmhSetFwlGlobalICMPv6ControlSwitch(i4SetValFwlGlobalICMPv6ControlSwitch) \
 nmhSetCmnWithLock(FwlGlobalICMPv6ControlSwitch, 10, FwlGlobalICMPv6ControlSwitchSet, FwlLock, FwlUnLock, 0, 0, 0, "%i", i4SetValFwlGlobalICMPv6ControlSwitch)
#define nmhSetFwlGlobalIpv6SpoofFiltering(i4SetValFwlGlobalIpv6SpoofFiltering) \
 nmhSetCmnWithLock(FwlGlobalIpv6SpoofFiltering, 10, FwlGlobalIpv6SpoofFilteringSet, FwlLock, FwlUnLock, 0, 0, 0, "%i", i4SetValFwlGlobalIpv6SpoofFiltering)
#define nmhSetFwlGlobalLogFileSize(u4SetValFwlGlobalLogFileSize) \
 nmhSetCmnWithLock(FwlGlobalLogFileSize, 10, FwlGlobalLogFileSizeSet, FwlLock, FwlUnLock, 0, 0, 0, "%u", u4SetValFwlGlobalLogFileSize)
#define nmhSetFwlGlobalLogSizeThreshold(u4SetValFwlGlobalLogSizeThreshold) \
 nmhSetCmnWithLock(FwlGlobalLogSizeThreshold, 10, FwlGlobalLogSizeThresholdSet, FwlLock, FwlUnLock, 0, 0, 0, "%u", u4SetValFwlGlobalLogSizeThreshold)
#define nmhSetFwlGlobalIdsLogSize(u4SetValFwlGlobalIdsLogSize) \
 nmhSetCmnWithLock(FwlGlobalIdsLogSize, 10, FwlGlobalIdsLogSizeSet, FwlLock, FwlUnLock, 0, 0, 0, "%u", u4SetValFwlGlobalIdsLogSize)
#define nmhSetFwlGlobalIdsLogThreshold(u4SetValFwlGlobalIdsLogThreshold) \
 nmhSetCmnWithLock(FwlGlobalIdsLogThreshold, 10, FwlGlobalIdsLogThresholdSet, FwlLock, FwlUnLock, 0, 0, 0, "%u", u4SetValFwlGlobalIdsLogThreshold)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FwlDefnTcpInterceptThreshold[10];
extern UINT4 FwlDefnInterceptTimeout[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFwlDefnTcpInterceptThreshold(i4SetValFwlDefnTcpInterceptThreshold) \
 nmhSetCmnWithLock(FwlDefnTcpInterceptThreshold, 10, FwlDefnTcpInterceptThresholdSet, FwlLock, FwlUnLock, 0, 0, 0, "%i", i4SetValFwlDefnTcpInterceptThreshold)
#define nmhSetFwlDefnInterceptTimeout(u4SetValFwlDefnInterceptTimeout) \
 nmhSetCmnWithLock(FwlDefnInterceptTimeout, 10, FwlDefnInterceptTimeoutSet, FwlLock, FwlUnLock, 0, 0, 0, "%t", u4SetValFwlDefnInterceptTimeout)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FwlFilterFilterName[12];
extern UINT4 FwlFilterSrcAddress[12];
extern UINT4 FwlFilterDestAddress[12];
extern UINT4 FwlFilterProtocol[12];
extern UINT4 FwlFilterSrcPort[12];
extern UINT4 FwlFilterDestPort[12];
extern UINT4 FwlFilterAckBit[12];
extern UINT4 FwlFilterRstBit[12];
extern UINT4 FwlFilterTos[12];
extern UINT4 FwlFilterAccounting[12];
extern UINT4 FwlFilterHitClear[12];
extern UINT4 FwlFilterAddrType[12];
extern UINT4 FwlFilterFlowId[12];
extern UINT4 FwlFilterDscp[12];
extern UINT4 FwlFilterRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFwlFilterSrcAddress(pFwlFilterFilterName ,pSetValFwlFilterSrcAddress) \
 nmhSetCmnWithLock(FwlFilterSrcAddress, 12, FwlFilterSrcAddressSet, FwlLock, FwlUnLock, 0, 0, 1, "%s %s", pFwlFilterFilterName ,pSetValFwlFilterSrcAddress)
#define nmhSetFwlFilterDestAddress(pFwlFilterFilterName ,pSetValFwlFilterDestAddress) \
 nmhSetCmnWithLock(FwlFilterDestAddress, 12, FwlFilterDestAddressSet, FwlLock, FwlUnLock, 0, 0, 1, "%s %s", pFwlFilterFilterName ,pSetValFwlFilterDestAddress)
#define nmhSetFwlFilterProtocol(pFwlFilterFilterName ,i4SetValFwlFilterProtocol) \
 nmhSetCmnWithLock(FwlFilterProtocol, 12, FwlFilterProtocolSet, FwlLock, FwlUnLock, 0, 0, 1, "%s %i", pFwlFilterFilterName ,i4SetValFwlFilterProtocol)
#define nmhSetFwlFilterSrcPort(pFwlFilterFilterName ,pSetValFwlFilterSrcPort) \
 nmhSetCmnWithLock(FwlFilterSrcPort, 12, FwlFilterSrcPortSet, FwlLock, FwlUnLock, 0, 0, 1, "%s %s", pFwlFilterFilterName ,pSetValFwlFilterSrcPort)
#define nmhSetFwlFilterDestPort(pFwlFilterFilterName ,pSetValFwlFilterDestPort) \
 nmhSetCmnWithLock(FwlFilterDestPort, 12, FwlFilterDestPortSet, FwlLock, FwlUnLock, 0, 0, 1, "%s %s", pFwlFilterFilterName ,pSetValFwlFilterDestPort)
#define nmhSetFwlFilterAckBit(pFwlFilterFilterName ,i4SetValFwlFilterAckBit) \
 nmhSetCmnWithLock(FwlFilterAckBit, 12, FwlFilterAckBitSet, FwlLock, FwlUnLock, 1, 0, 1, "%s %i", pFwlFilterFilterName ,i4SetValFwlFilterAckBit)
#define nmhSetFwlFilterRstBit(pFwlFilterFilterName ,i4SetValFwlFilterRstBit) \
 nmhSetCmnWithLock(FwlFilterRstBit, 12, FwlFilterRstBitSet, FwlLock, FwlUnLock, 1, 0, 1, "%s %i", pFwlFilterFilterName ,i4SetValFwlFilterRstBit)
#define nmhSetFwlFilterTos(pFwlFilterFilterName ,i4SetValFwlFilterTos) \
 nmhSetCmnWithLock(FwlFilterTos, 12, FwlFilterTosSet, FwlLock, FwlUnLock, 0, 0, 1, "%s %i", pFwlFilterFilterName ,i4SetValFwlFilterTos)
#define nmhSetFwlFilterAccounting(pFwlFilterFilterName ,i4SetValFwlFilterAccounting) \
 nmhSetCmnWithLock(FwlFilterAccounting, 12, FwlFilterAccountingSet, FwlLock, FwlUnLock, 0, 0, 1, "%s %i", pFwlFilterFilterName ,i4SetValFwlFilterAccounting)
#define nmhSetFwlFilterHitClear(pFwlFilterFilterName ,i4SetValFwlFilterHitClear) \
 nmhSetCmnWithLock(FwlFilterHitClear, 12, FwlFilterHitClearSet, FwlLock, FwlUnLock, 0, 0, 1, "%s %i", pFwlFilterFilterName ,i4SetValFwlFilterHitClear)
#define nmhSetFwlFilterAddrType(pFwlFilterFilterName ,i4SetValFwlFilterAddrType) \
 nmhSetCmnWithLock(FwlFilterAddrType, 12, FwlFilterAddrTypeSet, FwlLock, FwlUnLock, 0, 0, 1, "%s %i", pFwlFilterFilterName ,i4SetValFwlFilterAddrType)
#define nmhSetFwlFilterFlowId(pFwlFilterFilterName ,u4SetValFwlFilterFlowId) \
 nmhSetCmnWithLock(FwlFilterFlowId, 12, FwlFilterFlowIdSet, FwlLock, FwlUnLock, 0, 0, 1, "%s %u", pFwlFilterFilterName ,u4SetValFwlFilterFlowId)
#define nmhSetFwlFilterDscp(pFwlFilterFilterName ,i4SetValFwlFilterDscp) \
 nmhSetCmnWithLock(FwlFilterDscp, 12, FwlFilterDscpSet, FwlLock, FwlUnLock, 0, 0, 1, "%s %i", pFwlFilterFilterName ,i4SetValFwlFilterDscp)
#define nmhSetFwlFilterRowStatus(pFwlFilterFilterName ,i4SetValFwlFilterRowStatus) \
 nmhSetCmnWithLock(FwlFilterRowStatus, 12, FwlFilterRowStatusSet, FwlLock, FwlUnLock, 0, 1, 1, "%s %i", pFwlFilterFilterName ,i4SetValFwlFilterRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FwlRuleRuleName[12];
extern UINT4 FwlRuleFilterSet[12];
extern UINT4 FwlRuleRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFwlRuleFilterSet(pFwlRuleRuleName ,pSetValFwlRuleFilterSet) \
 nmhSetCmnWithLock(FwlRuleFilterSet, 12, FwlRuleFilterSetSet, FwlLock, FwlUnLock, 0, 0, 1, "%s %s", pFwlRuleRuleName ,pSetValFwlRuleFilterSet)
#define nmhSetFwlRuleRowStatus(pFwlRuleRuleName ,i4SetValFwlRuleRowStatus) \
 nmhSetCmnWithLock(FwlRuleRowStatus, 12, FwlRuleRowStatusSet, FwlLock, FwlUnLock, 0, 1, 1, "%s %i", pFwlRuleRuleName ,i4SetValFwlRuleRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FwlAclIfIndex[12];
extern UINT4 FwlAclAclName[12];
extern UINT4 FwlAclDirection[12];
extern UINT4 FwlAclAction[12];
extern UINT4 FwlAclSequenceNumber[12];
extern UINT4 FwlAclLogTrigger[12];
extern UINT4 FwlAclFragAction[12];
extern UINT4 FwlAclRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFwlAclAction(i4FwlAclIfIndex , pFwlAclAclName , i4FwlAclDirection ,i4SetValFwlAclAction) \
 nmhSetCmnWithLock(FwlAclAction, 12, FwlAclActionSet, FwlLock, FwlUnLock, 0, 0, 3, "%i %s %i %i", i4FwlAclIfIndex , pFwlAclAclName , i4FwlAclDirection ,i4SetValFwlAclAction)
#define nmhSetFwlAclSequenceNumber(i4FwlAclIfIndex , pFwlAclAclName , i4FwlAclDirection ,i4SetValFwlAclSequenceNumber) \
 nmhSetCmnWithLock(FwlAclSequenceNumber, 12, FwlAclSequenceNumberSet, FwlLock, FwlUnLock, 0, 0, 3, "%i %s %i %i", i4FwlAclIfIndex , pFwlAclAclName , i4FwlAclDirection ,i4SetValFwlAclSequenceNumber)
#define nmhSetFwlAclLogTrigger(i4FwlAclIfIndex , pFwlAclAclName , i4FwlAclDirection ,i4SetValFwlAclLogTrigger) \
 nmhSetCmnWithLock(FwlAclLogTrigger, 12, FwlAclLogTriggerSet, FwlLock, FwlUnLock, 0, 0, 3, "%i %s %i %i", i4FwlAclIfIndex , pFwlAclAclName , i4FwlAclDirection ,i4SetValFwlAclLogTrigger)
#define nmhSetFwlAclFragAction(i4FwlAclIfIndex , pFwlAclAclName , i4FwlAclDirection ,i4SetValFwlAclFragAction) \
 nmhSetCmnWithLock(FwlAclFragAction, 12, FwlAclFragActionSet, FwlLock, FwlUnLock, 0, 0, 3, "%i %s %i %i", i4FwlAclIfIndex , pFwlAclAclName , i4FwlAclDirection ,i4SetValFwlAclFragAction)
#define nmhSetFwlAclRowStatus(i4FwlAclIfIndex , pFwlAclAclName , i4FwlAclDirection ,i4SetValFwlAclRowStatus) \
 nmhSetCmnWithLock(FwlAclRowStatus, 12, FwlAclRowStatusSet, FwlLock, FwlUnLock, 0, 1, 3, "%i %s %i %i", i4FwlAclIfIndex , pFwlAclAclName , i4FwlAclDirection ,i4SetValFwlAclRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FwlIfIfIndex[12];
extern UINT4 FwlIfIfType[12];
extern UINT4 FwlIfIpOptions[12];
extern UINT4 FwlIfFragments[12];
extern UINT4 FwlIfFragmentSize[12];
extern UINT4 FwlIfICMPType[12];
extern UINT4 FwlIfICMPCode[12];
extern UINT4 FwlIfICMPv6MsgType[12];
extern UINT4 FwlIfRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFwlIfIfType(i4FwlIfIfIndex ,i4SetValFwlIfIfType) \
 nmhSetCmnWithLock(FwlIfIfType, 12, FwlIfIfTypeSet, FwlLock, FwlUnLock, 0, 0, 1, "%i %i", i4FwlIfIfIndex ,i4SetValFwlIfIfType)
#define nmhSetFwlIfIpOptions(i4FwlIfIfIndex ,i4SetValFwlIfIpOptions) \
 nmhSetCmnWithLock(FwlIfIpOptions, 12, FwlIfIpOptionsSet, FwlLock, FwlUnLock, 0, 0, 1, "%i %i", i4FwlIfIfIndex ,i4SetValFwlIfIpOptions)
#define nmhSetFwlIfFragments(i4FwlIfIfIndex ,i4SetValFwlIfFragments) \
 nmhSetCmnWithLock(FwlIfFragments, 12, FwlIfFragmentsSet, FwlLock, FwlUnLock, 0, 0, 1, "%i %i", i4FwlIfIfIndex ,i4SetValFwlIfFragments)
#define nmhSetFwlIfFragmentSize(i4FwlIfIfIndex ,u4SetValFwlIfFragmentSize) \
 nmhSetCmnWithLock(FwlIfFragmentSize, 12, FwlIfFragmentSizeSet, FwlLock, FwlUnLock, 0, 0, 1, "%i %u", i4FwlIfIfIndex ,u4SetValFwlIfFragmentSize)
#define nmhSetFwlIfICMPType(i4FwlIfIfIndex ,i4SetValFwlIfICMPType) \
 nmhSetCmnWithLock(FwlIfICMPType, 12, FwlIfICMPTypeSet, FwlLock, FwlUnLock, 0, 0, 1, "%i %i", i4FwlIfIfIndex ,i4SetValFwlIfICMPType)
#define nmhSetFwlIfICMPCode(i4FwlIfIfIndex ,i4SetValFwlIfICMPCode) \
 nmhSetCmnWithLock(FwlIfICMPCode, 12, FwlIfICMPCodeSet, FwlLock, FwlUnLock, 1, 0, 1, "%i %i", i4FwlIfIfIndex ,i4SetValFwlIfICMPCode)
#define nmhSetFwlIfICMPv6MsgType(i4FwlIfIfIndex ,i4SetValFwlIfICMPv6MsgType) \
 nmhSetCmnWithLock(FwlIfICMPv6MsgType, 12, FwlIfICMPv6MsgTypeSet, FwlLock, FwlUnLock, 0, 0, 1, "%i %i", i4FwlIfIfIndex ,i4SetValFwlIfICMPv6MsgType)
#define nmhSetFwlIfRowStatus(i4FwlIfIfIndex ,i4SetValFwlIfRowStatus) \
 nmhSetCmnWithLock(FwlIfRowStatus, 12, FwlIfRowStatusSet, FwlLock, FwlUnLock, 0, 1, 1, "%i %i", i4FwlIfIfIndex ,i4SetValFwlIfRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FwlDmzIpIndex[12];
extern UINT4 FwlDmzRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFwlDmzRowStatus(u4FwlDmzIpIndex ,i4SetValFwlDmzRowStatus) \
 nmhSetCmnWithLock(FwlDmzRowStatus, 12, FwlDmzRowStatusSet, FwlLock, FwlUnLock, 0, 1, 1, "%p %i", u4FwlDmzIpIndex ,i4SetValFwlDmzRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FwlUrlString[12];
extern UINT4 FwlUrlFilterRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFwlUrlFilterRowStatus(pFwlUrlString ,i4SetValFwlUrlFilterRowStatus) \
 nmhSetCmnWithLock(FwlUrlFilterRowStatus, 12, FwlUrlFilterRowStatusSet, FwlLock, FwlUnLock, 0, 1, 1, "%s %i", pFwlUrlString ,i4SetValFwlUrlFilterRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FwlStatIfIfIndex[12];
extern UINT4 FwlStatIfClear[12];
extern UINT4 FwlIfTrapThreshold[12];
extern UINT4 FwlStatIfClearIPv6[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFwlStatIfClear(i4FwlStatIfIfIndex ,i4SetValFwlStatIfClear) \
 nmhSetCmnWithLock(FwlStatIfClear, 12, FwlStatIfClearSet, FwlLock, FwlUnLock, 0, 0, 1, "%i %i", i4FwlStatIfIfIndex ,i4SetValFwlStatIfClear)
#define nmhSetFwlIfTrapThreshold(i4FwlStatIfIfIndex ,i4SetValFwlIfTrapThreshold) \
 nmhSetCmnWithLock(FwlIfTrapThreshold, 12, FwlIfTrapThresholdSet, FwlLock, FwlUnLock, 0, 0, 1, "%i %i", i4FwlStatIfIfIndex ,i4SetValFwlIfTrapThreshold)
#define nmhSetFwlStatIfClearIPv6(i4FwlStatIfIfIndex ,i4SetValFwlStatIfClearIPv6) \
 nmhSetCmnWithLock(FwlStatIfClearIPv6, 12, FwlStatIfClearIPv6Set, FwlLock, FwlUnLock, 0, 0, 1, "%i %i", i4FwlStatIfIfIndex ,i4SetValFwlStatIfClearIPv6)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FwlStatClear[10];
extern UINT4 FwlStatClearIPv6[10];
extern UINT4 FwlTrapThreshold[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFwlStatClear(i4SetValFwlStatClear) \
 nmhSetCmnWithLock(FwlStatClear, 10, FwlStatClearSet, FwlLock, FwlUnLock, 0, 0, 0, "%i", i4SetValFwlStatClear)
#define nmhSetFwlStatClearIPv6(i4SetValFwlStatClearIPv6) \
 nmhSetCmnWithLock(FwlStatClearIPv6, 10, FwlStatClearIPv6Set, FwlLock, FwlUnLock, 0, 0, 0, "%i", i4SetValFwlStatClearIPv6)
#define nmhSetFwlTrapThreshold(i4SetValFwlTrapThreshold) \
 nmhSetCmnWithLock(FwlTrapThreshold, 10, FwlTrapThresholdSet, FwlLock, FwlUnLock, 0, 0, 0, "%i", i4SetValFwlTrapThreshold)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FwlTrapMemFailMessage[11];
extern UINT4 FwlTrapAttackMessage[11];
extern UINT4 FwlIfIndex[11];
extern UINT4 FwlTrapEvent[11];
extern UINT4 FwlTrapEventTime[11];
extern UINT4 FwlIdsTrapEvent[11];
extern UINT4 FwlIdsTrapEventTime[11];
extern UINT4 FwlIdsAttackPktIp[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFwlTrapMemFailMessage(pSetValFwlTrapMemFailMessage) \
 nmhSetCmnWithLock(FwlTrapMemFailMessage, 11, FwlTrapMemFailMessageSet, FwlLock, FwlUnLock, 0, 0, 0, "%s", pSetValFwlTrapMemFailMessage)
#define nmhSetFwlTrapAttackMessage(pSetValFwlTrapAttackMessage) \
 nmhSetCmnWithLock(FwlTrapAttackMessage, 11, FwlTrapAttackMessageSet, FwlLock, FwlUnLock, 0, 0, 0, "%s", pSetValFwlTrapAttackMessage)
#define nmhSetFwlIfIndex(pSetValFwlIfIndex) \
 nmhSetCmnWithLock(FwlIfIndex, 11, FwlIfIndexSet, FwlLock, FwlUnLock, 0, 0, 0, "%o", pSetValFwlIfIndex)
#define nmhSetFwlTrapEvent(i4SetValFwlTrapEvent) \
 nmhSetCmnWithLock(FwlTrapEvent, 11, FwlTrapEventSet, FwlLock, FwlUnLock, 0, 0, 0, "%i", i4SetValFwlTrapEvent)
#define nmhSetFwlTrapEventTime(pSetValFwlTrapEventTime) \
 nmhSetCmnWithLock(FwlTrapEventTime, 11, FwlTrapEventTimeSet, FwlLock, FwlUnLock, 0, 0, 0, "%s", pSetValFwlTrapEventTime)
#define nmhSetFwlIdsTrapEvent(i4SetValFwlIdsTrapEvent) \
 nmhSetCmnWithLock(FwlIdsTrapEvent, 11, FwlIdsTrapEventSet, FwlLock, FwlUnLock, 0, 0, 0, "%i", i4SetValFwlIdsTrapEvent)
#define nmhSetFwlIdsTrapEventTime(pSetValFwlIdsTrapEventTime) \
 nmhSetCmnWithLock(FwlIdsTrapEventTime, 11, FwlIdsTrapEventTimeSet, FwlLock, FwlUnLock, 0, 0, 0, "%s", pSetValFwlIdsTrapEventTime)
#define nmhSetFwlIdsAttackPktIp(pSetValFwlIdsAttackPktIp) \
 nmhSetCmnWithLock(FwlIdsAttackPktIp, 11, FwlIdsAttackPktIpSet, FwlLock, FwlUnLock, 0, 0, 0, "%s", pSetValFwlIdsAttackPktIp)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FwlBlkListIpAddressType[12];
extern UINT4 FwlBlkListIpAddress[12];
extern UINT4 FwlBlkListIpMask[12];
extern UINT4 FwlBlkListRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFwlBlkListRowStatus(i4FwlBlkListIpAddressType , pFwlBlkListIpAddress , u4FwlBlkListIpMask ,i4SetValFwlBlkListRowStatus) \
 nmhSetCmnWithLock(FwlBlkListRowStatus, 12, FwlBlkListRowStatusSet, FwlLock, FwlUnLock, 0, 1, 3, "%i %s %u %i", i4FwlBlkListIpAddressType , pFwlBlkListIpAddress , u4FwlBlkListIpMask ,i4SetValFwlBlkListRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FwlWhiteListIpAddressType[12];
extern UINT4 FwlWhiteListIpAddress[12];
extern UINT4 FwlWhiteListIpMask[12];
extern UINT4 FwlWhiteListRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFwlWhiteListRowStatus(i4FwlWhiteListIpAddressType , pFwlWhiteListIpAddress , u4FwlWhiteListIpMask ,i4SetValFwlWhiteListRowStatus) \
 nmhSetCmnWithLock(FwlWhiteListRowStatus, 12, FwlWhiteListRowStatusSet, FwlLock, FwlUnLock, 0, 1, 3, "%i %s %u %i", i4FwlWhiteListIpAddressType , pFwlWhiteListIpAddress , u4FwlWhiteListIpMask ,i4SetValFwlWhiteListRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FwlDmzAddressType[12];
extern UINT4 FwlDmzIpv6Index[12];
extern UINT4 FwlDmzIpv6RowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFwlDmzAddressType(pFwlDmzIpv6Index ,i4SetValFwlDmzAddressType) \
 nmhSetCmnWithLock(FwlDmzAddressType, 12, FwlDmzAddressTypeSet, FwlLock, FwlUnLock, 0, 0, 1, "%s %i", pFwlDmzIpv6Index ,i4SetValFwlDmzAddressType)
#define nmhSetFwlDmzIpv6RowStatus(pFwlDmzIpv6Index ,i4SetValFwlDmzIpv6RowStatus) \
 nmhSetCmnWithLock(FwlDmzIpv6RowStatus, 12, FwlDmzIpv6RowStatusSet, FwlLock, FwlUnLock, 0, 1, 1, "%s %i", pFwlDmzIpv6Index ,i4SetValFwlDmzIpv6RowStatus)

#endif
