/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: slldv2cli.h,v 1.2 2013/05/24 13:36:02 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 LldpV2MessageTxInterval[11];
extern UINT4 LldpV2MessageTxHoldMultiplier[11];
extern UINT4 LldpV2ReinitDelay[11];
extern UINT4 LldpV2NotificationInterval[11];
extern UINT4 LldpV2TxCreditMax[11];
extern UINT4 LldpV2MessageFastTx[11];
extern UINT4 LldpV2TxFastInit[11];
extern UINT4 LldpTxDelay[9];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetLldpV2MessageTxInterval(u4SetValLldpV2MessageTxInterval) \
 nmhSetCmn(LldpV2MessageTxInterval, 11, LldpV2MessageTxIntervalSet, NULL, NULL, 0, 0, 0, "%u", u4SetValLldpV2MessageTxInterval)
#define nmhSetLldpV2MessageTxHoldMultiplier(u4SetValLldpV2MessageTxHoldMultiplier) \
 nmhSetCmn(LldpV2MessageTxHoldMultiplier, 11, LldpV2MessageTxHoldMultiplierSet, NULL, NULL, 0, 0, 0, "%u", u4SetValLldpV2MessageTxHoldMultiplier)
#define nmhSetLldpV2ReinitDelay(u4SetValLldpV2ReinitDelay) \
 nmhSetCmn(LldpV2ReinitDelay, 11, LldpV2ReinitDelaySet, NULL, NULL, 0, 0, 0, "%u", u4SetValLldpV2ReinitDelay)
#define nmhSetLldpV2NotificationInterval(u4SetValLldpV2NotificationInterval) \
 nmhSetCmn(LldpV2NotificationInterval, 11, LldpV2NotificationIntervalSet, NULL, NULL, 0, 0, 0, "%u", u4SetValLldpV2NotificationInterval)
#define nmhSetLldpV2TxCreditMax(u4SetValLldpV2TxCreditMax) \
 nmhSetCmn(LldpV2TxCreditMax, 11, LldpV2TxCreditMaxSet, NULL, NULL, 0, 0, 0, "%u", u4SetValLldpV2TxCreditMax)
#define nmhSetLldpV2MessageFastTx(u4SetValLldpV2MessageFastTx) \
 nmhSetCmn(LldpV2MessageFastTx, 11, LldpV2MessageFastTxSet, NULL, NULL, 0, 0, 0, "%u", u4SetValLldpV2MessageFastTx)
#define nmhSetLldpV2TxFastInit(u4SetValLldpV2TxFastInit) \
 nmhSetCmn(LldpV2TxFastInit, 11, LldpV2TxFastInitSet, NULL, NULL, 0, 0, 0, "%u", u4SetValLldpV2TxFastInit)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 LldpV2PortConfigIfIndex[13];
extern UINT4 LldpV2PortConfigDestAddressIndex[13];
extern UINT4 LldpV2PortConfigAdminStatus[13];
extern UINT4 LldpV2PortConfigNotificationEnable[13];
extern UINT4 LldpV2PortConfigTLVsTxEnable[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetLldpV2PortConfigAdminStatus(i4LldpV2PortConfigIfIndex , u4LldpV2PortConfigDestAddressIndex ,i4SetValLldpV2PortConfigAdminStatus) \
 nmhSetCmn(LldpV2PortConfigAdminStatus, 13, LldpV2PortConfigAdminStatusSet, NULL, NULL, 0, 0, 2, "%i %u %i", i4LldpV2PortConfigIfIndex , u4LldpV2PortConfigDestAddressIndex ,i4SetValLldpV2PortConfigAdminStatus)
#define nmhSetLldpV2PortConfigNotificationEnable(i4LldpV2PortConfigIfIndex , u4LldpV2PortConfigDestAddressIndex ,i4SetValLldpV2PortConfigNotificationEnable) \
 nmhSetCmn(LldpV2PortConfigNotificationEnable, 13, LldpV2PortConfigNotificationEnableSet, NULL, NULL, 0, 0, 2, "%i %u %i", i4LldpV2PortConfigIfIndex , u4LldpV2PortConfigDestAddressIndex ,i4SetValLldpV2PortConfigNotificationEnable)
#define nmhSetLldpV2PortConfigTLVsTxEnable(i4LldpV2PortConfigIfIndex , u4LldpV2PortConfigDestAddressIndex ,pSetValLldpV2PortConfigTLVsTxEnable) \
 nmhSetCmn(LldpV2PortConfigTLVsTxEnable, 13, LldpV2PortConfigTLVsTxEnableSet, NULL, NULL, 0, 0, 2, "%i %u %s", i4LldpV2PortConfigIfIndex , u4LldpV2PortConfigDestAddressIndex ,pSetValLldpV2PortConfigTLVsTxEnable)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 LldpV2ManAddrConfigIfIndex[13];
extern UINT4 LldpV2ManAddrConfigDestAddressIndex[13];
extern UINT4 LldpV2ManAddrConfigLocManAddrSubtype[13];
extern UINT4 LldpV2ManAddrConfigLocManAddr[13];
extern UINT4 LldpV2ManAddrConfigTxEnable[13];
extern UINT4 LldpV2ManAddrConfigRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetLldpV2ManAddrConfigTxEnable(i4LldpV2ManAddrConfigIfIndex , u4LldpV2ManAddrConfigDestAddressIndex , i4LldpV2ManAddrConfigLocManAddrSubtype , pLldpV2ManAddrConfigLocManAddr ,i4SetValLldpV2ManAddrConfigTxEnable) \
 nmhSetCmn(LldpV2ManAddrConfigTxEnable, 13, LldpV2ManAddrConfigTxEnableSet, NULL, NULL, 0, 0, 4, "%i %u %i %s %i", i4LldpV2ManAddrConfigIfIndex , u4LldpV2ManAddrConfigDestAddressIndex , i4LldpV2ManAddrConfigLocManAddrSubtype , pLldpV2ManAddrConfigLocManAddr ,i4SetValLldpV2ManAddrConfigTxEnable)
#define nmhSetLldpV2ManAddrConfigRowStatus(i4LldpV2ManAddrConfigIfIndex , u4LldpV2ManAddrConfigDestAddressIndex , i4LldpV2ManAddrConfigLocManAddrSubtype , pLldpV2ManAddrConfigLocManAddr ,i4SetValLldpV2ManAddrConfigRowStatus) \
 nmhSetCmn(LldpV2ManAddrConfigRowStatus, 13, LldpV2ManAddrConfigRowStatusSet, NULL, NULL, 0, 1, 4, "%i %u %i %s %i", i4LldpV2ManAddrConfigIfIndex , u4LldpV2ManAddrConfigDestAddressIndex , i4LldpV2ManAddrConfigLocManAddrSubtype , pLldpV2ManAddrConfigLocManAddr ,i4SetValLldpV2ManAddrConfigRowStatus)

#endif
