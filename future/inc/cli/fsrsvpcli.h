/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsrsvpcli.h,v 1.7 2018/01/03 11:31:19 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsRsvpTeIfIndex[13];
extern UINT4 FsMplsRsvpTeIfLblSpace[13];
extern UINT4 FsMplsRsvpTeIfLblType[13];
extern UINT4 FsMplsRsvpTeAtmMergeCap[13];
extern UINT4 FsMplsRsvpTeAtmVcDirection[13];
extern UINT4 FsMplsRsvpTeAtmMinVpi[13];
extern UINT4 FsMplsRsvpTeAtmMinVci[13];
extern UINT4 FsMplsRsvpTeAtmMaxVpi[13];
extern UINT4 FsMplsRsvpTeAtmMaxVci[13];
extern UINT4 FsMplsRsvpTeIfMtu[13];
extern UINT4 FsMplsRsvpTeIfRefreshMultiple[13];
extern UINT4 FsMplsRsvpTeIfTTL[13];
extern UINT4 FsMplsRsvpTeIfRefreshInterval[13];
extern UINT4 FsMplsRsvpTeIfRouteDelay[13];
extern UINT4 FsMplsRsvpTeIfUdpRequired[13];
extern UINT4 FsMplsRsvpTeIfHelloSupported[13];
extern UINT4 FsMplsRsvpTeIfLinkAttr[13];
extern UINT4 FsMplsRsvpTeIfStatus[13];
extern UINT4 FsMplsRsvpTeIfPlrId[13];
extern UINT4 FsMplsRsvpTeIfAvoidNodeId[13];
extern UINT4 FsMplsRsvpTeIfStorageType[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsRsvpTeIfLblSpace(i4FsMplsRsvpTeIfIndex ,i4SetValFsMplsRsvpTeIfLblSpace) \
 nmhSetCmn(FsMplsRsvpTeIfLblSpace, 13, FsMplsRsvpTeIfLblSpaceSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 1, "%i %i", i4FsMplsRsvpTeIfIndex ,i4SetValFsMplsRsvpTeIfLblSpace)
#define nmhSetFsMplsRsvpTeIfLblType(i4FsMplsRsvpTeIfIndex ,i4SetValFsMplsRsvpTeIfLblType) \
 nmhSetCmn(FsMplsRsvpTeIfLblType, 13, FsMplsRsvpTeIfLblTypeSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 1, "%i %i", i4FsMplsRsvpTeIfIndex ,i4SetValFsMplsRsvpTeIfLblType)
#define nmhSetFsMplsRsvpTeAtmMergeCap(i4FsMplsRsvpTeIfIndex ,i4SetValFsMplsRsvpTeAtmMergeCap) \
 nmhSetCmn(FsMplsRsvpTeAtmMergeCap, 13, FsMplsRsvpTeAtmMergeCapSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 1, "%i %i", i4FsMplsRsvpTeIfIndex ,i4SetValFsMplsRsvpTeAtmMergeCap)
#define nmhSetFsMplsRsvpTeAtmVcDirection(i4FsMplsRsvpTeIfIndex ,i4SetValFsMplsRsvpTeAtmVcDirection) \
 nmhSetCmn(FsMplsRsvpTeAtmVcDirection, 13, FsMplsRsvpTeAtmVcDirectionSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 1, "%i %i", i4FsMplsRsvpTeIfIndex ,i4SetValFsMplsRsvpTeAtmVcDirection)
#define nmhSetFsMplsRsvpTeAtmMinVpi(i4FsMplsRsvpTeIfIndex ,i4SetValFsMplsRsvpTeAtmMinVpi) \
 nmhSetCmn(FsMplsRsvpTeAtmMinVpi, 13, FsMplsRsvpTeAtmMinVpiSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 1, "%i %i", i4FsMplsRsvpTeIfIndex ,i4SetValFsMplsRsvpTeAtmMinVpi)
#define nmhSetFsMplsRsvpTeAtmMinVci(i4FsMplsRsvpTeIfIndex ,i4SetValFsMplsRsvpTeAtmMinVci) \
 nmhSetCmn(FsMplsRsvpTeAtmMinVci, 13, FsMplsRsvpTeAtmMinVciSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 1, "%i %i", i4FsMplsRsvpTeIfIndex ,i4SetValFsMplsRsvpTeAtmMinVci)
#define nmhSetFsMplsRsvpTeAtmMaxVpi(i4FsMplsRsvpTeIfIndex ,i4SetValFsMplsRsvpTeAtmMaxVpi) \
 nmhSetCmn(FsMplsRsvpTeAtmMaxVpi, 13, FsMplsRsvpTeAtmMaxVpiSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 1, "%i %i", i4FsMplsRsvpTeIfIndex ,i4SetValFsMplsRsvpTeAtmMaxVpi)
#define nmhSetFsMplsRsvpTeAtmMaxVci(i4FsMplsRsvpTeIfIndex ,i4SetValFsMplsRsvpTeAtmMaxVci) \
 nmhSetCmn(FsMplsRsvpTeAtmMaxVci, 13, FsMplsRsvpTeAtmMaxVciSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 1, "%i %i", i4FsMplsRsvpTeIfIndex ,i4SetValFsMplsRsvpTeAtmMaxVci)
#define nmhSetFsMplsRsvpTeIfMtu(i4FsMplsRsvpTeIfIndex ,i4SetValFsMplsRsvpTeIfMtu) \
 nmhSetCmn(FsMplsRsvpTeIfMtu, 13, FsMplsRsvpTeIfMtuSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 1, "%i %i", i4FsMplsRsvpTeIfIndex ,i4SetValFsMplsRsvpTeIfMtu)
#define nmhSetFsMplsRsvpTeIfRefreshMultiple(i4FsMplsRsvpTeIfIndex ,i4SetValFsMplsRsvpTeIfRefreshMultiple) \
 nmhSetCmn(FsMplsRsvpTeIfRefreshMultiple, 13, FsMplsRsvpTeIfRefreshMultipleSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 1, "%i %i", i4FsMplsRsvpTeIfIndex ,i4SetValFsMplsRsvpTeIfRefreshMultiple)
#define nmhSetFsMplsRsvpTeIfTTL(i4FsMplsRsvpTeIfIndex ,i4SetValFsMplsRsvpTeIfTTL) \
 nmhSetCmn(FsMplsRsvpTeIfTTL, 13, FsMplsRsvpTeIfTTLSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 1, "%i %i", i4FsMplsRsvpTeIfIndex ,i4SetValFsMplsRsvpTeIfTTL)
#define nmhSetFsMplsRsvpTeIfRefreshInterval(i4FsMplsRsvpTeIfIndex ,i4SetValFsMplsRsvpTeIfRefreshInterval) \
 nmhSetCmn(FsMplsRsvpTeIfRefreshInterval, 13, FsMplsRsvpTeIfRefreshIntervalSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 1, "%i %i", i4FsMplsRsvpTeIfIndex ,i4SetValFsMplsRsvpTeIfRefreshInterval)
#define nmhSetFsMplsRsvpTeIfRouteDelay(i4FsMplsRsvpTeIfIndex ,i4SetValFsMplsRsvpTeIfRouteDelay) \
 nmhSetCmn(FsMplsRsvpTeIfRouteDelay, 13, FsMplsRsvpTeIfRouteDelaySet, RsvpTeLock, RsvpTeUnLock, 0, 0, 1, "%i %i", i4FsMplsRsvpTeIfIndex ,i4SetValFsMplsRsvpTeIfRouteDelay)
#define nmhSetFsMplsRsvpTeIfUdpRequired(i4FsMplsRsvpTeIfIndex ,i4SetValFsMplsRsvpTeIfUdpRequired) \
 nmhSetCmn(FsMplsRsvpTeIfUdpRequired, 13, FsMplsRsvpTeIfUdpRequiredSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 1, "%i %i", i4FsMplsRsvpTeIfIndex ,i4SetValFsMplsRsvpTeIfUdpRequired)
#define nmhSetFsMplsRsvpTeIfHelloSupported(i4FsMplsRsvpTeIfIndex ,i4SetValFsMplsRsvpTeIfHelloSupported) \
 nmhSetCmn(FsMplsRsvpTeIfHelloSupported, 13, FsMplsRsvpTeIfHelloSupportedSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 1, "%i %i", i4FsMplsRsvpTeIfIndex ,i4SetValFsMplsRsvpTeIfHelloSupported)
#define nmhSetFsMplsRsvpTeIfLinkAttr(i4FsMplsRsvpTeIfIndex ,u4SetValFsMplsRsvpTeIfLinkAttr) \
 nmhSetCmn(FsMplsRsvpTeIfLinkAttr, 13, FsMplsRsvpTeIfLinkAttrSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 1, "%i %u", i4FsMplsRsvpTeIfIndex ,u4SetValFsMplsRsvpTeIfLinkAttr)
#define nmhSetFsMplsRsvpTeIfStatus(i4FsMplsRsvpTeIfIndex ,i4SetValFsMplsRsvpTeIfStatus) \
 nmhSetCmn(FsMplsRsvpTeIfStatus, 13, FsMplsRsvpTeIfStatusSet, RsvpTeLock, RsvpTeUnLock, 0, 1, 1, "%i %i", i4FsMplsRsvpTeIfIndex ,i4SetValFsMplsRsvpTeIfStatus)
#define nmhSetFsMplsRsvpTeIfPlrId(i4FsMplsRsvpTeIfIndex ,pSetValFsMplsRsvpTeIfPlrId) \
 nmhSetCmn(FsMplsRsvpTeIfPlrId, 13, FsMplsRsvpTeIfPlrIdSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 1, "%i %s", i4FsMplsRsvpTeIfIndex ,pSetValFsMplsRsvpTeIfPlrId)
#define nmhSetFsMplsRsvpTeIfAvoidNodeId(i4FsMplsRsvpTeIfIndex ,pSetValFsMplsRsvpTeIfAvoidNodeId) \
 nmhSetCmn(FsMplsRsvpTeIfAvoidNodeId, 13, FsMplsRsvpTeIfAvoidNodeIdSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 1, "%i %s", i4FsMplsRsvpTeIfIndex ,pSetValFsMplsRsvpTeIfAvoidNodeId)
#define nmhSetFsMplsRsvpTeIfStorageType(i4FsMplsRsvpTeIfIndex ,i4SetValFsMplsRsvpTeIfStorageType) \
 nmhSetCmn(FsMplsRsvpTeIfStorageType, 13, FsMplsRsvpTeIfStorageTypeSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 1, "%i %i", i4FsMplsRsvpTeIfIndex ,i4SetValFsMplsRsvpTeIfStorageType)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsRsvpTeNbrIfAddr[13];
extern UINT4 FsMplsRsvpTeNbrRRCapable[13];
extern UINT4 FsMplsRsvpTeNbrRMDCapable[13];
extern UINT4 FsMplsRsvpTeNbrEncapsType[13];
extern UINT4 FsMplsRsvpTeNbrHelloSupport[13];
extern UINT4 FsMplsRsvpTeNbrStatus[13];
extern UINT4 FsMplsRsvpTeNbrStorageType[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsRsvpTeNbrRRCapable(i4FsMplsRsvpTeIfIndex , u4FsMplsRsvpTeNbrIfAddr ,i4SetValFsMplsRsvpTeNbrRRCapable) \
 nmhSetCmn(FsMplsRsvpTeNbrRRCapable, 13, FsMplsRsvpTeNbrRRCapableSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 2, "%i %p %i", i4FsMplsRsvpTeIfIndex , u4FsMplsRsvpTeNbrIfAddr ,i4SetValFsMplsRsvpTeNbrRRCapable)
#define nmhSetFsMplsRsvpTeNbrRMDCapable(i4FsMplsRsvpTeIfIndex , u4FsMplsRsvpTeNbrIfAddr ,i4SetValFsMplsRsvpTeNbrRMDCapable) \
 nmhSetCmn(FsMplsRsvpTeNbrRMDCapable, 13, FsMplsRsvpTeNbrRMDCapableSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 2, "%i %p %i", i4FsMplsRsvpTeIfIndex , u4FsMplsRsvpTeNbrIfAddr ,i4SetValFsMplsRsvpTeNbrRMDCapable)
#define nmhSetFsMplsRsvpTeNbrEncapsType(i4FsMplsRsvpTeIfIndex , u4FsMplsRsvpTeNbrIfAddr ,i4SetValFsMplsRsvpTeNbrEncapsType) \
 nmhSetCmn(FsMplsRsvpTeNbrEncapsType, 13, FsMplsRsvpTeNbrEncapsTypeSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 2, "%i %p %i", i4FsMplsRsvpTeIfIndex , u4FsMplsRsvpTeNbrIfAddr ,i4SetValFsMplsRsvpTeNbrEncapsType)
#define nmhSetFsMplsRsvpTeNbrHelloSupport(i4FsMplsRsvpTeIfIndex , u4FsMplsRsvpTeNbrIfAddr ,i4SetValFsMplsRsvpTeNbrHelloSupport) \
 nmhSetCmn(FsMplsRsvpTeNbrHelloSupport, 13, FsMplsRsvpTeNbrHelloSupportSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 2, "%i %p %i", i4FsMplsRsvpTeIfIndex , u4FsMplsRsvpTeNbrIfAddr ,i4SetValFsMplsRsvpTeNbrHelloSupport)
#define nmhSetFsMplsRsvpTeNbrStatus(i4FsMplsRsvpTeIfIndex , u4FsMplsRsvpTeNbrIfAddr ,i4SetValFsMplsRsvpTeNbrStatus) \
 nmhSetCmn(FsMplsRsvpTeNbrStatus, 13, FsMplsRsvpTeNbrStatusSet, RsvpTeLock, RsvpTeUnLock, 0, 1, 2, "%i %p %i", i4FsMplsRsvpTeIfIndex , u4FsMplsRsvpTeNbrIfAddr ,i4SetValFsMplsRsvpTeNbrStatus)
#define nmhSetFsMplsRsvpTeNbrStorageType(i4FsMplsRsvpTeIfIndex , u4FsMplsRsvpTeNbrIfAddr ,i4SetValFsMplsRsvpTeNbrStorageType) \
 nmhSetCmn(FsMplsRsvpTeNbrStorageType, 13, FsMplsRsvpTeNbrStorageTypeSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 2, "%i %p %i", i4FsMplsRsvpTeIfIndex , u4FsMplsRsvpTeNbrIfAddr ,i4SetValFsMplsRsvpTeNbrStorageType)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsRsvpTeLsrID[11];
extern UINT4 FsMplsRsvpTeMaxTnls[11];
extern UINT4 FsMplsRsvpTeMaxErhopsPerTnl[11];
extern UINT4 FsMplsRsvpTeMaxActRoutePerTnl[11];
extern UINT4 FsMplsRsvpTeMaxIfaces[11];
extern UINT4 FsMplsRsvpTeMaxNbrs[11];
extern UINT4 FsMplsRsvpTeSockSupprtd[11];
extern UINT4 FsMplsRsvpTeHelloSupprtd[11];
extern UINT4 FsMplsRsvpTeHelloIntervalTime[11];
extern UINT4 FsMplsRsvpTeRRCapable[11];
extern UINT4 FsMplsRsvpTeMsgIdCapable[11];
extern UINT4 FsMplsRsvpTeRMDPolicyObject[11];
extern UINT4 FsMplsRsvpTeGenLblSpaceMinLbl[11];
extern UINT4 FsMplsRsvpTeGenLblSpaceMaxLbl[11];
extern UINT4 FsMplsRsvpTeGenDebugFlag[11];
extern UINT4 FsMplsRsvpTeGenPduDumpLevel[11];
extern UINT4 FsMplsRsvpTeGenPduDumpMsgType[11];
extern UINT4 FsMplsRsvpTeGenPduDumpDirection[11];
extern UINT4 FsMplsRsvpTeOperStatus[11];
extern UINT4 FsMplsRsvpTeOverRideOption[11];
extern UINT4 FsMplsRsvpTeMinTnlsWithMsgId[11];
extern UINT4 FsMplsRsvpTeNotificationEnabled[11];
extern UINT4 FsMplsRsvpTeNotifyMsgRetransmitIntvl[11];
extern UINT4 FsMplsRsvpTeNotifyMsgRetransmitDecay[11];
extern UINT4 FsMplsRsvpTeNotifyMsgRetransmitLimit[11];
extern UINT4 FsMplsRsvpTeAdminStatusTimeIntvl[11];
extern UINT4 FsMplsRsvpTePathStateRemovedSupport[11];
extern UINT4 FsMplsRsvpTeLabelSetEnabled[11];
extern UINT4 FsMplsRsvpTeAdminStatusCapabiliy[11];
extern UINT4 FsMplsRsvpTeGrCapability[11];
extern UINT4 FsMplsRsvpTeGrRecoveryPathCapability[11];
extern UINT4 FsMplsRsvpTeGrRestartTime[11];
extern UINT4 FsMplsRsvpTeGrRecoveryTime[11];
extern UINT4 FsMplsRsvpTeReoptimizeTime[11];
extern UINT4 FsMplsRsvpTeEroCacheTime[11];
extern UINT4 FsMplsRsvpTeReoptLinkMaintenance[11];
extern UINT4 FsMplsRsvpTeReoptNodeMaintenance[11];


#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsRsvpTeLsrID(pSetValFsMplsRsvpTeLsrID) \
 nmhSetCmn(FsMplsRsvpTeLsrID, 11, FsMplsRsvpTeLsrIDSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 0, "%s", pSetValFsMplsRsvpTeLsrID)
#define nmhSetFsMplsRsvpTeMaxTnls(i4SetValFsMplsRsvpTeMaxTnls) \
 nmhSetCmn(FsMplsRsvpTeMaxTnls, 11, FsMplsRsvpTeMaxTnlsSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 0, "%i", i4SetValFsMplsRsvpTeMaxTnls)
#define nmhSetFsMplsRsvpTeMaxErhopsPerTnl(i4SetValFsMplsRsvpTeMaxErhopsPerTnl) \
 nmhSetCmn(FsMplsRsvpTeMaxErhopsPerTnl, 11, FsMplsRsvpTeMaxErhopsPerTnlSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 0, "%i", i4SetValFsMplsRsvpTeMaxErhopsPerTnl)
#define nmhSetFsMplsRsvpTeMaxActRoutePerTnl(i4SetValFsMplsRsvpTeMaxActRoutePerTnl) \
 nmhSetCmn(FsMplsRsvpTeMaxActRoutePerTnl, 11, FsMplsRsvpTeMaxActRoutePerTnlSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 0, "%i", i4SetValFsMplsRsvpTeMaxActRoutePerTnl)
#define nmhSetFsMplsRsvpTeMaxIfaces(i4SetValFsMplsRsvpTeMaxIfaces) \
 nmhSetCmn(FsMplsRsvpTeMaxIfaces, 11, FsMplsRsvpTeMaxIfacesSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 0, "%i", i4SetValFsMplsRsvpTeMaxIfaces)
#define nmhSetFsMplsRsvpTeMaxNbrs(i4SetValFsMplsRsvpTeMaxNbrs) \
 nmhSetCmn(FsMplsRsvpTeMaxNbrs, 11, FsMplsRsvpTeMaxNbrsSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 0, "%i", i4SetValFsMplsRsvpTeMaxNbrs)
#define nmhSetFsMplsRsvpTeSockSupprtd(i4SetValFsMplsRsvpTeSockSupprtd) \
 nmhSetCmn(FsMplsRsvpTeSockSupprtd, 11, FsMplsRsvpTeSockSupprtdSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 0, "%i", i4SetValFsMplsRsvpTeSockSupprtd)
#define nmhSetFsMplsRsvpTeHelloSupprtd(i4SetValFsMplsRsvpTeHelloSupprtd) \
 nmhSetCmn(FsMplsRsvpTeHelloSupprtd, 11, FsMplsRsvpTeHelloSupprtdSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 0, "%i", i4SetValFsMplsRsvpTeHelloSupprtd)
#define nmhSetFsMplsRsvpTeHelloIntervalTime(i4SetValFsMplsRsvpTeHelloIntervalTime) \
 nmhSetCmn(FsMplsRsvpTeHelloIntervalTime, 11, FsMplsRsvpTeHelloIntervalTimeSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 0, "%i", i4SetValFsMplsRsvpTeHelloIntervalTime)
#define nmhSetFsMplsRsvpTeRRCapable(i4SetValFsMplsRsvpTeRRCapable) \
 nmhSetCmn(FsMplsRsvpTeRRCapable, 11, FsMplsRsvpTeRRCapableSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 0, "%i", i4SetValFsMplsRsvpTeRRCapable)
#define nmhSetFsMplsRsvpTeMsgIdCapable(i4SetValFsMplsRsvpTeMsgIdCapable) \
 nmhSetCmn(FsMplsRsvpTeMsgIdCapable, 11, FsMplsRsvpTeMsgIdCapableSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 0, "%i", i4SetValFsMplsRsvpTeMsgIdCapable)
#define nmhSetFsMplsRsvpTeRMDPolicyObject(pSetValFsMplsRsvpTeRMDPolicyObject) \
 nmhSetCmn(FsMplsRsvpTeRMDPolicyObject, 11, FsMplsRsvpTeRMDPolicyObjectSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 0, "%s", pSetValFsMplsRsvpTeRMDPolicyObject)
#define nmhSetFsMplsRsvpTeGenLblSpaceMinLbl(i4SetValFsMplsRsvpTeGenLblSpaceMinLbl) \
 nmhSetCmn(FsMplsRsvpTeGenLblSpaceMinLbl, 11, FsMplsRsvpTeGenLblSpaceMinLblSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 0, "%i", i4SetValFsMplsRsvpTeGenLblSpaceMinLbl)
#define nmhSetFsMplsRsvpTeGenLblSpaceMaxLbl(i4SetValFsMplsRsvpTeGenLblSpaceMaxLbl) \
 nmhSetCmn(FsMplsRsvpTeGenLblSpaceMaxLbl, 11, FsMplsRsvpTeGenLblSpaceMaxLblSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 0, "%i", i4SetValFsMplsRsvpTeGenLblSpaceMaxLbl)
#define nmhSetFsMplsRsvpTeGenDebugFlag(u4SetValFsMplsRsvpTeGenDebugFlag) \
 nmhSetCmn(FsMplsRsvpTeGenDebugFlag, 11, FsMplsRsvpTeGenDebugFlagSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 0, "%u", u4SetValFsMplsRsvpTeGenDebugFlag)
#define nmhSetFsMplsRsvpTeGenPduDumpLevel(i4SetValFsMplsRsvpTeGenPduDumpLevel) \
 nmhSetCmn(FsMplsRsvpTeGenPduDumpLevel, 11, FsMplsRsvpTeGenPduDumpLevelSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 0, "%i", i4SetValFsMplsRsvpTeGenPduDumpLevel)
#define nmhSetFsMplsRsvpTeGenPduDumpMsgType(i4SetValFsMplsRsvpTeGenPduDumpMsgType) \
 nmhSetCmn(FsMplsRsvpTeGenPduDumpMsgType, 11, FsMplsRsvpTeGenPduDumpMsgTypeSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 0, "%i", i4SetValFsMplsRsvpTeGenPduDumpMsgType)
#define nmhSetFsMplsRsvpTeGenPduDumpDirection(i4SetValFsMplsRsvpTeGenPduDumpDirection) \
 nmhSetCmn(FsMplsRsvpTeGenPduDumpDirection, 11, FsMplsRsvpTeGenPduDumpDirectionSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 0, "%i", i4SetValFsMplsRsvpTeGenPduDumpDirection)
#define nmhSetFsMplsRsvpTeOperStatus(i4SetValFsMplsRsvpTeOperStatus) \
 nmhSetCmn(FsMplsRsvpTeOperStatus, 11, FsMplsRsvpTeOperStatusSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 0, "%i", i4SetValFsMplsRsvpTeOperStatus)
#define nmhSetFsMplsRsvpTeOverRideOption(i4SetValFsMplsRsvpTeOverRideOption) \
 nmhSetCmn(FsMplsRsvpTeOverRideOption, 11, FsMplsRsvpTeOverRideOptionSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 0, "%i", i4SetValFsMplsRsvpTeOverRideOption)
#define nmhSetFsMplsRsvpTeMinTnlsWithMsgId(u4SetValFsMplsRsvpTeMinTnlsWithMsgId) \
 nmhSetCmn(FsMplsRsvpTeMinTnlsWithMsgId, 11, FsMplsRsvpTeMinTnlsWithMsgIdSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 0, "%u", u4SetValFsMplsRsvpTeMinTnlsWithMsgId)
#define nmhSetFsMplsRsvpTeNotificationEnabled(i4SetValFsMplsRsvpTeNotificationEnabled) \
 nmhSetCmn(FsMplsRsvpTeNotificationEnabled, 11, FsMplsRsvpTeNotificationEnabledSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 0, "%i", i4SetValFsMplsRsvpTeNotificationEnabled)
#define nmhSetFsMplsRsvpTeNotifyMsgRetransmitIntvl(u4SetValFsMplsRsvpTeNotifyMsgRetransmitIntvl) \
 nmhSetCmn(FsMplsRsvpTeNotifyMsgRetransmitIntvl, 11, FsMplsRsvpTeNotifyMsgRetransmitIntvlSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 0, "%u", u4SetValFsMplsRsvpTeNotifyMsgRetransmitIntvl)
#define nmhSetFsMplsRsvpTeNotifyMsgRetransmitDecay(u4SetValFsMplsRsvpTeNotifyMsgRetransmitDecay) \
 nmhSetCmn(FsMplsRsvpTeNotifyMsgRetransmitDecay, 11, FsMplsRsvpTeNotifyMsgRetransmitDecaySet, RsvpTeLock, RsvpTeUnLock, 0, 0, 0, "%u", u4SetValFsMplsRsvpTeNotifyMsgRetransmitDecay)
#define nmhSetFsMplsRsvpTeNotifyMsgRetransmitLimit(u4SetValFsMplsRsvpTeNotifyMsgRetransmitLimit) \
 nmhSetCmn(FsMplsRsvpTeNotifyMsgRetransmitLimit, 11, FsMplsRsvpTeNotifyMsgRetransmitLimitSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 0, "%u", u4SetValFsMplsRsvpTeNotifyMsgRetransmitLimit)
#define nmhSetFsMplsRsvpTeAdminStatusTimeIntvl(u4SetValFsMplsRsvpTeAdminStatusTimeIntvl) \
 nmhSetCmn(FsMplsRsvpTeAdminStatusTimeIntvl, 11, FsMplsRsvpTeAdminStatusTimeIntvlSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 0, "%u", u4SetValFsMplsRsvpTeAdminStatusTimeIntvl)
#define nmhSetFsMplsRsvpTePathStateRemovedSupport(i4SetValFsMplsRsvpTePathStateRemovedSupport) \
 nmhSetCmn(FsMplsRsvpTePathStateRemovedSupport, 11, FsMplsRsvpTePathStateRemovedSupportSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 0, "%i", i4SetValFsMplsRsvpTePathStateRemovedSupport)
#define nmhSetFsMplsRsvpTeLabelSetEnabled(i4SetValFsMplsRsvpTeLabelSetEnabled) \
 nmhSetCmn(FsMplsRsvpTeLabelSetEnabled, 11, FsMplsRsvpTeLabelSetEnabledSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 0, "%i", i4SetValFsMplsRsvpTeLabelSetEnabled)
#define nmhSetFsMplsRsvpTeAdminStatusCapabiliy(i4SetValFsMplsRsvpTeAdminStatusCapabiliy) \
 nmhSetCmn(FsMplsRsvpTeAdminStatusCapabiliy, 11, FsMplsRsvpTeAdminStatusCapabiliySet, RsvpTeLock, RsvpTeUnLock, 0, 0, 0, "%i", i4SetValFsMplsRsvpTeAdminStatusCapabiliy)
#define nmhSetFsMplsRsvpTeGrCapability(i4SetValFsMplsRsvpTeGrCapability) \
 nmhSetCmn(FsMplsRsvpTeGrCapability, 11, FsMplsRsvpTeGrCapabilitySet, RsvpTeLock, RsvpTeUnLock, 0, 0, 0, "%i", i4SetValFsMplsRsvpTeGrCapability)
#define nmhSetFsMplsRsvpTeGrRecoveryPathCapability(pSetValFsMplsRsvpTeGrRecoveryPathCapability) \
 nmhSetCmn(FsMplsRsvpTeGrRecoveryPathCapability, 11, FsMplsRsvpTeGrRecoveryPathCapabilitySet, RsvpTeLock, RsvpTeUnLock, 0, 0, 0, "%s", pSetValFsMplsRsvpTeGrRecoveryPathCapability)
#define nmhSetFsMplsRsvpTeGrRestartTime(i4SetValFsMplsRsvpTeGrRestartTime) \
 nmhSetCmn(FsMplsRsvpTeGrRestartTime, 11, FsMplsRsvpTeGrRestartTimeSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 0, "%i", i4SetValFsMplsRsvpTeGrRestartTime)
#define nmhSetFsMplsRsvpTeGrRecoveryTime(i4SetValFsMplsRsvpTeGrRecoveryTime) \
 nmhSetCmn(FsMplsRsvpTeGrRecoveryTime, 11, FsMplsRsvpTeGrRecoveryTimeSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 0, "%i", i4SetValFsMplsRsvpTeGrRecoveryTime)
#define nmhSetFsMplsRsvpTeReoptimizeTime(i4SetValFsMplsRsvpTeReoptimizeTime)    \
    nmhSetCmn(FsMplsRsvpTeReoptimizeTime, 11, FsMplsRsvpTeReoptimizeTimeSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsMplsRsvpTeReoptimizeTime)
#define nmhSetFsMplsRsvpTeEroCacheTime(i4SetValFsMplsRsvpTeEroCacheTime)    \
    nmhSetCmn(FsMplsRsvpTeEroCacheTime, 11, FsMplsRsvpTeEroCacheTimeSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsMplsRsvpTeEroCacheTime)
#endif

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsL3VpnRsvpTeMapPrefixType[13];
extern UINT4 FsMplsL3VpnRsvpTeMapPrefix[13];
extern UINT4 FsMplsL3VpnRsvpTeMapMaskType[13];
extern UINT4 FsMplsL3VpnRsvpTeMapMask[13];
extern UINT4 FsMplsL3VpnRsvpTeMapTnlIndex[13];
extern UINT4 FsMplsL3VpnRsvpTeMapRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsL3VpnRsvpTeMapTnlIndex(i4FsMplsL3VpnRsvpTeMapPrefixType , pFsMplsL3VpnRsvpTeMapPrefix , i4FsMplsL3VpnRsvpTeMapMaskType , pFsMplsL3VpnRsvpTeMapMask ,i4SetValFsMplsL3VpnRsvpTeMapTnlIndex) \
 nmhSetCmnWithLock(FsMplsL3VpnRsvpTeMapTnlIndex, 13, FsMplsL3VpnRsvpTeMapTnlIndexSet, RsvpTeLock, RsvpTeUnLock, 0, 0, 4, "%i %s %i %s %i", i4FsMplsL3VpnRsvpTeMapPrefixType , pFsMplsL3VpnRsvpTeMapPrefix , i4FsMplsL3VpnRsvpTeMapMaskType , pFsMplsL3VpnRsvpTeMapMask ,i4SetValFsMplsL3VpnRsvpTeMapTnlIndex)
#define nmhSetFsMplsL3VpnRsvpTeMapRowStatus(i4FsMplsL3VpnRsvpTeMapPrefixType , pFsMplsL3VpnRsvpTeMapPrefix , i4FsMplsL3VpnRsvpTeMapMaskType , pFsMplsL3VpnRsvpTeMapMask ,i4SetValFsMplsL3VpnRsvpTeMapRowStatus) \
 nmhSetCmnWithLock(FsMplsL3VpnRsvpTeMapRowStatus, 13, FsMplsL3VpnRsvpTeMapRowStatusSet, RsvpTeLock, RsvpTeUnLock, 0, 1, 4, "%i %s %i %s %i", i4FsMplsL3VpnRsvpTeMapPrefixType , pFsMplsL3VpnRsvpTeMapPrefix , i4FsMplsL3VpnRsvpTeMapMaskType , pFsMplsL3VpnRsvpTeMapMask ,i4SetValFsMplsL3VpnRsvpTeMapRowStatus)

#endif
