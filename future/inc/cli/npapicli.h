
/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: npapicli.h,v 1.1 2011/03/24 11:01:22 siva Exp $
 *
 * Description: NPAPI UT Test cases.
 *
 * ***********************************************************************/

#include "lr.h"
#include "cli.h"
#define CLI_NPAPI_UT_TEST 1

INT4 cli_process_npapi_test_cmd(tCliHandle CliHandle, UINT4 u4Command,...);
