/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsrtm6cli.h,v 1.3 2015/10/19 12:19:29 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsRrd6RouterId[10];
extern UINT4 FsRrd6FilterByOspfTag[10];
extern UINT4 FsRrd6FilterOspfTag[10];
extern UINT4 FsRrd6FilterOspfTagMask[10];
extern UINT4 FsRrd6RouterASNumber[10];
extern UINT4 FsRrd6AdminStatus[10];
extern UINT4 FsRrd6Trace[10];
extern UINT4 FsRrd6ThrotLimit[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsRrd6RouterId(u4SetValFsRrd6RouterId) \
 nmhSetCmn(FsRrd6RouterId, 10, FsRrd6RouterIdSet, Rtm6Lock, Rtm6UnLock, 0, 0, 0, "%p", u4SetValFsRrd6RouterId)
#define nmhSetFsRrd6FilterByOspfTag(i4SetValFsRrd6FilterByOspfTag) \
 nmhSetCmn(FsRrd6FilterByOspfTag, 10, FsRrd6FilterByOspfTagSet, Rtm6Lock, Rtm6UnLock, 0, 0, 0, "%i", i4SetValFsRrd6FilterByOspfTag)
#define nmhSetFsRrd6FilterOspfTag(i4SetValFsRrd6FilterOspfTag) \
 nmhSetCmn(FsRrd6FilterOspfTag, 10, FsRrd6FilterOspfTagSet, Rtm6Lock, Rtm6UnLock, 0, 0, 0, "%i", i4SetValFsRrd6FilterOspfTag)
#define nmhSetFsRrd6FilterOspfTagMask(i4SetValFsRrd6FilterOspfTagMask) \
 nmhSetCmn(FsRrd6FilterOspfTagMask, 10, FsRrd6FilterOspfTagMaskSet, Rtm6Lock, Rtm6UnLock, 0, 0, 0, "%i", i4SetValFsRrd6FilterOspfTagMask)
#define nmhSetFsRrd6RouterASNumber(i4SetValFsRrd6RouterASNumber) \
 nmhSetCmn(FsRrd6RouterASNumber, 10, FsRrd6RouterASNumberSet, Rtm6Lock, Rtm6UnLock, 0, 0, 0, "%i", i4SetValFsRrd6RouterASNumber)
#define nmhSetFsRrd6AdminStatus(i4SetValFsRrd6AdminStatus) \
 nmhSetCmn(FsRrd6AdminStatus, 10, FsRrd6AdminStatusSet, Rtm6Lock, Rtm6UnLock, 0, 0, 0, "%i", i4SetValFsRrd6AdminStatus)
#define nmhSetFsRrd6Trace(u4SetValFsRrd6Trace) \
 nmhSetCmn(FsRrd6Trace, 10, FsRrd6TraceSet, Rtm6Lock, Rtm6UnLock, 0, 0, 0, "%u", u4SetValFsRrd6Trace)
#define nmhSetFsRrd6ThrotLimit(u4SetValFsRrd6ThrotLimit) \
 nmhSetCmn(FsRrd6ThrotLimit, 10, FsRrd6ThrotLimitSet, Rtm6Lock, Rtm6UnLock, 0, 0, 0, "%u", u4SetValFsRrd6ThrotLimit)
#define nmhSetFsRtm6StaticRouteDistance(i4SetValFsMIRtm6StaticRouteDistance) \
 nmhSetCmn(FsRtm6StaticRouteDistance, 10, FsRtm6StaticRouteDistanceSet, Rtm6Lock, Rtm6UnLock, 0, 0, 0, "%i", i4SetValFsMIRtm6StaticRouteDistance)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsRrd6ControlDestIpAddress[11];
extern UINT4 FsRrd6ControlNetMaskLen[11];
extern UINT4 FsRrd6ControlSourceProto[11];
extern UINT4 FsRrd6ControlDestProto[11];
extern UINT4 FsRrd6ControlRouteExportFlag[11];
extern UINT4 FsRrd6ControlRowStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsRrd6ControlSourceProto(pFsRrd6ControlDestIpAddress , i4FsRrd6ControlNetMaskLen ,i4SetValFsRrd6ControlSourceProto) \
 nmhSetCmn(FsRrd6ControlSourceProto, 11, FsRrd6ControlSourceProtoSet, Rtm6Lock, Rtm6UnLock, 0, 0, 2, "%s %i %i", pFsRrd6ControlDestIpAddress , i4FsRrd6ControlNetMaskLen ,i4SetValFsRrd6ControlSourceProto)
#define nmhSetFsRrd6ControlDestProto(pFsRrd6ControlDestIpAddress , i4FsRrd6ControlNetMaskLen ,i4SetValFsRrd6ControlDestProto) \
 nmhSetCmn(FsRrd6ControlDestProto, 11, FsRrd6ControlDestProtoSet, Rtm6Lock, Rtm6UnLock, 0, 0, 2, "%s %i %i", pFsRrd6ControlDestIpAddress , i4FsRrd6ControlNetMaskLen ,i4SetValFsRrd6ControlDestProto)
#define nmhSetFsRrd6ControlRouteExportFlag(pFsRrd6ControlDestIpAddress , i4FsRrd6ControlNetMaskLen ,i4SetValFsRrd6ControlRouteExportFlag) \
 nmhSetCmn(FsRrd6ControlRouteExportFlag, 11, FsRrd6ControlRouteExportFlagSet, Rtm6Lock, Rtm6UnLock, 0, 0, 2, "%s %i %i", pFsRrd6ControlDestIpAddress , i4FsRrd6ControlNetMaskLen ,i4SetValFsRrd6ControlRouteExportFlag)
#define nmhSetFsRrd6ControlRowStatus(pFsRrd6ControlDestIpAddress , i4FsRrd6ControlNetMaskLen ,i4SetValFsRrd6ControlRowStatus) \
 nmhSetCmn(FsRrd6ControlRowStatus, 11, FsRrd6ControlRowStatusSet, Rtm6Lock, Rtm6UnLock, 0, 1, 2, "%s %i %i", pFsRrd6ControlDestIpAddress , i4FsRrd6ControlNetMaskLen ,i4SetValFsRrd6ControlRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsRrd6RoutingProtoId[11];
extern UINT4 FsRrd6AllowOspfAreaRoutes[11];
extern UINT4 FsRrd6AllowOspfExtRoutes[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsRrd6AllowOspfAreaRoutes(i4FsRrd6RoutingProtoId ,i4SetValFsRrd6AllowOspfAreaRoutes) \
 nmhSetCmn(FsRrd6AllowOspfAreaRoutes, 11, FsRrd6AllowOspfAreaRoutesSet, Rtm6Lock, Rtm6UnLock, 0, 0, 1, "%i %i", i4FsRrd6RoutingProtoId ,i4SetValFsRrd6AllowOspfAreaRoutes)
#define nmhSetFsRrd6AllowOspfExtRoutes(i4FsRrd6RoutingProtoId ,i4SetValFsRrd6AllowOspfExtRoutes) \
 nmhSetCmn(FsRrd6AllowOspfExtRoutes, 11, FsRrd6AllowOspfExtRoutesSet, Rtm6Lock, Rtm6UnLock, 0, 0, 1, "%i %i", i4FsRrd6RoutingProtoId ,i4SetValFsRrd6AllowOspfExtRoutes)

#endif
