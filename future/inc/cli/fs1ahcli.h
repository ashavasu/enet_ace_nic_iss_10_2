/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fs1ahcli.h,v 1.2 2008/12/03 08:27:10 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPbbBackboneEdgeBridgeName[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPbbBackboneEdgeBridgeName(pSetValFsPbbBackboneEdgeBridgeName)	\
	nmhSetCmn(FsPbbBackboneEdgeBridgeName, 13, FsPbbBackboneEdgeBridgeNameSet, NULL, NULL, 0, 0, 0, "%s", pSetValFsPbbBackboneEdgeBridgeName)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsdot1ahContextId[14];
extern UINT4 FsPbbVipISid[14];
extern UINT4 FsPbbVipDefaultDstBMAC[14];
extern UINT4 FsPbbVipType[14];
extern UINT4 FsPbbVipRowStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPbbVipISid(i4Fsdot1ahContextId , i4IfIndex ,u4SetValFsPbbVipISid)	\
	nmhSetCmn(FsPbbVipISid, 14, FsPbbVipISidSet, NULL, NULL, 0, 0, 2, "%i %i %u", i4Fsdot1ahContextId , i4IfIndex ,u4SetValFsPbbVipISid)
#define nmhSetFsPbbVipDefaultDstBMAC(i4Fsdot1ahContextId , i4IfIndex ,SetValFsPbbVipDefaultDstBMAC)	\
	nmhSetCmn(FsPbbVipDefaultDstBMAC, 14, FsPbbVipDefaultDstBMACSet, NULL, NULL, 0, 0, 2, "%i %i %m", i4Fsdot1ahContextId , i4IfIndex ,SetValFsPbbVipDefaultDstBMAC)
#define nmhSetFsPbbVipType(i4Fsdot1ahContextId , i4IfIndex ,pSetValFsPbbVipType)	\
	nmhSetCmn(FsPbbVipType, 14, FsPbbVipTypeSet, NULL, NULL, 0, 0, 2, "%i %i %s", i4Fsdot1ahContextId , i4IfIndex ,pSetValFsPbbVipType)
#define nmhSetFsPbbVipRowStatus(i4Fsdot1ahContextId , i4IfIndex ,i4SetValFsPbbVipRowStatus)	\
	nmhSetCmn(FsPbbVipRowStatus, 14, FsPbbVipRowStatusSet, NULL, NULL, 0, 1, 2, "%i %i %i", i4Fsdot1ahContextId , i4IfIndex ,i4SetValFsPbbVipRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPbbPipIfIndex[14];
extern UINT4 FsPbbPipBMACAddress[14];
extern UINT4 FsPbbPipName[14];
extern UINT4 FsPbbPipRowStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPbbPipBMACAddress(i4FsPbbPipIfIndex ,SetValFsPbbPipBMACAddress)	\
	nmhSetCmn(FsPbbPipBMACAddress, 14, FsPbbPipBMACAddressSet, NULL, NULL, 0, 0, 1, "%i %m", i4FsPbbPipIfIndex ,SetValFsPbbPipBMACAddress)
#define nmhSetFsPbbPipName(i4FsPbbPipIfIndex ,pSetValFsPbbPipName)	\
	nmhSetCmn(FsPbbPipName, 14, FsPbbPipNameSet, NULL, NULL, 0, 0, 1, "%i %s", i4FsPbbPipIfIndex ,pSetValFsPbbPipName)
#define nmhSetFsPbbPipRowStatus(i4FsPbbPipIfIndex ,i4SetValFsPbbPipRowStatus)	\
	nmhSetCmn(FsPbbPipRowStatus, 14, FsPbbPipRowStatusSet, NULL, NULL, 0, 1, 1, "%i %i", i4FsPbbPipIfIndex ,i4SetValFsPbbPipRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPbbVipToPipMappingPipIfIndex[14];
extern UINT4 FsPbbVipToPipMappingStorageType[14];
extern UINT4 FsPbbVipToPipMappingRowStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPbbVipToPipMappingPipIfIndex(i4Fsdot1ahContextId , i4IfIndex ,i4SetValFsPbbVipToPipMappingPipIfIndex)	\
	nmhSetCmn(FsPbbVipToPipMappingPipIfIndex, 14, FsPbbVipToPipMappingPipIfIndexSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4Fsdot1ahContextId , i4IfIndex ,i4SetValFsPbbVipToPipMappingPipIfIndex)
#define nmhSetFsPbbVipToPipMappingStorageType(i4Fsdot1ahContextId , i4IfIndex ,i4SetValFsPbbVipToPipMappingStorageType)	\
	nmhSetCmn(FsPbbVipToPipMappingStorageType, 14, FsPbbVipToPipMappingStorageTypeSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4Fsdot1ahContextId , i4IfIndex ,i4SetValFsPbbVipToPipMappingStorageType)
#define nmhSetFsPbbVipToPipMappingRowStatus(i4Fsdot1ahContextId , i4IfIndex ,i4SetValFsPbbVipToPipMappingRowStatus)	\
	nmhSetCmn(FsPbbVipToPipMappingRowStatus, 14, FsPbbVipToPipMappingRowStatusSet, NULL, NULL, 0, 1, 2, "%i %i %i", i4Fsdot1ahContextId , i4IfIndex ,i4SetValFsPbbVipToPipMappingRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPbbCBPServiceMappingBackboneSid[14];
extern UINT4 FsPbbCBPServiceMappingBVid[14];
extern UINT4 FsPbbCBPServiceMappingDefaultBackboneDest[14];
extern UINT4 FsPbbCBPServiceMappingType[14];
extern UINT4 FsPbbCBPServiceMappingLocalSid[14];
extern UINT4 FsPbbCBPServiceMappingRowStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPbbCBPServiceMappingBVid(i4Fsdot1ahContextId , i4IfIndex , u4FsPbbCBPServiceMappingBackboneSid ,i4SetValFsPbbCBPServiceMappingBVid)	\
	nmhSetCmn(FsPbbCBPServiceMappingBVid, 14, FsPbbCBPServiceMappingBVidSet, NULL, NULL, 0, 0, 3, "%i %i %u %i", i4Fsdot1ahContextId , i4IfIndex , u4FsPbbCBPServiceMappingBackboneSid ,i4SetValFsPbbCBPServiceMappingBVid)
#define nmhSetFsPbbCBPServiceMappingDefaultBackboneDest(i4Fsdot1ahContextId , i4IfIndex , u4FsPbbCBPServiceMappingBackboneSid ,SetValFsPbbCBPServiceMappingDefaultBackboneDest)	\
	nmhSetCmn(FsPbbCBPServiceMappingDefaultBackboneDest, 14, FsPbbCBPServiceMappingDefaultBackboneDestSet, NULL, NULL, 0, 0, 3, "%i %i %u %m", i4Fsdot1ahContextId , i4IfIndex , u4FsPbbCBPServiceMappingBackboneSid ,SetValFsPbbCBPServiceMappingDefaultBackboneDest)
#define nmhSetFsPbbCBPServiceMappingType(i4Fsdot1ahContextId , i4IfIndex , u4FsPbbCBPServiceMappingBackboneSid ,pSetValFsPbbCBPServiceMappingType)	\
	nmhSetCmn(FsPbbCBPServiceMappingType, 14, FsPbbCBPServiceMappingTypeSet, NULL, NULL, 0, 0, 3, "%i %i %u %s", i4Fsdot1ahContextId , i4IfIndex , u4FsPbbCBPServiceMappingBackboneSid ,pSetValFsPbbCBPServiceMappingType)
#define nmhSetFsPbbCBPServiceMappingLocalSid(i4Fsdot1ahContextId , i4IfIndex , u4FsPbbCBPServiceMappingBackboneSid ,u4SetValFsPbbCBPServiceMappingLocalSid)	\
	nmhSetCmn(FsPbbCBPServiceMappingLocalSid, 14, FsPbbCBPServiceMappingLocalSidSet, NULL, NULL, 0, 0, 3, "%i %i %u %u", i4Fsdot1ahContextId , i4IfIndex , u4FsPbbCBPServiceMappingBackboneSid ,u4SetValFsPbbCBPServiceMappingLocalSid)
#define nmhSetFsPbbCBPServiceMappingRowStatus(i4Fsdot1ahContextId , i4IfIndex , u4FsPbbCBPServiceMappingBackboneSid ,i4SetValFsPbbCBPServiceMappingRowStatus)	\
	nmhSetCmn(FsPbbCBPServiceMappingRowStatus, 14, FsPbbCBPServiceMappingRowStatusSet, NULL, NULL, 0, 1, 3, "%i %i %u %i", i4Fsdot1ahContextId , i4IfIndex , u4FsPbbCBPServiceMappingBackboneSid ,i4SetValFsPbbCBPServiceMappingRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPbbCbpRowStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPbbCbpRowStatus(i4Fsdot1ahContextId , i4IfIndex ,i4SetValFsPbbCbpRowStatus)	\
	nmhSetCmn(FsPbbCbpRowStatus, 14, FsPbbCbpRowStatusSet, NULL, NULL, 0, 1, 2, "%i %i %i", i4Fsdot1ahContextId , i4IfIndex ,i4SetValFsPbbCbpRowStatus)

#endif
