/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdqoscli.h,v 1.1 2009/02/17 12:25:25 nswamy-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DiffServDataPathIfDirection[12];
extern UINT4 DiffServDataPathStart[12];
extern UINT4 DiffServDataPathStorage[12];
extern UINT4 DiffServDataPathStatus[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DiffServClfrId[12];
extern UINT4 DiffServClfrStorage[12];
extern UINT4 DiffServClfrStatus[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DiffServClfrElementId[12];
extern UINT4 DiffServClfrElementPrecedence[12];
extern UINT4 DiffServClfrElementNext[12];
extern UINT4 DiffServClfrElementSpecific[12];
extern UINT4 DiffServClfrElementStorage[12];
extern UINT4 DiffServClfrElementStatus[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DiffServMultiFieldClfrId[12];
extern UINT4 DiffServMultiFieldClfrAddrType[12];
extern UINT4 DiffServMultiFieldClfrDstAddr[12];
extern UINT4 DiffServMultiFieldClfrDstPrefixLength[12];
extern UINT4 DiffServMultiFieldClfrSrcAddr[12];
extern UINT4 DiffServMultiFieldClfrSrcPrefixLength[12];
extern UINT4 DiffServMultiFieldClfrDscp[12];
extern UINT4 DiffServMultiFieldClfrFlowId[12];
extern UINT4 DiffServMultiFieldClfrProtocol[12];
extern UINT4 DiffServMultiFieldClfrDstL4PortMin[12];
extern UINT4 DiffServMultiFieldClfrDstL4PortMax[12];
extern UINT4 DiffServMultiFieldClfrSrcL4PortMin[12];
extern UINT4 DiffServMultiFieldClfrSrcL4PortMax[12];
extern UINT4 DiffServMultiFieldClfrStorage[12];
extern UINT4 DiffServMultiFieldClfrStatus[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DiffServMeterId[12];
extern UINT4 DiffServMeterSucceedNext[12];
extern UINT4 DiffServMeterFailNext[12];
extern UINT4 DiffServMeterSpecific[12];
extern UINT4 DiffServMeterStorage[12];
extern UINT4 DiffServMeterStatus[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DiffServTBParamId[12];
extern UINT4 DiffServTBParamType[12];
extern UINT4 DiffServTBParamRate[12];
extern UINT4 DiffServTBParamBurstSize[12];
extern UINT4 DiffServTBParamInterval[12];
extern UINT4 DiffServTBParamStorage[12];
extern UINT4 DiffServTBParamStatus[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DiffServActionId[12];
extern UINT4 DiffServActionInterface[12];
extern UINT4 DiffServActionNext[12];
extern UINT4 DiffServActionSpecific[12];
extern UINT4 DiffServActionStorage[12];
extern UINT4 DiffServActionStatus[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DiffServCountActId[12];
extern UINT4 DiffServCountActStorage[12];
extern UINT4 DiffServCountActStatus[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DiffServAlgDropId[12];
extern UINT4 DiffServAlgDropType[12];
extern UINT4 DiffServAlgDropNext[12];
extern UINT4 DiffServAlgDropQMeasure[12];
extern UINT4 DiffServAlgDropQThreshold[12];
extern UINT4 DiffServAlgDropSpecific[12];
extern UINT4 DiffServAlgDropStorage[12];
extern UINT4 DiffServAlgDropStatus[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DiffServRandomDropId[12];
extern UINT4 DiffServRandomDropMinThreshBytes[12];
extern UINT4 DiffServRandomDropMinThreshPkts[12];
extern UINT4 DiffServRandomDropMaxThreshBytes[12];
extern UINT4 DiffServRandomDropMaxThreshPkts[12];
extern UINT4 DiffServRandomDropProbMax[12];
extern UINT4 DiffServRandomDropWeight[12];
extern UINT4 DiffServRandomDropSamplingRate[12];
extern UINT4 DiffServRandomDropStorage[12];
extern UINT4 DiffServRandomDropStatus[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DiffServQId[12];
extern UINT4 DiffServQNext[12];
extern UINT4 DiffServQMinRate[12];
extern UINT4 DiffServQMaxRate[12];
extern UINT4 DiffServQStorage[12];
extern UINT4 DiffServQStatus[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DiffServSchedulerId[12];
extern UINT4 DiffServSchedulerNext[12];
extern UINT4 DiffServSchedulerMethod[12];
extern UINT4 DiffServSchedulerMinRate[12];
extern UINT4 DiffServSchedulerMaxRate[12];
extern UINT4 DiffServSchedulerStorage[12];
extern UINT4 DiffServSchedulerStatus[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DiffServMinRateId[12];
extern UINT4 DiffServMinRatePriority[12];
extern UINT4 DiffServMinRateAbsolute[12];
extern UINT4 DiffServMinRateRelative[12];
extern UINT4 DiffServMinRateStorage[12];
extern UINT4 DiffServMinRateStatus[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DiffServMaxRateId[12];
extern UINT4 DiffServMaxRateLevel[12];
extern UINT4 DiffServMaxRateAbsolute[12];
extern UINT4 DiffServMaxRateRelative[12];
extern UINT4 DiffServMaxRateThreshold[12];
extern UINT4 DiffServMaxRateStorage[12];
extern UINT4 DiffServMaxRateStatus[12];
