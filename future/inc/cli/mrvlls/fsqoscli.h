/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsqoscli.h,v 1.2 2011/02/09 11:56:54 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsQoSSystemControl[11];
extern UINT4 FsQoSStatus[11];
extern UINT4 FsQoSTrcFlag[11];
extern UINT4 FsQosSchedulingAlgorithm[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsQoSSystemControl(i4SetValFsQoSSystemControl)    \
        nmhSetCmn(FsQoSSystemControl, 11, FsQoSSystemControlSet, QoSLock, QoSUnLock, 0, 0, 0, "%i", i4SetValFsQoSSystemControl)
#define nmhSetFsQoSStatus(i4SetValFsQoSStatus)  \
        nmhSetCmn(FsQoSStatus, 11, FsQoSStatusSet, QoSLock, QoSUnLock, 0, 0, 0, "%i", i4SetValFsQoSStatus)
#define nmhSetFsQoSTrcFlag(u4SetValFsQoSTrcFlag)        \
        nmhSetCmn(FsQoSTrcFlag, 11, FsQoSTrcFlagSet, QoSLock, QoSUnLock, 0, 0, 0, "%u", u4SetValFsQoSTrcFlag)
#define nmhSetFsQosSchedulingAlgorithm(i4SetValFsQosSchedulingAlgorithm)        \
        nmhSetCmn(FsQosSchedulingAlgorithm, 11, FsQosSchedulingAlgorithmSet, QoSLock, QoSUnLock, 0, 0, 0, "%i", i4SetValFsQosSchedulingAlgorithm)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsQoSPriorityMapID[13];
extern UINT4 FsQoSPriorityMapName[13];
extern UINT4 FsQoSPriorityMapIfIndex[13];
extern UINT4 FsQoSPriorityMapVlanId[13];
extern UINT4 FsQoSPriorityMapInPriority[13];
extern UINT4 FsQoSPriorityMapInPriType[13];
extern UINT4 FsQoSPriorityMapRegenPriority[13];
extern UINT4 FsQoSPriorityMapMacAddress[13];
extern UINT4 FsQoSPriorityMapStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsQoSPriorityMapName(u4FsQoSPriorityMapID ,pSetValFsQoSPriorityMapName)   \
        nmhSetCmn(FsQoSPriorityMapName, 13, FsQoSPriorityMapNameSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %s", u4FsQoSPriorityMapID ,pSetValFsQoSPriorityMapName)
#define nmhSetFsQoSPriorityMapIfIndex(u4FsQoSPriorityMapID ,u4SetValFsQoSPriorityMapIfIndex)    \
        nmhSetCmn(FsQoSPriorityMapIfIndex, 13, FsQoSPriorityMapIfIndexSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSPriorityMapID ,u4SetValFsQoSPriorityMapIfIndex)
#define nmhSetFsQoSPriorityMapVlanId(u4FsQoSPriorityMapID ,u4SetValFsQoSPriorityMapVlanId)      \
        nmhSetCmn(FsQoSPriorityMapVlanId, 13, FsQoSPriorityMapVlanIdSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSPriorityMapID ,u4SetValFsQoSPriorityMapVlanId)
#define nmhSetFsQoSPriorityMapInPriority(u4FsQoSPriorityMapID ,i4SetValFsQoSPriorityMapInPriority)      \
        nmhSetCmn(FsQoSPriorityMapInPriority, 13, FsQoSPriorityMapInPrioritySet, QoSLock, QoSUnLock, 0, 0, 1, "%u %i", u4FsQoSPriorityMapID ,i4SetValFsQoSPriorityMapInPriority)
#define nmhSetFsQoSPriorityMapInPriType(u4FsQoSPriorityMapID ,i4SetValFsQoSPriorityMapInPriType)        \
        nmhSetCmn(FsQoSPriorityMapInPriType, 13, FsQoSPriorityMapInPriTypeSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %i", u4FsQoSPriorityMapID ,i4SetValFsQoSPriorityMapInPriType)
#define nmhSetFsQoSPriorityMapRegenPriority(u4FsQoSPriorityMapID ,u4SetValFsQoSPriorityMapRegenPriority)        \
        nmhSetCmn(FsQoSPriorityMapRegenPriority, 13, FsQoSPriorityMapRegenPrioritySet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSPriorityMapID ,u4SetValFsQoSPriorityMapRegenPriority)
#define nmhSetFsQoSPriorityMapMacAddress(u4FsQoSPriorityMapID ,SetValFsQoSPriorityMapMacAddress)        \
        nmhSetCmn(FsQoSPriorityMapMacAddress, 13, FsQoSPriorityMapMacAddressSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %m", u4FsQoSPriorityMapID ,SetValFsQoSPriorityMapMacAddress)
#define nmhSetFsQoSPriorityMapStatus(u4FsQoSPriorityMapID ,i4SetValFsQoSPriorityMapStatus)      \
        nmhSetCmn(FsQoSPriorityMapStatus, 13, FsQoSPriorityMapStatusSet, QoSLock, QoSUnLock, 0, 1, 1, "%u %i", u4FsQoSPriorityMapID ,i4SetValFsQoSPriorityMapStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsQoSQMapRegenPriType[13];
extern UINT4 FsQoSQMapRegenPriority[13];
extern UINT4 FsQoSQMapQId[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsQoSQMapQId(i4FsQoSQMapRegenPriType , u4FsQoSQMapRegenPriority ,u4SetValFsQoSQMapQId)    \
        nmhSetCmn(FsQoSQMapQId, 13, FsQoSQMapQIdSet, QoSLock, QoSUnLock, 0, 0, 2, "%i %u %u", i4FsQoSQMapRegenPriType , u4FsQoSQMapRegenPriority ,u4SetValFsQoSQMapQId)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsQoSIfIndex[13];
extern UINT4 FsQoSVlanPriPriority[13];
extern UINT4 FsQoSIPDscpPriority[13];
extern UINT4 FsQoSVlanIPPriority[13];
extern UINT4 FsQoSVIDOverride[13];
extern UINT4 FsQoSSAOverride[13];
extern UINT4 FsQoSDAOverride[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsQoSVlanPriPriority(u4FsQoSIfIndex ,i4SetValFsQoSVlanPriPriority)        \
        nmhSetCmn(FsQoSVlanPriPriority, 13, FsQoSVlanPriPrioritySet, QoSLock, QoSUnLock, 0, 0, 1, "%u %i", u4FsQoSIfIndex ,i4SetValFsQoSVlanPriPriority)
#define nmhSetFsQoSIPDscpPriority(u4FsQoSIfIndex ,i4SetValFsQoSIPDscpPriority)  \
        nmhSetCmn(FsQoSIPDscpPriority, 13, FsQoSIPDscpPrioritySet, QoSLock, QoSUnLock, 0, 0, 1, "%u %i", u4FsQoSIfIndex ,i4SetValFsQoSIPDscpPriority)
#define nmhSetFsQoSVlanIPPriority(u4FsQoSIfIndex ,i4SetValFsQoSVlanIPPriority)  \
        nmhSetCmn(FsQoSVlanIPPriority, 13, FsQoSVlanIPPrioritySet, QoSLock, QoSUnLock, 0, 0, 1, "%u %i", u4FsQoSIfIndex ,i4SetValFsQoSVlanIPPriority)
#define nmhSetFsQoSVIDOverride(u4FsQoSIfIndex ,i4SetValFsQoSVIDOverride)        \
        nmhSetCmn(FsQoSVIDOverride, 13, FsQoSVIDOverrideSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %i", u4FsQoSIfIndex ,i4SetValFsQoSVIDOverride)
#define nmhSetFsQoSSAOverride(u4FsQoSIfIndex ,i4SetValFsQoSSAOverride)  \
        nmhSetCmn(FsQoSSAOverride, 13, FsQoSSAOverrideSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %i", u4FsQoSIfIndex ,i4SetValFsQoSSAOverride)
#define nmhSetFsQoSDAOverride(u4FsQoSIfIndex ,i4SetValFsQoSDAOverride)  \
        nmhSetCmn(FsQoSDAOverride, 13, FsQoSDAOverrideSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %i", u4FsQoSIfIndex ,i4SetValFsQoSDAOverride)

#endif
