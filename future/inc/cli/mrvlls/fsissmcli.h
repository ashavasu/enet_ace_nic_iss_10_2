/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsissmcli.h,v 1.2 2011/02/09 11:56:54 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IssMetroL2FilterOuterEtherType[14];
extern UINT4 IssMetroL2FilterSVlanId[14];
extern UINT4 IssMetroL2FilterSVlanPriority[14];
extern UINT4 IssMetroL2FilterCVlanPriority[14];
extern UINT4 IssMetroL2FilterPacketTagType[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIssMetroL2FilterOuterEtherType(i4IssL2FilterNo ,i4SetValIssMetroL2FilterOuterEtherType)   \
        nmhSetCmn(IssMetroL2FilterOuterEtherType, 14, IssMetroL2FilterOuterEtherTypeSet, NULL, NULL, 0, 0, 1, "%i %i", i4IssL2FilterNo ,i4SetValIssMetroL2FilterOuterEtherType)
#define nmhSetIssMetroL2FilterSVlanId(i4IssL2FilterNo ,i4SetValIssMetroL2FilterSVlanId) \
        nmhSetCmn(IssMetroL2FilterSVlanId, 14, IssMetroL2FilterSVlanIdSet, NULL, NULL, 0, 0, 1, "%i %i", i4IssL2FilterNo ,i4SetValIssMetroL2FilterSVlanId)
#define nmhSetIssMetroL2FilterSVlanPriority(i4IssL2FilterNo ,i4SetValIssMetroL2FilterSVlanPriority)     \
        nmhSetCmn(IssMetroL2FilterSVlanPriority, 14, IssMetroL2FilterSVlanPrioritySet, NULL, NULL, 0, 0, 1, "%i %i", i4IssL2FilterNo ,i4SetValIssMetroL2FilterSVlanPriority)
#define nmhSetIssMetroL2FilterCVlanPriority(i4IssL2FilterNo ,i4SetValIssMetroL2FilterCVlanPriority)     \
        nmhSetCmn(IssMetroL2FilterCVlanPriority, 14, IssMetroL2FilterCVlanPrioritySet, NULL, NULL, 0, 0, 1, "%i %i", i4IssL2FilterNo ,i4SetValIssMetroL2FilterCVlanPriority)
#define nmhSetIssMetroL2FilterPacketTagType(i4IssL2FilterNo ,i4SetValIssMetroL2FilterPacketTagType)     \
        nmhSetCmn(IssMetroL2FilterPacketTagType, 14, IssMetroL2FilterPacketTagTypeSet, NULL, NULL, 0, 0, 1, "%i %i", i4IssL2FilterNo ,i4SetValIssMetroL2FilterPacketTagType)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IssMetroL3FilterSVlanId[14];
extern UINT4 IssMetroL3FilterSVlanPriority[14];
extern UINT4 IssMetroL3FilterCVlanId[14];
extern UINT4 IssMetroL3FilterCVlanPriority[14];
extern UINT4 IssMetroL3FilterPacketTagType[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIssMetroL3FilterSVlanId(i4IssL3FilterNo ,i4SetValIssMetroL3FilterSVlanId) \
        nmhSetCmn(IssMetroL3FilterSVlanId, 14, IssMetroL3FilterSVlanIdSet, NULL, NULL, 0, 0, 1, "%i %i", i4IssL3FilterNo ,i4SetValIssMetroL3FilterSVlanId)
#define nmhSetIssMetroL3FilterSVlanPriority(i4IssL3FilterNo ,i4SetValIssMetroL3FilterSVlanPriority)     \
        nmhSetCmn(IssMetroL3FilterSVlanPriority, 14, IssMetroL3FilterSVlanPrioritySet, NULL, NULL, 0, 0, 1, "%i %i", i4IssL3FilterNo ,i4SetValIssMetroL3FilterSVlanPriority)
#define nmhSetIssMetroL3FilterCVlanId(i4IssL3FilterNo ,i4SetValIssMetroL3FilterCVlanId) \
        nmhSetCmn(IssMetroL3FilterCVlanId, 14, IssMetroL3FilterCVlanIdSet, NULL, NULL, 0, 0, 1, "%i %i", i4IssL3FilterNo ,i4SetValIssMetroL3FilterCVlanId)
#define nmhSetIssMetroL3FilterCVlanPriority(i4IssL3FilterNo ,i4SetValIssMetroL3FilterCVlanPriority)     \
        nmhSetCmn(IssMetroL3FilterCVlanPriority, 14, IssMetroL3FilterCVlanPrioritySet, NULL, NULL, 0, 0, 1, "%i %i", i4IssL3FilterNo ,i4SetValIssMetroL3FilterCVlanPriority)
#define nmhSetIssMetroL3FilterPacketTagType(i4IssL3FilterNo ,i4SetValIssMetroL3FilterPacketTagType)     \
        nmhSetCmn(IssMetroL3FilterPacketTagType, 14, IssMetroL3FilterPacketTagTypeSet, NULL, NULL, 0, 0, 1, "%i %i", i4IssL3FilterNo ,i4SetValIssMetroL3FilterPacketTagType)

#endif
