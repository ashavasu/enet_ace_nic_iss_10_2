/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsissecli.h,v 1.2 2011/02/09 11:56:54 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IssExtRateCtrlIndex[13];
extern UINT4 IssExtRateCtrlPackets[13];
extern UINT4 IssExtRateCtrlLimitValue[13];
extern UINT4 IssExtRateCtrlPortRateLimit[13];
extern UINT4 IssExtRateCtrlPortBurstSize[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIssExtRateCtrlPackets(i4IssExtRateCtrlIndex ,i4SetValIssExtRateCtrlPackets)       \
        nmhSetCmn(IssExtRateCtrlPackets, 13, IssExtRateCtrlPacketsSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssExtRateCtrlIndex ,i4SetValIssExtRateCtrlPackets)
#define nmhSetIssExtRateCtrlLimitValue(i4IssExtRateCtrlIndex ,i4SetValIssExtRateCtrlLimitValue) \
        nmhSetCmn(IssExtRateCtrlLimitValue, 13, IssExtRateCtrlLimitValueSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssExtRateCtrlIndex ,i4SetValIssExtRateCtrlLimitValue)
#define nmhSetIssExtRateCtrlPortRateLimit(i4IssExtRateCtrlIndex ,i4SetValIssExtRateCtrlPortRateLimit)   \
        nmhSetCmn(IssExtRateCtrlPortRateLimit, 13, IssExtRateCtrlPortRateLimitSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssExtRateCtrlIndex ,i4SetValIssExtRateCtrlPortRateLimit)
#define nmhSetIssExtRateCtrlPortBurstSize(i4IssExtRateCtrlIndex ,i4SetValIssExtRateCtrlPortBurstSize)   \
        nmhSetCmn(IssExtRateCtrlPortBurstSize, 13, IssExtRateCtrlPortBurstSizeSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssExtRateCtrlIndex ,i4SetValIssExtRateCtrlPortBurstSize)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IssExtL2FilterNo[13];
extern UINT4 IssExtL2FilterDstMacAddr[13];
extern UINT4 IssExtL2FilterSrcMacAddr[13];
extern UINT4 IssExtL2FilterVlanId[13];
extern UINT4 IssExtL2FilterAction[13];
extern UINT4 IssExtL2FilterStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIssExtL2FilterDstMacAddr(i4IssExtL2FilterNo ,SetValIssExtL2FilterDstMacAddr)      \
        nmhSetCmn(IssExtL2FilterDstMacAddr, 13, IssExtL2FilterDstMacAddrSet, IssLock, IssUnLock, 0, 0, 1, "%i %m", i4IssExtL2FilterNo ,SetValIssExtL2FilterDstMacAddr)
#define nmhSetIssExtL2FilterSrcMacAddr(i4IssExtL2FilterNo ,SetValIssExtL2FilterSrcMacAddr)      \
        nmhSetCmn(IssExtL2FilterSrcMacAddr, 13, IssExtL2FilterSrcMacAddrSet, IssLock, IssUnLock, 0, 0, 1, "%i %m", i4IssExtL2FilterNo ,SetValIssExtL2FilterSrcMacAddr)
#define nmhSetIssExtL2FilterVlanId(i4IssExtL2FilterNo ,i4SetValIssExtL2FilterVlanId)    \
        nmhSetCmn(IssExtL2FilterVlanId, 13, IssExtL2FilterVlanIdSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssExtL2FilterNo ,i4SetValIssExtL2FilterVlanId)
#define nmhSetIssExtL2FilterAction(i4IssExtL2FilterNo ,i4SetValIssExtL2FilterAction)    \
        nmhSetCmn(IssExtL2FilterAction, 13, IssExtL2FilterActionSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssExtL2FilterNo ,i4SetValIssExtL2FilterAction)
#define nmhSetIssExtL2FilterStatus(i4IssExtL2FilterNo ,i4SetValIssExtL2FilterStatus)    \
        nmhSetCmn(IssExtL2FilterStatus, 13, IssExtL2FilterStatusSet, IssLock, IssUnLock, 0, 1, 1, "%i %i", i4IssExtL2FilterNo ,i4SetValIssExtL2FilterStatus)

#endif
