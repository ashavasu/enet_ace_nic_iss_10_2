/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fslldmcli.h,v 1.2 2016/01/28 11:08:15 siva Exp $
*
* Description: LLDP-MED Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsLldpMedPortConfigTLVsTxEnable[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsLldpMedPortConfigTLVsTxEnable(i4LldpV2PortConfigIfIndex , u4LldpV2PortConfigDestAddressIndex ,pSetValFsLldpMedPortConfigTLVsTxEnable) \
 nmhSetCmn(FsLldpMedPortConfigTLVsTxEnable, 13, FsLldpMedPortConfigTLVsTxEnableSet, LldpLock, LldpUnLock, 0, 0, 2, "%i %u %s", i4LldpV2PortConfigIfIndex , u4LldpV2PortConfigDestAddressIndex ,pSetValFsLldpMedPortConfigTLVsTxEnable)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsLldpMedLocMediaPolicyAppType[13];
extern UINT4 FsLldpMedLocMediaPolicyVlanID[13];
extern UINT4 FsLldpMedLocMediaPolicyPriority[13];
extern UINT4 FsLldpMedLocMediaPolicyDscp[13];
extern UINT4 FsLldpMedLocMediaPolicyUnknown[13];
extern UINT4 FsLldpMedLocMediaPolicyTagged[13];
extern UINT4 FsLldpMedLocMediaPolicyRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsLldpMedLocMediaPolicyVlanID(i4LldpV2LocPortIfIndex , pFsLldpMedLocMediaPolicyAppType ,i4SetValFsLldpMedLocMediaPolicyVlanID) \
 nmhSetCmn(FsLldpMedLocMediaPolicyVlanID, 13, FsLldpMedLocMediaPolicyVlanIDSet, LldpLock, LldpUnLock, 0, 0, 2, "%i %s %i", i4LldpV2LocPortIfIndex , pFsLldpMedLocMediaPolicyAppType ,i4SetValFsLldpMedLocMediaPolicyVlanID)
#define nmhSetFsLldpMedLocMediaPolicyPriority(i4LldpV2LocPortIfIndex , pFsLldpMedLocMediaPolicyAppType ,i4SetValFsLldpMedLocMediaPolicyPriority) \
 nmhSetCmn(FsLldpMedLocMediaPolicyPriority, 13, FsLldpMedLocMediaPolicyPrioritySet, LldpLock, LldpUnLock, 0, 0, 2, "%i %s %i", i4LldpV2LocPortIfIndex , pFsLldpMedLocMediaPolicyAppType ,i4SetValFsLldpMedLocMediaPolicyPriority)
#define nmhSetFsLldpMedLocMediaPolicyDscp(i4LldpV2LocPortIfIndex , pFsLldpMedLocMediaPolicyAppType ,i4SetValFsLldpMedLocMediaPolicyDscp) \
 nmhSetCmn(FsLldpMedLocMediaPolicyDscp, 13, FsLldpMedLocMediaPolicyDscpSet, LldpLock, LldpUnLock, 0, 0, 2, "%i %s %i", i4LldpV2LocPortIfIndex , pFsLldpMedLocMediaPolicyAppType ,i4SetValFsLldpMedLocMediaPolicyDscp)
#define nmhSetFsLldpMedLocMediaPolicyUnknown(i4LldpV2LocPortIfIndex , pFsLldpMedLocMediaPolicyAppType ,i4SetValFsLldpMedLocMediaPolicyUnknown) \
 nmhSetCmn(FsLldpMedLocMediaPolicyUnknown, 13, FsLldpMedLocMediaPolicyUnknownSet, LldpLock, LldpUnLock, 0, 0, 2, "%i %s %i", i4LldpV2LocPortIfIndex , pFsLldpMedLocMediaPolicyAppType ,i4SetValFsLldpMedLocMediaPolicyUnknown)
#define nmhSetFsLldpMedLocMediaPolicyTagged(i4LldpV2LocPortIfIndex , pFsLldpMedLocMediaPolicyAppType ,i4SetValFsLldpMedLocMediaPolicyTagged) \
 nmhSetCmn(FsLldpMedLocMediaPolicyTagged, 13, FsLldpMedLocMediaPolicyTaggedSet, LldpLock, LldpUnLock, 0, 0, 2, "%i %s %i", i4LldpV2LocPortIfIndex , pFsLldpMedLocMediaPolicyAppType ,i4SetValFsLldpMedLocMediaPolicyTagged)
#define nmhSetFsLldpMedLocMediaPolicyRowStatus(i4LldpV2LocPortIfIndex , pFsLldpMedLocMediaPolicyAppType ,i4SetValFsLldpMedLocMediaPolicyRowStatus) \
 nmhSetCmn(FsLldpMedLocMediaPolicyRowStatus, 13, FsLldpMedLocMediaPolicyRowStatusSet, LldpLock, LldpUnLock, 0, 1, 2, "%i %s %i", i4LldpV2LocPortIfIndex , pFsLldpMedLocMediaPolicyAppType ,i4SetValFsLldpMedLocMediaPolicyRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsLldpMedLocLocationRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsLldpMedLocLocationRowStatus(i4LldpLocPortNum , i4LldpXMedLocLocationSubtype ,i4SetValFsLldpMedLocLocationRowStatus) \
 nmhSetCmn(FsLldpMedLocLocationRowStatus, 13, FsLldpMedLocLocationRowStatusSet, LldpLock, LldpUnLock, 0, 1, 2, "%i %i %i", i4LldpLocPortNum , i4LldpXMedLocLocationSubtype ,i4SetValFsLldpMedLocLocationRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsLldpMedClearStats[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsLldpMedClearStats(i4SetValFsLldpMedClearStats) \
 nmhSetCmn(FsLldpMedClearStats, 11, FsLldpMedClearStatsSet, LldpLock, LldpUnLock, 0, 0, 0, "%i", i4SetValFsLldpMedClearStats)

#endif
