/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdsnucli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 UsmUserSpinLock[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetUsmUserSpinLock(i4SetValUsmUserSpinLock)	\
	nmhSetCmn(UsmUserSpinLock, 10, UsmUserSpinLockSet, NULL, NULL, 0, 0, 0, "%i", i4SetValUsmUserSpinLock)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 UsmUserEngineID[12];
extern UINT4 UsmUserName[12];
extern UINT4 UsmUserCloneFrom[12];
extern UINT4 UsmUserAuthProtocol[12];
extern UINT4 UsmUserAuthKeyChange[12];
extern UINT4 UsmUserOwnAuthKeyChange[12];
extern UINT4 UsmUserPrivProtocol[12];
extern UINT4 UsmUserPrivKeyChange[12];
extern UINT4 UsmUserOwnPrivKeyChange[12];
extern UINT4 UsmUserPublic[12];
extern UINT4 UsmUserStorageType[12];
extern UINT4 UsmUserStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetUsmUserCloneFrom(pUsmUserEngineID , pUsmUserName ,pSetValUsmUserCloneFrom)	\
	nmhSetCmn(UsmUserCloneFrom, 12, UsmUserCloneFromSet, NULL, NULL, 0, 0, 2, "%s %s %o", pUsmUserEngineID , pUsmUserName ,pSetValUsmUserCloneFrom)
#define nmhSetUsmUserAuthProtocol(pUsmUserEngineID , pUsmUserName ,pSetValUsmUserAuthProtocol)	\
	nmhSetCmn(UsmUserAuthProtocol, 12, UsmUserAuthProtocolSet, NULL, NULL, 0, 0, 2, "%s %s %o", pUsmUserEngineID , pUsmUserName ,pSetValUsmUserAuthProtocol)
#define nmhSetUsmUserAuthKeyChange(pUsmUserEngineID , pUsmUserName ,pSetValUsmUserAuthKeyChange)	\
	nmhSetCmn(UsmUserAuthKeyChange, 12, UsmUserAuthKeyChangeSet, NULL, NULL, 0, 0, 2, "%s %s %s", pUsmUserEngineID , pUsmUserName ,pSetValUsmUserAuthKeyChange)
#define nmhSetUsmUserOwnAuthKeyChange(pUsmUserEngineID , pUsmUserName ,pSetValUsmUserOwnAuthKeyChange)	\
	nmhSetCmn(UsmUserOwnAuthKeyChange, 12, UsmUserOwnAuthKeyChangeSet, NULL, NULL, 0, 0, 2, "%s %s %s", pUsmUserEngineID , pUsmUserName ,pSetValUsmUserOwnAuthKeyChange)
#define nmhSetUsmUserPrivProtocol(pUsmUserEngineID , pUsmUserName ,pSetValUsmUserPrivProtocol)	\
	nmhSetCmn(UsmUserPrivProtocol, 12, UsmUserPrivProtocolSet, NULL, NULL, 0, 0, 2, "%s %s %o", pUsmUserEngineID , pUsmUserName ,pSetValUsmUserPrivProtocol)
#define nmhSetUsmUserPrivKeyChange(pUsmUserEngineID , pUsmUserName ,pSetValUsmUserPrivKeyChange)	\
	nmhSetCmn(UsmUserPrivKeyChange, 12, UsmUserPrivKeyChangeSet, NULL, NULL, 0, 0, 2, "%s %s %s", pUsmUserEngineID , pUsmUserName ,pSetValUsmUserPrivKeyChange)
#define nmhSetUsmUserOwnPrivKeyChange(pUsmUserEngineID , pUsmUserName ,pSetValUsmUserOwnPrivKeyChange)	\
	nmhSetCmn(UsmUserOwnPrivKeyChange, 12, UsmUserOwnPrivKeyChangeSet, NULL, NULL, 0, 0, 2, "%s %s %s", pUsmUserEngineID , pUsmUserName ,pSetValUsmUserOwnPrivKeyChange)
#define nmhSetUsmUserPublic(pUsmUserEngineID , pUsmUserName ,pSetValUsmUserPublic)	\
	nmhSetCmn(UsmUserPublic, 12, UsmUserPublicSet, NULL, NULL, 0, 0, 2, "%s %s %s", pUsmUserEngineID , pUsmUserName ,pSetValUsmUserPublic)
#define nmhSetUsmUserStorageType(pUsmUserEngineID , pUsmUserName ,i4SetValUsmUserStorageType)	\
	nmhSetCmn(UsmUserStorageType, 12, UsmUserStorageTypeSet, NULL, NULL, 0, 0, 2, "%s %s %i", pUsmUserEngineID , pUsmUserName ,i4SetValUsmUserStorageType)
#define nmhSetUsmUserStatus(pUsmUserEngineID , pUsmUserName ,i4SetValUsmUserStatus)	\
	nmhSetCmn(UsmUserStatus, 12, UsmUserStatusSet, NULL, NULL, 0, 1, 2, "%s %s %i", pUsmUserEngineID , pUsmUserName ,i4SetValUsmUserStatus)

#endif
