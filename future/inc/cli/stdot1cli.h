/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdot1cli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 LldpXdot1ConfigPortVlanTxEnable[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetLldpXdot1ConfigPortVlanTxEnable(i4LldpPortConfigPortNum ,i4SetValLldpXdot1ConfigPortVlanTxEnable)	\
	nmhSetCmn(LldpXdot1ConfigPortVlanTxEnable, 14, LldpXdot1ConfigPortVlanTxEnableSet, LldpLock, LldpUnLock, 0, 0, 1, "%i %i", i4LldpPortConfigPortNum ,i4SetValLldpXdot1ConfigPortVlanTxEnable)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 LldpXdot1ConfigVlanNameTxEnable[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetLldpXdot1ConfigVlanNameTxEnable(i4LldpLocPortNum , i4LldpXdot1LocVlanId ,i4SetValLldpXdot1ConfigVlanNameTxEnable)	\
	nmhSetCmn(LldpXdot1ConfigVlanNameTxEnable, 14, LldpXdot1ConfigVlanNameTxEnableSet, LldpLock, LldpUnLock, 0, 0, 2, "%i %i %i", i4LldpLocPortNum , i4LldpXdot1LocVlanId ,i4SetValLldpXdot1ConfigVlanNameTxEnable)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 LldpXdot1ConfigProtoVlanTxEnable[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetLldpXdot1ConfigProtoVlanTxEnable(i4LldpLocPortNum , i4LldpXdot1LocProtoVlanId ,i4SetValLldpXdot1ConfigProtoVlanTxEnable)	\
	nmhSetCmn(LldpXdot1ConfigProtoVlanTxEnable, 14, LldpXdot1ConfigProtoVlanTxEnableSet, LldpLock, LldpUnLock, 0, 0, 2, "%i %i %i", i4LldpLocPortNum , i4LldpXdot1LocProtoVlanId ,i4SetValLldpXdot1ConfigProtoVlanTxEnable)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 LldpXdot1ConfigProtocolTxEnable[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetLldpXdot1ConfigProtocolTxEnable(i4LldpLocPortNum , i4LldpXdot1LocProtocolIndex ,i4SetValLldpXdot1ConfigProtocolTxEnable)	\
	nmhSetCmn(LldpXdot1ConfigProtocolTxEnable, 14, LldpXdot1ConfigProtocolTxEnableSet, LldpLock, LldpUnLock, 0, 0, 2, "%i %i %i", i4LldpLocPortNum , i4LldpXdot1LocProtocolIndex ,i4SetValLldpXdot1ConfigProtocolTxEnable)

#endif
