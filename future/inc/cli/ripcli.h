/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ripcli.h,v 1.43 2017/12/28 10:40:14 siva Exp $
 *
 *******************************************************************/

#ifndef __RIPCLI_H__
#define __RIPCLI_H__


/*SIZE constants*/
#define RIP_CLI_CLNTID_SIZE           6
#define RIP_CLI_BOOTFILE_SIZE         64
#define RIP_CLI_OPTVAL_SIZE           10
#define MAX_ADDR_BUFFER_SIZE          256
#define RIP_CLI_MAX_MALLOC_SIZE       10000
#define RIP_MAX_CONTEXT_STR_LEN       VCM_ALIAS_MAX_LEN

#define RIP_CLI_ENABLE  1
#define RIP_CLI_DISABLE  2

#define CLI_RIP_SECURITY_MIN          1
#define CLI_RIP_SECURITY_MAX          2

#define CLI_RIP_UP                    1/*Defination For Rip Enable */
#define MD5AUTHTYPE                   3/*This defination For Md5 Check */

#define CLI_RIP_AUTH_NONE         1
#define CLI_RIP_AUTH_SIMPLE       2
#define CLI_RIP_AUTH_MD5          MD5AUTHTYPE

#define CLI_RIP_SEND_NONE                  1
#define CLI_RIP_SEND_RIP1                  2
#define CLI_RIP_SEND_RIP1_COMPATIBLE       3
#define CLI_RIP_SEND_RIP2                  4

#define CLI_RIP_SEND_DEMAND_RIP1           5
#define CLI_RIP_SEND_DEMAND_RIP2           6

#define CLI_RIP_RECEIVE_RIP1               1
#define CLI_RIP_RECEIVE_RIP2               2
#define CLI_RIP_RECEIVE_RIP1_RIP2          3
#define CLI_RIP_RECEIVE_NONE               4

#define CLI_RIP_RESTRICT_DEF_ROUTE         0

#define CLI_RIP_IN_FILTER                  0
#define CLI_RIP_OUT_FILTER                 1

#define RIP_ALL_TRC 0x000000ff


/*#define RIP_MALLOC                    MEM_MALLOC*/ /*Macros */


#define CLI_RIP_ROUTER_MODE "RipRouter"
#define RIP_CLI_MAX_ARGS     4
#define RIP_MAX_NAME_LENGTH  16
#define RIP_MAX_INTERFACE_NAME 10
#define CLI_RIP_IPIF_NUMBER_OF_UNNUMBERED_INTERFACES 0x00FFFFFF

/* Enum declaration for RIP command identifiers */
enum
{
    CLI_RIP_DEBUG = 1,
    CLI_RIP_SHOW_RIP,
    CLI_RIP_SECURITY,
    CLI_RIP_NO_SECURITY,
    CLI_RIP_RETRANSMIT,
    CLI_RIP_NO_RETRANSMIT,
    CLI_RIP_NETWORK,
    CLI_RIP_NO_NETWORK,
    CLI_RIP_ROUTER,
    CLI_RIP_NO_ROUTER,
    CLI_RIP_AUTHENTICATION_LAST_KEY,
    CLI_RIP_NEIGHBOR,
    CLI_RIP_NO_NEIGHBOR,
    CLI_RIP_PASSIVE_INTF,   
    CLI_RIP_NO_PASSIVE_INTF,  
    CLI_RIP_SUMMARY_ADDR,
    CLI_RIP_OUTPUT_DELAY,
    CLI_RIP_NO_OUTPUT_DELAY,
    CLI_RIP_ROUTE_REDISTRIBUTE,
    CLI_RIP_NO_ROUTE_REDISTRIBUTE,
    CLI_RIP_DISTRIBUTE_LIST,
    CLI_RIP_SEND_VERSION,
    CLI_RIP_RECEIVE_VERSION,
    CLI_RIP_SEND_RECV_VERSION,
    CLI_RIP_AUTHENTICATION,
    CLI_RIP_AUTHENTICATION_KEY,
    CLI_RIP_NO_AUTHENTICATION,
    CLI_RIP_CRYPTO_AUTHENTICATION,
    CLI_RIP_CRYPTO_AUTH_KEY,
    CLI_RIP_DEL_CRYPTO_AUTH_KEY,
    CLI_RIP_AUTH_KEY_START_ACCEPT,
    CLI_RIP_AUTH_KEY_STOP_ACCEPT,
    CLI_RIP_AUTH_KEY_START_GEN,
    CLI_RIP_AUTH_KEY_STOP_GEN,
    CLI_RIP_TIMERS_BASIC,
    CLI_RIP_NO_TIMERS_BASIC,
    CLI_RIP_SPLIT_HORIZON,
    CLI_RIP_NO_SPLIT_HORIZON, 
    CLI_RIP_DEF_METRIC,
    CLI_RIP_NO_DEF_METRIC,
    CLI_RIP_DEF_ROUTE_ORIGINATE,
    CLI_RIP_NO_DEF_ROUTE_ORIGINATE,
    CLI_RIP_SUMMARY_ADDRESS,
    CLI_RIP_NO_SUMMARY_ADDRESS,
    CLI_RIP_ENABLE_AUTO_SUMMARY,
    CLI_RIP_DISABLE_AUTO_SUMMARY,
    CLI_RIP_INSTALL_DEF_RT,
    CLI_RIP_NO_INSTALL_DEF_RT,
    CLI_RIP_ROUTE_DISTANCE,
    CLI_RIP_NO_ROUTE_DISTANCE,
    CLI_RIP_SHOW_PEER_INFO,
    CLI_RIP_MAX_COMMANDS            
};        

/* Command identifier for CLI_RIP_RETRANSMIT command */
#define RIP_RETRANSMIT_RETRIES  1
#define RIP_RETRANSMIT_INTERVAL 2

/* Command identifiers for CLI_RIP_SHOW_RIP command */
#define CLI_RIP_SHOW_DATABASE       1
#define CLI_RIP_SHOW_STATISTICS     2
#define CLI_RIP_SHOW_IPADDR         3
#define CLI_RIP_SHOW_AUTHENTICATION 4



/* Error codes */
enum
{
    CLI_RIP_INTERFACE_DISABLED_ERR = 1,
    CLI_RIP_INVALID_NETWORK_ERR,
    CLI_RIP_ROUTE_NOT_STATIC_ERR,
    CLI_RIP_NEIGHBOR_NOT_EXISTS_ERR,
    CLI_RIP_WRONG_ADMIN_STAT,
    CLI_RIP_ADMIN_STAT_NOT_SET,
    CLI_RIP_WRONG_UPDATE_HOLD_GARBAGE_TIMER,
    CLI_RIP_INT_CONF_STAT_NOT_SET,
    CLI_RIP_UPDATE_HOLD_GARBAGE_TIMER_NOT_SET,
    CLI_RIP_WRONG_SPLIT_HORIZON_STAT,
    CLI_RIP_SPLIT_HORIZON_STAT_NOT_SET,
    CLI_RIP_WRONG_SEND_TYPE,
    CLI_RIP_SEND_TYPE_NOT_SET,
    CLI_RIP_WRONG_RECEIVE_TYPE,
    CLI_RIP_RECEIVE_TYPE_NOT_SET,
    CLI_RIP_AUTH_TYPE_NOT_SET,
    CLI_RIP_WRONG_AUTH_KEY,
    CLI_RIP_AUTH_KEY_NOT_SET,
    CLI_RIP_WRONG_DEF_MET,
    CLI_RIP_DEF_MET_NOT_SET,
    CLI_RIP_WRONG_DEF_ROUTE_INSTALL,
    CLI_RIP_DEF_ROUTE_INSTALL_NOT_SET,
    CLI_RIP_AUTHENTICATION_NOT_ENABLED,
    CLI_RIP_ROUTE_NOT_PRESENT,
    CLI_RIP_NEIGHBOR_ERR,
    CLI_RIP_OUROWN_ADDR_NBR_ERR,
    CLI_RIP_EXISTING_IF_AGGRN,
    CLI_RIP_IF_AGG_LIMIT_EXEEDED,
    CLI_RIP_WRONG_VALUE,
    CLI_RIP_INVALID_IP_ADDR,
    CLI_RIP_NOT_UNNUM_IF,
    CLI_RIP_INVALID_UNNUM_VERSION,
    CLI_RIP_NO_VALID_SOURCE,
    CLI_RIP_INV_ASSOC_ROUTE_MAP,
    CLI_RIP_INV_DISASSOC_ROUTE_MAP,
    CLI_RIP_RMAP_ASSOC_FAILED,
    CLI_RIP_SECONDARY_IP_NOT_ALLOWED,
    CLI_RIP_AUTH_WRONG_VALUE,
    CLI_RIP_AUTH_INVALID_KEY,
    CLI_RIP_AUTH_INVALID_TIME,
    CLI_RIP_DEMAND_TYPE_ERR,
    CLI_RIP_DEMAND_VERSION_ERR,
    CLI_RIP_MAX_ERR
};







/* Error strings */

#ifdef __RIPCLI_C__ 

CONST CHR1 *RipCliErrString[] = {
    NULL,
    "\r% RIP is not enabled on the interface\r\n",
    "\r% Network with specified IP address is not configured\r\n",
    "\r% Only static routes can be redistributed when RRD is disabled\r\n",
    "\r% Neighbor with the specified IP address is not configured\r\n",
    "\r% Wrong Admin Status \r\n",
    "\r% Unable to set Admin Status Value\r\n",
    "\r% Wrong Update,Hold and Garbage timer Value\r\n",
    "\r% Unable to set Interface Config Status Value\r\n",
    "\r% Unable to set Update,Hold and Garbage timer Value\r\n",
    "\r% Wrong Split Horizon Status \r\n",
    "\r% Unable tp set Split Horizon Status Value \r\n",
    "\r% Wrong Send Type Value \r\n",
    "\r% Unable to set Send Type Value\r\n",
    "\r% Wrong Receive Type Value \r\n",
    "\r% Unable to set Receive Type Value\r\n",
    "\r% Unable to set Auth Type Value\r\n",
    "\r% Wrong Auth Key Value \r\n",
    "\r% Unable to set Auth Key Value\r\n",
    "\r% Wrong Default Metric Value \r\n",
    "\r% Unable to set Default Metric Value\r\n",
    "\r% Wrong Default Route Install Value \r\n",
    "\r% Unable to set Default Route Install Value\r\n",
    "\r% Authentication is not enabled on this interface\r\n",
    "\r% Route not in database\r\n",
    "\r% More than 16 neighbors cannot be configured\r\n",
    "\r% Neighbor address cannot be our own address\r\n",
    "\r% Specified aggregation exist over the interface\r\n",
    "\r% Max number of aggregations have already configured\r\n",
    "\r% Unable to set default route install status\r\n",
    "\r% Invalid IP Address\r\n",
    "\r% Not an Unnumbered interface\r\n",
    "\r% Unnumbered interfaces not supported on this version \r\n",
    "\r% No valid source IP Address exists \r\n",
    "\r% Route Map Not Associated \r\n",
    "\r% Route Map Not Associated \r\n",
    "\r% Route-map association failed (probably another route map is used)\r\n",
    "\r% Network with secondary IP address is not allowed \r\n",
    "\r% Authentication wrong value \r\n",
    "\r% Authentication invalid Key value \r\n",
    "\r% Authentication invalid time for key \r\n",
    "\r% RIP demand version can be set only if interface type is WAN\r\n",
    "\r% RIP demand version is incorrect\r\n",
    "\r%  \r\n",
    "\r\n"
};
#endif

#define RIP_CLI_INV_VALUE        -1

/* Crypto Authentication */
#define     CLI_RIP_LAST_KEY_STATUS_TRUE    1
#define     CLI_RIP_LAST_KEY_STATUS_FALSE   0
#define     CLI_RIP_AUTH_SHA1               4
#define     CLI_RIP_AUTH_SHA256             5
#define     CLI_RIP_AUTH_SHA384             6
#define     CLI_RIP_AUTH_SHA512             7

/* Default timer values for no timers basic command */
#define CLI_RIP_DEF_UPDATE_TMR    30
#define CLI_RIP_DEF_HOLD_TMR      180
#define CLI_RIP_DEF_GARBAGE_TMR   120
#define CLI_RIP_DEF_METRIC_VALUE  3

/* Default values for ip rip retransmission command */
#define RIP_DEF_RETRANSMISSION_INT      5
#define RIP_DEF_RETRANSMISSION_RETRIES  36


#define  RIP_CLI_RRD_ENABLE  1
#define  RIP_CLI_RRD_DISABLE  2
#define  RIP_RRD_DIRECT_MASK                 0x00000002     /* Bit 1 */
#define  RIP_RRD_STATIC_MASK                 0x00000004     /* Bit 2 */
#define  RIP_RRD_OSPF_MASK                   0x00001000     /* Bit 12 */
#define  RIP_RRD_BGP_MASK                    0x00002000     /* Bit 13 */
#define  RIP_RRD_ISISL1_MASK                 0x00004000     /* Bit 14 */
#define  RIP_RRD_ISISL2_MASK                 0x00008000     /* Bit 15 */
#define  RIP_RRD_ISISL1L2_MASK                   0x0000c000     /* Bit 13 */

#define  RIP_RRD_ALL_MASK                    0x0000f006     /* Bit 1|bit 2|bit 12|bit 13  */
#define  RIP_CLI_NULL          0
/* Maximum number of Global statistics objects of RIP */

#define RIP_CLI_MAX_GLOBAL_STATS_OBJECTS       8 

#define RIP_GLOB_STATS_SIZE (CLI_MAX_HEADER_SIZE + \
                             CLI_MAX_COLUMN_WIDTH * \
                             RIP_CLI_MAX_GLOBAL_STATS_OBJECTS)

/* Maximum number of Global configuration objects of RIP */
#define RIP_CLI_MAX_GLOBAL_CONFIG_OBJECTS       8

#define RIP_GLOB_CONFIG_SIZE (CLI_MAX_HEADER_SIZE + \
                              CLI_MAX_COLUMN_WIDTH * \
                              RIP_CLI_MAX_GLOBAL_CONFIG_OBJECTS)



/* Maximum number of interface statistics objects of RIP */
#define RIP_CLI_MAX_INTF_STATS_OBJECTS          50

#define RIP_INT_STATS_SIZE (CLI_MAX_HEADER_SIZE + \
                              CLI_MAX_COLUMN_WIDTH * \
                              RIP_CLI_MAX_INTF_STATS_OBJECTS)


/* Maximum number of interface Configuration objects of RIP */
#define RIP_MAX_INTERFACE                      50    /*Maximum interface assumed
                                                      *is 50*/   
#define RIP_CLI_MAX_INTF_CONFIG_OBJECTS        25    
#define RIP_MAX_SIZE_CONF_INTERFACE            RIP_MAX_INTERFACE * \
                                               RIP_CLI_MAX_INTF_CONFIG_OBJECTS

#define RIP_INT_CONFIG_SIZE (CLI_MAX_HEADER_SIZE + \
                              CLI_MAX_COLUMN_WIDTH * \
                              RIP_MAX_SIZE_CONF_INTERFACE)


/*Structure For RipInerfaceCondTable*/
typedef struct _RipCliIfConf
{  
   UINT4       u4RipIfAddr;
   UINT4       u4RipNbrIfAddr;
   INT4        i4RipSplitHorizonFlag;
   UINT2       u2IfIndex;
   UINT2       u2Pad;
   INT4        i4RipIfAdminStat;
   UINT4       u4RipIfGarbgTmr;
   UINT4       u4RipIfRouteAgeTmr;
   UINT4       u4RipIfUpdateTmr;
   INT4        i4RipIfAuthType;
/* tSNMP_OCTET_STRING_TYPE *pRipIfAuthKey; */
   INT4        i4RipIfSendType;
   INT4        i4RipIfRecvType;
   INT4        i4RipIfDefMatric;
   INT4        i4RipIfStatus;
   UINT1       au1AuthKey[MAX_ADDR_BUFFER_SIZE];
}tRipCliIfConf;
#ifdef RRD_WANTED
typedef struct _RipRrdConf
{
   UINT1  au1RipRrdSrcProto[12];
   UINT4  u4RipRrdStatus;
}tRipRrdConf;
#endif
 /*Union Decleration*/
typedef union
{
   INT4              i4RipSecurityFlag;
   INT4              i4RipSpacingFlag;
   INT4              i4RipRetransTmr;
   UINT2             u2IfId; 
   UINT4             u4RipConfIfAddr;
   UINT4             u4RipTraceFlag;
   UINT4             u4RipStaticFlag; 
   tRipCliIfConf     sIfConf;
#ifdef RRD_WANTED   
   tRipRrdConf       sRipRrdConf;
#endif   
}tRipCliConfigParams;




/* Function prototypes for RIP CLI command action routines */
INT4 cli_process_rip_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command, ...));
INT4 cli_process_rip_test_cmd PROTO ((tCliHandle CliHandle, ...));
INT1 RipGetRouterCfgPrompt PROTO ((INT1 *, INT1 *));
extern INT1 nmhGetFirstIndexFsIpifTable PROTO ((INT4 *));
extern INT1 nmhGetNextIndexFsIpifTable PROTO ((INT4, INT4 *));  

extern INT1 nmhGetIfIpSubnetMask PROTO ((INT4, UINT4 *));
INT4 RipShowStatisticsInCxt PROTO ((tCliHandle, INT4));
INT4 RipShowAuthenticationInCxt PROTO ((tCliHandle, INT4));
VOID RipShowSimpleAuthenticationInCxt PROTO ((tCliHandle, INT4));
INT4 RipShowDatabaseCxt PROTO ((tCliHandle, INT4));
INT4 RipShowPeerInformation PROTO ((tCliHandle, INT4));
INT4 ShowRipIfAggregationCxt PROTO ((tCliHandle ,INT4));
INT4 RipShowIpAddrInCxt PROTO ((tCliHandle, UINT4, UINT4, INT4));
INT4 RipShowRunningConfigCxt (tCliHandle CliHandle, UINT4 u4Module, 
                              UINT4 u4ContextId);
UINT1 RipShowRunningConfigScalarCxt (tCliHandle CliHandle, INT4, UINT4 u4Module);
INT4 RipShowRunningConfigTableCxt (tCliHandle CliHandle, INT4);
INT4 RipShowRunningConfigInterfaceCxt (tCliHandle CliHandle, INT4);
INT4 RipShowRunningConfigInterfaceDetailsCxt (tCliHandle CliHandle, INT4 i4Index, UINT4 u4RipIfAddr,UINT1 *);
INT4 RipShowRunningConfigIfaceAndAggDetailsCxt (tCliHandle CliHandle, UINT4 u4CfaIndex);
INT4 RipShowRunningConfigInterfaceAggDetailsCxt (tCliHandle CliHandle, UINT4 u4Port,  UINT4 u4CfaIndex,UINT1 *,UINT1 *);
INT4 CliRipEnableInterface PROTO ((UINT4));
INT4 RipSetDebugLevel PROTO ((tCliHandle, UINT4, INT4));   
INT4 RipSetTraceValue (INT4 ,UINT1);
INT4 RipEnableRouter PROTO ((tCliHandle, INT4));
INT4 RipDisableRouter PROTO ((tCliHandle, INT4));
INT4 RipSetSecurity PROTO ((tCliHandle, INT4));
INT4 RipSetNetwork PROTO ((tCliHandle, UINT4, UINT4));
INT4 RipSetNoNetwork PROTO ((tCliHandle, UINT4, UINT4));
INT4 RipSetNeighbor PROTO ((tCliHandle, UINT4));
INT4 RipSetNoNeighbor PROTO ((tCliHandle, UINT4));
INT4 RipSetPassiveIntf PROTO ((tCliHandle, INT4, INT4));
INT4 RipSetRouteRedistribute PROTO ((tCliHandle, INT4, UINT1 *));
INT4 RipSetNoRouteRedistribute PROTO ((tCliHandle, INT4, UINT1 *));
INT4 RipCliSetDistribute PROTO ((tCliHandle, UINT1*, UINT1, UINT1));
INT4 RipSetRetransmissionInt PROTO ((tCliHandle, INT4));
INT4 RipSetRetransmissionRetries PROTO ((tCliHandle, INT4));
INT4 RipSetSpacing PROTO ((tCliHandle, INT4));
INT4 RipSetSendVersion PROTO ((tCliHandle, INT4, INT4));  
INT4 RipSetReceiveVersion PROTO ((tCliHandle, INT4, INT4)); 
INT4 RipSetSendAndRecvVersion PROTO ((tCliHandle, INT4, INT4));
INT4 RipSetAuthentication PROTO ((tCliHandle, INT4, INT4, UINT1 *)); 
INT4 RipSetAuthenticationKey PROTO ((tCliHandle, INT4, UINT1 *));
INT4 RipSetNoAuthentication PROTO ((tCliHandle, INT4)); 
INT4 RipSetSplitHorizon PROTO ((tCliHandle, INT4, INT4));
INT4 RipSetDefMetric PROTO ((tCliHandle, INT4));
INT4 RipSetTimers PROTO ((tCliHandle, INT4, UINT4, UINT4, UINT4));
INT4 RipDefaultRouteOriginate PROTO ((tCliHandle, INT4, INT4));
INT4 RipSummaryAddress PROTO ((tCliHandle, INT4, UINT4, UINT4));
INT4 RipNoSummaryAddress PROTO ((tCliHandle, INT4, UINT4, UINT4));
INT4 RipSetAutoSummary PROTO ((tCliHandle, INT4));
INT4 RipSetDefRtInstallStatus PROTO ((tCliHandle CliHandle, INT4 , INT4));
INT4 RipCliGetContext PROTO ((tCliHandle CliHandle, UINT4, INT4 *,
                INT4 *,UINT2 *));
INT4 RipCliGetCxtIdFrmVrfName (UINT1 *, INT4 *);
UINT1 RipShowConfigNBRListInCxt  (tCliHandle CliHandle, INT4);
UINT1 RipShowConfigIfTableCxt  (tCliHandle CliHandle, INT4);
VOID IssRipShowDebugging (tCliHandle);
INT4 RipCliGetIfAddrFromIfIndex(tCliHandle CliHandle, INT4 ,UINT4 *);
INT4 RipSetRouteDistance PROTO ((tCliHandle, INT4, UINT1 *));
INT4 RipSetNoRouteDistance PROTO ((tCliHandle, UINT1 *));
INT4 RipCtrlLastAuthenticationKey PROTO ((tCliHandle CliHandle, INT4 i4CxtId,
                                            INT4 i4LifeTimeStatus));
INT4 RipSetCryptoAuthentication PROTO ((tCliHandle CliHandle, INT4 i4CxtId,
                                        INT4 i4IfIndex, INT4 i4RipIfAuthType));
INT4 RipSetCryptoAuthenticationKey PROTO ((tCliHandle CliHandle, INT4 i4CxtId,
                                            INT4 i4IfIndex, INT4 i4CryptoAuthKeyId,
                                            UINT1 *pu1RipIfAuthKey));
INT4 RipSetDeleteCryptoAuthKey PROTO ((tCliHandle CliHandle, INT4 i4CxtId,
                                        INT4 i4IfIndex, INT4 i4CryptoAuthKeyId));
INT4 RipSetCryptoAuthKeyStartAccept PROTO ((tCliHandle CliHandle, INT4 i4CxtId,
                                            INT4 i4IfIndex, INT4 i4CryptoAuthKeyId,
                                            UINT1 *pu1RipIfStartAccept));
INT4 RipSetCryptoAuthKeyStopAccept PROTO ((tCliHandle CliHandle, INT4 i4CxtId,
                                            INT4 i4IfIndex, INT4 i4CryptoAuthKeyId,
                                            UINT1 *pu1RipIfStopAccept));
INT4 RipSetCryptoAuthKeyStartGenerate PROTO ((tCliHandle CliHandle, INT4 i4CxtId,
                                                INT4 i4IfIndex, INT4 i4CryptoAuthKeyId,
                                                UINT1 *pu1RipIfStartGenerate));
INT4 RipSetCryptoAuthKeyStopGenerate PROTO ((tCliHandle CliHandle, INT4 i4CxtId,
                                                INT4 i4IfIndex, INT4 i4CryptoAuthKeyId,
                                                UINT1 *pu1RipIfStopGenerate));
INT4 RipShowRunningConfigCryptoAuthDetails PROTO ((tCliHandle CliHandle,INT4 i4CxtId,
                                                    INT4 i4Index,UINT4 u4SrcAddress, UINT1 *,UINT1 *));
#endif /* __RIPCLI_H__ */
                 

                  

                
                

                
                
                
                    
                    
                  

