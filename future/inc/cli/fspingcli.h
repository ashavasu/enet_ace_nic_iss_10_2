/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fspingcli.h,v 1.3 2014/11/03 12:29:41 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPingIndex[12];
extern UINT4 FsPingDest[12];
extern UINT4 FsPingTimeout[12];
extern UINT4 FsPingTries[12];
extern UINT4 FsPingDataSize[12];
extern UINT4 FsPingEntryStatus[12];
extern UINT4 FsPingHostName[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPingDest(i4FsPingIndex ,u4SetValFsPingDest) \
 nmhSetCmn(FsPingDest, 12, FsPingDestSet, PingLock, PingUnLock, 0, 0, 1, "%i %p", i4FsPingIndex ,u4SetValFsPingDest)
#define nmhSetFsPingTimeout(i4FsPingIndex ,i4SetValFsPingTimeout) \
 nmhSetCmn(FsPingTimeout, 12, FsPingTimeoutSet, PingLock, PingUnLock, 0, 0, 1, "%i %i", i4FsPingIndex ,i4SetValFsPingTimeout)
#define nmhSetFsPingTries(i4FsPingIndex ,i4SetValFsPingTries) \
 nmhSetCmn(FsPingTries, 12, FsPingTriesSet, PingLock, PingUnLock, 0, 0, 1, "%i %i", i4FsPingIndex ,i4SetValFsPingTries)
#define nmhSetFsPingDataSize(i4FsPingIndex ,i4SetValFsPingDataSize) \
 nmhSetCmn(FsPingDataSize, 12, FsPingDataSizeSet, PingLock, PingUnLock, 0, 0, 1, "%i %i", i4FsPingIndex ,i4SetValFsPingDataSize)
#define nmhSetFsPingEntryStatus(i4FsPingIndex ,i4SetValFsPingEntryStatus) \
 nmhSetCmn(FsPingEntryStatus, 12, FsPingEntryStatusSet, PingLock, PingUnLock, 0, 1, 1, "%i %i", i4FsPingIndex ,i4SetValFsPingEntryStatus)
#define nmhSetFsPingHostName(i4FsPingIndex ,pSetValFsPingHostName)      \
        nmhSetCmnWithLock(FsPingHostName, 12, FsPingHostNameSet, PingLock, PingUnLock, 0, 0, 1, "%i %s", i4FsPingIndex ,pSetValFsPingHostName)

#endif
