/********************************************************************
*Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpfwlcli.h,v 1.1 2014/07/01 11:47:28 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/



/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FwlGlobalMasterControlSwitch[10];
extern UINT4 FwlGlobalICMPControlSwitch[10];
extern UINT4 FwlGlobalIpSpoofFiltering[10];
extern UINT4 FwlGlobalSrcRouteFiltering[10];
extern UINT4 FwlGlobalTinyFragmentFiltering[10];
extern UINT4 FwlGlobalTcpIntercept[10];
extern UINT4 FwlGlobalTrap[10];
extern UINT4 FwlGlobalTrace[10];
extern UINT4 FwlGlobalDebug[10];
extern UINT4 FwlGlobalUrlFiltering[10];
extern UINT4 FwlGlobalNetBiosFiltering[10];
extern UINT4 FwlGlobalNetBiosLan2Wan[10];
extern UINT4 FwlGlobalICMPv6ControlSwitch[10];
extern UINT4 FwlGlobalIpv6SpoofFiltering[10];
extern UINT4 FwlGlobalLogFileSize[10];
extern UINT4 FwlGlobalLogSizeThreshold[10];
extern UINT4 FwlGlobalIdsLogSize[10];
extern UINT4 FwlGlobalIdsLogThreshold[10];
extern UINT4 FwlGlobalIdsStatus[10];
extern UINT4 FwlGlobalLoadIdsRules [10 ];



/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FwlDefnTcpInterceptThreshold[10];
extern UINT4 FwlDefnInterceptTimeout[10];




/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FwlFilterFilterName[12];
extern UINT4 FwlFilterSrcAddress[12];
extern UINT4 FwlFilterDestAddress[12];
extern UINT4 FwlFilterProtocol[12];
extern UINT4 FwlFilterSrcPort[12];
extern UINT4 FwlFilterDestPort[12];
extern UINT4 FwlFilterAckBit[12];
extern UINT4 FwlFilterRstBit[12];
extern UINT4 FwlFilterTos[12];
extern UINT4 FwlFilterAccounting[12];
extern UINT4 FwlFilterHitClear[12];
extern UINT4 FwlFilterAddrType[12];
extern UINT4 FwlFilterFlowId[12];
extern UINT4 FwlFilterDscp[12];
extern UINT4 FwlFilterRowStatus[12];



/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FwlRuleRuleName[12];
extern UINT4 FwlRuleFilterSet[12];
extern UINT4 FwlRuleRowStatus[12];



/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FwlAclIfIndex[12];
extern UINT4 FwlAclAction[12];
extern UINT4 FwlAclSequenceNumber[12];
extern UINT4 FwlAclLogTrigger[12];
extern UINT4 FwlAclFragAction[12];
extern UINT4 FwlAclRowStatus[12];




/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FwlIfIfIndex[12];
extern UINT4 FwlIfIfType[12];
extern UINT4 FwlIfIpOptions[12];
extern UINT4 FwlIfFragments[12];
extern UINT4 FwlIfFragmentSize[12];
extern UINT4 FwlIfICMPType[12];
extern UINT4 FwlIfICMPCode[12];
extern UINT4 FwlIfICMPv6MsgType[12];
extern UINT4 FwlIfRowStatus[12];


 /* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FwlDmzIpIndex[12];
extern UINT4 FwlDmzRowStatus[12];



/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FwlUrlString[12];
extern UINT4 FwlUrlFilterRowStatus[12];


 /* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FwlStatIfIfIndex[12];
extern UINT4 FwlStatIfClear[12];
extern UINT4 FwlIfTrapThreshold[12];
extern UINT4 FwlStatIfClearIPv6[12];


 /* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FwlStatClear[10];
extern UINT4 FwlStatClearIPv6[10];
extern UINT4 FwlTrapThreshold[10];



/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FwlTrapMemFailMessage[11];
extern UINT4 FwlTrapAttackMessage[11];
extern UINT4 FwlIfIndex[11];
extern UINT4 FwlTrapEvent[11];
extern UINT4 FwlTrapEventTime[11];
extern UINT4 FwlIdsTrapEvent[11];
extern UINT4 FwlIdsTrapEventTime[11];
extern UINT4 FwlIdsAttackPktIp[11];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FwlBlkListIpAddressType[12];
extern UINT4 FwlBlkListIpAddress[12];
extern UINT4 FwlBlkListIpMask[12];
extern UINT4 FwlBlkListRowStatus[12];


 /* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FwlWhiteListIpAddressType[12];
extern UINT4 FwlWhiteListIpAddress[12];
extern UINT4 FwlWhiteListIpMask[12];
extern UINT4 FwlWhiteListRowStatus[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FwlDmzAddressType[12];
extern UINT4 FwlDmzIpv6Index[12];
extern UINT4 FwlDmzIpv6RowStatus[12];
