/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsbgp4cli.h,v 1.20 2016/02/18 07:55:38 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4GlobalAdminStatus[10];
extern UINT4 Fsbgp4LocalAs[10];
extern UINT4 Fsbgp4Identifier[10];
extern UINT4 Fsbgp4Synchronization[10];
extern UINT4 Fsbgp4DefaultLocalPref[10];
extern UINT4 Fsbgp4AdvtNonBgpRt[10];
extern UINT4 Fsbgp4TraceEnable[10];
extern UINT4 Fsbgp4DebugEnable[10];
extern UINT4 Fsbgp4OverlappingRoute[10];
extern UINT4 Fsbgp4MaxPeerEntry[10];
extern UINT4 Fsbgp4MaxNoofRoutes[10];
extern UINT4 Fsbgp4AlwaysCompareMED[10];
extern UINT4 Fsbgp4DefaultOriginate[10];
extern UINT4 Fsbgp4DefaultIpv4UniCast[10];
extern UINT4 Fsbgp4GRAdminStatus[10];
extern UINT4 Fsbgp4GRRestartTimeInterval[10];
extern UINT4 Fsbgp4GRSelectionDeferralTimeInterval[10];
extern UINT4 Fsbgp4GRStaleTimeInterval[10];
extern UINT4 Fsbgp4RestartSupport[10];
extern UINT4 Fsbgp4RestartReason[10];
extern UINT4 Fsbgp4IsTrapEnabled[10];
extern UINT4 Fsbgp4NextHopProcessingInterval[10];
extern UINT4 Fsbgp4IBGPRedistributionStatus[10];
extern UINT4 Fsbgp4IBGPMaxPaths[10];
extern UINT4 Fsbgp4EBGPMaxPaths[10];
extern UINT4 Fsbgp4EIBGPMaxPaths[10];
extern UINT4 Fsbgp4FourByteASNSupportStatus[10];
extern UINT4 Fsbgp4FourByteASNotationType[10];
extern UINT4 Fsbgp4MacMobDuplicationTimeInterval[10];
extern UINT4 Fsbgp4MaxMacMoves[10];
extern UINT4 Fsbgp4VpnRouteLeakStatus[10]; 

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4GlobalAdminStatus(i4SetValFsbgp4GlobalAdminStatus) \
 nmhSetCmnWithLock(Fsbgp4GlobalAdminStatus, 10, Fsbgp4GlobalAdminStatusSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4GlobalAdminStatus)
#define nmhSetFsbgp4LocalAs(u4SetValFsbgp4LocalAs) \
 nmhSetCmnWithLock(Fsbgp4LocalAs, 10, Fsbgp4LocalAsSet, BgpLock, BgpUnLock, 0, 0, 0, "%u", u4SetValFsbgp4LocalAs)
#define nmhSetFsbgp4Identifier(u4SetValFsbgp4Identifier) \
 nmhSetCmnWithLock(Fsbgp4Identifier, 10, Fsbgp4IdentifierSet, BgpLock, BgpUnLock, 0, 0, 0, "%p", u4SetValFsbgp4Identifier)
#define nmhSetFsbgp4Synchronization(i4SetValFsbgp4Synchronization) \
 nmhSetCmnWithLock(Fsbgp4Synchronization, 10, Fsbgp4SynchronizationSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4Synchronization)
#define nmhSetFsbgp4DefaultLocalPref(u4SetValFsbgp4DefaultLocalPref) \
 nmhSetCmnWithLock(Fsbgp4DefaultLocalPref, 10, Fsbgp4DefaultLocalPrefSet, BgpLock, BgpUnLock, 0, 0, 0, "%u", u4SetValFsbgp4DefaultLocalPref)
#define nmhSetFsbgp4AdvtNonBgpRt(i4SetValFsbgp4AdvtNonBgpRt) \
 nmhSetCmnWithLock(Fsbgp4AdvtNonBgpRt, 10, Fsbgp4AdvtNonBgpRtSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4AdvtNonBgpRt)
#define nmhSetFsbgp4TraceEnable(u4SetValFsbgp4TraceEnable) \
 nmhSetCmnWithLock(Fsbgp4TraceEnable, 10, Fsbgp4TraceEnableSet, BgpLock, BgpUnLock, 0, 0, 0, "%u", u4SetValFsbgp4TraceEnable)
#define nmhSetFsbgp4DebugEnable(u4SetValFsbgp4DebugEnable) \
 nmhSetCmnWithLock(Fsbgp4DebugEnable, 10, Fsbgp4DebugEnableSet, BgpLock, BgpUnLock, 0, 0, 0, "%u", u4SetValFsbgp4DebugEnable)
#define nmhSetFsbgp4OverlappingRoute(i4SetValFsbgp4OverlappingRoute) \
 nmhSetCmnWithLock(Fsbgp4OverlappingRoute, 10, Fsbgp4OverlappingRouteSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4OverlappingRoute)
#define nmhSetFsbgp4MaxPeerEntry(i4SetValFsbgp4MaxPeerEntry) \
 nmhSetCmnWithLock(Fsbgp4MaxPeerEntry, 10, Fsbgp4MaxPeerEntrySet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4MaxPeerEntry)
#define nmhSetFsbgp4MaxNoofRoutes(i4SetValFsbgp4MaxNoofRoutes) \
 nmhSetCmnWithLock(Fsbgp4MaxNoofRoutes, 10, Fsbgp4MaxNoofRoutesSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4MaxNoofRoutes)
#define nmhSetFsbgp4AlwaysCompareMED(i4SetValFsbgp4AlwaysCompareMED) \
 nmhSetCmnWithLock(Fsbgp4AlwaysCompareMED, 10, Fsbgp4AlwaysCompareMEDSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4AlwaysCompareMED)
#define nmhSetFsbgp4DefaultOriginate(i4SetValFsbgp4DefaultOriginate) \
 nmhSetCmnWithLock(Fsbgp4DefaultOriginate, 10, Fsbgp4DefaultOriginateSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4DefaultOriginate)
#define nmhSetFsbgp4DefaultIpv4UniCast(i4SetValFsbgp4DefaultIpv4UniCast) \
 nmhSetCmnWithLock(Fsbgp4DefaultIpv4UniCast, 10, Fsbgp4DefaultIpv4UniCastSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4DefaultIpv4UniCast)
#define nmhSetFsbgp4GRAdminStatus(i4SetValFsbgp4GRAdminStatus) \
 nmhSetCmnWithLock(Fsbgp4GRAdminStatus, 10, Fsbgp4GRAdminStatusSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4GRAdminStatus)
#define nmhSetFsbgp4GRRestartTimeInterval(i4SetValFsbgp4GRRestartTimeInterval) \
 nmhSetCmnWithLock(Fsbgp4GRRestartTimeInterval, 10, Fsbgp4GRRestartTimeIntervalSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4GRRestartTimeInterval)
#define nmhSetFsbgp4GRSelectionDeferralTimeInterval(i4SetValFsbgp4GRSelectionDeferralTimeInterval) \
 nmhSetCmnWithLock(Fsbgp4GRSelectionDeferralTimeInterval, 10, Fsbgp4GRSelectionDeferralTimeIntervalSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4GRSelectionDeferralTimeInterval)
#define nmhSetFsbgp4GRStaleTimeInterval(i4SetValFsbgp4GRStaleTimeInterval) \
 nmhSetCmnWithLock(Fsbgp4GRStaleTimeInterval, 10, Fsbgp4GRStaleTimeIntervalSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4GRStaleTimeInterval)
#define nmhSetFsbgp4RestartSupport(i4SetValFsbgp4RestartSupport) \
 nmhSetCmnWithLock(Fsbgp4RestartSupport, 10, Fsbgp4RestartSupportSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4RestartSupport)
#define nmhSetFsbgp4RestartReason(i4SetValFsbgp4RestartReason) \
 nmhSetCmnWithLock(Fsbgp4RestartReason, 10, Fsbgp4RestartReasonSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4RestartReason)
#define nmhSetFsbgp4IsTrapEnabled(i4SetValFsbgp4IsTrapEnabled) \
 nmhSetCmnWithLock(Fsbgp4IsTrapEnabled, 10, Fsbgp4IsTrapEnabledSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4IsTrapEnabled)
#define nmhSetFsbgp4NextHopProcessingInterval(i4SetValFsbgp4NextHopProcessingInterval) \
 nmhSetCmnWithLock(Fsbgp4NextHopProcessingInterval, 10, Fsbgp4NextHopProcessingIntervalSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4NextHopProcessingInterval)
#define nmhSetFsbgp4IBGPRedistributionStatus(i4SetValFsbgp4IBGPRedistributionStatus) \
 nmhSetCmnWithLock(Fsbgp4IBGPRedistributionStatus, 10, Fsbgp4IBGPRedistributionStatusSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4IBGPRedistributionStatus)
#define nmhSetFsbgp4IBGPMaxPaths(i4SetValFsbgp4IBGPMaxPaths) \
 nmhSetCmnWithLock(Fsbgp4IBGPMaxPaths, 10, Fsbgp4IBGPMaxPathsSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4IBGPMaxPaths)
#define nmhSetFsbgp4EBGPMaxPaths(i4SetValFsbgp4EBGPMaxPaths) \
 nmhSetCmnWithLock(Fsbgp4EBGPMaxPaths, 10, Fsbgp4EBGPMaxPathsSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4EBGPMaxPaths)
#define nmhSetFsbgp4EIBGPMaxPaths(i4SetValFsbgp4EIBGPMaxPaths) \
 nmhSetCmnWithLock(Fsbgp4EIBGPMaxPaths, 10, Fsbgp4EIBGPMaxPathsSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4EIBGPMaxPaths)
#define nmhSetFsbgp4FourByteASNSupportStatus(i4SetValFsbgp4FourByteASNSupportStatus) \
 nmhSetCmnWithLock(Fsbgp4FourByteASNSupportStatus, 10, Fsbgp4FourByteASNSupportStatusSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4FourByteASNSupportStatus)
#define nmhSetFsbgp4FourByteASNotationType(i4SetValFsbgp4FourByteASNotationType) \
 nmhSetCmnWithLock(Fsbgp4FourByteASNotationType, 10, Fsbgp4FourByteASNotationTypeSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4FourByteASNotationType)
#define nmhSetFsbgp4MacMobDuplicationTimeInterval(i4SetValFsbgp4MacMobDuplicationTimeInterval) \
 nmhSetCmnWithLock(Fsbgp4MacMobDuplicationTimeInterval, 10 Fsbgp4MacMobDuplicationTimeIntervalSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4MacMobDuplicationTimeInterval)
#define nmhSetFsbgp4MaxMacMoves(i4SetValFsbgp4MaxMacMoves) \
 nmhSetCmnWithLock(Fsbgp4MaxMacMoves, 10 Fsbgp4MaxMacMovesSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4MaxMacMoves)
#define nmhSetFsbgp4VpnRouteLeakStatus(i4SetValFsbgp4VpnRouteLeakStatus)    \                                         nmhSetCmnWithLock(Fsbgp4VpnRouteLeakStatus, 10, Fsbgp4VpnRouteLeakStatusSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4VpnRouteLeakStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4PeerExtPeerRemoteAddr[11];
extern UINT4 Fsbgp4PeerExtConfigurePeer[11];
extern UINT4 Fsbgp4PeerExtPeerRemoteAs[11];
extern UINT4 Fsbgp4PeerExtEBGPMultiHop[11];
extern UINT4 Fsbgp4PeerExtNextHopSelf[11];
extern UINT4 Fsbgp4PeerExtRflClient[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4PeerExtConfigurePeer(u4Fsbgp4PeerExtPeerRemoteAddr ,i4SetValFsbgp4PeerExtConfigurePeer) \
 nmhSetCmnWithLock(Fsbgp4PeerExtConfigurePeer, 11, Fsbgp4PeerExtConfigurePeerSet, BgpLock, BgpUnLock, 1, 0, 1, "%p %i", u4Fsbgp4PeerExtPeerRemoteAddr ,i4SetValFsbgp4PeerExtConfigurePeer)
#define nmhSetFsbgp4PeerExtPeerRemoteAs(u4Fsbgp4PeerExtPeerRemoteAddr ,u4SetValFsbgp4PeerExtPeerRemoteAs) \
 nmhSetCmnWithLock(Fsbgp4PeerExtPeerRemoteAs, 11, Fsbgp4PeerExtPeerRemoteAsSet, BgpLock, BgpUnLock, 1, 0, 1, "%p %u", u4Fsbgp4PeerExtPeerRemoteAddr ,u4SetValFsbgp4PeerExtPeerRemoteAs)
#define nmhSetFsbgp4PeerExtEBGPMultiHop(u4Fsbgp4PeerExtPeerRemoteAddr ,i4SetValFsbgp4PeerExtEBGPMultiHop) \
 nmhSetCmnWithLock(Fsbgp4PeerExtEBGPMultiHop, 11, Fsbgp4PeerExtEBGPMultiHopSet, BgpLock, BgpUnLock, 1, 0, 1, "%p %i", u4Fsbgp4PeerExtPeerRemoteAddr ,i4SetValFsbgp4PeerExtEBGPMultiHop)
#define nmhSetFsbgp4PeerExtNextHopSelf(u4Fsbgp4PeerExtPeerRemoteAddr ,i4SetValFsbgp4PeerExtNextHopSelf) \
 nmhSetCmnWithLock(Fsbgp4PeerExtNextHopSelf, 11, Fsbgp4PeerExtNextHopSelfSet, BgpLock, BgpUnLock, 1, 0, 1, "%p %i", u4Fsbgp4PeerExtPeerRemoteAddr ,i4SetValFsbgp4PeerExtNextHopSelf)
#define nmhSetFsbgp4PeerExtRflClient(u4Fsbgp4PeerExtPeerRemoteAddr ,i4SetValFsbgp4PeerExtRflClient) \
 nmhSetCmnWithLock(Fsbgp4PeerExtRflClient, 11, Fsbgp4PeerExtRflClientSet, BgpLock, BgpUnLock, 1, 0, 1, "%p %i", u4Fsbgp4PeerExtPeerRemoteAddr ,i4SetValFsbgp4PeerExtRflClient)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4MEDIndex[11];
extern UINT4 Fsbgp4MEDAdminStatus[11];
extern UINT4 Fsbgp4MEDRemoteAS[11];
extern UINT4 Fsbgp4MEDIPAddrPrefix[11];
extern UINT4 Fsbgp4MEDIPAddrPrefixLen[11];
extern UINT4 Fsbgp4MEDIntermediateAS[11];
extern UINT4 Fsbgp4MEDDirection[11];
extern UINT4 Fsbgp4MEDValue[11];
extern UINT4 Fsbgp4MEDPreference[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4MEDAdminStatus(i4Fsbgp4MEDIndex ,i4SetValFsbgp4MEDAdminStatus) \
 nmhSetCmnWithLock(Fsbgp4MEDAdminStatus, 11, Fsbgp4MEDAdminStatusSet, BgpLock, BgpUnLock, 1, 0, 1, "%i %i", i4Fsbgp4MEDIndex ,i4SetValFsbgp4MEDAdminStatus)
#define nmhSetFsbgp4MEDRemoteAS(i4Fsbgp4MEDIndex ,u4SetValFsbgp4MEDRemoteAS) \
 nmhSetCmnWithLock(Fsbgp4MEDRemoteAS, 11, Fsbgp4MEDRemoteASSet, BgpLock, BgpUnLock, 1, 0, 1, "%i %u", i4Fsbgp4MEDIndex ,u4SetValFsbgp4MEDRemoteAS)
#define nmhSetFsbgp4MEDIPAddrPrefix(i4Fsbgp4MEDIndex ,u4SetValFsbgp4MEDIPAddrPrefix) \
 nmhSetCmnWithLock(Fsbgp4MEDIPAddrPrefix, 11, Fsbgp4MEDIPAddrPrefixSet, BgpLock, BgpUnLock, 1, 0, 1, "%i %p", i4Fsbgp4MEDIndex ,u4SetValFsbgp4MEDIPAddrPrefix)
#define nmhSetFsbgp4MEDIPAddrPrefixLen(i4Fsbgp4MEDIndex ,i4SetValFsbgp4MEDIPAddrPrefixLen) \
 nmhSetCmnWithLock(Fsbgp4MEDIPAddrPrefixLen, 11, Fsbgp4MEDIPAddrPrefixLenSet, BgpLock, BgpUnLock, 1, 0, 1, "%i %i", i4Fsbgp4MEDIndex ,i4SetValFsbgp4MEDIPAddrPrefixLen)
#define nmhSetFsbgp4MEDIntermediateAS(i4Fsbgp4MEDIndex ,pSetValFsbgp4MEDIntermediateAS) \
 nmhSetCmnWithLock(Fsbgp4MEDIntermediateAS, 11, Fsbgp4MEDIntermediateASSet, BgpLock, BgpUnLock, 1, 0, 1, "%i %s", i4Fsbgp4MEDIndex ,pSetValFsbgp4MEDIntermediateAS)
#define nmhSetFsbgp4MEDDirection(i4Fsbgp4MEDIndex ,i4SetValFsbgp4MEDDirection) \
 nmhSetCmnWithLock(Fsbgp4MEDDirection, 11, Fsbgp4MEDDirectionSet, BgpLock, BgpUnLock, 1, 0, 1, "%i %i", i4Fsbgp4MEDIndex ,i4SetValFsbgp4MEDDirection)
#define nmhSetFsbgp4MEDValue(i4Fsbgp4MEDIndex ,u4SetValFsbgp4MEDValue) \
 nmhSetCmnWithLock(Fsbgp4MEDValue, 11, Fsbgp4MEDValueSet, BgpLock, BgpUnLock, 1, 0, 1, "%i %u", i4Fsbgp4MEDIndex ,u4SetValFsbgp4MEDValue)
#define nmhSetFsbgp4MEDPreference(i4Fsbgp4MEDIndex ,i4SetValFsbgp4MEDPreference) \
 nmhSetCmnWithLock(Fsbgp4MEDPreference, 11, Fsbgp4MEDPreferenceSet, BgpLock, BgpUnLock, 1, 0, 1, "%i %i", i4Fsbgp4MEDIndex ,i4SetValFsbgp4MEDPreference)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4LocalPrefIndex[11];
extern UINT4 Fsbgp4LocalPrefAdminStatus[11];
extern UINT4 Fsbgp4LocalPrefRemoteAS[11];
extern UINT4 Fsbgp4LocalPrefIPAddrPrefix[11];
extern UINT4 Fsbgp4LocalPrefIPAddrPrefixLen[11];
extern UINT4 Fsbgp4LocalPrefIntermediateAS[11];
extern UINT4 Fsbgp4LocalPrefDirection[11];
extern UINT4 Fsbgp4LocalPrefValue[11];
extern UINT4 Fsbgp4LocalPrefPreference[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4LocalPrefAdminStatus(i4Fsbgp4LocalPrefIndex ,i4SetValFsbgp4LocalPrefAdminStatus) \
 nmhSetCmnWithLock(Fsbgp4LocalPrefAdminStatus, 11, Fsbgp4LocalPrefAdminStatusSet, BgpLock, BgpUnLock, 1, 0, 1, "%i %i", i4Fsbgp4LocalPrefIndex ,i4SetValFsbgp4LocalPrefAdminStatus)
#define nmhSetFsbgp4LocalPrefRemoteAS(i4Fsbgp4LocalPrefIndex ,u4SetValFsbgp4LocalPrefRemoteAS) \
 nmhSetCmnWithLock(Fsbgp4LocalPrefRemoteAS, 11, Fsbgp4LocalPrefRemoteASSet, BgpLock, BgpUnLock, 1, 0, 1, "%i %u", i4Fsbgp4LocalPrefIndex ,u4SetValFsbgp4LocalPrefRemoteAS)
#define nmhSetFsbgp4LocalPrefIPAddrPrefix(i4Fsbgp4LocalPrefIndex ,u4SetValFsbgp4LocalPrefIPAddrPrefix) \
 nmhSetCmnWithLock(Fsbgp4LocalPrefIPAddrPrefix, 11, Fsbgp4LocalPrefIPAddrPrefixSet, BgpLock, BgpUnLock, 1, 0, 1, "%i %p", i4Fsbgp4LocalPrefIndex ,u4SetValFsbgp4LocalPrefIPAddrPrefix)
#define nmhSetFsbgp4LocalPrefIPAddrPrefixLen(i4Fsbgp4LocalPrefIndex ,i4SetValFsbgp4LocalPrefIPAddrPrefixLen) \
 nmhSetCmnWithLock(Fsbgp4LocalPrefIPAddrPrefixLen, 11, Fsbgp4LocalPrefIPAddrPrefixLenSet, BgpLock, BgpUnLock, 1, 0, 1, "%i %i", i4Fsbgp4LocalPrefIndex ,i4SetValFsbgp4LocalPrefIPAddrPrefixLen)
#define nmhSetFsbgp4LocalPrefIntermediateAS(i4Fsbgp4LocalPrefIndex ,pSetValFsbgp4LocalPrefIntermediateAS) \
 nmhSetCmnWithLock(Fsbgp4LocalPrefIntermediateAS, 11, Fsbgp4LocalPrefIntermediateASSet, BgpLock, BgpUnLock, 1, 0, 1, "%i %s", i4Fsbgp4LocalPrefIndex ,pSetValFsbgp4LocalPrefIntermediateAS)
#define nmhSetFsbgp4LocalPrefDirection(i4Fsbgp4LocalPrefIndex ,i4SetValFsbgp4LocalPrefDirection) \
 nmhSetCmnWithLock(Fsbgp4LocalPrefDirection, 11, Fsbgp4LocalPrefDirectionSet, BgpLock, BgpUnLock, 1, 0, 1, "%i %i", i4Fsbgp4LocalPrefIndex ,i4SetValFsbgp4LocalPrefDirection)
#define nmhSetFsbgp4LocalPrefValue(i4Fsbgp4LocalPrefIndex ,u4SetValFsbgp4LocalPrefValue) \
 nmhSetCmnWithLock(Fsbgp4LocalPrefValue, 11, Fsbgp4LocalPrefValueSet, BgpLock, BgpUnLock, 1, 0, 1, "%i %u", i4Fsbgp4LocalPrefIndex ,u4SetValFsbgp4LocalPrefValue)
#define nmhSetFsbgp4LocalPrefPreference(i4Fsbgp4LocalPrefIndex ,i4SetValFsbgp4LocalPrefPreference) \
 nmhSetCmnWithLock(Fsbgp4LocalPrefPreference, 11, Fsbgp4LocalPrefPreferenceSet, BgpLock, BgpUnLock, 1, 0, 1, "%i %i", i4Fsbgp4LocalPrefIndex ,i4SetValFsbgp4LocalPrefPreference)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4UpdateFilterIndex[11];
extern UINT4 Fsbgp4UpdateFilterAdminStatus[11];
extern UINT4 Fsbgp4UpdateFilterRemoteAS[11];
extern UINT4 Fsbgp4UpdateFilterIPAddrPrefix[11];
extern UINT4 Fsbgp4UpdateFilterIPAddrPrefixLen[11];
extern UINT4 Fsbgp4UpdateFilterIntermediateAS[11];
extern UINT4 Fsbgp4UpdateFilterDirection[11];
extern UINT4 Fsbgp4UpdateFilterAction[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4UpdateFilterAdminStatus(i4Fsbgp4UpdateFilterIndex ,i4SetValFsbgp4UpdateFilterAdminStatus) \
 nmhSetCmnWithLock(Fsbgp4UpdateFilterAdminStatus, 11, Fsbgp4UpdateFilterAdminStatusSet, BgpLock, BgpUnLock, 1, 0, 1, "%i %i", i4Fsbgp4UpdateFilterIndex ,i4SetValFsbgp4UpdateFilterAdminStatus)
#define nmhSetFsbgp4UpdateFilterRemoteAS(i4Fsbgp4UpdateFilterIndex ,u4SetValFsbgp4UpdateFilterRemoteAS) \
 nmhSetCmnWithLock(Fsbgp4UpdateFilterRemoteAS, 11, Fsbgp4UpdateFilterRemoteASSet, BgpLock, BgpUnLock, 1, 0, 1, "%i %u", i4Fsbgp4UpdateFilterIndex ,u4SetValFsbgp4UpdateFilterRemoteAS)
#define nmhSetFsbgp4UpdateFilterIPAddrPrefix(i4Fsbgp4UpdateFilterIndex ,u4SetValFsbgp4UpdateFilterIPAddrPrefix) \
 nmhSetCmnWithLock(Fsbgp4UpdateFilterIPAddrPrefix, 11, Fsbgp4UpdateFilterIPAddrPrefixSet, BgpLock, BgpUnLock, 1, 0, 1, "%i %p", i4Fsbgp4UpdateFilterIndex ,u4SetValFsbgp4UpdateFilterIPAddrPrefix)
#define nmhSetFsbgp4UpdateFilterIPAddrPrefixLen(i4Fsbgp4UpdateFilterIndex ,i4SetValFsbgp4UpdateFilterIPAddrPrefixLen) \
 nmhSetCmnWithLock(Fsbgp4UpdateFilterIPAddrPrefixLen, 11, Fsbgp4UpdateFilterIPAddrPrefixLenSet, BgpLock, BgpUnLock, 1, 0, 1, "%i %i", i4Fsbgp4UpdateFilterIndex ,i4SetValFsbgp4UpdateFilterIPAddrPrefixLen)
#define nmhSetFsbgp4UpdateFilterIntermediateAS(i4Fsbgp4UpdateFilterIndex ,pSetValFsbgp4UpdateFilterIntermediateAS) \
 nmhSetCmnWithLock(Fsbgp4UpdateFilterIntermediateAS, 11, Fsbgp4UpdateFilterIntermediateASSet, BgpLock, BgpUnLock, 1, 0, 1, "%i %s", i4Fsbgp4UpdateFilterIndex ,pSetValFsbgp4UpdateFilterIntermediateAS)
#define nmhSetFsbgp4UpdateFilterDirection(i4Fsbgp4UpdateFilterIndex ,i4SetValFsbgp4UpdateFilterDirection) \
 nmhSetCmnWithLock(Fsbgp4UpdateFilterDirection, 11, Fsbgp4UpdateFilterDirectionSet, BgpLock, BgpUnLock, 1, 0, 1, "%i %i", i4Fsbgp4UpdateFilterIndex ,i4SetValFsbgp4UpdateFilterDirection)
#define nmhSetFsbgp4UpdateFilterAction(i4Fsbgp4UpdateFilterIndex ,i4SetValFsbgp4UpdateFilterAction) \
 nmhSetCmnWithLock(Fsbgp4UpdateFilterAction, 11, Fsbgp4UpdateFilterActionSet, BgpLock, BgpUnLock, 1, 0, 1, "%i %i", i4Fsbgp4UpdateFilterIndex ,i4SetValFsbgp4UpdateFilterAction)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4AggregateIndex[11];
extern UINT4 Fsbgp4AggregateAdminStatus[11];
extern UINT4 Fsbgp4AggregateIPAddrPrefix[11];
extern UINT4 Fsbgp4AggregateIPAddrPrefixLen[11];
extern UINT4 Fsbgp4AggregateAdvertise[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4AggregateAdminStatus(i4Fsbgp4AggregateIndex ,i4SetValFsbgp4AggregateAdminStatus) \
 nmhSetCmnWithLock(Fsbgp4AggregateAdminStatus, 11, Fsbgp4AggregateAdminStatusSet, BgpLock, BgpUnLock, 1, 0, 1, "%i %i", i4Fsbgp4AggregateIndex ,i4SetValFsbgp4AggregateAdminStatus)
#define nmhSetFsbgp4AggregateIPAddrPrefix(i4Fsbgp4AggregateIndex ,u4SetValFsbgp4AggregateIPAddrPrefix) \
 nmhSetCmnWithLock(Fsbgp4AggregateIPAddrPrefix, 11, Fsbgp4AggregateIPAddrPrefixSet, BgpLock, BgpUnLock, 1, 0, 1, "%i %p", i4Fsbgp4AggregateIndex ,u4SetValFsbgp4AggregateIPAddrPrefix)
#define nmhSetFsbgp4AggregateIPAddrPrefixLen(i4Fsbgp4AggregateIndex ,i4SetValFsbgp4AggregateIPAddrPrefixLen) \
 nmhSetCmnWithLock(Fsbgp4AggregateIPAddrPrefixLen, 11, Fsbgp4AggregateIPAddrPrefixLenSet, BgpLock, BgpUnLock, 1, 0, 1, "%i %i", i4Fsbgp4AggregateIndex ,i4SetValFsbgp4AggregateIPAddrPrefixLen)
#define nmhSetFsbgp4AggregateAdvertise(i4Fsbgp4AggregateIndex ,i4SetValFsbgp4AggregateAdvertise) \
 nmhSetCmnWithLock(Fsbgp4AggregateAdvertise, 11, Fsbgp4AggregateAdvertiseSet, BgpLock, BgpUnLock, 1, 0, 1, "%i %i", i4Fsbgp4AggregateIndex ,i4SetValFsbgp4AggregateAdvertise)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4RRDAdminStatus[10];
extern UINT4 Fsbgp4RRDProtoMaskForEnable[10];
extern UINT4 Fsbgp4RRDSrcProtoMaskForDisable[10];
extern UINT4 Fsbgp4RRDDefaultMetric[10];
extern UINT4 Fsbgp4RRDRouteMapName[10];
extern UINT4 Fsbgp4RRDMatchTypeEnable[10];
extern UINT4 Fsbgp4RRDMatchTypeDisable[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4RRDAdminStatus(i4SetValFsbgp4RRDAdminStatus) \
 nmhSetCmnWithLock(Fsbgp4RRDAdminStatus, 10, Fsbgp4RRDAdminStatusSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4RRDAdminStatus)
#define nmhSetFsbgp4RRDProtoMaskForEnable(i4SetValFsbgp4RRDProtoMaskForEnable) \
 nmhSetCmnWithLock(Fsbgp4RRDProtoMaskForEnable, 10, Fsbgp4RRDProtoMaskForEnableSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4RRDProtoMaskForEnable)
#define nmhSetFsbgp4RRDSrcProtoMaskForDisable(i4SetValFsbgp4RRDSrcProtoMaskForDisable) \
 nmhSetCmnWithLock(Fsbgp4RRDSrcProtoMaskForDisable, 10, Fsbgp4RRDSrcProtoMaskForDisableSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4RRDSrcProtoMaskForDisable)
#define nmhSetFsbgp4RRDDefaultMetric(u4SetValFsbgp4RRDDefaultMetric) \
 nmhSetCmnWithLock(Fsbgp4RRDDefaultMetric, 10, Fsbgp4RRDDefaultMetricSet, BgpLock, BgpUnLock, 0, 0, 0, "%u", u4SetValFsbgp4RRDDefaultMetric)
#define nmhSetFsbgp4RRDRouteMapName(pSetValFsbgp4RRDRouteMapName) \
 nmhSetCmnWithLock(Fsbgp4RRDRouteMapName, 10, Fsbgp4RRDRouteMapNameSet, BgpLock, BgpUnLock, 0, 0, 0, "%s", pSetValFsbgp4RRDRouteMapName)
#define nmhSetFsbgp4RRDMatchTypeEnable(i4SetValFsbgp4RRDMatchTypeEnable) \
 nmhSetCmnWithLock(Fsbgp4RRDMatchTypeEnable, 10, Fsbgp4RRDMatchTypeEnableSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4RRDMatchTypeEnable)
#define nmhSetFsbgp4RRDMatchTypeDisable(i4SetValFsbgp4RRDMatchTypeDisable) \
 nmhSetCmnWithLock(Fsbgp4RRDMatchTypeDisable, 10, Fsbgp4RRDMatchTypeDisableSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4RRDMatchTypeDisable)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsBgp4RRDMetricProtocolId[12];
extern UINT4 FsBgp4RRDMetricValue[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsBgp4RRDMetricValue(i4FsBgp4RRDMetricProtocolId ,i4SetValFsBgp4RRDMetricValue) \
 nmhSetCmnWithLock(FsBgp4RRDMetricValue, 12, FsBgp4RRDMetricValueSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %i", i4FsBgp4RRDMetricProtocolId ,i4SetValFsBgp4RRDMetricValue)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4ImportRoutePrefix[11];
extern UINT4 Fsbgp4ImportRoutePrefixLen[11];
extern UINT4 Fsbgp4ImportRouteProtocol[11];
extern UINT4 Fsbgp4ImportRouteNextHop[11];
extern UINT4 Fsbgp4ImportRouteIfIndex[11];
extern UINT4 Fsbgp4ImportRouteMetric[11];
extern UINT4 Fsbgp4ImportRouteAction[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4ImportRouteAction(u4Fsbgp4ImportRoutePrefix , i4Fsbgp4ImportRoutePrefixLen , i4Fsbgp4ImportRouteProtocol , u4Fsbgp4ImportRouteNextHop , i4Fsbgp4ImportRouteIfIndex , i4Fsbgp4ImportRouteMetric ,i4SetValFsbgp4ImportRouteAction) \
 nmhSetCmnWithLock(Fsbgp4ImportRouteAction, 11, Fsbgp4ImportRouteActionSet, BgpLock, BgpUnLock, 1, 0, 6, "%p %i %i %p %i %i %i", u4Fsbgp4ImportRoutePrefix , i4Fsbgp4ImportRoutePrefixLen , i4Fsbgp4ImportRouteProtocol , u4Fsbgp4ImportRouteNextHop , i4Fsbgp4ImportRouteIfIndex , i4Fsbgp4ImportRouteMetric ,i4SetValFsbgp4ImportRouteAction)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4RflbgpClusterId[11];
extern UINT4 Fsbgp4RflRflSupport[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4RflbgpClusterId(pSetValFsbgp4RflbgpClusterId) \
 nmhSetCmnWithLock(Fsbgp4RflbgpClusterId, 11, Fsbgp4RflbgpClusterIdSet, BgpLock, BgpUnLock, 0, 0, 0, "%s", pSetValFsbgp4RflbgpClusterId)
#define nmhSetFsbgp4RflRflSupport(i4SetValFsbgp4RflRflSupport) \
 nmhSetCmnWithLock(Fsbgp4RflRflSupport, 11, Fsbgp4RflRflSupportSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4RflRflSupport)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4RfdCutOff[11];
extern UINT4 Fsbgp4RfdReuse[11];
extern UINT4 Fsbgp4RfdMaxHoldDownTime[11];
extern UINT4 Fsbgp4RfdDecayHalfLifeTime[11];
extern UINT4 Fsbgp4RfdDecayTimerGranularity[11];
extern UINT4 Fsbgp4RfdReuseTimerGranularity[11];
extern UINT4 Fsbgp4RfdReuseIndxArraySize[11];
extern UINT4 Fsbgp4RfdAdminStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4RfdCutOff(i4SetValFsbgp4RfdCutOff) \
 nmhSetCmnWithLock(Fsbgp4RfdCutOff, 11, Fsbgp4RfdCutOffSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4RfdCutOff)
#define nmhSetFsbgp4RfdReuse(i4SetValFsbgp4RfdReuse) \
 nmhSetCmnWithLock(Fsbgp4RfdReuse, 11, Fsbgp4RfdReuseSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4RfdReuse)
#define nmhSetFsbgp4RfdMaxHoldDownTime(i4SetValFsbgp4RfdMaxHoldDownTime) \
 nmhSetCmnWithLock(Fsbgp4RfdMaxHoldDownTime, 11, Fsbgp4RfdMaxHoldDownTimeSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4RfdMaxHoldDownTime)
#define nmhSetFsbgp4RfdDecayHalfLifeTime(i4SetValFsbgp4RfdDecayHalfLifeTime) \
 nmhSetCmnWithLock(Fsbgp4RfdDecayHalfLifeTime, 11, Fsbgp4RfdDecayHalfLifeTimeSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4RfdDecayHalfLifeTime)
#define nmhSetFsbgp4RfdDecayTimerGranularity(i4SetValFsbgp4RfdDecayTimerGranularity) \
 nmhSetCmnWithLock(Fsbgp4RfdDecayTimerGranularity, 11, Fsbgp4RfdDecayTimerGranularitySet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4RfdDecayTimerGranularity)
#define nmhSetFsbgp4RfdReuseTimerGranularity(i4SetValFsbgp4RfdReuseTimerGranularity) \
 nmhSetCmnWithLock(Fsbgp4RfdReuseTimerGranularity, 11, Fsbgp4RfdReuseTimerGranularitySet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4RfdReuseTimerGranularity)
#define nmhSetFsbgp4RfdReuseIndxArraySize(i4SetValFsbgp4RfdReuseIndxArraySize) \
 nmhSetCmnWithLock(Fsbgp4RfdReuseIndxArraySize, 11, Fsbgp4RfdReuseIndxArraySizeSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4RfdReuseIndxArraySize)
#define nmhSetFsbgp4RfdAdminStatus(i4SetValFsbgp4RfdAdminStatus) \
 nmhSetCmnWithLock(Fsbgp4RfdAdminStatus, 11, Fsbgp4RfdAdminStatusSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4RfdAdminStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4CommMaxInFTblEntries[11];
extern UINT4 Fsbgp4CommMaxOutFTblEntries[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4CommMaxInFTblEntries(i4SetValFsbgp4CommMaxInFTblEntries) \
 nmhSetCmnWithLock(Fsbgp4CommMaxInFTblEntries, 11, Fsbgp4CommMaxInFTblEntriesSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4CommMaxInFTblEntries)
#define nmhSetFsbgp4CommMaxOutFTblEntries(i4SetValFsbgp4CommMaxOutFTblEntries) \
 nmhSetCmnWithLock(Fsbgp4CommMaxOutFTblEntries, 11, Fsbgp4CommMaxOutFTblEntriesSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4CommMaxOutFTblEntries)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4AddCommIpNetwork[12];
extern UINT4 Fsbgp4AddCommIpPrefixLen[12];
extern UINT4 Fsbgp4AddCommVal[12];
extern UINT4 Fsbgp4AddCommRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4AddCommRowStatus(u4Fsbgp4AddCommIpNetwork , i4Fsbgp4AddCommIpPrefixLen , u4Fsbgp4AddCommVal ,i4SetValFsbgp4AddCommRowStatus) \
 nmhSetCmnWithLock(Fsbgp4AddCommRowStatus, 12, Fsbgp4AddCommRowStatusSet, BgpLock, BgpUnLock, 1, 1, 3, "%p %i %u %i", u4Fsbgp4AddCommIpNetwork , i4Fsbgp4AddCommIpPrefixLen , u4Fsbgp4AddCommVal ,i4SetValFsbgp4AddCommRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4DeleteCommIpNetwork[12];
extern UINT4 Fsbgp4DeleteCommIpPrefixLen[12];
extern UINT4 Fsbgp4DeleteCommVal[12];
extern UINT4 Fsbgp4DeleteCommRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4DeleteCommRowStatus(u4Fsbgp4DeleteCommIpNetwork , i4Fsbgp4DeleteCommIpPrefixLen , u4Fsbgp4DeleteCommVal ,i4SetValFsbgp4DeleteCommRowStatus) \
 nmhSetCmnWithLock(Fsbgp4DeleteCommRowStatus, 12, Fsbgp4DeleteCommRowStatusSet, BgpLock, BgpUnLock, 1, 1, 3, "%p %i %u %i", u4Fsbgp4DeleteCommIpNetwork , i4Fsbgp4DeleteCommIpPrefixLen , u4Fsbgp4DeleteCommVal ,i4SetValFsbgp4DeleteCommRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4CommSetStatusIpNetwork[12];
extern UINT4 Fsbgp4CommSetStatusIpPrefixLen[12];
extern UINT4 Fsbgp4CommSetStatus[12];
extern UINT4 Fsbgp4CommSetStatusRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4CommSetStatus(u4Fsbgp4CommSetStatusIpNetwork , i4Fsbgp4CommSetStatusIpPrefixLen ,i4SetValFsbgp4CommSetStatus) \
 nmhSetCmnWithLock(Fsbgp4CommSetStatus, 12, Fsbgp4CommSetStatusSet, BgpLock, BgpUnLock, 1, 0, 2, "%p %i %i", u4Fsbgp4CommSetStatusIpNetwork , i4Fsbgp4CommSetStatusIpPrefixLen ,i4SetValFsbgp4CommSetStatus)
#define nmhSetFsbgp4CommSetStatusRowStatus(u4Fsbgp4CommSetStatusIpNetwork , i4Fsbgp4CommSetStatusIpPrefixLen ,i4SetValFsbgp4CommSetStatusRowStatus) \
 nmhSetCmnWithLock(Fsbgp4CommSetStatusRowStatus, 12, Fsbgp4CommSetStatusRowStatusSet, BgpLock, BgpUnLock, 1, 1, 2, "%p %i %i", u4Fsbgp4CommSetStatusIpNetwork , i4Fsbgp4CommSetStatusIpPrefixLen ,i4SetValFsbgp4CommSetStatusRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4PeerAddress[12];
extern UINT4 Fsbgp4CommSendStatus[12];
extern UINT4 Fsbgp4CommPeerSendRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4CommSendStatus(u4Fsbgp4PeerAddress ,i4SetValFsbgp4CommSendStatus) \
 nmhSetCmnWithLock(Fsbgp4CommSendStatus, 12, Fsbgp4CommSendStatusSet, BgpLock, BgpUnLock, 1, 0, 1, "%p %i", u4Fsbgp4PeerAddress ,i4SetValFsbgp4CommSendStatus)
#define nmhSetFsbgp4CommPeerSendRowStatus(u4Fsbgp4PeerAddress ,i4SetValFsbgp4CommPeerSendRowStatus) \
 nmhSetCmnWithLock(Fsbgp4CommPeerSendRowStatus, 12, Fsbgp4CommPeerSendRowStatusSet, BgpLock, BgpUnLock, 1, 1, 1, "%p %i", u4Fsbgp4PeerAddress ,i4SetValFsbgp4CommPeerSendRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4InFilterCommVal[12];
extern UINT4 Fsbgp4CommIncomingFilterStatus[12];
extern UINT4 Fsbgp4InFilterRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4CommIncomingFilterStatus(u4Fsbgp4InFilterCommVal ,i4SetValFsbgp4CommIncomingFilterStatus) \
 nmhSetCmnWithLock(Fsbgp4CommIncomingFilterStatus, 12, Fsbgp4CommIncomingFilterStatusSet, BgpLock, BgpUnLock, 0, 0, 1, "%u %i", u4Fsbgp4InFilterCommVal ,i4SetValFsbgp4CommIncomingFilterStatus)
#define nmhSetFsbgp4InFilterRowStatus(u4Fsbgp4InFilterCommVal ,i4SetValFsbgp4InFilterRowStatus) \
 nmhSetCmnWithLock(Fsbgp4InFilterRowStatus, 12, Fsbgp4InFilterRowStatusSet, BgpLock, BgpUnLock, 0, 1, 1, "%u %i", u4Fsbgp4InFilterCommVal ,i4SetValFsbgp4InFilterRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4OutFilterCommVal[12];
extern UINT4 Fsbgp4CommOutgoingFilterStatus[12];
extern UINT4 Fsbgp4OutFilterRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4CommOutgoingFilterStatus(u4Fsbgp4OutFilterCommVal ,i4SetValFsbgp4CommOutgoingFilterStatus) \
 nmhSetCmnWithLock(Fsbgp4CommOutgoingFilterStatus, 12, Fsbgp4CommOutgoingFilterStatusSet, BgpLock, BgpUnLock, 0, 0, 1, "%u %i", u4Fsbgp4OutFilterCommVal ,i4SetValFsbgp4CommOutgoingFilterStatus)
#define nmhSetFsbgp4OutFilterRowStatus(u4Fsbgp4OutFilterCommVal ,i4SetValFsbgp4OutFilterRowStatus) \
 nmhSetCmnWithLock(Fsbgp4OutFilterRowStatus, 12, Fsbgp4OutFilterRowStatusSet, BgpLock, BgpUnLock, 0, 1, 1, "%u %i", u4Fsbgp4OutFilterCommVal ,i4SetValFsbgp4OutFilterRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4ExtCommMaxInFTblEntries[11];
extern UINT4 Fsbgp4ExtCommMaxOutFTblEntries[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4ExtCommMaxInFTblEntries(i4SetValFsbgp4ExtCommMaxInFTblEntries) \
 nmhSetCmnWithLock(Fsbgp4ExtCommMaxInFTblEntries, 11, Fsbgp4ExtCommMaxInFTblEntriesSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4ExtCommMaxInFTblEntries)
#define nmhSetFsbgp4ExtCommMaxOutFTblEntries(i4SetValFsbgp4ExtCommMaxOutFTblEntries) \
 nmhSetCmnWithLock(Fsbgp4ExtCommMaxOutFTblEntries, 11, Fsbgp4ExtCommMaxOutFTblEntriesSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4ExtCommMaxOutFTblEntries)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4AddExtCommIpNetwork[12];
extern UINT4 Fsbgp4AddExtCommIpPrefixLen[12];
extern UINT4 Fsbgp4AddExtCommVal[12];
extern UINT4 Fsbgp4AddExtCommRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4AddExtCommRowStatus(u4Fsbgp4AddExtCommIpNetwork , i4Fsbgp4AddExtCommIpPrefixLen , pFsbgp4AddExtCommVal ,i4SetValFsbgp4AddExtCommRowStatus) \
 nmhSetCmnWithLock(Fsbgp4AddExtCommRowStatus, 12, Fsbgp4AddExtCommRowStatusSet, BgpLock, BgpUnLock, 1, 1, 3, "%p %i %s %i", u4Fsbgp4AddExtCommIpNetwork , i4Fsbgp4AddExtCommIpPrefixLen , pFsbgp4AddExtCommVal ,i4SetValFsbgp4AddExtCommRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4DeleteExtCommIpNetwork[12];
extern UINT4 Fsbgp4DeleteExtCommIpPrefixLen[12];
extern UINT4 Fsbgp4DeleteExtCommVal[12];
extern UINT4 Fsbgp4DeleteExtCommRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4DeleteExtCommRowStatus(u4Fsbgp4DeleteExtCommIpNetwork , i4Fsbgp4DeleteExtCommIpPrefixLen , pFsbgp4DeleteExtCommVal ,i4SetValFsbgp4DeleteExtCommRowStatus) \
 nmhSetCmnWithLock(Fsbgp4DeleteExtCommRowStatus, 12, Fsbgp4DeleteExtCommRowStatusSet, BgpLock, BgpUnLock, 1, 1, 3, "%p %i %s %i", u4Fsbgp4DeleteExtCommIpNetwork , i4Fsbgp4DeleteExtCommIpPrefixLen , pFsbgp4DeleteExtCommVal ,i4SetValFsbgp4DeleteExtCommRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4ExtCommSetStatusIpNetwork[12];
extern UINT4 Fsbgp4ExtCommSetStatusIpPrefixLen[12];
extern UINT4 Fsbgp4ExtCommSetStatus[12];
extern UINT4 Fsbgp4ExtCommSetStatusRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4ExtCommSetStatus(u4Fsbgp4ExtCommSetStatusIpNetwork , i4Fsbgp4ExtCommSetStatusIpPrefixLen ,i4SetValFsbgp4ExtCommSetStatus) \
 nmhSetCmnWithLock(Fsbgp4ExtCommSetStatus, 12, Fsbgp4ExtCommSetStatusSet, BgpLock, BgpUnLock, 1, 0, 2, "%p %i %i", u4Fsbgp4ExtCommSetStatusIpNetwork , i4Fsbgp4ExtCommSetStatusIpPrefixLen ,i4SetValFsbgp4ExtCommSetStatus)
#define nmhSetFsbgp4ExtCommSetStatusRowStatus(u4Fsbgp4ExtCommSetStatusIpNetwork , i4Fsbgp4ExtCommSetStatusIpPrefixLen ,i4SetValFsbgp4ExtCommSetStatusRowStatus) \
 nmhSetCmnWithLock(Fsbgp4ExtCommSetStatusRowStatus, 12, Fsbgp4ExtCommSetStatusRowStatusSet, BgpLock, BgpUnLock, 1, 1, 2, "%p %i %i", u4Fsbgp4ExtCommSetStatusIpNetwork , i4Fsbgp4ExtCommSetStatusIpPrefixLen ,i4SetValFsbgp4ExtCommSetStatusRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4ExtCommPeerAddress[12];
extern UINT4 Fsbgp4ExtCommPeerSendStatus[12];
extern UINT4 Fsbgp4ExtCommPeerSendStatusRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4ExtCommPeerSendStatus(u4Fsbgp4ExtCommPeerAddress ,i4SetValFsbgp4ExtCommPeerSendStatus) \
 nmhSetCmnWithLock(Fsbgp4ExtCommPeerSendStatus, 12, Fsbgp4ExtCommPeerSendStatusSet, BgpLock, BgpUnLock, 1, 0, 1, "%p %i", u4Fsbgp4ExtCommPeerAddress ,i4SetValFsbgp4ExtCommPeerSendStatus)
#define nmhSetFsbgp4ExtCommPeerSendStatusRowStatus(u4Fsbgp4ExtCommPeerAddress ,i4SetValFsbgp4ExtCommPeerSendStatusRowStatus) \
 nmhSetCmnWithLock(Fsbgp4ExtCommPeerSendStatusRowStatus, 12, Fsbgp4ExtCommPeerSendStatusRowStatusSet, BgpLock, BgpUnLock, 1, 1, 1, "%p %i", u4Fsbgp4ExtCommPeerAddress ,i4SetValFsbgp4ExtCommPeerSendStatusRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4ExtCommInFilterCommVal[12];
extern UINT4 Fsbgp4ExtCommIncomingFilterStatus[12];
extern UINT4 Fsbgp4ExtCommInFilterRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4ExtCommIncomingFilterStatus(pFsbgp4ExtCommInFilterCommVal ,i4SetValFsbgp4ExtCommIncomingFilterStatus) \
 nmhSetCmnWithLock(Fsbgp4ExtCommIncomingFilterStatus, 12, Fsbgp4ExtCommIncomingFilterStatusSet, BgpLock, BgpUnLock, 0, 0, 1, "%s %i", pFsbgp4ExtCommInFilterCommVal ,i4SetValFsbgp4ExtCommIncomingFilterStatus)
#define nmhSetFsbgp4ExtCommInFilterRowStatus(pFsbgp4ExtCommInFilterCommVal ,i4SetValFsbgp4ExtCommInFilterRowStatus) \
 nmhSetCmnWithLock(Fsbgp4ExtCommInFilterRowStatus, 12, Fsbgp4ExtCommInFilterRowStatusSet, BgpLock, BgpUnLock, 0, 1, 1, "%s %i", pFsbgp4ExtCommInFilterCommVal ,i4SetValFsbgp4ExtCommInFilterRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4ExtCommOutFilterCommVal[12];
extern UINT4 Fsbgp4ExtCommOutgoingFilterStatus[12];
extern UINT4 Fsbgp4ExtCommOutFilterRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4ExtCommOutgoingFilterStatus(pFsbgp4ExtCommOutFilterCommVal ,i4SetValFsbgp4ExtCommOutgoingFilterStatus) \
 nmhSetCmnWithLock(Fsbgp4ExtCommOutgoingFilterStatus, 12, Fsbgp4ExtCommOutgoingFilterStatusSet, BgpLock, BgpUnLock, 0, 0, 1, "%s %i", pFsbgp4ExtCommOutFilterCommVal ,i4SetValFsbgp4ExtCommOutgoingFilterStatus)
#define nmhSetFsbgp4ExtCommOutFilterRowStatus(pFsbgp4ExtCommOutFilterCommVal ,i4SetValFsbgp4ExtCommOutFilterRowStatus) \
 nmhSetCmnWithLock(Fsbgp4ExtCommOutFilterRowStatus, 12, Fsbgp4ExtCommOutFilterRowStatusSet, BgpLock, BgpUnLock, 0, 1, 1, "%s %i", pFsbgp4ExtCommOutFilterCommVal ,i4SetValFsbgp4ExtCommOutFilterRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4PeerLinkRemAddr[12];
extern UINT4 Fsbgp4LinkBandWidth[12];
extern UINT4 Fsbgp4PeerLinkBwRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4LinkBandWidth(u4Fsbgp4PeerLinkRemAddr ,u4SetValFsbgp4LinkBandWidth) \
 nmhSetCmnWithLock(Fsbgp4LinkBandWidth, 12, Fsbgp4LinkBandWidthSet, BgpLock, BgpUnLock, 1, 0, 1, "%p %u", u4Fsbgp4PeerLinkRemAddr ,u4SetValFsbgp4LinkBandWidth)
#define nmhSetFsbgp4PeerLinkBwRowStatus(u4Fsbgp4PeerLinkRemAddr ,i4SetValFsbgp4PeerLinkBwRowStatus) \
 nmhSetCmnWithLock(Fsbgp4PeerLinkBwRowStatus, 12, Fsbgp4PeerLinkBwRowStatusSet, BgpLock, BgpUnLock, 1, 1, 1, "%p %i", u4Fsbgp4PeerLinkRemAddr ,i4SetValFsbgp4PeerLinkBwRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4CapabilitySupportAvailable[11];
extern UINT4 Fsbgp4MaxCapsPerPeer[11];
extern UINT4 Fsbgp4MaxInstancesPerCap[11];
extern UINT4 Fsbgp4MaxCapDataSize[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4CapabilitySupportAvailable(i4SetValFsbgp4CapabilitySupportAvailable) \
 nmhSetCmnWithLock(Fsbgp4CapabilitySupportAvailable, 11, Fsbgp4CapabilitySupportAvailableSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4CapabilitySupportAvailable)
#define nmhSetFsbgp4MaxCapsPerPeer(i4SetValFsbgp4MaxCapsPerPeer) \
 nmhSetCmnWithLock(Fsbgp4MaxCapsPerPeer, 11, Fsbgp4MaxCapsPerPeerSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4MaxCapsPerPeer)
#define nmhSetFsbgp4MaxInstancesPerCap(i4SetValFsbgp4MaxInstancesPerCap) \
 nmhSetCmnWithLock(Fsbgp4MaxInstancesPerCap, 11, Fsbgp4MaxInstancesPerCapSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4MaxInstancesPerCap)
#define nmhSetFsbgp4MaxCapDataSize(i4SetValFsbgp4MaxCapDataSize) \
 nmhSetCmnWithLock(Fsbgp4MaxCapDataSize, 11, Fsbgp4MaxCapDataSizeSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgp4MaxCapDataSize)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4CapPeerRemoteIpAddr[12];
extern UINT4 Fsbgp4SupportedCapabilityCode[12];
extern UINT4 Fsbgp4SupportedCapabilityInstance[12];
extern UINT4 Fsbgp4SupportedCapabilityLength[12];
extern UINT4 Fsbgp4SupportedCapabilityValue[12];
extern UINT4 Fsbgp4CapSupportedCapsRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4SupportedCapabilityLength(u4Fsbgp4CapPeerRemoteIpAddr , i4Fsbgp4SupportedCapabilityCode , i4Fsbgp4SupportedCapabilityInstance ,i4SetValFsbgp4SupportedCapabilityLength) \
 nmhSetCmnWithLock(Fsbgp4SupportedCapabilityLength, 12, Fsbgp4SupportedCapabilityLengthSet, BgpLock, BgpUnLock, 1, 0, 3, "%p %i %i %i", u4Fsbgp4CapPeerRemoteIpAddr , i4Fsbgp4SupportedCapabilityCode , i4Fsbgp4SupportedCapabilityInstance ,i4SetValFsbgp4SupportedCapabilityLength)
#define nmhSetFsbgp4SupportedCapabilityValue(u4Fsbgp4CapPeerRemoteIpAddr , i4Fsbgp4SupportedCapabilityCode , i4Fsbgp4SupportedCapabilityInstance ,pSetValFsbgp4SupportedCapabilityValue) \
 nmhSetCmnWithLock(Fsbgp4SupportedCapabilityValue, 12, Fsbgp4SupportedCapabilityValueSet, BgpLock, BgpUnLock, 1, 0, 3, "%p %i %i %s", u4Fsbgp4CapPeerRemoteIpAddr , i4Fsbgp4SupportedCapabilityCode , i4Fsbgp4SupportedCapabilityInstance ,pSetValFsbgp4SupportedCapabilityValue)
#define nmhSetFsbgp4CapSupportedCapsRowStatus(u4Fsbgp4CapPeerRemoteIpAddr , i4Fsbgp4SupportedCapabilityCode , i4Fsbgp4SupportedCapabilityInstance ,i4SetValFsbgp4CapSupportedCapsRowStatus) \
 nmhSetCmnWithLock(Fsbgp4CapSupportedCapsRowStatus, 12, Fsbgp4CapSupportedCapsRowStatusSet, BgpLock, BgpUnLock, 1, 1, 3, "%p %i %i %i", u4Fsbgp4CapPeerRemoteIpAddr , i4Fsbgp4SupportedCapabilityCode , i4Fsbgp4SupportedCapabilityInstance ,i4SetValFsbgp4CapSupportedCapsRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4PeerRemIpAddr[12];
extern UINT4 Fsbgp4StrictCapabilityMatch[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4StrictCapabilityMatch(u4Fsbgp4PeerRemIpAddr ,i4SetValFsbgp4StrictCapabilityMatch) \
 nmhSetCmnWithLock(Fsbgp4StrictCapabilityMatch, 12, Fsbgp4StrictCapabilityMatchSet, BgpLock, BgpUnLock, 1, 0, 1, "%p %i", u4Fsbgp4PeerRemIpAddr ,i4SetValFsbgp4StrictCapabilityMatch)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsbgpAscConfedId[11];
extern UINT4 FsbgpAscConfedBestPathCompareMED[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgpAscConfedId(u4SetValFsbgpAscConfedId) \
 nmhSetCmnWithLock(FsbgpAscConfedId, 11, FsbgpAscConfedIdSet, BgpLock, BgpUnLock, 0, 0, 0, "%u", u4SetValFsbgpAscConfedId)
#define nmhSetFsbgpAscConfedBestPathCompareMED(i4SetValFsbgpAscConfedBestPathCompareMED) \
 nmhSetCmnWithLock(FsbgpAscConfedBestPathCompareMED, 11, FsbgpAscConfedBestPathCompareMEDSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsbgpAscConfedBestPathCompareMED)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsbgpAscConfedPeerASNo[12];
extern UINT4 FsbgpAscConfedPeerStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgpAscConfedPeerStatus(u4FsbgpAscConfedPeerASNo ,i4SetValFsbgpAscConfedPeerStatus) \
 nmhSetCmnWithLock(FsbgpAscConfedPeerStatus, 12, FsbgpAscConfedPeerStatusSet, BgpLock, BgpUnLock, 0, 0, 1, "%u %i", u4FsbgpAscConfedPeerASNo ,i4SetValFsbgpAscConfedPeerStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4RtRefreshAllPeerInboundRequest[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4RtRefreshAllPeerInboundRequest(i4SetValFsbgp4RtRefreshAllPeerInboundRequest) \
 nmhSetCmnWithLock(Fsbgp4RtRefreshAllPeerInboundRequest, 10, Fsbgp4RtRefreshAllPeerInboundRequestSet, BgpLock, BgpUnLock, 1, 0, 0, "%i", i4SetValFsbgp4RtRefreshAllPeerInboundRequest)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4RtRefreshInboundPeerType[12];
extern UINT4 Fsbgp4RtRefreshInboundPeerAddr[12];
extern UINT4 Fsbgp4RtRefreshInboundRequest[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4RtRefreshInboundRequest(i4Fsbgp4RtRefreshInboundPeerType , pFsbgp4RtRefreshInboundPeerAddr ,i4SetValFsbgp4RtRefreshInboundRequest) \
 nmhSetCmnWithLock(Fsbgp4RtRefreshInboundRequest, 12, Fsbgp4RtRefreshInboundRequestSet, BgpLock, BgpUnLock, 1, 0, 2, "%i %s %i", i4Fsbgp4RtRefreshInboundPeerType , pFsbgp4RtRefreshInboundPeerAddr ,i4SetValFsbgp4RtRefreshInboundRequest)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4TCPMD5AuthPeerType[12];
extern UINT4 Fsbgp4TCPMD5AuthPeerAddr[12];
extern UINT4 Fsbgp4TCPMD5AuthPassword[12];
extern UINT4 Fsbgp4TCPMD5AuthPwdSet[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4TCPMD5AuthPassword(i4Fsbgp4TCPMD5AuthPeerType , pFsbgp4TCPMD5AuthPeerAddr ,pSetValFsbgp4TCPMD5AuthPassword) \
 nmhSetCmnWithLock(Fsbgp4TCPMD5AuthPassword, 12, Fsbgp4TCPMD5AuthPasswordSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %s", i4Fsbgp4TCPMD5AuthPeerType , pFsbgp4TCPMD5AuthPeerAddr ,pSetValFsbgp4TCPMD5AuthPassword)
#define nmhSetFsbgp4TCPMD5AuthPwdSet(i4Fsbgp4TCPMD5AuthPeerType , pFsbgp4TCPMD5AuthPeerAddr ,i4SetValFsbgp4TCPMD5AuthPwdSet) \
 nmhSetCmnWithLock(Fsbgp4TCPMD5AuthPwdSet, 12, Fsbgp4TCPMD5AuthPwdSetSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %i", i4Fsbgp4TCPMD5AuthPeerType , pFsbgp4TCPMD5AuthPeerAddr ,i4SetValFsbgp4TCPMD5AuthPwdSet)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4SoftReconfigAllPeerOutboundRequest[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4SoftReconfigAllPeerOutboundRequest(i4SetValFsbgp4SoftReconfigAllPeerOutboundRequest) \
 nmhSetCmnWithLock(Fsbgp4SoftReconfigAllPeerOutboundRequest, 10, Fsbgp4SoftReconfigAllPeerOutboundRequestSet, BgpLock, BgpUnLock, 1, 0, 0, "%i", i4SetValFsbgp4SoftReconfigAllPeerOutboundRequest)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4SoftReconfigOutboundPeerType[12];
extern UINT4 Fsbgp4SoftReconfigOutboundPeerAddr[12];
extern UINT4 Fsbgp4SoftReconfigOutboundRequest[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4SoftReconfigOutboundRequest(i4Fsbgp4SoftReconfigOutboundPeerType , pFsbgp4SoftReconfigOutboundPeerAddr ,i4SetValFsbgp4SoftReconfigOutboundRequest) \
 nmhSetCmnWithLock(Fsbgp4SoftReconfigOutboundRequest, 12, Fsbgp4SoftReconfigOutboundRequestSet, BgpLock, BgpUnLock, 1, 0, 2, "%i %s %i", i4Fsbgp4SoftReconfigOutboundPeerType , pFsbgp4SoftReconfigOutboundPeerAddr ,i4SetValFsbgp4SoftReconfigOutboundRequest)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4mpebgpPeerRemoteAddrType[11];
extern UINT4 Fsbgp4mpebgpPeerAdminStatus[11];
extern UINT4 Fsbgp4mpebgpPeerRemoteAddr[11];
extern UINT4 Fsbgp4mpebgpPeerConnectRetryInterval[11];
extern UINT4 Fsbgp4mpebgpPeerHoldTimeConfigured[11];
extern UINT4 Fsbgp4mpebgpPeerKeepAliveConfigured[11];
extern UINT4 Fsbgp4mpebgpPeerMinASOriginationInterval[11];
extern UINT4 Fsbgp4mpebgpPeerMinRouteAdvertisementInterval[11];
extern UINT4 Fsbgp4mpePeerAllowAutomaticStart[11];
extern UINT4 Fsbgp4mpePeerAllowAutomaticStop[11];
extern UINT4 Fsbgp4mpebgpPeerIdleHoldTimeConfigured[11];
extern UINT4 Fsbgp4mpeDampPeerOscillations[11];
extern UINT4 Fsbgp4mpePeerDelayOpen[11];
extern UINT4 Fsbgp4mpebgpPeerDelayOpenTimeConfigured[11];
extern UINT4 Fsbgp4mpePeerPrefixUpperLimit[11];
extern UINT4 Fsbgp4mpePeerTcpConnectRetryCnt[11];
extern UINT4 Fsbgp4mpePeerTCPAOAuthNoMKTDiscard[11];
extern UINT4 Fsbgp4mpePeerTCPAOAuthICMPAccept[11];
extern UINT4 Fsbgp4mpePeerIpPrefixNameIn[11];
extern UINT4 Fsbgp4mpePeerIpPrefixNameOut[11];
extern UINT4 Fsbgp4mpePeerBfdStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4mpebgpPeerAdminStatus(i4Fsbgp4mpebgpPeerRemoteAddrType , pFsbgp4mpebgpPeerRemoteAddr ,i4SetValFsbgp4mpebgpPeerAdminStatus) \
 nmhSetCmnWithLock(Fsbgp4mpebgpPeerAdminStatus, 11, Fsbgp4mpebgpPeerAdminStatusSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %i", i4Fsbgp4mpebgpPeerRemoteAddrType , pFsbgp4mpebgpPeerRemoteAddr ,i4SetValFsbgp4mpebgpPeerAdminStatus)
#define nmhSetFsbgp4mpebgpPeerConnectRetryInterval(i4Fsbgp4mpebgpPeerRemoteAddrType , pFsbgp4mpebgpPeerRemoteAddr ,i4SetValFsbgp4mpebgpPeerConnectRetryInterval) \
 nmhSetCmnWithLock(Fsbgp4mpebgpPeerConnectRetryInterval, 11, Fsbgp4mpebgpPeerConnectRetryIntervalSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %i", i4Fsbgp4mpebgpPeerRemoteAddrType , pFsbgp4mpebgpPeerRemoteAddr ,i4SetValFsbgp4mpebgpPeerConnectRetryInterval)
#define nmhSetFsbgp4mpebgpPeerHoldTimeConfigured(i4Fsbgp4mpebgpPeerRemoteAddrType , pFsbgp4mpebgpPeerRemoteAddr ,i4SetValFsbgp4mpebgpPeerHoldTimeConfigured) \
 nmhSetCmnWithLock(Fsbgp4mpebgpPeerHoldTimeConfigured, 11, Fsbgp4mpebgpPeerHoldTimeConfiguredSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %i", i4Fsbgp4mpebgpPeerRemoteAddrType , pFsbgp4mpebgpPeerRemoteAddr ,i4SetValFsbgp4mpebgpPeerHoldTimeConfigured)
#define nmhSetFsbgp4mpebgpPeerKeepAliveConfigured(i4Fsbgp4mpebgpPeerRemoteAddrType , pFsbgp4mpebgpPeerRemoteAddr ,i4SetValFsbgp4mpebgpPeerKeepAliveConfigured) \
 nmhSetCmnWithLock(Fsbgp4mpebgpPeerKeepAliveConfigured, 11, Fsbgp4mpebgpPeerKeepAliveConfiguredSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %i", i4Fsbgp4mpebgpPeerRemoteAddrType , pFsbgp4mpebgpPeerRemoteAddr ,i4SetValFsbgp4mpebgpPeerKeepAliveConfigured)
#define nmhSetFsbgp4mpebgpPeerMinASOriginationInterval(i4Fsbgp4mpebgpPeerRemoteAddrType , pFsbgp4mpebgpPeerRemoteAddr ,i4SetValFsbgp4mpebgpPeerMinASOriginationInterval) \
 nmhSetCmnWithLock(Fsbgp4mpebgpPeerMinASOriginationInterval, 11, Fsbgp4mpebgpPeerMinASOriginationIntervalSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %i", i4Fsbgp4mpebgpPeerRemoteAddrType , pFsbgp4mpebgpPeerRemoteAddr ,i4SetValFsbgp4mpebgpPeerMinASOriginationInterval)
#define nmhSetFsbgp4mpebgpPeerMinRouteAdvertisementInterval(i4Fsbgp4mpebgpPeerRemoteAddrType , pFsbgp4mpebgpPeerRemoteAddr ,i4SetValFsbgp4mpebgpPeerMinRouteAdvertisementInterval) \
 nmhSetCmnWithLock(Fsbgp4mpebgpPeerMinRouteAdvertisementInterval, 11, Fsbgp4mpebgpPeerMinRouteAdvertisementIntervalSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %i", i4Fsbgp4mpebgpPeerRemoteAddrType , pFsbgp4mpebgpPeerRemoteAddr ,i4SetValFsbgp4mpebgpPeerMinRouteAdvertisementInterval)
#define nmhSetFsbgp4mpePeerAllowAutomaticStart(i4Fsbgp4mpebgpPeerRemoteAddrType , pFsbgp4mpebgpPeerRemoteAddr ,i4SetValFsbgp4mpePeerAllowAutomaticStart) \
 nmhSetCmnWithLock(Fsbgp4mpePeerAllowAutomaticStart, 11, Fsbgp4mpePeerAllowAutomaticStartSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %i", i4Fsbgp4mpebgpPeerRemoteAddrType , pFsbgp4mpebgpPeerRemoteAddr ,i4SetValFsbgp4mpePeerAllowAutomaticStart)
#define nmhSetFsbgp4mpePeerAllowAutomaticStop(i4Fsbgp4mpebgpPeerRemoteAddrType , pFsbgp4mpebgpPeerRemoteAddr ,i4SetValFsbgp4mpePeerAllowAutomaticStop) \
 nmhSetCmnWithLock(Fsbgp4mpePeerAllowAutomaticStop, 11, Fsbgp4mpePeerAllowAutomaticStopSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %i", i4Fsbgp4mpebgpPeerRemoteAddrType , pFsbgp4mpebgpPeerRemoteAddr ,i4SetValFsbgp4mpePeerAllowAutomaticStop)
#define nmhSetFsbgp4mpebgpPeerIdleHoldTimeConfigured(i4Fsbgp4mpebgpPeerRemoteAddrType , pFsbgp4mpebgpPeerRemoteAddr ,i4SetValFsbgp4mpebgpPeerIdleHoldTimeConfigured) \
 nmhSetCmnWithLock(Fsbgp4mpebgpPeerIdleHoldTimeConfigured, 11, Fsbgp4mpebgpPeerIdleHoldTimeConfiguredSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %i", i4Fsbgp4mpebgpPeerRemoteAddrType , pFsbgp4mpebgpPeerRemoteAddr ,i4SetValFsbgp4mpebgpPeerIdleHoldTimeConfigured)
#define nmhSetFsbgp4mpeDampPeerOscillations(i4Fsbgp4mpebgpPeerRemoteAddrType , pFsbgp4mpebgpPeerRemoteAddr ,i4SetValFsbgp4mpeDampPeerOscillations) \
 nmhSetCmnWithLock(Fsbgp4mpeDampPeerOscillations, 11, Fsbgp4mpeDampPeerOscillationsSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %i", i4Fsbgp4mpebgpPeerRemoteAddrType , pFsbgp4mpebgpPeerRemoteAddr ,i4SetValFsbgp4mpeDampPeerOscillations)
#define nmhSetFsbgp4mpePeerDelayOpen(i4Fsbgp4mpebgpPeerRemoteAddrType , pFsbgp4mpebgpPeerRemoteAddr ,i4SetValFsbgp4mpePeerDelayOpen) \
 nmhSetCmnWithLock(Fsbgp4mpePeerDelayOpen, 11, Fsbgp4mpePeerDelayOpenSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %i", i4Fsbgp4mpebgpPeerRemoteAddrType , pFsbgp4mpebgpPeerRemoteAddr ,i4SetValFsbgp4mpePeerDelayOpen)
#define nmhSetFsbgp4mpebgpPeerDelayOpenTimeConfigured(i4Fsbgp4mpebgpPeerRemoteAddrType , pFsbgp4mpebgpPeerRemoteAddr ,i4SetValFsbgp4mpebgpPeerDelayOpenTimeConfigured) \
 nmhSetCmnWithLock(Fsbgp4mpebgpPeerDelayOpenTimeConfigured, 11, Fsbgp4mpebgpPeerDelayOpenTimeConfiguredSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %i", i4Fsbgp4mpebgpPeerRemoteAddrType , pFsbgp4mpebgpPeerRemoteAddr ,i4SetValFsbgp4mpebgpPeerDelayOpenTimeConfigured)
#define nmhSetFsbgp4mpePeerPrefixUpperLimit(i4Fsbgp4mpebgpPeerRemoteAddrType , pFsbgp4mpebgpPeerRemoteAddr ,i4SetValFsbgp4mpePeerPrefixUpperLimit) \
 nmhSetCmnWithLock(Fsbgp4mpePeerPrefixUpperLimit, 11, Fsbgp4mpePeerPrefixUpperLimitSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %i", i4Fsbgp4mpebgpPeerRemoteAddrType , pFsbgp4mpebgpPeerRemoteAddr ,i4SetValFsbgp4mpePeerPrefixUpperLimit)
#define nmhSetFsbgp4mpePeerTcpConnectRetryCnt(i4Fsbgp4mpebgpPeerRemoteAddrType , pFsbgp4mpebgpPeerRemoteAddr ,i4SetValFsbgp4mpePeerTcpConnectRetryCnt) \
 nmhSetCmnWithLock(Fsbgp4mpePeerTcpConnectRetryCnt, 11, Fsbgp4mpePeerTcpConnectRetryCntSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %i", i4Fsbgp4mpebgpPeerRemoteAddrType , pFsbgp4mpebgpPeerRemoteAddr ,i4SetValFsbgp4mpePeerTcpConnectRetryCnt)
#define nmhSetFsbgp4mpePeerTCPAOAuthNoMKTDiscard(i4Fsbgp4mpebgpPeerRemoteAddrType , pFsbgp4mpebgpPeerRemoteAddr ,i4SetValFsbgp4mpePeerTCPAOAuthNoMKTDiscard) \
 nmhSetCmnWithLock(Fsbgp4mpePeerTCPAOAuthNoMKTDiscard, 11, Fsbgp4mpePeerTCPAOAuthNoMKTDiscardSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %i", i4Fsbgp4mpebgpPeerRemoteAddrType , pFsbgp4mpebgpPeerRemoteAddr ,i4SetValFsbgp4mpePeerTCPAOAuthNoMKTDiscard)
#define nmhSetFsbgp4mpePeerTCPAOAuthICMPAccept(i4Fsbgp4mpebgpPeerRemoteAddrType , pFsbgp4mpebgpPeerRemoteAddr ,i4SetValFsbgp4mpePeerTCPAOAuthICMPAccept) \
 nmhSetCmnWithLock(Fsbgp4mpePeerTCPAOAuthICMPAccept, 11, Fsbgp4mpePeerTCPAOAuthICMPAcceptSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %i", i4Fsbgp4mpebgpPeerRemoteAddrType , pFsbgp4mpebgpPeerRemoteAddr ,i4SetValFsbgp4mpePeerTCPAOAuthICMPAccept)
#define nmhSetFsbgp4mpePeerIpPrefixNameIn(i4Fsbgp4mpebgpPeerRemoteAddrType , pFsbgp4mpebgpPeerRemoteAddr ,pSetValFsbgp4mpePeerIpPrefixNameIn) \
 nmhSetCmnWithLock(Fsbgp4mpePeerIpPrefixNameIn, 11, Fsbgp4mpePeerIpPrefixNameInSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %s", i4Fsbgp4mpebgpPeerRemoteAddrType , pFsbgp4mpebgpPeerRemoteAddr ,pSetValFsbgp4mpePeerIpPrefixNameIn)
#define nmhSetFsbgp4mpePeerIpPrefixNameOut(i4Fsbgp4mpebgpPeerRemoteAddrType , pFsbgp4mpebgpPeerRemoteAddr ,pSetValFsbgp4mpePeerIpPrefixNameOut) \
 nmhSetCmnWithLock(Fsbgp4mpePeerIpPrefixNameOut, 11, Fsbgp4mpePeerIpPrefixNameOutSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %s", i4Fsbgp4mpebgpPeerRemoteAddrType , pFsbgp4mpebgpPeerRemoteAddr ,pSetValFsbgp4mpePeerIpPrefixNameOut)
#define nmhSetFsbgp4mpePeerBfdStatus(i4Fsbgp4mpebgpPeerRemoteAddrType , pFsbgp4mpebgpPeerRemoteAddr ,i4SetValFsbgp4mpePeerBfdStatus) \
 nmhSetCmnWithLock(Fsbgp4mpePeerBfdStatus, 11, Fsbgp4mpePeerBfdStatusSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %i", i4Fsbgp4mpebgpPeerRemoteAddrType , pFsbgp4mpebgpPeerRemoteAddr ,i4SetValFsbgp4mpePeerBfdStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4mpePeerExtPeerType[11];
extern UINT4 Fsbgp4mpePeerExtPeerRemoteAddr[11];
extern UINT4 Fsbgp4mpePeerExtConfigurePeer[11];
extern UINT4 Fsbgp4mpePeerExtPeerRemoteAs[11];
extern UINT4 Fsbgp4mpePeerExtEBGPMultiHop[11];
extern UINT4 Fsbgp4mpePeerExtEBGPHopLimit[11];
extern UINT4 Fsbgp4mpePeerExtNextHopSelf[11];
extern UINT4 Fsbgp4mpePeerExtRflClient[11];
extern UINT4 Fsbgp4mpePeerExtTcpSendBufSize[11];
extern UINT4 Fsbgp4mpePeerExtTcpRcvBufSize[11];
extern UINT4 Fsbgp4mpePeerExtLclAddress[11];
extern UINT4 Fsbgp4mpePeerExtNetworkAddress[11];
extern UINT4 Fsbgp4mpePeerExtGateway[11];
extern UINT4 Fsbgp4mpePeerExtCommSendStatus[11];
extern UINT4 Fsbgp4mpePeerExtECommSendStatus[11];
extern UINT4 Fsbgp4mpePeerExtPassive[11];
extern UINT4 Fsbgp4mpePeerExtDefaultOriginate[11];
extern UINT4 Fsbgp4mpePeerExtActivateMPCapability[11];
extern UINT4 Fsbgp4mpePeerExtDeactivateMPCapability[11];
extern UINT4 Fsbgp4mpePeerExtMplsVpnVrfAssociated[11];
extern UINT4 Fsbgp4mpePeerExtMplsVpnCERouteTargetAdvt[11];
extern UINT4 Fsbgp4mpePeerExtMplsVpnCESiteOfOrigin[11];
extern UINT4 Fsbgp4mpePeerExtOverrideCapability[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4mpePeerExtConfigurePeer(i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,i4SetValFsbgp4mpePeerExtConfigurePeer) \
 nmhSetCmnWithLock(Fsbgp4mpePeerExtConfigurePeer, 11, Fsbgp4mpePeerExtConfigurePeerSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %i", i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,i4SetValFsbgp4mpePeerExtConfigurePeer)
#define nmhSetFsbgp4mpePeerExtPeerRemoteAs(i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,u4SetValFsbgp4mpePeerExtPeerRemoteAs) \
 nmhSetCmnWithLock(Fsbgp4mpePeerExtPeerRemoteAs, 11, Fsbgp4mpePeerExtPeerRemoteAsSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %u", i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,u4SetValFsbgp4mpePeerExtPeerRemoteAs)
#define nmhSetFsbgp4mpePeerExtEBGPMultiHop(i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,i4SetValFsbgp4mpePeerExtEBGPMultiHop) \
 nmhSetCmnWithLock(Fsbgp4mpePeerExtEBGPMultiHop, 11, Fsbgp4mpePeerExtEBGPMultiHopSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %i", i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,i4SetValFsbgp4mpePeerExtEBGPMultiHop)
#define nmhSetFsbgp4mpePeerExtEBGPHopLimit(i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,i4SetValFsbgp4mpePeerExtEBGPHopLimit) \
 nmhSetCmnWithLock(Fsbgp4mpePeerExtEBGPHopLimit, 11, Fsbgp4mpePeerExtEBGPHopLimitSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %i", i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,i4SetValFsbgp4mpePeerExtEBGPHopLimit)
#define nmhSetFsbgp4mpePeerExtNextHopSelf(i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,i4SetValFsbgp4mpePeerExtNextHopSelf) \
 nmhSetCmnWithLock(Fsbgp4mpePeerExtNextHopSelf, 11, Fsbgp4mpePeerExtNextHopSelfSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %i", i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,i4SetValFsbgp4mpePeerExtNextHopSelf)
#define nmhSetFsbgp4mpePeerExtRflClient(i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,i4SetValFsbgp4mpePeerExtRflClient) \
 nmhSetCmnWithLock(Fsbgp4mpePeerExtRflClient, 11, Fsbgp4mpePeerExtRflClientSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %i", i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,i4SetValFsbgp4mpePeerExtRflClient)
#define nmhSetFsbgp4mpePeerExtTcpSendBufSize(i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,i4SetValFsbgp4mpePeerExtTcpSendBufSize) \
 nmhSetCmnWithLock(Fsbgp4mpePeerExtTcpSendBufSize, 11, Fsbgp4mpePeerExtTcpSendBufSizeSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %i", i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,i4SetValFsbgp4mpePeerExtTcpSendBufSize)
#define nmhSetFsbgp4mpePeerExtTcpRcvBufSize(i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,i4SetValFsbgp4mpePeerExtTcpRcvBufSize) \
 nmhSetCmnWithLock(Fsbgp4mpePeerExtTcpRcvBufSize, 11, Fsbgp4mpePeerExtTcpRcvBufSizeSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %i", i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,i4SetValFsbgp4mpePeerExtTcpRcvBufSize)
#define nmhSetFsbgp4mpePeerExtLclAddress(i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,pSetValFsbgp4mpePeerExtLclAddress) \
 nmhSetCmnWithLock(Fsbgp4mpePeerExtLclAddress, 11, Fsbgp4mpePeerExtLclAddressSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %s", i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,pSetValFsbgp4mpePeerExtLclAddress)
#define nmhSetFsbgp4mpePeerExtNetworkAddress(i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,pSetValFsbgp4mpePeerExtNetworkAddress) \
 nmhSetCmnWithLock(Fsbgp4mpePeerExtNetworkAddress, 11, Fsbgp4mpePeerExtNetworkAddressSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %s", i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,pSetValFsbgp4mpePeerExtNetworkAddress)
#define nmhSetFsbgp4mpePeerExtGateway(i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,pSetValFsbgp4mpePeerExtGateway) \
 nmhSetCmnWithLock(Fsbgp4mpePeerExtGateway, 11, Fsbgp4mpePeerExtGatewaySet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %s", i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,pSetValFsbgp4mpePeerExtGateway)
#define nmhSetFsbgp4mpePeerExtCommSendStatus(i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,i4SetValFsbgp4mpePeerExtCommSendStatus) \
 nmhSetCmnWithLock(Fsbgp4mpePeerExtCommSendStatus, 11, Fsbgp4mpePeerExtCommSendStatusSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %i", i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,i4SetValFsbgp4mpePeerExtCommSendStatus)
#define nmhSetFsbgp4mpePeerExtECommSendStatus(i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,i4SetValFsbgp4mpePeerExtECommSendStatus) \
 nmhSetCmnWithLock(Fsbgp4mpePeerExtECommSendStatus, 11, Fsbgp4mpePeerExtECommSendStatusSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %i", i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,i4SetValFsbgp4mpePeerExtECommSendStatus)
#define nmhSetFsbgp4mpePeerExtPassive(i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,i4SetValFsbgp4mpePeerExtPassive) \
 nmhSetCmnWithLock(Fsbgp4mpePeerExtPassive, 11, Fsbgp4mpePeerExtPassiveSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %i", i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,i4SetValFsbgp4mpePeerExtPassive)
#define nmhSetFsbgp4mpePeerExtDefaultOriginate(i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,i4SetValFsbgp4mpePeerExtDefaultOriginate) \
 nmhSetCmnWithLock(Fsbgp4mpePeerExtDefaultOriginate, 11, Fsbgp4mpePeerExtDefaultOriginateSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %i", i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,i4SetValFsbgp4mpePeerExtDefaultOriginate)
#define nmhSetFsbgp4mpePeerExtActivateMPCapability(i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,i4SetValFsbgp4mpePeerExtActivateMPCapability) \
 nmhSetCmnWithLock(Fsbgp4mpePeerExtActivateMPCapability, 11, Fsbgp4mpePeerExtActivateMPCapabilitySet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %i", i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,i4SetValFsbgp4mpePeerExtActivateMPCapability)
#define nmhSetFsbgp4mpePeerExtDeactivateMPCapability(i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,i4SetValFsbgp4mpePeerExtDeactivateMPCapability) \
 nmhSetCmnWithLock(Fsbgp4mpePeerExtDeactivateMPCapability, 11, Fsbgp4mpePeerExtDeactivateMPCapabilitySet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %i", i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,i4SetValFsbgp4mpePeerExtDeactivateMPCapability)
#define nmhSetFsbgp4mpePeerExtMplsVpnVrfAssociated(i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,pSetValFsbgp4mpePeerExtMplsVpnVrfAssociated) \
 nmhSetCmnWithLock(Fsbgp4mpePeerExtMplsVpnVrfAssociated, 11, Fsbgp4mpePeerExtMplsVpnVrfAssociatedSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %s", i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,pSetValFsbgp4mpePeerExtMplsVpnVrfAssociated)
#define nmhSetFsbgp4mpePeerExtMplsVpnCERouteTargetAdvt(i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,i4SetValFsbgp4mpePeerExtMplsVpnCERouteTargetAdvt) \
 nmhSetCmnWithLock(Fsbgp4mpePeerExtMplsVpnCERouteTargetAdvt, 11, Fsbgp4mpePeerExtMplsVpnCERouteTargetAdvtSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %i", i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,i4SetValFsbgp4mpePeerExtMplsVpnCERouteTargetAdvt)
#define nmhSetFsbgp4mpePeerExtMplsVpnCESiteOfOrigin(i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,pSetValFsbgp4mpePeerExtMplsVpnCESiteOfOrigin) \
 nmhSetCmnWithLock(Fsbgp4mpePeerExtMplsVpnCESiteOfOrigin, 11, Fsbgp4mpePeerExtMplsVpnCESiteOfOriginSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %s", i4Fsbgp4mpePeerExtPeerType , pFsbgp4mpePeerExtPeerRemoteAddr ,pSetValFsbgp4mpePeerExtMplsVpnCESiteOfOrigin)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4mpeMEDIndex[11];
extern UINT4 Fsbgp4mpeMEDAdminStatus[11];
extern UINT4 Fsbgp4mpeMEDRemoteAS[11];
extern UINT4 Fsbgp4mpeMEDIPAddrAfi[11];
extern UINT4 Fsbgp4mpeMEDIPAddrSafi[11];
extern UINT4 Fsbgp4mpeMEDIPAddrPrefix[11];
extern UINT4 Fsbgp4mpeMEDIPAddrPrefixLen[11];
extern UINT4 Fsbgp4mpeMEDIntermediateAS[11];
extern UINT4 Fsbgp4mpeMEDDirection[11];
extern UINT4 Fsbgp4mpeMEDValue[11];
extern UINT4 Fsbgp4mpeMEDPreference[11];
extern UINT4 Fsbgp4mpeMEDVrfName[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4mpeMEDAdminStatus(i4Fsbgp4mpeMEDIndex ,i4SetValFsbgp4mpeMEDAdminStatus) \
 nmhSetCmnWithLock(Fsbgp4mpeMEDAdminStatus, 11, Fsbgp4mpeMEDAdminStatusSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %i", i4Fsbgp4mpeMEDIndex ,i4SetValFsbgp4mpeMEDAdminStatus)
#define nmhSetFsbgp4mpeMEDRemoteAS(i4Fsbgp4mpeMEDIndex ,u4SetValFsbgp4mpeMEDRemoteAS) \
 nmhSetCmnWithLock(Fsbgp4mpeMEDRemoteAS, 11, Fsbgp4mpeMEDRemoteASSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %u", i4Fsbgp4mpeMEDIndex ,u4SetValFsbgp4mpeMEDRemoteAS)
#define nmhSetFsbgp4mpeMEDIPAddrAfi(i4Fsbgp4mpeMEDIndex ,i4SetValFsbgp4mpeMEDIPAddrAfi) \
 nmhSetCmnWithLock(Fsbgp4mpeMEDIPAddrAfi, 11, Fsbgp4mpeMEDIPAddrAfiSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %i", i4Fsbgp4mpeMEDIndex ,i4SetValFsbgp4mpeMEDIPAddrAfi)
#define nmhSetFsbgp4mpeMEDIPAddrSafi(i4Fsbgp4mpeMEDIndex ,i4SetValFsbgp4mpeMEDIPAddrSafi) \
 nmhSetCmnWithLock(Fsbgp4mpeMEDIPAddrSafi, 11, Fsbgp4mpeMEDIPAddrSafiSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %i", i4Fsbgp4mpeMEDIndex ,i4SetValFsbgp4mpeMEDIPAddrSafi)
#define nmhSetFsbgp4mpeMEDIPAddrPrefix(i4Fsbgp4mpeMEDIndex ,pSetValFsbgp4mpeMEDIPAddrPrefix) \
 nmhSetCmnWithLock(Fsbgp4mpeMEDIPAddrPrefix, 11, Fsbgp4mpeMEDIPAddrPrefixSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %s", i4Fsbgp4mpeMEDIndex ,pSetValFsbgp4mpeMEDIPAddrPrefix)
#define nmhSetFsbgp4mpeMEDIPAddrPrefixLen(i4Fsbgp4mpeMEDIndex ,i4SetValFsbgp4mpeMEDIPAddrPrefixLen) \
 nmhSetCmnWithLock(Fsbgp4mpeMEDIPAddrPrefixLen, 11, Fsbgp4mpeMEDIPAddrPrefixLenSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %i", i4Fsbgp4mpeMEDIndex ,i4SetValFsbgp4mpeMEDIPAddrPrefixLen)
#define nmhSetFsbgp4mpeMEDIntermediateAS(i4Fsbgp4mpeMEDIndex ,pSetValFsbgp4mpeMEDIntermediateAS) \
 nmhSetCmnWithLock(Fsbgp4mpeMEDIntermediateAS, 11, Fsbgp4mpeMEDIntermediateASSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %s", i4Fsbgp4mpeMEDIndex ,pSetValFsbgp4mpeMEDIntermediateAS)
#define nmhSetFsbgp4mpeMEDDirection(i4Fsbgp4mpeMEDIndex ,i4SetValFsbgp4mpeMEDDirection) \
 nmhSetCmnWithLock(Fsbgp4mpeMEDDirection, 11, Fsbgp4mpeMEDDirectionSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %i", i4Fsbgp4mpeMEDIndex ,i4SetValFsbgp4mpeMEDDirection)
#define nmhSetFsbgp4mpeMEDValue(i4Fsbgp4mpeMEDIndex ,u4SetValFsbgp4mpeMEDValue) \
 nmhSetCmnWithLock(Fsbgp4mpeMEDValue, 11, Fsbgp4mpeMEDValueSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %u", i4Fsbgp4mpeMEDIndex ,u4SetValFsbgp4mpeMEDValue)
#define nmhSetFsbgp4mpeMEDPreference(i4Fsbgp4mpeMEDIndex ,i4SetValFsbgp4mpeMEDPreference) \
 nmhSetCmnWithLock(Fsbgp4mpeMEDPreference, 11, Fsbgp4mpeMEDPreferenceSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %i", i4Fsbgp4mpeMEDIndex ,i4SetValFsbgp4mpeMEDPreference)
#define nmhSetFsbgp4mpeMEDVrfName(i4Fsbgp4mpeMEDIndex ,pSetValFsbgp4mpeMEDVrfName) \
 nmhSetCmnWithLock(Fsbgp4mpeMEDVrfName, 11, Fsbgp4mpeMEDVrfNameSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %s", i4Fsbgp4mpeMEDIndex ,pSetValFsbgp4mpeMEDVrfName)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4mpeLocalPrefIndex[11];
extern UINT4 Fsbgp4mpeLocalPrefAdminStatus[11];
extern UINT4 Fsbgp4mpeLocalPrefRemoteAS[11];
extern UINT4 Fsbgp4mpeLocalPrefIPAddrAfi[11];
extern UINT4 Fsbgp4mpeLocalPrefIPAddrSafi[11];
extern UINT4 Fsbgp4mpeLocalPrefIPAddrPrefix[11];
extern UINT4 Fsbgp4mpeLocalPrefIPAddrPrefixLen[11];
extern UINT4 Fsbgp4mpeLocalPrefIntermediateAS[11];
extern UINT4 Fsbgp4mpeLocalPrefDirection[11];
extern UINT4 Fsbgp4mpeLocalPrefValue[11];
extern UINT4 Fsbgp4mpeLocalPrefPreference[11];
extern UINT4 Fsbgp4mpeLocalPrefVrfName[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4mpeLocalPrefAdminStatus(i4Fsbgp4mpeLocalPrefIndex ,i4SetValFsbgp4mpeLocalPrefAdminStatus) \
 nmhSetCmnWithLock(Fsbgp4mpeLocalPrefAdminStatus, 11, Fsbgp4mpeLocalPrefAdminStatusSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %i", i4Fsbgp4mpeLocalPrefIndex ,i4SetValFsbgp4mpeLocalPrefAdminStatus)
#define nmhSetFsbgp4mpeLocalPrefRemoteAS(i4Fsbgp4mpeLocalPrefIndex ,u4SetValFsbgp4mpeLocalPrefRemoteAS) \
 nmhSetCmnWithLock(Fsbgp4mpeLocalPrefRemoteAS, 11, Fsbgp4mpeLocalPrefRemoteASSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %u", i4Fsbgp4mpeLocalPrefIndex ,u4SetValFsbgp4mpeLocalPrefRemoteAS)
#define nmhSetFsbgp4mpeLocalPrefIPAddrAfi(i4Fsbgp4mpeLocalPrefIndex ,i4SetValFsbgp4mpeLocalPrefIPAddrAfi) \
 nmhSetCmnWithLock(Fsbgp4mpeLocalPrefIPAddrAfi, 11, Fsbgp4mpeLocalPrefIPAddrAfiSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %i", i4Fsbgp4mpeLocalPrefIndex ,i4SetValFsbgp4mpeLocalPrefIPAddrAfi)
#define nmhSetFsbgp4mpeLocalPrefIPAddrSafi(i4Fsbgp4mpeLocalPrefIndex ,i4SetValFsbgp4mpeLocalPrefIPAddrSafi) \
 nmhSetCmnWithLock(Fsbgp4mpeLocalPrefIPAddrSafi, 11, Fsbgp4mpeLocalPrefIPAddrSafiSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %i", i4Fsbgp4mpeLocalPrefIndex ,i4SetValFsbgp4mpeLocalPrefIPAddrSafi)
#define nmhSetFsbgp4mpeLocalPrefIPAddrPrefix(i4Fsbgp4mpeLocalPrefIndex ,pSetValFsbgp4mpeLocalPrefIPAddrPrefix) \
 nmhSetCmnWithLock(Fsbgp4mpeLocalPrefIPAddrPrefix, 11, Fsbgp4mpeLocalPrefIPAddrPrefixSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %s", i4Fsbgp4mpeLocalPrefIndex ,pSetValFsbgp4mpeLocalPrefIPAddrPrefix)
#define nmhSetFsbgp4mpeLocalPrefIPAddrPrefixLen(i4Fsbgp4mpeLocalPrefIndex ,i4SetValFsbgp4mpeLocalPrefIPAddrPrefixLen) \
 nmhSetCmnWithLock(Fsbgp4mpeLocalPrefIPAddrPrefixLen, 11, Fsbgp4mpeLocalPrefIPAddrPrefixLenSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %i", i4Fsbgp4mpeLocalPrefIndex ,i4SetValFsbgp4mpeLocalPrefIPAddrPrefixLen)
#define nmhSetFsbgp4mpeLocalPrefIntermediateAS(i4Fsbgp4mpeLocalPrefIndex ,pSetValFsbgp4mpeLocalPrefIntermediateAS) \
 nmhSetCmnWithLock(Fsbgp4mpeLocalPrefIntermediateAS, 11, Fsbgp4mpeLocalPrefIntermediateASSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %s", i4Fsbgp4mpeLocalPrefIndex ,pSetValFsbgp4mpeLocalPrefIntermediateAS)
#define nmhSetFsbgp4mpeLocalPrefDirection(i4Fsbgp4mpeLocalPrefIndex ,i4SetValFsbgp4mpeLocalPrefDirection) \
 nmhSetCmnWithLock(Fsbgp4mpeLocalPrefDirection, 11, Fsbgp4mpeLocalPrefDirectionSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %i", i4Fsbgp4mpeLocalPrefIndex ,i4SetValFsbgp4mpeLocalPrefDirection)
#define nmhSetFsbgp4mpeLocalPrefValue(i4Fsbgp4mpeLocalPrefIndex ,u4SetValFsbgp4mpeLocalPrefValue) \
 nmhSetCmnWithLock(Fsbgp4mpeLocalPrefValue, 11, Fsbgp4mpeLocalPrefValueSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %u", i4Fsbgp4mpeLocalPrefIndex ,u4SetValFsbgp4mpeLocalPrefValue)
#define nmhSetFsbgp4mpeLocalPrefPreference(i4Fsbgp4mpeLocalPrefIndex ,i4SetValFsbgp4mpeLocalPrefPreference) \
 nmhSetCmnWithLock(Fsbgp4mpeLocalPrefPreference, 11, Fsbgp4mpeLocalPrefPreferenceSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %i", i4Fsbgp4mpeLocalPrefIndex ,i4SetValFsbgp4mpeLocalPrefPreference)
#define nmhSetFsbgp4mpeLocalPrefVrfName(i4Fsbgp4mpeLocalPrefIndex ,pSetValFsbgp4mpeLocalPrefVrfName) \
 nmhSetCmnWithLock(Fsbgp4mpeLocalPrefVrfName, 11, Fsbgp4mpeLocalPrefVrfNameSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %s", i4Fsbgp4mpeLocalPrefIndex ,pSetValFsbgp4mpeLocalPrefVrfName)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4mpeUpdateFilterIndex[11];
extern UINT4 Fsbgp4mpeUpdateFilterAdminStatus[11];
extern UINT4 Fsbgp4mpeUpdateFilterRemoteAS[11];
extern UINT4 Fsbgp4mpeUpdateFilterIPAddrAfi[11];
extern UINT4 Fsbgp4mpeUpdateFilterIPAddrSafi[11];
extern UINT4 Fsbgp4mpeUpdateFilterIPAddrPrefix[11];
extern UINT4 Fsbgp4mpeUpdateFilterIPAddrPrefixLen[11];
extern UINT4 Fsbgp4mpeUpdateFilterIntermediateAS[11];
extern UINT4 Fsbgp4mpeUpdateFilterDirection[11];
extern UINT4 Fsbgp4mpeUpdateFilterAction[11];
extern UINT4 Fsbgp4mpeUpdateFilterVrfName[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4mpeUpdateFilterAdminStatus(i4Fsbgp4mpeUpdateFilterIndex ,i4SetValFsbgp4mpeUpdateFilterAdminStatus) \
 nmhSetCmnWithLock(Fsbgp4mpeUpdateFilterAdminStatus, 11, Fsbgp4mpeUpdateFilterAdminStatusSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %i", i4Fsbgp4mpeUpdateFilterIndex ,i4SetValFsbgp4mpeUpdateFilterAdminStatus)
#define nmhSetFsbgp4mpeUpdateFilterRemoteAS(i4Fsbgp4mpeUpdateFilterIndex ,u4SetValFsbgp4mpeUpdateFilterRemoteAS) \
 nmhSetCmnWithLock(Fsbgp4mpeUpdateFilterRemoteAS, 11, Fsbgp4mpeUpdateFilterRemoteASSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %u", i4Fsbgp4mpeUpdateFilterIndex ,u4SetValFsbgp4mpeUpdateFilterRemoteAS)
#define nmhSetFsbgp4mpeUpdateFilterIPAddrAfi(i4Fsbgp4mpeUpdateFilterIndex ,i4SetValFsbgp4mpeUpdateFilterIPAddrAfi) \
 nmhSetCmnWithLock(Fsbgp4mpeUpdateFilterIPAddrAfi, 11, Fsbgp4mpeUpdateFilterIPAddrAfiSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %i", i4Fsbgp4mpeUpdateFilterIndex ,i4SetValFsbgp4mpeUpdateFilterIPAddrAfi)
#define nmhSetFsbgp4mpeUpdateFilterIPAddrSafi(i4Fsbgp4mpeUpdateFilterIndex ,i4SetValFsbgp4mpeUpdateFilterIPAddrSafi) \
 nmhSetCmnWithLock(Fsbgp4mpeUpdateFilterIPAddrSafi, 11, Fsbgp4mpeUpdateFilterIPAddrSafiSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %i", i4Fsbgp4mpeUpdateFilterIndex ,i4SetValFsbgp4mpeUpdateFilterIPAddrSafi)
#define nmhSetFsbgp4mpeUpdateFilterIPAddrPrefix(i4Fsbgp4mpeUpdateFilterIndex ,pSetValFsbgp4mpeUpdateFilterIPAddrPrefix) \
 nmhSetCmnWithLock(Fsbgp4mpeUpdateFilterIPAddrPrefix, 11, Fsbgp4mpeUpdateFilterIPAddrPrefixSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %s", i4Fsbgp4mpeUpdateFilterIndex ,pSetValFsbgp4mpeUpdateFilterIPAddrPrefix)
#define nmhSetFsbgp4mpeUpdateFilterIPAddrPrefixLen(i4Fsbgp4mpeUpdateFilterIndex ,i4SetValFsbgp4mpeUpdateFilterIPAddrPrefixLen) \
 nmhSetCmnWithLock(Fsbgp4mpeUpdateFilterIPAddrPrefixLen, 11, Fsbgp4mpeUpdateFilterIPAddrPrefixLenSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %i", i4Fsbgp4mpeUpdateFilterIndex ,i4SetValFsbgp4mpeUpdateFilterIPAddrPrefixLen)
#define nmhSetFsbgp4mpeUpdateFilterIntermediateAS(i4Fsbgp4mpeUpdateFilterIndex ,pSetValFsbgp4mpeUpdateFilterIntermediateAS) \
 nmhSetCmnWithLock(Fsbgp4mpeUpdateFilterIntermediateAS, 11, Fsbgp4mpeUpdateFilterIntermediateASSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %s", i4Fsbgp4mpeUpdateFilterIndex ,pSetValFsbgp4mpeUpdateFilterIntermediateAS)
#define nmhSetFsbgp4mpeUpdateFilterDirection(i4Fsbgp4mpeUpdateFilterIndex ,i4SetValFsbgp4mpeUpdateFilterDirection) \
 nmhSetCmnWithLock(Fsbgp4mpeUpdateFilterDirection, 11, Fsbgp4mpeUpdateFilterDirectionSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %i", i4Fsbgp4mpeUpdateFilterIndex ,i4SetValFsbgp4mpeUpdateFilterDirection)
#define nmhSetFsbgp4mpeUpdateFilterAction(i4Fsbgp4mpeUpdateFilterIndex ,i4SetValFsbgp4mpeUpdateFilterAction) \
 nmhSetCmnWithLock(Fsbgp4mpeUpdateFilterAction, 11, Fsbgp4mpeUpdateFilterActionSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %i", i4Fsbgp4mpeUpdateFilterIndex ,i4SetValFsbgp4mpeUpdateFilterAction)
#define nmhSetFsbgp4mpeUpdateFilterVrfName(i4Fsbgp4mpeUpdateFilterIndex ,pSetValFsbgp4mpeUpdateFilterVrfName) \
 nmhSetCmnWithLock(Fsbgp4mpeUpdateFilterVrfName, 11, Fsbgp4mpeUpdateFilterVrfNameSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %s", i4Fsbgp4mpeUpdateFilterIndex ,pSetValFsbgp4mpeUpdateFilterVrfName)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4mpeAggregateIndex[11];
extern UINT4 Fsbgp4mpeAggregateAdminStatus[11];
extern UINT4 Fsbgp4mpeAggregateIPAddrAfi[11];
extern UINT4 Fsbgp4mpeAggregateIPAddrSafi[11];
extern UINT4 Fsbgp4mpeAggregateIPAddrPrefix[11];
extern UINT4 Fsbgp4mpeAggregateIPAddrPrefixLen[11];
extern UINT4 Fsbgp4mpeAggregateAdvertise[11];
extern UINT4 Fsbgp4mpeAggregateVrfName[11];
extern UINT4 Fsbgp4mpeAggregateAsSet[11];
extern UINT4 Fsbgp4mpeAggregateAdvertiseRouteMapName[11];
extern UINT4 Fsbgp4mpeAggregateSuppressRouteMapName[11];
extern UINT4 Fsbgp4mpeAggregateAttributeRouteMapName[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4mpeAggregateAdminStatus(i4Fsbgp4mpeAggregateIndex ,i4SetValFsbgp4mpeAggregateAdminStatus) \
 nmhSetCmnWithLock(Fsbgp4mpeAggregateAdminStatus, 11, Fsbgp4mpeAggregateAdminStatusSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %i", i4Fsbgp4mpeAggregateIndex ,i4SetValFsbgp4mpeAggregateAdminStatus)
#define nmhSetFsbgp4mpeAggregateIPAddrAfi(i4Fsbgp4mpeAggregateIndex ,i4SetValFsbgp4mpeAggregateIPAddrAfi) \
 nmhSetCmnWithLock(Fsbgp4mpeAggregateIPAddrAfi, 11, Fsbgp4mpeAggregateIPAddrAfiSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %i", i4Fsbgp4mpeAggregateIndex ,i4SetValFsbgp4mpeAggregateIPAddrAfi)
#define nmhSetFsbgp4mpeAggregateIPAddrSafi(i4Fsbgp4mpeAggregateIndex ,i4SetValFsbgp4mpeAggregateIPAddrSafi) \
 nmhSetCmnWithLock(Fsbgp4mpeAggregateIPAddrSafi, 11, Fsbgp4mpeAggregateIPAddrSafiSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %i", i4Fsbgp4mpeAggregateIndex ,i4SetValFsbgp4mpeAggregateIPAddrSafi)
#define nmhSetFsbgp4mpeAggregateIPAddrPrefix(i4Fsbgp4mpeAggregateIndex ,pSetValFsbgp4mpeAggregateIPAddrPrefix) \
 nmhSetCmnWithLock(Fsbgp4mpeAggregateIPAddrPrefix, 11, Fsbgp4mpeAggregateIPAddrPrefixSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %s", i4Fsbgp4mpeAggregateIndex ,pSetValFsbgp4mpeAggregateIPAddrPrefix)
#define nmhSetFsbgp4mpeAggregateIPAddrPrefixLen(i4Fsbgp4mpeAggregateIndex ,i4SetValFsbgp4mpeAggregateIPAddrPrefixLen) \
 nmhSetCmnWithLock(Fsbgp4mpeAggregateIPAddrPrefixLen, 11, Fsbgp4mpeAggregateIPAddrPrefixLenSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %i", i4Fsbgp4mpeAggregateIndex ,i4SetValFsbgp4mpeAggregateIPAddrPrefixLen)
#define nmhSetFsbgp4mpeAggregateAdvertise(i4Fsbgp4mpeAggregateIndex ,i4SetValFsbgp4mpeAggregateAdvertise) \
 nmhSetCmnWithLock(Fsbgp4mpeAggregateAdvertise, 11, Fsbgp4mpeAggregateAdvertiseSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %i", i4Fsbgp4mpeAggregateIndex ,i4SetValFsbgp4mpeAggregateAdvertise)
#define nmhSetFsbgp4mpeAggregateVrfName(i4Fsbgp4mpeAggregateIndex ,pSetValFsbgp4mpeAggregateVrfName) \
 nmhSetCmnWithLock(Fsbgp4mpeAggregateVrfName, 11, Fsbgp4mpeAggregateVrfNameSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %s", i4Fsbgp4mpeAggregateIndex ,pSetValFsbgp4mpeAggregateVrfName)
#define nmhSetFsbgp4mpeAggregateAsSet(i4Fsbgp4mpeAggregateIndex ,i4SetValFsbgp4mpeAggregateAsSet) \
 nmhSetCmnWithLock(Fsbgp4mpeAggregateAsSet, 11, Fsbgp4mpeAggregateAsSetSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %i", i4Fsbgp4mpeAggregateIndex ,i4SetValFsbgp4mpeAggregateAsSet)
#define nmhSetFsbgp4mpeAggregateAdvertiseRouteMapName(i4Fsbgp4mpeAggregateIndex ,pSetValFsbgp4mpeAggregateAdvertiseRouteMapName) \
 nmhSetCmnWithLock(Fsbgp4mpeAggregateAdvertiseRouteMapName, 11, Fsbgp4mpeAggregateAdvertiseRouteMapNameSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %s", i4Fsbgp4mpeAggregateIndex ,pSetValFsbgp4mpeAggregateAdvertiseRouteMapName)
#define nmhSetFsbgp4mpeAggregateSuppressRouteMapName(i4Fsbgp4mpeAggregateIndex ,pSetValFsbgp4mpeAggregateSuppressRouteMapName) \
 nmhSetCmnWithLock(Fsbgp4mpeAggregateSuppressRouteMapName, 11, Fsbgp4mpeAggregateSuppressRouteMapNameSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %s", i4Fsbgp4mpeAggregateIndex ,pSetValFsbgp4mpeAggregateSuppressRouteMapName)
#define nmhSetFsbgp4mpeAggregateAttributeRouteMapName(i4Fsbgp4mpeAggregateIndex ,pSetValFsbgp4mpeAggregateAttributeRouteMapName) \
 nmhSetCmnWithLock(Fsbgp4mpeAggregateAttributeRouteMapName, 11, Fsbgp4mpeAggregateAttributeRouteMapNameSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %s", i4Fsbgp4mpeAggregateIndex ,pSetValFsbgp4mpeAggregateAttributeRouteMapName)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4mpeImportRoutePrefixAfi[11];
extern UINT4 Fsbgp4mpeImportRoutePrefixSafi[11];
extern UINT4 Fsbgp4mpeImportRoutePrefix[11];
extern UINT4 Fsbgp4mpeImportRoutePrefixLen[11];
extern UINT4 Fsbgp4mpeImportRouteProtocol[11];
extern UINT4 Fsbgp4mpeImportRouteNextHop[11];
extern UINT4 Fsbgp4mpeImportRouteIfIndex[11];
extern UINT4 Fsbgp4mpeImportRouteMetric[11];
extern UINT4 Fsbgp4mpeImportRouteVrf[11];
extern UINT4 Fsbgp4mpeImportRouteAction[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4mpeImportRouteAction(i4Fsbgp4mpeImportRoutePrefixAfi , i4Fsbgp4mpeImportRoutePrefixSafi , pFsbgp4mpeImportRoutePrefix , i4Fsbgp4mpeImportRoutePrefixLen , i4Fsbgp4mpeImportRouteProtocol , pFsbgp4mpeImportRouteNextHop , i4Fsbgp4mpeImportRouteIfIndex , i4Fsbgp4mpeImportRouteMetric , pFsbgp4mpeImportRouteVrf ,i4SetValFsbgp4mpeImportRouteAction) \
 nmhSetCmnWithLock(Fsbgp4mpeImportRouteAction, 11, Fsbgp4mpeImportRouteActionSet, BgpLock, BgpUnLock, 0, 0, 9, "%i %i %s %i %i %s %i %i %s %i", i4Fsbgp4mpeImportRoutePrefixAfi , i4Fsbgp4mpeImportRoutePrefixSafi , pFsbgp4mpeImportRoutePrefix , i4Fsbgp4mpeImportRoutePrefixLen , i4Fsbgp4mpeImportRouteProtocol , pFsbgp4mpeImportRouteNextHop , i4Fsbgp4mpeImportRouteIfIndex , i4Fsbgp4mpeImportRouteMetric , pFsbgp4mpeImportRouteVrf ,i4SetValFsbgp4mpeImportRouteAction)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4mpeAddCommRtAfi[12];
extern UINT4 Fsbgp4mpeAddCommRtSafi[12];
extern UINT4 Fsbgp4mpeAddCommIpNetwork[12];
extern UINT4 Fsbgp4mpeAddCommIpPrefixLen[12];
extern UINT4 Fsbgp4mpeAddCommVal[12];
extern UINT4 Fsbgp4mpeAddCommRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4mpeAddCommRowStatus(i4Fsbgp4mpeAddCommRtAfi , i4Fsbgp4mpeAddCommRtSafi , pFsbgp4mpeAddCommIpNetwork , i4Fsbgp4mpeAddCommIpPrefixLen , u4Fsbgp4mpeAddCommVal ,i4SetValFsbgp4mpeAddCommRowStatus) \
 nmhSetCmnWithLock(Fsbgp4mpeAddCommRowStatus, 12, Fsbgp4mpeAddCommRowStatusSet, BgpLock, BgpUnLock, 0, 1, 5, "%i %i %s %i %u %i", i4Fsbgp4mpeAddCommRtAfi , i4Fsbgp4mpeAddCommRtSafi , pFsbgp4mpeAddCommIpNetwork , i4Fsbgp4mpeAddCommIpPrefixLen , u4Fsbgp4mpeAddCommVal ,i4SetValFsbgp4mpeAddCommRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4mpeDeleteCommRtAfi[12];
extern UINT4 Fsbgp4mpeDeleteCommRtSafi[12];
extern UINT4 Fsbgp4mpeDeleteCommIpNetwork[12];
extern UINT4 Fsbgp4mpeDeleteCommIpPrefixLen[12];
extern UINT4 Fsbgp4mpeDeleteCommVal[12];
extern UINT4 Fsbgp4mpeDeleteCommRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4mpeDeleteCommRowStatus(i4Fsbgp4mpeDeleteCommRtAfi , i4Fsbgp4mpeDeleteCommRtSafi , pFsbgp4mpeDeleteCommIpNetwork , i4Fsbgp4mpeDeleteCommIpPrefixLen , u4Fsbgp4mpeDeleteCommVal ,i4SetValFsbgp4mpeDeleteCommRowStatus) \
 nmhSetCmnWithLock(Fsbgp4mpeDeleteCommRowStatus, 12, Fsbgp4mpeDeleteCommRowStatusSet, BgpLock, BgpUnLock, 0, 1, 5, "%i %i %s %i %u %i", i4Fsbgp4mpeDeleteCommRtAfi , i4Fsbgp4mpeDeleteCommRtSafi , pFsbgp4mpeDeleteCommIpNetwork , i4Fsbgp4mpeDeleteCommIpPrefixLen , u4Fsbgp4mpeDeleteCommVal ,i4SetValFsbgp4mpeDeleteCommRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4mpeCommSetStatusAfi[12];
extern UINT4 Fsbgp4mpeCommSetStatusSafi[12];
extern UINT4 Fsbgp4mpeCommSetStatusIpNetwork[12];
extern UINT4 Fsbgp4mpeCommSetStatusIpPrefixLen[12];
extern UINT4 Fsbgp4mpeCommSetStatus[12];
extern UINT4 Fsbgp4mpeCommSetStatusRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4mpeCommSetStatus(i4Fsbgp4mpeCommSetStatusAfi , i4Fsbgp4mpeCommSetStatusSafi , pFsbgp4mpeCommSetStatusIpNetwork , i4Fsbgp4mpeCommSetStatusIpPrefixLen ,i4SetValFsbgp4mpeCommSetStatus) \
 nmhSetCmnWithLock(Fsbgp4mpeCommSetStatus, 12, Fsbgp4mpeCommSetStatusSet, BgpLock, BgpUnLock, 0, 0, 4, "%i %i %s %i %i", i4Fsbgp4mpeCommSetStatusAfi , i4Fsbgp4mpeCommSetStatusSafi , pFsbgp4mpeCommSetStatusIpNetwork , i4Fsbgp4mpeCommSetStatusIpPrefixLen ,i4SetValFsbgp4mpeCommSetStatus)
#define nmhSetFsbgp4mpeCommSetStatusRowStatus(i4Fsbgp4mpeCommSetStatusAfi , i4Fsbgp4mpeCommSetStatusSafi , pFsbgp4mpeCommSetStatusIpNetwork , i4Fsbgp4mpeCommSetStatusIpPrefixLen ,i4SetValFsbgp4mpeCommSetStatusRowStatus) \
 nmhSetCmnWithLock(Fsbgp4mpeCommSetStatusRowStatus, 12, Fsbgp4mpeCommSetStatusRowStatusSet, BgpLock, BgpUnLock, 0, 1, 4, "%i %i %s %i %i", i4Fsbgp4mpeCommSetStatusAfi , i4Fsbgp4mpeCommSetStatusSafi , pFsbgp4mpeCommSetStatusIpNetwork , i4Fsbgp4mpeCommSetStatusIpPrefixLen ,i4SetValFsbgp4mpeCommSetStatusRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4mpeAddExtCommRtAfi[12];
extern UINT4 Fsbgp4mpeAddExtCommRtSafi[12];
extern UINT4 Fsbgp4mpeAddExtCommIpNetwork[12];
extern UINT4 Fsbgp4mpeAddExtCommIpPrefixLen[12];
extern UINT4 Fsbgp4mpeAddExtCommVal[12];
extern UINT4 Fsbgp4mpeAddExtCommRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4mpeAddExtCommRowStatus(i4Fsbgp4mpeAddExtCommRtAfi , i4Fsbgp4mpeAddExtCommRtSafi , pFsbgp4mpeAddExtCommIpNetwork , i4Fsbgp4mpeAddExtCommIpPrefixLen , pFsbgp4mpeAddExtCommVal ,i4SetValFsbgp4mpeAddExtCommRowStatus) \
 nmhSetCmnWithLock(Fsbgp4mpeAddExtCommRowStatus, 12, Fsbgp4mpeAddExtCommRowStatusSet, BgpLock, BgpUnLock, 0, 1, 5, "%i %i %s %i %s %i", i4Fsbgp4mpeAddExtCommRtAfi , i4Fsbgp4mpeAddExtCommRtSafi , pFsbgp4mpeAddExtCommIpNetwork , i4Fsbgp4mpeAddExtCommIpPrefixLen , pFsbgp4mpeAddExtCommVal ,i4SetValFsbgp4mpeAddExtCommRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4mpeDeleteExtCommRtAfi[12];
extern UINT4 Fsbgp4mpeDeleteExtCommRtSafi[12];
extern UINT4 Fsbgp4mpeDeleteExtCommIpNetwork[12];
extern UINT4 Fsbgp4mpeDeleteExtCommIpPrefixLen[12];
extern UINT4 Fsbgp4mpeDeleteExtCommVal[12];
extern UINT4 Fsbgp4mpeDeleteExtCommRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4mpeDeleteExtCommRowStatus(i4Fsbgp4mpeDeleteExtCommRtAfi , i4Fsbgp4mpeDeleteExtCommRtSafi , pFsbgp4mpeDeleteExtCommIpNetwork , i4Fsbgp4mpeDeleteExtCommIpPrefixLen , pFsbgp4mpeDeleteExtCommVal ,i4SetValFsbgp4mpeDeleteExtCommRowStatus) \
 nmhSetCmnWithLock(Fsbgp4mpeDeleteExtCommRowStatus, 12, Fsbgp4mpeDeleteExtCommRowStatusSet, BgpLock, BgpUnLock, 0, 1, 5, "%i %i %s %i %s %i", i4Fsbgp4mpeDeleteExtCommRtAfi , i4Fsbgp4mpeDeleteExtCommRtSafi , pFsbgp4mpeDeleteExtCommIpNetwork , i4Fsbgp4mpeDeleteExtCommIpPrefixLen , pFsbgp4mpeDeleteExtCommVal ,i4SetValFsbgp4mpeDeleteExtCommRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4mpeExtCommSetStatusRtAfi[12];
extern UINT4 Fsbgp4mpeExtCommSetStatusRtSafi[12];
extern UINT4 Fsbgp4mpeExtCommSetStatusIpNetwork[12];
extern UINT4 Fsbgp4mpeExtCommSetStatusIpPrefixLen[12];
extern UINT4 Fsbgp4mpeExtCommSetStatus[12];
extern UINT4 Fsbgp4mpeExtCommSetStatusRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4mpeExtCommSetStatus(i4Fsbgp4mpeExtCommSetStatusRtAfi , i4Fsbgp4mpeExtCommSetStatusRtSafi , pFsbgp4mpeExtCommSetStatusIpNetwork , i4Fsbgp4mpeExtCommSetStatusIpPrefixLen ,i4SetValFsbgp4mpeExtCommSetStatus) \
 nmhSetCmnWithLock(Fsbgp4mpeExtCommSetStatus, 12, Fsbgp4mpeExtCommSetStatusSet, BgpLock, BgpUnLock, 0, 0, 4, "%i %i %s %i %i", i4Fsbgp4mpeExtCommSetStatusRtAfi , i4Fsbgp4mpeExtCommSetStatusRtSafi , pFsbgp4mpeExtCommSetStatusIpNetwork , i4Fsbgp4mpeExtCommSetStatusIpPrefixLen ,i4SetValFsbgp4mpeExtCommSetStatus)
#define nmhSetFsbgp4mpeExtCommSetStatusRowStatus(i4Fsbgp4mpeExtCommSetStatusRtAfi , i4Fsbgp4mpeExtCommSetStatusRtSafi , pFsbgp4mpeExtCommSetStatusIpNetwork , i4Fsbgp4mpeExtCommSetStatusIpPrefixLen ,i4SetValFsbgp4mpeExtCommSetStatusRowStatus) \
 nmhSetCmnWithLock(Fsbgp4mpeExtCommSetStatusRowStatus, 12, Fsbgp4mpeExtCommSetStatusRowStatusSet, BgpLock, BgpUnLock, 0, 1, 4, "%i %i %s %i %i", i4Fsbgp4mpeExtCommSetStatusRtAfi , i4Fsbgp4mpeExtCommSetStatusRtSafi , pFsbgp4mpeExtCommSetStatusIpNetwork , i4Fsbgp4mpeExtCommSetStatusIpPrefixLen ,i4SetValFsbgp4mpeExtCommSetStatusRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4mpePeerLinkType[12];
extern UINT4 Fsbgp4mpePeerLinkRemAddr[12];
extern UINT4 Fsbgp4mpeLinkBandWidth[12];
extern UINT4 Fsbgp4mpePeerLinkBwRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4mpeLinkBandWidth(i4Fsbgp4mpePeerLinkType , pFsbgp4mpePeerLinkRemAddr ,u4SetValFsbgp4mpeLinkBandWidth) \
 nmhSetCmnWithLock(Fsbgp4mpeLinkBandWidth, 12, Fsbgp4mpeLinkBandWidthSet, BgpLock, BgpUnLock, 0, 0, 2, "%i %s %u", i4Fsbgp4mpePeerLinkType , pFsbgp4mpePeerLinkRemAddr ,u4SetValFsbgp4mpeLinkBandWidth)
#define nmhSetFsbgp4mpePeerLinkBwRowStatus(i4Fsbgp4mpePeerLinkType , pFsbgp4mpePeerLinkRemAddr ,i4SetValFsbgp4mpePeerLinkBwRowStatus) \
 nmhSetCmnWithLock(Fsbgp4mpePeerLinkBwRowStatus, 12, Fsbgp4mpePeerLinkBwRowStatusSet, BgpLock, BgpUnLock, 0, 1, 2, "%i %s %i", i4Fsbgp4mpePeerLinkType , pFsbgp4mpePeerLinkRemAddr ,i4SetValFsbgp4mpePeerLinkBwRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4mpeCapPeerType[12];
extern UINT4 Fsbgp4mpeCapPeerRemoteIpAddr[12];
extern UINT4 Fsbgp4mpeSupportedCapabilityCode[12];
extern UINT4 Fsbgp4mpeSupportedCapabilityLength[12];
extern UINT4 Fsbgp4mpeSupportedCapabilityValue[12];
extern UINT4 Fsbgp4mpeCapSupportedCapsRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4mpeCapSupportedCapsRowStatus(i4Fsbgp4mpeCapPeerType , pFsbgp4mpeCapPeerRemoteIpAddr , i4Fsbgp4mpeSupportedCapabilityCode , i4Fsbgp4mpeSupportedCapabilityLength , pFsbgp4mpeSupportedCapabilityValue ,i4SetValFsbgp4mpeCapSupportedCapsRowStatus) \
 nmhSetCmnWithLock(Fsbgp4mpeCapSupportedCapsRowStatus, 12, Fsbgp4mpeCapSupportedCapsRowStatusSet, BgpLock, BgpUnLock, 0, 1, 5, "%i %s %i %i %s %i", i4Fsbgp4mpeCapPeerType , pFsbgp4mpeCapPeerRemoteIpAddr , i4Fsbgp4mpeSupportedCapabilityCode , i4Fsbgp4mpeSupportedCapabilityLength , pFsbgp4mpeSupportedCapabilityValue ,i4SetValFsbgp4mpeCapSupportedCapsRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4mpeRtRefreshInboundPeerType[12];
extern UINT4 Fsbgp4mpeRtRefreshInboundPeerAddr[12];
extern UINT4 Fsbgp4mpeRtRefreshInboundAfi[12];
extern UINT4 Fsbgp4mpeRtRefreshInboundSafi[12];
extern UINT4 Fsbgp4mpeRtRefreshInboundRequest[12];
extern UINT4 Fsbgp4mpeRtRefreshInboundPrefixFilter[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4mpeRtRefreshInboundRequest(i4Fsbgp4mpeRtRefreshInboundPeerType , pFsbgp4mpeRtRefreshInboundPeerAddr , i4Fsbgp4mpeRtRefreshInboundAfi , i4Fsbgp4mpeRtRefreshInboundSafi ,i4SetValFsbgp4mpeRtRefreshInboundRequest) \
 nmhSetCmnWithLock(Fsbgp4mpeRtRefreshInboundRequest, 12, Fsbgp4mpeRtRefreshInboundRequestSet, BgpLock, BgpUnLock, 0, 0, 4, "%i %s %i %i %i", i4Fsbgp4mpeRtRefreshInboundPeerType , pFsbgp4mpeRtRefreshInboundPeerAddr , i4Fsbgp4mpeRtRefreshInboundAfi , i4Fsbgp4mpeRtRefreshInboundSafi ,i4SetValFsbgp4mpeRtRefreshInboundRequest)
#define nmhSetFsbgp4mpeRtRefreshInboundPrefixFilter(i4Fsbgp4mpeRtRefreshInboundPeerType , pFsbgp4mpeRtRefreshInboundPeerAddr , i4Fsbgp4mpeRtRefreshInboundAfi , i4Fsbgp4mpeRtRefreshInboundSafi ,i4SetValFsbgp4mpeRtRefreshInboundPrefixFilter) \
 nmhSetCmnWithLock(Fsbgp4mpeRtRefreshInboundPrefixFilter, 12, Fsbgp4mpeRtRefreshInboundPrefixFilterSet, BgpLock, BgpUnLock, 0, 0, 4, "%i %s %i %i %i", i4Fsbgp4mpeRtRefreshInboundPeerType , pFsbgp4mpeRtRefreshInboundPeerAddr , i4Fsbgp4mpeRtRefreshInboundAfi , i4Fsbgp4mpeRtRefreshInboundSafi ,i4SetValFsbgp4mpeRtRefreshInboundPrefixFilter)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4mpeSoftReconfigOutboundPeerType[12];
extern UINT4 Fsbgp4mpeSoftReconfigOutboundPeerAddr[12];
extern UINT4 Fsbgp4mpeSoftReconfigOutboundAfi[12];
extern UINT4 Fsbgp4mpeSoftReconfigOutboundSafi[12];
extern UINT4 Fsbgp4mpeSoftReconfigOutboundRequest[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4mpeSoftReconfigOutboundRequest(i4Fsbgp4mpeSoftReconfigOutboundPeerType , pFsbgp4mpeSoftReconfigOutboundPeerAddr , i4Fsbgp4mpeSoftReconfigOutboundAfi , i4Fsbgp4mpeSoftReconfigOutboundSafi ,i4SetValFsbgp4mpeSoftReconfigOutboundRequest) \
 nmhSetCmnWithLock(Fsbgp4mpeSoftReconfigOutboundRequest, 12, Fsbgp4mpeSoftReconfigOutboundRequestSet, BgpLock, BgpUnLock, 0, 0, 4, "%i %s %i %i %i", i4Fsbgp4mpeSoftReconfigOutboundPeerType , pFsbgp4mpeSoftReconfigOutboundPeerAddr , i4Fsbgp4mpeSoftReconfigOutboundAfi , i4Fsbgp4mpeSoftReconfigOutboundSafi ,i4SetValFsbgp4mpeSoftReconfigOutboundRequest)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4MplsVpnVrfName[12];
extern UINT4 Fsbgp4MplsVpnVrfRouteTargetType[12];
extern UINT4 Fsbgp4MplsVpnVrfRouteTarget[12];
extern UINT4 Fsbgp4MplsVpnVrfRouteTargetRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4MplsVpnVrfRouteTargetRowStatus(pFsbgp4MplsVpnVrfName , i4Fsbgp4MplsVpnVrfRouteTargetType , pFsbgp4MplsVpnVrfRouteTarget ,i4SetValFsbgp4MplsVpnVrfRouteTargetRowStatus) \
 nmhSetCmnWithLock(Fsbgp4MplsVpnVrfRouteTargetRowStatus, 12, Fsbgp4MplsVpnVrfRouteTargetRowStatusSet, BgpLock, BgpUnLock, 0, 1, 3, "%s %i %s %i", pFsbgp4MplsVpnVrfName , i4Fsbgp4MplsVpnVrfRouteTargetType , pFsbgp4MplsVpnVrfRouteTarget ,i4SetValFsbgp4MplsVpnVrfRouteTargetRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4MplsVpnVrfRedisAfi[12];
extern UINT4 Fsbgp4MplsVpnVrfRedisSafi[12];
extern UINT4 Fsbgp4MplsVpnVrfRedisProtoMask[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4MplsVpnVrfRedisProtoMask(i4Fsbgp4MplsVpnVrfRedisAfi , i4Fsbgp4MplsVpnVrfRedisSafi , pFsbgp4MplsVpnVrfName ,i4SetValFsbgp4MplsVpnVrfRedisProtoMask) \
 nmhSetCmnWithLock(Fsbgp4MplsVpnVrfRedisProtoMask, 12, Fsbgp4MplsVpnVrfRedisProtoMaskSet, BgpLock, BgpUnLock, 0, 0, 3, "%i %i %s %i", i4Fsbgp4MplsVpnVrfRedisAfi , i4Fsbgp4MplsVpnVrfRedisSafi , pFsbgp4MplsVpnVrfName ,i4SetValFsbgp4MplsVpnVrfRedisProtoMask)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsBgp4DistInOutRouteMapName[12];
extern UINT4 FsBgp4DistInOutRouteMapType[12];
extern UINT4 FsBgp4DistInOutRouteMapValue[12];
extern UINT4 FsBgp4DistInOutRouteMapRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsBgp4DistInOutRouteMapValue(pFsBgp4DistInOutRouteMapName , i4FsBgp4DistInOutRouteMapType ,i4SetValFsBgp4DistInOutRouteMapValue) \
 nmhSetCmnWithLock(FsBgp4DistInOutRouteMapValue, 12, FsBgp4DistInOutRouteMapValueSet, BgpLock, BgpUnLock, 0, 0, 2, "%s %i %i", pFsBgp4DistInOutRouteMapName , i4FsBgp4DistInOutRouteMapType ,i4SetValFsBgp4DistInOutRouteMapValue)
#define nmhSetFsBgp4DistInOutRouteMapRowStatus(pFsBgp4DistInOutRouteMapName , i4FsBgp4DistInOutRouteMapType ,i4SetValFsBgp4DistInOutRouteMapRowStatus) \
 nmhSetCmnWithLock(FsBgp4DistInOutRouteMapRowStatus, 12, FsBgp4DistInOutRouteMapRowStatusSet, BgpLock, BgpUnLock, 0, 1, 2, "%s %i %i", pFsBgp4DistInOutRouteMapName , i4FsBgp4DistInOutRouteMapType ,i4SetValFsBgp4DistInOutRouteMapRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsBgp4PreferenceValue[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsBgp4PreferenceValue(i4SetValFsBgp4PreferenceValue) \
 nmhSetCmnWithLock(FsBgp4PreferenceValue, 10, FsBgp4PreferenceValueSet, BgpLock, BgpUnLock, 0, 0, 0, "%i", i4SetValFsBgp4PreferenceValue)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsBgp4NeighborRouteMapPeerAddrType[12];
extern UINT4 FsBgp4NeighborRouteMapPeer[12];
extern UINT4 FsBgp4NeighborRouteMapDirection[12];
extern UINT4 FsBgp4NeighborRouteMapName[12];
extern UINT4 FsBgp4NeighborRouteMapRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsBgp4NeighborRouteMapName(i4FsBgp4NeighborRouteMapPeerAddrType , pFsBgp4NeighborRouteMapPeer , i4FsBgp4NeighborRouteMapDirection ,pSetValFsBgp4NeighborRouteMapName) \
 nmhSetCmnWithLock(FsBgp4NeighborRouteMapName, 12, FsBgp4NeighborRouteMapNameSet, BgpLock, BgpUnLock, 0, 0, 3, "%i %s %i %s", i4FsBgp4NeighborRouteMapPeerAddrType , pFsBgp4NeighborRouteMapPeer , i4FsBgp4NeighborRouteMapDirection ,pSetValFsBgp4NeighborRouteMapName)
#define nmhSetFsBgp4NeighborRouteMapRowStatus(i4FsBgp4NeighborRouteMapPeerAddrType , pFsBgp4NeighborRouteMapPeer , i4FsBgp4NeighborRouteMapDirection ,i4SetValFsBgp4NeighborRouteMapRowStatus) \
 nmhSetCmnWithLock(FsBgp4NeighborRouteMapRowStatus, 12, FsBgp4NeighborRouteMapRowStatusSet, BgpLock, BgpUnLock, 0, 1, 3, "%i %s %i %i", i4FsBgp4NeighborRouteMapPeerAddrType , pFsBgp4NeighborRouteMapPeer , i4FsBgp4NeighborRouteMapDirection ,i4SetValFsBgp4NeighborRouteMapRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsBgp4PeerGroupName[11];
extern UINT4 FsBgp4PeerGroupRemoteAs[11];
extern UINT4 FsBgp4PeerGroupHoldTimeConfigured[11];
extern UINT4 FsBgp4PeerGroupKeepAliveConfigured[11];
extern UINT4 FsBgp4PeerGroupConnectRetryInterval[11];
extern UINT4 FsBgp4PeerGroupMinASOriginInterval[11];
extern UINT4 FsBgp4PeerGroupMinRouteAdvInterval[11];
extern UINT4 FsBgp4PeerGroupAllowAutomaticStart[11];
extern UINT4 FsBgp4PeerGroupAllowAutomaticStop[11];
extern UINT4 FsBgp4PeerGroupIdleHoldTimeConfigured[11];
extern UINT4 FsBgp4PeerGroupDampPeerOscillations[11];
extern UINT4 FsBgp4PeerGroupDelayOpen[11];
extern UINT4 FsBgp4PeerGroupDelayOpenTimeConfigured[11];
extern UINT4 FsBgp4PeerGroupPrefixUpperLimit[11];
extern UINT4 FsBgp4PeerGroupTcpConnectRetryCnt[11];
extern UINT4 FsBgp4PeerGroupEBGPMultiHop[11];
extern UINT4 FsBgp4PeerGroupEBGPHopLimit[11];
extern UINT4 FsBgp4PeerGroupNextHopSelf[11];
extern UINT4 FsBgp4PeerGroupRflClient[11];
extern UINT4 FsBgp4PeerGroupTcpSendBufSize[11];
extern UINT4 FsBgp4PeerGroupTcpRcvBufSize[11];
extern UINT4 FsBgp4PeerGroupCommSendStatus[11];
extern UINT4 FsBgp4PeerGroupECommSendStatus[11];
extern UINT4 FsBgp4PeerGroupPassive[11];
extern UINT4 FsBgp4PeerGroupDefaultOriginate[11];
extern UINT4 FsBgp4PeerGroupActivateMPCapability[11];
extern UINT4 FsBgp4PeerGroupDeactivateMPCapability[11];
extern UINT4 FsBgp4PeerGroupRouteMapNameIn[11];
extern UINT4 FsBgp4PeerGroupRouteMapNameOut[11];
extern UINT4 FsBgp4PeerGroupStatus[11];
extern UINT4 FsBgp4PeerGroupIpPrefixNameIn[11];
extern UINT4 FsBgp4PeerGroupIpPrefixNameOut[11];
extern UINT4 FsBgp4PeerGroupOrfType[11];
extern UINT4 FsBgp4PeerGroupOrfCapMode[11];
extern UINT4 FsBgp4PeerGroupOrfRequest[11];
extern UINT4 FsBgp4PeerGroupBfdStatus[11];
extern UINT4 FsBgp4PeerGroupOverrideCapability[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsBgp4PeerGroupRemoteAs(pFsBgp4PeerGroupName ,u4SetValFsBgp4PeerGroupRemoteAs) \
 nmhSetCmnWithLock(FsBgp4PeerGroupRemoteAs, 11, FsBgp4PeerGroupRemoteAsSet, BgpLock, BgpUnLock, 0, 0, 1, "%s %u", pFsBgp4PeerGroupName ,u4SetValFsBgp4PeerGroupRemoteAs)
#define nmhSetFsBgp4PeerGroupHoldTimeConfigured(pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupHoldTimeConfigured) \
 nmhSetCmnWithLock(FsBgp4PeerGroupHoldTimeConfigured, 11, FsBgp4PeerGroupHoldTimeConfiguredSet, BgpLock, BgpUnLock, 0, 0, 1, "%s %i", pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupHoldTimeConfigured)
#define nmhSetFsBgp4PeerGroupKeepAliveConfigured(pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupKeepAliveConfigured) \
 nmhSetCmnWithLock(FsBgp4PeerGroupKeepAliveConfigured, 11, FsBgp4PeerGroupKeepAliveConfiguredSet, BgpLock, BgpUnLock, 0, 0, 1, "%s %i", pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupKeepAliveConfigured)
#define nmhSetFsBgp4PeerGroupConnectRetryInterval(pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupConnectRetryInterval) \
 nmhSetCmnWithLock(FsBgp4PeerGroupConnectRetryInterval, 11, FsBgp4PeerGroupConnectRetryIntervalSet, BgpLock, BgpUnLock, 0, 0, 1, "%s %i", pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupConnectRetryInterval)
#define nmhSetFsBgp4PeerGroupMinASOriginInterval(pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupMinASOriginInterval) \
 nmhSetCmnWithLock(FsBgp4PeerGroupMinASOriginInterval, 11, FsBgp4PeerGroupMinASOriginIntervalSet, BgpLock, BgpUnLock, 0, 0, 1, "%s %i", pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupMinASOriginInterval)
#define nmhSetFsBgp4PeerGroupMinRouteAdvInterval(pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupMinRouteAdvInterval) \
 nmhSetCmnWithLock(FsBgp4PeerGroupMinRouteAdvInterval, 11, FsBgp4PeerGroupMinRouteAdvIntervalSet, BgpLock, BgpUnLock, 0, 0, 1, "%s %i", pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupMinRouteAdvInterval)
#define nmhSetFsBgp4PeerGroupAllowAutomaticStart(pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupAllowAutomaticStart) \
 nmhSetCmnWithLock(FsBgp4PeerGroupAllowAutomaticStart, 11, FsBgp4PeerGroupAllowAutomaticStartSet, BgpLock, BgpUnLock, 0, 0, 1, "%s %i", pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupAllowAutomaticStart)
#define nmhSetFsBgp4PeerGroupAllowAutomaticStop(pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupAllowAutomaticStop) \
 nmhSetCmnWithLock(FsBgp4PeerGroupAllowAutomaticStop, 11, FsBgp4PeerGroupAllowAutomaticStopSet, BgpLock, BgpUnLock, 0, 0, 1, "%s %i", pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupAllowAutomaticStop)
#define nmhSetFsBgp4PeerGroupIdleHoldTimeConfigured(pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupIdleHoldTimeConfigured) \
 nmhSetCmnWithLock(FsBgp4PeerGroupIdleHoldTimeConfigured, 11, FsBgp4PeerGroupIdleHoldTimeConfiguredSet, BgpLock, BgpUnLock, 0, 0, 1, "%s %i", pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupIdleHoldTimeConfigured)
#define nmhSetFsBgp4PeerGroupDampPeerOscillations(pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupDampPeerOscillations) \
 nmhSetCmnWithLock(FsBgp4PeerGroupDampPeerOscillations, 11, FsBgp4PeerGroupDampPeerOscillationsSet, BgpLock, BgpUnLock, 0, 0, 1, "%s %i", pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupDampPeerOscillations)
#define nmhSetFsBgp4PeerGroupDelayOpen(pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupDelayOpen) \
 nmhSetCmnWithLock(FsBgp4PeerGroupDelayOpen, 11, FsBgp4PeerGroupDelayOpenSet, BgpLock, BgpUnLock, 0, 0, 1, "%s %i", pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupDelayOpen)
#define nmhSetFsBgp4PeerGroupDelayOpenTimeConfigured(pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupDelayOpenTimeConfigured) \
 nmhSetCmnWithLock(FsBgp4PeerGroupDelayOpenTimeConfigured, 11, FsBgp4PeerGroupDelayOpenTimeConfiguredSet, BgpLock, BgpUnLock, 0, 0, 1, "%s %i", pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupDelayOpenTimeConfigured)
#define nmhSetFsBgp4PeerGroupPrefixUpperLimit(pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupPrefixUpperLimit) \
 nmhSetCmnWithLock(FsBgp4PeerGroupPrefixUpperLimit, 11, FsBgp4PeerGroupPrefixUpperLimitSet, BgpLock, BgpUnLock, 0, 0, 1, "%s %i", pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupPrefixUpperLimit)
#define nmhSetFsBgp4PeerGroupTcpConnectRetryCnt(pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupTcpConnectRetryCnt) \
 nmhSetCmnWithLock(FsBgp4PeerGroupTcpConnectRetryCnt, 11, FsBgp4PeerGroupTcpConnectRetryCntSet, BgpLock, BgpUnLock, 0, 0, 1, "%s %i", pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupTcpConnectRetryCnt)
#define nmhSetFsBgp4PeerGroupEBGPMultiHop(pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupEBGPMultiHop) \
 nmhSetCmnWithLock(FsBgp4PeerGroupEBGPMultiHop, 11, FsBgp4PeerGroupEBGPMultiHopSet, BgpLock, BgpUnLock, 0, 0, 1, "%s %i", pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupEBGPMultiHop)
#define nmhSetFsBgp4PeerGroupEBGPHopLimit(pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupEBGPHopLimit) \
 nmhSetCmnWithLock(FsBgp4PeerGroupEBGPHopLimit, 11, FsBgp4PeerGroupEBGPHopLimitSet, BgpLock, BgpUnLock, 0, 0, 1, "%s %i", pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupEBGPHopLimit)
#define nmhSetFsBgp4PeerGroupNextHopSelf(pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupNextHopSelf) \
 nmhSetCmnWithLock(FsBgp4PeerGroupNextHopSelf, 11, FsBgp4PeerGroupNextHopSelfSet, BgpLock, BgpUnLock, 0, 0, 1, "%s %i", pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupNextHopSelf)
#define nmhSetFsBgp4PeerGroupRflClient(pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupRflClient) \
 nmhSetCmnWithLock(FsBgp4PeerGroupRflClient, 11, FsBgp4PeerGroupRflClientSet, BgpLock, BgpUnLock, 0, 0, 1, "%s %i", pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupRflClient)
#define nmhSetFsBgp4PeerGroupTcpSendBufSize(pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupTcpSendBufSize) \
 nmhSetCmnWithLock(FsBgp4PeerGroupTcpSendBufSize, 11, FsBgp4PeerGroupTcpSendBufSizeSet, BgpLock, BgpUnLock, 0, 0, 1, "%s %i", pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupTcpSendBufSize)
#define nmhSetFsBgp4PeerGroupTcpRcvBufSize(pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupTcpRcvBufSize) \
 nmhSetCmnWithLock(FsBgp4PeerGroupTcpRcvBufSize, 11, FsBgp4PeerGroupTcpRcvBufSizeSet, BgpLock, BgpUnLock, 0, 0, 1, "%s %i", pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupTcpRcvBufSize)
#define nmhSetFsBgp4PeerGroupCommSendStatus(pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupCommSendStatus) \
 nmhSetCmnWithLock(FsBgp4PeerGroupCommSendStatus, 11, FsBgp4PeerGroupCommSendStatusSet, BgpLock, BgpUnLock, 0, 0, 1, "%s %i", pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupCommSendStatus)
#define nmhSetFsBgp4PeerGroupECommSendStatus(pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupECommSendStatus) \
 nmhSetCmnWithLock(FsBgp4PeerGroupECommSendStatus, 11, FsBgp4PeerGroupECommSendStatusSet, BgpLock, BgpUnLock, 0, 0, 1, "%s %i", pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupECommSendStatus)
#define nmhSetFsBgp4PeerGroupPassive(pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupPassive) \
 nmhSetCmnWithLock(FsBgp4PeerGroupPassive, 11, FsBgp4PeerGroupPassiveSet, BgpLock, BgpUnLock, 0, 0, 1, "%s %i", pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupPassive)
#define nmhSetFsBgp4PeerGroupDefaultOriginate(pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupDefaultOriginate) \
 nmhSetCmnWithLock(FsBgp4PeerGroupDefaultOriginate, 11, FsBgp4PeerGroupDefaultOriginateSet, BgpLock, BgpUnLock, 0, 0, 1, "%s %i", pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupDefaultOriginate)
#define nmhSetFsBgp4PeerGroupActivateMPCapability(pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupActivateMPCapability) \
 nmhSetCmnWithLock(FsBgp4PeerGroupActivateMPCapability, 11, FsBgp4PeerGroupActivateMPCapabilitySet, BgpLock, BgpUnLock, 0, 0, 1, "%s %i", pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupActivateMPCapability)
#define nmhSetFsBgp4PeerGroupDeactivateMPCapability(pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupDeactivateMPCapability) \
 nmhSetCmnWithLock(FsBgp4PeerGroupDeactivateMPCapability, 11, FsBgp4PeerGroupDeactivateMPCapabilitySet, BgpLock, BgpUnLock, 0, 0, 1, "%s %i", pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupDeactivateMPCapability)
#define nmhSetFsBgp4PeerGroupRouteMapNameIn(pFsBgp4PeerGroupName ,pSetValFsBgp4PeerGroupRouteMapNameIn) \
 nmhSetCmnWithLock(FsBgp4PeerGroupRouteMapNameIn, 11, FsBgp4PeerGroupRouteMapNameInSet, BgpLock, BgpUnLock, 0, 0, 1, "%s %s", pFsBgp4PeerGroupName ,pSetValFsBgp4PeerGroupRouteMapNameIn)
#define nmhSetFsBgp4PeerGroupRouteMapNameOut(pFsBgp4PeerGroupName ,pSetValFsBgp4PeerGroupRouteMapNameOut) \
 nmhSetCmnWithLock(FsBgp4PeerGroupRouteMapNameOut, 11, FsBgp4PeerGroupRouteMapNameOutSet, BgpLock, BgpUnLock, 0, 0, 1, "%s %s", pFsBgp4PeerGroupName ,pSetValFsBgp4PeerGroupRouteMapNameOut)
#define nmhSetFsBgp4PeerGroupStatus(pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupStatus) \
 nmhSetCmnWithLock(FsBgp4PeerGroupStatus, 11, FsBgp4PeerGroupStatusSet, BgpLock, BgpUnLock, 0, 1, 1, "%s %i", pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupStatus)
#define nmhSetFsBgp4PeerGroupIpPrefixNameIn(pFsBgp4PeerGroupName ,pSetValFsBgp4PeerGroupIpPrefixNameIn) \
 nmhSetCmnWithLock(FsBgp4PeerGroupIpPrefixNameIn, 11, FsBgp4PeerGroupIpPrefixNameInSet, BgpLock, BgpUnLock, 0, 0, 1, "%s %s", pFsBgp4PeerGroupName ,pSetValFsBgp4PeerGroupIpPrefixNameIn)
#define nmhSetFsBgp4PeerGroupIpPrefixNameOut(pFsBgp4PeerGroupName ,pSetValFsBgp4PeerGroupIpPrefixNameOut) \
 nmhSetCmnWithLock(FsBgp4PeerGroupIpPrefixNameOut, 11, FsBgp4PeerGroupIpPrefixNameOutSet, BgpLock, BgpUnLock, 0, 0, 1, "%s %s", pFsBgp4PeerGroupName ,pSetValFsBgp4PeerGroupIpPrefixNameOut)
#define nmhSetFsBgp4PeerGroupOrfType(pFsBgp4PeerGroupName ,u4SetValFsBgp4PeerGroupOrfType) \
 nmhSetCmnWithLock(FsBgp4PeerGroupOrfType, 11, FsBgp4PeerGroupOrfTypeSet, BgpLock, BgpUnLock, 0, 0, 1, "%s %u", pFsBgp4PeerGroupName ,u4SetValFsBgp4PeerGroupOrfType)
#define nmhSetFsBgp4PeerGroupOrfCapMode(pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupOrfCapMode) \
 nmhSetCmnWithLock(FsBgp4PeerGroupOrfCapMode, 11, FsBgp4PeerGroupOrfCapModeSet, BgpLock, BgpUnLock, 0, 0, 1, "%s %i", pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupOrfCapMode)
#define nmhSetFsBgp4PeerGroupOrfRequest(pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupOrfRequest) \
 nmhSetCmnWithLock(FsBgp4PeerGroupOrfRequest, 11, FsBgp4PeerGroupOrfRequestSet, BgpLock, BgpUnLock, 0, 0, 1, "%s %i", pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupOrfRequest)
#define nmhSetFsBgp4PeerGroupBfdStatus(pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupBfdStatus) \
 nmhSetCmnWithLock(FsBgp4PeerGroupBfdStatus, 11, FsBgp4PeerGroupBfdStatusSet, BgpLock, BgpUnLock, 0, 0, 1, "%s %i", pFsBgp4PeerGroupName ,i4SetValFsBgp4PeerGroupBfdStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsBgp4PeerAddrType[11];
extern UINT4 FsBgp4PeerAddress[11];
extern UINT4 FsBgp4PeerAddStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsBgp4PeerAddStatus(pFsBgp4PeerGroupName , i4FsBgp4PeerAddrType , pFsBgp4PeerAddress ,i4SetValFsBgp4PeerAddStatus) \
 nmhSetCmnWithLock(FsBgp4PeerAddStatus, 11, FsBgp4PeerAddStatusSet, BgpLock, BgpUnLock, 0, 0, 3, "%s %i %s %i", pFsBgp4PeerGroupName , i4FsBgp4PeerAddrType , pFsBgp4PeerAddress ,i4SetValFsBgp4PeerAddStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4TCPMKTAuthKeyId[12];
extern UINT4 Fsbgp4TCPMKTAuthRecvKeyId[12];
extern UINT4 Fsbgp4TCPMKTAuthMasterKey[12];
extern UINT4 Fsbgp4TCPMKTAuthAlgo[12];
extern UINT4 Fsbgp4TCPMKTAuthTcpOptExc[12];
extern UINT4 Fsbgp4TCPMKTAuthRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4TCPMKTAuthRecvKeyId(i4Fsbgp4TCPMKTAuthKeyId ,i4SetValFsbgp4TCPMKTAuthRecvKeyId) \
 nmhSetCmnWithLock(Fsbgp4TCPMKTAuthRecvKeyId, 12, Fsbgp4TCPMKTAuthRecvKeyIdSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %i", i4Fsbgp4TCPMKTAuthKeyId ,i4SetValFsbgp4TCPMKTAuthRecvKeyId)
#define nmhSetFsbgp4TCPMKTAuthMasterKey(i4Fsbgp4TCPMKTAuthKeyId ,pSetValFsbgp4TCPMKTAuthMasterKey) \
 nmhSetCmnWithLock(Fsbgp4TCPMKTAuthMasterKey, 12, Fsbgp4TCPMKTAuthMasterKeySet, BgpLock, BgpUnLock, 0, 0, 1, "%i %s", i4Fsbgp4TCPMKTAuthKeyId ,pSetValFsbgp4TCPMKTAuthMasterKey)
#define nmhSetFsbgp4TCPMKTAuthAlgo(i4Fsbgp4TCPMKTAuthKeyId ,i4SetValFsbgp4TCPMKTAuthAlgo) \
 nmhSetCmnWithLock(Fsbgp4TCPMKTAuthAlgo, 12, Fsbgp4TCPMKTAuthAlgoSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %i", i4Fsbgp4TCPMKTAuthKeyId ,i4SetValFsbgp4TCPMKTAuthAlgo)
#define nmhSetFsbgp4TCPMKTAuthTcpOptExc(i4Fsbgp4TCPMKTAuthKeyId ,i4SetValFsbgp4TCPMKTAuthTcpOptExc) \
 nmhSetCmnWithLock(Fsbgp4TCPMKTAuthTcpOptExc, 12, Fsbgp4TCPMKTAuthTcpOptExcSet, BgpLock, BgpUnLock, 0, 0, 1, "%i %i", i4Fsbgp4TCPMKTAuthKeyId ,i4SetValFsbgp4TCPMKTAuthTcpOptExc)
#define nmhSetFsbgp4TCPMKTAuthRowStatus(i4Fsbgp4TCPMKTAuthKeyId ,i4SetValFsbgp4TCPMKTAuthRowStatus) \
 nmhSetCmnWithLock(Fsbgp4TCPMKTAuthRowStatus, 12, Fsbgp4TCPMKTAuthRowStatusSet, BgpLock, BgpUnLock, 0, 1, 1, "%i %i", i4Fsbgp4TCPMKTAuthKeyId ,i4SetValFsbgp4TCPMKTAuthRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsbgp4TCPAOAuthPeerType[12];
extern UINT4 Fsbgp4TCPAOAuthPeerAddr[12];
extern UINT4 Fsbgp4TCPAOAuthKeyId[12];
extern UINT4 Fsbgp4TCPAOAuthKeyStatus[12];
extern UINT4 Fsbgp4TCPAOAuthKeyStartAccept[12];
extern UINT4 Fsbgp4TCPAOAuthKeyStartGenerate[12];
extern UINT4 Fsbgp4TCPAOAuthKeyStopGenerate[12];
extern UINT4 Fsbgp4TCPAOAuthKeyStopAccept[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsbgp4TCPAOAuthKeyStatus(i4Fsbgp4TCPAOAuthPeerType , pFsbgp4TCPAOAuthPeerAddr , i4Fsbgp4TCPAOAuthKeyId ,i4SetValFsbgp4TCPAOAuthKeyStatus) \
 nmhSetCmnWithLock(Fsbgp4TCPAOAuthKeyStatus, 12, Fsbgp4TCPAOAuthKeyStatusSet, BgpLock, BgpUnLock, 0, 0, 3, "%i %s %i %i", i4Fsbgp4TCPAOAuthPeerType , pFsbgp4TCPAOAuthPeerAddr , i4Fsbgp4TCPAOAuthKeyId ,i4SetValFsbgp4TCPAOAuthKeyStatus)
#define nmhSetFsbgp4TCPAOAuthKeyStartAccept(i4Fsbgp4TCPAOAuthPeerType , pFsbgp4TCPAOAuthPeerAddr , i4Fsbgp4TCPAOAuthKeyId ,pSetValFsbgp4TCPAOAuthKeyStartAccept) \
 nmhSetCmnWithLock(Fsbgp4TCPAOAuthKeyStartAccept, 12, Fsbgp4TCPAOAuthKeyStartAcceptSet, BgpLock, BgpUnLock, 0, 0, 3, "%i %s %i %s", i4Fsbgp4TCPAOAuthPeerType , pFsbgp4TCPAOAuthPeerAddr , i4Fsbgp4TCPAOAuthKeyId ,pSetValFsbgp4TCPAOAuthKeyStartAccept)
#define nmhSetFsbgp4TCPAOAuthKeyStartGenerate(i4Fsbgp4TCPAOAuthPeerType , pFsbgp4TCPAOAuthPeerAddr , i4Fsbgp4TCPAOAuthKeyId ,pSetValFsbgp4TCPAOAuthKeyStartGenerate) \
 nmhSetCmnWithLock(Fsbgp4TCPAOAuthKeyStartGenerate, 12, Fsbgp4TCPAOAuthKeyStartGenerateSet, BgpLock, BgpUnLock, 0, 0, 3, "%i %s %i %s", i4Fsbgp4TCPAOAuthPeerType , pFsbgp4TCPAOAuthPeerAddr , i4Fsbgp4TCPAOAuthKeyId ,pSetValFsbgp4TCPAOAuthKeyStartGenerate)
#define nmhSetFsbgp4TCPAOAuthKeyStopGenerate(i4Fsbgp4TCPAOAuthPeerType , pFsbgp4TCPAOAuthPeerAddr , i4Fsbgp4TCPAOAuthKeyId ,pSetValFsbgp4TCPAOAuthKeyStopGenerate) \
 nmhSetCmnWithLock(Fsbgp4TCPAOAuthKeyStopGenerate, 12, Fsbgp4TCPAOAuthKeyStopGenerateSet, BgpLock, BgpUnLock, 0, 0, 3, "%i %s %i %s", i4Fsbgp4TCPAOAuthPeerType , pFsbgp4TCPAOAuthPeerAddr , i4Fsbgp4TCPAOAuthKeyId ,pSetValFsbgp4TCPAOAuthKeyStopGenerate)
#define nmhSetFsbgp4TCPAOAuthKeyStopAccept(i4Fsbgp4TCPAOAuthPeerType , pFsbgp4TCPAOAuthPeerAddr , i4Fsbgp4TCPAOAuthKeyId ,pSetValFsbgp4TCPAOAuthKeyStopAccept) \
 nmhSetCmnWithLock(Fsbgp4TCPAOAuthKeyStopAccept, 12, Fsbgp4TCPAOAuthKeyStopAcceptSet, BgpLock, BgpUnLock, 0, 0, 3, "%i %s %i %s", i4Fsbgp4TCPAOAuthPeerType , pFsbgp4TCPAOAuthPeerAddr , i4Fsbgp4TCPAOAuthKeyId ,pSetValFsbgp4TCPAOAuthKeyStopAccept)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsBgp4RRDNetworkAddr[11];
extern UINT4 FsBgp4RRDNetworkAddrType[11];
extern UINT4 FsBgp4RRDNetworkPrefixLen[11];
extern UINT4 FsBgp4RRDNetworkRowStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsBgp4RRDNetworkAddrType(pFsBgp4RRDNetworkAddr ,i4SetValFsBgp4RRDNetworkAddrType) \
 nmhSetCmnWithLock(FsBgp4RRDNetworkAddrType, 11, FsBgp4RRDNetworkAddrTypeSet, BgpLock, BgpUnLock, 0, 0, 1, "%s %i", pFsBgp4RRDNetworkAddr ,i4SetValFsBgp4RRDNetworkAddrType)
#define nmhSetFsBgp4RRDNetworkPrefixLen(pFsBgp4RRDNetworkAddr ,i4SetValFsBgp4RRDNetworkPrefixLen) \
 nmhSetCmnWithLock(FsBgp4RRDNetworkPrefixLen, 11, FsBgp4RRDNetworkPrefixLenSet, BgpLock, BgpUnLock, 0, 0, 1, "%s %i", pFsBgp4RRDNetworkAddr ,i4SetValFsBgp4RRDNetworkPrefixLen)
#define nmhSetFsBgp4RRDNetworkRowStatus(pFsBgp4RRDNetworkAddr ,i4SetValFsBgp4RRDNetworkRowStatus) \
 nmhSetCmnWithLock(FsBgp4RRDNetworkRowStatus, 11, FsBgp4RRDNetworkRowStatusSet, BgpLock, BgpUnLock, 0, 1, 1, "%s %i", pFsBgp4RRDNetworkAddr ,i4SetValFsBgp4RRDNetworkRowStatus)

#endif
