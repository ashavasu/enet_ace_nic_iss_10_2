/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: fwlcli.h,v 1.19 2016/02/27 10:05:06 siva Exp $
 *
 * Description: This file contains macros, prototypes, data structures
 *              for firewall CLI  
 ********************************************************************/

#ifndef __FWLCLI_H__
#define __FWLCLI_H__
#include "cli.h"
/* The following are the commands to be used 
 */
#define CLI_SET_FWL_STATUS                      1 
#define CLI_SET_FWL_ICMP_ERROR                  2   
#define CLI_SET_FWL_VERIFY_REV_PATH             3 
#define CLI_SET_FWL_IP_SRC_ROUTE                4 
#define CLI_SET_FWL_INSPECT_TCP                 5 
#define CLI_SET_FWL_TCP_HALF_OPEN               6 
#define CLI_SET_FWL_TCP_SYN_WAIT                7 
#define CLI_SET_FWL_LOGS                        8
#define CLI_SET_FWL_TRACE                       9
#define CLI_SET_FWL_MAX_FILTERS                 10
#define CLI_SET_FWL_MAX_ACC_LISTS               11
#define CLI_ADD_FWL_FILTER                      12
#define CLI_UPDATE_FWL_FILTER                   13
#define CLI_DEL_FWL_FILTER                      14
#define CLI_ADD_FWL_ACCESS_LIST                 15
#define CLI_DEL_FWL_ACCESS_LIST                 16
#define CLI_SET_IP_INSPECT_OPTION               17
#define CLI_SET_ICMP_INSPECT_OPTION             18
#define CLI_SET_IP_FILTER_FRAGMENTS             19
#define CLI_SET_FWL_DMZ                         20
#define CLI_SHOW_FWL_CONFIG                     21
#define CLI_SHOW_FWL_STATS                      22
#define CLI_SHOW_FWL_INT_CONFIG                 23
#define CLI_SHOW_FWL_INT_STATS                  24
#define CLI_SHOW_FWL_FILTERS                    25
#define CLI_SHOW_FWL_ACCESS_LISTS               26
#define CLI_SET_UNTRUSTED_PORT                  27
#define CLI_SHOW_FWL_PORT_FILTERS               28
#define CLI_SHOW_FWL_ADDR_FILTERS               29
#define CLI_SHOW_FWL_LOGS                       30
#define CLI_SHOW_FWL_DMZ                        31
#define CLI_SHOW_FWL_MISC                       32
#define CLI_SET_FWL_NONSTDFTP                   33
#define CLI_SHOW_STATE_FILTER                   34
#define CLI_SHOW_INITFLOW_FILTERS               35 /* FWL_RED */
#define CLI_SHOW_FWL_SESSIONS                   36 /* FWL_RED */
#define CLI_FWL_COMMIT                          37 /* FWL_RED */
#define CLI_SHOW_FWL_URL_FILTERS                38
#define CLI_SET_FWL_URL_FILTERING               39
#define CLI_ADD_URL_FILTER                      40
#define CLI_DEL_URL_FILTER                      41
#define CLI_SET_FWL_IFTYPE                      42
#define CLI_SET_FWL_NETBIOS_FILTERING           43
#define CLI_SET_FWL_INSPECT_ICMP                44
#define CLI_PASS_NETBIOS_LAN2WAN                45
#define CLI_FWL_NEW_CONNECTION                  47 
#define CLI_FWL_DOS_CONFIG                      48
#define CLI_FWL_SHOW_DOS_CONFIG                 49
#define CLI_SEND_EMAIL_ALERT                    50
#define CLI_SET_FWL_INSPECT_UDP                 52 
#define CLI_SET_FWL_UDP_PACKETS                 53
#define CLI_SET_FWL_UDP_WAIT                    54
#define CLI_SET_FWL_ICMP_PACKETS                55
#define CLI_SET_FWL_ICMP_WAIT                   56
#define CLI_SET_FWL_LIMIT_CONCURRENT_SESS       57 
#define CLI_DEL_FWL_DMZ                         58
#define CLI_RESET_UNTRUSTED_PORT                59

  #define CLI_FWL_SHOW_PINHOLE                  60

#define CLI_SET_GBL_THRESHOLD                   61
#define CLI_CLEAR_GBL_STATS                     62
#define CLI_CLEAR_INT_STATS                     63
#define CLI_SHOW_FWL_INT_STATS_ALL              64
#define CLI_CLEAR_FWL_LOGS                      65

#define CLI_SET_FWL_FILTER_ACCOUNTING           66
#define CLI_CLEAR_FWL_FILTER_ACCOUNTING         67
#define CLI_SET_FWL_TRAP                        68

#define CLI_SET_FWL_IP_ADDR_IN_LIST             69
#define CLI_DEL_FWL_IP_ADDR_IN_LIST             70
#define CLI_PURGE_FWL_BLACK_WHITE_LIST          71
#define CLI_FWL_SHOW_BLACK_WHITE_IP_ADDRESS     72
#define CLI_SET_FWL_ICMPv6_ERROR                73
#define CLI_SET_FWL_IPV6_VERIFY_REV_PATH        74
#define CLI_ADD_FWL_IPV6_FILTER                 75
#define CLI_DEL_FWL_IPV6_FILTER                 76
#define CLI_CLEAR_IPV6_GBL_STATS                77
#define CLI_CLEAR_IPV6_INT_STATS                78
#define CLI_SHOW_FWL_IPV6_STATS                 79
#define CLI_SHOW_FWL_IPV6_INT_STATS_ALL         80
#define CLI_SHOW_FWL_IPV6_INT_STATS             81
#define CLI_SET_FWL_IPV6_DMZ                    82
#define CLI_DEL_FWL_IPV6_DMZ                    83
#define CLI_SET_ICMPV6_INSPECT_OPTION           84
#define CLI_SHOW_FWL_IPV6_DMZ                   85
#define CLI_SHOW_FWL_IPV6_FILTERS               86
#define CLI_SET_FWL_LOG_SIZE                    87
#define CLI_SET_FWL_LOG_SIZE_THRESHOLD          88
#define FWL_CLI_MAX_COMMANDS                    89
#define CLI_SET_FWL_DEBUG_LEVEL                 90
#define CLI_RESET_FWL_DEBUG_LEVEL               91
#define CLI_SET_FWL_DOS_ENABLE                  92
#define CLI_SET_FWL_DOS_DISABLE                 93
#define CLI_SET_FWL_RATE_LIMIT                  94
#define CLI_RESET_FWL_RATE_LIMIT                95
#define CLI_SET_FWL_RPF                         96
/* for firewall new connection */

/* #define CLI_FWL_DENY_CONNECTION            "1" */
#define CLI_FWL_TCP_INIT_THRESH_DROP           "2"

/* #define CLI_FWL_DEL_HALF_OPEN_CONNECTION   "2" */
#define CLI_FWL_TCP_INIT_THRESH_DEL_OLD         "1"

/* for firewall send email alerts */
#define CLI_FWL_ABNORMAL_TCP_FLAGS "1"
#define CLI_FWL_DOS_ATTACK         "2"
#define CLI_FWL_BLOCKED_SITE       "3"

/* for firewall schedule for ACL */

#define CLI_FWL_ACL_SUNDAY_MASK    0x01
#define CLI_FWL_ACL_MONDAY_MASK    0x02
#define CLI_FWL_ACL_TUESDAY_MASK   0x04
#define CLI_FWL_ACL_WEDNESDAY_MASK 0x08
#define CLI_FWL_ACL_THURSDAY_MASK  0x10
#define CLI_FWL_ACL_FRIDAY_MASK    0x20
#define CLI_FWL_ACL_SATURDAY_MASK  0x40
#define CLI_FWL_ACL_EVERYDAY_MASK  0x80 

#define CLI_FWL_ACL_SCHED_WHOLEDAY    "61"

#define CLI_FWL_DOS_SESSION_PER_MINUTE  "1"
#define CLI_FWL_DOS_INCOMPLETE          "2"
#define CLI_FWL_DOS_PER_NODE            "3"

/* firewall acl direction */
#define CLI_FWL_ACL_DIR_IN      "1"
#define CLI_FWL_ACL_DIR_OUT     "2"

/* firewall acl action */
#define CLI_FWL_ACL_PERMIT      "1"
#define CLI_FWL_ACL_REJECT     "2"

/* firewall interface type */
#define CLI_FWL_IFC_INTERNAL   "1"
#define CLI_FWL_IFC_EXTERNAL   "2"

/* firewall inspect ip options */
#define CLI_FWL_IP_OPTIONS_SOURCE_ROUTE   "1"
#define CLI_FWL_IP_OPTIONS_RECORD_ROUTE   "2"
#define CLI_FWL_IP_OPTIONS_TIMESTAMP      "3"
#define CLI_FWL_IP_OPTIONS_ANY            "4"
#define CLI_FWL_IP_OPTIONS_NONE           "5"
#define CLI_FWL_IP_OPTIONS_TRACE_ROUTE    "6"

/* firewall inspect fragments */
#define CLI_FWL_TINY_FRAGMENT              "1"
#define CLI_FWL_LARGE_FRAGMENT             "2"
#define CLI_FWL_ANY_FRAGMENT               "3"
#define CLI_FWL_NO_FRAGMENT                "4"

/* firewall inspect icmp type */
#define CLI_FWL_ICMP_TYPE_NONE            "255"

/* firewall inspect icmp code */
#define CLI_FWL_ICMP_CODE_NONE            "255"

/*Firewall Inspect ICMP Type -Echo Request*/
#define CLI_FWL_ECHO_REQUEST               "8"

/* firewall inspect icmpv6 message type */
#define CLI_FWL_ICMPV6_NO_INSPECT                 0x00000000
#define CLI_FWL_ICMPV6_INSPECT_ALL                0x000003FF
#define CLI_FWL_ICMPV6_DESTINATION_UNREACHABLE    0x00000001
#define CLI_FWL_ICMPV6_TIME_EXCEEDED              0x00000002
#define CLI_FWL_ICMPV6_PARAMETER_PROBLEM          0x00000004
#define CLI_FWL_ICMPV6_ECHO_REQUEST               0x00000008
#define CLI_FWL_ICMPV6_ECHO_REPLY                 0x00000010
#define CLI_FWL_ICMPV6_REDIRECT                   0x00000020
#define CLI_FWL_ICMPV6_INFORMATION_REQUEST        0x00000040
#define CLI_FWL_ICMPV6_INFORMATION_REPLY          0x00000080


#define CLI_FWL_ENABLE                     "1"
#define CLI_FWL_DISABLE                    "2"

/*RPF check mode*/

#define CLI_FWL_RPF_DISABLE     0
#define CLI_FWL_RPF_LOOSE       1
#define CLI_FWL_RPF_STRICT      2

/*Dos attack */

#define CLI_FWL_DOS_REDIRECT      1
#define CLI_FWL_DOS_SMURF         2
#define CLI_FWL_DOS_LAND          3
#define CLI_FWL_DOS_SHORT_HEADER  4
#define CLI_FWL_DOS_SNORK         6
/*Rate Limit */

#define CLI_FWL_RATE_TCP      1
#define CLI_FWL_RATE_UDP      2
#define CLI_FWL_RATE_ICMP     3

/*Traffic Mode*/

#define CLI_FWL_RATE_PPS      1
#define CLI_FWL_RATE_KBPS     2
#define CLI_FWL_RATE_BPS      3

/* Firewall interface types : external or internal */
#define CLI_FWL_INTERNAL_IF     1
#define CLI_FWL_EXTERNAL_IF     2

/*Default value of DNS Query Timeout is 30.Here it is set with 60*/
#define  CLI_DNS_QUERY_TO      60

/* RK ADD */
/* Firewall Log messages */
#define CLI_FWL_LOG_NONE       "0"          
#define CLI_FWL_LOG_BRIEF      "1"          
#define CLI_FWL_LOG_DETAIL     "2"          

#define CLI_FWL_ENABLE_NETBIOS   1
#define CLI_FWL_DISABLE_NETBIOS  2

#define FWL_CLI_NO_MEM_MSG_LEN                  30

#define CLI_FWL_TCP_ACK_ESTABLISH      1
#define CLI_FWL_TCP_RESET              2
#define FIREWALL_CLI_MODE             "firewall"

#define CLI_BLK_LIST                   1
#define CLI_WHITE_LIST                 2
#define LIST_CLI_MODE                 "list"

/* Maximum number of Global configuration objects of FIREWALL */
#define FWL_CLI_MAX_GLOBAL_CONFIG_OBJECTS       20

#define  FWL_INET_ATON(pc1Addr,pAddr) UtlInetAton((const CHR1 *)pc1Addr,pAddr)
#define FWL_GLOB_CONFIG_SIZE (CLI_MAX_HEADER_SIZE + \
                             CLI_MAX_COLUMN_WIDTH * \
                             FWL_CLI_MAX_GLOBAL_STATS_OBJECTS)

/* Maximum number of Global configuration objects of FIREWALL */
#define FWL_CLI_MAX_GLOBAL_STATS_OBJECTS      10

#define FWL_GLOB_STATS_SIZE (CLI_MAX_HEADER_SIZE + \
                             CLI_MAX_COLUMN_WIDTH * \
                             FWL_CLI_MAX_GLOBAL_STATS_OBJECTS)

/* Maximum number of interface configuration objects of FIREWALL */
#define FWL_CLI_MAX_INT_CONFIG_OBJECTS      10

#define FWL_INT_CONFIG_SIZE (CLI_MAX_HEADER_SIZE + \
                             CLI_MAX_COLUMN_WIDTH * \
                             FWL_CLI_MAX_INT_CONFIG_OBJECTS)

/* Maximum number of interface configuration objects of FIREWALL */
#define FWL_CLI_MAX_INT_STATS        10

#define FWL_INT_STATS_SIZE (CLI_MAX_HEADER_SIZE + \
                             CLI_MAX_COLUMN_WIDTH * \
                             FWL_CLI_MAX_INT_STATS)

#define FWL_MAX_FILTERS_SIZE (CLI_MAX_HEADER_SIZE + \
                              CLI_MAX_COLUMN_WIDTH * \
                              FWL_MAX_NUM_OF_FILTERS)

#define FWL_MAX_STATE_TABLE_SIZE    (CLI_MAX_HEADER_SIZE + \
                                     FWL_MAX_STATE_ENTRIES * \
                                     CLI_MAX_COLUMN_WIDTH) 


#define FWL_DMZ_DET_SIZE (CLI_MAX_HEADER_SIZE + \
                            CLI_MAX_COLUMN_WIDTH + \
                            FWL_CLI_MAX_DMZ_HOSTS)

#define FWL_MAX_DMZ_SIZE (CLI_MAX_HEADER_SIZE + \
                              CLI_MAX_COLUMN_WIDTH * \
                              FWL_MAX_NUM_OF_IF)

#define FWLCLI_GERI_LAN_IP_ADDR(x,y)    ((x & 0xffffff00) | (y & 0x000000ff))


#define CLI_FWL_MAX_UNTRUST_PORT_STR_LEN  128
#define CLI_FWL_UNIT_POSITION  10

enum {
    CLI_FWL_DEF_PRIORITY_RANGE_CLASH = 1,
    CLI_FWL_NO_SUCH_ACL,
    CLI_FWL_RPF_ALREADY_ENABLED,
    CLI_FWL_RPF_NOT_ENABLED,
    CLI_FWL_RPF_NOT_VALID,
    CLI_FWL_REFERENCED_FILTER,
    CLI_FWL_NO_SUCH_IF,
    CLI_FWL_INCONSIST_VALUE,
    CLI_FWL_INVALID_CLEAR_FLAG,
    CLI_FWL_INVALID_THRESHOLD,
    CLI_FWL_ACL_ALREADY_PRESENT,
    CLI_FWL_ACL_TABLE_FULL,
    CLI_FWL_INVALID_OPERATOR,
    CLI_FWL_INVALID_FILTER_COMBINATION,
    CLI_FWL_PORT_MISMATCH,
    CLI_FWL_FILTER_NOT_ACTIVE,
    CLI_FWL_MAX_FILTER,
    CLI_FWL_INVALID_UCAST_ADDR,
    CLI_FWL_UNSUPP_ADD_FLY_ERR,
    CLI_FWL_ERR_INVALID_BLACK_WHITE_LIST,
    CLI_FWL_ERR_EXISTING_ENTRY,
    CLI_FWL_ERR_INCONSISTENT_ENTRY,
    CLI_FWL_MAX_ERR
};
#ifdef __FWLCLI_C__
CONST CHR1  *FwlCliErrString [] = {
    NULL,
    "% The priority value clashes with the range of default rule priorites\r\n",
    "% Filter does not exist !\r\n",
    "% Already uRPF is enabled in this interface !\r\n",
    "% RPF is not configured on this interface!\r\n",
    "% Invalid RPF mode!\r\n",
    "% Filter is referred by Access List !\r\n",
    "% This is not a WAN Interface.Interface Statistics will be cleared for Global (or) WAN Interfaces\n\r\n",
    "% Object cannot be modified right now !\r\n",
    "% Invalid Clear Flag.Statistics Clear Flag can be Enable(1) or Disable(2)\r\n",
    "% Invalid Threshold Value.Threshold can be from 50 - 50000\r\n",
    "% ACL with same name is configured in the other direction.Cannot create the specified ACL!!\r\n",
    "% Max Access Lists configured. Cannot create a new ACL !\r\n",
    "% Filter combination should be Filter1,Filter2 or Filter1&Filter2 !\r\n",
    "% Check the filter set specified !\r\n",
    "% Mismatch in the port ranges for the filters in the filter set !\r\n",
    "% Firewall not Active !\r\n",
    "% No of filters in filterset has exceeded the maximum allowed filters !\r\n",
    "% Invalid Unicast IP Address !\r\n",
    "% Unsupported address family value !\r\n",
    "% Invalid BlackList/WhiteList Entry !\r\n",
    "% Entry already exists ! \r\n",
    "% Entry not found ! \r\n"
};
#endif /* __FWLCLI_C__ */

/* Macros for configuring timeout intervals for : 
 *  - Idle timer
 *  - TCP establising, established and closing states 
 *  - UDP and ICMP lifetime
 */
enum
{
   FWL_CFG_IDLE_TIMER_VALUE=0,
   FWL_CFG_TCP_INIT_TIMEOUT,
   FWL_CFG_TCP_EST_TIMEOUT,
   FWL_CFG_TCP_FIN_TIMEOUT,
   FWL_CFG_UDP_TIMEOUT,
   FWL_CFG_ICMP_TIMEOUT
};

typedef struct _FwlFilterEntry
{
   UINT1 au1FilterName[FWL_MAX_FILTER_NAME_LEN];
   UINT1 au1SrcAddress[FWL_MAX_ADDR_LEN];
   UINT1 u1Padding1;
   UINT1 au1DestAddress[FWL_MAX_ADDR_LEN];
   UINT1 u1Padding2;
   UINT4 u4IsProtoPres; 
   UINT4 u4Protocol; 

   UINT4 u4SrcPortPres;
   UINT1 au1SrcPortRange[FWL_MAX_PORT_LEN];
   UINT4 u4DestPortPres;
   UINT1 au1DestPortRange[FWL_MAX_PORT_LEN];
   UINT4 u4EsPres;
   UINT4 u4Established;
   UINT4 u4RstPres;
   UINT4 u4Reset;
   UINT4 u4IsDscpPres;
   UINT4 u4IsFlowIdPres;
   UINT4 u4FlowId;
   INT4  i4Dscp;

}tFwlFilterEntry;

typedef struct _FwlAccessList
{
   UINT4 u4Interface;
   UINT4 u4Direction;
   UINT4 u4Permit;
   UINT4 u4Priority;
   UINT4 u4LogTrigger;
   UINT4 u4FragAction;
   UINT1 au1AclName[FWL_MAX_ACL_NAME_LEN];
   UINT1 au1FilterComb[FWL_MAX_FILTER_SET_LEN];
}tFwlAccessList;

typedef struct _CliFwlShowConfig
{
   INT4  i4Status;
   INT4  i4IcmpError;
   INT4  i4IcmpType;
   INT4  i4Icmpv6Error;
   INT4  i4IpSpoof;
   INT4  i4Ipv6Spoof;
   INT4  i4NetBios;
   INT4  i4SourceRoute;
   INT4  i4TcpSyn;
   INT4  i4Trace;
   INT4  i4MaxFilters;
   INT4  i4MaxAcl;
   INT4  i4SynWait;
   INT4  i4SynAllow;
   INT4  i4TrapThreshold;
   INT4  i4TrapStatus;
   UINT4 u4FwlUrlFilterStatus;
   INT4  i4RetValFwlIfIpOptions;
   INT4  i4RetValFwlIcmpv6MsgType;
   INT4  i4RetValFwlIfFragments; 
   UINT2 u2MaxFragmentSize;
   UINT2 u2Reserved;

}tCliFwlShowConfig;

typedef struct _CliFwlShowStats
{
    UINT4  u4PktsCount;
    UINT4  u4PktsDenied;
    UINT4  u4PktsAccepted;
    UINT4  u4IcmpDenied;
    UINT4  u4SynDenied;
    UINT4  u4SpoofsDenied;
    UINT4  u4SrcRouteDenied;
    UINT4  u4TinyFragmentDenied;
    UINT4  u4LargeFragmentDenied;
    UINT4  u4TotalFragmentDenied;
    UINT4  u4IpOptionsDenied;
    UINT4  u4AttacksDenied;
    UINT4  u4IPv6PktsCount;
    UINT4  u4IPv6PktsDenied;
    UINT4  u4IPv6PktsAccepted;
    UINT4  u4IPv6IcmpDenied;
    UINT4  u4IPv6SpoofDenied;
    UINT4  u4IPv6AttackPktsDenied;

}tCliFwlShowStats;

typedef struct _CliFwlShowMisc
{
   INT4  i4DmzHostIp;
   INT4  i4IcmpInspectRequest;
   INT4  i4NonStdFtpPort;
}tCliFwlShowMisc;

INT4 FwlCliSetFwlStatus             PROTO ((tCliHandle CliHandle, 
                                            UINT4 u4FwlStatus));
INT4 FwlCliSetIcmpErrorMessage      PROTO ((tCliHandle CliHandle, 
                                            UINT4 u4IcmpError));
INT4 FwlCliSetIcmpv6ErrorMessage      PROTO ((tCliHandle CliHandle,
                                              UINT4 u4IcmpError));
INT4 FwlCliSetFwlDosAttack          PROTO ((tCliHandle CliHandle, 
                                            INT4 i4AttackType, INT4 i4AttackValue,
           INT4 i4HeaderLength ,INT4 i4PortNo));
INT4 FwlCliSetFwlRateLimit         PROTO ((tCliHandle CliHandle, 
                                            INT4 i4ProtoType, INT4 i4RateLimit,
           INT4 i4PortNo ,
           INT4 i4BurstSize , 
              INT4 i4TrafficMode));
INT4 FwlCliReSetFwlRateLimit         PROTO ((tCliHandle CliHandle, 
                                            INT4 i4ProtoType,INT4 i4PortNo));
INT4 FwlCliSetFwlDosAttackRedirect   PROTO ((tCliHandle CliHandle, 
                                            INT4 i4AttackValue));
INT4 FwlCliSetFwlDosAttackSmurf   PROTO ((tCliHandle CliHandle, 
                                            INT4 i4AttackValue));
INT4 FwlCliSetFwlDosLandAttack    PROTO ((tCliHandle CliHandle, 
                                            INT4 i4AttackValue));
INT4 FwlCliSetFwlDosShortHeader   PROTO ((tCliHandle CliHandle, 
                                            INT4 i4AttackValue, INT4 i4HeaderLength));
INT4 FwlCliSetFwlDosSnorkAttack   PROTO ((tCliHandle CliHandle, 
                                            INT4 i4AttackValue, INT4 i4Port));
INT4 FwlCliSetFwlDosSnorkAttackEnable   PROTO ((tCliHandle CliHandle, 
                                             INT4 i4Port));
INT4 FwlCliSetFwlDosSnorkAttackDisable   PROTO ((tCliHandle CliHandle, 
                                             INT4 i4Port));
INT4 FwlCliSetIPSpoofing            PROTO ((tCliHandle CliHandle, 
                                            UINT4 u4VerifyRevPath));

INT4 FwlCliSetRpfCheck            PROTO ((tCliHandle CliHandle, 
                                            INT4 i4RpfCheck , INT4 i4RpfMode));

INT4 FwlCliSetFwlRpfCheckEnable            PROTO ((tCliHandle CliHandle, 
                                            INT4 i4IfIndex,INT4 i4RpfMode));

INT4 FwlCliSetFwlRpfCheckDisable            PROTO ((tCliHandle CliHandle, 
                                            INT4 i4IfIndex,INT4 i4RpfMode));

INT4 FwlCliSetIPv6Spoofing            PROTO ((tCliHandle CliHandle,
                                            UINT4 u4VerifyRevPath));
INT4 FwlCliSetIPSourceRoute         PROTO ((tCliHandle CliHandle, 
                                            UINT4 u4SrcRoute));
INT4 FwlCliSetTcpIntercept          PROTO ((tCliHandle CliHandle, 
                                            UINT4 u4InspectTcp));
INT4 FwlCliSetTcpInterceptThreshold PROTO ((tCliHandle CliHandle, 
                                            UINT4 u4SynPackets));
INT4 FwlCliSetTcpSynWait            PROTO ((tCliHandle CliHandle, 
                                            UINT4 u4SynWait));

INT4  FwlCliSetFwlLogs              PROTO ((tCliHandle CliHandle,
                                            UINT4 u4Status,
                                            UINT4 u4LogServAddr,
                                            UINT4 u4LogServerPres));

INT4 FwlCliSetFwlTrace              PROTO ((UINT4 u4Trace));
INT4 FwlCliSetMaxFilters            PROTO ((tCliHandle CliHandle, 
                                            UINT4 u4MaxFilters));
INT4 FwlCliSetMaxAccessLists        PROTO ((tCliHandle CliHandle, 
                                            UINT4 u4MaxAccLists));
INT4 FwlCliDeleteFilter             PROTO ((tCliHandle CliHandle, 
                                            UINT1 *pu1FilterName));
INT4 FwlCliSetFwlFilterAccounting   PROTO ((tCliHandle CliHandle, 
                                            UINT1 *pu1FilterName,
                                            UINT4 u4FilterAccounting));
INT4 FwlCliSetLogFileSize           PROTO ((tCliHandle CliHandle, 
                                            UINT4 u4FwlLogFileSize));
INT4 FwlCliSetLogSizeThreshold      PROTO ((tCliHandle CliHandle,
                                            UINT4 u4FwlLogSizeThreshold));
INT4 FwlCliClearFwlFilterAccounting PROTO ((tCliHandle CliHandle, 
                                            UINT1 *pu1FilterName));
INT4 FwlCliShowAccessLists          PROTO ((tCliHandle CliHandle));
INT4 FwlCliShowFwlFilters           PROTO ((tCliHandle CliHandle));
INT4 FwlCliShowFwlIPv6Filters       PROTO ((tCliHandle CliHandle));
INT4 FwlCliShowInterfaceStats       PROTO ((tCliHandle CliHandle));
INT4 FwlCliShowIPv6InterfaceStats   PROTO ((tCliHandle CliHandle));
INT4 FwlCliShowFwlGlobalConfig      PROTO ((tCliHandle CliHandle));
INT4 FwlCliShowFwlGlobalStats       PROTO ((tCliHandle CliHandle)); 
INT4 FwlCliShowFwlIPv6GlobalStats   PROTO ((tCliHandle CliHandle));
INT4 FwlCliShowLogs                 PROTO ((tCliHandle CliHandle));
INT4 FwluCliShowLogs                PROTO ((tCliHandle CliHandle));

INT4 FwlShowLogs                    PROTO ((tCliHandle CliHandle));
INT4 FwlCliShowInterfaceConfig      PROTO ((tCliHandle CliHandle));
INT4 FwlCliFormatStatus             PROTO ((tCliHandle CliHandle,
                                            INT4 i4Status, 
                                            CONST CHR1 *pu1String));
INT4 FwlCliSetIpOption              PROTO ((tCliHandle CliHandle,
                                            UINT4 u4Interface, 
                                            UINT4 u4IpOption));
INT4 FwlCliSetIcmpOption            PROTO ((tCliHandle CliHandle,
                                            UINT4 u4Interface,
                                            UINT4 u4IcmpType,
                                            UINT4 u4IcmpCode));
INT4 FwlCliSetIcmpv6Option          PROTO ((tCliHandle CliHandle,
                                            UINT4 u4Interface,
                                            INT4 i4IcmpType));
INT4 FwlCliSetFragOption            PROTO ((tCliHandle CliHandle, 
                                            UINT4 u4Interface,
                                            UINT4 u4Fragment,
                                            UINT4 u4FragmentSize));
INT4 FwlCliDeleteAccessList         PROTO  ((tCliHandle CliHandle, 
                                             UINT4 u4Interface,
                                             UINT1 *pu1AclName,
                                             UINT4 u4Direction));
INT4 FwlCliAddAccessList            PROTO ((tCliHandle CliHandle, 
                                           tFwlAccessList *pFwlAccessList));
INT4 FwlCliUpdateFilter             PROTO ((tCliHandle CliHandle, 
                                            tFwlFilterEntry *pFwlFilterEntry));
INT4 FwlCliAddFilter                PROTO ((tCliHandle CliHandle,
                                            tFwlFilterEntry *pFwlFilterEntry));
INT4 FwlCliAddIPv6Filter            PROTO ((tCliHandle CliHandle,
                                            tFwlFilterEntry *pFwlFilterEntry));
INT4 FwlCliFormatIcmpType           PROTO ((tCliHandle CliHandle,
                                            UINT1 u1IcmpType));
INT4 FwlCliFormatFilterProtocol     PROTO ((tCliHandle CliHandle,
                                            UINT1 u1Protocol));
INT4 FwlCliFormatIcmpCode           PROTO ((tCliHandle CliHandle,
                                            UINT1 u1IcmpCode));
INT4 FwlCliSetUntrustedPort         PROTO ((tCliHandle CliHandle,
                                            UINT4 u4Interface,
                                            INT4 i4Threshold));
INT4 FwlCliReSetUntrustedPort         PROTO ((tCliHandle CliHandle,
                                            UINT4 u4Interface));
INT4 FwlCliFormatShowAddressFilters PROTO ((tCliHandle CliHandle));
INT4 FwlCliFormatShowAddressIPv6Filters PROTO ((tCliHandle CliHandle));
INT4 FwlCliFormatShowInterfaceStats PROTO ((tCliHandle CliHandle,
                                            UINT4 u4Interface ,
                                            UINT1 *pu1IfName));
INT4 FwlCliFormatShowIPv6InterfaceStats PROTO ((tCliHandle CliHandle,
                                                UINT4 u4Interface,
                                                UINT1 *pu1IfName));
INT4 FwlCliFormatShowGlobalStats    PROTO ((tCliHandle CliHandle,
                                            tCliFwlShowStats 
                                            *pCliFwlGlobalStats));
INT4 FwlCliFormatShowIPv6GlobalStats PROTO ((tCliHandle CliHandle,
                                             tCliFwlShowStats
                                             *pCliFwlGlobalStats));
INT4 FwlCliFormatShowConfig         PROTO ((tCliHandle CliHandle,
                                            tCliFwlShowConfig 
                                           *pCliFwlShowConfig));

INT4 FwlCliSetDmzHost               PROTO ((tCliHandle CliHandle,
                                            UINT4 u4Interface));
INT4 FwlCliSetDmzIPv6Host           PROTO ((tCliHandle CliHandle,
                                            tIp6Addr DmzIp6Addr));

INT4 FwlCliDeleteDmzHost            PROTO ((tCliHandle CliHandle ,UINT4 u4FwlDmzIp));
INT4 FwlCliDeleteDmzIPv6Host        PROTO ((tCliHandle CliHandle,
                                            tIp6Addr DmzIp6Addr));
INT4 FwlCliShowDmzHost              PROTO ((tCliHandle CliHandle));
INT4 FwlCliShowIPv6DmzHost          PROTO ((tCliHandle CliHandle));
INT4 FwlCliFormatDmzEntry           PROTO ((tCliHandle CliHandle,
                                            UINT4 u4Iface));
INT4 FwlShowStatefulTable           PROTO ((tCliHandle CliHandle));
INT4 FwlShowInitFlowTable           PROTO ((tCliHandle CliHandle));
INT4 FwlShowSessions                PROTO ((tCliHandle CliHandle));
INT4 FwlCliFormatShowMisc           PROTO ((tCliHandle CliHandle,
                                            tCliFwlShowMisc *pCliFwlShowMisc));
extern INT1 nmhGetIfIpAddr          PROTO((INT4 , UINT4 *));
INT4 cli_process_fwl_cmd            PROTO ((tCliHandle CliHandle,
                                            UINT4 u4Command, ...));
INT4 FwlSetNetbiosFiltering         PROTO ((UINT1 u1NetBiosFlag));
INT4 FwlCliCommit                   PROTO ((VOID));
INT4 FwlCliCommitInKernel           PROTO ((VOID));
INT4 FwlCliSetTimeoutConfigParam    PROTO ((tCliHandle CliHandle,
                                            UINT1 u1TimeoutConfigParam, 
                                            UINT4 u4TimeoutVal));
INT4 FwlCliShowTimeoutConfigParam   PROTO ((tCliHandle CliHandle));
INT4 FwlCliSetUdpInspectCount       PROTO ((UINT2 u2UdpCount));
INT4 FwlCliSetIcmpInspectCount      PROTO ((UINT2 u2IcmpCount));
INT4 cli_get_fwl_filter_protocol    PROTO ((UINT1* ));
VOID FwlDeleteLogs                  PROTO ((UINT1 *pFile));
PUBLIC VOID FwlUtilShowAllFirewallLogs     PROTO ((UINT1 *pFile));
INT4 FwlCliClearLogs                PROTO ((VOID)); 
INT1 FwlGetFwlConfigPrompt          PROTO ((INT1 *pi1ModeName, 
                                            INT1 *pi1DispStr));
INT4 FwlCliAddDefaultAcl            PROTO ((tCliHandle CliHandle));

INT4 FwlCliSetPassNetbiosLan2Wan PROTO ((tCliHandle CliHandle, UINT4 u4FwlNetBiosLan2WanStat));

/* Added for the show running config */
INT4 FwlShowRunningConfig PROTO ((tCliHandle CliHandle));
INT4 FwlShowRunningConfigScalar PROTO ((tCliHandle CliHandle));
INT4 FwlShowRunningConfigTabular PROTO ((tCliHandle CliHandle));

/* Added BalckList & WhiteList Ip address support */
INT1 FwlGetBlkWhiteConfigPrompt     PROTO ((INT1 *pi1ModeName,
                                            INT1 *pi1DispStr));
INT4 FwlCliConfIPAddressInBlkList PROTO ((tCliHandle CliHandle, 
                                          tFwlIpAddr *pAddr,
                                          UINT2 u2PrefixLen,
                                          UINT1 u1Action));
INT4 FwlCliConfIPAddressInWhiteList PROTO ((tCliHandle CliHandle, 
                                            tFwlIpAddr *pAddr,
                                            UINT2 u2PrefixLen,
                                            UINT1 u1Action));
INT4 FwlCliPurgeBlackWhiteListDataBase PROTO ((tCliHandle CliHandle, 
                                               UINT4 u4ListType));
INT4 FwlCliShowBlackWhiteListDataBase PROTO ((tCliHandle CliHandle,
                                              UINT4 u4ListType,
                                              INT1 i1ListHitCount));

/* Added for supporting URL filters */

INT4 FwlCliSetUrlFiltering PROTO ((tCliHandle CliHandle, INT4 i4UrlFilteringStatus));

INT4 FwlCliAddUrlFilter    PROTO ((tCliHandle CliHandle,UINT1 *pu1FilterName));
    
INT4 FwlCliDeleteUrlFilter PROTO ((tCliHandle CliHandle ,UINT1 *pu1FilterName));

INT4 FwlCliShowUrlFilters PROTO((tCliHandle CliHandle));

INT4 FwlCliClearGlobalStats PROTO((VOID));
INT4 FwlCliClearIPv6GlobalStats PROTO((VOID));
INT4 FwlCliClearIntfStats PROTO((UINT4 u4IfIndex));
INT4 FwlCliClearIntfIPv6Stats PROTO((UINT4 u4IfIndex));
INT4 FwlCliSetGblThreshold PROTO((INT4 i4Threshold));
INT4 FwlCliSetGblTrap PROTO((INT4 i4FwlGlobalTrap));
INT4 FwlCliGetStatefulTableEntries PROTO ((UINT4 u4HashIndex, 
         tStatefulSessionNode ** ppStateFilterInfo));
INT4 FwlCliShowHiddenPartialEntry PROTO ((tCliHandle CliHandle));

VOID PrintFwlPinHoles (tCliHandle , tStatefulSessionNode *);
VOID PrintFwlPinHolesHdr (tCliHandle CliHandle);
#endif
