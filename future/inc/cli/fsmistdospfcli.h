/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmistdospfcli.h,v 1.3 2013/06/15 13:37:43 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIStdOspfContextId[12];
extern UINT4 FsMIStdOspfRouterId[12];
extern UINT4 FsMIStdOspfAdminStat[12];
extern UINT4 FsMIStdOspfASBdrRtrStatus[12];
extern UINT4 FsMIStdOspfTOSSupport[12];
extern UINT4 FsMIStdOspfExtLsdbLimit[12];
extern UINT4 FsMIStdOspfMulticastExtensions[12];
extern UINT4 FsMIStdOspfExitOverflowInterval[12];
extern UINT4 FsMIStdOspfDemandExtensions[12];
extern UINT4 FsMIStdOspfStatus[12];
extern UINT4 FsMIStdOspfAreaId[11];
extern UINT4 FsMIStdOspfImportAsExtern[11];
extern UINT4 FsMIStdOspfAreaSummary[11];
extern UINT4 FsMIStdOspfAreaStatus[11];
extern UINT4 FsMIStdOspfStubAreaId[11];
extern UINT4 FsMIStdOspfStubTOS[11];
extern UINT4 FsMIStdOspfStubMetric[11];
extern UINT4 FsMIStdOspfStubStatus[11];
extern UINT4 FsMIStdOspfStubMetricType[11];
extern UINT4 FsMIStdOspfHostIpAddress[11];
extern UINT4 FsMIStdOspfHostTOS[11];
extern UINT4 FsMIStdOspfHostMetric[11];
extern UINT4 FsMIStdOspfHostStatus[11];
extern UINT4 FsMIStdOspfIfIpAddress[11];
extern UINT4 FsMIStdOspfAddressLessIf[11];
extern UINT4 FsMIStdOspfIfAreaId[11];
extern UINT4 FsMIStdOspfIfType[11];
extern UINT4 FsMIStdOspfIfAdminStat[11];
extern UINT4 FsMIStdOspfIfRtrPriority[11];
extern UINT4 FsMIStdOspfIfTransitDelay[11];
extern UINT4 FsMIStdOspfIfRetransInterval[11];
extern UINT4 FsMIStdOspfIfHelloInterval[11];
extern UINT4 FsMIStdOspfIfRtrDeadInterval[11];
extern UINT4 FsMIStdOspfIfPollInterval[11];
extern UINT4 FsMIStdOspfIfAuthKey[11];
extern UINT4 FsMIStdOspfIfStatus[11];
extern UINT4 FsMIStdOspfIfMulticastForwarding[11];
extern UINT4 FsMIStdOspfIfDemand[11];
extern UINT4 FsMIStdOspfIfAuthType[11];
extern UINT4 FsMIStdOspfIfMetricIpAddress[11];
extern UINT4 FsMIStdOspfIfMetricAddressLessIf[11];
extern UINT4 FsMIStdOspfIfMetricTOS[11];
extern UINT4 FsMIStdOspfIfMetricValue[11];
extern UINT4 FsMIStdOspfIfMetricStatus[11];
extern UINT4 FsMIStdOspfVirtIfAreaId[11];
extern UINT4 FsMIStdOspfVirtIfNeighbor[11];
extern UINT4 FsMIStdOspfVirtIfTransitDelay[11];
extern UINT4 FsMIStdOspfVirtIfRetransInterval[11];
extern UINT4 FsMIStdOspfVirtIfHelloInterval[11];
extern UINT4 FsMIStdOspfVirtIfRtrDeadInterval[11];
extern UINT4 FsMIStdOspfVirtIfAuthKey[11];
extern UINT4 FsMIStdOspfVirtIfStatus[11];
extern UINT4 FsMIStdOspfVirtIfAuthType[11];
extern UINT4 FsMIStdOspfNbrIpAddr[11];
extern UINT4 FsMIStdOspfNbrAddressLessIndex[11];
extern UINT4 FsMIStdOspfNbrPriority[11];
extern UINT4 FsMIStdOspfNbmaNbrStatus[11];
extern UINT4 FsMIStdOspfAreaAggregateAreaID[11];
extern UINT4 FsMIStdOspfAreaAggregateLsdbType[11];
extern UINT4 FsMIStdOspfAreaAggregateNet[11];
extern UINT4 FsMIStdOspfAreaAggregateMask[11];
extern UINT4 FsMIStdOspfAreaAggregateStatus[11];
extern UINT4 FsMIStdOspfAreaAggregateEffect[11];
extern UINT4 FsMIStdOspfIfCryptoAuthType [11];
extern UINT4 FsMIStdOspfVirtIfCryptoAuthType [11];

