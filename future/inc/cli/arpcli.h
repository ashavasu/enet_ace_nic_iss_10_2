 /* $Id: arpcli.h,v 1.1 2012/09/21 10:42:49 siva Exp $ */
#ifndef ARPCLI_H
#define ARPCLI_H
#include "lr.h"
#include "cli.h"

INT4 cli_process_arp_test_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);

#endif
