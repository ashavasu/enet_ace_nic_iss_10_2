/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: aclmrvlcli.h,v 1.5 2013/04/05 13:50:02 siva Exp $
 *
 * Description:This file contains the exported definitions and
 *             macros of ACLMRVL CLI
 *
 *******************************************************************/

#ifndef __ACLMRVLCLI_H__
#define __ACLMRVLCLI_H__
#include "cli.h"
#include "fssnmp.h"

#define ISS_MAX_LEN 255
#define ISS_ADDR_LEN 22
#define HOST_MASK "255.255.255.255"

enum {
    CLI_IP_ACL =1,
    CLI_NO_IP_ACL,
    CLI_MAC_ACL,
    CLI_NO_MAC_ACL,
    CLI_ACL_PERMIT,
    CLI_ACL_DENY,
    CLI_ACL_PERMIT_PROTO,
    CLI_ACL_DENY_PROTO,
    CLI_ACL_PERMIT_TCP,
    CLI_ACL_DENY_TCP,
    CLI_ACL_PERMIT_UDP,
    CLI_ACL_DENY_UDP,
    CLI_ACL_PERMIT_ICMP,
    CLI_ACL_DENY_ICMP,
    CLI_ACL_IP_ACCESS_GRP,
    CLI_ACL_IP_NO_ACCESS_GRP,
    CLI_PERMIT_DST_MAC_ACL,
    CLI_PERMIT_SRC_MAC_ACL,
    CLI_DENY_DST_MAC_ACL,
    CLI_DENY_SRC_MAC_ACL,
    CLI_ACL_MAC_ACCESS_GRP,
    CLI_ACL_MAC_NO_ACCESS_GRP,
    CLI_ACL_PB_PERMIT_PROTO,
    CLI_ACL_PB_DENY_PROTO,
    CLI_ACL_PB_PERMIT_TCP,
    CLI_ACL_PB_DENY_TCP,
    CLI_ACL_PB_PERMIT_UDP,
    CLI_ACL_PB_DENY_UDP,
    CLI_ACL_PB_PERMIT_ICMP,
    CLI_ACL_PB_DENY_ICMP,
    CLI_PB_PERMIT_MAC_ACL,
    CLI_PB_DENY_MAC_ACL,
    CLI_ACL_SHOW, 
    CLI_ACL_MAX_COMMANDS
};

/* Command identifiers for ip access-list command */
#define ACL_STANDARD 1
#define ACL_EXTENDED 2

/* Macro for maximum value of standard access list number */
#define ACL_STANDARD_MAX_VALUE 1000


/* Command identifiers for permit/deny command */
#define ACL_ANY                  1
#define ACL_HOST_IP              2
#define ACL_HOST_IP_MASK         3 

#define ACL_SRC                  1
#define ACL_DST                 2
/* Command identifiers for access list commands */
#define ACL_HOST_MAC             2


#define ISS_PORTLIST_LEN   BRG_PHY_PORT_LIST_SIZE

#define ISS_DEFAULT_INDEX 0
#define DEFAULT_ENABLE 1
#define ISS_ERROR  -1
#define CLI_ACL_MAX_ARGS 25 
#define ACL_MAX_NAME_LENGTH 16
#define  CLI_STDACL_MODE "StdAcl"
#define  CLI_EXTACL_MODE "ExtAcl"
#define  CLI_MACACL_MODE "MacAcl"
#define  CLI_USRDEFACL_MODE "UdbAcl"
#define  CLI_IPV6ACL_MODE    "IPV6Acl"

#define ACL_STANDARD_MIN_VALUE 1


/* Command identifier to distinguish in or out in access group command */
#define AARP                    0x80F3
#define AMBER                   0x6008
#define DEC_SPANNING            0x8138
#define DECNET_IV               0x6003
#define DIAGNOSTIC              0x6005
#define DSM                     0x8039
#define ETYPE_6000              0x6000
#define ETYPE_8042              0x8042
#define LAT                     0x6004
#define LAVC_SCA                0x6007
#define MOP_CONSOLE             0x6002
#define MOP_DUMP                0x6001
#define MSDOS                   0x8041
#define MUMPS                   0x6009
#define NET_BIOS                0x8040
#define VINES_ECHO              0x0BAF
#define VINES_IP                0x0BAD
#define XNS_ID                  0x0807

#define ISS_PROT_ANY       255
#define ISS_PROT_OSPFIGP   89
#define ISS_PROT_PIM       103

/* Command identifier for permit tcp command */
#define ACL_GREATER_THAN_PORT        1
#define ACL_LESSER_THAN_PORT         2
#define ACL_EQUAL_TO_PORT            3
#define ACL_RANGE_PORT               4         

/* Command identifier to distinguish ack or rst in permit tcp command */
#define ACL_ACK                  1
#define ACL_RST                  2
                                                                                                                                                             /* Command identifier to distinguish in or out in access group command */
#define ACL_ACCESS_IN            1
#define ACL_ACCESS_OUT           2


/* Error codes  *
 * Error code values and strings are maintained inside the protocol module itself.
 * */
enum {
        CLI_ACL_ERR = 0,
        CLI_ACL_FILTER_NO_CHANGE,
        CLI_ACL_PROVIDERBRIDGE_ERR,
        CLI_ACL_FILTER_NO_MEMBERS,
        CLI_ACL_FILTER_CREATION_FAILED,
        CLI_ACL_TCP_ERR,
        CLI_ACL_NO_FILTER,
        CLI_ACL_FILTER_EXISTS,
        CLI_ACL_FILTER_MAX,
        CLI_ACL_HW_UPDATE_FAILED,
        CLI_ACL_FILTER_NOT_ACTIVE,
        CLI_ACL_FILTER_DATA_INSUFFICIENCY,
        CLI_ACL_DELETION_FAILED,
        CLI_ACL_FILTER_ACTIVE_FAILED,
        CLI_ACL_IVALID_MAC,
        CLI_ACL_MAX_ERR
};


/* The error strings should be places under the switch so as to avoid redifinition
 *  * This will be visible only in modulecli.c file
 *   */
#ifdef __ACLCLI_C__

CONST CHR1  *AclCliErrString [] = {
        NULL,
        "% This filter is associated with some class-map in Diffserv\r\n",
        "% Bridge is not in provider bridge mode\r\n",
        "% Filter will be activated after any port association.\r\n",
        "% Filter creation failed\r\n",
        "% The Filter port values are out of range.\r\n",
        "% Filter does not exist\r\n",
        "% Filter already exists\r\n",
        "% Number of filter creation reached maximum\r\n",
        "% Hardware Update failed\r\n",
        "% Filter is not active\r\n",
        "% Filter Data is not sufficient\r\n",
        "% Filter Deletion Failed\r\n",
        "% Filter status should be in NotInService\r\n",
        "% Cannot Add Filter for Base Mac Address\r\n",
        "\r\n"
};
#endif


INT4 cli_process_acl_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Comamnd, ...));

INT1 StdAclGetCfgPrompt PROTO ((INT1 *, INT1 *));

INT1 ExtAclGetCfgPrompt PROTO ((INT1 *, INT1 *));

INT1 MacAclGetCfgPrompt PROTO ((INT1 *, INT1 *));

INT4 AclCreateMacFilter PROTO ((tCliHandle , INT4 ));

INT4 AclDestroyMacFilter PROTO ((tCliHandle , INT4 ));

INT4 AclExtDstMacFilterConfig PROTO ((tCliHandle, INT4, tMacAddr, UINT4));

INT4 AclExtSrcMacFilterConfig PROTO ((tCliHandle, INT4, tMacAddr, UINT4));
    
INT4 AclShowAccessLists PROTO ((tCliHandle CliHandle,INT4 i4FilterType,INT4 i4FilterNo));
INT4 AclShowL2Filter PROTO ((tCliHandle CliHandle,INT4 i4NextFilter));

INT4 AclShowRunningConfig(tCliHandle,UINT4);
VOID AclShowRunningConfigTables(tCliHandle);

VOID AclCliPrintPortList(tCliHandle CliHandle,
                         INT4 i4CommaCount, UINT1 * piIfName);

INT1 AclIPv6GetCfgPrompt PROTO ((INT1 *, INT1 *));
INT1 UserDefAclGetCfgPrompt PROTO ((INT1 *, INT1 *));

#ifdef __ACLCLI_C__
#define ACL_PORTS_PER_BYTE 8
UINT1               gau1UtlAclPortBitMask[ACL_PORTS_PER_BYTE] =
    { 0x01, 0x80, 0x40, 0x20, 0x10,
    0x08, 0x04, 0x02
};
#define ACL_ADD_PORT_LIST(au1PortArray, u2Port) \
{\
    UINT2 u2PortBytePos;\
        UINT2 u2PortBitPos;\
        u2PortBytePos = (UINT2)(u2Port / ACL_PORTS_PER_BYTE);\
        u2PortBitPos  = (UINT2)(u2Port % ACL_PORTS_PER_BYTE);\
        if (u2PortBitPos  == 0) {u2PortBytePos -= 1;} \
            au1PortArray[u2PortBytePos] = (UINT1)(au1PortArray[u2PortBytePos] \
                                                       | gau1UtlAclPortBitMask[u2PortBitPos]);\
}
#define ACL_DEL_PORT_LIST(au1PortArray, u2Port) \
{\
    UINT2 u2PortBytePos;\
        UINT2 u2PortBitPos;\
        u2PortBytePos = (UINT2)(u2Port / ACL_PORTS_PER_BYTE);\
        u2PortBitPos  = (UINT2)(u2Port % ACL_PORTS_PER_BYTE);\
        if (u2PortBitPos  == 0) {u2PortBytePos -= 1;} \
          au1PortArray[u2PortBytePos] = (UINT1)(au1PortArray[u2PortBytePos]                                            & ~ gau1UtlAclPortBitMask[u2PortBitPos]);\
}
#endif


#endif
