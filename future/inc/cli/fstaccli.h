/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fstaccli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsTacMcastChannelDefaultBandwidth[11];
extern UINT4 FsTacTraceOption[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsTacMcastChannelDefaultBandwidth(u4SetValFsTacMcastChannelDefaultBandwidth)	\
	nmhSetCmn(FsTacMcastChannelDefaultBandwidth, 11, FsTacMcastChannelDefaultBandwidthSet, TacLock, TacUnLock, 0, 0, 0, "%u", u4SetValFsTacMcastChannelDefaultBandwidth)
#define nmhSetFsTacTraceOption(u4SetValFsTacTraceOption)	\
	nmhSetCmn(FsTacTraceOption, 11, FsTacTraceOptionSet, TacLock, TacUnLock, 0, 0, 0, "%u", u4SetValFsTacTraceOption)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsTacMcastProfileId[13];
extern UINT4 FsTacMcastProfileAddrType[13];
extern UINT4 FsTacMcastProfileAction[13];
extern UINT4 FsTacMcastProfileDescription[13];
extern UINT4 FsTacMcastProfileStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsTacMcastProfileAction(u4FsTacMcastProfileId , i4FsTacMcastProfileAddrType ,i4SetValFsTacMcastProfileAction)	\
	nmhSetCmn(FsTacMcastProfileAction, 13, FsTacMcastProfileActionSet, TacLock, TacUnLock, 0, 0, 2, "%u %i %i", u4FsTacMcastProfileId , i4FsTacMcastProfileAddrType ,i4SetValFsTacMcastProfileAction)
#define nmhSetFsTacMcastProfileDescription(u4FsTacMcastProfileId , i4FsTacMcastProfileAddrType ,pSetValFsTacMcastProfileDescription)	\
	nmhSetCmn(FsTacMcastProfileDescription, 13, FsTacMcastProfileDescriptionSet, TacLock, TacUnLock, 0, 0, 2, "%u %i %s", u4FsTacMcastProfileId , i4FsTacMcastProfileAddrType ,pSetValFsTacMcastProfileDescription)
#define nmhSetFsTacMcastProfileStatus(u4FsTacMcastProfileId , i4FsTacMcastProfileAddrType ,i4SetValFsTacMcastProfileStatus)	\
	nmhSetCmn(FsTacMcastProfileStatus, 13, FsTacMcastProfileStatusSet, TacLock, TacUnLock, 0, 1, 2, "%u %i %i", u4FsTacMcastProfileId , i4FsTacMcastProfileAddrType ,i4SetValFsTacMcastProfileStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsTacMcastPrfFilterGrpStartAddr[13];
extern UINT4 FsTacMcastPrfFilterGrpEndAddr[13];
extern UINT4 FsTacMcastPrfFilterSrcStartAddr[13];
extern UINT4 FsTacMcastPrfFilterSrcEndAddr[13];
extern UINT4 FsTacMcastPrfFilterMode[13];
extern UINT4 FsTacMcastPrfFilterStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsTacMcastPrfFilterMode(u4FsTacMcastProfileId , i4FsTacMcastProfileAddrType , pFsTacMcastPrfFilterGrpStartAddr , pFsTacMcastPrfFilterGrpEndAddr , pFsTacMcastPrfFilterSrcStartAddr , pFsTacMcastPrfFilterSrcEndAddr ,i4SetValFsTacMcastPrfFilterMode)	\
	nmhSetCmn(FsTacMcastPrfFilterMode, 13, FsTacMcastPrfFilterModeSet, TacLock, TacUnLock, 0, 0, 6, "%u %i %s %s %s %s %i", u4FsTacMcastProfileId , i4FsTacMcastProfileAddrType , pFsTacMcastPrfFilterGrpStartAddr , pFsTacMcastPrfFilterGrpEndAddr , pFsTacMcastPrfFilterSrcStartAddr , pFsTacMcastPrfFilterSrcEndAddr ,i4SetValFsTacMcastPrfFilterMode)
#define nmhSetFsTacMcastPrfFilterStatus(u4FsTacMcastProfileId , i4FsTacMcastProfileAddrType , pFsTacMcastPrfFilterGrpStartAddr , pFsTacMcastPrfFilterGrpEndAddr , pFsTacMcastPrfFilterSrcStartAddr , pFsTacMcastPrfFilterSrcEndAddr ,i4SetValFsTacMcastPrfFilterStatus)	\
	nmhSetCmn(FsTacMcastPrfFilterStatus, 13, FsTacMcastPrfFilterStatusSet, TacLock, TacUnLock, 0, 1, 6, "%u %i %s %s %s %s %i", u4FsTacMcastProfileId , i4FsTacMcastProfileAddrType , pFsTacMcastPrfFilterGrpStartAddr , pFsTacMcastPrfFilterGrpEndAddr , pFsTacMcastPrfFilterSrcStartAddr , pFsTacMcastPrfFilterSrcEndAddr ,i4SetValFsTacMcastPrfFilterStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsTacMcastChannelAddressType[13];
extern UINT4 FsTacMcastChannelGrpAddress[13];
extern UINT4 FsTacMcastChannelSrcAddress[13];
extern UINT4 FsTacMcastChannelBandWidth[13];
extern UINT4 FsTacMcastChannelRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsTacMcastChannelBandWidth(i4FsTacMcastChannelAddressType , pFsTacMcastChannelGrpAddress , pFsTacMcastChannelSrcAddress ,u4SetValFsTacMcastChannelBandWidth)	\
	nmhSetCmn(FsTacMcastChannelBandWidth, 13, FsTacMcastChannelBandWidthSet, TacLock, TacUnLock, 0, 0, 3, "%i %s %s %u", i4FsTacMcastChannelAddressType , pFsTacMcastChannelGrpAddress , pFsTacMcastChannelSrcAddress ,u4SetValFsTacMcastChannelBandWidth)
#define nmhSetFsTacMcastChannelRowStatus(i4FsTacMcastChannelAddressType , pFsTacMcastChannelGrpAddress , pFsTacMcastChannelSrcAddress ,i4SetValFsTacMcastChannelRowStatus)	\
	nmhSetCmn(FsTacMcastChannelRowStatus, 13, FsTacMcastChannelRowStatusSet, TacLock, TacUnLock, 0, 1, 3, "%i %s %s %i", i4FsTacMcastChannelAddressType , pFsTacMcastChannelGrpAddress , pFsTacMcastChannelSrcAddress ,i4SetValFsTacMcastChannelRowStatus)

#endif
