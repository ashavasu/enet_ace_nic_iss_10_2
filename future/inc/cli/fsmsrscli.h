/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmsrscli.h,v 1.2 2008/08/28 10:13:03 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDot1dStpVersion[12];
extern UINT4 FsDot1dStpTxHoldCount[12];
extern UINT4 FsDot1dStpPathCostDefault[12];
extern UINT4 FsDot1dStpPortProtocolMigration[12];
extern UINT4 FsDot1dStpPortAdminEdgePort[12];
extern UINT4 FsDot1dStpPortAdminPointToPoint[12];
extern UINT4 FsDot1dStpPortAdminPathCost[12];
