/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdvaccli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 VacmSecurityModel[11];
extern UINT4 VacmSecurityName[11];
extern UINT4 VacmGroupName[11];
extern UINT4 VacmSecurityToGroupStorageType[11];
extern UINT4 VacmSecurityToGroupStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetVacmGroupName(i4VacmSecurityModel , pVacmSecurityName ,pSetValVacmGroupName)	\
	nmhSetCmn(VacmGroupName, 11, VacmGroupNameSet, NULL, NULL, 0, 0, 2, "%i %s %s", i4VacmSecurityModel , pVacmSecurityName ,pSetValVacmGroupName)
#define nmhSetVacmSecurityToGroupStorageType(i4VacmSecurityModel , pVacmSecurityName ,i4SetValVacmSecurityToGroupStorageType)	\
	nmhSetCmn(VacmSecurityToGroupStorageType, 11, VacmSecurityToGroupStorageTypeSet, NULL, NULL, 0, 0, 2, "%i %s %i", i4VacmSecurityModel , pVacmSecurityName ,i4SetValVacmSecurityToGroupStorageType)
#define nmhSetVacmSecurityToGroupStatus(i4VacmSecurityModel , pVacmSecurityName ,i4SetValVacmSecurityToGroupStatus)	\
	nmhSetCmn(VacmSecurityToGroupStatus, 11, VacmSecurityToGroupStatusSet, NULL, NULL, 0, 1, 2, "%i %s %i", i4VacmSecurityModel , pVacmSecurityName ,i4SetValVacmSecurityToGroupStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 VacmAccessContextPrefix[11];
extern UINT4 VacmAccessSecurityModel[11];
extern UINT4 VacmAccessSecurityLevel[11];
extern UINT4 VacmAccessContextMatch[11];
extern UINT4 VacmAccessReadViewName[11];
extern UINT4 VacmAccessWriteViewName[11];
extern UINT4 VacmAccessNotifyViewName[11];
extern UINT4 VacmAccessStorageType[11];
extern UINT4 VacmAccessStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetVacmAccessContextMatch(pVacmGroupName , pVacmAccessContextPrefix , i4VacmAccessSecurityModel , i4VacmAccessSecurityLevel ,i4SetValVacmAccessContextMatch)	\
	nmhSetCmn(VacmAccessContextMatch, 11, VacmAccessContextMatchSet, NULL, NULL, 0, 0, 4, "%s %s %i %i %i", pVacmGroupName , pVacmAccessContextPrefix , i4VacmAccessSecurityModel , i4VacmAccessSecurityLevel ,i4SetValVacmAccessContextMatch)
#define nmhSetVacmAccessReadViewName(pVacmGroupName , pVacmAccessContextPrefix , i4VacmAccessSecurityModel , i4VacmAccessSecurityLevel ,pSetValVacmAccessReadViewName)	\
	nmhSetCmn(VacmAccessReadViewName, 11, VacmAccessReadViewNameSet, NULL, NULL, 0, 0, 4, "%s %s %i %i %s", pVacmGroupName , pVacmAccessContextPrefix , i4VacmAccessSecurityModel , i4VacmAccessSecurityLevel ,pSetValVacmAccessReadViewName)
#define nmhSetVacmAccessWriteViewName(pVacmGroupName , pVacmAccessContextPrefix , i4VacmAccessSecurityModel , i4VacmAccessSecurityLevel ,pSetValVacmAccessWriteViewName)	\
	nmhSetCmn(VacmAccessWriteViewName, 11, VacmAccessWriteViewNameSet, NULL, NULL, 0, 0, 4, "%s %s %i %i %s", pVacmGroupName , pVacmAccessContextPrefix , i4VacmAccessSecurityModel , i4VacmAccessSecurityLevel ,pSetValVacmAccessWriteViewName)
#define nmhSetVacmAccessNotifyViewName(pVacmGroupName , pVacmAccessContextPrefix , i4VacmAccessSecurityModel , i4VacmAccessSecurityLevel ,pSetValVacmAccessNotifyViewName)	\
	nmhSetCmn(VacmAccessNotifyViewName, 11, VacmAccessNotifyViewNameSet, NULL, NULL, 0, 0, 4, "%s %s %i %i %s", pVacmGroupName , pVacmAccessContextPrefix , i4VacmAccessSecurityModel , i4VacmAccessSecurityLevel ,pSetValVacmAccessNotifyViewName)
#define nmhSetVacmAccessStorageType(pVacmGroupName , pVacmAccessContextPrefix , i4VacmAccessSecurityModel , i4VacmAccessSecurityLevel ,i4SetValVacmAccessStorageType)	\
	nmhSetCmn(VacmAccessStorageType, 11, VacmAccessStorageTypeSet, NULL, NULL, 0, 0, 4, "%s %s %i %i %i", pVacmGroupName , pVacmAccessContextPrefix , i4VacmAccessSecurityModel , i4VacmAccessSecurityLevel ,i4SetValVacmAccessStorageType)
#define nmhSetVacmAccessStatus(pVacmGroupName , pVacmAccessContextPrefix , i4VacmAccessSecurityModel , i4VacmAccessSecurityLevel ,i4SetValVacmAccessStatus)	\
	nmhSetCmn(VacmAccessStatus, 11, VacmAccessStatusSet, NULL, NULL, 0, 1, 4, "%s %s %i %i %i", pVacmGroupName , pVacmAccessContextPrefix , i4VacmAccessSecurityModel , i4VacmAccessSecurityLevel ,i4SetValVacmAccessStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 VacmViewSpinLock[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetVacmViewSpinLock(i4SetValVacmViewSpinLock)	\
	nmhSetCmn(VacmViewSpinLock, 10, VacmViewSpinLockSet, NULL, NULL, 0, 0, 0, "%i", i4SetValVacmViewSpinLock)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 VacmViewTreeFamilyViewName[12];
extern UINT4 VacmViewTreeFamilySubtree[12];
extern UINT4 VacmViewTreeFamilyMask[12];
extern UINT4 VacmViewTreeFamilyType[12];
extern UINT4 VacmViewTreeFamilyStorageType[12];
extern UINT4 VacmViewTreeFamilyStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetVacmViewTreeFamilyMask(pVacmViewTreeFamilyViewName , pVacmViewTreeFamilySubtree ,pSetValVacmViewTreeFamilyMask)	\
	nmhSetCmn(VacmViewTreeFamilyMask, 12, VacmViewTreeFamilyMaskSet, NULL, NULL, 0, 0, 2, "%s %o %s", pVacmViewTreeFamilyViewName , pVacmViewTreeFamilySubtree ,pSetValVacmViewTreeFamilyMask)
#define nmhSetVacmViewTreeFamilyType(pVacmViewTreeFamilyViewName , pVacmViewTreeFamilySubtree ,i4SetValVacmViewTreeFamilyType)	\
	nmhSetCmn(VacmViewTreeFamilyType, 12, VacmViewTreeFamilyTypeSet, NULL, NULL, 0, 0, 2, "%s %o %i", pVacmViewTreeFamilyViewName , pVacmViewTreeFamilySubtree ,i4SetValVacmViewTreeFamilyType)
#define nmhSetVacmViewTreeFamilyStorageType(pVacmViewTreeFamilyViewName , pVacmViewTreeFamilySubtree ,i4SetValVacmViewTreeFamilyStorageType)	\
	nmhSetCmn(VacmViewTreeFamilyStorageType, 12, VacmViewTreeFamilyStorageTypeSet, NULL, NULL, 0, 0, 2, "%s %o %i", pVacmViewTreeFamilyViewName , pVacmViewTreeFamilySubtree ,i4SetValVacmViewTreeFamilyStorageType)
#define nmhSetVacmViewTreeFamilyStatus(pVacmViewTreeFamilyViewName , pVacmViewTreeFamilySubtree ,i4SetValVacmViewTreeFamilyStatus)	\
	nmhSetCmn(VacmViewTreeFamilyStatus, 12, VacmViewTreeFamilyStatusSet, NULL, NULL, 0, 1, 2, "%s %o %i", pVacmViewTreeFamilyViewName , pVacmViewTreeFamilySubtree ,i4SetValVacmViewTreeFamilyStatus)

#endif
