/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsy156cli.h,v 1.2 2016/02/29 11:06:10 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsY1564ContextId[13];
extern UINT4 FsY1564ContextSystemControl[13];
extern UINT4 FsY1564ContextModuleStatus[13];
extern UINT4 FsY1564ContextTraceOption[13];
extern UINT4 FsY1564ContextTrapStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsY1564ContextSystemControl(u4FsY1564ContextId ,i4SetValFsY1564ContextSystemControl) \
 nmhSetCmnWithLock(FsY1564ContextSystemControl, 13, FsY1564ContextSystemControlSet, Y1564ApiLock, Y1564ApiUnLock, 0, 0, 1, "%u %i", u4FsY1564ContextId ,i4SetValFsY1564ContextSystemControl)
#define nmhSetFsY1564ContextModuleStatus(u4FsY1564ContextId ,i4SetValFsY1564ContextModuleStatus) \
 nmhSetCmnWithLock(FsY1564ContextModuleStatus, 13, FsY1564ContextModuleStatusSet, Y1564ApiLock, Y1564ApiUnLock, 0, 0, 1, "%u %i", u4FsY1564ContextId ,i4SetValFsY1564ContextModuleStatus)
#define nmhSetFsY1564ContextTraceOption(u4FsY1564ContextId ,u4SetValFsY1564ContextTraceOption) \
 nmhSetCmnWithLock(FsY1564ContextTraceOption, 13, FsY1564ContextTraceOptionSet, Y1564ApiLock, Y1564ApiUnLock, 0, 0, 1, "%u %u", u4FsY1564ContextId ,u4SetValFsY1564ContextTraceOption)
#define nmhSetFsY1564ContextTrapStatus(u4FsY1564ContextId ,i4SetValFsY1564ContextTrapStatus) \
 nmhSetCmnWithLock(FsY1564ContextTrapStatus, 13, FsY1564ContextTrapStatusSet, Y1564ApiLock, Y1564ApiUnLock, 0, 0, 1, "%u %i", u4FsY1564ContextId ,i4SetValFsY1564ContextTrapStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsY1564SlaId[13];
extern UINT4 FsY1564SlaEvcIndex[13];
extern UINT4 FsY1564SlaMEG[13];
extern UINT4 FsY1564SlaME[13];
extern UINT4 FsY1564SlaMEP[13];
extern UINT4 FsY1564SlaSacId[13];
extern UINT4 FsY1564SlaTrafProfileId[13];
extern UINT4 FsY1564SlaStepLoadRate[13];
extern UINT4 FsY1564SlaConfTestDuration[13];
extern UINT4 FsY1564SlaServiceConfId[13];
extern UINT4 FsY1564SlaTrafPolicing[13];
extern UINT4 FsY1564SlaTestSelector[13];
extern UINT4 FsY1564SlaRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsY1564SlaEvcIndex(u4FsY1564ContextId , u4FsY1564SlaId ,i4SetValFsY1564SlaEvcIndex) \
 nmhSetCmnWithLock(FsY1564SlaEvcIndex, 13, FsY1564SlaEvcIndexSet, Y1564ApiLock, Y1564ApiUnLock, 0, 0, 2, "%u %u %i", u4FsY1564ContextId , u4FsY1564SlaId ,i4SetValFsY1564SlaEvcIndex)
#define nmhSetFsY1564SlaMEG(u4FsY1564ContextId , u4FsY1564SlaId ,u4SetValFsY1564SlaMEG) \
 nmhSetCmnWithLock(FsY1564SlaMEG, 13, FsY1564SlaMEGSet, Y1564ApiLock, Y1564ApiUnLock, 0, 0, 2, "%u %u %u", u4FsY1564ContextId , u4FsY1564SlaId ,u4SetValFsY1564SlaMEG)
#define nmhSetFsY1564SlaME(u4FsY1564ContextId , u4FsY1564SlaId ,u4SetValFsY1564SlaME) \
 nmhSetCmnWithLock(FsY1564SlaME, 13, FsY1564SlaMESet, Y1564ApiLock, Y1564ApiUnLock, 0, 0, 2, "%u %u %u", u4FsY1564ContextId , u4FsY1564SlaId ,u4SetValFsY1564SlaME)
#define nmhSetFsY1564SlaMEP(u4FsY1564ContextId , u4FsY1564SlaId ,u4SetValFsY1564SlaMEP) \
 nmhSetCmnWithLock(FsY1564SlaMEP, 13, FsY1564SlaMEPSet, Y1564ApiLock, Y1564ApiUnLock, 0, 0, 2, "%u %u %u", u4FsY1564ContextId , u4FsY1564SlaId ,u4SetValFsY1564SlaMEP)
#define nmhSetFsY1564SlaSacId(u4FsY1564ContextId , u4FsY1564SlaId ,u4SetValFsY1564SlaSacId) \
 nmhSetCmnWithLock(FsY1564SlaSacId, 13, FsY1564SlaSacIdSet, Y1564ApiLock, Y1564ApiUnLock, 0, 0, 2, "%u %u %u", u4FsY1564ContextId , u4FsY1564SlaId ,u4SetValFsY1564SlaSacId)
#define nmhSetFsY1564SlaTrafProfileId(u4FsY1564ContextId , u4FsY1564SlaId ,u4SetValFsY1564SlaTrafProfileId) \
 nmhSetCmnWithLock(FsY1564SlaTrafProfileId, 13, FsY1564SlaTrafProfileIdSet, Y1564ApiLock, Y1564ApiUnLock, 0, 0, 2, "%u %u %u", u4FsY1564ContextId , u4FsY1564SlaId ,u4SetValFsY1564SlaTrafProfileId)
#define nmhSetFsY1564SlaStepLoadRate(u4FsY1564ContextId , u4FsY1564SlaId ,i4SetValFsY1564SlaStepLoadRate) \
 nmhSetCmnWithLock(FsY1564SlaStepLoadRate, 13, FsY1564SlaStepLoadRateSet, Y1564ApiLock, Y1564ApiUnLock, 0, 0, 2, "%u %u %i", u4FsY1564ContextId , u4FsY1564SlaId ,i4SetValFsY1564SlaStepLoadRate)
#define nmhSetFsY1564SlaConfTestDuration(u4FsY1564ContextId , u4FsY1564SlaId ,i4SetValFsY1564SlaConfTestDuration) \
 nmhSetCmnWithLock(FsY1564SlaConfTestDuration, 13, FsY1564SlaConfTestDurationSet, Y1564ApiLock, Y1564ApiUnLock, 0, 0, 2, "%u %u %i", u4FsY1564ContextId , u4FsY1564SlaId ,i4SetValFsY1564SlaConfTestDuration)
#define nmhSetFsY1564SlaServiceConfId(u4FsY1564ContextId , u4FsY1564SlaId ,u4SetValFsY1564SlaServiceConfId) \
 nmhSetCmnWithLock(FsY1564SlaServiceConfId, 13, FsY1564SlaServiceConfIdSet, Y1564ApiLock, Y1564ApiUnLock, 0, 0, 2, "%u %u %u", u4FsY1564ContextId , u4FsY1564SlaId ,u4SetValFsY1564SlaServiceConfId)
#define nmhSetFsY1564SlaTrafPolicing(u4FsY1564ContextId , u4FsY1564SlaId ,i4SetValFsY1564SlaTrafPolicing) \
 nmhSetCmnWithLock(FsY1564SlaTrafPolicing, 13, FsY1564SlaTrafPolicingSet, Y1564ApiLock, Y1564ApiUnLock, 0, 0, 2, "%u %u %i", u4FsY1564ContextId , u4FsY1564SlaId ,i4SetValFsY1564SlaTrafPolicing)
#define nmhSetFsY1564SlaTestSelector(u4FsY1564ContextId , u4FsY1564SlaId ,i4SetValFsY1564SlaTestSelector) \
 nmhSetCmnWithLock(FsY1564SlaTestSelector, 13, FsY1564SlaTestSelectorSet, Y1564ApiLock, Y1564ApiUnLock, 0, 0, 2, "%u %u %i", u4FsY1564ContextId , u4FsY1564SlaId ,i4SetValFsY1564SlaTestSelector)
#define nmhSetFsY1564SlaRowStatus(u4FsY1564ContextId , u4FsY1564SlaId ,i4SetValFsY1564SlaRowStatus) \
 nmhSetCmnWithLock(FsY1564SlaRowStatus, 13, FsY1564SlaRowStatusSet, Y1564ApiLock, Y1564ApiUnLock, 0, 1, 2, "%u %u %i", u4FsY1564ContextId , u4FsY1564SlaId ,i4SetValFsY1564SlaRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsY1564TrafProfId[13];
extern UINT4 FsY1564TrafProfDir[13];
extern UINT4 FsY1564TrafProfPktSize[13];
extern UINT4 FsY1564TrafProfPayload[13];
extern UINT4 FsY1564TrafProfOptEmixPktSize[13];
extern UINT4 FsY1564TrafProfPCP[13];
extern UINT4 FsY1564TrafProfRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsY1564TrafProfDir(u4FsY1564ContextId , u4FsY1564TrafProfId ,i4SetValFsY1564TrafProfDir) \
 nmhSetCmnWithLock(FsY1564TrafProfDir, 13, FsY1564TrafProfDirSet, Y1564ApiLock, Y1564ApiUnLock, 0, 0, 2, "%u %u %i", u4FsY1564ContextId , u4FsY1564TrafProfId ,i4SetValFsY1564TrafProfDir)
#define nmhSetFsY1564TrafProfPktSize(u4FsY1564ContextId , u4FsY1564TrafProfId ,i4SetValFsY1564TrafProfPktSize) \
 nmhSetCmnWithLock(FsY1564TrafProfPktSize, 13, FsY1564TrafProfPktSizeSet, Y1564ApiLock, Y1564ApiUnLock, 0, 0, 2, "%u %u %i", u4FsY1564ContextId , u4FsY1564TrafProfId ,i4SetValFsY1564TrafProfPktSize)
#define nmhSetFsY1564TrafProfPayload(u4FsY1564ContextId , u4FsY1564TrafProfId ,pSetValFsY1564TrafProfPayload) \
 nmhSetCmnWithLock(FsY1564TrafProfPayload, 13, FsY1564TrafProfPayloadSet, Y1564ApiLock, Y1564ApiUnLock, 0, 0, 2, "%u %u %s", u4FsY1564ContextId , u4FsY1564TrafProfId ,pSetValFsY1564TrafProfPayload)
#define nmhSetFsY1564TrafProfOptEmixPktSize(u4FsY1564ContextId , u4FsY1564TrafProfId ,pSetValFsY1564TrafProfOptEmixPktSize) \
 nmhSetCmnWithLock(FsY1564TrafProfOptEmixPktSize, 13, FsY1564TrafProfOptEmixPktSizeSet, Y1564ApiLock, Y1564ApiUnLock, 0, 0, 2, "%u %u %s", u4FsY1564ContextId , u4FsY1564TrafProfId ,pSetValFsY1564TrafProfOptEmixPktSize)
#define nmhSetFsY1564TrafProfPCP(u4FsY1564ContextId , u4FsY1564TrafProfId ,i4SetValFsY1564TrafProfPCP) \
 nmhSetCmnWithLock(FsY1564TrafProfPCP, 13, FsY1564TrafProfPCPSet, Y1564ApiLock, Y1564ApiUnLock, 0, 0, 2, "%u %u %i", u4FsY1564ContextId , u4FsY1564TrafProfId ,i4SetValFsY1564TrafProfPCP)
#define nmhSetFsY1564TrafProfRowStatus(u4FsY1564ContextId , u4FsY1564TrafProfId ,i4SetValFsY1564TrafProfRowStatus) \
 nmhSetCmnWithLock(FsY1564TrafProfRowStatus, 13, FsY1564TrafProfRowStatusSet, Y1564ApiLock, Y1564ApiUnLock, 0, 1, 2, "%u %u %i", u4FsY1564ContextId , u4FsY1564TrafProfId ,i4SetValFsY1564TrafProfRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsY1564SacId[13];
extern UINT4 FsY1564SacInfoRate[13];
extern UINT4 FsY1564SacFrLossRatio[13];
extern UINT4 FsY1564SacFrTransDelay[13];
extern UINT4 FsY1564SacFrDelayVar[13];
extern UINT4 FsY1564SacAvailability[13];
extern UINT4 FsY1564SacRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsY1564SacInfoRate(u4FsY1564ContextId , u4FsY1564SacId ,i4SetValFsY1564SacInfoRate) \
 nmhSetCmnWithLock(FsY1564SacInfoRate, 13, FsY1564SacInfoRateSet, Y1564ApiLock, Y1564ApiUnLock, 0, 0, 2, "%u %u %i", u4FsY1564ContextId , u4FsY1564SacId ,i4SetValFsY1564SacInfoRate)
#define nmhSetFsY1564SacFrLossRatio(u4FsY1564ContextId , u4FsY1564SacId ,i4SetValFsY1564SacFrLossRatio) \
 nmhSetCmnWithLock(FsY1564SacFrLossRatio, 13, FsY1564SacFrLossRatioSet, Y1564ApiLock, Y1564ApiUnLock, 0, 0, 2, "%u %u %i", u4FsY1564ContextId , u4FsY1564SacId ,i4SetValFsY1564SacFrLossRatio)
#define nmhSetFsY1564SacFrTransDelay(u4FsY1564ContextId , u4FsY1564SacId ,i4SetValFsY1564SacFrTransDelay) \
 nmhSetCmnWithLock(FsY1564SacFrTransDelay, 13, FsY1564SacFrTransDelaySet, Y1564ApiLock, Y1564ApiUnLock, 0, 0, 2, "%u %u %i", u4FsY1564ContextId , u4FsY1564SacId ,i4SetValFsY1564SacFrTransDelay)
#define nmhSetFsY1564SacFrDelayVar(u4FsY1564ContextId , u4FsY1564SacId ,i4SetValFsY1564SacFrDelayVar) \
 nmhSetCmnWithLock(FsY1564SacFrDelayVar, 13, FsY1564SacFrDelayVarSet, Y1564ApiLock, Y1564ApiUnLock, 0, 0, 2, "%u %u %i", u4FsY1564ContextId , u4FsY1564SacId ,i4SetValFsY1564SacFrDelayVar)
#define nmhSetFsY1564SacAvailability(u4FsY1564ContextId , u4FsY1564SacId ,pSetValFsY1564SacAvailability) \
 nmhSetCmnWithLock(FsY1564SacAvailability, 13, FsY1564SacAvailabilitySet, Y1564ApiLock, Y1564ApiUnLock, 0, 0, 2, "%u %u %s", u4FsY1564ContextId , u4FsY1564SacId ,pSetValFsY1564SacAvailability)
#define nmhSetFsY1564SacRowStatus(u4FsY1564ContextId , u4FsY1564SacId ,i4SetValFsY1564SacRowStatus) \
 nmhSetCmnWithLock(FsY1564SacRowStatus, 13, FsY1564SacRowStatusSet, Y1564ApiLock, Y1564ApiUnLock, 0, 1, 2, "%u %u %i", u4FsY1564ContextId , u4FsY1564SacId ,i4SetValFsY1564SacRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsY1564ServiceConfId[13];
extern UINT4 FsY1564ServiceConfColorMode[13];
extern UINT4 FsY1564ServiceConfCoupFlag[13];
extern UINT4 FsY1564ServiceConfCIR[13];
extern UINT4 FsY1564ServiceConfCBS[13];
extern UINT4 FsY1564ServiceConfEIR[13];
extern UINT4 FsY1564ServiceConfEBS[13];
extern UINT4 FsY1564ServiceConfRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsY1564ServiceConfColorMode(u4FsY1564ContextId , u4FsY1564ServiceConfId ,i4SetValFsY1564ServiceConfColorMode) \
 nmhSetCmnWithLock(FsY1564ServiceConfColorMode, 13, FsY1564ServiceConfColorModeSet, Y1564ApiLock, Y1564ApiUnLock, 0, 0, 2, "%u %u %i", u4FsY1564ContextId , u4FsY1564ServiceConfId ,i4SetValFsY1564ServiceConfColorMode)
#define nmhSetFsY1564ServiceConfCoupFlag(u4FsY1564ContextId , u4FsY1564ServiceConfId ,i4SetValFsY1564ServiceConfCoupFlag) \
 nmhSetCmnWithLock(FsY1564ServiceConfCoupFlag, 13, FsY1564ServiceConfCoupFlagSet, Y1564ApiLock, Y1564ApiUnLock, 0, 0, 2, "%u %u %i", u4FsY1564ContextId , u4FsY1564ServiceConfId ,i4SetValFsY1564ServiceConfCoupFlag)
#define nmhSetFsY1564ServiceConfCIR(u4FsY1564ContextId , u4FsY1564ServiceConfId ,u4SetValFsY1564ServiceConfCIR) \
 nmhSetCmnWithLock(FsY1564ServiceConfCIR, 13, FsY1564ServiceConfCIRSet, Y1564ApiLock, Y1564ApiUnLock, 0, 0, 2, "%u %u %u", u4FsY1564ContextId , u4FsY1564ServiceConfId ,u4SetValFsY1564ServiceConfCIR)
#define nmhSetFsY1564ServiceConfCBS(u4FsY1564ContextId , u4FsY1564ServiceConfId ,u4SetValFsY1564ServiceConfCBS) \
 nmhSetCmnWithLock(FsY1564ServiceConfCBS, 13, FsY1564ServiceConfCBSSet, Y1564ApiLock, Y1564ApiUnLock, 0, 0, 2, "%u %u %u", u4FsY1564ContextId , u4FsY1564ServiceConfId ,u4SetValFsY1564ServiceConfCBS)
#define nmhSetFsY1564ServiceConfEIR(u4FsY1564ContextId , u4FsY1564ServiceConfId ,u4SetValFsY1564ServiceConfEIR) \
 nmhSetCmnWithLock(FsY1564ServiceConfEIR, 13, FsY1564ServiceConfEIRSet, Y1564ApiLock, Y1564ApiUnLock, 0, 0, 2, "%u %u %u", u4FsY1564ContextId , u4FsY1564ServiceConfId ,u4SetValFsY1564ServiceConfEIR)
#define nmhSetFsY1564ServiceConfEBS(u4FsY1564ContextId , u4FsY1564ServiceConfId ,u4SetValFsY1564ServiceConfEBS) \
 nmhSetCmnWithLock(FsY1564ServiceConfEBS, 13, FsY1564ServiceConfEBSSet, Y1564ApiLock, Y1564ApiUnLock, 0, 0, 2, "%u %u %u", u4FsY1564ContextId , u4FsY1564ServiceConfId ,u4SetValFsY1564ServiceConfEBS)
#define nmhSetFsY1564ServiceConfRowStatus(u4FsY1564ContextId , u4FsY1564ServiceConfId ,i4SetValFsY1564ServiceConfRowStatus) \
 nmhSetCmnWithLock(FsY1564ServiceConfRowStatus, 13, FsY1564ServiceConfRowStatusSet, Y1564ApiLock, Y1564ApiUnLock, 0, 1, 2, "%u %u %i", u4FsY1564ContextId , u4FsY1564ServiceConfId ,i4SetValFsY1564ServiceConfRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsY1564PerformanceTestIndex[13];
extern UINT4 FsY1564PerformanceTestSlaList[13];
extern UINT4 FsY1564PerformanceTestDuration[13];
extern UINT4 FsY1564PerformanceTestRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsY1564PerformanceTestSlaList(u4FsY1564ContextId , u4FsY1564PerformanceTestIndex ,pSetValFsY1564PerformanceTestSlaList) \
 nmhSetCmnWithLock(FsY1564PerformanceTestSlaList, 13, FsY1564PerformanceTestSlaListSet, Y1564ApiLock, Y1564ApiUnLock, 0, 0, 2, "%u %u %s", u4FsY1564ContextId , u4FsY1564PerformanceTestIndex ,pSetValFsY1564PerformanceTestSlaList)
#define nmhSetFsY1564PerformanceTestDuration(u4FsY1564ContextId , u4FsY1564PerformanceTestIndex ,i4SetValFsY1564PerformanceTestDuration) \
 nmhSetCmnWithLock(FsY1564PerformanceTestDuration, 13, FsY1564PerformanceTestDurationSet, Y1564ApiLock, Y1564ApiUnLock, 0, 0, 2, "%u %u %i", u4FsY1564ContextId , u4FsY1564PerformanceTestIndex ,i4SetValFsY1564PerformanceTestDuration)
#define nmhSetFsY1564PerformanceTestRowStatus(u4FsY1564ContextId , u4FsY1564PerformanceTestIndex ,i4SetValFsY1564PerformanceTestRowStatus) \
 nmhSetCmnWithLock(FsY1564PerformanceTestRowStatus, 13, FsY1564PerformanceTestRowStatusSet, Y1564ApiLock, Y1564ApiUnLock, 0, 1, 2, "%u %u %i", u4FsY1564ContextId , u4FsY1564PerformanceTestIndex ,i4SetValFsY1564PerformanceTestRowStatus)

#endif
