/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fmcli.h,v 1.4 2007/02/01 14:52:32 iss Exp $
 *
 * Description: This file contains the data structure and macros
 *              for FM CLI module
 *********************************************************************/

#ifndef _FMCLI_H_
#define _FMCLI_H_

#include "cli.h"

/* max no of variable inputs to cli parser */
#define FM_CLI_MAX_ARGS              5
/* Max test pattern string length (f0f0f0f0) */
#define FM_CLI_TST_PTRN_STR_LEN      8
       
#define FM_CLI_ASSIGN_HEX(px)  px[0]='0';px[1]='x'


/* Command Identifiers */
enum
{
   FM_CLI_SYS_CTRL = 1,             /*1*/        
   FM_CLI_MOD_STAT,                 /*2*/    
   FM_CLI_DEBUG_SHOW,               /*3*/      
   FM_CLI_DEBUG,                    /*4*/ 
   FM_CLI_NO_DEBUG,                 /*5*/    
   FM_CLI_CLEAR_MIB_RESP,           /*6*/          
   FM_CLI_SHOW_GLOBAL_INFO,         /*7*/            
   FM_CLI_SHOW_MIB_RESP,            /*8*/         
   FM_CLI_SHOW_PORT_CONFIG,         /*9*/            
   FM_CLI_SHOW_LB_CUR_DETAIL,       /*10*/              
   FM_CLI_SHOW_LB_CUR,              /*11*/       
   FM_CLI_SHOW_LB_LAST_DETAIL,      /*12*/               
   FM_CLI_SHOW_LB_LAST,             /*13*/        
   FM_CLI_LB_TEST,                  /*14*/   
   FM_CLI_LM_SYM,                   /*15*/  
   FM_CLI_LM_FRAME,                 /*16*/    
   FM_CLI_LM_FRAME_PERIOD,          /*17*/           
   FM_CLI_LM_FRAME_SEC_SUM,         /*18*/            
   FM_CLI_CRITICAL_EVENT,           /*19*/          
   FM_CLI_DYING_GASP,               /*20*/      
   FM_CLI_LINK_FAULT,               /*21*/      
   FM_CLI_VAR_REQ_COUNT,            /*22*/         
   FM_CLI_VAR_REQ                   /*23*/
};

/*Error Strings*/
enum{
   FM_CLI_ERR_WRONG_INPUT_VAL = 1,         /*1*/
   FM_CLI_ERR_REMOTE_NOT_IN_LB,            /*2*/
   FM_CLI_ERR_EOAM_NOT_STARTED_ON_PORT,    /*3*/       
   FM_CLI_ERR_INCORRECT_DESCRIPTOR_LENGTH, /*4*/
   FM_CLI_ERR_INCORRECT_DESCRIPTOR_VAL,    /*5*/
   FM_CLI_ERR_EXCEEDING_MAX_DESCRIPTORS    /*6*/
};

/* Max number of error strings that can be set */
#define FM_CLI_MAX_ERR           6


/* The error strings should be placed under the switch so as to avoid 
 * redefinition. This will be visible only in modulecli.c file
 */
#ifdef FMCLI_C
CONST CHR1  *gapc1FmCliErrString [] = {
   NULL,
   "Please check input parameters\r\n",
   "Remote Peer not in loopback\r\n",
   "EOAM is not started on the interface\r\n",
   "Please correct descriptor length\r\n",
   "Please check descriptor values\r\n",
   "Number of descriptors exceeds configured value\r\n"
};
#endif/* FMCLI_C */

/*Function prototypes*/
PUBLIC INT4  cli_process_fm_cmd(tCliHandle,UINT4, ...);
PUBLIC INT4  FmCliSetSystemCtrl (tCliHandle, INT4);
PUBLIC INT4  FmCliSetModuleStatus (tCliHandle, INT4);
PUBLIC INT4  FmCliEnableDebug (tCliHandle, INT4);
PUBLIC INT4  FmCliDisableDebug (tCliHandle, INT4);
PUBLIC INT4  FmCliClearStats (tCliHandle, INT4);
PUBLIC INT4  FmCliShowGlobal (tCliHandle);
PUBLIC INT4  FmCliShowMibResp (tCliHandle, INT4);
PUBLIC UINT4 FmCliDispMibResp (tCliHandle, INT4, UINT4);
PUBLIC VOID  FmCliDispMibRespDetail (tCliHandle , UINT1 *, UINT1 *, INT4 *);
PUBLIC INT4  FmCliShowPortConfig (tCliHandle, INT4);
PUBLIC INT4  FmCliShowCurrentStats (tCliHandle, INT4);
PUBLIC INT4  FmCliShowCurrentStatsDetail (tCliHandle, INT4);
PUBLIC INT4  FmCliShowCurrentSessDetails (tCliHandle, INT4);
PUBLIC INT4  FmCliShowLastStats (tCliHandle, INT4);
PUBLIC INT4  FmCliShowLastStatsDetail (tCliHandle, INT4);
PUBLIC INT4  FmCliShowLastSessDetails (tCliHandle, INT4);
PUBLIC INT4  FmCliLBTestConfig (tCliHandle, INT4, UINT4, UINT4, UINT1 *, 
                                INT4, INT4);
PUBLIC INT4  FmCliLinkMonitorSymbol (tCliHandle, INT4, INT4);
PUBLIC INT4  FmCliLinkMonitorFrame (tCliHandle, INT4, INT4);
PUBLIC INT4  FmCliLinkMonitorFramePeriod (tCliHandle, INT4, INT4);
PUBLIC INT4  FmCliLinkMonitorFrameSecSum (tCliHandle, INT4, INT4);
PUBLIC INT4  FmCliCriticalEvent (tCliHandle, INT4, INT4);
PUBLIC INT4  FmCliDyingGasp (tCliHandle, INT4, INT4);
PUBLIC INT4  FmCliLinkFault (tCliHandle, INT4, INT4);
PUBLIC INT4  FmCliSetVarReqCount (tCliHandle, INT4, UINT4);
PUBLIC INT4  FmCliSetVarReq (tCliHandle, INT4, UINT1 *);

PUBLIC VOID  FmCliShowDebugging (tCliHandle);
PUBLIC VOID  FmCliShowRunningConfig (tCliHandle, UINT4);
PUBLIC VOID  FmCliShowRunningConfigGlobal (tCliHandle);
PUBLIC VOID  FmCliShowRunningConfigInterface (tCliHandle);
PUBLIC VOID  FmCliShowRunningConfigIfInfo (tCliHandle, INT4, BOOL1);

PUBLIC INT4  FmCliSrcPrintPortName(tCliHandle, INT4, BOOL1, UINT1 * );

#endif/*_FMCLI_H_*/
/*--------------------------------------------------------------------------*/
/*                         End of the file fmcli.h                          */
/*--------------------------------------------------------------------------*/
