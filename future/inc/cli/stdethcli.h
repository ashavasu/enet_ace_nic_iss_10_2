/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdethcli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Dot3PauseAdminMode[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDot3PauseAdminMode(i4Dot3StatsIndex ,i4SetValDot3PauseAdminMode)	\
	nmhSetCmn(Dot3PauseAdminMode, 11, Dot3PauseAdminModeSet, CfaLock, CfaUnlock, 0, 0, 1, "%i %i", i4Dot3StatsIndex ,i4SetValDot3PauseAdminMode)

#endif
