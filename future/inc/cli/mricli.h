/****************************************************************************/
/* Copyright (C) 2009 Aricent Inc . All Rights Reserved                     */
/*                                                                          */
/*  FILE NAME             : mricli.h                                        */
/*  PRINCIPAL AUTHOR      : Aricent                                         */
/*  SUBSYSTEM NAME        :                                                 */
/*  MODULE NAME           : MRI                                             */
/*  LANGUAGE              : C                                               */
/*  TARGET ENVIRONMENT    :                                                 */
/*  DATE OF FIRST RELEASE :                                                 */
/*  AUTHOR                : Aricent                                         */
/*  FUNCTIONS DEFINED(Applicable for Source files) :                        */
/*  DESCRIPTION           : This file contains the CLI definitions and      */
/*                          macros of Aricent MRI Module.                   */
/****************************************************************************/
/*                                                                          */
/*  Change History                                                          */
/*  Version               :                                                 */
/*  Date(DD/MM/YYYY)      :                                                 */
/*  Modified by           :                                                 */
/*  Description of change :                                                 */
/****************************************************************************/

#ifndef _MRI_CLI_H_
#define _MRI_CLI_H_

#include "lr.h"
#include "cli.h"

#define MRI_NEW_ROUTE 1
#define MRI_SAME_ROUTE 2

#define MRI_MAX_NUM_MRP 13
#define MRI_MAX_ADDR_BUFFER 20
#define MRI_MAX_NUM_OIFSTATE 3

#define MRI_CLI_MAX_ARGS  1
#define MAX_MRI_TIME_STR_LEN  15

enum eMriCliCmdValues {

  CLI_MRI_MROUTING_ENABLE = 1,
  CLI_MRI_MROUTING_DISABLE,
  CLI_MRI_RATELIMIT,
  CLI_MRI_NO_RATELIMIT,
  CLI_MRI_TTL,
  CLI_MRI_NO_TTL,
  CLI_MRI_SHOW_MROUTE

};

enum eMriCliErrCodes {

  CLI_MRI_INVALID_RATELIMIT = 1,
  CLI_MRI_INVALID_TTL,
  CLI_MRI_NO_MRP_ON_INTERFACE,
  CLI_MRI_MAX_ERR

};

#ifdef _MRI_CLI_C_

CONST CHR1 *gapi1MriCliErrString [] = {
   NULL,
   "Invalid rate-limit value\r\n",
   "Invalid ttl-threshold value.\r\n",
   "MRP not enabled on this interface.\r\n",
   "\r\n"
};

#endif /* _MRI_CLI_C_ */

INT1 cli_process_mri_cmd (tCliHandle CliHandle, UINT4 u4Command,...);


#endif/*_MRI_CLI_H_*/
