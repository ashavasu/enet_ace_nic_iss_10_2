/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsdot1cli.h,v 1.5 2015/03/07 10:42:14 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Dot1adPortNum[12];
extern UINT4 Dot1adPortPcpSelectionRow[12];
extern UINT4 Dot1adPortUseDei[12];
extern UINT4 Dot1adPortReqDropEncoding[12];
extern UINT4 Dot1adPortSVlanPriorityType[12];
extern UINT4 Dot1adPortSVlanPriority[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDot1adPortPcpSelectionRow(i4Dot1adPortNum ,i4SetValDot1adPortPcpSelectionRow) \
 nmhSetCmn(Dot1adPortPcpSelectionRow, 12, Dot1adPortPcpSelectionRowSet, NULL, NULL, 0, 0, 1, "%i %i", i4Dot1adPortNum ,i4SetValDot1adPortPcpSelectionRow)
#define nmhSetDot1adPortUseDei(i4Dot1adPortNum ,i4SetValDot1adPortUseDei) \
 nmhSetCmn(Dot1adPortUseDei, 12, Dot1adPortUseDeiSet, NULL, NULL, 0, 0, 1, "%i %i", i4Dot1adPortNum ,i4SetValDot1adPortUseDei)
#define nmhSetDot1adPortReqDropEncoding(i4Dot1adPortNum ,i4SetValDot1adPortReqDropEncoding) \
 nmhSetCmn(Dot1adPortReqDropEncoding, 12, Dot1adPortReqDropEncodingSet, NULL, NULL, 0, 0, 1, "%i %i", i4Dot1adPortNum ,i4SetValDot1adPortReqDropEncoding)
#define nmhSetDot1adPortSVlanPriorityType(i4Dot1adPortNum ,i4SetValDot1adPortSVlanPriorityType) \
 nmhSetCmn(Dot1adPortSVlanPriorityType, 12, Dot1adPortSVlanPriorityTypeSet, NULL, NULL, 0, 0, 1, "%i %i", i4Dot1adPortNum ,i4SetValDot1adPortSVlanPriorityType)
#define nmhSetDot1adPortSVlanPriority(i4Dot1adPortNum ,i4SetValDot1adPortSVlanPriority) \
 nmhSetCmn(Dot1adPortSVlanPriority, 12, Dot1adPortSVlanPrioritySet, NULL, NULL, 0, 0, 1, "%i %i", i4Dot1adPortNum ,i4SetValDot1adPortSVlanPriority)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Dot1adVidTranslationLocalVid[12];
extern UINT4 Dot1adVidTranslationRelayVid[12];
extern UINT4 Dot1adVidTranslationRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDot1adVidTranslationRelayVid(i4Dot1adPortNum , i4Dot1adVidTranslationLocalVid ,i4SetValDot1adVidTranslationRelayVid) \
 nmhSetCmn(Dot1adVidTranslationRelayVid, 12, Dot1adVidTranslationRelayVidSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4Dot1adPortNum , i4Dot1adVidTranslationLocalVid ,i4SetValDot1adVidTranslationRelayVid)
#define nmhSetDot1adVidTranslationRowStatus(i4Dot1adPortNum , i4Dot1adVidTranslationLocalVid ,i4SetValDot1adVidTranslationRowStatus)    \
    nmhSetCmn(Dot1adVidTranslationRowStatus, 12, Dot1adVidTranslationRowStatusSet, NULL, NULL, 0, 1, 2, "%i %i %i", i4Dot1adPortNum , i4Dot1adVidTranslationLocalVid ,i4SetValDot1adVidTranslationRowStatus)
#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Dot1adCVidRegistrationCVid[12];
extern UINT4 Dot1adCVidRegistrationSVid[12];
extern UINT4 Dot1adCVidRegistrationUntaggedPep[12];
extern UINT4 Dot1adCVidRegistrationUntaggedCep[12];
extern UINT4 Dot1adCVidRegistrationRowStatus[12];
extern UINT4 Dot1adCVidRegistrationSVlanPriorityType[12];
extern UINT4 Dot1adCVidRegistrationSVlanPriority[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDot1adCVidRegistrationSVid(i4Dot1adPortNum , i4Dot1adCVidRegistrationCVid ,i4SetValDot1adCVidRegistrationSVid) \
 nmhSetCmn(Dot1adCVidRegistrationSVid, 12, Dot1adCVidRegistrationSVidSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4Dot1adPortNum , i4Dot1adCVidRegistrationCVid ,i4SetValDot1adCVidRegistrationSVid)
#define nmhSetDot1adCVidRegistrationUntaggedPep(i4Dot1adPortNum , i4Dot1adCVidRegistrationCVid ,i4SetValDot1adCVidRegistrationUntaggedPep) \
 nmhSetCmn(Dot1adCVidRegistrationUntaggedPep, 12, Dot1adCVidRegistrationUntaggedPepSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4Dot1adPortNum , i4Dot1adCVidRegistrationCVid ,i4SetValDot1adCVidRegistrationUntaggedPep)
#define nmhSetDot1adCVidRegistrationUntaggedCep(i4Dot1adPortNum , i4Dot1adCVidRegistrationCVid ,i4SetValDot1adCVidRegistrationUntaggedCep) \
 nmhSetCmn(Dot1adCVidRegistrationUntaggedCep, 12, Dot1adCVidRegistrationUntaggedCepSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4Dot1adPortNum , i4Dot1adCVidRegistrationCVid ,i4SetValDot1adCVidRegistrationUntaggedCep)
#define nmhSetDot1adCVidRegistrationRowStatus(i4Dot1adPortNum , i4Dot1adCVidRegistrationCVid ,i4SetValDot1adCVidRegistrationRowStatus) \
 nmhSetCmn(Dot1adCVidRegistrationRowStatus, 12, Dot1adCVidRegistrationRowStatusSet, NULL, NULL, 0, 1, 2, "%i %i %i", i4Dot1adPortNum , i4Dot1adCVidRegistrationCVid ,i4SetValDot1adCVidRegistrationRowStatus)
#define nmhSetDot1adCVidRegistrationSVlanPriorityType(i4Dot1adPortNum , i4Dot1adCVidRegistrationCVid ,i4SetValDot1adCVidRegistrationSVlanPriorityType) \
 nmhSetCmn(Dot1adCVidRegistrationSVlanPriorityType, 12, Dot1adCVidRegistrationSVlanPriorityTypeSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4Dot1adPortNum , i4Dot1adCVidRegistrationCVid ,i4SetValDot1adCVidRegistrationSVlanPriorityType)
#define nmhSetDot1adCVidRegistrationSVlanPriority(i4Dot1adPortNum , i4Dot1adCVidRegistrationCVid ,i4SetValDot1adCVidRegistrationSVlanPriority) \
 nmhSetCmn(Dot1adCVidRegistrationSVlanPriority, 12, Dot1adCVidRegistrationSVlanPrioritySet, NULL, NULL, 0, 0, 2, "%i %i %i", i4Dot1adPortNum , i4Dot1adCVidRegistrationCVid ,i4SetValDot1adCVidRegistrationSVlanPriority)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Dot1adPepPvid[12];
extern UINT4 Dot1adPepDefaultUserPriority[12];
extern UINT4 Dot1adPepAccptableFrameTypes[12];
extern UINT4 Dot1adPepIngressFiltering[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDot1adPepPvid(i4Dot1adPortNum , i4Dot1adCVidRegistrationSVid ,i4SetValDot1adPepPvid) \
 nmhSetCmn(Dot1adPepPvid, 12, Dot1adPepPvidSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4Dot1adPortNum , i4Dot1adCVidRegistrationSVid ,i4SetValDot1adPepPvid)
#define nmhSetDot1adPepDefaultUserPriority(i4Dot1adPortNum , i4Dot1adCVidRegistrationSVid ,i4SetValDot1adPepDefaultUserPriority) \
 nmhSetCmn(Dot1adPepDefaultUserPriority, 12, Dot1adPepDefaultUserPrioritySet, NULL, NULL, 0, 0, 2, "%i %i %i", i4Dot1adPortNum , i4Dot1adCVidRegistrationSVid ,i4SetValDot1adPepDefaultUserPriority)
#define nmhSetDot1adPepAccptableFrameTypes(i4Dot1adPortNum , i4Dot1adCVidRegistrationSVid ,i4SetValDot1adPepAccptableFrameTypes) \
 nmhSetCmn(Dot1adPepAccptableFrameTypes, 12, Dot1adPepAccptableFrameTypesSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4Dot1adPortNum , i4Dot1adCVidRegistrationSVid ,i4SetValDot1adPepAccptableFrameTypes)
#define nmhSetDot1adPepIngressFiltering(i4Dot1adPortNum , i4Dot1adCVidRegistrationSVid ,i4SetValDot1adPepIngressFiltering) \
 nmhSetCmn(Dot1adPepIngressFiltering, 12, Dot1adPepIngressFilteringSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4Dot1adPortNum , i4Dot1adCVidRegistrationSVid ,i4SetValDot1adPepIngressFiltering)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Dot1adServicePriorityRegenReceivedPriority[12];
extern UINT4 Dot1adServicePriorityRegenRegeneratedPriority[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDot1adServicePriorityRegenRegeneratedPriority(i4Dot1adPortNum , i4Dot1adCVidRegistrationSVid , i4Dot1adServicePriorityRegenReceivedPriority ,i4SetValDot1adServicePriorityRegenRegeneratedPriority) \
 nmhSetCmn(Dot1adServicePriorityRegenRegeneratedPriority, 12, Dot1adServicePriorityRegenRegeneratedPrioritySet, NULL, NULL, 0, 0, 3, "%i %i %i %i", i4Dot1adPortNum , i4Dot1adCVidRegistrationSVid , i4Dot1adServicePriorityRegenReceivedPriority ,i4SetValDot1adServicePriorityRegenRegeneratedPriority)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Dot1adPcpDecodingPcpSelRow[12];
extern UINT4 Dot1adPcpDecodingPcpValue[12];
extern UINT4 Dot1adPcpDecodingPriority[12];
extern UINT4 Dot1adPcpDecodingDropEligible[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDot1adPcpDecodingPriority(i4Dot1adPortNum , i4Dot1adPcpDecodingPcpSelRow , i4Dot1adPcpDecodingPcpValue ,i4SetValDot1adPcpDecodingPriority) \
 nmhSetCmn(Dot1adPcpDecodingPriority, 12, Dot1adPcpDecodingPrioritySet, NULL, NULL, 0, 0, 3, "%i %i %i %i", i4Dot1adPortNum , i4Dot1adPcpDecodingPcpSelRow , i4Dot1adPcpDecodingPcpValue ,i4SetValDot1adPcpDecodingPriority)
#define nmhSetDot1adPcpDecodingDropEligible(i4Dot1adPortNum , i4Dot1adPcpDecodingPcpSelRow , i4Dot1adPcpDecodingPcpValue ,i4SetValDot1adPcpDecodingDropEligible) \
 nmhSetCmn(Dot1adPcpDecodingDropEligible, 12, Dot1adPcpDecodingDropEligibleSet, NULL, NULL, 0, 0, 3, "%i %i %i %i", i4Dot1adPortNum , i4Dot1adPcpDecodingPcpSelRow , i4Dot1adPcpDecodingPcpValue ,i4SetValDot1adPcpDecodingDropEligible)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Dot1adPcpEncodingPcpSelRow[12];
extern UINT4 Dot1adPcpEncodingPriority[12];
extern UINT4 Dot1adPcpEncodingDropEligible[12];
extern UINT4 Dot1adPcpEncodingPcpValue[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDot1adPcpEncodingPcpValue(i4Dot1adPortNum , i4Dot1adPcpEncodingPcpSelRow , i4Dot1adPcpEncodingPriority , i4Dot1adPcpEncodingDropEligible ,i4SetValDot1adPcpEncodingPcpValue) \
 nmhSetCmn(Dot1adPcpEncodingPcpValue, 12, Dot1adPcpEncodingPcpValueSet, NULL, NULL, 0, 0, 4, "%i %i %i %i %i", i4Dot1adPortNum , i4Dot1adPcpEncodingPcpSelRow , i4Dot1adPcpEncodingPriority , i4Dot1adPcpEncodingDropEligible ,i4SetValDot1adPcpEncodingPcpValue)

#endif
