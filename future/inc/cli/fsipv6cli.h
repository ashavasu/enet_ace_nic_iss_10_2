/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsipv6cli.h,v 1.11 2015/10/19 12:19:29 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsipv6NdCacheMaxRetries[11];
extern UINT4 Fsipv6PmtuConfigStatus[11];
extern UINT4 Fsipv6PmtuTimeOutInterval[11];
extern UINT4 Fsipv6JumboEnable[11];
extern UINT4 Fsipv6GlobalDebug[11];
extern UINT4 Fsipv6MaxRouteEntries[11];
extern UINT4 Fsipv6MaxLogicalIfaces[11];
extern UINT4 Fsipv6MaxTunnelIfaces[11];
extern UINT4 Fsipv6MaxAddresses[11];
extern UINT4 Fsipv6MaxFragReasmEntries[11];
extern UINT4 Fsipv6Nd6MaxCacheEntries[11];
extern UINT4 Fsipv6PmtuMaxDest[11];
extern UINT4 Fsipv6RFC5095Compatibility[11];
extern UINT4 Fsipv6RFC5942Compatibility[11];
extern UINT4 Fsipv6SENDSecLevel[11];
extern UINT4 Fsipv6SENDNbrSecLevel[11];
extern UINT4 Fsipv6SENDAuthType[11];
extern UINT4 Fsipv6SENDMinBits[11];
extern UINT4 Fsipv6SENDSecDAD[11];
extern UINT4 Fsipv6SENDPrefixChk[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsipv6NdCacheMaxRetries(i4SetValFsipv6NdCacheMaxRetries) \
 nmhSetCmn(Fsipv6NdCacheMaxRetries, 11, Fsipv6NdCacheMaxRetriesSet, Ip6Lock, Ip6UnLock, 0, 0, 0, "%i", i4SetValFsipv6NdCacheMaxRetries)
#define nmhSetFsipv6PmtuConfigStatus(i4SetValFsipv6PmtuConfigStatus) \
 nmhSetCmn(Fsipv6PmtuConfigStatus, 11, Fsipv6PmtuConfigStatusSet, Ip6Lock, Ip6UnLock, 0, 0, 0, "%i", i4SetValFsipv6PmtuConfigStatus)
#define nmhSetFsipv6PmtuTimeOutInterval(u4SetValFsipv6PmtuTimeOutInterval) \
 nmhSetCmn(Fsipv6PmtuTimeOutInterval, 11, Fsipv6PmtuTimeOutIntervalSet, Ip6Lock, Ip6UnLock, 0, 0, 0, "%u", u4SetValFsipv6PmtuTimeOutInterval)
#define nmhSetFsipv6JumboEnable(i4SetValFsipv6JumboEnable) \
 nmhSetCmn(Fsipv6JumboEnable, 11, Fsipv6JumboEnableSet, Ip6Lock, Ip6UnLock, 0, 0, 0, "%i", i4SetValFsipv6JumboEnable)
#define nmhSetFsipv6GlobalDebug(u4SetValFsipv6GlobalDebug) \
 nmhSetCmn(Fsipv6GlobalDebug, 11, Fsipv6GlobalDebugSet, Ip6Lock, Ip6UnLock, 0, 0, 0, "%u", u4SetValFsipv6GlobalDebug)
#define nmhSetFsipv6MaxRouteEntries(u4SetValFsipv6MaxRouteEntries) \
 nmhSetCmn(Fsipv6MaxRouteEntries, 11, Fsipv6MaxRouteEntriesSet, Ip6Lock, Ip6UnLock, 0, 0, 0, "%u", u4SetValFsipv6MaxRouteEntries)
#define nmhSetFsipv6MaxLogicalIfaces(u4SetValFsipv6MaxLogicalIfaces) \
 nmhSetCmn(Fsipv6MaxLogicalIfaces, 11, Fsipv6MaxLogicalIfacesSet, Ip6Lock, Ip6UnLock, 0, 0, 0, "%u", u4SetValFsipv6MaxLogicalIfaces)
#define nmhSetFsipv6MaxTunnelIfaces(u4SetValFsipv6MaxTunnelIfaces) \
 nmhSetCmn(Fsipv6MaxTunnelIfaces, 11, Fsipv6MaxTunnelIfacesSet, Ip6Lock, Ip6UnLock, 0, 0, 0, "%u", u4SetValFsipv6MaxTunnelIfaces)
#define nmhSetFsipv6MaxAddresses(u4SetValFsipv6MaxAddresses) \
 nmhSetCmn(Fsipv6MaxAddresses, 11, Fsipv6MaxAddressesSet, Ip6Lock, Ip6UnLock, 0, 0, 0, "%u", u4SetValFsipv6MaxAddresses)
#define nmhSetFsipv6MaxFragReasmEntries(u4SetValFsipv6MaxFragReasmEntries) \
 nmhSetCmn(Fsipv6MaxFragReasmEntries, 11, Fsipv6MaxFragReasmEntriesSet, Ip6Lock, Ip6UnLock, 0, 0, 0, "%u", u4SetValFsipv6MaxFragReasmEntries)
#define nmhSetFsipv6Nd6MaxCacheEntries(u4SetValFsipv6Nd6MaxCacheEntries) \
 nmhSetCmn(Fsipv6Nd6MaxCacheEntries, 11, Fsipv6Nd6MaxCacheEntriesSet, Ip6Lock, Ip6UnLock, 0, 0, 0, "%u", u4SetValFsipv6Nd6MaxCacheEntries)
#define nmhSetFsipv6PmtuMaxDest(u4SetValFsipv6PmtuMaxDest) \
 nmhSetCmn(Fsipv6PmtuMaxDest, 11, Fsipv6PmtuMaxDestSet, Ip6Lock, Ip6UnLock, 0, 0, 0, "%u", u4SetValFsipv6PmtuMaxDest)
#define nmhSetFsipv6RFC5095Compatibility(i4SetValFsipv6RFC5095Compatibility) \
 nmhSetCmn(Fsipv6RFC5095Compatibility, 11, Fsipv6RFC5095CompatibilitySet, Ip6Lock, Ip6UnLock, 0, 0, 0, "%i", i4SetValFsipv6RFC5095Compatibility)
#define nmhSetFsipv6RFC5942Compatibility(i4SetValFsipv6RFC5942Compatibility)    \
 nmhSetCmn(Fsipv6RFC5942Compatibility, 11, Fsipv6RFC5942CompatibilitySet, Ip6Lock, Ip6UnLock, 0, 0, 0, "%i", i4SetValFsipv6RFC5942Compatibility)
#define nmhSetFsipv6SENDSecLevel(i4SetValFsipv6SENDSecLevel)    \
        nmhSetCmn(Fsipv6SENDSecLevel, 11, Fsipv6SENDSecLevelSet, Ip6Lock, Ip6UnLock, 0, 0, 0, "%i", i4SetValFsipv6SENDSecLevel)
#define nmhSetFsipv6SENDNbrSecLevel(i4SetValFsipv6SENDNbrSecLevel)  \
        nmhSetCmn(Fsipv6SENDNbrSecLevel, 11, Fsipv6SENDNbrSecLevelSet, Ip6Lock, Ip6UnLock, 0, 0, 0, "%i", i4SetValFsipv6SENDNbrSecLevel)
#define nmhSetFsipv6SENDAuthType(i4SetValFsipv6SENDAuthType)    \
        nmhSetCmn(Fsipv6SENDAuthType, 11, Fsipv6SENDAuthTypeSet, Ip6Lock, Ip6UnLock, 0, 0, 0, "%i", i4SetValFsipv6SENDAuthType)
#define nmhSetFsipv6SENDMinBits(i4SetValFsipv6SENDMinBits)  \
        nmhSetCmn(Fsipv6SENDMinBits, 11, Fsipv6SENDMinBitsSet, Ip6Lock, Ip6UnLock, 0, 0, 0, "%i", i4SetValFsipv6SENDMinBits)
#define nmhSetFsipv6SENDSecDAD(i4SetValFsipv6SENDSecDAD)    \
        nmhSetCmn(Fsipv6SENDSecDAD, 11, Fsipv6SENDSecDADSet, Ip6Lock, Ip6UnLock, 0, 0, 0, "%i", i4SetValFsipv6SENDSecDAD)
#define nmhSetFsipv6SENDPrefixChk(i4SetValFsipv6SENDPrefixChk)  \
        nmhSetCmn(Fsipv6SENDPrefixChk, 11, Fsipv6SENDPrefixChkSet, Ip6Lock, Ip6UnLock, 0, 0, 0, "%i", i4SetValFsipv6SENDPrefixChk)
#define nmhSetFsipv6ECMPPRTTimer(i4SetValFsipv6ECMPPRTTimer)    \
 nmhSetCmnWithLock(Fsipv6ECMPPRTTimer, 11, Fsipv6ECMPPRTTimerSet, Ip6Lock, Ip6UnLock, 0, 0, 0, "%i", i4SetValFsipv6ECMPPRTTimer)
#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsipv6IfIndex[13];
extern UINT4 Fsipv6IfToken[13];
extern UINT4 Fsipv6IfAdminStatus[13];
extern UINT4 Fsipv6IfRouterAdvStatus[13];
extern UINT4 Fsipv6IfRouterAdvFlags[13];
extern UINT4 Fsipv6IfHopLimit[13];
extern UINT4 Fsipv6IfDefRouterTime[13];
extern UINT4 Fsipv6IfReachableTime[13];
extern UINT4 Fsipv6IfRetransmitTime[13];
extern UINT4 Fsipv6IfDelayProbeTime[13];
extern UINT4 Fsipv6IfPrefixAdvStatus[13];
extern UINT4 Fsipv6IfMinRouterAdvTime[13];
extern UINT4 Fsipv6IfMaxRouterAdvTime[13];
extern UINT4 Fsipv6IfDADRetries[13];
extern UINT4 Fsipv6IfForwarding[13];
extern UINT4 Fsipv6IfAdvSrcLLAdr[13];
extern UINT4 Fsipv6IfAdvIntOpt[13];
extern UINT4 Fsipv6IfNDProxyAdminStatus[13];
extern UINT4 Fsipv6IfNDProxyMode[13];
extern UINT4 Fsipv6IfNDProxyUpStream[13];
extern UINT4 Fsipv6IfSENDSecStatus[13];
extern UINT4 Fsipv6IfSENDDeltaTimestamp[13];
extern UINT4 Fsipv6IfSENDFuzzTimestamp[13];
extern UINT4 Fsipv6IfSENDDriftTimestamp[13];
extern UINT4 Fsipv6IfDefRoutePreference[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsipv6IfToken(i4Fsipv6IfIndex ,pSetValFsipv6IfToken) \
 nmhSetCmn(Fsipv6IfToken, 13, Fsipv6IfTokenSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %s", i4Fsipv6IfIndex ,pSetValFsipv6IfToken)
#define nmhSetFsipv6IfAdminStatus(i4Fsipv6IfIndex ,i4SetValFsipv6IfAdminStatus) \
 nmhSetCmn(Fsipv6IfAdminStatus, 13, Fsipv6IfAdminStatusSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %i", i4Fsipv6IfIndex ,i4SetValFsipv6IfAdminStatus)
#define nmhSetFsipv6IfRouterAdvStatus(i4Fsipv6IfIndex ,i4SetValFsipv6IfRouterAdvStatus) \
 nmhSetCmn(Fsipv6IfRouterAdvStatus, 13, Fsipv6IfRouterAdvStatusSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %i", i4Fsipv6IfIndex ,i4SetValFsipv6IfRouterAdvStatus)
#define nmhSetFsipv6IfRouterAdvFlags(i4Fsipv6IfIndex ,i4SetValFsipv6IfRouterAdvFlags) \
 nmhSetCmn(Fsipv6IfRouterAdvFlags, 13, Fsipv6IfRouterAdvFlagsSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %i", i4Fsipv6IfIndex ,i4SetValFsipv6IfRouterAdvFlags)
#define nmhSetFsipv6IfHopLimit(i4Fsipv6IfIndex ,i4SetValFsipv6IfHopLimit) \
 nmhSetCmn(Fsipv6IfHopLimit, 13, Fsipv6IfHopLimitSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %i", i4Fsipv6IfIndex ,i4SetValFsipv6IfHopLimit)
#define nmhSetFsipv6IfDefRouterTime(i4Fsipv6IfIndex ,i4SetValFsipv6IfDefRouterTime) \
 nmhSetCmn(Fsipv6IfDefRouterTime, 13, Fsipv6IfDefRouterTimeSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %i", i4Fsipv6IfIndex ,i4SetValFsipv6IfDefRouterTime)
#define nmhSetFsipv6IfReachableTime(i4Fsipv6IfIndex ,i4SetValFsipv6IfReachableTime) \
 nmhSetCmn(Fsipv6IfReachableTime, 13, Fsipv6IfReachableTimeSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %i", i4Fsipv6IfIndex ,i4SetValFsipv6IfReachableTime)
#define nmhSetFsipv6IfRetransmitTime(i4Fsipv6IfIndex ,i4SetValFsipv6IfRetransmitTime) \
 nmhSetCmn(Fsipv6IfRetransmitTime, 13, Fsipv6IfRetransmitTimeSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %i", i4Fsipv6IfIndex ,i4SetValFsipv6IfRetransmitTime)
#define nmhSetFsipv6IfDelayProbeTime(i4Fsipv6IfIndex ,i4SetValFsipv6IfDelayProbeTime) \
 nmhSetCmn(Fsipv6IfDelayProbeTime, 13, Fsipv6IfDelayProbeTimeSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %i", i4Fsipv6IfIndex ,i4SetValFsipv6IfDelayProbeTime)
#define nmhSetFsipv6IfPrefixAdvStatus(i4Fsipv6IfIndex ,i4SetValFsipv6IfPrefixAdvStatus) \
 nmhSetCmn(Fsipv6IfPrefixAdvStatus, 13, Fsipv6IfPrefixAdvStatusSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %i", i4Fsipv6IfIndex ,i4SetValFsipv6IfPrefixAdvStatus)
#define nmhSetFsipv6IfMinRouterAdvTime(i4Fsipv6IfIndex ,i4SetValFsipv6IfMinRouterAdvTime) \
 nmhSetCmn(Fsipv6IfMinRouterAdvTime, 13, Fsipv6IfMinRouterAdvTimeSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %i", i4Fsipv6IfIndex ,i4SetValFsipv6IfMinRouterAdvTime)
#define nmhSetFsipv6IfMaxRouterAdvTime(i4Fsipv6IfIndex ,i4SetValFsipv6IfMaxRouterAdvTime) \
 nmhSetCmn(Fsipv6IfMaxRouterAdvTime, 13, Fsipv6IfMaxRouterAdvTimeSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %i", i4Fsipv6IfIndex ,i4SetValFsipv6IfMaxRouterAdvTime)
#define nmhSetFsipv6IfDADRetries(i4Fsipv6IfIndex ,i4SetValFsipv6IfDADRetries) \
 nmhSetCmn(Fsipv6IfDADRetries, 13, Fsipv6IfDADRetriesSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %i", i4Fsipv6IfIndex ,i4SetValFsipv6IfDADRetries)
#define nmhSetFsipv6IfForwarding(i4Fsipv6IfIndex ,i4SetValFsipv6IfForwarding) \
 nmhSetCmn(Fsipv6IfForwarding, 13, Fsipv6IfForwardingSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %i", i4Fsipv6IfIndex ,i4SetValFsipv6IfForwarding)
#define nmhSetFsipv6IfAdvIntOpt(i4Fsipv6IfIndex ,i4SetValFsipv6IfAdvIntOpt) \
 nmhSetCmn(Fsipv6IfAdvIntOpt, 13, Fsipv6IfAdvIntOptSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %i", i4Fsipv6IfIndex ,i4SetValFsipv6IfAdvIntOpt)
#define nmhSetFsipv6IfAdvSrcLLAdr(i4Fsipv6IfIndex ,i4SetValFsipv6IfAdvSrcLLAdr) \
 nmhSetCmn(Fsipv6IfAdvSrcLLAdr, 13, Fsipv6IfAdvSrcLLAdrSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %i", i4Fsipv6IfIndex ,i4SetValFsipv6IfAdvSrcLLAdr)
#define nmhSetFsipv6IfNDProxyAdminStatus(i4Fsipv6IfIndex ,i4SetValFsipv6IfNDProxyAdminStatus)   \
    nmhSetCmn(Fsipv6IfNDProxyAdminStatus, 13, Fsipv6IfNDProxyAdminStatusSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %i", i4Fsipv6IfIndex ,i4SetValFsipv6IfNDProxyAdminStatus)
#define nmhSetFsipv6IfNDProxyMode(i4Fsipv6IfIndex ,i4SetValFsipv6IfNDProxyMode) \
    nmhSetCmn(Fsipv6IfNDProxyMode, 13, Fsipv6IfNDProxyModeSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %i", i4Fsipv6IfIndex ,i4SetValFsipv6IfNDProxyMode)
#define nmhSetFsipv6IfNDLocalProxyUpStream(i4Fsipv6IfIndex ,i4SetValFsipv6IfNDLocalProxyUpStream)   \
    nmhSetCmn(Fsipv6IfNDLocalProxyUpStream, 13, Fsipv6IfNDLocalProxyUpStreamSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %i", i4Fsipv6IfIndex ,i4SetValFsipv6IfNDLocalProxyUpStream)
#define nmhSetFsipv6IfSENDSecStatus(i4Fsipv6IfIndex ,i4SetValFsipv6IfSENDSecStatus) \
        nmhSetCmn(Fsipv6IfSENDSecStatus, 13, Fsipv6IfSENDSecStatusSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %i", i4Fsipv6IfIndex ,i4SetValFsipv6IfSENDSecStatus)
#define nmhSetFsipv6IfSENDDeltaTimestamp(i4Fsipv6IfIndex ,u4SetValFsipv6IfSENDDeltaTimestamp)   \
        nmhSetCmn(Fsipv6IfSENDDeltaTimestamp, 13, Fsipv6IfSENDDeltaTimestampSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %u", i4Fsipv6IfIndex ,u4SetValFsipv6IfSENDDeltaTimestamp)
#define nmhSetFsipv6IfSENDFuzzTimestamp(i4Fsipv6IfIndex ,u4SetValFsipv6IfSENDFuzzTimestamp) \
        nmhSetCmn(Fsipv6IfSENDFuzzTimestamp, 13, Fsipv6IfSENDFuzzTimestampSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %u", i4Fsipv6IfIndex ,u4SetValFsipv6IfSENDFuzzTimestamp)
#define nmhSetFsipv6IfSENDDriftTimestamp(i4Fsipv6IfIndex ,u4SetValFsipv6IfSENDDriftTimestamp)   \
        nmhSetCmn(Fsipv6IfSENDDriftTimestamp, 13, Fsipv6IfSENDDriftTimestampSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %u", i4Fsipv6IfIndex ,u4SetValFsipv6IfSENDDriftTimestamp)
#define nmhSetFsipv6IfDefRoutePreference(i4Fsipv6IfIndex ,i4SetValFsipv6IfDefRoutePreference) \
 nmhSetCmn(Fsipv6IfDefRoutePreference, 13, Fsipv6IfDefRoutePreferenceSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %i", i4Fsipv6IfIndex ,i4SetValFsipv6IfDefRoutePreference)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsipv6PrefixIndex[13];
extern UINT4 Fsipv6PrefixAddress[13];
extern UINT4 Fsipv6PrefixAddrLen[13];
extern UINT4 Fsipv6PrefixProfileIndex[13];
extern UINT4 Fsipv6PrefixAdminStatus[13];
extern UINT4 Fsipv6SupportEmbeddedRp[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsipv6PrefixProfileIndex(i4Fsipv6PrefixIndex , pFsipv6PrefixAddress , i4Fsipv6PrefixAddrLen ,i4SetValFsipv6PrefixProfileIndex) \
 nmhSetCmn(Fsipv6PrefixProfileIndex, 13, Fsipv6PrefixProfileIndexSet, Ip6Lock, Ip6UnLock, 0, 0, 3, "%i %s %i %i", i4Fsipv6PrefixIndex , pFsipv6PrefixAddress , i4Fsipv6PrefixAddrLen ,i4SetValFsipv6PrefixProfileIndex)
#define nmhSetFsipv6PrefixAdminStatus(i4Fsipv6PrefixIndex , pFsipv6PrefixAddress , i4Fsipv6PrefixAddrLen ,i4SetValFsipv6PrefixAdminStatus) \
 nmhSetCmn(Fsipv6PrefixAdminStatus, 13, Fsipv6PrefixAdminStatusSet, Ip6Lock, Ip6UnLock, 0, 1, 3, "%i %s %i %i", i4Fsipv6PrefixIndex , pFsipv6PrefixAddress , i4Fsipv6PrefixAddrLen ,i4SetValFsipv6PrefixAdminStatus)
#define nmhSetFsipv6SupportEmbeddedRp(i4Fsipv6PrefixIndex , pFsipv6PrefixAddress , i4Fsipv6PrefixAddrLen ,i4SetValFsipv6SupportEmbeddedRp) \
 nmhSetCmn(Fsipv6SupportEmbeddedRp, 13, Fsipv6SupportEmbeddedRpSet, Ip6Lock, Ip6UnLock, 0, 0, 3, "%i %s %i %i", i4Fsipv6PrefixIndex , pFsipv6PrefixAddress , i4Fsipv6PrefixAddrLen ,i4SetValFsipv6SupportEmbeddedRp)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsipv6AddrIndex[13];
extern UINT4 Fsipv6AddrAddress[13];
extern UINT4 Fsipv6AddrPrefixLen[13];
extern UINT4 Fsipv6AddrAdminStatus[13];
extern UINT4 Fsipv6AddrType[13];
extern UINT4 Fsipv6AddrProfIndex[13];
extern UINT4 Fsipv6AddrSENDCgaStatus[13];
extern UINT4 Fsipv6AddrSENDCgaModifier[13];
extern UINT4 Fsipv6AddrSENDCollisionCount[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsipv6AddrAdminStatus(i4Fsipv6AddrIndex , pFsipv6AddrAddress , i4Fsipv6AddrPrefixLen ,i4SetValFsipv6AddrAdminStatus) \
 nmhSetCmn(Fsipv6AddrAdminStatus, 13, Fsipv6AddrAdminStatusSet, Ip6Lock, Ip6UnLock, 0, 1, 3, "%i %s %i %i", i4Fsipv6AddrIndex , pFsipv6AddrAddress , i4Fsipv6AddrPrefixLen ,i4SetValFsipv6AddrAdminStatus)
#define nmhSetFsipv6AddrType(i4Fsipv6AddrIndex , pFsipv6AddrAddress , i4Fsipv6AddrPrefixLen ,i4SetValFsipv6AddrType) \
 nmhSetCmn(Fsipv6AddrType, 13, Fsipv6AddrTypeSet, Ip6Lock, Ip6UnLock, 0, 0, 3, "%i %s %i %i", i4Fsipv6AddrIndex , pFsipv6AddrAddress , i4Fsipv6AddrPrefixLen ,i4SetValFsipv6AddrType)
#define nmhSetFsipv6AddrProfIndex(i4Fsipv6AddrIndex , pFsipv6AddrAddress , i4Fsipv6AddrPrefixLen ,i4SetValFsipv6AddrProfIndex) \
 nmhSetCmn(Fsipv6AddrProfIndex, 13, Fsipv6AddrProfIndexSet, Ip6Lock, Ip6UnLock, 0, 0, 3, "%i %s %i %i", i4Fsipv6AddrIndex , pFsipv6AddrAddress , i4Fsipv6AddrPrefixLen ,i4SetValFsipv6AddrProfIndex)
#define nmhSetFsipv6AddrSENDCgaStatus(i4Fsipv6AddrIndex , pFsipv6AddrAddress , i4Fsipv6AddrPrefixLen ,i4SetValFsipv6AddrSENDCgaStatus)  \
        nmhSetCmn(Fsipv6AddrSENDCgaStatus, 13, Fsipv6AddrSENDCgaStatusSet, Ip6Lock, Ip6UnLock, 0, 0, 3, "%i %s %i %i", i4Fsipv6AddrIndex , pFsipv6AddrAddress , i4Fsipv6AddrPrefixLen ,i4SetValFsipv6AddrSENDCgaStatus)
#define nmhSetFsipv6AddrSENDCgaModifier(i4Fsipv6AddrIndex , pFsipv6AddrAddress , i4Fsipv6AddrPrefixLen ,pSetValFsipv6AddrSENDCgaModifier)   \
        nmhSetCmn(Fsipv6AddrSENDCgaModifier, 13, Fsipv6AddrSENDCgaModifierSet, Ip6Lock, Ip6UnLock, 0, 0, 3, "%i %s %i %s", i4Fsipv6AddrIndex , pFsipv6AddrAddress , i4Fsipv6AddrPrefixLen ,pSetValFsipv6AddrSENDCgaModifier)
#define nmhSetFsipv6AddrSENDCollisionCount(i4Fsipv6AddrIndex , pFsipv6AddrAddress , i4Fsipv6AddrPrefixLen ,i4SetValFsipv6AddrSENDCollisionCount)    \
        nmhSetCmn(Fsipv6AddrSENDCollisionCount, 13, Fsipv6AddrSENDCollisionCountSet, Ip6Lock, Ip6UnLock, 0, 0, 3, "%i %s %i %i", i4Fsipv6AddrIndex , pFsipv6AddrAddress , i4Fsipv6AddrPrefixLen ,i4SetValFsipv6AddrSENDCollisionCount)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsipv6AddrProfileIndex[13];
extern UINT4 Fsipv6AddrProfileStatus[13];
extern UINT4 Fsipv6AddrProfilePrefixAdvStatus[13];
extern UINT4 Fsipv6AddrProfileOnLinkAdvStatus[13];
extern UINT4 Fsipv6AddrProfileAutoConfAdvStatus[13];
extern UINT4 Fsipv6AddrProfilePreferredTime[13];
extern UINT4 Fsipv6AddrProfileValidTime[13];
extern UINT4 Fsipv6AddrProfileValidLifeTimeFlag[13];
extern UINT4 Fsipv6AddrProfilePreferredLifeTimeFlag[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsipv6AddrProfileStatus(u4Fsipv6AddrProfileIndex ,i4SetValFsipv6AddrProfileStatus) \
 nmhSetCmn(Fsipv6AddrProfileStatus, 13, Fsipv6AddrProfileStatusSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%u %i", u4Fsipv6AddrProfileIndex ,i4SetValFsipv6AddrProfileStatus)
#define nmhSetFsipv6AddrProfilePrefixAdvStatus(u4Fsipv6AddrProfileIndex ,i4SetValFsipv6AddrProfilePrefixAdvStatus) \
 nmhSetCmn(Fsipv6AddrProfilePrefixAdvStatus, 13, Fsipv6AddrProfilePrefixAdvStatusSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%u %i", u4Fsipv6AddrProfileIndex ,i4SetValFsipv6AddrProfilePrefixAdvStatus)
#define nmhSetFsipv6AddrProfileOnLinkAdvStatus(u4Fsipv6AddrProfileIndex ,i4SetValFsipv6AddrProfileOnLinkAdvStatus) \
 nmhSetCmn(Fsipv6AddrProfileOnLinkAdvStatus, 13, Fsipv6AddrProfileOnLinkAdvStatusSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%u %i", u4Fsipv6AddrProfileIndex ,i4SetValFsipv6AddrProfileOnLinkAdvStatus)
#define nmhSetFsipv6AddrProfileAutoConfAdvStatus(u4Fsipv6AddrProfileIndex ,i4SetValFsipv6AddrProfileAutoConfAdvStatus) \
 nmhSetCmn(Fsipv6AddrProfileAutoConfAdvStatus, 13, Fsipv6AddrProfileAutoConfAdvStatusSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%u %i", u4Fsipv6AddrProfileIndex ,i4SetValFsipv6AddrProfileAutoConfAdvStatus)
#define nmhSetFsipv6AddrProfilePreferredTime(u4Fsipv6AddrProfileIndex ,u4SetValFsipv6AddrProfilePreferredTime) \
 nmhSetCmn(Fsipv6AddrProfilePreferredTime, 13, Fsipv6AddrProfilePreferredTimeSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%u %u", u4Fsipv6AddrProfileIndex ,u4SetValFsipv6AddrProfilePreferredTime)
#define nmhSetFsipv6AddrProfileValidTime(u4Fsipv6AddrProfileIndex ,u4SetValFsipv6AddrProfileValidTime) \
 nmhSetCmn(Fsipv6AddrProfileValidTime, 13, Fsipv6AddrProfileValidTimeSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%u %u", u4Fsipv6AddrProfileIndex ,u4SetValFsipv6AddrProfileValidTime)
#define nmhSetFsipv6AddrProfileValidLifeTimeFlag(u4Fsipv6AddrProfileIndex ,i4SetValFsipv6AddrProfileValidLifeTimeFlag) \
 nmhSetCmn(Fsipv6AddrProfileValidLifeTimeFlag, 13, Fsipv6AddrProfileValidLifeTimeFlagSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%u %i", u4Fsipv6AddrProfileIndex ,i4SetValFsipv6AddrProfileValidLifeTimeFlag)
#define nmhSetFsipv6AddrProfilePreferredLifeTimeFlag(u4Fsipv6AddrProfileIndex ,i4SetValFsipv6AddrProfilePreferredLifeTimeFlag) \
 nmhSetCmn(Fsipv6AddrProfilePreferredLifeTimeFlag, 13, Fsipv6AddrProfilePreferredLifeTimeFlagSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%u %i", u4Fsipv6AddrProfileIndex ,i4SetValFsipv6AddrProfilePreferredLifeTimeFlag)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsipv6PmtuDest[13];
extern UINT4 Fsipv6Pmtu[13];
extern UINT4 Fsipv6PmtuAdminStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsipv6Pmtu(pFsipv6PmtuDest ,i4SetValFsipv6Pmtu) \
 nmhSetCmn(Fsipv6Pmtu, 13, Fsipv6PmtuSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%s %i", pFsipv6PmtuDest ,i4SetValFsipv6Pmtu)
#define nmhSetFsipv6PmtuAdminStatus(pFsipv6PmtuDest ,i4SetValFsipv6PmtuAdminStatus) \
 nmhSetCmn(Fsipv6PmtuAdminStatus, 13, Fsipv6PmtuAdminStatusSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%s %i", pFsipv6PmtuDest ,i4SetValFsipv6PmtuAdminStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsipv6NdLanCacheIfIndex[13];
extern UINT4 Fsipv6NdLanCacheIPv6Addr[13];
extern UINT4 Fsipv6NdLanCacheStatus[13];
extern UINT4 Fsipv6NdLanCachePhysAddr[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsipv6NdLanCacheStatus(i4Fsipv6NdLanCacheIfIndex , pFsipv6NdLanCacheIPv6Addr ,i4SetValFsipv6NdLanCacheStatus) \
 nmhSetCmn(Fsipv6NdLanCacheStatus, 13, Fsipv6NdLanCacheStatusSet, Ip6Lock, Ip6UnLock, 0, 0, 2, "%i %s %i", i4Fsipv6NdLanCacheIfIndex , pFsipv6NdLanCacheIPv6Addr ,i4SetValFsipv6NdLanCacheStatus)
#define nmhSetFsipv6NdLanCachePhysAddr(i4Fsipv6NdLanCacheIfIndex , pFsipv6NdLanCacheIPv6Addr ,pSetValFsipv6NdLanCachePhysAddr) \
 nmhSetCmn(Fsipv6NdLanCachePhysAddr, 13, Fsipv6NdLanCachePhysAddrSet, Ip6Lock, Ip6UnLock, 0, 0, 2, "%i %s %s", i4Fsipv6NdLanCacheIfIndex , pFsipv6NdLanCacheIPv6Addr ,pSetValFsipv6NdLanCachePhysAddr)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsipv6NdWanCacheIfIndex[13];
extern UINT4 Fsipv6NdWanCacheIPv6Addr[13];
extern UINT4 Fsipv6NdWanCacheStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsipv6NdWanCacheStatus(i4Fsipv6NdWanCacheIfIndex , pFsipv6NdWanCacheIPv6Addr ,i4SetValFsipv6NdWanCacheStatus) \
 nmhSetCmn(Fsipv6NdWanCacheStatus, 13, Fsipv6NdWanCacheStatusSet, Ip6Lock, Ip6UnLock, 0, 0, 2, "%i %s %i", i4Fsipv6NdWanCacheIfIndex , pFsipv6NdWanCacheIPv6Addr ,i4SetValFsipv6NdWanCacheStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsipv6PingIndex[13];
extern UINT4 Fsipv6PingDest[13];
extern UINT4 Fsipv6PingIfIndex[13];
extern UINT4 Fsipv6PingAdminStatus[13];
extern UINT4 Fsipv6PingInterval[13];
extern UINT4 Fsipv6PingRcvTimeout[13];
extern UINT4 Fsipv6PingTries[13];
extern UINT4 Fsipv6PingSize[13];
extern UINT4 Fsipv6PingData[13];
extern UINT4 Fsipv6PingSrcAddr[13];
extern UINT4 Fsipv6PingZoneId[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsipv6PingDest(i4Fsipv6PingIndex ,pSetValFsipv6PingDest) \
 nmhSetCmn(Fsipv6PingDest, 13, Fsipv6PingDestSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %s", i4Fsipv6PingIndex ,pSetValFsipv6PingDest)
#define nmhSetFsipv6PingIfIndex(i4Fsipv6PingIndex ,i4SetValFsipv6PingIfIndex) \
 nmhSetCmn(Fsipv6PingIfIndex, 13, Fsipv6PingIfIndexSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %i", i4Fsipv6PingIndex ,i4SetValFsipv6PingIfIndex)
#define nmhSetFsipv6PingAdminStatus(i4Fsipv6PingIndex ,i4SetValFsipv6PingAdminStatus) \
 nmhSetCmn(Fsipv6PingAdminStatus, 13, Fsipv6PingAdminStatusSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %i", i4Fsipv6PingIndex ,i4SetValFsipv6PingAdminStatus)
#define nmhSetFsipv6PingInterval(i4Fsipv6PingIndex ,i4SetValFsipv6PingInterval) \
 nmhSetCmn(Fsipv6PingInterval, 13, Fsipv6PingIntervalSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %i", i4Fsipv6PingIndex ,i4SetValFsipv6PingInterval)
#define nmhSetFsipv6PingRcvTimeout(i4Fsipv6PingIndex ,i4SetValFsipv6PingRcvTimeout) \
 nmhSetCmn(Fsipv6PingRcvTimeout, 13, Fsipv6PingRcvTimeoutSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %i", i4Fsipv6PingIndex ,i4SetValFsipv6PingRcvTimeout)
#define nmhSetFsipv6PingTries(i4Fsipv6PingIndex ,i4SetValFsipv6PingTries) \
 nmhSetCmn(Fsipv6PingTries, 13, Fsipv6PingTriesSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %i", i4Fsipv6PingIndex ,i4SetValFsipv6PingTries)
#define nmhSetFsipv6PingSize(i4Fsipv6PingIndex ,i4SetValFsipv6PingSize) \
 nmhSetCmn(Fsipv6PingSize, 13, Fsipv6PingSizeSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %i", i4Fsipv6PingIndex ,i4SetValFsipv6PingSize)
#define nmhSetFsipv6PingData(i4Fsipv6PingIndex ,pSetValFsipv6PingData) \
 nmhSetCmn(Fsipv6PingData, 13, Fsipv6PingDataSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %s", i4Fsipv6PingIndex ,pSetValFsipv6PingData)
#define nmhSetFsipv6PingSrcAddr(i4Fsipv6PingIndex ,pSetValFsipv6PingSrcAddr) \
 nmhSetCmn(Fsipv6PingSrcAddr, 13, Fsipv6PingSrcAddrSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %s", i4Fsipv6PingIndex ,pSetValFsipv6PingSrcAddr)
#define nmhSetFsipv6PingZoneId(i4Fsipv6PingIndex ,pSetValFsipv6PingZoneId) \
 nmhSetCmn(Fsipv6PingZoneId, 13, Fsipv6PingZoneIdSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %s", i4Fsipv6PingIndex ,pSetValFsipv6PingZoneId)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsipv6NdProxyAddr[13];
extern UINT4 Fsipv6NdProxyAdminStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsipv6NdProxyAdminStatus(pFsipv6NdProxyAddr ,i4SetValFsipv6NdProxyAdminStatus) \
 nmhSetCmn(Fsipv6NdProxyAdminStatus, 13, Fsipv6NdProxyAdminStatusSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%s %i", pFsipv6NdProxyAddr ,i4SetValFsipv6NdProxyAdminStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsipv6TunlAccessListIndex[13];
extern UINT4 Fsipv6TunlAccessListSrcAddr[13];
extern UINT4 Fsipv6TunlAccessListAdminStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsipv6TunlAccessListAdminStatus(i4Fsipv6TunlAccessListIndex , u4Fsipv6TunlAccessListSrcAddr ,i4SetValFsipv6TunlAccessListAdminStatus) \
 nmhSetCmn(Fsipv6TunlAccessListAdminStatus, 13, Fsipv6TunlAccessListAdminStatusSet, Ip6Lock, Ip6UnLock, 0, 0, 2, "%i %p %i", i4Fsipv6TunlAccessListIndex , u4Fsipv6TunlAccessListSrcAddr ,i4SetValFsipv6TunlAccessListAdminStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsipv6AddrSelPolicyPrefix[13];
extern UINT4 Fsipv6AddrSelPolicyPrefixLen[13];
extern UINT4 Fsipv6AddrSelPolicyIfIndex[13];
extern UINT4 Fsipv6AddrSelPolicyPrecedence[13];
extern UINT4 Fsipv6AddrSelPolicyLabel[13];
extern UINT4 Fsipv6AddrSelPolicyAddrType[13];
extern UINT4 Fsipv6AddrSelPolicyRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsipv6AddrSelPolicyPrecedence(pFsipv6AddrSelPolicyPrefix , i4Fsipv6AddrSelPolicyPrefixLen , i4Fsipv6AddrSelPolicyIfIndex ,i4SetValFsipv6AddrSelPolicyPrecedence) \
 nmhSetCmn(Fsipv6AddrSelPolicyPrecedence, 13, Fsipv6AddrSelPolicyPrecedenceSet, Ip6Lock, Ip6UnLock, 0, 0, 3, "%s %i %i %i", pFsipv6AddrSelPolicyPrefix , i4Fsipv6AddrSelPolicyPrefixLen , i4Fsipv6AddrSelPolicyIfIndex ,i4SetValFsipv6AddrSelPolicyPrecedence)
#define nmhSetFsipv6AddrSelPolicyLabel(pFsipv6AddrSelPolicyPrefix , i4Fsipv6AddrSelPolicyPrefixLen , i4Fsipv6AddrSelPolicyIfIndex ,i4SetValFsipv6AddrSelPolicyLabel) \
 nmhSetCmn(Fsipv6AddrSelPolicyLabel, 13, Fsipv6AddrSelPolicyLabelSet, Ip6Lock, Ip6UnLock, 0, 0, 3, "%s %i %i %i", pFsipv6AddrSelPolicyPrefix , i4Fsipv6AddrSelPolicyPrefixLen , i4Fsipv6AddrSelPolicyIfIndex ,i4SetValFsipv6AddrSelPolicyLabel)
#define nmhSetFsipv6AddrSelPolicyAddrType(pFsipv6AddrSelPolicyPrefix , i4Fsipv6AddrSelPolicyPrefixLen , i4Fsipv6AddrSelPolicyIfIndex ,i4SetValFsipv6AddrSelPolicyAddrType) \
 nmhSetCmn(Fsipv6AddrSelPolicyAddrType, 13, Fsipv6AddrSelPolicyAddrTypeSet, Ip6Lock, Ip6UnLock, 0, 0, 3, "%s %i %i %i", pFsipv6AddrSelPolicyPrefix , i4Fsipv6AddrSelPolicyPrefixLen , i4Fsipv6AddrSelPolicyIfIndex ,i4SetValFsipv6AddrSelPolicyAddrType)
#define nmhSetFsipv6AddrSelPolicyRowStatus(pFsipv6AddrSelPolicyPrefix , i4Fsipv6AddrSelPolicyPrefixLen , i4Fsipv6AddrSelPolicyIfIndex ,i4SetValFsipv6AddrSelPolicyRowStatus) \
 nmhSetCmn(Fsipv6AddrSelPolicyRowStatus, 13, Fsipv6AddrSelPolicyRowStatusSet, Ip6Lock, Ip6UnLock, 0, 1, 3, "%s %i %i %i", pFsipv6AddrSelPolicyPrefix , i4Fsipv6AddrSelPolicyPrefixLen , i4Fsipv6AddrSelPolicyIfIndex ,i4SetValFsipv6AddrSelPolicyRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsipv6ScopeZoneIndexIfIndex[13];
extern UINT4 Fsipv6ScopeZoneIndexInterfaceLocal[13];
extern UINT4 Fsipv6ScopeZoneIndexLinkLocal[13];
extern UINT4 Fsipv6ScopeZoneIndex3[13];
extern UINT4 Fsipv6ScopeZoneIndexAdminLocal[13];
extern UINT4 Fsipv6ScopeZoneIndexSiteLocal[13];
extern UINT4 Fsipv6ScopeZoneIndex6[13];
extern UINT4 Fsipv6ScopeZoneIndex7[13];
extern UINT4 Fsipv6ScopeZoneIndexOrganizationLocal[13];
extern UINT4 Fsipv6ScopeZoneIndex9[13];
extern UINT4 Fsipv6ScopeZoneIndexA[13];
extern UINT4 Fsipv6ScopeZoneIndexB[13];
extern UINT4 Fsipv6ScopeZoneIndexC[13];
extern UINT4 Fsipv6ScopeZoneIndexD[13];
extern UINT4 Fsipv6ScopeZoneIndexE[13];
extern UINT4 Fsipv6IfScopeZoneRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsipv6ScopeZoneIndexInterfaceLocal(i4Fsipv6ScopeZoneIndexIfIndex ,pSetValFsipv6ScopeZoneIndexInterfaceLocal) \
 nmhSetCmn(Fsipv6ScopeZoneIndexInterfaceLocal, 13, Fsipv6ScopeZoneIndexInterfaceLocalSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %s", i4Fsipv6ScopeZoneIndexIfIndex ,pSetValFsipv6ScopeZoneIndexInterfaceLocal)
#define nmhSetFsipv6ScopeZoneIndexLinkLocal(i4Fsipv6ScopeZoneIndexIfIndex ,pSetValFsipv6ScopeZoneIndexLinkLocal) \
 nmhSetCmn(Fsipv6ScopeZoneIndexLinkLocal, 13, Fsipv6ScopeZoneIndexLinkLocalSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %s", i4Fsipv6ScopeZoneIndexIfIndex ,pSetValFsipv6ScopeZoneIndexLinkLocal)
#define nmhSetFsipv6ScopeZoneIndex3(i4Fsipv6ScopeZoneIndexIfIndex ,pSetValFsipv6ScopeZoneIndex3) \
 nmhSetCmn(Fsipv6ScopeZoneIndex3, 13, Fsipv6ScopeZoneIndex3Set, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %s", i4Fsipv6ScopeZoneIndexIfIndex ,pSetValFsipv6ScopeZoneIndex3)
#define nmhSetFsipv6ScopeZoneIndexAdminLocal(i4Fsipv6ScopeZoneIndexIfIndex ,pSetValFsipv6ScopeZoneIndexAdminLocal) \
 nmhSetCmn(Fsipv6ScopeZoneIndexAdminLocal, 13, Fsipv6ScopeZoneIndexAdminLocalSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %s", i4Fsipv6ScopeZoneIndexIfIndex ,pSetValFsipv6ScopeZoneIndexAdminLocal)
#define nmhSetFsipv6ScopeZoneIndexSiteLocal(i4Fsipv6ScopeZoneIndexIfIndex ,pSetValFsipv6ScopeZoneIndexSiteLocal) \
 nmhSetCmn(Fsipv6ScopeZoneIndexSiteLocal, 13, Fsipv6ScopeZoneIndexSiteLocalSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %s", i4Fsipv6ScopeZoneIndexIfIndex ,pSetValFsipv6ScopeZoneIndexSiteLocal)
#define nmhSetFsipv6ScopeZoneIndex6(i4Fsipv6ScopeZoneIndexIfIndex ,pSetValFsipv6ScopeZoneIndex6) \
 nmhSetCmn(Fsipv6ScopeZoneIndex6, 13, Fsipv6ScopeZoneIndex6Set, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %s", i4Fsipv6ScopeZoneIndexIfIndex ,pSetValFsipv6ScopeZoneIndex6)
#define nmhSetFsipv6ScopeZoneIndex7(i4Fsipv6ScopeZoneIndexIfIndex ,pSetValFsipv6ScopeZoneIndex7) \
 nmhSetCmn(Fsipv6ScopeZoneIndex7, 13, Fsipv6ScopeZoneIndex7Set, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %s", i4Fsipv6ScopeZoneIndexIfIndex ,pSetValFsipv6ScopeZoneIndex7)
#define nmhSetFsipv6ScopeZoneIndexOrganizationLocal(i4Fsipv6ScopeZoneIndexIfIndex ,pSetValFsipv6ScopeZoneIndexOrganizationLocal) \
 nmhSetCmn(Fsipv6ScopeZoneIndexOrganizationLocal, 13, Fsipv6ScopeZoneIndexOrganizationLocalSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %s", i4Fsipv6ScopeZoneIndexIfIndex ,pSetValFsipv6ScopeZoneIndexOrganizationLocal)
#define nmhSetFsipv6ScopeZoneIndex9(i4Fsipv6ScopeZoneIndexIfIndex ,pSetValFsipv6ScopeZoneIndex9) \
 nmhSetCmn(Fsipv6ScopeZoneIndex9, 13, Fsipv6ScopeZoneIndex9Set, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %s", i4Fsipv6ScopeZoneIndexIfIndex ,pSetValFsipv6ScopeZoneIndex9)
#define nmhSetFsipv6ScopeZoneIndexA(i4Fsipv6ScopeZoneIndexIfIndex ,pSetValFsipv6ScopeZoneIndexA) \
 nmhSetCmn(Fsipv6ScopeZoneIndexA, 13, Fsipv6ScopeZoneIndexASet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %s", i4Fsipv6ScopeZoneIndexIfIndex ,pSetValFsipv6ScopeZoneIndexA)
#define nmhSetFsipv6ScopeZoneIndexB(i4Fsipv6ScopeZoneIndexIfIndex ,pSetValFsipv6ScopeZoneIndexB) \
 nmhSetCmn(Fsipv6ScopeZoneIndexB, 13, Fsipv6ScopeZoneIndexBSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %s", i4Fsipv6ScopeZoneIndexIfIndex ,pSetValFsipv6ScopeZoneIndexB)
#define nmhSetFsipv6ScopeZoneIndexC(i4Fsipv6ScopeZoneIndexIfIndex ,pSetValFsipv6ScopeZoneIndexC) \
 nmhSetCmn(Fsipv6ScopeZoneIndexC, 13, Fsipv6ScopeZoneIndexCSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %s", i4Fsipv6ScopeZoneIndexIfIndex ,pSetValFsipv6ScopeZoneIndexC)
#define nmhSetFsipv6ScopeZoneIndexD(i4Fsipv6ScopeZoneIndexIfIndex ,pSetValFsipv6ScopeZoneIndexD) \
 nmhSetCmn(Fsipv6ScopeZoneIndexD, 13, Fsipv6ScopeZoneIndexDSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %s", i4Fsipv6ScopeZoneIndexIfIndex ,pSetValFsipv6ScopeZoneIndexD)
#define nmhSetFsipv6ScopeZoneIndexE(i4Fsipv6ScopeZoneIndexIfIndex ,pSetValFsipv6ScopeZoneIndexE) \
 nmhSetCmn(Fsipv6ScopeZoneIndexE, 13, Fsipv6ScopeZoneIndexESet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %s", i4Fsipv6ScopeZoneIndexIfIndex ,pSetValFsipv6ScopeZoneIndexE)
#define nmhSetFsipv6IfScopeZoneRowStatus(i4Fsipv6ScopeZoneIndexIfIndex ,i4SetValFsipv6IfScopeZoneRowStatus) \
 nmhSetCmn(Fsipv6IfScopeZoneRowStatus, 13, Fsipv6IfScopeZoneRowStatusSet, Ip6Lock, Ip6UnLock, 0, 1, 1, "%i %i", i4Fsipv6ScopeZoneIndexIfIndex ,i4SetValFsipv6IfScopeZoneRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsipv6ScopeZoneName[13];
extern UINT4 Fsipv6IsDefaultScopeZone[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsipv6IsDefaultScopeZone(pFsipv6ScopeZoneName ,i4SetValFsipv6IsDefaultScopeZone) \
 nmhSetCmn(Fsipv6IsDefaultScopeZone, 13, Fsipv6IsDefaultScopeZoneSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%s %i", pFsipv6ScopeZoneName ,i4SetValFsipv6IsDefaultScopeZone)

#endif

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsipv6RARouteIfIndex[13];
extern UINT4 Fsipv6RARoutePrefix[13];
extern UINT4 Fsipv6RARoutePrefixLen[13];
extern UINT4 Fsipv6RARoutePreference[13];
extern UINT4 Fsipv6RARouteLifetime[13];
extern UINT4 Fsipv6RARouteRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsipv6RARoutePreference(i4Fsipv6RARouteIfIndex , pFsipv6RARoutePrefix , i4Fsipv6RARoutePrefixLen ,i4SetValFsipv6RARoutePreference) \
 nmhSetCmn(Fsipv6RARoutePreference, 13, Fsipv6RARoutePreferenceSet, Ip6Lock, Ip6UnLock, 0, 0, 3, "%i %s %i %i", i4Fsipv6RARouteIfIndex , pFsipv6RARoutePrefix , i4Fsipv6RARoutePrefixLen ,i4SetValFsipv6RARoutePreference)
#define nmhSetFsipv6RARouteLifetime(i4Fsipv6RARouteIfIndex , pFsipv6RARoutePrefix , i4Fsipv6RARoutePrefixLen ,u4SetValFsipv6RARouteLifetime) \
 nmhSetCmn(Fsipv6RARouteLifetime, 13, Fsipv6RARouteLifetimeSet, Ip6Lock, Ip6UnLock, 0, 0, 3, "%i %s %i %u", i4Fsipv6RARouteIfIndex , pFsipv6RARoutePrefix , i4Fsipv6RARoutePrefixLen ,u4SetValFsipv6RARouteLifetime)
#define nmhSetFsipv6RARouteRowStatus(i4Fsipv6RARouteIfIndex , pFsipv6RARoutePrefix , i4Fsipv6RARoutePrefixLen ,i4SetValFsipv6RARouteRowStatus) \
 nmhSetCmn(Fsipv6RARouteRowStatus, 13, Fsipv6RARouteRowStatusSet, Ip6Lock, Ip6UnLock, 0, 1, 3, "%i %s %i %i", i4Fsipv6RARouteIfIndex , pFsipv6RARoutePrefix , i4Fsipv6RARoutePrefixLen ,i4SetValFsipv6RARouteRowStatus)

#endif
extern UINT4 Fsipv6RouteDest[13];
extern UINT4 Fsipv6RoutePfxLength[13];
extern UINT4 Fsipv6RouteProtocol[13];
extern UINT4 Fsipv6RouteNextHop[13];
extern UINT4 Fsipv6RouteIfIndex[13];
extern UINT4 Fsipv6RouteMetric[13];
extern UINT4 Fsipv6RouteType[13];
extern UINT4 Fsipv6RouteTag[13];
extern UINT4 Fsipv6RouteAdminStatus[13];
extern UINT4 Fsipv6RoutePreference[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsipv6RouteIfIndex(pFsipv6RouteDest , i4Fsipv6RoutePfxLength , i4Fsipv6RouteProtocol , pFsipv6RouteNextHop ,i4SetValFsipv6RouteIfIndex) \
 nmhSetCmn(Fsipv6RouteIfIndex, 13, Fsipv6RouteIfIndexSet, Ip6Lock, Ip6UnLock, 0, 0, 4, "%s %i %i %s %i", pFsipv6RouteDest , i4Fsipv6RoutePfxLength , i4Fsipv6RouteProtocol , pFsipv6RouteNextHop ,i4SetValFsipv6RouteIfIndex)
#define nmhSetFsipv6RouteMetric(pFsipv6RouteDest , i4Fsipv6RoutePfxLength , i4Fsipv6RouteProtocol , pFsipv6RouteNextHop ,u4SetValFsipv6RouteMetric) \
 nmhSetCmn(Fsipv6RouteMetric, 13, Fsipv6RouteMetricSet, Ip6Lock, Ip6UnLock, 0, 0, 4, "%s %i %i %s %u", pFsipv6RouteDest , i4Fsipv6RoutePfxLength , i4Fsipv6RouteProtocol , pFsipv6RouteNextHop ,u4SetValFsipv6RouteMetric)
#define nmhSetFsipv6RouteType(pFsipv6RouteDest , i4Fsipv6RoutePfxLength , i4Fsipv6RouteProtocol , pFsipv6RouteNextHop ,i4SetValFsipv6RouteType) \
 nmhSetCmn(Fsipv6RouteType, 13, Fsipv6RouteTypeSet, Ip6Lock, Ip6UnLock, 0, 0, 4, "%s %i %i %s %i", pFsipv6RouteDest , i4Fsipv6RoutePfxLength , i4Fsipv6RouteProtocol , pFsipv6RouteNextHop ,i4SetValFsipv6RouteType)
#define nmhSetFsipv6RouteTag(pFsipv6RouteDest , i4Fsipv6RoutePfxLength , i4Fsipv6RouteProtocol , pFsipv6RouteNextHop ,u4SetValFsipv6RouteTag) \
 nmhSetCmn(Fsipv6RouteTag, 13, Fsipv6RouteTagSet, Ip6Lock, Ip6UnLock, 0, 0, 4, "%s %i %i %s %u", pFsipv6RouteDest , i4Fsipv6RoutePfxLength , i4Fsipv6RouteProtocol , pFsipv6RouteNextHop ,u4SetValFsipv6RouteTag)
#define nmhSetFsipv6RouteAdminStatus(pFsipv6RouteDest , i4Fsipv6RoutePfxLength , i4Fsipv6RouteProtocol , pFsipv6RouteNextHop ,i4SetValFsipv6RouteAdminStatus) \
 nmhSetCmn(Fsipv6RouteAdminStatus, 13, Fsipv6RouteAdminStatusSet, Ip6Lock, Ip6UnLock, 0, 1, 4, "%s %i %i %s %i", pFsipv6RouteDest , i4Fsipv6RoutePfxLength , i4Fsipv6RouteProtocol , pFsipv6RouteNextHop ,i4SetValFsipv6RouteAdminStatus)

#define nmhSetFsipv6RoutePreference(pFsipv6RouteDest , i4Fsipv6RoutePfxLength , i4Fsipv6RouteProtocol , pFsipv6RouteNextHop ,i4SetValFsipv6RoutePreference) \
  nmhSetCmn(Fsipv6RoutePreference, 13, Fsipv6RoutePreferenceSet, Ip6Lock, Ip6UnLock, 0, 0, 4, "%s %i %i %s %i", pFsipv6RouteDest , i4Fsipv6RoutePfxLength , i4Fsipv6RouteProtocol , pFsipv6RouteNextHop ,i4SetValFsipv6RoutePreference)
#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsipv6Protocol[13];
extern UINT4 Fsipv6Preference[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsipv6Preference(i4Fsipv6Protocol ,u4SetValFsipv6Preference) \
 nmhSetCmn(Fsipv6Preference, 13, Fsipv6PreferenceSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %u", i4Fsipv6Protocol ,u4SetValFsipv6Preference)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsipv6IfRaRDNSSIndex[13];
extern UINT4 Fsipv6IfRadvRDNSSOpen[13];
extern UINT4 Fsipv6IfRaRDNSSPreference[13];
extern UINT4 Fsipv6IfRaRDNSSLifetime[13];
extern UINT4 Fsipv6IfRaRDNSSAddrOne[13];
extern UINT4 Fsipv6IfRaRDNSSAddrTwo[13];
extern UINT4 Fsipv6IfRaRDNSSAddrThree[13];
extern UINT4 Fsipv6IfRaRDNSSRowStatus[13];
extern UINT4 Fsipv6IfDomainNameOne[13];
extern UINT4 Fsipv6IfDomainNameTwo[13];
extern UINT4 Fsipv6IfDomainNameThree[13];
extern UINT4 Fsipv6IfDnsLifeTime[13];
extern UINT4 Fsipv6IfRaRDNSSAddrOneLife[13];
extern UINT4 Fsipv6IfRaRDNSSAddrTwoLife[13];
extern UINT4 Fsipv6IfRaRDNSSAddrThreeLife[13];
extern UINT4 Fsipv6IfDomainNameOneLife[13];
extern UINT4 Fsipv6IfDomainNameTwoLife[13];
extern UINT4 Fsipv6IfDomainNameThreeLife[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsipv6IfRaRDNSSIndex(i4Fsipv6IfIndex ,i4SetValFsipv6IfRaRDNSSIndex) \
 nmhSetCmnWithLock(Fsipv6IfRaRDNSSIndex, 13, Fsipv6IfRaRDNSSIndexSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %i", i4Fsipv6IfIndex ,i4SetValFsipv6IfRaRDNSSIndex)
#define nmhSetFsipv6IfRadvRDNSSOpen(i4Fsipv6IfIndex ,i4SetValFsipv6IfRadvRDNSSOpen) \
 nmhSetCmnWithLock(Fsipv6IfRadvRDNSSOpen, 13, Fsipv6IfRadvRDNSSOpenSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %i", i4Fsipv6IfIndex ,i4SetValFsipv6IfRadvRDNSSOpen)
#define nmhSetFsipv6IfRaRDNSSPreference(i4Fsipv6IfIndex ,i4SetValFsipv6IfRaRDNSSPreference) \
 nmhSetCmnWithLock(Fsipv6IfRaRDNSSPreference, 13, Fsipv6IfRaRDNSSPreferenceSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %i", i4Fsipv6IfIndex ,i4SetValFsipv6IfRaRDNSSPreference)
#define nmhSetFsipv6IfRaRDNSSLifetime(i4Fsipv6IfIndex ,u4SetValFsipv6IfRaRDNSSLifetime) \
 nmhSetCmnWithLock(Fsipv6IfRaRDNSSLifetime, 13, Fsipv6IfRaRDNSSLifetimeSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %u", i4Fsipv6IfIndex ,u4SetValFsipv6IfRaRDNSSLifetime)
#define nmhSetFsipv6IfRaRDNSSAddrOne(i4Fsipv6IfIndex ,pSetValFsipv6IfRaRDNSSAddrOne) \
 nmhSetCmnWithLock(Fsipv6IfRaRDNSSAddrOne, 13, Fsipv6IfRaRDNSSAddrOneSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %s", i4Fsipv6IfIndex ,pSetValFsipv6IfRaRDNSSAddrOne)
#define nmhSetFsipv6IfRaRDNSSAddrTwo(i4Fsipv6IfIndex ,pSetValFsipv6IfRaRDNSSAddrTwo) \
 nmhSetCmnWithLock(Fsipv6IfRaRDNSSAddrTwo, 13, Fsipv6IfRaRDNSSAddrTwoSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %s", i4Fsipv6IfIndex ,pSetValFsipv6IfRaRDNSSAddrTwo)
#define nmhSetFsipv6IfRaRDNSSAddrThree(i4Fsipv6IfIndex ,pSetValFsipv6IfRaRDNSSAddrThree) \
 nmhSetCmnWithLock(Fsipv6IfRaRDNSSAddrThree, 13, Fsipv6IfRaRDNSSAddrThreeSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %s", i4Fsipv6IfIndex ,pSetValFsipv6IfRaRDNSSAddrThree)
#define nmhSetFsipv6IfRaRDNSSRowStatus(i4Fsipv6IfIndex ,i4SetValFsipv6IfRaRDNSSRowStatus) \
 nmhSetCmnWithLock(Fsipv6IfRaRDNSSRowStatus, 13, Fsipv6IfRaRDNSSRowStatusSet, Ip6Lock, Ip6UnLock, 0, 1, 1, "%i %i", i4Fsipv6IfIndex ,i4SetValFsipv6IfRaRDNSSRowStatus)
#define nmhSetFsipv6IfDomainNameOne(i4Fsipv6IfIndex ,pSetValFsipv6IfDomainNameOne) \
 nmhSetCmnWithLock(Fsipv6IfDomainNameOne, 13, Fsipv6IfDomainNameOneSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %s", i4Fsipv6IfIndex ,pSetValFsipv6IfDomainNameOne)
#define nmhSetFsipv6IfDomainNameTwo(i4Fsipv6IfIndex ,pSetValFsipv6IfDomainNameTwo) \
 nmhSetCmnWithLock(Fsipv6IfDomainNameTwo, 13, Fsipv6IfDomainNameTwoSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %s", i4Fsipv6IfIndex ,pSetValFsipv6IfDomainNameTwo)
#define nmhSetFsipv6IfDomainNameThree(i4Fsipv6IfIndex ,pSetValFsipv6IfDomainNameThree) \
 nmhSetCmnWithLock(Fsipv6IfDomainNameThree, 13, Fsipv6IfDomainNameThreeSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %s", i4Fsipv6IfIndex ,pSetValFsipv6IfDomainNameThree)
#define nmhSetFsipv6IfDnsLifeTime(i4Fsipv6IfIndex ,i4SetValFsipv6IfDnsLifeTime) \
 nmhSetCmnWithLock(Fsipv6IfDnsLifeTime, 13, Fsipv6IfDnsLifeTimeSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %i", i4Fsipv6IfIndex ,i4SetValFsipv6IfDnsLifeTime)
#define nmhSetFsipv6IfRaRDNSSAddrOneLife(i4Fsipv6IfIndex ,u4SetValFsipv6IfRaRDNSSAddrOneLife) \
 nmhSetCmnWithLock(Fsipv6IfRaRDNSSAddrOneLife, 13, Fsipv6IfRaRDNSSAddrOneLifeSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %u", i4Fsipv6IfIndex ,u4SetValFsipv6IfRaRDNSSAddrOneLife)
#define nmhSetFsipv6IfRaRDNSSAddrTwoLife(i4Fsipv6IfIndex ,u4SetValFsipv6IfRaRDNSSAddrTwoLife) \
 nmhSetCmnWithLock(Fsipv6IfRaRDNSSAddrTwoLife, 13, Fsipv6IfRaRDNSSAddrTwoLifeSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %u", i4Fsipv6IfIndex ,u4SetValFsipv6IfRaRDNSSAddrTwoLife)
#define nmhSetFsipv6IfRaRDNSSAddrThreeLife(i4Fsipv6IfIndex ,u4SetValFsipv6IfRaRDNSSAddrThreeLife) \
 nmhSetCmnWithLock(Fsipv6IfRaRDNSSAddrThreeLife, 13, Fsipv6IfRaRDNSSAddrThreeLifeSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %u", i4Fsipv6IfIndex ,u4SetValFsipv6IfRaRDNSSAddrThreeLife)
#define nmhSetFsipv6IfDomainNameOneLife(i4Fsipv6IfIndex ,u4SetValFsipv6IfDomainNameOneLife) \
 nmhSetCmnWithLock(Fsipv6IfDomainNameOneLife, 13, Fsipv6IfDomainNameOneLifeSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %u", i4Fsipv6IfIndex ,u4SetValFsipv6IfDomainNameOneLife)
#define nmhSetFsipv6IfDomainNameTwoLife(i4Fsipv6IfIndex ,u4SetValFsipv6IfDomainNameTwoLife) \
 nmhSetCmnWithLock(Fsipv6IfDomainNameTwoLife, 13, Fsipv6IfDomainNameTwoLifeSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %u", i4Fsipv6IfIndex ,u4SetValFsipv6IfDomainNameTwoLife)
#define nmhSetFsipv6IfDomainNameThreeLife(i4Fsipv6IfIndex ,u4SetValFsipv6IfDomainNameThreeLife) \
 nmhSetCmnWithLock(Fsipv6IfDomainNameThreeLife, 13, Fsipv6IfDomainNameThreeLifeSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %u", i4Fsipv6IfIndex ,u4SetValFsipv6IfDomainNameThreeLife)

#endif
