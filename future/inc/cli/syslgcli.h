/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: syslgcli.h,v 1.18 2017/12/28 10:40:14 siva Exp $
 *
 * Description:This file contains  syslog cli definitions
 *
 *******************************************************************/
#ifndef __SYSLGCLI_H__
#define __SYSLGCLI_H__

#include "cli.h"
#include "ip6util.h"

/* Command Identifiers */
enum {
    CLI_SYSLG_SHOW_LOGGING = 1,    
    CLI_SYSLG_SHOW_EMAIL_ALERT,    
    CLI_SYSLG_SET_LOGGING,
    CLI_SYSLG_NO_LOGGING,
    CLI_SYSLG_SET_SNDR_MAIL,
    CLI_SYSLG_NO_SNDR_MAIL,
    CLI_SYSLG_SET_CMDBUFF,
    CLI_SYSLG_CLEAR_LOGS,
    CLI_SYSLG_ROLE,
    CLI_SYSLG_RELAY_TRANS_TYPE,
    CLI_SHOW_SYSLG_REL_TRANSTYPE,
    CLI_NO_SYSLG_ROLE,
    CLI_SHW_SYSLG_ROLE,
    CLI_SYSLG_MAIL,
    CLI_NO_SYSLG_MAIL,
    CLI_SHW_SYSLG_MAIL,
    CLI_SYSLG_LOCALSTORAGE,
    CLI_NO_SYSLG_LOCALSTORAGE,
    CLI_SHW_SYSLG_LOCALSTORAGE,
    CLI_SYSLG_PORT,
    CLI_NO_SYSLG_PORT,
    CLI_SHOW_SYSLG_PORT,
    CLI_SYSLG_PROFILE,
    CLI_NO_SYSLG_PROFILE,
    CLI_SHOW_SYSLG_PROFILE,
    CLI_SYSLG_LOGGING_FILE,
    CLI_SHW_SYSLG_LOGGINGFILE,
    CLI_SYSLG_NO_LOGGING_FILE,
    CLI_SYSLG_LOGGING_FILESIZE,
    CLI_SYSLG_LOGGING_SERVER,
    CLI_SHW_SYSLG_LOGGINGSERVER,
    CLI_SYSLG_NO_LOGGING_SERVER,
    CLI_SYSLG_MAIL_SERVER,
    CLI_SHW_SYSLG_MAILSERVER,
    CLI_SYSLG_NO_MAILSERVER,
    CLI_SYSLG_FILENAME_ONE,
    CLI_SYSLG_FILENAME_TWO,
    CLI_SYSLG_FILENAME_THREE,
    CLI_SHOW_SYSLG_INFORMATION,
    CLI_SHOW_SYSLG_FILE_NAME,
    CLI_SYSLG_SMTP_AUTH,
    CLI_SYSLG_MAX_COMMANDS,
    CLI_SYSLG_SNMP_TRAP
};


#define LOGGING_SERVER_IP          0x03
#define LOGGING_BUFFER_SIZE        0x04
#define LOGGING_CONSOLE            0x05
#define LOGGING_FACILITY           0x06
#define LOGGING_SEVERITY           0x07
#define DEF_SYSLOG_MAX_BUFF        50
#define SNMP_TRAP_ENABLE  1
#define SNMP_TRAP_DISABLE 2

/* Error codes 
 * Error code values and strings are maintained inside the protocol module itself.
 */
enum {
    CLI_SYSLG_RCVR_MAIL_ERR = 1,
    CLI_SYSLG_SNDR_MAIL_ERR, 
    CLI_SYSLG_INVALID_IP,
    CLI_SYSLG_INV_BUF_SIZE,
    CLI_SYSLG_INV_STATUS,
    CLI_SYSLG_INV_NOLOG_IP,
    CLI_SYSLG_INV_LOCAL_IP,
    CLI_SYSLG_INV_SEVERITY_LEVEL,
    CLI_SYSLG_INV_FACILITY,
    CLI_SYSLG_INV_DIRECT_IP,
    CLI_SYSLG_INV_FILE_NAME,
    CLI_SYSLG_NO_MATCH_FILE_NAME,
    CLI_SYSLG_MAX_DOMAIN_LEN,
    CLI_SYSLG_CREATE_ERR,
    CLI_SYSLG_INVALID_IP_SERVER,
    CLI_SYSLG_MAX_MAIL_SERVER_REACHED,
    CLI_SYSLG_MAX_LOGGING_SERVER_REACHED,
    CLI_SYSLG_INV_SIZE,
    CLI_SYSLG_MAX_ERR 
};

/* The error strings should be places under the switch so as to avoid redifinition
 * This will be visible only in modulecli.c file
 */

#ifdef __SYSLGCLI_C__

CONST CHR1  *SysLogCliErrString [] = {
    NULL,
    "\r% Receiver mail-id error\r\n",
    "\r% Sender mail-id error\r\n",
    "\r% Server IP address does not belong to CLASS A, CLASS B or CLASS C.\r\n",
    "\r% Invalid buffer size\r\n",
    "\r% Invalid status option\r\n",
    "\r% Host not found for logging\r\n",
    "\r% Server IP address should be in the same subnet as one of directly connected interfaces.\r\n",
    "\r% Invalid severity level\r\n",
    "\r% Invalid facility value\r\n",
    "\r% Server IP address cannot be a directly connected interface address.\r\n",
    "\r% Invalid file name\r\n",
    "\r% No Matching Syslog file name found\r\n",
    "\r% Maximum domain name length exceeded\r\n",
    "\r% Maximum allowed sessions for syslog exceeded \r\n",
    "\r% Invalid Server Address\r\n",
    "\r% Maximum Mail Server Limit Exceeded\r\n",
    "\r% Maximum Logging Server Limit Exceeded\r\n",
    "\r% Invalid File Size\r\n"
    "\r\n"
};

#endif

/* SMTP authentication types */
#define SMTP_NO_AUTHENTICATE    1
#define SMTP_AUTH_LOGIN         2
#define SMTP_AUTH_PLAIN         3
#define SMTP_AUTH_CRAM_MD5      4
#define SMTP_AUTH_DIGEST_MD5    5

/*Logging file sizes*/
#define SYSLOG_MIN_FILESIZE    512
#define SYSLOG_MAX_FILESIZE    4194304

INT4 cli_process_syslg_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command, ...));

#ifdef SYSLOG_TEST_WANTED
INT4 cli_process_syslog_test_cmd PROTO ((tCliHandle CliHandle, ...));
#endif

#endif /* __SYSLGCLI_H__ */
