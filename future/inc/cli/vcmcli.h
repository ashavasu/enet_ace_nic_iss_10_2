/********************************************************************
 ** Copyright (C) 2006 Aricent Inc . All Rights Reserved
 **
 ** $Id: vcmcli.h,v 1.23 2017/11/14 07:31:11 siva Exp $
 **
 ** Description: This file contains the data structure and macros
 **              for VCM CLI module
 **
 ********************************************************************/

#ifndef __VCMCLI_H__
#define __VCMCLI_H__

#include "cli.h"

#define VCM_MAX_ARGS             6

#define CLI_VCM_MODE             "VCM"

#define CLI_VCM_SET_CMD          OSIX_TRUE
#define CLI_VCM_NO_CMD           OSIX_FALSE
#define VCM_CLI_ALL_ENABLE_TRC   0x000000ff

#define CLI_VCM_MAP              0
#define CLI_VCM_UNMAP            1

/* CLI related macros for IP interface mapping */
#ifdef VRF_WANTED
#define VCM_IPIFMAP_ADD          1
#define VCM_IPIFMAP_DELETE       2
#define VCM_SHOW_IP_ALL          3
#define VCM_SHOW_IP_CONTEXT      4
#define VCM_SHOW_IP_BRIEF        5
#define VCM_SHOW_IP_DETAIL       6
#define VCM_SHOW_IP_INTERFACE    7
#endif
enum
{
    CLI_VCM_SHOW_BRIEF= 1,
    CLI_VCM_SHOW_DETAIL,
    CLI_VCM_SHOW_INTERFACES,
    CLI_VCM_SHOW_ALL,
    CLI_VCM_SHOW_CTX_MAPPING,
    CLI_VCM_CREATE_OR_DELETE,
    CLI_VCM_MAP_PORTS,
    CLI_VCM_UNMAP_PORTS,
#ifdef VRF_WANTED
    CLI_VCM_MAP_IP_INTERFACE,
    CLI_VCM_UNMAP_IP_INTERFACE,
    CLI_VCM_SHOW_IP_INTERFACE,
#endif
    CLI_VCM_TRACE,
    CLI_VCM_SHOW_OWNER,
    CLI_VCM_CREATE_OWNER,
    CLI_VCM_VRF_CREATE,
    CLI_VCM_VRF_DELETE,
    CLI_VCM_SHOW_VRF_BRIEF,
    CLI_VCM_SHOW_VRF_DETAIL,
    CLI_VCM_SHOW_VRF_INTERFACE,
    CLI_VCM_SHOW_VRF_ALL,
    CLI_VCM_VRF_STAT_STATUS,
    CLI_VCM_VRF_STAT_CLEAR,
    CLI_VCM_SHOW_VRF_COUNTERS
};

enum {
    CLI_VCM_UNKNOWN_ERR = CLI_ERR_START_ID_VCM,
    CLI_VCM_MAP_BRG_MODE_ERR,
    CLI_VCM_MAP_PBB_BRG_MODE_ERR,
    CLI_VCM_CXT_ALIAS_LEN_ERR,
    CLI_VCM_MAX_L3_CXT_ERR,
    CLI_VCM_IFACE_MAPPED_ERR,
    CLI_VCM_CXT_UNDER_CREATION_ERR,
    CLI_VCM_L3IFACE_L2CXT_MAP_ERR,
    CLI_VCM_L2IFACE_L3CXT_MAP_ERR,
    CLI_VCM_IPIF_MAP_ERR,
    CLI_VCM_SISP_LOG_PORT_EXIST_ERR,
    CLI_VCM_LAGG_CONFIG_ERR,
    CLI_VCM_MAP_ICCL_MCLAG_ERR,
    CLI_VCM_SCHANNEL_CXT_MAP_ERR,
    CLI_VCM_L3SUBIF_EXIST_ERR,

    CLI_VCM_INVLAID_VC_ID,
    CLI_VCM_VC_ERROR,
    CLI_VRF_COUNTER_STATUS,
    CLI_VRF_COUNTER_RESET,
    CLI_VRF_STAT_CLEAR,
    CLI_VRF_STAT_SET,
    CLI_VCM_MAX_ERR
};

/* Accessing the ErrString with ErrCode value.
 * Since the ErrCode's starting Id is changed, this forumla is used
 * to access the Err String */
#define CLI_ERR_OFFSET_VCM(u4ErrCode)   (u4ErrCode - CLI_ERR_START_ID_VCM + 1)

#ifdef __VCMCLI_C__ 

CONST CHR1  *VcmCliErrString [] = {
    NULL,
    "Unknown Error\r\n",
    "Port cannot be mapped to a context whose bridge mode is not set.\r\n",
    "Virtual Port cannot be mapped to a context whose bridge mode is not PBB.\r\n",
    "Context Alias name exceeds the allowed length.\r\n",
    "Maximum L3 context is already created in the system.\r\n",
    "Unmap all the interfaces mapped to the context before deleting the context.\r\n",
    "Context is under creation. Create the context before mapping the interface.\r\n",
    "L3 interface can be mapped to L3 context only.\r\n",
    "L2 interface can be mapped to L2 context only.\r\n",
    "Unmap the interface before mapping it to a new vrf.\r\n",
    "Port can not be unmapped, Sisp ports exists for this port\r\n",
    "Port can not be unmapped, mirroring configured for this port\r\n",
    "ICCL or MCLAG Port cannot be mapped to a non-default context\r\n",
    "Mapping an S-Channel interface to a context is not allowed\r\n",
    "Port can not be unmapped, L3subinterface ports exists for this port\r\n",
    "Invalid VC Id\r\n",
    "VC Id doesn't exist in the system\r\n",
    "Invalid VRF counter status\r\n",
    "Invalid VRF reset value\r\n",
    "VRF statistics reset failed\r\n",
    "VRF statistics set failed\r\n",
    "\r\n"
};

#else
extern CONST CHR1  *VcmCliErrString [];
#endif

/* Macros related to CLI - Needs porting depending upon the CLI framework*/
#define VCM_CLI_PRINTF               CliPrintf
#define CLI_ERR_WRONG_VALUE          SNMP_ERR_WRONG_VALUE

INT4
cli_process_vcm_cmd PROTO ((tCliHandle CliHandle, UINT4, ...));

INT4 
VcmShowCommand PROTO ((tCliHandle , UINT1 *, UINT4));


INT4
VcmCreateorDeleteContext PROTO ((tCliHandle , UINT1 *, UINT1));

INT4 
VcmMapPort PROTO ((tCliHandle ,UINT4, UINT1 *));

INT4 
VcmUnMapPort PROTO ((tCliHandle ,UINT4, UINT1 *));

INT4
VcmSetTrace PROTO ((tCliHandle , INT4 , UINT4 ));

INT1
VcmGetVcmPrompt PROTO ((INT1 *, INT1 *));

VOID
VcmPrintVcBrief PROTO ((tCliHandle, UINT4));

VOID
VcmPrintVrfBrief PROTO ((tCliHandle, UINT4));

VOID
VcmPrintVcDetail PROTO ((tCliHandle, UINT4));

VOID 
VcmPrintVrfDetail PROTO ((tCliHandle, UINT4));

VOID
VcmPrintVcMapDetail PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex));

VOID 
VcmPrintIfMapEntries PROTO ((tCliHandle ,UINT4));

VOID
VcmPrintVrfIfMapEntries PROTO ((tCliHandle ,UINT4));

VOID
VcmDelIfMapEntry PROTO ((tCliHandle ,UINT4 ));

INT4
VcmPrintPortArray PROTO ((tCliHandle CliHandle, 
                      UINT4 *pu4PortArray, UINT4 u4ArrayLen,
                      UINT1 u1Column, UINT4 *pu4PagingStatus,
                      UINT1 u1MaxPortsPerLine));
        

INT4 VcmShowInterfaceMapping (tCliHandle CliHandle, UINT4 u4IfIndex);

INT4
VcmCreateOwner PROTO ((tCliHandle CliHandle, UINT4 u4VcId, UINT1 *pu1Owner));

INT4
VcmShowOwner PROTO ((tCliHandle CliHandle,UINT4 u4Port));

#ifdef VRF_WANTED
/* CLI prototypes for IP interface mapping */
INT4 VcmCreateVRContext PROTO ((tCliHandle CliHandle, UINT1 *pu1Alias));

INT4 VcmDeleteVRContext PROTO ((tCliHandle CliHandle, UINT1 *pu1Alias));

INT4 VcmAddOrDeleteIpIfMap PROTO ((tCliHandle CliHandle, UINT4 u4Flag,
                                   UINT4 u4IfIndex, UINT1 * pu1L3ContextName));

INT4
VcmVrfShowCommand PROTO ((tCliHandle , UINT1 *, UINT4));

INT4 VcmCreateOrDeleteVrfCounters PROTO ((tCliHandle CliHandle,UINT1 *pu1Alias, UINT4 u4Status));
INT4 VcmVrfShowCounters PROTO ((tCliHandle CliHandle,UINT1 *pu1VrfName));
INT4  VcmClearVrfCounters PROTO ((tCliHandle CliHandle, UINT1 *pu1L3ContextName));
#endif

#endif /* __VCMCLI_H__ */
