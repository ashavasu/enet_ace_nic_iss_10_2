/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fs2544cli.h,v 1.4 2016/02/27 10:06:05 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fs2544ContextId[13];
extern UINT4 Fs2544ContextSystemControl[13];
extern UINT4 Fs2544ContextModuleStatus[13];
extern UINT4 Fs2544ContextTraceOption[13];
extern UINT4 Fs2544ContextTrapStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFs2544ContextSystemControl(u4Fs2544ContextId ,i4SetValFs2544ContextSystemControl) \
 nmhSetCmnWithLock(Fs2544ContextSystemControl, 13, Fs2544ContextSystemControlSet, R2544ApiLock, R2544ApiUnLock, 0, 0, 1, "%u %i", u4Fs2544ContextId ,i4SetValFs2544ContextSystemControl)
#define nmhSetFs2544ContextModuleStatus(u4Fs2544ContextId ,i4SetValFs2544ContextModuleStatus) \
 nmhSetCmnWithLock(Fs2544ContextModuleStatus, 13, Fs2544ContextModuleStatusSet, R2544ApiLock, R2544ApiUnLock, 0, 0, 1, "%u %i", u4Fs2544ContextId ,i4SetValFs2544ContextModuleStatus)
#define nmhSetFs2544ContextTraceOption(u4Fs2544ContextId ,u4SetValFs2544ContextTraceOption) \
 nmhSetCmnWithLock(Fs2544ContextTraceOption, 13, Fs2544ContextTraceOptionSet, R2544ApiLock, R2544ApiUnLock, 0, 0, 1, "%u %u", u4Fs2544ContextId ,u4SetValFs2544ContextTraceOption)
#define nmhSetFs2544ContextTrapStatus(u4Fs2544ContextId ,i4SetValFs2544ContextTrapStatus) \
 nmhSetCmnWithLock(Fs2544ContextTrapStatus, 13, Fs2544ContextTrapStatusSet, R2544ApiLock, R2544ApiUnLock, 0, 0, 1, "%u %i", u4Fs2544ContextId ,i4SetValFs2544ContextTrapStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fs2544SlaId[13];
extern UINT4 Fs2544SlaMEG[13];
extern UINT4 Fs2544SlaME[13];
extern UINT4 Fs2544SlaMEP[13];
extern UINT4 Fs2544SlaMegLevel[13];
extern UINT4 Fs2544SlaTrafficProfileId[13];
extern UINT4 Fs2544SlaSacId[13];
extern UINT4 Fs2544SlaTestStatus[13];
extern UINT4 Fs2544SlaRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFs2544SlaMEG(u4Fs2544ContextId , u4Fs2544SlaId ,u4SetValFs2544SlaMEG) \
 nmhSetCmnWithLock(Fs2544SlaMEG, 13, Fs2544SlaMEGSet, R2544ApiLock, R2544ApiUnLock, 0, 0, 2, "%u %u %u", u4Fs2544ContextId , u4Fs2544SlaId ,u4SetValFs2544SlaMEG)
#define nmhSetFs2544SlaME(u4Fs2544ContextId , u4Fs2544SlaId ,u4SetValFs2544SlaME) \
 nmhSetCmnWithLock(Fs2544SlaME, 13, Fs2544SlaMESet, R2544ApiLock, R2544ApiUnLock, 0, 0, 2, "%u %u %u", u4Fs2544ContextId , u4Fs2544SlaId ,u4SetValFs2544SlaME)
#define nmhSetFs2544SlaMEP(u4Fs2544ContextId , u4Fs2544SlaId ,u4SetValFs2544SlaMEP) \
 nmhSetCmnWithLock(Fs2544SlaMEP, 13, Fs2544SlaMEPSet, R2544ApiLock, R2544ApiUnLock, 0, 0, 2, "%u %u %u", u4Fs2544ContextId , u4Fs2544SlaId ,u4SetValFs2544SlaMEP)
#define nmhSetFs2544SlaMegLevel(u4Fs2544ContextId , u4Fs2544SlaId ,i4SetValFs2544SlaMegLevel) \
 nmhSetCmnWithLock(Fs2544SlaMegLevel, 13, Fs2544SlaMegLevelSet, R2544ApiLock, R2544ApiUnLock, 0, 0, 2, "%u %u %i", u4Fs2544ContextId , u4Fs2544SlaId ,i4SetValFs2544SlaMegLevel)
#define nmhSetFs2544SlaTrafficProfileId(u4Fs2544ContextId , u4Fs2544SlaId ,u4SetValFs2544SlaTrafficProfileId) \
 nmhSetCmnWithLock(Fs2544SlaTrafficProfileId, 13, Fs2544SlaTrafficProfileIdSet, R2544ApiLock, R2544ApiUnLock, 0, 0, 2, "%u %u %u", u4Fs2544ContextId , u4Fs2544SlaId ,u4SetValFs2544SlaTrafficProfileId)
#define nmhSetFs2544SlaSacId(u4Fs2544ContextId , u4Fs2544SlaId ,u4SetValFs2544SlaSacId) \
 nmhSetCmnWithLock(Fs2544SlaSacId, 13, Fs2544SlaSacIdSet, R2544ApiLock, R2544ApiUnLock, 0, 0, 2, "%u %u %u", u4Fs2544ContextId , u4Fs2544SlaId ,u4SetValFs2544SlaSacId)
#define nmhSetFs2544SlaRowStatus(u4Fs2544ContextId , u4Fs2544SlaId ,i4SetValFs2544SlaRowStatus) \
 nmhSetCmnWithLock(Fs2544SlaRowStatus, 13, Fs2544SlaRowStatusSet, R2544ApiLock, R2544ApiUnLock, 0, 1, 2, "%u %u %i", u4Fs2544ContextId , u4Fs2544SlaId ,i4SetValFs2544SlaRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fs2544TrafficProfileId[13];
extern UINT4 Fs2544TrafficProfileName[13];
extern UINT4 Fs2544TrafficProfileSeqNoCheck[13];
extern UINT4 Fs2544TrafficProfileDwellTime[13];
extern UINT4 Fs2544TrafficProfileFrameSize[13];
extern UINT4 Fs2544TrafficProfilePCP[13];
extern UINT4 Fs2544TrafficProfileThTestStatus[13];
extern UINT4 Fs2544TrafficProfileFlTestStatus[13];
extern UINT4 Fs2544TrafficProfileLaTestStatus[13];
extern UINT4 Fs2544TrafficProfileBbTestStatus[13];
extern UINT4 Fs2544TrafficProfileThTrialDuration[13];
extern UINT4 Fs2544TrafficProfileThMaxRate[13];
extern UINT4 Fs2544TrafficProfileThMinRate[13];
extern UINT4 Fs2544TrafficProfileLaTrialDuration[13];
extern UINT4 Fs2544TrafficProfileLaDelayMeasureInterval[13];
extern UINT4 Fs2544TrafficProfileFlTrialDuration[13];
extern UINT4 Fs2544TrafficProfileFlMaxRate[13];
extern UINT4 Fs2544TrafficProfileFlMinRate[13];
extern UINT4 Fs2544TrafficProfileFlRateStep[13];
extern UINT4 Fs2544TrafficProfileBbTrialDuration[13];
extern UINT4 Fs2544TrafficProfileBbTrialCount[13];
extern UINT4 Fs2544TrafficProfileRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFs2544TrafficProfileName(u4Fs2544ContextId , u4Fs2544TrafficProfileId ,pSetValFs2544TrafficProfileName) \
 nmhSetCmnWithLock(Fs2544TrafficProfileName, 13, Fs2544TrafficProfileNameSet, R2544ApiLock, R2544ApiUnLock, 0, 0, 2, "%u %u %s", u4Fs2544ContextId , u4Fs2544TrafficProfileId ,pSetValFs2544TrafficProfileName)
#define nmhSetFs2544TrafficProfileSeqNoCheck(u4Fs2544ContextId , u4Fs2544TrafficProfileId ,i4SetValFs2544TrafficProfileSeqNoCheck) \
 nmhSetCmnWithLock(Fs2544TrafficProfileSeqNoCheck, 13, Fs2544TrafficProfileSeqNoCheckSet, R2544ApiLock, R2544ApiUnLock, 0, 0, 2, "%u %u %i", u4Fs2544ContextId , u4Fs2544TrafficProfileId ,i4SetValFs2544TrafficProfileSeqNoCheck)
#define nmhSetFs2544TrafficProfileDwellTime(u4Fs2544ContextId , u4Fs2544TrafficProfileId ,i4SetValFs2544TrafficProfileDwellTime) \
 nmhSetCmnWithLock(Fs2544TrafficProfileDwellTime, 13, Fs2544TrafficProfileDwellTimeSet, R2544ApiLock, R2544ApiUnLock, 0, 0, 2, "%u %u %i", u4Fs2544ContextId , u4Fs2544TrafficProfileId ,i4SetValFs2544TrafficProfileDwellTime)
#define nmhSetFs2544TrafficProfileFrameSize(u4Fs2544ContextId , u4Fs2544TrafficProfileId ,pSetValFs2544TrafficProfileFrameSize) \
 nmhSetCmnWithLock(Fs2544TrafficProfileFrameSize, 13, Fs2544TrafficProfileFrameSizeSet, R2544ApiLock, R2544ApiUnLock, 0, 0, 2, "%u %u %s", u4Fs2544ContextId , u4Fs2544TrafficProfileId ,pSetValFs2544TrafficProfileFrameSize)
#define nmhSetFs2544TrafficProfilePCP(u4Fs2544ContextId , u4Fs2544TrafficProfileId ,i4SetValFs2544TrafficProfilePCP) \
 nmhSetCmnWithLock(Fs2544TrafficProfilePCP, 13, Fs2544TrafficProfilePCPSet, R2544ApiLock, R2544ApiUnLock, 0, 0, 2, "%u %u %i", u4Fs2544ContextId , u4Fs2544TrafficProfileId ,i4SetValFs2544TrafficProfilePCP)
#define nmhSetFs2544TrafficProfileThTrialDuration(u4Fs2544ContextId , u4Fs2544TrafficProfileId ,u4SetValFs2544TrafficProfileThTrialDuration) \
 nmhSetCmnWithLock(Fs2544TrafficProfileThTrialDuration, 13, Fs2544TrafficProfileThTrialDurationSet, R2544ApiLock, R2544ApiUnLock, 0, 0, 2, "%u %u %u", u4Fs2544ContextId , u4Fs2544TrafficProfileId ,u4SetValFs2544TrafficProfileThTrialDuration)
#define nmhSetFs2544TrafficProfileThMaxRate(u4Fs2544ContextId , u4Fs2544TrafficProfileId ,u4SetValFs2544TrafficProfileThMaxRate) \
 nmhSetCmnWithLock(Fs2544TrafficProfileThMaxRate, 13, Fs2544TrafficProfileThMaxRateSet, R2544ApiLock, R2544ApiUnLock, 0, 0, 2, "%u %u %u", u4Fs2544ContextId , u4Fs2544TrafficProfileId ,u4SetValFs2544TrafficProfileThMaxRate)
#define nmhSetFs2544TrafficProfileThMinRate(u4Fs2544ContextId , u4Fs2544TrafficProfileId ,u4SetValFs2544TrafficProfileThMinRate) \
 nmhSetCmnWithLock(Fs2544TrafficProfileThMinRate, 13, Fs2544TrafficProfileThMinRateSet, R2544ApiLock, R2544ApiUnLock, 0, 0, 2, "%u %u %u", u4Fs2544ContextId , u4Fs2544TrafficProfileId ,u4SetValFs2544TrafficProfileThMinRate)
#define nmhSetFs2544TrafficProfileLaTrialDuration(u4Fs2544ContextId , u4Fs2544TrafficProfileId ,u4SetValFs2544TrafficProfileLaTrialDuration) \
 nmhSetCmnWithLock(Fs2544TrafficProfileLaTrialDuration, 13, Fs2544TrafficProfileLaTrialDurationSet, R2544ApiLock, R2544ApiUnLock, 0, 0, 2, "%u %u %u", u4Fs2544ContextId , u4Fs2544TrafficProfileId ,u4SetValFs2544TrafficProfileLaTrialDuration)
#define nmhSetFs2544TrafficProfileLaDelayMeasureInterval(u4Fs2544ContextId , u4Fs2544TrafficProfileId ,u4SetValFs2544TrafficProfileLaDelayMeasureInterval) \
 nmhSetCmnWithLock(Fs2544TrafficProfileLaDelayMeasureInterval, 13, Fs2544TrafficProfileLaDelayMeasureIntervalSet, R2544ApiLock, R2544ApiUnLock, 0, 0, 2, "%u %u %u", u4Fs2544ContextId , u4Fs2544TrafficProfileId ,u4SetValFs2544TrafficProfileLaDelayMeasureInterval)
#define nmhSetFs2544TrafficProfileFlTrialDuration(u4Fs2544ContextId , u4Fs2544TrafficProfileId ,u4SetValFs2544TrafficProfileFlTrialDuration) \
 nmhSetCmnWithLock(Fs2544TrafficProfileFlTrialDuration, 13, Fs2544TrafficProfileFlTrialDurationSet, R2544ApiLock, R2544ApiUnLock, 0, 0, 2, "%u %u %u", u4Fs2544ContextId , u4Fs2544TrafficProfileId ,u4SetValFs2544TrafficProfileFlTrialDuration)
#define nmhSetFs2544TrafficProfileFlMaxRate(u4Fs2544ContextId , u4Fs2544TrafficProfileId ,u4SetValFs2544TrafficProfileFlMaxRate) \
 nmhSetCmnWithLock(Fs2544TrafficProfileFlMaxRate, 13, Fs2544TrafficProfileFlMaxRateSet, R2544ApiLock, R2544ApiUnLock, 0, 0, 2, "%u %u %u", u4Fs2544ContextId , u4Fs2544TrafficProfileId ,u4SetValFs2544TrafficProfileFlMaxRate)
#define nmhSetFs2544TrafficProfileFlMinRate(u4Fs2544ContextId , u4Fs2544TrafficProfileId ,u4SetValFs2544TrafficProfileFlMinRate) \
 nmhSetCmnWithLock(Fs2544TrafficProfileFlMinRate, 13, Fs2544TrafficProfileFlMinRateSet, R2544ApiLock, R2544ApiUnLock, 0, 0, 2, "%u %u %u", u4Fs2544ContextId , u4Fs2544TrafficProfileId ,u4SetValFs2544TrafficProfileFlMinRate)
#define nmhSetFs2544TrafficProfileFlRateStep(u4Fs2544ContextId , u4Fs2544TrafficProfileId ,u4SetValFs2544TrafficProfileFlRateStep) \
 nmhSetCmnWithLock(Fs2544TrafficProfileFlRateStep, 13, Fs2544TrafficProfileFlRateStepSet, R2544ApiLock, R2544ApiUnLock, 0, 0, 2, "%u %u %u", u4Fs2544ContextId , u4Fs2544TrafficProfileId ,u4SetValFs2544TrafficProfileFlRateStep)
#define nmhSetFs2544TrafficProfileBbTrialDuration(u4Fs2544ContextId , u4Fs2544TrafficProfileId ,u4SetValFs2544TrafficProfileBbTrialDuration) \
 nmhSetCmnWithLock(Fs2544TrafficProfileBbTrialDuration, 13, Fs2544TrafficProfileBbTrialDurationSet, R2544ApiLock, R2544ApiUnLock, 0, 0, 2, "%u %u %u", u4Fs2544ContextId , u4Fs2544TrafficProfileId ,u4SetValFs2544TrafficProfileBbTrialDuration)
#define nmhSetFs2544TrafficProfileBbTrialCount(u4Fs2544ContextId , u4Fs2544TrafficProfileId ,u4SetValFs2544TrafficProfileBbTrialCount) \
 nmhSetCmnWithLock(Fs2544TrafficProfileBbTrialCount, 13, Fs2544TrafficProfileBbTrialCountSet, R2544ApiLock, R2544ApiUnLock, 0, 0, 2, "%u %u %u", u4Fs2544ContextId , u4Fs2544TrafficProfileId ,u4SetValFs2544TrafficProfileBbTrialCount)
#define nmhSetFs2544TrafficProfileRowStatus(u4Fs2544ContextId , u4Fs2544TrafficProfileId ,i4SetValFs2544TrafficProfileRowStatus) \
 nmhSetCmnWithLock(Fs2544TrafficProfileRowStatus, 13, Fs2544TrafficProfileRowStatusSet, R2544ApiLock, R2544ApiUnLock, 0, 1, 2, "%u %u %i", u4Fs2544ContextId , u4Fs2544TrafficProfileId ,i4SetValFs2544TrafficProfileRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fs2544SacId[13];
extern UINT4 Fs2544SacThAllowedFrameLoss[13];
extern UINT4 Fs2544SacLaAllowedFrameLoss[13];
extern UINT4 Fs2544SacFlAllowedFrameLoss[13];
extern UINT4 Fs2544SacRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFs2544SacThAllowedFrameLoss(u4Fs2544ContextId , u4Fs2544SacId ,u4SetValFs2544SacThAllowedFrameLoss) \
 nmhSetCmnWithLock(Fs2544SacThAllowedFrameLoss, 13, Fs2544SacThAllowedFrameLossSet, R2544ApiLock, R2544ApiUnLock, 0, 0, 2, "%u %u %u", u4Fs2544ContextId , u4Fs2544SacId ,u4SetValFs2544SacThAllowedFrameLoss)
#define nmhSetFs2544SacLaAllowedFrameLoss(u4Fs2544ContextId , u4Fs2544SacId ,u4SetValFs2544SacLaAllowedFrameLoss) \
 nmhSetCmnWithLock(Fs2544SacLaAllowedFrameLoss, 13, Fs2544SacLaAllowedFrameLossSet, R2544ApiLock, R2544ApiUnLock, 0, 0, 2, "%u %u %u", u4Fs2544ContextId , u4Fs2544SacId ,u4SetValFs2544SacLaAllowedFrameLoss)
#define nmhSetFs2544SacFlAllowedFrameLoss(u4Fs2544ContextId , u4Fs2544SacId ,u4SetValFs2544SacFlAllowedFrameLoss) \
 nmhSetCmnWithLock(Fs2544SacFlAllowedFrameLoss, 13, Fs2544SacFlAllowedFrameLossSet, R2544ApiLock, R2544ApiUnLock, 0, 0, 2, "%u %u %u", u4Fs2544ContextId , u4Fs2544SacId ,u4SetValFs2544SacFlAllowedFrameLoss)
#define nmhSetFs2544SacRowStatus(u4Fs2544ContextId , u4Fs2544SacId ,i4SetValFs2544SacRowStatus) \
 nmhSetCmnWithLock(Fs2544SacRowStatus, 13, Fs2544SacRowStatusSet, R2544ApiLock, R2544ApiUnLock, 0, 1, 2, "%u %u %i", u4Fs2544ContextId , u4Fs2544SacId ,i4SetValFs2544SacRowStatus)

#endif
