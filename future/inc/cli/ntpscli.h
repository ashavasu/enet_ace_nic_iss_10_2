/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: ntpscli.c, 2016/09/23
 *
 * Description:This file contains the CLI's Macros.
 *
 * *******************************************************************/


#ifndef _NTPS_CLI_H
#define _NTPS_CLI_H

#define       NTP_ENABLE    1
#define       NTP_DISABLE  0
#define       NTP_AUTH_ENABLE          1
#define       NTP_AUTH_DISABLE         0
#define       NTP_AUTH_MD5             1
#define       NTP_AUTH_SHA1            2
#define       NTP_AUTH_RSA_MD2         3
#define       NTP_AUTH_RSA_MD5         4
#define       NTP_AUTH_RSA_SHA         5
#define       NTP_AUTH_RSA_SHA1        6
#define       NTP_AUTH_RSA_MDC2        7
#define       NTP_AUTH_RSA_RIPEMD160   8
#define       NTP_AUTH_DSA_SHA         9
#define       NTP_AUTH_DSA_SHA1        10
#define       MAX_AUTH_TYPES           11

#define       NTP_KEY_MAX_LENGTH       52
#define       NTP_KEY_TRUSTED          1
#define       NTP_NO_KEY_TRUSTED       0
#define       NTP_KEY_STR_LENGTH      16
#define       NTP_RESTRICT_LEN    256

#define   NTP_TRACE_ENABLE    0xffffffff
#define   NTP_TRACE_DISABLE   0x0

#define   NTP_MGMT_TRC            MGMT_TRC
#define   NTP_CONTROL_PATH_TRC    CONTROL_PLANE_TRC
#define   NTP_CONF_FILE_TRC    	  DATA_PATH_TRC 
#define   NTP_OS_RESOURCE_TRC     OS_RESOURCE_TRC
#define   NTP_ALL_FAILURE_TRC     0x0000FFFF 
#define   NTP_DISABLE_TRC	      0x80000000

#define  NTP_DST_TIME_LEN           20
#define  NTP_BROADCAST_CLIENT_LEN    20
#define  NTP_BROADCAST_CLIENT "broadcastclient"
#define  NTP_TRUSTED_KEY "trustedkey "
#define  NTP_NO_TRUSTED_KEY "#trustedkey "
#define  NTP_TRUSTED_KEY_LEN 30
#define  NTP_SERVER_START   1
#define  NTP_SERVER_STOP    0
#define  NTP_SERVER_RESTART 2
#define  NTPS_MAX_KEYS_LIMIT 5

#define      NTP_RESTRICT_IGNORE 	1
#define      NTP_RESTRICT_KOD 		2
#define      NTP_RESTRICT_LIMITED 	3
#define      NTP_RESTRICT_LOWPRIOTRAP	4
#define      NTP_RESTRICT_NOMODIFY 	5
#define      NTP_RESTRICT_NOQUERY	6
#define      NTP_RESTRICT_NOPEER	7
#define      NTP_RESTRICT_NOSERVE	8
#define      NTP_RESTRICT_NOTRAP	9
#define      NTP_RESTRICT_NOTRUST	10
#define      NTP_RESTRICT_NTPPORT	11
#define      NTP_RESTRICT_VERSION	12
#define      NTP_RESTRICT_MAX		13

#define      NTP_RES_IGNORE 		1
#define      NTP_RES_KOD 			2
#define      NTP_RES_LIMITED 		4
#define      NTP_RES_LOWPRIOTRAP	8
#define      NTP_RES_NOMODIFY 		16
#define      NTP_RES_NOQUERY		32
#define      NTP_RES_NOPEER			64	
#define      NTP_RES_NOSERVE		128
#define      NTP_RES_NOTRAP			256
#define      NTP_RES_NOTRUST		512
#define      NTP_RES_NTPPORT		1024
#define      NTP_RES_VERSION		2048
#define      NTP_RES_MAX			4096


#define    NTP_FLAG_IGNORE "ignore"
#define    NTP_FLAG_KOD "kod"
#define    NTP_FLAG_LIMITED "limited"
#define    NTP_FLAG_LOWPRIOTRAP "lowpriotrap"
#define    NTP_FLAG_NOMODIFY "nomodify"
#define    NTP_FLAG_NOQUERY "noquery"
#define    NTP_FLAG_NOPEER "nopeer"
#define    NTP_FLAG_NOSERVE "noserve"
#define    NTP_FLAG_NOTRAP "notrap"
#define    NTP_FLAG_NOTRUST "notrust"
#define    NTP_FLAG_NTPPORT "ntpport"
#define    NTP_FLAG_VERSION "version"

enum {
    CLI_NTPS_SET_ADMIN_STATUS ,
    CLI_NTPS_ADD_BCAST_SERVER ,
    CLI_NTPS_ADD_MCAST_SERVER ,
    CLI_NTPS_DEL_UCAST_SERVER  ,
    CLI_NTPS_DEL_BCAST_SERVER ,
    CLI_NTPS_DEL_MCAST_SERVER ,
    CLI_NTPS_ADD_UCAST_SERVER  ,
    CLI_NTPS_ADD_SERVER_VERSION,
    CLI_NTPS_ADD_SERVER_BURST,
    CLI_NTPS_ADD_SERVER_KEY,
    CLI_NTPS_ADD_SERVER_MAXPOLL,
    CLI_NTPS_ADD_SERVER_MINPOLL,
    CLI_NTPS_ADD_PEER,
    CLI_NTPS_DELETE_PEER,
    CLI_NTPS_ADD_BROADCAST_CLIENT,
    CLI_NTPS_DELETE_BROADCAST_CLIENT,
    CLI_NTPS_ADD_TRUSTED_KEYID,
    CLI_NTPS_DEL_TRUSTED_KEYID,
    CLI_NTPS_SERVER_EXECUTE,
    CLI_NTPS_SERVER_CREATE_SOURCE,
    CLI_NTPS_SERVER_DELETE_SOURCE,
    NTPS_CLI_SET_AUTH,
    NTPS_CLI_SET_NO_AUTH,
    NTPS_CLI_SET_AUTH_KEY,
    NTPS_CLI_SET_NO_AUTH_KEY,
    NTPS_CLI_TRUSTED_KEY,
    NTPS_CLI_NO_TRUSTED_KEY,
    SHOW_NTPS_SERVER_STATUS,
    SHOW_NTPS_SERVER_DETAIL,
    SHOW_NTPS_SYNCHRONIZATION,
    SHOW_NTPS_SERVER_ASSOCIATIONS,
    SHOW_NTPS_SERVER_STATISTICS,
    CLI_NTPS_SET_RESTRICT,
    CLI_NTPS_NO_SET_RESTRICT,
    CLI_NTPS_DEBUG,
    SHOW_NTPS_CLIENTS,
    CLI_NTPS_SET_INT_STATUS

} ; 

enum {
    CLI_NTP_INVALID_STATUS_SERVER =1,
    CLI_NTP_ACCESS_NOT_CONFIGURED ,
    CLI_NTP_MAX_ACCESS_CONFIGURED ,
    CLI_NTP_SERVER_CONFIGURED ,
    CLI_NTP_SERVER_NOT_CONFIGURED ,
    CLI_NTP_ERR_KEY_USED,
    CLI_NTP_SERVER_MAX_ERR,
    CLI_NTP_KEY_SHOULD_TRUST,
    CLI_NTP_KEY_NOT_CONFIGURED,
    CLI_NTP_AUTH_DISABLE,
    CLI_NTP_AUTH_NO_DISABLE,
    CLI_NTP_NO_KEY_ERR,
    CLI_NTP_KEY_USED_SERVER,
    CLI_NTP_KEY_CREAT_ERR,
    CLI_NTP_TRUSTED_KEY_USED_SERVER,
    CLI_NTP_AUTH_MAX_KEY,
    CLI_NTP_AUTH_KEY_CONFIGURED,
    CLI_NTP_MAX_INTERFACE,
    CLI_NTP_NO_INTERFACE,
    CLI_NTP_STOP_FAIL,
    CLI_NTP_START_FAIL,
    CLI_NTP_START_CONF_FAIL,
    CLI_IPV6_INVALID_ADDRESS,
    CLI_NTP_ERR_INVALID_IP_ADDR,
    CLI_NTP_ERR_INVALID_IP_MASK,
    CLI_NTP_ERR_INVALID_NTP_VERSION,
    CLI_NTP_ERR_INVALID_BURST_VALUE,
    CLI_NTP_ERR_INVALID_NTP_AUTH_KEYID,
    CLI_NTP_ERR_INVALID_NTP_SERVER_MINPOLL,
    CLI_NTP_ERR_INVALID_NTP_SERVER_MAXPOLL,
    CLI_NTP_ERR_INVALID_NTP_SOURCE_INTERFACE,
    CLI_NTP_ERR_INVALID_NTP_ROWSTATUS,
    CLI_NTP_ERR_MAX_CLI_CMD
};

#ifdef _NTPS_CLI_C

CONST CHR1 *gau1NtpCliErrString [] = {
    NULL,
    "% Invalid Server Status",
    "% NTP restrict entry not configured\r\n",
    "% Maximum restrict entry are already configured\r\n",
    "% NTP server already exists\r\n",
    "% NTP server not configured \r\n",
    "% NTP Key already used by another server \r\n",
    "% Maximum allowed servers are already configured \r\n",
    "% Key should be trusted\r\n",
    "% Key not configured\r\n",
    "% Authentication is disabled\r\n",
    "% Authentication Keys exists, delete the keys and then disable authentication\r\n",
    "% Key not created\r\n",
    "% Delete the server first using the key\r\n",
    "% Key should be created first \r\n",
    "% Delete the server first using the trusted key\r\n",
    "% Maximum allowed keys are already configured \r\n",
    "% NTP Key already exists\r\n",
    "% Maximum NTP interfaces configured \r\n",
    "% NTP not enabled on this interface \r\n",
    "% NTP Operation - Stop Failed, verify NTP installation \r\n",
    "% NTP Operation - Start Failed, verify NTP installation \r\n",
    "% NTP Operation - Start Failed, error in updating ntp.conf \r\n",
    "% Invalid IPv6 address\r\n",
    "% Invalid IP address\r\n",
    "% Invalid IP Mask\r\n",
    "% Invalid server version\r\n",
    "% Invalid burst value\r\n",
    "% Invalid authentication key\r\n",
    "% Invalid min poll for server\r\n",
    "% Invalid max poll for server\r\n",
    "% Invalid NTP source interface \r\n",
    "% Invalid NTP command\r\n",
    "\r\n"
};
#endif
#define NTP_IS_VALID_IP(u4Addr) ((((u4Addr & 0xff000000) == 0)\
                              ||((u4Addr & 0x000000ff) == 0)\
                              ||((u4Addr & 0x000000ff) == 0x000000ff) \
                              ||((u4Addr & 0x7f000000) == 0x7f000000))? \
                              NTP_ZERO : NTP_ONE)

INT4
IssNtpsShowRunningConfig( tCliHandle );

INT4 
cli_process_ntps_cmd( tCliHandle CliHandle, UINT4 u4Command, ... );

extern UINT4 gu4CliSysLogLevel;
VOID
IssNtpsShowDebugging (tCliHandle CliHandle);
#endif
