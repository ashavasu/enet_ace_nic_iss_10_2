/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdisicli.h,v 1.2 2009/06/03 06:38:59 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IsisSysInstance[11];
extern UINT4 IsisSysType[11];
extern UINT4 IsisSysID[11];
extern UINT4 IsisSysMaxPathSplits[11];
extern UINT4 IsisSysMaxLSPGenInt[11];
extern UINT4 IsisSysOrigL1LSPBuffSize[11];
extern UINT4 IsisSysMaxAreaAddresses[11];
extern UINT4 IsisSysMinL1LSPGenInt[11];
extern UINT4 IsisSysMinL2LSPGenInt[11];
extern UINT4 IsisSysPollESHelloRate[11];
extern UINT4 IsisSysWaitTime[11];
extern UINT4 IsisSysAdminState[11];
extern UINT4 IsisSysOrigL2LSPBuffSize[11];
extern UINT4 IsisSysLogAdjacencyChanges[11];
extern UINT4 IsisSysMaxAreaCheck[11];
extern UINT4 IsisSysExistState[11];
extern UINT4 IsisSysL2toL1Leaking[11];
extern UINT4 IsisSysSetOverload[11];
extern UINT4 IsisSysL1MetricStyle[11];
extern UINT4 IsisSysL1SPFConsiders[11];
extern UINT4 IsisSysL2MetricStyle[11];
extern UINT4 IsisSysL2SPFConsiders[11];
extern UINT4 IsisSysTEEnabled[11];
extern UINT4 IsisSysMaxAge[11];
extern UINT4 IsisSysReceiveLSPBufferSize[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIsisSysType(i4IsisSysInstance ,i4SetValIsisSysType)	\
	nmhSetCmn(IsisSysType, 11, IsisSysTypeSet, NULL, NULL, 0, 0, 1, "%i %i", i4IsisSysInstance ,i4SetValIsisSysType)
#define nmhSetIsisSysID(i4IsisSysInstance ,pSetValIsisSysID)	\
	nmhSetCmn(IsisSysID, 11, IsisSysIDSet, NULL, NULL, 0, 0, 1, "%i %s", i4IsisSysInstance ,pSetValIsisSysID)
#define nmhSetIsisSysMaxPathSplits(i4IsisSysInstance ,i4SetValIsisSysMaxPathSplits)	\
	nmhSetCmn(IsisSysMaxPathSplits, 11, IsisSysMaxPathSplitsSet, NULL, NULL, 0, 0, 1, "%i %i", i4IsisSysInstance ,i4SetValIsisSysMaxPathSplits)
#define nmhSetIsisSysMaxLSPGenInt(i4IsisSysInstance ,i4SetValIsisSysMaxLSPGenInt)	\
	nmhSetCmn(IsisSysMaxLSPGenInt, 11, IsisSysMaxLSPGenIntSet, NULL, NULL, 0, 0, 1, "%i %i", i4IsisSysInstance ,i4SetValIsisSysMaxLSPGenInt)
#define nmhSetIsisSysOrigL1LSPBuffSize(i4IsisSysInstance ,i4SetValIsisSysOrigL1LSPBuffSize)	\
	nmhSetCmn(IsisSysOrigL1LSPBuffSize, 11, IsisSysOrigL1LSPBuffSizeSet, NULL, NULL, 0, 0, 1, "%i %i", i4IsisSysInstance ,i4SetValIsisSysOrigL1LSPBuffSize)
#define nmhSetIsisSysMaxAreaAddresses(i4IsisSysInstance ,i4SetValIsisSysMaxAreaAddresses)	\
	nmhSetCmn(IsisSysMaxAreaAddresses, 11, IsisSysMaxAreaAddressesSet, NULL, NULL, 0, 0, 1, "%i %i", i4IsisSysInstance ,i4SetValIsisSysMaxAreaAddresses)
#define nmhSetIsisSysMinL1LSPGenInt(i4IsisSysInstance ,i4SetValIsisSysMinL1LSPGenInt)	\
	nmhSetCmn(IsisSysMinL1LSPGenInt, 11, IsisSysMinL1LSPGenIntSet, NULL, NULL, 0, 0, 1, "%i %i", i4IsisSysInstance ,i4SetValIsisSysMinL1LSPGenInt)
#define nmhSetIsisSysMinL2LSPGenInt(i4IsisSysInstance ,i4SetValIsisSysMinL2LSPGenInt)	\
	nmhSetCmn(IsisSysMinL2LSPGenInt, 11, IsisSysMinL2LSPGenIntSet, NULL, NULL, 0, 0, 1, "%i %i", i4IsisSysInstance ,i4SetValIsisSysMinL2LSPGenInt)
#define nmhSetIsisSysPollESHelloRate(i4IsisSysInstance ,i4SetValIsisSysPollESHelloRate)	\
	nmhSetCmn(IsisSysPollESHelloRate, 11, IsisSysPollESHelloRateSet, NULL, NULL, 0, 0, 1, "%i %i", i4IsisSysInstance ,i4SetValIsisSysPollESHelloRate)
#define nmhSetIsisSysWaitTime(i4IsisSysInstance ,i4SetValIsisSysWaitTime)	\
	nmhSetCmn(IsisSysWaitTime, 11, IsisSysWaitTimeSet, NULL, NULL, 0, 0, 1, "%i %i", i4IsisSysInstance ,i4SetValIsisSysWaitTime)
#define nmhSetIsisSysAdminState(i4IsisSysInstance ,i4SetValIsisSysAdminState)	\
	nmhSetCmn(IsisSysAdminState, 11, IsisSysAdminStateSet, NULL, NULL, 0, 0, 1, "%i %i", i4IsisSysInstance ,i4SetValIsisSysAdminState)
#define nmhSetIsisSysOrigL2LSPBuffSize(i4IsisSysInstance ,i4SetValIsisSysOrigL2LSPBuffSize)	\
	nmhSetCmn(IsisSysOrigL2LSPBuffSize, 11, IsisSysOrigL2LSPBuffSizeSet, NULL, NULL, 0, 0, 1, "%i %i", i4IsisSysInstance ,i4SetValIsisSysOrigL2LSPBuffSize)
#define nmhSetIsisSysLogAdjacencyChanges(i4IsisSysInstance ,i4SetValIsisSysLogAdjacencyChanges)	\
	nmhSetCmn(IsisSysLogAdjacencyChanges, 11, IsisSysLogAdjacencyChangesSet, NULL, NULL, 0, 0, 1, "%i %i", i4IsisSysInstance ,i4SetValIsisSysLogAdjacencyChanges)
#define nmhSetIsisSysMaxAreaCheck(i4IsisSysInstance ,i4SetValIsisSysMaxAreaCheck)	\
	nmhSetCmn(IsisSysMaxAreaCheck, 11, IsisSysMaxAreaCheckSet, NULL, NULL, 0, 0, 1, "%i %i", i4IsisSysInstance ,i4SetValIsisSysMaxAreaCheck)
#define nmhSetIsisSysExistState(i4IsisSysInstance ,i4SetValIsisSysExistState)	\
	nmhSetCmn(IsisSysExistState, 11, IsisSysExistStateSet, NULL, NULL, 0, 1, 1, "%i %i", i4IsisSysInstance ,i4SetValIsisSysExistState)
#define nmhSetIsisSysL2toL1Leaking(i4IsisSysInstance ,i4SetValIsisSysL2toL1Leaking)	\
	nmhSetCmn(IsisSysL2toL1Leaking, 11, IsisSysL2toL1LeakingSet, NULL, NULL, 0, 0, 1, "%i %i", i4IsisSysInstance ,i4SetValIsisSysL2toL1Leaking)
#define nmhSetIsisSysSetOverload(i4IsisSysInstance ,i4SetValIsisSysSetOverload)	\
	nmhSetCmn(IsisSysSetOverload, 11, IsisSysSetOverloadSet, NULL, NULL, 0, 0, 1, "%i %i", i4IsisSysInstance ,i4SetValIsisSysSetOverload)
#define nmhSetIsisSysL1MetricStyle(i4IsisSysInstance ,i4SetValIsisSysL1MetricStyle)	\
	nmhSetCmn(IsisSysL1MetricStyle, 11, IsisSysL1MetricStyleSet, NULL, NULL, 0, 0, 1, "%i %i", i4IsisSysInstance ,i4SetValIsisSysL1MetricStyle)
#define nmhSetIsisSysL1SPFConsiders(i4IsisSysInstance ,i4SetValIsisSysL1SPFConsiders)	\
	nmhSetCmn(IsisSysL1SPFConsiders, 11, IsisSysL1SPFConsidersSet, NULL, NULL, 0, 0, 1, "%i %i", i4IsisSysInstance ,i4SetValIsisSysL1SPFConsiders)
#define nmhSetIsisSysL2MetricStyle(i4IsisSysInstance ,i4SetValIsisSysL2MetricStyle)	\
	nmhSetCmn(IsisSysL2MetricStyle, 11, IsisSysL2MetricStyleSet, NULL, NULL, 0, 0, 1, "%i %i", i4IsisSysInstance ,i4SetValIsisSysL2MetricStyle)
#define nmhSetIsisSysL2SPFConsiders(i4IsisSysInstance ,i4SetValIsisSysL2SPFConsiders)	\
	nmhSetCmn(IsisSysL2SPFConsiders, 11, IsisSysL2SPFConsidersSet, NULL, NULL, 0, 0, 1, "%i %i", i4IsisSysInstance ,i4SetValIsisSysL2SPFConsiders)
#define nmhSetIsisSysTEEnabled(i4IsisSysInstance ,i4SetValIsisSysTEEnabled)	\
	nmhSetCmn(IsisSysTEEnabled, 11, IsisSysTEEnabledSet, NULL, NULL, 0, 0, 1, "%i %i", i4IsisSysInstance ,i4SetValIsisSysTEEnabled)
#define nmhSetIsisSysMaxAge(i4IsisSysInstance ,i4SetValIsisSysMaxAge)	\
	nmhSetCmn(IsisSysMaxAge, 11, IsisSysMaxAgeSet, NULL, NULL, 0, 0, 1, "%i %i", i4IsisSysInstance ,i4SetValIsisSysMaxAge)
#define nmhSetIsisSysReceiveLSPBufferSize(i4IsisSysInstance ,i4SetValIsisSysReceiveLSPBufferSize)	\
	nmhSetCmn(IsisSysReceiveLSPBufferSize, 11, IsisSysReceiveLSPBufferSizeSet, NULL, NULL, 0, 0, 1, "%i %i", i4IsisSysInstance ,i4SetValIsisSysReceiveLSPBufferSize)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IsisManAreaAddr[11];
extern UINT4 IsisManAreaAddrExistState[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIsisManAreaAddrExistState(i4IsisSysInstance , pIsisManAreaAddr ,i4SetValIsisManAreaAddrExistState)	\
	nmhSetCmn(IsisManAreaAddrExistState, 11, IsisManAreaAddrExistStateSet, NULL, NULL, 0, 1, 2, "%i %s %i", i4IsisSysInstance , pIsisManAreaAddr ,i4SetValIsisManAreaAddrExistState)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IsisSysProtSuppProtocol[11];
extern UINT4 IsisSysProtSuppExistState[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIsisSysProtSuppExistState(i4IsisSysInstance , i4IsisSysProtSuppProtocol ,i4SetValIsisSysProtSuppExistState)	\
	nmhSetCmn(IsisSysProtSuppExistState, 11, IsisSysProtSuppExistStateSet, NULL, NULL, 0, 1, 2, "%i %i %i", i4IsisSysInstance , i4IsisSysProtSuppProtocol ,i4SetValIsisSysProtSuppExistState)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IsisSummAddressType[11];
extern UINT4 IsisSummAddress[11];
extern UINT4 IsisSummAddrPrefixLen[11];
extern UINT4 IsisSummAddrExistState[11];
extern UINT4 IsisSummAddrAdminState[11];
extern UINT4 IsisSummAddrMetric[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIsisSummAddrExistState(i4IsisSysInstance , i4IsisSummAddressType , pIsisSummAddress , u4IsisSummAddrPrefixLen ,i4SetValIsisSummAddrExistState)	\
	nmhSetCmn(IsisSummAddrExistState, 11, IsisSummAddrExistStateSet, NULL, NULL, 0, 1, 4, "%i %i %s %u %i", i4IsisSysInstance , i4IsisSummAddressType , pIsisSummAddress , u4IsisSummAddrPrefixLen ,i4SetValIsisSummAddrExistState)
#define nmhSetIsisSummAddrAdminState(i4IsisSysInstance , i4IsisSummAddressType , pIsisSummAddress , u4IsisSummAddrPrefixLen ,i4SetValIsisSummAddrAdminState)	\
	nmhSetCmn(IsisSummAddrAdminState, 11, IsisSummAddrAdminStateSet, NULL, NULL, 0, 0, 4, "%i %i %s %u %i", i4IsisSysInstance , i4IsisSummAddressType , pIsisSummAddress , u4IsisSummAddrPrefixLen ,i4SetValIsisSummAddrAdminState)
#define nmhSetIsisSummAddrMetric(i4IsisSysInstance , i4IsisSummAddressType , pIsisSummAddress , u4IsisSummAddrPrefixLen ,i4SetValIsisSummAddrMetric)	\
	nmhSetCmn(IsisSummAddrMetric, 11, IsisSummAddrMetricSet, NULL, NULL, 0, 0, 4, "%i %i %s %u %i", i4IsisSysInstance , i4IsisSummAddressType , pIsisSummAddress , u4IsisSummAddrPrefixLen ,i4SetValIsisSummAddrMetric)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IsisCircIndex[11];
extern UINT4 IsisCircIfIndex[11];
extern UINT4 IsisCircIfSubIndex[11];
extern UINT4 IsisCircLocalID[11];
extern UINT4 IsisCircAdminState[11];
extern UINT4 IsisCircExistState[11];
extern UINT4 IsisCircType[11];
extern UINT4 IsisCircExtDomain[11];
extern UINT4 IsisCircLevel[11];
extern UINT4 IsisCircMCAddr[11];
extern UINT4 IsisCircPassiveCircuit[11];
extern UINT4 IsisCircMeshGroupEnabled[11];
extern UINT4 IsisCircMeshGroup[11];
extern UINT4 IsisCircSmallHellos[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIsisCircIfIndex(i4IsisSysInstance , i4IsisCircIndex ,i4SetValIsisCircIfIndex)	\
	nmhSetCmn(IsisCircIfIndex, 11, IsisCircIfIndexSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4IsisSysInstance , i4IsisCircIndex ,i4SetValIsisCircIfIndex)
#define nmhSetIsisCircIfSubIndex(i4IsisSysInstance , i4IsisCircIndex ,i4SetValIsisCircIfSubIndex)	\
	nmhSetCmn(IsisCircIfSubIndex, 11, IsisCircIfSubIndexSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4IsisSysInstance , i4IsisCircIndex ,i4SetValIsisCircIfSubIndex)
#define nmhSetIsisCircLocalID(i4IsisSysInstance , i4IsisCircIndex ,i4SetValIsisCircLocalID)	\
	nmhSetCmn(IsisCircLocalID, 11, IsisCircLocalIDSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4IsisSysInstance , i4IsisCircIndex ,i4SetValIsisCircLocalID)
#define nmhSetIsisCircAdminState(i4IsisSysInstance , i4IsisCircIndex ,i4SetValIsisCircAdminState)	\
	nmhSetCmn(IsisCircAdminState, 11, IsisCircAdminStateSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4IsisSysInstance , i4IsisCircIndex ,i4SetValIsisCircAdminState)
#define nmhSetIsisCircExistState(i4IsisSysInstance , i4IsisCircIndex ,i4SetValIsisCircExistState)	\
	nmhSetCmn(IsisCircExistState, 11, IsisCircExistStateSet, NULL, NULL, 0, 1, 2, "%i %i %i", i4IsisSysInstance , i4IsisCircIndex ,i4SetValIsisCircExistState)
#define nmhSetIsisCircType(i4IsisSysInstance , i4IsisCircIndex ,i4SetValIsisCircType)	\
	nmhSetCmn(IsisCircType, 11, IsisCircTypeSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4IsisSysInstance , i4IsisCircIndex ,i4SetValIsisCircType)
#define nmhSetIsisCircExtDomain(i4IsisSysInstance , i4IsisCircIndex ,i4SetValIsisCircExtDomain)	\
	nmhSetCmn(IsisCircExtDomain, 11, IsisCircExtDomainSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4IsisSysInstance , i4IsisCircIndex ,i4SetValIsisCircExtDomain)
#define nmhSetIsisCircLevel(i4IsisSysInstance , i4IsisCircIndex ,i4SetValIsisCircLevel)	\
	nmhSetCmn(IsisCircLevel, 11, IsisCircLevelSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4IsisSysInstance , i4IsisCircIndex ,i4SetValIsisCircLevel)
#define nmhSetIsisCircMCAddr(i4IsisSysInstance , i4IsisCircIndex ,i4SetValIsisCircMCAddr)	\
	nmhSetCmn(IsisCircMCAddr, 11, IsisCircMCAddrSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4IsisSysInstance , i4IsisCircIndex ,i4SetValIsisCircMCAddr)
#define nmhSetIsisCircPassiveCircuit(i4IsisSysInstance , i4IsisCircIndex ,i4SetValIsisCircPassiveCircuit)	\
	nmhSetCmn(IsisCircPassiveCircuit, 11, IsisCircPassiveCircuitSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4IsisSysInstance , i4IsisCircIndex ,i4SetValIsisCircPassiveCircuit)
#define nmhSetIsisCircMeshGroupEnabled(i4IsisSysInstance , i4IsisCircIndex ,i4SetValIsisCircMeshGroupEnabled)	\
	nmhSetCmn(IsisCircMeshGroupEnabled, 11, IsisCircMeshGroupEnabledSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4IsisSysInstance , i4IsisCircIndex ,i4SetValIsisCircMeshGroupEnabled)
#define nmhSetIsisCircMeshGroup(i4IsisSysInstance , i4IsisCircIndex ,i4SetValIsisCircMeshGroup)	\
	nmhSetCmn(IsisCircMeshGroup, 11, IsisCircMeshGroupSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4IsisSysInstance , i4IsisCircIndex ,i4SetValIsisCircMeshGroup)
#define nmhSetIsisCircSmallHellos(i4IsisSysInstance , i4IsisCircIndex ,i4SetValIsisCircSmallHellos)	\
	nmhSetCmn(IsisCircSmallHellos, 11, IsisCircSmallHellosSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4IsisSysInstance , i4IsisCircIndex ,i4SetValIsisCircSmallHellos)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IsisCircLevelIndex[11];
extern UINT4 IsisCircLevelMetric[11];
extern UINT4 IsisCircLevelISPriority[11];
extern UINT4 IsisCircLevelHelloMultiplier[11];
extern UINT4 IsisCircLevelHelloTimer[11];
extern UINT4 IsisCircLevelDRHelloTimer[11];
extern UINT4 IsisCircLevelLSPThrottle[11];
extern UINT4 IsisCircLevelMinLSPRetransInt[11];
extern UINT4 IsisCircLevelCSNPInterval[11];
extern UINT4 IsisCircLevelPartSNPInterval[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIsisCircLevelMetric(i4IsisSysInstance , i4IsisCircIndex , i4IsisCircLevelIndex ,i4SetValIsisCircLevelMetric)	\
	nmhSetCmn(IsisCircLevelMetric, 11, IsisCircLevelMetricSet, NULL, NULL, 0, 0, 3, "%i %i %i %i", i4IsisSysInstance , i4IsisCircIndex , i4IsisCircLevelIndex ,i4SetValIsisCircLevelMetric)
#define nmhSetIsisCircLevelISPriority(i4IsisSysInstance , i4IsisCircIndex , i4IsisCircLevelIndex ,i4SetValIsisCircLevelISPriority)	\
	nmhSetCmn(IsisCircLevelISPriority, 11, IsisCircLevelISPrioritySet, NULL, NULL, 0, 0, 3, "%i %i %i %i", i4IsisSysInstance , i4IsisCircIndex , i4IsisCircLevelIndex ,i4SetValIsisCircLevelISPriority)
#define nmhSetIsisCircLevelHelloMultiplier(i4IsisSysInstance , i4IsisCircIndex , i4IsisCircLevelIndex ,i4SetValIsisCircLevelHelloMultiplier)	\
	nmhSetCmn(IsisCircLevelHelloMultiplier, 11, IsisCircLevelHelloMultiplierSet, NULL, NULL, 0, 0, 3, "%i %i %i %i", i4IsisSysInstance , i4IsisCircIndex , i4IsisCircLevelIndex ,i4SetValIsisCircLevelHelloMultiplier)
#define nmhSetIsisCircLevelHelloTimer(i4IsisSysInstance , i4IsisCircIndex , i4IsisCircLevelIndex ,i4SetValIsisCircLevelHelloTimer)	\
	nmhSetCmn(IsisCircLevelHelloTimer, 11, IsisCircLevelHelloTimerSet, NULL, NULL, 0, 0, 3, "%i %i %i %i", i4IsisSysInstance , i4IsisCircIndex , i4IsisCircLevelIndex ,i4SetValIsisCircLevelHelloTimer)
#define nmhSetIsisCircLevelDRHelloTimer(i4IsisSysInstance , i4IsisCircIndex , i4IsisCircLevelIndex ,i4SetValIsisCircLevelDRHelloTimer)	\
	nmhSetCmn(IsisCircLevelDRHelloTimer, 11, IsisCircLevelDRHelloTimerSet, NULL, NULL, 0, 0, 3, "%i %i %i %i", i4IsisSysInstance , i4IsisCircIndex , i4IsisCircLevelIndex ,i4SetValIsisCircLevelDRHelloTimer)
#define nmhSetIsisCircLevelLSPThrottle(i4IsisSysInstance , i4IsisCircIndex , i4IsisCircLevelIndex ,i4SetValIsisCircLevelLSPThrottle)	\
	nmhSetCmn(IsisCircLevelLSPThrottle, 11, IsisCircLevelLSPThrottleSet, NULL, NULL, 0, 0, 3, "%i %i %i %i", i4IsisSysInstance , i4IsisCircIndex , i4IsisCircLevelIndex ,i4SetValIsisCircLevelLSPThrottle)
#define nmhSetIsisCircLevelMinLSPRetransInt(i4IsisSysInstance , i4IsisCircIndex , i4IsisCircLevelIndex ,i4SetValIsisCircLevelMinLSPRetransInt)	\
	nmhSetCmn(IsisCircLevelMinLSPRetransInt, 11, IsisCircLevelMinLSPRetransIntSet, NULL, NULL, 0, 0, 3, "%i %i %i %i", i4IsisSysInstance , i4IsisCircIndex , i4IsisCircLevelIndex ,i4SetValIsisCircLevelMinLSPRetransInt)
#define nmhSetIsisCircLevelCSNPInterval(i4IsisSysInstance , i4IsisCircIndex , i4IsisCircLevelIndex ,i4SetValIsisCircLevelCSNPInterval)	\
	nmhSetCmn(IsisCircLevelCSNPInterval, 11, IsisCircLevelCSNPIntervalSet, NULL, NULL, 0, 0, 3, "%i %i %i %i", i4IsisSysInstance , i4IsisCircIndex , i4IsisCircLevelIndex ,i4SetValIsisCircLevelCSNPInterval)
#define nmhSetIsisCircLevelPartSNPInterval(i4IsisSysInstance , i4IsisCircIndex , i4IsisCircLevelIndex ,i4SetValIsisCircLevelPartSNPInterval)	\
	nmhSetCmn(IsisCircLevelPartSNPInterval, 11, IsisCircLevelPartSNPIntervalSet, NULL, NULL, 0, 0, 3, "%i %i %i %i", i4IsisSysInstance , i4IsisCircIndex , i4IsisCircLevelIndex ,i4SetValIsisCircLevelPartSNPInterval)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IsisIPRAIndex[11];
extern UINT4 IsisIPRAType[11];
extern UINT4 IsisIPRADestType[11];
extern UINT4 IsisIPRADest[11];
extern UINT4 IsisIPRADestPrefixLen[11];
extern UINT4 IsisIPRAExistState[11];
extern UINT4 IsisIPRAAdminState[11];
extern UINT4 IsisIPRAMetric[11];
extern UINT4 IsisIPRAMetricType[11];
extern UINT4 IsisIPRASNPAAddress[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIsisIPRADestType(i4IsisSysInstance , i4IsisIPRAType , i4IsisIPRAIndex ,i4SetValIsisIPRADestType)	\
	nmhSetCmn(IsisIPRADestType, 11, IsisIPRADestTypeSet, NULL, NULL, 0, 0, 3, "%i %i %i %i", i4IsisSysInstance , i4IsisIPRAType , i4IsisIPRAIndex ,i4SetValIsisIPRADestType)
#define nmhSetIsisIPRADest(i4IsisSysInstance , i4IsisIPRAType , i4IsisIPRAIndex ,pSetValIsisIPRADest)	\
	nmhSetCmn(IsisIPRADest, 11, IsisIPRADestSet, NULL, NULL, 0, 0, 3, "%i %i %i %s", i4IsisSysInstance , i4IsisIPRAType , i4IsisIPRAIndex ,pSetValIsisIPRADest)
#define nmhSetIsisIPRADestPrefixLen(i4IsisSysInstance , i4IsisIPRAType , i4IsisIPRAIndex ,u4SetValIsisIPRADestPrefixLen)	\
	nmhSetCmn(IsisIPRADestPrefixLen, 11, IsisIPRADestPrefixLenSet, NULL, NULL, 0, 0, 3, "%i %i %i %u", i4IsisSysInstance , i4IsisIPRAType , i4IsisIPRAIndex ,u4SetValIsisIPRADestPrefixLen)
#define nmhSetIsisIPRAExistState(i4IsisSysInstance , i4IsisIPRAType , i4IsisIPRAIndex ,i4SetValIsisIPRAExistState)	\
	nmhSetCmn(IsisIPRAExistState, 11, IsisIPRAExistStateSet, NULL, NULL, 0, 1, 3, "%i %i %i %i", i4IsisSysInstance , i4IsisIPRAType , i4IsisIPRAIndex ,i4SetValIsisIPRAExistState)
#define nmhSetIsisIPRAAdminState(i4IsisSysInstance , i4IsisIPRAType , i4IsisIPRAIndex ,i4SetValIsisIPRAAdminState)	\
	nmhSetCmn(IsisIPRAAdminState, 11, IsisIPRAAdminStateSet, NULL, NULL, 0, 0, 3, "%i %i %i %i", i4IsisSysInstance , i4IsisIPRAType , i4IsisIPRAIndex ,i4SetValIsisIPRAAdminState)
#define nmhSetIsisIPRAMetric(i4IsisSysInstance , i4IsisIPRAType , i4IsisIPRAIndex ,i4SetValIsisIPRAMetric)	\
	nmhSetCmn(IsisIPRAMetric, 11, IsisIPRAMetricSet, NULL, NULL, 0, 0, 3, "%i %i %i %i", i4IsisSysInstance , i4IsisIPRAType , i4IsisIPRAIndex ,i4SetValIsisIPRAMetric)
#define nmhSetIsisIPRAMetricType(i4IsisSysInstance , i4IsisIPRAType , i4IsisIPRAIndex ,i4SetValIsisIPRAMetricType)	\
	nmhSetCmn(IsisIPRAMetricType, 11, IsisIPRAMetricTypeSet, NULL, NULL, 0, 0, 3, "%i %i %i %i", i4IsisSysInstance , i4IsisIPRAType , i4IsisIPRAIndex ,i4SetValIsisIPRAMetricType)
#define nmhSetIsisIPRASNPAAddress(i4IsisSysInstance , i4IsisIPRAType , i4IsisIPRAIndex ,pSetValIsisIPRASNPAAddress)	\
	nmhSetCmn(IsisIPRASNPAAddress, 11, IsisIPRASNPAAddressSet, NULL, NULL, 0, 0, 3, "%i %i %i %s", i4IsisSysInstance , i4IsisIPRAType , i4IsisIPRAIndex ,pSetValIsisIPRASNPAAddress)

#endif
