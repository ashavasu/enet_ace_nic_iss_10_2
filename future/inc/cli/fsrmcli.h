/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsrmcli.h,v 1.8 2014/08/13 12:51:42 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsRmHbInterval[10];
extern UINT4 FsRmPeerDeadInterval[10];
extern UINT4 FsRmTrcLevel[10];
extern UINT4 FsRmPeerDeadIntMultiplier[10];
extern UINT4 FsRmSwitchId[10];
extern UINT4 FsRmConfiguredState[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsRmHbInterval(i4SetValFsRmHbInterval) \
 nmhSetCmn(FsRmHbInterval, 10, FsRmHbIntervalSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsRmHbInterval)
#define nmhSetFsRmPeerDeadInterval(i4SetValFsRmPeerDeadInterval) \
 nmhSetCmn(FsRmPeerDeadInterval, 10, FsRmPeerDeadIntervalSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsRmPeerDeadInterval)
#define nmhSetFsRmTrcLevel(u4SetValFsRmTrcLevel) \
 nmhSetCmn(FsRmTrcLevel, 10, FsRmTrcLevelSet, NULL, NULL, 0, 0, 0, "%u", u4SetValFsRmTrcLevel)
#define nmhSetFsRmPeerDeadIntMultiplier(i4SetValFsRmPeerDeadIntMultiplier) \
 nmhSetCmn(FsRmPeerDeadIntMultiplier, 10, FsRmPeerDeadIntMultiplierSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsRmPeerDeadIntMultiplier)
#define nmhSetFsRmSwitchId(i4SetValFsRmSwitchId) \
 nmhSetCmn(FsRmSwitchId, 10, FsRmSwitchIdSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsRmSwitchId)
#define nmhSetFsRmConfiguredState(i4SetValFsRmConfiguredState) \
 nmhSetCmn(FsRmConfiguredState, 10, FsRmConfiguredStateSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsRmConfiguredState)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsRmStackPortCount[10];
extern UINT4 FsRmColdStandby[10];
extern UINT4 FsRmModuleTrc[10];
extern UINT4 FsRmProtocolRestartFlag[10];
extern UINT4 FsRmProtocolRestartRetryCnt[10];
extern UINT4 FsRmHitlessRestartFlag[10];
extern UINT4 FsRmCopyPeerSyLogFile[10];
extern UINT4 FsRmDynamicSyncAuditTrigger[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsRmStackPortCount(i4SetValFsRmStackPortCount) \
 nmhSetCmn(FsRmStackPortCount, 10, FsRmStackPortCountSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsRmStackPortCount)
#define nmhSetFsRmColdStandby(i4SetValFsRmColdStandby) \
 nmhSetCmn(FsRmColdStandby, 10, FsRmColdStandbySet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsRmColdStandby)
#define nmhSetFsRmModuleTrc(u4SetValFsRmModuleTrc) \
 nmhSetCmn(FsRmModuleTrc, 10, FsRmModuleTrcSet, NULL, NULL, 0, 0, 0, "%u", u4SetValFsRmModuleTrc)
#define nmhSetFsRmProtocolRestartFlag(i4SetValFsRmProtocolRestartFlag) \
 nmhSetCmn(FsRmProtocolRestartFlag, 10, FsRmProtocolRestartFlagSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsRmProtocolRestartFlag)
#define nmhSetFsRmProtocolRestartRetryCnt(u4SetValFsRmProtocolRestartRetryCnt) \
 nmhSetCmn(FsRmProtocolRestartRetryCnt, 10, FsRmProtocolRestartRetryCntSet, NULL, NULL, 0, 0, 0, "%u", u4SetValFsRmProtocolRestartRetryCnt)
#define nmhSetFsRmHitlessRestartFlag(i4SetValFsRmHitlessRestartFlag) \
 nmhSetCmn(FsRmHitlessRestartFlag, 10, FsRmHitlessRestartFlagSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsRmHitlessRestartFlag)
#define nmhSetFsRmCopyPeerSyLogFile(i4InitiateCopy) \
 nmhSetCmn(FsRmCopyPeerSyLogFile, 10, FsRmCopyPeerSyLogFileSet, NULL, NULL, 0, 0, 0, "%i", i4InitiateCopy)
#define nmhSetFsRmDynamicSyncAuditTrigger(u4SetValFsRmDynamicSyncAuditTrigger)  \
 nmhSetCmn(FsRmDynamicSyncAuditTrigger, 10, FsRmDynamicSyncAuditTriggerSet, NULL, NULL, 0, 0, 0, "%u", u4SetValFsRmDynamicSyncAuditTrigger)

#endif
