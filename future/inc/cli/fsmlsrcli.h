/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: fsmlsrcli.h,v 1.3 2013/07/26 13:31:51 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsInSegmentDirection[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsInSegmentDirection(pMplsInSegmentIndex ,i4SetValFsMplsInSegmentDirection) \
 nmhSetCmn(FsMplsInSegmentDirection, 13, FsMplsInSegmentDirectionSet, NULL, NULL, 0, 0, 1, "%s %i", pMplsInSegmentIndex ,i4SetValFsMplsInSegmentDirection)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsOutSegmentDirection[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsOutSegmentDirection(pMplsOutSegmentIndex ,i4SetValFsMplsOutSegmentDirection) \
 nmhSetCmn(FsMplsOutSegmentDirection, 13, FsMplsOutSegmentDirectionSet, NULL, NULL, 0, 0, 1, "%s %i", pMplsOutSegmentIndex ,i4SetValFsMplsOutSegmentDirection)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsLsrRfc6428CompatibleCodePoint[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsLsrRfc6428CompatibleCodePoint(i4SetValFsMplsLsrRfc6428CompatibleCodePoint) \
 nmhSetCmn(FsMplsLsrRfc6428CompatibleCodePoint, 12, FsMplsLsrRfc6428CompatibleCodePointSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsMplsLsrRfc6428CompatibleCodePoint)

#endif
