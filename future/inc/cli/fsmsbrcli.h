/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmsbrcli.h,v 1.3 2008/10/31 13:09:30 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDot1dStpContextId[12];
extern UINT4 FsDot1dStpPriority[12];
extern UINT4 FsDot1dStpBridgeMaxAge[12];
extern UINT4 FsDot1dStpBridgeHelloTime[12];
extern UINT4 FsDot1dStpBridgeForwardDelay[12];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDot1dStpPort[12];
extern UINT4 FsDot1dStpPortPriority[12];
extern UINT4 FsDot1dStpPortEnable[12];
extern UINT4 FsDot1dStpPortPathCost[12];
extern UINT4 FsDot1dStpPortPathCost32[12];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDot1dTpAgingTime[12];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDot1dStaticAddress[12];
extern UINT4 FsDot1dStaticReceivePort[12];
extern UINT4 FsDot1dStaticRowStatus[12];
extern UINT4 FsDot1dStaticStatus[12];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDot1dStaticAllowedIsMember[12];
