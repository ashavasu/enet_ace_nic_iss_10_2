/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsdhclcli.h,v 1.4 2015/06/26 12:34:21 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DhcpClientConfigIfIndex[12];
extern UINT4 DhcpClientRenew[12];
extern UINT4 DhcpClientRebind[12];
extern UINT4 DhcpClientInform[12];
extern UINT4 DhcpClientRelease[12];
extern UINT4 DhcpClientIdentifier[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDhcpClientRenew(i4DhcpClientConfigIfIndex ,i4SetValDhcpClientRenew) \
 nmhSetCmn(DhcpClientRenew, 12, DhcpClientRenewSet, DhcpCProtocolLock, DhcpCProtocolUnlock, 0, 0, 1, "%i %i", i4DhcpClientConfigIfIndex ,i4SetValDhcpClientRenew)
#define nmhSetDhcpClientRebind(i4DhcpClientConfigIfIndex ,i4SetValDhcpClientRebind) \
 nmhSetCmn(DhcpClientRebind, 12, DhcpClientRebindSet, DhcpCProtocolLock, DhcpCProtocolUnlock, 0, 0, 1, "%i %i", i4DhcpClientConfigIfIndex ,i4SetValDhcpClientRebind)
#define nmhSetDhcpClientInform(i4DhcpClientConfigIfIndex ,i4SetValDhcpClientInform) \
 nmhSetCmn(DhcpClientInform, 12, DhcpClientInformSet, DhcpCProtocolLock, DhcpCProtocolUnlock, 0, 0, 1, "%i %i", i4DhcpClientConfigIfIndex ,i4SetValDhcpClientInform)
#define nmhSetDhcpClientRelease(i4DhcpClientConfigIfIndex ,i4SetValDhcpClientRelease) \
 nmhSetCmn(DhcpClientRelease, 12, DhcpClientReleaseSet, DhcpCProtocolLock, DhcpCProtocolUnlock, 0, 0, 1, "%i %i", i4DhcpClientConfigIfIndex ,i4SetValDhcpClientRelease)
#define nmhSetDhcpClientIdentifier(i4DhcpClientConfigIfIndex ,pSetValDhcpClientIdentifier) \
 nmhSetCmn(DhcpClientIdentifier, 12, DhcpClientIdentifierSet, DhcpCProtocolLock, DhcpCProtocolUnlock, 0, 0, 1, "%i %s", i4DhcpClientConfigIfIndex ,pSetValDhcpClientIdentifier)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DhcpClientDebugTrace[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDhcpClientDebugTrace(i4SetValDhcpClientDebugTrace) \
 nmhSetCmn(DhcpClientDebugTrace, 10, DhcpClientDebugTraceSet, DhcpCProtocolLock, DhcpCProtocolUnlock, 0, 0, 0, "%i", i4SetValDhcpClientDebugTrace)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DhcpClientOptIfIndex[12];
extern UINT4 DhcpClientOptType[12];
extern UINT4 DhcpClientOptLen[12];
extern UINT4 DhcpClientOptVal[12];
extern UINT4 DhcpClientOptRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDhcpClientOptLen(i4DhcpClientOptIfIndex, i4DhcpClientOptType, i4DhcpClientOptLen) \
 nmhSetCmn(DhcpClientOptLen, 12, DhcpClientOptLenSet, DhcpCProtocolLock, DhcpCProtocolUnlock, 0, 0, 2, "%i %i %i", i4DhcpClientOptIfIndex, i4DhcpClientOptType, i4DhcpClientOptLen)
#define nmhSetDhcpClientOptVal(i4DhcpClientOptIfIndex, i4DhcpClientOptType, pDhcpClientOptVal)\
 nmhSetCmn(DhcpClientOptVal, 12, DhcpClientOptValSet, DhcpCProtocolLock, DhcpCProtocolUnlock, 0, 0, 2, "%i %i %s", i4DhcpClientOptIfIndex, i4DhcpClientOptType, pDhcpClientOptVal)
#define nmhSetDhcpClientOptRowStatus(i4DhcpClientOptIfIndex , i4DhcpClientOptType ,i4SetValDhcpClientOptRowStatus) \
 nmhSetCmn(DhcpClientOptRowStatus, 12, DhcpClientOptRowStatusSet, DhcpCProtocolLock, DhcpCProtocolUnlock, 0, 1, 2, "%i %i %i", i4DhcpClientOptIfIndex , i4DhcpClientOptType ,i4SetValDhcpClientOptRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DhcpClientIfIndex[12];
extern UINT4 DhcpClientCounterReset[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDhcpClientCounterReset(i4DhcpClientIfIndex ,i4SetValDhcpClientCounterReset) \
 nmhSetCmn(DhcpClientCounterReset, 12, DhcpClientCounterResetSet, DhcpCProtocolLock, DhcpCProtocolUnlock, 0, 0, 1, "%i %i", i4DhcpClientIfIndex ,i4SetValDhcpClientCounterReset)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DhcpClientFastAccess[10];
extern UINT4 DhcpClientFastAccessDiscoverTimeOut[10];
extern UINT4 DhcpClientFastAccessNullStateTimeOut[10];
extern UINT4 DhcpClientFastAccessArpCheckTimeOut[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDhcpClientFastAccess(i4SetValDhcpClientFastAccess) \
 nmhSetCmn(DhcpClientFastAccess, 10, DhcpClientFastAccessSet, DhcpCProtocolLock, DhcpCProtocolUnlock, 0, 0, 0, "%i", i4SetValDhcpClientFastAccess)
#define nmhSetDhcpClientFastAccessDiscoverTimeOut(i4SetValDhcpClientFastAccessDiscoverTimeOut) \
 nmhSetCmn(DhcpClientFastAccessDiscoverTimeOut, 10, DhcpClientFastAccessDiscoverTimeOutSet, DhcpCProtocolLock, DhcpCProtocolUnlock, 0, 0, 0, "%i", i4SetValDhcpClientFastAccessDiscoverTimeOut)
#define nmhSetDhcpClientFastAccessNullStateTimeOut(i4SetValDhcpClientFastAccessNullStateTimeOut) \
 nmhSetCmn(DhcpClientFastAccessNullStateTimeOut, 10, DhcpClientFastAccessNullStateTimeOutSet, DhcpCProtocolLock, DhcpCProtocolUnlock, 0, 0, 0, "%i", i4SetValDhcpClientFastAccessNullStateTimeOut)
#define nmhSetDhcpClientFastAccessArpCheckTimeOut(i4SetValDhcpClientFastAccessArpCheckTimeOut) \
 nmhSetCmn(DhcpClientFastAccessArpCheckTimeOut, 10, DhcpClientFastAccessArpCheckTimeOutSet, DhcpCProtocolLock, DhcpCProtocolUnlock, 0, 0, 0, "%i", i4SetValDhcpClientFastAccessArpCheckTimeOut)

#endif
