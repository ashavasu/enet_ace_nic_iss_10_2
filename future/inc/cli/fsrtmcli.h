/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsrtmcli.h,v 1.5 2016/02/08 10:40:13 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsRrdRouterId[10];
extern UINT4 FsRrdFilterByOspfTag[10];
extern UINT4 FsRrdFilterOspfTag[10];
extern UINT4 FsRrdFilterOspfTagMask[10];
extern UINT4 FsRrdRouterASNumber[10];
extern UINT4 FsRrdAdminStatus[10];
extern UINT4 FsRtmIpStaticRouteDistance[10];
extern UINT4 FsEcmpAcrossProtocolAdminStatus[10];
extern UINT4 FsRtmRouteLeakStatus[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsRrdRouterId(u4SetValFsRrdRouterId) \
 nmhSetCmn(FsRrdRouterId, 10, FsRrdRouterIdSet, NULL, NULL, 0, 0, 0, "%p", u4SetValFsRrdRouterId)
#define nmhSetFsRrdFilterByOspfTag(i4SetValFsRrdFilterByOspfTag) \
 nmhSetCmn(FsRrdFilterByOspfTag, 10, FsRrdFilterByOspfTagSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsRrdFilterByOspfTag)
#define nmhSetFsRrdFilterOspfTag(i4SetValFsRrdFilterOspfTag) \
 nmhSetCmn(FsRrdFilterOspfTag, 10, FsRrdFilterOspfTagSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsRrdFilterOspfTag)
#define nmhSetFsRrdFilterOspfTagMask(i4SetValFsRrdFilterOspfTagMask) \
 nmhSetCmn(FsRrdFilterOspfTagMask, 10, FsRrdFilterOspfTagMaskSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsRrdFilterOspfTagMask)
#define nmhSetFsRrdRouterASNumber(i4SetValFsRrdRouterASNumber) \
 nmhSetCmn(FsRrdRouterASNumber, 10, FsRrdRouterASNumberSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsRrdRouterASNumber)
#define nmhSetFsRrdAdminStatus(i4SetValFsRrdAdminStatus) \
 nmhSetCmn(FsRrdAdminStatus, 10, FsRrdAdminStatusSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsRrdAdminStatus)
#define nmhSetFsRtmIpStaticRouteDistance(i4SetValFsRtmIpStaticRouteDistance)    \
        nmhSetCmn(FsRtmIpStaticRouteDistance, 10, FsRtmIpStaticRouteDistanceSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsRtmIpStaticRouteDistance)
#define nmhSetFsEcmpAcrossProtocolAdminStatus(i4SetValFsEcmpAcrossProtocolAdminStatus) \
     nmhSetCmn(FsEcmpAcrossProtocolAdminStatus, 10, FsEcmpAcrossProtocolAdminStatusSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsEcmpAcrossProtocolAdminStatus)
#define nmhSetFsRtmRouteLeakStatus(i4SetValFsRtmRouteLeakStatus)        \
     nmhSetCmnWithLock(FsRtmRouteLeakStatus, 10, FsRtmRouteLeakStatusSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsRtmRouteLeakStatus)
#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsRrdControlDestIpAddress[11];
extern UINT4 FsRrdControlNetMask[11];
extern UINT4 FsRrdControlSourceProto[11];
extern UINT4 FsRrdControlDestProto[11];
extern UINT4 FsRrdControlRouteExportFlag[11];
extern UINT4 FsRrdControlRowStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsRrdControlSourceProto(u4FsRrdControlDestIpAddress , u4FsRrdControlNetMask ,i4SetValFsRrdControlSourceProto) \
 nmhSetCmn(FsRrdControlSourceProto, 11, FsRrdControlSourceProtoSet, NULL, NULL, 0, 0, 2, "%p %p %i", u4FsRrdControlDestIpAddress , u4FsRrdControlNetMask ,i4SetValFsRrdControlSourceProto)
#define nmhSetFsRrdControlDestProto(u4FsRrdControlDestIpAddress , u4FsRrdControlNetMask ,i4SetValFsRrdControlDestProto) \
 nmhSetCmn(FsRrdControlDestProto, 11, FsRrdControlDestProtoSet, NULL, NULL, 0, 0, 2, "%p %p %i", u4FsRrdControlDestIpAddress , u4FsRrdControlNetMask ,i4SetValFsRrdControlDestProto)
#define nmhSetFsRrdControlRouteExportFlag(u4FsRrdControlDestIpAddress , u4FsRrdControlNetMask ,i4SetValFsRrdControlRouteExportFlag) \
 nmhSetCmn(FsRrdControlRouteExportFlag, 11, FsRrdControlRouteExportFlagSet, NULL, NULL, 0, 0, 2, "%p %p %i", u4FsRrdControlDestIpAddress , u4FsRrdControlNetMask ,i4SetValFsRrdControlRouteExportFlag)
#define nmhSetFsRrdControlRowStatus(u4FsRrdControlDestIpAddress , u4FsRrdControlNetMask ,i4SetValFsRrdControlRowStatus) \
 nmhSetCmn(FsRrdControlRowStatus, 11, FsRrdControlRowStatusSet, NULL, NULL, 0, 1, 2, "%p %p %i", u4FsRrdControlDestIpAddress , u4FsRrdControlNetMask ,i4SetValFsRrdControlRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsRrdRoutingProtoId[11];
extern UINT4 FsRrdAllowOspfAreaRoutes[11];
extern UINT4 FsRrdAllowOspfExtRoutes[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsRrdAllowOspfAreaRoutes(i4FsRrdRoutingProtoId ,i4SetValFsRrdAllowOspfAreaRoutes) \
 nmhSetCmn(FsRrdAllowOspfAreaRoutes, 11, FsRrdAllowOspfAreaRoutesSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsRrdRoutingProtoId ,i4SetValFsRrdAllowOspfAreaRoutes)
#define nmhSetFsRrdAllowOspfExtRoutes(i4FsRrdRoutingProtoId ,i4SetValFsRrdAllowOspfExtRoutes) \
 nmhSetCmn(FsRrdAllowOspfExtRoutes, 11, FsRrdAllowOspfExtRoutesSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsRrdRoutingProtoId ,i4SetValFsRrdAllowOspfExtRoutes)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsRtmCommonRouteDest[11];
extern UINT4 FsRtmCommonRouteMask[11];
extern UINT4 FsRtmCommonRouteTos[11];
extern UINT4 FsRtmCommonRouteNextHop[11];
extern UINT4 FsRtmCommonRouteIfIndex[11];
extern UINT4 FsRtmCommonRouteType[11];
extern UINT4 FsRtmCommonRouteInfo[11];
extern UINT4 FsRtmCommonRouteNextHopAS[11];
extern UINT4 FsRtmCommonRouteMetric1[11];
extern UINT4 FsRtmCommonRoutePrivateStatus[11];
extern UINT4 FsRtmCommonRoutePreference[11];
extern UINT4 FsRtmCommonRouteStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsRtmCommonRouteIfIndex(u4FsRtmCommonRouteDest , u4FsRtmCommonRouteMask , i4FsRtmCommonRouteTos , u4FsRtmCommonRouteNextHop ,i4SetValFsRtmCommonRouteIfIndex) \
 nmhSetCmn(FsRtmCommonRouteIfIndex, 11, FsRtmCommonRouteIfIndexSet, NULL, NULL, 0, 0, 4, "%p %p %i %p %i", u4FsRtmCommonRouteDest , u4FsRtmCommonRouteMask , i4FsRtmCommonRouteTos , u4FsRtmCommonRouteNextHop ,i4SetValFsRtmCommonRouteIfIndex)
#define nmhSetFsRtmCommonRouteType(u4FsRtmCommonRouteDest , u4FsRtmCommonRouteMask , i4FsRtmCommonRouteTos , u4FsRtmCommonRouteNextHop ,i4SetValFsRtmCommonRouteType) \
 nmhSetCmn(FsRtmCommonRouteType, 11, FsRtmCommonRouteTypeSet, NULL, NULL, 0, 0, 4, "%p %p %i %p %i", u4FsRtmCommonRouteDest , u4FsRtmCommonRouteMask , i4FsRtmCommonRouteTos , u4FsRtmCommonRouteNextHop ,i4SetValFsRtmCommonRouteType)
#define nmhSetFsRtmCommonRouteInfo(u4FsRtmCommonRouteDest , u4FsRtmCommonRouteMask , i4FsRtmCommonRouteTos , u4FsRtmCommonRouteNextHop ,pSetValFsRtmCommonRouteInfo) \
 nmhSetCmn(FsRtmCommonRouteInfo, 11, FsRtmCommonRouteInfoSet, NULL, NULL, 0, 0, 4, "%p %p %i %p %o", u4FsRtmCommonRouteDest , u4FsRtmCommonRouteMask , i4FsRtmCommonRouteTos , u4FsRtmCommonRouteNextHop ,pSetValFsRtmCommonRouteInfo)
#define nmhSetFsRtmCommonRouteNextHopAS(u4FsRtmCommonRouteDest , u4FsRtmCommonRouteMask , i4FsRtmCommonRouteTos , u4FsRtmCommonRouteNextHop ,i4SetValFsRtmCommonRouteNextHopAS) \
 nmhSetCmn(FsRtmCommonRouteNextHopAS, 11, FsRtmCommonRouteNextHopASSet, NULL, NULL, 0, 0, 4, "%p %p %i %p %i", u4FsRtmCommonRouteDest , u4FsRtmCommonRouteMask , i4FsRtmCommonRouteTos , u4FsRtmCommonRouteNextHop ,i4SetValFsRtmCommonRouteNextHopAS)
#define nmhSetFsRtmCommonRouteMetric1(u4FsRtmCommonRouteDest , u4FsRtmCommonRouteMask , i4FsRtmCommonRouteTos , u4FsRtmCommonRouteNextHop ,i4SetValFsRtmCommonRouteMetric1) \
 nmhSetCmn(FsRtmCommonRouteMetric1, 11, FsRtmCommonRouteMetric1Set, NULL, NULL, 0, 0, 4, "%p %p %i %p %i", u4FsRtmCommonRouteDest , u4FsRtmCommonRouteMask , i4FsRtmCommonRouteTos , u4FsRtmCommonRouteNextHop ,i4SetValFsRtmCommonRouteMetric1)
#define nmhSetFsRtmCommonRoutePrivateStatus(u4FsRtmCommonRouteDest , u4FsRtmCommonRouteMask , i4FsRtmCommonRouteTos , u4FsRtmCommonRouteNextHop ,i4SetValFsRtmCommonRoutePrivateStatus) \
 nmhSetCmn(FsRtmCommonRoutePrivateStatus, 11, FsRtmCommonRoutePrivateStatusSet, NULL, NULL, 0, 0, 4, "%p %p %i %p %i", u4FsRtmCommonRouteDest , u4FsRtmCommonRouteMask , i4FsRtmCommonRouteTos , u4FsRtmCommonRouteNextHop ,i4SetValFsRtmCommonRoutePrivateStatus)
#define nmhSetFsRtmCommonRoutePreference(u4FsRtmCommonRouteDest , u4FsRtmCommonRouteMask , i4FsRtmCommonRouteTos , u4FsRtmCommonRouteNextHop ,i4SetValFsRtmCommonRoutePreference)       \
        nmhSetCmn(FsRtmCommonRoutePreference, 11, FsRtmCommonRoutePreferenceSet, NULL, NULL, 0, 0, 4, "%p %p %i %p %i", u4FsRtmCommonRouteDest , u4FsRtmCommonRouteMask , i4FsRtmCommonRouteTos , u4FsRtmCommonRouteNextHop ,i4SetValFsRtmCommonRoutePreference)
#define nmhSetFsRtmCommonRouteStatus(u4FsRtmCommonRouteDest , u4FsRtmCommonRouteMask , i4FsRtmCommonRouteTos , u4FsRtmCommonRouteNextHop ,i4SetValFsRtmCommonRouteStatus) \
 nmhSetCmn(FsRtmCommonRouteStatus, 11, FsRtmCommonRouteStatusSet, NULL, NULL, 0, 1, 4, "%p %p %i %p %i", u4FsRtmCommonRouteDest , u4FsRtmCommonRouteMask , i4FsRtmCommonRouteTos , u4FsRtmCommonRouteNextHop ,i4SetValFsRtmCommonRouteStatus)

#endif
