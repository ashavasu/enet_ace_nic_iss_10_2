/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmgmdcli.h,v 1.6 2016/06/24 09:42:21 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMgmdIgmpGlobalStatus[11];
extern UINT4 FsMgmdIgmpTraceLevel[11];
extern UINT4 FsMgmdIgmpDebugLevel[11];
extern UINT4 FsMgmdMldGlobalStatus[11];
extern UINT4 FsMgmdMldTraceLevel[11];
extern UINT4 FsMgmdMldDebugLevel[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMgmdIgmpGlobalStatus(i4SetValFsMgmdIgmpGlobalStatus) \
 nmhSetCmn(FsMgmdIgmpGlobalStatus, 11, FsMgmdIgmpGlobalStatusSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsMgmdIgmpGlobalStatus)
#define nmhSetFsMgmdIgmpTraceLevel(i4SetValFsMgmdIgmpTraceLevel) \
 nmhSetCmn(FsMgmdIgmpTraceLevel, 11, FsMgmdIgmpTraceLevelSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsMgmdIgmpTraceLevel)
#define nmhSetFsMgmdIgmpDebugLevel(i4SetValFsMgmdIgmpDebugLevel) \
 nmhSetCmn(FsMgmdIgmpDebugLevel, 11, FsMgmdIgmpDebugLevelSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsMgmdIgmpDebugLevel)
#define nmhSetFsMgmdMldGlobalStatus(i4SetValFsMgmdMldGlobalStatus) \
 nmhSetCmn(FsMgmdMldGlobalStatus, 11, FsMgmdMldGlobalStatusSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsMgmdMldGlobalStatus)
#define nmhSetFsMgmdMldTraceLevel(i4SetValFsMgmdMldTraceLevel) \
 nmhSetCmn(FsMgmdMldTraceLevel, 11, FsMgmdMldTraceLevelSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsMgmdMldTraceLevel)
#define nmhSetFsMgmdMldDebugLevel(i4SetValFsMgmdMldDebugLevel) \
 nmhSetCmn(FsMgmdMldDebugLevel, 11, FsMgmdMldDebugLevelSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsMgmdMldDebugLevel)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMgmdInterfaceIfIndex[13];
extern UINT4 FsMgmdInterfaceAddrType[13];
extern UINT4 FsMgmdInterfaceAdminStatus[13];
extern UINT4 FsMgmdInterfaceFastLeaveStatus[13];
extern UINT4 FsMgmdInterfaceChannelTrackStatus[13];
extern UINT4 FsMgmdInterfaceGroupListId[13];
extern UINT4 FsMgmdInterfaceLimit[13];
extern UINT4 FsMgmdInterfaceJoinPktRate[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMgmdInterfaceAdminStatus(i4FsMgmdInterfaceIfIndex , i4FsMgmdInterfaceAddrType ,i4SetValFsMgmdInterfaceAdminStatus) \
 nmhSetCmn(FsMgmdInterfaceAdminStatus, 13, FsMgmdInterfaceAdminStatusSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4FsMgmdInterfaceIfIndex , i4FsMgmdInterfaceAddrType ,i4SetValFsMgmdInterfaceAdminStatus)
#define nmhSetFsMgmdInterfaceFastLeaveStatus(i4FsMgmdInterfaceIfIndex , i4FsMgmdInterfaceAddrType ,i4SetValFsMgmdInterfaceFastLeaveStatus) \
 nmhSetCmn(FsMgmdInterfaceFastLeaveStatus, 13, FsMgmdInterfaceFastLeaveStatusSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4FsMgmdInterfaceIfIndex , i4FsMgmdInterfaceAddrType ,i4SetValFsMgmdInterfaceFastLeaveStatus)
#define nmhSetFsMgmdInterfaceChannelTrackStatus(i4FsMgmdInterfaceIfIndex , i4FsMgmdInterfaceAddrType ,i4SetValFsMgmdInterfaceChannelTrackStatus) \
 nmhSetCmn(FsMgmdInterfaceChannelTrackStatus, 13, FsMgmdInterfaceChannelTrackStatusSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4FsMgmdInterfaceIfIndex , i4FsMgmdInterfaceAddrType ,i4SetValFsMgmdInterfaceChannelTrackStatus)
#define nmhSetFsMgmdInterfaceGroupListId(i4FsMgmdInterfaceIfIndex , i4FsMgmdInterfaceAddrType ,u4SetValFsMgmdInterfaceGroupListId) \
 nmhSetCmn(FsMgmdInterfaceGroupListId, 13, FsMgmdInterfaceGroupListIdSet, NULL, NULL, 0, 0, 2, "%i %i %u", i4FsMgmdInterfaceIfIndex , i4FsMgmdInterfaceAddrType ,u4SetValFsMgmdInterfaceGroupListId)
#define nmhSetFsMgmdInterfaceLimit(i4FsMgmdInterfaceIfIndex , i4FsMgmdInterfaceAddrType ,u4SetValFsMgmdInterfaceLimit) \
 nmhSetCmn(FsMgmdInterfaceLimit, 13, FsMgmdInterfaceLimitSet, NULL, NULL, 0, 0, 2, "%i %i %u", i4FsMgmdInterfaceIfIndex , i4FsMgmdInterfaceAddrType ,u4SetValFsMgmdInterfaceLimit)
#define nmhSetFsMgmdInterfaceJoinPktRate(i4FsMgmdInterfaceIfIndex , i4FsMgmdInterfaceAddrType ,i4SetValFsMgmdInterfaceJoinPktRate) \
 nmhSetCmn(FsMgmdInterfaceJoinPktRate, 13, FsMgmdInterfaceJoinPktRateSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4FsMgmdInterfaceIfIndex , i4FsMgmdInterfaceAddrType ,i4SetValFsMgmdInterfaceJoinPktRate)

#endif
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMgmdGlobalLimit[12];
extern UINT4 FsMgmdSSMMapStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMgmdGlobalLimit(u4SetValFsMgmdGlobalLimit) \
 nmhSetCmn(FsMgmdGlobalLimit, 12, FsMgmdGlobalLimitSet, NULL, NULL, 0, 0, 0, "%u", u4SetValFsMgmdGlobalLimit)
#define nmhSetFsMgmdSSMMapStatus(i4SetValFsMgmdSSMMapStatus) \
 nmhSetCmn(FsMgmdSSMMapStatus, 12, FsMgmdSSMMapStatusSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsMgmdSSMMapStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMgmdSSMMapStartGrpAddress[13];
extern UINT4 FsMgmdSSMMapEndGrpAddress[13];
extern UINT4 FsMgmdSSMMapSourceAddress[13];
extern UINT4 FsMgmdSSMMapRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMgmdSSMMapRowStatus(u4FsMgmdSSMMapStartGrpAddress , u4FsMgmdSSMMapEndGrpAddress , u4FsMgmdSSMMapSourceAddress ,i4SetValFsMgmdSSMMapRowStatus) \
 nmhSetCmn(FsMgmdSSMMapRowStatus, 13, FsMgmdSSMMapRowStatusSet, NULL, NULL, 0, 1, 3, "%p %p %p %i", u4FsMgmdSSMMapStartGrpAddress , u4FsMgmdSSMMapEndGrpAddress , u4FsMgmdSSMMapSourceAddress ,i4SetValFsMgmdSSMMapRowStatus)

#endif
