/*****************************************************************************/
/* Copyright (C) 2008 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2008                                               */
/* $Id: taccli.h,v 1.6 2010/05/19 07:12:28 prabuc Exp $                                                                    */
/*****************************************************************************/
/*    FILE  NAME            : taccli.h                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : Transmission and Admission Control (TAC)       */
/*    MODULE NAME           : TAC module global Structures and definitions   */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains CLI related structure and   */
/*                            definitions                                    */
/*---------------------------------------------------------------------------*/

#ifndef _TACCLI_H_
#define _TACCLI_H_

#include "lr.h"
#include "cli.h"

/* Macros to distinguish the CLI commands */
enum
{
    TACM_CLI_IP_PROFILE = 1,
    TACM_CLI_NO_IP_PROFILE,
    TACM_CLI_PROFILE_ACTION,
    TACM_CLI_FILTER,
    TACM_CLI_NO_FILTER,
    TACM_CLI_PROFILE_ACTIVE,
    TACM_CLI_NO_PROFILE_ACTIVE,
    TACM_CLI_SHOW_PROFILE,
    TACM_CLI_DEBUG,
    TACM_CLI_NO_DEBUG,
    TACM_CLI_STATUS
};

enum
{
    TACM_CLI_ERR_PRF_ACTIVE = 1,
    TACM_CLI_ERR_INV_MCAST_ADDR,
    TACM_CLI_ERR_WCARD_MCAST_ADDR,
    TACM_CLI_ERR_WCARD_SRC_ADDR,
    TACM_CLI_ERR_INV_MCAST_RANGE,
    TACM_CLI_ERR_INV_SRC_RANGE,
    TACM_CLI_ERR_RSVD_MCAST_ADDR,
    TACM_CLI_MAX_ERR
};

#ifdef __TACCLI_C__
CONST CHR1 * gapc1TacCliErrorString[] =
{
    NULL,
    "!!! Profile Entry is active\r\n",
    "!!! Invalid multicast address configured\r\n",
    "!!! Group end address should be a wild card if group start address "
         "is a wild card\r\n",
    "!!! Source end address should be a wild card if source start address "
         "is a wild card\r\n",
    "!!! Group end address is less than group start address\r\n",
    "!!! Source end address is less than Source start address\r\n",
    "!!! Configured group address belongs to reserved multicast address\r\n",
    "\r\n"
};
#endif

#define TACM_CLI_MAX_ARGS                   5

/* Macros to create or delete a profile */
#define TACM_PROFILE_CREATE                 1
#define TACM_PROFILE_DELETE                 2

/* Macros to set the create or delete a filter */
#define TACM_FILTER_CREATE                  1
#define TACM_FILTER_DELETE                  2

/* Macros for displaying profile information */
#define TACM_CLI_SHOW_FILTER                1
#define TACM_CLI_SHOW_FILTER_PROFILE        2
#define TACM_CLI_SHOW_STATS                 3
#define TACM_CLI_SHOW_STATS_PROFILE         4

/* Macros to enable or disable the debug option */
#define TACM_CLI_ENABLE_DEBUG               1
#define TACM_CLI_DISABLE_DEBUG              2

/* Profile config mode */
#define TACM_CLI_MODE_PROFILE               "(config-profile)#"

/* Length of profile Id */
#define TACM_CLI_MAX_PROF_LEN               sizeof (UINT4)

/* Macros used to set invalid profile id and address type */
#define TACM_CLI_INVALID_PROFILE_ID         0
#define TACM_CLI_INVALID_ADDR_TYPE          0

INT4 cli_process_tacm_cmd
PROTO ((tCliHandle CliHandle, UINT4 u4Command, ...));

INT1 TacCliGetProfCfgPrompt
PROTO ((INT1 *pi1ModeName, INT1 *pi1DispStr));

INT4
TacShowRunningConfig PROTO ((tCliHandle CliHandle));
INT4
TacCliSetStatus (tCliHandle CliHandle, INT4 i4TacStatus);

INT4
TacCliGetStatus (tCliHandle CliHandle);

VOID
TacCliShowDebugging (tCliHandle CliHandle);

#endif
