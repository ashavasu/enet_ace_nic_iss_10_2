/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpipv6cli.h,v 1.10 2015/02/12 11:57:59 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIIpv6NdCacheMaxRetries[13];
extern UINT4 FsMIIpv6PmtuConfigStatus[13];
extern UINT4 FsMIIpv6PmtuTimeOutInterval[13];
extern UINT4 FsMIIpv6JumboEnable[13];
extern UINT4 FsMIIpv6ContextDebug[13];
extern UINT4 FsMIIpv6RFC5095Compatibility[13];
extern UINT4 FsMIIpv6RFC5942Compatibility[13];
extern UINT4 FsMIIpv6SENDSecLevel[13];
extern UINT4 FsMIIpv6SENDNbrSecLevel[13];
extern UINT4 FsMIIpv6SENDAuthType[13];
extern UINT4 FsMIIpv6SENDMinBits[13];
extern UINT4 FsMIIpv6SENDSecDAD[13];
extern UINT4 FsMIIpv6SENDPrefixChk[13];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIIpv6IfToken[13];
extern UINT4 FsMIIpv6IfRouterAdvStatus[13];
extern UINT4 FsMIIpv6IfRouterAdvFlags[13];
extern UINT4 FsMIIpv6IfHopLimit[13];
extern UINT4 FsMIIpv6IfDefRouterTime[13];
extern UINT4 FsMIIpv6IfReachableTime[13];
extern UINT4 FsMIIpv6IfRetransmitTime[13];
extern UINT4 FsMIIpv6IfDelayProbeTime[13];
extern UINT4 FsMIIpv6IfPrefixAdvStatus[13];
extern UINT4 FsMIIpv6IfMinRouterAdvTime[13];
extern UINT4 FsMIIpv6IfMaxRouterAdvTime[13];
extern UINT4 FsMIIpv6IfDADRetries[13];
extern UINT4 FsMIIpv6IfForwarding[13];
extern UINT4 FsMIIpv6IfIcmpErrInterval[13];
extern UINT4 FsMIIpv6IfIcmpTokenBucketSize[13];
extern UINT4 FsMIIpv6IfDestUnreachableMsg[13];
extern UINT4 FsMIIpv6IfRedirectMsg[13];
extern UINT4 FsMIIpv6IfUnnumAssocIPIf[13];
extern UINT4 FsMIIpv6IfAdvSrcLLAdr[13];
extern UINT4 FsMIIpv6IfAdvIntOpt[13];
extern UINT4 FsMIIpv6IfSENDSecStatus[13];
extern UINT4 FsMIIpv6IfSENDDeltaTimestamp[13];
extern UINT4 FsMIIpv6IfSENDFuzzTimestamp[13];
extern UINT4 FsMIIpv6IfSENDDriftTimestamp[13];
extern UINT4 FsMIIpv6IfDefRoutePreference[13];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIIpv6AddrAddress[13];
extern UINT4 FsMIIpv6AddrPrefixLen[13];
extern UINT4 FsMIIpv6AddrAdminStatus[13];
extern UINT4 FsMIIpv6AddrType[13];
extern UINT4 FsMIIpv6AddrProfIndex[13];
extern UINT4 FsMIIpv6AddrSENDCgaStatus[13];
extern UINT4 FsMIIpv6AddrSENDCgaModifier[13];
extern UINT4 FsMIIpv6AddrSENDCollisionCount[13];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIIpv6AddrProfileIndex[13];
extern UINT4 FsMIIpv6AddrProfileStatus[13];
extern UINT4 FsMIIpv6AddrProfilePrefixAdvStatus[13];
extern UINT4 FsMIIpv6AddrProfileOnLinkAdvStatus[13];
extern UINT4 FsMIIpv6AddrProfileAutoConfAdvStatus[13];
extern UINT4 FsMIIpv6AddrProfilePreferredTime[13];
extern UINT4 FsMIIpv6AddrProfileValidTime[13];
extern UINT4 FsMIIpv6AddrProfileValidLifeTimeFlag[13];
extern UINT4 FsMIIpv6AddrProfilePreferredLifeTimeFlag[13];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIIpv6PmtuDest[13];
extern UINT4 FsMIIpv6Pmtu[13];
extern UINT4 FsMIIpv6PmtuAdminStatus[13];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIIpv6TunlAccessListIndex[13];
extern UINT4 FsMIIpv6TunlAccessListSrcAddr[13];
extern UINT4 FsMIIpv6TunlAccessListAdminStatus[13];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIIpv6RouteDest[13];
extern UINT4 FsMIIpv6RoutePfxLength[13];
extern UINT4 FsMIIpv6RouteProtocol[13];
extern UINT4 FsMIIpv6RouteNextHop[13];
extern UINT4 FsMIIpv6RouteIfIndex[13];
extern UINT4 FsMIIpv6RouteMetric[13];
extern UINT4 FsMIIpv6RouteType[13];
extern UINT4 FsMIIpv6RouteTag[13];
extern UINT4 FsMIIpv6RouteAdminStatus[13];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIIpv6Protocol[13];
extern UINT4 FsMIIpv6Preference[13];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIIpv6NDProxyAddr[13];
extern UINT4 FsMIIpv6NDProxyAdminStatus[13];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIIpv6PingIndex[13];
extern UINT4 FsMIIpv6PingDest[13];
extern UINT4 FsMIIpv6PingIfIndex[13];
extern UINT4 FsMIIpv6PingContextId[13];
extern UINT4 FsMIIpv6PingAdminStatus[13];
extern UINT4 FsMIIpv6PingInterval[13];
extern UINT4 FsMIIpv6PingRcvTimeout[13];
extern UINT4 FsMIIpv6PingTries[13];
extern UINT4 FsMIIpv6PingSize[13];
extern UINT4 FsMIIpv6PingData[13];
extern UINT4 FsMIIpv6PingSrcAddr[13];
extern UINT4 FsMIIpv6PingZoneId[13];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIIpv6GlobalDebug[11];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIIpv6AddrSelPolicyPrefix[13];
extern UINT4 FsMIIpv6AddrSelPolicyPrefixLen[13];
extern UINT4 FsMIIpv6AddrSelPolicyIfIndex[13];
extern UINT4 FsMIIpv6AddrSelPolicyPrecedence[13];
extern UINT4 FsMIIpv6AddrSelPolicyLabel[13];
extern UINT4 FsMIIpv6AddrSelPolicyAddrType[13];
extern UINT4 FsMIIpv6AddrSelPolicyRowStatus[13];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIIpv6ScopeZoneIndexIfIndex[13];
extern UINT4 FsMIIpv6ScopeZoneIndexInterfaceLocal[13];
extern UINT4 FsMIIpv6ScopeZoneIndexLinkLocal[13];
extern UINT4 FsMIIpv6ScopeZoneIndex3[13];
extern UINT4 FsMIIpv6ScopeZoneIndexAdminLocal[13];
extern UINT4 FsMIIpv6ScopeZoneIndexSiteLocal[13];
extern UINT4 FsMIIpv6ScopeZoneIndex6[13];
extern UINT4 FsMIIpv6ScopeZoneIndex7[13];
extern UINT4 FsMIIpv6ScopeZoneIndexOrganizationLocal[13];
extern UINT4 FsMIIpv6ScopeZoneIndex9[13];
extern UINT4 FsMIIpv6ScopeZoneIndexA[13];
extern UINT4 FsMIIpv6ScopeZoneIndexB[13];
extern UINT4 FsMIIpv6ScopeZoneIndexC[13];
extern UINT4 FsMIIpv6ScopeZoneIndexD[13];
extern UINT4 FsMIIpv6ScopeZoneIndexE[13];
extern UINT4 FsMIIpv6IfScopeZoneRowStatus[13];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIIpv6ScopeZoneContextId[13];
extern UINT4 FsMIIpv6ScopeZoneName[13];
extern UINT4 FsMIIpv6IsDefaultScopeZone[13];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIIpv6RARouteIfIndex[13];
extern UINT4 FsMIIpv6RARoutePrefix[13];
extern UINT4 FsMIIpv6RARoutePrefixLen[13];
extern UINT4 FsMIIpv6RARoutePreference[13];
extern UINT4 FsMIIpv6RARouteLifetime[13];
extern UINT4 FsMIIpv6RARouteRowStatus[13];
extern UINT4 FsMIIpv6IfRaRDNSSIndex [13];
extern UINT4 FsMIIpv6IfRadvRDNSSOpen [13];
extern UINT4 FsMIIpv6IfRaRDNSSPreference [13];
extern UINT4 FsMIIpv6IfRaRDNSSLifetime [13];
extern UINT4 FsMIIpv6IfRaRDNSSAddrOne [13];
extern UINT4 FsMIIpv6IfRaRDNSSAddrTwo [13];
extern UINT4 FsMIIpv6IfRaRDNSSAddrThree [13]; 
extern UINT4 FsMIIpv6IfRaRDNSSRowStatus [13]; 
extern UINT4 FsMIIpv6IfDomainNameOne[13];
extern UINT4 FsMIIpv6IfDomainNameTwo[13];
extern UINT4 FsMIIpv6IfDomainNameThree[13];
extern UINT4 FsMIIpv6IfDnsLifeTime[13];
extern UINT4 FsMIIpv6IfRaRDNSSAddrOneLife[13];
extern UINT4 FsMIIpv6IfRaRDNSSAddrTwoLife[13];
extern UINT4 FsMIIpv6IfRaRDNSSAddrThreeLife[13];
extern UINT4 FsMIIpv6IfDomainNameOneLife[13];
extern UINT4 FsMIIpv6IfDomainNameTwoLife[13];
extern UINT4 FsMIIpv6IfDomainNameThreeLife[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMIIpv6IfRadvRDNSSOpen(i4FsMIIpv6IfRaRDNSSIndex ,i4SetValFsMIIpv6IfRadvRDNSSOpen) \
 nmhSetCmnWithLock(FsMIIpv6IfRadvRDNSSOpen, 13, FsMIIpv6IfRadvRDNSSOpenSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %i", i4FsMIIpv6IfRaRDNSSIndex ,i4SetValFsMIIpv6IfRadvRDNSSOpen)
#define nmhSetFsMIIpv6IfRaRDNSSPreference(i4FsMIIpv6IfRaRDNSSIndex ,i4SetValFsMIIpv6IfRaRDNSSPreference) \
 nmhSetCmnWithLock(FsMIIpv6IfRaRDNSSPreference, 13, FsMIIpv6IfRaRDNSSPreferenceSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %i", i4FsMIIpv6IfRaRDNSSIndex ,i4SetValFsMIIpv6IfRaRDNSSPreference)
#define nmhSetFsMIIpv6IfRaRDNSSLifetime(i4FsMIIpv6IfRaRDNSSIndex ,u4SetValFsMIIpv6IfRaRDNSSLifetime) \
 nmhSetCmnWithLock(FsMIIpv6IfRaRDNSSLifetime, 13, FsMIIpv6IfRaRDNSSLifetimeSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %u", i4FsMIIpv6IfRaRDNSSIndex ,u4SetValFsMIIpv6IfRaRDNSSLifetime)
#define nmhSetFsMIIpv6IfRaRDNSSAddrOne(i4FsMIIpv6IfRaRDNSSIndex ,pSetValFsMIIpv6IfRaRDNSSAddrOne) \
 nmhSetCmnWithLock(FsMIIpv6IfRaRDNSSAddrOne, 13, FsMIIpv6IfRaRDNSSAddrOneSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %s", i4FsMIIpv6IfRaRDNSSIndex ,pSetValFsMIIpv6IfRaRDNSSAddrOne)
#define nmhSetFsMIIpv6IfRaRDNSSAddrTwo(i4FsMIIpv6IfRaRDNSSIndex ,pSetValFsMIIpv6IfRaRDNSSAddrTwo) \
 nmhSetCmnWithLock(FsMIIpv6IfRaRDNSSAddrTwo, 13, FsMIIpv6IfRaRDNSSAddrTwoSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %s", i4FsMIIpv6IfRaRDNSSIndex ,pSetValFsMIIpv6IfRaRDNSSAddrTwo)
#define nmhSetFsMIIpv6IfRaRDNSSAddrThree(i4FsMIIpv6IfRaRDNSSIndex ,pSetValFsMIIpv6IfRaRDNSSAddrThree) \
 nmhSetCmnWithLock(FsMIIpv6IfRaRDNSSAddrThree, 13, FsMIIpv6IfRaRDNSSAddrThreeSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %s", i4FsMIIpv6IfRaRDNSSIndex ,pSetValFsMIIpv6IfRaRDNSSAddrThree)
#define nmhSetFsMIIpv6IfRaRDNSSRowStatus(i4FsMIIpv6IfRaRDNSSIndex ,i4SetValFsMIIpv6IfRaRDNSSRowStatus) \
 nmhSetCmnWithLock(FsMIIpv6IfRaRDNSSRowStatus, 13, FsMIIpv6IfRaRDNSSRowStatusSet, Ip6Lock, Ip6UnLock, 0, 1, 1, "%i %i", i4FsMIIpv6IfRaRDNSSIndex ,i4SetValFsMIIpv6IfRaRDNSSRowStatus)
#define nmhSetFsMIIpv6IfDomainNameOne(i4FsMIIpv6IfRaRDNSSIndex ,pSetValFsMIIpv6IfDomainNameOne) \
 nmhSetCmnWithLock(FsMIIpv6IfDomainNameOne, 13, FsMIIpv6IfDomainNameOneSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %s", i4FsMIIpv6IfRaRDNSSIndex ,pSetValFsMIIpv6IfDomainNameOne)
#define nmhSetFsMIIpv6IfDomainNameTwo(i4FsMIIpv6IfRaRDNSSIndex ,pSetValFsMIIpv6IfDomainNameTwo) \
 nmhSetCmnWithLock(FsMIIpv6IfDomainNameTwo, 13, FsMIIpv6IfDomainNameTwoSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %s", i4FsMIIpv6IfRaRDNSSIndex ,pSetValFsMIIpv6IfDomainNameTwo)
#define nmhSetFsMIIpv6IfDomainNameThree(i4FsMIIpv6IfRaRDNSSIndex ,pSetValFsMIIpv6IfDomainNameThree) \
 nmhSetCmnWithLock(FsMIIpv6IfDomainNameThree, 13, FsMIIpv6IfDomainNameThreeSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %s", i4FsMIIpv6IfRaRDNSSIndex ,pSetValFsMIIpv6IfDomainNameThree)
#define nmhSetFsMIIpv6IfDnsLifeTime(i4FsMIIpv6IfRaRDNSSIndex ,i4SetValFsMIIpv6IfDnsLifeTime) \
 nmhSetCmnWithLock(FsMIIpv6IfDnsLifeTime, 13, FsMIIpv6IfDnsLifeTimeSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %i", i4FsMIIpv6IfRaRDNSSIndex ,i4SetValFsMIIpv6IfDnsLifeTime)
#define nmhSetFsMIIpv6IfRaRDNSSAddrOneLife(i4FsMIIpv6IfRaRDNSSIndex ,u4SetValFsMIIpv6IfRaRDNSSAddrOneLife) \
 nmhSetCmnWithLock(FsMIIpv6IfRaRDNSSAddrOneLife, 13, FsMIIpv6IfRaRDNSSAddrOneLifeSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %u", i4FsMIIpv6IfRaRDNSSIndex ,u4SetValFsMIIpv6IfRaRDNSSAddrOneLife)
#define nmhSetFsMIIpv6IfRaRDNSSAddrTwoLife(i4FsMIIpv6IfRaRDNSSIndex ,u4SetValFsMIIpv6IfRaRDNSSAddrTwoLife) \
 nmhSetCmnWithLock(FsMIIpv6IfRaRDNSSAddrTwoLife, 13, FsMIIpv6IfRaRDNSSAddrTwoLifeSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %u", i4FsMIIpv6IfRaRDNSSIndex ,u4SetValFsMIIpv6IfRaRDNSSAddrTwoLife)
#define nmhSetFsMIIpv6IfRaRDNSSAddrThreeLife(i4FsMIIpv6IfRaRDNSSIndex ,u4SetValFsMIIpv6IfRaRDNSSAddrThreeLife) \
 nmhSetCmnWithLock(FsMIIpv6IfRaRDNSSAddrThreeLife, 13, FsMIIpv6IfRaRDNSSAddrThreeLifeSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %u", i4FsMIIpv6IfRaRDNSSIndex ,u4SetValFsMIIpv6IfRaRDNSSAddrThreeLife)
#define nmhSetFsMIIpv6IfDomainNameOneLife(i4FsMIIpv6IfRaRDNSSIndex ,u4SetValFsMIIpv6IfDomainNameOneLife) \
 nmhSetCmnWithLock(FsMIIpv6IfDomainNameOneLife, 13, FsMIIpv6IfDomainNameOneLifeSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %u", i4FsMIIpv6IfRaRDNSSIndex ,u4SetValFsMIIpv6IfDomainNameOneLife)
#define nmhSetFsMIIpv6IfDomainNameTwoLife(i4FsMIIpv6IfRaRDNSSIndex ,u4SetValFsMIIpv6IfDomainNameTwoLife) \
 nmhSetCmnWithLock(FsMIIpv6IfDomainNameTwoLife, 13, FsMIIpv6IfDomainNameTwoLifeSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %u", i4FsMIIpv6IfRaRDNSSIndex ,u4SetValFsMIIpv6IfDomainNameTwoLife)
#define nmhSetFsMIIpv6IfDomainNameThreeLife(i4FsMIIpv6IfRaRDNSSIndex ,u4SetValFsMIIpv6IfDomainNameThreeLife) \
 nmhSetCmnWithLock(FsMIIpv6IfDomainNameThreeLife, 13, FsMIIpv6IfDomainNameThreeLifeSet, Ip6Lock, Ip6UnLock, 0, 0, 1, "%i %u", i4FsMIIpv6IfRaRDNSSIndex ,u4SetValFsMIIpv6IfDomainNameThreeLife)
#endif
