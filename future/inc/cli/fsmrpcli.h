/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmrpcli.h,v 1.2 2009/09/23 10:42:03 prabuc Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMrpGlobalTraceOption[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMrpGlobalTraceOption(i4SetValFsMrpGlobalTraceOption)	\
	nmhSetCmn(FsMrpGlobalTraceOption, 11, FsMrpGlobalTraceOptionSet, MrpLock, MrpUnLock, 0, 0, 0, "%i", i4SetValFsMrpGlobalTraceOption)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMrpInstanceSystemControl[13];
extern UINT4 FsMrpInstanceTraceInputString[13];
extern UINT4 FsMrpInstanceNotifyVlanRegFailure[13];
extern UINT4 FsMrpInstanceNotifyMacRegFailure[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMrpInstanceSystemControl(u4Ieee8021BridgeBaseComponentId ,i4SetValFsMrpInstanceSystemControl)	\
	nmhSetCmn(FsMrpInstanceSystemControl, 13, FsMrpInstanceSystemControlSet, MrpLock, MrpUnLock, 0, 0, 1, "%u %i", u4Ieee8021BridgeBaseComponentId ,i4SetValFsMrpInstanceSystemControl)
#define nmhSetFsMrpInstanceTraceInputString(u4Ieee8021BridgeBaseComponentId ,pSetValFsMrpInstanceTraceInputString)	\
	nmhSetCmn(FsMrpInstanceTraceInputString, 13, FsMrpInstanceTraceInputStringSet, MrpLock, MrpUnLock, 0, 0, 1, "%u %s", u4Ieee8021BridgeBaseComponentId ,pSetValFsMrpInstanceTraceInputString)
#define nmhSetFsMrpInstanceNotifyVlanRegFailure(u4Ieee8021BridgeBaseComponentId ,i4SetValFsMrpInstanceNotifyVlanRegFailure)	\
	nmhSetCmn(FsMrpInstanceNotifyVlanRegFailure, 13, FsMrpInstanceNotifyVlanRegFailureSet, MrpLock, MrpUnLock, 0, 0, 1, "%u %i", u4Ieee8021BridgeBaseComponentId ,i4SetValFsMrpInstanceNotifyVlanRegFailure)
#define nmhSetFsMrpInstanceNotifyMacRegFailure(u4Ieee8021BridgeBaseComponentId ,i4SetValFsMrpInstanceNotifyMacRegFailure)	\
	nmhSetCmn(FsMrpInstanceNotifyMacRegFailure, 13, FsMrpInstanceNotifyMacRegFailureSet, MrpLock, MrpUnLock, 0, 0, 1, "%u %i", u4Ieee8021BridgeBaseComponentId ,i4SetValFsMrpInstanceNotifyMacRegFailure)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMrpPortPeriodicSEMStatus[13];
extern UINT4 FsMrpPortParticipantType[13];
extern UINT4 FsMrpPortRegAdminControl[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMrpPortPeriodicSEMStatus(u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValFsMrpPortPeriodicSEMStatus)	\
	nmhSetCmn(FsMrpPortPeriodicSEMStatus, 13, FsMrpPortPeriodicSEMStatusSet, MrpLock, MrpUnLock, 0, 0, 2, "%u %u %i", u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValFsMrpPortPeriodicSEMStatus)
#define nmhSetFsMrpPortParticipantType(u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValFsMrpPortParticipantType)	\
	nmhSetCmn(FsMrpPortParticipantType, 13, FsMrpPortParticipantTypeSet, MrpLock, MrpUnLock, 0, 0, 2, "%u %u %i", u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValFsMrpPortParticipantType)
#define nmhSetFsMrpPortRegAdminControl(u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValFsMrpPortRegAdminControl)	\
	nmhSetCmn(FsMrpPortRegAdminControl, 13, FsMrpPortRegAdminControlSet, MrpLock, MrpUnLock, 0, 0, 2, "%u %u %i", u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValFsMrpPortRegAdminControl)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMrpApplicationAddress[13];
extern UINT4 FsMrpAttributeType[13];
extern UINT4 FsMrpApplicantControlAdminStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMrpApplicantControlAdminStatus(u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort , tFsMrpApplicationAddress , i4FsMrpAttributeType ,i4SetValFsMrpApplicantControlAdminStatus)	\
	nmhSetCmn(FsMrpApplicantControlAdminStatus, 13, FsMrpApplicantControlAdminStatusSet, MrpLock, MrpUnLock, 0, 0, 4, "%u %u %m %i %i", u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort , tFsMrpApplicationAddress , i4FsMrpAttributeType ,i4SetValFsMrpApplicantControlAdminStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMrpPortStatsClearStatistics[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMrpPortStatsClearStatistics(u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort , tFsMrpApplicationAddress ,i4SetValFsMrpPortStatsClearStatistics)	\
	nmhSetCmn(FsMrpPortStatsClearStatistics, 13, FsMrpPortStatsClearStatisticsSet, MrpLock, MrpUnLock, 0, 0, 3, "%u %u %m %i", u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort , tFsMrpApplicationAddress ,i4SetValFsMrpPortStatsClearStatistics)

#endif
