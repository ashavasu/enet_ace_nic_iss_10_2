/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsosmicli.h,v 1.2 2008/10/31 13:09:30 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIOspfExtRouteDest[11];
extern UINT4 FsMIOspfExtRouteMask[11];
extern UINT4 FsMIOspfExtRouteTOS[11];
extern UINT4 FsMIOspfExtRouteMetric[11];
extern UINT4 FsMIOspfExtRouteMetricType[11];
extern UINT4 FsMIOspfExtRouteTag[11];
extern UINT4 FsMIOspfExtRouteFwdAdr[11];
extern UINT4 FsMIOspfExtRouteIfIndex[11];
extern UINT4 FsMIOspfExtRouteNextHop[11];
extern UINT4 FsMIOspfExtRouteStatus[11];

