/********************************************************************
* Copyright (C) Future Sotware,1997-98,2001
*
* $Id: cfacli.h,v 1.182 2017/12/26 11:06:16 siva Exp $
*
* Description: This file contains the constants, gloabl structures,
  typedefs, enums uased in  SNTP  module.
*
*******************************************************************/
#ifndef __CFACLI_H__
#define __CFACLI_H__
#include "cli.h"
#define MAX_LEN 1024
#define MAX_MAC_LENGTH 25
enum
{
  CLI_CFA_INTERFACE_CREATE = 1,
  CLI_CFA_INTERFACE_DELETE,
  CLI_CFA_MGMT_VLAN,
  CLI_CFA_NO_MGMT_VLAN,
  CLI_CFA_SHOW_MTU,
  CLI_CFA_SHOW_BRG_PORT_TYPE,
  CLI_CFA_SHOW_MGMT_VLAN,
  CLI_CFA_SHOW_IP_INT,
  CLI_CFA_INTERFACE_ADMIN_STATUS,  
  CLI_CFA_LINK_STATUS_TRAP,  
  CLI_CFA_SET_MTU,
  CLI_CFA_SET_PORT_MAC,
  CLI_CFA_FLOWCONTROL,
  CLI_CFA_PORT_MODE,
  CLI_CFA_PORT_MODE_OF,
  CLI_CFA_NO_PORT_MODE_OF,
  CLI_CFA_WAN_TYPE,
  CLI_CFA_NO_PORT_MODE,
  CLI_CFA_SET_PORT_VID,
  CLI_CFA_UNSET_PORT_VID,
  CLI_CFA_BRG_PORT_TYPE,
  CLI_CFA_IVR_PRIMARY_IP_ADDR,
  CLI_CFA_IVR_SECONDARY_ADDR,
  CLI_CFA_IVR_NO_IP_ADDR,
  CLI_CFA_IVR_NO_IP_ADDR_SUBNET_MASK,
  CLI_CFA_IVR_NO_IP_DHCP,
  CLI_CFA_IVR_IP_DHCP,
  CLI_CFA_NO_IVR_IP_ADDR,
  CLI_CFA_IVR_UNNUM_IP,
  CLI_CFA_IVR_NO_UNNUM_IP,
  CLI_CFA_SHOW_INT_ALL,
  CLI_CFA_SHOW_INT_COUNTERS,
  CLI_CFA_SHOW_L3VLAN_INT_COUNTERS,
  CLI_CFA_CLEAR_INT_COUNTERS,
  CLI_CFA_SHOW_FLOWCONTROL,
  CFA_CONF_TNL,
  CFA_DEL_TNL,
  CFA_ENA_TNL_CKSUM,
  CFA_NO_TNL_CKSUM,
  CFA_ENA_TNL_PMTU,
  CFA_NO_TNL_PMTU,
  CFA_SET_TNL_DIR,
  CFA_OOB_CONFIG,
  CLI_CFA_OOB_IP_ADDR,
  CLI_CFA_OOB_SEC_IP_ADDR,
  CLI_CFA_OOB_NO_SEC_IP_ADDR,
  CLI_CFA_OOB_IP_DHCP,
  CLI_CFA_OOB_NO_IP_DHCP,
  CLI_CFA_DESC,
  CLI_CFA_NO_DESC,
#ifdef PPP_WANTED
  CLI_CFA_STACK_PPP,
  CLI_CFA_UNSTACK_PPP,
  CLI_CFA_PPP_IP_ADDR_NEGO,
  CLI_CFA_PPP_IP_ADDR,
  CLI_CFA_PPP_SERVER_IP_ADDR,
  CLI_CFA_PPP_NO_IP_ADDR,
  CLI_CFA_PPP_UNNUM_IP,
  CLI_CFA_PPP_NO_UNNUM_IP,
#endif
#ifdef HDLC_WANTED
  CLI_CFA_SET_HDLC_CONTROLLER_ENABLE,
#endif
  CLI_CFA_OOB_INTERFACE_ADMIN_STATUS,
  CLI_CFA_LINUX_VLAN_INTERFACE_CREATE,
  CLI_CFA_TLV_CREATE,
  CLI_CFA_TLV_DELETE,
  CLI_CFA_OPQ_ATTRIB_CREATE,
  CLI_CFA_OPQ_ATTRIB_DELETE,
  CLI_CFA_CUSTOM_PARAM_DELETE,
  CLI_CFA_SYS_SPECIFIC_PORTID,
  CLI_CFA_SHOW_SYS_SPECIFIC_PORTID,
  CLI_CFA_SHOW_CUSTOM_PARAM,
  CLI_CFA_CONNECT,
  CLI_CFA_NO_CONNECT,
  CLI_CFA_SHOW_CONNECT,
  CLI_CFA_PORT_TYPE,
  CLI_CFA_VIP_INTERFACE_ADMIN_STATUS,
  CLI_CFA_SHOW_VIP_ADMIN_STATUS,
  CLI_CFA_ENABLE_BACKPLNE_STATUS,
  CLI_CFA_DISABLE_BACKPLNE_STATUS,
  CLI_CFA_ADD_IFTYPE_DENY_PROT_ENTRY,
  CLI_CFA_REM_IFTYPE_DENY_PROT_ENTRY,
  CLI_CFA_SHOW_IFTYPE_DENY_PROT,
  CLI_CFA_DEBUG,
  CLI_CFA_NO_DEBUGS,
  CLI_CFA_LOOPBACK_ENABLE,
  CLI_CFA_LOOPBACK_DISABLE, 
  CLI_CFA_INTERFACE_PORT_STATE,
  CLI_CFA_ADD_MAP_VLANS,
  CLI_CFA_REM_MAP_VLANS,
  CLI_CFA_OVERWRITE_MAP_VLANS,
  CLI_CFA_PURGE_ALL_MAPPED_VLANS,
  CLI_CFA_NETWORK_TYPE,
  CFA_TNL_HOP_LIMIT,
  CLI_CFA_SHOW_INT_HC_COUNTERS,
  CLI_CFA_PKT_RX_VALUE,
  CLI_CFA_PKT_RX_MASK,
  CLI_CFA_PKT_RX_NO_VALUE,
  CLI_CFA_PKT_RX_NO_MASK,
  CLI_CFA_PKT_RX_SHOW_ENTRY,
  CLI_CFA_PKT_RX_SHOW_TABLE,
  CLI_CFA_PKT_RX_PORT,
  CLI_CFA_PKT_TX_VALUE,
  CLI_CFA_PKT_TX_PORT_INT_CNT,
  CLI_CFA_PKT_TX_PORT_CNT,
  CLI_CFA_PKT_TX_PORT,
  CLI_CFA_PKT_TX_DISABLE,
  CLI_CFA_PKT_TX_SHOW_ENTRY,
  CLI_CFA_PKT_TX_SHOW_TABLE,
  CLI_CFA_SEC_VLAN_LIST,
  CLI_CFA_NO_SEC_VLAN_LIST,
  CLI_CFA_SEC_BRIDGE_STATUS,
  CLI_CFA_SET_SEC_INTF_VLAN,
  CLI_CFA_RESET_SEC_INTF_VLAN,
  CLI_CFA_SHOW_SEC_VLAN_LIST,
  CLI_CFA_SET_ALIAS,
  CLI_CFA_SHOW_SEC_INTF_VLAN,
  CLI_CFA_AC_IFACE,
  CLI_CFA_IVR_NO_PRIMARY_IP_ADDR_SUBNET_MASK,
  CLI_CFA_VLAN_INTF_COUNTERS,
  CLI_CFA_UFD_SYS_CTRL,
  CLI_CFA_UFD_STATUS,
  CLI_CFA_INTERFACE_ROLE,
  CLI_CFA_UFD_GROUP_CREATE,
  CLI_CFA_UFD_GROUP_DELETE,
  CLI_CFA_UFD_GROUP_PORT,
  CLI_CFA_UFD_GROUP_DESIGUPLINK_PORT,
  CLI_CFA_SHOW_UFD_GLOBAL,
  CLI_CFA_SHOW_PORT_ROLE,
  CLI_CFA_SHOW_UFD_STATS,
  CLI_CFA_SHOW_UFD_GROUP,
  CLI_CFA_SHOW_UFD_BRIEF,
  CLI_CFA_SHOW_UFD,
  CLI_CFA_SH_STATUS,
  CLI_CFA_SH_SYS_CTRL,
  CLI_CFA_SHOW_SH,
  CLI_CFA_LINKUP_GLOBAL_STATUS,
  CLI_CFA_LINKUP_PORT_STATUS,
  CLI_CFA_SET_LINKUP,
  CLI_CFA_NO_SET_LINKUP,
  CLI_CFA_SHOW_LINKUP,
  CLI_CFA_OOB_DESC,
  CLI_CFA_OOB_NO_DESC,
  CLI_CFA_DOT1Q_VLAN,
  CLI_CFA_NO_DOT1Q_VLAN,
  CLI_CFA_SUB_SESS_METHOD
};

enum
{
    CFA_TLV_CREATE = 1,
    CFA_TLV_DELETE
};
enum
{
    CFA_ADD_VIRTUAL= 1,
    CFA_DEL_VIRTUAL
};

#define CLI_CFA_MAX_ARGS 8 
#define CFA_DESCRIPTION 1
#define CFA_STORMCONTROL 2
#define CFA_FLOWCONTROL 3
#define CFA_ETHERCHANNEL 4
#define CFA_CAPABILITIES 5
#define CFA_STATUS 6
#define CFA_L3IP 7
#define CFA_BRG_PORT_TYPE 8
#define CFA_PORT_SEC_STATE 9 
#define CFA_SHOW_IVR_MAPPING_INFO 10
#define CFA_RATELIMIT 11

#define CFA_COUNTERS 1
#define CFA_BCAST_COUNTERS 2
#define CFA_ERR_COUNTERS 3
#define CFA_MCAST_COUNTERS 4
#define CFA_UCAST_COUNTERS 5

#define CFA_CLI_MAX_COMMANDS       29

#define CFA_CLI_MAX_CONFIG_OBJECTS  8
#define CFA_CLI_MAX_STATS_OBJECTS  13
#define CFA_CLI_MAX_ETH_ST_OBJECTS 20

#define CFA_CLI_MAX_IFXTYPE   14
#define CFA_CLI_MAX_IFXSUBTYPE 2

/* size of table header + maximum colunm width * maximum number of
  configurable objects in Cfa */
#define CFA_CLI_INT_CONFIG (CLI_MAX_HEADER_SIZE + \
                            CLI_MAX_COLUMN_WIDTH * \
                            CFA_CLI_MAX_CONFIG_OBJECTS)

/* size of table header + maximum colunm width * maximum number of
   interfaces in Cfa */
#define CFA_CLI_ALL_INT_CONFIG (CLI_MAX_HEADER_SIZE + \
                                CLI_MAX_COLUMN_WIDTH * \
                                CFA_MAX_INTERFACES())

/* size of table header + maximum colunm width * maximum number of
   interfaces in Cfa */
#define CFA_CLI_ALL_INT_STATS (CLI_MAX_HEADER_SIZE + \
                               CLI_MAX_COLUMN_WIDTH * \
                               CFA_CLI_MAX_STATS_OBJECTS)

#define CFA_CLI_MGMT_VLAN_LIST (CLI_MAX_HEADER_SIZE + \
                               CLI_MAX_COLUMN_WIDTH * \
                               100)

/* size of table header + maximum column width * maximum number of ethernet
 *    status objects per interface */
#define CFA_CLI_ETH_INT_STATS (CLI_MAX_HEADER_SIZE + \
                               CLI_MAX_COLUMN_WIDTH * \
                               CFA_CLI_MAX_ETH_ST_OBJECTS)


/* maximum length of atm address */
#define CFA_CLI_MAX_ATM_ADDR_LEN  32

/* maximum length of interface name */
#define CFA_CLI_INTF_NAME_LEN 32
#define CLI_INTF_MODE "INTF"
#define CLI_L3IFSUB_MODE "INTFSUB"
#define CLI_INTR_MODE "INTR"
#define CLI_INTOF_MODE "INTOF"
#define CLI_UAP_MODE   "uap"

/*!!! NOTE : THIS FUNCTION HAS TO BE PORTED DEPENDING ON THE TARGET TYPE !!!!*/
#define  STRTOL(s,d,n)  strtol  ((const char *) (s), (char **) (d), (int) (n))


#define CFA_UFD_MODE "ufd"

#define CFA_PPP_MODE    "ppp"

#define CFA_CLI_IPADDR_MAX_LENGTH      4

#ifdef HDLC_WANTED
#define CFA_HDLC_MODE                       "serial"
#endif


/*Error codes 
 * Error code values and strings are maintained inside the protocol module itself.
 */
enum {
    CFA_CLI_MAX_TLV_ERR = CLI_ERR_START_ID_CFA,
    CFA_CLI_TLV_ENTRY_ERR,
    CFA_CLI_TLV_LENGTH_ERR,
    CLI_CFA_PPPOE_MTU_ERR,
    CLI_CFA_PORT_IN_AGG,
    CLI_CFA_SIZE_MTU_ERR,
    CLI_CFA_LL_MTU_ERR,
    CLI_CFA_INVALID_INTERFACE_MAC_ERR,
    CLI_CFA_ADMIN_STATUS_MTU_ERR,
    CLI_CFA_ADMIN_STATUS_MACADDR_ERR,
    CLI_CFA_IFTYPE_MACADDR_ERR,
    CLI_CFA_DUPLICATE_MACADDR_ERR,
    CLI_CFA_ADMIN_STATUS_IPADDR_ERR,
    CLI_CFA_IPADDR_ALLOC_ERR,
    CLI_CFA_ZERO_IP_ERR,
    CLI_CFA_SUBNET_ERR,
    CFA_CLI_IFM_CONFIG_ERR,
    CFA_CLI_DEFAULT_PORT,
    CFA_CLI_DELETE_PORT,
    CFA_CLI_DEL_PORTCHANNEL,
    CFA_CLI_PORT_MAPPED_ERR,
    CFA_CLI_NO_PRIMARY_IP_ERR, 
    CFA_CLI_PRIMARY_IP_DEL_ERR,
    CFA_CLI_ALLOC_METHOD_ERR,
    CFA_CLI_MAX_SEC_ERR,
    CFA_CLI_SAME_IP_ERR,
    CLI_CFA_BRG_PORT_TYPE_PC_ERR,
    CLI_CFA_BRG_PORT_TYPE_IN_PC_ERR,
    CLI_CFA_BRG_PORT_TYPE_PORT_ERR,
    CLI_CFA_BRG_PORT_TYPE_MAP_ERR,
    CLI_CFA_BRG_PORT_TYPE_MODE_ERR,
    CLI_CFA_ACCP_FRAME_TYPE_ERR,
    CLI_CFA_BRG_PORT_TYPE_VIRTUAL_PORT_ERR,  
    CLI_CFA_IP_BRIDGE_ERR,
    CLI_CFA_WRONG_SUBNET_ERR,
    CLI_CFA_VLAN_BASE_BRIDGE_ENABLED,
    CLI_CFA_LOOPBACK_MTU_ERR,
    CLI_CFA_LO_ALLOC_METHOD_ERR,
    CLI_CFA_LO_ALLOCPROT_ERR,
    CLI_CFA_INVALID_SUBNET,
    CLI_CFA_INVALID_IPADDR,
    CFA_CLI_RTR_PORT_MAPPED_ERR,
    CFA_CLI_RTR_PORT_AGG_MISMATCH_ERR,
    CLI_CFA_BACKPLANE_ERR,
    CLI_CFA_SISP_PORT_ERR,
    CLI_CFA_INVALID_IFTYPE_DENY,
    CLI_CFA_MIRR_ENABLED,
    CLI_CFA_RESV_VLAN_ERR,
    CLI_CFA_TRC_LEVEL_ERR,
    CLI_CFA_PORT_IN_PORTCHANNEL,
    CLI_CFA_LOOPBACK_LOCAL_ERR,
    CLI_CFA_NO_LOOPBACK_LOCAL_ERR,
    CLI_CFA_IVR_VLAN_ALREADY_EXISTS_ERR,
    CLI_CFA_IVR_VLAN_NOT_FOUND_ERR,
    CLI_CFA_IVR_VLAN_SEC_VLAN_ERR,
    CLI_CFA_ADMIN_STATUS_ERR,
    CLI_CFA_BRIDGED_IFACE_WAN_ERR,
    CLI_CFA_LAN_ERR,
    CLI_CFA_PPP_NOT_STACKED_ERR,
    CLI_CFA_PPP_ALREADY_STACKED_ERR,
    CLI_CFA_PPP_LL_ALREADY_STACKED_ERR,
    CLI_CFA_PPP_ADMIN_STATUS_ERR,
    CLI_CFA_PPP_LL_UP_ERR,
    CLI_CFA_PPP_LL_SWITCH_PORT_ERR,
    CLI_CFA_INVALID_INDEX,
    CLI_CFA_INVALID_DESCRIPTION,
    CLI_CFA_LOOPBACK_IPADDR,
    CLI_CFA_LOOPBACK_ERR,
    CLI_CFA_UNASSIGNED_IPADDR_ERR,
    CLI_CFA_INVALID_IP_UNNUM_PEER_MAC,
    CLI_CFA_INVALID_IP_UNNUM_ASSOC_IF,
    CLI_CFA_DEL_UNNUMINTERFACE,
    CLI_CFA_INVALID_AC_PORT_TYPE,
    CLI_CFA_INVALID_TUNNEL_CONF,
    CLI_CFA_INVALID_TUNNEL_ENTRY,
    CLI_CFA_MPLS_INTERFACE_STACK_ERR,
    CLI_CFA_OPENFLOW_INTERFACE_MAP_ERR,
    CLI_CFA_NO_ARP_TUNNEL_ERR,
    CLI_CFA_ADMIN_STATUS_SET_ERR,
    CLI_CFA_NO_DEL_VRRP_TRACK_INUSE,
    CLI_CFA_IP_NO_CHG_VRRP_INUSE,
    CLI_CFA_ALIAS_ERR,
    CLI_CFA_PORT_ICCL_VLAN_ERR,
    CLI_CFA_PORT_VLAN_IN_USE_VLAN,
    CLI_CFA_PORT_VLAN_USED_ERR,
    CLI_CFA_RSVD_VLAN_EXHAUST_ERR,
    CLI_CFA_ADMIN_STATUS_PORT_VID_ERR,
    CLI_CFA_INTERFACE_STACK_ERR,
    CLI_CFA_ICCH_NO_PNP_PORT_ERR,
    CLI_CFA_UFD_GROUP_MAX_LIMIT_REACHED_ERR,
    CLI_CFA_UFD_PORT_ADD_ERR,
    CLI_CFA_UFD_PORT_DEL_ERR,
    CLI_CFA_PVLAN_TYPE_ERR,
    CLI_CFA_UFD_SYS_CONTROL_ERR,
    CLI_CFA_UFD_MODULE_STATUS_ERR,
    CLI_CFA_SH_SYS_CONTROL_ERR,
    CLI_CFA_UFD_ERR_DISABLED_ERR,
    CLI_CFA_UFD_PORT_NOT_PRESENT_ERR,
    CLI_CFA_UFD_GROUP_MAX_PORT_LIMIT_ERR,
    CLI_CFA_UFD_GROUP_ZERO_DOWNLINK_ERR,
    CLI_CFA_UFD_GROUP_PORTCHANNEL_PORT_ERR,
    CLI_CFA_UFD_LAST_DOWNLINK_REM_ERR,
    CLI_CFA_UFD_PORT_ANOTHER_GROUP_MEMBER,
    CLI_CFA_UFD_ROUTER_PORT_ERR,
    CLI_CFA_ICCL_ADMIN_STATUS_SET_ERR,
    CLI_CFA_UFD_LAST_DOWNLINK_RPORT_ERR,
    CLI_CFA_LINKUP_DELAY_SYSTEM_STATUS_ERR,
    CLI_CFA_LINKUP_DELAY_SYSTEM_SET_ERR,
    CLI_CFA_LINKUP_DELAY_PORT_STATUS_ERR,
    CLI_CFA_LINKUP_DELAY_TIMER_ERR,
    CLI_CFA_LINKUP_ROUTER_PORT_ERR,
    CLI_CFA_LINKUP_PORT_CHANNEL_ERR,
    CLI_CFA_SCHANNEL_CREATE_NOT_ALLOWED,
    CLI_CFA_EVB_NOT_RUNNING_ERR,
    CLI_CFA_PORT_ICCL_INT_NO_DEL_ERR,
    CLI_CFA_PORT_ICCL_IVR_NO_DEL_ERR,
    CLI_CFA_PROXYARP_NOT_DELETED_ERR,
    CFA_CLI_PW_MAPPED_PWIF_ERR,
    CFA_CLI_PWIF_APP_ERPS,
    CLI_CFA_IP_NEXT_ICCL,
    CLI_CFA_SBP_ERR,
    CLI_CFA_L3SUB_IF_PRESENT,
    CLI_CFA_VLAN_OVERLAPS,
    CLI_CFA_ENCAP_ERR,
    CLI_CFA_L3SUB_IF_ERR,
    CLI_CFA_L3SUB_LAG_IF_ERR,
    CLI_CFA_SUB_SESS_ERR,
    CLI_CFA_SESS_ERR, 
    CLI_CFA_SUB_INT_TYPE_ERR,
    CLI_CFA_MAX_ERR
};

/* Accessing the ErrString with ErrCode value.
 * Since the ErrCode's starting Id is changed, this forumla is used
 * to access the Err String */
#define CLI_ERR_OFFSET_CFA(u4ErrCode)   (u4ErrCode - CLI_ERR_START_ID_CFA + 1)

#ifdef __CFACLI_C__
CONST CHR1  *CfaCliErrString [] = {
    NULL,
    "% Maximum TLVs reached for this port\r\n",
    "% TLV Entry is already present\r\n",
    "% Length of value and length field should be equal\r\n",
    "% MTU cannot exceed 1492 for PPP over Ethernet links\r\n",
    "% Ip Capabilities are disabled ,port is a part of Port-channel.\r\n",
    "% Invalid MTU value for this interface type\r\n",
    "% MTU cannot exceed that of the underlying physical interface \r\n",
    "% Interface must be valid to set mac address\r\n",
    "% Interface must first be made administratively down before setting MTU \r\n",
    "% Interface must first be made administratively down before setting Mac address \r\n",
    "% Mac address can be set only of ethernet ports and on port-channels.\r\n",
    "% Mac address already assigned to an interface.\r\n",
    "% Interface must first be made administratively down before setting IP Address \r\n",
    "% Address allocation method must be manual to configure IP Address \r\n",
    "% Invalid Subnet \r\n",
    "% This IP Address overlaps with an existing Interface IP Address \r\n",
    "% Failed to Update IP \r\n",
    "% Cannot create or delete default/management interface\r\n",
    "% Cannot delete the physical interface\r\n",
    "% Cannot delete the portchannel interface\r\n",
    "% Cannot delete port as it is mapped to a Virtual Switch\r\n",
    "% Primary Address should be configured before configuring secondary\r\n",
    "% Must delete secondary before deleting primary address\r\n",
    "% Allocation method must be  manual \r\n",
    "% Maximum secondary addresses allowed on interface exceeded \r\n",
    "% Same IP address exists on the interface \r\n",
    "% Bridge port type cannot be set for a port-channel if some ports are aggregated in it.\r\n",
    "% Bridge port type cannot be set for a port that is part of port-channel.\r\n",
    "% Bridge port type can be set only of ethernet ports and on port-channels.\r\n",
    "% Bridge port type cannot be set for a port that is not mapped to any virtual context.\r\n",
    "% Bridge port type invalid for this bridge mode.\r\n",
    "% Acceptable Frame Type should be Untagged & Priority Tagged before configuring Bridge port type as CNP (Port Based) for a port.\r\n",
    "% Bridge port type invalid for this port.\r\n",
    "% Cannot set IP address for an Bridged interface. \r\n",
    "% Subnet mask should be 32 bit for loopback interface \r\n",
    "% Cannot Create Interface Vlan in Transparent Mode\r\n",
    "% Cannot set MTU for Loop back interface \r\n",
    "% Cannot set ip address allocation method for Loop back interface \r\n",
    "% Cannot set ip address allocation protocol for Loop back interface \r\n",
    "% Invalid SubnetMask For the Given Ipaddress \r\n",
    "% Invalid IpAddress and SubnetMask\r\n",
    "% Unmap the port from switch before setting the port as a rtr port.\r\n",
    "% Port-Channel is not a router port. Unmap/remove the port from LAG before setting the port as a rtr port\r\n",
    "% Back Plane status can be set only for Physical interfaces\r\n",
    "% Cannot delete SISP port when mapped to a physical interface\r\n",
    "% Protocol Deny table cannot be configured for this IfType \r\n",
    "% Cannot Delete, Mirroring is enabled on the Port-Channel \r\n",
    "% VLAN ID should be in the range 1 to (Max.Vlans-Max.Router Ports)\r\n",
    "% Invalid trace level value. \r\n",
    "% Cannot set MTU when port is member of a port-channel\r\n",
    "% Cannot configure loopback when port is administratively down \r\n",
    "% Cannot disable loopback when loopback is not configured \r\n", 
    "% Secondary VLAN is already associated with an VLAN interface \r\n", 
    "% Secondary VLAN does not exists in the Table \r\n", 
    "% VLAN Interface for secondary VLAN is not allowed \r\n", 
    "% Interface must first be made administratively down \r\n",
    "% Ethernet interface must be a router port to set network type\r\n",
    "% Interface network type must first be set as WAN \r\n",
    "% PPP Interface is not yet layered with an underlying physical interface\r\n", 
    "% PPP virtual link is already layered over another physical interface\r\n", 
    "% Physical Interface is already layered below another PPP virtual link\r\n",
    "% PPP interface must first be made administratively down\r\n",
    "% Ethernet interface must first be administratively disabled\r\n",
    "% Ethernet interface must first be made a router port before setting PPPoE encapsulation\r\n",
    "% Invalid interface index\r\n",
    "% Description length exceeded\r\n",
    "% 127.0.0.1-Loopback address is used by sli module \r\n",
    "% 127.0.0.0/8 reserved for loopback interfaces \r\n",
    "% Cannot Delete: IP address not assigned or already deleted\r\n",
    "% Cannot set Destination MAC for unnumbered interface \r\n",
    "% Invalid interface for associating an IP Interface \r\n",
    "% Cannot delete the interface as it is mapped to unnumbered interface \r\n",
    "% AC interface can only be CEP or CNP (port-base) \r\n",
    "% Maximum 2 tunnels are allowed for a set of end-point addresses \r\n",
    "% Invalid tunnel If Entry \r\n",
    "% Unable to change the port mode. MPLS Interface is stacked over the interface \r\n", 
    "% Unable to change the port from Openflow Domain. Interface mapping exists \r\n",
    "% Cannot set admin up for automatic tunnels(6to4, isatap, compat, openflow), no ARP entry is available on tunnel interface \r\n",
    "% Unable to set Admin status \r\n",
    "% Cannot delete this interface. VRRP Link Tracking feature is in use \r\n",
    "% Cannot Add / Delete / Modify IP. VRRP is using it and this affects its working \r\n",
    "% The string should be of the form po<KEY> where KEY is the port-channel id used as ActorAdminKey \r\n",
    "% Configurations are not permitted on ICCL L3 IPVLAN \r\n",
    "% VLAN already in use\r\n",
    "% VLAN already in use for Router Port \r\n",
    "% Reserved VLAN range exhausted. Use port-vid to configure a VLAN to be associated with this port.\r\n",
    "% Interface must first be made administratively down before setting port-vid \r\n",
    "% Unable to destroy interface, as stacking configured over the interface \r\n",
    "% ICCL interface should be always Provider Network Port \r\n",
    "% Max number of UFD groups is already reached !!! \r\n",
    "% Addition of port in UFD group is failed \r\n",
    "% Deletion of port from UFD group is failed \r\n",
    "% Configuring secondary vlans as IVR interface is not allowed\r\n",
    "% UFD is not Started \r\n",
    "% UFD is not Enabled \r\n",
    "% Split-Horizon is not Enabled \r\n",
    "% Port is in UFD error disabled state, can not change the admin status \r\n",
    "% Some or all port(s) are not present in this group \r\n",
    "% Max portlimit for the UFD group reached\r\n",
    "% No downlink port is present in group, Can not add the uplink port\r\n",
    "% Interface is present in port channel, Can not be added to UFD group\r\n",
    "% Cannot delete all/last downlink port(s), Uplink port(s) is present in the UFD group\r\n",
    "% Port is already present in another UFD group, cannot be added in this group\r\n",
    "% Router port cannot be added to UFD group \r\n",
    "% Configuring Admin status as down for ICCL interface is not permitted\r\n",
    "% Cannot change last downlink port as router port, Uplink port(s) is present in the UFD group\r\n",
    "% LinkUp Delay is not enabled in the system\r\n", 
    "% Invalid LinkUp Delay status\r\n", 
    "% LinkUp Delay is not enabled  in the interface\r\n", 
    "% Invalid Timer Value for LinkUp Delay\r\n",
    "% LinkUp Delay cannot be set for router port\r\n", 
    "% LinkUp Delay cannot be set for port channel\r\n", 
    "% S-Channel creation is not allowed through ifMainTable\r\n",
    "% Port type cannot be set as UAP when EVB is not started\r\n",
    "% This configuration is not applicable for S-channel interface\r\n",
    "% Cannot delete ICCL port-channel interface\r\n",
    "% Cannot delete ICCL IVR interface\r\n",
    "% Proxy_Arp Admin Status is not Released Properly\r\n",
    "% Cannnot delete pw-interface as pw is mapped to it\r\n",
    "% Configuring Admin Status as down for pw-interface owned by ERPS is not permitted\r\n",
    "% The IP address overlaps with ICCL IP address configured in the system\r\n",
    "% Cannot delete this interface. L3subinterface is associated with this Port.\r\n",
    "% Encapsulation VLAN overlaps with another existing L3Subinterface.\r\n",
    "% Encapsulation VLAN configured already.\r\n",
    "% L3Subinterface is associated with this port.\r\n",
    "% L3SubInterface is associated with this port-channel.\r\n",
    "% Subscribers are associated with the interface, modification is not possible.\r\n",
    "% Cannot set session initiation method in the interface.\r\n",
    "% This configuration is applicable only for physical and LAG interfaces.\r\n",
    "% FATAL ERROR : Command Failed \r\n"
};
#else
extern CONST CHR1  *CfaCliErrString [];
#endif

#ifndef __CFACLIPROT_H__
#define __CFACLIPROT_H__ 
INT4
CfaIvrDeleteIpAddressAndSubnetMask (tCliHandle CliHandle, UINT4 u4IfIndex,
                                    UINT4 u4IpAddr, UINT4 u4IpSubnetMaskEntered);

INT4
CfaShowInterfacesHcCounters (tCliHandle CliHandle, INT4 i4Index);


INT4
CfaSetInterfaceAdminStatus PROTO (( tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4AdminStatus));

INT4
CfaSetLinkStatusTrap PROTO ((tCliHandle, UINT4 ,  UINT4));

extern INT1 nmhGetDot3adAggActorAdminKey (INT4 ,INT4 *);

INT4        
CfaSetInterfaceMtu PROTO ((tCliHandle , UINT4 , UINT4 ));

INT4        
CfaSetInterfaceAlias PROTO ((tCliHandle , UINT4 , UINT1 * ));

INT4 
CfaSetInterfaceExtMacAddr (tCliHandle , UINT4 , tMacAddr );
INT4
CfaSetInterfaceDesc(tCliHandle, UINT4 , UINT1  *);

INT4 
CfaDeleteInterface PROTO ((tCliHandle , UINT4 ));

INT4
CfaCreateVlanInterface PROTO  ((tCliHandle , UINT4 , UINT1 *, UINT4));

INT4
CfaCreateL3SubInterface PROTO  ((tCliHandle , UINT4 , UINT1 *, UINT4));

INT4
CfaCreateLoopbackInterface PROTO  ((tCliHandle , UINT4 , UINT1 *));

INT4
CfaCreatePortChannelInterface PROTO ((tCliHandle , UINT4 , UINT1 *));

INT4
CfaCreateInternalInterface PROTO ((tCliHandle , UINT4 , UINT1 *));

INT4
CfaCreateILanInterface PROTO ((tCliHandle , UINT4 , UINT1 *));

INT4
CfaCreateEthInterface PROTO ((tCliHandle , UINT4 , UINT1 *));
    
INT4
CfaCreateInterface PROTO ((tCliHandle , UINT4 , UINT4 , UINT1 *, UINT4, UINT4));

INT4
 CfaCreatePhysicalInterface (UINT1*,UINT1* ,UINT4*);
 

INT4
CfaSetFlowcontrol (tCliHandle , UINT4 , INT4);

INT4
CfaSetPortMode(tCliHandle, UINT4 ,INT4);

INT4
CfaCliSetPortVlanId (tCliHandle , UINT4, INT4 );
 
INT4
CfaCliUnSetPortVlanId (tCliHandle , UINT4);

INT4 
CfaSetOpenflowInterface (tCliHandle, UINT4, UINT4);

INT4 
CfaSetBrgPortType (tCliHandle, UINT4, INT4);

INT4
CfaConnectPorts (tCliHandle, UINT4, INT1, tPortList);

INT4
CfaDeleteInternalLan (tCliHandle, UINT4);

INT4
CfaShowVipAdminStatus (tCliHandle CliHandle, UINT4 u4Isid, UINT4 u4ContextId,UINT1* pu1Switch);

INT4 CfaCliSetDebugLevel (tCliHandle CliHandle, INT4 i4CliDebugLevel);

INT4
CfaIvrSetIpAddress (tCliHandle, UINT4, UINT4 ,  UINT4);

INT4
CfaSetOOBSecondaryIpAddress (tCliHandle, UINT4, UINT4 , UINT4);

INT4 
CfaSetSecurityBridgeMode (tCliHandle CliHandle,INT4 i4SecStatus);


INT4
CfaSetMgmtVlanList PROTO ((tCliHandle , UINT1 * , UINT1));

INT4
CfaSetSecVlanList (tCliHandle CliHandle, UINT1 *pu1IvrSetVlanList,
                    UINT1 u1Flag);

INT4
CfaSetSecIntfVlan(tCliHandle CliHandle,UINT4 u4IfIndex);

INT4
CfaShowInterfaceMtu PROTO ((tCliHandle , INT4 , UINT1 *));

INT4
CfaShowInterfaceMtuInfo PROTO ((tCliHandle , INT4));

VOID
CfaGetInterfaceBrgPortTypeString (INT4, UINT1 *);

INT4
CfaShowInterfaceBrgPortType (tCliHandle, INT4);

INT4
CfaAddIfTypeDenyProtocolEntry (tCliHandle CliHandle,
                               INT4 i4ContextId, INT4 i4ifType,
                               INT4 i4BrgPortType, INT4 i4Protocol);
INT4
CfaRemIfTypeDenyProtocolEntry (tCliHandle CliHandle,
                               INT4 i4ContextId, INT4 i4ifType,
                               INT4 i4BrgPortType, INT4 i4Protocol);
INT4
CfaCliShowIfTypeDenyProtoTable (tCliHandle CliHandle, UINT4 u4ContextId);

#ifdef ISS_WANTED
INT4
CfaShowInterfacesStorm PROTO ((tCliHandle CliHandle , INT4 i4NextIndex));
INT4
CfaShowInterfacesRateLimit PROTO ((tCliHandle CliHandle , INT4 i4NextIndex));

#endif /*ISS_WANTED*/
INT4
CfaShowInterfacesCapabilities PROTO ((tCliHandle CliHandle , INT4 i4NextIndex));

INT4
CfaShowInterfacesStatus PROTO ((tCliHandle CliHandle , INT4 i4NextIndex));

INT4
CfaShowInterfacesFlow PROTO ((tCliHandle CliHandle , INT4 i4NextIndex));

INT4
CfaShowInterfacesAll PROTO ((tCliHandle CliHandle , INT4 i4NextIndex));

INT4        
CfaShowInterfaces PROTO ((tCliHandle CliHandle, INT4 i4NextIndex , 
                          UINT4 u4Command, UINT1 *pu1Alias));
    
INT1
CfaShowIface PROTO ((tCliHandle CliHandle, INT4 i4Index, UINT4 u4Command));

INT4
CfaShowInterfacesCounters PROTO ((tCliHandle CliHandle, INT4 i4IfIndex, 
                                  UINT1 *pu1Alias));
INT4
CfaShowL3VlanInterfacesCounters PROTO ((tCliHandle CliHandle, INT4 i4IfIndex, 
                                  UINT1 *pu1Alias));
INT4
CfaShowIfacesInCounters PROTO ((tCliHandle CliHandle, INT4 i4Index, 
                                UINT1 *pu1Alias));
INT4
CfaShowL3VlanIfacesInCounters PROTO ((tCliHandle CliHandle, INT4 i4Index, 
                                UINT1 *pu1Alias));

INT4
CfaShowIfacesOutCounters PROTO ((tCliHandle CliHandle, INT4 i4Index, 
                                 UINT1 *pu1Alias));
INT4
CfaShowInterfaceInCounters PROTO ((tCliHandle CliHandle, INT4 i4Index));

INT4
CfaShowL3VlanInterfaceInCounters PROTO ((tCliHandle CliHandle, INT4 i4Index));

INT4
CfaShowInterfaceOutCounters PROTO ((tCliHandle CliHandle, INT4 i4Index));
    
INT4
CfaShowInterfacesDesc PROTO ((tCliHandle CliHandle , INT4 i4NextIndex));

INT4
CfaClearInterfacesCounters PROTO ((tCliHandle CliHandle, INT4 i4IfIndex));

INT4  CfaLinkUpDelayStatusOnPort (tCliHandle CliHandle,UINT4 u4IfaceIndex,UINT4 u4Status);

INT4  CfaLinkUpDelaySetTimerOnPort  (tCliHandle CliHandle,UINT4 u4IfaceIndex,UINT4 u4Timer);

INT4  CfaCliSetLinkUpDelayOnSystem(tCliHandle CliHandle,UINT4 u4Status);

INT4 CfaCliShowLinkUpDelay (tCliHandle CliHandle,INT4 i4NextIndex);

INT4 cli_get_current_ifc_index (VOID);

INT4 cli_get_index_from_ifc_name (UINT1 *);

#endif /* __CFACLIPROT_H__ */
INT4 cli_process_interface_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command, ...));
INT4 CfaGetSlotAndPortNum (INT1 *pi1IfNum, INT4 *pi4SlotNum, INT4 *pi4PortNum);
 

INT4
CfaValidateIfNum (UINT4 u4IfType, INT1 *pi1IfNum, INT4 *pi4SlotNum,
                  UINT4 *pu4RetIfNum);


INT4 CfaShowMgmtVlanList PROTO ((tCliHandle CliHandle));
INT4 CfaShowSecVlanList PROTO ((tCliHandle CliHandle));

VOID CfaShowSecIntfVlan (tCliHandle CliHandle);
INT4
CfaShowIpInt PROTO ((tCliHandle CliHandle, UINT4 u4Index, UINT4 u4ContextId,
                     UINT1 *pu1Alias));
INT4
CfaShowIpIfaceInCxt PROTO ((tCliHandle CliHandle, UINT4 u4ContextId, 
                            UINT4 u4IfIndex, UINT1 *pu1Alias));

INT4
CfaShowIpIfaceInfo PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex));


INT4  InterfaceShowRunningConfigDetails(tCliHandle CliHandle, INT4, UINT4, UINT1 *);


INT4
CfaIvrSetAddressAlloc (tCliHandle CliHandle,UINT4 u4IfIndex,
                       UINT1 u1AddrAllocMethod, UINT1 u1AddrAllocProto);

INT4
CfaConfTnlParams PROTO ((tCliHandle , UINT4 , UINT4 , tTnlIpAddr *,tTnlIpAddr *,                           UINT4, UINT4));
INT4
CfaDelTnlParams PROTO ((tCliHandle , UINT4 , UINT4 , tTnlIpAddr *,tTnlIpAddr *,                           UINT4, UINT4));
INT4        
CfaSetTnlCkSumFlag PROTO ((tCliHandle , UINT4 , INT4 ));

INT4        
CfaSetTnlPmtuFlag PROTO ((tCliHandle , UINT4 , INT4, INT4 ));

INT4        
CfaSetTnlDir PROTO ((tCliHandle , UINT4 , UINT1, UINT1));

INT4
CfaSetTnlHopLimit PROTO ((tCliHandle , UINT4 , INT4 ));

INT4
CfaCliCreateLogicalInterface PROTO ((tCliHandle, UINT4 , UINT1 *));

INT4
CfaIvrSetSecondaryIpAddress PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex, 
                               UINT4 u4IpAddr, UINT4 u4IpSubnetMask));
INT4
CfaIvrDeleteIpAddress PROTO ((tCliHandle CliHandle,UINT4 u4IfIndex,
                                                   UINT4 u4IpAddr));


INT4
CfaCliSetSysSpecificPortID PROTO ((tCliHandle CliHandle, INT4 i4IfaceIndex, UINT4 u4PortID));

INT4
CfaShowSysSpecificPortID PROTO ((tCliHandle CliHandle));

INT4
CfaCliSetOpqArribute PROTO ((tCliHandle CliHandle, INT4 i4IfaceIndex, INT4 u4OpqAttrID, UINT4 u4OpqAttrValue));

INT4
CfaCliDeleteOpqArribute PROTO ((tCliHandle CliHandle, INT4 i4IfaceIndex, INT4 u4OpqAttrID));

INT4
CfaShowCustomParam PROTO ((tCliHandle CliHandle));

INT4
CfaShowConnectParam PROTO ((tCliHandle CliHandle, tPortList IfPortList));

INT4
CfaCliDelAllCustomParams PROTO ((tCliHandle CliHandle, INT4 i4IfaceIndex));

INT4
CfaTestIfIpAddrAndIfIpSubnetMask PROTO ((INT4,UINT4,UINT4));

INT4 CfaSetCustTLV (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4TLVType,UINT4 u4TLVLength, UINT1 *, UINT4 u4Flag);

PUBLIC INT4
CfaSetDebug (tCliHandle CliHandle, UINT4 u4TraceInput, UINT1 u1Action);


INT4 CfaCreateSispInterface (tCliHandle CliHandle, UINT4 u4IfIndex, 
          UINT1 *pu1IfName);

INT4 CfaCliCreatePswInterface PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex,
          UINT1 *pu1IfName));

INT4 CfaCliCreateACInterface PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex,
          UINT1 *pu1IfName));

#ifdef VXLAN_WANTED
INT4 CfaCliCreateNveInterface PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex,
                                                UINT1 *pu1IfName));
#endif
#ifdef HDLC_WANTED
INT4 CfaCliCreateHdlcInterface PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex,
                                                UINT1 *pu1IfName));
INT4 CfaCliSetHdlcControllerEnable PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex));
#endif
#ifdef VCPEMGR_WANTED
INT4 CfaCliCreateTapInterface PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex,
                                                UINT1 *pu1IfName));
#endif
INT4
CfaSetBackPlaneStatus (UINT4 u4IfIndex, INT4 i4Status);
INT4
CfaIvrSetPeerMacAddress (tCliHandle CliHandle, UINT4 u4IfIndex, tMacAddr PeerMacAddr);

INT4
CfaUnnumIntSetAssociatedIPInterface (tCliHandle CliHandle, UINT4 u4IfIndex,
                        UINT4 u4AliasIfIndex );
INT4
CfaUnnumIntResetAssociatedIPInterface (tCliHandle CliHandle, UINT4 u4IfIndex,
                             tMacAddr PeermacAddr, UINT4 u4AssocAddr );
#define CLI_OOB_CONFIG "(config-if)#"
#define CLI_INT_RANGE "(config-if-range)#"

INT1      CfaConfigOOBInterface(INT1 *pi1ModeName, INT1 *pi1DispStr);

extern INT1  nmhTestv2FsIpPmtuEntryAge (UINT4 *, INT4);
extern INT4 FsIpPmtuEntryAgeSet (tSnmpIndex *, tRetVal *);

extern INT1 nmhGetFsIpifDrtBcastFwdingEnable(INT4, INT4 *);
/* Proto type for GET_NEXT Routine.  */

UINT4
CfaCliValidatePhysicalPort (tCliHandle CliHandle,INT4 i4PhyPort,INT4 i4SlotNum);



INT4 CfaCliGetIfIndexInCxt PROTO ((UINT1 *pu1L2SwitchName,
                           INT1 *pi1IfName, INT1 *pi1IfNum, UINT4 *pu4IfIndex));
INT4 CfaGetOuterAndInnerVlanIDFromIfName PROTO ((INT1 *pi1IfName, UINT2 *pu2Vlan, UINT2 *pu2InnerVlan));
INT4 CfaCliGetPoIndex PROTO ((INT1 *pi1IfName, INT1 *pi1IfNum, UINT4 *pu4IfIndex));
INT4 CfaCliGetPswIndex PROTO ((INT1 *pi1IfName, INT1 *pi1IfNum, UINT4 *pu4IfIndex));
INT4 CfaCliGetACIndex PROTO ((INT1 *pi1IfName, INT1 *pi1IfNum, UINT4 *pu4IfIndex));
INT4 CfaCliGetVirtualIndex PROTO ((INT1 *pi1IfName, INT1 *pi1IfNum, UINT4 *pu4IfIndex));
#ifdef VXLAN_WANTED
INT4 CfaCliGetNveIndex PROTO ((INT1 *pi1IfName, INT1 *pi1IfNum, UINT4 *pu4IfIndex));
#endif
#ifdef VCPEMGR_WANTED
INT4 CfaCliGetTapIndex PROTO ((INT1 *pi1IfName, INT1 *pi1IfNum, UINT4 *pu4IfIndex));
#endif
extern INT4 IpShowRunningConfigInterfaceDetails PROTO ((tCliHandle CliHandle, 
                                                        INT4 i4Index));

extern VOID RstpShowRunningConfigInterfaceDetails(tCliHandle,INT4,UINT1 *pu1Flag);
extern VOID ElmShowRunningConfigInterfaceDetails(tCliHandle,INT4);
extern VOID MstpShowRunningConfigInterfaceDetails(tCliHandle,INT4,UINT1 *pu1Flag);
extern VOID MstpShowRunningConfigInstInterface(tCliHandle,INT4,UINT1 *pu1Flag);
extern VOID PvrstShowRunningConfigInstInterface(tCliHandle,INT4,UINT1* pu1Flag);
extern VOID PvrstShowRunningConfigInterfaceDetails(tCliHandle,INT4,UINT1* pu1Flag);
extern INT4 DhrlShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4,INT4);
extern INT4 RipShowRunningConfigInterfaceDetails(tCliHandle CliHandle, UINT4 u4RipIfAddr, INT4 i4CxtId);
extern VOID AclShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4Index);
extern INT4 Rip6ShowRunningConfigInterfaceDetails (tCliHandle,INT4,UINT4);
extern INT4 IgmpShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4Index);
extern INT4 MldShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4Index);
extern INT4 IgmpProxyShowRunningConfigUpIfaceDetails (tCliHandle CliHandle, INT4 i4Index);
extern INT4 SnoopShowRunningConfigInterfaceDetails (tCliHandle, INT4, INT4);

#ifdef NPAPI_WANTED
#ifdef SWC
extern INT4 DiffservShowRunningConfigInterfaceDetails (tCliHandle, INT4);
#endif
#endif
#ifdef POE_WANTED
extern INT4 PoeShowRunningConfigInterfaceDetails (tCliHandle, INT4);
#endif

#endif
extern INT1 nmhGetFsMIStdIpProxyArpAdminStatus (INT4 ,INT4 *);

PUBLIC INT4
CfaSetPortSecState PROTO (( tCliHandle CliHandle, UINT4 u4IfIndex,
                            UINT4 u4PortSecState));
PUBLIC INT4 CfaCliShowPortSecState PROTO ((tCliHandle CliHandle, INT4 i4IfIndex)); 
INT4 CfaCliSetWanType (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4WanType);
INT4 CfaPppSetAddressAlloc (tCliHandle CliHandle, UINT4 u4IfIndex,
                                                        UINT1 u1AddrAllocMethod);
INT4 CfaCliSetPppNoIpAddr (tCliHandle CliHandle,UINT4 u4IfIndex);
INT4 CfaPppUnnumIntSetAssociatedIPInterface (tCliHandle CliHandle, UINT4 u4IfIndex,
                                     UINT4 u4AliasIfIndex);

INT4 CfaPppUnnumIntResetAssociatedIPInterface (tCliHandle CliHandle, UINT4 u4IfIndex,
                                          UINT4 u4AssocAddr);





#ifdef PPP_WANTED
extern INT1 nmhSetPPPoEBindingsEnabled (INT4, INT4);
extern INT1 nmhSetPPPoEHostUniqueEnabled (INT4);
extern INT1 nmhSetPPPoEMode (INT4);
extern INT1 nmhSetPppExtLinkConfigHostName (INT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhSetPPPoEACName (tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhSetPppExtLinkConfigLowerIfType (INT4, INT4);

extern INT1 nmhGetPppExtKeepAliveTimeOut(INT4 ,INT4 *);
extern INT4 PppIsIfaceBCPStatusOpen(UINT4);
extern INT4 PppIsIfaceIPCPStatusOpen(UINT4);
extern INT4 PppIsIfaceLCPStatusOpen(UINT4);
extern INT4 PppGetNegAuthProtocol(UINT4, INT4 *, INT4 *);

extern INT1 nmhGetPppExtIpLocToRemoteAddress (INT4, UINT4 *);
extern INT1 nmhGetPppExtIpRemoteToLocAddress (INT4, UINT4 *);
extern INT1 nmhGetPppExtIpRemoteToLocPrimaryDNSAddress (INT4, UINT4 *);
extern INT1 nmhGetPppExtLinkConfigHostName (INT4, tSNMP_OCTET_STRING_TYPE *) ;
extern INT1 nmhGetNextIndexPppSecuritySecretsTable (INT4, INT4 *,INT4, INT4 *);
extern INT1 nmhGetPppSecuritySecretsDirection (INT4, INT4 ,INT4 *);
extern INT1 nmhGetPppSecuritySecretsIdentity (INT4,INT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetPppSecuritySecretsSecret (INT4,INT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetPppTestConfigMTU (INT4, INT4*);


INT1 CfaCliCreatePppInterface (tCliHandle, UINT4, UINT1 *);
INT4 CfaCliStackPpp (tCliHandle CliHandle, INT4 i4PppIfaceNo,
                     INT4 i4LlIfaceNo, UINT1 u1IfType);
INT4 CfaCliUnStackPpp (tCliHandle CliHandle, INT4 i4PppIfaceNo);
INT4 CfaCliSetPppIpAddr (tCliHandle CliHandle, UINT4 u4IfIndex,
                         UINT4 u4IpAddr, UINT4 u4IpMask);
INT4 CfaCliPppShowInterfaceConfig (tCliHandle CliHandle, INT4 i4IfIndex);
INT4 CfaCliPppShowInterface (tCliHandle CliHandle, INT4 i4IfIndex);


#endif


INT4 CfaCliSetNetworkType (tCliHandle CliHandle, UINT4 u4IfIndex,
                           UINT4 u4NetworkType);
INT4 CfaCliSetPktRxValue (tCliHandle CliHandle, UINT4 u4IfIndex,
                          UINT1 *pu1WatchVal);
INT4 CfaCliSetPktRxMask (tCliHandle CliHandle, UINT4 u4IfIndex,
                         UINT1 *pu1WatchMask);
INT4 CfaCliSetPortForPktRx (UINT4 u4Index, UINT1 *pu1Ports);
INT4 CfaCliPktRxDisable (tCliHandle CliHandle, UINT4 u4IfIndex);
INT4 CfaCliClearPktRxMask (tCliHandle CliHandle, UINT4 u4IfIndex);
INT4 CfaCliShowPktRxEntry (tCliHandle CliHandle, UINT4  u4PatternIndex);
INT4 CfaCliShowPktRxTable (tCliHandle CliHandle);
INT4 CfaCliSetPktTxValue (tCliHandle CliHandle, UINT4 u4IfIndex,
                          UINT1 *pu1PktVal);
INT4 CfaCliSetPktTxPort (tCliHandle CliHandle, UINT4 u4IfIndex, UINT1 *pu1Ports);
INT4 CfaCliSetPktTxPortCount (tCliHandle CliHandle, UINT4 u4IfIndex, UINT1 *pu1Ports,
                         UINT4 u4Count);
INT4 CfaCliSetPktTxAll (tCliHandle CliHandle, UINT4 u4IfIndex, UINT1 *pu1Ports,
                         UINT4 u4Interval, UINT4 u4Count);
INT4 CfaCliPktTxDisable (tCliHandle CliHandle, UINT4 u4IfIndex);
INT4 CfaCliShowPktTxEntry (tCliHandle CliHandle, UINT4  u4IfIndex);
INT4 CfaCliShowPktTxTable (tCliHandle CliHandle);
INT4 CfaCliSetPortForPktTx (UINT4 u4Index, UINT1 *pu1Ports);
INT4 CfaCliSetCountForPktTx (UINT4 u4Index, UINT4 u4Count);
INT4 CfaCliSetIntervalForPktTx (UINT4 u4Index, UINT4 u4Interval);
INT4 CfaReSetSecIntfVlan PROTO ((tCliHandle CliHandle));

INT4 CfaCliSetACIfInfo PROTO ((tCliHandle CliHandle, UINT4 u4AcIndex, 
                               UINT4 u4PhyIndex, UINT4 u4CVlanId));
VOID IssInterfaceShowDebugging (tCliHandle CliHandle);
INT4 
CfaCliSetCounters (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4IpIntfStats);

INT4
CfaCliGetSbpIndex (INT1 *pi1IfName, INT1 *pi1IfNum, UINT4 *pu4IfIndex);

#ifdef VCPEMGR_WANTED
/* Tap interface functions */
PUBLIC INT4
CfaGddTapIfCreate (UINT1 *pu1IfName, UINT4 u4CfaIfIndex, UINT1 u1IfType);
PUBLIC INT4
CfaGddTapIfDelete (UINT4 u4IfIndex);
PUBLIC INT4
UpdateTapInterfaceStatus (UINT4 u4IfIndex, UINT1 u1Action);
#endif
INT4  CfaCliGetVlanIfIndexInCxt (UINT1 *pu1L2SwitchName, INT1 *pi1IfName,INT1 *pi1IfNum, INT1 *pi1IfInner,UINT4 *pu4IfIndex);
INT4 CfaSetSubSessMethod (tCliHandle CliHandle, UINT4 u4IfIndex,UINT4 u4Session);
