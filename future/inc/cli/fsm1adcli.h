/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsm1adcli.h,v 1.5 2015/03/07 10:42:14 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Dot1adMIPortNum[12];
extern UINT4 Dot1adMIPortPcpSelectionRow[12];
extern UINT4 Dot1adMIPortUseDei[12];
extern UINT4 Dot1adMIPortReqDropEncoding[12];
extern UINT4 Dot1adMIPortSVlanPriorityType[12];
extern UINT4 Dot1adMIPortSVlanPriority[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDot1adMIPortPcpSelectionRow(i4Dot1adMIPortNum ,i4SetValDot1adMIPortPcpSelectionRow) \
 nmhSetCmn(Dot1adMIPortPcpSelectionRow, 12, Dot1adMIPortPcpSelectionRowSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %i", i4Dot1adMIPortNum ,i4SetValDot1adMIPortPcpSelectionRow)
#define nmhSetDot1adMIPortUseDei(i4Dot1adMIPortNum ,i4SetValDot1adMIPortUseDei) \
 nmhSetCmn(Dot1adMIPortUseDei, 12, Dot1adMIPortUseDeiSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %i", i4Dot1adMIPortNum ,i4SetValDot1adMIPortUseDei)
#define nmhSetDot1adMIPortReqDropEncoding(i4Dot1adMIPortNum ,i4SetValDot1adMIPortReqDropEncoding) \
 nmhSetCmn(Dot1adMIPortReqDropEncoding, 12, Dot1adMIPortReqDropEncodingSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %i", i4Dot1adMIPortNum ,i4SetValDot1adMIPortReqDropEncoding)

#define nmhSetDot1adMIPortSVlanPriorityType(i4Dot1adMIPortNum ,i4SetValDot1adMIPortSVlanPriorityType) \
 nmhSetCmn(Dot1adMIPortSVlanPriorityType, 12, Dot1adMIPortSVlanPriorityTypeSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %i", i4Dot1adMIPortNum ,i4SetValDot1adMIPortSVlanPriorityType)

#define nmhSetDot1adMIPortSVlanPriority(i4Dot1adMIPortNum ,i4SetValDot1adMIPortSVlanPriority) \
 nmhSetCmn(Dot1adMIPortSVlanPriority, 12, Dot1adMIPortSVlanPrioritySet, VlanLock, VlanUnLock, 0, 0, 1, "%i %i", i4Dot1adMIPortNum ,i4SetValDot1adMIPortSVlanPriority)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Dot1adMIVidTranslationLocalVid[12];
extern UINT4 Dot1adMIVidTranslationRelayVid[12];
extern UINT4 Dot1adMIVidTranslationRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDot1adMIVidTranslationRelayVid(i4Dot1adMIPortNum , i4Dot1adMIVidTranslationLocalVid ,i4SetValDot1adMIVidTranslationRelayVid) \
 nmhSetCmn(Dot1adMIVidTranslationRelayVid, 12, Dot1adMIVidTranslationRelayVidSet, VlanLock, VlanUnLock, 0, 0, 2, "%i %i %i", i4Dot1adMIPortNum , i4Dot1adMIVidTranslationLocalVid ,i4SetValDot1adMIVidTranslationRelayVid)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Dot1adMICVidRegistrationCVid[12];
extern UINT4 Dot1adMICVidRegistrationSVid[12];
extern UINT4 Dot1adMICVidRegistrationUntaggedPep[12];
extern UINT4 Dot1adMICVidRegistrationUntaggedCep[12];
extern UINT4 Dot1adMICVidRegistrationSVlanPriorityType[12];
extern UINT4 Dot1adMICVidRegistrationSVlanPriority[12];
extern UINT4 Dot1adMICVidRegistrationRowStatus[12];
extern UINT4 Dot1adMICVidRegistrationRelayCVid[12];
#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDot1adMICVidRegistrationSVid(i4Dot1adMIPortNum , i4Dot1adMICVidRegistrationCVid ,i4SetValDot1adMICVidRegistrationSVid) \
 nmhSetCmn(Dot1adMICVidRegistrationSVid, 12, Dot1adMICVidRegistrationSVidSet, VlanLock, VlanUnLock, 0, 0, 2, "%i %i %i", i4Dot1adMIPortNum , i4Dot1adMICVidRegistrationCVid ,i4SetValDot1adMICVidRegistrationSVid)
#define nmhSetDot1adMICVidRegistrationUntaggedPep(i4Dot1adMIPortNum , i4Dot1adMICVidRegistrationCVid ,i4SetValDot1adMICVidRegistrationUntaggedPep) \
 nmhSetCmn(Dot1adMICVidRegistrationUntaggedPep, 12, Dot1adMICVidRegistrationUntaggedPepSet, VlanLock, VlanUnLock, 0, 0, 2, "%i %i %i", i4Dot1adMIPortNum , i4Dot1adMICVidRegistrationCVid ,i4SetValDot1adMICVidRegistrationUntaggedPep)
#define nmhSetDot1adMICVidRegistrationUntaggedCep(i4Dot1adMIPortNum , i4Dot1adMICVidRegistrationCVid ,i4SetValDot1adMICVidRegistrationUntaggedCep) \
 nmhSetCmn(Dot1adMICVidRegistrationUntaggedCep, 12, Dot1adMICVidRegistrationUntaggedCepSet, VlanLock, VlanUnLock, 0, 0, 2, "%i %i %i", i4Dot1adMIPortNum , i4Dot1adMICVidRegistrationCVid ,i4SetValDot1adMICVidRegistrationUntaggedCep)
#define nmhSetDot1adMICVidRegistrationRowStatus(i4Dot1adMIPortNum , i4Dot1adMICVidRegistrationCVid ,i4SetValDot1adMICVidRegistrationRowStatus) \
 nmhSetCmn(Dot1adMICVidRegistrationRowStatus, 12, Dot1adMICVidRegistrationRowStatusSet, VlanLock, VlanUnLock, 0, 1, 2, "%i %i %i", i4Dot1adMIPortNum , i4Dot1adMICVidRegistrationCVid ,i4SetValDot1adMICVidRegistrationRowStatus)

#define nmhSetDot1adMICVidRegistrationRelayCVid(i4Dot1adMIPortNum , i4Dot1adMICVidRegistrationCVid ,i4SetValDot1adMICVidRegistrationRelayCVid) \
 nmhSetCmn(Dot1adMICVidRegistrationRelayCVid, 12, Dot1adMICVidRegistrationRelayCVidSet, VlanLock, VlanUnLock, 0, 0, 2, "%i %i %i", i4Dot1adMIPortNum , i4Dot1adMICVidRegistrationCVid ,i4SetValDot1adMICVidRegistrationRelayCVid)
#endif

#define nmhSetDot1adMICVidRegistrationSVlanPriorityType(i4Dot1adMIPortNum , i4Dot1adMICVidRegistrationCVid ,i4SetValDot1adMICVidRegistrationSVlanPriorityType) \
 nmhSetCmn(Dot1adMICVidRegistrationSVlanPriorityType, 12, Dot1adMICVidRegistrationSVlanPriorityTypeSet, VlanLock, VlanUnLock, 0, 0, 2, "%i %i %i", i4Dot1adMIPortNum , i4Dot1adMICVidRegistrationCVid ,i4SetValDot1adMICVidRegistrationSVlanPriorityType)

#define nmhSetDot1adMICVidRegistrationSVlanPriority(i4Dot1adMIPortNum , i4Dot1adMICVidRegistrationCVid ,i4SetValDot1adMICVidRegistrationSVlanPriority) \
 nmhSetCmn(Dot1adMICVidRegistrationSVlanPriority, 12, Dot1adMICVidRegistrationSVlanPrioritySet, VlanLock, VlanUnLock, 0, 0, 2, "%i %i %i", i4Dot1adMIPortNum , i4Dot1adMICVidRegistrationCVid ,i4SetValDot1adMICVidRegistrationSVlanPriority)

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Dot1adMIPepPvid[12];
extern UINT4 Dot1adMIPepDefaultUserPriority[12];
extern UINT4 Dot1adMIPepAccptableFrameTypes[12];
extern UINT4 Dot1adMIPepIngressFiltering[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDot1adMIPepPvid(i4Dot1adMIPortNum , i4Dot1adMICVidRegistrationSVid ,i4SetValDot1adMIPepPvid) \
 nmhSetCmn(Dot1adMIPepPvid, 12, Dot1adMIPepPvidSet, VlanLock, VlanUnLock, 0, 0, 2, "%i %i %i", i4Dot1adMIPortNum , i4Dot1adMICVidRegistrationSVid ,i4SetValDot1adMIPepPvid)
#define nmhSetDot1adMIPepDefaultUserPriority(i4Dot1adMIPortNum , i4Dot1adMICVidRegistrationSVid ,i4SetValDot1adMIPepDefaultUserPriority) \
 nmhSetCmn(Dot1adMIPepDefaultUserPriority, 12, Dot1adMIPepDefaultUserPrioritySet, VlanLock, VlanUnLock, 0, 0, 2, "%i %i %i", i4Dot1adMIPortNum , i4Dot1adMICVidRegistrationSVid ,i4SetValDot1adMIPepDefaultUserPriority)
#define nmhSetDot1adMIPepAccptableFrameTypes(i4Dot1adMIPortNum , i4Dot1adMICVidRegistrationSVid ,i4SetValDot1adMIPepAccptableFrameTypes) \
 nmhSetCmn(Dot1adMIPepAccptableFrameTypes, 12, Dot1adMIPepAccptableFrameTypesSet, VlanLock, VlanUnLock, 0, 0, 2, "%i %i %i", i4Dot1adMIPortNum , i4Dot1adMICVidRegistrationSVid ,i4SetValDot1adMIPepAccptableFrameTypes)
#define nmhSetDot1adMIPepIngressFiltering(i4Dot1adMIPortNum , i4Dot1adMICVidRegistrationSVid ,i4SetValDot1adMIPepIngressFiltering) \
 nmhSetCmn(Dot1adMIPepIngressFiltering, 12, Dot1adMIPepIngressFilteringSet, VlanLock, VlanUnLock, 0, 0, 2, "%i %i %i", i4Dot1adMIPortNum , i4Dot1adMICVidRegistrationSVid ,i4SetValDot1adMIPepIngressFiltering)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Dot1adMIServicePriorityRegenReceivedPriority[12];
extern UINT4 Dot1adMIServicePriorityRegenRegeneratedPriority[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDot1adMIServicePriorityRegenRegeneratedPriority(i4Dot1adMIPortNum , i4Dot1adMICVidRegistrationSVid , i4Dot1adMIServicePriorityRegenReceivedPriority ,i4SetValDot1adMIServicePriorityRegenRegeneratedPriority) \
 nmhSetCmn(Dot1adMIServicePriorityRegenRegeneratedPriority, 12, Dot1adMIServicePriorityRegenRegeneratedPrioritySet, VlanLock, VlanUnLock, 0, 0, 3, "%i %i %i %i", i4Dot1adMIPortNum , i4Dot1adMICVidRegistrationSVid , i4Dot1adMIServicePriorityRegenReceivedPriority ,i4SetValDot1adMIServicePriorityRegenRegeneratedPriority)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Dot1adMIPcpDecodingPcpSelRow[12];
extern UINT4 Dot1adMIPcpDecodingPcpValue[12];
extern UINT4 Dot1adMIPcpDecodingPriority[12];
extern UINT4 Dot1adMIPcpDecodingDropEligible[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDot1adMIPcpDecodingPriority(i4Dot1adMIPortNum , i4Dot1adMIPcpDecodingPcpSelRow , i4Dot1adMIPcpDecodingPcpValue ,i4SetValDot1adMIPcpDecodingPriority) \
 nmhSetCmn(Dot1adMIPcpDecodingPriority, 12, Dot1adMIPcpDecodingPrioritySet, VlanLock, VlanUnLock, 0, 0, 3, "%i %i %i %i", i4Dot1adMIPortNum , i4Dot1adMIPcpDecodingPcpSelRow , i4Dot1adMIPcpDecodingPcpValue ,i4SetValDot1adMIPcpDecodingPriority)
#define nmhSetDot1adMIPcpDecodingDropEligible(i4Dot1adMIPortNum , i4Dot1adMIPcpDecodingPcpSelRow , i4Dot1adMIPcpDecodingPcpValue ,i4SetValDot1adMIPcpDecodingDropEligible) \
 nmhSetCmn(Dot1adMIPcpDecodingDropEligible, 12, Dot1adMIPcpDecodingDropEligibleSet, VlanLock, VlanUnLock, 0, 0, 3, "%i %i %i %i", i4Dot1adMIPortNum , i4Dot1adMIPcpDecodingPcpSelRow , i4Dot1adMIPcpDecodingPcpValue ,i4SetValDot1adMIPcpDecodingDropEligible)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Dot1adMIPcpEncodingPcpSelRow[12];
extern UINT4 Dot1adMIPcpEncodingPriority[12];
extern UINT4 Dot1adMIPcpEncodingDropEligible[12];
extern UINT4 Dot1adMIPcpEncodingPcpValue[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDot1adMIPcpEncodingPcpValue(i4Dot1adMIPortNum , i4Dot1adMIPcpEncodingPcpSelRow , i4Dot1adMIPcpEncodingPriority , i4Dot1adMIPcpEncodingDropEligible ,i4SetValDot1adMIPcpEncodingPcpValue) \
 nmhSetCmn(Dot1adMIPcpEncodingPcpValue, 12, Dot1adMIPcpEncodingPcpValueSet, VlanLock, VlanUnLock, 0, 0, 4, "%i %i %i %i %i", i4Dot1adMIPortNum , i4Dot1adMIPcpEncodingPcpSelRow , i4Dot1adMIPcpEncodingPriority , i4Dot1adMIPcpEncodingDropEligible ,i4SetValDot1adMIPcpEncodingPcpValue)

#endif
