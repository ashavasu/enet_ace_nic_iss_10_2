/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmiricli.h,v 1.4 2013/05/23 12:33:10 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIRipContextId[12];
extern UINT4 FsMIRip2Security[12];
extern UINT4 FsMIRip2Peers[12];
extern UINT4 FsMIRip2TrustNBRListEnable[12];
extern UINT4 FsMIRip2SpacingEnable[12];
extern UINT4 FsMIRip2AutoSummaryStatus[12];
extern UINT4 FsMIRip2RetransTimeoutInt[12];
extern UINT4 FsMIRip2MaxRetransmissions[12];
extern UINT4 FsMIRip2OverSubscriptionTimeout[12];
extern UINT4 FsMIRip2Propagate[12];
extern UINT4 FsMIRipTrcFlag[12];
extern UINT4 FsMIRipRowStatus[12];
extern UINT4 FsMIRipAdminStatus[12];
extern UINT4 FsMIRip2LastAuthKeyLifetimeStatus[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIRipGlobalTrcFlag[10];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIRip2TrustNBRIpAddr[12];
extern UINT4 FsMIRip2TrustNBRRowStatus[12];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIRip2IfConfAddress[12];
extern UINT4 FsMIRip2IfAdminStat[12];
extern UINT4 FsMIRip2IfConfUpdateTmr[12];
extern UINT4 FsMIRip2IfConfGarbgCollectTmr[12];
extern UINT4 FsMIRip2IfConfRouteAgeTmr[12];
extern UINT4 FsMIRip2IfSplitHorizonStatus[12];
extern UINT4 FsMIRip2IfConfDefRtInstall[12];
extern UINT4 FsMIRip2IfConfSpacingTmr[12];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIRipMd5AuthAddress[12];
extern UINT4 FsMIRipMd5AuthKeyId[12];
extern UINT4 FsMIRipMd5AuthKey[12];
extern UINT4 FsMIRipMd5KeyStartTime[12];
extern UINT4 FsMIRipMd5KeyExpiryTime[12];
extern UINT4 FsMIRipMd5KeyRowStatus[12];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIRipCryptoAuthIfIndex[12];
extern UINT4 FsMIRipCryptoAuthAddress[12];
extern UINT4 FsMIRipCryptoAuthKeyId[12];
extern UINT4 FsMIRipCryptoAuthKey[12];
extern UINT4 FsMIRipCryptoKeyStartAccept[12];
extern UINT4 FsMIRipCryptoKeyStartGenerate[12];
extern UINT4 FsMIRipCryptoKeyStopGenerate[12];
extern UINT4 FsMIRipCryptoKeyStopAccept[12];
extern UINT4 FsMIRipCryptoKeyStatus[12];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIRip2NBRUnicastIpAddr[12];
extern UINT4 FsMIRip2NBRUnicastNBRRowStatus[12];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIRipIfIndex[12];
extern UINT4 FsMIRipAggAddress[12];
extern UINT4 FsMIRipAggAddressMask[12];
extern UINT4 FsMIRipAggStatus[12];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIRipRRDGlobalStatus[12];
extern UINT4 FsMIRipRRDSrcProtoMaskEnable[12];
extern UINT4 FsMIRipRRDSrcProtoMaskDisable[12];
extern UINT4 FsMIRipRRDRouteTagType[12];
extern UINT4 FsMIRipRRDRouteTag[12];
extern UINT4 FsMIRipRRDRouteDefMetric[12];
extern UINT4 FsMIRipRRDRouteMapEnable[12];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIRipDistInOutRouteMapName[12];
extern UINT4 FsMIRipDistInOutRouteMapType[12];
extern UINT4 FsMIRipDistInOutRouteMapValue[12];
extern UINT4 FsMIRipDistInOutRouteMapRowStatus[12];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIRipPreferenceValue[12];

