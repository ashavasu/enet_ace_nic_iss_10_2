/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdtecli.h,v 1.3 2011/10/25 10:44:45 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 MplsTunnelNotificationMaxRate[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetMplsTunnelNotificationMaxRate(u4SetValMplsTunnelNotificationMaxRate) \
 nmhSetCmn(MplsTunnelNotificationMaxRate, 11, MplsTunnelNotificationMaxRateSet, NULL, NULL, 0, 0, 0, "%u", u4SetValMplsTunnelNotificationMaxRate)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 MplsTunnelIndex[13];
extern UINT4 MplsTunnelInstance[13];
extern UINT4 MplsTunnelIngressLSRId[13];
extern UINT4 MplsTunnelEgressLSRId[13];
extern UINT4 MplsTunnelDescr[13];
extern UINT4 MplsTunnelIsIf[13];
extern UINT4 MplsTunnelRole[13];
extern UINT4 MplsTunnelXCPointer[13];
extern UINT4 MplsTunnelSignallingProto[13];
extern UINT4 MplsTunnelSetupPrio[13];
extern UINT4 MplsTunnelHoldingPrio[13];
extern UINT4 MplsTunnelSessionAttributes[13];
extern UINT4 MplsTunnelLocalProtectInUse[13];
extern UINT4 MplsTunnelResourcePointer[13];
extern UINT4 MplsTunnelInstancePriority[13];
extern UINT4 MplsTunnelHopTableIndex[13];
extern UINT4 MplsTunnelPathInUse[13];
extern UINT4 MplsTunnelIncludeAnyAffinity[13];
extern UINT4 MplsTunnelIncludeAllAffinity[13];
extern UINT4 MplsTunnelExcludeAnyAffinity[13];
extern UINT4 MplsTunnelAdminStatus[13];
extern UINT4 MplsTunnelRowStatus[13];
extern UINT4 MplsTunnelStorageType[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetMplsTunnelName(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,pSetValMplsTunnelName) \
 nmhSetCmn(MplsTunnelName, 13, MplsTunnelNameSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %s", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,pSetValMplsTunnelName)
#define nmhSetMplsTunnelDescr(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,pSetValMplsTunnelDescr) \
 nmhSetCmn(MplsTunnelDescr, 13, MplsTunnelDescrSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %s", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,pSetValMplsTunnelDescr)
#define nmhSetMplsTunnelIsIf(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValMplsTunnelIsIf) \
 nmhSetCmn(MplsTunnelIsIf, 13, MplsTunnelIsIfSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValMplsTunnelIsIf)
#define nmhSetMplsTunnelRole(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValMplsTunnelRole) \
 nmhSetCmn(MplsTunnelRole, 13, MplsTunnelRoleSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValMplsTunnelRole)
#define nmhSetMplsTunnelXCPointer(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,pSetValMplsTunnelXCPointer) \
 nmhSetCmn(MplsTunnelXCPointer, 13, MplsTunnelXCPointerSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %o", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,pSetValMplsTunnelXCPointer)
#define nmhSetMplsTunnelSignallingProto(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValMplsTunnelSignallingProto) \
 nmhSetCmn(MplsTunnelSignallingProto, 13, MplsTunnelSignallingProtoSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValMplsTunnelSignallingProto)
#define nmhSetMplsTunnelSetupPrio(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValMplsTunnelSetupPrio) \
 nmhSetCmn(MplsTunnelSetupPrio, 13, MplsTunnelSetupPrioSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValMplsTunnelSetupPrio)
#define nmhSetMplsTunnelHoldingPrio(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValMplsTunnelHoldingPrio) \
 nmhSetCmn(MplsTunnelHoldingPrio, 13, MplsTunnelHoldingPrioSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValMplsTunnelHoldingPrio)
#define nmhSetMplsTunnelSessionAttributes(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,pSetValMplsTunnelSessionAttributes) \
 nmhSetCmn(MplsTunnelSessionAttributes, 13, MplsTunnelSessionAttributesSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %s", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,pSetValMplsTunnelSessionAttributes)
#define nmhSetMplsTunnelLocalProtectInUse(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValMplsTunnelLocalProtectInUse) \
 nmhSetCmn(MplsTunnelLocalProtectInUse, 13, MplsTunnelLocalProtectInUseSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValMplsTunnelLocalProtectInUse)
#define nmhSetMplsTunnelResourcePointer(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,pSetValMplsTunnelResourcePointer) \
 nmhSetCmn(MplsTunnelResourcePointer, 13, MplsTunnelResourcePointerSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %o", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,pSetValMplsTunnelResourcePointer)
#define nmhSetMplsTunnelInstancePriority(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,u4SetValMplsTunnelInstancePriority) \
 nmhSetCmn(MplsTunnelInstancePriority, 13, MplsTunnelInstancePrioritySet, NULL, NULL, 0, 0, 4, "%u %u %u %u %u", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,u4SetValMplsTunnelInstancePriority)
#define nmhSetMplsTunnelHopTableIndex(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,u4SetValMplsTunnelHopTableIndex) \
 nmhSetCmn(MplsTunnelHopTableIndex, 13, MplsTunnelHopTableIndexSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %u", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,u4SetValMplsTunnelHopTableIndex)
#define nmhSetMplsTunnelPathInUse(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,u4SetValMplsTunnelPathInUse) \
 nmhSetCmn(MplsTunnelPathInUse, 13, MplsTunnelPathInUseSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %u", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,u4SetValMplsTunnelPathInUse)
#define nmhSetMplsTunnelIncludeAnyAffinity(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,u4SetValMplsTunnelIncludeAnyAffinity) \
 nmhSetCmn(MplsTunnelIncludeAnyAffinity, 13, MplsTunnelIncludeAnyAffinitySet, NULL, NULL, 0, 0, 4, "%u %u %u %u %u", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,u4SetValMplsTunnelIncludeAnyAffinity)
#define nmhSetMplsTunnelIncludeAllAffinity(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,u4SetValMplsTunnelIncludeAllAffinity) \
 nmhSetCmn(MplsTunnelIncludeAllAffinity, 13, MplsTunnelIncludeAllAffinitySet, NULL, NULL, 0, 0, 4, "%u %u %u %u %u", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,u4SetValMplsTunnelIncludeAllAffinity)
#define nmhSetMplsTunnelExcludeAnyAffinity(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,u4SetValMplsTunnelExcludeAnyAffinity) \
 nmhSetCmn(MplsTunnelExcludeAnyAffinity, 13, MplsTunnelExcludeAnyAffinitySet, NULL, NULL, 0, 0, 4, "%u %u %u %u %u", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,u4SetValMplsTunnelExcludeAnyAffinity)
#define nmhSetMplsTunnelAdminStatus(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValMplsTunnelAdminStatus) \
 nmhSetCmn(MplsTunnelAdminStatus, 13, MplsTunnelAdminStatusSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValMplsTunnelAdminStatus)
#define nmhSetMplsTunnelRowStatus(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValMplsTunnelRowStatus) \
 nmhSetCmn(MplsTunnelRowStatus, 13, MplsTunnelRowStatusSet, NULL, NULL, 0, 1, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValMplsTunnelRowStatus)
#define nmhSetMplsTunnelStorageType(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValMplsTunnelStorageType) \
 nmhSetCmn(MplsTunnelStorageType, 13, MplsTunnelStorageTypeSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValMplsTunnelStorageType)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 MplsTunnelHopListIndex[13];
extern UINT4 MplsTunnelHopPathOptionIndex[13];
extern UINT4 MplsTunnelHopIndex[13];
extern UINT4 MplsTunnelHopAddrType[13];
extern UINT4 MplsTunnelHopIpAddr[13];
extern UINT4 MplsTunnelHopIpPrefixLen[13];
extern UINT4 MplsTunnelHopAsNumber[13];
extern UINT4 MplsTunnelHopAddrUnnum[13];
extern UINT4 MplsTunnelHopLspId[13];
extern UINT4 MplsTunnelHopType[13];
extern UINT4 MplsTunnelHopInclude[13];
extern UINT4 MplsTunnelHopPathOptionName[13];
extern UINT4 MplsTunnelHopEntryPathComp[13];
extern UINT4 MplsTunnelHopRowStatus[13];
extern UINT4 MplsTunnelHopStorageType[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetMplsTunnelHopAddrType(u4MplsTunnelHopListIndex , u4MplsTunnelHopPathOptionIndex , u4MplsTunnelHopIndex ,i4SetValMplsTunnelHopAddrType) \
 nmhSetCmn(MplsTunnelHopAddrType, 13, MplsTunnelHopAddrTypeSet, NULL, NULL, 0, 0, 3, "%u %u %u %i", u4MplsTunnelHopListIndex , u4MplsTunnelHopPathOptionIndex , u4MplsTunnelHopIndex ,i4SetValMplsTunnelHopAddrType)
#define nmhSetMplsTunnelHopIpAddr(u4MplsTunnelHopListIndex , u4MplsTunnelHopPathOptionIndex , u4MplsTunnelHopIndex ,pSetValMplsTunnelHopIpAddr) \
 nmhSetCmn(MplsTunnelHopIpAddr, 13, MplsTunnelHopIpAddrSet, NULL, NULL, 0, 0, 3, "%u %u %u %s", u4MplsTunnelHopListIndex , u4MplsTunnelHopPathOptionIndex , u4MplsTunnelHopIndex ,pSetValMplsTunnelHopIpAddr)
#define nmhSetMplsTunnelHopIpPrefixLen(u4MplsTunnelHopListIndex , u4MplsTunnelHopPathOptionIndex , u4MplsTunnelHopIndex ,u4SetValMplsTunnelHopIpPrefixLen) \
 nmhSetCmn(MplsTunnelHopIpPrefixLen, 13, MplsTunnelHopIpPrefixLenSet, NULL, NULL, 0, 0, 3, "%u %u %u %u", u4MplsTunnelHopListIndex , u4MplsTunnelHopPathOptionIndex , u4MplsTunnelHopIndex ,u4SetValMplsTunnelHopIpPrefixLen)
#define nmhSetMplsTunnelHopAsNumber(u4MplsTunnelHopListIndex , u4MplsTunnelHopPathOptionIndex , u4MplsTunnelHopIndex ,pSetValMplsTunnelHopAsNumber) \
 nmhSetCmn(MplsTunnelHopAsNumber, 13, MplsTunnelHopAsNumberSet, NULL, NULL, 0, 0, 3, "%u %u %u %s", u4MplsTunnelHopListIndex , u4MplsTunnelHopPathOptionIndex , u4MplsTunnelHopIndex ,pSetValMplsTunnelHopAsNumber)
#define nmhSetMplsTunnelHopAddrUnnum(u4MplsTunnelHopListIndex , u4MplsTunnelHopPathOptionIndex , u4MplsTunnelHopIndex ,pSetValMplsTunnelHopAddrUnnum) \
 nmhSetCmn(MplsTunnelHopAddrUnnum, 13, MplsTunnelHopAddrUnnumSet, NULL, NULL, 0, 0, 3, "%u %u %u %s", u4MplsTunnelHopListIndex , u4MplsTunnelHopPathOptionIndex , u4MplsTunnelHopIndex ,pSetValMplsTunnelHopAddrUnnum)
#define nmhSetMplsTunnelHopLspId(u4MplsTunnelHopListIndex , u4MplsTunnelHopPathOptionIndex , u4MplsTunnelHopIndex ,pSetValMplsTunnelHopLspId) \
 nmhSetCmn(MplsTunnelHopLspId, 13, MplsTunnelHopLspIdSet, NULL, NULL, 0, 0, 3, "%u %u %u %s", u4MplsTunnelHopListIndex , u4MplsTunnelHopPathOptionIndex , u4MplsTunnelHopIndex ,pSetValMplsTunnelHopLspId)
#define nmhSetMplsTunnelHopType(u4MplsTunnelHopListIndex , u4MplsTunnelHopPathOptionIndex , u4MplsTunnelHopIndex ,i4SetValMplsTunnelHopType) \
 nmhSetCmn(MplsTunnelHopType, 13, MplsTunnelHopTypeSet, NULL, NULL, 0, 0, 3, "%u %u %u %i", u4MplsTunnelHopListIndex , u4MplsTunnelHopPathOptionIndex , u4MplsTunnelHopIndex ,i4SetValMplsTunnelHopType)
#define nmhSetMplsTunnelHopInclude(u4MplsTunnelHopListIndex , u4MplsTunnelHopPathOptionIndex , u4MplsTunnelHopIndex ,i4SetValMplsTunnelHopInclude) \
 nmhSetCmn(MplsTunnelHopInclude, 13, MplsTunnelHopIncludeSet, NULL, NULL, 0, 0, 3, "%u %u %u %i", u4MplsTunnelHopListIndex , u4MplsTunnelHopPathOptionIndex , u4MplsTunnelHopIndex ,i4SetValMplsTunnelHopInclude)
#define nmhSetMplsTunnelHopPathOptionName(u4MplsTunnelHopListIndex , u4MplsTunnelHopPathOptionIndex , u4MplsTunnelHopIndex ,pSetValMplsTunnelHopPathOptionName) \
 nmhSetCmn(MplsTunnelHopPathOptionName, 13, MplsTunnelHopPathOptionNameSet, NULL, NULL, 0, 0, 3, "%u %u %u %s", u4MplsTunnelHopListIndex , u4MplsTunnelHopPathOptionIndex , u4MplsTunnelHopIndex ,pSetValMplsTunnelHopPathOptionName)
#define nmhSetMplsTunnelHopEntryPathComp(u4MplsTunnelHopListIndex , u4MplsTunnelHopPathOptionIndex , u4MplsTunnelHopIndex ,i4SetValMplsTunnelHopEntryPathComp) \
 nmhSetCmn(MplsTunnelHopEntryPathComp, 13, MplsTunnelHopEntryPathCompSet, NULL, NULL, 0, 0, 3, "%u %u %u %i", u4MplsTunnelHopListIndex , u4MplsTunnelHopPathOptionIndex , u4MplsTunnelHopIndex ,i4SetValMplsTunnelHopEntryPathComp)
#define nmhSetMplsTunnelHopRowStatus(u4MplsTunnelHopListIndex , u4MplsTunnelHopPathOptionIndex , u4MplsTunnelHopIndex ,i4SetValMplsTunnelHopRowStatus) \
 nmhSetCmn(MplsTunnelHopRowStatus, 13, MplsTunnelHopRowStatusSet, NULL, NULL, 0, 1, 3, "%u %u %u %i", u4MplsTunnelHopListIndex , u4MplsTunnelHopPathOptionIndex , u4MplsTunnelHopIndex ,i4SetValMplsTunnelHopRowStatus)
#define nmhSetMplsTunnelHopStorageType(u4MplsTunnelHopListIndex , u4MplsTunnelHopPathOptionIndex , u4MplsTunnelHopIndex ,i4SetValMplsTunnelHopStorageType) \
 nmhSetCmn(MplsTunnelHopStorageType, 13, MplsTunnelHopStorageTypeSet, NULL, NULL, 0, 0, 3, "%u %u %u %i", u4MplsTunnelHopListIndex , u4MplsTunnelHopPathOptionIndex , u4MplsTunnelHopIndex ,i4SetValMplsTunnelHopStorageType)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 MplsTunnelResourceIndex[13];
extern UINT4 MplsTunnelResourceMaxRate[13];
extern UINT4 MplsTunnelResourceMeanRate[13];
extern UINT4 MplsTunnelResourceMaxBurstSize[13];
extern UINT4 MplsTunnelResourceMeanBurstSize[13];
extern UINT4 MplsTunnelResourceExBurstSize[13];
extern UINT4 MplsTunnelResourceFrequency[13];
extern UINT4 MplsTunnelResourceWeight[13];
extern UINT4 MplsTunnelResourceRowStatus[13];
extern UINT4 MplsTunnelResourceStorageType[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetMplsTunnelResourceMaxRate(u4MplsTunnelResourceIndex ,u4SetValMplsTunnelResourceMaxRate) \
 nmhSetCmn(MplsTunnelResourceMaxRate, 13, MplsTunnelResourceMaxRateSet, NULL, NULL, 0, 0, 1, "%u %u", u4MplsTunnelResourceIndex ,u4SetValMplsTunnelResourceMaxRate)
#define nmhSetMplsTunnelResourceMeanRate(u4MplsTunnelResourceIndex ,u4SetValMplsTunnelResourceMeanRate) \
 nmhSetCmn(MplsTunnelResourceMeanRate, 13, MplsTunnelResourceMeanRateSet, NULL, NULL, 0, 0, 1, "%u %u", u4MplsTunnelResourceIndex ,u4SetValMplsTunnelResourceMeanRate)
#define nmhSetMplsTunnelResourceMaxBurstSize(u4MplsTunnelResourceIndex ,u4SetValMplsTunnelResourceMaxBurstSize) \
 nmhSetCmn(MplsTunnelResourceMaxBurstSize, 13, MplsTunnelResourceMaxBurstSizeSet, NULL, NULL, 0, 0, 1, "%u %u", u4MplsTunnelResourceIndex ,u4SetValMplsTunnelResourceMaxBurstSize)
#define nmhSetMplsTunnelResourceMeanBurstSize(u4MplsTunnelResourceIndex ,u4SetValMplsTunnelResourceMeanBurstSize) \
 nmhSetCmn(MplsTunnelResourceMeanBurstSize, 13, MplsTunnelResourceMeanBurstSizeSet, NULL, NULL, 0, 0, 1, "%u %u", u4MplsTunnelResourceIndex ,u4SetValMplsTunnelResourceMeanBurstSize)
#define nmhSetMplsTunnelResourceExBurstSize(u4MplsTunnelResourceIndex ,u4SetValMplsTunnelResourceExBurstSize) \
 nmhSetCmn(MplsTunnelResourceExBurstSize, 13, MplsTunnelResourceExBurstSizeSet, NULL, NULL, 0, 0, 1, "%u %u", u4MplsTunnelResourceIndex ,u4SetValMplsTunnelResourceExBurstSize)
#define nmhSetMplsTunnelResourceFrequency(u4MplsTunnelResourceIndex ,i4SetValMplsTunnelResourceFrequency) \
 nmhSetCmn(MplsTunnelResourceFrequency, 13, MplsTunnelResourceFrequencySet, NULL, NULL, 0, 0, 1, "%u %i", u4MplsTunnelResourceIndex ,i4SetValMplsTunnelResourceFrequency)
#define nmhSetMplsTunnelResourceWeight(u4MplsTunnelResourceIndex ,u4SetValMplsTunnelResourceWeight) \
 nmhSetCmn(MplsTunnelResourceWeight, 13, MplsTunnelResourceWeightSet, NULL, NULL, 0, 0, 1, "%u %u", u4MplsTunnelResourceIndex ,u4SetValMplsTunnelResourceWeight)
#define nmhSetMplsTunnelResourceRowStatus(u4MplsTunnelResourceIndex ,i4SetValMplsTunnelResourceRowStatus) \
 nmhSetCmn(MplsTunnelResourceRowStatus, 13, MplsTunnelResourceRowStatusSet, NULL, NULL, 0, 1, 1, "%u %i", u4MplsTunnelResourceIndex ,i4SetValMplsTunnelResourceRowStatus)
#define nmhSetMplsTunnelResourceStorageType(u4MplsTunnelResourceIndex ,i4SetValMplsTunnelResourceStorageType) \
 nmhSetCmn(MplsTunnelResourceStorageType, 13, MplsTunnelResourceStorageTypeSet, NULL, NULL, 0, 0, 1, "%u %i", u4MplsTunnelResourceIndex ,i4SetValMplsTunnelResourceStorageType)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 MplsTunnelCRLDPResMeanBurstSize[13];
extern UINT4 MplsTunnelCRLDPResExBurstSize[13];
extern UINT4 MplsTunnelCRLDPResFrequency[13];
extern UINT4 MplsTunnelCRLDPResWeight[13];
extern UINT4 MplsTunnelCRLDPResFlags[13];
extern UINT4 MplsTunnelCRLDPResRowStatus[13];
extern UINT4 MplsTunnelCRLDPResStorageType[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetMplsTunnelCRLDPResMeanBurstSize(u4MplsTunnelResourceIndex ,u4SetValMplsTunnelCRLDPResMeanBurstSize) \
 nmhSetCmn(MplsTunnelCRLDPResMeanBurstSize, 13, MplsTunnelCRLDPResMeanBurstSizeSet, NULL, NULL, 0, 0, 1, "%u %u", u4MplsTunnelResourceIndex ,u4SetValMplsTunnelCRLDPResMeanBurstSize)
#define nmhSetMplsTunnelCRLDPResExBurstSize(u4MplsTunnelResourceIndex ,u4SetValMplsTunnelCRLDPResExBurstSize) \
 nmhSetCmn(MplsTunnelCRLDPResExBurstSize, 13, MplsTunnelCRLDPResExBurstSizeSet, NULL, NULL, 0, 0, 1, "%u %u", u4MplsTunnelResourceIndex ,u4SetValMplsTunnelCRLDPResExBurstSize)
#define nmhSetMplsTunnelCRLDPResFrequency(u4MplsTunnelResourceIndex ,i4SetValMplsTunnelCRLDPResFrequency) \
 nmhSetCmn(MplsTunnelCRLDPResFrequency, 13, MplsTunnelCRLDPResFrequencySet, NULL, NULL, 0, 0, 1, "%u %i", u4MplsTunnelResourceIndex ,i4SetValMplsTunnelCRLDPResFrequency)
#define nmhSetMplsTunnelCRLDPResWeight(u4MplsTunnelResourceIndex ,u4SetValMplsTunnelCRLDPResWeight) \
 nmhSetCmn(MplsTunnelCRLDPResWeight, 13, MplsTunnelCRLDPResWeightSet, NULL, NULL, 0, 0, 1, "%u %u", u4MplsTunnelResourceIndex ,u4SetValMplsTunnelCRLDPResWeight)
#define nmhSetMplsTunnelCRLDPResFlags(u4MplsTunnelResourceIndex ,u4SetValMplsTunnelCRLDPResFlags) \
 nmhSetCmn(MplsTunnelCRLDPResFlags, 13, MplsTunnelCRLDPResFlagsSet, NULL, NULL, 0, 0, 1, "%u %u", u4MplsTunnelResourceIndex ,u4SetValMplsTunnelCRLDPResFlags)
#define nmhSetMplsTunnelCRLDPResRowStatus(u4MplsTunnelResourceIndex ,i4SetValMplsTunnelCRLDPResRowStatus) \
 nmhSetCmn(MplsTunnelCRLDPResRowStatus, 13, MplsTunnelCRLDPResRowStatusSet, NULL, NULL, 0, 1, 1, "%u %i", u4MplsTunnelResourceIndex ,i4SetValMplsTunnelCRLDPResRowStatus)
#define nmhSetMplsTunnelCRLDPResStorageType(u4MplsTunnelResourceIndex ,i4SetValMplsTunnelCRLDPResStorageType) \
 nmhSetCmn(MplsTunnelCRLDPResStorageType, 13, MplsTunnelCRLDPResStorageTypeSet, NULL, NULL, 0, 0, 1, "%u %i", u4MplsTunnelResourceIndex ,i4SetValMplsTunnelCRLDPResStorageType)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 MplsTunnelNotificationEnable[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetMplsTunnelNotificationEnable(i4SetValMplsTunnelNotificationEnable) \
 nmhSetCmn(MplsTunnelNotificationEnable, 11, MplsTunnelNotificationEnableSet, NULL, NULL, 0, 0, 0, "%i", i4SetValMplsTunnelNotificationEnable)

#endif
