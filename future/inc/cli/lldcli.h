/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: lldcli.h,v 1.44 2017/11/22 12:34:03 siva Exp $
 *
 * Description: This file contains the data structure and macros
 *              for LLDP CLI module
 *********************************************************************/

#ifndef _LLDCLI_H_
#define _LLDCLI_H_

#include "cli.h"
#include "ip6util.h"

/* Max No of variable inputs to CLI parser */
#define LLDP_CLI_MAX_ARGS              10

/* Max No of LLDP System Capabability bits */
#define LLDP_CLI_SYS_CAPAB_MAX_BITS ISS_SYS_CAPABILITIES_LEN*(BITS_PER_BYTE/2)

/* CLI Option value to show local managment address */
#define LLDP_CLI_SHOW_LOCAL_MGMT_ADDR   1

/* Max No of Dot3 Physical Media Capabability Bits */
#define LLDP_CLI_PHY_MEDIA_CAP_MAX_BITS 16 

/* Max Remote Chassis Id Length to be displayed in CLI */
#define LLDP_CLI_CHASSIS_DISP_LEN 17

/* Max Remote Chassis Id Length to be displayed in CLI */
#define LLDP_CLI_PORT_DISP_LEN 10

/* LLDP Remote switch MAC Address */
#define LLDP_CLI_MAN_ADDR_TYPE_MAC 0

#define DEFAULT_DEST_MAC_ADDR_INDEX 1
#define LLDPv2_MGMT_VID_DISABLED 2
#define LLDP_CIVIC_MODE "civic-loc-"
/* LLDP Neighbor info structure */
typedef struct LldpCliNeighborInfo {
    tCliHandle CliHandle;
    INT4       i4IfIndex;
    INT4       i4Option;
    UINT1      au1ChassisId[LLDP_MAX_LEN_CHASSISID + 1];
    UINT1      au1PortId[LLDP_MAX_LEN_PORTID + 1];
}tLldpCliNeighborInfo;

/*Error Strings*/
enum{
 LLDP_CLI_ERR_INVALID_TX_DELAY             = 1,
 LLDP_CLI_ERR_INVALID_MAN_ADDR_LEN         = 2,
 LLDP_CLI_ERR_INVALID_VLAN_NAME            = 3,
 LLDP_CLI_ERR_SHUTDOWN_IN_PROGRESS         = 4,
 LLDP_CLI_ERR_GLOBAL_SHUTWHILE_EXP         = 5,
 LLDP_CLI_ERR_DEFAULT_VLAN_INTERFACE       = 6,
 LLDP_VLAN_BASE_BRIDGE_LLDP_ENABLED        = 7,
 LLDP_CLI_ERR_MGMT_STATUS                  = 8,
 LLDP_CLI_INVALID_ADDR_V1                  = 9,
 LLDP_CLI_INVALID_TX_DELAY_V2              = 10,
 LLDP_CLI_ERR_INVALID_LA_TLV_ON_ICCL       = 11,
 LLDP_CLI_VLAN_NAME_NOT_CONFIGURED         = 12,
 LLDP_CLI_DEST_MAC_NOT_FOUND               = 13,
 LLDP_CLI_MAX_DEST_MAC_REACHED             = 14,
 LLDP_MED_CLI_ERR_UNKNOWN_POL              = 15,
 LLDP_CLI_INVALID_DESTINATION_MAC          = 16,
 LLDP_MED_CLI_INVALID_COORDINATE_LEN       = 17,
 LLDP_MED_CLI_INVALID_CIVIC_LEN            = 18,
 LLDP_MED_CLI_INVALID_ELIN_LEN             = 19,
 LLDP_CLI_INVALID_IP_ADDRESS               = 20,
 LLDP_VLAN_EVB_CDCP_ENABLED                = 21,
 LLDP_CLI_INVALID_IP6_ADDRESS              = 22,
 LLDP_CLI_DCBX_REGISTERED                  = 23,
 LLDP_CLI_MAX_ERR                          = 24

};

/* LLDP Error String */
#ifdef _LLDCLI_C
CONST CHR1  *gapc1LldpCliErrString[] = {
 NULL,
 "TxDelay should be less than or equal to (0.25 * Message Tx Interval)\r\n",
 "The Management address length should be equal to 512 bytes\r\n",
 "Tx can be enabled only when vlan name is configured for that vlan\r\n",
 "LLDP cannot be started while shutdown is in progress\r\n",
 "LLDP cannot be enabled/disabled when global shutwhile timer is running\r\n",
 "Default Vlan Interface is not available\r\n",
 "LLDP cannot be enabled/disabled in Transparent Mode\r\n",
 "Management address not available or oper status is down\r\n",
 "Dest-Mac address cannot be configured for lldp version1\r\n",
 "TxDelay cannot be configured for lldp version v2\r\n",
 "Link aggregation TLV cannot be set on ICCL\r\n",
 "Vlan Name TLV can be configured only after configuring Vlan name\r\n",
 "Dest-Mac address not configured\r\n",
 "Maximum LLDP DEST-MAC Configured\r\n",
 "Unknown policy flag cannot be set for Network Connectivity Device\r\n",
 "Invalid Destination Mac address\r\n",
 "Invalid length: Co-ordinate Location Id length should be 16\r\n",
 "Invalid length: Civic Location Id length should be 6 to 256\r\n",
 "Invalid length: Elin Location Id length should be 10 to 25\r\n",
 "Invalid Ip Address\r\n",
 "EVB-CDCP is still running\r\n",
 "Invalid Ipv6 Address\r\n",
 "Multiple agents cannot be configured when DCBX application registered\r\n",
 NULL
};
#endif


/* LLDP Admin Status */
enum
{
    LLDP_CLI_TRANSMIT     = 1,
    LLDP_CLI_RECEIVE      = 2,
    LLDP_CLI_NO_TRANSMIT  = 3,
    LLDP_CLI_NO_RECEIVE   = 4,
    LLDP_MAX_CLI_STATE    = 5

};


/* LLDP Show Neighbor */
enum
{
    LLDP_CLI_SHOW_NEIGBR_MSAP_INTF_DETL = 1,
    LLDP_CLI_SHOW_NEIGBR_MSAP_INTF      = 2,
    LLDP_CLI_SHOW_NEIGBR_MSAP_DETL      = 3,
    LLDP_CLI_SHOW_NEIGBR_INTF_DETL      = 4,
    LLDP_CLI_SHOW_NEIGBR_MSAP           = 5,
    LLDP_CLI_SHOW_NEIGBR_INTF           = 6,
    LLDP_CLI_SHOW_ALL_NEIGBR_DETL       = 7,
    LLDP_CLI_SHOW_ALL_NEIGBR            = 8,
    LLDP_MAX_CLI_SHOW_NEIGBR            = 9
};

/* LLDP Show Mac Address Option */
enum
{
    LLDP_CLI_SHOW_MAC       = 1,
    LLDP_CLI_SHOW_INTERFACE = 2,
    LLDP_CLI_SHOW_ALL       = 3
};
/* Command Identifiers */
enum
{
    LLDP_CLI_SYS_CTRL                = 1,
    LLDP_CLI_MOD_STAT                = 2,
    LLDP_CLI_TX_INTRVL               = 3,
    LLDP_CLI_NO_TX_INTRVL            = 4,
    LLDP_CLI_HOLD_MULPR              = 5,
    LLDP_CLI_NO_HOLD_MULPR           = 6,
    LLDP_CLI_REINIT_DELAY            = 7,
    LLDP_CLI_NO_REINIT_DELAY         = 8,
    LLDP_CLI_TX_DELAY                = 9,
    LLDP_CLI_NO_TX_DELAY             = 10,
    LLDP_CLI_NOTIFY_INTRVL           = 11,
    LLDP_CLI_NO_NOTIFY_INTRVL        = 12,
    LLDP_CLI_CHASSIS_SUBTYPE         = 13,
    LLDP_CLI_CLEAR_CNTRS             = 14,
    LLDP_CLI_CLEAR_TABLE             = 15,
    LLDP_CLI_DEBUG_SHOW              = 16,
    LLDP_CLI_DEBUG                   = 17,
    LLDP_CLI_SHOW_GLOBAL             = 18,
    LLDP_CLI_SHOW_INTF               = 19,
    LLDP_CLI_SHOW_NEIGBR             = 20,
    LLDP_CLI_SHOW_TRAFFIC            = 21,
    LLDP_CLI_SHOW_LOCAL              = 22,
    LLDP_CLI_SHOW_ERRORS             = 23,
    LLDP_CLI_SHOW_STATS              = 24,
    LLDP_CLI_TRANS_RECV              = 25,
    LLDP_CLI_SET_NOTIFY              = 26,
    LLDP_CLI_TLV_SEL                 = 27,
    LLDP_CLI_NO_TLV_SEL              = 28,
    LLDP_CLI_PORT_SUBTYPE            = 29,
    LLDP_CLI_DOT1TLV_SEL             = 30,
    LLDP_CLI_NO_DOT1TLV_SEL          = 31,
    LLDP_CLI_DOT3TLV_SEL             = 32,
    LLDP_CLI_NO_DOT3TLV_SEL          = 33,
    LLDP_CLI_SET_DST_MAC             = 34,
    LLDP_CLI_RESET_DST_MAC           = 35,
    LLDP_MAX_CLI_CMD_IDENTIFY        = 36,
    LLDP_CLI_MOD_VERSION             = 37,
    LLDP_TX_CREDIT_MAX               = 38,
    LLDP_MESSAGE_FAST_TX             = 39,
    LLDP_TX_FAST_INIT                = 40,
    LLDP_CLI_SHOW_PEER               = 41,
    LLDP_MED_CLI_TLV_SEL             = 42,
    LLDP_MED_CLI_NO_TLV_SEL          = 43,
    LLDP_MED_CLI_NW_POLICY_CONFIG    = 44,
    LLDP_MED_CLI_NO_NW_POLICY_CONFIG = 45,
    LLDP_MED_CLI_LOC_CONFIG          = 47,
    LLDP_MED_CLI_NO_LOC_CONFIG       = 48,
    LLDP_MED_CLI_COORDINATE_CONFIG   = 49,
    LLDP_MED_CLI_CIVIC_COUNTRY       = 50,
    LLDP_MED_CLI_CIVIC_LOC           = 51,
    LLDP_MED_CLI_CIVIC_ACTIVE        = 52,
    LLDP_MED_CLI_ELIN_CONFIG         = 53,
    LLDP_MED_CLI_MOD_STAT            = 54,
    LLDP_CLI_TRANSMIT_TAG            = 55,
    LLDP_CLI_SET_MGMT_ADDR           = 56,
    LLDP_MED_FAST_REPEAT_COUNT       = 57
};

/* Function Prototypes */

PUBLIC INT4
cli_process_lldp_cmd PROTO((tCliHandle CliHandle, UINT4 u4Command, ...));
PUBLIC INT4
LldpCliSetSystemCtrl PROTO((tCliHandle CliHandle, INT4 i4Status));
PUBLIC INT4
LldpCliSetModuleStatus PROTO((tCliHandle CliHandle, INT4 i4Status));
PUBLIC INT4
LldpCliSetTransmitInterval PROTO((tCliHandle CliHandle, INT4 i4Interval));
PUBLIC INT4
LldpCliSetHoldtimeMuliplier PROTO((tCliHandle CliHandle, INT4 i4Val));
PUBLIC INT4
LldpCliSetReinitDelay PROTO((tCliHandle CliHandle, INT4 i4ReinitDelay));
PUBLIC INT4
LldpCliSetTransmitDelay PROTO((tCliHandle CliHandle, INT4 i4TxDelay));
PUBLIC INT4
LldpCliSetNotifyInterval PROTO((tCliHandle CliHandle, INT4 i4NotifyInterval));
PUBLIC INT4
LldpCliSetChassisSubtype PROTO((tCliHandle CliHandle, INT4 i4ChassisIdSubtype,
                                UINT1 *pu1ChassisId));
PUBLIC INT4
LldpCliShowDebugging PROTO((tCliHandle CliHandle));
PUBLIC INT4
LldpCliUpdTraceInput PROTO((tCliHandle CliHandle, UINT1 *pu1TraceInput));
PUBLIC INT4
LldpCliClearCounters PROTO((tCliHandle CliHandle));
PUBLIC INT4
LldpCliClearTable PROTO((tCliHandle CliHandle));
PUBLIC INT4
LldpCliShowGlobal PROTO((tCliHandle CliHandle));
PUBLIC INT4
LldpCliShowInterface PROTO((tCliHandle CliHandle, INT4 i4IfIndex, UINT1 *pu1dstmac, UINT1 u1ShowOpt));
PUBLIC INT4
LldpCliShowOpt PROTO ((tCliHandle CliHandle, INT4 i4IfIndex, UINT1 *pu1MacAddr, UINT1 *pu1ShowOpt));
PUBLIC INT4
LldpCliShowNeighbor PROTO((tCliHandle CliHandle, INT4 i4Option,
                           INT4 i4IfIndex, UINT1 *pu1ChassisId,
                           UINT1 *pu1PortId, UINT1 *pau1DestMacAddr));
PUBLIC INT4
LldpCliShowTraffic (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 *pDestMac, UINT1 u1ShowOpt);

PUBLIC INT4
LldpCliShowLocal (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4Option, UINT1 u1ShowOption, UINT4 u4DestMac);

PUBLIC INT4
LldpCliShowLocalManAddr PROTO ((tCliHandle CliHandle));

PUBLIC INT4
LldpCliShowErrors PROTO ((tCliHandle CliHandle));

PUBLIC INT4
LldpCliShowStatistics PROTO ((tCliHandle CliHandle));
    
PUBLIC INT4
LldpCliSetAdminStatus PROTO ((tCliHandle CliHandle, INT4 i4IfIndex,
                              INT4 i4AdminStatus, UINT4 u4DestMacAddrIndex,
                              UINT1 u1InterfaceFlag));
PUBLIC INT4
LldpCliSetNotification PROTO ((tCliHandle CliHandle, INT4 i4IfIndex, 
                               INT4 i4Status, INT4 i4NotifyType, 
                               UINT4 u4DestMacAddrIndex, UINT1 u1InterfaceFlag));
PUBLIC INT4
LldpCliSetAllVlanNameTxVal PROTO ((INT4 i4IfIndex, INT4 i4TxStatusVal));

PUBLIC INT4
LldpCliSetAllProtoVlanIdTxVal PROTO ((INT4 i4IfIndex, INT4 i4TxStatusVal));

PUBLIC INT4
LldpCliSetTLV PROTO ((tCliHandle CliHandle, INT4 i4IfIndex, UINT1 *pu1Tlv,
                      INT4 i4MgmtAddrSubType, UINT1 *pu1ManAddr, 
                      UINT4 u4DestMacAddrIndex, UINT1 u1InterfaceFlag));
PUBLIC INT4
LldpCliResetTLV PROTO ((tCliHandle CliHandle, INT4 i4IfIndex, UINT1 *pu1Tlv,
                        INT4 i4MgmtAddrSubType, UINT1 *pu1ManAddr,
                        UINT4 u4DestMacAddrIndex, UINT1 u1InterfaceFlag));
PUBLIC INT4
LldpCliSetPortSubtype PROTO ((tCliHandle CliHandle, INT4 i4IfIndex,
                              INT4 i4PortIdSubtype, UINT1 *pu1PortId));
/*PUBLIC INT4
LldpCliSetDot1TLV PROTO ((tCliHandle CliHandle, INT4 i4IfIndex,
                          INT4 i4PortVlanId, UINT2 *pu2ProtoVlanId,
                          UINT1 *pu1VlanName, INT4 i4ProtoVlanAll,
                          INT4 i4VlanNameAll, INT4 i4SetVal));*/
PUBLIC INT4
LldpCliSetDot1TLV (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4PortVlanId,
                   UINT2 *pu2ProtoVlanId, UINT1 *pu1VlanName,
                   INT4 i4ProtoVlanAll, INT4 i4VlanNameAll, INT4 i4SetVal, INT4 i4VidDigest,
                   INT4 i4MgmtVidDigest, INT4 i4LinkAggTLV, UINT4 u4DestMacAddrIndex,
                   UINT1 u1InterfaceFlag);
PUBLIC INT4
LldpCliSetDot3TLV PROTO ((tCliHandle CliHandle, INT4 i4IfIndex,
                          UINT1 u1Dot3Tlv, INT4 i4SetVal));


INT4 LldpShowRunningConfig(tCliHandle,UINT4);
VOID LldpShowRunningConfigScalars(tCliHandle);
VOID LldpShowRunningConfigInterface(tCliHandle);
VOID LldpShowRunningConfigPhysicalInterfaceDetails(tCliHandle,INT4);
VOID LldpShowRunningConfigTLVDetails (tCliHandle,INT4);
VOID LldpShowRunningConfigBasicTLVDetails (tCliHandle,INT4);

PUBLIC INT4
LldpCliSetDstMac (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 *pu1MacAddr, UINT1 u1RowStatus);

PUBLIC INT4
LldpCliSetModuleVersion (tCliHandle CliHandle, INT4 i4Status);

PUBLIC INT4
LldpCliSetTxCreditMax (tCliHandle CliHandle, UINT4 u4Val);

PUBLIC INT4
LldpCliSetTxFast (tCliHandle CliHandle, UINT4 u4Val);

PUBLIC INT4
LldpCliSetTxFastInit (tCliHandle CliHandle, UINT4 u4Val);

PUBLIC INT4
LldpCliSetMedFastRepeatCount (tCliHandle CliHandle, UINT4 u4Val);

PUBLIC INT4
LldpCliSetMessageFastTx (tCliHandle CliHandle, UINT4 u4Val);

PUBLIC INT4
LldpCliGetMacAddrIndex (tCliHandle CliHandle, INT4 i4IfIndex,
                        UINT1 *pau1MacAddr, UINT4 *pu4DestMacAddrIndex,
                        UINT1 *pu1InterfaceFlag);
PUBLIC INT4 cli_process_lldp_test_cmd PROTO ((tCliHandle CliHandle,...));



PUBLIC INT4
LldpMedCliSetTlv (tCliHandle CliHandle, 
       INT4 i4IfIndex,
              UINT2 u2optTLV, 
       UINT4 u4DestMacAddrIndex, 
       UINT1 u1InterfaceFlag, 
              INT4 i4SetVal);


PUBLIC INT4
LldpMedCliSetNwPolicyConfig (tCliHandle CliHandle,
                           INT4  i4IfIndex,
                           UINT1 u1AppType,
                           BOOL1 bPolicyUnknown,
                           BOOL1 bVlanType,
                           UINT4 u4VlanId,
                           UINT4 u4Priority,
                           UINT4 u4dscp);


PUBLIC INT4
LldpMedCliDelNwPolicyConfig (tCliHandle CliHandle,
                           INT4  i4IfIndex,
                           UINT1 u1AppType);


PUBLIC VOID
LldpMedCliShowInventoryInfo(tCliHandle CliHandle);


PUBLIC VOID
LldpMedCliShowPortConfigInfo (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4DestMacIndex);

PUBLIC VOID
LldpMedCliShowRemCapabilitiesTable(tCliHandle CliHandle,
                                   UINT4 u4RemTimeMark,
                                   INT4 i4RemLocalPortNum,
                                   UINT4 u4RemDestMacAddr,
                                   INT4 i4RemIndex);

PUBLIC VOID
LldpMedCliShowRemInventoryTable(tCliHandle CliHandle,
                                UINT4 u4RemTimeMark,
                                INT4 i4RemLocalPortNum,
                                UINT4 u4RemDestMacAddr,
                                INT4 i4RemIndex);

PUBLIC VOID
LldpMedCliShowPolicyInfo (tCliHandle CliHandle, INT4 i4IfIndex);

PUBLIC VOID
LldpMedCliShowLocationInfo (tCliHandle CliHandle, INT4 i4IfIndex);

PUBLIC VOID
LldpMedCliShowPowerInfo (tCliHandle CliHandle, INT4 i4IfIndex);

PUBLIC VOID
LldpMedCliShowRemPolicyTable(tCliHandle CliHandle,
                             UINT4 u4RemTimeMark,
                             INT4 i4RemLocalPortNum,
                             UINT4 u4RemDestMacAddr,
                             INT4 i4RemIndex);

PUBLIC VOID
LldpMedCliShowRemLocationInfo(tCliHandle CliHandle,
                             UINT4 u4RemTimeMark,
                             INT4 i4RemLocalPortNum,
                             UINT4 u4RemDestMacAddr,
                             INT4 i4RemIndex);

PUBLIC VOID
LldpMedCliShowRemPowerInfo(tCliHandle CliHandle,
                           UINT4 u4RemTimeMark,
                           INT4 i4RemLocalPortNum,
                           UINT4 u4RemDestMacAddr,
                           INT4 i4RemIndex);
VOID
LldpMedShowRunningConfigNwPol (tCliHandle CliHandle, INT4 i4Index);

VOID
LldpMedShowRunningConfigTLVInfo (tCliHandle CliHandle, INT4 i4Index);

VOID
LldpMedShowRunningConfigLocInfo (tCliHandle CliHandle, INT4 i4Index);

PUBLIC INT4
LldpMedCliSetLocConfig (tCliHandle CliHandle,
                  INT4 i4IfIndex,
                  INT4 i4LocType,
                  UINT1 *pu1LocInfo);

PUBLIC INT4
LldpMedCliSetElinConfig (tCliHandle CliHandle,
                  INT4 i4IfIndex,
                  UINT1 *pu1LocInfo);

INT1
LldpGetCivicLocPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

PUBLIC INT4
LldpMedCliDelLocConfig (tCliHandle CliHandle,
                  INT4 i4IfIndex,
                  INT4 i4LocType);

PUBLIC INT4
LldpMedCliSetModuleStatus (tCliHandle CliHandle, 
                      INT4 i4IfIndex, 
                      INT4 i4Status);

VOID
LldpMedShowRunningAdminStatus PROTO((tCliHandle CliHandle, 
                               INT4 i4Index));

PUBLIC INT4
LldpMedCliSetCoordinateLocConfig (tCliHandle CliHandle,
                            INT4 i4IfIndex,
                            UINT1 u1latres,
                            UINT1 *u1latitude,
                            UINT1 u1longres,
                            UINT1 *u1longitude,
                            UINT1 u1alttype,
                            UINT1 *u1altitude,
                            UINT1 u1altres,
                            UINT1 u1datum);

PUBLIC VOID
LldpMedUtlCoordinateLocEncode (UINT1 *u1latitude, 
                         UINT1 u1latres, 
                         UINT1 *u1longitude,
                         UINT1 u1longres, 
                         UINT1 u1alttype, 
                         UINT1 *u1altitude,
                         UINT1 u1altres, 
                         UINT1 u1datum, 
                         UINT1 au1lat[]);

PUBLIC VOID
LldpMedUtlCoordinateLocDecode (UINT1 *u1latres, 
                         DBL8 *d8latitude, 
                         UINT1 *u1longres,
                         DBL8 *d8longitude, 
                         UINT1 *u1alttype, 
                         UINT1 *u1altres,
                         DBL8 *d8altitude, 
                         UINT1 *u1datum, 
                         UINT1 au1LocationId[]);

PUBLIC INT4
LldpMedCliSetCountryCodeConfig (tCliHandle CliHandle,
                             INT4 i4IfIndex,
                             UINT1 *u1ContryCode);

PUBLIC INT4
LldpMedCliSetCaTypeConfig (tCliHandle CliHandle,
                     INT4 i4IfIndex,
                     UINT1 u1CaType,
                     UINT1 *pu1CaValue);

PUBLIC INT4
LldpCliDotStrToLocInfo (tCliHandle CliHandle,
                   UINT1 *pu1DotStr,
                   INT4 i4LocType,
                   UINT1 *pu1Mac);

PUBLIC INT4
LldpMedCliSetCivicActive (tCliHandle CliHandle,
                   INT4 i4IfIndex);

PUBLIC INT4
LldpMedCliParseLocOctetStr (tCliHandle CliHandle,
                      UINT1 *LocId);

PUBLIC VOID
LldpMedUtlBitArrayShift(UINT1 *pau1loc, 
                  UINT1 u1pos, 
                  UINT1 u1count);

PUBLIC CONST CHR1*
LldpMedUtlCaTypeToStr(UINT1 u1CaType);

PUBLIC CONST CHR1*
LldpMedUtlRowStatusToStr(INT4 i4RowStatus);

PUBLIC CONST CHR1*
LldpMedUtlCivicWhatToStr(UINT1 u1What);

VOID
LldpCliLocToStr (UINT1 *au1LocInfo,
           UINT1 *pu1Octet,
           UINT4 u4length);

PUBLIC INT4
LldpCliSetTagStatus(tCliHandle,INT4);
INT4
LldpCliSetManagementIpAddress (tCliHandle, INT4,UINT1 *);

INT4
LldpCliSetManagementIpv6Address (tCliHandle,INT4,tIp6Addr);

#endif /*LLD_CLI_H_*/

/*--------------------------------------------------------------------------*/
/*                         End of the file lldcli.h                        */
/*--------------------------------------------------------------------------*/
