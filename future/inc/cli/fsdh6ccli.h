/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsdh6ccli.h,v 1.4 2009/10/28 07:09:58 prabuc Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDhcp6ClntTrapAdminControl[11];
extern UINT4 FsDhcp6ClntDebugTrace[11];
extern UINT4 FsDhcp6ClntSysLogAdminStatus[11];
extern UINT4 FsDhcp6ClntListenPort[11];
extern UINT4 FsDhcp6ClntTransmitPort[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDhcp6ClntTrapAdminControl(pSetValFsDhcp6ClntTrapAdminControl)	\
	nmhSetCmn(FsDhcp6ClntTrapAdminControl, 11, FsDhcp6ClntTrapAdminControlSet,D6ClApiLock, D6ClApiUnLock, 0, 0, 0, "%s", pSetValFsDhcp6ClntTrapAdminControl)
#define nmhSetFsDhcp6ClntDebugTrace(pSetValFsDhcp6ClntDebugTrace)	\
	nmhSetCmn(FsDhcp6ClntDebugTrace, 11, FsDhcp6ClntDebugTraceSet,D6ClApiLock, D6ClApiUnLock, 0, 0, 0, "%s", pSetValFsDhcp6ClntDebugTrace)
#define nmhSetFsDhcp6ClntSysLogAdminStatus(i4SetValFsDhcp6ClntSysLogAdminStatus)	\
	nmhSetCmn(FsDhcp6ClntSysLogAdminStatus, 11, FsDhcp6ClntSysLogAdminStatusSet,D6ClApiLock, D6ClApiUnLock, 0, 0, 0, "%i", i4SetValFsDhcp6ClntSysLogAdminStatus)
#define nmhSetFsDhcp6ClntListenPort(i4SetValFsDhcp6ClntListenPort)	\
	nmhSetCmn(FsDhcp6ClntListenPort, 11, FsDhcp6ClntListenPortSet,D6ClApiLock, D6ClApiUnLock, 0, 0, 0, "%i", i4SetValFsDhcp6ClntListenPort)
#define nmhSetFsDhcp6ClntTransmitPort(i4SetValFsDhcp6ClntTransmitPort)	\
	nmhSetCmn(FsDhcp6ClntTransmitPort, 11, FsDhcp6ClntTransmitPortSet,D6ClApiLock, D6ClApiUnLock, 0, 0, 0, "%i", i4SetValFsDhcp6ClntTransmitPort)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDhcp6ClntIfIndex[13];
extern UINT4 FsDhcp6ClntIfDuidType[13];
extern UINT4 FsDhcp6ClntIfDuidIfIndex[13];
extern UINT4 FsDhcp6ClntIfMaxRetCount[13];
extern UINT4 FsDhcp6ClntIfMaxRetDelay[13];
extern UINT4 FsDhcp6ClntIfMaxRetTime[13];
extern UINT4 FsDhcp6ClntIfInitRetTime[13];
extern UINT4 FsDhcp6ClntIfMinRefreshTime[13];
extern UINT4 FsDhcp6ClntIfRealmName[13];
extern UINT4 FsDhcp6ClntIfKey[13];
extern UINT4 FsDhcp6ClntIfKeyId[13];
extern UINT4 FsDhcp6ClntIfCounterRest[13];
extern UINT4 FsDhcp6ClntIfRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDhcp6ClntIfDuidType(i4FsDhcp6ClntIfIndex ,i4SetValFsDhcp6ClntIfDuidType)	\
	nmhSetCmn(FsDhcp6ClntIfDuidType, 13, FsDhcp6ClntIfDuidTypeSet,D6ClApiLock, D6ClApiUnLock, 0, 0, 1, "%i %i", i4FsDhcp6ClntIfIndex ,i4SetValFsDhcp6ClntIfDuidType)
#define nmhSetFsDhcp6ClntIfDuidIfIndex(i4FsDhcp6ClntIfIndex ,i4SetValFsDhcp6ClntIfDuidIfIndex)	\
	nmhSetCmn(FsDhcp6ClntIfDuidIfIndex, 13, FsDhcp6ClntIfDuidIfIndexSet,D6ClApiLock, D6ClApiUnLock, 0, 0, 1, "%i %i", i4FsDhcp6ClntIfIndex ,i4SetValFsDhcp6ClntIfDuidIfIndex)
#define nmhSetFsDhcp6ClntIfMaxRetCount(i4FsDhcp6ClntIfIndex ,i4SetValFsDhcp6ClntIfMaxRetCount)	\
	nmhSetCmn(FsDhcp6ClntIfMaxRetCount, 13, FsDhcp6ClntIfMaxRetCountSet,D6ClApiLock, D6ClApiUnLock, 0, 0, 1, "%i %i", i4FsDhcp6ClntIfIndex ,i4SetValFsDhcp6ClntIfMaxRetCount)
#define nmhSetFsDhcp6ClntIfMaxRetDelay(i4FsDhcp6ClntIfIndex ,i4SetValFsDhcp6ClntIfMaxRetDelay)	\
	nmhSetCmn(FsDhcp6ClntIfMaxRetDelay, 13, FsDhcp6ClntIfMaxRetDelaySet,D6ClApiLock, D6ClApiUnLock, 0, 0, 1, "%i %i", i4FsDhcp6ClntIfIndex ,i4SetValFsDhcp6ClntIfMaxRetDelay)
#define nmhSetFsDhcp6ClntIfMaxRetTime(i4FsDhcp6ClntIfIndex ,i4SetValFsDhcp6ClntIfMaxRetTime)	\
	nmhSetCmn(FsDhcp6ClntIfMaxRetTime, 13, FsDhcp6ClntIfMaxRetTimeSet,D6ClApiLock, D6ClApiUnLock, 0, 0, 1, "%i %i", i4FsDhcp6ClntIfIndex ,i4SetValFsDhcp6ClntIfMaxRetTime)
#define nmhSetFsDhcp6ClntIfInitRetTime(i4FsDhcp6ClntIfIndex ,i4SetValFsDhcp6ClntIfInitRetTime)	\
	nmhSetCmn(FsDhcp6ClntIfInitRetTime, 13, FsDhcp6ClntIfInitRetTimeSet,D6ClApiLock, D6ClApiUnLock, 0, 0, 1, "%i %i", i4FsDhcp6ClntIfIndex ,i4SetValFsDhcp6ClntIfInitRetTime)
#define nmhSetFsDhcp6ClntIfMinRefreshTime(i4FsDhcp6ClntIfIndex ,u4SetValFsDhcp6ClntIfMinRefreshTime)	\
	nmhSetCmn(FsDhcp6ClntIfMinRefreshTime,13,FsDhcp6ClntIfMinRefreshTimeSet,D6ClApiLock, D6ClApiUnLock, 0, 0, 1, "%i %u",i4FsDhcp6ClntIfIndex ,u4SetValFsDhcp6ClntIfMinRefreshTime)
#define nmhSetFsDhcp6ClntIfRealmName(i4FsDhcp6ClntIfIndex ,pSetValFsDhcp6ClntIfRealmName)	\
	nmhSetCmn(FsDhcp6ClntIfRealmName, 13, FsDhcp6ClntIfRealmNameSet,D6ClApiLock, D6ClApiUnLock, 0, 0, 1, "%i %s", i4FsDhcp6ClntIfIndex ,pSetValFsDhcp6ClntIfRealmName)
#define nmhSetFsDhcp6ClntIfKey(i4FsDhcp6ClntIfIndex ,pSetValFsDhcp6ClntIfKey)	\
	nmhSetCmn(FsDhcp6ClntIfKey, 13, FsDhcp6ClntIfKeySet,D6ClApiLock, D6ClApiUnLock, 0, 0, 1, "%i %s", i4FsDhcp6ClntIfIndex ,pSetValFsDhcp6ClntIfKey)
#define nmhSetFsDhcp6ClntIfKeyId(i4FsDhcp6ClntIfIndex ,u4SetValFsDhcp6ClntIfKeyId)	\
	nmhSetCmn(FsDhcp6ClntIfKeyId, 13, FsDhcp6ClntIfKeyIdSet,D6ClApiLock, D6ClApiUnLock, 0, 0, 1, "%i %u", i4FsDhcp6ClntIfIndex ,u4SetValFsDhcp6ClntIfKeyId)
#define nmhSetFsDhcp6ClntIfCounterRest(i4FsDhcp6ClntIfIndex ,i4SetValFsDhcp6ClntIfCounterRest)	\
	nmhSetCmn(FsDhcp6ClntIfCounterRest, 13, FsDhcp6ClntIfCounterRestSet,D6ClApiLock, D6ClApiUnLock, 0, 0, 1, "%i %i", i4FsDhcp6ClntIfIndex ,i4SetValFsDhcp6ClntIfCounterRest)
#define nmhSetFsDhcp6ClntIfRowStatus(i4FsDhcp6ClntIfIndex ,i4SetValFsDhcp6ClntIfRowStatus)	\
	nmhSetCmn(FsDhcp6ClntIfRowStatus, 13, FsDhcp6ClntIfRowStatusSet,D6ClApiLock, D6ClApiUnLock, 0, 1, 1, "%i %i", i4FsDhcp6ClntIfIndex ,i4SetValFsDhcp6ClntIfRowStatus)

#endif
