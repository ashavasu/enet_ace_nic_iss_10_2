/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsradicli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 RadiusExtDebugMask[10];
extern UINT4 RadiusMaxNoOfUserEntries[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetRadiusExtDebugMask(i4SetValRadiusExtDebugMask)	\
	nmhSetCmn(RadiusExtDebugMask, 10, RadiusExtDebugMaskSet, RadiusLock, RadiusUnLock, 0, 0, 0, "%i", i4SetValRadiusExtDebugMask)
#define nmhSetRadiusMaxNoOfUserEntries(i4SetValRadiusMaxNoOfUserEntries)	\
	nmhSetCmn(RadiusMaxNoOfUserEntries, 10, RadiusMaxNoOfUserEntriesSet, RadiusLock, RadiusUnLock, 0, 0, 0, "%i", i4SetValRadiusMaxNoOfUserEntries)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 RadiusExtServerAddress[12];
extern UINT4 RadiusExtServerType[12];
extern UINT4 RadiusExtServerSharedSecret[12];
extern UINT4 RadiusExtServerEnabled[12];
extern UINT4 RadiusExtServerResponseTime[12];
extern UINT4 RadiusExtServerMaximumRetransmission[12];
extern UINT4 RadiusExtServerEntryStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetRadiusExtServerAddress(i4RadiusExtServerIndex ,u4SetValRadiusExtServerAddress)	\
	nmhSetCmn(RadiusExtServerAddress, 12, RadiusExtServerAddressSet, RadiusLock, RadiusUnLock, 0, 0, 1, "%i %p", i4RadiusExtServerIndex ,u4SetValRadiusExtServerAddress)
#define nmhSetRadiusExtServerType(i4RadiusExtServerIndex ,i4SetValRadiusExtServerType)	\
	nmhSetCmn(RadiusExtServerType, 12, RadiusExtServerTypeSet, RadiusLock, RadiusUnLock, 0, 0, 1, "%i %i", i4RadiusExtServerIndex ,i4SetValRadiusExtServerType)
#define nmhSetRadiusExtServerSharedSecret(i4RadiusExtServerIndex ,pSetValRadiusExtServerSharedSecret)	\
	nmhSetCmn(RadiusExtServerSharedSecret, 12, RadiusExtServerSharedSecretSet, RadiusLock, RadiusUnLock, 0, 0, 1, "%i %s", i4RadiusExtServerIndex ,pSetValRadiusExtServerSharedSecret)
#define nmhSetRadiusExtServerEnabled(i4RadiusExtServerIndex ,i4SetValRadiusExtServerEnabled)	\
	nmhSetCmn(RadiusExtServerEnabled, 12, RadiusExtServerEnabledSet, RadiusLock, RadiusUnLock, 0, 0, 1, "%i %i", i4RadiusExtServerIndex ,i4SetValRadiusExtServerEnabled)
#define nmhSetRadiusExtServerResponseTime(i4RadiusExtServerIndex ,i4SetValRadiusExtServerResponseTime)	\
	nmhSetCmn(RadiusExtServerResponseTime, 12, RadiusExtServerResponseTimeSet, RadiusLock, RadiusUnLock, 0, 0, 1, "%i %i", i4RadiusExtServerIndex ,i4SetValRadiusExtServerResponseTime)
#define nmhSetRadiusExtServerMaximumRetransmission(i4RadiusExtServerIndex ,i4SetValRadiusExtServerMaximumRetransmission)	\
	nmhSetCmn(RadiusExtServerMaximumRetransmission, 12, RadiusExtServerMaximumRetransmissionSet, RadiusLock, RadiusUnLock, 0, 0, 1, "%i %i", i4RadiusExtServerIndex ,i4SetValRadiusExtServerMaximumRetransmission)
#define nmhSetRadiusExtServerEntryStatus(i4RadiusExtServerIndex ,i4SetValRadiusExtServerEntryStatus)	\
	nmhSetCmn(RadiusExtServerEntryStatus, 12, RadiusExtServerEntryStatusSet, RadiusLock, RadiusUnLock, 0, 1, 1, "%i %i", i4RadiusExtServerIndex ,i4SetValRadiusExtServerEntryStatus)

#endif
