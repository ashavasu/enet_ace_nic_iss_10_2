/********************************************************************
 *  * Copyright (C) Future Sotware,1997-98,2001
 *  *
 *  * $Id: sntpcli.h,v 1.23 2017/12/29 09:31:52 siva Exp $
 *  *
 *  * Description: This file contains the constants, gloabl structures, 
 *    typedefs, enums uased in  SNTP  module.
 *  *
 *  *******************************************************************/


#ifndef __SNTPCLI_H__
#define __SNTPCLI_H__

#include "ip.h"
#include "ip6util.h"
#include "cli.h"

/* Opcodes for CLI commands.
 * When a new command is added, add a new definition */

#define SNTP_ENABLE      1
#define SNTP_DISABLE     0

#define SNTP_CLI_SET_STATUS           1  /* Set SNTP (Enable/Diasble)Status */
#define SNTP_CLI_SET_VERSION          2
#define SNTP_CLI_SET_ADDR_MODE        3  /* Set SNTP client address mode*/
#define SNTP_CLI_SET_LISTENING_PORT   4  /* Set SNTP Client Listining port*/
#define SNTP_CLI_SET_DEFAULT_PORT     5  /* Set Default port for SNTP client*/
#define SNTP_CLI_SET_CLOCK            6
#define SNTP_CLI_SET_CLOCK_FORMAT     7 
#define SNTP_CLI_SET_TIME_ZONE        8   /* Set SNTP Time Zone */
#define SNTP_CLI_SET_NO_TIME_ZONE     9   /* Set SNTP Time Zone UTC */
#define SNTP_CLI_SET_SUMMER_TIME      10  /* Set Clock Sumer Time */
#define SNTP_CLI_STOP_SUMMER_TIME     11   /* Stop Clock Summer Time */
#define SNTP_CLI_SET_AUTH       12   /* Set SNTP Authentication */
#define SNTP_CLI_SET_NO_AUTH       13   /* Set SNTP Authentication */

#define SNTP_CLI_SET_AUTODISCOVER_STATUS                 14
#define SNTP_CLI_SET_UNICAST_POLL_INTERVAL               15
#define SNTP_CLI_SET_UNICAST_POLL_INTERVAL_TIMEOUT       16
#define SNTP_CLI_SET_MAX_UNICAST_POLL_RETRY              17
#define SNTP_CLI_SET_UNICAST_SERVER_OPTIONS              18
#define SNTP_CLI_NO_UNICAST_SERVER_OPTIONS               19

#define SNTP_CLI_SET_BCAST_SEND_REQUEST_STATUS           20 
#define SNTP_CLI_SET_BCAST_POLL_TIMEOUT                  21
#define SNTP_CLI_SET_BCAST_DELAY_TIME                    22

#define SNTP_CLI_SET_MCAST_SEND_REQUEST_STATUS           23
#define SNTP_CLI_SET_MCAST_POLL_TIMEOUT                  24
#define SNTP_CLI_SET_MCAST_DELAY_TIME                    25
#define SNTP_CLI_MCAST_SERVER_ADDRESS                    26

#define SNTP_CLI_SET_ACAST_POLL_INTERVAL                 27
#define SNTP_CLI_SET_ACAST_POLL_TIMEOUT                  28
#define SNTP_CLI_ACAST_MAX_RETRY_COUNT                   29
#define SNTP_CLI_SET_ACAST_BCAST_SERVER                  30 
#define SNTP_CLI_SET_ACAST_IPV4_MCAST_SERVER             31 
#define SNTP_CLI_SET_ACAST_IPV6_MCAST_SERVER             32

#define SNTP_CLI_SHOW_TIME                               33   /* Show current time */
#define SNTP_CLI_SHOW_STATUS                             34  /* Show SNTP status */
#define SNTP_CLI_SHOW_UNICAST_SERVER_INFO                35
#define SNTP_CLI_SHOW_BROADCAST_SERVER_INFO              36
#define SNTP_CLI_SHOW_MULTICAST_SERVER_INFO              37
#define SNTP_CLI_SHOW_ANYCAST_SERVER_INFO                38
#define SNTP_CLI_TRACE                                   39
#define SNTP_CLI_SHOW_STATS                              40


#define SNTP_CLI_MAX_COMMANDS         41

#define CLI_SNTP_TRACE_DIS_FLAG       0x80000000
#define SNTP_ALL_TRC                  0x00000fff

/*Sntp Status*/
#define SNTP_GREATER                  2
#define SNTP_LESSER                   4
#define SNTP_EQUAL                    8

#define SNTP_CLI_ACAST_DEFAULT_MCAST_IPV4_SERVER 1 
#define SNTP_CLI_ACAST_DEFAULT_MCAST_IPV6_SERVER 1 

#define SNTP_SEND_REQUEST_BCAST_MODE  1
#define SNTP_NO_REQUEST_BCAST_MODE    0
 
#define SNTP_SEND_REQUEST_MCAST_MODE  1
#define SNTP_NO_REQUEST_MCAST_MODE    0


#define SNTP_CLI_MODE                 "sntp"

/* sntpcli.c MACROS */
#define SNTP_AUTH_NONE 0
#define SNTP_AUTH_MD5  1
#define SNTP_AUTH_DES  2
#define SNTP_ZERO 0
#define SNTP_INVALID_KEY_LEN 0
#define SNTP_SUCCESS SUCCESS
#define SNTP_FAILURE FAILURE
#define SNTP_MIN_KEYID 0
#define SNTP_MAX_KEYID 65535
#define SNTP_TIME_ZONE_MIN_HOURS 0
#define SNTP_TIME_ZONE_MAX_HOURS 12
#define SNTP_TIME_ZONE_MIN_MINS  0 
#define SNTP_TIME_ZONE_MAX_MINS  59

/*Daylight Saving specific constants*/

#define CLI_SNTP_V1          1 
#define CLI_SNTP_V2          2 
#define CLI_SNTP_V3          3 
#define CLI_SNTP_V4          4 


#define CLI_SNTP_UNICAST     1 
#define CLI_SNTP_BROADCAST   2 
#define CLI_SNTP_MULTICAST   3 
#define CLI_SNTP_ANYCAST     4 

#define CLI_SNTP_FORMAT_AMPM  2
#define CLI_SNTP_FORMAT_HOURS 1
#define CLI_SNTP_AUTODISCOVER_ENABLE  1
#define CLI_SNTP_AUTODISCOVER_DISABLE 0

#define CLI_SNTP_BROADCAST_SITELOCAL  7
#define CLI_SNTP_BROADCAST_GLOBAL     8
#define CLI_SNTP_ANYCAST_BROADCAST    9
#define CLI_SNTP_ANYCAST_MULTICAST    10



#define SNTP_DST_MIN_DAY      0
#define SNTP_DST_MAX_DAY      6 

#define SNTP_DST_MIN_WEEK     1
#define SNTP_DST_MAX_WEEK     5 

#define SNTP_DST_MIN_MONTH    0
#define SNTP_DST_MAX_MONTH    11 

#define SNTP_DST_MIN_TIME     0
#define SNTP_DST_MAX_TIME     23


#define SNTP_DST_MIN_TIME_MNS    0
#define SNTP_DST_MAX_TIME_MNS    59
#define SNTP_DST_STRT_MON_NH_MIN 0
#define SNTP_DST_STRT_MON_NH_MAX 2
#define SNTP_DST_END_MON_NH_MIN 6
#define SNTP_DST_END_MON_NH_MAX 10
#define SNTP_DST_STRT_MON_SH_MIN 7
#define SNTP_DST_STRT_MON_SH_MAX 11
#define SNTP_DST_END_MON_SH_MIN 0
#define SNTP_DST_END_MON_SH_MAX 4


#define SNTP_NO_DST_FALL     0
#define SNTP_DST_FALL     1

/* Size constants */
#define SHOWTIME_OBJECTS 2
#define SNTP_CLI_SHOWTIME_SIZE (CLI_MAX_COLUMN_WIDTH * SHOWTIME_OBJECTS)

/* #ifdef DNS_RESOLVER_WANTED */
#define DEST_ADDRESS            0
#define DEST_NAME               1
#define SNTP_CLIENT_NAME_SIZE   128
#define MAX_ADDR_BUFFER         256
#define SNTP_CLI_MAX_KEYID      65535  

#define SNTP_DNS_FAMILY 16

enum {
    CLI_SNTP_ERR_SNTP_CLIENT_NOT_READY = 1,
    CLI_SNTP_ERR_SNTP_CLIENT_READY,
    CLI_SNTP_SERVER_IP_ADDRESS_NOT_CONFIGURED,
    CLI_SNTP_ERR_INVALID_STATUS,
    CLI_SNTP_ERR_INVALID_AUTH_TYPE,
    CLI_SNTP_ERR_AUTH_TYPE_MD5,
    CLI_SNTP_ERR_AUTH_TYPE_DES,
    CLI_SNTP_ERR_AUTH_TYPE_NULL,
    CLI_SNTP_ERR_INVALID_AUTH_KEY,
    CLI_SNTP_ERR_INVALID_AUTH_KEYID,
    CLI_SNTP_ERR_INVALID_POLL_INTERVAL,
    CLI_SNTP_ERR_INVALID_IP_ADDR,
    CLI_SNTP_ERR_INVALID_TIME_FLAG,
    CLI_SNTP_ERR_INVALID_TIME_HRS,
    CLI_SNTP_ERR_INVALID_TIME,
    CLI_SNTP_ERR_TIME_DIFF,
    CLI_SNTP_ERR_INVALID_TIME_MINS,
    CLI_SNTP_ERR_INVALID_CMD,
    CLI_SNTP_ERR_ANYCAST_NOSUPPORT, 
    CLI_SNTP_ERR_ANYCAST_MODE, 
    CLI_SNTP_ERR_UCAST_POLL_NOT_EXPONENT_VALUE,
    CLI_SNTP_ERR_ANYCAST_POLL_NOT_EXPONENT_VALUE,
    CLI_SNTP_ERR_MAX_DOMAIN_LEN,
    CLI_SNTP_ERR_MAX_CLI_CMD
};

#ifdef __SNTPCLI_C__

CONST CHR1 *gau1SntpCliErrString [] = {
    NULL,
    "% SNTP Client is Not Running !\r\n",
    "% SNTP Client is Running !\r\n",
    "% SNTP Server IP Address is Not Configured !\r\n",
    "% Invalid Status !\r\n",
    "% Invalid SNTP Authentication Type !\r\n",
    "% SNTP Authentication Type :MD5  !\r\n",
    "% SNTP Authentication Type :DES !\r\n",
    "% SNTP Authentication Type :NULL !\r\n",
    "% Invalid Authentication Key !\r\n",
    "% Invalid Authentication Key ID!\r\n",
    "% Invalid Poll Interval Value !\r\n",
    "% Invalid IP Address !\r\n",
    "% Time Flags should be +/- !\r\n", 
    "% Time Zone Hours Should be >=0 and <=12 !\r\n", 
    "% Invalid time zone  !\r\n", 
    "% Time Zone difference should be <= 12:00 Hrs !\r\n",
    "% Time Zone Minutes should be >= 0 and <= 59  !\r\n",
    "% Invalid command !\r\n",
    "% Anycast mode is currently not supported !\r\n", 
    "% Addressing mode should be Manycast !\r\n", 
    "% Invalid Poll Interval Value. Value should be exponent of 2 !\r\n",
    "% Invalid Manycast Poll Interval Value. Value should be exponent of 2 !\r\n",
    "% Maximum domain name length exceeded \r\n",
    "\r\n"
};
#endif

extern INT4 IpVerifyGetAddrType PROTO ((t_IP *, UINT1));
/* prototypes */

INT4 cli_process_sntp_cmd           PROTO ((tCliHandle CliHandle,
                      UINT4 u4Command, ...));


INT1 SntpGetSntpConfigPrompt         PROTO ((INT1 *pi1ModeName, 
                                           INT1 *pi1DispStr));


INT4  SntpCliStopClient       PROTO ((VOID));
INT4  SntpCliSetAuth          PROTO ((tCliHandle, UINT1, UINT4, UINT1 *));
INT4  SntpCliSetNoAuth        PROTO ((tCliHandle, UINT1, UINT4 ));
INT4  SntpCliShowTime         PROTO ((tCliHandle ));
INT4  SntpCliShowStatus       PROTO ((tCliHandle ));
INT4  SntpCliShowStatistics   PROTO ((tCliHandle ));
INT4  SntpCliSetTimeZone      PROTO ((tCliHandle , UINT1 *));
INT4  SntpCliSetStatus        PROTO ((tCliHandle, UINT4 ));


INT4  SntpCliSetSummerTime    PROTO ((tCliHandle,  UINT1 *, UINT1 *));
                                      
INT4  SntpCliStopSummerTime   PROTO ((tCliHandle ));
INT4  SntpShowRunningConfig (tCliHandle CliHandle);
VOID  IssSntpShowDebugging(tCliHandle CliHandle);

#ifdef SNTP_TEST_WANTED
INT4 cli_process_sntp_test_cmd (tCliHandle CliHandle,...);
#endif

#endif
