/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdmplcli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 MplsInSegmentIndex[13];
extern UINT4 MplsInSegmentInterface[13];
extern UINT4 MplsInSegmentLabel[13];
extern UINT4 MplsInSegmentLabelPtr[13];
extern UINT4 MplsInSegmentNPop[13];
extern UINT4 MplsInSegmentAddrFamily[13];
extern UINT4 MplsInSegmentTrafficParamPtr[13];
extern UINT4 MplsInSegmentRowStatus[13];
extern UINT4 MplsInSegmentStorageType[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetMplsInSegmentInterface(pMplsInSegmentIndex ,i4SetValMplsInSegmentInterface)	\
	nmhSetCmn(MplsInSegmentInterface, 13, MplsInSegmentInterfaceSet, NULL, NULL, 0, 0, 1, "%s %i", pMplsInSegmentIndex ,i4SetValMplsInSegmentInterface)
#define nmhSetMplsInSegmentLabel(pMplsInSegmentIndex ,u4SetValMplsInSegmentLabel)	\
	nmhSetCmn(MplsInSegmentLabel, 13, MplsInSegmentLabelSet, NULL, NULL, 0, 0, 1, "%s %u", pMplsInSegmentIndex ,u4SetValMplsInSegmentLabel)
#define nmhSetMplsInSegmentLabelPtr(pMplsInSegmentIndex ,pSetValMplsInSegmentLabelPtr)	\
	nmhSetCmn(MplsInSegmentLabelPtr, 13, MplsInSegmentLabelPtrSet, NULL, NULL, 0, 0, 1, "%s %o", pMplsInSegmentIndex ,pSetValMplsInSegmentLabelPtr)
#define nmhSetMplsInSegmentNPop(pMplsInSegmentIndex ,i4SetValMplsInSegmentNPop)	\
	nmhSetCmn(MplsInSegmentNPop, 13, MplsInSegmentNPopSet, NULL, NULL, 0, 0, 1, "%s %i", pMplsInSegmentIndex ,i4SetValMplsInSegmentNPop)
#define nmhSetMplsInSegmentAddrFamily(pMplsInSegmentIndex ,i4SetValMplsInSegmentAddrFamily)	\
	nmhSetCmn(MplsInSegmentAddrFamily, 13, MplsInSegmentAddrFamilySet, NULL, NULL, 0, 0, 1, "%s %i", pMplsInSegmentIndex ,i4SetValMplsInSegmentAddrFamily)
#define nmhSetMplsInSegmentTrafficParamPtr(pMplsInSegmentIndex ,pSetValMplsInSegmentTrafficParamPtr)	\
	nmhSetCmn(MplsInSegmentTrafficParamPtr, 13, MplsInSegmentTrafficParamPtrSet, NULL, NULL, 0, 0, 1, "%s %o", pMplsInSegmentIndex ,pSetValMplsInSegmentTrafficParamPtr)
#define nmhSetMplsInSegmentRowStatus(pMplsInSegmentIndex ,i4SetValMplsInSegmentRowStatus)	\
	nmhSetCmn(MplsInSegmentRowStatus, 13, MplsInSegmentRowStatusSet, NULL, NULL, 0, 1, 1, "%s %i", pMplsInSegmentIndex ,i4SetValMplsInSegmentRowStatus)
#define nmhSetMplsInSegmentStorageType(pMplsInSegmentIndex ,i4SetValMplsInSegmentStorageType)	\
	nmhSetCmn(MplsInSegmentStorageType, 13, MplsInSegmentStorageTypeSet, NULL, NULL, 0, 0, 1, "%s %i", pMplsInSegmentIndex ,i4SetValMplsInSegmentStorageType)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 MplsOutSegmentIndex[13];
extern UINT4 MplsOutSegmentInterface[13];
extern UINT4 MplsOutSegmentPushTopLabel[13];
extern UINT4 MplsOutSegmentTopLabel[13];
extern UINT4 MplsOutSegmentTopLabelPtr[13];
extern UINT4 MplsOutSegmentNextHopAddrType[13];
extern UINT4 MplsOutSegmentNextHopAddr[13];
extern UINT4 MplsOutSegmentTrafficParamPtr[13];
extern UINT4 MplsOutSegmentRowStatus[13];
extern UINT4 MplsOutSegmentStorageType[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetMplsOutSegmentInterface(pMplsOutSegmentIndex ,i4SetValMplsOutSegmentInterface)	\
	nmhSetCmn(MplsOutSegmentInterface, 13, MplsOutSegmentInterfaceSet, NULL, NULL, 0, 0, 1, "%s %i", pMplsOutSegmentIndex ,i4SetValMplsOutSegmentInterface)
#define nmhSetMplsOutSegmentPushTopLabel(pMplsOutSegmentIndex ,i4SetValMplsOutSegmentPushTopLabel)	\
	nmhSetCmn(MplsOutSegmentPushTopLabel, 13, MplsOutSegmentPushTopLabelSet, NULL, NULL, 0, 0, 1, "%s %i", pMplsOutSegmentIndex ,i4SetValMplsOutSegmentPushTopLabel)
#define nmhSetMplsOutSegmentTopLabel(pMplsOutSegmentIndex ,u4SetValMplsOutSegmentTopLabel)	\
	nmhSetCmn(MplsOutSegmentTopLabel, 13, MplsOutSegmentTopLabelSet, NULL, NULL, 0, 0, 1, "%s %u", pMplsOutSegmentIndex ,u4SetValMplsOutSegmentTopLabel)
#define nmhSetMplsOutSegmentTopLabelPtr(pMplsOutSegmentIndex ,pSetValMplsOutSegmentTopLabelPtr)	\
	nmhSetCmn(MplsOutSegmentTopLabelPtr, 13, MplsOutSegmentTopLabelPtrSet, NULL, NULL, 0, 0, 1, "%s %o", pMplsOutSegmentIndex ,pSetValMplsOutSegmentTopLabelPtr)
#define nmhSetMplsOutSegmentNextHopAddrType(pMplsOutSegmentIndex ,i4SetValMplsOutSegmentNextHopAddrType)	\
	nmhSetCmn(MplsOutSegmentNextHopAddrType, 13, MplsOutSegmentNextHopAddrTypeSet, NULL, NULL, 0, 0, 1, "%s %i", pMplsOutSegmentIndex ,i4SetValMplsOutSegmentNextHopAddrType)
#define nmhSetMplsOutSegmentNextHopAddr(pMplsOutSegmentIndex ,pSetValMplsOutSegmentNextHopAddr)	\
	nmhSetCmn(MplsOutSegmentNextHopAddr, 13, MplsOutSegmentNextHopAddrSet, NULL, NULL, 0, 0, 1, "%s %s", pMplsOutSegmentIndex ,pSetValMplsOutSegmentNextHopAddr)
#define nmhSetMplsOutSegmentTrafficParamPtr(pMplsOutSegmentIndex ,pSetValMplsOutSegmentTrafficParamPtr)	\
	nmhSetCmn(MplsOutSegmentTrafficParamPtr, 13, MplsOutSegmentTrafficParamPtrSet, NULL, NULL, 0, 0, 1, "%s %o", pMplsOutSegmentIndex ,pSetValMplsOutSegmentTrafficParamPtr)
#define nmhSetMplsOutSegmentRowStatus(pMplsOutSegmentIndex ,i4SetValMplsOutSegmentRowStatus)	\
	nmhSetCmn(MplsOutSegmentRowStatus, 13, MplsOutSegmentRowStatusSet, NULL, NULL, 0, 1, 1, "%s %i", pMplsOutSegmentIndex ,i4SetValMplsOutSegmentRowStatus)
#define nmhSetMplsOutSegmentStorageType(pMplsOutSegmentIndex ,i4SetValMplsOutSegmentStorageType)	\
	nmhSetCmn(MplsOutSegmentStorageType, 13, MplsOutSegmentStorageTypeSet, NULL, NULL, 0, 0, 1, "%s %i", pMplsOutSegmentIndex ,i4SetValMplsOutSegmentStorageType)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 MplsXCIndex[13];
extern UINT4 MplsXCInSegmentIndex[13];
extern UINT4 MplsXCOutSegmentIndex[13];
extern UINT4 MplsXCLspId[13];
extern UINT4 MplsXCLabelStackIndex[13];
extern UINT4 MplsXCRowStatus[13];
extern UINT4 MplsXCStorageType[13];
extern UINT4 MplsXCAdminStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetMplsXCLspId(pMplsXCIndex , pMplsXCInSegmentIndex , pMplsXCOutSegmentIndex ,pSetValMplsXCLspId)	\
	nmhSetCmn(MplsXCLspId, 13, MplsXCLspIdSet, NULL, NULL, 0, 0, 3, "%s %s %s %s", pMplsXCIndex , pMplsXCInSegmentIndex , pMplsXCOutSegmentIndex ,pSetValMplsXCLspId)
#define nmhSetMplsXCLabelStackIndex(pMplsXCIndex , pMplsXCInSegmentIndex , pMplsXCOutSegmentIndex ,pSetValMplsXCLabelStackIndex)	\
	nmhSetCmn(MplsXCLabelStackIndex, 13, MplsXCLabelStackIndexSet, NULL, NULL, 0, 0, 3, "%s %s %s %s", pMplsXCIndex , pMplsXCInSegmentIndex , pMplsXCOutSegmentIndex ,pSetValMplsXCLabelStackIndex)
#define nmhSetMplsXCRowStatus(pMplsXCIndex , pMplsXCInSegmentIndex , pMplsXCOutSegmentIndex ,i4SetValMplsXCRowStatus)	\
	nmhSetCmn(MplsXCRowStatus, 13, MplsXCRowStatusSet, NULL, NULL, 0, 1, 3, "%s %s %s %i", pMplsXCIndex , pMplsXCInSegmentIndex , pMplsXCOutSegmentIndex ,i4SetValMplsXCRowStatus)
#define nmhSetMplsXCStorageType(pMplsXCIndex , pMplsXCInSegmentIndex , pMplsXCOutSegmentIndex ,i4SetValMplsXCStorageType)	\
	nmhSetCmn(MplsXCStorageType, 13, MplsXCStorageTypeSet, NULL, NULL, 0, 0, 3, "%s %s %s %i", pMplsXCIndex , pMplsXCInSegmentIndex , pMplsXCOutSegmentIndex ,i4SetValMplsXCStorageType)
#define nmhSetMplsXCAdminStatus(pMplsXCIndex , pMplsXCInSegmentIndex , pMplsXCOutSegmentIndex ,i4SetValMplsXCAdminStatus)	\
	nmhSetCmn(MplsXCAdminStatus, 13, MplsXCAdminStatusSet, NULL, NULL, 0, 0, 3, "%s %s %s %i", pMplsXCIndex , pMplsXCInSegmentIndex , pMplsXCOutSegmentIndex ,i4SetValMplsXCAdminStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 MplsLabelStackIndex[13];
extern UINT4 MplsLabelStackLabelIndex[13];
extern UINT4 MplsLabelStackLabel[13];
extern UINT4 MplsLabelStackLabelPtr[13];
extern UINT4 MplsLabelStackRowStatus[13];
extern UINT4 MplsLabelStackStorageType[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetMplsLabelStackLabel(pMplsLabelStackIndex , u4MplsLabelStackLabelIndex ,u4SetValMplsLabelStackLabel)	\
	nmhSetCmn(MplsLabelStackLabel, 13, MplsLabelStackLabelSet, NULL, NULL, 0, 0, 2, "%s %u %u", pMplsLabelStackIndex , u4MplsLabelStackLabelIndex ,u4SetValMplsLabelStackLabel)
#define nmhSetMplsLabelStackLabelPtr(pMplsLabelStackIndex , u4MplsLabelStackLabelIndex ,pSetValMplsLabelStackLabelPtr)	\
	nmhSetCmn(MplsLabelStackLabelPtr, 13, MplsLabelStackLabelPtrSet, NULL, NULL, 0, 0, 2, "%s %u %o", pMplsLabelStackIndex , u4MplsLabelStackLabelIndex ,pSetValMplsLabelStackLabelPtr)
#define nmhSetMplsLabelStackRowStatus(pMplsLabelStackIndex , u4MplsLabelStackLabelIndex ,i4SetValMplsLabelStackRowStatus)	\
	nmhSetCmn(MplsLabelStackRowStatus, 13, MplsLabelStackRowStatusSet, NULL, NULL, 0, 1, 2, "%s %u %i", pMplsLabelStackIndex , u4MplsLabelStackLabelIndex ,i4SetValMplsLabelStackRowStatus)
#define nmhSetMplsLabelStackStorageType(pMplsLabelStackIndex , u4MplsLabelStackLabelIndex ,i4SetValMplsLabelStackStorageType)	\
	nmhSetCmn(MplsLabelStackStorageType, 13, MplsLabelStackStorageTypeSet, NULL, NULL, 0, 0, 2, "%s %u %i", pMplsLabelStackIndex , u4MplsLabelStackLabelIndex ,i4SetValMplsLabelStackStorageType)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 MplsXCNotificationsEnable[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetMplsXCNotificationsEnable(i4SetValMplsXCNotificationsEnable)	\
	nmhSetCmn(MplsXCNotificationsEnable, 11, MplsXCNotificationsEnableSet, NULL, NULL, 0, 0, 0, "%i", i4SetValMplsXCNotificationsEnable)

#endif
