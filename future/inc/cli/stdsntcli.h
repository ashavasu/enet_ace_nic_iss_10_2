/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdsntcli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 SnmpTargetSpinLock[9];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetSnmpTargetSpinLock(i4SetValSnmpTargetSpinLock)	\
	nmhSetCmn(SnmpTargetSpinLock, 9, SnmpTargetSpinLockSet, NULL, NULL, 0, 0, 0, "%i", i4SetValSnmpTargetSpinLock)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 SnmpTargetAddrName[11];
extern UINT4 SnmpTargetAddrTDomain[11];
extern UINT4 SnmpTargetAddrTAddress[11];
extern UINT4 SnmpTargetAddrTimeout[11];
extern UINT4 SnmpTargetAddrRetryCount[11];
extern UINT4 SnmpTargetAddrTagList[11];
extern UINT4 SnmpTargetAddrParams[11];
extern UINT4 SnmpTargetAddrStorageType[11];
extern UINT4 SnmpTargetAddrRowStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetSnmpTargetAddrTDomain(pSnmpTargetAddrName ,pSetValSnmpTargetAddrTDomain)	\
	nmhSetCmn(SnmpTargetAddrTDomain, 11, SnmpTargetAddrTDomainSet, NULL, NULL, 0, 0, 1, "%s %o", pSnmpTargetAddrName ,pSetValSnmpTargetAddrTDomain)
#define nmhSetSnmpTargetAddrTAddress(pSnmpTargetAddrName ,pSetValSnmpTargetAddrTAddress)	\
	nmhSetCmn(SnmpTargetAddrTAddress, 11, SnmpTargetAddrTAddressSet, NULL, NULL, 0, 0, 1, "%s %s", pSnmpTargetAddrName ,pSetValSnmpTargetAddrTAddress)
#define nmhSetSnmpTargetAddrTimeout(pSnmpTargetAddrName ,i4SetValSnmpTargetAddrTimeout)	\
	nmhSetCmn(SnmpTargetAddrTimeout, 11, SnmpTargetAddrTimeoutSet, NULL, NULL, 0, 0, 1, "%s %i", pSnmpTargetAddrName ,i4SetValSnmpTargetAddrTimeout)
#define nmhSetSnmpTargetAddrRetryCount(pSnmpTargetAddrName ,i4SetValSnmpTargetAddrRetryCount)	\
	nmhSetCmn(SnmpTargetAddrRetryCount, 11, SnmpTargetAddrRetryCountSet, NULL, NULL, 0, 0, 1, "%s %i", pSnmpTargetAddrName ,i4SetValSnmpTargetAddrRetryCount)
#define nmhSetSnmpTargetAddrTagList(pSnmpTargetAddrName ,pSetValSnmpTargetAddrTagList)	\
	nmhSetCmn(SnmpTargetAddrTagList, 11, SnmpTargetAddrTagListSet, NULL, NULL, 0, 0, 1, "%s %s", pSnmpTargetAddrName ,pSetValSnmpTargetAddrTagList)
#define nmhSetSnmpTargetAddrParams(pSnmpTargetAddrName ,pSetValSnmpTargetAddrParams)	\
	nmhSetCmn(SnmpTargetAddrParams, 11, SnmpTargetAddrParamsSet, NULL, NULL, 0, 0, 1, "%s %s", pSnmpTargetAddrName ,pSetValSnmpTargetAddrParams)
#define nmhSetSnmpTargetAddrStorageType(pSnmpTargetAddrName ,i4SetValSnmpTargetAddrStorageType)	\
	nmhSetCmn(SnmpTargetAddrStorageType, 11, SnmpTargetAddrStorageTypeSet, NULL, NULL, 0, 0, 1, "%s %i", pSnmpTargetAddrName ,i4SetValSnmpTargetAddrStorageType)
#define nmhSetSnmpTargetAddrRowStatus(pSnmpTargetAddrName ,i4SetValSnmpTargetAddrRowStatus)	\
	nmhSetCmn(SnmpTargetAddrRowStatus, 11, SnmpTargetAddrRowStatusSet, NULL, NULL, 0, 1, 1, "%s %i", pSnmpTargetAddrName ,i4SetValSnmpTargetAddrRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 SnmpTargetParamsName[11];
extern UINT4 SnmpTargetParamsMPModel[11];
extern UINT4 SnmpTargetParamsSecurityModel[11];
extern UINT4 SnmpTargetParamsSecurityName[11];
extern UINT4 SnmpTargetParamsSecurityLevel[11];
extern UINT4 SnmpTargetParamsStorageType[11];
extern UINT4 SnmpTargetParamsRowStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetSnmpTargetParamsMPModel(pSnmpTargetParamsName ,i4SetValSnmpTargetParamsMPModel)	\
	nmhSetCmn(SnmpTargetParamsMPModel, 11, SnmpTargetParamsMPModelSet, NULL, NULL, 0, 0, 1, "%s %i", pSnmpTargetParamsName ,i4SetValSnmpTargetParamsMPModel)
#define nmhSetSnmpTargetParamsSecurityModel(pSnmpTargetParamsName ,i4SetValSnmpTargetParamsSecurityModel)	\
	nmhSetCmn(SnmpTargetParamsSecurityModel, 11, SnmpTargetParamsSecurityModelSet, NULL, NULL, 0, 0, 1, "%s %i", pSnmpTargetParamsName ,i4SetValSnmpTargetParamsSecurityModel)
#define nmhSetSnmpTargetParamsSecurityName(pSnmpTargetParamsName ,pSetValSnmpTargetParamsSecurityName)	\
	nmhSetCmn(SnmpTargetParamsSecurityName, 11, SnmpTargetParamsSecurityNameSet, NULL, NULL, 0, 0, 1, "%s %s", pSnmpTargetParamsName ,pSetValSnmpTargetParamsSecurityName)
#define nmhSetSnmpTargetParamsSecurityLevel(pSnmpTargetParamsName ,i4SetValSnmpTargetParamsSecurityLevel)	\
	nmhSetCmn(SnmpTargetParamsSecurityLevel, 11, SnmpTargetParamsSecurityLevelSet, NULL, NULL, 0, 0, 1, "%s %i", pSnmpTargetParamsName ,i4SetValSnmpTargetParamsSecurityLevel)
#define nmhSetSnmpTargetParamsStorageType(pSnmpTargetParamsName ,i4SetValSnmpTargetParamsStorageType)	\
	nmhSetCmn(SnmpTargetParamsStorageType, 11, SnmpTargetParamsStorageTypeSet, NULL, NULL, 0, 0, 1, "%s %i", pSnmpTargetParamsName ,i4SetValSnmpTargetParamsStorageType)
#define nmhSetSnmpTargetParamsRowStatus(pSnmpTargetParamsName ,i4SetValSnmpTargetParamsRowStatus)	\
	nmhSetCmn(SnmpTargetParamsRowStatus, 11, SnmpTargetParamsRowStatusSet, NULL, NULL, 0, 1, 1, "%s %i", pSnmpTargetParamsName ,i4SetValSnmpTargetParamsRowStatus)

#endif
