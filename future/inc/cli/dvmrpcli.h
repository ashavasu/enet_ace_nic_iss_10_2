#ifndef DVMRPCLI_H
#define DVMRPCLI_H
#include "cli.h"
/* FutureDVMRP CLI command constants. To add a new command ,a new 
 * definition needs to be added to the list.
 */
enum {
     CLI_DVMRP_SHOW_ALL = 1,
     CLI_DVMRP_ENABLE,
     CLI_DVMRP_IFACE_STATUS_ENABLE,
     CLI_DVMRP_TRACE,
     CLI_DVMRP_NO_TRACE,
     CLI_DVMRP_PRUNE_LIFE_TIME,
     CLI_DVMRP_NO_PRUNE_LIFE_TIME
};

/* Error codes */
enum
{
    CLI_DVMRP_FATAL_ERR =1,
    CLI_DVMRP_IGMP_PROXY_ACTIVE_ERR,
    CLI_DVMRP_MAX_ERR
};

#ifdef __DVMRPCLI_C__

CONST CHR1  *DvmrpCliErrString [] = {

       NULL,
      "%% Fatal Error: Command Failed\r\n",
      " % DVMRP cannot start, IGMP Proxy is active \r\n",
      "\r\n"
};

#endif


/* This value needs to be updated if any new command is added to the list */
#define DVMRP_CLI_MAX_COMMANDS                    (10)
#define DVMRP_MODE_SIZE                            (8)
#define DVMRP_CLI_MAX_ARGS                             4

#define SHOW_NEIGHBOR  1
#define SHOW_DVMRP_ROUTE     2
#define SHOW_MROUTE    3
#define SHOW_NEXTHOP   4
#define SHOW_INFO      5
#define SHOW_PRUNE     6

#define CLI_DVMRP_TRACE_DIS_FLAG       0x80000000

#define DVMRP_NBR_DEBUG               0x01 
#define DVMRP_GRP_DEBUG               0x02 
#define DVMRP_JP_DEBUG                0x04  
#define DVMRP_IO_DEBUG                0x08
#define DVMRP_MRT_DEBUG               0x10
#define DVMRP_MDH_DEBUG               0x20
#define DVMRP_MGMT_DEBUG              0x40
#define DVMRP_ALL_DEBUG               0xff
/* SIZE constants */
#define DVMRP_DEF_INT_MODE                            2

INT4 DvmrpShowNextHop(tCliHandle CliHandle);
INT4 DvmrpShowNeighbor(tCliHandle CliHandle);
INT4 DvmrpShowMulticastRoute(tCliHandle);
INT4 DvmrpShowRouteInterface(tCliHandle CliHandle, UINT4 u4Index);
INT4 DvmrpShowInfo(tCliHandle);
INT4 DvmrpSetTrace (tCliHandle CliHandle, INT4 i4LogMask);
INT4 DvmrpSetIfaceStatus (tCliHandle CliHandle, INT4 i4Status);
INT4 DvmrpSetPruneLifeTime (tCliHandle CliHandle, INT4 u4Timer);
INT4 DvmrpSetModuleStatus (tCliHandle CliHandle, INT4 u1Status);
INT4 DvmrpShowMulticastPrune(tCliHandle CliHandle);
INT4 DvmrpShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module);
INT4 DvmrpShowRunningConfigInterfaceDetails (tCliHandle CliHandle,INT4 i4Index);
VOID IssDvmrpShowDebugging (tCliHandle);
INT4 cli_process_dvmrp_cmd (tCliHandle, UINT4, ...);
#endif   /* DVMRPCLI_H */
