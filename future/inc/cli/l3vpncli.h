/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: l3vpncli.h,v 1.5 2018/01/03 11:31:19 siva Exp $
*
* Description: This file contains the L3vpn CLI related prototypes,enum and macros
*********************************************************************/

#include "mpls.h"
#include "l3vpndefn.h"

INT4 cli_process_L3vpn_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);
INT4 cli_process_L3vpn_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);
INT4 cli_process_l3vpn_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);

#ifdef MPLS_TEST_WANTED
#define CLI_L3VPN_UT      1
INT4
cli_process_l3vpn_test_cmd (tCliHandle CliHandle, UINT4 u4Command,...);
#endif

enum
{
        CLI_MPLS_L3VPN_CREATE_VRF,
        CLI_MPLS_L3VPN_DELETE_VRF,
        CLI_L3VPN_MPLSL3VPNNOTIFICATIONENABLE,
        CLI_L3VPN_MPLSL3VPNVRFCONFRTEMXTHRSHTIME,
        CLI_L3VPN_MPLSL3VPNILLLBLRCVTHRSH,
        CLI_L3VPN_MPLSL3VPNVRFTABLE,
        CLI_L3VPN_MPLSL3VPNIFCONFTABLE,
        CLI_L3VPN_MPLSL3VPNVRFRTTABLE,
        CLI_L3VPN_MPLSL3VPNVRFRTETABLE,
        CLI_L3VPN_SHOW_VRF,
        CLI_L3VPN_SHOW_VRF_ALL,
        CLI_L3VPN_SHOW_VRF_RT,
        CLI_L3VPN_SHOW_VRF_IF_CONF,
        CLI_L3VPN_SHOW_EGRESS_ROUTES,
        CLI_L3VPN_ROUTE_ADD_PER_VRF,
        CLI_L3VPN_ROUTE_ADD_PER_ROUTE,
        CLI_L3VPN_ROUTE_DEL_EGRESS_ROUTE,
        CLI_L3VPN_DEBUG,
        CLI_L3VPN_BGP_ROUTE_ADD,
        CLI_L3VPN_BGP_ROUTE_DEL,
        CLI_L3VPN_SHOW_RTE_TABLE,
        CLI_L3VPN_SEARCH_TRIE,
        CLI_L3VPN_SHOW_VRF_PERF_TABLE,
    CLI_L3VPN_MPLSL3VPN_RSVP_TUNNEL_BINDING,
    CLI_L3VPN_MPLSL3VPN_NO_RSVP_TUNNEL_BINDING,
    CLI_L3VPN_SHOW_BINDING_RSVP_MAP_TABLE
};

#define CLI_ERR_START_ID_L3VPN 1
#define MPLS_L3VPN_VRF_MODE "ip_vrf_mpls_"

enum
{
        CLI_L3VPN_UNKNOWN_ERROR=CLI_ERR_START_ID_L3VPN,
        CLI_L3VPN_ACTIVE_VRF_FOUND,
        CLI_L3VPN_EXCEED_VRF_MAX_COUNT,
        CLI_L3VPN_EXCEED_VRF_MID_RTE_THRESH,
        CLI_L3VPN_EXCEED_VRF_MID_THRSH_WRONG,
        CLI_L3VPN_EXCEED_VRF_HIGH_THRSH_WRONG,
        CLI_L3VPN_EXCEED_VRF_HIGH_RTE_THRESH,
        CLI_L3VPN_EXCEED_ILL_LBL_RCV_THRSH,
        CLI_L3VPN_RT_DUPLICATE_ERR,
        CLI_L3VPN_ERROR_RT_DELETE,
        CLI_L3VPN_NO_SUCH_RT,
        CLI_L3VPN_WRONG_RT_FORMAT,
        CLI_L3VPN_WRONG_RD_FORMAT,
        CLI_L3VPN_INVALID_ASN,
        CLI_L3VPN_INVALID_VRF_NAME,
        CLI_L3VPN_INVALID_DEST_TYPE,
        CLI_L3VPN_INVALID_NHOP_TYPE,
        CLI_L3VPN_INVALID_DEST,
        CLI_L3VPN_INVALID_NHOP,
        CLI_L3VPN_INVALID_POLICY,
        CLI_L3VPN_INVALID_PREFIX_LEN,
        CLI_L3VPN_INVALID_METRIC,
        CLI_L3VPN_INVALID_XC_ENTRY,
        CLI_L3VPN_RTE_ENTRY_EXIST,
        CLI_L3VPN_RTE_ENTRY_INVALID,
        CLI_L3VPN_ACTIVE_RTE_ENTRY_EXIST,
        CLI_L3VPN_MAX_ERR
};

#define L3VPN_CLI_MAX_ARGS   30

/* Accessing the ErrString with ErrCode value.
 * Since the ErrCode's starting Id is changed, this forumla is
 * used to access the Err String */

#define CLI_ERR_OFFSET_L3VPN(u4ErrCode) ((u4ErrCode - CLI_ERR_START_ID_L3VPN))

#if defined  (__L3VPNCLIG_C__)
CONST CHR1  *L3vpnCliErrString [] = {
        "%% Unknown Error\r\n\n",
        "%% Active VRF exists\r\n\n",
        "%% Max-Route per vrf limit exceeded\r\n\n",
        "%% Mid-level water marker limit exceeded\r\n\n",
        "%% Mid-level water market should be less than High level water marker\r\n\n",
        "%% High-level water market should be greater than mid-level water marker\r\n\n",
        "%% High-level water marker limit exceeded\r\n\n",
        "%% Illegal Label Recieved Threshold Limit exceeded\r\n\n",
        "%% This RT is already configured for the VRF\n",
        "%% Error in Rt Deletion\n",
        "%% No such RT is configured for this VRF\n",
        "%% Wrong format of RT\n",
        "%% Wrong Format of RD\n",
        "%% ASN Validation Failed\n",
        "%% VRF-Name does not exist\n",
        "%% Destination IP is not an IPV4 Address\n",
        "%% Next Hop IP is not an IPV4 Address\n",
        "%% Destination Ip is Invalid\n",
        "%% NextHop Ip is Invalid\n",
        "%% Invalid Policy\n",
        "%% Invalid Prefix Len\n",
        "%% Invalid Metric\n",
        "%% Unable to create Xc Entry\n",
        "%% Route Entry Already Exists",
        "%% Invalid Route Entry",
        "%% Active Route Entry Exists",
        NULL
};
#else

PUBLIC CHR1 *L3vpnCliErrString[];
#endif

#define  CLI_L3VPN_FN_ENTRY_EXIT_TRC       0x100000
#define  CLI_L3VPN_MGMT_TRC                0x010000
#define  CLI_L3VPN_MAIN_TRC                0x001000
#define  CLI_L3VPN_UTIL_TRC                0x000100
#define  CLI_L3VPN_RESOURCE_TRC            0x000010
#define  CLI_L3VPN_ALL_FAIL_TRC            0x000001
#define  CLI_L3VPN_ALL_TRC             CLI_L3VPN_FN_ENTRY_EXIT_TRC |\
                                    CLI_L3VPN_MGMT_TRC  |\
                                    CLI_L3VPN_MAIN_TRC |\
                                 CLI_L3VPN_UTIL_TRC  |\
                                 CLI_L3VPN_RESOURCE_TRC |\
                                 CLI_L3VPN_ALL_FAIL_TRC


#define   CLI_MPLS_L3VPN_RT_IMPORT    MPLS_L3VPN_RT_IMPORT
#define   CLI_MPLS_L3VPN_RT_EXPORT    MPLS_L3VPN_RT_EXPORT
#define   CLI_MPLS_L3VPN_RT_BOTH      MPLS_L3VPN_RT_BOTH

#define   CLI_L3VPN_TRAP_ENABLE    L3VPN_TRAP_ENABLE
#define   CLI_L3VPN_TRAP_DISABLE   L3VPN_TRAP_DISABLE

#define   CLI_L3VPN_RT_SUBTYPE L3VPN_RT_SUBTYPE

#define CLI_VRF_ADMIN_STATUS_UP    L3VPN_VRF_ADMIN_STATUS_UP
#define CLI_VRF_ADMIN_STATUS_DOWN  L3VPN_VRF_ADMIN_STATUS_DOWN 

#define MAX_RT_POLICY_LEN 7

#define  MAX_CLI_L3VPN_TIME_STRING           9

#define CLI_L3VPN_MAX_RT_LEN L3VPN_MAX_RT_LEN

INT1
MplsL3VpnGetMplsL3VpnVrfCfgPrompt(INT1 *pi1ModeName, INT1 *pi1DispStr);

INT4
MplsL3VpnCliVrfChangeMode(UINT4 u4VcId);

UINT4
MplsL3VpnApiGetVrfTableRowStatus (UINT1 *pu1MplsL3VpnVrfName,INT4 i4MplsL3VpnVrfNameLen,INT4 *pMplsL3VpnVrfOperStatus);

INT4
CliGetL3vpnDebugLevel(tCliHandle CliHandle, UINT4 *pu4DebugLevel);

VOID
CliL3VpnConvertTimetoString (UINT4 u4Time, UINT1 *pi1Time);

#if 0
INT4
L3VpnCliParseAndGenerateRdRt (UINT1* pu1RandomString, UINT1* pu1RdRt);

UINT4 L3VpnUtilFindNextFreeRtIndex(UINT4 contextId, UINT4 *pu4FreeIndex);
#endif
