/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: httpcli.h,v 1.5 2013/02/12 12:09:40 siva Exp $
 *
 * Description: Header file for http CLI related declarations.
 *******************************************************************/

#include "lr.h"
#include "cli.h"

/* FutureHTTP CLI command constants. To add a new command ,a new 
 * definition needs to be added to the list.
 */
enum {
     HTTP_CLI_SET_AUTH_SCHEME = 1,
     HTTP_REDIRECT_DISABLE,
     HTTP_REDIRECT_ENABLE,
     HTTP_CLI_REDIRECT_V4_SERVERIP,
     HTTP_CLI_REDIRECT_V6_SERVERIP,
     HTTP_CLI_REDIRECT_DOMAINNAME,
     HTTP_CLI_NO_REDIRECT_ALL,
     HTTP_CLI_NO_REDIRECT_ENTRY,
     HTTP_CLI_SHOW_AUTH_SCHEME,
     HTTP_CLI_SHOW_HTTP_REDIRECT_ALL,
     HTTP_CLI_SHOW_HTTP_REDIRECT_URL,
     HTTP_CLI_CLEAR_SRV_STATS
};

/* This value needs to be updated if any new command is added to the list */
#define HTTP_CLI_MAX_COMMANDS      (7)

#define HTTP_MAX_ARGS               6
#define HTTP_MAX_ADDR_BUFFER        256

/* HTTP Authentication related constants */
enum {
     CLI_HTTP_DEFAULT = 0,
     CLI_HTTP_BASIC,
     CLI_HTTP_DIGEST
};

INT1 cli_process_http_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);

#ifdef WEBNM_TEST_WANTED
#define CLI_WEBNM_UT_TEST 1
INT4
cli_process_webnm_test_cmd (tCliHandle CliHandle,  UINT4 u4Command, ...);
#endif

#ifdef __HTTPCLI_C__
CONST CHR1  *HttpCliErrString [] = {
    NULL,
    "Unknown Error\r\n",
    "Entry not Found \r\n",
    "No Entries Found \r\n",
    "Maximum Redirection Entries Reached\r\n",
    "Specified URL exceeds Max URL length supported\r\n",
    "\r\n"
};
#endif

enum {
    CLI_HTTP_UNKNOWN_ERR = 1,
    CLI_HTTP_ENTRY_NOT_FOUND,
    CLI_HTTP_NO_ENTRIES_FOUND,
    CLI_HTTP_MAX_REDIRECTION_ENTRIES,
    CLI_HTTP_URL_LEN_NOT_SUPPORTED,
    CLI_HTTP_MAX_ERR 
};

INT4 HttpShowRunningConfig (tCliHandle CliHandle);
INT4 HttpShowRunningConfigScalar (tCliHandle CliHandle);
INT4 HttpShowRunningConfigTable (tCliHandle CliHandle);



