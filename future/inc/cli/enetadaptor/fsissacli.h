/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsissacli.h,v 1.1 2012/03/01 10:19:37 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IssAclRateCtrlIndex[13];
extern UINT4 IssAclRateCtrlDLFLimitValue[13];
extern UINT4 IssAclRateCtrlBCASTLimitValue[13];
extern UINT4 IssAclRateCtrlMCASTLimitValue[13];
extern UINT4 IssAclRateCtrlPortRateLimit[13];
extern UINT4 IssAclRateCtrlPortBurstSize[13];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IssAclL2FilterNo[13];
extern UINT4 IssAclL2FilterPriority[13];
extern UINT4 IssAclL2FilterEtherType[13];
extern UINT4 IssAclL2FilterProtocolType[13];
extern UINT4 IssAclL2FilterDstMacAddr[13];
extern UINT4 IssAclL2FilterSrcMacAddr[13];
extern UINT4 IssAclL2FilterVlanId[13];
extern UINT4 IssAclL2FilterInPortList[13];
extern UINT4 IssAclL2FilterAction[13];
extern UINT4 IssAclL2FilterStatus[13];
extern UINT4 IssAclL2FilterOutPortList[13];
extern UINT4 IssAclL2FilterDirection[13];
extern UINT4 IssAclL2FilterSubAction[13];
extern UINT4 IssAclL2FilterSubActionId[13];
extern UINT4 IssAclL2NextFilterNo[13];
extern UINT4 IssAclL2NextFilterType[13];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IssAclL3FilterNo[13];
extern UINT4 IssAclL3FilterPriority[13];
extern UINT4 IssAclL3FilterProtocol[13];
extern UINT4 IssAclL3FilterMessageType[13];
extern UINT4 IssAclL3FilterMessageCode[13];
extern UINT4 IssAclL3FilteAddrType[13];
extern UINT4 IssAclL3FilterDstIpAddr[13];
extern UINT4 IssAclL3FilterSrcIpAddr[13];
extern UINT4 IssAclL3FilterDstIpAddrPrefixLength[13];
extern UINT4 IssAclL3FilterSrcIpAddrPrefixLength[13];
extern UINT4 IssAclL3FilterMinDstProtPort[13];
extern UINT4 IssAclL3FilterMaxDstProtPort[13];
extern UINT4 IssAclL3FilterMinSrcProtPort[13];
extern UINT4 IssAclL3FilterMaxSrcProtPort[13];
extern UINT4 IssAclL3FilterInPortList[13];
extern UINT4 IssAclL3FilterOutPortList[13];
extern UINT4 IssAclL3FilterAckBit[13];
extern UINT4 IssAclL3FilterRstBit[13];
extern UINT4 IssAclL3FilterTos[13];
extern UINT4 IssAclL3FilterDscp[13];
extern UINT4 IssAclL3FilterDirection[13];
extern UINT4 IssAclL3FilterAction[13];
extern UINT4 IssAclL3FilterFlowId[13];
extern UINT4 IssAclL3FilterStatus[13];
extern UINT4 IssAclL3FilterSubAction[13];
extern UINT4 IssAclL3FilterSubActionId[13];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IssAclUserDefinedFilterId[13];
extern UINT4 IssAclUserDefinedFilterPktType[13];
extern UINT4 IssAclUserDefinedFilterOffSetBase[13];
extern UINT4 IssAclUserDefinedFilterOffSetValue[13];
extern UINT4 IssAclUserDefinedFilterOffSetMask[13];
extern UINT4 IssAclUserDefinedFilterPriority[13];
extern UINT4 IssAclUserDefinedFilterAction[13];
extern UINT4 IssAclUserDefinedFilterInPortList[13];
extern UINT4 IssAclUserDefinedFilterIdOneType[13];
extern UINT4 IssAclUserDefinedFilterIdOne[13];
extern UINT4 IssAclUserDefinedFilterIdTwoType[13];
extern UINT4 IssAclUserDefinedFilterIdTwo[13];
extern UINT4 IssAclUserDefinedFilterSubAction[13];
extern UINT4 IssAclUserDefinedFilterSubActionId[13];
extern UINT4 IssAclUserDefinedFilterStatus[13];

extern UINT4 IssRedirectInterfaceGrpId [13];
extern UINT4 IssRedirectInterfaceGrpFilterType [13]; 
extern UINT4 IssRedirectInterfaceGrpFilterId [13];
extern UINT4 IssRedirectInterfaceGrpDistByte [13];
extern UINT4 IssRedirectInterfaceGrpPortList [13];
extern UINT4 IssRedirectInterfaceGrpType [13];
extern UINT4 IssRedirectInterfaceGrpUdbPosition [13];
extern UINT4 IssRedirectInterfaceGrpStatus [13];

