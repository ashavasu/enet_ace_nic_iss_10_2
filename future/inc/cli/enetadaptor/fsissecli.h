/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsissecli.h,v 1.1 2012/03/01 10:19:37 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IssExtRateCtrlIndex[13];
extern UINT4 IssExtRateCtrlDLFLimitValue[13];
extern UINT4 IssExtRateCtrlBCASTLimitValue[13];
extern UINT4 IssExtRateCtrlMCASTLimitValue[13];
extern UINT4 IssExtRateCtrlPortRateLimit[13];
extern UINT4 IssExtRateCtrlPortBurstSize[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIssExtRateCtrlDLFLimitValue(i4IssExtRateCtrlIndex ,i4SetValIssExtRateCtrlDLFLimitValue)	\
	nmhSetCmn(IssExtRateCtrlDLFLimitValue, 13, IssExtRateCtrlDLFLimitValueSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssExtRateCtrlIndex ,i4SetValIssExtRateCtrlDLFLimitValue)
#define nmhSetIssExtRateCtrlBCASTLimitValue(i4IssExtRateCtrlIndex ,i4SetValIssExtRateCtrlBCASTLimitValue)	\
	nmhSetCmn(IssExtRateCtrlBCASTLimitValue, 13, IssExtRateCtrlBCASTLimitValueSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssExtRateCtrlIndex ,i4SetValIssExtRateCtrlBCASTLimitValue)
#define nmhSetIssExtRateCtrlMCASTLimitValue(i4IssExtRateCtrlIndex ,i4SetValIssExtRateCtrlMCASTLimitValue)	\
	nmhSetCmn(IssExtRateCtrlMCASTLimitValue, 13, IssExtRateCtrlMCASTLimitValueSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssExtRateCtrlIndex ,i4SetValIssExtRateCtrlMCASTLimitValue)
#define nmhSetIssExtRateCtrlPortRateLimit(i4IssExtRateCtrlIndex ,i4SetValIssExtRateCtrlPortRateLimit)	\
	nmhSetCmn(IssExtRateCtrlPortRateLimit, 13, IssExtRateCtrlPortRateLimitSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssExtRateCtrlIndex ,i4SetValIssExtRateCtrlPortRateLimit)
#define nmhSetIssExtRateCtrlPortBurstSize(i4IssExtRateCtrlIndex ,i4SetValIssExtRateCtrlPortBurstSize)	\
	nmhSetCmn(IssExtRateCtrlPortBurstSize, 13, IssExtRateCtrlPortBurstSizeSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssExtRateCtrlIndex ,i4SetValIssExtRateCtrlPortBurstSize)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IssExtL2FilterNo[13];
extern UINT4 IssExtL2FilterPriority[13];
extern UINT4 IssExtL2FilterEtherType[13];
extern UINT4 IssExtL2FilterProtocolType[13];
extern UINT4 IssExtL2FilterDstMacAddr[13];
extern UINT4 IssExtL2FilterSrcMacAddr[13];
extern UINT4 IssExtL2FilterVlanId[13];
extern UINT4 IssExtL2FilterInPortList[13];
extern UINT4 IssExtL2FilterAction[13];
extern UINT4 IssExtL2FilterStatus[13];
extern UINT4 IssExtL2FilterOutPortList[13];
extern UINT4 IssExtL2FilterDirection[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIssExtL2FilterPriority(i4IssExtL2FilterNo ,i4SetValIssExtL2FilterPriority)	\
	nmhSetCmn(IssExtL2FilterPriority, 13, IssExtL2FilterPrioritySet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssExtL2FilterNo ,i4SetValIssExtL2FilterPriority)
#define nmhSetIssExtL2FilterEtherType(i4IssExtL2FilterNo ,i4SetValIssExtL2FilterEtherType)	\
	nmhSetCmn(IssExtL2FilterEtherType, 13, IssExtL2FilterEtherTypeSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssExtL2FilterNo ,i4SetValIssExtL2FilterEtherType)
#define nmhSetIssExtL2FilterProtocolType(i4IssExtL2FilterNo ,u4SetValIssExtL2FilterProtocolType)	\
	nmhSetCmn(IssExtL2FilterProtocolType, 13, IssExtL2FilterProtocolTypeSet, IssLock, IssUnLock, 0, 0, 1, "%i %u", i4IssExtL2FilterNo ,u4SetValIssExtL2FilterProtocolType)
#define nmhSetIssExtL2FilterDstMacAddr(i4IssExtL2FilterNo ,SetValIssExtL2FilterDstMacAddr)	\
	nmhSetCmn(IssExtL2FilterDstMacAddr, 13, IssExtL2FilterDstMacAddrSet, IssLock, IssUnLock, 0, 0, 1, "%i %m", i4IssExtL2FilterNo ,SetValIssExtL2FilterDstMacAddr)
#define nmhSetIssExtL2FilterSrcMacAddr(i4IssExtL2FilterNo ,SetValIssExtL2FilterSrcMacAddr)	\
	nmhSetCmn(IssExtL2FilterSrcMacAddr, 13, IssExtL2FilterSrcMacAddrSet, IssLock, IssUnLock, 0, 0, 1, "%i %m", i4IssExtL2FilterNo ,SetValIssExtL2FilterSrcMacAddr)
#define nmhSetIssExtL2FilterVlanId(i4IssExtL2FilterNo ,i4SetValIssExtL2FilterVlanId)	\
	nmhSetCmn(IssExtL2FilterVlanId, 13, IssExtL2FilterVlanIdSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssExtL2FilterNo ,i4SetValIssExtL2FilterVlanId)
#define nmhSetIssExtL2FilterInPortList(i4IssExtL2FilterNo ,pSetValIssExtL2FilterInPortList)	\
	nmhSetCmn(IssExtL2FilterInPortList, 13, IssExtL2FilterInPortListSet, IssLock, IssUnLock, 0, 0, 1, "%i %s", i4IssExtL2FilterNo ,pSetValIssExtL2FilterInPortList)
#define nmhSetIssExtL2FilterAction(i4IssExtL2FilterNo ,i4SetValIssExtL2FilterAction)	\
	nmhSetCmn(IssExtL2FilterAction, 13, IssExtL2FilterActionSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssExtL2FilterNo ,i4SetValIssExtL2FilterAction)
#define nmhSetIssExtL2FilterStatus(i4IssExtL2FilterNo ,i4SetValIssExtL2FilterStatus)	\
	nmhSetCmn(IssExtL2FilterStatus, 13, IssExtL2FilterStatusSet, IssLock, IssUnLock, 0, 1, 1, "%i %i", i4IssExtL2FilterNo ,i4SetValIssExtL2FilterStatus)
#define nmhSetIssExtL2FilterOutPortList(i4IssExtL2FilterNo ,pSetValIssExtL2FilterOutPortList)	\
	nmhSetCmn(IssExtL2FilterOutPortList, 13, IssExtL2FilterOutPortListSet, IssLock, IssUnLock, 0, 0, 1, "%i %s", i4IssExtL2FilterNo ,pSetValIssExtL2FilterOutPortList)
#define nmhSetIssExtL2FilterDirection(i4IssExtL2FilterNo ,i4SetValIssExtL2FilterDirection)	\
	nmhSetCmn(IssExtL2FilterDirection, 13, IssExtL2FilterDirectionSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssExtL2FilterNo ,i4SetValIssExtL2FilterDirection)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IssExtL3FilterNo[13];
extern UINT4 IssExtL3FilterPriority[13];
extern UINT4 IssExtL3FilterProtocol[13];
extern UINT4 IssExtL3FilterMessageType[13];
extern UINT4 IssExtL3FilterMessageCode[13];
extern UINT4 IssExtL3FilterDstIpAddr[13];
extern UINT4 IssExtL3FilterSrcIpAddr[13];
extern UINT4 IssExtL3FilterDstIpAddrMask[13];
extern UINT4 IssExtL3FilterSrcIpAddrMask[13];
extern UINT4 IssExtL3FilterMinDstProtPort[13];
extern UINT4 IssExtL3FilterMaxDstProtPort[13];
extern UINT4 IssExtL3FilterMinSrcProtPort[13];
extern UINT4 IssExtL3FilterMaxSrcProtPort[13];
extern UINT4 IssExtL3FilterInPortList[13];
extern UINT4 IssExtL3FilterOutPortList[13];
extern UINT4 IssExtL3FilterAckBit[13];
extern UINT4 IssExtL3FilterRstBit[13];
extern UINT4 IssExtL3FilterTos[13];
extern UINT4 IssExtL3FilterDscp[13];
extern UINT4 IssExtL3FilterDirection[13];
extern UINT4 IssExtL3FilterAction[13];
extern UINT4 IssExtL3FilterStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIssExtL3FilterPriority(i4IssExtL3FilterNo ,i4SetValIssExtL3FilterPriority)	\
	nmhSetCmn(IssExtL3FilterPriority, 13, IssExtL3FilterPrioritySet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssExtL3FilterNo ,i4SetValIssExtL3FilterPriority)
#define nmhSetIssExtL3FilterProtocol(i4IssExtL3FilterNo ,i4SetValIssExtL3FilterProtocol)	\
	nmhSetCmn(IssExtL3FilterProtocol, 13, IssExtL3FilterProtocolSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssExtL3FilterNo ,i4SetValIssExtL3FilterProtocol)
#define nmhSetIssExtL3FilterMessageType(i4IssExtL3FilterNo ,i4SetValIssExtL3FilterMessageType)	\
	nmhSetCmn(IssExtL3FilterMessageType, 13, IssExtL3FilterMessageTypeSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssExtL3FilterNo ,i4SetValIssExtL3FilterMessageType)
#define nmhSetIssExtL3FilterMessageCode(i4IssExtL3FilterNo ,i4SetValIssExtL3FilterMessageCode)	\
	nmhSetCmn(IssExtL3FilterMessageCode, 13, IssExtL3FilterMessageCodeSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssExtL3FilterNo ,i4SetValIssExtL3FilterMessageCode)
#define nmhSetIssExtL3FilterDstIpAddr(i4IssExtL3FilterNo ,u4SetValIssExtL3FilterDstIpAddr)	\
	nmhSetCmn(IssExtL3FilterDstIpAddr, 13, IssExtL3FilterDstIpAddrSet, IssLock, IssUnLock, 0, 0, 1, "%i %p", i4IssExtL3FilterNo ,u4SetValIssExtL3FilterDstIpAddr)
#define nmhSetIssExtL3FilterSrcIpAddr(i4IssExtL3FilterNo ,u4SetValIssExtL3FilterSrcIpAddr)	\
	nmhSetCmn(IssExtL3FilterSrcIpAddr, 13, IssExtL3FilterSrcIpAddrSet, IssLock, IssUnLock, 0, 0, 1, "%i %p", i4IssExtL3FilterNo ,u4SetValIssExtL3FilterSrcIpAddr)
#define nmhSetIssExtL3FilterDstIpAddrMask(i4IssExtL3FilterNo ,u4SetValIssExtL3FilterDstIpAddrMask)	\
	nmhSetCmn(IssExtL3FilterDstIpAddrMask, 13, IssExtL3FilterDstIpAddrMaskSet, IssLock, IssUnLock, 0, 0, 1, "%i %p", i4IssExtL3FilterNo ,u4SetValIssExtL3FilterDstIpAddrMask)
#define nmhSetIssExtL3FilterSrcIpAddrMask(i4IssExtL3FilterNo ,u4SetValIssExtL3FilterSrcIpAddrMask)	\
	nmhSetCmn(IssExtL3FilterSrcIpAddrMask, 13, IssExtL3FilterSrcIpAddrMaskSet, IssLock, IssUnLock, 0, 0, 1, "%i %p", i4IssExtL3FilterNo ,u4SetValIssExtL3FilterSrcIpAddrMask)
#define nmhSetIssExtL3FilterMinDstProtPort(i4IssExtL3FilterNo ,u4SetValIssExtL3FilterMinDstProtPort)	\
	nmhSetCmn(IssExtL3FilterMinDstProtPort, 13, IssExtL3FilterMinDstProtPortSet, IssLock, IssUnLock, 0, 0, 1, "%i %u", i4IssExtL3FilterNo ,u4SetValIssExtL3FilterMinDstProtPort)
#define nmhSetIssExtL3FilterMaxDstProtPort(i4IssExtL3FilterNo ,u4SetValIssExtL3FilterMaxDstProtPort)	\
	nmhSetCmn(IssExtL3FilterMaxDstProtPort, 13, IssExtL3FilterMaxDstProtPortSet, IssLock, IssUnLock, 0, 0, 1, "%i %u", i4IssExtL3FilterNo ,u4SetValIssExtL3FilterMaxDstProtPort)
#define nmhSetIssExtL3FilterMinSrcProtPort(i4IssExtL3FilterNo ,u4SetValIssExtL3FilterMinSrcProtPort)	\
	nmhSetCmn(IssExtL3FilterMinSrcProtPort, 13, IssExtL3FilterMinSrcProtPortSet, IssLock, IssUnLock, 0, 0, 1, "%i %u", i4IssExtL3FilterNo ,u4SetValIssExtL3FilterMinSrcProtPort)
#define nmhSetIssExtL3FilterMaxSrcProtPort(i4IssExtL3FilterNo ,u4SetValIssExtL3FilterMaxSrcProtPort)	\
	nmhSetCmn(IssExtL3FilterMaxSrcProtPort, 13, IssExtL3FilterMaxSrcProtPortSet, IssLock, IssUnLock, 0, 0, 1, "%i %u", i4IssExtL3FilterNo ,u4SetValIssExtL3FilterMaxSrcProtPort)
#define nmhSetIssExtL3FilterInPortList(i4IssExtL3FilterNo ,pSetValIssExtL3FilterInPortList)	\
	nmhSetCmn(IssExtL3FilterInPortList, 13, IssExtL3FilterInPortListSet, IssLock, IssUnLock, 0, 0, 1, "%i %s", i4IssExtL3FilterNo ,pSetValIssExtL3FilterInPortList)
#define nmhSetIssExtL3FilterOutPortList(i4IssExtL3FilterNo ,pSetValIssExtL3FilterOutPortList)	\
	nmhSetCmn(IssExtL3FilterOutPortList, 13, IssExtL3FilterOutPortListSet, IssLock, IssUnLock, 0, 0, 1, "%i %s", i4IssExtL3FilterNo ,pSetValIssExtL3FilterOutPortList)
#define nmhSetIssExtL3FilterAckBit(i4IssExtL3FilterNo ,i4SetValIssExtL3FilterAckBit)	\
	nmhSetCmn(IssExtL3FilterAckBit, 13, IssExtL3FilterAckBitSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssExtL3FilterNo ,i4SetValIssExtL3FilterAckBit)
#define nmhSetIssExtL3FilterRstBit(i4IssExtL3FilterNo ,i4SetValIssExtL3FilterRstBit)	\
	nmhSetCmn(IssExtL3FilterRstBit, 13, IssExtL3FilterRstBitSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssExtL3FilterNo ,i4SetValIssExtL3FilterRstBit)
#define nmhSetIssExtL3FilterTos(i4IssExtL3FilterNo ,i4SetValIssExtL3FilterTos)	\
	nmhSetCmn(IssExtL3FilterTos, 13, IssExtL3FilterTosSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssExtL3FilterNo ,i4SetValIssExtL3FilterTos)
#define nmhSetIssExtL3FilterDscp(i4IssExtL3FilterNo ,i4SetValIssExtL3FilterDscp)	\
	nmhSetCmn(IssExtL3FilterDscp, 13, IssExtL3FilterDscpSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssExtL3FilterNo ,i4SetValIssExtL3FilterDscp)
#define nmhSetIssExtL3FilterDirection(i4IssExtL3FilterNo ,i4SetValIssExtL3FilterDirection)	\
	nmhSetCmn(IssExtL3FilterDirection, 13, IssExtL3FilterDirectionSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssExtL3FilterNo ,i4SetValIssExtL3FilterDirection)
#define nmhSetIssExtL3FilterAction(i4IssExtL3FilterNo ,i4SetValIssExtL3FilterAction)	\
	nmhSetCmn(IssExtL3FilterAction, 13, IssExtL3FilterActionSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssExtL3FilterNo ,i4SetValIssExtL3FilterAction)
#define nmhSetIssExtL3FilterStatus(i4IssExtL3FilterNo ,i4SetValIssExtL3FilterStatus)	\
	nmhSetCmn(IssExtL3FilterStatus, 13, IssExtL3FilterStatusSet, IssLock, IssUnLock, 0, 1, 1, "%i %i", i4IssExtL3FilterNo ,i4SetValIssExtL3FilterStatus)

#endif
