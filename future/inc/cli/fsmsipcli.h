/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmsipcli.h,v 1.4 2016/02/27 10:13:57 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIStdIpContextId[12];
extern UINT4 FsMIStdIpForwarding[12];
extern UINT4 FsMIStdIpDefaultTTL[12];
extern UINT4 FsMIStdIpv6IpForwarding[12];
extern UINT4 FsMIStdIpv6IpDefaultHopLimit[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIStdIpv4InterfaceIfIndex[12];
extern UINT4 FsMIStdIpv4InterfaceEnableStatus[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIStdIpv6InterfaceIfIndex[12];
extern UINT4 FsMIStdIpv6InterfaceEnableStatus[12];
extern UINT4 FsMIStdIpv6InterfaceForwarding[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIStdIpAddressAddrType[12];
extern UINT4 FsMIStdIpAddressAddr[12];
extern UINT4 FsMIStdIpAddressIfIndex[12];
extern UINT4 FsMIStdIpAddressType[12];
extern UINT4 FsMIStdIpAddressStatus[12];
extern UINT4 FsMIStdIpAddressRowStatus[12];
extern UINT4 FsMIStdIpAddressStorageType[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIStdIpNetToPhysicalIfIndex[12];
extern UINT4 FsMIStdIpNetToPhysicalNetAddressType[12];
extern UINT4 FsMIStdIpNetToPhysicalNetAddress[12];
extern UINT4 FsMIStdIpNetToPhysicalPhysAddress[12];
extern UINT4 FsMIStdIpNetToPhysicalType[12];
extern UINT4 FsMIStdIpNetToPhysicalRowStatus[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIStdIpv6RouterAdvertIfIndex[12];
extern UINT4 FsMIStdIpv6RouterAdvertSendAdverts[12];
extern UINT4 FsMIStdIpv6RouterAdvertMaxInterval[12];
extern UINT4 FsMIStdIpv6RouterAdvertMinInterval[12];
extern UINT4 FsMIStdIpv6RouterAdvertManagedFlag[12];
extern UINT4 FsMIStdIpv6RouterAdvertOtherConfigFlag[12];
extern UINT4 FsMIStdIpv6RouterAdvertLinkMTU[12];
extern UINT4 FsMIStdIpv6RouterAdvertReachableTime[12];
extern UINT4 FsMIStdIpv6RouterAdvertRetransmitTime[12];
extern UINT4 FsMIStdIpv6RouterAdvertCurHopLimit[12];
extern UINT4 FsMIStdIpv6RouterAdvertDefaultLifetime[12];
extern UINT4 FsMIStdIpv6RouterAdvertRowStatus[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIStdInetCidrRouteDestType[12];
extern UINT4 FsMIStdInetCidrRouteDest[12];
extern UINT4 FsMIStdInetCidrRoutePfxLen[12];
extern UINT4 FsMIStdInetCidrRoutePolicy[12];
extern UINT4 FsMIStdInetCidrRouteNextHopType[12];
extern UINT4 FsMIStdInetCidrRouteNextHop[12];
extern UINT4 FsMIStdInetCidrRouteIfIndex[12];
extern UINT4 FsMIStdInetCidrRouteType[12];
extern UINT4 FsMIStdInetCidrRouteNextHopAS[12];
extern UINT4 FsMIStdInetCidrRouteMetric1[12];
extern UINT4 FsMIStdInetCidrRouteMetric2[12];
extern UINT4 FsMIStdInetCidrRouteMetric3[12];
extern UINT4 FsMIStdInetCidrRouteMetric4[12];
extern UINT4 FsMIStdInetCidrRouteMetric5[12];
extern UINT4 FsMIStdInetCidrRouteStatus[12];
extern UINT4 FsMIStdInetCidrRouteAddrType[12];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIStdIpIndex[12];
extern UINT4 FsMIStdIpProxyArpAdminStatus[12];
extern UINT4 FsMIStdIpLocalProxyArpAdminStatus[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIStdIpProxyArpSubnetOption[10];

