/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsptpcli.h,v 1.2 2010/07/09 11:51:57 prabuc Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPtpGlobalSysCtrl[12];
extern UINT4 FsPtpGblTraceOption[12];
extern UINT4 FsPtpPrimaryContext[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPtpGlobalSysCtrl(i4SetValFsPtpGlobalSysCtrl)	\
	nmhSetCmnWithLock(FsPtpGlobalSysCtrl, 12, FsPtpGlobalSysCtrlSet, PtpApiLock, PtpApiUnLock, 0, 0, 0, "%i", i4SetValFsPtpGlobalSysCtrl)
#define nmhSetFsPtpGblTraceOption(pSetValFsPtpGblTraceOption)	\
	nmhSetCmnWithLock(FsPtpGblTraceOption, 12, FsPtpGblTraceOptionSet, PtpApiLock, PtpApiUnLock, 0, 0, 0, "%s", pSetValFsPtpGblTraceOption)
#define nmhSetFsPtpPrimaryContext(i4SetValFsPtpPrimaryContext)	\
	nmhSetCmnWithLock(FsPtpPrimaryContext, 12, FsPtpPrimaryContextSet, PtpApiLock, PtpApiUnLock, 0, 0, 0, "%i", i4SetValFsPtpPrimaryContext)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPtpContextId[14];
extern UINT4 FsPtpAdminStatus[14];
extern UINT4 FsPtpTraceOption[14];
extern UINT4 FsPtpPrimaryDomain[14];
extern UINT4 FsPtpContextRowStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPtpAdminStatus(i4FsPtpContextId ,i4SetValFsPtpAdminStatus)	\
	nmhSetCmnWithLock(FsPtpAdminStatus, 14, FsPtpAdminStatusSet, PtpApiLock, PtpApiUnLock, 0, 0, 1, "%i %i", i4FsPtpContextId ,i4SetValFsPtpAdminStatus)
#define nmhSetFsPtpTraceOption(i4FsPtpContextId ,pSetValFsPtpTraceOption)	\
	nmhSetCmnWithLock(FsPtpTraceOption, 14, FsPtpTraceOptionSet, PtpApiLock, PtpApiUnLock, 0, 0, 1, "%i %s", i4FsPtpContextId ,pSetValFsPtpTraceOption)
#define nmhSetFsPtpPrimaryDomain(i4FsPtpContextId ,i4SetValFsPtpPrimaryDomain)	\
	nmhSetCmnWithLock(FsPtpPrimaryDomain, 14, FsPtpPrimaryDomainSet, PtpApiLock, PtpApiUnLock, 0, 0, 1, "%i %i", i4FsPtpContextId ,i4SetValFsPtpPrimaryDomain)
#define nmhSetFsPtpContextRowStatus(i4FsPtpContextId ,i4SetValFsPtpContextRowStatus)	\
	nmhSetCmnWithLock(FsPtpContextRowStatus, 14, FsPtpContextRowStatusSet, PtpApiLock, PtpApiUnLock, 0, 1, 1, "%i %i", i4FsPtpContextId ,i4SetValFsPtpContextRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPtpDomainNumber[14];
extern UINT4 FsPtpDomainClockMode[14];
extern UINT4 FsPtpDomainGMClusterQueryInterval[14];
extern UINT4 FsPtpDomainRowStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPtpDomainClockMode(i4FsPtpContextId , i4FsPtpDomainNumber ,i4SetValFsPtpDomainClockMode)	\
	nmhSetCmnWithLock(FsPtpDomainClockMode, 14, FsPtpDomainClockModeSet, PtpApiLock, PtpApiUnLock, 0, 0, 2, "%i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber ,i4SetValFsPtpDomainClockMode)
#define nmhSetFsPtpDomainGMClusterQueryInterval(i4FsPtpContextId , i4FsPtpDomainNumber ,i4SetValFsPtpDomainGMClusterQueryInterval)	\
	nmhSetCmnWithLock(FsPtpDomainGMClusterQueryInterval, 14, FsPtpDomainGMClusterQueryIntervalSet, PtpApiLock, PtpApiUnLock, 0, 0, 2, "%i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber ,i4SetValFsPtpDomainGMClusterQueryInterval)
#define nmhSetFsPtpDomainRowStatus(i4FsPtpContextId , i4FsPtpDomainNumber ,i4SetValFsPtpDomainRowStatus)	\
	nmhSetCmnWithLock(FsPtpDomainRowStatus, 14, FsPtpDomainRowStatusSet, PtpApiLock, PtpApiUnLock, 0, 1, 2, "%i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber ,i4SetValFsPtpDomainRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPtpClockTwoStepFlag[14];
extern UINT4 FsPtpClockNumberPorts[14];
extern UINT4 FsPtpClockPriority1[14];
extern UINT4 FsPtpClockPriority2[14];
extern UINT4 FsPtpClockSlaveOnly[14];
extern UINT4 FsPtpClockPathTraceOption[14];
extern UINT4 FsPtpClockSecurityEnabled[14];
extern UINT4 FsPtpClockNumOfSA[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPtpClockTwoStepFlag(i4FsPtpContextId , i4FsPtpDomainNumber ,i4SetValFsPtpClockTwoStepFlag)	\
	nmhSetCmnWithLock(FsPtpClockTwoStepFlag, 14, FsPtpClockTwoStepFlagSet, PtpApiLock, PtpApiUnLock, 0, 0, 2, "%i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber ,i4SetValFsPtpClockTwoStepFlag)
#define nmhSetFsPtpClockNumberPorts(i4FsPtpContextId , i4FsPtpDomainNumber ,i4SetValFsPtpClockNumberPorts)	\
	nmhSetCmnWithLock(FsPtpClockNumberPorts, 14, FsPtpClockNumberPortsSet, PtpApiLock, PtpApiUnLock, 0, 0, 2, "%i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber ,i4SetValFsPtpClockNumberPorts)
#define nmhSetFsPtpClockPriority1(i4FsPtpContextId , i4FsPtpDomainNumber ,i4SetValFsPtpClockPriority1)	\
	nmhSetCmnWithLock(FsPtpClockPriority1, 14, FsPtpClockPriority1Set, PtpApiLock, PtpApiUnLock, 0, 0, 2, "%i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber ,i4SetValFsPtpClockPriority1)
#define nmhSetFsPtpClockPriority2(i4FsPtpContextId , i4FsPtpDomainNumber ,i4SetValFsPtpClockPriority2)	\
	nmhSetCmnWithLock(FsPtpClockPriority2, 14, FsPtpClockPriority2Set, PtpApiLock, PtpApiUnLock, 0, 0, 2, "%i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber ,i4SetValFsPtpClockPriority2)
#define nmhSetFsPtpClockSlaveOnly(i4FsPtpContextId , i4FsPtpDomainNumber ,i4SetValFsPtpClockSlaveOnly)	\
	nmhSetCmnWithLock(FsPtpClockSlaveOnly, 14, FsPtpClockSlaveOnlySet, PtpApiLock, PtpApiUnLock, 0, 0, 2, "%i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber ,i4SetValFsPtpClockSlaveOnly)
#define nmhSetFsPtpClockPathTraceOption(i4FsPtpContextId , i4FsPtpDomainNumber ,i4SetValFsPtpClockPathTraceOption)	\
	nmhSetCmnWithLock(FsPtpClockPathTraceOption, 14, FsPtpClockPathTraceOptionSet, PtpApiLock, PtpApiUnLock, 0, 0, 2, "%i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber ,i4SetValFsPtpClockPathTraceOption)
#define nmhSetFsPtpClockSecurityEnabled(i4FsPtpContextId , i4FsPtpDomainNumber ,i4SetValFsPtpClockSecurityEnabled)	\
	nmhSetCmnWithLock(FsPtpClockSecurityEnabled, 14, FsPtpClockSecurityEnabledSet, PtpApiLock, PtpApiUnLock, 0, 0, 2, "%i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber ,i4SetValFsPtpClockSecurityEnabled)
#define nmhSetFsPtpClockNumOfSA(i4FsPtpContextId , i4FsPtpDomainNumber ,u4SetValFsPtpClockNumOfSA)	\
	nmhSetCmnWithLock(FsPtpClockNumOfSA, 14, FsPtpClockNumOfSASet, PtpApiLock, PtpApiUnLock, 0, 0, 2, "%i %i %u", i4FsPtpContextId , i4FsPtpDomainNumber ,u4SetValFsPtpClockNumOfSA)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPtpPortIndex[14];
extern UINT4 FsPtpPortClockIdentity[14];
extern UINT4 FsPtpPortInterfaceType[14];
extern UINT4 FsPtpPortIfaceNumber[14];
extern UINT4 FsPtpPortMinDelayReqInterval[14];
extern UINT4 FsPtpPortAnnounceInterval[14];
extern UINT4 FsPtpPortAnnounceReceiptTimeout[14];
extern UINT4 FsPtpPortSyncInterval[14];
extern UINT4 FsPtpPortSynclimit[14];
extern UINT4 FsPtpPortDelayMechanism[14];
extern UINT4 FsPtpPortMinPdelayReqInterval[14];
extern UINT4 FsPtpPortVersionNumber[14];
extern UINT4 FsPtpPortUnicastNegOption[14];
extern UINT4 FsPtpPortUnicastMasterMaxSize[14];
extern UINT4 FsPtpPortAccMasterEnabled[14];
extern UINT4 FsPtpPortNumOfAltMaster[14];
extern UINT4 FsPtpPortAltMulcastSync[14];
extern UINT4 FsPtpPortAltMulcastSyncInterval[14];
extern UINT4 FsPtpPortPtpStatus[14];
extern UINT4 FsPtpPortRowStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPtpPortClockIdentity(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex ,pSetValFsPtpPortClockIdentity)	\
	nmhSetCmnWithLock(FsPtpPortClockIdentity, 14, FsPtpPortClockIdentitySet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %s", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex ,pSetValFsPtpPortClockIdentity)
#define nmhSetFsPtpPortInterfaceType(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex ,i4SetValFsPtpPortInterfaceType)	\
	nmhSetCmnWithLock(FsPtpPortInterfaceType, 14, FsPtpPortInterfaceTypeSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex ,i4SetValFsPtpPortInterfaceType)
#define nmhSetFsPtpPortIfaceNumber(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex ,i4SetValFsPtpPortIfaceNumber)	\
	nmhSetCmnWithLock(FsPtpPortIfaceNumber, 14, FsPtpPortIfaceNumberSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex ,i4SetValFsPtpPortIfaceNumber)
#define nmhSetFsPtpPortMinDelayReqInterval(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex ,i4SetValFsPtpPortMinDelayReqInterval)	\
	nmhSetCmnWithLock(FsPtpPortMinDelayReqInterval, 14, FsPtpPortMinDelayReqIntervalSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex ,i4SetValFsPtpPortMinDelayReqInterval)
#define nmhSetFsPtpPortAnnounceInterval(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex ,i4SetValFsPtpPortAnnounceInterval)	\
	nmhSetCmnWithLock(FsPtpPortAnnounceInterval, 14, FsPtpPortAnnounceIntervalSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex ,i4SetValFsPtpPortAnnounceInterval)
#define nmhSetFsPtpPortAnnounceReceiptTimeout(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex ,i4SetValFsPtpPortAnnounceReceiptTimeout)	\
	nmhSetCmnWithLock(FsPtpPortAnnounceReceiptTimeout, 14, FsPtpPortAnnounceReceiptTimeoutSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex ,i4SetValFsPtpPortAnnounceReceiptTimeout)
#define nmhSetFsPtpPortSyncInterval(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex ,i4SetValFsPtpPortSyncInterval)	\
	nmhSetCmnWithLock(FsPtpPortSyncInterval, 14, FsPtpPortSyncIntervalSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex ,i4SetValFsPtpPortSyncInterval)
#define nmhSetFsPtpPortSynclimit(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex ,pSetValFsPtpPortSynclimit)	\
	nmhSetCmnWithLock(FsPtpPortSynclimit, 14, FsPtpPortSynclimitSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %s", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex ,pSetValFsPtpPortSynclimit)
#define nmhSetFsPtpPortDelayMechanism(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex ,i4SetValFsPtpPortDelayMechanism)	\
	nmhSetCmnWithLock(FsPtpPortDelayMechanism, 14, FsPtpPortDelayMechanismSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex ,i4SetValFsPtpPortDelayMechanism)
#define nmhSetFsPtpPortMinPdelayReqInterval(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex ,i4SetValFsPtpPortMinPdelayReqInterval)	\
	nmhSetCmnWithLock(FsPtpPortMinPdelayReqInterval, 14, FsPtpPortMinPdelayReqIntervalSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex ,i4SetValFsPtpPortMinPdelayReqInterval)
#define nmhSetFsPtpPortVersionNumber(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex ,i4SetValFsPtpPortVersionNumber)	\
	nmhSetCmnWithLock(FsPtpPortVersionNumber, 14, FsPtpPortVersionNumberSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex ,i4SetValFsPtpPortVersionNumber)
#define nmhSetFsPtpPortUnicastNegOption(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex ,i4SetValFsPtpPortUnicastNegOption)	\
	nmhSetCmnWithLock(FsPtpPortUnicastNegOption, 14, FsPtpPortUnicastNegOptionSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex ,i4SetValFsPtpPortUnicastNegOption)
#define nmhSetFsPtpPortUnicastMasterMaxSize(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex ,i4SetValFsPtpPortUnicastMasterMaxSize)	\
	nmhSetCmnWithLock(FsPtpPortUnicastMasterMaxSize, 14, FsPtpPortUnicastMasterMaxSizeSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex ,i4SetValFsPtpPortUnicastMasterMaxSize)
#define nmhSetFsPtpPortAccMasterEnabled(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex ,i4SetValFsPtpPortAccMasterEnabled)	\
	nmhSetCmnWithLock(FsPtpPortAccMasterEnabled, 14, FsPtpPortAccMasterEnabledSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex ,i4SetValFsPtpPortAccMasterEnabled)
#define nmhSetFsPtpPortNumOfAltMaster(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex ,i4SetValFsPtpPortNumOfAltMaster)	\
	nmhSetCmnWithLock(FsPtpPortNumOfAltMaster, 14, FsPtpPortNumOfAltMasterSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex ,i4SetValFsPtpPortNumOfAltMaster)
#define nmhSetFsPtpPortAltMulcastSync(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex ,i4SetValFsPtpPortAltMulcastSync)	\
	nmhSetCmnWithLock(FsPtpPortAltMulcastSync, 14, FsPtpPortAltMulcastSyncSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex ,i4SetValFsPtpPortAltMulcastSync)
#define nmhSetFsPtpPortAltMulcastSyncInterval(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex ,i4SetValFsPtpPortAltMulcastSyncInterval)	\
	nmhSetCmnWithLock(FsPtpPortAltMulcastSyncInterval, 14, FsPtpPortAltMulcastSyncIntervalSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex ,i4SetValFsPtpPortAltMulcastSyncInterval)
#define nmhSetFsPtpPortPtpStatus(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex ,i4SetValFsPtpPortPtpStatus)	\
	nmhSetCmnWithLock(FsPtpPortPtpStatus, 14, FsPtpPortPtpStatusSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex ,i4SetValFsPtpPortPtpStatus)
#define nmhSetFsPtpPortRowStatus(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex ,i4SetValFsPtpPortRowStatus)	\
	nmhSetCmnWithLock(FsPtpPortRowStatus, 14, FsPtpPortRowStatusSet, PtpApiLock, PtpApiUnLock, 0, 1, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex ,i4SetValFsPtpPortRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPtpTransparentClockTwoStepFlag[14];
extern UINT4 FsPtpTransparentClockNumberPorts[14];
extern UINT4 FsPtpTransparentClockDelaymechanism[14];
extern UINT4 FsPtpTransparentClockPrimaryDomain[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPtpTransparentClockTwoStepFlag(i4FsPtpContextId , i4FsPtpDomainNumber ,i4SetValFsPtpTransparentClockTwoStepFlag)	\
	nmhSetCmnWithLock(FsPtpTransparentClockTwoStepFlag, 14, FsPtpTransparentClockTwoStepFlagSet, PtpApiLock, PtpApiUnLock, 0, 0, 2, "%i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber ,i4SetValFsPtpTransparentClockTwoStepFlag)
#define nmhSetFsPtpTransparentClockNumberPorts(i4FsPtpContextId , i4FsPtpDomainNumber ,i4SetValFsPtpTransparentClockNumberPorts)	\
	nmhSetCmnWithLock(FsPtpTransparentClockNumberPorts, 14, FsPtpTransparentClockNumberPortsSet, PtpApiLock, PtpApiUnLock, 0, 0, 2, "%i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber ,i4SetValFsPtpTransparentClockNumberPorts)
#define nmhSetFsPtpTransparentClockDelaymechanism(i4FsPtpContextId , i4FsPtpDomainNumber ,i4SetValFsPtpTransparentClockDelaymechanism)	\
	nmhSetCmnWithLock(FsPtpTransparentClockDelaymechanism, 14, FsPtpTransparentClockDelaymechanismSet, PtpApiLock, PtpApiUnLock, 0, 0, 2, "%i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber ,i4SetValFsPtpTransparentClockDelaymechanism)
#define nmhSetFsPtpTransparentClockPrimaryDomain(i4FsPtpContextId , i4FsPtpDomainNumber ,i4SetValFsPtpTransparentClockPrimaryDomain)	\
	nmhSetCmnWithLock(FsPtpTransparentClockPrimaryDomain, 14, FsPtpTransparentClockPrimaryDomainSet, PtpApiLock, PtpApiUnLock, 0, 0, 2, "%i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber ,i4SetValFsPtpTransparentClockPrimaryDomain)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPtpTransparentPortIndex[14];
extern UINT4 FsPtpTransparentPortInterfaceType[14];
extern UINT4 FsPtpTransparentPortIfaceNumber[14];
extern UINT4 FsPtpTransparentPortClockIdentity[14];
extern UINT4 FsPtpTransparentPortMinPdelayReqInterval[14];
extern UINT4 FsPtpTransparentPortPtpStatus[14];
extern UINT4 FsPtpTransparentPortRowStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPtpTransparentPortInterfaceType(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpTransparentPortIndex ,i4SetValFsPtpTransparentPortInterfaceType)	\
	nmhSetCmnWithLock(FsPtpTransparentPortInterfaceType, 14, FsPtpTransparentPortInterfaceTypeSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpTransparentPortIndex ,i4SetValFsPtpTransparentPortInterfaceType)
#define nmhSetFsPtpTransparentPortIfaceNumber(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpTransparentPortIndex ,i4SetValFsPtpTransparentPortIfaceNumber)	\
	nmhSetCmnWithLock(FsPtpTransparentPortIfaceNumber, 14, FsPtpTransparentPortIfaceNumberSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpTransparentPortIndex ,i4SetValFsPtpTransparentPortIfaceNumber)
#define nmhSetFsPtpTransparentPortClockIdentity(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpTransparentPortIndex ,pSetValFsPtpTransparentPortClockIdentity)	\
	nmhSetCmnWithLock(FsPtpTransparentPortClockIdentity, 14, FsPtpTransparentPortClockIdentitySet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %s", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpTransparentPortIndex ,pSetValFsPtpTransparentPortClockIdentity)
#define nmhSetFsPtpTransparentPortMinPdelayReqInterval(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpTransparentPortIndex ,i4SetValFsPtpTransparentPortMinPdelayReqInterval)	\
	nmhSetCmnWithLock(FsPtpTransparentPortMinPdelayReqInterval, 14, FsPtpTransparentPortMinPdelayReqIntervalSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpTransparentPortIndex ,i4SetValFsPtpTransparentPortMinPdelayReqInterval)
#define nmhSetFsPtpTransparentPortPtpStatus(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpTransparentPortIndex ,i4SetValFsPtpTransparentPortPtpStatus)	\
	nmhSetCmnWithLock(FsPtpTransparentPortPtpStatus, 14, FsPtpTransparentPortPtpStatusSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpTransparentPortIndex ,i4SetValFsPtpTransparentPortPtpStatus)
#define nmhSetFsPtpTransparentPortRowStatus(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpTransparentPortIndex ,i4SetValFsPtpTransparentPortRowStatus)	\
	nmhSetCmnWithLock(FsPtpTransparentPortRowStatus, 14, FsPtpTransparentPortRowStatusSet, PtpApiLock, PtpApiUnLock, 0, 1, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpTransparentPortIndex ,i4SetValFsPtpTransparentPortRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPtpGrandMasterClusterNetworkProtocol[14];
extern UINT4 FsPtpGrandMasterClusterAddLength[14];
extern UINT4 FsPtpGrandMasterClusterAddr[14];
extern UINT4 FsPtpGrandMasterClusterRowStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPtpGrandMasterClusterRowStatus(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpGrandMasterClusterNetworkProtocol , i4FsPtpGrandMasterClusterAddLength , pFsPtpGrandMasterClusterAddr ,i4SetValFsPtpGrandMasterClusterRowStatus)	\
	nmhSetCmnWithLock(FsPtpGrandMasterClusterRowStatus, 14, FsPtpGrandMasterClusterRowStatusSet, PtpApiLock, PtpApiUnLock, 0, 1, 5, "%i %i %i %i %s %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpGrandMasterClusterNetworkProtocol , i4FsPtpGrandMasterClusterAddLength , pFsPtpGrandMasterClusterAddr ,i4SetValFsPtpGrandMasterClusterRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPtpUnicastMasterNetworkProtocol[14];
extern UINT4 FsPtpUnicastMasterAddLength[14];
extern UINT4 FsPtpUnicastMasterAddr[14];
extern UINT4 FsPtpUnicastMasterRowStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPtpUnicastMasterRowStatus(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex , i4FsPtpUnicastMasterNetworkProtocol , i4FsPtpUnicastMasterAddLength , pFsPtpUnicastMasterAddr ,i4SetValFsPtpUnicastMasterRowStatus)	\
	nmhSetCmnWithLock(FsPtpUnicastMasterRowStatus, 14, FsPtpUnicastMasterRowStatusSet, PtpApiLock, PtpApiUnLock, 0, 1, 6, "%i %i %i %i %i %s %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpPortIndex , i4FsPtpUnicastMasterNetworkProtocol , i4FsPtpUnicastMasterAddLength , pFsPtpUnicastMasterAddr ,i4SetValFsPtpUnicastMasterRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPtpAccMasterNetworkProtocol[14];
extern UINT4 FsPtpAccMasterAddLength[14];
extern UINT4 FsPtpAccMasterAddr[14];
extern UINT4 FsPtpAccMasterAlternatePriority[14];
extern UINT4 FsPtpAccMasterRowStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPtpAccMasterAlternatePriority(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpAccMasterNetworkProtocol , i4FsPtpAccMasterAddLength , pFsPtpAccMasterAddr ,i4SetValFsPtpAccMasterAlternatePriority)	\
	nmhSetCmnWithLock(FsPtpAccMasterAlternatePriority, 14, FsPtpAccMasterAlternatePrioritySet, PtpApiLock, PtpApiUnLock, 0, 0, 5, "%i %i %i %i %s %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpAccMasterNetworkProtocol , i4FsPtpAccMasterAddLength , pFsPtpAccMasterAddr ,i4SetValFsPtpAccMasterAlternatePriority)
#define nmhSetFsPtpAccMasterRowStatus(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpAccMasterNetworkProtocol , i4FsPtpAccMasterAddLength , pFsPtpAccMasterAddr ,i4SetValFsPtpAccMasterRowStatus)	\
	nmhSetCmnWithLock(FsPtpAccMasterRowStatus, 14, FsPtpAccMasterRowStatusSet, PtpApiLock, PtpApiUnLock, 0, 1, 5, "%i %i %i %i %s %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpAccMasterNetworkProtocol , i4FsPtpAccMasterAddLength , pFsPtpAccMasterAddr ,i4SetValFsPtpAccMasterRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPtpSecKeyId[14];
extern UINT4 FsPtpSecKeyAlgorithmId[14];
extern UINT4 FsPtpSecKeyLength[14];
extern UINT4 FsPtpSecKey[14];
extern UINT4 FsPtpSecKeyStartTime[14];
extern UINT4 FsPtpSecKeyExpirationTime[14];
extern UINT4 FsPtpSecKeyValid[14];
extern UINT4 FsPtpSecKeyRowStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPtpSecKeyAlgorithmId(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSecKeyId ,i4SetValFsPtpSecKeyAlgorithmId)	\
	nmhSetCmnWithLock(FsPtpSecKeyAlgorithmId, 14, FsPtpSecKeyAlgorithmIdSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSecKeyId ,i4SetValFsPtpSecKeyAlgorithmId)
#define nmhSetFsPtpSecKeyLength(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSecKeyId ,i4SetValFsPtpSecKeyLength)	\
	nmhSetCmnWithLock(FsPtpSecKeyLength, 14, FsPtpSecKeyLengthSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSecKeyId ,i4SetValFsPtpSecKeyLength)
#define nmhSetFsPtpSecKey(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSecKeyId ,pSetValFsPtpSecKey)	\
	nmhSetCmnWithLock(FsPtpSecKey, 14, FsPtpSecKeySet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %s", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSecKeyId ,pSetValFsPtpSecKey)
#define nmhSetFsPtpSecKeyStartTime(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSecKeyId ,u4SetValFsPtpSecKeyStartTime)	\
	nmhSetCmnWithLock(FsPtpSecKeyStartTime, 14, FsPtpSecKeyStartTimeSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %t", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSecKeyId ,u4SetValFsPtpSecKeyStartTime)
#define nmhSetFsPtpSecKeyExpirationTime(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSecKeyId ,u4SetValFsPtpSecKeyExpirationTime)	\
	nmhSetCmnWithLock(FsPtpSecKeyExpirationTime, 14, FsPtpSecKeyExpirationTimeSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %t", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSecKeyId ,u4SetValFsPtpSecKeyExpirationTime)
#define nmhSetFsPtpSecKeyValid(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSecKeyId ,i4SetValFsPtpSecKeyValid)	\
	nmhSetCmnWithLock(FsPtpSecKeyValid, 14, FsPtpSecKeyValidSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSecKeyId ,i4SetValFsPtpSecKeyValid)
#define nmhSetFsPtpSecKeyRowStatus(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSecKeyId ,i4SetValFsPtpSecKeyRowStatus)	\
	nmhSetCmnWithLock(FsPtpSecKeyRowStatus, 14, FsPtpSecKeyRowStatusSet, PtpApiLock, PtpApiUnLock, 0, 1, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSecKeyId ,i4SetValFsPtpSecKeyRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPtpSAId[14];
extern UINT4 FsPtpSASrcPortNumber[14];
extern UINT4 FsPtpSASrcAddrLength[14];
extern UINT4 FsPtpSASrcAddr[14];
extern UINT4 FsPtpSADstPortNumber[14];
extern UINT4 FsPtpSADstAddrLength[14];
extern UINT4 FsPtpSADstAddr[14];
extern UINT4 FsPtpSASrcClockIdentity[14];
extern UINT4 FsPtpSADstClockIdentity[14];
extern UINT4 FsPtpSAKeyId[14];
extern UINT4 FsPtpSANextKeyId[14];
extern UINT4 FsPtpSATrustTimeout[14];
extern UINT4 FsPtpSAChallengeTimeOut[14];
extern UINT4 FsPtpSAChallengeRequired[14];
extern UINT4 FsPtpSAResponseRequired[14];
extern UINT4 FsPtpSADirection[14];
extern UINT4 FsPtpSARowStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPtpSASrcPortNumber(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSAId ,i4SetValFsPtpSASrcPortNumber)	\
	nmhSetCmnWithLock(FsPtpSASrcPortNumber, 14, FsPtpSASrcPortNumberSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSAId ,i4SetValFsPtpSASrcPortNumber)
#define nmhSetFsPtpSASrcAddrLength(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSAId ,i4SetValFsPtpSASrcAddrLength)	\
	nmhSetCmnWithLock(FsPtpSASrcAddrLength, 14, FsPtpSASrcAddrLengthSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSAId ,i4SetValFsPtpSASrcAddrLength)
#define nmhSetFsPtpSASrcAddr(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSAId ,pSetValFsPtpSASrcAddr)	\
	nmhSetCmnWithLock(FsPtpSASrcAddr, 14, FsPtpSASrcAddrSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %s", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSAId ,pSetValFsPtpSASrcAddr)
#define nmhSetFsPtpSADstPortNumber(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSAId ,i4SetValFsPtpSADstPortNumber)	\
	nmhSetCmnWithLock(FsPtpSADstPortNumber, 14, FsPtpSADstPortNumberSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSAId ,i4SetValFsPtpSADstPortNumber)
#define nmhSetFsPtpSADstAddrLength(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSAId ,i4SetValFsPtpSADstAddrLength)	\
	nmhSetCmnWithLock(FsPtpSADstAddrLength, 14, FsPtpSADstAddrLengthSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSAId ,i4SetValFsPtpSADstAddrLength)
#define nmhSetFsPtpSADstAddr(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSAId ,pSetValFsPtpSADstAddr)	\
	nmhSetCmnWithLock(FsPtpSADstAddr, 14, FsPtpSADstAddrSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %s", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSAId ,pSetValFsPtpSADstAddr)
#define nmhSetFsPtpSASrcClockIdentity(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSAId ,pSetValFsPtpSASrcClockIdentity)	\
	nmhSetCmnWithLock(FsPtpSASrcClockIdentity, 14, FsPtpSASrcClockIdentitySet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %s", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSAId ,pSetValFsPtpSASrcClockIdentity)
#define nmhSetFsPtpSADstClockIdentity(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSAId ,pSetValFsPtpSADstClockIdentity)	\
	nmhSetCmnWithLock(FsPtpSADstClockIdentity, 14, FsPtpSADstClockIdentitySet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %s", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSAId ,pSetValFsPtpSADstClockIdentity)
#define nmhSetFsPtpSAKeyId(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSAId ,i4SetValFsPtpSAKeyId)	\
	nmhSetCmnWithLock(FsPtpSAKeyId, 14, FsPtpSAKeyIdSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSAId ,i4SetValFsPtpSAKeyId)
#define nmhSetFsPtpSANextKeyId(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSAId ,i4SetValFsPtpSANextKeyId)	\
	nmhSetCmnWithLock(FsPtpSANextKeyId, 14, FsPtpSANextKeyIdSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSAId ,i4SetValFsPtpSANextKeyId)
#define nmhSetFsPtpSATrustTimeout(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSAId ,i4SetValFsPtpSATrustTimeout)	\
	nmhSetCmnWithLock(FsPtpSATrustTimeout, 14, FsPtpSATrustTimeoutSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSAId ,i4SetValFsPtpSATrustTimeout)
#define nmhSetFsPtpSAChallengeTimeOut(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSAId ,i4SetValFsPtpSAChallengeTimeOut)	\
	nmhSetCmnWithLock(FsPtpSAChallengeTimeOut, 14, FsPtpSAChallengeTimeOutSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSAId ,i4SetValFsPtpSAChallengeTimeOut)
#define nmhSetFsPtpSAChallengeRequired(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSAId ,i4SetValFsPtpSAChallengeRequired)	\
	nmhSetCmnWithLock(FsPtpSAChallengeRequired, 14, FsPtpSAChallengeRequiredSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSAId ,i4SetValFsPtpSAChallengeRequired)
#define nmhSetFsPtpSAResponseRequired(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSAId ,i4SetValFsPtpSAResponseRequired)	\
	nmhSetCmnWithLock(FsPtpSAResponseRequired, 14, FsPtpSAResponseRequiredSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSAId ,i4SetValFsPtpSAResponseRequired)
#define nmhSetFsPtpSADirection(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSAId ,i4SetValFsPtpSADirection)	\
	nmhSetCmnWithLock(FsPtpSADirection, 14, FsPtpSADirectionSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSAId ,i4SetValFsPtpSADirection)
#define nmhSetFsPtpSARowStatus(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSAId ,i4SetValFsPtpSARowStatus)	\
	nmhSetCmnWithLock(FsPtpSARowStatus, 14, FsPtpSARowStatusSet, PtpApiLock, PtpApiUnLock, 0, 1, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpSAId ,i4SetValFsPtpSARowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPtpAltTimeScaleKeyId[14];
extern UINT4 FsPtpAltTimeScalecurrentOffset[14];
extern UINT4 FsPtpAltTimeScalejumpSeconds[14];
extern UINT4 FsPtpAltTimeScaletimeOfNextJump[14];
extern UINT4 FsPtpAltTimeScaledisplayName[14];
extern UINT4 FsPtpAltTimeScaleRowStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPtpAltTimeScalecurrentOffset(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpAltTimeScaleKeyId ,i4SetValFsPtpAltTimeScalecurrentOffset)	\
	nmhSetCmnWithLock(FsPtpAltTimeScalecurrentOffset, 14, FsPtpAltTimeScalecurrentOffsetSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpAltTimeScaleKeyId ,i4SetValFsPtpAltTimeScalecurrentOffset)
#define nmhSetFsPtpAltTimeScalejumpSeconds(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpAltTimeScaleKeyId ,i4SetValFsPtpAltTimeScalejumpSeconds)	\
	nmhSetCmnWithLock(FsPtpAltTimeScalejumpSeconds, 14, FsPtpAltTimeScalejumpSecondsSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpAltTimeScaleKeyId ,i4SetValFsPtpAltTimeScalejumpSeconds)
#define nmhSetFsPtpAltTimeScaletimeOfNextJump(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpAltTimeScaleKeyId ,pSetValFsPtpAltTimeScaletimeOfNextJump)	\
	nmhSetCmnWithLock(FsPtpAltTimeScaletimeOfNextJump, 14, FsPtpAltTimeScaletimeOfNextJumpSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %s", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpAltTimeScaleKeyId ,pSetValFsPtpAltTimeScaletimeOfNextJump)
#define nmhSetFsPtpAltTimeScaledisplayName(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpAltTimeScaleKeyId ,pSetValFsPtpAltTimeScaledisplayName)	\
	nmhSetCmnWithLock(FsPtpAltTimeScaledisplayName, 14, FsPtpAltTimeScaledisplayNameSet, PtpApiLock, PtpApiUnLock, 0, 0, 3, "%i %i %i %s", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpAltTimeScaleKeyId ,pSetValFsPtpAltTimeScaledisplayName)
#define nmhSetFsPtpAltTimeScaleRowStatus(i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpAltTimeScaleKeyId ,i4SetValFsPtpAltTimeScaleRowStatus)	\
	nmhSetCmnWithLock(FsPtpAltTimeScaleRowStatus, 14, FsPtpAltTimeScaleRowStatusSet, PtpApiLock, PtpApiUnLock, 0, 1, 3, "%i %i %i %i", i4FsPtpContextId , i4FsPtpDomainNumber , i4FsPtpAltTimeScaleKeyId ,i4SetValFsPtpAltTimeScaleRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPtpTrapContextName[11];
extern UINT4 FsPtpTrapDomainNumber[11];
extern UINT4 FsPtpNotification[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPtpTrapContextName(pSetValFsPtpTrapContextName)	\
	nmhSetCmnWithLock(FsPtpTrapContextName, 11, FsPtpTrapContextNameSet, PtpApiLock, PtpApiUnLock, 0, 0, 0, "%s", pSetValFsPtpTrapContextName)
#define nmhSetFsPtpTrapDomainNumber(i4SetValFsPtpTrapDomainNumber)	\
	nmhSetCmnWithLock(FsPtpTrapDomainNumber, 11, FsPtpTrapDomainNumberSet, PtpApiLock, PtpApiUnLock, 0, 0, 0, "%i", i4SetValFsPtpTrapDomainNumber)
#define nmhSetFsPtpNotification(pSetValFsPtpNotification)	\
	nmhSetCmnWithLock(FsPtpNotification, 11, FsPtpNotificationSet, PtpApiLock, PtpApiUnLock, 0, 0, 0, "%s", pSetValFsPtpNotification)

#endif
