/* $Id: d6rlcli.h,v 1.14 2014/12/26 10:28:46 siva Exp $   */

#ifndef _DHCP6_RLY_CLI_H
#define _DHCP6_RLY_CLI_H
#define DHCP6_RLY_CLI_MAX_ARGS    4
#define DHCP6_RLY_INVALID_PKT_IN_TRAP         1
#define DHCP6_RLY_MAX_HOP_THRESHOLD_TRAP      2
#define DHCP6_RLY_ALL_TRAP                    3
#define DHCP6_RLY_TRC_MAX_SIZE                255
#define DHCP6_RLY_REMOTEID_CLI_DUID      1
#define DHCP6_RLY_REMOTEID_CLI_SW_NAME   2
#define DHCP6_RLY_REMOTEID_CLI_MGMT_IP   3
#define DHCP6_RLY_REMOTEID_CLI_USERDEFINED 4

/* Proto types */
PUBLIC INT4
cli_process_dhcp6r_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command, ...));

PUBLIC INT4
cli_process_dhcp6r_test_cmd PROTO ((tCliHandle CliHandle, ...));

VOID IssDhcp6RelayShowDebugging(tCliHandle CliHandle);

PUBLIC INT4
D6RlCliSetIfEntry PROTO ((tCliHandle CliHandle, UINT4 u4Index, 
                          tIp6Addr  *pSrvAddr, UINT4 u4OutIf, 
                          UINT4 * pu4ErrorCode));
PUBLIC INT4
D6RlCliDelIfEntry PROTO ((tCliHandle CliHandle, UINT4 u4Index, 
                          tIp6Addr  *pSrvAddr, UINT4 u4OutIf, 
                          UINT4 * pu4ErrorCode));

PUBLIC INT4
D6RlCliSetHopThreshold PROTO ((tCliHandle CliHandle, UINT4 u4Index,
                               UINT4 u4Value,UINT4 *pu4ErrorCode));

PUBLIC INT4
D6RlCliClearStatistics PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex,
                               UINT4 *pu4ErrorCode));

PUBLIC INT4
D6RlCliSetRemoteIdType PROTO ((tCliHandle CliHandle, UINT4 u4Index,
                               UINT4 u4Value,UINT4 *pu4ErrorCode));

PUBLIC INT4
D6RlCliSetRemoteIdDuid PROTO ((tCliHandle CliHandle, UINT4 u4Index,
                               UINT1 *pu1Value, 
          UINT1 u1RemIdDuidLen,
          UINT4 *pu4ErrorCode));

PUBLIC INT4
D6RlCliSetRemoteIdUserdefined (tCliHandle CliHandle, UINT4 u4IfIndex,
                        UINT1 *pu1RemIdUserDefined, UINT1 u1RemIdLen,
                        UINT4 *pu4ErrorCode);


PUBLIC INT4
D6RlCliShowInterface PROTO ((tCliHandle CliHandle, UINT4 u4Index));

PUBLIC INT4
D6RlCliShowStatistics PROTO ((tCliHandle CliHandle, UINT4 u4Index,
                              UINT4 * pu4ErrorCode));
PUBLIC INT4
D6RlCliPrintStatistics PROTO ((tCliHandle CliHandle, UINT4 u4Index,
                               UINT4 * pu4ErrorCode));

PUBLIC INT4
D6RlCliSetTraps PROTO ((tCliHandle CliHandle, UINT4 u4Command, 
                        UINT4 u4TrapValue));
PUBLIC INT4
D6RlCliSetSysLogStatus PROTO ((tCliHandle CliHandle, INT4 i4Status,
                               UINT4 *pu4ErrorCode));
PUBLIC INT4
D6RlCliSetRemoteIdStatus PROTO ((tCliHandle CliHandle, INT4 i4Status,
                               UINT4 *pu4ErrorCode));
PUBLIC INT4
D6RlCliSetPDRouteStatus PROTO ((tCliHandle CliHandle, INT4 i4Status,UINT4 *pu4ErrorCode));

PUBLIC INT4
D6RlCliSetDebugs PROTO ((tCliHandle CliHandle, UINT1 *pu1TraceInput));

PUBLIC INT4 D6RlCliSetClntUdpPorts PROTO ((tCliHandle,UINT4 ,UINT4 ));
PUBLIC INT4 D6RlCliShowRunningConfig PROTO ((tCliHandle CliHandle,UINT4 u4Module));
INT4 D6RlCliShowRunningScalars PROTO ((tCliHandle CliHandle));
VOID
D6RlCliShowRunningConfigInterface PROTO ((tCliHandle CliHandle));
PUBLIC VOID
D6RlCliShowRunningConfigInterfaceDetails PROTO ((tCliHandle CliHandle, INT4 i4IfIndex));
PUBLIC VOID
D6RlCliShowGlobalInfo (tCliHandle CliHandle);

PUBLIC INT4
D6RlCliShowRelayInfo (tCliHandle CliHandle);
PUBLIC INT4 D6RlCliShowGlbRelayInfo (tCliHandle CliHandle);
PUBLIC INT4
D6RlCliShowIfRelayInfo (tCliHandle CliHandle,INT4 i4IfIndex);



/* Enum declaration for DHCP6 RLY command identifiers */

enum
{
    CLI_DHCP6_RLY_IPV6_RELAY =1,
    CLI_DHCP6_RLY_NO_IPV6_RELAY,
    CLI_DHCP6_RLY_HOP_THRESHOLD,
    CLI_DHCP6_RLY_NO_HOP_THRESHOLD,
    CLI_DHCP6_RLY_CLEAR_INTERFACE_COUNTERS,
    CLI_DHCP6_RLY_CLEAR_ALL_INTERFACE_COUNTERS,
    CLI_DHCP6_RLY_ENABLE_TRAP,
    CLI_DHCP6_RLY_DISABLE_TRAP,
    CLI_DHCP6_RLY_SYSLOG,
    CLI_DHCP6_RLY_NO_SYSLOG,
    CLI_DHCP6_RLY_SHOW_INTF,
    CLI_DHCP6_RLY_SHOW_ALL_INTF,
    CLI_DHCP6_RLY_SHOW_GLOB_INTF,
    CLI_DHCP6_RLY_DEBUG,
    CLI_DHCP6_RLY_NO_DEBUG,
    CLI_DHCP6_RLY_SHOW_STATISTICS,
    CLI_DHCP6_RLY_SHOW_ALL_STATISTICS,
    CLI_DHCP6_RLY_UDP_SOURCE_DEST_PORT,
    CLI_DHCP6_RLY_REMOTEID_STATUS,
    CLI_DHCP6_RLY_REMOTEID_TYPE,
    CLI_DHCP6_RLY_RID_DUID,
    CLI_DHCP6_RLY_RID_USERDEFINED,
    CLI_DHCP6_RLY_PD_FORWARDING,
    CLI_DHCP6_RLY_NO_PD_FORWARDING,
    CLI_DHCP6_RLY_MAX_CMDS
};

#define DHCP6_RLY_UDP_LISTEN_PORT_NUM     1 
#define DHCP6_RLY_UDP_CLNT_TRANS_PORT_NUM 2 
#define DHCP6_RLY_UDP_SRV_TRANS_PORT_NUM  3 

/* Defines Remote-Id status */
#define DHCP6_RLY_REMOTEID_ENABLE         1   /* Enable */
#define DHCP6_RLY_REMOTEID_DISABLE        2   /* Disable */

/* Defines PD Forwarding status */
#define DHCP6_RLY_PDROUTE_ENABLE         1   /* Enable */
#define DHCP6_RLY_PDROUTE_DISABLE        2   /* Disable */

#define DHCP6_RLY_CLI_STR_LEN         383 /* ((128*2) + (128-1))*/

/* Error Codes */

enum
{
    DHCP6_RLY_UNABLE_TO_SET= 1,
    DHCP6_RLY_INVALID_INTF,
    DHCP6_RLY_UNEXPECTED,
    DHCP6_RLY_INVALID_CMD,
    DHCP6_RLY_NOT_ENABLED,
    DHCP6_RLY_CANNOT_LISTEN,
    DHCP6_RLY_INVALID_PORT,
    DHCP6_RLY_INVALID_DEBUG_STRING,
    DHCP6_RLY_CANNOT_DELETE,
 DHCP6_RLY_WRONG_LENGTH,
 DHCP6_RLY_DUPLICATE_REMOTE_ID_VALUE,
 DHCP6_RLY_WRONG_VALUE,
 DHCP6_RLY_INCONSISTENT_VALUE,
 DHCP6_RLY_REMID_OPTION_DISABLED,
 DHCP6_RLY_PDROUTE_CONTROL_WRONG_VALUE,
 DHCP6_RLY_INVALID_SERVER,
 DHCP6_RLY_INTERFACE_ALREADY_CREATED,
 DHCP6_RLY_INVALID_INTERFACE,
    DHCP6_RLY_MAX_SERVER,
    DHCP6_RLY_INTERFACE_CREATION_FAILED,
    DHCP6_RLY_MAX_ERR
};

#if defined (_DHCP6_RLY_CLI_C)
/* Error Strings */

CONST CHR1 * gaDhcp6RlyCliErrString [] = {
    NULL,
    "% Internal Error :Unable to Create Data Structures!\r\n",
    "% Invalid Interface!\r\n",
    "% Error: Unexpected failure!\r\n",
    "% Invalid Command\r\n",
    "% DHCPv6 Relay: Not enabled\r\n",
    "% Relay cannot listen on this port. Try Some other port\r\n",
    "% Invalid UDP port number.\r\n",
    "% Please provide a valid debug string. \r\n",
    "% Interface cannot deleted, server address mapped to interface. \r\n",
 "% String is not having correct length. \r\n",
 "% This remote id is already assigned to a different interface\r\n",
 "% Input Value is not correct.\r\n",
 "% Inconsistent Input Value.\r\n",
 "% Remote-Id option is not enable.\r\n",
 "% PD Route Control Wrong Value.\r\n",
 "% Invalid DHCPv6 server IP address.\r\n",
 "% Interface is already created.\r\n",
 "% Interface value is not in range.\r\n",
 "% Max Servers Reached,No more Servers can be Configured.\r\n",
 "% Out Interface node creation failed.\r\n"
};

#else
extern CONST CHR1  *gaDhcp6RlyCliErrString [];
#endif

#endif
