/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsotercli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FutRmTeLinkRegDeregistration[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFutRmTeLinkRegDeregistration(i4SetValFutRmTeLinkRegDeregistration)	\
	nmhSetCmn(FutRmTeLinkRegDeregistration, 12, FutRmTeLinkRegDeregistrationSet, OspfTeLock, OspfTeUnLock, 0, 0, 0, "%i", i4SetValFutRmTeLinkRegDeregistration)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FutRMTeLinkLocalIpAddr[13];
extern UINT4 FutRMTeLinkRemoteIpAddr[13];
extern UINT4 FutRMTeLinkRemoteRtrId[13];
extern UINT4 FutRMTeLinkMetric[13];
extern UINT4 FutRMTeLinkProtectionType[13];
extern UINT4 FutRMTeLinkResourceClass[13];
extern UINT4 FutRMTeLinkIncomingIfId[13];
extern UINT4 FutRMTeLinkOutgoingIfId[13];
extern UINT4 FutRMTeLinkMaxBw[13];
extern UINT4 FutRMTeLinkMaxResBw[13];
extern UINT4 FutRMTeLinkAreaId[13];
extern UINT4 FutRMTeLinkInfoType[13];
extern UINT4 FutRMTeLinkRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFutRMTeLinkLocalIpAddr(i4IfIndex ,u4SetValFutRMTeLinkLocalIpAddr)	\
	nmhSetCmn(FutRMTeLinkLocalIpAddr, 13, FutRMTeLinkLocalIpAddrSet, OspfTeLock, OspfTeUnLock, 0, 0, 1, "%i %p", i4IfIndex ,u4SetValFutRMTeLinkLocalIpAddr)
#define nmhSetFutRMTeLinkRemoteIpAddr(i4IfIndex ,u4SetValFutRMTeLinkRemoteIpAddr)	\
	nmhSetCmn(FutRMTeLinkRemoteIpAddr, 13, FutRMTeLinkRemoteIpAddrSet, OspfTeLock, OspfTeUnLock, 0, 0, 1, "%i %p", i4IfIndex ,u4SetValFutRMTeLinkRemoteIpAddr)
#define nmhSetFutRMTeLinkRemoteRtrId(i4IfIndex ,u4SetValFutRMTeLinkRemoteRtrId)	\
	nmhSetCmn(FutRMTeLinkRemoteRtrId, 13, FutRMTeLinkRemoteRtrIdSet, OspfTeLock, OspfTeUnLock, 0, 0, 1, "%i %p", i4IfIndex ,u4SetValFutRMTeLinkRemoteRtrId)
#define nmhSetFutRMTeLinkMetric(i4IfIndex ,u4SetValFutRMTeLinkMetric)	\
	nmhSetCmn(FutRMTeLinkMetric, 13, FutRMTeLinkMetricSet, OspfTeLock, OspfTeUnLock, 0, 0, 1, "%i %u", i4IfIndex ,u4SetValFutRMTeLinkMetric)
#define nmhSetFutRMTeLinkProtectionType(i4IfIndex ,i4SetValFutRMTeLinkProtectionType)	\
	nmhSetCmn(FutRMTeLinkProtectionType, 13, FutRMTeLinkProtectionTypeSet, OspfTeLock, OspfTeUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValFutRMTeLinkProtectionType)
#define nmhSetFutRMTeLinkResourceClass(i4IfIndex ,u4SetValFutRMTeLinkResourceClass)	\
	nmhSetCmn(FutRMTeLinkResourceClass, 13, FutRMTeLinkResourceClassSet, OspfTeLock, OspfTeUnLock, 0, 0, 1, "%i %u", i4IfIndex ,u4SetValFutRMTeLinkResourceClass)
#define nmhSetFutRMTeLinkIncomingIfId(i4IfIndex ,i4SetValFutRMTeLinkIncomingIfId)	\
	nmhSetCmn(FutRMTeLinkIncomingIfId, 13, FutRMTeLinkIncomingIfIdSet, OspfTeLock, OspfTeUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValFutRMTeLinkIncomingIfId)
#define nmhSetFutRMTeLinkOutgoingIfId(i4IfIndex ,i4SetValFutRMTeLinkOutgoingIfId)	\
	nmhSetCmn(FutRMTeLinkOutgoingIfId, 13, FutRMTeLinkOutgoingIfIdSet, OspfTeLock, OspfTeUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValFutRMTeLinkOutgoingIfId)
#define nmhSetFutRMTeLinkMaxBw(i4IfIndex ,u4SetValFutRMTeLinkMaxBw)	\
	nmhSetCmn(FutRMTeLinkMaxBw, 13, FutRMTeLinkMaxBwSet, OspfTeLock, OspfTeUnLock, 0, 0, 1, "%i %u", i4IfIndex ,u4SetValFutRMTeLinkMaxBw)
#define nmhSetFutRMTeLinkMaxResBw(i4IfIndex ,u4SetValFutRMTeLinkMaxResBw)	\
	nmhSetCmn(FutRMTeLinkMaxResBw, 13, FutRMTeLinkMaxResBwSet, OspfTeLock, OspfTeUnLock, 0, 0, 1, "%i %u", i4IfIndex ,u4SetValFutRMTeLinkMaxResBw)
#define nmhSetFutRMTeLinkAreaId(i4IfIndex ,u4SetValFutRMTeLinkAreaId)	\
	nmhSetCmn(FutRMTeLinkAreaId, 13, FutRMTeLinkAreaIdSet, OspfTeLock, OspfTeUnLock, 0, 0, 1, "%i %u", i4IfIndex ,u4SetValFutRMTeLinkAreaId)
#define nmhSetFutRMTeLinkInfoType(i4IfIndex ,i4SetValFutRMTeLinkInfoType)	\
	nmhSetCmn(FutRMTeLinkInfoType, 13, FutRMTeLinkInfoTypeSet, OspfTeLock, OspfTeUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValFutRMTeLinkInfoType)
#define nmhSetFutRMTeLinkRowStatus(i4IfIndex ,i4SetValFutRMTeLinkRowStatus)	\
	nmhSetCmn(FutRMTeLinkRowStatus, 13, FutRMTeLinkRowStatusSet, OspfTeLock, OspfTeUnLock, 0, 1, 1, "%i %i", i4IfIndex ,i4SetValFutRMTeLinkRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FutRMTeLinkSwDescriptorId[13];
extern UINT4 FutRMTeLinkSwDescrSwitchingCap[13];
extern UINT4 FutRMTeLinkSwDescrEncodingType[13];
extern UINT4 FutRMTeLinkSwDescrMinLSPBandwidth[13];
extern UINT4 FutRMTeLinkSwDescrMTU[13];
extern UINT4 FutRMTeLinkSwDescrIndication[13];
extern UINT4 FutRMTeLinkSwDescrRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFutRMTeLinkSwDescrSwitchingCap(i4IfIndex , u4FutRMTeLinkSwDescriptorId ,i4SetValFutRMTeLinkSwDescrSwitchingCap)	\
	nmhSetCmn(FutRMTeLinkSwDescrSwitchingCap, 13, FutRMTeLinkSwDescrSwitchingCapSet, OspfTeLock, OspfTeUnLock, 0, 0, 2, "%i %u %i", i4IfIndex , u4FutRMTeLinkSwDescriptorId ,i4SetValFutRMTeLinkSwDescrSwitchingCap)
#define nmhSetFutRMTeLinkSwDescrEncodingType(i4IfIndex , u4FutRMTeLinkSwDescriptorId ,i4SetValFutRMTeLinkSwDescrEncodingType)	\
	nmhSetCmn(FutRMTeLinkSwDescrEncodingType, 13, FutRMTeLinkSwDescrEncodingTypeSet, OspfTeLock, OspfTeUnLock, 0, 0, 2, "%i %u %i", i4IfIndex , u4FutRMTeLinkSwDescriptorId ,i4SetValFutRMTeLinkSwDescrEncodingType)
#define nmhSetFutRMTeLinkSwDescrMinLSPBandwidth(i4IfIndex , u4FutRMTeLinkSwDescriptorId ,u4SetValFutRMTeLinkSwDescrMinLSPBandwidth)	\
	nmhSetCmn(FutRMTeLinkSwDescrMinLSPBandwidth, 13, FutRMTeLinkSwDescrMinLSPBandwidthSet, OspfTeLock, OspfTeUnLock, 0, 0, 2, "%i %u %u", i4IfIndex , u4FutRMTeLinkSwDescriptorId ,u4SetValFutRMTeLinkSwDescrMinLSPBandwidth)
#define nmhSetFutRMTeLinkSwDescrMTU(i4IfIndex , u4FutRMTeLinkSwDescriptorId ,u4SetValFutRMTeLinkSwDescrMTU)	\
	nmhSetCmn(FutRMTeLinkSwDescrMTU, 13, FutRMTeLinkSwDescrMTUSet, OspfTeLock, OspfTeUnLock, 0, 0, 2, "%i %u %u", i4IfIndex , u4FutRMTeLinkSwDescriptorId ,u4SetValFutRMTeLinkSwDescrMTU)
#define nmhSetFutRMTeLinkSwDescrIndication(i4IfIndex , u4FutRMTeLinkSwDescriptorId ,u4SetValFutRMTeLinkSwDescrIndication)	\
	nmhSetCmn(FutRMTeLinkSwDescrIndication, 13, FutRMTeLinkSwDescrIndicationSet, OspfTeLock, OspfTeUnLock, 0, 0, 2, "%i %u %u", i4IfIndex , u4FutRMTeLinkSwDescriptorId ,u4SetValFutRMTeLinkSwDescrIndication)
#define nmhSetFutRMTeLinkSwDescrRowStatus(i4IfIndex , u4FutRMTeLinkSwDescriptorId ,i4SetValFutRMTeLinkSwDescrRowStatus)	\
	nmhSetCmn(FutRMTeLinkSwDescrRowStatus, 13, FutRMTeLinkSwDescrRowStatusSet, OspfTeLock, OspfTeUnLock, 0, 1, 2, "%i %u %i", i4IfIndex , u4FutRMTeLinkSwDescriptorId ,i4SetValFutRMTeLinkSwDescrRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FutRMTeLinkSwDescrMaxBwPriority[13];
extern UINT4 FutRMTeLinkSwDescrMaxLSPBandwidth[13];
extern UINT4 FutRMTeLinkSwDescrMaxBwRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFutRMTeLinkSwDescrMaxLSPBandwidth(i4IfIndex , u4FutRMTeLinkSwDescriptorId , u4FutRMTeLinkSwDescrMaxBwPriority ,u4SetValFutRMTeLinkSwDescrMaxLSPBandwidth)	\
	nmhSetCmn(FutRMTeLinkSwDescrMaxLSPBandwidth, 13, FutRMTeLinkSwDescrMaxLSPBandwidthSet, OspfTeLock, OspfTeUnLock, 0, 0, 3, "%i %u %u %u", i4IfIndex , u4FutRMTeLinkSwDescriptorId , u4FutRMTeLinkSwDescrMaxBwPriority ,u4SetValFutRMTeLinkSwDescrMaxLSPBandwidth)
#define nmhSetFutRMTeLinkSwDescrMaxBwRowStatus(i4IfIndex , u4FutRMTeLinkSwDescriptorId , u4FutRMTeLinkSwDescrMaxBwPriority ,i4SetValFutRMTeLinkSwDescrMaxBwRowStatus)	\
	nmhSetCmn(FutRMTeLinkSwDescrMaxBwRowStatus, 13, FutRMTeLinkSwDescrMaxBwRowStatusSet, OspfTeLock, OspfTeUnLock, 0, 1, 3, "%i %u %u %i", i4IfIndex , u4FutRMTeLinkSwDescriptorId , u4FutRMTeLinkSwDescrMaxBwPriority ,i4SetValFutRMTeLinkSwDescrMaxBwRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FutRMTeLinkSrlg[13];
extern UINT4 FutRMTeLinkSrlgRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFutRMTeLinkSrlgRowStatus(i4IfIndex , u4FutRMTeLinkSrlg ,i4SetValFutRMTeLinkSrlgRowStatus)	\
	nmhSetCmn(FutRMTeLinkSrlgRowStatus, 13, FutRMTeLinkSrlgRowStatusSet, OspfTeLock, OspfTeUnLock, 0, 1, 2, "%i %u %i", i4IfIndex , u4FutRMTeLinkSrlg ,i4SetValFutRMTeLinkSrlgRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FutRMTeLinkBandwidthPriority[13];
extern UINT4 FutRMTeLinkUnreservedBandwidth[13];
extern UINT4 FutRMTeLinkBandwidthRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFutRMTeLinkUnreservedBandwidth(i4IfIndex , u4FutRMTeLinkBandwidthPriority ,u4SetValFutRMTeLinkUnreservedBandwidth)	\
	nmhSetCmn(FutRMTeLinkUnreservedBandwidth, 13, FutRMTeLinkUnreservedBandwidthSet, OspfTeLock, OspfTeUnLock, 0, 0, 2, "%i %u %u", i4IfIndex , u4FutRMTeLinkBandwidthPriority ,u4SetValFutRMTeLinkUnreservedBandwidth)
#define nmhSetFutRMTeLinkBandwidthRowStatus(i4IfIndex , u4FutRMTeLinkBandwidthPriority ,i4SetValFutRMTeLinkBandwidthRowStatus)	\
	nmhSetCmn(FutRMTeLinkBandwidthRowStatus, 13, FutRMTeLinkBandwidthRowStatusSet, OspfTeLock, OspfTeUnLock, 0, 1, 2, "%i %u %i", i4IfIndex , u4FutRMTeLinkBandwidthPriority ,i4SetValFutRMTeLinkBandwidthRowStatus)

#endif
