/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsipcli.h,v 1.5 2010/07/27 07:23:33 prabuc Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsIpOptProcEnable[10];
extern UINT4 FsIpNumMultipath[10];
extern UINT4 FsIpLoadShareEnable[10];
extern UINT4 FsIpEnablePMTUD[10];
extern UINT4 FsIpPmtuEntryAge[10];
extern UINT4 FsIpPmtuTableSize[10];
extern UINT4 FsIpProxyArpSubnetOption[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsIpOptProcEnable(i4SetValFsIpOptProcEnable)	\
	nmhSetCmnWithLock(FsIpOptProcEnable, 10, FsIpOptProcEnableSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 0, "%i", i4SetValFsIpOptProcEnable)
#define nmhSetFsIpNumMultipath(i4SetValFsIpNumMultipath)	\
	nmhSetCmnWithLock(FsIpNumMultipath, 10, FsIpNumMultipathSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 0, "%i", i4SetValFsIpNumMultipath)
#define nmhSetFsIpLoadShareEnable(i4SetValFsIpLoadShareEnable)	\
	nmhSetCmnWithLock(FsIpLoadShareEnable, 10, FsIpLoadShareEnableSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 0, "%i", i4SetValFsIpLoadShareEnable)
#define nmhSetFsIpEnablePMTUD(i4SetValFsIpEnablePMTUD)	\
	nmhSetCmnWithLock(FsIpEnablePMTUD, 10, FsIpEnablePMTUDSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 0, "%i", i4SetValFsIpEnablePMTUD)
#define nmhSetFsIpPmtuEntryAge(i4SetValFsIpPmtuEntryAge)	\
	nmhSetCmnWithLock(FsIpPmtuEntryAge, 10, FsIpPmtuEntryAgeSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 0, "%i", i4SetValFsIpPmtuEntryAge)
#define nmhSetFsIpPmtuTableSize(i4SetValFsIpPmtuTableSize)	\
	nmhSetCmnWithLock(FsIpPmtuTableSize, 10, FsIpPmtuTableSizeSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 0, "%i", i4SetValFsIpPmtuTableSize)
#define nmhSetFsIpProxyArpSubnetOption(i4SetValFsIpProxyArpSubnetOption)	\
	nmhSetCmnWithLock(FsIpProxyArpSubnetOption, 10, FsIpProxyArpSubnetOptionSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 0, "%i", i4SetValFsIpProxyArpSubnetOption)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsIpTraceConfigDest[12];
extern UINT4 FsIpTraceConfigAdminStatus[12];
extern UINT4 FsIpTraceConfigMaxTTL[12];
extern UINT4 FsIpTraceConfigMinTTL[12];
extern UINT4 FsIpTraceConfigTimeout[12];
extern UINT4 FsIpTraceConfigMtu[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsIpTraceConfigAdminStatus(u4FsIpTraceConfigDest ,i4SetValFsIpTraceConfigAdminStatus)	\
	nmhSetCmnWithLock(FsIpTraceConfigAdminStatus, 12, FsIpTraceConfigAdminStatusSet, UdpProtocolLock, UdpProtocolUnLock, 0, 0, 1, "%p %i", u4FsIpTraceConfigDest ,i4SetValFsIpTraceConfigAdminStatus)
#define nmhSetFsIpTraceConfigMaxTTL(u4FsIpTraceConfigDest ,i4SetValFsIpTraceConfigMaxTTL)	\
	nmhSetCmnWithLock(FsIpTraceConfigMaxTTL, 12, FsIpTraceConfigMaxTTLSet, UdpProtocolLock, UdpProtocolUnLock, 0, 0, 1, "%p %i", u4FsIpTraceConfigDest ,i4SetValFsIpTraceConfigMaxTTL)
#define nmhSetFsIpTraceConfigMinTTL(u4FsIpTraceConfigDest ,i4SetValFsIpTraceConfigMinTTL)	\
	nmhSetCmnWithLock(FsIpTraceConfigMinTTL, 12, FsIpTraceConfigMinTTLSet, UdpProtocolLock, UdpProtocolUnLock, 0, 0, 1, "%p %i", u4FsIpTraceConfigDest ,i4SetValFsIpTraceConfigMinTTL)
#define nmhSetFsIpTraceConfigTimeout(u4FsIpTraceConfigDest ,i4SetValFsIpTraceConfigTimeout)	\
	nmhSetCmnWithLock(FsIpTraceConfigTimeout, 12, FsIpTraceConfigTimeoutSet, UdpProtocolLock, UdpProtocolUnLock, 0, 0, 1, "%p %i", u4FsIpTraceConfigDest ,i4SetValFsIpTraceConfigTimeout)
#define nmhSetFsIpTraceConfigMtu(u4FsIpTraceConfigDest ,i4SetValFsIpTraceConfigMtu)	\
	nmhSetCmnWithLock(FsIpTraceConfigMtu, 12, FsIpTraceConfigMtuSet, UdpProtocolLock, UdpProtocolUnLock, 0, 0, 1, "%p %i", u4FsIpTraceConfigDest ,i4SetValFsIpTraceConfigMtu)
#endif

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsIpAddrTabAddress[12];
extern UINT4 FsIpAddrTabAdvertise[12];
extern UINT4 FsIpAddrTabPreflevel[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsIpAddrTabAdvertise(u4FsIpAddrTabAddress ,i4SetValFsIpAddrTabAdvertise)	\
	nmhSetCmnWithLock(FsIpAddrTabAdvertise, 12, FsIpAddrTabAdvertiseSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 1, "%p %i", u4FsIpAddrTabAddress ,i4SetValFsIpAddrTabAdvertise)
#define nmhSetFsIpAddrTabPreflevel(u4FsIpAddrTabAddress ,i4SetValFsIpAddrTabPreflevel)	\
	nmhSetCmnWithLock(FsIpAddrTabPreflevel, 12, FsIpAddrTabPreflevelSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 1, "%p %i", u4FsIpAddrTabAddress ,i4SetValFsIpAddrTabPreflevel)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsIpRtrLstIface[12];
extern UINT4 FsIpRtrLstAddress[12];
extern UINT4 FsIpRtrLstPreflevel[12];
extern UINT4 FsIpRtrLstStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsIpRtrLstIface(u4FsIpRtrLstAddress ,i4SetValFsIpRtrLstIface)	\
	nmhSetCmnWithLock(FsIpRtrLstIface, 12, FsIpRtrLstIfaceSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 1, "%p %i", u4FsIpRtrLstAddress ,i4SetValFsIpRtrLstIface)
#define nmhSetFsIpRtrLstPreflevel(u4FsIpRtrLstAddress ,i4SetValFsIpRtrLstPreflevel)	\
	nmhSetCmnWithLock(FsIpRtrLstPreflevel, 12, FsIpRtrLstPreflevelSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 1, "%p %i", u4FsIpRtrLstAddress ,i4SetValFsIpRtrLstPreflevel)
#define nmhSetFsIpRtrLstStatus(u4FsIpRtrLstAddress ,i4SetValFsIpRtrLstStatus)	\
	nmhSetCmnWithLock(FsIpRtrLstStatus, 12, FsIpRtrLstStatusSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 1, 1, "%p %i", u4FsIpRtrLstAddress ,i4SetValFsIpRtrLstStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsIpPmtuDestination[12];
extern UINT4 FsIpPmtuTos[12];
extern UINT4 FsIpPathMtu[12];
extern UINT4 FsIpPmtuDisc[12];
extern UINT4 FsIpPmtuEntryStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsIpPathMtu(u4FsIpPmtuDestination , i4FsIpPmtuTos ,i4SetValFsIpPathMtu)	\
	nmhSetCmnWithLock(FsIpPathMtu, 12, FsIpPathMtuSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 2, "%p %i %i", u4FsIpPmtuDestination , i4FsIpPmtuTos ,i4SetValFsIpPathMtu)
#define nmhSetFsIpPmtuDisc(u4FsIpPmtuDestination , i4FsIpPmtuTos ,i4SetValFsIpPmtuDisc)	\
	nmhSetCmnWithLock(FsIpPmtuDisc, 12, FsIpPmtuDiscSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 2, "%p %i %i", u4FsIpPmtuDestination , i4FsIpPmtuTos ,i4SetValFsIpPmtuDisc)
#define nmhSetFsIpPmtuEntryStatus(u4FsIpPmtuDestination , i4FsIpPmtuTos ,i4SetValFsIpPmtuEntryStatus)	\
	nmhSetCmnWithLock(FsIpPmtuEntryStatus, 12, FsIpPmtuEntryStatusSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 1, 2, "%p %i %i", u4FsIpPmtuDestination , i4FsIpPmtuTos ,i4SetValFsIpPmtuEntryStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsIpRouteDest[12];
extern UINT4 FsIpRouteMask[12];
extern UINT4 FsIpRouteTos[12];
extern UINT4 FsIpRouteNextHop[12];
extern UINT4 FsIpRouteProto[12];
extern UINT4 FsIpRouteIfIndex[12];
extern UINT4 FsIpRouteType[12];
extern UINT4 FsIpRouteNextHopAS[12];
extern UINT4 FsIpRoutePreference[12];
extern UINT4 FsIpRouteStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsIpRouteIfIndex(u4FsIpRouteDest , u4FsIpRouteMask , i4FsIpRouteTos , u4FsIpRouteNextHop , i4FsIpRouteProto ,i4SetValFsIpRouteIfIndex)	\
	nmhSetCmnWithLock(FsIpRouteIfIndex, 12, FsIpRouteIfIndexSet, RtmProtocolLock, RtmProtocolUnlock, 0, 0, 5, "%p %p %i %p %i %i", u4FsIpRouteDest , u4FsIpRouteMask , i4FsIpRouteTos , u4FsIpRouteNextHop , i4FsIpRouteProto ,i4SetValFsIpRouteIfIndex)
#define nmhSetFsIpRouteType(u4FsIpRouteDest , u4FsIpRouteMask , i4FsIpRouteTos , u4FsIpRouteNextHop , i4FsIpRouteProto ,i4SetValFsIpRouteType)	\
	nmhSetCmnWithLock(FsIpRouteType, 12, FsIpRouteTypeSet, RtmProtocolLock, RtmProtocolUnlock, 0, 0, 5, "%p %p %i %p %i %i", u4FsIpRouteDest , u4FsIpRouteMask , i4FsIpRouteTos , u4FsIpRouteNextHop , i4FsIpRouteProto ,i4SetValFsIpRouteType)
#define nmhSetFsIpRouteNextHopAS(u4FsIpRouteDest , u4FsIpRouteMask , i4FsIpRouteTos , u4FsIpRouteNextHop , i4FsIpRouteProto ,i4SetValFsIpRouteNextHopAS)	\
	nmhSetCmnWithLock(FsIpRouteNextHopAS, 12, FsIpRouteNextHopASSet, RtmProtocolLock, RtmProtocolUnlock, 0, 0, 5, "%p %p %i %p %i %i", u4FsIpRouteDest , u4FsIpRouteMask , i4FsIpRouteTos , u4FsIpRouteNextHop , i4FsIpRouteProto ,i4SetValFsIpRouteNextHopAS)
#define nmhSetFsIpRoutePreference(u4FsIpRouteDest , u4FsIpRouteMask , i4FsIpRouteTos , u4FsIpRouteNextHop , i4FsIpRouteProto ,i4SetValFsIpRoutePreference)	\
	nmhSetCmnWithLock(FsIpRoutePreference, 12, FsIpRoutePreferenceSet, RtmProtocolLock, RtmProtocolUnlock, 0, 0, 5, "%p %p %i %p %i %i", u4FsIpRouteDest , u4FsIpRouteMask , i4FsIpRouteTos , u4FsIpRouteNextHop , i4FsIpRouteProto ,i4SetValFsIpRoutePreference)
#define nmhSetFsIpRouteStatus(u4FsIpRouteDest , u4FsIpRouteMask , i4FsIpRouteTos , u4FsIpRouteNextHop , i4FsIpRouteProto ,i4SetValFsIpRouteStatus)	\
	nmhSetCmnWithLock(FsIpRouteStatus, 12, FsIpRouteStatusSet, RtmProtocolLock, RtmProtocolUnlock, 0, 1, 5, "%p %p %i %p %i %i", u4FsIpRouteDest , u4FsIpRouteMask , i4FsIpRouteTos , u4FsIpRouteNextHop , i4FsIpRouteProto ,i4SetValFsIpRouteStatus)
#endif

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsIpifIndex[12];
extern UINT4 FsIpifMaxReasmSize[12];
extern UINT4 FsIpifIcmpRedirectEnable[12];
extern UINT4 FsIpifDrtBcastFwdingEnable[12];
extern UINT4 FsIpifProxyArpAdminStatus[12];
extern UINT4 FsIpifLocalProxyArpAdminStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsIpifMaxReasmSize(i4FsIpifIndex ,i4SetValFsIpifMaxReasmSize)	\
	nmhSetCmnWithLock(FsIpifMaxReasmSize, 12, FsIpifMaxReasmSizeSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 1, "%i %i", i4FsIpifIndex ,i4SetValFsIpifMaxReasmSize)
#define nmhSetFsIpifIcmpRedirectEnable(i4FsIpifIndex ,i4SetValFsIpifIcmpRedirectEnable)	\
	nmhSetCmnWithLock(FsIpifIcmpRedirectEnable, 12, FsIpifIcmpRedirectEnableSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 1, "%i %i", i4FsIpifIndex ,i4SetValFsIpifIcmpRedirectEnable)
#define nmhSetFsIpifDrtBcastFwdingEnable(i4FsIpifIndex ,i4SetValFsIpifDrtBcastFwdingEnable)	\
	nmhSetCmnWithLock(FsIpifDrtBcastFwdingEnable, 12, FsIpifDrtBcastFwdingEnableSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 1, "%i %i", i4FsIpifIndex ,i4SetValFsIpifDrtBcastFwdingEnable)
#define nmhSetFsIpifProxyArpAdminStatus(i4FsIpifIndex ,i4SetValFsIpifProxyArpAdminStatus)	\
	nmhSetCmnWithLock(FsIpifProxyArpAdminStatus, 12, FsIpifProxyArpAdminStatusSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 1, "%i %i", i4FsIpifIndex ,i4SetValFsIpifProxyArpAdminStatus)

#define nmhSetFsIpifLocalProxyArpAdminStatus(i4FsIpifIndex ,i4SetValFsIpifLProxyArpAdminStatus)	\
	nmhSetCmnWithLock(FsIpifProxyArpAdminStatus, 12, FsIpifLocalProxyArpAdminStatusSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 1, "%i %i", i4FsIpifIndex ,i4SetValFsIpifLProxyArpAdminStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsIcmpSendRedirectEnable[10];
extern UINT4 FsIcmpSendUnreachableEnable[10];
extern UINT4 FsIcmpSendEchoReplyEnable[10];
extern UINT4 FsIcmpNetMaskReplyEnable[10];
extern UINT4 FsIcmpTimeStampReplyEnable[10];
extern UINT4 FsIcmpDirectQueryEnable[10];
extern UINT4 FsDomainName[10];
extern UINT4 FsTimeToLive[10];
extern UINT4 FsIcmpSendSecurityFailuresEnable[10];
extern UINT4 FsIcmpRecvSecurityFailuresEnable[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsIcmpSendRedirectEnable(i4SetValFsIcmpSendRedirectEnable)	\
	nmhSetCmnWithLock(FsIcmpSendRedirectEnable, 10, FsIcmpSendRedirectEnableSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 0, "%i", i4SetValFsIcmpSendRedirectEnable)
#define nmhSetFsIcmpSendUnreachableEnable(i4SetValFsIcmpSendUnreachableEnable)	\
	nmhSetCmnWithLock(FsIcmpSendUnreachableEnable, 10, FsIcmpSendUnreachableEnableSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 0, "%i", i4SetValFsIcmpSendUnreachableEnable)
#define nmhSetFsIcmpSendEchoReplyEnable(i4SetValFsIcmpSendEchoReplyEnable)	\
	nmhSetCmnWithLock(FsIcmpSendEchoReplyEnable, 10, FsIcmpSendEchoReplyEnableSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 0, "%i", i4SetValFsIcmpSendEchoReplyEnable)
#define nmhSetFsIcmpNetMaskReplyEnable(i4SetValFsIcmpNetMaskReplyEnable)	\
	nmhSetCmnWithLock(FsIcmpNetMaskReplyEnable, 10, FsIcmpNetMaskReplyEnableSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 0, "%i", i4SetValFsIcmpNetMaskReplyEnable)
#define nmhSetFsIcmpTimeStampReplyEnable(i4SetValFsIcmpTimeStampReplyEnable)	\
	nmhSetCmnWithLock(FsIcmpTimeStampReplyEnable, 10, FsIcmpTimeStampReplyEnableSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 0, "%i", i4SetValFsIcmpTimeStampReplyEnable)
#define nmhSetFsIcmpDirectQueryEnable(i4SetValFsIcmpDirectQueryEnable)	\
	nmhSetCmnWithLock(FsIcmpDirectQueryEnable, 10, FsIcmpDirectQueryEnableSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 0, "%i", i4SetValFsIcmpDirectQueryEnable)
#define nmhSetFsDomainName(pSetValFsDomainName)	\
	nmhSetCmnWithLock(FsDomainName, 10, FsDomainNameSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 0, "%s", pSetValFsDomainName)
#define nmhSetFsTimeToLive(i4SetValFsTimeToLive)	\
	nmhSetCmnWithLock(FsTimeToLive, 10, FsTimeToLiveSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 0, "%i", i4SetValFsTimeToLive)
#define nmhSetFsIcmpSendSecurityFailuresEnable(i4SetValFsIcmpSendSecurityFailuresEnable)	\
	nmhSetCmnWithLock(FsIcmpSendSecurityFailuresEnable, 10, FsIcmpSendSecurityFailuresEnableSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 0, "%i", i4SetValFsIcmpSendSecurityFailuresEnable)
#define nmhSetFsIcmpRecvSecurityFailuresEnable(i4SetValFsIcmpRecvSecurityFailuresEnable)	\
	nmhSetCmnWithLock(FsIcmpRecvSecurityFailuresEnable, 10, FsIcmpRecvSecurityFailuresEnableSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 0, "%i", i4SetValFsIcmpRecvSecurityFailuresEnable)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsCidrAggAddress[12];
extern UINT4 FsCidrAggAddressMask[12];
extern UINT4 FsCidrAggStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsCidrAggStatus(u4FsCidrAggAddress , u4FsCidrAggAddressMask ,i4SetValFsCidrAggStatus)	\
	nmhSetCmnWithLock(FsCidrAggStatus, 12, FsCidrAggStatusSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 1, 2, "%p %p %i", u4FsCidrAggAddress , u4FsCidrAggAddressMask ,i4SetValFsCidrAggStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsCidrAdvertAddress[12];
extern UINT4 FsCidrAdvertAddressMask[12];
extern UINT4 FsCidrAdvertStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsCidrAdvertStatus(u4FsCidrAdvertAddress , u4FsCidrAdvertAddressMask ,i4SetValFsCidrAdvertStatus)	\
	nmhSetCmnWithLock(FsCidrAdvertStatus, 12, FsCidrAdvertStatusSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 1, 2, "%p %p %i", u4FsCidrAdvertAddress , u4FsCidrAdvertAddressMask ,i4SetValFsCidrAdvertStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsIrdpSendAdvertisementsEnable[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsIrdpSendAdvertisementsEnable(i4SetValFsIrdpSendAdvertisementsEnable)	\
	nmhSetCmnWithLock(FsIrdpSendAdvertisementsEnable, 10, FsIrdpSendAdvertisementsEnableSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 0, "%i", i4SetValFsIrdpSendAdvertisementsEnable)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsIrdpIfConfIfNum[12];
extern UINT4 FsIrdpIfConfSubref[12];
extern UINT4 FsIrdpIfConfAdvertisementAddress[12];
extern UINT4 FsIrdpIfConfMaxAdvertisementInterval[12];
extern UINT4 FsIrdpIfConfMinAdvertisementInterval[12];
extern UINT4 FsIrdpIfConfAdvertisementLifetime[12];
extern UINT4 FsIrdpIfConfPerformRouterDiscovery[12];
extern UINT4 FsIrdpIfConfSolicitationAddress[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsIrdpIfConfAdvertisementAddress(i4FsIrdpIfConfIfNum , i4FsIrdpIfConfSubref ,u4SetValFsIrdpIfConfAdvertisementAddress)	\
	nmhSetCmnWithLock(FsIrdpIfConfAdvertisementAddress, 12, FsIrdpIfConfAdvertisementAddressSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 2, "%i %i %p", i4FsIrdpIfConfIfNum , i4FsIrdpIfConfSubref ,u4SetValFsIrdpIfConfAdvertisementAddress)
#define nmhSetFsIrdpIfConfMaxAdvertisementInterval(i4FsIrdpIfConfIfNum , i4FsIrdpIfConfSubref ,i4SetValFsIrdpIfConfMaxAdvertisementInterval)	\
	nmhSetCmnWithLock(FsIrdpIfConfMaxAdvertisementInterval, 12, FsIrdpIfConfMaxAdvertisementIntervalSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 2, "%i %i %i", i4FsIrdpIfConfIfNum , i4FsIrdpIfConfSubref ,i4SetValFsIrdpIfConfMaxAdvertisementInterval)
#define nmhSetFsIrdpIfConfMinAdvertisementInterval(i4FsIrdpIfConfIfNum , i4FsIrdpIfConfSubref ,i4SetValFsIrdpIfConfMinAdvertisementInterval)	\
	nmhSetCmnWithLock(FsIrdpIfConfMinAdvertisementInterval, 12, FsIrdpIfConfMinAdvertisementIntervalSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 2, "%i %i %i", i4FsIrdpIfConfIfNum , i4FsIrdpIfConfSubref ,i4SetValFsIrdpIfConfMinAdvertisementInterval)
#define nmhSetFsIrdpIfConfAdvertisementLifetime(i4FsIrdpIfConfIfNum , i4FsIrdpIfConfSubref ,i4SetValFsIrdpIfConfAdvertisementLifetime)	\
	nmhSetCmnWithLock(FsIrdpIfConfAdvertisementLifetime, 12, FsIrdpIfConfAdvertisementLifetimeSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 2, "%i %i %i", i4FsIrdpIfConfIfNum , i4FsIrdpIfConfSubref ,i4SetValFsIrdpIfConfAdvertisementLifetime)
#define nmhSetFsIrdpIfConfPerformRouterDiscovery(i4FsIrdpIfConfIfNum , i4FsIrdpIfConfSubref ,i4SetValFsIrdpIfConfPerformRouterDiscovery)	\
	nmhSetCmnWithLock(FsIrdpIfConfPerformRouterDiscovery, 12, FsIrdpIfConfPerformRouterDiscoverySet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 2, "%i %i %i", i4FsIrdpIfConfIfNum , i4FsIrdpIfConfSubref ,i4SetValFsIrdpIfConfPerformRouterDiscovery)
#define nmhSetFsIrdpIfConfSolicitationAddress(i4FsIrdpIfConfIfNum , i4FsIrdpIfConfSubref ,u4SetValFsIrdpIfConfSolicitationAddress)	\
	nmhSetCmnWithLock(FsIrdpIfConfSolicitationAddress, 12, FsIrdpIfConfSolicitationAddressSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 2, "%i %i %p", i4FsIrdpIfConfIfNum , i4FsIrdpIfConfSubref ,u4SetValFsIrdpIfConfSolicitationAddress)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsRarpClientRetransmissionTimeout[10];
extern UINT4 FsRarpClientMaxRetries[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsRarpClientRetransmissionTimeout(i4SetValFsRarpClientRetransmissionTimeout)	\
	nmhSetCmnWithLock(FsRarpClientRetransmissionTimeout, 10, FsRarpClientRetransmissionTimeoutSet, ArpProtocolLock, ArpProtocolUnLock, 0, 0, 0, "%i", i4SetValFsRarpClientRetransmissionTimeout)
#define nmhSetFsRarpClientMaxRetries(i4SetValFsRarpClientMaxRetries)	\
	nmhSetCmnWithLock(FsRarpClientMaxRetries, 10, FsRarpClientMaxRetriesSet, ArpProtocolLock, ArpProtocolUnLock, 0, 0, 0, "%i", i4SetValFsRarpClientMaxRetries)
#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsRarpServerStatus[10];
extern UINT4 FsRarpServerTableMaxEntries[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsRarpServerStatus(i4SetValFsRarpServerStatus)	\
	nmhSetCmnWithLock(FsRarpServerStatus, 10, FsRarpServerStatusSet, ArpProtocolLock, ArpProtocolUnLock, 0, 0, 0, "%i", i4SetValFsRarpServerStatus)
#define nmhSetFsRarpServerTableMaxEntries(i4SetValFsRarpServerTableMaxEntries)	\
	nmhSetCmnWithLock(FsRarpServerTableMaxEntries, 10, FsRarpServerTableMaxEntriesSet, ArpProtocolLock, ArpProtocolUnLock, 0, 0, 0, "%i", i4SetValFsRarpServerTableMaxEntries)
#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsHardwareAddress[12];
extern UINT4 FsHardwareAddrLen[12];
extern UINT4 FsProtocolAddress[12];
extern UINT4 FsEntryStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsHardwareAddrLen(pFsHardwareAddress ,i4SetValFsHardwareAddrLen)	\
	nmhSetCmnWithLock(FsHardwareAddrLen, 12, FsHardwareAddrLenSet, ArpProtocolLock, ArpProtocolUnLock, 0, 0, 1, "%s %i", pFsHardwareAddress ,i4SetValFsHardwareAddrLen)
#define nmhSetFsProtocolAddress(pFsHardwareAddress ,u4SetValFsProtocolAddress)	\
	nmhSetCmnWithLock(FsProtocolAddress, 12, FsProtocolAddressSet, ArpProtocolLock, ArpProtocolUnLock, 0, 0, 1, "%s %p", pFsHardwareAddress ,u4SetValFsProtocolAddress)
#define nmhSetFsEntryStatus(pFsHardwareAddress ,i4SetValFsEntryStatus)	\
	nmhSetCmnWithLock(FsEntryStatus, 12, FsEntryStatusSet, ArpProtocolLock, ArpProtocolUnLock, 0, 1, 1, "%s %i", pFsHardwareAddress ,i4SetValFsEntryStatus)
#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsNoOfStaticRoutes[10];
extern UINT4 FsNoOfAggregatedRoutes[10];
extern UINT4 FsNoOfRoutes[10];
extern UINT4 FsNoOfReassemblyLists[10];
extern UINT4 FsNoOfFragmentsPerList[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsNoOfStaticRoutes(i4SetValFsNoOfStaticRoutes)	\
	nmhSetCmnWithLock(FsNoOfStaticRoutes, 10, FsNoOfStaticRoutesSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 0, "%i", i4SetValFsNoOfStaticRoutes)
#define nmhSetFsNoOfAggregatedRoutes(i4SetValFsNoOfAggregatedRoutes)	\
	nmhSetCmnWithLock(FsNoOfAggregatedRoutes, 10, FsNoOfAggregatedRoutesSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 0, "%i", i4SetValFsNoOfAggregatedRoutes)
#define nmhSetFsNoOfRoutes(i4SetValFsNoOfRoutes)	\
	nmhSetCmnWithLock(FsNoOfRoutes, 10, FsNoOfRoutesSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 0, "%i", i4SetValFsNoOfRoutes)
#define nmhSetFsNoOfReassemblyLists(i4SetValFsNoOfReassemblyLists)	\
	nmhSetCmnWithLock(FsNoOfReassemblyLists, 10, FsNoOfReassemblyListsSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 0, "%i", i4SetValFsNoOfReassemblyLists)
#define nmhSetFsNoOfFragmentsPerList(i4SetValFsNoOfFragmentsPerList)	\
	nmhSetCmnWithLock(FsNoOfFragmentsPerList, 10, FsNoOfFragmentsPerListSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 0, "%i", i4SetValFsNoOfFragmentsPerList)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsIpGlobalDebug[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsIpGlobalDebug(i4SetValFsIpGlobalDebug)	\
	nmhSetCmnWithLock(FsIpGlobalDebug, 10, FsIpGlobalDebugSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 0, "%i", i4SetValFsIpGlobalDebug)

#endif
