/*************************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
* $Id: dcbxcli.h,v 1.18 2016/05/25 10:06:47 siva Exp $
* Description: This header file contains all the CLI related macros
*              and prototypes for DCBX Module.
****************************************************************************/
#ifndef __DCBX_CLI_H__
#define __DCBX_CLI_H__

#include "cli.h"

#define DCBX_CLI_MAX_ARGS  10
#define DCBX_ZERO            0

/* Enum for DCBX CLI Commands */
enum{
/*1*/ CLI_ETS_SHUTDOWN =1,
/*2*/ CLI_ETS_START,
/*3*/ CLI_ETS_SYS_STATUS,
/*4*/ CLI_ETS_PORT_STATUS,
/*5*/ CLI_ETS_ENABLE_NOTIFY,
/*6*/ CLI_ETS_DISABLE_NOTIFY,
/*7*/ CLI_ETS_SET_PORT_MODE,
/*8*/ CLI_ETS_PORT_MODE_OFF,
/*9*/ CLI_ETS_WILLING_STATUS,
/*10*/ CLI_ETS_PRI_PG_MAP,
/*11*/ CLI_ETS_PGID_BW_CONF,
/*12*/ CLI_ETS_RECO_PGID_BW_CONF,
/*13*/ CLI_ETS_TLV_SEL,
/*14*/ CLI_ETS_NO_TLV_SEL,
/*15*/ CLI_ETS_CLR_COUNTERS,
/*16*/  CLI_SHOW_ETS_PORT_INFO,
/*17*/ CLI_SHOW_ETS_PORT_COUNTERS,
/*18*/ CLI_SHOW_ETS_GLOB_INFO,
/*19*/ CLI_ETS_SET_TSA_TABLE,
/*20*/ CLI_ETS_SET_RECO_TSA_TABLE,
/*21*/  CLI_ETS_COMMAND_END,
/*Add any ETS command identifier before this macro.*/
/*22*/ CLI_PFC_SHUTDOWN,
/*23*/ CLI_PFC_START,
/*24*/ CLI_PFC_SYS_STATUS,
/*25*/ CLI_PFC_PORT_STATUS,
/*26*/ CLI_PFC_ENABLE_NOTIFY,
/*27*/ CLI_PFC_DISABLE_NOTIFY,
/*28*/ CLI_PFC_SET_PORT_MODE,
/*29*/ CLI_PFC_PORT_MODE_OFF,
/*30*/ CLI_PFC_WILLING_STATUS,
/*31*/ CLI_PFC_PER_PRI_STATUS,
/*32*/ CLI_PFC_TLV_SEL,
/*33*/ CLI_PFC_NO_TLV_SEL,
/*34*/ CLI_PFC_CLR_COUNTERS,
/*35*/ CLI_PFC_THRESHOLD,
/*36*/ CLI_SHOW_PFC_PORT_INFO,
/*37*/ CLI_SHOW_PFC_PORT_COUNTERS,
/*38*/ CLI_SHOW_PFC_GLOB_CONF,
/*39*/ CLI_SHOW_PFC_GLOB_INFO,
/*40*/  CLI_PFC_COMMAND_END,
/*Add any PFC command identifier before this macro.*/
/*41*/ CLI_APP_PRI_SHUTDOWN,
/*42*/ CLI_APP_PRI_START,
/*43*/ CLI_APP_PRI_SYS_STATUS,
/*44*/ CLI_APP_PRI_PORT_STATUS,
/*45*/ CLI_APP_PRI_ENABLE_NOTIFY,
/*46*/ CLI_APP_PRI_DISABLE_NOTIFY,
/*47*/ CLI_APP_PRI_SET_PORT_MODE,
/*48*/ CLI_APP_PRI_PORT_MODE_OFF,
/*49*/ CLI_APP_PRI_WILLING_STATUS,
/*50*/ CLI_APP_PRI_MAP_PRI,
/*51*/ CLI_APP_PRI_UNMAP_PRI,
/*52*/ CLI_APP_PRI_TLV_SEL,
/*53*/ CLI_APP_PRI_NO_TLV_SEL,
/*54*/ CLI_APP_PRI_CLR_COUNTERS,
/*55*/ CLI_SHOW_APP_PRI_PORT_INFO,
/*56*/ CLI_SHOW_APP_PRI_PORT_COUNTERS,
/*57*/ CLI_SHOW_APP_PRI_GLOB_CONF,
/*58*/ CLI_SHOW_APP_PRI_GLOB_INFO,
/*59*/ CLI_APP_PRI_COMMAND_END,
/*60*/ CLI_CEE_ENABLE_NOTIFY,
/*61*/ CLI_CEE_DISABLE_NOTIFY,
/*62*/ CLI_SHOW_DCBX_DET,
/*63*/ CLI_DCBX_MODE,
/*64*/ CLI_CTRL_CLR_COUNTERS,
/*65*/ CLI_DCBX_ADMIN_STATUS,
/*66*/ CLI_DCBX_DEBUG,
/*67*/ CLI_DCBX_NO_DEBUG,
/*68*/ CLI_SHOW_DCBX_PORT_INFO
};

/* Enum for DCBX CLI Error Codes */
enum {
 /* ETS ERROR CODE */
/*1*/  CLI_ERR_ETS_MODULE_SHUTDOWN  = 1,
/*2*/  CLI_ERR_ETS_INVALID_SYS_CONTROL,
/*3*/  CLI_ERR_ETS_INVALID_SYS_STATUS,
/*4*/  CLI_ERR_ETS_INVALID_CLEAR_STATUS,
/*5*/  CLI_ERR_ETS_INVALID_NOTIFY,
/*6*/  CLI_ERR_ETS_NO_PORT_ENTRY,
/*7*/  CLI_ERR_ETS_INVALID_PORT_MODE_STATUS,
/*8*/  CLI_ERR_ETS_PORT_CREATION_FAILS,
/*9*/  CLI_ERR_ETS_PORT_ALREADY_PRESENT,
/*10*/ CLI_ERR_ETS_INVALID_WILLING_STATUS,
/*11*/ CLI_ERR_ETS_INVALID_TX_STATUS,
/*12*/ CLI_ERR_ETS_INVALID_BW_SUM,
/*13*/ CLI_ERR_ETS_INVALID_PRIORITY,
/*14*/ CLI_ERR_ETS_INVALID_TCGID,
/*15*/ CLI_ERR_ETS_INVALID_CLEAR_TRAP,
 /*PFC ERROR CODE */
/*16*/ CLI_ERR_PFC_MODULE_SHUTDOWN,
/*17*/ CLI_ERR_PFC_INVALID_SYS_CONTROL,
/*18*/ CLI_ERR_PFC_INVALID_SYS_STATUS,
/*19*/ CLI_ERR_PFC_INVALID_CLEAR_STATUS,
/*20*/ CLI_ERR_PFC_INVALID_NOTIFY,
/*21*/ CLI_ERR_PFC_NO_PORT_ENTRY,
/*22*/ CLI_ERR_PFC_INVALID_PORT_MODE_STATUS,
/*23*/ CLI_ERR_PFC_PORT_CREATION_FAILS,
/*24*/ CLI_ERR_PFC_PORT_ALREADY_PRESENT,
/*25*/ CLI_ERR_PFC_INVALID_WILLING_STATUS,
/*26*/ CLI_ERR_PFC_INVALID_TX_STATUS,
/*27*/ CLI_ERR_PFC_INVALID_PRIORITY,
/*28*/ CLI_ERR_PFC_INVALID_ENABLED_STATUS,
/*28*/ CLI_ERR_PFC_INVALID_THRESHOLD,
/*30*/ CLI_ERR_PFC_THRESH_MISCONF,
/*31*/ CLI_ERR_PFC_INVALID_CLEAR_TRAP,
/*Application Priority ERROR CODE */
/*32*/ CLI_ERR_APP_PRI_NO_PORT_ENTRY,
/*33*/ CLI_ERR_APP_PRI_INVALID_TX_STATUS,
/*34*/ CLI_ERR_APP_PRI_INVALID_WILLING_STATUS,
/*35*/ CLI_ERR_APP_PRI_INVALID_SYS_CONTROL,
/*36*/ CLI_ERR_APP_PRI_INVALID_SYS_STATUS,
/*37*/ CLI_ERR_APP_PRI_INVALID_CLEAR_STATUS,
/*38*/ CLI_ERR_APP_PRI_INVALID_NOTIFY,
/*39*/ CLI_ERR_APP_PRI_INVALID_PORT_MODE_STATUS,
/*40*/ CLI_ERR_APP_PRI_PORT_ALREADY_PRESENT,
/*41*/ CLI_ERR_APP_PRI_PORT_CREATION_FAILS,
/*42*/ CLI_ERR_NO_APP_PRI_MAPPING_TBL,
/*43*/ CLI_ERR_NO_APP_PRI_MAPPING_ENTRY,
/*44*/ CLI_ERR_APP_PRI_INVALID_PRIORITY,
/*45*/ CLI_ERR_APP_PRI_INVALID_PROTOCOL,
/*46*/ CLI_ERR_APP_PRI_INVALID_SELECTOR,
/*47*/ CLI_ERR_APP_PRI_MAX_APP_PRI_MAPPING,
/*48*/ CLI_ERR_APP_PRI_INVALID_CLEAR_TRAP,
/* DCBX ERROR CODE */
/*49*/ CLI_ERR_DCBX_INVALID_TRACE,
/*50*/ CLI_ERR_DCBX_NO_PORT,
/*51*/ CLI_ERR_DCBX_INVALID_ADMIN_STATUS,
/*52*/ CLI_ERR_DCBX_SAME_PRIOIRTY,
/*53*/ CLI_ERR_DCBX_PORT_IN_PORT_CHANNEL,
/*54*/ CLI_ERR_FSB_ENABLED,
/*55*/ CLI_ERR_CEE_INVALID_TRAP_VAL,
/*56*/ CLI_ERR_CEE_INVALID_CLEAR_COUNTER_VAL,
/*57*/ CLI_ERR_INVALID_DCBX_MODE,
/*58*/ CLI_ERR_DCBX_NO_PORT_ENTRY,
/*59*/ CLI_ERR_DCBX_NO_CTRL_ENTRY,
/*60*/ CLI_ERR_APP_PRI_FCOE_FIP_PRI_NOT_MATCH,
/*61*/ DCBX_CLI_MAX_ERR
};

#ifdef DCBX_CLI_C
CONST CHR1  *gDcbxCliErrString [] = {
/* ETS Error Messages */
    "\r\n",
/*1*/  "ETS Should Be Started Before Accessing It !.\r\n",
/*2*/  "Invalid ETS System Control Status.\r\n",
/*3*/  "Invalid ETS Module Status.\r\n",
/*4*/  "Invalid ETS Clear Stauts.\r\n",
/*5*/  "Invalid ETS Trap Value.\r\n",
/*6*/  "ETS is already Disabled in this Port.\r\n",
/*7*/  "Invalid ETS Port Mode Status.\r\n",
/*8*/  "ETS Port Creation Failed.\r\n",
/*9*/  "ETS is already Enabled in this Port.\r\n",
/*10*/ "Invalid ETS Port Willing Status.\r\n",
/*11*/ "Invalid ETS TLV Tx Status .\r\n",
/*12*/ "Sum of ETS Bandwidth should be 100.\r\n",
/*13*/ "Invalid ETS Priority Value.\r\n",
/*14*/ "Invalid ETS Traffic Class Group Id.\r\n",
/*15*/ "Invalid value for ETS clear trap counter.Values other than zero are not allowed\r\n",
/*PFC Error Messages */
/*16*/ "PFC Should Be Started Before Accessing It !.\r\n",
/*17*/ "Invalid PFC System Control Status.\r\n",
/*18*/ "Invalid PFC Module Status.\r\n",
/*19*/ "Invalid PFC Clear Status.\r\n",
/*20*/ "Invalid PFC Trap Value.\r\n",
/*21*/ "PFC is Disabled in this Port\r\n",
/*22*/ "Invalid PFC Port Mode Status.\r\n",
/*23*/ "PFC Port Creation Failed.\r\n",
/*24*/ "PFC is Already Enabled in this Port.\r\n",
/*25*/ "Invalid PFC Port Willing Status.\r\n",
/*26*/ "Invalid PFC TLV Tx Status .\r\n",
/*27*/ "Invalid PFC Priority Value .\r\n",
/*28*/ "Invalid PFC Enabled status.\r\n",
/*29*/ "Invalid PFC Threshold Value.\r\n",
/*30*/ "Maximum Threshold should be greater than Min Thresh.\r\n",
/*31*/ "Invalid value for PFC clear trap counter.Values other than zero are not allowed\r\n",
/* ApplicationPriority Messages */
/*32*/ "Application Priority is already Disabled in this port.\r\n",
/*33*/ "Invalid Application Priority TLV Tx Status.\r\n",
/*34*/ "Invalid Application Priority Port Willing Status.\r\n",
/*35*/ "Invalid Application Priority System Control Status.\r\n",
/*36*/ "Invalid Application Priority Module Status.\r\n",
/*37*/ "Invalid Application Priority Clear Status.\r\n",
/*38*/ "Invalid Application Priority Trap Value.\r\n",
/*39*/ "Invalid Application Priority Port Mode Status.\r\n",
/*40*/ "Application Priority is Already Enabled in this Port.\r\n",
/*41*/ "Application Priority Port Creation Failed.\r\n",
/*42*/ "Application Priority Mapping not found for this selector.\r\n",
/*43*/ "Application Priority Entry not present.\r\n",
/*44*/ "Invalid value for Application Priority.\r\n",
/*45*/ "Invalid value for Application Protocol.\r\n",
/*46*/ "Invalid value for Application Priority Selector.\r\n",
/*47*/ "Maximum number of Application to Priority Mappings already done.\r\n",
/*48*/ "Invalid value for ApplicationPriority clear trap counter.Values other than zero are not allowed\r\n",
/*DCBX Messages */
/*49*/ "Invalid DCB Trace flag.\r\n",
/*50*/ "Interface is not mapped to any of the context.\r\n",
/*51*/ "Invalid DCB Admin Status.\r\n",
/*52*/ "Same Priority specified more than once in priority list.\r\n",
/*53*/ "DCB configurations are not allowed in Port-channel interface.\r\n",
/*54*/ "FSB should be disabled, before disabling the module status.\r\n",
/*55*/ "Invalid CEE Trap Value. \r\n",
/*56*/ "Invalid CEE Clear Counter Value.Value can be DCBX_ENABLED/DCBX_DISABLED\r\n",
/*57*/ "Invalid CEE DCBX Mode\r\n",
/*58*/ "Port entry not present.\r\n",
/*59*/ "CEE Control entry not present.\r\n",
/*60*/ "FCOE or FIP priorities not matched.\r\n",
    "\r\n"
};
#endif
/* Enum for System Control */
typedef enum _tSysCntl {
   DCBX_SYS_CNTL_START = 1,   /*1*/
   DCBX_SYS_CNTL_SHUTDOWN     /*2*/
} etSysCntl;
/* Enum for Module Status */
typedef enum _tSysStatus {
 DCBX_SYS_STATUS_ENABLE = 1,   /*1*/
 DCBX_SYS_STATUS_DISABLE       /*2*/
} etSysStatus;
/* Enum for ETS  Enable or Disable */
typedef enum _tEtsStatus {
 ETS_ENABLE = 1,   /*1*/
 ETS_DISABLE       /*2*/
} etEtsStatus;
/* Enum for PFC Enable or Disable */
typedef enum _tPfcStatus {
 PFC_ENABLE = 1,   /*1*/
 PFC_DISABLE       /*2*/
} etPfcStatus;
/* Enum for Admin Mode */
typedef enum _tModeStatus {
    DCBX_ADM_MODE_AUTO = 0,   /*0*/
    DCBX_ADM_MODE_ON,         /*1*/
    DCBX_ADM_MODE_OFF         /*2*/
} etModeStatus;

typedef enum _tOperStatus {
    DCBX_OPER_OFF = 0,       /*0*/
    DCBX_OPER_INIT,          /*1*/
    DCBX_OPER_RECO           /*2*/
} etOperStatus;
/* Enum for State machine type */
typedef enum _tStateType {
    DCBX_ASYM_TYPE = 1,     /*1*/
    DCBX_SYM_TYPE,          /*2*/
    DCBX_FEAT_TYPE          /*3*/
}etStateType;
/* Enum for Trap Status */
typedef enum _tTrapStatus {
    DCBX_TRAP_DISABLE = 0,  /*0*/
    DCBX_TRAP_ENABLE        /*1*/
} etTrapStatus;
/* Enum for Config/Recommendation table configuration */
typedef enum _tBWConfig {
    ETS_CONFIG_TABLE = 0,   /*0*/
    ETS_RECOMMEND_TABLE     /*1*/
}etBWConfig;

typedef enum _tAPStatus {
 APP_PRI_ENABLE = 1,   /*1*/
 APP_PRI_DISABLE       /*2*/
}tAPStatus;
#define DCBX_MGMT_TRACE           0x00000001
#define DCBX_SEM_TRACE            0x00000002
#define DCBX_TLV_TRACE            0x00000004
#define DCBX_RESOURCE_TRACE       0x00000008
#define DCBX_FAILURE_TRACE        0x00000010
#define DCBX_RED_TRACE            0x00000020
#define DCBX_MBSM_TRACE           0x00000040
#define DCBX_CONTROL_PLANE_TRACE  0x00000080

#define DCBX_ALL_TRACE      (DCBX_MGMT_TRACE | \
                             DCBX_SEM_TRACE  | \
                             DCBX_TLV_TRACE  | \
                             DCBX_RESOURCE_TRACE | \
                             DCBX_FAILURE_TRACE  | \
                             DCBX_RED_TRACE  | \
                             DCBX_MBSM_TRACE | \
                             DCBX_CONTROL_PLANE_TRACE)

#define DCBX_MODULE_STATUS_TRAP  0x00000001    
#define DCBX_ADMIN_MODE_TRAP     0x00000002          
#define DCBX_PEER_STATUS_TRAP    0x00000004
#define DCBX_SEM_TRAP            0x00000008  
#define DCBX_ALL_TRAP     (DCBX_MODULE_STATUS_TRAP | DCBX_ADMIN_MODE_TRAP \
                          | DCBX_PEER_STATUS_TRAP | DCBX_SEM_TRAP)

/* List of TSA Algorithms */
#define DCBX_STRICT_PRIORITY_ALGO                  0
#define DCBX_CREDIT_BASED_SHAPER_ALGO              1
#define DCBX_ENHANCED_TRANSMISSION_SELECTION_ALGO  2
#define DCBX_VENDOR_SPECIFIC_ALGO                  255

/* CEE Related Macro Declaration */
#define DCBX_MODE_AUTO            2
#define DCBX_MODE_IEEE            DCBX_VER_IEEE
#define DCBX_MODE_CEE             DCBX_VER_CEE
#define DCBX_DEFAULT_MODE         DCBX_MODE_AUTO

/* DCBx Version related Macros*/
#define DCBX_VER_UNKNOWN          3 
#define DCBX_VER_IEEE             1
#define DCBX_VER_CEE              0
#define DCBX_DEFAULT_VER          DCBX_VER_IEEE
#define DCBX_DEF_ETS_SM_TYPE      DCBX_ASYM_STATE_MACHINE
#define DCBX_DEF_PFC_SM_TYPE      DCBX_SYM_STATE_MACHINE
#define DCBX_DEF_APP_PRI_SM_TYPE  DCBX_SYM_STATE_MACHINE

/* DCBX CEE Trap Related Macros */
#define DCBX_CEE_ALL_TRAP (DCBX_LLDP_TX_DISABLE_TRAP | DCBX_LLDP_RX_DISABLE_TRAP \
                           | DCBX_DUP_CTRL_TLV_TRAP | DCBX_PEER_TIMEOUT_TRAP \
                           | DCBX_DUP_FEAT_TLV_TRAP \
                           |DCBX_NO_FEAT_TLV_TRAP | DCBX_FEAT_ERROR_TRAP \
                           | DCBX_UNSUPP_PROTO_TRAP | DCBX_VER_CHG_TRAP)

#define DCBX_LLDP_TX_DISABLE_TRAP       0x00000001
#define DCBX_LLDP_RX_DISABLE_TRAP       0x00000002
#define DCBX_DUP_CTRL_TLV_TRAP          0x00000004
#define DCBX_PEER_TIMEOUT_TRAP          0x00000008
#define DCBX_DUP_FEAT_TLV_TRAP          0x00000010
#define DCBX_NO_FEAT_TLV_TRAP           0x00000020
#define DCBX_FEAT_ERROR_TRAP            0x00000040
#define DCBX_UNSUPP_PROTO_TRAP          0x00000080
#define DCBX_VER_CHG_TRAP               0x00000100
/*****************************************************************************
 dcbxcli.c  functions prototype 
****************************************************************************/
PUBLIC INT4 cli_process_dcbx_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command, ...));
 /*Cli show DCBX running Config */
PUBLIC INT4
DCBXCliShowRunningConfig PROTO ((tCliHandle CliHandle, INT4 i4IfIndex, INT1 dcbxShowInt));
PUBLIC VOID IssDCBXShowDebugging PROTO ((tCliHandle CliHandle));
/*Application Priority Prototype */
INT4        AppPriCliSetControlStatus
PROTO ((tCliHandle CliHandle, UINT1 u1Status));
INT4        AppPriCliSetModuleStatus
PROTO ((tCliHandle CliHandle, UINT1 u1Status));
INT4        AppPriCliSetPortStatus
PROTO ((tCliHandle CliHandle, INT4 i4IfIndex, UINT1 i1APRowStatus));
INT4        AppPriCliSetAdminMode
PROTO ((tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1AdminMode));
INT4        AppPriCliSetTrapStatus
PROTO ((tCliHandle CliHandle, UINT4 u4TrapType, UINT1 u1Status));
INT4        AppPriCliSetWillingStatus
PROTO ((tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1Status));
INT4
AppPriCliSetAppToPriMapping PROTO ((tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4Selector,
                                UINT4 u4Protocol, UINT4 u4Priority));

INT4
AppPriCliShowGbl PROTO ((tCliHandle CliHandle));
INT4
AppPriCliShowPortCounters PROTO ((tCliHandle CliHandle, INT4 i4IfIndex));
INT4
AppPriCliShowRemPortInfo PROTO ((tCliHandle CliHandle, INT4 i4IfIndex));
INT4
AppPriCliShowAdminPortInfo PROTO ((tCliHandle CliHandle, INT4 i4IfIndex));
INT4
AppPriCliShowLocalPortInfo PROTO((tCliHandle CliHandle, INT4 i4IfIndex));
INT4
AppPriCliShowPortDetails PROTO ((tCliHandle CliHandle, INT4 i4IfIndex,
                                 UINT1 u1Detail));
INT4
AppPriCliShowPortInfo PROTO ((tCliHandle CliHandle, INT4 i4IfIndex,
                              UINT1 u1Detail));
INT4
AppPriCliSetTlvSelection PROTO ((tCliHandle CliHandle, INT4 i4IfIndex,
                                 UINT1 u1Status));
INT4
AppPriCliClearCounters PROTO ((tCliHandle CliHandle, INT4 i4IfIndex));

INT4 AppPriShowRunningInterfaceConfig PROTO ((tCliHandle CliHandle, INT4 i4IfIndex, INT1 dcbxShowInt));
INT4
AppPriCliUnSetAppToPriMapping PROTO ((tCliHandle CliHandle, INT4 i4IfIndex,
                                      INT4 i4Selector, UINT4 u4Protocol));
#endif
