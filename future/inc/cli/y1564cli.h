/********************************************************************
 * Copyright (C) 2014 Aricent Inc . All Rights Reserved
 *
 * $Id: y1564cli.h,v 1.11 2016/02/29 11:06:10 siva Exp $
 *
 * Description: This file contains Y1564 CLI command constants, error
 *              strings and function prototypes.
 *
 *******************************************************************/


#ifndef __Y1564CLI_H__
#define __Y1564CLI_H__

#include "cli.h"

/* 
 * Y1564 CLI command constants. To add a new command ,a new definition needs to
 * be added to the list.
 */


enum {
    CLI_Y1564_SHUTDOWN = 1,
    CLI_Y1564_START,
    CLI_Y1564_ENABLE,
    CLI_Y1564_DISABLE,
    CLI_Y1564_SLA_CREATE,
    CLI_Y1564_SLA_DELETE,
    CLI_Y1564_TRAF_PROF_CREATE,
    CLI_Y1564_TRAF_PROF_DELETE,
    CLI_Y1564_SERVICE_CREATE,
    CLI_Y1564_SERVICE_DELETE,
    CLI_Y1564_SAC_CREATE,
    CLI_Y1564_SAC_DELETE,
    CLI_Y1564_NOTIFY_ENABLE,
    CLI_Y1564_NOTIFY_DISABLE,
    CLI_Y1564_SLA_PERFORMANCE_DURATION,
    CLI_Y1564_CONFIGURATION_TEST,
    CLI_Y1564_PERFORMANCE_SLA_LIST,
    CLI_Y1564_PERFORMANCE_SLA_LIST_DELETE,
    CLI_Y1564_PERFORMANCE_TEST,
    CLI_Y1564_SHOW_SLA_CT_REPORT,
    CLI_Y1564_SHOW_SLA_PT_REPORT,
    CLI_Y1564_REPORT_SLA,
    CLI_Y1564_REPORT_SLA_TFTP,
    CLI_Y1564_PERF_REPORT_SLA,
    CLI_Y1564_PERF_REPORT_SLA_TFTP,
    CLI_Y1564_DELETE_REPORT_SLA,
    CLI_Y1564_DELETE_PERF_REPORT_SLA,
    CLI_Y1564_DEBUG,
    CLI_Y1564_NO_DEBUG,
    CLI_Y1564_MAP_EVC,
    CLI_Y1564_NO_MAP_EVC,
    CLI_Y1564_MAP_SERVICECONF,
    CLI_Y1564_NO_MAP_SERVICECONF,
    CLI_Y1564_MAP_TRAF_PROFILE,
    CLI_Y1564_NO_MAP_TRAF_PROFILE,
    CLI_Y1564_MAP_SAC,
    CLI_Y1564_NO_MAP_SAC,
    CLI_Y1564_CFM_CFG,
    CLI_Y1564_NO_CFM_CFG,
    CLI_Y1564_SLA_STEP,
    CLI_Y1564_SLA_DURATION,
    CLI_Y1564_SLA_TRAFFIC_POLICING,
    CLI_Y1564_SLA_METER,
    CLI_Y1564_TRAF_PROF_DIRECTION,
    CLI_Y1564_TRAF_PROF_PKT_SIZE,
    CLI_Y1564_TRAF_PROF_PKT_SIZE_EMIX,
    CLI_Y1564_TRAF_PROF_PAYLOAD,
    CLI_Y1564_TRAF_PROF_PCP,
    CLI_Y1564_SHOW_SLA,
    CLI_Y1564_SHOW_TRAF_PROF,
    CLI_Y1564_SHOW_SAC,
    CLI_Y1564_SHOW_SLA_REPORT,
    CLI_Y1564_SHOW_SERVICE_CONF,
    CLI_Y1564_SHOW_PERF_TEST_CONF,
    CLI_Y1564_TRAFFIC_POLICING_ENABLE,
    CLI_Y1564_TRAFFIC_POLICING_DISABLE,
    CLI_Y1564_SHOW_GLOBAL,
    CLI_Y1564_TEST_SELECTOR,
    CLI_Y1564_MAX_COMMANDS
};

enum
{
    CLI_Y1564_DIRECTION_EXTERNAL = 1,
    CLI_Y1564_DIRECTION_INTERNAL
};

enum
{
    CLI_Y1564_EMIX_ENABLE = 1,
    CLI_Y1564_EMIX_DISABLE
};

enum {
         CLI_Y1564_UNKNOWN_ERR = 0,
/*1*/    CLI_Y1564_ERR_NOT_STARTED,
/*2*/    CLI_Y1564_ERR_BRG_MODE,
/*3*/    CLI_Y1564_ERR_CONTEXT_NOT_PRESENT,
/*4*/    CLI_Y1564_ERR_INVALID_TRACE_OPT,
/*5*/    CLI_Y1564_ERR_INVALID_TRAP_OPT,
/*6*/    CLI_Y1564_ERR_SLA_NOT_CREATED,
/*7*/    CLI_Y1564_ERR_SLA_ALREADY_CREATED,
/*8*/    CLI_Y1564_ERR_SAC_NOT_CREATED,
/*9*/    CLI_Y1564_ERR_SAC_ID_ALREADY_CREATED,
/*10*/   CLI_Y1564_ERR_DEASSOCIATE_SAC_ENTRY,
/*11*/   CLI_Y1564_ERR_SERVICE_CONF_NOT_CREATED,
/*12*/   CLI_Y1564_ERR_SERVICE_CONF_ALREADY_CREATED,
/*13*/   CLI_Y1564_ERR_DEASSOCIATE_SERVICE_CONF_ENTRY,
/*14*/   CLI_Y1564_ERR_TRAF_PROF_NOT_CREATED,
/*15*/   CLI_Y1564_ERR_TRAF_PROF_ALREADY_CREATED,
/*16*/   CLI_Y1564_ERR_DEASSOCIATE_TRAF_PROF_ENTRY,
/*17*/   CLI_Y1564_ERR_SLA_NOT_READY,
/*18*/   CLI_Y1564_ERR_SLA_TEST_PROGRESS,
/*19*/   CLI_Y1564_ERR_SLA_SAC_EXISTS,
/*20*/   CLI_Y1564_ERR_SLA_TRAF_PROF_EXISTS,
/*21*/   CLI_Y1564_ERR_SLA_EVC_EXISTS,
/*22*/   CLI_Y1564_ERR_SLA_SERVICE_CONFIG_EXISTS,
/*23*/   CLI_Y1564_ERR_SLA_SAC_NOT_EXISTS,
/*24*/   CLI_Y1564_ERR_SLA_TRAF_PROF_NOT_EXISTS,
/*25*/   CLI_Y1564_ERR_SLA_EVC_NOT_EXISTS,
/*26*/   CLI_Y1564_ERR_SLA_SERVICE_CONFIG_NOT_EXISTS,
/*27*/   CLI_Y1564_ERR_SLA_CFM_NOT_EXISTS,
/*28*/   CLI_Y1564_DEASSOCIATE_CFM_ENTRY,
/*29*/   CLI_Y1564_ERR_TRAF_PROF_NOT_READY,
/*30*/   CLI_Y1564_ERR_SAC_NOT_READY,
/*31*/   CLI_Y1564_ERR_SAC_INVALID_IR,
/*32*/   CLI_Y1564_ERR_SAC_INVALID_FLR,
/*32*/   CLI_Y1564_ERR_SAC_INVALID_FTD,
/*33*/   CLI_Y1564_ERR_SAC_INVALID_FDV,
/*34*/   CLI_Y1564_ERR_INVALID_AVAILABILITYVALUE,
/*35*/   CLI_Y1564_ERR_INVALID_COLOR_MODE,
/*36*/   CLI_Y1564_ERR_INVALID_COUP_FLAG,
/*37*/   CLI_Y1564_ERR_INVALID_CIR_RATE,
/*38*/   CLI_Y1564_ERR_INVALID_CBS_RATE,
/*39*/   CLI_Y1564_ERR_INVALID_EIR_RATE,
/*40*/   CLI_Y1564_ERR_INVALID_EBS_RATE,
/*41*/   CLI_Y1564_ERR_SERVICE_CONFIG_NOT_READY,
/*42*/   CLI_Y1564_ERR_INVALID_DIRECTION,
/*43*/   CLI_Y1564_ERR_INVALID_PKT_SIZE,
/*44*/   CLI_Y1564_ERR_INVALID_EVC,
/*45*/   CLI_Y1564_ERR_MEG_INVALID_RANGE,
/*46*/   CLI_Y1564_ERR_ME_INVALID_RANGE,
/*47*/   CLI_Y1564_ERR_MEP_INVALID_RANGE,
/*48*/   CLI_Y1564_ERR_INVALID_SAC_ID,
/*49*/   CLI_Y1564_ERR_INVALID_TRAF_PROF_ID,
/*50*/   CLI_Y1564_ERR_INVALID_RATE,
/*51*/   CLI_Y1564_ERR_INVALID_CONF_TEST_DURATION,
/*52*/   CLI_Y1564_ERR_INVALID_TEST_STATUS,
/*53*/   CLI_Y1564_ERR_INVALID_SERVICE_CONF_ID,
/*54*/   CLI_Y1564_ERR_INVALID_TRAF_POLICING,
/*55*/   CLI_Y1564_ERR_INVALID_PERF_DURATION,
/*56*/   CLI_Y1564_ERR_INVALID_TEST_SELECTOR,
/*57*/   CLI_Y1564_ERR_PERF_TEST_NOT_CREATED,
/*58*/   CLI_Y1564_ERR_PERF_TEST_ALREADY_CREATED,
/*59*/   CLI_Y1564_ERR_PERF_TEST_NOT_READY,
/*60*/   CLI_Y1564_ERR_INVALID_SLA_LIST,
/*61*/   CLI_Y1564_ERR_PERF_TEST_SLA_NOT_PRESENT,
/*62*/   CLI_Y1564_ERR_ALL_SLA_IN_PROGRESS_CONF_REPORT_UNAVAILABLE,
/*63*/   CLI_Y1564_ERR_MODULE_NOT_ENABLED,
/*64*/   CLI_Y1564_ERR_TEST_STOP_FAIL,
/*65*/   CLI_Y1564_ERR_SAME_SLA_IN_PROGRESS,
/*66*/   CLI_Y1564_ERR_TEST_NOT_IN_PROGRESS,
/*67*/   CLI_Y1564_ERR_TRAF_PROF_NOT_MAPPED,
/*68*/   CLI_Y1564_ERR_SAC_NOT_MAPPED,
/*69*/   CLI_Y1564_ERR_SERVICE_CONF_NOT_MAPPED,
/*70*/   CLI_Y1564_ERR_EVC_NOT_MAPPED,
/*71*/   CLI_Y1564_ERR_MEG_ME_MEP_NOT_MAPPED,
/*72*/   CLI_Y1564_ERR_INVALID_EMIX_PATTERN,
/*73*/   CLI_Y1564_ERR_INVALID_PAYLOAD,
/*74*/   CLI_Y1564_ERR_INVALID_PAYLOAD_LENGTH,
/*75*/   CLI_Y1564_ERR_MEG_ME_MEP_EXIST,
/*76*/   CLI_Y1564_DEASSOCIATE_EVC_ENTRY,
/*77*/   CLI_Y1564_CIR_GREATER_THAN_PORT_SPEED,
/*78*/   CLI_Y1564_FR_SIZE_GREATER_THAN_MTU,
/*79*/   CLI_Y1564_REMOTE_NODE_NOT_REACHABLE,
/*80*/   CLI_Y1564_ERR_MAX_SLA_REACHED ,
/*81*/   CLI_Y1564_ERR_MAX_SAC_REACHED,
/*82*/   CLI_Y1564_ERR_MAX_TRAF_PROF_REACHED,
/*83*/   CLI_Y1564_ERR_MAX_SERV_CONF_REACHED,
/*84*/   CLI_Y1564_ERR_INVALID_PERF_ID_RANGE,
/*85*/   CLI_Y1564_ERR_MAX_PERF_TABLE_ID_REACHED,
/*86*/   CLI_Y1564_ERR_EVC_EXIST,
/*87*/   CLI_Y1564_ERR_SLA_MAP_ERR,
/*88*/   CLI_Y1564_ERR_FEW_SLA_IN_PROGRESS_CONF_REPORT_UNAVAILABLE,
/*89*/   CLI_Y1564_SAC_GREATER_THAN_CIR_VALUE,
/*90*/   CLI_Y1564_CIR_GREATER_THAN_EIR_VALUE,
/*91*/   CLI_Y1564_ERR_TRAF_POLICY_TEST_START,
/*92*/   CLI_Y1564_ERR_MAX_CONF_TEST_REPORT_REACHED,
/*93*/   CLI_Y1564_ERR_MAX_PERF_TEST_REPORT_REACHED,
/*94*/   CLI_Y1564_ERR_INVALID_SLA_ID,
         CLI_Y1564_MAX_ERR
};

#ifdef  _Y1564CLI_C_
CONST CHR1  *gaY1564CliErrString [] = {
           NULL,
/*1*/   "% Y1564 module not started \r\n",
/*2*/   "% Y1564 cannot be started before bridge mode is set \r\n",
/*3*/   "% Y1564 context doesnot exist \r\n",
/*4*/   "% Invalid Trace options \r\n",
/*5*/   "% Invalid Trap Status Value \r\n",
/*6*/   "% SLA Entry is not present \r\n",
/*7*/   "% SLA Entry is already present \r\n",
/*8*/   "% SAC Entry is not present \r\n",
/*9*/   "% SAC Entry is already present\r\n",
/*10*/  "% De-Associate SAC entry from SLA and then Modify/Delete SAC entry\r\n",
/*11*/  "% Service Configuration Entry is not present \r\n",
/*12*/  "% Service Configuration Entry is already present \r\n",
/*13*/  "% De-Associate Service Configuration entry from SLA and then Modify/Delete Service Configuration entry\r\n",
/*14*/  "% Traffic Profile Entry is not present\r\n",
/*15*/  "% Traffic Profile Entry is already present \r\n",
/*16*/  "% De-Associate Traffic Profile entry from SLA and then Modify/Delete Traffic Profile entry\r\n",
/*17*/  "% Mandatory parameters CFM entries/SAC/Traffic Profile/EVC/Service Config Id is not set \r\n",
/*18*/  "% Modification/Deletion is not allowed when Test is in progress \r\n",
/*19*/  "% Y1564 SLA cannot be deleted, when SAC association/entry is present \r\n",
/*20*/  "% Y1564 SLA cannot be deleted, when Traffic Profile association/entry is present \r\n",
/*21*/  "% Y1564 SLA cannot be deleted, when EVC (to get bandwidth parameters) association is present \r\n",
/*22*/  "% Y1564 SLA cannot be deleted, when Service Config Id (to get bandwidth parameters) association is present \r\n",
/*23*/  "% SLA cannot be activated, when SAC is not associated to the SLA \r\n",
/*24*/  "% SLA cannot be activated, when Traffic Profile is not associated to the SLA \r\n",
/*25*/  "% EVC Index does not exist in MEF\r\n",
/*26*/  "% SLA cannot be activated, when Service Config Id is not associated to the SLA \r\n",
/*27*/  "% CFM entry does not exist in ECFM/Y.1731 \r\n",
/*28*/  "% De-Associate CFM(MEG,ME,MEP) entries from SLA and then Modify/Delete CFM entry \r\n",
/*29*/  "% Mandatory parameter payload information is not set \r\n",
/*30*/  "% Mandatory parameter IR/Availability information is not set \r\n",
/*31*/  "% SAC Invalid value is provided for Information rate \r\n",
/*32*/  "% SAC Invalid value is provided for Frame Loss Ratio \r\n",
/*32*/  "% SAC Invalid value is provided for Frame Transfer delay \r\n",
/*33*/  "% SAC Invalid value is provided for Frame delay variation \r\n",
/*34*/  "% SAC Invalid value is provided for Availability \r\n",
/*35*/  "% Service Conf Invalid value is provided for Color Mode \r\n",
/*36*/  "% Service Conf Invalid value is provided for Coupling Flag \r\n",
/*37*/  "% Service Conf Invalid value is provided for CIR Rate \r\n",
/*38*/  "% Service Conf Invalid value is provided for CBS Rate \r\n",
/*39*/  "% Service Conf Invalid value is provided for EIR Rate \r\n",
/*40*/  "% Service Conf Invalid value is provided for EBS Rate \r\n",
/*41*/  "% Mandatory parameter CIR, CBS, EIR and EBS information is not set \r\n",
/*42*/  "% Invalid Traffic Profile Direction \r\n",
/*43*/  "% Invalid Traffic Profile Packet Size \r\n",
/*44*/  "% Invalid value for EVC Index.The EVC Id configured is not within the range \r\n",
/*45*/  "% Invalid MEG Index. The MEG configured is not within the range \r\n",
/*46*/  "% Invalid ME Index. The ME configured is not within the range \r\n",
/*47*/  "% Invalid MEP Index. The MEP configured is not within the range \r\n",
/*48*/  "% Invalid value for SAC Identifier. The SAC Id configured is not within the range \r\n",
/*49*/  "% Invalid value for Traffic Profile Identifier.The TrafProf Id configured is not within the range \r\n",
/*50*/  "% Invalid value for Rate Step or Rate Step should be divisor of 100 \r\n",
/*51*/  "% Invalid Timer value. Time value configured is not within the range \r\n",
/*52*/  "% Invalid Test Status. The Value can be either start or stop \r\n",
/*53*/  "% Invalid service configuration Identifier. The Value configured is not within the range \r\n",
/*54*/  "% Invalid Traffic Policing Status. The value can be either on or off \r\n",
/*55*/  "% Invalid Timer value for performance test \r\n",
/*56*/  "% Invalid Test Selector value \r\n",
/*57*/  "% Performance Test Entry is not present\r\n",
/*58*/  "% Performance Test Entry is already present \r\n",
/*59*/  "% Mandatory parameter SLA list is not set to initiate performance test \r\n",
/*60*/  "% Invalid SLA list \r\n",
/*61*/  "% SLA Configured in Performance test entry is not present \r\n",
/*62*/  "% Performance test cannot be initiated for SLA's when configuration test is not success or test is in-progress \r\n",
/*63*/  "% Module Status is not enabled\r\n",
/*64*/  "% Unable to stop test \r\n",
/*65*/  "% Test is in progress for same SLA \r\n",
/*66*/  "% Test is not started\r\n",
/*67*/  "% No Traffic Profile entry is mapped to SLA\r\n",
/*68*/  "% No SAC entry is mapped to SLA\r\n",
/*69*/  "% No Service Configuration entry is mapped to SLA\r\n",
/*70*/  "% No EVC is mapped to SLA\r\n",
/*71*/  "% No CFM entry (either MEG, ME or MEP) is mapped to SLA\r\n",
/*72*/  "% Invalid EMIX pattern is provided\r\n",
/*73*/  "% Characters(A-Z,a-z) and numeric values (0-9) only allowed\r\n",
/*74*/  "% Invalid Payload length\r\n",
/*75*/  "% CFM Entries are already mapped with other SLA\r\n",
/*76*/  "% De-Associate EVC entries from SLA and then Modify/Delete EVC entry \r\n",
/*77*/  "% CIR is greater than port speed ,so test will not be initiated for SLA \r\n",
/*78*/  "% Frame Size is greater than the MTU size, so test will not be initiated \r\n",
/*79*/  "% Destination MAC address is ZERO. Either ECFM is not settled or Remote node is not reachable \r\n",
/*80*/  "% Maximum No of Allowed SLAs reached \r\n",
/*81*/  "% Maximum No of Allowed SACs reached \r\n",
/*82*/  "% Maximum No of Allowed Traffic Profile Entries reached \r\n",
/*83*/  "% Maximum No of Allowed Service Config Entries reached \r\n",
/*84*/  "% Invalid Perf Id Range \r\n",
/*85*/  "% Maximum No of Allowed Performance Test Table Entries reached \r\n",
/*86*/  "% EVC Entry is already mapped with other SLA \r\n",
/*87*/  "% Sla Entry is already mapped with other Perf Test Entry \r\n",
/*88*/  "% Performance test for few SLA's cannot be initiated when configuration test is not success or test is in-progress \r\n",
/*89*/  "% SAC Value is greater than the CIR Value, so test will not be initiated for SLA \r\n",
/*90*/  "% CIR Value is greater than the EIR Value \r\n",
/*91*/  "% Traffic Policing cannot be started when traffic policing status is disabled \r\n",
/*92*/  "% Maximum No of Allowed Configuration Test Report Entries reached \r\n",
/*93*/  "% Maximum No of Allowed Performance Test Report Entries reached \r\n",
/*94*/  "% Invalid value for SLA Identifier. The SLA Id configured is not within the range \r\n",
   "\r\n"
};
#else
extern CONST CHR1  *gaY1564CliErrString[];
#endif



#define Y1564_CLI_MAX_ARGS       10
#define Y1564_CLI_INVALID_CONTEXT   0xFFFFFFFF

#define Y1564_CLI_STOP       2
#define Y1564_CLI_START      1

INT4 cli_process_y1564_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);
INT4 cli_process_y1564_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);

INT4 Y1564CliSetSystemControl (tCliHandle CliHandle, UINT4 u4ContextId, 
                               INT4 i4SystemControl);

INT4 Y1564CliSetModuleStatus (tCliHandle CliHandle, UINT4 u4ContextId, 
                              INT4 i4ModuleStatus);

INT4 Y1564CliSetSlaCreate (tCliHandle CliHandle, UINT4 u4ContextId,
                           UINT4 u4SlaId);

INT4 Y1564CliSetSlaDelete (tCliHandle CliHandle, UINT4 u4ContextId, 
                           UINT4 u4SlaId);

INT4
Y1564CliSetTrafProfCreate (tCliHandle CliHandle, UINT4 u4ContextId,
                           UINT4 u4TrafProfId);
INT4
Y1564CliSetTrafProfDelete (tCliHandle CliHandle, UINT4 u4ContextId,
                           UINT4 u4TrafProfId);
INT4
Y1564CliSetServiceCreate (tCliHandle CliHandle, UINT4 u4ContextId,
                          UINT4 u4ServConfId);
INT4
Y1564CliSetServiceDelete (tCliHandle CliHandle, UINT4 u4ContextId,
                          UINT4 u4ServConfId);
INT4
Y1564CliSetSacCreate (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4SacId,
                      UINT4 u4SacIr, UINT4 u4SacFlr, UINT4 u4SacFtd,
                      UINT4 u4SacFdv, UINT1 *pu1Availablity);
INT4
Y1564CliSetSacDelete (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4SacId);

INT4
Y1564CliSetNotification (tCliHandle CliHandle, UINT4 u4ContextId,
                         INT4 i4TrapStatus);
INT4
Y1564CliSetPerfDuration (tCliHandle CliHandle, UINT4 u4ContextId,
                         UINT4 u4PerfTestId,
                         UINT4 u4PerfTestDuration);
INT4
Y1564CliSetConfigurationTest (tCliHandle CliHandle, UINT4 u4ContextId,
                              UINT4 u4SlaId, UINT4 u4SlaStatus);
INT4
Y1564CliSetPerformanceTest (tCliHandle CliHandle, UINT4 u4ContextId,
                            UINT4 u4PerfTestId, UINT4 u4PerfStatus);

INT4
Y1564CliSetPerformanceSlaList (tCliHandle CliHandle, UINT4 u4ContextId,
                               UINT4 u4PerfTestId, UINT1 * pu1SlaList);

INT4
Y1564CliSetPerformanceSlaListDelete (tCliHandle CliHandle, UINT4 u4ContextId,
                                     UINT4 u4PerfTestId);

INT4
Y1564CliSetDebugLevel (tCliHandle CliHandle, INT4 i4CliDebugLevel);
INT4
Y1564CliSetTraceOption (tCliHandle CliHandle, UINT4 u4ContextId,
                        INT4 i4DebugType, INT4 i4Action);
INT4
Y1564CliSetSlaMapEvc (tCliHandle CliHandle, UINT4 u4ContextId,
                      UINT4 u4SlaId, UINT4 u4EvcIndex);
INT4
Y1564CliSetSlaUnMapEvc (tCliHandle CliHandle, UINT4 u4ContextId,
                        UINT4 u4SlaId, UINT4 u4EvcIndex);
INT4
Y1564CliSetSlaMapServiceConfig (tCliHandle CliHandle, UINT4 u4ContextId,
                                UINT4 u4SlaId, UINT4 u4ServConfigId);
INT4
Y1564CliSetSlaUnMapServiceConfig (tCliHandle CliHandle, UINT4 u4ContextId,
                                  UINT4 u4SlaId, UINT4 u4ServConfigId);
INT4
Y1564CliSetSlaMapTrafficProfile (tCliHandle CliHandle,UINT4 u4ContextId,
                                 UINT4 u4SlaId, UINT4 u4TrafProfId);
INT4
Y1564CliSetSlaUnMapTrafficProfile (tCliHandle CliHandle,UINT4 u4ContextId,
                                   UINT4 u4SlaId, UINT4 u4TrafProfId);
INT4
Y1564CliSetSlaMapSac (tCliHandle CliHandle, UINT4 u4ContextId,
                      UINT4 u4SlaId, UINT4 u4SacId);
INT4
Y1564CliSetSlaUnMapSac (tCliHandle CliHandle, UINT4 u4ContextId,
                        UINT4 u4SlaId, UINT4 u4SacId);
INT4
Y1564CliSetSlaMapCfmEntries (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4SlaId,
                             UINT4 u4SlaMeg, UINT4 u4SlaMe, UINT4 u4SlaMep);
INT4
Y1564CliSetSlaUnMapCfmEntries (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4SlaId,
                               UINT4 u4SlaMeg, UINT4 u4SlaMe, UINT4 u4SlaMep);
INT4
Y1564CliSetSlaStepLoadRate (tCliHandle CliHandle, UINT4 u4ContextId,
                            UINT4 u4SlaId, UINT4 u4RateStep);
INT4
Y1564CliSetSlaDuration (tCliHandle CliHandle, UINT4 u4ContextId,
                        UINT4 u4SlaId, UINT4 u4Duration);
INT4
Y1564CliSetTrafPolicingStatus (tCliHandle CliHandle, UINT4 u4ContextId,
                               UINT4 u4SlaId, INT4 i4TrafPolicingStatus);
INT4 
Y1564CliSetMeterInfo (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4ServiceConfId,
                      UINT4 u4ServiceConfCIR, UINT4 u4ServiceConfCBS,
                      UINT4 u4ServiceConfEIR, UINT4 u4ServiceConfEBS,
                      UINT4 u4ServiceConfColorMode,
                      UINT4 u4ServiceConfCoupFlag);
INT4
Y1564CliSetTrafProfDirection (tCliHandle CliHandle, UINT4 u4ContextId,
                              UINT4 u4TrafProfId, UINT4 u4Direction);
INT4
Y1564CliSetPacketSize (tCliHandle CliHandle, UINT4 ContextId,
                       UINT4 u4TrafProfId, UINT4 u4PacketSize);
INT4
Y1564CliSetEmixPacketSize (tCliHandle CliHandle, UINT4 u4ContextId,
                           UINT4 u4TrafProfId, UINT1 *pu1EmixPktSize);
INT4
Y1564CliSetPayload (tCliHandle CliHandle, UINT4 u4ContextId,
                    UINT4 u4TrafProfId, UINT1 *pu1Payload);
INT4
Y1564CliSetPCP (tCliHandle CliHandle, UINT4 u4ContextId,
                UINT4 u4TrafProfId, UINT4 u4TrafProfPCP);
INT4
Y1564CliSetTestSelector (tCliHandle CliHandle, UINT4 u4ContextId,
                         UINT4 u4SlaId, UINT4 u4TrafProfPCP);
INT4
Y1564CliSaveConfReport (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4Command,
                        CHR1 * pu1FileName, UINT4 u4SlaId);
INT4
Y1564CliSavePerfReport (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4Command,
                        CHR1 * pu1FileName, UINT4 u4PerfTestId);
INT4
Y1564CliDeleteReport (tCliHandle CliHandle, CHR1 * pu1FileName);

INT1
Y1564GetSlaCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

INT1
Y1564GetTrafficProfileCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

INT1
Y1564GetVcmSlaCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

INT1
Y1564GetVcmTrafficProfileCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

INT1
Y1564GetServiceConfCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

INT1
Y1564GetVcmServiceConfCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

VOID
Y1564CliShowDebug (tCliHandle CliHandle, UINT4 u4ContextId);

INT4 Y1564ShowRunningConfig (tCliHandle CliHandle);

INT4 Y1564ShowRunningConfigCxtCmds (tCliHandle CliHandle);

INT4 Y1564ShowRunningConfigSacCmds (tCliHandle CliHandle);

INT4 Y1564ShowRunningConfigSlaCmds (tCliHandle CliHandle);

INT4 Y1564ShowRunningConfigTrafProfCmds (tCliHandle CliHandle);

INT4
Y1564ShowRunningConfigPerfTestCmds (tCliHandle CliHandle);

INT4
Y1564CliSelectContextOnMode (tCliHandle CliHandle,
                             UINT4 u4Cmd,
                             UINT4 *pu4Context,
                             UINT2 *pu2LocalPort);

INT4
Y1564ShowRunningConfigServConfCmds (tCliHandle CliHandle);

INT4
Y1564CliGetContextInfoForShowCmd (tCliHandle CliHandle, UINT1 *pu1ContextName,
                                  UINT4 u4CurrContextId, UINT4 *pu4ContextId);

INT4
Y1564CliShowSlaTable (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4SlaId);

INT4
Y1564CliShowTrafProfTable (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4ProfileId);

INT4
Y1564CliShowSacTable (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4SacId);

INT4
Y1564CliShowServiceConfTable (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4ServiceConfId);

INT4
Y1564CliShowPerfTestTable (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4PerfTestId);

INT4
Y1564CliShowConfTestReport (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4SlaId);

INT4
Y1564CliShowPerfTestReport (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4PerfTestId);

INT4
Y1564CliShowGlobalInfo (tCliHandle CliHandle, UINT4 u4ContextId);

VOID
Y1564CliShowPortType (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4TrafProfId);

#endif /* __Y1564CLI_H__ */
