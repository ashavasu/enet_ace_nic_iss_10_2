/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmidhcli.h,v 1.1 2014/04/23 12:21:03 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIDhcpSnpContextId[13];
extern UINT4 FsMIDhcpSnpSnoopingAdminStatus[13];
extern UINT4 FsMIDhcpSnpMacVerifyStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMIDhcpSnpSnoopingAdminStatus(i4FsMIDhcpSnpContextId ,i4SetValFsMIDhcpSnpSnoopingAdminStatus)	\
	nmhSetCmn(FsMIDhcpSnpSnoopingAdminStatus, 13, FsMIDhcpSnpSnoopingAdminStatusSet, L2DS_LOCK, L2DS_UNLOCK, 0, 0, 1, "%i %i", i4FsMIDhcpSnpContextId ,i4SetValFsMIDhcpSnpSnoopingAdminStatus)
#define nmhSetFsMIDhcpSnpMacVerifyStatus(i4FsMIDhcpSnpContextId ,i4SetValFsMIDhcpSnpMacVerifyStatus)	\
	nmhSetCmn(FsMIDhcpSnpMacVerifyStatus, 13, FsMIDhcpSnpMacVerifyStatusSet, L2DS_LOCK, L2DS_UNLOCK, 0, 0, 1, "%i %i", i4FsMIDhcpSnpContextId ,i4SetValFsMIDhcpSnpMacVerifyStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIDhcpSnpVlanId[13];
extern UINT4 FsMIDhcpSnpVlanSnpStatus[13];
extern UINT4 FsMIDhcpSnpInterfaceStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMIDhcpSnpVlanSnpStatus(i4FsMIDhcpSnpContextId , i4FsMIDhcpSnpVlanId ,i4SetValFsMIDhcpSnpVlanSnpStatus)	\
	nmhSetCmn(FsMIDhcpSnpVlanSnpStatus, 13, FsMIDhcpSnpVlanSnpStatusSet, L2DS_LOCK, L2DS_UNLOCK, 0, 0, 2, "%i %i %i", i4FsMIDhcpSnpContextId , i4FsMIDhcpSnpVlanId ,i4SetValFsMIDhcpSnpVlanSnpStatus)
#define nmhSetFsMIDhcpSnpInterfaceStatus(i4FsMIDhcpSnpContextId , i4FsMIDhcpSnpVlanId ,i4SetValFsMIDhcpSnpInterfaceStatus)	\
	nmhSetCmn(FsMIDhcpSnpInterfaceStatus, 13, FsMIDhcpSnpInterfaceStatusSet, L2DS_LOCK, L2DS_UNLOCK, 0, 1, 2, "%i %i %i", i4FsMIDhcpSnpContextId , i4FsMIDhcpSnpVlanId ,i4SetValFsMIDhcpSnpInterfaceStatus)

#endif
