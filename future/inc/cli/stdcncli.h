/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: stdcncli.h,v 1.2 2013/09/27 07:25:21 siva Exp $
*
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ieee8021CnGlobalComponentId[12];
extern UINT4 Ieee8021CnGlobalMasterEnable[12];
extern UINT4 Ieee8021CnGlobalCnmTransmitPriority[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIeee8021CnGlobalMasterEnable(u4Ieee8021CnGlobalComponentId ,i4SetValIeee8021CnGlobalMasterEnable) \
 nmhSetCmn(Ieee8021CnGlobalMasterEnable, 12, Ieee8021CnGlobalMasterEnableSet, NULL, NULL, 0, 0, 1, "%u %i", u4Ieee8021CnGlobalComponentId ,i4SetValIeee8021CnGlobalMasterEnable)
#define nmhSetIeee8021CnGlobalCnmTransmitPriority(u4Ieee8021CnGlobalComponentId ,u4SetValIeee8021CnGlobalCnmTransmitPriority) \
 nmhSetCmn(Ieee8021CnGlobalCnmTransmitPriority, 12, Ieee8021CnGlobalCnmTransmitPrioritySet, NULL, NULL, 0, 0, 1, "%u %u", u4Ieee8021CnGlobalComponentId ,u4SetValIeee8021CnGlobalCnmTransmitPriority)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ieee8021CnComPriComponentId[12];
extern UINT4 Ieee8021CnComPriPriority[12];
extern UINT4 Ieee8021CnComPriDefModeChoice[12];
extern UINT4 Ieee8021CnComPriAlternatePriority[12];
extern UINT4 Ieee8021CnComPriAdminDefenseMode[12];
extern UINT4 Ieee8021CnComPriCreation[12];
extern UINT4 Ieee8021CnComPriLldpInstanceChoice[12];
extern UINT4 Ieee8021CnComPriLldpInstanceSelector[12];
extern UINT4 Ieee8021CnComPriRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIeee8021CnComPriDefModeChoice(u4Ieee8021CnComPriComponentId , u4Ieee8021CnComPriPriority ,i4SetValIeee8021CnComPriDefModeChoice) \
 nmhSetCmn(Ieee8021CnComPriDefModeChoice, 12, Ieee8021CnComPriDefModeChoiceSet, NULL, NULL, 0, 0, 2, "%u %u %i", u4Ieee8021CnComPriComponentId , u4Ieee8021CnComPriPriority ,i4SetValIeee8021CnComPriDefModeChoice)
#define nmhSetIeee8021CnComPriAlternatePriority(u4Ieee8021CnComPriComponentId , u4Ieee8021CnComPriPriority ,u4SetValIeee8021CnComPriAlternatePriority) \
 nmhSetCmn(Ieee8021CnComPriAlternatePriority, 12, Ieee8021CnComPriAlternatePrioritySet, NULL, NULL, 0, 0, 2, "%u %u %u", u4Ieee8021CnComPriComponentId , u4Ieee8021CnComPriPriority ,u4SetValIeee8021CnComPriAlternatePriority)
#define nmhSetIeee8021CnComPriAdminDefenseMode(u4Ieee8021CnComPriComponentId , u4Ieee8021CnComPriPriority ,i4SetValIeee8021CnComPriAdminDefenseMode) \
 nmhSetCmn(Ieee8021CnComPriAdminDefenseMode, 12, Ieee8021CnComPriAdminDefenseModeSet, NULL, NULL, 0, 0, 2, "%u %u %i", u4Ieee8021CnComPriComponentId , u4Ieee8021CnComPriPriority ,i4SetValIeee8021CnComPriAdminDefenseMode)
#define nmhSetIeee8021CnComPriCreation(u4Ieee8021CnComPriComponentId , u4Ieee8021CnComPriPriority ,i4SetValIeee8021CnComPriCreation) \
 nmhSetCmn(Ieee8021CnComPriCreation, 12, Ieee8021CnComPriCreationSet, NULL, NULL, 0, 0, 2, "%u %u %i", u4Ieee8021CnComPriComponentId , u4Ieee8021CnComPriPriority ,i4SetValIeee8021CnComPriCreation)
#define nmhSetIeee8021CnComPriLldpInstanceChoice(u4Ieee8021CnComPriComponentId , u4Ieee8021CnComPriPriority ,i4SetValIeee8021CnComPriLldpInstanceChoice) \
 nmhSetCmn(Ieee8021CnComPriLldpInstanceChoice, 12, Ieee8021CnComPriLldpInstanceChoiceSet, NULL, NULL, 0, 0, 2, "%u %u %i", u4Ieee8021CnComPriComponentId , u4Ieee8021CnComPriPriority ,i4SetValIeee8021CnComPriLldpInstanceChoice)
#define nmhSetIeee8021CnComPriLldpInstanceSelector(u4Ieee8021CnComPriComponentId , u4Ieee8021CnComPriPriority ,u4SetValIeee8021CnComPriLldpInstanceSelector) \
 nmhSetCmn(Ieee8021CnComPriLldpInstanceSelector, 12, Ieee8021CnComPriLldpInstanceSelectorSet, NULL, NULL, 0, 0, 2, "%u %u %u", u4Ieee8021CnComPriComponentId , u4Ieee8021CnComPriPriority ,u4SetValIeee8021CnComPriLldpInstanceSelector)
#define nmhSetIeee8021CnComPriRowStatus(u4Ieee8021CnComPriComponentId , u4Ieee8021CnComPriPriority ,i4SetValIeee8021CnComPriRowStatus) \
 nmhSetCmn(Ieee8021CnComPriRowStatus, 12, Ieee8021CnComPriRowStatusSet, NULL, NULL, 0, 1, 2, "%u %u %i", u4Ieee8021CnComPriComponentId , u4Ieee8021CnComPriPriority ,i4SetValIeee8021CnComPriRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ieee8021CnPortPriComponentId[12];
extern UINT4 Ieee8021CnPortPriority[12];
extern UINT4 Ieee8021CnPortPriIfIndex[12];
extern UINT4 Ieee8021CnPortPriDefModeChoice[12];
extern UINT4 Ieee8021CnPortPriAdminDefenseMode[12];
extern UINT4 Ieee8021CnPortPriLldpInstanceChoice[12];
extern UINT4 Ieee8021CnPortPriLldpInstanceSelector[12];
extern UINT4 Ieee8021CnPortPriAlternatePriority[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIeee8021CnPortPriDefModeChoice(u4Ieee8021CnPortPriComponentId , u4Ieee8021CnPortPriority , i4Ieee8021CnPortPriIfIndex ,i4SetValIeee8021CnPortPriDefModeChoice) \
 nmhSetCmn(Ieee8021CnPortPriDefModeChoice, 12, Ieee8021CnPortPriDefModeChoiceSet, NULL, NULL, 0, 0, 3, "%u %u %i %i", u4Ieee8021CnPortPriComponentId , u4Ieee8021CnPortPriority , i4Ieee8021CnPortPriIfIndex ,i4SetValIeee8021CnPortPriDefModeChoice)
#define nmhSetIeee8021CnPortPriAdminDefenseMode(u4Ieee8021CnPortPriComponentId , u4Ieee8021CnPortPriority , i4Ieee8021CnPortPriIfIndex ,i4SetValIeee8021CnPortPriAdminDefenseMode) \
 nmhSetCmn(Ieee8021CnPortPriAdminDefenseMode, 12, Ieee8021CnPortPriAdminDefenseModeSet, NULL, NULL, 0, 0, 3, "%u %u %i %i", u4Ieee8021CnPortPriComponentId , u4Ieee8021CnPortPriority , i4Ieee8021CnPortPriIfIndex ,i4SetValIeee8021CnPortPriAdminDefenseMode)
#define nmhSetIeee8021CnPortPriLldpInstanceChoice(u4Ieee8021CnPortPriComponentId , u4Ieee8021CnPortPriority , i4Ieee8021CnPortPriIfIndex ,i4SetValIeee8021CnPortPriLldpInstanceChoice) \
 nmhSetCmn(Ieee8021CnPortPriLldpInstanceChoice, 12, Ieee8021CnPortPriLldpInstanceChoiceSet, NULL, NULL, 0, 0, 3, "%u %u %i %i", u4Ieee8021CnPortPriComponentId , u4Ieee8021CnPortPriority , i4Ieee8021CnPortPriIfIndex ,i4SetValIeee8021CnPortPriLldpInstanceChoice)
#define nmhSetIeee8021CnPortPriLldpInstanceSelector(u4Ieee8021CnPortPriComponentId , u4Ieee8021CnPortPriority , i4Ieee8021CnPortPriIfIndex ,u4SetValIeee8021CnPortPriLldpInstanceSelector) \
 nmhSetCmn(Ieee8021CnPortPriLldpInstanceSelector, 12, Ieee8021CnPortPriLldpInstanceSelectorSet, NULL, NULL, 0, 0, 3, "%u %u %i %u", u4Ieee8021CnPortPriComponentId , u4Ieee8021CnPortPriority , i4Ieee8021CnPortPriIfIndex ,u4SetValIeee8021CnPortPriLldpInstanceSelector)
#define nmhSetIeee8021CnPortPriAlternatePriority(u4Ieee8021CnPortPriComponentId , u4Ieee8021CnPortPriority , i4Ieee8021CnPortPriIfIndex ,u4SetValIeee8021CnPortPriAlternatePriority) \
 nmhSetCmn(Ieee8021CnPortPriAlternatePriority, 12, Ieee8021CnPortPriAlternatePrioritySet, NULL, NULL, 0, 0, 3, "%u %u %i %u", u4Ieee8021CnPortPriComponentId , u4Ieee8021CnPortPriority , i4Ieee8021CnPortPriIfIndex ,u4SetValIeee8021CnPortPriAlternatePriority)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ieee8021CnCpComponentId[12];
extern UINT4 Ieee8021CnCpIfIndex[12];
extern UINT4 Ieee8021CnCpIndex[12];
extern UINT4 Ieee8021CnCpQueueSizeSetPoint[12];
extern UINT4 Ieee8021CnCpFeedbackWeight[12];
extern UINT4 Ieee8021CnCpMinSampleBase[12];
extern UINT4 Ieee8021CnCpMinHeaderOctets[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIeee8021CnCpQueueSizeSetPoint(u4Ieee8021CnCpComponentId , i4Ieee8021CnCpIfIndex , u4Ieee8021CnCpIndex ,u4SetValIeee8021CnCpQueueSizeSetPoint) \
 nmhSetCmn(Ieee8021CnCpQueueSizeSetPoint, 12, Ieee8021CnCpQueueSizeSetPointSet, NULL, NULL, 0, 0, 3, "%u %i %u %u", u4Ieee8021CnCpComponentId , i4Ieee8021CnCpIfIndex , u4Ieee8021CnCpIndex ,u4SetValIeee8021CnCpQueueSizeSetPoint)
#define nmhSetIeee8021CnCpFeedbackWeight(u4Ieee8021CnCpComponentId , i4Ieee8021CnCpIfIndex , u4Ieee8021CnCpIndex ,i4SetValIeee8021CnCpFeedbackWeight) \
 nmhSetCmn(Ieee8021CnCpFeedbackWeight, 12, Ieee8021CnCpFeedbackWeightSet, NULL, NULL, 0, 0, 3, "%u %i %u %i", u4Ieee8021CnCpComponentId , i4Ieee8021CnCpIfIndex , u4Ieee8021CnCpIndex ,i4SetValIeee8021CnCpFeedbackWeight)
#define nmhSetIeee8021CnCpMinSampleBase(u4Ieee8021CnCpComponentId , i4Ieee8021CnCpIfIndex , u4Ieee8021CnCpIndex ,u4SetValIeee8021CnCpMinSampleBase) \
 nmhSetCmn(Ieee8021CnCpMinSampleBase, 12, Ieee8021CnCpMinSampleBaseSet, NULL, NULL, 0, 0, 3, "%u %i %u %u", u4Ieee8021CnCpComponentId , i4Ieee8021CnCpIfIndex , u4Ieee8021CnCpIndex ,u4SetValIeee8021CnCpMinSampleBase)
#define nmhSetIeee8021CnCpMinHeaderOctets(u4Ieee8021CnCpComponentId , i4Ieee8021CnCpIfIndex , u4Ieee8021CnCpIndex ,u4SetValIeee8021CnCpMinHeaderOctets) \
 nmhSetCmn(Ieee8021CnCpMinHeaderOctets, 12, Ieee8021CnCpMinHeaderOctetsSet, NULL, NULL, 0, 0, 3, "%u %i %u %u", u4Ieee8021CnCpComponentId , i4Ieee8021CnCpIfIndex , u4Ieee8021CnCpIndex ,u4SetValIeee8021CnCpMinHeaderOctets)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ieee8021CnRpPortPriComponentId[12];
extern UINT4 Ieee8021CnRpPortPriPriority[12];
extern UINT4 Ieee8021CnRpPortPriIfIndex[12];
extern UINT4 Ieee8021CnRpPortPriMaxRps[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIeee8021CnRpPortPriMaxRps(u4Ieee8021CnRpPortPriComponentId , u4Ieee8021CnRpPortPriPriority , i4Ieee8021CnRpPortPriIfIndex ,u4SetValIeee8021CnRpPortPriMaxRps) \
 nmhSetCmn(Ieee8021CnRpPortPriMaxRps, 12, Ieee8021CnRpPortPriMaxRpsSet, NULL, NULL, 0, 0, 3, "%u %u %i %u", u4Ieee8021CnRpPortPriComponentId , u4Ieee8021CnRpPortPriPriority , i4Ieee8021CnRpPortPriIfIndex ,u4SetValIeee8021CnRpPortPriMaxRps)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ieee8021CnRpgComponentId[12];
extern UINT4 Ieee8021CnRpgPriority[12];
extern UINT4 Ieee8021CnRpgIfIndex[12];
extern UINT4 Ieee8021CnRpgIdentifier[12];
extern UINT4 Ieee8021CnRpgEnable[12];
extern UINT4 Ieee8021CnRpgTimeReset[12];
extern UINT4 Ieee8021CnRpgByteReset[12];
extern UINT4 Ieee8021CnRpgThreshold[12];
extern UINT4 Ieee8021CnRpgMaxRate[12];
extern UINT4 Ieee8021CnRpgAiRate[12];
extern UINT4 Ieee8021CnRpgHaiRate[12];
extern UINT4 Ieee8021CnRpgGd[12];
extern UINT4 Ieee8021CnRpgMinDecFac[12];
extern UINT4 Ieee8021CnRpgMinRate[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIeee8021CnRpgEnable(u4Ieee8021CnRpgComponentId , u4Ieee8021CnRpgPriority , i4Ieee8021CnRpgIfIndex , u4Ieee8021CnRpgIdentifier ,i4SetValIeee8021CnRpgEnable) \
 nmhSetCmn(Ieee8021CnRpgEnable, 12, Ieee8021CnRpgEnableSet, NULL, NULL, 0, 0, 4, "%u %u %i %u %i", u4Ieee8021CnRpgComponentId , u4Ieee8021CnRpgPriority , i4Ieee8021CnRpgIfIndex , u4Ieee8021CnRpgIdentifier ,i4SetValIeee8021CnRpgEnable)
#define nmhSetIeee8021CnRpgTimeReset(u4Ieee8021CnRpgComponentId , u4Ieee8021CnRpgPriority , i4Ieee8021CnRpgIfIndex , u4Ieee8021CnRpgIdentifier ,i4SetValIeee8021CnRpgTimeReset) \
 nmhSetCmn(Ieee8021CnRpgTimeReset, 12, Ieee8021CnRpgTimeResetSet, NULL, NULL, 0, 0, 4, "%u %u %i %u %i", u4Ieee8021CnRpgComponentId , u4Ieee8021CnRpgPriority , i4Ieee8021CnRpgIfIndex , u4Ieee8021CnRpgIdentifier ,i4SetValIeee8021CnRpgTimeReset)
#define nmhSetIeee8021CnRpgByteReset(u4Ieee8021CnRpgComponentId , u4Ieee8021CnRpgPriority , i4Ieee8021CnRpgIfIndex , u4Ieee8021CnRpgIdentifier ,u4SetValIeee8021CnRpgByteReset) \
 nmhSetCmn(Ieee8021CnRpgByteReset, 12, Ieee8021CnRpgByteResetSet, NULL, NULL, 0, 0, 4, "%u %u %i %u %u", u4Ieee8021CnRpgComponentId , u4Ieee8021CnRpgPriority , i4Ieee8021CnRpgIfIndex , u4Ieee8021CnRpgIdentifier ,u4SetValIeee8021CnRpgByteReset)
#define nmhSetIeee8021CnRpgThreshold(u4Ieee8021CnRpgComponentId , u4Ieee8021CnRpgPriority , i4Ieee8021CnRpgIfIndex , u4Ieee8021CnRpgIdentifier ,u4SetValIeee8021CnRpgThreshold) \
 nmhSetCmn(Ieee8021CnRpgThreshold, 12, Ieee8021CnRpgThresholdSet, NULL, NULL, 0, 0, 4, "%u %u %i %u %u", u4Ieee8021CnRpgComponentId , u4Ieee8021CnRpgPriority , i4Ieee8021CnRpgIfIndex , u4Ieee8021CnRpgIdentifier ,u4SetValIeee8021CnRpgThreshold)
#define nmhSetIeee8021CnRpgMaxRate(u4Ieee8021CnRpgComponentId , u4Ieee8021CnRpgPriority , i4Ieee8021CnRpgIfIndex , u4Ieee8021CnRpgIdentifier ,u4SetValIeee8021CnRpgMaxRate) \
 nmhSetCmn(Ieee8021CnRpgMaxRate, 12, Ieee8021CnRpgMaxRateSet, NULL, NULL, 0, 0, 4, "%u %u %i %u %u", u4Ieee8021CnRpgComponentId , u4Ieee8021CnRpgPriority , i4Ieee8021CnRpgIfIndex , u4Ieee8021CnRpgIdentifier ,u4SetValIeee8021CnRpgMaxRate)
#define nmhSetIeee8021CnRpgAiRate(u4Ieee8021CnRpgComponentId , u4Ieee8021CnRpgPriority , i4Ieee8021CnRpgIfIndex , u4Ieee8021CnRpgIdentifier ,u4SetValIeee8021CnRpgAiRate) \
 nmhSetCmn(Ieee8021CnRpgAiRate, 12, Ieee8021CnRpgAiRateSet, NULL, NULL, 0, 0, 4, "%u %u %i %u %u", u4Ieee8021CnRpgComponentId , u4Ieee8021CnRpgPriority , i4Ieee8021CnRpgIfIndex , u4Ieee8021CnRpgIdentifier ,u4SetValIeee8021CnRpgAiRate)
#define nmhSetIeee8021CnRpgHaiRate(u4Ieee8021CnRpgComponentId , u4Ieee8021CnRpgPriority , i4Ieee8021CnRpgIfIndex , u4Ieee8021CnRpgIdentifier ,u4SetValIeee8021CnRpgHaiRate) \
 nmhSetCmn(Ieee8021CnRpgHaiRate, 12, Ieee8021CnRpgHaiRateSet, NULL, NULL, 0, 0, 4, "%u %u %i %u %u", u4Ieee8021CnRpgComponentId , u4Ieee8021CnRpgPriority , i4Ieee8021CnRpgIfIndex , u4Ieee8021CnRpgIdentifier ,u4SetValIeee8021CnRpgHaiRate)
#define nmhSetIeee8021CnRpgGd(u4Ieee8021CnRpgComponentId , u4Ieee8021CnRpgPriority , i4Ieee8021CnRpgIfIndex , u4Ieee8021CnRpgIdentifier ,i4SetValIeee8021CnRpgGd) \
 nmhSetCmn(Ieee8021CnRpgGd, 12, Ieee8021CnRpgGdSet, NULL, NULL, 0, 0, 4, "%u %u %i %u %i", u4Ieee8021CnRpgComponentId , u4Ieee8021CnRpgPriority , i4Ieee8021CnRpgIfIndex , u4Ieee8021CnRpgIdentifier ,i4SetValIeee8021CnRpgGd)
#define nmhSetIeee8021CnRpgMinDecFac(u4Ieee8021CnRpgComponentId , u4Ieee8021CnRpgPriority , i4Ieee8021CnRpgIfIndex , u4Ieee8021CnRpgIdentifier ,u4SetValIeee8021CnRpgMinDecFac) \
 nmhSetCmn(Ieee8021CnRpgMinDecFac, 12, Ieee8021CnRpgMinDecFacSet, NULL, NULL, 0, 0, 4, "%u %u %i %u %u", u4Ieee8021CnRpgComponentId , u4Ieee8021CnRpgPriority , i4Ieee8021CnRpgIfIndex , u4Ieee8021CnRpgIdentifier ,u4SetValIeee8021CnRpgMinDecFac)
#define nmhSetIeee8021CnRpgMinRate(u4Ieee8021CnRpgComponentId , u4Ieee8021CnRpgPriority , i4Ieee8021CnRpgIfIndex , u4Ieee8021CnRpgIdentifier ,u4SetValIeee8021CnRpgMinRate) \
 nmhSetCmn(Ieee8021CnRpgMinRate, 12, Ieee8021CnRpgMinRateSet, NULL, NULL, 0, 0, 4, "%u %u %i %u %u", u4Ieee8021CnRpgComponentId , u4Ieee8021CnRpgPriority , i4Ieee8021CnRpgIfIndex , u4Ieee8021CnRpgIdentifier ,u4SetValIeee8021CnRpgMinRate)

#endif
