/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: fsdcbxcli.h,v 1.6 2016/07/09 09:41:21 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDcbPfcMinThreshold[11];
extern UINT4 FsDcbPfcMaxThreshold[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDcbPfcMinThreshold(u4SetValFsDcbPfcMinThreshold) \
 nmhSetCmn(FsDcbPfcMinThreshold, 11, FsDcbPfcMinThresholdSet, NULL, NULL, 0, 0, 0, "%u", u4SetValFsDcbPfcMinThreshold)
#define nmhSetFsDcbPfcMaxThreshold(u4SetValFsDcbPfcMaxThreshold) \
 nmhSetCmn(FsDcbPfcMaxThreshold, 11, FsDcbPfcMaxThresholdSet, NULL, NULL, 0, 0, 0, "%u", u4SetValFsDcbPfcMaxThreshold)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDcbPortNumber[13];
extern UINT4 FsDcbRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDcbRowStatus(i4FsDcbPortNumber ,i4SetValFsDcbRowStatus) \
 nmhSetCmn(FsDcbRowStatus, 13, FsDcbRowStatusSet, NULL, NULL, 0, 1, 1, "%i %i", i4FsDcbPortNumber ,i4SetValFsDcbRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDcbxGlobalTraceLevel[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDcbxGlobalTraceLevel(i4SetValFsDcbxGlobalTraceLevel) \
 nmhSetCmn(FsDcbxGlobalTraceLevel, 13, FsDcbxGlobalTraceLevelSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsDcbxGlobalTraceLevel)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDCBXPortNumber[14];
extern UINT4 FsDCBXAdminStatus[14];
extern UINT4 FsDCBXMode[14];
extern UINT4 FsDcbxCEEGlobalEnableTrap[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDCBXAdminStatus(i4FsDCBXPortNumber ,i4SetValFsDCBXAdminStatus) \
 nmhSetCmn(FsDCBXAdminStatus, 14, FsDCBXAdminStatusSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsDCBXPortNumber ,i4SetValFsDCBXAdminStatus)
#define nmhSetFsDCBXMode(i4FsDCBXPortNumber , i4SetValFsDCBXMode) \
 nmhSetCmn(FsDCBXMode, 14, FsDCBXModeSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsDCBXPortNumber, i4SetValFsDCBXMode)
#define  nmhSetFsDcbxCEEGlobalEnableTrap(i4SetValFsDcbxCEEGlobalEnableTrap) \
 nmhSetCmn(FsDcbxCEEGlobalEnableTrap, 13, FsDcbxCEEGlobalEnableTrapSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsDcbxCEEGlobalEnableTrap)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsETSSystemControl[13];
extern UINT4 FsETSModuleStatus[13];
extern UINT4 FsETSClearCounters[13];
extern UINT4 FsETSGlobalEnableTrap[13];
extern UINT4 FsETSGeneratedTrapCount[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsETSSystemControl(i4SetValFsETSSystemControl) \
 nmhSetCmn(FsETSSystemControl, 13, FsETSSystemControlSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsETSSystemControl)
#define nmhSetFsETSModuleStatus(i4SetValFsETSModuleStatus) \
 nmhSetCmn(FsETSModuleStatus, 13, FsETSModuleStatusSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsETSModuleStatus)
#define nmhSetFsETSClearCounters(i4SetValFsETSClearCounters) \
 nmhSetCmn(FsETSClearCounters, 13, FsETSClearCountersSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsETSClearCounters)
#define nmhSetFsETSGlobalEnableTrap(i4SetValFsETSGlobalEnableTrap) \
 nmhSetCmn(FsETSGlobalEnableTrap, 13, FsETSGlobalEnableTrapSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsETSGlobalEnableTrap)
#define nmhSetFsETSGeneratedTrapCount(u4SetValFsETSGeneratedTrapCount) \
 nmhSetCmn(FsETSGeneratedTrapCount, 13, FsETSGeneratedTrapCountSet, NULL, NULL, 0, 0, 0, "%u", u4SetValFsETSGeneratedTrapCount)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsETSPortNumber[14];
extern UINT4 FsETSAdminMode[14];
extern UINT4 FsETSClearTLVCounters[14];
extern UINT4 FsETSRowStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsETSAdminMode(i4FsETSPortNumber ,i4SetValFsETSAdminMode) \
 nmhSetCmn(FsETSAdminMode, 14, FsETSAdminModeSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsETSPortNumber ,i4SetValFsETSAdminMode)
#define nmhSetFsETSClearTLVCounters(i4FsETSPortNumber ,i4SetValFsETSClearTLVCounters) \
 nmhSetCmn(FsETSClearTLVCounters, 14, FsETSClearTLVCountersSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsETSPortNumber ,i4SetValFsETSClearTLVCounters)
#define nmhSetFsETSRowStatus(i4FsETSPortNumber ,i4SetValFsETSRowStatus) \
 nmhSetCmn(FsETSRowStatus, 14, FsETSRowStatusSet, NULL, NULL, 0, 1, 1, "%i %i", i4FsETSPortNumber ,i4SetValFsETSRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPFCSystemControl[13];
extern UINT4 FsPFCModuleStatus[13];
extern UINT4 FsPFCClearCounters[13];
extern UINT4 FsPFCGlobalEnableTrap[13];
extern UINT4 FsPFCGeneratedTrapCount[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPFCSystemControl(i4SetValFsPFCSystemControl) \
 nmhSetCmn(FsPFCSystemControl, 13, FsPFCSystemControlSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsPFCSystemControl)
#define nmhSetFsPFCModuleStatus(i4SetValFsPFCModuleStatus) \
 nmhSetCmn(FsPFCModuleStatus, 13, FsPFCModuleStatusSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsPFCModuleStatus)
#define nmhSetFsPFCClearCounters(i4SetValFsPFCClearCounters) \
 nmhSetCmn(FsPFCClearCounters, 13, FsPFCClearCountersSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsPFCClearCounters)
#define nmhSetFsPFCGlobalEnableTrap(i4SetValFsPFCGlobalEnableTrap) \
 nmhSetCmn(FsPFCGlobalEnableTrap, 13, FsPFCGlobalEnableTrapSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsPFCGlobalEnableTrap)
#define nmhSetFsPFCGeneratedTrapCount(u4SetValFsPFCGeneratedTrapCount) \
 nmhSetCmn(FsPFCGeneratedTrapCount, 13, FsPFCGeneratedTrapCountSet, NULL, NULL, 0, 0, 0, "%u", u4SetValFsPFCGeneratedTrapCount)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPFCPortNumber[14];
extern UINT4 FsPFCAdminMode[14];
extern UINT4 FsPFCClearTLVCounters[14];
extern UINT4 FsPFCRowStatus[14];
extern UINT4 FsPFCClearPauseFrameCounters[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPFCAdminMode(i4FsPFCPortNumber ,i4SetValFsPFCAdminMode) \
 nmhSetCmn(FsPFCAdminMode, 14, FsPFCAdminModeSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsPFCPortNumber ,i4SetValFsPFCAdminMode)
#define nmhSetFsPFCClearTLVCounters(i4FsPFCPortNumber ,i4SetValFsPFCClearTLVCounters) \
 nmhSetCmn(FsPFCClearTLVCounters, 14, FsPFCClearTLVCountersSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsPFCPortNumber ,i4SetValFsPFCClearTLVCounters)
#define nmhSetFsPFCRowStatus(i4FsPFCPortNumber ,i4SetValFsPFCRowStatus) \
 nmhSetCmn(FsPFCRowStatus, 14, FsPFCRowStatusSet, NULL, NULL, 0, 1, 1, "%i %i", i4FsPFCPortNumber ,i4SetValFsPFCRowStatus)
#define nmhSetFsPFCClearPauseFrameCounters(i4FsPFCPortNumber ,i4SetValFsPFCClearPauseFrameCounters) \
 nmhSetCmn(FsPFCClearPauseFrameCounters, 14, FsPFCClearPauseFrameCountersSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsPFCPortNumber ,i4SetValFsPFCClearPauseFrameCounters)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsAppPriSystemControl[13];
extern UINT4 FsAppPriModuleStatus[13];
extern UINT4 FsAppPriClearCounters[13];
extern UINT4 FsAppPriGlobalEnableTrap[13];
extern UINT4 FsAppPriGeneratedTrapCount[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsAppPriSystemControl(i4SetValFsAppPriSystemControl) \
 nmhSetCmn(FsAppPriSystemControl, 13, FsAppPriSystemControlSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsAppPriSystemControl)
#define nmhSetFsAppPriModuleStatus(i4SetValFsAppPriModuleStatus) \
 nmhSetCmn(FsAppPriModuleStatus, 13, FsAppPriModuleStatusSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsAppPriModuleStatus)
#define nmhSetFsAppPriClearCounters(i4SetValFsAppPriClearCounters) \
 nmhSetCmn(FsAppPriClearCounters, 13, FsAppPriClearCountersSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsAppPriClearCounters)
#define nmhSetFsAppPriGlobalEnableTrap(i4SetValFsAppPriGlobalEnableTrap) \
 nmhSetCmn(FsAppPriGlobalEnableTrap, 13, FsAppPriGlobalEnableTrapSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsAppPriGlobalEnableTrap)
#define nmhSetFsAppPriGeneratedTrapCount(u4SetValFsAppPriGeneratedTrapCount) \
 nmhSetCmn(FsAppPriGeneratedTrapCount, 13, FsAppPriGeneratedTrapCountSet, NULL, NULL, 0, 0, 0, "%u", u4SetValFsAppPriGeneratedTrapCount)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsAppPriPortNumber[14];
extern UINT4 FsAppPriAdminMode[14];
extern UINT4 FsAppPriClearTLVCounters[14];
extern UINT4 FsAppPriRowStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsAppPriAdminMode(i4FsAppPriPortNumber ,i4SetValFsAppPriAdminMode) \
 nmhSetCmn(FsAppPriAdminMode, 14, FsAppPriAdminModeSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsAppPriPortNumber ,i4SetValFsAppPriAdminMode)
#define nmhSetFsAppPriClearTLVCounters(i4FsAppPriPortNumber ,i4SetValFsAppPriClearTLVCounters) \
 nmhSetCmn(FsAppPriClearTLVCounters, 14, FsAppPriClearTLVCountersSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsAppPriPortNumber ,i4SetValFsAppPriClearTLVCounters)
#define nmhSetFsAppPriRowStatus(i4FsAppPriPortNumber ,i4SetValFsAppPriRowStatus) \
 nmhSetCmn(FsAppPriRowStatus, 14, FsAppPriRowStatusSet, NULL, NULL, 0, 1, 1, "%i %i", i4FsAppPriPortNumber ,i4SetValFsAppPriRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsAppPriXAppRowStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsAppPriXAppRowStatus(i4LldpV2LocPortIfIndex, i4LldpXdot1dcbxAdminApplicationPriorityAESelector , u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol ,i4SetValFsAppPriXAppRowStatus) \
 nmhSetCmn(FsAppPriXAppRowStatus, 14, FsAppPriXAppRowStatusSet, NULL, NULL, 0, 1, 3, "%i %i %u %i", i4LldpV2LocPortIfIndex , i4LldpXdot1dcbxAdminApplicationPriorityAESelector , u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol ,i4SetValFsAppPriXAppRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FslldpXdot1dcbxAdminApplicationPriorityWilling[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFslldpXdot1dcbxAdminApplicationPriorityWilling(i4LldpV2LocPortIfIndex ,i4SetValFslldpXdot1dcbxAdminApplicationPriorityWilling) \
 nmhSetCmn(FslldpXdot1dcbxAdminApplicationPriorityWilling, 14, FslldpXdot1dcbxAdminApplicationPriorityWillingSet, NULL, NULL, 0, 0, 1, "%i %i", i4LldpV2LocPortIfIndex ,i4SetValFslldpXdot1dcbxAdminApplicationPriorityWilling)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FslldpXdot1dcbxConfigTCSupportedTxEnable[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFslldpXdot1dcbxConfigTCSupportedTxEnable(i4LldpV2PortConfigIfIndex , u4LldpV2PortConfigDestAddressIndex ,i4SetValFslldpXdot1dcbxConfigTCSupportedTxEnable) \
 nmhSetCmn(FslldpXdot1dcbxConfigTCSupportedTxEnable, 14, FslldpXdot1dcbxConfigTCSupportedTxEnableSet, NULL, NULL, 0, 0, 2, "%i %u %i", i4LldpV2PortConfigIfIndex , u4LldpV2PortConfigDestAddressIndex ,i4SetValFslldpXdot1dcbxConfigTCSupportedTxEnable)

#endif
