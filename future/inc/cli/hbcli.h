/*******************************************************************
 * Copyright (C) 2015 Aricent Inc . All Rights Reserved
 *
 * $Id: hbcli.h,v 1.8 2016/06/24 09:44:04 siva Exp $
 *
 * Description: HB CLI dcommands specific defintions
 *
 *******************************************************************/
#ifndef _HBCLI_H
#define _HBCLI_H

#include "cli.h"
#include "hb.h"

#define HB_CLI_MAX_ARGS 3

enum {
    HB_CLI_HB_INT = 1,
    HB_CLI_PDI_MULTIPLIER,
    HB_CLI_TRC_DBG,
    HB_CLI_TRC_NO_DBG,
    HB_CLI_SHOW_HB_INFO,
    HB_CLI_SHOW_DBG_INFO,
    HB_CLI_SHOW_HB_COUNTERS,
    HB_CLI_HB_STATS,
    HB_CLI_CLEAR_STATS
};

enum {
    CLI_HB_INVALID_HB_INTERVAL = 1,
    CLI_HB_INVALID_MULTIPLIER,
    CLI_HB_INVALID_TRACE,
    CLI_HB_MAX_ERR
};
enum {
    HB_CLI_COUNTER_ENABLE = 1,
    HB_CLI_COUNTER_DISABLE
};
#ifdef __HBCLI_C__
CONST CHR1  *HbCliErrString [] = {
    NULL,
    "% Invalid Heartbeat interval\r\n",
    "% Invalid Peer dead interval multiplier\r\n",
    "% Invalid debugging trace level\r\n",
};
#endif

/* Function Prototypes */
INT4  CliProcessHbCmd       PROTO ((tCliHandle CliHandle,
                                    UINT4 u4Command, ...));
UINT4
HbCliSetHbInterval (tCliHandle CliHandle, UINT4 u4HbInt);
UINT4
HbCliSetHbPeerDeadIntMultiplier (tCliHandle CliHandle,
                               UINT4 u4PeerDeadIntMultiplier);
UINT4
HbCliSetHbTrcLevel (tCliHandle CliHandle, UINT4 u4TrcLevel);
UINT4
HbCliShowHbInfo (tCliHandle CliHandle);
UINT4
HbCliHbDebugEnableDisable (tCliHandle CliHandle, UINT4 u4TrcVal, UINT1 u1DbgFlag);
UINT4
HbShowRunningConfig (tCliHandle CliHandle);

UINT4
HbCliHbShowDebugInfo (tCliHandle CliHandle);

UINT4
HbCliStatsEnableDisable (tCliHandle CliHandle, INT4 i4HbStatsEnable);

UINT4
HbCliShowStatsInfo (tCliHandle CliHandle);

UINT4
HbCliHbClearStatistics (tCliHandle CliHandle);
#endif
