/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: vxcli.h,v 1.16 2018/01/05 09:57:08 siva Exp $
*
* Description: This file contains the Vxlan CLI related prototypes,enum and macros
*********************************************************************/

#include "cli.h"

INT4 cli_process_Vxlan_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);
INT4 cli_process_Vxlan_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);

#ifdef VXLAN_TEST_WANTED
#define VXLAN_UT_TEST  1
INT4
cli_process_vxlan_test_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);
#endif

#define VXLAN_LOCK  VxlanMainTaskLock ()

#define VXLAN_UNLOCK  VxlanMainTaskUnLock ()

enum
{
 CLI_VXLAN_FSVXLANENABLE,
 CLI_VXLAN_FSVXLANUDPPORT,
 CLI_VXLAN_FSVXLANTRACEOPTION,
 CLI_VXLAN_FSVXLANNOTIFICATIONCNTL,
 CLI_VXLAN_FSEVPNENABLE,
 CLI_VXLAN_FSVXLANVTEPTABLE,
 CLI_VXLAN_FSVXLANNVETABLE,
 CLI_VXLAN_FSVXLANMCASTTABLE,
 CLI_VXLAN_FSVXLANVNIVLANMAPTABLE,
 CLI_VXLAN_FSVXLANINREPLICATABLE,
 CLI_VXLAN_FSEVPNID,
 CLI_VXLAN_DEL_FSEVPNID,
 CLI_VXLAN_DEL_INGRESS_REPLICA,
 CLI_VXLAN_FSEVPNVNIMAPTABLE,
 CLI_VXLAN_FSEVPNVXLANBGPRTTABLE,
 CLI_VXLAN_FSEVPNVXLANVRFTABLE,
 CLI_VXLAN_FSEVPNVXLANVRFRTTABLE,
 CLI_VXLAN_FSEVPNVXLANMULTIHOMEDPEERTABLE,
 CLI_VXLAN_FSEVPNVXLANVRF,
 CLI_VXLAN_DEL_FSEVPNVXLANVRF,
 CLI_VXLAN_FSEVPNANYCASTGW,
 CLI_VXLAN_NVE_INT_SHOW,
 CLI_VXLAN_NVE_VNI_SHOW,
 CLI_VXLAN_NVE_PEER_SHOW,
 CLI_VXLAN_VNI_STATISTICS_SHOW,
 CLI_VXLAN_VLAN_VNI_SHOW,
 CLI_VXLAN_UDP_PORT_SHOW,
 CLI_VXLAN_PACKET_HEADERS_SHOW,
        CLI_VXLAN_CLEAR_MAC,
        CLI_VXLAN_CLEAR_NVE,
 CLI_VXLAN_EVPN_VNI_SHOW,
 CLI_VXLAN_EVPN_MAC_SHOW,
 CLI_VXLAN_EVI_VNI_STATISTICS_SHOW,
        CLI_VXLAN_EVPN_STATUS_SHOW,
        CLI_VXLAN_EVPN_RDRT_SHOW,
        CLI_VXLAN_EVPN_NEIGHBORS_SHOW,
CLI_VXLAN_VRF_SHOW,
CLI_VXLAN_VRF_STATISTICS_SHOW
};

enum
{
  EVPN_BGP_RT_IMPORT = 1,
  EVPN_BGP_RT_EXPORT,
  EVPN_BGP_RT_BOTH,
  EVPN_BGP_MAX_RT_TYPE
};

enum
{
    CLI_VXLAN_INVALID_ENABLE_INPUT = 1,
    CLI_VXLAN_NOT_ENABLED,
    CLI_VXLAN_INVALID_UDP_PORT,
    CLI_VXLAN_INVALID_TRACE_OPTION,
    CLI_VXLAN_INVALID_NVE_INT_INDEX,
    CLI_VXLAN_INVALID_SRC_ADDR_TYPE,
    CLI_VXLAN_INVALID_SRC_IPV4_ADDR,
    CLI_VXLAN_INVALID_SRC_IPV6_ADDR,
    CLI_VXLAN_INVALID_SRC_ADDR_LENGTH,
    CLI_VXLAN_INVALID_VTEP_ROW_STATUS,
    CLI_VXLAN_NVE_ENTRY_EXISTS,
    CLI_VXLAN_MCAST_ENTRY_EXISTS,
    CLI_VXLAN_INREPLICA_ENTRY_EXISTS,
    CLI_VXLAN_INVALID_VNI,
    CLI_VXLAN_VNI_EXIST_NVE,
    CLI_VXLAN_INVALID_REM_ADDR_TYPE,
    CLI_VXLAN_INVALID_REM_IPV4_ADDR,
    CLI_VXLAN_INVALID_REM_IPV6_ADDR,
    CLI_VXLAN_INVALID_REM_ADDR_LENGTH,
    CLI_VXLAN_INVALID_VM_MAC_ADDR,
    CLI_VXLAN_INVALID_NVE_ROW_STATUS,
    CLI_VXLAN_NO_SRC_VTEP_IP,
    CLI_VXLAN_INVALID_MCAST_ADDR_TYPE,
    CLI_VXLAN_INVALID_MCAST_IPV4_ADDR,
    CLI_VXLAN_INVALID_MCAST_IPV6_ADDR,
    CLI_VXLAN_INVALID_MCAST_ADDR_LENGTH,
    CLI_VXLAN_INVALID_MCAST_ROW_STATUS,
    CLI_VXLAN_NO_VNI_NVE_MAPPING,
    CLI_VXLAN_NO_PKT_SEND,
    CLI_VXLAN_NO_PKT_RECEIVE,
    CLI_VXLAN_NO_PKT_DROP,
    CLI_VXLAN_INVALID_VNIVLAN_ROW_STATUS,
    CLI_VXLAN_MEMORY_FAILURE,
    CLI_VXLAN_VTEP_OBJ_INIT_FAIL,
    CLI_VXLAN_RBTREE_ADD_FAIL,
    CLI_VXLAN_VTEP_ENTRY_NOT_EXIST,
    CLI_VXLAN_VTEP_ROW_EXISTS,
    CLI_VXLAN_NVE_OBJ_INIT_FAIL,
    CLI_VXLAN_NVE_ENTRY_NOT_EXIST,
    CLI_VXLAN_NVE_ROW_EXISTS,
    CLI_VXLAN_MCAST_OBJ_INIT_FAIL,
    CLI_VXLAN_MCAST_ENTRY_NOT_EXIST,
    CLI_VXLAN_MCAST_ROW_EXISTS,
    CLI_VXLAN_VNIVLAN_OBJ_INIT_FAIL,
    CLI_VXLAN_VNIVLAN_ENTRY_NOT_EXIST,
    CLI_VXLAN_VNIVLAN_ROW_EXISTS,
    CLI_VXLAN_VNIVLAN_MAPPED_WITH_OTHER_VLAN,
    CLI_VXLAN_INREPLICA_OBJ_INIT_FAIL,
    CLI_VXLAN_INREPLICA_ENTRY_NOT_EXIST,
    CLI_VXLAN_INREPLICA_ROW_EXISTS,
    CLI_VXLAN_INVALID_INREPLICA_ROW_STATUS,
    CLI_VXLAN_SET_NVE_PORT_FAIL,
    CLI_VXLAN_HW_ADD_FAIL,
    CLI_VXLAN_HW_DEL_FAIL,
    CLI_EVPN_NOT_ENABLED,
    CLI_EVPN_INVALID_CNTL_INPUT,
    CLI_EVPN_NO_PKT_SEND,
    CLI_EVPN_NO_PKT_RECEIVE,
    CLI_EVPN_NO_PKT_DROP,
    CLI_EVPN_INVALID_EVIVNI_ROW_STATUS,
    CLI_EVPN_INVALID_ENABLE_INPUT,
    CLI_EVPN_NO_VNI_MAP_ENTRY,
    CLI_EVPN_NOT_DISABLED,
    CLI_EVPN_INVALID_RT_ROW_STATUS,
    CLI_EVPN_INVALID_RT_TYPE,
    CLI_EVPN_INVALID_RT_INDEX,
    CLI_EVPN_INVALID_RT_LEN,
    CLI_EVPN_INVALID_RT_VALUE,
    CLI_EVPN_INVALID_RD_LEN,
    CLI_EVPN_INVALID_RD_VALUE,
    CLI_EVPN_INVALID_EVI_VNI,
    CLI_VXLAN_VRF_RT_INIT_FAIL,
    CLI_EVPN_RT_DUPLICATE_ERR,
    CLI_EVPN_NO_SUCH_RT,
    CLI_EVPN_WRONG_ARP_VALUE,
    CLI_EVPN_ARP_ALREADY_EXISTS,
    CLI_EVPN_INVALID_LB_INPUT,
    CLI_VXLAN_INVALID_ESI_TYPE,
    CLI_EVPN_MCLAG_NOT_PRESENT,
    CLI_EVPN_VNI_EXISTS,
    CLI_VXLAN_INVALID_ESI_VALUE,
    CLI_VXLAN_INVALID_ESI_MAC_ADDR,
    CLI_VXLAN_VNI_EXIST_VRF,
    CLI_VXLAN_VRF_EXIST_VNI,
    CLI_VXLAN_L2VNI_EXIST,
    CLI_EVPN_ARP_SUP_ENABLED,
    CLI_EVPN_ARP_SUP_MCAST_ENABLED,
    CLI_EVPN_ARP_SUP_ING_REP_ENABLED,
    CLI_VXLAN_L3VNI_EXIST,
    CLI_VXLAN_L3VNI_ARP_SUPPRESSION_ERR,
    CLI_VXLAN__ARP_SUPPRESSION_MAP_L3VNI_ERR,
    CLI_VXLAN_MAX_COUNT_ERROR,
    CLI_VXLAN_MAX_ERR
};

#define VXLAN_CLI_MAX_ARGS   15

/* Accessing the ErrString with ErrCode value.
 * Since the ErrCode's starting Id is changed, this forumla is
 * used to access the Err String */


#if defined  (__VXLANCLIG_C__)
CONST CHR1  *VxlanCliErrString [] = {

 NULL,
    "\r% Invalid input for enabling/disabling VXLAN\r\n",
    "\r% VXLAN is not enabled\r\n",
    "\r% Invalid VXLAN UDP port number\r\n",
    "\r% Invalid VXLAN TRACE OPTION\r\n",
    "\r% Invalid NVE interface index\r\n",
    "\r% Invalid VXLAN VTEP Address Type\r\n",
    "\r% Invalid VXLAN VTEP IPV4 Address\r\n",
    "\r% Invalid VXLAN VTEP IPV6 Address\r\n",
    "\r% Invalid VXLAN VTEP IP Address Length\r\n",
    "\r% Invalid VXLAN VTEP Rowstatus\r\n",
    "\r% NVETable entry exists with this source-IP\r\n",
    "\r% Mcast Table entry exists with this source-IP\r\n",
    "\r% Ingress Replica Table entry exists with this source-IP\r\n",
    "\r% Invalid VXLAN Identifier\r\n",
    "\r% VXLAN Id is associated with another NVE\r\n",
    "\r% Invalid VXLAN Remote VTEP Address Type\r\n",
    "\r% Invalid VXLAN Remote VTEP IPv4 Address\r\n",
    "\r% Invalid VXLAN remote VTEP IPV6 Address\r\n",
    "\r% Invalid VXLAN Remote VTEP IP Address length\r\n",
    "\r% Invalid VXLAN Destination VM MAC Address\r\n",
    "\r% Invalid VXLAN Nve Rowstatus\r\n",
    "\r% Source IP Entry does not exist\r\n",
    "\r% Invalid VXLAN Multicast Address Type\r\n",
    "\r% Invalid VXLAN Multicast IPv4 Address\r\n",
    "\r% Invalid VXLAN Multicast IPV6 Address\r\n",
    "\r% Invalid VXLAN Multicast IPAddress length\r\n",
    "\r% Invalid VXLAN Multicast Rowstatus\r\n",
    "\r% NVE interface is not mapped with VNI\r\n",
    "\r% Packet send count should be updated only when a packet is sent out. So, it can only be cleared using command\r\n",
    "\r% Packet Recieved count should be updated only when a packet is recieved. So, it can only be cleared using command\r\n",
    "\r% Packet dropped count should be updated only when a packet is dropped. So, it can only be cleared using command\r\n",
    "\r% Invalid VXLAN VniVlanMap Rowstatus\r\n",
    "\r% Failed to Allocate Memory\r\n",
    "\r% Failed to intialize the VTEP table object\r\n",
    "\r% Failed to add a node in RB Tree\r\n",
    "\r% VTEP table entry does not exist in the Database\r\n",
    "\r% VTEP table row already exists\r\n",
    "\r% Failed to intialize the NVE table object\r\n",
    "\r% NVE table entry does not exist in the Database\r\n",
    "\r% NVE table row already exists\r\n",
    "\r% Failed to intialize the Multicast table object\r\n",
    "\r% Multicast table entry does not exist in the Database\r\n",
    "\r% Multicast table row already exists\r\n",
    "\r% Failed to intialize the Vni-Vlan table object\r\n",
    "\r% Vni-Vlan table entry does not exist in the Database\r\n",
    "\r% Vni-Vlan table row already exists\r\n",
    "\r% Vni is already mapped with some other VLAN\r\n",
    "\r% Failed to intialize the Ingress Replica table object\r\n",
    "\r% Ingress Replica table entry does not exist in the Database\r\n",
    "\r% Ingress-Replica table row already exists\r\n",
    "\r% Invalid VXLAN Ingress-Replica Rowstatus\r\n",
    "\r% Failed to set the NVE interface as the member port of the VLAN\r\n",
    "\r% Failed to add entry in to HW database\r\n",
    "\r% Failed to delete entry from HW database\r\n",
    "\r% EVPN is not enabled\r\n",
    "\r% Invalid input for enabling/disabling SNMP traps in EVPN\r\n",
    "\r% Packet Send count should be updated only when a packet is sent out. So it can only be cleared using command\r\n",
    "\r% Packet Recieved count should be updated only when a packet is recieved. So it can only be cleared using command\r\n",
    "\r% Packet Dropped count should be updated only when a packet is dropped. So it can only be cleared using command\r\n",
    "\r% Invalid Evi-Vni Map Rowstatus\r\n",
    "\r% Invalid input for enabling/disabling EVPN\r\n",
    "\r% No VNI-Vlan map Entry\r\n",
    "\r% EVPN is enabled over VXLAN. Disable EVPN before disabling VXLAN\r\n",
    "\r% Invalid RT Rowstatus\r\n",
    "\r% Invalid RT Type\r\n",
    "\r% Invalid RT Index \r\n",
    "\r% Invalid RT Length \r\n",
    "\r% Invalid RT Value \r\n",
    "\r% Invalid RD Length \r\n",
    "\r% Invalid RD Value \r\n",
    "\r% Invalid EVI/VNI number \r\n",
    "\r% Failed to intialize the VRF RT table object\r\n",
    "\r% This RT is already configured for the Table\r\n",
    "\r% No such RT is configured for this Table\n",
    "\r% Wrong ARP Suppress value is configured.\n",
    "\r% ARP Entry for this Member VNI id already exists.\n",
    "\r% Invalid input for enabling/disabling LoadBalance \r\n",
    "\r% Entered ESI Type is not supported \r\n",
    "\r% MC-LAG identifier is not present \r\n",
    "\r% Vni associated with another Evi-id \r\n",
    "\r% Invalid ESI Value \r\n",
    "\r% Invalid ESI MAC Address \r\n",
    "\r% VXLAN Id is associated with another VRF\r\n",
    "\r% Vrf is associated with another VNI\r\n",
    "\r% Vni is used as L2VNI\r\n",
    "\r% Arp suppression should be disabled for enabling Multicast/Ingress Replica \r\n",
    "\r% Multicast-VXLAN should disabled for enabling Arp Suppression \r\n",
    "\r% Ingress Replica-VXLAN should be disabled for enabling Arp Suppression \r\n",
    "\r% Vni is used as L3VNI\r\n",
    "\r% L3-VNI cannot be mapped with Suppress-Arp\r\n",
    "\r% VNI Configured with Suppress-Arp cannot be mapped to VRF\r\n",
    "\r% Max count is reached \r\n",
    "\r\n"
};
#else

PUBLIC CHR1* VxlanCliErrString;
#endif


#define   VXLAN_CLI_STATUS_ENABLE        1
#define   VXLAN_CLI_STATUS_DISABLE       2
#define   EVPN_CLI_STATUS_ENABLE        1
#define   EVPN_CLI_STATUS_DISABLE       2
#define   EVPN_CLI_LB_ENABLE          1
#define   EVPN_CLI_LB_DISABLE         2
#define   EVPN_CLI_ARP_SUPPRESS      1
#define   EVPN_CLI_ARP_ALLOW         2
#define   VXLAN_CLI_VNI_CLEAR_COUNTER    0
#define   EVPN_CLI_VNI_CLEAR_COUNTER     0
#define   VXLAN_VRF_CLI_VNI_CLEAR_COUNTER     0
#define   VXLAN_CLI_TRACE_ALL         (0xffffffff)
#define   VXLAN_CLI_TRACE_CRITICAL    (0x00000001)
#define   VXLAN_CLI_TRACE_UTILITIES   (0x00000002)
#define   VXLAN_CLI_TRACE_MGMT        (0x00000004)
#define   VXLAN_CLI_TRACE_ENTRY_EXIT  (0x00000008)
#define   VXLAN_CLI_TRACE_FAILURES    (0x00000010)
#define   VXLAN_CLI_TRACE_MEMORY      (0x00000020)
#define   VXLAN_CLI_TRACE_PKT         (0x00000040)
#define   VXLAN_CLI_TRACE_EVPN        (0x00000080)
#define   VXLAN_CLI_VLAN_TAGGED         1
#define   VXLAN_CLI_VLAN_UNTAGGED       0


#define  VXLAN_CLI_ADDRTYPE_IPV4      1
#define  VXLAN_CLI_ADDRTYPE_IPV6      2

#define  VXLAN_CLI_IP4_ADDR_LEN    4
#define  VXLAN_CLI_IP6_ADDR_LEN    16
#define  VXLAN_CLI_MAX_IP_ADD_LEN  16

#define  VXLAN_STRG_TYPE_NON_VOL 1
#define  VXLAN_STRG_TYPE_VOL     2

#define VXLAN_CLI_BRIEF   1
#define VXLAN_CLI_DETAIL  2
#define VXLAN_CLI_STAT    3

#define  VXLAN_CLI_REPLICA_LIST 10
#define  VXLAN_CLI_NVE_PEERS_COUNT 1

/* CLI Prompt Macros */
#define VXLAN_CLI_MAX_PROMPT_LENGTH  32
#define VXLAN_CLI_EVPN_MODE          "(config-evpn)#"
#define VXLAN_CLI_L2VNI_MODE         "(config-evpn-vni)#"
#define VXLAN_CLI_VRF_MODE           "(config-vrf-vxlan)#"
#define VXLAN_CLI_VRF_VNI_MODE       "(config-vrf-vxlan-l3vni)#"
#define VXLAN_CLI_CONFIG_MODE        "(config)#"
#define VXLAN_CLI_L2VNI_PROMPT       "vni)#"
#define VXLAN_CLI_EVPN_PROMPT        "evpn)#"
#define VXLAN_CLI_VRF_PROMPT         "vxlan)#"
#define VXLAN_CLI_L3VNI_PROMPT       "l3vni)#"

#define VXLAN_CLI_L2VNI_RD          1
#define VXLAN_CLI_VRF_VNI_RD        2

#define VXLAN_CLI_L2VNI_RT          1
#define VXLAN_CLI_VRF_VNI_RT        2

#define CLI_GET_EVI_ID()             CliGetContextIdInfo()
#define CLI_SET_EVI_ID(Id)           CliSetContextIdInfo(Id)

#define CLI_GET_MAP_EVI_VNI_ID()         CliGetModeInfo()
#define CLI_SET_MAP_EVI_VNI_ID(i4Idx)    CliSetModeInfo(i4Idx)

#define CLI_GET_VRF_ID()             CliGetContextIdInfo()
#define CLI_SET_VRF_ID(Id)           CliSetContextIdInfo(Id)

#define CLI_GET_VRF_VNI_ID()             CliGetModeInfo()
#define CLI_SET_VRF_VNI_ID(Id)           CliSetModeInfo(Id)


#define VXLAN_CLI_MAX_VRF_NAME_LEN 36

#define VXLAN_CLI_EVPN_MAC_ALL   1
#define VXLAN_CLI_EVPN_BGP       2
#define VXLAN_CLI_EVPN_STATIC    3

#define VXLAN_CLI_PEER_EVPN      1
#define VXLAN_CLI_PEER_STATIC    2

#define CLI_EVPN_BGP_MAX_RD_LEN  8
#define CLI_EVPN_BGP_MAX_RT_LEN  8
#define CLI_EVPN_BGP_RD_SUBTYPE 0
#define CLI_EVPN_BGP_RT_SUBTYPE 2
#define CLI_EVPN_MAX_ESI_LEN    10
#define CLI_EVPN_MAX_ESI_STR_LEN 30

#define CLI_GET_EVPN_VNI_BGP_MODE()            CliGetModeInfo()
#define CLI_SET_EVPN_VNI_BGP_MODE(u4ContextId) CliSetModeInfo(u4ContextId)

PUBLIC INT1
VxlanEvpnVniMapPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1PromptStr));

PUBLIC INT1
VxlanEvpnL2VniPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1PromptStr));

PUBLIC INT4
VxlanCliSetFsEviEntry PROTO ((tCliHandle CliHandle, UINT4 u4FsEvpnIdx));

PUBLIC INT1
VxlanEvpnVrfPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1PromptStr));

PUBLIC INT1
VxlanEvpnVrfVniPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1PromptStr));

INT4
EvpnBgpCliVrfChangeMode (UINT4 u4VcId);

INT4
EvpnCliGetCurPrompt (UINT1 *pu1Prompt);

INT4
EvpnBgpCliVrfVniChangeMode (UINT4 u4VniIdx);

INT4
EvpnCliEviVniChangeMode (UINT4 u4VniIdx);

INT4
EvpnCliFindNextFreeRtIndex (UINT4 u4ContextId, UINT4 *pu4FreeIndex, INT4 i4RTType);

INT4
EvpnCliParseAndGenerateRdRt (UINT1 *pu1RandomString, UINT1 *pu1RdRt);

INT4
EvpnCliParseAndGenerateESI (UINT1 *pu1RandomString, UINT1 *pu1ESIVal);

INT4
EvpnCliGetRdFromVniMapTable (UINT4 *pu4EviIdx, UINT4 *pu4VniIdx, UINT1 *pu1RdRt, INT4 *pi4BgpRdAuto);

INT4
EvpnCliGetEviFromVniMapTable (UINT4 *pu4VniIdx, INT4 *pi4EviIdx);

INT4
EvpnCliGetESIFromVniMapTable (UINT4 *pu4EviIdx, UINT4 *pu4VniIdx,
                              UINT1 *pu1ESIValue);
INT4
EvpnCliGetRtTypeRtIndex (INT4 i4EviIndex, UINT4 u4VniIndex,
                         UINT1 *pu1RtValue, UINT4 *pu4RtIndex,
                         INT4 *pi4RtType);
INT4
EvpnCliValidateNveTable (UINT4 u4NveIndex, UINT4 u4VniIndex);

INT4
EvpnCliGetRtTableFromEviVni (tCliHandle CliHandle, INT4 i4EviIndex, UINT4 u4VniIndex, UINT4 *pu4RtIndex,
                             INT4 *pi4RtType);
INT4
EvpnBgpCliGetDefaultRdValue (UINT4 u4EviIndex, UINT4 u4VniIndex,
                             UINT1 *pu1RouteDistinguisher);
INT4
EvpnBgpCliGetESIValue (UINT4 u4EviIndex, UINT4 u4VniIndex,
                       UINT1 *pu1ESIValue);
INT4
EvpnBgpCliGetDefaultRtValue (UINT4 u4EviIndex, UINT4 u4VniIndex,
                             UINT4 u4RtIndex, INT4 i4RtType,
                             UINT1 *pu1RouteTarget);
INT4
VxlanCliGetReplicaIndex (INT4 i4FsVxlanNveIfIndex,
                        UINT4 u4FsVxlanNveVniNumber,UINT4 *pu4FsVxlanRemoteVtepAddressLen,
                          UINT1 *pu1FsVxlanRemoteVtepAddress,INT4 *pi4FsVxlanReplicaIndex);
INT4
VxlanCliFindNextFreeReplicaIndex (UINT4 u4FsVxlanNveVniNumber , UINT4 *pu4FreeIndex);

INT4
EvpnCliGetRdFromVrfTable (UINT1 *pu1VxlanVrfName, UINT4 u4VniIndex, UINT1 *pu1RdRt, INT4 *pi4BgpRdAuto);

INT4
EvpnCliGetVrfRtTableFromVrf (tCliHandle CliHandle, UINT1 * pu1VxlanVrfName, UINT4 u4VniIndex, UINT4 *pu4RtIndex,
                             INT4 *pi4RtType);
INT4
EvpnCliGetVrfRtTypeRtIndex (UINT1 * pu1VxlanVrfName, UINT4 u4VniIndex,
                         UINT1 *pu1RtValue, UINT4 *pu4RtIndex,
                         INT4 *pi4RtType);

INT4
EvpnVrfCliGetDefaultRdValue (UINT1 *pu1VxlanVrfName, UINT4 u4VniId,
                             UINT1 *pu1RouteDistinguisher);

INT4
EvpnVrfCliGetDefaultRtValue (UINT1 *pu1VxlanVrfName, UINT4 u4VniId,
                             UINT4 u4RtIndex, INT4 i4RtType,
                             UINT1 *pu1RouteTarget);
INT4
EvpnCliGetVrfNameFromVniMapTable (UINT4 *pu4VniIdx, UINT1 *pu1VrfName);

PUBLIC INT4
VxlanCliSetFsVrfEntry PROTO ((tCliHandle CliHandle, UINT1 *pu1EvpnVrfName));

INT4 
VxlanVniIsL3Vni ( UINT4 u4VniId);

INT4
EvpnCliCheckRdInVniMapTable(UINT4 *pu4EviIdx, UINT4 *pu4VniIdx,
                            UINT1 *pu1RdRt, INT4 *pi4BgpRdAuto);
INT4
EvpnCliCheckRdIniVrfVniMapTable(UINT1 *pu1VrfName, UINT4 *pu4VniIdx,
                                UINT1 *pu1RdRt, INT4 *pi4BgpRdAuto);


