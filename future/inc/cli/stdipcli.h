/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdipcli.h,v 1.4 2008/12/30 05:19:10 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IpForwarding[8];
extern UINT4 IpDefaultTTL[8];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#ifdef IP_WANTED
#define nmhSetIpForwarding(i4SetValIpForwarding)	\
	nmhSetCmnWithLock(IpForwarding, 8, IpForwardingSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 0, "%i", i4SetValIpForwarding)
#define nmhSetIpDefaultTTL(i4SetValIpDefaultTTL)	\
	nmhSetCmnWithLock(IpDefaultTTL, 8, IpDefaultTTLSet, IpFwdProtocolLock, IpFwdProtocolUnlock, 0, 0, 0, "%i", i4SetValIpDefaultTTL)
#elif LNXIP4_WANTED
#define nmhSetIpForwarding(i4SetValIpForwarding)	\
	nmhSetCmnWithLock(IpForwarding, 8, IpForwardingSet, NULL, NULL, 0, 0, 0, "%i", i4SetValIpForwarding)
#define nmhSetIpDefaultTTL(i4SetValIpDefaultTTL)	\
	nmhSetCmnWithLock(IpDefaultTTL, 8, IpDefaultTTLSet, NULL, NULL, 0, 0, 0, "%i", i4SetValIpDefaultTTL)
#endif

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IpNetToMediaPhysAddress[10];
extern UINT4 IpNetToMediaType[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIpNetToMediaPhysAddress(i4IpNetToMediaIfIndex , u4IpNetToMediaNetAddress ,pSetValIpNetToMediaPhysAddress)	\
	nmhSetCmnWithLock(IpNetToMediaPhysAddress, 10, IpNetToMediaPhysAddressSet, ArpProtocolLock, ArpProtocolUnLock, 0, 0, 2, "%i %p %s", i4IpNetToMediaIfIndex , u4IpNetToMediaNetAddress ,pSetValIpNetToMediaPhysAddress)
#define nmhSetIpNetToMediaType(i4IpNetToMediaIfIndex , u4IpNetToMediaNetAddress ,i4SetValIpNetToMediaType)	\
	nmhSetCmnWithLock(IpNetToMediaType, 10, IpNetToMediaTypeSet, ArpProtocolLock, ArpProtocolUnLock, 0, 0, 2, "%i %p %i", i4IpNetToMediaIfIndex , u4IpNetToMediaNetAddress ,i4SetValIpNetToMediaType)
#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IpCidrRouteIfIndex[11];
extern UINT4 IpCidrRouteType[11];
extern UINT4 IpCidrRouteInfo[11];
extern UINT4 IpCidrRouteNextHopAS[11];
extern UINT4 IpCidrRouteMetric1[11];
extern UINT4 IpCidrRouteMetric2[11];
extern UINT4 IpCidrRouteMetric3[11];
extern UINT4 IpCidrRouteMetric4[11];
extern UINT4 IpCidrRouteMetric5[11];
extern UINT4 IpCidrRouteStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIpCidrRouteIfIndex(u4IpCidrRouteDest , u4IpCidrRouteMask , i4IpCidrRouteTos , u4IpCidrRouteNextHop ,i4SetValIpCidrRouteIfIndex)	\
	nmhSetCmnWithLock(IpCidrRouteIfIndex, 11, IpCidrRouteIfIndexSet, RtmProtocolLock, RtmProtocolUnlock, 0, 0, 4, "%p %p %i %p %i", u4IpCidrRouteDest , u4IpCidrRouteMask , i4IpCidrRouteTos , u4IpCidrRouteNextHop ,i4SetValIpCidrRouteIfIndex)
#define nmhSetIpCidrRouteType(u4IpCidrRouteDest , u4IpCidrRouteMask , i4IpCidrRouteTos , u4IpCidrRouteNextHop ,i4SetValIpCidrRouteType)	\
	nmhSetCmnWithLock(IpCidrRouteType, 11, IpCidrRouteTypeSet, RtmProtocolLock, RtmProtocolUnlock, 0, 0, 4, "%p %p %i %p %i", u4IpCidrRouteDest , u4IpCidrRouteMask , i4IpCidrRouteTos , u4IpCidrRouteNextHop ,i4SetValIpCidrRouteType)
#define nmhSetIpCidrRouteInfo(u4IpCidrRouteDest , u4IpCidrRouteMask , i4IpCidrRouteTos , u4IpCidrRouteNextHop ,pSetValIpCidrRouteInfo)	\
	nmhSetCmnWithLock(IpCidrRouteInfo, 11, IpCidrRouteInfoSet, RtmProtocolLock, RtmProtocolUnlock, 0, 0, 4, "%p %p %i %p %o", u4IpCidrRouteDest , u4IpCidrRouteMask , i4IpCidrRouteTos , u4IpCidrRouteNextHop ,pSetValIpCidrRouteInfo)
#define nmhSetIpCidrRouteNextHopAS(u4IpCidrRouteDest , u4IpCidrRouteMask , i4IpCidrRouteTos , u4IpCidrRouteNextHop ,i4SetValIpCidrRouteNextHopAS)	\
	nmhSetCmnWithLock(IpCidrRouteNextHopAS, 11, IpCidrRouteNextHopASSet, RtmProtocolLock, RtmProtocolUnlock, 0, 0, 4, "%p %p %i %p %i", u4IpCidrRouteDest , u4IpCidrRouteMask , i4IpCidrRouteTos , u4IpCidrRouteNextHop ,i4SetValIpCidrRouteNextHopAS)
#define nmhSetIpCidrRouteMetric1(u4IpCidrRouteDest , u4IpCidrRouteMask , i4IpCidrRouteTos , u4IpCidrRouteNextHop ,i4SetValIpCidrRouteMetric1)	\
	nmhSetCmnWithLock(IpCidrRouteMetric1, 11, IpCidrRouteMetric1Set, RtmProtocolLock, RtmProtocolUnlock, 0, 0, 4, "%p %p %i %p %i", u4IpCidrRouteDest , u4IpCidrRouteMask , i4IpCidrRouteTos , u4IpCidrRouteNextHop ,i4SetValIpCidrRouteMetric1)
#define nmhSetIpCidrRouteMetric2(u4IpCidrRouteDest , u4IpCidrRouteMask , i4IpCidrRouteTos , u4IpCidrRouteNextHop ,i4SetValIpCidrRouteMetric2)	\
	nmhSetCmnWithLock(IpCidrRouteMetric2, 11, IpCidrRouteMetric2Set, RtmProtocolLock, RtmProtocolUnlock, 0, 0, 4, "%p %p %i %p %i", u4IpCidrRouteDest , u4IpCidrRouteMask , i4IpCidrRouteTos , u4IpCidrRouteNextHop ,i4SetValIpCidrRouteMetric2)
#define nmhSetIpCidrRouteMetric3(u4IpCidrRouteDest , u4IpCidrRouteMask , i4IpCidrRouteTos , u4IpCidrRouteNextHop ,i4SetValIpCidrRouteMetric3)	\
	nmhSetCmnWithLock(IpCidrRouteMetric3, 11, IpCidrRouteMetric3Set, RtmProtocolLock, RtmProtocolUnlock, 0, 0, 4, "%p %p %i %p %i", u4IpCidrRouteDest , u4IpCidrRouteMask , i4IpCidrRouteTos , u4IpCidrRouteNextHop ,i4SetValIpCidrRouteMetric3)
#define nmhSetIpCidrRouteMetric4(u4IpCidrRouteDest , u4IpCidrRouteMask , i4IpCidrRouteTos , u4IpCidrRouteNextHop ,i4SetValIpCidrRouteMetric4)	\
	nmhSetCmnWithLock(IpCidrRouteMetric4, 11, IpCidrRouteMetric4Set, RtmProtocolLock, RtmProtocolUnlock, 0, 0, 4, "%p %p %i %p %i", u4IpCidrRouteDest , u4IpCidrRouteMask , i4IpCidrRouteTos , u4IpCidrRouteNextHop ,i4SetValIpCidrRouteMetric4)
#define nmhSetIpCidrRouteMetric5(u4IpCidrRouteDest , u4IpCidrRouteMask , i4IpCidrRouteTos , u4IpCidrRouteNextHop ,i4SetValIpCidrRouteMetric5)	\
	nmhSetCmnWithLock(IpCidrRouteMetric5, 11, IpCidrRouteMetric5Set, RtmProtocolLock, RtmProtocolUnlock, 0, 0, 4, "%p %p %i %p %i", u4IpCidrRouteDest , u4IpCidrRouteMask , i4IpCidrRouteTos , u4IpCidrRouteNextHop ,i4SetValIpCidrRouteMetric5)
#define nmhSetIpCidrRouteStatus(u4IpCidrRouteDest , u4IpCidrRouteMask , i4IpCidrRouteTos , u4IpCidrRouteNextHop ,i4SetValIpCidrRouteStatus)	\
	nmhSetCmnWithLock(IpCidrRouteStatus, 11, IpCidrRouteStatusSet, RtmProtocolLock, RtmProtocolUnlock, 0, 1, 4, "%p %p %i %p %i", u4IpCidrRouteDest , u4IpCidrRouteMask , i4IpCidrRouteTos , u4IpCidrRouteNextHop ,i4SetValIpCidrRouteStatus)
#endif
