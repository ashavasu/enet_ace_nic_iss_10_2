/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fspimcli.h,v 1.3 2014/08/23 12:01:35 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPimSPTGroupThreshold[11];
extern UINT4 FsPimSPTSourceThreshold[11];
extern UINT4 FsPimSPTSwitchingPeriod[11];
extern UINT4 FsPimSPTRpThreshold[11];
extern UINT4 FsPimSPTRpSwitchingPeriod[11];
extern UINT4 FsPimRegStopRateLimitingPeriod[11];
extern UINT4 FsPimGlobalTrace[11];
extern UINT4 FsPimGlobalDebug[11];
extern UINT4 FsPimPmbrStatus[11];
extern UINT4 FsPimRouterMode[11];
extern UINT4 FsPimStaticRpEnabled[11];
extern UINT4 FsPimStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPimSPTGroupThreshold(i4SetValFsPimSPTGroupThreshold) \
 nmhSetCmn(FsPimSPTGroupThreshold, 11, FsPimSPTGroupThresholdSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsPimSPTGroupThreshold)
#define nmhSetFsPimSPTSourceThreshold(i4SetValFsPimSPTSourceThreshold) \
 nmhSetCmn(FsPimSPTSourceThreshold, 11, FsPimSPTSourceThresholdSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsPimSPTSourceThreshold)
#define nmhSetFsPimSPTSwitchingPeriod(i4SetValFsPimSPTSwitchingPeriod) \
 nmhSetCmn(FsPimSPTSwitchingPeriod, 11, FsPimSPTSwitchingPeriodSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsPimSPTSwitchingPeriod)
#define nmhSetFsPimSPTRpThreshold(i4SetValFsPimSPTRpThreshold) \
 nmhSetCmn(FsPimSPTRpThreshold, 11, FsPimSPTRpThresholdSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsPimSPTRpThreshold)
#define nmhSetFsPimSPTRpSwitchingPeriod(i4SetValFsPimSPTRpSwitchingPeriod) \
 nmhSetCmn(FsPimSPTRpSwitchingPeriod, 11, FsPimSPTRpSwitchingPeriodSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsPimSPTRpSwitchingPeriod)
#define nmhSetFsPimRegStopRateLimitingPeriod(i4SetValFsPimRegStopRateLimitingPeriod) \
 nmhSetCmn(FsPimRegStopRateLimitingPeriod, 11, FsPimRegStopRateLimitingPeriodSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsPimRegStopRateLimitingPeriod)
#define nmhSetFsPimGlobalTrace(i4SetValFsPimGlobalTrace) \
 nmhSetCmn(FsPimGlobalTrace, 11, FsPimGlobalTraceSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsPimGlobalTrace)
#define nmhSetFsPimGlobalDebug(i4SetValFsPimGlobalDebug) \
 nmhSetCmn(FsPimGlobalDebug, 11, FsPimGlobalDebugSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsPimGlobalDebug)
#define nmhSetFsPimPmbrStatus(i4SetValFsPimPmbrStatus) \
 nmhSetCmn(FsPimPmbrStatus, 11, FsPimPmbrStatusSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsPimPmbrStatus)
#define nmhSetFsPimRouterMode(i4SetValFsPimRouterMode) \
 nmhSetCmn(FsPimRouterMode, 11, FsPimRouterModeSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsPimRouterMode)
#define nmhSetFsPimStaticRpEnabled(i4SetValFsPimStaticRpEnabled) \
 nmhSetCmn(FsPimStaticRpEnabled, 11, FsPimStaticRpEnabledSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsPimStaticRpEnabled)
#define nmhSetFsPimStatus(i4SetValFsPimStatus) \
 nmhSetCmn(FsPimStatus, 11, FsPimStatusSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsPimStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPimInterfaceIfIndex[13];
extern UINT4 FsPimInterfaceCompId[13];
extern UINT4 FsPimInterfaceDRPriority[13];
extern UINT4 FsPimInterfaceHelloHoldTime[13];
extern UINT4 FsPimInterfaceLanPruneDelayPresent[13];
extern UINT4 FsPimInterfaceLanDelay[13];
extern UINT4 FsPimInterfaceOverrideInterval[13];
extern UINT4 FsPimInterfaceAdminStatus[13];
extern UINT4 FsPimInterfaceBorderBit[13];
extern UINT4 FsPimInterfaceExtBorderBit[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPimInterfaceCompId(i4FsPimInterfaceIfIndex ,i4SetValFsPimInterfaceCompId) \
 nmhSetCmn(FsPimInterfaceCompId, 13, FsPimInterfaceCompIdSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsPimInterfaceIfIndex ,i4SetValFsPimInterfaceCompId)
#define nmhSetFsPimInterfaceDRPriority(i4FsPimInterfaceIfIndex ,i4SetValFsPimInterfaceDRPriority) \
 nmhSetCmn(FsPimInterfaceDRPriority, 13, FsPimInterfaceDRPrioritySet, NULL, NULL, 0, 0, 1, "%i %i", i4FsPimInterfaceIfIndex ,i4SetValFsPimInterfaceDRPriority)
#define nmhSetFsPimInterfaceHelloHoldTime(i4FsPimInterfaceIfIndex ,i4SetValFsPimInterfaceHelloHoldTime) \
 nmhSetCmn(FsPimInterfaceHelloHoldTime, 13, FsPimInterfaceHelloHoldTimeSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsPimInterfaceIfIndex ,i4SetValFsPimInterfaceHelloHoldTime)
#define nmhSetFsPimInterfaceLanPruneDelayPresent(i4FsPimInterfaceIfIndex ,i4SetValFsPimInterfaceLanPruneDelayPresent) \
 nmhSetCmn(FsPimInterfaceLanPruneDelayPresent, 13, FsPimInterfaceLanPruneDelayPresentSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsPimInterfaceIfIndex ,i4SetValFsPimInterfaceLanPruneDelayPresent)
#define nmhSetFsPimInterfaceLanDelay(i4FsPimInterfaceIfIndex ,i4SetValFsPimInterfaceLanDelay) \
 nmhSetCmn(FsPimInterfaceLanDelay, 13, FsPimInterfaceLanDelaySet, NULL, NULL, 0, 0, 1, "%i %i", i4FsPimInterfaceIfIndex ,i4SetValFsPimInterfaceLanDelay)
#define nmhSetFsPimInterfaceOverrideInterval(i4FsPimInterfaceIfIndex ,i4SetValFsPimInterfaceOverrideInterval) \
 nmhSetCmn(FsPimInterfaceOverrideInterval, 13, FsPimInterfaceOverrideIntervalSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsPimInterfaceIfIndex ,i4SetValFsPimInterfaceOverrideInterval)
#define nmhSetFsPimInterfaceAdminStatus(i4FsPimInterfaceIfIndex ,i4SetValFsPimInterfaceAdminStatus) \
 nmhSetCmn(FsPimInterfaceAdminStatus, 13, FsPimInterfaceAdminStatusSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsPimInterfaceIfIndex ,i4SetValFsPimInterfaceAdminStatus)
#define nmhSetFsPimInterfaceBorderBit(i4FsPimInterfaceIfIndex ,i4SetValFsPimInterfaceBorderBit) \
 nmhSetCmn(FsPimInterfaceBorderBit, 13, FsPimInterfaceBorderBitSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsPimInterfaceIfIndex ,i4SetValFsPimInterfaceBorderBit)
#define nmhSetFsPimInterfaceExtBorderBit(i4FsPimInterfaceIfIndex ,i4SetValFsPimInterfaceExtBorderBit)   \
    nmhSetCmn(FsPimInterfaceExtBorderBit, 13, FsPimInterfaceExtBorderBitSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsPimInterfaceIfIndex ,i4SetValFsPimInterfaceExtBorderBit)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPimCandidateRPCompId[13];
extern UINT4 FsPimCandidateRPGroupAddress[13];
extern UINT4 FsPimCandidateRPGroupMask[13];
extern UINT4 FsPimCandidateRPAddress[13];
extern UINT4 FsPimCandidateRPPriority[13];
extern UINT4 FsPimCandidateRPRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPimCandidateRPPriority(i4FsPimCandidateRPCompId , u4FsPimCandidateRPGroupAddress , u4FsPimCandidateRPGroupMask , u4FsPimCandidateRPAddress ,i4SetValFsPimCandidateRPPriority) \
 nmhSetCmn(FsPimCandidateRPPriority, 13, FsPimCandidateRPPrioritySet, NULL, NULL, 0, 0, 4, "%i %p %p %p %i", i4FsPimCandidateRPCompId , u4FsPimCandidateRPGroupAddress , u4FsPimCandidateRPGroupMask , u4FsPimCandidateRPAddress ,i4SetValFsPimCandidateRPPriority)
#define nmhSetFsPimCandidateRPRowStatus(i4FsPimCandidateRPCompId , u4FsPimCandidateRPGroupAddress , u4FsPimCandidateRPGroupMask , u4FsPimCandidateRPAddress ,i4SetValFsPimCandidateRPRowStatus) \
 nmhSetCmn(FsPimCandidateRPRowStatus, 13, FsPimCandidateRPRowStatusSet, NULL, NULL, 0, 1, 4, "%i %p %p %p %i", i4FsPimCandidateRPCompId , u4FsPimCandidateRPGroupAddress , u4FsPimCandidateRPGroupMask , u4FsPimCandidateRPAddress ,i4SetValFsPimCandidateRPRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPimStaticRPSetCompId[13];
extern UINT4 FsPimStaticRPSetGroupAddress[13];
extern UINT4 FsPimStaticRPSetGroupMask[13];
extern UINT4 FsPimStaticRPAddress[13];
extern UINT4 FsPimStaticRPRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPimStaticRPAddress(i4FsPimStaticRPSetCompId , u4FsPimStaticRPSetGroupAddress , u4FsPimStaticRPSetGroupMask ,u4SetValFsPimStaticRPAddress) \
 nmhSetCmn(FsPimStaticRPAddress, 13, FsPimStaticRPAddressSet, NULL, NULL, 0, 0, 3, "%i %p %p %p", i4FsPimStaticRPSetCompId , u4FsPimStaticRPSetGroupAddress , u4FsPimStaticRPSetGroupMask ,u4SetValFsPimStaticRPAddress)
#define nmhSetFsPimStaticRPRowStatus(i4FsPimStaticRPSetCompId , u4FsPimStaticRPSetGroupAddress , u4FsPimStaticRPSetGroupMask ,i4SetValFsPimStaticRPRowStatus) \
 nmhSetCmn(FsPimStaticRPRowStatus, 13, FsPimStaticRPRowStatusSet, NULL, NULL, 0, 1, 3, "%i %p %p %i", i4FsPimStaticRPSetCompId , u4FsPimStaticRPSetGroupAddress , u4FsPimStaticRPSetGroupMask ,i4SetValFsPimStaticRPRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPimComponentId[13];
extern UINT4 FsPimComponentMode[13];
extern UINT4 FsPimCompGraftRetryCount[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPimComponentMode(i4FsPimComponentId ,i4SetValFsPimComponentMode) \
 nmhSetCmn(FsPimComponentMode, 13, FsPimComponentModeSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsPimComponentId ,i4SetValFsPimComponentMode)
#define nmhSetFsPimCompGraftRetryCount(i4FsPimComponentId ,i4SetValFsPimCompGraftRetryCount) \
 nmhSetCmn(FsPimCompGraftRetryCount, 13, FsPimCompGraftRetryCountSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsPimComponentId ,i4SetValFsPimCompGraftRetryCount)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPimRegChkSumTblCompId[13];
extern UINT4 FsPimRegChkSumTblRPAddress[13];
extern UINT4 FsPimRPChkSumStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPimRPChkSumStatus(i4FsPimRegChkSumTblCompId , u4FsPimRegChkSumTblRPAddress ,i4SetValFsPimRPChkSumStatus) \
 nmhSetCmn(FsPimRPChkSumStatus, 13, FsPimRPChkSumStatusSet, NULL, NULL, 0, 0, 2, "%i %p %i", i4FsPimRegChkSumTblCompId , u4FsPimRegChkSumTblRPAddress ,i4SetValFsPimRPChkSumStatus)

#endif
