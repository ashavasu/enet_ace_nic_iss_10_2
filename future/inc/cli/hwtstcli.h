#ifndef _HWTESTCLI_H
#define _HWTESTCLI_H

#include "midconst.h"
#include "cli.h"

/* COMMANDS UNDER HWTEST MODE */
#define HWTEST_CLI_CLEAR        1
#define HWTEST_CLI_NAMES        2
#define HWTEST_CLI_EXECUTE      3
#define HWTEST_CLI_REPORT       4

/* SIZE CONSTANTS  */
/* Maximum no. of HWTEST CLI commands. If any new command is added to the */
/* list, please change HWTEST_CLI_MAX_COMMANDS also. */
#define HWTEST_CLI_MAX_COMMANDS      4
#define HWTEST_TESTLIST_MAX          16
#define HWTEST_TESTNAME_LEN          33
#define HWTEST_CLI_MAX_CONSOLE  (23 * 81)

VOID cli_process_hwtest_cmd(tCliHandle CliHandle,UINT4, ...);
#endif	 /* _HWTESTCLI_H */  
