 /********************************************************************
  * Copyright (C) Aricent Inc . All Rights Reserved
  *
  * $Id: pbbcli.h,v 1.17 2013/12/16 15:36:17 siva Exp $
  *
  * Description: This file contains enum values  and data structures 
  * defined for PBB module.
  **********************************************************************/

#ifndef __PBBCLI_H__
#define __PBBCLI_H__

#include "cli.h"

enum{
   CLI_PBB_ISID_E_LAN = 1,
   CLI_PBB_ISID_E_LINE = 2
};
enum{
    CLI_PBB_SHUT = 1,
    CLI_PBB_NO_SHUT = 2,
    CLI_PBB_GLB_OUI = 3,
    CLI_PBB_GLB_NO_OUI = 4,
    CLI_PBB_OUI = 5,
    CLI_PBB_NO_OUI = 6,
    CLI_PBB_ISID_MODE = 7,
    CLI_PBB_ENABLED = 8,
    CLI_PBB_DISABLED = 9,
    CLI_PBB_SVID = 10,
    CLI_PBB_CVID = 11,
    CLI_PBB_DEF_VID = 12,
    CLI_PBB_DEF_SVID = 13,
    CLI_PBB_DEF_CVID = 14,
    CLI_PBB_NO_DEF_VID = 15,
    CLI_PBB_PISID = 16,
    CLI_PBB_NO_PISID = 17,
    CLI_PBB_SET_PARAMETERS = 18,
    CLI_PBB_ISID_ACTIVE = 19,
    CLI_PBB_ISID_NO_ACTIVE = 20,
    CLI_PBB_ICOMPONENT = 21,
    CLI_PBB_DESTINATION_MAC = 22,
    CLI_PBB_NO_DESTINATION_MAC = 23,
    CLI_PBB_SET_SIZING_PARAMS = 24,
    CLI_PBB_ISID_PORTS = 25,
    CLI_PBB_ISID_BVLAN_PORT = 26,
    CLI_PBB_TRANSLATE_ISID = 27,
    CLI_PBB_ISID_MAP = 28,
    CLI_PBB_NO_TRANSLATE_ISID = 29,
    CLI_PBB_ISID_NO_BVLAN_PORT = 30,
    CLI_PBB_SHOW_OUI = 31,
    CLI_PBB_SHOW_SIZING_PARAMS = 32,
    CLI_PBB_SHOW_PISID = 33,
    CLI_PBB_SHOW_ISID = 34,
    CLI_PBB_NO_ISID = 35,
    PBB_CLI_DEBUG = 36,
    PBB_CLI_DEBUG_SHOW = 37,
    CLI_PBB_PORT_PCP_DECODE = 38,
    CLI_PBB_PORT_PCP_ENCODE = 39,
    CLI_PBB_PORT_NO_PCP_DECODE = 40,
    CLI_PBB_PORT_NO_PCP_ENCODE = 41,
    CLI_PBB_PORT_REQ_DROP_ENC = 42,
    CLI_PBB_PORT_PCP_SEL_ROW = 43,
    CLI_PBB_PORT_USE_DEI = 44,
    CLI_PBB_SHOW_PORT_PCP_ENCODE = 45,
    CLI_PBB_SHOW_PORT_PCP_DECODE = 46,
    CLI_PBB_SHOW_PORT_CONFIG = 47,
    CLI_PBB_NO_OUI_ALL = 48,
    CLI_PBB_SERVICE_TYPE = 49,
    CLI_PBB_MAP_BACKBONE_INSTANCE = 50,
    CLI_PBB_NO_BACKBONE_INSTANCE = 51,
    CLI_PBB_BACKBONE_INSTANCE = 52,
    CLI_PBB_SHOW_BACKBONE_INSTANCE = 53,
    CLI_PBB_SHOW_BACKBONE_MAP = 54    
};

enum {
    CLI_PBB_UNKNOWN_ERR = 1,
    CLI_PBB_MODULE_NOT_INITIALIZED, 
    CLI_PBB_BACKBONE_EDGE_BRIDGE_ADDRESS_INVALID, 
    CLI_PBB_BACKBONE_EDGE_BRIDGE_NAME_INVALID, 
    CLI_PBB_MODULE_SHUTDOWN, 
    CLI_PBB_INVALID_CONTEXT, 
    CLI_PBB_INVALID_VIP, 
    CLI_PBB_INVALID_BRIDGE_MODE, 
    CLI_PBB_INVALID_ROWSTATUS, 
    CLI_PBB_VALID_ROWSTATUS, 
    CLI_PBB_INVALID_ISID, 
    CLI_PBB_INVALID_ISID_VALUE, 
    CLI_PBB_ROWSTATUS_ACTIVE, 
    CLI_PBB_INVALID_MACADDR, 
    CLI_PBB_INVALID_VIPTYPE, 
    CLI_PBB_INVALID_COPMTYPE, 
    CLI_PBB_INVALID_ROWSTATUS_VALUE, 
    CLI_PBB_INVALID_PORT, 
    CLI_PBB_PIP_PORT_NOT_PRESENT, 
    CLI_PBB_MACADDR_NOT_PRESENT, 
    CLI_PBB_PIPNAME_NOT_PRESENT, 
    CLI_PBB_MACADDR_INVALID, 
    CLI_PBB_PIPNAME_INVALID,
    CLI_PBB_GLB_OUI_INVALID,
    CLI_PBB_GLB_OUI_LENGTH_INVALID,
    CLI_PBB_MAX_ISID_INVALID,
    CLI_PBB_MAX_ISID_PER_CONTEXT_INVALID,
    CLI_PBB_MAX_PORTS_ISID_INVALID,
    CLI_PBB_MAX_PORTS_ISID_CNTXT_INVALID,
    CLI_PBB_PORT_NOT_CBP,
    CLI_PBB_PORT_NOT_MAP_TO_ISID,
    CLI_PBB_PORT_NOT_CNP,
    CLI_PBB_PORT_NOT_PIP,
    CLI_PBB_PCP_SEL_INVALID,
    CLI_PBB_PCP_VALUE_INVALID,
    CLI_PBB_PCP_PRIORITY_INVALID,
    CLI_PBB_PCP_DE_INVALID,
    CLI_PBB_PORT_NOT_MAPPED,
    CLI_PBB_INVALID_CNP_PORT_TYPE,
    CLI_PBB_INVALID_PNP_PORT_TYPE,
    CLI_PBB_MULT_MAC_NOT_ALLOWED,
    CLI_PBB_BROAD_MAC_NOT_ALLOWED,
    CLI_PBB_PORT_PER_ISID_CTXT_EXCEED,
    CLI_PBB_PORT_PER_ISID_EXCEED,
    CLI_PBB_PORT_ISID_PIP_INVALID,
    CLI_PBB_TRANS_ISID_ERR,
    CLI_PBB_I_B_COMP_PRESENT,
    CLI_PBB_NO_SHUT_MEM_ALLOC_ERR,
    CLI_PBB_CONF_ERR,
    CLI_PBB_SERVICE_TYPE_ERR,
    CLI_PBB_INVALID_INSTANCE_ERR, 
    CLI_PBB_DEF_INSTANCE_CREATED_ERR, 
    CLI_PBB_DEF_INSTANCE_DELETE_ERR, 
    CLI_PBB_INSTANCE_DELETE_ERR, 
    CLI_PBB_INSTANCE_MULT_MAC_ERR,
    CLI_PBB_INSTANCE_BROAD_MAC_ERR,   
    CLI_PBB_MAX_ERR
};


#ifdef __PBBCLI_C__

CONST CHR1  *PbbCliErrString [] = {
    NULL,
    "Unknown Error\r\n",
    "PBB Module is not initialzed\r\n",
    "PBB Backbone Edge Bridge Address Invalid\r\n",
    "PBB Backbone Edge Bridge Name Invalid\r\n",
    "PBB Module is in Shutdown State\r\n",
    "Context is Invalid\r\n",
    "VIP is not Created\r\n",
    "Bridge Mode is not PBB\r\n",
    "Row is not Created\r\n",
    "Row is already Created\r\n",
    "ISID is not Created\r\n",
    "ISID Value is out of range\r\n",
    "ISID Can Not be created as VIP Row Status is already Active\r\n",
    "Invalid Mac Address\r\n",
    "Invalid VipType\r\n",
    "Operation is not allowed in this Component Type\r\n",
    "Rowstatus Value not allowed\r\n",
    "Port Set is not Valid\r\n",
    "Pip Port is not created\r\n",
    "Mac Address not Presnt\r\n",
    "Pip Name Not Present\r\n",
    "Mac Address Invalid\r\n",
    "Pip Name Invalid\r\n",
    "PBB OUI value is invalid\r\n",
    "PBB OUI Length should be 3 bytes\r\n",
    "Sizing Parameters Command Failed : Maximum number of Isid value - Out of "
        "range\r\n",
    "Sizing Parameters Command Failed : Maximum number of Isid per context"
        " value - Out of range\r\n",
    "Sizing Parameters Command Failed : Maximum number of Ports per Isid value "
        "- Out of range\r\n",
    "Sizing Parameters Command Failed : Maximum number of Ports per Isid per "
        "context value - Out of range\r\n",
    "Port Type should be Customer Backbone Port\r\n",
    "Port(s) should be mapped to this ISID\r\n",
    "Port Type should be Customer Network Port\r\n",
    "Port Type should be Provider Instance Port\r\n",
    "Pcp Selection Row Value is Invalid\r\n",
    "Pcp Value is Invalid\r\n",
    "Pcp Priority Value is Invalid\r\n",
    "PCP Drop eligiblity Value is Invalid\r\n",
    "PBB Port should be mapped to this Isid\r\n",
    "CNP Port Type cannot be mapped to ISID mode\r\n",
    "PNP Port Type cannot be mapped to ISID mode\r\n",
    "PBB Destination Mac Address for B-Comp cannot be multicast address\r\n",
    "PBB Destination Mac Address for B-Comp cannot be broadcast address\r\n",
    "PBB Cannot add more ports to this Isid since Ports per Isid per Context"
     " cannot exceed the value configured in the system sizing parameters\r\n",
    "PBB Cannot add more ports to this Isid since Ports per isid cannot"
     " exceed the value configured in the system sizing parameters\r\n",
     " port cannot be associated to the ISID/Context\r\n",
     " Local ISID already mapped to some other Backbone ISID \r\n",
     " PBB cant be shutdown when I or B Components still exist\r\n",
    "PBB unable to start due to memory shortage \r\n",
    "Configuration Failed\r\n",
    "Service type cannot be set as ISID is active\r\n",
    "Backbone Instance is Invalid\r\n",
    "Backbone default Instance is already created\r\n",
    "Backbone default Instance cannot be deleted\r\n",
    "Backbone Instance cannot be deleted, as mapped to some context\r\n",
    "PBB Instance Mac Address for cannot be multicast address\r\n",
    "PBB Instance Mac Address for cannot be broadcast address\r\n",   
    "\r\n"
};

#else
extern CONST CHR1  *PbbCliErrString [];
#endif
/*enum {
  CLI_PBB_MAX_ERR = 1
};*/



 
#define PBB_CLI_INVALID_CONTEXT   0xFFFFFFFF

#define CLI_PBB_MODE      "si-"
#define PBB_MAX_ARGS      20
#define PBB_TRC_MAX_SIZE              288

#define PBB_SNMP_TRUE                 2
#define PBB_SNMP_FALSE                1
#define PBB_ACTIVE           1
#define PBB_NOT_IN_SERVICE   2
#define PBB_NOT_READY        3
#define PBB_CREATE_AND_GO    4
#define PBB_CREATE_AND_WAIT  5
#define PBB_DESTROY          6

#define PBB_DEFAULT_GBL_OUI        "00:1E:83"


#define PBB_ENABLE                   1
#define PBB_DISABLE                  2

#define PBB_OUI_LENGTH          3  /* OUI Value size */
#define PBB_CLI_OUI_STR_LEN     9 
#define PBB_MAX_PORT_NAME_LENGTH CFA_MAX_PORT_NAME_LENGTH

#define PBB_CONTEXT_LIST_SIZE ((PBB_MAX_CONTEXTS + 31)/32 * 4)

typedef struct CbpStatus {
  UINT4  u4CbpNum;
        UINT4  u4LocalIsid;
        INT4  i4BvlanId;
        UINT1  au1Oui[PBB_OUI_LENGTH];
        UINT1  u1isRowCreated;
        INT4   i4RetValCBPRowStatus;
} tCbpStatus;




INT4 cli_process_pbb_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command, ...));
INT4 cli_process_pbb_show_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command, ...));
INT4 PbbISIDMode (tCliHandle CliHandle , UINT4 u4ISID);
INT1 PbbGetVcmIsidCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);
INT4 PbbCliSelectContextOnMode (tCliHandle CliHandle, UINT4 u4Cmd,
          UINT4 *pu4Context, UINT4 *pu4LocalPort);
INT4 PbbSetPisidOnPort(tCliHandle CliHandle, UINT4 u4Pisid, UINT4 u4PortId);
INT4 PbbDelPortPisid(tCliHandle CliHandle , UINT4 u4PortId);
INT4 PbbShutdown (tCliHandle CliHandle , INT4 i4Status);
INT4 PbbIsidActive (tCliHandle CliHandle , UINT4 u4ContextId , UINT4 u4IfIndex, INT4 i4Status);
INT4 PbbGlbOUI (tCliHandle CliHandle , tSNMP_OCTET_STRING_TYPE* oui );
INT4 PbbCreateOUI (tCliHandle CliHandle, UINT4 u4ContextId , tSNMP_OCTET_STRING_TYPE* pOui , UINT1 *pu1Ports);
INT4 PbbDeleteOUIList (tCliHandle CliHandle , UINT4 u4ContextId, UINT1 *pu1Ports);
INT4 PbbDeleteOUIAll (tCliHandle CliHandle , UINT4 u4ContextId);
INT4 PbbDeleteOUI (tCliHandle CliHandle, UINT4 u4ContextId, tCbpStatus PortsList[], UINT4 u4TotalPort);
INT4 PbbCliGetContextForShowCmd (tCliHandle CliHandle, UINT1 *pu1Name, UINT4 u4IfIndex, UINT4 u4CurrContext, UINT4 *pu4NextContext, UINT2 *pu2LocalPort);
INT4 PbbTranslateIsid(tCliHandle CliHandle, UINT4 u4LocalIsid, tPortList MemberPortList);
INT4 PbbCliShowSizingParameters(tCliHandle CliHandle);

INT4 PbbSetDestMacAddr(tCliHandle CliHandle, UINT4 u4IfIndex, UINT1 * SetValFsPbbDestinationMacAddr);

INT4
PbbBvlanMapping(tCliHandle CliHandle, tPortList MemberPortList,INT4 i4BvlanId);
INT4
PbbIsidMemberPorts(tCliHandle CliHandle, tPortList MemberPortList,INT4 i4ComponentType);



INT1
PbbGetCbpForIsid(UINT4 u4ContextId, UINT4 u4Isid, tSNMP_OCTET_STRING_TYPE *pRetValCbpList);
INT4 PbbCliShowOui(tCliHandle CliHandle);
INT4 PbbCreatePipIsidMapping(tCliHandle CliHandle,tPortList MemberPortList,UINT4 u4Vip,UINT4 u4Isid);
INT4 PbbCreateCbpIsidMapping(tPortList MemberPortList,UINT4 u4Isid,UINT4 u4ContextId);
INT4
PbbShowPisid (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4PortId);




INT4
PbbShowIsid(tCliHandle CliHandle, UINT4 u4Isid, UINT4 u4ContextId);
INT4
PbbShowIsidInContext(tCliHandle CliHandle, UINT4 u4Isid, UINT4 u4ContextId, UINT4 u4VipIfIndex);
INT4 PbbGetFirstOUIPortInIsidContext(UINT4 u4ContextId,UINT4 u4Isid,UINT4 *pu4Port);

INT4
PbbSelPipMac (
        tCliHandle CliHandle, 
        UINT4 u4PortId,
        UINT1 *pu1SetValArPbbMacAddr
        );

INT4
PbbVcmIsSwitchExist (UINT1 *pu1Alias, UINT4 *pu4VcNum);
INT1
PbbVlanGetMemberVlanList(UINT4 u4ContextId, tSNMP_OCTET_STRING_TYPE *pVlanIdList,
                                   UINT2 u2VipPort, UINT1 u1IsTagged);

INT4 PbbSetSizingParameters(
        tCliHandle CliHandle,
        INT4 *pi4MaxIsid,
        INT4 *pi4MaxIsidPerContext,
        INT4 *pi4MaxPortsPerIsid,
        INT4 *pi4MaxPortsPerIsidPerContext
        );

INT4
PbbCliGetVipIndex(UINT4 *u4Value);
INT4
PbbCliGetVip(UINT1 *pu1Isid, UINT4 *pu4Value);
 INT4
PbbDeleteISID (tCliHandle CliHandle, UINT4 u4Isid);

INT4
PbbCliShowDebugging (tCliHandle CliHandle);
INT4
PbbCliUpdTraceInput (tCliHandle CliHandle, UINT1 *pu1TraceInput);
INT4
PbbShowBackboneInstanceConfig (tCliHandle CliHandle, UINT1 * pu1InstanceName);
INT4
PbbShowBackboneInstanceMap (tCliHandle CliHandle, UINT4 u4ContextId);
INT4
PbbRemoveBackboneInstanceMapping (tCliHandle CliHandle, UINT4 u4ContextId);
INT4
PbbCreateBackboneInstanceMapping (tCliHandle CliHandle, UINT4 u4ContextId, UINT1* pu1InstanceName);
INT4
PbbDeleteBackboneInstance (tCliHandle CliHandle, UINT1* pu1InstanceName);
INT4
PbbCreateBackboneInstance (tCliHandle CliHandle, UINT1 * pu1InstanceName, UINT1 * pu1MacAddr);
INT4
PbbSetServiceType(tCliHandle CliHandle, UINT4 u4ContextId, INT4 i4CompType,
                    INT4 i4ServiceType,tPortList MemberPortList);
INT4
PbbSetPcpDecodingTable ( tCliHandle CliHandle, INT4 i4IfIndex,
                           INT4 i4SelRow, INT4 i4Pcp, INT4 i4Priority,
                           INT4 i4Dei);

INT4
PbbSetPcpEncodingTable (tCliHandle CliHandle, INT4 i4IfIndex,
                           INT4 i4SelRow, INT4 i4Priority, INT4 i4Pcp,
                           INT4 i4Dei);
INT4
PbbResetPcpDecodingTable (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4SelRow);


INT4
PbbResetPcpEncodingTable (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4SelRow);

INT4
PbbSetPipPcpDecodingTable ( tCliHandle CliHandle, INT4 i4IfIndex,
                           INT4 i4SelRow, INT4 i4Pcp, INT4 i4Priority,
                           INT4 i4Dei);
INT4
PbbSetPipPcpEncodingTable (tCliHandle CliHandle, INT4 i4IfIndex,
                           INT4 i4SelRow, INT4 i4Priority, INT4 i4Pcp,
                           INT4 i4Dei);
INT4
PbbResetPipPcpDecodingTable (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4SelRow);


INT4
PbbResetPipPcpEncodingTable (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4SelRow);

INT4
PbbSetPortReqDropEncoding (tCliHandle CliHandle, INT4 i4IfIndex,
                              INT4 i4ReqDropEncoding);
INT4
PbbSetPipReqDropEncoding (tCliHandle CliHandle, INT4 i4IfIndex,
                              INT4 i4ReqDropEncoding);
INT4
PbbSetPortPcpSelRow (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4PcpSelRow);

INT4
PbbSetPipPcpSelRow (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4PcpSelRow);
INT4
PbbSetPortUseDei (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4UseDei);
INT4
PbbSetPipUseDei (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4UseDei);
INT4
PbbShowPcpDecodingTable (tCliHandle CliHandle, INT4 i4IfIndex, UINT1*);
INT4
PbbShowPortConfig (tCliHandle CliHandle, INT4 i4IfIndex, UINT1*);

INT4
PbbShowPcpEncodingTable (tCliHandle CliHandle, INT4 i4IfIndex, UINT1*);
INT4
PbbShowPipPcpEncodingTable (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 *pu1ContextName);
INT4
PbbShowPipPcpDecodingTable (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 *pu1ContextName);
INT4
PbbShowPipPortConfig (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 *pu1ContextName);
INT4 PbbGlobalShowRunningConfig (tCliHandle CliHandle);
INT4 PbbShowRunningConfig (tCliHandle CliHandle, UINT4 u4ContextId, UINT4
                           u4Module);
VOID PbbShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4IfIndex);


#endif
