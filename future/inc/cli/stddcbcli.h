/********************************************************************
* Copyright (C) 2009 Aricent Inc . All Rights Reserved
*
* $Id: stddcbcli.h,v 1.4 2016/07/09 09:41:21 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 LldpXdot1dcbxConfigETSConfigurationTxEnable[15];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetLldpXdot1dcbxConfigETSConfigurationTxEnable(i4LldpV2PortConfigIfIndex , u4LldpV2PortConfigDestAddressIndex ,i4SetValLldpXdot1dcbxConfigETSConfigurationTxEnable) \
 nmhSetCmn(LldpXdot1dcbxConfigETSConfigurationTxEnable, 15, LldpXdot1dcbxConfigETSConfigurationTxEnableSet, NULL, NULL, 0, 0, 2, "%i %u %i", i4LldpV2PortConfigIfIndex , u4LldpV2PortConfigDestAddressIndex ,i4SetValLldpXdot1dcbxConfigETSConfigurationTxEnable)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 LldpXdot1dcbxConfigETSRecommendationTxEnable[15];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetLldpXdot1dcbxConfigETSRecommendationTxEnable(i4LldpV2PortConfigIfIndex , u4LldpV2PortConfigDestAddressIndex ,i4SetValLldpXdot1dcbxConfigETSRecommendationTxEnable) \
 nmhSetCmn(LldpXdot1dcbxConfigETSRecommendationTxEnable, 15, LldpXdot1dcbxConfigETSRecommendationTxEnableSet, NULL, NULL, 0, 0, 2, "%i %u %i", i4LldpV2PortConfigIfIndex , u4LldpV2PortConfigDestAddressIndex ,i4SetValLldpXdot1dcbxConfigETSRecommendationTxEnable)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 LldpXdot1dcbxConfigPFCTxEnable[15];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetLldpXdot1dcbxConfigPFCTxEnable(i4LldpV2PortConfigIfIndex , u4LldpV2PortConfigDestAddressIndex ,i4SetValLldpXdot1dcbxConfigPFCTxEnable) \
 nmhSetCmn(LldpXdot1dcbxConfigPFCTxEnable, 15, LldpXdot1dcbxConfigPFCTxEnableSet, NULL, NULL, 0, 0, 2, "%i %u %i", i4LldpV2PortConfigIfIndex , u4LldpV2PortConfigDestAddressIndex ,i4SetValLldpXdot1dcbxConfigPFCTxEnable)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 LldpXdot1dcbxConfigApplicationPriorityTxEnable[15];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetLldpXdot1dcbxConfigApplicationPriorityTxEnable(i4LldpV2PortConfigIfIndex , u4LldpV2PortConfigDestAddressIndex ,i4SetValLldpXdot1dcbxConfigApplicationPriorityTxEnable) \
 nmhSetCmn(LldpXdot1dcbxConfigApplicationPriorityTxEnable, 15, LldpXdot1dcbxConfigApplicationPriorityTxEnableSet, NULL, NULL, 0, 0, 2, "%i %u %i", i4LldpV2PortConfigIfIndex , u4LldpV2PortConfigDestAddressIndex ,i4SetValLldpXdot1dcbxConfigApplicationPriorityTxEnable)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 LldpXdot1dcbxAdminETSConWilling[16];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetLldpXdot1dcbxAdminETSConWilling(i4LldpV2LocPortIfIndex ,i4SetValLldpXdot1dcbxAdminETSConWilling) \
 nmhSetCmn(LldpXdot1dcbxAdminETSConWilling, 16, LldpXdot1dcbxAdminETSConWillingSet, NULL, NULL, 0, 0, 1, "%i %i", i4LldpV2LocPortIfIndex ,i4SetValLldpXdot1dcbxAdminETSConWilling)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 LldpXdot1dcbxAdminETSConPriority[16];
extern UINT4 LldpXdot1dcbxAdminETSConPriTrafficClass[16];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetLldpXdot1dcbxAdminETSConPriTrafficClass(i4LldpV2LocPortIfIndex , u4LldpXdot1dcbxAdminETSConPriority ,u4SetValLldpXdot1dcbxAdminETSConPriTrafficClass) \
 nmhSetCmn(LldpXdot1dcbxAdminETSConPriTrafficClass, 16, LldpXdot1dcbxAdminETSConPriTrafficClassSet, NULL, NULL, 0, 0, 2, "%i %u %u", i4LldpV2LocPortIfIndex , u4LldpXdot1dcbxAdminETSConPriority ,u4SetValLldpXdot1dcbxAdminETSConPriTrafficClass)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 LldpXdot1dcbxAdminETSConTrafficClass[16];
extern UINT4 LldpXdot1dcbxAdminETSConTrafficClassBandwidth[16];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetLldpXdot1dcbxAdminETSConTrafficClassBandwidth(i4LldpV2LocPortIfIndex , u4LldpXdot1dcbxAdminETSConTrafficClass ,u4SetValLldpXdot1dcbxAdminETSConTrafficClassBandwidth) \
 nmhSetCmn(LldpXdot1dcbxAdminETSConTrafficClassBandwidth, 16, LldpXdot1dcbxAdminETSConTrafficClassBandwidthSet, NULL, NULL, 0, 0, 2, "%i %u %u", i4LldpV2LocPortIfIndex , u4LldpXdot1dcbxAdminETSConTrafficClass ,u4SetValLldpXdot1dcbxAdminETSConTrafficClassBandwidth)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 LldpXdot1dcbxAdminETSConTSATrafficClass[16];
extern UINT4 LldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm[16];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm(i4LldpV2LocPortIfIndex , u4LldpXdot1dcbxAdminETSConTSATrafficClass ,i4SetValLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm) \
 nmhSetCmn(LldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm, 16, LldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmSet, NULL, NULL, 0, 0, 2, "%i %u %i", i4LldpV2LocPortIfIndex , u4LldpXdot1dcbxAdminETSConTSATrafficClass ,i4SetValLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 LldpXdot1dcbxAdminETSRecoTrafficClass[16];
extern UINT4 LldpXdot1dcbxAdminETSRecoTrafficClassBandwidth[16];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetLldpXdot1dcbxAdminETSRecoTrafficClassBandwidth(i4LldpV2LocPortIfIndex , u4LldpXdot1dcbxAdminETSRecoTrafficClass ,u4SetValLldpXdot1dcbxAdminETSRecoTrafficClassBandwidth) \
 nmhSetCmn(LldpXdot1dcbxAdminETSRecoTrafficClassBandwidth, 16, LldpXdot1dcbxAdminETSRecoTrafficClassBandwidthSet, NULL, NULL, 0, 0, 2, "%i %u %u", i4LldpV2LocPortIfIndex , u4LldpXdot1dcbxAdminETSRecoTrafficClass ,u4SetValLldpXdot1dcbxAdminETSRecoTrafficClassBandwidth)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 LldpXdot1dcbxAdminETSRecoTSATrafficClass[16];
extern UINT4 LldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithm[16];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithm(i4LldpV2LocPortIfIndex , u4LldpXdot1dcbxAdminETSRecoTSATrafficClass ,i4SetValLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithm) \
 nmhSetCmn(LldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithm, 16, LldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmSet, NULL, NULL, 0, 0, 2, "%i %u %i", i4LldpV2LocPortIfIndex , u4LldpXdot1dcbxAdminETSRecoTSATrafficClass ,i4SetValLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithm)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 LldpXdot1dcbxAdminPFCWilling[16];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetLldpXdot1dcbxAdminPFCWilling(i4LldpV2LocPortIfIndex ,i4SetValLldpXdot1dcbxAdminPFCWilling) \
 nmhSetCmn(LldpXdot1dcbxAdminPFCWilling, 16, LldpXdot1dcbxAdminPFCWillingSet, NULL, NULL, 0, 0, 1, "%i %i", i4LldpV2LocPortIfIndex ,i4SetValLldpXdot1dcbxAdminPFCWilling)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 LldpXdot1dcbxAdminPFCEnablePriority[16];
extern UINT4 LldpXdot1dcbxAdminPFCEnableEnabled[16];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetLldpXdot1dcbxAdminPFCEnableEnabled(i4LldpV2LocPortIfIndex , u4LldpXdot1dcbxAdminPFCEnablePriority ,i4SetValLldpXdot1dcbxAdminPFCEnableEnabled) \
 nmhSetCmn(LldpXdot1dcbxAdminPFCEnableEnabled, 16, LldpXdot1dcbxAdminPFCEnableEnabledSet, NULL, NULL, 0, 0, 2, "%i %u %i", i4LldpV2LocPortIfIndex , u4LldpXdot1dcbxAdminPFCEnablePriority ,i4SetValLldpXdot1dcbxAdminPFCEnableEnabled)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 LldpXdot1dcbxAdminApplicationPriorityAESelector[15];
extern UINT4 LldpXdot1dcbxAdminApplicationPriorityAEProtocol[15];
extern UINT4 LldpXdot1dcbxAdminApplicationPriorityAEPriority[15];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetLldpXdot1dcbxAdminApplicationPriorityAEPriority(i4LldpV2LocPortIfIndex , i4LldpXdot1dcbxAdminApplicationPriorityAESelector , u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol ,u4SetValLldpXdot1dcbxAdminApplicationPriorityAEPriority) \
 nmhSetCmn(LldpXdot1dcbxAdminApplicationPriorityAEPriority, 15, LldpXdot1dcbxAdminApplicationPriorityAEPrioritySet, NULL, NULL, 0, 0, 3, "%i %i %u %u", i4LldpV2LocPortIfIndex , i4LldpXdot1dcbxAdminApplicationPriorityAESelector , u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol ,u4SetValLldpXdot1dcbxAdminApplicationPriorityAEPriority)

#endif
