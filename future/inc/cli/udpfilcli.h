#ifndef _UDPFILCLI_H
#define _UDPFILCLI_H

#include "midconst.h"
#include "cli.h"

/* COMMANDS UNDER UDPFILTER MODE */
#define UDPFILT_CLI_SHOW_REDIRECT_INFO   1
#define UDPFILT_CLI_SHOW_BCASTBLOCK_INFO 2

VOID cli_process_udpfilter_cmd(tCliHandle CliHandle, UINT4, ...);

#endif	 /* _UDPFILCLI_H */  
