/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tpcli.h,v 1.5 Feb 10 08:58:32 2010 prabuc 
 *
 * Description:This file contains the Function prototypes and
 *             macros of TACACS CLI
 *             
 *
 *******************************************************************/


#ifndef __TPCLI_H__
#define __TPCLI_H__

#include "cli.h"
/* Import From utils */
#include "utilipvx.h"

/* TACACS+ CLI commands */
#define   TAC_CMD_SHOW_TACACS   1
#define   TAC_CMD_SET_SERVER   2
#define   TAC_CMD_NO_SERVER   3
#define   TAC_CMD_SET_ACTIVE_SERVER   4
#define   TAC_CMD_SET_NO_ACTIVE_SERVER   5
#define   TAC_CMD_SET_TRACE_LEVEL   6
#define   TAC_CMD_SET_RETRANSMIT    7
#define   TAC_CMD_SET_NO_RETRANSMIT 8

#define   TAC_CLI_CMDS_MAX   8
#define   TAC_CLI_SECRET_KEY_LEN   64 /* This is same TAC_SECRET_KEY_LEN 
                                      * defined in future/tacacs/inc/tpaaa.h
                                      */

#define   TAC_VALUE_NOT_PRESENT   0xFFFFFFFF

/* TACACS+ CLI command data structures */
enum {
      CLI_TAC_INV_SRV = 1,
      CLI_TAC_INV_RETRIES,
      CLI_TAC_MAX_SRV,
      CLI_TAC_INV_STATE,
      CLI_TAC_ACT_SRV,
      CLI_TAC_INV_PORT,
      CLI_TAC_INV_TIME,
      CLI_TAC_INV_IP,
      CLI_TAC_KEY_TOO_LONG,
      CLI_TAC_RTRY_EXCEED_SERVER,
      CLI_TAC_MAX_DOMAIN_LEN,
      CLI_TAC_MAX_ERR
};
/* The error strings should be places under the switch so as to avoid redifinition
 * This will be visible only in modulecli.c file
 */
#ifdef __TPCLI_C__
CONST CHR1  *TacCliErrString[] = {
       NULL,
       "\r% Invalid TACACS server \r\n",
       "\r% Invalid retransmit value \r\n",
       "\r% No more servers can be added \r\n",
       "\r% Invalid server status \r\n",
       "\r% Cannot modify/delete the active server \r\n",
       "\r% Invalid port number \r\n",
       "\r% Invalid timeout value \r\n",
       "\r% Invalid IpAddress \r\n",
       "\r% Key Length too Long\r\n",
       "\r% Retransmit value should be less than Max server count\r\n",
       "\r% Maximum domain name length exceeded \r\n"
};
#endif
typedef struct _tacsrvconf {
    tIPvXAddr     IpAddress; /* IpAddress holds both IPv4 and IPv6 addresses*/
    UINT4        u4Timeout;
    UINT4        u4Port;
    UINT4        u4SingleConnect;
    UINT1        au1Key[TAC_CLI_SECRET_KEY_LEN];
    UINT1        au1HostName[DNS_MAX_QUERY_LEN];
    UINT1        au1Pad[1];
} tTacSrvConf;

typedef union {
    tTacSrvConf   TacSrv;
    tIPvXAddr     IpAddress; /* IpAddress holds both IPv4 and IPv6 active server addresses*/
    UINT4         u4TraceLevel;
    UINT4         u4Retries;
    UINT1        au1HostName[DNS_MAX_QUERY_LEN];
    UINT1        au1Pad[1];
} tTacCliInputParams;

/* TACACS+ CLI process main function prototype */

VOID cli_process_tacacs_cmd (tCliHandle, UINT4 , ...);
INT4 tacCmdSetServer (tCliHandle CliHandle, tTacCliInputParams * pTacCliInput);
INT4 tacCmdNoServer (tCliHandle CliHandle, tTacCliInputParams * pTacCliInput);
INT4 tacCmdSetActiveServer (tCliHandle CliHandle, 
                            tTacCliInputParams * pTacCliInput);
INT4 tacCmdSetNoActiveServer (tCliHandle CliHandle);
INT4 tacCmdSetTraceLevel (tCliHandle CliHandle,
                          tTacCliInputParams * pTacCliInput);
INT4 tacCmdSetRetransmit (tCliHandle CliHandle,
                          tTacCliInputParams * pTacCliInput);
INT4 tacCmdSetNoRetransmit (tCliHandle CliHandle);
INT4 tacCmdShowTacacs (tCliHandle CliHandle, tTacCliInputParams * pTacCliInput);
INT4 tacCmdShowServers (tCliHandle CliHandle,
                        tTacCliInputParams * pTacCliInput);
INT4 tacCmdShowStatistics (tCliHandle CliHandle);

INT4 TacacsShowRunningConfig(tCliHandle CliHandle);
VOID IssTacacShowDebugging (tCliHandle CliHandle);





#endif /* __TPCLI_H__ */
