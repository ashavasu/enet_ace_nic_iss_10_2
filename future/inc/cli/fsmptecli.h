/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmptecli.h,v 1.8 2016/02/03 10:38:01 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsTunnelType[13];
extern UINT4 FsMplsTunnelLSRIdMapInfo[13];
extern UINT4 FsMplsTunnelMode[13];
extern UINT4 FsMplsTunnelMBBStatus[13];
extern UINT4 FsMplsTunnelDisJointType[13];
extern UINT4 FsMplsTunnelAttPointer[13];
extern UINT4 FsMplsTunnelEndToEndProtection[13];
extern UINT4 FsMplsTunnelPrConfigOperType[13];
extern UINT4 FsMplsTunnelSrlgType[13];
extern UINT4 FsMplsTunnelIfIndex[13];
extern UINT4 FsMplsTunnelInitReOptimize[13];
extern UINT4 FsMplsTunnelIsProtectingLsp[13];
extern UINT4 FsMplsTunnelBackupHopTableIndex[13];
extern UINT4 FsMplsTunnelBackupPathInUse[13];


#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsTunnelType(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,pSetValFsMplsTunnelType) \
 nmhSetCmn(FsMplsTunnelType, 13, FsMplsTunnelTypeSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %s", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,pSetValFsMplsTunnelType)
#define nmhSetFsMplsTunnelLSRIdMapInfo(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,pSetValFsMplsTunnelLSRIdMapInfo) \
 nmhSetCmn(FsMplsTunnelLSRIdMapInfo, 13, FsMplsTunnelLSRIdMapInfoSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %s", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,pSetValFsMplsTunnelLSRIdMapInfo)
#define nmhSetFsMplsTunnelMode(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsTunnelMode) \
 nmhSetCmn(FsMplsTunnelMode, 13, FsMplsTunnelModeSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsTunnelMode)
#define nmhSetFsMplsTunnelMBBStatus(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsTunnelMBBStatus) \
 nmhSetCmn(FsMplsTunnelMBBStatus, 13, FsMplsTunnelMBBStatusSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsTunnelMBBStatus)
#define nmhSetFsMplsTunnelDisJointType(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsTunnelDisJointType) \
 nmhSetCmn(FsMplsTunnelDisJointType, 13, FsMplsTunnelDisJointTypeSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsTunnelDisJointType)
#define nmhSetFsMplsTunnelAttPointer(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,pSetValFsMplsTunnelAttPointer) \
 nmhSetCmn(FsMplsTunnelAttPointer, 13, FsMplsTunnelAttPointerSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %o", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,pSetValFsMplsTunnelAttPointer)
#define nmhSetFsMplsTunnelEndToEndProtection(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,pSetValFsMplsTunnelEndToEndProtection) \
 nmhSetCmn(FsMplsTunnelEndToEndProtection, 13, FsMplsTunnelEndToEndProtectionSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %s", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,pSetValFsMplsTunnelEndToEndProtection)
#define nmhSetFsMplsTunnelPrConfigOperType(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsTunnelPrConfigOperType) \
 nmhSetCmn(FsMplsTunnelPrConfigOperType, 13, FsMplsTunnelPrConfigOperTypeSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsTunnelPrConfigOperType)
#define nmhSetFsMplsTunnelSrlgType(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsTunnelSrlgType) \
 nmhSetCmn(FsMplsTunnelSrlgType, 13, FsMplsTunnelSrlgTypeSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsTunnelSrlgType)
#define nmhSetFsMplsTunnelIfIndex(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsTunnelIfIndex) \
 nmhSetCmn(FsMplsTunnelIfIndex, 13, FsMplsTunnelIfIndexSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsTunnelIfIndex)
#define nmhSetFsMplsTunnelInitReOptimize(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsTunnelInitReOptimize) \
 nmhSetCmn(FsMplsTunnelInitReOptimize, 13, FsMplsTunnelInitReOptimizeSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsTunnelInitReOptimize)
#define nmhSetFsMplsTunnelIsProtectingLsp(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsTunnelIsProtectingLsp) \
 nmhSetCmn(FsMplsTunnelIsProtectingLsp, 13, FsMplsTunnelIsProtectingLspSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsTunnelIsProtectingLsp)
#define nmhSetFsMplsTunnelBackupHopTableIndex(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,u4SetValFsMplsTunnelBackupHopTableIndex)   \
        nmhSetCmn(FsMplsTunnelBackupHopTableIndex, 13, FsMplsTunnelBackupHopTableIndexSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %u", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,u4SetValFsMplsTunnelBackupHopTableIndex)
#define nmhSetFsMplsTunnelBackupPathInUse(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,u4SetValFsMplsTunnelBackupPathInUse)   \
        nmhSetCmn(FsMplsTunnelBackupPathInUse, 13, FsMplsTunnelBackupPathInUseSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %u", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,u4SetValFsMplsTunnelBackupPathInUse)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsTpTunnelDestTunnelIndex[13];
extern UINT4 FsMplsTpTunnelDestTunnelLspNum[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsTpTunnelDestTunnelIndex(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,u4SetValFsMplsTpTunnelDestTunnelIndex) \
 nmhSetCmn(FsMplsTpTunnelDestTunnelIndex, 13, FsMplsTpTunnelDestTunnelIndexSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %u", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,u4SetValFsMplsTpTunnelDestTunnelIndex)
#define nmhSetFsMplsTpTunnelDestTunnelLspNum(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,u4SetValFsMplsTpTunnelDestTunnelLspNum) \
 nmhSetCmn(FsMplsTpTunnelDestTunnelLspNum, 13, FsMplsTpTunnelDestTunnelLspNumSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %u", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,u4SetValFsMplsTpTunnelDestTunnelLspNum)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsTunnelAttributeIndex[13];
extern UINT4 FsTunnelAttributeName[13];
extern UINT4 FsTunnelAttributeSetupPrio[13];
extern UINT4 FsTunnelAttributeHoldingPrio[13];
extern UINT4 FsTunnelAttributeIncludeAnyAffinity[13];
extern UINT4 FsTunnelAttributeIncludeAllAffinity[13];
extern UINT4 FsTunnelAttributeExcludeAnyAffinity[13];
extern UINT4 FsTunnelAttributeSessionAttributes[13];
extern UINT4 FsTunnelAttributeBandwidth[13];
extern UINT4 FsTunnelAttributeTeClassType[13];
extern UINT4 FsTunnelAttributeSrlgType[13];
extern UINT4 FsTunnelAttributeRowStatus[13];
extern UINT4 FsTunnelAttributeMask[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsTunnelAttributeName(u4FsTunnelAttributeIndex ,pSetValFsTunnelAttributeName) \
 nmhSetCmn(FsTunnelAttributeName, 13, FsTunnelAttributeNameSet, NULL, NULL, 0, 0, 1, "%u %s", u4FsTunnelAttributeIndex ,pSetValFsTunnelAttributeName)
#define nmhSetFsTunnelAttributeSetupPrio(u4FsTunnelAttributeIndex ,i4SetValFsTunnelAttributeSetupPrio) \
 nmhSetCmn(FsTunnelAttributeSetupPrio, 13, FsTunnelAttributeSetupPrioSet, NULL, NULL, 0, 0, 1, "%u %i", u4FsTunnelAttributeIndex ,i4SetValFsTunnelAttributeSetupPrio)
#define nmhSetFsTunnelAttributeHoldingPrio(u4FsTunnelAttributeIndex ,i4SetValFsTunnelAttributeHoldingPrio) \
 nmhSetCmn(FsTunnelAttributeHoldingPrio, 13, FsTunnelAttributeHoldingPrioSet, NULL, NULL, 0, 0, 1, "%u %i", u4FsTunnelAttributeIndex ,i4SetValFsTunnelAttributeHoldingPrio)
#define nmhSetFsTunnelAttributeIncludeAnyAffinity(u4FsTunnelAttributeIndex ,u4SetValFsTunnelAttributeIncludeAnyAffinity) \
 nmhSetCmn(FsTunnelAttributeIncludeAnyAffinity, 13, FsTunnelAttributeIncludeAnyAffinitySet, NULL, NULL, 0, 0, 1, "%u %u", u4FsTunnelAttributeIndex ,u4SetValFsTunnelAttributeIncludeAnyAffinity)
#define nmhSetFsTunnelAttributeIncludeAllAffinity(u4FsTunnelAttributeIndex ,u4SetValFsTunnelAttributeIncludeAllAffinity) \
 nmhSetCmn(FsTunnelAttributeIncludeAllAffinity, 13, FsTunnelAttributeIncludeAllAffinitySet, NULL, NULL, 0, 0, 1, "%u %u", u4FsTunnelAttributeIndex ,u4SetValFsTunnelAttributeIncludeAllAffinity)
#define nmhSetFsTunnelAttributeExcludeAnyAffinity(u4FsTunnelAttributeIndex ,u4SetValFsTunnelAttributeExcludeAnyAffinity) \
 nmhSetCmn(FsTunnelAttributeExcludeAnyAffinity, 13, FsTunnelAttributeExcludeAnyAffinitySet, NULL, NULL, 0, 0, 1, "%u %u", u4FsTunnelAttributeIndex ,u4SetValFsTunnelAttributeExcludeAnyAffinity)
#define nmhSetFsTunnelAttributeSessionAttributes(u4FsTunnelAttributeIndex ,pSetValFsTunnelAttributeSessionAttributes) \
 nmhSetCmn(FsTunnelAttributeSessionAttributes, 13, FsTunnelAttributeSessionAttributesSet, NULL, NULL, 0, 0, 1, "%u %s", u4FsTunnelAttributeIndex ,pSetValFsTunnelAttributeSessionAttributes)
#define nmhSetFsTunnelAttributeBandwidth(u4FsTunnelAttributeIndex ,u4SetValFsTunnelAttributeBandwidth) \
 nmhSetCmn(FsTunnelAttributeBandwidth, 13, FsTunnelAttributeBandwidthSet, NULL, NULL, 0, 0, 1, "%u %u", u4FsTunnelAttributeIndex ,u4SetValFsTunnelAttributeBandwidth)
#define nmhSetFsTunnelAttributeTeClassType(u4FsTunnelAttributeIndex ,i4SetValFsTunnelAttributeTeClassType) \
 nmhSetCmn(FsTunnelAttributeTeClassType, 13, FsTunnelAttributeTeClassTypeSet, NULL, NULL, 0, 0, 1, "%u %i", u4FsTunnelAttributeIndex ,i4SetValFsTunnelAttributeTeClassType)
#define nmhSetFsTunnelAttributeSrlgType(u4FsTunnelAttributeIndex ,i4SetValFsTunnelAttributeSrlgType) \
 nmhSetCmn(FsTunnelAttributeSrlgType, 13, FsTunnelAttributeSrlgTypeSet, NULL, NULL, 0, 0, 1, "%u %i", u4FsTunnelAttributeIndex ,i4SetValFsTunnelAttributeSrlgType)
#define nmhSetFsTunnelAttributeRowStatus(u4FsTunnelAttributeIndex ,i4SetValFsTunnelAttributeRowStatus) \
 nmhSetCmn(FsTunnelAttributeRowStatus, 13, FsTunnelAttributeRowStatusSet, NULL, NULL, 0, 1, 1, "%u %i", u4FsTunnelAttributeIndex ,i4SetValFsTunnelAttributeRowStatus)
#define nmhSetFsTunnelAttributeMask(u4FsTunnelAttributeIndex ,pSetValFsTunnelAttributeMask) \
 nmhSetCmn(FsTunnelAttributeMask, 13, FsTunnelAttributeMaskSet, NULL, NULL, 0, 0, 1, "%u %s", u4FsTunnelAttributeIndex ,pSetValFsTunnelAttributeMask)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsTunnelSrlgNo[13];
extern UINT4 FsMplsTunnelSrlgRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsTunnelSrlgRowStatus(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId , u4FsMplsTunnelSrlgNo ,i4SetValFsMplsTunnelSrlgRowStatus) \
 nmhSetCmn(FsMplsTunnelSrlgRowStatus, 13, FsMplsTunnelSrlgRowStatusSet, NULL, NULL, 0, 1, 5, "%u %u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId , u4FsMplsTunnelSrlgNo ,i4SetValFsMplsTunnelSrlgRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsTunnelAttributeSrlgNo[13];
extern UINT4 FsTunnelAttributeSrlgRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsTunnelAttributeSrlgRowStatus(u4FsTunnelAttributeIndex , u4FsTunnelAttributeSrlgNo ,i4SetValFsTunnelAttributeSrlgRowStatus) \
 nmhSetCmn(FsTunnelAttributeSrlgRowStatus, 13, FsTunnelAttributeSrlgRowStatusSet, NULL, NULL, 0, 1, 2, "%u %u %i", u4FsTunnelAttributeIndex , u4FsTunnelAttributeSrlgNo ,i4SetValFsTunnelAttributeSrlgRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsTunnelHopIncludeAny[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsTunnelHopIncludeAny(u4MplsTunnelHopListIndex , u4MplsTunnelHopPathOptionIndex , u4MplsTunnelHopIndex ,i4SetValFsMplsTunnelHopIncludeAny) \
 nmhSetCmn(FsMplsTunnelHopIncludeAny, 13, FsMplsTunnelHopIncludeAnySet, NULL, NULL, 0, 0, 3, "%u %u %u %i", u4MplsTunnelHopListIndex , u4MplsTunnelHopPathOptionIndex , u4MplsTunnelHopIndex ,i4SetValFsMplsTunnelHopIncludeAny)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsGmplsTunnelNotifyErrorTrapEnable[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsGmplsTunnelNotifyErrorTrapEnable(i4SetValFsGmplsTunnelNotifyErrorTrapEnable) \
 nmhSetCmn(FsGmplsTunnelNotifyErrorTrapEnable, 12, FsGmplsTunnelNotifyErrorTrapEnableSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsGmplsTunnelNotifyErrorTrapEnable)

#endif

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsReoptimizationTunnelIndex[13];
extern UINT4 FsMplsReoptimizationTunnelIngressLSRId[13];
extern UINT4 FsMplsReoptimizationTunnelEgressLSRId[13];
extern UINT4 FsMplsReoptimizationTunnelStatus[13];
extern UINT4 FsMplsReoptimizationTunnelManualTrigger[13];
extern UINT4 FsMplsReoptimizationTunnelRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsReoptimizationTunnelStatus(u4FsMplsReoptimizationTunnelIndex , u4FsMplsReoptimizationTunnelIngressLSRId , u4FsMplsReoptimizationTunnelEgressLSRId ,i4SetValFsMplsReoptimizationTunnelStatus)    \
 nmhSetCmn(FsMplsReoptimizationTunnelStatus, 13, FsMplsReoptimizationTunnelStatusSet, NULL, NULL, 0, 0, 3, "%u %u %u %i", u4FsMplsReoptimizationTunnelIndex , u4FsMplsReoptimizationTunnelIngressLSRId , u4FsMplsReoptimizationTunnelEgressLSRId ,i4SetValFsMplsReoptimizationTunnelStatus)
#define nmhSetFsMplsReoptimizationTunnelRowStatus(u4FsMplsReoptimizationTunnelIndex , u4FsMplsReoptimizationTunnelIngressLSRId , u4FsMplsReoptimizationTunnelEgressLSRId ,i4SetValFsMplsReoptimizationTunnelRowStatus)  \
 nmhSetCmn(FsMplsReoptimizationTunnelRowStatus, 13, FsMplsReoptimizationTunnelRowStatusSet, NULL, NULL, 0, 1, 3, "%u %u %u %i", u4FsMplsReoptimizationTunnelIndex , u4FsMplsReoptimizationTunnelIngressLSRId , u4FsMplsReoptimizationTunnelEgressLSRId ,i4SetValFsMplsReoptimizationTunnelRowStatus)

#endif
