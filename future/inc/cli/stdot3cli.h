/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdot3cli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 LldpXdot3PortConfigTLVsTxEnable[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetLldpXdot3PortConfigTLVsTxEnable(i4LldpPortConfigPortNum ,pSetValLldpXdot3PortConfigTLVsTxEnable)	\
	nmhSetCmn(LldpXdot3PortConfigTLVsTxEnable, 14, LldpXdot3PortConfigTLVsTxEnableSet, LldpLock, LldpUnLock, 0, 0, 1, "%i %s", i4LldpPortConfigPortNum ,pSetValLldpXdot3PortConfigTLVsTxEnable)

#endif
