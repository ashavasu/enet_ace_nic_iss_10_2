/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmirtcli.h,v 1.7 2016/02/08 10:40:13 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIRtmThrottleLimit[11];
extern UINT4 FsMIEcmpAcrossProtocolAdminStatus[11];
extern UINT4 FsMIRtmRouteLeakStatus[11];
extern UINT4 FsMIRtmMaximumStaticRoutes[12];
extern UINT4 FsMIRtmMaximumRipRoutes[12];
extern UINT4 FsMIRtmMaximumOspfRoutes[12];
extern UINT4 FsMIRtmMaximumBgpRoutes[12];
extern UINT4 FsMIRtmMaximumISISRoutes[12];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIRtmContextId[12];
extern UINT4 FsMIRrdRouterId[12];
extern UINT4 FsMIRrdFilterByOspfTag[12];
extern UINT4 FsMIRrdFilterOspfTag[12];
extern UINT4 FsMIRrdFilterOspfTagMask[12];
extern UINT4 FsMIRrdRouterASNumber[12];
extern UINT4 FsMIRrdAdminStatus[12];
extern UINT4 FsMIRrdForce[12];
extern UINT4 FsMIRTMIpStaticRouteDistance[12];

extern UINT4 FsMIRrdControlDestIpAddress[12];
extern UINT4 FsMIRrdControlNetMask[12];
extern UINT4 FsMIRrdControlSourceProto[12];
extern UINT4 FsMIRrdControlDestProto[12];
extern UINT4 FsMIRrdControlRouteExportFlag[12];
extern UINT4 FsMIRrdControlRowStatus[12];

extern UINT4 FsMIRrdRoutingProtoId[12];
extern UINT4 FsMIRrdAllowOspfAreaRoutes[12];
extern UINT4 FsMIRrdAllowOspfExtRoutes[12];

extern UINT4 FsMIRtmCommonRouteDest[12];
extern UINT4 FsMIRtmCommonRouteMask[12];
extern UINT4 FsMIRtmCommonRouteTos[12];
extern UINT4 FsMIRtmCommonRouteNextHop[12];
extern UINT4 FsMIRtmCommonRouteIfIndex[12];
extern UINT4 FsMIRtmCommonRouteType[12];
extern UINT4 FsMIRtmCommonRouteInfo[12];
extern UINT4 FsMIRtmCommonRouteNextHopAS[12];
extern UINT4 FsMIRtmCommonRouteMetric1[12];
extern UINT4 FsMIRtmCommonRoutePrivateStatus[12];
extern UINT4 FsMIRtmCommonRoutePreference[12];
extern UINT4 FsMIRtmCommonRouteStatus[12];

