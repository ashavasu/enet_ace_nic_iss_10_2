/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmiy1cli.h,v 1.12 2017/08/24 12:00:15 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIY1731ContextId[14];
extern UINT4 FsMIY1731FrameLossBufferClear[14];
extern UINT4 FsMIY1731FrameDelayBufferClear[14];
extern UINT4 FsMIY1731LbrCacheClear[14];
extern UINT4 FsMIY1731ErrorLogClear[14];
extern UINT4 FsMIY1731FrameLossBufferSize[14];
extern UINT4 FsMIY1731FrameDelayBufferSize[14];
extern UINT4 FsMIY1731LbrCacheSize[14];
extern UINT4 FsMIY1731LbrCacheHoldTime[14];
extern UINT4 FsMIY1731TrapControl[14];
extern UINT4 FsMIY1731ErrorLogStatus[14];
extern UINT4 FsMIY1731ErrorLogSize[14];
extern UINT4 FsMIY1731OperStatus[14];
extern UINT4 FsMIY1731LbrCacheStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMIY1731FrameLossBufferClear(u4FsMIY1731ContextId ,i4SetValFsMIY1731FrameLossBufferClear) \
 nmhSetCmnWithLock(FsMIY1731FrameLossBufferClear, 14, FsMIY1731FrameLossBufferClearSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 1, "%u %i", u4FsMIY1731ContextId ,i4SetValFsMIY1731FrameLossBufferClear)
#define nmhSetFsMIY1731FrameDelayBufferClear(u4FsMIY1731ContextId ,i4SetValFsMIY1731FrameDelayBufferClear) \
 nmhSetCmnWithLock(FsMIY1731FrameDelayBufferClear, 14, FsMIY1731FrameDelayBufferClearSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%u %i", u4FsMIY1731ContextId ,i4SetValFsMIY1731FrameDelayBufferClear)
#define nmhSetFsMIY1731LbrCacheClear(u4FsMIY1731ContextId ,i4SetValFsMIY1731LbrCacheClear) \
 nmhSetCmnWithLock(FsMIY1731LbrCacheClear, 14, FsMIY1731LbrCacheClearSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%u %i", u4FsMIY1731ContextId ,i4SetValFsMIY1731LbrCacheClear)
#define nmhSetFsMIY1731ErrorLogClear(u4FsMIY1731ContextId ,i4SetValFsMIY1731ErrorLogClear) \
 nmhSetCmnWithLock(FsMIY1731ErrorLogClear, 14, FsMIY1731ErrorLogClearSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 1, "%u %i", u4FsMIY1731ContextId ,i4SetValFsMIY1731ErrorLogClear)
#define nmhSetFsMIY1731FrameLossBufferSize(u4FsMIY1731ContextId ,i4SetValFsMIY1731FrameLossBufferSize) \
 nmhSetCmnWithLock(FsMIY1731FrameLossBufferSize, 14, FsMIY1731FrameLossBufferSizeSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 1, "%u %i", u4FsMIY1731ContextId ,i4SetValFsMIY1731FrameLossBufferSize)
#define nmhSetFsMIY1731FrameDelayBufferSize(u4FsMIY1731ContextId ,i4SetValFsMIY1731FrameDelayBufferSize) \
 nmhSetCmnWithLock(FsMIY1731FrameDelayBufferSize, 14, FsMIY1731FrameDelayBufferSizeSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%u %i", u4FsMIY1731ContextId ,i4SetValFsMIY1731FrameDelayBufferSize)
#define nmhSetFsMIY1731LbrCacheSize(u4FsMIY1731ContextId ,i4SetValFsMIY1731LbrCacheSize) \
 nmhSetCmnWithLock(FsMIY1731LbrCacheSize, 14, FsMIY1731LbrCacheSizeSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%u %i", u4FsMIY1731ContextId ,i4SetValFsMIY1731LbrCacheSize)
#define nmhSetFsMIY1731LbrCacheHoldTime(u4FsMIY1731ContextId ,i4SetValFsMIY1731LbrCacheHoldTime) \
 nmhSetCmnWithLock(FsMIY1731LbrCacheHoldTime, 14, FsMIY1731LbrCacheHoldTimeSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%u %i", u4FsMIY1731ContextId ,i4SetValFsMIY1731LbrCacheHoldTime)
#define nmhSetFsMIY1731TrapControl(u4FsMIY1731ContextId ,pSetValFsMIY1731TrapControl) \
 nmhSetCmnWithLock(FsMIY1731TrapControl, 14, FsMIY1731TrapControlSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 1, "%u %s", u4FsMIY1731ContextId ,pSetValFsMIY1731TrapControl)
#define nmhSetFsMIY1731ErrorLogStatus(u4FsMIY1731ContextId ,i4SetValFsMIY1731ErrorLogStatus) \
 nmhSetCmnWithLock(FsMIY1731ErrorLogStatus, 14, FsMIY1731ErrorLogStatusSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 1, "%u %i", u4FsMIY1731ContextId ,i4SetValFsMIY1731ErrorLogStatus)
#define nmhSetFsMIY1731ErrorLogSize(u4FsMIY1731ContextId ,i4SetValFsMIY1731ErrorLogSize) \
 nmhSetCmnWithLock(FsMIY1731ErrorLogSize, 14, FsMIY1731ErrorLogSizeSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 1, "%u %i", u4FsMIY1731ContextId ,i4SetValFsMIY1731ErrorLogSize)
#define nmhSetFsMIY1731OperStatus(u4FsMIY1731ContextId ,i4SetValFsMIY1731OperStatus) \
 nmhSetCmnWithLock(FsMIY1731OperStatus, 14, FsMIY1731OperStatusSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 1, "%u %i", u4FsMIY1731ContextId ,i4SetValFsMIY1731OperStatus)
#define nmhSetFsMIY1731LbrCacheStatus(u4FsMIY1731ContextId ,i4SetValFsMIY1731LbrCacheStatus) \
 nmhSetCmnWithLock(FsMIY1731LbrCacheStatus, 14, FsMIY1731LbrCacheStatusSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%u %i", u4FsMIY1731ContextId ,i4SetValFsMIY1731LbrCacheStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIY1731MegIndex[14];
extern UINT4 FsMIY1731MegClientMEGLevel[14];
extern UINT4 FsMIY1731MegVlanPriority[14];
extern UINT4 FsMIY1731MegDropEnable[14];
extern UINT4 FsMIY1731MegRowStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMIY1731MegClientMEGLevel(u4FsMIY1731ContextId , u4FsMIY1731MegIndex ,i4SetValFsMIY1731MegClientMEGLevel) \
 nmhSetCmnWithLock(FsMIY1731MegClientMEGLevel, 14, FsMIY1731MegClientMEGLevelSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 2, "%u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex ,i4SetValFsMIY1731MegClientMEGLevel)
#define nmhSetFsMIY1731MegVlanPriority(u4FsMIY1731ContextId , u4FsMIY1731MegIndex ,i4SetValFsMIY1731MegVlanPriority) \
 nmhSetCmnWithLock(FsMIY1731MegVlanPriority, 14, FsMIY1731MegVlanPrioritySet, EcfmCcLock, EcfmCcUnLock, 0, 0, 2, "%u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex ,i4SetValFsMIY1731MegVlanPriority)
#define nmhSetFsMIY1731MegDropEnable(u4FsMIY1731ContextId , u4FsMIY1731MegIndex ,i4SetValFsMIY1731MegDropEnable) \
 nmhSetCmnWithLock(FsMIY1731MegDropEnable, 14, FsMIY1731MegDropEnableSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 2, "%u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex ,i4SetValFsMIY1731MegDropEnable)
/* 
 * BELOW ROUTINES ARE INTENTIONALLY MAPPED LIKE THIS TO HANDLE MSR NOTIFICATIONS
 * FOR ECFM AND Y1731 COMMON OBJECTS.
 * 
 * PLEASE DO NOT OVERWRITE.
 *
 */
#define nmhSetFsMIY1731MegRowStatus(u4FsMIY1731ContextId , u4FsMIY1731MegIndex ,i4SetValFsMIY1731MegRowStatus) \
   nmhSetFsMIY1731MegRowStatus(u4FsMIY1731ContextId , u4FsMIY1731MegIndex ,i4SetValFsMIY1731MegRowStatus)
#endif
/* 
 * CHANGE END 
 */ 


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIY1731MeIndex[14];
extern UINT4 FsMIY1731MeCciEnabled[14];
extern UINT4 FsMIY1731MeCcmApplication[14];
extern UINT4 FsMIY1731MeMegIdIcc[14];
extern UINT4 FsMIY1731MeMegIdUmc[14];
extern UINT4 FsMIY1731MeRowStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMIY1731MeCciEnabled(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex ,i4SetValFsMIY1731MeCciEnabled) \
 nmhSetCmnWithLock(FsMIY1731MeCciEnabled, 14, FsMIY1731MeCciEnabledSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 3, "%u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex ,i4SetValFsMIY1731MeCciEnabled)
#define nmhSetFsMIY1731MeCcmApplication(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex ,i4SetValFsMIY1731MeCcmApplication) \
 nmhSetCmnWithLock(FsMIY1731MeCcmApplication, 14, FsMIY1731MeCcmApplicationSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 3, "%u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex ,i4SetValFsMIY1731MeCcmApplication)
#define nmhSetFsMIY1731MeMegIdIcc(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex ,pSetValFsMIY1731MeMegIdIcc) \
 nmhSetCmnWithLock(FsMIY1731MeMegIdIcc, 14, FsMIY1731MeMegIdIccSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 3, "%u %u %u %s", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex ,pSetValFsMIY1731MeMegIdIcc)
#define nmhSetFsMIY1731MeMegIdUmc(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex ,pSetValFsMIY1731MeMegIdUmc) \
 nmhSetCmnWithLock(FsMIY1731MeMegIdUmc, 14, FsMIY1731MeMegIdUmcSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 3, "%u %u %u %s", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex ,pSetValFsMIY1731MeMegIdUmc)
/* 
 * BELOW ROUTINES ARE INTENTIONALLY MAPPED LIKE THIS TO HANDLE MSR NOTIFICATIONS
 * FOR ECFM AND Y1731 COMMON OBJECTS.
 * 
 * PLEASE DO NOT OVERWRITE.
 *
 */
#define nmhSetFsMIY1731MeRowStatus(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex ,i4SetValFsMIY1731MeRowStatus) \
    nmhSetFsMIY1731MeRowStatus(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex ,i4SetValFsMIY1731MeRowStatus)
#endif
/* 
 * CHANGE END 
 */ 


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIY1731MepIdentifier[14];
extern UINT4 FsMIY1731MepOutOfService[14];
extern UINT4 FsMIY1731MepRdiCapability[14];
extern UINT4 FsMIY1731MepRdiPeriod[14];
extern UINT4 FsMIY1731MepCcmDropEnable[14];
extern UINT4 FsMIY1731MepCcmPriority[14];
extern UINT4 FsMIY1731MepMulticastLbmRecvCapability[14];
extern UINT4 FsMIY1731MepLoopbackCapability[14];
extern UINT4 FsMIY1731MepTransmitLbmStatus[14];
extern UINT4 FsMIY1731MepTransmitLbmDestMacAddress[14];
extern UINT4 FsMIY1731MepTransmitLbmDestMepId[14];
extern UINT4 FsMIY1731MepTransmitLbmDestType[14];
extern UINT4 FsMIY1731MepTransmitLbmMessages[14];
extern UINT4 FsMIY1731MepTransmitLbmInterval[14];
extern UINT4 FsMIY1731MepTransmitLbmIntervalType[14];
extern UINT4 FsMIY1731MepTransmitLbmDeadline[14];
extern UINT4 FsMIY1731MepTransmitLbmDropEnable[14];
extern UINT4 FsMIY1731MepTransmitLbmPriority[14];
extern UINT4 FsMIY1731MepTransmitLbmVariableBytes[14];
extern UINT4 FsMIY1731MepTransmitLbmTlvType[14];
extern UINT4 FsMIY1731MepTransmitLbmDataPattern[14];
extern UINT4 FsMIY1731MepTransmitLbmDataPatternSize[14];
extern UINT4 FsMIY1731MepTransmitLbmTestPatternType[14];
extern UINT4 FsMIY1731MepTransmitLbmTestPatternSize[14];
extern UINT4 FsMIY1731MepBitErroredLbrIn[14];
extern UINT4 FsMIY1731MepTransmitLtmStatus[14];
extern UINT4 FsMIY1731MepTransmitLtmFlags[14];
extern UINT4 FsMIY1731MepTransmitLtmTargetMacAddress[14];
extern UINT4 FsMIY1731MepTransmitLtmTargetMepId[14];
extern UINT4 FsMIY1731MepTransmitLtmTargetIsMepId[14];
extern UINT4 FsMIY1731MepTransmitLtmTtl[14];
extern UINT4 FsMIY1731MepTransmitLtmDropEnable[14];
extern UINT4 FsMIY1731MepTransmitLtmPriority[14];
extern UINT4 FsMIY1731MepTransmitLtmTimeout[14];
extern UINT4 FsMIY1731MepMulticastTstRecvCapability[14];
extern UINT4 FsMIY1731MepTransmitTstPatternType[14];
extern UINT4 FsMIY1731MepTransmitTstVariableBytes[14];
extern UINT4 FsMIY1731MepTransmitTstPatternSize[14];
extern UINT4 FsMIY1731MepTransmitTstDestType[14];
extern UINT4 FsMIY1731MepTransmitTstDestMacAddress[14];
extern UINT4 FsMIY1731MepTransmitTstDestMepId[14];
extern UINT4 FsMIY1731MepTransmitTstPriority[14];
extern UINT4 FsMIY1731MepTransmitTstDropEnable[14];
extern UINT4 FsMIY1731MepTransmitTstMessages[14];
extern UINT4 FsMIY1731MepTransmitTstStatus[14];
extern UINT4 FsMIY1731MepTransmitTstInterval[14];
extern UINT4 FsMIY1731MepTransmitTstIntervalType[14];
extern UINT4 FsMIY1731MepTransmitTstDeadline[14];
extern UINT4 FsMIY1731MepBitErroredTstIn[14];
extern UINT4 FsMIY1731MepValidTstIn[14];
extern UINT4 FsMIY1731MepTstOut[14];
extern UINT4 FsMIY1731MepTransmitLmmStatus[14];
extern UINT4 FsMIY1731MepTransmitLmmInterval[14];
extern UINT4 FsMIY1731MepTransmitLmmDeadline[14];
extern UINT4 FsMIY1731MepTransmitLmmDropEnable[14];
extern UINT4 FsMIY1731MepTransmitLmmPriority[14];
extern UINT4 FsMIY1731MepTransmitLmmDestMacAddress[14];
extern UINT4 FsMIY1731MepTransmitLmmDestMepId[14];
extern UINT4 FsMIY1731MepTransmitLmmDestIsMepId[14];
extern UINT4 FsMIY1731MepTransmitLmmMessages[14];
extern UINT4 FsMIY1731MepNearEndFrameLossThreshold[14];
extern UINT4 FsMIY1731MepFarEndFrameLossThreshold[14];
extern UINT4 FsMIY1731MepTransmitDmStatus[14];
extern UINT4 FsMIY1731MepTransmitDmType[14];
extern UINT4 FsMIY1731MepTransmitDmInterval[14];
extern UINT4 FsMIY1731MepTransmitDmMessages[14];
extern UINT4 FsMIY1731MepTransmitDmmDropEnable[14];
extern UINT4 FsMIY1731MepTransmit1DmDropEnable[14];
extern UINT4 FsMIY1731MepTransmitDmmPriority[14];
extern UINT4 FsMIY1731MepTransmit1DmPriority[14];
extern UINT4 FsMIY1731MepTransmitDmDestMacAddress[14];
extern UINT4 FsMIY1731MepTransmitDmDestMepId[14];
extern UINT4 FsMIY1731MepTransmitDmDestIsMepId[14];
extern UINT4 FsMIY1731MepTransmitDmDeadline[14];
extern UINT4 FsMIY1731MepDmrOptionalFields[14];
extern UINT4 FsMIY1731Mep1DmRecvCapability[14];
extern UINT4 FsMIY1731MepFrameDelayThreshold[14];
extern UINT4 FsMIY1731MepAisCapability[14];
extern UINT4 FsMIY1731MepAisInterval[14];
extern UINT4 FsMIY1731MepAisPeriod[14];
extern UINT4 FsMIY1731MepAisPriority[14];
extern UINT4 FsMIY1731MepAisDropEnable[14];
extern UINT4 FsMIY1731MepAisDestIsMulticast[14];
extern UINT4 FsMIY1731MepAisClientMacAddress[14];
extern UINT4 FsMIY1731MepLckDestIsMulticast[14];
extern UINT4 FsMIY1731MepLckClientMacAddress[14];
extern UINT4 FsMIY1731MepLckInterval[14];
extern UINT4 FsMIY1731MepLckPeriod[14];
extern UINT4 FsMIY1731MepLckPriority[14];
extern UINT4 FsMIY1731MepLckDropEnable[14];
extern UINT4 FsMIY1731MepLckDelay[14];
extern UINT4 FsMIY1731MepUnicastCcmMacAddress[14];
extern UINT4 FsMIY1731MepRowStatus[14];
extern UINT4 FsMIY1731Mep1DmTransInterval[14];
extern UINT4 FsMIY1731MepTransmitThDestMacAddress[14];
extern UINT4 FsMIY1731MepTransmitThDestMepId[14];
extern UINT4 FsMIY1731MepTransmitThDestIsMepId[14];
extern UINT4 FsMIY1731MepTransmitThMessages[14];
extern UINT4 FsMIY1731MepTransmitThPps[14];
extern UINT4 FsMIY1731MepTransmitThDeadline[14];
extern UINT4 FsMIY1731MepTransmitThType[14];
extern UINT4 FsMIY1731MepTransmitThStatus[14];
extern UINT4 FsMIY1731MepTransmitThFrameSize[14];
extern UINT4 FsMIY1731MepTransmitThBurstMessages[14];
extern UINT4 FsMIY1731MepTransmitThBurstDeadline[14];
extern UINT4 FsMIY1731MepTransmitThBurstType[14];
extern UINT4 FsMIY1731MepTransmitThTestPatternType[14];
extern UINT4 FsMIY1731MepTransmitLbmMode[14];
extern UINT4 FsMIY1731MepLbmOut[14];
extern UINT4 FsMIY1731MepTransmitLbmTimeout[14];
extern UINT4 FsMIY1731MepTstCapability[14];
extern UINT4 FsMIY1731MepLoopbackStatus[14];
extern UINT4 FsMIY1731MepLmCapability[14];


#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMIY1731MepOutOfService(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepOutOfService) \
 nmhSetCmnWithLock(FsMIY1731MepOutOfService, 14, FsMIY1731MepOutOfServiceSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepOutOfService)
#define nmhSetFsMIY1731MepRdiCapability(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepRdiCapability) \
 nmhSetCmnWithLock(FsMIY1731MepRdiCapability, 14, FsMIY1731MepRdiCapabilitySet, EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepRdiCapability)
#define nmhSetFsMIY1731MepRdiPeriod(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepRdiPeriod) \
 nmhSetCmnWithLock(FsMIY1731MepRdiPeriod, 14, FsMIY1731MepRdiPeriodSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %u", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepRdiPeriod)
#define nmhSetFsMIY1731MepCcmDropEnable(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepCcmDropEnable) \
 nmhSetCmnWithLock(FsMIY1731MepCcmDropEnable, 14, FsMIY1731MepCcmDropEnableSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepCcmDropEnable)
#define nmhSetFsMIY1731MepCcmPriority(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepCcmPriority) \
 nmhSetCmnWithLock(FsMIY1731MepCcmPriority, 14, FsMIY1731MepCcmPrioritySet, EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepCcmPriority)
#define nmhSetFsMIY1731MepMulticastLbmRecvCapability(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepMulticastLbmRecvCapability) \
 nmhSetCmnWithLock(FsMIY1731MepMulticastLbmRecvCapability, 14, FsMIY1731MepMulticastLbmRecvCapabilitySet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepMulticastLbmRecvCapability)
#define nmhSetFsMIY1731MepLoopbackCapability(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepLoopbackCapability) \
 nmhSetCmnWithLock(FsMIY1731MepLoopbackCapability, 14, FsMIY1731MepLoopbackCapabilitySet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepLoopbackCapability)
#define nmhSetFsMIY1731MepTransmitLbmStatus(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLbmStatus) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitLbmStatus, 14, FsMIY1731MepTransmitLbmStatusSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLbmStatus)
#define nmhSetFsMIY1731MepTransmitLbmDestMacAddress(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,SetValFsMIY1731MepTransmitLbmDestMacAddress) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitLbmDestMacAddress, 14, FsMIY1731MepTransmitLbmDestMacAddressSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %m", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,SetValFsMIY1731MepTransmitLbmDestMacAddress)
#define nmhSetFsMIY1731MepTransmitLbmDestMepId(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitLbmDestMepId) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitLbmDestMepId, 14, FsMIY1731MepTransmitLbmDestMepIdSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %u", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitLbmDestMepId)
#define nmhSetFsMIY1731MepTransmitLbmDestType(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLbmDestType) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitLbmDestType, 14, FsMIY1731MepTransmitLbmDestTypeSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLbmDestType)
#define nmhSetFsMIY1731MepTransmitLbmMessages(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLbmMessages) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitLbmMessages, 14, FsMIY1731MepTransmitLbmMessagesSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLbmMessages)
#define nmhSetFsMIY1731MepTransmitLbmInterval(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitLbmInterval) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitLbmInterval, 14, FsMIY1731MepTransmitLbmIntervalSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %u", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitLbmInterval)
#define nmhSetFsMIY1731MepTransmitLbmIntervalType(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLbmIntervalType) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitLbmIntervalType, 14, FsMIY1731MepTransmitLbmIntervalTypeSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLbmIntervalType)
#define nmhSetFsMIY1731MepTransmitLbmDeadline(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitLbmDeadline) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitLbmDeadline, 14, FsMIY1731MepTransmitLbmDeadlineSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %u", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitLbmDeadline)
#define nmhSetFsMIY1731MepTransmitLbmDropEnable(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLbmDropEnable) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitLbmDropEnable, 14, FsMIY1731MepTransmitLbmDropEnableSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLbmDropEnable)
#define nmhSetFsMIY1731MepTransmitLbmPriority(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLbmPriority) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitLbmPriority, 14, FsMIY1731MepTransmitLbmPrioritySet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLbmPriority)
#define nmhSetFsMIY1731MepTransmitLbmVariableBytes(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLbmVariableBytes) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitLbmVariableBytes, 14, FsMIY1731MepTransmitLbmVariableBytesSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLbmVariableBytes)
#define nmhSetFsMIY1731MepTransmitLbmTlvType(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLbmTlvType) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitLbmTlvType, 14, FsMIY1731MepTransmitLbmTlvTypeSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLbmTlvType)
#define nmhSetFsMIY1731MepTransmitLbmDataPattern(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,pSetValFsMIY1731MepTransmitLbmDataPattern) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitLbmDataPattern, 14, FsMIY1731MepTransmitLbmDataPatternSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %s", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,pSetValFsMIY1731MepTransmitLbmDataPattern)
#define nmhSetFsMIY1731MepTransmitLbmDataPatternSize(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitLbmDataPatternSize) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitLbmDataPatternSize, 14, FsMIY1731MepTransmitLbmDataPatternSizeSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %u", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitLbmDataPatternSize)
#define nmhSetFsMIY1731MepTransmitLbmTestPatternType(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLbmTestPatternType) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitLbmTestPatternType, 14, FsMIY1731MepTransmitLbmTestPatternTypeSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLbmTestPatternType)
#define nmhSetFsMIY1731MepTransmitLbmTestPatternSize(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitLbmTestPatternSize) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitLbmTestPatternSize, 14, FsMIY1731MepTransmitLbmTestPatternSizeSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %u", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitLbmTestPatternSize)
#define nmhSetFsMIY1731MepBitErroredLbrIn(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepBitErroredLbrIn) \
 nmhSetCmnWithLock(FsMIY1731MepBitErroredLbrIn, 14, FsMIY1731MepBitErroredLbrInSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %u", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepBitErroredLbrIn)
/* 
 * BELOW ROUTINES ARE INTENTIONALLY MAPPED LIKE THIS TO HANDLE MSR NOTIFICATIONS
 * FOR ECFM AND Y1731 COMMON OBJECTS.
 * 
 * PLEASE DO NOT OVERWRITE.
 *
 */
#define nmhSetFsMIY1731MepTransmitLtmStatus(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLtmStatus) \
    nmhSetFsMIY1731MepTransmitLtmStatus(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLtmStatus)
#define nmhSetFsMIY1731MepTransmitLtmFlags(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,pSetValFsMIY1731MepTransmitLtmFlags) \
    nmhSetFsMIY1731MepTransmitLtmFlags(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,pSetValFsMIY1731MepTransmitLtmFlags)
#define nmhSetFsMIY1731MepTransmitLtmTargetMacAddress(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,SetValFsMIY1731MepTransmitLtmTargetMacAddress) \
    nmhSetFsMIY1731MepTransmitLtmTargetMacAddress(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,SetValFsMIY1731MepTransmitLtmTargetMacAddress)
#define nmhSetFsMIY1731MepTransmitLtmTargetMepId(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitLtmTargetMepId) \
    nmhSetFsMIY1731MepTransmitLtmTargetMepId(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitLtmTargetMepId)
#define nmhSetFsMIY1731MepTransmitLtmTargetIsMepId(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLtmTargetIsMepId) \
  nmhSetFsMIY1731MepTransmitLtmTargetIsMepId(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLtmTargetIsMepId)
#define nmhSetFsMIY1731MepTransmitLtmTtl(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitLtmTtl) \
  nmhSetFsMIY1731MepTransmitLtmTtl(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitLtmTtl)
#define nmhSetFsMIY1731MepTransmitLtmDropEnable(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLtmDropEnable) \
    nmhSetFsMIY1731MepTransmitLtmDropEnable(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLtmDropEnable)
#define nmhSetFsMIY1731MepTransmitLtmPriority(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLtmPriority) \
    nmhSetFsMIY1731MepTransmitLtmPriority(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLtmPriority)
#define nmhSetFsMIY1731MepTransmitLtmTimeout(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLtmTimeout) \
    nmhSetFsMIY1731MepTransmitLtmTimeout(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLtmTimeout)

/* 
 * CHANGE END 
 */ 
#define nmhSetFsMIY1731MepMulticastTstRecvCapability(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepMulticastTstRecvCapability) \
 nmhSetCmnWithLock(FsMIY1731MepMulticastTstRecvCapability, 14, FsMIY1731MepMulticastTstRecvCapabilitySet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepMulticastTstRecvCapability)
#define nmhSetFsMIY1731MepTransmitTstPatternType(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitTstPatternType) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitTstPatternType, 14, FsMIY1731MepTransmitTstPatternTypeSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitTstPatternType)
#define nmhSetFsMIY1731MepTransmitTstVariableBytes(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitTstVariableBytes) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitTstVariableBytes, 14, FsMIY1731MepTransmitTstVariableBytesSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitTstVariableBytes)
#define nmhSetFsMIY1731MepTransmitTstPatternSize(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitTstPatternSize) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitTstPatternSize, 14, FsMIY1731MepTransmitTstPatternSizeSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %u", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitTstPatternSize)
#define nmhSetFsMIY1731MepTransmitTstDestType(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitTstDestType) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitTstDestType, 14, FsMIY1731MepTransmitTstDestTypeSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitTstDestType)
#define nmhSetFsMIY1731MepTransmitTstDestMacAddress(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,SetValFsMIY1731MepTransmitTstDestMacAddress) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitTstDestMacAddress, 14, FsMIY1731MepTransmitTstDestMacAddressSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %m", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,SetValFsMIY1731MepTransmitTstDestMacAddress)
#define nmhSetFsMIY1731MepTransmitTstDestMepId(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitTstDestMepId) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitTstDestMepId, 14, FsMIY1731MepTransmitTstDestMepIdSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %u", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitTstDestMepId)
#define nmhSetFsMIY1731MepTransmitTstPriority(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitTstPriority) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitTstPriority, 14, FsMIY1731MepTransmitTstPrioritySet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitTstPriority)
#define nmhSetFsMIY1731MepTransmitTstDropEnable(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitTstDropEnable) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitTstDropEnable, 14, FsMIY1731MepTransmitTstDropEnableSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitTstDropEnable)
#define nmhSetFsMIY1731MepTransmitTstMessages(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitTstMessages) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitTstMessages, 14, FsMIY1731MepTransmitTstMessagesSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitTstMessages)
#define nmhSetFsMIY1731MepTransmitTstStatus(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitTstStatus) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitTstStatus, 14, FsMIY1731MepTransmitTstStatusSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitTstStatus)
#define nmhSetFsMIY1731MepTransmitTstInterval(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitTstInterval) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitTstInterval, 14, FsMIY1731MepTransmitTstIntervalSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %u", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitTstInterval)
#define nmhSetFsMIY1731MepTransmitTstIntervalType(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitTstIntervalType) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitTstIntervalType, 14, FsMIY1731MepTransmitTstIntervalTypeSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitTstIntervalType)
#define nmhSetFsMIY1731MepTransmitTstDeadline(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitTstDeadline) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitTstDeadline, 14, FsMIY1731MepTransmitTstDeadlineSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %u", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitTstDeadline)
#define nmhSetFsMIY1731MepBitErroredTstIn(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepBitErroredTstIn) \
 nmhSetCmnWithLock(FsMIY1731MepBitErroredTstIn, 14, FsMIY1731MepBitErroredTstInSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %u", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepBitErroredTstIn)
#define nmhSetFsMIY1731MepValidTstIn(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepValidTstIn) \
 nmhSetCmnWithLock(FsMIY1731MepValidTstIn, 14, FsMIY1731MepValidTstInSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %u", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepValidTstIn)
#define nmhSetFsMIY1731MepTstOut(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTstOut) \
 nmhSetCmnWithLock(FsMIY1731MepTstOut, 14, FsMIY1731MepTstOutSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %u", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTstOut)
#define nmhSetFsMIY1731MepTransmitLmmStatus(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLmmStatus) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitLmmStatus, 14, FsMIY1731MepTransmitLmmStatusSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLmmStatus)
#define nmhSetFsMIY1731MepTransmitLmmInterval(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLmmInterval) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitLmmInterval, 14, FsMIY1731MepTransmitLmmIntervalSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLmmInterval)
#define nmhSetFsMIY1731MepTransmitLmmDeadline(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitLmmDeadline) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitLmmDeadline, 14, FsMIY1731MepTransmitLmmDeadlineSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %u", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitLmmDeadline)
#define nmhSetFsMIY1731MepTransmitLmmDropEnable(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLmmDropEnable) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitLmmDropEnable, 14, FsMIY1731MepTransmitLmmDropEnableSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLmmDropEnable)
#define nmhSetFsMIY1731MepTransmitLmmPriority(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLmmPriority) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitLmmPriority, 14, FsMIY1731MepTransmitLmmPrioritySet, EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLmmPriority)
#define nmhSetFsMIY1731MepTransmitLmmDestMacAddress(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,SetValFsMIY1731MepTransmitLmmDestMacAddress) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitLmmDestMacAddress, 14, FsMIY1731MepTransmitLmmDestMacAddressSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %m", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,SetValFsMIY1731MepTransmitLmmDestMacAddress)
#define nmhSetFsMIY1731MepTransmitLmmDestMepId(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitLmmDestMepId) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitLmmDestMepId, 14, FsMIY1731MepTransmitLmmDestMepIdSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %u", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitLmmDestMepId)
#define nmhSetFsMIY1731MepTransmitLmmDestIsMepId(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLmmDestIsMepId) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitLmmDestIsMepId, 14, FsMIY1731MepTransmitLmmDestIsMepIdSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLmmDestIsMepId)
#define nmhSetFsMIY1731MepTransmitLmmMessages(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLmmMessages) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitLmmMessages, 14, FsMIY1731MepTransmitLmmMessagesSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLmmMessages)
#define nmhSetFsMIY1731MepNearEndFrameLossThreshold(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepNearEndFrameLossThreshold) \
 nmhSetCmnWithLock(FsMIY1731MepNearEndFrameLossThreshold, 14, FsMIY1731MepNearEndFrameLossThresholdSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %u", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepNearEndFrameLossThreshold)
#define nmhSetFsMIY1731MepFarEndFrameLossThreshold(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepFarEndFrameLossThreshold) \
 nmhSetCmnWithLock(FsMIY1731MepFarEndFrameLossThreshold, 14, FsMIY1731MepFarEndFrameLossThresholdSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %u", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepFarEndFrameLossThreshold)
#define nmhSetFsMIY1731MepTransmitDmStatus(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitDmStatus) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitDmStatus, 14, FsMIY1731MepTransmitDmStatusSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitDmStatus)
#define nmhSetFsMIY1731MepTransmitDmType(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitDmType) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitDmType, 14, FsMIY1731MepTransmitDmTypeSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitDmType)
#define nmhSetFsMIY1731MepTransmitDmInterval(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitDmInterval) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitDmInterval, 14, FsMIY1731MepTransmitDmIntervalSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitDmInterval)
#define nmhSetFsMIY1731MepTransmitDmMessages(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitDmMessages) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitDmMessages, 14, FsMIY1731MepTransmitDmMessagesSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitDmMessages)
#define nmhSetFsMIY1731MepTransmitDmmDropEnable(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitDmmDropEnable) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitDmmDropEnable, 14, FsMIY1731MepTransmitDmmDropEnableSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitDmmDropEnable)
#define nmhSetFsMIY1731MepTransmit1DmDropEnable(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmit1DmDropEnable) \
 nmhSetCmnWithLock(FsMIY1731MepTransmit1DmDropEnable, 14, FsMIY1731MepTransmit1DmDropEnableSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmit1DmDropEnable)
#define nmhSetFsMIY1731MepTransmitDmmPriority(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitDmmPriority) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitDmmPriority, 14, FsMIY1731MepTransmitDmmPrioritySet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitDmmPriority)
#define nmhSetFsMIY1731MepTransmit1DmPriority(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmit1DmPriority) \
 nmhSetCmnWithLock(FsMIY1731MepTransmit1DmPriority, 14, FsMIY1731MepTransmit1DmPrioritySet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmit1DmPriority)
#define nmhSetFsMIY1731MepTransmitDmDestMacAddress(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,SetValFsMIY1731MepTransmitDmDestMacAddress) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitDmDestMacAddress, 14, FsMIY1731MepTransmitDmDestMacAddressSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %m", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,SetValFsMIY1731MepTransmitDmDestMacAddress)
#define nmhSetFsMIY1731MepTransmitDmDestMepId(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitDmDestMepId) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitDmDestMepId, 14, FsMIY1731MepTransmitDmDestMepIdSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %u", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitDmDestMepId)
#define nmhSetFsMIY1731MepTransmitDmDestIsMepId(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitDmDestIsMepId) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitDmDestIsMepId, 14, FsMIY1731MepTransmitDmDestIsMepIdSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitDmDestIsMepId)
#define nmhSetFsMIY1731MepTransmitDmDeadline(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitDmDeadline) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitDmDeadline, 14, FsMIY1731MepTransmitDmDeadlineSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %u", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitDmDeadline)
#define nmhSetFsMIY1731MepDmrOptionalFields(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepDmrOptionalFields) \
 nmhSetCmnWithLock(FsMIY1731MepDmrOptionalFields, 14, FsMIY1731MepDmrOptionalFieldsSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepDmrOptionalFields)
#define nmhSetFsMIY1731Mep1DmRecvCapability(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731Mep1DmRecvCapability) \
 nmhSetCmnWithLock(FsMIY1731Mep1DmRecvCapability, 14, FsMIY1731Mep1DmRecvCapabilitySet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731Mep1DmRecvCapability)
#define nmhSetFsMIY1731MepFrameDelayThreshold(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepFrameDelayThreshold) \
 nmhSetCmnWithLock(FsMIY1731MepFrameDelayThreshold, 14, FsMIY1731MepFrameDelayThresholdSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %u", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepFrameDelayThreshold)
#define nmhSetFsMIY1731MepAisCapability(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepAisCapability) \
 nmhSetCmnWithLock(FsMIY1731MepAisCapability, 14, FsMIY1731MepAisCapabilitySet, EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepAisCapability)
#define nmhSetFsMIY1731MepAisInterval(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepAisInterval) \
 nmhSetCmnWithLock(FsMIY1731MepAisInterval, 14, FsMIY1731MepAisIntervalSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepAisInterval)
#define nmhSetFsMIY1731MepAisPeriod(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepAisPeriod) \
 nmhSetCmnWithLock(FsMIY1731MepAisPeriod, 14, FsMIY1731MepAisPeriodSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %u", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepAisPeriod)
#define nmhSetFsMIY1731MepAisPriority(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepAisPriority) \
 nmhSetCmnWithLock(FsMIY1731MepAisPriority, 14, FsMIY1731MepAisPrioritySet, EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepAisPriority)
#define nmhSetFsMIY1731MepAisDropEnable(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepAisDropEnable) \
 nmhSetCmnWithLock(FsMIY1731MepAisDropEnable, 14, FsMIY1731MepAisDropEnableSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepAisDropEnable)
#define nmhSetFsMIY1731MepAisDestIsMulticast(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepAisDestIsMulticast) \
 nmhSetCmnWithLock(FsMIY1731MepAisDestIsMulticast, 14, FsMIY1731MepAisDestIsMulticastSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepAisDestIsMulticast)
#define nmhSetFsMIY1731MepAisClientMacAddress(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,SetValFsMIY1731MepAisClientMacAddress) \
 nmhSetCmnWithLock(FsMIY1731MepAisClientMacAddress, 14, FsMIY1731MepAisClientMacAddressSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %m", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,SetValFsMIY1731MepAisClientMacAddress)
#define nmhSetFsMIY1731MepLckDestIsMulticast(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepLckDestIsMulticast) \
 nmhSetCmnWithLock(FsMIY1731MepLckDestIsMulticast, 14, FsMIY1731MepLckDestIsMulticastSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepLckDestIsMulticast)
#define nmhSetFsMIY1731MepLckClientMacAddress(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,SetValFsMIY1731MepLckClientMacAddress) \
 nmhSetCmnWithLock(FsMIY1731MepLckClientMacAddress, 14, FsMIY1731MepLckClientMacAddressSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %m", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,SetValFsMIY1731MepLckClientMacAddress)
#define nmhSetFsMIY1731MepLckInterval(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepLckInterval) \
 nmhSetCmnWithLock(FsMIY1731MepLckInterval, 14, FsMIY1731MepLckIntervalSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepLckInterval)
#define nmhSetFsMIY1731MepLckPeriod(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepLckPeriod) \
 nmhSetCmnWithLock(FsMIY1731MepLckPeriod, 14, FsMIY1731MepLckPeriodSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %u", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepLckPeriod)
#define nmhSetFsMIY1731MepLckPriority(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepLckPriority) \
 nmhSetCmnWithLock(FsMIY1731MepLckPriority, 14, FsMIY1731MepLckPrioritySet, EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %u", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepLckPriority)
#define nmhSetFsMIY1731MepLckDropEnable(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepLckDropEnable) \
 nmhSetCmnWithLock(FsMIY1731MepLckDropEnable, 14, FsMIY1731MepLckDropEnableSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepLckDropEnable)
#define nmhSetFsMIY1731MepLckDelay(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepLckDelay) \
 nmhSetCmnWithLock(FsMIY1731MepLckDelay, 14, FsMIY1731MepLckDelaySet, EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepLckDelay)
#define nmhSetFsMIY1731MepUnicastCcmMacAddress(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,SetValFsMIY1731MepUnicastCcmMacAddress) \
 nmhSetCmnWithLock(FsMIY1731MepUnicastCcmMacAddress, 14, FsMIY1731MepUnicastCcmMacAddressSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %m", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,SetValFsMIY1731MepUnicastCcmMacAddress)
/* 
 * BELOW ROUTINES ARE INTENTIONALLY MAPPED LIKE THIS TO HANDLE MSR NOTIFICATIONS
 * FOR ECFM AND Y1731 COMMON OBJECTS.
 * 
 * PLEASE DO NOT OVERWRITE.
 *
 */

#define nmhSetFsMIY1731MepRowStatus(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepRowStatus) \
   nmhSetFsMIY1731MepRowStatus(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepRowStatus)
/* 
 * CHANGE END 
 */ 
#define nmhSetFsMIY1731Mep1DmTransInterval(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731Mep1DmTransInterval) \
 nmhSetCmnWithLock(FsMIY1731Mep1DmTransInterval, 14, FsMIY1731Mep1DmTransIntervalSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731Mep1DmTransInterval)
#define nmhSetFsMIY1731MepTransmitThDestMacAddress(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,SetValFsMIY1731MepTransmitThDestMacAddress) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitThDestMacAddress, 14, FsMIY1731MepTransmitThDestMacAddressSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %m", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,SetValFsMIY1731MepTransmitThDestMacAddress)
#define nmhSetFsMIY1731MepTransmitThDestMepId(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitThDestMepId) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitThDestMepId, 14, FsMIY1731MepTransmitThDestMepIdSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %u", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitThDestMepId)
#define nmhSetFsMIY1731MepTransmitThDestIsMepId(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitThDestIsMepId) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitThDestIsMepId, 14, FsMIY1731MepTransmitThDestIsMepIdSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitThDestIsMepId)
#define nmhSetFsMIY1731MepTransmitThMessages(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitThMessages) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitThMessages, 14, FsMIY1731MepTransmitThMessagesSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitThMessages)
#define nmhSetFsMIY1731MepTransmitThPps(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitThPps) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitThPps, 14, FsMIY1731MepTransmitThPpsSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %u", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitThPps)
#define nmhSetFsMIY1731MepTransmitThDeadline(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitThDeadline) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitThDeadline, 14, FsMIY1731MepTransmitThDeadlineSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %u", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitThDeadline)
#define nmhSetFsMIY1731MepTransmitThType(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitThType) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitThType, 14, FsMIY1731MepTransmitThTypeSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitThType)
#define nmhSetFsMIY1731MepTransmitThStatus(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitThStatus) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitThStatus, 14, FsMIY1731MepTransmitThStatusSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitThStatus)
#define nmhSetFsMIY1731MepTransmitThFrameSize(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitThFrameSize) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitThFrameSize, 14, FsMIY1731MepTransmitThFrameSizeSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %u", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitThFrameSize)
#define nmhSetFsMIY1731MepTransmitThBurstMessages(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitThBurstMessages) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitThBurstMessages, 14, FsMIY1731MepTransmitThBurstMessagesSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitThBurstMessages)
#define nmhSetFsMIY1731MepTransmitThBurstDeadline(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitThBurstDeadline) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitThBurstDeadline, 14, FsMIY1731MepTransmitThBurstDeadlineSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %u", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepTransmitThBurstDeadline)
#define nmhSetFsMIY1731MepTransmitThBurstType(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitThBurstType) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitThBurstType, 14, FsMIY1731MepTransmitThBurstTypeSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitThBurstType)
#define nmhSetFsMIY1731MepTransmitThTestPatternType(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitThTestPatternType) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitThTestPatternType, 14, FsMIY1731MepTransmitThTestPatternTypeSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitThTestPatternType)
#define nmhSetFsMIY1731MepTransmitLbmMode(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLbmMode) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitLbmMode, 14, FsMIY1731MepTransmitLbmModeSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLbmMode)
#define nmhSetFsMIY1731MepLbmOut(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepLbmOut) \
 nmhSetCmnWithLock(FsMIY1731MepLbmOut, 14, FsMIY1731MepLbmOutSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %u", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,u4SetValFsMIY1731MepLbmOut)
#define nmhSetFsMIY1731MepTransmitLbmTimeout(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLbmTimeout) \
 nmhSetCmnWithLock(FsMIY1731MepTransmitLbmTimeout, 14, FsMIY1731MepTransmitLbmTimeoutSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTransmitLbmTimeout)
#define nmhSetFsMIY1731MepTstCapability(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTstCapability) \
 nmhSetCmnWithLock(FsMIY1731MepTstCapability, 14, FsMIY1731MepTstCapabilitySet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepTstCapability)
#define nmhSetFsMIY1731MepLoopbackStatus(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepLoopbackStatus)    \
    nmhSetCmnWithLock(FsMIY1731MepLoopbackStatus, 14, FsMIY1731MepLoopbackStatusSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepLoopbackStatus)
#define nmhSetFsMIY1731MepLmCapability(u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex , u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepLmCapability)    \
    nmhSetCmnWithLock(FsMIY1731MepLmCapability, 14, FsMIY1731MepLmCapabilitySet, EcfmCcLock, EcfmCcUnLock, 0, 0, 4, "%u %u %u %u %i", u4FsMIY1731ContextId , u4FsMIY1731MegIndex , u4FsMIY1731MeIndex ,u4FsMIY1731MepIdentifier ,i4SetValFsMIY1731MepLmCapability)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIY1731PortIfIndex[14];
extern UINT4 FsMIY1731PortAisOut[14];
extern UINT4 FsMIY1731PortAisIn[14];
extern UINT4 FsMIY1731PortLckOut[14];
extern UINT4 FsMIY1731PortLckIn[14];
extern UINT4 FsMIY1731PortTstOut[14];
extern UINT4 FsMIY1731PortTstIn[14];
extern UINT4 FsMIY1731PortLmmOut[14];
extern UINT4 FsMIY1731PortLmmIn[14];
extern UINT4 FsMIY1731PortLmrOut[14];
extern UINT4 FsMIY1731PortLmrIn[14];
extern UINT4 FsMIY1731Port1DmOut[14];
extern UINT4 FsMIY1731Port1DmIn[14];
extern UINT4 FsMIY1731PortDmmOut[14];
extern UINT4 FsMIY1731PortDmmIn[14];
extern UINT4 FsMIY1731PortDmrOut[14];
extern UINT4 FsMIY1731PortDmrIn[14];
extern UINT4 FsMIY1731PortApsOut[14];
extern UINT4 FsMIY1731PortApsIn[14];
extern UINT4 FsMIY1731PortMccOut[14];
extern UINT4 FsMIY1731PortMccIn[14];
extern UINT4 FsMIY1731PortVsmOut[14];
extern UINT4 FsMIY1731PortVsmIn[14];
extern UINT4 FsMIY1731PortVsrOut[14];
extern UINT4 FsMIY1731PortVsrIn[14];
extern UINT4 FsMIY1731PortExmOut[14];
extern UINT4 FsMIY1731PortExmIn[14];
extern UINT4 FsMIY1731PortExrOut[14];
extern UINT4 FsMIY1731PortExrIn[14];
extern UINT4 FsMIY1731PortOperStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMIY1731PortAisOut(i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortAisOut) \
 nmhSetCmnWithLock(FsMIY1731PortAisOut, 14, FsMIY1731PortAisOutSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 1, "%i %u", i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortAisOut)
#define nmhSetFsMIY1731PortAisIn(i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortAisIn) \
 nmhSetCmnWithLock(FsMIY1731PortAisIn, 14, FsMIY1731PortAisInSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 1, "%i %u", i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortAisIn)
#define nmhSetFsMIY1731PortLckOut(i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortLckOut) \
 nmhSetCmnWithLock(FsMIY1731PortLckOut, 14, FsMIY1731PortLckOutSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 1, "%i %u", i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortLckOut)
#define nmhSetFsMIY1731PortLckIn(i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortLckIn) \
 nmhSetCmnWithLock(FsMIY1731PortLckIn, 14, FsMIY1731PortLckInSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 1, "%i %u", i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortLckIn)
#define nmhSetFsMIY1731PortTstOut(i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortTstOut) \
 nmhSetCmnWithLock(FsMIY1731PortTstOut, 14, FsMIY1731PortTstOutSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%i %u", i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortTstOut)
#define nmhSetFsMIY1731PortTstIn(i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortTstIn) \
 nmhSetCmnWithLock(FsMIY1731PortTstIn, 14, FsMIY1731PortTstInSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%i %u", i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortTstIn)
#define nmhSetFsMIY1731PortLmmOut(i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortLmmOut) \
 nmhSetCmnWithLock(FsMIY1731PortLmmOut, 14, FsMIY1731PortLmmOutSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 1, "%i %u", i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortLmmOut)
#define nmhSetFsMIY1731PortLmmIn(i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortLmmIn) \
 nmhSetCmnWithLock(FsMIY1731PortLmmIn, 14, FsMIY1731PortLmmInSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 1, "%i %u", i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortLmmIn)
#define nmhSetFsMIY1731PortLmrOut(i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortLmrOut) \
 nmhSetCmnWithLock(FsMIY1731PortLmrOut, 14, FsMIY1731PortLmrOutSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 1, "%i %u", i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortLmrOut)
#define nmhSetFsMIY1731PortLmrIn(i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortLmrIn) \
 nmhSetCmnWithLock(FsMIY1731PortLmrIn, 14, FsMIY1731PortLmrInSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 1, "%i %u", i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortLmrIn)
#define nmhSetFsMIY1731Port1DmOut(i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731Port1DmOut) \
 nmhSetCmnWithLock(FsMIY1731Port1DmOut, 14, FsMIY1731Port1DmOutSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%i %u", i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731Port1DmOut)
#define nmhSetFsMIY1731Port1DmIn(i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731Port1DmIn) \
 nmhSetCmnWithLock(FsMIY1731Port1DmIn, 14, FsMIY1731Port1DmInSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%i %u", i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731Port1DmIn)
#define nmhSetFsMIY1731PortDmmOut(i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortDmmOut) \
 nmhSetCmnWithLock(FsMIY1731PortDmmOut, 14, FsMIY1731PortDmmOutSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%i %u", i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortDmmOut)
#define nmhSetFsMIY1731PortDmmIn(i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortDmmIn) \
 nmhSetCmnWithLock(FsMIY1731PortDmmIn, 14, FsMIY1731PortDmmInSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%i %u", i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortDmmIn)
#define nmhSetFsMIY1731PortDmrOut(i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortDmrOut) \
 nmhSetCmnWithLock(FsMIY1731PortDmrOut, 14, FsMIY1731PortDmrOutSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%i %u", i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortDmrOut)
#define nmhSetFsMIY1731PortDmrIn(i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortDmrIn) \
 nmhSetCmnWithLock(FsMIY1731PortDmrIn, 14, FsMIY1731PortDmrInSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%i %u", i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortDmrIn)
#define nmhSetFsMIY1731PortApsOut(i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortApsOut) \
 nmhSetCmnWithLock(FsMIY1731PortApsOut, 14, FsMIY1731PortApsOutSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%i %u", i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortApsOut)
#define nmhSetFsMIY1731PortApsIn(i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortApsIn) \
 nmhSetCmnWithLock(FsMIY1731PortApsIn, 14, FsMIY1731PortApsInSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%i %u", i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortApsIn)
#define nmhSetFsMIY1731PortMccOut(i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortMccOut) \
 nmhSetCmnWithLock(FsMIY1731PortMccOut, 14, FsMIY1731PortMccOutSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%i %u", i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortMccOut)
#define nmhSetFsMIY1731PortMccIn(i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortMccIn) \
 nmhSetCmnWithLock(FsMIY1731PortMccIn, 14, FsMIY1731PortMccInSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%i %u", i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortMccIn)
#define nmhSetFsMIY1731PortVsmOut(i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortVsmOut) \
 nmhSetCmnWithLock(FsMIY1731PortVsmOut, 14, FsMIY1731PortVsmOutSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%i %u", i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortVsmOut)
#define nmhSetFsMIY1731PortVsmIn(i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortVsmIn) \
 nmhSetCmnWithLock(FsMIY1731PortVsmIn, 14, FsMIY1731PortVsmInSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%i %u", i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortVsmIn)
#define nmhSetFsMIY1731PortVsrOut(i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortVsrOut) \
 nmhSetCmnWithLock(FsMIY1731PortVsrOut, 14, FsMIY1731PortVsrOutSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%i %u", i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortVsrOut)
#define nmhSetFsMIY1731PortVsrIn(i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortVsrIn) \
 nmhSetCmnWithLock(FsMIY1731PortVsrIn, 14, FsMIY1731PortVsrInSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%i %u", i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortVsrIn)
#define nmhSetFsMIY1731PortExmOut(i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortExmOut) \
 nmhSetCmnWithLock(FsMIY1731PortExmOut, 14, FsMIY1731PortExmOutSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%i %u", i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortExmOut)
#define nmhSetFsMIY1731PortExmIn(i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortExmIn) \
 nmhSetCmnWithLock(FsMIY1731PortExmIn, 14, FsMIY1731PortExmInSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%i %u", i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortExmIn)
#define nmhSetFsMIY1731PortExrOut(i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortExrOut) \
 nmhSetCmnWithLock(FsMIY1731PortExrOut, 14, FsMIY1731PortExrOutSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%i %u", i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortExrOut)
#define nmhSetFsMIY1731PortExrIn(i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortExrIn) \
 nmhSetCmnWithLock(FsMIY1731PortExrIn, 14, FsMIY1731PortExrInSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%i %u", i4FsMIY1731PortIfIndex ,u4SetValFsMIY1731PortExrIn)
#define nmhSetFsMIY1731PortOperStatus(i4FsMIY1731PortIfIndex ,i4SetValFsMIY1731PortOperStatus) \
 nmhSetCmnWithLock(FsMIY1731PortOperStatus, 14, FsMIY1731PortOperStatusSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 1, "%i %i", i4FsMIY1731PortIfIndex ,i4SetValFsMIY1731PortOperStatus)

#endif

extern UINT4 FsMIY1731AisOut[14];
extern UINT4 FsMIY1731AisIn[14];
extern UINT4 FsMIY1731LckOut[14];
extern UINT4 FsMIY1731LckIn[14];
extern UINT4 FsMIY1731TstOut[14];
extern UINT4 FsMIY1731TstIn[14];
extern UINT4 FsMIY1731LmmOut[14];
extern UINT4 FsMIY1731LmmIn[14];
extern UINT4 FsMIY1731LmrOut[14];
extern UINT4 FsMIY1731LmrIn[14];
extern UINT4 FsMIY17311DmOut[14];
extern UINT4 FsMIY17311DmIn[14];
extern UINT4 FsMIY1731DmmOut[14];
extern UINT4 FsMIY1731DmmIn[14];
extern UINT4 FsMIY1731DmrOut[14];
extern UINT4 FsMIY1731DmrIn[14];
extern UINT4 FsMIY1731ApsOut[14];
extern UINT4 FsMIY1731ApsIn[14];
extern UINT4 FsMIY1731MccOut[14];
extern UINT4 FsMIY1731MccIn[14];
extern UINT4 FsMIY1731VsmOut[14];
extern UINT4 FsMIY1731VsmIn[14];
extern UINT4 FsMIY1731VsrOut[14];
extern UINT4 FsMIY1731VsrIn[14];
extern UINT4 FsMIY1731ExmOut[14];
extern UINT4 FsMIY1731ExmIn[14];
extern UINT4 FsMIY1731ExrOut[14];
extern UINT4 FsMIY1731ExrIn[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMIY1731AisOut(u4FsMIY1731ContextId ,u4SetValFsMIY1731AisOut) \
 nmhSetCmnWithLock(FsMIY1731AisOut, 14, FsMIY1731AisOutSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 1, "%u %u", u4FsMIY1731ContextId ,u4SetValFsMIY1731AisOut)
#define nmhSetFsMIY1731AisIn(u4FsMIY1731ContextId ,u4SetValFsMIY1731AisIn) \
 nmhSetCmnWithLock(FsMIY1731AisIn, 14, FsMIY1731AisInSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 1, "%u %u", u4FsMIY1731ContextId ,u4SetValFsMIY1731AisIn)
#define nmhSetFsMIY1731LckOut(u4FsMIY1731ContextId ,u4SetValFsMIY1731LckOut) \
 nmhSetCmnWithLock(FsMIY1731LckOut, 14, FsMIY1731LckOutSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 1, "%u %u", u4FsMIY1731ContextId ,u4SetValFsMIY1731LckOut)
#define nmhSetFsMIY1731LckIn(u4FsMIY1731ContextId ,u4SetValFsMIY1731LckIn) \
 nmhSetCmnWithLock(FsMIY1731LckIn, 14, FsMIY1731LckInSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 1, "%u %u", u4FsMIY1731ContextId ,u4SetValFsMIY1731LckIn)
#define nmhSetFsMIY1731TstOut(u4FsMIY1731ContextId ,u4SetValFsMIY1731TstOut) \
 nmhSetCmnWithLock(FsMIY1731TstOut, 14, FsMIY1731TstOutSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%u %u", u4FsMIY1731ContextId ,u4SetValFsMIY1731TstOut)
#define nmhSetFsMIY1731TstIn(u4FsMIY1731ContextId ,u4SetValFsMIY1731TstIn) \
 nmhSetCmnWithLock(FsMIY1731TstIn, 14, FsMIY1731TstInSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%u %u", u4FsMIY1731ContextId ,u4SetValFsMIY1731TstIn)
#define nmhSetFsMIY1731LmmOut(u4FsMIY1731ContextId ,u4SetValFsMIY1731LmmOut) \
 nmhSetCmnWithLock(FsMIY1731LmmOut, 14, FsMIY1731LmmOutSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 1, "%u %u", u4FsMIY1731ContextId ,u4SetValFsMIY1731LmmOut)
#define nmhSetFsMIY1731LmmIn(u4FsMIY1731ContextId ,u4SetValFsMIY1731LmmIn) \
 nmhSetCmnWithLock(FsMIY1731LmmIn, 14, FsMIY1731LmmInSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 1, "%u %u", u4FsMIY1731ContextId ,u4SetValFsMIY1731LmmIn)
#define nmhSetFsMIY1731LmrOut(u4FsMIY1731ContextId ,u4SetValFsMIY1731LmrOut) \
 nmhSetCmnWithLock(FsMIY1731LmrOut, 14, FsMIY1731LmrOutSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 1, "%u %u", u4FsMIY1731ContextId ,u4SetValFsMIY1731LmrOut)
#define nmhSetFsMIY1731LmrIn(u4FsMIY1731ContextId ,u4SetValFsMIY1731LmrIn) \
 nmhSetCmnWithLock(FsMIY1731LmrIn, 14, FsMIY1731LmrInSet, EcfmCcLock, EcfmCcUnLock, 0, 0, 1, "%u %u", u4FsMIY1731ContextId ,u4SetValFsMIY1731LmrIn)
#define nmhSetFsMIY17311DmOut(u4FsMIY1731ContextId ,u4SetValFsMIY17311DmOut) \
 nmhSetCmnWithLock(FsMIY17311DmOut, 14, FsMIY17311DmOutSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%u %u", u4FsMIY1731ContextId ,u4SetValFsMIY17311DmOut)
#define nmhSetFsMIY17311DmIn(u4FsMIY1731ContextId ,u4SetValFsMIY17311DmIn) \
 nmhSetCmnWithLock(FsMIY17311DmIn, 14, FsMIY17311DmInSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%u %u", u4FsMIY1731ContextId ,u4SetValFsMIY17311DmIn)
#define nmhSetFsMIY1731DmmOut(u4FsMIY1731ContextId ,u4SetValFsMIY1731DmmOut) \
 nmhSetCmnWithLock(FsMIY1731DmmOut, 14, FsMIY1731DmmOutSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%u %u", u4FsMIY1731ContextId ,u4SetValFsMIY1731DmmOut)
#define nmhSetFsMIY1731DmmIn(u4FsMIY1731ContextId ,u4SetValFsMIY1731DmmIn) \
 nmhSetCmnWithLock(FsMIY1731DmmIn, 14, FsMIY1731DmmInSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%u %u", u4FsMIY1731ContextId ,u4SetValFsMIY1731DmmIn)
#define nmhSetFsMIY1731DmrOut(u4FsMIY1731ContextId ,u4SetValFsMIY1731DmrOut) \
 nmhSetCmnWithLock(FsMIY1731DmrOut, 14, FsMIY1731DmrOutSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%u %u", u4FsMIY1731ContextId ,u4SetValFsMIY1731DmrOut)
#define nmhSetFsMIY1731DmrIn(u4FsMIY1731ContextId ,u4SetValFsMIY1731DmrIn) \
 nmhSetCmnWithLock(FsMIY1731DmrIn, 14, FsMIY1731DmrInSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%u %u", u4FsMIY1731ContextId ,u4SetValFsMIY1731DmrIn)
#define nmhSetFsMIY1731ApsOut(u4FsMIY1731ContextId ,u4SetValFsMIY1731ApsOut) \
 nmhSetCmnWithLock(FsMIY1731ApsOut, 14, FsMIY1731ApsOutSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%u %u", u4FsMIY1731ContextId ,u4SetValFsMIY1731ApsOut)
#define nmhSetFsMIY1731ApsIn(u4FsMIY1731ContextId ,u4SetValFsMIY1731ApsIn) \
 nmhSetCmnWithLock(FsMIY1731ApsIn, 14, FsMIY1731ApsInSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%u %u", u4FsMIY1731ContextId ,u4SetValFsMIY1731ApsIn)
#define nmhSetFsMIY1731MccOut(u4FsMIY1731ContextId ,u4SetValFsMIY1731MccOut) \
 nmhSetCmnWithLock(FsMIY1731MccOut, 14, FsMIY1731MccOutSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%u %u", u4FsMIY1731ContextId ,u4SetValFsMIY1731MccOut)
#define nmhSetFsMIY1731MccIn(u4FsMIY1731ContextId ,u4SetValFsMIY1731MccIn) \
 nmhSetCmnWithLock(FsMIY1731MccIn, 14, FsMIY1731MccInSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%u %u", u4FsMIY1731ContextId ,u4SetValFsMIY1731MccIn)
#define nmhSetFsMIY1731VsmOut(u4FsMIY1731ContextId ,u4SetValFsMIY1731VsmOut) \
 nmhSetCmnWithLock(FsMIY1731VsmOut, 14, FsMIY1731VsmOutSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%u %u", u4FsMIY1731ContextId ,u4SetValFsMIY1731VsmOut)
#define nmhSetFsMIY1731VsmIn(u4FsMIY1731ContextId ,u4SetValFsMIY1731VsmIn) \
 nmhSetCmnWithLock(FsMIY1731VsmIn, 14, FsMIY1731VsmInSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%u %u", u4FsMIY1731ContextId ,u4SetValFsMIY1731VsmIn)
#define nmhSetFsMIY1731VsrOut(u4FsMIY1731ContextId ,u4SetValFsMIY1731VsrOut) \
 nmhSetCmnWithLock(FsMIY1731VsrOut, 14, FsMIY1731VsrOutSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%u %u", u4FsMIY1731ContextId ,u4SetValFsMIY1731VsrOut)
#define nmhSetFsMIY1731VsrIn(u4FsMIY1731ContextId ,u4SetValFsMIY1731VsrIn) \
 nmhSetCmnWithLock(FsMIY1731VsrIn, 14, FsMIY1731VsrInSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%u %u", u4FsMIY1731ContextId ,u4SetValFsMIY1731VsrIn)
#define nmhSetFsMIY1731ExmOut(u4FsMIY1731ContextId ,u4SetValFsMIY1731ExmOut) \
 nmhSetCmnWithLock(FsMIY1731ExmOut, 14, FsMIY1731ExmOutSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%u %u", u4FsMIY1731ContextId ,u4SetValFsMIY1731ExmOut)
#define nmhSetFsMIY1731ExmIn(u4FsMIY1731ContextId ,u4SetValFsMIY1731ExmIn) \
 nmhSetCmnWithLock(FsMIY1731ExmIn, 14, FsMIY1731ExmInSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%u %u", u4FsMIY1731ContextId ,u4SetValFsMIY1731ExmIn)
#define nmhSetFsMIY1731ExrOut(u4FsMIY1731ContextId ,u4SetValFsMIY1731ExrOut) \
 nmhSetCmnWithLock(FsMIY1731ExrOut, 14, FsMIY1731ExrOutSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%u %u", u4FsMIY1731ContextId ,u4SetValFsMIY1731ExrOut)
#define nmhSetFsMIY1731ExrIn(u4FsMIY1731ContextId ,u4SetValFsMIY1731ExrIn) \
 nmhSetCmnWithLock(FsMIY1731ExrIn, 14, FsMIY1731ExrInSet, EcfmLbLtLock, EcfmLbLtUnLock, 0, 0, 1, "%u %u", u4FsMIY1731ContextId ,u4SetValFsMIY1731ExrIn)

#endif
