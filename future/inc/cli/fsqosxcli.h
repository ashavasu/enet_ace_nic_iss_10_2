/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsqosxcli.h,v 1.7 2015/12/29 12:08:13 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsQoSSystemControl[12];
extern UINT4 FsQoSStatus[12];
extern UINT4 FsQoSTrcFlag[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsQoSSystemControl(i4SetValFsQoSSystemControl) \
 nmhSetCmnWithLock(FsQoSSystemControl, 12, FsQoSSystemControlSet, QoSLock, QoSUnLock, 0, 0, 0, "%i", i4SetValFsQoSSystemControl)
#define nmhSetFsQoSStatus(i4SetValFsQoSStatus) \
 nmhSetCmnWithLock(FsQoSStatus, 12, FsQoSStatusSet, QoSLock, QoSUnLock, 0, 0, 0, "%i", i4SetValFsQoSStatus)
#define nmhSetFsQoSTrcFlag(u4SetValFsQoSTrcFlag) \
 nmhSetCmnWithLock(FsQoSTrcFlag, 12, FsQoSTrcFlagSet, QoSLock, QoSUnLock, 0, 0, 0, "%u", u4SetValFsQoSTrcFlag)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsQoSPriorityMapID[14];
extern UINT4 FsQoSPriorityMapName[14];
extern UINT4 FsQoSPriorityMapIfIndex[14];
extern UINT4 FsQoSPriorityMapVlanId[14];
extern UINT4 FsQoSPriorityMapInPriority[14];
extern UINT4 FsQoSPriorityMapInPriType[14];
extern UINT4 FsQoSPriorityMapRegenPriority[14];
extern UINT4 FsQoSPriorityMapRegenInnerPriority[14];
extern UINT4 FsQoSPriorityMapStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsQoSPriorityMapName(u4FsQoSPriorityMapID ,pSetValFsQoSPriorityMapName) \
 nmhSetCmnWithLock(FsQoSPriorityMapName, 14, FsQoSPriorityMapNameSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %s", u4FsQoSPriorityMapID ,pSetValFsQoSPriorityMapName)
#define nmhSetFsQoSPriorityMapIfIndex(u4FsQoSPriorityMapID ,u4SetValFsQoSPriorityMapIfIndex) \
 nmhSetCmnWithLock(FsQoSPriorityMapIfIndex, 14, FsQoSPriorityMapIfIndexSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSPriorityMapID ,u4SetValFsQoSPriorityMapIfIndex)
#define nmhSetFsQoSPriorityMapVlanId(u4FsQoSPriorityMapID ,u4SetValFsQoSPriorityMapVlanId) \
 nmhSetCmnWithLock(FsQoSPriorityMapVlanId, 14, FsQoSPriorityMapVlanIdSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSPriorityMapID ,u4SetValFsQoSPriorityMapVlanId)
#define nmhSetFsQoSPriorityMapInPriority(u4FsQoSPriorityMapID ,i4SetValFsQoSPriorityMapInPriority) \
 nmhSetCmnWithLock(FsQoSPriorityMapInPriority, 14, FsQoSPriorityMapInPrioritySet, QoSLock, QoSUnLock, 0, 0, 1, "%u %i", u4FsQoSPriorityMapID ,i4SetValFsQoSPriorityMapInPriority)
#define nmhSetFsQoSPriorityMapInPriType(u4FsQoSPriorityMapID ,i4SetValFsQoSPriorityMapInPriType) \
 nmhSetCmnWithLock(FsQoSPriorityMapInPriType, 14, FsQoSPriorityMapInPriTypeSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %i", u4FsQoSPriorityMapID ,i4SetValFsQoSPriorityMapInPriType)
#define nmhSetFsQoSPriorityMapRegenPriority(u4FsQoSPriorityMapID ,u4SetValFsQoSPriorityMapRegenPriority) \
 nmhSetCmnWithLock(FsQoSPriorityMapRegenPriority, 14, FsQoSPriorityMapRegenPrioritySet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSPriorityMapID ,u4SetValFsQoSPriorityMapRegenPriority)
#define nmhSetFsQoSPriorityMapRegenInnerPriority(u4FsQoSPriorityMapID ,u4SetValFsQoSPriorityMapRegenInnerPriority) \
 nmhSetCmnWithLock(FsQoSPriorityMapRegenInnerPriority, 14, FsQoSPriorityMapRegenInnerPrioritySet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSPriorityMapID ,u4SetValFsQoSPriorityMapRegenInnerPriority)
#define nmhSetFsQoSPriorityMapStatus(u4FsQoSPriorityMapID ,i4SetValFsQoSPriorityMapStatus) \
 nmhSetCmnWithLock(FsQoSPriorityMapStatus, 14, FsQoSPriorityMapStatusSet, QoSLock, QoSUnLock, 0, 1, 1, "%u %i", u4FsQoSPriorityMapID ,i4SetValFsQoSPriorityMapStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsQoSClassMapId[14];
extern UINT4 FsQoSClassMapName[14];
extern UINT4 FsQoSClassMapL2FilterId[14];
extern UINT4 FsQoSClassMapL3FilterId[14];
extern UINT4 FsQoSClassMapPriorityMapId[14];
extern UINT4 FsQoSClassMapCLASS[14];
extern UINT4 FsQoSClassMapClfrId[14];
extern UINT4 FsQoSClassMapPreColor[14];
extern UINT4 FsQoSClassMapStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsQoSClassMapName(u4FsQoSClassMapId ,pSetValFsQoSClassMapName) \
 nmhSetCmnWithLock(FsQoSClassMapName, 14, FsQoSClassMapNameSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %s", u4FsQoSClassMapId ,pSetValFsQoSClassMapName)
#define nmhSetFsQoSClassMapL2FilterId(u4FsQoSClassMapId ,u4SetValFsQoSClassMapL2FilterId) \
 nmhSetCmnWithLock(FsQoSClassMapL2FilterId, 14, FsQoSClassMapL2FilterIdSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSClassMapId ,u4SetValFsQoSClassMapL2FilterId)
#define nmhSetFsQoSClassMapL3FilterId(u4FsQoSClassMapId ,u4SetValFsQoSClassMapL3FilterId) \
 nmhSetCmnWithLock(FsQoSClassMapL3FilterId, 14, FsQoSClassMapL3FilterIdSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSClassMapId ,u4SetValFsQoSClassMapL3FilterId)
#define nmhSetFsQoSClassMapPriorityMapId(u4FsQoSClassMapId ,u4SetValFsQoSClassMapPriorityMapId) \
 nmhSetCmnWithLock(FsQoSClassMapPriorityMapId, 14, FsQoSClassMapPriorityMapIdSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSClassMapId ,u4SetValFsQoSClassMapPriorityMapId)
#define nmhSetFsQoSClassMapCLASS(u4FsQoSClassMapId ,u4SetValFsQoSClassMapCLASS) \
 nmhSetCmnWithLock(FsQoSClassMapCLASS, 14, FsQoSClassMapCLASSSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSClassMapId ,u4SetValFsQoSClassMapCLASS)
#define nmhSetFsQoSClassMapClfrId(u4FsQoSClassMapId ,u4SetValFsQoSClassMapClfrId) \
 nmhSetCmnWithLock(FsQoSClassMapClfrId, 14, FsQoSClassMapClfrIdSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSClassMapId ,u4SetValFsQoSClassMapClfrId)
#define nmhSetFsQoSClassMapPreColor(u4FsQoSClassMapId ,i4SetValFsQoSClassMapPreColor) \
 nmhSetCmnWithLock(FsQoSClassMapPreColor, 14, FsQoSClassMapPreColorSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %i", u4FsQoSClassMapId ,i4SetValFsQoSClassMapPreColor)
#define nmhSetFsQoSClassMapStatus(u4FsQoSClassMapId ,i4SetValFsQoSClassMapStatus) \
 nmhSetCmnWithLock(FsQoSClassMapStatus, 14, FsQoSClassMapStatusSet, QoSLock, QoSUnLock, 0, 1, 1, "%u %i", u4FsQoSClassMapId ,i4SetValFsQoSClassMapStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsQoSClassToPriorityCLASS[14];
extern UINT4 FsQoSClassToPriorityRegenPri[14];
extern UINT4 FsQoSClassToPriorityGroupName[14];
extern UINT4 FsQoSClassToPriorityStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsQoSClassToPriorityRegenPri(u4FsQoSClassToPriorityCLASS ,u4SetValFsQoSClassToPriorityRegenPri) \
 nmhSetCmnWithLock(FsQoSClassToPriorityRegenPri, 14, FsQoSClassToPriorityRegenPriSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSClassToPriorityCLASS ,u4SetValFsQoSClassToPriorityRegenPri)
#define nmhSetFsQoSClassToPriorityGroupName(u4FsQoSClassToPriorityCLASS ,pSetValFsQoSClassToPriorityGroupName) \
 nmhSetCmnWithLock(FsQoSClassToPriorityGroupName, 14, FsQoSClassToPriorityGroupNameSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %s", u4FsQoSClassToPriorityCLASS ,pSetValFsQoSClassToPriorityGroupName)
#define nmhSetFsQoSClassToPriorityStatus(u4FsQoSClassToPriorityCLASS ,i4SetValFsQoSClassToPriorityStatus) \
 nmhSetCmnWithLock(FsQoSClassToPriorityStatus, 14, FsQoSClassToPriorityStatusSet, QoSLock, QoSUnLock, 0, 1, 1, "%u %i", u4FsQoSClassToPriorityCLASS ,i4SetValFsQoSClassToPriorityStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsQoSMeterId[14];
extern UINT4 FsQoSMeterName[14];
extern UINT4 FsQoSMeterType[14];
extern UINT4 FsQoSMeterInterval[14];
extern UINT4 FsQoSMeterColorMode[14];
extern UINT4 FsQoSMeterCIR[14];
extern UINT4 FsQoSMeterCBS[14];
extern UINT4 FsQoSMeterEIR[14];
extern UINT4 FsQoSMeterEBS[14];
extern UINT4 FsQoSMeterNext[14];
extern UINT4 FsQoSMeterStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsQoSMeterName(u4FsQoSMeterId ,pSetValFsQoSMeterName) \
 nmhSetCmnWithLock(FsQoSMeterName, 14, FsQoSMeterNameSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %s", u4FsQoSMeterId ,pSetValFsQoSMeterName)
#define nmhSetFsQoSMeterType(u4FsQoSMeterId ,i4SetValFsQoSMeterType) \
 nmhSetCmnWithLock(FsQoSMeterType, 14, FsQoSMeterTypeSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %i", u4FsQoSMeterId ,i4SetValFsQoSMeterType)
#define nmhSetFsQoSMeterInterval(u4FsQoSMeterId ,u4SetValFsQoSMeterInterval) \
 nmhSetCmnWithLock(FsQoSMeterInterval, 14, FsQoSMeterIntervalSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSMeterId ,u4SetValFsQoSMeterInterval)
#define nmhSetFsQoSMeterColorMode(u4FsQoSMeterId ,i4SetValFsQoSMeterColorMode) \
 nmhSetCmnWithLock(FsQoSMeterColorMode, 14, FsQoSMeterColorModeSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %i", u4FsQoSMeterId ,i4SetValFsQoSMeterColorMode)
#define nmhSetFsQoSMeterCIR(u4FsQoSMeterId ,u4SetValFsQoSMeterCIR) \
 nmhSetCmnWithLock(FsQoSMeterCIR, 14, FsQoSMeterCIRSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSMeterId ,u4SetValFsQoSMeterCIR)
#define nmhSetFsQoSMeterCBS(u4FsQoSMeterId ,u4SetValFsQoSMeterCBS) \
 nmhSetCmnWithLock(FsQoSMeterCBS, 14, FsQoSMeterCBSSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSMeterId ,u4SetValFsQoSMeterCBS)
#define nmhSetFsQoSMeterEIR(u4FsQoSMeterId ,u4SetValFsQoSMeterEIR) \
 nmhSetCmnWithLock(FsQoSMeterEIR, 14, FsQoSMeterEIRSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSMeterId ,u4SetValFsQoSMeterEIR)
#define nmhSetFsQoSMeterEBS(u4FsQoSMeterId ,u4SetValFsQoSMeterEBS) \
 nmhSetCmnWithLock(FsQoSMeterEBS, 14, FsQoSMeterEBSSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSMeterId ,u4SetValFsQoSMeterEBS)
#define nmhSetFsQoSMeterNext(u4FsQoSMeterId ,u4SetValFsQoSMeterNext) \
 nmhSetCmnWithLock(FsQoSMeterNext, 14, FsQoSMeterNextSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSMeterId ,u4SetValFsQoSMeterNext)
#define nmhSetFsQoSMeterStatus(u4FsQoSMeterId ,i4SetValFsQoSMeterStatus) \
 nmhSetCmnWithLock(FsQoSMeterStatus, 14, FsQoSMeterStatusSet, QoSLock, QoSUnLock, 0, 1, 1, "%u %i", u4FsQoSMeterId ,i4SetValFsQoSMeterStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsQoSPolicyMapId[14];
extern UINT4 FsQoSPolicyMapName[14];
extern UINT4 FsQoSPolicyMapIfIndex[14];
extern UINT4 FsQoSPolicyMapCLASS[14];
extern UINT4 FsQoSPolicyMapPHBType[14];
extern UINT4 FsQoSPolicyMapDefaultPHB[14];
extern UINT4 FsQoSPolicyMapMeterTableId[14];
extern UINT4 FsQoSPolicyMapFailMeterTableId[14];
extern UINT4 FsQoSPolicyMapInProfileConformActionFlag[14];
extern UINT4 FsQoSPolicyMapInProfileConformActionId[14];
extern UINT4 FsQoSPolicyMapInProfileActionSetPort[14];
extern UINT4 FsQoSPolicyMapConformActionSetIpTOS[14];
extern UINT4 FsQoSPolicyMapConformActionSetDscp[14];
extern UINT4 FsQoSPolicyMapConformActionSetVlanPrio[14];
extern UINT4 FsQoSPolicyMapConformActionSetVlanDE[14];
extern UINT4 FsQoSPolicyMapConformActionSetInnerVlanPrio[14];
extern UINT4 FsQoSPolicyMapConformActionSetMplsExp[14];
extern UINT4 FsQoSPolicyMapConformActionSetNewCLASS[14];
extern UINT4 FsQoSPolicyMapInProfileExceedActionFlag[14];
extern UINT4 FsQoSPolicyMapInProfileExceedActionId[14];
extern UINT4 FsQoSPolicyMapExceedActionSetIpTOS[14];
extern UINT4 FsQoSPolicyMapExceedActionSetDscp[14];
extern UINT4 FsQoSPolicyMapExceedActionSetInnerVlanPrio[14];
extern UINT4 FsQoSPolicyMapExceedActionSetVlanPrio[14];
extern UINT4 FsQoSPolicyMapExceedActionSetVlanDE[14];
extern UINT4 FsQoSPolicyMapExceedActionSetMplsExp[14];
extern UINT4 FsQoSPolicyMapExceedActionSetNewCLASS[14];
extern UINT4 FsQoSPolicyMapOutProfileActionFlag[14];
extern UINT4 FsQoSPolicyMapOutProfileActionId[14];
extern UINT4 FsQoSPolicyMapOutProfileActionSetIPTOS[14];
extern UINT4 FsQoSPolicyMapOutProfileActionSetDscp[14];
extern UINT4 FsQoSPolicyMapOutProfileActionSetInnerVlanPrio[14];
extern UINT4 FsQoSPolicyMapOutProfileActionSetVlanPrio[14];
extern UINT4 FsQoSPolicyMapOutProfileActionSetVlanDE[14];
extern UINT4 FsQoSPolicyMapOutProfileActionSetMplsExp[14];
extern UINT4 FsQoSPolicyMapOutProfileActionSetNewCLASS[14];
extern UINT4 FsQoSPolicyMapStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsQoSPolicyMapName(u4FsQoSPolicyMapId ,pSetValFsQoSPolicyMapName) \
 nmhSetCmnWithLock(FsQoSPolicyMapName, 14, FsQoSPolicyMapNameSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %s", u4FsQoSPolicyMapId ,pSetValFsQoSPolicyMapName)
#define nmhSetFsQoSPolicyMapIfIndex(u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapIfIndex) \
 nmhSetCmnWithLock(FsQoSPolicyMapIfIndex, 14, FsQoSPolicyMapIfIndexSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapIfIndex)
#define nmhSetFsQoSPolicyMapCLASS(u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapCLASS) \
 nmhSetCmnWithLock(FsQoSPolicyMapCLASS, 14, FsQoSPolicyMapCLASSSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapCLASS)
#define nmhSetFsQoSPolicyMapPHBType(u4FsQoSPolicyMapId ,i4SetValFsQoSPolicyMapPHBType) \
 nmhSetCmnWithLock(FsQoSPolicyMapPHBType, 14, FsQoSPolicyMapPHBTypeSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %i", u4FsQoSPolicyMapId ,i4SetValFsQoSPolicyMapPHBType)
#define nmhSetFsQoSPolicyMapDefaultPHB(u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapDefaultPHB) \
 nmhSetCmnWithLock(FsQoSPolicyMapDefaultPHB, 14, FsQoSPolicyMapDefaultPHBSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapDefaultPHB)
#define nmhSetFsQoSPolicyMapMeterTableId(u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapMeterTableId) \
 nmhSetCmnWithLock(FsQoSPolicyMapMeterTableId, 14, FsQoSPolicyMapMeterTableIdSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapMeterTableId)
#define nmhSetFsQoSPolicyMapFailMeterTableId(u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapFailMeterTableId) \
 nmhSetCmnWithLock(FsQoSPolicyMapFailMeterTableId, 14, FsQoSPolicyMapFailMeterTableIdSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapFailMeterTableId)
#define nmhSetFsQoSPolicyMapInProfileConformActionFlag(u4FsQoSPolicyMapId ,pSetValFsQoSPolicyMapInProfileConformActionFlag) \
 nmhSetCmnWithLock(FsQoSPolicyMapInProfileConformActionFlag, 14, FsQoSPolicyMapInProfileConformActionFlagSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %s", u4FsQoSPolicyMapId ,pSetValFsQoSPolicyMapInProfileConformActionFlag)
#define nmhSetFsQoSPolicyMapInProfileConformActionId(u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapInProfileConformActionId) \
 nmhSetCmnWithLock(FsQoSPolicyMapInProfileConformActionId, 14, FsQoSPolicyMapInProfileConformActionIdSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapInProfileConformActionId)
#define nmhSetFsQoSPolicyMapInProfileActionSetPort(u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapInProfileActionSetPort) \
 nmhSetCmnWithLock(FsQoSPolicyMapInProfileActionSetPort, 14, FsQoSPolicyMapInProfileActionSetPortSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapInProfileActionSetPort)
#define nmhSetFsQoSPolicyMapConformActionSetIpTOS(u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapConformActionSetIpTOS) \
 nmhSetCmnWithLock(FsQoSPolicyMapConformActionSetIpTOS, 14, FsQoSPolicyMapConformActionSetIpTOSSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapConformActionSetIpTOS)
#define nmhSetFsQoSPolicyMapConformActionSetDscp(u4FsQoSPolicyMapId ,i4SetValFsQoSPolicyMapConformActionSetDscp) \
 nmhSetCmnWithLock(FsQoSPolicyMapConformActionSetDscp, 14, FsQoSPolicyMapConformActionSetDscpSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %i", u4FsQoSPolicyMapId ,i4SetValFsQoSPolicyMapConformActionSetDscp)
#define nmhSetFsQoSPolicyMapConformActionSetVlanPrio(u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapConformActionSetVlanPrio) \
 nmhSetCmnWithLock(FsQoSPolicyMapConformActionSetVlanPrio, 14, FsQoSPolicyMapConformActionSetVlanPrioSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapConformActionSetVlanPrio)
#define nmhSetFsQoSPolicyMapConformActionSetVlanDE(u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapConformActionSetVlanDE) \
 nmhSetCmnWithLock(FsQoSPolicyMapConformActionSetVlanDE, 14, FsQoSPolicyMapConformActionSetVlanDESet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapConformActionSetVlanDE)
#define nmhSetFsQoSPolicyMapConformActionSetInnerVlanPrio(u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapConformActionSetInnerVlanPrio) \
 nmhSetCmnWithLock(FsQoSPolicyMapConformActionSetInnerVlanPrio, 14, FsQoSPolicyMapConformActionSetInnerVlanPrioSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapConformActionSetInnerVlanPrio)
#define nmhSetFsQoSPolicyMapConformActionSetMplsExp(u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapConformActionSetMplsExp) \
 nmhSetCmnWithLock(FsQoSPolicyMapConformActionSetMplsExp, 14, FsQoSPolicyMapConformActionSetMplsExpSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapConformActionSetMplsExp)
#define nmhSetFsQoSPolicyMapConformActionSetNewCLASS(u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapConformActionSetNewCLASS) \
 nmhSetCmnWithLock(FsQoSPolicyMapConformActionSetNewCLASS, 14, FsQoSPolicyMapConformActionSetNewCLASSSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapConformActionSetNewCLASS)
#define nmhSetFsQoSPolicyMapInProfileExceedActionFlag(u4FsQoSPolicyMapId ,pSetValFsQoSPolicyMapInProfileExceedActionFlag) \
 nmhSetCmnWithLock(FsQoSPolicyMapInProfileExceedActionFlag, 14, FsQoSPolicyMapInProfileExceedActionFlagSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %s", u4FsQoSPolicyMapId ,pSetValFsQoSPolicyMapInProfileExceedActionFlag)
#define nmhSetFsQoSPolicyMapInProfileExceedActionId(u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapInProfileExceedActionId) \
 nmhSetCmnWithLock(FsQoSPolicyMapInProfileExceedActionId, 14, FsQoSPolicyMapInProfileExceedActionIdSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapInProfileExceedActionId)
#define nmhSetFsQoSPolicyMapExceedActionSetIpTOS(u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapExceedActionSetIpTOS) \
 nmhSetCmnWithLock(FsQoSPolicyMapExceedActionSetIpTOS, 14, FsQoSPolicyMapExceedActionSetIpTOSSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapExceedActionSetIpTOS)
#define nmhSetFsQoSPolicyMapExceedActionSetDscp(u4FsQoSPolicyMapId ,i4SetValFsQoSPolicyMapExceedActionSetDscp) \
 nmhSetCmnWithLock(FsQoSPolicyMapExceedActionSetDscp, 14, FsQoSPolicyMapExceedActionSetDscpSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %i", u4FsQoSPolicyMapId ,i4SetValFsQoSPolicyMapExceedActionSetDscp)
#define nmhSetFsQoSPolicyMapExceedActionSetInnerVlanPrio(u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapExceedActionSetInnerVlanPrio) \
 nmhSetCmnWithLock(FsQoSPolicyMapExceedActionSetInnerVlanPrio, 14, FsQoSPolicyMapExceedActionSetInnerVlanPrioSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapExceedActionSetInnerVlanPrio)
#define nmhSetFsQoSPolicyMapExceedActionSetVlanPrio(u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapExceedActionSetVlanPrio) \
 nmhSetCmnWithLock(FsQoSPolicyMapExceedActionSetVlanPrio, 14, FsQoSPolicyMapExceedActionSetVlanPrioSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapExceedActionSetVlanPrio)
#define nmhSetFsQoSPolicyMapExceedActionSetVlanDE(u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapExceedActionSetVlanDE) \
 nmhSetCmnWithLock(FsQoSPolicyMapExceedActionSetVlanDE, 14, FsQoSPolicyMapExceedActionSetVlanDESet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapExceedActionSetVlanDE)
#define nmhSetFsQoSPolicyMapExceedActionSetMplsExp(u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapExceedActionSetMplsExp) \
 nmhSetCmnWithLock(FsQoSPolicyMapExceedActionSetMplsExp, 14, FsQoSPolicyMapExceedActionSetMplsExpSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapExceedActionSetMplsExp)
#define nmhSetFsQoSPolicyMapExceedActionSetNewCLASS(u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapExceedActionSetNewCLASS) \
 nmhSetCmnWithLock(FsQoSPolicyMapExceedActionSetNewCLASS, 14, FsQoSPolicyMapExceedActionSetNewCLASSSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapExceedActionSetNewCLASS)
#define nmhSetFsQoSPolicyMapOutProfileActionFlag(u4FsQoSPolicyMapId ,pSetValFsQoSPolicyMapOutProfileActionFlag) \
 nmhSetCmnWithLock(FsQoSPolicyMapOutProfileActionFlag, 14, FsQoSPolicyMapOutProfileActionFlagSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %s", u4FsQoSPolicyMapId ,pSetValFsQoSPolicyMapOutProfileActionFlag)
#define nmhSetFsQoSPolicyMapOutProfileActionId(u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapOutProfileActionId) \
 nmhSetCmnWithLock(FsQoSPolicyMapOutProfileActionId, 14, FsQoSPolicyMapOutProfileActionIdSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapOutProfileActionId)
#define nmhSetFsQoSPolicyMapOutProfileActionSetIPTOS(u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapOutProfileActionSetIPTOS) \
 nmhSetCmnWithLock(FsQoSPolicyMapOutProfileActionSetIPTOS, 14, FsQoSPolicyMapOutProfileActionSetIPTOSSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapOutProfileActionSetIPTOS)
#define nmhSetFsQoSPolicyMapOutProfileActionSetDscp(u4FsQoSPolicyMapId ,i4SetValFsQoSPolicyMapOutProfileActionSetDscp) \
 nmhSetCmnWithLock(FsQoSPolicyMapOutProfileActionSetDscp, 14, FsQoSPolicyMapOutProfileActionSetDscpSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %i", u4FsQoSPolicyMapId ,i4SetValFsQoSPolicyMapOutProfileActionSetDscp)
#define nmhSetFsQoSPolicyMapOutProfileActionSetInnerVlanPrio(u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapOutProfileActionSetInnerVlanPrio) \
 nmhSetCmnWithLock(FsQoSPolicyMapOutProfileActionSetInnerVlanPrio, 14, FsQoSPolicyMapOutProfileActionSetInnerVlanPrioSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapOutProfileActionSetInnerVlanPrio)
#define nmhSetFsQoSPolicyMapOutProfileActionSetVlanPrio(u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapOutProfileActionSetVlanPrio) \
 nmhSetCmnWithLock(FsQoSPolicyMapOutProfileActionSetVlanPrio, 14, FsQoSPolicyMapOutProfileActionSetVlanPrioSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapOutProfileActionSetVlanPrio)
#define nmhSetFsQoSPolicyMapOutProfileActionSetVlanDE(u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapOutProfileActionSetVlanDE) \
 nmhSetCmnWithLock(FsQoSPolicyMapOutProfileActionSetVlanDE, 14, FsQoSPolicyMapOutProfileActionSetVlanDESet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapOutProfileActionSetVlanDE)
#define nmhSetFsQoSPolicyMapOutProfileActionSetMplsExp(u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapOutProfileActionSetMplsExp) \
 nmhSetCmnWithLock(FsQoSPolicyMapOutProfileActionSetMplsExp, 14, FsQoSPolicyMapOutProfileActionSetMplsExpSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapOutProfileActionSetMplsExp)
#define nmhSetFsQoSPolicyMapOutProfileActionSetNewCLASS(u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapOutProfileActionSetNewCLASS) \
 nmhSetCmnWithLock(FsQoSPolicyMapOutProfileActionSetNewCLASS, 14, FsQoSPolicyMapOutProfileActionSetNewCLASSSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSPolicyMapId ,u4SetValFsQoSPolicyMapOutProfileActionSetNewCLASS)
#define nmhSetFsQoSPolicyMapStatus(u4FsQoSPolicyMapId ,i4SetValFsQoSPolicyMapStatus) \
 nmhSetCmnWithLock(FsQoSPolicyMapStatus, 14, FsQoSPolicyMapStatusSet, QoSLock, QoSUnLock, 0, 1, 1, "%u %i", u4FsQoSPolicyMapId ,i4SetValFsQoSPolicyMapStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsQoSQTemplateId[14];
extern UINT4 FsQoSQTemplateName[14];
extern UINT4 FsQoSQTemplateDropType[14];
extern UINT4 FsQoSQTemplateDropAlgoEnableFlag[14];
extern UINT4 FsQoSQTemplateSize[14];
extern UINT4 FsQoSQTemplateStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsQoSQTemplateName(u4FsQoSQTemplateId ,pSetValFsQoSQTemplateName) \
 nmhSetCmnWithLock(FsQoSQTemplateName, 14, FsQoSQTemplateNameSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %s", u4FsQoSQTemplateId ,pSetValFsQoSQTemplateName)
#define nmhSetFsQoSQTemplateDropType(u4FsQoSQTemplateId ,i4SetValFsQoSQTemplateDropType) \
 nmhSetCmnWithLock(FsQoSQTemplateDropType, 14, FsQoSQTemplateDropTypeSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %i", u4FsQoSQTemplateId ,i4SetValFsQoSQTemplateDropType)
#define nmhSetFsQoSQTemplateDropAlgoEnableFlag(u4FsQoSQTemplateId ,i4SetValFsQoSQTemplateDropAlgoEnableFlag) \
 nmhSetCmnWithLock(FsQoSQTemplateDropAlgoEnableFlag, 14, FsQoSQTemplateDropAlgoEnableFlagSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %i", u4FsQoSQTemplateId ,i4SetValFsQoSQTemplateDropAlgoEnableFlag)
#define nmhSetFsQoSQTemplateSize(u4FsQoSQTemplateId ,u4SetValFsQoSQTemplateSize) \
 nmhSetCmnWithLock(FsQoSQTemplateSize, 14, FsQoSQTemplateSizeSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSQTemplateId ,u4SetValFsQoSQTemplateSize)
#define nmhSetFsQoSQTemplateStatus(u4FsQoSQTemplateId ,i4SetValFsQoSQTemplateStatus) \
 nmhSetCmnWithLock(FsQoSQTemplateStatus, 14, FsQoSQTemplateStatusSet, QoSLock, QoSUnLock, 0, 1, 1, "%u %i", u4FsQoSQTemplateId ,i4SetValFsQoSQTemplateStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsQoSRandomDetectCfgDP[14];
extern UINT4 FsQoSRandomDetectCfgMinAvgThresh[14];
extern UINT4 FsQoSRandomDetectCfgMaxAvgThresh[14];
extern UINT4 FsQoSRandomDetectCfgMaxPktSize[14];
extern UINT4 FsQoSRandomDetectCfgMaxProb[14];
extern UINT4 FsQoSRandomDetectCfgExpWeight[14];
extern UINT4 FsQoSRandomDetectCfgStatus[14];
extern UINT4 FsQoSRandomDetectCfgGain[14];
extern UINT4 FsQoSRandomDetectCfgDropThreshType[14];
extern UINT4 FsQoSRandomDetectCfgECNThresh[14];
extern UINT4 FsQoSRandomDetectCfgActionFlag[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsQoSRandomDetectCfgMinAvgThresh(u4FsQoSQTemplateId , i4FsQoSRandomDetectCfgDP ,u4SetValFsQoSRandomDetectCfgMinAvgThresh) \
 nmhSetCmnWithLock(FsQoSRandomDetectCfgMinAvgThresh, 14, FsQoSRandomDetectCfgMinAvgThreshSet, QoSLock, QoSUnLock, 0, 0, 2, "%u %i %u", u4FsQoSQTemplateId , i4FsQoSRandomDetectCfgDP ,u4SetValFsQoSRandomDetectCfgMinAvgThresh)
#define nmhSetFsQoSRandomDetectCfgMaxAvgThresh(u4FsQoSQTemplateId , i4FsQoSRandomDetectCfgDP ,u4SetValFsQoSRandomDetectCfgMaxAvgThresh) \
 nmhSetCmnWithLock(FsQoSRandomDetectCfgMaxAvgThresh, 14, FsQoSRandomDetectCfgMaxAvgThreshSet, QoSLock, QoSUnLock, 0, 0, 2, "%u %i %u", u4FsQoSQTemplateId , i4FsQoSRandomDetectCfgDP ,u4SetValFsQoSRandomDetectCfgMaxAvgThresh)
#define nmhSetFsQoSRandomDetectCfgMaxPktSize(u4FsQoSQTemplateId , i4FsQoSRandomDetectCfgDP ,u4SetValFsQoSRandomDetectCfgMaxPktSize) \
 nmhSetCmnWithLock(FsQoSRandomDetectCfgMaxPktSize, 14, FsQoSRandomDetectCfgMaxPktSizeSet, QoSLock, QoSUnLock, 0, 0, 2, "%u %i %u", u4FsQoSQTemplateId , i4FsQoSRandomDetectCfgDP ,u4SetValFsQoSRandomDetectCfgMaxPktSize)
#define nmhSetFsQoSRandomDetectCfgMaxProb(u4FsQoSQTemplateId , i4FsQoSRandomDetectCfgDP ,u4SetValFsQoSRandomDetectCfgMaxProb) \
 nmhSetCmnWithLock(FsQoSRandomDetectCfgMaxProb, 14, FsQoSRandomDetectCfgMaxProbSet, QoSLock, QoSUnLock, 0, 0, 2, "%u %i %u", u4FsQoSQTemplateId , i4FsQoSRandomDetectCfgDP ,u4SetValFsQoSRandomDetectCfgMaxProb)
#define nmhSetFsQoSRandomDetectCfgExpWeight(u4FsQoSQTemplateId , i4FsQoSRandomDetectCfgDP ,u4SetValFsQoSRandomDetectCfgExpWeight) \
 nmhSetCmnWithLock(FsQoSRandomDetectCfgExpWeight, 14, FsQoSRandomDetectCfgExpWeightSet, QoSLock, QoSUnLock, 0, 0, 2, "%u %i %u", u4FsQoSQTemplateId , i4FsQoSRandomDetectCfgDP ,u4SetValFsQoSRandomDetectCfgExpWeight)
#define nmhSetFsQoSRandomDetectCfgStatus(u4FsQoSQTemplateId , i4FsQoSRandomDetectCfgDP ,i4SetValFsQoSRandomDetectCfgStatus) \
 nmhSetCmnWithLock(FsQoSRandomDetectCfgStatus, 14, FsQoSRandomDetectCfgStatusSet, QoSLock, QoSUnLock, 0, 1, 2, "%u %i %i", u4FsQoSQTemplateId , i4FsQoSRandomDetectCfgDP ,i4SetValFsQoSRandomDetectCfgStatus)

#define nmhSetFsQoSRandomDetectCfgGain(u4FsQoSQTemplateId , i4FsQoSRandomDetectCfgDP ,i4SetValFsQoSRandomDetectCfgGain) \
 nmhSetCmnWithLock(FsQoSRandomDetectCfgGain, 14, FsQoSRandomDetectCfgGainSet, QoSLock, QoSUnLock, 0, 1, 2, "%u %i %u", u4FsQoSQTemplateId , i4FsQoSRandomDetectCfgDP ,i4SetValFsQoSRandomDetectCfgGain)

#define nmhSetFsQoSRandomDetectCfgDropThreshType(u4FsQoSQTemplateId , i4FsQoSRandomDetectCfgDP ,i4SetValFsQoSRandomDetectCfgDropThreshType) \
 nmhSetCmnWithLock(FsQoSRandomDetectCfgDropThreshType, 14, FsQoSRandomDetectCfgDropThreshTypeSet, QoSLock, QoSUnLock, 0, 1, 2, "%u %i %i", u4FsQoSQTemplateId , i4FsQoSRandomDetectCfgDP ,i4SetValFsQoSRandomDetectCfgDropThreshType)

#define nmhSetFsQoSRandomDetectCfgECNThresh(u4FsQoSQTemplateId , i4FsQoSRandomDetectCfgDP ,i4SetValFsQoSRandomDetectCfgECNThresh) \
 nmhSetCmnWithLock(FsQoSRandomDetectCfgECNThresh, 14, FsQoSRandomDetectCfgECNThreshSet, QoSLock, QoSUnLock, 0, 1, 2, "%u %i %u", u4FsQoSQTemplateId , i4FsQoSRandomDetectCfgDP ,i4SetValFsQoSRandomDetectCfgECNThresh)

#define nmhSetFsQoSRandomDetectCfgActionFlag(u4FsQoSQTemplateId , i4FsQoSRandomDetectCfgDP ,pSetValFsQoSRandomDetectCfgActionFlag) \
 nmhSetCmnWithLock(FsQoSRandomDetectCfgActionFlag, 14, FsQoSRandomDetectCfgActionFlagSet, QoSLock, QoSUnLock, 0, 1, 2, "%u %i %s", u4FsQoSQTemplateId , i4FsQoSRandomDetectCfgDP ,pSetValFsQoSRandomDetectCfgActionFlag)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsQoSShapeTemplateId[14];
extern UINT4 FsQoSShapeTemplateName[14];
extern UINT4 FsQoSShapeTemplateCIR[14];
extern UINT4 FsQoSShapeTemplateCBS[14];
extern UINT4 FsQoSShapeTemplateEIR[14];
extern UINT4 FsQoSShapeTemplateEBS[14];
extern UINT4 FsQoSShapeTemplateStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsQoSShapeTemplateName(u4FsQoSShapeTemplateId ,pSetValFsQoSShapeTemplateName) \
 nmhSetCmnWithLock(FsQoSShapeTemplateName, 14, FsQoSShapeTemplateNameSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %s", u4FsQoSShapeTemplateId ,pSetValFsQoSShapeTemplateName)
#define nmhSetFsQoSShapeTemplateCIR(u4FsQoSShapeTemplateId ,u4SetValFsQoSShapeTemplateCIR) \
 nmhSetCmnWithLock(FsQoSShapeTemplateCIR, 14, FsQoSShapeTemplateCIRSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSShapeTemplateId ,u4SetValFsQoSShapeTemplateCIR)
#define nmhSetFsQoSShapeTemplateCBS(u4FsQoSShapeTemplateId ,u4SetValFsQoSShapeTemplateCBS) \
 nmhSetCmnWithLock(FsQoSShapeTemplateCBS, 14, FsQoSShapeTemplateCBSSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSShapeTemplateId ,u4SetValFsQoSShapeTemplateCBS)
#define nmhSetFsQoSShapeTemplateEIR(u4FsQoSShapeTemplateId ,u4SetValFsQoSShapeTemplateEIR) \
 nmhSetCmnWithLock(FsQoSShapeTemplateEIR, 14, FsQoSShapeTemplateEIRSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSShapeTemplateId ,u4SetValFsQoSShapeTemplateEIR)
#define nmhSetFsQoSShapeTemplateEBS(u4FsQoSShapeTemplateId ,u4SetValFsQoSShapeTemplateEBS) \
 nmhSetCmnWithLock(FsQoSShapeTemplateEBS, 14, FsQoSShapeTemplateEBSSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQoSShapeTemplateId ,u4SetValFsQoSShapeTemplateEBS)
#define nmhSetFsQoSShapeTemplateStatus(u4FsQoSShapeTemplateId ,i4SetValFsQoSShapeTemplateStatus) \
 nmhSetCmnWithLock(FsQoSShapeTemplateStatus, 14, FsQoSShapeTemplateStatusSet, QoSLock, QoSUnLock, 0, 1, 1, "%u %i", u4FsQoSShapeTemplateId ,i4SetValFsQoSShapeTemplateStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsQoSQMapCLASS[14];
extern UINT4 FsQoSQMapRegenPriType[14];
extern UINT4 FsQoSQMapRegenPriority[14];
extern UINT4 FsQoSQMapQId[14];
extern UINT4 FsQoSQMapStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsQoSQMapQId(i4IfIndex , u4FsQoSQMapCLASS , i4FsQoSQMapRegenPriType , u4FsQoSQMapRegenPriority ,u4SetValFsQoSQMapQId) \
 nmhSetCmnWithLock(FsQoSQMapQId, 14, FsQoSQMapQIdSet, QoSLock, QoSUnLock, 0, 0, 4, "%i %u %i %u %u", i4IfIndex , u4FsQoSQMapCLASS , i4FsQoSQMapRegenPriType , u4FsQoSQMapRegenPriority ,u4SetValFsQoSQMapQId)
#define nmhSetFsQoSQMapStatus(i4IfIndex , u4FsQoSQMapCLASS , i4FsQoSQMapRegenPriType , u4FsQoSQMapRegenPriority ,i4SetValFsQoSQMapStatus) \
 nmhSetCmnWithLock(FsQoSQMapStatus, 14, FsQoSQMapStatusSet, QoSLock, QoSUnLock, 0, 1, 4, "%i %u %i %u %i", i4IfIndex , u4FsQoSQMapCLASS , i4FsQoSQMapRegenPriType , u4FsQoSQMapRegenPriority ,i4SetValFsQoSQMapStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsQoSQId[14];
extern UINT4 FsQoSQCfgTemplateId[14];
extern UINT4 FsQoSQSchedulerId[14];
extern UINT4 FsQoSQWeight[14];
extern UINT4 FsQoSQPriority[14];
extern UINT4 FsQoSQShapeId[14];
extern UINT4 FsQoSQStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsQoSQCfgTemplateId(i4IfIndex , u4FsQoSQId ,u4SetValFsQoSQCfgTemplateId) \
 nmhSetCmnWithLock(FsQoSQCfgTemplateId, 14, FsQoSQCfgTemplateIdSet, QoSLock, QoSUnLock, 0, 0, 2, "%i %u %u", i4IfIndex , u4FsQoSQId ,u4SetValFsQoSQCfgTemplateId)
#define nmhSetFsQoSQSchedulerId(i4IfIndex , u4FsQoSQId ,u4SetValFsQoSQSchedulerId) \
 nmhSetCmnWithLock(FsQoSQSchedulerId, 14, FsQoSQSchedulerIdSet, QoSLock, QoSUnLock, 0, 0, 2, "%i %u %u", i4IfIndex , u4FsQoSQId ,u4SetValFsQoSQSchedulerId)
#define nmhSetFsQoSQWeight(i4IfIndex , u4FsQoSQId ,u4SetValFsQoSQWeight) \
 nmhSetCmnWithLock(FsQoSQWeight, 14, FsQoSQWeightSet, QoSLock, QoSUnLock, 0, 0, 2, "%i %u %u", i4IfIndex , u4FsQoSQId ,u4SetValFsQoSQWeight)
#define nmhSetFsQoSQPriority(i4IfIndex , u4FsQoSQId ,u4SetValFsQoSQPriority) \
 nmhSetCmnWithLock(FsQoSQPriority, 14, FsQoSQPrioritySet, QoSLock, QoSUnLock, 0, 0, 2, "%i %u %u", i4IfIndex , u4FsQoSQId ,u4SetValFsQoSQPriority)
#define nmhSetFsQoSQShapeId(i4IfIndex , u4FsQoSQId ,u4SetValFsQoSQShapeId) \
 nmhSetCmnWithLock(FsQoSQShapeId, 14, FsQoSQShapeIdSet, QoSLock, QoSUnLock, 0, 0, 2, "%i %u %u", i4IfIndex , u4FsQoSQId ,u4SetValFsQoSQShapeId)
#define nmhSetFsQoSQStatus(i4IfIndex , u4FsQoSQId ,i4SetValFsQoSQStatus) \
 nmhSetCmnWithLock(FsQoSQStatus, 14, FsQoSQStatusSet, QoSLock, QoSUnLock, 0, 1, 2, "%i %u %i", i4IfIndex , u4FsQoSQId ,i4SetValFsQoSQStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsQoSSchedulerId[14];
extern UINT4 FsQoSSchedulerSchedAlgorithm[14];
extern UINT4 FsQoSSchedulerShapeId[14];
extern UINT4 FsQoSSchedulerHierarchyLevel[14];
extern UINT4 FsQoSSchedulerGlobalId[14];
extern UINT4 FsQoSSchedulerStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsQoSSchedulerSchedAlgorithm(i4IfIndex , u4FsQoSSchedulerId ,i4SetValFsQoSSchedulerSchedAlgorithm) \
 nmhSetCmnWithLock(FsQoSSchedulerSchedAlgorithm, 14, FsQoSSchedulerSchedAlgorithmSet, QoSLock, QoSUnLock, 0, 0, 2, "%i %u %i", i4IfIndex , u4FsQoSSchedulerId ,i4SetValFsQoSSchedulerSchedAlgorithm)
#define nmhSetFsQoSSchedulerShapeId(i4IfIndex , u4FsQoSSchedulerId ,u4SetValFsQoSSchedulerShapeId) \
 nmhSetCmnWithLock(FsQoSSchedulerShapeId, 14, FsQoSSchedulerShapeIdSet, QoSLock, QoSUnLock, 0, 0, 2, "%i %u %u", i4IfIndex , u4FsQoSSchedulerId ,u4SetValFsQoSSchedulerShapeId)
#define nmhSetFsQoSSchedulerHierarchyLevel(i4IfIndex , u4FsQoSSchedulerId ,u4SetValFsQoSSchedulerHierarchyLevel) \
 nmhSetCmnWithLock(FsQoSSchedulerHierarchyLevel, 14, FsQoSSchedulerHierarchyLevelSet, QoSLock, QoSUnLock, 0, 0, 2, "%i %u %u", i4IfIndex , u4FsQoSSchedulerId ,u4SetValFsQoSSchedulerHierarchyLevel)
#define nmhSetFsQoSSchedulerGlobalId(i4IfIndex , u4FsQoSSchedulerId ,u4SetValFsQoSSchedulerGlobalId) \
 nmhSetCmnWithLock(FsQoSSchedulerGlobalId, 14, FsQoSSchedulerGlobalIdSet, QoSLock, QoSUnLock, 0, 0, 2, "%i %u %u", i4IfIndex , u4FsQoSSchedulerId ,u4SetValFsQoSSchedulerGlobalId)
#define nmhSetFsQoSSchedulerStatus(i4IfIndex , u4FsQoSSchedulerId ,i4SetValFsQoSSchedulerStatus) \
 nmhSetCmnWithLock(FsQoSSchedulerStatus, 14, FsQoSSchedulerStatusSet, QoSLock, QoSUnLock, 0, 1, 2, "%i %u %i", i4IfIndex , u4FsQoSSchedulerId ,i4SetValFsQoSSchedulerStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsQoSHierarchyLevel[14];
extern UINT4 FsQoSHierarchyQNext[14];
extern UINT4 FsQoSHierarchySchedNext[14];
extern UINT4 FsQoSHierarchyWeight[14];
extern UINT4 FsQoSHierarchyPriority[14];
extern UINT4 FsQoSHierarchyStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsQoSHierarchyQNext(i4IfIndex , u4FsQoSHierarchyLevel , u4FsQoSSchedulerId ,u4SetValFsQoSHierarchyQNext) \
 nmhSetCmnWithLock(FsQoSHierarchyQNext, 14, FsQoSHierarchyQNextSet, QoSLock, QoSUnLock, 0, 0, 3, "%i %u %u %u", i4IfIndex , u4FsQoSHierarchyLevel , u4FsQoSSchedulerId ,u4SetValFsQoSHierarchyQNext)
#define nmhSetFsQoSHierarchySchedNext(i4IfIndex , u4FsQoSHierarchyLevel , u4FsQoSSchedulerId ,u4SetValFsQoSHierarchySchedNext) \
 nmhSetCmnWithLock(FsQoSHierarchySchedNext, 14, FsQoSHierarchySchedNextSet, QoSLock, QoSUnLock, 0, 0, 3, "%i %u %u %u", i4IfIndex , u4FsQoSHierarchyLevel , u4FsQoSSchedulerId ,u4SetValFsQoSHierarchySchedNext)
#define nmhSetFsQoSHierarchyWeight(i4IfIndex , u4FsQoSHierarchyLevel , u4FsQoSSchedulerId ,u4SetValFsQoSHierarchyWeight) \
 nmhSetCmnWithLock(FsQoSHierarchyWeight, 14, FsQoSHierarchyWeightSet, QoSLock, QoSUnLock, 0, 0, 3, "%i %u %u %u", i4IfIndex , u4FsQoSHierarchyLevel , u4FsQoSSchedulerId ,u4SetValFsQoSHierarchyWeight)
#define nmhSetFsQoSHierarchyPriority(i4IfIndex , u4FsQoSHierarchyLevel , u4FsQoSSchedulerId ,i4SetValFsQoSHierarchyPriority) \
 nmhSetCmnWithLock(FsQoSHierarchyPriority, 14, FsQoSHierarchyPrioritySet, QoSLock, QoSUnLock, 0, 0, 3, "%i %u %u %i", i4IfIndex , u4FsQoSHierarchyLevel , u4FsQoSSchedulerId ,i4SetValFsQoSHierarchyPriority)
#define nmhSetFsQoSHierarchyStatus(i4IfIndex , u4FsQoSHierarchyLevel , u4FsQoSSchedulerId ,i4SetValFsQoSHierarchyStatus) \
 nmhSetCmnWithLock(FsQoSHierarchyStatus, 14, FsQoSHierarchyStatusSet, QoSLock, QoSUnLock, 0, 1, 3, "%i %u %u %i", i4IfIndex , u4FsQoSHierarchyLevel , u4FsQoSSchedulerId ,i4SetValFsQoSHierarchyStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsQoSPortDefaultUserPriority[14];
extern UINT4 FsQoSPortPbitPrefOverDscp[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsQoSPortDefaultUserPriority(i4IfIndex ,i4SetValFsQoSPortDefaultUserPriority) \
 nmhSetCmnWithLock(FsQoSPortDefaultUserPriority, 14, FsQoSPortDefaultUserPrioritySet, QoSLock, QoSUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValFsQoSPortDefaultUserPriority)
#define nmhSetFsQoSPortPbitPrefOverDscp(i4IfIndex ,i4SetValFsQoSPortPbitPrefOverDscp) \
 nmhSetCmnWithLock(FsQoSPortPbitPrefOverDscp, 14, FsQoSPortPbitPrefOverDscpSet, QoSLock, QoSUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValFsQoSPortPbitPrefOverDscp)

#endif

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsQoSPolicerStatsStatus[14];
extern UINT4 FsQoSPolicerStatsClearCounter[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsQoSPolicerStatsStatus(u4FsQoSMeterId ,i4SetValFsQoSPolicerStatsStatus)  \
        nmhSetCmn(FsQoSPolicerStatsStatus, 14, FsQoSPolicerStatsStatusSet, NULL, NULL, 0, 0, 1, "%u %i", u4FsQoSMeterId ,i4SetValFsQoSPolicerStatsStatus)
#define nmhSetFsQoSPolicerStatsClearCounter(u4FsQoSMeterId ,i4SetValFsQoSPolicerStatsClearCounter)  \
        nmhSetCmn(FsQoSPolicerStatsClearCounter, 14, FsQoSPolicerStatsClearCounterSet, NULL, NULL, 0, 0, 1, "%u %i", u4FsQoSMeterId ,i4SetValFsQoSPolicerStatsClearCounter)

#endif                                                                                                                                                       

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsQosHwCpuQId[14];
extern UINT4 FsQosHwCpuMinRate[14];
extern UINT4 FsQosHwCpuMaxRate[14];
extern UINT4 FsQosHwCpuRowStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsQosHwCpuMinRate(u4FsQosHwCpuQId ,u4SetValFsQosHwCpuMinRate) \
 nmhSetCmnWithLock(FsQosHwCpuMinRate, 14, FsQosHwCpuMinRateSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQosHwCpuQId ,u4SetValFsQosHwCpuMinRate)
#define nmhSetFsQosHwCpuMaxRate(u4FsQosHwCpuQId ,u4SetValFsQosHwCpuMaxRate) \
 nmhSetCmnWithLock(FsQosHwCpuMaxRate, 14, FsQosHwCpuMaxRateSet, QoSLock, QoSUnLock, 0, 0, 1, "%u %u", u4FsQosHwCpuQId ,u4SetValFsQosHwCpuMaxRate)
#define nmhSetFsQosHwCpuRowStatus(u4FsQosHwCpuQId ,i4SetValFsQosHwCpuRowStatus) \
 nmhSetCmnWithLock(FsQosHwCpuRowStatus, 14, FsQosHwCpuRowStatusSet, QoSLock, QoSUnLock, 0, 1, 1, "%u %i", u4FsQosHwCpuQId ,i4SetValFsQosHwCpuRowStatus)

#endif
