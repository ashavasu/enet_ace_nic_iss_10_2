/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdlacli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Dot3adAggIndex[11];
extern UINT4 Dot3adAggActorSystemPriority[11];
extern UINT4 Dot3adAggActorAdminKey[11];
extern UINT4 Dot3adAggCollectorMaxDelay[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDot3adAggActorSystemPriority(i4Dot3adAggIndex ,i4SetValDot3adAggActorSystemPriority)	\
	nmhSetCmn(Dot3adAggActorSystemPriority, 11, Dot3adAggActorSystemPrioritySet, LaLock, LaUnLock, 0, 0, 1, "%i %i", i4Dot3adAggIndex ,i4SetValDot3adAggActorSystemPriority)
#define nmhSetDot3adAggActorAdminKey(i4Dot3adAggIndex ,i4SetValDot3adAggActorAdminKey)	\
	nmhSetCmn(Dot3adAggActorAdminKey, 11, Dot3adAggActorAdminKeySet, LaLock, LaUnLock, 0, 0, 1, "%i %i", i4Dot3adAggIndex ,i4SetValDot3adAggActorAdminKey)
#define nmhSetDot3adAggCollectorMaxDelay(i4Dot3adAggIndex ,i4SetValDot3adAggCollectorMaxDelay)	\
	nmhSetCmn(Dot3adAggCollectorMaxDelay, 11, Dot3adAggCollectorMaxDelaySet, LaLock, LaUnLock, 0, 0, 1, "%i %i", i4Dot3adAggIndex ,i4SetValDot3adAggCollectorMaxDelay)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Dot3adAggPortIndex[11];
extern UINT4 Dot3adAggPortActorSystemPriority[11];
extern UINT4 Dot3adAggPortActorAdminKey[11];
extern UINT4 Dot3adAggPortPartnerAdminSystemPriority[11];
extern UINT4 Dot3adAggPortPartnerAdminSystemID[11];
extern UINT4 Dot3adAggPortPartnerAdminKey[11];
extern UINT4 Dot3adAggPortActorPortPriority[11];
extern UINT4 Dot3adAggPortPartnerAdminPort[11];
extern UINT4 Dot3adAggPortPartnerAdminPortPriority[11];
extern UINT4 Dot3adAggPortActorAdminState[11];
extern UINT4 Dot3adAggPortPartnerAdminState[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDot3adAggPortActorSystemPriority(i4Dot3adAggPortIndex ,i4SetValDot3adAggPortActorSystemPriority)	\
	nmhSetCmn(Dot3adAggPortActorSystemPriority, 11, Dot3adAggPortActorSystemPrioritySet, LaLock, LaUnLock, 0, 0, 1, "%i %i", i4Dot3adAggPortIndex ,i4SetValDot3adAggPortActorSystemPriority)
#define nmhSetDot3adAggPortActorAdminKey(i4Dot3adAggPortIndex ,i4SetValDot3adAggPortActorAdminKey)	\
	nmhSetCmn(Dot3adAggPortActorAdminKey, 11, Dot3adAggPortActorAdminKeySet, LaLock, LaUnLock, 0, 0, 1, "%i %i", i4Dot3adAggPortIndex ,i4SetValDot3adAggPortActorAdminKey)
#define nmhSetDot3adAggPortPartnerAdminSystemPriority(i4Dot3adAggPortIndex ,i4SetValDot3adAggPortPartnerAdminSystemPriority)	\
	nmhSetCmn(Dot3adAggPortPartnerAdminSystemPriority, 11, Dot3adAggPortPartnerAdminSystemPrioritySet, LaLock, LaUnLock, 0, 0, 1, "%i %i", i4Dot3adAggPortIndex ,i4SetValDot3adAggPortPartnerAdminSystemPriority)
#define nmhSetDot3adAggPortPartnerAdminSystemID(i4Dot3adAggPortIndex ,SetValDot3adAggPortPartnerAdminSystemID)	\
	nmhSetCmn(Dot3adAggPortPartnerAdminSystemID, 11, Dot3adAggPortPartnerAdminSystemIDSet, LaLock, LaUnLock, 0, 0, 1, "%i %m", i4Dot3adAggPortIndex ,SetValDot3adAggPortPartnerAdminSystemID)
#define nmhSetDot3adAggPortPartnerAdminKey(i4Dot3adAggPortIndex ,i4SetValDot3adAggPortPartnerAdminKey)	\
	nmhSetCmn(Dot3adAggPortPartnerAdminKey, 11, Dot3adAggPortPartnerAdminKeySet, LaLock, LaUnLock, 0, 0, 1, "%i %i", i4Dot3adAggPortIndex ,i4SetValDot3adAggPortPartnerAdminKey)
#define nmhSetDot3adAggPortActorPortPriority(i4Dot3adAggPortIndex ,i4SetValDot3adAggPortActorPortPriority)	\
	nmhSetCmn(Dot3adAggPortActorPortPriority, 11, Dot3adAggPortActorPortPrioritySet, LaLock, LaUnLock, 0, 0, 1, "%i %i", i4Dot3adAggPortIndex ,i4SetValDot3adAggPortActorPortPriority)
#define nmhSetDot3adAggPortPartnerAdminPort(i4Dot3adAggPortIndex ,i4SetValDot3adAggPortPartnerAdminPort)	\
	nmhSetCmn(Dot3adAggPortPartnerAdminPort, 11, Dot3adAggPortPartnerAdminPortSet, LaLock, LaUnLock, 0, 0, 1, "%i %i", i4Dot3adAggPortIndex ,i4SetValDot3adAggPortPartnerAdminPort)
#define nmhSetDot3adAggPortPartnerAdminPortPriority(i4Dot3adAggPortIndex ,i4SetValDot3adAggPortPartnerAdminPortPriority)	\
	nmhSetCmn(Dot3adAggPortPartnerAdminPortPriority, 11, Dot3adAggPortPartnerAdminPortPrioritySet, LaLock, LaUnLock, 0, 0, 1, "%i %i", i4Dot3adAggPortIndex ,i4SetValDot3adAggPortPartnerAdminPortPriority)
#define nmhSetDot3adAggPortActorAdminState(i4Dot3adAggPortIndex ,pSetValDot3adAggPortActorAdminState)	\
	nmhSetCmn(Dot3adAggPortActorAdminState, 11, Dot3adAggPortActorAdminStateSet, LaLock, LaUnLock, 0, 0, 1, "%i %s", i4Dot3adAggPortIndex ,pSetValDot3adAggPortActorAdminState)
#define nmhSetDot3adAggPortPartnerAdminState(i4Dot3adAggPortIndex ,pSetValDot3adAggPortPartnerAdminState)	\
	nmhSetCmn(Dot3adAggPortPartnerAdminState, 11, Dot3adAggPortPartnerAdminStateSet, LaLock, LaUnLock, 0, 0, 1, "%i %s", i4Dot3adAggPortIndex ,pSetValDot3adAggPortPartnerAdminState)

#endif
