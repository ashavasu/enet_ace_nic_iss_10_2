/* INCLUDE FILE HEADER :
 *
 * * $Id: snpcli.h,v 1.46 2017/08/04 13:14:54 siva Exp $
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : snpcli.h                                         |
 * |  PRINCIPAL AUTHOR      : ISS                                              |
 * |  SUBSYSTEM NAME        : SNOOPING                                         |
 * |  MODULE NAME           : CLI interface for Snooping configuration         |
 * |  LANGUAGE              : C                                                |
 * |  TARGET ENVIRONMENT    :                                                  |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |  DESCRIPTION           : This file contains command idenfiers for         | 
 * |                          definitions in igscmd.def, function prototypes   |
 * |                          for SNOOP (IGMP/MLD)  CLI and corresponding      |
 * |                          error code                                       |
 * |                          definitions                                      |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 */

#ifndef __SNPCLI_H__
#define __SNPCLI_H__
#include "cli.h"
/* SNOOP CLI command constants. To add a new command ,a new definition needs to
 * be added to the list.
 */
enum 
{
  SNOOP_SYS_SHUT = 1,
  SNOOP_SYS_NO_SHUT,
  SNOOP_MCAST_FWD_MODE_SET,
  SNOOP_MCAST_LEAVE_CONFIG_LEVEL,
  SNOOP_MCAST_REPORT_CONFIG_LEVEL,
  IGS_SYS_ENABLE,
  IGS_SYS_DISABLE,
  IGS_SEND_QUERY_ENABLE,
  IGS_SEND_QUERY_DISABLE,
  IGS_PROXY_ENABLE,
  IGS_PROXY_DISABLE,
  IGS_PROXY_REPORTING_ENABLE,
  IGS_PROXY_REPORTING_DISABLE,
  IGS_ROUTER_PORT_PURGE_INTERVAL,
  IGS_NO_ROUTER_PORT_PURGE_INTERVAL,
  IGS_PORT_PURGE_INTERVAL,
  IGS_NO_PORT_PURGE_INTERVAL,
  IGS_REPORT_SUPPRESS_INTERVAL,
  IGS_NO_REPORT_SUPPRESS_INTERVAL,
  IGS_RETRY_COUNT,
  IGS_NO_RETRY_COUNT,
  IGS_GROUP_QUERY_INTERVAL,
  IGS_NO_GROUP_QUERY_INTERVAL,
  IGS_AGGREGATION_INTERVAL,
  IGS_NO_AGGREGATION_INTERVAL,
  IGS_REPORT_FORWADING,
  IGS_NO_REPORT_FORWADING,
  IGS_QUERY_FORWADING,
  IGS_VLAN_ENABLE,
  IGS_VLAN_DISABLE,
  IGS_VLAN_OPER_VERSION,
  IGS_VLAN_FAST_LEAVE_ENABLE,
  IGS_VLAN_FAST_LEAVE_DISABLE,
  IGS_VLAN_QUERIER_ENABLE,
  IGS_VLAN_QUERIER_DISABLE,
  IGS_VLAN_QUERY_INTERVAL,
  IGS_NO_VLAN_QUERY_INTERVAL,
  IGS_VLAN_STARTUP_QUERY_INTERVAL,
  IGS_NO_VLAN_STARTUP_QUERY_INTERVAL,
  IGS_VLAN_STARTUP_QUERY_COUNT,
  IGS_NO_VLAN_STARTUP_QUERY_COUNT,
  IGS_VLAN_OTHER_QUERIER_PRESENT_INTERVAL,
  IGS_NO_VLAN_OTHER_QUERIER_PRESENT_INTERVAL,
  IGS_VLAN_MAX_RESP_CODE,
  IGS_NO_VLAN_MAX_RESP_CODE,
  IGS_VLAN_RTR_PORT,
  IGS_VLAN_NO_RTR_PORT,
  IGS_RTR_PORT_PURGE_INT,
  IGS_RTR_PORT_NO_PURGE_INT,
  IGS_RTR_PORT_OPER_VERSION,
  IGS_RTR_PORT_NO_OPER_VERSION,
  IGS_VLAN_MVLAN_PROFILE,
  IGS_VLAN_MVLAN_NO_PROFILE,
  IGS_VLAN_STATS_CLEAR_COUNTERS,
  IGS_TRACE_ENABLE,
  IGS_DEBUG_ENABLE,
  IGS_MVLAN_ENABLE,
  IGS_MVLAN_DISABLE,
  IGS_FILTER_ENABLE,
  IGS_FILTER_DISABLE,
  IGS_SHOW_VLAN_RTR_PORTS,
  IGS_RED_SHOW_VLAN_RTR_PORTS,
  IGS_SHOW_GLOBALS_INFO,
  IGS_SHOW_VLAN_INFO,
  IGS_RED_SHOW_VLAN_INFO,
  IGS_SHOW_GRP_ENTRIES,
  IGS_SHOW_FWD_ENTRIES,
  IGS_RED_SHOW_FWD_ENTRIES,
  IGS_SHOW_STATS,
  IGS_SHOW_MVLAN_MAPPING,
  IGS_SHOW_HOST_ENTRIES,
  IGS_VLAN_BLOCKED_RTR_PORT,
  IGS_VLAN_NO_BLOCKED_RTR_PORT,
  IGS_SHOW_VLAN_BLOCKED_RTR_PORTS,    
  MLDS_SYS_ENABLE,
  MLDS_SYS_DISABLE,
  MLDS_SEND_QUERY_ENABLE,
  MLDS_SEND_QUERY_DISABLE,
  MLDS_PROXY_ENABLE,
  MLDS_PROXY_DISABLE,
  MLDS_PROXY_REPORTING_ENABLE,
  MLDS_PROXY_REPORTING_DISABLE,
  MLDS_ROUTER_PORT_PURGE_INTERVAL,
  MLDS_NO_ROUTER_PORT_PURGE_INTERVAL,
  MLDS_PORT_PURGE_INTERVAL,
  MLDS_NO_PORT_PURGE_INTERVAL,
  MLDS_REPORT_SUPPRESS_INTERVAL,
  MLDS_NO_REPORT_SUPPRESS_INTERVAL,
  MLDS_RETRY_COUNT,
  MLDS_NO_RETRY_COUNT,
  MLDS_GROUP_QUERY_INTERVAL,
  MLDS_NO_GROUP_QUERY_INTERVAL,
  MLDS_AGGREGATION_INTERVAL,
  MLDS_NO_AGGREGATION_INTERVAL,
  MLDS_REPORT_FORWADING,
  MLDS_NO_REPORT_FORWADING,
  MLDS_VLAN_ENABLE,
  MLDS_VLAN_DISABLE,
  MLDS_VLAN_OPER_VERSION,
  MLDS_VLAN_FAST_LEAVE_ENABLE,
  MLDS_VLAN_FAST_LEAVE_DISABLE,
  MLDS_VLAN_QUERIER_ENABLE,
  MLDS_VLAN_QUERIER_DISABLE,
  MLDS_VLAN_QUERY_INTERVAL,
  MLDS_NO_VLAN_QUERY_INTERVAL,
  MLDS_VLAN_MAX_RESP_CODE,
  MLDS_NO_VLAN_MAX_RESP_CODE,
  MLDS_VLAN_RTR_PORT,
  MLDS_VLAN_NO_RTR_PORT,
  MLDS_RTR_PORT_PURGE_INT,
  MLDS_RTR_PORT_NO_PURGE_INT,
  MLDS_RTR_PORT_OPER_VERSION,
  MLDS_RTR_PORT_NO_OPER_VERSION,
  MLDS_VLAN_STATS_CLEAR_COUNTERS,
  MLDS_TRACE_ENABLE,
  MLDS_DEBUG_ENABLE,
  MLDS_SHOW_VLAN_RTR_PORTS,
  MLDS_SHOW_GLOBALS_INFO,
  MLDS_SHOW_VLAN_INFO,
  MLDS_SHOW_GRP_ENTRIES,
  MLDS_SHOW_FWD_ENTRIES,
  MLDS_SHOW_STATS,
  SNOOP_SYS_ENH_MODE_ENABLE,
  SNOOP_SYS_ENH_MODE_DISABLE,
  SNOOP_SYS_SPARSE_MODE_ENABLE,
  SNOOP_SYS_SPARSE_MODE_DISABLE,
  SNOOP_PORT_LEAVE_CONFIGS,
  SNOOP_PORT_RATE_LIMIT,
  SNOOP_NO_PORT_RATE_LIMIT,
  SNOOP_PORT_MAX_LIMIT,
  SNOOP_NO_PORT_MAX_LIMIT,
  SNOOP_PORT_PROFILE_ID,
  SNOOP_NO_PORT_PROFILE_ID,
  SNOOP_SHOW_PORT_CFG_ENTRIES,
   /********For Static***************/
  IGS_VLAN_ADD_STATIC_GRP_ENTRY,
  IGS_VLAN_REMOVE_STATIC_GRP_ENTRY,
  SNOOP_MCAST_FWD_MODE_CP_MODEL_SET,
  IGS_VLAN_ROBUSTNESS_VALUE,
  IGS_NO_VLAN_ROBUSTNESS_VALUE

};


#define SNOOP_IP_FWD_MODE                1
#define SNOOP_MAC_FWD_MODE               2

#define SNOOP_IP_FWD_MODE_CP_DRIVEN      1
#define SNOOP_IP_FWD_MODE_DP_DRIVEN      2

#define SNOOP_IGMP_VERSION_1             1
#define SNOOP_IGMP_VERSION_2             2
#define SNOOP_IGMP_VERSION_3             3

#define SNOOP_MLD_VERSION_1             1
#define SNOOP_MLD_VERSION_2             2

#define SNOOP_CLI_SHOW_RTR_PORT           1
#define SNOOP_CLI_SHOW_ALL_RTR_PORT_INFO  2
#define SNOOP_CLI_SHOW_SUMMARY           1

#define SNOOP_ALL_PORTS                  1 
#define SNOOP_RTR_PORTS                  2 
#define SNOOP_NON_EDGE_PORTS             3
#define SNOOP_NON_RTR_PORTS              2
#define SNOOP_RTR_PORTS                  2 

#define SNOOP_TRC_DATA                   1
#define SNOOP_TRC_CONTROL                2
#define SNOOP_TRC_Tx                     4
#define SNOOP_TRC_Rx                     8
#define SNOOP_TRC_ALL                    15

#define SNOOP_DBG_INIT                   0x00000001 
#define SNOOP_DBG_RESRC                  0x00000002 
#define SNOOP_DBG_TMR                    0x00000004 
#define SNOOP_DBG_SRC                    0x00000008 
#define SNOOP_DBG_GRP                    0x00000010 
#define SNOOP_DBG_QRY                    0x00000020 
#define SNOOP_DBG_RED                    0x00000040 
#define SNOOP_DBG_PKT                    0x00000080 
#define SNOOP_DBG_FWD                    0x00000100 
#define SNOOP_DBG_VLAN                   0x00000200 
#define SNOOP_DBG_ENTRY                  0x00000400 
#define SNOOP_DBG_EXIT                   0x00000800 
#define SNOOP_DBG_MGMT                   0x00001000 
#define SNOOP_DBG_NP                     0x00002000 
#define SNOOP_DBG_BUFFER                 0x00004000 
#define SNOOP_DBG_ICCH                   0x00008000 
#define SNOOP_DBG_ALL_FAILURE            0x00010000 
#define SNOOP_DBG_ALL                    0x0001F37F 
#define SNOOP_DBG_DEFVAL                 0x00000000 

#define SNOOP_MAX_INT4                 0x7fffffff
#define SNOOP_CLI_TRACE_DIS_FLAG       0x80000000

#define SNOOP_CLI_MAX_ARGS               4
              
#define SNOOP_CLI_MAX_COMMANDS          77 

#define SNOOP_CSR_STR_LEN               50

#define SNOOP_LEAVE_CONFIG_LEVEL_VLAN    1
#define SNOOP_LEAVE_CONFIG_LEVEL_PORT    2

#define SNOOP_REPORT_CONFIG_LEVEL_NON_RTR_PORT   1
#define SNOOP_REPORT_CONFIG_LEVEL_ALL_PORTS      2

 /* Error code values and strings are maintained inside the protocol 
  * module itself.
  */
enum {
  CLI_SNOOP_INVALID_PORT_ERR = 1,
  CLI_SNOOP_GMRP_MMRP_ENABLED_ERR,
  CLI_SNOOP_IGMP_TUNNEL_ENA_ERR,
  CLI_SNOOP_BRG_MODE_ERR,
  CLI_SNOOP_GENERAL_ERROR,
  CLI_SNOOP_INVALID_ADDRESS_ERR,
  CLI_SNOOP_RTR_PORTLIST_ERR,
  CLI_SNOOP_PROXY_REPORTING_ERR,
  CLI_SNOOP_PROXY_STATUS_ERR,
  CLI_SNOOP_PROXY_QUERIER_ERR,
  CLI_SNOOP_PROXY_MRC_ERR,
  CLI_SNOOP_INV_RTR_PORT_ERR,
  CLI_SNOOP_INV_RTR_PORT_VER_ERR,
  CLI_SNOOP_INV_RTR_PORT_OLD_QRY_INT_ERR,
  CLI_SNOOP_MODULE_SHUT_DOWN_ERR,
  CLI_SNOOP_INVALID_INSTANCE_ERR,
  CLI_SNOOP_ENH_MAC_MODE_ERR,
  CLI_SNOOP_SPARSE_IP_MODE_ERR,
  CLI_SNOOP_INVALID_INNER_VLAN_ID_ERR,
  CLI_SNOOP_INVALID_MODE_DEF_ERR,
  CLI_SNOOP_INVALID_MODE_ENH_ERR,
  CLI_SNOOP_PORT_CFG_ENTRY_NOT_FOUND_ERR,
  CLI_SNOOP_PORT_CFG_ENTRY_PORT_CRT_ERR,
  CLI_SNOOP_PORT_LEAVE_MODE_ERR,
  CLI_SNOOP_PORT_GLOB_LEAVE_CONF_LEV_ERR,
  CLI_SNOOP_PORT_RATE_LMT_ERR,
  CLI_SNOOP_PORT_GRP_MAX_LMT_TYPE_ERR,
  CLI_SNOOP_PORT_CHN_MAX_LMT_TYPE_ERR,
  CLI_SNOOP_PORT_MAX_LMT_ERR,
  CLI_SNOOP_PORT_PROFILE_ID_ERR,
  CLI_SNOOP_PROFILE_CRT_ERR,
  CLI_SNOOP_MVLAN_PROFILE_MAP_ERR,
  CLI_SNOOP_MVLAN_PROFILE_ACTION_ERR,
  CLI_SNOOP_MAX_MVLAN_ERR,
  CLI_SNOOP_INVALID_BLK_PORT_ERR,
  CLI_SNOOP_INVALID_RTR_PORT_ERR,
  CLI_SNOOP_LIMIT_MAC_MODE_ERR,
  CLI_SNOOP_PROFILE_MAC_MODE_ERR,
  CLI_SNOOP_BASE_BRIDGE_SNOOP_ENABLED,
  CLI_SNOOP_VLAN_GLOB_LEAVE_CONF_LEV_ERR,
  CLI_SNOOP_NON_MEMBER_RTR_PORTS,
  CLI_SNOOP_IGMP_PROXY_ENABLED_ERR,
  CLI_SNOOP_NO_VLAN_PRESENT,
  CLI_SNOOP_IS_RTR_PORT_ERR,
  CLI_SNOOP_ICCL_PORT_ERR,
  CLI_SNOOP_ICCL_MROUTER_PORT_ERR,
  CLI_SNOOP_MCLAG_PROXY_ENABLE_ERR,
  CLI_SNOOP_MCLAG_PROXY_REPORTING_DISABLE_ERR,
  CLI_SNOOP_ICCL_ROUTER_VERSION_ERR,
  CLI_SNOOP_MCLAG_SPARSE_MODE_ERR,
  CLI_SNOOP_STATIC_TIMEOUT_ENABLE_ERR,
  CLI_SNOOP_RESERVED_MULTICAST_ERR, 
  CLI_SNOOP_INVALID_VALUE_CONTROL_PLANE_DRIVEN,
  CLI_SNOOP_INVALID_MODE_CONTROL_PLANE_DRIVEN,
  CLI_SNOOP_MAX_RTR_PORT_ERR,
  CLI_SNOOP_INVALID_DBG_PKT,
  CLI_SNOOP_INVALID_DBG_ALL,
  CLI_SNOOP_INVALID_DBG_TRACE,
  CLI_SNOOP_MAX_ERR
};
/* The error strings should be places under the switch so as to avoid 
 * redifinition This will be visible only in modulecli.c file
 */
#ifdef __SNPCLI_C__

CONST CHR1  *SnoopCliErrString [] = {
    NULL,
    " \r% Invalid Port Number\r\n",
    " \r% GMRP or MMRP is NOT Disabled!!!\r\n",
    " \r% IGMP tunneling is enabled on a port!!!\r\n",
    " \r% Snoopoing cannot be enabled on a PCB/PEB!!!\r\n",
    " \r% Configuration failed!!!\r\n",
    " \r% Invalid Group address\r\n",
    " \r% Unknown Router Port(s) present\r\n",
    " \r% Disable IGS Proxy before enabling IGS Proxy-reporting\r\n",
    " \r% Disable IGS Proxy-reporting before enabling IGS Proxy\r\n",
    " \r% IGS Proxy cannot be configured as Non-Querier\r\n",
    " \r% Invalid max response value\r\n",
    " \r% Port(s) must be configured as mrouter port before changing Router port purge time-out interval or operating version\r\n",
    " \r% Invalid operating version for router port\r\n",
    " \r% Invalid old-querier interval for router port\r\n",
    " \r% Snoop Module is shutdown\r\n",
    " \r% Invalid Instance Id \r\n",
    " \r% Enhanced mode cannot be enabled in MAC forward mode \r\n",
    " \r% Sparse mode cannot be enabled in IP forwarding mode \r\n",
    " \r% Invalid InnerVlan Id \r\n",
    " \r% Enhanced Mode is disabled \r\n",
    " \r% Instance is configured in enhanced mode\r\n",
    " \r% Port conf entry is not found \r\n",
    " \r% Port does not exist \r\n",
    " \r% Invalid Leave process mode \r\n",
    " \r% Global Leave conf level is vlan based \r\n",
    " \r% Invalid Rate Limit value \r\n",
    " \r% Value should not be greater than Max groups \r\n",
    " \r% Value should not be greater than Max channels \r\n",
    " \r% Max Limit is greater than the system supported value \r\n",
    " \r% Invalid Profile Id \r\n",
    " \r% Profile is not created\r\n",
    " \r% Profile is already mapped to a VLAN\r\n",
    " \r% Only profiles with action as PERMIT, can be mapped for MVLAN\r\n",
    " \r% Max Multicast VLAN is already configured.\r\n",
    " \r% Unable to configure: blocked router port present\r\n",
    " \r% Unable to configure: router port present\r\n",
    " \r% Threshold limit cannot be assigned in MAC Fwd Mode\r\n",
    " \r% Filter profiles cannot be assigned in MAC Fwd Mode\r\n",
    " \r% Snooping Module cannot be enabled in Transparent Mode\r\n",
    " \r% Global Leave conf level is port based \r\n",
    " \r% Port(s) configured are not member ports of vlan\r\n",
    " \r% IGMP proxy must be disabled for router port configurations\r\n",
    " \r% Vlan does not exist\r\n",
    " \r% Port List contains router port\r\n",
    " \r% Fast leave cannot be enabled on ICCL interface\r\n",
    " \r% ICCL link cannot be removed from mrouter port list\r\n",
    " \r% When MC-LAG is enabled, Proxy mode can not be enabled\r\n",
    " \r% When MC-LAG is enabled, Proxy reporting can not be disabled\r\n",
    " \r% ICCL link always operates in V3 version\r\n",
    " \r% When MC-LAG is enabled, Sparse mode can not be enabled\r\n",
    " \r% Query Timeout interval cannot be configured for static mroute entry\r\n",
    " \r% Reserved multicast address is not allowed\r\n", 
    " \r% Invalid value for Enabling/Disabling Control Plane Driven Approach\r\n",
    " \r% Forwarding mode is not in IP Based Mode to enable control plane driven approach\r\n",
    " \r% Maximum multicast router ports reached in the system.\r\n",
    " \r% Pkt debug option can co-exist only with query/fwd/group options\r\n",
    " \r% Debug all option can co-exist with pkt debug option.\r\n",
    " \r% Entry or exit trace can not co-exist with other debug traces\r\n",
    "\r\n"
};

#endif
/*sys log error messages and their macros*/



/* function prototypes */
INT4 cli_process_snoop_cmd PROTO ((tCliHandle CliHandle, UINT4, ...));
INT4 cli_process_snoop_show_cmd PROTO ((tCliHandle CliHandle, UINT4, ...));

#endif /* _SNPCLI_H_ */

