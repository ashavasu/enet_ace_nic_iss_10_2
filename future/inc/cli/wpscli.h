/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: wpscli.h,v 1.2 2016/01/22 11:24:17 siva Exp $
 *
 * Description: WPS File.
 * *********************************************************************/

#ifndef WPSCLI_H
#define WPSCLI_H

#include "cli.h"
/* WPS CLI command constants. To add a new command ,a new definition needs to
 * be added to the list.
 */



enum
{
    CLI_WPS_TRACE_CHANGE = 1,        /* Changes the trace*/
    CLI_WPS_SET_STATUS,
    CLI_WPS_STATUS,
    CLI_WPS_ADDITIONALIE_SET,
    CLI_WPS_MAX_CMDS,
    CLI_WPS_SHOW_CONFIG,
    CLI_WPS_SHOW_AUTH_INFO,
    CLI_WPS_SHOW_STATUS,
    WPS_INIT_SHUT_TRC,
    WPS_MGMT_TRC,
    WPS_DATA_PATH_TRC,
    WPS_CONTROL_PATH_TRC,
    WPS_DUMP_TRC,
    WPS_OS_RESOURCE_TRC,
    WPS_ALL_FAILURE_TRC,
    WPS_BUFFER_TRC,
    CLI_WPS_DEBUG,
    CLI_WPS_NO_DEBUG,
};

#define WPS_CLEAR_FLAG_SET              1
#define WPS_CLEAR_FLAG_NOSET            0


#define WPS_ENABLED  1    /* Always true*/
#define WPS_DISABLED 2    /* Always true*/
#define WPS_PBC                        1    
#define WPS_PIN                        2    
#define WPS_ADDITIONLALIE_DISABLE      0    
#define WPS_CLI_TERMINATE               1    /* For \0*/
#define WPS_CLI_FIRST_ARG               1
#define WPS_CLI_SECOND_ARG              2
#define WPS_CLI_THIRD_ARG               3
#define WPS_CLI_TRACE_ADD               1 /* Macro to add trace*/
#define WPS_CLI_TRACE_DEL               2 /* Macro to delete trace*/
#define WPS_CLI_UINT8                   (2 * sizeof (UINT4))
#define WPS_CLI_SUITE_LEN               4    /* Specifies the number of 
                                                 bytes to be copied*/
#define WPS_CLI_DISPLAY_SUITE           6 
#define WPS_INIT_SHUT_TRC               INIT_SHUT_TRC     
#define WPS_MGMT_TRC                    MGMT_TRC          
#define WPS_DATA_PATH_TRC               DATA_PATH_TRC     
#define WPS_CONTROL_PATH_TRC            CONTROL_PLANE_TRC 
#define WPS_DUMP_TRC                    DUMP_TRC          
#define WPS_OS_RESOURCE_TRC             OS_RESOURCE_TRC   
#define WPS_ALL_FAILURE_TRC             ALL_FAILURE_TRC   
#define WPS_BUFFER_TRC                  BUFFER_TRC        
#define WPS_CLI_MAX_TRACE_LEN           200
#define WPS_CLI_MAC_ADDR_LEN            21
#define WPS_CLI_PROTOCOL_TYPE_LENGTH    10

/* PBC States Value */
#define WPS_PBC_SUCCESS                 1
#define WPS_PBC_OVERLAP                 2
#define WPS_PBC_INPROGRESS              3
#define WPS_PBC_INACTIVE                4
#define WPS_PBC_ERROR                   5

/* Error codes 
 * Error code values and strings are maintained inside the protocol module itself.
 */
enum {
    CLI_WPS_INVALID_SSID = 1,
    CLI_WPS_NO_STATIONS,
    CLI_WPS_NO_ENTRIES_TO_IFINDEX,
    CLI_WPS_NOT_ENABLED,
    CLI_WPS_INVALID_SA_TIMEOUT,
    CLI_WPS_INVALID_WLAN_ID,
    CLI_WPS_INVALID_CLEAR_FIELD,
    CLI_WPS_WLAN_NOT_CREATED,
    CLI_WPS_COMMAND_NOT_SUPPORTED,
    CLI_WPS_MAX_ERR
};
/* The error strings should be places under the switch so as to avoid redifinition
 * This will be visible only in modulecli.c file
 */
#ifdef WPSCLI_C

CONST CHR1  *WpsCliErrString [] = {
    NULL,
    "% No such SSID exists \r\n",
    "% No stations exists \r\n",
    "% No entries corresponding to Ifindex \r\n",
    "% Wps is not enabled \r\n",
    "% Invalid SA timeout \r\n",
    "% Invalid WLAN id \r\n",
    "% Invalid Clear field\r\n",
    "% WLAN is not created\r\n",
    "% Command Not Supported"
};

#endif /*WPSCLI_C*/
/*extern declarations */


INT4
cli_process_wps_command (tCliHandle CliHandle, INT4 u4Command, ...);

INT4 WpsSetTrace             PROTO ((tCliHandle CliHandle, INT4 i4TraceStats, 
                                      INT4 i4Trace));
INT4
WpsCliSetDebugs ( tCliHandle CliHandle,
                   INT4 i4CliTraceVal  ,
                   INT4 u1TraceFlag    );
INT4
WpsCliSetStatus(tCliHandle CliHandle, INT1 u1Status, UINT4 u4ProfileId, UINT4 u4ApProfileId);

INT4
WpsCliSetMethod(tCliHandle CliHandle, INT1 u1Status, UINT4 u4ProfileId);

 INT4
WpsCliSetAdditionalIE(tCliHandle CliHandle, INT1 u1Status, UINT4 u4ProfileId, UINT4 u4ApProfileId);
INT4
WpsCliShowConfig (tCliHandle CliHandle, UINT4 u4ProfileId, UINT4 u4WtpProfileId);

INT4 WpsCliShowPBCStatus (tCliHandle CliHandle, UINT4 u4ProfileId, UINT4 u4ApProfileId);

INT4
WpsShowAuthInfo (tCliHandle CliHandle, UINT4 u4ProfileId);

INT4
WpsCliShowStatsDisplay (tCliHandle CliHandle, UINT4 u4ProfileIdValue,
                         UINT4 u4WPSStatsIndexValue, UINT4 u4WlanId);
INT1 nmhGetFsWPSSetStatus(INT4 i4IfIndex, UINT4 u4CapwapBaseWtpProfileId,
                          INT4 *pi4RetValFsWPSSetStatus);
INT1 nmhGetFsWPSAdditionalIE(INT4 i4IfIndex, UINT4 u4CapwapBaseWtpProfileId,
                             INT4 *pi4RetValFsWPSAdditionalIE);
INT1 nmhGetFsWPSPushButtonStatus(INT4 i4IfIndex, UINT4 u4CapwapBaseWtpProfileId,
                                 INT4 *pi4RetValFsWPSPushButtonStatus);
INT1 nmhGetFsWPSTraceOption(INT4 *pi4RetValFsWPSTraceOption);
INT1 nmhTestv2FsWPSTraceOption(UINT4 *pu4ErrorCode , INT4 i4TestValFsWPSTraceOption);

INT1 nmhGetNextIndexFsWPSTable(INT4 i4IfIndex ,INT4 *pi4NextIfIndex, 
  UINT4 u4CapwapBaseWtpProfileId,UINT4 *pCapwapBaseWtpProfileId );

INT1
nmhTestv2FsWPSSetStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));
INT1
nmhSetFsWPSSetStatus ARG_LIST((INT4  , UINT4  ,INT4 ));
INT1
nmhTestv2FsWPSAdditionalIE ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));
INT1
nmhSetFsWPSAdditionalIE ARG_LIST((INT4  , UINT4  ,INT4 ));
INT4 WpsShowRunningConfig (tCliHandle CliHandle, UINT4 u4WlanIndex);
INT4 WpsCliShowRunningConfig (tCliHandle CliHandle, UINT4 u4ProfileId, UINT4 u4ApProfileId,UINT4 u4WlanId);
#endif /*WPSCLI_H*/
