/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdmpfcli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 MplsFTNIndex[13];
extern UINT4 MplsFTNRowStatus[13];
extern UINT4 MplsFTNDescr[13];
extern UINT4 MplsFTNMask[13];
extern UINT4 MplsFTNAddrType[13];
extern UINT4 MplsFTNSourceAddrMin[13];
extern UINT4 MplsFTNSourceAddrMax[13];
extern UINT4 MplsFTNDestAddrMin[13];
extern UINT4 MplsFTNDestAddrMax[13];
extern UINT4 MplsFTNSourcePortMin[13];
extern UINT4 MplsFTNSourcePortMax[13];
extern UINT4 MplsFTNDestPortMin[13];
extern UINT4 MplsFTNDestPortMax[13];
extern UINT4 MplsFTNProtocol[13];
extern UINT4 MplsFTNDscp[13];
extern UINT4 MplsFTNActionType[13];
extern UINT4 MplsFTNActionPointer[13];
extern UINT4 MplsFTNStorageType[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetMplsFTNRowStatus(u4MplsFTNIndex ,i4SetValMplsFTNRowStatus)	\
	nmhSetCmn(MplsFTNRowStatus, 13, MplsFTNRowStatusSet, NULL, NULL, 0, 1, 1, "%u %i", u4MplsFTNIndex ,i4SetValMplsFTNRowStatus)
#define nmhSetMplsFTNDescr(u4MplsFTNIndex ,pSetValMplsFTNDescr)	\
	nmhSetCmn(MplsFTNDescr, 13, MplsFTNDescrSet, NULL, NULL, 0, 0, 1, "%u %s", u4MplsFTNIndex ,pSetValMplsFTNDescr)
#define nmhSetMplsFTNMask(u4MplsFTNIndex ,pSetValMplsFTNMask)	\
	nmhSetCmn(MplsFTNMask, 13, MplsFTNMaskSet, NULL, NULL, 0, 0, 1, "%u %s", u4MplsFTNIndex ,pSetValMplsFTNMask)
#define nmhSetMplsFTNAddrType(u4MplsFTNIndex ,i4SetValMplsFTNAddrType)	\
	nmhSetCmn(MplsFTNAddrType, 13, MplsFTNAddrTypeSet, NULL, NULL, 0, 0, 1, "%u %i", u4MplsFTNIndex ,i4SetValMplsFTNAddrType)
#define nmhSetMplsFTNSourceAddrMin(u4MplsFTNIndex ,pSetValMplsFTNSourceAddrMin)	\
	nmhSetCmn(MplsFTNSourceAddrMin, 13, MplsFTNSourceAddrMinSet, NULL, NULL, 0, 0, 1, "%u %s", u4MplsFTNIndex ,pSetValMplsFTNSourceAddrMin)
#define nmhSetMplsFTNSourceAddrMax(u4MplsFTNIndex ,pSetValMplsFTNSourceAddrMax)	\
	nmhSetCmn(MplsFTNSourceAddrMax, 13, MplsFTNSourceAddrMaxSet, NULL, NULL, 0, 0, 1, "%u %s", u4MplsFTNIndex ,pSetValMplsFTNSourceAddrMax)
#define nmhSetMplsFTNDestAddrMin(u4MplsFTNIndex ,pSetValMplsFTNDestAddrMin)	\
	nmhSetCmn(MplsFTNDestAddrMin, 13, MplsFTNDestAddrMinSet, NULL, NULL, 0, 0, 1, "%u %s", u4MplsFTNIndex ,pSetValMplsFTNDestAddrMin)
#define nmhSetMplsFTNDestAddrMax(u4MplsFTNIndex ,pSetValMplsFTNDestAddrMax)	\
	nmhSetCmn(MplsFTNDestAddrMax, 13, MplsFTNDestAddrMaxSet, NULL, NULL, 0, 0, 1, "%u %s", u4MplsFTNIndex ,pSetValMplsFTNDestAddrMax)
#define nmhSetMplsFTNSourcePortMin(u4MplsFTNIndex ,u4SetValMplsFTNSourcePortMin)	\
	nmhSetCmn(MplsFTNSourcePortMin, 13, MplsFTNSourcePortMinSet, NULL, NULL, 0, 0, 1, "%u %u", u4MplsFTNIndex ,u4SetValMplsFTNSourcePortMin)
#define nmhSetMplsFTNSourcePortMax(u4MplsFTNIndex ,u4SetValMplsFTNSourcePortMax)	\
	nmhSetCmn(MplsFTNSourcePortMax, 13, MplsFTNSourcePortMaxSet, NULL, NULL, 0, 0, 1, "%u %u", u4MplsFTNIndex ,u4SetValMplsFTNSourcePortMax)
#define nmhSetMplsFTNDestPortMin(u4MplsFTNIndex ,u4SetValMplsFTNDestPortMin)	\
	nmhSetCmn(MplsFTNDestPortMin, 13, MplsFTNDestPortMinSet, NULL, NULL, 0, 0, 1, "%u %u", u4MplsFTNIndex ,u4SetValMplsFTNDestPortMin)
#define nmhSetMplsFTNDestPortMax(u4MplsFTNIndex ,u4SetValMplsFTNDestPortMax)	\
	nmhSetCmn(MplsFTNDestPortMax, 13, MplsFTNDestPortMaxSet, NULL, NULL, 0, 0, 1, "%u %u", u4MplsFTNIndex ,u4SetValMplsFTNDestPortMax)
#define nmhSetMplsFTNProtocol(u4MplsFTNIndex ,i4SetValMplsFTNProtocol)	\
	nmhSetCmn(MplsFTNProtocol, 13, MplsFTNProtocolSet, NULL, NULL, 0, 0, 1, "%u %i", u4MplsFTNIndex ,i4SetValMplsFTNProtocol)
#define nmhSetMplsFTNDscp(u4MplsFTNIndex ,i4SetValMplsFTNDscp)	\
	nmhSetCmn(MplsFTNDscp, 13, MplsFTNDscpSet, NULL, NULL, 0, 0, 1, "%u %i", u4MplsFTNIndex ,i4SetValMplsFTNDscp)
#define nmhSetMplsFTNActionType(u4MplsFTNIndex ,i4SetValMplsFTNActionType)	\
	nmhSetCmn(MplsFTNActionType, 13, MplsFTNActionTypeSet, NULL, NULL, 0, 0, 1, "%u %i", u4MplsFTNIndex ,i4SetValMplsFTNActionType)
#define nmhSetMplsFTNActionPointer(u4MplsFTNIndex ,pSetValMplsFTNActionPointer)	\
	nmhSetCmn(MplsFTNActionPointer, 13, MplsFTNActionPointerSet, NULL, NULL, 0, 0, 1, "%u %o", u4MplsFTNIndex ,pSetValMplsFTNActionPointer)
#define nmhSetMplsFTNStorageType(u4MplsFTNIndex ,i4SetValMplsFTNStorageType)	\
	nmhSetCmn(MplsFTNStorageType, 13, MplsFTNStorageTypeSet, NULL, NULL, 0, 0, 1, "%u %i", u4MplsFTNIndex ,i4SetValMplsFTNStorageType)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 MplsFTNMapIndex[13];
extern UINT4 MplsFTNMapPrevIndex[13];
extern UINT4 MplsFTNMapCurrIndex[13];
extern UINT4 MplsFTNMapRowStatus[13];
extern UINT4 MplsFTNMapStorageType[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetMplsFTNMapRowStatus(i4MplsFTNMapIndex , u4MplsFTNMapPrevIndex , u4MplsFTNMapCurrIndex ,i4SetValMplsFTNMapRowStatus)	\
	nmhSetCmn(MplsFTNMapRowStatus, 13, MplsFTNMapRowStatusSet, NULL, NULL, 0, 1, 3, "%i %u %u %i", i4MplsFTNMapIndex , u4MplsFTNMapPrevIndex , u4MplsFTNMapCurrIndex ,i4SetValMplsFTNMapRowStatus)
#define nmhSetMplsFTNMapStorageType(i4MplsFTNMapIndex , u4MplsFTNMapPrevIndex , u4MplsFTNMapCurrIndex ,i4SetValMplsFTNMapStorageType)	\
	nmhSetCmn(MplsFTNMapStorageType, 13, MplsFTNMapStorageTypeSet, NULL, NULL, 0, 0, 3, "%i %u %u %i", i4MplsFTNMapIndex , u4MplsFTNMapPrevIndex , u4MplsFTNMapCurrIndex ,i4SetValMplsFTNMapStorageType)

#endif
