/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdsnncli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 SnmpNotifyName[11];
extern UINT4 SnmpNotifyTag[11];
extern UINT4 SnmpNotifyType[11];
extern UINT4 SnmpNotifyStorageType[11];
extern UINT4 SnmpNotifyRowStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetSnmpNotifyTag(pSnmpNotifyName ,pSetValSnmpNotifyTag)	\
	nmhSetCmn(SnmpNotifyTag, 11, SnmpNotifyTagSet, NULL, NULL, 0, 0, 1, "%s %s", pSnmpNotifyName ,pSetValSnmpNotifyTag)
#define nmhSetSnmpNotifyType(pSnmpNotifyName ,i4SetValSnmpNotifyType)	\
	nmhSetCmn(SnmpNotifyType, 11, SnmpNotifyTypeSet, NULL, NULL, 0, 0, 1, "%s %i", pSnmpNotifyName ,i4SetValSnmpNotifyType)
#define nmhSetSnmpNotifyStorageType(pSnmpNotifyName ,i4SetValSnmpNotifyStorageType)	\
	nmhSetCmn(SnmpNotifyStorageType, 11, SnmpNotifyStorageTypeSet, NULL, NULL, 0, 0, 1, "%s %i", pSnmpNotifyName ,i4SetValSnmpNotifyStorageType)
#define nmhSetSnmpNotifyRowStatus(pSnmpNotifyName ,i4SetValSnmpNotifyRowStatus)	\
	nmhSetCmn(SnmpNotifyRowStatus, 11, SnmpNotifyRowStatusSet, NULL, NULL, 0, 1, 1, "%s %i", pSnmpNotifyName ,i4SetValSnmpNotifyRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 SnmpNotifyFilterProfileName[11];
extern UINT4 SnmpNotifyFilterProfileStorType[11];
extern UINT4 SnmpNotifyFilterProfileRowStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetSnmpNotifyFilterProfileName(pSnmpTargetParamsName ,pSetValSnmpNotifyFilterProfileName)	\
	nmhSetCmn(SnmpNotifyFilterProfileName, 11, SnmpNotifyFilterProfileNameSet, NULL, NULL, 0, 0, 1, "%s %s", pSnmpTargetParamsName ,pSetValSnmpNotifyFilterProfileName)
#define nmhSetSnmpNotifyFilterProfileStorType(pSnmpTargetParamsName ,i4SetValSnmpNotifyFilterProfileStorType)	\
	nmhSetCmn(SnmpNotifyFilterProfileStorType, 11, SnmpNotifyFilterProfileStorTypeSet, NULL, NULL, 0, 0, 1, "%s %i", pSnmpTargetParamsName ,i4SetValSnmpNotifyFilterProfileStorType)
#define nmhSetSnmpNotifyFilterProfileRowStatus(pSnmpTargetParamsName ,i4SetValSnmpNotifyFilterProfileRowStatus)	\
	nmhSetCmn(SnmpNotifyFilterProfileRowStatus, 11, SnmpNotifyFilterProfileRowStatusSet, NULL, NULL, 0, 1, 1, "%s %i", pSnmpTargetParamsName ,i4SetValSnmpNotifyFilterProfileRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 SnmpNotifyFilterSubtree[11];
extern UINT4 SnmpNotifyFilterMask[11];
extern UINT4 SnmpNotifyFilterType[11];
extern UINT4 SnmpNotifyFilterStorageType[11];
extern UINT4 SnmpNotifyFilterRowStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetSnmpNotifyFilterMask(pSnmpNotifyFilterProfileName , pSnmpNotifyFilterSubtree ,pSetValSnmpNotifyFilterMask)	\
	nmhSetCmn(SnmpNotifyFilterMask, 11, SnmpNotifyFilterMaskSet, NULL, NULL, 0, 0, 2, "%s %o %s", pSnmpNotifyFilterProfileName , pSnmpNotifyFilterSubtree ,pSetValSnmpNotifyFilterMask)
#define nmhSetSnmpNotifyFilterType(pSnmpNotifyFilterProfileName , pSnmpNotifyFilterSubtree ,i4SetValSnmpNotifyFilterType)	\
	nmhSetCmn(SnmpNotifyFilterType, 11, SnmpNotifyFilterTypeSet, NULL, NULL, 0, 0, 2, "%s %o %i", pSnmpNotifyFilterProfileName , pSnmpNotifyFilterSubtree ,i4SetValSnmpNotifyFilterType)
#define nmhSetSnmpNotifyFilterStorageType(pSnmpNotifyFilterProfileName , pSnmpNotifyFilterSubtree ,i4SetValSnmpNotifyFilterStorageType)	\
	nmhSetCmn(SnmpNotifyFilterStorageType, 11, SnmpNotifyFilterStorageTypeSet, NULL, NULL, 0, 0, 2, "%s %o %i", pSnmpNotifyFilterProfileName , pSnmpNotifyFilterSubtree ,i4SetValSnmpNotifyFilterStorageType)
#define nmhSetSnmpNotifyFilterRowStatus(pSnmpNotifyFilterProfileName , pSnmpNotifyFilterSubtree ,i4SetValSnmpNotifyFilterRowStatus)	\
	nmhSetCmn(SnmpNotifyFilterRowStatus, 11, SnmpNotifyFilterRowStatusSet, NULL, NULL, 0, 1, 2, "%s %o %i", pSnmpNotifyFilterProfileName , pSnmpNotifyFilterSubtree ,i4SetValSnmpNotifyFilterRowStatus)

#endif
