/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmprscli.h,v 1.13 2016/03/06 10:04:20 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIRstGlobalTrace[10];
extern UINT4 FsMIRstGlobalDebug[10];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIDot1wFutureRstContextId[12];
extern UINT4 FsMIRstSystemControl[12];
extern UINT4 FsMIRstModuleStatus[12];
extern UINT4 FsMIRstTraceOption[12];
extern UINT4 FsMIRstDebugOption[12];
extern UINT4 FsMIRstDynamicPathcostCalculation[12];
extern UINT4 FsMIRstCalcPortPathCostOnSpeedChg[12];
extern UINT4 FsMIRstFlushInterval[12];
extern UINT4 FsMIRstFlushIndicationThreshold[12];
extern UINT4 FsMIRstFwdDelayAltPortRoleTrOptimization[12];
extern UINT4 FsMIRstPort[12];
extern UINT4 FsMIRstPortAutoEdge[12];
extern UINT4 FsMIRstPortRestrictedRole[12];
extern UINT4 FsMIRstPortRestrictedTCN[12];
extern UINT4 FsMIRstPortEnableBPDURx[12];
extern UINT4 FsMIRstPortEnableBPDUTx[12];
extern UINT4 FsMIRstPortPseudoRootId[12];
extern UINT4 FsMIRstPortIsL2Gp[12];
extern UINT4 FsMIRstPortLoopGuard[12];
extern UINT4 FsMIRstPortRowStatus[12];
extern UINT4 FsMIRstPortBpduGuard[12];
extern UINT4 FsMIRstPortErrorRecovery[12];
extern UINT4 FsMIRstPortStpModeDot1wEnabled[12];
extern UINT4 FsMIRstBpduGuard[12];
extern UINT4 FsMIRstPortRootGuard[12];
extern UINT4 FsMIRstPortBpduGuardAction [12];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIRstSetGlobalTraps[10];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIRstSetTraps[12];
extern UINT4 FsMIRstStpPerfStatus[12];
