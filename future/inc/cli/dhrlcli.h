/* $Id: dhrlcli.h,v 1.15 2015/08/06 05:45:08 siva Exp $*/
#ifndef __DHRLCLI_H__
#define __DHRLCLI_H__

/* DHCP RELAY CLI command constants. To add a new command ,a new definition needs to be added to the list.
 */

#define MAX_DHCP_RELAY_SPECIAL_CAHRACTERS  3
#define MAX_DHCP_RELAY_ADDRESS             20
#define DHCPR_CLI_MAX_ARGS                 10
#define DHCPR_CLI_NO_CKT_ID                0
#define DHCPR_CLI_SET_REM_ID               1
   /* Dhcp option 82- circuit Id types */
#define DHCP_CIRCUITID_TYPE_INTERFACE_INDEX      0x0001
#define DHCP_CIRCUITID_TYPE_VLANID               0x0002
#define DHCP_CIRCUITID_TYPE_PHYPORT_LAGPORT      0x0004

enum {
    CLI_DHCPR_CIRCUIT_ID = 1,/*Interface Level configuration Starts */
    CLI_DHCPR_NO_CIRCUIT_ID,
    CLI_DHCPR_REMOTE_ID,
    CLI_DHCPR_NO_REMOTE_ID,/*Interface Level configuration Ends*/
    CLI_DHCPR_DEBUG,/* Context Specific Debug commands Starts */
    CLI_DHCPR_NO_DEBUG,/* Context Specific Debug commands Ends */
    CLI_DHCPR_NO_SERVICE,/* Global Configurations Starts */
    CLI_DHCPR_SERVICE,
    CLI_DHCPR_NO_SERVER,
    CLI_DHCPR_SERVER,
    CLI_DHCPR_INFO_OPTION,
    CLI_DHCPR_NO_INFO_OPTION,
    CLI_DHCPR_CIRCUIT_OPTION,/* Global Configurations Starts */
    CLI_DHCPR_CLEAR_STATS,/* Show/Clear commands begins */
    CLI_DHCPR_SHOW_INFO,
    CLI_DHCPR_SHOW_SERVER,
    CLI_DHCPR_SHOW_IF_INFO,/* Show/Clear commands Ends */
    CLI_DHCPR_MAX_COMMANDS
};
     
/* Command constants used in debug dhcp relay command */
#define DHCPR_DEBUG_NONE                   0
#define DHCPR_DEBUG_DEF_VALUE              1
#define DHCPR_DEBUG_ALL                    0x000000ff
#define DHCPR_DEBUG_ERRORS                 ALL_FAILURE_TRC

/* Error codes */
enum {
    CLI_DHCPR_NO_SERVER_ERR = 1,
    CLI_DHCPR_DUPLICATE_CKTID,
    CLI_DHCPR_INVALID_CKTID,
    CLI_DHCPR_DUPLICATE_REMID,
    CLI_DHCPR_INVALID_REMID,
    CLI_DHCPR_WRONG_LENGTH,
    CLI_DHCPR_RCHD_MAX_SERVERS,
    CLI_DHCPR_NO_ACCESS_CIRCUIT_ID,
    CLI_DHCPR_INVALID_SERVER,
    CLI_DHCPR_START_FAILED,
    CLI_DHCPR_STOP_FAILED,
    CLI_DHCPR_NO_RELAY,
    CLI_DHCPR_MAX_RLY_CXT,
    CLI_DHCPR_MAX_ERR
};  

#ifdef __DHRLCLI_C__

CONST CHR1 *DhcprCliErrString [] = {
    NULL,
    "\r% No such DHCP server defined\r\n",
    "\r% This circuit id is already assigned to a different interface\r\n",
    "\r% Enter a valid ckt id the id should be a valid L3 interface index\r\n",
    "\r% This remote id is already assigned to a different interface\r\n",
    "\r% Enter a valid remote id \r\n",
    "\r% Entered remote id exceeds the maximum allowed length\r\n",
    "\r% Max Servers Reached,No more Servers can be Configured\r\n",
    "\r% Enable the circuit id sub-option control\r\n",
    "\r% Invalid DHCP server IP address.\r\n",
    "\r% Failed to start the DHCP Relay on this context.\r\n",
 "\r% Failed to Disable the DHCP Relay on this context.\r\n",
 "\r% No Relay Enabled on this context.\r\n",
    "\r% Maximum relay instances reached; Relay configurations cannot be done on this context\r\n"
};
#endif


INT4 cli_process_dhcpr_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Comamnd, ...));


#endif /* _DHRLCLI_H_ */


