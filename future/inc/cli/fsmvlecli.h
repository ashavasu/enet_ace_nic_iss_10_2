/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmvlecli.h,v 1.9 2015/06/05 09:40:42 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIVlanContextId[12];
extern UINT4 FsMIVlanBridgeMode[12];
#define nmhSetFsMIVlanBridgeMode(i4FsMIVlanContextId ,i4SetValFsMIVlanBridgeMode) \
 nmhSetCmn(FsMIVlanBridgeMode, 12, FsMIVlanBridgeModeSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %i", i4FsMIVlanContextId ,i4SetValFsMIVlanBridgeMode)


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIVlanTunnelBpduPri[12];
extern UINT4 FsMIVlanTunnelStpAddress[12];
extern UINT4 FsMIVlanTunnelLacpAddress[12];
extern UINT4 FsMIVlanTunnelDot1xAddress[12];
extern UINT4 FsMIVlanTunnelGvrpAddress[12];
extern UINT4 FsMIVlanTunnelGmrpAddress[12];
extern UINT4 FsMIVlanTunnelMvrpAddress[12];
extern UINT4 FsMIVlanTunnelMmrpAddress[12];
extern UINT4 FsMIVlanTunnelElmiAddress[12];
extern UINT4 FsMIVlanTunnelLldpAddress[12];
extern UINT4 FsMIVlanTunnelEcfmAddress[12];
extern UINT4 FsMIVlanTunnelEoamAddress[12];
extern UINT4 FsMIVlanTunnelIgmpAddress[12];

#define nmhSetFsMIVlanTunnelBpduPri(i4FsMIVlanContextId ,i4SetValFsMIVlanTunnelBpduPri) \
 nmhSetCmn(FsMIVlanTunnelBpduPri, 12, FsMIVlanTunnelBpduPriSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %i", i4FsMIVlanContextId ,i4SetValFsMIVlanTunnelBpduPri)
#define nmhSetFsMIVlanTunnelStpAddress(i4FsMIVlanContextId ,SetValFsMIVlanTunnelStpAddress) \
 nmhSetCmn(FsMIVlanTunnelStpAddress, 12, FsMIVlanTunnelStpAddressSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %m", i4FsMIVlanContextId ,SetValFsMIVlanTunnelStpAddress)
#define nmhSetFsMIVlanTunnelLacpAddress(i4FsMIVlanContextId ,SetValFsMIVlanTunnelLacpAddress) \
 nmhSetCmn(FsMIVlanTunnelLacpAddress, 12, FsMIVlanTunnelLacpAddressSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %m", i4FsMIVlanContextId ,SetValFsMIVlanTunnelLacpAddress)
#define nmhSetFsMIVlanTunnelDot1xAddress(i4FsMIVlanContextId ,SetValFsMIVlanTunnelDot1xAddress) \
 nmhSetCmn(FsMIVlanTunnelDot1xAddress, 12, FsMIVlanTunnelDot1xAddressSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %m", i4FsMIVlanContextId ,SetValFsMIVlanTunnelDot1xAddress)
#define nmhSetFsMIVlanTunnelGvrpAddress(i4FsMIVlanContextId ,SetValFsMIVlanTunnelGvrpAddress) \
 nmhSetCmn(FsMIVlanTunnelGvrpAddress, 12, FsMIVlanTunnelGvrpAddressSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %m", i4FsMIVlanContextId ,SetValFsMIVlanTunnelGvrpAddress)
#define nmhSetFsMIVlanTunnelGmrpAddress(i4FsMIVlanContextId ,SetValFsMIVlanTunnelGmrpAddress) \
 nmhSetCmn(FsMIVlanTunnelGmrpAddress, 12, FsMIVlanTunnelGmrpAddressSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %m", i4FsMIVlanContextId ,SetValFsMIVlanTunnelGmrpAddress)

#define nmhSetFsMIVlanTunnelMvrpAddress(i4FsMIVlanContextId ,SetValFsMIVlanTunneelMvrpAddress)   \
    nmhSetCmn(FsMIVlanTunnelMvrpAddress, 12, FsMIVlanTunnelMvrpAddressSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %m", i4FsMIVlanContextId ,SetValFsMIVlanTunnelMvrpAddress)

#define nmhSetFsMIVlanTunnelMmrpAddress(i4FsMIVlanContextId ,SetValFsMIVlanTunnelMmrpAddress)   \
    nmhSetCmn(FsMIVlanTunnelMmrpAddress, 12, FsMIVlanTunnelMmrpAddressSet, VlanLLock, VlanUnLock, 0, 0, 1, "%i %m", i4FsMIVlanContextId ,SetValFsMIVlanTunnelMmrpAddress)

#define nmhSetFsMIVlanTunnelElmiAddress(i4FsMIVlanContextId ,SetValFsMIVlanTunnelElmiAddress)   \
    nmhSetCmn(FsMIVlanTunnelElmiAddress, 12, FsMIVlanTunnelElmiAddressSet, VlanLLock, VlanUnLock, 0, 0, 1, "%i %m", i4FsMIVlanContextId ,SetValFsMIVlanTunnelElmiAddress)

#define nmhSetFsMIVlanTunnelLldpAddress(i4FsMIVlanContextId ,SetValFsMIVlanTunnelLldpAddress)   \
    nmhSetCmn(FsMIVlanTunnelLldpAddress, 12, FsMIVlanTunnelLldpAddressSet, VlanLLock, VlanUnLock, 0, 0, 1, "%i %m", i4FsMIVlanContextId ,SetValFsMIVlanTunnelLldpAddress)

#define nmhSetFsMIVlanTunnelEcfmAddress(i4FsMIVlanContextId ,SetValFsMIVlanTunnelEcfmAddress)   \
    nmhSetCmn(FsMIVlanTunnelEcfmAddress, 12, FsMIVlanTunnelEcfmAddressSet, VlanLLock, VlanUnLock, 0, 0, 1, "%i %m", i4FsMIVlanContextId ,SetValFsMIVlanTunnelEcfmAddress)

#define nmhSetFsMIVlanTunnelEoamAddress(i4FsMIVlanContextId ,SetValFsMIVlanTunnelEoamAddress)   \
    nmhSetCmn(FsMIVlanTunnelEoamAddress, 12, FsMIVlanTunnelEoamAddressSet, NULL, NULL, 0, 0, 1, "%i %m", i4FsMIVlanContextId ,SetValFsMIVlanTunnelEoamAddress)

#define nmhSetFsMIVlanTunnelIgmpAddress(i4FsMIVlanContextId ,SetValFsMIVlanTunnelIgmpAddress)   \
    nmhSetCmn(FsMIVlanTunnelIgmpAddress, 12, FsMIVlanTunnelIgmpAddressSet, NULL, NULL, 0, 0, 1, "%i %m", i4FsMIVlanContextId ,SetValFsMIVlanTunnelIgmpAddress)

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIVlanPort[12];
extern UINT4 FsMIVlanTunnelStatus[12];
#define nmhSetFsMIVlanTunnelStatus(i4FsMIVlanPort ,i4SetValFsMIVlanTunnelStatus) \
 nmhSetCmn(FsMIVlanTunnelStatus, 12, FsMIVlanTunnelStatusSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %i", i4FsMIVlanPort ,i4SetValFsMIVlanTunnelStatus)


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIVlanTunnelProtocolDot1x[12];
extern UINT4 FsMIVlanTunnelProtocolLacp[12];
extern UINT4 FsMIVlanTunnelProtocolStp[12];
extern UINT4 FsMIVlanTunnelProtocolGvrp[12];
extern UINT4 FsMIVlanTunnelProtocolGmrp[12];
extern UINT4 FsMIVlanTunnelProtocolIgmp[12];
extern UINT4 FsMIVlanTunnelProtocolMvrp[12];
extern UINT4 FsMIVlanTunnelProtocolMmrp[12];
extern UINT4 FsMIVlanTunnelProtocolElmi[12];
extern UINT4 FsMIVlanTunnelProtocolLldp[12];
extern UINT4 FsMIVlanTunnelProtocolEcfm[12];
extern UINT4 FsMIVlanTunnelOverrideOption[12];
extern UINT4 FsMIVlanTunnelProtocolEoam[12];

#define nmhSetFsMIVlanTunnelProtocolDot1x(i4FsMIVlanPort ,i4SetValFsMIVlanTunnelProtocolDot1x) \
 nmhSetCmn(FsMIVlanTunnelProtocolDot1x, 12, FsMIVlanTunnelProtocolDot1xSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %i", i4FsMIVlanPort ,i4SetValFsMIVlanTunnelProtocolDot1x)
#define nmhSetFsMIVlanTunnelProtocolLacp(i4FsMIVlanPort ,i4SetValFsMIVlanTunnelProtocolLacp) \
 nmhSetCmn(FsMIVlanTunnelProtocolLacp, 12, FsMIVlanTunnelProtocolLacpSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %i", i4FsMIVlanPort ,i4SetValFsMIVlanTunnelProtocolLacp)
#define nmhSetFsMIVlanTunnelProtocolStp(i4FsMIVlanPort ,i4SetValFsMIVlanTunnelProtocolStp) \
 nmhSetCmn(FsMIVlanTunnelProtocolStp, 12, FsMIVlanTunnelProtocolStpSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %i", i4FsMIVlanPort ,i4SetValFsMIVlanTunnelProtocolStp)
#define nmhSetFsMIVlanTunnelProtocolGvrp(i4FsMIVlanPort ,i4SetValFsMIVlanTunnelProtocolGvrp) \
 nmhSetCmn(FsMIVlanTunnelProtocolGvrp, 12, FsMIVlanTunnelProtocolGvrpSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %i", i4FsMIVlanPort ,i4SetValFsMIVlanTunnelProtocolGvrp)
#define nmhSetFsMIVlanTunnelProtocolGmrp(i4FsMIVlanPort ,i4SetValFsMIVlanTunnelProtocolGmrp) \
 nmhSetCmn(FsMIVlanTunnelProtocolGmrp, 12, FsMIVlanTunnelProtocolGmrpSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %i", i4FsMIVlanPort ,i4SetValFsMIVlanTunnelProtocolGmrp)
#define nmhSetFsMIVlanTunnelProtocolIgmp(i4FsMIVlanPort ,i4SetValFsMIVlanTunnelProtocolIgmp) \
 nmhSetCmn(FsMIVlanTunnelProtocolIgmp, 12, FsMIVlanTunnelProtocolIgmpSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %i", i4FsMIVlanPort ,i4SetValFsMIVlanTunnelProtocolIgmp)

#define nmhSetFsMIVlanTunnelProtocolMvrp(i4FsMIVlanPort ,i4SetValFsMIVlanTunnelPProtocolMvrp)    \
    nmhSetCmn(FsMIVlanTunnelProtocolMvrp, 12, FsMIVlanTunnelProtocolMvrpSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %i", i4FsMIVlanPort ,i4SetValFsMIVlanTunnelProtocolMvrp)
#define nmhSetFsMIVlanTunnelProtocolMmrp(i4FsMIVlanPort ,i4SetValFsMIVlanTunnelProtocolMmrp)    \
    nmhSetCmn(FsMIVlanTunnelProtocolMmrp, 12, FsMIVlanTunnelProtocolMmrpSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %i", i4FsMIVlanPort ,i4SetValFsMIVlanTunnelProtocolMmrp)

#define nmhSetFsMIVlanTunnelProtocolElmi(i4FsMIVlanPort ,i4SetValFsMIVlanTunnelProtocolElmi)    \
    nmhSetCmn(FsMIVlanTunnelProtocolElmi, 12, FsMIVlanTunnelProtocolElmiSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %i", i4FsMIVlanPort ,i4SetValFsMIVlanTunnelProtocolElmi)

#define nmhSetFsMIVlanTunnelProtocolLldp(i4FsMIVlanPort ,i4SetValFsMIVlanTunnelProtocolLldp)    \
    nmhSetCmn(FsMIVlanTunnelProtocolLldp, 12, FsMIVlanTunnelProtocolLldpSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %i", i4FsMIVlanPort ,i4SetValFsMIVlanTunnelProtocolLldp)

#define nmhSetFsMIVlanTunnelProtocolEcfm(i4FsMIVlanPort ,i4SetValFsMIVlanTunnelProtocolEcfm)    \
    nmhSetCmn(FsMIVlanTunnelProtocolEcfm, 12, FsMIVlanTunnelProtocolEcfmSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %i", i4FsMIVlanPort ,i4SetValFsMIVlanTunnelProtocolEcfm)

#define nmhSetFsMIVlanTunnelOverrideOption(i4FsMIVlanPort ,i4SetValFsMIVlanTunnelOverrideOption)        \
        nmhSetCmn(FsMIVlanTunnelOverrideOption, 12, FsMIVlanTunnelOverrideOptionSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %i", i4FsMIVlanPort ,i4SetValFsMIVlanTunnelOverrideOption)

#define nmhSetFsMIVlanTunnelProtocolEoam(i4FsMIVlanPort ,i4SetValFsMIVlanTunnelProtocolEoam)    \
    nmhSetCmn(FsMIVlanTunnelProtocolEoam, 12, FsMIVlanTunnelProtocolEoamSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsMIVlanPort ,i4SetValFsMIVlanTunnelProtocolEoam)

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIServiceVlanId[12];
extern UINT4 FsMIServiceProtocolEnum[12];
extern UINT4 FsMIServiceVlanRsvdMacaddress[12];
extern UINT4 FsMIServiceVlanTunnelMacaddress[12];
extern UINT4 FsMIServiceVlanTunnelProtocolStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMIServiceVlanRsvdMacaddress(i4FsMIVlanContextId , i4FsMIServiceVlanId , i4FsMIServiceProtocolEnum ,SetValFsMIServiceVlanRsvdMacaddress) \
        nmhSetCmn(FsMIServiceVlanRsvdMacaddress, 12, FsMIServiceVlanRsvdMacaddressSet, VlanLock, VlanUnLock, 0, 0, 3, "%i %i %i %m", i4FsMIVlanContextId , i4FsMIServiceVlanId , i4FsMIServiceProtocolEnum ,SetValFsMIServiceVlanRsvdMacaddress)
#define nmhSetFsMIServiceVlanTunnelMacaddress(i4FsMIVlanContextId , i4FsMIServiceVlanId , i4FsMIServiceProtocolEnum ,SetValFsMIServiceVlanTunnelMacaddress)     \
        nmhSetCmn(FsMIServiceVlanTunnelMacaddress, 12, FsMIServiceVlanTunnelMacaddressSet, VlanLock, VlanUnLock, 0, 0, 3, "%i %i %i %m", i4FsMIVlanContextId , i4FsMIServiceVlanId , i4FsMIServiceProtocolEnum ,SetValFsMIServiceVlanTunnelMacaddress)
#define nmhSetFsMIServiceVlanTunnelProtocolStatus(i4FsMIVlanContextId , i4FsMIServiceVlanId , i4FsMIServiceProtocolEnum ,i4SetValFsMIServiceVlanTunnelProtocolStatus)   \
        nmhSetCmn(FsMIServiceVlanTunnelProtocolStatus, 12, FsMIServiceVlanTunnelProtocolStatusSet, VlanLock, VlanUnLock, 0, 0, 3, "%i %i %i %i", i4FsMIVlanContextId , i4FsMIServiceVlanId , i4FsMIServiceProtocolEnum ,i4SetValFsMIServiceVlanTunnelProtocolStatus)

#endif
