/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlancli.h,v 1.154 2016/06/30 10:09:34 siva Exp $
 *
 * Description: This file contains VLAN CLI command constants, error
 *              strings and function prototypes.
 *
 *******************************************************************/


#ifndef __VLANCLI_H__
#define __VLANCLI_H__

#include "cli.h"

/* 
 * VLAN CLI command constants. To add a new command ,a new definition needs to
 * be added to the list.
 */


enum {
 CLI_VLAN_ENABLE = 1,
 CLI_VLAN_SHUT ,
 CLI_VLAN_NO_SHUT ,
 CLI_VLAN_CREATE,
 CLI_VLAN_NO_CREATE,
 CLI_VLAN_MAC_GBL,
 CLI_VLAN_SUBNET_GBL,
 CLI_VLAN_NO_MAC_GBL,
 CLI_VLAN_NO_SUBNET_GBL,
 CLI_VLAN_PROTOCOL_VLAN_GBL,
 CLI_VLAN_NO_PROTOCOL_VLAN_GBL,
 CLI_VLAN_PROTO_GROUP,
 CLI_VLAN_NO_PROTO_GROUP,
 CLI_VLAN_TRAFFIC_CLASS,
 CLI_VLAN_PORTS,
 CLI_VLAN_MAC_STATIC_UNICAST,
 CLI_VLAN_MAC_STATIC_MULTICAST,
 CLI_VLAN_DOT1D_MAC_STATIC_UNICAST,
 CLI_VLAN_DOT1D_MAC_STATIC_MULTICAST,
 CLI_VLAN_NO_MAC_STATIC_UNICAST,
 CLI_VLAN_DOT1D_NO_MAC_STATIC_UNICAST,
 CLI_VLAN_NO_MAC_STATIC_MULTICAST,
 CLI_VLAN_DOT1D_NO_MAC_STATIC_MULTICAST,
 CLI_VLAN_SET_MAC_AGING_TIME,
 CLI_VLAN_NO_MAC_AGING_TIME,
 CLI_VLAN_MAC_MAP_ADD,
 CLI_VLAN_SUBNET_MAP_ADD,
 CLI_VLAN_MAC_MAP_DEL,
 CLI_VLAN_SUBNET_MAP_DEL,
 CLI_VLAN_FORWARD_ALL,
 CLI_VLAN_NO_FORWARD_ALL,
 CLI_VLAN_FORWARD_UNREG,
 CLI_VLAN_NO_FORWARD_UNREG,
 CLI_VLAN_DEBUGS,
 CLI_VLAN_NO_DEBUGS,
 CLI_VLAN_PORT_PVID,
 CLI_VLAN_NO_PORT_PVID,
#ifdef PBB_WANTED
 CLI_VLAN_SHOW_DEF_VID,
 CLI_VLAN_ISID_VLAN_MAP,
#endif
 CLI_VLAN_FRAME_TYPE,
 CLI_VLAN_NO_FRAME_TYPE,
 CLI_VLAN_FILTERING,
 CLI_VLAN_NO_FILTERING,
 CLI_VLAN_MAC_PORT,
 CLI_VLAN_SUBNET_PORT,
 CLI_VLAN_NO_MAC_PORT,
 CLI_VLAN_NO_SUBNET_PORT,
 CLI_VLAN_PORT_PROTOCOL,
 CLI_VLAN_NO_PORT_PROTOCOL,
 CLI_VLAN_MAP_PROTOCOL,
 CLI_VLAN_NO_MAP_PROTOCOL,
 CLI_VLAN_PORT_PRIO,
 CLI_VLAN_PORT_MODE,
 CLI_VLAN_NO_PORT_MODE,
 CLI_VLAN_NO_PORT_PRIO,
 CLI_VLAN_MAX_TRCLASS,
 CLI_VLAN_NO_MAX_TRCLASS,
 CLI_VLAN_PRIO_TRCLASS,
 CLI_VLAN_NO_PRIO_TRCLASS,
 CLI_VLAN_SHOW_VLAN_ID,
 CLI_VLAN_SHOW_VLAN_BRIEF,
 CLI_VLAN_SHOW_VLAN_SUMMARY,
 CLI_VLAN_SHOW_VLAN_ALL,
 CLI_VLAN_SHOW_VLAN_ALL_ASCENDING,
 CLI_VLAN_RED_SHOW_VLAN_ALL,
 CLI_VLAN_SHOW_GLOBALS,
 CLI_VLAN_SHOW_FWD_ALL,
 CLI_VLAN_SHOW_FWD_UNREG,
 CLI_VLAN_SHOW_TRCLASS,
 CLI_VLAN_SHOW_GARP_TIMER,
 CLI_VLAN_SHOW_PROTO_GROUP,
 CLI_VLAN_SHOW_PROTOCOLS,
 CLI_VLAN_SHOW_MAC,
 CLI_VLAN_SHOW_SUBNET,
 CLI_VLAN_SHOW_STATS,
 CLI_VLAN_SHOW_STATISTICS,
 CLI_VLAN_SHOW_MAC_TABLE,
 CLI_VLAN_DOT1D_SHOW_MAC_TABLE,
 CLI_VLAN_RED_SHOW_MAC_TABLE,
 CLI_VLAN_SHOW_MAC_COUNT,
 CLI_VLAN_SHOW_MAC_STATIC_UCAST_TABLE,
 CLI_VLAN_SHOW_PORT_SEC_MAC,
 CLI_VLAN_SHOW_PORT_SEC_UCAST_MAC_ADDR,
 CLI_VLAN_SHOW_DOT1D_MAC_STATIC_UCAST_TABLE,
 CLI_VLAN_SHOW_MAC_STATIC_MCAST_TABLE,
 CLI_VLAN_SHOW_DOT1D_MAC_STATIC_MCAST_TABLE,
 CLI_VLAN_SHOW_MAC_DYNAMIC_UCAST_TABLE,
 CLI_VLAN_SHOW_MAC_DYNAMIC_MCAST_TABLE,
 CLI_VLAN_SHOW_MAC_AGING_TIME,
 CLI_VLAN_SHOW_PORT_CONFIG,
 CLI_VLAN_CLR_VLAN_COUNTERS,
 CLI_VLAN_MAC_LEARNING_STATUS,
 CLI_VLAN_MAC_LIMIT,
 CLI_VLAN_NO_MAC_LIMIT,
 CLI_VLAN_SHOW_LEARNING_PARAMS,
 CLI_VLAN_LEARNING_MODE,
 CLI_VLAN_DEFAULT_HYBRIDTYPE,
 CLI_VLAN_MAPFID_VLAN,
 CLI_VLAN_NOMAPFID_VLAN,
 CLI_VLAN_SHOW_VLAN_FID_ID,
 CLI_VLAN_SHOW_VLAN_FID_DETAIL,
 CLI_VLAN_ACTIVE,
 CLI_VLAN_LOOPBACK_STATUS,
 CLI_VLAN_FLT_CRITERIA,
 CLI_VLAN_WILDCARD_ENTRY,
 CLI_VLAN_NO_WILDCARD_ENTRY,
 CLI_VLAN_SHOW_WILDCARD_ENTRY,
 CLI_VLAN_SHOW_WILDCARD_ALL,
 CLI_VLAN_SHOW_CAPABILITIES, 
 CLI_VLAN_GBL_DEBUG,
 CLI_VLAN_NO_GBL_DEBUG,
 CLI_VLAN_MAP_PORT,
 CLI_ADD_UNTAG_PORT,
 CLI_REMOVE_UNTAG_PORT,
 CLI_PORT_PROTECTED,
 CLI_VLAN_NO_PORT_PROTECTED,
 CLI_VLAN_SWITCH_MAC_LIMIT,
 CLI_VLAN_SEC_LEVEL,
 CLI_VLAN_BASE_BRIDGE_MODE,
 CLI_VLAN_NO_SWITCH_MAC_LIMIT,
 CLI_VLAN_INTERFACE_RANGE,
 CLI_GLOBAL_MAC_LEARNING_STATUS,
 CLI_VLAN_CONF_SW_STATS,
    CLI_VLAN_CONF_FILT_CRITERIA,
    CLI_VLAN_PVLAN_TYPE,
    CLI_VLAN_NO_PVLAN_TYPE,
    CLI_VLAN_ADD_PVLAN_ASSOCIATION,
    CLI_VLAN_REM_PVLAN_ASSOCIATION,
    CLI_VLAN_OVERWRITE_PVLAN_ASSOCIATION,
    CLI_VLAN_NO_PVLAN_ASSOCIATION,
    CLI_VLAN_PVLAN_HOST_ASSOCIATION,
    CLI_VLAN_NO_PVLAN_HOST_ASSOCIATION,
    CLI_VLAN_PVLAN_MAPPING,
    CLI_VLAN_NO_PVLAN_MAPPING,
    CLI_VLAN_SHOW_PVLAN_ALL_CTXT,
    CLI_VLAN_SHOW_PVLAN_IN_CTXT,
    CLI_VLAN_PORT_LEARNING_STATUS,
    CLI_VLAN_PORT_SEC_LEVEL,
    CLI_VLAN_PORT_ADD_UNICAST_MAC_ADDR,
    CLI_VLAN_COUNTER_STATUS,
    CLI_VLAN_MODE_OF,
    CLI_VLAN_NO_MODE_OF,
    CLI_VLAN_FLUSH_FDB,
    CLI_VLAN_USER_DEFINED_TPID,
    CLI_VLAN_NO_USER_DEFINED_TPID,
    CLI_VLAN_PORT_ALLOWABLE_TPID,
    CLI_VLAN_SHOW_USER_DEFINED_TPID,
    CLI_VLAN_PORT_ETHERTYPE,
    CLI_VLAN_NO_PORT_ETHERTYPE,
    CLI_VLAN_EGRESS_ETHER_TYPE,
    CLI_VLAN_NO_EGRESS_ETHER_TYPE,
    CLI_VLAN_PORT_EGRESS_TPID_TYPE,
    CLI_VLAN_PORT_NO_EGRESS_TPID_TYPE,
   CLI_VLAN_NAME,
   CLI_PORT_REFLECTION_STATUS
};

enum {
    CLI_VLAN_UNKNOWN_ERR = CLI_ERR_START_ID_VLAN,
    CLI_VLAN_INVALID_VLANID_ERR,
    CLI_VLAN_PVID_ERR,
    CLI_VLAN_L2VPN_VLAN_DEL_ERR,
    CLI_VLAN_MOD_DISABLED_ERR,
    CLI_VLAN_STATIC_ENTRY_NOT_PRESENT,
 CLI_VLAN_INVALID_MULTICAST_MAC,
    CLI_VLAN_ENTRY_NOT_PRESENT,
    CLI_VLAN_MAC_GBL_ERR, 
    CLI_VLAN_PROTOCOL_VLAN_GBL_ERR, 
    CLI_VLAN_MAC_PORT_ERR, 
    CLI_VLAN_SUBNET_PORT_ERR, 
    CLI_VLAN_PROTO_PORT_ERR, 
    CLI_VLAN_MAP_PROTO_GEN_ERR, 
    CLI_VLAN_MAP_PROTO_GRP_ERR, 
    CLI_VLAN_MAP_PROTO_ERR, 
    CLI_VLAN_MAP_PROTO_PORT_GRP_ERR, 
    CLI_VLAN_EMAP_PROTO_PORT_VLAN_RR, 
    CLI_VLAN_GARP_TIMER_ERR, 
    CLI_VLAN_GARP_TIMER_VAL_ERR, 
    CLI_VLAN_GVRP_GBL_ERR, 
    CLI_VLAN_GVRP_PORT_GVRP_ERR, 
    CLI_VLAN_GVRP_PORT_GARP_ERR, 
    CLI_VLAN_GMRP_GBL_ERR, 
    CLI_VLAN_GMRP_PORT_GARP_ERR, 
    CLI_VLAN_GMRP_PORT_GMRP_ERR, 
    CLI_VLAN_LEARNING_TYPE_ERR, 
    CLI_VLAN_PORT_INVALID_EG_ERR, 
    CLI_VLAN_PORT_OVERLAP_ERR, 
    CLI_VLAN_PORT_UNTAG_ERR, 
    CLI_VLAN_MAC_STATIC_VLANID_ERR, 
    CLI_VLAN_MAC_STATIC_OVERFLOW_ERR, 
    CLI_VLAN_MAC_STATIC_ADDR_ERR, 
    CLI_VLAN_INVALID_PORTLIST, 
    CLI_VLAN_FWD_STATIC_PORTS_ERR, 
    CLI_VLAN_FWD_FBIDEN_PORTS_ERR, 
    CLI_VLAN_FWD_PORTS_EXCL_ERR, 
    CLI_VLAN_RESTRICED_GARP_GBL_ERR, 
    CLI_VLAN_RESTRICED_GARP_PORT_ERR, 
    CLI_VLAN_PORTLIST_ERR,
    CLI_VLAN_MAX_PORTLIST_ERR,
    CLI_VLAN_INVALID_PORT_STATUS_ERR,
    CLI_VLAN_MCAST_EGR_PORTLIST_SUBSET_ERR,
    CLI_VLAN_MCAST_FBIDEN_PORTLIST_SUBSET_ERR,
    CLI_VLAN_UNTAG_PORTLIST_SUBSET_ERR,
    CLI_VLAN_IGS_ENABLED_ERR,
    CLI_VLAN_NOT_ACTIVE_ERR,
    CLI_VLAN_PORTCH_ERR,
    CLI_VLAN_CFY_ENABLED_ERR,
    CLI_VLAN_PORT_CFY_ENABLED_ERR,
    CLI_VLAN_TUNNELD_PORT_STATUS_ERR,
    CLI_VLAN_INCONSISTENT_TUNNEL_STATUS_ERR,
    CLI_VLAN_TUNNEL_PORT_MODE_ERR,
    CLI_VLAN_TUNNEL_NETWORK_PORT_ERR,
    CLI_VLAN_TUNNEL_PORT_CFY_ERR,
    CLI_VLAN_INVALID_CONSTRAINT_SET_ERR,
    CLI_VLAN_DEFAULT_CONSTRAINT_SET_MODIFY_ERR,
    CLI_VLAN_INVALID_VLANMODE_ERR,
    CLI_VLAN_CONSTRAINT_ALREADY_EXIST_ERR,
    CLI_VLAN_NO_FREE_CONSTRAINT_ERR,
    CLI_VLAN_CONSTRAINT_INCONSISTENT_ERR,
    CLI_VLAN_GVRP_PORT_MODE_ERR,
    CLI_VLAN_MVRP_PORT_MODE_ERR,
    CLI_VLAN_PORT_ACCEPT_FRAME_TYPE_ERR,
    CLI_VLAN_GMRP_PORT_MODE_ERR,
    CLI_VLAN_STP_PORT_MODE_ERR,
    CLI_VLAN_TAGGED_MEMBER_PORT_ERR,
    CLI_VLAN_UNTAGGED_MEMBER_PORT_ERR,
    CLI_VLAN_ACCESS_TAGGED_PORT_ERR,
    CLI_VLAN_TRUNK_UNTAGGED_PORT_ERR,
    CLI_VLAN_INVALID_FDBID_ERR,
    CLI_VLAN_INCONSISTENT_MSTFID_ERR,
    CLI_VLAN_DEF_VLAN_ERR,
    CLI_VLAN_NOT_PRESENT_ERR,
    CLI_VLAN_INCONSISTENT_RECEIVEPORT_ERR,
    CLI_VLAN_UCAST_MAC_ERR,
    CLI_VLAN_MAX_MCAST_ERR,
    CLI_VLAN_PB_PNP_UNTAG_ERR,   
    CLI_VLAN_PB_PORT_TAG_ERR,  
    CLI_VLAN_PB_FRAME_TYPE_ERR,
    CLI_VLAN_INCONSISTENT_VALUE_ERR,
    CLI_VLAN_PB_ING_FILTER_ERR,
    CLI_VLAN_TUNNEL_INVALID_BRIDGE_ERR,
    CLI_VLAN_PB_SERV_TYPE_ERR,
    CLI_VLAN_INCONSISTENT_INSTMAP_ERR,
    CLI_VLAN_INCONSISTENT_INST_ERR,
    CLI_VLAN_MAC_LIMIT_ERR,
    CLI_VLAN_MAC_STATUS_ERR,
    CLI_VLAN_MAC_LEARNING_MODE_ERR,
    CLI_VLAN_MAC_MAX_PORTS_ERR,
    CLI_VLAN_MAC_LEARNING_DISABLED_ERR,
    CLI_VLAN_GARP_ENABLED,
    CLI_VLAN_SNOOP_ENABLED,
    CLI_VLAN_INVALID_BRIDGE_MODE,
    CLI_VLAN_IVR_CONFLICT_ERR,
    CLI_VLAN_PVRST_HYBRID_CONFIG_ERR,
    CLI_VLAN_PVRST_PVID_HYBRID_ERR,    
    CLI_VLAN_MAC_ERR,
    CLI_VLAN_WRONG_FILTERING_UTILITY_ERR,
    CLI_WILDCARD_ENTRY_NOT_PRESENT_ERR,
    CLI_VLAN_CONFIG_NOT_ALLOWED_1AD,
    CLI_VLAN_SWITCH_MAC_LIMIT_ERR,
    CLI_VLAN_MAC_LIMIT_MORE_ERR,
    CLI_VLAN_MAX_LIMIT_REACHED,
    CLI_VLAN_PBB_PORT_TYPE_ERR,
    CLI_VLAN_PBB_SHUT_ERR,
 CLI_VLAN_PBB_CBP_UNTAG_ERR,
 CLI_VLAN_PBB_VIP_UNTAG_ERR,
 CLI_VLAN_PBB_VIP_TAG_ERR,
 CLI_VLAN_PBB_FRAME_TYPE_ERR,
 CLI_VLAN_CONFIG_NOT_ALLOWED_1AH_CNP_PORT_BASED,
    CLI_VLAN_CONFIG_NOT_ALLOWED_1AH_CBP,
    CLI_VLAN_INVALID_TRAFFIC_CLASS_ERR,
    CLI_VLAN_BASE_BRIDGE_MRP_ENABLED,
    CLI_VLAN_BASE_BRIDGE_GARP_ENABLED,
    CLI_VLAN_BASE_BRIDGE_SNOOP_ENABLED,
    CLI_VLAN_BASE_BRIDGE_PNAC_ENABLED,
    CLI_VLAN_BASE_BRIDGE_MST_ENABLED,
    CLI_VLAN_BASE_BRIDGE_PVRST_ENABLED,
    CLI_VLAN_BASE_BRIDGE_LA_ENABLED,
    CLI_VLAN_BASE_BRIDGE_LLDP_ENABLED,
    CLI_VLAN_BASE_BRIDGE_INTF_PRESENT,
    CLI_VLAN_BASE_BRIDGE_ENABLED,
    CLI_VLAN_BASE_BRIDGE_DISABLED,
    CLI_VLAN_BASE_BRIDGE_MODE_UNKNOWN,
    CLI_VLAN_INVALID_SUBNET_ERR,
    CLI_VLAN_HW_ERR,
    CLI_VLAN_PORT_PROTOCOL_NOT_PRESENT,
    CLI_VLAN_BASE_BRIDGE_MBSM_METRO_ENABLED,
    CLI_VLAN_SISP_LOGICAL_PORT_ERR,
    CLI_VLAN_MSTI_MAP_ERR,
    CLI_VLAN_SW_STATS_CONF_ERR,
    CLI_VLAN_L3_INTERFACE_ERR,
    CLI_VLAN_SAME_PVLAN_IDS_ERR,
    CLI_VLAN_SEC_VLAN_SUBSET_ERR,
    CLI_VLAN_PVLAN_SECONDARY_TYPE_ERR,
    CLI_VLAN_PVLAN_PRIMARY_TYPE_ERR,
    CLI_VLAN_PVLAN_ASSOCIATION_EXISTS,
    CLI_VLAN_PVLAN_ING_FILTER_ERR,
    CLI_VLAN_PVLAN_PORTS_EXIST_ERR,
    CLI_VLAN_PVLAN_SEC_VLAN_ASSOCIATION_EXIST_ERR,
    CLI_VLAN_PORT_IN_PORT_CHANNEL_ERR,
    CLI_VLAN_NO_PROTO_GRP_ERR,
    CLI_VLAN_PROTO_GRP_INFO_DEL_ERR,
    CLI_VLAN_HW_DEL_ERR,
    CLI_VLAN_MAX_ST_UCAST_ERR,
    CLI_VLAN_INVALID_ETHERTYPE_ERR,
    CLI_VLAN_CONFLICT_PRI_ETHERTYPE_ERR,
    CLI_VLAN_INVALID_USERDEFINED_TPID_ERR,
    CLI_VLAN_CONFLICT_TPID_ERR,
    CLI_VLAN_TRUNK_PORT_ACCEPT_FRAME_TYPE_ERR,
    CLI_VLAN_INVALID_PORT_VID_USED_ERR,
#ifdef MPLS_WANTED
    CLI_VLAN_LEARNING_VPWS_ERR,
#endif
    CLI_VLAN_HYBRID_UNTAG_ERR,
    CLI_VLAN_PVRST_HYBRID_PVID_ERR,
    CLI_VLAN_EVC_ASSOCIATION_EXIST_ERR,
    CLI_VLAN_CVLAN_MAPPING_ERR,
    CLI_VLAN_ICCL_TRUNK_PORT_ERR,
    CLI_VLAN_BLOCK_ICCL_VLAN,
    CLI_VLAN_ICCL_MAC_LEARNING_DISABLED_ERR,
    CLI_VLAN_BLOCK_INGRESS_FILTER_ON_MCLAG_IF,
    CLI_VLAN_BLOCK_FRAME_TYPE_ON_ICCL,
    CLI_VLAN_SECONDARY_ERR,
    CLI_VLAN_PVLAN_SUBSET_ERR,
    CLI_INVALID_UNICAST_MAC_SEC_TYPE_ERR,
    CLI_VLAN_PB_NETWORK_PORT_FRAME_TYPE_ERR,
    CLI_VLAN_PVLAN_ERR,
    CLI_VLAN_PVLAN_PORT_ERR,
    CLI_VLAN_SHUT_NOT_SUPPORTED_ERR,
    CLI_VLAN_PVLAN_PRIMARY,
    CLI_VLAN_LLC_HEADER_NOT_SUPPORTED,
    CLI_VLAN_PROTO_ERR,
    CLI_PVRST_MAX_VLAN,
    CLI_VLAN_INVALID_PORT_ERR,
    CLI_VLAN_NOT_SUPPORTED,
    CLI_VLAN_MAX_ERR
};

/* Accessing the ErrString with ErrCode value.
 * Since the ErrCode's starting Id is changed, this forumla is used
 * to access the Err String */
#define CLI_ERR_OFFSET_VLAN(u4ErrCode)   (u4ErrCode - CLI_ERR_START_ID_VLAN + 1)

#ifdef __VLANCLI_C__

CONST CHR1  *VlanCliErrString [] = {
    NULL,
    "Unknown Error\r\n",
    "Invalid Vlan Id\r\n% Vlan ID should be in the range 1 to (Max.Vlans-Max.Router Ports)\r\n",
    "Invalid Port Vlan Id \r\n",
    "Vlan Mapped to L2Vpn Map Entry, So Vlan cannot be deleted \r\n",
    "Vlan Module not enabled\r\n",
 "Static Vlan Entry not present\r\n",
 "Invalid Multicast Mac Address\r\n",
 "Vlan Entry not present\r\n",
    "MAC based VLAN not supported in the device \r\n",
    "Protocol based VLAN not supported in the device \r\n",
    "Global MAC based VLAN status is not enabled \r\n",
    "Global Subnet based VLAN status is not enabled \r\n",
    "Global Protocol based VLAN status is not enabled \r\n",
    "Unable to create protocol group\r\n",
    "Invalid group id \r\n",
    "Invalid protocol \r\n",
    "Invalid group id \r\n",
    "Invalid VLAN ID \r\n",
    "GARP is not enabled \r\n",
    "Leave timer should be greater than 2 times Join timer and Leave-all should be greater than Leave timer and timer value cannot be zero\r\n",
    "GARP is not enabled \r\n",
    "GARP is disabled \r\n",
    "GVRP is disabled \r\n",
    "GARP is not enabled \r\n",
    "GARP is disabled \r\n",
    "GMRP is disabled \r\n",
    "Specified learning type not supported \r\n",
    "Egress port list must not be empty \r\n",
    "Egress port list and Forbidden port list are overlapping \r\n",
    "Untagged port list contains ports not in Egress port list \r\n",
    "Invalid VLAN ID \r\n",
    "Unable to configure static MAC address - database overflow \r\n",
    "Invalid MAC address \r\n",
    "Invalid Port-List \r\n",
    "Static port list is invalid \r\n",
    "Forbidden port list is invalid \r\n",
    "Static and Forbidden ports are overlapping\r\n",
    "GARP not enabled on the device \r\n",
    "GARP not enabled on the port \r\n",
    "Port(s) not created\r\n",
    "Out of range port(s)\r\n",
    "Port not created\r\n",
    "The member ports are not members of the Static Vlan\r\n",
    "Multicast forbidden ports are not members of the Static Vlan\r\n",
    "Untagged ports are not members of the Vlan\r\n",
    "GMRP cannot be enabled when IGMP Snooping is enabled\r\n",
    "Vlan is NOT active\r\n",
    "GARP properties cannot be set on ports that are members of a port-channel\r\n",
    "Global mac and protocol vlan status is enabled.\r\n Cannot set bridge mode provider.\r\n",
    "Port mac and protocol vlan status is enabled.\r\n Cannot set bridge mode provider.\r\n",
    " Port can be set to Accept Only Tagged frames/ Admit all only if tunneling is disabled.\r\n",
    "Tunnelling is disabled. Cannot enable STP Bpdu tunnelling.\r\n",
    "Tunnelling is enabled.  Cannot set port mode to hybrid/trunk.\r\n",
    "Cannot set port mode as access in network ports.\r\n",
    "Bridge Mode is Provider. Cannot enable mac / protocol vlan classification.\r\n",
    "Invalid Constraint Set Value.\r\n",
    "Default Constraint Set Cannot be modified.\r\n",
    "Command Invalid for current Vlan Operational Learning Mode.\r\n",
    "Constraint already exist for Vlan Id and Set.\r\n",
    "Constraint table is full. No Free constraint entry available.\r\n",
    "Constraint inconsistent with previous entries of constraint table.\r\n",
    "Gvrp is enabled on port.Cannot configure as access port.\r\n",
    "Mvrp is enabled on port.Cannot configure as access port.\r\n",
    "On Access port, Only Untagged & Priority Tagged frames are accepted.\r\n Cannot configure as access port.\r\n",
    "Gmrp is enabled on port.Cannot configure as tunnel port.\r\n",
    "Stp is enabled on port.Cannot configure as tunnel port.\r\n",
    "Port is a tagged member of a Vlan. Cannot configure as access port.\r\n",
    "Port is a untagged member of a Vlan. Cannot configure as trunk port.\r\n",
    "Port list has a port configured as access port.\r\n",
    "Port list has a port configured as trunk port.\r\n",
    "Invalid FID.\r\n",
    "Vlan should be mapped to a different MST instance first before mapping to this FID.\r\n",
    "Default Vlan Cannot be Deleted.\r\n",
    "Vlan does not exist.\r\n",
    "Receive Port cannot be a member of Egress Ports.\r\n",
    "Invalid Unicast Mac Address.\r\n",
    "Switch Multicast MAC limit exceeded.\r\n",
    "Provider Network port cannot be set as an untagged port.\r\n",
    "CEP/CNP-PortBased/PCEP cannot be set as a tagged port.\r\n",
    "On PCEP and CNP (Port Based), Only Untagged & Priority Tagged Frames are accepted. \r\n",
    "Invalid input value \r\n",
    "Ingress Filtering cannot be disabled in CNP S-tagged Interface \r\n",
    "Tunneling can be enabled only in Provider Bridge (Q-in-Q) mode \r\n",
    "More than two ports can-not be configured for a VLAN when the service type is E-Line \r\n",
    "Vlan Instance Mapping inconsistent\r\n",
    "Vlans are not mapped to CIST\r\n",
    "Limit being set for vlan exceeds the dynamic unicast limit that can be used by the switch.\r\n",
    "Admin status for MAC Learning set for the VLAN is invalid.\r\n",      
    "Learning mode in the given context is not IVL.\r\n",      
    "The number of member ports in the given VLAN is > 2.\r\n",      
    "Mac learning is DISABLED in atleast one of the active vlans.\r\n",
    "Vlan cannot be disabled since GARP is enabled.\r\n",
    "Vlan cannot be disabled since IGS/MLDS is enabled.\r\n",
    "Bridge-mode is not yet set for this switch.\r\n",
    "Vlan with customer port can not have IVR interface.\r\n",
    "Cannot set Port-Type as Hybrid when PVRST+ is started.\r\n",
    "Cannot set PVID for Hybrid port.\r\n",    
    "Invalid Mac Address.\r\n",
    "Wrong filtering utility criteria.\r\n",
    "WildCard Entry not present.\r\n",
    "This configuration is not allowed for customer ports in 802.1ad Bridges.\r\n",
    "Dynamic unicast mac limit being set for the switch exceeds the device capability.\r\n",
    "Dynamic unicast mac limit set for the switch cannot be less than unicast mac limit of vlan.\r\n",
    "Maximum Vlan Limit Reached.\r\n",
    "PIP can not be set as member port of vlan. \r\n",
    "PBB must be shutdown before VLAN shutdown. \r\n",
    "Customer Backbone port cannot be set as an tagged port. \r\n",
    "VIP can not be set as Untagged Member port if already a Tagged Member port of the given VLAN. \r\n",
    "VIP can not be set as Tagged Member port if already a Untagged Member port of the given VLAN. \r\n",
    "CBP and PIP can accept only Tagged Frames. \r\n",
    "This configuration is not allowed for a CNP port-based on 802.1ah Bridges.\r\n",
    "This configuration is not allowed for a CBP on 802.1ah Bridges.\r\n",
    "Traffic class value must be less than the interface's traffic class value.\r\n",
    "MRP module cannot be operated in Transparent Bridge mode, please shutdown MRP\r\n",
    "GARP module cannot be operated in Transparent Bridge mode, please shutdown GARP\r\n",
    "SNOOP module cannot be operated in Transparent Bridge mode, please shutdown SNOOP\r\n",
    "PNAC module cannot be operated in Transparent Bridge mode, please shutdown PNAC\r\n",
    "MSTP module cannot be operated in Transparent Bridge mode, please shutdown MSTP\r\n",
    "PVRST module cannot be operated in Transparent Bridge mode, please shutdown PVRST\r\n",
    "LA module cannot be operated in Transparent Bridge mode, please shutdown LA\r\n",
    "LLDP module cannot be operated in Transparent Bridge mode, please shutdown LLDP\r\n",
    "Only Physical interfaces should be present in Transparent Mode. Delete other interfaces\r\n",
    "This configuration is not allowed for Transparent Bridges.\r\n",
    "This configuration is not allowed in Vlan-Aware Bridges.\r\n",
    "Base Bridge-Mode Unknown\r\n",
    "Invalid IP Subnet. The subnet is not aligned to the any CLASS of network (A,B,C).\r\n",
    "Hardware Configuration Failed.\r\n",
    "Port Protocol does not exist.\r\n",
    "Transparent Bridge Mode is not supported for MBSM/METRO\r\n",
    "Configuration not allowed in sisp port \r\n",
    "SISP enabled port cannot be member of same instance in different contexts \r\n",
    "since underlying hardware is present, enabling sw stats is not allowed \r\n",
    "Configuring vlan type as primary/secondary for vlans having IVR interface is not allowed\r\n",
    "Primary vlan id and secondary vlan id are same \r\n",
    "Secondary vlans member ports does not form a subset of primary vlans member ports \r\n",
    "Given vlan id is not of a secondary vlan \r\n",
    "Given vlan id is not of a primary vlan \r\n",
    "Given vlan id is already associated with another primary vlan \r\n",
    "Ingress Filtering cannot be disabled on PVLAN ports \r\n",
    "Private Vlan type cannot be changed for vlan with non-zero member ports\r\n",
    "Primary - Secondary Vlan association exists \r\n",
    "Port is already a member of port-channel. Configuration not allowed.\r\n",
    "Protocol group entry does not exist.\r\n",
    "Cannot delete protocol group. Group Id is mapped to port.\r\n",
    "Failed to delete mapped Vlan Id from protocol group.\r\n", 
    "Maximum VLAN Static Unicast Entry Count reached.\r\n",
    "Invalid Ether Type\r\n",
    "TPID conflicts with Primary Ingress EtherType\r\n",
    "Invalid User Defined TPID configured\r\n",
    "Configuring C-Tag/S-Tag/QinQ TPID is prohibited\r\n",
    "On Trunk port, Untagged & Priority Tagged frames are not accepted\r\n",
    "VLAN already in use for Router Port\r\n",
#ifdef MPLS_WANTED
    "Mac-Learning status cannot be changed for this VLAN. It is associated with VPWS Pw.\r\n",
#endif
    "Cannot modify port-list of VLAN 1 when PVRST is started\r\n",
    "Port PVID is other than Default VLAN. Port type cannot be set as Hybrid\r\n",
    "Specified Vlan deletion not allowed. Disassociate EVC from VLAN.\r\n",
    "Existing mapping found for specified Customer VLAN.\r\n",
    "ICCL interface is always trunk port\r\n",
    "Configurations are blocked on ICCL VLAN\r\n",
    "MAC learning cannot be enabled on ICCL interface\r\n",
    "Ingress filter cannot be enabled on MC-LAG interface\r\n",
    "Acceptable frame type cannot be modified on ICCL\r\n",
    "Members  port list  are overlapping  with secondary VLAN of Private VLAN\r\n",
    "Member ports are not subset of primary vlan\r\n",
    "Invalid unicast mac security type\r\n",
    "Network ports cannot accept Untagged frames\r\n",
    "Port channel cannot be a member of Primary/Secondary Vlan\r\n",
    "Port channel cannot be configured as promiscous port or host port\r\n",
    "This configuration is not supported for VLAN\r\n",
    "Member Ports overlap with existing Private vlan\r\n",
    "LLC header frame only supported RAW IPvX protocol Vaule 0xFFFF\r\n",
    "Provider Bridge shall implement Port based Vlan classification and shall not implement Protocol based Vlan classification\r\n",
    "Maximum VLAN limit reached for PVRST mode\r\n"
    "This configuration is not applicable for this interface.\r\n",
    "Feature not supported\r\n",
    "\r\n"
};

#else
extern CONST CHR1  *VlanCliErrString [];
#endif
/* Increased for Attachment Circuit interface */
#define VLAN_MAX_ARGS           36

#define VLAN_NOT_PRESENT                 0xffffffff 
#define VLAN_LIST_LEN                       512

#define VLAN_SET_CMD            1
#define VLAN_NO_CMD             2

#define CLI_VLAN_MODE      "vlan-"

#define VLAN_CLI_VLAN_ID_FDB_ID_PRESENT     0x01
#define VLAN_CLI_MAC_ADDR_PRESENT           0x02
#define VLAN_CLI_PORT_PRESENT               0x04
#define VLAN_CLI_FDB_STATUS_PRESENT         0x08
#define VLAN_CLI_VLAN_ID_PRESENT            0x10
#define VLAN_CLI_CONNECTION_ID_PRESENT      0x12

#define VLAN_CLI_MAX_PROTO_SIZE             16
#define VLAN_CLI_MAX_MAC_STRING_SIZE        21

#define VLAN_CLI_SPACE_ASCII                32
#define VLAN_CLI_MAC_ADDR_LEN               17

#define VLAN_CLI_PROTOCOL_STP      1
#define VLAN_CLI_PROTOCOL_GVRP     2
#define VLAN_CLI_PROTOCOL_IGMP     3

#define VLAN_CLI_UNICAST_MAC_SEC_SAV      1
#define VLAN_CLI_UNICAST_MAC_SEC_SHV      2
#define VLAN_CLI_UNICAST_MAC_SEC_OFF      3

#define VLAN_CLI_UNICAST_MAC_ADDR_TYPE_LOCKED 1
#define VLAN_CLI_UNICAST_MAC_ADDR_TYPE_UNLOCKED 2
#define VLAN_CLI_UNICAST_MAC_ADDR_TYPE_SECUREDLOCKED 3
#define VLAN_CLI_UNICAST_MAC_ADDR_TYPE_SECUREDUNLOCKED 4
#define VLAN_CLI_UNICAST_MAC_ADDR_TYPE_PROVITIONAL 5

#define VLAN_CLI_CONTEXTNAME_LENGTH 32
#define VLAN_DEFAULT_UNICAST_MAC_LIMIT      VLAN_DEF_UNICAST_LEARNING_LIMIT

#define VLAN_CLI_DEFAULT_FILTERING_CRITERIA    1
#define VLAN_CLI_ENHANCED_FILTERING_CRITERIA   2

#define CLI_INT_RANGE "(config-if-range)#"

#define CLI_VLAN_PROTOCOL_ID          VLAN_PROTOCOL_ID
#define CLI_QINQ_PROTOCOL_ID          VLAN_QINQ_PROTOCOL_ID
#define CLI_VLAN_PROVIDER_PROTOCOL_ID VLAN_PROVIDER_PROTOCOL_ID

#define CLI_VLAN_PORT_TPID1 1
#define CLI_VLAN_PORT_TPID2 2
#define CLI_VLAN_PORT_TPID3 3
#define VLAN_CLI_USER_DEFINED_TPID 4

#define CLI_INGRESS 1
#define CLI_EGRESS  2

#define CLI_VLAN_PORT_EGRESS_PORTBASED 1
#define CLI_VLAN_PORT_EGRESS_VLANBASED 2

typedef struct _tVlanCliArgs 
{
    UINT4    u4VlanIdFdbId1;
    UINT4    u4VlanIdFdbId2;
    UINT4    u4VlanId1;
    UINT4    u4VlanId2;
    UINT1   *pu1MacAddr1; 
    UINT1   *pu1MacAddr2; 
    UINT4    u4Status1;
    UINT4    u4Status2;
    UINT4    u4Port1;
    UINT4    u4Port2;
    UINT1   *pu1Ports;      /* 
                           * If Portlist is provided, then u4Port1
                           * will checked if it is a member of pu1Ports.
                           * u4Port2 will be ignored.
                           */
} tVlanCliArgs;


typedef struct _tFidList
{
  tTMO_SLL_NODE  NextFidEntry;
  UINT4 u4Fid;
} tFidListEntry;

typedef INT4 (* tVlanMacCmpFn) (UINT4 u4Mask, tVlanCliArgs *pArgs);

INT4
cli_process_vlan_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);

INT4
cli_process_vlan_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);

INT4
cli_process_pvlan_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);

INT4
cli_process_pvlan_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);

INT4
VlanEnable(tCliHandle CliHandle, INT4 i4Status);

INT4
VlanConfSwStatus (tCliHandle CliHandle, INT4 i4Status);

INT4
VlanConfApplyFilteringCriteria (tCliHandle CliHandle, INT4 i4Status);

INT4
VlanCreate (tCliHandle CliHandle, UINT4 u4VlanId);

INT4
VlanActive (tCliHandle CliHandle);

INT4
VlanDelete (tCliHandle CliHandle, UINT4 u4VlanId);

INT4
VlanMacbasedOnAllPorts (tCliHandle CliHandle, UINT4 u4Action);

INT4
VlanSubnetbasedOnAllPorts (tCliHandle CliHandle, UINT4 u4Action);

INT4
VlanPortProtocolBasedOnAllPorts (tCliHandle CliHandle, UINT4 u4Action);
INT4
VlanUpdateProtocolGroup (tCliHandle CliHandle, UINT1 u1FrameType,
                         UINT4 u4GroupId, UINT1 *pu1ProtocolValue,
                         UINT1 u1Length, UINT1 u1Action);

INT4
VlanSetLearningType (tCliHandle CliHandle, UINT4 u4LearningType);

INT4
VlanSetTrafficClasses (tCliHandle CliHandle, UINT4 u4Action);

INT4
VlanSetPorts (tCliHandle CliHandle, UINT1 *pu1MemberPorts,
              UINT1 *pu1UntaggedPorts, UINT1 *pu1ForbiddenPorts,
              UINT4 u4Flag);
INT4
VlanSetName (tCliHandle CliHandle, UINT1 *pau1Name);

INT4
VlanSetDot1dUnicastAdd (tCliHandle CliHandle, UINT1 *pu1MacAddress,
                        UINT4 u4ReceivePort, UINT1 *pu1Ports, UINT4 u4Status);
INT4
VlanFormPortList (tCliHandle CliHandle, UINT1 *pu1MemberPorts,
                  UINT1 *pu1UntaggedPorts, UINT1 *pu1ForbiddenPorts);

INT4
VlanSetUnicastAdd (tCliHandle CliHandle, UINT1 *pu1MacAddress,
                   UINT4 u4ReceivePort, UINT1 *pu1Ports, UINT4 u4Status, 
                   UINT4 u4VlanId, UINT1 *pu1ConnectionId);

INT4
VlanSetUnicastDel (tCliHandle CliHandle, UINT1 *pu1MacAddr, UINT4 u4RcvPort, UINT4 u4VlanId);

INT4
VlanDot1dSetUnicastDel (tCliHandle CliHandle, UINT1 *pu1MacAddr,
                        UINT4 u4RcvPort, UINT4 u4VlanId);

INT4
VlanMulticastEntryAdd (tCliHandle CliHandle, UINT1 *pu1MacAddr, 
                       UINT4 u4ReceivePort, UINT1 *pu1EgressPorts,
                       UINT1 *pu1ForbiddenPorts, UINT4 u4Status,
                       UINT4 u4EgressFlag, UINT4 u4ForbiddenFlag, UINT4 u4VlanId);

INT4
VlanDot1dMulticastEntryAdd (tCliHandle CliHandle, UINT1 *pu1MacAddr,
                            UINT4 u4ReceivePort, UINT1 *pu1EgressPorts,
                            UINT4 u4Status,
                            UINT4 u4EgressFlag);

INT4
VlanMulticastEntryDel (tCliHandle  CliHandle, UINT1 *pu1MacAddr, 
                       UINT4 u4ReceivePort, UINT4 u4VlanId);

INT4
VlanDot1dMulticastEntryDel (tCliHandle CliHandle, UINT1 *pu1MacAddr,
                            UINT4 u4ReceivePort, UINT4 u4VlanId);

#ifdef VLAN_EXTENDED_FILTER
INT4
VlanSetFwdEntry (tCliHandle CliHandle, UINT1 *pu1StaticPorts,
                 UINT1 *pu1ForbiddenPorts,
                 UINT4 u4EgressFlag, UINT4 u4ForbiddenFlag, 
                 UINT4 u4Type);

INT4
VlanReSetFwdEntry (tCliHandle CliHandle, UINT4 u4Type);
#endif 

#ifdef TRACE_WANTED
INT4
VlanSetDebugs (tCliHandle CliHandle, UINT4 u4DbgMod, UINT4 u4DbgValue,
               UINT1 u1Action);
INT4
VlanSetGlobalDebug (tCliHandle CliHandle, UINT1 u1Action);
#endif 

INT4
VlanSetPortPvid (tCliHandle CliHandle, UINT4 u4VlanId, UINT4 u4PortId);

INT4
VlanSetPortMode (tCliHandle CliHandle, UINT4 u4PortId, UINT4 u4PortMode);

INT4
VlanSetPortFrameType (tCliHandle CliHandle, INT4 i4FrameType, UINT4 u4PortId);

INT4
VlanSetPortFiltering (tCliHandle CliHandle, UINT4 u4Action, UINT4 u4PortId);

INT4
VlanSetPortMacbased (tCliHandle CliHandle, UINT4 u4Action, UINT4 u4PortId);

INT4
VlanSetPortSubnetbased (tCliHandle CliHandle, UINT4 u4Action, UINT4 u4PortId);

INT4
VlanSetPortProtocolbased (tCliHandle CliHandle, UINT4 u4Action, UINT4 u4PortId);

INT4
VlanSetPortProtoGroup (tCliHandle CliHandle, UINT4 u4GroupId, UINT4 u4VlanId,
                       UINT4 u4PortId, UINT1 u1Action);

INT4
VlanSetPortDefaultPriority (tCliHandle CliHandle, INT4 i4DefPriority,
                            UINT4 u4PortId);

INT4
VlanSetMaxTrafficClass (tCliHandle CliHandle, UINT4 u4MaxTrafficClass,
                        UINT4 u4PortId);

INT4
VlanSetPortPrioAndTrafficClass (tCliHandle CliHandle, UINT4 u4Priority,
                                UINT4 u4TrafficClass, UINT4 u4PortId,
                                UINT1 u1Action);

INT4
VlanSetHybridTypeDefault (tCliHandle CliHandle, UINT4 u4ConstraintType);

INT4
VlanCliFidVlanMapping (tCliHandle CliHandle, UINT4 u4Fid, UINT1 *pau1VlanList,
                       UINT1 u1Action);
INT4 VlanResetVlanStats (tCliHandle CliHandle, tVlanId VlanId);
INT4
VlanMacLearningStatus (tCliHandle CliHandle, tVlanId VlanId,
                       UINT4 u4Status);
INT4
VlanUnicastMacLimit (tCliHandle CliHandle, tVlanId VlanId,
                     UINT4 u4VlanMacLimit);


INT4 
VlanCliCompareArgs (UINT4 u4Mask, tVlanCliArgs *pArgs);

INT4
VlanUtilGetProtocol (UINT1 *pu1Proto, UINT1 *pu1ProtoVal, UINT1 u1Len);

INT4
VlanCheckMstpInconsistency (UINT4 u4Fid, tSNMP_OCTET_STRING_TYPE *pVlanList);

INT1
VlanGetVlanCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

INT1
VlanGetVcmVlanCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

INT4
VlanTestModificationInfo (tCliHandle               CliHandle,
                          UINT4                    u4VlanId,
                          tSNMP_OCTET_STRING_TYPE *pEgressPorts,
                          tSNMP_OCTET_STRING_TYPE *pUntaggedPorts,
                          tSNMP_OCTET_STRING_TYPE *pForbiddenPorts);

INT4
VlanTestStaticMcastModificationInfo (tCliHandle CliHandle, UINT4 u4VlanIndex,
                                     UINT1 *pu1MacAddr,
                                     INT4 i4Dot1qStaticMulticastReceivePort,
                                     tSNMP_OCTET_STRING_TYPE *pStaticPorts,
                                     tSNMP_OCTET_STRING_TYPE *pForbiddenPorts);

#ifdef VLAN_EXTENDED_FILTER
INT4
VlanTestStaticDefGroupModificationInfo (tCliHandle               CliHandle, 
                                        UINT4                    u4VlanId,
                                        tSNMP_OCTET_STRING_TYPE *pStaticPorts,
                                        tSNMP_OCTET_STRING_TYPE *pFbiddenPorts,
                                        UINT4                    u4Type);
#endif 

INT4
VlanOctetToIfName (tCliHandle CliHandle, UINT1  *pu1FirstLine,
                   UINT1 *pOctet, UINT4 u4OctetLen,
                   UINT4 u4MaxPorts, 
                   UINT1 u1Flag, UINT1 u1Column,
                   UINT4 *pu4PagingStatus);

INT4
VlanGetMulticastEntryCount (UINT4 *pu4McastCount);

INT4
VlanUpdateMacMapEntry (tCliHandle CliHandle, UINT1 *pu1MacAddr, UINT4 u4Port,
                       UINT4 u4VlanId, INT4 i4McastOption, UINT1 u1Flag);

INT4
VlanUpdateSubnetMapEntry (tCliHandle CliHandle, UINT4 u4SubnetAddr,
                          UINT4 u4SubnetMask, UINT4 u4Port,
                          UINT4 u4VlanId, INT4 i4ArpOption, UINT1 u1Flag);

INT4
VlanSetMcastBcastTrafficStatus (UINT4 u4IfIndex, UINT1 *pu1MacAddr,
                                 INT4 i4DiscardOption);

INT4
VlanSetARPOption (UINT4 u4Port, UINT4 u4SubnetAddr, UINT4 u4SubnetMask,
                  INT4 i4Option);
 
INT4
VlanShutdown (tCliHandle CliHandle, INT4 i4Status);

INT4 VlanCliSetDebugLevel (tCliHandle CliHandle, INT4 i4CliDebugLevel); 

INT4 VlanSetLearningMode (tCliHandle CliHandle, UINT4 u4VlanLearningMode);

VOID IssVlanShowDebugging (tCliHandle);

#ifdef L2RED_WANTED /* L2RED_ISS_MERGE */
VOID ShowVlanGvrpLrntPortsDb (tCliHandle CliHandle);
VOID ShowVlanGmrpLrntPortsDb (tCliHandle CliHandle);
INT4 ShowVlanGmrpLrntEntry (tRBElem *pRBElem, eRBVisit visit, 
                            UINT4 u4Level, VOID *pArg, VOID *pOut);
#endif /* L2RED_WANTED */


INT4
VlanCliSelectContextOnMode (tCliHandle CliHandle, UINT4 u4Cmd, 
                            UINT4 *pu4Context, UINT2 *pu2LocalPort); 

INT4
VlanCliGetContextForShowCmd (tCliHandle CliHandle, UINT1 *pu1Name, 
                             UINT4 u4IfIndex, UINT4 u4CurrContext, 
                             UINT4 *pu4NextContext, UINT2 *pu2LocalPort);


VOID VlanCliDelFidList(tTMO_SLL *pCliFidList);

INT4
ShowVlanDatabase (tCliHandle CliHandle, UINT4 u4ContextId, 
                    UINT4 u4DisplayType, UINT4 u4StartVlanId, UINT4 u4LastVlanId);

INT4
ShowVlanGlobalInfo (tCliHandle CliHandle, UINT4 u4ContextId);

INT4
ShowVlanForwardAll (tCliHandle CliHandle, UINT4 u4ContextId);

INT4
ShowVlanForwardUnreg (tCliHandle CliHandle, UINT4 u4ContextId);

INT4
ShowVlanTrafficClasses (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4PortId);

INT4
ShowVlanMaxTrafficClasses (tCliHandle CliHandle, UINT4 u4ContextId, 
                                            UINT4 u4PortId);

INT4
ShowVlanPortConfig (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4PortId);

INT4
ShowVlanProtocolGroup (tCliHandle CliHandle, UINT4 u4ContextId);

INT4
ShowVlanProtocolDatabase (tCliHandle CliHandle, UINT4 u4ContextId);

INT4
ShowVlanMacDatabaseOnPort (tCliHandle CliHandle, UINT4 i4Port);

    
INT4
ShowVlanMacDatabase (tCliHandle CliHandle, UINT4 u4ContextId);

INT4
ShowVlanSubnetDatabaseOnPort (tCliHandle CliHandle, UINT4 u4IfIndex);

INT4
ShowVlanSubnetDatabase (tCliHandle CliHandle, UINT4 u4ContextId);

INT4
ShowAllVlanSubnetDatabase (tCliHandle CliHandle);
   
INT4
ShowVlanGlobalStats (tCliHandle CliHandle, UINT4 u4ContextId,
UINT4 u4StartVlanId, UINT4 u4LastVlanId);

INT4
ShowVlanStatistics (tCliHandle CliHandle, UINT4 u4ContextId, 
                    tVlanId StartVlanId, tVlanId LastVlanId);

INT4 
ShowVlanMacLearningParams (tCliHandle CliHandle, UINT4 u4ContextId, 
                           tVlanId StartVlanId, tVlanId LastVlanId);

INT4 
ShowVlanMacAddrTable (tCliHandle CliHandle, UINT4 u4ContextId, 
                        UINT1 *pu1MacAddr, tVlanId StartVlanId,
                        tVlanId LastVlanId, UINT4 u4IfIndex);
INT4
ShowVlanDot1dMacAddrTable (tCliHandle CliHandle, UINT4 u4ContextId,
                           UINT1 *pu1MacAddr, UINT4 u4IfIndex);

INT4 
ShowVlanMacAddrCount (tCliHandle CliHandle, UINT4 u4ContextId); 

INT4 
ShowVlanMacAddrCountVlan (tCliHandle CliHandle, UINT4 u4ContextId, 
                            tVlanId VlanId, UINT4 * pu4PagingStatus);

INT4 
ShowVlanStUcastMacAddrTable (tCliHandle CliHandle, UINT4 u4ContextId, 
                               UINT1 *pu1MacAddr, tVlanId StartVlanId,
                               tVlanId LastVlanId, UINT4 u4IfIndex);
INT4 
ShowVlanDot1dStUcastMacAddrTable (tCliHandle CliHandle, UINT4 u4ContextId, 
                                  UINT1 *pu1MacAddr, tVlanId StartVlanId,
                                  tVlanId LastVlanId, UINT4 u4IfIndex);

INT4
ShowVlanUcastMacPortSecurity (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4IfIndex);

INT4 
ShowVlanDynamicUcastMacAddrTable (tCliHandle CliHandle, UINT4 u4ContextId, 
                                    UINT1 *pu1MacAddr, tVlanId StartVlanId,
                                    tVlanId LastVlanId, UINT4 u4IfIndex);

INT4 
ShowVlanStMcastMacAddrTable (tCliHandle CliHandle, UINT4 u4ContextId, 
                               UINT1 *pu1MacAddr, tVlanId StartVlanId,
                               tVlanId LastVlanId, UINT4 u4IfIndex);
INT4 
ShowVlanDot1dStMcastMacAddrTable (tCliHandle CliHandle, UINT4 u4ContextId, 
                                  UINT1 *pu1MacAddr, tVlanId StartVlanId,
                                  tVlanId LastVlanId, UINT4 u4IfIndex);

INT4 
ShowVlanDynamicMcastMacAddrTable (tCliHandle CliHandle, UINT4 u4ContextId, 
                                    UINT1 *pu1MacAddr, tVlanId StartVlanId,
                                    tVlanId LastVlanId, UINT4 u4IfIndex);

INT4
ShowVlanFdbEntryVlanMacAddr (tCliHandle CliHandle, UINT4 u4ContextId, 
                               UINT4 u4FdbId, tVlanId VlanId, tMacAddr MacAddr, 
                               UINT4 u4Port, UINT4 u4DispDynamicOnly, 
                               INT4 *pi4Count);

INT4 
ShowVlanDot1dFdbEntryVlanMacAddr (tCliHandle CliHandle, UINT4 u4ContextId,
                                  UINT4 u4FdbId, tVlanId VlanId,
                                  tMacAddr MacAddr,
                                  UINT4 u4Port, UINT4 u4DispDynamicOnly,
                                  INT4 *pi4Count);

INT4 
ShowVlanMcastEntryVlanMacAddr (tCliHandle CliHandle, UINT4 u4ContextId, 
                                 UINT4 u4VlanId, UINT1 *pu1MacAddr, 
                                 UINT4 u4Port, UINT4 u4DispDynamicOnly, 
                                 INT4 *pi4Count);
INT4
ShowVlanDot1dMcastEntryVlanMacAddr (tCliHandle CliHandle, UINT4 u4ContextId,
                                    UINT4 u4VlanId, UINT1 *pu1MacAddr,
                                    UINT4 u4Port, UINT4 u4DispDynamicOnly,
                                    INT4 *pi4Count);

INT4
VlanCliScanAndPrintFdbTable (tCliHandle CliHandle, UINT4 u4ContextId, 
                               tVlanMacCmpFn pMacCmpFn, UINT4 u4ArgsMask, 
                               tVlanCliArgs *pCliArgs,
                               UINT4 u4DisplayFlag, INT4 *pi4Count);


INT4
VlanCliAddFidListScanAndPrint (tCliHandle CliHandle, UINT4 u4ContextId,
                               tVlanId StartVlanId,tVlanId LastVlanId,
                               UINT4 u4ArgsMask, tVlanCliArgs *pCliArgs,
                               INT4 *pi4Count,INT4 i4DispFlag);

VOID
VlanCliUnicastMacSecScanAndPrint (tCliHandle CliHandle, UINT4 u4ContextId,
                               UINT4 u4IfIndex);

VOID
VlanCliUnicastMacUcastMacAddrScanAndPrint (tCliHandle CliHandle, UINT4 u4ContextId,
                               UINT4 u4IfIndex);

INT4
VlanCliScanAndPrintDot1dFdbTable (tCliHandle CliHandle, UINT4 u4ContextId,
                                  tVlanMacCmpFn pMacCmpFn, UINT4 u4ArgsMask,
                                  tVlanCliArgs * pCliArgs,
                                  UINT4 u4DisplayFlag, INT4 *pi4Count);

INT4
VlanCliScanAndPrintStUcastTable (tCliHandle CliHandle, UINT4 u4ContextId, 
                                   tVlanMacCmpFn pMacCmpFn, UINT4 u4ArgsMask, 
                                   tVlanCliArgs *pCliArgs,
                                   UINT4 u4DisplayFlag, INT4 *pi4Count);

INT4
VlanCliDot1dScanAndPrintStUcastTable (tCliHandle CliHandle, UINT4 u4ContextId, 
                                      tVlanMacCmpFn pMacCmpFn, UINT4 u4ArgsMask, 
                                      tVlanCliArgs *pCliArgs,
                                      UINT4 u4DisplayFlag, INT4 *pi4Count);


INT4
VlanCliScanAndPrintGroupTable (tCliHandle CliHandle, UINT4 u4ContextId, 
                                 tVlanMacCmpFn pMacCmpFn, UINT4 u4ArgsMask, 
                                 tVlanCliArgs *pCliArgs, UINT4 u4DynamicOnly, 
                                 UINT4 u4DisplayFlag, INT4 *pi4Count);

INT4
VlanCliScanAndPrintStMcastTable (tCliHandle CliHandle, UINT4 u4ContextId, 
                                   tVlanMacCmpFn pMacCmpFn, UINT4 u4ArgsMask, 
                                   tVlanCliArgs *pCliArgs, UINT4 u4DisplayFlag, 
                                   INT4 *pi4Count);
INT4
VlanCliDot1dScanAndPrintStMcastTable (tCliHandle CliHandle, UINT4 u4ContextId, 
                                      tVlanMacCmpFn pMacCmpFn, UINT4 u4ArgsMask, 
                                      tVlanCliArgs *pCliArgs,
                                      UINT4 u4DisplayFlag, INT4 *pi4Count);

INT4
VlanCliShowFidVlanMapping (tCliHandle CliHandle, UINT4 u4ContextId, 
                             UINT4 u4Command, UINT4 u4Fid);

INT4
VlanGlobalShowRunningConfig (tCliHandle CliHandle);
INT4 VlanShowRunningConfigScalars (tCliHandle CliHandle, UINT4 u4ContextId, UINT1 *pu1HeadFlag);
INT4 VlanShowRunningConfigInterface (tCliHandle CliHandle, UINT4 u4ContextId);
INT4
VlanShowRunningConfigInterfaceDetails (tCliHandle CliHandle, UINT4 u4ContextId,
                                       UINT4 u4IfIndex, INT4 i4LocalPort,
                                       UINT1 u1PbPortType, UINT1 *pu1HeadFlag);

INT4
VlanGarpShowRunningConfigInterface (tCliHandle CliHandle, 
                                    UINT4 u4CurrentIfIndex, UINT1 *pu1HeadFlag);
INT4 VlanShowRunningConfig (tCliHandle CliHandle, UINT4 u4ContextId, 
                            UINT4 u4VlanId, UINT4 u4Module);
INT4 VlanMplsShowRunningConfig (tCliHandle CliHandle, UINT4 u4ContextId,
                            UINT4 u4VlanId, UINT4 u4Module);

INT4 DisplayDot1vProtocolGroupInfo(tCliHandle CliHandle, UINT4 u4ContextId, 
                                   UINT4 u4FrameType, 
                               tSNMP_OCTET_STRING_TYPE NextSnmpOctetStr,
                               UINT4 *pu4PagingStatus);
INT4 DisplayDot1qFutureVlanMacMapTableInfo(tCliHandle CliHandle, 
                                           UINT4 u4ContextId, 
                                           tMacAddr NextMacAddr,
                                       UINT4 *pu4PagingStatus);
INT4 DisplayDot1qStaticUnicastTableInfo(tCliHandle CliHandle, UINT4 u4ContextId,
                                        UINT4 u4Fid, tMacAddr NextMacAddr, 
                                        UINT4 i4RcvPort, 
                                              UINT4 *pu4PagingStatus);
INT4 DisplayDot1qStaticMulticastTableInfo(tCliHandle CliHandle, 
                                          UINT4 u4ContextId, UINT4 u4VlanIndex,
                                                tMacAddr NextMacAddr, UINT4 i4RcvPort,
                                                UINT4 *pu4PagingStatus);
INT4 DisplayDot1dStaticTableInfo (tCliHandle CliHandle, tMacAddr NextMacAddr, 
                                  UINT4 i4RcvPort, UINT4 *pu4PagingStatus);
INT4 DisplayDot1qVlanStaticTableInfo(tCliHandle CliHandle, UINT4 u4ContextId, 
                                     UINT4 u4VlanIndex, UINT4 *pu4PagingStatus, 
                                     UINT1 *pu1isActiveVlan, UINT4 *pu4Module, UINT1 *pu1HeadFlag);

INT4 VlanShowRunningConfigTables (tCliHandle CliHandle, UINT4 u4ContextId, UINT1 *pu1HeadFlag);
INT4 VlanShowRunningConfigVlanInfo (tCliHandle CliHandle, UINT4 u4ContextId, 
                                    UINT4 u4VlanId, UINT4 u4Module, UINT1 *pu1HeadFlag);
INT4 VlanDisplayExtFilteringInfo(tCliHandle CliHandle, UINT4 u4ContextId, 
                                      UINT4 u4VlanIndex, UINT4 *pu4PagingStatus, 
                                      UINT1 *pu1isActiveVlan);
INT4
DisplayDot1qMacLearningParams (tCliHandle CliHandle, UINT4 u4ContextId, 
                               tVlanId VlanId, UINT4 *pu4PagingStatus);
INT4
VlanCliShowDeviceCapabilities (tCliHandle CliHandle, UINT4 u4ContextId);

VOID VlanPbCliVlanDisplay (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4VlanId);


INT4 VlanSetPortFilteringCriteria (tCliHandle CliHandle, UINT4 u4IfIndex, 
                                   UINT2 u2PortFltCriteria);
INT4
VlanAddWildCardEntry (tCliHandle CliHandle, UINT1 *pu1MacAddress,
                      UINT1 *pu1Ports);
INT4
VlanDelWildCardEntry (tCliHandle CliHandle, UINT1 *pu1MacAddress);
INT4
VlanMICliShowWildCardTable (tCliHandle CliHandle, UINT4 u4ContextId,
                            UINT1 *pu1MacAddr, UINT4 u4showwildcardentry);
INT4
DisplayWildCardTableInfo (tCliHandle CliHandle, UINT4 u4ContextId,
                          tMacAddr NextMacAddr, UINT4 *pu4PagingStatus);
INT4
DisplayVlanCounterStatus (tCliHandle CliHandle, UINT4 u4ContextId, 
     UINT4 u4VlanId, UINT1 *pu1isActiveVlan);
INT4
VlanSetPortProtected (tCliHandle CliHandle, UINT4 u4PortId, INT4 i4Status);

INT4
VlanSetPortUnicastMacLearningStatus (tCliHandle CliHandle, INT4 i4PortId,UINT4 u4Status);
INT4
VlanSetSwUnicastMacLimit (tCliHandle CliHandle, UINT4 u4UnicastMacLimit);

INT4
VlanSetSwitchUnicastMacLearningStatus (tCliHandle CliHandle, UINT4 u4UnicastMacStatus);

INT4
VlanSetPortUnicastSecType (tCliHandle CliHandle, INT4 i4PortId, INT4 i4PortMacSecType);

INT4
VlanSetPortUnicastMacAddr (tCliHandle CliHandle, INT4 i4PortId, UINT1 *pu1MacAddr, UINT4 u4MacAddrType,
                          UINT4 u4VlanId);

INT4
VlanSetBaseBridgeMode (tCliHandle CliHandle, INT4 i4BridgeMode);

INT4
VlanSetUnicastSecType (tCliHandle CliHandle, INT4 i4PortMacSecType);

INT4
VlanSetDefVid (tCliHandle CliHandle,  UINT4 u4VlanId, UINT4 i4PortId);

INT4 VlanSetUserDefinedTPID PROTO ((tCliHandle, UINT4));
INT4 VlanSetPortTPID PROTO ((tCliHandle, INT4, UINT4, UINT4));
INT4 VlanShowUserDefinedTPID PROTO ((tCliHandle, UINT4));
INT4 VlanSetPortEtherType PROTO ((tCliHandle, INT4, UINT4, UINT4));
INT4 VlanSetVlanEgressEthertype PROTO ((tCliHandle , tVlanId,UINT4 ));
INT4 VlanSetPortEgressTPIDType PROTO((tCliHandle,INT4,UINT4) );
INT4 VlanGetDefaultPortEtherType PROTO ((UINT2, UINT4, UINT4 *));
INT4 VlanPbbGetIsidOfVip(UINT4 *,UINT4,UINT4);

#ifdef PBB_WANTED
INT4 VlanSetVlanMapForIsid (
        tCliHandle CliHandle,
        UINT1 *pau1VlanList ,
        INT4 i4UntaggedPip, UINT2 u2VipPort);

INT4
VlanMICliShowDefaultVid (tCliHandle CliHandle, UINT4 u4ContextId,
                            UINT4 u4Isid);

INT4
VlanPbCliShowPBBIsid(tCliHandle CliHandle, UINT4 u4ContextId,
                            UINT4 u4Isid);
 INT4
VlanSetComponentType (tCliHandle CliHandle, UINT4 u4ComponentType, UINT4 u4ContextId);
#endif
INT4
VlanCliSetPvlanType (tCliHandle CliHandle, UINT4 u4ContextId, tVlanId VlanId,
                     UINT1 u1Type);

INT4
VlanCliSetPvlanAssociation (tCliHandle CliHandle, UINT4 u4ContextId, 
                            tVlanId VlanId, UINT1 u1Flag,
                            UINT1 *pu1SecVlanList);
INT4
VlanCliShowPrivateVlan (tCliHandle CliHandle, INT4 i4ContextId, UINT1 u1Type);
VOID
VlanCliShowAllPrivateVlan (tCliHandle CliHandle, INT4 i4ContextId,
                           UINT1 u1ActionFlag);
VOID
VlanCliShowPvlansOnType (tCliHandle CliHandle, INT4 i4ContextId,
                         UINT1 u1PvlanType, UINT1 u1ActionFlag);
VOID
VlanCliShowAllPvlansInCtxt (tCliHandle CliHandle, UINT4 u4CurrContextId);
VOID
VlanCliShowPvlansOnTypeInCtxt (tCliHandle CliHandle, UINT4 u4CurrContextId,
                               UINT1 u1Type);
INT4 
VlanCliDeleteAllAssocSecVlans (tCliHandle CliHandle, UINT4 u4ContextId,
                               tVlanId PrimaryVlanId);
INT4
VlanCliAssociateHostPort (tCliHandle CliHandle, UINT2 u2LocalPortId, 
                          tVlanId PrimaryVlanId, tVlanId SecondaryVlanId);
INT4
VlanCliDisAssociateHostPort (tCliHandle CliHandle, UINT2 u2LocalPortId);
INT4
VlanCliAssocPromiscuousPort (tCliHandle CliHandle, UINT2 u2LocalPortId,
                             tVlanId PrimaryVlanId, UINT1 u1ActionFlag, 
                             UINT1 *pu1VlanList);
INT4
VlanCliDisAssocPromicuousPort (tCliHandle CliHandle, UINT2 u2LocalPortId);
INT4
VlanMapToPort PROTO ((tCliHandle CliHandle, UINT4 u4VlanId, UINT2 u2LocalPortId));



VOID
VlanAddPortToDefaultVlan ( tCliHandle CliHandle, UINT2 u2LocalPortId);

UINT4
VlanRemoveUntaggedPortFromVlans (tCliHandle CliHandle, UINT4 u4VlanId,
                                 UINT2 u2LocalPortId, UINT1 u1SkipVlan);

INT4
VlanSetCounterStatus (tCliHandle CliHandle, UINT4 u4VlanId, INT4 i4CounterStatus);

INT4
VlanCliFlushFdb (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4VlanId);

INT4
VlanCreateOpenflowVlan (tCliHandle CliHandle, UINT4 u4VlanId);

INT4
VlanDeleteOpenflowVlan (tCliHandle CliHandle, UINT4 u4VlanId);

INT4
VlanSetLoopbackStatus (tCliHandle CliHandle, UINT4 u4VlanId, INT4 i4CounterStatus);

VOID
DisplayVlanLoopbackStatus (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4VlanId);

INT4
VlanCliPrintRemoteFdbTable (tCliHandle CliHandle, UINT4 u4ContextId,
                              INT4 *pi4Count);
VOID VlanCliPrintIntf (tCliHandle CliHandle , UINT4 i4LocalPort, UINT1 *pu1HeadFlag);

VOID VlanCliPrintCtxt (tCliHandle CliHandle, UINT4 u4ContextId, UINT1 *pu1HeadFlag);
VOID VlanCliPrintDefVlan (tCliHandle CliHandle, UINT4 u4ContextId,
                           UINT4 u4VlanIndex, UINT1 *pu1HeadFlag, UINT1 *pu1ActiveVlan);
#endif /* __VLANCLI_H__ */
