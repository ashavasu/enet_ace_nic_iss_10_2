#ifndef __EVCPROGLOBCLI_H__
#define __EVCPROGLOBCLI_H__

/* STP CLI Command constants */

#include "cli.h"

/*COMMAND IDENTIFIRS*/

enum
{
CLI_EVCPRO_ETH_EVC,
CLI_EVCPRO_NO_ETH_EVC,
CLI_MAP_CEVLAN,
CLI_NO_MAP_CEVLAN,
CLI_EVCPRO_SET_UNI_COUNT,
CLI_EVCPRO_SET_NO_UNI_COUNT,
CLI_EVCPRO_EVC_BW_PROFILE,
CLI_EVCPRO_SET_EVC_STATUS
};
	

/* Constants defined for EVCPROS CLI. These values are passed as arguments 
from
* the def file
*/
#define EVCPRO_CLI_MAX_ARGS      13
#define POINT_TO_POINT_EVC	 10
#define POINT_TO_MULTI_POINT_EVC 4
#define CE_VLAN_ID               5
#define DEFAULT_CE_VLAN_ID       6
#define ANY_CE_VLAN_ID           8
#define EVCPRO_NOT_ACTIVE          0
#define EVCPRO_NEW_NOT_ACTIVE      1
#define EVCPRO_ACTIVE              2
#define EVCPRO_NEW_ACTIVE          3
#define EVCPRO_PARTIALLY_ACTIVE    4
#define EVCPRO_NEW_PARTIALLY_ACTIVE 5

INT4  cli_process_evcpro_cmd  PROTO((tCliHandle  CliHandle,UINT4  u4Command,...));

INT1 EvcProGetEvcConfigPrompt(INT1 *pi1ModeName,INT1 *pi1DispStr);
INT1 EvcProGetEsiConfigPrompt(INT1 *pi1ModeName,INT1 *pi1DispStr);

#define CLI_SET_EVCNAME(IfIndex)            CliSetModeInfo(pu1EvcName)
#define CLI_GET_EVCNAME()                   CliGetModeInfo()
#define CLI_EVC_MODE      "config-evc"


#endif
