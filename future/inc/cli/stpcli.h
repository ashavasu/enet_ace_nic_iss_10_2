
/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stpcli.h,v 1.79 2018/01/09 11:02:04 siva Exp $
*
* Description:  Command constants Definition for CLI STP Commands
*********************************************************************/
#ifndef __STPCLI_H__
#define __STPCLI_H__

/* STP CLI Command constants */

# include"cli.h"

/*COMMAND IDENTIFIRS*/

enum
{
CLI_STP_MOD_STATUS   =1,
CLI_STP_NO_MOD_STATUS,
CLI_STP_SHUT ,
CLI_STP_SYS_CTRL   ,           
CLI_STP_PROVIDER_MOD_STATUS,
CLI_STP_NO_PROVIDER_MOD_STATUS,
CLI_STP_COMP_MODE,         
CLI_STP_NO_COMP_MODE,                  
CLI_STP_BRG_TIMES,           
CLI_STP_NO_BRG_TIMES,      
CLI_STP_TX_HOLDCOUNT,
CLI_STP_NO_TX_HOLDCOUNT,         
CLI_STP_BRG_PRIORITY,
CLI_STP_NO_BRG_PRIORITY,
CLI_STP_PATHCOST,
CLI_STP_NO_PATHCOST,
CLI_STP_DYNAMIC_PATHCOST_CALC,
CLI_STP_DYNAMIC_LAGG_PATHCOST_CALC,
CLI_STP_NO_DYNAMIC_PATHCOST_CALC,
CLI_STP_NO_DYNAMIC_LAGG_PATHCOST_CALC,
CLI_STP_PORT_PROPERTIES,
CLI_STP_NO_PORT_PROPERTIES,
CLI_STP_INT_RESTRIC_ROLE,
CLI_STP_NO_INT_RESTRIC_ROLE,
CLI_STP_INT_RESTRIC_TCN,
CLI_STP_NO_INT_RESTRIC_TCN,
CLI_STP_INT_HELLOTIME,
CLI_STP_NO_INT_HELLOTIME,
CLI_STP_INIT_PROTOCOL_MIGRATION,
CLI_STP_INIT_PORT_PROTOCOL_MIGRATION,
CLI_STP_DEBUG_ENABLE,
CLI_STP_DEBUG_DISABLE,
CLI_STP_SHOW_INFO,
CLI_STP_SHOW_DETAIL,
CLI_STP_SHOW_CVLAN_DETAIL,
CLI_STP_SHOW_PORT_INFO,
CLI_STP_SHOW_ROOT_INFO,
CLI_STP_SHOW_BRIDGE_INFO,
CLI_STP_SHOW_PERF_DATA,
CLI_STP_SHOW_PERF_DATA_IFACE,
CLI_STP_PORT_AUTOEDGE,
CLI_STP_PORT_NO_AUTOEDGE,
CLI_STP_PSEUDOROOT_ID,
CLI_MSTP_PSEUDOROOT_ID,
CLI_STP_NO_PSEUDOROOT_ID,
CLI_MSTP_NO_PSEUDOROOT_ID,
CLI_STP_L2GP,
CLI_STP_NO_L2GP,
CLI_STP_BPDU_RX,
CLI_STP_BPDU_TX,
CLI_STP_SHOW_L2GP_INTF,
CLI_STP_SHOW_L2GP_ALL,
CLI_STP_INTERFACE_LOOP_GUARD,
CLI_STP_NO_INTERFACE_LOOP_GUARD,
#ifdef MSTP_WANTED
CLI_MSTP_BRG_PRIORITY,
CLI_MSTP_ROOT_PRIORITY,
CLI_MSTP_NO_BRG_PRIORITY,
CLI_MSTP_MAX_HOPS,
CLI_MSTP_NO_MAX_HOPS,
CLI_MSTP_CONFIG_MODE,
CLI_MST_NAME,
CLI_MST_NO_NAME,
CLI_MST_REVISION,
CLI_MST_NO_REVISION,
CLI_MSTP_MAX_INST,
CLI_MSTP_NO_MAX_INST,
CLI_MSTP_INST_MAP_ENABLED,
CLI_MSTP_INST_MAP_DISABLED,
CLI_MSTP_VLAN_INST,
CLI_MSTP_NO_VLAN_INST,
CLI_MST_PORT_PROPERTIES,
CLI_MST_NO_PORT_PROPERTIES,
CLI_STP_SHOW_MST_INFO,
CLI_STP_SHOW_MST_INST_MAP,
CLI_STP_SHOW_MST_INTF,
CLI_STP_SHOW_PERF_DATA_INST_IFACE,
CLI_MSTP_INSTANCE,
CLI_MSTP_NO_INSTANCES,
CLI_MSTP_EXT_SYS_ID,
CLI_MSTP_COUNTER_RESET,
CLI_MSTP_PORT_COUNTER_RESET,
#endif
CLI_STP_BPDUGUARD_DISABLE,
CLI_STP_BPDUGUARD_ENABLE,
CLI_STP_NO_BPDUGUARD,
CLI_STP_GUARD_ROOT,
CLI_STP_NO_GUARD,
#ifdef PVRST_WANTED
CLI_STP_ENCAP_DOT1Q,
CLI_STP_ENCAP_ISL,
CLI_STP_PVRST_INST_STATUS_DOWN_PORT,
CLI_STP_PVRST_INST_STATUS_UP_PORT,
CLI_STP_INST_PROP,
CLI_STP_FORWARD_TIME,
CLI_STP_HELLO_TIME,
CLI_STP_MAX_AGE,
CLI_STP_HOLD_COUNT,
CLI_STP_NO_INST_PROP,
CLI_STP_VLAN_PORTPRIORITY,
CLI_STP_NO_VLAN_PORTPRIORITY,
CLI_STP_VLAN_COST,
CLI_STP_NO_VLAN_COST,
CLI_STP_SHOW_PVRST_INST_DET,
CLI_STP_SHOW_PVRST_INST_BRG_DET,
CLI_STP_SHOW_PVRST_INST_ROOT_DET,
CLI_STP_SHOW_PVRST_INTERFACE_DET,
CLI_STP_VLAN_ACTIVE,
CLI_STP_VLAN_DETAIL,
CLI_STP_VLAN_ACTIVE_DETAIL,
CLI_STP_VLAN_BLOCKEDPORTS,
CLI_STP_VLAN_PATHCOST,
CLI_STP_VLAN_SUMMARY,
CLI_STP_VLAN_BRIDGE_ADDRESS,
CLI_STP_VLAN_BRIDGE_DETAIL,
CLI_STP_VLAN_BRIDGE_FORWARD_TIME,
CLI_STP_VLAN_BRIDGE_HELLO_TIME,
CLI_STP_VLAN_BRIDGE_ID,
CLI_STP_VLAN_BRIDGE_MAX_AGE,
CLI_STP_VLAN_BRIDGE_PROTOCOL,
CLI_STP_VLAN_BRIDGE_PRIORITY_SYSTEM,
CLI_STP_VLAN_BRIDGE_PRIORITY,
CLI_STP_VLAN_ROOT_ADDRESS,
CLI_STP_VLAN_ROOT_COST,
CLI_STP_VLAN_ROOT_DETAIL,
CLI_STP_VLAN_ROOT_FORWARD_TIME,
CLI_STP_VLAN_ROOT_HELLO_TIME,
CLI_STP_VLAN_ROOT_ID,
CLI_STP_VLAN_ROOT_MAX_AGE,
CLI_STP_VLAN_ROOT_PORT,
CLI_STP_VLAN_ROOT_PRIORITY,
CLI_STP_VLAN_ROOT_PRIORITY_SYSTEM,
CLI_STP_VLAN_INTF_ACTIVE,
CLI_STP_VLAN_INTF_COST,
CLI_STP_VLAN_INTF_PRIORITY,
CLI_STP_VLAN_INTF_DET,
CLI_STP_VLAN_INTF_STATE,
CLI_STP_VLAN_INTF_ROOT_COST,
CLI_STP_VLAN_INTF_STATS,
CLI_STP_PVRST_FLUSH_IND_THRESHOLD,
CLI_STP_PVRST_NO_FLUSH_IND_THRESHOLD,

#endif
#ifdef PBB_WANTED
  CLI_STP_PBB_LINK_TYPE,
  CLI_STP_PBB_NO_LINK_TYPE,
#endif
CLI_STP_GBL_DEBUG_ENABLE,
CLI_STP_GBL_DEBUG_DISABLE,
CLI_STP_COUNTER_RESET,
CLI_STP_PORT_COUNTER_RESET,
CLI_MSTP_BRG_TIMES,
CLI_MSTP_NO_BRG_TIMES, 
CLI_STP_FLUSH_INTERVAL, 
CLI_STP_NO_FLUSH_INTERVAL,
CLI_STP_FLUSH_IND_THRESHOLD,
CLI_STP_NO_FLUSH_IND_THRESHOLD,
CLI_CVLAN_STP_DEBUG_ENABLE,
CLI_CVLAN_STP_DEBUG_DISABLE,
CLI_MSTP_FLUSH_IND_THRESHOLD,
CLI_MSTP_NO_FLUSH_IND_THRESHOLD,
CLI_STP_OPTIMIZATION_ALT_ROLE,
CLI_STP_GBL_BPDUGUARD_ENABLE,
CLI_STP_GBL_BPDUGUARD_DISABLE,
CLI_STP_SHOW_PORT_BPDUGUARD,
CLI_STP_ERROR_RECOVERY,
CLI_STP_SHOW_PORT_INCONSISTENCY,
CLI_AST_PERF_DATA_ENABLED,
CLI_AST_PERF_DATA_DISABLED,
CLI_STP_DOT1W_ENABLED,
CLI_STP_DOT1W_DISABLED,
CLI_STP_BPDU_FILTER
};

/* Constants defined for RSTP/MSTP/PVRST CLI. These values are passed as arguments from
* the def file
*/

#define STP_CLI_MAX_ARGS     5 

#define  DEFAULT_PORT_COST    2000000


#ifdef MSTP_WANTED
#define MST_NO_REGION     0
#define MST_NO_VLANS     0

#define MST_SET_CMD      1
#define MST_NO_CMD       0

#define INST_SET_CMD     1
#define INST_NO_CMD      0
#endif

#define RST_SET_CMD      1
#define RST_NO_CMD      0

#define PVRST_SET_CMD      1
#define PVRST_NO_CMD      0

#define P2P_TRUE             1
#define P2P_FALSE            0




#define  STP_FORWARD_TIME     1
#define  STP_HELLO_TIME       2
#define  STP_MAX_AGE          3
#define STP_PORT_COST         4
#define STP_LINK_TYPE         5
#define STP_POINT_TO_POINT    6
#define STP_SHARED_LAN        7

#define  STP_PATHCOST_METHOD  8 
#define  STP_ACTIVE_PORTS    9
#define  STP_BLOCKED_PORTS    10 
#define  STP_SUMMARY_PORT_STATES 11

#define  STP_PORT_PRIORITY     12
#define  STP_PORTFAST        13
#define  STP_PORT_STATE      14
#define  STP_PORT_STATS      15

#define  STP_ROOT_COST      16
#define  STP_ROOT_PORT      17
#define  STP_BRG_PROTOCOL     18

#define AST_STATE_MACHINE_DBG    19
#define STP_SHOW_DETAIL          20
#define STP_ID                   21
#define STP_PRIORITY             22
#define STP_ADDRESS              23
#define STP_PROTOCOL             24
#define STP_ACTIVE_DETAIL        25
#define STP_SHOW_INSTANCE_DETAIL 26
#define STP_RED_SUMMARY          27
#define STP_NO_PORT_COST         28
#define STP_RESTRICTED_ROLE      29
#define STP_RESTRICTED_TCN       30
#define PVRST_ROOT_GUARD           31
#define PVRST_ENCAP_TYPE           32
#define STP_PSEUDO_ROOTID          33


#define RST_OPTIMIZATION_ENABLE 1
#define RST_OPTIMIZATION_DISABLE 2


#define CLI_AST_ENABLED                21
#define CLI_AST_DISABLED               22

/* Macros used  From rstpcli.h*/
#define CLI_RSTP_MAX_BRIDGEID_BUFFER   8
#define CLI_RSTP_MAX_PORTID_BUFFER    2
#define CLI_RSTP_MAX_ADDRESS_BUFFER  32
#define AST_ARRAY_TIMESTAMP           64

#define RST_CLI_ALL_ENABLE_TRC 0x000000ff

/* Macros used from mstpcli.h*/
#ifdef MSTP_WANTED
#define CLI_MSTP_MAX_PORTID_BUFFER   2
#define CLI_MSTP_MAX_BRIDGEID_BUFFER  8
#define CLI_MSTP_MAX_NAME_BUFFER       36

#define MST_CLI_ALL_ENABLE_TRC 0x000000ff

# define CLI_MODE_MST            "mst-conf"
#endif

/*Macros used in stpcmd.def*/
#define CLI_STP_ROOT_PRIMARY   1
#define CLI_STP_ROOT_SECONDARY 2
#define CLI_STP_ROOT_DEFAULT   3

#define CLI_STP_PVRST_ROOT_PRIMARY   61441
#define CLI_STP_PVRST_ROOT_SECONDARY 61442

#define CLI_STP_ROOT_PRIMARY_PRIORITY 24576
#define CLI_STP_ROOT_SECONDARY_PRIORITY 28672
#define CLI_STP_ROOT_DEFAULT_PRIORITY 32768

enum
{
  CLI_STP_TIMER_RELATIONSHIP_ERR = 1,

  CLI_STP_BRIDGE_PRIORITY_ERR,

  CLI_STP_INVALID_INSTANCE_ID_ERR,
  
  CLI_STP_INVALID_VLAN_ID_ERR,
 
  CLI_STP_NO_PORT_ERR,

  CLI_STP_TUNNEL_PROTO_ERR,
  
  CLI_STP_PORT_TYPE_ERR,

  CLI_STP_PORT_PRIORITY_ERR,

  CLI_STP_PORT_PATHCOST_ERR,

  CLI_STP_INVALID_VALUE,

  CLI_STP_RES_ROLE_TCN_PORT_ERR,

  CLI_STP_SVLAN_GLOB_MOD_ERR,
  
  CLI_STP_BRG_MODE_ERR,
  
  CLI_STP_1AD_BRIDGE_ERR,
  
  CLI_STP_PVRST_PB_ERR,

  CLI_STP_RES_ROLE_TCN_L2GP_ERR,

  CLI_STP_L2GP_BPDUTX_ERR,

  CLI_STP_MAX_PORT_ERROR,

  CLI_RSTP_VIP_CONF_ERR,

  CLI_STP_NO_EDGE_LOOP_GUARD_ERR,
#ifdef MSTP_WANTED 

  CLI_STP_PORT_INST_ERR,

  CLI_STP_COMPATIBILITY_ERR,

  CLI_STP_VERSION_ERR,

  CLI_STP_MAX_INST_ERR,
  
  CLI_STP_DEBUG_OPTION_ERR,
  
  CLI_STP_SHARED_VLAN_ERR,

  CLI_STP_VLANS_NOT_MAPPED_ERR,
  
  CLI_STP_VLAN_LIST_ERR,
  
  CLI_STP_MAPPED_INST_ERR,
  
  CLI_STP_BASE_BRIDGE_MST_ENABLED,

  CLI_STP_VLAN_MAP_ERR,
  
  CLI_STP_VLAN_UNMAP_ERR,

  CLI_STP_MST_BRG_INVALID_ERR,

  CLI_STP_MST_INVALID_VLAN_LIST_ERR,
#endif

#ifdef PVRST_WANTED

  CLI_STP_MODE_ERR,

  CLI_STP_VLAN_NOTSTARTED_ERR,

  CLI_STP_CXT_NOTPRESENT_ERR,

  CLI_STP_HYBRID_PVID_ERR,

  CLI_STP_HYBRID_UNTAG_ERR,

  CLI_STP_HYBRID_VLAN_ERR,

  CLI_STP_ACCESS_PVID_ERR,

  CLI_STP_FORBIDDEN_PORT_ERR,

  CLI_STP_ASYNC_MODE_ERR,

  CLI_STP_BASE_BRIDGE_PVRST_ENABLED,

  CLI_STP_CONFIG_ROOT_GUARD_ERR,

  CLI_STP_TRUNK_ENCAP_ERR,
  CLI_STP_CONFIG_ROOT_GUARD_NO_EXISTS,
  CLI_STP_NO_VLAN_EXISTS_ERR,
  CLI_STP_PORT_NOT_VLAN_MEMBER_ERR,
  CLI_STP_ACCESS_PORT_PVID_ERR,

#endif
  CLI_STP_NO_SAME_INST_IN_DIFF_CTXT,

  CLI_STP_SISP_BRG_MODE_ERR,

  CLI_STP_SISP_COMPATIBLE_ERR,

  CLI_STP_SISP_PROTO_MIG_ERR,

  CLI_STP_SISP_INVALID_PROP_ERR,

  CLI_STP_NO_SHARED_LOOP_GUARD_ERR,

  CLI_STP_NO_CEP_ERR,

  CLI_STP_NO_CEP_SVLAN_ERR,

  CLI_STP_NO_LOOP_GUARD_EDGE_ERR,
  
  CLI_STP_SPECIFY_VLAN_ERR,

  CLI_STP_ROOT_PRIMARY_ERR,
  
  CLI_STP_NO_STP_ON_ICCL_ERR,

  CLI_STP_NO_STP_ON_MCLAG_ERR,

  CLI_STP_NO_MSTP_ON_ICCL_ERR, 

  CLI_STP_NO_MSTP_ON_MCLAG_ERR, 

  CLI_STP_NO_PVRST_ON_ICCL_ERR,

  CLI_STP_NO_PVRST_ON_MCLAG_ERR,

  CLI_STP_ROOT_SECONDARY_ERR,

  CLI_STP_ROOTGUARD_ENABLED_ERR,

  CLI_STP_LOOPGUARD_ENABLED_ERR,

  CLI_STP_MAX_ERR
};


#if defined (__STPCLI_C__) || defined (__STPMICLI_C__)  

CONST CHR1  *StpCliErrString [] = { 
     
       NULL, 
     " % The following Relation should be observed\r\n 2*(ForwardDelay -1)>=MaxAge >= 2*(Hello Time + 1)\r\n" , 
     " % Bridge Priority must be in increments of 4096 and can be upto 61440.\r\nAllowed values are:\r\n0     4096  8192  12288 16384 20480 24576 28672\r\n32768 36864 40960 45056 49152 53248 57344 61440\r\n",
     " % Invalid Instance ID\r\n",
     " % Invalid Vlan ID\r\n",
     " % Port does not exist\r\n",
     " % Protocol tunnel status should be set to peer\r\n",
     " % In provider bridge, stp cannot be enabled on tunnel port\r\n",
     " % Port Priority must be in increments of 16 upto 240\r\n",
     " % Pathcost must be in the range (1-65535) in STP compatible mode\r\n",
     " % Invalid Value\r\n",
     " % Restricted Role/Tcn cannot be disabled on this port. \r\n",
     " % Provider Spanning tree can not be enabled when global module is disabled. \r\n",
     " % Spanning tree cannot be started before bridge mode is set. \r\n",
     " % Bridge is not provider core bridge or provider edge bridge. \r\n",
     " % PVRST can be enabled only on customer bridge mode. \r\n",
     " % Resticted Role/Tcn cannot be set on L2gp Port. \r\n",
     " % bpdu transmit should be disabled on L2gp Port and Customer Backbone Ports. \r\n",
     " % Total Number of ports in this swtich is greater than 4094! Exiting.. \r\n",
     " % AutoEdge and AdminEdge cannot be configured for logical VIPs.. \r\n",
     " % Loop guard is can not be enabled on an Edge Port.. \r\n",
#ifdef MSTP_WANTED
     " % This Port is not a part of the specified Instance \r\n", 
     " % Bridge is not in MST mode\r\n",
     " % Please configure the hello timers for MST on a per-port basis\r\n",
     " % MSTP Maximum Instance Configuring  Failed \r\n",
     " % MSTP does not support state-machine variable changes debug option\r\n",
     " % Only default CIST context can exist in Shared VLAN learning\r\n",
     " % Vlans are not mapped to the specified instance\r\n",
     " % Inconsistent Vlan List \r\n",
     " % Vlans already mapped to the instance \r\n",
     " % MSTP Module cannot be enabled in Transparent Mode\r\n",
     " % Vlan cannot be mapped to instance Id\r\n",
     " % Vlan cannot be unmapped from instance Id\r\n",
     " % MSTP cannot be enabled in I-Component\r\n",
     " % One or more VLAN(s) in the VlanList is already mapped to some other non CIST instance\r\n",
#endif
#ifdef PVRST_WANTED
     " % Bridge is in MST mode\r\n", 
     " % Vlan module is shutdown. PVRST cannot be started !!  \r\n", 
     " % Current Context is not present in VLAN. PVRST cannot be started !!  \r\n",
     " % Port PVID is other than Default VLAN. PVRST cannot be started !!  \r\n",
     " % Port is not an untagged member of VLAN 1. PVRST cannot be started !!  \r\n",
     " % Port is a member of Vlan other than default VLAN. PVRST cannot be started !! \r\n",
     " % Access Port is member of Vlan other than PVID Vlan. PVRST cannot be started !!  \r\n",
     " % Forbidden port-list of any Vlan is not NULL. PVRST cannot be started !!  \r\n",    
     " % PVRST does not support Asynchronous NPAPI programming mode. \r\n",
     " % PVRST Module cannot be enabled in Transparent Mode\r\n",
     " % Make the port as trunk to enable root guard on the interface \r\n",
     " % Encapsulation is applicable only on trunk ports \r\n",
     " % Port is Not Configured as Trunk Port \r\n",
     " % Vlan Does Not Exist \r\n",
     " % Port is not member of Vlan \r\n",
     " % Access Port is member of Vlan other than PVID Vlan!!  \r\n",
#endif
     " % SISP enabled port cannot be part of same instance in two different contexts\r\n",
     " % RSTP cannot be enabled in a context that has SISP enabled ports\r\n",
     " % Compatibility cannot be changed to STP/RSTP in a context that has SISP enabled ports\r\n",
     " % Protocol Migration cannot be enabled on SISP Logical interfaces\r\n",
     " % AdminEdge, AutoEdge & P2P cannot be configured on SISP Logical interfaces\r\n",
     " % Loop guard cannot be enabled on an shared link.. \r\n",
     " % Debug customer spanning-tree can be enabled only on customer edge ports\r\n",
     " % Customer edge Port does not exist in the SVLAN context\r\n",
     " % AdminEdge cannot be configured on Loop guard enabled port\r\n",
     " % PVRST enabled : Specify Vlan Id \r\n",
     " % Failed to make this bridge as root for this instance\r\n",
     " % RSTP cannot be enabled on ICCL interface\r\n",
     " % RSTP cannot be enabled on MC-LAG interface\r\n",
     " % MSTP cannot be enabled on ICCL interface\r\n",
     " % MSTP cannot be enabled on MC-LAG interface\r\n",
     " % PVRST cannot be enabled on ICCL interface\r\n",
     " % PVRST cannot be enabled on MC-LAG interface\r\n",
     " % Failed to make this bridge as secondary for this instance\r\n",
     " % Loop Guard cannot be enabled on Root Guard enabled ports\r\n",
     " % Root Guard cannot be enabled on Loop Guard enabled ports\r\n",
     "\r\n"
};
#else
extern CONST CHR1  *StpCliErrString [];
#endif
#ifdef MSTP_WANTED
INT1  MstpGetMstpCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);
INT1  MstpGetVcmMstpCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);
#endif

INT4
AstCliSelectContextOnMode (tCliHandle CliHandle, UINT4 u4Cmd, 
                           UINT4 *pu4Context, UINT2 *pu2LocalPort);

INT4
AstCliGetContextForShowCmd (tCliHandle CliHandle, UINT1 *pu1Name, 
                            UINT4 u4IfIndex, UINT4 u4CurrContext, 
                            UINT4 *pu4NextContext, UINT2 *pu2LocalPort);

INT4  cli_process_stp_cmd  PROTO((tCliHandle  CliHandle,UINT4  u4Command, ...));

INT4  cli_process_stp_show_cmd PROTO((tCliHandle  CliHandle,
                                      UINT4  u4Command, ...));

INT4 cli_process_stp_pb_cmd PROTO((tCliHandle CliHandle,UINT4 u4Command, ...));

INT4 cli_process_stp_pb_show_cmd PROTO((tCliHandle CliHandle, 
                                        UINT4 u4Command, ...));
INT4
StpCliSetDebugLevel (tCliHandle CliHandle, INT4 i4CliDebugLevel);

#endif


