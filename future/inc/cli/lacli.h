

/* INCLUDE FILE HEADER :
 *
 * $Id: lacli.h,v 1.71 2017/11/16 14:26:51 siva Exp $
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : lacli.h                                          |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR      : Vibha R                                          |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : LA                                               |
 * |                                                                           |
 * |  MODULE NAME           : CLI interface for LA configuration               |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    :                                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION           : This file contains command idenfiers for         | 
 * |                          definitions in lacmd.def, function prototypes    |
 * |                          for LACLI and corresponding error code           |
 * |                          definitions                                      |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 *
 */

#ifndef __LACLI_H__
#define __LACLI_H__
#include "cli.h"
/* LA CLI command constants. To add a new command ,a new definition needs to
 * be added to the list.
 */
enum 
{
  CLI_LA_SYSTEMCONTROL = 1,
  CLI_LA_SYSTEMSHUT,
  CLI_LA_NO_SYSTEMSHUT,
  CLI_LA_SYSTEM_PRIORITY,
  CLI_LA_SYSTEM_ID,
  CLI_LA_NO_SYSTEM_ID,
  CLI_LA_NO_SYSTEM_PRIORITY,
  CLI_LA_POLICY,
  CLI_LA_NO_POLICY,
  CLI_LA_TIMEOUT,
  CLI_LA_NO_TIMEOUT,
  CLI_LA_ACTOR_PORTPRIORITY,
  CLI_LA_ACTOR_ADMIN_PORT,
  CLI_LA_NO_ACTOR_PORTPRIORITY,
  CLI_LA_PORT_ACTOR_MODE,
  CLI_LA_PORT_MODE_DISABLED,
  CLI_LA_PORT_WAIT_TIME,
  CLI_LA_NO_PORT_WAIT_TIME,
  CLI_LA_PORT_ADMIN_KEY,
  CLI_LA_PORTCHANNEL_MAC_SEL,
  CLI_LA_PORTCHANNEL_DEF_PORT,
  CLI_LA_PORTCHANNEL_NO_DEF_PORT,
  CLI_SHOW_LA_PORTCHANNEL_INFO,
  CLI_SHOW_LA_PORT_INFO,
  CLI_SHOW_LACP_INFO,
  CLI_LA_PORTCHANNEL_MAX_PORTS,
  CLI_LA_SET_TRACE_LEVEL,
  CLI_LA_RESET_TRACE_LEVEL,
  CLI_LA_NO_PARTNER_INDEP,
  CLI_LA_DLAG_SYSTEM_PRIORITY,
  CLI_LA_DLAG_SYSTEM_ID,
  CLI_LA_DLAG_NO_SYSTEM_ID,
  CLI_LA_DLAG_NO_SYSTEM_PRIORITY,
  CLI_LA_DLAG_PERIODIC_SYNC_TIME,
  CLI_LA_DLAG_MS_SELECTION_WAIT_TIME,
  CLI_LA_DLAG_DISTRIBUTE_PORT,
  CLI_LA_DLAG_NO_DISTRIBUTE_PORT,
  CLI_LA_DLAG_DISTRIBUTE_PORT_LIST,
  CLI_LA_DLAG_NO_DISTRIBUTE_PORT_LIST,
  CLI_LA_DLAG_STATUS,
  CLI_LA_DLAG_REDUNDANCY,
  CLI_SHOW_LA_DLAG_INFO,
  CLI_SHOW_LA_DLAG_CONSOLIDATED,
  CLI_SHOW_LA_DLAG_COUNTERS,
  CLI_LA_DLAG_GLOBAL_STATUS,
  CLI_LA_DLAG_GLOBAL_SYSTEM_PRIORITY,
  CLI_LA_DLAG_GLOBAL_SYSTEM_ID,
  CLI_LA_DLAG_GLOBAL_DISTRIBUTE_PORT,
  CLI_LA_DLAG_GLOBAL_DISTRIBUTE_PORT_LIST,
  CLI_LA_DLAG_GLOBAL_NO_SYSTEM_ID,
  CLI_LA_DLAG_GLOBAL_NO_SYSTEM_PRIORITY,
  CLI_LA_DLAG_GLOBAL_NO_DISTRIBUTE_PORT,
  CLI_LA_DLAG_GLOBAL_NO_DISTRIBUTE_PORT_LIST,
  CLI_LA_DLAG_GLOBAL_PERIODIC_SYNC_TIME,
  CLI_LA_DLAG_GLOBAL_NO_PERIODIC_SYNC_TIME,
  CLI_LA_MCLAG_NO_SYSTEM_PRIORITY,
  CLI_LA_ERR_TIME_SET,
  CLI_LA_ERR_THRESHOLD_SET,
  CLI_LA_ERR_THRESHOLD_RESET,
  CLI_LA_MCLAG_GLOBAL_STATUS,
  CLI_LA_MCLAG_GLOBAL_SYSTEM_PRIORITY,
  CLI_LA_MCLAG_GLOBAL_SYSTEM_ID,
  CLI_LA_MCLAG_GLOBAL_NO_SYSTEM_ID,
  CLI_LA_MCLAG_GLOBAL_NO_SYSTEM_PRIORITY,
  CLI_LA_MCLAG_GLOBAL_PERIODIC_SYNC_TIME,
  CLI_LA_MCLAG_GLOBAL_NO_PERIODIC_SYNC_TIME,
  CLI_SHOW_LA_MCLAG_CONSOLIDATED,
  CLI_SHOW_LA_MCLAG_INFO,
  CLI_SHOW_LA_MCLAG_COUNTERS,
  CLI_LA_MCLAG_STATUS,
  CLI_LA_MCLAG_SYSTEM_ID,
  CLI_LA_MCLAG_SYSTEM_PRIORITY,
  CLI_LA_MCLAG_NO_SYSTEM_ID,
  CLI_LA_PORT_DEF_STATE_THRESHOLD_SET,
  CLI_LA_PORT_DEF_STATE_THRESHOLD_RESET,
  CLI_LA_DEF_STATE_THRESHOLD_SET,
  CLI_LA_DEF_STATE_THRESHOLD_RESET,
  CLI_LA_HW_FAILURE_REC_THRESHOLD_SET,
  CLI_LA_SAME_STATE_REC_THRESHOLD_SET,
  CLI_LA_PORT_HW_FAILURE_REC_THRESHOLD_SET,
  CLI_LA_PORT_SAME_STATE_REC_THRESHOLD_SET,
  CLI_LA_REC_THRESH_EXCEED_ACT_SET,
  CLI_LA_MCLAG_CLEAR_COUNTER,
  CLI_LACP_CLEAR_COUNTERS,
  CLI_LA_MCLAG_SYSTEMSHUT,
  CLI_LA_MCLAG_NO_SYSTEMSHUT,
  CLI_LA_ENHANCED_LB_STATUS,
  CLI_LA_RANDOMIZED_LB_STATUS

};
#define MAX_LEN_TEMP_ARRAY                      255
#define LA_CLI_MAX_ARGS                         5
#define LA_CLI_ACTIVE                           1
#define LA_CLI_DISABLED                         3
#define LA_CLI_MANUAL                           2
#define LA_CLI_PASSIVE                          4

#define LA_CLI_FALSE                            0
#define LA_CLI_TRUE                             1


#define LA_CLI_ACTOR           1
#define LA_CLI_PARTNER         2

#define CLI_SHOW_LACP_COUNTERS 1 
#define CLI_SHOW_LACP_NEIGHBOR 2 

#define LA_DEFAULT_VALUE 0

#define LA_CLI_MAX_COMMANDS  18
#define LA_CLI_MAX_TEMP      20
#define LA_CLI_MAX_FLAG_DSP   10
#define LA_CLI_MAX_STATE     5

/* DEFAULT VALUES MACROS */
#define LA_PORT_WAITTIME_DEF 2

/* Display of D-LAG distribute ports in show command*/
#define LA_DLAG_CLI_MAX_PORTS_PER_LINE 5

/* DISPLAY COMMAND OPTIONS */
#define LA_SHOW_DETAIL 1
#define LA_SHOW_LOADBALANCE 2
#define LA_SHOW_PORT 3
#define LA_SHOW_PORTCHANNEL 4
#define LA_SHOW_SUMMARY 5
#define LA_SHOW_PROTOCOL 6
#define LA_SHOW_INT_ETHERCHANNEL 7
#define LA_SHOW_REDUNDANCY       8
#define LA_SHOW_MCLAG 9
#define LA_SHOW_ICCL 10
#define LA_ALL_TRC     (INIT_SHUT_TRC | \
                       MGMT_TRC | \
                       DATA_PATH_TRC | \
                       CONTROL_PLANE_TRC | \
                       DUMP_TRC | \
                       OS_RESOURCE_TRC | \
                       ALL_FAILURE_TRC | \
                       BUFFER_TRC)

/* Error codes 
 * Error code values and strings are maintained inside the protocol module itself.
 */
enum {
    CLI_LA_PORT_CHANNEL_ERR = 1,
    CLI_LA_DIFFSRV_ENABLE_ERR,
    CLI_LA_PNAC_ENABLE_ERR,
    CLI_LA_INCOMPATIBLE_MODE,
    CLI_LA_MIRRORTO_PORT_ERR,
    CLI_LA_AGG_MIRR_ERR,
    CLI_LA_PORT_MIRR_ERR,
    CLI_LA_KEY_CHANGE_ERR,
    CLI_LA_MAXPORT_MANUAL_ERR,
    CLI_LA_MAXPORT_ERR,
    CLI_LA_CHANGE_LACPMODE_ERR,
    CLI_LA_ZERO_MAC_ERR,
    CLI_LA_CONFLICT_MAC_ERR,
    CLI_LA_INVALID_IFACE_TYPE,
    CLI_LA_INVALID_PORT,
    CLI_LA_SISP_CANNOT_BE_PO_MEMBER,
    CLI_LA_CONTEXT_MISMATCH,
    CLI_LA_DEF_PORT_ALREADY_USED_ERR,
    CLI_LA_INVALID_DEF_PORT,
    CLI_LA_BASE_BRIDGE_LA_ENABLED,
    CLI_LA_SELECTION_POLICY_ERR,
    CLI_LA_INVALID_MAX_PORT,
    CLI_LA_MODULE_SHUTDOWN,
    CLI_LA_ADMIN_KEY_DEF_PORT_ERR,
    CLI_LA_PORT_UNMAP_ERR,
    CLI_LA_MTU_MISMATCH,
    CLI_LA_DLAG_STATUS_ENABLED,
    CLI_LA_DLAG_SYS_PRIO_DLAG_STATUS_ENABLED,
    CLI_LA_DLAG_SYS_ID_DLAG_STATUS_ENABLED,
    CLI_LA_DLAG_DIST_PORT_DLAG_STATUS_ENABLED,
    CLI_LA_DLAG_DIST_PORT_IS_MEMBER_PORT,
    CLI_LA_DLAG_DIST_PORT_OPER_STATUS_DOWN,
    CLI_LA_DLAG_INVALID_SYS_ID,
    CLI_LA_DLAG_INVALID_SYS_PRIORITY,
    CLI_LA_DLAG_INVALID_PERIODIC_SYNC_TIME,
    CLI_LA_AA_DLAG_INVALID_PERIODIC_SYNC_TIME,
    CLI_LA_DLAG_INVALID_MS_SELECTION_WAIT_TIME,
    CLI_LA_DLAG_STATUS_INTER_COLUMNAR_DEPENDENCY_ERR,
    CLI_LA_DLAG_GLOBAL_STATUS_INTER_COLUMNAR_DEPENDENCY_ERR,
    CLI_LA_DLAG_PER_CHANNEL_CONFIG_ERROR,
    CLI_LA_SELECTION_POLICY_NOT_SUPPORTED_IN_HW,
    CLI_LA_SERVICE_INSTANCE_POLICY_IN_PBB,
    CLI_LA_REMOTE_PORT_CONFIG_ERR,
    CLI_LA_MCLAG_SYS_PRIO_MCLAG_STATUS_ENABLED,
    CLI_LA_MCLAG_SYS_ID_MCLAG_STATUS_ENABLED,
    CLI_LA_MCLAG_INVALID_SYS_ID,
    CLI_LA_MCLAG_INVALID_SYS_PRIORITY,
    CLI_LA_MCLAG_INVALID_PERIODIC_SYNC_TIME,
    CLI_LA_AA_MCLAG_INVALID_PERIODIC_SYNC_TIME,
    CLI_LA_MCLAG_PER_CHANNEL_CONFIG_ERROR, 
    CLI_LA_NO_CUSTOMER_EDGE_PORT_ERR,
    CLI_LA_NO_DIFF_SPEED,
    CLI_LA_MCLAG_STATUS_CONFIG_ERR,
    CLI_LA_PORTTYPE_MISMATCH,
    CLI_LA_PORT_LACP_MODE,
    CLI_LA_PORT_NON_LACP_MODE,
    CLI_LA_MCLAG_NON_DEFAULT_CTXT,
    CLI_LA_PORT_LAST_DOWNLINK_GROUP,
    CLI_LA_MEMBER_CONTEXT_L3LAG_ERR,
    CLI_LA_PORT_MODE_ERR,
    CLI_LA_NO_HALF_DUPLEX_ERR,
    CLI_LA_ICCL_PORT_CHNL_USED_ERR,
    CLI_LA_ICCL_VLAN_USED_ERR,
    CLI_LA_ICCL_RSVD_VLAN_ID_ERR,
    CLI_LA_ICCH_IP_USED,
    CLI_LA_ICCH_SOCK_DEL_FAIL,
    CLI_LA_MCLAG_SHUTDOWN_ERR,
    CLI_LA_ICCL_IN_USE_ERR,
    CLI_LA_MCLAG_ENABLE_FLAG_NOT_SET_ERR,
    CLI_LA_MCLAG_SHUT_IN_PROGRESS,
    CLI_LA_MAX_ERR
};
/* The error strings should be places under the switch so as to avoid redifinition
 * This will be visible only in modulecli.c file
 */
#ifdef __LACLI_C__

CONST CHR1  *LaCliErrString [] = {
    NULL,
    "% This port channel does not exist\r\n",
    "% This port cannot belong to a port channel as DiffServ is enabled on the port\r\n",
    "% This port cannot belong to a port channel as Dot1x is enabled on the port \r\n",
    "% Mismatch between the interface type(router port/switchport) of the port and port-channel : This interface can not be added to the channel group\r\n",
    "% This port cannot belong to a port channel as this port is a monitor port \r\n",
    "% Portchannel is configured in Mirroring. Cannot add/remove ports in it \r\n",
    "% Port is configured in Mirroring. Cannot add/remove port to portchannel \r\n",
    "% This interface is already part of another port channel \r\n ",
    "% This port cannot belong to this  port channel as there are already maximum allowed members in the port channel configured for manual aggregation \r\n ",
    "% This port cannot belong to this port channel as there are already 8 members in the port channel \r\n",
    "% Configuration Failed while changing LACP Mode on this port \r\n ",
    "% Invalid MAC address \r\n",
    "% This MAC address conflicts with another port-channel's MAC address \r\n",
    "% Invalid Interface type \r\n",
    "% Invalid port number \r\n",
    "% SISP enabled port cannot be the member of a port channel\r\n",
    "% The port does not belong to the same Virtual Context as the port channel \r\n",
    "% Interface already attached to a port channel\r\n",
    "% Port channel which has a default port can not include any other ports\r\n",
    "% Port-Channel is not operable in Transparent Mode\r\n",
    "% This selection policy is invalid since it is specific to MPLS\r\n",
    "% Configuration Failed since the already configured members of manual aggregation are greater than the newly configured maximum value \r\n",
    "% LA Module is shutdown\r\n",
    "% Admin key value must be equal to one of the existing port-channels(ID) in which default port has been configured\r\n",
    "% Interface is not mapped to any context \r\n",
    "% Interface MTU is not matching with the Port Channel MTU \r\n",
    "% This D-LAG configuration is not allowed, when D-LAG status is enabled \r\n",
    "% D-LAG System Priority configuration is not allowed, when D-LAG status is enabled \r\n",
    "% D-LAG System Identifier configuration is not allowed, when D-LAG status is enabled \r\n",
    "% D-LAG Distributing port configuration is not allowed, when D-LAG status is enabled \r\n",
    "% Ports which are already part of port-channel cannot be configured as distributing port \r\n",
    "% Distributing port oper status is down \r\n",
    "% Invalid input value for D-LAG System ID \r\n",
    "% Invalid value input for D-LAG System Priority, value should be within the range 0 - 65535 \r\n",
    "% Invalid input value for D-LAG periodic-sync time, value should be within the range 0 - 90000 milliseconds \r\n",
    "% Invalid input value for Active Active D-LAG periodic-sync time, value should be within the range 0 - 90 seconds \r\n",
    "% Invalid input value for D-LAG master-slave-selection-wait time, value should be within the range 0 - 90000 milliseconds \r\n",
    "% D-LAG mandatory parameters (D-LAG System Identifier, D-LAG distribute port) should be configured before modifying D-LAG status \r\n",
    "% Global DLAG parameter (D-LAG System Identifier) should be configured before enabling D-LAG status\r\n", 
    "% Global DLAG Status is enabled. Per port-channel configuration not allowed \r\n",
    "% This Selection Policy is not supported in Hardware \r\n",
    "% Service Instance Selection Policy is only applicable for Provider Back-Bone Bridges\r\n", 
    "% Port-channel configuration is not allowed for Remote port\r\n",
    "% When MC-LAG status is enabled, MC-LAG System Priority configuration is not permitted\r\n",
    "% When MC-LAG status is enabled, MC-LAG System Identifier configuration is not permitted\r\n",
    "% Invalid MC-LAG System ID \r\n",
    "% Invalid MC-LAG System Priority. Permitted range 0 - 65535 \r\n",
    "% Invalid MC-LAG periodic-sync time. Permitted range 0 - 90000 milliseconds \r\n",
    "% Invalid Active Active MC-LAG periodic-sync time. Permitted range 0 - 90 seconds \r\n",
    "% When Global MC-LAG Status is enabled, Per port-channel configuration is not permitted \r\n",
    "% CEP not supported in LAG \r\n",
    "% LAG cannot be created between ports that are operating in different speeds \r\n",
    "% Configure MC-LAG system identifier before modifying MC-LAG status \r\n",
    "% Physical interface port type mismatch with the logical Interface port type\r\n",
    "% ICCL rejects LACP mode links \r\n",
    "% MC-LAG rejects non-LACP mode links \r\n",
    "% MC-LAG cannot be enabled on interface mapped to non-default context \r\n",
    "% Port is the last downlink in the UFD group, can not be added in port channel\r\n",
    "% Member of L3 port-channel must be mapped only to the default context \r\n",
    "% Promiscous port or host port cannot be configured as part of port-channel\r\n",
    "% LAG cannot be created for ports in Half Duplex mode \r\n",
    "% ICCL port-channel already in use.Reset the configuration to enable mc-lag\r\n",
    "% ICCL VLAN already in use.Reset the configuration to enable mc-lag\r\n", 
    "% Reserved VLAN is currently in use.Reset ICCL VLAN ID to enable mc-lag\r\n",
    "% ICCL IP Address is already in use.Reset ICCL IP Address to enable mc-lag\r\n",
    "% MC-LAG cannot be enabled as MC-LAG deletion is in progress.\r\n",
    "% MC-LAG is Shutdown. Cannot Enable/Disable MC-LAG\r\n",
    "% Cannot start MC-LAG, since ICCL parameters are already in use \r\n",
    "% MC-LAG disable is in progress.MC-LAG cannot be enabled\r\n",
    "% MC-LAG shutdown in progress\r\n",
    "\r\n"
};

#endif
/* function prototypes */
INT4 LaSetSystemControl    PROTO ((tCliHandle, INT4));
INT4 LaSetNoPartnerIndep    PROTO ((tCliHandle, INT4));
INT4 LaSetSystemPriority  PROTO ((tCliHandle, INT4));
INT4 LaSetLaPolicy      PROTO ((tCliHandle, INT4 , INT4));
INT4 LaSetTimeout          PROTO ((tCliHandle, UINT4, UINT4));
INT4 LaSetActorPortPriority   PROTO ((tCliHandle, UINT4, INT4)); 
INT4 LaSetPortActorMode     PROTO ((tCliHandle, UINT4, UINT2, UINT4, UINT1));
INT4 LaSetActorPortModeDisabled    PROTO ((tCliHandle, UINT4));
INT4 LaSetActorPortWaitTime        PROTO ((tCliHandle, UINT4, UINT4));
INT4 LaSetActorPortChannelMac      PROTO ((tCliHandle, UINT4, UINT4, tMacAddr));
VOID LaShowPortChannelInfo         PROTO ((tCliHandle ,UINT2, UINT1 ));
VOID LaShowIntEtherChannelInfo     PROTO ((tCliHandle, UINT4));
INT4 ShowLaLoadBalance  PROTO ((tCliHandle, UINT4, UINT1));
INT4 ShowLaProtocol     PROTO ((tCliHandle, UINT4, UINT1));
INT4 ShowLaPortChannel  PROTO ((tCliHandle, UINT4, UINT1));
INT4 ShowLaPort            PROTO ((tCliHandle, UINT4, UINT1));
INT4 ShowLaSummary      PROTO ((tCliHandle, UINT4, UINT1, UINT1));
INT4 LaShowLacpCounters    PROTO ((tCliHandle , INT4));
INT4 LaShowLacpNeighbor    PROTO ((tCliHandle , INT4, UINT1));
INT4 LaCalculateAllow      PROTO ((tCliHandle, UINT4, UINT2));
VOID ShowLaGlobalInfo      PROTO ((tCliHandle));
VOID ShowLaGroupInfo       PROTO ((tCliHandle, UINT4, UINT1));
INT4 LaSetSystemShut (tCliHandle CliHandle, INT4 i4LaStatus);
VOID LaRedPrintSyncUpInfoForPort (tCliHandle CliHandle, UINT2 u2Port);
VOID LaRedReadSyncUpTable (tCliHandle CliHandle);
INT4 LaSetSystemID (tCliHandle , tMacAddr );
INT4 LaSetActorAdminPort (tCliHandle , UINT4 , INT4);
INT4 LaSetPortActorAdminKeyLacpMode PROTO ((tCliHandle ,UINT4 ,UINT4 ,UINT4));
INT4 LaAttachDefaultPortToAggregator PROTO ((tCliHandle , UINT4 , UINT4));
INT4 LaSetActorPortChannelMaxPorts PROTO ((tCliHandle , UINT4 , UINT4)); 
INT4 cli_process_la_cmd    PROTO ((tCliHandle CliHandle, UINT4, ...));
INT4 LaCliSetTraceLevel    PROTO ((tCliHandle, INT4, UINT1));
INT4 AclEnabledForPort PROTO ((UINT4 u4IfIndex));

INT4 LaShowRunningConfig(tCliHandle,UINT4);
INT4 LaShowRunningConfigScalars(tCliHandle);
VOID LaShowRunningConfigTables(tCliHandle);
VOID LaShowRunningConfigInterface(tCliHandle);
VOID LaShowRunningConfigPhysicalInterfaceDetails(tCliHandle,INT4,UINT4);
VOID LaShowRunningConfigPortChannelInterfaceDetails(tCliHandle,INT4);
VOID LaShowRunningConfigDisplayPortDetails(tCliHandle,INT4,UINT4);

VOID IssLaShowDebugging (tCliHandle CliHandle);

/* D-LAG CLI function prototypes */
INT4 LaDLAGSetDLAGSystemPriority PROTO ((tCliHandle , UINT4 , UINT4)); 
INT4 LaDLAGSetDLAGSystemID PROTO ((tCliHandle , tMacAddr, UINT4));
INT4 LaDLAGSetDLAGPeriodicSyncTime PROTO ((tCliHandle , UINT4 , UINT4)); 
INT4 LaDLAGSetDLAGMSSelectionWaitTime PROTO ((tCliHandle , UINT4 , UINT4)); 
INT4 LaDLAGConfigureDistributePort PROTO ((tCliHandle , UINT4 , UINT4)); 
INT4 LaDLAGConfigureDistributePortList PROTO ((tCliHandle , UINT1 *, UINT4));
INT4 LaDLAGSetDLAGStatus PROTO ((tCliHandle, UINT4, UINT4)); 
INT4 LaDLAGSetDLAGRedundancy PROTO ((tCliHandle, UINT4, UINT4)); 
INT4 LaDLAGShowDetail    PROTO ((tCliHandle , INT4 , UINT1));
INT4 LaDLAGShowCounters    PROTO ((tCliHandle , INT4));

/* Active-Active DLAG CLI funtion prototypes */

INT4 LaDLAGSetDLAGSystemStatus PROTO ((tCliHandle,UINT4));
INT4 LaDLAGSetDLAGGlobalSystemPriority PROTO ((tCliHandle , UINT4 ));

INT4 LaDLAGSetDLAGGlobalSystemID PROTO ((tCliHandle , tMacAddr ));
INT4 LaDLAGSetDLAGGlobalPeriodicSyncTime PROTO ((tCliHandle , UINT4 ));

INT4 LaDLAGConfigureGlobalDistributePort PROTO ((tCliHandle , UINT4));
INT4 LaDLAGConfigureGlobalDistributePortList PROTO ((tCliHandle , UINT1 *));

extern INT4 CfaUpdtIvrInterface PROTO ((UINT4 u4IfIndex, UINT1 u1Status));

/* MCLAG CLI funtion prototypes */
INT4 LaSetMCLAGSystemStatus PROTO ((tCliHandle,UINT4));
INT4 LaMCLAGSetDLAGGlobalSystemID (tCliHandle CliHandle, tMacAddr SystemID);
INT4 LaMCLAGSetMCLAGGlobalSystemPriority (tCliHandle CliHandle,
                                   UINT4 u4LaMCLAGSystemPriority);
INT4
LaMCLAGSetMCLAGStatus (tCliHandle CliHandle, UINT4 u4MCLAGStatus, 
                        UINT4 u4AggIndex);
INT4
LaMCLAGSetMCLAGSystemID (tCliHandle CliHandle, tMacAddr SystemID,
                       UINT4 u4AggIndex);
INT4
LaMCLAGSetMCLAGSystemPriority (tCliHandle CliHandle, UINT4 u4LaMCLAGSystemPriority,
                             UINT4 u4AggIndex);
INT4
LaMCLAGSetMCLAGMSSelectionWaitTime (tCliHandle CliHandle,
                                  UINT4 u4MSSelectionWaitTime, UINT4 u4AggIndex);
INT4
LaMCLAGShowDetail (tCliHandle CliHandle, INT4 i4Key, UINT1 u1IsConsolidated);
INT4
LaMCLAGShowCounters (tCliHandle CliHandle, INT4 i4Key);

INT4
LaMCLAGSetMCLAGGlobalPeriodicTime (tCliHandle CliHandle,
                                   UINT4 u4LaMCLAGPeriodicTime);
extern INT4
LaSetErrorRecTime  (tCliHandle CliHandle, INT4 i4ErrRecDuration);

extern INT4
LaSetErrorRecThreshold  (tCliHandle CliHandle, INT4 i4ErrThreshold);

INT4
LaSetPortDefaultedThreshold (tCliHandle CliHandle, UINT4 u4IfIndex, INT4 i4PortDefThreshold);

INT4
LaSetDefaultedThreshold (tCliHandle CliHandle, INT4 i4DefThreshold);

INT4
LaSetHwFailureRecThreshold (tCliHandle CliHandle, INT4 i4HwFailureRecThreshold);

INT4
LaSetPortHwFailureRecThreshold (tCliHandle CliHandle, UINT4 u4IfIndex, INT4 i4PortHwFailureRecThreshold);

INT4
LaSetSameStateRecThreshold (tCliHandle CliHandle, INT4 i4SameStateThreshold);

INT4
LaSetPortSameStateRecThreshold (tCliHandle CliHandle, UINT4 u4IfIndex, INT4 i4PortSameStateRecThreshold);

INT4
LaSetRecThresholdExceedAction (tCliHandle CliHandle, UINT4 u4RecThresholdExceedAction);

INT4
LaMCLAGClearCounters (tCliHandle CliHandle);

INT4
LaClearStatistics (tCliHandle CliHandle);

INT4
LaPortChannelClearStatistics (tCliHandle CliHandle, INT4 i4Key);

#endif /* _LACLI_H_ */
