/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppcli.h,v 1.16 2014/11/24 12:08:09 siva Exp $
 *
 * Description:
 *
 *******************************************************************/

#ifndef __PPPCLI_H__
#define __PPPCLI_H__

/* MSAD ADD -S- */

#include "cli.h"

/* Row Status Definitions - same as the MIB variable */
#define   CFA_RS_ACTIVE               1
#define   CFA_RS_NOTINSERVICE         2
#define   CFA_RS_NOTREADY             3
#define   CFA_RS_CREATEANDGO          4
#define   CFA_RS_CREATEANDWAIT        5
#define   CFA_RS_DESTROY              6
#define   CFA_RS_INVALID              0   /* proprietary = NO_SUCH_INSTANCE */

#define  CLI_PPP_AUTH_CREATE          2
#define  CLI_PPP_AUTH_DELETE          1

#define  CLI_PPP_SERVICE_CREATE       2
#define  CLI_PPP_SERVICE_DELETE       1
#define  CLI_PPP_SUCCESS              0
#define  CLI_PPP_FAILURE              1
#define  CLI_TRACE_ENABLE             1
#define  CLI_TRACE_DISABLE            0
#define  DEFAULT_PPP_NETMASK       0xff000000

enum {
    CLI_PPP_ADMIN_STAT_OPEN = 1,
    CLI_PPP_INVALID_IP,
    CLI_PPP_SERVICE_NAME_NOT_CONFIGURED,
    CLI_PPP_SERVICE_NAME_CONFIGURED,
    CLI_PPP_SERVICE_NAME_MEMALLOC,
    CLI_PPP_MAX_ERR
};

#ifdef  __PPPCLI_C__

CONST CHR1  *PppCliErrString [] = {
    NULL,
    "% Link must first be made administratively down before setting peer IP Address\r\n",
    "% Invalid IP Address !\r\n",
    "% Service name not configured !\r\n",
    "% Service name already configured !\r\n",
    "% MemAlloc for Service Name Failed !\r\n",
};

extern INT1 nmhGetIfIpAddr          PROTO((INT4 , UINT4 *));
extern INT1 nmhGetIfIpSubnetMask    PROTO((INT4 , UINT4 *));
extern INT1 nmhGetIfIpAddrAllocMethod PROTO((INT4 , INT4 *));
extern INT1 nmhGetIfIpUnnumAssocIPIf PROTO((INT4 , UINT4 *));
#endif

/* PPP COMMANDS */
enum
{
  CLI_PPP_ADSL_DYNAMIC_MODE = 0,
  CLI_PPP_CONNECT           ,
  CLI_PPP_DISCONNECT        ,
  CLI_PPP_ADDRESS_POOL      ,
  CLI_PPP_NO_ADDRESS_POOL   ,
  CLI_PPP_PPPOE_MODE        ,
  CLI_PPP_AUTH_SERVER       ,
  CLI_PPP_AUTH_MODE         ,
  PPP_SET_CLI_TRACE_LEVEL   ,
  PPP_RESET_CLI_TRACE_LEVEL ,
  PPP_CLI_SHOW_ADDRESS_POOL ,
  PPP_CLI_SHOW_PPPOE_STATISTICS,                                            
  PPP_CLI_SHOW_BINDINGS ,
  PPP_CLI_CLEAR_PPPOE_STATISTICS, 
  CLI_PPP_KEEP_ALIVE_TIMEOUT,
  CLI_PPP_PADR_MAX_RETRY,
  CLI_PPP_PEER_PARAMS_ENABLE,
  CLI_PPP_PEER_PARAMS_DISABLE,
  CLI_PPP_LINK_HOSTNAME     ,
  CLI_PPP_PAP_USRNAME_PWD   ,
  CLI_PPP_PAP_AUTH_USRNAME_PWD,
  CLI_PPP_ENCAPSULATION_VLANID,
  CLI_PPP_ENCAPSULATION_PRIORITY,
  CLI_PPP_ENCAPSULATION_CFI,
  CLI_PPP_L2TP_STATIC_MODE,
  CLI_PPPOE_SERVICE_NAME
};
/* End of PPP COMMANDS */

/*Trace Levels for ppp modules*/
#define ENABLE_ALL_TRC      0x000006ff
#define DISABLE_ALL_TRC     0x000006ff
/* END of Trace Levels for ppp modules*/

#define PPP_MAX_IP_POOL               128
#define MAX_USRN_LEN                  256
#define MAX_PASSW_LEN                 256

#define MAX_HOST_NAME_LEN      32
#define MAX_IDLE_TIME_LEN      8
#define MAX_SERVER_LEN         32
#define MAX_ALLOC_METHOD_LEN   16
#define MAX_BAUD_LEN           8
#define MAX_SETTINGS_LEN       256
#define MAX_TELNO_LEN          64
#define MAX_AUTH_PROTO_LEN     10
#define MAX_CLI_COMMAND_LEN    1000

#define PPP_CLI_MAX_COMMANDS_PARAMS   10
#define PPP_CLI_MODE                  "ppp"

#define PPP_AUTH_PAP  1
#define PPP_AUTH_CHAP 2

#define L2TP_ON_DEMAND  1
#define L2TP_MANUAL     2

typedef struct _ConfigPppoe
{
        UINT1 au1AddressAlloc[MAX_ALLOC_METHOD_LEN];
        UINT1 au1PPPUserName[MAX_USRN_LEN];
        UINT1 au1PPPPassword[MAX_PASSW_LEN];
        UINT1 au1Server[MAX_PASSW_LEN];
        UINT1 au1MaxIdleTimeout[MAX_IDLE_TIME_LEN];
        UINT4 u4AutoReconnect;
        UINT1 au1WanIpAddr[MAX_ADDR_LEN];
        UINT1 au1WanIpMask[MAX_ADDR_LEN];
        UINT1 au1Gateway[MAX_ADDR_LEN];
        UINT1 au1PriDns[MAX_ADDR_LEN];
        UINT1 au1SecDns[MAX_ADDR_LEN];
} tConfigPppoe;


INT1 PppGetPppConfigPrompt   PROTO ((INT1 *pi1ModeName,
                                           INT1 *pi1DispStr));
INT1 PppGetTestPppIntfPrompt PROTO ((INT1 *pi1ModeName,
                                           INT1 *pi1DispStr));
INT1
PppGetTestPppPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1DispStr));


INT4 cli_process_ppp_cmd     PROTO ((tCliHandle CliHandle,
                                     UINT4 u4Command, ...));
INT4
cli_set_l2tp_static_mode PROTO ((UINT4 u4ServerIpAddr, UINT1 *pu1UserName,
                          UINT1 *pu1Password, UINT4 u4AuthType,
                          UINT4 u4IdleTimeOut, UINT4 u4KeepAlive,
                          UINT4 u4ConnectType, UINT4 u4IpAddr,
                          UINT4 u4Mask, UINT4 u4Gateway,
                          UINT1 *pu1PriDns, UINT1 *pu1SecDns));

INT4 cli_connect    PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex));
INT4 cli_disconnect PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex));
INT4 cli_get_router_name PROTO ((UINT1 *pu1RouterName));

INT4
cli_set_access_method PROTO ((UINT1 u1AccessMethod));

INT4
CliPppCreateAddressPool PROTO ((tCliHandle CliHandle, UINT4 u4AddrPoolIndex,
                                UINT4 u4StartAddress, UINT4 u4EndAddress));
INT4
CliPppDeleteAddressPool PROTO ((tCliHandle CliHandle, UINT4 u4AddrPoolIndex));

INT4
CliPppSetAuthServer     PROTO ((tCliHandle CliHandle, INT4 i4AuthServer));

INT4
CliPppSetMode     PROTO ((tCliHandle CliHandle, INT4 i4Mode));

VOID
IpRouteAdd PROTO ((UINT4 u4FsIpRouteDest, UINT4 u4FsIpRouteMask, 
                   INT4 i4FsIpRouteTos,   UINT4 u4FsIpRouteNextHop, 
                   INT4 i4FsIpRoutePreference));
INT4
CreateEthernetInterfaceStatic PROTO ((UINT1 *pu1IpAddr, UINT1 *pu1Mask,
                                      UINT1 *pu1Gateway, UINT1 *pu1PriDns,
                                      UINT1 *pu1SecDns));
INT4
CreateL2tpPppInterface PROTO ((UINT1 *pu1UserName, UINT1 *pu1Password,
                        UINT4 u4AuthType, UINT4 u4IdleTimeOut,
                        UINT4 u4KeepAlive, UINT4 u4ConnectType,
                        INT4 *pi4PppIfIndex));

INT4 CliPppSetTraceLevel PROTO ((tCliHandle CliHandle, 
                                 UINT4 u4PppTraceLevel, UINT1 u1TraceFlag));

INT4 
CliPppShowPppLocalPools PROTO ((tCliHandle CliHandle));

INT4
CliPppShowPppoeStatistics PROTO ((tCliHandle CliHandle));

INT4
CliPppClearPppoeStatistics PROTO ((tCliHandle CliHandle));

INT4
ClearPPPoEStat(tCliHandle CliHandle, INT4 i4PPPoEBindingsIndex);

INT4
ShowClearStat(tCliHandle CliHandle,INT4 i4PPPoEBindingsIndex );


INT4 
CliPppShowPppBindings PROTO ((tCliHandle CliHandle));

INT4
PppCliHostNameCfg (tCliHandle CliHandle, INT4 i4PppIfaceNo,
                      UINT1 *pu1HostName);
INT4
PppCliPapChapCfg (tCliHandle CliHandle, INT4 i4PppIfaceNo,
                     UINT1 u1Direction, UINT1 *pu1UserName,
                     UINT1 *pu1Password, UINT1 u1Action);
INT4 CliPppSetAuthMode (tCliHandle CliHandle, INT4 i4AuthMode);
INT4 PppCliSetKeepAliveTimeout (tCliHandle, INT4, INT4);


INT4
PppCliSetEncapsulationVlanId (tCliHandle CliHandle,INT4 i4PppIfaceNo, INT4 i4VlanId);

INT4
PppCliSetEncapsulationPriority (tCliHandle CliHandle,INT4 i4PppIfaceNo, INT4 i4Priority);

INT4
PppCliSetEncapsulationCfi (tCliHandle CliHandle,INT4 i4PppIfaceNo, INT4 i4Cfi);


INT4 PppCliSetPeerIpAddress (tCliHandle CliHandle, UINT4 u4IfIndex,
                             UINT4 u4IpAddr); 
INT4 PppCliSetPeerPrimaryDnsAddress (tCliHandle CliHandle, UINT4 u4IfIndex,
                                     UINT4 u4PrimaryDnsIpAddr);
INT4 PppCliSetPeerSecondaryDnsAddress (tCliHandle CliHandle, UINT4 u4IfIndex,
                                       UINT4 u4SecondaryDnsIpAddr);
VOID IssPPPShowDebugging PROTO ((tCliHandle CliHandle));

INT4 PppShowRunningConfig PROTO ((tCliHandle CliHandle, UINT4 u4Module));
VOID PppShowRunningConfigIntf PROTO ((tCliHandle CliHandle, INT4 i4IfIndex));
VOID PppLcpShowRunningConfig PROTO ((tCliHandle CliHandle, INT4 i4IfIndex));
VOID PppNcpShowRunningConfig PROTO ((tCliHandle CliHandle, INT4 i4IfIndex));
VOID PppSecurityShowRunningConfig PROTO ((tCliHandle CliHandle, INT4 i4IfIndex));
INT4 PppCliSetPadrRetryCount (tCliHandle CliHandle,INT4 u4MaxRetryVal);
INT4 PppCliServiceNameCfg (tCliHandle CliHandle, UINT1 *pu1ServiceName, UINT4 u4Action);

#endif  /* __PPPCLI_H__ */
