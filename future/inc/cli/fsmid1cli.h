/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmid1cli.h,v 1.3 2016/07/16 11:15:02 siva Exp $
*
* Description: Header file for nmhSet STD EVB mib  
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIEvbSystemContextId[14];
extern UINT4 FsMIEvbSystemControl[14];
extern UINT4 FsMIEvbSystemModuleStatus[14];
extern UINT4 FsMIEvbSystemTraceLevel[14];
extern UINT4 FsMIEvbSystemTrapStatus[14];
extern UINT4 FsMIEvbSystemStatsClear[14];
extern UINT4 FsMIEvbSchannelIdMode[14];
extern UINT4 FsMIEvbSystemRowStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMIEvbSystemControl(i4FsMIEvbSystemContextId ,i4SetValFsMIEvbSystemControl) \
 nmhSetCmn(FsMIEvbSystemControl, 14, FsMIEvbSystemControlSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %i", i4FsMIEvbSystemContextId ,i4SetValFsMIEvbSystemControl)
#define nmhSetFsMIEvbSystemModuleStatus(i4FsMIEvbSystemContextId ,i4SetValFsMIEvbSystemModuleStatus) \
 nmhSetCmn(FsMIEvbSystemModuleStatus, 14, FsMIEvbSystemModuleStatusSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %i", i4FsMIEvbSystemContextId ,i4SetValFsMIEvbSystemModuleStatus)
#define nmhSetFsMIEvbSystemTraceLevel(i4FsMIEvbSystemContextId ,u4SetValFsMIEvbSystemTraceLevel) \
 nmhSetCmn(FsMIEvbSystemTraceLevel, 14, FsMIEvbSystemTraceLevelSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %u", i4FsMIEvbSystemContextId ,u4SetValFsMIEvbSystemTraceLevel)
#define nmhSetFsMIEvbSystemTrapStatus(i4FsMIEvbSystemContextId ,i4SetValFsMIEvbSystemTrapStatus) \
 nmhSetCmn(FsMIEvbSystemTrapStatus, 14, FsMIEvbSystemTrapStatusSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %i", i4FsMIEvbSystemContextId ,i4SetValFsMIEvbSystemTrapStatus)
#define nmhSetFsMIEvbSystemStatsClear(i4FsMIEvbSystemContextId ,i4SetValFsMIEvbSystemStatsClear) \
 nmhSetCmn(FsMIEvbSystemStatsClear, 14, FsMIEvbSystemStatsClearSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %i", i4FsMIEvbSystemContextId ,i4SetValFsMIEvbSystemStatsClear)
#define nmhSetFsMIEvbSchannelIdMode(i4FsMIEvbSystemContextId ,i4SetValFsMIEvbSchannelIdMode) \
 nmhSetCmn(FsMIEvbSchannelIdMode, 14, FsMIEvbSchannelIdModeSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %i", i4FsMIEvbSystemContextId ,i4SetValFsMIEvbSchannelIdMode)
#define nmhSetFsMIEvbSystemRowStatus(i4FsMIEvbSystemContextId ,i4SetValFsMIEvbSystemRowStatus) \
 nmhSetCmn(FsMIEvbSystemRowStatus, 14, FsMIEvbSystemRowStatusSet, VlanLock, VlanUnLock, 0, 1, 1, "%i %i", i4FsMIEvbSystemContextId ,i4SetValFsMIEvbSystemRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIEvbCAPSChannelID[14];
extern UINT4 FsMIEvbCAPSChannelIfIndex[14];
extern UINT4 FsMIEvbSChannelFilterStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMIEvbCAPSChannelID(u4Ieee8021BridgePhyPort , u4Ieee8021BridgeEvbSchID ,u4SetValFsMIEvbCAPSChannelID) \
 nmhSetCmn(FsMIEvbCAPSChannelID, 14, FsMIEvbCAPSChannelIDSet, VlanLock, VlanUnLock, 0, 0, 2, "%u %u %u", u4Ieee8021BridgePhyPort , u4Ieee8021BridgeEvbSchID ,u4SetValFsMIEvbCAPSChannelID)
#define nmhSetFsMIEvbCAPSChannelIfIndex(u4Ieee8021BridgePhyPort , u4Ieee8021BridgeEvbSchID ,i4SetValFsMIEvbCAPSChannelIfIndex) \
 nmhSetCmn(FsMIEvbCAPSChannelIfIndex, 14, FsMIEvbCAPSChannelIfIndexSet, VlanLock, VlanUnLock, 0, 0, 2, "%u %u %i", u4Ieee8021BridgePhyPort , u4Ieee8021BridgeEvbSchID ,i4SetValFsMIEvbCAPSChannelIfIndex)
#define nmhSetFsMIEvbSChannelFilterStatus(u4Ieee8021BridgePhyPort , u4Ieee8021BridgeEvbSchID ,i4SetValFsMIEvbSChannelFilterStatus) \
 nmhSetCmn(FsMIEvbSChannelFilterStatus, 14, FsMIEvbSChannelFilterStatusSet, VlanLock, VlanUnLock, 0, 0, 2, "%u %u %i", u4Ieee8021BridgePhyPort , u4Ieee8021BridgeEvbSchID ,i4SetValFsMIEvbSChannelFilterStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIEvbUAPSchCdcpMode[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMIEvbUAPSchCdcpMode(u4Ieee8021BridgePhyPort ,i4SetValFsMIEvbUAPSchCdcpMode) \
 nmhSetCmn(FsMIEvbUAPSchCdcpMode, 14, FsMIEvbUAPSchCdcpModeSet, VlanLock, VlanUnLock, 0, 0, 1, "%u %i", u4Ieee8021BridgePhyPort ,i4SetValFsMIEvbUAPSchCdcpMode)

#endif
