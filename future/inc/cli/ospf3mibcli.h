/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: ospf3mibcli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ospfv3RouterId[9];
extern UINT4 Ospfv3AdminStat[9];
extern UINT4 Ospfv3ASBdrRtrStatus[9];
extern UINT4 Ospfv3ExtAreaLsdbLimit[9];
extern UINT4 Ospfv3MulticastExtensions[9];
extern UINT4 Ospfv3ExitOverflowInterval[9];
extern UINT4 Ospfv3DemandExtensions[9];
extern UINT4 Ospfv3TrafficEngineeringSupport[9];
extern UINT4 Ospfv3ReferenceBandwidth[9];
extern UINT4 Ospfv3RestartSupport[9];
extern UINT4 Ospfv3RestartInterval[9];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetOspfv3RouterId(u4SetValOspfv3RouterId)	\
	nmhSetCmn(Ospfv3RouterId, 9, Ospfv3RouterIdSet, V3OspfLock, V3OspfUnLock, 0, 0, 0, "%p", u4SetValOspfv3RouterId)
#define nmhSetOspfv3AdminStat(i4SetValOspfv3AdminStat)	\
	nmhSetCmn(Ospfv3AdminStat, 9, Ospfv3AdminStatSet, V3OspfLock, V3OspfUnLock, 0, 0, 0, "%i", i4SetValOspfv3AdminStat)
#define nmhSetOspfv3ASBdrRtrStatus(i4SetValOspfv3ASBdrRtrStatus)	\
	nmhSetCmn(Ospfv3ASBdrRtrStatus, 9, Ospfv3ASBdrRtrStatusSet, V3OspfLock, V3OspfUnLock, 0, 0, 0, "%i", i4SetValOspfv3ASBdrRtrStatus)
#define nmhSetOspfv3ExtAreaLsdbLimit(i4SetValOspfv3ExtAreaLsdbLimit)	\
	nmhSetCmn(Ospfv3ExtAreaLsdbLimit, 9, Ospfv3ExtAreaLsdbLimitSet, V3OspfLock, V3OspfUnLock, 0, 0, 0, "%i", i4SetValOspfv3ExtAreaLsdbLimit)
#define nmhSetOspfv3MulticastExtensions(pSetValOspfv3MulticastExtensions)	\
	nmhSetCmn(Ospfv3MulticastExtensions, 9, Ospfv3MulticastExtensionsSet, V3OspfLock, V3OspfUnLock, 0, 0, 0, "%s", pSetValOspfv3MulticastExtensions)
#define nmhSetOspfv3ExitOverflowInterval(u4SetValOspfv3ExitOverflowInterval)	\
	nmhSetCmn(Ospfv3ExitOverflowInterval, 9, Ospfv3ExitOverflowIntervalSet, V3OspfLock, V3OspfUnLock, 0, 0, 0, "%u", u4SetValOspfv3ExitOverflowInterval)
#define nmhSetOspfv3DemandExtensions(i4SetValOspfv3DemandExtensions)	\
	nmhSetCmn(Ospfv3DemandExtensions, 9, Ospfv3DemandExtensionsSet, V3OspfLock, V3OspfUnLock, 0, 0, 0, "%i", i4SetValOspfv3DemandExtensions)
#define nmhSetOspfv3TrafficEngineeringSupport(i4SetValOspfv3TrafficEngineeringSupport)	\
	nmhSetCmn(Ospfv3TrafficEngineeringSupport, 9, Ospfv3TrafficEngineeringSupportSet, V3OspfLock, V3OspfUnLock, 0, 0, 0, "%i", i4SetValOspfv3TrafficEngineeringSupport)
#define nmhSetOspfv3ReferenceBandwidth(u4SetValOspfv3ReferenceBandwidth)	\
	nmhSetCmn(Ospfv3ReferenceBandwidth, 9, Ospfv3ReferenceBandwidthSet, V3OspfLock, V3OspfUnLock, 0, 0, 0, "%u", u4SetValOspfv3ReferenceBandwidth)
#define nmhSetOspfv3RestartSupport(i4SetValOspfv3RestartSupport)	\
	nmhSetCmn(Ospfv3RestartSupport, 9, Ospfv3RestartSupportSet, V3OspfLock, V3OspfUnLock, 0, 0, 0, "%i", i4SetValOspfv3RestartSupport)
#define nmhSetOspfv3RestartInterval(i4SetValOspfv3RestartInterval)	\
	nmhSetCmn(Ospfv3RestartInterval, 9, Ospfv3RestartIntervalSet, V3OspfLock, V3OspfUnLock, 0, 0, 0, "%i", i4SetValOspfv3RestartInterval)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ospfv3AreaId[10];
extern UINT4 Ospfv3ImportAsExtern[10];
extern UINT4 Ospfv3AreaSummary[10];
extern UINT4 Ospfv3AreaStatus[10];
extern UINT4 Ospfv3StubMetric[10];
extern UINT4 Ospfv3AreaNssaTranslatorRole[10];
extern UINT4 Ospfv3AreaNssaTranslatorStabilityInterval[10];
extern UINT4 Ospfv3AreaStubMetricType[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetOspfv3ImportAsExtern(u4Ospfv3AreaId ,i4SetValOspfv3ImportAsExtern)	\
	nmhSetCmn(Ospfv3ImportAsExtern, 10, Ospfv3ImportAsExternSet, V3OspfLock, V3OspfUnLock, 0, 0, 1, "%p %i", u4Ospfv3AreaId ,i4SetValOspfv3ImportAsExtern)
#define nmhSetOspfv3AreaSummary(u4Ospfv3AreaId ,i4SetValOspfv3AreaSummary)	\
	nmhSetCmn(Ospfv3AreaSummary, 10, Ospfv3AreaSummarySet, V3OspfLock, V3OspfUnLock, 0, 0, 1, "%p %i", u4Ospfv3AreaId ,i4SetValOspfv3AreaSummary)
#define nmhSetOspfv3AreaStatus(u4Ospfv3AreaId ,i4SetValOspfv3AreaStatus)	\
	nmhSetCmn(Ospfv3AreaStatus, 10, Ospfv3AreaStatusSet, V3OspfLock, V3OspfUnLock, 0, 1, 1, "%p %i", u4Ospfv3AreaId ,i4SetValOspfv3AreaStatus)
#define nmhSetOspfv3StubMetric(u4Ospfv3AreaId ,i4SetValOspfv3StubMetric)	\
	nmhSetCmn(Ospfv3StubMetric, 10, Ospfv3StubMetricSet, V3OspfLock, V3OspfUnLock, 0, 0, 1, "%p %i", u4Ospfv3AreaId ,i4SetValOspfv3StubMetric)
#define nmhSetOspfv3AreaNssaTranslatorRole(u4Ospfv3AreaId ,i4SetValOspfv3AreaNssaTranslatorRole)	\
	nmhSetCmn(Ospfv3AreaNssaTranslatorRole, 10, Ospfv3AreaNssaTranslatorRoleSet, V3OspfLock, V3OspfUnLock, 0, 0, 1, "%p %i", u4Ospfv3AreaId ,i4SetValOspfv3AreaNssaTranslatorRole)
#define nmhSetOspfv3AreaNssaTranslatorStabilityInterval(u4Ospfv3AreaId ,u4SetValOspfv3AreaNssaTranslatorStabilityInterval)	\
	nmhSetCmn(Ospfv3AreaNssaTranslatorStabilityInterval, 10, Ospfv3AreaNssaTranslatorStabilityIntervalSet, V3OspfLock, V3OspfUnLock, 0, 0, 1, "%p %u", u4Ospfv3AreaId ,u4SetValOspfv3AreaNssaTranslatorStabilityInterval)
#define nmhSetOspfv3AreaStubMetricType(u4Ospfv3AreaId ,i4SetValOspfv3AreaStubMetricType)	\
	nmhSetCmn(Ospfv3AreaStubMetricType, 10, Ospfv3AreaStubMetricTypeSet, V3OspfLock, V3OspfUnLock, 0, 0, 1, "%p %i", u4Ospfv3AreaId ,i4SetValOspfv3AreaStubMetricType)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ospfv3HostAddressType[10];
extern UINT4 Ospfv3HostAddress[10];
extern UINT4 Ospfv3HostMetric[10];
extern UINT4 Ospfv3HostStatus[10];
extern UINT4 Ospfv3HostAreaID[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetOspfv3HostMetric(i4Ospfv3HostAddressType , pOspfv3HostAddress ,i4SetValOspfv3HostMetric)	\
	nmhSetCmn(Ospfv3HostMetric, 10, Ospfv3HostMetricSet, V3OspfLock, V3OspfUnLock, 0, 0, 2, "%i %s %i", i4Ospfv3HostAddressType , pOspfv3HostAddress ,i4SetValOspfv3HostMetric)
#define nmhSetOspfv3HostStatus(i4Ospfv3HostAddressType , pOspfv3HostAddress ,i4SetValOspfv3HostStatus)	\
	nmhSetCmn(Ospfv3HostStatus, 10, Ospfv3HostStatusSet, V3OspfLock, V3OspfUnLock, 0, 1, 2, "%i %s %i", i4Ospfv3HostAddressType , pOspfv3HostAddress ,i4SetValOspfv3HostStatus)
#define nmhSetOspfv3HostAreaID(i4Ospfv3HostAddressType , pOspfv3HostAddress ,u4SetValOspfv3HostAreaID)	\
	nmhSetCmn(Ospfv3HostAreaID, 10, Ospfv3HostAreaIDSet, V3OspfLock, V3OspfUnLock, 0, 0, 2, "%i %s %p", i4Ospfv3HostAddressType , pOspfv3HostAddress ,u4SetValOspfv3HostAreaID)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ospfv3IfIndex[10];
extern UINT4 Ospfv3IfAreaId[10];
extern UINT4 Ospfv3IfType[10];
extern UINT4 Ospfv3IfAdminStat[10];
extern UINT4 Ospfv3IfRtrPriority[10];
extern UINT4 Ospfv3IfTransitDelay[10];
extern UINT4 Ospfv3IfRetransInterval[10];
extern UINT4 Ospfv3IfHelloInterval[10];
extern UINT4 Ospfv3IfRtrDeadInterval[10];
extern UINT4 Ospfv3IfPollInterval[10];
extern UINT4 Ospfv3IfStatus[10];
extern UINT4 Ospfv3IfMulticastForwarding[10];
extern UINT4 Ospfv3IfDemand[10];
extern UINT4 Ospfv3IfMetricValue[10];
extern UINT4 Ospfv3IfInstId[10];
extern UINT4 Ospfv3IfDemandNbrProbe[10];
extern UINT4 Ospfv3IfDemandNbrProbeRetxLimit[10];
extern UINT4 Ospfv3IfDemandNbrProbeInterval[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetOspfv3IfAreaId(i4Ospfv3IfIndex ,u4SetValOspfv3IfAreaId)	\
	nmhSetCmn(Ospfv3IfAreaId, 10, Ospfv3IfAreaIdSet, V3OspfLock, V3OspfUnLock, 0, 0, 1, "%i %p", i4Ospfv3IfIndex ,u4SetValOspfv3IfAreaId)
#define nmhSetOspfv3IfType(i4Ospfv3IfIndex ,i4SetValOspfv3IfType)	\
	nmhSetCmn(Ospfv3IfType, 10, Ospfv3IfTypeSet, V3OspfLock, V3OspfUnLock, 0, 0, 1, "%i %i", i4Ospfv3IfIndex ,i4SetValOspfv3IfType)
#define nmhSetOspfv3IfAdminStat(i4Ospfv3IfIndex ,i4SetValOspfv3IfAdminStat)	\
	nmhSetCmn(Ospfv3IfAdminStat, 10, Ospfv3IfAdminStatSet, V3OspfLock, V3OspfUnLock, 0, 0, 1, "%i %i", i4Ospfv3IfIndex ,i4SetValOspfv3IfAdminStat)
#define nmhSetOspfv3IfRtrPriority(i4Ospfv3IfIndex ,i4SetValOspfv3IfRtrPriority)	\
	nmhSetCmn(Ospfv3IfRtrPriority, 10, Ospfv3IfRtrPrioritySet, V3OspfLock, V3OspfUnLock, 0, 0, 1, "%i %i", i4Ospfv3IfIndex ,i4SetValOspfv3IfRtrPriority)
#define nmhSetOspfv3IfTransitDelay(i4Ospfv3IfIndex ,i4SetValOspfv3IfTransitDelay)	\
	nmhSetCmn(Ospfv3IfTransitDelay, 10, Ospfv3IfTransitDelaySet, V3OspfLock, V3OspfUnLock, 0, 0, 1, "%i %i", i4Ospfv3IfIndex ,i4SetValOspfv3IfTransitDelay)
#define nmhSetOspfv3IfRetransInterval(i4Ospfv3IfIndex ,i4SetValOspfv3IfRetransInterval)	\
	nmhSetCmn(Ospfv3IfRetransInterval, 10, Ospfv3IfRetransIntervalSet, V3OspfLock, V3OspfUnLock, 0, 0, 1, "%i %i", i4Ospfv3IfIndex ,i4SetValOspfv3IfRetransInterval)
#define nmhSetOspfv3IfHelloInterval(i4Ospfv3IfIndex ,i4SetValOspfv3IfHelloInterval)	\
	nmhSetCmn(Ospfv3IfHelloInterval, 10, Ospfv3IfHelloIntervalSet, V3OspfLock, V3OspfUnLock, 0, 0, 1, "%i %i", i4Ospfv3IfIndex ,i4SetValOspfv3IfHelloInterval)
#define nmhSetOspfv3IfRtrDeadInterval(i4Ospfv3IfIndex ,i4SetValOspfv3IfRtrDeadInterval)	\
	nmhSetCmn(Ospfv3IfRtrDeadInterval, 10, Ospfv3IfRtrDeadIntervalSet, V3OspfLock, V3OspfUnLock, 0, 0, 1, "%i %i", i4Ospfv3IfIndex ,i4SetValOspfv3IfRtrDeadInterval)
#define nmhSetOspfv3IfPollInterval(i4Ospfv3IfIndex ,u4SetValOspfv3IfPollInterval)	\
	nmhSetCmn(Ospfv3IfPollInterval, 10, Ospfv3IfPollIntervalSet, V3OspfLock, V3OspfUnLock, 0, 0, 1, "%i %u", i4Ospfv3IfIndex ,u4SetValOspfv3IfPollInterval)
#define nmhSetOspfv3IfStatus(i4Ospfv3IfIndex ,i4SetValOspfv3IfStatus)	\
	nmhSetCmn(Ospfv3IfStatus, 10, Ospfv3IfStatusSet, V3OspfLock, V3OspfUnLock, 0, 1, 1, "%i %i", i4Ospfv3IfIndex ,i4SetValOspfv3IfStatus)
#define nmhSetOspfv3IfMulticastForwarding(i4Ospfv3IfIndex ,i4SetValOspfv3IfMulticastForwarding)	\
	nmhSetCmn(Ospfv3IfMulticastForwarding, 10, Ospfv3IfMulticastForwardingSet, V3OspfLock, V3OspfUnLock, 0, 0, 1, "%i %i", i4Ospfv3IfIndex ,i4SetValOspfv3IfMulticastForwarding)
#define nmhSetOspfv3IfDemand(i4Ospfv3IfIndex ,i4SetValOspfv3IfDemand)	\
	nmhSetCmn(Ospfv3IfDemand, 10, Ospfv3IfDemandSet, V3OspfLock, V3OspfUnLock, 0, 0, 1, "%i %i", i4Ospfv3IfIndex ,i4SetValOspfv3IfDemand)
#define nmhSetOspfv3IfMetricValue(i4Ospfv3IfIndex ,i4SetValOspfv3IfMetricValue)	\
	nmhSetCmn(Ospfv3IfMetricValue, 10, Ospfv3IfMetricValueSet, V3OspfLock, V3OspfUnLock, 0, 0, 1, "%i %i", i4Ospfv3IfIndex ,i4SetValOspfv3IfMetricValue)
#define nmhSetOspfv3IfInstId(i4Ospfv3IfIndex ,i4SetValOspfv3IfInstId)	\
	nmhSetCmn(Ospfv3IfInstId, 10, Ospfv3IfInstIdSet, V3OspfLock, V3OspfUnLock, 0, 0, 1, "%i %i", i4Ospfv3IfIndex ,i4SetValOspfv3IfInstId)
#define nmhSetOspfv3IfDemandNbrProbe(i4Ospfv3IfIndex ,i4SetValOspfv3IfDemandNbrProbe)	\
	nmhSetCmn(Ospfv3IfDemandNbrProbe, 10, Ospfv3IfDemandNbrProbeSet, V3OspfLock, V3OspfUnLock, 0, 0, 1, "%i %i", i4Ospfv3IfIndex ,i4SetValOspfv3IfDemandNbrProbe)
#define nmhSetOspfv3IfDemandNbrProbeRetxLimit(i4Ospfv3IfIndex ,u4SetValOspfv3IfDemandNbrProbeRetxLimit)	\
	nmhSetCmn(Ospfv3IfDemandNbrProbeRetxLimit, 10, Ospfv3IfDemandNbrProbeRetxLimitSet, V3OspfLock, V3OspfUnLock, 0, 0, 1, "%i %u", i4Ospfv3IfIndex ,u4SetValOspfv3IfDemandNbrProbeRetxLimit)
#define nmhSetOspfv3IfDemandNbrProbeInterval(i4Ospfv3IfIndex ,u4SetValOspfv3IfDemandNbrProbeInterval)	\
	nmhSetCmn(Ospfv3IfDemandNbrProbeInterval, 10, Ospfv3IfDemandNbrProbeIntervalSet, V3OspfLock, V3OspfUnLock, 0, 0, 1, "%i %u", i4Ospfv3IfIndex ,u4SetValOspfv3IfDemandNbrProbeInterval)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ospfv3VirtIfAreaId[10];
extern UINT4 Ospfv3VirtIfNeighbor[10];
extern UINT4 Ospfv3VirtIfIndex[10];
extern UINT4 Ospfv3VirtIfTransitDelay[10];
extern UINT4 Ospfv3VirtIfRetransInterval[10];
extern UINT4 Ospfv3VirtIfHelloInterval[10];
extern UINT4 Ospfv3VirtIfRtrDeadInterval[10];
extern UINT4 Ospfv3VirtIfStatus[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetOspfv3VirtIfIndex(u4Ospfv3VirtIfAreaId , u4Ospfv3VirtIfNeighbor ,i4SetValOspfv3VirtIfIndex)	\
	nmhSetCmn(Ospfv3VirtIfIndex, 10, Ospfv3VirtIfIndexSet, V3OspfLock, V3OspfUnLock, 0, 0, 2, "%p %p %i", u4Ospfv3VirtIfAreaId , u4Ospfv3VirtIfNeighbor ,i4SetValOspfv3VirtIfIndex)
#define nmhSetOspfv3VirtIfTransitDelay(u4Ospfv3VirtIfAreaId , u4Ospfv3VirtIfNeighbor ,i4SetValOspfv3VirtIfTransitDelay)	\
	nmhSetCmn(Ospfv3VirtIfTransitDelay, 10, Ospfv3VirtIfTransitDelaySet, V3OspfLock, V3OspfUnLock, 0, 0, 2, "%p %p %i", u4Ospfv3VirtIfAreaId , u4Ospfv3VirtIfNeighbor ,i4SetValOspfv3VirtIfTransitDelay)
#define nmhSetOspfv3VirtIfRetransInterval(u4Ospfv3VirtIfAreaId , u4Ospfv3VirtIfNeighbor ,i4SetValOspfv3VirtIfRetransInterval)	\
	nmhSetCmn(Ospfv3VirtIfRetransInterval, 10, Ospfv3VirtIfRetransIntervalSet, V3OspfLock, V3OspfUnLock, 0, 0, 2, "%p %p %i", u4Ospfv3VirtIfAreaId , u4Ospfv3VirtIfNeighbor ,i4SetValOspfv3VirtIfRetransInterval)
#define nmhSetOspfv3VirtIfHelloInterval(u4Ospfv3VirtIfAreaId , u4Ospfv3VirtIfNeighbor ,i4SetValOspfv3VirtIfHelloInterval)	\
	nmhSetCmn(Ospfv3VirtIfHelloInterval, 10, Ospfv3VirtIfHelloIntervalSet, V3OspfLock, V3OspfUnLock, 0, 0, 2, "%p %p %i", u4Ospfv3VirtIfAreaId , u4Ospfv3VirtIfNeighbor ,i4SetValOspfv3VirtIfHelloInterval)
#define nmhSetOspfv3VirtIfRtrDeadInterval(u4Ospfv3VirtIfAreaId , u4Ospfv3VirtIfNeighbor ,i4SetValOspfv3VirtIfRtrDeadInterval)	\
	nmhSetCmn(Ospfv3VirtIfRtrDeadInterval, 10, Ospfv3VirtIfRtrDeadIntervalSet, V3OspfLock, V3OspfUnLock, 0, 0, 2, "%p %p %i", u4Ospfv3VirtIfAreaId , u4Ospfv3VirtIfNeighbor ,i4SetValOspfv3VirtIfRtrDeadInterval)
#define nmhSetOspfv3VirtIfStatus(u4Ospfv3VirtIfAreaId , u4Ospfv3VirtIfNeighbor ,i4SetValOspfv3VirtIfStatus)	\
	nmhSetCmn(Ospfv3VirtIfStatus, 10, Ospfv3VirtIfStatusSet, V3OspfLock, V3OspfUnLock, 0, 1, 2, "%p %p %i", u4Ospfv3VirtIfAreaId , u4Ospfv3VirtIfNeighbor ,i4SetValOspfv3VirtIfStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ospfv3NbmaNbrIfIndex[10];
extern UINT4 Ospfv3NbmaNbrAddressType[10];
extern UINT4 Ospfv3NbmaNbrAddress[10];
extern UINT4 Ospfv3NbmaNbrPriority[10];
extern UINT4 Ospfv3NbmaNbrStorageType[10];
extern UINT4 Ospfv3NbmaNbrStatus[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetOspfv3NbmaNbrPriority(i4Ospfv3NbmaNbrIfIndex , i4Ospfv3NbmaNbrAddressType , pOspfv3NbmaNbrAddress ,i4SetValOspfv3NbmaNbrPriority)	\
	nmhSetCmn(Ospfv3NbmaNbrPriority, 10, Ospfv3NbmaNbrPrioritySet, V3OspfLock, V3OspfUnLock, 0, 0, 3, "%i %i %s %i", i4Ospfv3NbmaNbrIfIndex , i4Ospfv3NbmaNbrAddressType , pOspfv3NbmaNbrAddress ,i4SetValOspfv3NbmaNbrPriority)
#define nmhSetOspfv3NbmaNbrStorageType(i4Ospfv3NbmaNbrIfIndex , i4Ospfv3NbmaNbrAddressType , pOspfv3NbmaNbrAddress ,i4SetValOspfv3NbmaNbrStorageType)	\
	nmhSetCmn(Ospfv3NbmaNbrStorageType, 10, Ospfv3NbmaNbrStorageTypeSet, V3OspfLock, V3OspfUnLock, 0, 0, 3, "%i %i %s %i", i4Ospfv3NbmaNbrIfIndex , i4Ospfv3NbmaNbrAddressType , pOspfv3NbmaNbrAddress ,i4SetValOspfv3NbmaNbrStorageType)
#define nmhSetOspfv3NbmaNbrStatus(i4Ospfv3NbmaNbrIfIndex , i4Ospfv3NbmaNbrAddressType , pOspfv3NbmaNbrAddress ,i4SetValOspfv3NbmaNbrStatus)	\
	nmhSetCmn(Ospfv3NbmaNbrStatus, 10, Ospfv3NbmaNbrStatusSet, V3OspfLock, V3OspfUnLock, 0, 1, 3, "%i %i %s %i", i4Ospfv3NbmaNbrIfIndex , i4Ospfv3NbmaNbrAddressType , pOspfv3NbmaNbrAddress ,i4SetValOspfv3NbmaNbrStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ospfv3AreaAggregateAreaID[10];
extern UINT4 Ospfv3AreaAggregateAreaLsdbType[10];
extern UINT4 Ospfv3AreaAggregatePrefixType[10];
extern UINT4 Ospfv3AreaAggregatePrefix[10];
extern UINT4 Ospfv3AreaAggregatePrefixLength[10];
extern UINT4 Ospfv3AreaAggregateStatus[10];
extern UINT4 Ospfv3AreaAggregateEffect[10];
extern UINT4 Ospfv3AreaAggregateRouteTag[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetOspfv3AreaAggregateStatus(u4Ospfv3AreaAggregateAreaID , i4Ospfv3AreaAggregateAreaLsdbType , i4Ospfv3AreaAggregatePrefixType , pOspfv3AreaAggregatePrefix , u4Ospfv3AreaAggregatePrefixLength ,i4SetValOspfv3AreaAggregateStatus)	\
	nmhSetCmn(Ospfv3AreaAggregateStatus, 10, Ospfv3AreaAggregateStatusSet, V3OspfLock, V3OspfUnLock, 0, 1, 5, "%p %i %i %s %u %i", u4Ospfv3AreaAggregateAreaID , i4Ospfv3AreaAggregateAreaLsdbType , i4Ospfv3AreaAggregatePrefixType , pOspfv3AreaAggregatePrefix , u4Ospfv3AreaAggregatePrefixLength ,i4SetValOspfv3AreaAggregateStatus)
#define nmhSetOspfv3AreaAggregateEffect(u4Ospfv3AreaAggregateAreaID , i4Ospfv3AreaAggregateAreaLsdbType , i4Ospfv3AreaAggregatePrefixType , pOspfv3AreaAggregatePrefix , u4Ospfv3AreaAggregatePrefixLength ,i4SetValOspfv3AreaAggregateEffect)	\
	nmhSetCmn(Ospfv3AreaAggregateEffect, 10, Ospfv3AreaAggregateEffectSet, V3OspfLock, V3OspfUnLock, 0, 0, 5, "%p %i %i %s %u %i", u4Ospfv3AreaAggregateAreaID , i4Ospfv3AreaAggregateAreaLsdbType , i4Ospfv3AreaAggregatePrefixType , pOspfv3AreaAggregatePrefix , u4Ospfv3AreaAggregatePrefixLength ,i4SetValOspfv3AreaAggregateEffect)
#define nmhSetOspfv3AreaAggregateRouteTag(u4Ospfv3AreaAggregateAreaID , i4Ospfv3AreaAggregateAreaLsdbType , i4Ospfv3AreaAggregatePrefixType , pOspfv3AreaAggregatePrefix , u4Ospfv3AreaAggregatePrefixLength ,i4SetValOspfv3AreaAggregateRouteTag)	\
	nmhSetCmn(Ospfv3AreaAggregateRouteTag, 10, Ospfv3AreaAggregateRouteTagSet, V3OspfLock, V3OspfUnLock, 0, 0, 5, "%p %i %i %s %u %i", u4Ospfv3AreaAggregateAreaID , i4Ospfv3AreaAggregateAreaLsdbType , i4Ospfv3AreaAggregatePrefixType , pOspfv3AreaAggregatePrefix , u4Ospfv3AreaAggregatePrefixLength ,i4SetValOspfv3AreaAggregateRouteTag)

#endif
