/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: syncecli.h,v 1.8 2016/07/06 11:05:09 siva Exp $
*
* Description: This file contains the Synce CLI related prototypes,
* enum and macros
*********************************************************************/

#ifndef __SYNCE_CLI_H__
#define __SYNCE_CLI_H__

#include "cli.h"

INT4 cli_process_Synce_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);

#ifdef SYNCE_UT_WANTED
INT4 cli_process_synce_test_cmd(tCliHandle CliHandle,...);
#endif

INT4 cli_process_Synce_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);

VOID
SynceCliShowInterfaceInfo (tCliHandle CliHandle,
                           UINT4 u4IfIndex);
VOID
SynceCliShowContextInfo (tCliHandle CliHandle,
                         UINT4 u4ContextId);
VOID 
IssSynceShowDebugging (tCliHandle CliHandle);

enum
{
    CLI_SYNCE_FSSYNCETABLE,
    CLI_SYNCE_FSSYNCEIFTABLE,
    CLI_SYNCE_FSSYNCEGLOBALSYSCTRL,
    CLI_SYNCE_FSSYNCEGLOBALTABLE,
    CLI_SYNCE_FSSYNCETABLE_DEBUG
};

enum
{
    CLI_SYNCE_SHOW,
    CLI_SYNCE_SHOW_INTERFACE,
    CLI_SYNCE_SHOW_ESMC_RX,
    CLI_SYNCE_SHOW_ESMC_TX,
    CLI_SYNCE_CLEAR_CNTR
};

enum
{
    CLI_SYNCE_ERR_UNDEFINED,
    CLI_SYNCE_ERR_MODULE_DISABLED,
    CLI_SYNCE_ERR_INVALID_CONTEXT,
    CLI_SYNCE_ERR_INVALID_INTERFACE,
    CLI_SYNCE_ERR_INVALID_QL_CODE,
    CLI_SYNCE_ERR_SYNCE_MODE_DISABLED,
    CLI_SYNCE_ERR_SYNCE_DIS_ON_CTX,
    CLI_SYNCE_ERR_MAX_NUM_INTERFACE,
    CLI_SYNCE_ERR_SSM_MODE_NOT_CHANGED,
    CLI_SYNCE_ERR_INVALID_QL,
    CLI_SYNCE_ERR_QL_NOT_SUPPORTED,
    CLI_SYNCE_ERR_MS_QL_VALUE_LOW,
    CLI_SYNCE_ERR_MS_PRIORITY_ZERO,
    CLI_SYNCE_ERR_SW_IF_LOCKOUT,
    CLI_SYNCE_ERR_SW_ACTIVE_ON,
    CLI_SYNCE_ERR_SW_IF_SIGNAL_FAIL,
    CLI_SYNCE_ERR_ESMC_RX_NOT_SUPPORTED,
    CLI_SYNCE_ERR_INVALID_LOCK_CLEAR,
    CLI_SYNCE_ERR_INVALID_SWITCH_CLEAR,
    CLI_SYNCE_ERR_FORCE_SWITCH_SET,
    CLI_SYNCE_INVALID_SW_INPUT,
    CLI_SYNCE_ERR_INT_TX_MODE,
    CLI_SYNCE_ERR_IF_DIS_MODE,
    CLI_SYNCE_MAX_ERR
};

#define SYNCE_CLI_MAX_ARGS   15
#define SYNCE_CLI_SHOW_CMDS_MAX_ARGS    5

#if defined  (__SYNCECLIG_C__)
CONST CHR1  *gaSynceCliErrString [] = {
    "Undefined Error Occurred.\r\n",
    "Configuration is not allowed. SyncE module is disabled.\r\n",
    "Invalid Context.\r\n",
    "Invalid Interface.\r\n",
    "Invalid QL Code. QL code might not be supported in the currently selected "
    "SSM option.\r\n",
    "Synchronous mode is not enabled on interface.\r\n",
    "Synchronous mode is not enabled on the context.\r\n",
    "Synchronous mode is enabled on maximum number of supported interfaces.\r\n",
    "SSM option can not be changed because synchronous mode enabled on one or "
    "more ports.\r\n",
    "QL value is invalid in currently selected option.\r\n",
    "QL value is not supported.\r\n",
    "Switch request failed. QL value of interface is lower than the selected "
    "interface QL.\r\n",
    "Switch request failed. Priority of interface is set to 0.\r\n",
    "Switch request failed. Interface is in lockout.\r\n",
    "Switch request failed. Active switch is applied on System.\r\n",
    "Switch request failed. Interface is in signal fail state.\r\n",
    "ESMC mode couldn't be set as Rx because system is operating in QL-Disabled "
    "mode.\r\n",
    "Clear Lockout failed as Lock out is not set.\r\n",
    "Clear Switch failed as force or manual switch is not set.\r\n",
    "Manual Switch rejected as force switch is set.\r\n",
    "Invalid Input.\r\n",
    "Switch request failed. Interface is in Tx mode.\r\n",
    "Synce mode can not be disabled because force/manual switch is set\r\n",
    NULL,
};
#else

PUBLIC CHR1* gaSynceCliErrString [];
#endif


#define   CLI_SYNCE_SSM_OPTION_MODE1        1
#define   CLI_SYNCE_SSM_OPTION_MODE2GEN1    2
#define   CLI_SYNCE_SSM_OPTION_MODE2GEN2    3
#define   CLI_SYNCE_MODE_ENABLE             1
#define   CLI_SYNCE_MODE_DISABLE            0
#define   CLI_SYNCE_ESMC_MODE_NONE          0
#define   CLI_SYNCE_ESMC_MODE_RX            1
#define   CLI_SYNCE_ESMC_MODE_TX            2
#define   CLI_SYNCE_LOCKOUT_ENABLE          1
#define   CLI_SYNCE_LOCKOUT_DISABLE         0
#define   CLI_SYNCE_SWITCH_FORCED           1
#define   CLI_SYNCE_SWITCH_MANUAL           2
#define   CLI_SYNCE_SWITCH_NONE             0
#define   CLI_SYNCE_QL_MODE_ENABLED         1
#define   CLI_SYNCE_QL_MODE_DISABLED        0

#define   CLI_SYNCE_MODULE_START            1
#define   CLI_SYNCE_MODULE_SHUTDOWN         2

#define   SYNCE_CLOCK_QL_PRC                1
#define   SYNCE_CLOCK_QL_SSUA               2
#define   SYNCE_CLOCK_QL_SSUB               3
#define   SYNCE_CLOCK_QL_SEC                4
#define   SYNCE_CLOCK_QL_DNU                5
#define   SYNCE_CLOCK_QL_PRS                6
#define   SYNCE_CLOCK_QL_STU                7
#define   SYNCE_CLOCK_QL_ST2                8
#define   SYNCE_CLOCK_QL_TNC                9
#define   SYNCE_CLOCK_QL_ST3E               10
#define   SYNCE_CLOCK_QL_ST3                11
#define   SYNCE_CLOCK_QL_SIC                12
#define   SYNCE_CLOCK_QL_RES                13
#define   SYNCE_CLOCK_QL_PROV               14
#define   SYNCE_CLOCK_QL_DUS                15

#define   SYNCE_CNTR_ALL                   1
#define   SYNCE_CNTR_TX                    2
#define   SYNCE_CNTR_RX                    3
#define   SYNCE_CNTR_ERRORED               4
#define   SYNCE_CNTR_DROPPED               5

#define   SYNCE_MAX_STR_LEN 50
#endif
