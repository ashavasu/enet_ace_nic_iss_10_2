/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fserpscli.h,v 1.12 2016/01/11 13:21:31 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsErpsContextId[13];
extern UINT4 FsErpsCtxtSystemControl[13];
extern UINT4 FsErpsCtxtModuleStatus[13];
extern UINT4 FsErpsCtxtTraceInput[13];
extern UINT4 FsErpsCtxtTrapStatus[13];
extern UINT4 FsErpsCtxtClearRingStats[13];
extern UINT4 FsErpsCtxtRowStatus[13];
extern UINT4 FsErpsCtxtVlanGroupManager[13];
extern UINT4 FsErpsCtxtProprietaryClearFS[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsErpsCtxtSystemControl(u4FsErpsContextId ,i4SetValFsErpsCtxtSystemControl) \
 nmhSetCmnWithLock(FsErpsCtxtSystemControl, 13, FsErpsCtxtSystemControlSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 1, "%u %i", u4FsErpsContextId ,i4SetValFsErpsCtxtSystemControl)
#define nmhSetFsErpsCtxtModuleStatus(u4FsErpsContextId ,i4SetValFsErpsCtxtModuleStatus) \
 nmhSetCmnWithLock(FsErpsCtxtModuleStatus, 13, FsErpsCtxtModuleStatusSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 1, "%u %i", u4FsErpsContextId ,i4SetValFsErpsCtxtModuleStatus)
#define nmhSetFsErpsCtxtTraceInput(u4FsErpsContextId ,pSetValFsErpsCtxtTraceInput) \
 nmhSetCmnWithLock(FsErpsCtxtTraceInput, 13, FsErpsCtxtTraceInputSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 1, "%u %s", u4FsErpsContextId ,pSetValFsErpsCtxtTraceInput)
#define nmhSetFsErpsCtxtTrapStatus(u4FsErpsContextId ,i4SetValFsErpsCtxtTrapStatus) \
 nmhSetCmnWithLock(FsErpsCtxtTrapStatus, 13, FsErpsCtxtTrapStatusSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 1, "%u %i", u4FsErpsContextId ,i4SetValFsErpsCtxtTrapStatus)
#define nmhSetFsErpsCtxtClearRingStats(u4FsErpsContextId ,i4SetValFsErpsCtxtClearRingStats) \
 nmhSetCmnWithLock(FsErpsCtxtClearRingStats, 13, FsErpsCtxtClearRingStatsSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 1, "%u %i", u4FsErpsContextId ,i4SetValFsErpsCtxtClearRingStats)
#define nmhSetFsErpsCtxtRowStatus(u4FsErpsContextId ,i4SetValFsErpsCtxtRowStatus) \
 nmhSetCmnWithLock(FsErpsCtxtRowStatus, 13, FsErpsCtxtRowStatusSet, ErpsApiLock, ErpsApiUnLock, 0, 1, 1, "%u %i", u4FsErpsContextId ,i4SetValFsErpsCtxtRowStatus)
#define nmhSetFsErpsCtxtVlanGroupManager(u4FsErpsContextId ,i4SetValFsErpsCtxtVlanGroupManager) \
 nmhSetCmnWithLock(FsErpsCtxtVlanGroupManager, 13, FsErpsCtxtVlanGroupManagerSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 1, "%u %i", u4FsErpsContextId ,i4SetValFsErpsCtxtVlanGroupManager)
#define nmhSetFsErpsCtxtProprietaryClearFS(u4FsErpsContextId, i4SetValFsErpsCtxtProprietaryClearFS) \
 nmhSetCmnWithLock(FsErpsCtxtProprietaryClearFS, 13, FsErpsCtxtProprietaryClearFSSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 1, "%u %i", u4FsErpsContextId, i4SetValFsErpsCtxtProprietaryClearFS)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsErpsVlanId[13];
extern UINT4 FsErpsVlanGroupId[13];
extern UINT4 FsErpsVlanGroupRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsErpsVlanGroupRowStatus(u4FsErpsContextId , i4FsErpsVlanId , i4FsErpsVlanGroupId ,i4SetValFsErpsVlanGroupRowStatus) \
 nmhSetCmnWithLock(FsErpsVlanGroupRowStatus, 13, FsErpsVlanGroupRowStatusSet, ErpsApiLock, ErpsApiUnLock, 0, 1, 3, "%u %i %i %i", u4FsErpsContextId , i4FsErpsVlanId , i4FsErpsVlanGroupId ,i4SetValFsErpsVlanGroupRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsErpsRingId[13];
extern UINT4 FsErpsRingVlanId[13];
extern UINT4 FsErpsRingName[13];
extern UINT4 FsErpsRingPort1[13];
extern UINT4 FsErpsRingPort2[13];
extern UINT4 FsErpsRingRplPort[13];
extern UINT4 FsErpsRingPortBlockingOnVcRecovery[13];
extern UINT4 FsErpsRingOperatingMode[13];
extern UINT4 FsErpsRingMonitorMechanism[13]; 
extern UINT4 FsErpsRingRowStatus[13];
extern UINT4 FsErpsRingMacId[13];
extern UINT4 FsErpsRingProtectedVlanGroupId[13];
extern UINT4 FsErpsRingProtectionType[13];
extern UINT4 FsErpsRingRAPSCompatibleVersion[13];
extern UINT4 FsErpsRingRplNeighbourPort[13];
extern UINT4 FsErpsRingRAPSSubRingWithoutVC[13];
extern UINT4 FsErpsRingRplNextNeighbourPort[13];
extern UINT4 FsErpsRingProtectedVlanGroupList[13];
extern UINT4 FsErpsRingServiceType[13];
extern UINT4 FsErpsRingPort1SubPortList[13];
extern UINT4 FsErpsRingPort2SubPortList[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsErpsRingVlanId(u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingVlanId) \
 nmhSetCmnWithLock(FsErpsRingVlanId, 13, FsErpsRingVlanIdSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %i", u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingVlanId)
#define nmhSetFsErpsRingName(u4FsErpsContextId , u4FsErpsRingId ,pSetValFsErpsRingName) \
 nmhSetCmnWithLock(FsErpsRingName, 13, FsErpsRingNameSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %s", u4FsErpsContextId , u4FsErpsRingId ,pSetValFsErpsRingName)
#define nmhSetFsErpsRingPort1(u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingPort1) \
 nmhSetCmnWithLock(FsErpsRingPort1, 13, FsErpsRingPort1Set, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %i", u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingPort1)
#define nmhSetFsErpsRingPort2(u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingPort2) \
 nmhSetCmnWithLock(FsErpsRingPort2, 13, FsErpsRingPort2Set, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %i", u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingPort2)
#define nmhSetFsErpsRingRplPort(u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingRplPort) \
 nmhSetCmnWithLock(FsErpsRingRplPort, 13, FsErpsRingRplPortSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %i", u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingRplPort)
#define nmhSetFsErpsRingPortBlockingOnVcRecovery(u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingPortBlockingOnVcRecovery) \
 nmhSetCmnWithLock(FsErpsRingPortBlockingOnVcRecovery, 13, FsErpsRingPortBlockingOnVcRecoverySet, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %i", u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingPortBlockingOnVcRecovery)
#define nmhSetFsErpsRingOperatingMode(u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingOperatingMode) \
 nmhSetCmnWithLock(FsErpsRingOperatingMode, 13, FsErpsRingOperatingModeSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %i", u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingOperatingMode)
#define nmhSetFsErpsRingMonitorMechanism(u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingMonitorMechanism) \
 nmhSetCmnWithLock(FsErpsRingMonitorMechanism, 13, FsErpsRingMonitorMechanismSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %i", u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingMonitorMechanism)
#define nmhSetFsErpsRingRowStatus(u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingRowStatus) \
 nmhSetCmnWithLock(FsErpsRingRowStatus, 13, FsErpsRingRowStatusSet, ErpsApiLock, ErpsApiUnLock, 0, 1, 2, "%u %u %i", u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingRowStatus)
#define nmhSetFsErpsRingMacId(u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingMacId) \
 nmhSetCmnWithLock(FsErpsRingMacId, 13, FsErpsRingMacIdSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %i", u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingMacId)
#define nmhSetFsErpsRingProtectedVlanGroupId(u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingProtectedVlanGroupId) \
 nmhSetCmnWithLock(FsErpsRingProtectedVlanGroupId, 13, FsErpsRingProtectedVlanGroupIdSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %i", u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingProtectedVlanGroupId)
#define nmhSetFsErpsRingProtectionType(u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingProtectionType) \
 nmhSetCmnWithLock(FsErpsRingProtectionType, 13, FsErpsRingProtectionTypeSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %i", u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingProtectionType)
#define nmhSetFsErpsRingRAPSCompatibleVersion(u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingRAPSCompatibleVersion) \
 nmhSetCmnWithLock(FsErpsRingRAPSCompatibleVersion, 13, FsErpsRingRAPSCompatibleVersionSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %i", u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingRAPSCompatibleVersion)
#define nmhSetFsErpsRingRplNeighbourPort(u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingRplNeighbourPort) \
 nmhSetCmnWithLock(FsErpsRingRplNeighbourPort, 13, FsErpsRingRplNeighbourPortSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %i", u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingRplNeighbourPort)
#define nmhSetFsErpsRingRAPSSubRingWithoutVC(u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingRAPSSubRingWithoutVC) \
 nmhSetCmnWithLock(FsErpsRingRAPSSubRingWithoutVC, 13, FsErpsRingRAPSSubRingWithoutVCSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %i", u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingRAPSSubRingWithoutVC)
#define nmhSetFsErpsRingRplNextNeighbourPort(u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingRplNextNeighbourPort) \
 nmhSetCmnWithLock(FsErpsRingRplNextNeighbourPort, 13, FsErpsRingRplNextNeighbourPortSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %i", u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingRplNextNeighbourPort)
#define nmhSetFsErpsRingProtectedVlanGroupList(u4FsErpsContextId , u4FsErpsRingId ,pSetValFsErpsRingProtectedVlanGroupList) \
 nmhSetCmnWithLock(FsErpsRingProtectedVlanGroupList, 13, FsErpsRingProtectedVlanGroupListSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %s", u4FsErpsContextId , u4FsErpsRingId ,pSetValFsErpsRingProtectedVlanGroupList)
#define nmhSetFsErpsRingServiceType(u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingServiceType) \
 nmhSetCmnWithLock(FsErpsRingServiceType, 13, FsErpsRingServiceTypeSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %i", u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingServiceType)
#define nmhSetFsErpsRingPort1SubPortList(u4FsErpsContextId , u4FsErpsRingId ,pSetValFsErpsRingPort1SubPortList) \
 nmhSetCmnWithLock(FsErpsRingPort1SubPortList, 13, FsErpsRingPort1SubPortListSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %s", u4FsErpsContextId , u4FsErpsRingId ,pSetValFsErpsRingPort1SubPortList)
#define nmhSetFsErpsRingPort2SubPortList(u4FsErpsContextId , u4FsErpsRingId ,pSetValFsErpsRingPort2SubPortList) \
 nmhSetCmnWithLock(FsErpsRingPort2SubPortList, 13, FsErpsRingPort2SubPortListSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %s", u4FsErpsContextId , u4FsErpsRingId ,pSetValFsErpsRingPort2SubPortList)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsErpsRingMEG1[13];
extern UINT4 FsErpsRingCfmME1[13];
extern UINT4 FsErpsRingCfmMEP1[13];
extern UINT4 FsErpsRingMEG2[13];
extern UINT4 FsErpsRingCfmME2[13];
extern UINT4 FsErpsRingCfmMEP2[13];
extern UINT4 FsErpsRingCfmRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsErpsRingMEG1(u4FsErpsContextId , u4FsErpsRingId ,u4SetValFsErpsRingMEG1) \
 nmhSetCmnWithLock(FsErpsRingMEG1, 13, FsErpsRingMEG1Set, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %u", u4FsErpsContextId , u4FsErpsRingId ,u4SetValFsErpsRingMEG1)
#define nmhSetFsErpsRingCfmME1(u4FsErpsContextId , u4FsErpsRingId ,u4SetValFsErpsRingCfmME1) \
 nmhSetCmnWithLock(FsErpsRingCfmME1, 13, FsErpsRingCfmME1Set, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %u", u4FsErpsContextId , u4FsErpsRingId ,u4SetValFsErpsRingCfmME1)
#define nmhSetFsErpsRingCfmMEP1(u4FsErpsContextId , u4FsErpsRingId ,u4SetValFsErpsRingCfmMEP1) \
 nmhSetCmnWithLock(FsErpsRingCfmMEP1, 13, FsErpsRingCfmMEP1Set, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %u", u4FsErpsContextId , u4FsErpsRingId ,u4SetValFsErpsRingCfmMEP1)
#define nmhSetFsErpsRingMEG2(u4FsErpsContextId , u4FsErpsRingId ,u4SetValFsErpsRingMEG2) \
 nmhSetCmnWithLock(FsErpsRingMEG2, 13, FsErpsRingMEG2Set, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %u", u4FsErpsContextId , u4FsErpsRingId ,u4SetValFsErpsRingMEG2)
#define nmhSetFsErpsRingCfmME2(u4FsErpsContextId , u4FsErpsRingId ,u4SetValFsErpsRingCfmME2) \
 nmhSetCmnWithLock(FsErpsRingCfmME2, 13, FsErpsRingCfmME2Set, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %u", u4FsErpsContextId , u4FsErpsRingId ,u4SetValFsErpsRingCfmME2)
#define nmhSetFsErpsRingCfmMEP2(u4FsErpsContextId , u4FsErpsRingId ,u4SetValFsErpsRingCfmMEP2) \
 nmhSetCmnWithLock(FsErpsRingCfmMEP2, 13, FsErpsRingCfmMEP2Set, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %u", u4FsErpsContextId , u4FsErpsRingId ,u4SetValFsErpsRingCfmMEP2)
#define nmhSetFsErpsRingCfmRowStatus(u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingCfmRowStatus) \
 nmhSetCmnWithLock(FsErpsRingCfmRowStatus, 13, FsErpsRingCfmRowStatusSet, ErpsApiLock, ErpsApiUnLock, 0, 1, 2, "%u %u %i", u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingCfmRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsErpsRingConfigHoldOffTime[13];
extern UINT4 FsErpsRingConfigGuardTime[13];
extern UINT4 FsErpsRingConfigWTRTime[13];
extern UINT4 FsErpsRingConfigPeriodicTime[13];
extern UINT4 FsErpsRingConfigSwitchPort[13];
extern UINT4 FsErpsRingConfigSwitchCmd[13];
extern UINT4 FsErpsRingConfigRecoveryMethod[13];
extern UINT4 FsErpsRingConfigPropagateTC[13];
extern UINT4 FsErpsRingConfigWTBTime[13];
extern UINT4 FsErpsRingConfigClear[13];
extern UINT4 FsErpsRingConfigInterConnNode[13];
extern UINT4 FsErpsRingConfigMultipleFailure[13];
extern UINT4 FsErpsRingConfigIsPort1Present[13];
extern UINT4 FsErpsRingConfigIsPort2Present[13];
extern UINT4 FsErpsRingConfigInfoDistributingPort[13];
extern UINT4 FsErpsRingConfigKValue[13];
extern UINT4 FsErpsRingConfigFailureOfProtocol[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsErpsRingConfigHoldOffTime(u4FsErpsContextId , u4FsErpsRingId ,u4SetValFsErpsRingConfigHoldOffTime) \
 nmhSetCmnWithLock(FsErpsRingConfigHoldOffTime, 13, FsErpsRingConfigHoldOffTimeSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %u", u4FsErpsContextId , u4FsErpsRingId ,u4SetValFsErpsRingConfigHoldOffTime)
#define nmhSetFsErpsRingConfigGuardTime(u4FsErpsContextId , u4FsErpsRingId ,u4SetValFsErpsRingConfigGuardTime) \
 nmhSetCmnWithLock(FsErpsRingConfigGuardTime, 13, FsErpsRingConfigGuardTimeSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %u", u4FsErpsContextId , u4FsErpsRingId ,u4SetValFsErpsRingConfigGuardTime)
#define nmhSetFsErpsRingConfigWTRTime(u4FsErpsContextId , u4FsErpsRingId ,u4SetValFsErpsRingConfigWTRTime) \
 nmhSetCmnWithLock(FsErpsRingConfigWTRTime, 13, FsErpsRingConfigWTRTimeSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %u", u4FsErpsContextId , u4FsErpsRingId ,u4SetValFsErpsRingConfigWTRTime)
#define nmhSetFsErpsRingConfigPeriodicTime(u4FsErpsContextId , u4FsErpsRingId ,u4SetValFsErpsRingConfigPeriodicTime) \
 nmhSetCmnWithLock(FsErpsRingConfigPeriodicTime, 13, FsErpsRingConfigPeriodicTimeSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %u", u4FsErpsContextId , u4FsErpsRingId ,u4SetValFsErpsRingConfigPeriodicTime)
#define nmhSetFsErpsRingConfigSwitchPort(u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingConfigSwitchPort) \
 nmhSetCmnWithLock(FsErpsRingConfigSwitchPort, 13, FsErpsRingConfigSwitchPortSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %i", u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingConfigSwitchPort)
#define nmhSetFsErpsRingConfigSwitchCmd(u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingConfigSwitchCmd) \
 nmhSetCmnWithLock(FsErpsRingConfigSwitchCmd, 13, FsErpsRingConfigSwitchCmdSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %i", u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingConfigSwitchCmd)
#define nmhSetFsErpsRingConfigRecoveryMethod(u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingConfigRecoveryMethod) \
 nmhSetCmnWithLock(FsErpsRingConfigRecoveryMethod, 13, FsErpsRingConfigRecoveryMethodSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %i", u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingConfigRecoveryMethod)
#define nmhSetFsErpsRingConfigPropagateTC(u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingConfigPropagateTC) \
 nmhSetCmnWithLock(FsErpsRingConfigPropagateTC, 13, FsErpsRingConfigPropagateTCSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %i", u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingConfigPropagateTC)
#define nmhSetFsErpsRingConfigWTBTime(u4FsErpsContextId , u4FsErpsRingId ,u4SetValFsErpsRingConfigWTBTime) \
 nmhSetCmnWithLock(FsErpsRingConfigWTBTime, 13, FsErpsRingConfigWTBTimeSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %u", u4FsErpsContextId , u4FsErpsRingId ,u4SetValFsErpsRingConfigWTBTime)
#define nmhSetFsErpsRingConfigClear(u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingConfigClear) \
 nmhSetCmnWithLock(FsErpsRingConfigClear, 13, FsErpsRingConfigClearSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %i", u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingConfigClear)
#define nmhSetFsErpsRingConfigInterConnNode(u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingConfigInterConnNode) \
 nmhSetCmnWithLock(FsErpsRingConfigInterConnNode, 13, FsErpsRingConfigInterConnNodeSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %i", u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingConfigInterConnNode)
#define nmhSetFsErpsRingConfigMultipleFailure(u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingConfigMultipleFailure) \
 nmhSetCmnWithLock(FsErpsRingConfigMultipleFailure, 13, FsErpsRingConfigMultipleFailureSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %i", u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingConfigMultipleFailure)
#define nmhSetFsErpsRingConfigIsPort1Present(u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingConfigIsPort1Present) \
 nmhSetCmnWithLock(FsErpsRingConfigIsPort1Present, 13, FsErpsRingConfigIsPort1PresentSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %i", u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingConfigIsPort1Present)
#define nmhSetFsErpsRingConfigIsPort2Present(u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingConfigIsPort2Present) \
 nmhSetCmnWithLock(FsErpsRingConfigIsPort2Present, 13, FsErpsRingConfigIsPort2PresentSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %i", u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingConfigIsPort2Present)
#define nmhSetFsErpsRingConfigInfoDistributingPort(u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingConfigInfoDistributingPort) \
 nmhSetCmnWithLock(FsErpsRingConfigInfoDistributingPort, 13, FsErpsRingConfigInfoDistributingPortSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %i", u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingConfigInfoDistributingPort)
#define nmhSetFsErpsRingConfigKValue(u4FsErpsContextId , u4FsErpsRingId ,pSetValFsErpsRingConfigKValue) \
nmhSetCmnWithLock(FsErpsRingConfigKValue, 13, FsErpsRingConfigKValueSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %s", u4FsErpsContextId , u4FsErpsRingId ,pSetValFsErpsRingConfigKValue)
#define nmhSetFsErpsRingConfigFailureOfProtocol(u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingConfigFailureOfProtocol) \
nmhSetCmnWithLock(FsErpsRingConfigFailureOfProtocol, 13, FsErpsRingConfigFailureOfProtocolSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %i", u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingConfigFailureOfProtocol)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsErpsRingTcPropRingId[13];
extern UINT4 FsErpsRingTcPropRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsErpsRingTcPropRowStatus(u4FsErpsContextId , u4FsErpsRingId , u4FsErpsRingTcPropRingId ,i4SetValFsErpsRingTcPropRowStatus) \
 nmhSetCmnWithLock(FsErpsRingTcPropRowStatus, 13, FsErpsRingTcPropRowStatusSet, ErpsApiLock, ErpsApiUnLock, 0, 1, 3, "%u %u %u %i", u4FsErpsContextId , u4FsErpsRingId , u4FsErpsRingTcPropRingId ,i4SetValFsErpsRingTcPropRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsErpsRingConfigExtVCRecoveryPeriodicTime[13];
extern UINT4 FsErpsRingConfigExtMainRingId[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsErpsRingConfigExtVCRecoveryPeriodicTime(u4FsErpsContextId , u4FsErpsRingId ,u4SetValFsErpsRingConfigExtVCRecoveryPeriodicTime) \
 nmhSetCmnWithLock(FsErpsRingConfigExtVCRecoveryPeriodicTime, 13, FsErpsRingConfigExtVCRecoveryPeriodicTimeSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %u", u4FsErpsContextId , u4FsErpsRingId ,u4SetValFsErpsRingConfigExtVCRecoveryPeriodicTime)
#define nmhSetFsErpsRingConfigExtMainRingId(u4FsErpsContextId , u4FsErpsRingId ,u4SetValFsErpsRingConfigExtMainRingId) \
 nmhSetCmnWithLock(FsErpsRingConfigExtMainRingId, 13, FsErpsRingConfigExtMainRingIdSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %u", u4FsErpsContextId , u4FsErpsRingId ,u4SetValFsErpsRingConfigExtMainRingId)

#endif

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsErpsRingPwVcId1[13];
extern UINT4 FsErpsRingPwVcId2[13];
extern UINT4 FsErpsRingMplsRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsErpsRingPwVcId1(u4FsErpsContextId , u4FsErpsRingId ,u4SetValFsErpsRingPwVcId1)	\
	nmhSetCmnWithLock(FsErpsRingPwVcId1, 13, FsErpsRingPwVcId1Set, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %u", u4FsErpsContextId , u4FsErpsRingId ,u4SetValFsErpsRingPwVcId1)
#define nmhSetFsErpsRingPwVcId2(u4FsErpsContextId , u4FsErpsRingId ,u4SetValFsErpsRingPwVcId2)	\
	nmhSetCmnWithLock(FsErpsRingPwVcId2, 13, FsErpsRingPwVcId2Set, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %u", u4FsErpsContextId , u4FsErpsRingId ,u4SetValFsErpsRingPwVcId2)
#define nmhSetFsErpsRingMplsRowStatus(u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingMplsRowStatus)	\
	nmhSetCmnWithLock(FsErpsRingMplsRowStatus, 13, FsErpsRingMplsRowStatusSet, ErpsApiLock, ErpsApiUnLock, 0, 1, 2, "%u %u %i", u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingMplsRowStatus)

#endif

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsErpsRingClearRingStats[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsErpsRingClearRingStats(u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingClearRingStats) \
 nmhSetCmnWithLock(FsErpsRingClearRingStats, 13, FsErpsRingClearRingStatsSet, ErpsApiLock, ErpsApiUnLock, 0, 0, 2, "%u %u %i", u4FsErpsContextId , u4FsErpsRingId ,i4SetValFsErpsRingClearRingStats)

#endif
