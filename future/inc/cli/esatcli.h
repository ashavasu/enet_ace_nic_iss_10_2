/********************************************************************
 * Copyright (C) 2014 Aricent Inc . All Rights Reserved
 *
 * $Id: esatcli.h,v 1.2 2014/09/05 10:45:12 siva Exp $
 *
 * Description: This file contains ESAT CLI command constants, error
 *              strings and function prototypes.
 *
 *******************************************************************/


#ifndef __ESATCLI_H__
#define __ESATCLI_H__

#include "cli.h"

/* 
 * ESAT CLI command constants. To add a new command ,a new definition needs to
 * be added to the list.
 */


enum {
    CLI_ESAT_SHUTDOWN = 1,
    CLI_ESAT_START,
    CLI_ESAT_DEBUG,
    CLI_ESAT_NO_DEBUG,
    CLI_ESAT_SLA_CREATE,
    CLI_ESAT_SLA_DELETE,
    CLI_ESAT_MAP_INTERFACE,
    CLI_ESAT_MAP_TRAF_PROFILE,
    CLI_ESAT_MAP_SAC,
    CLI_ESAT_CFM_CFG,
    CLI_ESAT_SLA_STEP,
    CLI_ESAT_SLA_DURATION,
    CLI_ESAT_SLA_DIRECTION,
    CLI_ESAT_TRAF_PROF_CREATE,
    CLI_ESAT_TRAF_PROF_DELETE,
    CLI_ESAT_TRAF_PROF_TAG_TYPE,
    CLI_ESAT_TRAF_PROF_DIRECTION,
    CLI_ESAT_TRAF_PROF_INNER_VLAN,
    CLI_ESAT_TRAF_PROF_OUTER_VLAN,
    CLI_ESAT_TRAF_PROF_INNER_COS,
    CLI_ESAT_TRAF_PROF_OUTER_COS,
    CLI_ESAT_TRAF_PROF_PKT_SIZE,
    CLI_ESAT_TRAF_PROF_SRC_MAC,
    CLI_ESAT_TRAF_PROF_DEST_MAC,
    CLI_ESAT_TRAF_PROF_PAYLOAD,
    CLI_ESAT_SAC_CREATE,
    CLI_ESAT_SAC_DELETE,
    CLI_ESAT_CONFIGURATION_TEST,
    CLI_ESAT_SHOW_SLA,
    CLI_ESAT_SHOW_TRAF_PROF,
    CLI_ESAT_SHOW_SAC,
    CLI_ESAT_SHOW_SLA_REPORT,
    CLI_ESAT_MAX_COMMANDS
};

enum
{
    CLI_ESAT_DIRECTION_EXTERNAL = 1,
    CLI_ESAT_DIRECTION_INTERNAL
};

enum
{
    CLI_ESAT_TAG_TYPE_UNTAGGED = 1,
    CLI_ESAT_TAG_TYPE_SINGLE,
    CLI_ESAT_TAG_TYPE_DOUBLE,
    CLI_ESAT_TAG_TYPE_PRIORITY
};

enum
{
    CLI_ESAT_SCT_START = 1,
    CLI_ESAT_SCT_STOP
};

enum {
    CLI_ESAT_UNKNOWN_ERR = 1,
    CLI_ESAT_MODULE_SHUTDOWN,
    CLI_ESAT_INVALID_PORT,
    CLI_ESAT_INVALID_EVC,
    CLI_ESAT_INVALID_MEG,
    CLI_ESAT_INVALID_ME,
    CLI_ESAT_INVALID_MEP,
    CLI_ESAT_INVALID_DIRECTION,
    CLI_ESAT_TEST_IN_PROGRESS,
    CLI_ESAT_INVALID_IN_VLAN,
    CLI_ESAT_INVALID_OUT_VLAN,
    CLI_ESAT_INVALID_IN_COS,
    CLI_ESAT_INVALID_OUT_COS,
    CLI_ESAT_ERR_TRAF_PROF,
    CLI_ESAT_NO_SLA_INFO,
    CLI_ESAT_INVALID_RATE,
    CLI_ESAT_SLA_TEST_FAIL,
    CLI_ESAT_SAC_INVALID_IR,
    CLI_ESAT_SAC_INVALID_FTD,
    CLI_ESAT_SAC_INVALID_FDV,
    CLI_ESAT_TEST_NOT_STARTED,
    CLI_ESAT_TRAF_PROF_MAPPED,
    CLI_ESAT_SAC_MAPPED,
    CLI_ESAT_MAX_ERR
};

#ifdef  _ESATCLI_C_
CONST CHR1  *gaEsatCliErrString [] = {
    NULL,
    "Unknown Error\r\n",
    "% ESAT Module is Shut Down\r\n",
    "% SLA should be mapped to a Port, before mapping an EVC Index \r\n",
    "% EVC Index is not present in this context \r\n",
    "% Invalid MEG Index \r\n",
    "% Invalid ME Index \r\n",
    "% Invalid MEP Index \r\n",
    "% Invalid value for Direction \r\n",
    "% SLA Test already in Progress \r\n",
    "% Inner VLAN can be configured only when the TAG type is Single/Double Tagged \r\n",
    "% Outer VLAN can be configured only when the TAG type is Double Tagged \r\n",
    "% Inner CoS can be configured only when the TAG type is Single/Double/Priority Tagged \r\n",
    "% Outer CoS can be configured only when the TAG type is Double Tagged \r\n",
    "% Traffic Profile Entry is not Active,configure all mandatory parameters for traffic profile\r\n",
    "% SLA information is not present\r\n",
    "% Rate Step should be divisor of 100 \r\n",
    "% Failed to start the SLA test all mandatory parameters are not provided\r\n",
    "% Invalid values is provided for Information rate\r\n",
    "% Invalid values is provided for Frame Transfer delay \r\n",
    "% Invalid values is provided for Frame delay variation \r\n",
    "% Service Configuration Test is not running\r\n",
    "% Traffic Profile is Mapped with a SLA\r\n",
    "% SAC is Mapped with a SLA\r\n",
    "\r\n"
};
#else
extern CONST CHR1  *gaEsatCliErrString[];
#endif



#define ESAT_CLI_MAX_ARGS       10


INT4 cli_process_esat_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);
INT4 cli_process_esat_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);

INT4 EsatCliSetSystemControl (tCliHandle CliHandle, INT4 i4SystemControl);

INT4 EsatCliSetSlaCreate (tCliHandle CliHandle, UINT4 u4SlaId);

INT4 EsatCliSetSlaDelete (tCliHandle CliHandle, UINT4 u4SlaId);

INT4 EsatCliSetSlaMapInterface (tCliHandle CliHandle, UINT4 u4SlaId,
        UINT4 u4IfIndex, UINT4 u4EvcIndex);

INT4 EsatCliSetSlaMapTrafficProfile (tCliHandle CliHandle,
        UINT4 u4SlaId, UINT4 u4TrafProfId);

INT4 EsatCliSetSlaMapSac (tCliHandle CliHandle, UINT4 u4SlaId, UINT4 u4SacId);

INT4 EsatCliSetSlaMapCfmEntries (UINT4 u4SlaId,
        UINT4 u4SlaMeg, UINT4 u4SlaMe, UINT4 u4SlaMep);

INT4 EsatCliSetSlaRateStep (UINT4 u4SlaId, UINT4 u4RateStep);

INT4 EsatCliSetSlaDuration (UINT4 u4SlaId, UINT4 u4Duration);

INT4 EsatCliSetSlaDelay (UINT4 u4SlaId, UINT4 u4Delay);

INT4 EsatCliSetSlaDirection (UINT4 u4SlaId, UINT4 u4Direction);

INT4 EsatCliSetTrafProfCreate (tCliHandle CliHandle, UINT4 u4TrafProfId);

INT4 EsatCliSetTrafProfDelete (UINT4 u4TrafProfId);

INT4 EsatCliSetVlanTagType (UINT4 u4TrafProfId, UINT4 u4VlanTagType);

INT4 EsatCliSetTrafProfDirection (UINT4 u4TrafProfId, UINT4 u4Direction);

INT4 EsatCliSetInnerVlan (UINT4 u4TrafProfId, UINT4 u4InnerVlan);

INT4 EsatCliSetOuterVlan (UINT4 u4TrafProfId, UINT4 u4OuterVlan);

INT4 EsatCliSetInnerCos (UINT4 u4TrafProfId, UINT4 u4InnerCos);

INT4 EsatCliSetOuterCos (UINT4 u4TrafProfId, UINT4 u4OuterCos);

INT4 EsatCliSetPacketSize (UINT4 u4TrafProfId, UINT4 u4PacketSize);

INT4 EsatCliSetSrcMac (UINT4 u4TrafProfId, UINT1 *pu1SrcMacAddr);

INT4 EsatCliSetDestMac (UINT4 u4TrafProfId, UINT1 *pu1DestMacAddr);

INT4 EsatCliSetPayload (UINT4 u4TrafProfId, UINT1 *pu1Payload);

INT4 EsatCliSetConfigurationTest (UINT4 u4SlaId, UINT4 u4Status);

INT4 EsatCliSetSacCreate (tCliHandle CliHandle, UINT4 u4SacId,
        UINT4 u4SacIr, UINT4 u4SacFlr, UINT4 u4SacFtd, UINT4 u4SacFdv);

INT4 EsatCliSetSacDelete (UINT4 u4SacId);

INT4 EsatCliSetTraceOption (INT4 i4TraceInput, INT4 i4Action);

INT1
EsatGetSlaCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

INT1
EsatGetTrafficProfileCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

INT1
EsatGetVcmSlaCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

INT1
EsatGetVcmTrafficProfileCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

INT4
EsatCliShowSlaTable (tCliHandle CliHandle, UINT4 *pu4SlaId);
INT4
EsatCliShowTrafProfTable (tCliHandle CliHandle, UINT4 *pu4ProfileId);
INT4
EsatCliShowSacTable (tCliHandle CliHandle, UINT4 *pu4SacId);
INT4
EsatCliShowSlaStats (tCliHandle CliHandle, UINT4 *pu4SlaId);
INT4
EsatCliShowSlaReport (tCliHandle CliHandle, UINT4 *pu4SlaId);
#endif /* __ESATCLI_H__ */
