/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: vxmibcli.h,v 1.3 2018/01/05 09:57:08 siva Exp $
*
* Description: Extern Declaration of the Mib objects
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */

extern UINT4 FsVxlanEnable[12];

extern UINT4 FsVxlanUdpPort[12];

extern UINT4 FsVxlanTraceOption[12];

extern UINT4 FsVxlanNotificationCntl[12];

extern UINT4 FsEvpnVxlanEnable[12];

extern UINT4 FsVxlanVtepNveIfIndex[14];

extern UINT4 FsVxlanVtepAddressType[14];

extern UINT4 FsVxlanVtepAddress[14];

extern UINT4 FsVxlanVtepRowStatus[14];

extern UINT4 FsVxlanNveIfIndex[14];

extern UINT4 FsVxlanNveVniNumber[14];

extern UINT4 FsVxlanNveDestVmMac[14];

extern UINT4 FsVxlanNveRemoteVtepAddressType[14];

extern UINT4 FsVxlanNveRemoteVtepAddress[14];

extern UINT4 FsVxlanNveRowStatus[14];

extern UINT4 FsVxlanSuppressArp[14];

extern UINT4 FsVxlanMCastNveIfIndex[14];

extern UINT4 FsVxlanMCastVniNumber[14];

extern UINT4 FsVxlanMCastGroupAddressType[14];

extern UINT4 FsVxlanMCastGroupAddress[14];

extern UINT4 FsVxlanMCastRowStatus[14];

extern UINT4 FsVxlanVniVlanMapVlanId[14];

extern UINT4 FsVxlanVniVlanMapVniNumber[14];

extern UINT4 FsVxlanVniVlanMapPktSent[14];

extern UINT4 FsVxlanVniVlanMapPktRcvd[14];

extern UINT4 FsVxlanVniVlanMapPktDrpd[14];

extern UINT4 FsVxlanVniVlanTagStatus[14];

extern UINT4 FsVxlanVniVlanMapRowStatus[14];

extern UINT4 FsVxlanInReplicaNveIfIndex[14];

extern UINT4 FsVxlanInReplicaVniNumber[14];

extern UINT4 FsVxlanInReplicaRemoteVtepAddressType[14];

extern UINT4 FsVxlanInReplicaRemoteVtepAddress1[14];

extern UINT4 FsVxlanInReplicaRemoteVtepAddress2[14];

extern UINT4 FsVxlanInReplicaRemoteVtepAddress3[14];

extern UINT4 FsVxlanInReplicaRemoteVtepAddress4[14];

extern UINT4 FsVxlanInReplicaRemoteVtepAddress5[14];

extern UINT4 FsVxlanInReplicaRemoteVtepAddress6[14];

extern UINT4 FsVxlanInReplicaRemoteVtepAddress7[14];

extern UINT4 FsVxlanInReplicaRemoteVtepAddress8[14];

extern UINT4 FsVxlanInReplicaRemoteVtepAddress9[14];

extern UINT4 FsVxlanInReplicaRemoteVtepAddress10[14];

extern UINT4 FsVxlanInReplicaRowStatus[14];

extern UINT4 FsEvpnVxlanEviVniMapEviIndex[14];

extern UINT4 FsEvpnVxlanEviVniMapVniNumber[14];

extern UINT4 FsEvpnVxlanEviVniMapBgpRD[14];

extern UINT4 FsEvpnVxlanEviVniESI[14];

extern UINT4 FsEvpnVxlanEviVniLoadBalance[14];

extern UINT4 FsEvpnVxlanEviVniMapSentPkts[14];

extern UINT4 FsEvpnVxlanEviVniMapRcvdPkts[14];

extern UINT4 FsEvpnVxlanEviVniMapDroppedPkts[14];

extern UINT4 FsEvpnVxlanEviVniMapRowStatus[14];

extern UINT4 FsEvpnVxlanEviVniMapBgpRDAuto[14];

extern UINT4 FsEvpnVxlanBgpRTIndex[14];

extern UINT4 FsEvpnVxlanBgpRTType[14];

extern UINT4 FsEvpnVxlanBgpRT[14];

extern UINT4 FsEvpnVxlanBgpRTRowStatus[14];

extern UINT4 FsEvpnVxlanBgpRTAuto[14];

extern UINT4 FsEvpnVxlanVrfName[14];

extern UINT4 FsEvpnVxlanVrfRD[14];

extern UINT4 FsEvpnVxlanVrfRowStatus[14];

extern UINT4 FsEvpnVxlanVrfRTIndex[14];

extern UINT4 FsEvpnVxlanVrfRTType[14];

extern UINT4 FsEvpnVxlanVrfRT[14];

extern UINT4 FsEvpnVxlanVrfRTRowStatus[14];

extern UINT4 FsEvpnVxlanPeerIpAddressType[14];

extern UINT4 FsEvpnVxlanPeerIpAddress[14];

extern UINT4 FsEvpnVxlanMHEviVniESI[14];

extern UINT4 FsEvpnVxlanOrdinalNum[14];

extern UINT4 FsEvpnVxlanMultihomedPeerRowStatus[14];
