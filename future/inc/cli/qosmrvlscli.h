/* $Id: qosmrvlscli.h,v 1.7 2013/12/16 15:36:17 siva Exp $ */
/****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                     */
/*                                                                          */
/*  FILE NAME             : qoscli.h                                        */
/*  PRINCIPAL AUTHOR      : Aricent                                         */
/*  SUBSYSTEM NAME        : QoS                                             */
/*  MODULE NAME           : QoS-Ext                                         */
/*  LANGUAGE              : C                                               */
/*  TARGET ENVIRONMENT    : Linux                                           */
/*  DATE OF FIRST RELEASE :                                                 */
/*  AUTHOR                : Aricent                                         */
/*  FUNCTIONS DEFINED(Applicable for Source files) :                        */
/*  DESCRIPTION           : This file contains the CLI definitions and      */
/*                          macros of Aricent QoS Module.                   */
/****************************************************************************/
/*                                                                          */
/*  Change History                                                          */
/*  Version               :                                                 */
/*  Date(DD/MM/YYYY)      :                                                 */
/*  Modified by           :                                                 */
/*  Description of change :                                                 */
/****************************************************************************/

#ifndef __QOS_CLI_H__
#define __QOS_CLI_H__

#include "cli.h"
#define CLI_RS_INVALID_MASK  ~0UL
#define CLI_RS_INVALID_I_MASK  (-1)

#define QOS_CLI_MAX_ARGS      10   
#define QOS_CLI_NEW_ENTRY     0
#define QOS_CLI_MODIFY_ENTRY  1

/* CLI Prompt Macros */
#define QOS_CLI_MAX_PROMPT_LENGTH   32
#define QOS_CLI_PRI_MAP_MODE        "PriMap"
#define QOS_CLI_CLS_MAP_MODE        "ClsMap"
#define QOS_CLI_METER_MODE          "Meter"
#define QOS_CLI_PLY_MAP_MODE        "PlyMap"
#define QOS_CLI_QTEMP_MODE          "QTemp"


#define CLI_GET_PRI_MAP_ID()         CliGetModeInfo()
#define CLI_GET_CLS_MAP_ID()         CliGetModeInfo()
#define CLI_GET_METER_ID()           CliGetModeInfo()
#define CLI_GET_PLY_MAP_ID()         CliGetModeInfo()
#define CLI_GET_QTEMP_ID()           CliGetModeInfo()

#define CLI_SET_PRI_MAP_ID(Id)       CliSetModeInfo(Id)
#define CLI_SET_CLS_MAP_ID(Id)       CliSetModeInfo(Id)
#define CLI_SET_METER_ID(Id)         CliSetModeInfo(Id)
#define CLI_SET_PLY_MAP_ID(Id)       CliSetModeInfo(Id)
#define CLI_SET_QTEMP_ID(Id)         CliSetModeInfo(Id)



#define QOS_CLI_IN_PRI_TYPE_VALN_PRI     0
#define QOS_CLI_IN_PRI_TYPE_IP_DSCP      1
#define QOS_CLI_IN_PRI_TYPE_VLAN_ID      5
#define QOS_CLI_IN_PRI_TYPE_SRC_MAC      6
#define QOS_CLI_IN_PRI_TYPE_DEST_MAC     7

#define QOS_CLI_CLS_COLOR_NONE           0
#define QOS_CLI_CLS_COLOR_GREEN          1
#define QOS_CLI_CLS_COLOR_YELLOW         2
#define QOS_CLI_CLS_COLOR_RED            3

#define QOS_CLI_COLOR_MODE_AWARE         1
#define QOS_CLI_COLOR_MODE_BLIND         2

#define QOS_CLI_METER_TYPE_STB           1
#define QOS_CLI_METER_TYPE_AVGRATE       2   
#define QOS_CLI_METER_TYPE_SRTCM         3
#define QOS_CLI_METER_TYPE_TRTCM         4
#define QOS_CLI_METER_TYPE_TSWTCM        5   
#define QOS_CLI_METER_TYPE_MEFDECOUPLED  6
#define QOS_CLI_METER_TYPE_MEFCOUPLED    7

#define QOS_CLI_IN_CONF_ACT_DROP             0x0001
#define QOS_CLI_IN_CONF_ACT_PORT             0x0002
#define QOS_CLI_IN_CONF_ACT_IP_TOS           0x0004
#define QOS_CLI_IN_CONF_ACT_IP_DSCP          0x0008
#define QOS_CLI_IN_CONF_ACT_VLAN_PRI         0x0010
#define QOS_CLI_IN_CONF_ACT_VLAN_DE          0x0020
#define QOS_CLI_IN_CONF_ACT_INNER_VLAN_PRI   0x0040
#define QOS_CLI_IN_CONF_ACT_MPLS_EXP         0x0080

#define QOS_CLI_IN_EXC_ACT_DROP              0x0001
#define QOS_CLI_IN_EXC_ACT_IP_TOS            0x0002
#define QOS_CLI_IN_EXC_ACT_IP_DSCP           0x0004
#define QOS_CLI_IN_EXC_ACT_VLAN_PRI          0x0008
#define QOS_CLI_IN_EXC_ACT_VLAN_DE           0x0010
#define QOS_CLI_IN_EXC_INNER_VLAN_PRI        0x0020
#define QOS_CLI_IN_EXC_ACT_MPLS_EXP          0x0040

#define QOS_CLI_OUT_ACT_DROP                 0x0001
#define QOS_CLI_OUT_ACT_IP_TOS               0x0002
#define QOS_CLI_OUT_ACT_IP_DSCP              0x0004
#define QOS_CLI_OUT_ACT_VLAN_PRI             0x0008
#define QOS_CLI_OUT_ACT_VLAN_DE              0x0010
#define QOS_CLI_OUT_ACT_INNER_VLAN_PRI       0x0020
#define QOS_CLI_OUT_ACT_MPLS_EXP             0x0040

#define QOS_CLI_PLY_PHB_TYPE_NONE            0
#define QOS_CLI_PLY_PHB_TYPE_VLAN_PRI        1
#define QOS_CLI_PLY_PHB_TYPE_IP_TOS          2
#define QOS_CLI_PLY_PHB_TYPE_IP_DSCP         3
#define QOS_CLI_PLY_PHB_TYPE_MPLS_EXP        4


/* Q Map Table */
#define QOS_CLI_QMAP_PRI_TYPE_NONE           0
#define QOS_CLI_QMAP_PRI_TYPE_VLAN_PRI       1
#define QOS_CLI_QMAP_PRI_TYPE_IP_TOS         2  
#define QOS_CLI_QMAP_PRI_TYPE_IP_DSCP        3
#define QOS_CLI_QMAP_PRI_TYPE_VLAN_ID        4
#define QOS_CLI_QMAP_PRI_TYPE_SRC_MAC        5
#define QOS_CLI_QMAP_PRI_TYPE_DEST_MAC       6


#define QOS_CLI_MAX_MAC_STRING_SIZE        21

#define QOS_CLI_SCHED_ALGO_STRICT 0  
#define QOS_CLI_SCHED_ALGO_WRR    1


/* System Controls */
enum {

/* 1*/  CLI_QOS_SET_SYS_CNTRL_DISABLE  = 1,
/* 2*/  CLI_QOS_SET_SYS_CNTRL_ENABLE ,
/* 3*/  CLI_QOS_SET_SYS_STATUS ,
/* 4*/  CLI_QOS_ADD_PRI_MAP,
/* 5*/  CLI_QOS_SET_PRI_MAP_PARAMS,
/* 6*/  CLI_QOS_SHOW_PRI_MAP,
/* 7*/  CLI_QOS_DEL_PRI_MAP, 
/* 8*/  CLI_QOS_NO_SET_PRI_MAP_PARAMS,
/* 9*/  CLI_QOS_ADD_PLY_MAP,
/* 10*/ CLI_QOS_DEL_PLY_MAP,
/* 11*/ CLI_QOS_SHOW_GLB_INFO,
/* 12*/ CLI_QOS_ADD_QMAP,
/* 13*/ CLI_QOS_DEFAULT_QMAP,
/* 14*/ CLI_QOS_SHOW_QMAP,
/* 15*/ CLI_QOS_MATCH_PRI,
/* 16*/ CLI_QOS_NO_MATCH_PRI,
/* 17*/ CLI_QOS_OVERRIDE_PRIORITY,
/* 18*/ CLI_QOS_NO_OVERRIDE_PRIORITY,
/* 19*/ CLI_QOS_TAGIFBOTH_MATCH_PRI,
/* 20*/ CLI_QOS_SCHED_ALGO,
/* 21*/ CLI_QOS_SET_PRIORITY_MAP_PARAMS,
/* 22*/ CLI_QOS_SHOW_PORT_CONFIG,
/* 23*/ CLI_QOS_SHOW_SCHED_ALGO,
/* 24*/ CLI_QOS_SHOW_PORT_OVERRIDE_CONFIG
};


enum {
 
 /* Common ERROR CODE */
 QOS_CLI_ERR_MODULE_SHUTDOW  = 1,
 QOS_CLI_ERR_INVALID_SYS_CONTROL,
 QOS_CLI_ERR_INVALID_SYS_STATUS,
 QOS_CLI_ERR_INVALID_TRC_FLAG,
 QOS_CLI_ERR_STATUS_ACTIVE,
 QOS_CLI_ERR_INVALID_IF_INDEX,
 QOS_CLI_ERR_INVALID_VLAN_INDEX,
 QOS_CLI_ERR_INVALID_VLAN_PRI,
 QOS_CLI_ERR_INVALID_IP_TOS,
 QOS_CLI_ERR_INVALID_IP_DSCP,
 QOS_CLI_ERR_INVALID_MPLS_EXP,
 QOS_CLI_ERR_INVALID_VLAN_DE,
 QOS_CLI_ERR_INVALID_PRI_TYPE,
 QOS_CLI_ERR_TBL_NAME_INVALID,
 QOS_CLI_ERR_INVALID_CIR,
 QOS_CLI_ERR_INVALID_CBS,
 QOS_CLI_ERR_INVALID_EIR,
 QOS_CLI_ERR_INVALID_EBS,
 QOS_CLI_ERR_WEIGHT,
 QOS_CLI_ERR_PRIORITY,
 QOS_CLI_ERR_HL_INVALID,
 QOS_CLI_ERR_QMAP_PRI_RANGE,
 QOS_CLI_ERR_DEF_ENTRY,
 /* PriorityMapTbl ERROR CODE */
 QOS_CLI_ERR_PRI_MAP_RANGE,
 QOS_CLI_ERR_PRI_MAP_NO_ENTRY,
 QOS_CLI_ERR_INVALID_INNER_PRI,
 QOS_CLI_ERR_PRI_MAP_MAX_ENTRY,
 QOS_CLI_ERR_PRI_MAP_RFE,
 QOS_CLI_ERR_PRI_MAP_DUP_ENTRY,
 /* ClassMapTbl ERROR CODE */
 QOS_CLI_ERR_FILTER_NOT_FORQOS,
 QOS_CLI_ERR_CLS_MAP_RANGE,
 QOS_CLI_ERR_CLS_MAP_MAX_ENTRY,
 QOS_CLI_ERR_CLS_MAP_RFE,
 QOS_CLI_ERR_CLS_MAP_NO_ENTRY,
 QOS_CLI_ERR_L2_L3_OR_PRI,
 QOS_CLI_ERR_CLS_MAP_INVALID_CLS,
 QOS_CLI_ERR_L2_FILTER_ID_INVALID,
 QOS_CLI_ERR_L2_FILTER_ID_NOT_ACTIVE,
 QOS_CLI_ERR_L3_FILTER_ID_INVALID,
 QOS_CLI_ERR_L3_FILTER_ID_NOT_ACTIVE,
 QOS_CLI_ERR_PRI_MAP_ID_NOT_ACTIVE,
 /* ClassToPriotity Tbl ERROR CODE */
 QOS_CLI_ERR_CLS2PRI_MAP_MAX_ENTRY,
 QOS_CLI_ERR_INVALID_REGEN_PRI,
 QOS_CLI_ERR_CLS_NOT_CREATED,
 QOS_CLI_ERR_CLS2PRI_MAP_NO_ENTRY,
 /* MeterTbl ERROR CODE */
 QOS_CLI_ERR_METER_INVALID_TYPE,
 QOS_CLI_ERR_METER_INVALID_INTERVAL,
 QOS_CLI_ERR_METER_INVALID_COLOR_MODE,
 QOS_CLI_ERR_METER_RANGE,
 QOS_CLI_ERR_METER_MAX_ENTRY,
 QOS_CLI_ERR_METER_RFE,
 QOS_CLI_ERR_METER_MANDATORY,
 QOS_CLI_ERR_METER_NO_ENTRY,
 QOS_CLI_ERR_METER_NOT_ACTIVE,
 QOS_CLI_ERR_METER_INVALID_PARAM,
 /* PolicyMapTbl ERROR CODE */
 QOS_CLI_ERR_PHB_TYPE_INVALID,
 QOS_CLI_ERR_PRE_COLOR_INVALID,
 QOS_CLI_ERR_PLY_MAP_RANGE,
 QOS_CLI_ERR_PLY_MAP_MAX_ENTRY,
 QOS_CLI_ERR_PLY_MAP_NO_ENTRY,
 QOS_CLI_ERR_ACT_FLG_LEN_INVALID,
 QOS_CLI_ERR_ACT_FLG_INVALID,
 QOS_CLI_ERR_DROP_CLUB,
 QOS_CLI_ERR_NONE_CLUB,
 QOS_CLI_ERR_TOS_DSCP,
 QOS_CLI_ERR_VLAN_MPLS,
 QOS_CLI_ERR_NO_FILTER,
 QOS_CLI_ERR_PLY_MULTIMAP,
 /* QTemp Tbl ERROR CODE */
 QOS_CLI_ERR_QTEMP_DROP_TYPE,
 QOS_CLI_ERR_QTEMP_DROP_ALGO,
 QOS_CLI_ERR_QTEMP_QSIZE_INVALID,
 QOS_CLI_ERR_QTEMP_RANGE,
 QOS_CLI_ERR_QTEMP_MAX_ENTRY,
 QOS_CLI_ERR_QTEMP_RFE,
 QOS_CLI_ERR_QTEMP_NO_RDCFG,
 QOS_CLI_ERR_QTEMP_NOT_CREATION,
 QOS_CLI_ERR_QTEMP_NOT_ACTIVE,
 /* REDCfgTbl ERROR CODE */
 QOS_CLI_ERR_RDCFG_MIN_AVG_TH,
 QOS_CLI_ERR_RDCFG_MAX_AVG_TH,
 QOS_CLI_ERR_RDCFG_MAX_PKT,
 QOS_CLI_ERR_RDCFG_MAX_PROB,
 QOS_CLI_ERR_RDCFG_EXP_WEIGHT,
 QOS_CLI_ERR_RDCFG_DP_RANGE,
 QOS_CLI_ERR_RDCFG_MAX_ENTRY,
 QOS_CLI_ERR_RDCFG_REF,
 QOS_CLI_ERR_RDCFG_NO_ENTRY,
 /* Shape Tbl ERROR CODE */
 QOS_CLI_ERR_SHAPE_NOT_ACTIVE,
 QOS_CLI_ERR_SHAPE_NOT_CREATED,
 QOS_CLI_ERR_SHAPE_TEMP_RANGE,
 QOS_CLI_ERR_SHAPE_MAX_ENTRY,
 QOS_CLI_ERR_SHAPE_TEMP_RFE,
 /* Q Tbl ERROR CODE */
 QOS_CLI_ERR_Q_MAX_ENTRY,
 QOS_CLI_ERR_Q_RANGE,
 QOS_CLI_ERR_Q_RFE,
 QOS_CLI_ERR_Q_NOT_ACTIVE,
 QOS_CLI_ERR_Q_NO_ENTRY,
 QOS_CLI_ERR_Q_MANDATORY,
 /* QMAP Tbl ERROR CODE */
 QOS_CLI_ERR_QMAP_MAX_ENTRY,
 QOS_CLI_ERR_QMAP_NO_ENTRY,
 /* Sched Tbl ERROR CODE */
 QOS_CLI_ERR_SCHED_NO_ENTRY,
 QOS_CLI_ERR_SCHED_RANGE,
 QOS_CLI_ERR_SCHED_NOT_ACTIVE,
 QOS_CLI_ERR_SCHED_INVALID_ALGO,
 QOS_CLI_ERR_SCHED_MAX_ENTRY,
 QOS_CLI_ERR_SCHED_RFE,
 /* HS Tbl ERROR CODE */
 QOS_CLI_ERR_HS_SELF_REF,
 QOS_CLI_ERR_HS_MAX_ENTRY,
 QOS_CLI_ERR_HL_NOT_MATCH,
 QOS_CLI_ERR_HIERARCHY_NO_ENTRY,
 QOS_CLI_ERR_MAC_ADDRESS_NOT_PRESENT,
 QOS_CLI_ERR_VLAN_NOT_PRESENT
};

#ifdef __QOS_CLI_C__

CONST CHR1 *gapi1QoSCliErrString [] = {
    NULL,
    /* Common ERROR CODE */
    "QoS Should Be Started Before Accessing It !.\r\n",
    "Invalid System Control.\r\n",
    "Invalid System Status.\r\n",
    "Invalid Trace Flag.\r\n",
    "Can not change the value in ACTIVE State.\r\n",
    "Invalid IfIndex.\r\n",
    "Invalid Vlan Id.\r\n",
    "Invalid Vlan Priority.\r\n",
    "Invalid IP ToS.\r\n",
    "Invalid IP DSCP.\r\n",
    "Invalid MPLS Exp.\r\n",
    "Invalid VLAN DE.\r\n",
    "Invalid Priority Type.\r\n",
    "Table name Invalid char. Valid Char = (A-Z, a-z, 0-9, _, - ).\r\n",
    "Invalid CIR.\r\n",
    "Invalid CBS.\r\n",
    "Invalid EIR.\r\n",
    "Invalid EBS.\r\n",
    "Invalid Weight.\r\n",
    "Invalid Priority.\r\n",
    "Invalid Hierarchy Level.\r\n",
    "Priority value is out of Range.\r\n",
    "Default Table Entry can not be deleted.\r\n",
    /* PriorityMapTbl ERROR CODE */
    "Priority Map Id is out of Range.\r\n",
    "Priority Map Entry not Created.\r\n",
    "Invalid Inner Priority.\r\n",
    "Max Priority Map Entries are Configured.\r\n",
    "Priority Map Entry referenced by Class Map Table Entry.\r\n",
    "Priority Map values are Duplicate.\r\n",
    /* ClassMapTbl ERROR CODE */
    "Filter is not for QOS.\r\n",
    "Class Map Id out of Range.\r\n",
    "Max Class Map Entries are Configured.\r\n",
    "Class Map Entry referenced by Policy Map Table Entry.\r\n",
    "Class Map Entry not Created.\r\n",
    "Either L2 and/or L3 Filters or PriMapId Should be Configured.\r\n",
    "CLASS is out of Range.\r\n",
    "Invalid L2 Filter Id.\r\n",
    "L2 Filter Id Not Active.\r\n",
    "Invalid L3 Filter Id.\r\n",
    "L3 Filter Id Not Active.\r\n",
    "Priority Map Id Not Active.\r\n",
    /* ClassToPriotity Tbl ERROR CODE */
    "Class to Priority Map Max Entries Configured.\r\n",
    "ClassToPriotity Regen Priority value is out of range.\r\n",
    "Class Not yet Created.\r\n",
    "No Class To Priority Map Entry.\r\n",
    /* MeterTbl ERROR CODE */
    "Invalid Meter Type.\r\n",
    "Invalid Interval value.\r\n",
    "Invalid Color Mode.\r\n",
    "Meter Table Index out of Range.\r\n",
    "Meter Max Entries Configured.\r\n",
    "Meter Table Entry is referenced by  Policy Entry or other Meter.\r\n",
    "Meter Mandatory Fields are not set or Invalid fields are set.\r\n",
    "Meter Table Entry not created.\r\n",
    "Meter Table Entry not Active.\r\n",
    "Given meter parameter not valid for the current meter Type.\r\n",
    /* PolicyMapTbl ERROR CODE */
    "PHB Type Invalid.\r\n",
    "Pre Color Invalid.\r\n",
    "Policy map Table Index out of Range.\r\n",
    "Policy map max Entries Configured.\r\n",
    "Policy map entry not created.\r\n",
    "Policy map Action flag length invalid.\r\n",
    "Policy map Action value and Action flag Combination invalid.\r\n",
    "Policy map Action 'Drop' can not club with other options.\r\n",
    "Policy map Action 'None' can not club with other options.\r\n",
    "Action flag value 'TOS and DSCP' mutually exclusive\r\n",
    "Action flag value 'VLAN and MPLS Flags' mutually exclusive.\r\n",
    "Filter not configured for the CLASS.\r\n",
    "CLASS is already Mapped with another PolicyMapEntry.\r\n",
    /* QTemp Tbl ERROR CODE */
    "Q Template Drop Type value is out of range.\r\n",
    "Q Template Drop Algo value is out of range.\r\n",
    "Q Template Size is out of range.\r\n",
    "Q Template Id is out of range.\r\n",
    "Max No of Q Template Table Entries are Configured.\r\n",
    "This Entry is referenced by Q Table Entry.\r\n",
    "RD Tbl Entry for this QTempId is not Configured.\r\n",
    "Queue Template is not created.\r\n",
    "Queue Template entry is not active.\r\n",
    /* REDCfgTbl ERROR CODE */
    "RD Cfg MinAvgThresh  value is out of range.\r\n",
    "RD Cfg MaxAvgThresh  value is out of range.\r\n",
    "RD Cfg MaxPktSize value is out of range.\r\n",
    "RD Cfg Max Drop Probability value is out of range.\r\n",
    "RD Cfg Exponential Weight value is out of range.\r\n",
    "RD Cfg DP value is out of range.\r\n",
    "Max No of RD Cfg Table Entries are Configured.\r\n",
    "This Entry is referenced by QTemplate Table Entry.\r\n",
    "RD Cfg Entry is not yet created.\r\n",
    /* Shape Tbl ERROR CODE */
    "Shape Template entry is not active.\r\n",
    "Shape Template not created.\r\n",
    "Shape Template Index is out of Range.\r\n",
    "Max No of Shape Table Entries are Configured.\r\n",
    "This Entry is referenced by Q Table Entry or Scheduler Table Entry.\r\n",
    /* Q Tbl ERROR CODE */
    "Max No of Queue Table Entries are Configured. \r\n",
    "Queue Id is out of Range.\r\n",
    "Queue Entry is referenced by Hierarchy Table Entry .\r\n",
    "Queue entry is not active.\r\n",
    "Queue entry not created.\r\n",
    "Queue entry mandatory params not set.\r\n",
    /* QMAP Tbl ERROR CODE */
    "Max No of Queue map Entries are Configured.\r\n",
    "Queue map entry not created.\r\n",
    /* Sched Tbl ERROR CODE */
    "Scheduler entry not created.\r\n",
    "Scheduler Index is out of Range.\r\n",
    "Scheduler entry is not active.\r\n",
    "Invalid Scheduler Algorithm.\r\n",
    "Max No of Scheduler Table Entries are Configured.\r\n",
    "This Entry is referenced by Q or HierSched Table Entry.\r\n",
    /* HS Tbl ERROR CODE */
    "Scheduler Self Reference.\r\n",
    "Max No of Hierarchy Scheduler Entries are Configured.\r\n",
    "Hierarchy Level Not Matched (SchedTbl - HierSchedTbl).\r\n",
    "Hierarchy entry not created.\r\n",
    "Mac Entry Not present in the FDB Table.\r\n",
    "Vlan is not Active.\r\n",
    "\r\n"

};
#endif


/*****************************************************************************/
/* FileName : qoscli.c  functions prototype                                  */
/*****************************************************************************/

PUBLIC INT4 
cli_process_qos_cmd PROTO ((tCliHandle CliHandle, UINT4, ...));

INT4 
QoSCliSetSysControl PROTO ((tCliHandle CliHandle, INT4 i4SysControl));

INT4 
QoSCliSetStatus PROTO ((tCliHandle CliHandle, INT4 i4SysStatus));

INT4 
QoSCliShowGlobalInfo PROTO ((tCliHandle CliHandle));
    
/*****************************************************************************/
/*                  PRIORITY MAP TABLE FUNCTIONS                             */
/*****************************************************************************/

INT4
QoSCliAddPriMapEntry PROTO ((tCliHandle CliHandle, UINT4 u4Idx));

INT4
QoSCliGetPriMapIndexFromName PROTO ((tSNMP_OCTET_STRING_TYPE *pInOctStrName,
                                     UINT4 *pu4Idx, UINT4 *pu4Status));
PUBLIC INT1
QoSPriMapPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1PromptStr));

INT4
QoSCliSetPriMapParams PROTO ((tCliHandle CliHandle, UINT4 u4IfNum, 
                              UINT1 u1InPriType,UINT1 u1InPri, 
                              UINT1 u1TrafCls));
INT4
QoSCliNoSetPriMapParams PROTO ((tCliHandle CliHandle));

INT4
QoSCliShowPriMapEntry PROTO ((tCliHandle CliHandle, UINT4 u4Idx));
INT4
QoSCliUtlDisplayPriMapEntry PROTO ((tCliHandle CliHandle, UINT4 u4Idx));
INT4 
QoSCliDelPriMapEntry PROTO ((tCliHandle CliHandle, UINT4 u4Idx));

VOID
QoSCliRSPriMapTblEntry PROTO ((UINT4 u4Idx, UINT4 u4CurIfNum,
                               UINT4 u4CurInPriType, UINT4 u4CurInPri,
                               UINT4 u4CurRegenPri, 
                               INT4 i4RowStatus));
VOID
QoSCliRSNoSetPriMapParams PROTO ((UINT4 u4Idx, UINT4 u4CurIfNum,
                                  UINT4 u4CurInRegenPri, 
                                  INT4 i4RowStatus));

INT4
QoSCliSetPriorityMapParams PROTO ((tCliHandle CliHandle, INT4 i4InType, tMacAddr MacAddr, UINT1 u1RegenPriorty,UINT4 u4VlanId));


/*****************************************************************************/
/*                        QMAP TABLE FUNCTIONS                               */
/*****************************************************************************/
VOID
QoSCliRSQMapEntry PROTO ((INT4 i4MapType, 
                          UINT4 u4PriVal, UINT4 u4CurQIdx, INT4 i4RowStatus));
INT4
QoSCliAddQMapEntry PROTO ((tCliHandle CliHandle, INT4 i4MapType,
                           UINT4 u4MapVal, UINT4 u4QIdx));

INT4
QoSCliDefaultQMapEntry PROTO ((tCliHandle CliHandle, INT4 i4MapType,
                        UINT4 u4MapVal));
INT4
QoSCliUtlDisplayQMapEntry PROTO ((tCliHandle CliHandle, 
                                  UINT4 u4QIdx));
INT4
QoSCliShowQMapEntry PROTO ((tCliHandle CliHandle, INT4 i4IfIdx));

INT4
QoSCliSetSchedulingPloicy PROTO ((tCliHandle CliHandle, INT4));

INT4
QoSCliShowSchedulingPolicy PROTO ((tCliHandle CliHandle));

INT4
QosCliPortPriortyConfig PROTO ((tCliHandle CliHandle,UINT4,INT4));

INT4
QosCliPortNoPriortyConfig PROTO ((tCliHandle CliHandle,UINT4,INT4));

INT4
QosCliPortOverrideConfig PROTO ((tCliHandle CliHandle,UINT4,INT4));

INT4
QosCliPortNoOverrideConfig PROTO ((tCliHandle CliHandle,UINT4,INT4));

INT4
QosCliPortVlanIPPriorityConfig PROTO ((tCliHandle CliHandle,UINT4,INT4));

INT4
QosCliShowPortConfig PROTO ((tCliHandle CliHandle,UINT4));

INT4
QosCliShowPortOverrideConfig PROTO ((tCliHandle CliHandle, UINT4 u4PortId));

INT4
QoSCliSetSchedulingPolicy PROTO ((tCliHandle CliHandle, INT4 i4SchedAlgo));

INT4
QoSCliShowSchedulingPolicy PROTO ((tCliHandle CliHandle));
    
    
    
/*****************************************************************************/
/*                        SHOW RUNNING CONFIG FUNCTIONS                      */
/*****************************************************************************/

INT4 QoSShowRunningConfig PROTO ((tCliHandle CliHandle));
INT4 QoSCliShowRunningConfigGlobal PROTO ((tCliHandle CliHandle));
INT4 QoSCliShowRunningConfigTables PROTO ((tCliHandle CliHandle));
INT4 QoSCliShowRunningConfigGlobal PROTO ((tCliHandle CliHandle));
INT4 QoSPriorityMapShowRunningConfig PROTO ((tCliHandle CliHandle));
INT4 QoSPolicyMapShowRunningConfig  PROTO ((tCliHandle CliHandle));
INT4 QoSQueueMapShowRunningConfig PROTO ((tCliHandle CliHandle));
VOID QosShowRunningConfigInterfaceDetails PROTO ((tCliHandle CliHandle));

INT1 DiffsrvGetClassMapPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1PromptStr));

INT1 DiffsrvGetPolicyMapPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1PromptStr));

INT1 DiffsrvGetPolicyClassMapPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1PromptStr));

#endif /* __QOS_CLI_H__ */


    
