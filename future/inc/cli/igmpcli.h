/* $Id: igmpcli.h,v 1.29 2017/02/06 10:45:29 siva Exp $ */
 
#ifndef __IGMPCLI_H__
#define __IGMPCLI_H__

#include "lr.h"
#include "cli.h"
/* IGMP CLI command constants. To add a new command ,a new definition needs to
 * be added to the list.
 */
enum
{
    IGMP_CLI_STATUS = 1,
    IGMP_CLI_IFACE_STATUS,
    IGMP_CLI_FAST_LEAVE,
    IGMP_CLI_VERSION,
    IGMP_CLI_NO_VERSION,
    IGMP_CLI_QRY_INTRVL,
    IGMP_CLI_QRY_MAXRESPTIME,
    IGMP_CLI_ROBUSTNESS,
    IGMP_CLI_LAST_MBRQRYINTRL,
    IGMP_CLI_STATIC_MEMBERSHIP,
    IGMP_CLI_NO_STATIC_MEMBERSHIP,
    IGMP_CLI_DEL_INTERFACE,
    IGMP_CLI_CHANNEL_TRACK,
    IGMP_CLI_RATE_LIMIT,
    IGMP_CLI_NO_RATE_LIMIT,
    IGMP_CLI_IF_RATE_LIMIT,
    IGMP_CLI_IF_NO_RATE_LIMIT,
    IGMP_CLI_SHOW_GBL_CONFIG,
    IGMP_CLI_SHOW_INTF_CONFIG,
    IGMP_CLI_SHOW_GROUPS,
    IGMP_CLI_SHOW_SOURCES,
    IGMP_CLI_SHOW_GRP_CHANNEL,
    IGMP_CLI_SHOW_MEMBERSHIP,
    IGMP_CLI_SHOW_STATS,
    IGMP_CLI_CLEAR_STATS,
    IGMP_CLI_TRACE,
    IGMP_CLI_DEBUG,
    IGMP_CLI_GLOBAL_LIMIT,
    IGMP_CLI_INTF_LIMIT,
    IGMP_CLI_GROUP_LIST_CONFIG,
    IGMP_CLI_SHOW_LIST_CONFIG,
    IGMP_CLI_SSM_MAP_STATUS,
    IGMP_CLI_SSM_MAP_CONFIG,
    IGMP_CLI_SHOW_SSM_MAP,
    IGMP_CLI_MAX_COMMANDS
};
#define IGMP_CLI_ZERO   0


#define IGMP_CFA_INTF_NAME                64

#define INT_CONF_ALL                      65535
#define IGMP_MAX_ARGS                      5

#define IGMP_DISPLAY_TRACK      1
#define IGMP_DISPLAY_ALL        2

#define   CLI_IGMP_V1                               1
#define   CLI_IGMP_V2                               2
#define   CLI_IGMP_V3                               3
#define CLI_IGMP_TRACE_DIS_FLAG                      0x80000000


INT1 cli_process_igmp_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);
INT1 cli_process_igmp_test_cmd (tCliHandle CliHandle, ...);

enum {
    CLI_IGMP_UNKNOWN_ERR = 1,
    CLI_IGMP_INTERFACE_NOT_FOUND,
    CLI_IGMP_VERSION_TO_3,
    CLI_IGMP_PROXY_UPSTREAM,
    CLI_IGMP_QUERY_INTERVAL_ERR,
    CLI_IGMP_MAX_RESP_ERR,
    CLI_IGMPV1_QUERY_INTERVAL_ERR,
    CLI_IGMPV1_LAST_MEM_QUERY_INTVL_ERR,
    CLI_IGMP_NO_GROUP_ERR,
    CLI_IGMP_MULT_ADDR_ERR,
    CLI_IGMP_GLOBAL_STATUS_DISABLED,
    CLI_IGMP_INVALID_GRP_LIST_ID,
    CLI_IGMP_INVALID_GRP_LIST_IP_ADDRESS,
    CLI_IGMP_GRP_LIST_RECORD_EXISTS,
    CLI_IGMP_NO_GRP_LIST_RECORD_EXISTS,
    CLI_IGMP_LIMIT_NOT_IN_RANGE,
    CLI_IGMP_INVALID_GRP_LIST_IP_MASK,
    CLI_IGMP_GRP_LIST_SUMMARY_RECORD_EXISTS,
    CLI_IGMP_INVALID_GRP_ADDRESS,
    CLI_IGMP_TRACK_FOR_V3,
    CLI_IGMP_MAX_GROUPS,
    CLI_IGMP_LEARNED_DYNAMIC,
    CLI_IGMP_GRP_AND_SRC_ALREADY_LEARNED,
    CLI_IGMP_GRP_ALREADY_LEARNED,
    CLI_IGMP_DISABLE_TRACKING,
    CLI_IGMP_NOT_ENABLE, 
    CLI_IGMP_NOT_DISABLE, 
    CLI_IGMP_WRONG_INPUTS,
    CLI_IGMP_GRP_LIST_MEM_FAIL, 
    CLI_IGMP_GRP_LIST_RB_ADD_FAIL, 
    CLI_IGMP_INTERFACE_EXIST,
    CLI_IGMP_ENABLE_SOCK_FAILURE,
    CLI_IGMP_DISABLE_SOCK_FAILURE,
    CLI_IGMP_GRP_ALREADY_EXIST,
    CLI_IGMP_QUERY_INTERVAL_RANGE_ERR,
    CLI_IGMP_ROWSTATUS_OUT_OF_RANGE,
    CLI_IGMP_VERSION_ERR,
    CLI_IGMP_MAX_RESP_RANGE_ERR,
    CLI_IGMP_ROBUSTNESS_RANGE_ERR,
    CLI_IGMP_LAST_MEM_QUERY_INTVL_RANGE_ERR,
    CLI_IGMP_MULTICAST_RANGE_SSM,
    CLI_IGMP_INTERFACE_MEM_EXHAUST,
    CLI_IGMP_SSM_RECORD_TYPE,
    CLI_IGMP_GRP_LIMIT_LESS_THAN_PREV_LIMIT,
 CLI_IGMP_INVALID_SOURCE,
    CLI_IGMP_SSMMAP_NOT_ENABLE,
    CLI_IGMP_SSMMAP_EXISTS,
    CLI_IGMP_SSMMAP_NOT_EXISTS,
    CLI_IGMP_SSMMAP_OVERLAPS,
    CLI_IGMP_SSMMAP_SUPERSET,
    CLI_IGMP_SSMMAP_SRC_LIST_FULL,
    CLI_IGMP_SSMMAP_RB_ADD_FAIL,
    CLI_IGMP_SSMMAP_RB_REMOVE_FAIL,
    CLI_IGMP_SSMMAP_STATIC_GRP_ASSOC,
    CLI_IGMP_SSMMAP_MCAST_SSM_NO_STAR_G,
    CLI_IGMP_SSMMAP_MCAST_SSM_NOT_ENABLE,
    CLI_IGMP_SSMMAP_IGMP_NOT_ENABLE,
    CLI_IGMP_SSMMAP_INCONSIS_RANGE,
    CLI_IGMP_SSMMAP_MAX_LIMIT_RCHD,
    CLI_IGMP_INTF_GRP_SRC_FOUND,
    CLI_IGMP_INVALID_ADDRESS_RANGE,
 CLI_IGMP_NO_GRP_AND_SRC,
    CLI_IGMP_MAX_ERR 
};

#ifdef __IGMPCLI_C__
CONST CHR1 *IgmpCliErrString [] = {
    NULL,
    "Unknown Error\r\n",
    "Interface Entry not found \r\n",
    "Configure the version to v3 \r\n",
    "Interface is configured as IGMP Proxy upstream interface \r\n",
    "Query Interval must be greater than one tenth of"
    " Max Response Time \r\n",
    "One tenth of Max Response Time must be less than Query Interval\r\n",
    "Query Interval configuration is not allowed for IGMPv1 interface \r\n",
    "Last Member Query Interval configuration is not allowed for IGMPv1"
    " interface \r\n",
    "Group Address does not exist \r\n",
    "Configured Multicast address could not be assigned as it falls in reserved address range \r\n",
    "Igmp Global Status is disabled \r\n",
    "Invalid Group List Id \r\n",
    "Group Ip Address is not in multicast address range\r\n",
    "GroupList Record already exists  \r\n",
    "No GroupList Record exists with this inputs \r\n",
    "GroupLimit is not in valid Range \r\n",
    "Invalid mask for Group address \r\n",
    "Given group range is already present in the exempt list\r\n",
    "Invalid Group address \r\n",
    "Explicit tracking can be enabled for IGMPv3 interface only\r\n",
    "Max groups exceeded for configure limit\r\n",
    "Group membership Dynamically learned\r\n",
    "Configured IGMP group and source is already learned \r\n",
    "Configured IGMP group is already learned\r\n",
    "Explicit tracking should be disabled for changing the interface version to V2/V1\r\n",
    "Enabling IGMP failed\r\n",
    "Disabling IGMP failed\r\n",
    "IGMP invalid inputs\r\n",
    "Memory allocation for GroupList Record Node failed\r\n",
    "RB-tree addition of GroupList Record Node failed\r\n",
    "IGMP Interface already exists\r\n",
    "Enabling multicast socket option failed\r\n",
    "Disabling multicast socket option failed\r\n",
    "IGMP group already configured\r\n",
    "Query interval value is out of range\r\n",
    "Row status value is out of range\r\n",
    "IGMP version should be 1, 2 or 3\r\n",
    "Max response value is out of range\r\n",
    "Robustness value is out of range\r\n",
    "Last member query interval value is out of range\r\n",
    "Igmp group address should not be in SSM range if the configured version is V1/V2\r\n",
    "Maximum IGMP interface limit is reached.Cannot enable IGMP in this interface\r\n",
    "(*,G) Static Group creation is not allowed for SSM range without already configured IGMP SSM Mapping\r\n",
    "Group limit is less than already configured limit\r\n",
 "Invalid Source IP address\r\n", 
    "IGMP SSM Mapping status is disabled\r\n",
    "IGMP SSM Mapping for the given range of group address to the source address already exists\r\n",
    "IGMP SSM Mapping for the given range of group address to the source address does not exist\r\n",
    "IGMP SSM Mapping for the given range of group address overlaps with already configured range\r\n",
    "IGMP SSM Mapping for the given range of group address is a superset of already configured range\r\n",
    "IGMP SSM Mapping Source-List is full for the given range of group address\r\n",
    "RB-tree addition of IGMP SSM Mapping Node failed\r\n",
    "RB-tree removal of IGMP SSM Mapping Node failed\r\n",
    "IGMP SSM Mapped Sources are not associated with configured Static Group\r\n",
    "(*,G) Static Group creation is not allowed without already configured IGMP SSM Mapping\r\n",
    "Enable IGMP SSM Map feature for (*,G) Static Group creation\r\n", 
    "Enable IGMP Global status for (*,G) Static Group creation\r\n", 
    "Starting multicast group range is greater than ending multicast group range\r\n",
    "Maximum limit for IGMP SSM Mapping is already reached.\r\n",
    "Configured Igmp group and source entry should be removed to downgrade the interface version to V1/V2\r\n",
    "Configured Multicast address range could not be assigned as it falls in reserved address range\r\n",
    "Configured Igmp group and source entry does not exists\r\n",
    "\r\n"
};
#endif



#endif /* __IGMPCLI_H__ */

