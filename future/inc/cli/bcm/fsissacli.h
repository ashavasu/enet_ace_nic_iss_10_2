/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsissacli.h,v 1.8 2015/07/15 11:04:07 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IssAclRateCtrlIndex[13];
extern UINT4 IssAclRateCtrlDLFLimitValue[13];
extern UINT4 IssAclRateCtrlBCASTLimitValue[13];
extern UINT4 IssAclRateCtrlMCASTLimitValue[13];
extern UINT4 IssAclRateCtrlPortRateLimit[13];
extern UINT4 IssAclRateCtrlPortBurstSize[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIssAclRateCtrlDLFLimitValue(i4IssAclRateCtrlIndex ,i4SetValIssAclRateCtrlDLFLimitValue)   \
        nmhSetCmn(IssAclRateCtrlDLFLimitValue, 13, IssAclRateCtrlDLFLimitValueSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssAclRateCtrlIndex ,i4SetValIssAclRateCtrlDLFLimitValue)
#define nmhSetIssAclRateCtrlBCASTLimitValue(i4IssAclRateCtrlIndex ,i4SetValIssAclRateCtrlBCASTLimitValue)       \
        nmhSetCmn(IssAclRateCtrlBCASTLimitValue, 13, IssAclRateCtrlBCASTLimitValueSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssAclRateCtrlIndex ,i4SetValIssAclRateCtrlBCASTLimitValue)
#define nmhSetIssAclRateCtrlMCASTLimitValue(i4IssAclRateCtrlIndex ,i4SetValIssAclRateCtrlMCASTLimitValue)       \
        nmhSetCmn(IssAclRateCtrlMCASTLimitValue, 13, IssAclRateCtrlMCASTLimitValueSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssAclRateCtrlIndex ,i4SetValIssAclRateCtrlMCASTLimitValue)
#define nmhSetIssAclRateCtrlPortRateLimit(i4IssAclRateCtrlIndex ,i4SetValIssAclRateCtrlPortRateLimit)   \
        nmhSetCmn(IssAclRateCtrlPortRateLimit, 13, IssAclRateCtrlPortRateLimitSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssAclRateCtrlIndex ,i4SetValIssAclRateCtrlPortRateLimit)
#define nmhSetIssAclRateCtrlPortBurstSize(i4IssAclRateCtrlIndex ,i4SetValIssAclRateCtrlPortBurstSize)   \
        nmhSetCmn(IssAclRateCtrlPortBurstSize, 13, IssAclRateCtrlPortBurstSizeSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssAclRateCtrlIndex ,i4SetValIssAclRateCtrlPortBurstSize)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IssAclL2FilterNo[13];
extern UINT4 IssAclL2FilterPriority[13];
extern UINT4 IssAclL2FilterEtherType[13];
extern UINT4 IssAclL2FilterProtocolType[13];
extern UINT4 IssAclL2FilterDstMacAddr[13];
extern UINT4 IssAclL2FilterSrcMacAddr[13];
extern UINT4 IssAclL2FilterVlanId[13];
extern UINT4 IssAclL2FilterInPortList[13];
extern UINT4 IssAclL2FilterAction[13];
extern UINT4 IssAclL2FilterStatus[13];
extern UINT4 IssAclL2FilterOutPortList[13];
extern UINT4 IssAclL2FilterDirection[13];
extern UINT4 IssAclL2FilterInPortChannelList[13];
extern UINT4 IssAclL2FilterOutPortChannelList[13];
extern UINT4 IssAclL2FilterStatsEnabledStatus[13];
extern UINT4 IssAclClearL2FilterStats[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIssAclL2FilterPriority(i4IssAclL2FilterNo ,i4SetValIssAclL2FilterPriority)        \
        nmhSetCmn(IssAclL2FilterPriority, 13, IssAclL2FilterPrioritySet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssAclL2FilterNo ,i4SetValIssAclL2FilterPriority)
#define nmhSetIssAclL2FilterEtherType(i4IssAclL2FilterNo ,i4SetValIssAclL2FilterEtherType)      \
        nmhSetCmn(IssAclL2FilterEtherType, 13, IssAclL2FilterEtherTypeSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssAclL2FilterNo ,i4SetValIssAclL2FilterEtherType)
#define nmhSetIssAclL2FilterProtocolType(i4IssAclL2FilterNo ,u4SetValIssAclL2FilterProtocolType)        \
        nmhSetCmn(IssAclL2FilterProtocolType, 13, IssAclL2FilterProtocolTypeSet, IssLock, IssUnLock, 0, 0, 1, "%i %u", i4IssAclL2FilterNo ,u4SetValIssAclL2FilterProtocolType)
#define nmhSetIssAclL2FilterDstMacAddr(i4IssAclL2FilterNo ,SetValIssAclL2FilterDstMacAddr)      \
        nmhSetCmn(IssAclL2FilterDstMacAddr, 13, IssAclL2FilterDstMacAddrSet, IssLock, IssUnLock, 0, 0, 1, "%i %m", i4IssAclL2FilterNo ,SetValIssAclL2FilterDstMacAddr)
#define nmhSetIssAclL2FilterSrcMacAddr(i4IssAclL2FilterNo ,SetValIssAclL2FilterSrcMacAddr)      \
        nmhSetCmn(IssAclL2FilterSrcMacAddr, 13, IssAclL2FilterSrcMacAddrSet, IssLock, IssUnLock, 0, 0, 1, "%i %m", i4IssAclL2FilterNo ,SetValIssAclL2FilterSrcMacAddr)
#define nmhSetIssAclL2FilterVlanId(i4IssAclL2FilterNo ,i4SetValIssAclL2FilterVlanId)    \
        nmhSetCmn(IssAclL2FilterVlanId, 13, IssAclL2FilterVlanIdSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssAclL2FilterNo ,i4SetValIssAclL2FilterVlanId)
#define nmhSetIssAclL2FilterInPortList(i4IssAclL2FilterNo ,pSetValIssAclL2FilterInPortList)     \
        nmhSetCmn(IssAclL2FilterInPortList, 13, IssAclL2FilterInPortListSet, IssLock, IssUnLock, 0, 0, 1, "%i %s", i4IssAclL2FilterNo ,pSetValIssAclL2FilterInPortList)
#define nmhSetIssAclL2FilterAction(i4IssAclL2FilterNo ,i4SetValIssAclL2FilterAction)    \
        nmhSetCmn(IssAclL2FilterAction, 13, IssAclL2FilterActionSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssAclL2FilterNo ,i4SetValIssAclL2FilterAction)
#define nmhSetIssAclL2FilterStatus(i4IssAclL2FilterNo ,i4SetValIssAclL2FilterStatus)    \
        nmhSetCmn(IssAclL2FilterStatus, 13, IssAclL2FilterStatusSet, IssLock, IssUnLock, 0, 1, 1, "%i %i", i4IssAclL2FilterNo ,i4SetValIssAclL2FilterStatus)
#define nmhSetIssAclL2FilterOutPortList(i4IssAclL2FilterNo ,pSetValIssAclL2FilterOutPortList)   \
        nmhSetCmn(IssAclL2FilterOutPortList, 13, IssAclL2FilterOutPortListSet, IssLock, IssUnLock, 0, 0, 1, "%i %s", i4IssAclL2FilterNo ,pSetValIssAclL2FilterOutPortList)
#define nmhSetIssAclL2FilterDirection(i4IssAclL2FilterNo ,i4SetValIssAclL2FilterDirection)      \
 nmhSetCmn(IssAclL2FilterDirection, 13, IssAclL2FilterDirectionSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssAclL2FilterNo ,i4SetValIssAclL2FilterDirection)
#define nmhSetIssAclL2FilterInPortChannelList(i4IssAclL2FilterNo ,pSetValIssAclL2FilterInPortChannelList) \
 nmhSetCmn(IssAclL2FilterInPortChannelList, 13, IssAclL2FilterInPortChannelListSet, IssLock, IssUnLock, 0, 0, 1, "%i %s", i4IssAclL2FilterNo ,pSetValIssAclL2FilterInPortChannelList)
#define nmhSetIssAclL2FilterOutPortChannelList(i4IssAclL2FilterNo ,pSetValIssAclL2FilterOutPortChannelList) \
 nmhSetCmn(IssAclL2FilterOutPortChannelList, 13, IssAclL2FilterOutPortChannelListSet, IssLock, IssUnLock, 0, 0, 1, "%i %s", i4IssAclL2FilterNo ,pSetValIssAclL2FilterOutPortChannelList)
#define nmhSetIssAclL2FilterStatsEnabledStatus(i4IssAclL2FilterNo ,i4SetValIssAclL2FilterStatsEnabledStatus) \
 nmhSetCmn(IssAclL2FilterStatsEnabledStatus, 13, IssAclL2FilterStatsEnabledStatusSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssAclL2FilterNo ,i4SetValIssAclL2FilterStatsEnabledStatus)
#define nmhSetIssAclClearL2FilterStats(i4IssAclL2FilterNo ,i4SetValIssAclClearL2FilterStats) \
 nmhSetCmn(IssAclClearL2FilterStats, 13, IssAclClearL2FilterStatsSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssAclL2FilterNo ,i4SetValIssAclClearL2FilterStats)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IssAclL3FilterNo[13];
extern UINT4 IssAclL3FilterPriority[13];
extern UINT4 IssAclL3FilterProtocol[13];
extern UINT4 IssAclL3FilterMessageType[13];
extern UINT4 IssAclL3FilterMessageCode[13];
extern UINT4 IssAclL3FilteAddrType[13];
extern UINT4 IssAclL3FilterDstIpAddr[13];
extern UINT4 IssAclL3FilterSrcIpAddr[13];
extern UINT4 IssAclL3FilterDstIpAddrPrefixLength[13];
extern UINT4 IssAclL3FilterSrcIpAddrPrefixLength[13];
extern UINT4 IssAclL3FilterMinDstProtPort[13];
extern UINT4 IssAclL3FilterMaxDstProtPort[13];
extern UINT4 IssAclL3FilterMinSrcProtPort[13];
extern UINT4 IssAclL3FilterMaxSrcProtPort[13];
extern UINT4 IssAclL3FilterInPortList[13];
extern UINT4 IssAclL3FilterOutPortList[13];
extern UINT4 IssAclL3FilterAckBit[13];
extern UINT4 IssAclL3FilterRstBit[13];
extern UINT4 IssAclL3FilterTos[13];
extern UINT4 IssAclL3FilterDscp[13];
extern UINT4 IssAclL3FilterDirection[13];
extern UINT4 IssAclL3FilterAction[13];
extern UINT4 IssAclL3FilterFlowId[13];
extern UINT4 IssAclL3FilterStatus[13];
extern UINT4 IssAclL3FilterInPortChannelList[13];
extern UINT4 IssAclL3FilterOutPortChannelList[13];
extern UINT4 IssAclL3FilterStatsEnabledStatus[13];
extern UINT4 IssAclClearL3FilterStats[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIssAclL3FilterPriority(i4IssAclL3FilterNo ,i4SetValIssAclL3FilterPriority)        \
        nmhSetCmn(IssAclL3FilterPriority, 13, IssAclL3FilterPrioritySet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssAclL3FilterNo ,i4SetValIssAclL3FilterPriority)
#define nmhSetIssAclL3FilterProtocol(i4IssAclL3FilterNo ,i4SetValIssAclL3FilterProtocol)        \
        nmhSetCmn(IssAclL3FilterProtocol, 13, IssAclL3FilterProtocolSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssAclL3FilterNo ,i4SetValIssAclL3FilterProtocol)
#define nmhSetIssAclL3FilterMessageType(i4IssAclL3FilterNo ,i4SetValIssAclL3FilterMessageType)  \
        nmhSetCmn(IssAclL3FilterMessageType, 13, IssAclL3FilterMessageTypeSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssAclL3FilterNo ,i4SetValIssAclL3FilterMessageType)
#define nmhSetIssAclL3FilterMessageCode(i4IssAclL3FilterNo ,i4SetValIssAclL3FilterMessageCode)  \
        nmhSetCmn(IssAclL3FilterMessageCode, 13, IssAclL3FilterMessageCodeSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssAclL3FilterNo ,i4SetValIssAclL3FilterMessageCode)
#define nmhSetIssAclL3FilteAddrType(i4IssAclL3FilterNo ,i4SetValIssAclL3FilteAddrType)  \
        nmhSetCmn(IssAclL3FilteAddrType, 13, IssAclL3FilteAddrTypeSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssAclL3FilterNo ,i4SetValIssAclL3FilteAddrType)
#define nmhSetIssAclL3FilterDstIpAddr(i4IssAclL3FilterNo ,pSetValIssAclL3FilterDstIpAddr)       \
        nmhSetCmn(IssAclL3FilterDstIpAddr, 13, IssAclL3FilterDstIpAddrSet, IssLock, IssUnLock, 0, 0, 1, "%i %s", i4IssAclL3FilterNo ,pSetValIssAclL3FilterDstIpAddr)
#define nmhSetIssAclL3FilterSrcIpAddr(i4IssAclL3FilterNo ,pSetValIssAclL3FilterSrcIpAddr)       \
        nmhSetCmn(IssAclL3FilterSrcIpAddr, 13, IssAclL3FilterSrcIpAddrSet, IssLock, IssUnLock, 0, 0, 1, "%i %s", i4IssAclL3FilterNo ,pSetValIssAclL3FilterSrcIpAddr)
#define nmhSetIssAclL3FilterDstIpAddrPrefixLength(i4IssAclL3FilterNo ,u4SetValIssAclL3FilterDstIpAddrPrefixLength)      \
        nmhSetCmn(IssAclL3FilterDstIpAddrPrefixLength, 13, IssAclL3FilterDstIpAddrPrefixLengthSet, IssLock, IssUnLock, 0, 0, 1, "%i %u", i4IssAclL3FilterNo ,u4SetValIssAclL3FilterDstIpAddrPrefixLength)
#define nmhSetIssAclL3FilterSrcIpAddrPrefixLength(i4IssAclL3FilterNo ,u4SetValIssAclL3FilterSrcIpAddrPrefixLength)      \
        nmhSetCmn(IssAclL3FilterSrcIpAddrPrefixLength, 13, IssAclL3FilterSrcIpAddrPrefixLengthSet, IssLock, IssUnLock, 0, 0, 1, "%i %u", i4IssAclL3FilterNo ,u4SetValIssAclL3FilterSrcIpAddrPrefixLength)
#define nmhSetIssAclL3FilterMinDstProtPort(i4IssAclL3FilterNo ,u4SetValIssAclL3FilterMinDstProtPort)    \
        nmhSetCmn(IssAclL3FilterMinDstProtPort, 13, IssAclL3FilterMinDstProtPortSet, IssLock, IssUnLock, 0, 0, 1, "%i %u", i4IssAclL3FilterNo ,u4SetValIssAclL3FilterMinDstProtPort)
#define nmhSetIssAclL3FilterMaxDstProtPort(i4IssAclL3FilterNo ,u4SetValIssAclL3FilterMaxDstProtPort)    \
        nmhSetCmn(IssAclL3FilterMaxDstProtPort, 13, IssAclL3FilterMaxDstProtPortSet, IssLock, IssUnLock, 0, 0, 1, "%i %u", i4IssAclL3FilterNo ,u4SetValIssAclL3FilterMaxDstProtPort)
#define nmhSetIssAclL3FilterMinSrcProtPort(i4IssAclL3FilterNo ,u4SetValIssAclL3FilterMinSrcProtPort)    \
        nmhSetCmn(IssAclL3FilterMinSrcProtPort, 13, IssAclL3FilterMinSrcProtPortSet, IssLock, IssUnLock, 0, 0, 1, "%i %u", i4IssAclL3FilterNo ,u4SetValIssAclL3FilterMinSrcProtPort)
#define nmhSetIssAclL3FilterMaxSrcProtPort(i4IssAclL3FilterNo ,u4SetValIssAclL3FilterMaxSrcProtPort)    \
        nmhSetCmn(IssAclL3FilterMaxSrcProtPort, 13, IssAclL3FilterMaxSrcProtPortSet, IssLock, IssUnLock, 0, 0, 1, "%i %u", i4IssAclL3FilterNo ,u4SetValIssAclL3FilterMaxSrcProtPort)
#define nmhSetIssAclL3FilterInPortList(i4IssAclL3FilterNo ,pSetValIssAclL3FilterInPortList)     \
        nmhSetCmn(IssAclL3FilterInPortList, 13, IssAclL3FilterInPortListSet, IssLock, IssUnLock, 0, 0, 1, "%i %s", i4IssAclL3FilterNo ,pSetValIssAclL3FilterInPortList)
#define nmhSetIssAclL3FilterOutPortList(i4IssAclL3FilterNo ,pSetValIssAclL3FilterOutPortList)   \
        nmhSetCmn(IssAclL3FilterOutPortList, 13, IssAclL3FilterOutPortListSet, IssLock, IssUnLock, 0, 0, 1, "%i %s", i4IssAclL3FilterNo ,pSetValIssAclL3FilterOutPortList)
#define nmhSetIssAclL3FilterAckBit(i4IssAclL3FilterNo ,i4SetValIssAclL3FilterAckBit)    \
        nmhSetCmn(IssAclL3FilterAckBit, 13, IssAclL3FilterAckBitSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssAclL3FilterNo ,i4SetValIssAclL3FilterAckBit)
#define nmhSetIssAclL3FilterRstBit(i4IssAclL3FilterNo ,i4SetValIssAclL3FilterRstBit)    \
        nmhSetCmn(IssAclL3FilterRstBit, 13, IssAclL3FilterRstBitSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssAclL3FilterNo ,i4SetValIssAclL3FilterRstBit)
#define nmhSetIssAclL3FilterTos(i4IssAclL3FilterNo ,i4SetValIssAclL3FilterTos)  \
        nmhSetCmn(IssAclL3FilterTos, 13, IssAclL3FilterTosSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssAclL3FilterNo ,i4SetValIssAclL3FilterTos)
#define nmhSetIssAclL3FilterDscp(i4IssAclL3FilterNo ,i4SetValIssAclL3FilterDscp)        \
        nmhSetCmn(IssAclL3FilterDscp, 13, IssAclL3FilterDscpSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssAclL3FilterNo ,i4SetValIssAclL3FilterDscp)
#define nmhSetIssAclL3FilterDirection(i4IssAclL3FilterNo ,i4SetValIssAclL3FilterDirection)      \
        nmhSetCmn(IssAclL3FilterDirection, 13, IssAclL3FilterDirectionSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssAclL3FilterNo ,i4SetValIssAclL3FilterDirection)
#define nmhSetIssAclL3FilterAction(i4IssAclL3FilterNo ,i4SetValIssAclL3FilterAction)    \
        nmhSetCmn(IssAclL3FilterAction, 13, IssAclL3FilterActionSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssAclL3FilterNo ,i4SetValIssAclL3FilterAction)
#define nmhSetIssAclL3FilterFlowId(i4IssAclL3FilterNo ,u4SetValIssAclL3FilterFlowId)    \
        nmhSetCmn(IssAclL3FilterFlowId, 13, IssAclL3FilterFlowIdSet, IssLock, IssUnLock, 0, 0, 1, "%i %u", i4IssAclL3FilterNo ,u4SetValIssAclL3FilterFlowId)
#define nmhSetIssAclL3FilterStatus(i4IssAclL3FilterNo ,i4SetValIssAclL3FilterStatus)    \
        nmhSetCmn(IssAclL3FilterStatus, 13, IssAclL3FilterStatusSet, IssLock, IssUnLock, 0, 1, 1, "%i %i", i4IssAclL3FilterNo ,i4SetValIssAclL3FilterStatus)
#define nmhSetIssAclL3FilterInPortChannelList(i4IssAclL3FilterNo ,pSetValIssAclL3FilterInPortChannelList) \
 nmhSetCmn(IssAclL3FilterInPortChannelList, 13, IssAclL3FilterInPortChannelListSet, IssLock, IssUnLock, 0, 0, 1, "%i %s", i4IssAclL3FilterNo ,pSetValIssAclL3FilterInPortChannelList)
#define nmhSetIssAclL3FilterOutPortChannelList(i4IssAclL3FilterNo ,pSetValIssAclL3FilterOutPortChannelList) \
 nmhSetCmn(IssAclL3FilterOutPortChannelList, 13, IssAclL3FilterOutPortChannelListSet, IssLock, IssUnLock, 0, 0, 1, "%i %s", i4IssAclL3FilterNo ,pSetValIssAclL3FilterOutPortChannelList)
#define nmhSetIssAclL3FilterStatsEnabledStatus(i4IssAclL3FilterNo ,i4SetValIssAclL3FilterStatsEnabledStatus) \
 nmhSetCmn(IssAclL3FilterStatsEnabledStatus, 13, IssAclL3FilterStatsEnabledStatusSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssAclL3FilterNo ,i4SetValIssAclL3FilterStatsEnabledStatus)
#define nmhSetIssAclClearL3FilterStats(i4IssAclL3FilterNo ,i4SetValIssAclClearL3FilterStats) \
 nmhSetCmn(IssAclClearL3FilterStats, 13, IssAclClearL3FilterStatsSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssAclL3FilterNo ,i4SetValIssAclClearL3FilterStats)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IssAclUserDefinedFilterId[13];
extern UINT4 IssAclUserDefinedFilterPktType[13];
extern UINT4 IssAclUserDefinedFilterOffSetBase[13];
extern UINT4 IssAclUserDefinedFilterOffSetValue[13];
extern UINT4 IssAclUserDefinedFilterOffSetMask[13];
extern UINT4 IssAclUserDefinedFilterPriority[13];
extern UINT4 IssAclUserDefinedFilterAction[13];
extern UINT4 IssAclUserDefinedFilterInPortList[13];
extern UINT4 IssAclUserDefinedFilterIdOneType[13];
extern UINT4 IssAclUserDefinedFilterIdOne[13];
extern UINT4 IssAclUserDefinedFilterIdTwoType[13];
extern UINT4 IssAclUserDefinedFilterIdTwo[13];
extern UINT4 IssAclUserDefinedFilterSubAction[13];
extern UINT4 IssAclUserDefinedFilterSubActionId[13];
extern UINT4 IssAclUserDefinedFilterStatus[13];
extern UINT4 IssAclUserDefinedFilterStatsEnabledStatus[13];
extern UINT4 IssAclClearUserDefinedFilterStats[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIssAclUserDefinedFilterPktType(u4IssAclUserDefinedFilterId ,i4SetValIssAclUserDefinedFilterPktType) \
 nmhSetCmn(IssAclUserDefinedFilterPktType, 13, IssAclUserDefinedFilterPktTypeSet, IssLock, IssUnLock, 0, 0, 1, "%u %i", u4IssAclUserDefinedFilterId ,i4SetValIssAclUserDefinedFilterPktType)
#define nmhSetIssAclUserDefinedFilterOffSetBase(u4IssAclUserDefinedFilterId ,i4SetValIssAclUserDefinedFilterOffSetBase) \
 nmhSetCmn(IssAclUserDefinedFilterOffSetBase, 13, IssAclUserDefinedFilterOffSetBaseSet, IssLock, IssUnLock, 0, 0, 1, "%u %i", u4IssAclUserDefinedFilterId ,i4SetValIssAclUserDefinedFilterOffSetBase)
#define nmhSetIssAclUserDefinedFilterOffSetValue(u4IssAclUserDefinedFilterId ,pSetValIssAclUserDefinedFilterOffSetValue) \
 nmhSetCmn(IssAclUserDefinedFilterOffSetValue, 13, IssAclUserDefinedFilterOffSetValueSet, IssLock, IssUnLock, 0, 0, 1, "%u %s", u4IssAclUserDefinedFilterId ,pSetValIssAclUserDefinedFilterOffSetValue)
#define nmhSetIssAclUserDefinedFilterOffSetMask(u4IssAclUserDefinedFilterId ,pSetValIssAclUserDefinedFilterOffSetMask) \
 nmhSetCmn(IssAclUserDefinedFilterOffSetMask, 13, IssAclUserDefinedFilterOffSetMaskSet, IssLock, IssUnLock, 0, 0, 1, "%u %s", u4IssAclUserDefinedFilterId ,pSetValIssAclUserDefinedFilterOffSetMask)
#define nmhSetIssAclUserDefinedFilterPriority(u4IssAclUserDefinedFilterId ,i4SetValIssAclUserDefinedFilterPriority) \
 nmhSetCmn(IssAclUserDefinedFilterPriority, 13, IssAclUserDefinedFilterPrioritySet, IssLock, IssUnLock, 0, 0, 1, "%u %i", u4IssAclUserDefinedFilterId ,i4SetValIssAclUserDefinedFilterPriority)
#define nmhSetIssAclUserDefinedFilterAction(u4IssAclUserDefinedFilterId ,i4SetValIssAclUserDefinedFilterAction) \
 nmhSetCmn(IssAclUserDefinedFilterAction, 13, IssAclUserDefinedFilterActionSet, IssLock, IssUnLock, 0, 0, 1, "%u %i", u4IssAclUserDefinedFilterId ,i4SetValIssAclUserDefinedFilterAction)
#define nmhSetIssAclUserDefinedFilterInPortList(u4IssAclUserDefinedFilterId ,pSetValIssAclUserDefinedFilterInPortList) \
 nmhSetCmn(IssAclUserDefinedFilterInPortList, 13, IssAclUserDefinedFilterInPortListSet, IssLock, IssUnLock, 0, 0, 1, "%u %s", u4IssAclUserDefinedFilterId ,pSetValIssAclUserDefinedFilterInPortList)
#define nmhSetIssAclUserDefinedFilterIdOneType(u4IssAclUserDefinedFilterId ,i4SetValIssAclUserDefinedFilterIdOneType) \
 nmhSetCmn(IssAclUserDefinedFilterIdOneType, 13, IssAclUserDefinedFilterIdOneTypeSet, IssLock, IssUnLock, 0, 0, 1, "%u %i", u4IssAclUserDefinedFilterId ,i4SetValIssAclUserDefinedFilterIdOneType)
#define nmhSetIssAclUserDefinedFilterIdOne(u4IssAclUserDefinedFilterId ,u4SetValIssAclUserDefinedFilterIdOne) \
 nmhSetCmn(IssAclUserDefinedFilterIdOne, 13, IssAclUserDefinedFilterIdOneSet, IssLock, IssUnLock, 0, 0, 1, "%u %u", u4IssAclUserDefinedFilterId ,u4SetValIssAclUserDefinedFilterIdOne)
#define nmhSetIssAclUserDefinedFilterIdTwoType(u4IssAclUserDefinedFilterId ,i4SetValIssAclUserDefinedFilterIdTwoType) \
 nmhSetCmn(IssAclUserDefinedFilterIdTwoType, 13, IssAclUserDefinedFilterIdTwoTypeSet, IssLock, IssUnLock, 0, 0, 1, "%u %i", u4IssAclUserDefinedFilterId ,i4SetValIssAclUserDefinedFilterIdTwoType)
#define nmhSetIssAclUserDefinedFilterIdTwo(u4IssAclUserDefinedFilterId ,u4SetValIssAclUserDefinedFilterIdTwo) \
 nmhSetCmn(IssAclUserDefinedFilterIdTwo, 13, IssAclUserDefinedFilterIdTwoSet, IssLock, IssUnLock, 0, 0, 1, "%u %u", u4IssAclUserDefinedFilterId ,u4SetValIssAclUserDefinedFilterIdTwo)
#define nmhSetIssAclUserDefinedFilterSubAction(u4IssAclUserDefinedFilterId ,i4SetValIssAclUserDefinedFilterSubAction) \
 nmhSetCmn(IssAclUserDefinedFilterSubAction, 13, IssAclUserDefinedFilterSubActionSet, IssLock, IssUnLock, 0, 0, 1, "%u %i", u4IssAclUserDefinedFilterId ,i4SetValIssAclUserDefinedFilterSubAction)
#define nmhSetIssAclUserDefinedFilterSubActionId(u4IssAclUserDefinedFilterId ,i4SetValIssAclUserDefinedFilterSubActionId) \
 nmhSetCmn(IssAclUserDefinedFilterSubActionId, 13, IssAclUserDefinedFilterSubActionIdSet, IssLock, IssUnLock, 0, 0, 1, "%u %i", u4IssAclUserDefinedFilterId ,i4SetValIssAclUserDefinedFilterSubActionId)
#define nmhSetIssAclUserDefinedFilterStatus(u4IssAclUserDefinedFilterId ,i4SetValIssAclUserDefinedFilterStatus) \
 nmhSetCmn(IssAclUserDefinedFilterStatus, 13, IssAclUserDefinedFilterStatusSet, IssLock, IssUnLock, 0, 1, 1, "%u %i", u4IssAclUserDefinedFilterId ,i4SetValIssAclUserDefinedFilterStatus)
#define nmhSetIssAclUserDefinedFilterStatsEnabledStatus(u4IssAclUserDefinedFilterId ,i4SetValIssAclUserDefinedFilterStatsEnabledStatus) \
 nmhSetCmn(IssAclUserDefinedFilterStatsEnabledStatus, 13, IssAclUserDefinedFilterStatsEnabledStatusSet, IssLock, IssUnLock, 0, 0, 1, "%u %i", u4IssAclUserDefinedFilterId ,i4SetValIssAclUserDefinedFilterStatsEnabledStatus)
#define nmhSetIssAclClearUserDefinedFilterStats(u4IssAclUserDefinedFilterId ,i4SetValIssAclClearUserDefinedFilterStats) \
 nmhSetCmn(IssAclClearUserDefinedFilterStats, 13, IssAclClearUserDefinedFilterStatsSet, IssLock, IssUnLock, 0, 0, 1, "%u %i", u4IssAclUserDefinedFilterId ,i4SetValIssAclClearUserDefinedFilterStats)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IssRedirectInterfaceGrpId[13];
extern UINT4 IssRedirectInterfaceGrpFilterType[13];
extern UINT4 IssRedirectInterfaceGrpFilterId[13];
extern UINT4 IssRedirectInterfaceGrpDistByte[13];
extern UINT4 IssRedirectInterfaceGrpPortList[13];
extern UINT4 IssRedirectInterfaceGrpType[13];
extern UINT4 IssRedirectInterfaceGrpUdbPosition[13];
extern UINT4 IssRedirectInterfaceGrpStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIssRedirectInterfaceGrpFilterType(u4IssRedirectInterfaceGrpId ,i4SetValIssRedirectInterfaceGrpFilterType) \
 nmhSetCmnWithLock(IssRedirectInterfaceGrpFilterType, 13, IssRedirectInterfaceGrpFilterTypeSet, IssLock, IssUnLock, 0, 0, 1, "%u %i", u4IssRedirectInterfaceGrpId ,i4SetValIssRedirectInterfaceGrpFilterType)
#define nmhSetIssRedirectInterfaceGrpFilterId(u4IssRedirectInterfaceGrpId ,u4SetValIssRedirectInterfaceGrpFilterId) \
 nmhSetCmnWithLock(IssRedirectInterfaceGrpFilterId, 13, IssRedirectInterfaceGrpFilterIdSet, IssLock, IssUnLock, 0, 0, 1, "%u %u", u4IssRedirectInterfaceGrpId ,u4SetValIssRedirectInterfaceGrpFilterId)
#define nmhSetIssRedirectInterfaceGrpDistByte(u4IssRedirectInterfaceGrpId ,i4SetValIssRedirectInterfaceGrpDistByte) \
 nmhSetCmnWithLock(IssRedirectInterfaceGrpDistByte, 13, IssRedirectInterfaceGrpDistByteSet, IssLock, IssUnLock, 0, 0, 1, "%u %i", u4IssRedirectInterfaceGrpId ,i4SetValIssRedirectInterfaceGrpDistByte)
#define nmhSetIssRedirectInterfaceGrpPortList(u4IssRedirectInterfaceGrpId ,pSetValIssRedirectInterfaceGrpPortList) \
 nmhSetCmnWithLock(IssRedirectInterfaceGrpPortList, 13, IssRedirectInterfaceGrpPortListSet, IssLock, IssUnLock, 0, 0, 1, "%u %s", u4IssRedirectInterfaceGrpId ,pSetValIssRedirectInterfaceGrpPortList)
#define nmhSetIssRedirectInterfaceGrpType(u4IssRedirectInterfaceGrpId ,i4SetValIssRedirectInterfaceGrpType) \
 nmhSetCmnWithLock(IssRedirectInterfaceGrpType, 13, IssRedirectInterfaceGrpTypeSet, IssLock, IssUnLock, 0, 0, 1, "%u %i", u4IssRedirectInterfaceGrpId ,i4SetValIssRedirectInterfaceGrpType)
#define nmhSetIssRedirectInterfaceGrpUdbPosition(u4IssRedirectInterfaceGrpId ,i4SetValIssRedirectInterfaceGrpUdbPosition) \
 nmhSetCmnWithLock(IssRedirectInterfaceGrpUdbPosition, 13, IssRedirectInterfaceGrpUdbPositionSet, IssLock, IssUnLock, 0, 0, 1, "%u %i", u4IssRedirectInterfaceGrpId ,i4SetValIssRedirectInterfaceGrpUdbPosition)
#define nmhSetIssRedirectInterfaceGrpStatus(u4IssRedirectInterfaceGrpId ,i4SetValIssRedirectInterfaceGrpStatus) \
 nmhSetCmnWithLock(IssRedirectInterfaceGrpStatus, 13, IssRedirectInterfaceGrpStatusSet, IssLock, IssUnLock, 0, 1, 1, "%u %i", u4IssRedirectInterfaceGrpId ,i4SetValIssRedirectInterfaceGrpStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IssReservedFrameCtrlId[13];
extern UINT4 IssReservedFrameCtrlPktType[13];
extern UINT4 IssReservedFrameCtrlAction[13];
extern UINT4 IssReservedFrameCtrlOtherMacAddr[13];
extern UINT4 IssReservedFrameCtrlOtherMacMask[13];
extern UINT4 IssReservedFrameCtrlStatsEnabledStatus[13];
extern UINT4 IssClearReservedFrameCtrlStats[13];
extern UINT4 IssReservedFrameCtrlStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIssReservedFrameCtrlPktType(u4IssReservedFrameCtrlId ,i4SetValIssReservedFrameCtrlPktType) \
 nmhSetCmn(IssReservedFrameCtrlPktType, 13, IssReservedFrameCtrlPktTypeSet, NULL, NULL, 0, 0, 1, "%u %i", u4IssReservedFrameCtrlId ,i4SetValIssReservedFrameCtrlPktType)
#define nmhSetIssReservedFrameCtrlAction(u4IssReservedFrameCtrlId ,i4SetValIssReservedFrameCtrlAction) \
 nmhSetCmn(IssReservedFrameCtrlAction, 13, IssReservedFrameCtrlActionSet, NULL, NULL, 0, 0, 1, "%u %i", u4IssReservedFrameCtrlId ,i4SetValIssReservedFrameCtrlAction)
#define nmhSetIssReservedFrameCtrlOtherMacAddr(u4IssReservedFrameCtrlId ,SetValIssReservedFrameCtrlOtherMacAddr) \
 nmhSetCmn(IssReservedFrameCtrlOtherMacAddr, 13, IssReservedFrameCtrlOtherMacAddrSet, NULL, NULL, 0, 0, 1, "%u %m", u4IssReservedFrameCtrlId ,SetValIssReservedFrameCtrlOtherMacAddr)
#define nmhSetIssReservedFrameCtrlOtherMacMask(u4IssReservedFrameCtrlId ,i4SetValIssReservedFrameCtrlOtherMacMask) \
 nmhSetCmn(IssReservedFrameCtrlOtherMacMask, 13, IssReservedFrameCtrlOtherMacMaskSet, NULL, NULL, 0, 0, 1, "%u %i", u4IssReservedFrameCtrlId ,i4SetValIssReservedFrameCtrlOtherMacMask)
#define nmhSetIssReservedFrameCtrlStatsEnabledStatus(u4IssReservedFrameCtrlId ,i4SetValIssReservedFrameCtrlStatsEnabledStatus) \
 nmhSetCmn(IssReservedFrameCtrlStatsEnabledStatus, 13, IssReservedFrameCtrlStatsEnabledStatusSet, NULL, NULL, 0, 0, 1, "%u %i", u4IssReservedFrameCtrlId ,i4SetValIssReservedFrameCtrlStatsEnabledStatus)
#define nmhSetIssClearReservedFrameCtrlStats(u4IssReservedFrameCtrlId ,i4SetValIssClearReservedFrameCtrlStats) \
 nmhSetCmn(IssClearReservedFrameCtrlStats, 13, IssClearReservedFrameCtrlStatsSet, NULL, NULL, 0, 0, 1, "%u %i", u4IssReservedFrameCtrlId ,i4SetValIssClearReservedFrameCtrlStats)
#define nmhSetIssReservedFrameCtrlStatus(u4IssReservedFrameCtrlId ,i4SetValIssReservedFrameCtrlStatus) \
 nmhSetCmn(IssReservedFrameCtrlStatus, 13, IssReservedFrameCtrlStatusSet, NULL, NULL, 0, 1, 1, "%u %i", u4IssReservedFrameCtrlId ,i4SetValIssReservedFrameCtrlStatus)

#endif
