/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssslcli.h,v 1.3 2011/02/16 06:47:57 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 SslSecureHttpStatus[10];
extern UINT4 SslPort[10];
extern UINT4 SslTrace[10];
extern UINT4 SslVersion[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetSslSecureHttpStatus(i4SetValSslSecureHttpStatus)	\
	nmhSetCmn(SslSecureHttpStatus, 10, SslSecureHttpStatusSet, NULL, NULL, 0, 0, 0, "%i", i4SetValSslSecureHttpStatus)
#define nmhSetSslPort(i4SetValSslPort)	\
	nmhSetCmn(SslPort, 10, SslPortSet, NULL, NULL, 0, 0, 0, "%i", i4SetValSslPort)
#define nmhSetSslTrace(i4SetValSslTrace)	\
	nmhSetCmn(SslTrace, 10, SslTraceSet, NULL, NULL, 0, 0, 0, "%i", i4SetValSslTrace)
#define nmhSetSslVersion(i4SetValSslVersion)	\
	nmhSetCmn(SslVersion, 10, SslVersionSet, NULL, NULL, 0, 0, 0, "%i", i4SetValSslVersion)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 SslCipherList[10];
extern UINT4 SslDefaultCipherList[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetSslCipherList(i4SetValSslCipherList)	\
	nmhSetCmn(SslCipherList, 10, SslCipherListSet, NULL, NULL, 0, 0, 0, "%i", i4SetValSslCipherList)
#define nmhSetSslDefaultCipherList(i4SetValSslDefaultCipherList)	\
	nmhSetCmn(SslDefaultCipherList, 10, SslDefaultCipherListSet, NULL, NULL, 0, 0, 0, "%i", i4SetValSslDefaultCipherList)

#endif
