/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: mfwdcmcli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IpCmnMRouteEnable[11];
extern UINT4 IpCmnMRouteEnableCmdb[11];
extern UINT4 MfwdCmnGlobalTrace[11];
extern UINT4 MfwdCmnGlobalDebug[11];
extern UINT4 MfwdCmnAvgDataRate[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIpCmnMRouteEnable(i4SetValIpCmnMRouteEnable)	\
	nmhSetCmn(IpCmnMRouteEnable, 11, IpCmnMRouteEnableSet, NULL, NULL, 0, 0, 0, "%i", i4SetValIpCmnMRouteEnable)
#define nmhSetIpCmnMRouteEnableCmdb(i4SetValIpCmnMRouteEnableCmdb)	\
	nmhSetCmn(IpCmnMRouteEnableCmdb, 11, IpCmnMRouteEnableCmdbSet, NULL, NULL, 0, 0, 0, "%i", i4SetValIpCmnMRouteEnableCmdb)
#define nmhSetMfwdCmnGlobalTrace(i4SetValMfwdCmnGlobalTrace)	\
	nmhSetCmn(MfwdCmnGlobalTrace, 11, MfwdCmnGlobalTraceSet, NULL, NULL, 0, 0, 0, "%i", i4SetValMfwdCmnGlobalTrace)
#define nmhSetMfwdCmnGlobalDebug(i4SetValMfwdCmnGlobalDebug)	\
	nmhSetCmn(MfwdCmnGlobalDebug, 11, MfwdCmnGlobalDebugSet, NULL, NULL, 0, 0, 0, "%i", i4SetValMfwdCmnGlobalDebug)
#define nmhSetMfwdCmnAvgDataRate(i4SetValMfwdCmnAvgDataRate)	\
	nmhSetCmn(MfwdCmnAvgDataRate, 11, MfwdCmnAvgDataRateSet, NULL, NULL, 0, 0, 0, "%i", i4SetValMfwdCmnAvgDataRate)

#endif
