/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpbcli.h,v 1.3 2012/03/28 13:25:04 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIPbContextId[12];
extern UINT4 FsMIPbMulticastMacLimit[12];
extern UINT4 FsMIPbTunnelStpAddress[12];
extern UINT4 FsMIPbTunnelLacpAddress[12];
extern UINT4 FsMIPbTunnelDot1xAddress[12];
extern UINT4 FsMIPbTunnelGvrpAddress[12];
extern UINT4 FsMIPbTunnelGmrpAddress[12];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIPbPort[12];
extern UINT4 FsMIPbPortSVlanClassificationMethod[12];
extern UINT4 FsMIPbPortSVlanIngressEtherType[12];
extern UINT4 FsMIPbPortSVlanEgressEtherType[12];
extern UINT4 FsMIPbPortSVlanEtherTypeSwapStatus[12];
extern UINT4 FsMIPbPortSVlanTranslationStatus[12];
extern UINT4 FsMIPbPortUnicastMacLearning[12];
extern UINT4 FsMIPbPortUnicastMacLimit[12];
extern UINT4 FsMIPbPortBundleStatus[12];
extern UINT4 FsMIPbPortMultiplexStatus[12];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIPbSrcMacAddress[12];
extern UINT4 FsMIPbSrcMacSVlan[12];
extern UINT4 FsMIPbSrcMacRowStatus[12];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIPbDstMacAddress[12];
extern UINT4 FsMIPbDstMacSVlan[12];
extern UINT4 FsMIPbDstMacRowStatus[12];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIPbCVlanSrcMacCVlan[12];
extern UINT4 FsMIPbCVlanSrcMacAddr[12];
extern UINT4 FsMIPbCVlanSrcMacSVlan[12];
extern UINT4 FsMIPbCVlanSrcMacRowStatus[12];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIPbCVlanDstMacCVlan[12];
extern UINT4 FsMIPbCVlanDstMacAddr[12];
extern UINT4 FsMIPbCVlanDstMacSVlan[12];
extern UINT4 FsMIPbCVlanDstMacRowStatus[12];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIPbDscp[12];
extern UINT4 FsMIPbDscpSVlan[12];
extern UINT4 FsMIPbDscpRowStatus[12];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIPbCVlanDscpCVlan[12];
extern UINT4 FsMIPbCVlanDscp[12];
extern UINT4 FsMIPbCVlanDscpSVlan[12];
extern UINT4 FsMIPbCVlanDscpRowStatus[12];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIPbSrcIpAddr[12];
extern UINT4 FsMIPbSrcIpSVlan[12];
extern UINT4 FsMIPbSrcIpRowStatus[12];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIPbDstIpAddr[12];
extern UINT4 FsMIPbDstIpSVlan[12];
extern UINT4 FsMIPbDstIpRowStatus[12];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIPbSrcDstSrcIpAddr[12];
extern UINT4 FsMIPbSrcDstDstIpAddr[12];
extern UINT4 FsMIPbSrcDstIpSVlan[12];
extern UINT4 FsMIPbSrcDstIpRowStatus[12];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIPbCVlanDstIpCVlan[12];
extern UINT4 FsMIPbCVlanDstIp[12];
extern UINT4 FsMIPbCVlanDstIpSVlan[12];
extern UINT4 FsMIPbCVlanDstIpRowStatus[12];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIPbPortCVlan[12];
extern UINT4 FsMIPbPortCVlanClassifyStatus[12];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIPbLocalEtherType[12];
extern UINT4 FsMIPbRelayEtherType[12];
extern UINT4 FsMIPbEtherTypeSwapRowStatus[12];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIPbSVlanConfigServiceType[12];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIPbTunnelProtocolDot1x[12];
extern UINT4 FsMIPbTunnelProtocolLacp[12];
extern UINT4 FsMIPbTunnelProtocolStp[12];
extern UINT4 FsMIPbTunnelProtocolGvrp[12];
extern UINT4 FsMIPbTunnelProtocolGmrp[12];
extern UINT4 FsMIPbTunnelProtocolIgmp[12];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIPbPepExtCosPreservation[12];
