/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssnmpcli.h,v 1.8 2014/11/11 12:59:24 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 SnmpListenTrapPort[9];
extern UINT4 SnmpOverTcpStatus[9];
extern UINT4 SnmpListenTcpPort[9];
extern UINT4 SnmpTrapOverTcpStatus[9];
extern UINT4 SnmpListenTcpTrapPort[9];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetSnmpListenTrapPort(u4SetValSnmpListenTrapPort) \
 nmhSetCmn(SnmpListenTrapPort, 9, SnmpListenTrapPortSet, NULL, NULL, 0, 0, 0, "%u", u4SetValSnmpListenTrapPort)
#define nmhSetSnmpOverTcpStatus(i4SetValSnmpOverTcpStatus) \
 nmhSetCmn(SnmpOverTcpStatus, 9, SnmpOverTcpStatusSet, NULL, NULL, 0, 0, 0, "%i", i4SetValSnmpOverTcpStatus)
#define nmhSetSnmpListenTcpPort(u4SetValSnmpListenTcpPort) \
 nmhSetCmn(SnmpListenTcpPort, 9, SnmpListenTcpPortSet, NULL, NULL, 0, 0, 0, "%u", u4SetValSnmpListenTcpPort)
#define nmhSetSnmpTrapOverTcpStatus(i4SetValSnmpTrapOverTcpStatus) \
 nmhSetCmn(SnmpTrapOverTcpStatus, 9, SnmpTrapOverTcpStatusSet, NULL, NULL, 0, 0, 0, "%i", i4SetValSnmpTrapOverTcpStatus)
#define nmhSetSnmpListenTcpTrapPort(u4SetValSnmpListenTcpTrapPort) \
 nmhSetCmn(SnmpListenTcpTrapPort, 9, SnmpListenTcpTrapPortSet, NULL, NULL, 0, 0, 0, "%u", u4SetValSnmpListenTcpTrapPort)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 SnmpColdStartTrapControl[9];
extern UINT4 SnmpAgentControl[9];
extern UINT4 SnmpAllowedPduVersions[9];
extern UINT4 SnmpMinimumSecurityRequired[9];
extern UINT4 SnmpProxyListenTrapPort[9];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetSnmpColdStartTrapControl(i4SetValSnmpColdStartTrapControl) \
 nmhSetCmn(SnmpColdStartTrapControl, 9, SnmpColdStartTrapControlSet, NULL, NULL, 0, 0, 0, "%i", i4SetValSnmpColdStartTrapControl)
#define nmhSetSnmpAgentControl(i4SetValSnmpAgentControl) \
 nmhSetCmn(SnmpAgentControl, 9, SnmpAgentControlSet, NULL, NULL, 0, 0, 0, "%i", i4SetValSnmpAgentControl)
#define nmhSetSnmpAllowedPduVersions(i4SetValSnmpAllowedPduVersions) \
 nmhSetCmn(SnmpAllowedPduVersions, 9, SnmpAllowedPduVersionsSet, NULL, NULL, 0, 0, 0, "%i", i4SetValSnmpAllowedPduVersions)
#define nmhSetSnmpMinimumSecurityRequired(i4SetValSnmpMinimumSecurityRequired) \
 nmhSetCmn(SnmpMinimumSecurityRequired, 9, SnmpMinimumSecurityRequiredSet, NULL, NULL, 0, 0, 0, "%i", i4SetValSnmpMinimumSecurityRequired)
#define nmhSetSnmpProxyListenTrapPort(u4SetValSnmpProxyListenTrapPort) \
 nmhSetCmn(SnmpProxyListenTrapPort, 9, SnmpProxyListenTrapPortSet, NULL, NULL, 0, 0, 0, "%u", u4SetValSnmpProxyListenTrapPort)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsSnmpProxyMibName[11];
extern UINT4 FsSnmpProxyMibType[11];
extern UINT4 FsSnmpProxyMibId[11];
extern UINT4 FsSnmpProxyMibTargetParamsIn[11];
extern UINT4 FsSnmpProxyMibSingleTargetOut[11];
extern UINT4 FsSnmpProxyMibMultipleTargetOut[11];
extern UINT4 FsSnmpProxyMibStorageType[11];
extern UINT4 FsSnmpProxyMibRowStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsSnmpProxyMibType(pFsSnmpProxyMibName ,i4SetValFsSnmpProxyMibType) \
 nmhSetCmn(FsSnmpProxyMibType, 11, FsSnmpProxyMibTypeSet, NULL, NULL, 0, 0, 1, "%s %i", pFsSnmpProxyMibName ,i4SetValFsSnmpProxyMibType)
#define nmhSetFsSnmpProxyMibId(pFsSnmpProxyMibName ,pSetValFsSnmpProxyMibId) \
 nmhSetCmn(FsSnmpProxyMibId, 11, FsSnmpProxyMibIdSet, NULL, NULL, 0, 0, 1, "%s %o", pFsSnmpProxyMibName ,pSetValFsSnmpProxyMibId)
#define nmhSetFsSnmpProxyMibTargetParamsIn(pFsSnmpProxyMibName ,pSetValFsSnmpProxyMibTargetParamsIn) \
 nmhSetCmn(FsSnmpProxyMibTargetParamsIn, 11, FsSnmpProxyMibTargetParamsInSet, NULL, NULL, 0, 0, 1, "%s %s", pFsSnmpProxyMibName ,pSetValFsSnmpProxyMibTargetParamsIn)
#define nmhSetFsSnmpProxyMibSingleTargetOut(pFsSnmpProxyMibName ,pSetValFsSnmpProxyMibSingleTargetOut) \
 nmhSetCmn(FsSnmpProxyMibSingleTargetOut, 11, FsSnmpProxyMibSingleTargetOutSet, NULL, NULL, 0, 0, 1, "%s %s", pFsSnmpProxyMibName ,pSetValFsSnmpProxyMibSingleTargetOut)
#define nmhSetFsSnmpProxyMibMultipleTargetOut(pFsSnmpProxyMibName ,pSetValFsSnmpProxyMibMultipleTargetOut) \
 nmhSetCmn(FsSnmpProxyMibMultipleTargetOut, 11, FsSnmpProxyMibMultipleTargetOutSet, NULL, NULL, 0, 0, 1, "%s %s", pFsSnmpProxyMibName ,pSetValFsSnmpProxyMibMultipleTargetOut)
#define nmhSetFsSnmpProxyMibStorageType(pFsSnmpProxyMibName ,i4SetValFsSnmpProxyMibStorageType) \
 nmhSetCmn(FsSnmpProxyMibStorageType, 11, FsSnmpProxyMibStorageTypeSet, NULL, NULL, 0, 0, 1, "%s %i", pFsSnmpProxyMibName ,i4SetValFsSnmpProxyMibStorageType)
#define nmhSetFsSnmpProxyMibRowStatus(pFsSnmpProxyMibName ,i4SetValFsSnmpProxyMibRowStatus) \
 nmhSetCmn(FsSnmpProxyMibRowStatus, 11, FsSnmpProxyMibRowStatusSet, NULL, NULL, 0, 1, 1, "%s %i", pFsSnmpProxyMibName ,i4SetValFsSnmpProxyMibRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsSnmpListenAgentPort[9];
extern UINT4 FsSnmpEngineID[9];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsSnmpListenAgentPort(u4SetValFsSnmpListenAgentPort) \
 nmhSetCmn(FsSnmpListenAgentPort, 9, FsSnmpListenAgentPortSet, NULL, NULL, 0, 0, 0, "%u", u4SetValFsSnmpListenAgentPort)
#define nmhSetFsSnmpEngineID(pSetValFsSnmpEngineID) \
 nmhSetCmn(FsSnmpEngineID, 9, FsSnmpEngineIDSet, NULL, NULL, 0, 0, 0, "%s", pSetValFsSnmpEngineID)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 SnmpAgentxTransportDomain[10];
extern UINT4 SnmpAgentxMasterAgentAddr[10];
extern UINT4 SnmpAgentxMasterAgentPortNo[10];
extern UINT4 SnmpAgentxSubAgentControl[10];
extern UINT4 SnmpAgentxContextName[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetSnmpAgentxTransportDomain(i4SetValSnmpAgentxTransportDomain) \
 nmhSetCmn(SnmpAgentxTransportDomain, 10, SnmpAgentxTransportDomainSet, NULL, NULL, 0, 0, 0, "%i", i4SetValSnmpAgentxTransportDomain)
#define nmhSetSnmpAgentxMasterAgentAddr(pSetValSnmpAgentxMasterAgentAddr) \
 nmhSetCmn(SnmpAgentxMasterAgentAddr, 10, SnmpAgentxMasterAgentAddrSet, NULL, NULL, 0, 0, 0, "%s", pSetValSnmpAgentxMasterAgentAddr)
#define nmhSetSnmpAgentxMasterAgentPortNo(u4SetValSnmpAgentxMasterAgentPortNo) \
 nmhSetCmn(SnmpAgentxMasterAgentPortNo, 10, SnmpAgentxMasterAgentPortNoSet, NULL, NULL, 0, 0, 0, "%u", u4SetValSnmpAgentxMasterAgentPortNo)
#define nmhSetSnmpAgentxSubAgentControl(i4SetValSnmpAgentxSubAgentControl) \
 nmhSetCmn(SnmpAgentxSubAgentControl, 10, SnmpAgentxSubAgentControlSet, NULL, NULL, 0, 0, 0, "%i", i4SetValSnmpAgentxSubAgentControl)
#define nmhSetSnmpAgentxContextName(pSetValSnmpAgentxContextName) \
 nmhSetCmn(SnmpAgentxContextName, 10, SnmpAgentxContextNameSet, NULL, NULL, 0, 0, 0, "%s", pSetValSnmpAgentxContextName)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsSnmpTrapFilterOID[12];
extern UINT4 FsSnmpTrapFilterRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsSnmpTrapFilterRowStatus(pFsSnmpTrapFilterOID ,i4SetValFsSnmpTrapFilterRowStatus) \
 nmhSetCmn(FsSnmpTrapFilterRowStatus, 12, FsSnmpTrapFilterRowStatusSet, NULL, NULL, 0, 1, 1, "%o %i", pFsSnmpTrapFilterOID ,i4SetValFsSnmpTrapFilterRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsSnmpTargetHostName[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsSnmpTargetHostName(pSnmpTargetAddrName ,pSetValFsSnmpTargetHostName) \
 nmhSetCmn(FsSnmpTargetHostName, 12, FsSnmpTargetHostNameSet, NULL, NULL, 0, 0, 1, "%s %s", pSnmpTargetAddrName ,pSetValFsSnmpTargetHostName)

#endif
