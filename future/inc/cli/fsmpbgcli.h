/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpbgcli.h,v 1.13 2016/12/27 12:35:59 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIBgp4GlobalTraceDebug[10];
extern UINT4 FsMIBgp4LocalAs[10];
extern UINT4 FsMIBgp4MaxPeerEntry[10];
extern UINT4 FsMIBgp4MaxNoofRoutes[10];
extern UINT4 FsMIBgp4GRAdminStatus[10];
extern UINT4 FsMIBgp4GRRestartTimeInterval[10];
extern UINT4 FsMIBgp4GRSelectionDeferralTimeInterval[10];
extern UINT4 FsMIBgp4GRStaleTimeInterval[10];
extern UINT4 FsMIBgp4RestartSupport[10];
extern UINT4 FsMIBgp4MacMobDuplicationTimeInterval[10];
extern UINT4 FsMIBgp4MaxMacMoves[10];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIBgp4ContextId[12];
extern UINT4 FsMIBgp4GlobalAdminStatus[12];
extern UINT4 FsMIBgp4Identifier[12];
extern UINT4 FsMIBgp4Synchronization[12];
extern UINT4 FsMIBgp4DefaultLocalPref[12];
extern UINT4 FsMIBgp4AdvtNonBgpRt[12];
extern UINT4 FsMIBgp4TraceEnable[12];
extern UINT4 FsMIBgp4DebugEnable[12];
extern UINT4 FsMIBgp4DebugType[12];
extern UINT4 FsMIBgp4DebugTypeIPAddr[12];
extern UINT4 FsMIBgp4OverlappingRoute[12];
extern UINT4 FsMIBgp4AlwaysCompareMED[12];
extern UINT4 FsMIBgp4DefaultOriginate[12];
extern UINT4 FsMIBgp4DefaultIpv4UniCast[12];
extern UINT4 FsMIBgp4IsTrapEnabled[12];
extern UINT4 FsMIBgp4NextHopProcessingInterval[12];
extern UINT4 FsMIBgp4IBGPRedistributionStatus[12];
extern UINT4 FsMIBgp4RRDAdminStatus[12];
extern UINT4 FsMIBgp4RRDProtoMaskForEnable[12];
extern UINT4 FsMIBgp4RRDSrcProtoMaskForDisable[12];
extern UINT4 FsMIBgp4RRDDefaultMetric[12];
extern UINT4 FsMIBgp4RRDRouteMapName[12];
extern UINT4 FsMIBgp4RRDMatchTypeEnable[12];
extern UINT4 FsMIBgp4RRDMatchTypeDisable[12];
extern UINT4 FsMIBgp4AscConfedId[12];
extern UINT4 FsMIBgp4AscConfedBestPathCompareMED[12];
extern UINT4 FsMIBgp4RflbgpClusterId[12];
extern UINT4 FsMIBgp4RflRflSupport[12];
extern UINT4 FsMIBgp4RfdCutOff[12];
extern UINT4 FsMIBgp4RfdReuse[12];
extern UINT4 FsMIBgp4RfdMaxHoldDownTime[12];
extern UINT4 FsMIBgp4RfdDecayHalfLifeTime[12];
extern UINT4 FsMIBgp4RfdDecayTimerGranularity[12];
extern UINT4 FsMIBgp4RfdReuseTimerGranularity[12];
extern UINT4 FsMIBgp4RfdReuseIndxArraySize[12];
extern UINT4 FsMIBgp4RfdAdminStatus[12];
extern UINT4 FsMIBgp4CommMaxInFTblEntries[12];
extern UINT4 FsMIBgp4CommMaxOutFTblEntries[12];
extern UINT4 FsMIBgp4ExtCommMaxInFTblEntries[12];
extern UINT4 FsMIBgp4ExtCommMaxOutFTblEntries[12];
extern UINT4 FsMIBgp4CapabilitySupportAvailable[12];
extern UINT4 FsMIBgp4MaxCapsPerPeer[12];
extern UINT4 FsMIBgp4MaxInstancesPerCap[12];
extern UINT4 FsMIBgp4MaxCapDataSize[12];
extern UINT4 FsMIBgp4PreferenceValue[12];
extern UINT4 FsMIBgp4ContextStatus[12];
extern UINT4 FsMIBgp4IBGPMaxPaths[12];
extern UINT4 FsMIBgp4EBGPMaxPaths[12];
extern UINT4 FsMIBgp4EIBGPMaxPaths[12];
extern UINT4 FsMIBgp4FourByteASNSupportStatus[12];
extern UINT4 FsMIBgp4FourByteASNotationType[12];
extern UINT4 FsMIBgp4LocalAsNo[12];
extern UINT4 FsMIBgp4VpnLabelAllocPolicy[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIBgp4RRDMetricProtocolId[12];
extern UINT4 FsMIBgp4RRDMetricValue[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIBgp4InFilterCommVal[13];
extern UINT4 FsMIBgp4CommIncomingFilterStatus[13];
extern UINT4 FsMIBgp4InFilterRowStatus[13];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIBgp4OutFilterCommVal[13];
extern UINT4 FsMIBgp4CommOutgoingFilterStatus[13];
extern UINT4 FsMIBgp4OutFilterRowStatus[13];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIBgp4ExtCommInFilterCommVal[13];
extern UINT4 FsMIBgp4ExtCommIncomingFilterStatus[13];
extern UINT4 FsMIBgp4ExtCommInFilterRowStatus[13];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIBgp4ExtCommOutFilterCommVal[13];
extern UINT4 FsMIBgp4ExtCommOutgoingFilterStatus[13];
extern UINT4 FsMIBgp4ExtCommOutFilterRowStatus[13];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIBgp4TCPMD5AuthPeerType[13];
extern UINT4 FsMIBgp4TCPMD5AuthPeerAddr[13];
extern UINT4 FsMIBgp4TCPMD5AuthPassword[13];
extern UINT4 FsMIBgp4TCPMD5AuthPwdSet[13];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIBgpAscConfedPeerASNo[13];
extern UINT4 FsMIBgpAscConfedPeerStatus[13];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIBgp4mpebgpPeerRemoteAddrType[12];
extern UINT4 FsMIBgp4mpebgpPeerLocalAs[12];
extern UINT4 FsMIBgp4mpebgpPeerAdminStatus[12];
extern UINT4 FsMIBgp4mpebgpPeerRemoteAddr[12];
extern UINT4 FsMIBgp4mpebgpPeerConnectRetryInterval[12];
extern UINT4 FsMIBgp4mpebgpPeerHoldTimeConfigured[12];
extern UINT4 FsMIBgp4mpebgpPeerKeepAliveConfigured[12];
extern UINT4 FsMIBgp4mpebgpPeerMinASOriginationInterval[12];
extern UINT4 FsMIBgp4mpebgpPeerMinRouteAdvertisementInterval[12];
extern UINT4 FsMIBgp4mpePeerAllowAutomaticStart[12];
extern UINT4 FsMIBgp4mpePeerAllowAutomaticStop[12];
extern UINT4 FsMIBgp4mpebgpPeerIdleHoldTimeConfigured[12];
extern UINT4 FsMIBgp4mpeDampPeerOscillations[12];
extern UINT4 FsMIBgp4mpePeerDelayOpen[12];
extern UINT4 FsMIBgp4mpebgpPeerDelayOpenTimeConfigured[12];
extern UINT4 FsMIBgp4mpePeerPrefixUpperLimit[12];
extern UINT4 FsMIBgp4mpePeerTcpConnectRetryCnt[12];
extern UINT4 FsMIBgp4mpePeerTCPAOAuthNoMKTDiscard[12];
extern UINT4 FsMIBgp4mpePeerTCPAOAuthICMPAccept[12];
extern UINT4 FsMIBgp4mpePeerIpPrefixNameIn[12];
extern UINT4 FsMIBgp4mpePeerIpPrefixNameOut[12];
extern UINT4 FsMIBgp4mpePeerBfdStatus[12];
extern UINT4 FsMIBgp4mpePeerHoldAdvtRoutes[12];



/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIBgp4mpePeerExtPeerType[12];
extern UINT4 FsMIBgp4mpePeerExtPeerRemoteAddr[12];
extern UINT4 FsMIBgp4mpePeerExtConfigurePeer[12];
extern UINT4 FsMIBgp4mpePeerExtPeerRemoteAs[12];
extern UINT4 FsMIBgp4mpePeerExtEBGPMultiHop[12];
extern UINT4 FsMIBgp4mpePeerExtEBGPHopLimit[12];
extern UINT4 FsMIBgp4mpePeerExtNextHopSelf[12];
extern UINT4 FsMIBgp4mpePeerExtRflClient[12];
extern UINT4 FsMIBgp4mpePeerExtTcpSendBufSize[12];
extern UINT4 FsMIBgp4mpePeerExtTcpRcvBufSize[12];
extern UINT4 FsMIBgp4mpePeerExtLclAddress[12];
extern UINT4 FsMIBgp4mpePeerExtNetworkAddress[12];
extern UINT4 FsMIBgp4mpePeerExtGateway[12];
extern UINT4 FsMIBgp4mpePeerExtCommSendStatus[12];
extern UINT4 FsMIBgp4mpePeerExtECommSendStatus[12];
extern UINT4 FsMIBgp4mpePeerExtPassive[12];
extern UINT4 FsMIBgp4mpePeerExtDefaultOriginate[12];
extern UINT4 FsMIBgp4mpePeerExtOverrideCapability[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIBgp4mpeMEDIndex[12];
extern UINT4 FsMIBgp4mpeMEDAdminStatus[12];
extern UINT4 FsMIBgp4mpeMEDRemoteAS[12];
extern UINT4 FsMIBgp4mpeMEDIPAddrAfi[12];
extern UINT4 FsMIBgp4mpeMEDIPAddrSafi[12];
extern UINT4 FsMIBgp4mpeMEDIPAddrPrefix[12];
extern UINT4 FsMIBgp4mpeMEDIPAddrPrefixLen[12];
extern UINT4 FsMIBgp4mpeMEDIntermediateAS[12];
extern UINT4 FsMIBgp4mpeMEDDirection[12];
extern UINT4 FsMIBgp4mpeMEDValue[12];
extern UINT4 FsMIBgp4mpeMEDPreference[12];
extern UINT4 FsMIBgp4mpeMEDVrfName[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIBgp4mpeLocalPrefIndex[12];
extern UINT4 FsMIBgp4mpeLocalPrefAdminStatus[12];
extern UINT4 FsMIBgp4mpeLocalPrefRemoteAS[12];
extern UINT4 FsMIBgp4mpeLocalPrefIPAddrAfi[12];
extern UINT4 FsMIBgp4mpeLocalPrefIPAddrSafi[12];
extern UINT4 FsMIBgp4mpeLocalPrefIPAddrPrefix[12];
extern UINT4 FsMIBgp4mpeLocalPrefIPAddrPrefixLen[12];
extern UINT4 FsMIBgp4mpeLocalPrefIntermediateAS[12];
extern UINT4 FsMIBgp4mpeLocalPrefDirection[12];
extern UINT4 FsMIBgp4mpeLocalPrefValue[12];
extern UINT4 FsMIBgp4mpeLocalPrefPreference[12];
extern UINT4 FsMIBgp4mpeLocalPrefVrfName[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIBgp4mpeUpdateFilterIndex[12];
extern UINT4 FsMIBgp4mpeUpdateFilterAdminStatus[12];
extern UINT4 FsMIBgp4mpeUpdateFilterRemoteAS[12];
extern UINT4 FsMIBgp4mpeUpdateFilterIPAddrAfi[12];
extern UINT4 FsMIBgp4mpeUpdateFilterIPAddrSafi[12];
extern UINT4 FsMIBgp4mpeUpdateFilterIPAddrPrefix[12];
extern UINT4 FsMIBgp4mpeUpdateFilterIPAddrPrefixLen[12];
extern UINT4 FsMIBgp4mpeUpdateFilterIntermediateAS[12];
extern UINT4 FsMIBgp4mpeUpdateFilterDirection[12];
extern UINT4 FsMIBgp4mpeUpdateFilterAction[12];
extern UINT4 FsMIBgp4mpeUpdateFilterVrfName[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIBgp4mpeAggregateIndex[12];
extern UINT4 FsMIBgp4mpeAggregateAdminStatus[12];
extern UINT4 FsMIBgp4mpeAggregateIPAddrAfi[12];
extern UINT4 FsMIBgp4mpeAggregateIPAddrSafi[12];
extern UINT4 FsMIBgp4mpeAggregateIPAddrPrefix[12];
extern UINT4 FsMIBgp4mpeAggregateIPAddrPrefixLen[12];
extern UINT4 FsMIBgp4mpeAggregateAdvertise[12];
extern UINT4 FsMIBgp4mpeAggregateVrfName[12];
extern UINT4 FsMIBgp4mpeAggregateAsSet[12];
extern UINT4 FsMIBgp4mpeAggregateAdvertiseRouteMapName[12];
extern UINT4 FsMIBgp4mpeAggregateSuppressRouteMapName[12];
extern UINT4 FsMIBgp4mpeAggregateAttributeRouteMapName[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIBgp4mpeImportRoutePrefixAfi[12];
extern UINT4 FsMIBgp4mpeImportRoutePrefixSafi[12];
extern UINT4 FsMIBgp4mpeImportRoutePrefix[12];
extern UINT4 FsMIBgp4mpeImportRoutePrefixLen[12];
extern UINT4 FsMIBgp4mpeImportRouteProtocol[12];
extern UINT4 FsMIBgp4mpeImportRouteNextHop[12];
extern UINT4 FsMIBgp4mpeImportRouteIfIndex[12];
extern UINT4 FsMIBgp4mpeImportRouteMetric[12];
extern UINT4 FsMIBgp4mpeImportRouteVrf[12];
extern UINT4 FsMIBgp4mpeImportRouteAction[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIBgp4mpeAddCommRtAfi[13];
extern UINT4 FsMIBgp4mpeAddCommRtSafi[13];
extern UINT4 FsMIBgp4mpeAddCommIpNetwork[13];
extern UINT4 FsMIBgp4mpeAddCommIpPrefixLen[13];
extern UINT4 FsMIBgp4mpeAddCommVal[13];
extern UINT4 FsMIBgp4mpeAddCommRowStatus[13];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIBgp4mpeDeleteCommRtAfi[13];
extern UINT4 FsMIBgp4mpeDeleteCommRtSafi[13];
extern UINT4 FsMIBgp4mpeDeleteCommIpNetwork[13];
extern UINT4 FsMIBgp4mpeDeleteCommIpPrefixLen[13];
extern UINT4 FsMIBgp4mpeDeleteCommVal[13];
extern UINT4 FsMIBgp4mpeDeleteCommRowStatus[13];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIBgp4mpeCommSetStatusAfi[13];
extern UINT4 FsMIBgp4mpeCommSetStatusSafi[13];
extern UINT4 FsMIBgp4mpeCommSetStatusIpNetwork[13];
extern UINT4 FsMIBgp4mpeCommSetStatusIpPrefixLen[13];
extern UINT4 FsMIBgp4mpeCommSetStatus[13];
extern UINT4 FsMIBgp4mpeCommSetStatusRowStatus[13];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIBgp4mpeAddExtCommRtAfi[13];
extern UINT4 FsMIBgp4mpeAddExtCommRtSafi[13];
extern UINT4 FsMIBgp4mpeAddExtCommIpNetwork[13];
extern UINT4 FsMIBgp4mpeAddExtCommIpPrefixLen[13];
extern UINT4 FsMIBgp4mpeAddExtCommVal[13];
extern UINT4 FsMIBgp4mpeAddExtCommRowStatus[13];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIBgp4mpeDeleteExtCommRtAfi[13];
extern UINT4 FsMIBgp4mpeDeleteExtCommRtSafi[13];
extern UINT4 FsMIBgp4mpeDeleteExtCommIpNetwork[13];
extern UINT4 FsMIBgp4mpeDeleteExtCommIpPrefixLen[13];
extern UINT4 FsMIBgp4mpeDeleteExtCommVal[13];
extern UINT4 FsMIBgp4mpeDeleteExtCommRowStatus[13];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIBgp4mpeExtCommSetStatusRtAfi[13];
extern UINT4 FsMIBgp4mpeExtCommSetStatusRtSafi[13];
extern UINT4 FsMIBgp4mpeExtCommSetStatusIpNetwork[13];
extern UINT4 FsMIBgp4mpeExtCommSetStatusIpPrefixLen[13];
extern UINT4 FsMIBgp4mpeExtCommSetStatus[13];
extern UINT4 FsMIBgp4mpeExtCommSetStatusRowStatus[13];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIBgp4mpePeerLinkType[13];
extern UINT4 FsMIBgp4mpePeerLinkRemAddr[13];
extern UINT4 FsMIBgp4mpeLinkBandWidth[13];
extern UINT4 FsMIBgp4mpePeerLinkBwRowStatus[13];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIBgp4mpeCapPeerType[13];
extern UINT4 FsMIBgp4mpeCapPeerRemoteIpAddr[13];
extern UINT4 FsMIBgp4mpeSupportedCapabilityCode[13];
extern UINT4 FsMIBgp4mpeSupportedCapabilityLength[13];
extern UINT4 FsMIBgp4mpeSupportedCapabilityValue[13];
extern UINT4 FsMIBgp4mpeCapSupportedCapsRowStatus[13];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIBgp4mpeRtRefreshInboundPeerType[13];
extern UINT4 FsMIBgp4mpeRtRefreshInboundPeerAddr[13];
extern UINT4 FsMIBgp4mpeRtRefreshInboundAfi[13];
extern UINT4 FsMIBgp4mpeRtRefreshInboundSafi[13];
extern UINT4 FsMIBgp4mpeRtRefreshInboundRequest[13];
extern UINT4 FsMIBgp4mpeRtRefreshInboundPrefixFilter[13];



/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIBgp4mpeSoftReconfigOutboundPeerType[13];
extern UINT4 FsMIBgp4mpeSoftReconfigOutboundPeerAddr[13];
extern UINT4 FsMIBgp4mpeSoftReconfigOutboundAfi[13];
extern UINT4 FsMIBgp4mpeSoftReconfigOutboundSafi[13];
extern UINT4 FsMIBgp4mpeSoftReconfigOutboundRequest[13];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIBgp4DistInOutRouteMapName[13];
extern UINT4 FsMIBgp4DistInOutRouteMapType[13];
extern UINT4 FsMIBgp4DistInOutRouteMapValue[13];
extern UINT4 FsMIBgp4DistInOutRouteMapRowStatus[13];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIBgp4NeighborRouteMapPeerAddrType[13];
extern UINT4 FsMIBgp4NeighborRouteMapPeer[13];
extern UINT4 FsMIBgp4NeighborRouteMapDirection[13];
extern UINT4 FsMIBgp4NeighborRouteMapName[13];
extern UINT4 FsMIBgp4NeighborRouteMapRowStatus[13];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIBgp4PeerGroupName[12];
extern UINT4 FsMIBgp4PeerGroupRemoteAs[12];
extern UINT4 FsMIBgp4PeerGroupHoldTimeConfigured[12];
extern UINT4 FsMIBgp4PeerGroupKeepAliveConfigured[12];
extern UINT4 FsMIBgp4PeerGroupConnectRetryInterval[12];
extern UINT4 FsMIBgp4PeerGroupMinASOriginInterval[12];
extern UINT4 FsMIBgp4PeerGroupMinRouteAdvInterval[12];
extern UINT4 FsMIBgp4PeerGroupAllowAutomaticStart[12];
extern UINT4 FsMIBgp4PeerGroupAllowAutomaticStop[12];
extern UINT4 FsMIBgp4PeerGroupIdleHoldTimeConfigured[12];
extern UINT4 FsMIBgp4PeerGroupDampPeerOscillations[12];
extern UINT4 FsMIBgp4PeerGroupDelayOpen[12];
extern UINT4 FsMIBgp4PeerGroupDelayOpenTimeConfigured[12];
extern UINT4 FsMIBgp4PeerGroupPrefixUpperLimit[12];
extern UINT4 FsMIBgp4PeerGroupTcpConnectRetryCnt[12];
extern UINT4 FsMIBgp4PeerGroupEBGPMultiHop[12];
extern UINT4 FsMIBgp4PeerGroupEBGPHopLimit[12];
extern UINT4 FsMIBgp4PeerGroupNextHopSelf[12];
extern UINT4 FsMIBgp4PeerGroupRflClient[12];
extern UINT4 FsMIBgp4PeerGroupTcpSendBufSize[12];
extern UINT4 FsMIBgp4PeerGroupTcpRcvBufSize[12];
extern UINT4 FsMIBgp4PeerGroupCommSendStatus[12];
extern UINT4 FsMIBgp4PeerGroupECommSendStatus[12];
extern UINT4 FsMIBgp4PeerGroupPassive[12];
extern UINT4 FsMIBgp4PeerGroupDefaultOriginate[12];
extern UINT4 FsMIBgp4PeerGroupActivateMPCapability[12];
extern UINT4 FsMIBgp4PeerGroupDeactivateMPCapability[12];
extern UINT4 FsMIBgp4PeerGroupRouteMapNameIn[12];
extern UINT4 FsMIBgp4PeerGroupRouteMapNameOut[12];
extern UINT4 FsMIBgp4PeerGroupStatus[12];
extern UINT4 FsMIBgp4PeerGroupIpPrefixNameIn[12];
extern UINT4 FsMIBgp4PeerGroupIpPrefixNameOut[12];
extern UINT4 FsMIBgp4PeerGroupOrfType[12];
extern UINT4 FsMIBgp4PeerGroupOrfCapMode[12];
extern UINT4 FsMIBgp4PeerGroupOrfRequest[12];
extern UINT4 FsMIBgp4PeerGroupBfdStatus[12];
extern UINT4 FsMIBgp4PeerGroupOverrideCapability[12];
extern UINT4 FsMIBgp4PeerGroupLocalAs[12];



/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIBgp4PeerAddrType[12];
extern UINT4 FsMIBgp4PeerAddress[12];
extern UINT4 FsMIBgp4PeerAddStatus[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIBgp4RestartReason[10];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIBgp4TCPMKTAuthKeyId[13];
extern UINT4 FsMIBgp4TCPMKTAuthRecvKeyId[13];
extern UINT4 FsMIBgp4TCPMKTAuthMasterKey[13];
extern UINT4 FsMIBgp4TCPMKTAuthAlgo[13];
extern UINT4 FsMIBgp4TCPMKTAuthTcpOptExc[13];
extern UINT4 FsMIBgp4TCPMKTAuthRowStatus[13];



/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIBgp4TCPAOAuthPeerType[13];
extern UINT4 FsMIBgp4TCPAOAuthPeerAddr[13];
extern UINT4 FsMIBgp4TCPAOAuthKeyId[13];
extern UINT4 FsMIBgp4TCPAOAuthKeyStatus[13];
extern UINT4 FsMIBgp4TCPAOAuthKeyStartAccept[13];
extern UINT4 FsMIBgp4TCPAOAuthKeyStartGenerate[13];
extern UINT4 FsMIBgp4TCPAOAuthKeyStopGenerate[13];
extern UINT4 FsMIBgp4TCPAOAuthKeyStopAccept[13];

extern UINT4 FsMIBgp4Ipv4AddrFamily[12];
extern UINT4 FsMIBgp4Ipv6AddrFamily[12];
extern UINT4 FsMIBgp4VPNV4AddrFamily[12];
extern UINT4 FsMIBgp4L2vpnAddrFamily[12];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIBgp4RRDNetworkAddr[12];
extern UINT4 FsMIBgp4RRDNetworkAddrType[12];
extern UINT4 FsMIBgp4RRDNetworkPrefixLen[12];
extern UINT4 FsMIBgp4RRDNetworkRowStatus[12];
