/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsvcmcli.h,v 1.4 2009/08/24 13:12:47 prabuc Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsVcmTraceOption[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsVcmTraceOption(i4SetValFsVcmTraceOption)	\
	nmhSetCmnWithLock(FsVcmTraceOption, 10, FsVcmTraceOptionSet, VcmConfLock, VcmConfUnLock, 0, 0, 0, "%i", i4SetValFsVcmTraceOption)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsVCId[12];
extern UINT4 FsVcAlias[12];
extern UINT4 FsVCStatus[12];
extern UINT4 FsVcCxtType[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsVcAlias(i4FsVCId ,pSetValFsVcAlias)	\
	nmhSetCmnWithLock(FsVcAlias, 12, FsVcAliasSet, VcmConfLock, VcmConfUnLock, 0, 0, 1, "%i %s", i4FsVCId ,pSetValFsVcAlias)
#define nmhSetFsVCStatus(i4FsVCId ,i4SetValFsVCStatus)	\
	nmhSetCmnWithLock(FsVCStatus, 12, FsVCStatusSet, VcmConfLock, VcmConfUnLock, 0, 1, 1, "%i %i", i4FsVCId ,i4SetValFsVCStatus)
#define nmhSetFsVcCxtType(i4FsVCId ,i4SetValFsVcCxtType)    \
    nmhSetCmnWithLock(FsVcCxtType, 12, FsVcCxtTypeSet, VcmConfLock, VcmConfUnLock, 0, 0, 1, "%i %i", i4FsVCId ,i4SetValFsVcCxtType)
#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsVcmIfIndex[12];
extern UINT4 FsVcId[12];
extern UINT4 FsVcIfRowStatus[12];
extern UINT4 FsVcL2ContextId[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsVcId(i4FsVcmIfIndex ,i4SetValFsVcId)	\
	nmhSetCmnWithLock(FsVcId, 12, FsVcIdSet, VcmConfLock, VcmConfUnLock, 0, 0, 1, "%i %i", i4FsVcmIfIndex ,i4SetValFsVcId)
#define nmhSetFsVcIfRowStatus(i4FsVcmIfIndex ,i4SetValFsVcIfRowStatus)	\
	nmhSetCmnWithLock(FsVcIfRowStatus, 12, FsVcIfRowStatusSet, VcmConfLock, VcmConfUnLock, 0, 1, 1, "%i %i", i4FsVcmIfIndex ,i4SetValFsVcIfRowStatus)

#define nmhSetFsVcL2ContextId(i4FsVcmIfIndex ,i4SetValFsVcL2ContextId)  \
    nmhSetCmnWithLock(FsVcL2ContextId, 12, FsVcL2ContextIdSet, VcmConfLock, VcmConfUnLock, 0, 0, 1, "%i %i", i4FsVcmIfIndex ,i4SetValFsVcL2ContextId)
#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsVcIpIfAddr[12];
extern UINT4 FsVcIpId[12];
extern UINT4 FsVcIpIfRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsVcIpId(u4FsVcIpIfAddr ,i4SetValFsVcIpId)	\
	nmhSetCmnWithLock(FsVcIpId, 12, FsVcIpIdSet, VcmConfLock, VcmConfUnLock, 0, 0, 1, "%p %i", u4FsVcIpIfAddr ,i4SetValFsVcIpId)
#define nmhSetFsVcIpIfRowStatus(u4FsVcIpIfAddr ,i4SetValFsVcIpIfRowStatus)	\
	nmhSetCmnWithLock(FsVcIpIfRowStatus, 12, FsVcIpIfRowStatusSet, VcmConfLock, VcmConfUnLock, 0, 1, 1, "%p %i", u4FsVcIpIfAddr ,i4SetValFsVcIpIfRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsVcOwner[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsVcOwner(i4FsVCId ,pSetValFsVcOwner)	\
	nmhSetCmnWithLock(FsVcOwner, 12, FsVcOwnerSet, VcmConfLock, VcmConfUnLock, 0, 0, 1, "%i %s", i4FsVCId ,pSetValFsVcOwner)

#endif
