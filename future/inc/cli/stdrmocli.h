/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdrmocli.h,v 1.6 2014/06/24 11:31:52 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 EtherStatsDataSource[11];
extern UINT4 EtherStatsOwner[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetEtherStatsDataSource(i4EtherStatsIndex ,pSetValEtherStatsDataSource) \
 nmhSetCmn(EtherStatsDataSource, 11, EtherStatsDataSourceSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %o", i4EtherStatsIndex ,pSetValEtherStatsDataSource)
#define nmhSetEtherStatsOwner(i4EtherStatsIndex ,pSetValEtherStatsOwner) \
 nmhSetCmn(EtherStatsOwner, 11, EtherStatsOwnerSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %s", i4EtherStatsIndex ,pSetValEtherStatsOwner)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 HistoryControlDataSource[11];
extern UINT4 HistoryControlBucketsRequested[11];
extern UINT4 HistoryControlInterval[11];
extern UINT4 HistoryControlOwner[11];
extern UINT4 HistoryControlStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetHistoryControlDataSource(i4HistoryControlIndex ,pSetValHistoryControlDataSource) \
 nmhSetCmn(HistoryControlDataSource, 11, HistoryControlDataSourceSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %o", i4HistoryControlIndex ,pSetValHistoryControlDataSource)
#define nmhSetHistoryControlBucketsRequested(i4HistoryControlIndex ,i4SetValHistoryControlBucketsRequested) \
 nmhSetCmn(HistoryControlBucketsRequested, 11, HistoryControlBucketsRequestedSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %i", i4HistoryControlIndex ,i4SetValHistoryControlBucketsRequested)
#define nmhSetHistoryControlInterval(i4HistoryControlIndex ,i4SetValHistoryControlInterval) \
 nmhSetCmn(HistoryControlInterval, 11, HistoryControlIntervalSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %i", i4HistoryControlIndex ,i4SetValHistoryControlInterval)
#define nmhSetHistoryControlOwner(i4HistoryControlIndex ,pSetValHistoryControlOwner) \
 nmhSetCmn(HistoryControlOwner, 11, HistoryControlOwnerSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %s", i4HistoryControlIndex ,pSetValHistoryControlOwner)
#define nmhSetHistoryControlStatus(i4HistoryControlIndex ,i4SetValHistoryControlStatus) \
 nmhSetCmn(HistoryControlStatus, 11, HistoryControlStatusSet, RmonLock, RmonUnLock, 0, 1, 1, "%i %i", i4HistoryControlIndex ,i4SetValHistoryControlStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 AlarmInterval[11];
extern UINT4 AlarmVariable[11];
extern UINT4 AlarmSampleType[11];
extern UINT4 AlarmStartupAlarm[11];
extern UINT4 AlarmRisingThreshold[11];
extern UINT4 AlarmFallingThreshold[11];
extern UINT4 AlarmRisingEventIndex[11];
extern UINT4 AlarmFallingEventIndex[11];
extern UINT4 AlarmOwner[11];
extern UINT4 AlarmStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetAlarmInterval(i4AlarmIndex ,i4SetValAlarmInterval) \
 nmhSetCmn(AlarmInterval, 11, AlarmIntervalSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %i", i4AlarmIndex ,i4SetValAlarmInterval)
#define nmhSetAlarmVariable(i4AlarmIndex ,pSetValAlarmVariable) \
 nmhSetCmn(AlarmVariable, 11, AlarmVariableSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %o", i4AlarmIndex ,pSetValAlarmVariable)
#define nmhSetAlarmSampleType(i4AlarmIndex ,i4SetValAlarmSampleType) \
 nmhSetCmn(AlarmSampleType, 11, AlarmSampleTypeSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %i", i4AlarmIndex ,i4SetValAlarmSampleType)
#define nmhSetAlarmStartupAlarm(i4AlarmIndex ,i4SetValAlarmStartupAlarm) \
 nmhSetCmn(AlarmStartupAlarm, 11, AlarmStartupAlarmSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %i", i4AlarmIndex ,i4SetValAlarmStartupAlarm)
#define nmhSetAlarmRisingThreshold(i4AlarmIndex ,i4SetValAlarmRisingThreshold) \
 nmhSetCmn(AlarmRisingThreshold, 11, AlarmRisingThresholdSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %i", i4AlarmIndex ,i4SetValAlarmRisingThreshold)
#define nmhSetAlarmFallingThreshold(i4AlarmIndex ,i4SetValAlarmFallingThreshold) \
 nmhSetCmn(AlarmFallingThreshold, 11, AlarmFallingThresholdSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %i", i4AlarmIndex ,i4SetValAlarmFallingThreshold)
#define nmhSetAlarmRisingEventIndex(i4AlarmIndex ,i4SetValAlarmRisingEventIndex) \
 nmhSetCmn(AlarmRisingEventIndex, 11, AlarmRisingEventIndexSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %i", i4AlarmIndex ,i4SetValAlarmRisingEventIndex)
#define nmhSetAlarmFallingEventIndex(i4AlarmIndex ,i4SetValAlarmFallingEventIndex) \
 nmhSetCmn(AlarmFallingEventIndex, 11, AlarmFallingEventIndexSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %i", i4AlarmIndex ,i4SetValAlarmFallingEventIndex)
#define nmhSetAlarmOwner(i4AlarmIndex ,pSetValAlarmOwner) \
 nmhSetCmn(AlarmOwner, 11, AlarmOwnerSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %s", i4AlarmIndex ,pSetValAlarmOwner)
#define nmhSetAlarmStatus(i4AlarmIndex ,i4SetValAlarmStatus) \
 nmhSetCmn(AlarmStatus, 11, AlarmStatusSet, RmonLock, RmonUnLock, 0, 1, 1, "%i %i", i4AlarmIndex ,i4SetValAlarmStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 HostControlDataSource[11];
extern UINT4 HostControlOwner[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetHostControlDataSource(i4HostControlIndex ,pSetValHostControlDataSource) \
 nmhSetCmn(HostControlDataSource, 11, HostControlDataSourceSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %o", i4HostControlIndex ,pSetValHostControlDataSource)
#define nmhSetHostControlOwner(i4HostControlIndex ,pSetValHostControlOwner) \
 nmhSetCmn(HostControlOwner, 11, HostControlOwnerSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %s", i4HostControlIndex ,pSetValHostControlOwner)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 HostTopNHostIndex[11];
extern UINT4 HostTopNRateBase[11];
extern UINT4 HostTopNTimeRemaining[11];
extern UINT4 HostTopNRequestedSize[11];
extern UINT4 HostTopNOwner[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetHostTopNHostIndex(i4HostTopNControlIndex ,i4SetValHostTopNHostIndex) \
 nmhSetCmn(HostTopNHostIndex, 11, HostTopNHostIndexSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %i", i4HostTopNControlIndex ,i4SetValHostTopNHostIndex)
#define nmhSetHostTopNRateBase(i4HostTopNControlIndex ,i4SetValHostTopNRateBase) \
 nmhSetCmn(HostTopNRateBase, 11, HostTopNRateBaseSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %i", i4HostTopNControlIndex ,i4SetValHostTopNRateBase)
#define nmhSetHostTopNTimeRemaining(i4HostTopNControlIndex ,i4SetValHostTopNTimeRemaining) \
 nmhSetCmn(HostTopNTimeRemaining, 11, HostTopNTimeRemainingSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %i", i4HostTopNControlIndex ,i4SetValHostTopNTimeRemaining)
#define nmhSetHostTopNRequestedSize(i4HostTopNControlIndex ,i4SetValHostTopNRequestedSize) \
 nmhSetCmn(HostTopNRequestedSize, 11, HostTopNRequestedSizeSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %i", i4HostTopNControlIndex ,i4SetValHostTopNRequestedSize)
#define nmhSetHostTopNOwner(i4HostTopNControlIndex ,pSetValHostTopNOwner) \
 nmhSetCmn(HostTopNOwner, 11, HostTopNOwnerSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %s", i4HostTopNControlIndex ,pSetValHostTopNOwner)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 MatrixControlDataSource[11];
extern UINT4 MatrixControlOwner[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetMatrixControlDataSource(i4MatrixControlIndex ,pSetValMatrixControlDataSource) \
 nmhSetCmn(MatrixControlDataSource, 11, MatrixControlDataSourceSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %o", i4MatrixControlIndex ,pSetValMatrixControlDataSource)
#define nmhSetMatrixControlOwner(i4MatrixControlIndex ,pSetValMatrixControlOwner) \
 nmhSetCmn(MatrixControlOwner, 11, MatrixControlOwnerSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %s", i4MatrixControlIndex ,pSetValMatrixControlOwner)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FilterChannelIndex[11];
extern UINT4 FilterPktDataOffset[11];
extern UINT4 FilterPktData[11];
extern UINT4 FilterPktDataMask[11];
extern UINT4 FilterPktDataNotMask[11];
extern UINT4 FilterPktStatus[11];
extern UINT4 FilterPktStatusMask[11];
extern UINT4 FilterPktStatusNotMask[11];
extern UINT4 FilterOwner[11];
extern UINT4 FilterStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFilterChannelIndex(i4FilterIndex ,i4SetValFilterChannelIndex) \
 nmhSetCmn(FilterChannelIndex, 11, FilterChannelIndexSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %i", i4FilterIndex ,i4SetValFilterChannelIndex)
#define nmhSetFilterPktDataOffset(i4FilterIndex ,i4SetValFilterPktDataOffset) \
 nmhSetCmn(FilterPktDataOffset, 11, FilterPktDataOffsetSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %i", i4FilterIndex ,i4SetValFilterPktDataOffset)
#define nmhSetFilterPktData(i4FilterIndex ,pSetValFilterPktData) \
 nmhSetCmn(FilterPktData, 11, FilterPktDataSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %s", i4FilterIndex ,pSetValFilterPktData)
#define nmhSetFilterPktDataMask(i4FilterIndex ,pSetValFilterPktDataMask) \
 nmhSetCmn(FilterPktDataMask, 11, FilterPktDataMaskSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %s", i4FilterIndex ,pSetValFilterPktDataMask)
#define nmhSetFilterPktDataNotMask(i4FilterIndex ,pSetValFilterPktDataNotMask) \
 nmhSetCmn(FilterPktDataNotMask, 11, FilterPktDataNotMaskSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %s", i4FilterIndex ,pSetValFilterPktDataNotMask)
#define nmhSetFilterPktStatus(i4FilterIndex ,i4SetValFilterPktStatus) \
 nmhSetCmn(FilterPktStatus, 11, FilterPktStatusSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %i", i4FilterIndex ,i4SetValFilterPktStatus)
#define nmhSetFilterPktStatusMask(i4FilterIndex ,i4SetValFilterPktStatusMask) \
 nmhSetCmn(FilterPktStatusMask, 11, FilterPktStatusMaskSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %i", i4FilterIndex ,i4SetValFilterPktStatusMask)
#define nmhSetFilterPktStatusNotMask(i4FilterIndex ,i4SetValFilterPktStatusNotMask) \
 nmhSetCmn(FilterPktStatusNotMask, 11, FilterPktStatusNotMaskSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %i", i4FilterIndex ,i4SetValFilterPktStatusNotMask)
#define nmhSetFilterOwner(i4FilterIndex ,pSetValFilterOwner) \
 nmhSetCmn(FilterOwner, 11, FilterOwnerSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %s", i4FilterIndex ,pSetValFilterOwner)
#define nmhSetFilterStatus(i4FilterIndex ,i4SetValFilterStatus) \
 nmhSetCmn(FilterStatus, 11, FilterStatusSet, RmonLock, RmonUnLock, 0, 1, 1, "%i %i", i4FilterIndex ,i4SetValFilterStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 ChannelIfIndex[11];
extern UINT4 ChannelAcceptType[11];
extern UINT4 ChannelDataControl[11];
extern UINT4 ChannelTurnOnEventIndex[11];
extern UINT4 ChannelTurnOffEventIndex[11];
extern UINT4 ChannelEventIndex[11];
extern UINT4 ChannelEventStatus[11];
extern UINT4 ChannelDescription[11];
extern UINT4 ChannelOwner[11];
extern UINT4 ChannelStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetChannelIfIndex(i4ChannelIndex ,i4SetValChannelIfIndex) \
 nmhSetCmn(ChannelIfIndex, 11, ChannelIfIndexSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %i", i4ChannelIndex ,i4SetValChannelIfIndex)
#define nmhSetChannelAcceptType(i4ChannelIndex ,i4SetValChannelAcceptType) \
 nmhSetCmn(ChannelAcceptType, 11, ChannelAcceptTypeSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %i", i4ChannelIndex ,i4SetValChannelAcceptType)
#define nmhSetChannelDataControl(i4ChannelIndex ,i4SetValChannelDataControl) \
 nmhSetCmn(ChannelDataControl, 11, ChannelDataControlSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %i", i4ChannelIndex ,i4SetValChannelDataControl)
#define nmhSetChannelTurnOnEventIndex(i4ChannelIndex ,i4SetValChannelTurnOnEventIndex) \
 nmhSetCmn(ChannelTurnOnEventIndex, 11, ChannelTurnOnEventIndexSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %i", i4ChannelIndex ,i4SetValChannelTurnOnEventIndex)
#define nmhSetChannelTurnOffEventIndex(i4ChannelIndex ,i4SetValChannelTurnOffEventIndex) \
 nmhSetCmn(ChannelTurnOffEventIndex, 11, ChannelTurnOffEventIndexSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %i", i4ChannelIndex ,i4SetValChannelTurnOffEventIndex)
#define nmhSetChannelEventIndex(i4ChannelIndex ,i4SetValChannelEventIndex) \
 nmhSetCmn(ChannelEventIndex, 11, ChannelEventIndexSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %i", i4ChannelIndex ,i4SetValChannelEventIndex)
#define nmhSetChannelEventStatus(i4ChannelIndex ,i4SetValChannelEventStatus) \
 nmhSetCmn(ChannelEventStatus, 11, ChannelEventStatusSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %i", i4ChannelIndex ,i4SetValChannelEventStatus)
#define nmhSetChannelDescription(i4ChannelIndex ,pSetValChannelDescription) \
 nmhSetCmn(ChannelDescription, 11, ChannelDescriptionSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %s", i4ChannelIndex ,pSetValChannelDescription)
#define nmhSetChannelOwner(i4ChannelIndex ,pSetValChannelOwner) \
 nmhSetCmn(ChannelOwner, 11, ChannelOwnerSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %s", i4ChannelIndex ,pSetValChannelOwner)
#define nmhSetChannelStatus(i4ChannelIndex ,i4SetValChannelStatus) \
 nmhSetCmn(ChannelStatus, 11, ChannelStatusSet, RmonLock, RmonUnLock, 0, 1, 1, "%i %i", i4ChannelIndex ,i4SetValChannelStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 BufferControlChannelIndex[11];
extern UINT4 BufferControlFullAction[11];
extern UINT4 BufferControlCaptureSliceSize[11];
extern UINT4 BufferControlDownloadSliceSize[11];
extern UINT4 BufferControlDownloadOffset[11];
extern UINT4 BufferControlMaxOctetsRequested[11];
extern UINT4 BufferControlOwner[11];
extern UINT4 BufferControlStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetBufferControlChannelIndex(i4BufferControlIndex ,i4SetValBufferControlChannelIndex) \
 nmhSetCmn(BufferControlChannelIndex, 11, BufferControlChannelIndexSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %i", i4BufferControlIndex ,i4SetValBufferControlChannelIndex)
#define nmhSetBufferControlFullAction(i4BufferControlIndex ,i4SetValBufferControlFullAction) \
 nmhSetCmn(BufferControlFullAction, 11, BufferControlFullActionSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %i", i4BufferControlIndex ,i4SetValBufferControlFullAction)
#define nmhSetBufferControlCaptureSliceSize(i4BufferControlIndex ,i4SetValBufferControlCaptureSliceSize) \
 nmhSetCmn(BufferControlCaptureSliceSize, 11, BufferControlCaptureSliceSizeSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %i", i4BufferControlIndex ,i4SetValBufferControlCaptureSliceSize)
#define nmhSetBufferControlDownloadSliceSize(i4BufferControlIndex ,i4SetValBufferControlDownloadSliceSize) \
 nmhSetCmn(BufferControlDownloadSliceSize, 11, BufferControlDownloadSliceSizeSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %i", i4BufferControlIndex ,i4SetValBufferControlDownloadSliceSize)
#define nmhSetBufferControlDownloadOffset(i4BufferControlIndex ,i4SetValBufferControlDownloadOffset) \
 nmhSetCmn(BufferControlDownloadOffset, 11, BufferControlDownloadOffsetSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %i", i4BufferControlIndex ,i4SetValBufferControlDownloadOffset)
#define nmhSetBufferControlMaxOctetsRequested(i4BufferControlIndex ,i4SetValBufferControlMaxOctetsRequested) \
 nmhSetCmn(BufferControlMaxOctetsRequested, 11, BufferControlMaxOctetsRequestedSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %i", i4BufferControlIndex ,i4SetValBufferControlMaxOctetsRequested)
#define nmhSetBufferControlOwner(i4BufferControlIndex ,pSetValBufferControlOwner) \
 nmhSetCmn(BufferControlOwner, 11, BufferControlOwnerSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %s", i4BufferControlIndex ,pSetValBufferControlOwner)
#define nmhSetBufferControlStatus(i4BufferControlIndex ,i4SetValBufferControlStatus) \
 nmhSetCmn(BufferControlStatus, 11, BufferControlStatusSet, RmonLock, RmonUnLock, 0, 1, 1, "%i %i", i4BufferControlIndex ,i4SetValBufferControlStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 EventDescription[11];
extern UINT4 EventType[11];
extern UINT4 EventCommunity[11];
extern UINT4 EventOwner[11];
extern UINT4 EventStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetEventDescription(i4EventIndex ,pSetValEventDescription) \
 nmhSetCmn(EventDescription, 11, EventDescriptionSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %s", i4EventIndex ,pSetValEventDescription)
#define nmhSetEventType(i4EventIndex ,i4SetValEventType) \
 nmhSetCmn(EventType, 11, EventTypeSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %i", i4EventIndex ,i4SetValEventType)
#define nmhSetEventCommunity(i4EventIndex ,pSetValEventCommunity) \
 nmhSetCmn(EventCommunity, 11, EventCommunitySet, RmonLock, RmonUnLock, 0, 0, 1, "%i %s", i4EventIndex ,pSetValEventCommunity)
#define nmhSetEventOwner(i4EventIndex ,pSetValEventOwner) \
 nmhSetCmn(EventOwner, 11, EventOwnerSet, RmonLock, RmonUnLock, 0, 0, 1, "%i %s", i4EventIndex ,pSetValEventOwner)
#define nmhSetEventStatus(i4EventIndex ,i4SetValEventStatus) \
 nmhSetCmn(EventStatus, 11, EventStatusSet, RmonLock, RmonUnLock, 0, 1, 1, "%i %i", i4EventIndex ,i4SetValEventStatus)

#endif
