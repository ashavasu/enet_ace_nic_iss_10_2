/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fstunlcli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsTunlIfAddressType[13];
extern UINT4 FsTunlIfLocalInetAddress[13];
extern UINT4 FsTunlIfRemoteInetAddress[13];
extern UINT4 FsTunlIfEncapsMethod[13];
extern UINT4 FsTunlIfConfigID[13];
extern UINT4 FsTunlIfHopLimit[13];
extern UINT4 FsTunlIfTOS[13];
extern UINT4 FsTunlIfFlowLabel[13];
extern UINT4 FsTunlIfDirFlag[13];
extern UINT4 FsTunlIfDirection[13];
extern UINT4 FsTunlIfEncaplmt[13];
extern UINT4 FsTunlIfEncapOption[13];
extern UINT4 FsTunlIfAlias[13];
extern UINT4 FsTunlIfCksumFlag[13];
extern UINT4 FsTunlIfPmtuFlag[13];
extern UINT4 FsTunlIfStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsTunlIfHopLimit(i4FsTunlIfAddressType , pFsTunlIfLocalInetAddress , pFsTunlIfRemoteInetAddress , i4FsTunlIfEncapsMethod , i4FsTunlIfConfigID ,i4SetValFsTunlIfHopLimit)	\
	nmhSetCmn(FsTunlIfHopLimit, 13, FsTunlIfHopLimitSet, CfaLock, CfaUnlock, 0, 0, 5, "%i %s %s %i %i %i", i4FsTunlIfAddressType , pFsTunlIfLocalInetAddress , pFsTunlIfRemoteInetAddress , i4FsTunlIfEncapsMethod , i4FsTunlIfConfigID ,i4SetValFsTunlIfHopLimit)
#define nmhSetFsTunlIfTOS(i4FsTunlIfAddressType , pFsTunlIfLocalInetAddress , pFsTunlIfRemoteInetAddress , i4FsTunlIfEncapsMethod , i4FsTunlIfConfigID ,i4SetValFsTunlIfTOS)	\
	nmhSetCmn(FsTunlIfTOS, 13, FsTunlIfTOSSet, CfaLock, CfaUnlock, 0, 0, 5, "%i %s %s %i %i %i", i4FsTunlIfAddressType , pFsTunlIfLocalInetAddress , pFsTunlIfRemoteInetAddress , i4FsTunlIfEncapsMethod , i4FsTunlIfConfigID ,i4SetValFsTunlIfTOS)
#define nmhSetFsTunlIfFlowLabel(i4FsTunlIfAddressType , pFsTunlIfLocalInetAddress , pFsTunlIfRemoteInetAddress , i4FsTunlIfEncapsMethod , i4FsTunlIfConfigID ,i4SetValFsTunlIfFlowLabel)	\
	nmhSetCmn(FsTunlIfFlowLabel, 13, FsTunlIfFlowLabelSet, CfaLock, CfaUnlock, 0, 0, 5, "%i %s %s %i %i %i", i4FsTunlIfAddressType , pFsTunlIfLocalInetAddress , pFsTunlIfRemoteInetAddress , i4FsTunlIfEncapsMethod , i4FsTunlIfConfigID ,i4SetValFsTunlIfFlowLabel)
#define nmhSetFsTunlIfDirFlag(i4FsTunlIfAddressType , pFsTunlIfLocalInetAddress , pFsTunlIfRemoteInetAddress , i4FsTunlIfEncapsMethod , i4FsTunlIfConfigID ,i4SetValFsTunlIfDirFlag)	\
	nmhSetCmn(FsTunlIfDirFlag, 13, FsTunlIfDirFlagSet, CfaLock, CfaUnlock, 0, 0, 5, "%i %s %s %i %i %i", i4FsTunlIfAddressType , pFsTunlIfLocalInetAddress , pFsTunlIfRemoteInetAddress , i4FsTunlIfEncapsMethod , i4FsTunlIfConfigID ,i4SetValFsTunlIfDirFlag)
#define nmhSetFsTunlIfDirection(i4FsTunlIfAddressType , pFsTunlIfLocalInetAddress , pFsTunlIfRemoteInetAddress , i4FsTunlIfEncapsMethod , i4FsTunlIfConfigID ,i4SetValFsTunlIfDirection)	\
	nmhSetCmn(FsTunlIfDirection, 13, FsTunlIfDirectionSet, CfaLock, CfaUnlock, 0, 0, 5, "%i %s %s %i %i %i", i4FsTunlIfAddressType , pFsTunlIfLocalInetAddress , pFsTunlIfRemoteInetAddress , i4FsTunlIfEncapsMethod , i4FsTunlIfConfigID ,i4SetValFsTunlIfDirection)
#define nmhSetFsTunlIfEncaplmt(i4FsTunlIfAddressType , pFsTunlIfLocalInetAddress , pFsTunlIfRemoteInetAddress , i4FsTunlIfEncapsMethod , i4FsTunlIfConfigID ,u4SetValFsTunlIfEncaplmt)	\
	nmhSetCmn(FsTunlIfEncaplmt, 13, FsTunlIfEncaplmtSet, CfaLock, CfaUnlock, 0, 0, 5, "%i %s %s %i %i %u", i4FsTunlIfAddressType , pFsTunlIfLocalInetAddress , pFsTunlIfRemoteInetAddress , i4FsTunlIfEncapsMethod , i4FsTunlIfConfigID ,u4SetValFsTunlIfEncaplmt)
#define nmhSetFsTunlIfEncapOption(i4FsTunlIfAddressType , pFsTunlIfLocalInetAddress , pFsTunlIfRemoteInetAddress , i4FsTunlIfEncapsMethod , i4FsTunlIfConfigID ,i4SetValFsTunlIfEncapOption)	\
	nmhSetCmn(FsTunlIfEncapOption, 13, FsTunlIfEncapOptionSet, CfaLock, CfaUnlock, 0, 0, 5, "%i %s %s %i %i %i", i4FsTunlIfAddressType , pFsTunlIfLocalInetAddress , pFsTunlIfRemoteInetAddress , i4FsTunlIfEncapsMethod , i4FsTunlIfConfigID ,i4SetValFsTunlIfEncapOption)
#define nmhSetFsTunlIfAlias(i4FsTunlIfAddressType , pFsTunlIfLocalInetAddress , pFsTunlIfRemoteInetAddress , i4FsTunlIfEncapsMethod , i4FsTunlIfConfigID ,pSetValFsTunlIfAlias)	\
	nmhSetCmn(FsTunlIfAlias, 13, FsTunlIfAliasSet, CfaLock, CfaUnlock, 0, 0, 5, "%i %s %s %i %i %s", i4FsTunlIfAddressType , pFsTunlIfLocalInetAddress , pFsTunlIfRemoteInetAddress , i4FsTunlIfEncapsMethod , i4FsTunlIfConfigID ,pSetValFsTunlIfAlias)
#define nmhSetFsTunlIfCksumFlag(i4FsTunlIfAddressType , pFsTunlIfLocalInetAddress , pFsTunlIfRemoteInetAddress , i4FsTunlIfEncapsMethod , i4FsTunlIfConfigID ,i4SetValFsTunlIfCksumFlag)	\
	nmhSetCmn(FsTunlIfCksumFlag, 13, FsTunlIfCksumFlagSet, CfaLock, CfaUnlock, 0, 0, 5, "%i %s %s %i %i %i", i4FsTunlIfAddressType , pFsTunlIfLocalInetAddress , pFsTunlIfRemoteInetAddress , i4FsTunlIfEncapsMethod , i4FsTunlIfConfigID ,i4SetValFsTunlIfCksumFlag)
#define nmhSetFsTunlIfPmtuFlag(i4FsTunlIfAddressType , pFsTunlIfLocalInetAddress , pFsTunlIfRemoteInetAddress , i4FsTunlIfEncapsMethod , i4FsTunlIfConfigID ,i4SetValFsTunlIfPmtuFlag)	\
	nmhSetCmn(FsTunlIfPmtuFlag, 13, FsTunlIfPmtuFlagSet, CfaLock, CfaUnlock, 0, 0, 5, "%i %s %s %i %i %i", i4FsTunlIfAddressType , pFsTunlIfLocalInetAddress , pFsTunlIfRemoteInetAddress , i4FsTunlIfEncapsMethod , i4FsTunlIfConfigID ,i4SetValFsTunlIfPmtuFlag)
#define nmhSetFsTunlIfStatus(i4FsTunlIfAddressType , pFsTunlIfLocalInetAddress , pFsTunlIfRemoteInetAddress , i4FsTunlIfEncapsMethod , i4FsTunlIfConfigID ,i4SetValFsTunlIfStatus)	\
	nmhSetCmn(FsTunlIfStatus, 13, FsTunlIfStatusSet, CfaLock, CfaUnlock, 0, 1, 5, "%i %s %s %i %i %i", i4FsTunlIfAddressType , pFsTunlIfLocalInetAddress , pFsTunlIfRemoteInetAddress , i4FsTunlIfEncapsMethod , i4FsTunlIfConfigID ,i4SetValFsTunlIfStatus)

#endif
