#ifndef __DSCXECLI_H__
#define __DSCXECLI_H__

#include "cli.h"

/*===========================================================================*/
/*                     Defines used in commands file                         */
/*===========================================================================*/
#define DS_MAX_NAME_LENGTH          32
#define CLI_CLASSMAP_MODE          "ClassMap"
#define CLI_POLICYMAP_MODE         "PolicyMap"
#define CLI_POLICYCLASS_MODE       "PolClassMap"
#define CLI_TRAFFICCLASS_MODE      "TrafficClass"
#define DS_MAX_ARGS                 5

/* Type of identifier */
#define DS_MAC_FILTER                1     
#define DS_IP_FILTER                 2

/* Actions to be performed on in profile, out profile and no match conditions */
#define DS_DSCP                      2   /* insert dscp */
#define DS_PREC                      3   /* insert precedence or IP TOS */
#define DS_COS                       4   /* insert cos or new priority */


/*===========================================================================*/
/*                       enums used by DiffServ CLI                          */
/*===========================================================================*/

/* DiffServ CLI commands : to add a new command, a new value needs to be 
 * added to this enum 
 */
enum 
{
   CLI_DS_SYSTEM_CONTROL = 1,       /* start / shutdown system */
   CLI_DS_SHUT,                    /* shutdown system */
   CLI_DS_NO_SHUT,                 /* start the system */
   CLI_DS_CLASS_MAP,         	/* create/delete a class map  */
   CLI_DS_NO_CLASS_MAP,         	/* create/delete a class map  */
   CLI_DS_TC_CREATE,              /* Create a traffic class */
   CLI_DS_TC_NO_CREATE,           /* Delete a traffic class */
   CLI_DS_RANDOM_DETECT,
   CLI_DS_NO_RANDOM_DETECT,
   CLI_DS_DSCP_QUEUE_MAP,
   CLI_DS_QUEUE_PREC_SWITCHED,
   CLI_DS_QUEUE_PREC_ROUTED,
   CLI_DS_CLASS_MAP_MATCH,         /* Configure  class map matching criteria */
   CLI_DS_POL_MAP,                  /* create/delete a policy map  */
   CLI_DS_NO_POL_MAP,                  /* create/delete a policy map  */
   CLI_DS_POLCL_MAP,                /* map or unmap a classmap to a policy map  */
   CLI_DS_NO_POLCL_MAP,              /* map or unmap a classmap to a policy map  */
   CLI_DS_POL_IN_ACTION,            /* configure a policy map with inprofile */
   CLI_DS_NO_POL_IN_ACTION,            /* configure a policy map with inprofile */
   CLI_DS_TC_MAP,                 /* Map a traffic class to a policy */
   CLI_DS_TC_NO_MAP,              /* Unmap a traffic class from a policy */
   CLI_DS_TC_MAX_BW,              /* Set max. bandwidth for the traffic class*/ 
   CLI_DS_TC_MAX_BW_DROP,  
   CLI_DS_TC_NO_MAX_BW,
   CLI_DS_TC_NO_MAX_BW_DROP,
   CLI_DS_TC_GUARNTEED_BW,        /* Set guaranteed bandwidth */
   CLI_DS_TC_NO_GUARNTEED_BW,
   CLI_DS_TC_QUEUE_THRESHOLD,     /* Set drop levels for the traffic class */
   CLI_DS_TC_NO_QUEUE_THRESHOLD, 
   CLI_DS_TC_FAIRQUEUE,           /* Enable flow groups and set ther number */
   CLI_DS_TC_NO_FAIRQUEUE,        /* Disable flow groups */
   CLI_DS_TC_WEIGHT, 
   CLI_DS_TC_NO_WEIGHT, 
   CLI_DS_TC_RED_MAP,             /* Associate a RED curve with the traffic
                                     class */
   CLI_DS_TC_NO_RED_MAP,
   CLI_DS_PORT_MAX_BW,
   CLI_DS_PORT_MAX_BW_DROP,
   CLI_DS_PORT_NO_MAX_BW,
   CLI_DS_PORT_NO_MAX_BW_DROP,
   CLI_DS_PORT_QUEUE_THRESHOLD,
   CLI_DS_PORT_NO_QUEUE_THRESHOLD,
   CLI_DS_PORT_RED_MAP,
   CLI_DS_PORT_NO_RED_MAP,
   CLI_SHOW_DS_POLICYMAP,           /* show policy map details */
   CLI_SHOW_DS_CLASSMAP,             /* show class map details */
   CLI_SHOW_DS_DSCP_QUEUE_MAP,
   CLI_SHOW_DS_TC_FLOWGROUPS_MAP,
   CLI_SHOW_DS_QUEUE_PREC,
   CLI_SHOW_DS_TRAFFICCLASS,
   CLI_SHOW_DS_INTERFACE,
   CLI_SHOW_DS_RED,
   CLI_SHOW_DS_STATISTICS,
   DS_CLI_MAX_COMMANDS
};

/* Type of identifier */
enum
{
   DS_IND_MAC_FILTER = 1,
   DS_IND_IP_FILTER,
   DS_IND_CLASS_MAP,
   DS_IND_POLICY_MAP
};

/* Actions to be performed on in profile, out profile and no match conditions */
enum
{
   DS_ACT_DSCP,                 /* insert dscp */
   DS_ACT_PREC,                 /* insert precedence / IP TOS */
   DS_ACT_COS                   /* insert cos / new priority */
};

enum 
{
    DS_WEIGHT_FACTOR,
    DS_WEIGHT_SHIFT
};

/*===========================================================================*/
/*                     Structures used by DiffServ CLI                       */
/*===========================================================================*/

/* In profile, out profile or no match action details */

typedef struct
{

   INT4    i4ActionId;     /* Id associated with the action */
   UINT4   u4Dscp;         /* dscp to be inserted */
   UINT4   u4Cos;          /* cos or new priority to be inserted */
   UINT4   u4Precedence;   /* precedence or IP TOS to be inserted */
   INT4    i4Action;       /* The action : drop, dscp, cos, precedence */
   
}tDsAction;

/* classifier details */

typedef struct
{
   INT4   i4DsClfrId;          /* classifier id */
   INT4   i4DsMFClfrId;        /* multifield classifier id */
   INT4   i4DsInProActId;      /* in profile action id */
   INT4   i4DsTrafficClassId;  /* traffic class id */
}tDsClfr;


/* Error code values and strings are maintained inside the protocol 
 * module itself.
 * */
enum {
        CLI_DS_FATAL_ERR = 1,
        CLI_DS_CLASS_MAX_LIMIT_ERR,
        CLI_DS_POLICY_MAX_LIMIT_ERR,
        CLI_DS_INPROF_MAX_LIMIT_ERR,
        CLI_DS_OUTPROF_MAX_LIMIT_ERR,
        CLI_DS_NOMATCH_MAX_LIMIT_ERR,
        CLI_DS_MFC_CHANGE_CLASS_ACTIVE_ERR,
        CLI_DS_FILTER_INACTIVE_ERR,
        CLI_DS_CLASS_INACTIVE_ERR,
        CLI_DS_TC_RESERVED_ERR,
        CLI_DS_INVALID_TC_ERR, 
        CLI_DS_TC_OUTPORT_MISMATCH_ERR,
        CLI_DS_TC_PORT_INDEP_ERR,
        CLI_DS_TC_BW_EXCEED_ERR,
        CLI_DS_DROP_AT_MAXBW_ERR, 
        CLI_DS_FLOWGROUPS_UNAVAIL_ERR,
        CLI_DS_INVALID_RED_CURVE_ERR, 
        CLI_DS_TC_IN_USE_ERR,
        CLI_DS_QRANGE_EXCEED_ERR,
        CLI_DS_RED_IN_USE_ERR,
        CLI_DS_PORT_MAXBW_ERR,
        CLI_DS_MAX_ERR /* This should always be the last */
};
#ifdef __DSCXECLI_C__
CONST CHR1  *DsCliErrString [] = {
        NULL,
        "% FATAL ERROR: Command Failed \r\n",
        "% Class-Map creation failed, max-limit exceeded \r\n",
        "% Policy-Map creation failed, max-limit exceeded \r\n",
        "% In-Profile creation failed, max-limit exceeded \r\n",
        "% Out-Profile creation failed, max-limit exceeded \r\n",
        "% No-Match creation failed, max-limit exceeded \r\n",
        "% Changing class failed, policy already active \r\n",
        "% Filter-match failed, filter inconsistent or already mapped to a class\r\n",
        "% Class set failed, class not active \r\n",
        "% Traffic-class assign failed, reserved traffic class \r\n",
        "% Traffic-class assign failed, traffic class not created or not active yet \r\n",
        "% Traffic-class assign failed, mismatch with output port in ACL \r\n",
        "% Parameter not supported on port independent Traffic-class \r\n",
        "% Bandwidth set failed, total guaranteed bandwidth exceeds port's max. bandwidth \r\n",
        "% Drop threshold set failed since bandwidth exceed-action is set \r\n",
        "% Setting fair-queue, requested number of flow groups unavailable \r\n",
        "% RED Curve association failed, curve not created or not active \r\n",
        "% Traffic class deletion failed, being used by a policy \r\n",
        "% REDCurve creation failed, difference between RED Start and Stop exceeds 256 MB \r\n",
        "% REDCurve deletion failed, being used by a port or traffic class \r\n",
        "% Max-Bandwidth set failed, atleast 1 traffic class should have been attached to this port\r\n"
};
#endif

INT4  DsCreateMFClfrFromMacFilter (tCliHandle CliHandle,
                                   INT4 i4MacFilterNo,
                                   INT4 i4MFClfrId);

INT4  DsCreateMFClfrFromIpFilter (tCliHandle CliHandle,
                                  INT4 i4IpFilterNo,
                                  INT4 i4MFClfrId);

INT4 DiffservNoPolicyClass(tCliHandle CliHandle, INT4 i4ClassMap);
    
INT4  DsCreateClfr (tCliHandle CliHandle, tDsClfr *pDsClfr, BOOLEAN u1NewFlag);

INT4 DiffservPolicyClass PROTO ((tCliHandle , INT4 ));

INT4 DsCreateInProfAction PROTO ((tCliHandle CliHandle, tDsAction * pDsAction));

INT4 DiffservSystemControl PROTO ((tCliHandle , UINT4));

INT4 DiffservCreateClassMap PROTO ((tCliHandle, INT4 ));

INT4 DiffservDestroyClassMap PROTO ((tCliHandle , INT4 ));

INT4 DiffservCreatePolicyMap PROTO ((tCliHandle, INT4 ));

INT4 DiffservDestroyPolicyMap PROTO ((tCliHandle , INT4 ));

INT4 DiffservMatchFilter PROTO ((tCliHandle, UINT4 ,INT4));

INT4 DiffservInProfAction PROTO ((tCliHandle , UINT4, UINT4 ));

INT4 DiffservShutdown (tCliHandle CliHandle, UINT4 u4Status);

INT4 DiffservNoInProfAction PROTO ((tCliHandle CliHandle, UINT4 u4InProfType,
                                    UINT4 u4Value));

INT4 DsDeleteInProfAction PROTO ((tCliHandle CliHandle,INT4 i4PolicyIndex,
                                  UINT4 u4Flag));

INT1 DiffsrvGetClassMapPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1PromptStr));

INT1 DiffsrvGetPolicyMapPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1PromptStr));

INT1 DiffsrvGetPolicyClassMapPrompt PROTO ((INT1 *pi1ModeName,
                                            INT1 *pi1PromptStr));
INT4 DiffservShowPolicyMap PROTO ((tCliHandle CliHandle , INT4 u4PolIndex,
                                   INT4 u4ClassIndex ));

INT4 DiffservShowClassMap PROTO ((tCliHandle CliHandle , UINT4 u4ClassIndex ));


INT4 DiffservCreateTC (tCliHandle, INT4 i4TrafficClassId, INT4 i4OutPort);

INT4 DiffservDeleteTC (tCliHandle, INT4 i4TrafficClassId);

INT1 DiffsrvGetTrafficClassPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);
    
INT4 DiffservSetTCWeight (tCliHandle CliHandle, INT4 i4Value, INT4 i4Select);

INT4 DiffservResetTCWeight (tCliHandle CliHandle, INT4 i4Select);

INT4 DiffservSetREDParam (tCliHandle, INT4 i4REDCurveID, /* ID */
                                      INT4 i4REDCurveStart, /* Curve Start */
                                      INT4 i4REDCurveStop, /* Curve Stop */
                                      INT4 i4REDCurveQRange, /* QRange */
                                      INT4 i4REDCurveStopProb);/* StopProbability*/ 
                
INT4 DiffservDeleteREDCurve (tCliHandle, INT4 i4REDCurveID);

INT4 DiffservSetDSCPQueueMap (tCliHandle, UINT4 u4DSCPValue, UINT4 u4QueueID);

INT4 DiffservSetQueuePrecSwitched (tCliHandle, UINT4 u4COSQPrecedence,
                                               UINT4 u4IPTOSQPrecedence);
            
INT4 DiffservSetQueuePrecRouted (tCliHandle, UINT4 u4COSQPrecedence,
                                               UINT4 u4IPTOSQPrecedence);
    
INT4 DiffservMapTC (tCliHandle, INT4 i4TrafficClassId);

INT4 DiffservUnmapTC (tCliHandle);

INT4 DiffservSetTCMaxBw (tCliHandle, INT4 i4MaxBw, INT4 i4DropOption);
INT4 DiffservResetTCMaxBw (tCliHandle, INT4 i4DropOption);

INT4 DiffservSetTCGuarntdBw (tCliHandle, INT4 i4GurantdBw);
INT4 DiffservResetTCGuarntdBw (tCliHandle);
INT4 DiffservSetTCDropLevels (tCliHandle, INT4 i4Level1,
                              INT4 i4Level2, INT4 i4Level3);
INT4 DiffservResetTCDropLevels (tCliHandle);
INT4 DiffservSetTCREDMap (tCliHandle, INT4 i4REDCurveID);
INT4 DiffservSetTCNumFlowGroups (tCliHandle, INT4 i4NumFlowGroups);
INT4 DiffservSetPortMaxBw (tCliHandle, INT4 i4MaxBw, INT4 i4DropOption);
INT4 DiffservResetPortMaxBw (tCliHandle, INT4 i4DropOption);
INT4 DiffservSetPortDropLevels (tCliHandle, INT4 Level1,
                                INT4 i4Level2, INT4 i4Level3);
INT4 DiffservResetPortDropLevels (tCliHandle);
INT4 DiffservSetPortREDMap (tCliHandle, INT4 i4REDCurveID);
INT4 DiffservShowDscpQueueMap (tCliHandle);
INT4 DiffservShowTCFlowGroupsMap (tCliHandle CliHandle);
INT4 DiffservShowQueuePrecedence (tCliHandle);
INT4 DiffservShowTrafficClass (tCliHandle, INT4 i4TrafficClass);
INT4 DiffservShowREDParams (tCliHandle, INT4 i4REDCurveId);
INT4 DiffservShowQosInterface (tCliHandle, INT4 i4PortNum);
INT4 DiffservShowQosStatistics (tCliHandle);


#ifdef NPAPI_WANTED
INT4 DiffservShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module);
INT4 DiffservShowRunningConfigInterfaceDetails (tCliHandle CliHandle,
                                                INT4 i4PortNum);
INT4 DiffservClassMapShowRunningConfig (tCliHandle CliHandle);
INT4 DiffservPolicyMapShowRunningConfig (tCliHandle CliHandle);
INT4 DiffservShowRunningConfigScalars (tCliHandle CliHandle);
INT4 DiffservTrafficClassShowRunningConfig (tCliHandle CliHandle);
INT4 DiffservREDCurveShowRunningConfig (tCliHandle CliHandle);
#endif


extern tIssL2FilterEntry *IssGetL2FilterEntry PROTO ((INT4));

extern tIssL3FilterEntry *IssGetL3FilterEntry PROTO ((INT4));

INT4 cli_process_ds_cmd PROTO ((tCliHandle CliHandle,UINT4, ...));


/*===========================================================================*/
#endif   /* __DSCXECLI_H__ */
