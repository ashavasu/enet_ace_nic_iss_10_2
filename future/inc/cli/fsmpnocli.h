/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpnocli.h,v 1.1 2010/10/29 19:20:26 prabuc Exp $*
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsPwStatusNotifEnable[11];
extern UINT4 FsMplsPwOAMStatusNotifEnable[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsPwStatusNotifEnable(i4SetValFsMplsPwStatusNotifEnable)	\
	nmhSetCmnWithLock(FsMplsPwStatusNotifEnable, 11, FsMplsPwStatusNotifEnableSet, L2vpnLock, L2vpnUnLock, 0, 0, 0, "%i", i4SetValFsMplsPwStatusNotifEnable)
#define nmhSetFsMplsPwOAMStatusNotifEnable(i4SetValFsMplsPwOAMStatusNotifEnable)	\
	nmhSetCmnWithLock(FsMplsPwOAMStatusNotifEnable, 11, FsMplsPwOAMStatusNotifEnableSet, L2vpnLock, L2vpnUnLock, 0, 0, 0, "%i", i4SetValFsMplsPwOAMStatusNotifEnable)

#endif
