#ifndef __ACLMCLI_H__
#define __ACLMCLI_H__
#include "cli.h"

/* Command identifier to distinguish default values*/
#define ACL_DEF_SVLAN            0
#define ACL_DEF_SVLAN_PRIO       -1
#define ACL_DEF_CVLAN            0
#define ACL_DEF_CVLAN_PRIO       -1
#define ACL_DEF_ETYPE            0


INT4 AclPbTestL3Filter PROTO ((UINT4 *, tCliHandle, INT4, INT4, INT4,
                               INT4, INT4));

INT4 AclPbGetL3Filter PROTO ((INT4, INT4 *, INT4 *, INT4 *, INT4 *, INT4 *));
    
INT4 AclPbGetL2Filter PROTO ((INT4, INT4 *, INT4 *, INT4 *, INT4 *, INT4 *));
INT4 AclPbTestSetL2TagType PROTO ((UINT4 *, tCliHandle , INT4 , INT4 ));
INT4 AclPbTestSetL3TagType PROTO ((UINT4 *, tCliHandle , INT4 , INT4 ));

INT4 AclPbSetL3Filter PROTO ((tCliHandle, INT4, INT4, INT4, INT4, INT4));

INT4 AclPbTestL2Filter PROTO ((UINT4 *,tCliHandle, INT4, INT4, INT4, INT4,
                               INT4));

INT4 AclPbSetL2Filter PROTO ((tCliHandle, INT4, INT4, INT4, INT4, INT4));

INT4 AclPbShowL3Filter PROTO ((tCliHandle, INT4, INT4, INT4, INT4, INT4));

INT4 AclPbShowL2Filter PROTO ((tCliHandle, INT4, INT4, INT4, INT4, INT4));

#endif
