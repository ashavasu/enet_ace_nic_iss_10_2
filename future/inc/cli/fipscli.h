/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: fipscli.h,v 1.7 2017/09/12 13:27:05 siva Exp $
 *
 * Description: FIPS Test cli command.
 *
 * ***********************************************************************/
#ifndef __FIPSCLI_H__
#define __FIPSCLI_H__ 

#include "lr.h"
#include "cli.h"

#define CLI_FIPS_MAX_ARGS       15

#define CLI_FIPS_MAX_VALID_TRACE       0x0000FFFF

#define CLI_FIPS_KNOWN_ANS_TEST_SHA1   0x00000001
#define CLI_FIPS_KNOWN_ANS_TEST_SHA2   0x00000002
#define CLI_FIPS_KNOWN_ANS_TEST_HMAC   0x00000004
#define CLI_FIPS_KNOWN_ANS_TEST_AES    0x00000008
#define CLI_FIPS_KNOWN_ANS_TEST_DES    0x00000010
#define CLI_FIPS_KNOWN_ANS_TEST_RAND   0x00000020
#define CLI_FIPS_KNOWN_ANS_TEST_RSA    0x00000040
#define CLI_FIPS_KNOWN_ANS_TEST_DSA    0x00000080

/*Switch mode Macros*/
#define CLI_FIPS_MODE                  1
#define CLI_LEGACY_MODE                2
#define CLI_CNSA_MODE                  3

#define CLI_FIPS_KNOWN_ANS_TEST_ALL    0x000000FF

typedef enum
{
    CLI_FIPS_START_KNOWN_ANS_TEST = 1,
    CLI_FIPS_ZEROIZE_CRYPTO_KEYS,
    CLI_FIPS_SET_OPER_MODE,
    CLI_FIPS_SHOW_OPER_MODE,
    CLI_FIPS_DEBUG,
    CLI_FIPS_NO_DEBUG,
    CLI_FIPS_SET_BYPASS_OPER_MODE,
    CLI_FIPS_SHOW_BYPASS_OPER_MODE
}eFipsCmds;


INT4 cli_process_fips_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command, ...));

INT4 FipsCliSetOperMode PROTO ((tCliHandle, INT4));
INT4 FipsCliShowOperMode PROTO ((tCliHandle));

INT4 FipsCliZeroizeCryptoKeys PROTO ((tCliHandle));

INT4 FipsCliRunKnownAnsTests PROTO ((tCliHandle CliHandle, UINT4 u4SetAlgo));

INT4 FipsCliSetDebugLevel PROTO ((tCliHandle CliHandle, INT4 i4Trace));

INT4 FipsCliSetBypassOperMode PROTO ((tCliHandle, INT4));
INT4 FipsCliShowBypassOperMode PROTO ((tCliHandle));

#endif   /* __FIPSCLI_H__ */
