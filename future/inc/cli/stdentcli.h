/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdentcli.h,v 1.2 2009/09/26 10:14:53 prabuc Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 EntPhysicalIndex[12];
extern UINT4 EntPhysicalSerialNum[12];
extern UINT4 EntPhysicalAlias[12];
extern UINT4 EntPhysicalAssetID[12];
extern UINT4 EntPhysicalUris[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetEntPhysicalSerialNum(i4EntPhysicalIndex ,pSetValEntPhysicalSerialNum)	\
	nmhSetCmn(EntPhysicalSerialNum, 12, EntPhysicalSerialNumSet, NULL, NULL, 0, 0, 1, "%i %s", i4EntPhysicalIndex ,pSetValEntPhysicalSerialNum)
#define nmhSetEntPhysicalAlias(i4EntPhysicalIndex ,pSetValEntPhysicalAlias)	\
	nmhSetCmn(EntPhysicalAlias, 12, EntPhysicalAliasSet, NULL, NULL, 0, 0, 1, "%i %s", i4EntPhysicalIndex ,pSetValEntPhysicalAlias)
#define nmhSetEntPhysicalAssetID(i4EntPhysicalIndex ,pSetValEntPhysicalAssetID)	\
	nmhSetCmn(EntPhysicalAssetID, 12, EntPhysicalAssetIDSet, NULL, NULL, 0, 0, 1, "%i %s", i4EntPhysicalIndex ,pSetValEntPhysicalAssetID)
#define nmhSetEntPhysicalUris(i4EntPhysicalIndex ,pSetValEntPhysicalUris)	\
	nmhSetCmn(EntPhysicalUris, 12, EntPhysicalUrisSet, NULL, NULL, 0, 0, 1, "%i %s", i4EntPhysicalIndex ,pSetValEntPhysicalUris)

#endif
