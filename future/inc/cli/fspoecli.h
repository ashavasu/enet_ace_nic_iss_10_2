/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fspoecli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPoeGlobalAdminStatus[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPoeGlobalAdminStatus(i4SetValFsPoeGlobalAdminStatus)	\
	nmhSetCmn(FsPoeGlobalAdminStatus, 10, FsPoeGlobalAdminStatusSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsPoeGlobalAdminStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPoePdMacAddress[12];
extern UINT4 FsPoePdMacRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPoePdMacRowStatus(tFsPoePdMacAddress ,i4SetValFsPoePdMacRowStatus)	\
	nmhSetCmn(FsPoePdMacRowStatus, 12, FsPoePdMacRowStatusSet, NULL, NULL, 0, 1, 1, "%m %i", tFsPoePdMacAddress ,i4SetValFsPoePdMacRowStatus)

#endif
