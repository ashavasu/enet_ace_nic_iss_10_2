/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsissdcli.h,v 1.2 2008/10/31 12:44:38 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDsSystemControl[11];
extern UINT4 FsDsStatus[11];
extern UINT4 FsDsIPTOSQPrecedenceSwitched[11];
extern UINT4 FsDsIPTOSQPrecedenceRouted[11];
extern UINT4 FsDsDot1PQPrecedenceSwitched[11];
extern UINT4 FsDsDot1PQPrecedenceRouted[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDsSystemControl(i4SetValFsDsSystemControl)	\
	nmhSetCmn(FsDsSystemControl, 11, FsDsSystemControlSet, DfsLock, DfsUnLock, 0, 0, 0, "%i", i4SetValFsDsSystemControl)
#define nmhSetFsDsStatus(i4SetValFsDsStatus)	\
	nmhSetCmn(FsDsStatus, 11, FsDsStatusSet, DfsLock, DfsUnLock, 0, 0, 0, "%i", i4SetValFsDsStatus)
#define nmhSetFsDsIPTOSQPrecedenceSwitched(i4SetValFsDsIPTOSQPrecedenceSwitched)	\
	nmhSetCmn(FsDsIPTOSQPrecedenceSwitched, 11, FsDsIPTOSQPrecedenceSwitchedSet, DfsLock, DfsUnLock, 0, 0, 0, "%i", i4SetValFsDsIPTOSQPrecedenceSwitched)
#define nmhSetFsDsIPTOSQPrecedenceRouted(i4SetValFsDsIPTOSQPrecedenceRouted)	\
	nmhSetCmn(FsDsIPTOSQPrecedenceRouted, 11, FsDsIPTOSQPrecedenceRoutedSet, DfsLock, DfsUnLock, 0, 0, 0, "%i", i4SetValFsDsIPTOSQPrecedenceRouted)
#define nmhSetFsDsDot1PQPrecedenceSwitched(i4SetValFsDsDot1PQPrecedenceSwitched)	\
	nmhSetCmn(FsDsDot1PQPrecedenceSwitched, 11, FsDsDot1PQPrecedenceSwitchedSet, DfsLock, DfsUnLock, 0, 0, 0, "%i", i4SetValFsDsDot1PQPrecedenceSwitched)
#define nmhSetFsDsDot1PQPrecedenceRouted(i4SetValFsDsDot1PQPrecedenceRouted)	\
	nmhSetCmn(FsDsDot1PQPrecedenceRouted, 11, FsDsDot1PQPrecedenceRoutedSet, DfsLock, DfsUnLock, 0, 0, 0, "%i", i4SetValFsDsDot1PQPrecedenceRouted)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDiffServMultiFieldClfrId[13];
extern UINT4 FsDiffServMultiFieldClfrFilterId[13];
extern UINT4 FsDiffServMultiFieldClfrFilterType[13];
extern UINT4 FsDiffServMultiFieldClfrStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDiffServMultiFieldClfrFilterId(i4FsDiffServMultiFieldClfrId ,u4SetValFsDiffServMultiFieldClfrFilterId)	\
	nmhSetCmn(FsDiffServMultiFieldClfrFilterId, 13, FsDiffServMultiFieldClfrFilterIdSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %u", i4FsDiffServMultiFieldClfrId ,u4SetValFsDiffServMultiFieldClfrFilterId)
#define nmhSetFsDiffServMultiFieldClfrFilterType(i4FsDiffServMultiFieldClfrId ,i4SetValFsDiffServMultiFieldClfrFilterType)	\
	nmhSetCmn(FsDiffServMultiFieldClfrFilterType, 13, FsDiffServMultiFieldClfrFilterTypeSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %i", i4FsDiffServMultiFieldClfrId ,i4SetValFsDiffServMultiFieldClfrFilterType)
#define nmhSetFsDiffServMultiFieldClfrStatus(i4FsDiffServMultiFieldClfrId ,i4SetValFsDiffServMultiFieldClfrStatus)	\
	nmhSetCmn(FsDiffServMultiFieldClfrStatus, 13, FsDiffServMultiFieldClfrStatusSet, DfsLock, DfsUnLock, 0, 1, 1, "%i %i", i4FsDiffServMultiFieldClfrId ,i4SetValFsDiffServMultiFieldClfrStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDiffServClfrId[13];
extern UINT4 FsDiffServClfrMFClfrId[13];
extern UINT4 FsDiffServClfrInProActionId[13];
extern UINT4 FsDiffServClfrTrafficClassId[13];
extern UINT4 FsDiffServClfrStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDiffServClfrMFClfrId(i4FsDiffServClfrId ,i4SetValFsDiffServClfrMFClfrId)	\
	nmhSetCmn(FsDiffServClfrMFClfrId, 13, FsDiffServClfrMFClfrIdSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %i", i4FsDiffServClfrId ,i4SetValFsDiffServClfrMFClfrId)
#define nmhSetFsDiffServClfrInProActionId(i4FsDiffServClfrId ,i4SetValFsDiffServClfrInProActionId)	\
	nmhSetCmn(FsDiffServClfrInProActionId, 13, FsDiffServClfrInProActionIdSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %i", i4FsDiffServClfrId ,i4SetValFsDiffServClfrInProActionId)
#define nmhSetFsDiffServClfrTrafficClassId(i4FsDiffServClfrId ,i4SetValFsDiffServClfrTrafficClassId)	\
	nmhSetCmn(FsDiffServClfrTrafficClassId, 13, FsDiffServClfrTrafficClassIdSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %i", i4FsDiffServClfrId ,i4SetValFsDiffServClfrTrafficClassId)
#define nmhSetFsDiffServClfrStatus(i4FsDiffServClfrId ,i4SetValFsDiffServClfrStatus)	\
	nmhSetCmn(FsDiffServClfrStatus, 13, FsDiffServClfrStatusSet, DfsLock, DfsUnLock, 0, 1, 1, "%i %i", i4FsDiffServClfrId ,i4SetValFsDiffServClfrStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDiffServInProfileActionId[13];
extern UINT4 FsDiffServInProfileActionFlag[13];
extern UINT4 FsDiffServInProfileActionNewPrio[13];
extern UINT4 FsDiffServInProfileActionIpTOS[13];
extern UINT4 FsDiffServInProfileActionPort[13];
extern UINT4 FsDiffServInProfileActionDscp[13];
extern UINT4 FsDiffServInProfileActionStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDiffServInProfileActionFlag(i4FsDiffServInProfileActionId ,u4SetValFsDiffServInProfileActionFlag)	\
	nmhSetCmn(FsDiffServInProfileActionFlag, 13, FsDiffServInProfileActionFlagSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %u", i4FsDiffServInProfileActionId ,u4SetValFsDiffServInProfileActionFlag)
#define nmhSetFsDiffServInProfileActionNewPrio(i4FsDiffServInProfileActionId ,u4SetValFsDiffServInProfileActionNewPrio)	\
	nmhSetCmn(FsDiffServInProfileActionNewPrio, 13, FsDiffServInProfileActionNewPrioSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %u", i4FsDiffServInProfileActionId ,u4SetValFsDiffServInProfileActionNewPrio)
#define nmhSetFsDiffServInProfileActionIpTOS(i4FsDiffServInProfileActionId ,u4SetValFsDiffServInProfileActionIpTOS)	\
	nmhSetCmn(FsDiffServInProfileActionIpTOS, 13, FsDiffServInProfileActionIpTOSSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %u", i4FsDiffServInProfileActionId ,u4SetValFsDiffServInProfileActionIpTOS)
#define nmhSetFsDiffServInProfileActionPort(i4FsDiffServInProfileActionId ,u4SetValFsDiffServInProfileActionPort)	\
	nmhSetCmn(FsDiffServInProfileActionPort, 13, FsDiffServInProfileActionPortSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %u", i4FsDiffServInProfileActionId ,u4SetValFsDiffServInProfileActionPort)
#define nmhSetFsDiffServInProfileActionDscp(i4FsDiffServInProfileActionId ,u4SetValFsDiffServInProfileActionDscp)	\
	nmhSetCmn(FsDiffServInProfileActionDscp, 13, FsDiffServInProfileActionDscpSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %u", i4FsDiffServInProfileActionId ,u4SetValFsDiffServInProfileActionDscp)
#define nmhSetFsDiffServInProfileActionStatus(i4FsDiffServInProfileActionId ,i4SetValFsDiffServInProfileActionStatus)	\
	nmhSetCmn(FsDiffServInProfileActionStatus, 13, FsDiffServInProfileActionStatusSet, DfsLock, DfsUnLock, 0, 1, 1, "%i %i", i4FsDiffServInProfileActionId ,i4SetValFsDiffServInProfileActionStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDsDSCPValue[13];
extern UINT4 FsDsQPriority[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDsQPriority(u4FsDsDSCPValue ,i4SetValFsDsQPriority)	\
	nmhSetCmn(FsDsQPriority, 13, FsDsQPrioritySet, DfsLock, DfsUnLock, 0, 0, 1, "%u %i", u4FsDsDSCPValue ,i4SetValFsDsQPriority)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDsTrafficClassId[13];
extern UINT4 FsDsTCPortNum[13];
extern UINT4 FsDsTCMaxBandwidth[13];
extern UINT4 FsDsTCGuaranteedBandwidth[13];
extern UINT4 FsDsTCDropLevel1[13];
extern UINT4 FsDsTCDropLevel2[13];
extern UINT4 FsDsTCDropLevel3[13];
extern UINT4 FsDsTCDropAtMaxBandwidth[13];
extern UINT4 FsDsTCWeightFactor[13];
extern UINT4 FsDsTCWeightShift[13];
extern UINT4 FsDsTCNumFlowGroups[13];
extern UINT4 FsDsTCREDCurveId[13];
extern UINT4 FsDsTCEntryStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDsTCPortNum(i4FsDsTrafficClassId ,i4SetValFsDsTCPortNum)	\
	nmhSetCmn(FsDsTCPortNum, 13, FsDsTCPortNumSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %i", i4FsDsTrafficClassId ,i4SetValFsDsTCPortNum)
#define nmhSetFsDsTCMaxBandwidth(i4FsDsTrafficClassId ,i4SetValFsDsTCMaxBandwidth)	\
	nmhSetCmn(FsDsTCMaxBandwidth, 13, FsDsTCMaxBandwidthSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %i", i4FsDsTrafficClassId ,i4SetValFsDsTCMaxBandwidth)
#define nmhSetFsDsTCGuaranteedBandwidth(i4FsDsTrafficClassId ,i4SetValFsDsTCGuaranteedBandwidth)	\
	nmhSetCmn(FsDsTCGuaranteedBandwidth, 13, FsDsTCGuaranteedBandwidthSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %i", i4FsDsTrafficClassId ,i4SetValFsDsTCGuaranteedBandwidth)
#define nmhSetFsDsTCDropLevel1(i4FsDsTrafficClassId ,i4SetValFsDsTCDropLevel1)	\
	nmhSetCmn(FsDsTCDropLevel1, 13, FsDsTCDropLevel1Set, DfsLock, DfsUnLock, 0, 0, 1, "%i %i", i4FsDsTrafficClassId ,i4SetValFsDsTCDropLevel1)
#define nmhSetFsDsTCDropLevel2(i4FsDsTrafficClassId ,i4SetValFsDsTCDropLevel2)	\
	nmhSetCmn(FsDsTCDropLevel2, 13, FsDsTCDropLevel2Set, DfsLock, DfsUnLock, 0, 0, 1, "%i %i", i4FsDsTrafficClassId ,i4SetValFsDsTCDropLevel2)
#define nmhSetFsDsTCDropLevel3(i4FsDsTrafficClassId ,i4SetValFsDsTCDropLevel3)	\
	nmhSetCmn(FsDsTCDropLevel3, 13, FsDsTCDropLevel3Set, DfsLock, DfsUnLock, 0, 0, 1, "%i %i", i4FsDsTrafficClassId ,i4SetValFsDsTCDropLevel3)
#define nmhSetFsDsTCDropAtMaxBandwidth(i4FsDsTrafficClassId ,i4SetValFsDsTCDropAtMaxBandwidth)	\
	nmhSetCmn(FsDsTCDropAtMaxBandwidth, 13, FsDsTCDropAtMaxBandwidthSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %i", i4FsDsTrafficClassId ,i4SetValFsDsTCDropAtMaxBandwidth)
#define nmhSetFsDsTCWeightFactor(i4FsDsTrafficClassId ,i4SetValFsDsTCWeightFactor)	\
	nmhSetCmn(FsDsTCWeightFactor, 13, FsDsTCWeightFactorSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %i", i4FsDsTrafficClassId ,i4SetValFsDsTCWeightFactor)
#define nmhSetFsDsTCWeightShift(i4FsDsTrafficClassId ,i4SetValFsDsTCWeightShift)	\
	nmhSetCmn(FsDsTCWeightShift, 13, FsDsTCWeightShiftSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %i", i4FsDsTrafficClassId ,i4SetValFsDsTCWeightShift)
#define nmhSetFsDsTCNumFlowGroups(i4FsDsTrafficClassId ,i4SetValFsDsTCNumFlowGroups)	\
	nmhSetCmn(FsDsTCNumFlowGroups, 13, FsDsTCNumFlowGroupsSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %i", i4FsDsTrafficClassId ,i4SetValFsDsTCNumFlowGroups)
#define nmhSetFsDsTCREDCurveId(i4FsDsTrafficClassId ,i4SetValFsDsTCREDCurveId)	\
	nmhSetCmn(FsDsTCREDCurveId, 13, FsDsTCREDCurveIdSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %i", i4FsDsTrafficClassId ,i4SetValFsDsTCREDCurveId)
#define nmhSetFsDsTCEntryStatus(i4FsDsTrafficClassId ,i4SetValFsDsTCEntryStatus)	\
	nmhSetCmn(FsDsTCEntryStatus, 13, FsDsTCEntryStatusSet, DfsLock, DfsUnLock, 0, 1, 1, "%i %i", i4FsDsTrafficClassId ,i4SetValFsDsTCEntryStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDsPortNum[13];
extern UINT4 FsDsPortMaxBandwidth[13];
extern UINT4 FsDsPortDropLevel1[13];
extern UINT4 FsDsPortDropLevel2[13];
extern UINT4 FsDsPortDropLevel3[13];
extern UINT4 FsDsPortDropAtMaxBandwidth[13];
extern UINT4 FsDsPortREDCurveId[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDsPortMaxBandwidth(i4FsDsPortNum ,i4SetValFsDsPortMaxBandwidth)	\
	nmhSetCmn(FsDsPortMaxBandwidth, 13, FsDsPortMaxBandwidthSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %i", i4FsDsPortNum ,i4SetValFsDsPortMaxBandwidth)
#define nmhSetFsDsPortDropLevel1(i4FsDsPortNum ,i4SetValFsDsPortDropLevel1)	\
	nmhSetCmn(FsDsPortDropLevel1, 13, FsDsPortDropLevel1Set, DfsLock, DfsUnLock, 0, 0, 1, "%i %i", i4FsDsPortNum ,i4SetValFsDsPortDropLevel1)
#define nmhSetFsDsPortDropLevel2(i4FsDsPortNum ,i4SetValFsDsPortDropLevel2)	\
	nmhSetCmn(FsDsPortDropLevel2, 13, FsDsPortDropLevel2Set, DfsLock, DfsUnLock, 0, 0, 1, "%i %i", i4FsDsPortNum ,i4SetValFsDsPortDropLevel2)
#define nmhSetFsDsPortDropLevel3(i4FsDsPortNum ,i4SetValFsDsPortDropLevel3)	\
	nmhSetCmn(FsDsPortDropLevel3, 13, FsDsPortDropLevel3Set, DfsLock, DfsUnLock, 0, 0, 1, "%i %i", i4FsDsPortNum ,i4SetValFsDsPortDropLevel3)
#define nmhSetFsDsPortDropAtMaxBandwidth(i4FsDsPortNum ,i4SetValFsDsPortDropAtMaxBandwidth)	\
	nmhSetCmn(FsDsPortDropAtMaxBandwidth, 13, FsDsPortDropAtMaxBandwidthSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %i", i4FsDsPortNum ,i4SetValFsDsPortDropAtMaxBandwidth)
#define nmhSetFsDsPortREDCurveId(i4FsDsPortNum ,i4SetValFsDsPortREDCurveId)	\
	nmhSetCmn(FsDsPortREDCurveId, 13, FsDsPortREDCurveIdSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %i", i4FsDsPortNum ,i4SetValFsDsPortREDCurveId)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDsREDCurveId[13];
extern UINT4 FsDsREDCurveStart[13];
extern UINT4 FsDsREDCurveStop[13];
extern UINT4 FsDsREDCurveQRange[13];
extern UINT4 FsDsREDCurveStopProbability[13];
extern UINT4 FsDsREDCurveStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDsREDCurveStart(i4FsDsREDCurveId ,i4SetValFsDsREDCurveStart)	\
	nmhSetCmn(FsDsREDCurveStart, 13, FsDsREDCurveStartSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %i", i4FsDsREDCurveId ,i4SetValFsDsREDCurveStart)
#define nmhSetFsDsREDCurveStop(i4FsDsREDCurveId ,i4SetValFsDsREDCurveStop)	\
	nmhSetCmn(FsDsREDCurveStop, 13, FsDsREDCurveStopSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %i", i4FsDsREDCurveId ,i4SetValFsDsREDCurveStop)
#define nmhSetFsDsREDCurveQRange(i4FsDsREDCurveId ,i4SetValFsDsREDCurveQRange)	\
	nmhSetCmn(FsDsREDCurveQRange, 13, FsDsREDCurveQRangeSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %i", i4FsDsREDCurveId ,i4SetValFsDsREDCurveQRange)
#define nmhSetFsDsREDCurveStopProbability(i4FsDsREDCurveId ,i4SetValFsDsREDCurveStopProbability)	\
	nmhSetCmn(FsDsREDCurveStopProbability, 13, FsDsREDCurveStopProbabilitySet, DfsLock, DfsUnLock, 0, 0, 1, "%i %i", i4FsDsREDCurveId ,i4SetValFsDsREDCurveStopProbability)
#define nmhSetFsDsREDCurveStatus(i4FsDsREDCurveId ,i4SetValFsDsREDCurveStatus)	\
	nmhSetCmn(FsDsREDCurveStatus, 13, FsDsREDCurveStatusSet, DfsLock, DfsUnLock, 0, 1, 1, "%i %i", i4FsDsREDCurveId ,i4SetValFsDsREDCurveStatus)

#endif
