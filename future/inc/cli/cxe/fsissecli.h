/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsissecli.h,v 1.2 2008/10/31 12:44:38 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IssRateCtrlIndex[12];
extern UINT4 IssRateCtrlDLFLimitValue[12];
extern UINT4 IssRateCtrlBCASTLimitValue[12];
extern UINT4 IssRateCtrlMCASTLimitValue[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIssRateCtrlDLFLimitValue(i4IssRateCtrlIndex ,i4SetValIssRateCtrlDLFLimitValue)	\
	nmhSetCmn(IssRateCtrlDLFLimitValue, 12, IssRateCtrlDLFLimitValueSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssRateCtrlIndex ,i4SetValIssRateCtrlDLFLimitValue)
#define nmhSetIssRateCtrlBCASTLimitValue(i4IssRateCtrlIndex ,i4SetValIssRateCtrlBCASTLimitValue)	\
	nmhSetCmn(IssRateCtrlBCASTLimitValue, 12, IssRateCtrlBCASTLimitValueSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssRateCtrlIndex ,i4SetValIssRateCtrlBCASTLimitValue)
#define nmhSetIssRateCtrlMCASTLimitValue(i4IssRateCtrlIndex ,i4SetValIssRateCtrlMCASTLimitValue)	\
	nmhSetCmn(IssRateCtrlMCASTLimitValue, 12, IssRateCtrlMCASTLimitValueSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssRateCtrlIndex ,i4SetValIssRateCtrlMCASTLimitValue)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IssL2FilterNo[12];
extern UINT4 IssL2FilterEtherType[12];
extern UINT4 IssL2FilterProtocolType[12];
extern UINT4 IssL2FilterDstMacAddr[12];
extern UINT4 IssL2FilterSrcMacAddr[12];
extern UINT4 IssL2FilterVlanId[12];
extern UINT4 IssL2FilterInPort[12];
extern UINT4 IssL2FilterOutPort[12];
extern UINT4 IssL2FilterAction[12];
extern UINT4 IssL2FilterStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIssL2FilterEtherType(i4IssL2FilterNo ,i4SetValIssL2FilterEtherType)	\
	nmhSetCmn(IssL2FilterEtherType, 12, IssL2FilterEtherTypeSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssL2FilterNo ,i4SetValIssL2FilterEtherType)
#define nmhSetIssL2FilterProtocolType(i4IssL2FilterNo ,u4SetValIssL2FilterProtocolType)	\
	nmhSetCmn(IssL2FilterProtocolType, 12, IssL2FilterProtocolTypeSet, IssLock, IssUnLock, 0, 0, 1, "%i %u", i4IssL2FilterNo ,u4SetValIssL2FilterProtocolType)
#define nmhSetIssL2FilterDstMacAddr(i4IssL2FilterNo ,SetValIssL2FilterDstMacAddr)	\
	nmhSetCmn(IssL2FilterDstMacAddr, 12, IssL2FilterDstMacAddrSet, IssLock, IssUnLock, 0, 0, 1, "%i %m", i4IssL2FilterNo ,SetValIssL2FilterDstMacAddr)
#define nmhSetIssL2FilterSrcMacAddr(i4IssL2FilterNo ,SetValIssL2FilterSrcMacAddr)	\
	nmhSetCmn(IssL2FilterSrcMacAddr, 12, IssL2FilterSrcMacAddrSet, IssLock, IssUnLock, 0, 0, 1, "%i %m", i4IssL2FilterNo ,SetValIssL2FilterSrcMacAddr)
#define nmhSetIssL2FilterVlanId(i4IssL2FilterNo ,i4SetValIssL2FilterVlanId)	\
	nmhSetCmn(IssL2FilterVlanId, 12, IssL2FilterVlanIdSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssL2FilterNo ,i4SetValIssL2FilterVlanId)
#define nmhSetIssL2FilterInPort(i4IssL2FilterNo ,i4SetValIssL2FilterInPort)	\
	nmhSetCmn(IssL2FilterInPort, 12, IssL2FilterInPortSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssL2FilterNo ,i4SetValIssL2FilterInPort)
#define nmhSetIssL2FilterOutPort(i4IssL2FilterNo ,i4SetValIssL2FilterOutPort)	\
	nmhSetCmn(IssL2FilterOutPort, 12, IssL2FilterOutPortSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssL2FilterNo ,i4SetValIssL2FilterOutPort)
#define nmhSetIssL2FilterAction(i4IssL2FilterNo ,i4SetValIssL2FilterAction)	\
	nmhSetCmn(IssL2FilterAction, 12, IssL2FilterActionSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssL2FilterNo ,i4SetValIssL2FilterAction)
#define nmhSetIssL2FilterStatus(i4IssL2FilterNo ,i4SetValIssL2FilterStatus)	\
	nmhSetCmn(IssL2FilterStatus, 12, IssL2FilterStatusSet, IssLock, IssUnLock, 0, 1, 1, "%i %i", i4IssL2FilterNo ,i4SetValIssL2FilterStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IssL3FilterNo[12];
extern UINT4 IssL3FilterProtocol[12];
extern UINT4 IssL3FilterMessageType[12];
extern UINT4 IssL3FilterMessageCode[12];
extern UINT4 IssL3FilterDstIpAddr[12];
extern UINT4 IssL3FilterSrcIpAddr[12];
extern UINT4 IssL3FilterDstIpAddrMask[12];
extern UINT4 IssL3FilterSrcIpAddrMask[12];
extern UINT4 IssL3FilterMinDstProtPort[12];
extern UINT4 IssL3FilterMaxDstProtPort[12];
extern UINT4 IssL3FilterMinSrcProtPort[12];
extern UINT4 IssL3FilterMaxSrcProtPort[12];
extern UINT4 IssL3FilterVlan[12];
extern UINT4 IssL3FilterAckBit[12];
extern UINT4 IssL3FilterRstBit[12];
extern UINT4 IssL3FilterTos[12];
extern UINT4 IssL3FilterDscp[12];
extern UINT4 IssL3FilterAction[12];
extern UINT4 IssL3FilterStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIssL3FilterProtocol(i4IssL3FilterNo ,i4SetValIssL3FilterProtocol)	\
	nmhSetCmn(IssL3FilterProtocol, 12, IssL3FilterProtocolSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssL3FilterNo ,i4SetValIssL3FilterProtocol)
#define nmhSetIssL3FilterMessageType(i4IssL3FilterNo ,i4SetValIssL3FilterMessageType)	\
	nmhSetCmn(IssL3FilterMessageType, 12, IssL3FilterMessageTypeSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssL3FilterNo ,i4SetValIssL3FilterMessageType)
#define nmhSetIssL3FilterMessageCode(i4IssL3FilterNo ,i4SetValIssL3FilterMessageCode)	\
	nmhSetCmn(IssL3FilterMessageCode, 12, IssL3FilterMessageCodeSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssL3FilterNo ,i4SetValIssL3FilterMessageCode)
#define nmhSetIssL3FilterDstIpAddr(i4IssL3FilterNo ,u4SetValIssL3FilterDstIpAddr)	\
	nmhSetCmn(IssL3FilterDstIpAddr, 12, IssL3FilterDstIpAddrSet, IssLock, IssUnLock, 0, 0, 1, "%i %p", i4IssL3FilterNo ,u4SetValIssL3FilterDstIpAddr)
#define nmhSetIssL3FilterSrcIpAddr(i4IssL3FilterNo ,u4SetValIssL3FilterSrcIpAddr)	\
	nmhSetCmn(IssL3FilterSrcIpAddr, 12, IssL3FilterSrcIpAddrSet, IssLock, IssUnLock, 0, 0, 1, "%i %p", i4IssL3FilterNo ,u4SetValIssL3FilterSrcIpAddr)
#define nmhSetIssL3FilterDstIpAddrMask(i4IssL3FilterNo ,u4SetValIssL3FilterDstIpAddrMask)	\
	nmhSetCmn(IssL3FilterDstIpAddrMask, 12, IssL3FilterDstIpAddrMaskSet, IssLock, IssUnLock, 0, 0, 1, "%i %p", i4IssL3FilterNo ,u4SetValIssL3FilterDstIpAddrMask)
#define nmhSetIssL3FilterSrcIpAddrMask(i4IssL3FilterNo ,u4SetValIssL3FilterSrcIpAddrMask)	\
	nmhSetCmn(IssL3FilterSrcIpAddrMask, 12, IssL3FilterSrcIpAddrMaskSet, IssLock, IssUnLock, 0, 0, 1, "%i %p", i4IssL3FilterNo ,u4SetValIssL3FilterSrcIpAddrMask)
#define nmhSetIssL3FilterMinDstProtPort(i4IssL3FilterNo ,u4SetValIssL3FilterMinDstProtPort)	\
	nmhSetCmn(IssL3FilterMinDstProtPort, 12, IssL3FilterMinDstProtPortSet, IssLock, IssUnLock, 0, 0, 1, "%i %u", i4IssL3FilterNo ,u4SetValIssL3FilterMinDstProtPort)
#define nmhSetIssL3FilterMaxDstProtPort(i4IssL3FilterNo ,u4SetValIssL3FilterMaxDstProtPort)	\
	nmhSetCmn(IssL3FilterMaxDstProtPort, 12, IssL3FilterMaxDstProtPortSet, IssLock, IssUnLock, 0, 0, 1, "%i %u", i4IssL3FilterNo ,u4SetValIssL3FilterMaxDstProtPort)
#define nmhSetIssL3FilterMinSrcProtPort(i4IssL3FilterNo ,u4SetValIssL3FilterMinSrcProtPort)	\
	nmhSetCmn(IssL3FilterMinSrcProtPort, 12, IssL3FilterMinSrcProtPortSet, IssLock, IssUnLock, 0, 0, 1, "%i %u", i4IssL3FilterNo ,u4SetValIssL3FilterMinSrcProtPort)
#define nmhSetIssL3FilterMaxSrcProtPort(i4IssL3FilterNo ,u4SetValIssL3FilterMaxSrcProtPort)	\
	nmhSetCmn(IssL3FilterMaxSrcProtPort, 12, IssL3FilterMaxSrcProtPortSet, IssLock, IssUnLock, 0, 0, 1, "%i %u", i4IssL3FilterNo ,u4SetValIssL3FilterMaxSrcProtPort)
#define nmhSetIssL3FilterVlan(i4IssL3FilterNo ,i4SetValIssL3FilterVlan)	\
	nmhSetCmn(IssL3FilterVlan, 12, IssL3FilterVlanSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssL3FilterNo ,i4SetValIssL3FilterVlan)
#define nmhSetIssL3FilterAckBit(i4IssL3FilterNo ,i4SetValIssL3FilterAckBit)	\
	nmhSetCmn(IssL3FilterAckBit, 12, IssL3FilterAckBitSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssL3FilterNo ,i4SetValIssL3FilterAckBit)
#define nmhSetIssL3FilterRstBit(i4IssL3FilterNo ,i4SetValIssL3FilterRstBit)	\
	nmhSetCmn(IssL3FilterRstBit, 12, IssL3FilterRstBitSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssL3FilterNo ,i4SetValIssL3FilterRstBit)
#define nmhSetIssL3FilterTos(i4IssL3FilterNo ,i4SetValIssL3FilterTos)	\
	nmhSetCmn(IssL3FilterTos, 12, IssL3FilterTosSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssL3FilterNo ,i4SetValIssL3FilterTos)
#define nmhSetIssL3FilterDscp(i4IssL3FilterNo ,i4SetValIssL3FilterDscp)	\
	nmhSetCmn(IssL3FilterDscp, 12, IssL3FilterDscpSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssL3FilterNo ,i4SetValIssL3FilterDscp)
#define nmhSetIssL3FilterAction(i4IssL3FilterNo ,i4SetValIssL3FilterAction)	\
	nmhSetCmn(IssL3FilterAction, 12, IssL3FilterActionSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssL3FilterNo ,i4SetValIssL3FilterAction)
#define nmhSetIssL3FilterStatus(i4IssL3FilterNo ,i4SetValIssL3FilterStatus)	\
	nmhSetCmn(IssL3FilterStatus, 12, IssL3FilterStatusSet, IssLock, IssUnLock, 0, 1, 1, "%i %i", i4IssL3FilterNo ,i4SetValIssL3FilterStatus)

#endif
