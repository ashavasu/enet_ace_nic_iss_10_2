/***************************************************************************
* Copyright (C) Aricent communication Software Limited,2007
*
* Description: Contains all the constants and macro definitions for CLI
*
* $Id: rmapcli.h,v 1.17 2016/07/06 11:02:46 siva Exp $
***************************************************************************/
#ifndef  _RMAPCLI_H__
#define  _RMAPCLI_H__

#include "rmap.h"


#define RMAP_CLI_MAX_ARGS 10

/* Enums used in routemap creation,deletion and showing of 
   routemap */
typedef enum {
     RMAP_CLI_DEBUG=1, 
     RMAP_CLI_CREATE_RMAP,
     RMAP_CLI_NO_RMAP,
     RMAP_CLI_IP_PREFIX_LIST,
     RMAP_CLI_SHOW_RMAP,
     RMAP_CLI_SHOW_IP_PREFIX_LIST,

    RMAP_CLI_COMMANDS_WITH_PROMT_START,/* all match/set commands should be below */

    RMAP_CLI_MATCH_DST_IP4,
    RMAP_CLI_MATCH_DST_IP6,
    RMAP_CLI_MATCH_SRC_IP4,
    RMAP_CLI_MATCH_SRC_IP6,
    RMAP_CLI_MATCH_NEXTHOP_IP4,
    RMAP_CLI_MATCH_NEXTHOP_IP6,

     RMAP_CLI_MATCH_INTERFACE,
     RMAP_CLI_MATCH_METRIC,
     RMAP_CLI_MATCH_TAG,
    RMAP_CLI_MATCH_METRIC_TYPE,
     RMAP_CLI_MATCH_ROUTE_TYPE,

     RMAP_CLI_MATCH_ASPATH_TAG, 
     RMAP_CLI_MATCH_COMMUNITY,
    RMAP_CLI_MATCH_LOCAL_PREF,
     RMAP_CLI_MATCH_ORIGIN, 
    /***************************/

    RMAP_CLI_NO_MATCH_DST_IP4,
    RMAP_CLI_NO_MATCH_DST_IP6,
    RMAP_CLI_NO_MATCH_SRC_IP4,
    RMAP_CLI_NO_MATCH_SRC_IP6,
    RMAP_CLI_NO_MATCH_NEXTHOP_IP4,
    RMAP_CLI_NO_MATCH_NEXTHOP_IP6,

     RMAP_CLI_NO_MATCH_INTERFACE,
     RMAP_CLI_NO_MATCH_METRIC,
     RMAP_CLI_NO_MATCH_TAG,
    RMAP_CLI_NO_MATCH_METRIC_TYPE,
     RMAP_CLI_NO_MATCH_ROUTE_TYPE,

     RMAP_CLI_NO_MATCH_ASPATH_TAG,
     RMAP_CLI_NO_MATCH_COMMUNITY,
    RMAP_CLI_NO_MATCH_LOCAL_PREF,
     RMAP_CLI_NO_MATCH_ORIGIN,
    /***************************/

    RMAP_CLI_SET_NEXTHOP_IP4,
    RMAP_CLI_SET_NEXTHOP_IP6,

     RMAP_CLI_SET_INTERFACE,
     RMAP_CLI_SET_METRIC,
     RMAP_CLI_SET_TAG,
/*  RMAP_CLI_SET_METRIC_TYPE, --not supported*/
     RMAP_CLI_SET_ROUTE_TYPE,

     RMAP_CLI_SET_ASPATH_TAG,
     RMAP_CLI_SET_COMMUNITY,
    RMAP_CLI_SET_LOCAL_PREF,
     RMAP_CLI_SET_ORIGIN,

    RMAP_CLI_SET_WEIGHT, 
    RMAP_CLI_SET_AUTO_TAG_ENA, 
    RMAP_CLI_SET_LEVEL, 
    RMAP_CLI_SET_EXTCOMM_COST,
    /***************************/

    RMAP_CLI_NO_SET_NEXTHOP_IP4,
    RMAP_CLI_NO_SET_NEXTHOP_IP6,

     RMAP_CLI_NO_SET_INTERFACE,
     RMAP_CLI_NO_SET_METRIC,
     RMAP_CLI_NO_SET_TAG,
/*  RMAP_CLI_NO_SET_METRIC_TYPE, --not supported*/
     RMAP_CLI_NO_SET_ROUTE_TYPE,

     RMAP_CLI_NO_SET_ASPATH_TAG,
     RMAP_CLI_NO_SET_COMMUNITY,
    RMAP_CLI_NO_SET_LOCAL_PREF,
     RMAP_CLI_NO_SET_ORIGIN,

    RMAP_CLI_NO_SET_WEIGHT, 
    RMAP_CLI_NO_SET_AUTO_TAG_ENA, 
    RMAP_CLI_NO_SET_LEVEL,
    RMAP_CLI_NO_SET_EXTCOMM_COST,

    RMAP_CLI_COMMANDS_WITH_PROMT_END,/* all match/set commands should be above */
}tRMapCliCmds;


enum rmap_show_cmds {
    RMAP_SHOW_ALL_MAPS = 1,
    RMAP_SHOW_ONE_MAP_BY_NAME
};

#define RMAP_CLI_CFG_MODE    route-map


#define RMAP_CLI_SHOW_METRIC_TYPE(i4MetricType) \
    (i4MetricType == RMAP_METRIC_TYPE_INTER_AREA ? "inter-area" : \
    (i4MetricType == RMAP_METRIC_TYPE_INTRA_AREA ? "intra-area" : \
    (i4MetricType == RMAP_METRIC_TYPE_TYPE1EXTERNAL ? "type-1-external" : \
    (i4MetricType == RMAP_METRIC_TYPE_TYPE2EXTERNAL ? "type-2-external" : ""))))
 

#define RMAP_CLI_SHOW_ROUTE_TYPE(i4RouteType) \
    (i4RouteType == RMAP_ROUTE_TYPE_LOCAL ? "local" : \
    (i4RouteType == RMAP_ROUTE_TYPE_REMOTE ? "remote" : ""))


#define RMAP_CLI_SHOW_COMMUNITY(u4CommType) \
    (u4CommType == RMAP_COMM_INTERNET   ? "internet" : \
    (u4CommType == RMAP_COMM_LOCALAS    ? "local-as" : \
    (u4CommType == RMAP_COMM_NO_ADVT    ? "no-advt" : \
    (u4CommType == RMAP_COMM_NO_EXPORT  ? "no-export" : \
    (u4CommType == RMAP_COMM_NONE       ? "none": "")))))
                                
                                 
#define RMAP_CLI_SHOW_ORIGIN(i4OriginType) \
    (i4OriginType == RMAP_ORIGIN_IGP ? "igp" : \
    (i4OriginType == RMAP_ORIGIN_EGP ? "egp" : \
    (i4OriginType == RMAP_ORIGIN_INCOMPLETE ? "incomplete" : "")))


#define RMAP_CLI_SHOW_AUTO_TAG(autotag) \
    (autotag == RMAP_AUTO_TAG_ENA ? "enable" : \
    (autotag == RMAP_AUTO_TAG_DIS ? "disable" : ""))


#define RMAP_CLI_SHOW_LEVEL(level) \
    (level == RMAP_LEVEL_1          ? "level-1" : \
    (level == RMAP_LEVEL_2          ? "level-2" : \
    (level == RMAP_LEVEL_1_2        ? "level-1-2" : \
    (level == RMAP_LEVEL_STUB_AREA  ? "level-stub-area" : \
    (level == RMAP_LEVEL_BACKBONE   ? "level_backbone": "")))))

    


                             
                         
INT4 cli_process_routemap_cmd (tCliHandle, UINT4, ...);

INT1 RMapCliGetRMapPrompt (INT1 *, INT1 *);

INT4 RMapShowRunningConfig (tCliHandle CliHandle);
 
INT4 RmapCliValidateIpAddrAndPrefix (UINT1 *pu1AddrAndMask, UINT1 u1AddrType,
                                     tIpAddr *pAddr, UINT4 *pu4PrefixLen);
/* Error codes.                                         *
 * As error is displayed immediately after CLI command, *
 * it refers to last CLI command                        */
enum
{
    RMAP_CLI_ERR_NONE = 0,

    RMAP_CLI_ERR_MAP_CREATION,
    RMAP_CLI_ERR_MATCH_CREATION,
    RMAP_CLI_ERR_SET_CREATION,

    RMAP_CLI_ERR_MAP_DELETION,
    RMAP_CLI_ERR_MATCH_DELETION,
    RMAP_CLI_ERR_SET_DELETION,

    RMAP_CLI_ERR_INVALID_IP6_ADDR,
    RMAP_CLI_ERR_INVALID_IP6_PREFIXLEN,
    RMAP_CLI_ERR_INVALID_IP_ADDRESS,
    RMAP_CLI_IP_PREFIX_ENTRY_EXIST,
    RMAP_CLI_IP_PREFIX_ENTRY_NOT_EXIST,
    RMAP_CLI_IP_PREFIX_WITH_SAME_SEQ_NUM,
    RMAP_CLI_MAX_GEN_SEQ_NUM_EXCEED,
    RMAP_CLI_MAX_SEQ_NUM_EXCEED,
    RMAP_CLI_MAX_IP_PREFIX_LIST_RCHD,
    RMAP_CLI_MIN_PREFIX_LEN_INVALID,
    RMAP_CLI_MAX_PREFIX_LEN_INVALID,
    RMAP_CLI_MAX_LEN_LESS_THAN_MIN_LEN,
    RMAP_CLI_IP_PREFIX_EXIST_WITH_GIVEN_NAME,
    RMAP_CLI_RMAP_EXIST_WITH_GIVEN_NAME,
    RMAP_CLI_MAX_PREFIX_NAME_LEN,
    RMAP_CLI_ERR_INVALID_INTERFACE_TYPE,
    RMAP_CLI_ERR_INVALID_INTERFACE_ID,
    RMAP_CLI_ERR_SPACE_NOT_ALLOWED,
    RMAP_CLI_ENTRY_NOT_EXIST,
    RMAP_CLI_ENTRY_COMM_EXIST,
    RMAP_CLI_COMM_RANGE_ERROR,
    RMAP_CLI_COMM_RANGE_NONE_ERROR,
    RMAP_CLI_COMM_NONE_CONFIGURED,
    RMAP_CLI_COMM_NONE_CANNOT_BE_CONFIGURED,
    RMAP_CLI_ENTRY_NAME_EXCEEDS_MAX_VALUE,
    RMAP_CLI_MAX_COMMUNITIES_REACHED,
    RMAP_CLI_ERR_MAX
};

#ifdef __RMAPCLI_C__
CONST CHR1 *RMapCliErrString[] = {
    NULL,
    " Route Map node creation failed",
    " Route Map MATCH node creation failed - entry already exists",
    " Route Map SET node creation failed",

    " Route Map node deletion failed",
    " Route Map MATCH node deletion failed - entry doesn't exist",
    " Route Map SET node deletion failed",

    " Route Map error - invalid IPv6 address",
    " Route Map error - invalid IPv6 prefix",
    " Route Map error - invalid IP address",
    " Ip Prefix entry with the given set of rules already exists",
    " Ip Prefix entry not exists",
    " Ip Prefix entry with the given Sequence number already exists",
    " Generated Sequence number exceeds the allowed limit",
    " Configured Sequence number exceeds the allowed limit",
    " IP Prefix list creation failed",
    " Invalid Min Prefix length for the IP Prefix",
    " Invalid Max Prefix length for the IP Prefix",
    " Max Prefix less than Min prefix length",
    " Ip prefix list with the given name exists. So cannot create Route map in that name",
    " Route map with the given name exists. So cannot create Ip Prefix in that name",
    " IP Prefix name exceeds max length",
    " Route map interface must be a L3 interface or router port",
    " Route map invalid interface id",
    " Spaces not allowed in routemap name",
    " Specified entry does not exist",
    " Community already configured",
    " Community number is not in valid range.<A:B> format <1-65535>:<1-65535>,Number Format <65536-4294967295>",
    " Cannot configure community number 65535:65535.Use standard community none",
    " Communities cannot be configured.Remove community NONE to set community",
    " Route-map has some community values.Community NONE cannot be configured for this route-map",
    " Route-map Name exceeds maximum allowed limit",
    " Maximum Community Reached",
    NULL
};
#endif


#endif   /* _RMAPCLI_H__  */
