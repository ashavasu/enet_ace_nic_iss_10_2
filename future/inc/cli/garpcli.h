/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: garpcli.h,v 1.24 2016/03/09 11:37:56 siva Exp $
 *
 * Description: This file contains garp cli related values
 *
 *******************************************************************/

#ifndef __GARPCLI_H__
#define __GARPCLI_H__

#include "cli.h"



enum {
 CLI_GARP_SHUT_GARP = 1,
 CLI_GARP_NO_SHUT_GARP,
 CLI_GARP_GVRP,
 CLI_GARP_PORT_GVRP,
 CLI_GARP_GMRP,
 CLI_GARP_PORT_GMRP,
 CLI_GARP_JOIN_TIMER,
 CLI_GARP_LEAVE_TIMER,
 CLI_GARP_LEAVE_ALL_TIMER,
 CLI_GARP_RESTRICTED_VLAN,
 CLI_GARP_RESTRICTED_GROUP,
 CLI_GARP_SHOW_GARP_TIMER,
 CLI_GARP_SHOW_GMRP_STATS,
 CLI_GARP_SHOW_GVRP_STATS,
 CLI_GARP_DEBUGS,
 CLI_GARP_NO_DEBUGS,
 CLI_GARP_GBL_DEBUG,
 CLI_GARP_NO_GBL_DEBUG,
 CLI_GARP_CLEAR_STATISTICS
};

enum {
    CLI_GARP_UNKNOWN_ERR = 1,
    CLI_GARP_VLAN_DISABLED_ERR,
    CLI_GARP_DISABLED_ERR,
    CLI_GARP_INVALID_PORT_STATUS_ERR,
    CLI_GMRP_TUNNEL_ENABLED_ERR,
    CLI_GMRP_PROTO_ENA_ERR,
    CLI_GVRP_TUNNEL_ENABLED_ERR,
    CLI_GARP_IGS_ENABLED_ERR,
    CLI_GARP_MLDS_ENABLED_ERR,
    CLI_GARP_TIMER_VAL_ERR, 
    CLI_GARP_ACCESS_PORT_ERR,
    CLI_GARP_SHUT_APP_ENABLED_ERR,
    CLI_GARP_NOSHUT_VLAN_ENABLED_ERR,
    CLI_GARP_NOSHUT_MCLAG_ENABLED_ERR,
    CLI_GARP_PB_GMRP_ENABLED_ERR,
    CLI_GARP_PB_GVRP_PBPORT_ERR,
    CLI_GARP_PVRST_STARTED_ERR,
    CLI_GARP_PBB_MODE_GVRP_STARTED_ERR,
    CLI_GARP_PBB_MODE_GMRP_STARTED_ERR,
    CLI_GARP_PBB_MODE_STARTED_ERR,
    CLI_GARP_BASE_BRIDGE_GARP_ENABLED,
    CLI_GVRP_SISP_PORT_ERR,
    CLI_GARP_NOSHUT_MRP_START_ERR,
    CLI_GMRP_DISABLED_ON_ICCL_ERR,
    CLI_GVRP_DISABLED_ON_ICCL_ERR,
    CLI_GARP_MAX_ERR
};

#ifdef __GARPCLI_C__

CONST CHR1  *GarpCliErrString [] = {
    NULL,
    "Unknown Error\r\n",
    "Vlan Module is disabled\r\n",
    "GARP is disabled \r\n",
    "Port not created\r\n",
    "GMRP tunnel/discard is configured on this port\r\n",
    "In Provider bridge mode, GMRP cannot be enabled on tunnel port\r\n",
    "GVRP tunneling is enabled on this port\r\n",
    "GMRP cannot be enabled when IGMP Snooping is enabled\r\n",
    "GMRP cannot be enabled when MLD Snooping is enabled\r\n",
    "Leave timer should be greater than 2 times Join timer and Leave-all should be greater than Leave timer and timer value cannot be zero\r\n",
    "Gvrp cannot be enabled on access ports.\r\n",
    "Garp module cannot be shut down when other garp applications are running\r\n",
    "Garp module can be started only when VLAN module is started\r\n",
    "Garp module cannot be started when MCLAG is enabled\r\n",
    "Gmrp cannot be enabled on provider core and edge bridges. \r\n",
    "Gvrp cannot be enabled on ports other than PNP/CNP/PPNP. \r\n",
    "Gvrp cannot be enabled when PVRST is running. \r\n",
    "Gvrp cannot be enabled/disabled when bridge mode is Provider BackBone Bridge. \r\n",
    "Gmrp cannot be enabled/disabled when bridge mode is Provider BackBone Bridge. \r\n",
    "Garp cannot be Started when bridge mode is Provider BackBone Bridge. \r\n",
    "Garp Module cannot be enabled/disabled when base bridge-mode is Dot1d Bridge\r\n",
    "GVRP can't be enabled on Sisp logical port or Sisp enabled physical interfaces\r\n",
    "GARP cannot be started when MRP is running. \r\n",
    "GMRP cannot be enabled on ICCL interface \r\n",
    "GVRP cannot be enabled on ICCL interface \r\n",
    "\r\n"
};

#endif

#define GARP_MAX_ARGS          5
/*
 * In Cli Garp timers are in milliseconds, whereas in the protocol
 * it is in centi seconds.
 */
#define VLAN_GARP_TIMER_PROTOCOL_TO_CLI(i4Val) ((i4Val) * 10)
#define VLAN_GARP_TIMER_CLI_TO_PROTOCOL(i4Val) ((i4Val) / 10)

#define GARP_SET_CMD            1
#define GARP_NO_CMD             2

INT4
cli_process_garp_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);
    
INT4
cli_process_garp_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);
    
INT4
GarpCliSelectContextOnMode (tCliHandle CliHandle, UINT4 u4Cmd, 
                            UINT4 *pu4Context, UINT2 *pu2LocalPort);

INT4
GarpCliGetContextForShowCmd (tCliHandle CliHandle, UINT1 *pu1Name, 
                             UINT4 u4IfIndex, UINT4 u4CurrContext, 
                             UINT4 *pu4NextContext, UINT2 *pu2LocalPort);

INT4
GarpShutdown (tCliHandle CliHandle, INT4 i4Status);

INT4
GarpSetGvrp (tCliHandle CliHandle, UINT4 u4Action);

INT4
GarpSetPortGvrp (tCliHandle CliHandle, UINT4 u4Action, UINT4 u4PortId);

INT4
GarpSetGmrp (tCliHandle CliHandle, UINT4 u4Action);

INT4
GarpSetPortGmrp (tCliHandle CliHandle, UINT4 u4Action, UINT4 u4PortId);

INT4
GarpSetGarpTimers (tCliHandle CliHandle, UINT4 u4TimerType, UINT4 u4TimerValue,
                   UINT4 u4PortId);

INT4
GarpSetRestrictedVlanReg (tCliHandle CliHandle, UINT4 u4Action, UINT4 u4PortId);

INT4
GarpSetRestrictedGroupReg (tCliHandle CliHandle, UINT4 u4Action, UINT4 u4PortId);

#ifdef TRACE_WANTED
INT4
GarpSetDebugs (tCliHandle CliHandle, UINT4 u4DbgMod, UINT4 u4DbgValue,
               UINT1 u1Action);
INT4
GarpSetGlobalDebug (tCliHandle CliHandle, UINT1 u1Action);
#endif 

VOID IssGarpShowDebugging (tCliHandle);

INT4
ShowGarpTimers (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4PortId);

INT4
ShowGmrpStatistics (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4PortId);

INT4
ShowGvrpStatistics (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4PortId);

INT4 GarpShowRunningConfig(tCliHandle CliHandle, UINT4 u4ContextId);
INT4 GarpShowRunningConfigScalars(tCliHandle CliHandle, UINT4 u4ContextId, UINT1 *pu1HeadFlag);
INT4 GarpShowRunningConfigInterfaceDetails (tCliHandle CliHandle, 
                                            UINT4 u4ContextId, 
                                            INT4 i4LocalPort, UINT1 *pu1HeadFlag);
INT4 GarpShowRunningConfigTables(tCliHandle CliHandle, UINT4 u4ContextId);
INT4
GarpCliSetDebugLevel (tCliHandle CliHandle, INT4 i4CliDebugLevel);
INT4
GarpCliClearStatisticsPort( tCliHandle CliHandle, UINT4 u4ContextId,
UINT4);

#endif /* __GARPCLI_H__ */
