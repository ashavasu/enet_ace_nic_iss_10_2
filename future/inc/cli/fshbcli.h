/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fshbcli.h,v 1.2 2015/02/05 11:32:28 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsHbInterval[11];
extern UINT4 FsHbPeerDeadIntMultiplier[11];
extern UINT4 FsHbTrcLevel[11];
extern UINT4 FsHbStatsEnable[11];
extern UINT4 FsHbClearStats[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsHbInterval(u4SetValFsHbInterval) \
 nmhSetCmn(FsHbInterval, 11, FsHbIntervalSet, NULL, NULL, 0, 0, 0, "%u", u4SetValFsHbInterval)
#define nmhSetFsHbPeerDeadIntMultiplier(u4SetValFsHbPeerDeadIntMultiplier) \
 nmhSetCmn(FsHbPeerDeadIntMultiplier, 11, FsHbPeerDeadIntMultiplierSet, NULL, NULL, 0, 0, 0, "%u", u4SetValFsHbPeerDeadIntMultiplier)
#define nmhSetFsHbTrcLevel(u4SetValFsHbTrcLevel) \
 nmhSetCmn(FsHbTrcLevel, 11, FsHbTrcLevelSet, NULL, NULL, 0, 0, 0, "%u", u4SetValFsHbTrcLevel)
#define nmhSetFsHbStatsEnable(i4SetValFsHbStatsEnable) \
 nmhSetCmn(FsHbStatsEnable, 11, FsHbStatsEnableSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsHbStatsEnable)
#define nmhSetFsHbClearStats(i4SetValFsHbClearStats) \
 nmhSetCmn(FsHbClearStats, 11, FsHbClearStatsSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsHbClearStats)

#endif
