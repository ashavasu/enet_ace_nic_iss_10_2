/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fscfmmcli.h,v 1.6 2011/09/24 07:00:22 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIEcfmGlobalTrace[11];
extern UINT4 FsMIEcfmOui[11];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIEcfmContextId[13];
extern UINT4 FsMIEcfmSystemControl[13];
extern UINT4 FsMIEcfmModuleStatus[13];
extern UINT4 FsMIEcfmDefaultMdDefLevel[13];
extern UINT4 FsMIEcfmDefaultMdDefMhfCreation[13];
extern UINT4 FsMIEcfmDefaultMdDefIdPermission[13];
extern UINT4 FsMIEcfmLtrCacheStatus[13];
extern UINT4 FsMIEcfmLtrCacheClear[13];
extern UINT4 FsMIEcfmLtrCacheHoldTime[13];
extern UINT4 FsMIEcfmLtrCacheSize[13];
extern UINT4 FsMIEcfmMipCcmDbStatus[13];
extern UINT4 FsMIEcfmMipCcmDbClear[13];
extern UINT4 FsMIEcfmMipCcmDbSize[13];
extern UINT4 FsMIEcfmMipCcmDbHoldTime[13];
extern UINT4 FsMIEcfmMemoryFailureCount[13];
extern UINT4 FsMIEcfmBufferFailureCount[13];
extern UINT4 FsMIEcfmUpCount[13];
extern UINT4 FsMIEcfmDownCount[13];
extern UINT4 FsMIEcfmNoDftCount[13];
extern UINT4 FsMIEcfmRdiDftCount[13];
extern UINT4 FsMIEcfmMacStatusDftCount[13];
extern UINT4 FsMIEcfmRemoteCcmDftCount[13];
extern UINT4 FsMIEcfmErrorCcmDftCount[13];
extern UINT4 FsMIEcfmXconDftCount[13];
extern UINT4 FsMIEcfmCrosscheckDelay[13];
extern UINT4 FsMIEcfmMipDynamicEvaluationStatus[13];
extern UINT4 FsMIEcfmTrapControl[13];
extern UINT4 FsMIEcfmTraceOption[13];
extern UINT4 FsMIEcfmGlobalCcmOffload[13];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIEcfmPortIfIndex[13];
extern UINT4 FsMIEcfmPortLLCEncapStatus[13];
extern UINT4 FsMIEcfmPortModuleStatus[13];
extern UINT4 FsMIEcfmPortTxCfmPduCount[13];
extern UINT4 FsMIEcfmPortTxCcmCount[13];
extern UINT4 FsMIEcfmPortTxLbmCount[13];
extern UINT4 FsMIEcfmPortTxLbrCount[13];
extern UINT4 FsMIEcfmPortTxLtmCount[13];
extern UINT4 FsMIEcfmPortTxLtrCount[13];
extern UINT4 FsMIEcfmPortTxFailedCount[13];
extern UINT4 FsMIEcfmPortRxCfmPduCount[13];
extern UINT4 FsMIEcfmPortRxCcmCount[13];
extern UINT4 FsMIEcfmPortRxLbmCount[13];
extern UINT4 FsMIEcfmPortRxLbrCount[13];
extern UINT4 FsMIEcfmPortRxLtmCount[13];
extern UINT4 FsMIEcfmPortRxLtrCount[13];
extern UINT4 FsMIEcfmPortRxBadCfmPduCount[13];
extern UINT4 FsMIEcfmPortFrwdCfmPduCount[13];
extern UINT4 FsMIEcfmPortDsrdCfmPduCount[13];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIEcfmTxCfmPduCount[13];
extern UINT4 FsMIEcfmTxCcmCount[13];
extern UINT4 FsMIEcfmTxLbmCount[13];
extern UINT4 FsMIEcfmTxLbrCount[13];
extern UINT4 FsMIEcfmTxLtmCount[13];
extern UINT4 FsMIEcfmTxLtrCount[13];
extern UINT4 FsMIEcfmTxFailedCount[13];
extern UINT4 FsMIEcfmRxCfmPduCount[13];
extern UINT4 FsMIEcfmRxCcmCount[13];
extern UINT4 FsMIEcfmRxLbmCount[13];
extern UINT4 FsMIEcfmRxLbrCount[13];
extern UINT4 FsMIEcfmRxLtmCount[13];
extern UINT4 FsMIEcfmRxLtrCount[13];
extern UINT4 FsMIEcfmRxBadCfmPduCount[13];
extern UINT4 FsMIEcfmFrwdCfmPduCount[13];
extern UINT4 FsMIEcfmDsrdCfmPduCount[13];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIEcfmVlanVid[13];
extern UINT4 FsMIEcfmVlanPrimaryVid[13];
extern UINT4 FsMIEcfmVlanRowStatus[13];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIEcfmDefaultMdPrimaryVid[13];
extern UINT4 FsMIEcfmDefaultMdLevel[13];
extern UINT4 FsMIEcfmDefaultMdMhfCreation[13];
extern UINT4 FsMIEcfmDefaultMdIdPermission[13];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIEcfmMdIndex[13];
extern UINT4 FsMIEcfmMdFormat[13];
extern UINT4 FsMIEcfmMdName[13];
extern UINT4 FsMIEcfmMdMdLevel[13];
extern UINT4 FsMIEcfmMdMhfCreation[13];
extern UINT4 FsMIEcfmMdMhfIdPermission[13];
extern UINT4 FsMIEcfmMdRowStatus[13];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIEcfmMaIndex[13];
extern UINT4 FsMIEcfmMaPrimaryVlanId[13];
extern UINT4 FsMIEcfmMaFormat[13];
extern UINT4 FsMIEcfmMaName[13];
extern UINT4 FsMIEcfmMaMhfCreation[13];
extern UINT4 FsMIEcfmMaIdPermission[13];
extern UINT4 FsMIEcfmMaCcmInterval[13];
extern UINT4 FsMIEcfmMaRowStatus[13];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIEcfmMaMepListIdentifier[13];
extern UINT4 FsMIEcfmMaMepListRowStatus[13];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIEcfmMepIdentifier[13];
extern UINT4 FsMIEcfmMepIfIndex[13];
extern UINT4 FsMIEcfmMepDirection[13];
extern UINT4 FsMIEcfmMepPrimaryVid[13];
extern UINT4 FsMIEcfmMepActive[13];
extern UINT4 FsMIEcfmMepCciEnabled[13];
extern UINT4 FsMIEcfmMepCcmLtmPriority[13];
extern UINT4 FsMIEcfmMepLowPrDef[13];
extern UINT4 FsMIEcfmMepFngAlarmTime[13];
extern UINT4 FsMIEcfmMepFngResetTime[13];
extern UINT4 FsMIEcfmMepCcmSequenceErrors[13];
extern UINT4 FsMIEcfmMepCciSentCcms[13];
extern UINT4 FsMIEcfmMepLbrIn[13];
extern UINT4 FsMIEcfmMepLbrInOutOfOrder[13];
extern UINT4 FsMIEcfmMepLbrBadMsdu[13];
extern UINT4 FsMIEcfmMepUnexpLtrIn[13];
extern UINT4 FsMIEcfmMepLbrOut[13];
extern UINT4 FsMIEcfmMepTransmitLbmStatus[13];
extern UINT4 FsMIEcfmMepTransmitLbmDestMacAddress[13];
extern UINT4 FsMIEcfmMepTransmitLbmDestMepId[13];
extern UINT4 FsMIEcfmMepTransmitLbmDestIsMepId[13];
extern UINT4 FsMIEcfmMepTransmitLbmMessages[13];
extern UINT4 FsMIEcfmMepTransmitLbmDataTlv[13];
extern UINT4 FsMIEcfmMepTransmitLbmVlanPriority[13];
extern UINT4 FsMIEcfmMepTransmitLbmVlanDropEnable[13];
extern UINT4 FsMIEcfmMepTransmitLtmStatus[13];
extern UINT4 FsMIEcfmMepTransmitLtmFlags[13];
extern UINT4 FsMIEcfmMepTransmitLtmTargetMacAddress[13];
extern UINT4 FsMIEcfmMepTransmitLtmTargetMepId[13];
extern UINT4 FsMIEcfmMepTransmitLtmTargetIsMepId[13];
extern UINT4 FsMIEcfmMepTransmitLtmTtl[13];
extern UINT4 FsMIEcfmMepTransmitLtmEgressIdentifier[13];
extern UINT4 FsMIEcfmMepRowStatus[13];
extern UINT4 FsMIEcfmMepCcmOffload[13];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIEcfmMipIfIndex[13];
extern UINT4 FsMIEcfmMipMdLevel[13];
extern UINT4 FsMIEcfmMipVid[13];
extern UINT4 FsMIEcfmMipActive[13];
extern UINT4 FsMIEcfmDynMipPreventionRowStatus[13];
extern UINT4 FsMIEcfmMipRowStatus[13];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIEcfmRMepPortStatusDefect[13];
extern UINT4 FsMIEcfmRMepInterfaceStatusDefect[13];
extern UINT4 FsMIEcfmRMepCcmDefect[13];
extern UINT4 FsMIEcfmRMepRDIDefect[13];
extern UINT4 FsMIEcfmRMepMacAddress[13];
extern UINT4 FsMIEcfmRMepRdi[13];
extern UINT4 FsMIEcfmRMepPortStatusTlv[13];
extern UINT4 FsMIEcfmRMepInterfaceStatusTlv[13];
extern UINT4 FsMIEcfmRMepChassisIdSubtype[13];
extern UINT4 FsMIEcfmRMepDbChassisId[13];
extern UINT4 FsMIEcfmRMepManAddressDomain[13];
extern UINT4 FsMIEcfmRMepManAddress[13];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIEcfmXconnRMepId[13];
extern UINT4 FsMIEcfmErrorRMepId[13];
extern UINT4 FsMIEcfmMepDefectRDICcm[13];
extern UINT4 FsMIEcfmMepDefectMacStatus[13];
extern UINT4 FsMIEcfmMepDefectRemoteCcm[13];
extern UINT4 FsMIEcfmMepDefectErrorCcm[13];
extern UINT4 FsMIEcfmMepDefectXconnCcm[13];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIEcfmMepArchiveHoldTime[13];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIEcfmMaCrosscheckStatus[13];

