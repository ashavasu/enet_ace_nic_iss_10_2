/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: vrrp3cli.h,v 1.2 2014/03/04 11:17:43 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Vrrpv3OperationsVrId[12];
extern UINT4 Vrrpv3OperationsInetAddrType[12];
extern UINT4 Vrrpv3OperationsPrimaryIpAddr[12];
extern UINT4 Vrrpv3OperationsPriority[12];
extern UINT4 Vrrpv3OperationsAdvInterval[12];
extern UINT4 Vrrpv3OperationsPreemptMode[12];
extern UINT4 Vrrpv3OperationsAcceptMode[12];
extern UINT4 Vrrpv3OperationsRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetVrrpv3OperationsPrimaryIpAddr(i4IfIndex , i4Vrrpv3OperationsVrId , i4Vrrpv3OperationsInetAddrType ,pSetValVrrpv3OperationsPrimaryIpAddr)	\
	nmhSetCmn(Vrrpv3OperationsPrimaryIpAddr, 12, Vrrpv3OperationsPrimaryIpAddrSet, VrrpLock, VrrpUnLock, 0, 0, 3, "%i %i %i %s", i4IfIndex , i4Vrrpv3OperationsVrId , i4Vrrpv3OperationsInetAddrType ,pSetValVrrpv3OperationsPrimaryIpAddr)
#define nmhSetVrrpv3OperationsPriority(i4IfIndex , i4Vrrpv3OperationsVrId , i4Vrrpv3OperationsInetAddrType ,u4SetValVrrpv3OperationsPriority)	\
	nmhSetCmn(Vrrpv3OperationsPriority, 12, Vrrpv3OperationsPrioritySet, VrrpLock, VrrpUnLock, 0, 0, 3, "%i %i %i %u", i4IfIndex , i4Vrrpv3OperationsVrId , i4Vrrpv3OperationsInetAddrType ,u4SetValVrrpv3OperationsPriority)
#define nmhSetVrrpv3OperationsAdvInterval(i4IfIndex , i4Vrrpv3OperationsVrId , i4Vrrpv3OperationsInetAddrType ,i4SetValVrrpv3OperationsAdvInterval)	\
	nmhSetCmn(Vrrpv3OperationsAdvInterval, 12, Vrrpv3OperationsAdvIntervalSet, VrrpLock, VrrpUnLock, 0, 0, 3, "%i %i %i %i", i4IfIndex , i4Vrrpv3OperationsVrId , i4Vrrpv3OperationsInetAddrType ,i4SetValVrrpv3OperationsAdvInterval)
#define nmhSetVrrpv3OperationsPreemptMode(i4IfIndex , i4Vrrpv3OperationsVrId , i4Vrrpv3OperationsInetAddrType ,i4SetValVrrpv3OperationsPreemptMode)	\
	nmhSetCmn(Vrrpv3OperationsPreemptMode, 12, Vrrpv3OperationsPreemptModeSet, VrrpLock, VrrpUnLock, 0, 0, 3, "%i %i %i %i", i4IfIndex , i4Vrrpv3OperationsVrId , i4Vrrpv3OperationsInetAddrType ,i4SetValVrrpv3OperationsPreemptMode)
#define nmhSetVrrpv3OperationsAcceptMode(i4IfIndex , i4Vrrpv3OperationsVrId , i4Vrrpv3OperationsInetAddrType ,i4SetValVrrpv3OperationsAcceptMode)	\
	nmhSetCmn(Vrrpv3OperationsAcceptMode, 12, Vrrpv3OperationsAcceptModeSet, VrrpLock, VrrpUnLock, 0, 0, 3, "%i %i %i %i", i4IfIndex , i4Vrrpv3OperationsVrId , i4Vrrpv3OperationsInetAddrType ,i4SetValVrrpv3OperationsAcceptMode)
#define nmhSetVrrpv3OperationsRowStatus(i4IfIndex , i4Vrrpv3OperationsVrId , i4Vrrpv3OperationsInetAddrType ,i4SetValVrrpv3OperationsRowStatus)	\
	nmhSetCmn(Vrrpv3OperationsRowStatus, 12, Vrrpv3OperationsRowStatusSet, VrrpLock, VrrpUnLock, 0, 1, 3, "%i %i %i %i", i4IfIndex , i4Vrrpv3OperationsVrId , i4Vrrpv3OperationsInetAddrType ,i4SetValVrrpv3OperationsRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Vrrpv3AssociatedIpAddrAddress[12];
extern UINT4 Vrrpv3AssociatedIpAddrRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetVrrpv3AssociatedIpAddrRowStatus(i4IfIndex , i4Vrrpv3OperationsVrId , i4Vrrpv3OperationsInetAddrType , pVrrpv3AssociatedIpAddrAddress ,i4SetValVrrpv3AssociatedIpAddrRowStatus)	\
	nmhSetCmn(Vrrpv3AssociatedIpAddrRowStatus, 12, Vrrpv3AssociatedIpAddrRowStatusSet, VrrpLock, VrrpUnLock, 0, 1, 4, "%i %i %i %s %i", i4IfIndex , i4Vrrpv3OperationsVrId , i4Vrrpv3OperationsInetAddrType , pVrrpv3AssociatedIpAddrAddress ,i4SetValVrrpv3AssociatedIpAddrRowStatus)

#endif
