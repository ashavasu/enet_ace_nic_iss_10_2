/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdgnlcli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 MplsLdpEntityGenericLRMin[14];
extern UINT4 MplsLdpEntityGenericLRMax[14];
extern UINT4 MplsLdpEntityGenericLabelSpace[14];
extern UINT4 MplsLdpEntityGenericIfIndexOrZero[14];
extern UINT4 MplsLdpEntityGenericLRStorageType[14];
extern UINT4 MplsLdpEntityGenericLRRowStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetMplsLdpEntityGenericLabelSpace(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex , u4MplsLdpEntityGenericLRMin , u4MplsLdpEntityGenericLRMax ,i4SetValMplsLdpEntityGenericLabelSpace)	\
	nmhSetCmn(MplsLdpEntityGenericLabelSpace, 14, MplsLdpEntityGenericLabelSpaceSet, LdpLock, LdpUnLock, 0, 0, 4, "%s %u %u %u %i", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex , u4MplsLdpEntityGenericLRMin , u4MplsLdpEntityGenericLRMax ,i4SetValMplsLdpEntityGenericLabelSpace)
#define nmhSetMplsLdpEntityGenericIfIndexOrZero(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex , u4MplsLdpEntityGenericLRMin , u4MplsLdpEntityGenericLRMax ,i4SetValMplsLdpEntityGenericIfIndexOrZero)	\
	nmhSetCmn(MplsLdpEntityGenericIfIndexOrZero, 14, MplsLdpEntityGenericIfIndexOrZeroSet, LdpLock, LdpUnLock, 0, 0, 4, "%s %u %u %u %i", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex , u4MplsLdpEntityGenericLRMin , u4MplsLdpEntityGenericLRMax ,i4SetValMplsLdpEntityGenericIfIndexOrZero)
#define nmhSetMplsLdpEntityGenericLRStorageType(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex , u4MplsLdpEntityGenericLRMin , u4MplsLdpEntityGenericLRMax ,i4SetValMplsLdpEntityGenericLRStorageType)	\
	nmhSetCmn(MplsLdpEntityGenericLRStorageType, 14, MplsLdpEntityGenericLRStorageTypeSet, LdpLock, LdpUnLock, 0, 0, 4, "%s %u %u %u %i", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex , u4MplsLdpEntityGenericLRMin , u4MplsLdpEntityGenericLRMax ,i4SetValMplsLdpEntityGenericLRStorageType)
#define nmhSetMplsLdpEntityGenericLRRowStatus(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex , u4MplsLdpEntityGenericLRMin , u4MplsLdpEntityGenericLRMax ,i4SetValMplsLdpEntityGenericLRRowStatus)	\
	nmhSetCmn(MplsLdpEntityGenericLRRowStatus, 14, MplsLdpEntityGenericLRRowStatusSet, LdpLock, LdpUnLock, 0, 1, 4, "%s %u %u %u %i", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex , u4MplsLdpEntityGenericLRMin , u4MplsLdpEntityGenericLRMax ,i4SetValMplsLdpEntityGenericLRRowStatus)

#endif
