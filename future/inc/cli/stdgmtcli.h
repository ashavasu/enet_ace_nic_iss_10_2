/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdgmtcli.h,v 1.1 2011/11/30 09:58:10 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 GmplsTunnelUnnumIf[13];
extern UINT4 GmplsTunnelAttributes[13];
extern UINT4 GmplsTunnelLSPEncoding[13];
extern UINT4 GmplsTunnelSwitchingType[13];
extern UINT4 GmplsTunnelLinkProtection[13];
extern UINT4 GmplsTunnelGPid[13];
extern UINT4 GmplsTunnelSecondary[13];
extern UINT4 GmplsTunnelDirection[13];
extern UINT4 GmplsTunnelPathComp[13];
extern UINT4 GmplsTunnelUpstreamNotifyRecipientType[13];
extern UINT4 GmplsTunnelUpstreamNotifyRecipient[13];
extern UINT4 GmplsTunnelSendResvNotifyRecipientType[13];
extern UINT4 GmplsTunnelSendResvNotifyRecipient[13];
extern UINT4 GmplsTunnelDownstreamNotifyRecipientType[13];
extern UINT4 GmplsTunnelDownstreamNotifyRecipient[13];
extern UINT4 GmplsTunnelSendPathNotifyRecipientType[13];
extern UINT4 GmplsTunnelSendPathNotifyRecipient[13];
extern UINT4 GmplsTunnelAdminStatusFlags[13];
extern UINT4 GmplsTunnelExtraParamsPtr[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetGmplsTunnelUnnumIf(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValGmplsTunnelUnnumIf)	\
	nmhSetCmn(GmplsTunnelUnnumIf, 13, GmplsTunnelUnnumIfSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValGmplsTunnelUnnumIf)
#define nmhSetGmplsTunnelAttributes(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,pSetValGmplsTunnelAttributes)	\
	nmhSetCmn(GmplsTunnelAttributes, 13, GmplsTunnelAttributesSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %s", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,pSetValGmplsTunnelAttributes)
#define nmhSetGmplsTunnelLSPEncoding(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValGmplsTunnelLSPEncoding)	\
	nmhSetCmn(GmplsTunnelLSPEncoding, 13, GmplsTunnelLSPEncodingSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValGmplsTunnelLSPEncoding)
#define nmhSetGmplsTunnelSwitchingType(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValGmplsTunnelSwitchingType)	\
	nmhSetCmn(GmplsTunnelSwitchingType, 13, GmplsTunnelSwitchingTypeSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValGmplsTunnelSwitchingType)
#define nmhSetGmplsTunnelLinkProtection(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,pSetValGmplsTunnelLinkProtection)	\
	nmhSetCmn(GmplsTunnelLinkProtection, 13, GmplsTunnelLinkProtectionSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %s", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,pSetValGmplsTunnelLinkProtection)
#define nmhSetGmplsTunnelGPid(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValGmplsTunnelGPid)	\
	nmhSetCmn(GmplsTunnelGPid, 13, GmplsTunnelGPidSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValGmplsTunnelGPid)
#define nmhSetGmplsTunnelSecondary(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValGmplsTunnelSecondary)	\
	nmhSetCmn(GmplsTunnelSecondary, 13, GmplsTunnelSecondarySet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValGmplsTunnelSecondary)
#define nmhSetGmplsTunnelDirection(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValGmplsTunnelDirection)	\
	nmhSetCmn(GmplsTunnelDirection, 13, GmplsTunnelDirectionSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValGmplsTunnelDirection)
#define nmhSetGmplsTunnelPathComp(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValGmplsTunnelPathComp)	\
	nmhSetCmn(GmplsTunnelPathComp, 13, GmplsTunnelPathCompSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValGmplsTunnelPathComp)
#define nmhSetGmplsTunnelUpstreamNotifyRecipientType(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValGmplsTunnelUpstreamNotifyRecipientType)	\
	nmhSetCmn(GmplsTunnelUpstreamNotifyRecipientType, 13, GmplsTunnelUpstreamNotifyRecipientTypeSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValGmplsTunnelUpstreamNotifyRecipientType)
#define nmhSetGmplsTunnelUpstreamNotifyRecipient(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,pSetValGmplsTunnelUpstreamNotifyRecipient)	\
	nmhSetCmn(GmplsTunnelUpstreamNotifyRecipient, 13, GmplsTunnelUpstreamNotifyRecipientSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %s", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,pSetValGmplsTunnelUpstreamNotifyRecipient)
#define nmhSetGmplsTunnelSendResvNotifyRecipientType(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValGmplsTunnelSendResvNotifyRecipientType)	\
	nmhSetCmn(GmplsTunnelSendResvNotifyRecipientType, 13, GmplsTunnelSendResvNotifyRecipientTypeSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValGmplsTunnelSendResvNotifyRecipientType)
#define nmhSetGmplsTunnelSendResvNotifyRecipient(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,pSetValGmplsTunnelSendResvNotifyRecipient)	\
	nmhSetCmn(GmplsTunnelSendResvNotifyRecipient, 13, GmplsTunnelSendResvNotifyRecipientSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %s", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,pSetValGmplsTunnelSendResvNotifyRecipient)
#define nmhSetGmplsTunnelDownstreamNotifyRecipientType(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValGmplsTunnelDownstreamNotifyRecipientType)	\
	nmhSetCmn(GmplsTunnelDownstreamNotifyRecipientType, 13, GmplsTunnelDownstreamNotifyRecipientTypeSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValGmplsTunnelDownstreamNotifyRecipientType)
#define nmhSetGmplsTunnelDownstreamNotifyRecipient(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,pSetValGmplsTunnelDownstreamNotifyRecipient)	\
	nmhSetCmn(GmplsTunnelDownstreamNotifyRecipient, 13, GmplsTunnelDownstreamNotifyRecipientSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %s", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,pSetValGmplsTunnelDownstreamNotifyRecipient)
#define nmhSetGmplsTunnelSendPathNotifyRecipientType(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValGmplsTunnelSendPathNotifyRecipientType)	\
	nmhSetCmn(GmplsTunnelSendPathNotifyRecipientType, 13, GmplsTunnelSendPathNotifyRecipientTypeSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValGmplsTunnelSendPathNotifyRecipientType)
#define nmhSetGmplsTunnelSendPathNotifyRecipient(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,pSetValGmplsTunnelSendPathNotifyRecipient)	\
	nmhSetCmn(GmplsTunnelSendPathNotifyRecipient, 13, GmplsTunnelSendPathNotifyRecipientSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %s", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,pSetValGmplsTunnelSendPathNotifyRecipient)
#define nmhSetGmplsTunnelAdminStatusFlags(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,pSetValGmplsTunnelAdminStatusFlags)	\
	nmhSetCmn(GmplsTunnelAdminStatusFlags, 13, GmplsTunnelAdminStatusFlagsSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %s", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,pSetValGmplsTunnelAdminStatusFlags)
#define nmhSetGmplsTunnelExtraParamsPtr(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,pSetValGmplsTunnelExtraParamsPtr)	\
	nmhSetCmn(GmplsTunnelExtraParamsPtr, 13, GmplsTunnelExtraParamsPtrSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %o", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,pSetValGmplsTunnelExtraParamsPtr)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 GmplsTunnelHopExplicitForwardLabel[13];
extern UINT4 GmplsTunnelHopExplicitForwardLabelPtr[13];
extern UINT4 GmplsTunnelHopExplicitReverseLabel[13];
extern UINT4 GmplsTunnelHopExplicitReverseLabelPtr[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetGmplsTunnelHopExplicitForwardLabel(u4MplsTunnelHopListIndex , u4MplsTunnelHopPathOptionIndex , u4MplsTunnelHopIndex ,u4SetValGmplsTunnelHopExplicitForwardLabel)	\
	nmhSetCmn(GmplsTunnelHopExplicitForwardLabel, 13, GmplsTunnelHopExplicitForwardLabelSet, NULL, NULL, 0, 0, 3, "%u %u %u %u", u4MplsTunnelHopListIndex , u4MplsTunnelHopPathOptionIndex , u4MplsTunnelHopIndex ,u4SetValGmplsTunnelHopExplicitForwardLabel)
#define nmhSetGmplsTunnelHopExplicitForwardLabelPtr(u4MplsTunnelHopListIndex , u4MplsTunnelHopPathOptionIndex , u4MplsTunnelHopIndex ,pSetValGmplsTunnelHopExplicitForwardLabelPtr)	\
	nmhSetCmn(GmplsTunnelHopExplicitForwardLabelPtr, 13, GmplsTunnelHopExplicitForwardLabelPtrSet, NULL, NULL, 0, 0, 3, "%u %u %u %o", u4MplsTunnelHopListIndex , u4MplsTunnelHopPathOptionIndex , u4MplsTunnelHopIndex ,pSetValGmplsTunnelHopExplicitForwardLabelPtr)
#define nmhSetGmplsTunnelHopExplicitReverseLabel(u4MplsTunnelHopListIndex , u4MplsTunnelHopPathOptionIndex , u4MplsTunnelHopIndex ,u4SetValGmplsTunnelHopExplicitReverseLabel)	\
	nmhSetCmn(GmplsTunnelHopExplicitReverseLabel, 13, GmplsTunnelHopExplicitReverseLabelSet, NULL, NULL, 0, 0, 3, "%u %u %u %u", u4MplsTunnelHopListIndex , u4MplsTunnelHopPathOptionIndex , u4MplsTunnelHopIndex ,u4SetValGmplsTunnelHopExplicitReverseLabel)
#define nmhSetGmplsTunnelHopExplicitReverseLabelPtr(u4MplsTunnelHopListIndex , u4MplsTunnelHopPathOptionIndex , u4MplsTunnelHopIndex ,pSetValGmplsTunnelHopExplicitReverseLabelPtr)	\
	nmhSetCmn(GmplsTunnelHopExplicitReverseLabelPtr, 13, GmplsTunnelHopExplicitReverseLabelPtrSet, NULL, NULL, 0, 0, 3, "%u %u %u %o", u4MplsTunnelHopListIndex , u4MplsTunnelHopPathOptionIndex , u4MplsTunnelHopIndex ,pSetValGmplsTunnelHopExplicitReverseLabelPtr)

#endif
