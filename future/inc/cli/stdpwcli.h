/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdpwcli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 PwIndex[13];
extern UINT4 PwType[13];
extern UINT4 PwOwner[13];
extern UINT4 PwPsnType[13];
extern UINT4 PwSetUpPriority[13];
extern UINT4 PwHoldingPriority[13];
extern UINT4 PwPeerAddrType[13];
extern UINT4 PwPeerAddr[13];
extern UINT4 PwAttachedPwIndex[13];
extern UINT4 PwIfIndex[13];
extern UINT4 PwID[13];
extern UINT4 PwLocalGroupID[13];
extern UINT4 PwGroupAttachmentID[13];
extern UINT4 PwLocalAttachmentID[13];
extern UINT4 PwPeerAttachmentID[13];
extern UINT4 PwCwPreference[13];
extern UINT4 PwLocalIfMtu[13];
extern UINT4 PwLocalIfString[13];
extern UINT4 PwLocalCapabAdvert[13];
extern UINT4 PwFragmentCfgSize[13];
extern UINT4 PwFcsRetentioncfg[13];
extern UINT4 PwOutboundLabel[13];
extern UINT4 PwInboundLabel[13];
extern UINT4 PwName[13];
extern UINT4 PwDescr[13];
extern UINT4 PwAdminStatus[13];
extern UINT4 PwRowStatus[13];
extern UINT4 PwStorageType[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetPwType(u4PwIndex ,i4SetValPwType)	\
	nmhSetCmn(PwType, 13, PwTypeSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %i", u4PwIndex ,i4SetValPwType)
#define nmhSetPwOwner(u4PwIndex ,i4SetValPwOwner)	\
	nmhSetCmn(PwOwner, 13, PwOwnerSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %i", u4PwIndex ,i4SetValPwOwner)
#define nmhSetPwPsnType(u4PwIndex ,i4SetValPwPsnType)	\
	nmhSetCmn(PwPsnType, 13, PwPsnTypeSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %i", u4PwIndex ,i4SetValPwPsnType)
#define nmhSetPwSetUpPriority(u4PwIndex ,i4SetValPwSetUpPriority)	\
	nmhSetCmn(PwSetUpPriority, 13, PwSetUpPrioritySet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %i", u4PwIndex ,i4SetValPwSetUpPriority)
#define nmhSetPwHoldingPriority(u4PwIndex ,i4SetValPwHoldingPriority)	\
	nmhSetCmn(PwHoldingPriority, 13, PwHoldingPrioritySet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %i", u4PwIndex ,i4SetValPwHoldingPriority)
#define nmhSetPwPeerAddrType(u4PwIndex ,i4SetValPwPeerAddrType)	\
	nmhSetCmn(PwPeerAddrType, 13, PwPeerAddrTypeSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %i", u4PwIndex ,i4SetValPwPeerAddrType)
#define nmhSetPwPeerAddr(u4PwIndex ,pSetValPwPeerAddr)	\
	nmhSetCmn(PwPeerAddr, 13, PwPeerAddrSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %s", u4PwIndex ,pSetValPwPeerAddr)
#define nmhSetPwAttachedPwIndex(u4PwIndex ,u4SetValPwAttachedPwIndex)	\
	nmhSetCmn(PwAttachedPwIndex, 13, PwAttachedPwIndexSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %u", u4PwIndex ,u4SetValPwAttachedPwIndex)
#define nmhSetPwIfIndex(u4PwIndex ,i4SetValPwIfIndex)	\
	nmhSetCmn(PwIfIndex, 13, PwIfIndexSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %i", u4PwIndex ,i4SetValPwIfIndex)
#define nmhSetPwID(u4PwIndex ,u4SetValPwID)	\
	nmhSetCmn(PwID, 13, PwIDSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %u", u4PwIndex ,u4SetValPwID)
#define nmhSetPwLocalGroupID(u4PwIndex ,u4SetValPwLocalGroupID)	\
	nmhSetCmn(PwLocalGroupID, 13, PwLocalGroupIDSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %u", u4PwIndex ,u4SetValPwLocalGroupID)
#define nmhSetPwGroupAttachmentID(u4PwIndex ,pSetValPwGroupAttachmentID)	\
	nmhSetCmn(PwGroupAttachmentID, 13, PwGroupAttachmentIDSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %s", u4PwIndex ,pSetValPwGroupAttachmentID)
#define nmhSetPwLocalAttachmentID(u4PwIndex ,pSetValPwLocalAttachmentID)	\
	nmhSetCmn(PwLocalAttachmentID, 13, PwLocalAttachmentIDSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %s", u4PwIndex ,pSetValPwLocalAttachmentID)
#define nmhSetPwPeerAttachmentID(u4PwIndex ,pSetValPwPeerAttachmentID)	\
	nmhSetCmn(PwPeerAttachmentID, 13, PwPeerAttachmentIDSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %s", u4PwIndex ,pSetValPwPeerAttachmentID)
#define nmhSetPwCwPreference(u4PwIndex ,i4SetValPwCwPreference)	\
	nmhSetCmn(PwCwPreference, 13, PwCwPreferenceSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %i", u4PwIndex ,i4SetValPwCwPreference)
#define nmhSetPwLocalIfMtu(u4PwIndex ,u4SetValPwLocalIfMtu)	\
	nmhSetCmn(PwLocalIfMtu, 13, PwLocalIfMtuSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %u", u4PwIndex ,u4SetValPwLocalIfMtu)
#define nmhSetPwLocalIfString(u4PwIndex ,i4SetValPwLocalIfString)	\
	nmhSetCmn(PwLocalIfString, 13, PwLocalIfStringSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %i", u4PwIndex ,i4SetValPwLocalIfString)
#define nmhSetPwLocalCapabAdvert(u4PwIndex ,pSetValPwLocalCapabAdvert)	\
	nmhSetCmn(PwLocalCapabAdvert, 13, PwLocalCapabAdvertSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %s", u4PwIndex ,pSetValPwLocalCapabAdvert)
#define nmhSetPwFragmentCfgSize(u4PwIndex ,u4SetValPwFragmentCfgSize)	\
	nmhSetCmn(PwFragmentCfgSize, 13, PwFragmentCfgSizeSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %u", u4PwIndex ,u4SetValPwFragmentCfgSize)
#define nmhSetPwFcsRetentioncfg(u4PwIndex ,i4SetValPwFcsRetentioncfg)	\
	nmhSetCmn(PwFcsRetentioncfg, 13, PwFcsRetentioncfgSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %i", u4PwIndex ,i4SetValPwFcsRetentioncfg)
#define nmhSetPwOutboundLabel(u4PwIndex ,u4SetValPwOutboundLabel)	\
	nmhSetCmn(PwOutboundLabel, 13, PwOutboundLabelSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %u", u4PwIndex ,u4SetValPwOutboundLabel)
#define nmhSetPwInboundLabel(u4PwIndex ,u4SetValPwInboundLabel)	\
	nmhSetCmn(PwInboundLabel, 13, PwInboundLabelSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %u", u4PwIndex ,u4SetValPwInboundLabel)
#define nmhSetPwName(u4PwIndex ,pSetValPwName)	\
	nmhSetCmn(PwName, 13, PwNameSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %s", u4PwIndex ,pSetValPwName)
#define nmhSetPwDescr(u4PwIndex ,pSetValPwDescr)	\
	nmhSetCmn(PwDescr, 13, PwDescrSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %s", u4PwIndex ,pSetValPwDescr)
#define nmhSetPwAdminStatus(u4PwIndex ,i4SetValPwAdminStatus)	\
	nmhSetCmn(PwAdminStatus, 13, PwAdminStatusSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %i", u4PwIndex ,i4SetValPwAdminStatus)
#define nmhSetPwRowStatus(u4PwIndex ,i4SetValPwRowStatus)	\
	nmhSetCmn(PwRowStatus, 13, PwRowStatusSet, L2vpnLock, L2vpnUnLock, 0, 1, 1, "%u %i", u4PwIndex ,i4SetValPwRowStatus)
#define nmhSetPwStorageType(u4PwIndex ,i4SetValPwStorageType)	\
	nmhSetCmn(PwStorageType, 13, PwStorageTypeSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %i", u4PwIndex ,i4SetValPwStorageType)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 PwUpDownNotifEnable[11];
extern UINT4 PwDeletedNotifEnable[11];
extern UINT4 PwNotifRate[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetPwUpDownNotifEnable(i4SetValPwUpDownNotifEnable)	\
	nmhSetCmn(PwUpDownNotifEnable, 11, PwUpDownNotifEnableSet, L2vpnLock, L2vpnUnLock, 0, 0, 0, "%i", i4SetValPwUpDownNotifEnable)
#define nmhSetPwDeletedNotifEnable(i4SetValPwDeletedNotifEnable)	\
	nmhSetCmn(PwDeletedNotifEnable, 11, PwDeletedNotifEnableSet, L2vpnLock, L2vpnUnLock, 0, 0, 0, "%i", i4SetValPwDeletedNotifEnable)
#define nmhSetPwNotifRate(u4SetValPwNotifRate)	\
	nmhSetCmn(PwNotifRate, 11, PwNotifRateSet, L2vpnLock, L2vpnUnLock, 0, 0, 0, "%u", u4SetValPwNotifRate)

#endif
