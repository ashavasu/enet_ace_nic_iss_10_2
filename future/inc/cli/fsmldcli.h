/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmldcli.h,v 1.4 2015/02/13 11:13:58 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsmldNoOfCacheEntries[10];
extern UINT4 FsmldNoOfRoutingProtocols[10];
extern UINT4 FsmldTraceDebug[10];
extern UINT4 FsmldDebugLevel[10];
extern UINT4 FsmldMode[10];
extern UINT4 FsmldProtocolUpDown[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsmldNoOfCacheEntries(u4SetValFsmldNoOfCacheEntries) \
 nmhSetCmn(FsmldNoOfCacheEntries, 10, FsmldNoOfCacheEntriesSet, NULL, NULL, 0, 0, 0, "%u", u4SetValFsmldNoOfCacheEntries)
#define nmhSetFsmldNoOfRoutingProtocols(u4SetValFsmldNoOfRoutingProtocols) \
 nmhSetCmn(FsmldNoOfRoutingProtocols, 10, FsmldNoOfRoutingProtocolsSet, NULL, NULL, 0, 0, 0, "%u", u4SetValFsmldNoOfRoutingProtocols)
#define nmhSetFsmldTraceDebug(u4SetValFsmldTraceDebug) \
 nmhSetCmn(FsmldTraceDebug, 10, FsmldTraceDebugSet, NULL, NULL, 0, 0, 0, "%u", u4SetValFsmldTraceDebug)
#define nmhSetFsmldDebugLevel(u4SetValFsmldDebuglevel) \
 nmhSetCmn(FsmldDebugLevel, 10, FsmldDebugLevelSet, NULL, NULL, 0, 0, 0, "%u", u4SetValFsmldDebuglevel)
#define nmhSetFsmldMode(i4SetValFsmldMode) \
 nmhSetCmn(FsmldMode, 10, FsmldModeSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsmldMode)
#define nmhSetFsmldProtocolUpDown(i4SetValFsmldProtocolUpDown) \
 nmhSetCmn(FsmldProtocolUpDown, 10, FsmldProtocolUpDownSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsmldProtocolUpDown)

#endif
