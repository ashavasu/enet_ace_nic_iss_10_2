/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsosoacli.h,v 1.2 2010/07/28 12:22:14 prabuc Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FutOasAppRegFileName[11];
extern UINT4 FutOasAppDeRegFileName[11];
extern UINT4 FutOasAppOpqLsaSndFileName[11];

