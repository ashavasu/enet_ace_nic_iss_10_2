
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: snmp3cli.h,v 1.17 2017/11/20 13:11:25 siva Exp $
 *
 * Description: This file contains macros to the CLI commands and 
 * structures for the SNMPv3 module
 *******************************************************************/

#ifndef _SNMPCLINEW_H
#define _SNMPCLINEW_H

#include "cli.h"
#include "fssnmp.h"

#define     CLI_TRAP_ENABLE               1
#define     CLI_TRAP_DISABLE              2

#define     SNMP3_TRC_ENABLED             1
#define     SNMP3_TRC_DISABLED            2

/* Macros used in SNX CLI */
#define     CLI_SNX_IP4                   1/* Used to Specify IPv4 Address Type */
#define     CLI_SNX_IP6                   2/* Used to Specify IPv6 Address Type */

#define     CLI_SNX_MAX_ARGS              5 

#define     CLI_SNX_DEFAULT_MASTER_PORT   705
enum {
 CLI_SNMP3_COMMUNITY_CREATE = 1,
 CLI_SNMP3_COMMUNITY_NO_CREATE,
 CLI_SNMP3_GROUP_CREATE,
 CLI_SNMP3_GROUP_NO_CREATE,
 CLI_SNMP3_ACCESS_CREATE,
 CLI_SNMP3_ACCESS_NO_CREATE,
 CLI_SNMP3_ENGINE_ID,
 CLI_SNMP3_ENGINE_NO_ID,
 CLI_SNMP3_VIEW_CREATE,
 CLI_SNMP3_VIEW_NO_CREATE,
 CLI_SNMP3_TRGTADDR_CREATE,
 CLI_SNMP3_TRGTADDR_NO_CREATE,
 CLI_SNMP3_TRGTPARAMS_CREATE,
 CLI_SNMP3_TRGTPARAMS_NO_CREATE,
 CLI_SNMP3_USER_CREATE,
 CLI_SNMP3_USER_NO_CREATE,
 CLI_SNMP3_NOTIF_CREATE,
 CLI_SNMP3_NOTIF_NO_CREATE,
 CLI_SNMP3_SHOW_SNMP_TCP,
 CLI_SNMP3_SHOW_DETAILS,
 CLI_SNMP3_SHOW_COMMUNITY,
 CLI_SNMP3_SHOW_GROUP,
 CLI_SNMP3_SHOW_ACCESS,
 CLI_SNMP3_SHOW_ENGINEID,
 CLI_SNMP3_SHOW_VIEWTREE,
 CLI_SNMP3_SHOW_TRGTADDR,
 CLI_SNMP3_SHOW_TRGTPARAM,
 CLI_SNMP3_SHOW_USER,
 CLI_SNMP3_SHOW_NOTIF,
 CLI_SNMP3_AUTHTRAP_STATUS,
 CLI_SNMP3_ENABLE_TRAP,
 CLI_SNMP3_DISABLE_TRAP,
 CLI_SNMP3_SYSTEM_CONTACT,
 CLI_SNMP3_SYSTEM_LOCATION,
 CLI_SNMP3_SHOW_INFMSTAT,
 CLI_SNMP3_SHOW_TRAPS,
 CLI_SNMP3_LISTEN_TRAP_PORT,
 CLI_SNMP3_NO_LISTEN_TRAP_PORT,
 CLI_SNMP3_TCP_ENABLE,
 CLI_SNMP3_NO_TCP_ENABLE,
 CLI_SNMP3_TCP_TRAP_ENABLE,
 CLI_SNMP3_NO_TCP_TRAP_ENABLE,
 CLI_SNMP3_LISTEN_TCP_PORT,
 CLI_SNMP3_NO_LISTEN_TCP_PORT,
 CLI_SNMP3_LISTEN_TCP_TRAP_PORT,
 CLI_SNMP3_NO_LISTEN_TCP_TRAP_PORT,

 CLI_SNMP3_PROXY_LISTEN_TRAP_PORT,
 CLI_SNMP3_NO_PROXY_LISTEN_TRAP_PORT,
 CLI_SNMP3_SHOW_PROXY_LISTEN_TRAP_PORT,
 CLI_SNMP3_AGENT_ENABLE,
 CLI_SNMP3_AGENT_DISABLE,
 CLI_SNMP3_PROXY_CREATE,
 CLI_SNMP3_PROP_PROXY_CREATE,
 CLI_SNMP3_PROXY_NO_CREATE,
 CLI_SNMP3_PROP_PROXY_NO_CREATE,
 CLI_SNMP3_PROXY_CREATANDWAIT,
 CLI_SNMP3_PROXY_CREATEANDGO,
 CLI_SNMP3_SHOW_PROXY,
 CLI_SNMP3_SHOW_PROP_PROXY,
 CLI_SNMP3_AGENT_PORT,
 CLI_SNMP3_NOTIFY_FILTER_CREATE,
 CLI_SNMP3_NOTIFY_FILTER_NO_CREATE,
 CLI_SNMP3_SHOW_NOTIFY_FILTER,
 CLI_SNMP3_NAME_TO_OID,
 CLI_SNMP3_OID_TO_NAME,
 CLI_SNMP3_SET_NAME_VALUE,
 CLI_SNMP3_SET_OID_VALUE,
 CLI_SNMP3_GET_NAME,
 CLI_SNMP3_GET_OID,
 CLI_SNMP3_GETNEXT_NAME,
 CLI_SNMP3_GETNEXT_OID,
 CLI_SNMP3_NAME_WALK,
 CLI_SNMP3_OID_WALK,
 CLI_SNMP3_SET_TRAP_FILTER_NAME,
 CLI_SNMP3_SET_TRAP_FILTER_OID,
 CLI_SNMP3_SET_NO_TRAP_FILTER_NAME,
 CLI_SNMP3_SET_NO_TRAP_FILTER_OID,
 CLI_SNMP3_DEBUG,
 CLI_SNMP3_NO_DEBUG
};

/* Enum declaration for SNX action command identifiers */
enum
{
    CLI_SNX_SUBAGENT_ENABLE = 1,
    CLI_SNX_SHOW_INFO,
    CLI_SNX_SHOW_STATISTICS,
    CLI_SNX_SUBAGENT_DISABLE,
    CLI_SNX_MAX_COMMANDS            
};        

/* SNX Error codes */
enum
{
    CLI_INVALID_PORT_NO = 1,
    CLI_INVALID_IP_ADDR,
    CLI_AGENT_ENABLE_ERR,
    CLI_AGENTX_ENABLE_ERR,
    CLI_AGENTX_CTXT_NAME_ERR,
    CLI_SNMP_MAX_ERR
};


enum {
    CLI_SNMP3_MOD_DISABLED_ERR = 1,
    CLI_SNMP3_ENTRY_MODIFICATION_ERR,
    CLI_SNMP3_ENTRY_CREATION_ERR,
    CLI_SNMP3_COMMUNITY_NOTPRESENT_ERR,
    CLI_SNMP3_PROXY_NOTPRESENT_ERR,
    CLI_SNMP3_MIB_PROXY_NOTPRESENT_ERR,
    CLI_SNMP3_GRP_PRESENT_INACCESS_ERR,
    CLI_SNMP3_GRP_NOT_PRESENT_ERR,
    CLI_SNMP3_READACCESS_VIEW_PRESENT_ERR,
    CLI_SNMP3_WRITEACCESS_VIEW_PRESENT_ERR,
    CLI_SNMP3_NOTIFYACCESS_VIEW_PRESENT_ERR,
    CLI_SNMP3_GRPACCESS_NOT_PRESENT_ERR,
    CLI_SNMP3_ENGID_LENGTH_ERR,
    CLI_SNMP3_VIEW_NOTPRESENT_ERR,
    CLI_SNMP3_TRGTADDR_NOTPRESENT_ERR,
    CLI_SNMP3_TRGTPARAM_NOTPRESENT_ERR,
    CLI_SNMP3_NOTIF_NOTPRESENT_ERR,
    CLI_SNMP3_USER_NOTPRESENT_ERR,
    CLI_SNMP3_USER_PROTOCOL_ERR,
    CLI_SNMP3_INVALID_INPUT_ERR,
    CLI_SNMP3_NEW_ENTRY_ERR,
    CLI_SNMP3_OID_ERR,
    CLI_SNMP3_MASK_ERR,
    CLI_SNMP3_TAGLIST_ERR,
    CLI_SNMP3_AUTH_PASWD_ERR,
    CLI_SNMP3_AUTH_PASWD_LEN_ERR,
    CLI_SNMP3_PRIV_PASWD_LEN_ERR,
    CLI_SNMP3_PASWD_SECMODEL_ERR,
    CLI_SNMP3_INVALID_SECLEVEL_ERR,
    CLI_SNMP3_SECMODEL_SECLEVEL_ERR,
    CLI_SNMP3_ENTRY_EXIST_ERR,
    CLI_SNMP3_MULTIPLE_ENTRY_ERR,
    CLI_SNMP3_AGENTX_ENABLE_ERR,
    CLI_SNMP3_MIBID_ERR,
    CLI_SNMP3_PROXY_ENTRY_MODIFICATION_ERR,
    CLI_SNMP3_PROXY_ENTRY_CREATION_ERR,
    CLI_SNMP3_FILTER_ENTRY_NOTPRESENT_ERR,
    CLI_SNMP3_ENGINE_ID_ERR,
    CLI_SNMP3_PORT_NOT_IN_RANGE_ERR,
    CLI_SNMP3_PORT_IN_USE,
    CLI_SNMP3_CONFLICT_IN_VIEW,
#ifdef FIPS_WANTED
    CLI_SNMP3_NEW_ENTRY_PRIV_LEGACY_ERR,
    CLI_SNMP3_NEW_ENTRY_AUTH_FIPS_ERR,
    CLI_SNMP3_NEW_ENTRY_PRIV_FIPS_ERR,
    CLI_SNMP3_NEW_ENTRY_AUTH_CNSA_ERR,
    CLI_SNMP3_NEW_ENTRY_PRIV_CNSA_ERR,
#endif
    CLI_SNMP3_WRTIE_ACCESS_DENIED,
    CLI_SNMP3_TRGTPARAM_SECURE_MODE_ERR,
    CLI_SNMP3_GROUP_SECURE_MODE_ERR,
    CLI_SNMP3_MAX_ERR 
};


enum {
 CLI_SNMP3_COLD_START_TRAP_ID,
 CLI_SNMP3_AUTHTRAP_STATUS_ID,
 CLI_SNMP3_DHCP6_SERVER_TRAP_ID,
 CLI_SNMP3_DHCP6_CLIENT_TRAP_ID,
 CLI_SNMP3_DHCP6_RLY_TRAP_ID,
 CLI_SNMP3_ECFM_TRAP_ID,
 CLI_SNMP3_MPLS_TRAP_ID,
 CLI_SNMP3_NO_OF_TRAPS                   
};

#ifdef __SNMP3CLI_C__

CONST CHR1  *Snmp3CliErrString [] = {
    NULL,
    "\r% SNMPv3 module is not enabled. \r\n",
    "\r% Error in entry modification \r\n",
    "\r% Error in entry creation \r\n",
    "\r% Community entry NOT present \r\n",
    "\r% Proxy entry Not Present \r\n",
    "\r% Mib Proxy entry Not Present \r\n",
    "\r% Group present in Access list cannot delete\r\n",
    "\r% Group entry NOT present \r\n",
    "\r% Read view access present cannot delete \r\n",
    "\r% Write view access present cannot delete \r\n",
    "\r% Notify view access present cannot delete \r\n",
    "\r% Group Access entry NOT present \r\n",
    "\r% Invalid EngineID length, must be atleast 7 bytes long \r\n",
    "\r% View entry NOT present \r\n",
    "\r% Target Entry does NOT exist \r\n",
    "\r% Target Param Entry does NOT exist \r\n",
    "\r% Notification Entry does NOT exist \r\n",
    "\r% User Entry does NOT exist \r\n",
    "\r% Invalid Authentication/Private Protocol \r\n",
    "\r% Invalid Input \r\n",
    "\r% Error while Creating New Entry \r\n",
    "\r% Invalid OID \r\n",
    "\r% Invalid Mask \r\n",
    "\r% Invalid TagList \r\n",
    "\r% Invalid Authentication password \r\n",
    "\r% Invalid Authentication password length \r\n",
    "\r% Invalid Encryption password length \r\n",
    "\r% Invalid password for SecurityModel \r\n",
    "\r% Invalid SecurityLevel \r\n",
    "\r% Invalid combination SecurityModel/SecurityLevel \r\n",
    "\r% Entry Already Exist \r\n",
    "\r% Same User and SecurityModel Present in Another Group \r\n",
    "\r% Disable Agentx subagent to enable SNMP agent \r\n",
    "\r% Improper MIB-ID Specified \r\n",
    "\r% Improper value specified for Modifying Proxy Entry \r\n",
    "\r% Improper value specified for Creating Proxy Entry \r\n",
    "\r% Filter Entry does NOT exist \r\n",    
    "\r% Error in the EngineID entered \r\n",
    "\r% Port No is out of Range \r\n",
    "\r% Error: Socket creation failed.The port already in use. \r\n",
    "\r% Conflicting with previous view \r\n",
#ifdef FIPS_WANTED
    "\r% Configured Encryption Algorithm is not supported in LEGACY mode\r\n",
    "\r% Configured Authentication Algorithm is not supported in FIPS mode\r\n",
    "\r% Configured Encryption Algorithm is not supported in FIPS mode\r\n",
    "\r% Configured Authentication Algorithm is not supported in CNSA mode\r\n",
    "\r% Configured Encryption Algorithm is not supported in CNSA mode\r\n",
#endif
    "\r% Write access is not allowed for V1/V2 users\r\n",
    "\r% Snmp targetparams does not allowed write access for V1/V2 users in secure mode\r\n",
    "\r% Snmp group does not allowed write access for V1/V2 users in secure mode\r\n",
    "\r\n"
};

#endif

/* Error strings */

#ifdef __SNXAGTXCLI_C__

CONST CHR1 *SnxCliErrString[] = {
    NULL,
    "\r%  Invalid Port Number \r\n",
    "\r%  Invalid IP Address \r\n",
    "\r%  Disable SNMP agent to enable Agentx subagent \r\n",
    "\r%  Disable SNMP subagent to configure the Master Address, Context information and port number\r\n",
    "\r%  Context name exceeded its maximum length\r\n",
    "\r\n"
};
#endif



INT4 cli_process_snmp3_cmd (tCliHandle CliHandle, UINT4, ...);

INT4 Snmp3CliCommunityConfig (tCliHandle CliHandle, UINT1 *pu1CommIndex,
                              UINT1 *pu1CommName, UINT1 *pu1SecName,
                              UINT1 *pu1ContextName, UINT1 *pu1Tag,
         UINT4 u4StorageType, UINT1 *);

INT4 Snmp3CliRemoveCommunity (tCliHandle CliHandle, UINT1 *pu1SecName);

INT4 Snmp3CliGroupConfig (tCliHandle CliHandle, UINT1 *pu1GroupName,
                   UINT1 *pu1SecName, INT4 i4SecModel,
                          UINT4 u4StorageType);

INT4 Snmp3CliRemoveGroup (tCliHandle CliHandle, UINT1 *pu1GroupName,
                          UINT1 *pu1SecName, INT4 i4SecModel);

INT4 Snmp3CliAccessConfig (tCliHandle CliHandle, UINT1 *pu1Group,
                    UINT1 *pu1Context, INT4 i4SecLevel, INT4 i4SecModel,
                           UINT1 *pu1ReadName, UINT1 *pu1WriteName,
                           UINT1 *pu1NotifyName, UINT4 u4StorageType);

INT4 Snmp3CliAccessDelete (tCliHandle CliHandle, UINT1 *pu1GroupName,
                           UINT1 *pu1Context, INT4 i4SecLevel,
      INT4 i4SecModel);

INT4 Snmp3CliSetEngineId (tCliHandle CliHandle, UINT1 *pu1EngineId);

INT4 Snmp3CliReSetEngineId (tCliHandle CliHandle);

INT4
Snmp3CliProxyCreate(tCliHandle CliHandle,UINT1 *pu1ProxyName, 
                          UINT4 u4ProxyType, UINT1 *pu1ContextEngineId, 
                          UINT1 *pu1TargetParamIn, UINT1 *pu1TargetOut, 
                          UINT1 *pu1ContextName, UINT4 u4StorageType 
                          );
INT4
Snmp3CliPropProxyCreate(tCliHandle CliHandle,UINT1 *pu1ProxyName, 
                          UINT4 u4ProxyType, UINT1 *pu1MibId, 
                          UINT1 *pu1TargetParamIn, UINT1 *pu1TargetOut, 
                          UINT4 u4StorageType);

INT4 Snmp3CliProxyRemove (tCliHandle CliHandle, UINT1 *pu1ProxyName);
INT4 Snmp3CliPropProxyRemove (tCliHandle CliHandle, UINT1 *pu1ProxyName);
INT4 Snmp3CliProxyCreateAndGoAndWait (tCliHandle CliHandle, UINT1 *pu1ProxyName,UINT4 u4RowStatus);
INT4 Snmp3CliShowProxy (tCliHandle CliHandle);
INT4 Snmp3CliShowPropProxy (tCliHandle CliHandle);
INT4 Snmp3CliSetProxyListenTrapPort (tCliHandle,
                                     UINT4 u4ListenTrapPort);
INT4 Snmp3CliShowProxyListenTrapPort(tCliHandle CliHandle);
INT4 Snmp3CliShowProxyAgentPort(tCliHandle CliHandle);
INT4 Snmp3CliShowAgentPort(tCliHandle CliHandle);

INT4 Snmp3CliSetTraceOption (UINT4 i4TraceInput, UINT4 i4Action);
INT4 Snmp3CliViewConfig (tCliHandle CliHandle, UINT1 *pu1ViewName,
                         UINT1 *pu1OID, UINT1 *pu1Mask, UINT4 u4MaskType,
           UINT4 u4StorageType);

INT4 Snmp3CliViewDelete (tCliHandle CliHandle, UINT1 *pu1ViewName,
                         UINT1 *pu1OID);

INT4 Snmp3CliTargetAddrConfig (tCliHandle CliHandle, UINT1 *pu1AddrName,
                               UINT1 *pIpAddr, UINT1 *pu1AddrParam,
                               UINT1 *pu1Tag, UINT4 u4TimeOut, 
                               UINT4 u4RetryCount, UINT4 u4StorageType,
                               UINT4 u4IpVersion,UINT2 u2Port);

INT4 Snmp3CliTargetAddrDelete (tCliHandle CliHandle, UINT1 *pu1AddrName);

INT4 Snmp3CliTargetParamConfig (tCliHandle CliHandle, UINT1 *pu1ParamName,
                                INT4 i4MpModel, INT4 i4SecModel,
                                UINT1 *pu1SecName, INT4 i4SecLevel,
                          UINT4 u4StorageTypei,
                                UINT1 *pu1FilterProfileName,
                                UINT4 u4FilterStorageType);
INT4
Snmp3CliNotifyFilterConfig (tCliHandle CliHandle, UINT1 *pu1FilterProfileName,
                            UINT1 *pu1OID, UINT1 *pu1Mask, UINT4 u4FilterType,
           UINT4 u4StorageType);
INT4
Snmp3CliNotifyFilterDelete (tCliHandle CliHandle, UINT1 *pu1FilterName, 
                            UINT1 *pu1OID);

INT4 Snmp3CliTargetParamDelete (tCliHandle CliHandle, UINT1 *pu1ParamName);

INT4 Snmp3CliUsmConfig (tCliHandle CliHandle, UINT1 *pu1UserName,
                        INT4 i4AuthProto, UINT1 *pu1AuthPass, 
                        INT4 i4PrivProto, UINT1 *pu1PrivPass, 
                        UINT4 u4StorageType, UINT1 *pu1EngineId , UINT4 u4PrivSet);

INT4 Snmp3CliUsmDelete (tCliHandle CliHandle, UINT1 *pu1UserName, UINT1 *pu1EngineId);

INT4 Snmp3CliNotifyConfig (tCliHandle CliHandle, UINT1 *pu1NotifyName,
                           UINT1 *pu1Tag, UINT4 u4NotifyType, 
                           UINT4 u4StorageType);

INT4 Snmp3CliNotifyDelete (tCliHandle CliHandle, UINT1 *pu1NotifyName);

INT4 Snmp3CliSetListenTrapPort (tCliHandle CliHandle, UINT2 u2ListenTrapPort);
INT4 Snmp3CliSetListenAgentPort (tCliHandle CliHandle, UINT4 u2ListenAgentPort);

INT4 Snmp3CliShowDetails (tCliHandle CliHandle);

INT4 Snmp3CliShowCommunity (tCliHandle CliHandle);

INT4 Snmp3CliShowGroup (tCliHandle CliHandle);

INT4 Snmp3CliShowAccessGroup (tCliHandle CliHandle);

INT4 Snmp3CliShowEngineId (tCliHandle CliHandle);

INT4 Snmp3CliShowViewTree (tCliHandle CliHandle);

INT4 Snmp3CliShowTargetAddr (tCliHandle CliHandle);

INT4 Snmp3CliShowTargetParam (tCliHandle CliHandle);

INT4 Snmp3CliShowUser (tCliHandle CliHandle);

INT4 Snmp3CliShowNotification (tCliHandle CliHandle);

INT4 Snmp3CliShowInformStats (tCliHandle CliHandle);

INT4 Snmpv3ShowRunningConfig(tCliHandle CliHandle);

INT1 Snmp3CliSetAuthTrapStatus (tCliHandle, UINT4);
    
VOID Snmp3DefTargetAddrConfig PROTO ((VOID));
INT4 Snmp3CliShowNotifyFilter (tCliHandle CliHandle);

INT4
Snmp3GetLinkTrapStatus (VOID);

INT4
Snmp3CliShowTraps (tCliHandle);

INT4
Snmp3CliSetTraps (tCliHandle, UINT4, UINT4);

INT4
Snmp3CliSetSystemContact (tCliHandle, UINT1*);

INT4
Snmp3CliSetSystemLocation (tCliHandle, UINT1*);

INT4 
Snmp3CliAgentEnable PROTO ((tCliHandle CliHandle));

INT4 
Snmp3CliAgentDisable PROTO ((tCliHandle CliHandle));

/* Function prototypes for SNX CLI command action routines */
INT4 cli_process_snx_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command, ...));
INT4 SnxCliActivateSnmpAgent PROTO ((tCliHandle CliHandle));
INT4 SnxCliActivateAgentxSubagent PROTO (( tCliHandle CliHandle, tSNMP_OCTET_STRING_TYPE*, UINT4, tSNMP_OCTET_STRING_TYPE*));
INT4 SnxCliDeActivateSnmpAgent PROTO ((tCliHandle CliHandle));
INT4 SnxCliDeActivateAgentxSubagent PROTO (( tCliHandle CliHandle));
INT4 SnxCliShowGlobalInfo PROTO ((tCliHandle CliHandle));
INT4 SnxCliShowStatistics PROTO ((tCliHandle CliHandle));
INT4 SnxCliShowRunningConfig PROTO ((tCliHandle CliHandle));
INT4 Snmp3CliSetTcpStatus PROTO((tCliHandle CliHandle, UINT4 u4TcpStatus));
INT4 Snmp3CliSetListenTcpPort PROTO((tCliHandle CliHandle, UINT2 u2ListenTcpPort));
INT4 Snmp3CliSetTcpTrapStatus PROTO((tCliHandle CliHandle, UINT4 u4TcpTrapStatus));
INT4 Snmp3CliSetListenTcpTrapPort PROTO((tCliHandle CliHandle, UINT2 u2ListenTrapPort));
INT4
Snmp3CliShowSnmpTcpDetails PROTO ((tCliHandle CliHandle));
INT4 
Snmp3CliGetOidFromName(tCliHandle CliHandle, CHR1 *au1Name);
INT4 
Snmp3CliGetNameFromOid(tCliHandle CliHandle, CHR1 *au1Oid);

INT4
Snmp3CliSetValueForName(tCliHandle CliHandle, CHR1 *au1Name, UINT1 *pVal, UINT1 u1Value);

INT4
Snmp3CliSetValueForOid(tCliHandle CliHandle, CHR1 *pu1Oid, UINT1 *pVal, UINT1 u1Value);

INT4
Snmp3CliGetName (tCliHandle CliHandle, CHR1 *au1Name, UINT1 u1Value);

INT4
Snmp3CliGetOid (tCliHandle CliHandle, CHR1 *pu1Oid, UINT1 u1Value);

INT4
Snmp3CliGetNextName(tCliHandle CliHandle, CHR1 *au1Name, UINT1 u1Value);

INT4
Snmp3CliGetNextOid (tCliHandle CliHandle, CHR1 *pu1Oid, UINT1 u1Value);

INT4
Snmp3CliNameWalk (tCliHandle CliHandle, CHR1 *au1Name, UINT1 u1Count, UINT1 u1Value);

INT4
Snmp3CliOidWalk(tCliHandle CliHandle, CHR1 *pu1Oid, UINT1 u1Count, UINT1 u1Value);

INT4
Snmp3CliSetTrapFilterName(tCliHandle CliHandle, CHR1 *au1Name,UINT4 u4Flag);

INT4
Snmp3CliSetTrapFilterOid(tCliHandle CliHandle, CHR1 *pu1Oid,UINT4 u4Flag);


#endif

/***************************  End of snmp3cli.h ********************************/
