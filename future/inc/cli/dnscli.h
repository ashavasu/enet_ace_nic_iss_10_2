/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dnscli.h,v 1.10 2015/07/04 13:04:32 siva Exp $
 * 
 * Description: This file contains exported definitions and functions
 *              of DNS CLI module.
 *********************************************************************/
#ifndef _DNS_CLI_H_
#define _DNS_CLI_H_

#include "lr.h"
#include "cli.h"

/* Maximum number of variable inputs to CLI parser */
#define CLI_DNS_MAX_ARGS    5

/* Command Identifiers */
enum
{
    CLI_DNS_SYS_CTRL = 1,         /* 1 */
    CLI_DNS_MOD_STAT,             /* 2 */
    CLI_DNS_NAME_SERVER,          /* 3 */
    CLI_DNS_NO_NAME_SERVER,       /* 4 */
    CLI_DNS_DOMAIN_NAME,          /* 5 */
    CLI_DNS_NO_DOMAIN_NAME,       /* 6 */
    CLI_DNS_RETRY,                /* 7 */
    CLI_DNS_NO_RETRY,             /* 8 */
    CLI_DNS_TIMEOUT,              /* 9 */
    CLI_DNS_NO_TIMEOUT,           /* 10 */
    CLI_DNS_CACHE,                /* 11 */
    CLI_DNS_NO_CACHE,             /* 12 */
    CLI_DNS_CLEAR_CACHE,          /* 13 */
    CLI_DNS_SHOW_INFO,            /* 14 */
    CLI_DNS_SHOW_CACHE,           /* 15 */
    CLI_DNS_SHOW_STATS,           /* 16 */
    CLI_DNS_SHOW_QUERY,           /* 17 */
    CLI_DNS_SHOW_NAME_SERVER,     /* 18 */
    CLI_DNS_SHOW_DOMAIN_NAME,     /* 19 */
    CLI_DNS_SHOW_RUNNING_CONFIG,  /* 20 */
    CLI_DNS_SET_TRACE,            /* 21 */
    CLI_DNS_RESET_TRACE,          /* 22 */
    CLI_DNS_SHOW_TRACE,           /* 23 */
    CLI_DNS_CACHE_TTL,            /* 24 */
    CLI_DNS_MODE,                 /* 25 */
    CLI_DNS_NO_MODE,              /* 26 */
    CLI_DNS_PREF_TYPE,            /* 27 */
    CLI_DNS_NO_PREF_TYPE,         /* 28 */
    CLI_DNS_SHOW_CACHE_DETAIL,     /* 29 */
    CLI_DNS_NO_CACHE_TTL           /* 30 */
};


enum
{
    DNS_CLI_INVALID_SYSTEM_CONTROL = 1,  /* 1 */
    DNS_CLI_SHUTDOWN,                    /* 2 */
    DNS_CLI_INVALID_MODULE_STATUS,       /* 3 */
    DNS_CLI_INVALID_RETRY_COUNT,         /* 4 */
    DNS_CLI_INVALID_TIMEOUT,             /* 5 */
    DNS_CLI_NO_NS_ENTRY,                 /* 6 */
    DNS_INVALID_NS_IP_TYPE,              /* 7 */
    DNS_INVALID_NS_IP,                   /* 8 */
    DNS_CLI_INVALID_NS_ENTRY,            /* 9 */
    DNS_CLI_INVALID_VALUE,               /* 10 */
    DNS_CLI_NO_DOMAIN_NAME_ENTRY,        /* 11 */
    DNS_CLI_INVALID_DOMAIN_NAME_ENTRY,   /* 12 */
    DNS_CLI_INVALID_CACHE_VALUE,         /* 13 */
    DNS_CLI_INVALID_CACHE_TTL,           /* 14 */
    DNS_CLI_NO_CACHE_ENTRY,              /* 15 */
    DNS_CLI_INVALID_DOMAIN_NAME,         /* 16 */
    DNS_CLI_INVALID_TRACE,               /* 17 */
    DNS_CLI_NO_MULTIPLE_DOMAINS,         /* 18 */
    DNS_CLI_NO_MULTIPLE_NAME_SERVERS,    /* 19 */
    DNS_CLI_INVALID_MODE,                /* 20 */
    DNS_CLI_INVALID_TYPE,                /* 21 */
    DNS_CLI_DUPLICATE_NS                /* 22 */
};


#ifdef _DNS_CLI_C_
 CONST CHR1 *DnsCliErrString[] = {
    NULL,
    "% Invalid System Control Value\r\n",         /* 1 */
    "% Dns Module is Shutdown \r\n",              /* 2 */
    "% Invalid module status Value\r\n",          /* 3 */
    "% Invalid Retry count value \r\n",           /* 4 */
    "% Invalid Timeout value \r\n",               /* 5 */
    "% Name Server entry does not exists \r\n",   /* 6 */
    "% Invalid Name Server IP Type\r\n",          /* 7 */
    "% Invalid DNS Server IP Address\r\n",/* 8 */
    "% Name server entry already exists \r\n",    /* 9 */
    "% Invalid value \r\n",                       /* 10 */
    "% Domain name entry does not exists \r\n",   /* 11 */
    "% Domain name entry already exists \r\n",    /* 12 */
    "% Invalid Cache status value \r\n",          /* 13 */
    "% Invalid Cache TTL value \r\n",             /* 14 */
    "% Cache entry does not exists \r\n",         /* 15 */
    "% Invalid domain name \r\n", /* 16 */
    "% Invalid Trace Value \r\n",    /* 17 */
    "% Cannot Create More Domain Names \r\n",     /* 18 */
    "% Cannot Create More Domain Name Servers \r\n",
    "% Invalid Resolver Mode Value\r\n",
    "% Invalid Preferential Type Value\r\n",
    "% Entry of Name server already exists \r\n" /*22*/
};  
#endif


/* Function prototypes used in dnscli.c */
PUBLIC UINT4 cli_process_dns_cmd (tCliHandle, UINT4, ...);
PUBLIC INT4  DnsCliSetSystemControl (tCliHandle, INT4);
PUBLIC INT4  DnsCliSetModuleStatus (tCliHandle, INT4);
PUBLIC INT4  DnsCliDefaultNameServer (tCliHandle, tSNMP_OCTET_STRING_TYPE *);
PUBLIC INT4  DnsDefaultDomainName (tCliHandle, tSNMP_OCTET_STRING_TYPE *);
PUBLIC INT4  DnsQueryRetryCount (tCliHandle, UINT4);
PUBLIC INT4  DnsQueryTimeOut (tCliHandle, UINT4);
PUBLIC INT4  DnsCliShowStats (tCliHandle, INT4);
PUBLIC INT4  DnsCliShowGlobalInfo (tCliHandle);
PUBLIC INT4  DnsCliSetCacheStatus (tCliHandle, INT4, INT4);
PUBLIC INT4  DnsCliShowCacheInfo (tCliHandle);
PUBLIC INT4  DnsCliShowDetailCacheInfo (tCliHandle);
PUBLIC INT4  DnsCliCreateNameServer (tCliHandle, UINT4, UINT4, UINT1 *);
PUBLIC INT4  DnsCliDeleteNameServer (tCliHandle, UINT4, UINT4, UINT1 *);
PUBLIC INT4  DnsCliShowNameServer (tCliHandle);
PUBLIC INT4  DnsCliCreateDomainName (tCliHandle, UINT4, UINT1 *);
PUBLIC INT4  DnsCliDeleteDomainName (tCliHandle, UINT4, UINT1 *); 
PUBLIC INT4  DnsCliShowDomainName (tCliHandle);
PUBLIC INT4  DnsShowRunningConfig (tCliHandle, UINT4);
PUBLIC INT4  DnsCliShowStatistics (tCliHandle);
PUBLIC INT4  DnsCliShowPendingQueries (tCliHandle);
PUBLIC INT4  DnsCliSetTraceOption (tCliHandle, INT4);
PUBLIC INT4  DnsCliResetTraceOption (tCliHandle, INT4);
PUBLIC INT4  DnsCliShowTraceOption (tCliHandle);
PUBLIC INT4  DnsResolverMode (tCliHandle, INT4);
PUBLIC INT4  DnsPreferentialType (tCliHandle, INT4);
#endif
