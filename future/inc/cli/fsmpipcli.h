/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpipcli.h,v 1.5 2011/10/25 10:44:45 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */

extern UINT4 FsMIFsIpOptProcEnable[12];
extern UINT4 FsMIFsIpNumMultipath[12];
extern UINT4 FsMIFsIpLoadShareEnable[12];
extern UINT4 FsMIFsIpEnablePMTUD[12];
extern UINT4 FsMIFsIpPmtuEntryAge[12];
extern UINT4 FsMIFsIpContextDebug[12];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIFsIpTraceConfigAdminStatus[12];
extern UINT4 FsMIFsIpTraceConfigMaxTTL[12];
extern UINT4 FsMIFsIpTraceConfigMinTTL[12];
extern UINT4 FsMIFsIpTraceConfigTimeout[12];
extern UINT4 FsMIFsIpTraceConfigMtu[12];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIFsIpAddrTabAdvertise[12];
extern UINT4 FsMIFsIpAddrTabPreflevel[12];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIFsIpRtrLstIface[12];
extern UINT4 FsMIFsIpRtrLstAddress[12];
extern UINT4 FsMIFsIpRtrLstPreflevel[12];
extern UINT4 FsMIFsIpRtrLstStatus[12];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIFsIpPathMtu[12];
extern UINT4 FsMIFsIpPmtuDisc[12];
extern UINT4 FsMIFsIpPmtuEntryStatus[12];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIFsIpRouteIfIndex[12];
extern UINT4 FsMIFsIpRouteType[12];
extern UINT4 FsMIFsIpRouteNextHopAS[12];
extern UINT4 FsMIFsIpRoutePreference[12];
extern UINT4 FsMIFsIpRouteStatus[12];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIFsIpifMaxReasmSize[12];
extern UINT4 FsMIFsIpifIcmpRedirectEnable[12];
extern UINT4 FsMIFsIpifDrtBcastFwdingEnable[12];
extern UINT4 FsMIFsIpifProxyArpAdminStatus[12];
extern UINT4 FsMIFsIpifLocalProxyArpAdminStatus[12];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIFsIcmpSendRedirectEnable[12];
extern UINT4 FsMIFsIcmpSendUnreachableEnable[12];
extern UINT4 FsMIFsIcmpSendEchoReplyEnable[12];
extern UINT4 FsMIFsIcmpNetMaskReplyEnable[12];
extern UINT4 FsMIFsIcmpTimeStampReplyEnable[12];
extern UINT4 FsMIFsIcmpDirectQueryEnable[12];
extern UINT4 FsMIFsIcmpDomainName[12];
extern UINT4 FsMIFsIcmpTimeToLive[12];
extern UINT4 FsMIFsIcmpSendSecurityFailuresEnable[12];
extern UINT4 FsMIFsIcmpRecvSecurityFailuresEnable[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIFsIpCidrAggAddress[12];
extern UINT4 FsMIFsIpCidrAggAddressMask[12];
extern UINT4 FsMIFsIpCidrAggStatus[12];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIFsCidrAdvertAddress[12];
extern UINT4 FsMIFsCidrAdvertAddressMask[12];
extern UINT4 FsMIFsCidrAdvertStatus[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIFsIrdpSendAdvertisementsEnable[10];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIFsIrdpIfConfIfNum[12];
extern UINT4 FsMIFsIrdpIfConfSubref[12];
extern UINT4 FsMIFsIrdpIfConfAdvertisementAddress[12];
extern UINT4 FsMIFsIrdpIfConfMaxAdvertisementInterval[12];
extern UINT4 FsMIFsIrdpIfConfMinAdvertisementInterval[12];
extern UINT4 FsMIFsIrdpIfConfAdvertisementLifetime[12];
extern UINT4 FsMIFsIrdpIfConfPerformRouterDiscovery[12];
extern UINT4 FsMIFsIrdpIfConfSolicitationAddress[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIFsRarpClientRetransmissionTimeout[10];
extern UINT4 FsMIFsRarpClientMaxRetries[10];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIFsRarpServerStatus[10];
extern UINT4 FsMIFsRarpServerTableMaxEntries[10];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIFsHardwareAddress[12];
extern UINT4 FsMIFsHardwareAddrLen[12];
extern UINT4 FsMIFsProtocolAddress[12];
extern UINT4 FsMIFsEntryStatus[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIFsIpGlobalDebug[10];
extern UINT4 FsMIFsIpProxyArpSubnetOption[10];

