/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmfwdcli.h,v 1.2 2008/10/31 13:09:30 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IpMRouteEnable[11];
extern UINT4 IpMRouteEnableCmdb[11];
extern UINT4 MfwdGlobalTrace[11];
extern UINT4 MfwdGlobalDebug[11];
extern UINT4 MfwdAvgDataRate[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIpMRouteEnable(i4SetValIpMRouteEnable)	\
	nmhSetCmn(IpMRouteEnable, 11, IpMRouteEnableSet, NULL, NULL, 0, 0, 0, "%i", i4SetValIpMRouteEnable)
#define nmhSetIpMRouteEnableCmdb(i4SetValIpMRouteEnableCmdb)	\
	nmhSetCmn(IpMRouteEnableCmdb, 11, IpMRouteEnableCmdbSet, NULL, NULL, 0, 0, 0, "%i", i4SetValIpMRouteEnableCmdb)
#define nmhSetMfwdGlobalTrace(i4SetValMfwdGlobalTrace)	\
	nmhSetCmn(MfwdGlobalTrace, 11, MfwdGlobalTraceSet, NULL, NULL, 0, 0, 0, "%i", i4SetValMfwdGlobalTrace)
#define nmhSetMfwdGlobalDebug(i4SetValMfwdGlobalDebug)	\
	nmhSetCmn(MfwdGlobalDebug, 11, MfwdGlobalDebugSet, NULL, NULL, 0, 0, 0, "%i", i4SetValMfwdGlobalDebug)
#define nmhSetMfwdAvgDataRate(i4SetValMfwdAvgDataRate)	\
	nmhSetCmn(MfwdAvgDataRate, 11, MfwdAvgDataRateSet, NULL, NULL, 0, 0, 0, "%i", i4SetValMfwdAvgDataRate)

#endif
