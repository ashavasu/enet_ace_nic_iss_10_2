#ifndef __SISPCLI_H__
#define __SISPCLI_H__

#include "cli.h"

#define SISP_MAX_ARGS             6
#define SISP_VLAN_ID_10           10
#define SISP_VLAN_ID_100          100
#define SISP_VLAN_ID_1000         1000

#define SISP_MAX_CHAR_10VLAN      9 
#define SISP_MAX_CHAR_100VLAN     6
#define SISP_MAX_CHAR_1000VLAN    4
#define SISP_MAX_CHAR_DEFAULT     3

#define SISP_SINGLE_DIGIT         1 
#define SISP_DOUBLE_DIGIT         2
#define SISP_TRIPLE_DIGIT         3
#define SISP_QUADRUPLE_DIGIT      4

enum {
    CLI_SISP_SHUT = 1,
    CLI_SISP_NOSHUT,
    CLI_SISP_PORT_ENABLE,
    CLI_SISP_PORT_DISABLE,
    CLI_SISP_MAP_LOGICALPORT,
    CLI_SISP_UNMAP_LOGICALPORT,
    CLI_SISP_SHOW,
    CLI_SISP_SHOW_VLAN_INFO,
};

enum {
    CLI_VCM_SISP_UNKNOWN_ERR = 1,
    SISP_PORT_TYPE_ERR,
    SISP_PORT_NOT_ACTIVE,
    SISP_GLOBAL_SHUT,
    SISP_NOT_ENABLE_ON_PORT,
    SISP_PORT_MAP_EXISTS,
    SISP_PORT_ALREADY_MAPPED_TO_CTX,
    SISP_PORT_ALREADY_MAPPED_TO_PORT,
    SISP_PORT_CHANNEL_ERR,
    SISP_LOGICAL_PORT_EXIST_ERR,
    SISP_PORT_CTX_SAME_ERR,
    SISP_PORT_STP_MODE_ERR,
    SISP_PORT_GVRP_MVRP_ERR,
    SISP_BRG_MODE_ERR,
    SISP_BRG_PORT_TYPE_ERR,
    SISP_MAX_ENABLED_PORTS_ERR,
    SISP_MAX_PORTS_REACHED_ERR,
    SISP_PORT_NO_CREATION,
    SISP_BRIDGE_TYPE_ERR,
    CLI_VCM_SISP_MAX_ERR
};

#ifdef __SISPCLI_C__ 
CONST CHR1  *VcmSispCliErrString [] = {
    NULL,
    "Unknown Error\r\n",
    "SISP can be enabled only on Physical and Port Channel ports\r\n",
    "Port not mapped to any context\r\n",
    "SISP can not be enabled/disabled on port when SISP is globally shutdown\r\n",
    "SISP is not enabled on this port\r\n",
    "Port is already mapped to the context through some other logical port\r\n",
    "Logical port is already mapped to some other context\r\n",
    "Logical port is already mapped to some other Physical port\r\n",
    "SISP can't be enabled on a port that is in a port channel\r\n",
    "SISP can not be disabled, Sisp logical port(s) exist(s) for this port\r\n",
    "Primary & Secondary context of an interface can't be same\r\n",
    "SISP ports can't be created on STP/RSTP enabled contexts\r\n",
    "SISP can't be enabled on port when GVRP/MVRP is enabled\r\n",
    "Bridge mode of Primary & Secondary context does not match\r\n",
    "SISP can be enabled only on PNP and P-PNP type of ports in 802.1AD bridges\r\n",
    "Max allowable SISP enabled ports reached\r\n",
    "Maximum Ports for the secondary context already reached\r\n",
    "Sisp port is either not created or not active\r\n",
    "SISP can be enabled only on Customer, Provider Edge and Core bridges\r\n",
    "\r\n"
};
#else
extern CONST CHR1  *VcmSispCliErrString;
#endif

INT4
cli_process_sisp_cmd PROTO ((tCliHandle CliHandle, UINT4, ...));

INT4
VcmSispCliSetSystemControl PROTO ((tCliHandle CliHandle, UINT1 u1Status));

INT4
VcmSispCliSetPortControl PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex, 
                                 UINT1 u1Status));

INT4
VcmSispCliAddLogicalPortMap PROTO ((tCliHandle CliHandle, UINT4 u4PhyIfIndex, 
                                    UINT4 u4ContextId, UINT4 u4LogicalIfIdx));

INT4
VcmSispCliDelLogicalPortMap PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex, 
                                    UINT4 u4ContextId, UINT4 u4LogicalIfIdx));

INT4
VcmSispShow PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex));

INT4
VcmSispPortVlanMapShow PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex));

INT4
VcmSispShowRunningConfig PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex));

INT4
VcmSispSetDebug PROTO ((UINT4 u4DbgValue, UINT1 u1Action));

#endif
