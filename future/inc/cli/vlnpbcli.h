/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: vlnpbcli.h,v 1.60 2016/06/14 12:30:51 siva Exp $
 *
 * Description: This file contains all the CLI function prototypes
 *              exported by the PB Module.
 *
***********************************************************************/
#ifndef __VLNPBCLI_H__
#define __VLNPBCLI_H__

#include "cli.h"

enum{
   CLI_PB_L2_PROTO_DOT1X=1,
   CLI_PB_L2_PROTO_LACP,
   CLI_PB_L2_PROTO_STP,
   CLI_PB_L2_PROTO_GVRP,
   CLI_PB_L2_PROTO_GMRP,
   CLI_PB_L2_PROTO_IGMP,
   CLI_PB_L2_PROTO_MVRP,
   CLI_PB_L2_PROTO_MMRP,
   CLI_PB_L2_PROTO_EOAM,
   CLI_PB_L2_PROTO_ELMI,
   CLI_PB_L2_PROTO_LLDP,
   CLI_PB_L2_PROTO_ECFM
};
enum{
    CLI_PB_MAC_LIMIT = 1,
    CLI_PB_NO_MAC_LIMIT,
    CLI_PB_SVLAN_LEARNING_ENABLE,
    CLI_PB_PORT_CVLANID,
    CLI_PB_CVLAN_STATUS,
    CLI_PB_SVLAN_CLASS,
    CLI_PB_NO_SVLAN_CLASS,
    CLI_PB_PORT_TYPE,
    CLI_PB_CLASS_TYPE,
    CLI_PB_NO_CLASS_TYPE,
    CLI_PB_PORT_MAC_LIMIT,
    CLI_PB_NO_PORT_MAC_LIMIT,
    CLI_PB_PORT_ETHERTYPE,
    CLI_PB_NO_PORT_ETHERTYPE,
    CLI_PB_PORT_ETHERTYPE_SWAP,
    CLI_PB_PORT_SVLAN_SWAP,
    CLI_PB_PORT_RELAY_ETHERTYPE,
    CLI_PB_PORT_NO_RELAY_ETHERTYPE,
    CLI_PB_PORT_RELAY_VLANID,
    CLI_PB_PORT_NO_RELAY_VLAN,
    CLI_PB_PORT_LEARNING_STATUS,
    CLI_PB_PORT_SERVICE_REGEN_PRIORITY,
    CLI_PB_PORT_NO_SERVICE_REGEN_PRIORITY,
    CLI_PB_PORT_PCP_DECODE,
    CLI_PB_PORT_PCP_ENCODE,
    CLI_PB_PORT_REQ_DROP_ENC,
    CLI_PB_PORT_PCP_SEL_ROW,
    CLI_PB_PORT_USE_DEI,
    CLI_PB_DOT1X_TUNNEL_ADDRESS,
    CLI_PB_LACP_TUNNEL_ADDRESS,
    CLI_PB_STP_TUNNEL_ADDRESS,
    CLI_PB_GVRP_TUNNEL_ADDRESS,
    CLI_PB_MVRP_TUNNEL_ADDRESS,
    CLI_PB_GMRP_TUNNEL_ADDRESS,
    CLI_PB_MMRP_TUNNEL_ADDRESS,
    CLI_PB_EOAM_TUNNEL_ADDRESS,
    CLI_PB_IGMP_TUNNEL_ADDRESS,
    CLI_PB_ECFM_TUNNEL_ADDRESS,
    CLI_PB_ELMI_TUNNEL_ADDRESS,
    CLI_PB_LLDP_TUNNEL_ADDRESS,
    CLI_PB_L2PROTOCOL_TUNNEL,
    CLI_PB_L2PROTOCOL_PEER,
    CLI_PB_L2PROTOCOL_DISCARD,
    CLI_PB_L2PROTOCOL_OVERRIDE,
    CLI_PB_SET_PEP_PVID, 
    CLI_PB_SET_PEP_ACCPT_FRAME_TYPE,
    CLI_PB_SET_PEP_INGRESS_FILTERING,
    CLI_PB_SET_PEP_DEF_USER_PRIO,
    CLI_PB_SET_PEP_COS_PRESERVATION,
    CLI_PB_SET_VLAN_SERVICE_TYPE,
    CLI_VLAN_PB_L2PROTOCOL_TUNNEL,
    CLI_VLAN_PB_L2PROTOCOL_PEER,
    CLI_VLAN_PB_L2PROTOCOL_DISCARD,
    CLI_PB_RESET_PEP_PVID, 
    CLI_PB_RESET_PEP_ACCPT_FRAME_TYPE,
    CLI_PB_RESET_PEP_INGRESS_FILTERING,
    CLI_PB_RESET_PEP_DEF_USER_PRIO,
    CLI_PB_RESET_PEP_COS_PRESERVATION,
    CLI_PB_CVID_REG_CONFIG,
    CLI_PB_CVID_REG_DESTROY,
    CLI_PB_PORT_NO_PCP_DECODE,
    CLI_PB_PORT_NO_PCP_ENCODE,
    CLI_PB_SHOW_SVLAN_INFO,
    CLI_PB_SHOW_SVLAN,
    CLI_PB_SHOW_SVLAN_MAPPING,
    CLI_PB_SHOW_ETHERTYPE_MAPPING,
    CLI_PB_SHOW_PORT_CONFIG,
    CLI_PB_MULTICAST_MAC_LIMIT,
    CLI_PB_SHOW_TUNNEL_MAC_ADDRESS,
    CLI_PB_SHOW_PEP_CONFIG,
    CLI_PB_SHOW_PORT_PCP_ENCODE,
    CLI_PB_SHOW_PORT_PCP_DECODE,
    CLI_PB_SHOW_PORT_PRIORITY_REGEN,
    CLI_PB_BRIDGE_MODE,
    CLI_PB_TUNNEL_BPDU_PRIORITY,
    CLI_PB_NO_TUNNEL_BPDU_PRIORITY,
    CLI_PB_PORT_TUNNEL_STATUS,
    CLI_PB_NO_PORT_TUNNEL_STATUS,
    CLI_PB_SHOW_DOT1Q_TUNNEL,
    CLI_PB_SHOW_L2PROTOCOL_TUNNEL,
    CLI_PB_SHOW_SERVICE_VLAN_TUNNEL,
    CLI_PB_CLR_L2_TUNNEL_COUNTERS,
    CLI_PB_SVLAN_PRIORITY_CONFIG,
#ifdef PBB_WANTED
    CLI_PB_PBB_ISID_SHOW,
#endif
    CLI_PB_SHOW_DISC_STATS,
    CLI_PB_SHOW_CVLAN_STATS,
    CLI_PB_CLEAR_CVLAN_STATS,
    CLI_PB_CUSTOMER_VLAN_STATUS,
    CLI_PB_L2PROTOCOL_DEFAULT,
    CLI_PB_CLR_L2_DISCARD_COUNTERS,
    CLI_PB_CLR_L2_COUNTERS,
    CLI_PB_CEP_UNTAG_EGRESS_STATUS,
    CLI_PB_NO_L2_TUNNEL_ADDRESS
};

enum{
    CLI_PB_SVLAN_PRIORITY_NONE,
    CLI_PB_SVLAN_PRIORITY_FIXED,
    CLI_PB_SVLAN_PRIORITY_COPY
};

enum{
    CLI_PB_SRCMAC= 1,
    CLI_PB_DSTMAC,
    CLI_PB_SRCMAC_CVLAN,
    CLI_PB_DSTMAC_CVLAN,
    CLI_PB_DSCP,
    CLI_PB_DSCP_CVLAN,
    CLI_PB_SRCIP,
    CLI_PB_DSTIP,
    CLI_PB_SRCIP_DSTIP,
    CLI_PB_DSTIP_CVLAN,
    CLI_PB_CVLAN,
    CLI_PB_PVID,
    CLI_PB_CVLAN_SHOW_STATS,
    ALLTYPE
};
     
enum{
    CLI_PB_UNKNOWN_ERR = CLI_ERR_START_ID_VLAN_PB,
    CLI_PB_VLAN_NOT_ACTIVE_ERR,
    CLI_PB_INVALID_VLANID_ERR,
    CLI_PB_L2VPN_INVALID_VLAN_ERR,
    CLI_PB_PROVIDERBRIDGE_ERR,
    CLI_PB_PROVIDEREDGEBRIDGE_ERR,
    CLI_PB_INVALID_LIMIT_ERR,
    CLI_PB_INVALID_ETHERTYPE_ERR,
    CLI_PB_CONFIG_NOT_ALLOW_ON_SISP_ERR,
    CLI_PB_SVLAN_TRANS_CNP_PNP_PCNP_ERR,
    CLI_PB_CUSTOMER_VLAN_DISABLE_ERR,
    CLI_PB_CNP_PNP_ERR,
    CLI_PB_CONF_NOT_APPLICABLE_ERR,
    CLI_PB_TUNNEL_ENABLE_ERR,
    CLI_PB_TUNNEL_DISABLE_ERR,
    CLI_PB_CONF_FAIL_ERR,
    CLI_PB_INVALID_LOCAL_VLAN_ERR,
    CLI_PB_INVALID_RELAY_VLAN_ERR,
    CLI_PB_CVID_ENTRY_ERR,
    CLI_PB_INVALID_TUNNEL_ADDRESS_ERR,
    CLI_PB_PORT_OPER_UP_ERR,
    CLI_PB_INVALID_PORT_ERR,
    CLI_PB_TUNNEL_PORT_TYPE_ERR,
    CLI_PB_TUNNEL_PROTOCOL_ENA_ERR,
    CLI_PB_NO_PEP_ERR,
    CLI_PB_1AD_BRIDGE_ERR,
    CLI_PB_UNTAGPEP_ERR,
    CLI_PB_TUNNEL_PEP_ERR,
    CLI_PB_INVALID_FDBID_ERR,
    CLI_PB_INVALID_TUNNEL_PORT_ERR,
    CLI_PB_IGMP_TUNNEL_ERROR,
    CLI_PB_DOT1X_LAGG_ERR,
    CLI_PB_LACP_LAGG_ERR,
    CLI_PB_EOAM_LAGG_ERR,
    CLI_PB_LLDP_LAGG_ERR,
    CLI_PB_GVRP_PEER_ERROR,
    CLI_PB_MVRP_PEER_ERROR,
    CLI_PB_GMRP_PEER_ERROR,
    CLI_PB_MMRP_PEER_ERROR,
    CLI_PB_IGMP_PEER_ERROR,
    CLI_PB_STP_PEER_ERROR,   
    CLI_PB_DOT1X_ADDR_ALREADY_USE_ERR,
    CLI_PB_LACP_ADDR_ALREADY_USE_ERR,
    CLI_PB_STP_ADDR_ALREADY_USE_ERR,
    CLI_PB_GVRP_ADDR_ALREADY_USE_ERR,
    CLI_PB_MVRP_ADDR_ALREADY_USE_ERR,
    CLI_PB_GMRP_ADDR_ALREADY_USE_ERR,
    CLI_PB_MMRP_ADDR_ALREADY_USE_ERR,
    CLI_PB_CEP_PCP_ERR,
    CLI_PB_CEP_PCP_SEL_ERR,
    CLI_PB_CEP_USEDEI_ERR,
    CLI_PB_CEP_REQDROP_ENC_ERR,
    CLI_PB_INCONSISTENT_BRG_MODE_ERR,
    CLI_PB_INCONSISTENT_PORT_TYPE_ERR,
    CLI_PB_INCONSISTENT_FRAME_TYPE_ERROR,
    CLI_PB_INCONSISTENT_PNAC_AUTH_STATE_ERR,
    CLI_PB_BRIDGE_MODE_DEP_ERR,   
    CLI_PB_BRIDGE_MODE_SHUT_ERR,
    CLI_PB_INVALID_PORT_STATUS_ERR,
    CLI_PB_DOT1X_TUNNEL_ERR,
    CLI_PB_LACP_TUNNEL_ERR,
    CLI_PB_DOT1X_PEER_ERR,
    CLI_PB_LACP_PEER_ERR,
    CLI_PB_DOT1X_NETWORK_PORT_ERR,
    CLI_PB_LACP_NETWORK_PORT_ERR,
    CLI_PB_STP_NETWORK_PORT_ERR,
    CLI_PB_GVRP_NETWORK_PORT_ERR,
    CLI_PB_GMRP_NETWORK_PORT_ERR,
    CLI_PB_MVRP_NETWORK_PORT_ERR,
    CLI_PB_MMRP_NETWORK_PORT_ERR,
    CLI_PB_IGMP_NETWORK_PORT_ERR,
    CLI_PB_ELMI_NETWORK_PORT_ERR,
    CLI_PB_LLDP_NETWORK_PORT_ERR,
    CLI_PB_ECFM_NETWORK_PORT_ERR,
    CLI_PB_EOAM_NETWORK_PORT_ERR,
    CLI_PB_DOT1X_PORT_TYPE_ERR,
    CLI_PB_LACP_PORT_TYPE_ERR,
    CLI_PB_STP_PORT_MODE_ERR,
    CLI_PB_GMRP_PORT_MODE_ERR,
    CLI_PB_COS_PRESERVATION_UNTAGPEP_ERR,
    CLI_PB_UNTAGPEP_COS_PRESERVATION_ERR,
    CLI_PB_SERVICE_TYPE_CONF_ERR,
    CLI_PB_SVLAN_TRANS_PIP_CBP_ERR,
    CLI_PB_SISP_VLAN_TRANS_ERR,
    CLI_PB_MRP_SHUT_ERR,
    CLI_PB_GARP_SHUT_ERR,
    CLI_PB_BUNDLE_STATUS_CONFIG_ERR,
    CLI_PB_MULTIPLEX_STATUS_CONFIG_ERR,
    CLI_PB_BUNDLE_MULTIPLEX_CEP_PORT_ERR,
    CLI_PB_BUNDLE_DISABLE,
    CLI_PB_MULTIPLEX_DISABLE,
    CLI_PB_MULTICAST_MAC_MAX_ERR,
    CLI_PB_EOAM_PEER_ERROR,
    CLI_PB_CNP_CONFIG,
    CLI_PB_INVALID_SVLAN_PRI_TYPE,
    CLI_PB_CVID_REG_ERR,
    CLI_PB_VLAN_MAPNG_NOT_EXIST_ERR,
    CLI_PB_VLAN_MAPNG_EXIST_ERR,
    CLI_PB_VLAN_MAX_ENTRIES_ERR ,
    CLI_PB_VLAN_NOT_IN_SERVICE_ERR,
    CLI_PB_MAX_CVLAN_ERR,
    CLI_PB_ELMI_ADDR_ALREADY_USE_ERR,
    CLI_PB_LLDP_ADDR_ALREADY_USE_ERR,
    CLI_PB_ECFM_ADDR_ALREADY_USE_ERR,
    CLI_PB_EOAM_ADDR_ALREADY_USE_ERR,
    CLI_PB_ICCH_VLAN_ERR,
    CLI_PB_CEP_UNTAG_EGRESS_CONF_ERROR,
    CLI_PB_MAX_ERR
};

/* Accessing the ErrString with ErrCode value.
 * Since the ErrCode's starting Id is changed, this forumla is used
 * to access the Err String */
#define CLI_ERR_OFFSET_VLAN_PB(u4ErrCode) \
        (u4ErrCode - CLI_ERR_START_ID_VLAN_PB + 1)

#ifdef __VLNPBCLI_C__
CONST CHR1  *PbCliErrString [] = {
    NULL,
    "Unknown Error\r\n",
    "Vlan is NOT active\r\n",
    "Invalid Vlan Id\r\n",
    "Vlan Mapped to a Vpls Instance cannot be configured as E-line \r\n",
    "Bridge is not in provider bridge mode\r\n",
    "Bridge is not in provider edge bridge mode\r\n",
    "Value Out of Range\r\n",
    "Invalid Ether Type\r\n",
    "Configuration not allowed on SISP port \r\n",
    "Service vlan translation is applicable only on customerNetworkPort, providerNetworkPort and propCustomerNetworkPort\r\n",
    "Customer Vlan can not be disabled on a customerEdgePort\r\n",
    "Configuration is not applicable for customerNetworkPort and ProviderNetworkPort\r\n",
    "Configuration is applicable only on propCustomerEdgePort and propCustomerNetworkPort\r\n",
    "Tunneling is enable on this port.\r\n",
    "Tunneling is disable on this port.\r\n",
    "Configuration failed\r\n",
    "Local Vlan id is not valid.\r\n",
    "Relay Vlan id is not valid.\r\n",
    "C-VID Registration entries/C-VLAN statistics configuration can be applied only for CustomerEdgePort.\r\n",
    "Invalid tunnel address \r\n",
    "Port must be down to change the port type \r\n",
    "Invalid port number \r\n",
    "Tunneling can-not be set on PNPs, PPNPs, CNP-Stagged \r\n",
    "Protocol status must be disabled to set the tunnel status \r\n",
    "Logical port (PEP/CNP) is not created in the system for this port and S-VID\r\n",
    "Bridge is not provider core bridge or provider edge bridge. \r\n",
    "UntagggedPep is already set for one C-VLAN mapped to this service instance.  \r\n",
    "Tunneling can be enabled on CEP only when there is only one PEP associated with CEP and the S-VLAN service type is E-Line \r\n",
    "Invalid FID.\r\n",
    "Protocol tunnel status can-not be set for PNPs, PPNPs CNP(Stagged) and non-tunnel interfaces \r\n",
    " For tunneling the igmp packets, igmp module should be disabled \r\n",
    " Dot1x tunnel status can-not be set on port channel interfaces \r\n",
    " LACP tunnel status can-not be set on port channel interfaces \r\n",
    " EOAM tunnel status can-not be set on port channel interfaces \r\n",
    " LLDP tunnel status can-not be set on port channel interfaces \r\n",
    " Peering option is not supported for GVRP\r\n",
     " Peering option is not supported for MVRP\r\n",
    " Peering option is not supported for GMRP\r\n",
     " Peering option is not supported for MMRP\r\n",
    " Peering option is not supported for IGMP\r\n",
    " STP peering can be enabled only on Customer Edge Ports \r\n",
    " This address is in use for one of the other L2 protocols  tunneling \r\n",
    " This address is in use for one of the other L2 protocols  tunneling \r\n",
    " This address is in use for one of the other L2 protocols  tunneling \r\n",
    " This address is in use for one of the other L2 protocols  tunneling \r\n",
     " This address is in use for one of the other L2 protocols tunneling \r\n",
    " This address is in use for one of the other L2 protocols  tunneling \r\n",      
    " This address is in use for one of the other L2 protocols  tunneling \r\n",
    " PCP decoding/encoding configuration is not allowed for CEP/PCEP ports. \r\n",
    " 8P0D alone is supported as PCP selection row for CEP/PCEP ports. \r\n",
    " UseDei can be set to only False on CEP/PCEP ports. \r\n",
    " Require drop encoding can be set to only False on CEP/PCEP ports. \r\n",
    "The bridge is in customer bridge mode. \r\n",
    "Cannot enable tunnelling on non-access ports. \r\n",
    " Tunneling can be enabled only if port is set to Accept All Frames\r\n",
    "Dot1x port state is not in force-authorised state. Cannot enable tunnelling. \r\n",
    "Spanning Tree,GARP,ECFM,ELPS,ERPS,MRP must be shutdown and IGMP Snooping must be disabled globally \r\n  PBB must be started globally to change the Bridge mode to one of the PBB Bridge modes I-Component or B-Component.\r\n",
    "Provider backbone bridge is not configured.\r\n",
    "Port not created\r\n",
    "Dot1x Protocol tunnel cannot be set as tunnel when tunnelling is disabled on the port.\r\n",
    "Lacp Protocol tunnel cannot be set as tunnel when tunnelling is disabled on the port.\r\n",
    "Dot1x Protocol tunnel cannot be set as peer when tunnelling is enabled on the port.\r\n",
    "Lacp Protocol tunnel cannot be set as peer when tunnelling is enabled on the port.\r\n",
    "Dot1x protocol tunnel cannot be set on a network port with bridge mode as 802.1AD bridge.\r\n",
    "Lacp protocol tunnel cannot be set on a network port with bridge mode as 802.1AD bridge.\r\n",
    "Stp protocol tunnel cannot be set on a network port with bridge mode as 802.1AD bridge.\r\n",
    "Gvrp protocol tunnel cannot be set on a network port with bridge mode as 802.1AD bridge.\r\n",
    "Gmrp protocol tunnel cannot be set on a network port with bridge mode as 802.1AD bridge.\r\n",
    "Mvrp protocol tunnel cannot be set on a network port with bridge mode as 802.1AD bridge.\r\n",
    "Mmrp protocol tunnel cannot be set on a network port with bridge mode as 802.1AD bridge.\r\n",
    "Igmp protocol tunnel cannot be set on a network port with bridge mode as 802.1AD bridge.\r\n",
    "Elmi protocol tunnel cannot be set on a network port with bridge mode as 802.1AD bridge.\r\n",
    "Lldp protocol tunnel cannot be set on a network port with bridge mode as 802.1AD bridge.\r\n",
    "Ecfm protocol tunnel cannot be set on a network port with bridge mode as 802.1AD bridge.\r\n",
    "Eoam protocol tunnel cannot be set on a network port with bridge mode as 802.1AD bridge.\r\n",
    "Dot1x protocol tunnel cannot be set on non tunnel ports.\r\n",
    "Lacp protocol tunnel cannot be set on non tunnel ports.\r\n",
    "Stp must be disabled for configuring a port as tunnel port \r\n",
    "Gmrp must be disabled for configuring a port as tunnel port \r\n",
    "Untagged-PEP and Untagged-CEP must be false to enable COS preservation for this PEP.\r\n",
    "COS Preservation for this PEP must be disabled to configure Untagged-PEP/ Untagged-CEP as true.\r\n",
    "Configuration failed - VLAN with service type as e-line cannot take more than 2 ports. \r\n",
    "Service vlan translation is not applicable only on customerBackbonePort and providerInstancePort \r\n",
    "VLAN translation config failed - Some SISP enabled/SISP Port is configured in this vlan",
    "MRP System Shutdown \r\n",
    "GARP System Shutdown \r\n",
    "Bundling Status cannot be change, remove first CVlan registration entry corrosponding to this port.\r\n",
    "Multiplexing Status cannot be change, remove first SVlan registration entry corrosponding to this port..\r\n",
    "Bundling/Multiplexing can be enable/disable only on CEP Ports.\r\n",
    "Bundling is disable on this port,cannot map multiple CVlan to SVlan on this Port.\r\n",
    "Multiplexing is disable on this port,cannot map multiple SVlan on this Port. \r\n",
    "Multicast MAC maximum value reached. \r\n",
    " Peering option is not supported for EOAM\r\n",
    "Configuration only applicable for CNP S-Tagged and CNP PortBased Ports\r\n",
    "Invalid SVLAN Priority Type\r\n",
    "CVID Registration Table entry does not exist\r\n", 
    "VLAN mapping does not exist\r\n",
    "VLAN mapping already exists\r\n",
    "Reached Maximum number of entries\r\n",
    "Vlan not in service\r\n",
    "Reached maximum number of CVID registration entries\r\n",
    " This address is in use for one of the other L2 protocols  tunneling \r\n",
    " This address is in use for one of the other L2 protocols  tunneling \r\n",
    " This address is in use for one of the other L2 protocols  tunneling \r\n",
    " This address is in use for one of the other L2 protocols  tunneling \r\n",
    "SVLAN translation blocked on ICCH Vlan\r\n",
    " Configuration to allow/deny outgoing untagged frames is applicable only for CEP ports\r\n",
    "\r\n"
};
#else
extern CONST CHR1  *PbCliErrString [];
#endif

#define CLI_PB_ENABLED          1
#define CLI_PB_DISABLED         2

#define CLI_PB_INGRESS          1
#define CLI_PB_EGRESS           2

#define CLI_ADD                 1
#define CLI_DELETE              2

#define PB_DEFAULT_MULTICAST_MAC_LIMIT VLAN_DEV_MAX_MCAST_TABLE_SIZE
#define PB_DEFAULT_PORT_MAC_LIMIT      VLAN_PB_DYNAMIC_UNICAST_SIZE
#define PB_DEFAULT_PORT_ETHERTYPE      VLAN_PROVIDER_PROTOCOL_ID

#define VLAN_CLI_INVALID               0

INT4 cli_process_pb_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command, ...));
INT4 cli_process_pb_show_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command, ...));
INT4 VlanPbCliSelectContextOnMode PROTO ((tCliHandle CliHandle, UINT4 u4Cmd,
                                          UINT4 *pu4Context,
                                          UINT2 *pu2LocalPort));
INT4 VlanPbSetMulticastMacLimit PROTO ((tCliHandle, UINT4));
INT4 VlanPbSetCustomerVlanId PROTO ((tCliHandle, INT4, UINT4));
INT4 VlanPbSetCEPPortEgressUntaggedStatus PROTO ((tCliHandle CliHandle, INT4 i4PortId, 
                                                  UINT4 u4Status));
INT4 VlanPbSetProviderBridgePortType PROTO ((tCliHandle, INT4, UINT4, INT4));
INT4 VlanPbSetPortUnicastLimit PROTO ((tCliHandle, INT4, UINT4));
INT4 VlanPbSetPortEtherType PROTO ((tCliHandle, INT4, UINT4, UINT4));
INT4 VlanPbPortEtherTypeSwapStatus PROTO ((tCliHandle, INT4, UINT4));
INT4 VlanPbPortSVlanSwapStatus PROTO ((tCliHandle, INT4, UINT4));
INT4 VlanPbPortRelayEthertype PROTO ((tCliHandle, INT4, UINT4, UINT4));
INT4 VlanPbPortRelayVlanID PROTO ((tCliHandle, INT4, UINT4, UINT4));
INT4 VlanPbShowSVlanInformation PROTO ((tCliHandle, UINT4));
INT4 VlanPbSVlanClassSrcMacBase  PROTO ((tCliHandle, INT4, UINT4, tMacAddr, UINT4));
INT4 VlanPbSVlanClassSrcMacCVlanBase  PROTO ((tCliHandle, INT4, UINT4, UINT4,
                                              tMacAddr, UINT4));

INT4 VlanPbSVlanClassDstMacBase  PROTO ((tCliHandle, INT4, UINT4, tMacAddr, 
                                         UINT4));

INT4 VlanPbSVlanClassDstMacCVlanBase  PROTO ((tCliHandle, INT4, UINT4, UINT4,
                                              tMacAddr, UINT4));

INT4 VlanPbSVlanClassDSCPBase  PROTO ((tCliHandle, INT4, UINT4, UINT4,
                                       UINT4));

INT4 VlanPbSVlanClassDSCPCVlanBase  PROTO ((tCliHandle, INT4, UINT4, UINT4,
                                            UINT4, UINT4));

INT4 VlanPbSVlanClassSrcIPBase  PROTO ((tCliHandle, INT4, UINT4, UINT4,
                                        UINT4));

INT4 VlanPbSVlanClassSrcIPDstIPBase  PROTO ((tCliHandle, INT4, UINT4, UINT4,
                                             UINT4, UINT4));

INT4 VlanPbSVlanClassDstIPBase  PROTO ((tCliHandle, INT4, UINT4, UINT4,
                                        UINT4));

INT4 VlanPbSVlanClassDstIPCVlanBase  PROTO ((tCliHandle, INT4, UINT4, UINT4,
                                               UINT4, UINT4));

INT4 VlanPbSVlanClassCVlanBase  PROTO ((tCliHandle, INT4, UINT4, UINT4,
                                        UINT4));

INT4 VlanPbDeletePortRelayEthertype  PROTO ((tCliHandle, INT4, UINT4));
INT4 VlanPbDeletePortRelayVlanID  PROTO ((tCliHandle, INT4, UINT4));
INT4 VlanPbSetPortUnicastLearningStatus PROTO ((tCliHandle, INT4, UINT4));
INT4 VlanPbShowRunningConfigScalars  PROTO ((tCliHandle, UINT4, UINT1 *));
INT4
VlanPbShowRunningConfigInterfaceDetails PROTO ((tCliHandle CliHandle, INT4 i4LocalPort,
                                         UINT1 u1PbPortType, UINT1 *pu1HeadFlag));
INT4
VlanPbCliShowL2ProtocolServiceVlan (tCliHandle CliHandle, UINT4 u4ContextId, UINT1 u1Summary, tVlanId VlanId);
INT4
VlanPbCliShowL2PrtclServiceVlanTunnelSummary (tCliHandle CliHandle, UINT4 u4ContextId, UINT1 u1Summary);
INT4 VlanPbSetClassType  PROTO ((tCliHandle, INT4, UINT4));
INT4 VlanPbSetPortCustomerVlanStatus PROTO ((tCliHandle, INT4, UINT4));
INT4 VlanPbCheckSystemStatus PROTO ((tCliHandle CliHandle, UINT4 u4ContextId));  
VOID VlanPbShowSrcMacSVlanConf PROTO ((tCliHandle, UINT4, UINT4 *));
VOID VlanPbShowDstMacSVlanConf PROTO ((tCliHandle, UINT4, UINT4 *));
VOID VlanPbShowCVlanSrcMacSVlanConf PROTO ((tCliHandle, UINT4, UINT4 *));
VOID VlanPbShowCVlanDstMacSVlanConf PROTO ((tCliHandle, UINT4, UINT4 *));
VOID VlanPbShowDscpSVlanConf PROTO ((tCliHandle, UINT4, UINT4 *));
VOID VlanPbShowCVlanDscpSVlanConf PROTO ((tCliHandle, UINT4, UINT4 *));
VOID VlanPbShowSrcIPSVlanConf PROTO ((tCliHandle, UINT4, UINT4 *));
VOID VlanPbShowDstIPSVlanConf PROTO ((tCliHandle, UINT4, UINT4 *));
VOID VlanPbShowSrcDstIPSVlanConf PROTO ((tCliHandle, UINT4, UINT4 *));
VOID VlanPbShowCVlanDstIPSVlanConf PROTO ((tCliHandle, UINT4, UINT4 *));
VOID VlanPbShowCVlanSVlanConf PROTO ((tCliHandle, UINT4, UINT4 *));
VOID VlanPbShowPvidSVlanConf PROTO ((tCliHandle, UINT4, UINT4 *));
INT4 VlanPbShowSVlanConf PROTO ((tCliHandle, UINT4, UINT4));
INT4 VlanPbShowSVlanMapping PROTO ((tCliHandle, UINT4, INT4));
INT4 VlanPbShowEtherTypeMapping PROTO ((tCliHandle, UINT4, INT4));
INT4 VlanPbShowProviderBridgePortConfig PROTO ((tCliHandle, INT4, UINT4));
INT4 VlanPbShowMulticastLimit PROTO ((tCliHandle, UINT4));
VOID VlanPbShowRunningSrcMacSVlanTable PROTO ((tCliHandle, INT4, UINT4 *, UINT1 *));
VOID VlanPbShowRunningDstMacSVlanTable PROTO ((tCliHandle, INT4, UINT4 *, UINT1 *));
VOID VlanPbShowRunningCVlanSrcMacSVlanTable PROTO ((tCliHandle, INT4, UINT4 *, UINT1 *));
VOID VlanPbShowRunningCVlanDstMacSVlanTable PROTO ((tCliHandle, INT4, UINT4 *, UINT1 *));
VOID VlanPbShowRunningDscpSVlanTable PROTO ((tCliHandle, INT4, UINT4 *, UINT1 *));
VOID VlanPbShowRunningCVlanDscpSVlanTable PROTO ((tCliHandle, INT4, UINT4 *, UINT1 *));
VOID VlanPbShowRunningSrcIPSVlanTable PROTO ((tCliHandle, INT4, UINT4 *, UINT1 *));
VOID VlanPbShowRunningDstIPSVlanTable PROTO ((tCliHandle, INT4, UINT4 *, UINT1 *));
VOID VlanPbShowRunningSrcDstIPSVlanTable PROTO ((tCliHandle, INT4, UINT4 *, UINT1 *));
VOID VlanPbShowRunningCVlanDstIPSVlanTable PROTO ((tCliHandle, INT4, UINT4 *, UINT1 *));
VOID VlanPbShowRunningCVidSVlanTable PROTO ((tCliHandle, INT4, UINT4 *, UINT1 *));
VOID VlanPbShowRunningEtherTypeSwapTable PROTO ((tCliHandle, INT4, UINT4 *, UINT1 *));
VOID VlanPbShowRunningVIDTranslationTable PROTO ((tCliHandle, INT4, UINT4 *, UINT1 *));
INT4 VlanPbShowFidLimit PROTO ((tCliHandle CliHandle, UINT4 u4Fid, 
                                UINT4 u4ContextId));
INT4 VlanPbSetPcpDecodingTable PROTO((tCliHandle CliHandle, INT4 i4PortId, 
                                INT4 i4SelRow, INT4 i4Pcp, INT4 i4Priority, INT4 i4Dei));
INT4 VlanPbSetPcpEncodingTable PROTO((tCliHandle CliHandle, INT4 i4PortId, 
                                INT4 i4SelRow, INT4 i4Priority, INT4 i4Pcp, INT4 i4Dei));
INT4 VlanPbSetDot1xTunnelAddress PROTO((tCliHandle CliHandle, 
                                        UINT1 *pu1Dot1xAddress));
INT4 VlanPbSetLacpTunnelAddress PROTO((tCliHandle CliHandle, 
                                       UINT1 *pu1LacpAddress));
INT4 VlanPbSetStpTunnelAddress PROTO((tCliHandle CliHandle, 
                                      UINT1 *pu1StpAddress));
INT4 VlanPbSetGvrpTunnelAddress PROTO((tCliHandle CliHandle, 
                                       UINT1 *pu1GvrpAddress));
INT4 VlanPbSetMvrpTunnelAddress PROTO((tCliHandle CliHandle,
                                       UINT1 *pu1MvrpAddress));
INT4 VlanPbSetGmrpTunnelAddress PROTO((tCliHandle CliHandle, 
                                       UINT1 *pu1GmrpAddress));
INT4 VlanPbSetMmrpTunnelAddress PROTO((tCliHandle CliHandle,
                                       UINT1 *pu1MmrpAddress));
INT4 VlanPbSetEoamTunnelAddress PROTO((tCliHandle CliHandle,
                                       UINT1 *pu1EoamAddress));
INT4 VlanPbSetIgmpTunnelAddress PROTO((tCliHandle CliHandle,
                                       UINT1 *pu1IgmpAddress));
INT4 VlanPbSetEcfmTunnelAddress PROTO((tCliHandle CliHandle,
                                       UINT1 *pu1EcfmAddress));
INT4 VlanPbSetElmiTunnelAddress PROTO((tCliHandle CliHandle,
                                       UINT1 *pu1ElmiAddress));
INT4 VlanPbSetLldpTunnelAddress PROTO((tCliHandle CliHandle,
                                       UINT1 *pu1LldpAddress));
INT4 VlanPbSetPbL2ProtocolTunnel PROTO((tCliHandle CliHandle, INT4 i4PortId,
                                        UINT4 u4Type));
INT4 VlanPbSetPbL2ProtocolPeer PROTO((tCliHandle CliHandle, INT4 i4PortId,
                                      UINT4 u4Type));
INT4 VlanPbSetPbL2ProtocolDiscard PROTO((tCliHandle CliHandle, INT4 i4PortId,
                                         UINT4 u4Type));
INT4 VlanPbSetPbL2ProtocolDefault PROTO((tCliHandle CliHandle, INT4 i4PortId,
                                         UINT4 u4Type));
INT4
VlanPbSetPbL2ProtocolOverride PROTO((tCliHandle CliHandle, INT4 i4PortId, 
     UINT4 u4Type));
INT4 VlanPbSetPepPvid PROTO((tCliHandle CliHandle, INT4 i4PortId, INT4 i4SVlanId,
                             INT4 i4Pvid));
INT4 VlanPbSetPepAccpFrameType PROTO((tCliHandle CliHandle, INT4 i4PortId,
                                      tVlanId SVlanId, INT4 i4AccptFrameType));
INT4 VlanPbSetPepDefUserPriority PROTO((tCliHandle CliHandle, INT4 i4PortId,
                                        tVlanId SVlanId, INT4 i4DefUserPrio));
INT4 VlanPbSetPepIngressFiltering PROTO((tCliHandle CliHandle, INT4 i4PortId,
                                         tVlanId SVlanId, INT4 i4IngFiltering));
INT4 VlanPbSetPepCosPreservation PROTO ((tCliHandle CliHandle, INT4 i4PortId,
                                         tVlanId SVlanId, 
                                         INT4 i4CosPreservation));
INT4 VlanPbSetCNPServiceRegenPriority PROTO((tCliHandle CliHandle, INT4 i4PortId,
                        UINT4 u4VlanId, INT4 i4RecvPriority, INT4
                        i4RegenPriority));
VOID VlanPbPrintProtocolTunnelStatus PROTO((tCliHandle CliHandle, INT4 i4TunnelStatus, 
                                      INT4 i4Protocol));
VOID VlanPbDisplayProtoTunnelStatus PROTO((tCliHandle CliHandle,
                                           UINT1 u1BrgPortType,
                                           UINT4 u4BridgeMode,
                                           INT4 i4TunnelStatus,
        UINT1 *pu1HeadFlag,
                                           INT4 i4Protocol, 
        INT4 i4LocalPort));
UINT4 VlanPbShowTunnelMacAddress PROTO((tCliHandle CliHandle, 
                                        UINT4 u4ContextId));
INT4
VlanPbShowPortTablesInContext PROTO ((tCliHandle CliHandle, UINT4 u4Command,
                                      UINT4 u4ContextId, INT4 i4PortId));
INT4
VlanPbShowPortTables PROTO ((tCliHandle CliHandle, UINT4 u4Command,
                             UINT4 u4ContextId, UINT4 u4IfIndex));
INT4
VlanPbPrintTableHeading (tCliHandle CliHandle, UINT4 u4Command);
INT4 VlanPbShowPepConfiguration PROTO((tCliHandle CliHandle, INT4 i4PortId,
                                       UINT4 *pu4PagingStatus));
INT4 VlanPbShowPcpEncodingTable PROTO((tCliHandle CliHandle, INT4 i4PortId));
INT4 VlanPbShowPcpDecodingTable PROTO((tCliHandle CliHandle, INT4 i4PortId));
INT4 VlanPbShowPriorityRegenTable PROTO((tCliHandle CliHandle, INT4 i4PortId));
VOID
VlanPbShowRunningPepConfigTable (tCliHandle CliHandle, INT4 i4LocalPort,
                                 UINT4 *pu4PagingStatus, UINT1 *pu1HeadFlag);

INT4 DisplayPbServiceVlanInfo (tCliHandle CliHandle, UINT4 u4ContextId, 
          UINT4 u4VlanId, UINT1 *pu1isActiveVlan);
VOID
VlanPbShowRunningConfigPerVlanTable (tCliHandle CliHandle,
                                     UINT4 u4CurrentContextId,
                                     UINT4 i4CurrentVlanId, UINT1 *pu1HeadFlag);

VOID VlanPbShowRunningPcpDecodingTable (tCliHandle CliHandle, INT4 i4LocalPort,
                                        UINT4 *pu4PagingStatus, UINT1 *pu1HeadFlag);

VOID VlanPbShowRunningServicePrioRegenTable(tCliHandle CliHandle, INT4 i4LocalPort,
                                        UINT4 *pu4PagingStatus, UINT1 *pu1HeadFlag);
INT4 VlanPbSetVlanServiceType (tCliHandle CliHandle, 
                               tVlanId VlanId, INT4 i4ServiceType);
VOID VlanPbShowRunningPcpEncodingTable(tCliHandle CliHandle, INT4 i4LocalPort,
                                       UINT4 *pu4PagingStatus, UINT1 *pu1HeadFlag);
INT4 VlanPbSetPortUseDei (tCliHandle CliHandle, INT4 i4PortId, INT4 i4UseDei);
INT4 VlanPbConfSVlanPriority ( tCliHandle CliHandle, INT4 i4PortId,
                               UINT4 u4CVlanId, UINT4 u4PriorityType, UINT4 u4Priority);
INT4 VlanPbConfCVIDSVlanPriority ( tCliHandle CliHandle, INT4 i4PortId,
                                   UINT4 u4CVlanId, UINT4 u4PriorityType, UINT4 u4Priority);
INT4 VlanPbConfPortSVlanPriority ( tCliHandle CliHandle, INT4 i4PortId,
                                   UINT4 u4PriorityType, UINT4 u4Priority);

INT4 VlanPbSetPortReqDropEncoding (tCliHandle CliHandle, INT4 i4PortId, 
                                   INT4 i4ReqDropEncoding);

INT4
VlanPbConfCvidRegEntry (tCliHandle CliHandle, INT4 i4PortId,
                        UINT4 u4CVlanId, UINT4 u4SVlanId,
                        UINT4 u4UntagPep, UINT4 u4UntagCep, INT4 u4RelayCVlanId);

INT4
VlanPbDelCvidRegEntry (tCliHandle CliHandle, INT4 i4PortId,
                       UINT4 u4CVlanId);
INT4
VlanPbResetPcpDecodingTable (tCliHandle CliHandle, INT4 i4PortId, INT4 i4SelRow);

INT4
VlanPbResetPcpEncodingTable (tCliHandle CliHandle, INT4 i4PortId, INT4 i4SelRow);

INT4 VlanPbSetPortPcpSelRow (tCliHandle CliHandle, INT4 i4PortId,
                              INT4 i4PcpSelRow);
INT4
VlanPbCliShowL2ProtocolTunnel (tCliHandle CliHandle, UINT4 u4ContextId,
                               UINT4 u4Port, UINT1 u1Summary);

INT4
VlanPbCliShowL2PrtclTunnelSummary (tCliHandle CliHandle, UINT4 u4ContextId);

INT4
VlanPbSetBridgeMode (tCliHandle CliHandle, UINT4 u4BridgeMode);

INT4
VlanPbSetTunnelBpduPriority (tCliHandle CliHandle, INT4 i4VlanTunnelBpduPri);

INT4
VlanPbSetPortTunnelStatus (tCliHandle CliHandle, UINT4 u4Port, UINT1 u1Status);

INT4
VlanPbCliShowDot1qTunnelTable (tCliHandle CliHandle, UINT4 u4ContextId,
                               UINT4 u4Port);

INT4
VlanMICliShowL2ProtocolTunnel (tCliHandle CliHandle, UINT4 u4ContextId,
                               UINT4 u4Port, UINT1 u1Summary);

INT4
VlanPbClearL2TunnelCounters (tCliHandle CliHandle, UINT4 u4Port, tVlanId VlanId);

INT4
VlanPbClearL2DiscardCounters (tCliHandle CliHandle, UINT4 u4Port, tVlanId VlanId);

INT4
VlanMICliShowL2PrtclTunnelSummary (tCliHandle CliHandle, UINT4 u4ContextId);

INT4
VlanPbCliShowL2ProtocolDiscard (tCliHandle CliHandle, UINT4 u4ContextId, tVlanId VlanId);

INT4 VlanPbCliShowDiscardStats (tCliHandle CliHandle, UINT4 u4Port,
                                UINT4 u4ContextId, tVlanId VlanId);

VOID 
VlanShowProtocolTunnelStatus (tCliHandle CliHandle,INT4 i4PortId);

INT4
VlanPbSetL2ProtocolTunnelStatusPerVLAN (tCliHandle CliHandle, UINT4 u4ContextId,
                                        tVlanId VlanId, UINT4 u4L2PType, UINT4 u4TunnelStatus);
INT4
VlanPbShowCVlanStat (tCliHandle CliHandle,UINT4 u4ContextId,INT4 i4Port,INT4 i4CvlanId);

INT4
VlanPbClearCVlanStat (tCliHandle CliHandle, UINT4 u4ContextId,INT4 i4Port,INT4 i4CvlanId);

INT4
VlanPbSetCounterStatus (tCliHandle CliHandle, INT4 i4Port ,INT4 i4VlanId,
                      INT4 i4CounterStatus);




#endif
