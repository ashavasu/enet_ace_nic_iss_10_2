/* $Id: qosxcli.h,v 1.52 2017/09/20 11:39:17 siva Exp $ */
/****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                     */
/*                                                                          */
/*  FILE NAME             : qoscli.h                                        */
/*  PRINCIPAL AUTHOR      : Aricent                                         */
/*  SUBSYSTEM NAME        : QoS                                             */
/*  MODULE NAME           : QoS-Ext                                         */
/*  LANGUAGE              : C                                               */
/*  TARGET ENVIRONMENT    : Linux                                           */
/*  DATE OF FIRST RELEASE :                                                 */
/*  AUTHOR                : Aricent                                         */
/*  FUNCTIONS DEFINED(Applicable for Source files) :                        */
/*  DESCRIPTION           : This file contains the CLI definitions and      */
/*                          macros of Aricent QoS Module.                   */
/****************************************************************************/
/*                                                                          */
/*  Change History                                                          */
/*  Version               :                                                 */
/*  Date(DD/MM/YYYY)      :                                                 */
/*  Modified by           :                                                 */
/*  Description of change :                                                 */
/****************************************************************************/

#ifndef __QOS_CLI_H__
#define __QOS_CLI_H__

#include "cli.h"
#define CLI_RS_INVALID_MASK  ~0UL
#define CLI_RS_INVALID_I_MASK  (-1)

#define QOS_CLI_MAX_ARGS      10   
#define QOS_CLI_NEW_ENTRY     0
#define QOS_CLI_MODIFY_ENTRY  1
#define QOS_CLI_SET_TRCLASS_PCP_CMD 1
#define QOS_CLI_CLR_TRCLASS_PCP_CMD 2

/* CLI Prompt Macros */
#define QOS_CLI_MAX_PROMPT_LENGTH   32
#define QOS_CLI_PRI_MAP_MODE        "PriMap"
#define QOS_CLI_CLS_MAP_MODE        "ClsMap"
#define QOS_CLI_METER_MODE          "Meter"
#define QOS_CLI_PLY_MAP_MODE        "PlyMap"
#define QOS_CLI_QTEMP_MODE          "QTemp"
#define QOS_CLI_VLAN_MAP_MODE       "VlanMap"



#define CLI_GET_PRI_MAP_ID()         CliGetModeInfo()
#define CLI_GET_VLAN_MAP_ID()        CliGetModeInfo()
#define CLI_GET_CLS_MAP_ID()         CliGetModeInfo()
#define CLI_GET_METER_ID()           CliGetModeInfo()
#define CLI_GET_PLY_MAP_ID()         CliGetModeInfo()
#define CLI_GET_QTEMP_ID()           CliGetModeInfo()

#define CLI_SET_PRI_MAP_ID(Id)       CliSetModeInfo(Id)
#define CLI_SET_CLS_MAP_ID(Id)       CliSetModeInfo(Id)
#define CLI_SET_METER_ID(Id)         CliSetModeInfo(Id)
#define CLI_SET_PLY_MAP_ID(Id)       CliSetModeInfo(Id)
#define CLI_SET_QTEMP_ID(Id)         CliSetModeInfo(Id)



#define QOS_CLI_IN_PRI_TYPE_VLAN_PRI     0
#define QOS_CLI_IN_PRI_TYPE_IP_TOS       1
#define QOS_CLI_IN_PRI_TYPE_IP_DSCP      2
#define QOS_CLI_IN_PRI_TYPE_MPLS_EXP     3
#define QOS_CLI_IN_PRI_TYPE_VLAN_DEI     4
#define QOS_CLI_IN_PRI_TYPE_DOT1P        5

#define QOS_CLI_CLS_COLOR_NONE           0
#define QOS_CLI_CLS_COLOR_GREEN          1
#define QOS_CLI_CLS_COLOR_YELLOW         2
#define QOS_CLI_CLS_COLOR_RED            3

#define QOS_CLI_COLOR_MODE_AWARE         1
#define QOS_CLI_COLOR_MODE_BLIND         2

#define QOS_CLI_METER_TYPE_STB           1
#define QOS_CLI_METER_TYPE_AVGRATE       2   
#define QOS_CLI_METER_TYPE_SRTCM         3
#define QOS_CLI_METER_TYPE_TRTCM         4
#define QOS_CLI_METER_TYPE_TSWTCM        5   
#define QOS_CLI_METER_TYPE_MEFDECOUPLED  6
#define QOS_CLI_METER_TYPE_MEFCOUPLED    7

#define QOS_CLI_IN_CONF_ACT_DROP             0x0001
#define QOS_CLI_IN_CONF_ACT_PORT             0x0100
#define QOS_CLI_IN_CONF_ACT_IP_TOS           0x0004
#define QOS_CLI_IN_CONF_ACT_IP_DSCP          0x0008
#define QOS_CLI_IN_CONF_ACT_VLAN_PRI         0x0010
#define QOS_CLI_IN_CONF_ACT_VLAN_DE          0x0020
#define QOS_CLI_IN_CONF_ACT_INNER_VLAN_PRI   0x0040
#define QOS_CLI_IN_CONF_ACT_MPLS_EXP         0x0080
#define QOS_CLI_IN_CONF_ACT_INNER_VLAN_DE    0x0002

#define QOS_CLI_IN_EXC_ACT_DROP              0x0001
#define QOS_CLI_IN_EXC_ACT_IP_TOS            0x0002
#define QOS_CLI_IN_EXC_ACT_IP_DSCP           0x0004
#define QOS_CLI_IN_EXC_ACT_VLAN_PRI          0x0008
#define QOS_CLI_IN_EXC_ACT_VLAN_DE           0x0010
#define QOS_CLI_IN_EXC_INNER_VLAN_PRI        0x0020
#define QOS_CLI_IN_EXC_ACT_MPLS_EXP          0x0040
#define QOS_CLI_IN_EXC_INNER_VLAN_DE         0x0080

#define QOS_CLI_OUT_ACT_DROP                 0x0001
#define QOS_CLI_OUT_ACT_IP_TOS               0x0002
#define QOS_CLI_OUT_ACT_IP_DSCP              0x0004
#define QOS_CLI_OUT_ACT_VLAN_PRI             0x0008
#define QOS_CLI_OUT_ACT_VLAN_DE              0x0010
#define QOS_CLI_OUT_ACT_INNER_VLAN_PRI       0x0020
#define QOS_CLI_OUT_ACT_MPLS_EXP             0x0040
#define QOS_CLI_OUT_ACT_INNER_VLAN_DE        0x0080

#define QOS_CLI_PLY_PHB_TYPE_NONE            0
#define QOS_CLI_PLY_PHB_TYPE_VLAN_PRI        1
#define QOS_CLI_PLY_PHB_TYPE_IP_TOS          2
#define QOS_CLI_PLY_PHB_TYPE_IP_DSCP         3
#define QOS_CLI_PLY_PHB_TYPE_MPLS_EXP        4
#define QOS_CLI_PLY_PHB_TYPE_DOT1P           5

/* Q Template */
#define QOS_CLI_QTEMP_DROP_ALGO_ENABLE       1
#define QOS_CLI_QTEMP_DROP_ALGO_DISABLE      2

#define QOS_CLI_QTEMP_DROP_TYPE_TAIL         2
#define QOS_CLI_QTEMP_DROP_TYPE_HEAD         3
#define QOS_CLI_QTEMP_DROP_TYPE_RED          4
#define QOS_CLI_QTEMP_DROP_TYPE_ALWAYS       5
#define QOS_CLI_QTEMP_DROP_TYPE_WRED         6

#define QOS_Q_UNICAST_TYPE                   1
#define QOS_Q_MULTICAST_TYPE                 2

/* Scheduler Table */
#define QOS_CLI_SCHED_ALGO_SP                1
#define QOS_CLI_SCHED_ALGO_RR                2
#define QOS_CLI_SCHED_ALGO_WRR               3
#define QOS_CLI_SCHED_ALGO_WFQ               4
#define QOS_CLI_SCHED_ALGO_SRR               5
#define QOS_CLI_SCHED_ALGO_SWRR              6
#define QOS_CLI_SCHED_ALGO_SWFQ              7
#define QOS_CLI_SCHED_ALGO_DRR               8

/* Q Map Table */
#define QOS_CLI_QMAP_PRI_TYPE_NONE           0
#define QOS_CLI_QMAP_PRI_TYPE_VLAN_PRI       1
#define QOS_CLI_QMAP_PRI_TYPE_IP_TOS         2  
#define QOS_CLI_QMAP_PRI_TYPE_IP_DSCP        3
#define QOS_CLI_QMAP_PRI_TYPE_MPLS_EXP       4
#define QOS_CLI_QMAP_PRI_TYPE_VLAN_DEI       5
#define QOS_CLI_QMAP_PRI_TYPE_INT_PRI        6
#define QOS_CLI_QMAP_PRI_TYPE_DOT1P          7

#define QOS_CLI_QMAP_CLR_GREEN               0
#define QOS_CLI_QMAP_CLR_YELLOW              1
#define QOS_CLI_QMAP_CLR_RED                 2
/* RD Cfg Table */
#define QOS_CLI_RD_CFG_DISCARD_PKTS          1
#define QOS_CLI_RD_CFG_DISCARD_BYTES         2

#define QOS_CLI_RD_CFG_FLAG_NONE             0
#define QOS_CLI_RD_CFG_FLAG_CAP_AVG          1 
#define QOS_CLI_RD_CFG_FLAG_MARK_ECN         2

/* PCP Selection */
#define QOS_CLI_8P0D_SEL_ROW 0
#define QOS_CLI_7P1D_SEL_ROW 1
#define QOS_CLI_6P2D_SEL_ROW 2
#define QOS_CLI_5P3D_SEL_ROW 3

#define QOS_CLI_ETHERNET_PKT 0
#define QOS_CLI_IP_PKT 1
#define QOS_CLI_MPLS_PKT 2

typedef struct _tQoSCliMeterAction {
    INT4 i4ConNClass;
    INT4 i4ExcNClass;
    INT4 i4VioNClass;
    INT1 i1ConfVlanPri;
    INT1 i1ConfVlanDe;
    INT1 i1ConfIPDscp;
    INT1 i1ConfIPTos;
    INT1 i1ConfSetPort;
    INT1 i1ConfInVlanPri;
    INT1 i1ConfMPLSExp;
    INT1 i1ExcVlanPri;
    INT1 i1ExcVlanDe;
    INT1 i1ExcIPDscp;
    INT1 i1ExcIPTos;
    INT1 i1ExcInVlanPri;
    INT1 i1ExcMPLSExp;
    INT1 i1VioVlanPri;
    INT1 i1VioVlanDe;
    INT1 i1VioIPDscp;
    INT1 i1VioIPTos;
    INT1 i1VioInVlanPri;
    INT1 i1VioMPLSExp;
    INT1 i1ConfInVlanDE;
    INT1 i1ExcInVlanDE;
    INT1 i1VioInVlanDE;
    INT1 i1Rsvd[2];

} tQoSCliMeterAction;

typedef struct _tQoSCliRSMetAct {

    UINT4 u4ConNClass;
    UINT4 u4ExcNClass;
    UINT4 u4VioNClass;
    UINT4 u4ConfSetPort;
    UINT4 u4ConfInVlanPri;
    UINT4 u4ConfInVlanDE;
    UINT4 u4ConfVlanPri;
    UINT4 u4ConfVlanDe;
    UINT4 u4ConfIPDscp;
    UINT4 u4ConfIPTos;
    UINT4 u4ConfMPLSExp;
    UINT4 u4ExcInVlanPri;
    UINT4 u4ExcInVlanDE;
    UINT4 u4ExcVlanPri;
    UINT4 u4ExcVlanDe;
    UINT4 u4ExcIPDscp;
    UINT4 u4ExcIPTos;
    UINT4 u4ExcMPLSExp;
    UINT4 u4VioInVlanPri;
    UINT4 u4VioInVlanDE;
    UINT4 u4VioVlanPri;
    UINT4 u4VioVlanDe;
    UINT4 u4VioIPDscp;
    UINT4 u4VioIPTos;
    UINT4 u4VioMPLSExp;

} tQoSCliRSMetAct;



typedef struct _tQoSCliRSMeterParam {

    UINT4 u4MeterType;
    UINT4 u4ColorMode; 
    UINT4 u4Interval; 
    UINT4 u4CIR; 
    UINT4 u4CBS; 
    UINT4 u4EIR; 
    UINT4 u4EBS; 
    UINT4 u4NextMeter;

} tQoSCliRSMeterParam;

typedef struct _tQoSCliQParam {

    UINT4 u4QIdx;
    UINT4 u4IfIdx;
    UINT4 u4QType;
    UINT4 u4QWeight;
    UINT4 u4QPri;
    UINT4 u4SchedIdx;
    UINT4 u4ShapeIdx;
    UINT4 u4QueueType;

} tQoSCliQParam;

typedef struct _tQoSCliHSParam {

    UINT4 u4IfIdx;
    UINT4 u4HL;
    UINT4 u4SchedId;
    UINT4 u4NextSched;
    UINT4 u4NextQ;
    UINT4 u4Pri;
    UINT4 u4Weight;

} tQoSCliHSParam;


/* System Controls */
enum {

/* 1*/  CLI_QOS_SET_SYS_CNTRL_DISABLE  = 1,
/* 2*/  CLI_QOS_SET_SYS_CNTRL_ENABLE ,
/* 3*/  CLI_QOS_SET_SYS_STATUS ,
/* 4*/  CLI_QOS_ADD_PRI_MAP,
/* 5*/  CLI_QOS_SET_PRI_MAP_PARAMS,
/* 6*/  CLI_QOS_SHOW_PRI_MAP,
/* 7*/  CLI_QOS_DEL_PRI_MAP, 
/* 8*/  CLI_QOS_ADD_CLS_MAP,
/* 9*/  CLI_QOS_SET_CLS_MAP_PARAMS,
/* 10*/ CLI_QOS_NO_SET_PRI_MAP_PARAMS,
/* 11*/ CLI_QOS_SHOW_CLS_MAP,
/* 11*/ CLI_QOS_DEL_CLS_MAP, 
/* 13*/ CLI_QOS_ADD_METER, 
/* 14*/ CLI_QOS_DEL_METER, 
/* 15*/ CLI_QOS_ADD_PLY_MAP,
/* 16*/ CLI_QOS_DEL_PLY_MAP,
/* 17*/ CLI_QOS_SET_CLS_MAP_CLASS,
/* 18*/ CLI_QOS_SET_NO_CLS_MAP_CLASS,
/* 19*/ CLI_QOS_SET_METER_PARAMS,
/* 20*/ CLI_QOS_SET_PLY_CLS,
/* 21*/ CLI_QOS_NO_SET_PLY_IF,
/* 22*/ CLI_QOS_SET_PLY_METER_ACTION,
/* 23*/ CLI_QOS_SET_PLY_NO_METER,
/* 24*/ CLI_QOS_SHOW_CLS2PRI_MAP,
/* 25*/ CLI_QOS_SHOW_METER,
/* 26*/ CLI_QOS_SHOW_PLY_MAP,
/* 27*/ CLI_QOS_SHOW_GLB_INFO,
/* 28*/ CLI_QOS_ADD_Q_TEMP,
/* 29*/ CLI_QOS_DEL_Q_TEMP,
/* 30*/ CLI_QOS_SET_Q_TEMP_PARAMS,
/* 31*/ CLI_QOS_SET_RD_CFG_PARAMS,
/* 32*/ CLI_QOS_DEL_RD_CFG,
/* 33*/ CLI_QOS_SHOW_QTEMP,
/* 34*/ CLI_QOS_ADD_SHAPE_TEMP,
/* 35*/ CLI_QOS_DEL_SHAPE_TEMP,
/* 36*/ CLI_QOS_SHOW_SHAPE_TEMP,
/* 37*/ CLI_QOS_ADD_SCHED,
/* 38*/ CLI_QOS_DEL_SCHED,
/* 39*/ CLI_QOS_SHOW_SCHED,
/* 40*/ CLI_QOS_ADD_Q,
/* 41*/ CLI_QOS_DEL_Q,
/* 42*/ CLI_QOS_SHOW_Q,
/* 43*/ CLI_QOS_ADD_QMAP,
/* 44*/ CLI_QOS_DEL_QMAP,
/* 45*/ CLI_QOS_SHOW_QMAP,
/* 46*/ CLI_QOS_ADD_HS,
/* 47*/ CLI_QOS_DEL_HS,
/* 48*/ CLI_QOS_SHOW_HS,
/* 49*/ CLI_QOS_SET_DUP,
/* 50*/ CLI_QOS_SHOW_DUP,
/* 51*/ CLI_QOS_SHOW_MET_STATS,
/* 52*/ CLI_QOS_SHOW_Q_STATS,
/* 53*/ CLI_QOS_DEBUG, 
/* 54*/ CLI_QOS_NO_DEBUG,
/* 55*/ CLI_QOS_SET_PBIT_PREF,
/* 56*/ CLI_QOS_SHOW_PBIT_PREF,
/* 57*/ CLI_QOS_CPU_RATE_CONF,
/* 58*/ CLI_QOS_SHOW_CPU_Q,
/* 59*/ CLI_QOS_SHOW_DSCP_Q_MAP,
/* 60*/ CLI_QOS_TRCLASS_TO_PCP,
/* 61*/ CLI_QOS_NO_TRCLASS_TO_PCP,
/* 62*/ CLI_QOS_SET_METER_STATS,
/* 62*/ CLI_QOS_CLEAR_METER_STATS,
/* 63*/ CLI_QOS_ADD_VLAN_MAP,
/* 64*/ CLI_QOS_SET_VLAN_MAP_PARAMS,
/* 65*/ CLI_QOS_SET_VLAN_QUEUING_STATUS,
/* 66*/ CLI_QOS_SHOW_VLAN_MAP,
/* 67*/ CLI_QOS_DEL_VLAN_MAP,
/* 68*/ CLI_QOS_SHOW_PCP_SEL_ROW,
/* 69*/ CLI_QOS_SET_PCP_SEL_ROW,
/* 70*/ CLI_QOS_ADD_QMAP_EXT,
/* 71*/ CLI_QOS_SHOW_PORT_PCP_ENCODE,
/* 72*/ CLI_QOS_SHOW_PORT_PCP_DECODE,
/* 73*/ CLI_QOS_SHOW_PCP_QMAP
};


enum {
 
 /* Common ERROR CODE */
 QOS_CLI_ERR_MODULE_SHUTDOW  = CLI_ERR_START_ID_QOS,
 QOS_CLI_ERR_INVALID_SYS_CONTROL,
 QOS_CLI_ERR_INVALID_SYS_STATUS,
 QOS_CLI_ERR_INVALID_TRC_FLAG,
 QOS_CLI_ERR_STATUS_ACTIVE,
 QOS_CLI_ERR_INVALID_IF_INDEX,
 QOS_CLI_ERR_INVALID_VLAN_INDEX,
 QOS_CLI_ERR_INVALID_VLAN_PRI,
 QOS_CLI_ERR_INVALID_IP_TOS,
 QOS_CLI_ERR_INVALID_IP_DSCP,
 QOS_CLI_ERR_INVALID_MPLS_EXP,
 QOS_CLI_ERR_INVALID_VLAN_DE,
 QOS_CLI_ERR_INVALID_PRI_TYPE,
 QOS_CLI_ERR_TBL_NAME_INVALID,
 QOS_CLI_ERR_INVALID_CIR,
 QOS_CLI_ERR_INVALID_CBS,
 QOS_CLI_ERR_INVALID_EIR,
 QOS_CLI_ERR_INVALID_EBS,
 QOS_CLI_ERR_WEIGHT,
 QOS_CLI_ERR_PRIORITY,
 QOS_CLI_ERR_HL_INVALID,
 QOS_CLI_ERR_QMAP_PRI_RANGE,
 QOS_CLI_ERR_DEF_ENTRY,
 QOS_CLI_ERR_UNSUPPORTED_BY_HW,
 QOS_CLI_ERR_INVALID_HL_VALUE,
 /* PriorityMapTbl ERROR CODE */
 QOS_CLI_ERR_PRI_MAP_RANGE,
 QOS_CLI_ERR_PRI_MAP_NO_ENTRY,
 QOS_CLI_ERR_INVALID_INNER_PRI,
 QOS_CLI_ERR_PRI_MAP_MAX_ENTRY,
 QOS_CLI_ERR_PRI_MAP_RFE,
 QOS_CLI_ERR_PRI_MAP_DUP_ENTRY,
 QOS_CLI_ERR_DEF_PRI_MAP_INV_IN_PRI,
 /* Vlan Queueing ERROR CODE */
 QOS_CLI_ERR_VLAN_MAP_RANGE,
 QOS_CLI_ERR_VLAN_MAP_NO_ENTRY,
 QOS_CLI_ERR_VLAN_MAP_MAX_ENTRY,
 QOS_CLI_ERR_VLAN_MAP_RFE,
 QOS_CLI_ERR_VLAN_MAP_DISABLED,
 QOS_CLI_ERR_VLAN_MAP_HL_NOT_SUPPORTED,
 /* ClassMapTbl ERROR CODE */
 QOS_CLI_ERR_FILTER_NOT_FORQOS,
 QOS_CLI_ERR_CLS_MAP_RANGE,
 QOS_CLI_ERR_CLS_MAP_MAX_ENTRY,
 QOS_CLI_ERR_CLS_MAP_RFE,
 QOS_CLI_ERR_CLS_MAP_RFE_QMAP,
 QOS_CLI_ERR_CLS_MAP_NO_ENTRY,
 QOS_CLI_ERR_CLS_MAP_NOT_ACTIVE,
 QOS_CLI_ERR_L2_L3_OR_PRI,
 QOS_CLI_ERR_CLS_MAP_INVALID_CLS,
 QOS_CLI_ERR_L2_FILTER_ID_INVALID,
 QOS_CLI_ERR_L2_FILTER_ID_NOT_ACTIVE,
 QOS_CLI_ERR_L3_FILTER_ID_INVALID,
 QOS_CLI_ERR_L3_FILTER_ID_NOT_ACTIVE,
 QOS_CLI_ERR_PRI_MAP_ID_NOT_ACTIVE,
 QOS_CLI_ERR_VLAN_MAP_ID_NOT_ACTIVE,
 QOS_CLI_ERR_CLS_MAP_NP_NOT_SUPPORTED,
 QOS_CLI_ERR_CLS_MAP_INVALID_FILTER_ACTION,
 QOS_CLI_ERR_CLS_MAP_L2_L3_FILTER_ID,
 QOS_CLI_ERR_CLS_MAP_SET_CLS_NOT_SUPPORTED,
 /* ClassToPriotity Tbl ERROR CODE */
 QOS_CLI_ERR_CLS2PRI_MAP_MAX_ENTRY,
 QOS_CLI_ERR_INVALID_REGEN_PRI,
 QOS_CLI_ERR_CLS_NOT_CREATED,
 QOS_CLI_ERR_CLS2PRI_MAP_NO_ENTRY,
 QOS_CLI_ERR_NO_CLS_CLSMAP,
 /* MeterTbl ERROR CODE */
 QOS_CLI_ERR_METER_INVALID_TYPE,
 QOS_CLI_ERR_METER_INVALID_INTERVAL,
 QOS_CLI_ERR_METER_INVALID_COLOR_MODE,
 QOS_CLI_ERR_METER_RANGE,
 QOS_CLI_ERR_METER_MAX_ENTRY,
 QOS_CLI_ERR_METER_RFE,
 QOS_CLI_ERR_METER_MANDATORY,
 QOS_CLI_ERR_METER_NO_ENTRY,
 QOS_CLI_ERR_METER_NOT_ACTIVE,
 QOS_CLI_ERR_METER_INVALID_PARAM,
 QOS_CLI_ERR_METER_CIR_LESS_THAN_EIR,
 /* PolicyMapTbl ERROR CODE */
 QOS_CLI_ERR_PHB_TYPE_INVALID,
 QOS_CLI_ERR_PRE_COLOR_INVALID,
 QOS_CLI_ERR_PLY_MAP_RANGE,
 QOS_CLI_ERR_PLY_MAP_MAX_ENTRY,
 QOS_CLI_ERR_PLY_MAP_NO_ENTRY,
 QOS_CLI_ERR_ACT_FLG_LEN_INVALID,
 QOS_CLI_ERR_ACT_FLG_INVALID,
 QOS_CLI_ERR_DROP_CLUB,
 QOS_CLI_ERR_NONE_CLUB,
 QOS_CLI_ERR_TOS_DSCP,
 QOS_CLI_ERR_VLAN_MPLS,
 QOS_CLI_ERR_NO_FILTER,
 QOS_CLI_ERR_PLY_MULTIMAP,
 QOS_CLI_ERR_DEF_PLY_MAP,
 QOS_CLI_ERR_PLY_INVALID_METER_PARAM,
 QOS_CLI_ERR_PLY_ACL_STAGE_INVALID,
 /* QTemp Tbl ERROR CODE */
 QOS_CLI_ERR_QTEMP_DROP_TYPE,
 QOS_CLI_ERR_QTEMP_DROP_ALGO,
 QOS_CLI_ERR_QTEMP_QSIZE_INVALID,
 QOS_CLI_ERR_QTEMP_RANGE,
 QOS_CLI_ERR_QTEMP_MAX_ENTRY,
 QOS_CLI_ERR_QTEMP_RFE,
 QOS_CLI_ERR_QTEMP_NO_RDCFG,
 QOS_CLI_ERR_QTEMP_NOT_CREATION,
 QOS_CLI_ERR_QTEMP_NOT_ACTIVE,
 /* REDCfgTbl ERROR CODE */
 QOS_CLI_ERR_RDCFG_MIN_AVG_TH,
 QOS_CLI_ERR_RDCFG_MAX_AVG_TH,
 QOS_CLI_ERR_INVALID_MIN_AVG_TH,
 QOS_CLI_ERR_INVALID_MAX_AVG_TH,
 QOS_CLI_ERR_RDCFG_MAX_PKT,
 QOS_CLI_ERR_RDCFG_MAX_PROB,
 QOS_CLI_ERR_RDCFG_EXP_WEIGHT,
 QOS_CLI_ERR_RDCFG_DP_RANGE,
 QOS_CLI_ERR_RDCFG_MAX_ENTRY,
 QOS_CLI_ERR_RDCFG_REF,
 QOS_CLI_ERR_RDCFG_NO_ENTRY,
 /* Shape Tbl ERROR CODE */
 QOS_CLI_ERR_SHAPE_NOT_ACTIVE,
 QOS_CLI_ERR_SHAPE_NOT_CREATED,
 QOS_CLI_ERR_SHAPE_TEMP_RANGE,
 QOS_CLI_ERR_SHAPE_MAX_ENTRY,
 QOS_CLI_ERR_SHAPE_TEMP_RFE,
 QOS_CLI_ERR_SHAPE_TEMP_EIR_SUPPORT,
 QOS_CLI_ERR_SHAPE_TEMP_EBS_SUPPORT,
 /* Q Tbl ERROR CODE */
 QOS_CLI_ERR_Q_MAX_ENTRY,
 QOS_CLI_ERR_Q_RANGE,
 QOS_CLI_ERR_Q_RFE,
 QOS_CLI_ERR_Q_NOT_ACTIVE,
 QOS_CLI_ERR_Q_NO_ENTRY,
 QOS_CLI_ERR_Q_MANDATORY,
 QOS_CLI_ERR_QTYPE_MULTICAST,
 QOS_CLI_ERR_QTYPE_UNICAST,
 QOS_CLI_ERR_QTYPE_SUBQ_DEL,
 QOS_CLI_ERR_Q_ID_SCHED_ENTRY,
 /* QMAP Tbl ERROR CODE */
 QOS_CLI_ERR_QMAP_MAX_ENTRY,
 QOS_CLI_ERR_QMAP_NO_ENTRY,
 QOS_CLI_ERR_QMAP_INVALID_ACL_PARAMS,
 QOS_CLI_ERR_QMAP_INVALID_VLAN_MAP_PARAMS,
 /* Sched Tbl ERROR CODE */
 QOS_CLI_ERR_SCHED_NO_ENTRY,
 QOS_CLI_ERR_SCHED_RANGE,
 QOS_CLI_ERR_SCHED_NOT_ACTIVE,
 QOS_CLI_ERR_SCHED_INVALID_ALGO,
 QOS_CLI_ERR_SCHED_MAX_ENTRY,
 QOS_CLI_ERR_SCHED_RFE,
 QOS_CLI_ERR_SCHED_IMPROPER_CFG,
 /* HS Tbl ERROR CODE */
 QOS_CLI_ERR_NEXT_SCHED_Q_NO_ENTRY,
 QOS_CLI_ERR_HS_SELF_REF,
 QOS_CLI_ERR_HS_MAX_ENTRY,
 QOS_CLI_ERR_HL_NOT_MATCH,
 QOS_CLI_ERR_HIERARCHY_NO_ENTRY,
 QOS_CLI_ERR_DROP_FLG,
 /* CpuQ Rate Limit Tbl ERROR CODE */
 QOS_CLI_ERR_CPUQ_RATE_RANGE,
 QOS_CLI_ERR_CPUQ_MIN_GRT_MAX,
 QOS_CLI_ERR_CPUQ_MAX_LES_MIN,
 /* RD Cfg Table Error code */
 QOS_CLI_ERR_RDCFG_GAIN,
 QOS_CLI_ERR_RDCFG_DROP_THRESH_TYPE,
 QOS_CLI_ERR_RDCFG_ECN_THRESH,
 QOS_CLI_ERR_RDCFG_ACT_FLG_LEN_INVALID,
 /* QTemp Tbl ERROR CODE */
 QOS_CLI_ERR_QTEMP_RD_CFG_RFE,
 QOS_CLI_ERR_INVALID_PRIORITY_VALUE,
 QOS_CLI_ERR_PORTCHANNEL,
 QOS_CLI_ERR_MAX_SCHED_PER_PORT,
 QOS_CLI_ERR_DEF_SCHED_MODIFY,
 QOS_CLI_ERR_INVAL_QUEUE_PARAM,
 QOS_CLI_ERR_INVALID_VLAN_MAP_VALUE, 
 QOS_CLI_ERR_INVALID_IN_DEI,
 QOS_CLI_ERR_INVALID_REGEN_COLOR,
 QOS_CLI_ERR_PORT_TBL_RANGE,
 /*Port Table ERROR CODE */
 QOS_CLI_ERR_PORT_TBL_NO_ENTRY,
 QOS_CLI_ERR_PORT_TBL_MAX_ENTRY,
 QOS_CLI_ERR_INVALID_PCP_PKT_TYPE,
 QOS_CLI_ERR_INVALID_PCP_SEL_ROW,
 QOS_CLI_ERR_MAX_DE_CONFIGURED,
 QOS_CLI_MAX_ERR
};

/* Accessing the ErrString with ErrCode value.
 * Since the ErrCode's starting Id is changed, this forumla is used
 * to access the Err String */
#define CLI_ERR_OFFSET_QOS(u4ErrCode)   (u4ErrCode - CLI_ERR_START_ID_QOS + 1)

#ifdef __QOS_CLI_C__

CONST CHR1 *gapi1QoSCliErrString [] = {
    NULL,
    /* Common ERROR CODE */
    "QoS Should Be Started Before Accessing It !.\r\n",
    "Invalid System Control.\r\n",
    "Invalid System Status.\r\n",
    "Invalid Trace Flag.\r\n",
    "Can not change the value in ACTIVE State.\r\n",
    "Invalid IfIndex.\r\n",
    "Invalid Vlan Id.\r\n",
    "Invalid Vlan Priority.\r\n",
    "Invalid IP ToS.\r\n",
    "Invalid IP DSCP.\r\n",
    "Invalid MPLS Exp.\r\n",
    "Invalid VLAN DE.\r\n",
    "Invalid Priority Type.\r\n",
    "Table name Invalid char. Valid Char = (A-Z, a-z, 0-9, _, - ).\r\n",
    "Invalid CIR.\r\n",
    "Invalid CBS.\r\n",
    "Invalid EIR.\r\n",
    "Invalid EBS.\r\n",
    "Invalid Weight.\r\n",
    "Invalid Priority.\r\n",
    "Invalid Hierarchy Level.\r\n",
    "Priority value is out of Range.\r\n",
    "Default Table Entry can not be deleted.\r\n",
    "Unsupported operation by hardware.\r\n",
    "Invalid Hierarchy Level for scheduler node.\r\n",
    /* PriorityMapTbl ERROR CODE */
    "Priority Map Id is out of Range.\r\n",
    "Priority Map Entry not Created.\r\n",
    "Invalid Inner Priority.\r\n",
    "Max Priority Map Entries are Configured.\r\n",
    "Priority Map Entry referenced by Class Map Table Entry.\r\n",
    "Priority Map values are Duplicate.\r\n",
    "Default Priority Map - invalid in-priority.\r\n",
    /* Vlan Queueing MapTbl ERROR CODE */
    "Vlan Map Id is out of Range.\r\n",
    "Vlan Map Entry not Created.\r\n",
    "Max Vlan Map Entries are Configured.\r\n",
    "Vlan Map Entry referenced by Class Map Table Entry.\r\n",
    "Vlan Queueing feature not enabled.\r\n",
    "Feature cannot be enabled. Hierarchial Scheduling not supported.\r\n",
    /* ClassMapTbl ERROR CODE */
    "Filter is not for QOS.\r\n",
    "Class Map Id out of Range.\r\n",
    "Max Class Map Entries are Configured.\r\n",
    "Class Map Entry referenced by Policy Map Table Entry.\r\n",
    "Class Map Entry referenced by Queue Map Table Entry.\r\n",
    "Class Map Entry not Created.\r\n",
    "Class Map entry is not active.\r\n",
    "Either L2 and/or L3 Filters or PriMapId is associated with this class-map.\r\n",
    "CLASS is out of Range.\r\n",
    "Invalid L2 Filter Id.\r\n",
    "L2 Filter Id Not Active.\r\n",
    "Invalid L3 Filter Id.\r\n",
    "L3 Filter Id Not Active.\r\n",
    "Priority Map Id Not Active.\r\n",
    "Vlan Map Id Not Active.\r\n",
    "Regen Priority is not supported for Out filter.\r\n",
    "QOS cannot be applied for a deny filter.\r\n",
    "More than one Class Map Id cannot be Mapped to a L2 and/or L3 FilterId.\r\n",
    "Filter is not yet associated into the ClassMap.\r\n",
    /* ClassToPriotity Tbl ERROR CODE */
    "Class to Priority Map Max Entries Configured.\r\n",
    "ClassToPriotity Regen Priority value is out of range.\r\n",
    "Class Not yet Created.\r\n",
    "No Class To Priority Map Entry.\r\n",
    "No Such Class Exists for the ClassMap.\r\n",
    /* MeterTbl ERROR CODE */
    "Invalid Meter Type.\r\n",
    "Invalid Interval value.\r\n",
    "Invalid Color Mode.\r\n",
    "Meter Table Index out of Range.\r\n",
    "Meter Max Entries Configured.\r\n",
    "Meter Table Entry is referenced by  Policy Entry or other Meter.\r\n",
    "Meter Mandatory Fields are not set or Invalid fields are set.\r\n",
    "Meter Table Entry not created.\r\n",
    "Meter Table Entry not Active.\r\n",
    "Given meter parameter not valid for the current meter Type.\r\n",
    "CIR should be less than EIR or Invalid fields are set\r\n",
/* PolicyMapTbl ERROR CODE */
    "PHB Type Invalid.\r\n",
    "Pre Color Invalid.\r\n",
    "Policy map Table Index out of Range.\r\n",
    "Policy map max Entries Configured.\r\n",
    "Policy map entry not created.\r\n",
    "Policy map Action flag length invalid.\r\n",
    "Policy map Action value and Action flag Combination invalid.\r\n",
    "Policy map Action 'Drop' can not club with other options.\r\n",
    "Policy map Action 'None' can not club with other options.\r\n",
    "Action flag value 'TOS and DSCP' mutually exclusive\r\n",
    "Action flag value 'VLAN and MPLS Flags' mutually exclusive.\r\n",
    "Filter not configured for the CLASS.\r\n",
    "CLASS is already Mapped with another PolicyMapEntry.\r\n",
    "Default Policy Map with Default Class cannot be modified.\r\n",
    "Exceed action not allowed for simple token bucket meter.\r\n",
    "Given marking action is not supported at the specified access-list stage.\r\n",
    /* QTemp Tbl ERROR CODE */
    "Q Template Drop Type value is out of range.\r\n",
    "Q Template Drop Algo value is out of range.\r\n",
    "Q Template Size is out of range.\r\n",
    "Q Template Id is out of range.\r\n",
    "Max No of Q Template Table Entries are Configured.\r\n",
    "This Entry is referenced by Q Table Entry.\r\n",
    "RD Tbl Entry for this QTempId is not Configured.\r\n",
    "Queue Template is not created.\r\n",
    "Queue Template entry is not active.\r\n",
    /* REDCfgTbl ERROR CODE */
    "RD Cfg MinAvgThresh  value is out of range.\r\n",
    "RD Cfg MaxAvgThresh  value is out of range.\r\n",
    "RD Cfg MinAvgThresh should be less than or equal to MaxAvgThresh.\r\n",
    "RD Cfg MaxAvgThresh should be greater than or equal to MinAvgThresh.\r\n",
    "RD Cfg MaxPktSize value is out of range.\r\n",
    "RD Cfg Max Drop Probability value is out of range.\r\n",
    "RD Cfg Exponential Weight value is out of range.\r\n",
    "RD Cfg DP value is out of range.\r\n",
    "Max No of RD Cfg Table Entries are Configured.\r\n",
    "This Entry is referenced by QTemplate Table Entry.\r\n",
    "RD Cfg Entry is not yet created.\r\n",
    /* Shape Tbl ERROR CODE */
    "Shape Template entry is not active.\r\n",
    "Shape Template not created.\r\n",
    "Shape Template Index is out of Range.\r\n",
    "Max No of Shape Table Entries are Configured.\r\n",
    "This Entry is referenced by Q Table Entry or Scheduler Table Entry.\r\n",
    "Shape Template EIR Param is not supported in this Platform.\r\n",
    "Shape Template EBS Param is not supported in this Platform.\r\n",
    /* Q Tbl ERROR CODE */
    "Max No of Queue Table Entries are Configured. \r\n",
    "Queue Id is out of Range.\r\n",
    "Queue Entry is referenced by Hierarchy Table Entry .\r\n",
    "Queue entry is not active.\r\n",
    "Queue entry not created.\r\n",
    "Queue entry mandatory params not set.\r\n",
    "Cannot configure COSQ's as Multicast type.\r\n",
    "Cannot configure COSQ's as Unicast type.\r\n",
    "Subscriber Queues can not be deleted.\r\n",
    "Invalid Scheduler Id for Queue index.\r\n",
    /* QMAP Tbl ERROR CODE */
    "Max No of Queue map Entries are Configured.\r\n",
    "Queue map entry not created.\r\n",
    "Interface index should not be configured for ACL based queue-map association.\r\n",
    "Egress interface index required for VLAN based queue-map association.\r\n",
    /* Sched Tbl ERROR CODE */
    "Scheduler entry not created.\r\n",
    "Scheduler Index is out of Range.\r\n",
    "Scheduler entry is not active.\r\n",
    "Invalid Scheduler Algorithm.\r\n",
    "Max No of Scheduler Table Entries are Configured.\r\n",
    "This Entry is referenced by Q or HierSched Table Entry.\r\n",
    "Improper scheduler configuration.\r\n",
    /* HS Tbl ERROR CODE */
    "Next-level scheduler/queue is Not Maped.\r\n",
    "Scheduler Self Reference.\r\n",
    "Max No of Hierarchy Scheduler Entries are Configured.\r\n",
    "Hierarchy Level Not Matched (SchedTbl - HierSchedTbl).\r\n",
    "Hierarchy entry not created.\r\n",
    "New Class cannot be set for Action with Drop.\r\n",
    "CPU Rate Limiting value is out of Range.\r\n",
    "MinRate Value should be lesser than configured MaxRate.\r\n",
    "MaxRate Value should be greater than configured MinRate.\r\n",
    "RD Cfg Gain value is out of range.\r\n",
    "Invalid RD Cfg Drop Threshold Type.\r\n",
    "RD Cfg ECN Thresh value is out of range.\r\n",
    "RD Cfg Invalid Action Flag Value.\r\n",
    "This Entry is referenced by RD Table Entry.\r\n",
    "Invalid priority value.\r\n",
    "Port-channel member cannot be configured to Sched or Q.\n\r",
    "Delete the active Scheduler from this port first.\r\n",
    "Default Scheduler cannot be modified.\r\n",
    "Invalid/Unsupported parameter to Queue.\r\n",
    "Invalid Vlan map value configured.\r\n",
    "Invalid Incoming Packet DEI value configured. \r\n",
    "Invalid Regenerated Color Value configured. \r\n",
    "Configured PCP entry exceeds Port Table Range. \r\n",
    "Entry is not avilable in Port Table \r\n",
    "Maximum Entries reached in Port Table \r\n",
    "Invalid PCP Packet Type configured \r\n",
    "Invalid PCP Selection row value configured\r\n",
    "Maximum DE value configured for PCP Model Selected.\r\n",
    "\r\n"

};
#else
extern CONST CHR1 *gapi1QoSCliErrString [];
#endif


/*****************************************************************************/
/* FileName : qoscli.c  functions prototype                                  */
/*****************************************************************************/

PUBLIC INT4 
cli_process_qos_cmd PROTO ((tCliHandle CliHandle, UINT4, ...));

INT4 
QoSCliSetSysControl PROTO ((tCliHandle CliHandle, INT4 i4SysControl));

INT4 
QoSCliSetStatus PROTO ((tCliHandle CliHandle, INT4 i4SysStatus));

INT4 
QoSCliShowGlobalInfo PROTO ((tCliHandle CliHandle));

/*****************************************************************************/
/*                  VLAN MAP TABLES FUNCTION                                 */
/*****************************************************************************/
INT4
VlanQueueingCliSetStatus PROTO ((tCliHandle CliHandle, INT4 i4SysStatus));

INT4
QoSCliAddVlanMapEntry PROTO ((tCliHandle CliHandle, UINT4 u4Idx));

INT4
QoSCliSetVlanMapParams PROTO ((tCliHandle CliHandle, UINT4 u4IfNum, UINT4 u4VlanId));

INT4
QoSCliShowVlanQMapEntry PROTO ((tCliHandle CliHandle, UINT4 u4Idx));

INT4
QoSCliDelVlanMapEntry PROTO ((tCliHandle CliHandle, UINT4 u4Idx));

INT4
QoSCliUtlDisplayVlanQMapEntry PROTO ((tCliHandle CliHandle, UINT4 u4Idx));

VOID
QoSCliRSVlanMapTblEntry PROTO ((UINT4 u4Idx, UINT4 u4CurIfNum,
                            UINT4 u4CurVlanId, INT4 i4RowStatus));

/*****************************************************************************/
/*                  PRIORITY MAP TABLE FUNCTIONS                             */
/*****************************************************************************/

INT4
QoSCliAddPriMapEntry PROTO ((tCliHandle CliHandle, UINT4 u4Idx));

INT4
QoSCliGetPriMapIndexFromName PROTO ((tSNMP_OCTET_STRING_TYPE *pInOctStrName,
                                     UINT4 *pu4Idx, UINT4 *pu4Status));
PUBLIC INT1
QoSPriMapPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1PromptStr));

PUBLIC INT1
QoSVlanQMapPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1PromptStr));


INT4
QoSCliSetPriMapParams PROTO ((tCliHandle CliHandle, UINT4 u4IfNum, 
                              UINT4 u4VlanId, UINT1 u1InPriType, 
                              UINT1 u1InPri, UINT1 u1TrafCls,
                              INT1 i1InnerReGenPri, INT4 i4InDEI,
                              INT4 i4RegenColor));
INT4
QoSCliNoSetPriMapParams PROTO ((tCliHandle CliHandle, UINT4 u4Interface, 
                                UINT4 u4Vlan, UINT4 u4InnerReGenPri));
INT4
QoSCliShowPriMapEntry PROTO ((tCliHandle CliHandle, UINT4 u4Idx));
INT4
QoSCliUtlDisplayPriMapEntry PROTO ((tCliHandle CliHandle, UINT4 u4Idx));
INT4 
QoSCliDelPriMapEntry PROTO ((tCliHandle CliHandle, UINT4 u4Idx));

VOID
QoSCliRSPriMapTblEntry PROTO ((UINT4 u4Idx, UINT4 u4CurIfNum, UINT4 u4CurVlanId,
                               UINT4 u4CurInPriType, UINT4 u4CurInPri,
                               UINT4 u4CurRegenPri, UINT4 u4CurInnerRegenPri, 
                               UINT4 u4CurInDEI, UINT4 u4CurRegenColor,
                               INT4 i4RowStatus));
VOID
QoSCliRSNoSetPriMapParams PROTO ((UINT4 u4Idx, UINT4 u4CurIfNum,
                                  UINT4 u4CurVlanId, UINT4 u4CurInRegenPri, 
                                  INT4 i4RowStatus));

/*****************************************************************************/
/*                     CLASS MAP TABLE FUNCTIONS                             */
/*****************************************************************************/
INT4 
QoSCliAddClsMapEntry PROTO ((tCliHandle CliHandle, UINT4 u4Idx));

INT4
QoSCliGetClsMapIndexFromName PROTO ((tSNMP_OCTET_STRING_TYPE *pInOctStrName,
                                     UINT4 *pu4Idx, UINT4 *pu4Status));
PUBLIC INT1
QoSClsMapPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1PromptStr));

INT4
QoSCliSetClsMapParams PROTO ((tCliHandle CliHandle, INT4 i4L2FltrId, 
                              INT4 i4L3FltrId, INT4 i4PriMapId, INT4 i4VlanMapId)); 
INT4
QoSCliShowClsMapEntry PROTO ((tCliHandle CliHandle, UINT4 u4Idx));

INT4
QoSCliUtlDisplayClsMapEntry PROTO ((tCliHandle CliHandle, UINT4 u4Idx));

INT4 
QoSCliDelClsMapEntry PROTO ((tCliHandle CliHandle, UINT4 u4Idx));

INT4
QoSCliSetClsMapClass PROTO ((tCliHandle CliHandle, UINT4 u4Class, 
                             INT4 i4PreColor, INT4 i4ReGenPri, UINT1 *pu1Name));

/*****************************************************************************/
/*                     CLASS2PRI MAP TABLE FUNCTIONS                         */
/*****************************************************************************/
INT4
QoSCliShowClsToPriMapEntry PROTO ((tCliHandle CliHandle, UINT1 *pu1Name));
INT4
QoSCliUtlDisplayClsToPriMapEntry PROTO ((tCliHandle CliHandle, UINT4 u4Idx));
INT4
QoSCliDelClsToPriMap PROTO ((tCliHandle CliHandle, UINT4 u4Class)); 

/*****************************************************************************/
/*                        METER TABLE FUNCTIONS                              */
/*****************************************************************************/
PUBLIC INT1
QoSMeterPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1PromptStr));

INT4
QoSCliGetMeterIndexFromName PROTO ((tSNMP_OCTET_STRING_TYPE *pInOctStrName,
                                    UINT4 *pu4Idx, UINT4 *pu4Status));
INT4 
QoSCliAddMeterEntry PROTO ((tCliHandle CliHandle, UINT4 u4Idx));

INT4 
QoSCliSetMeterParams PROTO ((tCliHandle CliHandle, UINT4 u4MeterType, 
                             UINT4 u4ColorMode, INT4 i4Interval, INT4 i4CIR,
                             INT4 i4CBS, INT4 i4EIR, INT4 i4EBS, 
                             INT4 i4NextMeter));
INT4
QoSCliUtlDisplayMeterEntry PROTO ((tCliHandle CliHandle, UINT4 u4Idx));

INT4
QoSCliShowMeterEntry PROTO ((tCliHandle CliHandle, UINT4 u4Idx));

INT4 
QoSCliDelMeterEntry PROTO ((tCliHandle CliHandle, UINT4 u4Idx));

VOID
QoSCliRSMeterTblEntry PROTO ((UINT4 u4Idx ,tQoSCliRSMeterParam *pQoSRSMetParam, 
                              INT4 i4RowStatus));


/*****************************************************************************/
/*                       PLYAMP TABLE FUNCTIONS                              */
/*****************************************************************************/
PUBLIC INT1
QoSPlyMapPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1PromptStr));
    
INT4 
QoSCliAddPlyMapEntry PROTO ((tCliHandle CliHandle, UINT4 u4Idx));

INT4
QoSCliGetPlyMapIndexFromName PROTO ((tSNMP_OCTET_STRING_TYPE *pInOctStrName,
                                     UINT4 *pu4Idx, UINT4 *pu4Status));

INT4
QoSCliSetPlyMapClass PROTO ((tCliHandle CliHandle, INT4 i4Class, 
                             UINT4 u4IfIndex, INT4 i4PriType, INT4 i4PriVal,
                             INT4 i4DEI));

INT4
QoSCliNoSetPlyIf PROTO ((tCliHandle CliHandle));

INT4
QoSCliSetPlyNoMeter PROTO ((tCliHandle CliHandle));

INT4
QoSCliShowPlyMapEntry PROTO ((tCliHandle CliHandle, UINT4 u4Idx));

INT4 
QoSCliDelPlyMapEntry PROTO ((tCliHandle CliHandle, UINT4 u4Idx));

INT4
QoSCliUtlDisplayPlyMapEntry PROTO ((tCliHandle CliHandle, UINT4 u4Idx));

INT4
QoSCliUtlGetPlyMapOutProAct PROTO ((tCliHandle CliHandle, UINT4 u4Idx));

INT4
QoSCliUtlGetPlyMapInProConfAct PROTO ((tCliHandle CliHandle, UINT4 u4Idx));

INT4
QoSCliUtlGetPlyMapInProExcAct PROTO ((tCliHandle CliHandle, UINT4 u4Idx));


INT4
QoSCliSetPlyMapMeterAct PROTO ((tCliHandle CliHandle, UINT4 u4MetIdx, 
                                UINT1 u1InProConAct, UINT1 u1InProExcAct,
                                UINT1 u1OutProAct, tQoSCliMeterAction *pMetAct));


VOID
QoSCliRSPlyMapClass PROTO ((UINT4 u4Idx, UINT4 u4Class, UINT4 u4IfIndex,
                           UINT4 u4PriType, UINT4 u4PriVal, UINT4 u4DEI,
                           INT4 i4RowStatus));

VOID
QoSCliRSPlyMapMeterAct PROTO ((UINT4 u4Idx, UINT4 u4MetIdx, UINT1 u1InProConAct,                                UINT1 u1InProExcAct, UINT1 u1OutProAct,
                               tQoSCliRSMetAct * pMetAct, INT4 i4RowStatus));


/*****************************************************************************/
/*                   Q TEMPLATE TABLE FUNCTIONS                              */
/*****************************************************************************/
PUBLIC INT1
QoSQTempPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1PromptStr));

INT4 
QoSCliAddQTempEntry PROTO ((tCliHandle CliHandle, UINT4 u4QTemp));

INT4 
QoSCliDelQTempEntry PROTO ((tCliHandle CliHandle, UINT4 u4QTemp));

INT4 
QoSCliSetQTempParams PROTO ((tCliHandle CliHandle, INT4 i4DropType,
                             INT4 i4QTempSize, INT4 i4DropAlgo));

INT4 
QoSCliSetRDCfgParams PROTO ((tCliHandle CliHandle, INT4 i4DP, INT4 i4MinTH,
                             INT4 i4MaxTH, INT4 i4MaxPktSize, INT4 i4MaxProb,
                             INT4 i4ExpWeight, INT4 i4Gain, INT4 i4DropTHType,
                             INT4 i4ECNTH, UINT1 u1Flag));
INT4 
QoSCliDelRDCfgEntry PROTO ((tCliHandle CliHandle, INT4 i4DP));

INT4
QoSCliShowQTempEntry PROTO ((tCliHandle CliHandle, UINT4 u4Idx));

INT4
QoSCliUtlDisplayQTempEntry PROTO ((tCliHandle CliHandle, UINT4 u4Idx));
    
VOID
QoSCliRSSetQTempParams PROTO ((UINT4 u4Idx, INT4 i4DropType, INT4 i4QTempSize,
                               INT4 i4DropAlgo, INT4 i4RowStatus));

VOID
QoSCliRSSetRDCfgParams PROTO ((UINT4 u4Idx, INT4 i4DP, INT4 i4MinTH,
                               INT4 i4MaxTH, INT4 i4MaxPktSize, 
                               INT4 i4MaxProb, INT4 i4ExpWeight,
                               INT4 i4RowStatus));

/*****************************************************************************/
/*                   SHAPE TEMPLATE TABLE FUNCTIONS                          */
/*****************************************************************************/
VOID
QoSCliRSShapeTempEntry PROTO((UINT4 Vu4Idx, UINT4 u4CurrCIR, UINT4 u4CurrCBS, 
                              UINT4 u4CurrEIR, UINT4 u4CurrEBS, 
                              INT4 i4RowStatus));
INT4
QoSCliAddShapeTempEntry PROTO ((tCliHandle CliHandle, UINT4 u4Idx, INT4 i4CIR,
                                INT4 i4CBS, INT4 i4EIR, INT4 i4EBS));
INT4
QoSCliDelShapeTempEntry PROTO ((tCliHandle CliHandle, UINT4 u4Idx));

INT4
QoSCliUtlDisplayShapeTempEntry PROTO ((tCliHandle CliHandle, UINT4 u4Idx));

INT4
QoSCliShowShapeTempEntry PROTO ((tCliHandle CliHandle, UINT4 u4Idx));

/*****************************************************************************/
/*                   SCHEDULER  TABLE FUNCTIONS                              */
/*****************************************************************************/
VOID
QoSCliRSSchedEntry PROTO ((INT4 i4IfIdx, UINT4 u4SchedIdx, INT4 i4CurSA,
                           UINT4 u4CurShapeId, UINT4 u4CurHL, 
                           INT4 i4RowStatus));
INT4
QoSCliAddSchedEntry PROTO ((tCliHandle CliHandle, UINT4 u4SchedIdx,
                            INT4 i4IfIdx, INT4 i4SchedAlgo, INT4 i4ShapeId,
                            INT4 i4HL));
INT4
QoSCliDelSchedEntry PROTO ((tCliHandle CliHandle, UINT4 u4SchedIdx, 
                            INT4 i4IfIdx));
INT4
QoSCliUtlDisplaySchedEntry PROTO ((tCliHandle CliHandle, INT4 i4IfIdx, 
                                   UINT4 u4SchedIdx));
INT4
QoSCliShowSchedEntry PROTO ((tCliHandle CliHandle, INT4 i4IfIdx));

/*****************************************************************************/
/*                         Q TABLE FUNCTIONS                                 */
/*****************************************************************************/
VOID
QoSCliRSQEntry PROTO ((tQoSCliQParam *pQCurParam, INT4 i4RowStatus));

INT4
QoSCliAddQEntry PROTO ((tCliHandle CliHandle, tQoSCliQParam *pQParam));

INT4
QoSCliDelQEntry PROTO ((tCliHandle CliHandle, UINT4 u4QIdx, INT4 i4IfIdx));

INT4
QoSCliUtlDisplayQEntry PROTO ((tCliHandle CliHandle, INT4 i4IfIdx, 
                               UINT4 u4QIdx));
INT4
QoSCliShowQEntry PROTO ((tCliHandle CliHandle, INT4 i4IfIdx));

/*****************************************************************************/
/*                        QMAP TABLE FUNCTIONS                               */
/*****************************************************************************/
VOID
QoSCliRSQMapEntry PROTO ((INT4 i4IfIdx, UINT4 u4CLASS, INT4 i4MapType, 
                          UINT4 u4PriVal, UINT4 u4CurQIdx, INT4 i4RowStatus));
INT4
QoSCliAddQMapEntry PROTO ((tCliHandle CliHandle, INT4 i4IfIdx, INT4 i4MapType,
                           UINT4 u4MapVal, UINT4 u4QIdx, INT4 i4DEIval));
INT4
QoSCliDelQMapEntry PROTO ((tCliHandle CliHandle, INT4 i4IfIdx, INT4 i4MapType,
                           UINT4 u4MapVal));
INT4
QoSCliUtlDisplayQMapEntry PROTO ((tCliHandle CliHandle, INT4 i4IfIdx, 
                                  UINT4 u4QIdx));
INT4
QoSCliShowQMapEntry PROTO ((tCliHandle CliHandle, INT4 i4IfIdx));

/*****************************************************************************/
/*                        HS   TABLE FUNCTIONS                               */
/*****************************************************************************/
VOID
QoSCliRSHSEntry PROTO ((tQoSCliHSParam *pHSCurParam, INT4 i4RowStatus));

INT4
QoSCliAddHSEntry PROTO ((tCliHandle CliHandle, tQoSCliHSParam *pHSParam));

INT4
QoSCliDelHSEntry PROTO ((tCliHandle CliHandle, INT4 i4IfIdx, UINT4 u4HL, 
                         UINT4 u4SchedId));
INT4
QoSCliUtlDisplayHSEntry PROTO ((tCliHandle CliHandle, INT4 i4IfIdx, UINT4 u4HL,
                                UINT4 u4SchedId));
INT4
QoSCliShowHSEntry PROTO ((tCliHandle CliHandle, INT4 i4IfIdx));

INT4
QoSCliSetPbitPreferenceOverDscp PROTO ((tCliHandle CliHandle, INT4 i4IfIdx, INT4 i4Pref));

INT4
QoSCliShowPbitPreference PROTO ((tCliHandle CliHandle, INT4 i4IfIdx));

/*****************************************************************************/
/*                        DUP  TABLE FUNCTIONS                               */
/*****************************************************************************/
INT4
QoSCliSetDUPEntry PROTO ((tCliHandle CliHandle, INT4 i4IfIdx, INT4 i4UDPri));

INT4
QoSCliShowDUPEntry PROTO ((tCliHandle CliHandle, INT4 i4IfIdx));

INT4
QoSCliUtlDisplayMetStatsEntry PROTO ((tCliHandle CliHandle, UINT4 u4Idx));

INT4
QoSCliShowMetStats PROTO ((tCliHandle CliHandle, UINT4 u4MeterId));

INT4
QoSCliUtlDisplayQStats PROTO ((tCliHandle CliHandle, INT4 i4IfIdx, 
                               UINT4 u4QIdx));
INT4
QoSCliShowQStats PROTO ((tCliHandle CliHandle, INT4 i4IfIdx));

INT4
QosCliSetTrace PROTO ((tCliHandle CliHandle,UINT4 u4Flag,UINT1 u1Flag));

INT4
QosCliSetMeterStatStatus PROTO ((tCliHandle CliHandle,UINT4 u4MeterId,INT4 i4StatStatus));

INT4
QosCliClearMeterStats PROTO((tCliHandle CliHandle,UINT4 u4MeterId));

/*****************************************************************************/
/*                     CPU RateLimit TABLE FUNCTIONS                         */
/*****************************************************************************/
INT4
QoSCliConfCpuRateLimit PROTO ((tCliHandle CliHandle, UINT4 u4CpuQId,
                               UINT4 u4MinRate, UINT4 u4MaxRate));

INT4
QoSCliShowCpuRateLimit PROTO ((tCliHandle CliHandle));

INT4
QoSCliShowDscptoQMap PROTO ((tCliHandle CliHandle));

INT4
QoSCliSetTrafficClassToPcp PROTO ((tCliHandle CliHandle,
                                  INT4 i4TrafficClass,
                                  INT4 i4Priority, INT4 i4PortId));
/*****************************************************************************/
/*                    PCP Port TABLE FUNCTIONS                               */
/*****************************************************************************/
INT4 
QoSCliSetPcpSelRow PROTO ((tCliHandle CliHandle, INT4 i4IfIndex,
                           INT4 i4PktType, INT4 i4PcpSelRow));
INT4 
QoSCliShowPcpSelRow PROTO ((tCliHandle CliHandle, INT4 i4IfIndex,
                            INT4 i4PktType));
INT4 
QoSCliUtlDisplayPortTblEntry PROTO ((tCliHandle CliHandle, INT4 i4IfIndex,
                                     INT4 i4PktType));
VOID
QoSCliRSPortTblEntry PROTO  ((INT4 i4Idx, INT4 i4PktType, INT4 i4PcpSelRow,
                              INT4 i4RowStatus));
INT4
QoSCliShowPcpEncodingTable PROTO ((tCliHandle CliHandle, INT4 i4Idx,
                                  INT4 i4PktType));

INT4
QoSCliShowPcpDecodingTable PROTO ((tCliHandle CliHandle, INT4 i4Idx,
                                  INT4 i4PktType));
INT4
QoSCliShowPcpQueueMapping PROTO ((tCliHandle CliHandle));    
/*****************************************************************************/
/*                        SHOW RUNNING CONFIG FUNCTIONS                      */
/*****************************************************************************/

INT4 QoSShowRunningConfig PROTO ((tCliHandle CliHandle));
INT4 QoSCliShowRunningConfigTables PROTO ((tCliHandle CliHandle));
INT4 QoSCliShowRunningConfigGlobal PROTO ((tCliHandle CliHandle));
INT4 QoSPriorityMapShowRunningConfig PROTO ((tCliHandle CliHandle));
INT4 QoSClassMapShowRunningConfig  PROTO ((tCliHandle CliHandle));
INT4 QoSMeterShowRunningConfig  PROTO ((tCliHandle CliHandle));
INT4 QoSPolicyMapShowRunningConfig  PROTO ((tCliHandle CliHandle));
INT4 QoSPolicyMapStatsShowRunningConfig  PROTO ((tCliHandle CliHandle));
INT4 QoSCliUtlGetPlyMapInProConfActShowRunConfig PROTO ((tCliHandle CliHandle, 
                                                          UINT4 u4Idx));
INT4 QoSCliUtlGetPlyMapInProExcActShowRunConfig PROTO ((tCliHandle CliHandle, 
                                                          UINT4 u4Idx));
INT4 QoSCliUtlGetPlyMapOutProActShowRunConfig PROTO ((tCliHandle CliHandle, 
                                                          UINT4 u4Idx));
INT4 QoSQueueTemplateShowRunningConfig PROTO ((tCliHandle CliHandle));
INT4 QoSCliDisplayRedConfShowRunConfig PROTO ((tCliHandle CliHandle, 
                                                          UINT4 u4Idx));      
INT4 QoSShapeTemplateShowRunningConfig PROTO ((tCliHandle CliHandle));
INT4 QoSSchedulerTemplateShowRunningConfig PROTO ((tCliHandle CliHandle));
INT4 QoSQueueTableShowRunningConfig PROTO ((tCliHandle CliHandle));
INT4 QoSQueueMapShowRunningConfig PROTO ((tCliHandle CliHandle));
INT4 QoSHierarchyTableShowRunningConfig PROTO ((tCliHandle CliHandle));
INT4 QoSDefaultUserPriShowRunningConfig PROTO ((tCliHandle CliHandle));
INT4 QoSCpuRateLimitShowRunningConfig PROTO ((tCliHandle CliHandle));
INT4 QoSVlanQMapShowRunningConfig PROTO ((tCliHandle CliHandle));
INT4 QoSPortTableShowRunningConfig PROTO ((tCliHandle CliHandle));
VOID IssQosShowDebugging PROTO ((tCliHandle CliHandle));
INT4 QoSShowRunningConfigInterfaceDetails (tCliHandle, INT4);
#endif /* __QOS_CLI_H__ */


    
