/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsfsbcli.h,v 1.3 2017/10/09 13:13:59 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIFsbContextId[13];
extern UINT4 FsMIFsbSystemControl[13];
extern UINT4 FsMIFsbModuleStatus[13];
extern UINT4 FsMIFsbFcMapMode[13];
extern UINT4 FsMIFsbFcmap[13];
extern UINT4 FsMIFsbHouseKeepingTimePeriod[13];
extern UINT4 FsMIFsbTraceOption[13];
extern UINT4 FsMIFsbTrapStatus[13];
extern UINT4 FsMIFsbClearStats[13];
extern UINT4 FsMIFsbDefaultVlanId[13];
extern UINT4 FsMIFsbRowStatus[13];
extern UINT4 FsMIFsbTraceSeverityLevel[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMIFsbSystemControl(u4FsMIFsbContextId ,i4SetValFsMIFsbSystemControl) \
 nmhSetCmn(FsMIFsbSystemControl, 13, FsMIFsbSystemControlSet, FsbLock, FsbUnLock, 0, 0, 1, "%u %i", u4FsMIFsbContextId ,i4SetValFsMIFsbSystemControl)
#define nmhSetFsMIFsbModuleStatus(u4FsMIFsbContextId ,i4SetValFsMIFsbModuleStatus) \
 nmhSetCmn(FsMIFsbModuleStatus, 13, FsMIFsbModuleStatusSet, FsbLock, FsbUnLock, 0, 0, 1, "%u %i", u4FsMIFsbContextId ,i4SetValFsMIFsbModuleStatus)
#define nmhSetFsMIFsbFcMapMode(u4FsMIFsbContextId ,i4SetValFsMIFsbFcMapMode) \
 nmhSetCmn(FsMIFsbFcMapMode, 13, FsMIFsbFcMapModeSet, FsbLock, FsbUnLock, 0, 0, 1, "%u %i", u4FsMIFsbContextId ,i4SetValFsMIFsbFcMapMode)
#define nmhSetFsMIFsbFcmap(u4FsMIFsbContextId ,pSetValFsMIFsbFcmap) \
 nmhSetCmn(FsMIFsbFcmap, 13, FsMIFsbFcmapSet, FsbLock, FsbUnLock, 0, 0, 1, "%u %s", u4FsMIFsbContextId ,pSetValFsMIFsbFcmap)
#define nmhSetFsMIFsbHouseKeepingTimePeriod(u4FsMIFsbContextId ,u4SetValFsMIFsbHouseKeepingTimePeriod) \
 nmhSetCmn(FsMIFsbHouseKeepingTimePeriod, 13, FsMIFsbHouseKeepingTimePeriodSet, FsbLock, FsbUnLock, 0, 0, 1, "%u %u", u4FsMIFsbContextId ,u4SetValFsMIFsbHouseKeepingTimePeriod)
#define nmhSetFsMIFsbTraceOption(u4FsMIFsbContextId ,i4SetValFsMIFsbTraceOption) \
 nmhSetCmn(FsMIFsbTraceOption, 13, FsMIFsbTraceOptionSet, FsbLock, FsbUnLock, 0, 0, 1, "%u %i", u4FsMIFsbContextId ,i4SetValFsMIFsbTraceOption)
#define nmhSetFsMIFsbTrapStatus(u4FsMIFsbContextId ,i4SetValFsMIFsbTrapStatus) \
 nmhSetCmn(FsMIFsbTrapStatus, 13, FsMIFsbTrapStatusSet, FsbLock, FsbUnLock, 0, 0, 1, "%u %i", u4FsMIFsbContextId ,i4SetValFsMIFsbTrapStatus)
#define nmhSetFsMIFsbClearStats(u4FsMIFsbContextId ,i4SetValFsMIFsbClearStats) \
 nmhSetCmn(FsMIFsbClearStats, 13, FsMIFsbClearStatsSet, FsbLock, FsbUnLock, 0, 0, 1, "%u %i", u4FsMIFsbContextId ,i4SetValFsMIFsbClearStats)
#define nmhSetFsMIFsbDefaultVlanId(u4FsMIFsbContextId ,i4SetValFsMIFsbDefaultVlanId) \
 nmhSetCmn(FsMIFsbDefaultVlanId, 13, FsMIFsbDefaultVlanIdSet, FsbLock, FsbUnLock, 0, 0, 1, "%u %i", u4FsMIFsbContextId ,i4SetValFsMIFsbDefaultVlanId)
#define nmhSetFsMIFsbRowStatus(u4FsMIFsbContextId ,i4SetValFsMIFsbRowStatus) \
 nmhSetCmn(FsMIFsbRowStatus, 13, FsMIFsbRowStatusSet, FsbLock, FsbUnLock, 0, 1, 1, "%u %i", u4FsMIFsbContextId ,i4SetValFsMIFsbRowStatus)
#define nmhSetFsMIFsbTraceSeverityLevel(u4FsMIFsbContextId ,i4SetValFsMIFsbTraceSeverityLevel) \
 nmhSetCmn(FsMIFsbTraceSeverityLevel, 13, FsMIFsbTraceSeverityLevelSet, FsbLock, FsbUnLock, 0, 0, 1, "%u %i", u4FsMIFsbContextId ,i4SetValFsMIFsbTraceSeverityLevel)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIFsbFIPSnoopingVlanIndex[13];
extern UINT4 FsMIFsbFIPSnoopingFcmap[13];
extern UINT4 FsMIFsbFIPSnoopingEnabledStatus[13];
extern UINT4 FsMIFsbFIPSnoopingRowStatus[13];
extern UINT4 FsMIFsbFIPSnoopingPinnedPorts[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMIFsbFIPSnoopingFcmap(u4FsMIFsbContextId , i4FsMIFsbFIPSnoopingVlanIndex ,pSetValFsMIFsbFIPSnoopingFcmap) \
 nmhSetCmn(FsMIFsbFIPSnoopingFcmap, 13, FsMIFsbFIPSnoopingFcmapSet, FsbLock, FsbUnLock, 0, 0, 2, "%u %i %s", u4FsMIFsbContextId , i4FsMIFsbFIPSnoopingVlanIndex ,pSetValFsMIFsbFIPSnoopingFcmap)
#define nmhSetFsMIFsbFIPSnoopingEnabledStatus(u4FsMIFsbContextId , i4FsMIFsbFIPSnoopingVlanIndex ,i4SetValFsMIFsbFIPSnoopingEnabledStatus) \
 nmhSetCmn(FsMIFsbFIPSnoopingEnabledStatus, 13, FsMIFsbFIPSnoopingEnabledStatusSet, FsbLock, FsbUnLock, 0, 0, 2, "%u %i %i", u4FsMIFsbContextId , i4FsMIFsbFIPSnoopingVlanIndex ,i4SetValFsMIFsbFIPSnoopingEnabledStatus)
#define nmhSetFsMIFsbFIPSnoopingRowStatus(u4FsMIFsbContextId , i4FsMIFsbFIPSnoopingVlanIndex ,i4SetValFsMIFsbFIPSnoopingRowStatus) \
 nmhSetCmn(FsMIFsbFIPSnoopingRowStatus, 13, FsMIFsbFIPSnoopingRowStatusSet, FsbLock, FsbUnLock, 0, 1, 2, "%u %i %i", u4FsMIFsbContextId , i4FsMIFsbFIPSnoopingVlanIndex ,i4SetValFsMIFsbFIPSnoopingRowStatus)
#define nmhSetFsMIFsbFIPSnoopingPinnedPorts(u4FsMIFsbContextId , i4FsMIFsbFIPSnoopingVlanIndex ,pSetValFsMIFsbFIPSnoopingPinnedPorts) \
 nmhSetCmn(FsMIFsbFIPSnoopingPinnedPorts, 13, FsMIFsbFIPSnoopingPinnedPortsSet, FsbLock, FsbUnLock, 0, 0, 2, "%u %i %s", u4FsMIFsbContextId , i4FsMIFsbFIPSnoopingVlanIndex ,pSetValFsMIFsbFIPSnoopingPinnedPorts)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIFsbIntfVlanIndex[13];
extern UINT4 FsMIFsbIntfIfIndex[13];
extern UINT4 FsMIFsbIntfPortRole[13];
extern UINT4 FsMIFsbIntfRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMIFsbIntfPortRole(i4FsMIFsbIntfVlanIndex , i4FsMIFsbIntfIfIndex ,i4SetValFsMIFsbIntfPortRole) \
 nmhSetCmn(FsMIFsbIntfPortRole, 13, FsMIFsbIntfPortRoleSet, FsbLock, FsbUnLock, 0, 0, 2, "%i %i %i", i4FsMIFsbIntfVlanIndex , i4FsMIFsbIntfIfIndex ,i4SetValFsMIFsbIntfPortRole)
#define nmhSetFsMIFsbIntfRowStatus(i4FsMIFsbIntfVlanIndex , i4FsMIFsbIntfIfIndex ,i4SetValFsMIFsbIntfRowStatus) \
 nmhSetCmn(FsMIFsbIntfRowStatus, 13, FsMIFsbIntfRowStatusSet, FsbLock, FsbUnLock, 0, 1, 2, "%i %i %i", i4FsMIFsbIntfVlanIndex , i4FsMIFsbIntfIfIndex ,i4SetValFsMIFsbIntfRowStatus)

#endif
