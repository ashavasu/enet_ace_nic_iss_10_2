/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: natcli.h,v 1.11 2015/03/12 02:26:42 siva Exp $
 *
 * Description:This file contains the exported definitions and
 *             macros of NAT
 *
 *******************************************************************/

#ifndef __NATCLI_H__
#define __NATCLI_H__

#include "cli.h"

/* The following are the commands to be used 
 */

/* CONFIG CMDS -S- */

#define CLI_SET_NAT_STATUS                      1
#define CLI_ADD_VIRTUAL_SERVER                  2
#define CLI_ENABLE_VIRTUAL_SERVER               3
#define CLI_DISABLE_VIRTUAL_SERVER              4
#define CLI_DEL_VIRTUAL_SERVER                  5
#define CLI_ADD_NAT_IFACE_ENTRY                 6
#define CLI_ADD_NAT_STATIC_ENTRY                7 
#define CLI_DEL_NAT_STATIC_ENTRY                8
#define CLI_ADD_PORT_TRIGGER                    9
#define CLI_DEL_PORT_TRIGGER                    10
#define CLI_ADD_NAT_GLOBAL_ENTRY                11
#define CLI_DEL_NAT_GLOBAL_ENTRY                12
#define CLI_ADD_PORT_RANGE_FORWARDING           13
#define CLI_DEL_PORT_RANGE_FORWARDING           14
#define CLI_NAT_STATUS_IFACE                    24 
#define CLI_SET_NAT_TIMEOUT                     27 
#define CLI_RESET_NAT_TIMEOUT                   46
#define CLI_SET_NAT_TRACE_LEVEL                 28
#define CLI_RESET_NAT_TRACE_LEVEL               29
#define CLI_SET_NAT_SIP_ALG_PORT                31
#define CLI_SET_NAT_SIP_ALG_PARTIAL_ENTRY_TIMER 43
#define CLI_ADD_NAT_STATIC_POLICY               32                      
#define CLI_DEL_NAT_STATIC_POLICY               33                      
#define CLI_ADD_NAT_DYNAMIC_POLICY              34                      
#define CLI_DEL_NAT_DYNAMIC_POLICY              35                  
/* CONFIG CMDS -E- */

/* SHOW CMDS -S- */

#define CLI_SHOW_NAT_CONFIG                     15
#define CLI_SHOW_NAT_GLOBAL_TABLE               16
#define CLI_SHOW_NAT_STATIC_TABLE               17
#define CLI_SHOW_NAT_TRANSLATIONS               18
#define CLI_SHOW_VIRTUAL_SERVERS                19
#define CLI_SHOW_PORT_RANGE_FORWARDING          20
#define CLI_SHOW_NAT_IFACE_CONFIG               21
#define CLI_SHOW_PORT_TRIGGER                   22
#define CLI_SHOW_PORT_TRIGGER_RESERVED          23
#define CLI_SHOW_SIP_NAT_STATICNAPT             25 
#define CLI_SHOW_SIP_NAT_PARTIAL                26
#define CLI_SHOW_NAT_SIP_ALG_PORT                   47
#define CLI_SHOW_NAT_SIP_ALG_PARTIAL_ENTRY_TIMER    59
#define CLI_SHOW_NAT_POLICY_TABLE               36
/* SHOW CMDS -E- */

/* CONFIG , SHOW  CMDS OPTIONS */

#define CLI_NAT_ENABLE                          1
#define CLI_NAT_DISABLE                         2

/*  Default Port Number for Protocols  */
#define NAT_AUTH_APP_PORT                       113
#define NAT_DNS_APP_PORT                        53
#define NAT_FTP_APP_PORT                        21
#define NAT_POP3_APP_PORT                       110
#define NAT_PPTP_APP_PORT                       1723
#define NAT_SMTP_APP_PORT                       25
#define NAT_TELNET_APP_PORT                     23
#define NAT_HTTP_APP_PORT                       80
#define NAT_NNTP_APP_PORT                       119
#define NAT_SNMP_APP_PORT                       161
#define NAT_OTHER_APP_PORT                      255
                                  
/* Port trigger Options Protocol Numbers */
#define NAT_PORT_TRIGGER_TCP                    6
#define NAT_PORT_TRIGGER_UDP                    17
#define NAT_PORT_TRIGGER_PROTO_ANY              255
                                  
/* Port Range Options Protocol Numbers */
#define NAT_PORT_RANGE_TCP                      6
#define NAT_PORT_RANGE_UDP                      17
#define NAT_PORT_RANGE_PROTO_ANY                255

/* Nat Time out Macros */
#define CLI_NAT_IDLE_TIMEOUT                    1 
#define CLI_NAT_TCP_TIMEOUT                     2 
#define CLI_NAT_UDP_TIMEOUT                     3 

/* natcli.c MACROS  */

#define NAT_CLI_MAX_COMMANDS_PARAMS       7
#define CLI_NAT_MAX_PORT_STRING_LEN       256
#define CLI_NAT_APP_NAME_LEN              64
#define CLI_NAT_DESCRN_LEN                20
#define CLI_NAT_MAX_APP_ALLOWED           32
#define NAT_VIRTUAL_SERVER_ALL            0 
/* cli prompt mode */
#define NAT_CLI_MODE                 "nat"


/* Used in natcli.c */

typedef struct _NatShowConfig
{
   INT4  i4Flag;
   UINT4 u4NumEntries;
   UINT4 u4LocalPortStart;
   UINT4 u4IdleTimeOut;
   UINT4 u4TcpTimeOut;
   UINT4 u4UdpTimeOut;
   UINT4 u4Trc;
}tNatGlobalConf;

typedef struct _NatStaticNaptEntry
{
   UINT4 u4Interface;
   UINT4 u4LocalIpAddr;
   UINT4 u4TranslatedIpAddr;
   UINT2 u2StartLocPort;
   UINT2 u2EndLocPort;
   UINT2 u2TranslatedPort;
   UINT2 u2ProtocolNumber;
   INT4  i4Status;
}tNatStaticNaptEntry;


enum {
    CLI_NAT_ERR_INVALID_VS_APPL = 1,
    CLI_NAT_ERR_WAN_IF_NOT_READY,
    CLI_NAT_ERR_INVALID_STATUS,
    CLI_NAT_ERR_INVALID_INTF,
    CLI_NAT_ERR_IP_CONFLICT,
    CLI_NAT_ERR_NO_PORT_ERR,
    CLI_NAT_ERR_AUTOIP_CFG_FAILURE,
    CLI_NAT_ERR_INVALID_LOCAL_IP,
    CLI_NAT_ERR_INVALID_GLOBAL_IP,
    CLI_NAT_ERR_IP_INVALID,
    CLI_NAT_ERR_INVALID_PROTOCOL,
    CLI_NAT_ERR_INVALID_PORT_RANGE,
    CLI_NAT_ERR_LACK_OF_RES,
    CLI_NAT_ERR_NAT_DOWN,
    CLI_NAT_ERR_ENTRY_NOT_ACTIVE,
    CLI_NAT_ERR_INVALID_ENTRY,
    CLI_NAT_ERR_INCONSISTENT_ENTRY,
    CLI_NAT_ERR_EXISTING_ENTRY,
    CLI_NAT_ERR_INVALID_INPUT,
    CLI_NAT_ERR_CONFLICTING_PORT_RANGE,
    CLI_NAT_ERR_SAME_GLOBAL_PORT,
    CLI_NAT_ERR_IP_NOT_SET_FOR_WAN,
    CLI_NAT_ERR_APP_NAME_CONFLICT,
    CLI_NAT_ERR_INVALID_APP_NAME_LEN,
    CLI_NAT_ERR_TABLE_EMPTY, 
    CLI_NAT_ERR_INVALID_CMD,
    CLI_NAT_ERR_INVALID_TCP_TIMEOUT_RANGE,
    CLI_NAT_ERR_INVALID_UDP_TIMEOUT_RANGE,
    CLI_NAT_ERR_INVALID_POLICY,
    CLI_NAT_ERR_INVALID_ACCESS_LIST,
    CLI_NAT_ERR_MAX_ERR
};

#ifdef __NATCLI_C__

CONST CHR1 *NatCliErrString [] = {
    NULL,
    "% Invalid virtual server application !\r\n",
    "% WAN Interface is not ready !\r\n",
    "% Invalid status !\r\n",
    "% Invalid interface !\r\n",
    "% IP address makes conflict !\r\n",
    "% Specify the port number !\r\n",
    "% Adding interface IP to global pool failed !\r\n",
    "% Invalid local IP !\r\n",
    "% Invalid Global IP !\r\n",
    "% Ip Address not in Local subnet !\r\n",
    "% Invalid protocol !\r\n",
    "% Invalid port number !\r\n",
    "% Table limit exceeds !\r\n",
    "% Nat is not UP !\r\n",
    "% Entry cannot be modified now !\r\n",
    "% Entry not found !\r\n",
    "% Entry cannot be deleted !\r\n",
    "% Entry exists !\r\n",
    "% Invalid entry !\r\n",
    "% Portrange overlaps the existing one !\r\n",
    "% The Global Port given, is already in use !\r\n",
    "% Ip address is not set for WAN interface !\r\n",
    "% Application Name Existing !\r\n",
    "% Invalid Application Name Length !\r\n",
    "% Table is Empty !\r\n", 
    "% Invalid command !\r\n",
    "% TCP Timeout range is 300 - 86400 !\r\n",
    "% UDP Timeout range is 300 - 86400 !\r\n",
    "% Invalid Policy Id !\r\n",
    "% Invalid access-list!\r\n",
    "\r\n"
};
#endif

/* Function Prototypes */

/* natcmd.def File related prototypes */

INT4 cli_process_nat_cmd           PROTO ((tCliHandle CliHandle,
                                           UINT4 u4Command, ...));

INT1 NatGetNatConfigPrompt         PROTO ((INT1 *pi1ModeName, 
                                           INT1 *pi1DispStr));

/* natcli.c File related prototypes */

INT4 NatCliSetGlobalNatStatus      PROTO ((tCliHandle, INT4 , UINT4));

INT4 NatCliAddVirtualServer        PROTO ((tCliHandle, UINT4, UINT4, 
                                           INT4, INT4, UINT2,
                                           tSNMP_OCTET_STRING_TYPE *));

INT1 NatCliEnableVirtualServer     PROTO ((tCliHandle, UINT4, UINT4, 
                                           INT4, INT4));

INT1 NatCliEnableAllVirtualServer  PROTO ((tCliHandle)); 

INT1 NatCliDisableAllVirtualServer PROTO ((tCliHandle)); 

INT1 NatCliDelAllVirtualServer PROTO ((tCliHandle, INT4)); 

INT1 NatCliDisableVirtualServer    PROTO ((tCliHandle, UINT4, UINT4, 
                                           INT4, INT4));

INT4 NatCliDelVirtualServer        PROTO ((tCliHandle, UINT4, UINT4, 
                                           INT4, INT4, INT4));
INT4 NatCliAddNatInterfaceEntry    PROTO ((tCliHandle, UINT4, INT4));

INT4 NatCliAddNatStaticEntry       PROTO ((tCliHandle, UINT4, UINT4, UINT4));

INT4 NatCliDelNatStaticEntry       PROTO ((tCliHandle, UINT4, UINT4));

INT4 NatCliAddPortTrigger          PROTO ((tCliHandle, UINT1 *, UINT1 *,
                                           UINT1 *, UINT2));

INT4 NatCliDelPortTrigger          PROTO ((tCliHandle, UINT1 *));

INT4 NatCliAddNatGlobalEntry       PROTO ((tCliHandle, UINT4, UINT4, UINT4));

INT4 NatCliDelNatGlobalEntry       PROTO ((tCliHandle, UINT4, UINT4));

INT4 NatCliAddPortRangeForwarding  PROTO ((tCliHandle, UINT4, UINT4, UINT4,
                                            INT4, INT4, UINT2, UINT2));

INT4 NatCliDelPortRangeForwarding  PROTO ((tCliHandle, UINT4, UINT4, INT4,
                                           INT4, UINT2));

INT4 NatCliSetInterfaceNatStatus  PROTO((tCliHandle,INT4, UINT4));


INT4 NatCliShowNatConfig           PROTO ((tCliHandle));

VOID NatCliFormatShowConfig        PROTO ((tCliHandle, tNatGlobalConf *));

INT4 NatCliShowGlobalTable         PROTO ((tCliHandle CliHandle));

INT4 NatCliShowStaticTable         PROTO ((tCliHandle CliHandle));

INT4 NatCliShowDynamicTable        PROTO ((tCliHandle CliHandle));

INT4 NatCliFormatStatus            PROTO ((tCliHandle, INT4, CONST CHR1 *));


INT4 NatCliShowVirtualServers      PROTO ((tCliHandle));

INT4 NatCliShowPortRangeForwarding PROTO ((tCliHandle));

INT4 NatCliProcessShowNatIfaceConfig PROTO ((tCliHandle));

INT4 NatCliShowPortTrigger          PROTO ((tCliHandle));

VOID NatCliShowPortTriggerReserved  PROTO ((tCliHandle));
INT4 NatCliSetNatTimeout PROTO ((tCliHandle CliHandle,UINT4 u4Option,
                                 INT4 i4TimeoutValue));

INT4 NatShowRunningConfig PROTO ((tCliHandle CliHandle,UINT4 u4Module));

INT4 NatShowRunningConfigScalar PROTO ((tCliHandle CliHandle));

INT4 NatShowRunningConfigInterface PROTO ((tCliHandle CliHandle));

INT4 NatShowRunningConfigInterfaceDetails PROTO ((tCliHandle CliHandle, INT4 i4Index));
VOID NatCliShowRunningConfigPolicy PROTO((tCliHandle ));

INT1 NatCliSipShowStaticNapt (tCliHandle);
INT1 NatCliSipShowPartial (tCliHandle);

INT4 NatCliShowSipAlgPort (tCliHandle);
INT4 NatCliShowSipAlgPartialEntryTimer (tCliHandle);
INT4 NatCliSetSipAlgPort (tCliHandle, UINT4);
INT4 NatCliSetSipAlgPartialEntryTimer (tCliHandle, UINT4);
VOID PrintPartialEntriesHdr (tCliHandle CliHandle);

INT4 NatCliSetDebugLevel PROTO ((tCliHandle CliHandle, 
                                      UINT4 u4TraceLevel));

INT4 NatCliResetDebugLevel PROTO ((tCliHandle CliHandle, 
                                        UINT4 u4TraceLevel));
VOID IssNatShowDebugging PROTO ((tCliHandle CliHandle));

INT4
NatCliUpdatePolicy PROTO((tCliHandle , INT4, INT4 , UINT4 ));
INT4
NatCliConfigurePolicy PROTO((tCliHandle CliHandle, INT4 i4PolicyType, 
                             INT4 i4PolicyId, UINT1 *pu1AclName, 
                             UINT1 u1Action));
INT4
NatCliShowPolicyTable PROTO((tCliHandle ));



#endif /* __NATCLI_H__ */

/* ------------------------------------------------------------------------ */
/*                        END natcli.h                                      */
/* ------------------------------------------------------------------------ */

