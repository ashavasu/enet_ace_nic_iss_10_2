/******************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: issszcli.h,v 1.2 2013/11/29 11:04:13 siva Exp $
 *
 * Description: This File contains the constants and prototypes used for
 *              processing the memory status CLI command
 *
 ******************************************************************************/
/****************************************************************************/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved                     */
/*                                                                          */
/*  FILE NAME             : issszcli.h                                      */
/*  SUBSYSTEM NAME        : isssz                                           */
/*  MODULE NAME           : isssz - Cli                                     */
/*  LANGUAGE              : C                                               */
/*  TARGET ENVIRONMENT    : Linux                                           */
/*  DATE OF FIRST RELEASE :                                                 */
/*  AUTHOR                : Aricent                                         */
/*  FUNCTIONS DEFINED(Applicable for Source files) :                        */
/*  DESCRIPTION           : This File contains the variables that are       */
/*                          used by CLI interface.                          */
/****************************************************************************/
/*                                                                          */
/*  Change History                                                          */
/*  Version               :                                                 */
/*  Date(DD/MM/YYYY)      :                                                 */
/*  Modified by           :                                                 */
/*  Description of change :                                                 */
/****************************************************************************/

#ifndef _ISSSZ_CLI_H_
#define _ISSSZ_CLI_H_

#include "lr.h"
#include "cli.h"

/* Maximum number of CLI Arguments */
#define ISSSZ_CLI_MAX_ARGS 3

/* Command Identifiers */
enum
{
 CLI_ISSSZ_SHOW_MOD_SIZE_ALL = 1,  /* 1  */
 CLI_ISSSZ_SHOW_MOD_SIZE,          /* 2  */ 
 CLI_ISSSZ_GEN_CSV_FILES,          /* 3  */
 CLI_ISSSZ_SHOW_MOD_NAME    /* 4  */ 
};

PUBLIC INT4 cli_process_isssz_cmd (tCliHandle, UINT4, ...);

#endif
