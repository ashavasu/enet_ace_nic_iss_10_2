/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: ppipcpcli.h,v 1.1 2014/12/16 10:51:24 siva Exp $
*
* Description: Header file for extern declarations of the mib objects in ppipcpfs.mib
*********************************************************************/
#ifndef __PPIPCPCLI_H__
#define __PPIPCPCLI_H__

/* extern declaration corresponding to OID variable present in protocol ppipcpdb.h */
extern UINT4 PppExtLinkConfigTxACFC[13];
extern UINT4 PppExtLinkConfigRxACFC[13];
extern UINT4 PppExtLinkConfigTxPFC[13];
extern UINT4 PppExtLinkConfigRxPFC[13];
extern UINT4 PppExtLinkConfigLowerIfType[13];
extern UINT4 PppExtLinkConfigHostName[13];
extern UINT4 PppExtKeepAliveTimeOut[13];
extern UINT4 PppExtIpLocToRemoteAddress[14];
extern UINT4 PppExtIpRemoteToLocAddress[14];
extern UINT4 PppExtIpAllowVJForPeer[14];
extern UINT4 PppExtIpLocAddressNegFlag[14];
extern UINT4 PppExtIpRemoteAddressNegFlag[14];
extern UINT4 PppExtIpLocToRemotePrimaryDNSAddress[14];
extern UINT4 PppExtIpLocToRemoteSecondaryDNSAddress[14];
extern UINT4 PppExtIpLocToRemotePrimaryNBNSAddress[14];
extern UINT4 PppExtIpLocToRemoteSecondaryNBNSAddress[14];
extern UINT4 PppExtIpRemoteToLocPrimaryDNSAddress[14];
extern UINT4 PppExtIpRemoteToLocSecondaryDNSAddress[14];
extern UINT4 PppExtIpRemoteToLocPrimaryNBNSAddress[14];
extern UINT4 PppExtIpRemoteToLocSecondaryNBNSAddress[14];
extern UINT4 PppExtIpLocPrimaryDNSAddressNegFlag[14];
extern UINT4 PppExtIpLocSecondaryDNSAddressNegFlag[14];
extern UINT4 PppExtIpLocPrimaryNBNSAddressNegFlag[14];
extern UINT4 PppExtIpLocSecondaryNBNSAddressNegFlag[14];
extern UINT4 PppExtIpAllowIPHCForPeer[14];
extern UINT4 PppExtIpRowStatus[14];
extern UINT4 PppExtIpConfigLocalMaxSlotId[14];
extern UINT4 PppExtIpConfigLocalCompSlotId[14];
extern UINT4 PppExtIpConfigTcpSpace[14];
extern UINT4 PppExtIpConfigNonTcpSpace[14];
extern UINT4 PppExtIpConfigFMaxPeriod[14];
extern UINT4 PppExtIpConfigFMaxTime[14];
extern UINT4 PppExtIpConfigMaxHeader[14];
extern UINT4 PppExtIpConfigRtpCompression[14];
extern UINT4 PppExtAddressPoolIndex[14];
extern UINT4 PppExtIPAddressPoolLowerRange[14];
extern UINT4 PppExtIPAddressPoolUpperRange[14];
extern UINT4 PppExtIPAddressPoolStatus[14];
extern UINT4 PppExtIPAddressPoolSelector[10];
extern UINT4 PppDebugLevelMask[10];

#endif /*__PPIPCPCLI_H__*/
