/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdmgmcli.h,v 1.1 2010/12/22 11:46:47 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 MgmdHostInterfaceIfIndex[11];
extern UINT4 MgmdHostInterfaceQuerierType[11];
extern UINT4 MgmdHostInterfaceStatus[11];
extern UINT4 MgmdHostInterfaceVersion[11];
extern UINT4 MgmdHostInterfaceVersion3Robustness[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetMgmdHostInterfaceStatus(i4MgmdHostInterfaceIfIndex , i4MgmdHostInterfaceQuerierType ,i4SetValMgmdHostInterfaceStatus)	\
	nmhSetCmn(MgmdHostInterfaceStatus, 11, MgmdHostInterfaceStatusSet, IgmpLock, IgmpUnLock, 0, 1, 2, "%i %i %i", i4MgmdHostInterfaceIfIndex , i4MgmdHostInterfaceQuerierType ,i4SetValMgmdHostInterfaceStatus)
#define nmhSetMgmdHostInterfaceVersion(i4MgmdHostInterfaceIfIndex , i4MgmdHostInterfaceQuerierType ,u4SetValMgmdHostInterfaceVersion)	\
	nmhSetCmn(MgmdHostInterfaceVersion, 11, MgmdHostInterfaceVersionSet, IgmpLock, IgmpUnLock, 0, 0, 2, "%i %i %u", i4MgmdHostInterfaceIfIndex , i4MgmdHostInterfaceQuerierType ,u4SetValMgmdHostInterfaceVersion)
#define nmhSetMgmdHostInterfaceVersion3Robustness(i4MgmdHostInterfaceIfIndex , i4MgmdHostInterfaceQuerierType ,u4SetValMgmdHostInterfaceVersion3Robustness)	\
	nmhSetCmn(MgmdHostInterfaceVersion3Robustness, 11, MgmdHostInterfaceVersion3RobustnessSet, IgmpLock, IgmpUnLock, 0, 0, 2, "%i %i %u", i4MgmdHostInterfaceIfIndex , i4MgmdHostInterfaceQuerierType ,u4SetValMgmdHostInterfaceVersion3Robustness)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 MgmdRouterInterfaceIfIndex[11];
extern UINT4 MgmdRouterInterfaceQuerierType[11];
extern UINT4 MgmdRouterInterfaceQueryInterval[11];
extern UINT4 MgmdRouterInterfaceStatus[11];
extern UINT4 MgmdRouterInterfaceVersion[11];
extern UINT4 MgmdRouterInterfaceQueryMaxResponseTime[11];
extern UINT4 MgmdRouterInterfaceProxyIfIndex[11];
extern UINT4 MgmdRouterInterfaceRobustness[11];
extern UINT4 MgmdRouterInterfaceLastMemberQueryInterval[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetMgmdRouterInterfaceQueryInterval(i4MgmdRouterInterfaceIfIndex , i4MgmdRouterInterfaceQuerierType ,u4SetValMgmdRouterInterfaceQueryInterval)	\
	nmhSetCmn(MgmdRouterInterfaceQueryInterval, 11, MgmdRouterInterfaceQueryIntervalSet, IgmpLock, IgmpUnLock, 0, 0, 2, "%i %i %u", i4MgmdRouterInterfaceIfIndex , i4MgmdRouterInterfaceQuerierType ,u4SetValMgmdRouterInterfaceQueryInterval)
#define nmhSetMgmdRouterInterfaceStatus(i4MgmdRouterInterfaceIfIndex , i4MgmdRouterInterfaceQuerierType ,i4SetValMgmdRouterInterfaceStatus)	\
	nmhSetCmn(MgmdRouterInterfaceStatus, 11, MgmdRouterInterfaceStatusSet, IgmpLock, IgmpUnLock, 0, 1, 2, "%i %i %i", i4MgmdRouterInterfaceIfIndex , i4MgmdRouterInterfaceQuerierType ,i4SetValMgmdRouterInterfaceStatus)
#define nmhSetMgmdRouterInterfaceVersion(i4MgmdRouterInterfaceIfIndex , i4MgmdRouterInterfaceQuerierType ,u4SetValMgmdRouterInterfaceVersion)	\
	nmhSetCmn(MgmdRouterInterfaceVersion, 11, MgmdRouterInterfaceVersionSet, IgmpLock, IgmpUnLock, 0, 0, 2, "%i %i %u", i4MgmdRouterInterfaceIfIndex , i4MgmdRouterInterfaceQuerierType ,u4SetValMgmdRouterInterfaceVersion)
#define nmhSetMgmdRouterInterfaceQueryMaxResponseTime(i4MgmdRouterInterfaceIfIndex , i4MgmdRouterInterfaceQuerierType ,u4SetValMgmdRouterInterfaceQueryMaxResponseTime)	\
	nmhSetCmn(MgmdRouterInterfaceQueryMaxResponseTime, 11, MgmdRouterInterfaceQueryMaxResponseTimeSet, IgmpLock, IgmpUnLock, 0, 0, 2, "%i %i %u", i4MgmdRouterInterfaceIfIndex , i4MgmdRouterInterfaceQuerierType ,u4SetValMgmdRouterInterfaceQueryMaxResponseTime)
#define nmhSetMgmdRouterInterfaceProxyIfIndex(i4MgmdRouterInterfaceIfIndex , i4MgmdRouterInterfaceQuerierType ,i4SetValMgmdRouterInterfaceProxyIfIndex)	\
	nmhSetCmn(MgmdRouterInterfaceProxyIfIndex, 11, MgmdRouterInterfaceProxyIfIndexSet, IgmpLock, IgmpUnLock, 0, 0, 2, "%i %i %i", i4MgmdRouterInterfaceIfIndex , i4MgmdRouterInterfaceQuerierType ,i4SetValMgmdRouterInterfaceProxyIfIndex)
#define nmhSetMgmdRouterInterfaceRobustness(i4MgmdRouterInterfaceIfIndex , i4MgmdRouterInterfaceQuerierType ,u4SetValMgmdRouterInterfaceRobustness)	\
	nmhSetCmn(MgmdRouterInterfaceRobustness, 11, MgmdRouterInterfaceRobustnessSet, IgmpLock, IgmpUnLock, 0, 0, 2, "%i %i %u", i4MgmdRouterInterfaceIfIndex , i4MgmdRouterInterfaceQuerierType ,u4SetValMgmdRouterInterfaceRobustness)
#define nmhSetMgmdRouterInterfaceLastMemberQueryInterval(i4MgmdRouterInterfaceIfIndex , i4MgmdRouterInterfaceQuerierType ,u4SetValMgmdRouterInterfaceLastMemberQueryInterval)	\
	nmhSetCmn(MgmdRouterInterfaceLastMemberQueryInterval, 11, MgmdRouterInterfaceLastMemberQueryIntervalSet, IgmpLock, IgmpUnLock, 0, 0, 2, "%i %i %u", i4MgmdRouterInterfaceIfIndex , i4MgmdRouterInterfaceQuerierType ,u4SetValMgmdRouterInterfaceLastMemberQueryInterval)

#endif
