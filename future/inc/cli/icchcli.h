/*******************************************************************
 * Copyright (C) 2015 Aricent Inc . All Rights Reserved
 *
 * $Id: icchcli.h,v 1.19.2.1 2018/03/16 13:43:58 siva Exp $
 *
 * Description: ICCH CLI dcommands specific defintions
 *
 *******************************************************************/
#ifndef _ICCHCLI_H
#define _ICCHCLI_H

#include "cli.h"
#include "icch.h"

#define ICCH_CLI_MAX_ARGS 6

#define ICCH_NO_SESSION_ID -1

#define ICCH_CLI_INST_MODE "iccl-inst-"

#define ICCH_CLI_SET_INST_ID(VlanId) CliSetModeInfo(VlanId)

#define ICCH_CLI_GET_INST_ID() CliGetModeInfo()

/* ICCH command identifiers */
enum {
    ICCH_CLI_SET_ICCH_IP_MASK = 1,
    ICCH_CLI_SET_STATS,
    ICCH_CLI_SHOW_DETAIL,
    ICCH_CLI_TRC_DBG,
    ICCH_CLI_TRC_NO_DBG,
    ICCH_CLI_CLEAR_STATS,
    ICCH_INST_ID,
    ICCH_NO_INST_ID,
    ICCH_CLI_SET_PROTOCOL_SYNC,
    ICCH_CLI_FETCH_REMOTE_FDB
};

/* CLI Trace Values */
enum {
    ICCH_CLI_INIT_SHUT_TRC = 1,
    ICCH_CLI_MGMT_TRC,
    ICCH_CLI_DATA_PATH_TRC,
    ICCH_CLI_CTRL_PATH_TRC,
    ICCH_CLI_PKT_DUMP_TRC,
    ICCH_CLI_OS_RES_TRC,
    ICCH_CLI_ALL_FAILURE_TRC,
    ICCH_CLI_BUFF_TRC,
    ICCH_CLI_CRITICAL_TRC,
    ICCH_CLI_TMR_TRC,
    ICCH_CLI_SOCKET_TRC,
    ICCH_CLI_SYNC_EVT_TRC,
    ICCH_CLI_SYNC_MSG_TRC,
    ICCH_CLI_ALL_TRC
};

/*ICCH Cli Values*/
enum {
ICCH_CLI_ICCH_INFO = 1,
ICCH_CLI_ICCH_STATS
};

#define ICCH_DEFAULT_IP_MASK 0xff000000
#define ICCH_DEFAULT_IP_ADDRESS 0xA9FE0101
#define ICCH_DEFAULT_INTERFACE ((const char *) "po100")

/* Error code values and strings are maintained
 *  * inside the protocol module itself.
 *   */
enum {
    CLI_ICCH_INVALID_DEBUG_TRC_LEVEL = 0,
    CLI_ICCH_INVALID_MODULE_DEBUG,
    CLI_ICCH_INVALID_PROTO_SYNC,
    CLI_ICCH_NO_PORT_CHNNEL_INT,
    CLI_ICCH_INVALID_PC_ID,
    CLI_ICCH_INVALID_IP_ADDR,
    CLI_ICCH_INVALID_SUBNET,
    CLI_ICCH_INVALID_VLAN,
    CLI_ICCH_INVALID_STAT,
    CLI_ICCH_INVALID_CLEAR_STAT,
    CLI_ICCH_INVALID_INST_ID,
    CLI_ICCH_DEL_DEFAULT_INST_ID,
    CLI_ICCH_SYNCUP_DISABLED,
    CLI_ICCH_PKT_DUMP_ENABLED,
    CLI_ICCH_IP_OVERLAP,
    CLI_ICCL_CONF_FILE_NOT_EXISTS,
    CLI_ICCH_MAX_ERR
};

/* The error strings should be places under the switch
 * so as to avoid redifinition.
 * This will be visible only in modulecli.c file
 */
#ifdef __ICCHCLI_C__
CONST CHR1  *IcchCliErrString [] = {
    "% Invalid debugging trace level\r\n",
    "% Invalid module debugging trace\r\n",
    "% Invalid protocol\r\n",
    "% Interface type should be port channel\r\n",
    "% Invalid port channel ID\r\n",
    "% Invalid IP address\r\n",
    "% Invalid subnet mask\r\n",
    "% Invalid VLAN ID\r\n",
    "% Statistics can either be enabled or disabled\r\n",
    "% Invalid clear statistics\r\n",
    "% Invalid instance ID\r\n",
    "% Default ICCL instance cannot be deleted\r\n",
    "% Remote FDB cannot be fetched when ICCH sync up is disabled\r\n",
    "% Packet-dump trace cannot be enabled with other trace. Disable it and enable the required trace flag\r\n",
    "% The IP address overlaps with an existing IP interface in system\r\n",
    "% iccl.conf file does not exist.Configuration can be done only after reboot",
    "\r\n"

};
#endif

INT4 CliProcessIcchCmd (tCliHandle CliHandle , UINT4 u4Command , ... );
UINT4
IcchCliSetIccIpMask (tCliHandle CliHandle, INT4 *pi4IpAddress, INT4 *pi4IpMask,
                            INT4 i4IfIndex, UINT4 u4IfType, UINT4 u4IfId, UINT4 u4VlanId);
UINT4 IcchCliSetIcchStatistics (tCliHandle CliHandle , INT4 i4SetStats );
UINT4 IcchCliShowIcchDetail (tCliHandle CliHandle , INT1 i1Value, INT4 i4SessionId);
UINT4 IcchCliDebugEnableDisable (tCliHandle CliHandle , UINT4 u4TrcVal , UINT1 u1DbgFlag );
UINT4
IcchCliClearStatistics (tCliHandle CliHandle);
UINT4
IcchShowRunningConfig (tCliHandle CliHandle);

UINT4
IcchShowRunningConfigTables (tCliHandle CliHandle);

UINT4
IcchCliInstanceMode (tCliHandle CliHandle,UINT4 u4InstId, UINT1 u1Mode);

INT1
IcchCliGetConfigPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

UINT4
IcchCliFetchRemoteFdb (tCliHandle CliHandle);


UINT4 IcchCliSetProtocolSync (tCliHandle CliHandle, UINT4 u4SyncStatus, UINT4 u4EnableStatus);

VOID
IssIcchShowDebugging (tCliHandle CliHandle);
#endif
