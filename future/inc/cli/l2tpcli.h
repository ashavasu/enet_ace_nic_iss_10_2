#ifndef __L2TPCLI_H__
#define __L2TPCLI_H__
#include "cli.h"

#define CLI_ISS_PW_CLASS_CREATE               1
#define CLI_ISS_PW_TYPE                       2
#define CLI_ISS_IPSEC_STATUS                  3
#define CLI_ISS_PW_TOS_STATUS                 4
#define CLI_ISS_PW_TTL                        5
#define CLI_ISS_SESSION_CREATE                6
#define CLI_ISS_SESSION_IDS                   7
#define CLI_ISS_L2TP_COOKIE_ENTITIES          8
#define CLI_ISS_SESSION_PW_CLASS              9
#define CLI_ISS_SESSION_SEQUENCE_MODE        10
#define CLI_ISS_XCONNECT_ID                  11
#define CLI_ISS_PW_RECONNECT                 12
#define CLI_ISS_PORT_ENCAPSULATION_TYPE      13
#define CLI_SHOW_L2TP_CONFIG                 14
#define CLI_SHOW_L2TP_INTERFACE              15 
#define CLI_SHOW_L2TP_SESSION                16
#define CLI_SHOW_L2TP_PW_CLASS               17
#define CLI_SHOW_L2TP_XCONNECT               18
#define CLI_SHOW_L2TP_GLOBAL_STATS           19
#define CLI_ISS_XCONNECT_DISABLE             20
#define CLI_SET_L2TP_TRACE_LEVEL             21
#define CLI_RESET_L2TP_TRACE_LEVEL           22
#define CLI_ISS_L2TP_STATUS                  23
#define CLI_ISS_PW_CLASS_DELETE              24
#define CLI_ISS_L2TP_SHUTDOWN                25
#define CLI_ISS_L2TP_NO_SHUTDOWN             26
#define CLI_ISS_L2TP_GLOBAL_STATUS           27
#define CLI_SHOW_L2TP_SESSION_STATS          28
#define CLI_SHOW_L2TP_INTERFACE_STATS        29
#define CLI_ISS_XCONNECT_PARAMS              30
#define CLI_CLEAR_L2TP_SESSION_STATS         31
#define CLI_CLEAR_L2TP_ALL_SESSION_STATS     32
#define CLI_CLEAR_L2TP_GLOBAL_STATS          33
#define CLI_ISS_L2TP_REMOTE_IP               34
#define CLI_ISS_XCONNECT_ID_CREATE           35
#define CLI_SET_L2TP_TRAP_LEVEL              36
#define CLI_SHOW_L2TP_STATS                  37
#define CLI_ISS_XCONNECT_SESSION             38
#define CLI_ISS_XCONNECT_ENCAP               39
#define CLI_ISS_SESSION_DELETE               40
#define CLI_ISS_L2TP_LOOPBACK                41
#define CLI_ISS_L2TP_LOCAL_IP                42
#define CLI_ISS_L2TP_COOKIE_NONE             43


#define CLI_L2TP_DBG_ALL                      0x0001FFFF
#define CLI_L2TP_DBG_KERNEL                   0x00001000
#define CLI_L2TP_DBG_XCONNECT                 0x00000800
#define CLI_L2TP_DBG_SESSION                  0x00000400
#define CLI_L2TP_DBG_PSEUDOWIRE               0x00000200
#define CLI_L2TP_DBG_INFO                     0x00000100
#define CLI_L2TP_DBG_PKT_DUMP                 0x00000080
#define CLI_L2TP_DBG_FAILURE                  0x00000040
#define CLI_L2TP_DBG_ENTRY_EXIT               0x00000020
#define CLI_L2TP_DBG_SNMP                     0x00000010
#define CLI_L2TP_DBG_EVENT                    0x00000008
#define CLI_L2TP_DBG_BUFFER                   0x00000004
#define CLI_L2TP_DBG_DATA_PATH                0x00000002
#define CLI_L2TP_DBG_INIT_SHUT                0x00000001
#define CLI_L2TP_DBG_DISABLE_ALL              0x00000000

#define CLI_L2TP_DBG_CRITICAL_LEVEL           0x00000003
#define CLI_L2TP_DBG_ALERT_LEVEL              0x00000012
#define CLI_L2TP_DBG_DEBUG_LEVEL              0x00000150
#define CLI_L2TP_DBG_INFO_LEVEL               0x00000700

#define CLI_ISS_L2TP_ENABLE                   1
#define CLI_ISS_L2TP_DISABLE                  2

#define L2TP_CLI_DISABLE_ALL_TRAPS  0
#define L2TP_CLI_FAILURE_TRAPS      1
#define L2TP_CLI_PROTOCOL_TRAPS     2
#define L2TP_CLI_ALL_TRAPS          3
#define L2TP_CLI_TRAP_MAX           3

enum {
    L2TP_CLI_ERR_SYS_CTRL = 1,
    L2TP_CLI_ERR_SYS_SHUTDOWN,
    L2TP_CLI_ERR_GLOBAL_SHUTDOWN,
    L2TP_CLI_ERR_NO_SUCH_PW,
    L2TP_CLI_ERR_NO_SUCH_SESSION,
    L2TP_CLI_ERR_INVALID_COOKIE_TYPE,
    L2TP_CLI_ERR_COOKIE_TYPE_VALUE_MISMATCH,
    L2TP_CLI_ERR_INVALID_LOCAL_COOKIE,
    L2TP_CLI_ERR_INVALID_REMOTE_COOKIE,
    L2TP_CLI_ERR_NO_SUCH_XCONNECT,
    L2TP_CLI_ERR_XCONNECT_MEM_FAILURE,
    L2TP_CLI_ERR_INVALID_XCONNECT_ID,
    L2TP_CLI_ERR_INVALID_REMOTE_END_ID,
    L2TP_CLI_ERR_INVALID_PSEUDOWIRE_ID,
    L2TP_CLI_ERR_INVALID_IP_ADDR,
    L2TP_CLI_ERR_INVALID_ENCAP_TYPE,
    L2TP_CLI_ERR_PORT_ENTRY_NOT_CREATED,
    L2TP_CLI_ERR_PORT_ENTRY_MEM_FAILURE,
    L2TP_CLI_ERR_ALREADY_SET,
    L2TP_CLI_ERR_PORT_ENTRY_ENABLED,
    L2TP_CLI_ERR_NO_LOOPBACK,
    L2TP_CLI_ERR_SESSION_RFE,
    L2TP_CLI_ERR_PW_RFE,
    L2TP_CLI_ERR_ASSOCIATED_WITH_AC,
    L2TP_CLI_ERR_AC_WITH_SAME_PORT_PROPERTIES,
    L2TP_CLI_ERR_SESSION_INACTIVE,
    L2TP_CLI_ERR_PW_INACTIVE,
    L2TP_CLI_ERR_PORT_INACTIVE,
    L2TP_CLI_ERR_MAX_PSEUDOWIRE,
    L2TP_CLI_ERR_MAX_SESSION,
    L2TP_CLI_ERR_MAX_XCONNECT,
    CLI_L2TP_MAX_ERR
};

#ifdef __L2TPCLI_C__
/*Maximum XConnect session*/
#define L2TP_MAX_XCONNECT_SESSION 100

/*Interface Index storing structure based upon the X-connect attachment circuit*/
typedef struct
{
     UINT4    u4l2tpXconnectIfIndex; /*Interface Index*/
}tL2tpCliXconnectInfo;

#define L2TP_MAX_MODES 4 /*CLI session mode(SSH,TELNET,CONSOLE)*/

/*L2TP macro declaration for CLI session mode*/
#define   L2TP_CLI_MODE_CONSOLE           1
#define   L2TP_CLI_MODE_TELNET            2
#define   L2TP_CLI_MODE_SSH               3

tL2tpCliXconnectInfo gaL2tpCliXconnectInfo[L2TP_MAX_MODES];

#endif

#ifdef __L2TPCLI_C__
CONST CHR1  *L2tpCliErrString [] = {
    NULL,
    "% System Control invalid input\r\n",
    "% L2tp is shutdown in the system\r\n",
    "% L2tp is disabled globally\r\n",
    "% Pseudowire doesnot exists with the given class index\r\n",
    "% Session doesnot exists with the given remote end index\r\n",
    "% Invalid cookie type\r\n!!!",
    "% Mistmatch between cookie value and cookie type configured\r\n",
    "% Invalid local cookie value!!!\r\n",
    "% Invalid remote cookie value!!!\r\n",
    "% Attachment ciruit doesnot exists with the given index\r\n",
    "% Memory allocation failed for attachment circuit\r\n",
    "% Invalid attachment circuit index!!!\r\n",
    "% Invalid session remote end index!!!\r\n",
    "% Invalid pseudowire class index!!!\r\n",
    "% Invalid IP Address configured!!!\r\n",
    "% Invalid encapsulation type of the port!!!\r\n",
    "% Port entry is not yet created\r\n",
    "% Memory allocation failed for port entry creation\r\n",
    "% Value already set\r\n",
    "% l2tp is already enabled on this port\r\n",
    "% loopback/Ip interface is not yet created\r\n",
    "% L2tp session is already referred by an Xconnect entry\r\n",
    "% Pseudowire is already reffered with Session entry\r\n",
    "% Port/Pseudowire is already Associated with an Attachment circuit kindly delete that before updation\r\n",
    "% An attachment circuit with same port property already created\r\n",
    "% Session is Inactive\r\n",
    "% Pseudowire is Inactive\r\n",
    "% L2tp Port entry  is Inactive\r\n",
    "% Maximum pseudowire already created\r\n",
    "% Maximum session already created\r\n",
    "% Maximum xconnect already created\r\n",
    "% Entry Not Found\r\n"

};
#endif /* __L2TPCLI_C__ */

#define PORT_ENCAPSULATION_TYPE_PORT_STR          "port based"
#define PORT_ENCAPSULATION_TYPE_PORT_VLAN_STR     "vlan based"
#define PORT_ENCAPSULATION_TYPE_QINQ_STR          "q-in-q based"
#define PORT_ENCAPSULATION_TYPE_QINANY_STR        "q-in-any based"    
/*enum {
        CLI_L2TP_MAX_ERR =  1
};*/

#define L2TP_CLI_CHECK_AND_THROW_FATAL_ERROR(CliHandle) ISSCliCheckAndThrowFatalError(CliHandle)

#define CLI_L2TP_MAX_ARGS 10

INT4 cli_process_l2tp_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command, ...));
INT4 CliConfigPwMode PROTO ((tCliHandle CliHandle));
INT4 CliConfigSessionMode PROTO ((tCliHandle CliHandle));
INT4 CliConfigXConnectMode PROTO((tCliHandle CliHandle));
INT1 CliGetPwCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);
INT1 CliGetSessionCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);
INT1 CliGetXconnectCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);
INT4 L2tpCliEnableXconnect PROTO ((tCliHandle CliHandle,INT4 i4IfIndex,
		 UINT4 u4XConnectId, UINT4 u4PeerIpAddr, 
		 UINT4 u4RemoteSessionId, UINT4 u4PwClassId));
INT4 L2tpCliDisableXconnect PROTO ((tCliHandle CliHandle,
                                INT4 i4IfIndex, UINT4 u4XConnectId));
INT4 L2tpCliSetXconnectEncapType PROTO ((tCliHandle CliHandle,
                                INT4 i4IfIndex, INT4 i4EncapType));
INT4 L2tpCliShowXConnect PROTO((tCliHandle CliHandle,
                                INT4 i4IfIndex, UINT4 u4XConnectId));
INT4
L2tpCliPortEnable (tCliHandle CliHandle,INT4 i4IfIndex,INT4 i4PortStatus);
INT4
L2tpCliPortDisable (tCliHandle CliHandle,INT4 i4IfIndex,INT4 i4PortStatus);
INT4
L2tpCliSetL2TpEnabledStatusOnPort (tCliHandle CliHandle,INT4 i4IfIndex,INT4 i4PortStatus);
INT4
L2tpCliShowInterface(tCliHandle CliHandle,INT4 i4PortIndex);
INT4
L2tpCliSetPwMode (tCliHandle CliHandle,INT4 i4PwIndex,INT4 i4PwMode);
INT4
L2tpCliSetPwIpsecStatus (tCliHandle CliHandle,INT4 i4PwIndex,INT4 i4PwIpsecStatus);
INT4
L2tpCliCreatePw (tCliHandle CliHandle,INT4 i4PwIndex);
INT4
L2tpCliShowPw(tCliHandle CliHandle,INT4 i4PwIndex);
INT4
L2tpCliDeletePw (tCliHandle CliHandle,INT4 i4PwIndex);

INT4
L2tpCliSetLocalIpAddr (tCliHandle CliHandle,INT4 i4PwIndex,UINT4 u4LocalIpAddr);


INT4
L2tpCliSetLoopBackName (tCliHandle CliHandle,INT4 i4PwIndex,INT4 i4LoopIndex);

INT4 L2tpCliUpdateSession (tCliHandle CliHandle, UINT4 u4SessionIndex);
INT4 L2tpCliSetLocalSessionId (tCliHandle CliHandle,  UINT4 u4SessionIndex, UINT4 u4LocalSessionIndex);
INT4 L2tpCliSetRemoteSessionId (tCliHandle CliHandle, UINT4 u4SessionIndex, UINT4 u4RemoteSessionIndex);
INT4 L2tpCliShowSession (tCliHandle CliHandle, UINT4 u4SessionIndex);

INT4
L2tpCliSetSessionPwClassIndex PROTO((tCliHandle CliHandle,UINT4 u4SessionIndex,
                               INT4 i4PwClassIndex));

INT4 
L2tpCliSetPeerIpAddr PROTO((tCliHandle CliHandle ,UINT4 i4PwClassIndex,UINT4 u4DestIpAddr ));

INT4
L2tpCliSetEncapType (tCliHandle CliHandle,INT4 i4IfIndex,INT4 i4EncapType);
INT4 L2tpCliSetCookieType (tCliHandle CliHandle, UINT4 u4SessionIndex, UINT4 u4LocalSessionIndex);
INT4 L2tpCliSetRemoteCookieValue (tCliHandle CliHandle, UINT4 u4SessionIndex, UINT1* pi1RemoteCookie);
INT4 L2tpCliSetLocalCookieValue (tCliHandle CliHandle, UINT4 u4SessionIndex, UINT1* pi1LocalCookie);
INT4 L2tpCliSetCookieValue (tCliHandle CliHandle, UINT4 u4SessionIndex, UINT1* pi1LocalCookie, UINT1* pi1RemoteCookie);
INT4
L2tpCliNoShutdown (tCliHandle CliHandle,INT4 i4SystemControlStatus);
INT4
L2tpCliShutdown (tCliHandle CliHandle,INT4 i4SystemControlStatus);

INT4
L2tpCliSetGlobalL2tpStatus (tCliHandle CliHandle,INT4 i4GlobalL2tpStatus);
INT4
L2tpCliShowGlobalConfig(tCliHandle CliHandle);
INT4
L2tpCliGlobalConfig(tCliHandle CliHandle);
INT4
L2tpShowRunningConfig(tCliHandle CliHandle);
INT4
L2tpShowRunningConfigScalar PROTO ((tCliHandle CliHandle));
INT4
L2tpShowRunningConfigTabular PROTO ((tCliHandle CliHandle));

INT4
L2tpCliXconnectSessionUpdate PROTO(( tCliHandle CliHandle,
                              INT4 i4XConnectIndex,
                              INT4 i4IfIndex,
                              UINT4 u4RemoteSessionId));
INT4
L2tpCliXconnectEncapUpdate PROTO((tCliHandle CliHandle,
                           INT4 i4XConnectIndex,INT4 i4IfIndex,
                           INT4 i4XConnectEncapType,INT4 i4VlanId,
                           INT4 i4SVlanId,INT4 i4CVlanId));
INT4
L2tpCliSetTrapLevel PROTO((tCliHandle CliHandle, UINT4 u4TrapLevel));

INT4
L2tpCliShowSessionStats (tCliHandle CliHandle, UINT4 u4SessionIndex);
INT4
L2tpCliShowGlobalStats (tCliHandle CliHandle);
INT4
L2tpCliClearGlobalStats (tCliHandle CliHandle);
INT4
L2tpCliClearAllSessionStats (tCliHandle CliHandle);
INT4
L2tpCliClearSessionStats (tCliHandle CliHandle, UINT4 u4SessionIndex);

INT4 L2tpCliXconnectCreate PROTO((tCliHandle CliHandle,INT4 i4IfIndex,
                 UINT4 u4XConnectId));
VOID
L2tpCliStoreXConnectIfIndex PROTO((UINT4 u4CliSessionMode, UINT4 u4XConnectIfIndex));

VOID
L2tpCliGetXConnectIfIndex PROTO((UINT4 u4CliSessionMode, UINT4 *pu4XConnectIfIndex));

INT4
L2tpCliDelXConnectIfIndex PROTO((UINT4 u4XConnectId));

INT4
L2tpCliSetTraceLevel PROTO((tCliHandle CliHandle, INT4 i4TraceLevel,
                             UINT1 u1TraceFlag));

INT4
L2tpCliDeleteSession PROTO((tCliHandle CliHandle, UINT4 u4SessionIndex));

INT4
L2tpCliCreateSession PROTO((tCliHandle CliHandle, UINT4 u4SessionIndex));

#endif
