/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdripcli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Rip2IfStatStatus[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetRip2IfStatStatus(u4Rip2IfStatAddress ,i4SetValRip2IfStatStatus)	\
	nmhSetCmn(Rip2IfStatStatus, 10, Rip2IfStatStatusSet, RipLock, RipUnLock, 0, 1, 1, "%p %i", u4Rip2IfStatAddress ,i4SetValRip2IfStatStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Rip2IfConfDomain[10];
extern UINT4 Rip2IfConfAuthType[10];
extern UINT4 Rip2IfConfAuthKey[10];
extern UINT4 Rip2IfConfSend[10];
extern UINT4 Rip2IfConfReceive[10];
extern UINT4 Rip2IfConfDefaultMetric[10];
extern UINT4 Rip2IfConfStatus[10];
extern UINT4 Rip2IfConfSrcAddress[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetRip2IfConfDomain(u4Rip2IfConfAddress ,pSetValRip2IfConfDomain)	\
	nmhSetCmn(Rip2IfConfDomain, 10, Rip2IfConfDomainSet, RipLock, RipUnLock, 0, 0, 1, "%p %s", u4Rip2IfConfAddress ,pSetValRip2IfConfDomain)
#define nmhSetRip2IfConfAuthType(u4Rip2IfConfAddress ,i4SetValRip2IfConfAuthType)	\
	nmhSetCmn(Rip2IfConfAuthType, 10, Rip2IfConfAuthTypeSet, RipLock, RipUnLock, 0, 0, 1, "%p %i", u4Rip2IfConfAddress ,i4SetValRip2IfConfAuthType)
#define nmhSetRip2IfConfAuthKey(u4Rip2IfConfAddress ,pSetValRip2IfConfAuthKey)	\
	nmhSetCmn(Rip2IfConfAuthKey, 10, Rip2IfConfAuthKeySet, RipLock, RipUnLock, 0, 0, 1, "%p %s", u4Rip2IfConfAddress ,pSetValRip2IfConfAuthKey)
#define nmhSetRip2IfConfSend(u4Rip2IfConfAddress ,i4SetValRip2IfConfSend)	\
	nmhSetCmn(Rip2IfConfSend, 10, Rip2IfConfSendSet, RipLock, RipUnLock, 0, 0, 1, "%p %i", u4Rip2IfConfAddress ,i4SetValRip2IfConfSend)
#define nmhSetRip2IfConfReceive(u4Rip2IfConfAddress ,i4SetValRip2IfConfReceive)	\
	nmhSetCmn(Rip2IfConfReceive, 10, Rip2IfConfReceiveSet, RipLock, RipUnLock, 0, 0, 1, "%p %i", u4Rip2IfConfAddress ,i4SetValRip2IfConfReceive)
#define nmhSetRip2IfConfDefaultMetric(u4Rip2IfConfAddress ,i4SetValRip2IfConfDefaultMetric)	\
	nmhSetCmn(Rip2IfConfDefaultMetric, 10, Rip2IfConfDefaultMetricSet, RipLock, RipUnLock, 0, 0, 1, "%p %i", u4Rip2IfConfAddress ,i4SetValRip2IfConfDefaultMetric)
#define nmhSetRip2IfConfStatus(u4Rip2IfConfAddress ,i4SetValRip2IfConfStatus)	\
	nmhSetCmn(Rip2IfConfStatus, 10, Rip2IfConfStatusSet, RipLock, RipUnLock, 0, 1, 1, "%p %i", u4Rip2IfConfAddress ,i4SetValRip2IfConfStatus)
#define nmhSetRip2IfConfSrcAddress(u4Rip2IfConfAddress ,u4SetValRip2IfConfSrcAddress)	\
	nmhSetCmn(Rip2IfConfSrcAddress, 10, Rip2IfConfSrcAddressSet, RipLock, RipUnLock, 0, 0, 1, "%p %p", u4Rip2IfConfAddress ,u4SetValRip2IfConfSrcAddress)

#endif
