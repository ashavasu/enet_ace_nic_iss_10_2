/********************************************************************
 *                                                                  *
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved             *
 * $Id: rmn2cli.h,v 1.4 2015/07/27 07:00:41 siva Exp $              *
 * Description: This file contains the data structure and macros    *
 *              for RMON2 CLI module                                *
 *                                                                  *
 *******************************************************************/

#ifndef _RMN2CLI_H_
#define _RMN2CLI_H_

#include "lr.h"
#include "cli.h"


#define RMON2_MAX_ARGS 5



/* Command Identifiers */
enum{
    RMON2_CLI_STATUS = 1,                /*1*/
    RMON2_CLI_SHOW_TRACE,                /*2*/
    RMON2_CLI_TRACE,                     /*3*/
    RMON2_CLI_RESET_TRACE                /*4*/
};

PUBLIC INT4 cli_process_rmon2_cmd (tCliHandle , UINT4 , ...);
PUBLIC INT4 Rmon2CliSetModuleStatus (tCliHandle , INT4);
PUBLIC INT4 Rmon2CliSetTrace (tCliHandle , INT4 );
PUBLIC INT4 Rmon2CliResetTrace (tCliHandle, INT4);
PUBLIC VOID Rmon2CliShowTrace (tCliHandle);
PUBLIC VOID Rmon2ShowRunningConfig (tCliHandle); 

/* Max number of error strings that can be set */
#define CLI_RMON2_MAX_ERR           8

#define RMON2_ENABLE               1
#define RMON2_DISABLE              2
#define RMON2_CLI_TRACE_FUNC_ENTRY 0x00000001
#define RMON2_CLI_TRACE_FUNC_EXIT  0x00000002
#define RMON2_CLI_TRACE_CRITICAL   0x00000004
#define RMON2_CLI_TRACE_MEM_FAIL   0x00000008
#define RMON2_CLI_TRACE_DEBUG      0x00000010
#define RMON2_CLI_TRACE_ALL        0x0000001F


/*Error Strings
enum{

};

#ifdef __RMN2CLI_C__
CONST CHR1  *Rmon2CliErrString [] = {
    NULL,
    "Unknown Error\r\n",
    "\r\n"
};
#endif

*/
#endif /* _RMN2CLI_H_ */
