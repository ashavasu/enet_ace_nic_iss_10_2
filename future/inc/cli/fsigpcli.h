/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsigpcli.h,v 1.2 2008/10/31 13:09:30 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsIgmpProxyStatus[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsIgmpProxyStatus(i4SetValFsIgmpProxyStatus)	\
	nmhSetCmn(FsIgmpProxyStatus, 10, FsIgmpProxyStatusSet, IgmpLock, IgmpUnLock, 0, 0, 0, "%i", i4SetValFsIgmpProxyStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsIgmpProxyRtrIfaceIndex[12];
extern UINT4 FsIgmpProxyRtrIfaceCfgOperVersion[12];
extern UINT4 FsIgmpProxyRtrIfacePurgeInterval[12];
extern UINT4 FsIgmpProxyRtrIfaceRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsIgmpProxyRtrIfaceCfgOperVersion(i4FsIgmpProxyRtrIfaceIndex ,i4SetValFsIgmpProxyRtrIfaceCfgOperVersion)	\
	nmhSetCmn(FsIgmpProxyRtrIfaceCfgOperVersion, 12, FsIgmpProxyRtrIfaceCfgOperVersionSet, IgmpLock, IgmpUnLock, 0, 0, 1, "%i %i", i4FsIgmpProxyRtrIfaceIndex ,i4SetValFsIgmpProxyRtrIfaceCfgOperVersion)
#define nmhSetFsIgmpProxyRtrIfacePurgeInterval(i4FsIgmpProxyRtrIfaceIndex ,i4SetValFsIgmpProxyRtrIfacePurgeInterval)	\
	nmhSetCmn(FsIgmpProxyRtrIfacePurgeInterval, 12, FsIgmpProxyRtrIfacePurgeIntervalSet, IgmpLock, IgmpUnLock, 0, 0, 1, "%i %i", i4FsIgmpProxyRtrIfaceIndex ,i4SetValFsIgmpProxyRtrIfacePurgeInterval)
#define nmhSetFsIgmpProxyRtrIfaceRowStatus(i4FsIgmpProxyRtrIfaceIndex ,i4SetValFsIgmpProxyRtrIfaceRowStatus)	\
	nmhSetCmn(FsIgmpProxyRtrIfaceRowStatus, 12, FsIgmpProxyRtrIfaceRowStatusSet, IgmpLock, IgmpUnLock, 0, 1, 1, "%i %i", i4FsIgmpProxyRtrIfaceIndex ,i4SetValFsIgmpProxyRtrIfaceRowStatus)

#endif
