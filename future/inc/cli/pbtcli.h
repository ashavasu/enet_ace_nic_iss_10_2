 /******************************************************************** 
  * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
  * 
  * $Id: pbtcli.h,v 1.7 2013/06/26 11:11:56 siva Exp $ 
  * 
  * Description: This file contains PBBTE cli definitions 
  ********************************************************************/ 

#ifndef __PBBTECLI_H__
#define __PBBTECLI_H__

#include "cli.h"

/* 
 * PBBTE CLI command constants. To add a new command ,a new definition needs to
 * be added to the list.
 */
enum {
 CLI_PBBTE_SHUT = 1,
 CLI_PBBTE_NO_SHUT,
 CLI_PBBTE_MAP_ESP_VLAN,
 CLI_PBBTE_UNMAP_ESP_VLAN,
 CLI_PBBTE_TE_SID,
 CLI_PBBTE_NO_TE_SID,
 CLI_PBBTE_MAP_ESP_TESID,
 CLI_PBBTE_UNMAP_ESP_TESID,
 CLI_PBBTE_DEBUGS,
 CLI_PBBTE_NO_DEBUGS,
    CLI_PBBTE_GLOBAL_DEBUGS,
    CLI_PBBTE_NO_GLOBAL_DEBUGS,
 CLI_PBBTE_SHOW_ESP_VLANS,
    CLI_PBBTE_SHOW_TE_STATUS,
    CLI_PBBTE_SHOW_TESID_ESPID,
    CLI_PBBTE_SHOW_TESID_ESPID_DETAIL,
    CLI_PBBTE_SHOW_TESID_DETAIL,
    CLI_PBBTE_SHOW_TESID,
    CLI_PBBTE_SHOW_TESI_ENTRY_DETAIL,
    CLI_PBBTE_SHOW_TESI_ENTRY,
};

enum {
    PBBTE_HW_FAIL = 1,
    PBBTE_INVALID_PORTS,
    PBBTE_VLAN_NOT_PRESENT,
    PBBTE_CONTEXT_INVALID,
    PBBTE_VLAN_NOT_ACTIVE,
    PBBTE_MST_INST_MAPPING_FAILURE,
    PBBTE_INVALID_VLAN_ID,
    PBBTE_MODULE_SHUTDOWN,
    PBBTE_VLAN_NOT_AN_ESP_VLAN,
    PBBTE_NULL_MAC_ADDR,
    PBBTE_BCAST_MAC_ADDR,
    PBBTE_SRC_MCAST_ADDR_ERR,
    PBBTE_ESP_VLAN_PRESENT_ERR,
    PBBTE_TESID_NOT_FOUND_ERR,
    PBBTE_TESID_ALREADY_PRESENT_ERR,
    PBBTE_MAX_ERR
};
#ifdef __PBTCLI_C__
CONST CHR1  *PbbTeCliErrString [] = {
    NULL,
    " Hardware Configuration Failed.\r\n",
    " Invalid Ports specified.\r\n",
    " Vlan NOT present.\r\n",
    " Invalid Context.\r\n",
    " Vlan is NOT active\r\n",
    " Mapping Esp Vlan to TE-MSTID Instance Failed\r\n",
    " Invalid VLAN ID \r\n",
    " PBBTE Module is not started\r\n",
    " Vlan specified is not an ESP Vlan\r\n",
    " Null Mac-Address Specified\r\n",
    " Broadcast Mac-Address Specified\r\n",
    " Source Mac-Address Specified is Multicast Mac-Address\r\n",
    " Vlan is already configured as a ESP vlan\r\n",
    " TeSid Entry not found\r\n",
    " TeSid Entry is already mapped to some other context\r\n",
    "\r\n"
};
#endif


#define PBBTE_UCAST_COUNTERS 1
#define PBBTE_MCAST_COUNTERS 2
#define PBBTE_TRC_TYPE_ALL   3

#define PBBTE_MAX_ARGS           20
#define CLI_PBBTE_TESI_MODE          "tesi-"

#define PBBTE_SET_CMD            1
#define PBBTE_NO_CMD             2

INT4
cli_process_pbbte_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);

INT4
PbbTeCliSetSystemControl (tCliHandle CliHandle, UINT4 u4ContextId, 
     INT4 i4Status);

INT4
PbbTeCliConfigEspVlans (tCliHandle CliHandle, UINT4 u4ContextId, 
   UINT1 *pau1VlanList, UINT1 u1Action);
INT4
PbbTeCliConfigMaxP2PEsps (tCliHandle CliHandle, UINT4 );

INT4
PbbTeCliConfigMaxP2MPEsps (tCliHandle CliHandle, UINT4 );

INT4
PbbTeCliConfigMaxTeSidEntry (tCliHandle CliHandle, UINT4 );

INT4
PbbTeCliConfigTesid (tCliHandle CliHandle, UINT4 u4TeSidId);

INT4
PbbTeCliDeleteTesiEntry (tCliHandle CliHandle, UINT4 u4TeSidId);

INT4
PbbTeCliConfigTeSidEspEntry (tCliHandle CliHandle, UINT4 u4ContextId, 
        UINT4 u4TeSidId, UINT4 u4EspId, tMacAddr SrcAddr,
        tMacAddr DstAddr, UINT2 u2EspVid);
INT4
PbbTeCliDeleteTeSidEspEntry (tCliHandle CliHandle, UINT4 u4TeSidId, 
        UINT4 u4EspId);
#ifdef TRACE_WANTED 
INT4
PbbTeCliSetDebugs (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4DbgValue,
  UINT1 u1Action);
INT4
PbbTeCliSetGlobalDebugs (tCliHandle CliHandle, UINT1 u1Action);
#endif
INT4 
PbbTeCliShowModuleStatusInCtxt (tCliHandle CliHandle, UINT4 u4ContextId);

INT4
PbbTeCliShowModuleGlobalStatus (tCliHandle CliHandle);

INT4
PbbTeCliShowEspVlanIDsInCtxt (tCliHandle CliHandle, UINT4 u4ContextId);

INT4
PbbTeCliShowEspIDsGlobal (tCliHandle CliHandle);

INT4
PbbTeCliShowTesidEspIdDetail (tCliHandle, UINT4 u4TeSid, UINT4 u4EspID);

INT4
PbbTeCliShowTesidEspID (tCliHandle, UINT4 u4TeSid, UINT4 u4EspID);

INT4
PbbTeCliShowTesidDetail (tCliHandle CliHandle, UINT4 u4TeSid);

INT4
PbbTeCliShowTeSid (tCliHandle CliHandle, UINT4 u4TeSid);

INT4
PbbTeCliShowTeSiTableDetail(tCliHandle CliHandle);

INT4
PbbTeCliShowTeSiTable (tCliHandle CliHandle);

VOID
PbbTeCliPrintEspVlanList (tCliHandle CliHandle, INT4 i4CommaCount, 
     UINT2 VlanId);

INT4
PbbTeCliDisplayOctetToVlan (tCliHandle CliHandle, UINT1 *pOctet, 
    UINT4 u4OctetLen, INT4 bSeprtr, 
    INT4 *pi4CommaCount);
VOID
PbbTeCliHandleVlanFailures (INT4 i4ReturnValue);

VOID
PbbTeCliVlanFailurePrintMesg (tCliHandle, INT4 i4ReturnValue);

INT1
PbbTeGetVcmPbbTeCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

VOID
PbbTeCliDisplayTeSidEntry (tCliHandle CliHandle, UINT4 u4TeSid, UINT4 u4EspId);

VOID 
PbbTeCliPrintTeSidEntInDetail (tCliHandle CliHandle, UINT4 u4TeSid, 
          UINT4 u4EspId);
VOID
PbbTeCliMacToStr (UINT1 *pMacAddr, UINT1 *pu1Temp);

VOID
IssPbbTeShowDebugging(tCliHandle CliHandle);

#endif/*__PBBTECLI_H__*/
