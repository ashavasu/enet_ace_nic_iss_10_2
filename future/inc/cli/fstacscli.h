/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fstacscli.h,v 1.1 2015/04/28 11:45:05 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsTacClntExtActiveServerAddressType[11];
extern UINT4 FsTacClntExtActiveServer[11];
extern UINT4 FsTacClntExtTraceLevel[11];
extern UINT4 FsTacClntExtRetransmit[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsTacClntExtActiveServerAddressType(i4SetValFsTacClntExtActiveServerAddressType)	\
	nmhSetCmn(FsTacClntExtActiveServerAddressType, 11, FsTacClntExtActiveServerAddressTypeSet, TAC_PROT_LOCK, TAC_PROT_UNLOCK, 0, 0, 0, "%i", i4SetValFsTacClntExtActiveServerAddressType)
#define nmhSetFsTacClntExtActiveServer(pSetValFsTacClntExtActiveServer)	\
	nmhSetCmn(FsTacClntExtActiveServer, 11, FsTacClntExtActiveServerSet, TAC_PROT_LOCK, TAC_PROT_UNLOCK, 0, 0, 0, "%s", pSetValFsTacClntExtActiveServer)
#define nmhSetFsTacClntExtTraceLevel(u4SetValFsTacClntExtTraceLevel)	\
	nmhSetCmn(FsTacClntExtTraceLevel, 11, FsTacClntExtTraceLevelSet, TAC_PROT_LOCK, TAC_PROT_UNLOCK, 0, 0, 0, "%u", u4SetValFsTacClntExtTraceLevel)
#define nmhSetFsTacClntExtRetransmit(i4SetValFsTacClntExtRetransmit)	\
	nmhSetCmn(FsTacClntExtRetransmit, 11, FsTacClntExtRetransmitSet, TAC_PROT_LOCK, TAC_PROT_UNLOCK, 0, 0, 0, "%i", i4SetValFsTacClntExtRetransmit)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsTacClntExtServerAddressType[13];
extern UINT4 FsTacClntExtServerAddress[13];
extern UINT4 FsTacClntExtServerStatus[13];
extern UINT4 FsTacClntExtServerSingleConnect[13];
extern UINT4 FsTacClntExtServerPort[13];
extern UINT4 FsTacClntExtServerTimeout[13];
extern UINT4 FsTacClntExtServerKey[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsTacClntExtServerStatus(i4FsTacClntExtServerAddressType , pFsTacClntExtServerAddress ,i4SetValFsTacClntExtServerStatus)	\
	nmhSetCmn(FsTacClntExtServerStatus, 13, FsTacClntExtServerStatusSet, TAC_PROT_LOCK, TAC_PROT_UNLOCK, 0, 1, 2, "%i %s %i", i4FsTacClntExtServerAddressType , pFsTacClntExtServerAddress ,i4SetValFsTacClntExtServerStatus)
#define nmhSetFsTacClntExtServerSingleConnect(i4FsTacClntExtServerAddressType , pFsTacClntExtServerAddress ,i4SetValFsTacClntExtServerSingleConnect)	\
	nmhSetCmn(FsTacClntExtServerSingleConnect, 13, FsTacClntExtServerSingleConnectSet, TAC_PROT_LOCK, TAC_PROT_UNLOCK, 0, 0, 2, "%i %s %i", i4FsTacClntExtServerAddressType , pFsTacClntExtServerAddress ,i4SetValFsTacClntExtServerSingleConnect)
#define nmhSetFsTacClntExtServerPort(i4FsTacClntExtServerAddressType , pFsTacClntExtServerAddress ,i4SetValFsTacClntExtServerPort)	\
	nmhSetCmn(FsTacClntExtServerPort, 13, FsTacClntExtServerPortSet, TAC_PROT_LOCK, TAC_PROT_UNLOCK, 0, 0, 2, "%i %s %i", i4FsTacClntExtServerAddressType , pFsTacClntExtServerAddress ,i4SetValFsTacClntExtServerPort)
#define nmhSetFsTacClntExtServerTimeout(i4FsTacClntExtServerAddressType , pFsTacClntExtServerAddress ,i4SetValFsTacClntExtServerTimeout)	\
	nmhSetCmn(FsTacClntExtServerTimeout, 13, FsTacClntExtServerTimeoutSet, TAC_PROT_LOCK, TAC_PROT_UNLOCK, 0, 0, 2, "%i %s %i", i4FsTacClntExtServerAddressType , pFsTacClntExtServerAddress ,i4SetValFsTacClntExtServerTimeout)
#define nmhSetFsTacClntExtServerKey(i4FsTacClntExtServerAddressType , pFsTacClntExtServerAddress ,pSetValFsTacClntExtServerKey)	\
	nmhSetCmn(FsTacClntExtServerKey, 13, FsTacClntExtServerKeySet, TAC_PROT_LOCK, TAC_PROT_UNLOCK, 0, 0, 2, "%i %s %s", i4FsTacClntExtServerAddressType , pFsTacClntExtServerAddress ,pSetValFsTacClntExtServerKey)

#endif
