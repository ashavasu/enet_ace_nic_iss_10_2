 /* $Id: aclcxecli.h,v 1.7 2015/10/05 12:21:07 siva Exp $ */
#ifndef __ACLCXECLI_H__
#define __ACLCXECLI_H__
#include "cli.h"
#include "fssnmp.h"

#define ISS_MAX_LEN 255
#define ISS_ADDR_LEN 22

#define HOST_MASK "255.255.255.255"
enum {
    CLI_IP_ACL =1,
    CLI_NO_IP_ACL,
    CLI_MAC_ACL,
    CLI_NO_MAC_ACL,
    CLI_ACL_PERMIT,
    CLI_ACL_DENY,
    CLI_ACL_PERMIT_PROTO,
    CLI_ACL_DENY_PROTO,
    CLI_ACL_PERMIT_TCP,
    CLI_ACL_DENY_TCP,
    CLI_ACL_PERMIT_UDP,
    CLI_ACL_DENY_UDP,
    CLI_ACL_PERMIT_ICMP,
    CLI_ACL_DENY_ICMP,
    CLI_PERMIT_MAC_ACL,
    CLI_DENY_MAC_ACL,
    CLI_ACL_MAC_ACCESS_GRP,
    CLI_ACL_MAC_NO_ACCESS_GRP,
    CLI_ACL_SHOW, 
    CLI_ACL_MAX_COMMANDS
};

/* Command identifiers for ip access-list command */
#define ACL_STANDARD 1
#define ACL_EXTENDED 2

/* Command identifiers for permit/deny command */
#define ACL_ANY                  1
#define ACL_HOST_IP              2
#define ACL_HOST_IP_MASK         3 

#define ACL_SRC                  1
#define ACL_DST                 2
/* Command identifiers for access list commands */
#define ACL_HOST_MAC             2


/* Command identifier for permit tcp command */
#define ACL_GREATER_THAN_PORT        1
#define ACL_LESSER_THAN_PORT         2
#define ACL_EQUAL_TO_PORT            3
#define ACL_RANGE_PORT               4

/* Command identifier to distinguish ack or rst in permit tcp command */
#define ACL_ACK                  1
#define ACL_RST                  2

/* Command identifier to distinguish in or out in access group command */
#define ACL_ACCESS_IN            1
#define ACL_ACCESS_OUT           2

/* Command identifier to distinguish in or out in access group command */
#define AARP                    0x80F3 
#define AMBER                   0x6008 
#define DEC_SPANNING            0x8138
#define DECNET_IV               0x6003
#define DIAGNOSTIC              0x6005
#define DSM                     0x8039
#define ETYPE_6000              0x6000
#define ETYPE_8042              0x8042
#define LAT                     0x6004
#define LAVC_SCA                0x6007
#define MOP_CONSOLE             0x6002
#define MOP_DUMP                0x6001
#define MSDOS                   0x8041
#define MUMPS                   0x6009 
#define NET_BIOS                0x8040 
#define VINES_ECHO              0x0BAF 
#define VINES_IP                0x0BAD
#define XNS_ID                  0x0807

#define ISS_PROT_ICMP      1
#define ISS_PROT_IGMP      2
#define ISS_PROT_GGP       3
#define ISS_PROT_IP        4
#define ISS_PROT_TCP       6
#define ISS_PROT_EGP       8
#define ISS_PROT_IGP       9
#define ISS_PROT_NVP       11
#define ISS_PROT_UDP       17
#define ISS_PROT_IRTP      28
#define ISS_PROT_IDPR      35
#define ISS_PROT_RSVP      46
#define ISS_PROT_MHRP      48
#define ISS_PROT_IGRP      88
#define ISS_PROT_OSPFIGP   89
#define ISS_PROT_PIM       103
#define ISS_PROT_ANY       255


/* ICMP message type - IP filter */
#define ISS_ECHO_REPLY     0
#define ISS_DEST_UNREACH   3
#define ISS_SRC_QUENCH     4
#define ISS_REDIRECT       5
#define ISS_ECHO_REQ       8
#define ISS_TIME_EXCEED    11
#define ISS_PARAM_PROB     12
#define ISS_TIMEST_REQ     13
#define ISS_TIMEST_REP     14
#define ISS_INFO_REQ       15
#define ISS_INTO_REP       16
#define ISS_ADD_MK_REQ     17
#define ISS_ADD_MK_REP     18
#define ISS_NO_ICMP_TYPE   255

/* ICMP message code - IP filter */
#define ISS_NET_UNREACH    0
#define ISS_HOST_UNREACH   1
#define ISS_PROT_UNREACH   2
#define ISS_PORT_UNREACH   3
#define ISS_FRG_NEED       4
#define ISS_SRC_RT_FAIL    5
#define ISS_DST_NET_UNK    6
#define ISS_DST_HOST_UNK   7
#define ISS_SRC_HOST_ISO   8
#define ISS_DST_NET_ADMP   9
#define ISS_DST_HOST_ADMP  10
#define ISS_NET_UNRE_TOS   11
#define ISS_HOST_UNRE_TOS  12
#define ISS_NO_ICMP_CODE   255

#define ISS_PORTLIST_LEN   BRG_PHY_PORT_LIST_SIZE

#define ISS_DEFAULT_INDEX 0
#define DEFAULT_ENABLE 1
#define ISS_ERROR  -1
#define CLI_ACL_MAX_ARGS 18 
#define ACL_MAX_NAME_LENGTH 16
#define  CLI_STDACL_MODE "StdAcl"
#define  CLI_EXTACL_MODE "ExtAcl"
#define  CLI_MACACL_MODE "MacAcl"

/* Error codes  *
 * Error code values and strings are maintained inside the protocol module itself.
 * */
enum {
        CLI_ACL_ERR = 0,
        CLI_ACL_FILTER_NO_CHANGE,
        CLI_ACL_FILTER_INPORT_PRESENT_ERR,
        CLI_ACL_FILTER_OUTPORT_PRESENT_ERR,
        CLI_ACL_FILTER_CREATION_FAILED,
        CLI_ACL_MAX_ERR
};


/* The error strings should be places under the switch so as to avoid redifinition
 *  * This will be visible only in modulecli.c file
 *   */
#ifdef __ACLCLI_C__

CONST CHR1  *AclCliErrString [] = {
        NULL,
        "% This filter is associated with some class-map in Diffserv\r\n",
        "% This filter already has an input port\r\n",
        "% This filter already has an output port\r\n",
        "% Filter creation failed!!!. Conflicting "
           "filter may exist with same priority \r\n"
};
#endif


INT4 cli_process_acl_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Comamnd, ...));

INT1 StdAclGetCfgPrompt PROTO ((INT1 *, INT1 *));

INT1 ExtAclGetCfgPrompt PROTO ((INT1 *, INT1 *));

INT1 MacAclGetCfgPrompt PROTO ((INT1 *, INT1 *));

INT4  AclCreateIPFilter PROTO ((tCliHandle, UINT4, INT4 ));

INT4  AclDestroyIPFilter PROTO ((tCliHandle,  INT4 ));

INT4 AclCreateMacFilter PROTO ((tCliHandle , INT4 ));

INT4 AclDestroyMacFilter PROTO ((tCliHandle , INT4 ));

INT4 AclStdIpFilterConfig PROTO 
((tCliHandle, INT4,UINT4, UINT4 ,UINT4 ,UINT4, UINT4, UINT4));

INT4 AclExtIpFilterConfig PROTO 
((tCliHandle, INT4, INT4, UINT4, UINT4, UINT4, UINT4, UINT4, UINT4, INT4, INT4,
INT4));

INT4 AclExtIpFilterTcpUdpConfig PROTO ((tCliHandle, INT4 i4Action, INT4 i4Protocol,
  UINT4 u4SrcType , UINT4 u4SrcIpAddr,  UINT4 u4SrcIpMask, 
  UINT4 u4SrcFlag, UINT4 u4SrcMinPort,  UINT4 u4SrcMaxRangePort,
  UINT4 u4DestType, UINT4  u4DestIpAddr,UINT4 u4DestIpMask,
  UINT4 u4DestFlag, UINT4 u4DestMinPort ,UINT4  u4DestMaxRangePort, 
  UINT4 u4BitType , INT4 i4Tos, INT4 i4Dscp, INT4 i4Vlan));

 INT4 AclExtIpFilterIcmpConfig PROTO
 ((tCliHandle, INT4 i4Action, UINT4,UINT4 , UINT4, UINT4, UINT4, UINT4 , INT4, INT4, INT4));

 INT4  AclMacAccessGroup PROTO ((tCliHandle, INT4 , INT4));
  
 INT4 AclNoMacAccessGroup PROTO ((tCliHandle, INT4 i4FilterNo, INT4 ));
 
 INT4 AclExtMacFilterConfig PROTO ((tCliHandle CliHandle, INT4 i4Action,
                       UINT4 u4SrcType, tMacAddr SrcMacAddr,
                       UINT4 u4DestType ,tMacAddr DestMacAddr,
                       INT4 i4Protocol, INT4 i4Encap,
                       UINT4 u4VlanId));

INT4 AclShowAccessLists PROTO ((tCliHandle CliHandle,INT4 i4FilterType,INT4 i4FilterNo));
INT4 AclShowL2Filter PROTO ((tCliHandle CliHandle,INT4 i4NextFilter));
INT4 AclShowL3Filter PROTO ((tCliHandle CliHandle,INT4 i4NextFilter));

INT4 AclShowRunningConfig(tCliHandle,UINT4);
VOID AclShowRunningConfigTables(tCliHandle);
VOID AclShowRunningConfigInterfaceDetails(tCliHandle,INT4);

INT4 AclTestIpParams PROTO ((UINT1 ,INT4 i4FilterNo,UINT4 u4SrcType,UINT4 u4SrcIpAddr, UINT4 u4SrcMask ));
                                           
INT4 AclSetIpParams PROTO ((UINT1 ,INT4 i4FilterNo,UINT4 u4SrcType,UINT4 u4SrcIpAddr, UINT4 u4SrcMask ));

#endif
