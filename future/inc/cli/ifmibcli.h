/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: ifmibcli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IfAdminStatus[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIfAdminStatus(i4IfIndex ,i4SetValIfAdminStatus)	\
	nmhSetCmn(IfAdminStatus, 10, IfAdminStatusSet, CfaLock, CfaUnlock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValIfAdminStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IfLinkUpDownTrapEnable[11];
extern UINT4 IfPromiscuousMode[11];
extern UINT4 IfAlias[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIfLinkUpDownTrapEnable(i4IfIndex ,i4SetValIfLinkUpDownTrapEnable)	\
	nmhSetCmn(IfLinkUpDownTrapEnable, 11, IfLinkUpDownTrapEnableSet, CfaLock, CfaUnlock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValIfLinkUpDownTrapEnable)
#define nmhSetIfPromiscuousMode(i4IfIndex ,i4SetValIfPromiscuousMode)	\
	nmhSetCmn(IfPromiscuousMode, 11, IfPromiscuousModeSet, CfaLock, CfaUnlock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValIfPromiscuousMode)
#define nmhSetIfAlias(i4IfIndex ,pSetValIfAlias)	\
	nmhSetCmn(IfAlias, 11, IfAliasSet, CfaLock, CfaUnlock, 0, 0, 1, "%i %s", i4IfIndex ,pSetValIfAlias)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IfStackHigherLayer[11];
extern UINT4 IfStackLowerLayer[11];
extern UINT4 IfStackStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIfStackStatus(i4IfStackHigherLayer , i4IfStackLowerLayer ,i4SetValIfStackStatus)	\
	nmhSetCmn(IfStackStatus, 11, IfStackStatusSet, CfaLock, CfaUnlock, 0, 1, 2, "%i %i %i", i4IfStackHigherLayer , i4IfStackLowerLayer ,i4SetValIfStackStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IfRcvAddressAddress[11];
extern UINT4 IfRcvAddressStatus[11];
extern UINT4 IfRcvAddressType[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIfRcvAddressStatus(i4IfIndex , pIfRcvAddressAddress ,i4SetValIfRcvAddressStatus)	\
	nmhSetCmn(IfRcvAddressStatus, 11, IfRcvAddressStatusSet, CfaLock, CfaUnlock, 0, 1, 2, "%i %s %i", i4IfIndex , pIfRcvAddressAddress ,i4SetValIfRcvAddressStatus)
#define nmhSetIfRcvAddressType(i4IfIndex , pIfRcvAddressAddress ,i4SetValIfRcvAddressType)	\
	nmhSetCmn(IfRcvAddressType, 11, IfRcvAddressTypeSet, CfaLock, CfaUnlock, 0, 0, 2, "%i %s %i", i4IfIndex , pIfRcvAddressAddress ,i4SetValIfRcvAddressType)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IfTestId[11];
extern UINT4 IfTestStatus[11];
extern UINT4 IfTestType[11];
extern UINT4 IfTestOwner[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIfTestId(i4IfIndex ,i4SetValIfTestId)	\
	nmhSetCmn(IfTestId, 11, IfTestIdSet, CfaLock, CfaUnlock, 1, 0, 1, "%i %i", i4IfIndex ,i4SetValIfTestId)
#define nmhSetIfTestStatus(i4IfIndex ,i4SetValIfTestStatus)	\
	nmhSetCmn(IfTestStatus, 11, IfTestStatusSet, CfaLock, CfaUnlock, 1, 0, 1, "%i %i", i4IfIndex ,i4SetValIfTestStatus)
#define nmhSetIfTestType(i4IfIndex ,pSetValIfTestType)	\
	nmhSetCmn(IfTestType, 11, IfTestTypeSet, CfaLock, CfaUnlock, 1, 0, 1, "%i %o", i4IfIndex ,pSetValIfTestType)
#define nmhSetIfTestOwner(i4IfIndex ,pSetValIfTestOwner)	\
	nmhSetCmn(IfTestOwner, 11, IfTestOwnerSet, CfaLock, CfaUnlock, 1, 0, 1, "%i %s", i4IfIndex ,pSetValIfTestOwner)

#endif
