/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsdh6rcli.h,v 1.5 2014/06/23 11:25:49 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDhcp6RlyDebugTrace[11];
extern UINT4 FsDhcp6RlyTrapAdminControl[11];
extern UINT4 FsDhcp6RlySysLogAdminStatus[11];
extern UINT4 FsDhcp6RlyListenPort[11];
extern UINT4 FsDhcp6RlyClientTransmitPort[11];
extern UINT4 FsDhcp6RlyServerTransmitPort[11];
extern UINT4 FsDhcp6RlyOption37Control[11];
extern UINT4 FsDhcp6RlyPDRouteControl[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDhcp6RlyDebugTrace(pSetValFsDhcp6RlyDebugTrace) \
 nmhSetCmn(FsDhcp6RlyDebugTrace, 11, FsDhcp6RlyDebugTraceSet, D6RlMainLock, D6RlMainUnLock, 0, 0, 0, "%s", pSetValFsDhcp6RlyDebugTrace)
#define nmhSetFsDhcp6RlyTrapAdminControl(pSetValFsDhcp6RlyTrapAdminControl) \
 nmhSetCmn(FsDhcp6RlyTrapAdminControl, 11, FsDhcp6RlyTrapAdminControlSet, D6RlMainLock, D6RlMainUnLock, 0, 0, 0, "%s", pSetValFsDhcp6RlyTrapAdminControl)
#define nmhSetFsDhcp6RlySysLogAdminStatus(i4SetValFsDhcp6RlySysLogAdminStatus) \
 nmhSetCmn(FsDhcp6RlySysLogAdminStatus, 11, FsDhcp6RlySysLogAdminStatusSet, D6RlMainLock, D6RlMainUnLock, 0, 0, 0, "%i", i4SetValFsDhcp6RlySysLogAdminStatus)
#define nmhSetFsDhcp6RlyListenPort(i4SetValFsDhcp6RlyListenPort) \
 nmhSetCmn(FsDhcp6RlyListenPort, 11, FsDhcp6RlyListenPortSet, D6RlMainLock, D6RlMainUnLock, 0, 0, 0, "%i", i4SetValFsDhcp6RlyListenPort)
#define nmhSetFsDhcp6RlyClientTransmitPort(i4SetValFsDhcp6RlyClientTransmitPort) \
 nmhSetCmn(FsDhcp6RlyClientTransmitPort, 11, FsDhcp6RlyClientTransmitPortSet, D6RlMainLock, D6RlMainUnLock, 0, 0, 0, "%i", i4SetValFsDhcp6RlyClientTransmitPort)
#define nmhSetFsDhcp6RlyServerTransmitPort(i4SetValFsDhcp6RlyServerTransmitPort) \
 nmhSetCmn(FsDhcp6RlyServerTransmitPort, 11, FsDhcp6RlyServerTransmitPortSet, D6RlMainLock, D6RlMainUnLock, 0, 0, 0, "%i", i4SetValFsDhcp6RlyServerTransmitPort)
#define nmhSetFsDhcp6RlyOption37Control(i4SetValFsDhcp6RlyOption37Control) \
 nmhSetCmn(FsDhcp6RlyOption37Control, 11, FsDhcp6RlyOption37ControlSet, D6RlMainLock, D6RlMainUnLock, 0, 0, 0, "%i", i4SetValFsDhcp6RlyOption37Control)
#define nmhSetFsDhcp6RlyPDRouteControl(i4SetValFsDhcp6RlyPDRouteControl)        \
 nmhSetCmn(FsDhcp6RlyPDRouteControl, 11, FsDhcp6RlyPDRouteControlSet, D6RlMainLock, D6RlMainUnLock, 0, 0, 0, "%i", i4SetValFsDhcp6RlyPDRouteControl)
#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDhcp6RlyIfIndex[13];
extern UINT4 FsDhcp6RlyIfHopThreshold[13];
extern UINT4 FsDhcp6RlyIfCounterRest[13];
extern UINT4 FsDhcp6RlyIfRowStatus[13];
extern UINT4 FsDhcp6RlyIfRemoteIdOption[13];
extern UINT4 FsDhcp6RlyIfRemoteIdDUID[13];
extern UINT4 FsDhcp6RlyIfRemoteIdUserDefined[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDhcp6RlyIfHopThreshold(i4FsDhcp6RlyIfIndex ,i4SetValFsDhcp6RlyIfHopThreshold) \
 nmhSetCmn(FsDhcp6RlyIfHopThreshold, 13, FsDhcp6RlyIfHopThresholdSet, D6RlMainLock, D6RlMainUnLock, 0, 0, 1, "%i %i", i4FsDhcp6RlyIfIndex ,i4SetValFsDhcp6RlyIfHopThreshold)
#define nmhSetFsDhcp6RlyIfCounterRest(i4FsDhcp6RlyIfIndex ,i4SetValFsDhcp6RlyIfCounterRest) \
 nmhSetCmn(FsDhcp6RlyIfCounterRest, 13, FsDhcp6RlyIfCounterRestSet, D6RlMainLock, D6RlMainUnLock, 0, 0, 1, "%i %i", i4FsDhcp6RlyIfIndex ,i4SetValFsDhcp6RlyIfCounterRest)
#define nmhSetFsDhcp6RlyIfRowStatus(i4FsDhcp6RlyIfIndex ,i4SetValFsDhcp6RlyIfRowStatus) \
 nmhSetCmn(FsDhcp6RlyIfRowStatus, 13, FsDhcp6RlyIfRowStatusSet, D6RlMainLock, D6RlMainUnLock, 0, 1, 1, "%i %i", i4FsDhcp6RlyIfIndex ,i4SetValFsDhcp6RlyIfRowStatus)
#define nmhSetFsDhcp6RlyIfRemoteIdOption(i4FsDhcp6RlyIfIndex ,i4SetValFsDhcp6RlyIfRemoteIdOption) \
 nmhSetCmn(FsDhcp6RlyIfRemoteIdOption, 13, FsDhcp6RlyIfRemoteIdOptionSet, D6RlMainLock, D6RlMainUnLock, 0, 0, 1, "%i %i", i4FsDhcp6RlyIfIndex ,i4SetValFsDhcp6RlyIfRemoteIdOption)
#define nmhSetFsDhcp6RlyIfRemoteIdDUID(i4FsDhcp6RlyIfIndex ,pSetValFsDhcp6RlyIfRemoteIdDUID) \
 nmhSetCmn(FsDhcp6RlyIfRemoteIdDUID, 13, FsDhcp6RlyIfRemoteIdDUIDSet, D6RlMainLock, D6RlMainUnLock, 0, 0, 1, "%i %s", i4FsDhcp6RlyIfIndex ,pSetValFsDhcp6RlyIfRemoteIdDUID)
#define nmhSetFsDhcp6RlyIfRemoteIdUserDefined(i4FsDhcp6RlyIfIndex ,pSetValFsDhcp6RlyIfRemoteIdUserDefined)  \
 nmhSetCmnWithLock(FsDhcp6RlyIfRemoteIdUserDefined, 13, FsDhcp6RlyIfRemoteIdUserDefinedSet, D6RlMainLock, D6RlMainUnLock, 0, 0, 1, "%i %s", i4FsDhcp6RlyIfIndex ,pSetValFsDhcp6RlyIfRemoteIdUserDefined)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDhcp6RlyInIfIndex[13];
extern UINT4 FsDhcp6RlySrvAddress[13];
extern UINT4 FsDhcp6RlySrvAddressRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDhcp6RlySrvAddressRowStatus(i4FsDhcp6RlyInIfIndex , pFsDhcp6RlySrvAddress ,i4SetValFsDhcp6RlySrvAddressRowStatus) \
 nmhSetCmn(FsDhcp6RlySrvAddressRowStatus, 13, FsDhcp6RlySrvAddressRowStatusSet, D6RlMainLock, D6RlMainUnLock, 0, 1, 2, "%i %s %i", i4FsDhcp6RlyInIfIndex , pFsDhcp6RlySrvAddress ,i4SetValFsDhcp6RlySrvAddressRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDhcp6RlyOutIfIndex[13];
extern UINT4 FsDhcp6RlyOutIfRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDhcp6RlyOutIfRowStatus(i4FsDhcp6RlyInIfIndex , pFsDhcp6RlySrvAddress , i4FsDhcp6RlyOutIfIndex ,i4SetValFsDhcp6RlyOutIfRowStatus) \
 nmhSetCmn(FsDhcp6RlyOutIfRowStatus, 13, FsDhcp6RlyOutIfRowStatusSet, D6RlMainLock, D6RlMainUnLock, 0, 1, 3, "%i %s %i %i", i4FsDhcp6RlyInIfIndex , pFsDhcp6RlySrvAddress , i4FsDhcp6RlyOutIfIndex ,i4SetValFsDhcp6RlyOutIfRowStatus)

#endif
