/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpbrgcli.h,v 1.2 2008/10/31 13:09:30 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIDot1dFutureBaseContextId[13];
extern UINT4 FsMIDot1dBridgeSystemControl[13];
extern UINT4 FsMIDot1dBaseBridgeStatus[13];
extern UINT4 FsMIDot1dBaseBridgeCRCStatus[13];
extern UINT4 FsMIDot1dBaseBridgeDebug[13];
extern UINT4 FsMIDot1dBaseBridgeTrace[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMIDot1dBridgeSystemControl(i4FsMIDot1dFutureBaseContextId ,i4SetValFsMIDot1dBridgeSystemControl)	\
	nmhSetCmn(FsMIDot1dBridgeSystemControl, 13, FsMIDot1dBridgeSystemControlSet, AstLock, AstUnLock, 0, 0, 1, "%i %i", i4FsMIDot1dFutureBaseContextId ,i4SetValFsMIDot1dBridgeSystemControl)
#define nmhSetFsMIDot1dBaseBridgeStatus(i4FsMIDot1dFutureBaseContextId ,i4SetValFsMIDot1dBaseBridgeStatus)	\
	nmhSetCmn(FsMIDot1dBaseBridgeStatus, 13, FsMIDot1dBaseBridgeStatusSet, AstLock, AstUnLock, 0, 0, 1, "%i %i", i4FsMIDot1dFutureBaseContextId ,i4SetValFsMIDot1dBaseBridgeStatus)
#define nmhSetFsMIDot1dBaseBridgeCRCStatus(i4FsMIDot1dFutureBaseContextId ,i4SetValFsMIDot1dBaseBridgeCRCStatus)	\
	nmhSetCmn(FsMIDot1dBaseBridgeCRCStatus, 13, FsMIDot1dBaseBridgeCRCStatusSet, AstLock, AstUnLock, 0, 0, 1, "%i %i", i4FsMIDot1dFutureBaseContextId ,i4SetValFsMIDot1dBaseBridgeCRCStatus)
#define nmhSetFsMIDot1dBaseBridgeDebug(i4FsMIDot1dFutureBaseContextId ,i4SetValFsMIDot1dBaseBridgeDebug)	\
	nmhSetCmn(FsMIDot1dBaseBridgeDebug, 13, FsMIDot1dBaseBridgeDebugSet, AstLock, AstUnLock, 0, 0, 1, "%i %i", i4FsMIDot1dFutureBaseContextId ,i4SetValFsMIDot1dBaseBridgeDebug)
#define nmhSetFsMIDot1dBaseBridgeTrace(i4FsMIDot1dFutureBaseContextId ,i4SetValFsMIDot1dBaseBridgeTrace)	\
	nmhSetCmn(FsMIDot1dBaseBridgeTrace, 13, FsMIDot1dBaseBridgeTraceSet, AstLock, AstUnLock, 0, 0, 1, "%i %i", i4FsMIDot1dFutureBaseContextId ,i4SetValFsMIDot1dBaseBridgeTrace)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIDot1dFutureBasePort[13];
extern UINT4 FsMIDot1dBasePortAdminStatus[13];
extern UINT4 FsMIDot1dBasePortBcastStatus[13];
extern UINT4 FsMIDot1dBasePortFilterNumber[13];
extern UINT4 FsMIDot1dBasePortMcastNumber[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMIDot1dBasePortAdminStatus(i4FsMIDot1dFutureBasePort ,i4SetValFsMIDot1dBasePortAdminStatus)	\
	nmhSetCmn(FsMIDot1dBasePortAdminStatus, 13, FsMIDot1dBasePortAdminStatusSet, AstLock, AstUnLock, 0, 0, 1, "%i %i", i4FsMIDot1dFutureBasePort ,i4SetValFsMIDot1dBasePortAdminStatus)
#define nmhSetFsMIDot1dBasePortBcastStatus(i4FsMIDot1dFutureBasePort ,i4SetValFsMIDot1dBasePortBcastStatus)	\
	nmhSetCmn(FsMIDot1dBasePortBcastStatus, 13, FsMIDot1dBasePortBcastStatusSet, AstLock, AstUnLock, 0, 0, 1, "%i %i", i4FsMIDot1dFutureBasePort ,i4SetValFsMIDot1dBasePortBcastStatus)
#define nmhSetFsMIDot1dBasePortFilterNumber(i4FsMIDot1dFutureBasePort ,i4SetValFsMIDot1dBasePortFilterNumber)	\
	nmhSetCmn(FsMIDot1dBasePortFilterNumber, 13, FsMIDot1dBasePortFilterNumberSet, AstLock, AstUnLock, 0, 0, 1, "%i %i", i4FsMIDot1dFutureBasePort ,i4SetValFsMIDot1dBasePortFilterNumber)
#define nmhSetFsMIDot1dBasePortMcastNumber(i4FsMIDot1dFutureBasePort ,i4SetValFsMIDot1dBasePortMcastNumber)	\
	nmhSetCmn(FsMIDot1dBasePortMcastNumber, 13, FsMIDot1dBasePortMcastNumberSet, AstLock, AstUnLock, 0, 0, 1, "%i %i", i4FsMIDot1dFutureBasePort ,i4SetValFsMIDot1dBasePortMcastNumber)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIDot1dFutureTpPort[13];
extern UINT4 FsMIDot1dTpPortProtocolFilterMask[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMIDot1dTpPortProtocolFilterMask(i4FsMIDot1dFutureTpPort ,i4SetValFsMIDot1dTpPortProtocolFilterMask)	\
	nmhSetCmn(FsMIDot1dTpPortProtocolFilterMask, 13, FsMIDot1dTpPortProtocolFilterMaskSet, AstLock, AstUnLock, 0, 0, 1, "%i %i", i4FsMIDot1dFutureTpPort ,i4SetValFsMIDot1dTpPortProtocolFilterMask)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIDot1dFilterNumber[13];
extern UINT4 FsMIDot1dFilterSrcAddress[13];
extern UINT4 FsMIDot1dFilterSrcMask[13];
extern UINT4 FsMIDot1dFilterDstAddress[13];
extern UINT4 FsMIDot1dFilterDstMask[13];
extern UINT4 FsMIDot1dFilterPermiss[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMIDot1dFilterSrcMask(i4FsMIDot1dFutureBaseContextId , i4FsMIDot1dFilterNumber , tFsMIDot1dFilterSrcAddress , tFsMIDot1dFilterDstAddress ,SetValFsMIDot1dFilterSrcMask)	\
	nmhSetCmn(FsMIDot1dFilterSrcMask, 13, FsMIDot1dFilterSrcMaskSet, AstLock, AstUnLock, 0, 0, 4, "%i %i %m %m %m", i4FsMIDot1dFutureBaseContextId , i4FsMIDot1dFilterNumber , tFsMIDot1dFilterSrcAddress , tFsMIDot1dFilterDstAddress ,SetValFsMIDot1dFilterSrcMask)
#define nmhSetFsMIDot1dFilterDstMask(i4FsMIDot1dFutureBaseContextId , i4FsMIDot1dFilterNumber , tFsMIDot1dFilterSrcAddress , tFsMIDot1dFilterDstAddress ,SetValFsMIDot1dFilterDstMask)	\
	nmhSetCmn(FsMIDot1dFilterDstMask, 13, FsMIDot1dFilterDstMaskSet, AstLock, AstUnLock, 0, 0, 4, "%i %i %m %m %m", i4FsMIDot1dFutureBaseContextId , i4FsMIDot1dFilterNumber , tFsMIDot1dFilterSrcAddress , tFsMIDot1dFilterDstAddress ,SetValFsMIDot1dFilterDstMask)
#define nmhSetFsMIDot1dFilterPermiss(i4FsMIDot1dFutureBaseContextId , i4FsMIDot1dFilterNumber , tFsMIDot1dFilterSrcAddress , tFsMIDot1dFilterDstAddress ,i4SetValFsMIDot1dFilterPermiss)	\
	nmhSetCmn(FsMIDot1dFilterPermiss, 13, FsMIDot1dFilterPermissSet, AstLock, AstUnLock, 0, 0, 4, "%i %i %m %m %i", i4FsMIDot1dFutureBaseContextId , i4FsMIDot1dFilterNumber , tFsMIDot1dFilterSrcAddress , tFsMIDot1dFilterDstAddress ,i4SetValFsMIDot1dFilterPermiss)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIDot1dMlistNumber[13];
extern UINT4 FsMIDot1dMcastMacaddress[13];
extern UINT4 FsMIDot1dMcastPermiss[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMIDot1dMcastPermiss(i4FsMIDot1dFutureBaseContextId , tFsMIDot1dMcastMacaddress , i4FsMIDot1dMlistNumber ,i4SetValFsMIDot1dMcastPermiss)	\
	nmhSetCmn(FsMIDot1dMcastPermiss, 13, FsMIDot1dMcastPermissSet, AstLock, AstUnLock, 0, 0, 3, "%i %m %i %i", i4FsMIDot1dFutureBaseContextId , tFsMIDot1dMcastMacaddress , i4FsMIDot1dMlistNumber ,i4SetValFsMIDot1dMcastPermiss)

#endif
