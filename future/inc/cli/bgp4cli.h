/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgp4cli.h,v 1.89 2017/12/28 10:40:14 siva Exp $
 *
 * Description:This file includes all the BGP CLI related definitions
 * and declarations.          
 *
 *******************************************************************/
#ifndef BGP4CLI_H
#define BGP4CLI_H
#include "lr.h"
#include "cli.h"
#include "bgp.h"
#include "rmap.h"
#ifdef RRD_WANTED
#include "ip.h"
#include "rtm.h"
#endif

/* Should be same as that of the definition in bgp/inc/fsbgpcli.h */
#define VAL_NO                    (0)
#define VAL_SET                   (1)
#define VAL_INVALID               (2)
#define ALL_PEERS_SET             (1)
#define ALL_PEERS_NO              (2)
#define BGP4_INET_FAMILY_IPV4     (0x0001) 
#define DIR_INCOMING              (1)
#define DIR_OUTGOING              (2)
#define BGP4_CLI_ONE              (0x0001) 
#define BGP_CLI_VAL_SET        VAL_SET
#define BGP_CLI_VAL_NO         VAL_NO

#define BGP4_CLI_ROUTE_TYPE_EBGP    0
#define BGP4_CLI_ROUTE_TYPE_IBGP    1 
#define BGP4_CLI_ROUTE_TYPE_EIBGP   2
#define BGP4_CLI_IN_FILTER          0
#define BGP4_CLI_OUT_FILTER         1

#define BGP4_CLI_AFI_IPV4                          (1)
#define BGP4_CLI_AFI_IPV6                          (2)
#define BGP4_CLI_AFI_VPNV4                         (3)
#define BGP4_CLI_AFI_IPV4_VRF                      (4)
#ifdef VPLSADS_WANTED
#define BGP4_CLI_AFI_L2VPN                         (5)
#endif
#ifdef EVPN_WANTED
#define BGP4_CLI_AFI_EVPN                          (6)
#endif

#define BGP4_CLI_AFI_ENABLED                       1
#define BGP4_CLI_AFI_DISABLED                      2

#define  BGP4_CLI_GR_SUPPORT_NONE                  1
#define  BGP4_CLI_GR_SUPPORT_PLANNED               2
#define  BGP4_CLI_GR_SUPPORT_BOTH                  3

#define  BGP4_CLI_GR_REASON_UNKNOWN                0
#define  BGP4_CLI_GR_REASON_RESTART                1
#define  BGP4_CLI_GR_REASON_UPGRADE                2

#define  BGP4_CLI_DEF_STALE_TIME                       90 
#define  BGP4_CLI_DEF_RESTART_TIME                     120 

#define BGP4_TCP_AO_HMAC_SHA1      (1)
#define BGP4_TCP_AO_AES_128       (2)
#define BGP4_TCP_AO_OPTION_NO_EXCLUDE    (1)
#define BGP4_TCP_AO_OPTION_EXCLUDE     (2)

#define BGP4_CLI_PER_VRF_LBL_ALLOC  1
#define BGP4_CLI_PER_ROUTE_LBL_ALLOC  2
/*Clear bgp related */
enum
{
   BGP4_CLEAR_GROUP1 = 1,
   BGP4_CLEAR_GROUP2,
   BGP4_CLEAR_GROUP3,
   BGP4_CLEAR_GROUP4,
   BGP4_CLEAR_GROUP5,
   BGP4_AS_CHECK,
   BGP4_AS_NO_CHECK,
   BGP4_CLEAR_HARD,
   BGP4_CLEAR_SOFT,
   BGP4_CLEAR_SOFT_IN,
   BGP4_CLEAR_SOFT_OUT,
   BGP4_CLEAR_SOFT_BOTH,
   BGP4_CLEAR_ALL,
   BGP4_CLEAR_EXTERNAL,
   BGP4_CLEAR_IPV4,
   BGP4_CLEAR_IPV6
};
 
/* SIZE constants */
#define AS_VERSION_OBJ  1
#define FSBGP4_CLI_AS_VERSION_SIZE (CLI_MAX_COLUMN_WIDTH * AS_VERSION_OBJ)

#define ROUTER_BGP_OBJ  1
#define FSBGP4_CLI_ROUTER_BGP_SIZE (CLI_MAX_COLUMN_WIDTH * ROUTER_BGP_OBJ)

#define SHUTDOWN_OBJ  1
#define FSBGP4_CLI_SHUTDOWN_SIZE (CLI_MAX_COLUMN_WIDTH * SHUTDOWN_OBJ)

#define ROUTER_ID_OBJ  1
#define FSBGP4_CLI_ROUTER_ID_SIZE (CLI_MAX_COLUMN_WIDTH * ROUTER_ID_OBJ)

#define LOCAL_PREF_OBJ  1
#define FSBGP4_CLI_LOCAL_PREF_SIZE (CLI_MAX_COLUMN_WIDTH * LOCAL_PREF_OBJ)

#define PEER_CONFIG_OBJ  2
#define FSBGP4_CLI_PEER_CONFIG_SIZE \
                                (CLI_MAX_COLUMN_WIDTH * PEER_CONFIG_OBJ)
#define NON_BGP_RT_ADVT_OBJ  2
#define FSBGP4_CLI_NON_BGP_RT_ADVT_SIZE \
                              (CLI_MAX_COLUMN_WIDTH * NON_BGP_RT_ADVT_OBJ)

#define OVERLAP_POLICY_OBJ  1
#define FSBGP4_CLI_OVERLAP_POLICY_SIZE \
                            (CLI_MAX_COLUMN_WIDTH * OVERLAP_POLICY_OBJ)

#define REDISTRIBUTION_OBJ  1
#define FSBGP4_CLI_REDISTRIBUTION_SIZE \
                            (CLI_MAX_COLUMN_WIDTH * REDISTRIBUTION_OBJ)

#define SYNCHRONIZATION_OBJ  1
#define FSBGP4_CLI_SYNCHRONIZATION_SIZE \
                            (CLI_MAX_COLUMN_WIDTH * SYNCHRONIZATION_OBJ)

#define COMP_MED_OBJ  1
#define FSBGP4_CLI_COMP_MED_SIZE \
                            (CLI_MAX_COLUMN_WIDTH * COMP_MED_OBJ)

#define DFLT_METRIC_OBJ  1
#define FSBGP4_CLI_DFLT_METRIC_SIZE \
                            (CLI_MAX_COLUMN_WIDTH * DFLT_METRIC_OBJ)

#define MEDLP_TBL_OBJ  2
#define FSBGP4_CLI_MEDLP_TBL_SIZE (CLI_MAX_COLUMN_WIDTH * MEDLP_TBL_OBJ)

#define FILTER_TBL_OBJ  2
#define FSBGP4_CLI_FILTER_TBL_SIZE (CLI_MAX_COLUMN_WIDTH * FILTER_TBL_OBJ)

#define AGGR_TBL_OBJ  2
#define FSBGP4_CLI_AGGR_TBL_SIZE (CLI_MAX_COLUMN_WIDTH * AGGR_TBL_OBJ)

#define DAMP_TBL_OBJ  1
#define FSBGP4_CLI_DAMP_TBL_SIZE (CLI_MAX_COLUMN_WIDTH * DAMP_TBL_OBJ)

#define RFL_OBJ  2
#define FSBGP4_CLI_RFL_SIZE (CLI_MAX_COLUMN_WIDTH * RFL_OBJ)

#define COMM_OBJ  2
#define FSBGP4_CLI_COMM_SIZE (CLI_MAX_COLUMN_WIDTH * COMM_OBJ)

#define ECOMM_OBJ  2
#define FSBGP4_CLI_ECOMM_SIZE (CLI_MAX_COLUMN_WIDTH * ECOMM_OBJ)

/* for Confed */
#define CONFED_OBJ  1
#define FSBGP4_CLI_CONFED_SIZE (CLI_MAX_COLUMN_WIDTH * CONFED_OBJ)

#ifdef BGP_TEST_WANTED
#define BGP_UT_TEST 1
#endif

enum {
    CLI_BGP4_ROUTER_BGP=1,
    CLI_BGP4_SHUTDOWN,       
    CLI_BGP4_BGP_ROUTER_ID,
    CLI_BGP4_DFT_LOCAL_PREF,                    
    CLI_BGP4_PEER_REMOTE_AS_CONFIG,              
    CLI_BGP4_PEER_EBGP_MULTIHOP,                
    CLI_BGP4_PEER_SELF_NEXTHOP,                 
    CLI_BGP4_PEER_INTERVAL,                      
    CLI_BGP4_PEER_TIMER,                         
    CLI_BGP4_PEER_SHUTDOWN,                      
    CLI_BGP4_NON_BGP_ROUTE_ADVT,                 
    CLI_BGP4_OVERLAP_POLICY,
    CLI_BGP4_REDISTRIBUTE,                       
    CLI_BGP4_NETWORK_ROUTE,
    CLI_BGP4_SYNCHRONISATION,                   
    CLI_BGP4_FOUR_BYTE_ASN, 
    CLI_BGP4_FOUR_BYTE_ASN_ASDOT,
    CLI_BGP4_COMP_MED,                           
    CLI_BGP4_MED_TBL,                            
    CLI_BGP4_LP_TBL,                             
    CLI_BGP4_FILTER_TBL,                        
    CLI_BGP4_AGGR_TBL,                          
    CLI_BGP4_DAMP,                              
    CLI_BGP4_RFL_CLUS_ID,                        
    CLI_BGP4_TRAP,
    CLI_BGP4_RFL_SUPPORT,                        
    CLI_BGP4_RFL_PEER_CONFIG,                    
    CLI_BGP4_COMM_ROUTE_TBL,                     
    CLI_BGP4_COMM_FILTER_TBL,                    
    CLI_BGP4_COMM_POLICY_TBL,                    
    CLI_BGP4_ECOMM_ROUTE_TBL,                    
    CLI_BGP4_ECOMM_FILTER_TBL,                
    CLI_BGP4_ECOMM_POLICY_TBL,                   
                      
    CLI_BGP4_DFLT_METRIC,                        
                           
    CLI_BGP4_CONFED_ID,                          
    CLI_BGP4_CONFED_PEER,                        
    CLI_BGP4_COMP_MED_CONFED,                    
    CLI_BGP4_CLEAR_IP_BGP,                       
    CLI_BGP4_TCP_MD5_PASSWD,                     
    CLI_BGP4_NO_ROUTER_BGP,
    CLI_BGP4_NO_SHUTDOWN,       
    CLI_BGP4_NO_BGP_ROUTER_ID,
    CLI_BGP4_NO_DFT_LOCAL_PREF,                    
    CLI_BGP4_NO_PEER_REMOTE_AS_CONFIG,              
    CLI_BGP4_NO_PEER_EBGP_MULTIHOP,                
    CLI_BGP4_NO_PEER_SELF_NEXTHOP,                 
    CLI_BGP4_NO_PEER_INTERVAL,                      
    CLI_BGP4_NO_PEER_TIMER,                         
    CLI_BGP4_NO_PEER_SHUTDOWN,                      
    CLI_BGP4_NO_NON_BGP_ROUTE_ADVT,                 
    CLI_BGP4_NO_OVERLAP_POLICY,
    CLI_BGP4_NO_NETWORK_ROUTE,    
    CLI_BGP4_NO_REDISTRIBUTE,                       
    CLI_BGP4_NO_SYNCHRONISATION,                   
    CLI_BGP4_NO_FOUR_BYTE_ASN, 
    CLI_BGP4_FOUR_BYTE_ASN_NO_ASDOT,
    CLI_BGP4_NO_COMP_MED,                           
    CLI_BGP4_NO_MED_TBL,                            
    CLI_BGP4_NO_LP_TBL,                             
    CLI_BGP4_NO_FILTER_TBL,                        
    CLI_BGP4_NO_AGGR_TBL,                          
    CLI_BGP4_NO_DAMP,                              
    CLI_BGP4_NO_RFL_CLUS_ID,                        
    CLI_BGP4_NO_RFL_SUPPORT,                        
    CLI_BGP4_NO_RFL_PEER_CONFIG,                    
    CLI_BGP4_NO_COMM_ROUTE_TBL,                     
    CLI_BGP4_NO_COMM_FILTER_TBL,                    
    CLI_BGP4_NO_COMM_POLICY_TBL,                    
    CLI_BGP4_NO_ECOMM_ROUTE_TBL,                    
    CLI_BGP4_NO_ECOMM_FILTER_TBL,                
    CLI_BGP4_NO_ECOMM_POLICY_TBL,                   
    CLI_BGP4_NO_DFLT_METRIC,                        
    CLI_BGP4_NO_CONFED_ID,                          
    CLI_BGP4_NO_CONFED_PEER,                        
    CLI_BGP4_NO_COMP_MED_CONFED,                    
    CLI_BGP4_NO_TCP_MD5_PASSWD,                     
    CLI_BGP4_NO_IMPORT_ROUTE,                       
    BGP4_NO_IMPORT_DIRECT,
    BGP4_NO_IMPORT_RIP,
    BGP4_NO_IMPORT_OSPF,
    BGP4_NO_IMPORT_STATIC,
    BGP4_NO_IMPORT_ALL,
    CLI_BGP4_PEER_COMM_SEND_STATUS,
    CLI_BGP4_PEER_ECOMM_SEND_STATUS,
    CLI_BGP4_NO_PEER_COMM_SEND_STATUS,
    CLI_BGP4_NO_PEER_ECOMM_SEND_STATUS,
    CLI_BGP4_CLEAR_STATISTICS,
    CLI_BGP4_DEF_IPV4_UNI,
    CLI_BGP4_NO_DEF_IPV4_UNI,
    CLI_BGP4_PEER_ACTIVATE,
    CLI_BGP4_NO_PEER_ACTIVATE,
    CLI_BGP4_PEER_OVERRIDE_CAPABILITY,
    CLI_BGP4_NO_PEER_OVERRIDE_CAPABILITY,
    CLI_BGP4_PEER_ALLOW_AUTOSTART,
    CLI_BGP4_MAX_PEER_PREFIX_VALUE,
    CLI_BGP4_PEER_CONNECT_RETRY_COUNT,
    CLI_BGP4_PEER_ALLOW_AUTOSTOP,
    CLI_BGP4_DAMP_PEER_OSCILLATIONS,
    CLI_BGP4_PEER_DELAY_OPEN,
    CLI_BGP4_TCP_PASSIVE,
    CLI_BGP4_PEER_CE_ROUTE_TARGET_ADVT,
    CLI_BGP4_PEER_SOO_EXT_COM,
    CLI_BGP4_DEF_ROUTE_ORIGINATE,
    CLI_BGP4_PEER_UPDATESRC,
    CLI_BGP4_PEER_GATEWAY,
    CLI_BGP4_PEER_NETWORK_ADDR,
    CLI_BGP4_PEER_DEF_ROUTE_ORIGINATE,
    CLI_BGP4_PEER_CAP_CONFIG,
    CLI_BGP4_IPV4_UNICAST,
    CLI_BGP4_VRF_ROUTE_TARGET,
    CLI_BGP4_IMPORT_ROUTE,
    CLI_BGP4_IMPORT_IPV6_ROUTE,
    CLI_BGP4_CLEAR_IPV6_BGP,
    BGP4_CLI_ADDRESS_FAMILY,
    CLI_BGP4_ROUTE_DISTANCE,
    CLI_BGP4_NO_ROUTE_DISTANCE,
    CLI_BGP4_DISTRIBUTE_LIST,
    CLI_BGP4_GR_ENABLE,
    CLI_BGP4_GR_DISABLE,
    CLI_BGP4_SELECTION_TIMER,
    CLI_BGP4_NO_SELECTION_TIMER,
    CLI_BGP4_RESTART_SUPPORT,
    CLI_BGP4_RESTART_REASON,
    CLI_BGP4_NO_GR_RESTART_REASON,
    CLI_BGP4_PEER_GROUP_CONFIG,
    CLI_BGP4_PEER_GROUP_LIST_CONFIG,
    CLI_BGP4_ADVT_ROUTES,
    CLI_BGP4_NO_ADVT_ROUTES,
    CLI_BGP4_NEIGHBOR_ROUTEMAP,
    CLI_BGP4_NEXTHOP_PRCS_INTERVAL,
    CLI_BGP4_REDISTRIBUTE_INTERNAL,
    CLI_BGP4_NO_REDISTRIBUTE_INTERNAL,
    CLI_BGP4_TRACE,                              
    CLI_BGP4_NO_TRACE,
    CLI_BGP4_MAX_PATHS,
    CLI_BGP4_NO_MAX_PATHS,
    CLI_BGP4_PEER_LOCAL_AS_CONFIG,
    CLI_BGP4_NO_PEER_LOCAL_AS_CONFIG, 
    CLI_BGP4_TCP_AO_MKT_CONFIG,
    CLI_BGP4_NO_TCP_AO_MKT_CONFIG,
    CLI_BGP4_TCP_AO_ICMP_ACCEPT,
    CLI_BGP4_TCP_AO_NOMKT_PKT_DISC,
    CLI_BGP4_NO_TCP_AO_ICMP_ACCEPT,
    CLI_BGP4_NO_TCP_AO_NOMKT_PKT_DISC,
    CLI_BGP4_TCP_AO_MKT_ASSOCIATE,
    CLI_BGP4_NO_TCP_AO_MKT_ASSOCIATE,
    CLI_BGP4_LBL_ALLOC_MODE,
    /* CLI_BGP4_PEER_BFD_STATUS should be the last bgp configuration
     * command. Else bgp4cli.c also has to be updated */
    CLI_BGP4_PEER_BFD_STATUS,
    BGP4_CLI_AFI_L2VPN_EVPN,
    CLI_BGP4_SHOW_VERSION,                       
    CLI_BGP4_SHOW_IP_BGP,                        
    CLI_BGP4_SHOW_COMM,
    CLI_BGP4_SHOW_ECOMM,
    CLI_BGP4_SHOW_SUMMARY,
    CLI_BGP4_SHOW_VPNV4,
    CLI_BGP4_SHOW_MED,
    CLI_BGP4_SHOW_FILTER,
    CLI_BGP4_SHOW_AGGR,
    CLI_BGP4_SHOW_DAMP,
    CLI_BGP4_SHOW_IPV6_DAMP,
    CLI_BGP4_SHOW_LOC_PREF,
    CLI_BGP4_SHOW_COMM_ROUTE,
    CLI_BGP4_SHOW_COMM_POLICY,
    CLI_BGP4_SHOW_COMM_FILTER,
    CLI_BGP4_SHOW_ECOMM_ROUTE,
    CLI_BGP4_SHOW_ECOMM_POLICY,
    CLI_BGP4_SHOW_ECOMM_FILTER,
    CLI_BGP4_SHOW_BGP_INFO,
    CLI_BGP4_SHOW_TIMERS,
    CLI_BGP4_SHOW_RFL_INFO,
    CLI_BGP4_SHOW_CONFED_INFO,
    CLI_BGP4_SHOW_BGP_LABEL,
    CLI_BGP4_SHOW_IPV6_BGP,
    CLI_BGP4_SHOW_RESTART_SUPPORT,
    CLI_BGP4_SHOW_RESTART_MODE,
    CLI_BGP4_SHOW_RESTART_REASON,
    CLI_BGP4_SHOW_RESTART_EXITREASON,
    CLI_BGP4_SHOW_RESTART_STATUS,
    CLI_BGP4_SHOW_RESTART_ENDOFRIB_MARKER_STATUS,
    CLI_BGP4_SHOW_BGP_PEER_GRP,
    CLI_BGP4_SHOW_DAMP_FLAP_STAT,
    CLI_BGP4_SHOW_DAMP_PATHS,
    CLI_BGP4_SHOW_IPV6_DAMP_PATHS,
    CLI_BGP4_SHOW_IPV6_DAMP_FLAP_STAT,
    CLI_BGP4_SHOW_IPV6_NEIGH_TCPAO,
    CLI_BGP4_SHOW_IP_NEIGH_TCPAO,
    CLI_BGP4_SHOW_IP_TCPAO_MKT,
    CLI_BGP4_SHOW_L2VPN_VPLS,
    CLI_BGP4_SHOW_VPLS_INSTANCE,
    CLI_BGP4_SHOW_L2VPN_EVPN,
    CLI_BGP_EVPN_ROUTE_SHOW,
    CLI_BGP_EVPN_MAC_SHOW,
    CLI_BGP4_MAC_DUP_TIMER,
    CLI_BGP4_NO_MAC_DUP_TIMER,
    CLI_BGP4_MAX_MAC_MOVES,
    CLI_BGP4_NO_MAX_MAC_MOVES,
    CLI_BGP4_VRF_ROUTE_LEAK
};

/* Error codes */
enum
{
    CLI_BGP4_TASK_NOT_INIT_ERR = 1,
    CLI_BGP4_ADMIN_ACTIVE_ERR,
    CLI_BGP4_INVALID_IPV6_ADDRESS_ERR,
    CLI_BGP4_INVALID_AS_NO_ERR,
    CLI_BGP4_AS_NOT_CONFIG_ERR,
    CLI_BGP4_INVALID_ADMIN_STATUS_ERR,
    CLI_BGP4_INVALID_BGP_IDENTIFIER_ERR,
    CLI_BGP4_INVALID_LOCAL_PREF_ERR,
    CLI_BGP4_INVALID_PEER_ADDRESS_ERR,
    CLI_BGP4_INVALID_REMOTE_AS_ERR,
    CLI_BGP4_INVALID_NEXTHOP_SELF_CONFIG_ERR,
    CLI_BGP4_INVALID_KEEP_ALIVE_VALUE_ERR,
    CLI_BGP4_INVALID_HOLD_TIME_VALUE_ERR,
    CLI_BGP4_INVALID_AS_ORIG_INT_ERR,
    CLI_BGP4_INVALID_RT_ADV_INT_ERR,
    CLI_BGP4_INVALID_CONN_RETRY_INT_ERR,
    CLI_BGP4_PEER_ENABLED,
    CLI_BGP4_PEER_NOT_ENABLED,
    CLI_BGP4_INVALID_NON_BGP_RT_ADVT_CONFIG_ERR,
    CLI_BGP4_INVALID_OVERLAP_RT_POLICY_ERR,
    CLI_BGP4_INVALID_REDIS_PROTO_ENABLE_MASK_ERR,
    CLI_BGP4_INVALID_SYNC_CONFIG_ERR,
    CLI_BGP4_INVALID_MED_CONFIG_VALUE_ERR,
    CLI_BGP4_INVALID_METRIC_VALUE_ERR,
    CLI_BGP4_MED_TABLE_INDEX_ERR,
    CLI_BGP4_MED_TABLE_ADMIN_STATUS_ERR,
    CLI_BGP4_MED_TABLE_PREFIX_VALUE_ERR,
    CLI_BGP4_MED_TABLE_PREFIX_LEN_ERR,
    CLI_BGP4_MED_TABLE_INDEX_REMOTE_AS_ERR,
    CLI_BGP4_INT_AS_LEN_ERR,
    CLI_BGP4_INVALID_MED_VALUE_ERR,
    CLI_BGP4_DISP_STR_OBJ_ERR,
    CLI_BGP4_INVALID_MED_TABLE_DIR_ERR,
    CLI_BGP4_LP_TABLE_INDEX_ERR,
    CLI_BGP4_LP_TABLE_ADMIN_STATUS_ERR,
    CLI_BGP4_LP_TABLE_PREFIX_VALUE_ERR,
    CLI_BGP4_LP_TABLE_PREFIX_LEN_ERR,
    CLI_BGP4_LP_TABLE_REMOTE_AS_ERR,
    CLI_BGP4_INVALID_LP_TABLE_DIR_ERR,
    CLI_BGP4_INVALID_LP_VALUE_ERR,
    CLI_BGP4_FILTER_TABLE_INDEX_ERR,
    CLI_BGP4_FILTER_TABLE_ADM_STATUS_ERR,
    CLI_BGP4_FILTER_TABLE_PREFIX_ERR,
    CLI_BGP4_FILTER_TABLE_PREFIX_LEN_ERR,
    CLI_BGP4_FILTER_TABLE_REMOTE_AS_ERR,
    CLI_BGP4_INVALID_FILTER_TABLE_DIR_ERR,
    CLI_BGP4_INVALID_FILTER_TABLE_ACTION_ERR,
    CLI_BGP4_INVALID_CLUS_ID_ERR,
    CLI_BGP4_INVALID_RFL_OPT_VALUE_ERR,
    CLI_BGP4_INVALID_PREFIX_ERR,
    CLI_BGP4_COMM_VALUE_ERR,
    CLI_BGP4_COMM_IN_FILT_STATUS_ERR,
    CLI_BGP4_COMM_OUT_FILT_STATUS_ERR,
    CLI_BGP4_COMM_SET_STATUS_ERR,
    CLI_BGP4_ECOMM_VALUE_ERR,
    CLI_BGP4_ECOMM_SEND_STATUS_ERR,
    CLI_BGP4_ECOMM_IN_FLT_STATUS_ERR,
    CLI_BGP4_ECOMM_OUT_FLT_STATUS_ERR,
    CLI_BGP4_ECOMM_SET_STATUS_ERR,
    CLI_BGP4_INVALID_CONFED_ID_ERR,
    CLI_BGP4_INVALID_CONFED_PEER_AS_ERR,
    CLI_BGP4_EXC_MAX_CONFED_PEER_AS_ERR,
    CLI_BGP4_INVALID_CONFED_PEER_STATUS_ERR,
    CLI_BGP4_MED_OPT_VALUE_ERR,
    CLI_BGP4_PASSWD_SET_STATUS_ERR,
    CLI_BGP4_UNSUPP_ADD_FLY_ERR,
    CLI_BGP4_PASSWD_LEN_ERR,
    CLI_BGP4_PREFIX_LEN_ERR,
    CLI_BGP4_PREFIX_VALUE_ERR,
    CLI_BGP4_PROTO_VALUE_ERR,
    CLI_BGP4_NEXT_HOP_VALUE_ERR,
    CLI_BGP4_INT_INDEX_ERR,
    CLI_BGP4_METRIC_VALUE_ERR,
    CLI_BGP4_ACTION_VALUE_ERR,
    CLI_BGP4_PEER_ADD_LEN_ERR,
    CLI_BGP4_RT_REF_IN_REQ_ERR,
    CLI_BGP4_SFT_OUT_REQ_ERR,
    CLI_BGP4_INVALID_BGP_TRACE_ERR,
    CLI_BGP4_COMM_SEND_STATUS_ERR,
    CLI_BGP4_INVALID_AGGR_INDEX_ERR,
    CLI_BGP4_AGGR_PREFIX_ERR,
    CLI_BGP4_AGGR_ADMIN_STATUS_ERR,
    CLI_BGP4_AGGR_PREFIX_LEN_ERR,
    CLI_BGP4_AGGR_ADV_VALUE_ERR,
    CLI_BGP4_DECAY_HALF_LIFE_TIME_VALUE_ERR,
    CLI_BGP4_RFD_CUTOFF_VALUE_ERR,
    CLI_BGP4_RFD_REUSE_VALUE_ERR,
    CLI_BGP4_HIGHER_DECAY_HALF_LIFE_TIME,
    CLI_BGP4_MAX_HOLD_TIME_VALUE_ERR,
    CLI_BGP4_DECAY_TIMER_GRAN_VALUE_ERR,
    CLI_BGP4_HIGHER_DECAY_TIMER_GRAN_VALUE_ERR,
    CLI_BGP4_HIGHER_REUSE_TIMER_GRAN_VALUE_ERR,
    CLI_BGP4_REUSE_TIMER_GRAN_VALUE_ERR,
    CLI_BGP4_ADMIN_DOWN_ERR,
    CLI_BGP4_REUSE_IND_ARRAY_VALUE_ERR,
    CLI_BGP4_ECOMM_ENTRY_EXISTS_ERR,
    CLI_BGP4_INTER_AS_ERR,
    CLI_BGP4_RTM_NOT_REG,
    CLI_BGP4_NO_PEER_ENTRY,
    CLI_BGP4_EXT_PEER,
    CLI_BGP4_PEER_NOT_ESTAB,
    CLI_BGP4_EXCED_MAX_PEER,
    CLI_BGP4_INV_ASSOC_ROUTE_MAP,
    CLI_BGP4_INV_DISASSOC_ROUTE_MAP,
    CLI_BGP4_RMAP_ASSOC_FAILED,
    CLI_BGP4_WRONG_DISTANCE_VALUE,
    CLI_BGP4_RRD_NOT_SET_ERR,
    CLI_BGP4_PEER_CONNECT_RETRY_TIME_ERR,
    CLI_BGP4_PEER_TCP_RETRY_COUNT_ERR,
    CLI_BGP4_DELAY_OPEN_INTERVAL_ERR,
    CLI_BGP4_PEER_AUTOMATIC_START_STOP_ERR,
    CLI_BGP4_DAMP_PEER_OSCILLATIONS_STATUS_ERR,
    CLI_BGP4_PEER_DELAY_OPEN_INTERVAL_STATUS_ERR,
    CLI_BGP4_AS_SET_VALUE_ERR,
    CLI_BGP4_PEER_GRP_NO_AS_ERR,
    CLI_BGP4_PEER_GRP_INTERNAL_ERR,
    CLI_BGP4_PEER_GRP_EXTERNAL_ERR,
    CLI_BGP4_PEER_GRP_ADDR_FAMILY_ERR,
    CLI_BGP4_NO_PEER_GRP_ERR,
    CLI_BGP4_PEER_GRP_READD_ERR,
    CLI_BGP4_NH_CHG_PRCS_INTERVAL_ERR,
    CLI_BGP4_KEEPALIVE_TIME_GREATER_THAN_HOLD_TIME_ERR,
    CLI_BGP4_INVALID_UPDATE_SOURCE_ADDRESS_ERR,
    CLI_BGP4_INTER_AS_EQL_LOCAL_AS_ERR,
    CLI_BGP4_INVALID_PREFIX_LEN_ERR,
    CLI_BGP4_INVALID_ECOMM_VAL_ERR,
    CLI_BGP4_INVALID_GATEWAY_ADDRESS_ERR,
    CLI_BGP4_INVALID_RFL_PEER_ENTRY,
    CLI_BGP4_UPDATE_SOURCE_ADDRESS_NOT_CONFIG,
    CLI_BGP4_INVALID_AGGR_ADDR_ERR,
    CLI_BGP4_INVALID_IP_ADDRESS_ERR,
    CLI_BGP4_INVALID_RIB_ROUTE,
    CLI_BGP4_INVALID_NETWORK_ADDR,
    CLI_BGP4_WRONG_NETWORK_CONF,
    CLI_BGP4_EXCEED_MAX_AFISAFI_ENTRIES,
    CLI_BGP4_EXCEED_MAX_ENTRIES,
    CLI_BGP4_NO_ENTRY,
    CLI_BGP4_INVALID_REDIS_MASK_ERR,
    CLI_BGP4_INVALID_RMAP_VALUE_ERR,
    CLI_BGP4_INVALID_RFD_ADMIN_STAT,
    CLI_BGP4_RFD_ENABLE_ERR,
    CLI_BGP4_RFD_DECAY_ARRAY_OVERFLOW,
    CLI_BGP4_MAX_INTER_AS_REACHED,
    CLI_BGP4_UNSUPP_TCPAO_ALGO,
    CLI_BGP4_MKT_TIME_CONFIG_NOT_SUPPORTED,
    CLI_BGP4_INVALID_MKT_ID, 
    CLI_BGP4_TCP_AO_CONFIGURED,
    CLI_BGP4_MD5_AUTH_ENABLED,
    CLI_BGP4_TCP_AO_MKT_ACTIVE,
    CLI_BGP4_TCP_AO_MKT_NEIGHBOR_ASSO,
    CLI_BGP4_RESERVED_AS_ERR,
    CLI_BGP4_INVALID_FOUR_BYTE_SUPPORT_ERR,
    CLI_BGP4_FOUR_BYTE_ASN_RANGE_ERR,
    CLI_BGP4_INVALID_FOUR_BYTE_NOTATION_ERR,
    CLI_BGP4_FOUR_BYTE_ASN_DISABLED_ERR,
    CLI_BGP4_ORF_IN_REQ_ERR,
    CLI_BGP4_INVALID_ORF_TYPE,
    CLI_BGP4_INVALID_ORF_MODE,
    CLI_BGP4_GR_DIS_ERR,
    CLI_BGP4_PEER_DELETE_PENDING_ERR,
    CLI_BGP4_NOT_READY_ERR,
    CLI_BGP4_BFD_NOT_ENABLED, 
    CLI_BGP4_INVALID_LOCAL_AS,
    CLI_BGP4_UPDATE_SOURCE_APPLICABLE_FOR_LOOPBACK,
    CLI_BGP4_PEER_GRP_REMOTE_AS,
    CLI_BGP4_INTF_REMOTE_AS,
    CLI_BGP4_INTF_EBGP_MULTIHOP,    
    CLI_BGP4_INVALID_LABEL_ALLOC_POLICY,
    CLI_BGP4_INVALID_TCP_PASSIVE_STATUS,
    CLI_BGP4_LINK_LOCAL_NOT_SUPPORTED,
    CLI_BGP4_UPDATE_SRC_UNSUPP_INVALID_ERR,
    CLI_BGP4_RMAP_ASSOC_ERROR,
    CLI_BGP4_PEER_GROUP_EMPTY,
    CLI_BGP4_PEER_GROUP_DIFF_ADDR_FAMILY,
    CLI_BGP4_FOUR_BYTE_ASN_CONFIGURED_REMOVAL_ERR,
    CLI_BGP4_NETWORK_ENTRY_NOT_FOUND,
    CLI_BGP4_NETWORK_ENTRY_ALREADY_EXISTS,
    CLI_BGP4_MAX_NETWORK_ENTRY,
    CLI_BGP4_IP_ROUTE_NOT_PRESENT,
    CLI_BGP4_NETWORK_IPV4_PREFIX_OUT_OF_RANGE,
    CLI_BGP4_NETWORK_IPV6_PREFIX_OUT_OF_RANGE,
    CLI_BGP4_NEXT_HOP_SELF_CONFIGURED,
    CLI_BGP4_GATEWAY_CONFIGURED,
    CLI_BGP4_MKT_NOT_ASSOCIATED,
    CLI_BGP4_INVALID_NETWORK_MASK,
    CLI_BGP4_INVALID_MULTICAST_MASK,
    CLI_BGP4_INVALID_BROADCAST_MASK,
    CLI_BGP4_INVALID_LOOPBACK_MASK,
    CLI_BGP4_INVALID_MASK,
    CLI_BGP4_INVALID_MULTICAST_BROADCAST_ADDRESS_ERR,
    CLI_BGP4_INVALID_LINK_LOCAL_MASK,
    CLI_BGP4_NO_MATCH_NTW_CONFIG,
    CLI_BGP4_PEER_GATEWAY_CONFIGURED,
    CLI_BGP4_PEER_GROUP_NEXT_HOP_SELF_CONFIGURED,
    CLI_BGP4_SHUT_OVERLAP_ASNUM_ERR,
    CLI_BGP4_SHUT_OVERLAP_NO_SHUT_ERR,
    CLI_BGP4_PREFIX_LIST_EXISTS_ERR,
    CLI_BGP4_EBGP_MULTIHOP_INTERNAL_PEER_ERR,
    CLI_BGP4_EBGP_MULTIHOP_INTERNAL_PEER_GROUP_ERR,
    CLI_BGP4_EBGP_MULTI_HOP_ENABLE_ERROR,
    CLI_BGP4_PREFIX_NAME_ADDR_FAMILY,
    CLI_BGP4_MAX_ERR
};



/* Error strings */

#ifdef __BGP4CLI_C__ 

CONST CHR1 *Bgp4CliErrString[] = {
    NULL,
    "\r% Bgp Task not initialized\r\n",
    "\r% Bgp Admin status is active\r\n",
    "\r% Invalid IPV6 Address\r\n",
    "\r% AS-no is invalid\r\n",
    "\r% Local AS not configured\r\n",
    "\r% Bgp Admin status is invalid\r\n",
    "\r% bgp identifier is invalid \r\n",
    "\r% Local preference is invalid \r\n",
    "\r% Invalid peer address\r\n",
    "\r% Invalid remote-AS\r\n",
    "\r% Invalid Next-hop value\r\n",
    "\r% Invalid Keep alive value\r\n",
    "\r% Invalid Hold time value\r\n",
    "\r% Invalid AS origination interval\r\n",
    "\r% Invalid Route advertisement interval\r\n",
    "\r% Invalid Connect retry interval \r\n",
    "\r% Peer already enabled\r\n",
    "\r% Peer not configured\r\n",
    "\r% Invalid non-bgp route advertisement config value\r\n",
    "\r% Invalid overlap route policy config value\r\n",
    "\r% Invalid redistribute protocol enable mask\r\n",
    "\r% Invalid syncronisation config value\r\n",
    "\r% Invalid Always-Compare MED Config Value\r\n",
    "\r% Invalid metric value\r\n",
    "\r% Invalid MED table index\r\n",
    "\r% Invalid MED table admin status\r\n",
    "\r% Invalid MED table entry prefix value\r\n",
    "\r% Invalid MED table entry prefix length\r\n",
    "\r% Invalid MED table entry remote-as \r\n",
    "\r% Invalid intermediate AS length\r\n",
    "\r% Invalid MED value\r\n",
    "\r% Invalid display string object\r\n",
    "\r% Invalid MED table entry direction value\r\n",
    "\r% Invalid LP table index\r\n",
    "\r% Invalid LP table admin status\r\n",
    "\r% Invalid LP table entry prefix value\r\n",
    "\r% Invalid LP table entry prefix length\r\n",
    "\r% Invalid LP table entry remote-as \r\n",
    "\r% Invalid LP table entry direction value \r\n",
    "\r% Invalid LP table entry LP value\r\n",
    "\r% Invalid filter table index\r\n",
    "\r% Invalid filter table admin status\r\n",
    "\r% Invalid filter table entry prefix value\r\n",
    "\r% Invalid filter table entry prefix length\r\n",
    "\r% Invalid filter table entry remote-as \r\n",
    "\r% Invalid filter table entry direction value \r\n",
    "\r% Invalid filter table entry action value \r\n",
    "\r% Invalid Cluster-id \r\n",
    "\r% Invalid Route Reflector Support Option Value\r\n",
    "\r% Invalid prefix/length value\r\n",
    "\r% Invalid community value\r\n",
    "\r% Invalid community incoming filter status\r\n",
    "\r% Invalid community outgoing filter status\r\n",
    "\r% Invalid community set status\r\n",
    "\r% Invalid extended community value\r\n",
    "\r% Invalid ext-community send status\r\n",
    "\r% Invalid ext-community incoming filter status\r\n",
    "\r% Invalid ext-comunity outgoing filter status\r\n",
    "\r% Invalid ext-community set status\r\n",
    "\r% Invalid confederation identifier\r\n",
    "\r% The peer AS number must not be equal to BGP Speaker's Local AS number \r\n",
    "\r% Exceeded maximum configurable peer as\r\n",
    "\r% Invalid confed peer status\r\n",
    "\r% Invalid confed best path compare MED option value\r\n",
    "\r% Invalid password set status\r\n",
    "\r% Unsupported address family value/Invalid peer address\n",
    "\r% Invalid password length\r\n",
    "\r% Invalid prefix length\r\n",
    "\r% Invalid prefix value\r\n",
    "\r% Invalid protocol value \r\n",
    "\r% Invalid next-hop value\r\n",
    "\r% Invalid interface index\r\n",
    "\r% Invalid metric value\r\n",
    "\r% Invalid action value\r\n",
    "\r% Invalid peer address length\r\n",
    "\r% Invalid route-refresh inbound request\r\n",
    "\r% Invalid Soft Reconfig outbound request\r\n",
    "\r% Invalid BGP trace value\r\n",
    "\r% Invalid comm peer entry send status \r\n",
    "\r% Invalid aggregate index\r\n",
    "\r% Invalid aggregate table entry prefix value\r\n",
    "\r% Invalid aggregate table entry admin status not in correct state\r\n",
    "\r% Invalid aggregate prefix value\r\n",
    "\r% Invalid aggregate advertise value\r\n",
    "\r% Invalid decay half life time value\r\n",
    "\r% Invalid RFD cutoff value\r\n",
    "\r% Invalid RFD reuse value\r\n",
    "\r% Maximum suppress time cannot be less than half life time,Dampening is disabled\r\n",
    "\r% Invalid RFD max hold time value\r\n",
    "\r% Invalid RFD decay timer granularity value\r\n",
    "\r% RFD decay timer granularity value greater than max hold time\r\n",
    "\r% RFD Reuse timer granularity value greater than one fourth of Half-life time\r\n",
    "\r% Invalid RFD reuse timer granularity value\r\n",
    "\r% BGP Admin status is down\r\n",
    "\r% Invalid reuse index array size value\r\n",
    "\r% ext-comunity entry already exists\r\n",
    "\r% Invalid intermediate-as value\r\n",
    "\r% RTM not registered\r\n",
    "\r% No matching peer exist\r\n",
    "\r% External peer reflector client not enabled\r\n",
    "\r% Peer not in established state\r\n",
    "\r% Exceeded maximum configurable peer \r\n",
    "\r% Route Map Not Associated \r\n",
    "\r% Route Map Not Disassociated \r\n",
    "\r% Route-map association failed (probably another route map is used)\r\n",
    "\r% Wrong distance value\r\n",
    "\r% Router ID not Set\r\n",
    "\r% Invalid peer connect retry time \r\n",
    "\r% Invalid tcp retry count \r\n",
    "\r% Invalid delay open interval \r\n",
    "\r% Invalid peer allow automatic start/stop status \r\n",
    "\r% Invalid damp peer oscillations status \r\n",
    "\r% Invalid peer delay open status \r\n",
    "\r% Invalid aggregate AS SET value \r\n",
    "\r% Remote AS is not configured for the peer group \r\n",
    "\r% Peer group is internal \r\n",
    "\r% Peer group is external \r\n",
    "\r% Peer group address family is different \r\n",
    "\r% Peer group does not exist \r\n",
    "\r% Peer is already member of another peer group \r\n",
    "\r% Invalid next hop processing interval \r\n",
    "\r% KeepAlive Value should be Less than configured Hold time \r\n",
    "\r% Invalid Address \r\n",
    "\r% Intermediate AS value cannot be Local-AS\r\n",
    "\r% Invalid Prefix Length \r\n", 
    "\r% Invalid Ecomm value \r\n", 
    "\r% Invalid gateway Address\r\n", 
    "\r% BGP Route Reflector cannot be enabled on External BGP peer.\r\n",
    "\r% Invalid Update Source Address \r\n",
    "\r% Invalid aggregate Address \r\n",
    "\r% Invalid IP address \r\n",
    "\r% Route not present in RIB \r\n",
    "\r% Invalid network address.\r\n",
    "\r% The Peer Address & Network Address should be in different family.\r\n",
    "\r% Unable to allocate AFI-SAFI instances for the peer\r\n",
    "\r% Exceeded maximum configurable entries\r\n",
    "\r% Entry doesn't exist\r\n",
    "\r% Invalid Match Type \r\n",
    "\r% Invalid Route-Map \r\n",
    "\r% Invalid RFD Admin Status \r\n",
    "\r% RFD not enabled \r\n",
    "\r% RFD max hold time exceeds the RFD decay array size \r\n",
    "\r% Maximum Intermediate-AS configuration Reached \r\n",
    "\r% Unsupported algorithm \r\n",
    "\r% Configuration of time is not supported in this release \r\n",
    "\r% Invalid TCP-AO MKT Key Id\r\n",
    "\r% TCP-AO is associated with peer. MD5 configuration not allowed\r\n",
    "\r% MD5 Authentication is enabled. TCP-AO configuration not allowed\r\n",
    "\r% TCP-AO MKT is already active\r\n",
    "\r% MKT can't be destroyed. Active neighbor associations present\r\n",
    "\r% Reserved AS Number - Cannot be configured \r\n",
    "\r% Invalid 4-Byte ASN Support status \r\n",
    "\r% 4-Byte ASN Support is disabled - AS no should be < 65536\r\n",
    "\r% Invalid 4-Byte ASN Notation Type \r\n",
    "\r% Enabling/Disabling of Notation type is not allowed when 4-Byte ASN is disabled\r\n",
    "\r% Invalid ORF inbound request\r\n",
    "\r% Invalid ORF Type\r\n",
    "\r% Invalid ORF Mode\r\n",
    "\r% GR cannot be disabled, when HA is enabled \r\n",
    "\r% Peer deletion is pending, peer creation failed \r\n",
    "\r% BGP4 is not ready to receive configurations \r\n",
    "\r% Unable to set BFD Status. BFD module is not enabled \r\n",
    "\r% Peer Local AS should not be as Remote AS or Local AS \r\n",
    "\r% Update Source is applicable only for loopback interface\r\n",
    "\r% Unmap the peers in group to change/remove the AS of Peer group\r\n",
    "\r% Unmap the peers from group before changing the AS\r\n",
    "\r% Unmap the peers from group before changing the ebgp-multihop\r\n",
    "\r% Invalid Label allocation policy\r\n",
    "\r% Invalid TCP Passive Status\r\n",
    "\r% Link-local address configuration not supported\r\n",
    "\r% Unsupported address family value/Invalid peer address/Unsupported for peer-group\n",
    "\r% Route-Map already associated.Unconfigure route-map to configure new one\r\n",
    "\r% PeerGroup is Empty. No peers configured for this group\n",
    "\r% PeerGroup Address Family is differnt.\n",
    "\r% 4-Byte ASN Configured. Removal not supported\r\n",
    "\r% Network route is not present the Network route list \r\n",
    "\r% Network configuration already exists\r\n",
    "\r% Exceeds Maximum Network Routes \r\n",
    "\r% Configured network is not present in IP routing\r\n",
    "\r% Invalid IPv4 prefix length\r\n",
    "\r% Invalid IPv6 prefix length\r\n",
    "\r% Next-hop-self configured. Gateway configuration not allowed.\r\n",
    "\r% Gateway configured. next-hop-self configuration not allowed.\r\n",
    "\r% MKT value is not already associated.\r\n", 
    "\r% Incorrect network or mask/prefix-length configured.\r\n",
    "\r% multicast address configuration not allowed\r\n",
    "\r% Broadcast address configuration not allowed.\r\n",
    "\r% Loopback address configuration not allowed.\r\n",
    "\r% Invalid IP address configuration.\r\n",
    "\r% Broadcast/Multicast/Invalid address configuration not allowed.\r\n",
    "\r% Link local address configuration not allowed.\r\n",
    "\r% No matching network configuration.\r\n",
    "\r% Gateway configured for member peer. Next-hop-self configuration not allowed for peer group.\r\n",
    "\r% Next-hop-self configured for peer group. Gateway configuration not allowed.\r\n",
    "\r% Overlap-policy configuration is not allowed as as-num is not set.\r\n",
    "\r% Overlap-policy configuration is not allowed as bgp is not shutdown.\r\n",
    "\r% Prefix List with the same name exists.\r\n",
    "\r% ebgp multihop configuration not allowed for internal peer. \r\n",
    "\r% ebgp multihop configuration not allowed for internal peer group. \r\n",
    "\r% Unconfigure ebgp multihop before changing remote-as \r\n",
    "\r% Prefix-list family type doesn't match with neighbor address family.\r\n",
    "\r\n"
};
#endif

/* bit-mask for route status */
#define BGP_CLI_ROUTE_SUPPRESSED                            (0x00000001)
#define BGP_CLI_ROUTE_DAMPED                                (0x00000002)
#define BGP_CLI_ROUTE_HISTORY                               (0x00000004)
#define BGP_CLI_ROUTE_VALID                                 (0x00000080)
#define BGP_CLI_ROUTE_BEST                                  (0x00000010)
#define BGP_CLI_ROUTE_INTERNAL                              (0x00000020)
#define BGP_CLI_ROUTE_MPATH                                 (0x00000040)
#define BGP_CLI_ROUTE_STALE                                 (0x80000000)
#define BGP_CLI_ROUTE_INVALID                               (0x00000008)
#define BGP_CLI_ROUTE_AGGR                                  (0x01000000)
#define BGP_CLI_ROUTE_ATOMIC_AGGR                           (0x02000000)

/* bit-mask for capabilities */
#define BGP_CLI_CAP_ROUTE_REFRESH                           (0x00000001)
#define BGP_CLI_CAP_IPV4_UNICAST                            (0x00010000)
#define BGP_CLI_CAP_IPV4_MULTICAST                          (0x00020000)
#define BGP_CLI_CAP_IPV6_UNICAST                            (0x00040000)
#define BGP_CLI_CAP_IPV6_MULTICAST                          (0x00080000)
#define BGP_CLI_CAP_4_OCTET_ASNO                            (0x00100000)

/* max number of AS paths that will be shown */
#define BGP_CLI_MAX_AS_PATHS                                (64)
#define BGP_CLI_MAX_FOUR_BYTE_ASN                           (0xFFFFFFFF)
#define BGP_CLI_MAX_TWO_BYTE_ASN                            (0xFFFF)
#define BGP_CLI_IP_ADDR_STRING_LEN                          (50)
#define BGP_CLI_PRINT_BUF_LEN                               (81)
#define BGP_CLI_ASPATH_DISP_RESTRICT                        (5)
#define BGP_CLI_NEW_LINE_LEN                                (3)
#define BGP_CLI_DISPLAY_TIME_BUF                            (50)


/* This value needs to be updated if any new command is added to the list */
#define     BGP4_CLI_MAX_COMMANDS                   (41)

/* This value defines the maximum number of argument accepted by any bgp 
 * command */
#define     BGP4_CLI_MAX_ARGS                       (11)

/* Definitions used by FutureBGP4 CLI */    
    
/* Should be same as that of the BGP4_MAX_TEMP_ASARRAY_SIZE */    
#define BGP4_MAX_ASARRAY_SIZE                       (120)

/* Should be same as EXT_COMM_VALUE_LEN */
#define ECOMM_LEN                                   (8)
#define ECOMM_EXT_LEN                               (4)
#define RECV_COMM_LEN                               (800)
#define RECV_EXT_COMM_LEN                           (800)
#define INT_AS_LEN                                  (120)
#define CLUS_ID_LEN                                 (4)
#define CLI_BGP4_ENABLE                             (1)

/* Maximum number of routes that needs to the received in
 * a single call. */
#define BGP4_CLI_MAX_ROUTES                         (3)
    
/* Maximum number of Peers that needs to the received in
 * a single call. */
#define BGP4_CLI_MAX_PEERS                          (3)


/*type defs*/
#define MMI_MULTI_PRINT mmi_multi_more_printf
#define  BGP4_INET_ATON(pc1Addr,pAddr) BgpValidateIpAddr((const CHR1 *)pc1Addr,pAddr)
#define  BGP4_INET_VALID(pc1Addr,pAddr) BgpValidateIpv4Addr((const CHR1 *)pc1Addr,pAddr)

# define CLI_MODE_BGP            "bgp-conf"
# define CLI_MODE_AF4_BGP        "bgp-conf-af4"
# define CLI_MODE_AF6_BGP        "bgp-conf-af6"
# define CLI_MODE_AF_VPNV4_BGP   "bgp-conf-afvpnv4"
#ifdef VPLSADS_WANTED
# define CLI_MODE_AFL2VPN_BGP    "bgp-conf-afl2vpn"
#endif
#ifdef EVPN_WANTED
# define CLI_MODE_AF_EVPN_BGP    "bgp-conf-afevpn"
#endif

#define BGP_CLI_CHECK_AND_THROW_FATAL_ERROR(CliHandle) ISSCliCheckAndThrowFatalError(CliHandle)

#define BGP_CLI_CHECK_AND_THROW_FATAL_ERROR(CliHandle) ISSCliCheckAndThrowFatalError(CliHandle)

/* typedef for FutureBGP4 CLI */

#define BGP4_CLI_VAL_NO          (0)
#define BGP4_CLI_VAL_SET         (1)
#define BGP4_CLI_VAL_INVALID     (2)
#define ALL_PEERS_SET            (1)
#define ALL_PEERS_NO             (2)
#define DIR_IN                   (1)
#define DIR_OUT                  (2)
#define BGP4_CLI_NBR_SPECIFIED   (1)
#define BGP4_CLI_RIB_SPECIFIED   (1)
/* added for CLCLI */
#define SET_KEEP_ALIVE         (0)
#define SET_HOLD_TIME          (1)
#define RT_ADVT_INT            (0)
#define AS_ORIG_INT            (1)
#define CONN_RETRY_INT         (2)
#define BGP4_ADD  1
#define BGP4_DELETE  2
#define IN  1
#define OUT  2
#define STANDARD  1
#define EXTENDED  2
#define ECOMM_STR_LEN  24
#define BGP_CLI_IP_SHOW_IP      1
#define BGP_CLI_IP_SHOW_IP_PREF 2
#define BGP4_RFD_FLAP_STAT      3
#define BGP4_RFD_DAMPED_PATH    4
/* added for CLCLI */

/* This should be same as that of CLI_SUCCESS/CLI_FAILURE and
 * SNMP_SUCCESS/SNMP_FAILURE */
#define BGP4_CLI_SUCCESS    SNMP_SUCCESS
#define BGP4_CLI_FAILURE    SNMP_FAILURE 

/* BGP CLI Error Code */
#define BGP4_CLI_MORE                                       (-2)
#define BGP4_CLI_ERR_GENERAL_ERROR                          (1)
#define BGP4_CLI_ERR_MEM_ALLOCATE_FAIL                      (2)
#define BGP4_CLI_ERR_UNSUPPORTED_COMMAND                    (3)
#define BGP4_CLI_ERR_BGP_ADMIN_UP                           (4)
#define BGP4_CLI_ERR_BGP_ADMIN_DOWN                         (5)
#define BGP4_CLI_ERR_INVALID_AS_NO                          (6)
#define BGP4_CLI_ERR_INVALID_BGP_IDENTIFIER                 (7)
#define BGP4_CLI_ERR_INVALID_LOCAL_PREF                     (8)
#define BGP4_CLI_ERR_BGP_INACTIVE                           (9)
#define BGP4_CLI_ERR_BGP_AS_MISMATCH                        (10)
#define BGP4_CLI_ERR_PEER_INVALID_AS_NO                     (11)
#define BGP4_CLI_ERR_PEER_INVALID_EBGP_MULTIHOP             (12)
#define BGP4_CLI_ERR_PEER_INVALID_SELF_NEXTHOP              (13)
#define BGP4_CLI_ERR_PEER_CREATE_FAIL                       (14)
#define BGP4_CLI_ERR_PEER_DELETE_FAILS                      (15)
#define BGP4_CLI_ERR_INV_NON_BGP_ADVT_POLICY                (16)
#define BGP4_CLI_ERR_INV_OVERLAP_POLICY                     (17)
#define BGP4_CLI_ERR_PROTO_REDIS_NOT_SUPPORTED              (18)
#define BGP4_CLI_ERR_MED_TBL_INVALID_INDEX                  (19)
#define BGP4_CLI_ERR_MED_TBL_INVALID_IP_PREFIX              (20)
#define BGP4_CLI_ERR_MED_TBL_INVALID_IP_PREFIX_LEN          (21)
#define BGP4_CLI_ERR_MED_TBL_INVALID_REMOTE_AS              (22)  
#define BGP4_CLI_ERR_MED_TBL_INVALID_MED_VALUE              (23) 
#define BGP4_CLI_ERR_MED_TBL_INVALID_MED_DIRECTION          (24)
#define BGP4_CLI_ERR_MED_TBL_INVALID_MED_OVERRIDING_VALUE   (25)
#define BGP4_CLI_ERR_MED_TBL_INVALID_MED_INTERMEDIATE_AS    (26)
#define BGP4_CLI_ERR_LP_TBL_INVALID_INDEX                   (27)
#define BGP4_CLI_ERR_LP_TBL_INVALID_IP_PREFIX               (28)
#define BGP4_CLI_ERR_LP_TBL_INVALID_IP_PREFIX_LEN           (29)
#define BGP4_CLI_ERR_LP_TBL_INVALID_REMOTE_AS               (30)  
#define BGP4_CLI_ERR_LP_TBL_INVALID_LP_VALUE                (31) 
#define BGP4_CLI_ERR_LP_TBL_INVALID_LP_DIRECTION            (32)
#define BGP4_CLI_ERR_LP_TBL_INVALID_LP_OVERRIDING_VALUE     (33)
#define BGP4_CLI_ERR_LP_TBL_INVALID_LP_INTERMEDIATE_AS      (34)
#define BGP4_CLI_ERR_FILTER_TBL_INVALID_INDEX               (35)
#define BGP4_CLI_ERR_FILTER_TBL_INVALID_ACTION              (36)
#define BGP4_CLI_ERR_FILTER_TBL_INVALID_IP_PREFIX           (37)
#define BGP4_CLI_ERR_FILTER_TBL_INVALID_IP_PREFIX_LEN       (38)
#define BGP4_CLI_ERR_FILTER_TBL_INVALID_REMOTE_AS           (39)  
#define BGP4_CLI_ERR_FILTER_TBL_INVALID_DIRECTION           (40)
#define BGP4_CLI_ERR_FILTER_TBL_INVALID_INTERMEDIATE_AS     (41)
#define BGP4_CLI_ERR_AGGR_TBL_INVALID_INDEX                 (42)
#define BGP4_CLI_ERR_AGGR_TBL_INVALID_IP_PREFIX             (43)
#define BGP4_CLI_ERR_AGGR_TBL_INVALID_IP_PREFIX_LEN         (44)
#define BGP4_CLI_ERR_AGGR_TBL_INVALID_POLICY                (45)
#define BGP4_CLI_ERR_SET_AGGR_ADDR_FAIL                     (46)
#define BGP4_CLI_ERR_SET_AGGR_ADMIN_FAIL                    (47)
#define BGP4_CLI_ERR_SET_AGGR_POLICY_FAIL                   (48)
#define BGP4_CLI_ERR_DAMP_INV_DECAY_HALFLIFE_TIME           (49)
#define BGP4_CLI_ERR_DAMP_INV_REUSE_VAL                     (50)
#define BGP4_CLI_ERR_DAMP_INV_SUPPRESS_VAL                  (51)
#define BGP4_CLI_ERR_DAMP_INV_MAX_SUPPRESS_TIME             (52)
#define BGP4_CLI_ERR_DAMP_INV_DECAY_GRANULARITY             (53)
#define BGP4_CLI_ERR_DAMP_INV_REUSE_GRANULARITY             (54)
#define BGP4_CLI_ERR_DAMP_INV_REUSE_INDEX_ARRAY_SIZE        (55)
#define BGP4_CLI_ERR_RFL_CLUSTER_ID_NOT_SET                 (56)
#define BGP4_CLI_ERR_RFL_INV_CLUSTER_ID                     (57)
#define BGP4_CLI_ERR_RFL_INV_RFL_SUPPORT                    (58)
#define BGP4_CLI_ERR_INV_PEER_ADDR                          (59)
#define BGP4_CLI_ERR_RFL_PEER_NOT_PRESENT                   (60)
#define BGP4_CLI_ERR_COMM_INV_ACTION                        (61)
#define BGP4_CLI_ERR_COMM_INV_DIRECTION                     (62)
#define BGP4_CLI_ERR_COMM_INV_COMM_VALUE                    (63)
#define BGP4_CLI_ERR_ECOMM_INV_ACTION                       (64)
#define BGP4_CLI_ERR_ECOMM_INV_DIRECTION                    (65)
#define BGP4_CLI_ERR_ECOMM_INV_ECOMM_VALUE                  (66)
#define BGP4_CLI_ERR_INV_COMP_MED_VALUE                     (67)
#define BGP4_CLI_ERR_INV_DEF_METRIC_VALUE                   (68)

/* For Route Refresh/Soft-Reconfig IN/OUT */
#define BGP4_CLI_ERR_ALLPEER_SOFTRECONFIG_FAILURE           (69)
#define BGP4_CLI_ERR_RTREF_INVALID_INPUT                    (70)
#define BGP4_CLI_ERR_RTREF_COMMIT_FAILED                    (71)
#define BGP4_CLI_ERR_RTREF_INVALID_INDEX                    (72)
#define BGP4_CLI_ERR_SOFTRECONFIG_OUTBOUND_INVALID_INPUT    (73)
#define BGP4_CLI_ERR_SOFTRECONFIG_OUTBOUND_INVALID_INDEX    (74)
#define BGP4_CLI_ERR_SOFTRECONFIG_OUTBOUND_COMMIT_FAILED    (75)

/* For TCP MD5 */
#define BGP4_CLI_ERR_INVALID_PASSWORD_OR_PEERADDRESS        (76)
#define BGP4_CLI_ERR_TCPMD5_COMMIT_FAILED                   (77)
#define BGP4_CLI_ERR_TCPMD5_INVALID_INDEX                   (78)
#define BGP4_CLI_ERR_TCPMD5_PASSWORD_ALREADY_EXISTS         (79)

/* Confed Error codes */
#define BGP4_CLI_ERR_CONFED_INACTIVE                        (80)
#define BGP4_CLI_ERR_CONFED_ID_MISMATCH                     (81)
#define BGP4_CLI_ERR_INVALID_CONFED_ID                      (82)
#define BGP4_CLI_ERR_INVALID_CONFED_MED_INPUT               (83)
#define BGP4_CLI_ERR_INVALID_CONFED_PEER                    (84)
#define BGP4_CLI_ERR_INVALID_CONFED_INDEX                   (85)
#define BGP4_CLI_ERR_BGP_ALREADY_REG_TO_RTM                 (86)

#define BGP4_CLI_ERR_PEER_INVALID_KEEP_ALIVE_TIME           (87)
#define BGP4_CLI_ERR_PEER_INVALID_HOLD_TIME                 (88)
#define BGP4_CLI_ERR_UNSUPPORTED_TRACE                      (89)
#define BGP4_CLI_ERR_PEER_INVALID_SRCIF_ID                  (90)
#define BGP4_CLI_ERR_PEER_INVALID_MIN_ADVT_INT              (91)
#define BGP4_CLI_ERR_PEER_INVALID_MIN_ORIG_INT              (92)
#define BGP4_CLI_ERR_PEER_INVALID_CONN_RETRY_INT            (93)

#define BGP4_CLI_ERR_INVALID_LOCAL_ADDR                     (94)
#define BGP4_CLI_ERR_INVALID_CAPVALUE                       (95)
#define BGP4_CLI_ERR_PEER_INVALID_SHUTDOWN                  (96)
#define BGP4_CLI_ERR_PEER_SET_MIN_ADVT_INT_FAIL             (97)
#define BGP4_CLI_ERR_PEER_SET_MIN_ORIG_INT_FAIL             (98)
#define BGP4_CLI_ERR_PEER_SET_CONN_RETRY_INT_FAIL           (99)
#define BGP4_CLI_ERR_PEER_SET_KEEP_ALIVE_TIME_FAIL          (100)
#define BGP4_CLI_ERR_PEER_SET_HOLD_TIME_FAIL                (101)
#ifdef L3VPN
#define BGP4_CLI_ERR_VRF_ROUTE_TARGET_CREATE_FAIL           (102)
#define BGP4_CLI_ERR_VRF_ROUTE_TARGET_DELETE_FAIL           (103)
#define BGP4_CLI_ERR_PEER_INVALID_CE_RT_ADVT                (104)
#define BGP4_CLI_ERR_PEER_INVALID_SEND_LABEL                (105)
#define BGP4_CLI_ERR_PEER_INVALID_CE_PEER                   (106)
#define BGP4_CLI_ERR_PEER_INVALID_SOO                       (107)
#define BGP4_CLI_ERR_VRF_INVALID_INPUT                      (108)
#define BGP4_CLI_ERR_INVALID_ADDR_FAMILY                     (109)
#define BGP4_CLI_ERR_VPN4_FAMLY_NBR_CONFIG_FAIL             (110)
#define BGP4_CLI_ERR_LBL_IPV4_CONFIG_FAIL                    (111)
#define BGP4_CLI_ERR_VPNV4_CONFIG_FAIL                       (112)
#define BGP4_CLI_ERR_IPV4_CONFIG_FAIL                        (113)
#endif
/* origin codes */
#define BGP_CLI_ORIGIN_IGP                                  (0x01)
#define BGP_CLI_ORIGIN_EGP                                  (0x02)
#define BGP_CLI_ORIGIN_INCOMPLETE                           (0x03)

/* Alignment codes */
#define BGP4_CLI_LEFT_ALIGN                                  0x01
#define BGP4_CLI_RIGHT_ALIGN                                 0x02


/* macro to convert prefix length to network mask */
#define BGP_PREFIX_LEN_TO_MASK(u4Len, u4Mask)               \
        {                                                   \
            UINT4 u4Num=0x80000000;                         \
            INT4  i4Tmp;                                    \
            for (i4Tmp=0; i4Tmp < (u4Len); i4Tmp++)         \
            {                                               \
                (u4Mask) |= u4Num;                          \
                u4Num >>= 1;                                \
            }                                               \
        }
/* convert a hex ip address to string */
#define BGP_CLI_IPADDR_HEX2STR(u4IpAddr, ai1IpAddr)         \
        {                                                   \
            UINT4 u4Mask=0xFF000000;                        \
            UINT4 u4Num;                                    \
            UINT4 ai4Addr[4];                               \
            ai4Addr[0] = u4IpAddr;                          \
            for (u4Num=0; u4Num < 4; u4Num++)               \
            {                                               \
                (ai4Addr)[u4Num] = (((u4IpAddr & u4Mask) >> \
                            (24 - (u4Num*8))) & 0x000000FF);\
                u4Mask >>= 8;                               \
            }                                               \
            SPRINTF ((CHR1 *)ai1IpAddr, "%d.%d.%d.%d", ai4Addr[0],\
                        ai4Addr[1], ai4Addr[2], ai4Addr[3]);\
        }

/* macro for converting secs to printable format */
#define BGP_CLI_ONE_DAY  (24*60*60)
#define BGP_CLI_ONE_HOUR (60*60)
#define BGP_CLI_ONE_MIN  (60)

#define BGP4_CLI_NETWORK_ENABLE  (1)
#define BGP4_CLI_NETWORK_DISABLE (0)

#define BGP_CLI_SECS_TO_STR(u4Seconds, ai1String) \
        { \
            UINT4 u4Secs=u4Seconds, u4Days, u4Hrs, u4Mts; \
            u4Days = (u4Secs)/BGP_CLI_ONE_DAY; \
            if (u4Days) \
            { \
         u4Secs -= (u4Days * BGP_CLI_ONE_DAY); \
            } \
            u4Hrs = u4Secs/BGP_CLI_ONE_HOUR; \
            if (u4Hrs) \
            { \
         u4Secs -= (u4Hrs * BGP_CLI_ONE_HOUR); \
            } \
            u4Mts = u4Secs/BGP_CLI_ONE_MIN; \
            if (u4Mts) \
            { \
         u4Secs -= (u4Mts * BGP_CLI_ONE_MIN); \
            } \
            ai1String[0]=0; \
            if (u4Days) \
            { \
                SPRINTF ((CHR1 *)ai1String, "%d %s ", u4Days, \
                                (u4Days == 1 ? "day" : "days")); \
            }\
            if (u4Hrs) \
            { \
                SPRINTF ((CHR1 *)&ai1String[STRLEN(ai1String)], \
                                "%d %s ", u4Hrs, \
                                (u4Hrs == 1 ? "hour" : "hours")); \
            } \
            if (u4Mts) \
            { \
                SPRINTF ((CHR1 *)&ai1String[STRLEN(ai1String)], \
                                "%d %s ", u4Mts, \
                                (u4Mts == 1 ? "minute" : "minutes")); \
            } \
            SPRINTF ((CHR1 *)&ai1String[STRLEN(ai1String)], \
                                "%d %s", u4Secs, \
                                (u4Secs == 1 ? "second" : "seconds")); \
        }

#define BGP_CLI_SECS_TO_STRING(u4Seconds, ai1String) \
        { \
            UINT4 u4Secs=u4Seconds, u4Days, u4Hrs, u4Mts; \
            u4Days = (u4Secs)/BGP_CLI_ONE_DAY; \
            if (u4Days) \
            { \
         u4Secs -= (u4Days * BGP_CLI_ONE_DAY); \
            } \
            u4Hrs = u4Secs/BGP_CLI_ONE_HOUR; \
            if (u4Hrs) \
            { \
         u4Secs -= (u4Hrs * BGP_CLI_ONE_HOUR); \
            } \
            u4Mts = u4Secs/BGP_CLI_ONE_MIN; \
            if (u4Mts) \
            { \
         u4Secs -= (u4Mts * BGP_CLI_ONE_MIN); \
            } \
            ai1String[0]=0; \
            if (u4Days) \
            { \
                SPRINTF ((CHR1 *)ai1String, "%.2d:", u4Days); \
            }\
            else\
            {\
                SPRINTF ((CHR1 *)ai1String, "00:");\
            }\
            if (u4Hrs) \
            { \
                SPRINTF ((CHR1 *)&ai1String[STRLEN(ai1String)], \
                                "%.2d:", u4Hrs); \
            } \
            else\
            {\
                SPRINTF ((CHR1 *)&ai1String[STRLEN(ai1String)],"00:");\
            }\
            if (u4Mts) \
            { \
                SPRINTF ((CHR1 *)&ai1String[STRLEN(ai1String)], \
                                "%.2d:", u4Mts); \
            } \
            else\
            {\
                SPRINTF ((CHR1 *)&ai1String[STRLEN(ai1String)],"00:");\
            }\
            SPRINTF ((CHR1 *)&ai1String[STRLEN(ai1String)], \
                                "%.2d", u4Secs); \
        }
/* bit-mask for Peer Operation Status */
#define BGP_CLI_PEND_MULTIHOP                               (0x00000001)
#define BGP_CLI_PEND_MP_CONFIG_CAPS                         (0x00000002)
#define BGP_CLI_PEND_MP_RECV_CAPS                           (0x00000004)

#define BGP4_CLI_IPV4_UNICAST     (1)
#define BGP4_CLI_IPV6_UNICAST     (2)
#define BGP4_CLI_VPNV4_UNICAST    (3)
#define BGP4_CLI_LABELLED_IPV4    (4)
#define BGP4_CLI_ROUTE_REFRESH    (5)
#define BGP4_CLI_ORF_PREFIXLIST   (6)
#define BGP4_CLI_L2VPN_VPLS       (7)

/* bit-mask for capabilities */
#define BGP_CLI_CAP_ROUTE_REFRESH                           (0x00000001)
#define BGP_CLI_CAP_IPV4_UNICAST                            (0x00010000)
#define BGP_CLI_CAP_IPV4_MULTICAST                          (0x00020000)
#define BGP_CLI_CAP_IPV6_UNICAST                            (0x00040000)
#define BGP_CLI_CAP_IPV6_MULTICAST                          (0x00080000)
#define BGP_CLI_CAP_VPN4_UNICAST                            (0x00100000)
#define BGP_CLI_CAP_LBLD_IPV4_UNICAST                       (0x00200000)
#define BGP_CLI_CAP_IPV4_ORF_SEND                           (0x00400000)
#define BGP_CLI_CAP_IPV4_ORF_RECEIVE                        (0x00800000)
#define BGP_CLI_CAP_IPV6_ORF_SEND                           (0x01000000)
#define BGP_CLI_CAP_IPV6_ORF_RECEIVE                        (0x02000000)
#define BGP_CLI_CAP_L2VPN_VPLS                              (0x04000000)



#ifdef BGP4_IPV6_WANTED
#define BGP_CLI_FILL_IP_ADDR_PEER_GROUP(IpAddr,Ip6Addr,PeerGroup,pu1Peer,u4AddrType) \
{ \
                if (NULL != IpAddr) \
                { \
                    u4AddrType = BGP4_INET_AFI_IPV4; \
                    pu1Peer = IpAddr; \
                } \
                else if (NULL != Ip6Addr) \
                { \
                    u4AddrType = BGP4_INET_AFI_IPV6; \
                    pu1Peer = Ip6Addr; \
                } \
                else if (NULL != PeerGroup) \
                { \
                    pu1Peer = PeerGroup; \
                    if (Bgp4ValidatePeerGroupName (pu1Peer) == BGP4_FAILURE) \
                    { \
                        mmi_printf ("%% Invalid Peer IP Address/Group Name\r\n"); \
                    } \
                } \
}

#define BGP_CLI_CHECK_IP_ADDR_PEER_GROUP(u1AddrType,u4IpAddress,AddrGrp,pPeerRemoteAddr,au1PeerGrp) \
{ \
 tIp6Addr Ip6Address; \
 UINT4  u4TempAddress = ZERO; \
 if (NULL != AddrGrp) \
 { \
  if (BGP4_INET_AFI_IPV4 == u1AddrType) \
  { \
   MEMCPY (&u4TempAddress, AddrGrp, sizeof (UINT4)); \
   u4IpAddress = OSIX_HTONL (u4TempAddress); \
   pPeerRemoteAddr->u2Afi = BGP4_INET_AFI_IPV4; \
   MEMCPY ((pPeerRemoteAddr->au1Address), \
     &u4IpAddress, sizeof (UINT4)); \
   pPeerRemoteAddr->u2AddressLen = sizeof (UINT4); \
  } \
  else if (BGP4_INET_AFI_IPV6 == u1AddrType) \
  { \
   pPeerRemoteAddr->u2Afi = BGP4_INET_AFI_IPV6; \
   if (ZERO != INET_ATON6 (AddrGrp,&Ip6Address)) \
   { \
    MEMCPY (pPeerRemoteAddr->au1Address, \
      &Ip6Address, BGP4_IPV6_PREFIX_LEN); \
    pPeerRemoteAddr->u2AddressLen = BGP4_IPV6_PREFIX_LEN; \
   } \
  } \
  else \
  { \
   MEMCPY (au1PeerGrp, AddrGrp, STRLEN (AddrGrp)); \
  } \
 } \
}
#else
#define BGP_CLI_FILL_IP_ADDR_PEER_GROUP(IpAddr,Ip6Addr,PeerGroup,pu1Peer,u4AddrType) \
{ \
                if (NULL != IpAddr) \
                { \
                    u4AddrType = BGP4_INET_AFI_IPV4; \
                    pu1Peer = IpAddr; \
                } \
                else if (NULL != Ip6Addr) \
                { \
                    mmi_printf ("%% IPv6 is not enabled %%\r\n"); \
                } \
                else if (NULL != PeerGroup) \
                { \
                    pu1Peer = PeerGroup; \
                    if (Bgp4ValidatePeerGroupName (pu1Peer) == BGP4_FAILURE) \
                    { \
                        mmi_printf ("%% Invalid Peer IP Address/Group Name %%\r\n"); \
                    } \
                } \
}

#define BGP_CLI_CHECK_IP_ADDR_PEER_GROUP(u1AddrType,u4IpAddress,AddrGrp,pPeerRemoteAddr,au1PeerGrp) \
{ \
 UINT4  u4TempAddress = 0; \
 if (NULL != AddrGrp) \
 { \
  if (BGP4_INET_AFI_IPV4 == u1AddrType) \
  { \
   MEMCPY (&u4TempAddress, AddrGrp, sizeof (UINT4)); \
   u4IpAddress = OSIX_HTONL (u4TempAddress); \
   pPeerRemoteAddr->u2Afi = BGP4_INET_AFI_IPV4; \
   MEMCPY ((pPeerRemoteAddr->au1Address), \
     &u4IpAddress, sizeof (UINT4)); \
   pPeerRemoteAddr->u2AddressLen = sizeof (UINT4); \
  } \
  else if (BGP4_INET_AFI_IPV6 == u1AddrType) \
  { \
   mmi_printf ("\r\n%% IPv6 is not enabled %%\r\n"); \
  } \
  else \
  { \
   MEMCPY (au1PeerGrp, AddrGrp, STRLEN (AddrGrp)); \
  } \
 } \
}
#endif

/* Data type Declarations for Future BGP4 CLI */

typedef struct 
{
    UINT4       *pu4PeerRemoteAddr;
    UINT1       *pu1PeerGroupName;
    UINT2        u2PeerRemoteAS;
    UINT2        u2Pad;
}tBgp4PeerRemoteASConfig;

typedef struct
{
    UINT1        au1InterAS[BGP4_MAX_ASARRAY_SIZE];
    UINT4        u4Direction;
    UINT4        u4Index;
    UINT4        u4RemoteAS;
    UINT4        u4IpAddr;
    UINT4        u4IpMask;
    UINT4        u4MedLpValue;
    UINT4        u4Override;
}tBgp4MEDLPTableConfig;

typedef struct
{
    UINT1        au1InterAS[BGP4_MAX_ASARRAY_SIZE];
    UINT4        u4Action;
    UINT4        u4Direction;
    UINT4        u4Index;
    UINT4        u4RemoteAS;
    UINT4        u4IpAddr;
    UINT4        u4IpMask;
}tBgp4FilterTableConfig;

typedef struct
{
    UINT4        u4Index;
    UINT4        u4IpAddr;
    UINT4        u4IpMask;
    UINT4        u4Action;
}tBgp4AggrTableConfig;

typedef struct
{
    UINT4        *pu4HalfLife;
    UINT4        *pu4Reuse;
    UINT4        *pu4Suppress;
    UINT4        *pu4MaxSuppressTime;
    UINT4        *pu4DecayGranularity;
    UINT4        *pu4ReuseGranularity;
    UINT4        *pu4ReuseIndexArraySize;
}tBgp4DampConfig;

typedef struct
{
    UINT4         u4Action;
    UINT4         u4IpAddr; 
    UINT4         u4IpMask;
    UINT4         u4CommVal;
}tBgp4CommRouteTblConfig;

typedef struct
{
    UINT4         u4Action;
    UINT4         u4PeerAddr; 
}tBgp4CommPeerTblConfig;

typedef struct
{
    UINT4         u4Action;
    UINT4         u4Direction;
    UINT4         u4CommVal; 
}tBgp4CommFilterTblConfig;

typedef struct
{
    UINT4         u4Action;
    UINT4         u4IpAddr; 
    UINT4         u4IpMask;
}tBgp4CommPolicyTblConfig;

typedef struct
{
    UINT4         au4EcommVal[ECOMM_LEN+ECOMM_EXT_LEN];
    UINT4         u4Action;
    UINT4         u4IpAddr; 
    UINT4         u4IpMask;
}tBgp4EcommRouteTblConfig;

typedef struct
{
    UINT4         u4Action;
    UINT4         u4PeerAddr; 
}tBgp4EcommPeerTblConfig;

typedef struct
{
    UINT4         au4EcommVal[ECOMM_LEN+ECOMM_EXT_LEN];
    UINT4         u4Action;
    UINT4         u4Direction;
}tBgp4EcommFilterTblConfig;

typedef struct
{
    UINT4         u4Action;
    UINT4         u4IpAddr; 
    UINT4         u4IpMask;
}tBgp4EcommPolicyTblConfig;

typedef struct
{
    UINT4         u4IpAddr;                 
    UINT1         u1NeigDisp;    /* will be 1 if neighbor is specified
                                  * in the CLI command else 0 */
    UINT1         u1RibDisp;     /* will be 1 if Local Rib Table is specified
                                  * in the CLI command else 0 */
    UINT1         au1Pad[2];
}tBgp4ShowIpBgp;

typedef struct _BgpClearIpBgp
{
   UINT1        au1Address[4];
   UINT1        *pu1PeerGroup;
   INT4         i4PeerType; 
   UINT1        u1AllPeerSet;
   UINT1        u1SoftFlag;
   UINT1        u1PolicyDir;
   UINT1        u1Pad;
}tBgp4ClearIpBgpConfig;

typedef struct _BgpTcpMd5Pwd
{
    UINT1        au1Passwd[80];
    UINT1        au1Address[4];
    UINT1       *pu1PeerGroup;
    INT4         i4PeerType;
    UINT1        u1PwdLen;
    UINT1        au1Pad[3];
}tBgp4TcpMd5Pwd;

typedef struct _BgpImportRoute
{
    UINT4       u4Prefix;
    UINT4       u4NextHop;
    INT4        i4PrefLen;
    INT4        i4Metric;
    INT4        i4Index;
    INT4        i4Protocol;
    INT4        i4NumRts;
}tBgp4ImportRoute;

typedef struct _Bgp4PeerIntConfig
{
    UINT4        *pu4IpAddr;
    UINT1        *pu1PeerGroupName;
    INT4         *pi4OrigInt;
    INT4         *pi4AsAdvInt;
    INT4         *pi4ConTryInt;
}tBgp4PeerIntConfig;

typedef struct _Bgp4PeerTmrConfig
{
    UINT4        *pu4IpAddr;
    UINT1        *pu1PeerGroupName;
    INT4         *pi4KeepAliveTime;
    INT4         *pi4HoldTime;
}tBgp4PeerTmrConfig;

typedef struct _Bgp4PeerShutDownConfig
{
    UINT4       *pu4PeerRemoteAddr;
    UINT1       *pu1PeerGroupName;
}tBgp4PeerShutDownConfig;

typedef struct _Bgp4Trace
{
    UINT1   u1PeerTrace;
    UINT1   u1UpdTrace;
    UINT1   u1FdbTrace;
    UINT1   u1KeepTrace;
    UINT1   u1InTrace;
    UINT1   u1OutTrace;
    UINT1   u1DampTrace;
    UINT1   u1EventTrace;
    UINT1   u1AllTrace;
    UINT1   au1Pad[3];
}tBgp4Trace;
typedef union _Bgp4Ecomm
{
    tBgp4EcommRouteTblConfig     Bgp4EcommRouteTblConfig;
    tBgp4EcommPeerTblConfig      Bgp4EcommPeerTblConfig;
    tBgp4EcommFilterTblConfig    Bgp4EcommFilterTblConfig;
    tBgp4EcommPolicyTblConfig    Bgp4EcommPolicyTblConfig;
}tBgp4Ecomm;

/* structures for show routines */
/*****************************************************************************
 *                       "show ip bgp" interface structure                   *
 *****************************************************************************/
typedef struct t_ShowIpBgpRoute
{
    tNetAddress NetAddress;      /* network prefix               */
    tAddrPrefix NextHop;         /* next hop                     */
 struct
 {
  UINT1              *pu1ClusId;  /* Pointer to the list of
           * Cluster-id Attributes. */
  UINT2               u2ClusCnt;  /* Total number of Cluster-id
           * present. Each Cluster-id
           * attribute is 4 byte long.*/
  UINT1               u1Flag;     /* Cluster-id Attribute Flag. */
  UINT1               u1Pad;      /* For 4-byte alignment */
 }Cluster;

    struct
    {
        UINT1  *pu1CommVal;    /* Pointer to the list of Community Attributes. */
        UINT2   u2CommCnt;     /* Total number of Community attribute present.
                                  * each attribute is 4 byte long. */
        UINT1   u1Flag;        /* Comm Attribute Flag. */
        UINT1   u1Pad;         /* For 4-byte alignment */
    }Community;     /* Points to the Community Attribute */

#ifdef VPLSADS_WANTED    
  struct
    {
        UINT1  *pu1CommVal;    /* Pointer to the list of Ext Community Attributes. */
        UINT2   u2CommCnt;     /* Total number of Ext Community attribute present.
                                  * each attribute is 4 byte long. */
        UINT1   u1Flag;        /* Comm Attribute Flag. */
        UINT1   u1Pad;         /* For 4-byte alignment */
    }ExtCommunity;     /* Points to the Ext Community Attribute */
#endif

    UINT4 u4Network;             /* network prefix               */
    UINT4 u4NextHop;             /* next hop                     */
    INT4  i4PrefixLen;           /* network prefix length        */
    UINT4  u4Metric;              /* metric (MED)                 */
    UINT4 u4AttrFlag;             /* Flag to carry RouteInfo attributes */
    UINT4  u4LocPref;             /* local preference             */
    UINT4 u4Weight;              /* weight                       */
    struct
    {
        UINT4 u4NoOfAsPaths;     /* number of AS paths in u4Path */
        UINT1 au1AsType[BGP_CLI_MAX_AS_PATHS];
        UINT4 au4Path[BGP_CLI_MAX_AS_PATHS];
                                 /* AS Path array                */
    } ASPath;
    INT4  i4RouteStatus;         /* i4RouteStatus is a bitmask of following
                                  * values:
                                  *     0x01 - suppressed route
                                  *     0x02 - damped route
                                  *     0x04 - history route
                                  *     0x08 - valid route
                                  *     0x10 - best route
                                  *     0x20 - internal route
                                  */
    INT4  i4Origin;              /* u4Orgin is a bitmask of following
                                  * values:
                                  *     0x01 - route of IGP orgin
                                  *     0x02 - route of EGP orgin
                                  *     0x03 - incomplete route orgin
                                  */
    UINT4  u4BgpLabel;
    UINT4  u4NeigRouterId;        /* peer bgp router id                 */
    UINT4  u4OrigId;              /* Orginator id*/
    UINT4  u4RouteFlags;          /* Route Falgs*/
    UINT4  u4RouteExtFlags;       /* Route Ext Flags*/
#ifdef RFD_WANTED
   /* Dampening  parameters*/
    INT4   i4DampFom;
    UINT4  u4DampFlapCnt;
    UINT4  u4DampFlapTime;
    UINT4  u4DampReuseTime;
    UINT2  u2DampReuseInd;
    UINT1  u1DampRtStat;  
    UINT1  u1RtFlag;
#endif

#if defined (VPLSADS_WANTED) || defined (EVPN_WANTED)
    UINT4 u4Protocol;
#endif
#ifdef EVPN_WANTED
    UINT1     au1RD[MAX_LEN_RD_VALUE];
    UINT4     u4VnId;
    tMacAddr  MacAddress;
    UINT1     au1EthSegId[MAX_LEN_ETH_SEG_ID];
    UINT1     u1RouteType;
    UINT1     u1Rsvd[3];
#endif
} tShowIpBgpRoute;
/* the following information is used as a cookie in tShowIpBgp structure */
typedef struct t_ShowIpBgpCookie
{
    tNetAddress IpAddress; 
    tAddrPrefix PeerAddr;        /* peer field of Path Addr table */
    UINT4 u4AddrPrefix;          /* ip address prefix of Path Addr table */
    UINT4 u4Peer;                /* peer field of Path Addr table        */
#if defined (L3VPN) || defined (EVPN_WANTED)
    UINT1 au1RouteDisting[8];
#endif
#ifdef EVPN_WANTED
    UINT4     u4VnId;
    tMacAddr  MacAddress;
    UINT1     au1EthSegId[MAX_LEN_ETH_SEG_ID];
    UINT1     u1RouteType;
    UINT1     u1Rsvd[3];
#endif
    INT4  i4PrefixLength;        /* ip prefix length of Path Addr table  */
    UINT1 u1Protocol;            /* Protocol from which the route is learnt.
                                  * When Protocols not equal to BGP_ID, 
                                  * u4Peer is set to 0. */
    UINT1 u1AdvFlag;
    UINT1 au1Pad[2];             /* for 4byte boundary alignment */
} tShowIpBgpCookie;
typedef struct t_ShowIpBgp
{
    UINT4 u4TableVersion;        /* Internal version number of the table.
                                  * This number is incremented whenever the 
                                  * table changes.
                                  */
    UINT4 u4LocalRouterID;       /* local BGP router id                  */
    UINT4 u4NoOfRoutes;          /* number of routes presented in this return 
                                  * message
                                  */
    tShowIpBgpRoute aRoutes[1];   /* 'u4NoOfRoutes' follow here           */
} tShowIpBgp;

/*****************************************************************************
 *             "show ip bgp neighbor" command interface structures           *
 ***************************************************************************i*/

typedef struct t_BgpNeighborStats
{
    UINT4 u4Cap;                 /* capabilities:
                                  * bit mask  :  0x00000001 - Route Refresh
                                  *              0x00010000 - IPv4 Unicast
                                  *              0x00020000 - IPv4 Multicast
                                  *              0x00040000 - IPv6 Unicast
                                  *              0x00080000 - IPv6 Multicast
                                  *              0x00100000 - VPN4 unicast
                                  *              0x00200000 - ipv4 labelled unicasr
                                  *              0x00400000 - ipv4 orf send
                                  *              0x00800000 - ipv4 orf receive
                                  *              0x01000000 - ipv6 orf send
                                  *              0x02000000 - ipv6 orf receive
                                  */
    UINT4 u4Updates;             /* number of updates                      */
    UINT4 u4TotalMessages;       /* number of packets processed            */
    UINT4 u4Notification;        /* number of notifications                */
    UINT4 u4QMsgs;               /* number if messages on the Q to be
                                  * processed yet.
                                  */
    UINT4 u4RouteRefresh;
    UINT4 u4Ipv4RtRefreshCnt;
    UINT4 u4Ipv6RtRefreshCnt;
    UINT4 u4LbldIpv4RtRefreshCnt;
    UINT4 u4Vpnv4RtRefreshCnt;
    UINT4 u4Ipv4UniRtsCnt;
    UINT4 u4Ipv4LblRtsCnt;
    UINT4 u4Ipv6UniRtsCnt;
    UINT4 u4Vpn4UniRtsCnt;
} tBgpNeighborStats;

typedef struct t_IpBgpNeighbor
{
    tAddrPrefix       NeigIp;         /* peer ip address                    */
    tAddrPrefix       LocalIp;         /* local ip address                   */
    tAddrPrefix       NeighNetIp;      /* neighbor network address           */
    tAddrPrefix       UpdateSource;      /* neighbor network address        */
#ifdef ROUTEMAP_WANTED
    UINT1             au1IpPrefixFilterIn [RMAP_MAX_NAME_LEN + 4]; 
    UINT1             au1IpPrefixFilterOut [RMAP_MAX_NAME_LEN + 4];
#endif
    UINT4             u4NeigPort;      /* peer port                          */
    UINT4             u4LocalPort;     /* local port                         */
    UINT4             u4RemoteAs;      /* peer AS number                     */
    UINT4             u4NegBgpVersion; /* negotiated bgp version             */
    UINT4             u4NeigRouterId;  /* peer bgp router id                 */
    UINT4             u4BgpState;      /* state of the bgp connection (FSM 
                                        * state):
                                        *       1 - idle
                                        *       2 - connect
                                        *       3 - active
                                        *       4 - opensent
                                        *       5 - openconfirm
                                        *       6 - established
                                        */
    UINT4             u4PeerAdminState;/* Admin Status of the BGP Peer. */
    UINT4             u4PeerOperStatus;/* Operational Status of the BGP Peer
                                        * BGP_CLI_PEND_MP_CAPS |
                                        * BGP_CLI_PEND_MULTIHOP
                                        */
    UINT4             u4ElapsedTime;   /* time elapsed since connection UP   */
    UINT4             u4LastUpdateTime;/* time elapsed since last update msg
                                        * was received from the peer
                                        */
    UINT4             u4HoldTime;      /* bgp-timers: hold timer             */
    UINT4             u4KeepAlive;     /* bgp-timers: keep-alive timer       */
    UINT4             u4MinAdvtInt;    /* bgp-timers: mininum advertisement
                                        * interval timer.
                                        */
    UINT4             u4MinAsOrigInt;  /* bgp-timers: mininum AS Origination
                                        * interval.
                                        */
    UINT4             u4ConnectRetry;  /* bgp-timers: connect retry timer
                                        * interval.
                                        */
    UINT4             u4CommAttr;      /* community attribute flag:
                                        * '0' if community attribute is
                                        *     not configured for this peer.
                                        * '1' if community attribute is
                                        *     configured for this peer.
                                        */

    UINT4             u4ConnEstab;     /* no of times connections established*/
    INT4              i4ConnAuthStatus; /* Is session tcp md5 authenticated? */
    tBgpNeighborStats SendStats;       /* send statistics                    */
    tBgpNeighborStats RcvdStats;       /* receive statistics                 */
    UINT4             u4RestartInterval;
    UINT1             u1LastErrorCode; /* Last Error Code                    */
    UINT1             u1LastErrorSubCode; /* Last Error SubCode              */
    UINT1             u1GRCapability;  /* GR capability advertised = 1 received = 2
                                          advertise and received = 3 none = 4  */
    UINT1             u1BfdStatus;

} tIpBgpNeighbor;

typedef UINT4 tShowIpBgpNeigCookie;  /* peer table has only one index - 
                                      * the neighbor ip address
                                      */
                                           
typedef struct t_ShowIpBgpNeighbor
{
    UINT4  u4LocalAs;            /* local AS number                      */
    UINT4 u4NoOfNeig;            /* number of routes presented in this
                                  * return message
                                  */
    tIpBgpNeighbor aNeighbors[1];/* 'u4NoOfNeig' follow here             */
} tShowIpBgpNeighbor;


/* Prototype Declarations */
INT4 Bgp4ConfigRouterBgp (tCliHandle , UINT4, UINT1 );
INT4 Bgp4Shutdown (tCliHandle CliHandle, UINT1 );
INT4 Bgp4SetRouterId (tCliHandle CliHandle, UINT4);
INT4 Bgp4ResetRouterId (tCliHandle CliHandle);

INT4 Bgp4SetLocalPreference (tCliHandle CliHandle, UINT4);
INT4 Bgp4ResetLocalPreference (tCliHandle CliHandle);

INT4 Bgp4ConfigNeighborRemoteAs (tCliHandle, UINT4, tAddrPrefix *, UINT1 *, INT4);
INT4 Bgp4RemoveNeighborRemoteAs (tCliHandle, UINT4, tAddrPrefix *, UINT1 *);

INT4 Bgp4ConfigNeighborEBGPMultihop (tCliHandle, 
                                     tAddrPrefix *pIpAddress, /* Peer's Remote Addr */
                                     UINT1 *pu1PeerGroupName,/* Peer Group Name */
                                     UINT1 u1Set,    /* VAL_SET/VAL_NO */
                                     INT4 *pi4HopLimt);
INT4 Bgp4ConfigNeighborNextHopSelf (tCliHandle CliHandle,
                               tAddrPrefix *pIpAddress, /* Peer's Remote Addr */
                               UINT1 *pu1PeerGroupName, /* Peer's group name */
                               UINT1 u1Set );
INT4 Bgp4ConfigNeighborOverrideCapability (tCliHandle CliHandle,
                                          tAddrPrefix *pIpAddress, /* Peer's Remote Addr */
                                          UINT1 *pu1PeerGroupName, /* Peer's group name */
                                          UINT1 u1Set );
INT4 Bgp4ConfigVpnRouteLeaking (tCliHandle CliHandle,
                                 UINT1 u1Set);  /* VAL_SET/VAL_NO */

INT4 Bgp4ConfigNeighborTimer (tCliHandle CliHandle,
                         tAddrPrefix *pIpAddress,    /* Peer's Remote Addr */
                         UINT1 *pu1PeerGroupName,    /* Peer's group name */
                         INT4 *pi4KeepAliveTime,     /* Keep Alive Time */
                         INT4 *pi4HoldTime,          /* Hold Time */
                         INT4 *pi4IdleHoldTime,      /* IdleHold Time */
                         INT4 *pi4DelayOpenTime);    /* DelayOpen Time */
INT4 Bgp4RemoveNeighborTimer (tCliHandle CliHandle,
                         tAddrPrefix *pIpAddress,    /* Peer's Remote Addr */
                         UINT1 *pu1PeerGroupName,    /* Peer's group name */
                         INT4 *pi4KeepAliveTime,     /* Keep Alive Time */
                         INT4 *pi4HoldTime,          /* Hold Time */
                         INT4 *pi4IdleHoldTime,      /* IdleHold Time */
                         INT4 *pi4DelayOpenTime);    /* Delay Open time */
  

INT4 Bgp4ConfigNeighborInterval (tCliHandle CliHandle,
                            tAddrPrefix *pIpAddress,        /* Ip Address value */
                            UINT1       *pu1PeerGroupName,
                            INT4        *pi4MinOrigInt,     /* Min Origination Interval */
                            INT4        *pi4MinAsAdvtInt,   /* Min As Advertisement
                                                               Interval */
                            INT4        *pi4ConnectRetryInt
);
INT4 Bgp4RemoveNeighborInterval (tCliHandle CliHandle,
                            tAddrPrefix *pIpAddress,        /* Ip Address value */
                            UINT1       *pu1PeerGroupName,
                            INT4        *pi4MinOrigInt,     /* Min Origination Interval */
                            INT4        *pi4MinAsAdvtInt,   /* Min As Advertisement
                                                               Interval */
                            INT4        *pi4ConnectRetryInt/* Connect Retry Interval */);

INT4 Bgp4ConfigNeighborShutdown (tCliHandle CliHandle,
                            tAddrPrefix *pIpAddress,    /* Peer's Remote Addr */
                            UINT1 *pu1PeerGroupName,    /* Peer's group name */
                            UINT1 u1Set  );
INT4 Bgp4ConfigCommPeerSendStatus (tCliHandle CliHandle,
                              tAddrPrefix *pIpAddress, /* Peer Ip Address  */
                              UINT1       *pu1PeerGroupName,
                              UINT1        u1Set);
INT4 Bgp4ConfigEcommPeerSendStatus (tCliHandle CliHandle,
                               tAddrPrefix *pIpAddress, /* Peer Ip Address  */
                               UINT1       *pu1PeerGroupName,
                               UINT1 u1Set);
INT4 Bgp4CliSetHoldAdvtRoutes (tCliHandle CliHandle,
    tAddrPrefix *pIpAddress,
    UINT1       *pu1PeerGroupName,
           INT4       i4Status);

INT4 Bgp4SetNonBgpRtAdvt (tCliHandle CliHandle, INT4);
INT4 Bgp4ResetNonBgpRtAdvt (tCliHandle CliHandle);
INT4 Bgp4SetOverlapPolicy (tCliHandle CliHandle, INT4);

INT4 Bgp4ResetOverlapPolicy (tCliHandle CliHandle);
INT4 Bgp4CliNetworkRoute (tCliHandle CliHandle,UINT1 ,tNetAddress *);
INT1 Bgp4CheckPeerEntryForPeerGrp (tSNMP_OCTET_STRING_TYPE *, UINT4);
INT4 Bgp4ConfigRedistribute (tCliHandle CliHandle, INT4, UINT1, UINT1*, UINT4, INT4, UINT1);
INT4 Bgp4ConfigSynchronization (tCliHandle CliHandle, UINT1 );
INT4 Bgp4ConfigFourByteAsn (tCliHandle CliHandle, UINT1 );
INT4 Bgp4ConfigFourByteAsnNotation (tCliHandle CliHandle, UINT1 ); 
INT4 Bgp4ConfigNegiNextHopPrcsInterval (tCliHandle CliHandle, UINT4 );
INT4 Bgp4ConfigAlwaysCompareMed (tCliHandle CliHandle, UINT1 ); 
INT4 Bgp4SetDefaultMetric (tCliHandle CliHandle, UINT4);
INT4 Bgp4ResetDefaultMetric (tCliHandle CliHandle);

INT4 Bgp4ConfigMEDTable (tCliHandle CliHandle, 
                    UINT4 u4Index,
                    UINT4 u4RemoteAS,
                    tNetAddress *pIpAddress,
                    UINT4 u4MedValue,
                    INT4 i4Direction,
                    UINT4 u4Override, 
                    UINT1 *pu1IntermediateAsNums);

INT4 Bgp4RemoveMEDTable (tCliHandle CliHandle,UINT4);
INT4 Bgp4ConfigLocalPrefTable (tCliHandle CliHandle, 
                    UINT4 u4Index,
                    UINT4 u4RemoteAS,
                    tNetAddress *pIpAddress,
                    UINT4 u4MedValue,
                    INT4 i4Direction,
                    UINT4 u4Override, 
                    UINT1 *pu1IntermediateAsNums);


INT4 Bgp4RemoveLocalPrefTable (tCliHandle CliHandle,UINT4);

INT4 Bgp4ConfigUpdateFilterTable (tCliHandle CliHandle, 
                             UINT4 u4Index,
                             UINT4 u4RemoteAS,
                             tNetAddress *pIpAddress,
                             INT4 i4Direction,
                             UINT1 *pu1IntermediateAsNums, 
                             INT4 i4Action);

INT4 Bgp4RemoveUpdateFilterTable (tCliHandle CliHandle,UINT4);
INT4 Bgp4ConfigAggregateAddress (tCliHandle CliHandle, 
                            UINT4 u4Index,
                            tNetAddress *pIpAddress,  
                            UINT4 u4SummaryOnly,
                            INT4 i4AsSet, UINT1 *pu1SuppressMapName,
       UINT1 *pu1AdvMapName, UINT1 *pu1AttrMapName);

INT4 Bgp4RemoveAggregateAddress (tCliHandle CliHandle, UINT4);
INT4 Bgp4ConfigBgpDampening (tCliHandle CliHandle, UINT4, UINT4, UINT4, UINT4);
INT4 Bgp4DisableBgpDampening (tCliHandle CliHandle, UINT4 *, UINT4 *, UINT4 *, UINT4
*,UINT4 *,UINT4 *,UINT4 *);

INT4 Bgp4ConfigClusterId (tCliHandle CliHandle, UINT4);
INT4 Bgp4RemoveClusterId (tCliHandle CliHandle);


INT4 Bgp4ConfigClientToClientReflection (tCliHandle CliHandle, UINT1 );
INT4 Bgp4ConfigNeighborRouteRflClient (tCliHandle CliHandle,
                                  tAddrPrefix *pIpAddress, /* Peer's Address */
                                  UINT1 *pu1PeerGroupName, /* Peer's group */
                                  UINT1 u1Set);
INT4 Bgp4ConfigCommRouteTable (tCliHandle CliHandle, tNetAddress *pIpAddress,
                               UINT4 u4CommValue, INT4 i4Action, UINT1 u1Set);

INT4 Bgp4ConfigCommFilterTable (tCliHandle CliHandle, UINT4, INT4, INT4, UINT1 ); 
INT4 Bgp4ConfigCommPolicyTable (tCliHandle CliHandle, tNetAddress *, INT4, UINT1 );
INT4 Bgp4ConfigEcommRouteTable (tCliHandle CliHandle, tNetAddress *, UINT1 *, INT4, UINT1 );
INT4 Bgp4ConfigEcommFilterTable (tCliHandle CliHandle, UINT1 *, INT4, INT4, UINT1 ); 
INT4 Bgp4ConfigEcommPolicyTable (tCliHandle CliHandle, tNetAddress *, INT4, UINT1 );
INT4 Bgp4ConfigTrace (tCliHandle CliHandle, UINT4, UINT1, UINT4, UINT1 *, UINT4 );
INT4 Bgp4ConfigGREnableDisable (tCliHandle,UINT4 ,UINT4 ,UINT1);
INT4 Bgp4CliConfigUpdateDelay (tCliHandle ,INT4);
INT4 Bgp4CliSetRestartSupport (tCliHandle ,UINT4);
INT4 Bgp4CliSetRestartReason (tCliHandle, UINT4);
INT4 Bgp4EnableBgpDampening (tCliHandle,UINT1);

INT4 bgpShowIpBgpNeighbor (UINT4 *, UINT1 *, UINT4, tShowIpBgpNeigCookie *);
INT4 Bgp4CliDisplayBgpStaleRoutes (tCliHandle, tShowIpBgp *,
                                   INT4);

INT4 bgpMemCompareEcomm (UINT1 *,UINT1 *,UINT4);

INT4 Bgp4ShowIpBgpSummary (tCliHandle CliHandle,UINT1);
INT4 Bgp4ShowBgpVersion (tCliHandle CliHandle);
INT4 BgpSetTraceValue (INT4, UINT1);

INT1
Bgp4SetMaximumpaths (tCliHandle CliHandle,INT4 i4Type, INT4 i4Value);
INT1
Bgp4ShowIpBgp (tCliHandle CliHandle, tNetAddress *pIpAddr, UINT1 u1RibDisp,
               UINT1 u1NeigDisp, UINT1 u1LabelRibDisp, UINT1 u1StaleDisp, UINT1 u1Set,
               UINT1 u1IpPrefSpec, UINT1 u1RcvdOrfDisp, UINT1 u1AdvtRoutes);

INT1
Bgp4CliShowIpv4BgpNeighbor (tCliHandle CliHandle, tNetAddress *pIpAddr);

INT1
Bgp4CliShowIpv6BgpNeighbor (tCliHandle CliHandle, tNetAddress *pIpAddr);

INT1
Bgp4CliShowIpBgpRibRoutes (tCliHandle CliHandle, tNetAddress *pIpAddr);

INT1
Bgp4CliShowIpBgpRoutes (tCliHandle CliHandle, tNetAddress *pIpAddr, 
        UINT1 u1IpPrefSpec, UINT1 u1Flag);

INT1
Bgp4ShowIpv6Bgp (tCliHandle CliHandle, tNetAddress  *pIpAddr, UINT1 u1Set,
                 UINT1 u1NeigDisp, UINT1 u1StaleDisp, UINT1 u1RibDisp, UINT1 u1IpPrefSpec, UINT1 u1RcvdOrfDisp,
                 UINT1 u1AdvtRoutes);
INT1
Bgp4ShowIpv6BgpNeighbor (tCliHandle CliHandle, tAddrPrefix  *pIpAddr, 
    UINT1 u1Set);
INT1
Bgp4CliShowIpBgpStaleRoutes(tCliHandle, tNetAddress *);
INT4 Bgp4ShowIpBgpRestartExitReason (tCliHandle);
INT4 Bgp4ShowIpBgpRestartSupport (tCliHandle);
INT4 Bgp4ShowIpBgpRestartReason (tCliHandle);
INT4 Bgp4ShowIpBgpRestartStatus (tCliHandle);
INT1 Bgp4CliShowIpBgpEndOfRIBMarkerStatus(tCliHandle,tNetAddress *);
INT4 Bgp4CliDisplayBgpRoutes (tCliHandle CliHandle,tShowIpBgp *,INT4, UINT1);
INT4 Bgp4CliDisplayBgpDampRoutes(tCliHandle CliHandle,tShowIpBgp *,INT4, UINT1);
INT4 Bgp4CliDisplayAsPaths(tCliHandle CliHandle, tShowIpBgpRoute * pBgpRoute);
INT4 Bgp4CliDisplayBgpSpecRoutes (tCliHandle CliHandle,tShowIpBgp *);
INT4 Bgp4CliDisplayBgpNeighbor (tCliHandle CliHandle,tShowIpBgpNeighbor *);
INT4 Bgp4CliDisplayBgpSummary (tCliHandle CliHandle,tShowIpBgpNeighbor *,INT4);
INT4 Bgp4CliDisplayBgpInfo (tCliHandle CliHandle,tShowIpBgpNeighbor *,INT4);
INT4 Bgp4CliDisplayNeighborInfo (tCliHandle CliHandle,tShowIpBgpNeighbor *,INT4);
INT4 Bgp4ConfigNeighborLocalAs (tCliHandle CliHandle,  tAddrPrefix * pIpAddress,
                           UINT4 u4LocalAS);
#ifdef EVPN_WANTED
INT1
Bgp4CliShowIpBgpEvpnRoutes (tCliHandle CliHandle, tNetAddress *pIpAddr, UINT1 u1RouteType);
INT4 Bgp4CliDisplayEvpnRoutes (tCliHandle CliHandle,tShowIpBgp *, INT4, UINT1, UINT1 *,UINT1 *);
INT4
Bgp4CliShowIpBgpEvpnMac (tCliHandle CliHandle, tNetAddress *pIpAddr);
INT4 
Bgp4CliConfigMacDupTimer (tCliHandle CliHandle, INT4 i4MacDupTime);
INT4
Bgp4CliConfigMaxMacMoves (tCliHandle CliHandle, INT4 i4MaxMacMoves);

#endif
/* Proto-types for confed CLI funcs */
INT4 Bgp4ConfigConfedId (tCliHandle CliHandle,UINT4); 
INT4 Bgp4RemoveConfedId (tCliHandle CliHandle);

INT4 Bgp4ConfigConfedPeers (tCliHandle CliHandle,UINT4, UINT1);
INT4 Bgp4ConfigBestPathMedConfed (tCliHandle CliHandle,UINT1);
INT4 Bgp4ConfigNeighborPassword (tCliHandle CliHandle,
                            tAddrPrefix *pIpAddress,
                            UINT1 *pu1PeerGroupName,
                            UINT1 *pu1Password,
                            UINT1 u1PwdLen);
INT4 Bgp4RemoveNeighborPassword (tCliHandle CliHandle,
                            tAddrPrefix *pIpAddress,
                            UINT1 *pu1PeerGroupName);
INT4 Bgp4ConfigClearIpBgp (UINT1 u1ASCheck, 
                            UINT4 u4InputAS, UINT1 u1afi,UINT1 u1Group); 
INT4 Bgp4ConfigNeighborClearIpBgp (tCliHandle CliHandle, 
        tNetAddress *pIpAddress);
INT4 Bgp4UtilNeighborClearIpBgp (tNetAddress *pIpAddress);
INT4 Bgp4ClearRouteDampening (tCliHandle CliHandle, tNetAddress RtNetAddr,
                                            UINT1 u1RtFlag);
INT4 Bgp4UtilClearRouteDampening (tNetAddress RtNetAddr, UINT1 u1RtFlag);
INT4 Bgp4ClearRouteFlapStats (tCliHandle CliHandle, tNetAddress RtNetAddr,
                                           UINT1 u1RtFlag);
INT4 Bgp4UtilClearRouteFlapStats (tNetAddress RtNetAddr, UINT1 u1RtFlag);
INT4 Bgp4ConfigNeighborSoftReconfigClearIpBgp (tCliHandle CliHandle, 
            UINT1,
            tNetAddress *pIpAddress,
            INT4, UINT1);
INT4 Bgp4ConfigNeighborClearIpBgpSoftAll (tCliHandle CliHandle, UINT1 u1ASCheck,
                UINT4 u4InputAS, UINT1 u1afi, UINT1 u1Dir, UINT1 u1OrfFlag);
INT1
Bgp4ClearIpv6Bgp (tAddrPrefix *pIpAddress, UINT1 u1Set, UINT1 u1SoftFlag,
                  UINT1 u1PolicyDir, UINT1 u1AllPeerSet, UINT1 *pu1PeerGroup, UINT1);
INT4 Bgp4UtilNeighborSoftReconfigClearIpBgp (UINT1,
                                             tNetAddress *pIpAddress, INT4, UINT1);
INT4 Bgp4UtilNeighborClearIpBgpSoftAll (UINT1 u1ASCheck,
                                       UINT4 u4InputAS, UINT1 u1afi, UINT1 u1Dir, UINT1 u1OrfFlag);
INT4 ShowBgpProtocolInfo (tCliHandle CliHandle);
INT4 ShowBgpProtocolInfoInCxt (tCliHandle CliHandle, UINT4 u4BgpCxtId);
INT4 Bgp4ShowFilterInfo (tCliHandle CliHandle);
INT4 Bgp4ShowMEDInfo (tCliHandle CliHandle);
INT4 Bgp4ShowAggrInfo (tCliHandle CliHandle);
INT4 Bgp4ShowDampeningInfo (tCliHandle CliHandle);
INT4 Bgp4ShowLocPrefInfo (tCliHandle CliHandle);
INT4 Bgp4ShowCommRoute (tCliHandle CliHandle);
INT4 Bgp4ShowCommPolicy (tCliHandle CliHandle);
INT4 Bgp4ShowCommFilter (tCliHandle CliHandle);
INT4 Bgp4ShowEcommRoute (tCliHandle CliHandle);
INT4 Bgp4ShowEcommPolicy (tCliHandle CliHandle);
INT4 Bgp4ShowEcommFilter (tCliHandle CliHandle);
INT4 Bgp4ShowBgpInfo (tCliHandle CliHandle);
INT4 Bgp4ShowBgpTimers (tCliHandle CliHandle);
INT4 Bgp4ShowRflInfo (tCliHandle CliHandle);
INT4 Bgp4ShowConfedInfo (tCliHandle CliHandle);
INT1
Bgp4ShowIpBgpLabel (tCliHandle CliHandle, tNetAddress *pIpAddr, UINT1 u1Set);



INT4 BgpShowRunningConfig(tCliHandle CliHandle, UINT1 *pu1ContextName);
INT4 BgpShowRunningConfigInCxt (tCliHandle CliHandle, UINT4 u4Context);
VOID BgpShowRunningConfigScalars (tCliHandle CliHandle, UINT4 u4Context, UINT1 *pu1Flag);
VOID BgpShowRunningConfigTables (tCliHandle CliHandle);
INT4 BgpShowRunningConfigGRScalar (tCliHandle CliHandle);
INT4 BgpShowRunningConfigASScalars (tCliHandle CliHandle, UINT4 u4Context);
VOID Fsbgp4PeerExtTableInfo (tCliHandle CliHandle,INT4 i4ExtPeerType,
                             tSNMP_OCTET_STRING_TYPE *pIpNetwork, 
        UINT4 *pu4PagingStatus);
VOID Bgp4PeerExtTableIpv4Info (tCliHandle CliHandle,INT4 i4ExtPeerType,
                             tSNMP_OCTET_STRING_TYPE *pIpNetwork,
                             UINT4 *pu4PagingStatus,UINT1 *pu1AddrFlag);
VOID Bgp4PeerExtTableIpv6Info (tCliHandle CliHandle,INT4 i4ExtPeerType,
                               tSNMP_OCTET_STRING_TYPE *pIpNetwork,
                               UINT4 *pu4PagingStatus,UINT1 *pu1AddrFlag);
VOID Fsbgp4AggregateTableInfo (tCliHandle CliHandle,INT4 i4AggIndex,
                               UINT4 *pu4PagingStatus);
VOID Fsbgp4MEDTableInfo (tCliHandle CliHandle,INT4 i4MEDIndex,
                         UINT4 *pu4PagingStatus);
VOID Fsbgp4LocalPrefTableInfo (tCliHandle CliHandle,INT4 i4LPIndex,
                               UINT4 *pu4PagingStatus);
VOID Fsbgp4UpdateFilterTableInfo (tCliHandle CliHandle,INT4 i4UFIndex,
                                  UINT4 *pu4PagingStatus);
VOID Fsbgp4CommRouteAddDeleteCommTableInfo (tCliHandle CliHandle,
                INT4 i4RtAfi, INT4 i4RtSafi,   
         tSNMP_OCTET_STRING_TYPE *pIpNetwork, 
                                            INT4 i4CommIpPrefLen,
                                            UINT4 u4CommVal,INT4 i4Action,
                                            UINT4 *pu4PagingStatus);
VOID Fsbgp4CommRouteCommSetStatusTableInfo (tCliHandle CliHandle,
                                            INT4 i4RtAfi, INT4 i4RtSafi,
         tSNMP_OCTET_STRING_TYPE *pIpNetwork, 
                                            INT4 i4CommIpPrefLen,
                                            UINT4 *pu4PagingStatus);
VOID Fsbgp4CommInOutFilterTableInfo (tCliHandle CliHandle,UINT4 u4CommVal,
                                     INT4 i4Action,UINT4 *pu4PagingStatus);
VOID Fsbgp4ExtCommRouteAddDeleteExtCommTableInfo (tCliHandle CliHandle,
                                         INT4 i4RtAfi, INT4 i4RtSafi,
      tSNMP_OCTET_STRING_TYPE *pIpNetwork, 
                                         INT4 i4ExtCommIpPrefLen,
                                         tSNMP_OCTET_STRING_TYPE tExtCommVal,
                                         INT4 i4Action,UINT4 *pu4PagingStatus);
VOID Fsbgp4ExtCommRouteExtCommSetStatusTableInfo (tCliHandle CliHandle,
      INT4 i4RtAfi, INT4 i4RtSafi,
                                         tSNMP_OCTET_STRING_TYPE *pIpNetwork, 
                                             INT4 i4ExtCommIpPrefLen,
                                             UINT4 *pu4PagingStatus);
VOID Fsbgp4ExtCommInOutFilterTableInfo (tCliHandle CliHandle,
                                   tSNMP_OCTET_STRING_TYPE tExtCommVal,
                                   INT4 i4Action,UINT4 *pu4PagingStatus);
VOID FsbgpAscConfedPeerTableInfo (tCliHandle CliHandle,INT4 i4ConfedAS,
                                  UINT4 *pu4PagingStatus);
VOID BgpPeerTableInfo (tCliHandle CliHandle,UINT4 u4PeerRemoteAddr,
                       UINT4 *pu4PagingStatus);
VOID
FsBgpDisplayFlapStats(tCliHandle CliHandle,UINT4 u4AddrPrefix,INT4 i4AddrPrefixLen,
                      UINT4 u4AttrPeer,INT4 i4RtDampHistInstance);





/* Protype Declarations for FutureBGP4 CLI */
INT1 cli_process_bgp_cmd                   (tCliHandle CliHandle ,UINT4 u4Command , ...);
#ifdef BGP_TEST_WANTED
INT4 cli_process_bgp_test_cmd (tCliHandle CliHandle,UINT4 u4Command, ...);
#endif
INT1  BgpGetBgpCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);
INT1  BgpGetBgpAddfamilyCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);
INT1  BgpGetBgpVpnv4AddfamilyCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);
INT4 Bgp4ConfigNeighborCERouteTargetAdvt (tCliHandle CliHandle,
                                          tAddrPrefix *pIpAddress, 
                                          UINT1 *pu1PeerGroupName,
                                          UINT1 u1Set);

INT4 Bgp4ConfigNeighborSiteOfOrigin(tCliHandle CliHandle,
                                    tAddrPrefix *pIpAddress, 
                                    UINT1 *pu1SiteOfOrigin, UINT1 u1Set);

INT4 Bgp4ConfigNeighborCapability(tCliHandle CliHandle,
                             tAddrPrefix *pIpAddress,
                             UINT1 *pu1PeerGroupName,
                             UINT4  u4CapValue,
                             UINT1  u1Set, UINT1 u1OrfType, UINT1 u4OrfMode);

INT4 Bgp4ConfigNeighborUpdateSrc (tCliHandle CliHandle,
                             tAddrPrefix *pIpAddress,
                             UINT1 *pu1PeerGroupName,
                             tAddrPrefix  *pSrcAddr,
                             UINT1 u1Set);
INT4 Bgp4ConfigNeighborGateway  (tCliHandle CliHandle,
                            tAddrPrefix *pIpAddress,
                            UINT1 *pu1PeerGroupName,
                            tAddrPrefix *pGateway,
                            UINT1 u1Set);
INT4 Bgp4ConfigNeighborNetworkAddr (tCliHandle CliHandle, 
                               tAddrPrefix *pIpAddress,
                               UINT1 *pu1PeerGroupName,
                               tAddrPrefix *pNetworkAddr,
                               UINT1 u1Set);
INT4 Bgp4ConfigNeighborDefRouteOriginate (tCliHandle CliHandle, 
                                     tAddrPrefix *pIpAddress,
                                     UINT1 *pu1PeerGroupName,
                                     UINT1 u1Set);
#ifdef L3VPN
INT4 Bgp4ConfigVrfRouteTarget(tCliHandle CliHandle, INT4 i4RouteTargetType,
                         tSNMP_OCTET_STRING_TYPE *pRouteTargetVal, 
                         UINT1  u1Set );
INT4 Bgp4CliDisplayBgpLabels (tShowIpBgp *, INT4);
#endif /* L3VPN */
INT4
Bgp4ConfigNeighborActivate (tCliHandle CliHandle,
                            tAddrPrefix *pIpAddress, /* Peer's Remote Addr */
                            UINT1 *pu1PeerGroupName, /* Peer's group name  */
                            UINT4  u4CapValue       /* MP Capability value */
                           );
INT4
Bgp4DeactivateNeighborActivate (tCliHandle CliHandle,
                                tAddrPrefix *pIpAddress, /* Peer's Remote Addr */
                                UINT1 *pu1PeerGroupName, /* Peer's group name  */
                                UINT4  u4CapValue       /* MP Capability value */
                               );
INT1
Bgp4VpnGetVrfPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);
INT1 Bgp4CliImportRoute (tCliHandle CliHandle, tNetAddress pIpAddr, 
                    tAddrPrefix pNextHop, INT4 i4Metrict,INT4 i4Index,
                    INT4 i4Protocol, INT4 i4NumRts,
                    UINT1 u1Set);
INT1
Bgp4CliImportIpv6Route (tCliHandle CliHandle, tNetAddress *pIpAddr,
                    tAddrPrefix *pNextHop, INT4 i4Ipv6Metric,INT4 i4Index,
                    INT4 i4Protocol, INT4 i4NumRts,
                    UINT1 u1Set);
INT4
Bgp4CliConfiAddressFamily (tCliHandle CliHandle,
                            UINT2 u2Afi, UINT1  u1Set);

INT4 Bgp4ConfigDefaultAfiSafi (tCliHandle CliHandle, UINT1 u1Set);
INT4 Bgp4ConfigDefRouteOriginate (tCliHandle CliHandle, UINT1 u1Set);


INT4
bgpConfigNeighborSoftReconfigClearIpBgp (
         UINT1 u1AllPeers,        /* AllPeers_SET or AllPeers_NO */
         tAddrPrefix *pIpAddress, /* if non-NULL points to the IpAddress
                                   * specified on the CLI */
         UINT1 *pu1PeerGroupName  /* if non-NULL points to the Peer
                                   * Group name specified on the CLI */
    );

INT4
bgpConfigNeighborSoftReconfigClearIpBgpSoftIn (
                 UINT1        u1AllPeers,   /* AllPeers_SET or AllPeers_NO */
                 UINT4        u4AsafiMask,  /* to identify ipv4/vpnv4 */
                 tAddrPrefix *pIpAddress,   /* if non-NULL points to the
                                             * IpAddress specified on the CLI*/
                 UINT1 *pu1PeerGroupName,    /* if non-NULL points to the Peer
                                             * Group name specified on the CLI
                                             */
                 UINT1 u1OrfFlag
    );

INT4
bgpConfigNeighborSoftReconfigClearIpBgpSoftOut (
         UINT1        u1AllPeers, /* AllPeers_SET or AllPeers_NO */
         UINT4        u4AsafiMask, /* Afisafi mask to indicate the family */
         tAddrPrefix *pIpAddress, /* if non-NULL points to the
                                   * IpAddress specified on
                                   * the CLI */
         UINT1       *pu1PeerGroupName /* if non-NULL points to the
                                        * Peer Group name
                                        * specified on the CLI */
    );

#ifdef L3VPN
INT4
bgpShowIpBgpLabel (UINT1 *pu1Buffer,/* non-NULL buffer, where the
                                     * table is returned. this
                                     * buffer must be big enough
                                     * to hold atleast one route
                                     * (i.e. sizeof(tShowIpBgp)).
                                     */
                   UINT4 u4BufLen, /* length of pu1Buffer          */
                   UINT4 u4AsafiMask,
                   tShowIpBgpCookie * pCookie /* Cookie, used to fetch tables
                                               * that are greater than length
                                               * u4BufLen
                                               */
    );
#endif /* L3VPN */
INT4   bgpShowIpv6BgpNeighbor (tAddrPrefix *pNeighbor,
                               /* if non-NULL, contains the neighbor
                                * ip address; if NULL then the all
                                * neighbor entries are returned.  */
                               UINT1 *pu1Buffer,
                                /* non-NULL buffer, where the
                                 * table is returned. this
                                 * buffer must be big enough
                                 * to hold atleast one route (i.e.
                                 * sizeof(tShowIpBgpNeibhbor)).  */
                               UINT4 u4BufLen,    /* length of pu1Buffer */
                               tAddrPrefix * pCookie
                                /* Cookie, used to fetch tables
                                 * that are greater than length
                                 * u4BufLen */
                            );
/* Functions defined in fsbgpcli.c */
/* Prototype Declarations */
INT4 bgpShowBgpVersion (  UINT4 * );
INT4 bgpConfigRouterBgp ( UINT4, UINT1 );
INT4 bgpShutdown ( UINT1 );
INT4 bgpConfigRouterId ( UINT4, UINT1 );
INT4 bgpConfigDefaultLocalPreference ( UINT4, UINT1 );
INT4 bgpConfigDefaultAfiSafi ( UINT1 );
INT4 bgpConfigNeighborRemoteAs ( UINT4, tAddrPrefix *, UINT1 *, UINT1 );
INT4 bgpConfigNonBgpRtAdvt ( INT4, UINT1 );
INT4 bgpConfigOverlapPolicy ( INT4, UINT1 );
INT4 bgpConfigDefRouteOriginate (UINT1);
INT4 bgpConfigRedistribute ( INT4, UINT1 );
INT4 bgpConfigSynchronization ( UINT1 );
INT4 bgpConfigAlwaysCompareMed ( UINT1 );
INT4 bgpConfigDefaultMetric ( UINT4, UINT1 );
INT4 Bgp4CliGetCurPrompt(UINT1 *);
 
#ifdef L3VPN
INT4 bgpConfigMEDTable ( UINT4, UINT1*, INT4, UINT4, tNetAddress*, UINT4,
                         INT4, UINT4, UINT1 *, UINT1 );
INT4 bgpConfigLocalPrefTable ( UINT4, UINT1*, INT4, UINT4, tNetAddress *,
                               UINT4, INT4, UINT4, UINT1 *, UINT1 );
INT4 bgpConfigUpdateFilterTable ( UINT4, UINT1*, INT4, UINT4, tNetAddress *,
                                  INT4, INT4, UINT1 *, UINT1 );
INT4 bgpConfigAggregateAddress ( UINT4, UINT1 *, INT4, tNetAddress *,
                                 UINT4, UINT1 );
#else
INT4 bgpConfigMEDTable ( UINT4, UINT4, tNetAddress*, UINT4,
                         INT4, UINT4, UINT1 *, UINT1 );
INT4 bgpConfigLocalPrefTable ( UINT4, UINT4, tNetAddress *,
                               UINT4, INT4, UINT4, UINT1 *, UINT1 );
INT4 bgpConfigUpdateFilterTable ( UINT4, UINT4, tNetAddress *,
                                  INT4, INT4, UINT1 *, UINT1 );
INT4 bgpConfigAggregateAddress ( UINT4, tNetAddress *,
                                 UINT4, UINT1 );
#endif
INT4 bgpConfigBgpDampening ( UINT4 *, UINT4 *, UINT4 *, UINT4 *,
                             UINT4 *, UINT4 *, UINT4 *, UINT1 );
INT4 bgpConfigClusterId ( UINT4, UINT1 );
INT4 bgpConfigClientToClientReflection ( UINT1 );
INT4 bgpConfigCommFilterTable ( UINT4, INT4, INT4, UINT1 );
INT4 bgpConfigEcommFilterTable ( UINT1 *, INT4, INT4, UINT1 );
INT4 bgpConfigTrace ( UINT4, UINT1 );
INT4 bgpShowIpv4Bgp ( UINT1 *, UINT4, tShowIpBgpCookie *, UINT4 , UINT1 );
INT4 bgpShowIpv6Bgp ( UINT1 *, UINT4, tShowIpBgpCookie *, UINT1);
INT4 bgpShowIpv4BgpRIB ( UINT1 *, UINT4, UINT4,tShowIpBgpCookie *, UINT1);
INT4 Bgp4ShowAggregateRoute ( UINT1 *, UINT4, tShowIpBgpCookie *, UINT1,  INT4 *);
INT4 bgpShowIpv6BgpRIB ( UINT1 *, UINT4, tShowIpBgpCookie *, UINT1);
INT4   bgpShowIpv4BgpNeighbor (UINT4 *,UINT1 *,UINT4 ,tAddrPrefix *);
#ifdef L3VPN
INT4 bgpShowLabelledIpv4BgpRIB (UINT1 *,  UINT4, tShowIpBgpCookie *);



INT4 bgpConfigVrfRouteTarget(INT4 , tSNMP_OCTET_STRING_TYPE *, UINT1  );
INT4 bgpConfigNeighborCERouteTargetAdvt (tAddrPrefix *, UINT1 *, UINT1);
INT4 bgpConfigNeighborSiteOfOrigin(tAddrPrefix *, UINT1 *, UINT1 );
#endif

/* From fsbgpcli.h */
/* Proto-types for confed CLI funcs */
INT4 bgpConfigConfedId (UINT4, UINT1);
INT4 bgpConfigConfedPeers (UINT4, UINT1);
INT4 bgpConfigBestPathMedConfed (UINT1);

INT4 bgpConfigNeighborPassword (
                    tAddrPrefix *,  UINT1 *,  UINT1 *, UINT1 , UINT1);
INT4 bgpConfigAddressFamily(UINT1*, INT4, UINT2 , UINT1);

INT4 Bgp4SetRouteDistance PROTO ((tCliHandle, INT4, UINT1 *));
INT4 Bgp4SetNoRouteDistance PROTO ((tCliHandle, UINT1 *));
#ifdef ROUTEMAP_WANTED
INT4 Bgp4CliSetDistribute PROTO ((tCliHandle, UINT1*, UINT1, UINT1));
INT4 Bgp4CliSetNeighborRoutemap (tCliHandle, tAddrPrefix *, UINT1 *, UINT1 *, UINT1, UINT1, UINT1);
#endif /* ROUTEMAP_WANTED */
INT1 Bgp4CliShowIpBgpRestartMode (tCliHandle, UINT1, tNetAddress *);
VOID IssBgpShowDebugging (tCliHandle CliHandle);
INT4 Bgp4ConfigPeerGroupRemoteAs (tCliHandle CliHandle, UINT1 *pu1PeerGrp,
                                  UINT1 u1Set, UINT4 u4RemoteAs,
                                  INT4 i4IdleHoldValue);
INT4 Bgp4ConfigPeerGroupLocalAs (tCliHandle CliHandle, UINT1 *pu1PeerGrp,
                                  UINT4 u4LocalAs);
INT4 Bgp4ConfigPeerGroup (tCliHandle CliHandle, UINT1 *pu1PeerGroup,
                          UINT1 u1Status);
INT4 Bgp4ConfigPeerGroupList (tCliHandle CliHandle, UINT1 *pu1PeerGrp,
                              tNetAddress * pIpAddress, UINT1 u1Status);
INT4 Bgp4ShowPeerGroup (tCliHandle CliHandle, UINT1 *pu1PeerGrp);
INT4 Bgp4ShowPeerGroupSummary (tCliHandle CliHandle, UINT1 *pu1PeerGrp);
INT4 Bgp4ClearPeerGroup (tCliHandle CliHandle, UINT1 *pu1PeerGrp,
                         UINT1 u1PolicyDir, UINT1 u1SoftFlag, UINT1 u1OrfRqst,UINT1 u1Flag);
INT4 Bgp4UtilClearPeerGroup (UINT1 *pu1PeerGrp, UINT1 u1PolicyDir,
                             UINT1 u1SoftFlag, UINT1 u1OrfRqst);
VOID Bgp4CliShowRcvdOrfFilters (tCliHandle CliHandle, tAddrPrefix *pIpAddr);
VOID Bgp4CliShowAdvtRoutes (tCliHandle CliHandle, tAddrPrefix *pIpAddr);
INT4 Bgp4ConfigTcpAoGlobalMKT (tCliHandle CliHandle, UINT4 u4KeyId, 
                          UINT4 u4RcvKeyId,UINT4 u4Algo, UINT1 *pu1MasterKey, 
                   UINT1 u1KeyLen, UINT1 u1TcpOptExc);
INT4 Bgp4UnconfigTcpAoGlobalMKT(tCliHandle CliHandle, UINT4 u1KeyId);
INT4 Bgp4NeighborAOConfig(tCliHandle CliHandle, tAddrPrefix * pIpAddress, UINT1 u1Option, UINT1 u1Enable);
INT4 Bgp4NeighborAssociateMKT(tCliHandle CliHandle, tAddrPrefix * pIpAddress, UINT4 u4MktId,  UINT1 u1EnableFlag);
INT4 Bgp4ShowMKT(tCliHandle CliHandle);
INT4 Bgp4Ipv4NeighborShowTCPAO(tCliHandle CliHandle ,tAddrPrefix * pIpAddress);
INT4 Bgp4Ipv6NeighborShowTCPAO (tCliHandle CliHandle ,tAddrPrefix * pIpAddress);
INT4 Bgp4Ipv4CliDisplayNeighborTCPAO(tCliHandle CliHandle,tAddrPrefix * pIpAddress);
INT4 Bgp4Ipv6CliDisplayNeighborTCPAO(tCliHandle CliHandle,tAddrPrefix * pIpAddress);
VOID BGP4CliDisplayRedisOspf(tCliHandle CliHandle,INT4,UINT4);
INT4 Bgp4ConfigPeerGroupBfdStatus (tCliHandle CliHandle, UINT1 *pu1PeerGroupName, UINT1 u1BfdStatus);
INT4 Bgp4ConfigPeerBfdStatus (tCliHandle CliHandle, tAddrPrefix *pPeerAddr, UINT1 u1BfdStatus);
INT4 Bgp4CliConfLblAllocMode (tCliHandle CliHandle, INT4 i4LblAllocMode);
VOID Bgp4PeerExtTableVpnv4Info (tCliHandle CliHandle, INT4 i4ExtPeerType,
                                tSNMP_OCTET_STRING_TYPE * pIpNetwork,
                                UINT4 *pu4PagingStatus);
VOID Bgp4PeerExtTableEvpnInfo (tCliHandle CliHandle, INT4 i4ExtPeerType,
                                tSNMP_OCTET_STRING_TYPE * pIpNetwork,
                                UINT4 *pu4PagingStatus);


#endif   /* BGP4CLI_H */
