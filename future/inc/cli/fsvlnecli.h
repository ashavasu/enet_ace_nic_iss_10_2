/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsvlnecli.h,v 1.6 2014/08/22 11:10:34 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsVlanBridgeMode[10];
#define nmhSetFsVlanBridgeMode(i4SetValFsVlanBridgeMode) \
 nmhSetCmn(FsVlanBridgeMode, 10, FsVlanBridgeModeSet, VlanLock, VlanUnLock, 0, 0, 0, "%i", i4SetValFsVlanBridgeMode)


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsVlanTunnelBpduPri[10];
extern UINT4 FsVlanTunnelStpAddress[10];
extern UINT4 FsVlanTunnelLacpAddress[10];
extern UINT4 FsVlanTunnelDot1xAddress[10];
extern UINT4 FsVlanTunnelGvrpAddress[10];
extern UINT4 FsVlanTunnelGmrpAddress[10];
extern UINT4 FsVlanTunnelMvrpAddress[10];
extern UINT4 FsVlanTunnelMmrpAddress[10];
extern UINT4 FsVlanTunnelElmiAddress[10];
extern UINT4 FsVlanTunnelLldpAddress[10];
extern UINT4 FsVlanTunnelEcfmAddress[10];
extern UINT4 FsVlanTunnelEoamAddress[10];

#define nmhSetFsVlanTunnelBpduPri(i4SetValFsVlanTunnelBpduPri) \
 nmhSetCmn(FsVlanTunnelBpduPri, 10, FsVlanTunnelBpduPriSet, VlanLock, VlanUnLock, 0, 0, 0, "%i", i4SetValFsVlanTunnelBpduPri)
#define nmhSetFsVlanTunnelStpAddress(SetValFsVlanTunnelStpAddress) \
 nmhSetCmn(FsVlanTunnelStpAddress, 10, FsVlanTunnelStpAddressSet, VlanLock, VlanUnLock, 0, 0, 0, "%m", SetValFsVlanTunnelStpAddress)
#define nmhSetFsVlanTunnelLacpAddress(SetValFsVlanTunnelLacpAddress) \
 nmhSetCmn(FsVlanTunnelLacpAddress, 10, FsVlanTunnelLacpAddressSet, VlanLock, VlanUnLock, 0, 0, 0, "%m", SetValFsVlanTunnelLacpAddress)
#define nmhSetFsVlanTunnelDot1xAddress(SetValFsVlanTunnelDot1xAddress) \
 nmhSetCmn(FsVlanTunnelDot1xAddress, 10, FsVlanTunnelDot1xAddressSet, VlanLock, VlanUnLock, 0, 0, 0, "%m", SetValFsVlanTunnelDot1xAddress)
#define nmhSetFsVlanTunnelGvrpAddress(SetValFsVlanTunnelGvrpAddress) \
 nmhSetCmn(FsVlanTunnelGvrpAddress, 10, FsVlanTunnelGvrpAddressSet, VlanLock, VlanUnLock, 0, 0, 0, "%m", SetValFsVlanTunnelGvrpAddress)
#define nmhSetFsVlanTunnelGmrpAddress(SetValFsVlanTunnelGmrpAddress) \
 nmhSetCmn(FsVlanTunnelGmrpAddress, 10, FsVlanTunnelGmrpAddressSet, VlanLock, VlanUnLock, 0, 0, 0, "%m", SetValFsVlanTunnelGmrpAddress)
#define nmhSetFsVlanTunnelMvrpAddress(SetValFsVlanTunnelMvrpAddress)    \
    nmhSetCmn(FsVlanTunnelMvrpAddress, 10, FsVlanTunnelMvrpAddressSet, VlanLock,,VlanUnLock, 0, 0, 0, "%m", SetValFsVlanTunnelMvrpAddress)
#define nmhSetFsVlanTunnelMmrpAddress(SetValFsVlanTunnelMmrpAddress)    \
    nmhSetCmn(FsVlanTunnelMmrpAddress, 10, FsVlanTunnelMmrpAddressSet, VlanLock,,VlanUnLock, 0, 0, 0, "%m", SetValFsVlanTunnelMmrpAddress)
#define nmhSetFsVlanTunnelElmiAddress(SetValFsVlanTunnelElmiAddress)    \
    nmhSetCmn(FsVlanTunnelElmiAddress, 10, FsVlanTunnelElmiAddressSet, NULL, NULL, 0, 0, 0, "%m", SetValFsVlanTunnelElmiAddress)
#define nmhSetFsVlanTunnelLldpAddress(SetValFsVlanTunnelLldpAddress)    \
    nmhSetCmn(FsVlanTunnelLldpAddress, 10, FsVlanTunnelLldpAddressSet, NULL, NULL, 0, 0, 0, "%m", SetValFsVlanTunnelLldpAddress)
#define nmhSetFsVlanTunnelEcfmAddress(SetValFsVlanTunnelEcfmAddress)    \
    nmhSetCmn(FsVlanTunnelEcfmAddress, 10, FsVlanTunnelEcfmAddressSet, NULL, NULL, 0, 0, 0, "%m", SetValFsVlanTunnelEcfmAddress)
#define nmhSetFsVlanTunnelEoamAddress(SetValFsVlanTunnelEoamAddress)    \
    nmhSetCmn(FsVlanTunnelEoamAddress, 10, FsVlanTunnelEoamAddressSet, NULL, NULL, 0, 0, 0, "%m", SetValFsVlanTunnelEoamAddress)

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsVlanPort[12];
extern UINT4 FsVlanTunnelStatus[12];
#define nmhSetFsVlanTunnelStatus(i4FsVlanPort ,i4SetValFsVlanTunnelStatus) \
 nmhSetCmn(FsVlanTunnelStatus, 12, FsVlanTunnelStatusSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %i", i4FsVlanPort ,i4SetValFsVlanTunnelStatus)


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsVlanTunnelProtocolDot1x[12];
extern UINT4 FsVlanTunnelProtocolLacp[12];
extern UINT4 FsVlanTunnelProtocolStp[12];
extern UINT4 FsVlanTunnelProtocolGvrp[12];
extern UINT4 FsVlanTunnelProtocolGmrp[12];
extern UINT4 FsVlanTunnelProtocolIgmp[12];
extern UINT4 FsVlanTunnelProtocolMvrp[12];
extern UINT4 FsVlanTunnelProtocolMmrp[12];
extern UINT4 FsVlanTunnelProtocolElmi[12];
extern UINT4 FsVlanTunnelProtocolLldp[12];
extern UINT4 FsVlanTunnelProtocolEcfm[12];
extern UINT4 FsVlanTunnelOverrideOption[12];
extern UINT4 FsVlanTunnelProtocolEoam[12];

#define nmhSetFsVlanTunnelProtocolDot1x(i4FsVlanPort ,i4SetValFsVlanTunnelProtocolDot1x) \
 nmhSetCmn(FsVlanTunnelProtocolDot1x, 12, FsVlanTunnelProtocolDot1xSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %i", i4FsVlanPort ,i4SetValFsVlanTunnelProtocolDot1x)
#define nmhSetFsVlanTunnelProtocolLacp(i4FsVlanPort ,i4SetValFsVlanTunnelProtocolLacp) \
 nmhSetCmn(FsVlanTunnelProtocolLacp, 12, FsVlanTunnelProtocolLacpSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %i", i4FsVlanPort ,i4SetValFsVlanTunnelProtocolLacp)
#define nmhSetFsVlanTunnelProtocolStp(i4FsVlanPort ,i4SetValFsVlanTunnelProtocolStp) \
 nmhSetCmn(FsVlanTunnelProtocolStp, 12, FsVlanTunnelProtocolStpSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %i", i4FsVlanPort ,i4SetValFsVlanTunnelProtocolStp)
#define nmhSetFsVlanTunnelProtocolGvrp(i4FsVlanPort ,i4SetValFsVlanTunnelProtocolGvrp) \
 nmhSetCmn(FsVlanTunnelProtocolGvrp, 12, FsVlanTunnelProtocolGvrpSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %i", i4FsVlanPort ,i4SetValFsVlanTunnelProtocolGvrp)
#define nmhSetFsVlanTunnelProtocolGmrp(i4FsVlanPort ,i4SetValFsVlanTunnelProtocolGmrp) \
 nmhSetCmn(FsVlanTunnelProtocolGmrp, 12, FsVlanTunnelProtocolGmrpSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %i", i4FsVlanPort ,i4SetValFsVlanTunnelProtocolGmrp)
#define nmhSetFsVlanTunnelProtocolIgmp(i4FsVlanPort ,i4SetValFsVlanTunnelProtocolIgmp) \
 nmhSetCmn(FsVlanTunnelProtocolIgmp, 12, FsVlanTunnelProtocolIgmpSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %i", i4FsVlanPort ,i4SetValFsVlanTunnelProtocolIgmp)
#define nmhSetFsVlanTunnelProtocolMvrp(i4FsVlanPort ,i4SetValFsVlanTunnelProtocolMvrp)  \
    nmhSetCmn(FsVlanTunnelProtocolMvrp, 12, FsVlanTunnelProtocolMvrpSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %i", i4FsVlanPort ,i4SetValFsVlanTunnelProtocolMvrp)

#define nmhSetFsVlanTunnelProtocolMmrp(i4FsVlanPort ,i4SetValFsVlanTunnelProtoco lMmrp)  \
    nmhSetCmn(FsVlanTunnelProtocolMmrp, 12, FsVlanTunnelProtocolMmrpSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %i", i4FsVlanPort ,i4SetValFsVlanTunnelProtocolMmrp)

#define nmhSetFsVlanTunnelProtocolElmi(i4FsVlanPort ,i4SetValFsVlanTunnelProtocolElmi)  \
    nmhSetCmn(FsVlanTunnelProtocolElmi, 12, FsVlanTunnelProtocolElmiSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsVlanPort ,i4SetValFsVlanTunnelProtocolElmi)
#define nmhSetFsVlanTunnelProtocolLldp(i4FsVlanPort ,i4SetValFsVlanTunnelProtocolLldp)  \
    nmhSetCmn(FsVlanTunnelProtocolLldp, 12, FsVlanTunnelProtocolLldpSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsVlanPort ,i4SetValFsVlanTunnelProtocolLldp)
#define nmhSetFsVlanTunnelProtocolEcfm(i4FsVlanPort ,i4SetValFsVlanTunnelProtocolEcfm)  \
    nmhSetCmn(FsVlanTunnelProtocolEcfm, 12, FsVlanTunnelProtocolEcfmSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsVlanPort ,i4SetValFsVlanTunnelProtocolEcfm)

#define nmhSetFsVlanTunnelOverrideOption(i4FsVlanPort ,i4SetValFsVlanTunnelOverrideOption)      \
        nmhSetCmn(FsVlanTunnelOverrideOption, 12, FsVlanTunnelOverrideOptionSet, VlanLock, VlanUnLock, 0, 0, 1, "%i %i", i4FsVlanPort ,i4SetValFsVlanTunnelOverrideOption)

#define nmhSetFsVlanTunnelProtocolEoam(i4FsVlanPort ,i4SetValFsVlanTunnelProtocolEoam)  \
    nmhSetCmn(FsVlanTunnelProtocolEoam, 12, FsVlanTunnelProtocolEoamSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsVlanPort ,i4SetValFsVlanTunnelProtocolEoam)

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsServiceVlanId[12];
extern UINT4 FsServiceProtocolEnum[12];
extern UINT4 FsServiceVlanRsvdMacaddress[12];
extern UINT4 FsServiceVlanTunnelMacaddress[12];
extern UINT4 FsServiceVlanTunnelProtocolStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsServiceVlanRsvdMacaddress(i4FsServiceVlanId , i4FsServiceProtocolEnum ,SetValFsServiceVlanRsvdMacaddress)       \
        nmhSetCmn(FsServiceVlanRsvdMacaddress, 12, FsServiceVlanRsvdMacaddressSet, VlanLock, VlanUnLock, 0, 0, 2, "%i %i %m", i4FsServiceVlanId , i4FsServiceProtocolEnum ,SetValFsServiceVlanRsvdMacaddress)
#define nmhSetFsServiceVlanTunnelMacaddress(i4FsServiceVlanId , i4FsServiceProtocolEnum ,SetValFsServiceVlanTunnelMacaddress)   \
        nmhSetCmn(FsServiceVlanTunnelMacaddress, 12, FsServiceVlanTunnelMacaddressSet, VlanLock, VlanUnLock, 0, 0, 2, "%i %i %m", i4FsServiceVlanId , i4FsServiceProtocolEnum ,SetValFsServiceVlanTunnelMacaddress)
#define nmhSetFsServiceVlanTunnelProtocolStatus(i4FsServiceVlanId , i4FsServiceProtocolEnum ,i4SetValFsServiceVlanTunnelProtocolStatus) \
        nmhSetCmn(FsServiceVlanTunnelProtocolStatus, 12, FsServiceVlanTunnelProtocolStatusSet, VlanLock, VlanUnLock, 0, 0, 2, "%i %i %i", i4FsServiceVlanId , i4FsServiceProtocolEnum ,i4SetValFsServiceVlanTunnelProtocolStatus)

#endif
