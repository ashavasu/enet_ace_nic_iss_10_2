/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * Description: This file contains macros to the CLI commands and
 * structures for the TTCP module
 *******************************************************************/


#include "cli.h"
#include <stdio.h>
#include <ctype.h>
#define TTCP_CLI_MAX_COMMANDS 10
#define TTCP_CLI_MAX_ARGS 10

INT4 cli_process_ttcp_cmd    PROTO ((tCliHandle CliHandle, UINT4, ...));
    
enum
{
TTCP_CMD_OPT = 1
};
