/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsisiscli.h,v 1.10 2016/03/26 09:56:49 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsIsisMaxInstances[10];
extern UINT4 FsIsisMaxCircuits[10];
extern UINT4 FsIsisMaxAreaAddrs[10];
extern UINT4 FsIsisMaxAdjs[10];
extern UINT4 FsIsisMaxIPRAs[10];
extern UINT4 FsIsisMaxEvents[10];
extern UINT4 FsIsisMaxSummAddr[10];
extern UINT4 FsIsisStatus[10];
extern UINT4 FsIsisMaxLSPEntries[10];
extern UINT4 FsIsisMaxMAA[10];
extern UINT4 FsIsisFTStatus[10];
extern UINT4 FsIsisFactor[10];
extern UINT4 FsIsisMaxRoutes[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsIsisMaxInstances(i4SetValFsIsisMaxInstances) \
 nmhSetCmn(FsIsisMaxInstances, 10, FsIsisMaxInstancesSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsIsisMaxInstances)
#define nmhSetFsIsisMaxCircuits(i4SetValFsIsisMaxCircuits) \
 nmhSetCmn(FsIsisMaxCircuits, 10, FsIsisMaxCircuitsSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsIsisMaxCircuits)
#define nmhSetFsIsisMaxAreaAddrs(i4SetValFsIsisMaxAreaAddrs) \
 nmhSetCmn(FsIsisMaxAreaAddrs, 10, FsIsisMaxAreaAddrsSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsIsisMaxAreaAddrs)
#define nmhSetFsIsisMaxAdjs(i4SetValFsIsisMaxAdjs) \
 nmhSetCmn(FsIsisMaxAdjs, 10, FsIsisMaxAdjsSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsIsisMaxAdjs)
#define nmhSetFsIsisMaxIPRAs(i4SetValFsIsisMaxIPRAs) \
 nmhSetCmn(FsIsisMaxIPRAs, 10, FsIsisMaxIPRAsSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsIsisMaxIPRAs)
#define nmhSetFsIsisMaxEvents(i4SetValFsIsisMaxEvents) \
 nmhSetCmn(FsIsisMaxEvents, 10, FsIsisMaxEventsSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsIsisMaxEvents)
#define nmhSetFsIsisMaxSummAddr(i4SetValFsIsisMaxSummAddr) \
 nmhSetCmn(FsIsisMaxSummAddr, 10, FsIsisMaxSummAddrSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsIsisMaxSummAddr)
#define nmhSetFsIsisStatus(i4SetValFsIsisStatus) \
 nmhSetCmn(FsIsisStatus, 10, FsIsisStatusSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsIsisStatus)
#define nmhSetFsIsisMaxLSPEntries(i4SetValFsIsisMaxLSPEntries) \
 nmhSetCmn(FsIsisMaxLSPEntries, 10, FsIsisMaxLSPEntriesSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsIsisMaxLSPEntries)
#define nmhSetFsIsisMaxMAA(i4SetValFsIsisMaxMAA) \
 nmhSetCmn(FsIsisMaxMAA, 10, FsIsisMaxMAASet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsIsisMaxMAA)
#define nmhSetFsIsisFTStatus(i4SetValFsIsisFTStatus) \
 nmhSetCmn(FsIsisFTStatus, 10, FsIsisFTStatusSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsIsisFTStatus)
#define nmhSetFsIsisFactor(i4SetValFsIsisFactor) \
 nmhSetCmn(FsIsisFactor, 10, FsIsisFactorSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsIsisFactor)
#define nmhSetFsIsisMaxRoutes(i4SetValFsIsisMaxRoutes) \
 nmhSetCmn(FsIsisMaxRoutes, 10, FsIsisMaxRoutesSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsIsisMaxRoutes)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsIsisExtSysInstance[13];
extern UINT4 FsIsisExtSysAuthSupp[13];
extern UINT4 FsIsisExtSysAreaAuthType[13];
extern UINT4 FsIsisExtSysDomainAuthType[13];
extern UINT4 FsIsisExtSysAreaTxPasswd[13];
extern UINT4 FsIsisExtSysDomainTxPasswd[13];
extern UINT4 FsIsisExtSysMinSPFSchTime[13];
extern UINT4 FsIsisExtSysMaxSPFSchTime[13];
extern UINT4 FsIsisExtSysMinLSPMark[13];
extern UINT4 FsIsisExtSysMaxLSPMark[13];
extern UINT4 FsIsisExtSysDelMetSupp[13];
extern UINT4 FsIsisExtSysErrMetSupp[13];
extern UINT4 FsIsisExtSysExpMetSupp[13];
extern UINT4 FsIsisExtSysRouterID[13];
extern UINT4 FsIsisExtRestartSupport[13];
extern UINT4 FsIsisExtGRRestartTimeInterval[13];
extern UINT4 FsIsisExtGRT2TimeIntervalLevel1[13];
extern UINT4 FsIsisExtGRT2TimeIntervalLevel2[13];
extern UINT4 FsIsisExtGRT1TimeInterval[13];
extern UINT4 FsIsisExtGRT1RetryCount[13];
extern UINT4 FsIsisExtRestartReason[13];
extern UINT4 FsIsisExtHelperSupport[13];
extern UINT4 FsIsisExtHelperGraceTimeLimit[13];
extern UINT4 FsIsisRRDAdminStatus[13];
extern UINT4 FsIsisRRDProtoMaskForEnable[13];
extern UINT4 FsIsisRRDProtoMaskForDisable[13];
extern UINT4 FsIsisRRDRouteMapName[13];
extern UINT4 FsIsisRRDImportLevel[13];
extern UINT4 FsIsisMultiTopologySupport[13];
extern UINT4 FsIsisExtSysBfdSupport[13];
extern UINT4 FsIsisExtSysBfdAllIfStatus[13];
extern UINT4 FsIsisDotCompliance[13];
extern UINT4 FsIsisExtSysDynHostNameSupport[13];
extern UINT4 FsIsisMetricStyle[13];
#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsIsisExtSysAuthSupp(i4FsIsisExtSysInstance ,i4SetValFsIsisExtSysAuthSupp) \
 nmhSetCmn(FsIsisExtSysAuthSupp, 13, FsIsisExtSysAuthSuppSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsIsisExtSysInstance ,i4SetValFsIsisExtSysAuthSupp)
#define nmhSetFsIsisExtSysAreaAuthType(i4FsIsisExtSysInstance ,i4SetValFsIsisExtSysAreaAuthType) \
 nmhSetCmn(FsIsisExtSysAreaAuthType, 13, FsIsisExtSysAreaAuthTypeSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsIsisExtSysInstance ,i4SetValFsIsisExtSysAreaAuthType)
#define nmhSetFsIsisExtSysDomainAuthType(i4FsIsisExtSysInstance ,i4SetValFsIsisExtSysDomainAuthType) \
 nmhSetCmn(FsIsisExtSysDomainAuthType, 13, FsIsisExtSysDomainAuthTypeSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsIsisExtSysInstance ,i4SetValFsIsisExtSysDomainAuthType)
#define nmhSetFsIsisExtSysAreaTxPasswd(i4FsIsisExtSysInstance ,pSetValFsIsisExtSysAreaTxPasswd) \
 nmhSetCmn(FsIsisExtSysAreaTxPasswd, 13, FsIsisExtSysAreaTxPasswdSet, NULL, NULL, 0, 0, 1, "%i %s", i4FsIsisExtSysInstance ,pSetValFsIsisExtSysAreaTxPasswd)
#define nmhSetFsIsisExtSysDomainTxPasswd(i4FsIsisExtSysInstance ,pSetValFsIsisExtSysDomainTxPasswd) \
 nmhSetCmn(FsIsisExtSysDomainTxPasswd, 13, FsIsisExtSysDomainTxPasswdSet, NULL, NULL, 0, 0, 1, "%i %s", i4FsIsisExtSysInstance ,pSetValFsIsisExtSysDomainTxPasswd)
#define nmhSetFsIsisExtSysMinSPFSchTime(i4FsIsisExtSysInstance ,i4SetValFsIsisExtSysMinSPFSchTime) \
 nmhSetCmn(FsIsisExtSysMinSPFSchTime, 13, FsIsisExtSysMinSPFSchTimeSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsIsisExtSysInstance ,i4SetValFsIsisExtSysMinSPFSchTime)
#define nmhSetFsIsisExtSysMaxSPFSchTime(i4FsIsisExtSysInstance ,i4SetValFsIsisExtSysMaxSPFSchTime) \
 nmhSetCmn(FsIsisExtSysMaxSPFSchTime, 13, FsIsisExtSysMaxSPFSchTimeSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsIsisExtSysInstance ,i4SetValFsIsisExtSysMaxSPFSchTime)
#define nmhSetFsIsisExtSysMinLSPMark(i4FsIsisExtSysInstance ,i4SetValFsIsisExtSysMinLSPMark) \
 nmhSetCmn(FsIsisExtSysMinLSPMark, 13, FsIsisExtSysMinLSPMarkSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsIsisExtSysInstance ,i4SetValFsIsisExtSysMinLSPMark)
#define nmhSetFsIsisExtSysMaxLSPMark(i4FsIsisExtSysInstance ,i4SetValFsIsisExtSysMaxLSPMark) \
 nmhSetCmn(FsIsisExtSysMaxLSPMark, 13, FsIsisExtSysMaxLSPMarkSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsIsisExtSysInstance ,i4SetValFsIsisExtSysMaxLSPMark)
#define nmhSetFsIsisExtSysDelMetSupp(i4FsIsisExtSysInstance ,i4SetValFsIsisExtSysDelMetSupp) \
 nmhSetCmn(FsIsisExtSysDelMetSupp, 13, FsIsisExtSysDelMetSuppSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsIsisExtSysInstance ,i4SetValFsIsisExtSysDelMetSupp)
#define nmhSetFsIsisExtSysErrMetSupp(i4FsIsisExtSysInstance ,i4SetValFsIsisExtSysErrMetSupp) \
 nmhSetCmn(FsIsisExtSysErrMetSupp, 13, FsIsisExtSysErrMetSuppSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsIsisExtSysInstance ,i4SetValFsIsisExtSysErrMetSupp)
#define nmhSetFsIsisExtSysExpMetSupp(i4FsIsisExtSysInstance ,i4SetValFsIsisExtSysExpMetSupp) \
 nmhSetCmn(FsIsisExtSysExpMetSupp, 13, FsIsisExtSysExpMetSuppSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsIsisExtSysInstance ,i4SetValFsIsisExtSysExpMetSupp)
#define nmhSetFsIsisExtSysRouterID(i4FsIsisExtSysInstance ,pSetValFsIsisExtSysRouterID) \
 nmhSetCmn(FsIsisExtSysRouterID, 13, FsIsisExtSysRouterIDSet, NULL, NULL, 0, 0, 1, "%i %s", i4FsIsisExtSysInstance ,pSetValFsIsisExtSysRouterID)
#define nmhSetFsIsisExtRestartSupport(i4FsIsisExtSysInstance ,i4SetValFsIsisExtRestartSupport) \
 nmhSetCmn(FsIsisExtRestartSupport, 13, FsIsisExtRestartSupportSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsIsisExtSysInstance ,i4SetValFsIsisExtRestartSupport)
#define nmhSetFsIsisExtGRRestartTimeInterval(i4FsIsisExtSysInstance ,i4SetValFsIsisExtGRRestartTimeInterval) \
 nmhSetCmn(FsIsisExtGRRestartTimeInterval, 13, FsIsisExtGRRestartTimeIntervalSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsIsisExtSysInstance ,i4SetValFsIsisExtGRRestartTimeInterval)
#define nmhSetFsIsisExtGRT2TimeIntervalLevel1(i4FsIsisExtSysInstance ,i4SetValFsIsisExtGRT2TimeIntervalLevel1) \
 nmhSetCmn(FsIsisExtGRT2TimeIntervalLevel1, 13, FsIsisExtGRT2TimeIntervalLevel1Set, NULL, NULL, 0, 0, 1, "%i %i", i4FsIsisExtSysInstance ,i4SetValFsIsisExtGRT2TimeIntervalLevel1)
#define nmhSetFsIsisExtGRT2TimeIntervalLevel2(i4FsIsisExtSysInstance ,i4SetValFsIsisExtGRT2TimeIntervalLevel2) \
 nmhSetCmn(FsIsisExtGRT2TimeIntervalLevel2, 13, FsIsisExtGRT2TimeIntervalLevel2Set, NULL, NULL, 0, 0, 1, "%i %i", i4FsIsisExtSysInstance ,i4SetValFsIsisExtGRT2TimeIntervalLevel2)
#define nmhSetFsIsisExtGRT1TimeInterval(i4FsIsisExtSysInstance ,i4SetValFsIsisExtGRT1TimeInterval) \
 nmhSetCmn(FsIsisExtGRT1TimeInterval, 13, FsIsisExtGRT1TimeIntervalSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsIsisExtSysInstance ,i4SetValFsIsisExtGRT1TimeInterval)
#define nmhSetFsIsisExtGRT1RetryCount(i4FsIsisExtSysInstance ,i4SetValFsIsisExtGRT1RetryCount) \
 nmhSetCmn(FsIsisExtGRT1RetryCount, 13, FsIsisExtGRT1RetryCountSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsIsisExtSysInstance ,i4SetValFsIsisExtGRT1RetryCount)
#define nmhSetFsIsisExtRestartReason(i4FsIsisExtSysInstance ,i4SetValFsIsisExtRestartReason) \
 nmhSetCmn(FsIsisExtRestartReason, 13, FsIsisExtRestartReasonSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsIsisExtSysInstance ,i4SetValFsIsisExtRestartReason)
#define nmhSetFsIsisExtHelperSupport(i4FsIsisExtSysInstance ,i4SetValFsIsisExtHelperSupport) \
 nmhSetCmn(FsIsisExtHelperSupport, 13, FsIsisExtHelperSupportSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsIsisExtSysInstance ,i4SetValFsIsisExtHelperSupport)
#define nmhSetFsIsisExtHelperGraceTimeLimit(i4FsIsisExtSysInstance ,i4SetValFsIsisExtHelperGraceTimeLimit) \
 nmhSetCmn(FsIsisExtHelperGraceTimeLimit, 13, FsIsisExtHelperGraceTimeLimitSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsIsisExtSysInstance ,i4SetValFsIsisExtHelperGraceTimeLimit)
#define nmhSetFsIsisRRDAdminStatus(i4FsIsisExtSysInstance ,i4SetValFsIsisRRDAdminStatus) \
 nmhSetCmn(FsIsisRRDAdminStatus, 13, FsIsisRRDAdminStatusSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsIsisExtSysInstance ,i4SetValFsIsisRRDAdminStatus)
#define nmhSetFsIsisRRDProtoMaskForEnable(i4FsIsisExtSysInstance ,i4SetValFsIsisRRDProtoMaskForEnable) \
 nmhSetCmn(FsIsisRRDProtoMaskForEnable, 13, FsIsisRRDProtoMaskForEnableSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsIsisExtSysInstance ,i4SetValFsIsisRRDProtoMaskForEnable)
#define nmhSetFsIsisRRDProtoMaskForDisable(i4FsIsisExtSysInstance ,i4SetValFsIsisRRDProtoMaskForDisable) \
 nmhSetCmn(FsIsisRRDProtoMaskForDisable, 13, FsIsisRRDProtoMaskForDisableSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsIsisExtSysInstance ,i4SetValFsIsisRRDProtoMaskForDisable)
#define nmhSetFsIsisRRDRouteMapName(i4FsIsisExtSysInstance ,pSetValFsIsisRRDRouteMapName) \
 nmhSetCmn(FsIsisRRDRouteMapName, 13, FsIsisRRDRouteMapNameSet, NULL, NULL, 0, 0, 1, "%i %s", i4FsIsisExtSysInstance ,pSetValFsIsisRRDRouteMapName)
#define nmhSetFsIsisRRDImportLevel(i4FsIsisExtSysInstance ,i4SetValFsIsisRRDImportLevel) \
 nmhSetCmn(FsIsisRRDImportLevel, 13, FsIsisRRDImportLevelSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsIsisExtSysInstance ,i4SetValFsIsisRRDImportLevel)
#define nmhSetFsIsisMultiTopologySupport(i4FsIsisExtSysInstance ,i4SetValFsIsisMultiTopologySupport) \
 nmhSetCmn(FsIsisMultiTopologySupport, 13, FsIsisMultiTopologySupportSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsIsisExtSysInstance ,i4SetValFsIsisMultiTopologySupport)
#define nmhSetFsIsisExtSysBfdSupport(i4FsIsisExtSysInstance ,i4SetValFsIsisExtSysBfdSupport) \
 nmhSetCmn(FsIsisExtSysBfdSupport, 13, FsIsisExtSysBfdSupportSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsIsisExtSysInstance ,i4SetValFsIsisExtSysBfdSupport)
#define nmhSetFsIsisExtSysBfdAllIfStatus(i4FsIsisExtSysInstance ,i4SetValFsIsisExtSysBfdAllIfStatus) \
 nmhSetCmn(FsIsisExtSysBfdAllIfStatus, 13, FsIsisExtSysBfdAllIfStatusSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsIsisExtSysInstance ,i4SetValFsIsisExtSysBfdAllIfStatus)
#define nmhSetFsIsisDotCompliance(i4FsIsisExtSysInstance, i4SetValFsIsisDotCompliance) \
  nmhSetCmn(FsIsisDotCompliance, 13, FsIsisDotComplianceSet, NULL, NULL, 0, 0, 1,"%i %i", i4FsIsisExtSysInstance, i4SetValFsIsisDotCompliance)
#define nmhSetFsIsisExtSysDynHostNameSupport(i4FsIsisExtSysInstance ,i4SetValFsIsisExtSysDynHostNameSupport) \
 nmhSetCmn(FsIsisExtSysDynHostNameSupport, 13, FsIsisExtSysDynHostNameSupportSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsIsisExtSysInstance ,i4SetValFsIsisExtSysDynHostNameSupport)
 #define nmhSetFsIsisMetricStyle(i4FsIsisExtSysInstance ,i4SetValFsIsisMetricStyle) \
 nmhSetCmnWithLock(FsIsisMetricStyle, 13, FsIsisMetricStyleSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsIsisExtSysInstance ,i4SetValFsIsisMetricStyle)
#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsIsisExtSysAreaRxPasswd[13];
extern UINT4 FsIsisExtSysAreaRxPasswdExistState[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsIsisExtSysAreaRxPasswdExistState(i4FsIsisExtSysInstance , pFsIsisExtSysAreaRxPasswd ,i4SetValFsIsisExtSysAreaRxPasswdExistState) \
 nmhSetCmn(FsIsisExtSysAreaRxPasswdExistState, 13, FsIsisExtSysAreaRxPasswdExistStateSet, NULL, NULL, 0, 1, 2, "%i %s %i", i4FsIsisExtSysInstance , pFsIsisExtSysAreaRxPasswd ,i4SetValFsIsisExtSysAreaRxPasswdExistState)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsIsisExtSysDomainRxPassword[13];
extern UINT4 FsIsisExtSysDomainRxPasswdExistState[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsIsisExtSysDomainRxPasswdExistState(i4FsIsisExtSysInstance , pFsIsisExtSysDomainRxPassword ,i4SetValFsIsisExtSysDomainRxPasswdExistState) \
 nmhSetCmn(FsIsisExtSysDomainRxPasswdExistState, 13, FsIsisExtSysDomainRxPasswdExistStateSet, NULL, NULL, 0, 1, 2, "%i %s %i", i4FsIsisExtSysInstance , pFsIsisExtSysDomainRxPassword ,i4SetValFsIsisExtSysDomainRxPasswdExistState)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsIsisExtSummAddressType[13];
extern UINT4 FsIsisExtSummAddress[13];
extern UINT4 FsIsisExtSummAddrPrefixLen[13];
extern UINT4 FsIsisExtSummAddrDelayMetric[13];
extern UINT4 FsIsisExtSummAddrErrorMetric[13];
extern UINT4 FsIsisExtSummAddrExpenseMetric[13];
extern UINT4 FsIsisExtSummAddrFullMetric[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsIsisExtSummAddrDelayMetric(i4FsIsisExtSysInstance , i4FsIsisExtSummAddressType , pFsIsisExtSummAddress , u4FsIsisExtSummAddrPrefixLen ,i4SetValFsIsisExtSummAddrDelayMetric) \
 nmhSetCmn(FsIsisExtSummAddrDelayMetric, 13, FsIsisExtSummAddrDelayMetricSet, NULL, NULL, 0, 0, 4, "%i %i %s %u %i", i4FsIsisExtSysInstance , i4FsIsisExtSummAddressType , pFsIsisExtSummAddress , u4FsIsisExtSummAddrPrefixLen ,i4SetValFsIsisExtSummAddrDelayMetric)
#define nmhSetFsIsisExtSummAddrErrorMetric(i4FsIsisExtSysInstance , i4FsIsisExtSummAddressType , pFsIsisExtSummAddress , u4FsIsisExtSummAddrPrefixLen ,i4SetValFsIsisExtSummAddrErrorMetric) \
 nmhSetCmn(FsIsisExtSummAddrErrorMetric, 13, FsIsisExtSummAddrErrorMetricSet, NULL, NULL, 0, 0, 4, "%i %i %s %u %i", i4FsIsisExtSysInstance , i4FsIsisExtSummAddressType , pFsIsisExtSummAddress , u4FsIsisExtSummAddrPrefixLen ,i4SetValFsIsisExtSummAddrErrorMetric)
#define nmhSetFsIsisExtSummAddrExpenseMetric(i4FsIsisExtSysInstance , i4FsIsisExtSummAddressType , pFsIsisExtSummAddress , u4FsIsisExtSummAddrPrefixLen ,i4SetValFsIsisExtSummAddrExpenseMetric) \
 nmhSetCmn(FsIsisExtSummAddrExpenseMetric, 13, FsIsisExtSummAddrExpenseMetricSet, NULL, NULL, 0, 0, 4, "%i %i %s %u %i", i4FsIsisExtSysInstance , i4FsIsisExtSummAddressType , pFsIsisExtSummAddress , u4FsIsisExtSummAddrPrefixLen ,i4SetValFsIsisExtSummAddrExpenseMetric)
#define nmhSetFsIsisExtSummAddrFullMetric(i4FsIsisExtSysInstance , i4FsIsisExtSummAddressType , pFsIsisExtSummAddress , u4FsIsisExtSummAddrPrefixLen ,u4SetValFsIsisExtSummAddrFullMetric) \
 nmhSetCmn(FsIsisExtSummAddrFullMetric, 13, FsIsisExtSummAddrFullMetricSet, NULL, NULL, 0, 0, 4, "%i %i %s %u %u", i4FsIsisExtSysInstance , i4FsIsisExtSummAddressType , pFsIsisExtSummAddress , u4FsIsisExtSummAddrPrefixLen ,u4SetValFsIsisExtSummAddrFullMetric)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsIsisExtCircIndex[13];
extern UINT4 FsIsisExtCircIfStatus[13];
extern UINT4 FsIsisExtCircTxEnable[13];
extern UINT4 FsIsisExtCircRxEnable[13];
extern UINT4 FsIsisExtCircSNPA[13];
extern UINT4 FsIsisExtCircBfdStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsIsisExtCircIfStatus(i4FsIsisExtSysInstance , i4FsIsisExtCircIndex ,i4SetValFsIsisExtCircIfStatus) \
 nmhSetCmn(FsIsisExtCircIfStatus, 13, FsIsisExtCircIfStatusSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4FsIsisExtSysInstance , i4FsIsisExtCircIndex ,i4SetValFsIsisExtCircIfStatus)
#define nmhSetFsIsisExtCircTxEnable(i4FsIsisExtSysInstance , i4FsIsisExtCircIndex ,i4SetValFsIsisExtCircTxEnable) \
 nmhSetCmn(FsIsisExtCircTxEnable, 13, FsIsisExtCircTxEnableSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4FsIsisExtSysInstance , i4FsIsisExtCircIndex ,i4SetValFsIsisExtCircTxEnable)
#define nmhSetFsIsisExtCircRxEnable(i4FsIsisExtSysInstance , i4FsIsisExtCircIndex ,i4SetValFsIsisExtCircRxEnable) \
 nmhSetCmn(FsIsisExtCircRxEnable, 13, FsIsisExtCircRxEnableSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4FsIsisExtSysInstance , i4FsIsisExtCircIndex ,i4SetValFsIsisExtCircRxEnable)
#define nmhSetFsIsisExtCircSNPA(i4FsIsisExtSysInstance , i4FsIsisExtCircIndex ,pSetValFsIsisExtCircSNPA) \
 nmhSetCmn(FsIsisExtCircSNPA, 13, FsIsisExtCircSNPASet, NULL, NULL, 0, 0, 2, "%i %i %s", i4FsIsisExtSysInstance , i4FsIsisExtCircIndex ,pSetValFsIsisExtCircSNPA)
#define nmhSetFsIsisExtCircBfdStatus(i4FsIsisExtSysInstance , i4FsIsisExtCircIndex ,i4SetValFsIsisExtCircBfdStatus) \
 nmhSetCmn(FsIsisExtCircBfdStatus, 13, FsIsisExtCircBfdStatusSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4FsIsisExtSysInstance , i4FsIsisExtCircIndex ,i4SetValFsIsisExtCircBfdStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsIsisExtCircLevelIndex[13];
extern UINT4 FsIsisExtCircLevelDelayMetric[13];
extern UINT4 FsIsisExtCircLevelErrorMetric[13];
extern UINT4 FsIsisExtCircLevelExpenseMetric[13];
extern UINT4 FsIsisExtCircLevelTxPassword[13];
extern UINT4 FsIsisExtCircLevelWideMetric[13];
extern UINT4 FsIsisExtCircLevelAuthType[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsIsisExtCircLevelDelayMetric(i4FsIsisExtSysInstance , i4FsIsisExtCircIndex , i4FsIsisExtCircLevelIndex ,i4SetValFsIsisExtCircLevelDelayMetric) \
 nmhSetCmn(FsIsisExtCircLevelDelayMetric, 13, FsIsisExtCircLevelDelayMetricSet, NULL, NULL, 0, 0, 3, "%i %i %i %i", i4FsIsisExtSysInstance , i4FsIsisExtCircIndex , i4FsIsisExtCircLevelIndex ,i4SetValFsIsisExtCircLevelDelayMetric)
#define nmhSetFsIsisExtCircLevelErrorMetric(i4FsIsisExtSysInstance , i4FsIsisExtCircIndex , i4FsIsisExtCircLevelIndex ,i4SetValFsIsisExtCircLevelErrorMetric) \
 nmhSetCmn(FsIsisExtCircLevelErrorMetric, 13, FsIsisExtCircLevelErrorMetricSet, NULL, NULL, 0, 0, 3, "%i %i %i %i", i4FsIsisExtSysInstance , i4FsIsisExtCircIndex , i4FsIsisExtCircLevelIndex ,i4SetValFsIsisExtCircLevelErrorMetric)
#define nmhSetFsIsisExtCircLevelExpenseMetric(i4FsIsisExtSysInstance , i4FsIsisExtCircIndex , i4FsIsisExtCircLevelIndex ,i4SetValFsIsisExtCircLevelExpenseMetric) \
 nmhSetCmn(FsIsisExtCircLevelExpenseMetric, 13, FsIsisExtCircLevelExpenseMetricSet, NULL, NULL, 0, 0, 3, "%i %i %i %i", i4FsIsisExtSysInstance , i4FsIsisExtCircIndex , i4FsIsisExtCircLevelIndex ,i4SetValFsIsisExtCircLevelExpenseMetric)
#define nmhSetFsIsisExtCircLevelTxPassword(i4FsIsisExtSysInstance , i4FsIsisExtCircIndex , i4FsIsisExtCircLevelIndex ,pSetValFsIsisExtCircLevelTxPassword) \
 nmhSetCmn(FsIsisExtCircLevelTxPassword, 13, FsIsisExtCircLevelTxPasswordSet, NULL, NULL, 0, 0, 3, "%i %i %i %s", i4FsIsisExtSysInstance , i4FsIsisExtCircIndex , i4FsIsisExtCircLevelIndex ,pSetValFsIsisExtCircLevelTxPassword)
#define nmhSetFsIsisExtCircLevelWideMetric(i4FsIsisExtSysInstance , i4FsIsisExtCircIndex , i4FsIsisExtCircLevelIndex ,u4SetValFsIsisExtCircLevelWideMetric) \
 nmhSetCmn(FsIsisExtCircLevelWideMetric, 13, FsIsisExtCircLevelWideMetricSet, NULL, NULL, 0, 0, 3, "%i %i %i %u", i4FsIsisExtSysInstance , i4FsIsisExtCircIndex , i4FsIsisExtCircLevelIndex ,u4SetValFsIsisExtCircLevelWideMetric)
#define nmhSetFsIsisExtCircLevelAuthType(i4FsIsisExtSysInstance , i4FsIsisExtCircIndex , i4FsIsisExtCircLevelIndex ,i4SetValFsIsisExtCircLevelAuthType) \
        nmhSetCmn(FsIsisExtCircLevelAuthType, 13, FsIsisExtCircLevelAuthTypeSet, NULL, NULL, 0, 0, 3, "%i %i %i %i", i4FsIsisExtSysInstance , i4FsIsisExtCircIndex , i4FsIsisExtCircLevelIndex ,i4SetValFsIsisExtCircLevelAuthType)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsIsisExtIPRAType[13];
extern UINT4 FsIsisExtIPRAIndex[13];
extern UINT4 FsIsisExtIPRADelayMetric[13];
extern UINT4 FsIsisExtIPRAErrorMetric[13];
extern UINT4 FsIsisExtIPRAExpenseMetric[13];
extern UINT4 FsIsisExtIPRADelayMetricType[13];
extern UINT4 FsIsisExtIPRAErrorMetricType[13];
extern UINT4 FsIsisExtIPRAExpenseMetricType[13];
extern UINT4 FsIsisExtIPRANextHopType[13];
extern UINT4 FsIsisExtIPRANextHop[13];
extern UINT4 FsIsisExtIPRAFullMetric[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsIsisExtIPRADelayMetric(i4FsIsisExtSysInstance , i4FsIsisExtIPRAType , i4FsIsisExtIPRAIndex ,i4SetValFsIsisExtIPRADelayMetric) \
 nmhSetCmn(FsIsisExtIPRADelayMetric, 13, FsIsisExtIPRADelayMetricSet, NULL, NULL, 0, 0, 3, "%i %i %i %i", i4FsIsisExtSysInstance , i4FsIsisExtIPRAType , i4FsIsisExtIPRAIndex ,i4SetValFsIsisExtIPRADelayMetric)
#define nmhSetFsIsisExtIPRAErrorMetric(i4FsIsisExtSysInstance , i4FsIsisExtIPRAType , i4FsIsisExtIPRAIndex ,i4SetValFsIsisExtIPRAErrorMetric) \
 nmhSetCmn(FsIsisExtIPRAErrorMetric, 13, FsIsisExtIPRAErrorMetricSet, NULL, NULL, 0, 0, 3, "%i %i %i %i", i4FsIsisExtSysInstance , i4FsIsisExtIPRAType , i4FsIsisExtIPRAIndex ,i4SetValFsIsisExtIPRAErrorMetric)
#define nmhSetFsIsisExtIPRAExpenseMetric(i4FsIsisExtSysInstance , i4FsIsisExtIPRAType , i4FsIsisExtIPRAIndex ,i4SetValFsIsisExtIPRAExpenseMetric) \
 nmhSetCmn(FsIsisExtIPRAExpenseMetric, 13, FsIsisExtIPRAExpenseMetricSet, NULL, NULL, 0, 0, 3, "%i %i %i %i", i4FsIsisExtSysInstance , i4FsIsisExtIPRAType , i4FsIsisExtIPRAIndex ,i4SetValFsIsisExtIPRAExpenseMetric)
#define nmhSetFsIsisExtIPRADelayMetricType(i4FsIsisExtSysInstance , i4FsIsisExtIPRAType , i4FsIsisExtIPRAIndex ,i4SetValFsIsisExtIPRADelayMetricType) \
 nmhSetCmn(FsIsisExtIPRADelayMetricType, 13, FsIsisExtIPRADelayMetricTypeSet, NULL, NULL, 0, 0, 3, "%i %i %i %i", i4FsIsisExtSysInstance , i4FsIsisExtIPRAType , i4FsIsisExtIPRAIndex ,i4SetValFsIsisExtIPRADelayMetricType)
#define nmhSetFsIsisExtIPRAErrorMetricType(i4FsIsisExtSysInstance , i4FsIsisExtIPRAType , i4FsIsisExtIPRAIndex ,i4SetValFsIsisExtIPRAErrorMetricType) \
 nmhSetCmn(FsIsisExtIPRAErrorMetricType, 13, FsIsisExtIPRAErrorMetricTypeSet, NULL, NULL, 0, 0, 3, "%i %i %i %i", i4FsIsisExtSysInstance , i4FsIsisExtIPRAType , i4FsIsisExtIPRAIndex ,i4SetValFsIsisExtIPRAErrorMetricType)
#define nmhSetFsIsisExtIPRAExpenseMetricType(i4FsIsisExtSysInstance , i4FsIsisExtIPRAType , i4FsIsisExtIPRAIndex ,i4SetValFsIsisExtIPRAExpenseMetricType) \
 nmhSetCmn(FsIsisExtIPRAExpenseMetricType, 13, FsIsisExtIPRAExpenseMetricTypeSet, NULL, NULL, 0, 0, 3, "%i %i %i %i", i4FsIsisExtSysInstance , i4FsIsisExtIPRAType , i4FsIsisExtIPRAIndex ,i4SetValFsIsisExtIPRAExpenseMetricType)
#define nmhSetFsIsisExtIPRANextHopType(i4FsIsisExtSysInstance , i4FsIsisExtIPRAType , i4FsIsisExtIPRAIndex ,i4SetValFsIsisExtIPRANextHopType) \
 nmhSetCmn(FsIsisExtIPRANextHopType, 13, FsIsisExtIPRANextHopTypeSet, NULL, NULL, 0, 0, 3, "%i %i %i %i", i4FsIsisExtSysInstance , i4FsIsisExtIPRAType , i4FsIsisExtIPRAIndex ,i4SetValFsIsisExtIPRANextHopType)
#define nmhSetFsIsisExtIPRANextHop(i4FsIsisExtSysInstance , i4FsIsisExtIPRAType , i4FsIsisExtIPRAIndex ,pSetValFsIsisExtIPRANextHop) \
 nmhSetCmn(FsIsisExtIPRANextHop, 13, FsIsisExtIPRANextHopSet, NULL, NULL, 0, 0, 3, "%i %i %i %s", i4FsIsisExtSysInstance , i4FsIsisExtIPRAType , i4FsIsisExtIPRAIndex ,pSetValFsIsisExtIPRANextHop)
#define nmhSetFsIsisExtIPRAFullMetric(i4FsIsisExtSysInstance , i4FsIsisExtIPRAType , i4FsIsisExtIPRAIndex ,u4SetValFsIsisExtIPRAFullMetric) \
 nmhSetCmn(FsIsisExtIPRAFullMetric, 13, FsIsisExtIPRAFullMetricSet, NULL, NULL, 0, 0, 3, "%i %i %i %u", i4FsIsisExtSysInstance , i4FsIsisExtIPRAType , i4FsIsisExtIPRAIndex ,u4SetValFsIsisExtIPRAFullMetric)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsIsisExtCircLevelRxPassword[13];
extern UINT4 FsIsisExtCircLevelRxPasswordExistState[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsIsisExtCircLevelRxPasswordExistState(i4FsIsisExtSysInstance , i4FsIsisExtCircIndex , i4FsIsisExtCircLevelIndex , pFsIsisExtCircLevelRxPassword ,i4SetValFsIsisExtCircLevelRxPasswordExistState) \
 nmhSetCmn(FsIsisExtCircLevelRxPasswordExistState, 13, FsIsisExtCircLevelRxPasswordExistStateSet, NULL, NULL, 0, 1, 4, "%i %i %i %s %i", i4FsIsisExtSysInstance , i4FsIsisExtCircIndex , i4FsIsisExtCircLevelIndex , pFsIsisExtCircLevelRxPassword ,i4SetValFsIsisExtCircLevelRxPasswordExistState)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsIsisExtIPIfIndex[13];
extern UINT4 FsIsisExtIPIfSubIndex[13];
extern UINT4 FsIsisExtIPIfAddrType[13];
extern UINT4 FsIsisExtIPIfAddr[13];
extern UINT4 FsIsisExtIPIfExistState[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsIsisExtIPIfExistState(i4FsIsisExtSysInstance , i4FsIsisExtIPIfIndex , i4FsIsisExtIPIfSubIndex , i4FsIsisExtIPIfAddrType , pFsIsisExtIPIfAddr ,i4SetValFsIsisExtIPIfExistState) \
 nmhSetCmn(FsIsisExtIPIfExistState, 13, FsIsisExtIPIfExistStateSet, NULL, NULL, 0, 1, 5, "%i %i %i %i %s %i", i4FsIsisExtSysInstance , i4FsIsisExtIPIfIndex , i4FsIsisExtIPIfSubIndex , i4FsIsisExtIPIfAddrType , pFsIsisExtIPIfAddr ,i4SetValFsIsisExtIPIfExistState)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsIsisExtLogModId[13];
extern UINT4 FsIsisExtLogLevel[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsIsisExtLogLevel(i4FsIsisExtLogModId ,i4SetValFsIsisExtLogLevel) \
 nmhSetCmn(FsIsisExtLogLevel, 13, FsIsisExtLogLevelSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsIsisExtLogModId ,i4SetValFsIsisExtLogLevel)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsIsisDistInOutRouteMapName[12];
extern UINT4 FsIsisDistInOutRouteMapType[12];
extern UINT4 FsIsisDistInOutRouteMapValue[12];
extern UINT4 FsIsisDistInOutRouteMapRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsIsisDistInOutRouteMapValue(i4FsIsisExtSysInstance , pFsIsisDistInOutRouteMapName , i4FsIsisDistInOutRouteMapType ,i4SetValFsIsisDistInOutRouteMapValue) \
 nmhSetCmn(FsIsisDistInOutRouteMapValue, 12, FsIsisDistInOutRouteMapValueSet, NULL, NULL, 0, 0, 3, "%i %s %i %i", i4FsIsisExtSysInstance , pFsIsisDistInOutRouteMapName , i4FsIsisDistInOutRouteMapType ,i4SetValFsIsisDistInOutRouteMapValue)
#define nmhSetFsIsisDistInOutRouteMapRowStatus(i4FsIsisExtSysInstance , pFsIsisDistInOutRouteMapName , i4FsIsisDistInOutRouteMapType ,i4SetValFsIsisDistInOutRouteMapRowStatus) \
 nmhSetCmn(FsIsisDistInOutRouteMapRowStatus, 12, FsIsisDistInOutRouteMapRowStatusSet, NULL, NULL, 0, 1, 3, "%i %s %i %i", i4FsIsisExtSysInstance , pFsIsisDistInOutRouteMapName , i4FsIsisDistInOutRouteMapType ,i4SetValFsIsisDistInOutRouteMapRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsIsisPreferenceValue[12];
extern UINT4 FsIsisPreferenceRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsIsisPreferenceValue(i4FsIsisExtSysInstance ,i4SetValFsIsisPreferenceValue) \
 nmhSetCmn(FsIsisPreferenceValue, 12, FsIsisPreferenceValueSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsIsisExtSysInstance ,i4SetValFsIsisPreferenceValue)
#define nmhSetFsIsisPreferenceRowStatus(i4FsIsisExtSysInstance ,i4SetValFsIsisPreferenceRowStatus) \
 nmhSetCmn(FsIsisPreferenceRowStatus, 12, FsIsisPreferenceRowStatusSet, NULL, NULL, 0, 1, 1, "%i %i", i4FsIsisExtSysInstance ,i4SetValFsIsisPreferenceRowStatus)

#endif
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsIsisRRDMetricProtocolId[12];
extern UINT4 FsIsisRRDMetricValue[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsIsisRRDMetricValue(i4FsIsisExtSysInstance , i4FsIsisRRDMetricProtocolId ,u4SetValFsIsisRRDMetricValue) \
 nmhSetCmn(FsIsisRRDMetricValue, 12, FsIsisRRDMetricValueSet, NULL, NULL, 0, 0, 2, "%i %i %u", i4FsIsisExtSysInstance , i4FsIsisRRDMetricProtocolId ,u4SetValFsIsisRRDMetricValue)

#endif
