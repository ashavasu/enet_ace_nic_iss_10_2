#ifndef __LCMGLOBCLI_H__
#define __LCMGLOBCLI_H__

/* STP CLI Command constants */

#include "cli.h"

/*COMMAND IDENTIFIRS*/

enum
{
CLI_LCM_UNI_ID,
CLI_LCM_NO_UNI_ID,
CLI_LCM_SET_ETH_SER_INSTANCE,
CLI_LCM_SET_NO_ETH_SER_INSTANCE,
CLI_LCM_UNI_TYPE,
CLI_LCM_NO_UNI_TYPE,
CLI_LCM_SET_OAM_CFM,
CLI_ELMI_MAP_CEVLAN,
CLI_ELMI_NO_MAP_CEVLAN,
CLI_LCM_SHOW_ETHER_SERVICE_EVC,
CLI_LCM_SHOW_ETH_SER_INSTANCE,
CLI_LCM_UNI_BW_PROFILE,
};
	

/* Constants defined for LCMS CLI. These values are passed as arguments 
from
* the def file
*/
#define LCM_CLI_MAX_ARGS         13
#define EVC_BUNDLING             1
#define ALL_TO_ONE   	         2
#define EVC_MULTIPLEX            3
#define OAM_CFA_PROTOCOL_TYPE    9

#define LCM_UINT4_MAX  0xFFFFFFFF
#define LCM_MIN_EFP_IDENTIFIER_VALUE               1
#define LCM_MAX_EFP_IDENTIFIER_VALUE               LCM_UINT4_MAX

INT4  cli_process_lcm_cmd  PROTO((tCliHandle  CliHandle,UINT4  u4Command,...));

INT1 LcmGetEvcConfigPrompt(INT1 *pi1ModeName,INT1 *pi1DispStr);
INT1 LcmGetEsiConfigPrompt(INT1 *pi1ModeName,INT1 *pi1DispStr);

#define CLI_ESI_MODE      "config-if-esi"

#endif
