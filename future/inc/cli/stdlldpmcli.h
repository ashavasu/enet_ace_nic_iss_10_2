/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdlldpmcli.h,v 1.1 2015/09/09 13:43:25 siva Exp $
*
* Description: LLDP-MED Standard Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 LldpXMedPortConfigTLVsTxEnable[14];
extern UINT4 LldpXMedPortConfigNotifEnable[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetLldpXMedPortConfigTLVsTxEnable(i4LldpPortConfigPortNum ,pSetValLldpXMedPortConfigTLVsTxEnable)	\
	nmhSetCmn(LldpXMedPortConfigTLVsTxEnable, 14, LldpXMedPortConfigTLVsTxEnableSet, LldpLock, LldpUnLock, 0, 0, 1, "%i %s", i4LldpPortConfigPortNum ,pSetValLldpXMedPortConfigTLVsTxEnable)
#define nmhSetLldpXMedPortConfigNotifEnable(i4LldpPortConfigPortNum ,i4SetValLldpXMedPortConfigNotifEnable)	\
	nmhSetCmn(LldpXMedPortConfigNotifEnable, 14, LldpXMedPortConfigNotifEnableSet, LldpLock, LldpUnLock, 0, 0, 1, "%i %i", i4LldpPortConfigPortNum ,i4SetValLldpXMedPortConfigNotifEnable)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 LldpXMedFastStartRepeatCount[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetLldpXMedFastStartRepeatCount(u4SetValLldpXMedFastStartRepeatCount)	\
	nmhSetCmn(LldpXMedFastStartRepeatCount, 12, LldpXMedFastStartRepeatCountSet, LldpLock, LldpUnLock, 0, 0, 0, "%u", u4SetValLldpXMedFastStartRepeatCount)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 LldpXMedLocLocationSubtype[14];
extern UINT4 LldpXMedLocLocationInfo[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetLldpXMedLocLocationInfo(i4LldpLocPortNum , i4LldpXMedLocLocationSubtype ,pSetValLldpXMedLocLocationInfo)	\
	nmhSetCmn(LldpXMedLocLocationInfo, 14, LldpXMedLocLocationInfoSet, LldpLock, LldpUnLock, 0, 0, 2, "%i %i %s", i4LldpLocPortNum , i4LldpXMedLocLocationSubtype ,pSetValLldpXMedLocLocationInfo)

#endif
