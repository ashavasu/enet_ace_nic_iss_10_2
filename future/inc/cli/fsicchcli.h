/********************************************************************
* Copyright (C) 2015 Aricent Inc . All Rights Reserved
*
* $Id: fsicchcli.h,v 1.4 2015/06/05 09:41:39 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsIcchTrcLevel[11];
extern UINT4 FsIcchStatsEnable[11];
extern UINT4 FsIcchClearStats[11];
extern UINT4 FsIcchEnableProtoSync[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsIcchTrcLevel(u4SetValFsIcchTrcLevel)    \
    nmhSetCmn(FsIcchTrcLevel, 11, FsIcchTrcLevelSet, NULL, NULL, 0, 0, 0, "%u", u4SetValFsIcchTrcLevel)
#define nmhSetFsIcchStatsEnable(i4SetValFsIcchStatsEnable)  \
    nmhSetCmn(FsIcchStatsEnable, 11, FsIcchStatsEnableSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsIcchStatsEnable)
#define nmhSetFsIcchClearStats(i4SetValFsIcchClearStats)    \
    nmhSetCmn(FsIcchClearStats, 11, FsIcchClearStatsSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsIcchClearStats)
#define nmhSetFsIcchEnableProtoSync(u4SetValFsIcchEnableProtoSync)    \
    nmhSetCmn(FsIcchEnableProtoSync, 11, FsIcchEnableProtoSyncSet, NULL, NULL, 0, 0, 0, "%u", u4SetValFsIcchEnableProtoSync)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsIcclSessionInstanceId[13];
extern UINT4 FsIcclSessionInterface[13];
extern UINT4 FsIcclSessionIpAddress[13];
extern UINT4 FsIcclSessionSubnetMask[13];
extern UINT4 FsIcclSessionVlan[13];
extern UINT4 FsIcclSessionRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsIcclSessionInterface(u4FsIcclSessionInstanceId ,pSetValFsIcclSessionInterface)  \
    nmhSetCmn(FsIcclSessionInterface, 13, FsIcclSessionInterfaceSet, NULL, NULL, 0, 0, 1, "%u %s", u4FsIcclSessionInstanceId ,pSetValFsIcclSessionInterface)
#define nmhSetFsIcclSessionIpAddress(u4FsIcclSessionInstanceId ,u4SetValFsIcclSessionIpAddress) \
    nmhSetCmn(FsIcclSessionIpAddress, 13, FsIcclSessionIpAddressSet, NULL, NULL, 0, 0, 1, "%u %p", u4FsIcclSessionInstanceId ,u4SetValFsIcclSessionIpAddress)
#define nmhSetFsIcclSessionSubnetMask(u4FsIcclSessionInstanceId ,u4SetValFsIcclSessionSubnetMask)   \
    nmhSetCmn(FsIcclSessionSubnetMask, 13, FsIcclSessionSubnetMaskSet, NULL, NULL, 0, 0, 1, "%u %p", u4FsIcclSessionInstanceId ,u4SetValFsIcclSessionSubnetMask)
#define nmhSetFsIcclSessionVlan(u4FsIcclSessionInstanceId ,i4SetValFsIcclSessionVlan)   \
    nmhSetCmn(FsIcclSessionVlan, 13, FsIcclSessionVlanSet, NULL, NULL, 0, 0, 1, "%u %i", u4FsIcclSessionInstanceId ,i4SetValFsIcclSessionVlan)
#define nmhSetFsIcclSessionRowStatus(u4FsIcclSessionInstanceId ,i4SetValFsIcclSessionRowStatus) \
    nmhSetCmn(FsIcclSessionRowStatus, 13, FsIcclSessionRowStatusSet, NULL, NULL, 0, 1, 1, "%u %i", u4FsIcclSessionInstanceId ,i4SetValFsIcclSessionRowStatus)

#endif
