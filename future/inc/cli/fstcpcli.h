/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fstcpcli.h,v 1.4 2013/01/23 11:53:54 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/

#include "cli.h"
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsTcpAckOption[9];
extern UINT4 FsTcpTimeStampOption[9];
extern UINT4 FsTcpBigWndOption[9];
extern UINT4 FsTcpIncrIniWnd[9];
extern UINT4 FsTcpMaxNumOfTCB[9];
extern UINT4 FsTcpTraceDebug[9];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsTcpAckOption(i4SetValFsTcpAckOption) \
 nmhSetCmn(FsTcpAckOption, 9, FsTcpAckOptionSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsTcpAckOption)
#define nmhSetFsTcpTimeStampOption(i4SetValFsTcpTimeStampOption) \
 nmhSetCmn(FsTcpTimeStampOption, 9, FsTcpTimeStampOptionSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsTcpTimeStampOption)
#define nmhSetFsTcpBigWndOption(i4SetValFsTcpBigWndOption) \
 nmhSetCmn(FsTcpBigWndOption, 9, FsTcpBigWndOptionSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsTcpBigWndOption)
#define nmhSetFsTcpIncrIniWnd(i4SetValFsTcpIncrIniWnd) \
 nmhSetCmn(FsTcpIncrIniWnd, 9, FsTcpIncrIniWndSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsTcpIncrIniWnd)
#define nmhSetFsTcpMaxNumOfTCB(i4SetValFsTcpMaxNumOfTCB) \
 nmhSetCmn(FsTcpMaxNumOfTCB, 9, FsTcpMaxNumOfTCBSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsTcpMaxNumOfTCB)
#define nmhSetFsTcpTraceDebug(i4SetValFsTcpTraceDebug) \
 nmhSetCmn(FsTcpTraceDebug, 9, FsTcpTraceDebugSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsTcpTraceDebug)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsTcpConnLocalAddress[11];
extern UINT4 FsTcpConnLocalPort[11];
extern UINT4 FsTcpConnRemAddress[11];
extern UINT4 FsTcpConnRemPort[11];
extern UINT4 FsTcpKaMainTmr[11];
extern UINT4 FsTcpKaRetransTmr[11];
extern UINT4 FsTcpKaRetransCnt[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsTcpKaMainTmr(u4FsTcpConnLocalAddress , i4FsTcpConnLocalPort , u4FsTcpConnRemAddress , i4FsTcpConnRemPort ,i4SetValFsTcpKaMainTmr) \
 nmhSetCmn(FsTcpKaMainTmr, 11, FsTcpKaMainTmrSet, NULL, NULL, 0, 0, 4, "%p %i %p %i %i", u4FsTcpConnLocalAddress , i4FsTcpConnLocalPort , u4FsTcpConnRemAddress , i4FsTcpConnRemPort ,i4SetValFsTcpKaMainTmr)
#define nmhSetFsTcpKaRetransTmr(u4FsTcpConnLocalAddress , i4FsTcpConnLocalPort , u4FsTcpConnRemAddress , i4FsTcpConnRemPort ,i4SetValFsTcpKaRetransTmr) \
 nmhSetCmn(FsTcpKaRetransTmr, 11, FsTcpKaRetransTmrSet, NULL, NULL, 0, 0, 4, "%p %i %p %i %i", u4FsTcpConnLocalAddress , i4FsTcpConnLocalPort , u4FsTcpConnRemAddress , i4FsTcpConnRemPort ,i4SetValFsTcpKaRetransTmr)
#define nmhSetFsTcpKaRetransCnt(u4FsTcpConnLocalAddress , i4FsTcpConnLocalPort , u4FsTcpConnRemAddress , i4FsTcpConnRemPort ,i4SetValFsTcpKaRetransCnt) \
 nmhSetCmn(FsTcpKaRetransCnt, 11, FsTcpKaRetransCntSet, NULL, NULL, 0, 0, 4, "%p %i %p %i %i", u4FsTcpConnLocalAddress , i4FsTcpConnLocalPort , u4FsTcpConnRemAddress , i4FsTcpConnRemPort ,i4SetValFsTcpKaRetransCnt)

INT1 cli_process_fstcp_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);

enum {
    TCP_CLI_MAX_RETRIES    = 0,
};

#define FS_TCP_MAX_ARGS       15

INT4
TcpCliConfigureMaxRetries(tCliHandle CliHandle, UINT4 u4MaxRetries,  UINT4 u4ContextID );

#endif
