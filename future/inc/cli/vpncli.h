/*
 * $Id: vpncli.h,v 1.9 2014/12/09 12:43:58 siva Exp $
 */
#include "cli.h"
#include "sec.h"

#define VPN_CRYPTO_MODE   "config-crypto-map-"

/* VPN CLI command Ids */
enum {
    CLI_VPN_SET_VPN_STATUS = 1,
    CLI_VPN_CREATE_NEW_CRYPTO_MAP,
    CLI_VPN_DEL_CRYPTOMAP,
    CLI_VPN_SET_MODE,
    CLI_VPN_SET_PEER_IP,
    CLI_VPN_SET_CRYPTO_PARAMS,
    CLI_VPN_SET_ACCESS_PARAMS,
    CLI_VPN_SET_POLICY_TO_INTERFACE,
    CLI_VPN_DEL_POLICY_FROM_INTERFACE,
    CLI_VPN_SET_POLICY_TYPE,
    CLI_VPN_IPSEC_PROPOSAL,
    CLI_VPN_IKE_PROPOSAL,
    CLI_VPN_ISAKMP_PEER_IDENTITY,
    CLI_VPN_ISAKMP_LOCAL_IDENTITY,
    CLI_VPN_SHOW_VPN_CONFIG,
    CLI_VPN_SHOW_VPN_POLICY,
    CLI_VPN_ADD_RA_USER,
    CLI_VPN_DEL_RA_USER,
    CLI_VPN_ADD_RA_ADDRESS_POOL,
    CLI_VPN_DEL_RA_ADDRESS_POOL,
    CLI_VPN_SHOW_VPN_RA_USERS,
    CLI_VPN_SHOW_VPN_RA_ADDRESSPOOL,
    CLI_VPN_SHOW_GLOB_PKTS_STATS,
    CLI_VPN_SHOW_GLOB_IKE_STATS,
    CLI_VPN_SHOW_VPN_LOGS,
    CLI_VPN_ADD_REMOTE_ID_INFO,
    CLI_VPN_DEL_REMOTE_ID_INFO,
    CLI_VPN_SHOW_REMOTE_ID_INFO,
    CLI_VPN_CLEAR_VPN_LOGS,
    CLI_VPN_GEN_KEYPAIR,
    CLI_VPN_IMPORT_KEY,
    CLI_VPN_SHOW_KEYS,
    CLI_VPN_DEL_KEYPAIR,
    CLI_VPN_GEN_CERT_REQ,
    CLI_VPN_IMPORT_CERT,
    CLI_VPN_SHOW_CERTS,
    CLI_VPN_DEL_CERT,
    CLI_VPN_IMPORT_PEER_CERT,
    CLI_VPN_SHOW_PEER_CERTS,
    CLI_VPN_DEL_PEER_CERT,
    CLI_VPN_IMPORT_CA_CERT,
    CLI_VPN_SHOW_CA_CERTS,
    CLI_VPN_DEL_CA_CERT,
    CLI_VPN_CERT_MAP_PEER,
    CLI_VPN_SHOW_CERT_MAP,
    CLI_VPN_DEL_CERT_MAP,
    CLI_VPN_RA_GLOBAL,
    CLI_VPN_IKE_VERSION,
    CLI_VPN_SAVE_CERT,
    CLI_VPN_IKE_TRIGGER,
    CLI_VPN_SET_IPV6_PEER,
    CLI_VPN_IKE_IPV6_TRIGGER,
    CLI_VPN_ADD_IPV6_RA_ADDRESS_POOL,
    CLI_VPN_SET_IPV6_ACCESS_PARAMS,
    CLI_VPN_SET_ENCRYPT_TYPE,
    CLI_VPN_SET_DECRYPT_TYPE,
    CLI_VPN_IKE_DEBUG,
    CLI_VPN_IKE_NO_DEBUG,
    CLI_VPN_IPSEC_DEBUG,
    CLI_VPN_IPSEC_NO_DEBUG
};

#define SEC_FILTER                       1 
#define SEC_ALLOW                        2 
#define SEC_APPLY                        3 
#define SEC_BYPASS                       4 

#define CLI_VPN_IPSEC_MANUAL             1
#define CLI_VPN_PRESHARED                2 
#define CLI_VPN_CERT                     3
#define CLI_VPN_XAUTH                    4
#define CLI_VPN_RA_PRESHARED             5 
#define CLI_VPN_RA_CERT                  6 
#define CLI_VPN_XAUTH_CERT               7 

#define CLI_VPN_TUNNEL                   1
#define CLI_VPN_TRANSPORT                2

#define CLI_AR_ENABLE                    1
#define CLI_AR_DISABLE                   2

#define CLI_VPN_AH                       51
#define CLI_VPN_ESP                      50

#define CLI_HMAC_MD5                     1
#define CLI_HMAC_SHA1                    2

#define CLI_VPN_LIFETIME_SECS            1
#define CLI_VPN_LIFETIME_KB              2
#define CLI_VPN_LIFETIME_MINS            3
#define CLI_VPN_LIFETIME_HRS             4
#define CLI_VPN_LIFETIME_DAYS            5

/* IKE Version */
#define CLI_VPN_IKE_V1                   0x10
#define CLI_VPN_IKE_V2                   0x20

/* IKE Exchange modes */
#define CLI_VPN_IKE_MAIN_MODE         2 
#define CLI_VPN_IKE_AGGRESSIVE_MODE   4

/* IKE Encryption Algo AES Key Length */
#define  CLI_VPN_IKE_AES_KEY_LEN_128       128  /* Key Length 128 bits */
#define  CLI_VPN_IKE_AES_KEY_LEN_192       192  /* Key Length 192 bits */
#define  CLI_VPN_IKE_AES_KEY_LEN_256       256  /* Key Length 256 bits */

/* IKE Authentication method */
#define CLI_VPN_IKE_PRESHARED_KEY         1
#define CLI_VPN_IKE_RSA_SIGNATURE         3

/* IKE Group Descriptipns */
#define CLI_VPN_IKE_DH_GROUP_1               1      /* 768-bit MODP */
#define CLI_VPN_IKE_DH_GROUP_2               2      /* 1024-bit MODP group */
#define CLI_VPN_IKE_DH_GROUP_5               5      /* 1536-bit MODP group */

/* Lifetime Types */
#define FSIKE_LIFETIME_SECS            1
#define FSIKE_LIFETIME_KB              2
/* Propreitary */
#define FSIKE_LIFETIME_MINS            3
#define FSIKE_LIFETIME_HRS             4
#define FSIKE_LIFETIME_DAYS            5

/* Values for KeyID Type */
#define CLI_VPN_IKE_KEY_IPV4                 1
#define CLI_VPN_IKE_KEY_FQDN                 2
#define CLI_VPN_IKE_KEY_EMAIL                3
#define CLI_VPN_IKE_KEY_IPV6                 5
#define CLI_VPN_IKE_KEY_DN                   9
#define CLI_VPN_IKE_KEY_KEYID                11

#define CLI_VPN_DENY                     1
#define CLI_VPN_APPLY                    3
#define CLI_VPN_PERMIT                   4

#define CLI_VPN_DES                         4
#define CLI_VPN_3DES                        5
#define CLI_VPN_ESP_NULL                    11
#define CLI_VPN_AES_128                     12
#define CLI_VPN_AES_192                     13
#define CLI_VPN_AES_256                     14 

#define CLI_VPN_ICMP                        1 
#define CLI_VPN_ANY_PROTO                9000

#define SEC_AUTH_KEY_SIZE         44

#define ESP_KEY_SEPERATOR         '.'
#define ESP_FIRST_KEY_TERMINATION  16
#define ESP_SECOND_KEY_TERMINATION 33
#define ESP_START_SECOND_KEY       17
#define ESP_START_THIRD_KEY        34



/* IKE certificate related macros - START*/

#define CLI_VPN_SHOW_ALL                 "0"
#define CLI_VPN_SHOW_DYNAMIC             "1"
#define CLI_VPN_DEL_DYNAMIC              "1"
#define CLI_VPN_DEL_ALL                  "2"

#define CLI_VPN_MAP_ALL                  0
#define CLI_VPN_MAP_DEFAULT              "default"
#define CLI_VPN_DEL_MAP_ALL              0

#define CLI_VPN_PEER_CERT_UNTRUSTED      1
#define CLI_VPN_PEER_CERT_TRUSTED        2

#define CLI_VPN_NBITS_512                512
#define CLI_VPN_NBITS_1024               1024
#define CLI_VPN_NBITS_2048               2048
#define CLI_VPN_NBITS_4096               4096

#define CLI_VPN_AUTH_DSA                 2
#define CLI_VPN_AUTH_RSA                 3

#define CLI_VPN_CERT_ENCODE_PEM          1
#define CLI_VPN_CERT_ENCODE_DER          2

#define CLI_VPN_CERT_SHOW                46
#define CLI_VPN_PEER_CERT_SHOW           49
#define CLI_VPN_CA_CERT_SHOW             52
#define CLI_VPN_CERT_MAP_SHOW            55

#define CLI_VPN_ERASE_CERT               47
#define CLI_VPN_ERASE_PEER_CERT          50
#define CLI_VPN_ERASE_CA_CERT            53
#define CLI_VPN_ERASE_CERT_MAP           56
/* IKE certificate related macros - END*/

enum {
    CLI_VPN_ERR_PEER_INFO =1,
    CLI_VPN_ERR_ACL_PARAM,
    CLI_VPN_ERR_IPSEC_MODE,
    CLI_VPN_ERR_PHASEI_INFO,
    CLI_VPN_ERR_PHASEII_INFO,
    CLI_VPN_ERR_VPN_MODE,
    CLI_VPN_IPC_SFNT_FAIL,
    CLI_VPN_FILL_IPSEC_ACCESS_FAIL,
    CLI_VPN_MEM_ALLOC_FAIL,
    CLI_VPN_BUILD_SFNT_MSG_FAIL,
    CLI_VPN_INVLAID_INTF_PARAMS,
    CLI_VPN_ERR_MAN_ENCRY_PARAM,
    CLI_VPN_ERR_MAN_ENCRY_KEY_LEN,
    CLI_VPN_ERR_MAN_AUTH_PARAM,
    CLI_VPN_ERR_MAN_AUTH_KEY_LEN,
    CLI_VPN_ERR_MAN_KEY,
    CLI_VPN_ERR_MAN_SPI_PARAM,
    CLI_VPN_ERR_NO_POLICY,
    CLI_VPN_ERR_ID_INFO,
    CLI_VPN_ERR_REMOTE_ID_NOT_EXIST,
    CLI_VPN_ERR_MAX_REMOTE_IDS,
    CLI_VPN_ERR_IKE_LTIME_INFO,
    CLI_VPN_ERR_IPSEC_LTIME_INFO,
    CLI_VPN_ERR_POLICY_NOT_MANUAL,
    CLI_VPN_ERR_POLICY_NOT_IKE,
    CLI_VPN_ERR_USER_SECRET_LENGTH,
    CLI_VPN_ERR_USER_NAME_LENGTH,
    CLI_VPN_ERR_DUP_ENTRY,
    CLI_VPN_ERR_RAVPN_MAX_USERS,
    CLI_VPN_ERR_RAVPN_MAX_ADDR_POOLS,
    CLI_VPN_ERR_RAVPN_ADDR_POOL_NAME_LEN,
    CLI_VPN_ERR_MAX_VPN_POLICIES,
    CLI_VPN_ERR_CLEAR_LOG,
    CLI_VPN_ERR_REMID_REF,
    CLI_VPN_ERR_XAUTH_EXCHG_MODE,
    CLI_VPN_ERR_REMOTE_ID_ACTIVE,
    CLI_VPN_ERR_PHASEI_RAVPN_MODE,
    CLI_VPN_ERR_WAN_IP,
    CLI_VPN_ERR_WAN_TYPE,
    CLI_VPN_ERR_MP_TYPE,
    CLI_VPN_ERR_NO_RAVPN_POOL,
    CLI_VPN_ERR_RAVPN_POOL_ACTIVE,
    CLI_VPN_ERR_INVALID_RAVPN_ACL,
    CLI_VPN_ERR_POLICY_NAME_LENGTH,
    CLI_VPN_ERR_RAVPN_GLOBAL,
    CLI_VPN_ERR_IKE_VERSION_NOT_SET,
    CLI_VPN_ERR_IKE_VERSION_SET_FAILED,
    CLI_VPN_ERR_RAVPN_USER_NOT_EXIST,
    CLI_VPN_ERR_INVALID_PORT_RANGE,
    CLI_VPN_ERR_REMOTE_ID_DIFF_AUTH,
    CLI_VPN_ERR_V2_XAUTH_FAILED,
    CLI_VPN_ERR_IPV6_ACL_PARAM,
    CLI_VPN_ERR_VPN_MODE_ALGO,
    CLI_VPN_ERR_VPN_MODE_CONFIG_ALGO,
    CLI_VPN_ERR_MAN_TDES_KEY,
    CLI_VPN_ERR_FIPS_AGGRESSIVE_MODE,
    CLI_VPN_ERR_REDUNDANT_POLICY_TO_PEER,
    CLI_VPN_ERR_ACL_SRC_PARAM,
    CLI_VPN_ERR_ACL_DST_PARAM,
    CLI_VPN_ERR_ACL_SRC_DST_PARAM
};

INT1
VpnGetCryptoMapPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

INT4
cli_process_vpn_cmd(tCliHandle Clihandle, UINT4 u4Command, ...);

extern INT4
CfaValidateMask (UINT4);

