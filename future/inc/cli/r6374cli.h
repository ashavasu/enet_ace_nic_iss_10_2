/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: r6374cli.h,v 1.6 2017/07/25 12:00:35 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              in clicmds.def and cli.c file.
 ********************************************************************/


#ifndef _R6374CLI_H_
#define _R6374CLI_H_
#include "cli.h"

enum
{
    CLI_RFC6374_SHOW_LM_TABLE = 1,
    CLI_RFC6374_SHOW_DM_TABLE
};

enum{
CLI_RFC6374_SYS_START,
CLI_RFC6374_SYS_SHUTDOWN,
CLI_RFC6374_MODULE_ENABLE,
CLI_RFC6374_MODULE_DISABLE,
CLI_RFC6374_CREATE_SERVICE_TNL,
CLI_RFC6374_CREATE_SERVICE_PW,
CLI_RFC6374_DELETE_SERVICE,
CLI_RFC6374_ENTER_SERVICEMODE,
CLI_RFC6374_SERVICEMODE_TNL_FWD_ID,
CLI_RFC6374_SERVICEMODE_TNL_REV_ID,
CLI_RFC6374_SERVICEMODE_TNL_SRC_ADDR,
CLI_RFC6374_SERVICEMODE_TNL_DST_ADDR,
CLI_RFC6374_SERVICEMODE_PW,
CLI_RFC6374_SERVICEMODE_TC,
CLI_RFC6374_SERVICEMODE_NEGOTIATION,
CLI_RFC6374_SERVICEMODE_LM_TS_FORMAT,
CLI_RFC6374_SERVICEMODE_DM_TS_FORMAT,
CLI_RFC6374_SERVICEMODE_LMDM_TS_FORMAT,
CLI_RFC6374_INIT_LM_FROM_SERVICEMODE,
CLI_RFC6374_INIT_DM_FROM_SERVICEMODE,
CLI_RFC6374_INIT_LM_FROM_GLOBAL,
CLI_RFC6374_INIT_DM_FROM_GLOBAL,
CLI_RFC6374_CONFIG_LM_PARAMS,
CLI_RFC6374_CONFIG_DM_PARAMS,
CLI_RFC6374_RESET_LM_PARAMS,
CLI_RFC6374_RESET_DM_PARAMS,
CLI_RFC6374_ENCAP_TYPE,
CLI_RFC6374_SHOW_STATS_INFO,
CLI_RFC6374_SHOW_STATS_INFO_ALL,
CLI_RFC6374_SHOW_SERVICE_INFO,
CLI_RFC6374_SHOW_SERVICE_INFO_ALL,
CLI_RFC6374_DEBUG,
CLI_RFC6374_NO_DEBUG,
CLI_RFC6374_ENABLE_TRAP,
CLI_RFC6374_DISABLE_TRAP,
CLI_RFC6374_CLEAR_STATS_INFO_ALL,
CLI_RFC6374_CLEAR_STATS_INFO,
CLI_RFC6374_CLEAR_LOSS_BUFFER,
CLI_RFC6374_CLEAR_DELAY_BUFFER,
CLI_RFC6374_CLEAR_ALL_BUFFER,
CLI_RFC6374_CLEAR_LOSS_BUFFER_FOR_SERVICE,
CLI_RFC6374_CLEAR_DELAY_BUFFER_FOR_SERVICE,
CLI_RFC6374_CLEAR_ALL_BUFFER_FOR_SERVICE,
CLI_RFC6374_SHOW_GLOBAL,
CLI_RFC6374_INIT_LMDM_FROM_GLOBAL,
CLI_RFC6374_CONFIG_LMDM_PARAMS,
CLI_RFC6374_RESET_CONFIG_LMDM_PARAMS,
CLI_RFC6374_INIT_LMDM_FROM_SERVICEMODE,
CLI_RFC6374_SHOW_BUFFER,
CLI_RFC6374_SHOW_BUFFER_ALL,
CLI_RFC6374_DYADIC_MEASUREMENT,
CLI_RFC6374_DYADIC_PRO_ROLE,
CLI_RFC6374_SESS_INT_QUERY,
CLI_RFC6374_MIN_RECEP_INTERVAL,
CLI_RFC6374_QUERY_TRANSMIT_RETRY_COUNT
};


enum
{
/*0*/    RFC6374_CLI_MIN_ERR = 0,
/*1*/    RFC6374_CLI_SERVICE_TABLE_EMPTY,
/*2*/    RFC6374_CLI_LM_TABLE_EMPTY,
/*3*/    RFC6374_CLI_DM_TABLE_EMPTY,
/*4*/    RFC6374_CLI_SERVICE_NOT_PRESENT,
/*5*/    RFC6374_CLI_TABLES_EMPTY,
/*6*/    RFC6374_CLI_LOSS_SESSION_ONGOING,
/*7*/    RFC6374_CLI_DELAY_SESSION_ONGOING,
/*8*/    RFC6374_CLI_LOSS_MEASUREMENT_ONGOING,
/*9*/    RFC6374_CLI_DELAY_MEASUREMENT_ONGOING,
/*10*/   RFC6374_CLI_MPLS_PATH_NOT_CONFIG,
/*11*/   RFC6374_CLI_FWD_REV_TNL_SAME,
/*12*/   RFC6374_CLI_SRC_DST_IP_ADDR_SAME,
/*13*/   RFC6374_CLI_SERVICE_ALREADY_CREATED,
/*14*/   RFC6374_CLI_SERVICE_CONFIG_PATH_TUNNEL,
/*15*/   RFC6374_CLI_SERVICE_CONFIG_PATH_PW,
/*16*/   RFC6374_CLI_CMB_MODE_PROACTIVE_NOT_ALLOWED,
/*17*/   RFC6374_CLI_MODULE_DISABLED,
/*18*/   RFC6374_CLI_SESSION_ONGOING,
/*19*/   RFC6374_CLI_SERVICE_ALREADY_CONF_SAME_TUNNEL_ID,
/*20*/   RFC6374_CLI_SERVICE_ALREADY_CONF_SAME_PW_IP_ID,
/*21*/   RFC6374_CLI_MPLS_PATH_DOWN,
/*22*/   RFC6374_CLI_BFD_DOWN,
/*23*/   RFC6374_CLI_BFD_TRAFFIC_CLASS_MISMATCH,
/*24*/   RFC6374_CLI_IP_ADDR_ZERO,
/*25*/   RFC6374_CLI_CLEAR_BUFFER_NOT_POSSIBLE,
/*26*/   RFC6374_CLI_MAX_SERVICES_CREATED,
/*27*/   RFC6374_CLI_MAX_SESSIONS_RUNNING,
/*28*/   RFC6374_CLI_CMB_LOSS_DELAY_SESSION_ONGOING,
/*29*/   RFC6374_CLI_CMB_LOSS_DELAY_MEASUREMENT_ONGOING,
/*30*/   RFC6374_CLI_DYADIC_MEAS_LOSS_TYPE_ONE_WAY,
/*31*/   RFC6374_CLI_DYADIC_MEAS_DELAY_TYPE_ONE_WAY,
/*32*/   RFC6374_CLI_DYADIC_MEAS_COMB_LMDM_TYPE_ONE_WAY,
/*33*/   RFC6374_CLI_DYADIC_MEAS_ENABLE_LOSS_TYPE_ONE_WAY,
/*34*/   RFC6374_CLI_DYADIC_MEAS_ENABLE_DELAY_TYPE_ONE_WAY,
/*35*/   RFC6374_CLI_DYADIC_MEAS_ENABLE_COMB_LMDM_TYPE_ONE_WAY,
/*36*/   RFC6374_CLI_LM_DM_MODE_PROACTIVE_NOT_ALLOWED,
/*37*/   RFC6374_CLI_TSF_NTP_NOT_SUPPORTED,
/*38*/   RFC6374_CLI_DYADIC_MEAS_ENABLED,
/*49*/   RFC6374_CLI_TSF_NEGO_ENABLED,
/*40*/   RFC6374_CLI_TEST_NOT_IN_PROGRESS,
/*41*/   RFC6374_CLI_MAX_ERR
};

#ifdef _R6374CLI_C_

CONST CHR1  *gaR6374CliErrorString [] = {
/*0*/    NULL,
/*1*/    "Service entry is not present to display/clear\r\n",
/*2*/    "LM entry is not present to display/clear\r\n",
/*3*/    "DM entry is not present to display/clear\r\n",
/*4*/    "Service entry is not present\r\n",
/*5*/    "Entry is not present in any table to clear\r\n",
/*6*/    "Loss Measurement is OnGoing, Cannot Config the Params\r\n",
/*7*/    "Delay Measurement is OnGoing, Cannot Config the Params\r\n",
/*8*/    "Loss Measurement is OnGoing, Cannot Start Loss Again for Same Service\r\n",
/*9*/    "Delay Measurement is OnGoing, Cannot Start Delay Again for Same Service\r\n",
/*10*/   "MPLS path is not configured\r\n",
/*11*/   "Forward Tunnel Id and Reverse Tunnel Id are same\r\n",
/*12*/   "Source IP Address and Destination IP Address are same\r\n",
/*13*/   "Service entry is already present\r\n",
/*14*/   "Service is configured for Path Type Tunnel\r\n",
/*15*/   "Service is configured for Path Type PW\r\n",
/*16*/   "Cannot Change Combined LMDM mode to proactive as LM/DM is set to Proactive\r\n",
/*17*/   "MPLS PM module is disabled\r\n",
/*18*/   "Service cannot be deleted when LM/DM test is in progress\r\n",
/*19*/   "Forward/Reverse Tunnel Id is in use by other Service\r\n",
/*20*/   "Pseudowire Id/Destination IP is in use by other Service\r\n",
/*21*/   "MPLS Path status is down for Fwd-Tunnel/Rev-Tunnel/PW\r\n",
/*22*/   "BFD Session Status is down\r\n",
/*23*/   "BFD Session traffic class is different from Service traffic class\r\n",
/*24*/   "IP Address is zero\r\n",
/*25*/   "Clear buffer not possible either no entry present or LM/DM ongoing\r\n",
/*26*/   "Service cannot be created as max limit reached\r\n",
/*27*/   "Session cannot be started as max limit reached\r\n",
/*28*/   "Combined Loss and Delay Measurement is OnGoing, Cannot Config the Params\r\n",
/*29*/   "Combined Loss and Delay Measurement is OnGoing, Cannot Start test again for Same Service\r\n",
/*30*/   "Cannot Change the Loss Type to One-way as Dyadic is enabled\r\n",
/*31*/   "Cannot Change the Delay Type to One-way as Dyadic is enabled\r\n",
/*32*/   "Cannot Change the Comb LMDM Type to One-way as Dyadic is enabled\r\n",
/*33*/   "Cannot Change the Dyadic to Enable/Disable as Loss Type is set to One-way\r\n",
/*34*/   "Cannot Change the Dyadic to Enable/Disable as Delay Type is set to One-way\r\n",
/*35*/   "Cannot Change the Dyadic to Enable/Disable as Comb LMDM Type is set to One-way\r\n",
/*36*/   "Cannot Change LM/DM mode to proactive as Combined LMDM is set to Proactive\r\n",
/*37*/   "Timestamp Format NTP is not supported\r\n",
/*38*/   "Cannot change the Timestamp Format Negotiation to enable as Dyadic is set to enabled \r\n",
/*39*/   "Cannot change the Dyadic to Enable as Timestamp Format negotiation is set to enabled \r\n",
/*40*/   "Test is not in-progress. Hence need not stop it again \r\n",
/*41*/   "Max Entry\r\n"
};

#endif

#define RFC6374_CLI_MAX_ARGS                          15
#define CLI_RFC6374_PATH_TYPE_TNL                      3
#define CLI_RFC6374_PATH_TYPE_PW                       5

#define CLI_RFC6374_ADDR_TYPE_IPV4                     0  
#define CLI_RFC6374_ADDR_TYPE_IPV6                     1

#define CLI_RFC6374_DEFAULT_CONTEXT                    0

#define CLI_RFC6374_CREATE_AND_GO                      0x04

#define RFC6374_CLI_RESET_LMDM_TYPE         0x0001
#define RFC6374_CLI_RESET_LMDM_METHOD         0x0002
#define RFC6374_CLI_RESET_LMDM_MODE         0x0004
#define RFC6374_CLI_RESET_LMDM_QUERY_INTERVAL        0x0008
#define RFC6374_CLI_RESET_LMDM_PADDING_SIZE        0x0010
#define RFC6374_CLI_SHOW_LM_BUFFER                     0x0010
#define RFC6374_CLI_SHOW_DM_BUFFER                     0x0020
#define RFC6374_CLI_SHOW_ALL_BUFFER                    0x0040

#define CLI_RFC6374_TIMER_UNIT_MILLI_SECONDS           1
#define CLI_RFC6374_TIMER_UNIT_SECONDS                 2
#define CLI_RFC6374_TIMER_UNIT_MINUTES                 3

#define CLI_RFC6374_ENABLE                             1
#define CLI_RFC6374_DISABLE                            2

#define CLI_RFC6374_START                              1
#define CLI_RFC6374_STOP                               2

#define CLI_RFC6374_TYPE_ONE_WAY                             1
#define CLI_RFC6374_TYPE_TWO_WAY                             2

#define CLI_RFC6374_TYPE_LMDM_ONE_WAY                  1
#define CLI_RFC6374_TYPE_LMDM_TWO_WAY                  2

#define CLI_RFC6374_METHOD_DIRECT                             1
#define CLI_RFC6374_METHOD_INFERED                            2

#define CLI_RFC6374_METHOD_LMDM_DIRECT                 1
#define CLI_RFC6374_METHOD_LMDM_INFERED                2

#define CLI_RFC6374_MODE_ONDEMAND                             1
#define CLI_RFC6374_MODE_PROACTIVE                            2

#define CLI_RFC6374_MODE_LMDM_ONDEMAND                 1
#define CLI_RFC6374_MODE_LMDM_PROACTIVE                2

#define CLI_RFC6374_GAL_GACH_HEADER                    1
#define CLI_RFC6374_ACH_HEADER                         2

#define CLI_RFC6374_CLEAR_LM                              1
#define CLI_RFC6374_CLEAR_DM                              2

#define CLI_RFC6374_DYADIC_MEAS_ENABLE                 1
#define CLI_RFC6374_DYADIC_MEAS_DISABLE                2

#define CLI_RFC6374_DYADIC_PRO_ROLE_ACTIVE             1
#define CLI_RFC6374_DYADIC_PRO_ROLE_PASSIVE            2

#define CLI_RFC6374_NEGOTIATION_ENABLE                 1
#define CLI_RFC6374_NEGOTIATION_DISABLE                2

#define CLI_RFC6374_SESS_INT_QUERY_ENABLE                    1
#define CLI_RFC6374_SESS_INT_QUERY_DISABLE                   2


#define CLI_RFC6374_NEGO_TS_FORMAT_NULL                0
#define CLI_RFC6374_NEGO_TS_FORMAT_SEG                 1
#define CLI_RFC6374_NEGO_TS_FORMAT_NTP                 2
#define CLI_RFC6374_NEGO_TS_FORMAT_PTP                 3

#define CLI_RFC6374_RESET_LM_TYPE                       0x00000001
#define CLI_RFC6374_RESET_LM_METHOD                     0x00000002
#define CLI_RFC6374_RESET_LM_MODE                       0x00000004
#define CLI_RFC6374_RESET_LM_QUERY_INTERVAL             0x00000008

#define CLI_RFC6374_RESET_DM_TYPE                       0x00000001
#define CLI_RFC6374_RESET_DM_MODE                       0x00000002
#define CLI_RFC6374_RESET_DM_QUERY_INTERVAL             0x00000004
#define CLI_RFC6374_RESET_DM_PADDING_SIZE              0x00000008

#define CLI_RFC6374_MIN_PADDING_SIZE                   0
#define CLI_RFC6374_MAX_PADDING_SIZE                   9000

#define CLI_RFC6374_DEFAULT_QUERY_TRANSMIT_RETRYCNT    25
#define CLI_RFC6374_DEFAULT_RECEPTION_INTERVAL         100

#define   RFC6374_NO_TRC      0x00000000
#define   RFC6374_INIT_SHUT_TRC      0x00000001
#define   RFC6374_MGMT_TRC           0x00000002
#define   RFC6374_CRITICAL_TRC       0x00000004
#define   RFC6374_CONTROL_PLANE_TRC  0x00000008
#define   RFC6374_PKT_DUMP_TRC       0x00000010
#define   RFC6374_OS_RESOURCE_TRC    0x00000020
#define   RFC6374_ALL_FAILURE_TRC    0x00000040
#define   RFC6374_FN_ENTRY_TRC       0x00000080
#define   RFC6374_FN_EXIT_TRC        0x00000100

#define CLI_RFC6374_DBG_NO_TRC                         RFC6374_NO_TRC
#define CLI_RFC6374_DBG_INIT                           RFC6374_INIT_SHUT_TRC
#define CLI_RFC6374_DBG_MGMT                           RFC6374_MGMT_TRC
#define CLI_RFC6374_DBG_CRITICAL                       RFC6374_CRITICAL_TRC 
#define CLI_RFC6374_DBG_CTRL_PLANE                     RFC6374_CONTROL_PLANE_TRC 
#define CLI_RFC6374_DBG_PKT_DUMP                       RFC6374_PKT_DUMP_TRC
#define CLI_RFC6374_DBG_RESOURCE                       RFC6374_OS_RESOURCE_TRC
#define CLI_RFC6374_DBG_ALL_FAIL                       RFC6374_ALL_FAILURE_TRC
#define CLI_RFC6374_DBG_FN_ENTRY                       RFC6374_FN_ENTRY_TRC
#define CLI_RFC6374_DBG_FN_EXIT                        RFC6374_FN_EXIT_TRC
#define CLI_RFC6374_DBG_ALL                            (CLI_RFC6374_DBG_INIT |\
                                                       CLI_RFC6374_DBG_MGMT |\
                                                        CLI_RFC6374_DBG_CRITICAL  |\
                                                        CLI_RFC6374_DBG_CTRL_PLANE  |\
                                                       CLI_RFC6374_DBG_PKT_DUMP |\
                                                       CLI_RFC6374_DBG_RESOURCE |\
                                                       CLI_RFC6374_DBG_ALL_FAIL |\
                                                       CLI_RFC6374_DBG_FN_ENTRY |\
                                                       CLI_RFC6374_DBG_FN_EXIT)

#define CLI_RFC6374_ALL_TRAP_DISABLED                  0
#define CLI_RFC6374_UNSUP_VERSION_TRAP                 (1 << RFC6374_UNSUP_VERSION_TRAP)
#define CLI_RFC6374_UNSUP_CC_TRAP                      (1 << RFC6374_UNSUP_CC_TRAP)
#define CLI_RFC6374_CONN_MISMATCH_TRAP                 (1 << RFC6374_CONN_MISMATCH_TRAP)
#define CLI_RFC6374_DATA_FORMAT_INVALID_TRAP           (1 << RFC6374_DATA_FORMAT_INVALID_TRAP)
#define CLI_RFC6374_UNSUP_MAND_TLV_TRAP                (1 << RFC6374_UNSUP_MAND_TLV_TRAP)
#define CLI_RFC6374_UNSUP_TIMESTAMP_TRAP               (1 << RFC6374_UNSUP_TIMESTAMP_TRAP)
#define CLI_RFC6374_RESP_TIMEOUT_TRAP                  (1 << RFC6374_RESP_TIMEOUT_TRAP)
#define CLI_RFC6374_RES_UNAVAIL_TRAP                   (1 << RFC6374_RES_UNAVAIL_TRAP)
#define CLI_RFC6374_TEST_ABORT_TRAP                    (1 << RFC6374_TEST_ABORT_TRAP)
#define CLI_RFC6374_PROACTIVE_TEST_RESTART_TRAP        (1 << RFC6374_PROACTIVE_TEST_RESTART_TRAP)
#define CLI_RFC6374_UNSUPP_QUERY_INTERVAL_TRAP         (1 << RFC6374_UNSUPP_QUERY_INTERVAL)

#define CLI_RFC6374_ALL_TRAP_ENABLED                   (CLI_RFC6374_UNSUP_VERSION_TRAP | \
                                                        CLI_RFC6374_UNSUP_CC_TRAP |\
                                                        CLI_RFC6374_CONN_MISMATCH_TRAP |\
                                                        CLI_RFC6374_DATA_FORMAT_INVALID_TRAP |\
                                                        CLI_RFC6374_UNSUP_MAND_TLV_TRAP |\
                                                        CLI_RFC6374_UNSUP_TIMESTAMP_TRAP |\
                                                        CLI_RFC6374_RESP_TIMEOUT_TRAP |\
                                                        CLI_RFC6374_RES_UNAVAIL_TRAP |\
                                                        CLI_RFC6374_TEST_ABORT_TRAP |\
                                                        CLI_RFC6374_PROACTIVE_TEST_RESTART_TRAP |\
                                                        CLI_RFC6374_UNSUPP_QUERY_INTERVAL_TRAP)
                                                      

#define R6374_CLI_TRAP_OPTION_LSB 0x0f
#define R6374_CLI_TRAP_OPTION_MSB 0xfe

#define R6374_CLI_ALL_TRAP_ENABLED(u1CurTrapOptionLsb, u1CurTrapOptionMsb)\
        (((u1CurTrapOptionLsb == R6374_CLI_TRAP_OPTION_LSB)\
          && (u1CurTrapOptionMsb == R6374_CLI_TRAP_OPTION_MSB))\
          ?(RFC6374_TRUE):(RFC6374_FALSE))

#define R6374_CLI_UNSUP_VERSRION_TRAP_ENABLED(u1TrapControl)\
        ((u1TrapControl & CLI_RFC6374_UNSUP_VERSION_TRAP) ?(RFC6374_TRUE):(RFC6374_FALSE))
 
#define R6374_CLI_UNSUP_CC_TRAP_ENABLED(u1TrapControl)\
        ((u1TrapControl & CLI_RFC6374_UNSUP_CC_TRAP) ?(RFC6374_TRUE):(RFC6374_FALSE))
                                                    
#define R6374_CLI_UNSUP_CONN_MISMATCH_TRAP_ENABLED(u1TrapControl)\
        ((u1TrapControl & CLI_RFC6374_CONN_MISMATCH_TRAP) ?(RFC6374_TRUE):(RFC6374_FALSE))

#define R6374_CLI_UNSUP_DATA_FORMAT_INVALID_TRAP_ENABLED(u1TrapControl)\
        ((u1TrapControl & CLI_RFC6374_DATA_FORMAT_INVALID_TRAP) ?(RFC6374_TRUE):(RFC6374_FALSE))

#define R6374_CLI_UNSUP_MAND_TLV_TRAP_ENABLED(u1TrapControl)\
        ((u1TrapControl & CLI_RFC6374_UNSUP_MAND_TLV_TRAP) ?(RFC6374_TRUE):(RFC6374_FALSE))

#define R6374_CLI_UNSUP_TIMESTAMP_TRAP_ENABLED(u1TrapControl)\
        ((u1TrapControl & CLI_RFC6374_UNSUP_TIMESTAMP_TRAP) ?(RFC6374_TRUE):(RFC6374_FALSE))

#define R6374_CLI_UNSUP_RESP_TIMEOUT_TRAP_ENABLED(u1TrapControl)\
        ((u1TrapControl & CLI_RFC6374_RESP_TIMEOUT_TRAP) ?(RFC6374_TRUE):(RFC6374_FALSE))

#define R6374_CLI_UNSUP_RES_UNAVAIL_TRAP_ENABLED(u1TrapControl)\
        ((u1TrapControl & (R6374_CLI_TRAP_OPTION_LSB >> 2)) ?(RFC6374_TRUE):(RFC6374_FALSE))

/* To check whether second bit is set in LSB or not do left shift of
 * value 1 by 1 position */
#define R6374_CLI_TEST_ABORT_TRAP_ENABLED(u1TrapControl)\
        ((u1TrapControl & (1 << 1)) ?(RFC6374_TRUE):(RFC6374_FALSE))

/* To check whether third bit is set in LSB or not do left shift of
 * value 1 by 2 position */
#define RFC6374_CLI_PROACTIVE_TEST_RESTART_TRAP(u1TrapControl)\
         ((u1TrapControl & (1 << 2)) ?(RFC6374_TRUE):(RFC6374_FALSE))

#define R6374_CLI_UNSUPP_QUERY_INTERVAL_TRAP_ENABLED(u1TrapControl)\
        ((u1TrapControl & (1 << 3)) ?(RFC6374_TRUE):(RFC6374_FALSE))

#define CLI_RFC6374_CLEAR_GLOBAL_STATS                 1         
                                                     
#define CLI_RFC6374_TRUE                               1                           
#define CLI_RFC6374_FALSE                              2                             
#define CLI_RFC6374_MAX_OID_LENGTH                     56

#define CLI_RFC6374_MAX_IPV4_MASK_LEN                  32
#define CLI_RFC6374_MAX_IPV6_MASK_LEN                  128

INT4 cli_process_rfc6374_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);

INT4 R6374CliSetSystemControl (tCliHandle CliHandle, UINT4 u4ContextId,
                               INT4 i4SystemControl);

INT4 R6374CliSetModuleStatus (tCliHandle CliHandle, UINT4 u4ContextId,
                              INT4 i4ModuleStatus);

INT4 R6374CliCreateServiceUsingTnl (tCliHandle , UINT4 ,
                                tSNMP_OCTET_STRING_TYPE ,
                                UINT4, UINT4 , 
                                tSNMP_OCTET_STRING_TYPE, 
                                tSNMP_OCTET_STRING_TYPE);

INT4 R6374CliCreateServiceUsingPW (tCliHandle , UINT4, 
                                         tSNMP_OCTET_STRING_TYPE, 
                                         tSNMP_OCTET_STRING_TYPE, 
                                         UINT4);

INT4 R6374CliDeleteService (tCliHandle CliHandle, UINT4 u4ContextId, tSNMP_OCTET_STRING_TYPE);


INT4
R6374CliServiceModeModifyFwdTnlId (tCliHandle CliHandle, UINT4 u4ContextId,
        tSNMP_OCTET_STRING_TYPE, UINT4 u4FwdTnlId);

INT4
R6374CliServiceModeModifyRevTnlId (tCliHandle CliHandle, UINT4 u4ContextId,
        tSNMP_OCTET_STRING_TYPE, UINT4 u4RevTnlId);

INT4
R6374CliServiceModeModifySrcIpAddr (tCliHandle, UINT4, tSNMP_OCTET_STRING_TYPE, 
                                    tSNMP_OCTET_STRING_TYPE);
INT4
R6374CliServiceModeModifyDestIpAddr (tCliHandle, UINT4, tSNMP_OCTET_STRING_TYPE,
                                    tSNMP_OCTET_STRING_TYPE);

INT4 R6374CliServiceModeModifyPW (tCliHandle ,UINT4 , tSNMP_OCTET_STRING_TYPE, 
                                  tSNMP_OCTET_STRING_TYPE ,UINT4 );

INT4
R6374CliServiceModeModifyTC (tCliHandle CliHandle, UINT4 u4ContextId, 
        tSNMP_OCTET_STRING_TYPE, INT4 i4TC);
INT4
R6374CliServiceModeTSFNegotiation (tCliHandle CliHandle, UINT4 u4ContextId,
                                   tSNMP_OCTET_STRING_TYPE,
       INT4 i4NegoStatus);
INT4
R6374CliServiceModeLMTSFormat (tCliHandle CliHandle, UINT4 u4ContextId,
                               tSNMP_OCTET_STRING_TYPE,
          INT4 i4LmPrefTSFormat);
INT4
R6374CliServiceModeDMTSFormat (tCliHandle CliHandle, UINT4 u4ContextId,
                               tSNMP_OCTET_STRING_TYPE,
          INT4 i4DmPrefTSFormat);

INT4
R6374CliServiceModeLMDMTSFormat (tCliHandle CliHandle, UINT4 u4ContextId,
                                 tSNMP_OCTET_STRING_TYPE,
            INT4 i4LmDmPrefTSFormat);

INT4
R6374CliInitLM (tCliHandle CliHandle,UINT4 u4ContextId, INT4 i4InitLM,
                tSNMP_OCTET_STRING_TYPE);

INT4
R6374CliInitDM (tCliHandle CliHandle,UINT4 u4ContextId, INT4 i4InitDM,
                tSNMP_OCTET_STRING_TYPE);

INT4
R6374CliConfigLMParams (tCliHandle CliHandle,UINT4 u4ContextId,
        tSNMP_OCTET_STRING_TYPE,
        INT4 i4Type, INT4 i4Method, INT4 i4Mode, UINT4 u4Count,
        UINT4 u4Interval);

INT4
R6374CliConfigDMParams (tCliHandle CliHandle,UINT4 u4ContextId,
        tSNMP_OCTET_STRING_TYPE,
        INT4 i4Type,  INT4 i4Mode, UINT4 u4Count,
        UINT4 u4Interval, UINT4 u4PadSize);

INT4
R6374CliResetLMParams (tCliHandle CliHandle,
                UINT4 u4ContextId,tSNMP_OCTET_STRING_TYPE, UINT4 u4ResetParam);

INT4
R6374CliResetDMParams (tCliHandle CliHandle,
                UINT4 u4ContextId,tSNMP_OCTET_STRING_TYPE, UINT4 u4ResetParam);
INT4
R6374CliEncapType (tCliHandle CliHandle, UINT4 u4ContextId,
                                tSNMP_OCTET_STRING_TYPE, INT4 i4Type);

INT4
R6374CliShowServiceConfigInfoAll(tCliHandle CliHandle, UINT4 u4ContextId, 
        UINT1 u1Detail);

INT4
R6374CliShowServiceConfigInfo(tCliHandle CliHandle, UINT4 u4ContextId , 
        tSNMP_OCTET_STRING_TYPE Fs6374ServiceName, UINT1 u1Detail);

INT4
R6374CliShowStatsAll (tCliHandle CliHandle, UINT4 u4ContextId);

INT4
R6374CliShowStats (tCliHandle CliHandle, UINT4 u4ContextId,
                tSNMP_OCTET_STRING_TYPE Fs6374ServiceName);

INT4
R6374CliCreateServiceEntry (tCliHandle CliHandle, UINT4 u4ContextId,
                            tSNMP_OCTET_STRING_TYPE ServiceName,
                            UINT4 u4FwdTnlOrPwId, UINT4 u4RevTnlId,
                            tSNMP_OCTET_STRING_TYPE SrcIpAddr,
                            tSNMP_OCTET_STRING_TYPE DestIpAddr,
                            UINT4 u4Command);

INT4
R6374CliShowLmBufferAll(tCliHandle CliHandle,
                UINT4 u4ContextId ,
                        UINT1 u1Detail);
INT4
R6374CliShowLmBufferPerService(tCliHandle CliHandle, UINT4 u4ContextId,
                tSNMP_OCTET_STRING_TYPE ServiceName,
                        UINT1 u1Detail);

VOID R6374ShowLmBufferPerServiceDetail (tCliHandle CliHandle, 
        UINT4 u4ContextId,
        tSNMP_OCTET_STRING_TYPE ServiceName,
        UINT4 u4SessId,
        INT4 i4Type);

INT4
R6374CliShowDmBufferAll(tCliHandle CliHandle,
                UINT4 u4ContextId ,
                        UINT1 u1Detail);
INT4
R6374CliShowDmBufferPerService(tCliHandle CliHandle, UINT4 u4ContextId,
                tSNMP_OCTET_STRING_TYPE ServiceName,
                        UINT1 u1Detail);

VOID R6374ShowDmBufferPerServiceDetail (tCliHandle CliHandle, 
        UINT4 u4ContextId,
        tSNMP_OCTET_STRING_TYPE ServiceName,
        UINT4 u4SessId);

INT1
R6374GetServiceConfCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

VOID
R6374ShowServiceConfigInfoDetail (tCliHandle CliHandle, UINT4 u4ContextId ,
                tSNMP_OCTET_STRING_TYPE ServiceName);

VOID R6374ClearServiceStats (UINT4 u4ContextId,
        tSNMP_OCTET_STRING_TYPE *pServiceName);
INT4
R6374CliClearStatsAll (tCliHandle CliHandle, UINT4 u4ContextId);

INT4
R6374CliClearStats (tCliHandle CliHandle, UINT4 u4ContextId,
                tSNMP_OCTET_STRING_TYPE Fs6374ServiceName);

PUBLIC INT4
R6374CliClearDelayBuffer (tCliHandle CliHandle, UINT4 u4ContextId);

PUBLIC INT4
R6374CliClearLossBuffer (tCliHandle CliHandle, UINT4 u4ContextId);

PUBLIC INT4
R6374CliClearBufferAll (tCliHandle CliHandle, UINT4 u4ContextId);

PUBLIC INT4
R6374CliShowRunningConfig (tCliHandle CliHandle);

INT4
R6374CliSetDebugLevel (tCliHandle CliHandle, UINT4 u4ContextId,
                                UINT4 u4DebugType, INT4 i4Action);

INT4
R6374CliSetTrapStatus (tCliHandle CliHandle, UINT4 u4ContextId,
                         UINT2 u2R6374Val, UINT1 u1Status);
INT4
R6374CliGetCxtIdFromName (UINT1 *pu1ContextName, UINT4 *pu4ContextId);

PUBLIC INT4
R6374CliClearDelayBufferForService (tCliHandle CliHandle, UINT4 u4ContextId, tSNMP_OCTET_STRING_TYPE ServiceName);

PUBLIC INT4
R6374CliClearLossBufferForService (tCliHandle CliHandle, UINT4 u4ContextId, tSNMP_OCTET_STRING_TYPE ServiceName);

PUBLIC INT4
R6374CliClearBufferAllForService (tCliHandle CliHandle, UINT4 u4ContextId, tSNMP_OCTET_STRING_TYPE ServiceName);

PUBLIC INT4 R6374CliLmDmNoRespSess (tCliHandle CliHandle, UINT4 u4ContextId,
        tSNMP_OCTET_STRING_TYPE *pService, UINT1 u1MeasurementType);

PUBLIC INT4 R6374CliDisplayLmDmNoRespSess (tCliHandle CliHandle, UINT4 u4ContextId,
        tSNMP_OCTET_STRING_TYPE *pServiceName, UINT1 u1MeasurementType);

VOID
IssMplsPmRFC6374ShowDebugging (tCliHandle CliHandle);

INT4
RFC6374CliShowGlobalInfo (tCliHandle CliHandle, UINT4 u4ContextId);

INT4
R6374CliInitLMDM (tCliHandle CliHandle, UINT4 u4ContextId, INT4 i4InitLMDM,
                  tSNMP_OCTET_STRING_TYPE ServiceName);
INT4
R6374CliDyadicMeasurement(tCliHandle CliHandle,UINT4 u4ContextId,
                          tSNMP_OCTET_STRING_TYPE gServiceName,
                          INT4 i4DyadicMeasurement);
INT4
R6374CliDyadicProactiveRole (tCliHandle CliHandle,UINT4 u4ContextId,
                             tSNMP_OCTET_STRING_TYPE ServiceName,
                             INT4 i4DyadicProactiveRole);
INT4
R6374CliConfigLMDMParams (tCliHandle CliHandle, UINT4 u4ContextId,
                          tSNMP_OCTET_STRING_TYPE ServiceName,
                          INT4 i4Type, INT4 i4Method, INT4 i4Mode,
                          UINT4 u4Count, UINT4 u4Interval, UINT4 u4PadSize);
INT4
R6374CliResetLMDMParams (tCliHandle CliHandle, UINT4 u4ContextId,
           tSNMP_OCTET_STRING_TYPE, UINT4 u4Command);

INT4
R6374CliShowBufferAll (tCliHandle CliHandle, UINT4 u4ContextId ,
                       UINT1 u1Detail);

INT4
R6374CliShowBufferPerService (tCliHandle CliHandle, UINT4 u4ContextId ,
                              tSNMP_OCTET_STRING_TYPE ServiceName, UINT4 u4Detail);
INT4
R6374CliSessionIntervalQuery(tCliHandle CliHandle,UINT4 u4ContextId,
                          tSNMP_OCTET_STRING_TYPE ServiceName,
                          INT4 i4SessIntQueryStatus);
INT4
R6374CliSetMinimumReceptionInterval(tCliHandle CliHandle,UINT4 u4ContextId,
                          tSNMP_OCTET_STRING_TYPE ServiceName,
                          UINT4 u4MinReceptionInterval);

INT4
R6374CliSetRetryCountForCongestionMgmt(tCliHandle CliHandle,UINT4 u4ContextId,
                          tSNMP_OCTET_STRING_TYPE ServiceName,
                          UINT4 u4RetryCount);

INT4
R6374CliSetQueryTransmitRetryCount(tCliHandle CliHandle,UINT4 u4ContextId,
                          tSNMP_OCTET_STRING_TYPE ServiceName,
                          UINT4 u4RetryCount);

#endif
