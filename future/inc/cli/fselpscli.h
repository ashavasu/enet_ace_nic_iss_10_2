/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fselpscli.h,v 1.5 2013/07/06 13:45:53 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsElpsGlobalTraceOption[11];
extern UINT4 FsElpsPSCChannelCode[11];
extern UINT4 FsElpsRapidTxTime[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsElpsGlobalTraceOption(i4SetValFsElpsGlobalTraceOption) \
 nmhSetCmn(FsElpsGlobalTraceOption, 11, FsElpsGlobalTraceOptionSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsElpsGlobalTraceOption)
#define nmhSetFsElpsPSCChannelCode(u4SetValFsElpsPSCChannelCode) \
 nmhSetCmn(FsElpsPSCChannelCode, 11, FsElpsPSCChannelCodeSet, NULL, NULL, 0, 0, 0, "%u", u4SetValFsElpsPSCChannelCode)
#define nmhSetFsElpsRapidTxTime(u4SetValFsElpsRapidTxTime) \
 nmhSetCmn(FsElpsRapidTxTime, 11, FsElpsRapidTxTimeSet, NULL, NULL, 0, 0, 0, "%u", u4SetValFsElpsRapidTxTime)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsElpsContextId[13];
extern UINT4 FsElpsContextSystemControl[13];
extern UINT4 FsElpsContextModuleStatus[13];
extern UINT4 FsElpsContextTraceInputString[13];
extern UINT4 FsElpsContextEnableTrap[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsElpsContextSystemControl(u4FsElpsContextId ,i4SetValFsElpsContextSystemControl) \
 nmhSetCmn(FsElpsContextSystemControl, 13, FsElpsContextSystemControlSet, NULL, NULL, 0, 0, 1, "%u %i", u4FsElpsContextId ,i4SetValFsElpsContextSystemControl)
#define nmhSetFsElpsContextModuleStatus(u4FsElpsContextId ,i4SetValFsElpsContextModuleStatus) \
 nmhSetCmn(FsElpsContextModuleStatus, 13, FsElpsContextModuleStatusSet, NULL, NULL, 0, 0, 1, "%u %i", u4FsElpsContextId ,i4SetValFsElpsContextModuleStatus)
#define nmhSetFsElpsContextTraceInputString(u4FsElpsContextId ,pSetValFsElpsContextTraceInputString) \
 nmhSetCmn(FsElpsContextTraceInputString, 13, FsElpsContextTraceInputStringSet, NULL, NULL, 0, 0, 1, "%u %s", u4FsElpsContextId ,pSetValFsElpsContextTraceInputString)
#define nmhSetFsElpsContextEnableTrap(u4FsElpsContextId ,i4SetValFsElpsContextEnableTrap) \
 nmhSetCmn(FsElpsContextEnableTrap, 13, FsElpsContextEnableTrapSet, NULL, NULL, 0, 0, 1, "%u %i", u4FsElpsContextId ,i4SetValFsElpsContextEnableTrap)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsElpsPgConfigPgId[13];
extern UINT4 FsElpsPgConfigType[13];
extern UINT4 FsElpsPgConfigServiceType[13];
extern UINT4 FsElpsPgConfigMonitorMechanism[13];
extern UINT4 FsElpsPgConfigIngressPort[13];
extern UINT4 FsElpsPgConfigWorkingPort[13];
extern UINT4 FsElpsPgConfigProtectionPort[13];
extern UINT4 FsElpsPgConfigWorkingServiceValue[13];
extern UINT4 FsElpsPgConfigProtectionServiceValue[13];
extern UINT4 FsElpsPgConfigOperType[13];
extern UINT4 FsElpsPgConfigProtType[13];
extern UINT4 FsElpsPgConfigName[13];
extern UINT4 FsElpsPgConfigRowStatus[13];
extern UINT4 FsElpsPgConfigWorkingServicePointer[13];
extern UINT4 FsElpsPgConfigWorkingReverseServicePointer[13];
extern UINT4 FsElpsPgConfigProtectionServicePointer[13];
extern UINT4 FsElpsPgConfigProtectionReverseServicePointer[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsElpsPgConfigType(u4FsElpsContextId , u4FsElpsPgConfigPgId ,i4SetValFsElpsPgConfigType) \
 nmhSetCmn(FsElpsPgConfigType, 13, FsElpsPgConfigTypeSet, NULL, NULL, 0, 0, 2, "%u %u %i", u4FsElpsContextId , u4FsElpsPgConfigPgId ,i4SetValFsElpsPgConfigType)
#define nmhSetFsElpsPgConfigServiceType(u4FsElpsContextId , u4FsElpsPgConfigPgId ,i4SetValFsElpsPgConfigServiceType) \
 nmhSetCmn(FsElpsPgConfigServiceType, 13, FsElpsPgConfigServiceTypeSet, NULL, NULL, 0, 0, 2, "%u %u %i", u4FsElpsContextId , u4FsElpsPgConfigPgId ,i4SetValFsElpsPgConfigServiceType)
#define nmhSetFsElpsPgConfigMonitorMechanism(u4FsElpsContextId , u4FsElpsPgConfigPgId ,i4SetValFsElpsPgConfigMonitorMechanism) \
 nmhSetCmn(FsElpsPgConfigMonitorMechanism, 13, FsElpsPgConfigMonitorMechanismSet, NULL, NULL, 0, 0, 2, "%u %u %i", u4FsElpsContextId , u4FsElpsPgConfigPgId ,i4SetValFsElpsPgConfigMonitorMechanism)
#define nmhSetFsElpsPgConfigIngressPort(u4FsElpsContextId , u4FsElpsPgConfigPgId ,i4SetValFsElpsPgConfigIngressPort) \
 nmhSetCmn(FsElpsPgConfigIngressPort, 13, FsElpsPgConfigIngressPortSet, NULL, NULL, 0, 0, 2, "%u %u %i", u4FsElpsContextId , u4FsElpsPgConfigPgId ,i4SetValFsElpsPgConfigIngressPort)
#define nmhSetFsElpsPgConfigWorkingPort(u4FsElpsContextId , u4FsElpsPgConfigPgId ,i4SetValFsElpsPgConfigWorkingPort) \
 nmhSetCmn(FsElpsPgConfigWorkingPort, 13, FsElpsPgConfigWorkingPortSet, NULL, NULL, 0, 0, 2, "%u %u %i", u4FsElpsContextId , u4FsElpsPgConfigPgId ,i4SetValFsElpsPgConfigWorkingPort)
#define nmhSetFsElpsPgConfigProtectionPort(u4FsElpsContextId , u4FsElpsPgConfigPgId ,i4SetValFsElpsPgConfigProtectionPort) \
 nmhSetCmn(FsElpsPgConfigProtectionPort, 13, FsElpsPgConfigProtectionPortSet, NULL, NULL, 0, 0, 2, "%u %u %i", u4FsElpsContextId , u4FsElpsPgConfigPgId ,i4SetValFsElpsPgConfigProtectionPort)
#define nmhSetFsElpsPgConfigWorkingServiceValue(u4FsElpsContextId , u4FsElpsPgConfigPgId ,u4SetValFsElpsPgConfigWorkingServiceValue) \
 nmhSetCmn(FsElpsPgConfigWorkingServiceValue, 13, FsElpsPgConfigWorkingServiceValueSet, NULL, NULL, 0, 0, 2, "%u %u %u", u4FsElpsContextId , u4FsElpsPgConfigPgId ,u4SetValFsElpsPgConfigWorkingServiceValue)
#define nmhSetFsElpsPgConfigProtectionServiceValue(u4FsElpsContextId , u4FsElpsPgConfigPgId ,u4SetValFsElpsPgConfigProtectionServiceValue) \
 nmhSetCmn(FsElpsPgConfigProtectionServiceValue, 13, FsElpsPgConfigProtectionServiceValueSet, NULL, NULL, 0, 0, 2, "%u %u %u", u4FsElpsContextId , u4FsElpsPgConfigPgId ,u4SetValFsElpsPgConfigProtectionServiceValue)
#define nmhSetFsElpsPgConfigOperType(u4FsElpsContextId , u4FsElpsPgConfigPgId ,i4SetValFsElpsPgConfigOperType) \
 nmhSetCmn(FsElpsPgConfigOperType, 13, FsElpsPgConfigOperTypeSet, NULL, NULL, 0, 0, 2, "%u %u %i", u4FsElpsContextId , u4FsElpsPgConfigPgId ,i4SetValFsElpsPgConfigOperType)
#define nmhSetFsElpsPgConfigProtType(u4FsElpsContextId , u4FsElpsPgConfigPgId ,i4SetValFsElpsPgConfigProtType) \
 nmhSetCmn(FsElpsPgConfigProtType, 13, FsElpsPgConfigProtTypeSet, NULL, NULL, 0, 0, 2, "%u %u %i", u4FsElpsContextId , u4FsElpsPgConfigPgId ,i4SetValFsElpsPgConfigProtType)
#define nmhSetFsElpsPgConfigName(u4FsElpsContextId , u4FsElpsPgConfigPgId ,pSetValFsElpsPgConfigName) \
 nmhSetCmn(FsElpsPgConfigName, 13, FsElpsPgConfigNameSet, NULL, NULL, 0, 0, 2, "%u %u %s", u4FsElpsContextId , u4FsElpsPgConfigPgId ,pSetValFsElpsPgConfigName)
#define nmhSetFsElpsPgConfigRowStatus(u4FsElpsContextId , u4FsElpsPgConfigPgId ,i4SetValFsElpsPgConfigRowStatus) \
 nmhSetCmn(FsElpsPgConfigRowStatus, 13, FsElpsPgConfigRowStatusSet, NULL, NULL, 0, 1, 2, "%u %u %i", u4FsElpsContextId , u4FsElpsPgConfigPgId ,i4SetValFsElpsPgConfigRowStatus)
#define nmhSetFsElpsPgConfigWorkingServicePointer(u4FsElpsContextId , u4FsElpsPgConfigPgId ,pSetValFsElpsPgConfigWorkingServicePointer) \
 nmhSetCmn(FsElpsPgConfigWorkingServicePointer, 13, FsElpsPgConfigWorkingServicePointerSet, NULL, NULL, 0, 0, 2, "%u %u %o", u4FsElpsContextId , u4FsElpsPgConfigPgId ,pSetValFsElpsPgConfigWorkingServicePointer)
#define nmhSetFsElpsPgConfigWorkingReverseServicePointer(u4FsElpsContextId , u4FsElpsPgConfigPgId ,pSetValFsElpsPgConfigWorkingReverseServicePointer) \
 nmhSetCmn(FsElpsPgConfigWorkingReverseServicePointer, 13, FsElpsPgConfigWorkingReverseServicePointerSet, NULL, NULL, 0, 0, 2, "%u %u %o", u4FsElpsContextId , u4FsElpsPgConfigPgId ,pSetValFsElpsPgConfigWorkingReverseServicePointer)
#define nmhSetFsElpsPgConfigProtectionServicePointer(u4FsElpsContextId , u4FsElpsPgConfigPgId ,pSetValFsElpsPgConfigProtectionServicePointer) \
 nmhSetCmn(FsElpsPgConfigProtectionServicePointer, 13, FsElpsPgConfigProtectionServicePointerSet, NULL, NULL, 0, 0, 2, "%u %u %o", u4FsElpsContextId , u4FsElpsPgConfigPgId ,pSetValFsElpsPgConfigProtectionServicePointer)
#define nmhSetFsElpsPgConfigProtectionReverseServicePointer(u4FsElpsContextId , u4FsElpsPgConfigPgId ,pSetValFsElpsPgConfigProtectionReverseServicePointer) \
 nmhSetCmn(FsElpsPgConfigProtectionReverseServicePointer, 13, FsElpsPgConfigProtectionReverseServicePointerSet, NULL, NULL, 0, 0, 2, "%u %u %o", u4FsElpsContextId , u4FsElpsPgConfigPgId ,pSetValFsElpsPgConfigProtectionReverseServicePointer)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsElpsPgCmdHoTime[13];
extern UINT4 FsElpsPgCmdWTR[13];
extern UINT4 FsElpsPgCmdExtCmd[13];
extern UINT4 FsElpsPgCmdApsPeriodicTime[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsElpsPgCmdHoTime(u4FsElpsContextId , u4FsElpsPgConfigPgId ,u4SetValFsElpsPgCmdHoTime) \
 nmhSetCmn(FsElpsPgCmdHoTime, 13, FsElpsPgCmdHoTimeSet, NULL, NULL, 0, 0, 2, "%u %u %u", u4FsElpsContextId , u4FsElpsPgConfigPgId ,u4SetValFsElpsPgCmdHoTime)
#define nmhSetFsElpsPgCmdWTR(u4FsElpsContextId , u4FsElpsPgConfigPgId ,u4SetValFsElpsPgCmdWTR) \
 nmhSetCmn(FsElpsPgCmdWTR, 13, FsElpsPgCmdWTRSet, NULL, NULL, 0, 0, 2, "%u %u %u", u4FsElpsContextId , u4FsElpsPgConfigPgId ,u4SetValFsElpsPgCmdWTR)
#define nmhSetFsElpsPgCmdExtCmd(u4FsElpsContextId , u4FsElpsPgConfigPgId ,i4SetValFsElpsPgCmdExtCmd) \
 nmhSetCmn(FsElpsPgCmdExtCmd, 13, FsElpsPgCmdExtCmdSet, NULL, NULL, 0, 0, 2, "%u %u %i", u4FsElpsContextId , u4FsElpsPgConfigPgId ,i4SetValFsElpsPgCmdExtCmd)
#define nmhSetFsElpsPgCmdApsPeriodicTime(u4FsElpsContextId , u4FsElpsPgConfigPgId ,u4SetValFsElpsPgCmdApsPeriodicTime) \
 nmhSetCmn(FsElpsPgCmdApsPeriodicTime, 13, FsElpsPgCmdApsPeriodicTimeSet, NULL, NULL, 0, 0, 2, "%u %u %u", u4FsElpsContextId , u4FsElpsPgConfigPgId ,u4SetValFsElpsPgCmdApsPeriodicTime)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsElpsPgCfmWorkingMEG[13];
extern UINT4 FsElpsPgCfmWorkingME[13];
extern UINT4 FsElpsPgCfmWorkingMEP[13];
extern UINT4 FsElpsPgCfmProtectionMEG[13];
extern UINT4 FsElpsPgCfmProtectionME[13];
extern UINT4 FsElpsPgCfmProtectionMEP[13];
extern UINT4 FsElpsPgCfmRowStatus[13];
extern UINT4 FsElpsPgCfmWorkingReverseMEG[13];
extern UINT4 FsElpsPgCfmWorkingReverseME[13];
extern UINT4 FsElpsPgCfmWorkingReverseMEP[13];
extern UINT4 FsElpsPgCfmProtectionReverseMEG[13];
extern UINT4 FsElpsPgCfmProtectionReverseME[13];
extern UINT4 FsElpsPgCfmProtectionReverseMEP[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsElpsPgCfmWorkingMEG(u4FsElpsContextId , u4FsElpsPgConfigPgId ,u4SetValFsElpsPgCfmWorkingMEG) \
 nmhSetCmn(FsElpsPgCfmWorkingMEG, 13, FsElpsPgCfmWorkingMEGSet, NULL, NULL, 0, 0, 2, "%u %u %u", u4FsElpsContextId , u4FsElpsPgConfigPgId ,u4SetValFsElpsPgCfmWorkingMEG)
#define nmhSetFsElpsPgCfmWorkingME(u4FsElpsContextId , u4FsElpsPgConfigPgId ,u4SetValFsElpsPgCfmWorkingME) \
 nmhSetCmn(FsElpsPgCfmWorkingME, 13, FsElpsPgCfmWorkingMESet, NULL, NULL, 0, 0, 2, "%u %u %u", u4FsElpsContextId , u4FsElpsPgConfigPgId ,u4SetValFsElpsPgCfmWorkingME)
#define nmhSetFsElpsPgCfmWorkingMEP(u4FsElpsContextId , u4FsElpsPgConfigPgId ,u4SetValFsElpsPgCfmWorkingMEP) \
 nmhSetCmn(FsElpsPgCfmWorkingMEP, 13, FsElpsPgCfmWorkingMEPSet, NULL, NULL, 0, 0, 2, "%u %u %u", u4FsElpsContextId , u4FsElpsPgConfigPgId ,u4SetValFsElpsPgCfmWorkingMEP)
#define nmhSetFsElpsPgCfmProtectionMEG(u4FsElpsContextId , u4FsElpsPgConfigPgId ,u4SetValFsElpsPgCfmProtectionMEG) \
 nmhSetCmn(FsElpsPgCfmProtectionMEG, 13, FsElpsPgCfmProtectionMEGSet, NULL, NULL, 0, 0, 2, "%u %u %u", u4FsElpsContextId , u4FsElpsPgConfigPgId ,u4SetValFsElpsPgCfmProtectionMEG)
#define nmhSetFsElpsPgCfmProtectionME(u4FsElpsContextId , u4FsElpsPgConfigPgId ,u4SetValFsElpsPgCfmProtectionME) \
 nmhSetCmn(FsElpsPgCfmProtectionME, 13, FsElpsPgCfmProtectionMESet, NULL, NULL, 0, 0, 2, "%u %u %u", u4FsElpsContextId , u4FsElpsPgConfigPgId ,u4SetValFsElpsPgCfmProtectionME)
#define nmhSetFsElpsPgCfmProtectionMEP(u4FsElpsContextId , u4FsElpsPgConfigPgId ,u4SetValFsElpsPgCfmProtectionMEP) \
 nmhSetCmn(FsElpsPgCfmProtectionMEP, 13, FsElpsPgCfmProtectionMEPSet, NULL, NULL, 0, 0, 2, "%u %u %u", u4FsElpsContextId , u4FsElpsPgConfigPgId ,u4SetValFsElpsPgCfmProtectionMEP)
#define nmhSetFsElpsPgCfmRowStatus(u4FsElpsContextId , u4FsElpsPgConfigPgId ,i4SetValFsElpsPgCfmRowStatus) \
 nmhSetCmn(FsElpsPgCfmRowStatus, 13, FsElpsPgCfmRowStatusSet, NULL, NULL, 0, 1, 2, "%u %u %i", u4FsElpsContextId , u4FsElpsPgConfigPgId ,i4SetValFsElpsPgCfmRowStatus)
#define nmhSetFsElpsPgCfmWorkingReverseMEG(u4FsElpsContextId , u4FsElpsPgConfigPgId ,u4SetValFsElpsPgCfmWorkingReverseMEG) \
 nmhSetCmn(FsElpsPgCfmWorkingReverseMEG, 13, FsElpsPgCfmWorkingReverseMEGSet, NULL, NULL, 0, 0, 2, "%u %u %u", u4FsElpsContextId , u4FsElpsPgConfigPgId ,u4SetValFsElpsPgCfmWorkingReverseMEG)
#define nmhSetFsElpsPgCfmWorkingReverseME(u4FsElpsContextId , u4FsElpsPgConfigPgId ,u4SetValFsElpsPgCfmWorkingReverseME) \
 nmhSetCmn(FsElpsPgCfmWorkingReverseME, 13, FsElpsPgCfmWorkingReverseMESet, NULL, NULL, 0, 0, 2, "%u %u %u", u4FsElpsContextId , u4FsElpsPgConfigPgId ,u4SetValFsElpsPgCfmWorkingReverseME)
#define nmhSetFsElpsPgCfmWorkingReverseMEP(u4FsElpsContextId , u4FsElpsPgConfigPgId ,u4SetValFsElpsPgCfmWorkingReverseMEP) \
 nmhSetCmn(FsElpsPgCfmWorkingReverseMEP, 13, FsElpsPgCfmWorkingReverseMEPSet, NULL, NULL, 0, 0, 2, "%u %u %u", u4FsElpsContextId , u4FsElpsPgConfigPgId ,u4SetValFsElpsPgCfmWorkingReverseMEP)
#define nmhSetFsElpsPgCfmProtectionReverseMEG(u4FsElpsContextId , u4FsElpsPgConfigPgId ,u4SetValFsElpsPgCfmProtectionReverseMEG) \
 nmhSetCmn(FsElpsPgCfmProtectionReverseMEG, 13, FsElpsPgCfmProtectionReverseMEGSet, NULL, NULL, 0, 0, 2, "%u %u %u", u4FsElpsContextId , u4FsElpsPgConfigPgId ,u4SetValFsElpsPgCfmProtectionReverseMEG)
#define nmhSetFsElpsPgCfmProtectionReverseME(u4FsElpsContextId , u4FsElpsPgConfigPgId ,u4SetValFsElpsPgCfmProtectionReverseME) \
 nmhSetCmn(FsElpsPgCfmProtectionReverseME, 13, FsElpsPgCfmProtectionReverseMESet, NULL, NULL, 0, 0, 2, "%u %u %u", u4FsElpsContextId , u4FsElpsPgConfigPgId ,u4SetValFsElpsPgCfmProtectionReverseME)
#define nmhSetFsElpsPgCfmProtectionReverseMEP(u4FsElpsContextId , u4FsElpsPgConfigPgId ,u4SetValFsElpsPgCfmProtectionReverseMEP) \
 nmhSetCmn(FsElpsPgCfmProtectionReverseMEP, 13, FsElpsPgCfmProtectionReverseMEPSet, NULL, NULL, 0, 0, 2, "%u %u %u", u4FsElpsContextId , u4FsElpsPgConfigPgId ,u4SetValFsElpsPgCfmProtectionReverseMEP)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsElpsPgServiceListValue[13];
extern UINT4 FsElpsPgServiceListRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsElpsPgServiceListRowStatus(u4FsElpsContextId , u4FsElpsPgConfigPgId , u4FsElpsPgServiceListValue ,i4SetValFsElpsPgServiceListRowStatus) \
 nmhSetCmn(FsElpsPgServiceListRowStatus, 13, FsElpsPgServiceListRowStatusSet, NULL, NULL, 0, 1, 3, "%u %u %u %i", u4FsElpsContextId , u4FsElpsPgConfigPgId , u4FsElpsPgServiceListValue ,i4SetValFsElpsPgServiceListRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsElpsPgServiceListId[13];
extern UINT4 FsElpsPgWorkingServiceListPointer[13];
extern UINT4 FsElpsPgWorkingReverseServiceListPointer[13];
extern UINT4 FsElpsPgProtectionServiceListPointer[13];
extern UINT4 FsElpsPgProtectionReverseServiceListPointer[13];
extern UINT4 FsElpsPgServiceListPointerRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsElpsPgWorkingServiceListPointer(u4FsElpsContextId , u4FsElpsPgConfigPgId , u4FsElpsPgServiceListId ,pSetValFsElpsPgWorkingServiceListPointer) \
 nmhSetCmn(FsElpsPgWorkingServiceListPointer, 13, FsElpsPgWorkingServiceListPointerSet, NULL, NULL, 0, 0, 3, "%u %u %u %o", u4FsElpsContextId , u4FsElpsPgConfigPgId , u4FsElpsPgServiceListId ,pSetValFsElpsPgWorkingServiceListPointer)
#define nmhSetFsElpsPgWorkingReverseServiceListPointer(u4FsElpsContextId , u4FsElpsPgConfigPgId , u4FsElpsPgServiceListId ,pSetValFsElpsPgWorkingReverseServiceListPointer) \
 nmhSetCmn(FsElpsPgWorkingReverseServiceListPointer, 13, FsElpsPgWorkingReverseServiceListPointerSet, NULL, NULL, 0, 0, 3, "%u %u %u %o", u4FsElpsContextId , u4FsElpsPgConfigPgId , u4FsElpsPgServiceListId ,pSetValFsElpsPgWorkingReverseServiceListPointer)
#define nmhSetFsElpsPgProtectionServiceListPointer(u4FsElpsContextId , u4FsElpsPgConfigPgId , u4FsElpsPgServiceListId ,pSetValFsElpsPgProtectionServiceListPointer) \
 nmhSetCmn(FsElpsPgProtectionServiceListPointer, 13, FsElpsPgProtectionServiceListPointerSet, NULL, NULL, 0, 0, 3, "%u %u %u %o", u4FsElpsContextId , u4FsElpsPgConfigPgId , u4FsElpsPgServiceListId ,pSetValFsElpsPgProtectionServiceListPointer)
#define nmhSetFsElpsPgProtectionReverseServiceListPointer(u4FsElpsContextId , u4FsElpsPgConfigPgId , u4FsElpsPgServiceListId ,pSetValFsElpsPgProtectionReverseServiceListPointer) \
 nmhSetCmn(FsElpsPgProtectionReverseServiceListPointer, 13, FsElpsPgProtectionReverseServiceListPointerSet, NULL, NULL, 0, 0, 3, "%u %u %u %o", u4FsElpsContextId , u4FsElpsPgConfigPgId , u4FsElpsPgServiceListId ,pSetValFsElpsPgProtectionReverseServiceListPointer)
#define nmhSetFsElpsPgServiceListPointerRowStatus(u4FsElpsContextId , u4FsElpsPgConfigPgId , u4FsElpsPgServiceListId ,i4SetValFsElpsPgServiceListPointerRowStatus) \
 nmhSetCmn(FsElpsPgServiceListPointerRowStatus, 13, FsElpsPgServiceListPointerRowStatusSet, NULL, NULL, 0, 1, 3, "%u %u %u %i", u4FsElpsContextId , u4FsElpsPgConfigPgId , u4FsElpsPgServiceListId ,i4SetValFsElpsPgServiceListPointerRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsElpsPgStatsClearStatistics[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsElpsPgStatsClearStatistics(u4FsElpsContextId , u4FsElpsPgConfigPgId ,i4SetValFsElpsPgStatsClearStatistics) \
 nmhSetCmn(FsElpsPgStatsClearStatistics, 13, FsElpsPgStatsClearStatisticsSet, NULL, NULL, 0, 0, 2, "%u %u %i", u4FsElpsContextId , u4FsElpsPgConfigPgId ,i4SetValFsElpsPgStatsClearStatistics)

#endif
