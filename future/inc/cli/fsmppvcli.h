/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmppvcli.h,v 1.12 2017/09/19 13:50:23 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIPvrstGlobalTrace[10];
extern UINT4 FsMIPvrstGlobalDebug[10];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIFuturePvrstContextId[12];
extern UINT4 FsMIPvrstSystemControl[12];
extern UINT4 FsMIPvrstModuleStatus[12];
extern UINT4 FsMIPvrstPathCostDefaultType[12];
extern UINT4 FsMIPvrstDynamicPathCostCalculation[12];
extern UINT4 FsMIPvrstTrace[12];
extern UINT4 FsMIPvrstDebug[12];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIPvrstPort[12];
extern UINT4 FsMIPvrstPortAdminEdgeStatus[12];
extern UINT4 FsMIPvrstPortEnabledStatus[12];
extern UINT4 FsMIPvrstRootGuard[12];
extern UINT4 FsMIPvrstBpduGuard[12];
extern UINT4 FsMIPvrstGlobalBpduGuard[12];
extern UINT4 FsMIPvrstFlushInterval[12];
extern UINT4 FsMIPvrstForceProtocolVersion[12];
extern UINT4 FsMIPvrstEncapType[12];
extern UINT4 FsMIPvrstPortAdminPointToPoint[12];
extern UINT4 FsMIPvrstPortRowStatus[12];
extern UINT4 FsMIPvrstPortLoopGuard[12];
extern UINT4 FsMIPvrstPortEnableBPDURx[12];
extern UINT4 FsMIPvrstPortEnableBPDUTx[12];
extern UINT4 FsMIPvrstBpduFilter[12];
extern UINT4 FsMIPvrstPortAutoEdge[12];
extern UINT4 FsMIPvrstPortBpduGuardAction [12];
extern UINT4 FsMIPvrstInstPortProtocolMigration [12];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIPvrstInstVlanId[12];
extern UINT4 FsMIPvrstInstBridgePriority[12];
extern UINT4 FsMIPvrstInstBridgeMaxAge[12];
extern UINT4 FsMIPvrstInstBridgeHelloTime[12];
extern UINT4 FsMIPvrstInstBridgeForwardDelay[12];
extern UINT4 FsMIPvrstInstTxHoldCount[12];
extern UINT4 FsMIPvrstInstFlushIndicationThreshold[12];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIPvrstInstPortIndex[12];
extern UINT4 FsMIPvrstInstPortEnableStatus[12];
extern UINT4 FsMIPvrstInstPortPathCost[12];
extern UINT4 FsMIPvrstInstPortPriority[12];
extern UINT4 FsMIPvrstInstPortAdminPathCost[12];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIFsPvrstSetGlobalTrapOption[10];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIPvrstSetTraps[12];
extern UINT4 FsMIPvrstCalcPortPathCostOnSpeedChg[12];
