/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: erpscli.h,v 1.50 2017/09/22 12:34:49 siva Exp $
 *
 * Description: This file contains all the macros for ERPS Module.
 *
 *******************************************************************/
#ifndef __ERPSCLI_H__
#define __ERPSCLI_H__

#include "cli.h"

/* 
 * ERPS CLI command constants. To add a new command ,a new definition needs to
 * be added to the list.
 */


enum {
    CLI_ERPS_SHUTDOWN = 1,
    CLI_ERPS_START,
    CLI_ERPS_ENABLE,
    CLI_ERPS_DISABLE,
    CLI_ERPS_GROUP_CREATE,
    CLI_ERPS_GROUP_DELETE,
    CLI_ERPS_GROUP_NAME,
    CLI_ERPS_NOTIFY_ENABLE,
    CLI_ERPS_NOTIFY_DISABLE,
    CLI_ERPS_DEBUG,
    CLI_ERPS_NO_DEBUG,
    CLI_ERPS_RING_GROUP_ACTIVATE,
    CLI_ERPS_RING_GROUP_DEACTIVATE,
    CLI_ERPS_RING_GROUP_WRKG_PORTS,
    CLI_ERPS_RING_GROUP_PROTECT_PORTS,
    CLI_ERPS_RING_VC_BLOCK,
    CLI_ERPS_RING_NO_VC_BLOCK,
    CLI_ERPS_RING_GROUP_UNPROTECT_PORTS,
    CLI_ERPS_RING_GROUP_CFM_CFG,
    CLI_ERPS_RING_GROUP_NO_CFM_CFG,
    CLI_ERPS_RING_GROUP_MPLS_PW_CFG,
    CLI_ERPS_RING_GROUP_NO_MPLS_PW_CFG,
    CLI_ERPS_RING_NORMAL_TO_FS,
    CLI_ERPS_RING_NORMAL_TO_MS,
    CLI_ERPS_RING_FS_TO_NORMAL,
    CLI_ERPS_RING_MS_TO_NORMAL,
    CLI_ERPS_RING_GROUP_REVERT_WITH_WTR,
    CLI_ERPS_RING_GROUP_REVERT,
    CLI_ERPS_RING_NON_REVERT,
    CLI_ERPS_RING_GROUP_TIMER_VAL,
    CLI_ERPS_RING_PROPAGATE_TC,
    CLI_ERPS_RING_NOT_PROPAGATE_TC,
    CLI_ERPS_RING_DEL_ALL_PROPAGATE_TC,
    CLI_ERPS_RING_DEL_PROPAGATE_TC,
    CLI_ERPS_SHOW_GLB_INFO,
    CLI_ERPS_SHOW_RING_GROUP_INFO,
    CLI_ERPS_CLEAR_STATS_ALL,
    CLI_ERPS_SHOW_DEBUG,
    CLI_ERPS_ADD_VLAN_LIST,
    CLI_ERPS_REM_VLAN_LIST,
    CLI_ERPS_OVERWRITE_VLAN_LIST,
    CLI_ERPS_REM_VLAN_GROUP,
    CLI_ERPS_SET_GROUP_MANAGER,
    CLI_ERPS_MAP_GROUP_TO_RING,
    CLI_ERPS_ASSIGN_MAC_ID,
    CLI_ERPS_SET_PROTECTION_TYPE,
    CLI_ERPS_SHOW_VLAN_GROUP_INFO,
    CLI_ERPS_RING_VC_PERIODIC_TIMER_VAL,
    CLI_ERPS_RING_MAIN_RING_ID,
    CLI_ERPS_RING_VERSION,
    CLI_ERPS_RING_RPL_NEIGHBOUR,
    CLI_ERPS_RING_NO_RPL_NEIGHBOUR,
    CLI_ERPS_RING_CLEAR_COMMAND,
    CLI_ERPS_RING_SET_WTB,
    CLI_ERPS_RING_WITHOUT_VC,
    CLI_ERPS_RING_RPL_NEXT_NEIGHBOUR, 
    CLI_ERPS_RING_NO_RPL_NEXT_NEIGHBOUR,
    CLI_ERPS_RING_INTER_CONN_NODE,
    CLI_ERPS_RING_MULTIPLE_FAILURE,
    CLI_ERPS_RING_GROUP_NAME,
    CLI_ERPS_RING_PORT1_PRESENCE,
    CLI_ERPS_RING_PORT2_PRESENCE,
    CLI_ERPS_PROP_CLEAR_FS_ENABLE,
    CLI_ERPS_PROP_CLEAR_FS_DISABLE,
    CLI_ERPS_RING_DISTRIBUTE_PORT,
    CLI_ERPS_RING_NO_DISTRIBUTE_PORT,
    CLI_ERPS_MAP_VLANGROUP_LIST_TO_RING,
    CLI_ERPS_RING_GROUP_WRKG_SUBPORTS,
    CLI_ERPS_RING_GROUP_DEL_WRKG_SUBPORTS,
    CLI_ERPS_RING_GROUP_SERVICE_TYPE,
    CLI_ERPS_TEST,
    CLI_ERPS_MONITOR_MECH,
    CLI_ERPS_RING_KVALUE,
    CLI_ERPS_FOP_TO_ENABLE,
    CLI_ERPS_FOP_TO_DISABLE
};

enum {
    CLI_ERPS_UNKNOWN_ERR = 1,
    CLI_ERPS_ERR_MODULE_SHUTDOWN,
    CLI_ERPS_ERR_MODULE_DISABLED,
    CLI_ERPS_ERR_RING_ACTIVE,
    CLI_ERPS_ERR_RING_NOT_READY,
    CLI_ERPS_ERR_RING_NOT_CREATED,
    CLI_ERPS_ERR_RING_ALREADY_CREATED,
    CLI_ERPS_ERR_RING_NOT_ACTIVE,
    CLI_ERPS_ERR_CFM_ROW_ACTIVE,
    CLI_ERPS_ERR_CFM_NOT_READY,
    CLI_ERPS_ERR_RING_CFM_ENTRY_NOT_ACTIVE,
    CLI_ERPS_ERR_RING_TC_ENTRY_NOT_PRESENT,
    CLI_ERPS_ERR_RING_SELF_TC_ENTRY,
    CLI_ERPS_ERR_RING_AS_TC_PROP_ENTRY,
    CLI_ERPS_ERR_RING_DEL_IF_CFM_EXIST,
    CLI_ERPS_ERR_CFM_ENTRY_EXISTS,
    CLI_ERPS_ERR_RPL_PORT,
    CLI_ERPS_ERR_MS_FS_EXISTS,
    CLI_ERPS_ERR_MS_LOCAL_RAPS_SF_EXISTS,
    CLI_ERPS_ERR_NOT_A_SUBRING,
    CLI_ERPS_CONTEXT_NOT_PRESENT,
    CLI_ERPS_ERR_SAME_RING_PORT,
    CLI_ERPS_ERR_TC_ENTRY_NOT_PRESENT,
    CLI_ERPS_ERR_BLOCK_PORT_VC_RECOVERY,
    CLI_ERPS_ERR_GROUP_MANAGER_ERPS,
    CLI_ERPS_ERR_VLAN_ID_INVALID,
    CLI_ERPS_ERR_VLAN_GROUP_ID_INVALID,
    CLI_ERPS_ERR_VLAN_GROUP_ID_ALREADY_MAPPED,
    CLI_ERPS_ERR_VLAN_GROUP_ID_MAP,
    CLI_ERPS_ERR_VLAN_GROUP_ID_UN_MAP,
    CLI_ERPS_ERR_RING_RAPS_VLAN,
    CLI_ERPS_ERR_MAIN_RING_NOT_EXISTS,
    CLI_ERPS_ERR_SUB_RING_LIST_EXCEEDED,
    CLI_ERPS_MANUAL_RECOVERY_NOT_SUPPORTED,
    CLI_ERPS_COMP_VERSION_MISMATCH,
    CLI_ERPS_MANUAL_NO_APS_SWITCH_METHOD,
    CLI_ERPS_ERR_RPL_OR_RPL_RPL_NEIGHBOUR,
    CLI_ERPS_ERR_MS_MS_EXISTS,
    CLI_ERPS_ERR_FS_FS_EXISTS,
    CLI_ERPS_ERR_SW_CMD_TYPE_NOT_NONE,
    CLI_ERPS_ERR_RPL_NEXT_NEIGHBOUR_ON_RPL_PORTS,
    CLI_ERPS_ERR_SAME_RPL_NEXT_NEIGHBOUR,
    CLI_ERPS_ERR_WRONG_INTER_CONN_NODE_VAL,
    CLI_ERPS_ERR_WRONG_MULTIPLE_FAILURE_VAL,
    CLI_ERPS_ERR_RING_PORT_PRESENCE,
    CLI_ERPS_ERR_VER_NOT_COM,
    CLI_ERPS_ERR_MAX_VLAN_GROUP_LIST,
    CLI_ERPS_ERR_DISTRIBUTE_PORT_IS_RING_PORT,
    CLI_ERPS_ERR_NO_REMOTE_PORTS,
    CLI_ERPS_ERR_NO_MEP_INFO_CONFIGURED,
    CLI_ERPS_ERR_PORT1_PRESENCE_REMOTE,
    CLI_ERPS_ERR_PORT2_PRESENCE_REMOTE,
    CLI_ERPS_ERR_SUBPORTLIST_INVALID, 
    CLI_ERPS_ERR_INVALID_SERVICE,
    CLI_ERPS_ERR_MEG_INVALID_RANGE,
    CLI_ERPS_ERR_ME_INVALID_RANGE,
    CLI_ERPS_ERR_INVALID_KVALUE,
    CLI_ERPS_ERR_RING_ID_ZERO,
    CLI_ERPS_ERR_RINGS_EXCEEDED,
    CLI_ERPS_ERR_RAPS_VLAN_NOT_MAPPED,
    CLI_ERPS_ERR_VLAN_IS_NOT_CREATED,
    CLI_ERPS_CLEAR_CMD_NOT_ALLOWED,
    CLI_ERPS_CLEAR_CMD_NOT_ALLOWED_ON_RPL,
    CLI_ERPS_ERR_VLAN_GROUP_NOT_PRESENT,
    CLI_ERPS_ERR_VLAN_NOT_PRESENT_IN_VLAN_GROUP,
    CLI_ERPS_ERR_WTB_GUARD_TIMER_VALUE_MISMATCH,
    CLI_ERPS_ERR_VLAN_IS_NOT_PRESENT,
    CLI_ERPS_ERR_RING_RAPS_VLAN_ALREADY_CREATED,
    CLI_ERPS_ERR_ICCL_RPL_PORT,
    CLI_ERPS_ERR_ICCL_RPL_NEIGHBOR_PORT,
    CLI_ERPS_ERR_ICCL_SWITCH_CMD,
    CLI_ERPS_ERR_MCLAG_PORT,
    CLI_ERPS_ICCL_ERR_RING_MODE,
    CLI_ERPS_ICCL_HOLD_OFF_ERR,
    CLI_ERPS_MCLAG_WITHOUT_VC,
    CLI_ERPS_ERR_MPLS_ROW_ACTIVE,
    CLI_ERPS_ERR_MPLS_NOT_READY,
    CLI_ERPS_ERR_MPLS_ENTRY_EXISTS,
    CLI_ERPS_ERR_PORT_NOT_PRESENT,
    CLI_ERPS_ERR_MAN_VER_NOT_COM,
    CLI_ERPS_ERR_MPLS_SERVICE_TYPE,
    CLI_ERPS_ERR_RING_GROUP_UNABLE_ACTIVE,
    CLI_ERPS_ERR_MONITOR_INVALID,
    CLI_ERPS_MAX_ERR
};

#ifdef _ERPSCLI_C_
CONST CHR1  *gaErpsCliErrString [] = {
    NULL,
    "Unknown Error\r\n",
    "ERPS Module is ShutDown \r\n",
    "ERPS Module is disabled \r\n",
    "Ring Entry is active \r\n",
    "Ring Port, R-APS Vlan CFM or MPLS(in case of VPLS-RES) entries in ring is not set\r\n",
    "Ring Entry is not present \r\n",
    "Ring Entry is already created \r\n",
    "Ring Entry is not active \r\n",
    "Cfm Entry is active \r\n",
    "Cfm Entry is in not ready state \r\n",
    "Cfm entry is in-active \r\n",
    "TC  entry is not found \r\n",
    "Ring Id in TC list should not be self id\r\n",
    "TC list cannot be configured for ring \r\n",
    "Ring cannot be deleted, when its configured in TC prop list\r\n",
    "Ring cannot be deleted, when CFM entry exists\r\n",
    "RPL or switch port configured has to be one among the ring ports\r\n",
    "Manual switch can't be configured, when force switch is configured\r\n",
    "Manual switch can't be configured, when local or R-APS SF exists\r\n",
    "This Configuration is applicable only for Sub-Rings\r\n",
    "ERPS context doesnot exist.\r\n",
    "Both the ring ports cannot be same\r\n",
    "TC entry for the sub-ring does not exist\r\n",
    "This command is only applicable for interconnected node of sub-ring\r\n",
    "The Vlan Group Manager must be ERPS\r\n",
    "The Vlan Ids entered must be valid\r\n",
    "The Vlan Group Id entered must be valid\r\n",
    "Vlan Group Id already mapped to another Ring\r\n",
    "Vlan entered is not mapped to default group id\r\n",
    "Vlan entered is already mapped to default group id\r\n",
    "RAPS VLAN is Member of another Ring in the same Context.\r\n",
    "The entered Main Ring doesnot exist\r\n",
    "The  number of sub-rings associated with the main ring has exceeded maximum limits\r\n",
    "Manual recovery method cannot be configured when the compatible version number is 2\r\n",
    "This configuration is not supported when the compatible version number is 1.\r\n",
    "This command is not supported when the compatible version number is 2 ; Please use the aps clear command.\r\n",
    "Both RPL port and RPL neighbor port cannot be enabled at the same ring node \r\n",
    "Manual switch can't be configured, when Manual switch already exists\r\n",
    "Force switch can't be configured, when Local Force  switch already exists\r\n",
    "Before setting Manual Switch/Forced Switch, Switch Command Type should be set to NONE\r\n",
    "RPL Next neighbour port can't be configured if either RPL or RPL neighbor is already configured in the same ring node.\r\n",
    "Both the ring ports of the same ring node can't be configured as RPL Next Neighbor port.\r\n ",
    "Inter connected node object can take values only among primary,secondary,none.\r\n",
    "Multiple failure object can take values only among primary,secondary,disabled.\r\n",
    "Ring port should be present either in local(1) line card or remote(2) line card.\r\n",
    "Version v1 is not configurable when ring configured without virtual channel\r\n",
    "Vlan Group Id List is not valid\r\n",
    "Ring ports cannot be configured as distributing port.\r\n",
    "This configuration will be allowed only if atleast one of the Ring ports is configured as remote port.\r\n",
    "This configuration will not be allowed if MEP info is not available for Remote port.\r\n",
    "Both ring ports of a Ring Node can't be configured as remote, make port 1 as local first.\r\n",
    "Both ring ports of a Ring Node can't be configured as remote, make port 2 as local first.\r\n",
    "Only Pseudo-wire interfaces are allowed for SubPortList configuration.\r\n",
    "SubPortList configuration is supported only for MPLS-LSP-PW service.\r\n",
    "Invalid MEG Value. MEG value should not be Zero\r\n",
    "Invalid ME value. ME value should not be Zero\r\n",
    "Invalid Kvalue. KValue configured is not within the range\r\n",
    "Ring Id should be greater than zero\r\n",
    "Number of Rings configured exceeds the maximum limit\r\n",
    "RAPS-Vlan is not mapped to vlan group\r\n",
    "Atleast one of the Vlan is not created.\r\n",
    "Clear command is not allowed on a node when Local Forced Switch/Manual Switch is not present\r\n",
    "Clear command is not allowed on RPL node when Local FS/MS is not present or WTB/WTR is not running or not in Non-Revertive mode\r\n",
    "The entered Vlan Group Id does not exist to remove the vlan\r\n",
    "The entered Vlan Id does not exist in the given Vlan Group Id\r\n",
    "WTB timer is not five seconds longer than the guard timer.\n",
    "Vlan entered is not created.\n",
     "RAPS-Vlan with same ring port already created.\n",
     "ICCL interface cannot be configured as an RPL port.\n",
     "ICCL interface cannot be configured as an RPL neighbor port.\n",
     "Force/Manual switch cannot be applied on an ICCL interface.\n",
     "MC-LAG enabled interface cannot be added as a Ring port.\n",
     "When MC-LAG is enabled in the system, non-revertive mode operation is not permitted.\n",
     "When MC-LAG is enabled in the system, hold-off timer should be zero.\n",
     "When MC-LAG is enabled in the system, configuring the sub-ring without VC is not permitted.\n",
     "MPLS Row entry is active.\n",
     "MPLS entry not ready.\n",
     "MPLS entry exists.\n",
     "Remote line card can't be configured when ring port is not present.\n",
     "Manual recovery method is not valid for Compatible version V2.\n",
     "serivce type is not MPLS-LSP-PW \n",
     "Unable to activate Ring Group entity.\n",
     "Monitor mechanism cannot be set as CFM for service type as MPLS_LSP_PW.\n",
    "\r\n"
};
#else
extern CONST CHR1  *gaErpsCliErrString [];
#endif

enum {
    ERPS_CLI_SHOW_RING_GROUP_CONFIG    = 0,
    ERPS_CLI_SHOW_RING_GROUP_STATS = 1,
    ERPS_CLI_SHOW_RING_GROUP_TIMER  = 2,
    ERPS_CLI_SHOW_RING_GROUP_ALL  = 3
};

enum {
ERPS_CLI_SUBPORTLIST_OVERRIDE, /* Enum that directs to override the exising Port 
                                * SubPortList configuration.
                                */
ERPS_CLI_SUBPORTLIST_ADD       /* Enum that directs to add the PortList
                                * information to the existing configuration.
                                */
};

#define ERPS_CLI_MAX_ARGS       15

#define ERPS_MAX_LIST_PER_LINE 10
#define ERPS_CLI_DISPLAY_LINE  36
#define ERPS_SET_CMD            1
#define ERPS_NO_CMD             2

#define CLI_ERPS_RING_MODE  "ring-"

#define ERPS_CLI_INVALID_CONTEXT   0xFFFFFFFF
#define ERPS_CLI_COMPARE_VLANS(x,y)               ((y-x) != 1) ? FALSE : TRUE
#define ERPS_CLI_MAX_MAC_STRING_SIZE 21

INT4
cli_process_erps_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);

INT4
cli_process_erps_sh_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);

INT4
ErpsCliSetSystemControl (tCliHandle CliHandle, UINT4 u4ContextId,
                         INT4 i4SystemControl,UINT1 u1ContextDelFlag);
INT4
ErpsCliSetModuleStatus (tCliHandle CliHandle, UINT4 u4ContextId, INT4 i4Status);
INT4
ErpsCliCreateRingGroup (tCliHandle CliHandle, UINT4 u4ContextId, 
                        UINT4 u4RingId);
INT4
ErpsCliDeleteRingGroup (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4RingId);

INT4
ErpsCliConfigureRingName (tCliHandle CliHandle, UINT4 u4ContextId,
                          UINT1 *pu1RingName, UINT4 u4RingId, INT4 i4Command);
INT4
ErpsCliSetNotification (tCliHandle CliHandle, UINT4 u4ContextId,
                        INT4 i4TrapStatus);
INT4
ErpsCliSetDebugs (tCliHandle CliHandle, UINT4 u4ContextId, UINT1 *pu1TraceInput);
INT4
ErpsCliActivateRingGroup (tCliHandle CliHandle, UINT4 u4ContextId,
                          UINT4 u4RingId,INT4 i4RowStatus);
INT4
ErpsCliDeActivateRingGroup (tCliHandle CliHandle, UINT4 u4ContextId,
                            UINT4 u4RingId);
INT4
ErpsCliConfigureRingPorts (tCliHandle CliHandle, UINT4 u4ContextId,
                           UINT4 u4RingId,
                           INT4 i4RingPort1, INT4 i4RingPort2, INT4 i4RingVlan);
INT4
ErpsCliConfigureRPLPort (tCliHandle CliHandle, UINT4 u4ContextId,
                         UINT4 u4RingId,
                         INT4 i4RPLPort);
INT4
ErpsCliConfigureCfmEntries (tCliHandle CliHandle,UINT4 u4ContextId,
                            UINT4 u4RingId,UINT4 u4RingMEG1,UINT4 u4RingME1,
                            UINT4 u4RingMEP1,UINT4 u4RingMEG2,
                            UINT4 u4RingME2, UINT4 u4RingMEP2);

 INT4
ErpsCliDelCfmEntries (tCliHandle CliHandle,UINT4 u4ContextId,
                      UINT4 u4RingId);

INT4 ErpsCliConfigureMplsPwEntry(tCliHandle CliHandle,UINT4 u4ContextId,
 UINT4 u4RingId, UINT4 u4RingPwId1, UINT4 u4RingPwId2);

INT4 ErpsCliDelMplsPwEntry(tCliHandle CliHandle,UINT4 u4ContextId,
 UINT4 u4RingId); 

VOID ErpsShowRunningConfigRingMplsCmd(tCliHandle CliHandle,UINT4 u4FsErpsContextId,UINT4 u4FsErpsRingId);

INT4
ErpsCliConfigureSwitchCommand (tCliHandle CliHandle, UINT4 u4ContextId,
                            UINT4 u4RingId,
                               INT4 i4SwitchCmd, INT4 i4SwitchPort);
INT4
ErpsCliConfigureOperatingMode (tCliHandle CliHandle,
                               UINT4 u4ContextId,
                            UINT4 u4RingId,
                               INT4 i4OperatingMode, UINT4 u4WTRTime);
INT4
ErpsCliConfigureRecoveryMethod (tCliHandle CliHandle,
                               UINT4 u4ContextId,
                            UINT4 u4RingId,
                               INT4 i4OperatingMode, UINT4 u4RecoveryMethod);
INT4
ErpsCliConfigureRingTimers (tCliHandle CliHandle,
                            UINT4 u4ContextId,
                            UINT4 u4RingId,
                            UINT4 u4PeriodicTmrVal,
                            UINT4 u4HoldOffTmrVal, UINT4 u4GuardTmrVal);
INT4
ErpsCliGetContextInfoForShowCmd (tCliHandle CliHandle, UINT1 *pu1ContextName,
                                 UINT4 u4CurrContextId, UINT4 *pu4ContextId);
INT4
ErpsCliShowGlobalInfo (tCliHandle CliHandle, UINT4 u4ContextId);
INT4
ErpsCliShowDebugging (tCliHandle CliHandle, UINT4 u4ContextId);
VOID
ErpsCliShowRingStatus (tCliHandle CliHandle, UINT4 u4ContextId,
                       UINT4 u4RingId, UINT4 *pu4ShowStatus);
VOID
ErpsCliShowRingConfig (tCliHandle CliHandle, UINT4 u4ContextId, UINT4
                       u4RingId, UINT4 *pu4ShowStatus);
VOID
ErpsCliShowRingStatistics (tCliHandle CliHandle, UINT4 u4ContextId,
                           UINT4 u4RingId, UINT4 *pu4ShowStatus);
VOID
ErpsCliShowRingTimers (tCliHandle CliHandle, UINT4 u4ContextId,
                       UINT4 u4RingId, UINT4 *pu4ShowStatus);
INT4
ErpsCliShowRingInfo (tCliHandle CliHandle, UINT4 u4ContextId,
                     UINT4 u4RingId, INT4 i4ShowCmdType);
VOID
ErpsCliShowAllRingInfo (tCliHandle CliHandle,
                        UINT4 u4ContextId, UINT4 u4NextRingId,
                        UINT4 *pu4ShowStatus);
INT4
ErpsCliPropagateTopologyChange (tCliHandle CliHandle, 
                                UINT4 u4ContextId,
                                UINT4 u4RingId, UINT4 *pu1TcProgList,
                                INT4 i4TcProgStatus);
INT4
ErpsCliClearRingStats (tCliHandle CliHandle, UINT4 u4ContextId,
                       UINT4 u4RingId);
INT1
ErpsGetErpsCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);
INT1
ErpsGetVcmErpsCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);
VOID
ErpsCliPrintRingTcList (tCliHandle CliHandle, INT4 i4CommaCount, 
                        UINT2 VlanId);
INT4
ErpsCliDisplayOctetToList (tCliHandle CliHandle, UINT1 *pOctet,
                           UINT4 u4OctetLen, INT4 bSeprtr, INT4 *pi4CommaCount);
INT4
ErpsCliShowTcListForRing (tCliHandle CliHandle, UINT4 u4ContextId,
                          UINT4 u4RingId);
INT4
ErpsCliConfigRingRowStatus(tCliHandle CliHandle, UINT4 u4ContextId,
                           UINT4 u4RingId, UINT1 u1RowStatus);
INT4
ErpsCliDelTopologyList (tCliHandle CliHandle, UINT4 u4ContextId,
                        UINT4 u4RingId, UINT4 *pu4TcProgList);
    INT4
ErpsCliCfgRingTopoEntry (tCliHandle CliHandle, UINT4 u4ContextId,
                         UINT4 u4RingId, UINT4 *pu4TcProgList,UINT4 i4RowStatus);

INT4
ErpsCliDelAllTopologyList (tCliHandle CliHandle, UINT4 u4ContextId,
                           UINT4 u4RingId);
INT4
ErpsCliConfigureRingversion (tCliHandle CliHandle, UINT4 u4ContextId,
                                    UINT4 u4RingId, UINT4 u4Ringversion);
INT4
ErpsCliConfigureInterConnNode (tCliHandle CliHandle, UINT4 u4ContextId,
                               UINT4 u4RingId, UINT4 u4InterConnNode);
INT4
ErpsCliConfigureMultipleFailure (tCliHandle CliHandle, UINT4 u4ContextId,
                                 UINT4 u4RingId, UINT4 u4MultipleFailure);
INT4
ErpsCliConfigureRingRPLNeighbour (tCliHandle CliHandle, UINT4 u4ContextId,
                UINT4  u4RingId,UINT4 u4RplNeighbourPort );
INT4
ErpsCliConfigureRingRPLNextNeighbour (tCliHandle CliHandle, UINT4 u4ContextId,
                                      UINT4 u4RingId, UINT4 u4RplNextNeighbourPort);
INT4
ErpsCliClearSwitchMethod (tCliHandle CliHandle, UINT4 u4ContextId,
                UINT4  u4RingId,INT4 i4ConfigChangeMode);
INT4
ErpsCliConfigWTBTime (tCliHandle CliHandle, UINT4 u4ContextId,
                UINT4  u4RingId,UINT4 u4WTBtime);
INT4
ErpsCliSetPropClearFS (tCliHandle CliHandle, UINT4 u4ContextId, INT4 i4Status);

INT4
ErpsCliSetMonitor PROTO ((tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4RingId,
                     INT4 i4Monitor));

/* ERPS Show Running Config Commands */
INT4 ErpsShowRunningConfig(tCliHandle CliHandle);
INT4 ErpsShowRunningConfigCxtCmds(tCliHandle CliHandle);
INT4 ErpsShowRunningConfigRingCmds(tCliHandle CliHandle, UINT4 u4FsErpsContextId);
VOID ErpsShowRunningConfigRingCfmCmd(tCliHandle CliHandle,UINT4 u4FsErpsContextId,UINT4 u4FsErpsRingId);
VOID ErpsShowRunningConfigRingTmrCmd(tCliHandle CliHandle,UINT4 u4FsErpsContextId,UINT4 u4FsErpsRingId);
INT4
ErpsShowRunningConfigTcTable (tCliHandle CliHandle, UINT4 u4ContextId,
                              UINT4 u4RingId);

INT4 ErpsCliConfPortBlockOnVcRecovery (tCliHandle CliHandle,
                                       UINT4 u4ContextId,
                                       UINT4 u4RingId,
                                       UINT1 u1PortBlockStatus);
INT4
ErpsCliConfigureMainRingId (tCliHandle CliHandle,
                            UINT4 u4ContextId,
                            UINT4 u4RingId, UINT4 u4MainRingId);
INT4
ErpsCliConfigureVcPeriodicTimer(tCliHandle CliHandle,
                                UINT4 u4ContextId,
                                UINT4 u4RingId,
                                UINT4 u4VcPeriodicTime);


INT4 ErpsCliCfgVlanGroupManager (tCliHandle CliHandle, UINT4 u4ContextId, 
                                 INT4 i4VlanGroupManager);
INT4 ErpsCliMapVlanGroupId (tCliHandle CliHandle, UINT4 u4ContextId,
                            UINT4 u4RingId, INT4 i4GroupId);
INT4 ErpsCliCfgProtectionType (tCliHandle CliHandle, UINT4 u4ContextId,
                               UINT4 u4RingId, INT4 i4ProtectionType);
INT4 ErpsCliMapMacId (tCliHandle CliHandle, UINT4 u4ContextId, 
                      UINT4 u4RingId, INT4 i4MacId);
INT4 ErpsCliMapVlanGroupList (tCliHandle CliHandle,UINT4 u4ContextId,
                         UINT4 u4RingId,UINT1 *pu1VlanList);
INT4 ErpsCliAddVlanList (tCliHandle CliHandle,UINT4 u4ContextId,
                         INT4  i4VlanGroupId,UINT1 *pu1VlanList);
INT4 ErpsCliRemVlanList (tCliHandle CliHandle,UINT4 u4ContextId,
                         INT4  i4VlanGroupId,UINT1 *pu1VlanList);
INT4 ErpsCliModVlanList (tCliHandle CliHandle,UINT4 u4ContextId,
                         INT4  i4VlanGroupId,UINT1 *pu1VlanList);
INT4 ErpsCliRemoveVlanGroup (tCliHandle CliHandle,UINT4 u4ContextId,
                             INT4  i4VlanGroupId);
INT4 ErpsCliShowVlanGroupInfo (tCliHandle CliHandle, UINT4 u4ContextId,
                               INT4 i4VlanGroupId,UINT2 u2SrcFlag);
INT4 ErpsCliDisplayVlanList (tCliHandle CliHandle,UINT1 *pu1VlanList,
                             UINT2 u2SrcFlag);
INT4
ErpsCliConfigureRingRAPSWithoutVC(tCliHandle CliHandle, UINT4 u4ContextId,
                                  UINT4 u4RingId, UINT4 u4RAPSWithoutVC);
INT4   
ErpsCliConfigureRingPortPresence(tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4RingId, UINT1 u1IsPort1Present, UINT1 u1RingPort);      

INT4
ErpsCliConfigureDistributePort (tCliHandle CliHandle, UINT4 u4ContextId,
                                UINT4 u4RingId, UINT4 u4DistributePort);

INT4
ErpsCliConfigureKValue PROTO ((tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4RingId,
                                                        UINT1 *KValue));

INT4
ErpsCliSetFailureofProtocol PROTO ((tCliHandle CliHandle,UINT4 u4ContextId, UINT4 u4RingId,
                             INT4 i4Status));
INT4
ErpsCliSetServiceType (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4RingId,
                       INT4  i4ServiceType);
INT4 ErpsCliSetDebugLevel (tCliHandle CliHandle, INT4 i4CliDebugLevel); 
INT4
ErpsCliReSetRingWrkingSubPorts (tCliHandle CliHandle, UINT4 u4ContextId,
                                UINT4 u4RingId);
INT4
ErpsCliSetRingWrkingSubPorts (tCliHandle CliHandle, UINT4 u4ContextId,
                              UINT4 u4RingId, tPortListExt *pPortList1,
                              tPortListExt *pPortList2, UINT4 u4ConfigType);
VOID
ErpsCliShowSubPortList (tCliHandle CliHandle, UINT1 *pu1OctetList);
VOID
ErpsCliHandleFailureSubPortList (UINT1* Port1List,
                                 UINT1* Port2List);
#ifdef ERPS_TEST_WANTED
VOID ErpsTestExecUT (UINT4 u4UtId);
#endif

#endif /* __ERPSCLI_H__ */
