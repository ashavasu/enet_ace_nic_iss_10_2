/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdtcpcli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 TcpConnState[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetTcpConnState(u4TcpConnLocalAddress , i4TcpConnLocalPort , u4TcpConnRemAddress , i4TcpConnRemPort ,i4SetValTcpConnState)	\
	nmhSetCmn(TcpConnState, 10, TcpConnStateSet, NULL, NULL, 0, 0, 4, "%p %i %p %i %i", u4TcpConnLocalAddress , i4TcpConnLocalPort , u4TcpConnRemAddress , i4TcpConnRemPort ,i4SetValTcpConnState)

#endif
