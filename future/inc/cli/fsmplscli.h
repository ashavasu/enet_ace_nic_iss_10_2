/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmplscli.h,v 1.18 2015/09/15 07:03:08 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsAdminStatus[11];
extern UINT4 FsMplsQosPolicy[11];
extern UINT4 FsMplsFmDebugLevel[11];
extern UINT4 FsMplsTeDebugLevel[11];
extern UINT4 FsMplsLsrLabelAllocationMethod[11];
extern UINT4 FsMplsDiffServElspPreConfExpPhbMapIndex[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsAdminStatus(i4SetValFsMplsAdminStatus) \
 nmhSetCmnWithLock(FsMplsAdminStatus, 11, FsMplsAdminStatusSet, CmnLock, CmnUnLock, 0, 0, 0, "%i", i4SetValFsMplsAdminStatus)
#define nmhSetFsMplsQosPolicy(i4SetValFsMplsQosPolicy) \
 nmhSetCmnWithLock(FsMplsQosPolicy, 11, FsMplsQosPolicySet, CmnLock, CmnUnLock, 0, 0, 0, "%i", i4SetValFsMplsQosPolicy)
#define nmhSetFsMplsFmDebugLevel(i4SetValFsMplsFmDebugLevel) \
 nmhSetCmnWithLock(FsMplsFmDebugLevel, 11, FsMplsFmDebugLevelSet, CmnLock, CmnUnLock, 0, 0, 0, "%i", i4SetValFsMplsFmDebugLevel)
#define nmhSetFsMplsTeDebugLevel(u4SetValFsMplsTeDebugLevel) \
 nmhSetCmnWithLock(FsMplsTeDebugLevel, 11, FsMplsTeDebugLevelSet, CmnLock, CmnUnLock, 0, 0, 0, "%u", u4SetValFsMplsTeDebugLevel)
#define nmhSetFsMplsLsrLabelAllocationMethod(i4SetValFsMplsLsrLabelAllocationMethod) \
 nmhSetCmnWithLock(FsMplsLsrLabelAllocationMethod, 11, FsMplsLsrLabelAllocationMethodSet, LdpLock, LdpUnLock, 0, 0, 0, "%i", i4SetValFsMplsLsrLabelAllocationMethod)
#define nmhSetFsMplsDiffServElspPreConfExpPhbMapIndex(i4SetValFsMplsDiffServElspPreConfExpPhbMapIndex) \
 nmhSetCmnWithLock(FsMplsDiffServElspPreConfExpPhbMapIndex, 11, FsMplsDiffServElspPreConfExpPhbMapIndexSet, CmnLock, CmnUnLock, 0, 0, 0, "%i", i4SetValFsMplsDiffServElspPreConfExpPhbMapIndex)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsLdpEntityPHPRequestMethod[13];
extern UINT4 FsMplsLdpEntityTransAddrTlvEnable[13];
extern UINT4 FsMplsLdpEntityTransportAddress[13];
extern UINT4 FsMplsLdpEntityLdpOverRsvpEnable[13];
extern UINT4 FsMplsLdpEntityOutTunnelIndex[13];
extern UINT4 FsMplsLdpEntityOutTunnelInstance[13];
extern UINT4 FsMplsLdpEntityOutTunnelIngressLSRId[13];
extern UINT4 FsMplsLdpEntityOutTunnelEgressLSRId[13];
extern UINT4 FsMplsLdpEntityInTunnelIndex[13];
extern UINT4 FsMplsLdpEntityInTunnelInstance[13];
extern UINT4 FsMplsLdpEntityInTunnelIngressLSRId[13];
extern UINT4 FsMplsLdpEntityInTunnelEgressLSRId[13];
extern UINT4 FsMplsLdpEntityIpv6TransportAddress[13];
extern UINT4 FsMplsLdpEntityIpv6TransAddrTlvEnable[13];
extern UINT4 FsMplsLdpEntityIpv6TransportAddrKind[13];
extern UINT4 FsMplsLdpEntityBfdStatus[13];


#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsLdpEntityPHPRequestMethod(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValFsMplsLdpEntityPHPRequestMethod) \
 nmhSetCmnWithLock(FsMplsLdpEntityPHPRequestMethod, 13, FsMplsLdpEntityPHPRequestMethodSet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %i", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValFsMplsLdpEntityPHPRequestMethod)
#define nmhSetFsMplsLdpEntityTransAddrTlvEnable(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValFsMplsLdpEntityTransAddrTlvEnable) \
 nmhSetCmnWithLock(FsMplsLdpEntityTransAddrTlvEnable, 13, FsMplsLdpEntityTransAddrTlvEnableSet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %i", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValFsMplsLdpEntityTransAddrTlvEnable)
#define nmhSetFsMplsLdpEntityTransportAddress(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,u4SetValFsMplsLdpEntityTransportAddress) \
 nmhSetCmnWithLock(FsMplsLdpEntityTransportAddress, 13, FsMplsLdpEntityTransportAddressSet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %p", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,u4SetValFsMplsLdpEntityTransportAddress)
#define nmhSetFsMplsLdpEntityLdpOverRsvpEnable(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValFsMplsLdpEntityLdpOverRsvpEnable) \
 nmhSetCmnWithLock(FsMplsLdpEntityLdpOverRsvpEnable, 13, FsMplsLdpEntityLdpOverRsvpEnableSet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %i", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValFsMplsLdpEntityLdpOverRsvpEnable)
#define nmhSetFsMplsLdpEntityOutTunnelIndex(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,u4SetValFsMplsLdpEntityOutTunnelIndex) \
 nmhSetCmnWithLock(FsMplsLdpEntityOutTunnelIndex, 13, FsMplsLdpEntityOutTunnelIndexSet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %u", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,u4SetValFsMplsLdpEntityOutTunnelIndex)
#define nmhSetFsMplsLdpEntityOutTunnelInstance(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,u4SetValFsMplsLdpEntityOutTunnelInstance) \
 nmhSetCmnWithLock(FsMplsLdpEntityOutTunnelInstance, 13, FsMplsLdpEntityOutTunnelInstanceSet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %u", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,u4SetValFsMplsLdpEntityOutTunnelInstance)
#define nmhSetFsMplsLdpEntityOutTunnelIngressLSRId(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,u4SetValFsMplsLdpEntityOutTunnelIngressLSRId) \
 nmhSetCmnWithLock(FsMplsLdpEntityOutTunnelIngressLSRId, 13, FsMplsLdpEntityOutTunnelIngressLSRIdSet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %u", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,u4SetValFsMplsLdpEntityOutTunnelIngressLSRId)
#define nmhSetFsMplsLdpEntityOutTunnelEgressLSRId(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,u4SetValFsMplsLdpEntityOutTunnelEgressLSRId) \
 nmhSetCmnWithLock(FsMplsLdpEntityOutTunnelEgressLSRId, 13, FsMplsLdpEntityOutTunnelEgressLSRIdSet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %u", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,u4SetValFsMplsLdpEntityOutTunnelEgressLSRId)
#define nmhSetFsMplsLdpEntityInTunnelIndex(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,u4SetValFsMplsLdpEntityInTunnelIndex) \
 nmhSetCmnWithLock(FsMplsLdpEntityInTunnelIndex, 13, FsMplsLdpEntityInTunnelIndexSet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %u", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,u4SetValFsMplsLdpEntityInTunnelIndex)
#define nmhSetFsMplsLdpEntityInTunnelInstance(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,u4SetValFsMplsLdpEntityInTunnelInstance) \
 nmhSetCmnWithLock(FsMplsLdpEntityInTunnelInstance, 13, FsMplsLdpEntityInTunnelInstanceSet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %u", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,u4SetValFsMplsLdpEntityInTunnelInstance)
#define nmhSetFsMplsLdpEntityInTunnelIngressLSRId(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,u4SetValFsMplsLdpEntityInTunnelIngressLSRId) \
 nmhSetCmnWithLock(FsMplsLdpEntityInTunnelIngressLSRId, 13, FsMplsLdpEntityInTunnelIngressLSRIdSet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %u", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,u4SetValFsMplsLdpEntityInTunnelIngressLSRId)
#define nmhSetFsMplsLdpEntityInTunnelEgressLSRId(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,u4SetValFsMplsLdpEntityInTunnelEgressLSRId) \
 nmhSetCmnWithLock(FsMplsLdpEntityInTunnelEgressLSRId, 13, FsMplsLdpEntityInTunnelEgressLSRIdSet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %u", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,u4SetValFsMplsLdpEntityInTunnelEgressLSRId)
#define nmhSetFsMplsLdpEntityIpv6TransportAddress(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,pSetValFsMplsLdpEntityIpv6TransportAddress)   \
    nmhSetCmnWithLock(FsMplsLdpEntityIpv6TransportAddress, 13, FsMplsLdpEntityIpv6TransportAddressSet, NULL, NULL, 0, 0, 2, "%s %u %s", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,pSetValFsMplsLdpEntityIpv6TransportAddress)
#define nmhSetFsMplsLdpEntityIpv6TransAddrTlvEnable(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValFsMplsLdpEntityIpv6TransAddrTlvEnable)  \
    nmhSetCmnWithLock(FsMplsLdpEntityIpv6TransAddrTlvEnable, 13, FsMplsLdpEntityIpv6TransAddrTlvEnableSet, NULL, NULL, 0, 0, 2, "%s %u %i", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValFsMplsLdpEntityIpv6TransAddrTlvEnable)
#define nmhSetFsMplsLdpEntityIpv6TransportAddrKind(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValFsMplsLdpEntityIpv6TransportAddrKind)    \
    nmhSetCmnWithLock(FsMplsLdpEntityIpv6TransportAddrKind, 13, FsMplsLdpEntityIpv6TransportAddrKindSet, NULL, NULL, 0, 0, 2, "%s %u %i", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValFsMplsLdpEntityIpv6TransportAddrKind)
#define nmhSetFsMplsLdpEntityBfdStatus(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValFsMplsLdpEntityBfdStatus) \
 nmhSetCmnWithLock(FsMplsLdpEntityBfdStatus, 13, FsMplsLdpEntityBfdStatusSet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %i", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValFsMplsLdpEntityBfdStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsLdpLsrId[11];
extern UINT4 FsMplsLdpForceOption[11];
extern UINT4 FsMplsRsvpTeGrMaxWaitTime[11];
extern UINT4 FsMplsLdpGrMaxWaitTime[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsLdpLsrId(pSetValFsMplsLdpLsrId) \
 nmhSetCmnWithLock(FsMplsLdpLsrId, 11, FsMplsLdpLsrIdSet, LdpLock, LdpUnLock, 0, 0, 0, "%s", pSetValFsMplsLdpLsrId)
#define nmhSetFsMplsLdpForceOption(i4SetValFsMplsLdpForceOption) \
 nmhSetCmnWithLock(FsMplsLdpForceOption, 11, FsMplsLdpForceOptionSet, LdpLock, LdpUnLock, 0, 0, 0, "%i", i4SetValFsMplsLdpForceOption)
#define nmhSetFsMplsRsvpTeGrMaxWaitTime(i4SetValFsMplsRsvpTeGrMaxWaitTime) \
 nmhSetCmnWithLock(FsMplsRsvpTeGrMaxWaitTime, 11, FsMplsRsvpTeGrMaxWaitTimeSet, CmnLock, CmnUnLock, 0, 0, 0, "%i", i4SetValFsMplsRsvpTeGrMaxWaitTime)
#define nmhSetFsMplsLdpGrMaxWaitTime(i4SetValFsMplsLdpGrMaxWaitTime) \
 nmhSetCmnWithLock(FsMplsLdpGrMaxWaitTime, 11, FsMplsLdpGrMaxWaitTimeSet, CmnLock, CmnUnLock, 0, 0, 0, "%i", i4SetValFsMplsLdpGrMaxWaitTime)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsCrlspDebugLevel[11];
extern UINT4 FsMplsCrlspDumpType[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsCrlspDebugLevel(i4SetValFsMplsCrlspDebugLevel) \
 nmhSetCmnWithLock(FsMplsCrlspDebugLevel, 11, FsMplsCrlspDebugLevelSet, LdpLock, LdpUnLock, 0, 0, 0, "%i", i4SetValFsMplsCrlspDebugLevel)
#define nmhSetFsMplsCrlspDumpType(i4SetValFsMplsCrlspDumpType) \
 nmhSetCmnWithLock(FsMplsCrlspDumpType, 11, FsMplsCrlspDumpTypeSet, LdpLock, LdpUnLock, 0, 0, 0, "%i", i4SetValFsMplsCrlspDumpType)
#endif

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsCrlspTnlIndex[13];
extern UINT4 FsMplsCrlspTnlRowStatus[13];
extern UINT4 FsMplsCrlspTnlStorageType[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsCrlspTnlRowStatus(u4FsMplsCrlspTnlIndex ,i4SetValFsMplsCrlspTnlRowStatus) \
 nmhSetCmnWithLock(FsMplsCrlspTnlRowStatus, 13, FsMplsCrlspTnlRowStatusSet, LdpLock, LdpUnLock, 0, 1, 1, "%u %i", u4FsMplsCrlspTnlIndex ,i4SetValFsMplsCrlspTnlRowStatus)
#define nmhSetFsMplsCrlspTnlStorageType(u4FsMplsCrlspTnlIndex ,i4SetValFsMplsCrlspTnlStorageType) \
 nmhSetCmnWithLock(FsMplsCrlspTnlStorageType, 13, FsMplsCrlspTnlStorageTypeSet, LdpLock, LdpUnLock, 0, 0, 1, "%u %i", u4FsMplsCrlspTnlIndex ,i4SetValFsMplsCrlspTnlStorageType)
#endif

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsCrlspDumpDirection[11];
extern UINT4 FsMplsCrlspPersistance[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsCrlspDumpDirection(i4SetValFsMplsCrlspDumpDirection) \
 nmhSetCmnWithLock(FsMplsCrlspDumpDirection, 11, FsMplsCrlspDumpDirectionSet, LdpLock, LdpUnLock, 0, 0, 0, "%i", i4SetValFsMplsCrlspDumpDirection)
#define nmhSetFsMplsCrlspPersistance(i4SetValFsMplsCrlspPersistance) \
 nmhSetCmnWithLock(FsMplsCrlspPersistance, 11, FsMplsCrlspPersistanceSet, LdpLock, LdpUnLock, 0, 0, 0, "%i", i4SetValFsMplsCrlspPersistance)
#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsTunnelRSVPResTokenBucketRate[13];
extern UINT4 FsMplsTunnelRSVPResTokenBucketSize[13];
extern UINT4 FsMplsTunnelRSVPResPeakDataRate[13];
extern UINT4 FsMplsTunnelRSVPResMinimumPolicedUnit[13];
extern UINT4 FsMplsTunnelRSVPResMaximumPacketSize[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsTunnelRSVPResTokenBucketRate(u4MplsTunnelResourceIndex ,u4SetValFsMplsTunnelRSVPResTokenBucketRate) \
 nmhSetCmnWithLock(FsMplsTunnelRSVPResTokenBucketRate, 13, FsMplsTunnelRSVPResTokenBucketRateSet, NULL, NULL, 0, 0, 1, "%u %u", u4MplsTunnelResourceIndex ,u4SetValFsMplsTunnelRSVPResTokenBucketRate)
#define nmhSetFsMplsTunnelRSVPResTokenBucketSize(u4MplsTunnelResourceIndex ,u4SetValFsMplsTunnelRSVPResTokenBucketSize) \
 nmhSetCmnWithLock(FsMplsTunnelRSVPResTokenBucketSize, 13, FsMplsTunnelRSVPResTokenBucketSizeSet, NULL, NULL, 0, 0, 1, "%u %u", u4MplsTunnelResourceIndex ,u4SetValFsMplsTunnelRSVPResTokenBucketSize)
#define nmhSetFsMplsTunnelRSVPResPeakDataRate(u4MplsTunnelResourceIndex ,u4SetValFsMplsTunnelRSVPResPeakDataRate) \
 nmhSetCmnWithLock(FsMplsTunnelRSVPResPeakDataRate, 13, FsMplsTunnelRSVPResPeakDataRateSet, NULL, NULL, 0, 0, 1, "%u %u", u4MplsTunnelResourceIndex ,u4SetValFsMplsTunnelRSVPResPeakDataRate)
#define nmhSetFsMplsTunnelRSVPResMinimumPolicedUnit(u4MplsTunnelResourceIndex ,i4SetValFsMplsTunnelRSVPResMinimumPolicedUnit) \
 nmhSetCmnWithLock(FsMplsTunnelRSVPResMinimumPolicedUnit, 13, FsMplsTunnelRSVPResMinimumPolicedUnitSet, NULL, NULL, 0, 0, 1, "%u %i", u4MplsTunnelResourceIndex ,i4SetValFsMplsTunnelRSVPResMinimumPolicedUnit)
#define nmhSetFsMplsTunnelRSVPResMaximumPacketSize(u4MplsTunnelResourceIndex ,i4SetValFsMplsTunnelRSVPResMaximumPacketSize) \
 nmhSetCmnWithLock(FsMplsTunnelRSVPResMaximumPacketSize, 13, FsMplsTunnelRSVPResMaximumPacketSizeSet, NULL, NULL, 0, 0, 1, "%u %i", u4MplsTunnelResourceIndex ,i4SetValFsMplsTunnelRSVPResMaximumPacketSize)
#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsTunnelCRLDPResPeakDataRate[13];
extern UINT4 FsMplsTunnelCRLDPResCommittedDataRate[13];
extern UINT4 FsMplsTunnelCRLDPResPeakBurstSize[13];
extern UINT4 FsMplsTunnelCRLDPResCommittedBurstSize[13];
extern UINT4 FsMplsTunnelCRLDPResExcessBurstSize[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsTunnelCRLDPResPeakDataRate(u4MplsTunnelResourceIndex ,u4SetValFsMplsTunnelCRLDPResPeakDataRate) \
 nmhSetCmnWithLock(FsMplsTunnelCRLDPResPeakDataRate, 13, FsMplsTunnelCRLDPResPeakDataRateSet, NULL, NULL, 0, 0, 1, "%u %u", u4MplsTunnelResourceIndex ,u4SetValFsMplsTunnelCRLDPResPeakDataRate)
#define nmhSetFsMplsTunnelCRLDPResCommittedDataRate(u4MplsTunnelResourceIndex ,u4SetValFsMplsTunnelCRLDPResCommittedDataRate) \
 nmhSetCmnWithLock(FsMplsTunnelCRLDPResCommittedDataRate, 13, FsMplsTunnelCRLDPResCommittedDataRateSet, NULL, NULL, 0, 0, 1, "%u %u", u4MplsTunnelResourceIndex ,u4SetValFsMplsTunnelCRLDPResCommittedDataRate)
#define nmhSetFsMplsTunnelCRLDPResPeakBurstSize(u4MplsTunnelResourceIndex ,u4SetValFsMplsTunnelCRLDPResPeakBurstSize) \
 nmhSetCmnWithLock(FsMplsTunnelCRLDPResPeakBurstSize, 13, FsMplsTunnelCRLDPResPeakBurstSizeSet, NULL, NULL, 0, 0, 1, "%u %u", u4MplsTunnelResourceIndex ,u4SetValFsMplsTunnelCRLDPResPeakBurstSize)
#define nmhSetFsMplsTunnelCRLDPResCommittedBurstSize(u4MplsTunnelResourceIndex ,u4SetValFsMplsTunnelCRLDPResCommittedBurstSize) \
 nmhSetCmnWithLock(FsMplsTunnelCRLDPResCommittedBurstSize, 13, FsMplsTunnelCRLDPResCommittedBurstSizeSet, NULL, NULL, 0, 0, 1, "%u %u", u4MplsTunnelResourceIndex ,u4SetValFsMplsTunnelCRLDPResCommittedBurstSize)
#define nmhSetFsMplsTunnelCRLDPResExcessBurstSize(u4MplsTunnelResourceIndex ,u4SetValFsMplsTunnelCRLDPResExcessBurstSize) \
 nmhSetCmnWithLock(FsMplsTunnelCRLDPResExcessBurstSize, 13, FsMplsTunnelCRLDPResExcessBurstSizeSet, NULL, NULL, 0, 0, 1, "%u %u", u4MplsTunnelResourceIndex ,u4SetValFsMplsTunnelCRLDPResExcessBurstSize)
#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsDiffServElspMapIndex[13];
extern UINT4 FsMplsDiffServElspMapExpIndex[13];
extern UINT4 FsMplsDiffServElspMapPhbDscp[13];
extern UINT4 FsMplsDiffServElspMapStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsDiffServElspMapPhbDscp(i4FsMplsDiffServElspMapIndex , i4FsMplsDiffServElspMapExpIndex ,i4SetValFsMplsDiffServElspMapPhbDscp) \
 nmhSetCmnWithLock(FsMplsDiffServElspMapPhbDscp, 13, FsMplsDiffServElspMapPhbDscpSet, CmnLock, CmnUnLock, 0, 0, 2, "%i %i %i", i4FsMplsDiffServElspMapIndex , i4FsMplsDiffServElspMapExpIndex ,i4SetValFsMplsDiffServElspMapPhbDscp)
#define nmhSetFsMplsDiffServElspMapStatus(i4FsMplsDiffServElspMapIndex , i4FsMplsDiffServElspMapExpIndex ,i4SetValFsMplsDiffServElspMapStatus) \
 nmhSetCmnWithLock(FsMplsDiffServElspMapStatus, 13, FsMplsDiffServElspMapStatusSet, CmnLock, CmnUnLock, 0, 1, 2, "%i %i %i", i4FsMplsDiffServElspMapIndex , i4FsMplsDiffServElspMapExpIndex ,i4SetValFsMplsDiffServElspMapStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsDiffServParamsIndex[13];
extern UINT4 FsMplsDiffServParamsServiceType[13];
extern UINT4 FsMplsDiffServParamsLlspPscDscp[13];
extern UINT4 FsMplsDiffServParamsElspType[13];
extern UINT4 FsMplsDiffServParamsElspSigExpPhbMapIndex[13];
extern UINT4 FsMplsDiffServParamsStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsDiffServParamsServiceType(i4FsMplsDiffServParamsIndex ,i4SetValFsMplsDiffServParamsServiceType) \
 nmhSetCmnWithLock(FsMplsDiffServParamsServiceType, 13, FsMplsDiffServParamsServiceTypeSet, CmnLock, CmnUnLock, 0, 0, 1, "%i %i", i4FsMplsDiffServParamsIndex ,i4SetValFsMplsDiffServParamsServiceType)
#define nmhSetFsMplsDiffServParamsLlspPscDscp(i4FsMplsDiffServParamsIndex ,i4SetValFsMplsDiffServParamsLlspPscDscp) \
 nmhSetCmnWithLock(FsMplsDiffServParamsLlspPscDscp, 13, FsMplsDiffServParamsLlspPscDscpSet, CmnLock, CmnUnLock, 0, 0, 1, "%i %i", i4FsMplsDiffServParamsIndex ,i4SetValFsMplsDiffServParamsLlspPscDscp)
#define nmhSetFsMplsDiffServParamsElspType(i4FsMplsDiffServParamsIndex ,i4SetValFsMplsDiffServParamsElspType) \
 nmhSetCmnWithLock(FsMplsDiffServParamsElspType, 13, FsMplsDiffServParamsElspTypeSet, CmnLock, CmnUnLock, 0, 0, 1, "%i %i", i4FsMplsDiffServParamsIndex ,i4SetValFsMplsDiffServParamsElspType)
#define nmhSetFsMplsDiffServParamsElspSigExpPhbMapIndex(i4FsMplsDiffServParamsIndex ,i4SetValFsMplsDiffServParamsElspSigExpPhbMapIndex) \
 nmhSetCmnWithLock(FsMplsDiffServParamsElspSigExpPhbMapIndex, 13, FsMplsDiffServParamsElspSigExpPhbMapIndexSet, CmnLock, CmnUnLock, 0, 0, 1, "%i %i", i4FsMplsDiffServParamsIndex ,i4SetValFsMplsDiffServParamsElspSigExpPhbMapIndex)
#define nmhSetFsMplsDiffServParamsStatus(i4FsMplsDiffServParamsIndex ,i4SetValFsMplsDiffServParamsStatus) \
 nmhSetCmnWithLock(FsMplsDiffServParamsStatus, 13, FsMplsDiffServParamsStatusSet, CmnLock, CmnUnLock, 0, 1, 1, "%i %i", i4FsMplsDiffServParamsIndex ,i4SetValFsMplsDiffServParamsStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsDiffServClassType[13];
extern UINT4 FsMplsDiffServServiceType[13];
extern UINT4 FsMplsDiffServLlspPsc[13];
extern UINT4 FsMplsDiffServElspType[13];
extern UINT4 FsMplsDiffServElspListIndex[13];
extern UINT4 FsMplsDiffServRowStatus[13];
extern UINT4 FsMplsDiffServStorageType[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsDiffServClassType(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsDiffServClassType) \
 nmhSetCmnWithLock(FsMplsDiffServClassType, 13, FsMplsDiffServClassTypeSet, CmnLock, CmnUnLock, 0, 0, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsDiffServClassType)
#define nmhSetFsMplsDiffServServiceType(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsDiffServServiceType) \
 nmhSetCmnWithLock(FsMplsDiffServServiceType, 13, FsMplsDiffServServiceTypeSet, CmnLock, CmnUnLock, 0, 0, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsDiffServServiceType)
#define nmhSetFsMplsDiffServLlspPsc(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsDiffServLlspPsc) \
 nmhSetCmnWithLock(FsMplsDiffServLlspPsc, 13, FsMplsDiffServLlspPscSet, CmnLock, CmnUnLock, 0, 0, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsDiffServLlspPsc)
#define nmhSetFsMplsDiffServElspType(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsDiffServElspType) \
 nmhSetCmnWithLock(FsMplsDiffServElspType, 13, FsMplsDiffServElspTypeSet, CmnLock, CmnUnLock, 0, 0, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsDiffServElspType)
#define nmhSetFsMplsDiffServElspListIndex(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsDiffServElspListIndex) \
 nmhSetCmnWithLock(FsMplsDiffServElspListIndex, 13, FsMplsDiffServElspListIndexSet, CmnLock, CmnUnLock, 0, 0, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsDiffServElspListIndex)
#define nmhSetFsMplsDiffServRowStatus(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsDiffServRowStatus) \
 nmhSetCmnWithLock(FsMplsDiffServRowStatus, 13, FsMplsDiffServRowStatusSet, CmnLock, CmnUnLock, 0, 1, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsDiffServRowStatus)
#define nmhSetFsMplsDiffServStorageType(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsDiffServStorageType) \
 nmhSetCmnWithLock(FsMplsDiffServStorageType, 13, FsMplsDiffServStorageTypeSet, CmnLock, CmnUnLock, 0, 0, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsDiffServStorageType)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsDiffServElspInfoListIndex[13];
extern UINT4 FsMplsDiffServElspInfoIndex[13];
extern UINT4 FsMplsDiffServElspInfoPHB[13];
extern UINT4 FsMplsDiffServElspInfoResourcePointer[13];
extern UINT4 FsMplsDiffServElspInfoRowStatus[13];
extern UINT4 FsMplsDiffServElspInfoStorageType[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsDiffServElspInfoPHB(i4FsMplsDiffServElspInfoListIndex , i4FsMplsDiffServElspInfoIndex ,i4SetValFsMplsDiffServElspInfoPHB) \
 nmhSetCmnWithLock(FsMplsDiffServElspInfoPHB, 13, FsMplsDiffServElspInfoPHBSet, CmnLock, CmnUnLock, 0, 0, 2, "%i %i %i", i4FsMplsDiffServElspInfoListIndex , i4FsMplsDiffServElspInfoIndex ,i4SetValFsMplsDiffServElspInfoPHB)
#define nmhSetFsMplsDiffServElspInfoResourcePointer(i4FsMplsDiffServElspInfoListIndex , i4FsMplsDiffServElspInfoIndex ,pSetValFsMplsDiffServElspInfoResourcePointer) \
 nmhSetCmnWithLock(FsMplsDiffServElspInfoResourcePointer, 13, FsMplsDiffServElspInfoResourcePointerSet, CmnLock, CmnUnLock, 0, 0, 2, "%i %i %o", i4FsMplsDiffServElspInfoListIndex , i4FsMplsDiffServElspInfoIndex ,pSetValFsMplsDiffServElspInfoResourcePointer)
#define nmhSetFsMplsDiffServElspInfoRowStatus(i4FsMplsDiffServElspInfoListIndex , i4FsMplsDiffServElspInfoIndex ,i4SetValFsMplsDiffServElspInfoRowStatus) \
 nmhSetCmnWithLock(FsMplsDiffServElspInfoRowStatus, 13, FsMplsDiffServElspInfoRowStatusSet, CmnLock, CmnUnLock, 0, 1, 2, "%i %i %i", i4FsMplsDiffServElspInfoListIndex , i4FsMplsDiffServElspInfoIndex ,i4SetValFsMplsDiffServElspInfoRowStatus)
#define nmhSetFsMplsDiffServElspInfoStorageType(i4FsMplsDiffServElspInfoListIndex , i4FsMplsDiffServElspInfoIndex ,i4SetValFsMplsDiffServElspInfoStorageType) \
 nmhSetCmnWithLock(FsMplsDiffServElspInfoStorageType, 13, FsMplsDiffServElspInfoStorageTypeSet, CmnLock, CmnUnLock, 0, 0, 2, "%i %i %i", i4FsMplsDiffServElspInfoListIndex , i4FsMplsDiffServElspInfoIndex ,i4SetValFsMplsDiffServElspInfoStorageType)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsTnlModel[11];
extern UINT4 FsMplsRsrcMgmtType[11];
extern UINT4 FsMplsTTLVal[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsTnlModel(i4SetValFsMplsTnlModel) \
 nmhSetCmnWithLock(FsMplsTnlModel, 11, FsMplsTnlModelSet, CmnLock, CmnUnLock, 0, 0, 0, "%i", i4SetValFsMplsTnlModel)
#define nmhSetFsMplsRsrcMgmtType(i4SetValFsMplsRsrcMgmtType) \
 nmhSetCmnWithLock(FsMplsRsrcMgmtType, 11, FsMplsRsrcMgmtTypeSet, CmnLock, CmnUnLock, 0, 0, 0, "%i", i4SetValFsMplsRsrcMgmtType)
#define nmhSetFsMplsTTLVal(i4SetValFsMplsTTLVal) \
 nmhSetCmnWithLock(FsMplsTTLVal, 11, FsMplsTTLValSet, CmnLock, CmnUnLock, 0, 0, 0, "%i", i4SetValFsMplsTTLVal)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsDsTeStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsDsTeStatus(i4SetValFsMplsDsTeStatus) \
 nmhSetCmnWithLock(FsMplsDsTeStatus, 11, FsMplsDsTeStatusSet, CmnLock, CmnUnLock, 0, 0, 0, "%i", i4SetValFsMplsDsTeStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsDsTeClassTypeIndex[13];
extern UINT4 FsMplsDsTeClassTypeDescription[13];
extern UINT4 FsMplsDsTeClassTypeRowStatus[13];
extern UINT4 FsMplsDsTeClassTypeBwPercentage[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsDsTeClassTypeDescription(u4FsMplsDsTeClassTypeIndex ,pSetValFsMplsDsTeClassTypeDescription) \
 nmhSetCmnWithLock(FsMplsDsTeClassTypeDescription, 13, FsMplsDsTeClassTypeDescriptionSet, CmnLock, CmnUnLock, 0, 0, 1, "%u %s", u4FsMplsDsTeClassTypeIndex ,pSetValFsMplsDsTeClassTypeDescription)
#define nmhSetFsMplsDsTeClassTypeRowStatus(u4FsMplsDsTeClassTypeIndex ,i4SetValFsMplsDsTeClassTypeRowStatus) \
 nmhSetCmnWithLock(FsMplsDsTeClassTypeRowStatus, 13, FsMplsDsTeClassTypeRowStatusSet, CmnLock, CmnUnLock, 0, 1, 1, "%u %i", u4FsMplsDsTeClassTypeIndex ,i4SetValFsMplsDsTeClassTypeRowStatus)
#define nmhSetFsMplsDsTeClassTypeBwPercentage(u4FsMplsDsTeClassTypeIndex ,i4SetValFsMplsDsTeClassTypeBwPercentage) \
 nmhSetCmnWithLock(FsMplsDsTeClassTypeBwPercentage, 13, FsMplsDsTeClassTypeBwPercentageSet, CmnLock, CmnUnLock, 0, 0, 1, "%u %i", u4FsMplsDsTeClassTypeIndex ,i4SetValFsMplsDsTeClassTypeBwPercentage)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsDsTeTcIndex[13];
extern UINT4 FsMplsDsTeTcType[13];
extern UINT4 FsMplsDsTeTcDescription[13];
extern UINT4 FsMplsDsTeTcMapRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsDsTeTcType(u4FsMplsDsTeClassTypeIndex , u4FsMplsDsTeTcIndex ,i4SetValFsMplsDsTeTcType) \
 nmhSetCmnWithLock(FsMplsDsTeTcType, 13, FsMplsDsTeTcTypeSet, CmnLock, CmnUnLock, 0, 0, 2, "%u %u %i", u4FsMplsDsTeClassTypeIndex , u4FsMplsDsTeTcIndex ,i4SetValFsMplsDsTeTcType)
#define nmhSetFsMplsDsTeTcDescription(u4FsMplsDsTeClassTypeIndex , u4FsMplsDsTeTcIndex ,pSetValFsMplsDsTeTcDescription) \
 nmhSetCmnWithLock(FsMplsDsTeTcDescription, 13, FsMplsDsTeTcDescriptionSet, CmnLock, CmnUnLock, 0, 0, 2, "%u %u %s", u4FsMplsDsTeClassTypeIndex , u4FsMplsDsTeTcIndex ,pSetValFsMplsDsTeTcDescription)
#define nmhSetFsMplsDsTeTcMapRowStatus(u4FsMplsDsTeClassTypeIndex , u4FsMplsDsTeTcIndex ,i4SetValFsMplsDsTeTcMapRowStatus) \
 nmhSetCmnWithLock(FsMplsDsTeTcMapRowStatus, 13, FsMplsDsTeTcMapRowStatusSet, CmnLock, CmnUnLock, 0, 1, 2, "%u %u %i", u4FsMplsDsTeClassTypeIndex , u4FsMplsDsTeTcIndex ,i4SetValFsMplsDsTeTcMapRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsDsTeTeClassPriority[13];
extern UINT4 FsMplsDsTeTeClassDesc[13];
extern UINT4 FsMplsDsTeTeClassNumber[13];
extern UINT4 FsMplsDsTeTeClassRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsDsTeTeClassDesc(u4FsMplsDsTeClassTypeIndex , u4FsMplsDsTeTeClassPriority ,pSetValFsMplsDsTeTeClassDesc) \
 nmhSetCmnWithLock(FsMplsDsTeTeClassDesc, 13, FsMplsDsTeTeClassDescSet, CmnLock, CmnUnLock, 0, 0, 2, "%u %u %s", u4FsMplsDsTeClassTypeIndex , u4FsMplsDsTeTeClassPriority ,pSetValFsMplsDsTeTeClassDesc)
#define nmhSetFsMplsDsTeTeClassNumber(u4FsMplsDsTeClassTypeIndex , u4FsMplsDsTeTeClassPriority ,u4SetValFsMplsDsTeTeClassNumber) \
 nmhSetCmnWithLock(FsMplsDsTeTeClassNumber, 13, FsMplsDsTeTeClassNumberSet, CmnLock, CmnUnLock, 0, 0, 2, "%u %u %u", u4FsMplsDsTeClassTypeIndex , u4FsMplsDsTeTeClassPriority ,u4SetValFsMplsDsTeTeClassNumber)
#define nmhSetFsMplsDsTeTeClassRowStatus(u4FsMplsDsTeClassTypeIndex , u4FsMplsDsTeTeClassPriority ,i4SetValFsMplsDsTeTeClassRowStatus) \
 nmhSetCmnWithLock(FsMplsDsTeTeClassRowStatus, 13, FsMplsDsTeTeClassRowStatusSet, CmnLock, CmnUnLock, 0, 1, 2, "%u %u %i", u4FsMplsDsTeClassTypeIndex , u4FsMplsDsTeTeClassPriority ,i4SetValFsMplsDsTeTeClassRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsL2VpnTrcFlag[12];
extern UINT4 FsMplsL2VpnCleanupInterval[12];
extern UINT4 FsMplsL2VpnAdminStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsL2VpnTrcFlag(i4SetValFsMplsL2VpnTrcFlag) \
 nmhSetCmnWithLock(FsMplsL2VpnTrcFlag, 12, FsMplsL2VpnTrcFlagSet, L2vpnLock, L2vpnUnLock, 0, 0, 0, "%i", i4SetValFsMplsL2VpnTrcFlag)
#define nmhSetFsMplsL2VpnCleanupInterval(i4SetValFsMplsL2VpnCleanupInterval) \
 nmhSetCmnWithLock(FsMplsL2VpnCleanupInterval, 12, FsMplsL2VpnCleanupIntervalSet, L2vpnLock, L2vpnUnLock, 0, 0, 0, "%i", i4SetValFsMplsL2VpnCleanupInterval)
#define nmhSetFsMplsL2VpnAdminStatus(i4SetValFsMplsL2VpnAdminStatus) \
 nmhSetCmnWithLock(FsMplsL2VpnAdminStatus, 12, FsMplsL2VpnAdminStatusSet, L2vpnLock, L2vpnUnLock, 0, 0, 0, "%i", i4SetValFsMplsL2VpnAdminStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsL2VpnPwMode[14];
extern UINT4 FsMplsL2VpnVplsIndex[14];
extern UINT4 FsMplsL2VpnPwLocalCapabAdvert[14];
extern UINT4 FsMplsL2VpnPwLocalCCAdvert[14];
extern UINT4 FsMplsL2VpnPwLocalCVAdvert[14];
extern UINT4 FsMplsL2VpnPwRemoteCCAdvert[14];
extern UINT4 FsMplsL2VpnPwRemoteCVAdvert[14];
extern UINT4 FsMplsL2VpnPwOamEnable[14];
extern UINT4 FsMplsL2VpnPwGenAGIType[14];
extern UINT4 FsMplsL2VpnPwGenLocalAIIType[14];
extern UINT4 FsMplsL2VpnPwGenRemoteAIIType[14];
extern UINT4 FsMplsL2VpnPwAIIFormat[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsL2VpnPwMode(u4PwIndex ,i4SetValFsMplsL2VpnPwMode) \
 nmhSetCmnWithLock(FsMplsL2VpnPwMode, 14, FsMplsL2VpnPwModeSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %i", u4PwIndex ,i4SetValFsMplsL2VpnPwMode)
#define nmhSetFsMplsL2VpnVplsIndex(u4PwIndex ,u4SetValFsMplsL2VpnVplsIndex) \
 nmhSetCmnWithLock(FsMplsL2VpnVplsIndex, 14, FsMplsL2VpnVplsIndexSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %u", u4PwIndex ,u4SetValFsMplsL2VpnVplsIndex)
#define nmhSetFsMplsL2VpnPwLocalCapabAdvert(u4PwIndex ,pSetValFsMplsL2VpnPwLocalCapabAdvert) \
 nmhSetCmnWithLock(FsMplsL2VpnPwLocalCapabAdvert, 14, FsMplsL2VpnPwLocalCapabAdvertSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %s", u4PwIndex ,pSetValFsMplsL2VpnPwLocalCapabAdvert)
#define nmhSetFsMplsL2VpnPwLocalCCAdvert(u4PwIndex ,pSetValFsMplsL2VpnPwLocalCCAdvert) \
 nmhSetCmnWithLock(FsMplsL2VpnPwLocalCCAdvert, 14, FsMplsL2VpnPwLocalCCAdvertSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %s", u4PwIndex ,pSetValFsMplsL2VpnPwLocalCCAdvert)
#define nmhSetFsMplsL2VpnPwLocalCVAdvert(u4PwIndex ,pSetValFsMplsL2VpnPwLocalCVAdvert) \
 nmhSetCmnWithLock(FsMplsL2VpnPwLocalCVAdvert, 14, FsMplsL2VpnPwLocalCVAdvertSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %s", u4PwIndex ,pSetValFsMplsL2VpnPwLocalCVAdvert)
#define nmhSetFsMplsL2VpnPwRemoteCCAdvert(u4PwIndex ,pSetValFsMplsL2VpnPwRemoteCCAdvert) \
 nmhSetCmnWithLock(FsMplsL2VpnPwRemoteCCAdvert, 14, FsMplsL2VpnPwRemoteCCAdvertSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %s", u4PwIndex ,pSetValFsMplsL2VpnPwRemoteCCAdvert)
#define nmhSetFsMplsL2VpnPwRemoteCVAdvert(u4PwIndex ,pSetValFsMplsL2VpnPwRemoteCVAdvert) \
 nmhSetCmnWithLock(FsMplsL2VpnPwRemoteCVAdvert, 14, FsMplsL2VpnPwRemoteCVAdvertSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %s", u4PwIndex ,pSetValFsMplsL2VpnPwRemoteCVAdvert)
#define nmhSetFsMplsL2VpnPwOamEnable(u4PwIndex ,i4SetValFsMplsL2VpnPwOamEnable) \
 nmhSetCmnWithLock(FsMplsL2VpnPwOamEnable, 14, FsMplsL2VpnPwOamEnableSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %i", u4PwIndex ,i4SetValFsMplsL2VpnPwOamEnable)
#define nmhSetFsMplsL2VpnPwGenAGIType(u4PwIndex ,u4SetValFsMplsL2VpnPwGenAGIType) \
 nmhSetCmnWithLock(FsMplsL2VpnPwGenAGIType, 14, FsMplsL2VpnPwGenAGITypeSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %u", u4PwIndex ,u4SetValFsMplsL2VpnPwGenAGIType)
#define nmhSetFsMplsL2VpnPwGenLocalAIIType(u4PwIndex ,u4SetValFsMplsL2VpnPwGenLocalAIIType) \
 nmhSetCmnWithLock(FsMplsL2VpnPwGenLocalAIIType, 14, FsMplsL2VpnPwGenLocalAIITypeSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %u", u4PwIndex ,u4SetValFsMplsL2VpnPwGenLocalAIIType)
#define nmhSetFsMplsL2VpnPwGenRemoteAIIType(u4PwIndex ,u4SetValFsMplsL2VpnPwGenRemoteAIIType) \
 nmhSetCmnWithLock(FsMplsL2VpnPwGenRemoteAIIType, 14, FsMplsL2VpnPwGenRemoteAIITypeSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %u", u4PwIndex ,u4SetValFsMplsL2VpnPwGenRemoteAIIType)
#define nmhSetFsMplsL2VpnPwAIIFormat(u4PwIndex ,pSetValFsMplsL2VpnPwAIIFormat) \
 nmhSetCmnWithLock(FsMplsL2VpnPwAIIFormat, 14, FsMplsL2VpnPwAIIFormatSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %s", u4PwIndex ,pSetValFsMplsL2VpnPwAIIFormat)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsVplsInstanceIndex[14];
extern UINT4 FsMplsVplsVsi[14];
extern UINT4 FsMplsVplsVpnId[14];
extern UINT4 FsMplsVplsName[14];
extern UINT4 FsMplsVplsDescr[14];
extern UINT4 FsMplsVplsFdbHighWatermark[14];
extern UINT4 FsMplsVplsFdbLowWatermark[14];
extern UINT4 FsMplsVplsRowStatus[14];
extern UINT4 FsMplsVplsL2MapFdbId[14];
extern UINT4 FsmplsVplsMtu[14];
extern UINT4 FsmplsVplsStorageType[14];
extern UINT4 FsmplsVplsSignalingType[14];
extern UINT4 FsmplsVplsControlWord[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsVplsVsi(u4FsMplsVplsInstanceIndex ,i4SetValFsMplsVplsVsi) \
 nmhSetCmnWithLock(FsMplsVplsVsi, 14, FsMplsVplsVsiSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %i", u4FsMplsVplsInstanceIndex ,i4SetValFsMplsVplsVsi)
#define nmhSetFsMplsVplsVpnId(u4FsMplsVplsInstanceIndex ,pSetValFsMplsVplsVpnId) \
 nmhSetCmnWithLock(FsMplsVplsVpnId, 14, FsMplsVplsVpnIdSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %s", u4FsMplsVplsInstanceIndex ,pSetValFsMplsVplsVpnId)
#define nmhSetFsMplsVplsName(u4FsMplsVplsInstanceIndex ,pSetValFsMplsVplsName) \
 nmhSetCmnWithLock(FsMplsVplsName, 14, FsMplsVplsNameSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %s", u4FsMplsVplsInstanceIndex ,pSetValFsMplsVplsName)
#define nmhSetFsMplsVplsDescr(u4FsMplsVplsInstanceIndex ,pSetValFsMplsVplsDescr) \
 nmhSetCmnWithLock(FsMplsVplsDescr, 14, FsMplsVplsDescrSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %s", u4FsMplsVplsInstanceIndex ,pSetValFsMplsVplsDescr)
#define nmhSetFsMplsVplsFdbHighWatermark(u4FsMplsVplsInstanceIndex ,u4SetValFsMplsVplsFdbHighWatermark) \
 nmhSetCmnWithLock(FsMplsVplsFdbHighWatermark, 14, FsMplsVplsFdbHighWatermarkSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %u", u4FsMplsVplsInstanceIndex ,u4SetValFsMplsVplsFdbHighWatermark)
#define nmhSetFsMplsVplsFdbLowWatermark(u4FsMplsVplsInstanceIndex ,u4SetValFsMplsVplsFdbLowWatermark) \
 nmhSetCmnWithLock(FsMplsVplsFdbLowWatermark, 14, FsMplsVplsFdbLowWatermarkSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %u", u4FsMplsVplsInstanceIndex ,u4SetValFsMplsVplsFdbLowWatermark)
#define nmhSetFsMplsVplsRowStatus(u4FsMplsVplsInstanceIndex ,i4SetValFsMplsVplsRowStatus) \
 nmhSetCmnWithLock(FsMplsVplsRowStatus, 14, FsMplsVplsRowStatusSet, L2vpnLock, L2vpnUnLock, 0, 1, 1, "%u %i", u4FsMplsVplsInstanceIndex ,i4SetValFsMplsVplsRowStatus)
#define nmhSetFsMplsVplsL2MapFdbId(u4FsMplsVplsInstanceIndex ,i4SetValFsMplsVplsL2MapFdbId) \
 nmhSetCmnWithLock(FsMplsVplsL2MapFdbId, 14, FsMplsVplsL2MapFdbIdSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %i", u4FsMplsVplsInstanceIndex ,i4SetValFsMplsVplsL2MapFdbId)
#define nmhSetFsmplsVplsMtu(u4FsMplsVplsInstanceIndex ,u4SetValFsmplsVplsMtu) \
 nmhSetCmnWithLock(FsmplsVplsMtu, 14, FsmplsVplsMtuSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %u", u4FsMplsVplsInstanceIndex ,u4SetValFsmplsVplsMtu)
#define nmhSetFsmplsVplsStorageType(u4FsMplsVplsInstanceIndex ,i4SetValFsmplsVplsStorageType) \
 nmhSetCmnWithLock(FsmplsVplsStorageType, 14, FsmplsVplsStorageTypeSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %i", u4FsMplsVplsInstanceIndex ,i4SetValFsmplsVplsStorageType)
#define nmhSetFsmplsVplsSignalingType(u4FsMplsVplsInstanceIndex ,i4SetValFsmplsVplsSignalingType) \
 nmhSetCmnWithLock(FsmplsVplsSignalingType, 14, FsmplsVplsSignalingTypeSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %i", u4FsMplsVplsInstanceIndex ,i4SetValFsmplsVplsSignalingType)
#define nmhSetFsmplsVplsControlWord(u4FsMplsVplsInstanceIndex ,i4SetValFsmplsVplsControlWord) \
 nmhSetCmnWithLock(FsmplsVplsControlWord, 14, FsmplsVplsControlWordSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %i", u4FsMplsVplsInstanceIndex ,i4SetValFsmplsVplsControlWord)

#endif

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPwMplsInboundTunnelIndex[14];
extern UINT4 FsPwMplsInboundTunnelEgressLSR[14];
extern UINT4 FsPwMplsInboundTunnelIngressLSR[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPwMplsInboundTunnelIndex(u4PwIndex ,u4SetValFsPwMplsInboundTunnelIndex) \
 nmhSetCmnWithLock(FsPwMplsInboundTunnelIndex, 14, FsPwMplsInboundTunnelIndexSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %u", u4PwIndex ,u4SetValFsPwMplsInboundTunnelIndex)
#define nmhSetFsPwMplsInboundTunnelEgressLSR(u4PwIndex ,pSetValFsPwMplsInboundTunnelEgressLSR) \
 nmhSetCmnWithLock(FsPwMplsInboundTunnelEgressLSR, 14, FsPwMplsInboundTunnelEgressLSRSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %s", u4PwIndex ,pSetValFsPwMplsInboundTunnelEgressLSR)
#define nmhSetFsPwMplsInboundTunnelIngressLSR(u4PwIndex ,pSetValFsPwMplsInboundTunnelIngressLSR) \
 nmhSetCmnWithLock(FsPwMplsInboundTunnelIngressLSR, 14, FsPwMplsInboundTunnelIngressLSRSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %s", u4PwIndex ,pSetValFsPwMplsInboundTunnelIngressLSR)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsLocalCCTypesCapabilities[12];
extern UINT4 FsMplsLocalCVTypesCapabilities[12];
extern UINT4 FsMplsRouterID[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsLocalCCTypesCapabilities(pSetValFsMplsLocalCCTypesCapabilities) \
 nmhSetCmnWithLock(FsMplsLocalCCTypesCapabilities, 12, FsMplsLocalCCTypesCapabilitiesSet, L2vpnLock, L2vpnUnLock, 0, 0, 0, "%s", pSetValFsMplsLocalCCTypesCapabilities)
#define nmhSetFsMplsLocalCVTypesCapabilities(pSetValFsMplsLocalCVTypesCapabilities) \
 nmhSetCmnWithLock(FsMplsLocalCVTypesCapabilities, 12, FsMplsLocalCVTypesCapabilitiesSet, L2vpnLock, L2vpnUnLock, 0, 0, 0, "%s", pSetValFsMplsLocalCVTypesCapabilities)
#define nmhSetFsMplsRouterID(pSetValFsMplsRouterID) \
 nmhSetCmnWithLock(FsMplsRouterID, 12, FsMplsRouterIDSet, CmnLock, CmnUnLock, 0, 0, 0, "%s", pSetValFsMplsRouterID)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsPortBundleStatus[14];
extern UINT4 FsMplsPortMultiplexStatus[14];
extern UINT4 FsMplsPortAllToOneBundleStatus[14];
extern UINT4 FsMplsPortRowStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsPortBundleStatus(i4IfIndex ,i4SetValFsMplsPortBundleStatus) \
 nmhSetCmnWithLock(FsMplsPortBundleStatus, 14, FsMplsPortBundleStatusSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValFsMplsPortBundleStatus)
#define nmhSetFsMplsPortMultiplexStatus(i4IfIndex ,i4SetValFsMplsPortMultiplexStatus) \
 nmhSetCmnWithLock(FsMplsPortMultiplexStatus, 14, FsMplsPortMultiplexStatusSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValFsMplsPortMultiplexStatus)
#define nmhSetFsMplsPortAllToOneBundleStatus(i4IfIndex ,i4SetValFsMplsPortAllToOneBundleStatus) \
 nmhSetCmnWithLock(FsMplsPortAllToOneBundleStatus, 14, FsMplsPortAllToOneBundleStatusSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValFsMplsPortAllToOneBundleStatus)
#define nmhSetFsMplsPortRowStatus(i4IfIndex ,i4SetValFsMplsPortRowStatus) \
 nmhSetCmnWithLock(FsMplsPortRowStatus, 14, FsMplsPortRowStatusSet, L2vpnLock, L2vpnUnLock, 0, 1, 1, "%i %i", i4IfIndex ,i4SetValFsMplsPortRowStatus)
#endif
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsVplsAcMapVplsIndex[14];
extern UINT4 FsMplsVplsAcMapAcIndex[14];
extern UINT4 FsMplsVplsAcMapPortIfIndex[14];
extern UINT4 FsMplsVplsAcMapPortVlan[14];
extern UINT4 FsMplsVplsAcMapRowStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsVplsAcMapPortIfIndex(u4FsMplsVplsAcMapVplsIndex , u4FsMplsVplsAcMapAcIndex ,u4SetValFsMplsVplsAcMapPortIfIndex) \
 nmhSetCmnWithLock(FsMplsVplsAcMapPortIfIndex, 14, FsMplsVplsAcMapPortIfIndexSet, L2vpnLock, L2vpnUnLock, 0, 0, 2, "%u %u %u", u4FsMplsVplsAcMapVplsIndex , u4FsMplsVplsAcMapAcIndex ,u4SetValFsMplsVplsAcMapPortIfIndex)
#define nmhSetFsMplsVplsAcMapPortVlan(u4FsMplsVplsAcMapVplsIndex , u4FsMplsVplsAcMapAcIndex ,u4SetValFsMplsVplsAcMapPortVlan) \
 nmhSetCmnWithLock(FsMplsVplsAcMapPortVlan, 14, FsMplsVplsAcMapPortVlanSet, L2vpnLock, L2vpnUnLock, 0, 0, 2, "%u %u %u", u4FsMplsVplsAcMapVplsIndex , u4FsMplsVplsAcMapAcIndex ,u4SetValFsMplsVplsAcMapPortVlan)
#define nmhSetFsMplsVplsAcMapRowStatus(u4FsMplsVplsAcMapVplsIndex , u4FsMplsVplsAcMapAcIndex ,i4SetValFsMplsVplsAcMapRowStatus) \
 nmhSetCmnWithLock(FsMplsVplsAcMapRowStatus, 14, FsMplsVplsAcMapRowStatusSet, L2vpnLock, L2vpnUnLock, 0, 1, 2, "%u %u %i", u4FsMplsVplsAcMapVplsIndex , u4FsMplsVplsAcMapAcIndex ,i4SetValFsMplsVplsAcMapRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsL2VpnMaxPwVcEntries[12];
extern UINT4 FsMplsL2VpnMaxPwVcMplsEntries[12];
extern UINT4 FsMplsL2VpnMaxPwVcMplsInOutEntries[12];
extern UINT4 FsMplsL2VpnMaxEthernetPwVcs[12];
extern UINT4 FsMplsL2VpnMaxPwVcEnetEntries[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsL2VpnMaxPwVcEntries(u4SetValFsMplsL2VpnMaxPwVcEntries) \
 nmhSetCmnWithLock(FsMplsL2VpnMaxPwVcEntries, 12, FsMplsL2VpnMaxPwVcEntriesSet, L2vpnLock, L2vpnUnLock, 0, 0, 0, "%u", u4SetValFsMplsL2VpnMaxPwVcEntries)
#define nmhSetFsMplsL2VpnMaxPwVcMplsEntries(u4SetValFsMplsL2VpnMaxPwVcMplsEntries) \
 nmhSetCmnWithLock(FsMplsL2VpnMaxPwVcMplsEntries, 12, FsMplsL2VpnMaxPwVcMplsEntriesSet, L2vpnLock, L2vpnUnLock, 0, 0, 0, "%u", u4SetValFsMplsL2VpnMaxPwVcMplsEntries)
#define nmhSetFsMplsL2VpnMaxPwVcMplsInOutEntries(u4SetValFsMplsL2VpnMaxPwVcMplsInOutEntries) \
 nmhSetCmnWithLock(FsMplsL2VpnMaxPwVcMplsInOutEntries, 12, FsMplsL2VpnMaxPwVcMplsInOutEntriesSet, L2vpnLock, L2vpnUnLock, 0, 0, 0, "%u", u4SetValFsMplsL2VpnMaxPwVcMplsInOutEntries)
#define nmhSetFsMplsL2VpnMaxEthernetPwVcs(u4SetValFsMplsL2VpnMaxEthernetPwVcs) \
 nmhSetCmnWithLock(FsMplsL2VpnMaxEthernetPwVcs, 12, FsMplsL2VpnMaxEthernetPwVcsSet, L2vpnLock, L2vpnUnLock, 0, 0, 0, "%u", u4SetValFsMplsL2VpnMaxEthernetPwVcs)
#define nmhSetFsMplsL2VpnMaxPwVcEnetEntries(u4SetValFsMplsL2VpnMaxPwVcEnetEntries) \
 nmhSetCmnWithLock(FsMplsL2VpnMaxPwVcEnetEntries, 12, FsMplsL2VpnMaxPwVcEnetEntriesSet, L2vpnLock, L2vpnUnLock, 0, 0, 0, "%u", u4SetValFsMplsL2VpnMaxPwVcEnetEntries)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsSimulateFailure[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsSimulateFailure(i4SetValFsMplsSimulateFailure) \
 nmhSetCmnWithLock(FsMplsSimulateFailure, 11, FsMplsSimulateFailureSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsMplsSimulateFailure)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsLdpGrCapability[11];
extern UINT4 FsMplsLdpGrForwardEntryHoldTime[11];
extern UINT4 FsMplsLdpGrMaxRecoveryTime[11];
extern UINT4 FsMplsLdpGrNeighborLivenessTime[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsLdpGrCapability(i4SetValFsMplsLdpGrCapability) \
 nmhSetCmnWithLock(FsMplsLdpGrCapability, 11, FsMplsLdpGrCapabilitySet, LdpLock, LdpUnLock, 0, 0, 0, "%i", i4SetValFsMplsLdpGrCapability)
#define nmhSetFsMplsLdpGrForwardEntryHoldTime(i4SetValFsMplsLdpGrForwardEntryHoldTime) \
 nmhSetCmnWithLock(FsMplsLdpGrForwardEntryHoldTime, 11, FsMplsLdpGrForwardEntryHoldTimeSet, LdpLock, LdpUnLock, 0, 0, 0, "%i", i4SetValFsMplsLdpGrForwardEntryHoldTime)
#define nmhSetFsMplsLdpGrMaxRecoveryTime(i4SetValFsMplsLdpGrMaxRecoveryTime) \
 nmhSetCmnWithLock(FsMplsLdpGrMaxRecoveryTime, 11, FsMplsLdpGrMaxRecoveryTimeSet, LdpLock, LdpUnLock, 0, 0, 0, "%i", i4SetValFsMplsLdpGrMaxRecoveryTime)
#define nmhSetFsMplsLdpGrNeighborLivenessTime(i4SetValFsMplsLdpGrNeighborLivenessTime) \
 nmhSetCmnWithLock(FsMplsLdpGrNeighborLivenessTime, 11, FsMplsLdpGrNeighborLivenessTimeSet, LdpLock, LdpUnLock, 0, 0, 0, "%i", i4SetValFsMplsLdpGrNeighborLivenessTime)

#endif

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsLdpConfigurationSequenceTLVEnable[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsLdpConfigurationSequenceTLVEnable(i4SetValFsMplsLdpConfigurationSequenceTLVEnable) \
   nmhSetCmnWithLock(FsMplsLdpConfigurationSequenceTLVEnable, 11, FsMplsLdpConfigurationSequenceTLVEnableSet, LdpLock, LdpUnLock, 0, 0, 0, "%i", i4SetValFsMplsLdpConfigurationSequenceTLVEnable)

#endif
#ifdef MPLS_L3VPN_WANTED


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsL3VpnVrfName[13];
extern UINT4 FsMplsL3VpnVrfEgressRteInetCidrDestType[13];
extern UINT4 FsMplsL3VpnVrfEgressRteInetCidrDest[13];
extern UINT4 FsMplsL3VpnVrfEgressRteInetCidrPfxLen[13];
extern UINT4 FsMplsL3VpnVrfEgressRteInetCidrLabelPolicy[13];
extern UINT4 FsMplsL3VpnVrfEgressRteInetCidrNHopType[13];
extern UINT4 FsMplsL3VpnVrfEgressRteInetCidrNextHop[13];
extern UINT4 FsMplsL3VpnVrfEgressRteInetCidrIfIndex[13];
extern UINT4 FsMplsL3VpnVrfEgressRteInetCidrMetric1[13];
extern UINT4 FsMplsL3VpnVrfEgressRteInetCidrStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsL3VpnVrfName(u4FsMplsL3VpnVrfEgressRtePathAttrLabel ,pSetValFsMplsL3VpnVrfName) \
    nmhSetCmn(FsMplsL3VpnVrfName, 13, FsMplsL3VpnVrfNameSet, NULL, NULL, 0, 0, 1, "%u %s", u4FsMplsL3VpnVrfEgressRtePathAttrLabel ,pSetValFsMplsL3VpnVrfName)
#define nmhSetFsMplsL3VpnVrfEgressRteInetCidrDestType(u4FsMplsL3VpnVrfEgressRtePathAttrLabel ,i4SetValFsMplsL3VpnVrfEgressRteInetCidrDestType)  \
    nmhSetCmn(FsMplsL3VpnVrfEgressRteInetCidrDestType, 13, FsMplsL3VpnVrfEgressRteInetCidrDestTypeSet, NULL, NULL, 0, 0, 1, "%u %i", u4FsMplsL3VpnVrfEgressRtePathAttrLabel ,i4SetValFsMplsL3VpnVrfEgressRteInetCidrDestType)
#define nmhSetFsMplsL3VpnVrfEgressRteInetCidrDest(u4FsMplsL3VpnVrfEgressRtePathAttrLabel ,pSetValFsMplsL3VpnVrfEgressRteInetCidrDest)   \
    nmhSetCmn(FsMplsL3VpnVrfEgressRteInetCidrDest, 13, FsMplsL3VpnVrfEgressRteInetCidrDestSet, NULL, NULL, 0, 0, 1, "%u %s", u4FsMplsL3VpnVrfEgressRtePathAttrLabel ,pSetValFsMplsL3VpnVrfEgressRteInetCidrDest)
#define nmhSetFsMplsL3VpnVrfEgressRteInetCidrPfxLen(u4FsMplsL3VpnVrfEgressRtePathAttrLabel ,u4SetValFsMplsL3VpnVrfEgressRteInetCidrPfxLen)  \
    nmhSetCmn(FsMplsL3VpnVrfEgressRteInetCidrPfxLen, 13, FsMplsL3VpnVrfEgressRteInetCidrPfxLenSet, NULL, NULL, 0, 0, 1, "%u %u", u4FsMplsL3VpnVrfEgressRtePathAttrLabel ,u4SetValFsMplsL3VpnVrfEgressRteInetCidrPfxLen)
#define nmhSetFsMplsL3VpnVrfEgressRteInetCidrLabelPolicy(u4FsMplsL3VpnVrfEgressRtePathAttrLabel ,i4SetValFsMplsL3VpnVrfEgressRteInetCidrLabelPolicy)    \
    nmhSetCmn(FsMplsL3VpnVrfEgressRteInetCidrLabelPolicy, 13, FsMplsL3VpnVrfEgressRteInetCidrLabelPolicySet, NULL, NULL, 0, 0, 1, "%u %i", u4FsMplsL3VpnVrfEgressRtePathAttrLabel ,i4SetValFsMplsL3VpnVrfEgressRteInetCidrLabelPolicy)
#define nmhSetFsMplsL3VpnVrfEgressRteInetCidrNHopType(u4FsMplsL3VpnVrfEgressRtePathAttrLabel ,i4SetValFsMplsL3VpnVrfEgressRteInetCidrNHopType)  \
    nmhSetCmn(FsMplsL3VpnVrfEgressRteInetCidrNHopType, 13, FsMplsL3VpnVrfEgressRteInetCidrNHopTypeSet, NULL, NULL, 0, 0, 1, "%u %i", u4FsMplsL3VpnVrfEgressRtePathAttrLabel ,i4SetValFsMplsL3VpnVrfEgressRteInetCidrNHopType)
#define nmhSetFsMplsL3VpnVrfEgressRteInetCidrNextHop(u4FsMplsL3VpnVrfEgressRtePathAttrLabel ,pSetValFsMplsL3VpnVrfEgressRteInetCidrNextHop) \
    nmhSetCmn(FsMplsL3VpnVrfEgressRteInetCidrNextHop, 13, FsMplsL3VpnVrfEgressRteInetCidrNextHopSet, NULL, NULL, 0, 0, 1, "%u %s", u4FsMplsL3VpnVrfEgressRtePathAttrLabel ,pSetValFsMplsL3VpnVrfEgressRteInetCidrNextHop)
#define nmhSetFsMplsL3VpnVrfEgressRteInetCidrIfIndex(u4FsMplsL3VpnVrfEgressRtePathAttrLabel ,i4SetValFsMplsL3VpnVrfEgressRteInetCidrIfIndex)    \
    nmhSetCmn(FsMplsL3VpnVrfEgressRteInetCidrIfIndex, 13, FsMplsL3VpnVrfEgressRteInetCidrIfIndexSet, NULL, NULL, 0, 0, 1, "%u %i", u4FsMplsL3VpnVrfEgressRtePathAttrLabel ,i4SetValFsMplsL3VpnVrfEgressRteInetCidrIfIndex)
#define nmhSetFsMplsL3VpnVrfEgressRteInetCidrMetric1(u4FsMplsL3VpnVrfEgressRtePathAttrLabel ,i4SetValFsMplsL3VpnVrfEgressRteInetCidrMetric1)    \
    nmhSetCmn(FsMplsL3VpnVrfEgressRteInetCidrMetric1, 13, FsMplsL3VpnVrfEgressRteInetCidrMetric1Set, NULL, NULL, 0, 0, 1, "%u %i", u4FsMplsL3VpnVrfEgressRtePathAttrLabel ,i4SetValFsMplsL3VpnVrfEgressRteInetCidrMetric1)
#define nmhSetFsMplsL3VpnVrfEgressRteInetCidrStatus(u4FsMplsL3VpnVrfEgressRtePathAttrLabel ,i4SetValFsMplsL3VpnVrfEgressRteInetCidrStatus)  \
    nmhSetCmn(FsMplsL3VpnVrfEgressRteInetCidrStatus, 13, FsMplsL3VpnVrfEgressRteInetCidrStatusSet, NULL, NULL, 0, 1, 1, "%u %i", u4FsMplsL3VpnVrfEgressRtePathAttrLabel ,i4SetValFsMplsL3VpnVrfEgressRteInetCidrStatus)

#endif
#endif
