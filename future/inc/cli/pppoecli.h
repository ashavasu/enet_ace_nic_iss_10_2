/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: pppoecli.h,v 1.1 2014/12/16 10:51:24 siva Exp $
*
* Description: Header file for extern declarations of the mib objects in pppoe.mib
*********************************************************************/
#ifndef __PPPOECLI_H__
#define __PPPOECLI_H__

/* extern declaration corresponding to OID variable present in protocol pppoedb.h */
extern UINT4 PPPoEModeOid[11];
extern UINT4 PPPoEConfigMaxTotalSessions[11];
extern UINT4 PPPoEConfigMaxSessionsPerHost[11];
extern UINT4 PPPoEConfigPADITxInterval[11];
extern UINT4 PPPoEConfigPADRMaxNumberOfRetries[11];
extern UINT4 PPPoEConfigPADRWaitTime[11];
extern UINT4 PPPoEHostUniqueEnabled[11];
extern UINT4 PPPoEACName[11];
extern UINT4 PPPoEConfigServiceName[13];
extern UINT4 PPPoEConfigServiceRowStatus[13];
extern UINT4 PPPoEBindingsIndex[13];
extern UINT4 PPPoEBindingsEnabled[13];
extern UINT4 PPPoEVlanPPPIndex[13];
extern UINT4 PPPoEVlanID[13];
extern UINT4 PPPoECFI[13];
extern UINT4 PPPoECoS[13];
extern UINT4 PPPoEVlanRowStatus[13];

#endif /*__PPPOECLI_H__*/
