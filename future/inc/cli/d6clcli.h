
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: d6clcli.h,v 1.8 2013/05/17 11:54:51 siva Exp $
 * Description: This file contains macros to the CLI commands and 
 * structures for the DHCP Client
 *******************************************************************/

#ifndef _DHCP6_CLNT_CLI_H
#define _DHCP6_CLNT_CLI_H

#include "cli.h"

/* DHCP6 CLI command constants. To add a new command ,a new definition needs to
 * be added to the list.
 */


INT4 
cli_process_d6cl_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Comamnd, ...));

INT4 
cli_process_d6cl_show_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Comamnd, ...));

VOID IssDhcp6ClientShowDebugging(tCliHandle CliHandle);

enum {
    CLI_DHCP6_CLNT_INTERFACE_PROPERTIES = 1,
    CLI_DHCP6_CLNT_NO_INTERFACE_PROPERTIES,
    CLI_DHCP6_CLNT_DUID_INTERFACE_INDEX,
    CLI_DHCP6_CLNT_DUID_TYPE,
    CLI_DHCP6_CLNT_AUTH_REALM_KEY_ID,
    CLI_DHCP6_CLNT_ENABLE_CLIENT_FUNC,
    CLI_DHCP6_CLNT_DISABLE_CLIENT_FUNC,
    CLI_DHCP6_CLNT_ENABLE_TRAP,
    CLI_DHCP6_CLNT_DISABLE_TRAP,
    CLI_DHCP6_CLNT_DEBUG,
    CLI_DHCP6_CLNT_NO_DEBUG,
    CLI_DHCP6_CLNT_CLEAR_ALL_INTERFACE_STATS,
    CLI_DHCP6_CLNT_CLEAR_INTERFACE_STATS,
    CLI_DHCP6_CLNT_SHOW_INTERFACE_STATS,
    CLI_DHCP6_CLNT_SHOW_ALL_INTERFACE_STATS,
    CLI_DHCP6_CLNT_SHOW_INTERFACE_CONF,
    CLI_DHCP6_CLNT_SHOW_ALL_INTERFACE_CONF,
    CLI_DHCP6_CLNT_SHOW_GLOBAL_CONF,
    CLI_DHCP6_CLNT_REFRESH_TIMER_VAL,
    CLI_DHCP6_CLNT_NO_REFRESH_TIMER_VAL,
    CLI_DHCP6_CLNT_UDP_SOURCE_DEST_PORT,
    CLI_DHCP6_CLNT_SYSLOG_STATUS
};

#define DHCP6_CLNT_CLI_MAX_ARGS           13
#define DHCP6_CLNT_INITIAL_RETRANSMISSION_TIME 1 
#define DHCP6_CLNT_MAX_RETRANSMISSION_TIME 2 
#define DHCP6_CLNT_MAX_RETRANSMISSION_COUNT 3 
#define DHCP6_CLNT_MAX_RETRANSMISSION_DELAY 4 
#define DHCP6_CLNT_ALL_INTERFACE_PROPERTIES 5 
#define DHCP6_CLNT_LINK_LAYER_ADD_TIME      6
#define DHCP6_CLNT_VENDOR_ASS_UNIQUE_ID 7 
#define DHCP6_CLNT_LINK_LAYER_ADDRESS 8 
#define DHCP6_CLNT_UDP_SOURCE_PORT_NUMBER    9 
#define DHCP6_CLNT_UDP_DEST_PORT_NUMBER 10 

#define DHCP6_CLNT_REALM_TYPE           11
#define DHCP6_CLNT_KEY_TYPE           12
#define DHCP6_CLNT_KEY_ID              13

#define DHCP6_CLNT_SYSLOG_ENABLE 1
#define DHCP6_CLNT_SYSLOG_DISABLE 2
#define DHCP6_CLNT_ALL_TRAP 0x03 
#define DHCP6_CLNT_INVALID_PKT_IN_TRAP 0x01 
#define DHCP6_CLNT_AUTH_FAIL_TRAP 0x02 
#define DHCP6_CLNT_TRC_MAX_SIZE              255

INT4 D6ClCliSetDebugs (tCliHandle CliHandle, UINT1 *pu1TraceInput);

INT4 D6ClCliSetUdpPorts (tCliHandle CliHandle,UINT4 u4Type, UINT4 u4Value);
INT4 D6ClCliSetTraps (tCliHandle CliHandle,UINT4 u4Command, UINT4 u4TrapValue);

INT4 D6ClCliClearStatsAllInterface (tCliHandle CliHandle);

INT4 D6ClCliClearStatsInterface (tCliHandle CliHandle, INT4 i4Index,
                                   UINT4 u4DuidType);

INT4
D6ClCliDisableClientFun (tCliHandle CliHandle, INT4 i4Index);

INT4
D6ClCliEnableClientFun (tCliHandle CliHandle, INT4 i4Index);
INT4
D6ClCliSetRealmKey (tCliHandle CliHandle, INT4 i4Index,
                     UINT4 u4Type ,UINT1 *pu1KeyName);

INT4
D6ClCliSetDuidType (tCliHandle CliHandle, INT4 i4Index,
                           UINT4 u4DuidType);

INT4
D6ClCliSetDuidIfIndex (tCliHandle CliHandle, INT4 i4Index,
                           UINT4 u4DuidIfValue);
INT4
D6ClCliSetRefreshTimerVal (tCliHandle CliHandle, INT4 i4Index,
                           UINT4 u4Value);
INT4
D6ClCliSetMaxRetranDelay (tCliHandle CliHandle, INT4 i4Index,
                           UINT4 u4Value);
INT4
D6ClCliSetMaxRetranCount (tCliHandle CliHandle, INT4 i4Index,
                           UINT4 u4Value);

INT4
D6ClCliSetMaxRetranTime (tCliHandle CliHandle, INT4 i4Index,
                           UINT4 u4Value);
INT4
D6ClCliSetInitRetranTime (tCliHandle CliHandle, INT4 i4Index,
                                    UINT4 u4Value);
INT4
D6ClCliShowInterfaceStats (tCliHandle CliHandle, INT4 i4Index);
INT4
D6ClCliShowAllInterfaceStats (tCliHandle CliHandle);
INT4
D6ClCliSetSysLogStatus (tCliHandle CliHandle,UINT4 u4SysLogStatus);
INT4
D6ClCliSetInitRetraTime (tCliHandle CliHandle, INT4 i4Index,
                           UINT4 u4Value);

INT4
D6ClCliShowAllInterfaceConf (tCliHandle CliHandle);

INT4
D6ClCliShowInterfaceConf (tCliHandle CliHandle, INT4 i4Index);

INT4
D6ClCliShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module);

INT4
D6ClCliShowRunningScalars (tCliHandle CliHandle);

VOID
D6ClCliShowRunningConfigInterface (tCliHandle CliHandle);

VOID
D6ClCliShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4IfIndex);

INT4
D6ClCliSetKeyId (tCliHandle CliHandle, INT4 i4Index, UINT4 u4Value);
VOID D6ClCliShowGlobalInfo (tCliHandle CliHandle);
INT4 D6ClCliShowGlobalConf (tCliHandle CliHandle);
INT4 D6ClCliShowIfConf (tCliHandle CliHandle,INT4 i4Index);



/*Macros to register, unregister cc/lblt task with cli*/
#define DHCP6_CLNT_REGISTER_CLI_LOCK()\
do{\
CliRegisterLock (CliHandle, D6ClApiLock, D6ClApiUnLock);\
}while(0);

#define DHCP6_CLNT_UNREGISTER_CLI_LOCK()\
do{\
CliUnRegisterLock (CliHandle);\
}while(0);

enum{
DHCP6_CLNT_CLI_ENTRY_EXISTS = 1,
DHCP6_CLNT_CLI_ERR_IP6,
DHCP6_CLNT_CLI_INV_IFTYPE,
DHCP6_CLNT_CLI_INV_ENTRY,
DHCP6_CLNT_CLI_INV_STATUS,
DHCP6_CLNT_CLI_ACT_ROW_STATUS,
DHCP6_CLNT_CLI_INV_IF_TYPE,
DHCP6_CLNT_CLI_INT_SERVER_EXISTS,
DHCP6_CLNT_CLI_INV_RELAY_EXISTS,
DHCP6_CLNT_CLI_IP_ADDRESS_ERR,
DHCP6_CLNT_CLI_INFO_REFRESH_ERR,
DHCP6_CLNT_CLI_INV_DEBUG_STRING,
DHCP6_CLNT_CLI_MAX_ERR
};

#if defined (_DHCP6_CLNT_CLI_C) 
CONST CHR1 *gaD6ClCliErrString [] = {
    NULL,    
    "% Entry already exists\r\n",
    "% Interface does not exist in lower layer\r\n",
    "% Invalid Interface type\r\n",
    "% Client Interface does not exist\r\n",
    "% Invalid status\r\n",
    "% Row status is active\r\n",
    "% Interface does not exists. Client name cannot be generated\r\n",
    "% Interface already in server mode, so interface cannot created in client mode.\r\n",
    "% Interface already in relay mode, so interface cannot created in client mode.\r\n",
    "% Ip address not configured to interface, so interface cannot be created.\r\n",
    "% Value provided for information refresh is not within valid range. \r\n",
    "% Please provide a valid debug string. \r\n",
    "\r\n"
};

#else
extern CONST CHR1  *gaD6ClCliErrString [];
#endif

#ifdef DHCP6_CLNT_TEST_WANTED
VOID D6ClTestCliExecUT(INT4 i4UtId);
#endif /* DHCP6_CLNT_TEST_WANTED */

#endif /*  */
