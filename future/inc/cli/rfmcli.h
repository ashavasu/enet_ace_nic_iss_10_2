/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: rfmcli.h,v 1.14 2017/05/23 14:21:48 siva Exp $
*
* Description: This file contains the RFMGMT CLI related prototypes,
*         enum and macros.
*********************************************************************/


#ifndef __RFMGMTCLI__H__
#define __RFMGMTCLI__H__

#include "lr.h"
#include "cli.h"
#include "rfmtrc.h"

INT4 cli_process_rfmgmt_test_cmd PROTO ((tCliHandle 
            CliHandle, ...));

INT4 cli_process_rfmgmt_cmd PROTO ((tCliHandle 
            CliHandle, UINT4 u4Command, ...));

INT4 RfMgmtRadioCreate PROTO ((tCliHandle, INT4));

INT4 RfMgmtRadioDelete PROTO ((tCliHandle, INT4));

INT4 RfMgmtCliTpcMode PROTO ((tCliHandle, INT4, UINT4));

INT4 RfMgmtCliTpcSelectionMode PROTO ((tCliHandle, INT4, UINT4));

INT4 RfMgmtCliTpcPerAPMode PROTO((tCliHandle, INT4, UINT1 *, UINT4, UINT4 *, UINT1));

INT4 RfMgmtCliDcaMode PROTO ((tCliHandle, INT4, UINT4));

INT4 RfMgmtCliDcaSelectionMode PROTO ((tCliHandle, INT4, UINT4));

INT4 RfMgmtCliDcaPerAPMode PROTO((tCliHandle, INT4, UINT1 *, UINT4, UINT4 *, UINT1));

INT4 RfmgmtCliUtilAssignTxPower PROTO((tCliHandle, UINT4, UINT1, INT4, INT4, UINT4, UINT4 *));

INT4 RfmgmtCliUtilAssignChannel PROTO((tCliHandle, UINT4, UINT1, INT4, INT4, UINT4, UINT4 *));

INT1 RfMgmtCliDcaSensitivity PROTO ((tCliHandle, INT4, UINT4));

INT1 RfMgmtCliDcaChannelUpdate PROTO ((tCliHandle, INT4));

INT1 RfMgmtCliDcaInterval PROTO ((tCliHandle, INT4, UINT4));

INT1 RfMgmtCliTpcInterval PROTO ((tCliHandle, INT4, UINT4));

INT1 RfMgmtCliShaInterval PROTO ((tCliHandle, INT4, UINT4));

INT1 RfMgmtCli11hTpcInterval PROTO ((tCliHandle, INT4, UINT4));

INT1 RfMgmtCli11hMinLinkThreshold PROTO ((tCliHandle, INT4, UINT4));

INT1 RfMgmtCli11hMaxLinkThreshold PROTO ((tCliHandle, INT4, UINT4));

INT1 RfMgmtCli11hStaCountThreshold PROTO ((tCliHandle, INT4, UINT4));

INT4 RfMgmtCliShaStatus PROTO ((tCliHandle, INT4, UINT4));

INT4 RfMgmtCli11hTpcStatus PROTO ((tCliHandle, INT4, UINT4));


INT1 RfMgmtCliTpcUpdate PROTO ((tCliHandle, INT4));

INT4 RfMgmtCliClientThreshold PROTO ((tCliHandle, INT4, UINT4));

INT4 RfMgmtShowAPAutoRf PROTO ((tCliHandle CliHandle,
    INT4 i4RadioType, UINT1 * pu1CapwapBaseWtpProfileName,
    UINT4 u4RadioId));

INT4 RfMgmtShow11hTpcInfo PROTO ((tCliHandle CliHandle,
     UINT1 * pu1CapwapBaseWtpProfileName,
    UINT4 u4RadioId));
 
INT4 RfMgmtCli11hDfsStatus PROTO ((tCliHandle, INT4, UINT4));

INT4 RfMgmtShow11hDfsInfo PROTO ((tCliHandle CliHandle,
     UINT1 * pu1CapwapBaseWtpProfileName,
    UINT4 u4RadioId));   

INT1 RfMgmtCli11hDfsInterval PROTO ((tCliHandle,INT4, UINT4));

INT1 RfMgmtCli11hDfsQuietInterval PROTO((tCliHandle, INT4, UINT4, UINT1 *, UINT4 *));              

INT1 RfMgmtCli11hDfsQuietPeriod PROTO((tCliHandle, INT4, UINT4, UINT1 *, UINT4 *));

INT1 RfMgmtCli11hDfsMeasurementInterval PROTO((tCliHandle, INT4, UINT4, UINT1 *, UINT4 *));  

INT1 RfMgmtCli11hDfsChannelSwitchStatus PROTO((tCliHandle, INT4, UINT4, UINT1 *, UINT4 *)); 

INT1 RfMgmtCliRssiThreshold PROTO ((tCliHandle, INT4, INT4));

INT1 RfMgmtCliPowerThreshold PROTO ((tCliHandle, INT4, INT4));

INT1 RfMgmtCliChannelSwitchStatus PROTO ((tCliHandle, UINT4));

INT1 RfMgmtCliTpcSNRThreshold PROTO ((tCliHandle, INT4, INT4));

INT1 RfMgmtCliSNRScanPeriod PROTO((tCliHandle, INT4, UINT4, UINT1 *, UINT4 *));

INT1 RfMgmtCliSNRScanStatus PROTO((tCliHandle, INT4, UINT4, UINT1 *, UINT4 *));

INT1 RfMgmtCliAPAutoScanStatus PROTO((tCliHandle, INT4, UINT4, UINT1 *, UINT4 *));

INT1 RfMgmtCliDcaChannelScanDuration PROTO((tCliHandle, INT4, UINT4, UINT1 *, UINT4 *));

INT1 RfMgmtCliDcaScanFrequency PROTO((tCliHandle, INT4, UINT4, UINT1 *, UINT4 *));

INT1 RfMgmtCliNeighborAgingPeriod PROTO((tCliHandle, INT4, UINT4, UINT1 *, UINT4 *));

INT1 RfMgmtCli11hTpcRequestInterval PROTO((tCliHandle, INT4, UINT4, UINT1 *, UINT4 *));

INT1 RfMgmtCliTxPowerTrapStatus PROTO((tCliHandle, UINT4));

INT1 RfMgmtCliShowTxPowerTrapStatus PROTO((tCliHandle));

INT1 RfMgmtCliShowChannelTrapStatus PROTO((tCliHandle));

INT1 RfMgmtCliChannelChangeTrapStatus PROTO((tCliHandle, UINT4));

INT4 RfMgmtShowAdvancedChannel PROTO((tCliHandle, INT4));

INT4 RfMgmtShowAdvancedSha PROTO((tCliHandle, INT4));

INT4 RfMgmtShowAdvancedCoverage PROTO((tCliHandle, INT4));

INT4 RfMgmtShowAdvancedTxPower PROTO((tCliHandle, INT4));

INT1 RfMgmtCliTpcStats PROTO((tCliHandle, INT4, UINT1 *, UINT4 *));

INT1 RfMgmtCliNeighborAPConfig PROTO((tCliHandle, INT4, UINT1 *, UINT4 *));
 
INT1 RfMgmtCliShowChannelSwitchStatus PROTO ((tCliHandle));

INT1 RfMgmtCliShowBssidSNRScanStatus PROTO ((tCliHandle, INT4, UINT1 *, UINT4, UINT4));
 
INT1 RfMgmtCliBssidSNRScanStatus PROTO((tCliHandle, INT4, UINT4, UINT1 *, UINT4, UINT4));

INT4 RfMgmtCliDisableDebug PROTO((tCliHandle, INT4));

INT4 RfMgmtCliEnableDebug PROTO((tCliHandle, INT4));

INT4 RfMgmtGetDot11RadioIfIndex PROTO((UINT4, UINT4, INT4, INT4 *));
INT4 RfMgmtGetRadioIfIndex PROTO((UINT4, UINT4, INT4, INT4 *));

INT4 RfmgmtShowRunningConfig PROTO ((tCliHandle CliHandle));

INT4 RfmShowRunningRrmExtConfigTable PROTO ((tCliHandle CliHandle));

INT4 RfMgmtShowFailedAPNeighbors PROTO ((tCliHandle, INT4, UINT1 *));

INT4 RfMgmtCliClearNeighborDetails PROTO ((tCliHandle, INT4));

INT4 RfMgmtCliExternalAPStatus PROTO ((tCliHandle, INT4, UINT4));

INT1 RfMgmtCliNeighborCountThreshold PROTO ((tCliHandle, INT4, UINT4));


INT4 RfMgmtUTProcess PROTO ((tCliHandle, INT4 ));

#ifdef ROGUEAP_WANTED
INT4
RfmShowRunningRogueRuleConfigTable PROTO ((tCliHandle));

INT4
RfmgmtShowRunningRogueApScalars PROTO ((tCliHandle CliHandle));

INT4
RfmShowRunningRogueManualConfigTable PROTO ((tCliHandle));

INT4
RfmShowRunningRogueogueRuleConfigTable PROTO ((tCliHandle));

INT1 nmhGetFsRogueApLearntFrom PROTO ((tMacAddr,tMacAddr *));



INT4 RogueSetRfGroupName PROTO ((tCliHandle, UINT1 *));

INT4 RogueClassifyAP PROTO((tCliHandle, UINT1, UINT1, tMacAddr));

INT4
RogueDeleteClassAP PROTO ((tCliHandle, UINT1, UINT1));

INT4
RogueAddDeleteAP PROTO ((tCliHandle, UINT1, UINT1, UINT1, tMacAddr));

INT4
RogueAddRule PROTO ((tCliHandle, UINT1, UINT1, UINT1, UINT1 *));

INT4
RogueClassifyRule PROTO ((tCliHandle, UINT1, UINT1, UINT1 *));

INT4
RogueRuleFunc PROTO ((tCliHandle, UINT1, UINT1 *));

INT4
RogueSetRuleConditionClient PROTO ((tCliHandle, UINT4, UINT1 *));

INT4
RogueSetRuleConditionDuration PROTO ((tCliHandle, UINT4, UINT1 *));

INT4
RogueSetRuleConditionEncrypt PROTO ((tCliHandle, UINT1, UINT1 *));

INT4
RogueSetRuleConditionRssi PROTO ((tCliHandle, INT1, UINT1 *));

INT4
RogueSetRuleConditionTpc PROTO ((tCliHandle, UINT1, UINT1 *));

INT4
RogueSetRuleConditionSsid PROTO ((tCliHandle, UINT1 *, UINT1 *));

INT4
RogueDeleteRuleConditionClient PROTO ((tCliHandle, UINT1 *));

INT4
RogueDeleteRuleConditionDuration PROTO ((tCliHandle, UINT1 *));

INT4
RogueDeleteRuleConditionEncrypt PROTO ((tCliHandle, UINT1 *));

INT4
RogueDeleteRuleConditionRssi PROTO ((tCliHandle, UINT1 *));

INT4
RogueDeleteRuleConditionTpc PROTO ((tCliHandle, UINT1 *));

INT4
RogueDeleteRuleConditionSsid PROTO ((tCliHandle, UINT1 *));

INT4
RogueSetRogueDetection PROTO ((tCliHandle, UINT1));

INT4
RogueShowDetailedRogueAp PROTO ((tCliHandle, tMacAddr));

INT4
RogueShowSummaryRogueAp PROTO ((tCliHandle, UINT1));

INT4
RogueShowSummaryRogueRule PROTO ((tCliHandle));

INT4
RogueShowFullSummaryRogueAp PROTO((tCliHandle));

INT4
RogueShowDetailedRogueRule PROTO ((tCliHandle, UINT1 *));

INT4
RogueTrapStatusSet PROTO ((tCliHandle, UINT1));

INT4
RogueShowStatus PROTO ((tCliHandle));

INT4
RogueSetTimeout PROTO ((tCliHandle, UINT4));
#endif 

enum {
 CLI_RFMGMT_CREATE_RADIO = 1,
 CLI_RFMGMT_DELETE_RADIO,
 CLI_RFMGMT_TPC_SELECTION,
 CLI_RFMGMT_DCA_SELECTION,
 CLI_RFMGMT_DCA_SENSITIVITY,
 CLI_RFMGMT_DCA_INTERVAL,
 CLI_RFMGMT_TPC_INTERVAL,
 CLI_RFMGMT_DCA_CHANNEL_UPDATE,
 CLI_RFMGMT_TPC_UPDATE,
 CLI_RFMGMT_DCA_CLIENT_THRESHOLD,
 CLI_RFMGMT_DEBUG,
 CLI_RFMGMT_NO_DEBUG,
 CLI_RFMGMT_SHOW_11H_TPC_INFO,
 CLI_RFMGMT_SHOW_11H_DFS_INFO, 
 CLI_RFMGMT_SHOW_AP_AUTORF,
 CLI_RFMGMT_SHOW_AP_ALLOWEDCHANNEL,
 CLI_RFMGMT_TPC_SNR_THRESHOLD,
 CLI_RFMGMT_DCA_CHANNEL_STATUS,
 CLI_RFMGMT_DCA_AUTO_SCAN_STATUS,
 CLI_RFMGMT_DCA_SCAN_FREQUENCY,
 CLI_RFMGMT_DCA_CHANNEL_SCAN_DURATION,
 CLI_RFMGMT_DCA_CHANNEL_NEIGHBOR_AGING,
 CLI_RFMGMT_BSSID_SNR_SCAN,
 CLI_RFMGMT_CHANNEL_TRAP_STATUS,
 CLI_RFMGMT_TX_POWER_TRAP_STATUS,
 CLI_RFMGMT_CLIENT_SNR_SCAN_STATUS,
 CLI_RFMGMT_CLIENT_SNR_SCAN_PERIOD,
 CLI_RFMGMT_DCA_RSSI_THRESHOLD,
 CLI_RFMGMT_POWER_THRESHOLD,
 CLI_RFMGMT_CHANNEL_SWITCH_STATUS,
 CLI_RFMGMT_SHOW_CHANNEL,
 CLI_RFMGMT_SHOW_SHA,
 CLI_RFMGMT_SHOW_COVERAGE,
 CLI_RFMGMT_SHOW_TXPOWER,
 CLI_RFMGMT_SHOW_TPC_STATS,
 CLI_RFMGMT_SHOW_NEIGHBOR_AP_CONFIG,
 CLI_RFMGMT_SHOW_CHANNEL_SWITCH_STATUS,
 CLI_RFMGMT_SHOW_SSID_SCAN_STATUS,
 CLI_RFMGMT_SHOW_TX_POWER_TRAP_STATUS,
 CLI_RFMGMT_SHOW_CHANNEL_TRAP_STATUS,
#ifdef ROGUEAP_WANTED
 CLI_ROGUE_RFNAME,
 CLI_ROGUE_AP_CLASSIFY,
 CLI_ROGUE_AP_DELETE_CLASS,
 CLI_ROGUE_AP_ADD_DELETE,
 CLI_ROGUE_RULE_ADD_AP,
 CLI_ROGUE_RULE_CLASSIFY,
 CLI_ROGUE_RULE_FUNC,
 CLI_ROGUE_RULE_CONDITION_SET,
 CLI_ROGUE_RULE_CONDITION_DELETE,
 CLI_ROGUE_DETECTION,
 CLI_RFMGMT_ROGUE_TRAP_STATUS,
 CLI_ROGUE_TIMEOUT,
 CLI_SHOW_ROGUE_AP_STATUS,
 CLI_SHOW_ROGUE_AP,
 CLI_SHOW_ROGUE_RULE,
#endif
 CLI_RFMGMT_SHA_INTERVAL,
 CLI_RFMGMT_SHA_STATUS,
 CLI_RFMGMT_SHOW_FAILED_AP_NEIGBHOR,
 CLI_RFMGMT_11H_TPC_STATUS,
 CLI_RFMGMT_11H_TPC_INTERVAL,
 CLI_RFMGMT_11H_TPC_REQUEST_INTERVAL,
 CLI_RFMGMT_11H_TPC_MIN_LINK_THRESHOLD,
 CLI_RFMGMT_11H_TPC_MAX_LINK_THRESHOLD,
 CLI_RFMGMT_11H_TPC_STA_COUNT_THRESHOLD,
 CLI_RFMGMT_DFS_CHANNEL_SWITCH_STATUS, 
 CLI_RFMGMT_11H_DFS_STATUS,
 CLI_RFMGMT_11H_DFS_INTERVAL, 
 CLI_RFMGMT_11H_DFS_QUIET_INTERVAL,                
 CLI_RFMGMT_11H_DFS_QUIET_PERIOD, 
 CLI_RFMGMT_11H_DFS_MEASUREMENT_INTERVAL,
 CLI_RFMGMT_EXTERNAL_APS_STATUS,
 CLI_RFMGMT_CLEAR_NEIGHBOR_INFO,
 CLI_RFMGMT_NEIGHBOR_COUNT_THRESHOLD
};

#if defined  (__RFMGMTCLI_C__)
CONST CHR1 *gapRfMgmtCliErrString[] = {
    "Invalid value received\r\n",
    "Invalid value configuration\r\n",
    "Failed to create row status\r\n",
    NULL
   
};
#else

PUBLIC CHR1 *gapRfMgmtCliErrString[];
#endif

enum
{
    CLI_INVALID_VALUE,
    CLI_INCONSISTENT_VALUE,
    CLI_ROW_CREATION_FAIL,
    CLI_RFMGMT_MAX_ERR 
};


#define CLI_RADIO_TYPEA    2
#define CLI_RADIO_TYPEB    1
#define CLI_TPC_MODE_GLOBAL   1
#define CLI_TPC_MODE_PER_AP   2
#define CLI_TPC_SELECTION_AUTO   1
#define CLI_TPC_SELECTION_ONCE   2
#define CLI_TPC_SELECTION_OFF   3
#define CLI_TPC_PER_AP_GLOBAL   1
#define CLI_TPC_PER_AP_MANUAL   2
#define CLI_DCA_MODE_GLOBAL   1
#define CLI_DCA_MODE_PER_AP   2
#define CLI_DCA_SELECTION_AUTO   1
#define CLI_DCA_SELECTION_ONCE   2
#define CLI_DCA_SELECTION_OFF   3
#define CLI_DCA_PER_AP_GLOBAL   1
#define CLI_DCA_PER_AP_MANUAL   2
#define CLI_DCA_SENSITIVITY_LOW       1
#define CLI_DCA_SENSITIVITY_MEDIUM    2
#define CLI_DCA_SENSITIVITY_HIGH      3
#define CLI_DCA_ADD_CHANNEL    1
#define CLI_DCA_DELETE_CHANNEL   2
#define CLI_RFMGMT_DCA_AUTO_SCAN_ENABLE  1
#define CLI_RFMGMT_DCA_AUTO_SCAN_DISABLE 2
#define CLI_RFMGMT_SHA_SCAN_ENABLE  1
#define CLI_RFMGMT_SHA_SCAN_DISABLE 2
#define CLI_RFMGMT_SNR_AUTO_SCAN_ENABLE  1
#define CLI_RFMGMT_SNR_AUTO_SCAN_DISABLE 2
#define CLI_RFMGMT_CHANNEL_CHANGE_TRAP_ENABLE 1
#define CLI_RFMGMT_CHANNEL_CHANGE_TRAP_DISABLE  2
#define CLI_RFMGMT_TX_POWER_CHANGE_TRAP_ENABLE 1
#define CLI_RFMGMT_TX_POWER_CHANGE_TRAP_DISABLE 2
#define CLI_RFMGMT_CLIENT_SNR_SCAN_ENABLE 1
#define CLI_RFMGMT_CLIENT_SNR_SCAN_DISABLE 2
#define CLI_RFMGMT_CHANNEL_SWITCH_ENABLE 1
#define CLI_RFMGMT_CHANNEL_SWITCH_DISABLE 2
#define CLI_RFMGMT_SSID_SCAN_ENABLE         1
#define CLI_RFMGMT_SSID_SCAN_DISABLE      2
#define CLI_RFMGMT_11H_TPC_ENABLE   1
#define CLI_RFMGMT_11H_TPC_DISABLE  2
#ifdef ROGUEAP_WANTED
#define CLI_ROGUE_TRAP_ENABLE 1
#define CLI_ROGUE_TRAP_DISABLE 2
#define CLI_ROGUE_DETECTION_ENABLE  1
#define CLI_ROGUE_DETECTION_DISABLE 2
#define CLI_ENCRYPTION_OPEN 0
#define CLI_ENCRYPTION_WPA 1
#define CLI_ENCRYPTION_DELETE 2
#define CLI_RULE_FUNC_ENABLE 1
#define CLI_RULE_FUNC_DISABLE 0
#define CLI_RULE_FUNC_DELETE 2
#define CLI_AP_CLASS_FRIENDLY 2
#define CLI_AP_CLASS_MALICIOUS 3
#define CLI_AP_CLASS_DETAILED 4
#define CLI_AP_STATE_NONE 1
#define CLI_AP_STATE_ALERT 2
#define CLI_AP_STATE_CONTAIN 3
#define CLI_AP_FUNC_ADD 1
#define CLI_AP_FUNC_DELETE 2
#define CLI_AP_CLASS_UNCLASSIFIED 1
#define CLI_DEFAULT_PROTECTION 2
#define CLI_DEFAULT_RSSI -129 
#define CLI_DEFAULT_CLIENT_COUNT 11
#define CLI_DEFAULT_DURATION 3601
#endif 
#define CLI_RFMGMT_11H_DFS_ENABLE   1
#define CLI_RFMGMT_11H_DFS_DISABLE  2
#define CLI_RFMGMT_CONSIDER_EXT_APS  1
#define CLI_RFMGMT_IGNORE_EXT_APS  2

#define CLI_MAC_TO_STR_LEN          21
#define RFMGMT_CLI_MAX_ARGS         20

#endif
