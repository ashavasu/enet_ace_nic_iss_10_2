/*  $Id: vrrpcli.h,v 1.20 2017/12/16 11:57:10 siva Exp $ */

#ifndef __VRRP_CLI_H__
#define __VRRP_CLI_H__ 
#include "cli.h"
enum
{
  VRRP_CLI_START = 1,
  VRRP_CLI_NO_START,
  VRRP_CLI_VRRP_IF,
  VRRP_CLI_NO_VRRP_IF, 
  VRRP_CLI_IP,
  VRRP_CLI_NO_IP,
  VRRP_CLI_PRIORITY,
  VRRP_CLI_NO_PRIORITY,
  VRRP_CLI_PREEMPT,
  VRRP_CLI_NO_PREEMPT,
  VRRP_CLI_AUTH,
  VRRP_CLI_NO_AUTH,
  VRRP_CLI_AUTH_DEPRECATE,
  VRRP_CLI_TIMER,
  VRRP_CLI_NO_TIMER,
  VRRP_CLI_MSEC_TIMER,
  VRRP_CLI_SHOW,
  VRRP_CLI_SHOW_TRACK,
  VRRP_CLI_TRACE,
  VRRP_CLI_NO_TRACE,
  VRRP_CLI_IPV6,
  VRRP_CLI_NO_IPV6,
  VRRP_CLI_SET_VERSION,
  VRRP_CLI_ACCEPT,
  VRRP_CLI_OPER_TRACK,
  VRRP_CLI_NO_OPER_TRACK,
  VRRP_CLI_TRACK_GROUP,
  VRRP_CLI_NO_TRACK_GROUP,
  VRRP_CLI_TRACK_LINKS,
  VRRP_CLI_NO_TRACK_LINKS
};

/* max no of variable inputs to cli parser */
#define VRRP_CLI_MAX_ARGS       10
#define VRRP_CLI_BRIEF          1
#define VRRP_CLI_DETAIL         2
#define VRRP_CLI_STAT           3
#define VRRP_CLI_STAT_ALL       4

#define VRRP_CLI_PRIMARY          1
#define VRRP_CLI_SECONDARY        2
#define VRRP_CLI_DELETE_VRID      3
#define VRRP_CLI_AUTOCONFIG       3
#define VRRP_MAX_VRID_SUPP        128


#define VRRP_CLI_VERSION_2               1
#define VRRP_CLI_VERSION_2_3             2
#define VRRP_CLI_VERSION_3               3

#define VRRP_CLI_FMLY_IPV4               1
#define VRRP_CLI_FMLY_IPV6               2

# define VRRP_CLI_AUTHDEPRECATE_ENABLE  1
# define VRRP_CLI_AUTHDEPRECATE_DISABLE 2

# define VRRP_CLI_ACCEPT_ENABLE  1
# define VRRP_CLI_ACCEPT_DISABLE 2

/* flags for the VRRP trace call  */
#define   VRRP_CLI_TRC_NONE                 (0x00000000)
#define   VRRP_CLI_TRC_ALL                  (0x0000ffff)
#define   VRRP_CLI_TRC_PKT                  (0x00000001)
#define   VRRP_CLI_TRC_EVENTS               (0x00000002)
#define   VRRP_CLI_TRC_INIT                 (0x00000004)
#define   VRRP_CLI_TRC_TIMERS               (0x00000008)
#define   VRRP_CLI_TRC_ALL_FAILURES         (0x00000010)
#define   VRRP_CLI_TRC_MEMORY               (0x00000020)
#define   VRRP_CLI_TRC_BUFFER               (0x00000040)
#define   VRRP_CLI_TRC_VERSION_2            (0x00000080)
#define   VRRP_CLI_TRC_VERSION_3            (0x00000100)
#define   VRRP_CLI_TRC_STATE                (0x00000200)


/*Error codes 
 * Error code values and strings are maintained inside the protocol module itself.
 */
enum {
    CLI_VRRP_NO_VR_ENTRY = 1,
    CLI_VRRP_VR_ACTIVE_OR_DOWN,
    CLI_VRRP_DUP_VR_ENTRY,
    CLI_VRRP_INVALID_PRIORITY,
    CLI_VRRP_INVALID_PREEMPT,
    CLI_VRRP_INVALID_ACCEPT,
    CLI_VRRP_NO_ASSOC_IP,
    CLI_VRRP_MORE_ASSOC_IP,
    CLI_VRRP_INVALID_IFINDEX,
    CLI_VRRP_INVALID_VRID,
    CLI_VRRP_INVALID_ADDR_TYPE,
    CLI_VRRP_INVALID_ADMIN_STATE,
    CLI_VRRP_INVALID_ROW_STATUS,
    CLI_VRRP_INVALID_AUTH_TYPE,
    CLI_VRRP_AUTH_KEY_FOR_NO_AUTH,
    CLI_VRRP_INVALID_AUTH_KEY_LEN,
    CLI_VRRP_INVALID_PROTOCOL,
    CLI_VRRP_INVALID_VALUE,
    CLI_VRRP_KEY_TOO_LONG,
    CLI_VRRP_INVALID_IFINDEX_VRID,
    CLI_VRRP_SET_VRIP,
    CLI_VRRP_UNNUM_IF,
    CLI_VRRP_MAX_ASSOC_PER_VRID,
    CLI_VRRP_NO_PRIMARY_OVERRIDE,
    CLI_VRRP_SET_AUTHDEPRECATE_FLAG,
    CLI_VRRP_INV_TRC,
    CLI_VRRP_INVALID_IP,
    CLI_VRRP_NOT_LINK_LOCAL_IP,
    CLI_VRRP_DOWNGRADE_VERSION_ERROR,
    CLI_VRRP_INVALID_VERSION,
    CLI_VRRP_VERSION_FETCH_ERROR,
    CLI_VRRP_IPV6_NO_SUPPORT,
    CLI_VRRP_ACCEPT_NO_SUPPORT,
    CLI_VRRP_REG_WITH_IP_ERROR,
    CLI_VRRP_DEREG_WITH_IP_ERROR,
    CLI_VRRP_IP_NOT_FOUND,
    CLI_VRRP_ADVT_INT_V2_ERROR,
    CLI_VRRP_ADVT_INT_V3_ERROR,
    CLI_VRRP_ADVT_INT_MSEC_V2_ERROR,
    CLI_VRRP_NO_TRACK_GROUP,
    CLI_VRRP_NO_VR_IN_IF,
    CLI_VRRP_DUP_ASSO_ENTRY,
    CLI_VRRP_NO_ASSOC_ENTRY,
    CLI_VRRP_DEL_ASSOC_WITH_ACTIVE_VR,
    CLI_VRRP_ACTIVE_TRACK_GROUP,
    CLI_VRRP_DUP_TRACK_GROUP,
    CLI_VRRP_INVALID_LINKS,
    CLI_VRRP_ACTIVE_TRACK_WITH_NO_IF,
    CLI_VRRP_DEL_TRACK_WITH_MORE_VR,
    CLI_VRRP_DEL_TRACK_WITH_MORE_IF,
    CLI_VRRP_INVALID_GROUP,
    CLI_VRRP_DUP_TRACK_IF,
    CLI_VRRP_NO_TRACK_IF,
    CLI_VRRP_DEL_TRACK_IF_WITH_MORE_VR,
    CLI_VRRP_INVALID_NOTIF,
    CLI_VRRP_INVALID_RTR_STATUS,
    CLI_VRRP_MAX_VR,
    CLI_VRRP_MAX_ASSOC_IP,
    CLI_VRRP_MAX_TRACK_GROUP,
    CLI_VRRP_MAX_TRACK_IF,
    CLI_VRRP_MAX_AUTH,
    CLI_VRRP_MAX_TX_BUF,
    CLI_VRRP_ADD_OPER,
    CLI_VRRP_JOIN_MCAST,
    CLI_VRRP_DEL_PRIMARY_THRO_SEC,
    CLI_VRRP_DEL_SEC_THRO_PRIMARY,
    CLI_VRRP_DUPLICATE_SECONDARY_IP,
    CLI_VRRP_INVALID_NETWORK,
    CLI_VRRP_VERSION_V2_V3_ERR,
    CLI_VRRP_DUPLICATE_PRIMARY_IP,
    CLI_VRRP_DEL_PRIMARY_IP,
    CLI_VRRP_PRIMARY_IP_DOWN,
    CLI_VRRP_INCORRECT_IFINDEX,
    CLI_VRRP_INVALID_DEC_PRIORITY,
    CLI_VRRP_INCONS_PRIORITY,
    CLI_VRRP_DEC_PRIO_NO_IP_ADD,
    CLI_VRRP_DEC_PRIO_NO_IP_DEL,
    CLI_VRRP_DEC_PRIO_NO_IP_MODIFY,
    CLI_VRRP_EXCEED_MAX_VRID,
    CLI_VRRP_IPOWNER_ERR,
    CLI_VRRP_MAX_ERR 
};

#ifdef __VRRPCLI_C__
CONST CHR1  *VrrpCliErrString [] = {
    NULL,
    "\r! No Such Virtual Router Entry \r\n",
    "\r% Virtual Entry Status Should be Active and Down\r\n",
    "\r% Virtual Router Entry already present\r\n",
    "\r% Priority should be in the range 1-254\r\n",
    "\r% Preempt Mode can only be enabled(1) or disabled(2)\r\n",
    "\r% Accept Mode can only be enabled(1) or disable(2)\r\n",
    "\r% Virtual Router Entry can't be activated with Associated IP Address Entries\r\n",
    "\r% Virtual Router Entry can't be destroy with Associated IP Address Entries\r\n",
    "\r% Invalid Interface Index\r\n",
    "\r% VRID should be in the range 1-255\r\n",
    "\r% Address Type can only be IPv4(1) or IPv6(2)\r\n",
    "\r% Admin State can only be Up(1) or Down(2)\r\n",
    "\r% Invalid Row Status Value\r\n",
    "\r% Authentication Type can only be NoAuth(1), SimpleText(2) or HMAC(3)\r\n",
    "\r% Authentication Key not applicable for NoAuth(1) Authentication type\r\n",
    "\r% Authentication Key Length is incorrect\r\n",
    "\r% Protocol can only be between 1 and 4\r\n",
    "\r% Invalid Value \r\n",
    "\r% Authentication Key is too long \r\n",
    "\r% Invalid Interface or Virtual Router ID \r\n",
    "\r% Primary IP Should be Configured first for VRID \r\n",
    "\r% IP Address is not configured for the Interface  \r\n",
    "\r% Maximum Associated Addresses reached for this Oper Entry\r\n",
    "\r% Primary IpAddress can not be deleted \r\n",
    "\r% Vrrp AuthDeprecate flag should be disabled \r\n",
    "\r% Invalid Trace Level\r\n",
    "\r% Incorrect IP Address\r\n",
    "\r% Primary IPv6 Address in VRRP should be Link Local IP\r\n",
    "\r% Downgrading version is not allowed\r\n",
    "\r% Command Not applicable in configured version\r\n",
    "\r% Version Not Set\r\n",
    "\r% IPv6 Not Supported in Version 2\r\n",
    "\r% Accept mode not supported in Version 2\r\n",
    "\r% Registration with IP Failed\r\n",
    "\r% Deregistration with IP Failed\r\n",
    "\r% No Such IP Address Configured\r\n",
    "\r% Interval should be in the range (1-255) seconds In Version 2\r\n",
    "\r% Interval should be in the range (1-40secs)/ (10-40950 milliseconds) in Version 3\r\n",
    "\r% Interval should be in the range (100-255000) milliseconds in Version 2\r\n",
    "\r% No Such Track Group\r\n",
    "\r% No Virtual Router Entries created in this interface\r\n",
    "\r% Associated IP Address Entry already present\r\n",
    "\r% No Associated IP Address Entry\r\n",
    "\r% Last Associated IP Address Entry can't be deleted with active Virtual Router Entry\r\n",
    "\r% Object cannot be set with Track Group being Active\r\n",
    "\r% Track Group Entry already present\r\n",
    "\r% Tracked Links count cannot exceed tracked interfaces created\r\n",
    "\r% Tracked Group cannot be activated with no tracked interfaces\r\n",
    "\r% Tracked Group cannot be deleted with more Virtual Router Entries attached\r\n",
    "\r% Tracked Group cannot be deleted with more tracked interfaces mapped\r\n",
    "\r% Invalid Group Index Value\r\n",
    "\r% Tracked Group Interface already present\r\n",
    "\r% No such Tracked Group interface\r\n",
    "\r% Last Tracked Group Interface cannot be deleted with more Virtual Router Entries attached\r\n",
    "\r% Notification Control can only be enabled(1) or disabled(2)\r\n",
    "\r% Router Status can only be enabled(1) or disabled(2)\r\n",
    "\r% Maximum Virtual Router Entries Reached\r\n",
    "\r% Maximum Associated IP Entries reached\r\n",
    "\r% Maximum Track Group Entries reached\r\n",
    "\r% Maximum Tracked Group Interfaces reached\r\n",
    "\r% Maximum Authentication Keys reached\r\n",
    "\r% Maximum VRRP Tx Buffer Entries reached\r\n",
    "\r% VRRP Addition of Oper Table failed\r\n",
    "\r% Joining Multicast Group failed\r\n",
    "\r% Deleting an IP Address configured as primary through secondary keyword is not allowed\r\n",
    "\r% Deleting an IP Address configured as secondary through primary keyword is not allowed\r\n",
    "\r% Primary IP conflicts with existing secondary IP Address\r\n",
    "\r% Entered IP Address is not valid in the Primary/Secondary interface network\r\n",
    "\r! In version v2-v3, configurations are not allowed\r\n",
    "\r% Secondary IP conflicts with existing Primary IP Address\r\n",
    "\r!Warning: Deleting the Primary IP with more secondary IP addresses\r\n",
    "\r% Cannot add Secondary IP Address when Primary IP Address is not configured\r\n",
    "\r% Configuration can be accepted only over L3 VLAN or Router Port\r\n",
    "\r% Decrement Priority should be less than Current Priority\r\n",
    "\r% Priority should be greater than or equal to configured decrement priority\r\n",
    "\r% Priority should be greater than or equal to configured decrement priority - IP Addition not allowed\r\n",
    "\r% Priority should be greater than or equal to configured decrement priority - IP Deletion not allowed\r\n",
    "\r% Priority should be greater than or equal to configured decrement priority - IP Modify not allowed\r\n",
    "\r% Exceeded Maximum Virtual Router Instance, Reuse the Existing VRID\r\n",
    "\r% Tracking not supported on IP address owner\r\n",
    "\r% Invalid\r\n"
};
#endif

#define CLI_VRRP "config-vrrp"
#define CLI_VRRP_IF "config-vrrp-if"

INT4 VrrpShowRunningConfig (tCliHandle CliHandle);
VOID IssVrrpShowDebugging (tCliHandle CliHandle);

INT4 cli_process_vrrp_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Comamnd, ...));
INT4 cli_process_vrrp_test_cmd (tCliHandle CliHandle, ...);
INT1 VrrpGetVrrpCfgPrompt PROTO ((INT1 *, INT1 *));
INT1 VrrpGetVrrpIfCfgPrompt PROTO ((INT1 *, INT1 *));
INT1 VrrpStatsVrrpSemFail PROTO ((INT4, INT4, UINT4 *));
INT1 VrrpStatsVrrpSendNDNeighborAdvtFail PROTO ((INT4, INT4, UINT4 *));
INT1 VrrpStatsVrrpSendPacketToIpv4Fail PROTO ((INT4, INT4, UINT4 *));
INT1 VrrpStatsVrrpGetIpv4PortFromOperIndexFail PROTO ((INT4, INT4, UINT4 *));
INT1 VrrpStatsVrrpSendGratArpFail PROTO ((INT4, INT4, UINT4 *));
INT1 VrrpStatsEthRegisterMacAddrFail PROTO ((INT4, INT4, UINT4 *));
INT1 VrrpStatsVrrpNetIpv4DeleteIntfFail PROTO ((INT4, INT4, UINT4 *));
INT1 VrrpStatsVrrpIpifJoinMcastGroupFail PROTO ((INT4, INT4, UINT4 *));
INT1 VrrpStatsVrrpIpifLeaveMcastGroupFail PROTO ((INT4, INT4, UINT4 *));
INT1 VrrpStatsVrrpSocketFail PROTO ((INT4, INT4, UINT4 *));
INT1 VrrpStatsPktProcessValidateAdverPacketFail PROTO ((INT4, INT4, UINT4 *));
INT1 VrrpStatsPktProcessValidateAdverV2PacketFail PROTO ((INT4, INT4, UINT4 *));
INT1 VrrpStatsPktProcessValidateAdverV3PacketFail PROTO ((INT4, INT4, UINT4 *));
INT1 VrrpStatsPktProcessTransmitV2PacketFail PROTO ((INT4, INT4, UINT4 *));
INT1 VrrpStatsPktProcessTransmitV3PacketFail PROTO ((INT4, INT4, UINT4 *));
INT1 Vrrpv3StatsVrrpSemFail PROTO ((INT4, INT4, INT4, UINT4 *));
INT1 Vrrpv3StatsVrrpSendNDNeighborAdvtFail PROTO ((INT4, INT4, INT4, UINT4 *));
INT1 Vrrpv3StatsVrrpSendPacketToIpv4Fail PROTO ((INT4, INT4, INT4, UINT4 *));
INT1 Vrrpv3StatsVrrpSendPacketToIpv6Fail PROTO ((INT4, INT4, INT4, UINT4 *));
INT1 Vrrpv3StatsVrrpGetIpv4PortFromOperIndexFail PROTO ((INT4, INT4, INT4, UINT4 *));
INT1 Vrrpv3StatsVrrpGetIpv6PortFromOperIndexFail PROTO ((INT4, INT4, INT4, UINT4 *));
INT1 Vrrpv3StatsVrrpSendGratArpFail PROTO ((INT4, INT4, INT4, UINT4 *));
INT1 Vrrpv3StatsEthRegisterMacAddrFail PROTO ((INT4, INT4, INT4, UINT4 *));
INT1 Vrrpv3StatsVrrpNetIpv4DeleteIntfFail PROTO ((INT4, INT4, INT4, UINT4 *));
INT1 Vrrpv3StatsVrrpNetIpv6DeleteIntfFail PROTO ((INT4, INT4, INT4, UINT4 *));
INT1 Vrrpv3StatsVrrpIpifJoinMcastGroupFail PROTO ((INT4, INT4, INT4, UINT4 *));
INT1 Vrrpv3StatsVrrpIpifLeaveMcastGroupFail PROTO ((INT4, INT4, INT4, UINT4 *));
INT1 Vrrpv3StatsVrrpSocketFail PROTO ((INT4, INT4, INT4, UINT4 *));
INT1 Vrrpv3StatsPktProcessValidateAdverPacketFail PROTO ((INT4, INT4, INT4, UINT4 *));
INT1 Vrrpv3StatsPktProcessValidateAdverV2PacketFail PROTO ((INT4, INT4, INT4, UINT4 *));
INT1 Vrrpv3StatsPktProcessValidateAdverV3PacketFail PROTO ((INT4, INT4, INT4, UINT4 *));
INT1 Vrrpv3StatsPktProcessTransmitV2PacketFail PROTO ((INT4, INT4, INT4, UINT4 *));
INT1 Vrrpv3StatsPktProcessTransmitV3PacketFail PROTO ((INT4, INT4, INT4, UINT4 *));
#endif /* __VRRP_CLI_H__ */
