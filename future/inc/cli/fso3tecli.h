/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fso3tecli.h,v 1.3 2017/12/26 13:34:19 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FutOspfv3TestIfIndex[12];
extern UINT4 FutOspfv3TestDemandTraffic[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFutOspfv3TestDemandTraffic(i4FutOspfv3TestIfIndex ,i4SetValFutOspfv3TestDemandTraffic) \
 nmhSetCmn(FutOspfv3TestDemandTraffic, 12, FutOspfv3TestDemandTrafficSet, V3OspfLock, V3OspfUnLock, 0, 0, 1, "%i %i", i4FutOspfv3TestIfIndex ,i4SetValFutOspfv3TestDemandTraffic)

#endif
