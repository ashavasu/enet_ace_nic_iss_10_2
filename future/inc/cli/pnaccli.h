
/* INCLUDE FILE HEADER :
 *
 * $Id: pnaccli.h,v 1.33 2017/09/04 10:53:17 siva Exp $
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : pnaccli.h                                        |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR      : Jeeva Sethuraman                                 |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : PNAC                                             |
 * |                                                                           |
 * |  MODULE NAME           : CLI interface for PNAC configuration             |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    :                                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION           : This file contains command idenfiers for         |
 * |                          definitions in pnaccmd.def, function prototypes  |
 * |                          for PNAC CLI and corresponding error code        |
 * |                          definitions                                      |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 *
 */

#ifndef __PNACCLI_H__
#define __PNACCLI_H__

#include "cli.h"
/* PNAC CLI command constants. To add a new command ,a new definition needs to
 * be added to the list.
 */

enum
{
 CLI_PNAC_SYSTEMCONTROL = 1,
 CLI_PNAC_NO_SYSTEMCONTROL,
 CLI_PNAC_SYSTEMSHUT,
 CLI_PNAC_NO_SYSTEMSHUT,
 CLI_PNAC_AUTH_SERVER,
 CLI_PNAC_NO_AUTH_SERVER, 
 CLI_PNAC_NAS,                           
 CLI_PNAC_AUTH_SESSION_REAUTH,
 CLI_PNAC_REAUTH,
 CLI_PNAC_NO_REAUTH,
 CLI_PNAC_TIMEOUT,                      
 CLI_PNAC_NO_TIMEOUT,                      
 CLI_PNAC_AS_SETTING, 
 CLI_PNAC_NO_AS_SETTING, 
 CLI_PNAC_MAXREQ_COUNT,
 CLI_PNAC_NO_MAXREQ_COUNT,
 CLI_PNAC_PORT_DEFAULT, 
 CLI_PNAC_PORT_CONTROL_SET,
 CLI_PNAC_PORT_CONTROL_RESET,
 CLI_PNAC_DEBUG,
 CLI_PNAC_NO_DEBUG,
 CLI_SHOW_PNAC,
 CLI_PNAC_SUPPACCESS_CONTROL_SET,
 CLI_PNAC_SUPPACCESS_CONTROL_RESET,
 CLI_PNAC_CONTROL_DIRECTION_SET,
 CLI_PNAC_CONTROL_DIRECTION_RESET,
 CLI_PNAC_MAXSTART_COUNT,
 CLI_PNAC_NO_MAXSTART_COUNT,
 CLI_PNAC_AUTHMODE_SET,
 CLI_PNAC_AUTHMODE_RESET,
 CLI_PNAC_SESS_INIT,
 CLI_PNAC_SESS_REAUTH,
 CLI_PNAC_CENTRALIZED,
 CLI_PNAC_DISTRIBUTED,
 CLI_DPNAC_SYNC_TIME,
 CLI_DPNAC_SYNC_TIME_RESET,
 CLI_DPNAC_MAX_KEEP_ALIVE_COUNT,
 CLI_DPNAC_MAX_KEEP_ALIVE_COUNT_RESET,
 CLI_SHOW_D_PNAC,
 CLI_CLEAR_PNAC_STATS,
 CLI_CLEAR_PNAC_STATS_IF_INFO,
 CLI_CLEAR_PNAC_STATS_MAC_INFO,
 CLI_CLEAR_PNAC_STATS_IF_MAC_INFO,
 CLI_PNAC_PORT_AUTHSTATUS_SET,
 CLI_PNAC_MAXREAUTH_COUNT,
 CLI_PNAC_NO_MAXREAUTH_COUNT
};

#define PNAC_CLI_MAX_ARGS         7
#define PNAC_CLI_PERMISSION_ALLOW 1
#define PNAC_CLI_PERMISSION_DENY  2
#define PNAC_CLI_MAX_COMMANDS     39
#define PNAC_CLI_NAME_SIZE        21

#define PNAC_CLI_QUIET_PERIOD         1
#define PNAC_CLI_REAUTH_PERIOD        2
#define PNAC_CLI_SERVER_TIMEOUT       3
#define PNAC_CLI_SUPP_TIMEOUT         4
#define PNAC_CLI_TX_PERIOD            5
#define PNAC_CLI_START_PERIOD         6
#define PNAC_CLI_HELD_PERIOD          7
#define PNAC_CLI_AUTH_PERIOD          8
#define PNAC_CLI_DEBUG_ALL            9
#define PNAC_CLI_DEBUG_ERRORS         10
#define PNAC_CLI_DEBUG_EVENTS         11
#define PNAC_CLI_DEBUG_PACKETS        12
#define PNAC_CLI_DEBUG_STATEMC        13
#define PNAC_CLI_DEBUG_RED            14
#define PNAC_CLI_IF_INFO              15
#define PNAC_CLI_STAT_IF_INFO     16
#define PNAC_CLI_SUPPSTAT_IF_INFO     17
#define PNAC_CLI_DATABASE_INFO        18
#define PNAC_CLI_ALL_INFO             19
#define PNAC_CLI_ALL_INTF_INFO        20
#define PNAC_CLI_RED_IF_INFO          21
#define PNAC_CLI_RED_ALL_INTF_INFO    22
#define PNAC_CLI_MAC_INFO             23
#define PNAC_CLI_ALLMAC_INFO          24
#define PNAC_CLI_MAC_STATS            25
#define PNAC_CLI_ALLMAC_STATS         26
#define PNAC_CLI_DEBUG_INIT_SHUT      27
#define PNAC_CLI_DEBUG_MGMT           28


#define PNAC_CLI_FORCE_UNAUTHORIZED   1
#define PNAC_CLI_AUTO                 2
#define PNAC_CLI_FORCE_AUTHORIZED     3
#define PNAC_CLI_INACTIVE             1
#define PNAC_CLI_ACTIVE               2
#define PNAC_CLI_BOTH                 0 
#define PNAC_CLI_IN                   1 
#define PNAC_CLI_PORTBASED            1 
#define PNAC_CLI_MACBASED             2
#define PNAC_CLI_PORT_AUTH_ENABLE     1
#define PNAC_CLI_PORT_AUTH_DISABLE    2 
#define DPNAC_CLI_AUTH_SLOT_INFO      1
#define DPNAC_CLI_AUTH_ALL            2
#define DPNAC_CLI_STAT_SLOT_INFO      3
#define DPNAC_CLI_STAT_ALL            4
#define DPNAC_CLI_DETAIL              5

#define DPNAC_CLI_SYNC_TIME_RESET       1
#define DPNAC_CLI_MAX_ALIVE_COUNT_RESET 2


/* Error codes 
 * Error code values and strings are maintained inside the protocol module itself.
 */
enum {
    CLI_PNAC_NVT_ERR = 1,
    CLI_PNAC_FATAL_ERR,
    CLI_PNAC_SW_INIT_FAILED,
    CLI_PNAC_HW_INIT_FAILED,
    CLI_PNAC_TUNNEL_ERR,
    CLI_PNAC_NON_PRINTABLE,
    CLI_PNAC_AUTH_INCAPABLE,
    CLI_PNAC_LACP_ENABLED,
    CLI_PNAC_USER_NOT_EXISTS,
    CLI_PNAC_REAUTH_FAIL,
    CLI_PNAC_REAUTH_ENABLE_FAIL,
    CLI_PNAC_NOT_BOTH_CAPABLE,
    CLI_PNAC_NOT_AUTH_CAPABLE,
    CLI_PNAC_NOT_AUTO,
    CLI_SUPP_MAC_NOT_EXISTS,
    CLI_ACCESS_ACTIVE_NOT_ALLOWED,
    CLI_CNTRL_DIR_IN_NOT_ALLOWED,
    CLI_START_PERIOD_CONFIG_NOT_ALLOWED,
    CLI_HELD_PERIOD_CONFIG_NOT_ALLOWED,
    CLI_AUTH_PERIOD_CONFIG_NOT_ALLOWED,
    CLI_BASE_BRIDGE_PNAC_ENABLED,
    CLI_MAX_START_CONFIG_NOT_ALLOWED,
    CLI_PNAC_NASID_CONFIG_NOT_ALLOWED,
    CLI_PNAC_INVALID_NASID,
    CLI_PNAC_MAX_USERS_REACHED,
    CLI_DPNAC_MAC_MODE_NOT_ALLOWED,
    CLI_DPNAC_ACCESS_CTRL_NOT_ALLOWED,
    CLI_DPNAC_REMOTE_PORT_CONFIG_ERR,
    CLI_DPNAC_SLOT_INFO_NOT_EXISTS,
    CLI_PNAC_CLEAR_IF_STATISTICS_FAILED,
    CLI_PNAC_CLEAR_MAC_STATISTICS_FAILED,
    CLI_PNAC_NO_ENTRY_MAC_IF,
    CLI_SUPP_PAE_NOT_SUPPORTED_MAC_BASED,
    CLI_PNAC_NO_SUPP_CONFIG,
    CLI_PNAC_IN_NOT_ALLOWED,
    CLI_PNAC_MAX_ERR

};
/* The error strings should be places under the switch so as to avoid redifinition
 * This will be visible only in modulecli.c file
 */
#ifdef __PNACCLI_C__

CONST CHR1  *PnacCliErrString [] = {
    NULL,
    "% Invalid characters present\r\n" ,
    "% FATAL ERROR : Command FAILED \r\n",
    "% Module initialization has failed \r\n",
    "% Initializing the H/W has failed \r\n",
    "% Protocol tunnel status is enabled on some of the port(s) in the system \r\n",
    "% Non-printable characters entered \r\n",
    "% The port is not authenticator capable \r\n",
    "% When LACP is enabled the port can only be force-authorized \r\n",
    "% User doesn't exist \r\n",
    "% Unable to set reauthentication status,as the Port/Mac is not Auto-Authorized \r\n",
    "% Unable to start reauthentication ,as the Port is not Auto \r\n",
    "% The port is not both authenticator and supplicant capable\r\n",
    "% The port is not authenticator capable\r\n",
    "% The port-control should be auto for mac-based\r\n",
    "% The given supplicant mac does not exist\r\n",
    "% The access-control active configuration not allowed for mac-based port\r\n",
    "% The control dir IN configuration not allowed for mac-based port\r\n",
    "% Start period configuration is not allowed for mac-based port\r\n",
    "% Held period configuration is not allowed for mac-based port\r\n",
    "% SuppAuth period configuration is not allowed for mac-based port\r\n",
    "% PNAC Module cannot be enabled in Transparent Mode\r\n",
    "% Max-Start configuration is not allowed for mac-based port\r\n",
    "% Nas Id can be set only when pnac authentic server is remote(radius or tacacs)\r\n",
    "% Invalid Nas Id \r\n",
    "% Maximum allowable users in the local data base has been reached\r\n",
    "% Mac based Authentication configuration not allowed for Distributed PNAC\r\n",
    "% Access control Active configuration is not supported for Distributed PNAC\r\n",
    "% Port Control mode change is not allowed for Remote port\r\n",
    "% Slot Information is not present\r\n",
    "% No entry found for this Port Number \r\n",
    "% No entry found for this MAC Address \r\n",
    "% No entry found for this MAC Address and Port Number \r\n",
    "% Supplicant pae is not supported for mac-based port\r\n",
    "% Supplicant features are not supported \r\n",
    "% Configuration of control direction as IN is not supported \r\n"
};

#endif
INT4 cli_process_pnac_cmd PROTO ((tCliHandle CliHandle,UINT4, ...));
INT4
PnacSetSystemControl PROTO ((tCliHandle CliHandle, INT4 i4PnacStatus));
INT4
PnacSetAuthServer PROTO ((tCliHandle CliHandle, INT4 i4AuthMethod, INT4 i4RemAuthMethod));
INT4
PnacSetNasId PROTO ((tCliHandle CliHandle, UINT1 * iNasId));
INT4
PnacSetReAuthInit PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex));
INT4
PnacSetPortReAuth PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex,
                          UINT4 u4ReAuthStatus));
INT4
PnacSetTimers PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4TmrType,
                      UINT4 u4TmrValue));
INT4
PnacSetNoTimers PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex, INT4 i4TmrType));
INT4
PnacSetASUserInfo PROTO ((tCliHandle CliHandle, UINT1 *au1PortList,                                      UINT1 *pu1UserName , UINT1 *pu1Password,                                                       UINT4 u4Permission, UINT4 u4AuthTimeout));
INT4
PnacSetNoASUserInfo PROTO ((tCliHandle CliHandle, UINT1 *pu1UserName));
INT4
PnacSetMaxReq PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4MaxReq));
INT4
PnacSetMaxStart PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4MaxStart));
INT4
PnacSetDefaults PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex));
INT4
PnacSetDebug PROTO ((tCliHandle CliHandle, UINT4 u4Level, UINT1 u1DebugSet));
INT4
PnacShowInterfaceInfo PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex));
INT4
DPnacShowDetail PROTO ((tCliHandle CliHandle));
INT4
DPnacShowSlotDetails PROTO ((tCliHandle CliHandle, UINT4 u4SlotId));
INT4
DPnacShowSlotStats PROTO ((tCliHandle CliHandle, UINT4 u4SlotId));
INT4
PnacShowAuthInterfaceStats PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex));
INT4
PnacShowSuppInterfaceStats PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex));
INT4
PnacShowGlobalInfo PROTO ((tCliHandle CliHandle));
INT4
PnacShowDatabaseInfo PROTO ((tCliHandle CliHandle));
INT4 
PnacSetPortControl PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex,INT4 i4PortControl));
INT4 
PnacSetSuppAccessCtrlWithAuth PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex,
                                      INT4 i4SuppAccessCtrlWithAuth));
INT4
PnacSetPortControlDirection PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex,
                                    INT4 i4PortControlDir));
INT4
PnacSetPortAuthMode PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex,
                            INT4 i4PortAuthMode));
INT4
PnacSessionInitialise PROTO ((tCliHandle CliHandle, UINT1* pu1MacAddr, 
                              UINT1 u1Status));
INT4
PnacSessionReauth PROTO ((tCliHandle CliHandle, UINT1* pu1MacAddr, 
                          UINT1 u1Status));
INT4
PnacShowMacInfo PROTO ((tCliHandle CliHandle, tMacAddr SrcMacAddr));
INT4
PnacShowAllMacInfo PROTO ((tCliHandle CliHandle));
INT4
PnacOctetToIfName (tCliHandle CliHandle, UINT1 *pu1Octet, UINT4 u4OctetLen,
                   UINT4 u4MaxPorts);
INT4
PnacSetSystemShut(tCliHandle CliHandle, INT4 i4PnacStatus);

INT4
PnacSetSystemMode(tCliHandle CliHandle, INT4 i4PnacMode);

INT4
DPnacSetPeriodicSyncTime (tCliHandle CliHandle, UINT4 u4DPnacPeriodicSyncTime);

INT4
DPnacSetMaxAliveCount (tCliHandle CliHandle, INT4 i4DPnacMaxAliveCount);

VOID IssPnacShowDebugging (tCliHandle);
INT4 PnacShowRunningConfig(tCliHandle CliHandle, UINT4 u4Module);
INT4 PnacShowRunningConfigInterfaceDetails(tCliHandle CliHandle, INT4 i4Index);
INT4 PnacShowRunningConfigInterface(tCliHandle CliHandle);
INT4 PnacShowRunningConfigScalars(tCliHandle CliHandle);
INT4 PnacShowRunningConfigTables(tCliHandle CliHandle);
VOID PnacShowAuthSmState (tCliHandle CliHandle, INT4 i4AuthSMState);
INT4 PnacShowMacStats (tCliHandle CliHandle, UINT1* pSrcMacAddr);
INT4 PnacShowAllMacStats (tCliHandle CliHandle);
INT4 PnacClearStatsIfInfo (tCliHandle CliHandle,UINT4 u4IfIndex);
INT4 PnacClearStatsIfMacInfo(tCliHandle CliHandle,UINT4 u4IfIndex,UINT1 *pu1MacAddr);
INT4 PnacSetPortAuthStatus (tCliHandle CliHandle,UINT4 u4IfIndex,INT4 i4PortAuthStatus);
INT4 PnacClearAllStats(tCliHandle CliHandle);
INT4 PnacClearStatsMacInfo (tCliHandle CliHandle,UINT1 *pu1MacAddr);
INT4 PnacSetPortAuthReAuthMax (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4PnacPaeAuthReAuthMax);
#endif


