/*************************************************************************
 * * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * * $Id: elmcli.h,v 1.4 2012/08/14 08:52:30 siva Exp $
 * * Description: This header file contains all the CLI related macros
 * *              and prototypes for ELMI Module.
 * ****************************************************************************/
#ifndef __ELMCLI_H__
#define __ELMCLI_H__

/* STP CLI Command constants */

# include "cli.h"

/*COMMAND IDENTIFIRS*/

enum
{
CLI_ELMI_GLOB_MOD_STATUS =1,
CLI_ELMI_NO_GLOB_MOD_STATUS,
CLI_ELMI_CLEAR_STAT,
CLI_ELMI_TRACE_ENABLE,
CLI_ELMI_TRACE_DISABLE,
CLI_ELMI_SHOW_INFO,
CLI_ELMI_PORT_PROPERTIES,
CLI_ELMI_NO_PORT_PROPERTIES,
CLI_ELMI_PORT_MOD_STATUS,
CLI_ELMI_NO_PORT_MOD_STATUS,
CLI_ELMI_CUSTOMER_MODE,
CLI_ELMI_NETWORK_MODE,
CLI_ELMI_START_UP,
CLI_ELMI_SHUT_DOWN,
CLI_ELMI_SHOW_STATUS
};

enum
{
CLI_ELMI_NO_GLOB_MOD_STATUS_ERR = 1,
CLI_ELMI_NO_PORT_ERR,
CLI_ELM_MAX_ERR
};


/* Constants defined for ELMI CLI. These values are passed as arguments from
* the def file
*/

#define ELM_CLI_MAX_ARGS           13
#define ELMI_POLLING_COUNTER  1
#define ELMI_STATUS_COUNTER  2
#define ELMI_POLLING_TIMER  3
#define ELMI_POLLING_VERIFICATION_TIMER 4
#define ELMI_EVC_PARAMETER_INFO  6
#define ELMI_EVC_STATISTICS_INFO 8
#define ELMI_STATUS_INTF                9 
#define ELMI_STATUS_ALL                 10 
#define ELMI_ALL_FLAG    11
#define ELMI_ALL_FAILURE_TRACE  12

#define ELMI_EVENT_HANDLING_TRACE 13
#define ELMI_PDU_TRACE   14

#define CLI_ELM_MAX_ERR  3

#define     ELM_MEM_TRC                     (0x1 << 16)
#define     ELM_PDU_TRC                     0x00000008
#define     ELM_EVENT_HANDLING_TRC          (0x1 << 17)
#define     ELM_TMR_TRC                     (0x1 << 18)
#define     ELM_ALL_FLAG                    131071

#define     ELM_ALL_TRC                     0x000700ff 
#define   ELM_INIT_SHUT_TRC      INIT_SHUT_TRC     
#define   ELM_MGMT_TRC           MGMT_TRC          
#define   ELM_DATA_PATH_TRC      DATA_PATH_TRC     
#define   ELM_CONTROL_PATH_TRC   CONTROL_PLANE_TRC 
#define   ELM_DUMP_TRC           DUMP_TRC          
#define   ELM_OS_RESOURCE_TRC    OS_RESOURCE_TRC   
#define   ELM_ALL_FAILURE_TRC    ALL_FAILURE_TRC   
#define   ELM_BUFFER_TRC         BUFFER_TRC        



#if defined (__ELMICLI_C__) || defined (__ELMMICLI_C__)  

CONST CHR1  *ElmCliErrString []={
        NULL,
 "\r% ELMI Module is not Enabled Globally\r",
 "\r% Port Does Not exist\r",
 };
#else
extern CONST CHR1  *ElmCliErrString [];
#endif


INT4  cli_process_elm_cmd  PROTO((tCliHandle  CliHandle,UINT4  u4Command, ...));


#endif


