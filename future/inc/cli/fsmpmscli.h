/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpmscli.h,v 1.13 2017/09/20 06:32:06 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIMstGlobalTrace[10];
extern UINT4 FsMIMstGlobalDebug[10];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIDot1sFutureMstContextId[12];
extern UINT4 FsMIMstSystemControl[12];
extern UINT4 FsMIMstModuleStatus[12];
extern UINT4 FsMIMstMaxMstInstanceNumber[12];
extern UINT4 FsMIMstMaxHopCount[12];
extern UINT4 FsMIMstCistBridgePriority[12];
extern UINT4 FsMIMstCistBridgeMaxAge[12];
extern UINT4 FsMIMstCistBridgeForwardDelay[12];
extern UINT4 FsMIMstPathCostDefaultType[12];
extern UINT4 FsMIMstTrace[12];
extern UINT4 FsMIMstDebug[12];
extern UINT4 FsMIMstForceProtocolVersion[12];
extern UINT4 FsMIMstTxHoldCount[12];
extern UINT4 FsMIMstMstiConfigIdSel[12];
extern UINT4 FsMIMstMstiRegionName[12];
extern UINT4 FsMIMstMstiRegionVersion[12];
extern UINT4 FsMIMstCistBridgeHelloTime[12];
extern UINT4 FsMIMstCistDynamicPathcostCalculation[12];
extern UINT4 FsMIMstCalcPortPathCostOnSpeedChg[12];
extern UINT4 FsMIMstFlushInterval[12];
extern UINT4 FsMIMstCistFlushIndicationThreshold[12];
extern UINT4 FsMIMstMstiInstanceIndex[12];
extern UINT4 FsMIMstMstiBridgePriority[12];
extern UINT4 FsMIMstMstiFlushIndicationThreshold[12];
extern UINT4 FsMIMstInstanceIndex[12];
extern UINT4 FsMIMstMapVlanIndex[12];
extern UINT4 FsMIMstUnMapVlanIndex[12];
extern UINT4 FsMIMstSetVlanList[12];
extern UINT4 FsMIMstResetVlanList[12];
extern UINT4 FsMIMstCistPort[12];
extern UINT4 FsMIMstCistPortPathCost[12];
extern UINT4 FsMIMstCistPortPriority[12];
extern UINT4 FsMIMstCistPortAdminP2P[12];
extern UINT4 FsMIMstCistPortAdminEdgeStatus[12];
extern UINT4 FsMIMstCistPortProtocolMigration[12];
extern UINT4 FsMIMstCistForcePortState[12];
extern UINT4 FsMIMstCistPortHelloTime[12];
extern UINT4 FsMIMstCistPortAutoEdgeStatus[12];
extern UINT4 FsMIMstCistPortRestrictedRole[12];
extern UINT4 FsMIMstCistPortRestrictedTCN[12];
extern UINT4 FsMIMstCistPortAdminPathCost[12];
extern UINT4 FsMIMstCistPortEnableBPDURx[12];
extern UINT4 FsMIMstCistPortEnableBPDUTx[12];
extern UINT4 FsMIMstCistPortPseudoRootId[12];
extern UINT4 FsMIMstCistPortIsL2Gp[12];
extern UINT4 FsMIMstCistPortLoopGuard[12];
extern UINT4 FsMIMstMstiPort[12];
extern UINT4 FsMIMstMstiPortPathCost[12];
extern UINT4 FsMIMstMstiPortPriority[12];
extern UINT4 FsMIMstMstiForcePortState[12];
extern UINT4 FsMIMstMstiPortAdminPathCost[12];
extern UINT4 FsMIMstMstiPortPseudoRootId[12];
extern UINT4 FsMIMstPort[12];
extern UINT4 FsMIMstPortRowStatus[12];
extern UINT4 FsMIMstCistPortBpduGuard[12];
extern UINT4 FsMIMstCistPortRootGuard[12];
extern UINT4 FsMIMstCistPortErrorRecovery[12];
extern UINT4 FsMIMstBpduGuard[12];
extern UINT4 FsMIMstStpPerfStatus[12];
extern UINT4 FsMIMstInstPortsMap[12];
extern UINT4 FsMIMstPortBpduGuardAction [12];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIDot1sFsMstSetGlobalTrapOption[10];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIMstSetTraps[12];
