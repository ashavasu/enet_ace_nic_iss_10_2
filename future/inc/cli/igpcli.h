/* $Id: igpcli.h,v 1.4 2013/02/05 12:24:06 siva Exp $ */

#ifndef _IGPCLI_H
#define _IGPCLI_H

#include "lr.h"
#include "cli.h"
/* IGMP Proxy CLI command constants. To add a new command ,a new definition 
 * needs to be added to the list.
 */
enum
{
  IGMP_PROXY_STATUS_ENABLE = 1,    /* 1 */
  IGMP_PROXY_STATUS_DISABLE,       /* 2 */
  IGMP_PROXY_CLI_UPIFACE,          /* 3 */
  IGMP_PROXY_CLI_UPIFACE_INT,      /* 4 */
  IGMP_PROXY_CLI_VERSION,          /* 5 */
  IGMP_PROXY_CLI_SHOW_UPIFACE,     /* 6 */
  IGMP_PROXY_CLI_SHOW_MROUTES,     /* 7 */
  IGMP_PROXY_CLI_SHOW_MROUTES_VID, /* 8 */
  IGMP_PROXY_CLI_SHOW_MROUTES_GRP, /* 9 */
  IGMP_PROXY_CLI_SHOW_MROUTES_SRC  /* 10 */
};
 /* Error code values and strings are maintained inside the protocol
  * module itself.
  */

enum 
{
  CLI_IGP_PIM_DVMRP_ERR = 1, /* 1 */
  CLI_IGP_IGMP_DISABLED_ERR, /* 2 */
  CLI_IGP_IGMP_INT_ERR,      /* 3 */      
  CLI_IGP_UPSTREAM_ERR,      /* 4 */
  CLI_IGP_IGS_RTR_PORT_ERR,  /* 5 */
  CLI_IGP_UPSTREAM_NOT_FOUND, /* 6 */
  CLI_IGMP_PROXY_MAX_ERR
};

INT4
cli_process_igp_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);
/* The error strings should be places under the switch so as to avoid
 * redifinition This will be visible only in modulecli.c file
 */
#ifdef _IGPCLI_C
CONST CHR1  *IgpCliErrString [] = {
    NULL,
    " \r% PIM/DVMRP not disabled\r\n",
    " \r% IGMP should be enabled globally\r\n",
    " \r% IGMP should be enabled on the interface\r\n",            
    " \r% Interface should be configured as an upstream interface\r\n",
    " \r% Delete the IGS static router ports before enabling IGMP PROXY\r\n",
 " \r% Upstream interface entry not found\r\n",
    "\r\n"
};
#endif

#endif /* __IGPCLI_H__ */

    
    
