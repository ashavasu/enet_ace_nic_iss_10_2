/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsripcli.h,v 1.3 2013/05/23 12:33:10 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsRip2Security[10];
extern UINT4 FsRip2Peers[10];
extern UINT4 FsRip2TrustNBRListEnable[10];
extern UINT4 FsRip2SpacingEnable[10];
extern UINT4 FsRip2AutoSummaryStatus[10];
extern UINT4 FsRip2RetransTimeoutInt[10];
extern UINT4 FsRip2MaxRetransmissions[10];
extern UINT4 FsRip2OverSubscriptionTimeout[10];
extern UINT4 FsRip2Propagate[10];
extern UINT4 FsRip2MaxRoutes[10];
extern UINT4 FsRipTrcFlag[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsRip2Security(i4SetValFsRip2Security) \
 nmhSetCmn(FsRip2Security, 10, FsRip2SecuritySet, RipLock, RipUnLock, 0, 0, 0, "%i", i4SetValFsRip2Security)
#define nmhSetFsRip2Peers(i4SetValFsRip2Peers) \
 nmhSetCmn(FsRip2Peers, 10, FsRip2PeersSet, RipLock, RipUnLock, 0, 0, 0, "%i", i4SetValFsRip2Peers)
#define nmhSetFsRip2TrustNBRListEnable(i4SetValFsRip2TrustNBRListEnable) \
 nmhSetCmn(FsRip2TrustNBRListEnable, 10, FsRip2TrustNBRListEnableSet, RipLock, RipUnLock, 0, 0, 0, "%i", i4SetValFsRip2TrustNBRListEnable)
#define nmhSetFsRip2SpacingEnable(i4SetValFsRip2SpacingEnable) \
 nmhSetCmn(FsRip2SpacingEnable, 10, FsRip2SpacingEnableSet, RipLock, RipUnLock, 0, 0, 0, "%i", i4SetValFsRip2SpacingEnable)
#define nmhSetFsRip2AutoSummaryStatus(i4SetValFsRip2AutoSummaryStatus) \
 nmhSetCmn(FsRip2AutoSummaryStatus, 10, FsRip2AutoSummaryStatusSet, RipLock, RipUnLock, 0, 0, 0, "%i", i4SetValFsRip2AutoSummaryStatus)
#define nmhSetFsRip2RetransTimeoutInt(i4SetValFsRip2RetransTimeoutInt) \
 nmhSetCmn(FsRip2RetransTimeoutInt, 10, FsRip2RetransTimeoutIntSet, RipLock, RipUnLock, 0, 0, 0, "%i", i4SetValFsRip2RetransTimeoutInt)
#define nmhSetFsRip2MaxRetransmissions(i4SetValFsRip2MaxRetransmissions) \
 nmhSetCmn(FsRip2MaxRetransmissions, 10, FsRip2MaxRetransmissionsSet, RipLock, RipUnLock, 0, 0, 0, "%i", i4SetValFsRip2MaxRetransmissions)
#define nmhSetFsRip2OverSubscriptionTimeout(i4SetValFsRip2OverSubscriptionTimeout) \
 nmhSetCmn(FsRip2OverSubscriptionTimeout, 10, FsRip2OverSubscriptionTimeoutSet, RipLock, RipUnLock, 0, 0, 0, "%i", i4SetValFsRip2OverSubscriptionTimeout)
#define nmhSetFsRip2Propagate(i4SetValFsRip2Propagate) \
 nmhSetCmn(FsRip2Propagate, 10, FsRip2PropagateSet, RipLock, RipUnLock, 0, 0, 0, "%i", i4SetValFsRip2Propagate)
#define nmhSetFsRip2MaxRoutes(i4SetValFsRip2MaxRoutes) \
 nmhSetCmn(FsRip2MaxRoutes, 10, FsRip2MaxRoutesSet, RipLock, RipUnLock, 0, 0, 0, "%i", i4SetValFsRip2MaxRoutes)
#define nmhSetFsRipTrcFlag(i4SetValFsRipTrcFlag) \
 nmhSetCmn(FsRipTrcFlag, 10, FsRipTrcFlagSet, RipLock, RipUnLock, 0, 0, 0, "%i", i4SetValFsRipTrcFlag)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsRip2TrustNBRIpAddr[12];
extern UINT4 FsRip2TrustNBRRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsRip2TrustNBRRowStatus(u4FsRip2TrustNBRIpAddr ,i4SetValFsRip2TrustNBRRowStatus) \
 nmhSetCmn(FsRip2TrustNBRRowStatus, 12, FsRip2TrustNBRRowStatusSet, RipLock, RipUnLock, 0, 1, 1, "%p %i", u4FsRip2TrustNBRIpAddr ,i4SetValFsRip2TrustNBRRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsRip2IfConfAddress[12];
extern UINT4 FsRip2IfAdminStat[12];
extern UINT4 FsRip2IfConfUpdateTmr[12];
extern UINT4 FsRip2IfConfGarbgCollectTmr[12];
extern UINT4 FsRip2IfConfRouteAgeTmr[12];
extern UINT4 FsRip2IfSplitHorizonStatus[12];
extern UINT4 FsRip2IfConfDefRtInstall[12];
extern UINT4 FsRip2IfConfSpacingTmr[12];
extern UINT4 FsRip2IfConfAuthType[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsRip2IfAdminStat(u4FsRip2IfConfAddress ,i4SetValFsRip2IfAdminStat) \
 nmhSetCmn(FsRip2IfAdminStat, 12, FsRip2IfAdminStatSet, RipLock, RipUnLock, 0, 0, 1, "%p %i", u4FsRip2IfConfAddress ,i4SetValFsRip2IfAdminStat)
#define nmhSetFsRip2IfConfUpdateTmr(u4FsRip2IfConfAddress ,i4SetValFsRip2IfConfUpdateTmr) \
 nmhSetCmn(FsRip2IfConfUpdateTmr, 12, FsRip2IfConfUpdateTmrSet, RipLock, RipUnLock, 0, 0, 1, "%p %i", u4FsRip2IfConfAddress ,i4SetValFsRip2IfConfUpdateTmr)
#define nmhSetFsRip2IfConfGarbgCollectTmr(u4FsRip2IfConfAddress ,i4SetValFsRip2IfConfGarbgCollectTmr) \
 nmhSetCmn(FsRip2IfConfGarbgCollectTmr, 12, FsRip2IfConfGarbgCollectTmrSet, RipLock, RipUnLock, 0, 0, 1, "%p %i", u4FsRip2IfConfAddress ,i4SetValFsRip2IfConfGarbgCollectTmr)
#define nmhSetFsRip2IfConfRouteAgeTmr(u4FsRip2IfConfAddress ,i4SetValFsRip2IfConfRouteAgeTmr) \
 nmhSetCmn(FsRip2IfConfRouteAgeTmr, 12, FsRip2IfConfRouteAgeTmrSet, RipLock, RipUnLock, 0, 0, 1, "%p %i", u4FsRip2IfConfAddress ,i4SetValFsRip2IfConfRouteAgeTmr)
#define nmhSetFsRip2IfSplitHorizonStatus(u4FsRip2IfConfAddress ,i4SetValFsRip2IfSplitHorizonStatus) \
 nmhSetCmn(FsRip2IfSplitHorizonStatus, 12, FsRip2IfSplitHorizonStatusSet, RipLock, RipUnLock, 0, 0, 1, "%p %i", u4FsRip2IfConfAddress ,i4SetValFsRip2IfSplitHorizonStatus)
#define nmhSetFsRip2IfConfDefRtInstall(u4FsRip2IfConfAddress ,i4SetValFsRip2IfConfDefRtInstall) \
 nmhSetCmn(FsRip2IfConfDefRtInstall, 12, FsRip2IfConfDefRtInstallSet, RipLock, RipUnLock, 0, 0, 1, "%p %i", u4FsRip2IfConfAddress ,i4SetValFsRip2IfConfDefRtInstall)
#define nmhSetFsRip2IfConfSpacingTmr(u4FsRip2IfConfAddress ,i4SetValFsRip2IfConfSpacingTmr) \
 nmhSetCmn(FsRip2IfConfSpacingTmr, 12, FsRip2IfConfSpacingTmrSet, RipLock, RipUnLock, 0, 0, 1, "%p %i", u4FsRip2IfConfAddress ,i4SetValFsRip2IfConfSpacingTmr)
#define nmhSetFsRip2IfConfAuthType(u4FsRip2IfConfAddress ,i4SetValFsRip2IfConfAuthType) \
 nmhSetCmnWithLock(FsRip2IfConfAuthType, 12, FsRip2IfConfAuthTypeSet, RipLock, RipUnLock, 0, 0, 1, "%p %i", u4FsRip2IfConfAddress ,i4SetValFsRip2IfConfAuthType)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsRipMd5AuthAddress[12];
extern UINT4 FsRipMd5AuthKeyId[12];
extern UINT4 FsRipMd5AuthKey[12];
extern UINT4 FsRipMd5KeyStartTime[12];
extern UINT4 FsRipMd5KeyExpiryTime[12];
extern UINT4 FsRipMd5KeyRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsRipMd5AuthKey(u4FsRipMd5AuthAddress , i4FsRipMd5AuthKeyId ,pSetValFsRipMd5AuthKey) \
 nmhSetCmn(FsRipMd5AuthKey, 12, FsRipMd5AuthKeySet, RipLock, RipUnLock, 0, 0, 2, "%p %i %s", u4FsRipMd5AuthAddress , i4FsRipMd5AuthKeyId ,pSetValFsRipMd5AuthKey)
#define nmhSetFsRipMd5KeyStartTime(u4FsRipMd5AuthAddress , i4FsRipMd5AuthKeyId ,i4SetValFsRipMd5KeyStartTime) \
 nmhSetCmn(FsRipMd5KeyStartTime, 12, FsRipMd5KeyStartTimeSet, RipLock, RipUnLock, 0, 0, 2, "%p %i %i", u4FsRipMd5AuthAddress , i4FsRipMd5AuthKeyId ,i4SetValFsRipMd5KeyStartTime)
#define nmhSetFsRipMd5KeyExpiryTime(u4FsRipMd5AuthAddress , i4FsRipMd5AuthKeyId ,i4SetValFsRipMd5KeyExpiryTime) \
 nmhSetCmn(FsRipMd5KeyExpiryTime, 12, FsRipMd5KeyExpiryTimeSet, RipLock, RipUnLock, 0, 0, 2, "%p %i %i", u4FsRipMd5AuthAddress , i4FsRipMd5AuthKeyId ,i4SetValFsRipMd5KeyExpiryTime)
#define nmhSetFsRipMd5KeyRowStatus(u4FsRipMd5AuthAddress , i4FsRipMd5AuthKeyId ,i4SetValFsRipMd5KeyRowStatus) \
 nmhSetCmn(FsRipMd5KeyRowStatus, 12, FsRipMd5KeyRowStatusSet, RipLock, RipUnLock, 0, 1, 2, "%p %i %i", u4FsRipMd5AuthAddress , i4FsRipMd5AuthKeyId ,i4SetValFsRipMd5KeyRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsRipCryptoAuthIfIndex[12];
extern UINT4 FsRipCryptoAuthAddress[12];
extern UINT4 FsRipCryptoAuthKeyId[12];
extern UINT4 FsRipCryptoAuthKey[12];
extern UINT4 FsRipCryptoKeyStartAccept[12];
extern UINT4 FsRipCryptoKeyStartGenerate[12];
extern UINT4 FsRipCryptoKeyStopGenerate[12];
extern UINT4 FsRipCryptoKeyStopAccept[12];
extern UINT4 FsRipCryptoKeyStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsRipCryptoAuthKey(i4FsRipCryptoAuthIfIndex , u4FsRipCryptoAuthAddress , i4FsRipCryptoAuthKeyId ,pSetValFsRipCryptoAuthKey) \
 nmhSetCmnWithLock(FsRipCryptoAuthKey, 12, FsRipCryptoAuthKeySet, RipLock, RipUnLock, 0, 0, 3, "%i %p %i %s", i4FsRipCryptoAuthIfIndex , u4FsRipCryptoAuthAddress , i4FsRipCryptoAuthKeyId ,pSetValFsRipCryptoAuthKey)
#define nmhSetFsRipCryptoKeyStartAccept(i4FsRipCryptoAuthIfIndex , u4FsRipCryptoAuthAddress , i4FsRipCryptoAuthKeyId ,pSetValFsRipCryptoKeyStartAccept) \
 nmhSetCmnWithLock(FsRipCryptoKeyStartAccept, 12, FsRipCryptoKeyStartAcceptSet, RipLock, RipUnLock, 0, 0, 3, "%i %p %i %s", i4FsRipCryptoAuthIfIndex , u4FsRipCryptoAuthAddress , i4FsRipCryptoAuthKeyId ,pSetValFsRipCryptoKeyStartAccept)
#define nmhSetFsRipCryptoKeyStartGenerate(i4FsRipCryptoAuthIfIndex , u4FsRipCryptoAuthAddress , i4FsRipCryptoAuthKeyId ,pSetValFsRipCryptoKeyStartGenerate) \
 nmhSetCmnWithLock(FsRipCryptoKeyStartGenerate, 12, FsRipCryptoKeyStartGenerateSet, RipLock, RipUnLock, 0, 0, 3, "%i %p %i %s", i4FsRipCryptoAuthIfIndex , u4FsRipCryptoAuthAddress , i4FsRipCryptoAuthKeyId ,pSetValFsRipCryptoKeyStartGenerate)
#define nmhSetFsRipCryptoKeyStopGenerate(i4FsRipCryptoAuthIfIndex , u4FsRipCryptoAuthAddress , i4FsRipCryptoAuthKeyId ,pSetValFsRipCryptoKeyStopGenerate) \
 nmhSetCmnWithLock(FsRipCryptoKeyStopGenerate, 12, FsRipCryptoKeyStopGenerateSet, RipLock, RipUnLock, 0, 0, 3, "%i %p %i %s", i4FsRipCryptoAuthIfIndex , u4FsRipCryptoAuthAddress , i4FsRipCryptoAuthKeyId ,pSetValFsRipCryptoKeyStopGenerate)
#define nmhSetFsRipCryptoKeyStopAccept(i4FsRipCryptoAuthIfIndex , u4FsRipCryptoAuthAddress , i4FsRipCryptoAuthKeyId ,pSetValFsRipCryptoKeyStopAccept) \
 nmhSetCmnWithLock(FsRipCryptoKeyStopAccept, 12, FsRipCryptoKeyStopAcceptSet, RipLock, RipUnLock, 0, 0, 3, "%i %p %i %s", i4FsRipCryptoAuthIfIndex , u4FsRipCryptoAuthAddress , i4FsRipCryptoAuthKeyId ,pSetValFsRipCryptoKeyStopAccept)
#define nmhSetFsRipCryptoKeyStatus(i4FsRipCryptoAuthIfIndex , u4FsRipCryptoAuthAddress , i4FsRipCryptoAuthKeyId ,i4SetValFsRipCryptoKeyStatus) \
 nmhSetCmnWithLock(FsRipCryptoKeyStatus, 12, FsRipCryptoKeyStatusSet, RipLock, RipUnLock, 0, 0, 3, "%i %p %i %i", i4FsRipCryptoAuthIfIndex , u4FsRipCryptoAuthAddress , i4FsRipCryptoAuthKeyId ,i4SetValFsRipCryptoKeyStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsRip2NBRUnicastIpAddr[12];
extern UINT4 FsRip2NBRUnicastNBRRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsRip2NBRUnicastNBRRowStatus(u4FsRip2NBRUnicastIpAddr ,i4SetValFsRip2NBRUnicastNBRRowStatus) \
 nmhSetCmn(FsRip2NBRUnicastNBRRowStatus, 12, FsRip2NBRUnicastNBRRowStatusSet, RipLock, RipUnLock, 0, 1, 1, "%p %i", u4FsRip2NBRUnicastIpAddr ,i4SetValFsRip2NBRUnicastNBRRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsRipIfIndex[12];
extern UINT4 FsRipAggAddress[12];
extern UINT4 FsRipAggAddressMask[12];
extern UINT4 FsRipAggStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsRipAggStatus(i4FsRipIfIndex , u4FsRipAggAddress , u4FsRipAggAddressMask ,i4SetValFsRipAggStatus) \
 nmhSetCmn(FsRipAggStatus, 12, FsRipAggStatusSet, RipLock, RipUnLock, 0, 1, 3, "%i %p %p %i", i4FsRipIfIndex , u4FsRipAggAddress , u4FsRipAggAddressMask ,i4SetValFsRipAggStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsRipAdminStatus[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsRipAdminStatus(i4SetValFsRipAdminStatus) \
 nmhSetCmn(FsRipAdminStatus, 10, FsRipAdminStatusSet, RipLock, RipUnLock, 0, 0, 0, "%i", i4SetValFsRipAdminStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsRip2LastAuthKeyLifetimeStatus[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsRip2LastAuthKeyLifetimeStatus(i4SetValFsRip2LastAuthKeyLifetimeStatus) \
 nmhSetCmnWithLock(FsRip2LastAuthKeyLifetimeStatus, 10, FsRip2LastAuthKeyLifetimeStatusSet, RipLock, RipUnLock, 0, 0, 0, "%i", i4SetValFsRip2LastAuthKeyLifetimeStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsRipRRDGlobalStatus[10];
extern UINT4 FsRipRRDSrcProtoMaskEnable[10];
extern UINT4 FsRipRRDSrcProtoMaskDisable[10];
extern UINT4 FsRipRRDRouteTagType[10];
extern UINT4 FsRipRRDRouteTag[10];
extern UINT4 FsRipRRDRouteDefMetric[10];
extern UINT4 FsRipRRDRouteMapEnable[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsRipRRDGlobalStatus(i4SetValFsRipRRDGlobalStatus) \
 nmhSetCmn(FsRipRRDGlobalStatus, 10, FsRipRRDGlobalStatusSet, RipLock, RipUnLock, 0, 0, 0, "%i", i4SetValFsRipRRDGlobalStatus)
#define nmhSetFsRipRRDSrcProtoMaskEnable(i4SetValFsRipRRDSrcProtoMaskEnable) \
 nmhSetCmn(FsRipRRDSrcProtoMaskEnable, 10, FsRipRRDSrcProtoMaskEnableSet, RipLock, RipUnLock, 0, 0, 0, "%i", i4SetValFsRipRRDSrcProtoMaskEnable)
#define nmhSetFsRipRRDSrcProtoMaskDisable(i4SetValFsRipRRDSrcProtoMaskDisable) \
 nmhSetCmn(FsRipRRDSrcProtoMaskDisable, 10, FsRipRRDSrcProtoMaskDisableSet, RipLock, RipUnLock, 0, 0, 0, "%i", i4SetValFsRipRRDSrcProtoMaskDisable)
#define nmhSetFsRipRRDRouteTagType(i4SetValFsRipRRDRouteTagType) \
 nmhSetCmn(FsRipRRDRouteTagType, 10, FsRipRRDRouteTagTypeSet, RipLock, RipUnLock, 0, 0, 0, "%i", i4SetValFsRipRRDRouteTagType)
#define nmhSetFsRipRRDRouteTag(i4SetValFsRipRRDRouteTag) \
 nmhSetCmn(FsRipRRDRouteTag, 10, FsRipRRDRouteTagSet, RipLock, RipUnLock, 0, 0, 0, "%i", i4SetValFsRipRRDRouteTag)
#define nmhSetFsRipRRDRouteDefMetric(i4SetValFsRipRRDRouteDefMetric) \
 nmhSetCmn(FsRipRRDRouteDefMetric, 10, FsRipRRDRouteDefMetricSet, RipLock, RipUnLock, 0, 0, 0, "%i", i4SetValFsRipRRDRouteDefMetric)
#define nmhSetFsRipRRDRouteMapEnable(pSetValFsRipRRDRouteMapEnable) \
 nmhSetCmn(FsRipRRDRouteMapEnable, 10, FsRipRRDRouteMapEnableSet, RipLock, RipUnLock, 0, 0, 0, "%s", pSetValFsRipRRDRouteMapEnable)

#endif
