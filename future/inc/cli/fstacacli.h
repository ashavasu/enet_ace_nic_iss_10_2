/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fstacacli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsTacClntActiveServer[10];
extern UINT4 FsTacClntTraceLevel[10];
extern UINT4 FsTacClntRetransmit[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsTacClntActiveServer(u4SetValFsTacClntActiveServer)	\
	nmhSetCmn(FsTacClntActiveServer, 10, FsTacClntActiveServerSet, TacProtocolLock, TacProtocolUnlock, 0, 0, 0, "%p", u4SetValFsTacClntActiveServer)
#define nmhSetFsTacClntTraceLevel(u4SetValFsTacClntTraceLevel)	\
	nmhSetCmn(FsTacClntTraceLevel, 10, FsTacClntTraceLevelSet, TacProtocolLock, TacProtocolUnlock, 0, 0, 0, "%u", u4SetValFsTacClntTraceLevel)
#define nmhSetFsTacClntRetransmit(i4SetValFsTacClntRetransmit)	\
	nmhSetCmn(FsTacClntRetransmit, 10, FsTacClntRetransmitSet, TacProtocolLock, TacProtocolUnlock, 0, 0, 0, "%i", i4SetValFsTacClntRetransmit)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsTacClntServerAddress[12];
extern UINT4 FsTacClntServerStatus[12];
extern UINT4 FsTacClntServerSingleConnect[12];
extern UINT4 FsTacClntServerPort[12];
extern UINT4 FsTacClntServerTimeout[12];
extern UINT4 FsTacClntServerKey[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsTacClntServerStatus(u4FsTacClntServerAddress ,i4SetValFsTacClntServerStatus)	\
	nmhSetCmn(FsTacClntServerStatus, 12, FsTacClntServerStatusSet, TacProtocolLock, TacProtocolUnlock, 0, 1, 1, "%p %i", u4FsTacClntServerAddress ,i4SetValFsTacClntServerStatus)
#define nmhSetFsTacClntServerSingleConnect(u4FsTacClntServerAddress ,i4SetValFsTacClntServerSingleConnect)	\
	nmhSetCmn(FsTacClntServerSingleConnect, 12, FsTacClntServerSingleConnectSet, TacProtocolLock, TacProtocolUnlock, 0, 0, 1, "%p %i", u4FsTacClntServerAddress ,i4SetValFsTacClntServerSingleConnect)
#define nmhSetFsTacClntServerPort(u4FsTacClntServerAddress ,i4SetValFsTacClntServerPort)	\
	nmhSetCmn(FsTacClntServerPort, 12, FsTacClntServerPortSet, TacProtocolLock, TacProtocolUnlock, 0, 0, 1, "%p %i", u4FsTacClntServerAddress ,i4SetValFsTacClntServerPort)
#define nmhSetFsTacClntServerTimeout(u4FsTacClntServerAddress ,i4SetValFsTacClntServerTimeout)	\
	nmhSetCmn(FsTacClntServerTimeout, 12, FsTacClntServerTimeoutSet, TacProtocolLock, TacProtocolUnlock, 0, 0, 1, "%p %i", u4FsTacClntServerAddress ,i4SetValFsTacClntServerTimeout)
#define nmhSetFsTacClntServerKey(u4FsTacClntServerAddress ,pSetValFsTacClntServerKey)	\
	nmhSetCmn(FsTacClntServerKey, 12, FsTacClntServerKeySet, TacProtocolLock, TacProtocolUnlock, 0, 0, 1, "%p %s", u4FsTacClntServerAddress ,pSetValFsTacClntServerKey)

#endif
