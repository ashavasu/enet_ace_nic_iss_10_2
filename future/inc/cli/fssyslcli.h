/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssyslcli.h,v 1.4 2014/06/24 11:32:57 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsSyslogLogging[10];
extern UINT4 FsSyslogTimeStamp[10];
extern UINT4 FsSyslogConsoleLog[10];
extern UINT4 FsSyslogSysBuffers[10];
extern UINT4 FsSyslogClearLog[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsSyslogLogging(i4SetValFsSyslogLogging) \
 nmhSetCmn(FsSyslogLogging, 10, FsSyslogLoggingSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsSyslogLogging)
#define nmhSetFsSyslogTimeStamp(i4SetValFsSyslogTimeStamp) \
 nmhSetCmn(FsSyslogTimeStamp, 10, FsSyslogTimeStampSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsSyslogTimeStamp)
#define nmhSetFsSyslogConsoleLog(i4SetValFsSyslogConsoleLog) \
 nmhSetCmn(FsSyslogConsoleLog, 10, FsSyslogConsoleLogSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsSyslogConsoleLog)
#define nmhSetFsSyslogSysBuffers(i4SetValFsSyslogSysBuffers) \
 nmhSetCmn(FsSyslogSysBuffers, 10, FsSyslogSysBuffersSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsSyslogSysBuffers)
#define nmhSetFsSyslogClearLog(i4SetValFsSyslogClearLog) \
 nmhSetCmn(FsSyslogClearLog, 10, FsSyslogClearLogSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsSyslogClearLog)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsSyslogConfigModule[12];
extern UINT4 FsSyslogConfigLogLevel[12];



/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsSyslogFacility[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsSyslogFacility(i4SetValFsSyslogFacility) \
 nmhSetCmn(FsSyslogFacility, 10, FsSyslogFacilitySet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsSyslogFacility)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsSyslogLogSrvAddr[10];
extern UINT4 FsSyslogLogNoLogServer[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsSyslogLogSrvAddr(u4SetValFsSyslogLogSrvAddr) \
 nmhSetCmn(FsSyslogLogSrvAddr, 10, FsSyslogLogSrvAddrSet, NULL, NULL, 0, 0, 0, "%p", u4SetValFsSyslogLogSrvAddr)
#define nmhSetFsSyslogLogNoLogServer(i4SetValFsSyslogLogNoLogServer) \
 nmhSetCmn(FsSyslogLogNoLogServer, 10, FsSyslogLogNoLogServerSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsSyslogLogNoLogServer)

extern UINT4 FsSyslogFwdRowStatus[12];
#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsSyslogFwdRowStatus(i4FsSyslogFwdPriority , i4FsSyslogFwdAddressType , pFsSyslogFwdServerIP ,i4SetValFsSyslogFwdRowStatus) \
 nmhSetCmn(FsSyslogFwdRowStatus, 12, FsSyslogFwdRowStatusSet, NULL, NULL, 0, 1, 3, "%i %i %s %i", i4FsSyslogFwdPriority , i4FsSyslogFwdAddressType , pFsSyslogFwdServerIP ,i4SetValFsSyslogFwdRowStatus)
#endif
#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsSyslogSmtpSrvAddr[10];
extern UINT4 FsSyslogSmtpRcvrMailId[10];
extern UINT4 FsSyslogSmtpSenderMailId[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsSyslogSmtpSrvAddr(u4SetValFsSyslogSmtpSrvAddr) \
 nmhSetCmn(FsSyslogSmtpSrvAddr, 10, FsSyslogSmtpSrvAddrSet, NULL, NULL, 0, 0, 0, "%p", u4SetValFsSyslogSmtpSrvAddr)
#define nmhSetFsSyslogSmtpRcvrMailId(pSetValFsSyslogSmtpRcvrMailId) \
 nmhSetCmn(FsSyslogSmtpRcvrMailId, 10, FsSyslogSmtpRcvrMailIdSet, NULL, NULL, 0, 0, 0, "%s", pSetValFsSyslogSmtpRcvrMailId)
#define nmhSetFsSyslogSmtpSenderMailId(pSetValFsSyslogSmtpSenderMailId) \
 nmhSetCmn(FsSyslogSmtpSenderMailId, 10, FsSyslogSmtpSenderMailIdSet, NULL, NULL, 0, 0, 0, "%s", pSetValFsSyslogSmtpSenderMailId)

extern UINT4 FsSyslogMailRowStatus[12];
#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsSyslogMailRowStatus(i4FsSyslogMailPriority , i4FsSyslogMailServAddType , pFsSyslogMailServAdd ,i4SetValFsSyslogMailRowStatus) \
 nmhSetCmn(FsSyslogMailRowStatus, 12, FsSyslogMailRowStatusSet, NULL, NULL, 0, 1, 3, "%i %i %s %i", i4FsSyslogMailPriority , i4FsSyslogMailServAddType , pFsSyslogMailServAdd ,i4SetValFsSyslogMailRowStatus)
#endif
#endif
