/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: std1llcli.h,v 1.2 2016/07/16 11:15:02 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 LldpXdot1EvbConfigEvbTxEnable[19];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetLldpXdot1EvbConfigEvbTxEnable(i4LldpV2PortConfigIfIndex , u4LldpV2PortConfigDestAddressIndex ,i4SetValLldpXdot1EvbConfigEvbTxEnable) \
 nmhSetCmn(LldpXdot1EvbConfigEvbTxEnable, 19, LldpXdot1EvbConfigEvbTxEnableSet, VlanLock, VlanUnLock, 0, 0, 2, "%i %u %i", i4LldpV2PortConfigIfIndex , u4LldpV2PortConfigDestAddressIndex ,i4SetValLldpXdot1EvbConfigEvbTxEnable)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 LldpXdot1EvbConfigCdcpTxEnable[19];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetLldpXdot1EvbConfigCdcpTxEnable(i4LldpV2PortConfigIfIndex , u4LldpV2PortConfigDestAddressIndex ,i4SetValLldpXdot1EvbConfigCdcpTxEnable) \
 nmhSetCmn(LldpXdot1EvbConfigCdcpTxEnable, 19, LldpXdot1EvbConfigCdcpTxEnableSet, VlanLock, VlanUnLock, 0, 0, 2, "%i %u %i", i4LldpV2PortConfigIfIndex , u4LldpV2PortConfigDestAddressIndex ,i4SetValLldpXdot1EvbConfigCdcpTxEnable)

#endif
