/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* $Id: rbrgcli.h,v 1.4 2012/03/13 12:48:19 siva Exp $
*
* Description: This file contains the Rbrg CLI related prototypes,enum and macros
*********************************************************************/

#include "cli.h"

INT4 cli_process_Rbrg_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);
INT4
cli_process_rbrg_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);

#define RBRG_LOCK  RbrgMainTaskLock ()

#define RBRG_UNLOCK  RbrgMainTaskUnLock ()

enum
{
 CLI_RBRG_FSRBRIDGEGLOBALTRACE,
 CLI_RBRG_FSRBRIDGEGLOBALTABLE,
 CLI_RBRG_FSRBRIDGENICKNAMETABLE,
 CLI_RBRG_FSRBRIDGEPORTTABLE,
 CLI_RBRG_FSRBRIDGEUNIFDBTABLE,
 CLI_RBRG_FSRBRIDGEUNIFIBTABLE,
 CLI_RBRG_FSRBRIDGEMULTIFIBTABLE,
 CLI_RBRG_CLEAR_ALL_COUNTER,
 CLI_RBRG_CLEAR_PORT_COUNTER,
    CLI_RBRG_SHOW_GLOB_CONF,
    CLI_RBRG_SHOW_COUNTERS,
    CLI_RBRG_SHOW_PORT_INFO,
    CLI_RBRG_SHOW_FDB_INFO,
    CLI_RBRG_SHOW_FIB_INFO,
    CLI_RBRG_SHOW_MULTI_FIB_INFO,
    CLI_RBRG_SHOW_NICKNAME_INFO

};


enum
{
    RBRG_CLI_INVALID_CONTEXT_ID,
    RBRG_CLI_NICKNAME_NOT_SET,
    RBRG_CLI_DEL_FIB_ENTRIES,
    RBRG_CLI_FIB_DELETION_FAILED,
    RBRG_CLI_HOP_COUNT_EXCEEDED,
    RBRG_CLI_FIB_NO_TRUNK_PORT,
    RBRG_CLI_STATIC_FIB_FDB_ENTRIES_EXIST,
    RBRG_CLI_MODULE_SHUT,
    RBRG_CLI_PORT_OUT_OF_RANGE,
    RBRG_CLI_NICKNAME_OUT_OF_RANGE,
    RBRG_CLI_CONFIDENCE_OUT_OF_RANGE,
    RBRG_CLI_INVALID_ROWSTATUS,
    RBRG_CLI_MTU_OUT_OF_RANGE,
    RBRG_CLI_FIB_ENTRY_NOT_FOUND,
    RBRG_CLI_FIB_DELETE_STATIC_FDB_ENTRIES_EXIST,
    RBRG_CLI_INVALID_TRACE,
    RBRG_CLI_CONTEXT_OUT_OF_RANGE,
    RBRG_CLI_MAX_NICKNAME_REACHED,
    RBRG_CLI_NICKNAME_PRIORITY_OUT_OF_RANGE,
    RBRG_CLI_NICKNAME_DTR_PRIORITY_OUT_OF_RANGE,
    RBRG_CLI_DESIG_VLAN_OUT_OF_RANGE,
    RBRG_CLI_FDB_FIB_ENTRY_NOT_PRESENT,
    RBRG_CLI_PORT_ENTRY_NOT_FOUND,
    RBRG_CLI_FDB_ENTRY_NOT_FOUND,
    RBRG_CLI_NICKNAME_ENTRY_NOT_FOUND,
    RBRG_CLI_MULTIFIB_ENTRY_NOT_FOUND,
    CLI_RBRG_MAX_ERR
};

#define RBRG_CLI_MAX_ARGS   25

#if defined  (__RBRGCLIG_C__)
CONST CHR1  *RbrgCliErrString [] = {
/* 1 */"Invaiid Context Identifier\r\n",
       "Nickname should be set before enabling access port\r\n",
       "Delete the static FIB and FDB entries before performing this action\r\n",
       "Deletion of Fib entry failed.FIB Entry not found\r\n",
       "Hop count range exceeeded. Range is from 10 to 100.\r\n",
       "Port is not a trunk port.FIB entry can be created only over a trunk port\r\n",
       "Static FIB or FIB entries on this port exist. Port property can be changed only if the static entries are removed\r\n",
       "Rbridge module is shutdown\r\n",
       "Port number is out of range\r\n",
       "Nickname is out of range\r\n",
       "Confidence is out of range\r\n",
       "Invalid value for row status\r\n",
       "MTU is out of range. Allowed range is from 1470 to 1500\r\n",
       "FIB entry not found\r\n",
       "FIB deletion is not allowed. Static FDB entries exist\r\n",
       "Invalid trace option\r\n",
       "Context Id out of range\r\n",
       "Maximum number of nicknames have been already created in the system\r\n",
       "Nickname priority is out of range\r\n",
       "Nickname Distribution Priority is out of range\r\n",
       "Designated VLAN is out range. Allowed range is from 1 to 4094\r\n",
       "FDB entry can be created only if the corresponding FIB entry exists\r\n",
       "Port table entry does not exist\r\n",
       "Fdb Entry does not exist\r\n",
       "Nickname entry not found\r\n",
       "MultiFib entry not found\r\n",
 NULL,
};
#else

PUBLIC CHR1* RbrgCliErrString;
#endif


#define   CLI_RBRG_START                 1
#define   CLI_RBRG_SHUT                  2
#define   CLI_RBRG_ENABLE                1
#define   CLI_RBRG_DISABLE               2
#define   CLI_RBRG_UCAST_ENABLE          1
#define   CLI_RBRG_UCAST_DISABLE         2
#define   CLI_RBRG_MCAST_ENABLE          1
#define   CLI_RBRG_MCAST_DISABLE         2
#define   CLI_RBRG_PORT_DISABLE          0  
#define   CLI_RBRG_PORT_ENABLE           1
#define   CLI_RBRG_TRUNK_ENABLE          1
#define   CLI_RBRG_TRUNK_DISABLE         0
#define   CLI_RBRG_ACCESS_ENABLE         1
#define   CLI_RBRG_ACCESS_DISABLE        0
#define   CLI_RBRG_MAC_LEARNING_DISBALE  0
#define   CLI_RBRG_MAC_LEARNING_ENABLE   1
#define   CLI_RBRG_DEBUG_ALL             9
#define   CLI_RBRG_DEBUG_MGMT            1
#define   CLI_RBRG_DEBUG_RESOURCE        2
#define   CLI_RBRG_DEBUG_FAILURE         3
#define   CLI_RBRG_DEBUG_CONTROLPLANE    4
#define   CLI_RBRG_DEBUG_CRITICAL        5
#define   CLI_RBRG_DEBUG_ALL             9
#define   CLI_RBRG_DEBUG_MGMT            1
#define   CLI_RBRG_DEBUG_RESOURCE        2
#define   CLI_RBRG_DEBUG_FAILURE         3
#define   CLI_RBRG_DEBUG_CONTROLPLANE    4
#define   CLI_RBRG_DEBUG_CRITICAL        5
#define   CLI_RBRG_DEBUG_INIT_SHUT       6
#define   CLI_RBRG_INVALID_CXT           0xFFFFFFFF
#define RBRG_MIN_MTU                    1470
#define RBRG_DEFAULT_MTU                1470
#define RBRG_MAX_MTU                    1500
#define RBRG_MIN_HOPCOUNT               10
#define RBRG_DEFAULT_HOPCOUNT           10
#define RBRG_MAX_HOPCOUNT               100
#define RBRG_DEFAULT_CONFIDENCE         255
#define RBRG_DEFAULT_PRIORITY           192
#define RBRG_DEFAULT_DTRPRIORITY        32768
#define RBRG_DEFAULT_DESIGVLAN          1
enum
{
   RBRG_STATIC = 0,
   RBRG_DYNAMIC,
   RBRG_BOTH,
   RBRG_INVALID
};
INT4 RbrgShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module);
