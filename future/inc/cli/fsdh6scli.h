/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsdh6scli.h,v 1.3 2009/10/30 05:21:22 prabuc Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDhcp6SrvDebugTrace[11];
extern UINT4 FsDhcp6SrvTrapAdminControl[11];
extern UINT4 FsDhcp6SrvSysLogAdminStatus[11];
extern UINT4 FsDhcp6SrvListenPort[11];
extern UINT4 FsDhcp6SrvClientTransmitPort[11];
extern UINT4 FsDhcp6SrvRelayTransmitPort[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDhcp6SrvDebugTrace(pSetValFsDhcp6SrvDebugTrace)	\
	nmhSetCmn(FsDhcp6SrvDebugTrace, 11, FsDhcp6SrvDebugTraceSet,D6SrMainLock,D6SrMainUnLock, 0, 0, 0, "%s", pSetValFsDhcp6SrvDebugTrace)
#define nmhSetFsDhcp6SrvTrapAdminControl(pSetValFsDhcp6SrvTrapAdminControl)	\
	nmhSetCmn(FsDhcp6SrvTrapAdminControl, 11, FsDhcp6SrvTrapAdminControlSet,D6SrMainLock,D6SrMainUnLock, 0, 0, 0, "%s", pSetValFsDhcp6SrvTrapAdminControl)
#define nmhSetFsDhcp6SrvSysLogAdminStatus(i4SetValFsDhcp6SrvSysLogAdminStatus)	\
	nmhSetCmn(FsDhcp6SrvSysLogAdminStatus, 11, FsDhcp6SrvSysLogAdminStatusSet,D6SrMainLock,D6SrMainUnLock, 0, 0, 0, "%i", i4SetValFsDhcp6SrvSysLogAdminStatus)
#define nmhSetFsDhcp6SrvListenPort(i4SetValFsDhcp6SrvListenPort)	\
	nmhSetCmn(FsDhcp6SrvListenPort, 11, FsDhcp6SrvListenPortSet,D6SrMainLock,D6SrMainUnLock, 0, 0, 0, "%i", i4SetValFsDhcp6SrvListenPort)
#define nmhSetFsDhcp6SrvClientTransmitPort(i4SetValFsDhcp6SrvClientTransmitPort)	\
	nmhSetCmn(FsDhcp6SrvClientTransmitPort, 11, FsDhcp6SrvClientTransmitPortSet,D6SrMainLock,D6SrMainUnLock, 0, 0, 0, "%i", i4SetValFsDhcp6SrvClientTransmitPort)
#define nmhSetFsDhcp6SrvRelayTransmitPort(i4SetValFsDhcp6SrvRelayTransmitPort)	\
	nmhSetCmn(FsDhcp6SrvRelayTransmitPort, 11, FsDhcp6SrvRelayTransmitPortSet,D6SrMainLock,D6SrMainUnLock, 0, 0, 0, "%i", i4SetValFsDhcp6SrvRelayTransmitPort)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDhcp6SrvPoolIndex[13];
extern UINT4 FsDhcp6SrvPoolName[13];
extern UINT4 FsDhcp6SrvPoolPreference[13];
extern UINT4 FsDhcp6SrvPoolDuidType[13];
extern UINT4 FsDhcp6SrvPoolDuidIfIndex[13];
extern UINT4 FsDhcp6SrvPoolRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDhcp6SrvPoolName(u4FsDhcp6SrvPoolIndex ,pSetValFsDhcp6SrvPoolName)	\
	nmhSetCmn(FsDhcp6SrvPoolName, 13, FsDhcp6SrvPoolNameSet,D6SrMainLock,D6SrMainUnLock, 0, 0, 1, "%u %s", u4FsDhcp6SrvPoolIndex ,pSetValFsDhcp6SrvPoolName)
#define nmhSetFsDhcp6SrvPoolPreference(u4FsDhcp6SrvPoolIndex ,i4SetValFsDhcp6SrvPoolPreference)	\
	nmhSetCmn(FsDhcp6SrvPoolPreference, 13, FsDhcp6SrvPoolPreferenceSet,D6SrMainLock,D6SrMainUnLock, 0, 0, 1, "%u %i", u4FsDhcp6SrvPoolIndex ,i4SetValFsDhcp6SrvPoolPreference)
#define nmhSetFsDhcp6SrvPoolDuidType(u4FsDhcp6SrvPoolIndex ,i4SetValFsDhcp6SrvPoolDuidType)	\
	nmhSetCmn(FsDhcp6SrvPoolDuidType, 13, FsDhcp6SrvPoolDuidTypeSet,D6SrMainLock,D6SrMainUnLock, 0, 0, 1, "%u %i", u4FsDhcp6SrvPoolIndex ,i4SetValFsDhcp6SrvPoolDuidType)
#define nmhSetFsDhcp6SrvPoolDuidIfIndex(u4FsDhcp6SrvPoolIndex ,i4SetValFsDhcp6SrvPoolDuidIfIndex)	\
	nmhSetCmn(FsDhcp6SrvPoolDuidIfIndex, 13, FsDhcp6SrvPoolDuidIfIndexSet,D6SrMainLock,D6SrMainUnLock, 0, 0, 1, "%u %i", u4FsDhcp6SrvPoolIndex ,i4SetValFsDhcp6SrvPoolDuidIfIndex)
#define nmhSetFsDhcp6SrvPoolRowStatus(u4FsDhcp6SrvPoolIndex ,i4SetValFsDhcp6SrvPoolRowStatus)	\
	nmhSetCmn(FsDhcp6SrvPoolRowStatus, 13, FsDhcp6SrvPoolRowStatusSet,D6SrMainLock,D6SrMainUnLock, 0, 1, 1, "%u %i", u4FsDhcp6SrvPoolIndex ,i4SetValFsDhcp6SrvPoolRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDhcp6SrvIncludePrefixContextId[13];
extern UINT4 FsDhcp6SrvIncludePrefixIndex[13];
extern UINT4 FsDhcp6SrvIncludePrefix[13];
extern UINT4 FsDhcp6SrvIncludePrefixPool[13];
extern UINT4 FsDhcp6SrvIncludePrefixRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDhcp6SrvIncludePrefix(i4FsDhcp6SrvIncludePrefixContextId , i4FsDhcp6SrvIncludePrefixIndex ,pSetValFsDhcp6SrvIncludePrefix)	\
	nmhSetCmn(FsDhcp6SrvIncludePrefix, 13, FsDhcp6SrvIncludePrefixSet, D6SrMainLock, D6SrMainUnLock, 0, 0, 2, "%i %i %s", i4FsDhcp6SrvIncludePrefixContextId , i4FsDhcp6SrvIncludePrefixIndex ,pSetValFsDhcp6SrvIncludePrefix)
#define nmhSetFsDhcp6SrvIncludePrefixPool(i4FsDhcp6SrvIncludePrefixContextId , i4FsDhcp6SrvIncludePrefixIndex ,i4SetValFsDhcp6SrvIncludePrefixPool)	\
	nmhSetCmn(FsDhcp6SrvIncludePrefixPool, 13, FsDhcp6SrvIncludePrefixPoolSet, D6SrMainLock, D6SrMainUnLock, 0, 0, 2, "%i %i %i", i4FsDhcp6SrvIncludePrefixContextId , i4FsDhcp6SrvIncludePrefixIndex ,i4SetValFsDhcp6SrvIncludePrefixPool)
#define nmhSetFsDhcp6SrvIncludePrefixRowStatus(i4FsDhcp6SrvIncludePrefixContextId , i4FsDhcp6SrvIncludePrefixIndex ,i4SetValFsDhcp6SrvIncludePrefixRowStatus)	\
	nmhSetCmn(FsDhcp6SrvIncludePrefixRowStatus, 13, FsDhcp6SrvIncludePrefixRowStatusSet, D6SrMainLock, D6SrMainUnLock, 0, 1, 2, "%i %i %i", i4FsDhcp6SrvIncludePrefixContextId , i4FsDhcp6SrvIncludePrefixIndex ,i4SetValFsDhcp6SrvIncludePrefixRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDhcp6SrvOptionIndex[13];
extern UINT4 FsDhcp6SrvOptionType[13];
extern UINT4 FsDhcp6SrvOptionLength[13];
extern UINT4 FsDhcp6SrvOptionValue[13];
extern UINT4 FsDhcp6SrvOptionPreference[13];
extern UINT4 FsDhcp6SrvOptionRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDhcp6SrvOptionType(u4FsDhcp6SrvPoolIndex , u4FsDhcp6SrvOptionIndex ,i4SetValFsDhcp6SrvOptionType)	\
	nmhSetCmn(FsDhcp6SrvOptionType, 13, FsDhcp6SrvOptionTypeSet,D6SrMainLock,D6SrMainUnLock, 0, 0, 2, "%u %u %i", u4FsDhcp6SrvPoolIndex , u4FsDhcp6SrvOptionIndex ,i4SetValFsDhcp6SrvOptionType)
#define nmhSetFsDhcp6SrvOptionLength(u4FsDhcp6SrvPoolIndex , u4FsDhcp6SrvOptionIndex ,i4SetValFsDhcp6SrvOptionLength)	\
	nmhSetCmn(FsDhcp6SrvOptionLength, 13, FsDhcp6SrvOptionLengthSet,D6SrMainLock,D6SrMainUnLock, 0, 0, 2, "%u %u %i", u4FsDhcp6SrvPoolIndex , u4FsDhcp6SrvOptionIndex ,i4SetValFsDhcp6SrvOptionLength)
#define nmhSetFsDhcp6SrvOptionValue(u4FsDhcp6SrvPoolIndex , u4FsDhcp6SrvOptionIndex ,pSetValFsDhcp6SrvOptionValue)	\
	nmhSetCmn(FsDhcp6SrvOptionValue, 13, FsDhcp6SrvOptionValueSet,D6SrMainLock,D6SrMainUnLock, 0, 0, 2, "%u %u %s", u4FsDhcp6SrvPoolIndex , u4FsDhcp6SrvOptionIndex ,pSetValFsDhcp6SrvOptionValue)
#define nmhSetFsDhcp6SrvOptionPreference(u4FsDhcp6SrvPoolIndex , u4FsDhcp6SrvOptionIndex ,i4SetValFsDhcp6SrvOptionPreference)	\
	nmhSetCmn(FsDhcp6SrvOptionPreference, 13, FsDhcp6SrvOptionPreferenceSet,D6SrMainLock,D6SrMainUnLock, 0, 0, 2, "%u %u %i", u4FsDhcp6SrvPoolIndex , u4FsDhcp6SrvOptionIndex ,i4SetValFsDhcp6SrvOptionPreference)
#define nmhSetFsDhcp6SrvOptionRowStatus(u4FsDhcp6SrvPoolIndex , u4FsDhcp6SrvOptionIndex ,i4SetValFsDhcp6SrvOptionRowStatus)	\
	nmhSetCmn(FsDhcp6SrvOptionRowStatus, 13, FsDhcp6SrvOptionRowStatusSet,D6SrMainLock,D6SrMainUnLock, 0, 1, 2, "%u %u %i", u4FsDhcp6SrvPoolIndex , u4FsDhcp6SrvOptionIndex ,i4SetValFsDhcp6SrvOptionRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDhcp6SrvSubOptionType[13];
extern UINT4 FsDhcp6SrvSubOptionLength[13];
extern UINT4 FsDhcp6SrvSubOptionValue[13];
extern UINT4 FsDhcp6SrvSubOptionRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDhcp6SrvSubOptionLength(u4FsDhcp6SrvPoolIndex , u4FsDhcp6SrvOptionIndex , u4FsDhcp6SrvSubOptionType ,i4SetValFsDhcp6SrvSubOptionLength)	\
	nmhSetCmn(FsDhcp6SrvSubOptionLength, 13, FsDhcp6SrvSubOptionLengthSet,D6SrMainLock,D6SrMainUnLock, 0, 0, 3, "%u %u %u %i", u4FsDhcp6SrvPoolIndex , u4FsDhcp6SrvOptionIndex , u4FsDhcp6SrvSubOptionType ,i4SetValFsDhcp6SrvSubOptionLength)
#define nmhSetFsDhcp6SrvSubOptionValue(u4FsDhcp6SrvPoolIndex , u4FsDhcp6SrvOptionIndex , u4FsDhcp6SrvSubOptionType ,pSetValFsDhcp6SrvSubOptionValue)	\
	nmhSetCmn(FsDhcp6SrvSubOptionValue, 13, FsDhcp6SrvSubOptionValueSet,D6SrMainLock,D6SrMainUnLock, 0, 0, 3, "%u %u %u %s", u4FsDhcp6SrvPoolIndex , u4FsDhcp6SrvOptionIndex , u4FsDhcp6SrvSubOptionType ,pSetValFsDhcp6SrvSubOptionValue)
#define nmhSetFsDhcp6SrvSubOptionRowStatus(u4FsDhcp6SrvPoolIndex , u4FsDhcp6SrvOptionIndex , u4FsDhcp6SrvSubOptionType ,i4SetValFsDhcp6SrvSubOptionRowStatus)	\
	nmhSetCmn(FsDhcp6SrvSubOptionRowStatus, 13, FsDhcp6SrvSubOptionRowStatusSet,D6SrMainLock,D6SrMainUnLock, 0, 1, 3, "%u %u %u %i", u4FsDhcp6SrvPoolIndex , u4FsDhcp6SrvOptionIndex , u4FsDhcp6SrvSubOptionType ,i4SetValFsDhcp6SrvSubOptionRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDhcp6SrvClientIndex[13];
extern UINT4 FsDhcp6SrvClientId[13];
extern UINT4 FsDhcp6SrvClientIdType[13];
extern UINT4 FsDhcp6SrvClientRealm[13];
extern UINT4 FsDhcp6SrvClientRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDhcp6SrvClientId(u4FsDhcp6SrvClientIndex ,pSetValFsDhcp6SrvClientId)	\
	nmhSetCmn(FsDhcp6SrvClientId, 13, FsDhcp6SrvClientIdSet,D6SrMainLock,D6SrMainUnLock, 0, 0, 1, "%u %s", u4FsDhcp6SrvClientIndex ,pSetValFsDhcp6SrvClientId)
#define nmhSetFsDhcp6SrvClientIdType(u4FsDhcp6SrvClientIndex ,i4SetValFsDhcp6SrvClientIdType)	\
	nmhSetCmn(FsDhcp6SrvClientIdType, 13, FsDhcp6SrvClientIdTypeSet,D6SrMainLock,D6SrMainUnLock, 0, 0, 1, "%u %i", u4FsDhcp6SrvClientIndex ,i4SetValFsDhcp6SrvClientIdType)
#define nmhSetFsDhcp6SrvClientRealm(u4FsDhcp6SrvClientIndex ,u4SetValFsDhcp6SrvClientRealm)	\
	nmhSetCmn(FsDhcp6SrvClientRealm, 13, FsDhcp6SrvClientRealmSet,D6SrMainLock,D6SrMainUnLock, 0, 0, 1, "%u %u", u4FsDhcp6SrvClientIndex ,u4SetValFsDhcp6SrvClientRealm)
#define nmhSetFsDhcp6SrvClientRowStatus(u4FsDhcp6SrvClientIndex ,i4SetValFsDhcp6SrvClientRowStatus)	\
	nmhSetCmn(FsDhcp6SrvClientRowStatus, 13, FsDhcp6SrvClientRowStatusSet,D6SrMainLock,D6SrMainUnLock, 0, 1, 1, "%u %i", u4FsDhcp6SrvClientIndex ,i4SetValFsDhcp6SrvClientRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDhcp6SrvRealmIndex[13];
extern UINT4 FsDhcp6SrvRealmName[13];
extern UINT4 FsDhcp6SrvRealmRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDhcp6SrvRealmName(u4FsDhcp6SrvRealmIndex ,pSetValFsDhcp6SrvRealmName)	\
	nmhSetCmn(FsDhcp6SrvRealmName, 13, FsDhcp6SrvRealmNameSet,D6SrMainLock,D6SrMainUnLock, 0, 0, 1, "%u %s", u4FsDhcp6SrvRealmIndex ,pSetValFsDhcp6SrvRealmName)
#define nmhSetFsDhcp6SrvRealmRowStatus(u4FsDhcp6SrvRealmIndex ,i4SetValFsDhcp6SrvRealmRowStatus)	\
	nmhSetCmn(FsDhcp6SrvRealmRowStatus, 13, FsDhcp6SrvRealmRowStatusSet,D6SrMainLock,D6SrMainUnLock, 0, 1, 1, "%u %i", u4FsDhcp6SrvRealmIndex ,i4SetValFsDhcp6SrvRealmRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDhcp6SrvKeyIdentifier[13];
extern UINT4 FsDhcp6SrvKey[13];
extern UINT4 FsDhcp6SrvKeyRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDhcp6SrvKey(u4FsDhcp6SrvRealmIndex , u4FsDhcp6SrvKeyIdentifier ,pSetValFsDhcp6SrvKey)	\
	nmhSetCmn(FsDhcp6SrvKey, 13, FsDhcp6SrvKeySet,D6SrMainLock,D6SrMainUnLock, 0, 0, 2, "%u %u %s", u4FsDhcp6SrvRealmIndex , u4FsDhcp6SrvKeyIdentifier ,pSetValFsDhcp6SrvKey)
#define nmhSetFsDhcp6SrvKeyRowStatus(u4FsDhcp6SrvRealmIndex , u4FsDhcp6SrvKeyIdentifier ,i4SetValFsDhcp6SrvKeyRowStatus)	\
	nmhSetCmn(FsDhcp6SrvKeyRowStatus, 13, FsDhcp6SrvKeyRowStatusSet,D6SrMainLock,D6SrMainUnLock, 0, 1, 2, "%u %u %i", u4FsDhcp6SrvRealmIndex , u4FsDhcp6SrvKeyIdentifier ,i4SetValFsDhcp6SrvKeyRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDhcp6SrvIfIndex[13];
extern UINT4 FsDhcp6SrvIfPool[13];
extern UINT4 FsDhcp6SrvIfCounterReset[13];
extern UINT4 FsDhcp6SrvIfRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDhcp6SrvIfPool(i4FsDhcp6SrvIfIndex ,i4SetValFsDhcp6SrvIfPool)	\
	nmhSetCmn(FsDhcp6SrvIfPool, 13, FsDhcp6SrvIfPoolSet,D6SrMainLock,D6SrMainUnLock, 0, 0, 1, "%i %i", i4FsDhcp6SrvIfIndex ,i4SetValFsDhcp6SrvIfPool)
#define nmhSetFsDhcp6SrvIfCounterReset(i4FsDhcp6SrvIfIndex ,i4SetValFsDhcp6SrvIfCounterReset)	\
	nmhSetCmn(FsDhcp6SrvIfCounterReset, 13, FsDhcp6SrvIfCounterResetSet,D6SrMainLock,D6SrMainUnLock, 0, 0, 1, "%i %i", i4FsDhcp6SrvIfIndex ,i4SetValFsDhcp6SrvIfCounterReset)
#define nmhSetFsDhcp6SrvIfRowStatus(i4FsDhcp6SrvIfIndex ,i4SetValFsDhcp6SrvIfRowStatus)	\
	nmhSetCmn(FsDhcp6SrvIfRowStatus, 13, FsDhcp6SrvIfRowStatusSet,D6SrMainLock,D6SrMainUnLock, 0, 1, 1, "%i %i", i4FsDhcp6SrvIfIndex ,i4SetValFsDhcp6SrvIfRowStatus)

#endif
