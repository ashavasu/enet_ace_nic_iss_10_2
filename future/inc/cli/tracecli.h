/******************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: tracecli.h,v 1.2 2010/10/29 13:04:51 prabuc Exp $
 *
 * Description: This File contains the constants and prototypes used for
 *              processing the Traceroute CLI command
 *
 ******************************************************************************/

#ifndef __TRCCLI_H__
#define __TRCCLI_H__

#include "cli.h"

#define  CLI_TRACE_MAX_ARGS         7

INT4 cli_process_trace_cmd (tCliHandle CliHandle, UINT4 u4Comamnd, ...);

/* command identification */
enum {
        CLI_TRACE_INIT = 1,
        CLI_TRACE_MAX_CMDS
};

#endif

/******************************** END OF FILE *********************************/
