/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmptucli.h,v 1.1 2009/10/23 10:21:38 prabuc Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMITunlIfAddressType[14];
extern UINT4 FsMITunlIfLocalInetAddress[14];
extern UINT4 FsMITunlIfRemoteInetAddress[14];
extern UINT4 FsMITunlIfEncapsMethod[14];
extern UINT4 FsMITunlIfConfigID[14];
extern UINT4 FsMITunlIfHopLimit[14];
extern UINT4 FsMITunlIfTOS[14];
extern UINT4 FsMITunlIfFlowLabel[14];
extern UINT4 FsMITunlIfDirFlag[14];
extern UINT4 FsMITunlIfDirection[14];
extern UINT4 FsMITunlIfEncaplmt[14];
extern UINT4 FsMITunlIfEncapOption[14];
extern UINT4 FsMITunlIfAlias[14];
extern UINT4 FsMITunlIfCksumFlag[14];
extern UINT4 FsMITunlIfPmtuFlag[14];
extern UINT4 FsMITunlIfStatus[14];
