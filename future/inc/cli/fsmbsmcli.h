/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmbsmcli.h,v 1.3 2008/10/31 13:09:30 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 MbsmMaxNumOfLCSlots[11];
extern UINT4 MbsmMaxNumOfSlots[11];
extern UINT4 MbsmMaxNumOfPortsPerLC[11];
extern UINT4 MbsmLoadSharingFlag[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetMbsmMaxNumOfLCSlots(i4SetValMbsmMaxNumOfLCSlots)	\
	nmhSetCmnWithLock(MbsmMaxNumOfLCSlots, 11, MbsmMaxNumOfLCSlotsSet, MbsmLock, MbsmUnLock, 0, 0, 0, "%i", i4SetValMbsmMaxNumOfLCSlots)
#define nmhSetMbsmMaxNumOfSlots(i4SetValMbsmMaxNumOfSlots)	\
	nmhSetCmnWithLock(MbsmMaxNumOfSlots, 11, MbsmMaxNumOfSlotsSet, MbsmLock, MbsmUnLock, 0, 0, 0, "%i", i4SetValMbsmMaxNumOfSlots)
#define nmhSetMbsmMaxNumOfPortsPerLC(i4SetValMbsmMaxNumOfPortsPerLC)	\
	nmhSetCmnWithLock(MbsmMaxNumOfPortsPerLC, 11, MbsmMaxNumOfPortsPerLCSet, MbsmLock, MbsmUnLock, 0, 0, 0, "%i", i4SetValMbsmMaxNumOfPortsPerLC)
#define nmhSetMbsmLoadSharingFlag(i4SetValMbsmLoadSharingFlag)	\
	nmhSetCmnWithLock(MbsmLoadSharingFlag, 11, MbsmLoadSharingFlagSet, MbsmLock, MbsmUnLock, 0, 0, 0, "%i", i4SetValMbsmLoadSharingFlag)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 MbsmSlotId[12];
extern UINT4 MbsmSlotModuleType[12];
extern UINT4 MbsmSlotModuleStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetMbsmSlotModuleType(i4MbsmSlotId ,i4SetValMbsmSlotModuleType)	\
	nmhSetCmnWithLock(MbsmSlotModuleType, 12, MbsmSlotModuleTypeSet, MbsmLock, MbsmUnLock, 0, 0, 1, "%i %i", i4MbsmSlotId ,i4SetValMbsmSlotModuleType)
#define nmhSetMbsmSlotModuleStatus(i4MbsmSlotId ,i4SetValMbsmSlotModuleStatus)	\
	nmhSetCmnWithLock(MbsmSlotModuleStatus, 12, MbsmSlotModuleStatusSet, MbsmLock, MbsmUnLock, 0, 1, 1, "%i %i", i4MbsmSlotId ,i4SetValMbsmSlotModuleStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 MbsmLCIndex[12];
extern UINT4 MbsmLCName[12];
extern UINT4 MbsmLCMaxPorts[12];
extern UINT4 MbsmLCRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetMbsmLCName(i4MbsmLCIndex ,pSetValMbsmLCName)	\
	nmhSetCmnWithLock(MbsmLCName, 12, MbsmLCNameSet, MbsmLock, MbsmUnLock, 0, 0, 1, "%i %s", i4MbsmLCIndex ,pSetValMbsmLCName)
#define nmhSetMbsmLCMaxPorts(i4MbsmLCIndex ,i4SetValMbsmLCMaxPorts)	\
	nmhSetCmnWithLock(MbsmLCMaxPorts, 12, MbsmLCMaxPortsSet, MbsmLock, MbsmUnLock, 0, 0, 1, "%i %i", i4MbsmLCIndex ,i4SetValMbsmLCMaxPorts)
#define nmhSetMbsmLCRowStatus(i4MbsmLCIndex ,i4SetValMbsmLCRowStatus)	\
	nmhSetCmnWithLock(MbsmLCRowStatus, 12, MbsmLCRowStatusSet, MbsmLock, MbsmUnLock, 0, 1, 1, "%i %i", i4MbsmLCIndex ,i4SetValMbsmLCRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 MbsmLCPortIndex[12];
extern UINT4 MbsmLCPortIfType[12];
extern UINT4 MbsmLCPortSpeed[12];
extern UINT4 MbsmLCPortHighSpeed[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetMbsmLCPortIfType(i4MbsmLCIndex , i4MbsmLCPortIndex ,i4SetValMbsmLCPortIfType)	\
	nmhSetCmnWithLock(MbsmLCPortIfType, 12, MbsmLCPortIfTypeSet, MbsmLock, MbsmUnLock, 0, 0, 2, "%i %i %i", i4MbsmLCIndex , i4MbsmLCPortIndex ,i4SetValMbsmLCPortIfType)
#define nmhSetMbsmLCPortSpeed(i4MbsmLCIndex , i4MbsmLCPortIndex ,u4SetValMbsmLCPortSpeed)	\
	nmhSetCmnWithLock(MbsmLCPortSpeed, 12, MbsmLCPortSpeedSet, MbsmLock, MbsmUnLock, 0, 0, 2, "%i %i %u", i4MbsmLCIndex , i4MbsmLCPortIndex ,u4SetValMbsmLCPortSpeed)
#define nmhSetMbsmLCPortHighSpeed(i4MbsmLCIndex , i4MbsmLCPortIndex ,u4SetValMbsmLCPortHighSpeed)	\
	nmhSetCmnWithLock(MbsmLCPortHighSpeed, 12, MbsmLCPortHighSpeedSet, MbsmLock, MbsmUnLock, 0, 0, 2, "%i %i %u", i4MbsmLCIndex , i4MbsmLCPortIndex ,u4SetValMbsmLCPortHighSpeed)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 MbsmLCConfigSlotId[12];
extern UINT4 MbsmLCConfigCardName[12];
extern UINT4 MbsmLCConfigStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetMbsmLCConfigCardName(i4MbsmLCConfigSlotId ,pSetValMbsmLCConfigCardName)	\
	nmhSetCmnWithLock(MbsmLCConfigCardName, 12, MbsmLCConfigCardNameSet, MbsmLock, MbsmUnLock, 0, 0, 1, "%i %s", i4MbsmLCConfigSlotId ,pSetValMbsmLCConfigCardName)
#define nmhSetMbsmLCConfigStatus(i4MbsmLCConfigSlotId ,i4SetValMbsmLCConfigStatus)	\
	nmhSetCmnWithLock(MbsmLCConfigStatus, 12, MbsmLCConfigStatusSet, MbsmLock, MbsmUnLock, 0, 0, 1, "%i %i", i4MbsmLCConfigSlotId ,i4SetValMbsmLCConfigStatus)

#endif
