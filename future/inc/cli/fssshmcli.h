/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssshmcli.h,v 1.3 2012/12/12 15:10:35 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 SshVersionCompatibility[10];
extern UINT4 SshCipherList[10];
extern UINT4 SshMacList[10];
extern UINT4 SshTrace[10];
extern UINT4 SshStatus[10];
extern UINT4 SshTransportMaxAllowedBytes[10];
extern UINT4 SshSrvBindAddr[10];
extern UINT4 SshServerBindPortNo[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetSshVersionCompatibility(i4SetValSshVersionCompatibility) \
 nmhSetCmn(SshVersionCompatibility, 10, SshVersionCompatibilitySet, NULL, NULL, 0, 0, 0, "%i", i4SetValSshVersionCompatibility)
#define nmhSetSshCipherList(i4SetValSshCipherList) \
 nmhSetCmn(SshCipherList, 10, SshCipherListSet, NULL, NULL, 0, 0, 0, "%i", i4SetValSshCipherList)
#define nmhSetSshMacList(i4SetValSshMacList) \
 nmhSetCmn(SshMacList, 10, SshMacListSet, NULL, NULL, 0, 0, 0, "%i", i4SetValSshMacList)
#define nmhSetSshTrace(i4SetValSshTrace) \
 nmhSetCmn(SshTrace, 10, SshTraceSet, NULL, NULL, 0, 0, 0, "%i", i4SetValSshTrace)
#define nmhSetSshStatus(i4SetValSshStatus) \
 nmhSetCmn(SshStatus, 10, SshStatusSet, NULL, NULL, 0, 0, 0, "%i", i4SetValSshStatus)
#define nmhSetSshTransportMaxAllowedBytes(i4SetValSshTransportMaxAllowedBytes) \
 nmhSetCmn(SshTransportMaxAllowedBytes, 10, SshTransportMaxAllowedBytesSet, NULL, NULL, 0, 0, 0, "%i", i4SetValSshTransportMaxAllowedBytes)
#define nmhSetSshSrvBindAddr(pSetValSshSrvBindAddr) \
 nmhSetCmn(SshSrvBindAddr, 10, SshSrvBindAddrSet, NULL, NULL, 0, 0, 0, "%s", pSetValSshSrvBindAddr)
#define nmhSetSshServerBindPortNo(u4SetValSshServerBindPortNo) \
 nmhSetCmn(SshServerBindPortNo, 10, SshServerBindPortNoSet, NULL, NULL, 0, 0, 0, "%u", u4SetValSshServerBindPortNo)

#endif
