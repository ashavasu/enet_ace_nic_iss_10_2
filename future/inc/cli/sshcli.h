/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: sshcli.h,v 1.7 2012/12/12 15:10:35 siva Exp $
 *
 * Description: This has macros and prototypes used by ssh and cli. 
 *
 ***********************************************************************/

#ifndef __SSHCLI_H__
#define __SSHCLI_H__

#include "cli.h"

/* Command Identifiers */
enum {
    CLI_SSH_SHOW_IPSSH = 1,
    CLI_ISS_SHOW_SSH_CONF,
    CLI_SSH_DEBUG,    
    CLI_SSH_NO_DEBUG,    
    CLI_SSH_IPSSH,
    CLI_SSH_NO_IPSSH,
    CLI_SSH_SET_STATUS,
    CLI_SSH_SERVER_CONF,
    CLI_SSH_MAX_COMMANDS,
    CLI_SSH_MAX_ALLOWED_BYTE,
    CLI_SSH_SERVER_PUB,
    CLI_SSH_SERVER_PUB_DELETE
};


/* max no of variable inputs to cli parser */
#define SSH_CLI_MAX_ARGS               10

#define CLI_SSH_VERSION            0x0001
#define CLI_SSH_CIPHER_ALGO_LIST   0x0002
#define CLI_SSH_MAC_ALGO_LIST      0x0003

#define CLI_SSH_VERSION_1          0x0001
#define CLI_SSH_VERSION_2          0x0002
#define CLI_SSH_3DES_CBC           0x0001
#define CLI_SSH_DES_CBC            0x0002
#define CLI_SSH_HMAC_SHA1          0x0001
#define CLI_SSH_HMAC_MD5           0x0002
#define SSH_ERR_MSG_SIZE           128

#define SSH_ENABLE                 1
#define SSH_DISABLE                2
#define TRANSPORT_MIN_ALLOWED_BYTE 1
#define TRANSPORT_MAX_ALLOWED_BYTE 32768
/* Error codes 
 * Error code values and 
 * Error strings are maintained inside the protocol module itself.
 */
enum {
    CLI_SSH_INVALID_TRC = 1,
    CLI_SSH_INVALID_VERSION,
    CLI_SSH_INVALID_CIPHER_ALGO,
    CLI_SSH_INVALID_MAC_ALGO,
    CLI_SSH_ERR_INVALID_SESSION_STATUS,
    CLI_SSH_MAX_ERR,
    CLI_SSH_INVALID_BYTE,
    CLI_SSH_INVALID_PORT
};


/* The error strings should be places under the switch so as to avoid redifinition
 * This will be visible only in modulecli.c file
 */

#ifdef __SSHCLI_C__

CONST CHR1  *SshCliErrString [] = {
    NULL,
    "\r% Invalid Trace Level\r\n",
    "\r% Non-compatible version type\r\n",
    "\r% Invalid Cipher algo specified\r\n",
    "\r% Invalid Mac algo specified\r\n",
    "\r% Invalid SSH Session Status\r\n"
};

#endif

INT4 cli_process_ssh_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command, ...));

#define SSH_PUBL_SAVE_FILE                 "authorized_keys" 
#define CURRENT_DIRECTORY_SIZE              128    /* Length of FLASH size Path */
#define SSH_MAX_PUB_KEY_SIZE             748    /* For setting the aurhorised /
                                                   key in FLASH */

#endif /* __SSHCLI_H__ */

