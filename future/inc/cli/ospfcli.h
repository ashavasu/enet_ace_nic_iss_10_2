/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ospfcli.h,v 1.67 2017/03/10 12:45:33 siva Exp $
 *
 * Description: This file contains the data structure and macros 
 *              for OSPF CLI module 
 *
 *******************************************************************/
#ifndef __OSPFCLI_H__
#define __OSPFCLI_H__

#include "lr.h"
#include "cli.h"
#include "ip.h"

/* Command Identifiers */
enum {
    OSPF_CLI_SHOW_INTERFACE,
    OSPF_CLI_SHOW_VLAN_INTERFACE,
    OSPF_CLI_SHOW_NBR,
    OSPF_CLI_SHOW_REQ_LSA,
    OSPF_CLI_SHOW_TRANS_LSA,
    OSPF_CLI_SHOW_VIRTUALLINKS,
    OSPF_CLI_SHOW_BORDERROUTERS, 
    OSPF_CLI_SHOW_SUMMARY_ADDRESS,
    OSPF_CLI_SHOW_EXT_SUMMARY_ADDR, 
    OSPF_CLI_SHOW_OSPF,
    OSPF_CLI_SHOW_ROUTETABLE,
    OSPF_CLI_SHOWDATABASE,
    OSPF_CLI_SHOW_LSA_DATABASE,
    OSPF_CLI_SHOW_REDUNDANCY,
    OSPF_CLI_SET_TRACE_LEVEL,
    OSPF_CLI_RESET_TRACE_LEVEL,
    OSPF_CLI_ROUTEROSPF,
    OSPF_CLI_NO_ROUTEROSPF,
    OSPF_CLI_ROUTERID,
    OSPF_CLI_NO_ROUTERID,
    OSPF_CLI_STABILITY_INT,
    OSPF_CLI_DEF_STABILITY_INT,
    OSPF_CLI_TRANS_ROLE,
    OSPF_CLI_DEF_TRANS_ROLE,
    OSPF_CLI_COMPATRFC1583,
    OSPF_CLI_NO_COMPATRFC1583,
    OSPF_CLI_SET_ABR_TYPE,
    OSPF_CLI_NBR,
    OSPF_CLI_NO_NBR,
    OSPF_CLI_DEF_NBR_PARAMS,
    OSPF_CLI_AREA_DEFAULT_COST,
    OSPF_CLI_DEF_AREACOST,
    OSPF_CLI_AREANSSA,
    OSPF_CLI_DEF_AREANSSA,
    OSPF_CLI_AREASTUB,
    OSPF_CLI_DEF_AREATYPE,
    OSPF_CLI_DEL_AREA,
    OSPF_CLI_DEFAULT_INFO,
    OSPF_CLI_NO_DEFAULT_INFO,
    OSPF_CLI_VIRTUAL_LINK,
    OSPF_CLI_NO_VIRTUAL_LINK,
    OSPF_CLI_DEFAULT_METRIC,
    OSPF_CLI_DEL_DEFAULT_METRIC,
    OSPF_CLI_ASBR,
    OSPF_CLI_NO_ASBR,
    OSPF_CLI_SUMMARY,
    OSPF_CLI_DEL_SUMM_ADDR,
    OSPF_CLI_ADD_EXT_SUMM_ADDR,
    OSPF_CLI_DEL_EXT_SUMM_ADDR,
    OSPF_CLI_REDISTRIBUTE,
    OSPF_CLI_DEF_REDISTRIBUTE,
    OSPF_CLI_DISTRIBUTE_LIST,
    OSPF_CLI_REDIST_CONFIG,
    OSPF_CLI_DEL_REDIST_CONFIG,
    OSPF_CLI_NETWORK_AREA,
    OSPF_CLI_DEL_NETWORK_AREA,
    OSPF_CLI_NSSA_ASBR_DFRT,
    OSPF_CLI_NSSA_ASBR_NO_DFRT,
    OSPF_CLI_IF_PASSIVE,
    OSPF_CLI_IF_NO_PASSIVE,
    OSPF_CLI_ALL_IF_PASSIVE,
    OSPF_CLI_ALL_IF_NO_PASSIVE,
    OSPF_CLI_DEMANDCIRCUIT,
    OSPF_CLI_NO_DEMANDCIRCUIT,
    OSPF_CLI_RETRANSMITINT,
    OSPF_CLI_DEF_RETRANS,
    OSPF_CLI_TRANSMITDELAY,
    OSPF_CLI_DEF_TRANSMITDELAY,
    OSPF_CLI_PRIORITY,
    OSPF_CLI_DEF_PRIORITY,
    OSPF_CLI_HELLOINT,
    OSPF_CLI_DEF_HELLO,
    OSPF_CLI_DEADINT,
    OSPF_CLI_DEF_DEADINT,
    OSPF_CLI_COST,
    OSPF_CLI_DEF_COST,
    OSPF_CLI_IF_NETWORK_TYPE,
    OSPF_CLI_DEF_NETWORK,
    OSPF_CLI_AUTHKEY,
    OSPF_CLI_NO_AUTHKEY,
    OSPF_CLI_AUTHTYPE,
    OSPF_CLI_DEF_IFAUTH,
    OSPF_CLI_MESSAGEDIGESTKEY,
    OSPF_CLI_DEL_MESSAGEDIGESTKEY,
    OSPF_CLI_OPQ,
    OSPF_CLI_NO_OPQ,
    OSPF_CLI_RESTART_SUPPORT,
    OSPF_CLI_GRACE_PERIOD,
    OSPF_CLI_HELPER_SUPPORT,
    OSPF_CLI_NO_HELPER_SUPPORT,
    OSPF_CLI_STRICT_LSA_CHECK,
    OSPF_CLI_HELPER_GRACE_LIMIT,
    OSPF_CLI_GR_ACK_STATE,
    OSPF_CLI_GRLSA_RETRANS_COUNT,
    OSPF_CLI_RESTART_REASON,
    OSPF_CLI_STAGGERING_STATUS,
    OSPF_CLI_STAGGERING_INTERVAL,
    OSPF_CLI_DISTANCE,
    OSPF_CLI_NO_DISTANCE,
    OSPF_CLI_SPF_TIMERS,
    OSPF_CLI_NO_SPF_TIMERS,
    OSPF_CLI_MD5_KEY_ACCEPT_START,
    OSPF_CLI_MD5_KEY_GEN_START,
    OSPF_CLI_MD5_KEY_GEN_STOP,
    OSPF_CLI_MD5_KEY_ACCEPT_STOP,
    OSPF_CLI_BFD_ADMIN_STATUS,
    OSPF_CLI_BFD_STATUS_ALL_INTF,
    OSPF_CLI_BFD_STATUS_SPECIFIC_INTF,
    OSPF_CLI_VIRT_KEY_ACCEPT_START,
    OSPF_CLI_VIRT_KEY_GENERATE_START,
    OSPF_CLI_VIRT_KEY_GENERATE_STOP,
    OSPF_CLI_VIRT_KEY_ACCEPT_STOP,
    OSPF_CLI_SHOW_RED_EXT_RT_MSG,
    OSPF_CLI_ADD_HOST_METRIC,
    OSPF_CLI_DEL_HOST_METRIC,
    OSPF_CLI_IP_OSPF_BFD
};


/* max no of variable inputs to cli parser */
#define OSPF_CLI_MAX_ARGS         15
#define OSPF_CLI_NULL_STR         ""
#define OSPF_CLI_DEF_TOS           0
#define OSPF_CLI_INV_VALUE        -1

#define OSPF_BFD_ENABLED 1
#define OSPF_BFD_DISABLED 2

#define OSPF_INVALID_IFINDEX       -1
#define CLI_OSPF_DEFAULT_PREFERENCE    110

/* macro used in configuring neighbor which takes a neighbor address and 
 * a default Addressless interface index as the table indices
 */
#define OSPF_CLI_DEF_ADDRLS_IFINDEX 0

/* Show neighbor options */
#define OSPF_SHOW_NBR_INTF        0x01
#define OSPF_SHOW_NBR_ID          0x02
#define OSPF_SHOW_NBR_DETAIL      0x04
#define OSPF_SHOW_NBR_ALL         0x08
#define OSPF_NETWORK_DEFAULT_TYPE 0x00

#define OSPF_CLI_ADVERTISE           1
#define OSPF_CLI_DO_NOT_ADVERTISE    2

/*constants to Set the Virtual Link If table*/
#define OSPF_VIRT_NO_OPTIONS      0x01
#define OSPF_VIRT_HELLO_INT      0x02
#define OSPF_VIRT_RETRANS_INT     0x04
#define OSPF_VIRT_TRANSMIT_DELAY  0x08
#define OSPF_VIRT_DEAD_INT        0x10
#define OSPF_VIRT_AUTH_TYPE     0x20
#define OSPF_VIRT_AUTH_KEY        0x40
#define OSPF_VIRT_MD5_KEY         0x80

/*constants to get the LSA database parameters */
#define LSA_AREA_DATABASE             0x0001
#define LSA_DB_SUMMARY                0x0002
#define LSA_SELF_ORG                  0x0004
#define LSA_ADV_ROUTER                0x0008
#define LSA_LINK_STATE_ID             0x0010
#define LSA_INTERNAL                  0x0020

/* following marcos should be in sync with ospf/inc/osdefn.h */
#define CLI_OSPF_DEF_HELLO_INTERVAL         10 
#define CLI_OSPF_DEF_RTR_DEAD_INTERVAL      40 
#define CLI_OSPF_DEF_IF_TRANS_DELAY         1
#define CLI_OSPF_DEF_RTR_PRIORITY           1
#define CLI_OSPF_DEF_RXMT_INTERVAL          5
#define CLI_OSPF_DEF_NBR_PRIORITY           1
#define CLI_OSPF_DEF_NBR_POLL_INTERVAL      120
#define CLI_OSPF_AS_EXT_DEF_METRIC          10

#define CLI_OSPF_LOCAL_RT_MASK              1
#define CLI_OSPF_STATIC_RT_MASK             2
#define CLI_OSPF_RIP_RT_MASK                3
#define CLI_OSPF_BGP_RT_MASK                4
#define CLI_OSPF_ISISL1_RT_MASK             5
#define CLI_OSPF_ISISL2_RT_MASK             6
#define CLI_OSPF_ISISL1L2_RT_MASK           7
#define CLI_OSPF_ALL_RT_MASK                8

#define CLI_EXTERNAL_TYPE_1                1
#define CLI_EXTERNAL_TYPE_2                2

#define CLI_DEF_METRIC_VAL                10

#define CLI_OSPF_STAGGERING_ENABLED  1
#define CLI_OSPF_STAGGERING_DISABLED 2



/* OSPF Specific Trace Categories */
#define  CLI_OSPF_DISABLEALL_TRC  0x00000000  /* Disable all Trace */

#define  CLI_OSPF_HP_TRC   0x00010000
#define  CLI_OSPF_DDP_TRC  0x00020000
#define  CLI_OSPF_LRQ_TRC  0x00040000
#define  CLI_OSPF_LSU_TRC  0x00080000
#define  CLI_OSPF_LAK_TRC  0x00100000
#define  CLI_OSPF_ISM_TRC  0x00200000
#define  CLI_OSPF_NSM_TRC  0x00400000
#define  CLI_OSPF_RT_TRC   0x00800000

#define  CLI_OSPF_NSSA_TRC   0x04000000
#define  CLI_OSPF_RAG_TRC    0x08000000

/* OSPF MODULE Specific Trace Categories */
#define  CLI_OSPF_CONFIGURATION_TRC  0x10000000
#define  CLI_OSPF_ADJACENCY_TRC      0x20000000
#define  CLI_OSPF_LSDB_TRC           0x40000000
#define  CLI_OSPF_PPP_TRC            0x80000000
#define  CLI_OSPF_RTMODULE_TRC       0x01000000
#define  CLI_OSPF_INTERFACE_TRC      0x02000000

/* OSPF FUNCTION Specific Trace Categories */
#define  CLI_OSPF_FN_ENTRY  0x00001000
#define  CLI_OSPF_FN_EXIT   0x00002000

/* OSPF MEMORY RESOURCE Specific Trace Categories */
#define  CLI_OSPF_MEM_SUCC  0x00004000
#define  CLI_OSPF_MEM_FAIL  0x00008000

/*-- New Packet Dumping Specific definations --*/

/* OSPF Packet Dumping Trace Categories */
#define  CLI_OSPF_DUMP_HGH_TRC  0x00000100  /* Packet HighLevel dump */
#define  CLI_OSPF_DUMP_LOW_TRC  0x00000200  /* Packet LowLevel dump */
#define  CLI_OSPF_DUMP_HEX_TRC  0x00000400  /* Packet Hex dump */
#define  CLI_OSPF_CRITICAL_TRC  0x00000800  /* Critical Trace */

/* Trace specific for OSPF graceful restart */
#define  CLI_OSPF_RESTART_TRC   0x00000100  /* Restarting router module */
#define  CLI_OSPF_HELPER_TRC    0x00000200  /* Helper module */
#define  CLI_OSPF_REDUNDANCY_TRC  0x00000400  /* HA module */

#define MAX_OSPF_BR_INFO    164
#define MAX_OSPF_RT_INFO    148
#define MAX_OSPF_SA_INFO    144
#define MAX_OSPF_VI_INFO    196
#define MAX_OSPF_EXT_SA_INFO    148
/* These Macros are defined for the Testing purpose */
/* These macros needs to de re defined when the Command handler changes */
#define OSPF_CLI_MAX_BR                   5
#define OSPF_CLI_MAX_RT                   4
#define OSPF_CLI_MAX_SA                   5
#define OSPF_CLI_MAX_VI                   3 
/* These Macros are defined for the Testing purpose */

#define OSPF_CLI_IPADDR_TO_STR(pString,u4IpAddr) \
                               CLI_CONVERT_IPADDR_TO_STR(pString,u4IpAddr)

#define OSPF_MAX_NBR_STATE_STR_LEN 30
#define OSPF_IFNAME_LEN            MAX_IFNAME_SIZE
#define OSPF_MAX_IP_STR_LEN        16

/*constants to show LSA*/
#define OSPF_CLI_SHOW_LSA_ALL       1 
#define OSPF_CLI_SHOW_LSA_NBR       2
#define OSPF_CLI_SHOW_LSA_IF        3 
#define OSPF_CLI_SHOW_LSA_IFNBR     4

#define MAX_AUTH_STRING_LEN         8
#define MAX_MD5_AUTH_STRING_LEN    16
#define OSPF_DST_TIME_LEN           20

#define BFD_CLI_MAX_STATE          10

/* Error codes 
 * Error code values and 
 * Error strings are maintained inside the protocol module itself.
 */
enum {
    CLI_OSPF_NO_FREE_ENTRY = 1,
    CLI_OSPF_NOT_ENABLED,
    CLI_OSPF_INV_STATUS,
    CLI_OSPF_ENTRY_EXISTS,
    CLI_OSPF_INV_ENTRY,
    CLI_OSPF_INV_NET,
    CLI_OSPF_SUMMARY_ENABLED,
    CLI_OSPF_INV_TOS_STATUS,
    CLI_OSPF_NO_MD5_KEY,
    CLI_OSPF_INV_AUTHKEY_STATUS,
    CLI_OSPF_OPQ_APP_REG,
    CLI_OSPF_INV_RTR_ID,
    CLI_OSPF_INV_VALUE,
    CLI_OSPF_INV_AREA_TYPE,
    CLI_OSPF_INV_AREA,
    CLI_OSPF_INV_EXT_AGGR,
    CLI_OSPF_NO_BDR_RTR,
    CLI_OSPF_RRD_DISABLED,
    CLI_OSPF_REDIS_DISABLED,
    CLI_OSPF_REDIS_ENABLED, 
    CLI_OSPF_INV_TAGTYPE,
    CLI_OSPF_INV_VIRT_AREA,
    CLI_OSPF_INV_VAREA_STATUS,
    CLI_OSPF_INV_LENGTH,
    CLI_OSPF_INV_AUTHTYPE,
    CLI_OSPF_INV_MD5KEY,
    CLI_OSPF_INV_IFTYPE,
    CLI_OSPF_INV_OSPFIF,
     CLI_OSPF_INV_METRIC,
    CLI_OSPF_INTERFACE_NOT_EXISTS,
    CLI_OSPF_BACKBONE_AREA_ERR,
    CLI_OSPF_INV_ASSOC_RMAP,
    CLI_OSPF_INV_DISASSOC_RMAP,
    CLI_OSPF_NOT_UNNUM_IF,
    CLI_OSPF_NBR_INV_IFACE,
    CLI_OSPF_INV_TRNS_ROLE,
    CLI_OSPF_INV_STAB_INT,
    CLI_OSPF_INV_AREA_NEIGHBOR,
    CLI_OSPF_OPQ_DISABLED,
    CLI_OSPF_OPQ_GR_ENABLED,
    CLI_OSPF_RESTART_INPROGRESS,
    CLI_OSPF_HELPER_DISABLED,
    CLI_OSPF_RESTART_NOT_SUPPORTED,
    CLI_OSPF_RMAP_ASSOC_FAILED,
 CLI_OSPF_AREA_ALLOC_FAILED,
    CLI_OSPF_INCONS_ADDR_MASK,
    CLI_OSPF_INV_VIRT_AREA_NSSA,
    CLI_OSPF_MD5_ENABLED,
    CLI_OSPF_INV_NSSA_VIRT_LINK,
    CLI_OSPF_INV_VIRT_BACKBONE_AREA,
    CLI_OSPF_DEF_ENTRY,
    CLI_OSPF_NBR_LOCAL_IFACE,
    CLI_OSPF_CANNOT_ADD_DEF_RANGE,
    CLI_OSPF_AREA_NOT_CONFIG,
    CLI_OSPF_SUMMARY_ADDR_ALREADY_EXISTS,
    CLI_OSPF_EXCEEDS_MAX_ADDR_RANGES_PER_AREA,
    CLI_OSPF_INVALID_PEER_ADDRESS_ERR,
    CLI_OSPF_MAX_MD5,
    CLI_OSPF_BFD_ADMIN_STATUS_DISABLED,
    CLI_OSPF_BFD_ADMIN_STATUS_ENABLED,
    CLI_OSPF_BFD_ALL_INTF_ENABLED,
    CLI_OSPF_BFD_INTF_DISABLED,
    CLI_OSPF_INV_DEF_COST,
    CLI_OSPF_MISMATCH_VALUE,
    CLI_OSPF_SECONDARY_IP_NOT_ALLOWED,
    CLI_OSPF_VIRT_LINK_EXISTS,
    CLI_OSPF_NO_INTERFACE_ACTIVE,
    CLI_OSPF_OPAQUE_ENABLED,
    CLI_OSPF_PRI_INV_DELETE,
    CLI_OSPF_MAX_SEC_CONF,
    CLI_OSPF_STAG_NOT_ENABLED,
    CLI_OSPF_NO_AUTH_DELETE,
    CLI_OSPF_INVALID_METRIC,
    CLI_OSPF_NO_HOST,
    CLI_OSPF_NO_NBMA_NEIGHBOR,
    CLI_OSPF_MAX_ERR        
};


/* The error strings should be places under the switch so as to avoid redifinition
 * This will be visible only in modulecli.c file
 */
#ifdef __OSPFCLI_C__

CONST CHR1  *OspfCliErrString [] = {
    NULL,
    "% No Free Entry\r\n",
    "% OSPF not enabled\r\n",
    "% Invalid status\r\n",
    "% Entry already exists\r\n",
    "% Invalid Index, Entry doesn't exists\r\n",
    "% Neighbor IP address does not fall into any of the interface network\r\n",
    "% summary should be disabled for Metric Type 1 \r\n",
    "% Invalid Metric TOS Status\r\n",
    "% MD5 Key not configured\r\n",
    "% Invalid Authkey status\r\n",
    "% Cannot disable OSPF, Opq Application is registered\r\n",
    "% Invalid Router ID\r\n",
    "% Invalid Value\r\n",
    "% Invalid Area type\r\n",
    "% Invalid Area address\r\n",
    "% Invalid External summary address\r\n",
    "% Not an AS Border router\r\n",
    "% RRD not enabled\r\n",
    "% Redistribution not enabled\r\n",
    "% Redistribution is enabled\r\n", 
    "% Redistribution Tag type is not set to manual\r\n",
    "% Invalid Virtual area indices\r\n",
    "% Invalid Virtual Interface Status\r\n",
    "% Invalid Key Length\r\n",
    "% Invalid Authentication type\r\n",
    "% Invalid MD5 Authentication key\r\n",
    "% Invalid Interface type\r\n",
    "% OSPF not enabled on this interface\r\n",
    "% Invalid Metric.Exceeded the range\r\n",
    "% Invalid  IP address\r\n",
    "% Backbone area cannot be configured as Stub area or Nssa\r\n",
    "% Route Map Not Associated\r\n",
    "% Route Map Not Associated\r\n",
    "% Not an unnumbered interface\r\n",
    "% Neighbor can be configured on NBMA or point-to-multipoint networks\r\n",
    "% Translator Role can be configured only on NSSA Area\r\n",
    "% Stability Interval can be configured only on NSSA Area\r\n",
    "% Configured Neighbours present on this interface, If Type not set \n",
    "% Opaque functionality is not enabled\r\n",
    "% Opaque Capability can't be disabled, When Graceful restart support is enabled\r\n",
    "% Graceful restart is already in progress\r\n",
    "% Helper mode is disabled\r\n",
    "% Graceful restart is not supported\r\n",
    "% Route-map association failed (probably another route map is used)\r\n",
    "% Max ospf area exceeded\r\n",
    "% Inconsistent address/mask combination \r\n",
    "% Virtual link cannot be configured on NSSA area \r\n",
    "% MD5 Authentication is enabled \r\n",
    "% Area for which virtual link has been configured, cannot serve as NSSA area\r\n",
    "% Backbone area cannot server as transmit area for Virtual link\r\n",
    "% a default cost can be configured only for NSSA/Stub area\r\n",
    "% IP address of local interface cannot be configured\r\n",
    "% Cannot add this range as 0.0.0.0 represents default\r\n",
    "% Area Not Configured\r\n",
    "% Summary address already exist\r\n",
    "% Exceeds Maximum address ranges per area\r\n",
    "% Invalid address for redistibution\r\n",
    "% Maximum MD5 keys allocated is exceeded\r\n",
    "% BFD is not enabled for OSPF\r\n",
    "% Already BFD enabled for OSPF\r\n",
    "% All interfaces are already monitored by BFD \r\n",
    "% Bfd not enabled on this Interface \r\n",
    "% Default-cost can be configured only on NSSA/STUB Area\r\n",
    "% Mismatch between the Input and configured value\r\n", 
    "% Network with secondary IP address is not allowed \r\n",
    "% Virtual link is already mapped with other transit area\r\n",
    "% No Valid IP Address available in the system, Dynamic Router ID failed\r\n",
    "% OSPF Opaque Applications are enabled\r\n",
    "% Unable to delete primary network, when secondary network exists\r\n",
    "% Unable to Configure - Maximum Secondary networks Configured per Interface\r\n",
    "% OSPF route calculation staggering is not enabled\r\n",
    "% Cryptographic Authentication not deleted\r\n",
    "% invalid value for configuring host metric\r\n",
    "% host does not exist\r\n",
    "% No NBMA Neighbor is Configured\r\n"
};

#endif

/*  Area Paraameters Structure */
typedef struct _AreaStruct {
    UINT4       u4RouterId;     /* OSPF Router Id */
    INT4        i4IfRetransInterval;
    INT4        i4IfTransitDelay;
    INT4        i4IfHelloInterval;
    INT4        i4IfRtrDeadInterval;
    INT4        i4IfAuthType;
    UINT4       u4AreaId;
    INT4        i4AuthkeyId;
    INT4        i4IfCryptoAuthType;
    UINT1       au1AuthKey[MAX_MD5_AUTH_STRING_LEN];
    UINT1       u1AreaBitMask;
    UINT1       u1ExtnAreaBitMask;
    UINT1       au1Reserved[2];
}tAreaStruct;

/* Struct for Show LSA Database */
typedef struct _Database {
   UINT4     u4OspfCxtId;
   INT4  i4DatabaseOptions;
   INT4  i4LsdbType;
   UINT4 u4LsId;
   UINT4 u4AdvRouteId;
   UINT4 u4RouterId;
   UINT4 u4AreaId;
} tDatabase;

typedef struct _ShowOspfBRCookie {
    UINT4     u4OspfCxtId;
    UINT4     u4OspfBRIpAddr;
    UINT4     u4OspfBRIpMask;
    UINT4     u4OspfBRNextHop;
    INT4      i4OspfBRTos;
    INT4      i4OspfBRRtType;
 } tShowOspfBRCookie;

typedef struct _ShowOspfBRInfo  {
    UINT4     u4OspfCxtId;
    UINT4     u4OspfBRIpAddr;
    UINT4     u4OspfBRNextHop;
    UINT4     u4OspfBRArea;
    INT4      i4OspfBRRtrType;
    INT4      i4OspfBRRtType;
    INT4      i4OspfBRCost;
    INT4      i4OspfBRTos;
} tShowOspfBRInfo;
 
typedef struct _ShowOspfBR {
    UINT4     u4NoOfBR;
    tShowOspfBRInfo BRInfo[1];
} tShowOspfBR;

typedef struct _ShowOspfRTCookie {
    UINT4     u4OspfCxtId;
    UINT4     u4OspfRTIpAddr;
    UINT4     u4OspfRTIpMask;
    UINT4     u4OspfRTNextHop;
    INT4      i4OspfRTTos;
 } tShowOspfRTCookie;
      
typedef struct _ShowOspfRTInfo  {
    UINT4     u4OspfCxtId;
    UINT4     u4OspfRTIpAddr;
    UINT4     u4OspfRTIpMask;
    UINT4     u4OspfRTNextHop;
    UINT4     u4OspfRTArea;
    UINT4     u4OspfRTIfIndex;
    INT4      i4OspfRTRtType;
    INT4      i4OspfRTCost;
    INT4      i4OspfRTTos;
} tShowOspfRTInfo;
 
typedef struct _ShowOspfRT {
    UINT4     u4NoOfRT;
    tShowOspfRTInfo RTInfo[1];
} tShowOspfRT;

typedef struct _ShowOspfDbInfo {
    UINT4 u4LsaAreaId;
    UINT4 u4LsaLsId;
    UINT4 u4LsaRtrId;
    UINT4 u4LsaAge;
    INT4  i4LsaType;
    INT4  i4LsaSeq;
    INT4 i4LsaCksum;
}tShowOspfDbInfo;

typedef struct _ShowOspfDb {
  UINT4 u4NoOfLsa;
  tShowOspfDbInfo LsaInfo[1];
}tShowOspfDb;

typedef struct _ShowOspfDbCookie {
    UINT4 u4OspfLsdbAreaId;
    UINT4 u4OspfLsdbLsId;
    UINT4 u4OspfLsdbRouterId;
    INT4 i4OspfLsdbType;
} tShowOspfDbCookie;

typedef struct _ShowOspfSACookie { 
    UINT4     u4OspfCxtId;
    UINT4     u4OspfSAAreaId;
    UINT4     u4OspfSANet;
    UINT4     u4OspfSAMask;
    INT4      i4OspfSALsdbType;
 } tShowOspfSACookie;


/* Structure containing the indices for ospfVirtIfTable. */
typedef struct _ShowOspfVICookie {
    UINT4     u4OspfCxtId;
    UINT4     u4OspfVIAreaId;
    UINT4     u4OspfVINbr;
} tShowOspfVICookie;


/* Structure containing the indices for FutOspfAsExternalAggregationTable. */
typedef struct _ShowOspfExtSACookie {
 UINT4  u4OspfCxtId;
 UINT4  u4OspfExtSANet;
 UINT4  u4OspfExtSAMask;
 UINT4  u4OspfExtSAAreaId;
} tShowOspfExtSACookie ;   


typedef struct _ShowOspfSAInfo  {
    UINT4     u4OspfCxtId;
    UINT4     u4OspfSANet;
    UINT4     u4OspfSAMask;
    UINT4     u4OspfSAArea;
    INT4      i4OspfSALsaType;
    INT4      i4OspfSAEffect;
    INT4      i4OspfSATag;
} tShowOspfSAInfo;
 
typedef struct _ShowOspfSA {
    UINT4     u4NoOfSA;
    tShowOspfSAInfo SAInfo[1];
} tShowOspfSA;


/* Structure containing the fields to be displayed for Virtual Link Show 
 * command.
 */
typedef struct _ShowOspfVIInfo  {
    UINT4     u4OspfCxtId;
    UINT4     u4OspfVIAreaId;
    UINT4     u4OspfVINbr;
    INT4      i4OspfVIStatus;
    INT4      i4OspfVNbrState;
    INT4      i4OspfVITransInt;
    INT4      i4OspfVIRetransInt;
    INT4      i4OspfVIHelloInt;
    INT4      i4OspfVIDeadInt;
    INT4      i4OspfVIRestartStatus;
    INT4      i4OspfVIRestartAge;
    INT4      i4OspfVIRstExitReason;
    INT4      i4AuthKeyType ;
    INT4      i4SimpleKeyStatus ;
    INT4      i4Md5KeyStatus ;
    INT4      i4CryptoAuthType;

} tShowOspfVIInfo;
      
/* Buffer for storing 'n' number virtual interface information. 
 * 'n' - number of virtual link is stored in u4NoOfVI 
 */
typedef struct _ShowOspfVI {
    UINT4     u4NoOfVI;
    tShowOspfVIInfo VIInfo[1];
} tShowOspfVI;

/* Structure containing the fields to be displayed for External Summary Address 
 * command.
 */
typedef struct _ShowOspfExtSAInfo {
    UINT4  u4OspfCxtId;
    UINT4  u4OspfExtSANet;
    UINT4  u4OspfExtSAMask;
    UINT4  u4OspfExtSAAreaId;
    INT4   i4OspfExtSATranslation;
    INT4   i4OspfExtSAEffect;
} tShowOspfExtSAInfo;

/* Buffer for storing 'n' number of External Summary address information. 
 * 'n' - number of Summary addresses stored in u4NoOfExtSA.
 */
typedef struct _ShowOspfExtSA{
 UINT4  u4NoOfExtSA;
 tShowOspfExtSAInfo ExtSAInfo[1];
} tShowOspfExtSA;


#define CLI_MODE_ROUTER_OSPF "ospf-rtr-cfg"

extern INT1 nmhGetFsIpRoutingProtoPref PROTO ((INT4, INT4 *));
INT1 OspfGetRouterCfgPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1DispStr));
INT4 cli_process_ospf_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command, ...));
INT4 OspfShowRunningConfigInterfaceDetails(tCliHandle CliHandle,
                                           UINT4 u4IfIndex);

#endif /* __OSPFCLI_H__ */
