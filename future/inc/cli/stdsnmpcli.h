/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdsnmpcli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 SysContact[8];
extern UINT4 SysName[8];
extern UINT4 SysLocation[8];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetSysContact(pSetValSysContact)	\
	nmhSetCmn(SysContact, 8, SysContactSet, NULL, NULL, 0, 0, 0, "%s", pSetValSysContact)
#define nmhSetSysName(pSetValSysName)	\
	nmhSetCmn(SysName, 8, SysNameSet, NULL, NULL, 0, 0, 0, "%s", pSetValSysName)
#define nmhSetSysLocation(pSetValSysLocation)	\
	nmhSetCmn(SysLocation, 8, SysLocationSet, NULL, NULL, 0, 0, 0, "%s", pSetValSysLocation)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 SnmpEnableAuthenTraps[8];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetSnmpEnableAuthenTraps(i4SetValSnmpEnableAuthenTraps)	\
	nmhSetCmn(SnmpEnableAuthenTraps, 8, SnmpEnableAuthenTrapsSet, NULL, NULL, 0, 0, 0, "%i", i4SetValSnmpEnableAuthenTraps)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 SnmpSetSerialNo[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetSnmpSetSerialNo(i4SetValSnmpSetSerialNo)	\
	nmhSetCmn(SnmpSetSerialNo, 10, SnmpSetSerialNoSet, NULL, NULL, 0, 0, 0, "%i", i4SetValSnmpSetSerialNo)

#endif
