/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdpimcli.h,v 1.3 2016/09/30 10:55:14 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 PimJoinPruneInterval[9];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetPimJoinPruneInterval(i4SetValPimJoinPruneInterval) \
 nmhSetCmn(PimJoinPruneInterval, 9, PimJoinPruneIntervalSet, PimMutexLock, PimMutexUnLock, 0, 0, 0, "%i", i4SetValPimJoinPruneInterval)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 PimInterfaceIfIndex[11];
extern UINT4 PimInterfaceMode[11];
extern UINT4 PimInterfaceHelloInterval[11];
extern UINT4 PimInterfaceStatus[11];
extern UINT4 PimInterfaceJoinPruneInterval[11];
extern UINT4 PimInterfaceCBSRPreference[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetPimInterfaceMode(i4PimInterfaceIfIndex ,i4SetValPimInterfaceMode) \
 nmhSetCmn(PimInterfaceMode, 11, PimInterfaceModeSet, PimMutexLock, PimMutexUnLock, 0, 0, 1, "%i %i", i4PimInterfaceIfIndex ,i4SetValPimInterfaceMode)
#define nmhSetPimInterfaceHelloInterval(i4PimInterfaceIfIndex ,i4SetValPimInterfaceHelloInterval) \
 nmhSetCmn(PimInterfaceHelloInterval, 11, PimInterfaceHelloIntervalSet, PimMutexLock, PimMutexUnLock, 0, 0, 1, "%i %i", i4PimInterfaceIfIndex ,i4SetValPimInterfaceHelloInterval)
#define nmhSetPimInterfaceStatus(i4PimInterfaceIfIndex ,i4SetValPimInterfaceStatus) \
 nmhSetCmn(PimInterfaceStatus, 11, PimInterfaceStatusSet, PimMutexLock, PimMutexUnLock, 0, 1, 1, "%i %i", i4PimInterfaceIfIndex ,i4SetValPimInterfaceStatus)
#define nmhSetPimInterfaceJoinPruneInterval(i4PimInterfaceIfIndex ,i4SetValPimInterfaceJoinPruneInterval) \
 nmhSetCmn(PimInterfaceJoinPruneInterval, 11, PimInterfaceJoinPruneIntervalSet, PimMutexLock, PimMutexUnLock, 0, 0, 1, "%i %i", i4PimInterfaceIfIndex ,i4SetValPimInterfaceJoinPruneInterval)
#define nmhSetPimInterfaceCBSRPreference(i4PimInterfaceIfIndex ,i4SetValPimInterfaceCBSRPreference) \
 nmhSetCmn(PimInterfaceCBSRPreference, 11, PimInterfaceCBSRPreferenceSet, PimMutexLock, PimMutexUnLock, 0, 0, 1, "%i %i", i4PimInterfaceIfIndex ,i4SetValPimInterfaceCBSRPreference)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 PimRPGroupAddress[11];
extern UINT4 PimRPAddress[11];
extern UINT4 PimRPRowStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetPimRPRowStatus(u4PimRPGroupAddress , u4PimRPAddress ,i4SetValPimRPRowStatus) \
 nmhSetCmn(PimRPRowStatus, 11, PimRPRowStatusSet, PimMutexLock, PimMutexUnLock, 1, 1, 2, "%p %p %i", u4PimRPGroupAddress , u4PimRPAddress ,i4SetValPimRPRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 PimCandidateRPGroupAddress[11];
extern UINT4 PimCandidateRPGroupMask[11];
extern UINT4 PimCandidateRPAddress[11];
extern UINT4 PimCandidateRPRowStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetPimCandidateRPAddress(u4PimCandidateRPGroupAddress , u4PimCandidateRPGroupMask ,u4SetValPimCandidateRPAddress) \
 nmhSetCmn(PimCandidateRPAddress, 11, PimCandidateRPAddressSet, PimMutexLock, PimMutexUnLock, 0, 0, 2, "%p %p %p", u4PimCandidateRPGroupAddress , u4PimCandidateRPGroupMask ,u4SetValPimCandidateRPAddress)
#define nmhSetPimCandidateRPRowStatus(u4PimCandidateRPGroupAddress , u4PimCandidateRPGroupMask ,i4SetValPimCandidateRPRowStatus) \
 nmhSetCmn(PimCandidateRPRowStatus, 11, PimCandidateRPRowStatusSet, PimMutexLock, PimMutexUnLock, 0, 1, 2, "%p %p %i", u4PimCandidateRPGroupAddress , u4PimCandidateRPGroupMask ,i4SetValPimCandidateRPRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 PimComponentIndex[11];
extern UINT4 PimComponentCRPHoldTime[11];
extern UINT4 PimComponentStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetPimComponentCRPHoldTime(i4PimComponentIndex ,i4SetValPimComponentCRPHoldTime) \
 nmhSetCmn(PimComponentCRPHoldTime, 11, PimComponentCRPHoldTimeSet, PimMutexLock, PimMutexUnLock, 0, 0, 1, "%i %i", i4PimComponentIndex ,i4SetValPimComponentCRPHoldTime)
#define nmhSetPimComponentStatus(i4PimComponentIndex ,i4SetValPimComponentStatus) \
 nmhSetCmn(PimComponentStatus, 11, PimComponentStatusSet, PimMutexLock, PimMutexUnLock, 0, 1, 1, "%i %i", i4PimComponentIndex ,i4SetValPimComponentStatus)

#endif
