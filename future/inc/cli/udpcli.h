#ifndef __UDPCLI_H__
#define __UDPCLI_H__

#include "lr.h"
#include "cli.h"
/* UDP CLI command constants. To add a new command ,a new definition needs to
 * be added to the list.
 */
enum
{
    UDP_CLI_SHOW_STATS = 1,
    UDP_CLI_SHOW_CONN,
    UDP_CLI_MAX_COMMANDS,
};

/* Maha >>
 * Change this macro name to UDP_CLI_MAX_ARGS
 */
#define UDP_MAX_ARGS 5

INT1 cli_process_udp_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);

#endif /* __UDPCLI_H__ */

