/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: mbsmcli.h,v 1.7 2016/03/09 11:37:35 siva Exp $
 *
 *********************************************************************/
#ifndef __MBSMCLI_H__
#define __MBSMCLI_H__
#include "cli.h"

INT4 cli_process_mbsm_cmd(tCliHandle CliHandle ,UINT4 u4Command , ...);

/* list of MBSM CLI commands */
typedef enum
{
    MBSM_CLI_SHOW_HW = 1,
    MBSM_CLI_SLOT_ENTRY_ADD,
    MBSM_CLI_SLOT_ENTRY_DEL,
    MBSM_CLI_SLOT_MODTYPE_ADD,
    MBSM_CLI_LOAD_SHARING_ENABLE,
    MBSM_CLI_LOAD_SHARING_DISABLE,
    MBSM_CLI_ATTACH_CARD,
    MBSM_CLI_DETTACH_CARD
} tMbsmCliCmds;

/* Prototypes used in CLI */
INT4 MbsmCliPreConfigAdd (tCliHandle ,INT4 , INT1 *);
INT4 MbsmCliPreConfigDel (tCliHandle ,INT4 );
INT4 MbsmCliSlotModTypeAdd (tCliHandle ,INT4 , INT4 );
INT4 MbsmCliShowHardware (tCliHandle );
INT4 MbsmCliLoadSharingEnable (tCliHandle);
INT4 MbsmCliLoadSharingDisable (tCliHandle);
INT4 MbsmShowRunningConfig (tCliHandle );
INT4 MbsmCliUpdateLCModType (tCliHandle CliHandle, INT4 i4SlotId,
                             INT1 *pi1CardName, INT4 i4SlotStatus);
INT4 MbsmCliInsertLC (tCliHandle CliHandle, INT4 i4SlotId, 
                      UINT1 *pu1CardName);
INT4 MbsmCliRemoveLC (tCliHandle CliHandle, INT4 i4SlotId);
INT4 MbsmDetCardTypeandPortNum (UINT1 *pu1CardName,
                            INT4 *pi4ModuleType, UINT4 *pu4NumPorts);
INT4 MbsmCliShowNpSyncStatus (tCliHandle CliHandle);



#endif /* __MBSMCLI_H__ */
