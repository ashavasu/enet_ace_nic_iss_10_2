/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fspbbcli.h,v 1.2 2009/01/19 11:28:05 prabuc-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPbbShutdownStatus[11];
extern UINT4 FsPbbGlbOUI[11];
extern UINT4 FsPbbMaxNoOfISID[11];
extern UINT4 FsPbbMaxNoOfISIDPerContext[11];
extern UINT4 FsPbbMaxPortsPerISID[11];
extern UINT4 FsPbbMaxPortsPerISIDPerContext[11];
extern UINT4 FsPbbTraceInput[11];
#define nmhSetFsPbbShutdownStatus(i4SetValFsPbbShutdownStatus)	\
	nmhSetCmn(FsPbbShutdownStatus, 11, FsPbbShutdownStatusSet, PbbLock, PbbUnLock, 0, 0, 0, "%i", i4SetValFsPbbShutdownStatus)
#define nmhSetFsPbbGlbOUI(pSetValFsPbbGlbOUI)	\
	nmhSetCmn(FsPbbGlbOUI, 11, FsPbbGlbOUISet, PbbLock, PbbUnLock, 0, 0, 0, "%s", pSetValFsPbbGlbOUI)
#define nmhSetFsPbbMaxNoOfISID(i4SetValFsPbbMaxNoOfISID)	\
	nmhSetCmn(FsPbbMaxNoOfISID, 11, FsPbbMaxNoOfISIDSet, PbbLock, PbbUnLock, 0, 0, 0, "%i", i4SetValFsPbbMaxNoOfISID)
#define nmhSetFsPbbMaxNoOfISIDPerContext(i4SetValFsPbbMaxNoOfISIDPerContext)	\
	nmhSetCmn(FsPbbMaxNoOfISIDPerContext, 11, FsPbbMaxNoOfISIDPerContextSet, PbbLock, PbbUnLock, 0, 0, 0, "%i", i4SetValFsPbbMaxNoOfISIDPerContext)
#define nmhSetFsPbbMaxPortsPerISID(i4SetValFsPbbMaxPortsPerISID)	\
	nmhSetCmn(FsPbbMaxPortsPerISID, 11, FsPbbMaxPortsPerISIDSet, PbbLock, PbbUnLock, 0, 0, 0, "%i", i4SetValFsPbbMaxPortsPerISID)
#define nmhSetFsPbbMaxPortsPerISIDPerContext(i4SetValFsPbbMaxPortsPerISIDPerContext)	\
	nmhSetCmn(FsPbbMaxPortsPerISIDPerContext, 11, FsPbbMaxPortsPerISIDPerContextSet, PbbLock, PbbUnLock, 0, 0, 0, "%i", i4SetValFsPbbMaxPortsPerISIDPerContext)
#define nmhSetFsPbbTraceInput(pSetValFsPbbTraceInput)	\
	nmhSetCmn(FsPbbTraceInput, 11, FsPbbTraceInputSet, PbbLock, PbbUnLock, 0, 0, 0, "%s", pSetValFsPbbTraceInput)


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPbbContextId[13];
extern UINT4 FsPbbOUI[13];
extern UINT4 FsPbbOUIRowStatus[13];
#define nmhSetFsPbbOUI(i4FsPbbContextId , u4FsPbbCBPServiceMappingBackboneSid , i4IfIndex ,pSetValFsPbbOUI)	\
	nmhSetCmn(FsPbbOUI, 13, FsPbbOUISet, PbbLock, PbbUnLock, 0, 0, 3, "%i %u %i %s", i4FsPbbContextId , u4FsPbbCBPServiceMappingBackboneSid , i4IfIndex ,pSetValFsPbbOUI)
#define nmhSetFsPbbOUIRowStatus(i4FsPbbContextId , u4FsPbbCBPServiceMappingBackboneSid , i4IfIndex ,i4SetValFsPbbOUIRowStatus)	\
	nmhSetCmn(FsPbbOUIRowStatus, 13, FsPbbOUIRowStatusSet, PbbLock, PbbUnLock, 0, 1, 3, "%i %u %i %i", i4FsPbbContextId , u4FsPbbCBPServiceMappingBackboneSid , i4IfIndex ,i4SetValFsPbbOUIRowStatus)


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPbbPortPisid[13];
extern UINT4 FsPbbPIsidRowStatus[13];
#define nmhSetFsPbbPortPisid(i4FsPbbContextId , i4IfIndex ,i4SetValFsPbbPortPisid)	\
	nmhSetCmn(FsPbbPortPisid, 13, FsPbbPortPisidSet, PbbLock, PbbUnLock, 0, 0, 2, "%i %i %i", i4FsPbbContextId , i4IfIndex ,i4SetValFsPbbPortPisid)
#define nmhSetFsPbbPIsidRowStatus(i4FsPbbContextId , i4IfIndex ,i4SetValFsPbbPIsidRowStatus)	\
	nmhSetCmn(FsPbbPIsidRowStatus, 13, FsPbbPIsidRowStatusSet, PbbLock, PbbUnLock, 0, 1, 2, "%i %i %i", i4FsPbbContextId , i4IfIndex ,i4SetValFsPbbPIsidRowStatus)


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPbbPortPcpSelectionRow[13];
extern UINT4 FsPbbPortUseDei[13];
extern UINT4 FsPbbPortReqDropEncoding[13];
#define nmhSetFsPbbPortPcpSelectionRow(i4FsPbbPipIfIndex ,i4SetValFsPbbPortPcpSelectionRow)	\
	nmhSetCmn(FsPbbPortPcpSelectionRow, 13, FsPbbPortPcpSelectionRowSet, PbbLock, PbbUnLock, 0, 0, 1, "%i %i", i4FsPbbPipIfIndex ,i4SetValFsPbbPortPcpSelectionRow)
#define nmhSetFsPbbPortUseDei(i4FsPbbPipIfIndex ,i4SetValFsPbbPortUseDei)	\
	nmhSetCmn(FsPbbPortUseDei, 13, FsPbbPortUseDeiSet, PbbLock, PbbUnLock, 0, 0, 1, "%i %i", i4FsPbbPipIfIndex ,i4SetValFsPbbPortUseDei)
#define nmhSetFsPbbPortReqDropEncoding(i4FsPbbPipIfIndex ,i4SetValFsPbbPortReqDropEncoding)	\
	nmhSetCmn(FsPbbPortReqDropEncoding, 13, FsPbbPortReqDropEncodingSet, PbbLock, PbbUnLock, 0, 0, 1, "%i %i", i4FsPbbPipIfIndex ,i4SetValFsPbbPortReqDropEncoding)


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPbbPcpDecodingPcpSelRow[13];
extern UINT4 FsPbbPcpDecodingPcpValue[13];
extern UINT4 FsPbbPcpDecodingPriority[13];
extern UINT4 FsPbbPcpDecodingDropEligible[13];
#define nmhSetFsPbbPcpDecodingPriority(i4FsPbbPipIfIndex , i4FsPbbPcpDecodingPcpSelRow , i4FsPbbPcpDecodingPcpValue ,i4SetValFsPbbPcpDecodingPriority)	\
	nmhSetCmn(FsPbbPcpDecodingPriority, 13, FsPbbPcpDecodingPrioritySet, PbbLock, PbbUnLock, 0, 0, 3, "%i %i %i %i", i4FsPbbPipIfIndex , i4FsPbbPcpDecodingPcpSelRow , i4FsPbbPcpDecodingPcpValue ,i4SetValFsPbbPcpDecodingPriority)
#define nmhSetFsPbbPcpDecodingDropEligible(i4FsPbbPipIfIndex , i4FsPbbPcpDecodingPcpSelRow , i4FsPbbPcpDecodingPcpValue ,i4SetValFsPbbPcpDecodingDropEligible)	\
	nmhSetCmn(FsPbbPcpDecodingDropEligible, 13, FsPbbPcpDecodingDropEligibleSet, PbbLock, PbbUnLock, 0, 0, 3, "%i %i %i %i", i4FsPbbPipIfIndex , i4FsPbbPcpDecodingPcpSelRow , i4FsPbbPcpDecodingPcpValue ,i4SetValFsPbbPcpDecodingDropEligible)


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPbbPcpEncodingPcpSelRow[13];
extern UINT4 FsPbbPcpEncodingPriority[13];
extern UINT4 FsPbbPcpEncodingDropEligible[13];
extern UINT4 FsPbbPcpEncodingPcpValue[13];
#define nmhSetFsPbbPcpEncodingPcpValue(i4FsPbbPipIfIndex , i4FsPbbPcpEncodingPcpSelRow , i4FsPbbPcpEncodingPriority , i4FsPbbPcpEncodingDropEligible ,i4SetValFsPbbPcpEncodingPcpValue)	\
	nmhSetCmn(FsPbbPcpEncodingPcpValue, 13, FsPbbPcpEncodingPcpValueSet, PbbLock, PbbUnLock, 0, 0, 4, "%i %i %i %i %i", i4FsPbbPipIfIndex , i4FsPbbPcpEncodingPcpSelRow , i4FsPbbPcpEncodingPriority , i4FsPbbPcpEncodingDropEligible ,i4SetValFsPbbPcpEncodingPcpValue)



/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPbbInstanceId[13];
extern UINT4 FsPbbInstanceMacAddr[13];
extern UINT4 FsPbbInstanceName[13];
extern UINT4 FsPbbInstanceRowStatus[13];

#define nmhSetFsPbbInstanceMacAddr(i4FsPbbInstanceId ,SetValFsPbbInstanceMacAddr)	\
	nmhSetCmn(FsPbbInstanceMacAddr, 13, FsPbbInstanceMacAddrSet, PbbLock, PbbUnLock, 0, 0, 1, "%i %m", i4FsPbbInstanceId ,SetValFsPbbInstanceMacAddr)
#define nmhSetFsPbbInstanceName(i4FsPbbInstanceId ,pSetValFsPbbInstanceName)	\
	nmhSetCmn(FsPbbInstanceName, 13, FsPbbInstanceNameSet, PbbLock, PbbUnLock, 0, 0, 1, "%i %s", i4FsPbbInstanceId ,pSetValFsPbbInstanceName)
#define nmhSetFsPbbInstanceRowStatus(i4FsPbbInstanceId ,i4SetValFsPbbInstanceRowStatus)	\
	nmhSetCmn(FsPbbInstanceRowStatus, 13, FsPbbInstanceRowStatusSet, PbbLock, PbbUnLock, 0, 1, 1, "%i %i", i4FsPbbInstanceId ,i4SetValFsPbbInstanceRowStatus)



/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPbbContextToInstanceId[13];

#define nmhSetFsPbbContextToInstanceId(i4FsPbbContextId ,i4SetValFsPbbContextToInstanceId)	\
	nmhSetCmn(FsPbbContextToInstanceId, 13, FsPbbContextToInstanceIdSet, PbbLock, PbbUnLock, 0, 0, 1, "%i %i", i4FsPbbContextId ,i4SetValFsPbbContextToInstanceId)

