/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmidhrlycli.h,v 1.2 2017/12/14 10:23:43 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIDhcpConfigGblTraceLevel[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMIDhcpConfigGblTraceLevel(i4SetValFsMIDhcpConfigGblTraceLevel) \
 nmhSetCmnWithLock(FsMIDhcpConfigGblTraceLevel, 11, FsMIDhcpConfigGblTraceLevelSet, DHCPR_PROTO_LOCK, DHCPR_PROTO_UNLOCK, 0, 0, 0, "%i", i4SetValFsMIDhcpConfigGblTraceLevel)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIDhcpContextId[13];
extern UINT4 FsMIDhcpRelaying[13];
extern UINT4 FsMIDhcpRelayServersOnly[13];
extern UINT4 FsMIDhcpRelaySecsThreshold[13];
extern UINT4 FsMIDhcpRelayHopsThreshold[13];
extern UINT4 FsMIDhcpRelayRAIOptionControl[13];
extern UINT4 FsMIDhcpRelayRAICircuitIDSubOptionControl[13];
extern UINT4 FsMIDhcpRelayRAIRemoteIDSubOptionControl[13];
extern UINT4 FsMIDhcpRelayRAISubnetMaskSubOptionControl[13];
extern UINT4 FsMIDhcpConfigTraceLevel[13];
extern UINT4 FsMIDhcpConfigDhcpCircuitOption[13];
extern UINT4 FsMIDhcpRelayCounterReset[13];
extern UINT4 FsMIDhcpRelayContextRowStatus[13];
extern UINT4 FsMIDhcpRelayRAIVPNIDSubOptionControl[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMIDhcpRelaying(i4FsMIDhcpContextId ,i4SetValFsMIDhcpRelaying) \
 nmhSetCmnWithLock(FsMIDhcpRelaying, 13, FsMIDhcpRelayingSet, DHCPR_PROTO_LOCK, DHCPR_PROTO_UNLOCK, 0, 0, 1, "%i %i", i4FsMIDhcpContextId ,i4SetValFsMIDhcpRelaying)
#define nmhSetFsMIDhcpRelayServersOnly(i4FsMIDhcpContextId ,i4SetValFsMIDhcpRelayServersOnly) \
 nmhSetCmnWithLock(FsMIDhcpRelayServersOnly, 13, FsMIDhcpRelayServersOnlySet, DHCPR_PROTO_LOCK, DHCPR_PROTO_UNLOCK, 0, 0, 1, "%i %i", i4FsMIDhcpContextId ,i4SetValFsMIDhcpRelayServersOnly)
#define nmhSetFsMIDhcpRelaySecsThreshold(i4FsMIDhcpContextId ,i4SetValFsMIDhcpRelaySecsThreshold) \
 nmhSetCmnWithLock(FsMIDhcpRelaySecsThreshold, 13, FsMIDhcpRelaySecsThresholdSet, DHCPR_PROTO_LOCK, DHCPR_PROTO_UNLOCK, 0, 0, 1, "%i %i", i4FsMIDhcpContextId ,i4SetValFsMIDhcpRelaySecsThreshold)
#define nmhSetFsMIDhcpRelayHopsThreshold(i4FsMIDhcpContextId ,i4SetValFsMIDhcpRelayHopsThreshold) \
 nmhSetCmnWithLock(FsMIDhcpRelayHopsThreshold, 13, FsMIDhcpRelayHopsThresholdSet, DHCPR_PROTO_LOCK, DHCPR_PROTO_UNLOCK, 0, 0, 1, "%i %i", i4FsMIDhcpContextId ,i4SetValFsMIDhcpRelayHopsThreshold)
#define nmhSetFsMIDhcpRelayRAIOptionControl(i4FsMIDhcpContextId ,i4SetValFsMIDhcpRelayRAIOptionControl) \
 nmhSetCmnWithLock(FsMIDhcpRelayRAIOptionControl, 13, FsMIDhcpRelayRAIOptionControlSet, DHCPR_PROTO_LOCK, DHCPR_PROTO_UNLOCK, 0, 0, 1, "%i %i", i4FsMIDhcpContextId ,i4SetValFsMIDhcpRelayRAIOptionControl)
#define nmhSetFsMIDhcpRelayRAICircuitIDSubOptionControl(i4FsMIDhcpContextId ,i4SetValFsMIDhcpRelayRAICircuitIDSubOptionControl) \
 nmhSetCmnWithLock(FsMIDhcpRelayRAICircuitIDSubOptionControl, 13, FsMIDhcpRelayRAICircuitIDSubOptionControlSet, DHCPR_PROTO_LOCK, DHCPR_PROTO_UNLOCK, 0, 0, 1, "%i %i", i4FsMIDhcpContextId ,i4SetValFsMIDhcpRelayRAICircuitIDSubOptionControl)
#define nmhSetFsMIDhcpRelayRAIRemoteIDSubOptionControl(i4FsMIDhcpContextId ,i4SetValFsMIDhcpRelayRAIRemoteIDSubOptionControl) \
 nmhSetCmnWithLock(FsMIDhcpRelayRAIRemoteIDSubOptionControl, 13, FsMIDhcpRelayRAIRemoteIDSubOptionControlSet, DHCPR_PROTO_LOCK, DHCPR_PROTO_UNLOCK, 0, 0, 1, "%i %i", i4FsMIDhcpContextId ,i4SetValFsMIDhcpRelayRAIRemoteIDSubOptionControl)
#define nmhSetFsMIDhcpRelayRAISubnetMaskSubOptionControl(i4FsMIDhcpContextId ,i4SetValFsMIDhcpRelayRAISubnetMaskSubOptionControl) \
 nmhSetCmnWithLock(FsMIDhcpRelayRAISubnetMaskSubOptionControl, 13, FsMIDhcpRelayRAISubnetMaskSubOptionControlSet, DHCPR_PROTO_LOCK, DHCPR_PROTO_UNLOCK, 0, 0, 1, "%i %i", i4FsMIDhcpContextId ,i4SetValFsMIDhcpRelayRAISubnetMaskSubOptionControl)
#define nmhSetFsMIDhcpConfigTraceLevel(i4FsMIDhcpContextId ,i4SetValFsMIDhcpConfigTraceLevel) \
 nmhSetCmnWithLock(FsMIDhcpConfigTraceLevel, 13, FsMIDhcpConfigTraceLevelSet, DHCPR_PROTO_LOCK, DHCPR_PROTO_UNLOCK, 0, 0, 1, "%i %i", i4FsMIDhcpContextId ,i4SetValFsMIDhcpConfigTraceLevel)
#define nmhSetFsMIDhcpConfigDhcpCircuitOption(i4FsMIDhcpContextId ,pSetValFsMIDhcpConfigDhcpCircuitOption) \
 nmhSetCmnWithLock(FsMIDhcpConfigDhcpCircuitOption, 13, FsMIDhcpConfigDhcpCircuitOptionSet, DHCPR_PROTO_LOCK, DHCPR_PROTO_UNLOCK, 0, 0, 1, "%i %s", i4FsMIDhcpContextId ,pSetValFsMIDhcpConfigDhcpCircuitOption)
#define nmhSetFsMIDhcpRelayCounterReset(i4FsMIDhcpContextId ,i4SetValFsMIDhcpRelayCounterReset) \
 nmhSetCmnWithLock(FsMIDhcpRelayCounterReset, 13, FsMIDhcpRelayCounterResetSet, DHCPR_PROTO_LOCK, DHCPR_PROTO_UNLOCK, 0, 0, 1, "%i %i", i4FsMIDhcpContextId ,i4SetValFsMIDhcpRelayCounterReset)
#define nmhSetFsMIDhcpRelayContextRowStatus(i4FsMIDhcpContextId ,i4SetValFsMIDhcpRelayContextRowStatus) \
 nmhSetCmnWithLock(FsMIDhcpRelayContextRowStatus, 13, FsMIDhcpRelayContextRowStatusSet, DHCPR_PROTO_LOCK, DHCPR_PROTO_UNLOCK, 0, 1, 1, "%i %i", i4FsMIDhcpContextId ,i4SetValFsMIDhcpRelayContextRowStatus)
#define nmhSetFsMIDhcpRelayRAIVPNIDSubOptionControl(i4FsMIDhcpContextId ,i4SetValFsMIDhcpRelayRAIVPNIDSubOptionControl) \
 nmhSetCmnWithLock(FsMIDhcpRelayRAIVPNIDSubOptionControl, 13, FsMIDhcpRelayRAIVPNIDSubOptionControlSet, DHCPR_PROTO_LOCK, DHCPR_PROTO_UNLOCK, 0, 0, 1, "%i %i", i4FsMIDhcpContextId ,i4SetValFsMIDhcpRelayRAIVPNIDSubOptionControl)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIDhcpRelaySrvIpAddress[13];
extern UINT4 FsMIDhcpRelaySrvAddressRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMIDhcpRelaySrvAddressRowStatus(i4FsMIDhcpContextId , u4FsMIDhcpRelaySrvIpAddress ,i4SetValFsMIDhcpRelaySrvAddressRowStatus) \
 nmhSetCmnWithLock(FsMIDhcpRelaySrvAddressRowStatus, 13, FsMIDhcpRelaySrvAddressRowStatusSet, DHCPR_PROTO_LOCK, DHCPR_PROTO_UNLOCK, 0, 1, 2, "%i %p %i", i4FsMIDhcpContextId , u4FsMIDhcpRelaySrvIpAddress ,i4SetValFsMIDhcpRelaySrvAddressRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIDhcpRelayIfCircuitId[13];
extern UINT4 FsMIDhcpRelayIfRemoteId[13];
extern UINT4 FsMIDhcpRelayIfRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMIDhcpRelayIfCircuitId(i4FsMIDhcpContextId , i4IfIndex ,u4SetValFsMIDhcpRelayIfCircuitId) \
 nmhSetCmnWithLock(FsMIDhcpRelayIfCircuitId, 13, FsMIDhcpRelayIfCircuitIdSet, DHCPR_PROTO_LOCK, DHCPR_PROTO_UNLOCK, 0, 0, 2, "%i %i %u", i4FsMIDhcpContextId , i4IfIndex ,u4SetValFsMIDhcpRelayIfCircuitId)
#define nmhSetFsMIDhcpRelayIfRemoteId(i4FsMIDhcpContextId , i4IfIndex ,pSetValFsMIDhcpRelayIfRemoteId) \
 nmhSetCmnWithLock(FsMIDhcpRelayIfRemoteId, 13, FsMIDhcpRelayIfRemoteIdSet, DHCPR_PROTO_LOCK, DHCPR_PROTO_UNLOCK, 0, 0, 2, "%i %i %s", i4FsMIDhcpContextId , i4IfIndex ,pSetValFsMIDhcpRelayIfRemoteId)
#define nmhSetFsMIDhcpRelayIfRowStatus(i4FsMIDhcpContextId , i4IfIndex ,i4SetValFsMIDhcpRelayIfRowStatus) \
 nmhSetCmnWithLock(FsMIDhcpRelayIfRowStatus, 13, FsMIDhcpRelayIfRowStatusSet, DHCPR_PROTO_LOCK, DHCPR_PROTO_UNLOCK, 0, 1, 2, "%i %i %i", i4FsMIDhcpContextId , i4IfIndex ,i4SetValFsMIDhcpRelayIfRowStatus)

#endif
