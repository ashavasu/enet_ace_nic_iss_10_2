
/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: l2dscli.h,v 1.4 2013/07/01 12:41:02 siva Exp $
 *
 * Description: This file contains the macros and proto types
 *              for DHCP Snooping CLI module
 *********************************************************************/

#ifndef _L2DSCLI_H
#define _L2DSCLI_H


#include "lr.h"
#include "cli.h"

enum
{
    L2DS_STATUS_ENABLE = 1,      /* 1 */
    L2DS_STATUS_DISABLE,         /* 2 */   
    L2DS_MAC_STATUS_ENABLE,      /* 3 */
    L2DS_MAC_STATUS_DISABLE,     /* 4 */
    L2DS_IFACE_STATUS_ENABLE,    /* 5 */
    L2DS_IFACE_STATUS_DISABLE,   /* 6 */
    L2DS_GLOBAL_SHOW,            /* 7 */
    L2DS_VLAN_SHOW,              /* 8 */
    L2DS_TRACE_ENABLE,           /* 9 */
    L2DS_TRACE_DISABLE,          /* 10 */
    L2DS_PORT_STATE_TRUSTED,     /* 11 */
    L2DS_PORT_STATE_UNTRUSTED    /* 12 */
};

INT4
cli_process_l2ds_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);
VOID IssDhcpSnoopingShowDebugging (tCliHandle CliHandle);
#endif
