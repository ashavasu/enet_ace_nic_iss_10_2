/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdldpcli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 MplsLdpEntityLdpId[14];
extern UINT4 MplsLdpEntityIndex[14];
extern UINT4 MplsLdpEntityProtocolVersion[14];
extern UINT4 MplsLdpEntityAdminStatus[14];
extern UINT4 MplsLdpEntityTcpPort[14];
extern UINT4 MplsLdpEntityUdpDscPort[14];
extern UINT4 MplsLdpEntityMaxPduLength[14];
extern UINT4 MplsLdpEntityKeepAliveHoldTimer[14];
extern UINT4 MplsLdpEntityHelloHoldTimer[14];
extern UINT4 MplsLdpEntityInitSessionThreshold[14];
extern UINT4 MplsLdpEntityLabelDistMethod[14];
extern UINT4 MplsLdpEntityLabelRetentionMode[14];
extern UINT4 MplsLdpEntityPathVectorLimit[14];
extern UINT4 MplsLdpEntityHopCountLimit[14];
extern UINT4 MplsLdpEntityTransportAddrKind[14];
extern UINT4 MplsLdpEntityTargetPeer[14];
extern UINT4 MplsLdpEntityTargetPeerAddrType[14];
extern UINT4 MplsLdpEntityTargetPeerAddr[14];
extern UINT4 MplsLdpEntityLabelType[14];
extern UINT4 MplsLdpEntityStorageType[14];
extern UINT4 MplsLdpEntityRowStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetMplsLdpEntityProtocolVersion(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,u4SetValMplsLdpEntityProtocolVersion)	\
	nmhSetCmn(MplsLdpEntityProtocolVersion, 14, MplsLdpEntityProtocolVersionSet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %u", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,u4SetValMplsLdpEntityProtocolVersion)
#define nmhSetMplsLdpEntityAdminStatus(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityAdminStatus)	\
	nmhSetCmn(MplsLdpEntityAdminStatus, 14, MplsLdpEntityAdminStatusSet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %i", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityAdminStatus)
#define nmhSetMplsLdpEntityTcpPort(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,u4SetValMplsLdpEntityTcpPort)	\
	nmhSetCmn(MplsLdpEntityTcpPort, 14, MplsLdpEntityTcpPortSet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %u", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,u4SetValMplsLdpEntityTcpPort)
#define nmhSetMplsLdpEntityUdpDscPort(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,u4SetValMplsLdpEntityUdpDscPort)	\
	nmhSetCmn(MplsLdpEntityUdpDscPort, 14, MplsLdpEntityUdpDscPortSet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %u", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,u4SetValMplsLdpEntityUdpDscPort)
#define nmhSetMplsLdpEntityMaxPduLength(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,u4SetValMplsLdpEntityMaxPduLength)	\
	nmhSetCmn(MplsLdpEntityMaxPduLength, 14, MplsLdpEntityMaxPduLengthSet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %u", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,u4SetValMplsLdpEntityMaxPduLength)
#define nmhSetMplsLdpEntityKeepAliveHoldTimer(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,u4SetValMplsLdpEntityKeepAliveHoldTimer)	\
	nmhSetCmn(MplsLdpEntityKeepAliveHoldTimer, 14, MplsLdpEntityKeepAliveHoldTimerSet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %u", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,u4SetValMplsLdpEntityKeepAliveHoldTimer)
#define nmhSetMplsLdpEntityHelloHoldTimer(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,u4SetValMplsLdpEntityHelloHoldTimer)	\
	nmhSetCmn(MplsLdpEntityHelloHoldTimer, 14, MplsLdpEntityHelloHoldTimerSet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %u", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,u4SetValMplsLdpEntityHelloHoldTimer)
#define nmhSetMplsLdpEntityInitSessionThreshold(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityInitSessionThreshold)	\
	nmhSetCmn(MplsLdpEntityInitSessionThreshold, 14, MplsLdpEntityInitSessionThresholdSet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %i", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityInitSessionThreshold)
#define nmhSetMplsLdpEntityLabelDistMethod(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityLabelDistMethod)	\
	nmhSetCmn(MplsLdpEntityLabelDistMethod, 14, MplsLdpEntityLabelDistMethodSet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %i", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityLabelDistMethod)
#define nmhSetMplsLdpEntityLabelRetentionMode(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityLabelRetentionMode)	\
	nmhSetCmn(MplsLdpEntityLabelRetentionMode, 14, MplsLdpEntityLabelRetentionModeSet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %i", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityLabelRetentionMode)
#define nmhSetMplsLdpEntityPathVectorLimit(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityPathVectorLimit)	\
	nmhSetCmn(MplsLdpEntityPathVectorLimit, 14, MplsLdpEntityPathVectorLimitSet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %i", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityPathVectorLimit)
#define nmhSetMplsLdpEntityHopCountLimit(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityHopCountLimit)	\
	nmhSetCmn(MplsLdpEntityHopCountLimit, 14, MplsLdpEntityHopCountLimitSet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %i", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityHopCountLimit)
#define nmhSetMplsLdpEntityTransportAddrKind(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityTransportAddrKind)	\
	nmhSetCmn(MplsLdpEntityTransportAddrKind, 14, MplsLdpEntityTransportAddrKindSet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %i", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityTransportAddrKind)
#define nmhSetMplsLdpEntityTargetPeer(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityTargetPeer)	\
	nmhSetCmn(MplsLdpEntityTargetPeer, 14, MplsLdpEntityTargetPeerSet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %i", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityTargetPeer)
#define nmhSetMplsLdpEntityTargetPeerAddrType(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityTargetPeerAddrType)	\
	nmhSetCmn(MplsLdpEntityTargetPeerAddrType, 14, MplsLdpEntityTargetPeerAddrTypeSet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %i", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityTargetPeerAddrType)
#define nmhSetMplsLdpEntityTargetPeerAddr(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,pSetValMplsLdpEntityTargetPeerAddr)	\
	nmhSetCmn(MplsLdpEntityTargetPeerAddr, 14, MplsLdpEntityTargetPeerAddrSet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %s", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,pSetValMplsLdpEntityTargetPeerAddr)
#define nmhSetMplsLdpEntityLabelType(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityLabelType)	\
	nmhSetCmn(MplsLdpEntityLabelType, 14, MplsLdpEntityLabelTypeSet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %i", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityLabelType)
#define nmhSetMplsLdpEntityStorageType(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityStorageType)	\
	nmhSetCmn(MplsLdpEntityStorageType, 14, MplsLdpEntityStorageTypeSet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %i", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityStorageType)
#define nmhSetMplsLdpEntityRowStatus(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityRowStatus)	\
	nmhSetCmn(MplsLdpEntityRowStatus, 14, MplsLdpEntityRowStatusSet, LdpLock, LdpUnLock, 0, 1, 2, "%s %u %i", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 MplsFecIndex[15];
extern UINT4 MplsFecType[15];
extern UINT4 MplsFecAddrType[15];
extern UINT4 MplsFecAddr[15];
extern UINT4 MplsFecAddrPrefixLength[15];
extern UINT4 MplsFecStorageType[15];
extern UINT4 MplsFecRowStatus[15];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetMplsFecType(u4MplsFecIndex ,i4SetValMplsFecType)	\
	nmhSetCmn(MplsFecType, 15, MplsFecTypeSet, LdpLock, LdpUnLock, 0, 0, 1, "%u %i", u4MplsFecIndex ,i4SetValMplsFecType)
#define nmhSetMplsFecAddrType(u4MplsFecIndex ,i4SetValMplsFecAddrType)	\
	nmhSetCmn(MplsFecAddrType, 15, MplsFecAddrTypeSet, LdpLock, LdpUnLock, 0, 0, 1, "%u %i", u4MplsFecIndex ,i4SetValMplsFecAddrType)
#define nmhSetMplsFecAddr(u4MplsFecIndex ,pSetValMplsFecAddr)	\
	nmhSetCmn(MplsFecAddr, 15, MplsFecAddrSet, LdpLock, LdpUnLock, 0, 0, 1, "%u %s", u4MplsFecIndex ,pSetValMplsFecAddr)
#define nmhSetMplsFecAddrPrefixLength(u4MplsFecIndex ,u4SetValMplsFecAddrPrefixLength)	\
	nmhSetCmn(MplsFecAddrPrefixLength, 15, MplsFecAddrPrefixLengthSet, LdpLock, LdpUnLock, 0, 0, 1, "%u %u", u4MplsFecIndex ,u4SetValMplsFecAddrPrefixLength)
#define nmhSetMplsFecStorageType(u4MplsFecIndex ,i4SetValMplsFecStorageType)	\
	nmhSetCmn(MplsFecStorageType, 15, MplsFecStorageTypeSet, LdpLock, LdpUnLock, 0, 0, 1, "%u %i", u4MplsFecIndex ,i4SetValMplsFecStorageType)
#define nmhSetMplsFecRowStatus(u4MplsFecIndex ,i4SetValMplsFecRowStatus)	\
	nmhSetCmn(MplsFecRowStatus, 15, MplsFecRowStatusSet, LdpLock, LdpUnLock, 0, 1, 1, "%u %i", u4MplsFecIndex ,i4SetValMplsFecRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 MplsLdpLspFecSegment[14];
extern UINT4 MplsLdpLspFecSegmentIndex[14];
extern UINT4 MplsLdpLspFecIndex[14];
extern UINT4 MplsLdpLspFecStorageType[14];
extern UINT4 MplsLdpLspFecRowStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetMplsLdpLspFecStorageType(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex , pMplsLdpPeerLdpId , i4MplsLdpLspFecSegment , pMplsLdpLspFecSegmentIndex , u4MplsLdpLspFecIndex ,i4SetValMplsLdpLspFecStorageType)	\
	nmhSetCmn(MplsLdpLspFecStorageType, 14, MplsLdpLspFecStorageTypeSet, LdpLock, LdpUnLock, 0, 0, 6, "%s %u %s %i %s %u %i", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex , pMplsLdpPeerLdpId , i4MplsLdpLspFecSegment , pMplsLdpLspFecSegmentIndex , u4MplsLdpLspFecIndex ,i4SetValMplsLdpLspFecStorageType)
#define nmhSetMplsLdpLspFecRowStatus(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex , pMplsLdpPeerLdpId , i4MplsLdpLspFecSegment , pMplsLdpLspFecSegmentIndex , u4MplsLdpLspFecIndex ,i4SetValMplsLdpLspFecRowStatus)	\
	nmhSetCmn(MplsLdpLspFecRowStatus, 14, MplsLdpLspFecRowStatusSet, LdpLock, LdpUnLock, 0, 1, 6, "%s %u %s %i %s %u %i", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex , pMplsLdpPeerLdpId , i4MplsLdpLspFecSegment , pMplsLdpLspFecSegmentIndex , u4MplsLdpLspFecIndex ,i4SetValMplsLdpLspFecRowStatus)

#endif
