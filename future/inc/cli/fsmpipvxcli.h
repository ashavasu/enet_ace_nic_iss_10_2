/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpipvxcli.h,v 1.2 2009/08/24 13:12:47 prabuc Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIIpvxAddrPrefixIfIndex[12];
extern UINT4 FsMIIpvxAddrPrefixAddrType[12];
extern UINT4 FsMIIpvxAddrPrefix[12];
extern UINT4 FsMIIpvxAddrPrefixLen[12];
extern UINT4 FsMIIpvxAddrPrefixProfileIndex[12];
extern UINT4 FsMIIpvxAddrPrefixSecAddrFlag[12];
extern UINT4 FsMIIpvxAddrPrefixRowStatus[12];


