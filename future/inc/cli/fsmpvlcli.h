/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpvlcli.h,v 1.11 2015/12/29 12:00:15 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIDot1qFutureVlanGlobalTrace[10];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIDot1qFutureVlanContextId[12];
extern UINT4 FsMIDot1qFutureVlanStatus[12];
extern UINT4 FsMIDot1qFutureVlanMacBasedOnAllPorts[12];
extern UINT4 FsMIDot1qFutureVlanPortProtoBasedOnAllPorts[12];
extern UINT4 FsMIDot1qFutureVlanShutdownStatus[12];
extern UINT4 FsMIDot1qFutureGarpShutdownStatus[12];
extern UINT4 FsMIDot1qFutureVlanDebug[12];
extern UINT4 FsMIDot1qFutureVlanLearningMode[12];
extern UINT4 FsMIDot1qFutureVlanHybridTypeDefault[12];
extern UINT4 FsMIDot1qFutureGarpDebug[12];
extern UINT4 FsMIDot1qFutureUnicastMacLearningLimit[12];
extern UINT4 FsMIDot1qFutureBaseBridgeMode[12];
extern UINT4 FsMIDot1qFutureVlanSubnetBasedOnAllPorts[12];
extern UINT4 FsMIDot1qFutureVlanGlobalMacLearningStatus[12];
extern UINT4 FsMIDot1qFutureVlanApplyEnhancedFilteringCriteria[12];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIDot1qFutureVlanPort[12];
extern UINT4 FsMIDot1qFutureVlanPortType[12];
extern UINT4 FsMIDot1qFutureVlanPortMacBasedClassification[12];
extern UINT4 FsMIDot1qFutureVlanPortPortProtoBasedClassification[12];
extern UINT4 FsMIDot1qFutureVlanFilteringUtilityCriteria[12];
extern UINT4 FsMIDot1qFutureVlanPortProtected[12];
extern UINT4 FsMIDot1qFutureVlanPortSubnetBasedClassification[12];
extern UINT4 FsMIDot1qFutureVlanPortUnicastMacLearning[12];
extern UINT4 FsMIDot1qFutureVlanPortIngressEtherType [12];
extern UINT4 FsMIDot1qFutureVlanPortEgressEtherType [12];
extern UINT4 FsMIDot1qFutureVlanPortEgressTPIDType [12];
extern UINT4 FsMIDot1qFutureVlanPortAllowableTPID1 [12];
extern UINT4 Dot1qFutureVlanPortUnicastMacSecType[12];
extern UINT4 Dot1qFuturePortPacketReflectionStatus[12];
extern UINT4 FsMIDot1qFutureVlanPortAllowableTPID2 [12];
extern UINT4 FsMIDot1qFutureVlanPortAllowableTPID3 [12];

/* extern declaration corresponding to OID variable present in protocol db.h */
/* Manually commented the FsMIDot1qFutureVlanPortMacMapAddr as it is used 
 * in the vlmiset.c as argument */
/* extern UINT4 FsMIDot1qFutureVlanPortMacMapAddr[12]; */
extern UINT4 FsMIDot1qFutureVlanPortMacMapVid[12];
extern UINT4 FsMIDot1qFutureVlanPortMacMapName[12];
extern UINT4 FsMIDot1qFutureVlanPortMacMapMcastBcastOption[12];
extern UINT4 FsMIDot1qFutureVlanPortMacMapRowStatus[12];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIDot1qFutureVlanFid[12];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIDot1qFutureVlanBridgeMode[12];
extern UINT4 FsMIDot1qFutureVlanTunnelBpduPri[12];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIDot1qFutureVlanTunnelStatus[12];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIDot1qFutureVlanTunnelStpPDUs[12];
extern UINT4 FsMIDot1qFutureVlanTunnelGvrpPDUs[12];
extern UINT4 FsMIDot1qFutureVlanTunnelIgmpPkts[12];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIDot1qFutureVlanCounterStatus[12];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIDot1qFutureVlanUnicastMacLimit[12];
extern UINT4 FsMIDot1qFutureVlanAdminMacLearningStatus[12];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIDot1qFutureGarpGlobalTrace[10];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIDot1qFutureVlanWildCardMacAddress[12];
extern UINT4 FsMIDot1qFutureVlanWildCardRowStatus[12];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIDot1qFutureVlanIsWildCardEgressPort[12];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIDot1qFutureStaticConnectionIdentifier[12];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIDot1qFutureVlanPortSubnetMapAddr[12];
extern UINT4 FsMIDot1qFutureVlanPortSubnetMapVid[12];
extern UINT4 FsMIDot1qFutureVlanPortSubnetMapARPOption[12];
extern UINT4 FsMIDot1qFutureVlanPortSubnetMapRowStatus[12];
extern UINT4 FsMIDot1qFutureVlanSwStatsEnabled[10];
extern UINT4 FsMIDot1qFutureVlanPortSubnetMapExtAddr[12];
extern UINT4 FsMIDot1qFutureVlanPortSubnetMapExtMask[12];
extern UINT4 FsMIDot1qFutureVlanPortSubnetMapExtVid[12];
extern UINT4 FsMIDot1qFutureVlanPortSubnetMapExtARPOption[12];
extern UINT4 FsMIDot1qFutureVlanPortSubnetMapExtRowStatus[12];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIDot1qFuturePortPacketReflectionStatus[12];
