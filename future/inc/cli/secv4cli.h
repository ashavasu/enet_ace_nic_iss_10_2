
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secv4cli.h,v 1.4 2011/02/16 14:01:49 siva Exp $
 *
 * Description:This file contains the definitions and macros used by
 *             cli and ipsec modules     
 *
 *******************************************************************/


#ifndef __SECv4CLI_H__
#define __SECv4CLI_H__

#include "cli.h"

#define IPSEC_ENABLE               "1"
#define IPSEC_DISABLE              "2"

#define CLI_SEC_TCP                   6
#define CLI_SEC_UDP                   17

/* Opcodes for CLI commands.
 * When a new command is added, add a new definition */

#define CLI_CREATE_IPSECv4_POLICY        1 /*To create an entry in policy-list*/
#define CLI_CREATE_IPSECv4_SA            2 /*To cretae an entry in sa-list */
#define CLI_SET_IPSECv4_AUTH_SA_PARAMS   3 /*To set Authentication SA params*/ 
#define CLI_SET_IPSECv4_ESP_SA_PARAMS    4 /*To set Encryption SA params*/ 
#define CLI_CREATE_IPSECv4_ACCESS_LIST   5 /*To create an entry in access-list */
#define CLI_CREATE_IPSECv4_SELECTOR      6 /*To create an entry in selector*/  
#define CLI_SHOW_IPSECv4_IF_STAT         7 /*To show interface specific stat */ 
#define CLI_SHOW_IPSECv4_AH_ESP_STAT     8 /*To show ah/esp specific stat */
#define CLI_SHOW_IPSECv4_INTRUDER_STAT   9 /*To show intruder specific stat*/
#define CLI_SET_IPSECv4_ADMIN            10 /*To enable IPsec */
#define CLI_SET_IPSECv4_TRACE_LEVEL      11 /*To set the trace level */
#define CLI_SHOW_IPSECv4_POLICY          12 /*To show entries in policy list */
#define CLI_SHOW_IPSECv4_SELECTOR        13 /*To show entries in selector list*/
#define CLI_SHOW_IPSECv4_SA              14 /*To show entries in SA list */ 
#define CLI_SHOW_IPSECv4_ACCESS          15 /*To show entries in access list */
#define CLI_SHOW_IPSECv4_CONFIG          16 /*To show global variables ofipsec*/
#define CLI_DELETE_IPSECv4_POLICY        17 /*To delete entries in policy list*/
#define CLI_DELETE_IPSECv4_SELECTOR      18 /*To delete entry in selector list*/
#define CLI_DELETE_IPSECv4_SA            19 /*To delete entries in SA list */ 
#define CLI_DELETE_IPSECv4_ACCESS        20 /*To delete entries in access list*/
#define CLI_SET_IPSECv4_MAX_SA           21 /*To set max no of SA */


#define CLI_MAX_IPSECv4_COMMANDS       21  /* Max commands */

INT1 Secv4GetSecv4ConfigPrompt(INT1 *, INT1 *);
INT4 cli_process_ipsecv4_cmd(tCliHandle Clihandle, UINT4 u4Command, ...);

#endif  /* __SECv4CLI_H__ */
