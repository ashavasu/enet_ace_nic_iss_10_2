/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsdnscli.h,v 1.3 2014/10/13 12:08:12 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDnsSystemControl[11];
extern UINT4 FsDnsModuleStatus[11];
extern UINT4 FsDnsTraceOption[11];
extern UINT4 FsDnsQueryRetryCount[11];
extern UINT4 FsDnsQueryTimeOut[11];
extern UINT4 FsDnsResolverMode[11];
extern UINT4 FsDnsPreferentialType[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDnsSystemControl(i4SetValFsDnsSystemControl) \
 nmhSetCmn(FsDnsSystemControl, 11, FsDnsSystemControlSet, DnsLock, DnsUnLock, 0, 0, 0, "%i", i4SetValFsDnsSystemControl)
#define nmhSetFsDnsModuleStatus(i4SetValFsDnsModuleStatus) \
 nmhSetCmn(FsDnsModuleStatus, 11, FsDnsModuleStatusSet, DnsLock, DnsUnLock, 0, 0, 0, "%i", i4SetValFsDnsModuleStatus)
#define nmhSetFsDnsTraceOption(i4SetValFsDnsTraceOption) \
 nmhSetCmn(FsDnsTraceOption, 11, FsDnsTraceOptionSet, DnsLock, DnsUnLock, 0, 0, 0, "%i", i4SetValFsDnsTraceOption)
#define nmhSetFsDnsQueryRetryCount(u4SetValFsDnsQueryRetryCount) \
 nmhSetCmn(FsDnsQueryRetryCount, 11, FsDnsQueryRetryCountSet, DnsLock, DnsUnLock, 0, 0, 0, "%u", u4SetValFsDnsQueryRetryCount)
#define nmhSetFsDnsQueryTimeOut(u4SetValFsDnsQueryTimeOut) \
 nmhSetCmn(FsDnsQueryTimeOut, 11, FsDnsQueryTimeOutSet, DnsLock, DnsUnLock, 0, 0, 0, "%u", u4SetValFsDnsQueryTimeOut)
#define nmhSetFsDnsResolverMode(i4SetValFsDnsResolverMode) \
 nmhSetCmn(FsDnsResolverMode, 11, FsDnsResolverModeSet, DnsLock, DnsUnLock, 0, 0, 0, "%i", i4SetValFsDnsResolverMode)
#define nmhSetFsDnsPreferentialType(i4SetValFsDnsPreferentialType) \
 nmhSetCmn(FsDnsPreferentialType, 11, FsDnsPreferentialTypeSet, DnsLock, DnsUnLock, 0, 0, 0, "%i", i4SetValFsDnsPreferentialType)
#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDnsNameServerIndex[13];
extern UINT4 FsDnsServerIPAddressType[13];
extern UINT4 FsDnsServerIPAddress[13];
extern UINT4 FsDnsNameServerRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDnsServerIPAddressType(u4FsDnsNameServerIndex ,i4SetValFsDnsServerIPAddressType) \
 nmhSetCmn(FsDnsServerIPAddressType, 13, FsDnsServerIPAddressTypeSet, DnsLock, DnsUnLock, 0, 0, 1, "%u %i", u4FsDnsNameServerIndex ,i4SetValFsDnsServerIPAddressType)
#define nmhSetFsDnsServerIPAddress(u4FsDnsNameServerIndex ,pSetValFsDnsServerIPAddress) \
 nmhSetCmn(FsDnsServerIPAddress, 13, FsDnsServerIPAddressSet, DnsLock, DnsUnLock, 0, 0, 1, "%u %s", u4FsDnsNameServerIndex ,pSetValFsDnsServerIPAddress)
#define nmhSetFsDnsNameServerRowStatus(u4FsDnsNameServerIndex ,i4SetValFsDnsNameServerRowStatus) \
 nmhSetCmn(FsDnsNameServerRowStatus, 13, FsDnsNameServerRowStatusSet, DnsLock, DnsUnLock, 0, 1, 1, "%u %i", u4FsDnsNameServerIndex ,i4SetValFsDnsNameServerRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDnsDomainNameIndex[13];
extern UINT4 FsDnsDomainName[13];
extern UINT4 FsDnsDomainNameRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDnsDomainName(u4FsDnsDomainNameIndex ,pSetValFsDnsDomainName) \
 nmhSetCmn(FsDnsDomainName, 13, FsDnsDomainNameSet, DnsLock, DnsUnLock, 0, 0, 1, "%u %s", u4FsDnsDomainNameIndex ,pSetValFsDnsDomainName)
#define nmhSetFsDnsDomainNameRowStatus(u4FsDnsDomainNameIndex ,i4SetValFsDnsDomainNameRowStatus) \
 nmhSetCmn(FsDnsDomainNameRowStatus, 13, FsDnsDomainNameRowStatusSet, DnsLock, DnsUnLock, 0, 1, 1, "%u %i", u4FsDnsDomainNameIndex ,i4SetValFsDnsDomainNameRowStatus)

#endif
