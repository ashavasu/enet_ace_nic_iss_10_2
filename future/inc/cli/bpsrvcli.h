
#ifndef __BPSRVCLI_H__
#define __BPSRVCLI_H__

#define BPSRV_ENABLED       1
#define BPSRV_DISABLED      2
#define BPSRV_MAX_AUTH_KEY_SIZE 16
#define BPSRV_CLI_MAX_ARGS  4


/* Proto types */
INT4 cli_process_bpsrv_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);
INT4 BeepSrvShowRunningConfig (tCliHandle CliHandle);


/* Enum declaration for BEEP Server command identifiers */



enum
{
    CLI_BPSRV_SHOW_CONF =1,
    CLI_BPSRV_SERVER,
    CLI_BPSRV_NO_SERVER,
    CLI_BPSRV_ENCRYPT,
    CLI_BPSRV_NO_ENCRYPT,
    CLI_BPSRV_AUTHENTICATION,
    CLI_BPSRV_NO_AUTHENTICATION,
    CLI_BPSRV_PORT,
    CLI_BPSRV_6PORT,
    CLI_BPSRV_AUTH_KEY,
    CLI_BPSRV_MAX_CMDS
};


/* Error Codes */

enum
{
    BEEP_SERVER_ALREADY_ENABLED_ERR = 1,
    BEEP_SERVER_ALREADY_DISABLED_ERR,
    BPSRV_UNABLE_TO_SET_ERR,
    BPSRV_INVALID_CMD_ERR,
    BPSRV_SHOW_FAIL_ERR,
    BPSRV_INVALID_PORT_ERR,
    CLI_BPSRV_MAX_ERR
};

#ifdef __BPSRV_CLI_C__
/* Error Strings */

CONST CHR1 * gaBeepSrvCliErrString [] = {
     NULL,
    "Beep Server is Already Running!\r\n",
    "Beep Server is Already Disabled!\r\n",
    "Beep Server: Unable to set value!\r\n",
    "Beep Server: Invalid Command\r\n",
    "Beep Server: Cannot execute show command\r\n",
    "Beep Server: Invalid Port Number Specified\r\n",
};


#endif
#endif
