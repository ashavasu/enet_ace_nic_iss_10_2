/* $Id: pimcli.h,v 1.31 2016/06/24 09:42:21 siva Exp $   */

#ifndef PIMCLI_H
#define PIMCLI_H
#include "lr.h"
#include "cli.h"

/* FuturePIM CLI command constants. To add a new command ,a new 
 * definition needs to be added to the list.
 */
enum {
     PIM_CLI_STATUS = 1,
     PIM_CLI_TRACE,
     PIM_CLI_DEBUG,
     PIM_CLI_ENABLE_RPCANDIDATE_ADDRESS,
     PIM_CLI_DISABLE_RPCANDIDATE_ADDRESS,
     PIM_CLI_STATIC_RP_ADDRESS,
     PIM_CLI_NO_STATIC_RP_ADDRESS,
     PIMV6_CLI_EMBEDDED_RP_ADDRESS,
     PIMV6_CLI_NO_EMBEDDED_RP_ADDRESS,
     PIM_CLI_HELLOINTERVAL,
     PIM_CLI_PIMJOINPRUNEINTERVAL,
     PIM_CLI_BSRCANDIDATE,
     PIM_CLI_BSRBORDER,
     PIM_CLI_PIMINTERFACE_COMP_ID,
     PIM_CLI_INT_HELLOHOLDTIME,
     PIM_CLI_INT_DRPRIORITY,
     PIM_CLI_INT_LAN_DELAY,
     PIM_CLI_INT_OVERRIDE_INTERVAL,
     PIM_CLI_LAN_PRUNE_DELAY_PRESENT,
     PIM_CLI_INT_GRAFT_RETRY_INTERVAL,
     PIM_CLI_INT_DEF_GRAFT_RETRY_INTERVAL,
     PIM_CLI_DEFAULT_HELLOINTERVAL,
     PIM_CLI_NO_INTERFACE, 
     PIM_CLI_SHOW_PIM_INTERFACE,
     PIM_CLI_SHOW_PIM_NBR,
     PIM_CLI_SHOW_PIM_RPCANDIDATE,
     PIM_CLI_SHOW_PIM_RPSET,
     PIM_CLI_SHOW_PIM_BSR,
     PIM_CLI_SHOW_PIM_RPSTATIC,
     PIM_CLI_SHOW_COMPONENT,
     PIM_CLI_SHOW_THRESHOLDS,
     PIM_CLI_SHOW_RPF,
     PIM_CLI_SHOW_PIM_MULTICASTROUTE,
     PIM_CLI_SPT_GRPTHRESHOLD,
     PIM_CLI_SPT_SRCTHRESHOLD,
     PIM_CLI_SPT_SWITCHPERIOD,
     PIM_CLI_RP_THRESHOLD,
     PIM_CLI_RP_SWITCHPERIOD,
     PIM_CLI_REGSTOP_RATELIMIT_PERIOD,
     PIM_CLI_PMBR_STATUS,
     PIM_CLI_CREATE_COMPONENT,
     PIM_CLI_DESTROY_COMPONENT,
     PIM_CLI_STATIC_RP_ENABLED,
     PIM_CLI_STATE_REFRESH_INTERVAL,
     PIM_CLI_NO_STATE_REFRESH_GENERATION,
     PIM_CLI_SRM_PROCESSING_DISABLED,
     PIM_CLI_SRM_PROCESSING_ENABLED,
     PIM_CLI_SOURCE_ACTIVE_INTERVAL,
     PIM_CLI_DEF_SOURCE_ACTIVE_INTERVAL,
     PIM_CLI_RPCANDIDATE_HOLDTIME,
     PIM_CLI_SET_COMP_MODE,
     PIMV6_CLI_STATUS,
     PIMV6_CLI_ENABLE_RPCANDIDATE_ADDRESS,
     PIMV6_CLI_DISABLE_RPCANDIDATE_ADDRESS,
     PIMV6_CLI_RPCANDIDATE_HOLDTIME,
     PIMV6_CLI_STATIC_RP_ADDRESS,
     PIMV6_CLI_NO_STATIC_RP_ADDRESS,
     PIMV6_CLI_HELLOINTERVAL,
     PIMV6_CLI_PIMJOINPRUNEINTERVAL,
     PIMV6_CLI_BSRCANDIDATE,
     PIMV6_CLI_BSRBORDER,
     PIMV6_CLI_PIMINTERFACE_COMP_ID,
     PIMV6_CLI_INT_HELLOHOLDTIME,
     PIMV6_CLI_INT_DRPRIORITY,
     PIMV6_CLI_INT_LAN_DELAY,
     PIMV6_CLI_INT_OVERRIDE_INTERVAL,
     PIMV6_CLI_LAN_PRUNE_DELAY_PRESENT,
     PIMV6_CLI_INT_GRAFT_RETRY_INTERVAL,
     PIMV6_CLI_INT_DEF_GRAFT_RETRY_INTERVAL,
     PIMV6_CLI_DEFAULT_HELLOINTERVAL,
     PIMV6_CLI_NO_INTERFACE,
     PIMV6_CLI_SHOW_PIM_INTERFACE,
     PIMV6_CLI_SHOW_PIM_NBR,
     PIMV6_CLI_SHOW_PIM_RPCANDIDATE,
     PIMV6_CLI_SHOW_PIM_RPSET,
     PIMV6_CLI_SHOW_PIM_BSR,
     PIMV6_CLI_SHOW_PIM_RPSTATIC,
     PIMV6_CLI_SHOW_COMPONENT,
     PIMV6_CLI_SHOW_THRESHOLDS,
     PIMV6_CLI_SHOW_RPF,
     PIMV6_CLI_SHOW_PIM_MULTICASTROUTE,
     PIM_CLI_SHOW_HA_STATE,
     PIM_CLI_SHOW_HA_SHADOWTABLE,
     PIMV6_CLI_SHOW_HA_SHADOWTABLE,
     PIM_CLI_NO_PIMINTERFACE_COMP_ID,
     PIMV6_CLI_NO_PIMINTERFACE_COMP_ID,
     PIM_CLI_RPF_STATUS,
     PIM_CLI_BIDIR_OFFER_INTERVAL,
     PIM_CLI_BIDIR_OFFER_LIMIT,
     PIM_CLI_BIDIR_DEF_OFFER_LIMIT,
     PIM_CLI_BIDIR_STATUS,
     PIM_CLI_SHOW_RP_HASH,
     PIM_SHOW_DF,
     PIM_SHOW_BIDIR,
     PIMV6_CLI_SHOW_RP_HASH,
     PIM_CLI_BIDIR_DEF_OFFER_INTERVAL,
     PIM_CLI_EXT_BORDER,
     PIM_CLI_SHOW_PIM_INTERFACE_DF,
     PIM_CLI_CLEAR_STATS,
     PIMV6_CLI_CLEAR_STATS
     
};

/* This value needs to be updated if any new command is added to the list */
#define PIM_CLI_MAX_COMMANDS                    (36)
#define PIM_MODE_SIZE                            (8)

#define CLI_PIM_NBR_MODULE               1
#define CLI_PIM_GRP_MODULE               2
#define CLI_PIM_JP_MODULE                4
#define CLI_PIM_AST_MODULE               8
#define CLI_PIM_BSR_MODULE               16
#define CLI_PIM_IO_MODULE                32
#define CLI_PIM_PMBR_MODULE              64
#define CLI_PIM_MRT_MODULE               128
#define CLI_PIM_MDH_MODULE               256
#define CLI_PIM_MGMT_MODULE              512
#define CLI_PIM_SRM_MODULE               1024
#define CLI_PIM_HA_MODULE                2048
#define CLI_PIM_DF_MODULE                4096
#define CLI_PIM_BMRT_MODULE              8192
#define CLI_PIM_NPAPI_MODULE             16384
#define CLI_PIM_INIT_SHUT_MODULE         32768   
#define CLI_PIM_OSRESOURCE_MODULE        65536
#define CLI_PIM_BUFFER_MODULE            131072
#define CLI_PIM_ENTRY_MODULE             262144
#define CLI_PIM_EXIT_MODULE              524288
#define CLI_PIM_ALL_MODULES              1048575
#define CLI_PIM_MAX_INT4                 0x7fffffff 
#define CLI_PIM_TRACE_DIS_FLAG       0x80000000

/* SIZE constants */
#define MAX_ADDR_BUFFER 256
#define MAX_MALLOC_SIZE 1000
#define CLI_PIM_COMP_MODE "pim-comp"

#define PIM_DEF_RP_ADDR                             0x00000000
#define PIM_DEF_CRP_HOLDTIME                        0
#define PIM_DEF_INT_MODE                            2
#define PIM_NO_INT_MODE                             0xFFFF
#define PIM_DEF_HELLO_INTERVAL                      30
#define PIM_DEF_JOINPRUNE_INTERVAL                  60
#define PIM_DEF_BSR_CANDIDATE                       -1
#define PIM_DEF_BSR_BORDER                          0
#define PIM_DEF_INTERFACE_COMP_ID                   0
#define PIM_DEF_ROUTER_MODE                         2
#define PIM_DEF_DR_PRIORITY                         1
#define PIM_DEF_HELLO_HOLDTIME                      105
#define PIM_DEF_LAN_DELAY                           0
#define PIM_DEF_OVERRIDE_INTERVAL                   0

#define MAX_INT_OBJECTS  SYS_MAX_INTERFACES 
                         /* Maximum Number of Interface Objects.*/
 
#define PIM_CLI_INTENTRY_SIZE ((MAX_INT_OBJECTS * CLI_MAX_COLUMN_WIDTH) + CLI_MAX_HEADER_SIZE)

#define PIM_SHOW_INTERFACE          1
#define PIM_SHOW_INTERFACE_DETAIL   2
#define PIM_SHOW_GROUP_SUMMARY      3
#define PIM_SHOW_SOURCE_SUMMARY     4
#define PIM_SHOW_COMP_SUMMARY       5
#define PIM_SHOW_PROXY_SUMMARY      6
#define PIM_MAX_ARGS               16
#define PIM_INVALID_COMPID         -1
#define PIM_INVALID_IFINDEX        -1
#define PIM_MAX_ADDR_BUFFER        256
#define PIM_MAX_BUF_VAL            40

INT1 cli_process_pim_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);
INT1 PimGetCompConfigPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1DispStr));

enum {
    CLI_PIM_UNKNOWN_ERR = 1,
    CLI_PIM_INVALID_COMP_ID,
    CLI_PIM_COMP_ENTRY_NOT_FOUND,
    CLI_PIM_INVALID_CRP_HOLDTIME,
    CLI_PIM_INTERFACE_NOT_FOUND,
    CLI_PIM_INVALID_GROUP_PREFIX,
    CLI_PIM_CANNOT_DESTROY_DEFAULT_COMPONENT,
    CLI_PIM_INVALID_COMP_MODE,
    CLI_PIM_CANDIDATE_RP_NOT_CONFIGURED,
    CLI_PIM_STATIC_RP_NOT_CONFIGURED,
    CLI_PIM_INTERFACE_NOT_CREATED,
    CLI_PIM_NO_ENTRIES_FOUND,
    CLI_PIM_NOT_ENABLED,
    CLI_PIM_EMBD_RP_NOT_SUPPORTED,
    CLI_PIM_INVALID_SCOPE_NAME,
    CLI_PIM_IF_MAPPED_SCOPE,
    CLI_PIM_SCOPE_REDEFINED,
    CLI_PIM_GRP_IF_SCOPE_NOT_MATCHED,
    CLI_PIM_GRP_COMP_SCOPE_NOT_MATCHED,
    CLI_PIM_SCOPE_DISABLED,
    CLI_PIM_MODE_CHG_DISABLED,
    CLI_PIM_IFACE_WITHOUT_COMP,
    CLI_PIM_COMP_IF_SCOPE_NOT_MATCHED,
    CLI_PIM_IGMP_PROXY_ENABLED,
    CLI_PIM_MISMATCH_COMP_ID,
    CLI_PIM_MAX_COMP_ERR,
    CLI_PIM_IFACE_MEM_EXHAUST,
    CLI_PIM_SCOPE_MEM_EXHAUST,
    CLI_PIM_ERR_RP,
    CLI_PIM_ERR_RP_DM,
    CLI_PIM_ERR_BIDIR_DM,
    CLI_PIM_ERR_RP_CONF,
    CLI_PIM_ERR_SSM_RANGE_RP_CONF,
    CLI_PIM_ERR_ADMIN_DOWN,
    CLI_PIM_ERR_PMBR,
    CLI_PIM_UNKNOWN_CMD,
    CLI_PIM_INVALID_STATUS,
    CLI_PIM_UNABLE_TO_REG_WITH_NETIP, 
    CLI_PIM_UNABLE_TO_REG_WITH_IP,
    CLI_PIM_UNABLE_TO_REG_WITH_IGMP, 
    CLI_PIM_UNABLE_TO_REG_FOR_MCAST_DATA,
    CLI_PIM_FAILED_TO_SET_MCAST_STATUS,
    CLI_PIM_UNABLE_TO_REG_WITH_IP6,
    CLI_PIM_UNABLE_TO_REG_WITH_MLD,
    CLI_PIM_INVALID_SPT_GRP_THRESHOLD,
    CLI_PIM_INVALID_SPT_SRC_THRESHOLD,
    CLI_PIM_INVALID_SPT_SWITCHING_PERIOD,
    CLI_PIM_INVALID_SPT_RP_THRESHOLD,
    CLI_PIM_INVALID_SPT_RP_SWITCHING_PERIOD,
    CLI_PIM_INVALID_REG_STOP_RATE_LIMITING_PERIOD,
    CLI_PIM_SCOPE_ENABLED,
    CLI_PIM_MEM_EXHAUST,
    CLI_PIM_HASH_TABLE_CREATION_ERR,
    CLI_PIM_COMP_CREATION_ERROR,
    CLI_PIM_INVALID_STATE_REF_INTERVAL,
    CLI_PIM_INVALID_SOURCE_ACTIVE_INTERVAL,
    CLI_PIM_COMP_PARAM_CHANGE_ERR,
    CLI_PIM_BM_NOT_ENABLED,
    CLI_PIM_INVALID_ADDR_SCOPE,
    CLI_PIM_ADDR_IS_LOOPBACK,
    CLI_PIM_FAILED_TO_ALLOC_CRU_BUFFER,
    CLI_PIM_Q_PKT_TO_IP,
    CLI_PIM_SEND_CRP_FAILED,
    CLI_PIM_GET_PORT_FROM_IFINDEX_ERR,
    CLI_PIM_DHCP_ENABLED,
    CLI_PIM_IFACE_NO_SCOPE_COUNT, 
    CLI_PIM_FAILED_TO_GET_VLANID,
    CLI_PIM_ROUTE_CREATION_FAILED,
    CLI_PIM_IPMC_INIT_FAILED,
    CLI_PIM_INVALID_HELLO_INTERVAL,
    CLI_PIM_INVALID_JOINPRUNE_INTERVAL,
    CLI_PIM_INVALID_CBSR_PREF,
    CLI_PIM_INVALID_DR_PRIORITY,
    CLI_PIM_INVALID_LAN_DELAY,
    CLI_PIM_INVALID_IF_OVERRIDE_INTERVAL,
    CLI_PIM_INVALID_IF_GRAFT_RETRY_INTERVAL,
    CLI_PIM_INVALID_BIDIR_OFFER_INTERVAL,
    CLI_PIM_INVALID_OFFER_LIMIT,
    CLI_PIM_NO_GROUP_MASK_NODE,
    CLI_PIM_GRP_PFX_VALIDATION_ERR,
    CLI_PIM_NO_DF,
    CLI_PIM_NO_PIM_NBR,
    CLI_PIM_HA_DISABLED,
    CLI_PIM_NO_GROUP_PREFIX_NODE,
    CLI_PIM_NP_PROGRAMMING_NOT_ALLOWED,
    CLI_PIM_STANDBY_PIM,
    CLI_PIM_FAILED_TO_DEL_ROUTE_ENTRY,
    CLI_PIM_FAILED_TO_ADD_ROUTE_ENTRY,
    CLI_PIM_FAILED_TO_DEL_OIF,
    CLI_PIM_FAILED_TO_ADD_OIF,
    CLI_PIM_FAILED_TO_ADD_IIF,
    CLI_PIM_MULTICAST_RANGE_SSM,
    CLI_PIM_NETIP_INIT_ERR,
    CLI_PIM_SSM_MAP_EXIST,
    CLI_PIM_MAX_ERR
};

#ifdef __PIMCLI_C__
CONST CHR1  *PimCliErrString [] = {
    NULL,
    "Unknown Error\r\n",
    "Invalid Component Id\r\n",
    "Component Entry not found \r\n",
    "CRP Holdtime Not Configured\r\n",
    "Interface Entry not found \r\n",
    "Invalid Group prefix \r\n",
    "Cannot Destroy Default Component \r\n",
    "Component Mode is not Sparse \r\n",
    "Candidate RP not Configured \r\n",
    "Static RP not Configured \r\n",
    "PIM Interface Not Created \r\n",
    "No Entries Found \r\n",
    "PIM cannot be enabled when DHCP is enabled.\r\n",
    "Embedded RP is not supported for PIMV4 nodes.\r\n",
    "Scope Zone Name is not matching with the scope configured in netip\r\n",
    "Scope Zone name can't be changed when the component is mapped with any PIM Interface.\r\n",
    "Scope Zone Name is already configured for another component\r\n",
    "Multicast Group scope is not matching with PIM interface scope.\r\n",
    "Multicast Group scope is not matching with component's scope.\r\n",
    "Not allowed when scope zone is not activated in netip.\r\n",
    "Mode change not allowed when scope zone is enabled in netip.\r\n",
    "PIM interface not mapped to any component.Map a component.\r\n",
    "Component's Scope is not matching with the interface scope in NETIP.\r\n",
    "PIM cannot be enabled when IGMP Proxy is enabled.\r\n",
    "Component Id is not matched\r\n",
    "Max PIM Component configured\r\n", 
    "Memory has been exhausted for pim Interface.\r\n",
    "Memory has been exhausted for pim scope.\r\n",
    "Error in enabling Static RP\r\n",
    "Static RP can't be enabled/disabled in Dense Mode\r\n",
    "Bidir Status can't be enabled/disabled in Dense Mode\r\n",
    "Static RP already exists for the group !!! \r\n",
    "For SSM Group Range, RP configuration is not allowed \r\n",
    "External border bit can be set only if, Admin status of the L3 interface is down \r\n",
    "Externl border bit can be set only if, Router is PMBR \r\n",
    "Command not in PIM scope \r\n",
    "Invalid Status \r\n",
    "Failed to register with NETIP \r\n",
    "Failed to register with IP \r\n",
    "Failed to register with IGMP \r\n",
    "Failed to register for Mcast Data \r\n",
    "Failed to set Mcast Status \r\n",
    "Failed to register with IP6 \r\n",
    "Failed to register with MLD \r\n",
    "Group threshold must be greater or equal to zero \r\n",
    "Source threshold must be greater than or equal to zero \r\n",
    "Switching period must be greater than or equal to zero \r\n",
    "RP threshold must be greater than or equal to zero \r\n",
    "RP switching period must be greater than or equal to zero \r\n",
    "Register stop rate limiting period must be greater than or equal to zero \r\n",
    "Pim Global is scope enabled. Please disable and try again \r\n",
    "Failed to allocate memory. MemPool exhausted \r\n",
    "Failed to create Hash table to store multicast routing information\r\n",
    "Failed to create component. Maximum component reached or MEM ALLOC failure \r\n",
    "Refresh interval value is out of range. \r\n",
    "SOURCE ACTIVE interval value is out of range. \r\n",
    "Error due to changes in the component parameters like zone name or Mode \r\n",
    "Bidirection PIM is disabled. Enable bidirectional PIM globally \r\n",
    "IP address does not belongs to any of the class \r\n",
    "Address provided is a loopback address \r\n",
    "Failed to allocate CRU buffer For the CRP message\r\n",
    "Failed in queuing PIM packets to IP \r\n",
    "Failure to send CRP message \r\n",
    "Failed to receive If index from port information \r\n",
    "DHCP to be disabled for PIM to be enabled\r\n",
    "Interface scope count is zero.No component is mapped. So it cannot be made ACTIVE \r\n",
    "Failed to get Vlanid for If Index \r\n",
    "Failed to create route entry \r\n",
    "Failed in IPMC init  \r\n",
    "Hello Interval is out of range. \r\n",
    "Join/Prune Interval is out of range. \r\n",
    "CBSR Preference is out of range. \r\n",
    "DR Priority is out of range.  \r\n",
    "Interface LAN Delay is out of range.  \r\n",
    "Interface Override Interval is out of range.  \r\n",
    "Interface Graft Retry Interval is out of range.\r\n",
    "Bidir Offer Interval is out of range. \r\n",
    "Bidir Offer Limit is out of range.  \r\n",
    "Failed to get Group Mask node \r\n",
    "Failed in Group Prefix Validation Failed \r\n",
    "DF node doesnot exists \r\n",
    "No PIM neighbor found \r\n",
    "High Availability is disabled for the current PIM instance \r\n",
    "Group Prefix does not exist \r\n",
    "NP Programming is disabled \r\n",
    "NP programming is not allowed when PIM is in standby mode\r\n",
    "Failed to delete route entry in NP\r\n",
    "Failed to add route entry to NP\r\n",
    "Failed to delete outgoing interface in NP\r\n",
    "Failed to add outgoing interface in NP\r\n",
    "Failed to add incoming interface in NP\r\n",
    "RP group address should not be in SSM range\r\n",
    "NETIP initialisation not done \r\n",
    "SSM Mapping range exists for the given multicast group address \r\n",
    "\r\n"
};
#endif

#ifdef PIM_TEST_WANTED
INT4 cli_process_pim_test_cmd (tCliHandle CliHandle,...);
#endif

#endif   /* PIMCLI_H */
