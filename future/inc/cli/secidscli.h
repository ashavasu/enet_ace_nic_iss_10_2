/********************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: secidscli.h,v 1.5 2012/03/21 13:14:45 siva Exp $
 *
 *******************************************************************************/
/* INCLUDE FILE HEADER :
 *
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : secidscli.h                                      |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : SECURITY                                         |
 * |                                                                           |
 * |  MODULE NAME           : CLI interface for SECURITY module                |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    :                                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION           : This file contains command idenfiers for         | 
 * |                          definitions in seccmd.def, function prototypes   |
 * |                          for SECCLI and corresponding error code          |
 * |                          definitions                                      |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 *
 */

#ifndef __SECIDSCLI_H__
#define __SECIDSCLI_H__
#include "cli.h"

/* Command Identifiers */
enum
{
    CLI_SEC_SHOW_VERSION = 1,
    CLI_SEC_SET_DEBUG_LEVEL,
    CLI_SEC_RESET_DEBUG_LEVEL,
    CLI_SEC_RELOAD_IDS,
    CLI_SEC_LOAD_IDS,
    CLI_SEC_UNLOAD_IDS,
    CLI_SEC_IDS_SET_LOG_SIZE,
    CLI_SEC_IDS_SET_LOG_THRESHOLD,
    CLI_SEC_IDS_SET_STATUS,
    CLI_SEC_IDS_SHOW_STATUS
};    

#define CLI_IDS_ENABLE 1
#define CLI_IDS_DISABLE 2

/* Function prototype */
PUBLIC INT4 cli_process_sec_cmd PROTO ((tCliHandle ,UINT4, ...));

INT4 SecCliShowIdsVersion PROTO ((tCliHandle));
INT4 SecCliSetDebugLevel PROTO ((tCliHandle CliHandle, INT4 i4CliDebugVal, UINT1 u1TraceFlag));
INT4 SecCliReloadIds PROTO ((tCliHandle CliHandle, INT4 i4Status));
INT4 SecCliLoadIds PROTO ((tCliHandle CliHandle));
INT4 SecCliUnloadIds PROTO ((tCliHandle CliHandle));
INT4 SecCliSetLogFileSize PROTO ((tCliHandle CliHandle,
                                  UINT4 u4IdsLogFileSize));
INT4 SecCliSetLogSizeThreshold PROTO ((tCliHandle CliHandle,
                                       UINT4 u4IdsLogThreshold));

INT4
SecCliSetIdsStatus (tCliHandle CliHandle, UINT4 u4IdsStatus);
INT4
SecCliShowIdsStatus (tCliHandle CliHandle);
/* Macro Definitions */
#define SEC_CLI_MAX_ARGS       10

#endif /* __SECIDSCLI_H__ */
