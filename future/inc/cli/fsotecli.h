/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsotecli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FutOspfTeAdminStatus[10];
extern UINT4 FutOspfTeTraceLevel[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFutOspfTeAdminStatus(i4SetValFutOspfTeAdminStatus)	\
	nmhSetCmn(FutOspfTeAdminStatus, 10, FutOspfTeAdminStatusSet, OspfTeLock, OspfTeUnLock, 0, 0, 0, "%i", i4SetValFutOspfTeAdminStatus)
#define nmhSetFutOspfTeTraceLevel(i4SetValFutOspfTeTraceLevel)	\
	nmhSetCmn(FutOspfTeTraceLevel, 10, FutOspfTeTraceLevelSet, OspfTeLock, OspfTeUnLock, 0, 0, 0, "%i", i4SetValFutOspfTeTraceLevel)

#endif
