/********************************************************************
 * Copyright (C) Future Software Limited,1997-98,2001
 *
 * $Id: issucli.h,v 1.8 2016/03/10 13:13:09 siva Exp $
 *
 * Description:This file contains prototypes 
 *******************************************************************/
/* Macro and prototypes for cli.h */
#ifndef __ISSUCLI_H__
#define __ISSUCLI_H__ 

enum
{
    CLI_ISSU_LOAD_VERSION = 1,
    CLI_ISSU_FORCE_STANDBY,
    CLI_ISSU_COMPATIBILITY_MATRIX_PATH,
    CLI_ISSU_COMPATIBILITY_CHECK_INIT,
    CLI_ISSU_MODE,
    CLI_ISSU_MAINTENANCE_MODE_ENABLE,
    CLI_ISSU_MAINTENANCE_MODE_DISABLE,
    CLI_TRAP_STATUS,
    CLI_SHOW_ISSU_STATE,
    CLI_SHOW_ISSU_STATE_DETAIL,
    CLI_SHOW_ISSU_COMP_MATRIX,
    CLI_SHOW_ISSU_COMP_MATRIX_STATUS,
    CLI_SHOW_ISSU_MAINTENANCE_MODE,
    CLI_SHOW_ISSU_MAINTENANCE_MODE_COMMANDS,
    CLI_ISSU_DEBUG,
    CLI_ISSU_NO_DEBUG
};

/* Error codes
 *  * Error code values and strings are maintained inside the protocol module itself.
 *   */
enum
{
 CLI_ISSU_MM_CMD_IN_PROGRESS = 1,
 CLI_ISSU_NODE_BLOCKED,
 CLI_ISSU_MODE_SET_NOT_ALLOWED,
 CLI_ISSU_MAINTENANCE_MODE_NOT_ALLOWED_IN_STANDBY,
 CLI_ISSU_MAINTENANCE_MODE_NOT_ALLOWED_PEER_DOWN,
 CLI_ISSU_MAINTENANCE_MODE_DISABLED,
 CLI_ISSU_NODE_IN_ACTIVE_STATE,
 CLI_ISSU_NODE_IN_STANDBY_STATE,
 CLI_ISSU_SW_UPGRADE_FAILED,
 CLI_ISSU_MM_ENABLE_FAILED,
 CLI_ISSU_MM_DISABLE_FAILED,
 CLI_ISSU_NOT_INCOMPATIBLE_MODE,
 CLI_ISSU_BASECOMPATIBLE_MODE_NOT_SUPPORTED,
 CLI_ISSU_LOAD_SW_PATH_MAX_SIZE_EXCEEDED,
 CLI_ISSU_MSTP_RSTP_PVRST_ENABLED,
 CLI_ISSU_MAX_ERR
};

/* The error strings should be places under the switch so as to avoid redefinition
 *  * This will be visible only in modulecli.c file
 *   */
#ifdef ISSUCLI_C

CONST CHR1  *IssuCliErrString [] = {
 NULL,
 "% Maintenance Mode cannot be disabled as ISSU Command is in Progress \r\n",
 "% Node is in standby state cannot configure ISSU mode \r\n",
 "% Maintenance Mode is Enabled ISSU mode cannot be set\r\n",
 "% Node is in standby state cannot configure ISSU Maintenance Mode \r\n",
 "% Peer Node is down ISSU Maintenance Mode cannot be enabled \r\n",
 "% Maintenance mode is disabled cannot configure ISSU command \r\n",
 "% Load version is not allowed in Active Node \r\n",
 "% Node is already in Standby State \r\n",
 "% Failed to Upgrade the Software \r\n",
 "% Failed to Enter Maintenance Mode \r\n",
 "% Failed to Exit Maintenance Mode \r\n",
 "% Force-Standby is allowed only in-compatible mode \r\n",
 "% Base Compatible Mode is not supported \r\n",
 "% Load software path size should not exceed 127 characters \r\n",
 "% Spanning Tree feature is enabled and hence Warm Boot cannot be initiated \r\n",
 "\r\n"
};
#else
extern CONST CHR1  *IssuCliErrString [];
#endif /*ISSUCLI_C*/


#define ISSU_CLI_FULL_COMPATIBLE_MODE       1
#define ISSU_CLI_BASE_COMPATIBLE_MODE       2
#define ISSU_CLI_INCOMPATIBLE_MODE          3 

#define ISSU_CLI_TRAP_ENABLE                1
#define ISSU_CLI_TRAP_DISABLE               2

INT4 cli_process_issu_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);

/******************************/
/* Util APIs for config Command*/
/******************************/
INT4 IssuCliSetIssuCommand(tCliHandle,UINT4,UINT1 *);
INT4 IssuCliSetSoftwareCompatFilePath(tCliHandle,UINT1 *);
INT4 IssuCliSetSoftwareCompatCheckInit(tCliHandle, UINT1 *);
INT4 IssuCliSetIssuMode(tCliHandle, INT4);
INT4 IssuCliSetMaintenanceMode (tCliHandle, INT4);
INT4 IssuCliSetTrapStatus(tCliHandle, INT4);

/******************************/
/* Util APIs for Debug Command*/
/******************************/
INT4 IssuCliSetTraceOption(tCliHandle, INT4, UINT1);
VOID IssuCliShowDebug (tCliHandle CliHandle);

/******************************/
/* Util APIs for Show Command */
/******************************/
INT4 IssuCliShowIssuState (tCliHandle);
INT4 IssuCliShowIssuStateDetail (tCliHandle);
INT4 IssuCliShowIssuCompatibilityMatrixFile (tCliHandle);
INT4 IssuCliShowIssuCompatMatrixStatus (tCliHandle);
VOID IssuCliShowIssuMaintenanceMode (tCliHandle);
VOID IssuCliShowIssuMaintenanceModeCommands (tCliHandle);

#endif   /* __ISSUCLI_H__ */

/*-----------------------------------------------------------------------*/
/*                       End of the file  issucli.h                      */
/*-----------------------------------------------------------------------*/
