/*************************************************************************
* Copyright (C) 2007-2015 Aricent Group . All Rights Reserved
*
* $Id: fsbcli.h,v 1.6 2017/10/09 13:13:59 siva Exp $
*
* Description: This file contains command idenfiers for
*               definitions in fsbcmd.def, command macros
*               for FSB CLI and corresponding error code definitions
*
*************************************************************************/
#ifndef _FSBCLI_H
#define _FSBCLI_H

#include "cli.h"

/* FSB CLI command constants. To add a new command ,a new definition needs to
 * be added to the list.
 */

enum {
    CLI_FSB_SHUTDOWN = 1,
    CLI_FSB_START,
    CLI_FSB_MODULE_STATUS,
    CLI_FSB_FCMAP_MODE,
    CLI_FSB_SET_FCMAP,
    CLI_FSB_HOUSE_KEEPING_PERIOD,
    CLI_FSB_DEBUG,
    CLI_FSB_TRAP_STATUS,
    CLI_FSB_UNTAGGED_VLAN,
    CLI_FSB_SET_FCOE_VLAN,
    CLI_FSB_SET_VLAN_STATUS,
    CLI_FSB_SET_VLAN_FCMAP,
    CLI_FSB_ADD_PINNED_PORTS,
    CLI_FSB_DELETE_PINNED_PORTS,
    CLI_FSB_PORT_ROLE,
    CLI_FSB_CLEAR_STATS,
    CLI_FSB_SHOW_FIP_STATUS,
    CLI_FSB_SHOW_VLAN,
    CLI_FSB_SHOW_INT,
    CLI_FSB_SHOW_FIP_SESSION,
    CLI_FSB_SHOW_FIP_FCF,
    CLI_FSB_SHOW_GLOBAL_STATS,
    CLI_FSB_SHOW_VLAN_STATS,
    CLI_FSB_SHOW_SESSION_STATS,
    CLI_FSB_SHOW_CXT_FILTER,
    CLI_FSB_SHOW_VLAN_FILTER,
    CLI_FSB_SHOW_SESSION_FILTER,
    CLI_FSB_SHOW_SCHANNEL_FILTER,
    CLI_FSB_SHOW_FCF_FILTER,
    CLI_FSB_SHOW_SESSION_ENTRIES,
    CLI_FSB_SEVERITY_LEVEL,
    CLI_FSB_MAX_CMDS
};

/* Error codes
 * Error code values and strings are maintained inside the protocol module itself.
 */
enum {
    CLI_FSB_ERR_MODULE_SHUTDOWN = 1,
    CLI_FSB_ERR_MODULE_ENABLED,
    CLI_FSB_ERR_VLAN_NOT_ACTIVE,
    CLI_FSB_ERR_NO_CONFIG_UNTAGGED_VLAN,
    CLI_FSB_ERR_MAX_FCOE_VLAN_EXCEEDS,
    CLI_FSB_ERR_NO_FCOE_VLAN,
    CLI_FSB_ERR_NO_PORT_IN_VLAN,
    CLI_FSB_ERR_NO_CONTEXT_MAPPED_TO_PORT,
    CLI_FSB_ERR_NO_FCOE_PORT,
    CLI_FSB_ERR_PORT_OPER_DOWN,
    CLI_FSB_ERR_DCBX_DISABLED,
    CLI_FSB_ERR_DCBX_ADMIN_STATUS_DOWN,
    CLI_FSB_ERR_INVALID_PINNED_PORTS,
    CLI_FSB_MAX_ERR
};

/* The error strings should be places under the switch so as to avoid redifinition
 * This will be visible only in modulecli.c file
 */
#ifdef _FSBCLI_C_

CONST CHR1  *FsbCliErrString [] = {
    NULL,
    "%% FSB Module is ShutDown \r\n",
    "%% Configuration not allowed since Module Status is Enabled \r\n",
    "%% VLAN entered is not Active \r\n",
    "%% Untagged VLAN needs to be configured before Enabling the Module Status \r\n",
    "%% Configuration of MAX number of FCoE VLAN is reached\r\n",
    "%% VLAN entered is not FCoE VLAN \r\n",
    "%% No ports in Vlan. Cannot change Enabled status\r\n",
    "%% Port is not mapped to a Context\r\n",
    "%% Port is not mapped to FCoE VLAN \r\n",
    "%% Oper Status for ports in the VLAN is Down",
    "%% DCBX/PFC/ETS Module status is Disabled",
    "%% DCBX admin status is not enabled on all the ports in the FCoE VLAN",
    "%% Pinned Ports should not span across the Switching Units\r\n",
    "\r\n"
};

#endif  /* _FSBCLI_C_ */

/* CLI-MIB Configuration Related Macros */

#define FSB_ENABLE                          OSIX_ENABLED
#define FSB_DISABLE                         OSIX_DISABLED

#define FSB_START                           1
#define FSB_SHUTDOWN                        2

#define FSB_FCMAP_GLOBAL                    1
#define FSB_FCMAP_VLAN                      2

#define FSB_NO_HOUSEKEEPING_TIME            0
#define FSB_MIN_HOUSEKEEPING_TIME           250
#define FSB_MAX_HOUSEKEEPING_TIME           350

#define FSB_ENODE_FACING                    1
#define FSB_FCF_FACING                      2
#define FSB_ENODE_FCF_FACING                3

/*FSB Severity levels for Traces*/
#define FSB_EMERGENCY_LEVEL               0
#define FSB_ALERT_LEVEL                   1
#define FSB_CRITICAL_LEVEL                2
#define FSB_ERROR_LEVEL                   3
#define FSB_WARNING_LEVEL                 4
#define FSB_NOTIFICATION_LEVEL            5
#define FSB_INFORMATIONAL_LEVEL           6
#define FSB_DEBUGGING_LEVEL               7
#define FSB_NONE                           1

#define FSB_INIT_SHUT_TRC                   INIT_SHUT_TRC
#define FSB_MGMT_TRC                        MGMT_TRC
#define FSB_DATA_PATH_TRC                   DATA_PATH_TRC
#define FSB_CONTROL_PATH_TRC                CONTROL_PLANE_TRC
#define FSB_DUMP_TRC                        DUMP_TRC
#define FSB_OS_RESOURCE_TRC                 OS_RESOURCE_TRC
#define FSB_ALL_FAILURE_TRC                 ALL_FAILURE_TRC
#define FSB_BUFFER_TRC                      BUFFER_TRC
#define FSB_CRITICAL_TRC                    0x00000100
#define FSB_SESSION_TRC                     0x00000200
#define FSB_NO_TRC                          NO_TRC

/* MIB Default Macros */
#define FSB_DEFAULT_SYSTEM_CONTROL          FSB_SHUTDOWN
#define FSB_DEFAULT_MODULE_STATUS           FSB_DISABLE
#define FSB_DEFAULT_FCMAP_MODE              FSB_FCMAP_GLOBAL
#define FSB_DEFAULT_HOUSEKEEPING_TIME       300
#define FSB_DEFAULT_TRACE_OPTION            FSB_NONE
#define FSB_DEFAULT_TRAP_OPTION             FSB_DISABLE
#define FSB_DEFAULT_CLEAR_STATS             FSB_FALSE
#define FSB_DEFAULT_VLAN_INIT_VAL           1
#define FSB_DEFAULT_VLAN_ENABLED_STATUS     FSB_DISABLE
#define FSB_DEFAULT_PORT_ROLE               FSB_ENODE_FACING

#define FSB_CLI_INVALID_CONTEXT             0xFFFFFFFF

#define FSB_TRC_ALL                         (FSB_INIT_SHUT_TRC | \
                                            FSB_MGMT_TRC | \
                                            FSB_DATA_PATH_TRC | \
                                            FSB_CONTROL_PATH_TRC | \
                                            FSB_OS_RESOURCE_TRC | \
                                            FSB_ALL_FAILURE_TRC | \
                                            FSB_BUFFER_TRC | \
                                            FSB_CRITICAL_TRC | \
                                            FSB_SESSION_TRC)

/* Extern declaration */
INT4 cli_process_fsb_command PROTO ((tCliHandle, UINT4, ...));
PUBLIC VOID IssFsbShowDebugging PROTO ((tCliHandle CliHandle));

#endif  /* _FSBCLI_H */


