/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsissacli.h,v 1.1.1.1 2015/03/04 10:39:47 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IssAclRateCtrlIndex[13];
extern UINT4 IssAclRateCtrlDLFLimitValue[13];
extern UINT4 IssAclRateCtrlBCASTLimitValue[13];
extern UINT4 IssAclRateCtrlMCASTLimitValue[13];
extern UINT4 IssAclRateCtrlPortRateLimit[13];
extern UINT4 IssAclRateCtrlPortBurstSize[13];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IssAclL2FilterNo[13];
extern UINT4 IssAclL2FilterPriority[13];
extern UINT4 IssAclL2FilterEtherType[13];
extern UINT4 IssAclL2FilterProtocolType[13];
extern UINT4 IssAclL2FilterDstMacAddr[13];
extern UINT4 IssAclL2FilterSrcMacAddr[13];
extern UINT4 IssAclL2FilterVlanId[13];
extern UINT4 IssAclL2FilterInPortList[13];
extern UINT4 IssAclL2FilterAction[13];
extern UINT4 IssAclL2FilterStatus[13];
extern UINT4 IssAclL2FilterOutPortList[13];
extern UINT4 IssAclL2FilterDirection[13];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IssAclL3FilterNo[13];
extern UINT4 IssAclL3FilterPriority[13];
extern UINT4 IssAclL3FilterProtocol[13];
extern UINT4 IssAclL3FilterMessageType[13];
extern UINT4 IssAclL3FilterMessageCode[13];
extern UINT4 IssAclL3FilteAddrType[13];
extern UINT4 IssAclL3FilterDstIpAddr[13];
extern UINT4 IssAclL3FilterSrcIpAddr[13];
extern UINT4 IssAclL3FilterDstIpAddrPrefixLength[13];
extern UINT4 IssAclL3FilterSrcIpAddrPrefixLength[13];
extern UINT4 IssAclL3FilterMinDstProtPort[13];
extern UINT4 IssAclL3FilterMaxDstProtPort[13];
extern UINT4 IssAclL3FilterMinSrcProtPort[13];
extern UINT4 IssAclL3FilterMaxSrcProtPort[13];
extern UINT4 IssAclL3FilterInPortList[13];
extern UINT4 IssAclL3FilterOutPortList[13];
extern UINT4 IssAclL3FilterAckBit[13];
extern UINT4 IssAclL3FilterRstBit[13];
extern UINT4 IssAclL3FilterTos[13];
extern UINT4 IssAclL3FilterDscp[13];
extern UINT4 IssAclL3FilterDirection[13];
extern UINT4 IssAclL3FilterAction[13];
extern UINT4 IssAclL3FilterFlowId[13];
extern UINT4 IssAclL3FilterStatus[13];
