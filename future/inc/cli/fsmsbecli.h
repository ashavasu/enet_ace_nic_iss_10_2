/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmsbecli.h,v 1.2 2008/10/31 13:09:30 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDot1dBridgeContextId[14];
extern UINT4 FsDot1dTrafficClassesEnabled[14];
extern UINT4 FsDot1dGmrpStatus[14];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDot1dPortDefaultUserPriority[14];
extern UINT4 FsDot1dPortNumTrafficClasses[14];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDot1dUserPriority[14];
extern UINT4 FsDot1dRegenUserPriority[14];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDot1dTrafficClassPriority[14];
extern UINT4 FsDot1dTrafficClass[14];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDot1dPortGarpJoinTime[14];
extern UINT4 FsDot1dPortGarpLeaveTime[14];
extern UINT4 FsDot1dPortGarpLeaveAllTime[14];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDot1dPortGmrpStatus[14];
extern UINT4 FsDot1dPortRestrictedGroupRegistration[14];

