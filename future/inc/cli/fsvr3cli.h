/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsvr3cli.h,v 1.2 2014/03/04 11:17:43 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsVrrpVersionSupported[12];
extern UINT4 FsVrrpv3TraceOption[12];
extern UINT4 FsVrrpv3NotificationCntl[12];
extern UINT4 FsVrrpv3Status[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsVrrpVersionSupported(i4SetValFsVrrpVersionSupported)	\
	nmhSetCmn(FsVrrpVersionSupported, 12, FsVrrpVersionSupportedSet, VrrpLock, VrrpUnLock, 0, 0, 0, "%i", i4SetValFsVrrpVersionSupported)
#define nmhSetFsVrrpv3TraceOption(i4SetValFsVrrpv3TraceOption)	\
	nmhSetCmn(FsVrrpv3TraceOption, 12, FsVrrpv3TraceOptionSet, VrrpLock, VrrpUnLock, 0, 0, 0, "%i", i4SetValFsVrrpv3TraceOption)
#define nmhSetFsVrrpv3NotificationCntl(i4SetValFsVrrpv3NotificationCntl)	\
	nmhSetCmn(FsVrrpv3NotificationCntl, 12, FsVrrpv3NotificationCntlSet, VrrpLock, VrrpUnLock, 0, 0, 0, "%i", i4SetValFsVrrpv3NotificationCntl)
#define nmhSetFsVrrpv3Status(i4SetValFsVrrpv3Status)	\
	nmhSetCmn(FsVrrpv3Status, 12, FsVrrpv3StatusSet, VrrpLock, VrrpUnLock, 0, 0, 0, "%i", i4SetValFsVrrpv3Status)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsVrrpv3OperationsTrackGroupId[14];
extern UINT4 FsVrrpv3OperationsDecrementPriority[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsVrrpv3OperationsTrackGroupId(i4IfIndex , i4Vrrpv3OperationsVrId , i4Vrrpv3OperationsInetAddrType ,u4SetValFsVrrpv3OperationsTrackGroupId)	\
	nmhSetCmn(FsVrrpv3OperationsTrackGroupId, 14, FsVrrpv3OperationsTrackGroupIdSet, VrrpLock, VrrpUnLock, 0, 0, 3, "%i %i %i %u", i4IfIndex , i4Vrrpv3OperationsVrId , i4Vrrpv3OperationsInetAddrType ,u4SetValFsVrrpv3OperationsTrackGroupId)
#define nmhSetFsVrrpv3OperationsDecrementPriority(i4IfIndex , i4Vrrpv3OperationsVrId , i4Vrrpv3OperationsInetAddrType ,u4SetValFsVrrpv3OperationsDecrementPriority)	\
	nmhSetCmn(FsVrrpv3OperationsDecrementPriority, 14, FsVrrpv3OperationsDecrementPrioritySet, VrrpLock, VrrpUnLock, 0, 0, 3, "%i %i %i %u", i4IfIndex , i4Vrrpv3OperationsVrId , i4Vrrpv3OperationsInetAddrType ,u4SetValFsVrrpv3OperationsDecrementPriority)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsVrrpv3OperationsTrackGroupIndex[14];
extern UINT4 FsVrrpv3OperationsTrackedGroupTrackedLinks[14];
extern UINT4 FsVrrpv3OperationsTrackRowStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsVrrpv3OperationsTrackedGroupTrackedLinks(u4FsVrrpv3OperationsTrackGroupIndex ,u4SetValFsVrrpv3OperationsTrackedGroupTrackedLinks)	\
	nmhSetCmn(FsVrrpv3OperationsTrackedGroupTrackedLinks, 14, FsVrrpv3OperationsTrackedGroupTrackedLinksSet, VrrpLock, VrrpUnLock, 0, 0, 1, "%u %u", u4FsVrrpv3OperationsTrackGroupIndex ,u4SetValFsVrrpv3OperationsTrackedGroupTrackedLinks)
#define nmhSetFsVrrpv3OperationsTrackRowStatus(u4FsVrrpv3OperationsTrackGroupIndex ,i4SetValFsVrrpv3OperationsTrackRowStatus)	\
	nmhSetCmn(FsVrrpv3OperationsTrackRowStatus, 14, FsVrrpv3OperationsTrackRowStatusSet, VrrpLock, VrrpUnLock, 0, 1, 1, "%u %i", u4FsVrrpv3OperationsTrackGroupIndex ,i4SetValFsVrrpv3OperationsTrackRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsVrrpv3OperationsTrackGroupIfIndex[14];
extern UINT4 FsVrrpv3OperationsTrackGroupIfRowStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsVrrpv3OperationsTrackGroupIfRowStatus(u4FsVrrpv3OperationsTrackGroupIndex , i4FsVrrpv3OperationsTrackGroupIfIndex ,i4SetValFsVrrpv3OperationsTrackGroupIfRowStatus)	\
	nmhSetCmn(FsVrrpv3OperationsTrackGroupIfRowStatus, 14, FsVrrpv3OperationsTrackGroupIfRowStatusSet, VrrpLock, VrrpUnLock, 0, 1, 2, "%u %i %i", u4FsVrrpv3OperationsTrackGroupIndex , i4FsVrrpv3OperationsTrackGroupIfIndex ,i4SetValFsVrrpv3OperationsTrackGroupIfRowStatus)

#endif
