/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdvrrcli.h,v 1.3 2014/03/11 14:08:54 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 VrrpNotificationCntl[9];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetVrrpNotificationCntl(i4SetValVrrpNotificationCntl) \
 nmhSetCmn(VrrpNotificationCntl, 9, VrrpNotificationCntlSet, VrrpLock, VrrpUnLock, 0, 0, 0, "%i", i4SetValVrrpNotificationCntl)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 VrrpOperVrId[11];
extern UINT4 VrrpOperAdminState[11];
extern UINT4 VrrpOperPriority[11];
extern UINT4 VrrpOperPrimaryIpAddr[11];
extern UINT4 VrrpOperAuthType[11];
extern UINT4 VrrpOperAuthKey[11];
extern UINT4 VrrpOperAdvertisementInterval[11];
extern UINT4 VrrpOperPreemptMode[11];
extern UINT4 VrrpOperProtocol[11];
extern UINT4 VrrpOperRowStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetVrrpOperAdminState(i4IfIndex , i4VrrpOperVrId ,i4SetValVrrpOperAdminState) \
 nmhSetCmn(VrrpOperAdminState, 11, VrrpOperAdminStateSet, VrrpLock, VrrpUnLock, 0, 0, 2, "%i %i %i", i4IfIndex , i4VrrpOperVrId ,i4SetValVrrpOperAdminState)
#define nmhSetVrrpOperPriority(i4IfIndex , i4VrrpOperVrId ,i4SetValVrrpOperPriority) \
 nmhSetCmn(VrrpOperPriority, 11, VrrpOperPrioritySet, VrrpLock, VrrpUnLock, 0, 0, 2, "%i %i %i", i4IfIndex , i4VrrpOperVrId ,i4SetValVrrpOperPriority)
#define nmhSetVrrpOperPrimaryIpAddr(i4IfIndex , i4VrrpOperVrId ,u4SetValVrrpOperPrimaryIpAddr) \
 nmhSetCmn(VrrpOperPrimaryIpAddr, 11, VrrpOperPrimaryIpAddrSet, VrrpLock, VrrpUnLock, 0, 0, 2, "%i %i %p", i4IfIndex , i4VrrpOperVrId ,u4SetValVrrpOperPrimaryIpAddr)
#define nmhSetVrrpOperAuthType(i4IfIndex , i4VrrpOperVrId ,i4SetValVrrpOperAuthType) \
 nmhSetCmn(VrrpOperAuthType, 11, VrrpOperAuthTypeSet, VrrpLock, VrrpUnLock, 0, 0, 2, "%i %i %i", i4IfIndex , i4VrrpOperVrId ,i4SetValVrrpOperAuthType)
#define nmhSetVrrpOperAuthKey(i4IfIndex , i4VrrpOperVrId ,pSetValVrrpOperAuthKey) \
 nmhSetCmn(VrrpOperAuthKey, 11, VrrpOperAuthKeySet, VrrpLock, VrrpUnLock, 0, 0, 2, "%i %i %s", i4IfIndex , i4VrrpOperVrId ,pSetValVrrpOperAuthKey)
#define nmhSetVrrpOperAdvertisementInterval(i4IfIndex , i4VrrpOperVrId ,i4SetValVrrpOperAdvertisementInterval) \
 nmhSetCmn(VrrpOperAdvertisementInterval, 11, VrrpOperAdvertisementIntervalSet, VrrpLock, VrrpUnLock, 0, 0, 2, "%i %i %i", i4IfIndex , i4VrrpOperVrId ,i4SetValVrrpOperAdvertisementInterval)
#define nmhSetVrrpOperPreemptMode(i4IfIndex , i4VrrpOperVrId ,i4SetValVrrpOperPreemptMode) \
 nmhSetCmn(VrrpOperPreemptMode, 11, VrrpOperPreemptModeSet, VrrpLock, VrrpUnLock, 0, 0, 2, "%i %i %i", i4IfIndex , i4VrrpOperVrId ,i4SetValVrrpOperPreemptMode)
#define nmhSetVrrpOperProtocol(i4IfIndex , i4VrrpOperVrId ,i4SetValVrrpOperProtocol) \
 nmhSetCmn(VrrpOperProtocol, 11, VrrpOperProtocolSet, VrrpLock, VrrpUnLock, 0, 0, 2, "%i %i %i", i4IfIndex , i4VrrpOperVrId ,i4SetValVrrpOperProtocol)
#define nmhSetVrrpOperRowStatus(i4IfIndex , i4VrrpOperVrId ,i4SetValVrrpOperRowStatus) \
 nmhSetCmn(VrrpOperRowStatus, 11, VrrpOperRowStatusSet, VrrpLock, VrrpUnLock, 0, 1, 2, "%i %i %i", i4IfIndex , i4VrrpOperVrId ,i4SetValVrrpOperRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 VrrpAssoIpAddr[11];
extern UINT4 VrrpAssoIpAddrRowStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetVrrpAssoIpAddrRowStatus(i4IfIndex , i4VrrpOperVrId , u4VrrpAssoIpAddr ,i4SetValVrrpAssoIpAddrRowStatus) \
 nmhSetCmn(VrrpAssoIpAddrRowStatus, 11, VrrpAssoIpAddrRowStatusSet, VrrpLock, VrrpUnLock, 0, 1, 3, "%i %i %p %i", i4IfIndex , i4VrrpOperVrId , u4VrrpAssoIpAddr ,i4SetValVrrpAssoIpAddrRowStatus)

#endif
