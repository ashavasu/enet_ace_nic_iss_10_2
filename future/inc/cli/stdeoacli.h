/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdeoacli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Dot3OamAdminState[11];
extern UINT4 Dot3OamMode[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDot3OamAdminState(i4IfIndex ,i4SetValDot3OamAdminState)	\
	nmhSetCmn(Dot3OamAdminState, 11, Dot3OamAdminStateSet, EoamLock, EoamUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValDot3OamAdminState)
#define nmhSetDot3OamMode(i4IfIndex ,i4SetValDot3OamMode)	\
	nmhSetCmn(Dot3OamMode, 11, Dot3OamModeSet, EoamLock, EoamUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValDot3OamMode)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Dot3OamLoopbackStatus[11];
extern UINT4 Dot3OamLoopbackIgnoreRx[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDot3OamLoopbackStatus(i4IfIndex ,i4SetValDot3OamLoopbackStatus)	\
	nmhSetCmn(Dot3OamLoopbackStatus, 11, Dot3OamLoopbackStatusSet, EoamLock, EoamUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValDot3OamLoopbackStatus)
#define nmhSetDot3OamLoopbackIgnoreRx(i4IfIndex ,i4SetValDot3OamLoopbackIgnoreRx)	\
	nmhSetCmn(Dot3OamLoopbackIgnoreRx, 11, Dot3OamLoopbackIgnoreRxSet, EoamLock, EoamUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValDot3OamLoopbackIgnoreRx)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Dot3OamErrSymPeriodWindowHi[11];
extern UINT4 Dot3OamErrSymPeriodWindowLo[11];
extern UINT4 Dot3OamErrSymPeriodThresholdHi[11];
extern UINT4 Dot3OamErrSymPeriodThresholdLo[11];
extern UINT4 Dot3OamErrSymPeriodEvNotifEnable[11];
extern UINT4 Dot3OamErrFramePeriodWindow[11];
extern UINT4 Dot3OamErrFramePeriodThreshold[11];
extern UINT4 Dot3OamErrFramePeriodEvNotifEnable[11];
extern UINT4 Dot3OamErrFrameWindow[11];
extern UINT4 Dot3OamErrFrameThreshold[11];
extern UINT4 Dot3OamErrFrameEvNotifEnable[11];
extern UINT4 Dot3OamErrFrameSecsSummaryWindow[11];
extern UINT4 Dot3OamErrFrameSecsSummaryThreshold[11];
extern UINT4 Dot3OamErrFrameSecsEvNotifEnable[11];
extern UINT4 Dot3OamDyingGaspEnable[11];
extern UINT4 Dot3OamCriticalEventEnable[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDot3OamErrSymPeriodWindowHi(i4IfIndex ,u4SetValDot3OamErrSymPeriodWindowHi)	\
	nmhSetCmn(Dot3OamErrSymPeriodWindowHi, 11, Dot3OamErrSymPeriodWindowHiSet, EoamLock, EoamUnLock, 0, 0, 1, "%i %u", i4IfIndex ,u4SetValDot3OamErrSymPeriodWindowHi)
#define nmhSetDot3OamErrSymPeriodWindowLo(i4IfIndex ,u4SetValDot3OamErrSymPeriodWindowLo)	\
	nmhSetCmn(Dot3OamErrSymPeriodWindowLo, 11, Dot3OamErrSymPeriodWindowLoSet, EoamLock, EoamUnLock, 0, 0, 1, "%i %u", i4IfIndex ,u4SetValDot3OamErrSymPeriodWindowLo)
#define nmhSetDot3OamErrSymPeriodThresholdHi(i4IfIndex ,u4SetValDot3OamErrSymPeriodThresholdHi)	\
	nmhSetCmn(Dot3OamErrSymPeriodThresholdHi, 11, Dot3OamErrSymPeriodThresholdHiSet, EoamLock, EoamUnLock, 0, 0, 1, "%i %u", i4IfIndex ,u4SetValDot3OamErrSymPeriodThresholdHi)
#define nmhSetDot3OamErrSymPeriodThresholdLo(i4IfIndex ,u4SetValDot3OamErrSymPeriodThresholdLo)	\
	nmhSetCmn(Dot3OamErrSymPeriodThresholdLo, 11, Dot3OamErrSymPeriodThresholdLoSet, EoamLock, EoamUnLock, 0, 0, 1, "%i %u", i4IfIndex ,u4SetValDot3OamErrSymPeriodThresholdLo)
#define nmhSetDot3OamErrSymPeriodEvNotifEnable(i4IfIndex ,i4SetValDot3OamErrSymPeriodEvNotifEnable)	\
	nmhSetCmn(Dot3OamErrSymPeriodEvNotifEnable, 11, Dot3OamErrSymPeriodEvNotifEnableSet, EoamLock, EoamUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValDot3OamErrSymPeriodEvNotifEnable)
#define nmhSetDot3OamErrFramePeriodWindow(i4IfIndex ,u4SetValDot3OamErrFramePeriodWindow)	\
	nmhSetCmn(Dot3OamErrFramePeriodWindow, 11, Dot3OamErrFramePeriodWindowSet, EoamLock, EoamUnLock, 0, 0, 1, "%i %u", i4IfIndex ,u4SetValDot3OamErrFramePeriodWindow)
#define nmhSetDot3OamErrFramePeriodThreshold(i4IfIndex ,u4SetValDot3OamErrFramePeriodThreshold)	\
	nmhSetCmn(Dot3OamErrFramePeriodThreshold, 11, Dot3OamErrFramePeriodThresholdSet, EoamLock, EoamUnLock, 0, 0, 1, "%i %u", i4IfIndex ,u4SetValDot3OamErrFramePeriodThreshold)
#define nmhSetDot3OamErrFramePeriodEvNotifEnable(i4IfIndex ,i4SetValDot3OamErrFramePeriodEvNotifEnable)	\
	nmhSetCmn(Dot3OamErrFramePeriodEvNotifEnable, 11, Dot3OamErrFramePeriodEvNotifEnableSet, EoamLock, EoamUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValDot3OamErrFramePeriodEvNotifEnable)
#define nmhSetDot3OamErrFrameWindow(i4IfIndex ,u4SetValDot3OamErrFrameWindow)	\
	nmhSetCmn(Dot3OamErrFrameWindow, 11, Dot3OamErrFrameWindowSet, EoamLock, EoamUnLock, 0, 0, 1, "%i %u", i4IfIndex ,u4SetValDot3OamErrFrameWindow)
#define nmhSetDot3OamErrFrameThreshold(i4IfIndex ,u4SetValDot3OamErrFrameThreshold)	\
	nmhSetCmn(Dot3OamErrFrameThreshold, 11, Dot3OamErrFrameThresholdSet, EoamLock, EoamUnLock, 0, 0, 1, "%i %u", i4IfIndex ,u4SetValDot3OamErrFrameThreshold)
#define nmhSetDot3OamErrFrameEvNotifEnable(i4IfIndex ,i4SetValDot3OamErrFrameEvNotifEnable)	\
	nmhSetCmn(Dot3OamErrFrameEvNotifEnable, 11, Dot3OamErrFrameEvNotifEnableSet, EoamLock, EoamUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValDot3OamErrFrameEvNotifEnable)
#define nmhSetDot3OamErrFrameSecsSummaryWindow(i4IfIndex ,i4SetValDot3OamErrFrameSecsSummaryWindow)	\
	nmhSetCmn(Dot3OamErrFrameSecsSummaryWindow, 11, Dot3OamErrFrameSecsSummaryWindowSet, EoamLock, EoamUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValDot3OamErrFrameSecsSummaryWindow)
#define nmhSetDot3OamErrFrameSecsSummaryThreshold(i4IfIndex ,i4SetValDot3OamErrFrameSecsSummaryThreshold)	\
	nmhSetCmn(Dot3OamErrFrameSecsSummaryThreshold, 11, Dot3OamErrFrameSecsSummaryThresholdSet, EoamLock, EoamUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValDot3OamErrFrameSecsSummaryThreshold)
#define nmhSetDot3OamErrFrameSecsEvNotifEnable(i4IfIndex ,i4SetValDot3OamErrFrameSecsEvNotifEnable)	\
	nmhSetCmn(Dot3OamErrFrameSecsEvNotifEnable, 11, Dot3OamErrFrameSecsEvNotifEnableSet, EoamLock, EoamUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValDot3OamErrFrameSecsEvNotifEnable)
#define nmhSetDot3OamDyingGaspEnable(i4IfIndex ,i4SetValDot3OamDyingGaspEnable)	\
	nmhSetCmn(Dot3OamDyingGaspEnable, 11, Dot3OamDyingGaspEnableSet, EoamLock, EoamUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValDot3OamDyingGaspEnable)
#define nmhSetDot3OamCriticalEventEnable(i4IfIndex ,i4SetValDot3OamCriticalEventEnable)	\
	nmhSetCmn(Dot3OamCriticalEventEnable, 11, Dot3OamCriticalEventEnableSet, EoamLock, EoamUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValDot3OamCriticalEventEnable)

#endif
