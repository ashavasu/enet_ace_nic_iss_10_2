/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: sd1lv2cli.h,v 1.1 2013/03/28 12:53:02 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 LldpV2Xdot1ConfigPortVlanTxEnable[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetLldpV2Xdot1ConfigPortVlanTxEnable(i4LldpV2PortConfigIfIndex , u4LldpV2PortConfigDestAddressIndex ,i4SetValLldpV2Xdot1ConfigPortVlanTxEnable)	\
	nmhSetCmn(LldpV2Xdot1ConfigPortVlanTxEnable, 13, LldpV2Xdot1ConfigPortVlanTxEnableSet, NULL, NULL, 0, 0, 2, "%i %u %i", i4LldpV2PortConfigIfIndex , u4LldpV2PortConfigDestAddressIndex ,i4SetValLldpV2Xdot1ConfigPortVlanTxEnable)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 LldpV2Xdot1ConfigVlanNameTxEnable[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetLldpV2Xdot1ConfigVlanNameTxEnable(i4LldpV2LocPortIfIndex , i4LldpV2Xdot1LocVlanId ,i4SetValLldpV2Xdot1ConfigVlanNameTxEnable)	\
	nmhSetCmn(LldpV2Xdot1ConfigVlanNameTxEnable, 13, LldpV2Xdot1ConfigVlanNameTxEnableSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4LldpV2LocPortIfIndex , i4LldpV2Xdot1LocVlanId ,i4SetValLldpV2Xdot1ConfigVlanNameTxEnable)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 LldpV2Xdot1ConfigProtoVlanTxEnable[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetLldpV2Xdot1ConfigProtoVlanTxEnable(i4LldpV2LocPortIfIndex , i4LldpV2Xdot1LocProtoVlanId ,i4SetValLldpV2Xdot1ConfigProtoVlanTxEnable)	\
	nmhSetCmn(LldpV2Xdot1ConfigProtoVlanTxEnable, 13, LldpV2Xdot1ConfigProtoVlanTxEnableSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4LldpV2LocPortIfIndex , i4LldpV2Xdot1LocProtoVlanId ,i4SetValLldpV2Xdot1ConfigProtoVlanTxEnable)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 LldpV2Xdot1ConfigProtocolTxEnable[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetLldpV2Xdot1ConfigProtocolTxEnable(i4LldpV2LocPortIfIndex , pLldpV2Xdot1LocProtocolIndex ,i4SetValLldpV2Xdot1ConfigProtocolTxEnable)	\
	nmhSetCmn(LldpV2Xdot1ConfigProtocolTxEnable, 13, LldpV2Xdot1ConfigProtocolTxEnableSet, NULL, NULL, 0, 0, 2, "%i %s %i", i4LldpV2LocPortIfIndex , pLldpV2Xdot1LocProtocolIndex ,i4SetValLldpV2Xdot1ConfigProtocolTxEnable)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 LldpV2Xdot1ConfigVidUsageDigestTxEnable[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetLldpV2Xdot1ConfigVidUsageDigestTxEnable(i4LldpV2LocPortIfIndex ,i4SetValLldpV2Xdot1ConfigVidUsageDigestTxEnable)	\
	nmhSetCmn(LldpV2Xdot1ConfigVidUsageDigestTxEnable, 13, LldpV2Xdot1ConfigVidUsageDigestTxEnableSet, NULL, NULL, 0, 0, 1, "%i %i", i4LldpV2LocPortIfIndex ,i4SetValLldpV2Xdot1ConfigVidUsageDigestTxEnable)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 LldpV2Xdot1ConfigManVidTxEnable[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetLldpV2Xdot1ConfigManVidTxEnable(i4LldpV2LocPortIfIndex ,i4SetValLldpV2Xdot1ConfigManVidTxEnable)	\
	nmhSetCmn(LldpV2Xdot1ConfigManVidTxEnable, 13, LldpV2Xdot1ConfigManVidTxEnableSet, NULL, NULL, 0, 0, 1, "%i %i", i4LldpV2LocPortIfIndex ,i4SetValLldpV2Xdot1ConfigManVidTxEnable)

#endif
