/********************************************************************
* Copyright (C) Future Sotware,1997-98,2001
*
* $Id: dhcpccli.h,v 1.14 2015/07/04 13:04:49 siva Exp $
 
* Description: This file contains macros to the CLI commands and 
* structures for the DHCP Client
 *******************************************************************/

#ifndef __DHCPCCLI_H__
#define __DHCPCCLI_H__

#include "cli.h"

/* DHCP CLI command constants. To add a new command ,a new definition needs to
 * be added to the list.
 */

enum {
    CLI_DHCPCLNT_DEBUG = 1,
    CLI_DHCPCLNT_NO_DEBUG,
    CLI_DHCPCLNT_RENEW,
    CLI_DHCPCLNT_RELEASE,
    CLI_DHCPCLNT_SHOW_STATS,
    CLI_DHCPCLNT_SHOW_ALL_OPTION,
    CLI_DHCPCLNT_ID,
    CLI_DHCPCLNT_NO_ID,
    CLI_DHCPCLNT_OPTION,
    CLI_DHCPCLNT_SHOW_CLNTID,
    CLI_DHCPCLNT_NO_FAST_ACC,
    CLI_DHCPCLNT_FAST_ACC,
    CLI_DHCPCLNT_NO_IDLE_TIMER,
    CLI_DHCPCLNT_IDLE_TIMER,
    CLI_DHCPCLNT_NO_DISC_TIMER,
    CLI_DHCPCLNT_DISC_TIMER,
    CLI_DHCPCLNT_ARP_CHECK_TIMER,
    CLI_DHCPCLNT_NO_ARP_CHECK_TIMER,
    CLI_DHCPCLNT_SHOW_FASTACCESS,
    CLI_DHCP_CLNT_CLEAR_INTERFACE_STATS,
    CLI_DHCP_CLNT_CLEAR_ALL_INTERFACE_STATS,
    CLI_DHCPCLNT_VENDOR_SPECIFIC,
    CLI_DHCPCLNT_NO_VENDOR_SPECIFIC
};


/* max no of variable inputs to cli parser */
#define DHCPC_CLI_MAX_ARGS       10
/* Constants used by debug dhcp client command */
#define DHCPC_DEBUG_DISABLE_ALL   0  
#define DHCPC_DEBUG_ALL           0x0000ffff
#define DHCPC_DEBUG_EVENT         0x00000001
#define DHCPC_DEBUG_PACKETS       0x00000008
#define DHCPC_DEBUG_ERRORS        0x00000040
#define DHCPC_DEBUG_BIND          0x00000010

#define DHCPCLNT_MAX_CONF_SIZE     10 * SYS_MAX_INTERFACES 
#define DHCPCLNT_MAX_STATS         25 * SYS_MAX_INTERFACES

#define DHCPCLNT_CLI_CONF_SIZE     ( CLI_MAX_HEADER_SIZE + \
                                     CLI_MAX_COLUMN_WIDTH * \
                                     DHCPCLNT_MAX_CONF_SIZE )

#define DHCPCLNT_MAX_STATS_SIZE    ( CLI_MAX_HEADER_SIZE + \
                                     CLI_MAX_COLUMN_WIDTH * \
                                     DHCPCLNT_MAX_STATS )


/* Error codes 
 * Error code values and 
 * Error strings are maintained inside the protocol module itself.
 */
enum {
    CLI_DHCPC_INV_TRC = 1,
    CLI_DHCPC_INV_STATUS,
    CLI_DHCPC_INV_RELEASE_STATE,
    CLI_DHCPC_INV_RENEW_STATE,
    CLI_DHCPC_IDLE_FAST_ACCESS_DISABLE_ERR,
    CLI_DHCPC_DISCOVER_FAST_ACCESS_DISABLE_ERR,
    CLI_DHCPC_ARP_CHECK_FAST_ACCESS_DISABLE_ERR,
    CLI_DHCPC_DIS_STATUS_ERR,
    CLI_DHCPC_MAX_ERR 
};

/* The error strings should be places under the switch so as to avoid redifinition
 * This will be visible only in modulecli.c file
 */

#ifdef __DHCPCCLI_C__

CONST CHR1  *DHCPCCliErrString [] = {
    NULL,
    "\r% Invalid Trace Level\r\n",
    "\r% Invalid status option\r\n",
    "\r% Cannot release since DHCP IP Address is not bounded\r\n",
    "\r% Cannot renew since DHCP IP Address is not bounded\r\n",
    "\r% DHCP Client Idle Timer value Cannot be changed when Fast Access is Disabled\r\n",
    "\r% DHCP Client Discovery Timer value Cannot be changed when Fast Access is Disabled\r\n",
    "\r% DHCP Client Arp Check Timer value Cannot be changed when Fast Access is Disabled\r\n",
    "\r%% Invalid Interface: DHCP Client is not enabled"
};

#endif


   
typedef union {
 UINT4 u4IfIndex;
 INT4  i4TraceLevel;
} tDhcpCliInputParams;


INT4 
cli_process_dhcpc_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Comamnd, ...));

#endif /* __DHCPCCLI_H__ */
