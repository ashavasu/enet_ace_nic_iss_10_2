/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fspnaccli.h,v 1.6 2015/06/11 10:01:50 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDPnacSystemStatus[10];
extern UINT4 FsDPnacPeriodicSyncTime[10];
extern UINT4 FsDPnacMaxKeepAliveCount[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDPnacSystemStatus(i4SetValFsDPnacSystemStatus) \
 nmhSetCmn(FsDPnacSystemStatus, 10, FsDPnacSystemStatusSet, PnacLock, PnacUnLock, 0, 0, 0, "%i", i4SetValFsDPnacSystemStatus)
#define nmhSetFsDPnacPeriodicSyncTime(u4SetValFsDPnacPeriodicSyncTime) \
 nmhSetCmn(FsDPnacPeriodicSyncTime, 10, FsDPnacPeriodicSyncTimeSet, PnacLock, PnacUnLock, 0, 0, 0, "%u", u4SetValFsDPnacPeriodicSyncTime)
#define nmhSetFsDPnacMaxKeepAliveCount(i4SetValFsDPnacMaxKeepAliveCount) \
 nmhSetCmn(FsDPnacMaxKeepAliveCount, 10, FsDPnacMaxKeepAliveCountSet, PnacLock, PnacUnLock, 0, 0, 0, "%i", i4SetValFsDPnacMaxKeepAliveCount)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPnacSystemControl[10];
extern UINT4 FsPnacTraceOption[10];
extern UINT4 FsPnacAuthenticServer[10];
extern UINT4 FsPnacNasId[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPnacSystemControl(i4SetValFsPnacSystemControl) \
 nmhSetCmn(FsPnacSystemControl, 10, FsPnacSystemControlSet, PnacLock, PnacUnLock, 0, 0, 0, "%i", i4SetValFsPnacSystemControl)
#define nmhSetFsPnacTraceOption(i4SetValFsPnacTraceOption) \
 nmhSetCmn(FsPnacTraceOption, 10, FsPnacTraceOptionSet, PnacLock, PnacUnLock, 0, 0, 0, "%i", i4SetValFsPnacTraceOption)
#define nmhSetFsPnacAuthenticServer(i4SetValFsPnacAuthenticServer) \
 nmhSetCmn(FsPnacAuthenticServer, 10, FsPnacAuthenticServerSet, PnacLock, PnacUnLock, 0, 0, 0, "%i", i4SetValFsPnacAuthenticServer)
#define nmhSetFsPnacNasId(pSetValFsPnacNasId) \
 nmhSetCmn(FsPnacNasId, 10, FsPnacNasIdSet, PnacLock, PnacUnLock, 0, 0, 0, "%s", pSetValFsPnacNasId)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPnacPaePortNumber[12];
extern UINT4 FsPnacPaePortAuthMode[12];
extern UINT4 FsPnacPaePortUserName[12];
extern UINT4 FsPnacPaePortPassword[12];
extern UINT4 FsPnacPaePortStatisticsClear[12];
extern UINT4 FsPnacPaePortAuthStatus[12];
extern UINT4 FsPnacPaeAuthReAuthMax[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPnacPaePortAuthMode(i4FsPnacPaePortNumber ,i4SetValFsPnacPaePortAuthMode) \
 nmhSetCmn(FsPnacPaePortAuthMode, 12, FsPnacPaePortAuthModeSet, PnacLock, PnacUnLock, 0, 0, 1, "%i %i", i4FsPnacPaePortNumber ,i4SetValFsPnacPaePortAuthMode)
#define nmhSetFsPnacPaePortUserName(i4FsPnacPaePortNumber ,pSetValFsPnacPaePortUserName) \
 nmhSetCmn(FsPnacPaePortUserName, 12, FsPnacPaePortUserNameSet, PnacLock, PnacUnLock, 0, 0, 1, "%i %s", i4FsPnacPaePortNumber ,pSetValFsPnacPaePortUserName)
#define nmhSetFsPnacPaePortPassword(i4FsPnacPaePortNumber ,pSetValFsPnacPaePortPassword) \
 nmhSetCmn(FsPnacPaePortPassword, 12, FsPnacPaePortPasswordSet, PnacLock, PnacUnLock, 0, 0, 1, "%i %s", i4FsPnacPaePortNumber ,pSetValFsPnacPaePortPassword)
#define nmhSetFsPnacPaePortStatisticsClear(i4FsPnacPaePortNumber ,i4SetValFsPnacPaePortStatisticsClear) \
 nmhSetCmn(FsPnacPaePortStatisticsClear, 12, FsPnacPaePortStatisticsClearSet, PnacLock, PnacUnLock, 0, 0, 1, "%i %i", i4FsPnacPaePortNumber ,i4SetValFsPnacPaePortStatisticsClear)
#define nmhSetFsPnacPaePortAuthStatus(i4FsPnacPaePortNumber ,i4SetValFsPnacPaePortAuthStatus) \
 nmhSetCmn(FsPnacPaePortAuthStatus, 12, FsPnacPaePortAuthStatusSet, PnacLock, PnacUnLock, 0, 0, 1, "%i %i", i4FsPnacPaePortNumber ,i4SetValFsPnacPaePortAuthStatus)
#define nmhSetFsPnacPaeAuthReAuthMax(i4FsPnacPaePortNumber ,u4SetValFsPnacPaeAuthReAuthMax) \
 nmhSetCmn(FsPnacPaeAuthReAuthMax, 12, FsPnacPaeAuthReAuthMaxSet, PnacLock, PnacUnLock, 0, 0, 1, "%i %u", i4FsPnacPaePortNumber ,u4SetValFsPnacPaeAuthReAuthMax)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPnacRemoteAuthServerType[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPnacRemoteAuthServerType(i4SetValFsPnacRemoteAuthServerType) \
 nmhSetCmn(FsPnacRemoteAuthServerType, 10, FsPnacRemoteAuthServerTypeSet, PnacLock, PnacUnLock, 0, 0, 0, "%i", i4SetValFsPnacRemoteAuthServerType)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPnacAuthSessionSuppAddress[12];
extern UINT4 FsPnacAuthSessionInitialize[12];
extern UINT4 FsPnacAuthSessionReauthenticate[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPnacAuthSessionInitialize(tFsPnacAuthSessionSuppAddress ,i4SetValFsPnacAuthSessionInitialize) \
 nmhSetCmn(FsPnacAuthSessionInitialize, 12, FsPnacAuthSessionInitializeSet, PnacLock, PnacUnLock, 0, 0, 1, "%m %i", tFsPnacAuthSessionSuppAddress ,i4SetValFsPnacAuthSessionInitialize)
#define nmhSetFsPnacAuthSessionReauthenticate(tFsPnacAuthSessionSuppAddress ,i4SetValFsPnacAuthSessionReauthenticate) \
 nmhSetCmn(FsPnacAuthSessionReauthenticate, 12, FsPnacAuthSessionReauthenticateSet, PnacLock, PnacUnLock, 0, 0, 1, "%m %i", tFsPnacAuthSessionSuppAddress ,i4SetValFsPnacAuthSessionReauthenticate)

#endif
/* extern declaration corresponding to OID variable present in protocol db.h */ 
extern UINT4 FsPnacAuthSessionStatisticsClear[12]; 
 
#if defined(ISS_WANTED) ||  defined(RM_WANTED) 
#define nmhSetFsPnacAuthSessionStatisticsClear(tFsPnacAuthSessionSuppAddress ,i4SetValFsPnacAuthSessionStatisticsClear) \
nmhSetCmnWithLock(FsPnacAuthSessionStatisticsClear, 12, FsPnacAuthSessionStatisticsClearSet, PnacLock, PnacUnLock, 0, 0, 1, "%m %i", tFsPnacAuthSessionSuppAddress ,i4SetValFsPnacAuthSessionStatisticsClear) 
#endif 

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPnacASUserConfigUserName[12];
extern UINT4 FsPnacASUserConfigPassword[12];
extern UINT4 FsPnacASUserConfigAuthTimeout[12];
extern UINT4 FsPnacASUserConfigPortList[12];
extern UINT4 FsPnacASUserConfigPermission[12];
extern UINT4 FsPnacASUserConfigRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPnacASUserConfigPassword(pFsPnacASUserConfigUserName ,pSetValFsPnacASUserConfigPassword) \
 nmhSetCmn(FsPnacASUserConfigPassword, 12, FsPnacASUserConfigPasswordSet, PnacLock, PnacUnLock, 0, 0, 1, "%s %s", pFsPnacASUserConfigUserName ,pSetValFsPnacASUserConfigPassword)
#define nmhSetFsPnacASUserConfigAuthTimeout(pFsPnacASUserConfigUserName ,u4SetValFsPnacASUserConfigAuthTimeout) \
 nmhSetCmn(FsPnacASUserConfigAuthTimeout, 12, FsPnacASUserConfigAuthTimeoutSet, PnacLock, PnacUnLock, 0, 0, 1, "%s %u", pFsPnacASUserConfigUserName ,u4SetValFsPnacASUserConfigAuthTimeout)
#define nmhSetFsPnacASUserConfigPortList(pFsPnacASUserConfigUserName ,pSetValFsPnacASUserConfigPortList) \
 nmhSetCmn(FsPnacASUserConfigPortList, 12, FsPnacASUserConfigPortListSet, PnacLock, PnacUnLock, 0, 0, 1, "%s %s", pFsPnacASUserConfigUserName ,pSetValFsPnacASUserConfigPortList)
#define nmhSetFsPnacASUserConfigPermission(pFsPnacASUserConfigUserName ,i4SetValFsPnacASUserConfigPermission) \
 nmhSetCmn(FsPnacASUserConfigPermission, 12, FsPnacASUserConfigPermissionSet, PnacLock, PnacUnLock, 0, 0, 1, "%s %i", pFsPnacASUserConfigUserName ,i4SetValFsPnacASUserConfigPermission)
#define nmhSetFsPnacASUserConfigRowStatus(pFsPnacASUserConfigUserName ,i4SetValFsPnacASUserConfigRowStatus) \
 nmhSetCmn(FsPnacASUserConfigRowStatus, 12, FsPnacASUserConfigRowStatusSet, PnacLock, PnacUnLock, 0, 1, 1, "%s %i", pFsPnacASUserConfigUserName ,i4SetValFsPnacASUserConfigRowStatus)

#endif
