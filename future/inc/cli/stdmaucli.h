/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdmaucli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 RpMauStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetRpMauStatus(i4RpMauGroupIndex , i4RpMauPortIndex , i4RpMauIndex ,i4SetValRpMauStatus)	\
	nmhSetCmn(RpMauStatus, 11, RpMauStatusSet, CfaLock, CfaUnlock, 0, 0, 3, "%i %i %i %i", i4RpMauGroupIndex , i4RpMauPortIndex , i4RpMauIndex ,i4SetValRpMauStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IfMauStatus[11];
extern UINT4 IfMauDefaultType[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIfMauStatus(i4IfMauIfIndex , i4IfMauIndex ,i4SetValIfMauStatus)	\
	nmhSetCmn(IfMauStatus, 11, IfMauStatusSet, CfaLock, CfaUnlock, 0, 0, 2, "%i %i %i", i4IfMauIfIndex , i4IfMauIndex ,i4SetValIfMauStatus)
#define nmhSetIfMauDefaultType(i4IfMauIfIndex , i4IfMauIndex ,pSetValIfMauDefaultType)	\
	nmhSetCmn(IfMauDefaultType, 11, IfMauDefaultTypeSet, CfaLock, CfaUnlock, 0, 0, 2, "%i %i %o", i4IfMauIfIndex , i4IfMauIndex ,pSetValIfMauDefaultType)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IfMauAutoNegAdminStatus[11];
extern UINT4 IfMauAutoNegCapAdvertised[11];
extern UINT4 IfMauAutoNegRestart[11];
extern UINT4 IfMauAutoNegCapAdvertisedBits[11];
extern UINT4 IfMauAutoNegRemoteFaultAdvertised[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIfMauAutoNegAdminStatus(i4IfMauIfIndex , i4IfMauIndex ,i4SetValIfMauAutoNegAdminStatus)	\
	nmhSetCmn(IfMauAutoNegAdminStatus, 11, IfMauAutoNegAdminStatusSet, CfaLock, CfaUnlock, 0, 0, 2, "%i %i %i", i4IfMauIfIndex , i4IfMauIndex ,i4SetValIfMauAutoNegAdminStatus)
#define nmhSetIfMauAutoNegCapAdvertised(i4IfMauIfIndex , i4IfMauIndex ,i4SetValIfMauAutoNegCapAdvertised)	\
	nmhSetCmn(IfMauAutoNegCapAdvertised, 11, IfMauAutoNegCapAdvertisedSet, CfaLock, CfaUnlock, 1, 0, 2, "%i %i %i", i4IfMauIfIndex , i4IfMauIndex ,i4SetValIfMauAutoNegCapAdvertised)
#define nmhSetIfMauAutoNegRestart(i4IfMauIfIndex , i4IfMauIndex ,i4SetValIfMauAutoNegRestart)	\
	nmhSetCmn(IfMauAutoNegRestart, 11, IfMauAutoNegRestartSet, CfaLock, CfaUnlock, 0, 0, 2, "%i %i %i", i4IfMauIfIndex , i4IfMauIndex ,i4SetValIfMauAutoNegRestart)
#define nmhSetIfMauAutoNegCapAdvertisedBits(i4IfMauIfIndex , i4IfMauIndex ,pSetValIfMauAutoNegCapAdvertisedBits)	\
	nmhSetCmn(IfMauAutoNegCapAdvertisedBits, 11, IfMauAutoNegCapAdvertisedBitsSet, CfaLock, CfaUnlock, 0, 0, 2, "%i %i %s", i4IfMauIfIndex , i4IfMauIndex ,pSetValIfMauAutoNegCapAdvertisedBits)
#define nmhSetIfMauAutoNegRemoteFaultAdvertised(i4IfMauIfIndex , i4IfMauIndex ,i4SetValIfMauAutoNegRemoteFaultAdvertised)	\
	nmhSetCmn(IfMauAutoNegRemoteFaultAdvertised, 11, IfMauAutoNegRemoteFaultAdvertisedSet, CfaLock, CfaUnlock, 0, 0, 2, "%i %i %i", i4IfMauIfIndex , i4IfMauIndex ,i4SetValIfMauAutoNegRemoteFaultAdvertised)

#endif
