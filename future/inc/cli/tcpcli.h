/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: tcpcli.h,v 1.4 2013/03/15 13:55:16 siva Exp $
 *
 **********************************************************************/
#ifndef __TCPCLI_H__
#define __TCPCLI_H__

#include "lr.h"
#include "cli.h"
/* TCP CLI command constants. To add a new command ,a new definition needs to
 * be added to the list.
 */
enum
{
    TCP_CLI_SHOW_STATS = 1,
    TCP_CLI_SHOW_CONN,
    TCP_CLI_SHOW_LIST,
    TCP_CLI_SHOW_RTO,
    TCP_CLI_CLEAR_STATS
};

#define TCP_MAX_ARGS 5
/*Prototypes of the functions used in the file tcpcli.h*/
INT1 cli_process_tcp_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);
INT4 TcpShowStats (tCliHandle CliHandle);
INT4 TcpShowConnections (tCliHandle CliHandle);
INT4 TcpShowListener (tCliHandle CliHandle);
INT4 TcpShowRetransmissionDetails (tCliHandle CliHandle);
#endif /* __TCPCLI_H__ */

#ifdef TCP_TEST_WANTED
#define TCPTST_MAX_ARGS 4 
#define MAX_RMT_CONN_REQ_PEND 5
#define MAX_STR_SIZE 80
#define ONE_KILO_BYTE 1024
#define AFINET4 4
#define AFINET6 6
#define TCPTST_FALSE TCP_FALSE
#define TCPTST_TRUE  TCP_TRUE

#define TCP_PTR_TO_U4(x) (*(UINT4*)(x)) /* For args sent as pointers */
#define TCP_PTR_TO_I4(x) (*(INT4*)(x))
enum
{
    TCP_UT_TEST=1,
    TCP_SERVER_TEST,
    TCP_CLIENT_TEST,
    TCP_FSERVER_TEST,
    TCP_FCLIENT_TEST,
    TCP_CLIENT_TEST_SEND,
};
INT4 cli_process_tcp_test_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);

#endif /* end of TCP_TEST_WANTED */

