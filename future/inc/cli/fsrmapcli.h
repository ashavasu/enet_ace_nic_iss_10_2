/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsrmapcli.h,v 1.9 2016/04/01 12:09:21 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsRouteMapName[12];
extern UINT4 FsRouteMapSeqNum[12];
extern UINT4 FsRouteMapAccess[12];
extern UINT4 FsRouteMapRowStatus[12];
#define nmhSetFsRouteMapAccess(pFsRouteMapName , u4FsRouteMapSeqNum ,i4SetValFsRouteMapAccess) \
 nmhSetCmn(FsRouteMapAccess, 12, FsRouteMapAccessSet, RMapLockWrite, RMapUnLockWrite, 1, 0, 2, "%s %u %i", pFsRouteMapName , u4FsRouteMapSeqNum ,i4SetValFsRouteMapAccess)
#define nmhSetFsRouteMapRowStatus(pFsRouteMapName , u4FsRouteMapSeqNum ,i4SetValFsRouteMapRowStatus) \
 nmhSetCmn(FsRouteMapRowStatus, 12, FsRouteMapRowStatusSet, RMapLockWrite, RMapUnLockWrite, 1, 1, 2, "%s %u %i", pFsRouteMapName , u4FsRouteMapSeqNum ,i4SetValFsRouteMapRowStatus)

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsRouteMapMatchInterface[12];
extern UINT4 FsRouteMapMatchIpAddress[12];
extern UINT4 FsRouteMapMatchIpAddrMask[12];
extern UINT4 FsRouteMapMatchIpNextHop[12];
extern UINT4 FsRouteMapMatchMetric[12];
extern UINT4 FsRouteMapMatchTag[12];
extern UINT4 FsRouteMapMatchRouteType[12];
extern UINT4 FsRouteMapMatchMetricType[12];
extern UINT4 FsRouteMapMatchASPathTag[12];
extern UINT4 FsRouteMapMatchCommunity[12];
extern UINT4 FsRouteMapMatchOrigin[12];
extern UINT4 FsRouteMapMatchLocalPreference[12];
extern UINT4 FsRouteMapMatchRowStatus[12];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsRouteMapSetInterface[12];
extern UINT4 FsRouteMapSetIpNextHop[12];
extern UINT4 FsRouteMapSetMetric[12];
extern UINT4 FsRouteMapSetTag[12];
extern UINT4 FsRouteMapSetMetricType[12];
extern UINT4 FsRouteMapSetASPathTag[12];
extern UINT4 FsRouteMapSetCommunity[12];
extern UINT4 FsRouteMapSetOrigin[12];
extern UINT4 FsRouteMapSetOriginASNum[12];
extern UINT4 FsRouteMapSetLocalPreference[12];
extern UINT4 FsRouteMapSetRowStatus[12];
#define nmhSetFsRouteMapSetInterface(pFsRouteMapName , u4FsRouteMapSeqNum ,i4SetValFsRouteMapSetInterface) \
 nmhSetCmn(FsRouteMapSetInterface, 12, FsRouteMapSetInterfaceSet, RMapLockWrite, RMapUnLockWrite, 1, 0, 2, "%s %u %i", pFsRouteMapName , u4FsRouteMapSeqNum ,i4SetValFsRouteMapSetInterface)
#define nmhSetFsRouteMapSetIpNextHop(pFsRouteMapName , u4FsRouteMapSeqNum ,u4SetValFsRouteMapSetIpNextHop) \
 nmhSetCmn(FsRouteMapSetIpNextHop, 12, FsRouteMapSetIpNextHopSet, RMapLockWrite, RMapUnLockWrite, 1, 0, 2, "%s %u %p", pFsRouteMapName , u4FsRouteMapSeqNum ,u4SetValFsRouteMapSetIpNextHop)
#define nmhSetFsRouteMapSetMetric(pFsRouteMapName , u4FsRouteMapSeqNum ,i4SetValFsRouteMapSetMetric) \
 nmhSetCmn(FsRouteMapSetMetric, 12, FsRouteMapSetMetricSet, RMapLockWrite, RMapUnLockWrite, 1, 0, 2, "%s %u %i", pFsRouteMapName , u4FsRouteMapSeqNum ,i4SetValFsRouteMapSetMetric)
#define nmhSetFsRouteMapSetTag(pFsRouteMapName , u4FsRouteMapSeqNum ,u4SetValFsRouteMapSetTag) \
 nmhSetCmn(FsRouteMapSetTag, 12, FsRouteMapSetTagSet, RMapLockWrite, RMapUnLockWrite, 1, 0, 2, "%s %u %u", pFsRouteMapName , u4FsRouteMapSeqNum ,u4SetValFsRouteMapSetTag)
#define nmhSetFsRouteMapSetMetricType(pFsRouteMapName , u4FsRouteMapSeqNum ,i4SetValFsRouteMapSetMetricType) \
 nmhSetCmn(FsRouteMapSetMetricType, 12, FsRouteMapSetMetricTypeSet, RMapLockWrite, RMapUnLockWrite, 1, 0, 2, "%s %u %i", pFsRouteMapName , u4FsRouteMapSeqNum ,i4SetValFsRouteMapSetMetricType)
#define nmhSetFsRouteMapSetASPathTag(pFsRouteMapName , u4FsRouteMapSeqNum ,u4SetValFsRouteMapSetASPathTag) \
 nmhSetCmn(FsRouteMapSetASPathTag, 12, FsRouteMapSetASPathTagSet, RMapLockWrite, RMapUnLockWrite, 1, 0, 2, "%s %u %u", pFsRouteMapName , u4FsRouteMapSeqNum ,u4SetValFsRouteMapSetASPathTag)
#define nmhSetFsRouteMapSetCommunity(pFsRouteMapName , u4FsRouteMapSeqNum ,u4SetValFsRouteMapSetCommunity) \
 nmhSetCmn(FsRouteMapSetCommunity, 12, FsRouteMapSetCommunitySet, RMapLockWrite, RMapUnLockWrite, 1, 0, 2, "%s %u %u", pFsRouteMapName , u4FsRouteMapSeqNum ,u4SetValFsRouteMapSetCommunity)
#define nmhSetFsRouteMapSetOrigin(pFsRouteMapName , u4FsRouteMapSeqNum ,i4SetValFsRouteMapSetOrigin) \
 nmhSetCmn(FsRouteMapSetOrigin, 12, FsRouteMapSetOriginSet, RMapLockWrite, RMapUnLockWrite, 1, 0, 2, "%s %u %i", pFsRouteMapName , u4FsRouteMapSeqNum ,i4SetValFsRouteMapSetOrigin)
#define nmhSetFsRouteMapSetOriginASNum(pFsRouteMapName , u4FsRouteMapSeqNum ,u4SetValFsRouteMapSetOriginASNum) \
 nmhSetCmn(FsRouteMapSetOriginASNum, 12, FsRouteMapSetOriginASNumSet, RMapLockWrite, RMapUnLockWrite, 1, 0, 2, "%s %u %u", pFsRouteMapName , u4FsRouteMapSeqNum ,u4SetValFsRouteMapSetOriginASNum)
#define nmhSetFsRouteMapSetLocalPreference(pFsRouteMapName , u4FsRouteMapSeqNum ,i4SetValFsRouteMapSetLocalPreference) \
 nmhSetCmn(FsRouteMapSetLocalPreference, 12, FsRouteMapSetLocalPreferenceSet, RMapLockWrite, RMapUnLockWrite, 1, 0, 2, "%s %u %i", pFsRouteMapName , u4FsRouteMapSeqNum ,i4SetValFsRouteMapSetLocalPreference)
#define nmhSetFsRouteMapSetRowStatus(pFsRouteMapName , u4FsRouteMapSeqNum ,i4SetValFsRouteMapSetRowStatus) \
 nmhSetCmn(FsRouteMapSetRowStatus, 12, FsRouteMapSetRowStatusSet, RMapLockWrite, RMapUnLockWrite, 1, 1, 2, "%s %u %i", pFsRouteMapName , u4FsRouteMapSeqNum ,i4SetValFsRouteMapSetRowStatus)

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsRMapName[12];
extern UINT4 FsRMapSeqNum[12];
extern UINT4 FsRMapAccess[12];
extern UINT4 FsRMapRowStatus[12];
extern UINT4 FsRMapIsIpPrefixList[12];


#define nmhSetFsRMapAccess(pFsRMapName , u4FsRMapSeqNum ,i4SetValFsRMapAccess) \
 nmhSetCmn(FsRMapAccess, 12, FsRMapAccessSet, RMapLockWrite, RMapUnLockWrite, 0, 0, 2, "%s %u %i", pFsRMapName , u4FsRMapSeqNum ,i4SetValFsRMapAccess)
#define nmhSetFsRMapRowStatus(pFsRMapName , u4FsRMapSeqNum ,i4SetValFsRMapRowStatus) \
 nmhSetCmn(FsRMapRowStatus, 12, FsRMapRowStatusSet, RMapLockWrite, RMapUnLockWrite, 0, 1, 2, "%s %u %i", pFsRMapName , u4FsRMapSeqNum ,i4SetValFsRMapRowStatus)
#define nmhSetFsRMapIsIpPrefixList(pFsRMapName , u4FsRMapSeqNum ,i4SetValFsRMapIsIpPrefixList) \
 nmhSetCmn(FsRMapIsIpPrefixList, 12, FsRMapIsIpPrefixListSet, RMapLockWrite, RMapUnLockWrite, 0, 0, 2, "%s %u %i", pFsRMapName , u4FsRMapSeqNum ,i4SetValFsRMapIsIpPrefixList)

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsRMapMatchDestInetType[12];
extern UINT4 FsRMapMatchDestInetAddress[12];
extern UINT4 FsRMapMatchDestInetPrefix[12];
extern UINT4 FsRMapMatchSourceInetType[12];
extern UINT4 FsRMapMatchSourceInetAddress[12];
extern UINT4 FsRMapMatchSourceInetPrefix[12];
extern UINT4 FsRMapMatchNextHopInetType[12];
extern UINT4 FsRMapMatchNextHopInetAddr[12];
extern UINT4 FsRMapMatchInterface[12];
extern UINT4 FsRMapMatchMetric[12];
extern UINT4 FsRMapMatchTag[12];
extern UINT4 FsRMapMatchMetricType[12];
extern UINT4 FsRMapMatchRouteType[12];
extern UINT4 FsRMapMatchASPathTag[12];
extern UINT4 FsRMapMatchCommunity[12];
extern UINT4 FsRMapMatchLocalPref[12];
extern UINT4 FsRMapMatchOrigin[12];
extern UINT4 FsRMapMatchRowStatus[12];
extern UINT4 FsRMapMatchDestMaxPrefixLen[12];
extern UINT4 FsRMapMatchDestMinPrefixLen[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsRMapSetNextHopInetType[12];
extern UINT4 FsRMapSetNextHopInetAddr[12];
extern UINT4 FsRMapSetInterface[12];
extern UINT4 FsRMapSetMetric[12];
extern UINT4 FsRMapSetTag[12];
extern UINT4 FsRMapSetRouteType[12];
extern UINT4 FsRMapSetASPathTag[12];
extern UINT4 FsRMapSetCommunity[12];
extern UINT4 FsRMapSetLocalPref[12];
extern UINT4 FsRMapSetOrigin[12];
extern UINT4 FsRMapSetWeight[12];
extern UINT4 FsRMapSetEnableAutoTag[12];
extern UINT4 FsRMapSetLevel[12];
extern UINT4 FsRMapSetRowStatus[12];
extern UINT4 FsRMapSetExtCommId[12];
extern UINT4 FsRMapSetExtCommCost[12];
extern UINT4 FsRMapSetCommunityAdditive[12];

#define nmhSetFsRMapSetNextHopInetType(pFsRMapName , u4FsRMapSeqNum ,i4SetValFsRMapSetNextHopInetType) \
 nmhSetCmn(FsRMapSetNextHopInetType, 12, FsRMapSetNextHopInetTypeSet, RMapLockWrite, RMapUnLockWrite, 0, 0, 2, "%s %u %i", pFsRMapName , u4FsRMapSeqNum ,i4SetValFsRMapSetNextHopInetType)
#define nmhSetFsRMapSetNextHopInetAddr(pFsRMapName , u4FsRMapSeqNum ,pSetValFsRMapSetNextHopInetAddr) \
 nmhSetCmn(FsRMapSetNextHopInetAddr, 12, FsRMapSetNextHopInetAddrSet, RMapLockWrite, RMapUnLockWrite, 0, 0, 2, "%s %u %s", pFsRMapName , u4FsRMapSeqNum ,pSetValFsRMapSetNextHopInetAddr)
#define nmhSetFsRMapSetInterface(pFsRMapName , u4FsRMapSeqNum ,i4SetValFsRMapSetInterface) \
 nmhSetCmn(FsRMapSetInterface, 12, FsRMapSetInterfaceSet, RMapLockWrite, RMapUnLockWrite, 0, 0, 2, "%s %u %i", pFsRMapName , u4FsRMapSeqNum ,i4SetValFsRMapSetInterface)
#define nmhSetFsRMapSetMetric(pFsRMapName , u4FsRMapSeqNum ,i4SetValFsRMapSetMetric) \
 nmhSetCmn(FsRMapSetMetric, 12, FsRMapSetMetricSet, RMapLockWrite, RMapUnLockWrite, 0, 0, 2, "%s %u %i", pFsRMapName , u4FsRMapSeqNum ,i4SetValFsRMapSetMetric)
#define nmhSetFsRMapSetTag(pFsRMapName , u4FsRMapSeqNum ,u4SetValFsRMapSetTag) \
 nmhSetCmn(FsRMapSetTag, 12, FsRMapSetTagSet, RMapLockWrite, RMapUnLockWrite, 0, 0, 2, "%s %u %u", pFsRMapName , u4FsRMapSeqNum ,u4SetValFsRMapSetTag)
#define nmhSetFsRMapSetRouteType(pFsRMapName , u4FsRMapSeqNum ,i4SetValFsRMapSetRouteType) \
 nmhSetCmn(FsRMapSetRouteType, 12, FsRMapSetRouteTypeSet, RMapLockWrite, RMapUnLockWrite, 0, 0, 2, "%s %u %i", pFsRMapName , u4FsRMapSeqNum ,i4SetValFsRMapSetRouteType)
#define nmhSetFsRMapSetASPathTag(pFsRMapName , u4FsRMapSeqNum ,u4SetValFsRMapSetASPathTag) \
 nmhSetCmn(FsRMapSetASPathTag, 12, FsRMapSetASPathTagSet, RMapLockWrite, RMapUnLockWrite, 0, 0, 2, "%s %u %u", pFsRMapName , u4FsRMapSeqNum ,u4SetValFsRMapSetASPathTag)
#define nmhSetFsRMapSetCommunity(pFsRMapName , u4FsRMapSeqNum ,pSetValFsRMapSetCommunity) \
 nmhSetCmn(FsRMapSetCommunity, 12, FsRMapSetCommunitySet, RMapLockWrite, RMapUnLockWrite, 0, 0, 2, "%s %u %s", pFsRMapName , u4FsRMapSeqNum ,pSetValFsRMapSetCommunity)
#define nmhSetFsRMapSetLocalPref(pFsRMapName , u4FsRMapSeqNum ,i4SetValFsRMapSetLocalPref) \
 nmhSetCmn(FsRMapSetLocalPref, 12, FsRMapSetLocalPrefSet, RMapLockWrite, RMapUnLockWrite, 0, 0, 2, "%s %u %i", pFsRMapName , u4FsRMapSeqNum ,i4SetValFsRMapSetLocalPref)
#define nmhSetFsRMapSetOrigin(pFsRMapName , u4FsRMapSeqNum ,i4SetValFsRMapSetOrigin) \
 nmhSetCmn(FsRMapSetOrigin, 12, FsRMapSetOriginSet, RMapLockWrite, RMapUnLockWrite, 0, 0, 2, "%s %u %i", pFsRMapName , u4FsRMapSeqNum ,i4SetValFsRMapSetOrigin)
#define nmhSetFsRMapSetWeight(pFsRMapName , u4FsRMapSeqNum ,u4SetValFsRMapSetWeight) \
 nmhSetCmn(FsRMapSetWeight, 12, FsRMapSetWeightSet, RMapLockWrite, RMapUnLockWrite, 0, 0, 2, "%s %u %u", pFsRMapName , u4FsRMapSeqNum ,u4SetValFsRMapSetWeight)
#define nmhSetFsRMapSetEnableAutoTag(pFsRMapName , u4FsRMapSeqNum ,i4SetValFsRMapSetEnableAutoTag) \
 nmhSetCmn(FsRMapSetEnableAutoTag, 12, FsRMapSetEnableAutoTagSet, RMapLockWrite, RMapUnLockWrite, 0, 0, 2, "%s %u %i", pFsRMapName , u4FsRMapSeqNum ,i4SetValFsRMapSetEnableAutoTag)
#define nmhSetFsRMapSetLevel(pFsRMapName , u4FsRMapSeqNum ,i4SetValFsRMapSetLevel) \
 nmhSetCmn(FsRMapSetLevel, 12, FsRMapSetLevelSet, RMapLockWrite, RMapUnLockWrite, 0, 0, 2, "%s %u %i", pFsRMapName , u4FsRMapSeqNum ,i4SetValFsRMapSetLevel)
#define nmhSetFsRMapSetRowStatus(pFsRMapName , u4FsRMapSeqNum ,i4SetValFsRMapSetRowStatus) \
 nmhSetCmn(FsRMapSetRowStatus, 12, FsRMapSetRowStatusSet, RMapLockWrite, RMapUnLockWrite, 0, 1, 2, "%s %u %i", pFsRMapName , u4FsRMapSeqNum ,i4SetValFsRMapSetRowStatus)
#define nmhSetFsRMapSetExtCommId(pFsRMapName , u4FsRMapSeqNum ,u4SetValFsRMapSetExtCommId) \
 nmhSetCmn(FsRMapSetExtCommId, 12, FsRMapSetExtCommIdSet, RMapLockWrite, RMapUnLockWrite, 0, 0, 2, "%s %u %u", pFsRMapName , u4FsRMapSeqNum ,u4SetValFsRMapSetExtCommId)
#define nmhSetFsRMapSetExtCommCost(pFsRMapName , u4FsRMapSeqNum ,u4SetValFsRMapSetExtCommCost) \
 nmhSetCmn(FsRMapSetExtCommCost, 12, FsRMapSetExtCommCostSet, RMapLockWrite, RMapUnLockWrite, 0, 0, 2, "%s %u %u", pFsRMapName , u4FsRMapSeqNum ,u4SetValFsRMapSetExtCommCost)
#define nmhSetFsRMapSetCommunityAdditive(pFsRMapName , u4FsRMapSeqNum ,i4SetValFsRMapSetCommunityAdditive) \
 nmhSetCmn(FsRMapSetCommunityAdditive, 12, FsRMapSetCommunityAdditiveSet, RMapLockWrite, RMapUnLockWrite, 0, 0, 2, "%s %u %i", pFsRMapName , u4FsRMapSeqNum ,i4SetValFsRMapSetCommunityAdditive)
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsRmapTrapCfgEnable[10];

#define nmhSetFsRmapTrapCfgEnable(i4SetValFsRmapTrapCfgEnable) \
 nmhSetCmn(FsRmapTrapCfgEnable, 10, FsRmapTrapCfgEnableSet, RMapLockWrite, RMapUnLockWrite, 0, 0, 0, "%i", i4SetValFsRmapTrapCfgEnable)
