/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmsvlcli.h,v 1.2 2008/10/31 13:09:30 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDot1qVlanContextId[14];
extern UINT4 FsDot1qGvrpStatus[14];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDot1qForwardAllRowStatus[14];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDot1qForwardAllPort[14];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDot1qForwardUnregRowStatus[14];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDot1qForwardUnregPort[14];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDot1qStaticUnicastAddress[14];
extern UINT4 FsDot1qStaticUnicastReceivePort[14];
extern UINT4 FsDot1qStaticUnicastRowStatus[14];
extern UINT4 FsDot1qStaticUnicastStatus[14];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDot1qStaticAllowedIsMember[14];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDot1qStaticMulticastAddress[14];
extern UINT4 FsDot1qStaticMulticastReceivePort[14];
extern UINT4 FsDot1qStaticMulticastRowStatus[14];
extern UINT4 FsDot1qStaticMulticastStatus[14];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDot1qStaticMcastPort[14];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDot1qVlanStaticName[14];
extern UINT4 FsDot1qVlanStaticRowStatus[14];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDot1qVlanStaticPort[14];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDot1qPvid[14];
extern UINT4 FsDot1qPortAcceptableFrameTypes[14];
extern UINT4 FsDot1qPortIngressFiltering[14];
extern UINT4 FsDot1qPortGvrpStatus[14];
extern UINT4 FsDot1qPortRestrictedVlanRegistration[14];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDot1qConstraintVlan[14];
extern UINT4 FsDot1qConstraintSet[14];
extern UINT4 FsDot1qConstraintType[14];
extern UINT4 FsDot1qConstraintStatus[14];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDot1qConstraintSetDefault[14];
extern UINT4 FsDot1qConstraintTypeDefault[14];
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDot1vProtocolTemplateFrameType[14];
extern UINT4 FsDot1vProtocolTemplateProtocolValue[14];
extern UINT4 FsDot1vProtocolGroupId[14];
extern UINT4 FsDot1vProtocolGroupRowStatus[14];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDot1vProtocolPortGroupId[14];
extern UINT4 FsDot1vProtocolPortGroupVid[14];
extern UINT4 FsDot1vProtocolPortRowStatus[14];

