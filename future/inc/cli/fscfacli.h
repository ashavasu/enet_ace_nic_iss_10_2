/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fscfacli.h,v 1.19 2016/10/03 10:34:42 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IfMaxInterfaces[10];
extern UINT4 IfMaxPhysInterfaces[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIfMaxInterfaces(i4SetValIfMaxInterfaces) \
 nmhSetCmn(IfMaxInterfaces, 10, IfMaxInterfacesSet, CfaLock, CfaUnlock, 1, 0, 0, "%i", i4SetValIfMaxInterfaces)
#define nmhSetIfMaxPhysInterfaces(i4SetValIfMaxPhysInterfaces) \
 nmhSetCmn(IfMaxPhysInterfaces, 10, IfMaxPhysInterfacesSet, CfaLock, CfaUnlock, 1, 0, 0, "%i", i4SetValIfMaxPhysInterfaces)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IfMainIndex[12];
extern UINT4 IfMainType[12];
extern UINT4 IfMainMtu[12];
extern UINT4 IfMainAdminStatus[12];
extern UINT4 IfMainEncapType[12];
extern UINT4 IfMainBrgPortType[12];
extern UINT4 IfMainRowStatus[12];
extern UINT4 IfMainSubType[12];
extern UINT4 IfMainNetworkType[12];
extern UINT4 IfMainWanType[12];
extern UINT4 IfMainDesc[12];
extern UINT4 IfMainStorageType[12];
extern UINT4 IfMainExtSubType[12];
extern UINT4 IfMainPortRole[12];
extern UINT4 IfMainUfdGroupId[12];
extern UINT4 IfMainDesigUplinkStatus[12];
extern UINT4 IfMainEncapDot1qVlanId[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIfMainType(i4IfMainIndex ,i4SetValIfMainType) \
 nmhSetCmn(IfMainType, 12, IfMainTypeSet, CfaLock, CfaUnlock, 0, 0, 1, "%i %i", i4IfMainIndex ,i4SetValIfMainType)
#define nmhSetIfMainMtu(i4IfMainIndex ,i4SetValIfMainMtu) \
 nmhSetCmn(IfMainMtu, 12, IfMainMtuSet, CfaLock, CfaUnlock, 0, 0, 1, "%i %i", i4IfMainIndex ,i4SetValIfMainMtu)
#define nmhSetIfMainAdminStatus(i4IfMainIndex ,i4SetValIfMainAdminStatus) \
 nmhSetCmn(IfMainAdminStatus, 12, IfMainAdminStatusSet, CfaLock, CfaUnlock, 0, 0, 1, "%i %i", i4IfMainIndex ,i4SetValIfMainAdminStatus)
#define nmhSetIfMainEncapType(i4IfMainIndex ,i4SetValIfMainEncapType) \
 nmhSetCmn(IfMainEncapType, 12, IfMainEncapTypeSet, CfaLock, CfaUnlock, 0, 0, 1, "%i %i", i4IfMainIndex ,i4SetValIfMainEncapType)
#define nmhSetIfMainBrgPortType(i4IfMainIndex ,i4SetValIfMainBrgPortType) \
 nmhSetCmn(IfMainBrgPortType, 12, IfMainBrgPortTypeSet, CfaLock, CfaUnlock, 0, 0, 1, "%i %i", i4IfMainIndex ,i4SetValIfMainBrgPortType)
#define nmhSetIfMainRowStatus(i4IfMainIndex ,i4SetValIfMainRowStatus) \
 nmhSetCmn(IfMainRowStatus, 12, IfMainRowStatusSet, CfaLock, CfaUnlock, 0, 1, 1, "%i %i", i4IfMainIndex ,i4SetValIfMainRowStatus)
#define nmhSetIfMainSubType(i4IfMainIndex ,i4SetValIfMainSubType) \
 nmhSetCmn(IfMainSubType, 12, IfMainSubTypeSet, CfaLock, CfaUnlock, 0, 0, 1, "%i %i", i4IfMainIndex ,i4SetValIfMainSubType)
#define nmhSetIfMainWanType(i4IfMainIndex ,i4SetValIfMainWanType) \
 nmhSetCmn(IfMainWanType, 12, IfMainWanTypeSet, CfaLock, CfaUnlock, 0, 0, 1, "%i %i", i4IfMainIndex ,i4SetValIfMainWanType)
#define nmhSetIfMainNetworkType(i4IfMainIndex ,i4SetValIfMainNetworkType) \
 nmhSetCmn(IfMainNetworkType, 12, IfMainNetworkTypeSet, CfaLock, CfaUnlock, 0, 0, 1, "%i %i", i4IfMainIndex ,i4SetValIfMainNetworkType)
#define nmhSetIfMainDesc(i4IfMainIndex ,pSetValIfMainDesc) \
 nmhSetCmn(IfMainDesc, 12, IfMainDescSet, CfaLock, CfaUnlock, 0, 0, 1, "%i %s", i4IfMainIndex ,pSetValIfMainDesc)
#define nmhSetIfMainStorageType(i4IfMainIndex ,i4SetValIfMainStorageType) \
 nmhSetCmn(IfMainStorageType, 12, IfMainStorageTypeSet, CfaLock, CfaUnlock, 0, 0, 1, "%i %i", i4IfMainIndex ,i4SetValIfMainStorageType)
#define nmhSetIfMainExtSubType(i4IfMainIndex ,i4SetValIfMainExtSubType) \
 nmhSetCmn(IfMainExtSubType, 12, IfMainExtSubTypeSet, CfaLock, CfaUnlock, 0, 0, 1, "%i %i", i4IfMainIndex ,i4SetValIfMainExtSubType)
#define nmhSetIfMainPortRole(i4IfMainIndex ,i4SetValIfMainPortRole) \
    nmhSetCmn(IfMainPortRole, 12, IfMainPortRoleSet, NULL, NULL, 0, 0, 1, "%i %i", i4IfMainIndex ,i4SetValIfMainPortRole)
#define nmhSetIfMainUfdGroupId(i4IfMainIndex ,i4SetValIfMainUfdGroupId) \
        nmhSetCmn(IfMainUfdGroupId, 12, IfMainUfdGroupIdSet, NULL, NULL, 0, 0, 1, "%i %i", i4IfMainIndex ,i4SetValIfMainUfdGroupId)
#define nmhSetIfMainDesigUplinkStatus(i4IfMainIndex ,i4SetValIfMainDesigUplinkStatus)   \
    nmhSetCmn(IfMainDesigUplinkStatus, 12, IfMainDesigUplinkStatusSet, NULL, NULL, 0, 0, 1, "%i %i", i4IfMainIndex ,i4SetValIfMainDesigUplinkStatus)
#define nmhSetIfMainEncapDot1qVlanId(i4IfMainIndex ,i4SetValIfMainEncapDot1qVlanId) \
    nmhSetCmnWithLock(IfMainEncapDot1qVlanId, 12, IfMainEncapDot1qVlanIdSet, CfaLock, CfaUnlock, 0, 0, 1, "%i %i", i4IfMainIndex ,i4SetValIfMainEncapDot1qVlanId)



#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IfIpAddrAllocMethod[12];
extern UINT4 IfIpAddr[12];
extern UINT4 IfIpSubnetMask[12];
extern UINT4 IfIpBroadcastAddr[12];
extern UINT4 IfIpForwardingEnable[12];
extern UINT4 IfIpAddrAllocProtocol[12];
extern UINT4 IfIpDestMacAddress[12];
extern UINT4 IfIpUnnumAssocIPIf[12];
extern UINT4 IfIpIntfStatsEnable[12];
extern UINT4 IfIpPortVlanId[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIfIpAddrAllocMethod(i4IfMainIndex ,i4SetValIfIpAddrAllocMethod) \
 nmhSetCmn(IfIpAddrAllocMethod, 12, IfIpAddrAllocMethodSet, CfaLock, CfaUnlock, 0, 0, 1, "%i %i", i4IfMainIndex ,i4SetValIfIpAddrAllocMethod)
#define nmhSetIfIpAddr(i4IfMainIndex ,u4SetValIfIpAddr) \
 nmhSetCmn(IfIpAddr, 12, IfIpAddrSet, CfaLock, CfaUnlock, 0, 0, 1, "%i %p", i4IfMainIndex ,u4SetValIfIpAddr)
#define nmhSetIfIpSubnetMask(i4IfMainIndex ,u4SetValIfIpSubnetMask) \
 nmhSetCmn(IfIpSubnetMask, 12, IfIpSubnetMaskSet, CfaLock, CfaUnlock, 0, 0, 1, "%i %p", i4IfMainIndex ,u4SetValIfIpSubnetMask)
#define nmhSetIfIpBroadcastAddr(i4IfMainIndex ,u4SetValIfIpBroadcastAddr) \
 nmhSetCmn(IfIpBroadcastAddr, 12, IfIpBroadcastAddrSet, CfaLock, CfaUnlock, 0, 0, 1, "%i %p", i4IfMainIndex ,u4SetValIfIpBroadcastAddr)
#define nmhSetIfIpForwardingEnable(i4IfMainIndex ,i4SetValIfIpForwardingEnable) \
 nmhSetCmn(IfIpForwardingEnable, 12, IfIpForwardingEnableSet, CfaLock, CfaUnlock, 0, 0, 1, "%i %i", i4IfMainIndex ,i4SetValIfIpForwardingEnable)
#define nmhSetIfIpAddrAllocProtocol(i4IfMainIndex ,i4SetValIfIpAddrAllocProtocol) \
 nmhSetCmn(IfIpAddrAllocProtocol, 12, IfIpAddrAllocProtocolSet, CfaLock, CfaUnlock, 0, 0, 1, "%i %i", i4IfMainIndex ,i4SetValIfIpAddrAllocProtocol)
#define nmhSetIfIpDestMacAddress(i4IfMainIndex ,SetValIfIpDestMacAddress) \
 nmhSetCmn(IfIpDestMacAddress, 12, IfIpDestMacAddressSet, CfaLock, CfaUnlock, 0, 0, 1, "%i %m", i4IfMainIndex ,SetValIfIpDestMacAddress)
#define nmhSetIfIpUnnumAssocIPIf(i4IfMainIndex ,i4SetValIfIpUnnumAssocIPIf) \
 nmhSetCmn(IfIpUnnumAssocIPIf, 12, IfIpUnnumAssocIPIfSet, CfaLock, CfaUnlock, 0, 0, 1, "%i %i", i4IfMainIndex ,i4SetValIfIpUnnumAssocIPIf)
#define nmhSetIfIpIntfStatsEnable(i4IfMainIndex ,i4SetValIfIpIntfStatsEnable)   \
        nmhSetCmn(IfIpIntfStatsEnable, 12, IfIpIntfStatsEnableSet, CfaLock, CfaUnlock, 0, 0, 1, "%i %i", i4IfMainIndex ,i4SetValIfIpIntfStatsEnable)
#define nmhSetIfIpPortVlanId(i4IfMainIndex ,i4SetValIfIpPortVlanId) \
 nmhSetCmn(IfIpPortVlanId , 12, IfIpPortVlanIdSet, CfaLock, CfaUnlock, 0, 0, 1, "%i %i", i4IfMainIndex ,i4SetValIfIpPortVlanId)
#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IfWanConnectionType[12];
extern UINT4 IfWanVirtualPathId[12];
extern UINT4 IfWanVirtualCircuitId[12];
extern UINT4 IfWanPeerMediaAddress[12];
extern UINT4 IfWanSustainedSpeed[12];
extern UINT4 IfWanPeakSpeed[12];
extern UINT4 IfWanMaxBurstSize[12];
extern UINT4 IfWanIpQosProfileIndex[12];
extern UINT4 IfWanIdleTimeout[12];
extern UINT4 IfWanPeerIpAddr[12];
extern UINT4 IfWanRtpHdrComprEnable[12];
extern UINT4 IfWanPersistence[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIfWanConnectionType(i4IfMainIndex ,i4SetValIfWanConnectionType) \
 nmhSetCmn(IfWanConnectionType, 12, IfWanConnectionTypeSet, CfaLock, CfaUnlock, 1, 0, 1, "%i %i", i4IfMainIndex ,i4SetValIfWanConnectionType)
#define nmhSetIfWanVirtualPathId(i4IfMainIndex ,i4SetValIfWanVirtualPathId) \
 nmhSetCmn(IfWanVirtualPathId, 12, IfWanVirtualPathIdSet, CfaLock, CfaUnlock, 1, 0, 1, "%i %i", i4IfMainIndex ,i4SetValIfWanVirtualPathId)
#define nmhSetIfWanVirtualCircuitId(i4IfMainIndex ,i4SetValIfWanVirtualCircuitId) \
 nmhSetCmn(IfWanVirtualCircuitId, 12, IfWanVirtualCircuitIdSet, CfaLock, CfaUnlock, 1, 0, 1, "%i %i", i4IfMainIndex ,i4SetValIfWanVirtualCircuitId)
#define nmhSetIfWanPeerMediaAddress(i4IfMainIndex ,pSetValIfWanPeerMediaAddress) \
 nmhSetCmn(IfWanPeerMediaAddress, 12, IfWanPeerMediaAddressSet, CfaLock, CfaUnlock, 1, 0, 1, "%i %s", i4IfMainIndex ,pSetValIfWanPeerMediaAddress)
#define nmhSetIfWanSustainedSpeed(i4IfMainIndex ,i4SetValIfWanSustainedSpeed) \
 nmhSetCmn(IfWanSustainedSpeed, 12, IfWanSustainedSpeedSet, CfaLock, CfaUnlock, 1, 0, 1, "%i %i", i4IfMainIndex ,i4SetValIfWanSustainedSpeed)
#define nmhSetIfWanPeakSpeed(i4IfMainIndex ,i4SetValIfWanPeakSpeed) \
 nmhSetCmn(IfWanPeakSpeed, 12, IfWanPeakSpeedSet, CfaLock, CfaUnlock, 1, 0, 1, "%i %i", i4IfMainIndex ,i4SetValIfWanPeakSpeed)
#define nmhSetIfWanMaxBurstSize(i4IfMainIndex ,i4SetValIfWanMaxBurstSize) \
 nmhSetCmn(IfWanMaxBurstSize, 12, IfWanMaxBurstSizeSet, CfaLock, CfaUnlock, 1, 0, 1, "%i %i", i4IfMainIndex ,i4SetValIfWanMaxBurstSize)
#define nmhSetIfWanIpQosProfileIndex(i4IfMainIndex ,i4SetValIfWanIpQosProfileIndex) \
 nmhSetCmn(IfWanIpQosProfileIndex, 12, IfWanIpQosProfileIndexSet, CfaLock, CfaUnlock, 1, 0, 1, "%i %i", i4IfMainIndex ,i4SetValIfWanIpQosProfileIndex)
#define nmhSetIfWanIdleTimeout(i4IfMainIndex ,i4SetValIfWanIdleTimeout) \
 nmhSetCmn(IfWanIdleTimeout, 12, IfWanIdleTimeoutSet, CfaLock, CfaUnlock, 1, 0, 1, "%i %i", i4IfMainIndex ,i4SetValIfWanIdleTimeout)
#define nmhSetIfWanPeerIpAddr(i4IfMainIndex ,u4SetValIfWanPeerIpAddr) \
 nmhSetCmn(IfWanPeerIpAddr, 12, IfWanPeerIpAddrSet, CfaLock, CfaUnlock, 1, 0, 1, "%i %p", i4IfMainIndex ,u4SetValIfWanPeerIpAddr)
#define nmhSetIfWanRtpHdrComprEnable(i4IfMainIndex ,i4SetValIfWanRtpHdrComprEnable) \
 nmhSetCmn(IfWanRtpHdrComprEnable, 12, IfWanRtpHdrComprEnableSet, CfaLock, CfaUnlock, 1, 0, 1, "%i %i", i4IfMainIndex ,i4SetValIfWanRtpHdrComprEnable)
#define nmhSetIfWanPersistence(i4IfMainIndex ,i4SetValIfWanPersistence) \
 nmhSetCmn(IfWanPersistence, 12, IfWanPersistenceSet, CfaLock, CfaUnlock, 1, 0, 1, "%i %i", i4IfMainIndex ,i4SetValIfWanPersistence)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IfAutoProfileIfIndex[12];
extern UINT4 IfAutoProfileIpAddrAllocMethod[12];
extern UINT4 IfAutoProfileDefIpSubnetMask[12];
extern UINT4 IfAutoProfileDefIpBroadcastAddr[12];
extern UINT4 IfAutoProfileIdleTimeout[12];
extern UINT4 IfAutoProfileEncapType[12];
extern UINT4 IfAutoProfileIpQosProfileIndex[12];
extern UINT4 IfAutoProfileRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIfAutoProfileIpAddrAllocMethod(i4IfAutoProfileIfIndex ,i4SetValIfAutoProfileIpAddrAllocMethod) \
 nmhSetCmn(IfAutoProfileIpAddrAllocMethod, 12, IfAutoProfileIpAddrAllocMethodSet, CfaLock, CfaUnlock, 1, 0, 1, "%i %i", i4IfAutoProfileIfIndex ,i4SetValIfAutoProfileIpAddrAllocMethod)
#define nmhSetIfAutoProfileDefIpSubnetMask(i4IfAutoProfileIfIndex ,u4SetValIfAutoProfileDefIpSubnetMask) \
 nmhSetCmn(IfAutoProfileDefIpSubnetMask, 12, IfAutoProfileDefIpSubnetMaskSet, CfaLock, CfaUnlock, 1, 0, 1, "%i %p", i4IfAutoProfileIfIndex ,u4SetValIfAutoProfileDefIpSubnetMask)
#define nmhSetIfAutoProfileDefIpBroadcastAddr(i4IfAutoProfileIfIndex ,u4SetValIfAutoProfileDefIpBroadcastAddr) \
 nmhSetCmn(IfAutoProfileDefIpBroadcastAddr, 12, IfAutoProfileDefIpBroadcastAddrSet, CfaLock, CfaUnlock, 1, 0, 1, "%i %p", i4IfAutoProfileIfIndex ,u4SetValIfAutoProfileDefIpBroadcastAddr)
#define nmhSetIfAutoProfileIdleTimeout(i4IfAutoProfileIfIndex ,i4SetValIfAutoProfileIdleTimeout) \
 nmhSetCmn(IfAutoProfileIdleTimeout, 12, IfAutoProfileIdleTimeoutSet, CfaLock, CfaUnlock, 1, 0, 1, "%i %i", i4IfAutoProfileIfIndex ,i4SetValIfAutoProfileIdleTimeout)
#define nmhSetIfAutoProfileEncapType(i4IfAutoProfileIfIndex ,i4SetValIfAutoProfileEncapType) \
 nmhSetCmn(IfAutoProfileEncapType, 12, IfAutoProfileEncapTypeSet, CfaLock, CfaUnlock, 1, 0, 1, "%i %i", i4IfAutoProfileIfIndex ,i4SetValIfAutoProfileEncapType)
#define nmhSetIfAutoProfileIpQosProfileIndex(i4IfAutoProfileIfIndex ,i4SetValIfAutoProfileIpQosProfileIndex) \
 nmhSetCmn(IfAutoProfileIpQosProfileIndex, 12, IfAutoProfileIpQosProfileIndexSet, CfaLock, CfaUnlock, 1, 0, 1, "%i %i", i4IfAutoProfileIfIndex ,i4SetValIfAutoProfileIpQosProfileIndex)
#define nmhSetIfAutoProfileRowStatus(i4IfAutoProfileIfIndex ,i4SetValIfAutoProfileRowStatus) \
 nmhSetCmn(IfAutoProfileRowStatus, 12, IfAutoProfileRowStatusSet, CfaLock, CfaUnlock, 1, 1, 1, "%i %i", i4IfAutoProfileIfIndex ,i4SetValIfAutoProfileRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IfIvrBridgedIface[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIfIvrBridgedIface(i4IfMainIndex ,i4SetValIfIvrBridgedIface) \
 nmhSetCmn(IfIvrBridgedIface, 12, IfIvrBridgedIfaceSet, CfaLock, CfaUnlock, 0, 0, 1, "%i %i", i4IfMainIndex ,i4SetValIfIvrBridgedIface)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IfSetMgmtVlanList[10];
extern UINT4 IfResetMgmtVlanList[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIfSetMgmtVlanList(pSetValIfSetMgmtVlanList) \
 nmhSetCmn(IfSetMgmtVlanList, 10, IfSetMgmtVlanListSet, CfaLock, CfaUnlock, 0, 0, 0, "%s", pSetValIfSetMgmtVlanList)
#define nmhSetIfResetMgmtVlanList(pSetValIfResetMgmtVlanList) \
 nmhSetCmn(IfResetMgmtVlanList, 10, IfResetMgmtVlanListSet, CfaLock, CfaUnlock, 0, 0, 0, "%s", pSetValIfResetMgmtVlanList)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IfSecondaryIpAddress[12];
extern UINT4 IfSecondaryIpSubnetMask[12];
extern UINT4 IfSecondaryIpBroadcastAddr[12];
extern UINT4 IfSecondaryIpRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIfSecondaryIpSubnetMask(i4IfMainIndex , u4IfSecondaryIpAddress ,u4SetValIfSecondaryIpSubnetMask) \
 nmhSetCmn(IfSecondaryIpSubnetMask, 12, IfSecondaryIpSubnetMaskSet, CfaLock, CfaUnlock, 0, 0, 2, "%i %p %p", i4IfMainIndex , u4IfSecondaryIpAddress ,u4SetValIfSecondaryIpSubnetMask)
#define nmhSetIfSecondaryIpBroadcastAddr(i4IfMainIndex , u4IfSecondaryIpAddress ,u4SetValIfSecondaryIpBroadcastAddr) \
 nmhSetCmn(IfSecondaryIpBroadcastAddr, 12, IfSecondaryIpBroadcastAddrSet, CfaLock, CfaUnlock, 0, 0, 2, "%i %p %p", i4IfMainIndex , u4IfSecondaryIpAddress ,u4SetValIfSecondaryIpBroadcastAddr)
#define nmhSetIfSecondaryIpRowStatus(i4IfMainIndex , u4IfSecondaryIpAddress ,i4SetValIfSecondaryIpRowStatus) \
 nmhSetCmn(IfSecondaryIpRowStatus, 12, IfSecondaryIpRowStatusSet, CfaLock, CfaUnlock, 0, 1, 2, "%i %p %i", i4IfMainIndex , u4IfSecondaryIpAddress ,i4SetValIfSecondaryIpRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IfMainExtMacAddress[12];
extern UINT4 IfMainExtSysSpecificPortID[12];
extern UINT4 IfMainExtInterfaceType[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIfMainExtMacAddress(i4IfMainIndex ,SetValIfMainExtMacAddress) \
 nmhSetCmn(IfMainExtMacAddress, 12, IfMainExtMacAddressSet, CfaLock, CfaUnlock, 0, 0, 1, "%i %m", i4IfMainIndex ,SetValIfMainExtMacAddress)
#define nmhSetIfMainExtSysSpecificPortID(i4IfMainIndex ,u4SetValIfMainExtSysSpecificPortID) \
 nmhSetCmn(IfMainExtSysSpecificPortID, 12, IfMainExtSysSpecificPortIDSet, CfaLock, CfaUnlock, 0, 0, 1, "%i %u", i4IfMainIndex ,u4SetValIfMainExtSysSpecificPortID)
#define nmhSetIfMainExtInterfaceType(i4IfMainIndex ,i4SetValIfMainExtInterfaceType) \
 nmhSetCmn(IfMainExtInterfaceType, 12, IfMainExtInterfaceTypeSet, CfaLock, CfaUnlock, 0, 0, 1, "%i %i", i4IfMainIndex ,i4SetValIfMainExtInterfaceType)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IfCustTLVType[12];
extern UINT4 IfCustTLVLength[12];
extern UINT4 IfCustTLVValue[12];
extern UINT4 IfCustTLVRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIfCustTLVLength(i4IfMainIndex , u4IfCustTLVType ,u4SetValIfCustTLVLength) \
 nmhSetCmn(IfCustTLVLength, 12, IfCustTLVLengthSet, CfaLock, CfaUnlock, 0, 0, 2, "%i %u %u", i4IfMainIndex , u4IfCustTLVType ,u4SetValIfCustTLVLength)
#define nmhSetIfCustTLVValue(i4IfMainIndex , u4IfCustTLVType ,pSetValIfCustTLVValue) \
 nmhSetCmn(IfCustTLVValue, 12, IfCustTLVValueSet, CfaLock, CfaUnlock, 0, 0, 2, "%i %u %s", i4IfMainIndex , u4IfCustTLVType ,pSetValIfCustTLVValue)
#define nmhSetIfCustTLVRowStatus(i4IfMainIndex , u4IfCustTLVType ,i4SetValIfCustTLVRowStatus) \
 nmhSetCmn(IfCustTLVRowStatus, 12, IfCustTLVRowStatusSet, CfaLock, CfaUnlock, 0, 1, 2, "%i %u %i", i4IfMainIndex , u4IfCustTLVType ,i4SetValIfCustTLVRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IfCustOpaqueAttributeID[12];
extern UINT4 IfCustOpaqueAttribute[12];
extern UINT4 IfCustOpaqueRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIfCustOpaqueAttribute(i4IfMainIndex , i4IfCustOpaqueAttributeID ,u4SetValIfCustOpaqueAttribute) \
 nmhSetCmn(IfCustOpaqueAttribute, 12, IfCustOpaqueAttributeSet, CfaLock, CfaUnlock, 0, 0, 2, "%i %i %u", i4IfMainIndex , i4IfCustOpaqueAttributeID ,u4SetValIfCustOpaqueAttribute)
#define nmhSetIfCustOpaqueRowStatus(i4IfMainIndex , i4IfCustOpaqueAttributeID ,i4SetValIfCustOpaqueRowStatus) \
 nmhSetCmn(IfCustOpaqueRowStatus, 12, IfCustOpaqueRowStatusSet, CfaLock, CfaUnlock, 0, 1, 2, "%i %i %i", i4IfMainIndex , i4IfCustOpaqueAttributeID ,i4SetValIfCustOpaqueRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IfTypeProtoDenyContextId[12];
extern UINT4 IfTypeProtoDenyMainType[12];
extern UINT4 IfTypeProtoDenyBrgPortType[12];
extern UINT4 IfTypeProtoDenyProtocol[12];
extern UINT4 IfTypeProtoDenyRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIfTypeProtoDenyRowStatus(u4IfTypeProtoDenyContextId , i4IfTypeProtoDenyMainType , i4IfTypeProtoDenyBrgPortType , i4IfTypeProtoDenyProtocol ,i4SetValIfTypeProtoDenyRowStatus) \
 nmhSetCmn(IfTypeProtoDenyRowStatus, 12, IfTypeProtoDenyRowStatusSet, CfaLock, CfaUnlock, 0, 1, 4, "%u %i %i %i %i", u4IfTypeProtoDenyContextId , i4IfTypeProtoDenyMainType , i4IfTypeProtoDenyBrgPortType , i4IfTypeProtoDenyProtocol ,i4SetValIfTypeProtoDenyRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IfmDebug[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIfmDebug(u4SetValIfmDebug) \
 nmhSetCmn(IfmDebug, 10, IfmDebugSet, CfaLock, CfaUnlock, 0, 0, 0, "%u", u4SetValIfmDebug)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FfFastForwardingEnable[10];
extern UINT4 FfCacheSize[10];
extern UINT4 FfIpChecksumValidationEnable[10];
extern UINT4 FfStaticEntryInvalidTrapEnable[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFfFastForwardingEnable(i4SetValFfFastForwardingEnable) \
 nmhSetCmn(FfFastForwardingEnable, 10, FfFastForwardingEnableSet, CfaLock, CfaUnlock, 1, 0, 0, "%i", i4SetValFfFastForwardingEnable)
#define nmhSetFfCacheSize(i4SetValFfCacheSize) \
 nmhSetCmn(FfCacheSize, 10, FfCacheSizeSet, CfaLock, CfaUnlock, 1, 0, 0, "%i", i4SetValFfCacheSize)
#define nmhSetFfIpChecksumValidationEnable(i4SetValFfIpChecksumValidationEnable) \
 nmhSetCmn(FfIpChecksumValidationEnable, 10, FfIpChecksumValidationEnableSet, CfaLock, CfaUnlock, 1, 0, 0, "%i", i4SetValFfIpChecksumValidationEnable)
#define nmhSetFfStaticEntryInvalidTrapEnable(i4SetValFfStaticEntryInvalidTrapEnable) \
 nmhSetCmn(FfStaticEntryInvalidTrapEnable, 10, FfStaticEntryInvalidTrapEnableSet, CfaLock, CfaUnlock, 1, 0, 0, "%i", i4SetValFfStaticEntryInvalidTrapEnable)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FfHostCacheDestAddr[12];
extern UINT4 FfHostCacheNextHopAddr[12];
extern UINT4 FfHostCacheIfIndex[12];
extern UINT4 FfHostCacheNextHopMediaAddr[12];
extern UINT4 FfHostCacheEntryType[12];
extern UINT4 FfHostCacheRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFfHostCacheNextHopAddr(u4FfHostCacheDestAddr ,u4SetValFfHostCacheNextHopAddr) \
 nmhSetCmn(FfHostCacheNextHopAddr, 12, FfHostCacheNextHopAddrSet, CfaLock, CfaUnlock, 1, 0, 1, "%p %p", u4FfHostCacheDestAddr ,u4SetValFfHostCacheNextHopAddr)
#define nmhSetFfHostCacheIfIndex(u4FfHostCacheDestAddr ,i4SetValFfHostCacheIfIndex) \
 nmhSetCmn(FfHostCacheIfIndex, 12, FfHostCacheIfIndexSet, CfaLock, CfaUnlock, 1, 0, 1, "%p %i", u4FfHostCacheDestAddr ,i4SetValFfHostCacheIfIndex)
#define nmhSetFfHostCacheNextHopMediaAddr(u4FfHostCacheDestAddr ,pSetValFfHostCacheNextHopMediaAddr) \
 nmhSetCmn(FfHostCacheNextHopMediaAddr, 12, FfHostCacheNextHopMediaAddrSet, CfaLock, CfaUnlock, 1, 0, 1, "%p %s", u4FfHostCacheDestAddr ,pSetValFfHostCacheNextHopMediaAddr)
#define nmhSetFfHostCacheEntryType(u4FfHostCacheDestAddr ,i4SetValFfHostCacheEntryType) \
 nmhSetCmn(FfHostCacheEntryType, 12, FfHostCacheEntryTypeSet, CfaLock, CfaUnlock, 1, 0, 1, "%p %i", u4FfHostCacheDestAddr ,i4SetValFfHostCacheEntryType)
#define nmhSetFfHostCacheRowStatus(u4FfHostCacheDestAddr ,i4SetValFfHostCacheRowStatus) \
 nmhSetCmn(FfHostCacheRowStatus, 12, FfHostCacheRowStatusSet, CfaLock, CfaUnlock, 1, 1, 1, "%p %i", u4FfHostCacheDestAddr ,i4SetValFfHostCacheRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FmMemoryResourceTrapEnable[10];
extern UINT4 FmTimersResourceTrapEnable[10];
extern UINT4 FmTracingEnable[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFmMemoryResourceTrapEnable(i4SetValFmMemoryResourceTrapEnable) \
 nmhSetCmn(FmMemoryResourceTrapEnable, 10, FmMemoryResourceTrapEnableSet, CfaLock, CfaUnlock, 1, 0, 0, "%i", i4SetValFmMemoryResourceTrapEnable)
#define nmhSetFmTimersResourceTrapEnable(i4SetValFmTimersResourceTrapEnable) \
 nmhSetCmn(FmTimersResourceTrapEnable, 10, FmTimersResourceTrapEnableSet, CfaLock, CfaUnlock, 1, 0, 0, "%i", i4SetValFmTimersResourceTrapEnable)
#define nmhSetFmTracingEnable(i4SetValFmTracingEnable) \
 nmhSetCmn(FmTracingEnable, 10, FmTracingEnableSet, CfaLock, CfaUnlock, 1, 0, 0, "%i", i4SetValFmTracingEnable)

#endif
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IfSecurityBridging[10];
extern UINT4 IfSetSecVlanList[10];
extern UINT4 IfResetSecVlanList[10];
extern UINT4 IfSecIvrIfIndex[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIfSecurityBridging(i4SetValIfSecurityBridging) \
 nmhSetCmn(IfSecurityBridging, 10, IfSecurityBridgingSet, NULL, NULL, 0, 0, 0, "%i", i4SetValIfSecurityBridging)
#define nmhSetIfSetSecVlanList(pSetValIfSetSecVlanList) \
 nmhSetCmn(IfSetSecVlanList, 10, IfSetSecVlanListSet, NULL, NULL, 0, 0, 0, "%s", pSetValIfSetSecVlanList)
#define nmhSetIfResetSecVlanList(pSetValIfResetSecVlanList) \
 nmhSetCmn(IfResetSecVlanList, 10, IfResetSecVlanListSet, NULL, NULL, 0, 0, 0, "%s", pSetValIfResetSecVlanList)
#define nmhSetIfSecIvrIfIndex(i4SetValIfSecIvrIfIndex) \
 nmhSetCmn(IfSecIvrIfIndex, 10, IfSecIvrIfIndexSet, NULL, NULL, 0, 0, 0, "%i", i4SetValIfSecIvrIfIndex)
#endif

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IfACPortIdentifier[12];
extern UINT4 IfACCustomerVlan[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIfACPortIdentifier(i4IfMainIndex ,i4SetValIfACPortIdentifier) \
 nmhSetCmn(IfACPortIdentifier, 12, IfACPortIdentifierSet, CfaLock, CfaUnlock, 0, 0, 1, "%i %i", i4IfMainIndex ,i4SetValIfACPortIdentifier)
#define nmhSetIfACCustomerVlan(i4IfMainIndex ,i4SetValIfACCustomerVlan) \
 nmhSetCmn(IfACCustomerVlan, 12, IfACCustomerVlanSet, CfaLock, CfaUnlock, 0, 0, 1, "%i %i", i4IfMainIndex ,i4SetValIfACCustomerVlan)

#endif

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IfUfdSystemControl[10];
extern UINT4 IfUfdModuleStatus[10];
extern UINT4 IfSplitHorizonSysControl[10];
extern UINT4 IfSplitHorizonModStatus[10];


#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIfUfdSystemControl(i4SetValIfUfdSystemControl)    \
    nmhSetCmn(IfUfdSystemControl, 10, IfUfdSystemControlSet, NULL, NULL, 0, 0, 0, "%i", i4SetValIfUfdSystemControl)
#define nmhSetIfUfdModuleStatus(i4SetValIfUfdModuleStatus)  \
    nmhSetCmn(IfUfdModuleStatus, 10, IfUfdModuleStatusSet, NULL, NULL, 0, 0, 0, "%i", i4SetValIfUfdModuleStatus)
#define nmhSetIfSplitHorizonSysControl(i4SetValIfSplitHorizonSysControl)    \
        nmhSetCmn(IfSplitHorizonSysControl, 10, IfSplitHorizonSysControlSet, NULL, NULL, 0, 0, 0, "%i", i4SetValIfSplitHorizonSysControl)
#define nmhSetIfSplitHorizonModStatus(i4SetValIfSplitHorizonModStatus)  \
            nmhSetCmn(IfSplitHorizonModStatus, 10, IfSplitHorizonModStatusSet, NULL, NULL, 0, 0, 0, "%i", i4SetValIfSplitHorizonModStatus)


#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IfUfdGroupId[12];
extern UINT4 IfUfdGroupName[12];
extern UINT4 IfUfdGroupUplinkPorts[12];
extern UINT4 IfUfdGroupDownlinkPorts[12];
extern UINT4 IfUfdGroupDesigUplinkPort[12];
extern UINT4 IfUfdGroupRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIfUfdGroupName(i4IfUfdGroupId ,pSetValIfUfdGroupName) \
    nmhSetCmn(IfUfdGroupName, 12, IfUfdGroupNameSet, NULL, NULL, 0, 0, 1, "%i %s", i4IfUfdGroupId ,pSetValIfUfdGroupName)
#define nmhSetIfUfdGroupUplinkPorts(i4IfUfdGroupId ,pSetValIfUfdGroupUplinkPorts)   \
    nmhSetCmn(IfUfdGroupUplinkPorts, 12, IfUfdGroupUplinkPortsSet, NULL, NULL, 0, 0, 1, "%i %s", i4IfUfdGroupId ,pSetValIfUfdGroupUplinkPorts)
#define nmhSetIfUfdGroupDownlinkPorts(i4IfUfdGroupId ,pSetValIfUfdGroupDownlinkPorts)   \
    nmhSetCmn(IfUfdGroupDownlinkPorts, 12, IfUfdGroupDownlinkPortsSet, NULL, NULL, 0, 0, 1, "%i %s", i4IfUfdGroupId ,pSetValIfUfdGroupDownlinkPorts)
#define nmhSetIfUfdGroupDesigUplinkPort(i4IfUfdGroupId ,i4SetValIfUfdGroupDesigUplinkPort)  \
    nmhSetCmn(IfUfdGroupDesigUplinkPort, 12, IfUfdGroupDesigUplinkPortSet, NULL, NULL, 0, 0, 1, "%i %i", i4IfUfdGroupId ,i4SetValIfUfdGroupDesigUplinkPort)
#define nmhSetIfUfdGroupRowStatus(i4IfUfdGroupId ,i4SetValIfUfdGroupRowStatus)  \
    nmhSetCmn(IfUfdGroupRowStatus, 12, IfUfdGroupRowStatusSet, NULL, NULL, 0, 1, 1, "%i %i", i4IfUfdGroupId ,i4SetValIfUfdGroupRowStatus)

#endif

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IfOOBNode0SecondaryIpAddress[10];
extern UINT4 IfOOBNode0SecondaryIpMask[10];
extern UINT4 IfOOBNode1SecondaryIpAddress[10];
extern UINT4 IfOOBNode1SecondaryIpMask[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIfOOBNode0SecondaryIpAddress(u4SetValIfOOBNode0SecondaryIpAddress) \
 nmhSetCmn(IfOOBNode0SecondaryIpAddress, 10, IfOOBNode0SecondaryIpAddressSet, CfaLock, CfaUnlock, 0, 0, 0, "%p", u4SetValIfOOBNode0SecondaryIpAddress)
#define nmhSetIfOOBNode0SecondaryIpMask(u4SetValIfOOBNode0SecondaryIpMask) \
 nmhSetCmn(IfOOBNode0SecondaryIpMask, 10, IfOOBNode0SecondaryIpMaskSet, CfaLock, CfaUnlock, 0, 0, 0, "%p", u4SetValIfOOBNode0SecondaryIpMask)
#define nmhSetIfOOBNode1SecondaryIpAddress(u4SetValIfOOBNode1SecondaryIpAddress) \
 nmhSetCmn(IfOOBNode1SecondaryIpAddress, 10, IfOOBNode1SecondaryIpAddressSet, CfaLock, CfaUnlock, 0, 0, 0, "%p", u4SetValIfOOBNode1SecondaryIpAddress)
#define nmhSetIfOOBNode1SecondaryIpMask(u4SetValIfOOBNode1SecondaryIpMask) \
 nmhSetCmn(IfOOBNode1SecondaryIpMask, 10, IfOOBNode1SecondaryIpMaskSet, CfaLock, CfaUnlock, 0, 0, 0, "%p", u4SetValIfOOBNode1SecondaryIpMask)


#endif
