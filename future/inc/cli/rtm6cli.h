/* $Id: rtm6cli.h,v 1.7 2015/07/29 06:15:43 siva Exp $   */
/* Description: This file contains RRD cli definitions */

#ifndef __RTM6CLI_H__
#define __RTM6CLI_H__

#include "cli.h"

#define CLI_RRD6_PERMIT               1
#define CLI_RRD6_DENY                 2

#define CLI_RRD6_OSPF_AREA_RT         1
#define CLI_RRD6_OSPF_EXT_RT          2

#define     RRD6_CLI_DEF_DEST         0
#define     RRD6_CLI_DEF_RANGE        128

#define RRD6_CLI_IP_PROTO_STATIC      3
#define RRD6_CLI_IP_PROTO_RIP         5 
#define RRD6_CLI_IP_PROTO_OSPF        13 
#define RRD6_CLI_IP_PROTO_BGP         14 

#define RRD6_CLI_MAX_ARGS             10
#define RRD6_MAX_ADDR_BUFFER          256
#define     RRD6_CLI_STATIC                       1
#define     RRD6_CLI_LOCAL                        2
#define     RRD6_CLI_RIP                          3
#define     RRD6_CLI_OSPF                         4
#define     RRD6_CLI_ALL                          255


#define     RRD6_CLI_RIP_MASK                     0x00000010
#define     RRD6_CLI_OSPF_MASK                    0x00000020
#define     RRD6_CLI_ALL_MASK                     0x00000070

/* Command Identifier */

enum {
    CLI_RRD6_SHOW_CONTROL_INFO=1,
    CLI_RRD6_SHOW_STATUS, 
#ifndef IP_WANTED
    CLI_RRD6_AS_NUM,      
    CLI_RRD6_ROUTER_ID,
#endif
    CLI_RRD6_EXPORT_OSPF,
    CLI_RRD6_NO_EXPORT_OSPF,
    CLI_RRD6_DEF_CONTROL_STATUS,  
    CLI_RRD6_REDISTRIBUTE_POLICY,
    CLI_RRD6_NO_REDISTRIBUTE_POLICY,
    CLI_RRD6_THROT_VALUE,
    CLI_RRD6_RED_DYN_INFO,
    CLI_RRD6_SHOW_MEM_RES,
    CLI_RRD6_MAX_MEM_RES,
    CLI_RRD6_MAX_COMMANDS
};


/* Error codes */
enum {
    CLI_RRD6_INVALID_INPUT=1,
    CLI_RRD6_NOENTRY_CTRL_TABLE,
    CLI_RRD6_EXPORT_FLAG_NOTSET,
    CLI_RRD6_INVALID_SRCPROTO,
    CLI_RRD6_INVALID_DESTPROTO,
    CLI_RRD6_NO_REDISTRIBUTE,
    CLI_RRD6_PROTO_NOREG,
    CLI_RRD6_MEMORY_OVERRUN,
    CLI_RRD6_ROUTE_EXIST,
    CLI_RRD6_MAX_ERR
   }; 


/* Error strings */

#ifdef __RTM6CLI_C__

CONST CHR1 *Rrd6CliErrString [] = {
    NULL,
    "\r% Invalid Input\r\n",
    "\r% No Such Entry in RRD Control Table\r\n",
    "\r% Global Export Flag Not Enabled\r\n",
    "\r% Invalid Source Protocol\r\n",
    "\r% Invalid Destination Protocol\r\n",
    "\r% Redistribution not enabled\r\n",
    "\r% Protocol not registered with RRD\r\n",
    "\r% Memory Unavailable\r\n",
    "\r% Memory Reservation Failure. Free Existing protocol routes\r\n",
};    
#endif




/* Prototypes added for rrdcli.c */

INT4 cli_process_rrd6_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Comamnd, ...));  
INT4 CliRrd6AsNum (tCliHandle,INT4);
INT4 CliRrd6RouterId (tCliHandle,UINT4);
INT4 CliRrd6AddControlTable (tCliHandle,UINT1,tIp6Addr *,UINT4,UINT2,INT4);
INT4 CliRrd6DelControlTable (tCliHandle,tIp6Addr *,UINT4);
INT4 CliRrd6OspfAreaStatus (tCliHandle,INT4,INT4);
INT4 CliRrd6OspfExtStatus (tCliHandle,INT4,INT4);
INT4 CliRrd6DefControlEntry (tCliHandle, INT4);
INT4 ShowRtm6ControlInfoInCxt (tCliHandle, UINT4);
INT4 ShowRrd6StatusInCxt (tCliHandle, UINT4);
void CliRrd6ModDefCtrlEntry (tCliHandle, INT4);
INT4 CliRrd6ThrotValue (tCliHandle ,UINT4);
INT4 Rtm6CliGetCommandType (UINT4, INT1 *); 
INT4 ShowRtm6RouteMemReservation(tCliHandle CliHandle);
VOID Rtm6ShowScalarsInCxt (tCliHandle CliHandle);
#ifdef RM_WANTED
INT4 ShowRtm6RedDynInfo(tCliHandle);
#endif

#endif /* __RTM6CLI_H__ */
