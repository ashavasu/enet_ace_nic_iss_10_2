/************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved     */
/* Licensee Aricent Inc., 2004-2005                         */
/*                                                          */
/* $Id: isscli.h,v 1.236 2017/12/12 10:04:56 siva Exp $   */                  
/*                                                          */
/*  FILE NAME             : isscli.h                        */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                    */
/*  SUBSYSTEM NAME        : ISS                             */
/*  MODULE NAME           : CLI                             */
/*  LANGUAGE              : C                               */
/*  TARGET ENVIRONMENT    :                                 */
/*  DATE OF FIRST RELEASE :                                 */
/*  AUTHOR                : Aricent Inc.                    */
/*  DESCRIPTION           : This file contains CLI routines */
/*                          for system commands             */
/************************************************************/


#ifndef __ISSCLI_H__
#define __ISSCLI_H__
#include "cli.h"
#include "fssnmp.h"
#define MAX_NAME_SIZE 21
#define MAX_FILENAME_SIZE 128 
#define MAX_FILEREAD_SIZE 240
#define MAX_COLUMN_SIZE  80
#define LINES_TO_DISPLAY 20

/* ISS CLI command constants. To add a new command ,a new definition needs to
 * be added to the list.
 */

#define ISS_SPEED_AUTO 9

#define ISS_TELNET_CLIENT            1
#define ISS_SSH_CLIENT               2

#define ISS_SSH_STRING               "-ssh"
#define ISS_TELNET_STRING            "-telnet"
#define ISS_TRC_MAX_SIZE             255

enum
{
   ISS_SHOW_ALL = 1,
   ISS_SHOW_TEMPERATURE = 2,
   ISS_SHOW_FAN = 3,
   ISS_SHOW_RAM = 4,
   ISS_SHOW_CPU = 5,
   ISS_SHOW_FLASH = 6,
   ISS_SHOW_POWER = 7
};

enum
{
   ISS_SET_MAX_RAM = 1,
   ISS_SET_MAX_CPU = 2,
   ISS_SET_MAX_FLASH = 3,
   ISS_SET_MIN_TEMPERATURE = 4,
   ISS_SET_MAX_TEMPERATURE = 5,
   ISS_SET_MIN_POWER = 6,
   ISS_SET_MAX_POWER = 7
};

enum
{
  CLI_ISS_DEF_IPADDR_MODE = 1,
  CLI_ISS_DEF_IPADDR_ALLOC_PROTO,
  CLI_ISS_DEF_IPADDR,
  CLI_ISS_BASE_MAC,
  CLI_ISS_DEBUG_LOGGING,
  CLI_ISS_NO_DEBUG_LOGGING,
  CLI_ISS_DEBUG_NP,
  CLI_ISS_NO_DEBUG_NP,
  CLI_ISS_NPAPI_DEBUG_ON,
  CLI_ISS_NPAPI_DEBUG_OFF,
  CLI_ISS_RESTART,
  CLI_ISS_MIRR_RSPAN,
  CLI_ISS_MIRR_SPAN_SOURCE,
  CLI_ISS_MIRR_SPAN_SOURCE_VLAN,
  CLI_ISS_MIRR_SPAN_MAC_SOURCE_ACL,
  CLI_ISS_MIRR_SPAN_IP_SOURCE_ACL,
  CLI_ISS_MIRR_SPAN_DEST,
  CLI_ISS_NO_MIRR,
  CLI_ISS_SHOW_MIRR,
  CLI_ISS_SHOW_MIRR_RECORD,
  CLI_ISS_UT_MIRR,
  CLI_ISS_LOGIN_AUTH,
  CLI_ISS_NO_LOGIN_AUTH,
  CLI_ISS_ERASE_CONF,
  CLI_ISS_ARCHIVE,
  CLI_ISS_RUN_SCRIPT,
  CLI_ISS_CONFIG_SAVE,
  CLI_ISS_COPY_RESTORE,
  CLI_ISS_COPY_SAVE,
  CLI_ISS_COPY_LOGS,
  CLI_ISS_COPY_GEN,
  CLI_ISS_DEL_FLASHFILE,
  CLI_ISS_PING,
  CLI_ISS_STORM_CONTROL,
  CLI_ISS_NO_STORM_CONTROL,
  CLI_ISS_PORT_RATE_LIMIT,
  CLI_ISS_NO_PORT_RATE_LIMIT,
  CLI_ISS_INT_SPEED,
  CLI_ISS_NO_INT_SPEED,
  CLI_ISS_INT_DUPLEX,
  CLI_ISS_NO_INT_DUPLEX,
  CLI_ISS_INT_NEGO,
  CLI_ISS_NO_INT_NEGO,
  CLI_ISS_INT_HOL,
  CLI_ISS_NO_INT_HOL,
  CLI_ISS_LOOPBACK,
  CLI_ISS_IP_AUTH,
  CLI_ISS_NO_IP_AUTH, 
  CLI_ISS_HTTP_STATUS,
  CLI_ISS_HTTP_PORT,
  CLI_ISS_NO_HTTP_PORT,
  CLI_ISS_TELNET_STATUS,
  CLI_ISS_SHOW_SYS_INFO,
  CLI_ISS_SHOW_SYS_ACK,
  CLI_ISS_SHOW_NVRAM,
  CLI_ISS_SHOW_WTP_NVRAM,
  CLI_ISS_SHOW_PORT_MONITORING,
  CLI_ISS_SHOW_DBG_LOGGING,
  CLI_ISS_SHOW_IP_AUTH, 
  CLI_ISS_SHOW_HTTP_STATUS, 
  CLI_ISS_SHOW_TELNET_STATUS,
  CLI_ISS_SET_DATE,
  CLI_ISS_SHOW_LOOPBACK,  
  CLI_ISS_SHOW_DATE,
  CLI_ISS_SHOW_DEBUGGING,
  CLI_ISS_SHOW_RUNNING_CONFIG,
  CLI_ISS_SERIAL_CONSOLE_PROMPT,
  CLI_ISS_SWITCH_NAME,
  CLI_ISS_DEF_RESTORE_FILE,
  CLI_ISS_DEF_RM_INT,
  CLI_ISS_DEFAULT_VLAN_ID,
  CLI_MODULE_SHUTDOWN,
  CLI_MODULE_START,
  CLI_ISS_INCR_SAVE,
  CLI_ISS_DEFVAL_SAVE,
  CLI_ISS_AUTO_SAVE,
  CLI_ISS_FRONT_PANEL_PORT_COUNT,
  CLI_ISS_DEF_FRONT_PANEL_PORT_COUNT,
  CLI_ISS_ROLLBACK,
  CLI_ISS_AUDIT_STATUS,
  CLI_ISS_AUDIT_LOG_FILE,
  CLI_ISS_AUDIT_LOG_FILE_SIZE,
  CLI_ISS_AUDIT_LOG_SIZE_THRESHOLD,
  CLI_ISS_AUDIT_LOG_RESET,
  CLI_ISS_SHOW_AUDIT,
  CLI_ISS_SHOW_AUDIT_LOG_FILE,
  CLI_ISS_FIRMWARE_UPGRADE,
  CLI_ISS_SET_SWITCH_TEMPERATURE,
  CLI_ISS_SET_SWITCH_THRESHOLD,
  CLI_ISS_SET_SWITCH_POWER,
  CLI_ISS_SHOW_SWITCH_INFO,
  CLI_ISS_SET_TIMER_SPEED,
  CLI_ISS_MGMT_PORT_ROUTING,
  CLI_ISS_MAC_LEARNING_RATE_LIMIT,
  CLI_ISS_NO_MAC_LEARNING_RATE_LIMIT,
  CLI_ISS_SHOW_MAC_LEARN_RATE,
  CLI_ISS_ACL_PROVISION_MODE,
  CLI_ISS_ACL_HW_ACTION,
  CLI_ISS_VRF_UNQ_MAC_OPTION, 
  CLI_ISS_SHOW_PORT_ISOLATION, 
  CLI_ISS_CREATE_PORT_ISOLATION, 
  CLI_ISS_ADD_PORT_ISOLATION, 
  CLI_ISS_DEL_PORT_ISOLATION,
  CLI_ISS_REM_PORT_ISOLATION,
  CLI_ISS_LOGIN_BLOCK_PARAMS,
  CLI_ISS_RM_HB_MODE,
  CLI_ISS_RM_RTYPE,
  CLI_ISS_RM_DTYPE,
  CLI_ISS_CONFIG_RESTORE,
  CLI_ISS_CLEAR_CONFIG,
  CLI_ISS_SSH_CLIENT_SESSION,
  CLI_ISS_TELNET_CLIENT_SESSION,
  CLI_ISS_TELNET_CLIENT_STATUS,
  CLI_ISS_SSH_CLIENT_STATUS,
  CLI_ISS_SHOW_SSH_TEL_CLIENT,
  CLI_ISS_SLEEP,
  CLI_ISS_INT_CPU_LEARN,
  CLI_ISS_NO_INT_CPU_LEARN,
  CLI_ISS_AUTO_PORT_CREATE,
  CLI_ISS_TRAFFIC_SEPARATION_CONTROL,
  CLI_ISS_INT_AUTO_MDI_OR_MDIX,
  CLI_ISS_NO_INT_AUTO_MDI_OR_MDIX,
  CLI_ISS_SET_INT_MDI_OR_MDIX,
  CLI_ISS_PORT_RATE_PAUSE_LIMIT,
  CLI_ISS_NO_PORT_RATE_PAUSE_LIMIT,
  CLI_ISS_MIRROR_STATUS,
  CLI_ISS_DEF_EXEC_TIMEOUT,
  CLI_ISS_NO_DEF_EXEC_TIMEOUT,
  CLI_ISS_DEF_RM_INT_TYPE,
  CLI_ISS_HEALTH_CHK_CLEAR_COUNTER,
  CLI_ISS_SHOW_HEALTH_CHK_STATUS,
  CLI_ISS_SHOW_CONFIG_RESTORE_STATUS,
  CLI_ISS_DUMP_MEM_LOC,
  CLI_ISS_DEBUG_FLASH_LOGGING,
  CLI_ISS_HW_CONSOLE,
  CLI_ISS_IMAGE_DUMP,
  CLI_ISS_DUMP_OSRESOURCE,
  CLI_ISS_WEB_SESSION_TIMEOUT,
  CLI_ISS_SHOW_STDBYDBG_LOGGING,
  CLI_ISS_STANDBY_RESTART,
  CLI_ISS_DEBUG_TRACE,
  CLI_ISS_TEST_CLEAR_CONFIG,
  CLI_ISS_DISS_CENTRALIZED_IP,
  CLI_ISS_PORT_RATE_DEFAULT,
  CLI_ISS_NO_SWITCH_MODE,
  CLI_ISS_SWITCH_MODE,
  CLI_ISS_SHOW_SWITCHING_MODE,
  CLI_ISS_DEF_RESTORE_TYPE,
  CLI_ISS_CPU_MIRRORING,
  CLI_ISS_SHOW_CPU_MIRR_DETAILS,
  CLI_ISS_DEBUG_TIMESTAMP
};

enum {
 CLI_ISS_INVALID_MODE= CLI_ERR_START_ID_ISS,
 CLI_ISS_BAD_IPMASK,
 CLI_ISS_INVALID_MAC,
 CLI_ISS_NONEG_SPEED,
 CLI_ISS_NONEG_DUPLEX,
 CLI_ISS_NONEG_FLOW_CONTROL,
 CLI_ISS_MONITOR_LA,
 CLI_ISS_MONITOR_MIRRORING_ENABLED,
 CLI_ISS_MIRR_MONITORING_ENABLED,
 CLI_MIRR_SRC_NOT_EXIST,
 CLI_MIRR_SRC_IN_PORT_CHANNEL,
 CLI_MIRR_DEST_NOT_EXIST,
 CLI_ISS_MIRR_PORT_DEST_CFG,
 CLI_ISS_MIRR_MODE_NOT_VALID,
 CLI_ISS_MIRR_PORT_SRC_CFG,
 CLI_ISS_MIRR_PORT_DEST_SRC_CFG,
 CLI_MIRR_MAX_DEST_PORTS,
 CLI_MIRR_MAX_SRC_PORTS,
 CLI_SET_MIRR_RSPAN_NOT_EXISTS,
 CLI_ISS_INVALID_IP_ADDR,
 CLI_ISS_INVALID_IP_ADDR_IP_MASK,
 CLI_ISS_INVALID_HTTP_PORT,
 CLI_ISS_INVALID_HTTP_STATUS,
 CLI_ISS_SPEED_NOT_SUPPORTED,
 CLI_ISS_40G_10G_PORT_SHUTDOWN,
 CLI_ISS_EOAM_DUPLEX_CONFLICT,
 CLI_ISS_INVALID_CONSOLE_OPTION,
 CLI_ISS_INCR_SAVE_ENABLE_FAILED,
 CLI_ISS_INCR_SAVE_DISABLE_FAILED,
 CLI_ISS_AUTO_SAVE_ENABLE_FAILED,
 CLI_ISS_AUTO_SAVE_DISABLE_FAILED,
 CLI_ISS_ROLLBACK_ENABLE_FAILED,
 CLI_ISS_ROLLBACK_DISABLE_FAILED,
 CLI_ISS_INVALID_FRONT_PANEL_PORT,
 CLI_ISS_TASK_STATUS_CONFLICT,
 CLI_ISS_TEMPERATURE_LIMIT_ERR,
 CLI_ISS_CPU_LIMIT_ERR,
 CLI_ISS_POWER_LIMIT_ERR,
 CLI_ISS_RAM_LIMIT_ERR,
 CLI_ISS_FLASH_LIMIT_ERR,
 CLI_ISS_SET_DEFVAL_SAVE_FAILED,
 CLI_ISS_AUTO_NEG_SFP,
 CLI_ISS_SELF_IP,
 CLI_ISS_INVALID_VRUNQ,
 CLI_ISS_PI_DEL_FAILED,
 CLI_ISS_PI_ADD_FAILED,
 CLI_ISS_PI_TABLE_EMPTY, 
 CLI_ISS_PI_INVALID_DELETION,
 CLI_ISS_INVALID_RESTART,
 CLI_ISS_CLI_INVALID_LOCKOUT_TIME,
 CLI_ISS_CLI_INVALID_LOGIN_ATTEMPT,
 CLI_ISS_SSH_CLIENT_DISABLED,
 CLI_ISS_TELNET_CLIENT_DISABLED,
 CLI_ISS_INVALID_PORT_RATE,
 CLI_ISS_INVALID_RM_INTERFACE_TYPE,
 CLI_ISS_INVALID_CLRCFG_RESTORE_FILE,
 CLI_ISS_PROTOCOL_NOT_SPECIFIED,
 CLI_ISS_INVALID_RESTORE_FILE_LEN,
 CLI_ISS_CLRCFG_NOT_SUPPORTED,
 CLI_ISS_STANDBY_UNIT_UNAVAILABLE,
 CLI_ISS_HA_NOT_AVAIL,
 CLI_ISS_CLRCFG_NOT_SUPP_STDBY,
 CLI_ISS_NONEG_SPEED_1G_NOT_SUPPORTED,
 CLI_ISS_SPEED_10_100_ONLY_SUPPORTED,
 CLI_ISS_NO_DIFF_SPEED,
 CLI_ISS_INVALID_RATE,
 CLI_MIRR_MORE_THAN_ONE_DEST_PER_SESSION,
 CLI_MIRR_INVALID_CONF_SESSION,
 CLI_ISS_PI_INVALID_ADDITION_DELETION,
 CLI_MIRR_SESSION_VAL_FAILURE,
 CLI_MIRR_SESSION_NOT_CREATED,
 CLI_MIRR_ROW_STATUS_ACTIVE,
 CLI_MIRR_INV_CXT_ID,
 CLI_ISS_ICCL_VLAN_ERR,
 CLI_ISS_SET_RESTORE_TYPE_FAILED,
 CLI_ISS_SPEED_INVALID,
 CLI_ISS_SPEED_NON_BASE_NOT_SUPPORTED,
 CLI_MIRR_EGRESS_ACL_UNSUPPORTED,
 CLI_ISS_SET_DEBUG_TIMESTAMP_FAILED,
 CLI_ISS_INVALID_DEBUG_TIMESTAMP,
 CLI_ISS_MAX_ERR

};
enum{
    CFA_DHCP_CLIENT = 1,
    CFA_DHCP6_CLIENT,
    CFA_PIM,
    CFA_OSPF,
    CFA_DHCP_SRVCLI,
    CFA_DHCP_SRVRLY,
    CFA_DHCP6_SRVCLI,
    CFA_DHCP6_SRVRLY,
    CFA_DOT1X,
    CFA_VRRP,
    CFA_RIPV1,
    CFA_RIPV2,
    CFA_LLDP,
    CFA_MLDS,
    CFA_MCAST_LISTENER_REPORT,
    CFA_MCAST_LISTENER_DONE,
    CFA_MCASTV2_LISTENER_REPORT,
    CFA_CFI_BIT,
    CFA_IPV6_ND,
    CFA_IP6_ALL_ROUTER,
    CFA_IP6_ALL_NODE,
    CFA_IP6_SOLICITED_NODE,
    CFA_OSPFV3_ALLD_ROUTER,
    CFA_OSPFV3_ALLSPF_ROUTER,
    CFA_RIP6,
    CFA_PIMV6,
    CFA_PVRST,
    CFA_SNTP,
    CFA_VRRP_IPV6,
    CFA_VRRP_IPV6_LINK_LOCAL,
    CFA_IEEE_MAC
};


/* Values that are related to display the details of task,
 ** queue and semaphore using dump os resource command
 ***/
enum OsixResourceTypes {
    TASK = 1,
    QUE,
    SEM
};

/* Accessing the ErrString with ErrCode value.
 * Since the ErrCode's starting Id is changed, this forumla is used
 * to access the Err String */
#define CLI_ERR_OFFSET_ISS(u4ErrCode)   (u4ErrCode - CLI_ERR_START_ID_ISS + 1)

#ifdef __ISSCLI_C__ 
CONST CHR1 *IssCliErrString[] = {
    NULL,
    "% Invalid Mode \r\n",
    "% Bad mask for given IP address \r\n",
    "% Invalid Base MAC address \r\n",
    "% Speed cannot be set if auto-negotiation is enabled \r\n",
    "% Duplexity cannot be set if auto-negotiation is enabled \r\n",
    "% Flow Control cannot be set if auto-negotiation is enabled \r\n",
    "% This port cannot be a monitor port since it belongs to a port-channel \r\n",
    "% This port cannot be a monitor port since mirroring is enabled \r\n",
    "% Mirroring can not be enabled on monitor port \r\n",
    "% Source Port not configured in Session \r\n",
    "% Mirroring cannot be enabled on a port configured in port channel\r\n",
    "% Destination Port not configured in Session \r\n",
    "% Port already configured as Mirror Destination \r\n",
    "% This access-list has been already configured to another monitor session\r\n",
    "% Source configuration conflicts with another session\r\n",
    "% Port already configured as Mirror Destination or as Source Port\r\n",
    "% Maximum destination mirror ports exceeded \r\n",
    "% Maximum source mirror ports exceeded \r\n",
    "% RSPAN Vlan given not exists \r\n",
    "% Invalid IP Address \r\n",
    "% Invalid combination of IP Address and IP Mask\r\n",
    "% Invalid Port number. This port has already been assigned to some other service\r\n",
    "% Invalid Http server status\r\n",
    "% This speed is not supported by hardware\r\n",
    "% Port state will be admin down during speed change from/to 1/10/25/40/50/100G\r\n",
    "% Duplexity cannot be modified when ethernet-oam is enabled on the interface\r\n",
    "% Invalid console option\r\n",
    "% Unable to Enable Incremental Save Feature\r\n",
    "% Unable to Disable Incremental Save Feature\r\n",
    "% Unable to Enable Auto Save Trigger\r\n",
    "% Unable to Disable Auto Save Trigger\r\n",
    "% Rollback enable failed\r\n",
    "% Rollback disable failed\r\n",
    "% Invalid Number of Front panel ports\r\n" ,
    "% Task is already started\r\n",
    "% Temperature threshold should be in the range from -14 to 40 celsius\r\n",
    "% CPU utilization threshold should be in the range of 1-100 percentage\r\n",
    "% PowerSupply voltage should be in the range of 100-230 volts \r\n",
    "% RAM utilization threshold should be in the range of 1-100 percentage\r\n",
    "% Flash utilization threshold should be in the range of 1-100 percentage\r\n",
    "% Default Value save flag set operation failed\r\n",
    "% Auto-negotiation cannot be changed\r\n",
    "% This operation is not permitted for self IP \r\n",
    "% Invalid VRF unique mac option\r\n",
    "% Port Isolation entry is not available for deletion\r\n", 
    "% Invalid indices to Port Isolation Entry creation\r\n",
    "% Port Isolation table is empty \r\n",
    "% Volatile Port isolation entries cannot be deleted\r\n",
    "% ISS Cannot be restarted\r\n",
    "% CLI Lockout time value should be in the range of 30-600 seconds\r\n",
    "% CLI Login Attempt value should be in the range of 1-10\r\n",
    "% SSH client session is disabled\r\n",
    "% Telnet client session is disabled\r\n",
    "% Invalid port rate \r\n",
    "% Invalid value for RM Interface Type \r\n",
    "% Clear Config: Restoration file does not exist \r\n",
    "% Clear counters: Protocol not specified \r\n",
    "% Clear Config: Restoration file size should not exceed 15 characters \r\n",
    "% Clear config is not supported for packages included in the build \r\n",
    "% Standby Unit not available\r\n",
    "% High Availability not available in this image\r\n",
    "% Clear config is not supported in standby node \r\n",
    "% Speed 1000Mbps not configurable, but achieved through Auto Mode \r\n",
    "% Speed 10/100Mbps only supported and configurable with mode No-Nego \r\n",
    "% LAG cannot be created between the ports that are operating in different speeds \r\n",
    "% Unsupported Rate or Burst Value \r\n",
    "% Only one destination port per mirror session is allowed \r\n",
    "% Invalid/UnSupported Configuration to monitor session \r\n",
    "% Ingress and Egress port cannot be same in port isolation \r\n",
    "% Session Validation Failure \r\n",
    "% Session Not Created \r\n",
    "% Row status is active \r\n",
    "% Invalid Context ID \r\n",
    "% ICCL VLAN cannot be default VLAN \r\n",
 "% Default restore type setting failed\r\n",
    "% Unsupported configuration on the port with speed 10Gbps or more \r\n",
    "% 40G/50G/100G speed configurations are allowed only on base-ports\r\n",
    "% Egress Mirroring for ACL is not allowed\r\n",
    "% Timestamp set failed for debug trace \r\n",
    "% Invalid set value for debug timestamp \r\n",
    
};
#endif
   
#if defined __ISSEXCLI_C__ || defined ISSPICLI_C || defined __FSMEFCLI_C__
extern CONST CHR1 *IssCliErrString[];
#endif /* defined __ISSEXCLI_C__ || defined ISSPICLI_C */

#define CLI_ISS_MAX_ARGS 25       /* Number of arguments for system commands */

#define ISS_MIRR_PORT                1
#define ISS_MIRR_TO_PORT             2
#define ISS_RESET_MIRROR_TO_PORT       0

#define ISS_LOCAL                1
#define ISS_RADIUS               2
#define ISS_TACACS               3
#define ISS_RADIUS_LOCAL         4
#define ISS_TACACS_LOCAL         5
#define ISS_PAM_AUTH             6

#define ISS_STARTUP              1
#define ISS_NVRAM                2

#define ISS_TFTP                 1
#define ISS_SFTP                 2
#define ISS_REMOTE               3
#define ISS_FLASH                4
#define ISS_ABSOLUTE_PATH        5

#define ISS_REMOTE_RESTORE       1
#define ISS_FLASH_RESTORE        2
#define ISS_REMOTE_TFTP_RESTORE  3
#define ISS_REMOTE_SFTP_RESTORE  4

#define ISS_REMOTE_SAVE          2
#define ISS_REMOTE_TFTP_SAVE     3
#define ISS_REMOTE_SFTP_SAVE     4
#define ISS_FLASH_SAVE           5
#define ISS_LOOP_DURATION        1

#define ISS_NUM_SPOKES           4

#ifdef MRVLLS
#define ISS_RATE_BCAST_LIMIT            1
#define ISS_RATE_MCAST_BCAST_LIMIT      2
#define ISS_RATE_DLF_MCAST_BCAST_LIMIT  3
#define ISS_RATE_ALL_FRAMES_LIMIT       4

#define ISS_STORM_CONTROL_128K     1
#define ISS_STORM_CONTROL_256K     2
#define ISS_STORM_CONTROL_512K     3
#define ISS_STORM_CONTROL_1M       4
#define ISS_STORM_CONTROL_2M       5
#define ISS_STORM_CONTROL_4M       6
#define ISS_STORM_CONTROL_8M       7
#define ISS_STORM_CONTROL_16M      8
#define ISS_STORM_CONTROL_32M      9
#define ISS_STORM_CONTROL_64M      10
#define ISS_STORM_CONTROL_128M     11
#define ISS_STORM_CONTROL_256M     12
#else
#define ISS_RATE_BCAST_LIMIT     1
#define ISS_RATE_MCAST_LIMIT     2
#define ISS_RATE_DLF_LIMIT       3
#define ISS_STORM_CONTROL_ENABLE 1
#define ISS_STORM_CONTROL_DISABLE 0
#define ISS_RATE_DEFVAL          0
#define ISS_RATE_ZEROVAL          0
#endif

#define ISS_CLI_RATE_LIMIT_INVALID       -1
#define ISS_CLI_STORM_CONTROL_ENABLE  1
#define ISS_CLI_STORM_CONTROL_DISABLE  0
#define ISS_DEFAULT_MASK         0xFFFFFFFF

#define ISS_ALL_ENTRIES          1
#define ISS_SPECIFIC_ENTRY       2
#define ISS_NEW_ENTRY            3

#define ISS_CLI_SERIAL_CONSOLE      1
#define ISS_CLI_NO_SERIAL_CONSOLE   2

#define ISS_CLI_NP_CTL_FLG         0x0001
#define ISS_CLI_NP_ENTRY_EXIT_FLG  0x0010
#define ISS_CLI_NP_PKT_RX_FLG      0x0100
#define ISS_CLI_NP_PKT_TX_FLG      0x1000
#define ISS_CLI_NP_TIME_STAMP_FLG  0x0002
#define ISS_CLI_NP_RX_DUMP_PKT_FLG  0x0004
#define ISS_CLI_NP_TX_DUMP_PKT_FLG  0x0008

#define ISS_MIRR_INIT_VAL  0

#define ISS_MIRR_SHOW_DETAIL  1
#define ISS_MIRR_SHOW_SESSION 2
#define ISS_MIRR_SHOW_ALL     3
#define ISS_MIRR_SHOW_RANGE   4
#define ISS_MIRR_SHOW_LOCAL   5
#define ISS_MIRR_SHOW_RUNNING 6

#define ISS_MIRR_LIST_SIZE   ((ISS_MIRR_MAX_SESSIONS + 31)/32 * 4) 

#define CLI_ISS_HEALTH_CHK_POPULATE_COUNTER 1

#ifdef VXLAN_WANTED
#define NP_VXLAN_ENTRY_EXIT_FLG  0x00000001
#define NP_VXLAN_UCAST_CFG_FLG   0x00000010
#define NP_VXLAN_MCAST_CFG_FLG   0x00000100
#define NP_VXLAN_ACL_CFG_FLG     0x00001000
#define NP_VXLAN_ERROR_FLG       0x00011000
#define NP_VXLAN_TRC_ALL         (NP_VXLAN_ENTRY_EXIT_FLG | \
                                  NP_VXLAN_UCAST_CFG_FLG  | \
                                  NP_VXLAN_MCAST_CFG_FLG  | \
                                  NP_VXLAN_ACL_CFG_FLG    | \
                                  NP_VXLAN_ERROR_FLG)
extern VOID NpOpusFmDbgDumpArpTable (UINT4 sw, UINT4 verbose);
extern VOID NpVxlanDebugShow (VOID);
extern VOID NpVxlanGetTraceLevel (UINT4 *pu4TraceLevel);
extern VOID NpVxlanSetTraceLevel (UINT4 u4TraceLevel);
extern VOID NpVxlanShowDebugging (VOID);
extern VOID NpOpusShowMaxDefineConfig (VOID);
extern INT4 NpAclUpdateHitCount (UINT4 u4Rule,UINT4 u4RuleNo,INT4 u4Flag);
#endif /* VXLAN_WANTED */


INT4 cli_process_isshealth_test_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);


VOID cli_process_iss_cmd PROTO ((tCliHandle CliHandle, UINT4, ...));
VOID cli_process_iss_ext_cmd PROTO ((tCliHandle CliHandle, UINT4, ...));
VOID cli_process_iss_pi_cmd PROTO ((tCliHandle CliHandle, UINT4, ...));

INT4 CliSetIssDefaultIpAddrCfgMode(tCliHandle ,UINT4);

INT4 CliSetIssDefaultIpAddrAllocProto PROTO ((tCliHandle CliHandle,
                                              UINT4 u4Protocol));

INT4 CliIssConfigDefValSaveFlag PROTO ((tCliHandle CliHandle,
                                      INT4 i4DefValSaveOption));

INT4 CliSetIssDefaultRmInterfaceType (tCliHandle CliHandle, INT4 i4RmInterfaceType);
INT4 CliSetIssDefaultIpAddress(tCliHandle ,UINT4 , UINT4 ,UINT4);
INT4 CliSetIssDefaultRmInit (tCliHandle,UINT1*);
INT4 CliSetIssBaseMacAddr(tCliHandle , tMacAddr);
INT4 CliSetIssDebugLogging(tCliHandle ,UINT4, UINT1);
INT4 CliSetIssDumpOsResource(tCliHandle, UINT4, CHR1 *);
INT4 CliSetImageDump (tCliHandle,UINT1 *);
INT4 CliSetIssDefaultVlanId PROTO ((tCliHandle , UINT4 ));
INT4 CliSetIssSystemTmrSpeed PROTO ((tCliHandle , UINT4 ));
INT4 CliSetIssRestart PROTO ((VOID));
INT4 CliSetIssStandbyRestart PROTO ((VOID));
INT4 CliSetIssMirrorStatus (tCliHandle,INT4);
INT4 CliSetIssMirrorRspan (tCliHandle,INT4 ,UINT4,UINT4 ,UINT4, UINT4);
INT4 CliSetIssMirrorSpanSource(tCliHandle,INT4, INT4, UINT4, UINT4, UINT4);
INT4 CliSetIssMirrorSpanSourceVlan(tCliHandle,INT4, INT4, UINT4, UINT4, UINT4,
                                   UINT4);
INT4 CliSetIssMirrorSpanDestination (tCliHandle,INT4,UINT4 ,UINT4);
INT4 CliSetIssNoMirror (tCliHandle CliHandle,INT4);
VOID CliIssShowMirrorStatus(tCliHandle CliHandle, INT4, UINT1);
INT4 CliIssShowMirrorSession (tCliHandle ,INT4);
INT4 CliIssShowMirrorSessionDetail (tCliHandle ,INT4);
INT4 CliIssShowMirrorRecords (tCliHandle);
INT4 CliSetIssLoginAuth(tCliHandle , UINT4, UINT4);
INT4
CliSetIssRollbackFlag (tCliHandle, INT4);
INT4 CliSetIssErase(tCliHandle ,UINT4, UINT1 *);
INT4 CliIssConfigSave (tCliHandle, UINT4, UINT1 *,
                       tIPvXAddr *, UINT1 *, UINT1 *, UINT1 *);
INT4 CliIssCopyToStartupFile(tCliHandle , UINT4 , UINT1 *, tIPvXAddr , UINT1 *, UINT1 *, UINT1 *);
INT4 CliIssCopyFromStartupFile(tCliHandle ,UINT4 , UINT1 * , tIPvXAddr , UINT1 *, UINT1 *, UINT1 *);
INT4 CliIssCopyLogs (tCliHandle, UINT1 *, tIPvXAddr ,UINT1 *,
                     UINT1 *, UINT1 *, UINT4, UINT1);

INT4 CliIssImageDownload(tCliHandle , UINT1 *, tIPvXAddr, UINT1 *, UINT1 *, UINT1 *, UINT4);
INT4 CliSetIssSpeed(tCliHandle,UINT4,UINT4);
INT4 CliSetIssDuplexity(tCliHandle,UINT4,UINT4);
INT4 CliSetIssNegotiation(tCliHandle,UINT4,UINT4);
INT4 CliSetIssStormControl(tCliHandle,UINT4,UINT4,UINT4);
INT4 CliSetIssConfPortRateLimit (tCliHandle ,UINT4 ,INT4, INT4);
INT4 CliSetIssConfDefaultRate (tCliHandle,UINT4,INT4);
INT4 IssCopyFileGeneric(tCliHandle, UINT1 *, 
                        UINT4, UINT1 *, UINT1 *, tIPvXAddr, UINT1 *,
                        UINT1 *, UINT4, UINT1 *, UINT1 *, tIPvXAddr, UINT1 *);

INT4 CliSetIssDefaultRestoreFile (tCliHandle CliHandle, UINT1 *pu1FileName);
INT4 CliSetIssSwitchName (tCliHandle CliHandle, UINT1 *pu1SwitchName);
INT4 CliIssShowSysInfo(tCliHandle );
INT4 CliIssShowSysAck(tCliHandle );
INT4 CliIssShowNvRam(tCliHandle);
#ifdef WTP_WANTED
INT4 CliIssShowWtpNvRam(tCliHandle);
#endif
INT4 CliIssShowPortMonitor(tCliHandle);
INT4 CliIssShowDebugLogging (tCliHandle);
INT4 CliIssShowStandbyDebugLogging (tCliHandle);
INT4 CliMibSave (tCliHandle CliHandle, UINT4 u4Mode);
VOID CliPrintWheel (tCliHandle,UINT1 *);
VOID bootp_exit(UINT1);
INT4 CliIssIpAuth (tCliHandle, UINT4, UINT4, UINT1*, UINT1*, UINT1 , UINT4);
INT4 CliIssNoIpAuth (tCliHandle, UINT4, UINT4);
INT4 CliIssHttpPort (tCliHandle CliHandle, INT4 i4HttpPort);
INT4 CliIssHttpStatus (tCliHandle CliHandle, INT4 i4HttpStatus);
INT4 CliIssShowIpAuth (tCliHandle, UINT4, UINT4);
INT4 CliIssShowHttpStatus (tCliHandle);
INT4 IssShowIpManagerTable (tCliHandle, UINT4, UINT4);
VOID IssIpAuthOctetToVlanList (tCliHandle, UINT1 *, UINT4);
VOID IssConfIpAuthOctetToVlanList (tCliHandle, UINT1 *, UINT4);
VOID IssDisplayServicesAllowed (tCliHandle, UINT4);
INT4 CliIssShowDebugging (tCliHandle CliHandle);
INT4 CliIssSetCliSerialConsolePrompt (tCliHandle , UINT4 );

INT4 CliSetIssCutThrough (tCliHandle CliHandle);
INT4 CliSetIssNoCutThrough (tCliHandle CliHandle);
INT4 CliIssShowSwitchModeType (tCliHandle CliHandle);
INT4 SwitchModeTypeShowRunningConfig(tCliHandle CliHandle);
INT4
CliIssSetMgmtPortRouting (tCliHandle CliHandle, INT4 i4RoutingFlag);

INT4 CliIssConfigIncrSaveFlag (tCliHandle, INT4);
INT4 CliIssConfigAutoSaveFlag (tCliHandle, INT4);
INT4 CliIssConfigAutoPortCreate (tCliHandle, INT4);


INT4                                     
CliIssSetDate(tCliHandle , INT1 *, UINT4 *, UINT4 , UINT4 *);
INT4            
CliIssShowDate (tCliHandle CliHandle);
INT4
CliIssSleep (INT4 ); 
INT4
CliSetIssAuditLogStatus (tCliHandle , UINT4 );
INT4
CliSetIssAuditLogFile (tCliHandle , UINT1* );
INT4
CliSetIssAuditLogFileSize (tCliHandle , UINT4 );
INT4
CliSetIssAuditLogSizeThreshold (tCliHandle, UINT4);
INT4
CliSetIssAuditLogReset (tCliHandle);
INT4
CliShowIssAuditLog (tCliHandle );
INT4
CliShowIssAuditLogFile (tCliHandle );

INT4 CliIssSetAclHwTrigger (tCliHandle, INT4);
INT4 CliIssSetAclHwAction (tCliHandle, INT4);
INT4 CliIssSetHeartBeatMode (tCliHandle, UINT4);
INT4 CliIssSetRmRType (tCliHandle, UINT4);
INT4 CliIssSetRmDType (tCliHandle, UINT4);

INT4
CliSetIssFrontPanelPortCount (tCliHandle CliHandle,
                              UINT4 u4CliFrontPanelPortCount);
    
INT4 CliSetIssSwitchRAMInfo PROTO ((tCliHandle, UINT4));
INT4 CliSetIssSwitchCPUInfo PROTO ((tCliHandle, UINT4));
INT4 CliSetIssSwitchFlashInfo PROTO ((tCliHandle, UINT4));
INT4 CliSetIssSwitchTemperatureInfo PROTO ((tCliHandle, UINT4, INT4));
INT4 CliSetIssSwitchPowerSupplyInfo PROTO ((tCliHandle, UINT4, UINT4));
INT4
IssPICliShowPortIsolation PROTO ((tCliHandle , INT4));
INT1
IssPICliUtlDelPortIsolationEntry PROTO ((tCliHandle, UINT4 ,tVlanId, INT4));
INT1
IssPICliDelPortIsolation PROTO((tCliHandle ,
                        UINT4      ,
                        tVlanId    ,
                        UINT4      *));
INT1
IssPICliCreatePortIsolation PROTO((tCliHandle ,
                           UINT4      ,
                           tVlanId    ,
                           UINT4      *));

INT1
IssPICliAddPortIsolation PROTO ((tCliHandle ,
                        UINT4 ,tVlanId ,UINT4 *));


INT4
CliSetIssMaxLoginAttempt (tCliHandle,
                         UINT4 ,
                         UINT1);
 
INT4 CliIssShowAllSwitchInfo PROTO ((tCliHandle));
INT4 CliIssShowSwitchTemperature PROTO ((tCliHandle));
INT4 CliIssShowSwitchFanStatus PROTO ((tCliHandle));
INT4 CliIssShowSwitchRAMThreshold PROTO ((tCliHandle));
INT4 CliIssShowSwitchCPUThreshold PROTO ((tCliHandle));
INT4 CliIssShowSwitchFlashThreshold PROTO ((tCliHandle));
INT4 CliIssShowSwitchPowerStatus PROTO ((tCliHandle));
INT4 CliIssSetVrfUnqMacOption PROTO ((tCliHandle CliHandle,
                                      INT4 i4VrfUnqMacFlag));
INT4 IssFirmwareUpgrade (tCliHandle CliHandle, UINT1 *pu1SrcFileName,
                          tIPvXAddr SrcIpAddress,UINT1 *pu1HostName,
     UINT1 *pu1DstFileName, UINT1 *au1DstFilePath);
INT4 CliIssConfigRestore (tCliHandle CliHandle, UINT4 u4Mode, 
                          UINT4 u4IpAddress, UINT1 *pu1FileName);
INT4 CliIssClearConfig (tCliHandle CliHandle, UINT1 *pu1FileName);

INT4 CliIssTrafficSeprtnCliSetControl PROTO ((tCliHandle CliHandle, 
                                       INT4 i4TrafficControl));
INT4 CliIssTrafficSeprtnShowControl PROTO ((tCliHandle CliHandle));


extern VOID IssSpanningTreeShowDebugging (tCliHandle);
extern VOID IssSnoopShowDebugging (tCliHandle, UINT1);
extern VOID IssSslShowDebugging (tCliHandle);
extern VOID IssSshShowDebugging (tCliHandle);
extern VOID IssDhcpSrvShowDebugging (tCliHandle);
extern VOID IssDhcpRelayShowDebugging (tCliHandle,INT4);
extern VOID IssDhcpClientShowDebugging (tCliHandle);
extern VOID IssOspfShowDebugging (tCliHandle, UINT4 u4OspfCxtId);
extern VOID IssPimShowDebugging (tCliHandle);
extern INT1 nmhGetFsPnacTraceOption ARG_LIST((INT4 *));
extern INT1 nmhGetFsRstTraceOption ARG_LIST((INT4 *));
extern INT1 nmhGetSysContact ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetSysName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetSysLocation ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
extern VOID IssMfwdShowDebugging (tCliHandle);
extern VOID IssIgmpShowDebugging (tCliHandle CliHandle);
extern VOID IssRip6ShowDebugging (tCliHandle CliHandle);
extern VOID IssIp6ShowDebugging (tCliHandle CliHandle);
extern VOID IssFmDbgDumpRouteTables (UINT4 u4HwSwitch,
                UINT4 u4HwDumpRouteFlags);

INT4 AclShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module);
INT4
CliIssCheckMsrStatus (tCliHandle CliHandle);

INT4 CliIssShowRunningConfig (tCliHandle, UINT4, UINT4, UINT1*);
VOID IssPICliShowRunningConfig (tCliHandle ,
                                UINT4);
INT4 IssSysShowRunningConfig (tCliHandle);
INT4 SysInfoShowRunningConfig (tCliHandle);
INT4 IssStpVlanShowRunningConfig(tCliHandle CliHandle,UINT4 ,UINT4 ,UINT4 );

#ifdef NPAPI_WANTED
VOID IssNpapiShowDebugging PROTO((tCliHandle));
INT4 IssIntfShowRunningConfig (tCliHandle, INT4, UINT1*);
#ifdef SWC
extern INT4 DiffservShowRunningConfig(tCliHandle, UINT4);
#elif MRVLLS
extern INT4  QoSShowRunningConfig(tCliHandle);
#else
extern INT4 DiffservShowRunningConfig(tCliHandle);
#endif

#endif

INT4 InterfaceShowRunningConfig(tCliHandle, INT4, UINT4);
INT4 InterfaceShowRunningConfigMappingDetails (tCliHandle , INT4);
extern INT4 SnoopShowRunningConfigVlanTable(tCliHandle, INT4, UINT4, UINT2);
extern INT4 SnoopShowRunningConfigVlanStaticMacstTable(tCliHandle, INT4, INT4);
extern INT4 RstpShowRunningConfig(tCliHandle,UINT4,UINT4);
extern INT4 MstpShowRunningConfig(tCliHandle,UINT4,UINT4);
extern INT4 PvrstShowRunningConfig(tCliHandle,UINT4,UINT4);
extern INT4 DhcpSrvShowRunningConfig(tCliHandle CliHandle); 
extern INT4 DhcpRelayShowRunningConfig(tCliHandle CliHandle, UINT4 u4Module);
extern INT4 DhcpClientShowRunningConfig(tCliHandle CliHandle, UINT4 u4Module);
extern INT4 SslShowRunningConfig (tCliHandle CliHandle);
extern INT4 SshShowRunningConfig (tCliHandle CliHandle);
extern INT4 SyslgShowRunningConfig (tCliHandle CliHandle);
extern INT4 MriShowRunningConfig (tCliHandle);
extern INT4 IgmpShowRunningConfig(tCliHandle, UINT4);
extern INT4 MldShowRunningConfig (tCliHandle, UINT4);
extern INT4 IgmpProxyShowRunningConfig(tCliHandle, UINT4);
extern UINT1 RipShowRunningConfigScalar (tCliHandle CliHandle, INT4 i4CxtId);
extern INT4 RipShowRunningConfigTable (tCliHandle CliHandle, UINT1 u1Flag, INT4 i4CxtId);
extern INT4 RipShowRunningConfigInterface (tCliHandle CliHandle, INT4 i4CxtId);
extern INT4 PimShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module, INT4 i4Addrtype);
extern INT4 PimShowRunningConfigTable (tCliHandle CliHandle, INT4 i4AddrType);
extern INT4 PimShowRunningConfigInterface (tCliHandle CliHandle, INT4 i4AddrType);
extern INT4 PimShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4Index, INT4 i4AddrType);
extern INT4 PimShowRunningConfigScalar (tCliHandle CliHandle, INT4 i4AddrType);
extern INT4
MsdpShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module, INT4 i4AddrType);
extern INT1 nmhGetFirstIndexFsSynceIfTable ARG_LIST((INT4 *));
extern INT4 SynceShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module, ...);
extern INT4 SynceShowRunningConfigFsSynceIfTable(tCliHandle CliHandle, UINT4  u4Module, ...);
extern INT4 OfcShowRunningConfig(tCliHandle CliHandle, UINT4  u4Module, ...);
extern INT4 FsbShowRunningConfigInterfaceDetails (tCliHandle, INT4);
#ifdef L2TPV3_WANTED
extern INT4 L2tpShowRunningConfig (tCliHandle);
#endif
#ifdef VXLAN_WANTED
extern INT4 VxlanShowRunningConfig(tCliHandle CliHandle, UINT4  u4Module, ...);
#endif
#ifdef EVPN_VXLAN_WANTED
extern INT4 EvpnShowRunningConfig(tCliHandle CliHandle, UINT4  u4Module, ...);
#endif /* EVPN_VXLAN_WANTED */
extern VOID SnoopShowRunningConfig (tCliHandle, UINT4, UINT4);
extern INT4 OspfShowRunningConfigInCxt (tCliHandle CliHandle, UINT4 u4Module,
                          UINT4 u4OspfCxtId);
extern INT4 Ipv6ShowRunningConfigInCxt (tCliHandle CliHandle,UINT4 u4Module, UINT4);
extern INT4 Rip6ShowRunningConfig(tCliHandle,UINT4);
extern INT4  MbsmShowRunningConfig(tCliHandle);
extern INT4 Ospfv3ShowRunningConfig (tCliHandle CliHandle,UINT4 u4Module);
INT4 CliSetIssHOlBlockPrevention (tCliHandle CliHandle, UINT4 u4IfIndex,
                                  INT4 i4HolStatus);
INT4 CliSetIssCpuCntrlLearning (tCliHandle CliHandle, UINT4 u4IfIndex,
    INT4 i4CpuLearnStatus);
INT4 CliSetIssMdiOrMdixCap (tCliHandle,UINT4,INT4);
INT4 CliGetIssMdiOrMdixCap (tCliHandle,UINT4,INT4 *);

INT4 CliSetIssPauseRateLimit PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex,
                                     INT4 i4MaxPortRate, INT4 i4MinPortRate));

INT4 CliSetIssHwConsole PROTO ((tCliHandle CliHandle, UINT1 *pu1HwCmd ));
INT4 CliConfigHwMode PROTO ((tCliHandle CliHandle));
INT1 CliGetHwCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

extern INT4 ElmShowRunningConfig(tCliHandle,UINT4);
extern INT4 SnoopBasicShowRunningConfig (tCliHandle, UINT4);

INT4 CliIssSetModuleSystemControl (tCliHandle CliHandle, INT4 i4ModuleId,
                                   INT4 i4SysCtrl);
INT4 CliIssSetMacLearnLimitRate (tCliHandle CliHandle, INT4 i4IssMacLearnRateLimit,
                                 UINT4 u4IssMacLearnRateLimitInterval);
INT4 CliIssShowMacLearnLimitRate (tCliHandle CliHandle);

extern INT4 IsIsShowRunningConfigInCxt PROTO ((tCliHandle CliHandle,UINT4 u4Module, INT4 i4Instances));
extern INT4 EntShowRunningConfig PROTO ((tCliHandle CliHandle));
extern INT4 IPSecv6ShowRunningconfig PROTO ((tCliHandle CliHandle));

#ifdef POE_WANTED
extern INT4 PoeShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module);
#endif
enum
{
   CLI_ENT_CFG_ASSET_ID = 1,
   CLI_ENT_CFG_SER_NUM,
   CLI_ENT_CFG_ALIAS_NAME,
   CLI_ENT_CFG_URIS,
   CLI_ENT_SHOW_LOG_ENTITY,
   CLI_ENT_SHOW_LOG_ENTITY_ALL,
   CLI_ENT_SHOW_PHY_ENTITY,
   CLI_ENT_SHOW_PHY_ENTITY_ALL,
   CLI_ENT_SHOW_LP_MAPPING_ALL,
   CLI_ENT_SHOW_ALIAS_MAPPING,
   CLI_ENT_SHOW_ALIAS_MAPPING_ALL,
   CLI_ENT_SHOW_PHY_CONTAINMENT,
   CLI_ENT_SHOW_PHY_CONTAINMENT_ALL,
   CLI_ENT_RESET_ASSET_ID,
   CLI_ENT_RESET_SER_NUM,
   CLI_ENT_RESET_ALIAS_NAME,
   CLI_ENT_RESET_URIS,
   CLI_ENT_RESET_ALL
};
INT4 cli_process_ent_cmd PROTO ((tCliHandle CliHandle, UINT4, ...));

INT4 CliIssTelnetStatus PROTO ((tCliHandle CliHandle, INT4 i4TelnetStatus));
INT4 CliIssShowTelnetStatus PROTO((tCliHandle CliHandle));

extern INT4 PuttyEstablishClientSession PROTO ((INT4, UINT1 **));
INT4 CliIssEstablishClientSession PROTO ((UINT1 **, UINT1, UINT1));
VOID CliIssShowSshTelnetClient PROTO ((tCliHandle, UINT1));
INT4 CliIssHealthChkClr PROTO((tCliHandle CliHandle, UINT1 *));
VOID CliIssShowHealthChkStatus PROTO ((tCliHandle CliHandle));
VOID CliIssShowConfigRestoreStatus PROTO ((tCliHandle CliHandle));

PUBLIC INT4
FpamUtlShowRunningConfig (tCliHandle CliHandle);
INT4 CliIssSetSshTelnetClientStatus PROTO ((INT4, UINT1));
INT4 CliIssShutModuleShowRunningConfig PROTO ((tCliHandle , UINT1 *));
INT4 CliSetIssDebugFlashLogging (tCliHandle CliHandle, UINT4 u4Mode,UINT1 *pu1FilePath);
INT4 CliIssSetRestoreType PROTO ((tCliHandle, INT4));
INT4 CliSetIssCpuMirroring PROTO ((tCliHandle , INT4 , UINT4));
VOID CliShowIssCpuMirroring PROTO ((tCliHandle , UINT1));
extern INT1  etext, end;
#endif /* _ISSCLI_H_ */
