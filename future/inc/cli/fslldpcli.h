/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fslldpcli.h,v 1.8 2016/07/12 12:40:15 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsLldpSystemControl[10];
extern UINT4 FsLldpModuleStatus[10];
extern UINT4 FsLldpTraceInput[10];

extern UINT4 FsLldpTagStatus[10];
extern UINT4 FsLldpConfiguredMgmtIpv4Address[10];
extern UINT4 FsLldpConfiguredMgmtIpv6Address[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsLldpSystemControl(i4SetValFsLldpSystemControl) \
 nmhSetCmnWithLock(FsLldpSystemControl, 10, FsLldpSystemControlSet, LldpLock, LldpUnLock, 0, 0, 0, "%i", i4SetValFsLldpSystemControl)
#define nmhSetFsLldpModuleStatus(i4SetValFsLldpModuleStatus) \
 nmhSetCmnWithLock(FsLldpModuleStatus, 10, FsLldpModuleStatusSet, LldpLock, LldpUnLock, 0, 0, 0, "%i", i4SetValFsLldpModuleStatus)
#define nmhSetFsLldpTraceInput(pSetValFsLldpTraceInput) \
 nmhSetCmnWithLock(FsLldpTraceInput, 10, FsLldpTraceInputSet, LldpLock, LldpUnLock, 0, 0, 0, "%s", pSetValFsLldpTraceInput)
#define nmhSetFsLldpTraceLevel(i4SetValFsLldpTraceLevel) \
 nmhSetCmnWithLock(FsLldpTraceLevel, 10, FsLldpTraceLevelSet, LldpLock, LldpUnLock, 0, 0, 0, "%i", i4SetValFsLldpTraceLevel)
#define nmhSetFsLldpTagStatus(i4SetValFsLldpTagStatus) \
 nmhSetCmnWithLock(FsLldpTagStatus, 10, FsLldpTagStatusSet, LldpLock, LldpUnLock, 0, 0, 0, "%i", i4SetValFsLldpTagStatus)
#define nmhSetFsLldpConfiguredMgmtIpv4Address(pSetValFsLldpConfiguredMgmtIpv4Address) \
 nmhSetCmnWithLock(FsLldpConfiguredMgmtIpv4Address, 10, FsLldpConfiguredMgmtIpv4AddressSet, LldpLock, LldpUnLock, 0, 0, 0, "%s", pSetValFsLldpConfiguredMgmtIpv4Address)
#define nmhSetFsLldpConfiguredMgmtIpv6Address(pSetValFsLldpConfiguredMgmtIpv6Address) \
 nmhSetCmnWithLock(FsLldpConfiguredMgmtIpv6Address, 10, FsLldpConfiguredMgmtIpv6AddressSet, LldpLock, LldpUnLock, 0, 0, 0, "%s", pSetValFsLldpConfiguredMgmtIpv6Address)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsLldpLocChassisIdSubtype[10];
extern UINT4 FsLldpLocChassisId[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsLldpLocChassisIdSubtype(i4SetValFsLldpLocChassisIdSubtype) \
 nmhSetCmn(FsLldpLocChassisIdSubtype, 10, FsLldpLocChassisIdSubtypeSet, LldpLock, LldpUnLock, 0, 0, 0, "%i", i4SetValFsLldpLocChassisIdSubtype)
#define nmhSetFsLldpLocChassisId(pSetValFsLldpLocChassisId) \
 nmhSetCmn(FsLldpLocChassisId, 10, FsLldpLocChassisIdSet, LldpLock, LldpUnLock, 0, 0, 0, "%s", pSetValFsLldpLocChassisId)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsLldpLocPortIdSubtype[12];
extern UINT4 FsLldpLocPortId[12];
extern UINT4 FsLldpPortConfigNotificationType[12];
extern UINT4 FsLldpLocPortDstMac[12];
extern UINT4 FsLldpMedAdminStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsLldpLocPortIdSubtype(i4LldpLocPortNum ,i4SetValFsLldpLocPortIdSubtype) \
 nmhSetCmn(FsLldpLocPortIdSubtype, 12, FsLldpLocPortIdSubtypeSet, LldpLock, LldpUnLock, 0, 0, 1, "%i %i", i4LldpLocPortNum ,i4SetValFsLldpLocPortIdSubtype)
#define nmhSetFsLldpLocPortId(i4LldpLocPortNum ,pSetValFsLldpLocPortId) \
 nmhSetCmn(FsLldpLocPortId, 12, FsLldpLocPortIdSet, LldpLock, LldpUnLock, 0, 0, 1, "%i %s", i4LldpLocPortNum ,pSetValFsLldpLocPortId)
#define nmhSetFsLldpPortConfigNotificationType(i4LldpLocPortNum ,i4SetValFsLldpPortConfigNotificationType) \
 nmhSetCmn(FsLldpPortConfigNotificationType, 12, FsLldpPortConfigNotificationTypeSet, LldpLock, LldpUnLock, 0, 0, 1, "%i %i", i4LldpLocPortNum ,i4SetValFsLldpPortConfigNotificationType)
#define nmhSetFsLldpLocPortDstMac(i4LldpLocPortNum ,SetValFsLldpLocPortDstMac) \
 nmhSetCmn(FsLldpLocPortDstMac, 12, FsLldpLocPortDstMacSet, LldpLock, LldpUnLock, 0, 0, 1, "%i %m", i4LldpLocPortNum ,SetValFsLldpLocPortDstMac)
#define nmhSetFsLldpMedAdminStatus(i4LldpLocPortNum ,i4SetValFsLldpMedAdminStatus) \
 nmhSetCmn(FsLldpMedAdminStatus, 12, FsLldpMedAdminStatusSet, LldpLock, LldpUnLock, 0, 0, 1, "%i %i", i4LldpLocPortNum ,i4SetValFsLldpMedAdminStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsLldpClearStats[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsLldpClearStats(i4SetValFsLldpClearStats) \
 nmhSetCmn(FsLldpClearStats, 10, FsLldpClearStatsSet, LldpLock, LldpUnLock, 0, 0, 0, "%i", i4SetValFsLldpClearStats)
#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fslldpv2Version[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFslldpv2Version(i4SetValFslldpv2Version) \
 nmhSetCmn(Fslldpv2Version, 10, Fslldpv2VersionSet, LldpLock, LldpUnLock, 0, 0, 0, "%i", i4SetValFslldpv2Version)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fslldpv2ConfigPortMapIfIndex[12];
extern UINT4 Fslldpv2ConfigPortMapDestMacAddress[12];
extern UINT4 Fslldpv2ConfigPortRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFslldpv2ConfigPortRowStatus(i4Fslldpv2ConfigPortMapIfIndex , tFslldpv2ConfigPortMapDestMacAddress ,i4SetValFslldpv2ConfigPortRowStatus) \
 nmhSetCmn(Fslldpv2ConfigPortRowStatus, 12, Fslldpv2ConfigPortRowStatusSet, LldpLock, LldpUnLock, 0, 1, 2, "%i %m %i", i4Fslldpv2ConfigPortMapIfIndex , tFslldpv2ConfigPortMapDestMacAddress ,i4SetValFslldpv2ConfigPortRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FslldpV2AddressTableIndex[12];
extern UINT4 FslldpV2DestMacAddress[12];
extern UINT4 Fslldpv2DestRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFslldpV2DestMacAddress(u4FslldpV2AddressTableIndex ,SetValFslldpV2DestMacAddress) \
 nmhSetCmn(FslldpV2DestMacAddress, 12, FslldpV2DestMacAddressSet, LldpLock, LldpUnLock, 0, 0, 1, "%u %m", u4FslldpV2AddressTableIndex ,SetValFslldpV2DestMacAddress)
#define nmhSetFslldpv2DestRowStatus(u4FslldpV2AddressTableIndex ,i4SetValFslldpv2DestRowStatus) \
 nmhSetCmn(Fslldpv2DestRowStatus, 12, Fslldpv2DestRowStatusSet, LldpLock, LldpUnLock, 0, 1, 1, "%u %i", u4FslldpV2AddressTableIndex ,i4SetValFslldpv2DestRowStatus)

#endif
