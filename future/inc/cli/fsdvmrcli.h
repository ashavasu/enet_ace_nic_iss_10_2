/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsdvmrcli.h,v 1.2 2008/10/31 13:09:30 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DvmrpStatus[10];
extern UINT4 DvmrpLogEnabled[10];
extern UINT4 DvmrpLogMask[10];
extern UINT4 DvmrpPruneLifeTime[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDvmrpStatus(i4SetValDvmrpStatus)	\
	nmhSetCmn(DvmrpStatus, 10, DvmrpStatusSet, NULL, NULL, 0, 0, 0, "%i", i4SetValDvmrpStatus)
#define nmhSetDvmrpLogEnabled(i4SetValDvmrpLogEnabled)	\
	nmhSetCmn(DvmrpLogEnabled, 10, DvmrpLogEnabledSet, NULL, NULL, 0, 0, 0, "%i", i4SetValDvmrpLogEnabled)
#define nmhSetDvmrpLogMask(i4SetValDvmrpLogMask)	\
	nmhSetCmn(DvmrpLogMask, 10, DvmrpLogMaskSet, NULL, NULL, 0, 0, 0, "%i", i4SetValDvmrpLogMask)
#define nmhSetDvmrpPruneLifeTime(i4SetValDvmrpPruneLifeTime)	\
	nmhSetCmn(DvmrpPruneLifeTime, 10, DvmrpPruneLifeTimeSet, NULL, NULL, 0, 0, 0, "%i", i4SetValDvmrpPruneLifeTime)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DvmrpInterfaceIfIndex[12];
extern UINT4 DvmrpInterfaceStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDvmrpInterfaceStatus(i4DvmrpInterfaceIfIndex ,i4SetValDvmrpInterfaceStatus)	\
	nmhSetCmn(DvmrpInterfaceStatus, 12, DvmrpInterfaceStatusSet, NULL, NULL, 0, 1, 1, "%i %i", i4DvmrpInterfaceIfIndex ,i4SetValDvmrpInterfaceStatus)

#endif
