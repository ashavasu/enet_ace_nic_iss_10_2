/********************************************************************
 * Copyright (C) Future Software Limited,1997-98,2001
 *
 * $Id: bfdclig.h,v 1.12 2015/01/23 11:47:37 siva Exp $
 *
 * Description:This file contains prototypes 
 *******************************************************************/
#ifndef __BFDCLIG_H__
#define __BFDCLIG_H__

enum
{
    CLI_BFD_MODULE_NOT_STARTED = 1,
    CLI_BFD_DEMAND_ECHO_NOT_SUPPORTED,
    CLI_BFD_SESS_NOT_CREATED,
    CLI_BFD_INVALID_SESS_INDEX,
    CLI_BFD_VER_NOT_SUPPORTED,
    CLI_BFD_INVALID_DEST_UDP_PORT,
    CLI_BFD_INVALID_SRC_UDP_PORT,
    CLI_BFD_DEMAND_MODE_NOT_SUPPORTED,
    CLI_BFD_MULTIPOINT_FLAG_NOT_SUPPORTED,
    CLI_BFD_AUTH_CHANGE_NOT_ALLOWED,
    CLI_BFD_AUTH_PARAM_CHANGE_NOT_ALLOWED,
    CLI_BFD_AUTH_SIMPLE_PASSWD,
    CLI_BFD_INVALID_AUTH_KEY_LEN,
    CLI_BFD_FILL_MAND_FIELD,
    CLI_BFD_ENCAP_TYPE_MISMATCH,
    CLI_BFD_SESS_ROLE_CHANGE_NOT_ALLOWED,
    CLI_BFD_SESS_MODE_CHANGE_NOT_ALLOWED,
    CLI_BFD_CCV_NOT_SUPPORTED_FOR_MPLS,
    CLI_BFD_NO_RDISCR_CHANGE,
    CLI_BFD_EXP_CHANGE_NOT_ALLOWED,
    CLI_BFD_TMR_NEGOT_MODI_NOT_ALLOWED,
    CLI_BFD_OFFLOAD_ENA_DIS_NOT_ALLOWED,
    CLI_BFD_ENCAP_CHANGE_NOT_ALLOWED,
    CLI_BFD_MAPTYPE_CHANGE_NOT_ALLOWED,
    CLI_BFD_LDP_NOT_SUPPORTED,
    CLI_BFD_MAPPOINTER_CHANGE_NOT_ALLOWED,
    CLI_BFD_CARD_MODI_NOT_ALLOWED,
    CLI_BFD_SLOT_MODI_NOT_ALLOWED,
    CLI_BFD_SESS_TYPE_CHANGE_NOT_ALLOWED,
    CLI_BFD_DESTUDP_CHANGE_NOT_ALLOWED,
    CLI_BFD_SRCUDP_CHANGE_NOT_ALLOWED,
    CLI_BFD_BASE_OID_DOES_NOT_MATCH,
    CLI_BFD_PATH_NOT_EXIST,
    CLI_BFD_SESS_NOT_EXISTS,
    CLI_BFD_SESS_MAPENTRY_NOT_EXISTS,
    CLI_BFD_SESS_TABLE_NOT_EXISTS,
    CLI_BFD_AUTH_NOT_ENABLED,
    CLI_BFD_AUTH_ENABLED,
    CLI_BFD_SESS_TYPE_NOT_SUPPORTED_FOR_IP,
    CLI_BFD_SESS_INTERFACE_CHANGE_NOT_ALLOWED,
    CLI_BFD_SESS_SRCADDR_CHANGE_NOT_ALLOWED,
    CLI_BFD_SESS_DSTADDR_CHANGE_NOT_ALLOWED,
    CLI_BFD_MODIFY_DYNAMIC_SESS_NOT_ALLOWED,
    CLI_BFD_SRC_ADDR_DST_ADDR_MISMATCH,
    CLI_BFD_SAME_PATH_MONITORING_EXIST,
    CLI_BFD_MODIFY_NOT_ALLOWED_WHEN_ACTIVE,
 CLI_BFD_CONNECTED_IP_IPV6_ADDR,
 CLI_BFD_DST_ZERO_NETWORK,
    CLI_BFD_MAX_ERR
};

#ifdef __BFDCLI_C__
CONST CHR1  *BfdCliErrString [] = {

    NULL,
    "Module is Shutdown \r\n",
    "Demand mode and Echo function are not supported \r\n",
    "Session not created \r\n",
    "Invalid Session Index \r\n",
    "Version not supported \r\n",
    "Invalid destination UDP port\r\n",
    "Invalid source UDP port\r\n",
    "Demand mode not supported\r\n",
    "Multipoint not supported\r\n",
    "Enabling/Disabling of authentication is not allowed when session is enabled\r\n",
    "Modification of authentication type is not allowed when session is enabled\r\n",
    "Only simple password authentication is supported\r\n",
    "Invalid Authentication Key Len\r\n",
    "Map Type/Map Pointer not filled\r\n",
    "Encapsulation type mismatch\r\n",
    "Modification of session role is not allowed when session is active\r\n",
    "Modification of session mode is not allowed when session is active\r\n",
    "CV mode not supported for mpls\r\n",
    "Modification of remote discriminator is not allowed when session is active\r\n",
    "Modification of exp value is not allowed when session is active\r\n",
    "Enabling/Disabling of Timer Negotiation not allowed when session is active\r\n",
    "Enabling/Disabling of offload is not allowed when session is active\r\n",
    "Modification of encapsulation is not allowed when session is active\r\n",
    "Modification of maptype is not allowed when session is active\r\n",
    "Non TE path not supported\r\n",
    "Modification of mappointer is not allowed when session is active\r\n",
    "Modification of card number is not allowed when session is active\r\n",
    "Modification of slot number is not allowed when session is active\r\n",
    "Modification of session type is not allowed when session is active\r\n",
    "Modification of destination UDP port is not allowed when session is active\r\n",
    "Modification of source UDP portis not allowed when session is active\r\n",
    "Base oid does not match\r\n",
    "Path does not exist\r\n",
    "Session does not exist\r\n",
    "Map Entry does not exist for the given discriminator\r\n",
    "Sessions not created\r\n",
    "Authentication flag should be enabled\r\n",
    "Valid Authentication type should be configured when Authentication is present\r\n",
    "Session type not supported for IP path monitoring\r\n",
    "Modification of Session Interface is not allowed when session is active\r\n",
    "Modification of Session Source address is not allowed when session is active\r\n",
    "Modification of Session Destination address is not allowed when session is active\r\n",
    "Modification of Path-Type/Sess-Type/Sess-Mode/Session-AdminStatus not allowed for the"
     " dynamically created entry\r\n",
    "Source address type/length did not match with destination address\r\n",
    "Already a BFD session is monitoring the same path\r\n",
    "Modification of session parameters is not allowed when session is ACTIVE\r\n",
 "Map Type/Map Pointer is the IP/Ipv6 address of connected interface\r\n",
 "Session cannot be created for Zero Network\n"
};
#else
extern CONST CHR1 *BfdCliErrString[]; 
#endif

#endif
