/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsipvxcli.h,v 1.1 2009/01/20 12:54:18 prabuc-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsIpvxAddrPrefixIfIndex[13];
extern UINT4 FsIpvxAddrPrefixAddrType[13];
extern UINT4 FsIpvxAddrPrefix[13];
extern UINT4 FsIpvxAddrPrefixLen[13];
extern UINT4 FsIpvxAddrPrefixProfileIndex[13];
extern UINT4 FsIpvxAddrPrefixSecAddrFlag[13];
extern UINT4 FsIpvxAddrPrefixRowStatus[13];
