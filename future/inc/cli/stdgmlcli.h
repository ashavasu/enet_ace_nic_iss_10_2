/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdgmlcli.h,v 1.1 2011/11/30 09:58:10 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 GmplsInterfaceSignalingCaps[13];
extern UINT4 GmplsInterfaceRsvpHelloPeriod[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetGmplsInterfaceSignalingCaps(i4MplsInterfaceIndex ,pSetValGmplsInterfaceSignalingCaps)	\
	nmhSetCmn(GmplsInterfaceSignalingCaps, 13, GmplsInterfaceSignalingCapsSet, NULL, NULL, 0, 0, 1, "%i %s", i4MplsInterfaceIndex ,pSetValGmplsInterfaceSignalingCaps)
#define nmhSetGmplsInterfaceRsvpHelloPeriod(i4MplsInterfaceIndex ,u4SetValGmplsInterfaceRsvpHelloPeriod)	\
	nmhSetCmn(GmplsInterfaceRsvpHelloPeriod, 13, GmplsInterfaceRsvpHelloPeriodSet, NULL, NULL, 0, 0, 1, "%i %u", i4MplsInterfaceIndex ,u4SetValGmplsInterfaceRsvpHelloPeriod)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 GmplsInSegmentDirection[13];
extern UINT4 GmplsInSegmentExtraParamsPtr[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetGmplsInSegmentDirection(pMplsInSegmentIndex ,i4SetValGmplsInSegmentDirection)	\
	nmhSetCmn(GmplsInSegmentDirection, 13, GmplsInSegmentDirectionSet, NULL, NULL, 0, 0, 1, "%s %i", pMplsInSegmentIndex ,i4SetValGmplsInSegmentDirection)
#define nmhSetGmplsInSegmentExtraParamsPtr(pMplsInSegmentIndex ,pSetValGmplsInSegmentExtraParamsPtr)	\
	nmhSetCmn(GmplsInSegmentExtraParamsPtr, 13, GmplsInSegmentExtraParamsPtrSet, NULL, NULL, 0, 0, 1, "%s %o", pMplsInSegmentIndex ,pSetValGmplsInSegmentExtraParamsPtr)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 GmplsOutSegmentDirection[13];
extern UINT4 GmplsOutSegmentTTLDecrement[13];
extern UINT4 GmplsOutSegmentExtraParamsPtr[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetGmplsOutSegmentDirection(pMplsOutSegmentIndex ,i4SetValGmplsOutSegmentDirection)	\
	nmhSetCmn(GmplsOutSegmentDirection, 13, GmplsOutSegmentDirectionSet, NULL, NULL, 0, 0, 1, "%s %i", pMplsOutSegmentIndex ,i4SetValGmplsOutSegmentDirection)
#define nmhSetGmplsOutSegmentTTLDecrement(pMplsOutSegmentIndex ,u4SetValGmplsOutSegmentTTLDecrement)	\
	nmhSetCmn(GmplsOutSegmentTTLDecrement, 13, GmplsOutSegmentTTLDecrementSet, NULL, NULL, 0, 0, 1, "%s %u", pMplsOutSegmentIndex ,u4SetValGmplsOutSegmentTTLDecrement)
#define nmhSetGmplsOutSegmentExtraParamsPtr(pMplsOutSegmentIndex ,pSetValGmplsOutSegmentExtraParamsPtr)	\
	nmhSetCmn(GmplsOutSegmentExtraParamsPtr, 13, GmplsOutSegmentExtraParamsPtrSet, NULL, NULL, 0, 0, 1, "%s %o", pMplsOutSegmentIndex ,pSetValGmplsOutSegmentExtraParamsPtr)

#endif
