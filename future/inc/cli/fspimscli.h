/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fspimscli.h,v 1.4 2016/09/30 10:55:14 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPimStdJoinPruneInterval[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPimStdJoinPruneInterval(i4SetValFsPimStdJoinPruneInterval) \
 nmhSetCmn(FsPimStdJoinPruneInterval, 11, FsPimStdJoinPruneIntervalSet, PimMutexLock, PimMutexUnLock, 0, 0, 0, "%i", i4SetValFsPimStdJoinPruneInterval)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPimStdInterfaceIfIndex[13];
extern UINT4 FsPimStdInterfaceAddrType[13];
extern UINT4 FsPimStdInterfaceMode[13];
extern UINT4 FsPimStdInterfaceHelloInterval[13];
extern UINT4 FsPimStdInterfaceStatus[13];
extern UINT4 FsPimStdInterfaceJoinPruneInterval[13];
extern UINT4 FsPimStdInterfaceCBSRPreference[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPimStdInterfaceMode(i4FsPimStdInterfaceIfIndex , i4FsPimStdInterfaceAddrType ,i4SetValFsPimStdInterfaceMode) \
 nmhSetCmn(FsPimStdInterfaceMode, 13, FsPimStdInterfaceModeSet, PimMutexLock, PimMutexUnLock, 0, 0, 2, "%i %i %i", i4FsPimStdInterfaceIfIndex , i4FsPimStdInterfaceAddrType ,i4SetValFsPimStdInterfaceMode)
#define nmhSetFsPimStdInterfaceHelloInterval(i4FsPimStdInterfaceIfIndex , i4FsPimStdInterfaceAddrType ,i4SetValFsPimStdInterfaceHelloInterval) \
 nmhSetCmn(FsPimStdInterfaceHelloInterval, 13, FsPimStdInterfaceHelloIntervalSet, PimMutexLock, PimMutexUnLock, 0, 0, 2, "%i %i %i", i4FsPimStdInterfaceIfIndex , i4FsPimStdInterfaceAddrType ,i4SetValFsPimStdInterfaceHelloInterval)
#define nmhSetFsPimStdInterfaceStatus(i4FsPimStdInterfaceIfIndex , i4FsPimStdInterfaceAddrType ,i4SetValFsPimStdInterfaceStatus) \
 nmhSetCmn(FsPimStdInterfaceStatus, 13, FsPimStdInterfaceStatusSet, PimMutexLock, PimMutexUnLock, 0, 1, 2, "%i %i %i", i4FsPimStdInterfaceIfIndex , i4FsPimStdInterfaceAddrType ,i4SetValFsPimStdInterfaceStatus)
#define nmhSetFsPimStdInterfaceJoinPruneInterval(i4FsPimStdInterfaceIfIndex , i4FsPimStdInterfaceAddrType ,i4SetValFsPimStdInterfaceJoinPruneInterval) \
 nmhSetCmn(FsPimStdInterfaceJoinPruneInterval, 13, FsPimStdInterfaceJoinPruneIntervalSet, PimMutexLock, PimMutexUnLock, 0, 0, 2, "%i %i %i", i4FsPimStdInterfaceIfIndex , i4FsPimStdInterfaceAddrType ,i4SetValFsPimStdInterfaceJoinPruneInterval)
#define nmhSetFsPimStdInterfaceCBSRPreference(i4FsPimStdInterfaceIfIndex , i4FsPimStdInterfaceAddrType ,i4SetValFsPimStdInterfaceCBSRPreference) \
 nmhSetCmn(FsPimStdInterfaceCBSRPreference, 13, FsPimStdInterfaceCBSRPreferenceSet, PimMutexLock, PimMutexUnLock, 0, 0, 2, "%i %i %i", i4FsPimStdInterfaceIfIndex , i4FsPimStdInterfaceAddrType ,i4SetValFsPimStdInterfaceCBSRPreference)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPimStdRPAddrType[13];
extern UINT4 FsPimStdRPGroupAddress[13];
extern UINT4 FsPimStdRPAddress[13];
extern UINT4 FsPimStdRPRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPimStdRPRowStatus(i4FsPimStdRPAddrType , pFsPimStdRPGroupAddress , pFsPimStdRPAddress ,i4SetValFsPimStdRPRowStatus) \
 nmhSetCmn(FsPimStdRPRowStatus, 13, FsPimStdRPRowStatusSet, PimMutexLock, PimMutexUnLock, 1, 1, 3, "%i %s %s %i", i4FsPimStdRPAddrType , pFsPimStdRPGroupAddress , pFsPimStdRPAddress ,i4SetValFsPimStdRPRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPimStdCandidateRPAddrType[13];
extern UINT4 FsPimStdCandidateRPGroupAddress[13];
extern UINT4 FsPimStdCandidateRPGroupMaskLen[13];
extern UINT4 FsPimStdCandidateRPAddress[13];
extern UINT4 FsPimStdCandidateRPRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPimStdCandidateRPAddress(i4FsPimStdCandidateRPAddrType , pFsPimStdCandidateRPGroupAddress , i4FsPimStdCandidateRPGroupMaskLen ,pSetValFsPimStdCandidateRPAddress) \
 nmhSetCmn(FsPimStdCandidateRPAddress, 13, FsPimStdCandidateRPAddressSet, PimMutexLock, PimMutexUnLock, 0, 0, 3, "%i %s %i %s", i4FsPimStdCandidateRPAddrType , pFsPimStdCandidateRPGroupAddress , i4FsPimStdCandidateRPGroupMaskLen ,pSetValFsPimStdCandidateRPAddress)
#define nmhSetFsPimStdCandidateRPRowStatus(i4FsPimStdCandidateRPAddrType , pFsPimStdCandidateRPGroupAddress , i4FsPimStdCandidateRPGroupMaskLen ,i4SetValFsPimStdCandidateRPRowStatus) \
 nmhSetCmn(FsPimStdCandidateRPRowStatus, 13, FsPimStdCandidateRPRowStatusSet, PimMutexLock, PimMutexUnLock, 0, 1, 3, "%i %s %i %i", i4FsPimStdCandidateRPAddrType , pFsPimStdCandidateRPGroupAddress , i4FsPimStdCandidateRPGroupMaskLen ,i4SetValFsPimStdCandidateRPRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPimStdComponentIndex[13];
extern UINT4 FsPimStdComponentScopeZoneName[13];
extern UINT4 FsPimStdComponentCRPHoldTime[13];
extern UINT4 FsPimStdComponentStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPimStdComponentCRPHoldTime(i4FsPimStdComponentIndex ,i4SetValFsPimStdComponentCRPHoldTime) \
 nmhSetCmn(FsPimStdComponentCRPHoldTime, 13, FsPimStdComponentCRPHoldTimeSet, PimMutexLock, PimMutexUnLock, 0, 0, 1, "%i %i", i4FsPimStdComponentIndex ,i4SetValFsPimStdComponentCRPHoldTime)
#define nmhSetFsPimStdComponentStatus(i4FsPimStdComponentIndex ,i4SetValFsPimStdComponentStatus) \
 nmhSetCmn(FsPimStdComponentStatus, 13, FsPimStdComponentStatusSet, PimMutexLock, PimMutexUnLock, 0, 1, 1, "%i %i", i4FsPimStdComponentIndex ,i4SetValFsPimStdComponentStatus)
#define nmhSetFsPimStdComponentScopeZoneName(i4FsPimStdComponentIndex ,pSetValFsPimStdComponentScopeZoneName) \
 nmhSetCmn(FsPimStdComponentScopeZoneName, 13, FsPimStdComponentScopeZoneNameSet, PimMutexLock, PimMutexUnLock, 0, 0, 1, "%i %s", i4FsPimStdComponentIndex ,pSetValFsPimStdComponentScopeZoneName)

#endif
