/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdpwmcli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 PwMplsMplsType[13];
extern UINT4 PwMplsExpBitsMode[13];
extern UINT4 PwMplsExpBits[13];
extern UINT4 PwMplsTtl[13];
extern UINT4 PwMplsLocalLdpID[13];
extern UINT4 PwMplsLocalLdpEntityID[13];
extern UINT4 PwMplsStorageType[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetPwMplsMplsType(u4PwIndex ,pSetValPwMplsMplsType)	\
	nmhSetCmn(PwMplsMplsType, 13, PwMplsMplsTypeSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %s", u4PwIndex ,pSetValPwMplsMplsType)
#define nmhSetPwMplsExpBitsMode(u4PwIndex ,i4SetValPwMplsExpBitsMode)	\
	nmhSetCmn(PwMplsExpBitsMode, 13, PwMplsExpBitsModeSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %i", u4PwIndex ,i4SetValPwMplsExpBitsMode)
#define nmhSetPwMplsExpBits(u4PwIndex ,u4SetValPwMplsExpBits)	\
	nmhSetCmn(PwMplsExpBits, 13, PwMplsExpBitsSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %u", u4PwIndex ,u4SetValPwMplsExpBits)
#define nmhSetPwMplsTtl(u4PwIndex ,u4SetValPwMplsTtl)	\
	nmhSetCmn(PwMplsTtl, 13, PwMplsTtlSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %u", u4PwIndex ,u4SetValPwMplsTtl)
#define nmhSetPwMplsLocalLdpID(u4PwIndex ,pSetValPwMplsLocalLdpID)	\
	nmhSetCmn(PwMplsLocalLdpID, 13, PwMplsLocalLdpIDSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %s", u4PwIndex ,pSetValPwMplsLocalLdpID)
#define nmhSetPwMplsLocalLdpEntityID(u4PwIndex ,pSetValPwMplsLocalLdpEntityID)	\
	nmhSetCmn(PwMplsLocalLdpEntityID, 13, PwMplsLocalLdpEntityIDSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %s", u4PwIndex ,pSetValPwMplsLocalLdpEntityID)
#define nmhSetPwMplsStorageType(u4PwIndex ,i4SetValPwMplsStorageType)	\
	nmhSetCmn(PwMplsStorageType, 13, PwMplsStorageTypeSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %i", u4PwIndex ,i4SetValPwMplsStorageType)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 PwMplsOutboundLsrXcIndex[13];
extern UINT4 PwMplsOutboundTunnelIndex[13];
extern UINT4 PwMplsOutboundTunnelLclLSR[13];
extern UINT4 PwMplsOutboundTunnelPeerLSR[13];
extern UINT4 PwMplsOutboundIfIndex[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetPwMplsOutboundLsrXcIndex(u4PwIndex ,pSetValPwMplsOutboundLsrXcIndex)	\
	nmhSetCmn(PwMplsOutboundLsrXcIndex, 13, PwMplsOutboundLsrXcIndexSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %s", u4PwIndex ,pSetValPwMplsOutboundLsrXcIndex)
#define nmhSetPwMplsOutboundTunnelIndex(u4PwIndex ,u4SetValPwMplsOutboundTunnelIndex)	\
	nmhSetCmn(PwMplsOutboundTunnelIndex, 13, PwMplsOutboundTunnelIndexSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %u", u4PwIndex ,u4SetValPwMplsOutboundTunnelIndex)
#define nmhSetPwMplsOutboundTunnelLclLSR(u4PwIndex ,pSetValPwMplsOutboundTunnelLclLSR)	\
	nmhSetCmn(PwMplsOutboundTunnelLclLSR, 13, PwMplsOutboundTunnelLclLSRSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %s", u4PwIndex ,pSetValPwMplsOutboundTunnelLclLSR)
#define nmhSetPwMplsOutboundTunnelPeerLSR(u4PwIndex ,pSetValPwMplsOutboundTunnelPeerLSR)	\
	nmhSetCmn(PwMplsOutboundTunnelPeerLSR, 13, PwMplsOutboundTunnelPeerLSRSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %s", u4PwIndex ,pSetValPwMplsOutboundTunnelPeerLSR)
#define nmhSetPwMplsOutboundIfIndex(u4PwIndex ,i4SetValPwMplsOutboundIfIndex)	\
	nmhSetCmn(PwMplsOutboundIfIndex, 13, PwMplsOutboundIfIndexSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %i", u4PwIndex ,i4SetValPwMplsOutboundIfIndex)

#endif
