/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fseoamcli.h,v 1.2 2008/10/31 13:09:30 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsEoamSystemControl[10];
extern UINT4 FsEoamModuleStatus[10];
extern UINT4 FsEoamErrorEventResend[10];
extern UINT4 FsEoamOui[10];
extern UINT4 FsEoamTraceOption[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsEoamSystemControl(i4SetValFsEoamSystemControl)	\
	nmhSetCmn(FsEoamSystemControl, 10, FsEoamSystemControlSet, EoamLock, EoamUnLock, 0, 0, 0, "%i", i4SetValFsEoamSystemControl)
#define nmhSetFsEoamModuleStatus(i4SetValFsEoamModuleStatus)	\
	nmhSetCmn(FsEoamModuleStatus, 10, FsEoamModuleStatusSet, EoamLock, EoamUnLock, 0, 0, 0, "%i", i4SetValFsEoamModuleStatus)
#define nmhSetFsEoamErrorEventResend(u4SetValFsEoamErrorEventResend)	\
	nmhSetCmn(FsEoamErrorEventResend, 10, FsEoamErrorEventResendSet, EoamLock, EoamUnLock, 0, 0, 0, "%u", u4SetValFsEoamErrorEventResend)
#define nmhSetFsEoamOui(pSetValFsEoamOui)	\
	nmhSetCmn(FsEoamOui, 10, FsEoamOuiSet, EoamLock, EoamUnLock, 0, 0, 0, "%s", pSetValFsEoamOui)
#define nmhSetFsEoamTraceOption(i4SetValFsEoamTraceOption)	\
	nmhSetCmn(FsEoamTraceOption, 10, FsEoamTraceOptionSet, EoamLock, EoamUnLock, 0, 0, 0, "%i", i4SetValFsEoamTraceOption)

#endif
