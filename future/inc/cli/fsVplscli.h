/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsVplscli.h,v 1.1 2014/03/09 13:30:19 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 VplsBgpConfigVERangeSize[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetVplsBgpConfigVERangeSize(u4VplsConfigIndex ,u4SetValVplsBgpConfigVERangeSize)	\
	nmhSetCmnWithLock(VplsBgpConfigVERangeSize, 13, VplsBgpConfigVERangeSizeSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %u", u4VplsConfigIndex ,u4SetValVplsBgpConfigVERangeSize)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 VplsBgpVEId[13];
extern UINT4 VplsBgpVEName[13];
extern UINT4 VplsBgpVEPreference[13];
extern UINT4 VplsBgpVERowStatus[13];
extern UINT4 VplsBgpVEStorageType[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetVplsBgpVEName(u4VplsConfigIndex , u4VplsBgpVEId ,pSetValVplsBgpVEName)	\
	nmhSetCmnWithLock(VplsBgpVEName, 13, VplsBgpVENameSet, L2vpnLock, L2vpnUnLock, 0, 0, 2, "%u %u %s", u4VplsConfigIndex , u4VplsBgpVEId ,pSetValVplsBgpVEName)
#define nmhSetVplsBgpVEPreference(u4VplsConfigIndex , u4VplsBgpVEId ,u4SetValVplsBgpVEPreference)	\
	nmhSetCmnWithLock(VplsBgpVEPreference, 13, VplsBgpVEPreferenceSet, L2vpnLock, L2vpnUnLock, 0, 0, 2, "%u %u %u", u4VplsConfigIndex , u4VplsBgpVEId ,u4SetValVplsBgpVEPreference)
#define nmhSetVplsBgpVERowStatus(u4VplsConfigIndex , u4VplsBgpVEId ,i4SetValVplsBgpVERowStatus)	\
	nmhSetCmnWithLock(VplsBgpVERowStatus, 13, VplsBgpVERowStatusSet, L2vpnLock, L2vpnUnLock, 0, 1, 2, "%u %u %i", u4VplsConfigIndex , u4VplsBgpVEId ,i4SetValVplsBgpVERowStatus)
#define nmhSetVplsBgpVEStorageType(u4VplsConfigIndex , u4VplsBgpVEId ,i4SetValVplsBgpVEStorageType)	\
	nmhSetCmnWithLock(VplsBgpVEStorageType, 13, VplsBgpVEStorageTypeSet, L2vpnLock, L2vpnUnLock, 0, 0, 2, "%u %u %i", u4VplsConfigIndex , u4VplsBgpVEId ,i4SetValVplsBgpVEStorageType)

#endif
