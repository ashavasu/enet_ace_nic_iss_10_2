/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ip6cli.h,v 1.60 2017/12/19 13:41:54 siva Exp $
 *
 * Description: Cli related functions are handled here.
 *
 *******************************************************************/

#ifndef __IP6CLI_H__
#define __IP6CLI_H__

#include "cli.h"

VOID cli_process_ip6_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command, ...));
VOID cli_process_rtm6_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command, ...));
    
/* command identification */
enum {
        IP6_CLI_ROUTING = 1,
        IP6_CLI_INTERFACE,
        IP6_ADDR,
        IP6_INTERFACE_ID,
        IP6_CLI_PING,
        IP6_ADDR_SHOW,
        IP6_RT_SHOW,
        IP6_IF_SHOW,
        IP6_CLI_ROUTE_ADD,
        IP6_CLI_ROUTE_DEL,
        IP6_ND6_SHOW,
        IP6_ENABLE_RT_ADV_STATUS,
        IP6_ENABLE_RT_ADV_RDNSS,
        IP6_DISABLE_RT_ADV_RDNSS,
        IP6_ENABLE_RT_ADV_RDNSS_STATUS,
        IP6_ENABLE_RT_ADV_LINKLOCAL,
        IP6_ENABLE_RT_ADV_INTERVAL,
        IP6_RA_RDNSS_PREFERENCE,
        IP6_RA_RDNSS_LIFETIME,
        IP6_ENABLE_RT_ADV_FLAG,
        IP6_HOP_LIMIT,
        IP6_DEF_ROUTER_LIFE_TIME,
        IP6_PREFIX_ADV_STATUS,
        IP6_DAD_RETRIES,
        IP6_IF_STATS_SHOW,
        IP6_TRAFFIC_SHOW,
        IP6_REACHABLE_TIME,
        IP6_ROUTEADV_INTERVAL,
        IP6_ADDR_DEL,
        IP6_INTERFACE_ID_DEL,
        IP6_CLI_TRACE,
        IP6_DEFAULT_ROUTE_SHOW,
        IP6_PROTO_PREFERENC,
        IP6_RETRANS_TIME,
        IP6_RA_LINK_MTU,
        IP6_PREFIX_LIST,
        IP6_PREFIX_LIST_DEL,
        IP6_PREFIX_CONFIG,
        IP6_PREFIX_CONFIG_OPT,
        IP6_PREFIX_CONFIG_DEL,
        IP6_PREFIX_SHOW,
        IP6_ND6_CACHE_ADD,
        IP6_ND6_CACHE_DEL,
        IP6_TRACEROUTE,
        PING6_UNSECURED,
        IP6_UNITUNL_SRC,
        IP6_NO_UNITUNL_SRC,
 IP6_CLEAR_TRAFFIC,
 IP6_CLEAR_ROUTE,
 IP6_CLEAR_NEIGHBORS,
 IP6_RT_SUMMARY_SHOW,
 IP6_RT_FAIL_SHOW,
        IP6_DEFAULT_HOP_LIMIT,
        IP6_RT_ADVERT_LINK_MTU,
        IP6_RT_ADVERT_RETX_TIME,
        IP6_PATH_MTU_DISCOVER,
        IP6_NO_PATH_MTU_DISCOVER,
        IP6_PATH_MTU_ADD,
        IP6_PATH_MTU_DEL,
        IP6_CLI_INTF_ROUTING,
        IP6_CLI_RFC5095COMPAT,
        IP6_CLI_NO_RFC5095COMPAT,
        IP6_CLI_RFC5942COMPAT,
        IP6_CLI_NO_RFC5942COMPAT,
        IP6_SHOW_PMTU,
        IP6_POLICY_PREFIX_CONFIG,
        IP6_POLICY_PREFIX_DELETE,
        IP6_SHOW_ADDR_SEL_POLICY,
        IP6_ZONE_CONFIG,   /*RFC 4007 Zone config added for Scoped
                            address architecture */
        IP6_ZONE_DEL,
        IP6_DEF_ZONE_CONFIG,
        IP6_SHOW_INT_SCOPE_ZONE,
        IP6_SHOW_ZONE_INTERFACE,
        IP6_SHOW_DEF_SCOPE_ZONE,
        IP6_ERROR_CONTROL_ENABLE,
        IP6_ERROR_CONTROL_DISABLE,
        IP6_DEST_UNREACHABLE_DISABLE,
        IP6_DEST_UNREACHABLE_ENABLE,
        IP6_UNNUM_INDEX,
        IP6_NO_UNNUM_INDEX,
        IP6_ICMP_REDIRECT_ENABLE,
        IP6_ICMP_REDIRECT_DISABLE,
        IP6_ND_PROXY_ENABLE,
        IP6_ND_PROXY_DISABLE,
        IP6_ND_LOCAL_PROXY_ENABLE,
        IP6_ND_LOCAL_PROXY_DISABLE,
        IP6_ND_PROXY_UPSTREAM,
        IP6_ND_PROXY_DOWNSTREAM,
  IP6_HOST_PING,
        IP6_ND6_SUMMARY_SHOW,
        IP6_RA_DNS_DOMNAME_LIFETIME,
        IP6_RA_NO_DNS_DOMNAME_LIFETIME,
        IP6_RA_DNS_LIFETIME,
 IP6_RA_DNS_NO_LIFETIME,
        IP6_ND_SEND,
        IP6_ND_NO_SEND,
        IP6_CLI_ADDR_CGA,
        IP6_CLI_ADDR_LINKLOCAL_CGA,
        IP6_SEND_GEN_SEC,
        IP6_SEND_NBR_SEC,
        RSA_BITS512,
        RSA_BITS1024,
        IP6_SEND_MIN_KEY,
        IP6_SEND_PRF_ENABLE,
        IP6_SEND_PRF_DISABLE,
        IP6_SEND_ENABLE,
        IP6_SEND_MIXED,
        IP6_SEND_UNSECURE,
        IP6_SEND_TIME_DELTA,
        IP6_SEND_TIME_FUZZ,
        IP6_SEND_TIME_DRIFT,
        IP6_SHOW_SEND_CGA,
        IP6_SHOW_SEND_TIMESTAMP,
        IP6_SHOW_SEND_NONCE,
        IP6_SEND_UNSEC_ADV_ENABLE,
        IP6_SEND_UNSEC_ADV_DISABLE,
        IP6_SEND_AUTH_CGA,
        IP6_SEND_AUTH_TA,
        IP6_SEND_AUTH_BOTH,
        IP6_SEND_AUTH_ANYONE,
        IP6_SHOW_SEND,
        IP6_SEND_SHOW_CGA,
        IP6_SEND_SHOW_TIME,
        IP6_SEND_SHOW_NONCE,
        IP6_SEND_SHOW_STAT,
        IP6_CLEAR_SPC_NEIGHBORS,
        IP6_ND_DEFAULT_ROUT_PREF,
        IP6_ROUTE_INFO_CONFIG,
        IP6_DEL_ROUTE_CONFIG,
        IP6_RA_ROUTE_SHOW,
        IP6_ECMP_TIMER,
        IP6_NO_ECMP_TIMER,
        CLI_IP6_ROUTE_DEF_DISTANCE,
        CLI_IP6_SHOW_DEF_DISTANCE,
        IP6_ND_TIMEOUT,
        IP6_NO_ND_TIMEOUT
};

enum {
       CLI_IP6_INVALID_ADDRESS = 1,
       CLI_IP6_INVALID_PREFIXLEN,
       CLI_IP6_INVALID_NEXTHOP,
       CLI_IP6_INVALID_INTERFACE,
       CLI_IP6_INVALID_INTERFACE_FOR_NEXTHOP,
       CLI_IP6_INVALID_FORWARDING_STATUS,
       CLI_IP6_INVALID_ADMIN_STATUS,
       CLI_IP6_ADDR_ALREADY_EXISTS,
       CLI_IP6_ADDR_NOT_FOUND,
       CLI_IP6_INCOMPATIBLE_ADDR,
       CLI_IP6_INVALID_ADDR_TYPE,
       CLI_IP6_INVALID_PING_INDEX,
       CLI_IP6_INVALID_PING_ADMIN_STATUS,
       CLI_IP6_INVALID_PING_DEST,
       CLI_IP6_PING_IN_PROGRESS,
       CLI_IP6_INVALID_PING_TRIES,
       CLI_IP6_INVALID_PING_TIMEOUT,
       CLI_IP6_INVALID_PING_SIZE,
       CLI_IP6_INVALID_PING_DATA,
       CLI_IP6_INVALID_RT_DNS_LIFETIME,
       CLI_IP6_INVALID_PING_SOURCE,
       CLI_IP6_NEXTHOP_UNREACHABLE,
       CLI_IP6_ROUTE_ALREADY_PRESENT,
       CLI_IP6_ROUTE_NOT_FOUND,
       CLI_IP6_INVALID_ROUTE_PROTO,
       CLI_IP6_INVALID_ROUTE_METRIC,
       CLI_IP6_NO_NEIGH_ENTRIES,
       CLI_IP6_INVALID_RT_ADV_STATUS,
       CLI_IP6_RT_ADV_RDNSS_ADDR,
       CLI_IP6_RT_ADV_MAX_RDNSS,
       CLI_IP6_INVALID_RT_ADV_RDNSS_ADDR,
       CLI_IP6_INVALID_RT_ADV_DNS_ADDR,
       CLI_IP6_INVALID_RT_ADV_RDNSS_STATUS,
       CLI_IP6_INVALID_RT_ADV_RDNSS_PREFERENCE, 
       CLI_IP6_SUPPRESSRA_IN_PROGRESS, 
       CLI_IP6_INVALID_RT_ADV_RDNSS_LIFETIME,
       CLI_IP6_INVALID_RT_ADV_FLAGS,
       CLI_IP6_INVALID_HOPLIMIT,
       CLI_IP6_INVALID_RA_LIFETIME,
       CLI_IP6_INVALID_DAD_RETRIES,
       CLI_IP6_INVALID_RA_REACHTIME,
       CLI_IP6_INVALID_RA_INTERVAL,
       CLI_IP6_INVALID_MIN_RA_INTERVAL,
       CLI_IP6_VAL_LT_PREF_LIFETIME,
       CLI_IP6_INVALID_PROF_INDEX,
       CLI_IP6_INVALID_PROF_STATUS,
       CLI_IP6_PROFILE_IN_USE,
       CLI_IP6_INVALID_PREFIX_ADV_STATUS,
    CLI_IP6_PROF_ONLINK_ADV_STATUS,
    CLI_IP6_PROF_ONLINK_ADV_INVALID,
       CLI_IP6_AUTOCONF_ADV_STATUS,
       CLI_IP6_INV_VAL_LIFETIME_FLAG,
       CLI_IP6_INV_PREF_LIFETIME_FLAG,
       CLI_IP6_VALID_NOT_FIXED,
       CLI_IP6_DEF_PROF_INDEX,
       CLI_IP6_INVALID_PREF_ADMIN_STATUS,
       CLI_IP6_PREFIX_ALREADY_EXISTS,
       CLI_IP6_PREFIX_NOT_FOUND,
       CLI_IP6_PROFILE_NOT_FOUND,
       CLI_IP6_ND_OUR_ADDR,
       CLI_IP6_ND_LAN_CACHE_STATUS,
       CLI_IP6_INVALID_PHYS_ADDR,
       CLI_IP6_CLEARED_ND_TABLE,
       CLI_IP6_CLEARED_RT_TABLE,
       CLI_IP6_NO_ROUTES,
       CLI_IP6_DISABLE_RT_PROTOCOLS,
       CLI_IP6_INTERFACE_NOT_DISABLED,
       CLI_IP6_ADDR_NOT_CONFIGURED,
       CLI_IP6_INVALID_RA_RETTIME,
       CLI_IP6_INVALID_RA_LINK_MTU,
       CLI_IP6_INCONST_RA_ROWSTATUS,
       CLI_IP6_INVALID_CONTEXT,
       CLI_IP6_PMTUD_FAIL,
       CLI_IP6_INVALID_PMTU,
       CLI_IP6_ROUTING_DISABLED,
       CLI_IP6_INVALID_EMBEDDED_RP,
       CLI_IP6_INVALID_INTERFACE_ID,
       CLI_IP6_INVALID_PRECEDENCE,
       CLI_IP6_INVALID_LABEL,
       CLI_IP6_POLICY_PREFIX_ALREADY_EXISTS,
       CLI_IP6_POLICY_PREFIX_NOT_FOUND,
       CLI_IP6_INVALID_ZONE_INDEX,  /*Macro Added for RFC4007*/
       CLI_IP6_INVALID_ZONE_LEN,
       CLI_IP6_INVALID_ZONE_ID,
       CLI_IP6_INVALID_ZONE_NAME,
       CLI_IP6_ZONE_ALREADY_EXIST,
       CLI_IP6_ZONE_DOES_NOT_EXIST,
       CLI_IP6_INVALID_SCOPE,
       CLI_IP6_INVALID_DEF_ZONE_VAL,
       CLI_IP6_INVALID_AUTOCONF_ZONE_DEL,
       CLI_IP6_VIOLATION_OF_ZONE_INTEGRITY,
       CLI_IP6_INVALID_FORMAT_FOR_GLOBAL_ADDR,
       CLI_IP6_MISMATCH_OF_INTERFACE,
       CLI_IP6_MAX_ZONES_CONFIGURED,
       CLI_IP6_INVALID_ROW_STATUS_FOR_ZONE,
       CLI_IP6_FWD_DIS_RA_CANT_ALLOW,
       CLI_IP6_ADDR_PRESENT_FOR_UNNUM,
       CLI_IP6_UNNUM_PRESENT_FOR_ADDR,
       CLI_IP6_INVALID_UNNUM_ASSOC_IF,
       CLI_IP6_INVALID_UNNUM_INDEX_TYPE,
       CLI_IP6_DEL_UNNUMBERED_ADDR,
       CLI_IP6_NO_UNNUM_INDEX_ADDR,
       CLI_IP6_ADDR_ENTRY_NOT_CONFIGURED,
       CLI_IP6_INVALID_NDPROXY_STATUS,
       CLI_IP6_INVALID_NDPROXY_MODE,
       CLI_IP6_INVALID_NDPROXY_UPSTREAM,
       CLI_IP6_IP_NO_CHG_VRRP_INUSE,
       CLI_IP6_LOOPBACK_UNNUM_NOT_ALLOWED,
    CLI_IP6_DHCP_PD_ROUTE,
       CLI_IP6_INVALID_SEND_MODE,
       CLI_IP6_SEND_INVALID_DELTA_TIME,
       CLI_IP6_SEND_INVALID_FUZZ_TIME,
       CLI_IP6_SEND_INVALID_DRIFT_TIME,
       CLI_IP6_SEND_RSA_ERROR,
       CLI_IP6_SEND_NOT_ENABLE,
       CLI_IP6_SEND_USE_CGA,
       CLI_IP6_SEND_CONFIG_SEC_LEVEL,
       CLI_IP6_SEND_CONFIG_NBR_SEC_LEVEL,
       CLI_IP6_PROXY_SEND_DISABLE,
       CLI_IP6_SEND_PROXY_DISABLE,
       CLI_IP6_SEND_INVALID_SEC,
       CLI_IP6_SEND_INVALID_NBR_SEC,
       CLI_IP6_SEND_INVALID_KEY_LEN,
       CLI_IP6_SEND_REMOVE_CONF_ADDR,
       CLI_IP6_SEND_AUTH_TYPE_NOT_ALLOWED,
       CLI_IP6_SEND_LL_ADDR_DEL_NOT_ALLOWED,
       CLI_IP6_INVALID_RA_ROUTE_PREFERENCE,
       CLI_IP6_INVALID_RA_ROUTE_LIFETIME,
       CLI_IP6_INVALID_DEF_ROUT_PREF,
       CLI_IP6_INVALID_RA_ROWSTATUS,
       CLI_IP6_RA_ROUTE_ALREADY_CONFIGD,
       CLI_IP6_RA_ROUTE_INFO_AVAIL_FOR_PREFIX,
       CLI_IP6_PREFIX_ALREADY_CONFIG_FOR_RAINFO,
       CLI_IP6_MAX_RAROUTE_INFO_REACHD,
       CLI_IP6_NOT_A_VALID_PREFIX,
       CLI_IP6_DEF_ROUTE_ERR,
       CLI_IP6_PREFIX_LEN_ERR,
       CLI_IP6_RA_INFO_NOT_PRESENT,
       CLI_IP6_MAX_ROUTE,
       CLI_IP6_MEM_UNAVAILABLE,
       CLI_IP6_DISTANCE_SET_ERR,
       CLI_IP6_DISTANCE_GET_ERR,
       CLI_IP6_TOUT_ERR,
       CLI_IP6_PORT_IN_AGG,
       CLI_IP6_L3SUB_IF_DOT1Q_VLAN_DISABLED,
       CLI_IP6_MAX_ERR
};
#ifdef __IP6CLI_C__
CONST CHR1 *Ip6CliErrString[] = {
       NULL,
       "% Invalid IPv6 address\r\n",
       "% Invalid prefix length\r\n",
       "% Invalid nextHop address\r\n",
       "% Invalid / Uninitialized interface\r\n",
       "% No valid interface exists for the given NextHop\r\n",
       "% Invalid forwarding status\r\n",
       "% Invalid interface admin status\r\n",
       "% Address/Prefix already exists\r\n",
       "% Address not found\r\n",
       "% Incompatible address type\r\n",
       "% Invalid address type\r\n",
       "% Invalid ping index\r\n",
       "% Invalid ping admin status\n",
       "% Invalid ping destination\r\n",
       "% Ping in progress\r\n",
       "% Invalid ping tries value\r\n",
       "% Invalid ping timeout\r\n",
       "% Invalid ping size\r\n",
       "% Invalid ping data\r\n",
       "% AdvDNSSLLifetime must be at least MaxRtrAdvInterval \r\n",
       "% Invalid ping source address\r\n",
       "% Nexthop is not reachable\r\n",
       "% Route is already present\r\n",
       "% Route not found\r\n",
       "% Invalid Route protocol\r\n",
       "% Invalid Route metric\r\n",
       "% No Neighbor entries in the table\r\n",
       "% Invalid Router advertisement status\r\n",
       "% Duplicate RDNSS address\r\n",
       "% MAX Server address is already configured\r\n",
       "% RDNSS server address is not present\r\n",
       "% DNS server address is not present\r\n",
       "% Invalid Router advertisement RDNSS status\r\n",
       "% Invalid Router advertisement RDNSS Preference\r\n",
       "% Suppress-ra is in progress. Disable suppress-ra to proceed this operation\r\n",
       "% rdnss-lifetime value should be greater than or equal to ra-interval value\r\n",
       "% Invalid Router advertisement flags\r\n",
       "% Invalid Hop Limit value\r\n",
       "% ra-lifetime value should be greater than or equal to ra-interval value\r\n",
       "% Invalid DAD retres value\r\n",
       "% Invalid router reachable time\r\n",
       "% Invalid router advertisement interval\r\n",
       "% Minimum ra-interval should not be greater than 3/4 of Max ra-interal\r\n",
       "% Valid lifetime can't be lesser than preferred lifetime\r\n",
       "% Profile table is full, can't create a profile\r\n",
       "% Invalid address profile status\r\n",
       "% Profile in use, can't be deleted\r\n",
       "% Invalid prefix advertisement status\r\n",
  "% Invalid router advertisement onlink advertisement status\r\n",
  "% Cannot set router advertisement onlink advertisement status when rfc5942 is disabled\r\n",
       "% Invalid router advertisement auto configuration status\r\n",
       "% Invalid Valid lifetime flag value\r\n",
       "% Invalid Preferred lifetime flag value\r\n",
       "% Valid lifetime has to be fixed before setting preferred lifetime to fixed\r\n",
       "% Can not delete the default profile index\r\n",
       "% Invalid prefix admin status\r\n",
       "% Prefix already exists\r\n",
       "% Prefix not found\r\n",
       "% Profile not found\r\n",
       "% Can not add a neighbor entry for my own address\r\n",
       "% Invalid ND Lan Cache Status\r\n",
       "% Invalid link layer address\r\n",
       "  Cleared neighbor entries\r\n",
       "  Cleared route entries\r\n",
       "  No Route entries in the table\r\n",
       "  Ensure to disable all the IPv6 routing protocols\r\n",
       "% Interface not disabled - Unicast Address configured\r\n",
       "% Address not configured - Interface Status Down\r\n",
       "% Invalid retransmit time\r\n",
       "% Invalid router advertisement link MTU\r\n",
       "% Router advertisement Entry Exist\r\n",
       "% Invalid VR Id\r\n",
       "% Unable to set PMTU Discovery status\r\n",
       "% Unable to set Path MTU value\r\n",
       "% WARNING Ensure to disable all the IPv6 routing protocols\r\n",
       "% Invalid option for embedded rp\r\n",
       "% Invalid interface identifier\r\n",
       "% Invalid precedence\r\n",
       "% Invalid label\r\n",
       "% Prefix already exists in the policy table\r\n",
       "% Prefix not found in the policy table \r\n",
       "% Invalid scope-zone Index\r\n",
       "% Invalid scope-zone Name Len\r\n",
       "% Invalid scope-zone ID \r\n",
       "% Invalid scope-zone name \r\n",
       "% Scope-Zone already exist \r\n",
       "% Scope-Zone does not exist \r\n",
       "% Invalid value for scope \r\n",
       "% Invalid Default scope-zone Val \r\n",
       "% Cannot Delete the auto configured scope-zone\r\n",
       "% Zone Integrity violated for other zones on this interface \r\n",
       "% <Addr%Interface> Invalid Address Format for Global Address \r\n",
       "% Mismatch of interface in <Addr%Interface> and the source interface specified\r\n",
       "% Max Zones configured already\r\n",
       "% Enable IPv6 on this interface before configuring scope-zone \r\n",
       "% IPV6 Forwarding disabled : Router advertisement related configurations cannot be accepted\r\n",
       "% Cannot configure unnumbered interface if IPv6 address is configured \r\n",
       "% Cannot configure IPv6 address if unnumbered interface is configured \r\n",
       "% Invalid / Uninitialized interface for associating an unnumbered interface \r\n",
       "% Invalid index type for unnumbered interface \r\n",
       "% Cannot delete the IPV6 address configured as interface is mapped to a unnumbered interface \r\n",
       "% IPv6 address not configured for unnumbered interface \r\n",
       "% IPv6 Address specified is not configured already \r\n",
       "% Invalid Admin status for Proxy interface \r\n",
       "% Invalid Mode for Proxy interface \r\n",
       "% Invalid Upstream status for Proxy interface \r\n",
       "% Cannot Add / Delete / Modify IP. VRRP is using it and this affects its working\r\n",
       "% Loopback interface cannot be configured as unnumbered interface\r\n",
    "% Deletion of static route configured via DHCP6-PD is not allowed\r\n",
       "% Invalid SeND status for interface \r\n",
       "% Invalid SeND Delta Timestamp Value \r\n",
       "% Invalid SeND Fuzz Timestamp Value \r\n",
       "% Invalid SeND Drift Timestamp Value \r\n",
       "% RSA Key should be configured before enabling Secure Neighbor Discovery and CGA address generation\r\n",
       "% Enable SeND in either secured or mixed mode to configure CGA Address \r\n",
       "% CGA token should present with IPv6 address as Secure Neighbor Discovery is enabled \r\n",
       "% Sec level should be configured before CGA Generation \r\n",
       "% Acceptable neighbor sec level should be configured before enabling SeND \r\n",
       "% Disable SeND to enable ND Proxy \r\n",
       "% Disable Proxy to enable SeND \r\n",
       "% Acceptable security levels are 0 and 1 \r\n",
       "% Acceptable security level for neighbors should be less than 7 \r\n",
       "% Invalid minimum key length \r\n",
       "% Remove all configured unsecured addresses to enable Secure ND. \r\n",
       "% CGA alone is supported for authorization method. \r\n",
       "% Link-local address cannot be deleted when SeND is enabled. \r\n",
       "% Invalid RA Route Information Preference value. \r\n",
       "% Invalid RA Route Information Lifetime value. \r\n",
       "% Invalid RA Default Router Preference value. \r\n",
       "% Invalid RA Row status value. \r\n",
       "% RA Route information already configured. \r\n",
       "% RA Route Information already configured for the given prefix. \r\n",
       "% Prefix already configured for the given RA Route Prefix. \r\n",
       "% Maximum of 17 route information can be configured. \r\n",
       "% Not a valid Prefix. \r\n",
       "% For default route ::/0 prefix length should be zero. \r\n",
       "% For default route ::/0 alone prefix length can be zero. \r\n",
       "% RA Route Information not present. \r\n",
       "% Exceeded maximum routes allowed\r\n",
       "% Memory Unavailable \r\n",
       "% Default Administrative Distance value set failed\r\n",
       "% Default Administrative Distance value get failed\r\n",
       "% IPv6 Nd Cache Timeout value cannot be set \r\n",
       "% IPv6 Capabilities are disabled ,port is a part of Port-channel\r\n",
       "% IPv6 Address configuration on L3 sub-interface is only allowed if that subinterface is already configured as part of an IEEE 802.1Q\r\n"
};
#endif
enum {
CLI_ADDR6_SCOPE_RESERVED0,
CLI_ADDR6_SCOPE_INTLOCAL,
CLI_ADDR6_SCOPE_LLOCAL,
CLI_ADDR6_SCOPE_SUBNETLOCAL,
CLI_ADDR6_SCOPE_ADMINLOCAL,
CLI_ADDR6_SCOPE_SITELOCAL,
CLI_ADDR6_SCOPE_UNASSIGN6,
CLI_ADDR6_SCOPE_UNASSIGN7,
CLI_ADDR6_SCOPE_ORGLOCAL,
CLI_ADDR6_SCOPE_UNASSIGN9,
CLI_ADDR6_SCOPE_UNASSIGNA,
CLI_ADDR6_SCOPE_UNASSIGNB,
CLI_ADDR6_SCOPE_UNASSIGNC,
CLI_ADDR6_SCOPE_UNASSIGND,
CLI_ADDR6_SCOPE_GLOBAL,
CLI_ADDR6_SCOPE_RESERVEDF,
CLI_ADDR6_SCOPE_INVALID
};

#define  IP6_IF_RA_RDNSS_DEF_LIFETIME   1200
#define  IP6_IF_RA_RDNSS_MIN_LIFETIME   0
#define  IP6_IF_RA_RDNSS_MAX_LIFETIME   0xffffffff
#define  IP6_IF_MIN_REACHTIME           0                /* In MSec. */
#define  IP6_IF_MAX_REACHTIME           3600000          /* In MSec. */
 
#define IP6_ROUTING_ENABLE  "1"
#define IP6_ROUTING_DISABLE "2"
#define IP6_CLI_IF_ROUTING_ENABLE "1"
#define IP6_CLI_IF_ROUTING_DISABLE "2"

#define IP6_CLI_IF_UP       "1"
#define IP6_CLI_IF_DISABLE  "2"
#define IP6_CLI_IF_DELETE   "3"
#define IP6_CLI_IF_CREATE   "4"
#define IP6_ENABLE_IF       "1"
#define IP6_DISABLE_IF      "2"

#define IP6_CLI_ADDR_ANY        0     /* No Address Type Specified */
#define IP6_CLI_ADDR_UNICAST    1     /* MAP it to IP6_ADDR_TYPE_UNICAST */
#define IP6_CLI_ADDR_ANYCAST    2     /* MAP it to IP6_ADDR_TYPE_ANYCAST */
#define IP6_CLI_ADDR_LINKLOCAL  3     /* MAP it to IP6_ADDR_TYPE_LINK_LOCAL */
#define IP6_CLI_ADDR_EUI64      4     /* MAP it to ADDR6_UNSPECIFIED */

/* Default Values for Ping Table */
#define CLI_PING6_DEFAULT_COUNT  5
#define CLI_PING6_DEFAULT_TIMEOUT 1
#define CLI_PING6_DEFAULT_WAIT   1  /* interval between successive pings */
#define CLI_PING6_DEFAULT_SIZE   100

#define IP6_PING_DUMMY_EVT   25

#define PING6_ENABLE  IP6_SUCCESS
#define PING6_DISABLE IP6_FAILURE

#define IP6_CLI_MAX_COMMANDS 36

#define  IP6_CLI_MOD_TRC      1    /* to be substitued for cmod argument */
#define  ICMP6_CLI_MOD_TRC    2 
#define  UDP6_CLI_MOD_TRC     3 
#define  ND6_CLI_MOD_TRC      4 
#define  PING6_CLI_MOD_TRC    5 
#define  V6_OVER_V4_CLI_MOD_TRC 6 
#define  PACKET_CLI_MOD_TRC     7
#define  MIP6_CLI_MOD_TRC     9
#define  IP6_CLI_NO_MOD_TRC    8 

/* IP6  size Constants */
#define IP6_MAX_PREFIX_STR_LEN 4
#define IP6_MAX_ADDR_STR_LEN (40 + IP6_MAX_PREFIX_STR_LEN)

/*CONSTANTS FOR 0,1 and -1*/
#define  IP6_MINUS_ONE         -1
#define  IP6_ZERO               0
#define  IP6_ONE                1

#define  IP6_CLI_HC_ENABLED     1
#define  IP6_CLI_HC_DISABLED    0


/* SIZE CONSTANTS */

#define MAX_ADDR_BUFFER                   256
/* 5 - maximum number of row to display a addr information */

#define IP6_CLI_ADDR_SIZE      (5 * CLI_MAX_COLUMN_WIDTH) + \
                                CLI_MAX_HEADER_SIZE
/* 7 - maximum number of row to display a route information */

#define IP6_CLI_ROUTE_SIZE     (7 * CLI_MAX_COLUMN_WIDTH) + \
                                CLI_MAX_HEADER_SIZE

#define IP6_CLI_IFSTATS_SIZE   (20 * CLI_MAX_COLUMN_WIDTH) + \
                                CLI_MAX_HEADER_SIZE

#define IP6_CLI_ND6_SIZE       (4 * CLI_MAX_COLUMN_WIDTH) + \
                                CLI_MAX_HEADER_SIZE

#define IP6_CLI_PREFIX_SIZE    (5 * CLI_MAX_COLUMN_WIDTH) + \
                                CLI_MAX_HEADER_SIZE
          

#define IP6_TRAFFIC_OBJECTS   124 /* sizeof(icmp6Stats) + sizeof(udp6Stats) */ 
#define IP6_CLI_TRAFFIC_SHOW    ( CLI_MAX_COLUMN_WIDTH * IP6_TRAFFIC_OBJECTS )

#define IP6_CLI_MAX_ARGS         13
#define IP6_ND_SUPPRESS_RA       2
#define IP6_ND_NO_SUPPRESS_RA    1
#define IP6_RA_RDNSS             1
#define IP6_RA_NO_RDNSS          2
#define IP6_RA_RDNSS_OPEN        1
#define IP6_RA_NO_RDNSS_OPEN     2
#define IP6_RA_ADV_INTERVAL      1
#define IP6_RA_NO_ADV_INTERVAL   2
#define IP6_RA_ADV_LINKLOCAL     1
#define IP6_RA_NO_ADV_LINKLOCAL  2
#define IP6_RA_DNS_DOMNAME       1
#define IP6_RA_NO_DNS_DOMNAME    2
#define IP6_ND_DEF_RDNSS_PREFERENCE 8 
#define IP6_ND_DEF_RDNSS_LIFETIME (2 * IP6_ND_DEF_RA_INTERVAL) 
#define IP6_DEF_DAD_RETRIES      0
#define IP6_ND_RA_M_BIT          1
#define IP6_ND_RA_O_BIT          2
#define IP6_ND_RA_NO_M_BIT       3
#define IP6_ND_RA_NO_O_BIT       4
#define IP6_ND_DEF_DAD_RETRIES   1
#define IP6_ND_DEF_REACH_TIME    30000
#ifdef LNXIP6_WANTED
#define IP6_ND_DEF_RETRANS_TIME  0
#else
#define IP6_ND_DEF_RETRANS_TIME  1000
#endif
#define IP6_ND_DEF_HOPLIMIT      64
#define IP6_ND_DEF_RA_INTERVAL   600
#define IP6_NO_DEF_RA_MIN_INTERVAL  3    
#define IP6_ND_DEF_HOPLIMIT      64
#define IP6_DEF_RT_ADVERT_LINK_MTU  0
#define IP6_DEF_RT_ADVERT_RETX_TIME  0
#define IP6_ND_DEF_RA_MIN_INTERVAL (0.33 * IP6_ND_DEF_RA_INTERVAL)
#define IP6_ND_INVLID_INTERVAL   0xFFFF0000
#define IP6_ND_DEF_RA_LIFETIME   1800

/* ICMPv6 Error message Constants */
#define  IP6_ERR_MSG_DEF_INETRVAL           100
#define  IP6_ERR_MSG_DEF_BUCKET_SIZE        10

/* This values should be same as MIB Default (defined in nd6.h)*/
#define CLI_IP6_RA_DEF_REACHTIME     30000
#define CLI_IP6_RA_DEF_RETTIME       1000
#define CLI_IP6_RA_DEF_MAX_RA_TIME   600
#define CLI_IP6_RA_DEF_MIN_RA_TIME   (0.33 * CLI_IP6_RA_DEF_MAX_RA_TIME)
#define CLI_IP6_RA_DEF_DEFTIME       (3 * CLI_IP6_RA_DEF_MAX_RA_TIME)
#define CLI_IP6_RA_DEF_HOPLMT        64
#define CLI_IP6_RA_DEF_LINK_MTU      0
#define CLI_IP6_RA_DEF_SEND_STATUS   IPVX_TRUTH_VALUE_FALSE
#define CLI_IP6_RA_DEF_O_FLAG        IPVX_TRUTH_VALUE_FALSE
#define CLI_IP6_RA_DEF_M_FLAG        IPVX_TRUTH_VALUE_FALSE

/* Route Protocols */
#define IP6_CLI_RAROUTE_PREF_LOW   0
#define IP6_CLI_RAROUTE_PREF_MED   1
#define IP6_CLI_RAROUTE_PREF_HIGH  2

#define SHOW_LOCAL_PROTOID           2
#define SHOW_NETMGMT_PROTOID         3
#define SHOW_OSPF_PROTOID            6
#define SHOW_RIP_PROTOID             5
#define SHOW_BGP_PROTOID             7
#define SHOW_ISIS_PROTOID            9

/*****************************************************************************
 *                       "show ip6rt" interface structure                    *
 *****************************************************************************/
#define IP6_SHOW_MAX_ROUTE          10  /* Collect a maximum of specified
                                         * routes to be displayed per cycle */

typedef struct t_ShowIp6Route
{
    tIp6Addr    PrefixAddr;      /* network prefix              */
    tIp6Addr    NextHop;         /* next hop                    */
    UINT4       u4Metric;        /* metric                      */
    UINT4       u4Preference;    /* Protocol Preference Value   */
    UINT4       u4IfIndex;       /* Interface Index             */
    UINT1       u1PrefixLen;     /* Route's Prefix Length       */
    UINT1       u1RouteStatus;   /* Active/Inactive             */
    INT1        i1Protocol;      /* Protocol                    */
    UINT1       u1Pad;
} tShowIp6Route;

/* the following information is used as a cookie in tShowIp6Rt structure */
typedef struct t_ShowIp6RtCookie
{
    tIp6Addr    PrefixAddr;     /* Route's Prefix                           */
    tIp6Addr    NextHop;        /* Next Hop                                 */
    UINT1       u1PrefixLen;    /* Route's Prefix Length                    */
    INT1        i1Protocol;     /* Protocol from which the route is learnt. */
    INT1        i1DispFlag;     /* Flag indicating to display best route or */
                                /* all routes                               */
    UINT1       u1Pad;
} tShowIp6RtCookie;

typedef struct t_ShowIp6Rt
{
    UINT4 u4NoOfRoutes;          /* number of routes presented in this return 
                                  * message
                                  */
    tShowIp6Route aRoutes[1];  /* 'u4NoOfRoutes' follow here           */
} tShowIp6Rt;


/*****************************************************************************
 *                       "show ip6nd" interface structure                    *
 *****************************************************************************/
#define IP6_SHOW_MAX_ND             10  /* Collect a maximum of specified
                                         * ND Entries to be displayed per
                                         * cycle */

typedef struct t_ShowIp6NdAttr
{
    tIp6Addr    DestAddr;                /* Destination Address     */
    UINT1       lladdr[6];               /* Link Address            */
    UINT1       u1ReachState;            /* Reachability Status     */
    UINT1       u1Pad;
    UINT4       u4IfIndex;               /* Interface Index         */
    UINT4       u4RemTime;               /* Entry Valid Life Time   */
} tShowIp6NdAttr;

/* the following information is used as a cookie in tShowIp6Nd structure */
typedef struct t_ShowIp6NdCookie
{
    tIp6Addr    DestAddr;                /* Destination Address */
    UINT4       u4IfIndex;               /* Interface Index     */
} tShowIp6NdCookie;

typedef struct t_ShowIp6Nd
{
    UINT4          u4NoOfNd;    /* number of ND entries presented in this
                                 * return message
                                 */
    tShowIp6NdAttr aNdAttr[1];  /* 'u4NoOfNd' follow here */
} tShowIp6Nd;

/*****************************************************************************
 *                       "show ip6Prefix" interface structure                *
 *****************************************************************************/
#define IP6_SHOW_MAX_PREFIX         10  /* Collect a maximum of specified
                                         * PREFIXES to be displayed per
                                         * cycle */

typedef struct t_ShowIp6PrefixAttr
{
    tIp6Addr    Prefix;                  /* Address Prefix          */
    UINT4       u4IfIndex;               /* Interface Index         */
    UINT4       u4ValidTime;             /* Prefix Valid Life Time  */
    UINT4       u4PrefTime;              /* Prefered Life Time      */
    UINT1       u1PrefixLen;             /* Address Prefix Length   */
    UINT1       u1OnLinkStatus;          /* On-Link Status          */
    UINT1       u1AutoConfigStatus;      /* Auto-Config Status      */
    UINT1       u1AdvtStatus;            /* Advertisement Status    */
} tShowIp6PrefixAttr;

/* the following information is used as a cookie in tShowIp6Prefix structure */
typedef struct t_ShowIp6PrefixCookie
{
    tIp6Addr    Prefix;                  /* Address Prefix          */
    UINT4       u4IfIndex;               /* Interface Index         */
    UINT1       u1PrefixLen;             /* Address Prefix Length   */
    UINT1       u1AllPrefix;             /* Flag indicating whether
                                          * to return all or specific
                                          * entry
                                          */
    UINT1       au1Pad[2];
} tShowIp6PrefixCookie;

typedef struct t_ShowIp6Prefix
{
    UINT4          u4NoOfPrefix;    /* number of Prefixes presented in this
                                     * return message
                                     */
    tShowIp6PrefixAttr aPrefixAttr[1];  /* 'u4NoOfPrefix' follow here */
} tShowIp6Prefix;


typedef struct 
{
    UINT4  u4In_rcvs;      
    UINT4  u4In_hdr_err;
    UINT4  u4In_len_err;    
    UINT4  u4In_cksum_err; 
    UINT4  u4In_ver_err; 
    UINT4  u4In_ttl_err;
    UINT4  u4In_addr_err;
    UINT4  u4Reasm_reqs;
    UINT4  u4Reasm_oks;
    UINT4  u4Reasm_timeout;
    UINT4  u4Out_frag_pkts;
    UINT4  u4Frag_fails;
    UINT4  u4In_bcasts;
    UINT4  u4Out_requests;
    UINT4  u4Forw_attempts;
    UINT4  u4Out_no_routes;
    UINT4  u4Out_gen_err;
    UINT4  u4Bad_proto;
    UINT4  u4In_opt_err;
    INT4   i4IpForwEnable;
    INT4   i4DefaultTtl;
    INT4   i4AggRoutes;
    INT4   i4Multipath;
    INT4   i4IpLoadShareEnable;
    INT4   i4IpEnablePMTUD;
}t_CLI_IP6_STATS;       

typedef struct 
{
    UINT4  u4IcmpInMsgs;
    UINT4  u4IcmpInErr;
    UINT4  u4IcmpInDestUnreach;
    UINT4  u4IcmpInRedirects;
    UINT4  u4IcmpInTimeExceeds;
    UINT4  u4IcmpInParmProbs;
    UINT4  u4IcmpInSrcQuench;
    UINT4  u4IcmpInEchos;
    UINT4  u4IcmpInEchosReps;
    UINT4  u4IcmpInAddrMasks;
    UINT4  u4IcmpInAddrMaskReps;
    UINT4  u4IcmpInTimeStamps;
    UINT4  u4IcmpInTimeStampsReps;
    UINT4  u4IcmpInDomainNameReq;
    UINT4  u4IcmpInDomainNameRep;
    UINT4  u4IcmpInSecurityFail;
    UINT4  u4IcmpInToobig;
    UINT4  u4IcmpInRtAdv;
    UINT4  u4IcmpInRtSol;
    UINT4  u4IcmpInNbAdv;
    UINT4  u4IcmpInNbSol;
    UINT4  u4IcmpOutMsgs;
    UINT4  u4IcmpOutErr;
    UINT4  u4IcmpOutDestUnreach;
    UINT4  u4IcmpOutRedirects;
    UINT4  u4IcmpOutTimeExceeds;
    UINT4  u4IcmpOutParmProbs;
    UINT4  u4IcmpOutSrcQuench;
    UINT4  u4IcmpOutEchos;
    UINT4  u4IcmpOutEchosReps;
    UINT4  u4IcmpOutAddrMasks;
    UINT4  u4IcmpOutAddrMaskReps;
    UINT4  u4IcmpOutTimeStamps;
    UINT4  u4IcmpOutTimeStampsReps;
    UINT4  u4IcmpOutDomainNameReq;
    UINT4  u4IcmpOutDomainNameRep;
    UINT4  u4IcmpOutSecurityFail;
    UINT4  u4IcmpOutTooBig;
    UINT4  u4IcmpOutRtAdv;
    UINT4  u4IcmpOutRtSol;
    UINT4  u4IcmpOutNbAdv;
    UINT4  u4IcmpOutNbSol;
    UINT4  u4IcmpOutRateLimit;
}t_CLI_ICMP6_STATS;       

typedef struct 
{
    t_CLI_IP6_STATS    Ip6Stats;
    t_CLI_ICMP6_STATS  Icmp6Stats;
}tIp6CliInfo;       

/*****************************************************************************
 *                       "show ip6if <int>" interface structure              *
 *****************************************************************************/
#define IP6_SHOW_MAX_INTF_ADDR      10  /* Collect a maximum of specified
                                         * INTF ADDR to be displayed per
                                         * cycle */

typedef struct t_ShowIpIntfAddrAttr
{
    tIp6Addr    Ip6Addr;                 /* Address                 */
    UINT1       u1PrefixLen;             /* Address Prefix Length   */
    UINT1       u1AddrType;              /* Address Type            */
    UINT1       u1Status;
    UINT1       u1Scope;
    UINT1       u1CfgMethod;
    UINT1       au1Pad[3];
} tShowIp6IntfAddrAttr;

typedef struct t_ShowIp6IntfAddr
{
    UINT4       u4NoOfAddr;             /* number of Address presented in
                                         * this return message
                                         */
    tShowIp6IntfAddrAttr    aIntfAddrAttr[1]; /* 'u4NoOfAddr' follow here */
} tShowIp6IntfAddr;

/* the following information is used as a cookie in tShowIp6Intf structure */
typedef struct t_ShowIp6IntfAddrCookie
{
    tIp6Addr    Ip6Addr;                 /* Address                 */
    UINT1       u1PrefixLen;             /* Address Prefix Length   */
    UINT1       u1AddrType;              /* Address Type            */
    UINT1       au1Pad[2];
} tShowIp6IntfAddrCookie;

typedef struct t_ShowIp6Interface
{
    INT4        i4IfType;               /* Type */
    INT4        i4AdminStatus;          /* Admin Status */
    INT4        i4OperStatus;           /* Oper Status */
    INT4        i4DadRetries;           /* DAD Retry Count */
    INT4        i4RouterLifeTime;       /* Router Life Time */
    INT4        i4RouterAdvtStatus;     /* Router Advt Status */
    INT4        i4RouteAdvRDNSS;        /* Router Advt RDNSS  */    
    INT4        i4RouteAdvRDNSSStatus;  /* Router Advt RDNSS Status */
    INT4        i4RouterReachTime;      /* Router Reach Time */
    INT4        i4RouterRetransTime;    /* Router Retransmit Time */
    INT4        i4RouterMinAdvtTime;    /* Router Min Advertise Time*/
    INT4        i4RouterMaxAdvtTime;    /* Router Max Advertise Time */
    INT4        i4RouterAdvtManagedFlag;/* Router Advertise Managed Flag*/
    INT4        i4RouterAdvtOConfigFlag; /* Other Config Flag*/
    INT4        i4Icmp6DstUnReachable;  /* Dest unreachable status flag*/
    INT4        i4Icmp6ErrInterval;     /* Icmp6 Err Msg Interval*/
    INT4        i4Icmp6BucketSize;      /* no of Err pkt to be sent in the given Err interval*/
    INT4        i4Icmp6RedirectMsg;     /* ICMPv6 Redirect Message Status */
    INT4        i4IfForwdStatus;        /* Forwarding status on this interface*/
    INT4        i4RALinkLocalStatus;
    INT4        i4RAInterval;
    INT4        i4RADNSStateActive; /* DNS State -ACTIVE/DISABLED */
    UINT4       i4DnsLifeTime;         /* DNS Life time value*/
    UINT4       i4DnsLifeTimeOne;         /* DNS server one Life time value*/
    UINT4       i4DnsLifeTimeTwo;         /* DNS server two Life time value*/
    UINT4       i4DnsLifeTimeThree;         /* DNS server three Life time value*/
    UINT4       u4Icmp6RLErrMsgCnt;     /* Rate limited Err msg count */
    UINT4       u4Mtu;                  /* MTU */
    UINT4       u4SrcAddr;              /* Tunl Source Address */
    UINT4       u4DestAddr;             /* Tunl Dest Address */
    UINT4       u4NoOfAddr;             /* number of Address presented in
                                         * this return message
                                         */
    UINT4       u4RaLinkMTU;            /* RA Link MTU Value */
    UINT4       u4RACurHopLimit;        /* RA Link Hop Limit Value*/
    UINT4       u4UnnumAssocIPv6If;     /* Associated unnumbered interface index */
    UINT4       u4RDNSSPreference;      /* RDNSS Preference value*/
    UINT4       u4RDNSSLifetime;      /* RDNSS Lifetime value*/
    UINT4       u4RDNSSLifetimeOne;      /* RDNSS Lifetime value*/
    UINT4       u4RDNSSLifetimeTwo;      /* RDNSS Lifetime value*/
    UINT4       u4RDNSSLifetimeThree;      /* RDNSS Lifetime value*/
    UINT1       u1TunlType;             /* Tunl Type */
#ifdef LNXIP6_WANTED
    UINT1       au1DomainNameOne[IP6_DOMAIN_NAME_LEN + 1]; /*DNS domain name one*/
    UINT1       au1DomainNameTwo[IP6_DOMAIN_NAME_LEN + 1]; /*DNS domain name two*/
    UINT1       au1DomainNameThree[IP6_DOMAIN_NAME_LEN + 1]; /*DNS domain name three*/
#else
    UINT1       au1Pad[3];
#endif
} tShowIp6Interface;

INT4 Ipv6ShowRunningConfigInterfaceDetails(tCliHandle CliHandle,
                                                  INT4 i4IfIndex, UINT1 *u1Flag);
#define   ND6_AUDIT_SHOW_CMD    "end;show ipv6 neighbors > "
#define   ND6_CLI_MAX_GROUPS_LINE_LEN    200
#define   ND6_CLI_EOF                  2
#define   ND6_CLI_NO_EOF                 1
#define   ND6_CLI_RDONLY               OSIX_FILE_RO
#define   ND6_CLI_WRONLY               OSIX_FILE_WO

#define ND6_AUDIT_FILE_ACTIVE "/tmp/nd6_output_file_active"
#define ND6_AUDIT_FILE_STDBY "/tmp/nd6_output_file_stdby"

#define NS      1   /* Neighbor Solicitation */
#define NA      2   /* Neighbor Advertisement */
#define RS      3   /* Router Solicitation */
#define RA      4   /* Router Advertisement */
#define RD      5   /* Redirect Message */
#define CPS     6   /* Certificate Path Solicitation */
#define CPA     7   /* Certificate Path Advertisement */

INT4
Nd6CliGetShowCmdOutputToFile (UINT1 *);

INT4
Nd6CliCalcSwAudCheckSum (UINT1 *, UINT2 *);

INT1
Nd6CliReadLineFromFile (INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen, INT2 *pi2ReadLen);

#endif /* __IP6CLI_H__ */

#ifdef IP6_TEST_WANTED
#define IP6_UT_TEST 1
VOID cli_process_ip6_test_cmd (tCliHandle CliHandle,...);
#endif
extern INT1
nmhGetIpForwarding ARG_LIST((INT4 *));

extern INT1
nmhGetIpDefaultTTL ARG_LIST((INT4 *));

extern INT1
nmhGetIpReasmTimeout ARG_LIST((INT4 *));

extern INT1
nmhGetIpv6IpForwarding ARG_LIST((INT4 *));

extern INT1
nmhGetIpv6IpDefaultHopLimit ARG_LIST((INT4 *));

extern INT1
nmhGetIpv4InterfaceTableLastChange ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetIpForwarding ARG_LIST((INT4 ));

extern INT1
nmhSetIpDefaultTTL ARG_LIST((INT4 ));

extern INT1
nmhSetIpv6IpForwarding ARG_LIST((INT4 ));

extern INT1
nmhSetIpv6IpDefaultHopLimit ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2IpForwarding ARG_LIST((UINT4 *  ,INT4 ));

extern INT1
nmhTestv2IpDefaultTTL ARG_LIST((UINT4 *  ,INT4 ));

extern INT1
nmhTestv2Ipv6IpForwarding ARG_LIST((UINT4 *  ,INT4 ));

extern INT1
nmhTestv2Ipv6IpDefaultHopLimit ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

extern INT1
nmhDepv2IpForwarding ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

extern INT1
nmhDepv2IpDefaultTTL ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

extern INT1
nmhDepv2Ipv6IpForwarding ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

extern INT1
nmhDepv2Ipv6IpDefaultHopLimit ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetIpv6InterfaceTableLastChange ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for Ipv6InterfaceTable. */
extern INT1
nmhValidateIndexInstanceIpv6InterfaceTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Ipv6InterfaceTable  */

extern INT1
nmhGetFirstIndexIpv6InterfaceTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexIpv6InterfaceTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetIpv6InterfaceReasmMaxSize ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpv6InterfaceIdentifier ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetIpv6InterfaceEnableStatus ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIpv6InterfaceReachableTime ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpv6InterfaceRetransmitTime ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpv6InterfaceForwarding ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetIpv6InterfaceEnableStatus ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetIpv6InterfaceForwarding ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2Ipv6InterfaceEnableStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2Ipv6InterfaceForwarding ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

extern INT1
nmhDepv2Ipv6InterfaceTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IpSystemStatsTable. */
extern INT1
nmhValidateIndexInstanceIpSystemStatsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IpSystemStatsTable  */

extern INT1
nmhGetFirstIndexIpSystemStatsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexIpSystemStatsTable ARG_LIST((INT4 , INT4 *));

/* Proto Validate Index Instance for IpAddressPrefixTable. */
extern INT1
nmhValidateIndexInstanceIpAddressPrefixTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for IpAddressPrefixTable  */

extern INT1
nmhGetFirstIndexIpAddressPrefixTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexIpAddressPrefixTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetIpAddressPrefixOrigin ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

extern INT1
nmhGetIpAddressPrefixOnLinkFlag ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

extern INT1
nmhGetIpAddressPrefixAutonomousFlag ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

extern INT1
nmhGetIpAddressPrefixAdvPreferredLifetime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

extern INT1
nmhGetIpAddressPrefixAdvValidLifetime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetIpAddressSpinLock ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetIpAddressSpinLock ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2IpAddressSpinLock ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

extern INT1
nmhDepv2IpAddressSpinLock ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IpAddressTable. */
extern INT1
nmhValidateIndexInstanceIpAddressTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for IpAddressTable  */

extern INT1
nmhGetFirstIndexIpAddressTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexIpAddressTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetIpAddressIfIndex ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetIpAddressType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetIpAddressPrefix ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OID_TYPE * ));

extern INT1
nmhGetIpAddressOrigin ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetIpAddressStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetIpAddressCreated ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

extern INT1
nmhGetIpAddressLastChanged ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

extern INT1
nmhGetIpAddressRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetIpAddressStorageType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetIpAddressIfIndex ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetIpAddressType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetIpAddressStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetIpAddressRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetIpAddressStorageType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2IpAddressIfIndex ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhTestv2IpAddressType ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhTestv2IpAddressStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhTestv2IpAddressRowStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhTestv2IpAddressStorageType ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

extern INT1
nmhDepv2IpAddressTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IpNetToPhysicalTable. */
extern INT1
nmhValidateIndexInstanceIpNetToPhysicalTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for IpNetToPhysicalTable  */

extern INT1
nmhGetFirstIndexIpNetToPhysicalTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexIpNetToPhysicalTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetIpNetToPhysicalPhysAddress ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetIpNetToPhysicalLastUpdated ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

extern INT1
nmhGetIpNetToPhysicalType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetIpNetToPhysicalState ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetIpNetToPhysicalRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetIpNetToPhysicalPhysAddress ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetIpNetToPhysicalType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetIpNetToPhysicalRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2IpNetToPhysicalPhysAddress ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2IpNetToPhysicalType ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhTestv2IpNetToPhysicalRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

extern INT1
nmhDepv2IpNetToPhysicalTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ipv6ScopeZoneIndexTable. */
extern INT1
nmhValidateIndexInstanceIpv6ScopeZoneIndexTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Ipv6ScopeZoneIndexTable  */

extern INT1
nmhGetFirstIndexIpv6ScopeZoneIndexTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexIpv6ScopeZoneIndexTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetIpv6ScopeZoneIndexLinkLocal ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpv6ScopeZoneIndex3 ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpv6ScopeZoneIndexAdminLocal ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpv6ScopeZoneIndexSiteLocal ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpv6ScopeZoneIndex6 ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpv6ScopeZoneIndex7 ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpv6ScopeZoneIndexOrganizationLocal ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpv6ScopeZoneIndex9 ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpv6ScopeZoneIndexA ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpv6ScopeZoneIndexB ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpv6ScopeZoneIndexC ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIpv6ScopeZoneIndexD ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for IpDefaultRouterTable. */
extern INT1
nmhValidateIndexInstanceIpDefaultRouterTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for IpDefaultRouterTable  */

extern INT1
nmhGetFirstIndexIpDefaultRouterTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexIpDefaultRouterTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetIpDefaultRouterLifetime ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

extern INT1
nmhGetIpDefaultRouterPreference ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetIpv6RouterAdvertSpinLock ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetIpv6RouterAdvertSpinLock ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2Ipv6RouterAdvertSpinLock ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

extern INT1
nmhDepv2Ipv6RouterAdvertSpinLock ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2Ipv6RouterAdvertSendAdverts ARG_LIST((UINT4 *  ,INT4  ,INT4 ));


/* Low Level DEP Routines for.  */

extern INT1
nmhDepv2Ipv6RouterAdvertTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetIpInReceives ARG_LIST((UINT4 *));

extern INT1
nmhGetIpInHdrErrors ARG_LIST((UINT4 *));

extern INT1
nmhGetIpInAddrErrors ARG_LIST((UINT4 *));

extern INT1
nmhGetIpForwDatagrams ARG_LIST((UINT4 *));

extern INT1
nmhGetIpInUnknownProtos ARG_LIST((UINT4 *));

extern INT1
nmhGetIpInDiscards ARG_LIST((UINT4 *));

extern INT1
nmhGetIpInDelivers ARG_LIST((UINT4 *));

extern INT1
nmhGetIpOutRequests ARG_LIST((UINT4 *));

extern INT1
nmhGetIpOutDiscards ARG_LIST((UINT4 *));

extern INT1
nmhGetIpOutNoRoutes ARG_LIST((UINT4 *));

extern INT1
nmhGetIpReasmReqds ARG_LIST((UINT4 *));

extern INT1
nmhGetIpReasmOKs ARG_LIST((UINT4 *));

extern INT1
nmhGetIpReasmFails ARG_LIST((UINT4 *));

extern INT1
nmhGetIpFragOKs ARG_LIST((UINT4 *));

extern INT1
nmhGetIpFragFails ARG_LIST((UINT4 *));

extern INT1
nmhGetIpFragCreates ARG_LIST((UINT4 *));

extern INT1
nmhGetIpRoutingDiscards ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for IpAddrTable. */
extern INT1
nmhValidateIndexInstanceIpAddrTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for IpAddrTable  */

extern INT1
nmhGetFirstIndexIpAddrTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexIpAddrTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetIpAdEntIfIndex ARG_LIST((UINT4 ,INT4 *));

extern INT1
nmhGetIpAdEntNetMask ARG_LIST((UINT4 ,UINT4 *));

extern INT1
nmhGetIpAdEntBcastAddr ARG_LIST((UINT4 ,INT4 *));

extern INT1
nmhGetIpAdEntReasmMaxSize ARG_LIST((UINT4 ,INT4 *));

/* Proto Validate Index Instance for IpNetToMediaTable. */
extern INT1
nmhValidateIndexInstanceIpNetToMediaTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for IpNetToMediaTable  */

extern INT1
nmhGetFirstIndexIpNetToMediaTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexIpNetToMediaTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetIpNetToMediaPhysAddress ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetIpNetToMediaType ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetIpNetToMediaIfIndex ARG_LIST((INT4  , UINT4  ,INT4 ));

extern INT1
nmhSetIpNetToMediaPhysAddress ARG_LIST((INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetIpNetToMediaNetAddress ARG_LIST((INT4  , UINT4  ,UINT4 ));

extern INT1
nmhSetIpNetToMediaType ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2IpNetToMediaIfIndex ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

extern INT1
nmhTestv2IpNetToMediaPhysAddress ARG_LIST((UINT4 *  ,INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2IpNetToMediaNetAddress ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

extern INT1
nmhTestv2IpNetToMediaType ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

extern INT1
nmhDepv2IpNetToMediaTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetIcmpInMsgs ARG_LIST((UINT4 *));

extern INT1
nmhGetIcmpInErrors ARG_LIST((UINT4 *));

extern INT1
nmhGetIcmpInDestUnreachs ARG_LIST((UINT4 *));

extern INT1
nmhGetIcmpInTimeExcds ARG_LIST((UINT4 *));

extern INT1
nmhGetIcmpInParmProbs ARG_LIST((UINT4 *));

extern INT1
nmhGetIcmpInSrcQuenchs ARG_LIST((UINT4 *));

extern INT1
nmhGetIcmpInRedirects ARG_LIST((UINT4 *));

extern INT1
nmhGetIcmpInEchos ARG_LIST((UINT4 *));

extern INT1
nmhGetIcmpInEchoReps ARG_LIST((UINT4 *));

extern INT1
nmhGetIcmpInTimestamps ARG_LIST((UINT4 *));

extern INT1
nmhGetIcmpInTimestampReps ARG_LIST((UINT4 *));

extern INT1
nmhGetIcmpInAddrMasks ARG_LIST((UINT4 *));

extern INT1
nmhGetIcmpInAddrMaskReps ARG_LIST((UINT4 *));

extern INT1
nmhGetIcmpOutMsgs ARG_LIST((UINT4 *));

extern INT1
nmhGetIcmpOutErrors ARG_LIST((UINT4 *));

extern INT1
nmhGetIcmpOutDestUnreachs ARG_LIST((UINT4 *));

extern INT1
nmhGetIcmpOutTimeExcds ARG_LIST((UINT4 *));

extern INT1
nmhGetIcmpOutParmProbs ARG_LIST((UINT4 *));

extern INT1
nmhGetIcmpOutSrcQuenchs ARG_LIST((UINT4 *));

extern INT1
nmhGetIcmpOutRedirects ARG_LIST((UINT4 *));

extern INT1
nmhGetIcmpOutEchos ARG_LIST((UINT4 *));

extern INT1
nmhGetIcmpOutEchoReps ARG_LIST((UINT4 *));

extern INT1
nmhGetIcmpOutTimestamps ARG_LIST((UINT4 *));

extern INT1
nmhGetIcmpOutTimestampReps ARG_LIST((UINT4 *));

extern INT1
nmhGetIcmpOutAddrMasks ARG_LIST((UINT4 *));

extern INT1
nmhGetIcmpOutAddrMaskReps ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetInetCidrRouteNumber ARG_LIST((UINT4 *));

extern INT1
nmhGetInetCidrRouteDiscards ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for InetCidrRouteTable. */
extern INT1
nmhValidateIndexInstanceInetCidrRouteTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for InetCidrRouteTable  */

extern INT1
nmhGetFirstIndexInetCidrRouteTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 * , tSNMP_OID_TYPE *  , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexInetCidrRouteTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , tSNMP_OID_TYPE *, tSNMP_OID_TYPE *  , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetInetCidrRouteIfIndex ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetInetCidrRouteType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetInetCidrRouteProto ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetInetCidrRouteAge ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

extern INT1
nmhGetInetCidrRouteNextHopAS ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

extern INT1
nmhGetInetCidrRouteMetric1 ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetInetCidrRouteMetric2 ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetInetCidrRouteMetric3 ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetInetCidrRouteMetric4 ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetInetCidrRouteMetric5 ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetInetCidrRouteStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetInetCidrRouteIfIndex ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetInetCidrRouteType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetInetCidrRouteNextHopAS ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

extern INT1
nmhSetInetCidrRouteMetric1 ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetInetCidrRouteMetric2 ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetInetCidrRouteMetric3 ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetInetCidrRouteMetric4 ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetInetCidrRouteMetric5 ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetInetCidrRouteStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2InetCidrRouteIfIndex ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhTestv2InetCidrRouteType ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhTestv2InetCidrRouteNextHopAS ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

extern INT1
nmhTestv2InetCidrRouteMetric1 ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhTestv2InetCidrRouteMetric2 ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhTestv2InetCidrRouteMetric3 ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhTestv2InetCidrRouteMetric4 ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhTestv2InetCidrRouteMetric5 ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhTestv2InetCidrRouteStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

extern INT1
nmhDepv2InetCidrRouteTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetIpCidrRouteNumber ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for IpCidrRouteTable. */
extern INT1
nmhValidateIndexInstanceIpCidrRouteTable ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for IpCidrRouteTable  */

extern INT1
nmhGetFirstIndexIpCidrRouteTable ARG_LIST((UINT4 * , UINT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexIpCidrRouteTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetIpCidrRouteIfIndex ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

extern INT1
nmhGetIpCidrRouteType ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

extern INT1
nmhGetIpCidrRouteProto ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

extern INT1
nmhGetIpCidrRouteAge ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

extern INT1
nmhGetIpCidrRouteInfo ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,tSNMP_OID_TYPE * ));

extern INT1
nmhGetIpCidrRouteNextHopAS ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

extern INT1
nmhGetIpCidrRouteMetric1 ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

extern INT1
nmhGetIpCidrRouteMetric2 ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

extern INT1
nmhGetIpCidrRouteMetric3 ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

extern INT1
nmhGetIpCidrRouteMetric4 ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

extern INT1
nmhGetIpCidrRouteMetric5 ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

extern INT1
nmhGetIpCidrRouteStatus ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetIpCidrRouteIfIndex ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

extern INT1
nmhSetIpCidrRouteType ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

extern INT1
nmhSetIpCidrRouteInfo ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,tSNMP_OID_TYPE *));

extern INT1
nmhSetIpCidrRouteNextHopAS ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

extern INT1
nmhSetIpCidrRouteMetric1 ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

extern INT1
nmhSetIpCidrRouteMetric2 ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

extern INT1
nmhSetIpCidrRouteMetric3 ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

extern INT1
nmhSetIpCidrRouteMetric4 ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

extern INT1
nmhSetIpCidrRouteMetric5 ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

extern INT1
nmhSetIpCidrRouteStatus ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2IpCidrRouteIfIndex ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

extern INT1
nmhTestv2IpCidrRouteType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

extern INT1
nmhTestv2IpCidrRouteInfo ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,tSNMP_OID_TYPE *));

extern INT1
nmhTestv2IpCidrRouteNextHopAS ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

extern INT1
nmhTestv2IpCidrRouteMetric1 ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

extern INT1
nmhTestv2IpCidrRouteMetric2 ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

extern INT1
nmhTestv2IpCidrRouteMetric3 ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

extern INT1
nmhTestv2IpCidrRouteMetric4 ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

extern INT1
nmhTestv2IpCidrRouteMetric5 ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

extern INT1
nmhTestv2IpCidrRouteStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

extern INT1
nmhDepv2IpCidrRouteTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetIpForwardNumber ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for IpForwardTable. */
extern INT1
nmhValidateIndexInstanceIpForwardTable ARG_LIST((UINT4  , INT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for IpForwardTable  */

extern INT1
nmhGetFirstIndexIpForwardTable ARG_LIST((UINT4 * , INT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexIpForwardTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetIpForwardMask ARG_LIST((UINT4  , INT4  , INT4  , UINT4 ,UINT4 *));

extern INT1
nmhGetIpForwardIfIndex ARG_LIST((UINT4  , INT4  , INT4  , UINT4 ,INT4 *));

extern INT1
nmhGetIpForwardType ARG_LIST((UINT4  , INT4  , INT4  , UINT4 ,INT4 *));

extern INT1
nmhGetIpForwardAge ARG_LIST((UINT4  , INT4  , INT4  , UINT4 ,INT4 *));

extern INT1
nmhGetIpForwardInfo ARG_LIST((UINT4  , INT4  , INT4  , UINT4 ,tSNMP_OID_TYPE * ));

extern INT1
nmhGetIpForwardNextHopAS ARG_LIST((UINT4  , INT4  , INT4  , UINT4 ,INT4 *));

extern INT1
nmhGetIpForwardMetric1 ARG_LIST((UINT4  , INT4  , INT4  , UINT4 ,INT4 *));

extern INT1
nmhGetIpForwardMetric2 ARG_LIST((UINT4  , INT4  , INT4  , UINT4 ,INT4 *));

extern INT1
nmhGetIpForwardMetric3 ARG_LIST((UINT4  , INT4  , INT4  , UINT4 ,INT4 *));

extern INT1
nmhGetIpForwardMetric4 ARG_LIST((UINT4  , INT4  , INT4  , UINT4 ,INT4 *));

extern INT1
nmhGetIpForwardMetric5 ARG_LIST((UINT4  , INT4  , INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetIpForwardMask ARG_LIST((UINT4  , INT4  , INT4  , UINT4  ,UINT4 ));

extern INT1
nmhSetIpForwardIfIndex ARG_LIST((UINT4  , INT4  , INT4  , UINT4  ,INT4 ));

extern INT1
nmhSetIpForwardType ARG_LIST((UINT4  , INT4  , INT4  , UINT4  ,INT4 ));

extern INT1
nmhSetIpForwardInfo ARG_LIST((UINT4  , INT4  , INT4  , UINT4  ,tSNMP_OID_TYPE *));

extern INT1
nmhSetIpForwardNextHopAS ARG_LIST((UINT4  , INT4  , INT4  , UINT4  ,INT4 ));

extern INT1
nmhSetIpForwardMetric1 ARG_LIST((UINT4  , INT4  , INT4  , UINT4  ,INT4 ));

extern INT1
nmhSetIpForwardMetric2 ARG_LIST((UINT4  , INT4  , INT4  , UINT4  ,INT4 ));

extern INT1
nmhSetIpForwardMetric3 ARG_LIST((UINT4  , INT4  , INT4  , UINT4  ,INT4 ));

extern INT1
nmhSetIpForwardMetric4 ARG_LIST((UINT4  , INT4  , INT4  , UINT4  ,INT4 ));

extern INT1
nmhSetIpForwardMetric5 ARG_LIST((UINT4  , INT4  , INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2IpForwardMask ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  , UINT4  ,UINT4 ));

extern INT1
nmhTestv2IpForwardIfIndex ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  , UINT4  ,INT4 ));

extern INT1
nmhTestv2IpForwardType ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  , UINT4  ,INT4 ));

extern INT1
nmhTestv2IpForwardInfo ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  , UINT4  ,tSNMP_OID_TYPE *));

extern INT1
nmhTestv2IpForwardNextHopAS ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  , UINT4  ,INT4 ));

extern INT1
nmhTestv2IpForwardMetric1 ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  , UINT4  ,INT4 ));

extern INT1
nmhTestv2IpForwardMetric2 ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  , UINT4  ,INT4 ));

extern INT1
nmhTestv2IpForwardMetric3 ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  , UINT4  ,INT4 ));

extern INT1
nmhTestv2IpForwardMetric4 ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  , UINT4  ,INT4 ));

extern INT1
nmhTestv2IpForwardMetric5 ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

extern INT1
nmhDepv2IpForwardTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetUdpInDatagrams ARG_LIST((UINT4 *));

extern INT1
nmhGetUdpNoPorts ARG_LIST((UINT4 *));

extern INT1
nmhGetUdpInErrors ARG_LIST((UINT4 *));

extern INT1
nmhGetUdpOutDatagrams ARG_LIST((UINT4 *));

extern INT1
nmhGetUdpHCInDatagrams ARG_LIST((tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetUdpHCOutDatagrams ARG_LIST((tSNMP_COUNTER64_TYPE *));

/* Proto Validate Index Instance for UdpEndpointTable. */
extern INT1
nmhValidateIndexInstanceUdpEndpointTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for UdpEndpointTable  */

extern INT1
nmhGetFirstIndexUdpEndpointTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexUdpEndpointTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetUdpEndpointProcess ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for UdpTable. */
extern INT1
nmhValidateIndexInstanceUdpTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for UdpTable  */

extern INT1
nmhGetFirstIndexUdpTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexUdpTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/*Proto type for ICMPv6 interface statistics*/
extern INT1
nmhGetFsIpv6IfIcmpInMsgs ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsIpv6IfIcmpInErrors ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsIpv6IfIcmpInDestUnreachs ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsIpv6IfIcmpInAdminProhibs ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsIpv6IfIcmpInTimeExcds ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsIpv6IfIcmpInParmProblems ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsIpv6IfIcmpInPktTooBigs ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsIpv6IfIcmpInEchos ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsIpv6IfIcmpInEchoReplies ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsIpv6IfIcmpInRouterSolicits ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsIpv6IfIcmpInRouterAdvertisements ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsIpv6IfIcmpInNeighborSolicits ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsIpv6IfIcmpInNeighborAdvertisements ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsIpv6IfIcmpInRedirects ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsIpv6IfIcmpInGroupMembQueries ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsIpv6IfIcmpInGroupMembResponses ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsIpv6IfIcmpInGroupMembReductions ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsIpv6IfIcmpOutMsgs ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsIpv6IfIcmpOutErrors ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsIpv6IfIcmpOutDestUnreachs ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsIpv6IfIcmpOutAdminProhibs ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsIpv6IfIcmpOutTimeExcds ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsIpv6IfIcmpOutParmProblems ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsIpv6IfIcmpOutPktTooBigs ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsIpv6IfIcmpOutEchos ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsIpv6IfIcmpOutEchoReplies ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsIpv6IfIcmpOutRouterSolicits ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsIpv6IfIcmpOutRouterAdvertisements ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsIpv6IfIcmpOutNeighborSolicits ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsIpv6IfIcmpOutNeighborAdvertisements ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsIpv6IfIcmpOutRedirects ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsIpv6IfIcmpOutGroupMembQueries ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsIpv6IfIcmpOutGroupMembResponses ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsIpv6IfIcmpOutGroupMembReductions ARG_LIST((INT4 ,UINT4 *));
