/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsrmoncli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 RmonDebugType[9];
extern UINT4 RmonEnableStatus[9];
extern UINT4 RmonHwStatsSupp[9];
extern UINT4 RmonHwHistorySupp[9];
extern UINT4 RmonHwAlarmSupp[9];
extern UINT4 RmonHwHostSupp[9];
extern UINT4 RmonHwHostTopNSupp[9];
extern UINT4 RmonHwMatrixSupp[9];
extern UINT4 RmonHwEventSupp[9];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetRmonDebugType(u4SetValRmonDebugType)	\
	nmhSetCmn(RmonDebugType, 9, RmonDebugTypeSet, RmonLock, RmonUnLock, 0, 0, 0, "%u", u4SetValRmonDebugType)
#define nmhSetRmonEnableStatus(i4SetValRmonEnableStatus)	\
	nmhSetCmn(RmonEnableStatus, 9, RmonEnableStatusSet, RmonLock, RmonUnLock, 0, 0, 0, "%i", i4SetValRmonEnableStatus)
#define nmhSetRmonHwStatsSupp(i4SetValRmonHwStatsSupp)	\
	nmhSetCmn(RmonHwStatsSupp, 9, RmonHwStatsSuppSet, RmonLock, RmonUnLock, 0, 0, 0, "%i", i4SetValRmonHwStatsSupp)
#define nmhSetRmonHwHistorySupp(i4SetValRmonHwHistorySupp)	\
	nmhSetCmn(RmonHwHistorySupp, 9, RmonHwHistorySuppSet, RmonLock, RmonUnLock, 0, 0, 0, "%i", i4SetValRmonHwHistorySupp)
#define nmhSetRmonHwAlarmSupp(i4SetValRmonHwAlarmSupp)	\
	nmhSetCmn(RmonHwAlarmSupp, 9, RmonHwAlarmSuppSet, RmonLock, RmonUnLock, 0, 0, 0, "%i", i4SetValRmonHwAlarmSupp)
#define nmhSetRmonHwHostSupp(i4SetValRmonHwHostSupp)	\
	nmhSetCmn(RmonHwHostSupp, 9, RmonHwHostSuppSet, RmonLock, RmonUnLock, 0, 0, 0, "%i", i4SetValRmonHwHostSupp)
#define nmhSetRmonHwHostTopNSupp(i4SetValRmonHwHostTopNSupp)	\
	nmhSetCmn(RmonHwHostTopNSupp, 9, RmonHwHostTopNSuppSet, RmonLock, RmonUnLock, 0, 0, 0, "%i", i4SetValRmonHwHostTopNSupp)
#define nmhSetRmonHwMatrixSupp(i4SetValRmonHwMatrixSupp)	\
	nmhSetCmn(RmonHwMatrixSupp, 9, RmonHwMatrixSuppSet, RmonLock, RmonUnLock, 0, 0, 0, "%i", i4SetValRmonHwMatrixSupp)
#define nmhSetRmonHwEventSupp(i4SetValRmonHwEventSupp)	\
	nmhSetCmn(RmonHwEventSupp, 9, RmonHwEventSuppSet, RmonLock, RmonUnLock, 0, 0, 0, "%i", i4SetValRmonHwEventSupp)

#endif
