/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmefmcli.h,v 1.4 2014/08/22 11:10:34 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMefContextId[13];
extern UINT4 FsMefTransmode[13];
extern UINT4 FsMefFrameLossBufferClear[13];
extern UINT4 FsMefFrameDelayBufferClear[13];
extern UINT4 FsMefFrameLossBufferSize[13];
extern UINT4 FsMefFrameDelayBufferSize[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMefTransmode(u4FsMefContextId ,i4SetValFsMefTransmode) \
 nmhSetCmn(FsMefTransmode, 13, FsMefTransmodeSet, NULL, NULL, 0, 0, 1, "%u %i", u4FsMefContextId ,i4SetValFsMefTransmode)
#define nmhSetFsMefFrameLossBufferClear(u4FsMefContextId ,i4SetValFsMefFrameLossBufferClear) \
 nmhSetCmn(FsMefFrameLossBufferClear, 13, FsMefFrameLossBufferClearSet, NULL, NULL, 0, 0, 1, "%u %i", u4FsMefContextId ,i4SetValFsMefFrameLossBufferClear)
#define nmhSetFsMefFrameDelayBufferClear(u4FsMefContextId ,i4SetValFsMefFrameDelayBufferClear) \
 nmhSetCmn(FsMefFrameDelayBufferClear, 13, FsMefFrameDelayBufferClearSet, NULL, NULL, 0, 0, 1, "%u %i", u4FsMefContextId ,i4SetValFsMefFrameDelayBufferClear)
#define nmhSetFsMefFrameLossBufferSize(u4FsMefContextId ,i4SetValFsMefFrameLossBufferSize) \
 nmhSetCmn(FsMefFrameLossBufferSize, 13, FsMefFrameLossBufferSizeSet, NULL, NULL, 0, 0, 1, "%u %i", u4FsMefContextId ,i4SetValFsMefFrameLossBufferSize)
#define nmhSetFsMefFrameDelayBufferSize(u4FsMefContextId ,i4SetValFsMefFrameDelayBufferSize) \
 nmhSetCmn(FsMefFrameDelayBufferSize, 13, FsMefFrameDelayBufferSizeSet, NULL, NULL, 0, 0, 1, "%u %i", u4FsMefContextId ,i4SetValFsMefFrameDelayBufferSize)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsUniId[13];
extern UINT4 FsUniServiceMultiplexingBundling[13];
extern UINT4 FsUniCVlanId[13];
extern UINT4 FsUniL2CPDot1x[13];
extern UINT4 FsUniL2CPLacp[13];
extern UINT4 FsUniL2CPStp[13];
extern UINT4 FsUniL2CPGvrp[13];
extern UINT4 FsUniL2CPGmrp[13];
extern UINT4 FsUniL2CPMvrp[13];
extern UINT4 FsUniL2CPMmrp[13];
extern UINT4 FsUniL2CPLldp[13];
extern UINT4 FsUniL2CPElmi[13];
extern UINT4 FsUniRowStatus[13];
extern UINT4 FsUniL2CPEcfm[13];
extern UINT4 FsUniL2CPOverrideOption[13];
extern UINT4 FsUniL2CPEoam[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsUniId(i4IfIndex ,pSetValFsUniId) \
 nmhSetCmn(FsUniId, 13, FsUniIdSet, NULL, NULL, 0, 0, 1, "%i %s", i4IfIndex ,pSetValFsUniId)
#define nmhSetFsUniServiceMultiplexingBundling(i4IfIndex ,pSetValFsUniServiceMultiplexingBundling) \
 nmhSetCmn(FsUniServiceMultiplexingBundling, 13, FsUniServiceMultiplexingBundlingSet, NULL, NULL, 0, 0, 1, "%i %s", i4IfIndex ,pSetValFsUniServiceMultiplexingBundling)
#define nmhSetFsUniCVlanId(i4IfIndex ,i4SetValFsUniCVlanId) \
 nmhSetCmn(FsUniCVlanId, 13, FsUniCVlanIdSet, NULL, NULL, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValFsUniCVlanId)
#define nmhSetFsUniL2CPDot1x(i4IfIndex ,i4SetValFsUniL2CPDot1x) \
 nmhSetCmn(FsUniL2CPDot1x, 13, FsUniL2CPDot1xSet, NULL, NULL, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValFsUniL2CPDot1x)
#define nmhSetFsUniL2CPLacp(i4IfIndex ,i4SetValFsUniL2CPLacp) \
 nmhSetCmn(FsUniL2CPLacp, 13, FsUniL2CPLacpSet, NULL, NULL, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValFsUniL2CPLacp)
#define nmhSetFsUniL2CPStp(i4IfIndex ,i4SetValFsUniL2CPStp) \
 nmhSetCmn(FsUniL2CPStp, 13, FsUniL2CPStpSet, NULL, NULL, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValFsUniL2CPStp)
#define nmhSetFsUniL2CPGvrp(i4IfIndex ,i4SetValFsUniL2CPGvrp) \
 nmhSetCmn(FsUniL2CPGvrp, 13, FsUniL2CPGvrpSet, NULL, NULL, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValFsUniL2CPGvrp)
#define nmhSetFsUniL2CPGmrp(i4IfIndex ,i4SetValFsUniL2CPGmrp) \
 nmhSetCmn(FsUniL2CPGmrp, 13, FsUniL2CPGmrpSet, NULL, NULL, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValFsUniL2CPGmrp)
#define nmhSetFsUniL2CPMvrp(i4IfIndex ,i4SetValFsUniL2CPMvrp) \
 nmhSetCmn(FsUniL2CPMvrp, 13, FsUniL2CPMvrpSet, NULL, NULL, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValFsUniL2CPMvrp)
#define nmhSetFsUniL2CPMmrp(i4IfIndex ,i4SetValFsUniL2CPMmrp) \
 nmhSetCmn(FsUniL2CPMmrp, 13, FsUniL2CPMmrpSet, NULL, NULL, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValFsUniL2CPMmrp)
#define nmhSetFsUniL2CPLldp(i4IfIndex ,i4SetValFsUniL2CPLldp) \
 nmhSetCmn(FsUniL2CPLldp, 13, FsUniL2CPLldpSet, NULL, NULL, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValFsUniL2CPLldp)
#define nmhSetFsUniL2CPElmi(i4IfIndex ,i4SetValFsUniL2CPElmi) \
 nmhSetCmn(FsUniL2CPElmi, 13, FsUniL2CPElmiSet, NULL, NULL, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValFsUniL2CPElmi)
#define nmhSetFsUniRowStatus(i4IfIndex ,i4SetValFsUniRowStatus) \
 nmhSetCmn(FsUniRowStatus, 13, FsUniRowStatusSet, NULL, NULL, 0, 1, 1, "%i %i", i4IfIndex ,i4SetValFsUniRowStatus)
#define nmhSetFsUniL2CPEcfm(i4IfIndex ,i4SetValFsUniL2CPEcfm)   \
 nmhSetCmn(FsUniL2CPEcfm, 13, FsUniL2CPEcfmSet, NULL, NULL, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValFsUniL2CPEcfm)
#define nmhSetFsUniL2CPOverrideOption(i4IfIndex ,i4SetValFsUniL2CPOverrideOption)       \
 nmhSetCmn(FsUniL2CPOverrideOption, 13, FsUniL2CPOverrideOptionSet, NULL, NULL, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValFsUniL2CPOverrideOption)
#define nmhSetFsUniL2CPEoam(i4IfIndex ,i4SetValFsUniL2CPEoam)   \
 nmhSetCmn(FsUniL2CPEoam, 13, FsUniL2CPEoamSet, NULL, NULL, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValFsUniL2CPEoam)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsEvcContextId[13];
extern UINT4 FsEvcIndex[13];
extern UINT4 FsEvcId[13];
extern UINT4 FsEvcType[13];
extern UINT4 FsEvcCVlanIdPreservation[13];
extern UINT4 FsEvcCVlanCoSPreservation[13];
extern UINT4 FsEvcRowStatus[13];
extern UINT4 FsEvcL2CPTunnel[13];
extern UINT4 FsEvcL2CPPeer[13];
extern UINT4 FsEvcL2CPDiscard[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsEvcId(i4FsEvcContextId , i4FsEvcIndex ,pSetValFsEvcId) \
 nmhSetCmn(FsEvcId, 13, FsEvcIdSet, NULL, NULL, 0, 0, 2, "%i %i %s", i4FsEvcContextId , i4FsEvcIndex ,pSetValFsEvcId)
#define nmhSetFsEvcType(i4FsEvcContextId , i4FsEvcIndex ,i4SetValFsEvcType) \
 nmhSetCmn(FsEvcType, 13, FsEvcTypeSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4FsEvcContextId , i4FsEvcIndex ,i4SetValFsEvcType)
#define nmhSetFsEvcCVlanIdPreservation(i4FsEvcContextId , i4FsEvcIndex ,i4SetValFsEvcCVlanIdPreservation) \
 nmhSetCmn(FsEvcCVlanIdPreservation, 13, FsEvcCVlanIdPreservationSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4FsEvcContextId , i4FsEvcIndex ,i4SetValFsEvcCVlanIdPreservation)
#define nmhSetFsEvcCVlanCoSPreservation(i4FsEvcContextId , i4FsEvcIndex ,i4SetValFsEvcCVlanCoSPreservation) \
 nmhSetCmn(FsEvcCVlanCoSPreservation, 13, FsEvcCVlanCoSPreservationSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4FsEvcContextId , i4FsEvcIndex ,i4SetValFsEvcCVlanCoSPreservation)
#define nmhSetFsEvcRowStatus(i4FsEvcContextId , i4FsEvcIndex ,i4SetValFsEvcRowStatus) \
 nmhSetCmn(FsEvcRowStatus, 13, FsEvcRowStatusSet, NULL, NULL, 0, 1, 2, "%i %i %i", i4FsEvcContextId , i4FsEvcIndex ,i4SetValFsEvcRowStatus)
#define nmhSetFsEvcL2CPTunnel(i4FsEvcContextId , i4FsEvcIndex ,pSetValFsEvcL2CPTunnel)  \
            nmhSetCmn(FsEvcL2CPTunnel, 13, FsEvcL2CPTunnelSet, NULL, NULL, 0, 0, 2, "%i %i %s", i4FsEvcContextId , i4FsEvcIndex ,pSetValFsEvcL2CPTunnel)
#define nmhSetFsEvcL2CPPeer(i4FsEvcContextId , i4FsEvcIndex ,pSetValFsEvcL2CPPeer)      \
            nmhSetCmn(FsEvcL2CPPeer, 13, FsEvcL2CPPeerSet, NULL, NULL, 0, 0, 2, "%i %i %s", i4FsEvcContextId , i4FsEvcIndex ,pSetValFsEvcL2CPPeer)
#define nmhSetFsEvcL2CPDiscard(i4FsEvcContextId , i4FsEvcIndex ,pSetValFsEvcL2CPDiscard)        \
            nmhSetCmn(FsEvcL2CPDiscard, 13, FsEvcL2CPDiscardSet, NULL, NULL, 0, 0, 2, "%i %i %s", i4FsEvcContextId , i4FsEvcIndex ,pSetValFsEvcL2CPDiscard)
#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsEvcFilterInstance[13];
extern UINT4 FsEvcFilterDestMacAddress[13];
extern UINT4 FsEvcFilterAction[13];
extern UINT4 FsEvcFilterId[13];
extern UINT4 FsEvcFilterRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsEvcFilterDestMacAddress(i4FsEvcContextId , i4FsEvcIndex , i4FsEvcFilterInstance ,SetValFsEvcFilterDestMacAddress) \
 nmhSetCmn(FsEvcFilterDestMacAddress, 13, FsEvcFilterDestMacAddressSet, NULL, NULL, 0, 0, 3, "%i %i %i %m", i4FsEvcContextId , i4FsEvcIndex , i4FsEvcFilterInstance ,SetValFsEvcFilterDestMacAddress)
#define nmhSetFsEvcFilterAction(i4FsEvcContextId , i4FsEvcIndex , i4FsEvcFilterInstance ,i4SetValFsEvcFilterAction) \
 nmhSetCmn(FsEvcFilterAction, 13, FsEvcFilterActionSet, NULL, NULL, 0, 0, 3, "%i %i %i %i", i4FsEvcContextId , i4FsEvcIndex , i4FsEvcFilterInstance ,i4SetValFsEvcFilterAction)
#define nmhSetFsEvcFilterId(i4FsEvcContextId , i4FsEvcIndex , i4FsEvcFilterInstance ,i4SetValFsEvcFilterId) \
 nmhSetCmn(FsEvcFilterId, 13, FsEvcFilterIdSet, NULL, NULL, 0, 0, 3, "%i %i %i %i", i4FsEvcContextId , i4FsEvcIndex , i4FsEvcFilterInstance ,i4SetValFsEvcFilterId)
#define nmhSetFsEvcFilterRowStatus(i4FsEvcContextId , i4FsEvcIndex , i4FsEvcFilterInstance ,i4SetValFsEvcFilterRowStatus) \
 nmhSetCmn(FsEvcFilterRowStatus, 13, FsEvcFilterRowStatusSet, NULL, NULL, 0, 1, 3, "%i %i %i %i", i4FsEvcContextId , i4FsEvcIndex , i4FsEvcFilterInstance ,i4SetValFsEvcFilterRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMefETreeIngresPort[13];
extern UINT4 FsMefETreeEvcIndex[13];
extern UINT4 FsMefETreeEgressPort[13];
extern UINT4 FsMefETreeRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMefETreeRowStatus(i4FsMefETreeIngresPort , i4FsMefETreeEvcIndex , i4FsMefETreeEgressPort ,i4SetValFsMefETreeRowStatus) \
 nmhSetCmn(FsMefETreeRowStatus, 13, FsMefETreeRowStatusSet, NULL, NULL, 0, 1, 3, "%i %i %i %i", i4FsMefETreeIngresPort , i4FsMefETreeEvcIndex , i4FsMefETreeEgressPort ,i4SetValFsMefETreeRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsUniCVlanEvcCVlanId[13];
extern UINT4 FsUniCVlanEvcEvcIndex[13];
extern UINT4 FsUniCVlanEvcRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsUniCVlanEvcEvcIndex(i4IfIndex , i4FsUniCVlanEvcCVlanId ,i4SetValFsUniCVlanEvcEvcIndex) \
 nmhSetCmn(FsUniCVlanEvcEvcIndex, 13, FsUniCVlanEvcEvcIndexSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4IfIndex , i4FsUniCVlanEvcCVlanId ,i4SetValFsUniCVlanEvcEvcIndex)
#define nmhSetFsUniCVlanEvcRowStatus(i4IfIndex , i4FsUniCVlanEvcCVlanId ,i4SetValFsUniCVlanEvcRowStatus) \
 nmhSetCmn(FsUniCVlanEvcRowStatus, 13, FsUniCVlanEvcRowStatusSet, NULL, NULL, 0, 1, 2, "%i %i %i", i4IfIndex , i4FsUniCVlanEvcCVlanId ,i4SetValFsUniCVlanEvcRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMefFilterNo[13];
extern UINT4 FsMefFilterDscp[13];
extern UINT4 FsMefFilterDirection[13];
extern UINT4 FsMefFilterEvc[13];
extern UINT4 FsMefFilterCVlanPriority[13];
extern UINT4 FsMefFilterIfIndex[13];
extern UINT4 FsMefFilterStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMefFilterDscp(i4FsMefFilterNo ,i4SetValFsMefFilterDscp) \
 nmhSetCmn(FsMefFilterDscp, 13, FsMefFilterDscpSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsMefFilterNo ,i4SetValFsMefFilterDscp)
#define nmhSetFsMefFilterDirection(i4FsMefFilterNo ,i4SetValFsMefFilterDirection) \
 nmhSetCmn(FsMefFilterDirection, 13, FsMefFilterDirectionSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsMefFilterNo ,i4SetValFsMefFilterDirection)
#define nmhSetFsMefFilterEvc(i4FsMefFilterNo ,i4SetValFsMefFilterEvc) \
 nmhSetCmn(FsMefFilterEvc, 13, FsMefFilterEvcSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsMefFilterNo ,i4SetValFsMefFilterEvc)
#define nmhSetFsMefFilterCVlanPriority(i4FsMefFilterNo ,i4SetValFsMefFilterCVlanPriority) \
 nmhSetCmn(FsMefFilterCVlanPriority, 13, FsMefFilterCVlanPrioritySet, NULL, NULL, 0, 0, 1, "%i %i", i4FsMefFilterNo ,i4SetValFsMefFilterCVlanPriority)
#define nmhSetFsMefFilterIfIndex(i4FsMefFilterNo ,i4SetValFsMefFilterIfIndex) \
 nmhSetCmn(FsMefFilterIfIndex, 13, FsMefFilterIfIndexSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsMefFilterNo ,i4SetValFsMefFilterIfIndex)
#define nmhSetFsMefFilterStatus(i4FsMefFilterNo ,i4SetValFsMefFilterStatus) \
 nmhSetCmn(FsMefFilterStatus, 13, FsMefFilterStatusSet, NULL, NULL, 0, 1, 1, "%i %i", i4FsMefFilterNo ,i4SetValFsMefFilterStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMefClassMapId[13];
extern UINT4 FsMefClassMapName[13];
extern UINT4 FsMefClassMapFilterId[13];
extern UINT4 FsMefClassMapCLASS[13];
extern UINT4 FsMefClassMapStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMefClassMapName(u4FsMefClassMapId ,pSetValFsMefClassMapName) \
 nmhSetCmn(FsMefClassMapName, 13, FsMefClassMapNameSet, NULL, NULL, 0, 0, 1, "%u %s", u4FsMefClassMapId ,pSetValFsMefClassMapName)
#define nmhSetFsMefClassMapFilterId(u4FsMefClassMapId ,u4SetValFsMefClassMapFilterId) \
 nmhSetCmn(FsMefClassMapFilterId, 13, FsMefClassMapFilterIdSet, NULL, NULL, 0, 0, 1, "%u %u", u4FsMefClassMapId ,u4SetValFsMefClassMapFilterId)
#define nmhSetFsMefClassMapCLASS(u4FsMefClassMapId ,u4SetValFsMefClassMapCLASS) \
 nmhSetCmn(FsMefClassMapCLASS, 13, FsMefClassMapCLASSSet, NULL, NULL, 0, 0, 1, "%u %u", u4FsMefClassMapId ,u4SetValFsMefClassMapCLASS)
#define nmhSetFsMefClassMapStatus(u4FsMefClassMapId ,i4SetValFsMefClassMapStatus) \
 nmhSetCmn(FsMefClassMapStatus, 13, FsMefClassMapStatusSet, NULL, NULL, 0, 1, 1, "%u %i", u4FsMefClassMapId ,i4SetValFsMefClassMapStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMefClassCLASS[13];
extern UINT4 FsMefClassStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMefClassStatus(u4FsMefClassCLASS ,i4SetValFsMefClassStatus) \
 nmhSetCmn(FsMefClassStatus, 13, FsMefClassStatusSet, NULL, NULL, 0, 1, 1, "%u %i", u4FsMefClassCLASS ,i4SetValFsMefClassStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMefMeterId[13];
extern UINT4 FsMefMeterName[13];
extern UINT4 FsMefMeterType[13];
extern UINT4 FsMefMeterColorMode[13];
extern UINT4 FsMefMeterCIR[13];
extern UINT4 FsMefMeterCBS[13];
extern UINT4 FsMefMeterEIR[13];
extern UINT4 FsMefMeterEBS[13];
extern UINT4 FsMefMeterStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMefMeterName(u4FsMefMeterId ,pSetValFsMefMeterName) \
 nmhSetCmn(FsMefMeterName, 13, FsMefMeterNameSet, NULL, NULL, 0, 0, 1, "%u %s", u4FsMefMeterId ,pSetValFsMefMeterName)
#define nmhSetFsMefMeterType(u4FsMefMeterId ,i4SetValFsMefMeterType) \
 nmhSetCmn(FsMefMeterType, 13, FsMefMeterTypeSet, NULL, NULL, 0, 0, 1, "%u %i", u4FsMefMeterId ,i4SetValFsMefMeterType)
#define nmhSetFsMefMeterColorMode(u4FsMefMeterId ,i4SetValFsMefMeterColorMode) \
 nmhSetCmn(FsMefMeterColorMode, 13, FsMefMeterColorModeSet, NULL, NULL, 0, 0, 1, "%u %i", u4FsMefMeterId ,i4SetValFsMefMeterColorMode)
#define nmhSetFsMefMeterCIR(u4FsMefMeterId ,u4SetValFsMefMeterCIR) \
 nmhSetCmn(FsMefMeterCIR, 13, FsMefMeterCIRSet, NULL, NULL, 0, 0, 1, "%u %u", u4FsMefMeterId ,u4SetValFsMefMeterCIR)
#define nmhSetFsMefMeterCBS(u4FsMefMeterId ,u4SetValFsMefMeterCBS) \
 nmhSetCmn(FsMefMeterCBS, 13, FsMefMeterCBSSet, NULL, NULL, 0, 0, 1, "%u %u", u4FsMefMeterId ,u4SetValFsMefMeterCBS)
#define nmhSetFsMefMeterEIR(u4FsMefMeterId ,u4SetValFsMefMeterEIR) \
 nmhSetCmn(FsMefMeterEIR, 13, FsMefMeterEIRSet, NULL, NULL, 0, 0, 1, "%u %u", u4FsMefMeterId ,u4SetValFsMefMeterEIR)
#define nmhSetFsMefMeterEBS(u4FsMefMeterId ,u4SetValFsMefMeterEBS) \
 nmhSetCmn(FsMefMeterEBS, 13, FsMefMeterEBSSet, NULL, NULL, 0, 0, 1, "%u %u", u4FsMefMeterId ,u4SetValFsMefMeterEBS)
#define nmhSetFsMefMeterStatus(u4FsMefMeterId ,i4SetValFsMefMeterStatus) \
 nmhSetCmn(FsMefMeterStatus, 13, FsMefMeterStatusSet, NULL, NULL, 0, 1, 1, "%u %i", u4FsMefMeterId ,i4SetValFsMefMeterStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMefPolicyMapId[13];
extern UINT4 FsMefPolicyMapName[13];
extern UINT4 FsMefPolicyMapCLASS[13];
extern UINT4 FsMefPolicyMapMeterTableId[13];
extern UINT4 FsMefPolicyMapStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMefPolicyMapName(u4FsMefPolicyMapId ,pSetValFsMefPolicyMapName) \
 nmhSetCmn(FsMefPolicyMapName, 13, FsMefPolicyMapNameSet, NULL, NULL, 0, 0, 1, "%u %s", u4FsMefPolicyMapId ,pSetValFsMefPolicyMapName)
#define nmhSetFsMefPolicyMapCLASS(u4FsMefPolicyMapId ,u4SetValFsMefPolicyMapCLASS) \
 nmhSetCmn(FsMefPolicyMapCLASS, 13, FsMefPolicyMapCLASSSet, NULL, NULL, 0, 0, 1, "%u %u", u4FsMefPolicyMapId ,u4SetValFsMefPolicyMapCLASS)
#define nmhSetFsMefPolicyMapMeterTableId(u4FsMefPolicyMapId ,u4SetValFsMefPolicyMapMeterTableId) \
 nmhSetCmn(FsMefPolicyMapMeterTableId, 13, FsMefPolicyMapMeterTableIdSet, NULL, NULL, 0, 0, 1, "%u %u", u4FsMefPolicyMapId ,u4SetValFsMefPolicyMapMeterTableId)
#define nmhSetFsMefPolicyMapStatus(u4FsMefPolicyMapId ,i4SetValFsMefPolicyMapStatus) \
 nmhSetCmn(FsMefPolicyMapStatus, 13, FsMefPolicyMapStatusSet, NULL, NULL, 0, 1, 1, "%u %i", u4FsMefPolicyMapId ,i4SetValFsMefPolicyMapStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMefMegIndex[13];
extern UINT4 FsMefMeIndex[13];
extern UINT4 FsMefMepIdentifier[13];
extern UINT4 FsMefMepTransmitLmmStatus[13];
extern UINT4 FsMefMepTransmitLmmInterval[13];
extern UINT4 FsMefMepTransmitLmmDeadline[13];
extern UINT4 FsMefMepTransmitLmmDropEnable[13];
extern UINT4 FsMefMepTransmitLmmPriority[13];
extern UINT4 FsMefMepTransmitLmmDestMacAddress[13];
extern UINT4 FsMefMepTransmitLmmDestMepId[13];
extern UINT4 FsMefMepTransmitLmmDestIsMepId[13];
extern UINT4 FsMefMepTransmitLmmMessages[13];
extern UINT4 FsMefMepNearEndFrameLossThreshold[13];
extern UINT4 FsMefMepFarEndFrameLossThreshold[13];
extern UINT4 FsMefMepTransmitDmStatus[13];
extern UINT4 FsMefMepTransmitDmType[13];
extern UINT4 FsMefMepTransmitDmInterval[13];
extern UINT4 FsMefMepTransmitDmMessages[13];
extern UINT4 FsMefMepTransmitDmmDropEnable[13];
extern UINT4 FsMefMepTransmit1DmDropEnable[13];
extern UINT4 FsMefMepTransmitDmmPriority[13];
extern UINT4 FsMefMepTransmit1DmPriority[13];
extern UINT4 FsMefMepTransmitDmDestMacAddress[13];
extern UINT4 FsMefMepTransmitDmDestMepId[13];
extern UINT4 FsMefMepTransmitDmDestIsMepId[13];
extern UINT4 FsMefMepTransmitDmDeadline[13];
extern UINT4 FsMefMepDmrOptionalFields[13];
extern UINT4 FsMefMep1DmRecvCapability[13];
extern UINT4 FsMefMepFrameDelayThreshold[13];
extern UINT4 FsMefMepRowStatus[13];
extern UINT4 FsMefMep1DmTransInterval[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMefMepTransmitLmmStatus(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepTransmitLmmStatus) \
 nmhSetCmn(FsMefMepTransmitLmmStatus, 13, FsMefMepTransmitLmmStatusSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepTransmitLmmStatus)
#define nmhSetFsMefMepTransmitLmmInterval(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepTransmitLmmInterval) \
 nmhSetCmn(FsMefMepTransmitLmmInterval, 13, FsMefMepTransmitLmmIntervalSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepTransmitLmmInterval)
#define nmhSetFsMefMepTransmitLmmDeadline(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,u4SetValFsMefMepTransmitLmmDeadline) \
 nmhSetCmn(FsMefMepTransmitLmmDeadline, 13, FsMefMepTransmitLmmDeadlineSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %u", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,u4SetValFsMefMepTransmitLmmDeadline)
#define nmhSetFsMefMepTransmitLmmDropEnable(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepTransmitLmmDropEnable) \
 nmhSetCmn(FsMefMepTransmitLmmDropEnable, 13, FsMefMepTransmitLmmDropEnableSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepTransmitLmmDropEnable)
#define nmhSetFsMefMepTransmitLmmPriority(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepTransmitLmmPriority) \
 nmhSetCmn(FsMefMepTransmitLmmPriority, 13, FsMefMepTransmitLmmPrioritySet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepTransmitLmmPriority)
#define nmhSetFsMefMepTransmitLmmDestMacAddress(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,SetValFsMefMepTransmitLmmDestMacAddress) \
 nmhSetCmn(FsMefMepTransmitLmmDestMacAddress, 13, FsMefMepTransmitLmmDestMacAddressSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %m", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,SetValFsMefMepTransmitLmmDestMacAddress)
#define nmhSetFsMefMepTransmitLmmDestMepId(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,u4SetValFsMefMepTransmitLmmDestMepId) \
 nmhSetCmn(FsMefMepTransmitLmmDestMepId, 13, FsMefMepTransmitLmmDestMepIdSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %u", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,u4SetValFsMefMepTransmitLmmDestMepId)
#define nmhSetFsMefMepTransmitLmmDestIsMepId(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepTransmitLmmDestIsMepId) \
 nmhSetCmn(FsMefMepTransmitLmmDestIsMepId, 13, FsMefMepTransmitLmmDestIsMepIdSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepTransmitLmmDestIsMepId)
#define nmhSetFsMefMepTransmitLmmMessages(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepTransmitLmmMessages) \
 nmhSetCmn(FsMefMepTransmitLmmMessages, 13, FsMefMepTransmitLmmMessagesSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepTransmitLmmMessages)
#define nmhSetFsMefMepNearEndFrameLossThreshold(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,u4SetValFsMefMepNearEndFrameLossThreshold) \
 nmhSetCmn(FsMefMepNearEndFrameLossThreshold, 13, FsMefMepNearEndFrameLossThresholdSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %u", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,u4SetValFsMefMepNearEndFrameLossThreshold)
#define nmhSetFsMefMepFarEndFrameLossThreshold(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,u4SetValFsMefMepFarEndFrameLossThreshold) \
 nmhSetCmn(FsMefMepFarEndFrameLossThreshold, 13, FsMefMepFarEndFrameLossThresholdSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %u", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,u4SetValFsMefMepFarEndFrameLossThreshold)
#define nmhSetFsMefMepTransmitDmStatus(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepTransmitDmStatus) \
 nmhSetCmn(FsMefMepTransmitDmStatus, 13, FsMefMepTransmitDmStatusSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepTransmitDmStatus)
#define nmhSetFsMefMepTransmitDmType(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepTransmitDmType) \
 nmhSetCmn(FsMefMepTransmitDmType, 13, FsMefMepTransmitDmTypeSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepTransmitDmType)
#define nmhSetFsMefMepTransmitDmInterval(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepTransmitDmInterval) \
 nmhSetCmn(FsMefMepTransmitDmInterval, 13, FsMefMepTransmitDmIntervalSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepTransmitDmInterval)
#define nmhSetFsMefMepTransmitDmMessages(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepTransmitDmMessages) \
 nmhSetCmn(FsMefMepTransmitDmMessages, 13, FsMefMepTransmitDmMessagesSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepTransmitDmMessages)
#define nmhSetFsMefMepTransmitDmmDropEnable(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepTransmitDmmDropEnable) \
 nmhSetCmn(FsMefMepTransmitDmmDropEnable, 13, FsMefMepTransmitDmmDropEnableSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepTransmitDmmDropEnable)
#define nmhSetFsMefMepTransmit1DmDropEnable(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepTransmit1DmDropEnable) \
 nmhSetCmn(FsMefMepTransmit1DmDropEnable, 13, FsMefMepTransmit1DmDropEnableSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepTransmit1DmDropEnable)
#define nmhSetFsMefMepTransmitDmmPriority(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepTransmitDmmPriority) \
 nmhSetCmn(FsMefMepTransmitDmmPriority, 13, FsMefMepTransmitDmmPrioritySet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepTransmitDmmPriority)
#define nmhSetFsMefMepTransmit1DmPriority(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepTransmit1DmPriority) \
 nmhSetCmn(FsMefMepTransmit1DmPriority, 13, FsMefMepTransmit1DmPrioritySet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepTransmit1DmPriority)
#define nmhSetFsMefMepTransmitDmDestMacAddress(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,SetValFsMefMepTransmitDmDestMacAddress) \
 nmhSetCmn(FsMefMepTransmitDmDestMacAddress, 13, FsMefMepTransmitDmDestMacAddressSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %m", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,SetValFsMefMepTransmitDmDestMacAddress)
#define nmhSetFsMefMepTransmitDmDestMepId(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,u4SetValFsMefMepTransmitDmDestMepId) \
 nmhSetCmn(FsMefMepTransmitDmDestMepId, 13, FsMefMepTransmitDmDestMepIdSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %u", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,u4SetValFsMefMepTransmitDmDestMepId)
#define nmhSetFsMefMepTransmitDmDestIsMepId(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepTransmitDmDestIsMepId) \
 nmhSetCmn(FsMefMepTransmitDmDestIsMepId, 13, FsMefMepTransmitDmDestIsMepIdSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepTransmitDmDestIsMepId)
#define nmhSetFsMefMepTransmitDmDeadline(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,u4SetValFsMefMepTransmitDmDeadline) \
 nmhSetCmn(FsMefMepTransmitDmDeadline, 13, FsMefMepTransmitDmDeadlineSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %u", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,u4SetValFsMefMepTransmitDmDeadline)
#define nmhSetFsMefMepDmrOptionalFields(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepDmrOptionalFields) \
 nmhSetCmn(FsMefMepDmrOptionalFields, 13, FsMefMepDmrOptionalFieldsSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepDmrOptionalFields)
#define nmhSetFsMefMep1DmRecvCapability(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMep1DmRecvCapability) \
 nmhSetCmn(FsMefMep1DmRecvCapability, 13, FsMefMep1DmRecvCapabilitySet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMep1DmRecvCapability)
#define nmhSetFsMefMepFrameDelayThreshold(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,u4SetValFsMefMepFrameDelayThreshold) \
 nmhSetCmn(FsMefMepFrameDelayThreshold, 13, FsMefMepFrameDelayThresholdSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %u", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,u4SetValFsMefMepFrameDelayThreshold)
#define nmhSetFsMefMepRowStatus(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepRowStatus) \
 nmhSetCmn(FsMefMepRowStatus, 13, FsMefMepRowStatusSet, NULL, NULL, 0, 1, 4, "%u %u %u %u %i", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepRowStatus)
#define nmhSetFsMefMep1DmTransInterval(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMep1DmTransInterval) \
 nmhSetCmn(FsMefMep1DmTransInterval, 13, FsMefMep1DmTransIntervalSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMep1DmTransInterval)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMefMepAvailabilityStatus[13];
extern UINT4 FsMefMepAvailabilityInterval[13];
extern UINT4 FsMefMepAvailabilityDeadline[13];
extern UINT4 FsMefMepAvailabilityLowerThreshold[13];
extern UINT4 FsMefMepAvailabilityUpperThreshold[13];
extern UINT4 FsMefMepAvailabilityModestAreaIsAvailable[13];
extern UINT4 FsMefMepAvailabilityWindowSize[13];
extern UINT4 FsMefMepAvailabilityDestMacAddress[13];
extern UINT4 FsMefMepAvailabilityDestMepId[13];
extern UINT4 FsMefMepAvailabilityDestIsMepId[13];
extern UINT4 FsMefMepAvailabilityType[13];
extern UINT4 FsMefMepAvailabilitySchldDownInitTime[13];
extern UINT4 FsMefMepAvailabilitySchldDownEndTime[13];
extern UINT4 FsMefMepAvailabilityPriority[13];
extern UINT4 FsMefMepAvailabilityDropEnable[13];
extern UINT4 FsMefMepAvailabilityRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMefMepAvailabilityStatus(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepAvailabilityStatus) \
 nmhSetCmn(FsMefMepAvailabilityStatus, 13, FsMefMepAvailabilityStatusSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepAvailabilityStatus)
#define nmhSetFsMefMepAvailabilityInterval(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepAvailabilityInterval) \
 nmhSetCmn(FsMefMepAvailabilityInterval, 13, FsMefMepAvailabilityIntervalSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepAvailabilityInterval)
#define nmhSetFsMefMepAvailabilityDeadline(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,u4SetValFsMefMepAvailabilityDeadline) \
 nmhSetCmn(FsMefMepAvailabilityDeadline, 13, FsMefMepAvailabilityDeadlineSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %u", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,u4SetValFsMefMepAvailabilityDeadline)
#define nmhSetFsMefMepAvailabilityLowerThreshold(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,pSetValFsMefMepAvailabilityLowerThreshold) \
 nmhSetCmn(FsMefMepAvailabilityLowerThreshold, 13, FsMefMepAvailabilityLowerThresholdSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %s", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,pSetValFsMefMepAvailabilityLowerThreshold)
#define nmhSetFsMefMepAvailabilityUpperThreshold(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,pSetValFsMefMepAvailabilityUpperThreshold) \
 nmhSetCmn(FsMefMepAvailabilityUpperThreshold, 13, FsMefMepAvailabilityUpperThresholdSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %s", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,pSetValFsMefMepAvailabilityUpperThreshold)
#define nmhSetFsMefMepAvailabilityModestAreaIsAvailable(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepAvailabilityModestAreaIsAvailable) \
 nmhSetCmn(FsMefMepAvailabilityModestAreaIsAvailable, 13, FsMefMepAvailabilityModestAreaIsAvailableSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepAvailabilityModestAreaIsAvailable)
#define nmhSetFsMefMepAvailabilityWindowSize(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,u4SetValFsMefMepAvailabilityWindowSize) \
 nmhSetCmn(FsMefMepAvailabilityWindowSize, 13, FsMefMepAvailabilityWindowSizeSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %u", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,u4SetValFsMefMepAvailabilityWindowSize)
#define nmhSetFsMefMepAvailabilityDestMacAddress(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,SetValFsMefMepAvailabilityDestMacAddress) \
 nmhSetCmn(FsMefMepAvailabilityDestMacAddress, 13, FsMefMepAvailabilityDestMacAddressSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %m", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,SetValFsMefMepAvailabilityDestMacAddress)
#define nmhSetFsMefMepAvailabilityDestMepId(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,u4SetValFsMefMepAvailabilityDestMepId) \
 nmhSetCmn(FsMefMepAvailabilityDestMepId, 13, FsMefMepAvailabilityDestMepIdSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %u", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,u4SetValFsMefMepAvailabilityDestMepId)
#define nmhSetFsMefMepAvailabilityDestIsMepId(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepAvailabilityDestIsMepId) \
 nmhSetCmn(FsMefMepAvailabilityDestIsMepId, 13, FsMefMepAvailabilityDestIsMepIdSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepAvailabilityDestIsMepId)
#define nmhSetFsMefMepAvailabilityType(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepAvailabilityType) \
 nmhSetCmn(FsMefMepAvailabilityType, 13, FsMefMepAvailabilityTypeSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepAvailabilityType)
#define nmhSetFsMefMepAvailabilitySchldDownInitTime(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,u4SetValFsMefMepAvailabilitySchldDownInitTime) \
 nmhSetCmn(FsMefMepAvailabilitySchldDownInitTime, 13, FsMefMepAvailabilitySchldDownInitTimeSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %u", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,u4SetValFsMefMepAvailabilitySchldDownInitTime)
#define nmhSetFsMefMepAvailabilitySchldDownEndTime(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,u4SetValFsMefMepAvailabilitySchldDownEndTime) \
 nmhSetCmn(FsMefMepAvailabilitySchldDownEndTime, 13, FsMefMepAvailabilitySchldDownEndTimeSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %u", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,u4SetValFsMefMepAvailabilitySchldDownEndTime)
#define nmhSetFsMefMepAvailabilityPriority(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,u4SetValFsMefMepAvailabilityPriority) \
 nmhSetCmn(FsMefMepAvailabilityPriority, 13, FsMefMepAvailabilityPrioritySet, NULL, NULL, 0, 0, 4, "%u %u %u %u %u", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,u4SetValFsMefMepAvailabilityPriority)
#define nmhSetFsMefMepAvailabilityDropEnable(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepAvailabilityDropEnable) \
 nmhSetCmn(FsMefMepAvailabilityDropEnable, 13, FsMefMepAvailabilityDropEnableSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepAvailabilityDropEnable)
#define nmhSetFsMefMepAvailabilityRowStatus(u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepAvailabilityRowStatus) \
 nmhSetCmn(FsMefMepAvailabilityRowStatus, 13, FsMefMepAvailabilityRowStatusSet, NULL, NULL, 0, 1, 4, "%u %u %u %u %i", u4FsMefContextId , u4FsMefMegIndex , u4FsMefMeIndex , u4FsMefMepIdentifier ,i4SetValFsMefMepAvailabilityRowStatus)

#endif
