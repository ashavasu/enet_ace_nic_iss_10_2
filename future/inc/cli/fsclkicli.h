/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsclkicli.h,v 1.3 2012/11/27 10:42:11 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsClkIwfClockVariance[12];
extern UINT4 FsClkIwfClockClass[12];
extern UINT4 FsClkIwfClockAccuracy[12];
extern UINT4 FsClkIwfClockTimeSource[12];
extern UINT4 FsClkIwfCurrentUtcOffset[12];
extern UINT4 FsClkIwfARBTime[12];
extern UINT4 FsClkIwfHoldoverSpecification[12];
extern UINT4 FsClkIwfUtcOffset[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsClkIwfClockVariance(i4SetValFsClkIwfClockVariance) \
 nmhSetCmn(FsClkIwfClockVariance, 12, FsClkIwfClockVarianceSet, ClkMainLock, ClkMainUnLock, 0, 0, 0, "%i", i4SetValFsClkIwfClockVariance)
#define nmhSetFsClkIwfClockClass(i4SetValFsClkIwfClockClass) \
 nmhSetCmn(FsClkIwfClockClass, 12, FsClkIwfClockClassSet, ClkMainLock, ClkMainUnLock, 0, 0, 0, "%i", i4SetValFsClkIwfClockClass)
#define nmhSetFsClkIwfClockAccuracy(i4SetValFsClkIwfClockAccuracy) \
 nmhSetCmn(FsClkIwfClockAccuracy, 12, FsClkIwfClockAccuracySet, ClkMainLock, ClkMainUnLock, 0, 0, 0, "%i", i4SetValFsClkIwfClockAccuracy)
#define nmhSetFsClkIwfClockTimeSource(i4SetValFsClkIwfClockTimeSource) \
 nmhSetCmn(FsClkIwfClockTimeSource, 12, FsClkIwfClockTimeSourceSet, ClkMainLock, ClkMainUnLock, 0, 0, 0, "%i", i4SetValFsClkIwfClockTimeSource)
#define nmhSetFsClkIwfCurrentUtcOffset(pSetValFsClkIwfCurrentUtcOffset) \
 nmhSetCmn(FsClkIwfCurrentUtcOffset, 12, FsClkIwfCurrentUtcOffsetSet, ClkMainLock, ClkMainUnLock, 1, 0, 0, "%s", pSetValFsClkIwfCurrentUtcOffset)
#define nmhSetFsClkIwfARBTime(pSetValFsClkIwfARBTime) \
 nmhSetCmn(FsClkIwfARBTime, 12, FsClkIwfARBTimeSet, ClkMainLock, ClkMainUnLock, 0, 0, 0, "%s", pSetValFsClkIwfARBTime)
#define nmhSetFsClkIwfHoldoverSpecification(i4SetValFsClkIwfHoldoverSpecification) \
 nmhSetCmn(FsClkIwfHoldoverSpecification, 12, FsClkIwfHoldoverSpecificationSet, ClkMainLock, ClkMainUnLock, 0, 0, 0, "%i", i4SetValFsClkIwfHoldoverSpecification)
#define nmhSetFsClkIwfUtcOffset(pSetValFsClkIwfUtcOffset) \
 nmhSetCmn(FsClkIwfUtcOffset, 12, FsClkIwfUtcOffsetSet, ClkMainLock, ClkMainUnLock, 0, 0, 0, "%s", pSetValFsClkIwfUtcOffset)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsClkIwfNotification[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsClkIwfNotification(pSetValFsClkIwfNotification) \
 nmhSetCmn(FsClkIwfNotification, 11, FsClkIwfNotificationSet, ClkMainLock, ClkMainUnLock, 0, 0, 0, "%s", pSetValFsClkIwfNotification)

#endif
