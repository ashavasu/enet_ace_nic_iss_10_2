/* $Id: elpscli.h,v 1.14 2016/01/12 13:02:53 siva Exp $ */


#ifndef __ELPSCLI_H__
#define __ELPSCLI_H__

#include "cli.h"

/* 
 * ELPS CLI command constants. To add a new command ,a new definition needs to
 * be added to the list.
 */


enum {
    CLI_ELPS_SHUTDOWN = 1,
    CLI_ELPS_START,
    CLI_ELPS_ENABLE,
    CLI_ELPS_DISABLE,
    CLI_ELPS_PG_CREATE,
    CLI_ELPS_PG_DELETE,
    CLI_ELPS_NOTIFY_ENABLE,
    CLI_ELPS_NOTIFY_DISABLE,
    CLI_ELPS_SET_GROUP_MANAGER,
    CLI_ELPS_PG_ACTIVATE,
    CLI_ELPS_PG_DEACTIVATE,
    CLI_ELPS_PG_HOLDOFF_TIME,
    CLI_ELPS_PG_WTR_TIME,
    CLI_ELPS_PG_TIMERS,
    CLI_ELPS_PG_OPER_TYPE_AND_WTR, 
    CLI_ELPS_PG_WORKING_INFO, 
    CLI_ELPS_PG_PROTECT_INFO, 
    CLI_ELPS_PG_INGRESS_PORT,
    CLI_ELPS_PG_SET_NAME, 
    CLI_ELPS_PG_RESET_NAME, 
    CLI_ELPS_PG_MONITOR_MECH,
    CLI_ELPS_PG_WRKG_ECFM_CFG,
    CLI_ELPS_PG_PROT_ECFM_CFG,
    CLI_ELPS_PG_FS,
    CLI_ELPS_PG_MS_PROTECTION,
    CLI_ELPS_PG_MS_WORKING,
    CLI_ELPS_PG_LOP,
    CLI_ELPS_PG_EXERCISE,
    CLI_ELPS_PG_CLEAR_EXT_CMD,
    CLI_ELPS_PG_CLEAR,
    CLI_ELPS_PG_FREEZE,
    CLI_ELPS_PG_CLEAR_FREEZE,
    CLI_ELPS_GBL_DEBUG,
    CLI_ELPS_NO_GBL_DEBUG,
    CLI_ELPS_DEBUG,
    CLI_ELPS_NO_DEBUG,
    CLI_ELPS_SHOW_GLB_INFO,
    CLI_ELPS_SHOW_PG_INFO,
    CLI_ELPS_SHOW_LIST_PROT,
    CLI_ELPS_SHOW_DEBUG, 
    CLI_ELPS_CLEAR_STATS_ALL,
    CLI_ELPS_RAPID_TX_TIME,
    CLI_ELPS_CHANNEL_CODE,
    CLI_ELPS_PG_LSP_INFO,
    CLI_ELPS_PG_LSP_LIST_INFO,
    CLI_ELPS_PG_PW_INFO,
    CLI_ELPS_PG_PW_LIST_INFO,
    CLI_ELPS_MAP_WORKING_VLAN_GROUP_TO_PG,
    CLI_ELPS_MAP_PROTECTION_VLAN_GROUP_TO_PG,
    CLI_ELPS_UNMAP_WORKING_VLAN_GROUP,
    CLI_ELPS_UNMAP_PROTECTION_VLAN_GROUP,
    CLI_ELPS_PG_PROT_TYPE,
 CLI_ELPS_PG_LSP_REV_INFO,
};

enum {
    CLI_ELPS_UNKNOWN_ERR = 1,
    CLI_ELPS_ERR_MODULE_SHUTDOWN,
    CLI_ELPS_ERR_PG_ACTIVE,
    CLI_ELPS_ERR_MODULE_DISABLED,
    CLI_ELPS_ERR_PG_NOT_ACTIVE,
    CLI_ELPS_ERR_PG_ALREADY_CREATED,
    CLI_ELPS_ERR_PG_NOT_READY,
    CLI_ELPS_ERR_PG_CFM_ENTRY_NOT_ACTIVE,
    CLI_ELPS_ERR_PG_SERVICE_LIST_ENTRY_NOT_CREATED,
    CLI_ELPS_ERR_CFM_ROW_ACTIVE,
    CLI_ELPS_ERR_CFM_NOT_READY,
    CLI_ELPS_ERR_PORT_UNMAPPED,
    CLI_ELPS_ERR_PG_FREEZED,
    CLI_ELPS_ERR_PGNAME_ALREADY_USED,
    CLI_ELPS_ERR_MEG_ME_ZERO,
    CLI_ELPS_ERR_MSW_INVALID_IN_REVERTIVE,
    CLI_ELPS_ERR_CMD_REJECTED,
    CLI_ELPS_ERR_MONITOR_INCONSISTENT,
    CLI_ELPS_ERR_ALL_NOT_SUPPORTED,
    CLI_ELPS_ERR_SUPPORT_IN_DEFAULT_CXT,
    CLI_ELPS_ERR_SRV_VAL_NOT_SUPPORTED,
    CLI_ELPS_ERR_SRV_PTR_NOT_SUPPORTED,
    CLI_ELPS_ERR_SRV_PTR_TABLE_NOT_SUPPORTED,
    CLI_ELPS_ERR_RVR_SRV_PTR_NOT_SUPPORTED,
    CLI_ELPS_ERR_RVR_CFM_PTR_NOT_SUPPORTED,
    CLI_ELPS_ERR_INVALID_PG_ID,
    CLI_ELPS_ERR_PG_NOT_CREATE,
    CLI_ELPS_ERR_WORKING_CFM_ENTRY_ALREADY_USED,
    CLI_ELPS_ERR_GROUP_ID_EXIST,
    CLI_ELPS_MAX_ERR
};

#ifdef _ELPSCLI_C_

CONST CHR1  *gapc1ElpsCliErrString [] = {
    NULL,
    "Unknown Error\r\n",
    "Module is ShutDown \r\n",
    "Protection Group to be de-activated to change this parameter \r\n",
    "External Command cannot be applied when module is disabled \r\n",
    "Protection Group should be active to apply an External Command \r\n",
    "Protection Group is already created \r\n",
    "Mandatory parameters needed to activate Protection Group are not applied" 
        "\r\n",
    "Cfm Parameters such as Working and Protection MEG, ME and MA are not yet"
        " applied, hence cannot activate protection group \r\n",
    "Service List is not configured for the List type Protection Group\r\n",
    "Protection Group Cfm parameters cannot be applied, when the entry is" 
        " active \r\n",
    "Mandatory parameters needed to activate Protection Group Cfm entry are "
        "not applied\r\n",
    "Port is not mapped to this context\r\n",
    "Protection Group is frozen using Freeze,no external command accepted\r\n",
    "Protection Group name is already configured for some other Protection " 
        "Group\r\n",
    "Value of Working or Protection MEG or ME cannot be zero\r\n",
    "Manual Switch to Working command is invalid in Revertive mode\r\n",
    "Existing command or Last received far end command has higher priority, "
         "hence command rejected \r\n",
    "Monitoring method for Protection Group is not compatible with Service "
        "Type\r\n",
    "Protection Group Config Type 'ALL' is not valid for LSP/PW services\r\n",
    "Protection Group Service Type MPLS-LSP/PW"
         " is supported only in default context\r\n",
    "Protection Group - Service Value"
        " is supported only for VLAN services\r\n",
    "Protection Group - Service Pointer is supported only for "
        "MPLS-LSP/PW services\r\n",
    "Protection Group - Service Pointer Table is supported only for "
        "MPLS-LSP/PW services\r\n",
    "Protection Group - Reverse Service Pointer is supported only for "
        "MPLS-LSP services\r\n",
    "Protection Group - Reverse MEG/ME/MEP is not valid"
        " for VLAN/PW services\r\n",
 "Invalid Group Id\r\n", 
 "Protection Group is not created\r\n",
    "Working CFM entries are already used by some other Protection Group\r\n",
    "Working group Id is already configured for other Protection Group Entry\r\n",
    "\r\n"
};

#else
extern CONST CHR1  *gapc1ElpsCliErrString [];
#endif

enum {
    ELPS_CLI_SHOW_PG_ALL    = 0,
    ELPS_CLI_SHOW_PG_CONFIG = 1,
    ELPS_CLI_SHOW_PG_STATUS = 2,
    ELPS_CLI_SHOW_PG_STATS  = 3,
    ELPS_CLI_SHOW_PG_TIMER  = 4,
    ELPS_CLI_SHOW_PG_WE     = 5,
    ELPS_CLI_SHOW_PG_PE     = 6,
    ELPS_CLI_MAX_SHOW_CMDS  = 7
};
#define ELPS_CLI_INVALID_CONTEXT 0xffffffff
#define ELPS_CLI_MAX_ARGS       15

#define ELPS_SET_CMD            1
#define ELPS_NO_CMD             2

#define CLI_ELPS_PG_CLR_FS      1
#define CLI_ELPS_PG_CLR_MS      2
#define CLI_ELPS_PG_CLR_LOP     3
#define CLI_ELPS_PG_CLR_EXER    4
#define CLI_ELPS_PG_CLR_MS_W    5

#define CLI_ELPS_PG_MODE  "pg-"

INT4
cli_process_elps_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);

INT4
cli_process_elps_sh_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);

INT1
ElpsGetElpsCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

INT1
ElpsGetVcmElpsCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

INT4
ElpsCliSetSystemControl (tCliHandle CliHandle, UINT4 u4ContextId,
                         INT4 i4Status);

INT4
ElpsCliSetModuleStatus (tCliHandle CliHandle, UINT4 u4ContextId,
                        INT4 i4Status);

INT4
ElpsCliConfigProtectionGroup (tCliHandle CliHandle, UINT4 u4ContextId,                                                        UINT4 u4PgId, UINT1 u1Action);

INT4
ElpsCliSetNotifyStatus (tCliHandle CliHandle, UINT4 u4ContextId,
                        INT4 i4TrapStatus);

INT4
ElpsCliSetPgEnableStatus (tCliHandle CliHandle,UINT4 u4ContextId,
                          UINT4 u4PgId, INT4 i4PgEnableStatus);

INT4
ElpsCliSetPgOperTypeAndWTRTime (tCliHandle CliHandle, UINT4 u4ContextId, 
                                UINT4 u4PgId, INT4 i4OperType, UINT4 u4WTRTime);

INT4
ElpsCliSetPgTimers (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4PgId,
                    UINT4 u4PeriodicTime, UINT4 u4HoldOffTime);

INT4
ElpsCliSetPgHoldOffTime (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4PgId,
                         UINT4 u4HoldOffTime);

INT4
ElpsCliSetPgWTRTime (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4PgId,
                     UINT4 u4WTRTime);

INT4
ElpsCliSetPgIngressPort (tCliHandle CliHandle, UINT4 u4ContextId,
                         UINT4 u4PgId, UINT4 u4IngressPortId);

INT4
ElpsCliSetPgServiceType (tCliHandle CliHandle, UINT4 u4ContextId,
                         UINT4 u4PgId, INT4 i4ServiceType);

INT4
ElpsCliSetWorkingPort (tCliHandle CliHandle, UINT4 u4ContextId,
                       UINT4 u4PgId, UINT4 u4IfIndex);

INT4
ElpsCliSetWorkingService (tCliHandle CliHandle, UINT4 u4ContextId,
                          UINT4 u4PgId, UINT4 u4ServiceId);

INT4
ElpsCliSetWorkingServiceList (tCliHandle CliHandle, UINT4 u4ContextId,
                              UINT4 u4PgId, UINT1 *pu1SrvList);

INT4
ElpsCliSetWorkingServiceAsAll (tCliHandle CliHandle, UINT4 u4ContextId,
                               UINT4 u4PgId);
INT4 ElpsCliSetDebugLevel (tCliHandle CliHandle, INT4 i4CliDebugLevel);
INT4
ElpsCliTestForWorkingInfo (UINT4 u4ContextId, UINT4 u4PgId, 
                           UINT4 u4ServiceListId);

INT4
ElpsCliTestForConfigType (tCliHandle CliHandle, UINT4 u4ContextId,
                          UINT4 u4PgId, INT4 i4CfgType);

INT4
ElpsCliSetProtectionPort (tCliHandle CliHandle, UINT4 u4ContextId,
                          UINT4 u4PgId, UINT4 u4IfIndex);

INT4
ElpsCliSetProtectionService (tCliHandle CliHandle, UINT4 u4ContextId,
                             UINT4 u4PgId, UINT4 u4ServiceId);

INT4
ElpsCliSetProtectionServiceList (tCliHandle CliHandle, UINT4 u4ContextId,
                                 UINT4 u4PgId, UINT1 *pu1SrvList);

INT4
ElpsCliSetPgMonitor (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4PgId,
                     INT4 i4PgMonitor);

INT4
ElpsCliSetWorkingCfmCfg (tCliHandle CliHandle,
                         UINT4 u4ContextId,
                         UINT4 u4PgId,
                         UINT4 u4WorkingMegId,
                         UINT4 u4WorkingMeId,
                         UINT4 u4WorkingMepId,
       UINT4 u4Direction);

INT4
ElpsCliSetProtectionCfmCfg (tCliHandle CliHandle,
                            UINT4 u4ContextId,
                            UINT4 u4PgId,
                            UINT4 u4ProtectionMegId,
                            UINT4 u4ProtetionMeId,
                            UINT4 u4ProtectionMepId,
        UINT4 u4Direction);

INT4
ElpsCliCfgLocalCommand (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4PgId,
                        INT4 i4ExtCmd);

INT4
ElpsCliCfgClearExtCmd (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4PgId, 
                       INT4 i4Command);

INT4
ElpsCliSetGlobalDebug (tCliHandle CliHandle, INT4 i4GlobalTraceOption);

INT4
ElpsCliGetContextInfoForShowCmd (tCliHandle CliHandle, UINT1 * pu1ContextName,
                                 UINT4 u4CurrContextId, UINT4 * pu4ContextId, 
                                 UINT4 u4Command);

INT4
ElpsCliShowGlobalInfo (tCliHandle CliHandle, UINT4 u4ContextId);

INT4
ElpsCliConfigPgName (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4PgId,
                     UINT1 * pu1PgName);

INT4
ElpsCliClearPgStats (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4PgId);


INT4
ElpsCliShowPgInfo (tCliHandle CliHandle, UINT4 u4ContextId,
                         UINT4 u4PgId, INT4 i4ShowCmdType);

VOID
ElpsCliShowPgConfigurations (tCliHandle CliHandle, UINT4 u4ContextId, 
                             UINT4 u4PgId, 
                             UINT4 *pu4PagingStatus);

VOID
ElpsCliShowPgStatus (tCliHandle CliHandle, UINT4 u4ContextId, 
                     UINT4 u4PgId, UINT4 *pu4PagingStatus);

VOID
ElpsCliShowPgStatistics (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4PgId,
                         UINT4 *pu4PagingStatus);

VOID
ElpsCliShowPgTimerInfo (tCliHandle CliHandle, UINT4 u4ContextId, 
                        UINT4 u4PgId, UINT4 *pu4PagingStatus);

VOID
ElpsCliShowPgWorkingEntity (tCliHandle CliHandle, UINT4 u4ContextId, 
                            UINT4 u4PgId,
                            UINT4 *pu4PagingStatus);

VOID
ElpsCliShowPgProtectionEntity (tCliHandle CliHandle, UINT4 u4ContextId, 
                               UINT4 u4PgId,
                               UINT4 *pu4PagingStatus);

VOID
ElpsCliShowAllPgInfo (tCliHandle CliHandle, UINT4 u4ContextId, 
                      UINT4 u4NextPgId, 
                      UINT4 *pu4PagingStatus);
INT4
ElpsCliShowPgListInProtPort (tCliHandle CliHandle, UINT4 u4IfIndex);

INT4
ElpsCliSetDebugs (tCliHandle CliHandle, UINT4 u4ContextId,
                  UINT1 *pu1TraceInput);

INT4
ElpsCliPrintServiceList (tCliHandle CliHandle, UINT4 u4ContextId, 
                          UINT4 u4PgId);

VOID
ElpsCliShowServiceListForPg (tCliHandle CliHandle, UINT1 *pVlanList);

VOID
ElpsCliPrintServices (tCliHandle CliHandle, INT4 i4CommaCount, 
                      UINT2 u2ServiceId);

INT4
ElpsCliShowRunningConfig (tCliHandle CliHandle, UINT4 u4ContextId);

INT4
ElpsCliShowDebugging (tCliHandle CliHandle, UINT4 u4ContextId);

INT4
ElpsCliSetRapidTxTime(tCliHandle CliHandle, UINT4 u4RapidTxTime);


INT4
ElpsCliSetChannelCode(tCliHandle CliHandle, UINT4 u4ChannelCode);

INT4
ElpsCliSetLspService(tCliHandle CliHandle, UINT4 u4ContextId,
      UINT4 u4PgId, UINT4 u4EntityType,
      UINT4 u4TnlIndex, UINT4 u4TnlInstance,
      UINT4 u4TnlIngressLsrId, UINT4 u4TnlEgressLsrId,
      UINT4 u4RvrTnlIndex, UINT4 u4RvrTnlInstance,
      UINT4 u4RvrTnlIngressLsrId, UINT4 u4RvrTnlEgressLsrId);



INT4
ElpsCliSetLspList (tCliHandle CliHandle, UINT4 u4ContextId,
             UINT4 u4PgId, UINT4 u4ListId, UINT4 u4EntityType,
       UINT4 u4TnlIndex, UINT4 u4TnlInstance,
       UINT4 u4TnlIngressLsrId, UINT4 u4TnlEgressLsrId,
       UINT4 u4RvrTnlIndex, UINT4 u4RvrTnlInstance,
       UINT4 u4RvrTnlIngressLsrId, UINT4 u4RvrTnlEgressLsrId);

INT4
ElpsCliSetPwService(tCliHandle CliHandle, UINT4 u4ContextId,
      UINT4 u4PgId, UINT4 u4EntityType,
                     UINT4 u4VcId, UINT4 u4PeerAddr,
                     UINT4 u4AddressType);

INT4
ElpsCliSetPwList (tCliHandle CliHandle, UINT4 u4ContextId,
             UINT4 u4PgId, UINT4 u4ListId, UINT4 u4EntityType,
                   UINT4 u4VcId, UINT4 u4PeerAddr,
                   UINT4 u4AddressType);

INT4 ElpsCliDeleteList (tCliHandle CliHandle, UINT4 u4ContextId,
            UINT4 u4PgId, UINT4 u4ListId);

INT4
ElpsCliPrintWorkingServiceList (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4PgId);

INT4
ElpsCliPrintProtectionSrvList (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4PgId);

INT4
ElpsCliSetVlanGroupManager(tCliHandle CliHandle, UINT4 u4ContextId,
                           INT4 i4VlanGroupManager);
INT4
ElpsCliSetWorkingVlanInstanceToPg(tCliHandle CliHandle, UINT4 u4ContextId,
                  UINT4 u4PgId,UINT4 u4GroupId);
INT4
ElpsCliSetProtectionVlanInstanceToPg(tCliHandle CliHandle, UINT4 u4ContextId,
                  UINT4 u4PgId,UINT4 u4GroupId);
INT4
ElpsCliUnmapWorkingVlanInstanceToPg(tCliHandle CliHandle, UINT4 u4ContextId,
                  UINT4 u4PgId, UINT4 u4GroupId);
INT4
ElpsCliUnmapProtectionVlanInstanceToPg(tCliHandle CliHandle, UINT4 u4ContextId,
                  UINT4 u4PgId, UINT4 u4GroupId);
INT4
ElpsCliSetPgProtType (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4PgId, 
                      INT4 i4ProtType);
INT4
ElpsCliSetPgPscVer (tCliHandle CliHandle, UINT4 u4ContextId,
                                               UINT4 u4PgId, UINT4 u4PscVer);

#ifdef ELPS_TEST_WANTED
INT4 cli_process_elps_test_cmd (tCliHandle CliHandle,...);
#endif

#endif /* __ELPSCLI_H__ */
