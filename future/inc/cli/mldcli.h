/* $Id: mldcli.h,v 1.12 2015/06/30 09:25:12 siva Exp $*/
#ifndef __MLDCLI_H__
#define __MLDCLI_H__

#include "lr.h"
#include "cli.h"
/* MLD CLI command constants. To add a new command ,a new definition needs to
 * be added to the list.
 */
enum
{
    MLD_CLI_STATUS = 1,
    MLD_CLI_IFACE_STATUS,
    MLD_CLI_FAST_LEAVE,
    MLD_CLI_VERSION,
    MLD_CLI_NO_VERSION,
    MLD_CLI_QRY_INTRVL,
    MLD_CLI_QRY_MAXRESPTIME,
    MLD_CLI_ROBUSTNESS,
    MLD_CLI_LAST_MBRQRYINTRL,
    MLD_CLI_DEL_INTERFACE,
    MLD_CLI_RATE_LIMIT,
    MLD_CLI_NO_RATE_LIMIT,
    MLD_CLI_IF_RATE_LIMIT,
    MLD_CLI_IF_NO_RATE_LIMIT,
    MLD_CLI_SHOW_GBL_CONFIG,
    MLD_CLI_SHOW_INTF_CONFIG,
    MLD_CLI_SHOW_GROUPS,
    MLD_CLI_SHOW_SOURCES,
    MLD_CLI_SHOW_STATS,
    MLD_CLI_CLEAR_STATS,
    MLD_CLI_TRACE,
    MLD_CLI_DEBUG
};

#define MLD_CLI_MAX_COMMANDS             18

#define MLD_CFA_INTF_NAME                64
#define MLD_MAX_ARGS                      5
/*regarding the MGMD mib IGMPv2=MLDv1; IGMPv3=MLDv2 */
#define   CLI_MLD_V1                               2
#define   CLI_MLD_V2                               3
#define CLI_MLD_TRACE_DIS_FLAG                     0x80000000


INT1 cli_process_mld_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);

enum {
    CLI_MLD_UNKNOWN_ERR = 1,
    CLI_MLD_INTERFACE_NOT_FOUND,
    CLI_MLD_VERSION_TO_2,
    CLI_MLD_PROXY_UPSTREAM,
    CLI_MLD_QUERY_INTERVAL_ERR,
    CLI_MLD_MAX_RESP_ERR,
    CLI_MLD_NOT_ENABLE,
    CLI_MLD_NOT_DISABLE,
    CLI_MLD_VERSION_ERR,
    CLI_MLD_MAX_ERR
};

#ifdef __MLDCLI_C__
CONST CHR1 *MldCliErrString [] = {
    NULL,
    "Unknown Error\r\n",
    "MLD is already disabled\r\n",
    "Configure the version to v2 \r\n",
    "Interface is configured as MLD Proxy upstream interface \r\n",
    "Query Interval must be greater than one tenth of"
    " Max Response Time \r\n",
    "One tenth of Max Response Time must be less than Query Interval\r\n",
    " Enable MLD failed \r\n",
    " Disable MLD failed \r\n",
    " MLD version should be either 1 or 2\r\n",
    "\r\n"
};  
#endif

#endif /* __MLDCLI_H__ */

