/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rmcli.h,v 1.16 2014/06/27 11:18:40 siva Exp $
 *
 * Description: RM CLI dcommands specific defintions 
 * 
 *******************************************************************/
#ifndef _RMCLI_H
#define _RMCLI_H

#include "cli.h"
#include "rmgr.h"

/* RM command identifiers */
enum {
    RM_CLI_SHOW_CONF = 1,
    RM_CLI_SHOW_STATS,
    RM_CLI_CLEAR_STATS,
    RM_CLI_SET_HB_INT,
    RM_CLI_SET_PEER_DEAD_INT,
    RM_CLI_SET_PEER_DEAD_INT_MULTI,
    RM_CLI_SET_FSW,
    RM_CLI_CHANGE_CFG_MODE,
    RM_CLI_DEBUG,
    RM_CLI_MOD_DEBUG,
    RM_CLI_NO_DEBUG,
    RM_CLI_MOD_NO_DEBUG,
    RM_CLI_SET_NODE_STATUS,
    RM_CLI_STACK,
    RM_CLI_NO_STACK,
    RM_CLI_SHOW_LINK_STATUS,
    RM_CLI_SHOW_STACK,
    RM_CLI_SET_PRIORITY,
    RM_TRIGGER_DYNAMIC_SYNC_AUDIT, 
    RM_SHOW_DYN_AUDIT_STATUS,
    RM_CLI_SET_RESTART_ENABLE,
    RM_CLI_SET_RESTART_DISABLE,
    RM_CLI_SET_RESTART_COUNT,
    /* HITLESS RESTART */
    RM_CLI_HR_ENABLE,
    RM_CLI_SHOW_SWOVER_TIME,
    RM_CLI_SET_RM_STACKING_IP_MASK,
    RM_CLI_MAX_CLI_CMDS,
    RM_CLI_SET_INITIATE
};

/* Error code values and strings are maintained 
 * inside the protocol module itself.
 */
enum {
    CLI_RM_FSW_ALLOWED_ONLY_IN_ACTIVE = 1,
    CLI_RM_NOT_ALLOWED_ON_STANDBY_DOWN,
    CLI_RM_NOT_ALLOWED_DURING_PROTOCOL_SYNCUP,
    CLI_RM_INVALID_DEBUG_TRC_LEVEL,
    CLI_RM_INVALID_MODULE_DEBUG,
    CLI_RM_DYN_AUDIT_NOT_COMPLETE,
    CLI_RM_DYN_AUDIT_MOD_VALUE,
    CLI_RM_MAX_ERR
};

/* The error strings should be places under the switch 
 * so as to avoid redifinition.
 * This will be visible only in modulecli.c file
 */
#define RM_PREFERRED_MASTER 100
#define RM_BACKUP_MASTER     50
#define RM_PREFERRED_SLAVE   25

#define RM_COLDSTANDBY_ENABLE  1
#define RM_COLDSTANDBY_DISABLE 2



#define RM_COLDSTANDBY_STACK_DETAILS       1
#define RM_COLDSTANDBY_STACK_COUNTERS      2
#define RM_COLDSTANDBY_STACK_PEER          3
#define RM_COLDSTANDBY_STACK_BRIEF         4

#ifdef __RMCLI_C__
CONST CHR1  *RmCliErrString [] = {
    NULL,
    "Force Switchover is allowed only in the ACTIVE node\r\n",
    "Force Switchover is not allowed when the STANDBY node is down\r\n",
    "Force Switchover is not allowed during protocol synchronization\r\n",
    "Invalid debugging trace level specified\r\n",
    "Invalid module debugging trace specified\r\n",
    "Dynamic Synch audit is not completed by all the modules\r\n",
    "Invalid Module name specified for a dynamic Synch audit\n\n",
};
#endif

#define   RM_CLI_MAX_ARGS     3

/* RM config mode string */
#define   RM_CLI_CFG_MODE "(config-r)#"

/* Function Prototypes */
INT4  CliProcessRmCmd       PROTO ((tCliHandle CliHandle, 
                                    UINT4 u4Command, ...));
UINT4 RmCliShowConf            PROTO ((tCliHandle CliHandle));
UINT4 RmCliShowStats            PROTO ((tCliHandle CliHandle));
UINT4 RmShowRunningConfig   PROTO ((tCliHandle CliHandle));
VOID  RmCliFormatNodeState     PROTO ((UINT1 *pu1Output, INT4 i4NodeState));
UINT4 RmCliSetHbInterval       PROTO ((tCliHandle CliHandle, 
                                    INT4 i4HbInterval));
UINT4 RmCliSetNodeStatus PROTO ((tCliHandle CliHandle, INT4 i4NodeState));

UINT4 RmCliSetPeerDeadInterval PROTO ((tCliHandle CliHandle,
                                    INT4 i4PeerDeadInt));

UINT4 RmCliSetStackingInterfaceParams PROTO ((tCliHandle CliHandle,
                                              INT4 *pi4IpAddress, INT4 *pi4Mask, INT4 *pi4IfIndex));
UINT4 RmCliSetPeerDeadIntMultiplier PROTO ((tCliHandle CliHandle,
                                    INT4 i4PeerDeadIntMultiplier));

UINT4 RmCliForceSwitchOver     PROTO ((tCliHandle CliHandle));
UINT4 RmCliSetDebugTrcLevel    PROTO ((tCliHandle CliHandle, UINT4 u4TrcLevel,
                                       UINT1 u1SetFlag));
UINT4 RmCliSetDebugModTrc      PROTO ((tCliHandle CliHandle, UINT4 u4ModTrc,
                                       UINT1 u1SetFlag));
INT1  RmCliGetConfigPrompt     PROTO ((INT1 *pi1ModeName, 
                                       INT1 *pi1DispStr));

#ifdef L2RED_TEST_WANTED
VOID RmTestCliSetProtoAck (INT4 i4SetVal);
VOID RmTestCliSetSeqMiss(INT4 i4SetVal);
VOID RmTestCliShowDebuggingInfo(tCliHandle CliHandle);
VOID RmTestCliSetProtoSyncBlock(UINT4 u4AppId, UINT4 u4BlockFlg);
#endif
UINT4 RmCliSetSwitchId        PROTO ((tCliHandle CliHandle, INT4));
UINT4 RmCliSetPriority        PROTO ((tCliHandle CliHandle, INT4));
UINT4 RmCliShowStackLink      PROTO ((tCliHandle CliHandle));
UINT4 RmCliShowStackDetails   PROTO ((tCliHandle CliHandle, INT4));
UINT4 RmCliShowSwitchOverTime PROTO ((tCliHandle CliHandle,INT4));
UINT4 RmCliSetVlanId          PROTO ((tCliHandle, INT4));
UINT4 RmCliSetStackIP         PROTO ((tCliHandle, UINT4));
UINT4 RmCliSetNumofStackPorts PROTO ((tCliHandle, UINT4));
UINT4 RmCliShowStackCounters  PROTO ((tCliHandle));
UINT4 RmCliShowStackPeer      PROTO ((tCliHandle,INT4));
UINT4 RmCliIsStackEnable      PROTO ((tCliHandle));
UINT4 RmCliSetColdStandby     PROTO ((tCliHandle, UINT4)); 
#ifdef L2RED_TEST_WANTED
UINT4 RmCliStartDynSyncAudit        PROTO ((tCliHandle, INT4));
UINT4 RmCliShowDynamicAuditStatus   PROTO ((tCliHandle, INT4));
#endif
UINT4 RmCliSetRestartFlag      PROTO ((tCliHandle, INT4));
UINT4 RmCliSetRestartCount     PROTO ((tCliHandle, UINT4));
/* HITLESS RESTART */
UINT4 RmCliSetHRStatus         PROTO ((tCliHandle, INT4));
UINT4 RmCliSetInitiateCopy    PROTO ((tCliHandle, INT4));
/* Show debugging */
VOID IssRmgrModuleShowDebugging PROTO((tCliHandle CliHandle));
VOID IssRmgrShowDebugging       PROTO((tCliHandle CliHandle));

#endif  /* _RMCLI_H */
