/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdldacli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 MplsLdpEntityAtmIfIndexOrZero[14];
extern UINT4 MplsLdpEntityAtmMergeCap[14];
extern UINT4 MplsLdpEntityAtmVcDirectionality[14];
extern UINT4 MplsLdpEntityAtmLsrConnectivity[14];
extern UINT4 MplsLdpEntityAtmDefaultControlVpi[14];
extern UINT4 MplsLdpEntityAtmDefaultControlVci[14];
extern UINT4 MplsLdpEntityAtmUnlabTrafVpi[14];
extern UINT4 MplsLdpEntityAtmUnlabTrafVci[14];
extern UINT4 MplsLdpEntityAtmStorageType[14];
extern UINT4 MplsLdpEntityAtmRowStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetMplsLdpEntityAtmIfIndexOrZero(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityAtmIfIndexOrZero)	\
	nmhSetCmn(MplsLdpEntityAtmIfIndexOrZero, 14, MplsLdpEntityAtmIfIndexOrZeroSet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %i", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityAtmIfIndexOrZero)
#define nmhSetMplsLdpEntityAtmMergeCap(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityAtmMergeCap)	\
	nmhSetCmn(MplsLdpEntityAtmMergeCap, 14, MplsLdpEntityAtmMergeCapSet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %i", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityAtmMergeCap)
#define nmhSetMplsLdpEntityAtmVcDirectionality(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityAtmVcDirectionality)	\
	nmhSetCmn(MplsLdpEntityAtmVcDirectionality, 14, MplsLdpEntityAtmVcDirectionalitySet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %i", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityAtmVcDirectionality)
#define nmhSetMplsLdpEntityAtmLsrConnectivity(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityAtmLsrConnectivity)	\
	nmhSetCmn(MplsLdpEntityAtmLsrConnectivity, 14, MplsLdpEntityAtmLsrConnectivitySet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %i", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityAtmLsrConnectivity)
#define nmhSetMplsLdpEntityAtmDefaultControlVpi(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityAtmDefaultControlVpi)	\
	nmhSetCmn(MplsLdpEntityAtmDefaultControlVpi, 14, MplsLdpEntityAtmDefaultControlVpiSet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %i", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityAtmDefaultControlVpi)
#define nmhSetMplsLdpEntityAtmDefaultControlVci(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityAtmDefaultControlVci)	\
	nmhSetCmn(MplsLdpEntityAtmDefaultControlVci, 14, MplsLdpEntityAtmDefaultControlVciSet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %i", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityAtmDefaultControlVci)
#define nmhSetMplsLdpEntityAtmUnlabTrafVpi(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityAtmUnlabTrafVpi)	\
	nmhSetCmn(MplsLdpEntityAtmUnlabTrafVpi, 14, MplsLdpEntityAtmUnlabTrafVpiSet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %i", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityAtmUnlabTrafVpi)
#define nmhSetMplsLdpEntityAtmUnlabTrafVci(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityAtmUnlabTrafVci)	\
	nmhSetCmn(MplsLdpEntityAtmUnlabTrafVci, 14, MplsLdpEntityAtmUnlabTrafVciSet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %i", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityAtmUnlabTrafVci)
#define nmhSetMplsLdpEntityAtmStorageType(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityAtmStorageType)	\
	nmhSetCmn(MplsLdpEntityAtmStorageType, 14, MplsLdpEntityAtmStorageTypeSet, LdpLock, LdpUnLock, 0, 0, 2, "%s %u %i", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityAtmStorageType)
#define nmhSetMplsLdpEntityAtmRowStatus(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityAtmRowStatus)	\
	nmhSetCmn(MplsLdpEntityAtmRowStatus, 14, MplsLdpEntityAtmRowStatusSet, LdpLock, LdpUnLock, 0, 1, 2, "%s %u %i", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex ,i4SetValMplsLdpEntityAtmRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 MplsLdpEntityAtmLRMinVpi[14];
extern UINT4 MplsLdpEntityAtmLRMinVci[14];
extern UINT4 MplsLdpEntityAtmLRMaxVpi[14];
extern UINT4 MplsLdpEntityAtmLRMaxVci[14];
extern UINT4 MplsLdpEntityAtmLRStorageType[14];
extern UINT4 MplsLdpEntityAtmLRRowStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetMplsLdpEntityAtmLRMaxVpi(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex , i4MplsLdpEntityAtmLRMinVpi , i4MplsLdpEntityAtmLRMinVci ,i4SetValMplsLdpEntityAtmLRMaxVpi)	\
	nmhSetCmn(MplsLdpEntityAtmLRMaxVpi, 14, MplsLdpEntityAtmLRMaxVpiSet, LdpLock, LdpUnLock, 0, 0, 4, "%s %u %i %i %i", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex , i4MplsLdpEntityAtmLRMinVpi , i4MplsLdpEntityAtmLRMinVci ,i4SetValMplsLdpEntityAtmLRMaxVpi)
#define nmhSetMplsLdpEntityAtmLRMaxVci(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex , i4MplsLdpEntityAtmLRMinVpi , i4MplsLdpEntityAtmLRMinVci ,i4SetValMplsLdpEntityAtmLRMaxVci)	\
	nmhSetCmn(MplsLdpEntityAtmLRMaxVci, 14, MplsLdpEntityAtmLRMaxVciSet, LdpLock, LdpUnLock, 0, 0, 4, "%s %u %i %i %i", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex , i4MplsLdpEntityAtmLRMinVpi , i4MplsLdpEntityAtmLRMinVci ,i4SetValMplsLdpEntityAtmLRMaxVci)
#define nmhSetMplsLdpEntityAtmLRStorageType(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex , i4MplsLdpEntityAtmLRMinVpi , i4MplsLdpEntityAtmLRMinVci ,i4SetValMplsLdpEntityAtmLRStorageType)	\
	nmhSetCmn(MplsLdpEntityAtmLRStorageType, 14, MplsLdpEntityAtmLRStorageTypeSet, LdpLock, LdpUnLock, 0, 0, 4, "%s %u %i %i %i", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex , i4MplsLdpEntityAtmLRMinVpi , i4MplsLdpEntityAtmLRMinVci ,i4SetValMplsLdpEntityAtmLRStorageType)
#define nmhSetMplsLdpEntityAtmLRRowStatus(pMplsLdpEntityLdpId , u4MplsLdpEntityIndex , i4MplsLdpEntityAtmLRMinVpi , i4MplsLdpEntityAtmLRMinVci ,i4SetValMplsLdpEntityAtmLRRowStatus)	\
	nmhSetCmn(MplsLdpEntityAtmLRRowStatus, 14, MplsLdpEntityAtmLRRowStatusSet, LdpLock, LdpUnLock, 0, 1, 4, "%s %u %i %i %i", pMplsLdpEntityLdpId , u4MplsLdpEntityIndex , i4MplsLdpEntityAtmLRMinVpi , i4MplsLdpEntityAtmLRMinVci ,i4SetValMplsLdpEntityAtmLRRowStatus)

#endif
