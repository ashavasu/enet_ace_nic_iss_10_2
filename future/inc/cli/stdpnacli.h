/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdpnacli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Dot1xPaeSystemAuthControl[9];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDot1xPaeSystemAuthControl(i4SetValDot1xPaeSystemAuthControl)	\
	nmhSetCmn(Dot1xPaeSystemAuthControl, 9, Dot1xPaeSystemAuthControlSet, PnacLock, PnacUnLock, 0, 0, 0, "%i", i4SetValDot1xPaeSystemAuthControl)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Dot1xPaePortNumber[11];
extern UINT4 Dot1xPaePortInitialize[11];
extern UINT4 Dot1xPaePortReauthenticate[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDot1xPaePortInitialize(i4Dot1xPaePortNumber ,i4SetValDot1xPaePortInitialize)	\
	nmhSetCmn(Dot1xPaePortInitialize, 11, Dot1xPaePortInitializeSet, PnacLock, PnacUnLock, 0, 0, 1, "%i %i", i4Dot1xPaePortNumber ,i4SetValDot1xPaePortInitialize)
#define nmhSetDot1xPaePortReauthenticate(i4Dot1xPaePortNumber ,i4SetValDot1xPaePortReauthenticate)	\
	nmhSetCmn(Dot1xPaePortReauthenticate, 11, Dot1xPaePortReauthenticateSet, PnacLock, PnacUnLock, 0, 0, 1, "%i %i", i4Dot1xPaePortNumber ,i4SetValDot1xPaePortReauthenticate)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Dot1xAuthAdminControlledDirections[11];
extern UINT4 Dot1xAuthAuthControlledPortControl[11];
extern UINT4 Dot1xAuthQuietPeriod[11];
extern UINT4 Dot1xAuthTxPeriod[11];
extern UINT4 Dot1xAuthSuppTimeout[11];
extern UINT4 Dot1xAuthServerTimeout[11];
extern UINT4 Dot1xAuthMaxReq[11];
extern UINT4 Dot1xAuthReAuthPeriod[11];
extern UINT4 Dot1xAuthReAuthEnabled[11];
extern UINT4 Dot1xAuthKeyTxEnabled[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDot1xAuthAdminControlledDirections(i4Dot1xPaePortNumber ,i4SetValDot1xAuthAdminControlledDirections)	\
	nmhSetCmn(Dot1xAuthAdminControlledDirections, 11, Dot1xAuthAdminControlledDirectionsSet, PnacLock, PnacUnLock, 0, 0, 1, "%i %i", i4Dot1xPaePortNumber ,i4SetValDot1xAuthAdminControlledDirections)
#define nmhSetDot1xAuthAuthControlledPortControl(i4Dot1xPaePortNumber ,i4SetValDot1xAuthAuthControlledPortControl)	\
	nmhSetCmn(Dot1xAuthAuthControlledPortControl, 11, Dot1xAuthAuthControlledPortControlSet, PnacLock, PnacUnLock, 0, 0, 1, "%i %i", i4Dot1xPaePortNumber ,i4SetValDot1xAuthAuthControlledPortControl)
#define nmhSetDot1xAuthQuietPeriod(i4Dot1xPaePortNumber ,u4SetValDot1xAuthQuietPeriod)	\
	nmhSetCmn(Dot1xAuthQuietPeriod, 11, Dot1xAuthQuietPeriodSet, PnacLock, PnacUnLock, 0, 0, 1, "%i %u", i4Dot1xPaePortNumber ,u4SetValDot1xAuthQuietPeriod)
#define nmhSetDot1xAuthTxPeriod(i4Dot1xPaePortNumber ,u4SetValDot1xAuthTxPeriod)	\
	nmhSetCmn(Dot1xAuthTxPeriod, 11, Dot1xAuthTxPeriodSet, PnacLock, PnacUnLock, 0, 0, 1, "%i %u", i4Dot1xPaePortNumber ,u4SetValDot1xAuthTxPeriod)
#define nmhSetDot1xAuthSuppTimeout(i4Dot1xPaePortNumber ,u4SetValDot1xAuthSuppTimeout)	\
	nmhSetCmn(Dot1xAuthSuppTimeout, 11, Dot1xAuthSuppTimeoutSet, PnacLock, PnacUnLock, 0, 0, 1, "%i %u", i4Dot1xPaePortNumber ,u4SetValDot1xAuthSuppTimeout)
#define nmhSetDot1xAuthServerTimeout(i4Dot1xPaePortNumber ,u4SetValDot1xAuthServerTimeout)	\
	nmhSetCmn(Dot1xAuthServerTimeout, 11, Dot1xAuthServerTimeoutSet, PnacLock, PnacUnLock, 0, 0, 1, "%i %u", i4Dot1xPaePortNumber ,u4SetValDot1xAuthServerTimeout)
#define nmhSetDot1xAuthMaxReq(i4Dot1xPaePortNumber ,u4SetValDot1xAuthMaxReq)	\
	nmhSetCmn(Dot1xAuthMaxReq, 11, Dot1xAuthMaxReqSet, PnacLock, PnacUnLock, 0, 0, 1, "%i %u", i4Dot1xPaePortNumber ,u4SetValDot1xAuthMaxReq)
#define nmhSetDot1xAuthReAuthPeriod(i4Dot1xPaePortNumber ,u4SetValDot1xAuthReAuthPeriod)	\
	nmhSetCmn(Dot1xAuthReAuthPeriod, 11, Dot1xAuthReAuthPeriodSet, PnacLock, PnacUnLock, 0, 0, 1, "%i %u", i4Dot1xPaePortNumber ,u4SetValDot1xAuthReAuthPeriod)
#define nmhSetDot1xAuthReAuthEnabled(i4Dot1xPaePortNumber ,i4SetValDot1xAuthReAuthEnabled)	\
	nmhSetCmn(Dot1xAuthReAuthEnabled, 11, Dot1xAuthReAuthEnabledSet, PnacLock, PnacUnLock, 0, 0, 1, "%i %i", i4Dot1xPaePortNumber ,i4SetValDot1xAuthReAuthEnabled)
#define nmhSetDot1xAuthKeyTxEnabled(i4Dot1xPaePortNumber ,i4SetValDot1xAuthKeyTxEnabled)	\
	nmhSetCmn(Dot1xAuthKeyTxEnabled, 11, Dot1xAuthKeyTxEnabledSet, PnacLock, PnacUnLock, 0, 0, 1, "%i %i", i4Dot1xPaePortNumber ,i4SetValDot1xAuthKeyTxEnabled)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Dot1xSuppHeldPeriod[11];
extern UINT4 Dot1xSuppAuthPeriod[11];
extern UINT4 Dot1xSuppStartPeriod[11];
extern UINT4 Dot1xSuppMaxStart[11];
extern UINT4 Dot1xSuppAccessCtrlWithAuth[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDot1xSuppHeldPeriod(i4Dot1xPaePortNumber ,u4SetValDot1xSuppHeldPeriod)	\
	nmhSetCmn(Dot1xSuppHeldPeriod, 11, Dot1xSuppHeldPeriodSet, PnacLock, PnacUnLock, 0, 0, 1, "%i %u", i4Dot1xPaePortNumber ,u4SetValDot1xSuppHeldPeriod)
#define nmhSetDot1xSuppAuthPeriod(i4Dot1xPaePortNumber ,u4SetValDot1xSuppAuthPeriod)	\
	nmhSetCmn(Dot1xSuppAuthPeriod, 11, Dot1xSuppAuthPeriodSet, PnacLock, PnacUnLock, 0, 0, 1, "%i %u", i4Dot1xPaePortNumber ,u4SetValDot1xSuppAuthPeriod)
#define nmhSetDot1xSuppStartPeriod(i4Dot1xPaePortNumber ,u4SetValDot1xSuppStartPeriod)	\
	nmhSetCmn(Dot1xSuppStartPeriod, 11, Dot1xSuppStartPeriodSet, PnacLock, PnacUnLock, 0, 0, 1, "%i %u", i4Dot1xPaePortNumber ,u4SetValDot1xSuppStartPeriod)
#define nmhSetDot1xSuppMaxStart(i4Dot1xPaePortNumber ,u4SetValDot1xSuppMaxStart)	\
	nmhSetCmn(Dot1xSuppMaxStart, 11, Dot1xSuppMaxStartSet, PnacLock, PnacUnLock, 0, 0, 1, "%i %u", i4Dot1xPaePortNumber ,u4SetValDot1xSuppMaxStart)
#define nmhSetDot1xSuppAccessCtrlWithAuth(i4Dot1xPaePortNumber ,i4SetValDot1xSuppAccessCtrlWithAuth)	\
	nmhSetCmn(Dot1xSuppAccessCtrlWithAuth, 11, Dot1xSuppAccessCtrlWithAuthSet, PnacLock, PnacUnLock, 0, 0, 1, "%i %i", i4Dot1xPaePortNumber ,i4SetValDot1xSuppAccessCtrlWithAuth)

#endif
