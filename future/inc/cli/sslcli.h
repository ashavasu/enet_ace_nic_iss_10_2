/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: sslcli.h,v 1.9 2015/01/02 12:06:12 siva Exp $
 *
 * Description: This has #defines for the SSL CLI submodule
 *
 ***********************************************************************/

#ifndef __SSLCLI_H__
#define __SSLCLI_H__

#include "cli.h"

/* Command Identifiers */
enum {
    CLI_SSL_GEN_CERT_REQ = 1,
    CLI_SSL_SHOW_SERVER_CERT,
    CLI_SSL_SERVER_CERT,
    CLI_SSL_SHOW_SERVER_STATUS,
    CLI_SSL_DEBUG,    
    CLI_SSL_NO_DEBUG,    
    CLI_SSL_HTTPS,    
    CLI_SSL_NO_HTTPS,    
    CLI_SSL_SERV_VERSION,
 CLI_SSL_KEY_GEN,
    CLI_SSL_MAX_COMMANDS
};


/* max no of variable inputs to cli parser */
#define SSL_CLI_MAX_ARGS             14

#define CLI_HTTPS_CONFIG           0x01
#define CLI_SSL_CIPHER_SUITES      0x02
#define CLI_SSL_CRYPTO_KEY         0x03

#define RSA_BITS512                512
#define RSA_BITS1024               1024

/* Error codes 
 * Error code values and 
 * Error strings are maintained inside the protocol module itself.
 */
enum {
    CLI_SSL_INVALID_TRC = 1,
    CLI_SSL_INV_HTTPS_STATUS,
    CLI_SSL_INV_CIPHER_LIST,
    CLI_SSL_SRV_CERT_NOT_CONFIG,
    CLI_SSL_INV_RSA,
    CLI_SSL_INV_VERSION,
    CLI_SSL_MAX_ERR 
};

/* The error strings should be places under the switch so as to avoid redifinition
 * This will be visible only in modulecli.c file
 */

#ifdef __SSLCLI_C__

CONST CHR1  *SslCliErrString [] = {
    NULL,
    "\r% Invalid Trace Level\r\n",
    "\r% Invalid Secure Http status\r\n",
    "\r% Invalid Cipher suite list\r\n",
    "\r% Server Certificate not configured\r\n",
    "\r% Invalid RSA key\r\n",
    "\r% Invalid SSL Version\r\n"
};

#endif

INT4 cli_process_ssl_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command, ...));

#endif /* __SSLCLI_H__ */
