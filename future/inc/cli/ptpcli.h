/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* $Id: ptpcli.h,v 1.5 2013/06/23 12:26:14 siva Exp $                        */
/* Licensee Aricent Inc., 2001-2002                                          */
/*****************************************************************************/
#ifndef __PTPCLI_H__
#define __PTPCLI_H__

#include "cli.h"
/* 
 * PTP CLI command constants. To add a new command ,a new definition needs to
 * be added to the list.
 */

/* CLI Constants */

#define CLI_PTP_ANNOUNCE_INTRVL_DFLT 1
#define CLI_PTP_ANNOUNCE_TIME_OUT_DFLT 3 
#define CLI_PTP_DELAY_REQ_INTRVL_DFLT 2
#define CLI_PTP_SYNC_INTRVL_DFLT 0
#define CLI_PTP_DELAY_TYPE_DEFLT 1
#define CLI_PTP_VERSION_DFLT 2
#define CLI_PTP_DOMAIN_DFLT 0
#define CLI_PTP_SYNC_LIMIT_DFLT 1000000000
#define CLI_PTP_ORDINARY_CLK 0
#define CLI_PTP_BOUNDARY_CLK 1
#define CLI_PTP_E2ETRANS_CLK 2
#define CLI_PTP_P2PTRANS_CLK 3 
#define CLI_PTP_FORWARD_ONLY_CLK 4
#define CLI_PTP_MANAGEMENT_NODE  5
#define CLI_PTP_DFLT_MODE (CLI_PTP_FORWARD_ONLY_CLK)
#define CLI_PTP_DFLT_PRI 128
#define CLI_PTP_DFLT_MAX_ACC_MASTER 20
#define CLI_MAX_ALT_MASTERS_DFLT 20
#define CLI_PTP_TRC_MAX_SIZE 200

#define CLI_PTP_IPV4 1
#define CLI_PTP_IPV6 2
#define CLI_PTP_ETH  3


#define PTP_CLI_MAX_ARGS       15
#define PTP_MAX_CLOCK_ID       30
#define PTP_MAX_ADDR_LEN       30

#define CLI_PTP_MODE  "(ptp-config)#"

#define CLI_PTP_TRAP_GLOB_ERR          0x00000001
#define CLI_PTP_TRAP_SYS_CTRL_CHNG     0x00000002
#define CLI_PTP_TRAP_SYS_ADMIN_CHNG    0x00000004
#define CLI_PTP_TRAP_PORT_STATE_CHNG   0x00000008
#define CLI_PTP_TRAP_PORT_ADMIN_CHNG   0x00000010
#define CLI_PTP_TRAP_SYNC_FAULT        0x00000020
#define CLI_PTP_TRAP_GM_FAULT          0x00000040
#define CLI_PTP_TRAP_ACC_MASTER_FAULT  0x00000080
#define CLI_PTP_TRAP_UCAST_ADMIN_CHNG  0x00000100

#define CLI_PTP_L2_CONTEXT 1
#define CLI_PTP_L3_CONTEXT 2

#define CLI_PTP_ALL_TRC (" init-shut mgmt datapath ctrl pkt-dump" \
                         " resource all-fail buffer critical")
/* Enumerations */

enum {
        CLI_PTP_SHUTDOWN = 1,
        CLI_PTP_START,
        CLI_PTP_ENABLE,
        CLI_PTP_DISABLE,
        CLI_PTP_VRF_START,
        CLI_PTP_VRF_SHUTDOWN,
        CLI_PTP_VRF_ENABLE,
        CLI_PTP_VRF_DISABLE,
        CLI_SET_DOMAIN_ID,
        CLI_PTP_TRAP_ENABLE,
        CLI_PTP_TRAP_DISABLE,
        CLI_PTP_TWO_STEP_CLK,
        CLI_PTP_ONE_STEP_CLK,
        CLI_PTP_MAX_DOMAINS,
        CLI_PTP_MAX_CONTEXTS,
        CLI_PTP_MAX_PORTS,
        CLI_PTP_NUM_PORTS,
        CLI_PTP_PORT_ENABLE,
        CLI_PTP_PORT_DISABLE,
        CLI_PTP_CLK_MODE_PRIORITY,
        CLI_PTP_CLK_PRI_ONE_RESET,
        CLI_PTP_CLK_PRI_TWO_RESET,
        CLI_PTP_CLK_SLAVE_ONLY,
        CLI_PTP_CLK_DFLT_MODE,
        CLI_PTP_ANNOUNCE_INTRVL,
        CLI_PTP_ANNOUNCE_TIME_OUT,
        CLI_PTP_DELAY_REQ_INTRVL,
        CLI_PTP_DELAY_REQ_ENABLE,
        CLI_PTP_SYNC_INTRVL,
        CLI_PTP_DELAY_TYPE,
        CLI_PTP_VERSION,
        CLI_PTP_DOMAIN,
        CLI_PTP_SYNC_LIMIT,
        CLI_MAX_ALT_MASTERS,
        CLI_PTP_PATH_TRC_ENABLE,
        CLI_PTP_PATH_TRC_DISABLE,
        CLI_PTP_MAX_ACCEPT_MASTER,
        CLI_PTP_CLK_PRI_ONE,
        CLI_PTP_CLK_PRI_TWO,
        CLI_PTP_CLK_MODE,
        CLI_PTP_CLK_NO_SLAVE,
        CLI_PTP_PORT_DELETE,
        CLI_PTP_ALT_TIME_SCALE_ENABLE,
        CLI_PTP_ALT_TIME_SCALE_DISABLE,
        CLI_PTP_ALT_TIME_SCALE,
        CLI_PTP_NO_ALT_TIME_MAX,
        CLI_PTP_ACC_MASTER,
        CLI_PTP_ACC_MASTER_PROTO,
        CLI_PTP_ALT_MASTER_ENABLE,
        CLI_PTP_ALT_MASTER_DISABLE,
        CLI_PTP_DEL_ACC_MASTER,
        CLI_PTP_PORT_CREATE,
        CLI_PTP_UNICAST_MASTERS,
        CLI_PTP_ALT_MASTER_SYNC_INTVL,
        CLI_PTP_ALT_TIME_SET_NAME,
        CLI_PTP_ALT_TIME_NO_SET_NAME,
        CLI_PTP_UNICAST_ENABLE,
        CLI_PTP_UNICAST_DISABLE,
        CLI_PTP_SHOW_LIST_PORT,
        CLI_PTP_CLEAR_STATS_ALL,
        CLI_PTP_ACCEPT_MASTER_ENABLE,
        CLI_PTP_ACCEPT_MASTER_DISABLE,
        CLI_PTP_UCAST_MAX_MASTERS,
        CLI_PTP_SECURITY_ENABLE,
        CLI_PTP_SECURITY_DISABLE,
        CLI_PTP_SECURITY_MAX_SA,
        CLI_PTP_SHOW_CLOCK,
        CLI_PTP_NO_ALT_TIME_SCALE,
        CLI_PTP_SHOW_GLOBAL,
        CLI_PTP_PRIMARY_CXT,
        CLI_PTP_PRIMARY_DOMAIN,
        CLI_PTP_CLK_PORTS,
        CLI_PTP_SHOW_VRF,
        CLI_PTP_SHOW_FM_CLK,
        CLI_PTP_SHOW_PORT_COUNTERS,
        CLI_PTP_SHOW_PARENT_STATS,
        CLI_PTP_SHOW_PORT,
        CLI_PTP_SHOW_TIME_PROPERTY,
        CLI_PTP_SHOW_CURRENT_DS,
        CLI_PTP_SHOW_SA,
        CLI_PTP_SHOW_ACC_MASTER,
        CLI_PTP_SHOW_ACC_MASTERS,
        CLI_PTP_SHOW,
        CLI_PTP_SHOW_UNICAST_MASTERS,
        CLI_PTP_SHOW_SEC_KEYLIST,
        CLI_PTP_GBL_DEBUG,
        CLI_PTP_CXT_DEBUG,
        CLI_PTP_SHOW_ALT_TIME_SCALE,
        CLI_PTP_NO_GBL_DEBUG,
        CLI_PTP_DEBUG,
        CLI_PTP_NO_DEBUG,
        CLI_PTP_SHOW_DEBUG,
        CLI_PTP_SET_G8261_SWITCHCOUNT,
        CLI_PTP_SET_G8261_PACKETSIZE,
        CLI_PTP_SET_G8261_LOAD,
        CLI_PTP_SET_G8261_NWDIST,
        CLI_PTP_SET_G8261_DELAY_DURATION,
        CLI_PTP_SET_G8261_NWDIR,
        CLI_PTP_SET_G8261_FLOWVAR,
        CLI_PTP_SET_G8261_FLOW_INTERVAL,
        CLI_PTP_SET_G8261_FREQUENCY,
        CLI_PTP_SHOW_G8261_DELAY_OUTPUT,
        CLI_PTP_SET_G8261_ENABLE,
        CLI_PTP_SET_G8261_DISABLE
};

enum {
        CLI_PTP_UNKNOWN_ERR = 1,
        CLI_PTP_ERR_MODULE_SHUTDOWN,
        CLI_PTP_ERR_CONTEXT_NOT_PRESENT,
        CLI_PTP_ERR_DOMAIN_NOT_PRESENT,
        CLI_PTP_ERR_PORT_NOT_PRESENT,
        CLI_PTP_ERR_GRAND_MASTER_NOT_PRESENT,
        CLI_PTP_ERR_UNICAST_MASTER_NOT_PRESENT,
        CLI_PTP_ERR_ACCEPT_MASTER_NOT_PRESENT,
        CLI_PTP_ERR_SEC_KEY_NOT_PRESENT,
        CLI_PTP_ERR_SA_NOT_PRESENT,
        CLI_PTP_ERR_ATS_NOT_PRESENT,
        CLI_PTP_ERR_DOMAIN_PRESENT,
        CLI_PTP_ERR_PORT_PRESENT,
        CLI_PTP_ERR_GRAND_MASTER_PRESENT,
        CLI_PTP_ERR_UNICAST_MASTER_PRESENT,
        CLI_PTP_ERR_ACCEPT_MASTER_PRESENT,
        CLI_PTP_ERR_SEC_KEY_PRESENT,
        CLI_PTP_ERR_SA_PRESENT,
        CLI_PTP_ERR_ATS_PRESENT,
        CLI_PTP_ERR_NOT_ORDINARY_BOUNDARY_CLK,
        CLI_PTP_ERR_NOT_TRANSPARENT_CLK,
        CLI_PTP_ERR_ORDINARY_CLK,
        CLI_PTP_ERR_NOT_ORDINARY_CLK,
        CLI_PTP_ERR_PORT_VERSION_ONE,
        CLI_PTP_ERR_PORT_INVALID,
        CLI_PTP_ERR_MAX_CONTEXT,
        CLI_PTP_ERR_MAX_DMN,
        CLI_PTP_ERR_MAX_PORTS,
        CLI_PTP_ERR_MAX_MASTER,
        CLI_PTP_ERR_CLK_ID_MISMATCH,
        CLI_PTP_ERR_MAX_CLK_PORTS,
        CLI_PTP_ERR_IF_CXT_DIFFERENT,
        CLI_PTP_ERR_ACCEPT_MAST_CLK_MODE,
        CLI_PTP_ERR_FORWARD_CLK,
        CLI_PTP_MAX_ERR
};

#ifdef _PTPCLI_C_
CONST CHR1  *gapc1PtpCliErrString [] = {
        NULL,
        "Unknown Error\r\n",
        "PTP is shutdown, please start PTP\r\n",
        "Context does not exists\r\n",
        "Domain data set does not exists\r\n",
        "Port config data set does not exists\r\n",
        "Grand master data set does not exists\r\n",
        "Unicast master data set does not exists\r\n",
        "Acceptable master data set does not exists\r\n",
        "Security key data set does not exists\r\n",
        "Security association data set does not exists\r\n",
        "Alternate time scale data set does not exists\r\n",
        "Domain data set already exists\r\n",
        "Port config data set already exists\r\n",
        "Grand master data set already exists\r\n",
        "Unicast master data set already exists\r\n",
        "Acceptable master data set already exists\r\n",
        "Security key data set already exists\r\n",
        "Security association data set already exists\r\n",
        "Alternate time scale data set already exists\r\n",
        "Clock is neither ordinary nor boundary\r\n",
        "Clock is not in transparent mode\r\n",
        "Clock is ordinary clock, cannot enable PTP in more than "
         "one port\r\n",
        "Clock is not ordinary clock, cannot configure slave only "
         "option\r\n",
        "Version one can be configured only in Boundary clock mode\r\n",
        "Invalid Port type or port number\r\n",
        "Maximum contexts reached, no more contexts allowed\r\n",
        "Maximum domains reached, no more domains allowed\r\n",
        "Maximum PTP ports reached, no more ports allowed in system\r\n",
        "Maximum masters reached\r\n",
        "The clock id did not match the clock id present in "
         "domain entry\r\n",
        "Maximum PTP ports configured in clock is reached, "
         "no more ports allowed\r\n",
        "Port is not mapped to the given context\r\n",
        "Acceptable master configuration is not allowed on transparent "
        "clock and forward mode\r\n",
        "Clock is in forward mode.This opertion is not permitted\r\n",
        "\r\n"
};

#endif

enum
{
        CLI_G8261TS_INVALID_SWITCH_COUNT = 1,
        CLI_G8261TS_INVALID_PKTSIZE,
        CLI_G8261TS_INVALID_LOAD,
        CLI_G8261TS_INVALID_NWDISTDIRECTION,
        CLI_G8261TS_INVALID_FLOW_VARIATION,
        CLI_G8261TS_INVALID_DURATION,
        CLI_G8261TS_INVALID_NWDIST,
        CLI_G8261TS_INVALID_FLOWINTERVAL,
        CLI_G8261TS_INVALID_FREQUENCY,
        CLI_G8261TS_INVALID_DIRECTION,
        CLI_G8261_MAX_ERR
};
#ifdef _G8261CLI_C_
CONST CHR1  *gapc1G8261CliErrString [] = {
        NULL,
        "Invalid input given for switch count\r\n",
        "Invalid packet size\r\n",
        "Invalid load given as input\r\n",
        "Invalid direction for network disturbance given as input\r\n",
        "Invalid input given for flow variation factor \r\n",
        "Invalid input given for duration of network disturbance or load\r\n",
        "Invalid input given for network disturbance\r\n",
        "Invalid input given for flow interval\r\n",
        "Invalid input given for frequency\r\n",
        "Invalid direction given as input\r\n",
        "\r\n"
};
#endif
enum {
        PTP_CLI_SHOW_ALL    = 0,
        PTP_CLI_SHOWCONFIG  = 1,
        PTP_CLI_SHOW_STATUS = 2,
        PTP_CLI_SHOW_STATS  = 3,
        PTP_CLI_SHOW_TIMER  = 4,
        PTP_CLI_SHOW_SEC    = 5,
        PTP_CLI_SHOW_OPT    = 6,
        PTP_CLI_MAX_SHOW_CMDS  = 7
};

INT1
PtpGetPtpCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

INT1
PtpGetVcmPtpCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

INT4
PTPCliSetSystemControl (tCliHandle CliHandle, UINT4 u4ContextId,
                INT4 i4Status);

INT4
PTPCliSetModuleStatus (tCliHandle CliHandle, UINT4 u4ContextId,
                       INT4 i4Status);

INT4
PtpShowRunningConfig PROTO ((tCliHandle CliHandle, UINT4 u4Module));

PUBLIC INT4
cli_process_g8261_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command, ...));
/******************************************************************************
 *                             ptpcli.c                                       *
 ******************************************************************************/
PUBLIC INT4
cli_process_ptp_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command, ...));

PUBLIC INT4
cli_process_ptp_show_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command, ...));

PUBLIC INT4
cli_process_ptp_if_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command, ...));
#ifdef PTP_TEST_WANTED
enum
{
        PTP_UT_TEST = 1
};

PUBLIC INT4
cli_process_ptp_test_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);
#endif

#endif /* __PTPCLI_H__ */
