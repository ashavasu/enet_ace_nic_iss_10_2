/******************************************************************** 
* Copyright (C) Future Software,2002 
* 
* $Id: seccli.h,v 1.3 2011/07/15 10:53:38 siva Exp $ 
* 
* Description: This file contains the CLI MACRO definitions required 
*              common to ipsecv4 and ipsecv6. 
* 
*******************************************************************/ 
#ifndef __SECCLI_H__
#define __SECCLI_H__

#define CLI_APPLY                "3"
#define CLI_MANUAL               "1"
#define CLI_AUTOMATIC            "2"
#define CLI_BYPASS               "4"

/* Changed the Values to suit the IPSEC DOI for Ike */
#define CLI_TRANSPORT            "2"
#define CLI_TUNNEL               "1"
#define CLI_AH                   "51"
#define CLI_ESP                  "50"
#define CLI_NULL                  "0"
#define CLI_MD5                   "4" 
#define CLI_HMACMD5               "1"
#define CLI_HMACSHA               "2"
#define CLI_XCBCMAC               "5"
#define CLI_HMACSHA256            "12"
#define CLI_HMACSHA384            "13"
#define CLI_HMACSHA512            "14"
#define CLI_KEYEDMD5              "3"  
#define CLI_DESCBC                "4" 
#define CLI_3DESCBC               "5" 
#define CLI_AES                   "12" 
#define CLI_ESP_NULL              "11" 

#define CLI_TCP                   "6"
#define CLI_UDP                   "17"
#define CLI_ICMPV4                "1"
#define CLI_ICMPV6                "58"
#define CLI_ANY_PROTOCOL          "9000" 
#define CLI_ANY_PORT              "9000" 
#define CLI_INBOUND               "1"
#define CLI_OUTBOUND              "2"
#define CLI_ANY_DIRECTION         "3"
#define CLI_ALLOW                 "2"
#define CLI_FILTER                "1" 
#define CLI_ACCESS_ANY            "9000"    
#define CLI_ANTI_REPLAY_ENABLE    "1"
#define CLI_ANTI_REPLAY_DISABLE   "2"
#define CLI_SET                   "3"
#define CLI_COPY                  "1"
#define CLI_CLEAR                 "2"  
#define ZERO_INST                 "0"  

#define CLI_IPSEC_MANUAL          "1"

#endif /* __SECCLI_H__ */
