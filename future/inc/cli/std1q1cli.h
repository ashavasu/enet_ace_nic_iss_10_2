/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: std1q1cli.h,v 1.2 2009/09/23 10:42:03 prabuc Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ieee8021QBridgeComponentId[13];
extern UINT4 Ieee8021QBridgeMvrpEnabledStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIeee8021QBridgeMvrpEnabledStatus(u4Ieee8021QBridgeComponentId ,i4SetValIeee8021QBridgeMvrpEnabledStatus)	\
	nmhSetCmn(Ieee8021QBridgeMvrpEnabledStatus, 13, Ieee8021QBridgeMvrpEnabledStatusSet, MrpLock, MrpUnLock, 0, 0, 1, "%u %i", u4Ieee8021QBridgeComponentId ,i4SetValIeee8021QBridgeMvrpEnabledStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ieee8021QBridgeCVlanPortComponentId[13];
extern UINT4 Ieee8021QBridgeCVlanPortNumber[13];
extern UINT4 Ieee8021QBridgeCVlanPortRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIeee8021QBridgeCVlanPortRowStatus(u4Ieee8021QBridgeCVlanPortComponentId , u4Ieee8021QBridgeCVlanPortNumber ,i4SetValIeee8021QBridgeCVlanPortRowStatus)	\
	nmhSetCmn(Ieee8021QBridgeCVlanPortRowStatus, 13, Ieee8021QBridgeCVlanPortRowStatusSet, NULL, NULL, 0, 1, 2, "%u %u %i", u4Ieee8021QBridgeCVlanPortComponentId , u4Ieee8021QBridgeCVlanPortNumber ,i4SetValIeee8021QBridgeCVlanPortRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ieee8021QBridgeFdbComponentId[13];
extern UINT4 Ieee8021QBridgeFdbId[13];
extern UINT4 Ieee8021QBridgeFdbAgingTime[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIeee8021QBridgeFdbAgingTime(u4Ieee8021QBridgeFdbComponentId , u4Ieee8021QBridgeFdbId ,i4SetValIeee8021QBridgeFdbAgingTime)	\
	nmhSetCmn(Ieee8021QBridgeFdbAgingTime, 13, Ieee8021QBridgeFdbAgingTimeSet, NULL, NULL, 0, 0, 2, "%u %u %i", u4Ieee8021QBridgeFdbComponentId , u4Ieee8021QBridgeFdbId ,i4SetValIeee8021QBridgeFdbAgingTime)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ieee8021QBridgeForwardAllVlanIndex[13];
extern UINT4 Ieee8021QBridgeForwardAllStaticPorts[13];
extern UINT4 Ieee8021QBridgeForwardAllForbiddenPorts[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIeee8021QBridgeForwardAllStaticPorts(u4Ieee8021QBridgeVlanCurrentComponentId , u4Ieee8021QBridgeForwardAllVlanIndex ,pSetValIeee8021QBridgeForwardAllStaticPorts)	\
	nmhSetCmn(Ieee8021QBridgeForwardAllStaticPorts, 13, Ieee8021QBridgeForwardAllStaticPortsSet, NULL, NULL, 0, 0, 2, "%u %u %s", u4Ieee8021QBridgeVlanCurrentComponentId , u4Ieee8021QBridgeForwardAllVlanIndex ,pSetValIeee8021QBridgeForwardAllStaticPorts)
#define nmhSetIeee8021QBridgeForwardAllForbiddenPorts(u4Ieee8021QBridgeVlanCurrentComponentId , u4Ieee8021QBridgeForwardAllVlanIndex ,pSetValIeee8021QBridgeForwardAllForbiddenPorts)	\
	nmhSetCmn(Ieee8021QBridgeForwardAllForbiddenPorts, 13, Ieee8021QBridgeForwardAllForbiddenPortsSet, NULL, NULL, 0, 0, 2, "%u %u %s", u4Ieee8021QBridgeVlanCurrentComponentId , u4Ieee8021QBridgeForwardAllVlanIndex ,pSetValIeee8021QBridgeForwardAllForbiddenPorts)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ieee8021QBridgeForwardUnregisteredVlanIndex[13];
extern UINT4 Ieee8021QBridgeForwardUnregisteredStaticPorts[13];
extern UINT4 Ieee8021QBridgeForwardUnregisteredForbiddenPorts[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIeee8021QBridgeForwardUnregisteredStaticPorts(u4Ieee8021QBridgeVlanCurrentComponentId , u4Ieee8021QBridgeForwardUnregisteredVlanIndex ,pSetValIeee8021QBridgeForwardUnregisteredStaticPorts)	\
	nmhSetCmn(Ieee8021QBridgeForwardUnregisteredStaticPorts, 13, Ieee8021QBridgeForwardUnregisteredStaticPortsSet, NULL, NULL, 0, 0, 2, "%u %u %s", u4Ieee8021QBridgeVlanCurrentComponentId , u4Ieee8021QBridgeForwardUnregisteredVlanIndex ,pSetValIeee8021QBridgeForwardUnregisteredStaticPorts)
#define nmhSetIeee8021QBridgeForwardUnregisteredForbiddenPorts(u4Ieee8021QBridgeVlanCurrentComponentId , u4Ieee8021QBridgeForwardUnregisteredVlanIndex ,pSetValIeee8021QBridgeForwardUnregisteredForbiddenPorts)	\
	nmhSetCmn(Ieee8021QBridgeForwardUnregisteredForbiddenPorts, 13, Ieee8021QBridgeForwardUnregisteredForbiddenPortsSet, NULL, NULL, 0, 0, 2, "%u %u %s", u4Ieee8021QBridgeVlanCurrentComponentId , u4Ieee8021QBridgeForwardUnregisteredVlanIndex ,pSetValIeee8021QBridgeForwardUnregisteredForbiddenPorts)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ieee8021QBridgeStaticUnicastComponentId[13];
extern UINT4 Ieee8021QBridgeStaticUnicastVlanIndex[13];
extern UINT4 Ieee8021QBridgeStaticUnicastAddress[13];
extern UINT4 Ieee8021QBridgeStaticUnicastReceivePort[13];
extern UINT4 Ieee8021QBridgeStaticUnicastStaticEgressPorts[13];
extern UINT4 Ieee8021QBridgeStaticUnicastForbiddenEgressPorts[13];
extern UINT4 Ieee8021QBridgeStaticUnicastStorageType[13];
extern UINT4 Ieee8021QBridgeStaticUnicastRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIeee8021QBridgeStaticUnicastStaticEgressPorts(u4Ieee8021QBridgeStaticUnicastComponentId , u4Ieee8021QBridgeStaticUnicastVlanIndex , tIeee8021QBridgeStaticUnicastAddress , u4Ieee8021QBridgeStaticUnicastReceivePort ,pSetValIeee8021QBridgeStaticUnicastStaticEgressPorts)	\
	nmhSetCmn(Ieee8021QBridgeStaticUnicastStaticEgressPorts, 13, Ieee8021QBridgeStaticUnicastStaticEgressPortsSet, NULL, NULL, 0, 0, 4, "%u %u %m %u %s", u4Ieee8021QBridgeStaticUnicastComponentId , u4Ieee8021QBridgeStaticUnicastVlanIndex , tIeee8021QBridgeStaticUnicastAddress , u4Ieee8021QBridgeStaticUnicastReceivePort ,pSetValIeee8021QBridgeStaticUnicastStaticEgressPorts)
#define nmhSetIeee8021QBridgeStaticUnicastForbiddenEgressPorts(u4Ieee8021QBridgeStaticUnicastComponentId , u4Ieee8021QBridgeStaticUnicastVlanIndex , tIeee8021QBridgeStaticUnicastAddress , u4Ieee8021QBridgeStaticUnicastReceivePort ,pSetValIeee8021QBridgeStaticUnicastForbiddenEgressPorts)	\
	nmhSetCmn(Ieee8021QBridgeStaticUnicastForbiddenEgressPorts, 13, Ieee8021QBridgeStaticUnicastForbiddenEgressPortsSet, NULL, NULL, 0, 0, 4, "%u %u %m %u %s", u4Ieee8021QBridgeStaticUnicastComponentId , u4Ieee8021QBridgeStaticUnicastVlanIndex , tIeee8021QBridgeStaticUnicastAddress , u4Ieee8021QBridgeStaticUnicastReceivePort ,pSetValIeee8021QBridgeStaticUnicastForbiddenEgressPorts)
#define nmhSetIeee8021QBridgeStaticUnicastStorageType(u4Ieee8021QBridgeStaticUnicastComponentId , u4Ieee8021QBridgeStaticUnicastVlanIndex , tIeee8021QBridgeStaticUnicastAddress , u4Ieee8021QBridgeStaticUnicastReceivePort ,i4SetValIeee8021QBridgeStaticUnicastStorageType)	\
	nmhSetCmn(Ieee8021QBridgeStaticUnicastStorageType, 13, Ieee8021QBridgeStaticUnicastStorageTypeSet, NULL, NULL, 0, 0, 4, "%u %u %m %u %i", u4Ieee8021QBridgeStaticUnicastComponentId , u4Ieee8021QBridgeStaticUnicastVlanIndex , tIeee8021QBridgeStaticUnicastAddress , u4Ieee8021QBridgeStaticUnicastReceivePort ,i4SetValIeee8021QBridgeStaticUnicastStorageType)
#define nmhSetIeee8021QBridgeStaticUnicastRowStatus(u4Ieee8021QBridgeStaticUnicastComponentId , u4Ieee8021QBridgeStaticUnicastVlanIndex , tIeee8021QBridgeStaticUnicastAddress , u4Ieee8021QBridgeStaticUnicastReceivePort ,i4SetValIeee8021QBridgeStaticUnicastRowStatus)	\
	nmhSetCmn(Ieee8021QBridgeStaticUnicastRowStatus, 13, Ieee8021QBridgeStaticUnicastRowStatusSet, NULL, NULL, 0, 1, 4, "%u %u %m %u %i", u4Ieee8021QBridgeStaticUnicastComponentId , u4Ieee8021QBridgeStaticUnicastVlanIndex , tIeee8021QBridgeStaticUnicastAddress , u4Ieee8021QBridgeStaticUnicastReceivePort ,i4SetValIeee8021QBridgeStaticUnicastRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ieee8021QBridgeStaticMulticastAddress[13];
extern UINT4 Ieee8021QBridgeStaticMulticastReceivePort[13];
extern UINT4 Ieee8021QBridgeStaticMulticastStaticEgressPorts[13];
extern UINT4 Ieee8021QBridgeStaticMulticastForbiddenEgressPorts[13];
extern UINT4 Ieee8021QBridgeStaticMulticastStorageType[13];
extern UINT4 Ieee8021QBridgeStaticMulticastRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIeee8021QBridgeStaticMulticastStaticEgressPorts(u4Ieee8021QBridgeVlanCurrentComponentId , u4Ieee8021QBridgeVlanIndex , tIeee8021QBridgeStaticMulticastAddress , u4Ieee8021QBridgeStaticMulticastReceivePort ,pSetValIeee8021QBridgeStaticMulticastStaticEgressPorts)	\
	nmhSetCmn(Ieee8021QBridgeStaticMulticastStaticEgressPorts, 13, Ieee8021QBridgeStaticMulticastStaticEgressPortsSet, NULL, NULL, 0, 0, 4, "%u %u %m %u %s", u4Ieee8021QBridgeVlanCurrentComponentId , u4Ieee8021QBridgeVlanIndex , tIeee8021QBridgeStaticMulticastAddress , u4Ieee8021QBridgeStaticMulticastReceivePort ,pSetValIeee8021QBridgeStaticMulticastStaticEgressPorts)
#define nmhSetIeee8021QBridgeStaticMulticastForbiddenEgressPorts(u4Ieee8021QBridgeVlanCurrentComponentId , u4Ieee8021QBridgeVlanIndex , tIeee8021QBridgeStaticMulticastAddress , u4Ieee8021QBridgeStaticMulticastReceivePort ,pSetValIeee8021QBridgeStaticMulticastForbiddenEgressPorts)	\
	nmhSetCmn(Ieee8021QBridgeStaticMulticastForbiddenEgressPorts, 13, Ieee8021QBridgeStaticMulticastForbiddenEgressPortsSet, NULL, NULL, 0, 0, 4, "%u %u %m %u %s", u4Ieee8021QBridgeVlanCurrentComponentId , u4Ieee8021QBridgeVlanIndex , tIeee8021QBridgeStaticMulticastAddress , u4Ieee8021QBridgeStaticMulticastReceivePort ,pSetValIeee8021QBridgeStaticMulticastForbiddenEgressPorts)
#define nmhSetIeee8021QBridgeStaticMulticastStorageType(u4Ieee8021QBridgeVlanCurrentComponentId , u4Ieee8021QBridgeVlanIndex , tIeee8021QBridgeStaticMulticastAddress , u4Ieee8021QBridgeStaticMulticastReceivePort ,i4SetValIeee8021QBridgeStaticMulticastStorageType)	\
	nmhSetCmn(Ieee8021QBridgeStaticMulticastStorageType, 13, Ieee8021QBridgeStaticMulticastStorageTypeSet, NULL, NULL, 0, 0, 4, "%u %u %m %u %i", u4Ieee8021QBridgeVlanCurrentComponentId , u4Ieee8021QBridgeVlanIndex , tIeee8021QBridgeStaticMulticastAddress , u4Ieee8021QBridgeStaticMulticastReceivePort ,i4SetValIeee8021QBridgeStaticMulticastStorageType)
#define nmhSetIeee8021QBridgeStaticMulticastRowStatus(u4Ieee8021QBridgeVlanCurrentComponentId , u4Ieee8021QBridgeVlanIndex , tIeee8021QBridgeStaticMulticastAddress , u4Ieee8021QBridgeStaticMulticastReceivePort ,i4SetValIeee8021QBridgeStaticMulticastRowStatus)	\
	nmhSetCmn(Ieee8021QBridgeStaticMulticastRowStatus, 13, Ieee8021QBridgeStaticMulticastRowStatusSet, NULL, NULL, 0, 1, 4, "%u %u %m %u %i", u4Ieee8021QBridgeVlanCurrentComponentId , u4Ieee8021QBridgeVlanIndex , tIeee8021QBridgeStaticMulticastAddress , u4Ieee8021QBridgeStaticMulticastReceivePort ,i4SetValIeee8021QBridgeStaticMulticastRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ieee8021QBridgeVlanStaticComponentId[13];
extern UINT4 Ieee8021QBridgeVlanStaticVlanIndex[13];
extern UINT4 Ieee8021QBridgeVlanStaticName[13];
extern UINT4 Ieee8021QBridgeVlanStaticEgressPorts[13];
extern UINT4 Ieee8021QBridgeVlanForbiddenEgressPorts[13];
extern UINT4 Ieee8021QBridgeVlanStaticUntaggedPorts[13];
extern UINT4 Ieee8021QBridgeVlanStaticRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIeee8021QBridgeVlanStaticName(u4Ieee8021QBridgeVlanStaticComponentId , u4Ieee8021QBridgeVlanStaticVlanIndex ,pSetValIeee8021QBridgeVlanStaticName)	\
	nmhSetCmn(Ieee8021QBridgeVlanStaticName, 13, Ieee8021QBridgeVlanStaticNameSet, NULL, NULL, 0, 0, 2, "%u %u %s", u4Ieee8021QBridgeVlanStaticComponentId , u4Ieee8021QBridgeVlanStaticVlanIndex ,pSetValIeee8021QBridgeVlanStaticName)
#define nmhSetIeee8021QBridgeVlanStaticEgressPorts(u4Ieee8021QBridgeVlanStaticComponentId , u4Ieee8021QBridgeVlanStaticVlanIndex ,pSetValIeee8021QBridgeVlanStaticEgressPorts)	\
	nmhSetCmn(Ieee8021QBridgeVlanStaticEgressPorts, 13, Ieee8021QBridgeVlanStaticEgressPortsSet, NULL, NULL, 0, 0, 2, "%u %u %s", u4Ieee8021QBridgeVlanStaticComponentId , u4Ieee8021QBridgeVlanStaticVlanIndex ,pSetValIeee8021QBridgeVlanStaticEgressPorts)
#define nmhSetIeee8021QBridgeVlanForbiddenEgressPorts(u4Ieee8021QBridgeVlanStaticComponentId , u4Ieee8021QBridgeVlanStaticVlanIndex ,pSetValIeee8021QBridgeVlanForbiddenEgressPorts)	\
	nmhSetCmn(Ieee8021QBridgeVlanForbiddenEgressPorts, 13, Ieee8021QBridgeVlanForbiddenEgressPortsSet, NULL, NULL, 0, 0, 2, "%u %u %s", u4Ieee8021QBridgeVlanStaticComponentId , u4Ieee8021QBridgeVlanStaticVlanIndex ,pSetValIeee8021QBridgeVlanForbiddenEgressPorts)
#define nmhSetIeee8021QBridgeVlanStaticUntaggedPorts(u4Ieee8021QBridgeVlanStaticComponentId , u4Ieee8021QBridgeVlanStaticVlanIndex ,pSetValIeee8021QBridgeVlanStaticUntaggedPorts)	\
	nmhSetCmn(Ieee8021QBridgeVlanStaticUntaggedPorts, 13, Ieee8021QBridgeVlanStaticUntaggedPortsSet, NULL, NULL, 0, 0, 2, "%u %u %s", u4Ieee8021QBridgeVlanStaticComponentId , u4Ieee8021QBridgeVlanStaticVlanIndex ,pSetValIeee8021QBridgeVlanStaticUntaggedPorts)
#define nmhSetIeee8021QBridgeVlanStaticRowStatus(u4Ieee8021QBridgeVlanStaticComponentId , u4Ieee8021QBridgeVlanStaticVlanIndex ,i4SetValIeee8021QBridgeVlanStaticRowStatus)	\
	nmhSetCmn(Ieee8021QBridgeVlanStaticRowStatus, 13, Ieee8021QBridgeVlanStaticRowStatusSet, NULL, NULL, 0, 1, 2, "%u %u %i", u4Ieee8021QBridgeVlanStaticComponentId , u4Ieee8021QBridgeVlanStaticVlanIndex ,i4SetValIeee8021QBridgeVlanStaticRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ieee8021QBridgePvid[13];
extern UINT4 Ieee8021QBridgePortAcceptableFrameTypes[13];
extern UINT4 Ieee8021QBridgePortIngressFiltering[13];
extern UINT4 Ieee8021QBridgePortMvrpEnabledStatus[13];
extern UINT4 Ieee8021QBridgePortRestrictedVlanRegistration[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIeee8021QBridgePvid(u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,u4SetValIeee8021QBridgePvid)	\
	nmhSetCmn(Ieee8021QBridgePvid, 13, Ieee8021QBridgePvidSet, NULL, NULL, 0, 0, 2, "%u %u %u", u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,u4SetValIeee8021QBridgePvid)
#define nmhSetIeee8021QBridgePortAcceptableFrameTypes(u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValIeee8021QBridgePortAcceptableFrameTypes)	\
	nmhSetCmn(Ieee8021QBridgePortAcceptableFrameTypes, 13, Ieee8021QBridgePortAcceptableFrameTypesSet, NULL, NULL, 0, 0, 2, "%u %u %i", u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValIeee8021QBridgePortAcceptableFrameTypes)
#define nmhSetIeee8021QBridgePortIngressFiltering(u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValIeee8021QBridgePortIngressFiltering)	\
	nmhSetCmn(Ieee8021QBridgePortIngressFiltering, 13, Ieee8021QBridgePortIngressFilteringSet, NULL, NULL, 0, 0, 2, "%u %u %i", u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValIeee8021QBridgePortIngressFiltering)
#define nmhSetIeee8021QBridgePortMvrpEnabledStatus(u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValIeee8021QBridgePortMvrpEnabledStatus)	\
	nmhSetCmn(Ieee8021QBridgePortMvrpEnabledStatus, 13, Ieee8021QBridgePortMvrpEnabledStatusSet, MrpLock, MrpUnLock, 0, 0, 2, "%u %u %i", u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValIeee8021QBridgePortMvrpEnabledStatus)
#define nmhSetIeee8021QBridgePortRestrictedVlanRegistration(u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValIeee8021QBridgePortRestrictedVlanRegistration)	\
	nmhSetCmn(Ieee8021QBridgePortRestrictedVlanRegistration, 13, Ieee8021QBridgePortRestrictedVlanRegistrationSet, MrpLock, MrpUnLock, 0, 0, 2, "%u %u %i", u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValIeee8021QBridgePortRestrictedVlanRegistration)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ieee8021QBridgeLearningConstraintsComponentId[13];
extern UINT4 Ieee8021QBridgeLearningConstraintsVlan[13];
extern UINT4 Ieee8021QBridgeLearningConstraintsSet[13];
extern UINT4 Ieee8021QBridgeLearningConstraintsType[13];
extern UINT4 Ieee8021QBridgeLearningConstraintsStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIeee8021QBridgeLearningConstraintsType(u4Ieee8021QBridgeLearningConstraintsComponentId , u4Ieee8021QBridgeLearningConstraintsVlan , i4Ieee8021QBridgeLearningConstraintsSet ,i4SetValIeee8021QBridgeLearningConstraintsType)	\
	nmhSetCmn(Ieee8021QBridgeLearningConstraintsType, 13, Ieee8021QBridgeLearningConstraintsTypeSet, NULL, NULL, 0, 0, 3, "%u %u %i %i", u4Ieee8021QBridgeLearningConstraintsComponentId , u4Ieee8021QBridgeLearningConstraintsVlan , i4Ieee8021QBridgeLearningConstraintsSet ,i4SetValIeee8021QBridgeLearningConstraintsType)
#define nmhSetIeee8021QBridgeLearningConstraintsStatus(u4Ieee8021QBridgeLearningConstraintsComponentId , u4Ieee8021QBridgeLearningConstraintsVlan , i4Ieee8021QBridgeLearningConstraintsSet ,i4SetValIeee8021QBridgeLearningConstraintsStatus)	\
	nmhSetCmn(Ieee8021QBridgeLearningConstraintsStatus, 13, Ieee8021QBridgeLearningConstraintsStatusSet, NULL, NULL, 0, 1, 3, "%u %u %i %i", u4Ieee8021QBridgeLearningConstraintsComponentId , u4Ieee8021QBridgeLearningConstraintsVlan , i4Ieee8021QBridgeLearningConstraintsSet ,i4SetValIeee8021QBridgeLearningConstraintsStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ieee8021QBridgeLearningConstraintDefaultsComponentId[13];
extern UINT4 Ieee8021QBridgeLearningConstraintDefaultsSet[13];
extern UINT4 Ieee8021QBridgeLearningConstraintDefaultsType[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIeee8021QBridgeLearningConstraintDefaultsSet(u4Ieee8021QBridgeLearningConstraintDefaultsComponentId ,i4SetValIeee8021QBridgeLearningConstraintDefaultsSet)	\
	nmhSetCmn(Ieee8021QBridgeLearningConstraintDefaultsSet, 13, Ieee8021QBridgeLearningConstraintDefaultsSetSet, NULL, NULL, 0, 0, 1, "%u %i", u4Ieee8021QBridgeLearningConstraintDefaultsComponentId ,i4SetValIeee8021QBridgeLearningConstraintDefaultsSet)
#define nmhSetIeee8021QBridgeLearningConstraintDefaultsType(u4Ieee8021QBridgeLearningConstraintDefaultsComponentId ,i4SetValIeee8021QBridgeLearningConstraintDefaultsType)	\
	nmhSetCmn(Ieee8021QBridgeLearningConstraintDefaultsType, 13, Ieee8021QBridgeLearningConstraintDefaultsTypeSet, NULL, NULL, 0, 0, 1, "%u %i", u4Ieee8021QBridgeLearningConstraintDefaultsComponentId ,i4SetValIeee8021QBridgeLearningConstraintDefaultsType)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ieee8021QBridgeProtocolGroupComponentId[13];
extern UINT4 Ieee8021QBridgeProtocolTemplateFrameType[13];
extern UINT4 Ieee8021QBridgeProtocolTemplateProtocolValue[13];
extern UINT4 Ieee8021QBridgeProtocolGroupId[13];
extern UINT4 Ieee8021QBridgeProtocolGroupRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIeee8021QBridgeProtocolGroupId(u4Ieee8021QBridgeProtocolGroupComponentId , i4Ieee8021QBridgeProtocolTemplateFrameType , pIeee8021QBridgeProtocolTemplateProtocolValue ,i4SetValIeee8021QBridgeProtocolGroupId)	\
	nmhSetCmn(Ieee8021QBridgeProtocolGroupId, 13, Ieee8021QBridgeProtocolGroupIdSet, NULL, NULL, 0, 0, 3, "%u %i %s %i", u4Ieee8021QBridgeProtocolGroupComponentId , i4Ieee8021QBridgeProtocolTemplateFrameType , pIeee8021QBridgeProtocolTemplateProtocolValue ,i4SetValIeee8021QBridgeProtocolGroupId)
#define nmhSetIeee8021QBridgeProtocolGroupRowStatus(u4Ieee8021QBridgeProtocolGroupComponentId , i4Ieee8021QBridgeProtocolTemplateFrameType , pIeee8021QBridgeProtocolTemplateProtocolValue ,i4SetValIeee8021QBridgeProtocolGroupRowStatus)	\
	nmhSetCmn(Ieee8021QBridgeProtocolGroupRowStatus, 13, Ieee8021QBridgeProtocolGroupRowStatusSet, NULL, NULL, 0, 1, 3, "%u %i %s %i", u4Ieee8021QBridgeProtocolGroupComponentId , i4Ieee8021QBridgeProtocolTemplateFrameType , pIeee8021QBridgeProtocolTemplateProtocolValue ,i4SetValIeee8021QBridgeProtocolGroupRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ieee8021QBridgeProtocolPortGroupId[13];
extern UINT4 Ieee8021QBridgeProtocolPortGroupVid[13];
extern UINT4 Ieee8021QBridgeProtocolPortRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIeee8021QBridgeProtocolPortGroupVid(u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort , i4Ieee8021QBridgeProtocolPortGroupId ,i4SetValIeee8021QBridgeProtocolPortGroupVid)	\
	nmhSetCmn(Ieee8021QBridgeProtocolPortGroupVid, 13, Ieee8021QBridgeProtocolPortGroupVidSet, NULL, NULL, 0, 0, 3, "%u %u %i %i", u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort , i4Ieee8021QBridgeProtocolPortGroupId ,i4SetValIeee8021QBridgeProtocolPortGroupVid)
#define nmhSetIeee8021QBridgeProtocolPortRowStatus(u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort , i4Ieee8021QBridgeProtocolPortGroupId ,i4SetValIeee8021QBridgeProtocolPortRowStatus)	\
	nmhSetCmn(Ieee8021QBridgeProtocolPortRowStatus, 13, Ieee8021QBridgeProtocolPortRowStatusSet, NULL, NULL, 0, 1, 3, "%u %u %i %i", u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort , i4Ieee8021QBridgeProtocolPortGroupId ,i4SetValIeee8021QBridgeProtocolPortRowStatus)

#endif
