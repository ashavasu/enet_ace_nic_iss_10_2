/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: npdbgcli.h,v 1.5 2013/07/03 12:26:08 siva Exp $
*
* Description: Contains NP debug CLI related macros.
*
*******************************************************************/

#ifndef NPDEBUGCLI_H
#define NPDEBUGCLI_H
enum {

 CLI_HW_REGISTER_READ =1,
 CLI_HW_REGISTER_WRITE,
 CLI_DEBUG_REGISTER_READ_ARP,
 CLI_SW_RATE_LIMIT,
 CLI_HW_PORT_MAPPING,
 CLI_HW_EGRESS_COUNTER_SET,
 CLI_HW_EGRESS_COUNTERS_READ
};
INT4 cli_process_xcat_cmd(tCliHandle CliHandle, UINT4 u4Command,...);

#endif

