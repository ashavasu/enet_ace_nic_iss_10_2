/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsissdcli.h,v 1.2 2010/02/11 12:17:51 prabuc Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDsSystemControl[11];
extern UINT4 FsDsStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDsSystemControl(i4SetValFsDsSystemControl)	\
	nmhSetCmn(FsDsSystemControl, 11, FsDsSystemControlSet, DfsLock, DfsUnLock, 0, 0, 0, "%i", i4SetValFsDsSystemControl)
#define nmhSetFsDsStatus(i4SetValFsDsStatus)	\
	nmhSetCmn(FsDsStatus, 11, FsDsStatusSet, DfsLock, DfsUnLock, 0, 0, 0, "%i", i4SetValFsDsStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDiffServMultiFieldClfrId[13];
extern UINT4 FsDiffServMultiFieldClfrFilterId[13];
extern UINT4 FsDiffServMultiFieldClfrFilterType[13];
extern UINT4 FsDiffServMultiFieldClfrStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDiffServMultiFieldClfrFilterId(i4FsDiffServMultiFieldClfrId ,u4SetValFsDiffServMultiFieldClfrFilterId)	\
	nmhSetCmn(FsDiffServMultiFieldClfrFilterId, 13, FsDiffServMultiFieldClfrFilterIdSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %u", i4FsDiffServMultiFieldClfrId ,u4SetValFsDiffServMultiFieldClfrFilterId)
#define nmhSetFsDiffServMultiFieldClfrFilterType(i4FsDiffServMultiFieldClfrId ,i4SetValFsDiffServMultiFieldClfrFilterType)	\
	nmhSetCmn(FsDiffServMultiFieldClfrFilterType, 13, FsDiffServMultiFieldClfrFilterTypeSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %i", i4FsDiffServMultiFieldClfrId ,i4SetValFsDiffServMultiFieldClfrFilterType)
#define nmhSetFsDiffServMultiFieldClfrStatus(i4FsDiffServMultiFieldClfrId ,i4SetValFsDiffServMultiFieldClfrStatus)	\
	nmhSetCmn(FsDiffServMultiFieldClfrStatus, 13, FsDiffServMultiFieldClfrStatusSet, DfsLock, DfsUnLock, 0, 1, 1, "%i %i", i4FsDiffServMultiFieldClfrId ,i4SetValFsDiffServMultiFieldClfrStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDiffServClfrId[13];
extern UINT4 FsDiffServClfrMFClfrId[13];
extern UINT4 FsDiffServClfrInProActionId[13];
extern UINT4 FsDiffServClfrOutProActionId[13];
extern UINT4 FsDiffServClfrStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDiffServClfrMFClfrId(i4FsDiffServClfrId ,i4SetValFsDiffServClfrMFClfrId)	\
	nmhSetCmn(FsDiffServClfrMFClfrId, 13, FsDiffServClfrMFClfrIdSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %i", i4FsDiffServClfrId ,i4SetValFsDiffServClfrMFClfrId)
#define nmhSetFsDiffServClfrInProActionId(i4FsDiffServClfrId ,i4SetValFsDiffServClfrInProActionId)	\
	nmhSetCmn(FsDiffServClfrInProActionId, 13, FsDiffServClfrInProActionIdSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %i", i4FsDiffServClfrId ,i4SetValFsDiffServClfrInProActionId)
#define nmhSetFsDiffServClfrOutProActionId(i4FsDiffServClfrId ,i4SetValFsDiffServClfrOutProActionId)	\
	nmhSetCmn(FsDiffServClfrOutProActionId, 13, FsDiffServClfrOutProActionIdSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %i", i4FsDiffServClfrId ,i4SetValFsDiffServClfrOutProActionId)
#define nmhSetFsDiffServClfrStatus(i4FsDiffServClfrId ,i4SetValFsDiffServClfrStatus)	\
	nmhSetCmn(FsDiffServClfrStatus, 13, FsDiffServClfrStatusSet, DfsLock, DfsUnLock, 0, 1, 1, "%i %i", i4FsDiffServClfrId ,i4SetValFsDiffServClfrStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDiffServInProfileActionId[13];
extern UINT4 FsDiffServInProfileActionFlag[13];
extern UINT4 FsDiffServInProfileActionNewPrio[13];
extern UINT4 FsDiffServInProfileActionIpTOS[13];
extern UINT4 FsDiffServInProfileActionPort[13];
extern UINT4 FsDiffServInProfileActionDscp[13];
extern UINT4 FsDiffServInProfileActionStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDiffServInProfileActionFlag(i4FsDiffServInProfileActionId ,u4SetValFsDiffServInProfileActionFlag)	\
	nmhSetCmn(FsDiffServInProfileActionFlag, 13, FsDiffServInProfileActionFlagSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %u", i4FsDiffServInProfileActionId ,u4SetValFsDiffServInProfileActionFlag)
#define nmhSetFsDiffServInProfileActionNewPrio(i4FsDiffServInProfileActionId ,u4SetValFsDiffServInProfileActionNewPrio)	\
	nmhSetCmn(FsDiffServInProfileActionNewPrio, 13, FsDiffServInProfileActionNewPrioSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %u", i4FsDiffServInProfileActionId ,u4SetValFsDiffServInProfileActionNewPrio)
#define nmhSetFsDiffServInProfileActionIpTOS(i4FsDiffServInProfileActionId ,u4SetValFsDiffServInProfileActionIpTOS)	\
	nmhSetCmn(FsDiffServInProfileActionIpTOS, 13, FsDiffServInProfileActionIpTOSSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %u", i4FsDiffServInProfileActionId ,u4SetValFsDiffServInProfileActionIpTOS)
#define nmhSetFsDiffServInProfileActionPort(i4FsDiffServInProfileActionId ,u4SetValFsDiffServInProfileActionPort)	\
	nmhSetCmn(FsDiffServInProfileActionPort, 13, FsDiffServInProfileActionPortSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %u", i4FsDiffServInProfileActionId ,u4SetValFsDiffServInProfileActionPort)
#define nmhSetFsDiffServInProfileActionDscp(i4FsDiffServInProfileActionId ,u4SetValFsDiffServInProfileActionDscp)	\
	nmhSetCmn(FsDiffServInProfileActionDscp, 13, FsDiffServInProfileActionDscpSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %u", i4FsDiffServInProfileActionId ,u4SetValFsDiffServInProfileActionDscp)
#define nmhSetFsDiffServInProfileActionStatus(i4FsDiffServInProfileActionId ,i4SetValFsDiffServInProfileActionStatus)	\
	nmhSetCmn(FsDiffServInProfileActionStatus, 13, FsDiffServInProfileActionStatusSet, DfsLock, DfsUnLock, 0, 1, 1, "%i %i", i4FsDiffServInProfileActionId ,i4SetValFsDiffServInProfileActionStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDiffServOutProfileActionId[13];
extern UINT4 FsDiffServOutProfileActionFlag[13];
extern UINT4 FsDiffServOutProfileActionDscp[13];
extern UINT4 FsDiffServOutProfileActionMID[13];
extern UINT4 FsDiffServOutProfileActionStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDiffServOutProfileActionFlag(i4FsDiffServOutProfileActionId ,u4SetValFsDiffServOutProfileActionFlag)	\
	nmhSetCmn(FsDiffServOutProfileActionFlag, 13, FsDiffServOutProfileActionFlagSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %u", i4FsDiffServOutProfileActionId ,u4SetValFsDiffServOutProfileActionFlag)
#define nmhSetFsDiffServOutProfileActionDscp(i4FsDiffServOutProfileActionId ,u4SetValFsDiffServOutProfileActionDscp)	\
	nmhSetCmn(FsDiffServOutProfileActionDscp, 13, FsDiffServOutProfileActionDscpSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %u", i4FsDiffServOutProfileActionId ,u4SetValFsDiffServOutProfileActionDscp)
#define nmhSetFsDiffServOutProfileActionMID(i4FsDiffServOutProfileActionId ,i4SetValFsDiffServOutProfileActionMID)	\
	nmhSetCmn(FsDiffServOutProfileActionMID, 13, FsDiffServOutProfileActionMIDSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %i", i4FsDiffServOutProfileActionId ,i4SetValFsDiffServOutProfileActionMID)
#define nmhSetFsDiffServOutProfileActionStatus(i4FsDiffServOutProfileActionId ,i4SetValFsDiffServOutProfileActionStatus)	\
	nmhSetCmn(FsDiffServOutProfileActionStatus, 13, FsDiffServOutProfileActionStatusSet, DfsLock, DfsUnLock, 0, 1, 1, "%i %i", i4FsDiffServOutProfileActionId ,i4SetValFsDiffServOutProfileActionStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDiffServMeterId[13];
extern UINT4 FsDiffServMetertokenSize[13];
extern UINT4 FsDiffServMeterRefreshCount[13];
extern UINT4 FsDiffServMeterStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDiffServMetertokenSize(i4FsDiffServMeterId ,u4SetValFsDiffServMetertokenSize)	\
	nmhSetCmn(FsDiffServMetertokenSize, 13, FsDiffServMetertokenSizeSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %u", i4FsDiffServMeterId ,u4SetValFsDiffServMetertokenSize)
#define nmhSetFsDiffServMeterRefreshCount(i4FsDiffServMeterId ,u4SetValFsDiffServMeterRefreshCount)	\
	nmhSetCmn(FsDiffServMeterRefreshCount, 13, FsDiffServMeterRefreshCountSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %u", i4FsDiffServMeterId ,u4SetValFsDiffServMeterRefreshCount)
#define nmhSetFsDiffServMeterStatus(i4FsDiffServMeterId ,i4SetValFsDiffServMeterStatus)	\
	nmhSetCmn(FsDiffServMeterStatus, 13, FsDiffServMeterStatusSet, DfsLock, DfsUnLock, 0, 1, 1, "%i %i", i4FsDiffServMeterId ,i4SetValFsDiffServMeterStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDiffServSchedulerId[13];
extern UINT4 FsDiffServSchedulerDPId[13];
extern UINT4 FsDiffServSchedulerQueueCount[13];
extern UINT4 FsDiffServSchedulerWeight[13];
extern UINT4 FsDiffServSchedulerStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDiffServSchedulerDPId(i4FsDiffServSchedulerId ,i4SetValFsDiffServSchedulerDPId)	\
	nmhSetCmn(FsDiffServSchedulerDPId, 13, FsDiffServSchedulerDPIdSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %i", i4FsDiffServSchedulerId ,i4SetValFsDiffServSchedulerDPId)
#define nmhSetFsDiffServSchedulerQueueCount(i4FsDiffServSchedulerId ,u4SetValFsDiffServSchedulerQueueCount)	\
	nmhSetCmn(FsDiffServSchedulerQueueCount, 13, FsDiffServSchedulerQueueCountSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %u", i4FsDiffServSchedulerId ,u4SetValFsDiffServSchedulerQueueCount)
#define nmhSetFsDiffServSchedulerWeight(i4FsDiffServSchedulerId ,pSetValFsDiffServSchedulerWeight)	\
	nmhSetCmn(FsDiffServSchedulerWeight, 13, FsDiffServSchedulerWeightSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %s", i4FsDiffServSchedulerId ,pSetValFsDiffServSchedulerWeight)
#define nmhSetFsDiffServSchedulerStatus(i4FsDiffServSchedulerId ,i4SetValFsDiffServSchedulerStatus)	\
	nmhSetCmn(FsDiffServSchedulerStatus, 13, FsDiffServSchedulerStatusSet, DfsLock, DfsUnLock, 0, 1, 1, "%i %i", i4FsDiffServSchedulerId ,i4SetValFsDiffServSchedulerStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDiffServPortId[13];
extern UINT4 FsDiffServCoSqAlgorithm[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDiffServCoSqAlgorithm(i4FsDiffServPortId ,i4SetValFsDiffServCoSqAlgorithm)	\
	nmhSetCmn(FsDiffServCoSqAlgorithm, 13, FsDiffServCoSqAlgorithmSet, DfsLock, DfsUnLock, 0, 0, 1, "%i %i", i4FsDiffServPortId ,i4SetValFsDiffServCoSqAlgorithm)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsDiffServBaseCoSqPortId[13];
extern UINT4 FsDiffServPortCoSqId[13];
extern UINT4 FsDiffServCoSqWeight[13];
extern UINT4 FsDiffServCoSqBwMin[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsDiffServCoSqWeight(i4FsDiffServBaseCoSqPortId , i4FsDiffServPortCoSqId ,i4SetValFsDiffServCoSqWeight)	\
	nmhSetCmn(FsDiffServCoSqWeight, 13, FsDiffServCoSqWeightSet, DfsLock, DfsUnLock, 0, 0, 2, "%i %i %i", i4FsDiffServBaseCoSqPortId , i4FsDiffServPortCoSqId ,i4SetValFsDiffServCoSqWeight)
#define nmhSetFsDiffServCoSqBwMin(i4FsDiffServBaseCoSqPortId , i4FsDiffServPortCoSqId ,u4SetValFsDiffServCoSqBwMin)	\
	nmhSetCmn(FsDiffServCoSqBwMin, 13, FsDiffServCoSqBwMinSet, DfsLock, DfsUnLock, 0, 0, 2, "%i %i %u", i4FsDiffServBaseCoSqPortId , i4FsDiffServPortCoSqId ,u4SetValFsDiffServCoSqBwMin)

#endif
