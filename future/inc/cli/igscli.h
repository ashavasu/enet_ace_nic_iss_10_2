/* $Id: igscli.h,v 1.14 2015/02/13 11:13:58 siva Exp $*/
/* INCLUDE FILE HEADER :
 *
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : igscli.h                                         |
 * |  PRINCIPAL AUTHOR      : ISS                                              |
 * |  SUBSYSTEM NAME        : IGS                                              |
 * |  MODULE NAME           : CLI interface for IGS configuration              |
 * |  LANGUAGE              : C                                                |
 * |  TARGET ENVIRONMENT    :                                                  |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |  DESCRIPTION           : This file contains command idenfiers for         | * |                          definitions in igscmd.def, function prototypes   |
 * |                          for IGS CLI and corresponding error code         |
 * |                          definitions                                      |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 */

#ifndef __IGSCLI_H__
#define __IGSCLI_H__
#include "cli.h"
/* IGS CLI command constants. To add a new command ,a new definition needs to
 * be added to the list.
 */
enum 
{
  IGS_SYS_SHUT = 1,
  IGS_SYS_NO_SHUT,
  IGS_SYS_ENABLE,
  IGS_SYS_DISABLE,
  IGS_PROXY_ENABLE,
  IGS_PROXY_DISABLE,
  IGS_MCAST_FWD_MODE,
  IGS_ROUTER_PORT_PURGE_INTERVAL,
  IGS_NO_ROUTER_PORT_PURGE_INTERVAL,
  IGS_PORT_PURGE_INTERVAL,
  IGS_NO_PORT_PURGE_INTERVAL,
  IGS_REPORT_SUPPRESS_INTERVAL,
  IGS_NO_REPORT_SUPPRESS_INTERVAL,
  IGS_RETRY_COUNT,
  IGS_NO_RETRY_COUNT,
  IGS_GROUP_QUERY_INTERVAL,
  IGS_NO_GROUP_QUERY_INTERVAL,
  IGS_REPORT_FORWADING,
  IGS_NO_REPORT_FORWADING,
  IGS_VLAN_ENABLE,
  IGS_VLAN_DISABLE,
  IGS_VLAN_OPER_VERSION,
  IGS_VLAN_FAST_LEAVE_ENABLE,
  IGS_VLAN_FAST_LEAVE_DISABLE,
  IGS_VLAN_QUERIER_ENABLE,
  IGS_VLAN_QUERIER_DISABLE,
  IGS_VLAN_QUERY_INTERVAL,
  IGS_NO_VLAN_QUERY_INTERVAL,
  IGS_VLAN_RTR_PORT,
  IGS_VLAN_NO_RTR_PORT,
  IGS_TRACE_ENABLE,
  IGS_DEBUG_ENABLE,
  IGS_SHOW_VLAN_RTR_PORTS,
  IGS_RED_SHOW_VLAN_RTR_PORTS,
  IGS_SHOW_GLOBALS_INFO,
  IGS_SHOW_VLAN_INFO,
  IGS_RED_SHOW_VLAN_INFO,
  IGS_SHOW_GRP_ENTRIES,
  IGS_SHOW_FWD_ENTRIES,
  IGS_RED_SHOW_FWD_ENTRIES,
  IGS_SHOW_STATS
};

#define IGS_IP_FWD_MODE                1
#define IGS_MAC_FWD_MODE               2

#define IGS_IGMP_VERSION_1             1
#define IGS_IGMP_VERSION_2             2
#define IGS_IGMP_VERSION_3             3

#define IGS_ALL_PORTS                  1 
#define IGS_RTR_PORTS                  2 

#define IGS_TRC_INIT                   1
#define IGS_TRC_RESRC                  2
#define IGS_TRC_TMR                    4
#define IGS_TRC_SRC                    8
#define IGS_TRC_GRP                    16
#define IGS_TRC_QRY                    32
#define IGS_TRC_VLAN                   64
#define IGS_TRC_PKT                    128
#define IGS_TRC_FWD                    254
#define IGS_TRC_MGMT                   512
#define IGS_TRC_RED                    1024
#define IGS_TRC_ALL                    0xfff

#define IGS_CLI_MAX_ARGS               3
              
#define IGS_CLI_MAX_COMMANDS           37

#define IGS_MAX_INT4                 0x7fffffff
#define IGS_CLI_TRACE_DIS_FLAG       0x80000000

 /* Error code values and strings are maintained inside the protocol 
  * module itself.
  */
enum {
  CLI_IGS_INVALID_PORT_ERR = 1,
  CLI_IGS_GMRP_ENABLED_ERR,
  CLI_IGS_MAX_ERR
};
/* The error strings should be places under the switch so as to avoid 
 * redifinition This will be visible only in modulecli.c file
 */
#ifdef __IGSCLI_C__

CONST CHR1  *IgsCliErrString [] = {
    NULL,
    " \r% Invalid Port Number\r\n",
    " \r% GMRP is NOT Disabled!!!\r\n"
};

#endif

/* function prototypes */
INT4 cli_process_igs_cmd PROTO ((tCliHandle CliHandle, UINT4, ...));

#endif /* _IGSCLI_H_ */

