/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsfipscli.h,v 1.2 2011/07/20 08:33:23 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsFipsOperMode[11];
extern UINT4 FsFipsTestAlgo[11];
extern UINT4 FsfipsZeroizeCryptoKeys[11];
extern UINT4 FsFipsTraceLevel[11];
extern UINT4 FsFipsBypassCapability[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsFipsOperMode(i4SetValFsFipsOperMode) \
 nmhSetCmn(FsFipsOperMode, 11, FsFipsOperModeSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsFipsOperMode)
#define nmhSetFsFipsTestAlgo(i4SetValFsFipsTestAlgo) \
 nmhSetCmn(FsFipsTestAlgo, 11, FsFipsTestAlgoSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsFipsTestAlgo)
#define nmhSetFsfipsZeroizeCryptoKeys(i4SetValFsfipsZeroizeCryptoKeys) \
 nmhSetCmn(FsfipsZeroizeCryptoKeys, 11, FsfipsZeroizeCryptoKeysSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsfipsZeroizeCryptoKeys)
#define nmhSetFsFipsTraceLevel(i4SetValFsFipsTraceLevel) \
 nmhSetCmn(FsFipsTraceLevel, 11, FsFipsTraceLevelSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsFipsTraceLevel)
#define nmhSetFsFipsBypassCapability(i4SetValFsFipsBypassCapability) \
 nmhSetCmn(FsFipsBypassCapability, 11, FsFipsBypassCapabilitySet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsFipsBypassCapability)

#endif
