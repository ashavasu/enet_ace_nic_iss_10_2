/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: std1evcli.h,v 1.2 2016/07/16 11:15:02 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ieee8021BridgeEvbSysEvbLldpTxEnable[11];
extern UINT4 Ieee8021BridgeEvbSysEvbLldpManual[11];
extern UINT4 Ieee8021BridgeEvbSysEvbLldpGidCapable[11];
extern UINT4 Ieee8021BridgeEvbSysEcpAckTimer[11];
extern UINT4 Ieee8021BridgeEvbSysEcpMaxRetries[11];
extern UINT4 Ieee8021BridgeEvbSysVdpDfltRsrcWaitDelay[11];
extern UINT4 Ieee8021BridgeEvbSysVdpDfltReinitKeepAlive[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIeee8021BridgeEvbSysEvbLldpTxEnable(i4SetValIeee8021BridgeEvbSysEvbLldpTxEnable) \
 nmhSetCmn(Ieee8021BridgeEvbSysEvbLldpTxEnable, 11, Ieee8021BridgeEvbSysEvbLldpTxEnableSet, VlanLock, VlanUnLock, 0, 0, 0, "%i", i4SetValIeee8021BridgeEvbSysEvbLldpTxEnable)
#define nmhSetIeee8021BridgeEvbSysEvbLldpManual(i4SetValIeee8021BridgeEvbSysEvbLldpManual) \
 nmhSetCmn(Ieee8021BridgeEvbSysEvbLldpManual, 11, Ieee8021BridgeEvbSysEvbLldpManualSet, VlanLock, VlanUnLock, 0, 0, 0, "%i", i4SetValIeee8021BridgeEvbSysEvbLldpManual)
#define nmhSetIeee8021BridgeEvbSysEvbLldpGidCapable(i4SetValIeee8021BridgeEvbSysEvbLldpGidCapable) \
 nmhSetCmn(Ieee8021BridgeEvbSysEvbLldpGidCapable, 11, Ieee8021BridgeEvbSysEvbLldpGidCapableSet, VlanLock, VlanUnLock, 0, 0, 0, "%i", i4SetValIeee8021BridgeEvbSysEvbLldpGidCapable)
#define nmhSetIeee8021BridgeEvbSysEcpAckTimer(i4SetValIeee8021BridgeEvbSysEcpAckTimer) \
 nmhSetCmn(Ieee8021BridgeEvbSysEcpAckTimer, 11, Ieee8021BridgeEvbSysEcpAckTimerSet, VlanLock, VlanUnLock, 0, 0, 0, "%i", i4SetValIeee8021BridgeEvbSysEcpAckTimer)
#define nmhSetIeee8021BridgeEvbSysEcpMaxRetries(i4SetValIeee8021BridgeEvbSysEcpMaxRetries) \
 nmhSetCmn(Ieee8021BridgeEvbSysEcpMaxRetries, 11, Ieee8021BridgeEvbSysEcpMaxRetriesSet, VlanLock, VlanUnLock, 0, 0, 0, "%i", i4SetValIeee8021BridgeEvbSysEcpMaxRetries)
#define nmhSetIeee8021BridgeEvbSysVdpDfltRsrcWaitDelay(i4SetValIeee8021BridgeEvbSysVdpDfltRsrcWaitDelay) \
 nmhSetCmn(Ieee8021BridgeEvbSysVdpDfltRsrcWaitDelay, 11, Ieee8021BridgeEvbSysVdpDfltRsrcWaitDelaySet, VlanLock, VlanUnLock, 0, 0, 0, "%i", i4SetValIeee8021BridgeEvbSysVdpDfltRsrcWaitDelay)
#define nmhSetIeee8021BridgeEvbSysVdpDfltReinitKeepAlive(i4SetValIeee8021BridgeEvbSysVdpDfltReinitKeepAlive) \
 nmhSetCmn(Ieee8021BridgeEvbSysVdpDfltReinitKeepAlive, 11, Ieee8021BridgeEvbSysVdpDfltReinitKeepAliveSet, VlanLock, VlanUnLock, 0, 0, 0, "%i", i4SetValIeee8021BridgeEvbSysVdpDfltReinitKeepAlive)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ieee8021BridgeEvbSbpComponentID[13];
extern UINT4 Ieee8021BridgeEvbSbpPortNumber[13];
extern UINT4 Ieee8021BridgeEvbSbpLldpManual[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIeee8021BridgeEvbSbpLldpManual(u4Ieee8021BridgeEvbSbpComponentID , u4Ieee8021BridgeEvbSbpPortNumber ,i4SetValIeee8021BridgeEvbSbpLldpManual) \
 nmhSetCmn(Ieee8021BridgeEvbSbpLldpManual, 13, Ieee8021BridgeEvbSbpLldpManualSet, VlanLock, VlanUnLock, 0, 0, 2, "%u %u %i", u4Ieee8021BridgeEvbSbpComponentID , u4Ieee8021BridgeEvbSbpPortNumber ,i4SetValIeee8021BridgeEvbSbpLldpManual)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ieee8021BridgeEvbUAPSchCdcpAdminEnable[13];
extern UINT4 Ieee8021BridgeEvbUAPSchAdminCDCPRole[13];
extern UINT4 Ieee8021BridgeEvbUAPSchAdminCDCPChanCap[13];
extern UINT4 Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow[13];
extern UINT4 Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh[13];
extern UINT4 Ieee8021BridgeEvbUAPConfigStorageType[13];
extern UINT4 Ieee8021BridgeEvbUAPConfigRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIeee8021BridgeEvbUAPSchCdcpAdminEnable(u4Ieee8021BridgePhyPort ,i4SetValIeee8021BridgeEvbUAPSchCdcpAdminEnable) \
 nmhSetCmn(Ieee8021BridgeEvbUAPSchCdcpAdminEnable, 13, Ieee8021BridgeEvbUAPSchCdcpAdminEnableSet, VlanLock, VlanUnLock, 0, 0, 1, "%u %i", u4Ieee8021BridgePhyPort ,i4SetValIeee8021BridgeEvbUAPSchCdcpAdminEnable)
#define nmhSetIeee8021BridgeEvbUAPSchAdminCDCPRole(u4Ieee8021BridgePhyPort ,i4SetValIeee8021BridgeEvbUAPSchAdminCDCPRole) \
 nmhSetCmn(Ieee8021BridgeEvbUAPSchAdminCDCPRole, 13, Ieee8021BridgeEvbUAPSchAdminCDCPRoleSet, VlanLock, VlanUnLock, 0, 0, 1, "%u %i", u4Ieee8021BridgePhyPort ,i4SetValIeee8021BridgeEvbUAPSchAdminCDCPRole)
#define nmhSetIeee8021BridgeEvbUAPSchAdminCDCPChanCap(u4Ieee8021BridgePhyPort ,i4SetValIeee8021BridgeEvbUAPSchAdminCDCPChanCap) \
 nmhSetCmn(Ieee8021BridgeEvbUAPSchAdminCDCPChanCap, 13, Ieee8021BridgeEvbUAPSchAdminCDCPChanCapSet, VlanLock, VlanUnLock, 0, 0, 1, "%u %i", u4Ieee8021BridgePhyPort ,i4SetValIeee8021BridgeEvbUAPSchAdminCDCPChanCap)
#define nmhSetIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow(u4Ieee8021BridgePhyPort ,u4SetValIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow) \
 nmhSetCmn(Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow, 13, Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLowSet, VlanLock, VlanUnLock, 0, 0, 1, "%u %u", u4Ieee8021BridgePhyPort ,u4SetValIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow)
#define nmhSetIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh(u4Ieee8021BridgePhyPort ,u4SetValIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh) \
 nmhSetCmn(Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh, 13, Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHighSet, VlanLock, VlanUnLock, 0, 0, 1, "%u %u", u4Ieee8021BridgePhyPort ,u4SetValIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh)
#define nmhSetIeee8021BridgeEvbUAPConfigStorageType(u4Ieee8021BridgePhyPort ,i4SetValIeee8021BridgeEvbUAPConfigStorageType) \
 nmhSetCmn(Ieee8021BridgeEvbUAPConfigStorageType, 13, Ieee8021BridgeEvbUAPConfigStorageTypeSet, VlanLock, VlanUnLock, 0, 0, 1, "%u %i", u4Ieee8021BridgePhyPort ,i4SetValIeee8021BridgeEvbUAPConfigStorageType)
#define nmhSetIeee8021BridgeEvbUAPConfigRowStatus(u4Ieee8021BridgePhyPort ,i4SetValIeee8021BridgeEvbUAPConfigRowStatus) \
 nmhSetCmn(Ieee8021BridgeEvbUAPConfigRowStatus, 13, Ieee8021BridgeEvbUAPConfigRowStatusSet, VlanLock, VlanUnLock, 0, 1, 1, "%u %i", u4Ieee8021BridgePhyPort ,i4SetValIeee8021BridgeEvbUAPConfigRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ieee8021BridgeEvbSchID[13];
extern UINT4 Ieee8021BridgeEvbCAPAssociateSBPOrURPCompID[13];
extern UINT4 Ieee8021BridgeEvbCAPAssociateSBPOrURPPort[13];
extern UINT4 Ieee8021BridgeEvbCAPRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIeee8021BridgeEvbCAPAssociateSBPOrURPCompID(u4Ieee8021BridgePhyPort , u4Ieee8021BridgeEvbSchID ,u4SetValIeee8021BridgeEvbCAPAssociateSBPOrURPCompID) \
 nmhSetCmn(Ieee8021BridgeEvbCAPAssociateSBPOrURPCompID, 13, Ieee8021BridgeEvbCAPAssociateSBPOrURPCompIDSet, VlanLock, VlanUnLock, 0, 0, 2, "%u %u %u", u4Ieee8021BridgePhyPort , u4Ieee8021BridgeEvbSchID ,u4SetValIeee8021BridgeEvbCAPAssociateSBPOrURPCompID)
#define nmhSetIeee8021BridgeEvbCAPAssociateSBPOrURPPort(u4Ieee8021BridgePhyPort , u4Ieee8021BridgeEvbSchID ,u4SetValIeee8021BridgeEvbCAPAssociateSBPOrURPPort) \
 nmhSetCmn(Ieee8021BridgeEvbCAPAssociateSBPOrURPPort, 13, Ieee8021BridgeEvbCAPAssociateSBPOrURPPortSet, VlanLock, VlanUnLock, 0, 0, 2, "%u %u %u", u4Ieee8021BridgePhyPort , u4Ieee8021BridgeEvbSchID ,u4SetValIeee8021BridgeEvbCAPAssociateSBPOrURPPort)
#define nmhSetIeee8021BridgeEvbCAPRowStatus(u4Ieee8021BridgePhyPort , u4Ieee8021BridgeEvbSchID ,i4SetValIeee8021BridgeEvbCAPRowStatus) \
 nmhSetCmn(Ieee8021BridgeEvbCAPRowStatus, 13, Ieee8021BridgeEvbCAPRowStatusSet, VlanLock, VlanUnLock, 0, 1, 2, "%u %u %i", u4Ieee8021BridgePhyPort , u4Ieee8021BridgeEvbSchID ,i4SetValIeee8021BridgeEvbCAPRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ieee8021BridgeEvbURPComponentId[13];
extern UINT4 Ieee8021BridgeEvbURPPort[13];
extern UINT4 Ieee8021BridgeEvbURPIfIndex[13];
extern UINT4 Ieee8021BridgeEvbURPBindToISSPort[13];
extern UINT4 Ieee8021BridgeEvbURPLldpManual[13];
extern UINT4 Ieee8021BridgeEvbURPVdpOperRespWaitDelay[13];
extern UINT4 Ieee8021BridgeEvbURPVdpOperReinitKeepAlive[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIeee8021BridgeEvbURPIfIndex(u4Ieee8021BridgeEvbURPComponentId , u4Ieee8021BridgeEvbURPPort ,i4SetValIeee8021BridgeEvbURPIfIndex) \
 nmhSetCmn(Ieee8021BridgeEvbURPIfIndex, 13, Ieee8021BridgeEvbURPIfIndexSet, VlanLock, VlanUnLock, 0, 0, 2, "%u %u %i", u4Ieee8021BridgeEvbURPComponentId , u4Ieee8021BridgeEvbURPPort ,i4SetValIeee8021BridgeEvbURPIfIndex)
#define nmhSetIeee8021BridgeEvbURPBindToISSPort(u4Ieee8021BridgeEvbURPComponentId , u4Ieee8021BridgeEvbURPPort ,u4SetValIeee8021BridgeEvbURPBindToISSPort) \
 nmhSetCmn(Ieee8021BridgeEvbURPBindToISSPort, 13, Ieee8021BridgeEvbURPBindToISSPortSet, VlanLock, VlanUnLock, 0, 0, 2, "%u %u %u", u4Ieee8021BridgeEvbURPComponentId , u4Ieee8021BridgeEvbURPPort ,u4SetValIeee8021BridgeEvbURPBindToISSPort)
#define nmhSetIeee8021BridgeEvbURPLldpManual(u4Ieee8021BridgeEvbURPComponentId , u4Ieee8021BridgeEvbURPPort ,i4SetValIeee8021BridgeEvbURPLldpManual) \
 nmhSetCmn(Ieee8021BridgeEvbURPLldpManual, 13, Ieee8021BridgeEvbURPLldpManualSet, VlanLock, VlanUnLock, 0, 0, 2, "%u %u %i", u4Ieee8021BridgeEvbURPComponentId , u4Ieee8021BridgeEvbURPPort ,i4SetValIeee8021BridgeEvbURPLldpManual)
#define nmhSetIeee8021BridgeEvbURPVdpOperRespWaitDelay(u4Ieee8021BridgeEvbURPComponentId , u4Ieee8021BridgeEvbURPPort ,u4SetValIeee8021BridgeEvbURPVdpOperRespWaitDelay) \
 nmhSetCmn(Ieee8021BridgeEvbURPVdpOperRespWaitDelay, 13, Ieee8021BridgeEvbURPVdpOperRespWaitDelaySet, VlanLock, VlanUnLock, 0, 0, 2, "%u %u %u", u4Ieee8021BridgeEvbURPComponentId , u4Ieee8021BridgeEvbURPPort ,u4SetValIeee8021BridgeEvbURPVdpOperRespWaitDelay)
#define nmhSetIeee8021BridgeEvbURPVdpOperReinitKeepAlive(u4Ieee8021BridgeEvbURPComponentId , u4Ieee8021BridgeEvbURPPort ,u4SetValIeee8021BridgeEvbURPVdpOperReinitKeepAlive) \
 nmhSetCmn(Ieee8021BridgeEvbURPVdpOperReinitKeepAlive, 13, Ieee8021BridgeEvbURPVdpOperReinitKeepAliveSet, VlanLock, VlanUnLock, 0, 0, 2, "%u %u %u", u4Ieee8021BridgeEvbURPComponentId , u4Ieee8021BridgeEvbURPPort ,u4SetValIeee8021BridgeEvbURPVdpOperReinitKeepAlive)

#endif
