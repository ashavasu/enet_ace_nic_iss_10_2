/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsisscli.h,v 1.27 2017/01/27 12:27:26 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IssSwitchName[10];
extern UINT4 IssDefaultIpAddrCfgMode[10];
extern UINT4 IssDefaultIpAddr[10];
extern UINT4 IssDefaultIpSubnetMask[10];
extern UINT4 IssDefaultInterface[10];
extern UINT4 IssRestart[10];
extern UINT4 IssStandbyRestart[10];
extern UINT4 IssConfigSaveOption[10];
extern UINT4 IssConfigSaveIpAddr[10];
extern UINT4 IssConfigSaveFileName[10];
extern UINT4 IssInitiateConfigSave[10];
extern UINT4 IssConfigRestoreOption[10];
extern UINT4 IssConfigRestoreIpAddr[10];
extern UINT4 IssConfigRestoreFileName[10];
extern UINT4 IssInitiateConfigRestore[10];
extern UINT4 IssDlImageFromIp[10];
extern UINT4 IssDlImageName[10];
extern UINT4 IssInitiateDlImage[10];
extern UINT4 IssLoggingOption[10];
extern UINT4 IssPeerLoggingOption[10];
extern UINT4 IssUploadLogFileToIp[10];
extern UINT4 IssLogFileName[10];
extern UINT4 IssInitiateUlLogFile[10];
extern UINT4 IssSysContact[10];
extern UINT4 IssSysLocation[10];
extern UINT4 IssLoginAuthentication[10];
extern UINT4 IssSwitchBaseMacAddress[10];
extern UINT4 IssSwitchDate[10];
extern UINT4 IssNoCliConsole[10];
extern UINT4 IssDefaultIpAddrAllocProtocol[10];
extern UINT4 IssHttpPort[10];
extern UINT4 IssHttpStatus[10];
extern UINT4 IssDefaultRmIfName[10];
extern UINT4 IssDefaultVlanId[10];
extern UINT4 IssNpapiMode[10];
extern UINT4 IssConfigAutoSaveTrigger[10];
extern UINT4 IssConfigIncrSaveFlag[10];
extern UINT4 IssConfigRollbackFlag[10];
extern UINT4 IssConfigSyncUpOperation[10];
extern UINT4 IssAuditLogStatus[10];
extern UINT4 IssAuditLogFileName[10];
extern UINT4 IssAuditLogFileSize[10];
extern UINT4 IssAuditLogSizeThreshold[10];
extern UINT4 IssAuditLogReset[10];
extern UINT4 IssAuditLogRemoteIpAddr[10];
extern UINT4 IssAuditLogInitiateTransfer[10];
extern UINT4 IssAuditTransferFileName[10];
extern UINT4 IssDebugOption[10];
extern UINT4 IssConfigDefaultValueSaveOption[10];
extern UINT4 IssConfigSaveIpAddrType[10];
extern UINT4 IssConfigSaveIpvxAddr[10];
extern UINT4 IssConfigRestoreIpAddrType[10];
extern UINT4 IssConfigRestoreIpvxAddr[10];
extern UINT4 IssDlImageFromIpAddrType[10];
extern UINT4 IssDlImageFromIpvx[10];
extern UINT4 IssUploadLogFileToIpAddrType[10];
extern UINT4 IssUploadLogFileToIpvx[10];
extern UINT4 IssAuditLogRemoteIpAddrType[10];
extern UINT4 IssAuditLogRemoteIpvxAddr[10];
extern UINT4 IssSystemTimerSpeed[10];
extern UINT4 IssMacLearnRateLimit[10];
extern UINT4 IssMacLearnRateLimitInterval[10];
extern UINT4 IssLoginAttempts[10];
extern UINT4 IssLoginLockTime[10];
extern UINT4 IssTelnetStatus[10];
extern UINT4 IssTelnetClientStatus[10];
extern UINT4 IssSshClientStatus[10];
extern UINT4 IssWebSessionTimeOut[10];
extern UINT4 IssWebSessionMaxUsers[10];
extern UINT4 IssHeartBeatMode[10];
extern UINT4 IssRmRType[10];
extern UINT4 IssRmDType[10];
extern UINT4 IssRmStackingInterfaceType[10];
extern UINT4 IssRestoreType[10];
extern UINT4 IssSwitchModeType[10];
extern UINT4 IssCpuMirrorType[10];
extern UINT4 IssCpuMirrorToPort[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIssSwitchName(pSetValIssSwitchName) \
 nmhSetCmn(IssSwitchName, 10, IssSwitchNameSet, IssLock, IssUnLock, 0, 0, 0, "%s", pSetValIssSwitchName)
#define nmhSetIssDefaultIpAddrCfgMode(i4SetValIssDefaultIpAddrCfgMode) \
 nmhSetCmn(IssDefaultIpAddrCfgMode, 10, IssDefaultIpAddrCfgModeSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssDefaultIpAddrCfgMode)
#define nmhSetIssDefaultIpAddr(u4SetValIssDefaultIpAddr) \
 nmhSetCmn(IssDefaultIpAddr, 10, IssDefaultIpAddrSet, IssLock, IssUnLock, 0, 0, 0, "%p", u4SetValIssDefaultIpAddr)
#define nmhSetIssDefaultIpSubnetMask(u4SetValIssDefaultIpSubnetMask) \
 nmhSetCmn(IssDefaultIpSubnetMask, 10, IssDefaultIpSubnetMaskSet, IssLock, IssUnLock, 0, 0, 0, "%p", u4SetValIssDefaultIpSubnetMask)
#define nmhSetIssDefaultInterface(pSetValIssDefaultInterface) \
 nmhSetCmn(IssDefaultInterface, 10, IssDefaultInterfaceSet, IssLock, IssUnLock, 0, 0, 0, "%s", pSetValIssDefaultInterface)
#define nmhSetIssRestart(i4SetValIssRestart) \
 nmhSetCmn(IssRestart, 10, IssRestartSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssRestart)
#define nmhSetIssStandbyRestart(i4SetValIssStandbyRestart) \
 nmhSetCmn(IssStandbyRestart, 10, IssStandbyRestartSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssStandbyRestart)
#define nmhSetIssConfigSaveOption(i4SetValIssConfigSaveOption) \
 nmhSetCmn(IssConfigSaveOption, 10, IssConfigSaveOptionSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssConfigSaveOption)
#define nmhSetIssConfigSaveIpAddr(u4SetValIssConfigSaveIpAddr) \
 nmhSetCmn(IssConfigSaveIpAddr, 10, IssConfigSaveIpAddrSet, IssLock, IssUnLock, 0, 0, 0, "%p", u4SetValIssConfigSaveIpAddr)
#define nmhSetIssConfigSaveFileName(pSetValIssConfigSaveFileName) \
 nmhSetCmn(IssConfigSaveFileName, 10, IssConfigSaveFileNameSet, IssLock, IssUnLock, 0, 0, 0, "%s", pSetValIssConfigSaveFileName)
#define nmhSetIssInitiateConfigSave(i4SetValIssInitiateConfigSave) \
 nmhSetCmn(IssInitiateConfigSave, 10, IssInitiateConfigSaveSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssInitiateConfigSave)
#define nmhSetIssConfigRestoreOption(i4SetValIssConfigRestoreOption) \
 nmhSetCmn(IssConfigRestoreOption, 10, IssConfigRestoreOptionSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssConfigRestoreOption)
#define nmhSetIssConfigRestoreIpAddr(u4SetValIssConfigRestoreIpAddr) \
 nmhSetCmn(IssConfigRestoreIpAddr, 10, IssConfigRestoreIpAddrSet, IssLock, IssUnLock, 0, 0, 0, "%p", u4SetValIssConfigRestoreIpAddr)
#define nmhSetIssConfigRestoreFileName(pSetValIssConfigRestoreFileName) \
 nmhSetCmn(IssConfigRestoreFileName, 10, IssConfigRestoreFileNameSet, IssLock, IssUnLock, 0, 0, 0, "%s", pSetValIssConfigRestoreFileName)
#define nmhSetIssInitiateConfigRestore(i4SetValIssInitiateConfigRestore) \
 nmhSetCmn(IssInitiateConfigRestore, 10, IssInitiateConfigRestoreSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssInitiateConfigRestore)
#define nmhSetIssDlImageFromIp(u4SetValIssDlImageFromIp) \
 nmhSetCmn(IssDlImageFromIp, 10, IssDlImageFromIpSet, IssLock, IssUnLock, 0, 0, 0, "%p", u4SetValIssDlImageFromIp)
#define nmhSetIssDlImageName(pSetValIssDlImageName) \
 nmhSetCmn(IssDlImageName, 10, IssDlImageNameSet, IssLock, IssUnLock, 0, 0, 0, "%s", pSetValIssDlImageName)
#define nmhSetIssInitiateDlImage(i4SetValIssInitiateDlImage) \
 nmhSetCmn(IssInitiateDlImage, 10, IssInitiateDlImageSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssInitiateDlImage)
#define nmhSetIssLoggingOption(i4SetValIssLoggingOption) \
 nmhSetCmn(IssLoggingOption, 10, IssLoggingOptionSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssLoggingOption)
#define nmhSetIssPeerLoggingOption(i4SetValIssPeerLoggingOption) \
 nmhSetCmn(IssPeerLoggingOption, 10, IssPeerLoggingOptionSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssPeerLoggingOption)
#define nmhSetIssUploadLogFileToIp(u4SetValIssUploadLogFileToIp) \
 nmhSetCmn(IssUploadLogFileToIp, 10, IssUploadLogFileToIpSet, IssLock, IssUnLock, 0, 0, 0, "%p", u4SetValIssUploadLogFileToIp)
#define nmhSetIssLogFileName(pSetValIssLogFileName) \
 nmhSetCmn(IssLogFileName, 10, IssLogFileNameSet, IssLock, IssUnLock, 0, 0, 0, "%s", pSetValIssLogFileName)
#define nmhSetIssInitiateUlLogFile(i4SetValIssInitiateUlLogFile) \
 nmhSetCmn(IssInitiateUlLogFile, 10, IssInitiateUlLogFileSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssInitiateUlLogFile)
#define nmhSetIssSysContact(pSetValIssSysContact) \
 nmhSetCmn(IssSysContact, 10, IssSysContactSet, IssLock, IssUnLock, 0, 0, 0, "%s", pSetValIssSysContact)
#define nmhSetIssSysLocation(pSetValIssSysLocation) \
 nmhSetCmn(IssSysLocation, 10, IssSysLocationSet, IssLock, IssUnLock, 0, 0, 0, "%s", pSetValIssSysLocation)
#define nmhSetIssLoginAuthentication(i4SetValIssLoginAuthentication) \
 nmhSetCmn(IssLoginAuthentication, 10, IssLoginAuthenticationSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssLoginAuthentication)
#define nmhSetIssSwitchBaseMacAddress(SetValIssSwitchBaseMacAddress) \
 nmhSetCmn(IssSwitchBaseMacAddress, 10, IssSwitchBaseMacAddressSet, IssLock, IssUnLock, 0, 0, 0, "%m", SetValIssSwitchBaseMacAddress)
#define nmhSetIssSwitchDate(pSetValIssSwitchDate) \
 nmhSetCmn(IssSwitchDate, 10, IssSwitchDateSet, IssLock, IssUnLock, 0, 0, 0, "%s", pSetValIssSwitchDate)
#define nmhSetIssNoCliConsole(i4SetValIssNoCliConsole) \
 nmhSetCmn(IssNoCliConsole, 10, IssNoCliConsoleSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssNoCliConsole)
#define nmhSetIssDefaultIpAddrAllocProtocol(i4SetValIssDefaultIpAddrAllocProtocol) \
 nmhSetCmn(IssDefaultIpAddrAllocProtocol, 10, IssDefaultIpAddrAllocProtocolSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssDefaultIpAddrAllocProtocol)
#define nmhSetIssHttpPort(i4SetValIssHttpPort) \
 nmhSetCmn(IssHttpPort, 10, IssHttpPortSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssHttpPort)
#define nmhSetIssHttpStatus(i4SetValIssHttpStatus) \
 nmhSetCmn(IssHttpStatus, 10, IssHttpStatusSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssHttpStatus)
#define nmhSetIssDefaultRmIfName(pSetValIssDefaultRmIfName) \
 nmhSetCmn(IssDefaultRmIfName, 10, IssDefaultRmIfNameSet, IssLock, IssUnLock, 0, 0, 0, "%s", pSetValIssDefaultRmIfName)
#define nmhSetIssDefaultVlanId(i4SetValIssDefaultVlanId) \
 nmhSetCmn(IssDefaultVlanId, 10, IssDefaultVlanIdSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssDefaultVlanId)
#define nmhSetIssNpapiMode(pSetValIssNpapiMode) \
 nmhSetCmn(IssNpapiMode, 10, IssNpapiModeSet, IssLock, IssUnLock, 0, 0, 0, "%s", pSetValIssNpapiMode)
#define nmhSetIssConfigAutoSaveTrigger(i4SetValIssConfigAutoSaveTrigger) \
 nmhSetCmn(IssConfigAutoSaveTrigger, 10, IssConfigAutoSaveTriggerSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssConfigAutoSaveTrigger)
#define nmhSetIssConfigIncrSaveFlag(i4SetValIssConfigIncrSaveFlag) \
 nmhSetCmn(IssConfigIncrSaveFlag, 10, IssConfigIncrSaveFlagSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssConfigIncrSaveFlag)
#define nmhSetIssConfigRollbackFlag(i4SetValIssConfigRollbackFlag) \
 nmhSetCmn(IssConfigRollbackFlag, 10, IssConfigRollbackFlagSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssConfigRollbackFlag)
#define nmhSetIssConfigSyncUpOperation(i4SetValIssConfigSyncUpOperation) \
 nmhSetCmn(IssConfigSyncUpOperation, 10, IssConfigSyncUpOperationSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssConfigSyncUpOperation)
#define nmhSetIssAuditLogStatus(i4SetValIssAuditLogStatus) \
 nmhSetCmn(IssAuditLogStatus, 10, IssAuditLogStatusSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssAuditLogStatus)
#define nmhSetIssAuditLogFileName(pSetValIssAuditLogFileName) \
 nmhSetCmn(IssAuditLogFileName, 10, IssAuditLogFileNameSet, IssLock, IssUnLock, 0, 0, 0, "%s", pSetValIssAuditLogFileName)
#define nmhSetIssAuditLogFileSize(u4SetValIssAuditLogFileSize) \
 nmhSetCmn(IssAuditLogFileSize, 10, IssAuditLogFileSizeSet, IssLock, IssUnLock, 0, 0, 0, "%u", u4SetValIssAuditLogFileSize)
#define nmhSetIssAuditLogSizeThreshold(u4SetValIssAuditLogSizeThreshold) \
 nmhSetCmn(IssAuditLogSizeThreshold, 10, IssAuditLogSizeThresholdSet, IssLock, IssUnLock, 0, 0, 0, "%u", u4SetValIssAuditLogSizeThreshold)
#define nmhSetIssAuditLogReset(i4SetValIssAuditLogReset) \
 nmhSetCmn(IssAuditLogReset, 10, IssAuditLogResetSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssAuditLogReset)
#define nmhSetIssAuditLogRemoteIpAddr(u4SetValIssAuditLogRemoteIpAddr) \
 nmhSetCmn(IssAuditLogRemoteIpAddr, 10, IssAuditLogRemoteIpAddrSet, IssLock, IssUnLock, 0, 0, 0, "%p", u4SetValIssAuditLogRemoteIpAddr)
#define nmhSetIssAuditLogInitiateTransfer(i4SetValIssAuditLogInitiateTransfer) \
 nmhSetCmn(IssAuditLogInitiateTransfer, 10, IssAuditLogInitiateTransferSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssAuditLogInitiateTransfer)
#define nmhSetIssAuditTransferFileName(pSetValIssAuditTransferFileName) \
 nmhSetCmn(IssAuditTransferFileName, 10, IssAuditTransferFileNameSet, IssLock, IssUnLock, 0, 0, 0, "%s", pSetValIssAuditTransferFileName)
#define nmhSetIssConfigDefaultValueSaveOption(i4SetValIssConfigDefaultValueSaveOption)  \
    nmhSetCmn(IssConfigDefaultValueSaveOption, 10, IssConfigDefaultValueSaveOptionSet, NULL, NULL, 0, 0, 0, "%i", i4SetValIssConfigDefaultValueSaveOption)
#define nmhSetIssConfigSaveIpAddrType(i4SetValIssConfigSaveIpAddrType) \
 nmhSetCmn(IssConfigSaveIpAddrType, 10, IssConfigSaveIpAddrTypeSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssConfigSaveIpAddrType)
#define nmhSetIssConfigSaveIpvxAddr(pSetValIssConfigSaveIpvxAddr) \
 nmhSetCmn(IssConfigSaveIpvxAddr, 10, IssConfigSaveIpvxAddrSet, IssLock, IssUnLock, 0, 0, 0, "%s", pSetValIssConfigSaveIpvxAddr)
#define nmhSetIssConfigRestoreIpAddrType(i4SetValIssConfigRestoreIpAddrType) \
 nmhSetCmn(IssConfigRestoreIpAddrType, 10, IssConfigRestoreIpAddrTypeSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssConfigRestoreIpAddrType)
#define nmhSetIssConfigRestoreIpvxAddr(pSetValIssConfigRestoreIpvxAddr) \
 nmhSetCmn(IssConfigRestoreIpvxAddr, 10, IssConfigRestoreIpvxAddrSet, IssLock, IssUnLock, 0, 0, 0, "%s", pSetValIssConfigRestoreIpvxAddr)
#define nmhSetIssDlImageFromIpAddrType(i4SetValIssDlImageFromIpAddrType) \
 nmhSetCmn(IssDlImageFromIpAddrType, 10, IssDlImageFromIpAddrTypeSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssDlImageFromIpAddrType)
#define nmhSetIssDlImageFromIpvx(pSetValIssDlImageFromIpvx) \
 nmhSetCmn(IssDlImageFromIpvx, 10, IssDlImageFromIpvxSet, IssLock, IssUnLock, 0, 0, 0, "%s", pSetValIssDlImageFromIpvx)
#define nmhSetIssUploadLogFileToIpAddrType(i4SetValIssUploadLogFileToIpAddrType) \
 nmhSetCmn(IssUploadLogFileToIpAddrType, 10, IssUploadLogFileToIpAddrTypeSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssUploadLogFileToIpAddrType)
#define nmhSetIssUploadLogFileToIpvx(pSetValIssUploadLogFileToIpvx) \
 nmhSetCmn(IssUploadLogFileToIpvx, 10, IssUploadLogFileToIpvxSet, IssLock, IssUnLock, 0, 0, 0, "%s", pSetValIssUploadLogFileToIpvx)
#define nmhSetIssAuditLogRemoteIpAddrType(i4SetValIssAuditLogRemoteIpAddrType) \
 nmhSetCmn(IssAuditLogRemoteIpAddrType, 10, IssAuditLogRemoteIpAddrTypeSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssAuditLogRemoteIpAddrType)
#define nmhSetIssAuditLogRemoteIpvxAddr(pSetValIssAuditLogRemoteIpvxAddr) \
 nmhSetCmn(IssAuditLogRemoteIpvxAddr, 10, IssAuditLogRemoteIpvxAddrSet, IssLock, IssUnLock, 0, 0, 0, "%s", pSetValIssAuditLogRemoteIpvxAddr)
#define nmhSetIssSystemTimerSpeed(u4SetValIssSystemTimerSpeed) \
 nmhSetCmn(IssSystemTimerSpeed, 10, IssSystemTimerSpeedSet, NULL, NULL, 0, 0, 0, "%u", u4SetValIssSystemTimerSpeed)
#define nmhSetIssMacLearnRateLimit(i4SetValIssMacLearnRateLimit) \
 nmhSetCmnWithLock(IssMacLearnRateLimit, 10, IssMacLearnRateLimitSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssMacLearnRateLimit)
#define nmhSetIssMacLearnRateLimitInterval(u4SetValIssMacLearnRateLimitInterval) \
 nmhSetCmnWithLock(IssMacLearnRateLimitInterval, 10, IssMacLearnRateLimitIntervalSet, IssLock, IssUnLock, 0, 0, 0, "%u", u4SetValIssMacLearnRateLimitInterval)

#define nmhSetIssLoginAttempts(i4SetValIssLoginAttempts) \
 nmhSetCmnWithLock(IssLoginAttempts, 10, IssLoginAttemptsSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssLoginAttempts)
#define nmhSetIssLoginLockTime(i4SetValIssLoginLockTime) \
 nmhSetCmnWithLock(IssLoginLockTime, 10, IssLoginLockTimeSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssLoginLockTime)
#define nmhSetIssTelnetStatus(i4SetValIssTelnetStatus) \
 nmhSetCmnWithLock(IssTelnetStatus, 10, IssTelnetStatusSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssTelnetStatus)
#define nmhSetIssTelnetClientStatus(i4SetValIssTelnetClientStatus) \
        nmhSetCmnWithLock(IssTelnetClientStatus, 10, IssTelnetClientStatusSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssTelnetClientStatus)
#define nmhSetIssSshClientStatus(i4SetValIssSshClientStatus) \
        nmhSetCmnWithLock(IssSshClientStatus, 10, IssSshClientStatusSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssSshClientStatus)
#define nmhSetIssWebSessionTimeOut(i4SetValIssWebSessionTimeOut)        \
        nmhSetCmnWithLock(IssWebSessionTimeOut, 10, IssWebSessionTimeOutSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssWebSessionTimeOut)
#define nmhSetIssWebSessionMaxUsers(i4SetValIssWebSessionMaxUsers)      \
        nmhSetCmnWithLock(IssWebSessionMaxUsers, 10, IssWebSessionMaxUsersSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssWebSessionMaxUsers)
#define nmhSetIssHeartBeatMode(i4SetValIssHeartBeatMode)        \
        nmhSetCmnWithLock(IssHeartBeatMode, 10, IssHeartBeatModeSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssHeartBeatMode)
#define nmhSetIssRmRType(i4SetValIssRmRType)    \
        nmhSetCmnWithLock(IssRmRType, 10, IssRmRTypeSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssRmRType)
#define nmhSetIssRmDType(i4SetValIssRmDType)    \
        nmhSetCmnWithLock(IssRmDType, 10, IssRmDTypeSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssRmDType)
#define nmhSetIssRmStackingInterfaceType(i4SetValIssRmStackingInterfaceType) \
 nmhSetCmn(IssRmStackingInterfaceType, 10, IssRmStackingInterfaceTypeSet, NULL, NULL, 0, 0, 0, "%i", i4SetValIssRmStackingInterfaceType)
#define nmhSetIssRestoreType(i4SetValIssRestoreType)    \
    nmhSetCmn(IssRestoreType, 10, IssRestoreTypeSet, NULL, NULL, 0, 0, 0, "%i", i4SetValIssRestoreType)
#define nmhSetIssSwitchModeType(i4SetValIssSwitchModeType) \
    nmhSetCmnWithLock(IssSwitchModeType,10, IssSwitchModeTypeSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssSwitchModeType)
#define nmhSetIssCpuMirrorType(i4SetValIssCpuMirrorType) \
    nmhSetCmn(IssCpuMirrorType, 10, IssCpuMirrorTypeSet, IssLock, IssUnLock, 0, 0, 0, "%i",i4SetValIssCpuMirrorType)
#define nmhSetIssCpuMirrorToPort(u4SetValIssCpuMirrorToPort) \
    nmhSetCmn(IssCpuMirrorToPort, 10, IssCpuMirrorToPortSet, IssLock, IssUnLock, 0, 0, 0, "%i",u4SetValIssCpuMirrorToPort)
#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IssConfigCtrlIndex[12];
extern UINT4 IssConfigCtrlEgressStatus[12];
extern UINT4 IssConfigCtrlStatsCollection[12];
extern UINT4 IssConfigCtrlStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIssConfigCtrlEgressStatus(i4IssConfigCtrlIndex ,i4SetValIssConfigCtrlEgressStatus) \
 nmhSetCmn(IssConfigCtrlEgressStatus, 12, IssConfigCtrlEgressStatusSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssConfigCtrlIndex ,i4SetValIssConfigCtrlEgressStatus)
#define nmhSetIssConfigCtrlStatsCollection(i4IssConfigCtrlIndex ,i4SetValIssConfigCtrlStatsCollection) \
 nmhSetCmn(IssConfigCtrlStatsCollection, 12, IssConfigCtrlStatsCollectionSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssConfigCtrlIndex ,i4SetValIssConfigCtrlStatsCollection)
#define nmhSetIssConfigCtrlStatus(i4IssConfigCtrlIndex ,i4SetValIssConfigCtrlStatus) \
 nmhSetCmn(IssConfigCtrlStatus, 12, IssConfigCtrlStatusSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssConfigCtrlIndex ,i4SetValIssConfigCtrlStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IssPortCtrlIndex[12];
extern UINT4 IssPortCtrlMode[12];
extern UINT4 IssPortCtrlDuplex[12];
extern UINT4 IssPortCtrlSpeed[12];
extern UINT4 IssPortCtrlFlowControl[12];
extern UINT4 IssPortCtrlRenegotiate[12];
extern UINT4 IssPortCtrlMaxMacAddr[12];
extern UINT4 IssPortCtrlMaxMacAction[12];
extern UINT4 IssPortHOLBlockPrevention[12];
extern UINT4 IssPortCpuControlledLearning[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIssPortCtrlMode(i4IssPortCtrlIndex ,i4SetValIssPortCtrlMode) \
 nmhSetCmn(IssPortCtrlMode, 12, IssPortCtrlModeSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssPortCtrlIndex ,i4SetValIssPortCtrlMode)
#define nmhSetIssPortCtrlDuplex(i4IssPortCtrlIndex ,i4SetValIssPortCtrlDuplex) \
 nmhSetCmn(IssPortCtrlDuplex, 12, IssPortCtrlDuplexSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssPortCtrlIndex ,i4SetValIssPortCtrlDuplex)
#define nmhSetIssPortCtrlSpeed(i4IssPortCtrlIndex ,i4SetValIssPortCtrlSpeed) \
 nmhSetCmn(IssPortCtrlSpeed, 12, IssPortCtrlSpeedSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssPortCtrlIndex ,i4SetValIssPortCtrlSpeed)
#define nmhSetIssPortCtrlFlowControl(i4IssPortCtrlIndex ,i4SetValIssPortCtrlFlowControl) \
 nmhSetCmn(IssPortCtrlFlowControl, 12, IssPortCtrlFlowControlSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssPortCtrlIndex ,i4SetValIssPortCtrlFlowControl)
#define nmhSetIssPortCtrlRenegotiate(i4IssPortCtrlIndex ,i4SetValIssPortCtrlRenegotiate) \
 nmhSetCmn(IssPortCtrlRenegotiate, 12, IssPortCtrlRenegotiateSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssPortCtrlIndex ,i4SetValIssPortCtrlRenegotiate)
#define nmhSetIssPortCtrlMaxMacAddr(i4IssPortCtrlIndex ,i4SetValIssPortCtrlMaxMacAddr) \
 nmhSetCmn(IssPortCtrlMaxMacAddr, 12, IssPortCtrlMaxMacAddrSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssPortCtrlIndex ,i4SetValIssPortCtrlMaxMacAddr)
#define nmhSetIssPortCtrlMaxMacAction(i4IssPortCtrlIndex ,i4SetValIssPortCtrlMaxMacAction) \
 nmhSetCmn(IssPortCtrlMaxMacAction, 12, IssPortCtrlMaxMacActionSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssPortCtrlIndex ,i4SetValIssPortCtrlMaxMacAction)
#define nmhSetIssPortHOLBlockPrevention(i4IssPortCtrlIndex ,i4SetValIssPortHOLBlockPrevention) \
 nmhSetCmn(IssPortHOLBlockPrevention, 12, IssPortHOLBlockPreventionSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssPortCtrlIndex ,i4SetValIssPortHOLBlockPrevention)
#define nmhSetIssPortCpuControlledLearning(i4IssPortCtrlIndex ,i4SetValIssPortCpuControlledLearning) \
 nmhSetCmn(IssPortCpuControlledLearning, 12, IssPortCpuControlledLearningSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssPortCtrlIndex ,i4SetValIssPortCpuControlledLearning)
#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IssPortIsolationIngressPort[12];
extern UINT4 IssPortIsolationInVlanId[12];
extern UINT4 IssPortIsolationEgressPort[12];
extern UINT4 IssPortIsolationRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIssPortIsolationRowStatus(i4IssPortIsolationIngressPort , i4IssPortIsolationInVlanId , i4IssPortIsolationEgressPort ,i4SetValIssPortIsolationRowStatus) \
 nmhSetCmn(IssPortIsolationRowStatus, 12, IssPortIsolationRowStatusSet, IssLock, IssUnLock, 0, 1, 3, "%i %i %i %i", i4IssPortIsolationIngressPort , i4IssPortIsolationInVlanId , i4IssPortIsolationEgressPort ,i4SetValIssPortIsolationRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IssMirrorStatus[10];
extern UINT4 IssMirrorToPort[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIssMirrorStatus(i4SetValIssMirrorStatus) \
 nmhSetCmn(IssMirrorStatus, 10, IssMirrorStatusSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssMirrorStatus)
#define nmhSetIssMirrorToPort(i4SetValIssMirrorToPort) \
 nmhSetCmn(IssMirrorToPort, 10, IssMirrorToPortSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssMirrorToPort)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IssMirrorCtrlIndex[12];
extern UINT4 IssMirrorCtrlIngressMirroring[12];
extern UINT4 IssMirrorCtrlEgressMirroring[12];
extern UINT4 IssMirrorCtrlStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIssMirrorCtrlIngressMirroring(i4IssMirrorCtrlIndex ,i4SetValIssMirrorCtrlIngressMirroring) \
 nmhSetCmn(IssMirrorCtrlIngressMirroring, 12, IssMirrorCtrlIngressMirroringSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssMirrorCtrlIndex ,i4SetValIssMirrorCtrlIngressMirroring)
#define nmhSetIssMirrorCtrlEgressMirroring(i4IssMirrorCtrlIndex ,i4SetValIssMirrorCtrlEgressMirroring) \
 nmhSetCmn(IssMirrorCtrlEgressMirroring, 12, IssMirrorCtrlEgressMirroringSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssMirrorCtrlIndex ,i4SetValIssMirrorCtrlEgressMirroring)
#define nmhSetIssMirrorCtrlStatus(i4IssMirrorCtrlIndex ,i4SetValIssMirrorCtrlStatus) \
 nmhSetCmn(IssMirrorCtrlStatus, 12, IssMirrorCtrlStatusSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssMirrorCtrlIndex ,i4SetValIssMirrorCtrlStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IssMirrorCtrlExtnSessionIndex[12];
extern UINT4 IssMirrorCtrlExtnMirrType[12];
extern UINT4 IssMirrorCtrlExtnRSpanStatus[12];
extern UINT4 IssMirrorCtrlExtnRSpanVlanId[12];
extern UINT4 IssMirrorCtrlExtnRSpanContext[12];
extern UINT4 IssMirrorCtrlExtnStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIssMirrorCtrlExtnMirrType(i4IssMirrorCtrlExtnSessionIndex ,i4SetValIssMirrorCtrlExtnMirrType) \
 nmhSetCmn(IssMirrorCtrlExtnMirrType, 12, IssMirrorCtrlExtnMirrTypeSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssMirrorCtrlExtnSessionIndex ,i4SetValIssMirrorCtrlExtnMirrType)
#define nmhSetIssMirrorCtrlExtnRSpanStatus(i4IssMirrorCtrlExtnSessionIndex ,i4SetValIssMirrorCtrlExtnRSpanStatus) \
 nmhSetCmn(IssMirrorCtrlExtnRSpanStatus, 12, IssMirrorCtrlExtnRSpanStatusSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssMirrorCtrlExtnSessionIndex ,i4SetValIssMirrorCtrlExtnRSpanStatus)
#define nmhSetIssMirrorCtrlExtnRSpanVlanId(i4IssMirrorCtrlExtnSessionIndex ,i4SetValIssMirrorCtrlExtnRSpanVlanId) \
 nmhSetCmn(IssMirrorCtrlExtnRSpanVlanId, 12, IssMirrorCtrlExtnRSpanVlanIdSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssMirrorCtrlExtnSessionIndex ,i4SetValIssMirrorCtrlExtnRSpanVlanId)
#define nmhSetIssMirrorCtrlExtnRSpanContext(i4IssMirrorCtrlExtnSessionIndex ,i4SetValIssMirrorCtrlExtnRSpanContext) \
 nmhSetCmn(IssMirrorCtrlExtnRSpanContext, 12, IssMirrorCtrlExtnRSpanContextSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssMirrorCtrlExtnSessionIndex ,i4SetValIssMirrorCtrlExtnRSpanContext)
#define nmhSetIssMirrorCtrlExtnStatus(i4IssMirrorCtrlExtnSessionIndex ,i4SetValIssMirrorCtrlExtnStatus) \
 nmhSetCmn(IssMirrorCtrlExtnStatus, 12, IssMirrorCtrlExtnStatusSet, IssLock, IssUnLock, 0, 1, 1, "%i %i", i4IssMirrorCtrlExtnSessionIndex ,i4SetValIssMirrorCtrlExtnStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IssMirrorCtrlExtnSrcId[12];
extern UINT4 IssMirrorCtrlExtnSrcCfg[12];
extern UINT4 IssMirrorCtrlExtnSrcMode[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIssMirrorCtrlExtnSrcCfg(i4IssMirrorCtrlExtnSessionIndex , i4IssMirrorCtrlExtnSrcId ,i4SetValIssMirrorCtrlExtnSrcCfg) \
 nmhSetCmn(IssMirrorCtrlExtnSrcCfg, 12, IssMirrorCtrlExtnSrcCfgSet, IssLock, IssUnLock, 0, 0, 2, "%i %i %i", i4IssMirrorCtrlExtnSessionIndex , i4IssMirrorCtrlExtnSrcId ,i4SetValIssMirrorCtrlExtnSrcCfg)
#define nmhSetIssMirrorCtrlExtnSrcMode(i4IssMirrorCtrlExtnSessionIndex , i4IssMirrorCtrlExtnSrcId ,i4SetValIssMirrorCtrlExtnSrcMode) \
 nmhSetCmn(IssMirrorCtrlExtnSrcMode, 12, IssMirrorCtrlExtnSrcModeSet, IssLock, IssUnLock, 0, 0, 2, "%i %i %i", i4IssMirrorCtrlExtnSessionIndex , i4IssMirrorCtrlExtnSrcId ,i4SetValIssMirrorCtrlExtnSrcMode)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IssMirrorCtrlExtnSrcVlanContext[12];
extern UINT4 IssMirrorCtrlExtnSrcVlanId[12];
extern UINT4 IssMirrorCtrlExtnSrcVlanCfg[12];
extern UINT4 IssMirrorCtrlExtnSrcVlanMode[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIssMirrorCtrlExtnSrcVlanCfg(i4IssMirrorCtrlExtnSessionIndex , i4IssMirrorCtrlExtnSrcVlanContext , i4IssMirrorCtrlExtnSrcVlanId ,i4SetValIssMirrorCtrlExtnSrcVlanCfg) \
 nmhSetCmn(IssMirrorCtrlExtnSrcVlanCfg, 12, IssMirrorCtrlExtnSrcVlanCfgSet, IssLock, IssUnLock, 0, 0, 3, "%i %i %i %i", i4IssMirrorCtrlExtnSessionIndex , i4IssMirrorCtrlExtnSrcVlanContext , i4IssMirrorCtrlExtnSrcVlanId ,i4SetValIssMirrorCtrlExtnSrcVlanCfg)
#define nmhSetIssMirrorCtrlExtnSrcVlanMode(i4IssMirrorCtrlExtnSessionIndex , i4IssMirrorCtrlExtnSrcVlanContext , i4IssMirrorCtrlExtnSrcVlanId ,i4SetValIssMirrorCtrlExtnSrcVlanMode) \
 nmhSetCmn(IssMirrorCtrlExtnSrcVlanMode, 12, IssMirrorCtrlExtnSrcVlanModeSet, IssLock, IssUnLock, 0, 0, 3, "%i %i %i %i", i4IssMirrorCtrlExtnSessionIndex , i4IssMirrorCtrlExtnSrcVlanContext , i4IssMirrorCtrlExtnSrcVlanId ,i4SetValIssMirrorCtrlExtnSrcVlanMode)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IssMirrorCtrlExtnDestination[12];
extern UINT4 IssMirrorCtrlExtnDestCfg[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIssMirrorCtrlExtnDestCfg(i4IssMirrorCtrlExtnSessionIndex , i4IssMirrorCtrlExtnDestination ,i4SetValIssMirrorCtrlExtnDestCfg) \
 nmhSetCmn(IssMirrorCtrlExtnDestCfg, 12, IssMirrorCtrlExtnDestCfgSet, IssLock, IssUnLock, 0, 0, 2, "%i %i %i", i4IssMirrorCtrlExtnSessionIndex , i4IssMirrorCtrlExtnDestination ,i4SetValIssMirrorCtrlExtnDestCfg)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IssIpAuthMgrIpAddr[12];
extern UINT4 IssIpAuthMgrIpMask[12];
extern UINT4 IssIpAuthMgrPortList[12];
extern UINT4 IssIpAuthMgrVlanList[12];
extern UINT4 IssIpAuthMgrOOBPort[12];
extern UINT4 IssIpAuthMgrAllowedServices[12];
extern UINT4 IssIpAuthMgrRowStatus[12];
extern UINT4 IssRateCtrlIndex[12];
extern UINT4 IssRateCtrlDLFLimitValue[12];
extern UINT4 IssRateCtrlBCASTLimitValue[12];
extern UINT4 IssRateCtrlMCASTLimitValue[12];
extern UINT4 IssRateCtrlPortRateLimit[12];
extern UINT4 IssRateCtrlPortBurstSize[12];
extern UINT4 IssL2FilterNo[12];
extern UINT4 IssL2FilterPriority[12];
extern UINT4 IssL2FilterEtherType[12];
extern UINT4 IssL2FilterProtocolType[12];
extern UINT4 IssL2FilterDstMacAddr[12];
extern UINT4 IssL2FilterSrcMacAddr[12];
extern UINT4 IssL2FilterVlanId[12];
extern UINT4 IssL2FilterInPortList[12];
extern UINT4 IssL2FilterAction[12];
extern UINT4 IssL2FilterStatus[12];
extern UINT4 IssL3FilterNo[12];
extern UINT4 IssL3FilterPriority[12];
extern UINT4 IssL3FilterProtocol[12];
extern UINT4 IssL3FilterMessageType[12];
extern UINT4 IssL3FilterMessageCode[12];
extern UINT4 IssL3FilterDstIpAddr[12];
extern UINT4 IssL3FilterSrcIpAddr[12];
extern UINT4 IssL3FilterDstIpAddrMask[12];
extern UINT4 IssL3FilterSrcIpAddrMask[12];
extern UINT4 IssL3FilterMinDstProtPort[12];
extern UINT4 IssL3FilterMaxDstProtPort[12];
extern UINT4 IssL3FilterMinSrcProtPort[12];
extern UINT4 IssL3FilterMaxSrcProtPort[12];
extern UINT4 IssL3FilterInPortList[12];
extern UINT4 IssL3FilterOutPortList[12];
extern UINT4 IssL3FilterAckBit[12];
extern UINT4 IssL3FilterRstBit[12];
extern UINT4 IssL3FilterTos[12];
extern UINT4 IssL3FilterDscp[12];
extern UINT4 IssL3FilterDirection[12];
extern UINT4 IssL3FilterAction[12];
extern UINT4 IssL3FilterStatus[12];


#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIssIpAuthMgrPortList(u4IssIpAuthMgrIpAddr , u4IssIpAuthMgrIpMask ,pSetValIssIpAuthMgrPortList) \
 nmhSetCmn(IssIpAuthMgrPortList, 12, IssIpAuthMgrPortListSet, IssLock, IssUnLock, 0, 0, 2, "%p %p %s", u4IssIpAuthMgrIpAddr , u4IssIpAuthMgrIpMask ,pSetValIssIpAuthMgrPortList)
#define nmhSetIssIpAuthMgrVlanList(u4IssIpAuthMgrIpAddr , u4IssIpAuthMgrIpMask ,pSetValIssIpAuthMgrVlanList) \
 nmhSetCmn(IssIpAuthMgrVlanList, 12, IssIpAuthMgrVlanListSet, IssLock, IssUnLock, 0, 0, 2, "%p %p %s", u4IssIpAuthMgrIpAddr , u4IssIpAuthMgrIpMask ,pSetValIssIpAuthMgrVlanList)
#define nmhSetIssIpAuthMgrOOBPort(u4IssIpAuthMgrIpAddr , u4IssIpAuthMgrIpMask ,i4SetValIssIpAuthMgrOOBPort) \
 nmhSetCmn(IssIpAuthMgrOOBPort, 12, IssIpAuthMgrOOBPortSet, IssLock, IssUnLock, 0, 0, 2, "%p %p %i", u4IssIpAuthMgrIpAddr , u4IssIpAuthMgrIpMask ,i4SetValIssIpAuthMgrOOBPort)
#define nmhSetIssIpAuthMgrAllowedServices(u4IssIpAuthMgrIpAddr , u4IssIpAuthMgrIpMask ,i4SetValIssIpAuthMgrAllowedServices) \
 nmhSetCmn(IssIpAuthMgrAllowedServices, 12, IssIpAuthMgrAllowedServicesSet, IssLock, IssUnLock, 0, 0, 2, "%p %p %i", u4IssIpAuthMgrIpAddr , u4IssIpAuthMgrIpMask ,i4SetValIssIpAuthMgrAllowedServices)
#define nmhSetIssIpAuthMgrRowStatus(u4IssIpAuthMgrIpAddr , u4IssIpAuthMgrIpMask ,i4SetValIssIpAuthMgrRowStatus) \
 nmhSetCmn(IssIpAuthMgrRowStatus, 12, IssIpAuthMgrRowStatusSet, IssLock, IssUnLock, 0, 1, 2, "%p %p %i", u4IssIpAuthMgrIpAddr , u4IssIpAuthMgrIpMask ,i4SetValIssIpAuthMgrRowStatus)
#define nmhSetIssRateCtrlDLFLimitValue(i4IssRateCtrlIndex ,i4SetValIssRateCtrlDLFLimitValue)    \
    nmhSetCmn(IssRateCtrlDLFLimitValue, 12, IssRateCtrlDLFLimitValueSet, IssLock ,IssUnLock, 0, 0, 1, "%i %i", i4IssRateCtrlIndex ,i4SetValIssRateCtrlDLFLimitValue)
#define nmhSetIssRateCtrlBCASTLimitValue(i4IssRateCtrlIndex ,i4SetValIssRateCtrlBCASTLimitValue)    \
    nmhSetCmn(IssRateCtrlBCASTLimitValue, 12, IssRateCtrlBCASTLimitValueSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssRateCtrlIndex ,i4SetValIssRateCtrlBCASTLimitValue)
#define nmhSetIssRateCtrlMCASTLimitValue(i4IssRateCtrlIndex ,i4SetValIssRateCtrlMCASTLimitValue)    \
    nmhSetCmn(IssRateCtrlMCASTLimitValue, 12, IssRateCtrlMCASTLimitValueSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssRateCtrlIndex ,i4SetValIssRateCtrlMCASTLimitValue)
#define nmhSetIssRateCtrlPortRateLimit(i4IssRateCtrlIndex ,i4SetValIssRateCtrlPortRateLimit)    \
    nmhSetCmn(IssRateCtrlPortRateLimit, 12, IssRateCtrlPortRateLimitSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssRateCtrlIndex ,i4SetValIssRateCtrlPortRateLimit)
#define nmhSetIssRateCtrlPortBurstSize(i4IssRateCtrlIndex ,i4SetValIssRateCtrlPortBurstSize)    \
    nmhSetCmn(IssRateCtrlPortBurstSize, 12, IssRateCtrlPortBurstSizeSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssRateCtrlIndex ,i4SetValIssRateCtrlPortBurstSize)
#define nmhSetIssL2FilterPriority(i4IssL2FilterNo ,i4SetValIssL2FilterPriority)\
    nmhSetCmn(IssL2FilterPriority, 12, IssL2FilterPrioritySet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssL2FilterNo ,i4SetValIssL2FilterPriority)
#define nmhSetIssL2FilterEtherType(i4IssL2FilterNo ,i4SetValIssL2FilterEtherTypee)   \
    nmhSetCmn(IssL2FilterEtherType, 12, IssL2FilterEtherTypeSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssL2FilterNo ,i4SetValIssL2FilterEtherType)
#define nmhSetIssL2FilterProtocolType(i4IssL2FilterNo ,u4SetValIssL2FilterProtocolType) \
    nmhSetCmn(IssL2FilterProtocolType, 12, IssL2FilterProtocolTypeSet,IssLock,IssUnLock, 0, 0, 1, "%i %u", i4IssL2FilterNo ,u4SetValIssL2FilterProtocolType)
#define nmhSetIssL2FilterDstMacAddr(i4IssL2FilterNo ,SetValIssL2FilterDstMacAddr)   \
    nmhSetCmn(IssL2FilterDstMacAddr, 12, IssL2FilterDstMacAddrSet, IssLock, IssUnLock, 0, 0, 1, "%i %m", i4IssL2FilterNo ,SetValIssL2FilterDstMacAddr)
#define nmhSetIssL2FilterSrcMacAddr(i4IssL2FilterNo ,SetValIssL2FilterSrcMacAddr)   \
    nmhSetCmn(IssL2FilterSrcMacAddr, 12, IssL2FilterSrcMacAddrSet, IssLock, IssUnLock, 0, 0, 1, "%i %m", i4IssL2FilterNo ,SetValIssL2FilterSrcMacAddr)
#define nmhSetIssL2FilterVlanId(i4IssL2FilterNo ,i4SetValIssL2FilterVlanId) \
    nmhSetCmn(IssL2FilterVlanId, 12, IssL2FilterVlanIdSet, IssLock, IssUnLock, 00, 0, 1, "%i %i", i4IssL2FilterNo ,i4SetValIssL2FilterVlanId)
#define nmhSetIssL2FilterInPortList(i4IssL2FilterNo ,pSetValIssL2FilterInPortList)  \
    nmhSetCmn(IssL2FilterInPortList, 12, IssL2FilterInPortListSet, IssLock, IssUnLock, 0, 0, 1, "%i %s", i4IssL2FilterNo ,pSetValIssL2FilterInPortList)
#define nmhSetIssL2FilterAction(i4IssL2FilterNo ,i4SetValIssL2FilterAction) \
    nmhSetCmn(IssL2FilterAction, 12, IssL2FilterActionSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssL2FilterNo ,i4SetValIssL2FilterAction)
#define nmhSetIssL2FilterStatus(i4IssL2FilterNo ,i4SetValIssL2FilterStatus) \
    nmhSetCmn(IssL2FilterStatus, 12, IssL2FilterStatusSet, IssLock, IssUnLock, 0, 1, 1, "%i %i", i4IssL2FilterNo ,i4SetValIssL2FilterStatus)
#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IssL4SwitchingFilterNo[12];
extern UINT4 IssL4SwitchingProtocol[12];
extern UINT4 IssL4SwitchingPortNo[12];
extern UINT4 IssL4SwitchingCopyToPort[12];
extern UINT4 IssL4SwitchingFilterStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIssL4SwitchingProtocol(i4IssL4SwitchingFilterNo ,i4SetValIssL4SwitchingProtocol) \
 nmhSetCmn(IssL4SwitchingProtocol, 12, IssL4SwitchingProtocolSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssL4SwitchingFilterNo ,i4SetValIssL4SwitchingProtocol)
#define nmhSetIssL4SwitchingPortNo(i4IssL4SwitchingFilterNo ,u4SetValIssL4SwitchingPortNo) \
etIssL3FilterPriority(i4IssL3FilterNo ,i4SetValIssL3FilterPriority)\
    nmhSetCmn(IssL3FilterPriority, 12, IssL3FilterPrioritySet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssL3FilterNo ,i4SetValIssL3FilterPriority)
#define nmhSetIssL3FilterProtocol(i4IssL3FilterNo ,i4SetValIssL3FilterProtocol)\
    nmhSetCmn(IssL3FilterProtocol, 12, IssL3FilterProtocolSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssL3FilterNo ,i4SetValIssL3FilterProtocol)
#define nmhSetIssL3FilterMessageType(i4IssL3FilterNo ,i4SetValIssL3FilterMessageeType)   \
    nmhSetCmn(IssL3FilterMessageType, 12, IssL3FilterMessageTypeSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssL3FilterNo ,i4SetValIssL3FilterMessageType)
#define nmhSetIssL3FilterMessageCode(i4IssL3FilterNo ,i4SetValIssL3FilterMessageCode)   \
    nmhSetCmn(IssL3FilterMessageCode, 12, IssL3FilterMessageCodeSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssL3FilterNo ,i4SetValIssL3FilterMessageCode)
#define nmhSetIssL3FilterDstIpAddr(i4IssL3FilterNo ,u4SetValIssL3FilterDstIpAddr)   \
    nmhSetCmn(IssL3FilterDstIpAddr, 12, IssL3FilterDstIpAddrSet, IssLock, IssUnLock, 0, 0, 1, "%i %p", i4IssL3FilterNo ,u4SetValIssL3FilterDstIpAddr)
#define nmhSetIssL3FilterSrcIpAddr(i4IssL3FilterNo ,u4SetValIssL3FilterSrcIpAddr)   \
    nmhSetCmn(IssL3FilterSrcIpAddr, 12, IssL3FilterSrcIpAddrSet, IssLock, IssUnLock, 0, 0, 1, "%i %p", i4IssL3FilterNo ,u4SetValIssL3FilterSrcIpAddr)
#define nmhSetIssL3FilterDstIpAddrMask(i4IssL3FilterNo ,u4SetValIssL3FilterDstIpAddrMask)   \
    nmhSetCmn(IssL3FilterDstIpAddrMask, 12, IssL3FilterDstIpAddrMaskSet, IssLock, IssUnLock, 0, 0, 1, "%i %p", i4IssL3FilterNo ,u4SetValIssL3FilterDstIpAddrMask)
#define nmhSetIssL3FilterSrcIpAddrMask(i4IssL3FilterNo ,u4SetValIssL3FilterSrcIpAddrMask)   \
    nmhSetCmn(IssL3FilterSrcIpAddrMask, 12, IssL3FilterSrcIpAddrMaskSet, IssLock, IssUnLock, 0, 0, 1, "%i %p", i4IssL3FilterNo ,u4SetValIssL3FilterSrcIpAddrMask)
#define nmhSetIssL3FilterMinDstProtPort(i4IssL3FilterNo ,u4SetValIssL3FilterMinDstProtPort) \
    nmhSetCmn(IssL3FilterMinDstProtPort, 12, IssL3FilterMinDstProtPortSet, IssLock, IssUnLock, 0, 0, 1, "%i %u", i4IssL3FilterNo ,u4SetValIssL3FilterMinDstProtPort)
#define nmhSetIssL3FilterMaxDstProtPort(i4IssL3FilterNo ,u4SetValIssL3FilterMaxDstProtPort) \
    nmhSetCmn(IssL3FilterMaxDstProtPort, 12, IssL3FilterMaxDstProtPortSet, IssLock, IssUnLock, 0, 0, 1, "%i %u", i4IssL3FilterNo ,u4SetValIssL3FilterMaxDstProtPort)
#define nmhSetIssL3FilterMinSrcProtPort(i4IssL3FilterNo ,u4SetValIssL3FilterMinSrcProtPort) \
    nmhSetCmn(IssL3FilterMinSrcProtPort, 12, IssL3FilterMinSrcProtPortSet, IssLock, IssUnLock, 0, 0, 1, "%i %u", i4IssL3FilterNo ,u4SetValIssL3FilterMinSrcProtPort)
#define nmhSetIssL3FilterMaxSrcProtPort(i4IssL3FilterNo ,u4SetValIssL3FilterMaxSrcProtPort) \
    nmhSetCmn(IssL3FilterMaxSrcProtPort, 12, IssL3FilterMaxSrcProtPortSet, IssLock, IssUnLock, 0, 0, 1, "%i %u", i4IssL3FilterNo ,u4SetValIssL3FilterMaxSrcProtPort)
#define nmhSetIssL3FilterInPortList(i4IssL3FilterNo ,pSetValIssL3FilterInPortList)  \
    nmhSetCmn(IssL3FilterInPortList, 12, IssL3FilterInPortListSet, IssLock, IssUnLock, 0, 0, 1, "%i %s", i4IssL3FilterNo ,pSetValIssL3FilterInPortList)
#define nmhSetIssL3FilterOutPortList(i4IssL3FilterNo ,pSetValIssL3FilterOutPortList)    \
    nmhSetCmn(IssL3FilterOutPortList, 12, IssL3FilterOutPortListSet, IssLock, IssUnLock, 0, 0, 1, "%i %s", i4IssL3FilterNo ,pSetValIssL3FilterOutPortList)
#define nmhSetIssL3FilterAckBit(i4IssL3FilterNo ,i4SetValIssL3FilterAckBit) \
    nmhSetCmn(IssL3FilterAckBit, 12, IssL3FilterAckBitSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssL3FilterNo ,i4SetValIssL3FilterAckBit)
#define nmhSetIssL3FilterRstBit(i4IssL3FilterNo ,i4SetValIssL3FilterRstBit) \
    nmhSetCmn(IssL3FilterRstBit, 12, IssL3FilterRstBitSet, IssLock, IssUnLock, 0,0, 1, "%i %i", i4IssL3FilterNo ,i4SetValIssL3FilterRstBit)
#define nmhSetIssL3FilterTos(i4IssL3FilterNo ,i4SetValIssL3FilterTos)   \
    nmhSetCmn(IssL3FilterTos, 12, IssL3FilterTosSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssL3FilterNo ,i4SetValIssL3FilterTos)
#define nmhSetIssL3FilterDscp(i4IssL3FilterNo ,i4SetValIssL3FilterDscp) \
    nmhSetCmn(IssL3FilterDscp, 12, IssL3FilterDscpSet, IssLock, IssUnLock, 0, 0,1, "%i %i", i4IssL3FilterNo ,i4SetValIssL3FilterDscp)
#define nmhSetIssL3FilterDirection(i4IssL3FilterNo ,i4SetValIssL3FilterDirection)   \
    nmhSetCmn(IssL3FilterDirection, 12, IssL3FilterDirectionSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssL3FilterNo ,i4SetValIssL3FilterDirection)
#define nmhSetIssL3FilterAction(i4IssL3FilterNo ,i4SetValIssL3FilterAction) \
    nmhSetCmn(IssL3FilterAction, 12, IssL3FilterActionSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssL3FilterNo ,i4SetValIssL3FilterAction)
#define nmhSetIssL3FilterStatus(i4IssL3FilterNo ,i4SetValIssL3FilterStatus) \
    nmhSetCmn(IssL3FilterStatus, 12, IssL3FilterStatusSet, IssLock, IssUnLock, 0, 1, 1, "%i %i", i4IssL3FilterNo ,i4SetValIssL3FilterStatus)
#define nmhSetIssL4SwitchingCopyToPort(i4IssL4SwitchingFilterNo ,i4SetValIssL4SwitchingCopyToPort) \
 nmhSetCmn(IssL4SwitchingCopyToPort, 12, IssL4SwitchingCopyToPortSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssL4SwitchingFilterNo ,i4SetValIssL4SwitchingCopyToPort)
#define nmhSetIssL4SwitchingFilterStatus(i4IssL4SwitchingFilterNo ,i4SetValIssL4SwitchingFilterStatus) \
 nmhSetCmn(IssL4SwitchingFilterStatus, 12, IssL4SwitchingFilterStatusSet, IssLock, IssUnLock, 0, 1, 1, "%i %i", i4IssL4SwitchingFilterNo ,i4SetValIssL4SwitchingFilterStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IssModuleId[12];
extern UINT4 IssModuleSystemControl[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIssModuleSystemControl(i4IssModuleId ,i4SetValIssModuleSystemControl) \
 nmhSetCmn(IssModuleSystemControl, 12, IssModuleSystemControlSet, IssLock, IssUnLock, 0, 0, 1, "%i %i", i4IssModuleId ,i4SetValIssModuleSystemControl)

#endif
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IssAclProvisionMode[10];
extern UINT4 IssAclTriggerCommit[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIssAclProvisionMode(i4SetValIssAclProvisionMode)  \
    nmhSetCmnWithLock(IssAclProvisionMode, 10, IssAclProvisionModeSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssAclProvisionMode)
#define nmhSetIssAclTriggerCommit(i4SetValIssAclTriggerCommit)  \
        nmhSetCmnWithLock(IssAclTriggerCommit, 10, IssAclTriggerCommitSet, IssLock, IssUnLock, 0, 0, 0, "%i", i4SetValIssAclTriggerCommit)

#endif





/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IssHealthChkClearCtr[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIssHealthChkClearCtr(pSetValIssHealthChkClearCtr) \
 nmhSetCmn(IssHealthChkClearCtr, 10, IssHealthChkClearCtrSet, IssLock, IssUnLock, 0, 0, 0, "%s", pSetValIssHealthChkClearCtr)

#endif
