/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdsnpcli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 SnmpProxyName[11];
extern UINT4 SnmpProxyType[11];
extern UINT4 SnmpProxyContextEngineID[11];
extern UINT4 SnmpProxyContextName[11];
extern UINT4 SnmpProxyTargetParamsIn[11];
extern UINT4 SnmpProxySingleTargetOut[11];
extern UINT4 SnmpProxyMultipleTargetOut[11];
extern UINT4 SnmpProxyStorageType[11];
extern UINT4 SnmpProxyRowStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetSnmpProxyType(pSnmpProxyName ,i4SetValSnmpProxyType)	\
	nmhSetCmn(SnmpProxyType, 11, SnmpProxyTypeSet, NULL, NULL, 0, 0, 1, "%s %i", pSnmpProxyName ,i4SetValSnmpProxyType)
#define nmhSetSnmpProxyContextEngineID(pSnmpProxyName ,pSetValSnmpProxyContextEngineID)	\
	nmhSetCmn(SnmpProxyContextEngineID, 11, SnmpProxyContextEngineIDSet, NULL, NULL, 0, 0, 1, "%s %s", pSnmpProxyName ,pSetValSnmpProxyContextEngineID)
#define nmhSetSnmpProxyContextName(pSnmpProxyName ,pSetValSnmpProxyContextName)	\
	nmhSetCmn(SnmpProxyContextName, 11, SnmpProxyContextNameSet, NULL, NULL, 0, 0, 1, "%s %s", pSnmpProxyName ,pSetValSnmpProxyContextName)
#define nmhSetSnmpProxyTargetParamsIn(pSnmpProxyName ,pSetValSnmpProxyTargetParamsIn)	\
	nmhSetCmn(SnmpProxyTargetParamsIn, 11, SnmpProxyTargetParamsInSet, NULL, NULL, 0, 0, 1, "%s %s", pSnmpProxyName ,pSetValSnmpProxyTargetParamsIn)
#define nmhSetSnmpProxySingleTargetOut(pSnmpProxyName ,pSetValSnmpProxySingleTargetOut)	\
	nmhSetCmn(SnmpProxySingleTargetOut, 11, SnmpProxySingleTargetOutSet, NULL, NULL, 0, 0, 1, "%s %s", pSnmpProxyName ,pSetValSnmpProxySingleTargetOut)
#define nmhSetSnmpProxyMultipleTargetOut(pSnmpProxyName ,pSetValSnmpProxyMultipleTargetOut)	\
	nmhSetCmn(SnmpProxyMultipleTargetOut, 11, SnmpProxyMultipleTargetOutSet, NULL, NULL, 0, 0, 1, "%s %s", pSnmpProxyName ,pSetValSnmpProxyMultipleTargetOut)
#define nmhSetSnmpProxyStorageType(pSnmpProxyName ,i4SetValSnmpProxyStorageType)	\
	nmhSetCmn(SnmpProxyStorageType, 11, SnmpProxyStorageTypeSet, NULL, NULL, 0, 0, 1, "%s %i", pSnmpProxyName ,i4SetValSnmpProxyStorageType)
#define nmhSetSnmpProxyRowStatus(pSnmpProxyName ,i4SetValSnmpProxyRowStatus)	\
	nmhSetCmn(SnmpProxyRowStatus, 11, SnmpProxyRowStatusSet, NULL, NULL, 0, 1, 1, "%s %i", pSnmpProxyName ,i4SetValSnmpProxyRowStatus)

#endif
