

#ifndef __OSPFTECLI_H__
#define __OSPFTECLI_H__

#include "lr.h"
#include "cli.h"

/* Command Identifiers */
enum {
    OSPFTE_CLI_ADMIN_UP = 1,
    OSPFTE_CLI_ADMIN_DOWN,
    OSPFTE_CLI_DBG,
    OSPFTE_CLI_NO_DBG,
    OSPFTE_CLI_SHOW,
};

#define OSPFTE_CLI_MAX_ARGS 15
INT4 cli_process_ospfte_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);
INT4 OspfTeShowRunningConfig (tCliHandle);
VOID IssOspfteCliShowDebugging (tCliHandle);

#endif /* __OSPFTECLI_H__ */
