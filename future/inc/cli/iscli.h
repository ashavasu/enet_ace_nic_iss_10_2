/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: iscli.h,v 1.21 2017/09/11 13:44:06 siva Exp $
 *
 * Description: This file contains the data structure and macros 
 *              for ISIS CLI module 
 *
 *******************************************************************/
#ifndef __ISISCLI_H__
#define __ISISCLI_H__

#include "lr.h"
#include "cli.h"
#include "ip.h"
#include "isis.h"

enum {
    ISIS_CLI_CMD_CREATE_INST,
    ISIS_CLI_CMD_DEST_INST,
    ISIS_CLI_CMD_ADD_NET,
    ISIS_CLI_CMD_DEL_NET,
    ISIS_CLI_CMD_INST_TYPE,
    ISIS_CLI_CMD_ADD_SUMM_ADDR,
    ISIS_CLI_CMD_ADD_SUMM_IPV6_ADDR,
    ISIS_CLI_CMD_DEL_SUMM_ADDR,
    ISIS_CLI_CMD_DEL_SUMM_IPV6_ADDR,
    ISIS_CLI_CMD_ENABLE_ROUTE_LEAK,
    ISIS_CLI_CMD_DISABLE_ROUTE_LEAK,
    ISIS_CLI_CMD_ADD_IPRA,
    ISIS_CLI_CMD_ADD_IPV6_IPRA,
    ISIS_CLI_CMD_ADD_IPV6_IF,
    ISIS_CLI_CMD_DEL_IPV6_IF,
    ISIS_CLI_CMD_IPRA_METRIC,
    ISIS_CLI_CMD_IPRA_METRIC_TYPE,
    ISIS_CLI_CMD_DEL_IPRA,
    ISIS_CLI_CMD_SET_OL_BIT,
    ISIS_CLI_CMD_RESET_OL_BIT,
    ISIS_CLI_CMD_SET_AREA_PASSWD,
    ISIS_CLI_CMD_RESET_AREA_PASSWD,
    ISIS_CLI_CMD_SET_DOMAIN_PASSWD,
    ISIS_CLI_CMD_RESET_DOMAIN_PASSWD,
    ISIS_CLI_CMD_GR_SUPPORT,
    ISIS_CLI_CMD_T1_INTERVAL,
    ISIS_CLI_CMD_NO_T1_INTERVAL,
    ISIS_CLI_CMD_T1_RETRY,
    ISIS_CLI_CMD_NO_T1_RETRY,
    ISIS_CLI_CMD_T2_LEVEL1,
    ISIS_CLI_CMD_T2_LEVEL2,
    ISIS_CLI_CMD_T2_LEVEL12,
    ISIS_CLI_CMD_NO_T2_LEVEL1,
    ISIS_CLI_CMD_NO_T2_LEVEL2,
    ISIS_CLI_CMD_NO_T2_LEVEL12,
    ISIS_CLI_CMD_T3_MANUAL,
    ISIS_CLI_CMD_NO_T3_MANUAL,
    ISIS_CLI_CMD_RESTART_REASON,
    ISIS_CLI_CMD_HELPER_SUPPORT,
    ISIS_CLI_CMD_NO_HELPER_SUPPORT,
    ISIS_CLI_CMD_HELPER_GRACE_LIMIT,
    ISIS_CLI_CMD_NO_HELPER_GRACE_LIMIT,
    ISIS_CLI_CMD_GR_NO_SUPPORT,
    ISIS_CLI_CMD_CREATE_CKT,
    ISIS_CLI_CMD_DELETE_CKT,
    ISIS_CLI_CMD_SET_CKT_TYPE,
    ISIS_CLI_CMD_CKT_METRIC,
    ISIS_CLI_CMD_NO_CKT_METRIC,
    ISIS_CLI_CMD_HELLO_INT,
    ISIS_CLI_CMD_HELLO_MULTI,
    ISIS_CLI_CMD_CKT_ADD_PASSWD,
    ISIS_CLI_CMD_CKT_DEL_PASSWD,
    ISIS_CLI_CMD_LSP_INT,
    ISIS_CLI_CMD_RETR_INT,
    ISIS_CLI_CMD_CKT_PRIO,
    ISIS_CLI_CMD_CKT_CSNP_INT,
    ISIS_CLI_REDISTRIBUTE,
    ISIS_CLI_NO_REDISTRIBUTE,
    ISIS_CLI_CMD_SET_AUTH_MODE,
    ISIS_CLI_CMD_DEL_AUTH_MODE,
    ISIS_CLI_CMD_ISIS_IF_AUTH_MODE,
    ISIS_CLI_CMD_DEL_ISIS_IF_AUTH_MODE,
    ISIS_CLI_SHOW_INSTS,
    ISIS_CLI_SHOW_INTFS,
    ISIS_CLI_SHOW_ADJS,
    ISIS_CLI_SHOW_ROUTES,
    ISIS_CLI_SHOW_IPV6_ROUTES,
    ISIS_CLI_SHOW_LSPDB,
    ISIS_CLI_SHOW_IF_INFO,
    ISIS_CLI_SHOW_DBG_INFO,
    ISIS_CLI_SHOW_PKT_STATS,
    ISIS_CLI_SHOW_NSF,
    ISIS_CLI_CMD_DEBUG_ADJN,
    ISIS_CLI_CMD_DEBUG_DECN,
    ISIS_CLI_CMD_DEBUG_UPDT,
    ISIS_CLI_CMD_DEBUG_INTERFACE,
    ISIS_CLI_CMD_DEBUG_CONTROL,
    ISIS_CLI_CMD_DEBUG_TIMER,
    ISIS_CLI_CMD_DEBUG_FAULT,
    ISIS_CLI_CMD_DEBUG_RTMI,
    ISIS_CLI_CMD_DEBUG_DLLI,
    ISIS_CLI_CMD_DEBUG_TRFR,
    ISIS_CLI_CMD_DEBUG_SNMP,
    ISIS_CLI_CMD_DEBUG_UTIL,
    ISIS_CLI_CMD_DEBUG_ALL,
    ISIS_CLI_CMD_DEBUG_DUMP,
    ISIS_CLI_CMD_DEBUG_CRITICAL,
    ISIS_CLI_CMD_DEBUG_ENTRYEXIT,
    ISIS_CLI_CMD_DEBUG_DETAIL,
    ISIS_CLI_CMD_NO_DEBUG_ADJN,
    ISIS_CLI_CMD_NO_DEBUG_DECN,
    ISIS_CLI_CMD_NO_DEBUG_UPDT,
    ISIS_CLI_CMD_NO_DEBUG_INTERFACE,
    ISIS_CLI_CMD_NO_DEBUG_CONTROL,
    ISIS_CLI_CMD_NO_DEBUG_TIMER,
    ISIS_CLI_CMD_NO_DEBUG_FAULT,
    ISIS_CLI_CMD_NO_DEBUG_RTMI,
    ISIS_CLI_CMD_NO_DEBUG_DLLI,
    ISIS_CLI_CMD_NO_DEBUG_TRFR,
    ISIS_CLI_CMD_NO_DEBUG_SNMP,
    ISIS_CLI_CMD_NO_DEBUG_UTIL,
    ISIS_CLI_CMD_NO_DEBUG_ALL,
    ISIS_CLI_CMD_NO_DEBUG_DUMP,
    ISIS_CLI_CMD_NO_DEBUG_CRITICAL,
    ISIS_CLI_CMD_NO_DEBUG_ENTRYEXIT,
    ISIS_CLI_CMD_NO_DEBUG_DETAIL,
    ISIS_CLI_CMD_DISTRIBUTE_LIST,
    ISIS_CLI_CMD_ROUTE_DISTANCE,
    ISIS_CLI_CMD_NO_ROUTE_DISTANCE,
    ISIS_CLI_CMD_MULTI_TOPOLOGY,
    ISIS_CLI_CMD_PASSIVE_INTERFACE,
    ISIS_CLI_CMD_PASSIVE_INTERFACE_DEFAULT,
    ISIS_SYNC_STATUS,
    ISIS_CLI_SHOW_ACTUAL_DB,
    ISIS_CLI_SHOW_CONF_DB,
    ISIS_CLI_CMD_BFD,
    ISIS_CLI_CMD_BFD_ALL_IF,
    ISIS_CLI_CMD_BFD_IF,
    ISIS_CLI_CMD_DYN_HOSTNAME,
    ISIS_CLI_SHOW_HOSTNAME,
 ISIS_CLI_CMD_SET_METRIC_STYLE,
    CLI_ISIS_LEVEL_NOT_SUPPORT
};


#define ISIS_PASSIVE_INTERFACE_ENABLE 1
#define ISIS_PASSIVE_INTERFACE_DISABLE 2
#define ISIS_CLI_CONST_L1 1
#define ISIS_CLI_CONST_L12 3
#define ISIS_CLI_CONST_L2 2
#define ISIS_CLI_ERROR -1
#define ISIS_CLI_NO_ERROR 0
#define ISIS_CLI_INV_VALUE -1
#define ISIS_CLI_CONST_INTERNAL 0
#define ISIS_CLI_CONST_EXTERNAL 1
#define CLI_MODE_ROUTER_ISIS "isis-router"
#define  ISIS_INET_ATON(pc1Addr,pAddr) UtlInetAton((const CHR1 *)pc1Addr,pAddr)

#define ISIS_CLI_ROUTE_LEAK_ENABLE 1
#define ISIS_CLI_ROUTE_LEAK_DISABLE 2

#define ISIS_CLI_DEFAULT_CXT_ID 0

#define ISIS_NARROW 1  /*Single topology which is default value for metric style*/
#define ISIS_WIDE 2   /*Multi topology as metric style*/

/*Restart Support*/
#define ISIS_CLI_GR_RESTART_NONE    1
#define ISIS_CLI_GR_RESTART_PLANNED         2
#define ISIS_CLI_GR_RESTART_BOTH    3

/*Restart Reason*/
#define ISIS_CLI_GR_UNKNOWN         1
#define ISIS_CLI_GR_SW_RESTART      2
#define ISIS_CLI_GR_SW_UPGRADE      3
#define ISIS_CLI_GR_SW_RED          4


/*Helper Support*/
#define ISIS_CLI_GR_HELPER_NONE             1
#define ISIS_CLI_GR_HELPER_RESTART          2
#define ISIS_CLI_GR_HELPER_BOTH             3

#define ISIS_CLI_VAL_SET  1
#define ISIS_CLI_VAL_NO   2

/* Multi-topology Support */
#define ISIS_MT_ENABLE                     1
#define ISIS_MT_DISABLE                    2

/* BFD for M-ISIS Support */
#define ISIS_CLI_BFD_ENABLE     1
#define ISIS_CLI_BFD_DISABLE    2

/* Dynamic Hostname Support */
#define ISIS_CLI_DYNHOSTNME_ENABLE                     1
#define ISIS_CLI_DYNHOSTNME_DISABLE                    2

/* Point-to-point Support */
#define ISIS_CLI_BC_CKT     1
#define ISIS_CLI_P2P_CKT    2
/* Error codes 
 * Error code values and 
 * Error strings are maintained inside the protocol module itself.
 */


enum {
    CLI_ISIS_INV_INSTANCE = 1,
    CLI_ISIS_ROUTER_MODE_ERR,
    CLI_ISIS_INS_EN_INTER,
    CLI_ISIS_INS_NOT_EN_INTER,
    CLI_ISIS_BRIDGED_INTER,
    CLI_ISIS_INVALID_INDEX,
    CLI_ISIS_INV_LEVEL,
    CLI_ISIS_NO_ENTRY,
    CLI_ISIS_INV_LEN,
    CLI_ISIS_INV_SEL,
    CLI_ISIS_CREATE_INS,
    CLI_ISIS_INV_SYS_ID,
    CLI_ISIS_COMPLIANCE_EXIST,
    CLI_ISIS_INVALID_AREA_ID,
    CLI_ISIS_CRE_INS,
    CLI_ISIS_INV_IPRA,
    CLI_ISIS_INV_DEF_MET,
    CLI_ISIS_INV_DEL_MET,
    CLI_ISIS_INV_ERR_MET,
    CLI_ISIS_INV_EXP_MET,
    CLI_ISIS_INCORRECT_PASS,
    CLI_ISIS_INCORRECT_LEVEL,
    CLI_ISIS_RETRIEVE_RECORD_ERR,
    CLI_ISIS_INV_HELLO_INT,
    CLI_ISIS_INV_HELLO_MUL,
    CLI_ISIS_CKT_LVL_NO_EXIST,
    CLI_ISIS_INV_LSP,
    CLI_ISIS_INV_PRIO,
    CLI_ISIS_INV_CSNP_INT,
    CLI_ISIS_INV_LSP_ID,
    CLI_ISIS_LSP_NOT_FOUND,
    CLI_ISIS_NO_ADJ,
    CLI_ISIS_INVALID_INPUT,
    CLI_ISIS_UNABLE_TO_CREATE_ENTRY,
    CLI_ISIS_PARAM_NOT_INIT,
    CLI_ISIS_INVALID_SYS_TYPE,
    CLI_ISIS_INVALID_EXIST_STATE,
    CLI_ISIS_ROW_ACTIVE,
    CLI_ISIS_ENTRY_EXIST,
    CLI_ISIS_MAX_ENTRY,
    CLI_ISIS_RMAP_ASSOC_FAILED,
    CLI_ISIS_WRONG_DISTANCE_VALUE,
    CLI_ISIS_INV_ASSOC_ROUTE_MAP,
    CLI_ISIS_INV_DISASSOC_ROUTE_MAP,
    CLI_ISIS_NOTCONFIG_PASS,
    CLI_ISIS_NO_LOOPBACK_ACTIVE,
    CLI_ISIS_NO_ENTRY_ERROR,
    CLI_ISIS_INVALID_REDIS_PROTO_ENABLE_MASK_ERR,
    CLI_ISIS_MET_NOT_SUPP_MT,
    CLI_ISIS_MET_NOT_SUPP_ST,
    CLI_ISIS_NO_AREA_ADDR,
    CLI_ISIS_BFD_NOT_ENABLED,
    CLI_ISIS_NO_CKT_TYPE_PASSIVE, 
    CLI_ISIS_INVALID_FOR_P2P, 
    CLI_ISIS_MAX_ERR
};


/* The error strings should be places under the switch so as to avoid redifinition
 * This will be visible only in modulecli.c file
 */      


#ifdef __ISISCLI_C__

CONST CHR1  *IsisCliErrString [] = {
    NULL,
    "% Invalid Instance, Entry doesn't exists\r\n",
    "% Unable to enter into router mode\r\n",
    "% Isis is already enabled on this interface\r\n",
    "% ISIS is not enabled on this interface\r\n",
    "% Isis Cannot be enabled on bridged interface\r\n",
    "% Invalid Index\r\n",
    "% Invalid Level\r\n",
    "% No such entry exists\r\n",
    "% Invalid Length\r\n",
    "% Invalid Selector\r\n",
    "% Create an ISIS Instance and then configure NET\r\n",
    "% Enter the Correct system ID\r\n",
    "% Different Compliance format exist for the Index\r\n",
    "% Invalid Area ID\r\n",
    "% Create an ISIS Instance\r\n",
    "% Invalid Instance Index / IPRA Index\r\n",
    "% Invalid Default Metric\r\n",
    "% Invalid Delay Metric \r\n",
    "% Invalid Error Metric \r\n",
    "% Invalid Expense Metric \r\n",
    "% Incorrect Password\r\n",
    "% Incorrect level\r\n",
    "% Could not retrieve Circuit record\r\n",
    "% Incorrect Hello interval\r\n",
    "% Incorrect Hello Multiplier\r\n",
    "% The circuit level doesnot exist\r\n",
    "% Invalid LSP Interval\r\n",
    "% Incorrect IS Priority\r\n",
    "% Incorrect CSNP Interval\r\n",
    "% Invalid LSP Indentifier\r\n",
    "% LSP Not found\r\n",
    "% No Adjacencies formed in this interface\r\n",
    "% Invalid Input\r\n",
    "% Could  not create system entry\r\n",
    "% Mandatory Params Not Initialised\r\n",
    "% Invalid Value of SysType\r\n",
    "% Invalid Value of ExistState\r\n",
    "% Could not Set Index,as RowStatus is ACTIVE\r\n", 
    "% Entry already Exist\r\n",
    "% Number of Entries equals maximum, cannot create anymore Entries\r\n",
    "% Route-map association failed (probably another route map is used)\r\n",
    "% Wrong distance value\r\n",
    "% Route Map Not Associated \r\n",
    "% Route Map Not Disassociated \r\n",
    "% Password is not configured \r\n",
    "% Loop Back Interfaces can not be made active\r\n",
    "% IS-IS not active in any of the interfaces\r\n",
    "% Invalid Redistribution ProtoMask \r\n",
    "% Narrow/Delay/Error/Expense metric is not supported when multi-topology is enabled \r\n",
    "% Full/Wide metric is not supported when multi-topology is disabled \r\n",
    "% Invalid Area Address \r\n",
    "% Enable BFD for ISIS to enable BFD on all interfaces/a particular interface \r\n",
    "% Circuit-type cannot be changed to non-default values for passive Interfaces\r\n",
    "% Invalid Configuration for Point-to-Point Interface \r\n",
    "% Invalid \r\n"
   };

#endif /* __ISISCLI_C__ */


INT4 ShowIsisProtocolsInfo(tCliHandle CliHandle);
INT4
ShowIsisProtocolsInfoInCxt(tCliHandle CliHandle , UINT4 u4IsisCxtId);
INT4
IsisCliShowHostNmeTable (tCliHandle CliHandle, UINT4 u4InstIdx);

INT4 cli_process_isis_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command, ...));
INT1 IsIsGetRouterCfgPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1DispStr));
VOID IsIsShowRunningConfigInterfaceInCxt PROTO((tCliHandle CliHandle, INT4 i4Index));
INT4 cli_process_isis_test_cmd (tCliHandle CliHandle, ...);
#endif /* __ISISCLI_H__ */


