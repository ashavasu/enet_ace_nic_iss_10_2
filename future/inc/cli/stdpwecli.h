/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdpwecli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 PwEnetPwInstance[13];
extern UINT4 PwEnetPwVlan[13];
extern UINT4 PwEnetVlanMode[13];
extern UINT4 PwEnetPortVlan[13];
extern UINT4 PwEnetPortIfIndex[13];
extern UINT4 PwEnetVcIfIndex[13];
extern UINT4 PwEnetRowStatus[13];
extern UINT4 PwEnetStorageType[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetPwEnetPwVlan(u4PwIndex , u4PwEnetPwInstance ,u4SetValPwEnetPwVlan)	\
	nmhSetCmn(PwEnetPwVlan, 13, PwEnetPwVlanSet, L2vpnLock, L2vpnUnLock, 0, 0, 2, "%u %u %u", u4PwIndex , u4PwEnetPwInstance ,u4SetValPwEnetPwVlan)
#define nmhSetPwEnetVlanMode(u4PwIndex , u4PwEnetPwInstance ,i4SetValPwEnetVlanMode)	\
	nmhSetCmn(PwEnetVlanMode, 13, PwEnetVlanModeSet, L2vpnLock, L2vpnUnLock, 0, 0, 2, "%u %u %i", u4PwIndex , u4PwEnetPwInstance ,i4SetValPwEnetVlanMode)
#define nmhSetPwEnetPortVlan(u4PwIndex , u4PwEnetPwInstance ,u4SetValPwEnetPortVlan)	\
	nmhSetCmn(PwEnetPortVlan, 13, PwEnetPortVlanSet, L2vpnLock, L2vpnUnLock, 0, 0, 2, "%u %u %u", u4PwIndex , u4PwEnetPwInstance ,u4SetValPwEnetPortVlan)
#define nmhSetPwEnetPortIfIndex(u4PwIndex , u4PwEnetPwInstance ,i4SetValPwEnetPortIfIndex)	\
	nmhSetCmn(PwEnetPortIfIndex, 13, PwEnetPortIfIndexSet, L2vpnLock, L2vpnUnLock, 0, 0, 2, "%u %u %i", u4PwIndex , u4PwEnetPwInstance ,i4SetValPwEnetPortIfIndex)
#define nmhSetPwEnetVcIfIndex(u4PwIndex , u4PwEnetPwInstance ,i4SetValPwEnetVcIfIndex)	\
	nmhSetCmn(PwEnetVcIfIndex, 13, PwEnetVcIfIndexSet, L2vpnLock, L2vpnUnLock, 0, 0, 2, "%u %u %i", u4PwIndex , u4PwEnetPwInstance ,i4SetValPwEnetVcIfIndex)
#define nmhSetPwEnetRowStatus(u4PwIndex , u4PwEnetPwInstance ,i4SetValPwEnetRowStatus)	\
	nmhSetCmn(PwEnetRowStatus, 13, PwEnetRowStatusSet, L2vpnLock, L2vpnUnLock, 0, 1, 2, "%u %u %i", u4PwIndex , u4PwEnetPwInstance ,i4SetValPwEnetRowStatus)
#define nmhSetPwEnetStorageType(u4PwIndex , u4PwEnetPwInstance ,i4SetValPwEnetStorageType)	\
	nmhSetCmn(PwEnetStorageType, 13, PwEnetStorageTypeSet, L2vpnLock, L2vpnUnLock, 0, 0, 2, "%u %u %i", u4PwIndex , u4PwEnetPwInstance ,i4SetValPwEnetStorageType)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 PwEnetMplsPriMapping[13];
extern UINT4 PwEnetMplsPriMappingRowStatus[13];
extern UINT4 PwEnetMplsPriMappingStorageType[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetPwEnetMplsPriMapping(u4PwIndex ,pSetValPwEnetMplsPriMapping)	\
	nmhSetCmn(PwEnetMplsPriMapping, 13, PwEnetMplsPriMappingSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %s", u4PwIndex ,pSetValPwEnetMplsPriMapping)
#define nmhSetPwEnetMplsPriMappingRowStatus(u4PwIndex ,i4SetValPwEnetMplsPriMappingRowStatus)	\
	nmhSetCmn(PwEnetMplsPriMappingRowStatus, 13, PwEnetMplsPriMappingRowStatusSet, L2vpnLock, L2vpnUnLock, 0, 1, 1, "%u %i", u4PwIndex ,i4SetValPwEnetMplsPriMappingRowStatus)
#define nmhSetPwEnetMplsPriMappingStorageType(u4PwIndex ,i4SetValPwEnetMplsPriMappingStorageType)	\
	nmhSetCmn(PwEnetMplsPriMappingStorageType, 13, PwEnetMplsPriMappingStorageTypeSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %i", u4PwIndex ,i4SetValPwEnetMplsPriMappingStorageType)

#endif
