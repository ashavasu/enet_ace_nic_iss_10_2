#ifndef __DSCLI_H__
#define __DSCLI_H__

#include "cli.h"
#include "diffsrv.h"

/*===========================================================================*/
/*                     Defines used in commands file                         */
/*===========================================================================*/
#define DS_MAX_NAME_LENGTH          32
#define CLI_CLASSMAP_MODE          "ClassMap"
#define CLI_POLICYMAP_MODE         "PolicyMap"
#define CLI_POLICYCLASS_MODE        "PolClassMap"
#define DS_MAX_ARGS                 5
/* Type of action */
#define DS_POL_EXCEED                1     /* out profile or exceed action */
#define DS_POL_NOMATCH               2     /* no match action */
#define DS_POL_INPROF                3     /* in profile action */

/* Type of identifier */
#define DS_MATCH_FILTER              1     
#define DS_MATCH_DSCP                2
#define DS_CLASSMAP                  3
#define DS_POLICYMAP                 4

/* Actions to be performed on in profile, out profile and no match conditions */
#define DS_DROP                      1   /* drop the packet - do not switch */
#define DS_DSCP                      2   /* insert dscp */
#define DS_PREC                      3   /* insert precedence or IP TOS */
#define DS_COS                       4   /* insert cos or new priority */
/* Direction of INGRESS/EGRESS */
#define DS_DIRECTION_INGRESS             "1"
#define DS_DIRECTION_EGRESS              "2"

/* Direction of filtering */
#define DS_DIRECTION_IN               1
#define DS_DIRECTION_OUT              2

/* Filtering action */
#define DS_PKT_ALLOW                  1
#define DS_PKT_DROP                   2
/* Other defines */
#define DS_DEFAULT_INDEX              0
#define DS_NONE                       0
#define DS_ERR_BUF_SIZE              80

/* Macros for scheduling algorithms */

#define DS_COSQ_SCHE_ALGO_STRICT                  FS_DS_COSQ_SCHE_ALGO_STRICT 
#define DS_COSQ_SCHE_ALGO_RR                      FS_DS_COSQ_SCHE_ALGO_RR
#define DS_COSQ_SCHE_ALGO_WRR                     FS_DS_COSQ_SCHE_ALGO_WRR
#define DS_COSQ_SCHE_ALGO_WFQ                     FS_DS_COSQ_SCHE_ALGO_WFQ
#define DS_COSQ_SCHE_ALGO_STRICT_RR               FS_DS_COSQ_SCHE_ALGO_STRICT_RR
#define DS_COSQ_SCHE_ALGO_STRICT_WRR              FS_DS_COSQ_SCHE_ALGO_STRICT_WRR
#define DS_COSQ_SCHE_ALGO_STRICT_WFQ              FS_DS_COSQ_SCHE_ALGO_STRICT_WFQ
#define DS_COSQ_SCHE_ALGO_DEFICIT                 FS_DS_COSQ_SCHE_ALGO_DEFICIT

/*===========================================================================*/
/*                       enums used by DiffServ CLI                          */
/*===========================================================================*/

/* DiffServ CLI commands : to add a new command, a new value needs to be 
 * added to this enum 
 */
enum 
{
   CLI_DS_SYSTEM_CONTROL = 1,       /* start / shutdown system */
   CLI_DS_SHUT,                    /* shutdown system */
   CLI_DS_NO_SHUT,                 /* start the system */
   CLI_DS_CLASS_MAP,         	/* create/delete a class map  */
   CLI_DS_NO_CLASS_MAP,         	/* create/delete a class map  */
   CLI_DS_CLASS_MAP_MATCH,         /* Configure  class map matching criteria */
   CLI_DS_POL_MAP,                  /* create/delete a policy map  */
   CLI_DS_NO_POL_MAP,                  /* create/delete a policy map  */
   CLI_DS_POLCL_MAP,                /* map or unmap a classmap to a policy map  */
   CLI_DS_NO_POLCL_MAP,              /* map or unmap a classmap to a policy map  */
   CLI_DS_POL_IN_ACTION,            /* configure a policy map with inprofile */
   CLI_DS_NO_POL_IN_ACTION,            /* configure a policy map with inprofile */
   CLI_DS_POLICE,
   CLI_DS_COSQ_ALGO,                 /* configure scheduling algorithm */
   CLI_DS_COSQ_WEIGHT_BW,           /* configure weights and bandwidth for cosq */
   CLI_SHOW_DS_POLICYMAP,           /* show policy map details */
   CLI_SHOW_DS_CLASSMAP,             /* show class map details */
   CLI_SHOW_DS_COSQ_WEIGHT_BW,       /* show weights and bandwidths*/
   CLI_SHOW_DS_COSQ_ALGO,             /* show cosq algorithm */
   DS_CLI_MAX_COMMANDS
};

/* Type of action */
enum 
{
   DS_NOMATCH,                  /* no match action */
   DS_INPROF,                    /* in profile action */
   DS_OUTPROF                    /* in profile action */
};

/* Actions to be performed on in profile, out profile and no match conditions */
enum
{
   DS_ACT_DROP = 1,             /* drop the packet - do not switch */
   DS_ACT_DSCP,                 /* insert dscp */
   DS_ACT_PREC,                 /* insert precedence / IP TOS */
   DS_ACT_COS                   /* insert cos / new priority */
};

/* Type of identifier */
enum
{
   DS_IND_MAC_FILTER = 1,
   DS_IND_IP_FILTER,
   DS_IND_CLASS_MAP,
   DS_IND_POLICY_MAP
};
/*===========================================================================*/
/*                     Structures used by DiffServ CLI                       */
/*===========================================================================*/

/* Metering entry for a exceed or out profile action */

typedef struct
{
   INT4   i4DataPathId;         /* Datapath Index */
   INT4   i4MeterId;       /* The meter id */
   INT4   u4Refresh;       /* The refresh count */ 
   
}tDsMeter;

/* In profile, out profile or no match action details */

typedef struct
{

   INT4    i4ActionId;     /* Id associated with the action */
   UINT4   u4Dscp;         /* dscp to be inserted */
   UINT4   u4Cos;          /* cos or new priority to be inserted */
   UINT4   u4Precedence;   /* precedence or IP TOS to be inserted */
   INT4    i4Action;       /* The action : drop, dscp, cos, precedence */
   
}tDsAction;

/* classifier details */

typedef struct
{

   INT4   i4DsClfrId;          /* classifier id */
   INT4   i4DsMFClfrId;        /* multifield classifier id */
   INT4   i4DsInProActId;      /* in profile action id */
   INT4   i4DsOutProActId;     /* out profile action id */
   INT4   i4DsNoMatchActId;    /* no match action id */
   INT4   i4DsDataPathId;      /* data path id */
   
}tDsClfr;




/* Error code values and strings are maintained inside the protocol module itself.
*   */
enum {
        CLI_DS_FATAL_ERR = 1,
        CLI_DS_CLASS_MAX_LIMIT_ERR,
        CLI_DS_POLICY_MAX_LIMIT_ERR,
        CLI_DS_INPROF_MAX_LIMIT_ERR,
        CLI_DS_OUTPROF_MAX_LIMIT_ERR,
        CLI_DS_NOMATCH_MAX_LIMIT_ERR,
        CLI_DS_DATAPATH_MAX_LIMIT_ERR,
        CLI_DS_METER_MAX_LIMIT_ERR,
        CLI_DS_MFC_CHANGE_CLASS_ACTIVE_ERR,
        CLI_DS_FILTER_INACTIVE_ERR,
        CLI_DS_CLASS_INACTIVE_ERR,
        CLI_DS_MAX_ERR
};
#ifdef __DSCLI_C__
CONST CHR1  *DsCliErrString [] = {
        NULL,
        "% FATAL ERROR: Command Failed \r\n",
        "% Class-Map creation failed, max-limit exceeded \r\n",
        "% Policy-Map creation failed, max-limit exceeded \r\n",
        "% In-Profile creation failed, max-limit exceeded \r\n",
        "% Out-Profile creation failed, max-limit exceeded \r\n",
        "% No-Match creation failed, max-limit exceeded \r\n",
        "% Datapath creation failed, max-limit exceeded \r\n",
        "% Meter creation failed, max-limit exceeded \r\n",
        "% Changing class failed, policy already active \r\n",
        "% Filter-match failed, filter inconsistent or already mapped to a class \n\
        or filter not enabled on any interface\r\n",
        "% Class set failed, class not active \r\n"
};
#endif

#ifdef  __DSCLI_C__

#define DS_PORTS_PER_BYTE 8

UINT1               gau1UtlDsPortBitMask[DS_PORTS_PER_BYTE] =
    { 0x01, 0x80, 0x40, 0x20, 0x10,
    0x08, 0x04, 0x02
}
#define DS_ADD_PORT_TO_LIST(au1PortArray, u2Port) \
{\
    UINT2 u2PortBytePos;\
        UINT2 u2PortBitPos;\
        u2PortBytePos = (UINT2)(u2Port / DS_PORTS_PER_BYTE);\
        u2PortBitPos  = (UINT2)(u2Port % DS_PORTS_PER_BYTE);\
        if (u2PortBitPos  == 0) {u2PortBytePos -= 1;} \
            au1PortArray[u2PortBytePos] = (UINT1)(au1PortArray[u2PortBytePos] \
                                                       | gau1UtlDsPortBitMask[u2PortBitPos]);\
}
#define DS_DEL_PORT_FROM_LIST(au1PortArray, u2Port) \
{\
    UINT2 u2PortBytePos;\
        UINT2 u2PortBitPos;\
        u2PortBytePos = (UINT2)(u2Port / DS_PORTS_PER_BYTE);\
        u2PortBitPos  = (UINT2)(u2Port % DS_PORTS_PER_BYTE);\
        if (u2PortBitPos  == 0) {u2PortBytePos -= 1;} \
          au1PortArray[u2PortBytePos] = (UINT1)(au1PortArray[u2PortBytePos]                                            & ~ gau1UtlDsPortBitMask[u2PortBitPos]);\
}
#endif

;
INT4  DsCreateMFClfrFromMacFilter (tCliHandle CliHandle,
                                   INT4 i4MacFilterNo,
                                   INT4 i4MFClfrId);

INT4  DsCreateMFClfrFromIpFilter (tCliHandle CliHandle,
                                  INT4 i4IpFilterNo,
                                  INT4 i4MFClfrId);

INT4 DiffservNoPolicyClass(tCliHandle CliHandle, INT4 i4ClassMap);
    
INT4  DsCreateClfr (tCliHandle CliHandle, tDsClfr *pDsClfr, BOOLEAN u1NewFlag);

INT4 DiffservPolicyClass PROTO ((tCliHandle , INT4 ));

INT4 DsCreateInProfAction PROTO ((tCliHandle CliHandle, tDsAction * pDsAction));

INT4 DiffservOutProfAction PROTO ((tCliHandle CliHandle, UINT4, UINT4, UINT4 ));

INT4 DsCreateMeterEntry PROTO ((tCliHandle , tDsMeter * ));

INT4 DsCreateOutProfAction PROTO ((tCliHandle, tDsAction * pDsAction, INT4 i4MeterId));

INT4 DiffservNoMatchAction PROTO ((tCliHandle, UINT4 ,UINT4 ));

INT4 DsCreateNoMatchAction PROTO ((tCliHandle CliHandle, tDsAction * pDsAction));
    
INT4 DiffservSystemControl PROTO ((tCliHandle , UINT4));

INT4 DiffservCreateClassMap PROTO ((tCliHandle, INT4 ));

INT4 DiffservDestroyClassMap PROTO ((tCliHandle , INT4 ));

INT4 DiffservCreatePolicyMap PROTO ((tCliHandle, INT4 ));

INT4 DiffservDestroyPolicyMap PROTO ((tCliHandle , INT4 ));

INT4 DiffservMatchFilter PROTO ((tCliHandle, UINT4 ,INT4));

INT4 DiffservInProfAction PROTO ((tCliHandle , UINT4, UINT4 ));

INT4 DiffServSetCosqAlgo PROTO ((tCliHandle, UINT4, UINT4));

INT4 DiffServUpdateCosqWeightBw PROTO ((tCliHandle, UINT4, UINT4, UINT4, UINT4));

INT1 DiffsrvGetClassMapPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1PromptStr));

INT1 DiffsrvGetPolicyMapPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1PromptStr));

INT1 DiffsrvGetPolicyClassMapPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1PromptStr));
INT4 DiffservShowPolicyMap PROTO ((tCliHandle CliHandle , INT4 u4PolIndex, INT4 u4ClassIndex ));

INT4 DiffservShowClassMap PROTO ((tCliHandle CliHandle , UINT4 u4ClassIndex ));

VOID DiffservShowCoSqAlgo PROTO ((tCliHandle, INT4));

VOID DiffservShowCoSqWeightBw PROTO ((tCliHandle, INT4));

#ifdef NPAPI_WANTED
INT4 DiffservShowRunningConfig (tCliHandle CliHandle);
INT4 DiffservClassMapShowRunningConfig (tCliHandle CliHandle);
INT4 DiffservPolicyMapShowRunningConfig (tCliHandle CliHandle);
INT4 DiffservCoSqAlgoShowRunningConfig (tCliHandle CliHandle);
INT4 DiffservCoSqWeightBwShowRunningConfig (tCliHandle CliHandle);
#endif

INT4
DiffservShutdown (tCliHandle CliHandle, UINT4 u4Status);


INT4 DiffservNoInProfAction PROTO ((tCliHandle CliHandle, UINT4 u4InProfType, UINT4 u4Value));

INT4 DsDeleteInProfAction PROTO ((tCliHandle CliHandle,INT4 i4PolicyIndex,UINT4 u4Flag));

VOID DsCliPrintPortList( tCliHandle CliHandle,
                         INT4 i4CommaCount, UINT1 * piIfName);

extern tIssL2FilterEntry *IssGetL2FilterEntry PROTO ((INT4));

extern tIssL3FilterEntry *IssGetL3FilterEntry PROTO ((INT4));

INT4 cli_process_ds_cmd PROTO ((tCliHandle CliHandle,UINT4, ...));


/*===========================================================================*/
#endif   /* __DSCLI_H__ */
