/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fselmicli.h,v 1.4 2011/09/24 07:00:22 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsElmiSystemControl[10];
extern UINT4 FsElmiModuleStatus[10];
extern UINT4 FsElmiTraceOption[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsElmiSystemControl(i4SetValFsElmiSystemControl) \
 nmhSetCmn(FsElmiSystemControl, 10, FsElmiSystemControlSet, ElmLock, ElmUnLock, 0, 0, 0, "%i", i4SetValFsElmiSystemControl)
#define nmhSetFsElmiModuleStatus(i4SetValFsElmiModuleStatus) \
 nmhSetCmn(FsElmiModuleStatus, 10, FsElmiModuleStatusSet, ElmLock, ElmUnLock, 0, 0, 0, "%i", i4SetValFsElmiModuleStatus)
#define nmhSetFsElmiTraceOption(i4SetValFsElmiTraceOption) \
 nmhSetCmn(FsElmiTraceOption, 10, FsElmiTraceOptionSet, ElmLock, ElmUnLock, 0, 0, 0, "%i", i4SetValFsElmiTraceOption)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsElmiPort[12];
extern UINT4 FsElmiPortElmiStatus[12];
extern UINT4 FsElmiUniSide[12];
extern UINT4 FsElmiStatusCounter[12];
extern UINT4 FsElmiPollingVerificationTimerValue[12];
extern UINT4 FsElmiPollingTimerValue[12];
extern UINT4 FsElmiPollingCounterValue[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsElmiPortElmiStatus(i4FsElmiPort ,i4SetValFsElmiPortElmiStatus) \
 nmhSetCmn(FsElmiPortElmiStatus, 12, FsElmiPortElmiStatusSet, ElmLock, ElmUnLock, 0, 0, 1, "%i %i", i4FsElmiPort ,i4SetValFsElmiPortElmiStatus)
#define nmhSetFsElmiUniSide(i4FsElmiPort ,i4SetValFsElmiUniSide) \
 nmhSetCmn(FsElmiUniSide, 12, FsElmiUniSideSet, ElmLock, ElmUnLock, 0, 0, 1, "%i %i", i4FsElmiPort ,i4SetValFsElmiUniSide)
#define nmhSetFsElmiStatusCounter(i4FsElmiPort ,i4SetValFsElmiStatusCounter) \
 nmhSetCmn(FsElmiStatusCounter, 12, FsElmiStatusCounterSet, ElmLock, ElmUnLock, 0, 0, 1, "%i %i", i4FsElmiPort ,i4SetValFsElmiStatusCounter)
#define nmhSetFsElmiPollingVerificationTimerValue(i4FsElmiPort ,i4SetValFsElmiPollingVerificationTimerValue) \
 nmhSetCmn(FsElmiPollingVerificationTimerValue, 12, FsElmiPollingVerificationTimerValueSet, ElmLock, ElmUnLock, 0, 0, 1, "%i %i", i4FsElmiPort ,i4SetValFsElmiPollingVerificationTimerValue)
#define nmhSetFsElmiPollingTimerValue(i4FsElmiPort ,i4SetValFsElmiPollingTimerValue) \
 nmhSetCmn(FsElmiPollingTimerValue, 12, FsElmiPollingTimerValueSet, ElmLock, ElmUnLock, 0, 0, 1, "%i %i", i4FsElmiPort ,i4SetValFsElmiPollingTimerValue)
#define nmhSetFsElmiPollingCounterValue(i4FsElmiPort ,i4SetValFsElmiPollingCounterValue) \
 nmhSetCmn(FsElmiPollingCounterValue, 12, FsElmiPollingCounterValueSet, ElmLock, ElmUnLock, 0, 0, 1, "%i %i", i4FsElmiPort ,i4SetValFsElmiPollingCounterValue)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsElmiSetGlobalTrapOption[10];
extern UINT4 FsElmiSetTraps[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsElmiSetGlobalTrapOption(i4SetValFsElmiSetGlobalTrapOption) \
 nmhSetCmn(FsElmiSetGlobalTrapOption, 10, FsElmiSetGlobalTrapOptionSet, ElmLock, ElmUnLock, 0, 0, 0, "%i", i4SetValFsElmiSetGlobalTrapOption)
#define nmhSetFsElmiSetTraps(i4SetValFsElmiSetTraps) \
 nmhSetCmn(FsElmiSetTraps, 10, FsElmiSetTrapsSet, ElmLock, ElmUnLock, 0, 0, 0, "%i", i4SetValFsElmiSetTraps)

#endif
