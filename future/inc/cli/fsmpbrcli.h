/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpbrcli.h,v 1.2 2012/04/27 12:11:14 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIPbRstContextId[];
extern UINT4 FsMIPbProviderStpStatus[12];
extern UINT4 FsMIPbRstCVlanStpDebugOption[12];
