/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsdhcpscli.h,v 1.3 2013/07/09 12:53:34 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DhcpSrvEnable[10];
extern UINT4 DhcpSrvDebugLevel[10];
extern UINT4 DhcpSrvOfferReuseTimeOut[10];
extern UINT4 DhcpSrvIcmpEchoEnable[10];
extern UINT4 DhcpSrvBootServerAddress[10];
extern UINT4 DhcpSrvDefBootFilename[10];
extern UINT4 DhcpSrvBootpClientsSupported[10];
extern UINT4 DhcpSrvAutomaticBootpEnabled[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDhcpSrvEnable(i4SetValDhcpSrvEnable) \
 nmhSetCmn(DhcpSrvEnable, 10, DhcpSrvEnableSet, DhcpSProtocolLock, DhcpSProtocolUnLock, 0, 0, 0, "%i", i4SetValDhcpSrvEnable)
#define nmhSetDhcpSrvDebugLevel(i4SetValDhcpSrvDebugLevel) \
 nmhSetCmn(DhcpSrvDebugLevel, 10, DhcpSrvDebugLevelSet, DhcpSProtocolLock, DhcpSProtocolUnLock, 0, 0, 0, "%i", i4SetValDhcpSrvDebugLevel)
#define nmhSetDhcpSrvOfferReuseTimeOut(u4SetValDhcpSrvOfferReuseTimeOut) \
 nmhSetCmn(DhcpSrvOfferReuseTimeOut, 10, DhcpSrvOfferReuseTimeOutSet, DhcpSProtocolLock, DhcpSProtocolUnLock, 0, 0, 0, "%t", u4SetValDhcpSrvOfferReuseTimeOut)
#define nmhSetDhcpSrvIcmpEchoEnable(i4SetValDhcpSrvIcmpEchoEnable) \
 nmhSetCmn(DhcpSrvIcmpEchoEnable, 10, DhcpSrvIcmpEchoEnableSet, DhcpSProtocolLock, DhcpSProtocolUnLock, 0, 0, 0, "%i", i4SetValDhcpSrvIcmpEchoEnable)
#define nmhSetDhcpSrvBootServerAddress(u4SetValDhcpSrvBootServerAddress) \
 nmhSetCmn(DhcpSrvBootServerAddress, 10, DhcpSrvBootServerAddressSet, DhcpSProtocolLock, DhcpSProtocolUnLock, 0, 0, 0, "%p", u4SetValDhcpSrvBootServerAddress)
#define nmhSetDhcpSrvDefBootFilename(pSetValDhcpSrvDefBootFilename) \
 nmhSetCmn(DhcpSrvDefBootFilename, 10, DhcpSrvDefBootFilenameSet, DhcpSProtocolLock, DhcpSProtocolUnLock, 0, 0, 0, "%s", pSetValDhcpSrvDefBootFilename)
#define nmhSetDhcpSrvBootpClientsSupported(i4SetValDhcpSrvBootpClientsSupported) \
 nmhSetCmn(DhcpSrvBootpClientsSupported, 10, DhcpSrvBootpClientsSupportedSet, DhcpSProtocolLock, DhcpSProtocolUnLock, 0, 0, 0, "%i", i4SetValDhcpSrvBootpClientsSupported)
#define nmhSetDhcpSrvAutomaticBootpEnabled(i4SetValDhcpSrvAutomaticBootpEnabled) \
 nmhSetCmn(DhcpSrvAutomaticBootpEnabled, 10, DhcpSrvAutomaticBootpEnabledSet, DhcpSProtocolLock, DhcpSProtocolUnLock, 0, 0, 0, "%i", i4SetValDhcpSrvAutomaticBootpEnabled)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DhcpSrvSubnetPoolIndex[12];
extern UINT4 DhcpSrvSubnetSubnet[12];
extern UINT4 DhcpSrvSubnetPortNumber[12];
extern UINT4 DhcpSrvSubnetMask[12];
extern UINT4 DhcpSrvSubnetStartIpAddress[12];
extern UINT4 DhcpSrvSubnetEndIpAddress[12];
extern UINT4 DhcpSrvSubnetLeaseTime[12];
extern UINT4 DhcpSrvSubnetPoolName[12];
extern UINT4 DhcpSrvSubnetUtlThreshold[12];
extern UINT4 DhcpSrvSubnetPoolRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDhcpSrvSubnetSubnet(i4DhcpSrvSubnetPoolIndex ,u4SetValDhcpSrvSubnetSubnet) \
 nmhSetCmn(DhcpSrvSubnetSubnet, 12, DhcpSrvSubnetSubnetSet, DhcpSProtocolLock, DhcpSProtocolUnLock, 0, 0, 1, "%i %p", i4DhcpSrvSubnetPoolIndex ,u4SetValDhcpSrvSubnetSubnet)
#define nmhSetDhcpSrvSubnetPortNumber(i4DhcpSrvSubnetPoolIndex ,i4SetValDhcpSrvSubnetPortNumber) \
 nmhSetCmn(DhcpSrvSubnetPortNumber, 12, DhcpSrvSubnetPortNumberSet, DhcpSProtocolLock, DhcpSProtocolUnLock, 0, 0, 1, "%i %i", i4DhcpSrvSubnetPoolIndex ,i4SetValDhcpSrvSubnetPortNumber)
#define nmhSetDhcpSrvSubnetMask(i4DhcpSrvSubnetPoolIndex ,u4SetValDhcpSrvSubnetMask) \
 nmhSetCmn(DhcpSrvSubnetMask, 12, DhcpSrvSubnetMaskSet, DhcpSProtocolLock, DhcpSProtocolUnLock, 0, 0, 1, "%i %p", i4DhcpSrvSubnetPoolIndex ,u4SetValDhcpSrvSubnetMask)
#define nmhSetDhcpSrvSubnetStartIpAddress(i4DhcpSrvSubnetPoolIndex ,u4SetValDhcpSrvSubnetStartIpAddress) \
 nmhSetCmn(DhcpSrvSubnetStartIpAddress, 12, DhcpSrvSubnetStartIpAddressSet, DhcpSProtocolLock, DhcpSProtocolUnLock, 0, 0, 1, "%i %p", i4DhcpSrvSubnetPoolIndex ,u4SetValDhcpSrvSubnetStartIpAddress)
#define nmhSetDhcpSrvSubnetEndIpAddress(i4DhcpSrvSubnetPoolIndex ,u4SetValDhcpSrvSubnetEndIpAddress) \
 nmhSetCmn(DhcpSrvSubnetEndIpAddress, 12, DhcpSrvSubnetEndIpAddressSet, DhcpSProtocolLock, DhcpSProtocolUnLock, 0, 0, 1, "%i %p", i4DhcpSrvSubnetPoolIndex ,u4SetValDhcpSrvSubnetEndIpAddress)
#define nmhSetDhcpSrvSubnetLeaseTime(i4DhcpSrvSubnetPoolIndex ,i4SetValDhcpSrvSubnetLeaseTime) \
 nmhSetCmn(DhcpSrvSubnetLeaseTime, 12, DhcpSrvSubnetLeaseTimeSet, DhcpSProtocolLock, DhcpSProtocolUnLock, 0, 0, 1, "%i %i", i4DhcpSrvSubnetPoolIndex ,i4SetValDhcpSrvSubnetLeaseTime)
#define nmhSetDhcpSrvSubnetPoolName(i4DhcpSrvSubnetPoolIndex ,pSetValDhcpSrvSubnetPoolName) \
 nmhSetCmn(DhcpSrvSubnetPoolName, 12, DhcpSrvSubnetPoolNameSet, DhcpSProtocolLock, DhcpSProtocolUnLock, 0, 0, 1, "%i %s", i4DhcpSrvSubnetPoolIndex ,pSetValDhcpSrvSubnetPoolName)
#define nmhSetDhcpSrvSubnetUtlThreshold(i4DhcpSrvSubnetPoolIndex ,i4SetValDhcpSrvSubnetUtlThreshold) \
 nmhSetCmn(DhcpSrvSubnetUtlThreshold, 12, DhcpSrvSubnetUtlThresholdSet, DhcpSProtocolLock, DhcpSProtocolUnLock, 0, 0, 1, "%i %i", i4DhcpSrvSubnetPoolIndex ,i4SetValDhcpSrvSubnetUtlThreshold)
#define nmhSetDhcpSrvSubnetPoolRowStatus(i4DhcpSrvSubnetPoolIndex ,i4SetValDhcpSrvSubnetPoolRowStatus) \
 nmhSetCmn(DhcpSrvSubnetPoolRowStatus, 12, DhcpSrvSubnetPoolRowStatusSet, DhcpSProtocolLock, DhcpSProtocolUnLock, 0, 1, 1, "%i %i", i4DhcpSrvSubnetPoolIndex ,i4SetValDhcpSrvSubnetPoolRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DhcpSrvExcludeStartIpAddress[12];
extern UINT4 DhcpSrvExcludeEndIpAddress[12];
extern UINT4 DhcpSrvExcludeAddressRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDhcpSrvExcludeEndIpAddress(i4DhcpSrvSubnetPoolIndex , u4DhcpSrvExcludeStartIpAddress ,u4SetValDhcpSrvExcludeEndIpAddress) \
 nmhSetCmn(DhcpSrvExcludeEndIpAddress, 12, DhcpSrvExcludeEndIpAddressSet, DhcpSProtocolLock, DhcpSProtocolUnLock, 0, 0, 2, "%i %p %p", i4DhcpSrvSubnetPoolIndex , u4DhcpSrvExcludeStartIpAddress ,u4SetValDhcpSrvExcludeEndIpAddress)
#define nmhSetDhcpSrvExcludeAddressRowStatus(i4DhcpSrvSubnetPoolIndex , u4DhcpSrvExcludeStartIpAddress ,i4SetValDhcpSrvExcludeAddressRowStatus) \
 nmhSetCmn(DhcpSrvExcludeAddressRowStatus, 12, DhcpSrvExcludeAddressRowStatusSet, DhcpSProtocolLock, DhcpSProtocolUnLock, 0, 1, 2, "%i %p %i", i4DhcpSrvSubnetPoolIndex , u4DhcpSrvExcludeStartIpAddress ,i4SetValDhcpSrvExcludeAddressRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DhcpSrvGblOptType[12];
extern UINT4 DhcpSrvGblOptLen[12];
extern UINT4 DhcpSrvGblOptVal[12];
extern UINT4 DhcpSrvGblOptRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDhcpSrvGblOptLen(i4DhcpSrvGblOptType ,i4SetValDhcpSrvGblOptLen) \
 nmhSetCmn(DhcpSrvGblOptLen, 12, DhcpSrvGblOptLenSet, DhcpSProtocolLock, DhcpSProtocolUnLock, 0, 0, 1, "%i %i", i4DhcpSrvGblOptType ,i4SetValDhcpSrvGblOptLen)
#define nmhSetDhcpSrvGblOptVal(i4DhcpSrvGblOptType ,pSetValDhcpSrvGblOptVal) \
 nmhSetCmn(DhcpSrvGblOptVal, 12, DhcpSrvGblOptValSet, DhcpSProtocolLock, DhcpSProtocolUnLock, 0, 0, 1, "%i %s", i4DhcpSrvGblOptType ,pSetValDhcpSrvGblOptVal)
#define nmhSetDhcpSrvGblOptRowStatus(i4DhcpSrvGblOptType ,i4SetValDhcpSrvGblOptRowStatus) \
 nmhSetCmn(DhcpSrvGblOptRowStatus, 12, DhcpSrvGblOptRowStatusSet, DhcpSProtocolLock, DhcpSProtocolUnLock, 0, 1, 1, "%i %i", i4DhcpSrvGblOptType ,i4SetValDhcpSrvGblOptRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DhcpSrvSubnetOptType[12];
extern UINT4 DhcpSrvSubnetOptLen[12];
extern UINT4 DhcpSrvSubnetOptVal[12];
extern UINT4 DhcpSrvSubnetOptRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDhcpSrvSubnetOptLen(i4DhcpSrvSubnetPoolIndex , i4DhcpSrvSubnetOptType ,i4SetValDhcpSrvSubnetOptLen) \
 nmhSetCmn(DhcpSrvSubnetOptLen, 12, DhcpSrvSubnetOptLenSet, DhcpSProtocolLock, DhcpSProtocolUnLock, 0, 0, 2, "%i %i %i", i4DhcpSrvSubnetPoolIndex , i4DhcpSrvSubnetOptType ,i4SetValDhcpSrvSubnetOptLen)
#define nmhSetDhcpSrvSubnetOptVal(i4DhcpSrvSubnetPoolIndex , i4DhcpSrvSubnetOptType ,pSetValDhcpSrvSubnetOptVal) \
 nmhSetCmn(DhcpSrvSubnetOptVal, 12, DhcpSrvSubnetOptValSet, DhcpSProtocolLock, DhcpSProtocolUnLock, 0, 0, 2, "%i %i %s", i4DhcpSrvSubnetPoolIndex , i4DhcpSrvSubnetOptType ,pSetValDhcpSrvSubnetOptVal)
#define nmhSetDhcpSrvSubnetOptRowStatus(i4DhcpSrvSubnetPoolIndex , i4DhcpSrvSubnetOptType ,i4SetValDhcpSrvSubnetOptRowStatus) \
 nmhSetCmn(DhcpSrvSubnetOptRowStatus, 12, DhcpSrvSubnetOptRowStatusSet, DhcpSProtocolLock, DhcpSProtocolUnLock, 0, 1, 2, "%i %i %i", i4DhcpSrvSubnetPoolIndex , i4DhcpSrvSubnetOptType ,i4SetValDhcpSrvSubnetOptRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DhcpSrvHostType[12];
extern UINT4 DhcpSrvHostId[12];
extern UINT4 DhcpSrvHostOptType[12];
extern UINT4 DhcpSrvHostOptLen[12];
extern UINT4 DhcpSrvHostOptVal[12];
extern UINT4 DhcpSrvHostOptRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDhcpSrvHostOptLen(i4DhcpSrvHostType , pDhcpSrvHostId , i4DhcpSrvSubnetPoolIndex , i4DhcpSrvHostOptType ,i4SetValDhcpSrvHostOptLen) \
 nmhSetCmn(DhcpSrvHostOptLen, 12, DhcpSrvHostOptLenSet, DhcpSProtocolLock, DhcpSProtocolUnLock, 0, 0, 4, "%i %s %i %i %i", i4DhcpSrvHostType , pDhcpSrvHostId , i4DhcpSrvSubnetPoolIndex , i4DhcpSrvHostOptType ,i4SetValDhcpSrvHostOptLen)
#define nmhSetDhcpSrvHostOptVal(i4DhcpSrvHostType , pDhcpSrvHostId , i4DhcpSrvSubnetPoolIndex , i4DhcpSrvHostOptType ,pSetValDhcpSrvHostOptVal) \
 nmhSetCmn(DhcpSrvHostOptVal, 12, DhcpSrvHostOptValSet, DhcpSProtocolLock, DhcpSProtocolUnLock, 0, 0, 4, "%i %s %i %i %s", i4DhcpSrvHostType , pDhcpSrvHostId , i4DhcpSrvSubnetPoolIndex , i4DhcpSrvHostOptType ,pSetValDhcpSrvHostOptVal)
#define nmhSetDhcpSrvHostOptRowStatus(i4DhcpSrvHostType , pDhcpSrvHostId , i4DhcpSrvSubnetPoolIndex , i4DhcpSrvHostOptType ,i4SetValDhcpSrvHostOptRowStatus) \
 nmhSetCmn(DhcpSrvHostOptRowStatus, 12, DhcpSrvHostOptRowStatusSet, DhcpSProtocolLock, DhcpSProtocolUnLock, 0, 1, 4, "%i %s %i %i %i", i4DhcpSrvHostType , pDhcpSrvHostId , i4DhcpSrvSubnetPoolIndex , i4DhcpSrvHostOptType ,i4SetValDhcpSrvHostOptRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DhcpSrvHostIpAddress[12];
extern UINT4 DhcpSrvHostPoolName[12];
extern UINT4 DhcpSrvHostBootFileName[12];
extern UINT4 DhcpSrvHostBootServerAddress[12];
extern UINT4 DhcpSrvHostConfigRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDhcpSrvHostIpAddress(i4DhcpSrvHostType , pDhcpSrvHostId , i4DhcpSrvSubnetPoolIndex ,u4SetValDhcpSrvHostIpAddress) \
 nmhSetCmn(DhcpSrvHostIpAddress, 12, DhcpSrvHostIpAddressSet, DhcpSProtocolLock, DhcpSProtocolUnLock, 0, 0, 3, "%i %s %i %p", i4DhcpSrvHostType , pDhcpSrvHostId , i4DhcpSrvSubnetPoolIndex ,u4SetValDhcpSrvHostIpAddress)
#define nmhSetDhcpSrvHostPoolName(i4DhcpSrvHostType , pDhcpSrvHostId , i4DhcpSrvSubnetPoolIndex ,i4SetValDhcpSrvHostPoolName) \
 nmhSetCmn(DhcpSrvHostPoolName, 12, DhcpSrvHostPoolNameSet, DhcpSProtocolLock, DhcpSProtocolUnLock, 0, 0, 3, "%i %s %i %i", i4DhcpSrvHostType , pDhcpSrvHostId , i4DhcpSrvSubnetPoolIndex ,i4SetValDhcpSrvHostPoolName)
#define nmhSetDhcpSrvHostBootFileName(i4DhcpSrvHostType , pDhcpSrvHostId , i4DhcpSrvSubnetPoolIndex ,pSetValDhcpSrvHostBootFileName) \
 nmhSetCmn(DhcpSrvHostBootFileName, 12, DhcpSrvHostBootFileNameSet, DhcpSProtocolLock, DhcpSProtocolUnLock, 0, 0, 3, "%i %s %i %s", i4DhcpSrvHostType , pDhcpSrvHostId , i4DhcpSrvSubnetPoolIndex ,pSetValDhcpSrvHostBootFileName)
#define nmhSetDhcpSrvHostBootServerAddress(i4DhcpSrvHostType , pDhcpSrvHostId , i4DhcpSrvSubnetPoolIndex ,u4SetValDhcpSrvHostBootServerAddress) \
 nmhSetCmn(DhcpSrvHostBootServerAddress, 12, DhcpSrvHostBootServerAddressSet, DhcpSProtocolLock, DhcpSProtocolUnLock, 0, 0, 3, "%i %s %i %p", i4DhcpSrvHostType , pDhcpSrvHostId , i4DhcpSrvSubnetPoolIndex ,u4SetValDhcpSrvHostBootServerAddress)
#define nmhSetDhcpSrvHostConfigRowStatus(i4DhcpSrvHostType , pDhcpSrvHostId , i4DhcpSrvSubnetPoolIndex ,i4SetValDhcpSrvHostConfigRowStatus) \
 nmhSetCmn(DhcpSrvHostConfigRowStatus, 12, DhcpSrvHostConfigRowStatusSet, DhcpSProtocolLock, DhcpSProtocolUnLock, 0, 1, 3, "%i %s %i %i", i4DhcpSrvHostType , pDhcpSrvHostId , i4DhcpSrvSubnetPoolIndex ,i4SetValDhcpSrvHostConfigRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DhcpSrvBindIpAddress[12];
extern UINT4 DhcpSrvBindEntryStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDhcpSrvBindEntryStatus(u4DhcpSrvBindIpAddress ,i4SetValDhcpSrvBindEntryStatus) \
 nmhSetCmn(DhcpSrvBindEntryStatus, 12, DhcpSrvBindEntryStatusSet, DhcpSProtocolLock, DhcpSProtocolUnLock, 0, 1, 1, "%p %i", u4DhcpSrvBindIpAddress ,i4SetValDhcpSrvBindEntryStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DhcpCountResetCounters[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDhcpCountResetCounters(i4SetValDhcpCountResetCounters) \
 nmhSetCmn(DhcpCountResetCounters, 10, DhcpCountResetCountersSet, DhcpSProtocolLock, DhcpSProtocolUnLock, 0, 0, 0, "%i", i4SetValDhcpCountResetCounters)

#endif
