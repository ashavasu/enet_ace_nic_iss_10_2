/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fslacli.h,v 1.18 2017/09/28 13:34:19 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsLaSystemControl[10];
extern UINT4 FsLaStatus[10];
extern UINT4 FsLaTraceOption[10];
extern UINT4 FsLaActorSystemID[10];
extern UINT4 FsLaNoPartnerIndep[10];
#ifdef DLAG_WANTED
extern UINT4 FsLaDLAGSystemStatus[10];
extern UINT4 FsLaDLAGSystemID[10];
extern UINT4 FsLaDLAGSystemPriority[10];
extern UINT4 FsLaDLAGPeriodicSyncTime[10];
extern UINT4 FsLaDLAGDistributingPortIndex[10];
extern UINT4 FsLaDLAGDistributingPortList[10];
#endif
extern UINT4 FsLaRecTmrDuration[10];
extern UINT4 FsLaRecThreshold[10];
extern UINT4 FsLaMCLAGSystemStatus[10];
extern UINT4 FsLaMCLAGSystemID[10];
extern UINT4 FsLaMCLAGSystemPriority[10];
extern UINT4 FsLaMCLAGPeriodicSyncTime[10];
extern UINT4 FsLaMCLAGClearCounters[10];
extern UINT4 FsLaClearStatistics[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsLaSystemControl(i4SetValFsLaSystemControl) \
 nmhSetCmn(FsLaSystemControl, 10, FsLaSystemControlSet, LaLock, LaUnLock, 0, 0, 0, "%i", i4SetValFsLaSystemControl)
#define nmhSetFsLaStatus(i4SetValFsLaStatus) \
 nmhSetCmn(FsLaStatus, 10, FsLaStatusSet, LaLock, LaUnLock, 0, 0, 0, "%i", i4SetValFsLaStatus)
#define nmhSetFsLaTraceOption(i4SetValFsLaTraceOption) \
 nmhSetCmn(FsLaTraceOption, 10, FsLaTraceOptionSet, LaLock, LaUnLock, 0, 0, 0, "%i", i4SetValFsLaTraceOption)
#define nmhSetFsLaActorSystemID(SetValFsLaActorSystemID) \
 nmhSetCmn(FsLaActorSystemID, 10, FsLaActorSystemIDSet, LaLock, LaUnLock, 0, 0, 0, "%m", SetValFsLaActorSystemID)
#define nmhSetFsLaNoPartnerIndep(i4SetValFsLaNoPartnerIndep) \
 nmhSetCmn(FsLaNoPartnerIndep, 10, FsLaNoPartnerIndepSet, LaLock, LaUnLock, 0, 0, 0, "%i", i4SetValFsLaNoPartnerIndep)
#ifdef DLAG_WANTED
#define nmhSetFsLaDLAGSystemStatus(i4SetValFsLaDLAGSystemStatus) \
 nmhSetCmn(FsLaDLAGSystemStatus, 10, FsLaDLAGSystemStatusSet, LaLock, LaUnLock, 0, 0, 0, "%i", i4SetValFsLaDLAGSystemStatus)
#define nmhSetFsLaDLAGSystemID(SetValFsLaDLAGSystemID) \
 nmhSetCmn(FsLaDLAGSystemID, 10, FsLaDLAGSystemIDSet, LaLock, LaUnLock, 0, 0, 0, "%m", SetValFsLaDLAGSystemID)
#define nmhSetFsLaDLAGSystemPriority(i4SetValFsLaDLAGSystemPriority) \
 nmhSetCmn(FsLaDLAGSystemPriority, 10, FsLaDLAGSystemPrioritySet, LaLock, LaUnLock, 0, 0, 0, "%i", i4SetValFsLaDLAGSystemPriority)
#define nmhSetFsLaDLAGPeriodicSyncTime(u4SetValFsLaDLAGPeriodicSyncTime) \
 nmhSetCmn(FsLaDLAGPeriodicSyncTime, 10, FsLaDLAGPeriodicSyncTimeSet, LaLock, LaUnLock, 0, 0, 0, "%u", u4SetValFsLaDLAGPeriodicSyncTime)
#define nmhSetFsLaDLAGDistributingPortIndex(i4SetValFsLaDLAGDistributingPortIndex) \
 nmhSetCmn(FsLaDLAGDistributingPortIndex, 10, FsLaDLAGDistributingPortIndexSet, LaLock, LaUnLock, 0, 0, 0, "%i", i4SetValFsLaDLAGDistributingPortIndex)
#define nmhSetFsLaDLAGDistributingPortList(pSetValFsLaDLAGDistributingPortList) \
 nmhSetCmn(FsLaDLAGDistributingPortList, 10, FsLaDLAGDistributingPortListSet, LaLock, LaUnLock, 0, 0, 0, "%s", pSetValFsLaDLAGDistributingPortList)
#endif
#define nmhSetFsLaRecTmrDuration(u4SetValFsLaRecTmrDuration)   \
 nmhSetCmn(FsLaRecTmrDuration, 10, FsLaRecTmrDurationSet, LaLock, LaUnLock, 0, 0, 0, "%u", u4SetValFsLaRecTmrDuration)
#define nmhSetFsLaRecThreshold(u4SetValFsLaRecThreshold)   \
 nmhSetCmn(FsLaRecThreshold, 10, FsLaRecThresholdSet, LaLock, LaUnLock, 0, 0, 0, "%u", u4SetValFsLaRecThreshold)
#define nmhSetFsLaMCLAGSystemStatus(i4SetValFsLaMCLAGSystemStatus) \
 nmhSetCmn(FsLaMCLAGSystemStatus, 10, FsLaMCLAGSystemStatusSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsLaMCLAGSystemStatus)
#define nmhSetFsLaMCLAGSystemID(SetValFsLaMCLAGSystemID)    \
     nmhSetCmn(FsLaMCLAGSystemID, 10, FsLaMCLAGSystemIDSet, NULL, NULL, 0, 0, 0, "%m", SetValFsLaMCLAGSystemID)
#define nmhSetFsLaMCLAGSystemPriority(i4SetValFsLaMCLAGSystemPriority)  \
         nmhSetCmn(FsLaMCLAGSystemPriority, 10, FsLaMCLAGSystemPrioritySet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsLaMCLAGSystemPriority)
#define nmhSetFsLaMCLAGPeriodicSyncTime(u4SetValFsLaMCLAGPeriodicSyncTime)  \
             nmhSetCmn(FsLaMCLAGPeriodicSyncTime, 10, FsLaMCLAGPeriodicSyncTimeSet, NULL, NULL, 0, 0, 0, "%u", u4SetValFsLaMCLAGPeriodicSyncTime)
#define nmhSetFsLaMCLAGClearCounters(i4SetValFsLaMCLAGClearCounters)  \
             nmhSetCmn(FsLaMCLAGClearCounters, 10, FsLaMCLAGClearCountersSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsLaMCLAGClearCounters)
#define nmhSetFsLaClearStatistics(i4SetValFsLaClearStatistics)  \
    nmhSetCmnWithLock(FsLaClearStatistics, 10, FsLaClearStatisticsSet, LaLock, LaUnLock, 0, 0, 0, "%i", i4SetValFsLaClearStatistics)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsLaPortChannelIfIndex[12];
extern UINT4 FsLaPortChannelAdminMacAddress[12];
extern UINT4 FsLaPortChannelMacSelection[12];
extern UINT4 FsLaPortChannelSelectionPolicy[12];
extern UINT4 FsLaPortChannelDefaultPortIndex[12];
extern UINT4 FsLaPortChannelMaxPorts[12];
extern UINT4 FsLaPortChannelSelectionPolicyBitList[12];
#ifdef DLAG_WANTED
extern UINT4 FsLaPortChannelDLAGDistributingPortIndex[12];
extern UINT4 FsLaPortChannelDLAGSystemID[12];
extern UINT4 FsLaPortChannelDLAGSystemPriority[12];
extern UINT4 FsLaPortChannelDLAGPeriodicSyncTime[12];
extern UINT4 FsLaPortChannelDLAGMSSelectionWaitTime[12];
extern UINT4 FsLaPortChannelDLAGStatus[12];
extern UINT4 FsLaPortChannelDLAGRedundancy[12];
extern UINT4 FsLaPortChannelDLAGDistributingPortList[12];
#endif
extern UINT4 FsLaPortChannelMCLAGStatus[12];
extern UINT4 FsLaPortChannelMCLAGSystemID[12];
extern UINT4 FsLaPortChannelMCLAGSystemPriority[12];
extern UINT4 FsLaPortChannelClearStatistics[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsLaPortChannelAdminMacAddress(i4FsLaPortChannelIfIndex ,SetValFsLaPortChannelAdminMacAddress) \
 nmhSetCmn(FsLaPortChannelAdminMacAddress, 12, FsLaPortChannelAdminMacAddressSet, LaLock, LaUnLock, 0, 0, 1, "%i %m", i4FsLaPortChannelIfIndex ,SetValFsLaPortChannelAdminMacAddress)
#define nmhSetFsLaPortChannelMacSelection(i4FsLaPortChannelIfIndex ,i4SetValFsLaPortChannelMacSelection) \
 nmhSetCmn(FsLaPortChannelMacSelection, 12, FsLaPortChannelMacSelectionSet, LaLock, LaUnLock, 0, 0, 1, "%i %i", i4FsLaPortChannelIfIndex ,i4SetValFsLaPortChannelMacSelection)
#define nmhSetFsLaPortChannelSelectionPolicy(i4FsLaPortChannelIfIndex ,i4SetValFsLaPortChannelSelectionPolicy) \
 nmhSetCmn(FsLaPortChannelSelectionPolicy, 12, FsLaPortChannelSelectionPolicySet, LaLock, LaUnLock, 0, 0, 1, "%i %i", i4FsLaPortChannelIfIndex ,i4SetValFsLaPortChannelSelectionPolicy)
#define nmhSetFsLaPortChannelDefaultPortIndex(i4FsLaPortChannelIfIndex ,i4SetValFsLaPortChannelDefaultPortIndex) \
 nmhSetCmn(FsLaPortChannelDefaultPortIndex, 12, FsLaPortChannelDefaultPortIndexSet, LaLock, LaUnLock, 0, 0, 1, "%i %i", i4FsLaPortChannelIfIndex ,i4SetValFsLaPortChannelDefaultPortIndex)
#define nmhSetFsLaPortChannelMaxPorts(i4FsLaPortChannelIfIndex ,i4SetValFsLaPortChannelMaxPorts) \
 nmhSetCmn(FsLaPortChannelMaxPorts, 12, FsLaPortChannelMaxPortsSet, LaLock, LaUnLock, 0, 0, 1, "%i %i", i4FsLaPortChannelIfIndex ,i4SetValFsLaPortChannelMaxPorts)
#define nmhSetFsLaPortChannelSelectionPolicyBitList(i4FsLaPortChannelIfIndex ,i4SetValFsLaPortChannelSelectionPolicyBitList) \
 nmhSetCmn(FsLaPortChannelSelectionPolicyBitList, 12, FsLaPortChannelSelectionPolicyBitListSet, LaLock, LaUnLock, 0, 0, 1, "%i %i", i4FsLaPortChannelIfIndex ,i4SetValFsLaPortChannelSelectionPolicyBitList)
#ifdef DLAG_WANTED
#define nmhSetFsLaPortChannelDLAGDistributingPortIndex(i4FsLaPortChannelIfIndex ,i4SetValFsLaPortChannelDLAGDistributingPortIndex) \
 nmhSetCmn(FsLaPortChannelDLAGDistributingPortIndex, 12, FsLaPortChannelDLAGDistributingPortIndexSet, LaLock, LaUnLock, 0, 0, 1, "%i %i", i4FsLaPortChannelIfIndex ,i4SetValFsLaPortChannelDLAGDistributingPortIndex)
#define nmhSetFsLaPortChannelDLAGSystemID(i4FsLaPortChannelIfIndex ,SetValFsLaPortChannelDLAGSystemID) \
 nmhSetCmn(FsLaPortChannelDLAGSystemID, 12, FsLaPortChannelDLAGSystemIDSet, LaLock, LaUnLock, 0, 0, 1, "%i %m", i4FsLaPortChannelIfIndex ,SetValFsLaPortChannelDLAGSystemID)
#define nmhSetFsLaPortChannelDLAGSystemPriority(i4FsLaPortChannelIfIndex ,i4SetValFsLaPortChannelDLAGSystemPriority) \
 nmhSetCmn(FsLaPortChannelDLAGSystemPriority, 12, FsLaPortChannelDLAGSystemPrioritySet, LaLock, LaUnLock, 0, 0, 1, "%i %i", i4FsLaPortChannelIfIndex ,i4SetValFsLaPortChannelDLAGSystemPriority)
#define nmhSetFsLaPortChannelDLAGPeriodicSyncTime(i4FsLaPortChannelIfIndex ,u4SetValFsLaPortChannelDLAGPeriodicSyncTime) \
 nmhSetCmn(FsLaPortChannelDLAGPeriodicSyncTime, 12, FsLaPortChannelDLAGPeriodicSyncTimeSet, LaLock, LaUnLock, 0, 0, 1, "%i %u", i4FsLaPortChannelIfIndex ,u4SetValFsLaPortChannelDLAGPeriodicSyncTime)
#define nmhSetFsLaPortChannelDLAGMSSelectionWaitTime(i4FsLaPortChannelIfIndex ,u4SetValFsLaPortChannelDLAGMSSelectionWaitTime) \
 nmhSetCmn(FsLaPortChannelDLAGMSSelectionWaitTime, 12, FsLaPortChannelDLAGMSSelectionWaitTimeSet, LaLock, LaUnLock, 0, 0, 1, "%i %u", i4FsLaPortChannelIfIndex ,u4SetValFsLaPortChannelDLAGMSSelectionWaitTime)
#define nmhSetFsLaPortChannelDLAGStatus(i4FsLaPortChannelIfIndex ,i4SetValFsLaPortChannelDLAGStatus) \
 nmhSetCmn(FsLaPortChannelDLAGStatus, 12, FsLaPortChannelDLAGStatusSet, LaLock, LaUnLock, 0, 0, 1, "%i %i", i4FsLaPortChannelIfIndex ,i4SetValFsLaPortChannelDLAGStatus)
#define nmhSetFsLaPortChannelDLAGRedundancy(i4FsLaPortChannelIfIndex ,i4SetValFsLaPortChannelDLAGRedundancy) \
 nmhSetCmn(FsLaPortChannelDLAGRedundancy, 12, FsLaPortChannelDLAGRedundancySet, LaLock, LaUnLock, 0, 0, 1, "%i %i", i4FsLaPortChannelIfIndex ,i4SetValFsLaPortChannelDLAGRedundancy)
#define nmhSetFsLaPortChannelDLAGDistributingPortList(i4FsLaPortChannelIfIndex ,pSetValFsLaPortChannelDLAGDistributingPortList) \
 nmhSetCmn(FsLaPortChannelDLAGDistributingPortList, 12, FsLaPortChannelDLAGDistributingPortListSet, LaLock, LaUnLock, 0, 0, 1, "%i %s", i4FsLaPortChannelIfIndex ,pSetValFsLaPortChannelDLAGDistributingPortList)
#endif
#define nmhSetFsLaPortChannelMCLAGStatus(i4FsLaPortChannelIfIndex ,i4SetValFsLaPortChannelMCLAGStatus)  \
     nmhSetCmn(FsLaPortChannelMCLAGStatus, 12, FsLaPortChannelMCLAGStatusSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsLaPortChannelIfIndex ,i4SetValFsLaPortChannelMCLAGStatus)
#define nmhSetFsLaPortChannelMCLAGSystemID(i4FsLaPortChannelIfIndex ,SetValFsLaPortChannelMCLAGSystemID)    \
         nmhSetCmn(FsLaPortChannelMCLAGSystemID, 12, FsLaPortChannelMCLAGSystemIDSet, NULL, NULL, 0, 0, 1, "%i %m", i4FsLaPortChannelIfIndex ,SetValFsLaPortChannelMCLAGSystemID)
#define nmhSetFsLaPortChannelMCLAGSystemPriority(i4FsLaPortChannelIfIndex ,i4SetValFsLaPortChannelMCLAGSystemPriority)  \
             nmhSetCmn(FsLaPortChannelMCLAGSystemPriority, 12, FsLaPortChannelMCLAGSystemPrioritySet, NULL, NULL, 0, 0, 1, "%i %i", i4FsLaPortChannelIfIndex ,i4SetValFsLaPortChannelMCLAGSystemPriority)
#define nmhSetFsLaPortChannelClearStatistics(i4FsLaPortChannelIfIndex ,i4SetValFsLaPortChannelClearStatistics)  \
                 nmhSetCmnWithLock(FsLaPortChannelClearStatistics, 12, FsLaPortChannelClearStatisticsSet, LaLock, LaUnLock, 0, 0, 1, "%i %i", i4FsLaPortChannelIfIndex ,i4SetValFsLaPortChannelClearStatistics)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsLaPortIndex[12];
extern UINT4 FsLaPortMode[12];
extern UINT4 FsLaPortActorResetAdminState[12];
extern UINT4 FsLaPortAggregateWaitTime[12];
extern UINT4 FsLaPortPartnerResetAdminState[12];
extern UINT4 FsLaPortActorAdminPort[12];
extern UINT4 FsLaPortRestoreMtu[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsLaPortMode(i4FsLaPortIndex ,i4SetValFsLaPortMode) \
 nmhSetCmn(FsLaPortMode, 12, FsLaPortModeSet, LaLock, LaUnLock, 0, 0, 1, "%i %i", i4FsLaPortIndex ,i4SetValFsLaPortMode)
#define nmhSetFsLaPortActorResetAdminState(i4FsLaPortIndex ,pSetValFsLaPortActorResetAdminState) \
 nmhSetCmn(FsLaPortActorResetAdminState, 12, FsLaPortActorResetAdminStateSet, LaLock, LaUnLock, 0, 0, 1, "%i %s", i4FsLaPortIndex ,pSetValFsLaPortActorResetAdminState)
#define nmhSetFsLaPortAggregateWaitTime(i4FsLaPortIndex ,u4SetValFsLaPortAggregateWaitTime) \
 nmhSetCmn(FsLaPortAggregateWaitTime, 12, FsLaPortAggregateWaitTimeSet, LaLock, LaUnLock, 0, 0, 1, "%i %t", i4FsLaPortIndex ,u4SetValFsLaPortAggregateWaitTime)
#define nmhSetFsLaPortPartnerResetAdminState(i4FsLaPortIndex ,pSetValFsLaPortPartnerResetAdminState) \
 nmhSetCmn(FsLaPortPartnerResetAdminState, 12, FsLaPortPartnerResetAdminStateSet, LaLock, LaUnLock, 0, 0, 1, "%i %s", i4FsLaPortIndex ,pSetValFsLaPortPartnerResetAdminState)
#define nmhSetFsLaPortActorAdminPort(i4FsLaPortIndex ,i4SetValFsLaPortActorAdminPort) \
 nmhSetCmn(FsLaPortActorAdminPort, 12, FsLaPortActorAdminPortSet, LaLock, LaUnLock, 0, 0, 1, "%i %i", i4FsLaPortIndex ,i4SetValFsLaPortActorAdminPort)
#define nmhSetFsLaPortRestoreMtu(i4FsLaPortIndex ,i4SetValFsLaPortRestoreMtu) \
 nmhSetCmn(FsLaPortRestoreMtu, 12, FsLaPortRestoreMtuSet, LaLock, LaUnLock, 0, 0, 1, "%i %i", i4FsLaPortIndex ,i4SetValFsLaPortRestoreMtu)

#endif
