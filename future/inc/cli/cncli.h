/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: cncli.h,v 1.3 2013/07/24 14:19:44 siva Exp $
*
* Description: This file contains the data structure and macros
*              for CN CLI module
**********************************************************************/
#ifndef _CNCLI_H_
#define _CNCLI_H_

#include "cli.h"

/* Max No of variable inputs to CLI parser */
#define CN_CLI_MAX_ARGS              7

/*Error Strings*/
enum
{
    CN_CLI_CNPV_CREATION_VIOLATED = 1,
    CN_CLI_PORT_DEF_DEFENSE_SET_NOT_ALLOWED,
    CN_CLI_INVALID_COMP_ID,
    CN_CLI_INVALID_PORT_ID,
    CN_CLI_INVALID_CNPV,
    CN_CLI_INVALID_CPINDEX,
    CN_CLI_INVALID_VLAN_PRIORITY,
    CN_CLI_INVALID_LLDP_INST_SELECTOR,
    CN_CLI_ENTRY_NOT_FOUND,
    CN_CLI_CNPV_ALREADY_EXIST,
    CN_CLI_COMP_ENTRY_NOT_FOUND,
    CN_CLI_COMP_PRI_ENTRY_NOT_FOUND,
    CN_CLI_PORT_ENTRY_NOT_FOUND,
    CN_CLI_PORT_PRI_ENTRY_NOT_FOUND,
    CN_CLI_ERROR_PORT_ENTRY_NOT_FOUND,
    CN_CLI_CPID_ENTRY_NOT_FOUND,
    CN_CLI_ERROR_GET_TC_FAILED,
    CN_CLI_ENTRY_NOT_ACTIVE,
    CN_CLI_INVALID_QSETPT,
    CN_CLI_INVALID_FB_WGT,
    CN_CLI_INVALID_SAMPLE_BASE,
    CN_CLI_INVALID_HDR_OCTS,
    CN_CLI_INVALID_CPID_LEN,
    CN_CLI_PRIORITY_MAP_ALREADY_EXIST,
    CN_CLI_WRONG_FEED_BACK_WGT,
    CN_CLI_WRONG_CONG_PROP
};

#ifdef CN_CLI_C
/*CN Error String*/
CONST CHR1  *gapc1CnCliErrString[] =
{
  NULL,
  "Maximum CNPVs for this component has been created already",      
  "Setting of Port default defense after creation of cnpv is not allowed",
  "Invalid Context Id",
  "Invalid PortId",
  "Invalid CNPV",
  "Invalid CP-Index",
  "Invalid VLAN Priority. Priority range: 0-7",
  "Invalid Value for LLDP Instance Selector.Only default value is allowed",
  "Entry does not exist",
  "CNPV Already Created",
  "Component not created",
  "CNPV not configured in the Componnet",
  "Port not found",
  "CNPV not configured on the port",
  "Error Port Entry does not exist",
  "CPID Table does not exist",
  "Failed to get Traffic Class for CNPV",
  "Component Priority Entry is not Active",
  "Queiue Size Set Point is not in allowed range",
  "Feed Back Weight is not in allowed range",
  "Minimum Sample Base is not in allowed range",
  "Header Octets not in allowed range",
  "Invalid CPID Length",
  "Priority Map for this VLAN-Priority already Exist. can't be configured as CNPV",
  "Enter integer value for FeedBackWeight",
  "Enter value for at least one congestion point properties",
  NULL
};
#endif

/* Command Identifiers */
enum
{
 CLI_CN_SYS_CTRL = 1,
 CLI_CN_SET_CN,
 CLI_CN_SET_CNM_PRIO,
 CLI_CN_SET_CNPV,
 CLI_CN_NO_CNPV,
 CLI_CN_SET_DEF_CHOICE,
 CLI_CN_SET_ALT_PRI,
 CLI_CN_SET_DEF_MODE,
 CLI_CN_SET_PORT_DEF_CH,
 CLI_CN_SET_LLDP_CHOICE,
 CLI_CN_SET_LLDP_SELECTOR,
 CLI_CN_CLEAR_COUNTER,
 CLI_CN_DEBUG,
 CLI_CN_NO_DEBUG,
 CLI_CN_SET_NOTIFY,
 CLI_CN_NO_NOTIFY,
 CLI_CN_PORT_SET_DEF_CH,
 CLI_CN_PORT_DEF_MODE,
 CLI_CN_PORT_LLDP_CH,
 CLI_CN_PORT_LLDP_SELECTOR,
 CLI_CN_PORT_ALT_PRI,
 CLI_CN_PORT_SET_CP_PARAM,
 CLI_CN_SHOW_GLOB_INFO,
 CLI_CN_SHOW_INT_COUNTERS,
 CLI_CN_SHOW_CNPV,
 CLI_CN_SHOW_CP_PARAM,
 CLI_CN_MAX_CMD_IDENTIFY
};

/* Function Prototypes */
PUBLIC INT4
cli_process_cn_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command, ...));

PUBLIC INT4 CnCliShowRunningConfig PROTO((tCliHandle CliHandle, UINT4 u4CompId,
                                          UINT4 u4Module));
PUBLIC INT4
CnCliShowGlobalRunningConfig PROTO ((tCliHandle CliHandle));
PUBLIC VOID IssCnShowDebugging PROTO ((tCliHandle CliHandle));
#endif

/*--------------------------------------------------------------------------*/
/*                         End of the file cncli.h                        */
/*--------------------------------------------------------------------------*/

