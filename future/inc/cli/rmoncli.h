/********************************************************************
 *                                                                  *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                        *
 *                                                                  *
 * $Id: rmoncli.h,v 1.21 2016/06/18 11:23:16 siva Exp $             *
 *                                                                  *
 * Description: Contains SNMP low level routines for History Group  *
 *                                                                  *
 *******************************************************************/

#ifndef _RMONCLI_H
#define _RMONCLI_H

#include "midconst.h"
#include "cli.h"





/* RMON CLI Command Constants. 
 * To add a new RMON CLI command, add the command definition to this list */


enum {
    CLI_RMON_SHOW_RMON = 1,
    CLI_RMON_STATUS,
    CLI_RMON_ALARM,
    CLI_RMON_NO_ALARM,
    CLI_RMON_EVENT,
    CLI_RMON_NO_EVENT,
    CLI_RMON_COLLECTION_STATS,
    CLI_RMON_NO_COLLECTION_STATS,
    CLI_RMON_COLLECTION_HISTORY,
    CLI_RMON_NO_COLLECTION_HISTORY,
    CLI_RMON_COLLECTION_STATS_FOR_VLAN,
    CLI_RMON_NO_COLLECTION_STATS_FOR_VLAN,
    CLI_RMON_COLLECTION_HISTORY_FOR_VLAN,
    CLI_RMON_NO_COLLECTION_HISTORY_FOR_VLAN,
    CLI_RMON_MAX_COMMANDS
};

/* Command options for rmon alarm command */
#define RMON_ALARM_ABSOLUTE 1
#define RMON_ALARM_DELTA 2

/* Command constants for SHOW RMON command */

#define RMON_SHOW_STATS    1
#define RMON_SHOW_ALARMS   2
#define RMON_SHOW_EVENTS   4
#define RMON_SHOW_HISTORY  8
#define RMON_SHOW_HISTORY_OVERVIEW 16

#define RMON_U8_STR_LENGTH 21 
  
/* Command constants for rmon event command */

#define RMON_EVENT_NONE           1
#define RMON_EVENT_LOG            2
#define RMON_EVENT_TRAP           3  
#define RMON_EVENT_LOG_AND_TRAP   4

/* Macros for default values of buckets and interval */
#define RMON_DEF_BUCKETS          50
#define RMON_DEF_INTERVAL         1800


/* Malloc and free macros for RMON */
#define RMON_MALLOC(size, type) MEM_MALLOC(size, type)
#define RMON_FREE(ptr)          MEM_FREE(ptr)

#define RMON_MAX_HOSTCTRL_ENTRIES (MAX_INTERFACE*10) 

#define RMON_MAX_HOST_ENTRIES 200 

#define RMON_MAX_TOPNCTRL_ENTRIES (MAX_TOPN_CONTROL_ENTRY*15) 

#define RMON_MAX_TOPN_ENTRIES 200 

#define RMON_MAX_MTRXCTRL_ENTRIES (MAX_INTERFACE*10) 

#define RMON_MAX_MTRX_ENTRIES 200 

#define RMON_CLI_MAX_CONSOLE  (23 * 81)

#define CLI_RM_CREATE_REQUEST 2
#define CLI_RM_VALID 1 
#define CLI_RM_INVALID 4 

/* Size of table header + maximum colunm width maximum number 
 * global configuration objects for RMON */

#define RMON_MAX_TOPNCTRL_TABLE    (CLI_MAX_HEADER_SIZE + \
                                    (CLI_MAX_COLUMN_WIDTH * \
                                    RMON_MAX_TOPNCTRL_ENTRIES))

#define RMON_MAX_TOPN_TABLE        (CLI_MAX_HEADER_SIZE + \
                                    (CLI_MAX_COLUMN_WIDTH * \
                                    RMON_MAX_TOPN_ENTRIES))

#define RMON_MAX_HOSTCTRL_TABLE    (CLI_MAX_HEADER_SIZE + \
                                    (CLI_MAX_COLUMN_WIDTH * \
                                    RMON_MAX_HOSTCTRL_ENTRIES))

#define RMON_MAX_HOST_TABLE        (CLI_MAX_HEADER_SIZE + \
                                    (CLI_MAX_COLUMN_WIDTH * \
                                    RMON_MAX_HOST_ENTRIES))

#define RMON_MAX_MTRXCTRL_TABLE    (CLI_MAX_HEADER_SIZE + \
                                    (CLI_MAX_COLUMN_WIDTH * \
                                    RMON_MAX_MTRXCTRL_ENTRIES))

#define RMON_MAX_MTRX_TABLE        (CLI_MAX_HEADER_SIZE + \
                                    (CLI_MAX_COLUMN_WIDTH * \
                                    RMON_MAX_MTRX_ENTRIES))

#define CLI_OWN_STR_LEN             127 
#define CLI_RM_OBJECT_NAME_LEN      255
#define RMON_DESC_LEN               127 
#define COMMUNITY_STR_LEN           127

/* To define the structures to hold the command input arguments */
typedef struct
{
   INT4  i4EtherStatsIndex;  /* Table Index */
   INT4  i4RmonEtherPortOrVlanIndex; /* IfIndex/VLAN ID to be configured */
   INT4  i4RmonEthIndexType; /* INDEX Type of the entry */
   INT4  i4RmonEtherStatus1; /* Initial status of entry */
   INT4  i4RmonEtherStatus2; /* Final status of entry */
   UINT1 au1RmonEtherOwner[CLI_OWN_STR_LEN]; /* Control entry owner */
   UINT1 u1Reserved[1];
}tRmonEthStatsEntry;

typedef struct
{
   INT4  i4HistoryControlIndex; /* HistoryControl Table Index */
   INT4  i4RmonEtherPortOrVlanIndex; /* Interface Index/VLAN ID to be configured */
   INT4  i4RmonEthHisIndexType; /* INDEX Type of the entry */
   INT4  i4RmonEthHistBucketsReqstd; /* No. of HistoryBuckets Requested */
   INT4  i4RmonEthHistCtrlInterval; /* History sample interval */
   INT4  i4RmonEthHistCtrlStatus1; /* Initial Status of entry */
   INT4  i4RmonEthHistCtrlStatus2; /* Final status of entry */
   UINT1 au1RmonEthHistCtrlOwner[CLI_OWN_STR_LEN]; /* Control entry Owner */
   UINT1 u1Reserved[1];
}tRmonEthHistCtrlEntry;

typedef struct
{
   INT4  i4AlarmIndex; /* Alarm Table Index */
   INT4 i4RmonAlmInterval; /* Alarm Sample Interval */
   INT4 i4RmonAlmSampleType; /* Alarm Sample Type [Delta/Absolute] */
   INT4 i4RmonAlmStartupAlm; /* StartUp Alarm [Rising/Falling/Rising&Falling] */
   INT4 i4RmonAlmRiseThrshd; /* Alarm Rising Threshold Value */
   INT4 i4RmonAlmFallThrshd; /* Alarm Falling Threshold Value */
   INT4 i4RmonAlmRiseEvtIndex; /* Alarm Rising Event index */
   INT4  i4RmonAlmFallEvtIndex; /* Alarm Falling Event index */
   INT4 i4RmonAlmStatus1; /* Initial Status of entry */
   INT4 i4RmonAlmStatus2; /* Final Status of entry */
   UINT1 au1RmonAlmVariable[CLI_RM_OBJECT_NAME_LEN]; /* Alarm Variable - length 256 */
   UINT1 au1RmonAlmOwner[CLI_OWN_STR_LEN]; /* Alarm Entry Owner */
   UINT1 u1Reserved[2];
}tRmonAlarmEntry;

typedef struct
{
   INT4  i4HostControlIndex; /* Host Control Table Index */
   INT4  i4RmonHostIfIndex; /* Host Data Source - IfIndex */
   INT4 i4RmonHostCtrlStatus1; /* Initial Status of Entry */
   INT4 i4RmonHostCtrlStatus2; /* Final Status of Entry */
   UINT1 au1RmonHostCtrlOwner[CLI_OWN_STR_LEN]; /* Host Control Entry Owner */
   UINT1 u1Reserved[1];
}tRmonHostCtrlEntry;

typedef struct
{
   INT4  i4HostTopNCtrlIndex; /* HostTopN Control Table Index */
   INT4  i4RmonTopNCtrlIfIndex; /* Host Control Index - data Source */
   INT4 i4RmonTopNTimeRemain; /* TopN remaining Time */
   INT4  i4RmonTopNReqSize; /* TopNTable size */
   INT4 i4RmonTopNCtrlStatus1; /* Initial Status of Entry */
   INT4 i4RmonTopNCtrlStatus2; /* Final Status of Entry */
   INT4  i4RmonTopNRateBase; /* The Host Statistical Variable to be reported. */
   UINT1 au1RmonTopNCtrlOwner[CLI_OWN_STR_LEN]; /* TopN Control Entry Owner */
   UINT1 u1Reserved[1];
}tRmonTopNCtrlEntry;

typedef struct
{
   INT4  i4MatrixCtrlIndex; /* MatrixControl Table Index */
   INT4  i4RmonMatrixCtrlIfIndex; /* Matrix Control Data Source */
   INT4 i4RmonMatrixCtrlStatus1; /* Initial Status of entry */
   INT4 i4RmonMatrixCtrlStatus2; /* Final Status of entry */
   UINT1 au1RmonMatrixCtrlOwner[CLI_OWN_STR_LEN]; /* Matrix Control entry owner */
   UINT1 u1Reserved[1];
}tRmonMatrixCtrlEntry;

typedef struct
{
   INT4  i4EventIndex; /* Event Table Index */
   INT4 i4RmonEventStatus1; /* Initial Status of Event entry */
   INT4 i4RmonEventStatus2; /* Final status of Event entry */
   INT4 i4RmonEventType; /* Event Type - log/trap/log & trap */
   INT4  i4RmonEventCommLen; /* Length of Event Community */
   UINT1 au1RmonEventCommunity[COMMUNITY_STR_LEN]; /* Event Community */
   UINT1 au1RmonEventDescr[RMON_DESC_LEN]; /*Event Description Length */
   UINT1 au1RmonEventOwner[CLI_OWN_STR_LEN]; /* Event Entry Owner */
   UINT1 u1Reserved[3];
}tRmonEventEntry;


/* Error codes */
enum {
    CLI_RMON_CONFIG_ERR = 1,
    CLI_RMON_DISABLED_ERR,
    CLI_RMON_ALARM_LENGTH_ERR,
    CLI_RMON_ALARM_EXISTS_ERR,
    CLI_RMON_ALARM_INVALID_MIBID_ERR,
    CLI_RMON_INVALID_STATUS_ERR,
    CLI_RMON_INVALID_ALARM_INDEX_ERR,
    CLI_RMON_EVENT_ENTRY_EXISTS_ERR,
    CLI_RMON_NVT_CHAR_ERR,
    CLI_RMON_EVENT_NOTEXISTS_ERR,
    CLI_RMON_INVALID_HISTORY_INDEX_ERR,
    CLI_RMON_STATS_ENTRY_EXISTS_ERR,
    CLI_RMON_INVALID_STATS_INDEX_ERR,
    CLI_RMON_STATS_CONFIGURED_ERR,
    CLI_RMON_HISTORY_ENTRY_NOTEXISTS_ERR,
    CLI_RMON_HISTORY_INDEX_EXISTS_ERR,
    CLI_RMON_HISTORY_DUPLICATE_ENTRY_EXISTS_ERR,
    CLI_RMON_EVENT_NOT_CONFIGURED_ERR,
    CLI_RMON_DEPENDENT_ALARM,
    CLI_RMON_STATS_UNMATCHING_INTERFACE_OR_VLAN_ERR,
    CLI_RMON_HIST_UNMATCHING_INTERFACE_OR_VLAN_ERR,
    CLI_RMON_SNMP_COMMUNITY_ABSENT,
    CLI_RMON_INVALID_COMMUNITY,
    CLI_RMON_INVALID_OID,
    CLI_RMON_INVALID_CONTEXT,
    CLI_RMON_MAX_HISTORY,
    CLI_RMON_MAX_ALARM,
    CLI_RMON_MAX_EVENT,
    CLI_RMON_SET_EVENT,
    CLI_RMON_SET_COLLECTION_STAT,
    CLI_RMON_SET_COLLECTION_HIST,
    CLI_RMON_INVALID_EVENT_INDEX,
    CLI_RMON_MEMORY_ALLOCATION_FAILURE,
    CLI_RMON_ALARM_ENTRY_NOTEXISTS_ERR,
    CLI_RMON_ALARM_NOT_CONFIGURED,  
    CLI_RMON_EVENT_ENTRY_MEM_ALLOC_FAIL,
    CLI_RMON_MAX_ERR
};     

#ifdef __RMONCLI_C__

CONST CHR1 *RmonCliErrString [] = { 
    NULL,
    "\r% FATAL ERROR: command failed\r\n",
    "\r% RMON feature is disabled\r\n",
    "\r% Length of alarm variable exceeds limit\r\n",
    "\r% Alarm entry with this index already exists\r\n",
    "\r% Invalid MIB object\r\n",
    "\r% Invalid status\r\n",
    "\r% Alarm entry with this index does not exist\r\n",
    "\r% Event entry with this index already exists\r\n",
    "\r% Invalid characters are present in the description\r\n",
    "\r% Event entry with this index does not exist\r\n",
    "\r% Invalid history index\r\n",
    "\r% Statistics entry with this index already exists\r\n",
    "\r% Statistics entry with this index does not exist\r\n",
    "\r% Cannot configure more than one statistic collection per interface or vlan\r\n",
    "\r% History entry with this index does not exist\r\n",
    "\r% History entry with this index already exists\r\n",
    "\r% Entries of this History Index are already mapped to other index\r\n",
    "\r% The specified events have not been configured\r\n",
    "\r% This event cannot be deleted since dependent alarm entry exists\r\n",
    "\r% StatsIndex not matching the Current Interface or Vlan\r\n",
    "\r% History Index not matching the Current Interface or Vlan\r\n",
    "\r% Specified SNMP Community is not active\r\n",
    "\r% Invalid Community name.\r\n",
    "\r% Invalid Interface Index or Vlan Id\r\n",
    "\r% Invalid context\r\n",
    "\r% Max history count reached\r\n",
    "\r% Max alarm count reached\r\n",
    "\r% Max RMON event table entries reached.\r\n",
    "\r% Event configuration set failed \r\n",
    "\r% Collection statistics configuration set failed \r\n",
    "\r% Collection history configuration set failed \r\n",
    "\r% Invalid Event index\r\n",
    "\r% Memory Allocation failure in RMON\r\n",
    "\r% Alarm entry does not exist\r\n",
    "\r% Alarm Variable is not configured.\r\n",
    "\r% Memory Allocation Failed for the Event entry.\r\n"
};

#endif

/* Prototype declarations for RMON commands */



VOID RmonFormatStatus PROTO ((tCliHandle, INT4));
INT4 RmonShowAlarms PROTO ((tCliHandle));
INT4 RmonShowEvents PROTO ((tCliHandle));
INT4 RmonShowRunningConfig (tCliHandle,UINT4);
INT4 RmonShowRunningConfigGlobal (tCliHandle);
INT4 RmonShowRunningConfigInterface (tCliHandle,UINT1,INT4);
INT4 RmonShowRunningConfigVlan (tCliHandle,UINT1,UINT4);
INT4 RmonShowHistoryIndex PROTO ((tCliHandle, INT4, UINT1));
INT4 RmonShowHistoryTable PROTO ((tCliHandle, UINT1));  
INT4 RmonShowStatisticsIndex PROTO ((tCliHandle, INT4));
INT4 RmonShowStatisticsTable PROTO ((tCliHandle));
INT4 RmonSetStatus PROTO ((tCliHandle, INT4));
INT4 RmonSetAlarm PROTO ((tCliHandle,  tRmonAlarmEntry *));
INT4 RmonSetNoAlarm PROTO ((tCliHandle, INT4));
INT4 RmonSetEvent PROTO ((tCliHandle, tRmonEventEntry *));
INT4 RmonSetNoEvent PROTO ((tCliHandle, INT4));
INT4 RmonSetCollectionHistory PROTO ((tCliHandle, tRmonEthHistCtrlEntry *));
INT4 RmonSetNoCollectionHistory PROTO ((tCliHandle, tRmonEthHistCtrlEntry *));
INT4 RmonSetCollectionStats PROTO ((tCliHandle, tRmonEthStatsEntry *));
INT4 RmonSetNoCollectionStats PROTO ((tCliHandle , tRmonEthStatsEntry *));



INT4 cli_process_rmon_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Comamnd, ...));



#endif  /* _RMONCLI_H */
