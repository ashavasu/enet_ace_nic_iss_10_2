/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fshttpcli.h,v 1.2 2013/10/28 09:51:04 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsHttpRedirectionStatus[12];
extern UINT4 FsConfigHttpAuthScheme[12];
extern UINT4 FsHttpRequestCount[12];
extern UINT4 FsHttpRequestDiscards[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsHttpRedirectionStatus(i4SetValFsHttpRedirectionStatus) \
 nmhSetCmn(FsHttpRedirectionStatus, 12, FsHttpRedirectionStatusSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsHttpRedirectionStatus)
#define nmhSetFsConfigHttpAuthScheme(i4SetValFsConfigHttpAuthScheme) \
 nmhSetCmn(FsConfigHttpAuthScheme, 12, FsConfigHttpAuthSchemeSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsConfigHttpAuthScheme)
#define nmhSetFsHttpRequestCount(i4SetValFsHttpRequestCount) \
 nmhSetCmn(FsHttpRequestCount, 12, FsHttpRequestCountSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsHttpRequestCount)
#define nmhSetFsHttpRequestDiscards(i4SetValFsHttpRequestDiscards) \
 nmhSetCmn(FsHttpRequestDiscards, 12, FsHttpRequestDiscardsSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsHttpRequestDiscards)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsHttpRedirectionURL[14];
extern UINT4 FsHttpRedirectedSrvAddrType[14];
extern UINT4 FsHttpRedirectedSrvIP[14];
extern UINT4 FsHttpRedirectedSrvDomainName[14];
extern UINT4 FsHttpRedirectionEntryStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsHttpRedirectedSrvAddrType(pFsHttpRedirectionURL ,i4SetValFsHttpRedirectedSrvAddrType) \
 nmhSetCmn(FsHttpRedirectedSrvAddrType, 14, FsHttpRedirectedSrvAddrTypeSet, NULL, NULL, 0, 0, 1, "%s %i", pFsHttpRedirectionURL ,i4SetValFsHttpRedirectedSrvAddrType)
#define nmhSetFsHttpRedirectedSrvIP(pFsHttpRedirectionURL ,pSetValFsHttpRedirectedSrvIP) \
 nmhSetCmn(FsHttpRedirectedSrvIP, 14, FsHttpRedirectedSrvIPSet, NULL, NULL, 0, 0, 1, "%s %s", pFsHttpRedirectionURL ,pSetValFsHttpRedirectedSrvIP)
#define nmhSetFsHttpRedirectedSrvDomainName(pFsHttpRedirectionURL ,pSetValFsHttpRedirectedSrvDomainName) \
 nmhSetCmn(FsHttpRedirectedSrvDomainName, 14, FsHttpRedirectedSrvDomainNameSet, NULL, NULL, 0, 0, 1, "%s %s", pFsHttpRedirectionURL ,pSetValFsHttpRedirectedSrvDomainName)
#define nmhSetFsHttpRedirectionEntryStatus(pFsHttpRedirectionURL ,i4SetValFsHttpRedirectionEntryStatus) \
 nmhSetCmn(FsHttpRedirectionEntryStatus, 14, FsHttpRedirectionEntryStatusSet, NULL, NULL, 0, 1, 1, "%s %i", pFsHttpRedirectionURL ,i4SetValFsHttpRedirectionEntryStatus)

#endif
