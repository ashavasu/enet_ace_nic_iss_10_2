/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpfrcli.h,v 1.3 2013/12/11 10:07:30 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsFrrRevertiveMode[11];
extern UINT4 FsMplsFrrDetourMergingEnabled[11];
extern UINT4 FsMplsFrrDetourEnabled[11];
extern UINT4 FsMplsFrrCspfRetryInterval[11];
extern UINT4 FsMplsFrrCspfRetryCount[11];
extern UINT4 FsMplsFrrNotifsEnabled[11];
extern UINT4 FsMplsFrrMakeAfterBreakEnabled[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsFrrRevertiveMode(pSetValFsMplsFrrRevertiveMode) \
 nmhSetCmn(FsMplsFrrRevertiveMode, 11, FsMplsFrrRevertiveModeSet, NULL, NULL, 0, 0, 0, "%s", pSetValFsMplsFrrRevertiveMode)
#define nmhSetFsMplsFrrDetourMergingEnabled(i4SetValFsMplsFrrDetourMergingEnabled) \
 nmhSetCmn(FsMplsFrrDetourMergingEnabled, 11, FsMplsFrrDetourMergingEnabledSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsMplsFrrDetourMergingEnabled)
#define nmhSetFsMplsFrrDetourEnabled(i4SetValFsMplsFrrDetourEnabled) \
 nmhSetCmn(FsMplsFrrDetourEnabled, 11, FsMplsFrrDetourEnabledSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsMplsFrrDetourEnabled)
#define nmhSetFsMplsFrrCspfRetryInterval(i4SetValFsMplsFrrCspfRetryInterval) \
 nmhSetCmn(FsMplsFrrCspfRetryInterval, 11, FsMplsFrrCspfRetryIntervalSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsMplsFrrCspfRetryInterval)
#define nmhSetFsMplsFrrCspfRetryCount(u4SetValFsMplsFrrCspfRetryCount) \
 nmhSetCmn(FsMplsFrrCspfRetryCount, 11, FsMplsFrrCspfRetryCountSet, NULL, NULL, 0, 0, 0, "%u", u4SetValFsMplsFrrCspfRetryCount)
#define nmhSetFsMplsFrrNotifsEnabled(i4SetValFsMplsFrrNotifsEnabled) \
 nmhSetCmn(FsMplsFrrNotifsEnabled, 11, FsMplsFrrNotifsEnabledSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsMplsFrrNotifsEnabled)
#define nmhSetFsMplsFrrMakeAfterBreakEnabled(i4SetValFsMplsFrrMakeAfterBreakEnabled) \
 nmhSetCmn(FsMplsFrrMakeAfterBreakEnabled, 11, FsMplsFrrMakeAfterBreakEnabledSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsMplsFrrMakeAfterBreakEnabled)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsFrrConstProtectionMethod[14];
extern UINT4 FsMplsFrrConstProtectionType[14];
extern UINT4 FsMplsFrrConstSetupPrio[14];
extern UINT4 FsMplsFrrConstHoldingPrio[14];
extern UINT4 FsMplsFrrConstSEStyle[14];
extern UINT4 FsMplsFrrConstInclAnyAffinity[14];
extern UINT4 FsMplsFrrConstInclAllAffinity[14];
extern UINT4 FsMplsFrrConstExclAnyAffinity[14];
extern UINT4 FsMplsFrrConstHopLimit[14];
extern UINT4 FsMplsFrrConstBandwidth[14];
extern UINT4 FsMplsFrrConstRowStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsFrrConstProtectionMethod(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsFrrConstProtectionMethod) \
 nmhSetCmn(FsMplsFrrConstProtectionMethod, 14, FsMplsFrrConstProtectionMethodSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsFrrConstProtectionMethod)
#define nmhSetFsMplsFrrConstProtectionType(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsFrrConstProtectionType) \
 nmhSetCmn(FsMplsFrrConstProtectionType, 14, FsMplsFrrConstProtectionTypeSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsFrrConstProtectionType)
#define nmhSetFsMplsFrrConstSetupPrio(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,u4SetValFsMplsFrrConstSetupPrio) \
 nmhSetCmn(FsMplsFrrConstSetupPrio, 14, FsMplsFrrConstSetupPrioSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %u", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,u4SetValFsMplsFrrConstSetupPrio)
#define nmhSetFsMplsFrrConstHoldingPrio(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,u4SetValFsMplsFrrConstHoldingPrio) \
 nmhSetCmn(FsMplsFrrConstHoldingPrio, 14, FsMplsFrrConstHoldingPrioSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %u", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,u4SetValFsMplsFrrConstHoldingPrio)
#define nmhSetFsMplsFrrConstSEStyle(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsFrrConstSEStyle) \
 nmhSetCmn(FsMplsFrrConstSEStyle, 14, FsMplsFrrConstSEStyleSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsFrrConstSEStyle)
#define nmhSetFsMplsFrrConstInclAnyAffinity(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,u4SetValFsMplsFrrConstInclAnyAffinity) \
 nmhSetCmn(FsMplsFrrConstInclAnyAffinity, 14, FsMplsFrrConstInclAnyAffinitySet, NULL, NULL, 0, 0, 4, "%u %u %u %u %u", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,u4SetValFsMplsFrrConstInclAnyAffinity)
#define nmhSetFsMplsFrrConstInclAllAffinity(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,u4SetValFsMplsFrrConstInclAllAffinity) \
 nmhSetCmn(FsMplsFrrConstInclAllAffinity, 14, FsMplsFrrConstInclAllAffinitySet, NULL, NULL, 0, 0, 4, "%u %u %u %u %u", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,u4SetValFsMplsFrrConstInclAllAffinity)
#define nmhSetFsMplsFrrConstExclAnyAffinity(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,u4SetValFsMplsFrrConstExclAnyAffinity) \
 nmhSetCmn(FsMplsFrrConstExclAnyAffinity, 14, FsMplsFrrConstExclAnyAffinitySet, NULL, NULL, 0, 0, 4, "%u %u %u %u %u", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,u4SetValFsMplsFrrConstExclAnyAffinity)
#define nmhSetFsMplsFrrConstHopLimit(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,u4SetValFsMplsFrrConstHopLimit) \
 nmhSetCmn(FsMplsFrrConstHopLimit, 14, FsMplsFrrConstHopLimitSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %u", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,u4SetValFsMplsFrrConstHopLimit)
#define nmhSetFsMplsFrrConstBandwidth(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,u4SetValFsMplsFrrConstBandwidth) \
 nmhSetCmn(FsMplsFrrConstBandwidth, 14, FsMplsFrrConstBandwidthSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %u", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,u4SetValFsMplsFrrConstBandwidth)
#define nmhSetFsMplsFrrConstRowStatus(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsFrrConstRowStatus) \
 nmhSetCmn(FsMplsFrrConstRowStatus, 14, FsMplsFrrConstRowStatusSet, NULL, NULL, 0, 1, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsFrrConstRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsTunnelExtMaxGblRevertTime[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsTunnelExtMaxGblRevertTime(u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsTunnelExtMaxGblRevertTime) \
 nmhSetCmn(FsMplsTunnelExtMaxGblRevertTime, 14, FsMplsTunnelExtMaxGblRevertTimeSet, NULL, NULL, 0, 0, 4, "%u %u %u %u %i", u4MplsTunnelIndex , u4MplsTunnelInstance , u4MplsTunnelIngressLSRId , u4MplsTunnelEgressLSRId ,i4SetValFsMplsTunnelExtMaxGblRevertTime)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMplsBypassTunnelIfIndex[14];
extern UINT4 FsMplsBypassTunnelIndex[14];
extern UINT4 FsMplsBypassTunnelIngressLSRId[14];
extern UINT4 FsMplsBypassTunnelEgressLSRId[14];
extern UINT4 FsMplsBypassTunnelRowStatus[14];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsMplsBypassTunnelRowStatus(i4FsMplsBypassTunnelIfIndex , u4FsMplsBypassTunnelIndex , u4FsMplsBypassTunnelIngressLSRId , u4FsMplsBypassTunnelEgressLSRId ,i4SetValFsMplsBypassTunnelRowStatus) \
 nmhSetCmn(FsMplsBypassTunnelRowStatus, 14, FsMplsBypassTunnelRowStatusSet, NULL, NULL, 0, 1, 4, "%i %u %u %u %i", i4FsMplsBypassTunnelIfIndex , u4FsMplsBypassTunnelIndex , u4FsMplsBypassTunnelIngressLSRId , u4FsMplsBypassTunnelEgressLSRId ,i4SetValFsMplsBypassTunnelRowStatus)

#endif
