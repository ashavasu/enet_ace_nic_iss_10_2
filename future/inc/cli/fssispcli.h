/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssispcli.h,v 1.2 2009/09/24 14:29:58 prabuc Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsSispSystemControl[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsSispSystemControl(i4SetValFsSispSystemControl)	\
	nmhSetCmnWithLock(FsSispSystemControl, 11, FsSispSystemControlSet, VcmConfLock, VcmConfUnLock, 0, 0, 0, "%i", i4SetValFsSispSystemControl)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsSispPortIndex[13];
extern UINT4 FsSispPortCtrlStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsSispPortCtrlStatus(i4FsSispPortIndex ,i4SetValFsSispPortCtrlStatus)	\
	nmhSetCmnWithLock(FsSispPortCtrlStatus, 13, FsSispPortCtrlStatusSet, VcmConfLock, VcmConfUnLock, 0, 0, 1, "%i %i", i4FsSispPortIndex ,i4SetValFsSispPortCtrlStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsSispPortMapContextId[13];
extern UINT4 FsSispPortMapSharedPort[13];
extern UINT4 FsSispPortMapRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsSispPortMapSharedPort(i4FsSispPortIndex , i4FsSispPortMapContextId ,i4SetValFsSispPortMapSharedPort)	\
	nmhSetCmnWithLock(FsSispPortMapSharedPort, 13, FsSispPortMapSharedPortSet, VcmConfLock, VcmConfUnLock, 0, 0, 2, "%i %i %i", i4FsSispPortIndex , i4FsSispPortMapContextId ,i4SetValFsSispPortMapSharedPort)
#define nmhSetFsSispPortMapRowStatus(i4FsSispPortIndex , i4FsSispPortMapContextId ,i4SetValFsSispPortMapRowStatus)	\
	nmhSetCmnWithLock(FsSispPortMapRowStatus, 13, FsSispPortMapRowStatusSet, VcmConfLock, VcmConfUnLock, 0, 1, 2, "%i %i %i", i4FsSispPortIndex , i4FsSispPortMapContextId ,i4SetValFsSispPortMapRowStatus)

#endif
