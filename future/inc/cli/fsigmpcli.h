/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsigmpcli.h,v 1.7 2016/06/24 09:42:21 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsIgmpGlobalStatus[10];
extern UINT4 FsIgmpTraceLevel[10];
extern UINT4 FsIgmpDebugLevel[10];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsIgmpInterfaceIfIndex[12];
extern UINT4 FsIgmpInterfaceAdminStatus[12];
extern UINT4 FsIgmpInterfaceFastLeaveStatus[12];
extern UINT4 FsIgmpInterfaceChannelTrackStatus[12];
extern UINT4 FsIgmpInterfaceGroupListId[12];
extern UINT4 FsIgmpInterfaceLimit[12];
extern UINT4 FsIgmpInterfaceJoinPktRate[12];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsIgmpGrpListId[12];
extern UINT4 FsIgmpGrpIP[12];
extern UINT4 FsIgmpGrpPrefixLen[12];
extern UINT4 FsIgmpGrpListRowStatus[12];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsIgmpGlobalLimit[11];
extern UINT4 FsIgmpSSMMapStatus[11];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsIgmpSSMMapStartGrpAddress[12];
extern UINT4 FsIgmpSSMMapEndGrpAddress[12];
extern UINT4 FsIgmpSSMMapSourceAddress[12];
extern UINT4 FsIgmpSSMMapRowStatus[12];


