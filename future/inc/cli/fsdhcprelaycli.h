/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsdhcprelaycli.h,v 1.5 2017/12/14 10:23:43 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DhcpRelaying[10];
extern UINT4 DhcpRelayServersOnly[10];
extern UINT4 DhcpRelaySecsThreshold[10];
extern UINT4 DhcpRelayHopsThreshold[10];
extern UINT4 DhcpRelayRAIOptionControl[10];
extern UINT4 DhcpRelayRAICircuitIDSubOptionControl[10];
extern UINT4 DhcpRelayRAIRemoteIDSubOptionControl[10];
extern UINT4 DhcpRelayRAISubnetMaskSubOptionControl[10];
extern UINT4 DhcpConfigTraceLevel[10];
extern UINT4 DhcpConfigDhcpCircuitOption[10];
extern UINT4 DhcpRelayCounterReset[10];
extern UINT4 DhcpRelayRAIVPNIDSubOptionControl[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDhcpRelaying(i4SetValDhcpRelaying) \
 nmhSetCmn(DhcpRelaying, 10, DhcpRelayingSet, DhcpRProtocolLock, DhcpRProtocolUnlock, 0, 0, 0, "%i", i4SetValDhcpRelaying)
#define nmhSetDhcpRelayServersOnly(i4SetValDhcpRelayServersOnly) \
 nmhSetCmn(DhcpRelayServersOnly, 10, DhcpRelayServersOnlySet, DhcpRProtocolLock, DhcpRProtocolUnlock, 0, 0, 0, "%i", i4SetValDhcpRelayServersOnly)
#define nmhSetDhcpRelaySecsThreshold(i4SetValDhcpRelaySecsThreshold) \
 nmhSetCmn(DhcpRelaySecsThreshold, 10, DhcpRelaySecsThresholdSet, DhcpRProtocolLock, DhcpRProtocolUnlock, 0, 0, 0, "%i", i4SetValDhcpRelaySecsThreshold)
#define nmhSetDhcpRelayHopsThreshold(i4SetValDhcpRelayHopsThreshold) \
 nmhSetCmn(DhcpRelayHopsThreshold, 10, DhcpRelayHopsThresholdSet, DhcpRProtocolLock, DhcpRProtocolUnlock, 0, 0, 0, "%i", i4SetValDhcpRelayHopsThreshold)
#define nmhSetDhcpRelayRAIOptionControl(i4SetValDhcpRelayRAIOptionControl) \
 nmhSetCmn(DhcpRelayRAIOptionControl, 10, DhcpRelayRAIOptionControlSet, DhcpRProtocolLock, DhcpRProtocolUnlock, 0, 0, 0, "%i", i4SetValDhcpRelayRAIOptionControl)
#define nmhSetDhcpRelayRAICircuitIDSubOptionControl(i4SetValDhcpRelayRAICircuitIDSubOptionControl) \
 nmhSetCmn(DhcpRelayRAICircuitIDSubOptionControl, 10, DhcpRelayRAICircuitIDSubOptionControlSet, DhcpRProtocolLock, DhcpRProtocolUnlock, 0, 0, 0, "%i", i4SetValDhcpRelayRAICircuitIDSubOptionControl)
#define nmhSetDhcpRelayRAIRemoteIDSubOptionControl(i4SetValDhcpRelayRAIRemoteIDSubOptionControl) \
 nmhSetCmn(DhcpRelayRAIRemoteIDSubOptionControl, 10, DhcpRelayRAIRemoteIDSubOptionControlSet, DhcpRProtocolLock, DhcpRProtocolUnlock, 0, 0, 0, "%i", i4SetValDhcpRelayRAIRemoteIDSubOptionControl)
#define nmhSetDhcpRelayRAISubnetMaskSubOptionControl(i4SetValDhcpRelayRAISubnetMaskSubOptionControl) \
 nmhSetCmn(DhcpRelayRAISubnetMaskSubOptionControl, 10, DhcpRelayRAISubnetMaskSubOptionControlSet, DhcpRProtocolLock, DhcpRProtocolUnlock, 0, 0, 0, "%i", i4SetValDhcpRelayRAISubnetMaskSubOptionControl)
#define nmhSetDhcpConfigTraceLevel(i4SetValDhcpConfigTraceLevel) \
 nmhSetCmn(DhcpConfigTraceLevel, 10, DhcpConfigTraceLevelSet, DhcpRProtocolLock, DhcpRProtocolUnlock, 0, 0, 0, "%i", i4SetValDhcpConfigTraceLevel)
#define nmhSetDhcpConfigDhcpCircuitOption(pSetValDhcpConfigDhcpCircuitOption) \
 nmhSetCmn(DhcpConfigDhcpCircuitOption, 10, DhcpConfigDhcpCircuitOptionSet, DhcpRProtocolLock, DhcpRProtocolUnlock, 0, 0, 0, "%s", pSetValDhcpConfigDhcpCircuitOption)
#define nmhSetDhcpRelayCounterReset(i4SetValDhcpRelayCounterReset) \
 nmhSetCmn(DhcpRelayCounterReset, 10, DhcpRelayCounterResetSet, DhcpRProtocolLock, DhcpRProtocolUnlock, 0, 0, 0, "%i", i4SetValDhcpRelayCounterReset)
#define nmhSetDhcpRelayRAIVPNIDSubOptionControl(i4SetValDhcpRelayRAIVPNIDSubOptionControl) \
 nmhSetCmn(DhcpRelayRAIVPNIDSubOptionControl, 10, DhcpRelayRAIVPNIDSubOptionControlSet, DhcpRProtocolLock, DhcpRProtocolUnlock, 0, 0, 0, "%i", i4SetValDhcpRelayRAIVPNIDSubOptionControl)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DhcpRelaySrvIpAddress[12];
extern UINT4 DhcpRelaySrvAddressRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDhcpRelaySrvAddressRowStatus(u4DhcpRelaySrvIpAddress ,i4SetValDhcpRelaySrvAddressRowStatus) \
 nmhSetCmn(DhcpRelaySrvAddressRowStatus, 12, DhcpRelaySrvAddressRowStatusSet, DhcpRProtocolLock, DhcpRProtocolUnlock, 0, 1, 1, "%p %i", u4DhcpRelaySrvIpAddress ,i4SetValDhcpRelaySrvAddressRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DhcpRelayIfCircuitId[12];
extern UINT4 DhcpRelayIfRemoteId[12];
extern UINT4 DhcpRelayIfRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDhcpRelayIfCircuitId(i4IfIndex ,u4SetValDhcpRelayIfCircuitId) \
 nmhSetCmn(DhcpRelayIfCircuitId, 12, DhcpRelayIfCircuitIdSet, DhcpRProtocolLock, DhcpRProtocolUnlock, 0, 0, 1, "%i %u", i4IfIndex ,u4SetValDhcpRelayIfCircuitId)
#define nmhSetDhcpRelayIfRemoteId(i4IfIndex ,pSetValDhcpRelayIfRemoteId) \
 nmhSetCmn(DhcpRelayIfRemoteId, 12, DhcpRelayIfRemoteIdSet, DhcpRProtocolLock, DhcpRProtocolUnlock, 0, 0, 1, "%i %s", i4IfIndex ,pSetValDhcpRelayIfRemoteId)
#define nmhSetDhcpRelayIfRowStatus(i4IfIndex ,i4SetValDhcpRelayIfRowStatus) \
 nmhSetCmn(DhcpRelayIfRowStatus, 12, DhcpRelayIfRowStatusSet, DhcpRProtocolLock, DhcpRProtocolUnlock, 0, 1, 1, "%i %i", i4IfIndex ,i4SetValDhcpRelayIfRowStatus)

#endif
