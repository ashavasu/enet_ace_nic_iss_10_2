 /* $Id: msdpcli.h,v 1.9 2013/09/02 13:59:21 siva Exp $ */
#ifndef MSDPCLI_H
#define MSDPCLI_H
#include "lr.h"
#include "cli.h"

#define CLI_MSDP_DISABLE   0
#define CLI_MSDP_ENABLE    1
#define CLI_PEER_ADMIN_ENABLE  1
#define CLI_PEER_ADMIN_DISABLE 2

PUBLIC INT4 Msd6PortGetFirstIfAddr (UINT4, UINT1 *);
INT4 MsdpCliGetTraceValue(VOID);
INT4 MsdpCliGetSARedistStatus (INT4 i4AddrType);
INT4 MsdpCliGetRPStatus (INT4 i4AddrType);
INT4 cli_process_Msdp_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);
INT4 cli_process_Msdp_Show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);
enum
{
 CLI_MSDP_FSMSDPTRACELEVEL,
 CLI_MSDP_FSMSDPIPV4ADMINSTAT,
 CLI_MSDP_FSMSDPIPV6ADMINSTAT,
 CLI_MSDP_FSMSDPCACHELIFETIME,
 CLI_MSDP_FSMSDPMAXPEERSESSIONS,
 CLI_MSDP_FSMSDPMAPPINGCOMPONENTID,
 CLI_MSDP_FSMSDPLISTENERPORT,
 CLI_MSDP_FSMSDPPEERFILTER,
 CLI_MSDP_FSMSDPPEERTABLE,
 CLI_MSDP_FSMSDPSACACHETABLE,
 CLI_MSDP_FSMSDPMESHGROUPTABLE,
 CLI_MSDP_FSMSDPRPTABLE,
 CLI_MSDP_FSMSDPPEERFILTERTABLE,
 CLI_MSDP_FSMSDPSAREDISTRIBUTIONTABLE,
        CLI_MSDP_CLEAR_PEER,
        CLI_MSDP_CLEAR_CACHE,
        CLI_MSDP_SHOW_SUMMARY,
        CLI_MSDP_SHOW_COUNT,
        CLI_MSDP_SHOW_CACHE,
        CLI_MSDP_SHOW_PEER,
        CLI_MSDP_SHOW_MESH,
        CLI_MSDP_SHOW_RPFPEER,
        CLI_MSDPV6_CLEAR_PEER,
        CLI_MSDPV6_CLEAR_CACHE,
        CLI_MSDPV6_SHOW_SUMMARY,
        CLI_MSDPV6_SHOW_COUNT,
        CLI_MSDPV6_SHOW_CACHE,
        CLI_MSDPV6_SHOW_PEER,
        CLI_MSDPV6_SHOW_MESH,
        CLI_MSDPV6_SHOW_RPFPEER,
        CLI_MSDP_DEBUG,
        CLI_MSDP_UT_TEST,
};

#define CLI_MSDP_PEER_TRC         1
#define CLI_MSDP_CACHE_TRC        2
#define CLI_MSDP_INPUT_TRC        4
#define CLI_MSDP_OUTPUT_TRC       8
#define CLI_MSDP_ALL_TRC          255

enum 
{
    MSDP_ERROR = 1,
    MSDP_IN_SAME_STATE,
    MSDP_STATUS_INVALID,
    CACHE_LIFETIME_INVALID,
    MAX_SESSIONS_INVALID,
    FILTER_ERROR,
    PEERS_COUNT_EXCEEDED,
    CONNECT_RETRY_INVALID,
    HOLDTIME_RANGE_INVALID,
    KEEPALIVE_RANGE_INVALID,
    DATA_TTL_INVALID,
    TYPE_INVALID,
    PEER_EXISTS,
    PEER_ALREADY_EXISTS,
    MESH_ALREADY_EXISTS,
    PEER_NOT_EXIST,
    RP_ALREADY_EXISTS,
    RP_EXISTS,
    RP_NOT_EXIST,
    MESH_EXISTS,
    MESH_NOT_EXIST,
    REDIST_NOT_EXIST,
    RANGE_INVALID,
    ROW_EXIST,
    ROW_NOT_EXIST,
    IP_INVALID,
    MSDP_PEER_ADMIN_ERROR,
    MSDP_INVALID_PEER_STAT,
    MSDP_INVALID_ROUTEMAP_NAME,
    CLI_MSDP_MAX_ERR
}; 

#if defined  (__MSDPCLIG_C__)
CONST CHR1  *MsdpCliErrString [] = {
        " \r\n",
        "%% MSDP is disabled.. No MSDP configuration is allowed\r\n",
        "%% MSDP is already in the same state.\r\n",
        "%% Admin Status is Invalid.Only Enable or Disable is allowed\r\n",
        "%% Cache Lifetime entered is below minimum value.\r\n",
        "%% Entered value is not within the allowed limit.\r\n",
        "%% Filter value should be either accept-all or deny-all.\r\n",
        "%% Maximum limit reached.Peer configuration is not allowed\r\n",
        "%% Connect Retry value is not valid.Violating the limits\r\n",
        "%% KeepAlive time is not valid.Violating the limits\r\n",
        "%% Peer Hold-time value is invalid.Violating the limits\r\n",
        "%% Data TTL entered is invalid.\r\n",
        "%% Encapsulation type error..Enter correct type\r\n",
        "%% Already a Peer exists with the same state\r\n",
        "%% Already the peer is created.Reconfigure by deleting the existing "
            "one\r\n",
        "%% Already the mesh is created.Reconfigure by deleting the existing "
            "one\r\n",
        "%% Peer is not configured.\r\n",
        "%% RP is already configured .Reconfigure by deleting the existing "
            "one\r\n",
        "%% RP is already configured with same status.\r\n",
        "%% No RP configured.\r\n",
        "%% Mesh is configured already with same status\r\n",
        "%% No Mesh is configured\r\n",
        "%% Redistribution not configured.\r\n",
        "%% Entered range is invalid\r\n",
        "%% A Row exists already\r\n",
        "%% Entry not available.\r\n",
        "%% Local IP Address not exist\r\n",
        "%% Unable to do the requested operation.\r\n",
        "%% Msdp Peer Admin Status is invalid\r\n",
        "%% Route Map name provided is invalid\r\n"
};
#else
PUBLIC CHR1* MsdpCliErrString;
#endif

VOID IssMsdpShowDebugging (tCliHandle);

#endif
