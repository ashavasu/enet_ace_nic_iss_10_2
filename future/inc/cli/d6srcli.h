/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 * $Id: d6srcli.h,v 1.9 2014/09/25 12:16:19 siva Exp $   
 * Description: This file contains macros to the CLI commands and structures 
 *              for the DHCP Server.
 * ************************************************************************/

#ifndef DHCP6_SRV_CLI_H__
#define DHCP6_SRV_CLI_H__

#include "cli.h"

/* DHCP6 CLI command constants : To add a new command, A new definition 
 * needs to  be added to the list.                                     */

INT4 
cli_process_dhcp6s_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Comamnd, ...));

VOID IssDhcp6ServerShowDebugging (tCliHandle);

enum
{
    CLI_DHCP6_SRV_POOL = 1,
    CLI_DHCP6_SRV_NO_POOL,
    CLI_DHCP6_SRV_AUTH_REALM_KEY_ID,
    CLI_DHCP6_SRV_AUTH_CLNT_ID,
    CLI_DHCP6_SRV_NO_AUTH_CLNT_ID,
    CLI_DHCP6_SRV_NO_AUTH_REALM_KEY_ID,
    CLI_DHCP6_SRV_ENABLE_TRAP,
    CLI_DHCP6_SRV_DISABLE_TRAP,
    CLI_DHCP6_SRV_VENDOR,
    CLI_DHCP6_SRV_NO_VENDOR,
    CLI_DHCP6_SRV_OPTION,
    CLI_DHCP6_SRV_NO_OPTION,
    CLI_DHCP6_SRV_LINK_ADDR,
    CLI_DHCP6_SRV_NO_LINK_ADDR,
    CLI_DHCP6_SRV_DOMAIN_NAME,
    CLI_DHCP6_SRV_NO_DOMAIN_NAME,
    CLI_DHCP6_SRV_DNS_SERVER,
    CLI_DHCP6_SRV_NO_DNS_SERVER,
    CLI_DHCP6_SRV_SIP_ADDR,
    CLI_DHCP6_SRV_NO_SIP_ADDR,
    CLI_DHCP6_SRV_SIP_DOMAIN,
    CLI_DHCP6_SRV_SIP_NO_DOMAIN,
    CLI_DHCP6_SRV_POOL_OPTION,
    CLI_DHCP6_SRV_POOL_NO_OPTION,
    CLI_DHCP6_SRV_TYPE,
    CLI_DHCP6_SRV_INTERFACE,
    CLI_DHCP6_SRV_REFRESH_TIME,
    CLI_DHCP6_SRV_NO_REFRESH_TIME,
    CLI_DHCP6_SRV_ENABLE,
    CLI_DHCP6_SRV_DISABLE,
    CLI_DHCP6_SRV_SHOW_POOL,
    CLI_DHCP6_SRV_SHOW_ALL_POOL,
    CLI_DHCP6_SRV_SHOW_INTERFACE_CONF,
    CLI_DHCP6_SRV_SHOW_ALL_INTERFACE_CONF,
    CLI_DHCP6_SRV_SHOW_GLOBAL_CONF,
    CLI_DHCP6_SRV_SHOW_STATS,
    CLI_DHCP6_SRV_SHOW_ALL_STATS,
    CLI_DHCP6_SRV_CLEAR_STATS,
    CLI_DHCP6_SRV_CLEAR_ALL_STATS,
    CLI_DHCP6_SRV_DEBUG,
    CLI_DHCP6_SRV_UDP_SOURCE_DEST_PORT,
    CLI_DHCP6_SRV_SYSLOG_STATUS,
    CLI_DHCP6_SRV_NO_DEBUG
};

#define DHCP6_SRV_CLI_INVALID_VALUE         -1

/* Command constants for option command */
#define DHCP6_SRV_OPTION_ASCII              1
#define DHCP6_SRV_OPTION_HEX                2
#define DHCP6_SRV_OPTION_IP                 3

#define DHCP6_SRV_CLI_MAX_ARGUMENTS         13

#define CLI_DHCP6_SRV_DUID_LLT              1
#define CLI_DHCP6_SRV_DUID_EN               2
#define CLI_DHCP6_SRV_DUID_LL               3

#define DHCP6_SRV_AUTH_OPTION_TYPE          11
#define DHCP6_SRV_STATUS_CODE_OPTION        13
#define DHCP6_SRV_VENDOR_SPECIFIC_OPTION    17
#define DHCP6_SRV_DNS_RECURSIVE_OPTION      23
#define DHCP6_SRV_DOMAIN_NAME_OPTION        24
#define DHCP6_SRV_SIP_SERVER_DOMAIN_OPTION  21
#define DHCP6_SRV_SIP_IPV6_ADDR_OPTION      22

#define DHCP6_SRV_CLI_MAX_ERR               10

#define DHCP6_SRV_SYSLOG_ENABLE             1
#define DHCP6_SRV_SYSLOG_DISABLE            2

#define DHCP6_SRV_REFRESH_TIME_FINITY       1
#define DHCP6_SRV_REFRESH_TIME_INFINITY     2

#define DHCP6_SRV_ALL_TRAP                  0x07
#define DHCP6_SRV_INVALID_PKT_IN_TRAP       0x02
#define DHCP6_SRV_AUTH_FAIL_TRAP            0x04
#define DHCP6_SRV_UNKNOWN_TLV_TRAP          0x01

#define DHCP6_SRV_TRC_MAX_SIZE              255

#define DHCP6_SRV_MAX_SERVER_OPTION_SIZE    255

#define DHCP6_SRV_UDP_RELAY_TRANS_PORT_NUM  1
#define DHCP6_SRV_UDP_REPLY_TRANS_PORT_NUM  2
#define DHCP6_SRV_UDP_LISTEN_PORT_NUM       3 
#define CLI_MODE_POOL_DHCP6_SRV             "dhcp6-pool-cfg"
#define CLI_MODE_VENDOR_DHCP6_SRV           "dhcp6-vendor-cfg"
#define CLI_MODE_CLNT_DHCP6_SRV             "dhcp6-clnt-cfg"

INT1 Dhcp6SGetPoolCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);
INT1 Dhcp6SGetVendorCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);
INT1 D6SrGetClientCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);
INT4 D6SrCliCreateAddressPool (tCliHandle CliHandle, UINT1 *pu1PoolName);
INT4 D6SrCliDestroyAddressPool (tCliHandle CliHandle, UINT1 *pu1PoolName);
INT4 D6SrCliCreateVendor (tCliHandle CliHandle, UINT4 u4VendorIndex,
                            UINT1 *pu1OptValue, UINT4 u4Len, UINT4 u4PoolIndex);
INT4 D6SrCliSetAuthInfo (tCliHandle CliHandle, UINT1 *pu1RealmName, 
                         UINT1 *pu1KeyName, UINT4 u4Index);
INT4 D6SrCliSetAuthRealmInfo (tCliHandle CliHandle, tSNMP_OCTET_STRING_TYPE *pu1RealmName, UINT4 u4RealmIndex);
INT4 D6SrCliSetAuthKeyInfo (tCliHandle CliHandle, UINT1 *pu1KeyName, 
                                UINT4 u4RealmIndex);
INT4 D6SrCliDeleteAuthInfo (tCliHandle CliHandle, UINT1 *pu1RealmName, 
                              UINT1 *pu1KeyName, UINT4);
INT4 D6SrCliProcessAddOption (tCliHandle CliHandle, UINT4 u4PoolIndex, 
                                UINT4 u4Type, UINT1 *pu1OptValue, UINT4 u4Len, 
                                INT4 i4Preference);
INT4 D6SrCliProcessDeleteOption (tCliHandle CliHandle, UINT4 u4PoolIndex, UINT4 u4Type, 
                                   UINT1 *pu1OptValue, UINT4 u4Len);
INT4 D6SrCliProcessLinkAddOption (tCliHandle CliHandle, UINT4 u4PoolIndex, 
                                    UINT1 *pu1OptValue, UINT4 u4Len);
INT4 D6SrCliProcessLinkDeleteOption (tCliHandle CliHandle, UINT4 u4PoolIndex, 
                                       UINT1 *pu1OptValue, UINT4 u4Len);
INT4 D6SrCliProcessPoolOption (tCliHandle CliHandle, UINT4 u4PoolIndex, 
                                 UINT4 u4Type, UINT1 *pu1OptValue, UINT4 u4Len, 
                                 INT4 i4Preference);
INT4 D6SrCliProcessAddSubOption (tCliHandle CliHandle, UINT4 u4PoolIndex, 
                                   UINT4 u4VendorIndex, UINT4 u4Type, 
                                   UINT1 *pu1OptValue, UINT4 u4Len);
INT4 D6SrCliProcessDeleteSubOption (tCliHandle CliHandle, UINT4 u4PoolIndex, 
                                      UINT4 u4VendorIndex, UINT4 u4Type, 
                                      UINT1 *pu1OptValue, UINT4 u4Len);
INT4 D6SrCliSetTraps (tCliHandle CliHandle, UINT4 u4TrapValue, UINT1 u1TrapFlag);
INT4 D6SrCliSetDuidType (tCliHandle CliHandle, INT4 u4PoolIndex, UINT4 u4DuidType);
INT4 D6SrCliSetDuidInterface (tCliHandle CliHandle, INT4 u4PoolIndex, INT4 i4IfIndex);
INT4 D6SrCliSetIfEnable (tCliHandle CliHandle, UINT4 u4IfIndex, UINT1 *pu1PoolName, 
                           UINT1 *pu1Preference);
INT4 D6SrCliSetIfDisable (tCliHandle CliHandle, UINT4 u4IfIndex);
INT4 D6SrCliSetTrace (tCliHandle CliHandle, UINT1 *pu1TraceInput);
INT4 D6SrCliSetSysLogStatus (tCliHandle CliHandle, INT4 i4SysLogStatus);

INT4 D6SrCliShowServerInfo (tCliHandle CliHandle);
INT4 D6SrCliShowAllPoolInfo (tCliHandle CliHandle);
INT4 D6SrCliShowAllInterfaceInfo (tCliHandle CliHandle);
INT4 D6SrCliShowAllInterfaceStats (tCliHandle CliHandle);
INT4 D6SrCliShowPoolInfo (tCliHandle CliHandle, UINT4);
INT4 D6SrCliShowInterfaceInfo (tCliHandle CliHandle, INT4);
INT4 D6SrCliShowInterfaceStats (tCliHandle CliHandle, INT4);
INT4 D6SrCliShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module);
INT4 D6SrCliShowRunningScalars (tCliHandle CliHandle);
INT4 D6SrCliShowRunningConfigTables (tCliHandle CliHandle);
VOID D6SrCliShowRunningConfigInterface (tCliHandle CliHandle);
VOID D6SrCliShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4IfIndex);
INT4 D6SrCliSetClntUdpPorts (tCliHandle CliHandle, UINT4 u4Type, UINT4 u4Value);
PUBLIC VOID D6SrCliShowGlobalInfo (tCliHandle CliHandle );
INT4 D6SrCliClearAllInterfaceInfo (tCliHandle CliHandle);
INT4 D6SrCliClearInterfaceInfo (tCliHandle CliHandle,INT4,UINT4);
INT4 D6SrCliSetClntInfo (tCliHandle CliHandle, UINT1 *pu1ClntName,UINT4 u4ClientType);
INT4 D6SrCliDelClntInfo (tCliHandle CliHandle, UINT1 *pu1ClntName);
INT4 D6SrCliShowSrvGlobalInfo (tCliHandle CliHandle);
INT4 D6SrCliShowIfInfo (tCliHandle CliHandle,INT4 i4Index);
INT4 D6SrCliShowPoolConf (tCliHandle CliHandle,INT1 *pu1PoolName );
INT4 D6SrCliSetRefresTime (tCliHandle CliHandle,UINT4,UINT4,UINT4,UINT4,UINT4 );
INT4 Dhcp6SCliProcessAddRefOption (tCliHandle CliHandle, UINT4 u4PoolIndex,
                                    UINT4 *pu4OptValue);
INT4 D6SrCliReSetRefresTime (tCliHandle CliHandle,UINT4 u4PoolIndex);


enum{
    DHCP6_SRV_CLI_ENTRY_EXISTS = 1,
    DHCP6_SRV_CLI_ERR_IP6,
    DHCP6_SRV_CLI_INV_IFTYPE,
    DHCP6_SRV_CLI_INV_ENTRY,
    DHCP6_SRV_CLI_INV_STATUS,
    DHCP6_SRV_CLI_ACT_ROW_STATUS,
    DHCP6_SRV_CLI_INFO_REFRESH_INV_LEN,
    DHCP6_SRV_CLI_INFO_REFRESH_INV_VAL,
    DHCP6_SRV_CLI_INV_DEBUG_STRING
};

#if defined (_DHCP6_SRV_CLI_C)
CONST CHR1 *gaD6SrCliErrString [] = {
    NULL,
    "% Entry already exists\r\n",
    "% Interface does not exist in lower layer\r\n",
    "% Invalid Interface type\r\n",
    "% Entry does not exist\r\n",
    "% Invalid status\r\n",
    "% Row status is active\r\n",
    "% Valid Refresh Timer Option length value is only four\r\n",
    "% Refresh Timer Option value should be equal to or more than 600 seconds\r\n",
     "% Please provide a valid debug string. \r\n",
    "\r\n"
};

#else
extern CONST CHR1  *gaD6SrCliErrString [];
#endif


#endif /* __DHCP6_SRV_CCLI_H__ */

