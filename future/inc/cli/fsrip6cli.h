/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsrip6cli.h,v 1.3 2009/10/28 08:57:50 prabuc Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsrip6RoutePreference[10];
extern UINT4 Fsrip6GlobalDebug[10];
extern UINT4 Fsrip6GlobalInstanceIndex[10];
extern UINT4 Fsrip6PeerFilter[10];
extern UINT4 Fsrip6AdvFilter[10];
extern UINT4 FsRip6RRDAdminStatus[10];
extern UINT4 Fsrip6RRDProtoMaskForEnable[10];
extern UINT4 Fsrip6RRDSrcProtoMaskForDisable[10];
extern UINT4 Fsrip6RRDRouteDefMetric[10];
extern UINT4 Fsrip6RRDRouteMapName[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsrip6RoutePreference(i4SetValFsrip6RoutePreference)	\
	nmhSetCmn(Fsrip6RoutePreference, 10, Fsrip6RoutePreferenceSet, Rip6Lock, Rip6UnLock, 0, 0, 0, "%i", i4SetValFsrip6RoutePreference)
#define nmhSetFsrip6GlobalDebug(i4SetValFsrip6GlobalDebug)	\
	nmhSetCmn(Fsrip6GlobalDebug, 10, Fsrip6GlobalDebugSet, Rip6Lock, Rip6UnLock, 0, 0, 0, "%i", i4SetValFsrip6GlobalDebug)
#define nmhSetFsrip6GlobalInstanceIndex(i4SetValFsrip6GlobalInstanceIndex)	\
	nmhSetCmn(Fsrip6GlobalInstanceIndex, 10, Fsrip6GlobalInstanceIndexSet, Rip6Lock, Rip6UnLock, 0, 0, 0, "%i", i4SetValFsrip6GlobalInstanceIndex)
#define nmhSetFsrip6PeerFilter(i4SetValFsrip6PeerFilter)	\
	nmhSetCmn(Fsrip6PeerFilter, 10, Fsrip6PeerFilterSet, Rip6Lock, Rip6UnLock, 0, 0, 0, "%i", i4SetValFsrip6PeerFilter)
#define nmhSetFsrip6AdvFilter(i4SetValFsrip6AdvFilter)	\
	nmhSetCmn(Fsrip6AdvFilter, 10, Fsrip6AdvFilterSet, Rip6Lock, Rip6UnLock, 0, 0, 0, "%i", i4SetValFsrip6AdvFilter)
#define nmhSetFsRip6RRDAdminStatus(i4SetValFsRip6RRDAdminStatus)	\
	nmhSetCmn(FsRip6RRDAdminStatus, 10, FsRip6RRDAdminStatusSet, Rip6Lock, Rip6UnLock, 0, 0, 0, "%i", i4SetValFsRip6RRDAdminStatus)
#define nmhSetFsrip6RRDProtoMaskForEnable(i4SetValFsrip6RRDProtoMaskForEnable)	\
	nmhSetCmn(Fsrip6RRDProtoMaskForEnable, 10, Fsrip6RRDProtoMaskForEnableSet, Rip6Lock, Rip6UnLock, 0, 0, 0, "%i", i4SetValFsrip6RRDProtoMaskForEnable)
#define nmhSetFsrip6RRDSrcProtoMaskForDisable(i4SetValFsrip6RRDSrcProtoMaskForDisable)	\
	nmhSetCmn(Fsrip6RRDSrcProtoMaskForDisable, 10, Fsrip6RRDSrcProtoMaskForDisableSet, Rip6Lock, Rip6UnLock, 0, 0, 0, "%i", i4SetValFsrip6RRDSrcProtoMaskForDisable)
#define nmhSetFsrip6RRDRouteDefMetric(i4SetValFsrip6RRDRouteDefMetric)	\
	nmhSetCmn(Fsrip6RRDRouteDefMetric, 10, Fsrip6RRDRouteDefMetricSet, Rip6Lock, Rip6UnLock, 0, 0, 0, "%i", i4SetValFsrip6RRDRouteDefMetric)
#define nmhSetFsrip6RRDRouteMapName(pSetValFsrip6RRDRouteMapName)	\
	nmhSetCmn(Fsrip6RRDRouteMapName, 10, Fsrip6RRDRouteMapNameSet, Rip6Lock, Rip6UnLock, 0, 0, 0, "%s", pSetValFsrip6RRDRouteMapName)
#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsrip6InstanceIndex[12];
extern UINT4 Fsrip6InstanceStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsrip6InstanceStatus(i4Fsrip6InstanceIndex ,i4SetValFsrip6InstanceStatus)	\
	nmhSetCmn(Fsrip6InstanceStatus, 12, Fsrip6InstanceStatusSet, Rip6Lock, Rip6UnLock, 0, 0, 1, "%i %i", i4Fsrip6InstanceIndex ,i4SetValFsrip6InstanceStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsrip6IfIndex[12];
extern UINT4 Fsrip6InstIfMapIfAtchStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsrip6InstIfMapIfAtchStatus(i4Fsrip6IfIndex ,i4SetValFsrip6InstIfMapIfAtchStatus)	\
	nmhSetCmn(Fsrip6InstIfMapIfAtchStatus, 12, Fsrip6InstIfMapIfAtchStatusSet, Rip6Lock, Rip6UnLock, 0, 0, 1, "%i %i", i4Fsrip6IfIndex ,i4SetValFsrip6InstIfMapIfAtchStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsrip6RipIfIndex[12];
extern UINT4 Fsrip6RipIfProfileIndex[12];
extern UINT4 Fsrip6RipIfCost[12];
extern UINT4 Fsrip6RipIfProtocolEnable[12];
extern UINT4 Fsrip6RipIfDefRouteAdvt[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsrip6RipIfProfileIndex(i4Fsrip6RipIfIndex ,i4SetValFsrip6RipIfProfileIndex)	\
	nmhSetCmn(Fsrip6RipIfProfileIndex, 12, Fsrip6RipIfProfileIndexSet, Rip6Lock, Rip6UnLock, 0, 0, 1, "%i %i", i4Fsrip6RipIfIndex ,i4SetValFsrip6RipIfProfileIndex)
#define nmhSetFsrip6RipIfCost(i4Fsrip6RipIfIndex ,i4SetValFsrip6RipIfCost)	\
	nmhSetCmn(Fsrip6RipIfCost, 12, Fsrip6RipIfCostSet, Rip6Lock, Rip6UnLock, 0, 0, 1, "%i %i", i4Fsrip6RipIfIndex ,i4SetValFsrip6RipIfCost)
#define nmhSetFsrip6RipIfProtocolEnable(i4Fsrip6RipIfIndex ,i4SetValFsrip6RipIfProtocolEnable)	\
	nmhSetCmn(Fsrip6RipIfProtocolEnable, 12, Fsrip6RipIfProtocolEnableSet, Rip6Lock, Rip6UnLock, 0, 0, 1, "%i %i", i4Fsrip6RipIfIndex ,i4SetValFsrip6RipIfProtocolEnable)
#define nmhSetFsrip6RipIfDefRouteAdvt(i4Fsrip6RipIfIndex ,i4SetValFsrip6RipIfDefRouteAdvt)	\
	nmhSetCmn(Fsrip6RipIfDefRouteAdvt, 12, Fsrip6RipIfDefRouteAdvtSet, Rip6Lock, Rip6UnLock, 0, 0, 1, "%i %i", i4Fsrip6RipIfIndex ,i4SetValFsrip6RipIfDefRouteAdvt)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsrip6RipProfileIndex[12];
extern UINT4 Fsrip6RipProfileStatus[12];
extern UINT4 Fsrip6RipProfileHorizon[12];
extern UINT4 Fsrip6RipProfilePeriodicUpdTime[12];
extern UINT4 Fsrip6RipProfileTrigDelayTime[12];
extern UINT4 Fsrip6RipProfileRouteAge[12];
extern UINT4 Fsrip6RipProfileGarbageCollectTime[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsrip6RipProfileStatus(i4Fsrip6RipProfileIndex ,i4SetValFsrip6RipProfileStatus)	\
	nmhSetCmn(Fsrip6RipProfileStatus, 12, Fsrip6RipProfileStatusSet, Rip6Lock, Rip6UnLock, 0, 0, 1, "%i %i", i4Fsrip6RipProfileIndex ,i4SetValFsrip6RipProfileStatus)
#define nmhSetFsrip6RipProfileHorizon(i4Fsrip6RipProfileIndex ,i4SetValFsrip6RipProfileHorizon)	\
	nmhSetCmn(Fsrip6RipProfileHorizon, 12, Fsrip6RipProfileHorizonSet, Rip6Lock, Rip6UnLock, 0, 0, 1, "%i %i", i4Fsrip6RipProfileIndex ,i4SetValFsrip6RipProfileHorizon)
#define nmhSetFsrip6RipProfilePeriodicUpdTime(i4Fsrip6RipProfileIndex ,i4SetValFsrip6RipProfilePeriodicUpdTime)	\
	nmhSetCmn(Fsrip6RipProfilePeriodicUpdTime, 12, Fsrip6RipProfilePeriodicUpdTimeSet, Rip6Lock, Rip6UnLock, 0, 0, 1, "%i %i", i4Fsrip6RipProfileIndex ,i4SetValFsrip6RipProfilePeriodicUpdTime)
#define nmhSetFsrip6RipProfileTrigDelayTime(i4Fsrip6RipProfileIndex ,i4SetValFsrip6RipProfileTrigDelayTime)	\
	nmhSetCmn(Fsrip6RipProfileTrigDelayTime, 12, Fsrip6RipProfileTrigDelayTimeSet, Rip6Lock, Rip6UnLock, 0, 0, 1, "%i %i", i4Fsrip6RipProfileIndex ,i4SetValFsrip6RipProfileTrigDelayTime)
#define nmhSetFsrip6RipProfileRouteAge(i4Fsrip6RipProfileIndex ,i4SetValFsrip6RipProfileRouteAge)	\
	nmhSetCmn(Fsrip6RipProfileRouteAge, 12, Fsrip6RipProfileRouteAgeSet, Rip6Lock, Rip6UnLock, 0, 0, 1, "%i %i", i4Fsrip6RipProfileIndex ,i4SetValFsrip6RipProfileRouteAge)
#define nmhSetFsrip6RipProfileGarbageCollectTime(i4Fsrip6RipProfileIndex ,i4SetValFsrip6RipProfileGarbageCollectTime)	\
	nmhSetCmn(Fsrip6RipProfileGarbageCollectTime, 12, Fsrip6RipProfileGarbageCollectTimeSet, Rip6Lock, Rip6UnLock, 0, 0, 1, "%i %i", i4Fsrip6RipProfileIndex ,i4SetValFsrip6RipProfileGarbageCollectTime)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsrip6RipRouteDest[12];
extern UINT4 Fsrip6RipRoutePfxLength[12];
extern UINT4 Fsrip6RipRouteProtocol[12];
extern UINT4 Fsrip6RipRouteIfIndex[12];
extern UINT4 Fsrip6RipRouteMetric[12];
extern UINT4 Fsrip6RipRouteTag[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsrip6RipRouteMetric(pFsrip6RipRouteDest , i4Fsrip6RipRoutePfxLength , i4Fsrip6RipRouteProtocol , i4Fsrip6RipRouteIfIndex ,i4SetValFsrip6RipRouteMetric)	\
	nmhSetCmn(Fsrip6RipRouteMetric, 12, Fsrip6RipRouteMetricSet, Rip6Lock, Rip6UnLock, 0, 0, 4, "%s %i %i %i %i", pFsrip6RipRouteDest , i4Fsrip6RipRoutePfxLength , i4Fsrip6RipRouteProtocol , i4Fsrip6RipRouteIfIndex ,i4SetValFsrip6RipRouteMetric)
#define nmhSetFsrip6RipRouteTag(pFsrip6RipRouteDest , i4Fsrip6RipRoutePfxLength , i4Fsrip6RipRouteProtocol , i4Fsrip6RipRouteIfIndex ,i4SetValFsrip6RipRouteTag)	\
	nmhSetCmn(Fsrip6RipRouteTag, 12, Fsrip6RipRouteTagSet, Rip6Lock, Rip6UnLock, 0, 0, 4, "%s %i %i %i %i", pFsrip6RipRouteDest , i4Fsrip6RipRoutePfxLength , i4Fsrip6RipRouteProtocol , i4Fsrip6RipRouteIfIndex ,i4SetValFsrip6RipRouteTag)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsrip6RipPeerAddr[12];
extern UINT4 Fsrip6RipPeerEntryStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsrip6RipPeerEntryStatus(pFsrip6RipPeerAddr ,i4SetValFsrip6RipPeerEntryStatus)	\
	nmhSetCmn(Fsrip6RipPeerEntryStatus, 12, Fsrip6RipPeerEntryStatusSet, Rip6Lock, Rip6UnLock, 0, 0, 1, "%s %i", pFsrip6RipPeerAddr ,i4SetValFsrip6RipPeerEntryStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Fsrip6RipAdvFilterAddress[12];
extern UINT4 Fsrip6RipAdvFilterStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsrip6RipAdvFilterStatus(pFsrip6RipAdvFilterAddress ,i4SetValFsrip6RipAdvFilterStatus)	\
	nmhSetCmn(Fsrip6RipAdvFilterStatus, 12, Fsrip6RipAdvFilterStatusSet, Rip6Lock, Rip6UnLock, 0, 0, 1, "%s %i", pFsrip6RipAdvFilterAddress ,i4SetValFsrip6RipAdvFilterStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsRip6DistInOutRouteMapName[12];
extern UINT4 FsRip6DistInOutRouteMapType[12];
extern UINT4 FsRip6DistInOutRouteMapValue[12];
extern UINT4 FsRip6DistInOutRouteMapRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsRip6DistInOutRouteMapValue(pFsRip6DistInOutRouteMapName , i4FsRip6DistInOutRouteMapType ,i4SetValFsRip6DistInOutRouteMapValue)	\
	nmhSetCmn(FsRip6DistInOutRouteMapValue, 12, FsRip6DistInOutRouteMapValueSet, Rip6Lock, Rip6UnLock, 0, 0, 2, "%s %i %i", pFsRip6DistInOutRouteMapName , i4FsRip6DistInOutRouteMapType ,i4SetValFsRip6DistInOutRouteMapValue)
#define nmhSetFsRip6DistInOutRouteMapRowStatus(pFsRip6DistInOutRouteMapName , i4FsRip6DistInOutRouteMapType ,i4SetValFsRip6DistInOutRouteMapRowStatus)	\
	nmhSetCmn(FsRip6DistInOutRouteMapRowStatus, 12, FsRip6DistInOutRouteMapRowStatusSet, Rip6Lock, Rip6UnLock, 0, 1, 2, "%s %i %i", pFsRip6DistInOutRouteMapName , i4FsRip6DistInOutRouteMapType ,i4SetValFsRip6DistInOutRouteMapRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsRip6PreferenceValue[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsRip6PreferenceValue(i4SetValFsRip6PreferenceValue)	\
	nmhSetCmn(FsRip6PreferenceValue, 10, FsRip6PreferenceValueSet, Rip6Lock, Rip6UnLock, 0, 0, 0, "%i", i4SetValFsRip6PreferenceValue)

#endif
