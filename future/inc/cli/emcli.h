/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emcli.h,v 1.8 2010/02/10 06:38:00 prabuc Exp $
 *
 * Description: This file contains the data structure and macros
 *              for EOAM CLI module
 *********************************************************************/

#ifndef _EMCLI_H_
#define _EMCLI_H_

#include "cli.h"

/* 
 * max value of symbol period window that can be configured
 * in millions is 0x10c6f7ab5ed/18446744073709.
 * Eg. If the given value in cli is 18446744073709 (which is in millions),
 * while storing it will be multiplied by 1000000 (1 million)
 * and the result is 18446744073709000000 which is nearly equal to the 
 * maximum UINT8 value (18446744073709551615)
 */
#define EOAM_CLI_MAX_SYM_PERIOD_WINDOW(pz) \
FSAP_U8_ASSIGN_HI(pz,0x10c6); \
FSAP_U8_ASSIGN_LO(pz,0xf7a0b5ed)
    

/* max no of variable inputs to cli parser */
#define EOAM_CLI_MAX_ARGS              5
/* OUI string display length (xx:xx:xx) includes '\0' */
#define EOAM_CLI_OUI_STR_LEN           9
/* Mac address string display length (xx:xx:xx:xx:xx:xx) */
#define EOAM_CLI_DISP_MAC_LEN          21

/* Command Identifiers */
enum{
   EOAM_CLI_SYS_CTRL = 1,                /*1*/     
   EOAM_CLI_MOD_STAT,                    /*2*/ 
   EOAM_CLI_OUI,                         /*3*/   
   EOAM_CLI_NO_OUI,                      /*4*/
   EOAM_CLI_EVENT_RESEND,                /*5*/     
   EOAM_CLI_NO_EVENT_RESEND,             /*6*/        
   EOAM_CLI_DEBUG_SHOW,                  /*7*/   
   EOAM_CLI_DEBUG,                       /*8*/   
   EOAM_CLI_NO_DEBUG,                    /*9*/ 
   EOAM_CLI_CLEAR_STAT,                  /*10*/   
   EOAM_CLI_CLEAR_LOCAL,                 /*11*/    
   EOAM_CLI_CLEAR_SYM_PERIOD,            /*12*/         
   EOAM_CLI_CLEAR_FRAME,                 /*13*/    
   EOAM_CLI_CLEAR_FRAME_PERIOD,          /*14*/           
   EOAM_CLI_CLEAR_FRAME_SEC_SUMMARY,     /*15*/                
   EOAM_CLI_CLEAR_EVENT_LOG,             /*16*/        
   EOAM_CLI_SHOW_GLOBAL,                 /*17*/    
   EOAM_CLI_SHOW_PORT_LOCAL,             /*18*/        
   EOAM_CLI_SHOW_PORT_NEIGH,             /*19*/        
   EOAM_CLI_SHOW_LB_CPB,                 /*20*/
   EOAM_CLI_SHOW_EVT_NOTIF,              /*21*/
   EOAM_CLI_SHOW_PORT_STAT,              /*22*/       
   EOAM_CLI_SHOW_PORT_EVENT,             /*23*/        
   EOAM_CLI_PORT,                        /*24*/    
   EOAM_CLI_MODE,                        /*25*/
   EOAM_CLI_REMOTE_LB_CMD,               /*26*/      
   EOAM_CLI_REMOTE_LB,                   /*27*/  
   EOAM_CLI_LM_SYMBOL,                   /*28*/  
   EOAM_CLI_LM_FRAME,                    /*29*/ 
   EOAM_CLI_LM_FRAME_PERIOD,             /*30*/        
   EOAM_CLI_LM_FRAME_SEC_SUM,            /*31*/         
   EOAM_CLI_LM,                          /*32*/                 
   EOAM_CLI_LM_SYMBOL_WINDOW,            /*33*/         
   EOAM_CLI_LM_FRAME_WINDOW,             /*34*/        
   EOAM_CLI_LM_FRAME_PERIOD_WINDOW,      /*35*/               
   EOAM_CLI_LM_FRAME_SEC_SUM_WINDOW,     /*36*/                
   EOAM_CLI_LM_SYMBOL_THRES,             /*37*/        
   EOAM_CLI_LM_FRAME_THRES,              /*38*/       
   EOAM_CLI_LM_FRAME_PERIOD_THRES,       /*39*/              
   EOAM_CLI_LM_FRAME_SEC_SUM_THRES,      /*40*/               
   EOAM_CLI_NO_LM_SYMBOL_THRES,          /*41*/           
   EOAM_CLI_NO_LM_FRAME_THRES,           /*42*/          
   EOAM_CLI_NO_LM_FRAME_PERIOD_THRES,    /*43*/                 
   EOAM_CLI_NO_LM_FRAME_SEC_SUM_THRES,   /*44*/                  
   EOAM_CLI_NO_LM_SYMBOL_WINDOW,         /*45*/            
   EOAM_CLI_NO_LM_FRAME_WINDOW,          /*46*/           
   EOAM_CLI_NO_LM_FRAME_PERIOD_WINDOW,   /*47*/                  
   EOAM_CLI_NO_LM_FRAME_SEC_SUM_WINDOW,  /*48*/                   
   EOAM_CLI_CRITICAL_EVENT,              /*49*/       
   EOAM_CLI_DYING_GASP,                   /*50*/
   EOAM_CLI_LM_STATUS                    /*51*/
};

/*Error Strings*/
enum{
   EOAM_CLI_ERR_PORT_NOT_FULL_DUPLEX = 1,    /*1*/   
   EOAM_CLI_ERR_THRESH_EXCEEDS_WINDOW,       /*2*/    
   EOAM_CLI_ERR_UNACCEPTABLE_THRESH_VALUE,   /*3*/        
   EOAM_CLI_ERR_UNACCEPTABLE_WINDOW_VALUE,   /*4*/        
   EOAM_CLI_ERR_NODE_IN_LOCAL_LOOPBACK,      /*5*/     
   EOAM_CLI_REMOTE_LB_INCAPABLE,             /*6*/
   EOAM_CLI_ERR_THRESHOLD_HIGH               /*7*/ 
};

/* Max number of error strings that can be set */
#define EOAM_CLI_MAX_ERR           8

/* The error strings should be placed under the switch so as to avoid 
 * redefinition. This will be visible only in modulecli.c file
 */

#ifdef EMCLI_C
CONST CHR1  *gapc1EoamCliErrString [] = {
   NULL,
   "Interface operation should be full duplex for EOAM to be enabled\r\n",
   "Window size must be greater than threshold value\r\n",
   "Unacceptable Threshold value\r\n",
   "Unacceptable Window value\r\n",
   "Interface in Local Loopback. Cannot entertain remote loopback commands\r\n",
   "Remote peer does not have loopback capability\r\n",
   "Threshold value must be lesser than window size\r\n"
};
#endif/* EMCLI_C */

/* Function prototypes */
PUBLIC INT4 cli_process_eoam_cmd(tCliHandle,UINT4, ...);
PUBLIC INT4 EoamCliSetSystemCtrl (tCliHandle, INT4);
PUBLIC INT4 EoamCliSetModuleStatus (tCliHandle, INT4);
PUBLIC INT4 EoamCliConfOui (tCliHandle, tSNMP_OCTET_STRING_TYPE *);
PUBLIC INT4 EoamCliSetErrorResendCount (tCliHandle, UINT4);
PUBLIC INT4 EoamCliEnableDebug (tCliHandle, INT4);
PUBLIC INT4 EoamCliDisableDebug (tCliHandle, INT4);

PUBLIC INT4 EoamCliShowGlobal (tCliHandle);
PUBLIC INT4 EoamCliShowLocalInfo (tCliHandle, INT4);
PUBLIC INT4 EoamCliShowPortInfo(tCliHandle, INT4, UINT1);
PUBLIC INT4 EoamCliShowLocFuncSupp(tCliHandle, INT4, UINT1);
PUBLIC INT4 EoamCliShowSymPeriod(tCliHandle, INT4, UINT1);
PUBLIC INT4 EoamCliShowFramePeriod(tCliHandle, INT4, UINT1);
PUBLIC INT4 EoamCliShowFrame(tCliHandle, INT4, UINT1);
PUBLIC INT4 EoamCliShowFrameSecSumm(tCliHandle, INT4, UINT1);
PUBLIC INT4 EoamCliShowPeerInfo (tCliHandle, INT4);
PUBLIC INT4 EoamCliShowPeerPortInfo (tCliHandle, INT4, UINT1);
PUBLIC INT4 EoamCliShowPeerFuncSupp (tCliHandle, INT4, UINT1);
PUBLIC INT4 EoamCliShowLBCpb (tCliHandle, INT4);
PUBLIC INT4 EoamCliShowEvtNotif (tCliHandle, INT4);
PUBLIC INT4 EoamCliShowStats (tCliHandle, INT4);
PUBLIC INT4 EoamCliShowRxStats(tCliHandle, INT4, UINT1);
PUBLIC INT4 EoamCliShowTxStats(tCliHandle, INT4, UINT1);
PUBLIC INT4 EoamCliShowRxTxStats(tCliHandle, INT4, UINT1);
PUBLIC INT4 EoamCliShowEventLog (tCliHandle, INT4);
PUBLIC VOID EoamCliDispEvtLogEntry(tCliHandle, INT4, UINT4);

PUBLIC INT4 EoamCliSetPortAdminStatus (tCliHandle, INT4, INT4);
PUBLIC INT4 EoamCliSetPortMode (tCliHandle, INT4, INT4);
PUBLIC INT4 EoamCliSetPortRemoteLBCmd (tCliHandle, INT4, INT4);
PUBLIC INT4 EoamCliSetPortRemoteLB (tCliHandle, INT4, INT4);
PUBLIC INT4 EoamCliSetSymbolPeriodNotif (tCliHandle, INT4, INT4);
PUBLIC INT4 EoamCliSetFrameNotif (tCliHandle, INT4, INT4);
PUBLIC INT4 EoamCliSetFramePeriodNotif (tCliHandle, INT4, INT4);
PUBLIC INT4 EoamCliSetFrameSecSummaryNotif (tCliHandle, INT4, INT4);
PUBLIC INT4 EoamCliSetSymbolPeriodWindow (tCliHandle, INT4, FS_UINT8);
PUBLIC INT4 EoamCliSetFrameWindow (tCliHandle, INT4, UINT4);
PUBLIC INT4 EoamCliSetFramePeriodWindow (tCliHandle, INT4, UINT4);
PUBLIC INT4 EoamCliSetFrameSecSummaryWindow (tCliHandle , INT4, UINT4);
PUBLIC INT4 EoamCliSetSymbolPeriodThres (tCliHandle, INT4, FS_UINT8);
PUBLIC INT4 EoamCliSetFrameThres (tCliHandle, INT4, UINT4);
PUBLIC INT4 EoamCliSetFramePeriodThres (tCliHandle, INT4, UINT4);  
PUBLIC INT4 EoamCliSetFrameSecSummaryThres (tCliHandle, INT4, UINT4);
PUBLIC INT4 EoamCliSetCriticalEvent (tCliHandle, INT4, INT4);
PUBLIC INT4 EoamCliSetDyingGasp (tCliHandle, INT4, INT4);
PUBLIC INT4 EoamCliSetLMStatus (tCliHandle, INT4);

PUBLIC INT4 EoamCliClearStats (tCliHandle, INT4);
PUBLIC INT4 EoamCliClearLocal (tCliHandle, INT4);                               
PUBLIC INT4 EoamCliClearEventLog (tCliHandle, INT4);

PUBLIC VOID EoamCliShowDebugging (tCliHandle);                                 
PUBLIC VOID EoamCliShowRunningConfig (tCliHandle, UINT4);
PUBLIC VOID EoamCliShowRunningConfigGlobal (tCliHandle);
PUBLIC VOID EoamCliShowRunningConfigIntf (tCliHandle);
PUBLIC VOID EoamCliShowRunningConfigIfInfo (tCliHandle, INT4, BOOL1);
PUBLIC VOID EoamCliSrcOamTable (tCliHandle, INT4, BOOL1, UINT1 *);
PUBLIC VOID EoamCliSrcOamLBTable (tCliHandle, INT4, BOOL1, UINT1 *);
PUBLIC VOID EoamCliSrcSymbolPeriod (tCliHandle, INT4, BOOL1, UINT1 *);
PUBLIC VOID EoamCliSrcFramePeriod (tCliHandle, INT4, BOOL1, UINT1 *);
PUBLIC VOID EoamCliSrcFrame (tCliHandle, INT4, BOOL1, UINT1 *);
PUBLIC VOID EoamCliSrcFrameSecsSummary (tCliHandle, INT4, BOOL1, UINT1 *);
PUBLIC VOID EoamCliSrcFaultIndication (tCliHandle, INT4, BOOL1, UINT1 *);
PUBLIC INT4 EoamCliSrcPrintPortName(tCliHandle , INT4, BOOL1, UINT1 * );
#endif/*_EMCLI_H_*/

/*--------------------------------------------------------------------------*/
/*                         End of the file emcli.h                          */
/*--------------------------------------------------------------------------*/
