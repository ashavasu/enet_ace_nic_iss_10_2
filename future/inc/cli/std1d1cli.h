/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: std1d1cli.h,v 1.2 2009/09/23 10:42:03 prabuc Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ieee8021BridgeBaseComponentId[13];
extern UINT4 Ieee8021BridgeBaseBridgeAddress[13];
extern UINT4 Ieee8021BridgeBaseComponentType[13];
extern UINT4 Ieee8021BridgeBaseDeviceCapabilities[13];
extern UINT4 Ieee8021BridgeBaseTrafficClassesEnabled[13];
extern UINT4 Ieee8021BridgeBaseMmrpEnabledStatus[13];
extern UINT4 Ieee8021BridgeBaseRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIeee8021BridgeBaseBridgeAddress(u4Ieee8021BridgeBaseComponentId ,SetValIeee8021BridgeBaseBridgeAddress)	\
	nmhSetCmn(Ieee8021BridgeBaseBridgeAddress, 13, Ieee8021BridgeBaseBridgeAddressSet, NULL, NULL, 0, 0, 1, "%u %m", u4Ieee8021BridgeBaseComponentId ,SetValIeee8021BridgeBaseBridgeAddress)
#define nmhSetIeee8021BridgeBaseComponentType(u4Ieee8021BridgeBaseComponentId ,i4SetValIeee8021BridgeBaseComponentType)	\
	nmhSetCmn(Ieee8021BridgeBaseComponentType, 13, Ieee8021BridgeBaseComponentTypeSet, NULL, NULL, 0, 0, 1, "%u %i", u4Ieee8021BridgeBaseComponentId ,i4SetValIeee8021BridgeBaseComponentType)
#define nmhSetIeee8021BridgeBaseDeviceCapabilities(u4Ieee8021BridgeBaseComponentId ,pSetValIeee8021BridgeBaseDeviceCapabilities)	\
	nmhSetCmn(Ieee8021BridgeBaseDeviceCapabilities, 13, Ieee8021BridgeBaseDeviceCapabilitiesSet, NULL, NULL, 0, 0, 1, "%u %s", u4Ieee8021BridgeBaseComponentId ,pSetValIeee8021BridgeBaseDeviceCapabilities)
#define nmhSetIeee8021BridgeBaseTrafficClassesEnabled(u4Ieee8021BridgeBaseComponentId ,i4SetValIeee8021BridgeBaseTrafficClassesEnabled)	\
	nmhSetCmn(Ieee8021BridgeBaseTrafficClassesEnabled, 13, Ieee8021BridgeBaseTrafficClassesEnabledSet, NULL, NULL, 0, 0, 1, "%u %i", u4Ieee8021BridgeBaseComponentId ,i4SetValIeee8021BridgeBaseTrafficClassesEnabled)
#define nmhSetIeee8021BridgeBaseMmrpEnabledStatus(u4Ieee8021BridgeBaseComponentId ,i4SetValIeee8021BridgeBaseMmrpEnabledStatus)	\
	nmhSetCmn(Ieee8021BridgeBaseMmrpEnabledStatus, 13, Ieee8021BridgeBaseMmrpEnabledStatusSet, MrpLock, MrpUnLock, 0, 0, 1, "%u %i", u4Ieee8021BridgeBaseComponentId ,i4SetValIeee8021BridgeBaseMmrpEnabledStatus)
#define nmhSetIeee8021BridgeBaseRowStatus(u4Ieee8021BridgeBaseComponentId ,i4SetValIeee8021BridgeBaseRowStatus)	\
	nmhSetCmn(Ieee8021BridgeBaseRowStatus, 13, Ieee8021BridgeBaseRowStatusSet, NULL, NULL, 0, 1, 1, "%u %i", u4Ieee8021BridgeBaseComponentId ,i4SetValIeee8021BridgeBaseRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ieee8021BridgeBasePortComponentId[13];
extern UINT4 Ieee8021BridgeBasePort[13];
extern UINT4 Ieee8021BridgeBasePortIfIndex[13];
extern UINT4 Ieee8021BridgeBasePortAdminPointToPoint[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIeee8021BridgeBasePortIfIndex(u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValIeee8021BridgeBasePortIfIndex)	\
	nmhSetCmn(Ieee8021BridgeBasePortIfIndex, 13, Ieee8021BridgeBasePortIfIndexSet, NULL, NULL, 0, 0, 2, "%u %u %i", u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValIeee8021BridgeBasePortIfIndex)
#define nmhSetIeee8021BridgeBasePortAdminPointToPoint(u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValIeee8021BridgeBasePortAdminPointToPoint)	\
	nmhSetCmn(Ieee8021BridgeBasePortAdminPointToPoint, 13, Ieee8021BridgeBasePortAdminPointToPointSet, NULL, NULL, 0, 0, 2, "%u %u %i", u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValIeee8021BridgeBasePortAdminPointToPoint)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ieee8021BridgePortDefaultUserPriority[13];
extern UINT4 Ieee8021BridgePortNumTrafficClasses[13];
extern UINT4 Ieee8021BridgePortPriorityCodePointSelection[13];
extern UINT4 Ieee8021BridgePortUseDEI[13];
extern UINT4 Ieee8021BridgePortRequireDropEncoding[13];
extern UINT4 Ieee8021BridgePortServiceAccessPrioritySelection[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIeee8021BridgePortDefaultUserPriority(u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,u4SetValIeee8021BridgePortDefaultUserPriority)	\
	nmhSetCmn(Ieee8021BridgePortDefaultUserPriority, 13, Ieee8021BridgePortDefaultUserPrioritySet, NULL, NULL, 0, 0, 2, "%u %u %u", u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,u4SetValIeee8021BridgePortDefaultUserPriority)
#define nmhSetIeee8021BridgePortNumTrafficClasses(u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValIeee8021BridgePortNumTrafficClasses)	\
	nmhSetCmn(Ieee8021BridgePortNumTrafficClasses, 13, Ieee8021BridgePortNumTrafficClassesSet, NULL, NULL, 0, 0, 2, "%u %u %i", u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValIeee8021BridgePortNumTrafficClasses)
#define nmhSetIeee8021BridgePortPriorityCodePointSelection(u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValIeee8021BridgePortPriorityCodePointSelection)	\
	nmhSetCmn(Ieee8021BridgePortPriorityCodePointSelection, 13, Ieee8021BridgePortPriorityCodePointSelectionSet, NULL, NULL, 0, 0, 2, "%u %u %i", u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValIeee8021BridgePortPriorityCodePointSelection)
#define nmhSetIeee8021BridgePortUseDEI(u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValIeee8021BridgePortUseDEI)	\
	nmhSetCmn(Ieee8021BridgePortUseDEI, 13, Ieee8021BridgePortUseDEISet, NULL, NULL, 0, 0, 2, "%u %u %i", u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValIeee8021BridgePortUseDEI)
#define nmhSetIeee8021BridgePortRequireDropEncoding(u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValIeee8021BridgePortRequireDropEncoding)	\
	nmhSetCmn(Ieee8021BridgePortRequireDropEncoding, 13, Ieee8021BridgePortRequireDropEncodingSet, NULL, NULL, 0, 0, 2, "%u %u %i", u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValIeee8021BridgePortRequireDropEncoding)
#define nmhSetIeee8021BridgePortServiceAccessPrioritySelection(u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValIeee8021BridgePortServiceAccessPrioritySelection)	\
	nmhSetCmn(Ieee8021BridgePortServiceAccessPrioritySelection, 13, Ieee8021BridgePortServiceAccessPrioritySelectionSet, NULL, NULL, 0, 0, 2, "%u %u %i", u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValIeee8021BridgePortServiceAccessPrioritySelection)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ieee8021BridgeUserPriority[13];
extern UINT4 Ieee8021BridgeRegenUserPriority[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIeee8021BridgeRegenUserPriority(u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort , u4Ieee8021BridgeUserPriority ,u4SetValIeee8021BridgeRegenUserPriority)	\
	nmhSetCmn(Ieee8021BridgeRegenUserPriority, 13, Ieee8021BridgeRegenUserPrioritySet, NULL, NULL, 0, 0, 3, "%u %u %u %u", u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort , u4Ieee8021BridgeUserPriority ,u4SetValIeee8021BridgeRegenUserPriority)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ieee8021BridgeTrafficClassPriority[13];
extern UINT4 Ieee8021BridgeTrafficClass[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIeee8021BridgeTrafficClass(u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort , u4Ieee8021BridgeTrafficClassPriority ,i4SetValIeee8021BridgeTrafficClass)	\
	nmhSetCmn(Ieee8021BridgeTrafficClass, 13, Ieee8021BridgeTrafficClassSet, NULL, NULL, 0, 0, 3, "%u %u %u %i", u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort , u4Ieee8021BridgeTrafficClassPriority ,i4SetValIeee8021BridgeTrafficClass)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ieee8021BridgePortDecodingComponentId[13];
extern UINT4 Ieee8021BridgePortDecodingPortNum[13];
extern UINT4 Ieee8021BridgePortDecodingPriorityCodePointRow[13];
extern UINT4 Ieee8021BridgePortDecodingPriorityCodePoint[13];
extern UINT4 Ieee8021BridgePortDecodingPriority[13];
extern UINT4 Ieee8021BridgePortDecodingDropEligible[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIeee8021BridgePortDecodingPriority(u4Ieee8021BridgePortDecodingComponentId , u4Ieee8021BridgePortDecodingPortNum , i4Ieee8021BridgePortDecodingPriorityCodePointRow , i4Ieee8021BridgePortDecodingPriorityCodePoint ,u4SetValIeee8021BridgePortDecodingPriority)	\
	nmhSetCmn(Ieee8021BridgePortDecodingPriority, 13, Ieee8021BridgePortDecodingPrioritySet, NULL, NULL, 0, 0, 4, "%u %u %i %i %u", u4Ieee8021BridgePortDecodingComponentId , u4Ieee8021BridgePortDecodingPortNum , i4Ieee8021BridgePortDecodingPriorityCodePointRow , i4Ieee8021BridgePortDecodingPriorityCodePoint ,u4SetValIeee8021BridgePortDecodingPriority)
#define nmhSetIeee8021BridgePortDecodingDropEligible(u4Ieee8021BridgePortDecodingComponentId , u4Ieee8021BridgePortDecodingPortNum , i4Ieee8021BridgePortDecodingPriorityCodePointRow , i4Ieee8021BridgePortDecodingPriorityCodePoint ,i4SetValIeee8021BridgePortDecodingDropEligible)	\
	nmhSetCmn(Ieee8021BridgePortDecodingDropEligible, 13, Ieee8021BridgePortDecodingDropEligibleSet, NULL, NULL, 0, 0, 4, "%u %u %i %i %i", u4Ieee8021BridgePortDecodingComponentId , u4Ieee8021BridgePortDecodingPortNum , i4Ieee8021BridgePortDecodingPriorityCodePointRow , i4Ieee8021BridgePortDecodingPriorityCodePoint ,i4SetValIeee8021BridgePortDecodingDropEligible)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ieee8021BridgePortEncodingComponentId[13];
extern UINT4 Ieee8021BridgePortEncodingPortNum[13];
extern UINT4 Ieee8021BridgePortEncodingPriorityCodePointRow[13];
extern UINT4 Ieee8021BridgePortEncodingPriorityCodePoint[13];
extern UINT4 Ieee8021BridgePortEncodingDropEligible[13];
extern UINT4 Ieee8021BridgePortEncodingPriority[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIeee8021BridgePortEncodingPriority(u4Ieee8021BridgePortEncodingComponentId , u4Ieee8021BridgePortEncodingPortNum , i4Ieee8021BridgePortEncodingPriorityCodePointRow , i4Ieee8021BridgePortEncodingPriorityCodePoint , i4Ieee8021BridgePortEncodingDropEligible ,u4SetValIeee8021BridgePortEncodingPriority)	\
	nmhSetCmn(Ieee8021BridgePortEncodingPriority, 13, Ieee8021BridgePortEncodingPrioritySet, NULL, NULL, 0, 0, 5, "%u %u %i %i %i %u", u4Ieee8021BridgePortEncodingComponentId , u4Ieee8021BridgePortEncodingPortNum , i4Ieee8021BridgePortEncodingPriorityCodePointRow , i4Ieee8021BridgePortEncodingPriorityCodePoint , i4Ieee8021BridgePortEncodingDropEligible ,u4SetValIeee8021BridgePortEncodingPriority)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ieee8021BridgeServiceAccessPriorityComponentId[13];
extern UINT4 Ieee8021BridgeServiceAccessPriorityPortNum[13];
extern UINT4 Ieee8021BridgeServiceAccessPriorityReceived[13];
extern UINT4 Ieee8021BridgeServiceAccessPriorityValue[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIeee8021BridgeServiceAccessPriorityValue(u4Ieee8021BridgeServiceAccessPriorityComponentId , u4Ieee8021BridgeServiceAccessPriorityPortNum , u4Ieee8021BridgeServiceAccessPriorityReceived ,u4SetValIeee8021BridgeServiceAccessPriorityValue)	\
	nmhSetCmn(Ieee8021BridgeServiceAccessPriorityValue, 13, Ieee8021BridgeServiceAccessPriorityValueSet, NULL, NULL, 0, 0, 3, "%u %u %u %u", u4Ieee8021BridgeServiceAccessPriorityComponentId , u4Ieee8021BridgeServiceAccessPriorityPortNum , u4Ieee8021BridgeServiceAccessPriorityReceived ,u4SetValIeee8021BridgeServiceAccessPriorityValue)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ieee8021BridgePortMrpJoinTime[13];
extern UINT4 Ieee8021BridgePortMrpLeaveTime[13];
extern UINT4 Ieee8021BridgePortMrpLeaveAllTime[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIeee8021BridgePortMrpJoinTime(u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValIeee8021BridgePortMrpJoinTime)	\
	nmhSetCmn(Ieee8021BridgePortMrpJoinTime, 13, Ieee8021BridgePortMrpJoinTimeSet, MrpLock, MrpUnLock, 0, 0, 2, "%u %u %i", u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValIeee8021BridgePortMrpJoinTime)
#define nmhSetIeee8021BridgePortMrpLeaveTime(u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValIeee8021BridgePortMrpLeaveTime)	\
	nmhSetCmn(Ieee8021BridgePortMrpLeaveTime, 13, Ieee8021BridgePortMrpLeaveTimeSet, MrpLock, MrpUnLock, 0, 0, 2, "%u %u %i", u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValIeee8021BridgePortMrpLeaveTime)
#define nmhSetIeee8021BridgePortMrpLeaveAllTime(u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValIeee8021BridgePortMrpLeaveAllTime)	\
	nmhSetCmn(Ieee8021BridgePortMrpLeaveAllTime, 13, Ieee8021BridgePortMrpLeaveAllTimeSet, MrpLock, MrpUnLock, 0, 0, 2, "%u %u %i", u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValIeee8021BridgePortMrpLeaveAllTime)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ieee8021BridgePortMmrpEnabledStatus[13];
extern UINT4 Ieee8021BridgePortRestrictedGroupRegistration[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIeee8021BridgePortMmrpEnabledStatus(u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValIeee8021BridgePortMmrpEnabledStatus)	\
	nmhSetCmn(Ieee8021BridgePortMmrpEnabledStatus, 13, Ieee8021BridgePortMmrpEnabledStatusSet, MrpLock, MrpUnLock, 0, 0, 2, "%u %u %i", u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValIeee8021BridgePortMmrpEnabledStatus)
#define nmhSetIeee8021BridgePortRestrictedGroupRegistration(u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValIeee8021BridgePortRestrictedGroupRegistration)	\
	nmhSetCmn(Ieee8021BridgePortRestrictedGroupRegistration, 13, Ieee8021BridgePortRestrictedGroupRegistrationSet, MrpLock, MrpUnLock, 0, 0, 2, "%u %u %i", u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValIeee8021BridgePortRestrictedGroupRegistration)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ieee8021BridgeILanIfRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIeee8021BridgeILanIfRowStatus(i4IfIndex ,i4SetValIeee8021BridgeILanIfRowStatus)	\
	nmhSetCmn(Ieee8021BridgeILanIfRowStatus, 13, Ieee8021BridgeILanIfRowStatusSet, NULL, NULL, 0, 1, 1, "%i %i", i4IfIndex ,i4SetValIeee8021BridgeILanIfRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ieee8021BridgeDot1dPortRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetIeee8021BridgeDot1dPortRowStatus(u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValIeee8021BridgeDot1dPortRowStatus)	\
	nmhSetCmn(Ieee8021BridgeDot1dPortRowStatus, 13, Ieee8021BridgeDot1dPortRowStatusSet, NULL, NULL, 0, 1, 2, "%u %u %i", u4Ieee8021BridgeBasePortComponentId , u4Ieee8021BridgeBasePort ,i4SetValIeee8021BridgeDot1dPortRowStatus)

#endif
