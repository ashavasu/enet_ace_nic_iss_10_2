/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsos3cli.h,v 1.5 2017/12/26 13:34:19 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FutOspfv3TraceLevel[10];
extern UINT4 FutOspfv3ABRType[10];
extern UINT4 FutOspfv3NssaAsbrDefRtTrans[10];
extern UINT4 FutOspfv3DefaultPassiveInterface[10];
extern UINT4 FutOspfv3SpfDelay[10];
extern UINT4 FutOspfv3SpfHoldTime[10];
extern UINT4 FutOspfv3RTStaggeringInterval[10];
extern UINT4 FutOspfv3RTStaggeringStatus[10];
extern UINT4 FutOspfv3RestartStrictLsaChecking[10];
extern UINT4 FutOspfv3HelperSupport[10];
extern UINT4 FutOspfv3HelperGraceTimeLimit[10];
extern UINT4 FutOspfv3RestartAckState[10];
extern UINT4 FutOspfv3GraceLsaRetransmitCount[10];
extern UINT4 FutOspfv3RestartReason[10];
extern UINT4 FutOspfv3ExtTraceLevel[10];
extern UINT4 FutOspfv3SetTraps[10];
extern UINT4 FutOspfv3BfdStatus[10];
extern UINT4 FutOspfv3BfdAllIfState[10];
extern UINT4 FutOspfv3RouterIdPermanence[10];
extern UINT4 FutOspfv3ClearProcess[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFutOspfv3TraceLevel(i4SetValFutOspfv3TraceLevel) \
 nmhSetCmn(FutOspfv3TraceLevel, 10, FutOspfv3TraceLevelSet, V3OspfLock, V3OspfUnLock, 0, 0, 0, "%i", i4SetValFutOspfv3TraceLevel)
#define nmhSetFutOspfv3ABRType(i4SetValFutOspfv3ABRType) \
 nmhSetCmn(FutOspfv3ABRType, 10, FutOspfv3ABRTypeSet, V3OspfLock, V3OspfUnLock, 0, 0, 0, "%i", i4SetValFutOspfv3ABRType)
#define nmhSetFutOspfv3NssaAsbrDefRtTrans(i4SetValFutOspfv3NssaAsbrDefRtTrans) \
 nmhSetCmn(FutOspfv3NssaAsbrDefRtTrans, 10, FutOspfv3NssaAsbrDefRtTransSet, V3OspfLock, V3OspfUnLock, 0, 0, 0, "%i", i4SetValFutOspfv3NssaAsbrDefRtTrans)
#define nmhSetFutOspfv3DefaultPassiveInterface(i4SetValFutOspfv3DefaultPassiveInterface) \
 nmhSetCmn(FutOspfv3DefaultPassiveInterface, 10, FutOspfv3DefaultPassiveInterfaceSet, V3OspfLock, V3OspfUnLock, 0, 0, 0, "%i", i4SetValFutOspfv3DefaultPassiveInterface)
#define nmhSetFutOspfv3SpfDelay(i4SetValFutOspfv3SpfDelay) \
 nmhSetCmn(FutOspfv3SpfDelay, 10, FutOspfv3SpfDelaySet, V3OspfLock, V3OspfUnLock, 0, 0, 0, "%i", i4SetValFutOspfv3SpfDelay)
#define nmhSetFutOspfv3SpfHoldTime(i4SetValFutOspfv3SpfHoldTime) \
 nmhSetCmn(FutOspfv3SpfHoldTime, 10, FutOspfv3SpfHoldTimeSet, V3OspfLock, V3OspfUnLock, 0, 0, 0, "%i", i4SetValFutOspfv3SpfHoldTime)
#define nmhSetFutOspfv3RTStaggeringInterval(i4SetValFutOspfv3RTStaggeringInterval) \
 nmhSetCmn(FutOspfv3RTStaggeringInterval, 10, FutOspfv3RTStaggeringIntervalSet, V3OspfLock, V3OspfUnLock, 0, 0, 0, "%i", i4SetValFutOspfv3RTStaggeringInterval)
#define nmhSetFutOspfv3RTStaggeringStatus(i4SetValFutOspfv3RTStaggeringStatus) \
 nmhSetCmn(FutOspfv3RTStaggeringStatus, 10, FutOspfv3RTStaggeringStatusSet, V3OspfLock, V3OspfUnLock, 0, 0, 0, "%i", i4SetValFutOspfv3RTStaggeringStatus)
#define nmhSetFutOspfv3RestartStrictLsaChecking(i4SetValFutOspfv3RestartStrictLsaChecking) \
 nmhSetCmn(FutOspfv3RestartStrictLsaChecking, 10, FutOspfv3RestartStrictLsaCheckingSet, V3OspfLock, V3OspfUnLock, 0, 0, 0, "%i", i4SetValFutOspfv3RestartStrictLsaChecking)
#define nmhSetFutOspfv3HelperSupport(pSetValFutOspfv3HelperSupport) \
 nmhSetCmn(FutOspfv3HelperSupport, 10, FutOspfv3HelperSupportSet, V3OspfLock, V3OspfUnLock, 0, 0, 0, "%s", pSetValFutOspfv3HelperSupport)
#define nmhSetFutOspfv3HelperGraceTimeLimit(i4SetValFutOspfv3HelperGraceTimeLimit) \
 nmhSetCmn(FutOspfv3HelperGraceTimeLimit, 10, FutOspfv3HelperGraceTimeLimitSet, V3OspfLock, V3OspfUnLock, 0, 0, 0, "%i", i4SetValFutOspfv3HelperGraceTimeLimit)
#define nmhSetFutOspfv3RestartAckState(i4SetValFutOspfv3RestartAckState) \
 nmhSetCmn(FutOspfv3RestartAckState, 10, FutOspfv3RestartAckStateSet, V3OspfLock, V3OspfUnLock, 0, 0, 0, "%i", i4SetValFutOspfv3RestartAckState)
#define nmhSetFutOspfv3GraceLsaRetransmitCount(i4SetValFutOspfv3GraceLsaRetransmitCount) \
 nmhSetCmn(FutOspfv3GraceLsaRetransmitCount, 10, FutOspfv3GraceLsaRetransmitCountSet, V3OspfLock, V3OspfUnLock, 0, 0, 0, "%i", i4SetValFutOspfv3GraceLsaRetransmitCount)
#define nmhSetFutOspfv3RestartReason(i4SetValFutOspfv3RestartReason) \
 nmhSetCmn(FutOspfv3RestartReason, 10, FutOspfv3RestartReasonSet, V3OspfLock, V3OspfUnLock, 0, 0, 0, "%i", i4SetValFutOspfv3RestartReason)
#define nmhSetFutOspfv3ExtTraceLevel(i4SetValFutOspfv3ExtTraceLevel) \
 nmhSetCmn(FutOspfv3ExtTraceLevel, 10, FutOspfv3ExtTraceLevelSet, V3OspfLock, V3OspfUnLock, 0, 0, 0, "%i", i4SetValFutOspfv3ExtTraceLevel)
#define nmhSetFutOspfv3SetTraps(i4SetValFutOspfv3SetTraps) \
 nmhSetCmn(FutOspfv3SetTraps, 10, FutOspfv3SetTrapsSet, V3OspfLock, V3OspfUnLock, 0, 0, 0, "%i", i4SetValFutOspfv3SetTraps)
#define nmhSetFutOspfv3BfdStatus(i4SetValFutOspfv3BfdStatus)    \
    nmhSetCmn(FutOspfv3BfdStatus, 10, FutOspfv3BfdStatusSet, V3OspfLock, V3OspfUnLock, 0, 0, 0, "%i", i4SetValFutOspfv3BfdStatus)
#define nmhSetFutOspfv3BfdAllIfState(i4SetValFutOspfv3BfdAllIfState)    \
    nmhSetCmn(FutOspfv3BfdAllIfState, 10, FutOspfv3BfdAllIfStateSet, V3OspfLock, V3OspfUnLock, 0, 0, 0, "%i", i4SetValFutOspfv3BfdAllIfState)
#define nmhSetFutOspfv3RouterIdPermanence(i4SetValFutOspfv3RouterIdPermanence) \
    nmhSetCmn(FutOspfv3RouterIdPermanence, 10, FutOspfv3RouterIdPermanenceSet, V3OspfLock, V3OspfUnLock, 0, 0, 0, "%i", i4SetValFutOspfv3RouterIdPermanence)
#define nmhSetFutOspfv3ClearProcess(i4SetValFutOspfv3ClearProcess)     \
    nmhSetCmn(FutOspfv3ClearProcess, 10, FutOspfv3ClearProcessSet, V3OspfLock, V3OspfUnLock, 0, 0, 0, "%i", i4SetValFutOspfv3ClearProcess)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FutOspfv3IfIndex[11];
extern UINT4 FutOspfv3IfPassive[11];
extern UINT4 FutOspfv3IfBfdState[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFutOspfv3IfPassive(i4FutOspfv3IfIndex ,i4SetValFutOspfv3IfPassive) \
 nmhSetCmn(FutOspfv3IfPassive, 11, FutOspfv3IfPassiveSet, V3OspfLock, V3OspfUnLock, 0, 0, 1, "%i %i", i4FutOspfv3IfIndex ,i4SetValFutOspfv3IfPassive)
#define nmhSetFutOspfv3IfBfdState(i4FutOspfv3IfIndex ,i4SetValFutOspfv3IfBfdState)  \
    nmhSetCmn(FutOspfv3IfBfdState, 11, FutOspfv3IfBfdStateSet, V3OspfLock, V3OspfUnLock, 0, 0, 1, "%i %i", i4FutOspfv3IfIndex ,i4SetValFutOspfv3IfBfdState)


#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FutOspfv3AsExternalAggregationNetType[11];
extern UINT4 FutOspfv3AsExternalAggregationNet[11];
extern UINT4 FutOspfv3AsExternalAggregationPfxLength[11];
extern UINT4 FutOspfv3AsExternalAggregationAreaId[11];
extern UINT4 FutOspfv3AsExternalAggregationEffect[11];
extern UINT4 FutOspfv3AsExternalAggregationTranslation[11];
extern UINT4 FutOspfv3AsExternalAggregationStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFutOspfv3AsExternalAggregationEffect(i4FutOspfv3AsExternalAggregationNetType , pFutOspfv3AsExternalAggregationNet , u4FutOspfv3AsExternalAggregationPfxLength , u4FutOspfv3AsExternalAggregationAreaId ,i4SetValFutOspfv3AsExternalAggregationEffect) \
 nmhSetCmn(FutOspfv3AsExternalAggregationEffect, 11, FutOspfv3AsExternalAggregationEffectSet, V3OspfLock, V3OspfUnLock, 0, 0, 4, "%i %s %u %p %i", i4FutOspfv3AsExternalAggregationNetType , pFutOspfv3AsExternalAggregationNet , u4FutOspfv3AsExternalAggregationPfxLength , u4FutOspfv3AsExternalAggregationAreaId ,i4SetValFutOspfv3AsExternalAggregationEffect)
#define nmhSetFutOspfv3AsExternalAggregationTranslation(i4FutOspfv3AsExternalAggregationNetType , pFutOspfv3AsExternalAggregationNet , u4FutOspfv3AsExternalAggregationPfxLength , u4FutOspfv3AsExternalAggregationAreaId ,i4SetValFutOspfv3AsExternalAggregationTranslation) \
 nmhSetCmn(FutOspfv3AsExternalAggregationTranslation, 11, FutOspfv3AsExternalAggregationTranslationSet, V3OspfLock, V3OspfUnLock, 0, 0, 4, "%i %s %u %p %i", i4FutOspfv3AsExternalAggregationNetType , pFutOspfv3AsExternalAggregationNet , u4FutOspfv3AsExternalAggregationPfxLength , u4FutOspfv3AsExternalAggregationAreaId ,i4SetValFutOspfv3AsExternalAggregationTranslation)
#define nmhSetFutOspfv3AsExternalAggregationStatus(i4FutOspfv3AsExternalAggregationNetType , pFutOspfv3AsExternalAggregationNet , u4FutOspfv3AsExternalAggregationPfxLength , u4FutOspfv3AsExternalAggregationAreaId ,i4SetValFutOspfv3AsExternalAggregationStatus) \
 nmhSetCmn(FutOspfv3AsExternalAggregationStatus, 11, FutOspfv3AsExternalAggregationStatusSet, V3OspfLock, V3OspfUnLock, 0, 1, 4, "%i %s %u %p %i", i4FutOspfv3AsExternalAggregationNetType , pFutOspfv3AsExternalAggregationNet , u4FutOspfv3AsExternalAggregationPfxLength , u4FutOspfv3AsExternalAggregationAreaId ,i4SetValFutOspfv3AsExternalAggregationStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FutOspfv3RedistRouteDestType[11];
extern UINT4 FutOspfv3RedistRouteDest[11];
extern UINT4 FutOspfv3RedistRoutePfxLength[11];
extern UINT4 FutOspfv3RedistRouteMetric[11];
extern UINT4 FutOspfv3RedistRouteMetricType[11];
extern UINT4 FutOspfv3RedistRouteTagType[11];
extern UINT4 FutOspfv3RedistRouteTag[11];
extern UINT4 FutOspfv3RedistRouteStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFutOspfv3RedistRouteMetric(i4FutOspfv3RedistRouteDestType , pFutOspfv3RedistRouteDest , u4FutOspfv3RedistRoutePfxLength ,i4SetValFutOspfv3RedistRouteMetric) \
 nmhSetCmn(FutOspfv3RedistRouteMetric, 11, FutOspfv3RedistRouteMetricSet, V3OspfLock, V3OspfUnLock, 0, 0, 3, "%i %s %u %i", i4FutOspfv3RedistRouteDestType , pFutOspfv3RedistRouteDest , u4FutOspfv3RedistRoutePfxLength ,i4SetValFutOspfv3RedistRouteMetric)
#define nmhSetFutOspfv3RedistRouteMetricType(i4FutOspfv3RedistRouteDestType , pFutOspfv3RedistRouteDest , u4FutOspfv3RedistRoutePfxLength ,i4SetValFutOspfv3RedistRouteMetricType) \
 nmhSetCmn(FutOspfv3RedistRouteMetricType, 11, FutOspfv3RedistRouteMetricTypeSet, V3OspfLock, V3OspfUnLock, 0, 0, 3, "%i %s %u %i", i4FutOspfv3RedistRouteDestType , pFutOspfv3RedistRouteDest , u4FutOspfv3RedistRoutePfxLength ,i4SetValFutOspfv3RedistRouteMetricType)
#define nmhSetFutOspfv3RedistRouteTagType(i4FutOspfv3RedistRouteDestType , pFutOspfv3RedistRouteDest , u4FutOspfv3RedistRoutePfxLength ,i4SetValFutOspfv3RedistRouteTagType) \
 nmhSetCmn(FutOspfv3RedistRouteTagType, 11, FutOspfv3RedistRouteTagTypeSet, V3OspfLock, V3OspfUnLock, 0, 0, 3, "%i %s %u %i", i4FutOspfv3RedistRouteDestType , pFutOspfv3RedistRouteDest , u4FutOspfv3RedistRoutePfxLength ,i4SetValFutOspfv3RedistRouteTagType)
#define nmhSetFutOspfv3RedistRouteTag(i4FutOspfv3RedistRouteDestType , pFutOspfv3RedistRouteDest , u4FutOspfv3RedistRoutePfxLength ,i4SetValFutOspfv3RedistRouteTag) \
 nmhSetCmn(FutOspfv3RedistRouteTag, 11, FutOspfv3RedistRouteTagSet, V3OspfLock, V3OspfUnLock, 0, 0, 3, "%i %s %u %i", i4FutOspfv3RedistRouteDestType , pFutOspfv3RedistRouteDest , u4FutOspfv3RedistRoutePfxLength ,i4SetValFutOspfv3RedistRouteTag)
#define nmhSetFutOspfv3RedistRouteStatus(i4FutOspfv3RedistRouteDestType , pFutOspfv3RedistRouteDest , u4FutOspfv3RedistRoutePfxLength ,i4SetValFutOspfv3RedistRouteStatus) \
 nmhSetCmn(FutOspfv3RedistRouteStatus, 11, FutOspfv3RedistRouteStatusSet, V3OspfLock, V3OspfUnLock, 0, 1, 3, "%i %s %u %i", i4FutOspfv3RedistRouteDestType , pFutOspfv3RedistRouteDest , u4FutOspfv3RedistRoutePfxLength ,i4SetValFutOspfv3RedistRouteStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FutOspfv3RRDStatus[11];
extern UINT4 FutOspfv3RRDSrcProtoMask[11];
extern UINT4 FutOspfv3RRDRouteMapName[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFutOspfv3RRDStatus(i4SetValFutOspfv3RRDStatus) \
 nmhSetCmn(FutOspfv3RRDStatus, 11, FutOspfv3RRDStatusSet, V3OspfLock, V3OspfUnLock, 0, 0, 0, "%i", i4SetValFutOspfv3RRDStatus)
#define nmhSetFutOspfv3RRDSrcProtoMask(i4SetValFutOspfv3RRDSrcProtoMask) \
 nmhSetCmn(FutOspfv3RRDSrcProtoMask, 11, FutOspfv3RRDSrcProtoMaskSet, V3OspfLock, V3OspfUnLock, 0, 0, 0, "%i", i4SetValFutOspfv3RRDSrcProtoMask)
#define nmhSetFutOspfv3RRDRouteMapName(pSetValFutOspfv3RRDRouteMapName) \
 nmhSetCmn(FutOspfv3RRDRouteMapName, 11, FutOspfv3RRDRouteMapNameSet, V3OspfLock, V3OspfUnLock, 0, 0, 0, "%s", pSetValFutOspfv3RRDRouteMapName)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FutOspfv3DistInOutRouteMapName[12];
extern UINT4 FutOspfv3DistInOutRouteMapType[12];
extern UINT4 FutOspfv3DistInOutRouteMapValue[12];
extern UINT4 FutOspfv3DistInOutRouteMapRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFutOspfv3DistInOutRouteMapValue(pFutOspfv3DistInOutRouteMapName , i4FutOspfv3DistInOutRouteMapType ,i4SetValFutOspfv3DistInOutRouteMapValue) \
 nmhSetCmn(FutOspfv3DistInOutRouteMapValue, 12, FutOspfv3DistInOutRouteMapValueSet, V3OspfLock, V3OspfUnLock, 0, 0, 2, "%s %i %i", pFutOspfv3DistInOutRouteMapName , i4FutOspfv3DistInOutRouteMapType ,i4SetValFutOspfv3DistInOutRouteMapValue)
#define nmhSetFutOspfv3DistInOutRouteMapRowStatus(pFutOspfv3DistInOutRouteMapName , i4FutOspfv3DistInOutRouteMapType ,i4SetValFutOspfv3DistInOutRouteMapRowStatus) \
 nmhSetCmn(FutOspfv3DistInOutRouteMapRowStatus, 12, FutOspfv3DistInOutRouteMapRowStatusSet, V3OspfLock, V3OspfUnLock, 0, 1, 2, "%s %i %i", pFutOspfv3DistInOutRouteMapName , i4FutOspfv3DistInOutRouteMapType ,i4SetValFutOspfv3DistInOutRouteMapRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FutOspf3PreferenceValue[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFutOspf3PreferenceValue(i4SetValFutOspf3PreferenceValue) \
 nmhSetCmn(FutOspf3PreferenceValue, 10, FutOspf3PreferenceValueSet, V3OspfLock, V3OspfUnLock, 0, 0, 0, "%i", i4SetValFutOspf3PreferenceValue)

#endif



/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FutOspfv3ExtRouteDestType[11];
extern UINT4 FutOspfv3ExtRouteDest[11];
extern UINT4 FutOspfv3ExtRoutePfxLength[11];
extern UINT4 FutOspfv3ExtRouteNextHopType[11];
extern UINT4 FutOspfv3ExtRouteNextHop[11];
extern UINT4 FutOspfv3ExtRouteMetric[11];
extern UINT4 FutOspfv3ExtRouteMetricType[11];
extern UINT4 FutOspfv3ExtRouteStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFutOspfv3ExtRouteMetric(i4FutOspfv3ExtRouteDestType , pFutOspfv3ExtRouteDest , u4FutOspfv3ExtRoutePfxLength , i4FutOspfv3ExtRouteNextHopType , pFutOspfv3ExtRouteNextHop ,i4SetValFutOspfv3ExtRouteMetric)        \
        nmhSetCmn(FutOspfv3ExtRouteMetric, 11, FutOspfv3ExtRouteMetricSet, NULL, NULL, 0, 0, 5, "%i %s %u %i %s %i", i4FutOspfv3ExtRouteDestType , pFutOspfv3ExtRouteDest , u4FutOspfv3ExtRoutePfxLength , i4FutOspfv3ExtRouteNextHopType , pFutOspfv3ExtRouteNextHop ,i4SetValFutOspfv3ExtRouteMetric)
#define nmhSetFutOspfv3ExtRouteMetricType(i4FutOspfv3ExtRouteDestType , pFutOspfv3ExtRouteDest , u4FutOspfv3ExtRoutePfxLength , i4FutOspfv3ExtRouteNextHopType , pFutOspfv3ExtRouteNextHop ,i4SetValFutOspfv3ExtRouteMetricType)        \
        nmhSetCmn(FutOspfv3ExtRouteMetricType, 11, FutOspfv3ExtRouteMetricTypeSet, NULL, NULL, 0, 0, 5, "%i %s %u %i %s %i", i4FutOspfv3ExtRouteDestType , pFutOspfv3ExtRouteDest , u4FutOspfv3ExtRoutePfxLength , i4FutOspfv3ExtRouteNextHopType , pFutOspfv3ExtRouteNextHop ,i4SetValFutOspfv3ExtRouteMetricType)
#define nmhSetFutOspfv3ExtRouteStatus(i4FutOspfv3ExtRouteDestType , pFutOspfv3ExtRouteDest , u4FutOspfv3ExtRoutePfxLength , i4FutOspfv3ExtRouteNextHopType , pFutOspfv3ExtRouteNextHop ,i4SetValFutOspfv3ExtRouteStatus)        \
        nmhSetCmn(FutOspfv3ExtRouteStatus, 11, FutOspfv3ExtRouteStatusSet, NULL, NULL, 0, 1, 5, "%i %s %u %i %s %i", i4FutOspfv3ExtRouteDestType , pFutOspfv3ExtRouteDest , u4FutOspfv3ExtRoutePfxLength , i4FutOspfv3ExtRouteNextHopType , pFutOspfv3ExtRouteNextHop ,i4SetValFutOspfv3ExtRouteStatus)

#endif



extern UINT4 FutOspfv3RRDProtocolId[13];
extern UINT4 FutOspfv3RRDMetricValue[13];
extern UINT4 FutOspfv3RRDMetricType[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFutOspfv3RRDMetricValue(i4FutOspfv3RRDProtocolId ,i4SetValFutOspfv3RRDMetricValue)       \
       nmhSetCmn(FutOspfv3RRDMetricValue, 13, FutOspfv3RRDMetricValueSet, NULL, NULL, 0, 0, 1, "%i %i", i4FutOspfv3RRDProtocolId ,i4SetValFutOspfv3RRDMetricValue)
#define nmhSetFutOspfv3RRDMetricType(i4FutOspfv3RRDProtocolId ,i4SetValFutOspfv3RRDMetricType) \
       nmhSetCmn(FutOspfv3RRDMetricType, 13, FutOspfv3RRDMetricTypeSet, NULL, NULL, 0, 0, 1, "%i %i", i4FutOspfv3RRDProtocolId ,i4SetValFutOspfv3RRDMetricType)

#endif




