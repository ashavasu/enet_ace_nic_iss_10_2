/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsfmcli.h,v 1.2 2008/10/31 13:09:30 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsFmSystemControl[10];
extern UINT4 FsFmModuleStatus[10];
extern UINT4 FsFmTraceOption[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsFmSystemControl(i4SetValFsFmSystemControl)	\
	nmhSetCmn(FsFmSystemControl, 10, FsFmSystemControlSet, FmLock, FmUnLock, 0, 0, 0, "%i", i4SetValFsFmSystemControl)
#define nmhSetFsFmModuleStatus(i4SetValFsFmModuleStatus)	\
	nmhSetCmn(FsFmModuleStatus, 10, FsFmModuleStatusSet, FmLock, FmUnLock, 0, 0, 0, "%i", i4SetValFsFmModuleStatus)
#define nmhSetFsFmTraceOption(i4SetValFsFmTraceOption)	\
	nmhSetCmn(FsFmTraceOption, 10, FsFmTraceOptionSet, FmLock, FmUnLock, 0, 0, 0, "%i", i4SetValFsFmTraceOption)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsFmSymPeriodAction[12];
extern UINT4 FsFmFrameAction[12];
extern UINT4 FsFmFramePeriodAction[12];
extern UINT4 FsFmFrameSecSummAction[12];
extern UINT4 FsFmCriticalEventAction[12];
extern UINT4 FsFmDyingGaspAction[12];
extern UINT4 FsFmLinkFaultAction[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsFmSymPeriodAction(i4IfIndex ,i4SetValFsFmSymPeriodAction)	\
	nmhSetCmn(FsFmSymPeriodAction, 12, FsFmSymPeriodActionSet, FmLock, FmUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValFsFmSymPeriodAction)
#define nmhSetFsFmFrameAction(i4IfIndex ,i4SetValFsFmFrameAction)	\
	nmhSetCmn(FsFmFrameAction, 12, FsFmFrameActionSet, FmLock, FmUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValFsFmFrameAction)
#define nmhSetFsFmFramePeriodAction(i4IfIndex ,i4SetValFsFmFramePeriodAction)	\
	nmhSetCmn(FsFmFramePeriodAction, 12, FsFmFramePeriodActionSet, FmLock, FmUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValFsFmFramePeriodAction)
#define nmhSetFsFmFrameSecSummAction(i4IfIndex ,i4SetValFsFmFrameSecSummAction)	\
	nmhSetCmn(FsFmFrameSecSummAction, 12, FsFmFrameSecSummActionSet, FmLock, FmUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValFsFmFrameSecSummAction)
#define nmhSetFsFmCriticalEventAction(i4IfIndex ,i4SetValFsFmCriticalEventAction)	\
	nmhSetCmn(FsFmCriticalEventAction, 12, FsFmCriticalEventActionSet, FmLock, FmUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValFsFmCriticalEventAction)
#define nmhSetFsFmDyingGaspAction(i4IfIndex ,i4SetValFsFmDyingGaspAction)	\
	nmhSetCmn(FsFmDyingGaspAction, 12, FsFmDyingGaspActionSet, FmLock, FmUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValFsFmDyingGaspAction)
#define nmhSetFsFmLinkFaultAction(i4IfIndex ,i4SetValFsFmLinkFaultAction)	\
	nmhSetCmn(FsFmLinkFaultAction, 12, FsFmLinkFaultActionSet, FmLock, FmUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValFsFmLinkFaultAction)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsFmLBTestPattern[12];
extern UINT4 FsFmLBTestPktSize[12];
extern UINT4 FsFmLBTestCount[12];
extern UINT4 FsFmLBTestWaitTime[12];
extern UINT4 FsFmLBTestCommand[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsFmLBTestPattern(i4IfIndex ,pSetValFsFmLBTestPattern)	\
	nmhSetCmn(FsFmLBTestPattern, 12, FsFmLBTestPatternSet, FmLock, FmUnLock, 0, 0, 1, "%i %s", i4IfIndex ,pSetValFsFmLBTestPattern)
#define nmhSetFsFmLBTestPktSize(i4IfIndex ,u4SetValFsFmLBTestPktSize)	\
	nmhSetCmn(FsFmLBTestPktSize, 12, FsFmLBTestPktSizeSet, FmLock, FmUnLock, 0, 0, 1, "%i %u", i4IfIndex ,u4SetValFsFmLBTestPktSize)
#define nmhSetFsFmLBTestCount(i4IfIndex ,u4SetValFsFmLBTestCount)	\
	nmhSetCmn(FsFmLBTestCount, 12, FsFmLBTestCountSet, FmLock, FmUnLock, 0, 0, 1, "%i %u", i4IfIndex ,u4SetValFsFmLBTestCount)
#define nmhSetFsFmLBTestWaitTime(i4IfIndex ,i4SetValFsFmLBTestWaitTime)	\
	nmhSetCmn(FsFmLBTestWaitTime, 12, FsFmLBTestWaitTimeSet, FmLock, FmUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValFsFmLBTestWaitTime)
#define nmhSetFsFmLBTestCommand(i4IfIndex ,i4SetValFsFmLBTestCommand)	\
	nmhSetCmn(FsFmLBTestCommand, 12, FsFmLBTestCommandSet, FmLock, FmUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValFsFmLBTestCommand)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsFmVarRetrievalMaxVar[12];
extern UINT4 FsFmVarRetrievalRequest[12];
extern UINT4 FsFmVarRetrievalClearResponse[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsFmVarRetrievalMaxVar(i4IfIndex ,u4SetValFsFmVarRetrievalMaxVar)	\
	nmhSetCmn(FsFmVarRetrievalMaxVar, 12, FsFmVarRetrievalMaxVarSet, FmLock, FmUnLock, 0, 0, 1, "%i %u", i4IfIndex ,u4SetValFsFmVarRetrievalMaxVar)
#define nmhSetFsFmVarRetrievalRequest(i4IfIndex ,pSetValFsFmVarRetrievalRequest)	\
	nmhSetCmn(FsFmVarRetrievalRequest, 12, FsFmVarRetrievalRequestSet, FmLock, FmUnLock, 0, 0, 1, "%i %s", i4IfIndex ,pSetValFsFmVarRetrievalRequest)
#define nmhSetFsFmVarRetrievalClearResponse(i4IfIndex ,i4SetValFsFmVarRetrievalClearResponse)	\
	nmhSetCmn(FsFmVarRetrievalClearResponse, 12, FsFmVarRetrievalClearResponseSet, FmLock, FmUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValFsFmVarRetrievalClearResponse)

#endif
