/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsl2vpcli.h,v 1.3 2014/03/09 15:03:31 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 VplsConfigIndex[13];
extern UINT4 VplsConfigName[13];
extern UINT4 VplsConfigDescr[13];
extern UINT4 VplsConfigAdminStatus[13];
extern UINT4 VplsConfigMacLearning[13];
extern UINT4 VplsConfigDiscardUnknownDest[13];
extern UINT4 VplsConfigMacAging[13];
extern UINT4 VplsConfigFwdFullHighWatermark[13];
extern UINT4 VplsConfigFwdFullLowWatermark[13];
extern UINT4 VplsConfigRowStatus[13];
extern UINT4 VplsConfigMtu[13];
extern UINT4 VplsConfigStorageType[13];
extern UINT4 VplsConfigSignalingType[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetVplsConfigName(u4VplsConfigIndex ,pSetValVplsConfigName)	\
	nmhSetCmnWithLock(VplsConfigName, 13, VplsConfigNameSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %s", u4VplsConfigIndex ,pSetValVplsConfigName)
#define nmhSetVplsConfigDescr(u4VplsConfigIndex ,pSetValVplsConfigDescr)	\
	nmhSetCmnWithLock(VplsConfigDescr, 13, VplsConfigDescrSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %s", u4VplsConfigIndex ,pSetValVplsConfigDescr)
#define nmhSetVplsConfigAdminStatus(u4VplsConfigIndex ,i4SetValVplsConfigAdminStatus)	\
	nmhSetCmnWithLock(VplsConfigAdminStatus, 13, VplsConfigAdminStatusSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %i", u4VplsConfigIndex ,i4SetValVplsConfigAdminStatus)
#define nmhSetVplsConfigMacLearning(u4VplsConfigIndex ,i4SetValVplsConfigMacLearning)	\
	nmhSetCmnWithLock(VplsConfigMacLearning, 13, VplsConfigMacLearningSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %i", u4VplsConfigIndex ,i4SetValVplsConfigMacLearning)
#define nmhSetVplsConfigDiscardUnknownDest(u4VplsConfigIndex ,i4SetValVplsConfigDiscardUnknownDest)	\
	nmhSetCmnWithLock(VplsConfigDiscardUnknownDest, 13, VplsConfigDiscardUnknownDestSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %i", u4VplsConfigIndex ,i4SetValVplsConfigDiscardUnknownDest)
#define nmhSetVplsConfigMacAging(u4VplsConfigIndex ,i4SetValVplsConfigMacAging)	\
	nmhSetCmnWithLock(VplsConfigMacAging, 13, VplsConfigMacAgingSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %i", u4VplsConfigIndex ,i4SetValVplsConfigMacAging)
#define nmhSetVplsConfigFwdFullHighWatermark(u4VplsConfigIndex ,u4SetValVplsConfigFwdFullHighWatermark)	\
	nmhSetCmnWithLock(VplsConfigFwdFullHighWatermark, 13, VplsConfigFwdFullHighWatermarkSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %u", u4VplsConfigIndex ,u4SetValVplsConfigFwdFullHighWatermark)
#define nmhSetVplsConfigFwdFullLowWatermark(u4VplsConfigIndex ,u4SetValVplsConfigFwdFullLowWatermark)	\
	nmhSetCmnWithLock(VplsConfigFwdFullLowWatermark, 13, VplsConfigFwdFullLowWatermarkSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %u", u4VplsConfigIndex ,u4SetValVplsConfigFwdFullLowWatermark)
#define nmhSetVplsConfigRowStatus(u4VplsConfigIndex ,i4SetValVplsConfigRowStatus)	\
	nmhSetCmnWithLock(VplsConfigRowStatus, 13, VplsConfigRowStatusSet, L2vpnLock, L2vpnUnLock, 0, 1, 1, "%u %i", u4VplsConfigIndex ,i4SetValVplsConfigRowStatus)
#define nmhSetVplsConfigMtu(u4VplsConfigIndex ,u4SetValVplsConfigMtu)	\
	nmhSetCmnWithLock(VplsConfigMtu, 13, VplsConfigMtuSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %u", u4VplsConfigIndex ,u4SetValVplsConfigMtu)
#define nmhSetVplsConfigStorageType(u4VplsConfigIndex ,i4SetValVplsConfigStorageType)	\
	nmhSetCmnWithLock(VplsConfigStorageType, 13, VplsConfigStorageTypeSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %i", u4VplsConfigIndex ,i4SetValVplsConfigStorageType)
#define nmhSetVplsConfigSignalingType(u4VplsConfigIndex ,i4SetValVplsConfigSignalingType)	\
	nmhSetCmnWithLock(VplsConfigSignalingType, 13, VplsConfigSignalingTypeSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %i", u4VplsConfigIndex ,i4SetValVplsConfigSignalingType)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 VplsPwBindConfigType[13];
extern UINT4 VplsPwBindType[13];
extern UINT4 VplsPwBindRowStatus[13];
extern UINT4 VplsPwBindStorageType[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetVplsPwBindConfigType(u4VplsConfigIndex , u4PwIndex ,i4SetValVplsPwBindConfigType)	\
	nmhSetCmnWithLock(VplsPwBindConfigType, 13, VplsPwBindConfigTypeSet, L2vpnLock, L2vpnUnLock, 0, 0, 2, "%u %u %i", u4VplsConfigIndex , u4PwIndex ,i4SetValVplsPwBindConfigType)
#define nmhSetVplsPwBindType(u4VplsConfigIndex , u4PwIndex ,i4SetValVplsPwBindType)	\
	nmhSetCmnWithLock(VplsPwBindType, 13, VplsPwBindTypeSet, L2vpnLock, L2vpnUnLock, 0, 0, 2, "%u %u %i", u4VplsConfigIndex , u4PwIndex ,i4SetValVplsPwBindType)
#define nmhSetVplsPwBindRowStatus(u4VplsConfigIndex , u4PwIndex ,i4SetValVplsPwBindRowStatus)	\
	nmhSetCmnWithLock(VplsPwBindRowStatus, 13, VplsPwBindRowStatusSet, L2vpnLock, L2vpnUnLock, 0, 1, 2, "%u %u %i", u4VplsConfigIndex , u4PwIndex ,i4SetValVplsPwBindRowStatus)
#define nmhSetVplsPwBindStorageType(u4VplsConfigIndex , u4PwIndex ,i4SetValVplsPwBindStorageType)	\
	nmhSetCmnWithLock(VplsPwBindStorageType, 13, VplsPwBindStorageTypeSet, L2vpnLock, L2vpnUnLock, 0, 0, 2, "%u %u %i", u4VplsConfigIndex , u4PwIndex ,i4SetValVplsPwBindStorageType)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 VplsBgpADConfigRouteDistinguisher[13];
extern UINT4 VplsBgpADConfigPrefix[13];
extern UINT4 VplsBgpADConfigVplsId[13];
extern UINT4 VplsBgpADConfigRowStatus[13];
extern UINT4 VplsBgpADConfigStorageType[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetVplsBgpADConfigRouteDistinguisher(u4VplsConfigIndex ,pSetValVplsBgpADConfigRouteDistinguisher)	\
	nmhSetCmnWithLock(VplsBgpADConfigRouteDistinguisher, 13, VplsBgpADConfigRouteDistinguisherSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %s", u4VplsConfigIndex ,pSetValVplsBgpADConfigRouteDistinguisher)
#define nmhSetVplsBgpADConfigPrefix(u4VplsConfigIndex ,u4SetValVplsBgpADConfigPrefix)	\
	nmhSetCmnWithLock(VplsBgpADConfigPrefix, 13, VplsBgpADConfigPrefixSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %u", u4VplsConfigIndex ,u4SetValVplsBgpADConfigPrefix)
#define nmhSetVplsBgpADConfigVplsId(u4VplsConfigIndex ,pSetValVplsBgpADConfigVplsId)	\
	nmhSetCmnWithLock(VplsBgpADConfigVplsId, 13, VplsBgpADConfigVplsIdSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %s", u4VplsConfigIndex ,pSetValVplsBgpADConfigVplsId)
#define nmhSetVplsBgpADConfigRowStatus(u4VplsConfigIndex ,i4SetValVplsBgpADConfigRowStatus)	\
	nmhSetCmnWithLock(VplsBgpADConfigRowStatus, 13, VplsBgpADConfigRowStatusSet, L2vpnLock, L2vpnUnLock, 0, 1, 1, "%u %i", u4VplsConfigIndex ,i4SetValVplsBgpADConfigRowStatus)
#define nmhSetVplsBgpADConfigStorageType(u4VplsConfigIndex ,i4SetValVplsBgpADConfigStorageType)	\
	nmhSetCmnWithLock(VplsBgpADConfigStorageType, 13, VplsBgpADConfigStorageTypeSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %i", u4VplsConfigIndex ,i4SetValVplsBgpADConfigStorageType)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 VplsBgpRteTargetIndex[13];
extern UINT4 VplsBgpRteTargetRTType[13];
extern UINT4 VplsBgpRteTargetRT[13];
extern UINT4 VplsBgpRteTargetRTRowStatus[13];
extern UINT4 VplsBgpRteTargetStorageType[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetVplsBgpRteTargetRTType(u4VplsConfigIndex , u4VplsBgpRteTargetIndex ,i4SetValVplsBgpRteTargetRTType)	\
	nmhSetCmnWithLock(VplsBgpRteTargetRTType, 13, VplsBgpRteTargetRTTypeSet, L2vpnLock, L2vpnUnLock, 0, 0, 2, "%u %u %i", u4VplsConfigIndex , u4VplsBgpRteTargetIndex ,i4SetValVplsBgpRteTargetRTType)
#define nmhSetVplsBgpRteTargetRT(u4VplsConfigIndex , u4VplsBgpRteTargetIndex ,pSetValVplsBgpRteTargetRT)	\
	nmhSetCmnWithLock(VplsBgpRteTargetRT, 13, VplsBgpRteTargetRTSet, L2vpnLock, L2vpnUnLock, 0, 0, 2, "%u %u %s", u4VplsConfigIndex , u4VplsBgpRteTargetIndex ,pSetValVplsBgpRteTargetRT)
#define nmhSetVplsBgpRteTargetRTRowStatus(u4VplsConfigIndex , u4VplsBgpRteTargetIndex ,i4SetValVplsBgpRteTargetRTRowStatus)	\
	nmhSetCmnWithLock(VplsBgpRteTargetRTRowStatus, 13, VplsBgpRteTargetRTRowStatusSet, L2vpnLock, L2vpnUnLock, 0, 1, 2, "%u %u %i", u4VplsConfigIndex , u4VplsBgpRteTargetIndex ,i4SetValVplsBgpRteTargetRTRowStatus)
#define nmhSetVplsBgpRteTargetStorageType(u4VplsConfigIndex , u4VplsBgpRteTargetIndex ,i4SetValVplsBgpRteTargetStorageType)	\
	nmhSetCmnWithLock(VplsBgpRteTargetStorageType, 13, VplsBgpRteTargetStorageTypeSet, L2vpnLock, L2vpnUnLock, 0, 0, 2, "%u %u %i", u4VplsConfigIndex , u4VplsBgpRteTargetIndex ,i4SetValVplsBgpRteTargetStorageType)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 VplsStatusNotifEnable[11];
extern UINT4 VplsNotificationMaxRate[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetVplsStatusNotifEnable(i4SetValVplsStatusNotifEnable)	\
	nmhSetCmnWithLock(VplsStatusNotifEnable, 11, VplsStatusNotifEnableSet, L2vpnLock, L2vpnUnLock, 0, 0, 0, "%i", i4SetValVplsStatusNotifEnable)
#define nmhSetVplsNotificationMaxRate(u4SetValVplsNotificationMaxRate)	\
	nmhSetCmnWithLock(VplsNotificationMaxRate, 11, VplsNotificationMaxRateSet, L2vpnLock, L2vpnUnLock, 0, 0, 0, "%u", u4SetValVplsNotificationMaxRate)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsL2VpnPwRedundancyStatus[11];
extern UINT4 FsL2VpnPwRedNegotiationTimeOut[11];
extern UINT4 FsL2VpnPwRedundancySyncFailNotifyEnable[11];
extern UINT4 FsL2VpnPwRedundancyPwStatusNotifyEnable[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsL2VpnPwRedundancyStatus(i4SetValFsL2VpnPwRedundancyStatus)	\
	nmhSetCmnWithLock(FsL2VpnPwRedundancyStatus, 11, FsL2VpnPwRedundancyStatusSet, L2vpnLock, L2vpnUnLock, 0, 0, 0, "%i", i4SetValFsL2VpnPwRedundancyStatus)
#define nmhSetFsL2VpnPwRedNegotiationTimeOut(u4SetValFsL2VpnPwRedNegotiationTimeOut)	\
	nmhSetCmnWithLock(FsL2VpnPwRedNegotiationTimeOut, 11, FsL2VpnPwRedNegotiationTimeOutSet, L2vpnLock, L2vpnUnLock, 0, 0, 0, "%u", u4SetValFsL2VpnPwRedNegotiationTimeOut)
#define nmhSetFsL2VpnPwRedundancySyncFailNotifyEnable(i4SetValFsL2VpnPwRedundancySyncFailNotifyEnable)	\
	nmhSetCmnWithLock(FsL2VpnPwRedundancySyncFailNotifyEnable, 11, FsL2VpnPwRedundancySyncFailNotifyEnableSet, L2vpnLock, L2vpnUnLock, 0, 0, 0, "%i", i4SetValFsL2VpnPwRedundancySyncFailNotifyEnable)
#define nmhSetFsL2VpnPwRedundancyPwStatusNotifyEnable(i4SetValFsL2VpnPwRedundancyPwStatusNotifyEnable)	\
	nmhSetCmnWithLock(FsL2VpnPwRedundancyPwStatusNotifyEnable, 11, FsL2VpnPwRedundancyPwStatusNotifyEnableSet, L2vpnLock, L2vpnUnLock, 0, 0, 0, "%i", i4SetValFsL2VpnPwRedundancyPwStatusNotifyEnable)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsL2VpnPwRedGroupIndex[13];
extern UINT4 FsL2VpnPwRedGroupProtType[13];
extern UINT4 FsL2VpnPwRedGroupReversionType[13];
extern UINT4 FsL2VpnPwRedGroupContentionResolutionMethod[13];
extern UINT4 FsL2VpnPwRedGroupMasterSlaveMode[13];
extern UINT4 FsL2VpnPwRedGroupDualHomeApps[13];
extern UINT4 FsL2VpnPwRedGroupName[13];
extern UINT4 FsL2VpnPwRedGroupWtrTimer[13];
extern UINT4 FsL2VpnPwRedGroupAdminCmd[13];
extern UINT4 FsL2VpnPwRedGroupAdminActivePw[13];
extern UINT4 FsL2VpnPwRedGroupRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsL2VpnPwRedGroupProtType(u4FsL2VpnPwRedGroupIndex ,i4SetValFsL2VpnPwRedGroupProtType)	\
	nmhSetCmnWithLock(FsL2VpnPwRedGroupProtType, 13, FsL2VpnPwRedGroupProtTypeSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %i", u4FsL2VpnPwRedGroupIndex ,i4SetValFsL2VpnPwRedGroupProtType)
#define nmhSetFsL2VpnPwRedGroupReversionType(u4FsL2VpnPwRedGroupIndex ,i4SetValFsL2VpnPwRedGroupReversionType)	\
	nmhSetCmnWithLock(FsL2VpnPwRedGroupReversionType, 13, FsL2VpnPwRedGroupReversionTypeSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %i", u4FsL2VpnPwRedGroupIndex ,i4SetValFsL2VpnPwRedGroupReversionType)
#define nmhSetFsL2VpnPwRedGroupContentionResolutionMethod(u4FsL2VpnPwRedGroupIndex ,i4SetValFsL2VpnPwRedGroupContentionResolutionMethod)	\
	nmhSetCmnWithLock(FsL2VpnPwRedGroupContentionResolutionMethod, 13, FsL2VpnPwRedGroupContentionResolutionMethodSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %i", u4FsL2VpnPwRedGroupIndex ,i4SetValFsL2VpnPwRedGroupContentionResolutionMethod)
#define nmhSetFsL2VpnPwRedGroupMasterSlaveMode(u4FsL2VpnPwRedGroupIndex ,i4SetValFsL2VpnPwRedGroupMasterSlaveMode)	\
	nmhSetCmnWithLock(FsL2VpnPwRedGroupMasterSlaveMode, 13, FsL2VpnPwRedGroupMasterSlaveModeSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %i", u4FsL2VpnPwRedGroupIndex ,i4SetValFsL2VpnPwRedGroupMasterSlaveMode)
#define nmhSetFsL2VpnPwRedGroupDualHomeApps(u4FsL2VpnPwRedGroupIndex ,pSetValFsL2VpnPwRedGroupDualHomeApps)	\
	nmhSetCmnWithLock(FsL2VpnPwRedGroupDualHomeApps, 13, FsL2VpnPwRedGroupDualHomeAppsSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %s", u4FsL2VpnPwRedGroupIndex ,pSetValFsL2VpnPwRedGroupDualHomeApps)
#define nmhSetFsL2VpnPwRedGroupName(u4FsL2VpnPwRedGroupIndex ,pSetValFsL2VpnPwRedGroupName)	\
	nmhSetCmnWithLock(FsL2VpnPwRedGroupName, 13, FsL2VpnPwRedGroupNameSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %s", u4FsL2VpnPwRedGroupIndex ,pSetValFsL2VpnPwRedGroupName)
#define nmhSetFsL2VpnPwRedGroupWtrTimer(u4FsL2VpnPwRedGroupIndex ,u4SetValFsL2VpnPwRedGroupWtrTimer)	\
	nmhSetCmnWithLock(FsL2VpnPwRedGroupWtrTimer, 13, FsL2VpnPwRedGroupWtrTimerSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %u", u4FsL2VpnPwRedGroupIndex ,u4SetValFsL2VpnPwRedGroupWtrTimer)
#define nmhSetFsL2VpnPwRedGroupAdminCmd(u4FsL2VpnPwRedGroupIndex ,i4SetValFsL2VpnPwRedGroupAdminCmd)	\
	nmhSetCmnWithLock(FsL2VpnPwRedGroupAdminCmd, 13, FsL2VpnPwRedGroupAdminCmdSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %i", u4FsL2VpnPwRedGroupIndex ,i4SetValFsL2VpnPwRedGroupAdminCmd)
#define nmhSetFsL2VpnPwRedGroupAdminActivePw(u4FsL2VpnPwRedGroupIndex ,u4SetValFsL2VpnPwRedGroupAdminActivePw)	\
	nmhSetCmnWithLock(FsL2VpnPwRedGroupAdminActivePw, 13, FsL2VpnPwRedGroupAdminActivePwSet, L2vpnLock, L2vpnUnLock, 0, 0, 1, "%u %u", u4FsL2VpnPwRedGroupIndex ,u4SetValFsL2VpnPwRedGroupAdminActivePw)
#define nmhSetFsL2VpnPwRedGroupRowStatus(u4FsL2VpnPwRedGroupIndex ,i4SetValFsL2VpnPwRedGroupRowStatus)	\
	nmhSetCmnWithLock(FsL2VpnPwRedGroupRowStatus, 13, FsL2VpnPwRedGroupRowStatusSet, L2vpnLock, L2vpnUnLock, 0, 1, 1, "%u %i", u4FsL2VpnPwRedGroupIndex ,i4SetValFsL2VpnPwRedGroupRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsL2VpnPwRedNodeAddrType[13];
extern UINT4 FsL2VpnPwRedNodeAddr[13];
extern UINT4 FsL2VpnPwRedNodeRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsL2VpnPwRedNodeRowStatus(u4FsL2VpnPwRedGroupIndex , i4FsL2VpnPwRedNodeAddrType , pFsL2VpnPwRedNodeAddr ,i4SetValFsL2VpnPwRedNodeRowStatus)	\
	nmhSetCmnWithLock(FsL2VpnPwRedNodeRowStatus, 13, FsL2VpnPwRedNodeRowStatusSet, L2vpnLock, L2vpnUnLock, 0, 1, 3, "%u %i %s %i", u4FsL2VpnPwRedGroupIndex , i4FsL2VpnPwRedNodeAddrType , pFsL2VpnPwRedNodeAddr ,i4SetValFsL2VpnPwRedNodeRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsL2VpnPwRedPwIndex[13];
extern UINT4 FsL2VpnPwRedPwPreferance[13];
extern UINT4 FsL2VpnPwRedPwRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsL2VpnPwRedPwPreferance(u4FsL2VpnPwRedGroupIndex , u4FsL2VpnPwRedPwIndex ,i4SetValFsL2VpnPwRedPwPreferance)	\
	nmhSetCmnWithLock(FsL2VpnPwRedPwPreferance, 13, FsL2VpnPwRedPwPreferanceSet, L2vpnLock, L2vpnUnLock, 0, 0, 2, "%u %u %i", u4FsL2VpnPwRedGroupIndex , u4FsL2VpnPwRedPwIndex ,i4SetValFsL2VpnPwRedPwPreferance)
#define nmhSetFsL2VpnPwRedPwRowStatus(u4FsL2VpnPwRedGroupIndex , u4FsL2VpnPwRedPwIndex ,i4SetValFsL2VpnPwRedPwRowStatus)	\
	nmhSetCmnWithLock(FsL2VpnPwRedPwRowStatus, 13, FsL2VpnPwRedPwRowStatusSet, L2vpnLock, L2vpnUnLock, 0, 1, 2, "%u %u %i", u4FsL2VpnPwRedGroupIndex , u4FsL2VpnPwRedPwIndex ,i4SetValFsL2VpnPwRedPwRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsL2VpnPwRedSimulateFailure[12];
extern UINT4 FsL2VpnPwRedSimulateFailureForNbr[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsL2VpnPwRedSimulateFailure(i4SetValFsL2VpnPwRedSimulateFailure)	\
	nmhSetCmnWithLock(FsL2VpnPwRedSimulateFailure, 12, FsL2VpnPwRedSimulateFailureSet, L2vpnLock, L2vpnUnLock, 0, 0, 0, "%i", i4SetValFsL2VpnPwRedSimulateFailure)
#define nmhSetFsL2VpnPwRedSimulateFailureForNbr(u4SetValFsL2VpnPwRedSimulateFailureForNbr)	\
	nmhSetCmnWithLock(FsL2VpnPwRedSimulateFailureForNbr, 12, FsL2VpnPwRedSimulateFailureForNbrSet, L2vpnLock, L2vpnUnLock, 0, 0, 0, "%p", u4SetValFsL2VpnPwRedSimulateFailureForNbr)

#endif
