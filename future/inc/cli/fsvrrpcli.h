/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsvrrpcli.h,v 1.6 2014/07/20 11:09:50 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsVrrpStatus[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsVrrpStatus(i4SetValFsVrrpStatus) \
 nmhSetCmn(FsVrrpStatus, 10, FsVrrpStatusSet, VrrpLock, VrrpUnLock, 0, 0, 0, "%i", i4SetValFsVrrpStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsVrrpAdminPriority[12];
extern UINT4 FsVrrpOperAdvertisementIntervalInMsec[12];
extern UINT4 FsVrrpOperTrackGroupId[12];
extern UINT4 FsVrrpOperDecrementPriority[12];
extern UINT4 FsVrrpAcceptMode[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsVrrpAdminPriority(i4IfIndex , i4VrrpOperVrId ,i4SetValFsVrrpAdminPriority) \
 nmhSetCmn(FsVrrpAdminPriority, 12, FsVrrpAdminPrioritySet, VrrpLock, VrrpUnLock, 0, 0, 2, "%i %i %i", i4IfIndex , i4VrrpOperVrId ,i4SetValFsVrrpAdminPriority)
#define nmhSetFsVrrpOperAdvertisementIntervalInMsec(i4IfIndex , i4VrrpOperVrId ,i4SetValFsVrrpOperAdvertisementIntervalInMsec) \
 nmhSetCmn(FsVrrpOperAdvertisementIntervalInMsec, 12, FsVrrpOperAdvertisementIntervalInMsecSet, VrrpLock, VrrpUnLock, 0, 0, 2, "%i %i %i", i4IfIndex , i4VrrpOperVrId ,i4SetValFsVrrpOperAdvertisementIntervalInMsec)
#define nmhSetFsVrrpOperTrackGroupId(i4IfIndex , i4VrrpOperVrId ,u4SetValFsVrrpOperTrackGroupId) \
 nmhSetCmn(FsVrrpOperTrackGroupId, 12, FsVrrpOperTrackGroupIdSet, VrrpLock, VrrpUnLock, 0, 0, 2, "%i %i %u", i4IfIndex , i4VrrpOperVrId ,u4SetValFsVrrpOperTrackGroupId)
#define nmhSetFsVrrpOperDecrementPriority(i4IfIndex , i4VrrpOperVrId ,u4SetValFsVrrpOperDecrementPriority) \
 nmhSetCmn(FsVrrpOperDecrementPriority, 12, FsVrrpOperDecrementPrioritySet, VrrpLock, VrrpUnLock, 0, 0, 2, "%i %i %u", i4IfIndex , i4VrrpOperVrId ,u4SetValFsVrrpOperDecrementPriority)
#define nmhSetFsVrrpAcceptMode(i4IfIndex , i4VrrpOperVrId ,i4SetValFsVrrpAcceptMode) \
 nmhSetCmn(FsVrrpAcceptMode, 12, FsVrrpAcceptModeSet, VrrpLock, VrrpUnLock, 0, 0, 2, "%i %i %i", i4IfIndex , i4VrrpOperVrId ,i4SetValFsVrrpAcceptMode)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsVrrpAuthDeprecate[10];
extern UINT4 FsVrrpTraceOption[10];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsVrrpAuthDeprecate(i4SetValFsVrrpAuthDeprecate) \
 nmhSetCmn(FsVrrpAuthDeprecate, 10, FsVrrpAuthDeprecateSet, VrrpLock, VrrpUnLock, 0, 0, 0, "%i", i4SetValFsVrrpAuthDeprecate)
#define nmhSetFsVrrpTraceOption(i4SetValFsVrrpTraceOption) \
 nmhSetCmn(FsVrrpTraceOption, 10, FsVrrpTraceOptionSet, VrrpLock, VrrpUnLock, 0, 0, 0, "%i", i4SetValFsVrrpTraceOption)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsVrrpOperTrackGroupIndex[12];
extern UINT4 FsVrrpOperTrackedGroupTrackedLinks[12];
extern UINT4 FsVrrpOperTrackRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsVrrpOperTrackedGroupTrackedLinks(u4FsVrrpOperTrackGroupIndex ,u4SetValFsVrrpOperTrackedGroupTrackedLinks) \
 nmhSetCmn(FsVrrpOperTrackedGroupTrackedLinks, 12, FsVrrpOperTrackedGroupTrackedLinksSet, VrrpLock, VrrpUnLock, 0, 0, 1, "%u %u", u4FsVrrpOperTrackGroupIndex ,u4SetValFsVrrpOperTrackedGroupTrackedLinks)
#define nmhSetFsVrrpOperTrackRowStatus(u4FsVrrpOperTrackGroupIndex ,i4SetValFsVrrpOperTrackRowStatus) \
 nmhSetCmn(FsVrrpOperTrackRowStatus, 12, FsVrrpOperTrackRowStatusSet, VrrpLock, VrrpUnLock, 0, 1, 1, "%u %i", u4FsVrrpOperTrackGroupIndex ,i4SetValFsVrrpOperTrackRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsVrrpOperTrackGroupIfIndex[12];
extern UINT4 FsVrrpOperTrackGroupIfRowStatus[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsVrrpOperTrackGroupIfRowStatus(u4FsVrrpOperTrackGroupIndex , i4FsVrrpOperTrackGroupIfIndex ,i4SetValFsVrrpOperTrackGroupIfRowStatus) \
 nmhSetCmn(FsVrrpOperTrackGroupIfRowStatus, 12, FsVrrpOperTrackGroupIfRowStatusSet, VrrpLock, VrrpUnLock, 0, 1, 2, "%u %i %i", u4FsVrrpOperTrackGroupIndex , i4FsVrrpOperTrackGroupIfIndex ,i4SetValFsVrrpOperTrackGroupIfRowStatus)

#endif
