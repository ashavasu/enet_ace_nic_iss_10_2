#ifndef __CFAGLOBCLI_H__
#define __CFAGLOBCLI_H__

/* STP CLI Command constants */

#include "cli.h"

/*COMMAND IDENTIFIRS*/

enum
{
CLI_CFA_UNI_ID,
CLI_CFA_NO_UNI_ID,
CLI_CFA_UNI_TYPE,
CLI_CFA_NO_UNI_TYPE,
CLI_CFA_UNI_BW_PROFILE
};
	

/* Constants defined for CFAS CLI. These values are passed as arguments 
from
* the def file
*/
#define CLI_CFA_UNI_MAX_ARGS     13
#define EVC_BUNDLING             1
#define ALL_TO_ONE   	         2
#define EVC_MULTIPLEX            3

INT4  cli_process_cfauni_cmd  PROTO((tCliHandle  CliHandle,UINT4  u4Command,...));

#endif
