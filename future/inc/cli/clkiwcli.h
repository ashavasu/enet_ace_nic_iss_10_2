/********************************************************************
  * Copyright (C) 2006 Aricent Inc . All Rights Reserved
  *
  * $Id: clkiwcli.h,v 1.5 2016/04/05 13:25:06 siva Exp $
  *
  * Description: Header file for CLKIWF CLI commands.
  *
  ********************************************************************/
#ifndef __CLKIWCLI_H__
#define __CLKIWCLI_H__

#include "cli.h"
/* 
 * CLKIWF CLI command constants. To add a new command ,a new definition needs to
 * be added to the list.
 * */

/* CLI Constants */


#define CLKIW_CLI_MAX_ARGS       15


/* Clock Time Source as per Table 7 IEEE 1588 */

#define CLI_CLKIW_ATOMIC_CLK 0x10
#define CLI_CLKIW_GPS_CLK    0x20
#define CLI_CLKIW_PTP_CLK    0x40
#define CLI_CLKIW_NTP_CLK    0x50
#define CLI_CLKIW_INTERNAL_OSCILLATOR_CLK 0xA0

#define  CLI_CLKIW_DFLT_CLASS      248
#define  CLI_CLKIW_DFLT_VARIANCE   0
#define  CLI_CLKIW_DFLT_ACCURACY   254
#define  CLI_CLKIW_DFLT_UTC_OFFSET 0 
#define  CLI_CLKIW_DFLT_TIME_SOURCE (CLI_CLKIW_PTP_CLK)

/* Clock Accuracy */

#define CLI_CLK_25NS_ACCURACY    0x20
#define CLI_CLK_100NS_ACCURACY   0x21
#define CLI_CLK_250NS_ACCURACY   0x22
#define CLI_CLK_1MS_ACCURACY     0x23
#define CLI_CLK_2PT5MS_ACCURACY  0x24
#define CLI_CLK_10MS_ACCURACY    0x25
#define CLI_CLK_25MS_ACCURACY    0x26
#define CLI_CLK_100MS_ACCURACY   0x27
#define CLI_CLK_250MS_ACCURACY   0x28
#define CLI_CLK_1MIS_ACCURACY    0x29
#define CLI_CLK_2PT5MIS_ACCURACY 0x2A
#define CLI_CLK_10MIS_ACCURACY   0x2B
#define CLI_CLK_25MIS_ACCURACY   0x2C
#define CLI_CLK_100MIS_ACCURACY  0x2D
#define CLI_CLK_250MIS_ACCURACY  0x2E
#define CLI_CLK_1S_ACCURACY      0x2F
#define CLI_CLK_10S_ACCURACY     0x30
#define CLI_CLK_GREATER_10S_ACCURACY 0x31

/* Traps */
#define CLI_CLKIW_TRAP_GLOB_ERR           0x00000001
#define CLI_CLKIW_TRAP_TIME_SRC_CHNG      CLK_TIME_SRC_TRAP 
#define CLI_CLKIW_TRAP_CLASS_CHNG         CLK_CLASS_TRAP 
#define CLI_CLKIW_TRAP_ACCURACY_CHNG      CLK_ACCURACY_TRAP
#define CLI_CLKIW_TRAP_VARIANCE_CHNG      CLK_VARIANCE_TRAP
#define CLI_CLKIW_TRAP_HOLDOVER_CHNG      CLK_HOLDOVER_TRAP

#define CLI_CLKIWF_DEF_UTC_OFFSET         "+00:00"
#define CLI_CLKIWF_DEF_ARB_TIME           "+00:00"
/* Enumerations */
enum {
    CLI_CLKIW_UNKNOWN = 1,
    CLI_CLKIW_VARIANCE,
    CLI_CLKIW_CLASS, 
    CLI_CLKIW_ACCURACY,
    CLI_CLKIW_ARB_TIME,
    CLI_CLKIW_TIME_SOURCE,
    CLI_CLKIW_UTC_OFFSET,
    CLI_CLKIW_HOLD_OVER_ENABLE,
    CLI_CLKIW_HOLD_OVER_DISABLE,
    CLI_CLKIW_NOTIFY_ENABLE,
    CLI_CLKIW_NOTIFY_DISABLE,
    CLI_CLKIW_DEBUG,
    CLI_CLKIW_SHOW_PROPERTIES
};


enum {
        CLI_CLKIW_UNKNOWN_ERR = 1,
        CLI_CLKIW_ERR_VARIANCE,
        CLI_CLKIW_ERR_TIME_SRC,
        CLI_CLKIW_ERR_ACCURACY,
        CLI_CLKIW_ERR_UTC_OFFSET,
        CLI_CLKIW_ERR_ARB_TIME,
        CLI_CLKIW_MAX_ERR
};

#ifdef _CLKIWCLI_C_

/* Error Strings */

CONST CHR1  *gapc1ClkIwCliErrString [] = {
    NULL,
    "Unknown Error\r\n",
    "Invalid clock variance\r\n",
    "Invalid clock time source\r\n",
    "Invalid clock accuracy\r\n",
    "Invalid clock utc offset\r\n",
    "Invalid Arbitrary time\r\n",
    "Invalid hold over configuration \r\n",
    "\r\n"
};

#endif

/******************************************************************************
 *                            clkiwcli.c                                       *
 ******************************************************************************/


PUBLIC INT4
cli_process_clkiw_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command, ...));

PUBLIC INT4
ClkIwShowRunningConfig PROTO ((tCliHandle CliHandle,UINT4 u4Module));


#endif /* __CLKIWCLI_H__ */
