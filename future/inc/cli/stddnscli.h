/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stddnscli.h,v 1.1 2011/01/21 07:00:38 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DnsResConfigMaxCnames[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDnsResConfigMaxCnames(i4SetValDnsResConfigMaxCnames)	\
	nmhSetCmn(DnsResConfigMaxCnames, 11, DnsResConfigMaxCnamesSet, DnsLock, DnsUnLock, 0, 0, 0, "%i", i4SetValDnsResConfigMaxCnames)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DnsResConfigSbeltAddr[13];
extern UINT4 DnsResConfigSbeltName[13];
extern UINT4 DnsResConfigSbeltRecursion[13];
extern UINT4 DnsResConfigSbeltPref[13];
extern UINT4 DnsResConfigSbeltSubTree[13];
extern UINT4 DnsResConfigSbeltClass[13];
extern UINT4 DnsResConfigSbeltStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDnsResConfigSbeltName(u4DnsResConfigSbeltAddr , pDnsResConfigSbeltSubTree , i4DnsResConfigSbeltClass ,pSetValDnsResConfigSbeltName)	\
	nmhSetCmn(DnsResConfigSbeltName, 13, DnsResConfigSbeltNameSet, DnsLock, DnsUnLock, 0, 0, 3, "%p %s %i %s", u4DnsResConfigSbeltAddr , pDnsResConfigSbeltSubTree , i4DnsResConfigSbeltClass ,pSetValDnsResConfigSbeltName)
#define nmhSetDnsResConfigSbeltRecursion(u4DnsResConfigSbeltAddr , pDnsResConfigSbeltSubTree , i4DnsResConfigSbeltClass ,i4SetValDnsResConfigSbeltRecursion)	\
	nmhSetCmn(DnsResConfigSbeltRecursion, 13, DnsResConfigSbeltRecursionSet, DnsLock, DnsUnLock, 0, 0, 3, "%p %s %i %i", u4DnsResConfigSbeltAddr , pDnsResConfigSbeltSubTree , i4DnsResConfigSbeltClass ,i4SetValDnsResConfigSbeltRecursion)
#define nmhSetDnsResConfigSbeltPref(u4DnsResConfigSbeltAddr , pDnsResConfigSbeltSubTree , i4DnsResConfigSbeltClass ,i4SetValDnsResConfigSbeltPref)	\
	nmhSetCmn(DnsResConfigSbeltPref, 13, DnsResConfigSbeltPrefSet, DnsLock, DnsUnLock, 0, 0, 3, "%p %s %i %i", u4DnsResConfigSbeltAddr , pDnsResConfigSbeltSubTree , i4DnsResConfigSbeltClass ,i4SetValDnsResConfigSbeltPref)
#define nmhSetDnsResConfigSbeltStatus(u4DnsResConfigSbeltAddr , pDnsResConfigSbeltSubTree , i4DnsResConfigSbeltClass ,i4SetValDnsResConfigSbeltStatus)	\
	nmhSetCmn(DnsResConfigSbeltStatus, 13, DnsResConfigSbeltStatusSet, DnsLock, DnsUnLock, 0, 1, 3, "%p %s %i %i", u4DnsResConfigSbeltAddr , pDnsResConfigSbeltSubTree , i4DnsResConfigSbeltClass ,i4SetValDnsResConfigSbeltStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DnsResConfigReset[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDnsResConfigReset(i4SetValDnsResConfigReset)	\
	nmhSetCmn(DnsResConfigReset, 11, DnsResConfigResetSet, DnsLock, DnsUnLock, 0, 0, 0, "%i", i4SetValDnsResConfigReset)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DnsResLameDelegationSource[13];
extern UINT4 DnsResLameDelegationName[13];
extern UINT4 DnsResLameDelegationClass[13];
extern UINT4 DnsResLameDelegationStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDnsResLameDelegationStatus(u4DnsResLameDelegationSource , pDnsResLameDelegationName , i4DnsResLameDelegationClass ,i4SetValDnsResLameDelegationStatus)	\
	nmhSetCmn(DnsResLameDelegationStatus, 13, DnsResLameDelegationStatusSet, DnsLock, DnsUnLock, 0, 1, 3, "%p %s %i %i", u4DnsResLameDelegationSource , pDnsResLameDelegationName , i4DnsResLameDelegationClass ,i4SetValDnsResLameDelegationStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DnsResCacheStatus[11];
extern UINT4 DnsResCacheMaxTTL[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDnsResCacheStatus(i4SetValDnsResCacheStatus)	\
	nmhSetCmn(DnsResCacheStatus, 11, DnsResCacheStatusSet, DnsLock, DnsUnLock, 0, 0, 0, "%i", i4SetValDnsResCacheStatus)
#define nmhSetDnsResCacheMaxTTL(u4SetValDnsResCacheMaxTTL)	\
	nmhSetCmn(DnsResCacheMaxTTL, 11, DnsResCacheMaxTTLSet, DnsLock, DnsUnLock, 0, 0, 0, "%u", u4SetValDnsResCacheMaxTTL)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DnsResCacheRRName[13];
extern UINT4 DnsResCacheRRClass[13];
extern UINT4 DnsResCacheRRType[13];
extern UINT4 DnsResCacheRRStatus[13];
extern UINT4 DnsResCacheRRIndex[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDnsResCacheRRStatus(pDnsResCacheRRName , i4DnsResCacheRRClass , i4DnsResCacheRRType , i4DnsResCacheRRIndex ,i4SetValDnsResCacheRRStatus)	\
	nmhSetCmn(DnsResCacheRRStatus, 13, DnsResCacheRRStatusSet, DnsLock, DnsUnLock, 0, 1, 4, "%s %i %i %i %i", pDnsResCacheRRName , i4DnsResCacheRRClass , i4DnsResCacheRRType , i4DnsResCacheRRIndex ,i4SetValDnsResCacheRRStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DnsResNCacheStatus[11];
extern UINT4 DnsResNCacheMaxTTL[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDnsResNCacheStatus(i4SetValDnsResNCacheStatus)	\
	nmhSetCmn(DnsResNCacheStatus, 11, DnsResNCacheStatusSet, DnsLock, DnsUnLock, 0, 0, 0, "%i", i4SetValDnsResNCacheStatus)
#define nmhSetDnsResNCacheMaxTTL(u4SetValDnsResNCacheMaxTTL)	\
	nmhSetCmn(DnsResNCacheMaxTTL, 11, DnsResNCacheMaxTTLSet, DnsLock, DnsUnLock, 0, 0, 0, "%u", u4SetValDnsResNCacheMaxTTL)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 DnsResNCacheErrQName[13];
extern UINT4 DnsResNCacheErrQClass[13];
extern UINT4 DnsResNCacheErrQType[13];
extern UINT4 DnsResNCacheErrStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetDnsResNCacheErrStatus(pDnsResNCacheErrQName , i4DnsResNCacheErrQClass , i4DnsResNCacheErrQType , i4DnsResNCacheErrIndex ,i4SetValDnsResNCacheErrStatus)	\
	nmhSetCmn(DnsResNCacheErrStatus, 13, DnsResNCacheErrStatusSet, DnsLock, DnsUnLock, 0, 1, 4, "%s %i %i %i %i", pDnsResNCacheErrQName , i4DnsResNCacheErrQClass , i4DnsResNCacheErrQType , i4DnsResNCacheErrIndex ,i4SetValDnsResNCacheErrStatus)

#endif
