/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: mrpcli.h,v 1.7 2013/06/14 12:06:46 siva Exp $
 *
 * Description: This file contains the data structure and macros
 *              for MRP CLI module
 *********************************************************************/

#ifndef __MRPCLI_H__
#define __MRPCLI_H__

#include "cli.h"

enum
{
    CLI_MRP_SHUT_MRP            = 1,
    CLI_MRP_START_MRP           = 2,
    CLI_MRP_APPL_STATUS         = 3,
    CLI_MRP_PORT_APPL_STATUS    = 4,
    CLI_MRP_PERIODIC_TIMER      = 5,
    CLI_MRP_PARTICIPANT_TYPE    = 6,
    CLI_MRP_APP_ADMIN_CTRL      = 7,
    CLI_MRP_NOTIFY_REG_FAIL     = 8,
    CLI_MRP_CLEAR_STATISTICS    = 9,
    CLI_MRP_JOIN_TIMER          = 10,
    CLI_MRP_LEAVE_TIMER         = 11,
    CLI_MRP_LEAVE_ALL_TIMER     = 12,
    CLI_MRP_RESTRICTED_VLAN     = 13,
    CLI_MRP_RESTRICTED_MAC      = 15,
    CLI_MRP_GBL_DEBUG           = 16,
    CLI_MRP_DEBUG               = 17,
    CLI_MRP_NO_GBL_DEBUG        = 18,
    CLI_MRP_SHOW_CONFIG         = 19,
    CLI_MRP_SHOW_STATISTICS     = 20,
    CLI_MRP_SHOW_MVRP_MACHINES  = 21,
    CLI_MRP_SHOW_MMRP_MACHINES  = 22,
    CLI_MRP_SHOW_MRP_TIMER      = 23,
    CLI_MRP_CLEAR_CONFIG        = 24,
    CLI_MRP_REGISTRAR_ADMIN_CTRL= 25              
};

enum 
{
    CLI_MRP_MMRP = 1,
    CLI_MRP_MVRP = 2 
};

enum
{
    CLI_MRP_REGISTER_NORMAL = 0,
    CLI_MRP_REGISTER_FIXED = 1,
    CLI_MRP_REGISTER_FORBIDDEN = 2 
};
enum
{
    MRP_CLI_MMRP_ALL_GROUPS = 0,
    MRP_CLI_MMRP_UNREG_GROUPS = 1,
    MRP_CLI_MMRP_GROUPS = 2
};

enum
{
    CLI_MRP_MMRP_SERVICE_REQ = 1,
    CLI_MRP_MMRP_MAC         = 2
};

enum
{
    CLI_MRP_NORMAL_APPLICANT = 1,
    CLI_MRP_NON_PARTICIPANT = 2,
    CLI_MRP_ACTIVE_APPLICANT = 3
};

enum
{
    CLI_MRP_FULL_PARTICIPANT = 1,
    CLI_MRP_APPLICANT_ONLY   = 2
};

enum
{
    MRP_SHOW_CONTEXT_ONLY = 1,
    MRP_SHOW_ALL,
    MRP_SHOW_PORT_ONLY
};


enum
{
    CLI_MRP_UNKNOWN_ERR             = 1,
    CLI_MRP_VLAN_DISABLED_ERR       = 2,
    CLI_MRP_DISABLED_ERR            = 3,
    CLI_MRP_INVALID_PORT_STATUS_ERR = 4,
    CLI_MMRP_TUNNEL_ENABLED_ERR      = 5,
    CLI_MRP_PROTO_ENA_ERR           = 6,  
    CLI_MVRP_TUNNEL_ENABLED_ERR      =7,
    CLI_MRP_IGS_ENABLED_ERR         = 8,
    CLI_MRP_MLDS_ENABLED_ERR        = 9,
    CLI_MRP_TIMER_VAL_ERR           = 10, 
    CLI_MRP_ACCESS_PORT_ERR         = 11,
    CLI_MRP_SHUT_APP_ENABLED_ERR    = 12,
    CLI_MRP_NOSHUT_VLAN_ENABLED_ERR = 13,
    CLI_MRP_PVRST_STARTED_ERR       = 14,
    CLI_MRP_INVALID_MRP_CONTEXT     = 15,
    CLI_MRP_NOSHUT_GARP_STARTED_ERR = 16,
    CLI_MRP_INVALID_MAC_ADDR        = 17,
    CLI_MRP_INVALID_APP_ENTRY       = 18,
    CLI_MRP_INVALID_MVRP_ATTR       = 19,
    CLI_MRP_INVALID_MMRP_ATTR       = 20,
    CLI_MRP_INVALID_APP_PORT_ENTRY  = 21,
    CLI_MRP_INVALID_BRIDGE_MODE     = 22,
    CLI_MRP_INVALID_VAL             = 23,
    CLI_MRP_PORT_MMRP_ENABLED_ERR   = 24,
    CLI_MRP_PORT_MVRP_ENABLED_ERR   = 25,
    CLI_MRP_PORT_MMRP_STATUS_ERR    = 26,
    CLI_MRP_PORT_MVRP_STATUS_ERR    = 27,
    CLI_MMRP_PROTO_ENABLED_ERR      = 28,
    CLI_MRP_CONFIG_NOT_ALLOWED      = 29,
    CLI_MVRP_SISP_CONFIG_ERR        = 30,
    CLI_MRP_BASE_BRIDGE_ENABLED_ERR = 31,
    CLI_MRP_MAX_ERR
};

#define MRP_CLI_MAX_MAC_STRING_SIZE      21

#define MRP_CLI_INVALID_CONTEXT    0xFFFFFFFF

#ifdef __MRPCLI_C__

CONST CHR1  *gaMrpCliErrString [] = {
    NULL,
    "Unknown Error\r\n",
    "Vlan Module is disabled\r\n",
    "MRP is shutdown \r\n",
    "Port is not created\r\n",
    "MMRP tunnel/discard is configured on this port\r\n",
    "In Provider bridge mode, MMRP cannot be enabled on tunnel port\r\n",
    "MVRP tunneling is enabled on this port\r\n",
    "MMRP cannot be enabled when IGMP Snooping is enabled\r\n",
    "MMRP cannot be enabled when MLD Snooping is enabled\r\n",
    "Leave timer should be greater than 2 times Join timer and Leave-all should be greater than Leave timer and timer value cannot be zero\r\n",
    "MVRP cannot be enabled on access ports.\r\n",
    "MRP module cannot be shutdown when other MRP applications are running\r\n",
    "MRP module can be started only when VLAN module is started\r\n",
    "MVRP cannot be enabled when PVRST is running. \r\n",
    "Invalid MRP Context.\r\n",
    "MRP cannot be started when GARP is running.\r\n",
    "Invalid MRP Application address.\r\n",
    "Application Entry is not present.\r\n",
    "Invalid MVRP Attribute Type.\r\n",
    "Invalid MMRP Attribute Type.\r\n",
    "Application Port Entry is not present.\r\n",
    "MRP cannot be started before bridge mode is set.\r\n",
    "Invalid Value.\r\n",
    "MMRP cannot enabled on CEP/CNP-PortBased/PCEP/PCNP/CBP.\r\n",
    "MVRP cannot enabled on CEP/CNP-PortBased/PCEP/PCNP/CBP.\r\n",
    "MMRP is diabled on the port.\r\n",
    "MVRP is disabled on the port.\r\n",
    "In Provider bridge mode, MMRP cannot be enabled on tunnel port\r\n",
    "This Configuration is not allowed on CEP/CNP-PortBased/PCEP/PCNP/CBP.\r\n",
    "MVRP cannot be enabled on SISP enabled physical and logical interface\r\n",
    "MRP cannot be started when the bridge is in Transparent mode\r\n",
    "\r\n"
};

#endif

#define MRP_MAX_ARGS          7
/*
 * In Cli Garp timers are in milliseconds, whereas in the protocol
 * it is in centi seconds.
 */
#define  MRP_TIMER_PROTOCOL_TO_CLI(i4Val) ((i4Val) * 10)
#define  MRP_TIMER_CLI_TO_PROTOCOL(i4Val) ((i4Val) / 10)

#define MRP_SET_CMD            1
#define MRP_NO_CMD             2

INT4
cli_process_mrp_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);
    
INT4
cli_process_mrp_sh_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);

INT4
MrpCliSelectContextOnMode (tCliHandle CliHandle, UINT4 u4Cmd, 
                            UINT4 *pu4Context, UINT2 *pu2LocalPort);

INT4
MrpCliGetContextForShowCmd (tCliHandle CliHandle, UINT1 *pu1Name, 
                             UINT4 u4IfIndex, UINT4 u4CurrContext, 
                             UINT4 *pu4NextContext, UINT2 *pu2LocalPort);
INT4
MrpCliGetNextActiveContext (tCliHandle CliHandle, UINT4 u4CurrContextId,
                            UINT4 *pu4NextContextId);

VOID MrpCliDisplayShutStatus (tCliHandle CliHandle, UINT4 u4ContextId);

INT4
MrpCliSetRegisterAdminCtrl (tCliHandle CliHandle, UINT4 u4ContextId,
                            UINT1 u1AdminRegCtrl, UINT2 u2LocalPortId);

VOID
MrpCliChkAndDisplayCtxtStatus (tCliHandle CliHandle, UINT4 u4CurrContextId,
          UINT4 u4NextContextId);

INT4
MrpCliSetSystemCtrl (tCliHandle CliHandle, UINT4 u4ContextId, 
                                                INT4 i4Status);
INT4
MrpCliSetMvrpStatus (tCliHandle CliHandle, UINT4 u4ContextId, 
                                                    UINT4 u4Status);
INT4
MrpCliSetPortMvrpStatus (tCliHandle CliHandle, UINT4 u4ContextId,
                                        UINT4 u4Status, UINT4 u4PortId);
INT4
MrpCliSetMmrpStatus (tCliHandle CliHandle, UINT4 u4ContextId, 
                                                UINT4 u4Status);
INT4
MrpCliSetPortMmrpStatus (tCliHandle CliHandle, UINT4 u4ContextId,
                                       UINT4 u4Status, UINT4 u4PortId);
INT4
MrpCliSetPeriodicTimerStatus (tCliHandle CliHandle, UINT4 u4ContextId,
                                           UINT4 u4Action, UINT4 u4PortId);
INT4
MrpCliSetMrpParticipantType (tCliHandle CliHandle, UINT4 u4ContextId,
                                           UINT4 u4Type, UINT4 u4PortId);
INT4
MrpCliSetMvrpAppAdminCtrl (tCliHandle CliHandle, UINT4 u4ContextId,
                  INT4 i4AttributeType, UINT4 u4AdminCtrl, UINT4 u4PortId);
INT4
MrpCliSetMmrpAppAdminCtrl (tCliHandle CliHandle, UINT4 u4ContextId,
               UINT4 u4AttributeType, UINT4 u4AdminCtrl, UINT4 u4PortId);
INT4
MrpCliSetNotifyVlanRegFail (tCliHandle CliHandle, UINT4 u4ContextId,
                                                        UINT4 u4Status);
INT4
MrpCliSetNotifyMacRegFail (tCliHandle CliHandle, UINT4 u4ContextId,
                                                      UINT4 u4Status);

INT4
MrpCliClearMrpStatistics (tCliHandle CliHandle, UINT4 u4ContextId,
                                   UINT4   u4Application,  UINT2 u2PortId);
INT4
MrpCliSetMrpTimers (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4TimerType,
                                        UINT4 u4TimerValue, UINT4 u4PortId);
INT4
MrpCliSetRestrictedVlanReg (tCliHandle CliHandle, UINT4 u4ContextId,
                                        UINT4 u4Action, UINT4 u4PortId);
INT4
MrpCliSetRestrictedMacReg (tCliHandle CliHandle, UINT4 u4ContextId,
                                        UINT4 u4Action, UINT4 u4PortId);

PUBLIC INT4
MrpCliUpdateTraceInput (tCliHandle CliHandle, UINT4 u4ContextId,
                                            UINT1 *pu1TraceInput);
INT4
MrpCliUpdateGlobalTrace (tCliHandle CliHandle, UINT4 u4ContextId,
                                                   UINT4 u4Action);
INT4
MrpCliShowMrpTimers (tCliHandle CliHandle, UINT4 u4ContextId,
                            UINT1 u1DisplayFlag, UINT2 u2PortId);
INT4
MrpCliShowMrpStatistics (tCliHandle CliHandle, UINT4 u4ContextId,
                   UINT1 u1Application, UINT1 u1DisplayFlag, UINT2 u2PortId);

INT4
MrpCliPrintStatistics (tCliHandle CliHandle, UINT4 u4ContextId, tMacAddr MacAddr,
                                UINT2  u2PortId);

INT4
MrpCliShowMrpConfig (tCliHandle CliHandle, UINT4 u4ContextId,
                   UINT2 u2Application, UINT1 u1DisplayFlag, UINT2 u2PortId);

VOID
MrpCliShowMrpConfigOnContext (tCliHandle CliHandle, UINT4 u4ContextId, 
                              UINT2 u2Application);

VOID
MrpCliShowMrpConfigOnPort (tCliHandle CliHandle, UINT4 u4ContextId,
                           UINT2 u2PortId, UINT2 u2Application);

VOID
MrpCliShowAllMrpConfig (tCliHandle CliHandle, UINT2 u2Application);

VOID
MrpCliPrintMrpSwitchConfig PROTO ((tCliHandle CliHandle, UINT4  u4ContextId,
                                   UINT2   u2Application));

INT4
MrpCliPrintMrpConfig(tCliHandle CliHandle, UINT4 u4ContextId,
       UINT2 u2Application, UINT2 u2PortId);
INT4 MrpCliShowMvrpStateMachines (tCliHandle CliHandle, UINT4  u4ContextId,
                      tVlanId  u2VlanId, UINT1 u1DisplayFlag, UINT2 u2PortId);

INT4 MrpCliShowMmrpStateMachines (tCliHandle CliHandle, UINT4  u4ContextId,
                    tVlanId  VlanId,UINT1  *pu1AttrVal, UINT1 u1SerRqVal, 
                                UINT1 u1DisplayFlag, UINT2 u2PortId);
VOID
MrpCliPrintStateMachineHeader PROTO ((tCliHandle CliHandle, UINT4 u4ContextId));

VOID 
MrpCliPrintMrpTimerTblHeader PROTO ((tCliHandle CliHandle, UINT4 u4ContextId));

INT4
MrpCliClearMrpConfiguration PROTO ((tCliHandle CliHandle, UINT4 u4ContextId,
                             UINT4   u4Application, UINT2 u2PortId));

VOID
MrpCliClearConfiguration PROTO ((UINT4 u4ContextId, UINT4 u4Application));

VOID
MrpCliClearConfigurationOnPort  PROTO ((UINT4 u4ContextId, UINT4 u4Port, 
                                        UINT4 u4Application));

/*--------------------------------------------------------------------------*/
/*                       mrpsrc.c                                           */
/*--------------------------------------------------------------------------*/
VOID
MrpSrcShowRunningConfigIfaceDetails (tCliHandle CliHandle, INT4 i4IfIndex);

VOID
MrpSrcShowRunningConfigTables (tCliHandle CliHandle, UINT4 u4CompId);

VOID
MrpSrcShowRunningConfig (tCliHandle CliHandle, UINT4 u4ContextId);

INT4
MrpSrcGetNextAppIdForDisplay (INT4 i4CurrAppId, tMacAddr MacAddr, 
                              UINT4 u4CompId, INT4 *pi4NextAppId);
VOID IssMrpShowDebugging (tCliHandle CliHandle);

#endif /* __MRPCLI_H__ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  mrpcli.h                        */
/*-----------------------------------------------------------------------*/
