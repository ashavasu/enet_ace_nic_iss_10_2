/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: fscncli.h,v 1.1 2010/07/05 06:43:57 prabuc Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsCnSystemControl[11];
extern UINT4 FsCnGlobalEnableTrap[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsCnSystemControl(i4SetValFsCnSystemControl)	\
	nmhSetCmn(FsCnSystemControl, 11, FsCnSystemControlSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsCnSystemControl)
#define nmhSetFsCnGlobalEnableTrap(i4SetValFsCnGlobalEnableTrap)	\
	nmhSetCmn(FsCnGlobalEnableTrap, 11, FsCnGlobalEnableTrapSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsCnGlobalEnableTrap)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsCnXGlobalTraceLevel[13];
extern UINT4 FsCnXGlobalClearCounters[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsCnXGlobalTraceLevel(u4Ieee8021CnGlobalComponentId ,i4SetValFsCnXGlobalTraceLevel)	\
	nmhSetCmn(FsCnXGlobalTraceLevel, 13, FsCnXGlobalTraceLevelSet, NULL, NULL, 0, 0, 1, "%u %i", u4Ieee8021CnGlobalComponentId ,i4SetValFsCnXGlobalTraceLevel)
#define nmhSetFsCnXGlobalClearCounters(u4Ieee8021CnGlobalComponentId ,i4SetValFsCnXGlobalClearCounters)	\
	nmhSetCmn(FsCnXGlobalClearCounters, 13, FsCnXGlobalClearCountersSet, NULL, NULL, 0, 0, 1, "%u %i", u4Ieee8021CnGlobalComponentId ,i4SetValFsCnXGlobalClearCounters)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsCnXPortPriClearCpCounters[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsCnXPortPriClearCpCounters(u4Ieee8021CnPortPriComponentId , u4Ieee8021CnPortPriority , i4Ieee8021CnPortPriIfIndex ,i4SetValFsCnXPortPriClearCpCounters)	\
	nmhSetCmn(FsCnXPortPriClearCpCounters, 13, FsCnXPortPriClearCpCountersSet, NULL, NULL, 0, 0, 3, "%u %u %i %i", u4Ieee8021CnPortPriComponentId , u4Ieee8021CnPortPriority , i4Ieee8021CnPortPriIfIndex ,i4SetValFsCnXPortPriClearCpCounters)

#endif
