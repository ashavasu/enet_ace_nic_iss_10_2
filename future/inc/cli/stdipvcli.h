/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdipvcli.h,v 1.3 2009/01/20 12:54:18 prabuc-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IpForwarding[8];
extern UINT4 IpDefaultTTL[8];
extern UINT4 Ipv6IpForwarding[8];
extern UINT4 Ipv6IpDefaultHopLimit[8];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ipv4InterfaceIfIndex[10];
extern UINT4 Ipv4InterfaceEnableStatus[10];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ipv6InterfaceIfIndex[10];
extern UINT4 Ipv6InterfaceEnableStatus[10];
extern UINT4 Ipv6InterfaceForwarding[10];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IpAddressSpinLock[8];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IpAddressAddrType[10];
extern UINT4 IpAddressAddr[10];
extern UINT4 IpAddressIfIndex[10];
extern UINT4 IpAddressType[10];
extern UINT4 IpAddressStatus[10];
extern UINT4 IpAddressRowStatus[10];
extern UINT4 IpAddressStorageType[10];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IpNetToPhysicalIfIndex[10];
extern UINT4 IpNetToPhysicalNetAddressType[10];
extern UINT4 IpNetToPhysicalNetAddress[10];
extern UINT4 IpNetToPhysicalPhysAddress[10];
extern UINT4 IpNetToPhysicalType[10];
extern UINT4 IpNetToPhysicalRowStatus[10];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ipv6RouterAdvertSpinLock[8];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 Ipv6RouterAdvertIfIndex[10];
extern UINT4 Ipv6RouterAdvertSendAdverts[10];
extern UINT4 Ipv6RouterAdvertMaxInterval[10];
extern UINT4 Ipv6RouterAdvertMinInterval[10];
extern UINT4 Ipv6RouterAdvertManagedFlag[10];
extern UINT4 Ipv6RouterAdvertOtherConfigFlag[10];
extern UINT4 Ipv6RouterAdvertLinkMTU[10];
extern UINT4 Ipv6RouterAdvertReachableTime[10];
extern UINT4 Ipv6RouterAdvertRetransmitTime[10];
extern UINT4 Ipv6RouterAdvertCurHopLimit[10];
extern UINT4 Ipv6RouterAdvertDefaultLifetime[10];
extern UINT4 Ipv6RouterAdvertRowStatus[10];

/*deprecated*/
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IpNetToMediaIfIndex[10];
extern UINT4 IpNetToMediaPhysAddress[10];
extern UINT4 IpNetToMediaNetAddress[10];
extern UINT4 IpNetToMediaType[10];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 InetCidrRouteDestType[11];
extern UINT4 InetCidrRouteDest[11];
extern UINT4 InetCidrRoutePfxLen[11];
extern UINT4 InetCidrRoutePolicy[11];
extern UINT4 InetCidrRouteNextHopType[11];
extern UINT4 InetCidrRouteNextHop[11];
extern UINT4 InetCidrRouteIfIndex[11];
extern UINT4 InetCidrRouteType[11];
extern UINT4 InetCidrRouteNextHopAS[11];
extern UINT4 InetCidrRouteMetric1[11];
extern UINT4 InetCidrRouteMetric2[11];
extern UINT4 InetCidrRouteMetric3[11];
extern UINT4 InetCidrRouteMetric4[11];
extern UINT4 InetCidrRouteMetric5[11];
extern UINT4 InetCidrRouteStatus[11];

/*Deprecated*/
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IpCidrRouteIfIndex[11];
extern UINT4 IpCidrRouteType[11];
extern UINT4 IpCidrRouteInfo[11];
extern UINT4 IpCidrRouteNextHopAS[11];
extern UINT4 IpCidrRouteMetric1[11];
extern UINT4 IpCidrRouteMetric2[11];
extern UINT4 IpCidrRouteMetric3[11];
extern UINT4 IpCidrRouteMetric4[11];
extern UINT4 IpCidrRouteMetric5[11];
extern UINT4 IpCidrRouteStatus[11];

/*Deprecated*/
/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IpForwardMask[11];
extern UINT4 IpForwardIfIndex[11];
extern UINT4 IpForwardType[11];
extern UINT4 IpForwardInfo[11];
extern UINT4 IpForwardNextHopAS[11];
extern UINT4 IpForwardMetric1[11];
extern UINT4 IpForwardMetric2[11];
extern UINT4 IpForwardMetric3[11];
extern UINT4 IpForwardMetric4[11];
extern UINT4 IpForwardMetric5[11];
