/* $Id: mfwdcli.h,v 1.7 2014/03/15 14:14:12 siva Exp $ */
#ifndef MFWDCLI_H
#define MFWDCLI_H
#include "lr.h"
#include "cli.h"

/* FutureMFWD CLI command constants. To add a new command ,a new 
 * definition needs to be added to the list.
 */
enum {
     MFWD_CLI_IP_MULTICAST_ROUTING = 1,
     MFWD_CLI_SHOW_IP_MULTICAST_ROUTE,
     MFWD_CLI_SHOW_IPV6_MULTICAST_ROUTE,
     MFWD_CLI_TRACE
};

/* This value needs to be updated if any new command is added to the list */
#define     MFWD_CLI_MAX_COMMANDS                  (3)

#define MFWD_IO_MODULE              1
#define MFWD_MDH_MODULE             2
#define MFWD_MRP_MODULE             4
#define MFWD_MGMT_MODULE            8
#define MFWD_ALL_MODULES            65535
#define MFWD_MAX_INT4               0x7fffffff
#define CLI_MFWD_TRACE_DIS_FLAG     0x80000000
#define MFWD_MAX_ARGS               10

#define MAX_INT_OBJECTS  SYS_MAX_INTERFACES 
#define MAX_ADDR_BUFFER 256
#define MAX_OIFSTATE_BUFFER 12
                         /* Maximum Number of Interface Objects.*/
#define MFWD_CLI_INTENTRY_SIZE ((MAX_INT_OBJECTS * CLI_MAX_COLUMN_WIDTH) + CLI_MAX_HEADER_SIZE)


INT1 cli_process_mfwd_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);

enum {
    CLI_MFWD_UNKNOWN_ERR = 1,
    CLI_MFWD_NO_ENTRIES_FOUND,
    CLI_MFWD_MAX_ERR 
};

#ifdef __MFWDCLI_C__
CONST CHR1  *MfwdCliErrString [] = {
    NULL,
    "Unknown Error\r\n",
    "No Entries Found \r\n",
    "\r\n"
};
#endif


#endif   /* MFWDCLI_H */
