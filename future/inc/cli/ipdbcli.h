/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ipdbcli.h,v 1.5 2010/08/17 12:29:35 prabuc Exp $
 *
 * Description: This file contains the data structure and macros
 *              for IPDB CLI module
 *********************************************************************/

#ifndef _IPDBCLI_H
#define _IPDBCLI_H

#include "lr.h"
#include "cli.h"
/* IP Binding management module CLI command constants. To add a new command,
 * a new definition needs to be added to the list.
 */
enum
{
    IPDB_STATIC_BINDING_ADD = 1, /* 1 */
    IPDB_STATIC_BINDING_DEL,     /* 2 */
    IPDB_BINDING_SHOW,           /* 3 */
    IPDB_SHOW_ALL,               /* 4 */   
    IPDB_SHOW_STATIC,            /* 5 */
    IPDB_SHOW_DHCP,              /* 6 */
    IPDB_SHOW_PPP,               /* 7 */
    IPDB_SHOW_ARP_SPOOF,         /* 8 */
    IPDB_SHOW_MAC_FORCE_FWD,     /* 9 */
    IPDB_ARP_MAC_FORCE_FWD,      /* 10 */
    IPDB_ARP_DOWNSTREAM_BCAST,   /* 11 */
    IPDB_TRACE_ENABLE,           /* 12 */
    IPDB_TRACE_DISABLE,          /* 13 */
    IPDB_SHOW_BINDING_COUNT,     /* 14 */
    IPDB_SHOW_GLOBAL,            /* 15 */
    IPDB_ENABLE_IP_SRC_GUARD,    /* 16 */
    IPDB_DISABLE_IP_SRC_GUARD,   /* 17 */
    IPDB_SHOW_IP_SRC_GUARD       /* 18 */
};

enum
{
    ISS_IPDB_SHOW_RUNNING_CONFIG = 1 
};

/* Error codes
 * Error code values and strings are maintained inside the protocol module 
 * itself.
 */
enum
{
        CLI_IPDB_COUNT_ERR = 1,   /* 1 */
        CLI_IPDB_VLAN_ERR,        /* 2 */
        CLI_IPDB_INVALID_PORT_ERR, /* 3 */
        CLI_IPDB_INVALID_VALUE_ERR, /* 4 */
        CLI_IPDB_NOT_READY_ERR,     /* 5 */
        CLI_IPDB_NO_ENTRY_ERR,   /* 6 */
        CLI_IPDB_VLAN_NOT_ACTIVE_ERR, /* 7 */
        CLI_IPDB_MAX_ERR
};

#ifdef __IPDBCLI_C__
CONST CHR1  *gaIpdbCliErrorString [] = 
{
        NULL,
        "% Maximum Entries allowed for Static Binding exceeded \r\n",
        "% The given VLAN is not a Valid one\r\n",
        "% The given port is not valid\r\n",
        "% The given value is not valid\r\n",
        "% Entry for the given index is in not ready state\r\n",
        "% Entry for the given index is not available\r\n",
        "% Given VLAN is not active\r\n",
        "\r\n"
};
#endif

INT4
cli_process_ipdb_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);

PUBLIC VOID IssIpdbShowDebugging PROTO ((tCliHandle CliHandle));

#endif
