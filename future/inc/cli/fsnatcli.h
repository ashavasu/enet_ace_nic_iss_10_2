/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsnatcli.h,v 1.2 2011/01/25 11:12:59 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 NatEnable[11];
extern UINT4 NatTypicalNumberOfEntries[11];
extern UINT4 NatTranslatedLocalPortStart[11];
extern UINT4 NatIdleTimeOut[11];
extern UINT4 NatTcpTimeOut[11];
extern UINT4 NatUdpTimeOut[11];
extern UINT4 NatTrcFlag[11];
extern UINT4 NatIKEPortTranslation[11];
extern UINT4 NatIKETimeout[11];
extern UINT4 NatIPSecTimeout[11];
extern UINT4 NatIPSecPendingTimeout[11];
extern UINT4 NatIPSecMaxRetry[11];
extern UINT4 SipAlgPort[11];
extern UINT4 NatSipAlgPartialEntryTimeOut[11];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 NatGlobalAddressInterfaceNum[12];
extern UINT4 NatGlobalAddressTranslatedLocalIp[12];
extern UINT4 NatGlobalAddressMask[12];
extern UINT4 NatGlobalAddressEntryStatus[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 NatLocalAddressInterfaceNumber[12];
extern UINT4 NatLocalAddressLocalIp[12];
extern UINT4 NatLocalAddressMask[12];
extern UINT4 NatLocalAddressEntryStatus[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 NatStaticInterfaceNum[12];
extern UINT4 NatStaticLocalIp[12];
extern UINT4 NatStaticTranslatedLocalIp[12];
extern UINT4 NatStaticEntryStatus[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 NatStaticNaptInterfaceNum[12];
extern UINT4 NatStaticNaptLocalIp[12];
extern UINT4 NatStaticNaptStartLocalPort[12];
extern UINT4 NatStaticNaptEndLocalPort[12];
extern UINT4 NatStaticNaptProtocolNumber[12];
extern UINT4 NatStaticNaptTranslatedLocalIp[12];
extern UINT4 NatStaticNaptTranslatedLocalPort[12];
extern UINT4 NatStaticNaptDescription[12];
extern UINT4 NatStaticNaptEntryStatus[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 NatIfInterfaceNumber[12];
extern UINT4 NatIfNat[12];
extern UINT4 NatIfNapt[12];
extern UINT4 NatIfTwoWayNat[12];
extern UINT4 NatIfEntryStatus[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 NatIPSecSessionInterfaceNum[12];
extern UINT4 NatIPSecSessionLocalIp[12];
extern UINT4 NatIPSecSessionOutsideIp[12];
extern UINT4 NatIPSecSessionSPIInside[12];
extern UINT4 NatIPSecSessionSPIOutside[12];
extern UINT4 NatIPSecSessionEntryStatus[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 NatIPSecPendingInterfaceNum[12];
extern UINT4 NatIPSecPendingLocalIp[12];
extern UINT4 NatIPSecPendingOutsideIp[12];
extern UINT4 NatIPSecPendingSPIInside[12];
extern UINT4 NatIPSecPendingSPIOutside[12];
extern UINT4 NatIPSecPendingEntryStatus[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 NatIKESessionInterfaceNum[12];
extern UINT4 NatIKESessionLocalIp[12];
extern UINT4 NatIKESessionOutsideIp[12];
extern UINT4 NatIKESessionInitCookie[12];
extern UINT4 NatIKESessionEntryStatus[12];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 NatPortTrigInfoAppName[12];
extern UINT4 NatPortTrigInfoInBoundPortRange[12];
extern UINT4 NatPortTrigInfoOutBoundPortRange[12];
extern UINT4 NatPortTrigInfoProtocol[12];
extern UINT4 NatPortTrigInfoEntryStatus[12];
extern UINT4 NatPolicyEntryStatus[12];
extern UINT4 NatPolicyTranslatedIp[12];
