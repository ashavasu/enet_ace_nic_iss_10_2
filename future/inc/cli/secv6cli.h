
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secv6cli.h,v 1.10 2011/07/15 10:53:38 siva Exp $ 
 *
 * Description:This file contains the definitions and macros used by
 *             cli and ipsec modules     
 *
 *******************************************************************/


#ifndef __SECv6CLI_H__
#define __SECv6CLI_H__

#include "cli.h"

/* Opcodes for CLI commands.
 * When a new command is added, add a new definition */

#define CLI_CREATE_IPSECv6_POLICY      1 /*To create an entry in policy-list*/
#define CLI_CREATE_IPSECv6_SA          2 /*To cretae an entry in sa-list */
#define CLI_SET_IPSECv6_SA_PARAMS      3 /*To set security association params*/ 
#define CLI_CREATE_IPSECv6_ACCESS_LIST 4 /*To create an entry in access-list */
#define CLI_CREATE_IPSECv6_SELECTOR    5 /*To create an entry in selector*/  
#define CLI_SHOW_IPSECv6_IF_STAT       6 /*To show interface specific stat */ 
#define CLI_SHOW_IPSECv6_AH_ESP_STAT   7 /*To show ah/esp specific stat */
#define CLI_SHOW_IPSECv6_INTRUDER_STAT 8 /*To show intruder specific stat*/
#define CLI_SET_IPSECv6_ADMIN          9 /*To enable IPsec */
#define CLI_SET_IPSECv6_TRACE_LEVEL    10 /*To set the trace level */
#define CLI_SHOW_IPSECv6_POLICY        11 /*To show entries in policy list */
#define CLI_SHOW_IPSECv6_SELECTOR      12 /*To show entries in selector list */
#define CLI_SHOW_IPSECv6_SA            13 /*To show entries in SA list */ 
#define CLI_SHOW_IPSECv6_ACCESS        14 /*To show entries in access list */
#define CLI_SHOW_IPSECv6_CONFIG        15 /*To show global variables of ipsec*/
#define CLI_DELETE_IPSECv6_POLICY      16 /*To delete entries in policy list */
#define CLI_DELETE_IPSECv6_SELECTOR    17 /*To delete entry in selector list */
#define CLI_DELETE_IPSECv6_SA          18 /*To delete entries in SA list */ 
#define CLI_DELETE_IPSECv6_ACCESS      19 /*To delete entries in access list */
#define CLI_CRYPTO_TRANSFORM_CONF      20 /*Crypto Transform Config mode */

INT1 IPSecv6GetCryptoPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);

VOID cli_process_ipsecv6_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);

enum
{
           CLI_IPSECV6_DIR_IN_OUT_BOUND = 1,
           CLI_IPSECV6_ENABLE_IPSEC,
           CLI_IPSECV6_INDEX_EXIST,
           CLI_IPSECV6_FLAG_ALLOW_FILTER,
           CLI_IPSECV6_APPLY_BYPASS,
           CLI_IPSECV6_INVALID_ADDRESS,
           CLI_IPSECV6_INVALID_ALGORITHM,
           CLI_IPSECV6_INVALID_PROTOCOL,
           CLI_IPSECV6_INVALID_BUNDLE_LENGTH,
           CLI_IPSECV6_FATAL_ERROR,
           CLI_IPSECV6_TRANSPORT_TUNNEL,
           CLI_IPSECV6_INVALID_INDEX,
           CLI_IPSECV6_SA_NOT_CREATE,
           CLI_IPSECV6_PROTO_AH_ESP,
           CLI_IPSECV6_INVALID_INDEX_RANGE,
           CLI_IPSECV6_AUTH_OR_ENCR,
           CLI_IPSECV6_ENCR_ALGO_FOR_ESP,
           CLI_IPSECV6_INVALID_SPI_RANGE,
           CLI_IPSECV6_REPLY_EANBLE_DISABLE,
           CLI_IPSECV6_INVALID_AH_ALGO,
           CLI_IPSECV6_INVALID_KEY_LENGTH,
           CLI_IPSECV6_ESP_FOR_ENCR_ALGO,
           CLI_IPSECV6_INVALID_ESP_ALGO,
           CLI_IPSECV6_AH_ESP_NULL,
           CLI_IPSECV6_INVALID_DES_KEY_LENGTH,
           CLI_IPSECV6_INVALID_AES_KEY_LENGTH,
           CLI_IPSECV6_AH_KEY_FAIL,
           CLI_IPSECV6_NULL_ESP_KEY_FAIL,
           CLI_IPSECV6_INVALID_MASK,
           CLI_IPSECV6_INVALID_SEL_INDEX,
           CLI_IPSECV6_CREATE_POLICY,
           CLI_IPSECV6_INVALID_SA_INDEX_RANGE,
           CLI_IPSECV6_INVALID_TRACE,
           CLI_IPSECV6_INVALID_SELECTOR,
           CLI_IPSECV6_INVALID_KEY_LENGTH_HMACSHA1,
           CLI_IPSECV6_INVALID_KEY_LENGTH_HMACMD5,
           CLI_IPSECV6_INVALID_KEY_LENGTH_XCBCMAC,
           CLI_IPSECV6_INVALID_KEY_LENGTH_HMACSHA256,
           CLI_IPSECV6_INVALID_KEY_LENGTH_HMACSHA384,
           CLI_IPSECV6_INVALID_KEY_LENGTH_HMACSHA512,
           CLI_IPSECV6_INVALID_COMMAND,
           CLI_IPSECV6_MANUAL_AUTOMATIC,
           CLI_IPSECV6_MAX_ERROR
};

#ifdef __IP6SECV6CLI_C__
CONST CHR1 *IpSecv6CliErrString[] = {
           NULL,
           "% Direction can be either Inbound or Outbound or Any \r\n",
           "% Disable IPSec and configure SA \r\n",
           "% Entry already exists for the given index \r\n",
           "% Flag can be either Allow or Filter \r\n",
           "% Flag can be either Apply or ByPass \r\n",
           "% Invalid Address \r\n",
           "% Invalid Algorithm \r\n",
           "% Invalid Protocol \r\n",
           "% Max no of indicies for bundle is 100 \r\n",
           "% Memory allocation failure \r\n",
           "% Mode can be either 1-Transport, 2-Tunnel \r\n ",
           "% No entry for the corressponding index \r\n",
           "% Please create SA entry for the given index \r\n",
           "% Protocol can be either AH or ESP \r\n",
           "% Index Range is between 1 to 2147483647 \r\n",
           "% Set either AUTH or ENCR Algorithm \r\n",
           "% Set ENCR Algorithm only for ESP \r\n",
           "% SPI range is 255 to 2147483647 \r\n",
           "% Configure either 1-Enable or 2-Disable \r\n ",
           "% Invalid AH Algorithm \r\n",
           "% Key length must be 20 bytes-arHmac_Sha1,16bytes-arHmac_MD5,16bytes-XcbcMac,32bytes-HmacSha256,48bytes-HmacSha384,64Bytes-HmacSha512 \r\n",
           "% Please configure Proto as ESP for encr algorithm \r\n",
           "% Valid Algorithms are null, des-cbc, 3des-cbc or aes \r\n ",
           "% Both Authentication and Encryption algorithm cannot be null \r\n",
           "% The length of the key must be 8  bytes \r\n",
           "% Key can be 16 or 24 or 32 bytes \r\n ",
           "% Encryption Key not required for AH Protocol \r\n",
           "% Encryption Key not required for Null ESP Algorithm \r\n",
           "% Invalid Mask \r\n",
           "% Invalid Selector Indicies \r\n",
           "% Create policy entry for the given Policy Index \r\n ",
           "% Range for indicies is between 1 to  10000 \r\n ",
           "% Value can be set to 0 to 9 \r\n",
           "% No entry in selector or formatted selector \r\n",
           "% Key length must be 20 bytes \r\n",
           "% Key length must be 16 bytes \r\n",
           "% Key length must be 16 bytes \r\n",
           "% Key length must be 32 bytes \r\n",
           "% Key length must be 48 bytes \r\n",
           "% Key length must be 64 bytes \r\n",
           "% Invalid Command \r\n",
           "% Configure either Manual or Automatic \r\n "
};
#endif
#endif  /* __SECv6CLI_H__ */
