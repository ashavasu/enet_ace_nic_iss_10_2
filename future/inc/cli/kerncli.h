#ifndef __KERNCLI_H__
#define __KERNCLI_H__

#include "cli.h"

typedef struct _tKernCliParams
{
    UINT4    u4Command;
    UINT4    u4Arg1;
    UINT4    u4Arg2;
}tKernCliParams;

enum
{
    CLI_SHOW_PORT = 1,
    CLI_SHOW_VLAN,
    CLI_SHOW_FDB,
    CLI_SHOW_L2MC,
    CLI_KERN_TRACE,
    CLI_SHOW_INST_PORTSTATE,
    CLI_SHOW_VRRP_TBL,
    CLI_SHOW_MGMT_VLAN_LIST
};

#define KERN_MAX_ARGS 3
INT4
cli_process_kern_cmd PROTO ((tCliHandle CliHandle,UINT4 u4Command, ...));
#endif
