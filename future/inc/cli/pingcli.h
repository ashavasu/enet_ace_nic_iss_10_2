/* $Id: pingcli.h,v 1.6 2015/10/26 14:25:51 siva Exp $ */
#ifndef __PINGCLI_H__
#define __PINGCLI_H__

#include "cli.h"

#define  CLI_IP_PING             1
#define  CLI_HOST_PING           2
#define  CLI_SOURCE_IP_PING      3
#define  CLI_SOURCE_VLAN_IP_PING 4
#define  PING_CLI_MAX_ARGS  10
#define  PING_CLI_MAX_LEN   255

typedef struct
{
   UINT4                  u4IpPingDest;
   UINT4                  u4ContextId;
   UINT4                  u4IpPingSource;
   INT4                   i4IpPingAdminStatus;
   INT4                   i4IpPingTimeout;
   INT4                   i4IpPingTries;
   INT4                   i4IpPingMTU;
   UINT1                  au1HostName[DNS_MAX_QUERY_LEN];
   UINT1                  uIpPingDfBit;
}tPingEntry;

INT4 cli_process_ping_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Comamnd, ...));  
INT4 CliIpPing PROTO ((tCliHandle, tPingEntry *, BOOL1));
INT4 IpResolveNeighbor PROTO ((tPingEntry PingEntry));
#endif
