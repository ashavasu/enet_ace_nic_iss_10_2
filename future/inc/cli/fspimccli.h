/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fspimccli.h,v 1.10 2016/09/30 10:55:13 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPimCmnSPTGroupThreshold[11];
extern UINT4 FsPimCmnSPTSourceThreshold[11];
extern UINT4 FsPimCmnSPTSwitchingPeriod[11];
extern UINT4 FsPimCmnSPTRpThreshold[11];
extern UINT4 FsPimCmnSPTRpSwitchingPeriod[11];
extern UINT4 FsPimCmnRegStopRateLimitingPeriod[11];
extern UINT4 FsPimCmnGlobalTrace[11];
extern UINT4 FsPimCmnGlobalDebug[11];
extern UINT4 FsPimCmnPmbrStatus[11];
extern UINT4 FsPimCmnRouterMode[11];
extern UINT4 FsPimCmnStaticRpEnabled[11];
extern UINT4 FsPimCmnIpStatus[11];
extern UINT4 FsPimCmnIpv6Status[11];
extern UINT4 FsPimCmnSRProcessingStatus[11];
extern UINT4 FsPimCmnRefreshInterval[11];
extern UINT4 FsPimCmnSourceActiveInterval[11];
extern UINT4 FsPimCmnIpRpfVector[11];
extern UINT4 FsPimCmnIpBidirPIMStatus[11];
extern UINT4 FsPimCmnIpBidirOfferInterval[11];
extern UINT4 FsPimCmnIpBidirOfferLimit[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPimCmnSPTGroupThreshold(i4SetValFsPimCmnSPTGroupThreshold) \
 nmhSetCmn(FsPimCmnSPTGroupThreshold, 11, FsPimCmnSPTGroupThresholdSet, PimMutexLock, PimMutexUnLock, 0, 0, 0, "%i", i4SetValFsPimCmnSPTGroupThreshold)
#define nmhSetFsPimCmnSPTSourceThreshold(i4SetValFsPimCmnSPTSourceThreshold) \
 nmhSetCmn(FsPimCmnSPTSourceThreshold, 11, FsPimCmnSPTSourceThresholdSet, PimMutexLock, PimMutexUnLock, 0, 0, 0, "%i", i4SetValFsPimCmnSPTSourceThreshold)
#define nmhSetFsPimCmnSPTSwitchingPeriod(i4SetValFsPimCmnSPTSwitchingPeriod) \
 nmhSetCmn(FsPimCmnSPTSwitchingPeriod, 11, FsPimCmnSPTSwitchingPeriodSet, PimMutexLock, PimMutexUnLock, 0, 0, 0, "%i", i4SetValFsPimCmnSPTSwitchingPeriod)
#define nmhSetFsPimCmnSPTRpThreshold(i4SetValFsPimCmnSPTRpThreshold) \
 nmhSetCmn(FsPimCmnSPTRpThreshold, 11, FsPimCmnSPTRpThresholdSet, PimMutexLock, PimMutexUnLock, 0, 0, 0, "%i", i4SetValFsPimCmnSPTRpThreshold)
#define nmhSetFsPimCmnSPTRpSwitchingPeriod(i4SetValFsPimCmnSPTRpSwitchingPeriod) \
 nmhSetCmn(FsPimCmnSPTRpSwitchingPeriod, 11, FsPimCmnSPTRpSwitchingPeriodSet, PimMutexLock, PimMutexUnLock, 0, 0, 0, "%i", i4SetValFsPimCmnSPTRpSwitchingPeriod)
#define nmhSetFsPimCmnRegStopRateLimitingPeriod(i4SetValFsPimCmnRegStopRateLimitingPeriod) \
 nmhSetCmn(FsPimCmnRegStopRateLimitingPeriod, 11, FsPimCmnRegStopRateLimitingPeriodSet, PimMutexLock, PimMutexUnLock, 0, 0, 0, "%i", i4SetValFsPimCmnRegStopRateLimitingPeriod)
#define nmhSetFsPimCmnGlobalTrace(i4SetValFsPimCmnGlobalTrace) \
 nmhSetCmn(FsPimCmnGlobalTrace, 11, FsPimCmnGlobalTraceSet, PimMutexLock, PimMutexUnLock, 0, 0, 0, "%i", i4SetValFsPimCmnGlobalTrace)
#define nmhSetFsPimCmnGlobalDebug(i4SetValFsPimCmnGlobalDebug) \
 nmhSetCmn(FsPimCmnGlobalDebug, 11, FsPimCmnGlobalDebugSet, PimMutexLock, PimMutexUnLock, 0, 0, 0, "%i", i4SetValFsPimCmnGlobalDebug)
#define nmhSetFsPimCmnPmbrStatus(i4SetValFsPimCmnPmbrStatus) \
 nmhSetCmn(FsPimCmnPmbrStatus, 11, FsPimCmnPmbrStatusSet, PimMutexLock, PimMutexUnLock, 0, 0, 0, "%i", i4SetValFsPimCmnPmbrStatus)
#define nmhSetFsPimCmnRouterMode(i4SetValFsPimCmnRouterMode) \
 nmhSetCmn(FsPimCmnRouterMode, 11, FsPimCmnRouterModeSet, PimMutexLock, PimMutexUnLock, 0, 0, 0, "%i", i4SetValFsPimCmnRouterMode)
#define nmhSetFsPimCmnStaticRpEnabled(i4SetValFsPimCmnStaticRpEnabled) \
 nmhSetCmn(FsPimCmnStaticRpEnabled, 11, FsPimCmnStaticRpEnabledSet, PimMutexLock, PimMutexUnLock, 0, 0, 0, "%i", i4SetValFsPimCmnStaticRpEnabled)
#define nmhSetFsPimCmnIpStatus(i4SetValFsPimCmnIpStatus) \
 nmhSetCmn(FsPimCmnIpStatus, 11, FsPimCmnIpStatusSet, PimMutexLock, PimMutexUnLock, 0, 0, 0, "%i", i4SetValFsPimCmnIpStatus)
#define nmhSetFsPimCmnIpv6Status(i4SetValFsPimCmnIpv6Status) \
 nmhSetCmn(FsPimCmnIpv6Status, 11, FsPimCmnIpv6StatusSet, PimMutexLock, PimMutexUnLock, 0, 0, 0, "%i", i4SetValFsPimCmnIpv6Status)
#define nmhSetFsPimCmnSRProcessingStatus(i4SetValFsPimCmnSRProcessingStatus)\
 nmhSetCmn(FsPimCmnSRProcessingStatus, 11, FsPimCmnSRProcessingStatusSet, PimMutexLock, PimMutexUnLock, 0, 0, 0, "%i", i4SetValFsPimCmnSRProcessingStatus)
#define nmhSetFsPimCmnRefreshInterval(i4SetValFsPimCmnRefreshInterval) \
 nmhSetCmn(FsPimCmnRefreshInterval, 11, FsPimCmnRefreshIntervalSet, PimMutexLock, PimMutexUnLock, 0, 0, 0, "%i", i4SetValFsPimCmnRefreshInterval)
#define  nmhSetFsPimCmnSourceActiveInterval(u4SetValFsPimCmnSourceActiveInterval)\
 nmhSetCmn(FsPimCmnSourceActiveInterval, 11, FsPimCmnSourceActiveIntervalSet, PimMutexLock, PimMutexUnLock, 0, 0, 0, "%u", u4SetValFsPimCmnSourceActiveInterval)
#define  nmhSetFsPimCmnIpRpfVector(i4SetValFsPimCmnIpRpfVector)\
 nmhSetCmn(FsPimCmnIpRpfVector, 11, FsPimCmnIpRpfVectorSet, PimMutexLock, PimMutexUnLock, 0, 0, 0, "%i", i4SetValFsPimCmnIpRpfVector)
#define nmhSetFsPimCmnIpBidirPIMStatus(i4SetValFsPimCmnIpBidirPIMStatus) \
 nmhSetCmn(FsPimCmnIpBidirPIMStatus, 11, FsPimCmnIpBidirPIMStatusSet, PimMutexLock, PimMutexUnLock, 0, 0, 0, "%i", i4SetValFsPimCmnIpBidirPIMStatus)
#define nmhSetFsPimCmnIpBidirOfferInterval(i4SetValFsPimCmnIpBidirOfferInterval) \
 nmhSetCmn(FsPimCmnIpBidirOfferInterval, 11, FsPimCmnIpBidirOfferIntervalSet, PimMutexLock, PimMutexUnLock, 0, 0, 0, "%i", i4SetValFsPimCmnIpBidirOfferInterval)
#define nmhSetFsPimCmnIpBidirOfferLimit(i4SetValFsPimCmnIpBidirOfferLimit) \
 nmhSetCmn(FsPimCmnIpBidirOfferLimit, 11, FsPimCmnIpBidirOfferLimitSet, PimMutexLock, PimMutexUnLock, 0, 0, 0, "%i", i4SetValFsPimCmnIpBidirOfferLimit)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPimCmnInterfaceIfIndex[13];
extern UINT4 FsPimCmnInterfaceAddrType[13];
extern UINT4 FsPimCmnInterfaceCompId[13];
extern UINT4 FsPimCmnInterfaceDRPriority[13];
extern UINT4 FsPimCmnInterfaceLanPruneDelayPresent[13];
extern UINT4 FsPimCmnInterfaceLanDelay[13];
extern UINT4 FsPimCmnInterfaceOverrideInterval[13];
extern UINT4 FsPimCmnInterfaceAdminStatus[13];
extern UINT4 FsPimCmnInterfaceBorderBit[13];
extern UINT4 FsPimCmnInterfaceGraftRetryInterval[13];
extern UINT4 FsPimCmnInterfaceTtl[13];
extern UINT4 FsPimCmnInterfaceRateLimit[13];
extern UINT4 FsPimCmnInterfaceCompIdList[13];
extern UINT4 FsPimCmnInterfaceExtBorderBit[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPimCmnInterfaceCompId(i4FsPimCmnInterfaceIfIndex , i4FsPimCmnInterfaceAddrType ,i4SetValFsPimCmnInterfaceCompId) \
 nmhSetCmn(FsPimCmnInterfaceCompId, 13, FsPimCmnInterfaceCompIdSet, PimMutexLock, PimMutexUnLock, 0, 0, 2, "%i %i %i", i4FsPimCmnInterfaceIfIndex , i4FsPimCmnInterfaceAddrType ,i4SetValFsPimCmnInterfaceCompId)
#define nmhSetFsPimCmnInterfaceDRPriority(i4FsPimCmnInterfaceIfIndex , i4FsPimCmnInterfaceAddrType ,u4SetValFsPimCmnInterfaceDRPriority) \
 nmhSetCmn(FsPimCmnInterfaceDRPriority, 13, FsPimCmnInterfaceDRPrioritySet, PimMutexLock, PimMutexUnLock, 0, 0, 2, "%i %i %u", i4FsPimCmnInterfaceIfIndex , i4FsPimCmnInterfaceAddrType ,u4SetValFsPimCmnInterfaceDRPriority)
#define nmhSetFsPimCmnInterfaceLanPruneDelayPresent(i4FsPimCmnInterfaceIfIndex , i4FsPimCmnInterfaceAddrType ,i4SetValFsPimCmnInterfaceLanPruneDelayPresent) \
 nmhSetCmn(FsPimCmnInterfaceLanPruneDelayPresent, 13, FsPimCmnInterfaceLanPruneDelayPresentSet, PimMutexLock, PimMutexUnLock, 0, 0, 2, "%i %i %i", i4FsPimCmnInterfaceIfIndex , i4FsPimCmnInterfaceAddrType ,i4SetValFsPimCmnInterfaceLanPruneDelayPresent)
#define nmhSetFsPimCmnInterfaceLanDelay(i4FsPimCmnInterfaceIfIndex , i4FsPimCmnInterfaceAddrType ,i4SetValFsPimCmnInterfaceLanDelay) \
 nmhSetCmn(FsPimCmnInterfaceLanDelay, 13, FsPimCmnInterfaceLanDelaySet, PimMutexLock, PimMutexUnLock, 0, 0, 2, "%i %i %i", i4FsPimCmnInterfaceIfIndex , i4FsPimCmnInterfaceAddrType ,i4SetValFsPimCmnInterfaceLanDelay)
#define nmhSetFsPimCmnInterfaceOverrideInterval(i4FsPimCmnInterfaceIfIndex , i4FsPimCmnInterfaceAddrType ,i4SetValFsPimCmnInterfaceOverrideInterval) \
 nmhSetCmn(FsPimCmnInterfaceOverrideInterval, 13, FsPimCmnInterfaceOverrideIntervalSet, PimMutexLock, PimMutexUnLock, 0, 0, 2, "%i %i %i", i4FsPimCmnInterfaceIfIndex , i4FsPimCmnInterfaceAddrType ,i4SetValFsPimCmnInterfaceOverrideInterval)
#define nmhSetFsPimCmnInterfaceAdminStatus(i4FsPimCmnInterfaceIfIndex , i4FsPimCmnInterfaceAddrType ,i4SetValFsPimCmnInterfaceAdminStatus) \
 nmhSetCmn(FsPimCmnInterfaceAdminStatus, 13, FsPimCmnInterfaceAdminStatusSet, PimMutexLock, PimMutexUnLock, 0, 0, 2, "%i %i %i", i4FsPimCmnInterfaceIfIndex , i4FsPimCmnInterfaceAddrType ,i4SetValFsPimCmnInterfaceAdminStatus)
#define nmhSetFsPimCmnInterfaceBorderBit(i4FsPimCmnInterfaceIfIndex , i4FsPimCmnInterfaceAddrType ,i4SetValFsPimCmnInterfaceBorderBit) \
 nmhSetCmn(FsPimCmnInterfaceBorderBit, 13, FsPimCmnInterfaceBorderBitSet, PimMutexLock, PimMutexUnLock, 0, 0, 2, "%i %i %i", i4FsPimCmnInterfaceIfIndex , i4FsPimCmnInterfaceAddrType ,i4SetValFsPimCmnInterfaceBorderBit)
#define nmhSetFsPimCmnInterfaceGraftRetryInterval(i4FsPimCmnInterfaceIfIndex, i4FsPimCmnInterfaceAddrType, u4SetValFsPimCmnInterfaceGraftRetryInterval)\
 nmhSetCmn(FsPimCmnInterfaceGraftRetryInterval, 13, FsPimCmnInterfaceGraftRetryIntervalSet, PimMutexLock, PimMutexUnLock, 0, 0, 2, "%i %i %u", i4FsPimCmnInterfaceIfIndex , i4FsPimCmnInterfaceAddrType ,u4SetValFsPimCmnInterfaceGraftRetryInterval)
#define nmhSetFsPimCmnInterfaceTtl(i4FsPimCmnInterfaceIfIndex , i4FsPimCmnInterfaceAddrType ,i4SetValFsPimCmnInterfaceTtl) \
 nmhSetCmn(FsPimCmnInterfaceTtl, 13, FsPimCmnInterfaceTtlSet, PimMutexLock, PimMutexUnLock, 0, 0, 2, "%i %i %i", i4FsPimCmnInterfaceIfIndex , i4FsPimCmnInterfaceAddrType ,i4SetValFsPimCmnInterfaceTtl)
#define nmhSetFsPimCmnInterfaceRateLimit(i4FsPimCmnInterfaceIfIndex , i4FsPimCmnInterfaceAddrType ,i4SetValFsPimCmnInterfaceRateLimit) \
 nmhSetCmn(FsPimCmnInterfaceRateLimit, 13, FsPimCmnInterfaceRateLimitSet, PimMutexLock, PimMutexUnLock, 0, 0, 2, "%i %i %i", i4FsPimCmnInterfaceIfIndex , i4FsPimCmnInterfaceAddrType ,i4SetValFsPimCmnInterfaceRateLimit)
#define nmhSetFsPimCmnInterfaceCompIdList(i4FsPimCmnInterfaceIfIndex , i4FsPimCmnInterfaceAddrType ,pSetValFsPimCmnInterfaceCompIdList) \
 nmhSetCmn(FsPimCmnInterfaceCompIdList, 13, FsPimCmnInterfaceCompIdListSet, PimMutexLock, PimMutexUnLock, 0, 0, 2, "%i %i %s", i4FsPimCmnInterfaceIfIndex , i4FsPimCmnInterfaceAddrType ,pSetValFsPimCmnInterfaceCompIdList)
#define nmhSetFsPimCmnInterfaceExtBorderBit(i4FsPimCmnInterfaceIfIndex , i4FsPimCmnInterfaceAddrType ,i4SetValFsPimCmnInterfaceExtBorderBit)    \
 nmhSetCmn(FsPimCmnInterfaceExtBorderBit, 13, FsPimCmnInterfaceExtBorderBitSet, PimMutexLock, PimMutexUnLock, 0, 0, 2, "%i %i %i", i4FsPimCmnInterfaceIfIndex , i4FsPimCmnInterfaceAddrType ,i4SetValFsPimCmnInterfaceExtBorderBit)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPimCmnCandidateRPCompId[13];
extern UINT4 FsPimCmnCandidateRPAddrType[13];
extern UINT4 FsPimCmnCandidateRPGroupAddress[13];
extern UINT4 FsPimCmnCandidateRPGroupMasklen[13];
extern UINT4 FsPimCmnCandidateRPAddress[13];
extern UINT4 FsPimCmnCandidateRPPriority[13];
extern UINT4 FsPimCmnCandidateRPRowStatus[13];
extern UINT4 FsPimCmnCandidateRPPimMode[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPimCmnCandidateRPPriority(i4FsPimCmnCandidateRPCompId , i4FsPimCmnCandidateRPAddrType , pFsPimCmnCandidateRPGroupAddress , i4FsPimCmnCandidateRPGroupMasklen , pFsPimCmnCandidateRPAddress ,i4SetValFsPimCmnCandidateRPPriority) \
 nmhSetCmn(FsPimCmnCandidateRPPriority, 13, FsPimCmnCandidateRPPrioritySet, PimMutexLock, PimMutexUnLock, 0, 0, 5, "%i %i %s %i %s %i", i4FsPimCmnCandidateRPCompId , i4FsPimCmnCandidateRPAddrType , pFsPimCmnCandidateRPGroupAddress , i4FsPimCmnCandidateRPGroupMasklen , pFsPimCmnCandidateRPAddress ,i4SetValFsPimCmnCandidateRPPriority)
#define nmhSetFsPimCmnCandidateRPRowStatus(i4FsPimCmnCandidateRPCompId , i4FsPimCmnCandidateRPAddrType , pFsPimCmnCandidateRPGroupAddress , i4FsPimCmnCandidateRPGroupMasklen , pFsPimCmnCandidateRPAddress ,i4SetValFsPimCmnCandidateRPRowStatus) \
 nmhSetCmn(FsPimCmnCandidateRPRowStatus, 13, FsPimCmnCandidateRPRowStatusSet, PimMutexLock, PimMutexUnLock, 0, 1, 5, "%i %i %s %i %s %i", i4FsPimCmnCandidateRPCompId , i4FsPimCmnCandidateRPAddrType , pFsPimCmnCandidateRPGroupAddress , i4FsPimCmnCandidateRPGroupMasklen , pFsPimCmnCandidateRPAddress ,i4SetValFsPimCmnCandidateRPRowStatus)
#define nmhSetFsPimCmnCandidateRPPimMode(i4FsPimCmnCandidateRPCompId , i4FsPimCmnCandidateRPAddrType , pFsPimCmnCandidateRPGroupAddress , i4FsPimCmnCandidateRPGroupMasklen , pFsPimCmnCandidateRPAddress ,i4SetValFsPimCmnCandidateRPPimMode) \
 nmhSetCmn(FsPimCmnCandidateRPPimMode, 13, FsPimCmnCandidateRPPimModeSet, PimMutexLock, PimMutexUnLock, 0, 0, 5, "%i %i %s %i %s %i", i4FsPimCmnCandidateRPCompId , i4FsPimCmnCandidateRPAddrType , pFsPimCmnCandidateRPGroupAddress , i4FsPimCmnCandidateRPGroupMasklen , pFsPimCmnCandidateRPAddress ,i4SetValFsPimCmnCandidateRPPimMode)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPimCmnStaticRPSetCompId[13];
extern UINT4 FsPimCmnStaticRPAddrType[13];
extern UINT4 FsPimCmnStaticRPSetGroupAddress[13];
extern UINT4 FsPimCmnStaticRPSetGroupMasklen[13];
extern UINT4 FsPimCmnStaticRPAddress[13];
extern UINT4 FsPimCmnStaticRPRowStatus[13];
extern UINT4 FsPimCmnStaticRPEmbdFlag[13];
extern UINT4 FsPimCmnStaticRPPimMode[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPimCmnStaticRPAddress(i4FsPimCmnStaticRPSetCompId , i4FsPimCmnStaticRPAddrType , pFsPimCmnStaticRPSetGroupAddress , i4FsPimCmnStaticRPSetGroupMasklen ,pSetValFsPimCmnStaticRPAddress) \
 nmhSetCmn(FsPimCmnStaticRPAddress, 13, FsPimCmnStaticRPAddressSet, PimMutexLock, PimMutexUnLock, 0, 0, 4, "%i %i %s %i %s", i4FsPimCmnStaticRPSetCompId , i4FsPimCmnStaticRPAddrType , pFsPimCmnStaticRPSetGroupAddress , i4FsPimCmnStaticRPSetGroupMasklen ,pSetValFsPimCmnStaticRPAddress)
#define nmhSetFsPimCmnStaticRPRowStatus(i4FsPimCmnStaticRPSetCompId , i4FsPimCmnStaticRPAddrType , pFsPimCmnStaticRPSetGroupAddress , i4FsPimCmnStaticRPSetGroupMasklen ,i4SetValFsPimCmnStaticRPRowStatus) \
 nmhSetCmn(FsPimCmnStaticRPRowStatus, 13, FsPimCmnStaticRPRowStatusSet, PimMutexLock, PimMutexUnLock, 0, 1, 4, "%i %i %s %i %i", i4FsPimCmnStaticRPSetCompId , i4FsPimCmnStaticRPAddrType , pFsPimCmnStaticRPSetGroupAddress , i4FsPimCmnStaticRPSetGroupMasklen ,i4SetValFsPimCmnStaticRPRowStatus)
#define nmhSetFsPimCmnStaticRPEmbdFlag(i4FsPimCmnStaticRPSetCompId , i4FsPimCmnStaticRPAddrType , pFsPimCmnStaticRPSetGroupAddress , i4FsPimCmnStaticRPSetGroupMasklen ,i4SetValFsPimCmnStaticRPEmbdFlag)   \
 nmhSetCmn(FsPimCmnStaticRPEmbdFlag, 13, FsPimCmnStaticRPEmbdFlagSet, PimMutexLock, PimMutexUnLock, 0, 0, 4, "%i %i %s %i %i", i4FsPimCmnStaticRPSetCompId , i4FsPimCmnStaticRPAddrType , pFsPimCmnStaticRPSetGroupAddress , i4FsPimCmnStaticRPSetGroupMasklen ,i4SetValFsPimCmnStaticRPEmbdFlag)
#define nmhSetFsPimCmnStaticRPPimMode(i4FsPimCmnStaticRPSetCompId , i4FsPimCmnStaticRPAddrType , pFsPimCmnStaticRPSetGroupAddress , i4FsPimCmnStaticRPSetGroupMasklen ,i4SetValFsPimCmnStaticRPPimMode) \
 nmhSetCmn(FsPimCmnStaticRPPimMode, 13, FsPimCmnStaticRPPimModeSet, PimMutexLock, PimMutexUnLock, 0, 0, 4, "%i %i %s %i %i", i4FsPimCmnStaticRPSetCompId , i4FsPimCmnStaticRPAddrType , pFsPimCmnStaticRPSetGroupAddress , i4FsPimCmnStaticRPSetGroupMasklen ,i4SetValFsPimCmnStaticRPPimMode)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPimCmnComponentId[13];
extern UINT4 FsPimCmnComponentMode[13];
extern UINT4 FsPimCmnCompGraftRetryCount[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPimCmnComponentMode(i4FsPimCmnComponentId ,i4SetValFsPimCmnComponentMode) \
 nmhSetCmn(FsPimCmnComponentMode, 13, FsPimCmnComponentModeSet, PimMutexLock, PimMutexUnLock, 0, 0, 1, "%i %i", i4FsPimCmnComponentId ,i4SetValFsPimCmnComponentMode)
#define nmhSetFsPimCmnCompGraftRetryCount(i4FsPimCmnComponentId ,i4SetValFsPimCmnCompGraftRetryCount) \
 nmhSetCmn(FsPimCmnCompGraftRetryCount, 13, FsPimCmnCompGraftRetryCountSet, PimMutexLock, PimMutexUnLock, 0, 0, 1, "%i %i", i4FsPimCmnComponentId ,i4SetValFsPimCmnCompGraftRetryCount)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsPimCmnRegChkSumTblCompId[13];
extern UINT4 FsPimCmnRegChkSumTblRPAddrType[13];
extern UINT4 FsPimCmnRegChkSumTblRPAddress[13];
extern UINT4 FsPimCmnRPChkSumStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsPimCmnRPChkSumStatus(i4FsPimCmnRegChkSumTblCompId , i4FsPimCmnRegChkSumTblRPAddrType , pFsPimCmnRegChkSumTblRPAddress ,i4SetValFsPimCmnRPChkSumStatus) \
 nmhSetCmn(FsPimCmnRPChkSumStatus, 13, FsPimCmnRPChkSumStatusSet, PimMutexLock, PimMutexUnLock, 0, 0, 3, "%i %i %s %i", i4FsPimCmnRegChkSumTblCompId , i4FsPimCmnRegChkSumTblRPAddrType , pFsPimCmnRegChkSumTblRPAddress ,i4SetValFsPimCmnRPChkSumStatus)

#endif
