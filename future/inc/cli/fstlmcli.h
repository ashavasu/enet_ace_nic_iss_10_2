/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fstlmcli.h,v 1.2 2012/04/02 12:31:19 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsTeLinkTraceOption[11];
extern UINT4 FsTeLinkModuleStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsTeLinkTraceOption(i4SetValFsTeLinkTraceOption) \
 nmhSetCmn(FsTeLinkTraceOption, 11, FsTeLinkTraceOptionSet, TlmLock, TlmUnLock, 0, 0, 0, "%i", i4SetValFsTeLinkTraceOption)
#define nmhSetFsTeLinkModuleStatus(i4SetValFsTeLinkModuleStatus) \
 nmhSetCmn(FsTeLinkModuleStatus, 11, FsTeLinkModuleStatusSet, TlmLock, TlmUnLock, 0, 0, 0, "%i", i4SetValFsTeLinkModuleStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsTeLinkName[13];
extern UINT4 FsTeLinkRemoteRtrId[13];
extern UINT4 FsTeLinkType[13];
extern UINT4 FsTeLinkInfoType[13];
extern UINT4 FsTeLinkIfType[13];
extern UINT4 FsTeLinkIsAdvertise[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsTeLinkName(i4IfIndex ,pSetValFsTeLinkName) \
 nmhSetCmn(FsTeLinkName, 13, FsTeLinkNameSet, TlmLock, TlmUnLock, 0, 0, 1, "%i %s", i4IfIndex ,pSetValFsTeLinkName)
#define nmhSetFsTeLinkRemoteRtrId(i4IfIndex ,u4SetValFsTeLinkRemoteRtrId) \
 nmhSetCmn(FsTeLinkRemoteRtrId, 13, FsTeLinkRemoteRtrIdSet, TlmLock, TlmUnLock, 0, 0, 1, "%i %p", i4IfIndex ,u4SetValFsTeLinkRemoteRtrId)
#define nmhSetFsTeLinkType(i4IfIndex ,i4SetValFsTeLinkType) \
 nmhSetCmn(FsTeLinkType, 13, FsTeLinkTypeSet, TlmLock, TlmUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValFsTeLinkType)
#define nmhSetFsTeLinkInfoType(i4IfIndex ,i4SetValFsTeLinkInfoType) \
 nmhSetCmn(FsTeLinkInfoType, 13, FsTeLinkInfoTypeSet, TlmLock, TlmUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValFsTeLinkInfoType)
#define nmhSetFsTeLinkIfType(i4IfIndex ,i4SetValFsTeLinkIfType) \
 nmhSetCmn(FsTeLinkIfType, 13, FsTeLinkIfTypeSet, TlmLock, TlmUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValFsTeLinkIfType)
#define nmhSetFsTeLinkIsAdvertise(i4IfIndex ,i4SetValFsTeLinkIsAdvertise) \
 nmhSetCmn(FsTeLinkIsAdvertise, 13, FsTeLinkIsAdvertiseSet, TlmLock, TlmUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValFsTeLinkIsAdvertise)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsTeLinkBwThresholdIndex[13];
extern UINT4 FsTeLinkBwThreshold0[13];
extern UINT4 FsTeLinkBwThreshold1[13];
extern UINT4 FsTeLinkBwThreshold2[13];
extern UINT4 FsTeLinkBwThreshold3[13];
extern UINT4 FsTeLinkBwThreshold4[13];
extern UINT4 FsTeLinkBwThreshold5[13];
extern UINT4 FsTeLinkBwThreshold6[13];
extern UINT4 FsTeLinkBwThreshold7[13];
extern UINT4 FsTeLinkBwThreshold8[13];
extern UINT4 FsTeLinkBwThreshold9[13];
extern UINT4 FsTeLinkBwThreshold10[13];
extern UINT4 FsTeLinkBwThreshold11[13];
extern UINT4 FsTeLinkBwThreshold12[13];
extern UINT4 FsTeLinkBwThreshold13[13];
extern UINT4 FsTeLinkBwThreshold14[13];
extern UINT4 FsTeLinkBwThreshold15[13];
extern UINT4 FsTeLinkBwThresholdRowStatus[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsTeLinkBwThreshold0(i4IfIndex , i4FsTeLinkBwThresholdIndex ,i4SetValFsTeLinkBwThreshold0)\
 nmhSetCmn(FsTeLinkBwThreshold0, 13, FsTeLinkBwThreshold0Set, TlmLock, TlmUnLock, 0, 0, 2, "%i %i %i", i4IfIndex , i4FsTeLinkBwThresholdIndex ,i4SetValFsTeLinkBwThreshold0)
#define nmhSetFsTeLinkBwThreshold1(i4IfIndex , i4FsTeLinkBwThresholdIndex ,i4SetValFsTeLinkBwThreshold1) \
 nmhSetCmn(FsTeLinkBwThreshold1, 13, FsTeLinkBwThreshold1Set, TlmLock, TlmUnLock, 0, 0, 2, "%i %i %i", i4IfIndex , i4FsTeLinkBwThresholdIndex ,i4SetValFsTeLinkBwThreshold1)
#define nmhSetFsTeLinkBwThreshold2(i4IfIndex , i4FsTeLinkBwThresholdIndex ,i4SetValFsTeLinkBwThreshold2) \
 nmhSetCmn(FsTeLinkBwThreshold2, 13, FsTeLinkBwThreshold2Set, TlmLock, TlmUnLock, 0, 0, 2, "%i %i %i", i4IfIndex , i4FsTeLinkBwThresholdIndex ,i4SetValFsTeLinkBwThreshold2)
#define nmhSetFsTeLinkBwThreshold3(i4IfIndex , i4FsTeLinkBwThresholdIndex ,i4SetValFsTeLinkBwThreshold3) \
 nmhSetCmn(FsTeLinkBwThreshold3, 13, FsTeLinkBwThreshold3Set, TlmLock, TlmUnLock, 0, 0, 2, "%i %i %i", i4IfIndex , i4FsTeLinkBwThresholdIndex ,i4SetValFsTeLinkBwThreshold3)
#define nmhSetFsTeLinkBwThreshold4(i4IfIndex , i4FsTeLinkBwThresholdIndex ,i4SetValFsTeLinkBwThreshold4) \
 nmhSetCmn(FsTeLinkBwThreshold4, 13, FsTeLinkBwThreshold4Set, TlmLock, TlmUnLock, 0, 0, 2, "%i %i %i", i4IfIndex , i4FsTeLinkBwThresholdIndex ,i4SetValFsTeLinkBwThreshold4)
#define nmhSetFsTeLinkBwThreshold5(i4IfIndex , i4FsTeLinkBwThresholdIndex ,i4SetValFsTeLinkBwThreshold5) \
 nmhSetCmn(FsTeLinkBwThreshold5, 13, FsTeLinkBwThreshold5Set, TlmLock, TlmUnLock, 0, 0, 2, "%i %i %i", i4IfIndex , i4FsTeLinkBwThresholdIndex ,i4SetValFsTeLinkBwThreshold5)
#define nmhSetFsTeLinkBwThreshold6(i4IfIndex , i4FsTeLinkBwThresholdIndex ,i4SetValFsTeLinkBwThreshold6) \
 nmhSetCmn(FsTeLinkBwThreshold6, 13, FsTeLinkBwThreshold6Set, TlmLock, TlmUnLock, 0, 0, 2, "%i %i %i", i4IfIndex , i4FsTeLinkBwThresholdIndex ,i4SetValFsTeLinkBwThreshold6)
#define nmhSetFsTeLinkBwThreshold7(i4IfIndex , i4FsTeLinkBwThresholdIndex ,i4SetValFsTeLinkBwThreshold7) \
 nmhSetCmn(FsTeLinkBwThreshold7, 13, FsTeLinkBwThreshold7Set, TlmLock, TlmUnLock, 0, 0, 2, "%i %i %i", i4IfIndex , i4FsTeLinkBwThresholdIndex ,i4SetValFsTeLinkBwThreshold7)
#define nmhSetFsTeLinkBwThreshold8(i4IfIndex , i4FsTeLinkBwThresholdIndex ,i4SetValFsTeLinkBwThreshold8) \
 nmhSetCmn(FsTeLinkBwThreshold8, 13, FsTeLinkBwThreshold8Set, TlmLock, TlmUnLock, 0, 0, 2, "%i %i %i", i4IfIndex , i4FsTeLinkBwThresholdIndex ,i4SetValFsTeLinkBwThreshold8)
#define nmhSetFsTeLinkBwThreshold9(i4IfIndex , i4FsTeLinkBwThresholdIndex ,i4SetValFsTeLinkBwThreshold9) \
 nmhSetCmn(FsTeLinkBwThreshold9, 13, FsTeLinkBwThreshold9Set, TlmLock, TlmUnLock, 0, 0, 2, "%i %i %i", i4IfIndex , i4FsTeLinkBwThresholdIndex ,i4SetValFsTeLinkBwThreshold9)
#define nmhSetFsTeLinkBwThreshold10(i4IfIndex , i4FsTeLinkBwThresholdIndex ,i4SetValFsTeLinkBwThreshold10) \
 nmhSetCmn(FsTeLinkBwThreshold10, 13, FsTeLinkBwThreshold10Set, TlmLock, TlmUnLock, 0, 0, 2, "%i %i %i", i4IfIndex , i4FsTeLinkBwThresholdIndex ,i4SetValFsTeLinkBwThreshold10)
#define nmhSetFsTeLinkBwThreshold11(i4IfIndex , i4FsTeLinkBwThresholdIndex ,i4SetValFsTeLinkBwThreshold11) \
 nmhSetCmn(FsTeLinkBwThreshold11, 13, FsTeLinkBwThreshold11Set, TlmLock, TlmUnLock, 0, 0, 2, "%i %i %i", i4IfIndex , i4FsTeLinkBwThresholdIndex ,i4SetValFsTeLinkBwThreshold11)
#define nmhSetFsTeLinkBwThreshold12(i4IfIndex , i4FsTeLinkBwThresholdIndex ,i4SetValFsTeLinkBwThreshold12) \
 nmhSetCmn(FsTeLinkBwThreshold12, 13, FsTeLinkBwThreshold12Set, TlmLock, TlmUnLock, 0, 0, 2, "%i %i %i", i4IfIndex , i4FsTeLinkBwThresholdIndex ,i4SetValFsTeLinkBwThreshold12)
#define nmhSetFsTeLinkBwThreshold13(i4IfIndex , i4FsTeLinkBwThresholdIndex ,i4SetValFsTeLinkBwThreshold13) \
 nmhSetCmn(FsTeLinkBwThreshold13, 13, FsTeLinkBwThreshold13Set, TlmLock, TlmUnLock, 0, 0, 2, "%i %i %i", i4IfIndex , i4FsTeLinkBwThresholdIndex ,i4SetValFsTeLinkBwThreshold13)
#define nmhSetFsTeLinkBwThreshold14(i4IfIndex , i4FsTeLinkBwThresholdIndex ,i4SetValFsTeLinkBwThreshold14) \
 nmhSetCmn(FsTeLinkBwThreshold14, 13, FsTeLinkBwThreshold14Set, TlmLock, TlmUnLock, 0, 0, 2, "%i %i %i", i4IfIndex , i4FsTeLinkBwThresholdIndex ,i4SetValFsTeLinkBwThreshold14)
#define nmhSetFsTeLinkBwThreshold15(i4IfIndex , i4FsTeLinkBwThresholdIndex ,i4SetValFsTeLinkBwThreshold15) \
 nmhSetCmn(FsTeLinkBwThreshold15, 13, FsTeLinkBwThreshold15Set, TlmLock, TlmUnLock, 0, 0, 2, "%i %i %i", i4IfIndex , i4FsTeLinkBwThresholdIndex ,i4SetValFsTeLinkBwThreshold15)
#define nmhSetFsTeLinkBwThresholdRowStatus(i4IfIndex , i4FsTeLinkBwThresholdIndex ,i4SetValFsTeLinkBwThresholdRowStatus) \
 nmhSetCmn(FsTeLinkBwThresholdRowStatus, 13, FsTeLinkBwThresholdRowStatusSet, TlmLock, TlmUnLock, 0, 1, 2, "%i %i %i", i4IfIndex , i4FsTeLinkBwThresholdIndex ,i4SetValFsTeLinkBwThresholdRowStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsTeLinkBwThresholdForceOption[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsTeLinkBwThresholdForceOption(i4SetValFsTeLinkBwThresholdForceOption) \
 nmhSetCmn(FsTeLinkBwThresholdForceOption, 11, FsTeLinkBwThresholdForceOptionSet, TlmLock, TlmUnLock, 0, 0, 0, "%i", i4SetValFsTeLinkBwThresholdForceOption)

#endif
