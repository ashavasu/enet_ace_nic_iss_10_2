/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmparcli.h,v 1.2 2009/08/24 13:12:47 prabuc Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIArpCacheTimeout[12];
extern UINT4 FsMIArpCachePendTime[12];
extern UINT4 FsMIArpMaxRetries[12];

