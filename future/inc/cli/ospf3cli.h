/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ospf3cli.h,v 1.30 2017/12/28 10:40:14 siva Exp $
 *
 * Description: Cli related functions are handled here.
 *
 *******************************************************************/
#ifndef __OSPF3CLI_H__
#define __OSPF3CLI_H__

#include "ipv6.h"

#define OSPFV3_CLI_PRINTF(args) \
        V3OspfUnLock (); \
        CliPrintf args; \
        V3OspfLock();

#define  OSPFV3_CLI_STATUS_ACTIVE    1
#define  OSPFV3_CLI_STATUS_CW        5
#define  OSPFV3_CLI_STATUS_CG        4
#define  OSPFV3_CLI_STATUS_DESTROY   6

enum {
    OSPFV3_CLI_ROUTERID = 1,
    OSPFV3_CLI_AREAID,
    OSPFV3_CLI_DEMANDCIRCUIT,
    OSPFV3_CLI_RETRANSMITINT,
    OSPFV3_CLI_TRANSITDELAY,
    OSPFV3_CLI_PRIORITY,
    OSPFV3_CLI_HELLOINT,
    OSPFV3_CLI_DEADINT,
    OSPFV3_CLI_POLLINT,
    OSPFV3_CLI_IF_NETWORK_TYPE,
    OSPFV3_CLI_METRIC,
    OSPFV3_CLI_CONF_NBR,
    OSPFV3_CLI_IF_PASSIVE,
    OSPFV3_CLI_NBR_PROBE,
    OSPFV3_CLI_NBRPROBE_RETRANS_LIMIT,
    OSPFV3_CLI_NBR_PROBEINT,
    OSPFV3_CLI_ADMINSTAT,
    OSPFV3_CLI_AREATYPE,
    OSPFV3_CLI_STABILITY_INT,
    OSPFV3_CLI_TRANS_ROLE,
    OSPFV3_CLI_SPF_TIME,
    OSPFV3_CLI_SET_ABR_TYPE,
    OSPFV3_CLI_AREA_DEFAULT_METRIC,
    OSPFV3_CLI_AREA_DEFAULT_METRICTYPE,
    OSPFV3_CLI_VIRTUAL_LINK,
    OSPFV3_CLI_ASBR,
    OSPFV3_CLI_AREA_RANGE,
    OSPFV3_CLI_ADD_EXT_SUMM_ADDR,
    OSPFV3_CLI_REDISTRIBUTE,
    OSPFV3_CLI_DISTRIBUTE_LIST,
    OSPFV3_CLI_DEFAULT_PASSIVE,
    OSPFV3_CLI_HOST,
    OSPFV3_CLI_DEL_AREA,
    OSPFV3_CLI_NSSA_ASBR_DFRT,
    OSPFV3_CLI_REDIST_CONFIG,
    OSPFV3_CLI_SET_TRACE_LEVEL,
    OSPFV3_CLI_LSDB_LIMIT,
    OSPFV3_CLI_EXIT_OVERFLOW_STATE,
    OSPFV3_CLI_DONT_EXIT_OVERFLOW_STATE,
    OSPFV3_CLI_DEMAND_EXT,
    OSPFV3_CLI_REF_BW,
    OSPFV3_CLI_NO_REF_BW,
    OSPFV3_CLI_SHOW_ALL_INTERFACE,
    OSPFV3_CLI_SHOW_INTERFACE,
    OSPFV3_CLI_SHOW_ALL_NBR,
    OSPFV3_CLI_SHOW_NBR,
    OSPFV3_CLI_SHOW_REQ_LSA_NBR,
    OSPFV3_CLI_SHOW_REQ_LSA_ALL,
    OSPFV3_CLI_SHOW_TRANS_LSA_NBR,
    OSPFV3_CLI_SHOW_TRANS_LSA_ALL,
    OSPFV3_CLI_SHOW_VIRTUAL_LINKS,
    OSPFV3_CLI_SHOW_BORDER_ROUTERS,
    OSPFV3_CLI_SHOW_AREA_RANGE,
    OSPFV3_CLI_SHOW_SUMMARY_PREFIX,
    OSPFV3_CLI_SHOW_OSPF,
    OSPFV3_CLI_SHOW_DB,
    OSPFV3_CLI_SHOW_RT,
    OSPFV3_CLI_SHOW_AREA,
    OSPFV3_CLI_SHOW_HOST,
    OSPFV3_CLI_SHOW_RRD_INFO,
    OSPFV3_CLI_SHOW_RED_INFO,
    OSPFV3_CLI_DISTANCE,
    OSPFV3_CLI_NO_DISTANCE,
    OSPFV3_CLI_STAGGERING_STATUS,
    OSPFV3_CLI_STAGGERING_INTERVAL,
    OSPFV3_CLI_NO_STAGGERING_INTERVAL,
    OSPFV3_CLI_RESTART_SUPPORT,
    OSPFV3_CLI_NO_RESTART_SUPPORT,
    OSPFV3_CLI_HELPER_SUPPORT,
    OSPFV3_CLI_NO_HELPER_SUPPORT,
    OSPFV3_CLI_STRICT_LSA_CHECK,
    OSPFV3_CLI_HELPER_GRACE_LIMIT,
    OSPFV3_CLI_GR_ACK_STATE,
    OSPFV3_CLI_GRLSA_RETRANS_COUNT,
    OSPFV3_CLI_RESTART_REASON,
    OSPFV3_CLI_NO_RESTART_REASON,
    OSPFV3_CLI_LINK_SUPPRESS,
    OSPFV3_CLI_BFD_ADMIN_STATUS,
    OSPFV3_CLI_BFD_STATUS_ALL_INTF,
    OSPFV3_CLI_BFD_STATUS_SPECIFIC_INTF,
    OSPFV3_CLI_IPV6_OSPF_BFD,
    OSPFV3_CLI_AUTHTYPE,
    OSPFV3_CLI_SHA_KEY_ACCEPT_START,
    OSPFV3_CLI_SHA_KEY_GEN_START,
    OSPFV3_CLI_SHA_KEY_GEN_STOP,
    OSPFV3_CLI_SHA_KEY_ACCEPT_STOP,
    OSPFV3_CLI_AUTHKEY,
    OSPFV3_CLI_DEL_AUTHKEY,
    OSPFV3_CLI_AUTH_MODE,
    OSPFV3_CLI_SHOW_COUNTERS,
    OSPFV3_CLI_SHOW_PKT_STATS,
    OSPFV3_CLI_SHA_VIRT_KEY_ACCEPT_START,
    OSPFV3_CLI_SHA_VIRT_KEY_GEN_START,
    OSPFV3_CLI_SHA_VIRT_KEY_GEN_STOP,
    OSPFV3_CLI_SHA_VIRT_KEY_ACCEPT_STOP,
    OSPFV3_CLI_DEFAULT_INFO,
    OSPFV3_CLI_NO_DEFAULT_INFO,
    OSPFV3_CLI_NO_ROUTERID,
    OSPFV3_CLI_NSSA_CONFIG,
    OSPFV3_CLI_CLEAR_IPV6_OSPF,
    OSPFV3_CLI_SHOWDATABASE
};
 
/*AT Macros*/
#define OSPFV3_DST_TIME_LEN           20



#define OSPFV3_CLI_MAX_ARGUMENTS 15

#define OSPFV3_CLI_MAX_TOKENS           14
#define OSPFV3_CLI_MAX_LSA              3
#define OSPFV3_CLI_MAX_EXTLSA           15 
#define OSPFV3_CLI_MAX_BR               15
#define OSPFV3_CLI_MAX_RT               15
#define OSPFV3_CLI_MAX_SA               15
#define OSPFV3_CLI_MAX_VI               8  
#define OSPFV3_CLI_MAX_HOST             8
#define OSPFV3_CLI_MAX_AREA             5
#define OSPFV3_CLI_INVALID_VALUE       -1

#define OSPFV3_CLI_RESP_TRUE            1
#define OSPFV3_CLI_RESP_FALSE           0

#define OSPFV3_CLI_MAX_LEN_IF_NAME     12

#define OSPFV3_CLI_AS_EXT_DEF_METRIC   10

#define OSPFV3_CLI_KEY_SIZE            17

#define  OSPFV3_CLI_NO_OPTIONS              0x00    /*0000 0000*/
#define  OSPFV3_CLI_SET_HELLO_INT_BIT       0x01    /*0000 0001*/
#define  OSPFV3_CLI_SET_RETRANS_INT_BIT     0x02    /*0000 0010*/
#define  OSPFV3_CLI_SET_TRANSIT_DELAY_BIT   0x04    /*0000 0100*/
#define  OSPFV3_CLI_SET_DEAD_INT_BIT        0x08    /*0000 1000*/
#define  OSPFV3_CLI_SET_PRIORITY_BIT        0x10    /*0001 0000*/
#define  OSPFV3_CLI_SET_TAG_BIT             0x20    /*0010 0000*/
#define  OSPFV3_CLI_SET_METRIC_BIT          0x40    /*0100 0000*/
#define  OSPFV3_CLI_SET_COST_BIT            0x80    /*1000 0000*/

#define  OSPFV3_CLI_SET_VIRT_AUTH_BIT       0x10    /*1000 0000*/
#define  OSPFV3_CLI_SET_VIRT_AUTH_KEY_BIT   0x20    /*1000 0000*/
#define  OSPFV3_CLI_SET_VIRT_AUTH_MODE_BIT  0x40    /*1000 0000*/

#define  AUTHKEY_STATUS_VALID    1
#define  AUTHKEY_STATUS_INVALID  2
#define  AUTHKEY_STATUS_DELETE   3

#define OSPFV3_CLI_MAX_STRING_LEN  40 

/* used in o3cli.o for masking purpose*/
#define  OSPFV3_CLI_DB_DETAIL      0x0100
#define  OSPFV3_CLI_DB_HEX         0x0200
#define  OSPFV3_CLI_DB_SUMMARY     0x0400
#define  OSPFV3_CLI_DB_ALL_LSA     0x0800
#define  OSPFV3_CLI_DB_RTR         0x2001
#define  OSPFV3_CLI_DB_NETWORK     0x2002
#define  OSPFV3_CLI_DB_INTER_PFX   0x2003
#define  OSPFV3_CLI_DB_INTER_RTR   0x2004
#define  OSPFV3_CLI_DB_EXTERNAL    0x4005
#define  OSPFV3_CLI_DB_NSSA        0x2007
#define  OSPFV3_CLI_DB_LINK        0x0008
#define  OSPFV3_CLI_DB_INTRA_PFX   0x2009

#define  OSPFV3_CLI_DISP_MASK      0xf0ff

/*constants to get the LSA database parameters */
#define  OSPFV3_CLI_ROUTER_LSA                    1
#define  OSPFV3_CLI_NETWORK_LSA                   2
#define  OSPFV3_CLI_INTER_AREA_PREFIX_LSA         3
#define  OSPFV3_CLI_INTER_AREA_ROUTER_LSA         4
#define  OSPFV3_CLI_AS_EXT_LSA                    5
#define  OSPFV3_CLI_NSSA_LSA                      7
#define  OSPFV3_CLI_LINK_LSA                      8
#define  OSPFV3_CLI_INTRA_AREA_PREFIX_LSA         9

#define OSPFV3_CLI_SHOW_NBR_INTF     3
#define OSPFV3_CLI_SHOW_REQ_LSA_NBR_INT 3
#define OSPFV3_CLI_SHOW_TRANS_LSA_NBR_INTF 3
/*used in o3cli.o for show database summary purpose*/
#define OSPFV3_LSA_AREA_DATABASE             1
#define OSPFV3_LSA_DB_SUMMARY                2
#define OSPFV3_LSA_SELF_ORG                  3
#define OSPFV3_LSA_ADV_ROUTER                4


#define OSPFV3_CLI_MATCH_FND    1
#define OSPFV3_CLI_MATCH_NOTFND 0

#define OSPFV3_CLI_RTRID_TO_STR(pString,u4IpAddr) \
{\
     tUtlInAddr ipaddr;\
     ipaddr.u4Addr = OSIX_HTONL (u4IpAddr);\
     pString = (UINT1*)UtlInetNtoa(ipaddr);\
}

#define OSPFV3_CLI_AREAID_TO_STR(pString,u4IpAddr) \
        OSPFV3_CLI_RTRID_TO_STR(pString,u4IpAddr)
#define OSPFV3_CLI_LINKSTATEID_TO_STR(pString,u4IpAddr) \
        OSPFV3_CLI_RTRID_TO_STR(pString,u4IpAddr)

/* OSPFV3 Specific Trace Categories */
#define  OSPFV3_CLI_HP_TRC   0x00010000 
#define  OSPFV3_CLI_DDP_TRC  0x00020000 
#define  OSPFV3_CLI_LRQ_TRC  0x00040000 
#define  OSPFV3_CLI_LSU_TRC  0x00080000 
#define  OSPFV3_CLI_LAK_TRC  0x00100000 
#define  OSPFV3_CLI_ISM_TRC  0x00200000 
#define  OSPFV3_CLI_NSM_TRC  0x00400000 
#define  OSPFV3_CLI_RT_TRC   0x00800000 

#define  OSPFV3_CLI_NSSA_TRC   0x04000000
#define  OSPFV3_CLI_RAG_TRC    0x08000000

/* OSPFV3 MODULE Specific Trace Categories */
#define  OSPFV3_CLI_CONFIGURATION_TRC  0x10000000 
#define  OSPFV3_CLI_ADJACENCY_TRC      0x20000000 
#define  OSPFV3_CLI_LSDB_TRC           0x40000000 
#define  OSPFV3_CLI_PPP_TRC            0x80000000 
#define  OSPFV3_CLI_RTMODULE_TRC       0x01000000 
#define  OSPFV3_CLI_INTERFACE_TRC      0x02000000 

/* OSPF FUNCTION Specific Trace Categories */
#define  OSPFV3_CLI_FN_ENTRY  0x00001000 
#define  OSPFV3_CLI_FN_EXIT   0x00002000 

/* OSPF MEMORY RESOURCE Specific Trace Categories */
#define  OSPFV3_CLI_MEM_SUCC  0x00004000 
#define  OSPFV3_CLI_MEM_FAIL  0x00008000 

/* OSPF Packet Dumping Trace Categories */
#define  OSPFV3_CLI_DUMP_HGH_TRC  0x00000100  /* Packet HighLevel dump */
#define  OSPFV3_CLI_DUMP_LOW_TRC  0x00000200  /* Packet LowLevel dump */
#define  OSPFV3_CLI_DUMP_HEX_TRC  0x00000400  /* Packet Hex dump */
#define  OSPFV3_CLI_CRITICAL_TRC  0x00000800  /* Critical Trace */

/* OSPFv3 Graceful Restart Trace Categories */
#define OSPFV3_CLI_RESTART_TRC 0x00000001
#define OSPFV3_CLI_HELPER_TRC  0x00000002
#define OSPFV3_RM_TRC          0x00000004

#define OSPFV3_DISPLAY_STR_LEN           900

#define  OSPFV3_CLI_NO_AREA_SUMMARY        1
#define  OSPFV3_CLI_SEND_AREA_SUMMARY      2
#define  OSPFV3_CLI_STUB_AREA              2
#define  OSPFV3_CLI_NSSA_AREA              3
#define  OSPFV3_CLI_DEF_INFO_ORIG          5
#define  OSPFV3_CLI_NO_DEF_INFO_ORIG       6

#define  OSPFV3_CLI_IF_BROADCAST           1
#define  OSPFV3_IF_CLI_NBMA                2
#define  OSPFV3_IF_CLI_PTOMP               5
#define  OSPFV3_IF_CLI_PTOP                3

#define  OSPFV3_CLI_NSSA_TRANS_ALWAYS      1
#define  OSPFV3_CLI_NSSA_TRANS_CANDI       2

#define  OSPFV3_CLI_STANDARD_ABR           1
#define  OSPFV3_CLI_CISCO_ABR              2
#define  OSPFV3_CLI_IBM_ABR                3

#define  OSPFV3_CLI_TYPE7_LSA         0x2007
#define  OSPFV3_CLI_SUMMARY_LSA       0x2003

#define  OSPFV3_CLI_ADVERTISE              1
#define  OSPFV3_CLI_DONT_ADVERTISE         2
#define  OSPFV3_CLI_ALLOW_ALL              3
#define  OSPFV3_CLI_DENY_ALL               4

#define  OSPFV3_CLI_ENABLED                1
#define  OSPFV3_CLI_DISABLED               2

#define  OSPFV3_CLI_LOCAL_RT_MASK     0x0002
#define  OSPFV3_CLI_STATIC_RT_MASK    0x0004
#define  OSPFV3_CLI_RIPNG_RT_MASK     0x0010
#define  OSPFV3_CLI_BGPPLUS_RT_MASK   0x0040
#define  OSPFV3_CLI_ISISL1_RT_MASK    0x0200
#define  OSPFV3_CLI_ISISL2_RT_MASK    0x0400
#define  OSPFV3_CLI_ISISL1L2_RT_MASK  0x0600
#define  OSPFV3_CLI_ALL_RT_MASK       0x0656

#define  OSPFV3_CLI_TYPE_1_METRIC     3 
#define  OSPFV3_CLI_TYPE_2_METRIC     4 

#define  OSPFV3_CLI_DEF_METRIC_VAL    10
/* Error codes 
 * Error code values and 
 * Error strings are maintained inside the protocol module itself.
 */
enum {
    OSPFV3_CLI_NO_FREE_ENTRY = 1,
    OSPFV3_CLI_RTR_NOT_ENABLED,
    OSPFV3_CLI_RTR_ID_NOT_SET,
    OSPFV3_CLI_INV_STATUS,
    OSPFV3_CLI_ENTRY_EXISTS,
    OSPFV3_CLI_INV_ENTRY,
    OSPFV3_CLI_INV_RTR_ID,
    OSPFV3_CLI_INV_VALUE,
    OSPFV3_CLI_INV_AREA_TYPE,
    OSPFV3_CLI_INV_AREA,
    OSPFV3_CLI_NO_AREA,
    OSPFV3_CLI_INV_BACKBONE_CONF,
    OSPFV3_CLI_INV_NSSA_TRANSIT_CONF,
    OSPFV3_CLI_INV_STAB_INT,
    OSPFV3_CLI_ERR_DEL_AREA,
    OSPFV3_CLI_ERR_DEL_VAREA,
    OSPFV3_CLI_INV_EXT_AGGR,
    OSPFV3_CLI_NO_BDR_RTR,
    OSPFV3_CLI_NO_ASB_RTR,
    OSPFV3_CLI_REDIS_DISABLED,
    OSPFV3_CLI_INV_TAGTYPE,
    OSPFV3_CLI_INV_VIRT_AREA,
    OSPFV3_CLI_INV_VAREA_STATUS,
    OSPFV3_CLI_INV_IFTYPE,
    OSPFV3_CLI_ERR_NBR_EXISTS,
    OSPFV3_CLI_ERR_CONF_NBR,
    OSPFV3_CLI_INV_OSPFIF,
    OSPFV3_CLI_INV_NBR_ADDRESS,
    OSPFV3_CLI_ERR_DEMAND,
    OSPFV3_CLI_ERR_METRIC_STUB,
    OSPFV3_CLI_ERR_METRIC_NSSA,
    OSPFV3_CLI_ERR_ASBR,
    OSPFV3_CLI_ERR_GET_STATUS,
    OSPFV3_CLI_ERR_GET_STATUS_DESTROY,
    OSPFV3_CLI_ERR_TEST_STATUS_CW,
    OSPFV3_CLI_ERR_SET_STATUS_CW,
    OSPFV3_CLI_ERR_TEST_STATUS_CG,
    OSPFV3_CLI_ERR_SET_STATUS_CG,
    OSPFV3_CLI_ERR_TEST_STATUS_ACTIVE,
    OSPFV3_CLI_ERR_SET_STATUS_ACTIVE,
    OSPFV3_CLI_ERR_TEST_STATUS_DESTROY,
    OSPFV3_CLI_ERR_SET_STATUS_DESTROY,
    OSPFV3_CLI_ERR_IP6,
    OSPFV3_CLI_IP6_INVALID_ADDRESS,
    OSPFV3_CLI_INVALID_PREFIXLEN,
    OSPFV3_CLI_INV_ASSOC_RMAP,
    OSPFV3_CLI_INV_DISASSOC_RMAP,
    OSPFV3_CLI_RMAP_ASSOC_FAILED,
    OSPFV3_CLI_RESTART_INPROGRESS,
    OSPFV3_CLI_HELPER_DISABLED,
    OSPFV3_CLI_AREA_ALLOC_FAILED,
    OSPFV3_CLI_HOST_ALLOC_FAILED,
    OSPFV3_CLI_INV_DEF_METRIC,
    OSPFV3_CLI_INV_TRNS_ROLE,
    OSPFV3_CLI_RMAP_NOT_PREV_ASSOC,
    OSPFV3_CLI_INV_LINK_IFTYPE,
    OSPFV3_CLI_CONTEXTS_EXCEED_SYSTEM,
    OSPFV3_CLI_CONTEXTS_EXCEED_MAX,
    OSPFV3_CLI_BFD_ADMIN_STATUS_DISABLED,
    OSPFV3_CLI_BFD_INTF_DISABLED,
    OSPFV3_CLI_BFD_ALL_INTF_ENABLED,
    OSPFV3_CLI_INV_AUTHTYPE,
    OSPFV3_CLI_INV_AUTHMODE,
    OSPFV3_CLI_NO_AUTH_KEY,
    OSPFV3_CLI_INV_AUTHKEY_STATUS,
    OSPFV3_CLI_INV_KEY,
    OSPFV3_CLI_INV_LENGTH,
    OSPFV3_CLI_AUTH_DISABLED,
    OSPFV3_CLI_MAX_AUTH_KEY,
    OSPFV3_CLI_INV_STOPACCEPT,
    OSPFV3_CLI_INV_STARTACCEPT,
    OSPFV3_CLI_WAIT_TIMEOUT_ERR,
    OSPFV3_CLI_MISMATCH_VALUE,
    OSPFV3_CLI_NO_ACT_INT,
 OSPFV3_CLI_DEFAULT_PASSIVE_ENABLED,
    OSPFV3_CLI_SELF_LINKLOCAL_ADDR,
    OSPFV3_CLI_MAX_ERR
};

/* The error strings should be places under the switch so as to avoid 
 * redifinition. This will be visible only in modulecli.c file
 */

#ifdef __OSPF3CLI_C__

CONST CHR1  *Ospf3CliErrString [] = {
    NULL,
    "% Maximum number of entries already created\r\n",
    "% OSPFV3 not enabled\r\n",
    "% Router ID is not set\r\n",
    "% Invalid status\r\n",
    "% Entry already exists\r\n",
    "% Entry does not exist\r\n",
    "% Invalid Router ID\r\n",
    "% Invalid Value\r\n",
    "% Invalid Area type\r\n",
    "% Invalid Area address\r\n",
    "% Area not created\r\n",
    "% Configuration is not allowed on Backbone Area\r\n",
    "% Trying to set Transit Area as other than Normal area\r\n",
    "% Stability Interval can only be configured for NSSA Area\r\n",
    "% Area deletion not allowed as some interfaces are configured\r\n",
    "% Area deletion not allowed as virtual links are configured thru area\r\n",
    "% Invalid External summary address\r\n",
    "% Not a Border router\r\n",
    "% Not an AS Border router\r\n",
    "% Redistribution not enabled or router is not ASBR\r\n",
    "% Redistribution Tag type cannot be configured manually\r\n",
    "% Invalid Virtual area indices\r\n",
    "% Invalid Virtual Interface Status\r\n",
    "% Invalid Interface type\r\n",
    "% Configured neighbors present. Cannot set interface type\r\n",
    "% Neighbors can be configured only in NBMA/PTOMP networks\r\n",
    "% OSPFV3 not enabled on this interface\r\n",
    "% NBMA neighbor address should be alink local address\r\n",
    "% Demand Extension feature or IfDemand is not enabled\r\n",
    "% Area is Stub and MetricType is other than OSPFV3_METRIC\r\n",
    "% Metric type OSPFV3_METRIC is invalid for Area NSSA, and AreaSummary SEND_AREA_SUMMARY\r\n",
    "% Router is in Stub Area\r\n",
    "% Getting the interface status failure\r\n",
    "% Trying to delete non-existing entry\r\n",
    "% Status Test with CREATE_AND_WAIT Failure\r\n",
    "% Status Set with CREATE_AND_WAIT Failure\r\n",
    "% Status Test with CREATE_AND_GO Failure\r\n",
    "% Status Set with CREATE_AND_GO Failure\r\n",
    "% Status Test with ACTIVE Failure\r\n",
    "% Status Set with ACTIVE Failure\r\n",
    "% Status Test with DESTROY Failure\r\n",
    "% Status Set with DESTROY Failure\r\n",
    "% Interface does not exist in lower layer\r\n",
    "% Invalid IPv6 address\r\n",
    "% Invalid prefix length\r\n",
    "% Route Map Not Associated\r\n",
    "% Route Map Not Disassociated\r\n",
    "% Route-map association failed (probably another route map is used)\r\n",
 "% Greaceful Restart InProgress\r\n",
 "% Helper Support Disabled \r\n",
 "% Max ospf area exceeded\r\n",
 "% Max number of hosts exceeded\r\n",
 "% Default-metric can be configured only on NSSA/STUB Area\r\n",
 "% Translation-role can be configured only on NSSA Area\r\n",
 "% Route Map is not previously Associated\r\n",
 "% Link LSA Suppression feature can be configured only in POP/PTOMP networks\r\n",
 "% No of contexts supported in OSPF3 exceeded the no of contexts supported in system\r\n",
 "% Max number of contexts exceeded\r\n",
 "% BFD is not enabled for OSPF\r\n",
 "% Bfd not enabled on this Interface\r\n",
 "% Disabling Bfd on this interface is not possible, as Bfd All interface is enabled\r\n",
 "% Invalid Authentication type\r\n",
 "% Invalid Authentication Mode\r\n",
 "% No Authentication key configured for the interface\r\n",
 "% Invalid Authentication key Status\r\n",
 "% Invalid Authentication Key\r\n",
 "% Invalid Authentication Key Length\r\n",
 "% Authentication Trailer is disabled\r\n",
 "% Maximum Authentication Key Reached\r\n",
 "% KeyStopAccept time should be greater than KeyStopGenerate Time\r\n",
 "% KeyStartAccept time should be less than KeyStartGenerate Time\r\n",
 "% OSPFV3 is busy in processing. Please try again later.\r\n",
 "% Mismatch between the Input and existin configured value\r\n",
 "% No Valid IP Address available,Dynamic Router ID selection failed\r\n",
 "% Disabling Passive-interface on this interface is not possible, as default passive-interface is enabled\r\n",
 "% Link local Address of local interface cannot be configured as neighbor \r\n"
};
#endif /* __OSPF3CLI_C__ */

typedef struct _V3OsIfTbl
{
    UINT1 au1IfName[OSPFV3_CLI_MAX_LEN_IF_NAME];
    INT4 i4Interval;
    INT4 i4IfRtrPriority;
    INT4 i4Metric;
    INT4 i4IfType;
    UINT4 u4IfIndex;
    UINT4 u4AreaId;
    UINT4 u4RetxLimit;
    UINT1 u1ShowIfOption;
    UINT1 au1Rsvd[3];
} tV3OsIfTbl;

typedef struct _V3OsRRDRouteConfigTbl
{
    tIp6Addr futOspfRRDRouteDest;
    INT4  i4Metric;
    INT4  i4MetricType;
    INT4  i4TagValue;
    UINT1 u1TagFlag;
    UINT1 u1FutOspfRRDRoutePrefixLen;
    UINT1 au1Rsvd[2];
} tV3OsRRDRouteConfigTbl;

typedef struct _V3OsNeighborTbl
{
    tIp6Addr ifIpAddr;
    UINT4  u4NeighborId;
    INT4  i4NbrPriority;
    UINT4 u4IfIndex;
    UINT1 au1IfName[OSPFV3_CLI_MAX_LEN_IF_NAME];
    UINT4 u4NbrBitMask;
    UINT1 u1ShowNbrOption;
    UINT1 u1Reserved[3];
} tV3OsNeighborTbl;

typedef struct _V3OsLsa
{
    UINT4 u4NbrId;
    UINT4 u4AreaId;
    UINT1 au1IfName[OSPFV3_CLI_MAX_LEN_IF_NAME];
    UINT4 u4ShowLsaType;
    UINT4 u4ShowType;
    UINT1 u1ShowLsaOption;
    UINT1 u1AreaFlag;
    UINT1 u1Reserved[2];
} tV3OsLsa;

typedef struct _V3OsAreaStruct {
    tIp6Addr    ifIpAddr;
    UINT4       u4RouterId;
    UINT4       u4AreaId;
    UINT2       u2RetransInterval;
    UINT2       u2TransitDelay;
    UINT2       u2HelloInterval;
    UINT2       u2RtrDeadInterval;
    INT4        i4StabInterval;
    INT4        i4MetricValue;
    INT4        i4AreaSummary;
    INT4        i4StubMetricType;
    INT4        i4TransRole;
    INT4        i4ImportAsExtern;
    UINT4       u4IfIndex;
    UINT4       u4AreaBitMask;
}tV3OsAreaStruct;

typedef struct _V3OsGlobal {
    UINT4 u4RouterId;
    INT4 i4AbrType;
    INT4 i4Flag;
    INT4 i4RefBw;
    INT4 i4SpfDelayTime;
    INT4 i4SpfHoldTime;
    INT4 i4RedistProtoMask;
    INT4 i4TraceVal;
    INT4 i4ExtLsdbLimit;
}tV3OsGlobal;

typedef struct _V3OsSummaryStruct {
   tIp6Addr summPrefix;
   UINT4 u4SummCost;
   UINT4 u4SummAreaId;
   INT4  i4SummEffect;
   INT4  i4SummLsaType;
   INT4  i4SummTag;
   INT4  i4SummTrans;
   UINT1 u1SummPrefixLen;
   UINT1 au1Rsvd[3];
}tV3OsSummaryStruct;

typedef union 
{
    tV3OsIfTbl sIfConf;
    tV3OsNeighborTbl sNeighbor;
    tV3OsLsa sLsa;
    tV3OsAreaStruct sArea;
    tV3OsGlobal sGlobal;
    tV3OsSummaryStruct sSummary;
    tV3OsRRDRouteConfigTbl sRRDRoute; 
}unV3OspfCliConfigParams;


typedef struct _ShowOspfv3BRInfo  {
    UINT4     u4OspfBRIpAddr;
    tIp6Addr  ospfBRNextHop;
    UINT4     u4OspfBRArea;
    INT4      i4OspfBRRtrType;
    INT4      i4OspfBRRtType;
    INT4      i4OspfBRCost;
} tShowOspfv3BRInfo;
      

typedef struct _ShowOspfv3SAInfo  {
    tIp6Addr  ospfSAPrefix;
    UINT4     u4OspfSAPfxLen;
    UINT4     u4OspfSAArea;
    INT4      i4OspfSALsaType;
    INT4      i4OspfSAEffect;
    INT4      i4OspfSATag;
} tShowOspfv3SAInfo;
      

/* Structure containing the fields to be displayed for Virtual Link Show 
 * command.
 */
typedef struct _ShowOspfv3VIInfo  {
    UINT4     u4OspfVIAreaId;
    UINT4     u4OspfVINbr;
    INT4      i4OspfVIStatus;
    INT4      i4OspfVNbrState;
    INT4      i4OspfVITransInt;
    INT4      i4OspfVIRetransInt;
    INT4      i4OspfVIHelloInt;
    INT4      i4OspfVIDeadInt;
} tShowOspfv3VIInfo;
      

/* Structure containing the fields to be displayed for External Summary Address 
 * command.
 */
typedef struct _ShowOspfv3ExtSAInfo {
    tIp6Addr ospfExtSAPrefix;
    UINT4  u4OspfExtSAPfxLen;
    UINT4  u4OspfExtSAAreaId;
    INT4   i4OspfExtSATranslation;
    INT4   i4OspfExtSAEffect;
} tShowOspfv3ExtSAInfo;

typedef struct _ShowOspfv3v3AreaInfo {
    UINT4  u4Ospfv3AreaId;
    INT4   i4Ospfv3ImportAsExtern;
    UINT4  u4Ospfv3SpfRuns;
    UINT4  u4Ospfv3AreaBdrRtrCount;
    UINT4  u4Ospfv3AsBdrRtrCount;
    INT4   i4Ospfv3AreaSummary;
    INT4   i4Ospfv3StubMetric;
    INT4   i4Ospfv3StubMetricType;
    INT4   i4Ospfv3AreaNssaTranslatorRole;
    INT4   i4Ospfv3AreaNssaTranslatorState;
    INT4   u4Ospsfv3AreaNssaStablityInterval;
} tShowOspfv3AreaInfo;

typedef struct _ShowOspfv3v3HostInfo {
    tIp6Addr  hostIp6Addr;
    UINT4     u4Ospfv3HostAreaId;
    INT4      i4Ospfv3HostMetric;
} tShowOspfv3HostInfo;

typedef struct _Ospfv3AreaInfo {
    UINT1    u4AreaId[4];
    UINT4    u4AreaScopeLsaCount;
    UINT4    u4AreaScopeLsaChksumSum;
    UINT4    u4IndicationLsaCount;
    UINT4    u4DcBitResetLsaCount;
    INT4     i4IfCount;
    INT4     i4SpfCount;
} tOspfv3AreaInfo;

typedef struct _ShowOspfv3Info {
    tOspfv3AreaInfo  *pAreaInfo;
    UINT1     u4RouterId[4];
    UINT1     u1AbrType[20];
    UINT4     u4SpfInterval;
    UINT4     u4SpfHoldTimeInterval;
    UINT4     u4ExitOverflowInterval;
    UINT4     u4RefBw;
    INT4      i4ExtLsdbLimit;
    UINT4     u4TraceValue;
    UINT4     u4AsScopeLsaCount;
    UINT4     u4AsScopeLsaChksumSum;
    UINT4     u4AreasCount;
    UINT1     bDemandExtension;
    UINT1     bDefaultPassiveInterface;
    UINT1     bNssaAsbrDefRtTrans;
    UINT1     bAreaBdrRtr;
    UINT1     bAsBdrRtr;
    UINT1     au1Rsvd[3];
} tShowOspfv3Info;

/* Structure to store Neighbor Information */
typedef struct _NbrsInIf {
    UINT4    u4NbrIfId;      /* Neighbor Interface ID */
    UINT1    nbrRtrId[4];    /* Neighbor Router-ID */
    UINT1    au1NbrType[20]; /* Neighbor Type information to distinguish
                              * between normal OSPFv3 nbr and Active/Standby
                              * Nbr over the link */

} tNbrsInIf;

typedef struct _ShowOspfv3IfInfo {
    tNbrsInIf *pNbrsInIf;
    UINT1    u1IfName[OSPFV3_CLI_MAX_STRING_LEN];
    UINT4    u4InterfaceId;
    UINT4    u4PollInterval;
    UINT4     u4NbrProbeRxmtLimit;
    UINT4     u4NbrProbeInterval;
    UINT4     u4HelloTime;
    INT4      i4NbrCount;
    UINT1    u4AreaId[4];
    tIp6Addr ifIp6Addr;
    UINT1    u4RouterId[4];
    UINT1    u1IfType[20];
    UINT4    u4IfMetric;
    UINT1    u1IsmState[20];
    UINT1    desgRtr[4];
    tIp6Addr drIp6Addr;
    UINT1    backupDesgRtr[4];
    tIp6Addr bdrIp6Addr;
    UINT2     u2IfTransDelay;
    UINT2     u2HelloInterval;
    UINT2     u2RtrDeadInterval;
    UINT2     u2RxmtInterval;
    UINT1     u1RtrPriority;
    UINT1     u1Options[3];
    UINT1     bDcEndpt;
    UINT1     bDemandNbrProbe;
    UINT1     bPassive;
    UINT1     bLinkLsaSuppress; /* Flag to inform 
                                 * link LSA suppression status */
} tShowOspfv3IfInfo;

typedef struct _V3LsHeader {
    UINT2               u2LsaAge;
                            /* the time in seconds since the link state advt
                             * was originated */
    UINT2               u2LsaType;
                            /* the type of the link state advt.  */
    UINT1              linkStateId[4];
                            /* this field distinguish between multiple LSA's
        * whose LS type is same and advertise by the 
        * same router. */
    UINT1              advRtrId[4];
                            /* the router ID of the router that originated
                             * the advt.  */
    INT4               lsaSeqNum;
                            /* identifies the instance of the advertisement */
    UINT2               u2LsaChksum;
                            /* the checksum of the complete contents of the
                             * link state advt */
    UINT2               u2LsaLen;
                            /* length in bytes of the advt.
                             * including the 16 -byte header */
}  tV3LsHeader;


typedef struct _ShowLsaReqInfo {
    UINT1        nbrRtrId[4];
    tIp6Addr     nbrIpv6Addr;
    tV3LsHeader  lsHeader;
} tShowLsaReqInfo;

typedef struct _ShowLsaRxmtInfo {
    UINT1        nbrRtrId[4];
    tIp6Addr     nbrIpv6Addr;
    tV3LsHeader  lsHeader;
} tShowLsaRxmtInfo;

typedef struct _ShowRedistConfigInfo {
    tSNMP_OCTET_STRING_TYPE  configPfx;
    UINT4       u4Metric;
    UINT4       u4Tag;
    UINT1       au1MetricType[OSPFV3_CLI_MAX_STRING_LEN];
    UINT1       au1TagType[OSPFV3_CLI_MAX_STRING_LEN];
    UINT1       u1PrefixLength;
    UINT1       u1Rsvd[3];
} tShowRedistConfigInfo;

#define CLI_MODE_ROUTER_OSPF3 "ospfv3-rtr-cfg"

INT1 Ospfv3GetRouterCfgPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1DispStr));
INT4 cli_process_ospfv3_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command, ...));
INT4 Ospfv3ShowRunningConfigScalars (tCliHandle CliHandle);
INT4 Ospfv3ShowRunningConfigTables (tCliHandle CliHandle, UINT4 u4Ospfv3AuthCxtId);
INT4 Ospfv3ShowRunningConfigInCxt (tCliHandle CliHandle, UINT4 u4Module, UINT4 u4ContextId);
INT4 Ospfv3ShowRunningConfigInterface (tCliHandle CliHandle);
INT4 Ospfv3ShowRunningConfigInterfaceDetails (tCliHandle CliHandle,
                                              INT4 i4IfIndex);
VOID IssOspf3ShowDebugging (tCliHandle cliHandle);
INT4 Ospf3SetTraceValue (INT4 i4TrapLevel,UINT1 u1LoggingCmd);

#ifdef OSPFV3_RELQ_TEST_WANTED

#define OSPFV3_CLI_TEST_POPULATE_LSA                1
#define OSPFV3_CLI_TEST_START_RT_CALCULATION        2
#define OSPFV3_CLI_TEST_DEL_ALL_LSA                 3
INT4 cli_process_ospfv3_test_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command, ...));

#endif/* OSPFV3_RELQ_TEST_WANTED */

#ifdef ROUTEMAP_WANTED
INT4 V3OspfSetRouteDistance PROTO ((tCliHandle CliHandle, INT4 i4Distance,
                                  UINT1 *pu1RMapName));
INT4 V3OspfSetNoRouteDistance PROTO ((tCliHandle CliHandle, UINT1 *pu1RMapName));
#endif

#endif /* __OSPF3CLI_H__ */
