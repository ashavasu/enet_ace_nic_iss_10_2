/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lsppclig.h,v 1.14 2015/02/18 11:19:08 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              clicmds.def and clig.c file.
 ********************************************************************/


#ifndef __LSPPCLIG_H__
#define __LSPPCLIG_H__

#include "cli.h"

enum
{
    CLI_LSPP_FSLSPPGLOBALCONFIGTABLE,
    CLI_LSPP_FSLSPPPINGTRACETABLE
};




#define CLI_LSPP_MAX_ARGS 60

enum
{
    CLI_LSPP_CONTEXT_NOT_EXIST = 0,
    CLI_LSPP_MODULE_SHUTDOWN,
    CLI_LSPP_GLOBAL_CONFIG_ENTRY_NOT_EXIST,
    CLI_LSPP_INVALID_BASE_OID,
    CLI_LSPP_INVALID_MIP_NODE_ID,
    CLI_LSPP_INVALID_SWEEP_START_VALUE,
    CLI_LSPP_INVALID_SWEEP_END_VALUE,
    CLI_LSPP_INTF_LBL_TLV_CANNOT_SET,
    CLI_LSPP_SAMESEQ_IN_CONJUCTION,
    CLI_LSPP_SWEEP_IN_CONJUCTION,
    CLI_LSPP_BURST_IN_CONJUCTION,
    CLI_LSPP_REV_PATH_VERIFY_INVALID,
    CLI_LSPP_INVALID_OPTION_FOR_PW,
    CLI_LSPP_INTERMEDIATE_CV_INVALID,
    CLI_LSPP_INTER_CV_MANDATE_FIELDS_NOT_EXIST,
    CLI_LSPP_TTL_NOT_CONFIGRD_FOR_INTER_CV,
    CLI_LSPP_INVALID_ENCAP,
    CLI_LSPP_INVALID_ENCAP_FOR_PW,
    CLI_LSPP_ENCAP_VALID_FOR_PW_ONLY,
    CLI_LSPP_ENCAP_INVALID_FOR_RSVP,
    CLI_LSPP_INVALID_REPLY_MODE_FOR_RSVP,
    LSPP_CLI_SEM_TAKE_FAILED,
    LSPP_CLI_GET_PING_TRACE_ENTRY_FAILED,
    LSPP_CLI_GET_ECHO_SEQUENCE_ENTRY_FAILED,
    LSPP_CLI_GET_HOP_ENTRY_FAILED,
    LSPP_CLI_GET_MEG_NAME_FAILED,
    LSPP_CLI_GET_GLOBAL_CONFIG_ENTRY_FAILED,
    LSPP_CLI_DESTROY_PINGTRACE_ENTRY_FAILED,
    LSPP_CLI_INVALID_REPLY_MODE_FOR_PW,
    CLI_LSPP_INVALID_REPLY_MODE_FOR_ENCAP,
    CLI_LSPP_INVALID_REPEAT_COUNT,
    CLI_LSPP_INVALID_BFD_AGEOUT_VALUE,
    CLI_LSPP_INVALID_MIP_GLOBAL_ID,
    CLI_LSPP_INVALID_MAX_AGE_OUT_TIME,
    CLI_LSPP_NOT_SUPPORT_ERR,
    CLI_LSPP_MAX_ERR
};

#ifdef __LSPPCLIG_C__
CONST CHR1  *LsppCliErrString [] = {

    "\r\n%% Context with the given context name not exist !!!\r\n",
    "\r\n%% Module status shut down !!!\r\n",
    "\r\n%% Global config entry with the given context name not present \r\n",
    "\r\n%% Base OID of the given path Invalid !!!\r\n",
    "\r\n%% Invalid MIP Node Id given as input\r\n",
    "\r\n%% Invalid Sweep start value given as input\r\n",
    "\r\n%% Invalid Sweep end value given as input\r\n",
    "\r\n%% Interface and Label stack TLV cannot be enabled, if DsMap is not enabled\r\n",
    "\r\n%% Same-sequence number  option should not be enabled in conjuction with sweep and same-sequence number\r\n",
    "\r\n%% Sweep option should not be enabled in conjuction with burst and same-sequence number\r\n",
    "\r\n%% Burst option should not be enabled in conjuction with sweep and same-sequence number\r\n",
    "\r\n%% Reverse Path verification is only for MPLS-TP LSPs whose reverse path must be a LSP\r\n",
    "\r\n%% Force Explicit Null or TTL should not be configured while monitoring pseudowire\r\n",
    "\r\n%% Global Id,Node Id should be configured only for path type MEP\r\n",
    "\r\n%% Connectivity verification to the intermediate node is only for MPLS-TP\r\n",
    "\r\n%% TTL must be configured when performing connectivity verification for intermediate node\r\n",
    "\r\n%% Encapsulation type must be either 'mpls-ip' or 'mpls-ach' when the  reply mode is configured to 2 (ip) or DSCP value is configured\r\n",
    "\r\n%% Encapsulation type must be VCCV NEGOTIATED for PWs\r\n",
    "\r\n%% Encapsulation type VCCV negotiated is valid only for PWs\r\n",
    "\r\n%% Invalid Encapsulation type for RSVP-TE\r\n",
    "\r\n%% Invalid Reply mode for RSVP-TE path\r\n",
    "\r\n%% Sem take failed while printing PingTrace output\r\n",
    "\r\n%% Get Ping Trace entry to print the PingTrace output failed\r\n",
    "\r\n%% Get Echo sequence entry to print the PingTrace output failed\r\n",
    "\r\n%% Get Hop entry to print the PingTrace output failed\r\n",
    "\r\n%% Unable to get MEG ME name from the external port function\r\n",
    "\r\n%% Get Global Config entry to print the PingTrace output failed\r\n",
    "\r\n%% Deletion of Ping Trace entry after the completion of ping failed\r\n",
    "\r\n%% Reply Mode for Pw must be only via application control channel\r\n",
    "\r\n%% Reply Mode is not valid for given encapsulation\r\n",
    "\r\n%% Invalid Repeat Count\r\n",
    "\r\n%% Invalid Bfd AgeOut Timer Value\r\n",
    "\r\n%% Invalid MIP Global Id given as input\r\n",
    "\r\n%% Invalid LSP MAX Age Out Time\r\n",
    "\r\n%% This Scenario is not supported\r\n"
};
#endif


INT4 cli_process_Lspp_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);


#endif
