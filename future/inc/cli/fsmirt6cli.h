/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmirt6cli.h,v 1.4 2015/10/19 12:19:29 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIRtm6GlobalTrace[11];
extern UINT4 FsMIRtm6ThrotLimit[11];

extern UINT4 FsMIRtm6MaximumBgpRoutes[12];
extern UINT4 FsMIRtm6MaximumRipRoutes[12];
extern UINT4 FsMIRtm6MaximumStaticRoutes[12];
extern UINT4 FsMIRtm6MaximumOspfRoutes[12];
extern UINT4 FsMIRtm6MaximumISISRoutes[12];

extern UINT4 FsMIRtm6ContextId[12];
extern UINT4 FsMIRrd6RouterId[12];
extern UINT4 FsMIRrd6FilterByOspfTag[12];
extern UINT4 FsMIRrd6FilterOspfTag[12];
extern UINT4 FsMIRrd6FilterOspfTagMask[12];
extern UINT4 FsMIRrd6RouterASNumber[12];
extern UINT4 FsMIRrd6AdminStatus[12];
extern UINT4 FsMIRrd6Trace[12];
extern UINT4 FsMIRtm6StaticRouteDistance[12];

extern UINT4 FsMIRrd6ControlDestIpAddress[12];
extern UINT4 FsMIRrd6ControlNetMaskLen[12];
extern UINT4 FsMIRrd6ControlSourceProto[12];
extern UINT4 FsMIRrd6ControlDestProto[12];
extern UINT4 FsMIRrd6ControlRouteExportFlag[12];
extern UINT4 FsMIRrd6ControlRowStatus[12];

extern UINT4 FsMIRrd6RoutingProtoId[12];
extern UINT4 FsMIRrd6AllowOspfAreaRoutes[12];
extern UINT4 FsMIRrd6AllowOspfExtRoutes[12];

