/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: dhcpcli.h,v 1.13 2014/03/11 13:27:00 siva Exp $
 * 
 * Description: This file contains macros to the CLI commands and
 *   structures for the DHCP server
 ********************************************************************/

#ifndef __DHCPCLI_H__
#define __DHCPCLI_H__

#include "cli.h"

enum {
    CLI_DHCPSRV_SHOW_INFO = 1,
    CLI_DHCPSRV_SHOW_POOLS,
    CLI_DHCPSRV_SHOW_BINDING,
    CLI_DHCPSRV_SHOW_STATS,
    CLI_DHCPSRV_SERVICE,
    CLI_DHCPSRV_NO_SERVICE,
    CLI_DHCPSRV_POOL,
    CLI_DHCPSRV_NO_POOL,
    CLI_DHCPSRV_NEXT_SERVER,
    CLI_DHCPSRV_NO_NEXT_SERVER,
    CLI_DHCPSRV_BOOTFILE,
    CLI_DHCPSRV_NO_BOOTFILE,
    CLI_DHCPSRV_PARAMS,
    CLI_DHCPSRV_NO_PARAMS,
    CLI_DHCPSRV_OPTION,
    CLI_DHCPSRV_NO_OPTION,
    CLI_DHCPSRV_NETWORK,
    CLI_DHCPSRV_NO_NETWORK,
    CLI_DHCPSRV_EXCLUDED_ADDR,
    CLI_DHCPSRV_NO_EXCLUDED_ADDR,
    CLI_DHCPSRV_DOMAIN_NAME,
    CLI_DHCPSRV_NO_DOMAIN_NAME,
    CLI_DHCPSRV_DNS_SERVER,
    CLI_DHCPSRV_NO_DNS_SERVER,
    CLI_DHCPSRV_NETBIOS_SERVER,
    CLI_DHCPSRV_NO_NETBIOS_SERVER,
    CLI_DHCPSRV_NETBIOS_NODETYPE,
    CLI_DHCPSRV_NO_NETBIOS_NODETYPE,
    CLI_DHCPSRV_DEFAULT_ROUTER,
    CLI_DHCPSRV_NO_DEFAULT_ROUTER,
    CLI_DHCPSRV_POOL_OPTION,
    CLI_DHCPSRV_NO_POOL_OPTION,
    CLI_DHCPSRV_LEASE,
    CLI_DHCPSRV_NO_LEASE,
    CLI_DHCPSRV_THRESHOLD,
    CLI_DHCPSRV_NO_THRESHOLD,
    CLI_DHCPSRV_HOST_IP,
    CLI_DHCPSRV_NO_HOST_IP,
    CLI_DHCPSRV_NO_HOST,
    CLI_DHCPSRV_HOST_OPTION,
    CLI_DHCPSRV_NO_HOST_OPTION,
    CLI_DHCPSRV_DEBUG,
    CLI_DHCPSRV_NO_DEBUG,
    CLI_DHCPSRV_NO_EXCLUDED_ADDR_ALL,
    CLI_DHCPSRV_EXCLUDED_ADDR_ALL,
    CLI_DHCPSRV_MAX_COMMANDS,
    CLI_DHCPSRV_CLEAR_STATS,
    CLI_DHCPSRV_SIP_SERVER,
    CLI_DHCPSRV_NO_SIP_SERVER,
    CLI_DHCPSRV_SIP_SERVER_GLOBAL,
    CLI_DHCPSRV_NO_SIP_SERVER_GLOBAL,
    CLI_DHCPSRV_SIP_SERVER_HOST,
    CLI_DHCPSRV_NO_SIP_SERVER_HOST,
    CLI_DHCPSRV_NTP_SERVER,
    CLI_DHCPSRV_NO_NTP_SERVER,
    CLI_DHCPSRV_NTP_SERVER_GLOBAL,
    CLI_DHCPSRV_NO_NTP_SERVER_GLOBAL,
    CLI_DHCPSRV_DNS_SERVER_GLOBAL,
    CLI_DHCPSRV_NO_DNS_SERVER_GLOBAL,
    CLI_DHCPSRV_NTP_SERVER_HOST,
    CLI_DHCPSRV_NO_NTP_SERVER_HOST,
    CLI_DHCPSRV_DNS_SERVER_HOST,
    CLI_DHCPSRV_NO_DNS_SERVER_HOST,
    CLI_DHCPSRV_VENDOR_SPECIFIC,
    CLI_DHCPSRV_NO_VENDOR_SPECIFIC
};

/* max no of variable inputs to cli parser */
#define DHCPS_CLI_MAX_ARGS       10

/* Command constants for ip dhcp command */
#define DHCPSRV_PING_PACKETS      1
#define DHCPSRV_OFFER_REUSE       2
#define DHCPSRV_BINDING_ADDR      3


/* Command constants for option command */
#define DHCPSRV_OPTION_ASCII      1
#define DHCPSRV_OPTION_HEX        2
#define DHCPSRV_OPTION_IP         3


/* Command constants for network command */
#define NETWORK_MASK_DECIMAL      1
#define NETWORK_MASK_BITS         2
#define NETWORK_DEFAULT_MASK      3


/* Command constants for netbios node-type command */
#define NETBIOS_NODE              0xff
#define NETBIOS_B_NODE            0x1
#define NETBIOS_H_NODE            0x8
#define NETBIOS_M_NODE            0x4
#define NETBIOS_P_NODE            0x2


/* Command constants for lease command */
#define LEASE_INFINITE            1
#define LEASE_FINITE              2

/* Command constants for debug dhcpserver command */
#define DEBUG_DHCPSRV_NONE        0
#define DEBUG_DHCPSRV_ALL         0x0000ffff
#define DEBUG_DHCPSRV_EVENTS      0x00000001
#define DEBUG_DHCPSRV_PACKETS     0x00000008
#define DEBUG_DHCPSRV_ERRORS      0x00000040
#define DEBUG_DHCPSRV_BIND        0x00000010



#define DHCPSRV_CLI_CLNTID_SIZE   20 /* 5 extra bytes addded */
#define DHCPSRV_CLI_BOOTFILE_SIZE 64
#define DHCPSRV_CLI_OPTVAL_SIZE   256  
#define HW_ADDR_STR_LEN          20
#define MAX_DATE_LEN             100
#define IP_ADDR_STR_LEN          17
#define IP_ADDR_SIZE             4
#define DHCPSRV_LEASE_PERIOD      3600
#define DHCPSRV_BIND_OFFERD 1
#define DHCPSRV_BIND_ASSIGND 2


#define DHCPSRV_MAX_ADDR_POOLS    DHCP_MAX_POOL_REC
#define DHCPSRV_MAX_EXCL_POOLS    DHCP_MAX_POOL_REC 
#define DHCPSRV_MAX_BINDS         DHCP_MAX_BIND_REC
#define DHCPSRV_MAX_HOSTS         DHCP_MAX_HOST_REC
#define DHCPSRV_MAX_OPTIONS       64 /* total options(77) - sys_options(13) */
#define DHCPSRV_MAX_CONF_SIZE     10
#define DHCPSRV_MAX_STATS         20
#define DHCPSRV_MAX_BT_CONF_SIZE  10       

#define DHCPSRV_CLI_CONF_SIZE         ( CLI_MAX_HEADER_SIZE + \
                                        CLI_MAX_COLUMN_WIDTH * \
                                        DHCPSRV_MAX_CONF_SIZE )

#define DHCPSRV_CLI_POOL_SIZE         ( CLI_MAX_HEADER_SIZE + \
                                        CLI_MAX_COLUMN_WIDTH * \
                                        DHCPSRV_MAX_ADDR_POOLS )

#define DHCPSRV_CLI_EXCL_POOL_SIZE    ( CLI_MAX_HEADER_SIZE + \
                                        CLI_MAX_COLUMN_WIDTH * \
                                        DHCPSRV_MAX_EXCL_POOLS )
                                  
#define DHCPSRV_CLI_MAX_GBLOPT_SIZE  ( CLI_MAX_HEADER_SIZE + \
                                       CLI_MAX_COLUMN_WIDTH * \
                                       DHCPSRV_MAX_OPTIONS )
                                  
#define DHCPSRV_CLI_MAX_SUBOPT_SIZE  ( CLI_MAX_HEADER_SIZE + \
                                       CLI_MAX_COLUMN_WIDTH * \
                                       DHCPSRV_MAX_ADDR_POOLS * \
                                       DHCPSRV_MAX_OPTIONS )

#define DHCP_CLI_MAX_HOSTOPT_SIZE  ( CLI_MAX_HEADER_SIZE + \
                                     CLI_MAX_COLUMN_WIDTH * \
                                     DHCPSRV_MAX_HOSTS * \
                                     DHCPSRV_MAX_OPTIONS )

#define DHCP_CLI_MAX_STATS_SIZE    ( CLI_MAX_HEADER_SIZE + \
                                     CLI_MAX_COLUMN_WIDTH * \
                                     DHCPSRV_MAX_STATS )

#define DHCP_CLI_MAX_BINDS_SIZE    ( CLI_MAX_HEADER_SIZE + \
                                     CLI_MAX_COLUMN_WIDTH * \
                                     DHCPSRV_MAX_BINDS )

#define DHCP_CLI_MAX_BOOTCONF_SIZE ( CLI_MAX_HEADER_SIZE + \
                                     CLI_MAX_COLUMN_WIDTH * \
                                     DHCPSRV_MAX_BT_CONF_SIZE )


#define DHCP_CLI_MAX_OPTIONS_DIS_SIZE ( CLI_MAX_HEADER_SIZE + \
                                     CLI_MAX_COLUMN_WIDTH * \
         DHCPSRV_MAX_OPTIONS)


/* Structure for addition of DHCP client address pool */
typedef struct addpool
{
   UINT4 u4PoolIndex;
   UINT4 u4PortNum;
   UINT4 u4Subnet;
   UINT4 u4SubnetMask;
   UINT4 u4StartIp;
   UINT4 u4EndIp;
   UINT4 u4LeaseTime;
}tDhcpCliAddPool;  

/* Structure for adding a option for a specic host */
typedef struct hostoption
{
   UINT4 u4PoolIndex; 
   UINT4 u4HostType;
   UINT1  au1ClientId[DHCPSRV_CLI_CLNTID_SIZE];
   UINT4 u4Type;
   UINT4 u4Len;
   UINT1 au1OptValue[DHCPSRV_CLI_OPTVAL_SIZE];
}tDhcpCliAddHostOpt;   


/* Error codes 
 * Error code values and 
 * Error strings are maintained inside the protocol module itself.
 */
enum {
    CLI_DHCPS_INV_TRC = 1,
    CLI_DHCPS_INV_STATUS,
    CLI_DHCPS_INV_POOL_ID,
    CLI_DHCPS_INV_BOOTSRV_ADDR,
    CLI_DHCPS_INV_TIMEOUT_VALUE,
    CLI_DHCPS_INV_BIND_ADDR,
    CLI_DHCPS_INV_ENTRY,
    CLI_DHCPS_INV_OPTION_LEN,
    CLI_DHCPS_INV_IPADDR,
    CLI_DHCPS_INV_IFINDEX,
    CLI_DHCPS_INV_SUBNETMASK,
    CLI_DHCPS_INV_LEASETIME,
    CLI_DHCPS_INV_STARTIP,
    CLI_DHCPS_INV_ENDIP,
    CLI_DHCPS_INV_OPTION_TYPE,
    CLI_DHCPS_INV_EXCL_POOL,
    CLI_DHCPS_ENTRY_NOT_CREATED,
    CLI_DHCPS_CONFIGURE_START_IPADDR,
    CLI_DHCPS_CONFIGURE_END_IPADDR,
    CLI_DHCPS_IPADDR_RANGE,
    CLI_DHCPS_LEASETIME,
    CLI_DHCPS_START_IPADDR_NOT_SUBNET,
    CLI_DHCPS_END_IPADDR_NOT_SUBNET,
    CLI_DHCPS_ADDRESSPOOL_SUBNET_ERR,
    CLI_DHCPS_STATIC_HOST_OVERLAP,
    CLI_DHCPS_INV_THRESHOLD,
    CLI_DHCPS_MAX_ERR 
};

/* The error strings should be places under the switch so as to avoid redifinition
 * This will be visible only in modulecli.c file
 */

#ifdef __DHCPCLI_C__

CONST CHR1  *DhcpSrvCliErrString [] = {
    NULL,
    "\r% Invalid Trace Level\r\n",
    "\r% Invalid status option\r\n",
    "\r% DHCP Pool ID doesn't exists\r\n",
    "\r% Invalid Boot Server IP\r\n",
    "\r% Invalid Timeout value specified\r\n",
    "\r% Bind Entry does not exist\r\n",
    "\r% Invalid Index. Entry not created\r\n",
    "\r% Invalid Option length specified\r\n",
    "\r% Invalid IP Address specified\r\n",
    "\r% Invalid Interface Index specified\r\n",
    "\r% Invalid Subnet Mask specified\r\n",
    "\r% Invalid Lease Time\r\n",
    "\r% Invalid Start IP for exclude address pool\r\n",
    "\r% Invalid End IP for exclude address pool\r\n",
    "\r% Invalid configuration\r\n",
    "\r% Entry is not created\r\n",
    "\r% Invalid subnet pool start IP address\r\n",
    "\r% Should Configure start IPAddress for the Pool\r\n",
    "\r% Should configure End IP address for the Pool\r\n",
    "\r% Start IP address should be Less than End IP address\r\n",
    "\r% Lease Time should not be zero \r\n",
    "\r% Start IP address doesn't belong to this subnet\r\n",
    "\r% End IP address doesn't belong to this subnet\r\n",
    "\r% Another address pool has same subnet-Please specify some other subnet\r\n",
    "\r% Static IP is in Exclude-address range\r\n",
    "\r% Invalid threshold level for the pool\r\n"
};

#endif

#define CLI_DHCP_POOL "dhcp-pool-"
INT4 cli_process_dhcp_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Comamnd, ...));
INT1 DhcpSrvGetPoolCfgPrompt PROTO ((INT1 *, INT1 *));

#endif /* __DHCPCLI_H__ */
