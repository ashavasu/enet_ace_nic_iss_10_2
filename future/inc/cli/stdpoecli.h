/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdpoecli.h,v 1.2 2008/10/31 13:09:31 premap-iss Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 PethPsePortGroupIndex[11];
extern UINT4 PethPsePortIndex[11];
extern UINT4 PethPsePortAdminEnable[11];
extern UINT4 PethPsePortPowerPairs[11];
extern UINT4 PethPsePortPowerPriority[11];
extern UINT4 PethPsePortType[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetPethPsePortAdminEnable(i4PethPsePortGroupIndex , i4PethPsePortIndex ,i4SetValPethPsePortAdminEnable)	\
	nmhSetCmn(PethPsePortAdminEnable, 11, PethPsePortAdminEnableSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4PethPsePortGroupIndex , i4PethPsePortIndex ,i4SetValPethPsePortAdminEnable)
#define nmhSetPethPsePortPowerPairs(i4PethPsePortGroupIndex , i4PethPsePortIndex ,i4SetValPethPsePortPowerPairs)	\
	nmhSetCmn(PethPsePortPowerPairs, 11, PethPsePortPowerPairsSet, NULL, NULL, 0, 0, 2, "%i %i %i", i4PethPsePortGroupIndex , i4PethPsePortIndex ,i4SetValPethPsePortPowerPairs)
#define nmhSetPethPsePortPowerPriority(i4PethPsePortGroupIndex , i4PethPsePortIndex ,i4SetValPethPsePortPowerPriority)	\
	nmhSetCmn(PethPsePortPowerPriority, 11, PethPsePortPowerPrioritySet, NULL, NULL, 0, 0, 2, "%i %i %i", i4PethPsePortGroupIndex , i4PethPsePortIndex ,i4SetValPethPsePortPowerPriority)
#define nmhSetPethPsePortType(i4PethPsePortGroupIndex , i4PethPsePortIndex ,pSetValPethPsePortType)	\
	nmhSetCmn(PethPsePortType, 11, PethPsePortTypeSet, NULL, NULL, 0, 0, 2, "%i %i %s", i4PethPsePortGroupIndex , i4PethPsePortIndex ,pSetValPethPsePortType)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 PethMainPseGroupIndex[12];
extern UINT4 PethMainPseUsageThreshold[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetPethMainPseUsageThreshold(i4PethMainPseGroupIndex ,i4SetValPethMainPseUsageThreshold)	\
	nmhSetCmn(PethMainPseUsageThreshold, 12, PethMainPseUsageThresholdSet, NULL, NULL, 0, 0, 1, "%i %i", i4PethMainPseGroupIndex ,i4SetValPethMainPseUsageThreshold)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 PethNotificationControlGroupIndex[12];
extern UINT4 PethNotificationControlEnable[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetPethNotificationControlEnable(i4PethNotificationControlGroupIndex ,i4SetValPethNotificationControlEnable)	\
	nmhSetCmn(PethNotificationControlEnable, 12, PethNotificationControlEnableSet, NULL, NULL, 0, 0, 1, "%i %i", i4PethNotificationControlGroupIndex ,i4SetValPethNotificationControlEnable)

#endif
