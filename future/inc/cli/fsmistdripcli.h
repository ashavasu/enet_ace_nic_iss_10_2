/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmistdripcli.h,v 1.3 2009/09/15 14:58:27 prabuc Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIStdRip2IfStatAddress[11];
extern UINT4 FsMIStdRip2IfStatStatus[11];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIStdRip2IfConfAddress[11];
extern UINT4 FsMIStdRip2IfConfDomain[11];
extern UINT4 FsMIStdRip2IfConfAuthType[11];
extern UINT4 FsMIStdRip2IfConfAuthKey[11];
extern UINT4 FsMIStdRip2IfConfSend[11];
extern UINT4 FsMIStdRip2IfConfReceive[11];
extern UINT4 FsMIStdRip2IfConfDefaultMetric[11];
extern UINT4 FsMIStdRip2IfConfStatus[11];
extern UINT4 FsMIStdRip2IfConfSrcAddress[11];
