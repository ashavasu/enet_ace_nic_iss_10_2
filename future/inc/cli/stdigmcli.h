/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdigmcli.h,v 1.3 2010/12/22 11:44:47 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IgmpInterfaceIfIndex[11];
extern UINT4 IgmpInterfaceQueryInterval[11];
extern UINT4 IgmpInterfaceStatus[11];
extern UINT4 IgmpInterfaceVersion[11];
extern UINT4 IgmpInterfaceQueryMaxResponseTime[11];
extern UINT4 IgmpInterfaceProxyIfIndex[11];
extern UINT4 IgmpInterfaceRobustness[11];
extern UINT4 IgmpInterfaceLastMembQueryIntvl[11];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IgmpCacheAddress[11];
extern UINT4 IgmpCacheIfIndex[11];
extern UINT4 IgmpCacheSelf[11];
extern UINT4 IgmpCacheStatus[11];


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 IgmpSrcListAddress[11];
extern UINT4 IgmpSrcListIfIndex[11];
extern UINT4 IgmpSrcListHostAddress[11];
extern UINT4 IgmpSrcListStatus[11];
