/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdtlmcli.h,v 1.1 2011/12/07 13:08:58 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 TeLinkAddressType[12];
extern UINT4 TeLinkLocalIpAddr[12];
extern UINT4 TeLinkRemoteIpAddr[12];
extern UINT4 TeLinkMetric[12];
extern UINT4 TeLinkProtectionType[12];
extern UINT4 TeLinkWorkingPriority[12];
extern UINT4 TeLinkResourceClass[12];
extern UINT4 TeLinkIncomingIfId[12];
extern UINT4 TeLinkOutgoingIfId[12];
extern UINT4 TeLinkRowStatus[12];
extern UINT4 TeLinkStorageType[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetTeLinkAddressType(i4IfIndex ,i4SetValTeLinkAddressType) \
 nmhSetCmn(TeLinkAddressType, 12, TeLinkAddressTypeSet, TlmLock, TlmUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValTeLinkAddressType)
#define nmhSetTeLinkLocalIpAddr(i4IfIndex ,pSetValTeLinkLocalIpAddr) \
 nmhSetCmn(TeLinkLocalIpAddr, 12, TeLinkLocalIpAddrSet, TlmLock, TlmUnLock, 0, 0, 1, "%i %s", i4IfIndex ,pSetValTeLinkLocalIpAddr)
#define nmhSetTeLinkRemoteIpAddr(i4IfIndex ,pSetValTeLinkRemoteIpAddr) \
 nmhSetCmn(TeLinkRemoteIpAddr, 12, TeLinkRemoteIpAddrSet, TlmLock, TlmUnLock, 0, 0, 1, "%i %s", i4IfIndex ,pSetValTeLinkRemoteIpAddr)
#define nmhSetTeLinkMetric(i4IfIndex ,u4SetValTeLinkMetric) \
 nmhSetCmn(TeLinkMetric, 12, TeLinkMetricSet, TlmLock, TlmUnLock, 0, 0, 1, "%i %u", i4IfIndex ,u4SetValTeLinkMetric)
#define nmhSetTeLinkProtectionType(i4IfIndex ,i4SetValTeLinkProtectionType) \
 nmhSetCmn(TeLinkProtectionType, 12, TeLinkProtectionTypeSet, TlmLock, TlmUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValTeLinkProtectionType)
#define nmhSetTeLinkWorkingPriority(i4IfIndex ,u4SetValTeLinkWorkingPriority) \
 nmhSetCmn(TeLinkWorkingPriority, 12, TeLinkWorkingPrioritySet, TlmLock, TlmUnLock, 0, 0, 1, "%i %u", i4IfIndex ,u4SetValTeLinkWorkingPriority)
#define nmhSetTeLinkResourceClass(i4IfIndex ,u4SetValTeLinkResourceClass) \
 nmhSetCmn(TeLinkResourceClass, 12, TeLinkResourceClassSet, TlmLock, TlmUnLock, 0, 0, 1, "%i %u", i4IfIndex ,u4SetValTeLinkResourceClass)
#define nmhSetTeLinkIncomingIfId(i4IfIndex ,i4SetValTeLinkIncomingIfId) \
 nmhSetCmn(TeLinkIncomingIfId, 12, TeLinkIncomingIfIdSet, TlmLock, TlmUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValTeLinkIncomingIfId)
#define nmhSetTeLinkOutgoingIfId(i4IfIndex ,i4SetValTeLinkOutgoingIfId) \
 nmhSetCmn(TeLinkOutgoingIfId, 12, TeLinkOutgoingIfIdSet, TlmLock, TlmUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValTeLinkOutgoingIfId)
#define nmhSetTeLinkRowStatus(i4IfIndex ,i4SetValTeLinkRowStatus) \
 nmhSetCmn(TeLinkRowStatus, 12, TeLinkRowStatusSet, TlmLock, TlmUnLock, 0, 1, 1, "%i %i", i4IfIndex ,i4SetValTeLinkRowStatus)
#define nmhSetTeLinkStorageType(i4IfIndex ,i4SetValTeLinkStorageType) \
 nmhSetCmn(TeLinkStorageType, 12, TeLinkStorageTypeSet, TlmLock, TlmUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValTeLinkStorageType)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 TeLinkDescriptorId[12];
extern UINT4 TeLinkDescrSwitchingCapability[12];
extern UINT4 TeLinkDescrEncodingType[12];
extern UINT4 TeLinkDescrMinLspBandwidth[12];
extern UINT4 TeLinkDescrMaxLspBandwidthPrio0[12];
extern UINT4 TeLinkDescrMaxLspBandwidthPrio1[12];
extern UINT4 TeLinkDescrMaxLspBandwidthPrio2[12];
extern UINT4 TeLinkDescrMaxLspBandwidthPrio3[12];
extern UINT4 TeLinkDescrMaxLspBandwidthPrio4[12];
extern UINT4 TeLinkDescrMaxLspBandwidthPrio5[12];
extern UINT4 TeLinkDescrMaxLspBandwidthPrio6[12];
extern UINT4 TeLinkDescrMaxLspBandwidthPrio7[12];
extern UINT4 TeLinkDescrInterfaceMtu[12];
extern UINT4 TeLinkDescrIndication[12];
extern UINT4 TeLinkDescrRowStatus[12];
extern UINT4 TeLinkDescrStorageType[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetTeLinkDescrSwitchingCapability(i4IfIndex , u4TeLinkDescriptorId ,i4SetValTeLinkDescrSwitchingCapability) \
 nmhSetCmn(TeLinkDescrSwitchingCapability, 12, TeLinkDescrSwitchingCapabilitySet, TlmLock, TlmUnLock, 0, 0, 2, "%i %u %i", i4IfIndex , u4TeLinkDescriptorId ,i4SetValTeLinkDescrSwitchingCapability)
#define nmhSetTeLinkDescrEncodingType(i4IfIndex , u4TeLinkDescriptorId ,i4SetValTeLinkDescrEncodingType) \
 nmhSetCmn(TeLinkDescrEncodingType, 12, TeLinkDescrEncodingTypeSet, TlmLock, TlmUnLock, 0, 0, 2, "%i %u %i", i4IfIndex , u4TeLinkDescriptorId ,i4SetValTeLinkDescrEncodingType)
#define nmhSetTeLinkDescrMinLspBandwidth(i4IfIndex , u4TeLinkDescriptorId ,pSetValTeLinkDescrMinLspBandwidth) \
 nmhSetCmn(TeLinkDescrMinLspBandwidth, 12, TeLinkDescrMinLspBandwidthSet, TlmLock, TlmUnLock, 0, 0, 2, "%i %u %s", i4IfIndex , u4TeLinkDescriptorId ,pSetValTeLinkDescrMinLspBandwidth)
#define nmhSetTeLinkDescrMaxLspBandwidthPrio0(i4IfIndex , u4TeLinkDescriptorId ,pSetValTeLinkDescrMaxLspBandwidthPrio0) \
 nmhSetCmn(TeLinkDescrMaxLspBandwidthPrio0, 12, TeLinkDescrMaxLspBandwidthPrio0Set, TlmLock, TlmUnLock, 0, 0, 2, "%i %u %s", i4IfIndex , u4TeLinkDescriptorId ,pSetValTeLinkDescrMaxLspBandwidthPrio0)
#define nmhSetTeLinkDescrMaxLspBandwidthPrio1(i4IfIndex , u4TeLinkDescriptorId ,pSetValTeLinkDescrMaxLspBandwidthPrio1) \
 nmhSetCmn(TeLinkDescrMaxLspBandwidthPrio1, 12, TeLinkDescrMaxLspBandwidthPrio1Set, TlmLock, TlmUnLock, 0, 0, 2, "%i %u %s", i4IfIndex , u4TeLinkDescriptorId ,pSetValTeLinkDescrMaxLspBandwidthPrio1)
#define nmhSetTeLinkDescrMaxLspBandwidthPrio2(i4IfIndex , u4TeLinkDescriptorId ,pSetValTeLinkDescrMaxLspBandwidthPrio2) \
 nmhSetCmn(TeLinkDescrMaxLspBandwidthPrio2, 12, TeLinkDescrMaxLspBandwidthPrio2Set, TlmLock, TlmUnLock, 0, 0, 2, "%i %u %s", i4IfIndex , u4TeLinkDescriptorId ,pSetValTeLinkDescrMaxLspBandwidthPrio2)
#define nmhSetTeLinkDescrMaxLspBandwidthPrio3(i4IfIndex , u4TeLinkDescriptorId ,pSetValTeLinkDescrMaxLspBandwidthPrio3) \
 nmhSetCmn(TeLinkDescrMaxLspBandwidthPrio3, 12, TeLinkDescrMaxLspBandwidthPrio3Set, TlmLock, TlmUnLock, 0, 0, 2, "%i %u %s", i4IfIndex , u4TeLinkDescriptorId ,pSetValTeLinkDescrMaxLspBandwidthPrio3)
#define nmhSetTeLinkDescrMaxLspBandwidthPrio4(i4IfIndex , u4TeLinkDescriptorId ,pSetValTeLinkDescrMaxLspBandwidthPrio4) \
 nmhSetCmn(TeLinkDescrMaxLspBandwidthPrio4, 12, TeLinkDescrMaxLspBandwidthPrio4Set, TlmLock, TlmUnLock, 0, 0, 2, "%i %u %s", i4IfIndex , u4TeLinkDescriptorId ,pSetValTeLinkDescrMaxLspBandwidthPrio4)
#define nmhSetTeLinkDescrMaxLspBandwidthPrio5(i4IfIndex , u4TeLinkDescriptorId ,pSetValTeLinkDescrMaxLspBandwidthPrio5) \
 nmhSetCmn(TeLinkDescrMaxLspBandwidthPrio5, 12, TeLinkDescrMaxLspBandwidthPrio5Set, TlmLock, TlmUnLock, 0, 0, 2, "%i %u %s", i4IfIndex , u4TeLinkDescriptorId ,pSetValTeLinkDescrMaxLspBandwidthPrio5)
#define nmhSetTeLinkDescrMaxLspBandwidthPrio6(i4IfIndex , u4TeLinkDescriptorId ,pSetValTeLinkDescrMaxLspBandwidthPrio6) \
 nmhSetCmn(TeLinkDescrMaxLspBandwidthPrio6, 12, TeLinkDescrMaxLspBandwidthPrio6Set, TlmLock, TlmUnLock, 0, 0, 2, "%i %u %s", i4IfIndex , u4TeLinkDescriptorId ,pSetValTeLinkDescrMaxLspBandwidthPrio6)
#define nmhSetTeLinkDescrMaxLspBandwidthPrio7(i4IfIndex , u4TeLinkDescriptorId ,pSetValTeLinkDescrMaxLspBandwidthPrio7) \
 nmhSetCmn(TeLinkDescrMaxLspBandwidthPrio7, 12, TeLinkDescrMaxLspBandwidthPrio7Set, TlmLock, TlmUnLock, 0, 0, 2, "%i %u %s", i4IfIndex , u4TeLinkDescriptorId ,pSetValTeLinkDescrMaxLspBandwidthPrio7)
#define nmhSetTeLinkDescrInterfaceMtu(i4IfIndex , u4TeLinkDescriptorId ,u4SetValTeLinkDescrInterfaceMtu) \
 nmhSetCmn(TeLinkDescrInterfaceMtu, 12, TeLinkDescrInterfaceMtuSet, TlmLock, TlmUnLock, 0, 0, 2, "%i %u %u", i4IfIndex , u4TeLinkDescriptorId ,u4SetValTeLinkDescrInterfaceMtu)
#define nmhSetTeLinkDescrIndication(i4IfIndex , u4TeLinkDescriptorId ,i4SetValTeLinkDescrIndication) \
 nmhSetCmn(TeLinkDescrIndication, 12, TeLinkDescrIndicationSet, TlmLock, TlmUnLock, 0, 0, 2, "%i %u %i", i4IfIndex , u4TeLinkDescriptorId ,i4SetValTeLinkDescrIndication)
#define nmhSetTeLinkDescrRowStatus(i4IfIndex , u4TeLinkDescriptorId ,i4SetValTeLinkDescrRowStatus) \
 nmhSetCmn(TeLinkDescrRowStatus, 12, TeLinkDescrRowStatusSet, TlmLock, TlmUnLock, 0, 1, 2, "%i %u %i", i4IfIndex , u4TeLinkDescriptorId ,i4SetValTeLinkDescrRowStatus)
#define nmhSetTeLinkDescrStorageType(i4IfIndex , u4TeLinkDescriptorId ,i4SetValTeLinkDescrStorageType) \
 nmhSetCmn(TeLinkDescrStorageType, 12, TeLinkDescrStorageTypeSet, TlmLock, TlmUnLock, 0, 0, 2, "%i %u %i", i4IfIndex , u4TeLinkDescriptorId ,i4SetValTeLinkDescrStorageType)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 TeLinkSrlg[12];
extern UINT4 TeLinkSrlgRowStatus[12];
extern UINT4 TeLinkSrlgStorageType[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetTeLinkSrlgRowStatus(i4IfIndex , u4TeLinkSrlg ,i4SetValTeLinkSrlgRowStatus) \
 nmhSetCmn(TeLinkSrlgRowStatus, 12, TeLinkSrlgRowStatusSet, TlmLock, TlmUnLock, 0, 1, 2, "%i %u %i", i4IfIndex , u4TeLinkSrlg ,i4SetValTeLinkSrlgRowStatus)
#define nmhSetTeLinkSrlgStorageType(i4IfIndex , u4TeLinkSrlg ,i4SetValTeLinkSrlgStorageType) \
 nmhSetCmn(TeLinkSrlgStorageType, 12, TeLinkSrlgStorageTypeSet, TlmLock, TlmUnLock, 0, 0, 2, "%i %u %i", i4IfIndex , u4TeLinkSrlg ,i4SetValTeLinkSrlgStorageType)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 TeLinkBandwidthPriority[12];
extern UINT4 TeLinkBandwidthRowStatus[12];
extern UINT4 TeLinkBandwidthStorageType[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetTeLinkBandwidthRowStatus(i4IfIndex , u4TeLinkBandwidthPriority ,i4SetValTeLinkBandwidthRowStatus) \
 nmhSetCmn(TeLinkBandwidthRowStatus, 12, TeLinkBandwidthRowStatusSet, TlmLock, TlmUnLock, 0, 1, 2, "%i %u %i", i4IfIndex , u4TeLinkBandwidthPriority ,i4SetValTeLinkBandwidthRowStatus)
#define nmhSetTeLinkBandwidthStorageType(i4IfIndex , u4TeLinkBandwidthPriority ,i4SetValTeLinkBandwidthStorageType) \
 nmhSetCmn(TeLinkBandwidthStorageType, 12, TeLinkBandwidthStorageTypeSet, TlmLock, TlmUnLock, 0, 0, 2, "%i %u %i", i4IfIndex , u4TeLinkBandwidthPriority ,i4SetValTeLinkBandwidthStorageType)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 ComponentLinkMaxResBandwidth[12];
extern UINT4 ComponentLinkPreferredProtection[12];
extern UINT4 ComponentLinkRowStatus[12];
extern UINT4 ComponentLinkStorageType[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetComponentLinkMaxResBandwidth(i4IfIndex ,pSetValComponentLinkMaxResBandwidth) \
 nmhSetCmn(ComponentLinkMaxResBandwidth, 12, ComponentLinkMaxResBandwidthSet, TlmLock, TlmUnLock, 0, 0, 1, "%i %s", i4IfIndex ,pSetValComponentLinkMaxResBandwidth)
#define nmhSetComponentLinkPreferredProtection(i4IfIndex ,i4SetValComponentLinkPreferredProtection) \
 nmhSetCmn(ComponentLinkPreferredProtection, 12, ComponentLinkPreferredProtectionSet, TlmLock, TlmUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValComponentLinkPreferredProtection)
#define nmhSetComponentLinkRowStatus(i4IfIndex ,i4SetValComponentLinkRowStatus) \
 nmhSetCmn(ComponentLinkRowStatus, 12, ComponentLinkRowStatusSet, TlmLock, TlmUnLock, 0, 1, 1, "%i %i", i4IfIndex ,i4SetValComponentLinkRowStatus)
#define nmhSetComponentLinkStorageType(i4IfIndex ,i4SetValComponentLinkStorageType) \
 nmhSetCmn(ComponentLinkStorageType, 12, ComponentLinkStorageTypeSet, TlmLock, TlmUnLock, 0, 0, 1, "%i %i", i4IfIndex ,i4SetValComponentLinkStorageType)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 ComponentLinkDescrId[12];
extern UINT4 ComponentLinkDescrSwitchingCapability[12];
extern UINT4 ComponentLinkDescrEncodingType[12];
extern UINT4 ComponentLinkDescrMinLspBandwidth[12];
extern UINT4 ComponentLinkDescrMaxLspBandwidthPrio0[12];
extern UINT4 ComponentLinkDescrMaxLspBandwidthPrio1[12];
extern UINT4 ComponentLinkDescrMaxLspBandwidthPrio2[12];
extern UINT4 ComponentLinkDescrMaxLspBandwidthPrio3[12];
extern UINT4 ComponentLinkDescrMaxLspBandwidthPrio4[12];
extern UINT4 ComponentLinkDescrMaxLspBandwidthPrio5[12];
extern UINT4 ComponentLinkDescrMaxLspBandwidthPrio6[12];
extern UINT4 ComponentLinkDescrMaxLspBandwidthPrio7[12];
extern UINT4 ComponentLinkDescrInterfaceMtu[12];
extern UINT4 ComponentLinkDescrIndication[12];
extern UINT4 ComponentLinkDescrRowStatus[12];
extern UINT4 ComponentLinkDescrStorageType[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetComponentLinkDescrSwitchingCapability(i4IfIndex , u4ComponentLinkDescrId ,i4SetValComponentLinkDescrSwitchingCapability) \
 nmhSetCmn(ComponentLinkDescrSwitchingCapability, 12, ComponentLinkDescrSwitchingCapabilitySet, TlmLock, TlmUnLock, 0, 0, 2, "%i %u %i", i4IfIndex , u4ComponentLinkDescrId ,i4SetValComponentLinkDescrSwitchingCapability)
#define nmhSetComponentLinkDescrEncodingType(i4IfIndex , u4ComponentLinkDescrId ,i4SetValComponentLinkDescrEncodingType) \
 nmhSetCmn(ComponentLinkDescrEncodingType, 12, ComponentLinkDescrEncodingTypeSet, TlmLock, TlmUnLock, 0, 0, 2, "%i %u %i", i4IfIndex , u4ComponentLinkDescrId ,i4SetValComponentLinkDescrEncodingType)
#define nmhSetComponentLinkDescrMinLspBandwidth(i4IfIndex , u4ComponentLinkDescrId ,pSetValComponentLinkDescrMinLspBandwidth) \
 nmhSetCmn(ComponentLinkDescrMinLspBandwidth, 12, ComponentLinkDescrMinLspBandwidthSet, TlmLock, TlmUnLock, 0, 0, 2, "%i %u %s", i4IfIndex , u4ComponentLinkDescrId ,pSetValComponentLinkDescrMinLspBandwidth)
#define nmhSetComponentLinkDescrMaxLspBandwidthPrio0(i4IfIndex , u4ComponentLinkDescrId ,pSetValComponentLinkDescrMaxLspBandwidthPrio0) \
 nmhSetCmn(ComponentLinkDescrMaxLspBandwidthPrio0, 12, ComponentLinkDescrMaxLspBandwidthPrio0Set, TlmLock, TlmUnLock, 0, 0, 2, "%i %u %s", i4IfIndex , u4ComponentLinkDescrId ,pSetValComponentLinkDescrMaxLspBandwidthPrio0)
#define nmhSetComponentLinkDescrMaxLspBandwidthPrio1(i4IfIndex , u4ComponentLinkDescrId ,pSetValComponentLinkDescrMaxLspBandwidthPrio1) \
 nmhSetCmn(ComponentLinkDescrMaxLspBandwidthPrio1, 12, ComponentLinkDescrMaxLspBandwidthPrio1Set, TlmLock, TlmUnLock, 0, 0, 2, "%i %u %s", i4IfIndex , u4ComponentLinkDescrId ,pSetValComponentLinkDescrMaxLspBandwidthPrio1)
#define nmhSetComponentLinkDescrMaxLspBandwidthPrio2(i4IfIndex , u4ComponentLinkDescrId ,pSetValComponentLinkDescrMaxLspBandwidthPrio2) \
 nmhSetCmn(ComponentLinkDescrMaxLspBandwidthPrio2, 12, ComponentLinkDescrMaxLspBandwidthPrio2Set, TlmLock, TlmUnLock, 0, 0, 2, "%i %u %s", i4IfIndex , u4ComponentLinkDescrId ,pSetValComponentLinkDescrMaxLspBandwidthPrio2)
#define nmhSetComponentLinkDescrMaxLspBandwidthPrio3(i4IfIndex , u4ComponentLinkDescrId ,pSetValComponentLinkDescrMaxLspBandwidthPrio3) \
 nmhSetCmn(ComponentLinkDescrMaxLspBandwidthPrio3, 12, ComponentLinkDescrMaxLspBandwidthPrio3Set, TlmLock, TlmUnLock, 0, 0, 2, "%i %u %s", i4IfIndex , u4ComponentLinkDescrId ,pSetValComponentLinkDescrMaxLspBandwidthPrio3)
#define nmhSetComponentLinkDescrMaxLspBandwidthPrio4(i4IfIndex , u4ComponentLinkDescrId ,pSetValComponentLinkDescrMaxLspBandwidthPrio4) \
 nmhSetCmn(ComponentLinkDescrMaxLspBandwidthPrio4, 12, ComponentLinkDescrMaxLspBandwidthPrio4Set, TlmLock, TlmUnLock, 0, 0, 2, "%i %u %s", i4IfIndex , u4ComponentLinkDescrId ,pSetValComponentLinkDescrMaxLspBandwidthPrio4)
#define nmhSetComponentLinkDescrMaxLspBandwidthPrio5(i4IfIndex , u4ComponentLinkDescrId ,pSetValComponentLinkDescrMaxLspBandwidthPrio5) \
 nmhSetCmn(ComponentLinkDescrMaxLspBandwidthPrio5, 12, ComponentLinkDescrMaxLspBandwidthPrio5Set, TlmLock, TlmUnLock, 0, 0, 2, "%i %u %s", i4IfIndex , u4ComponentLinkDescrId ,pSetValComponentLinkDescrMaxLspBandwidthPrio5)
#define nmhSetComponentLinkDescrMaxLspBandwidthPrio6(i4IfIndex , u4ComponentLinkDescrId ,pSetValComponentLinkDescrMaxLspBandwidthPrio6) \
 nmhSetCmn(ComponentLinkDescrMaxLspBandwidthPrio6, 12, ComponentLinkDescrMaxLspBandwidthPrio6Set, TlmLock, TlmUnLock, 0, 0, 2, "%i %u %s", i4IfIndex , u4ComponentLinkDescrId ,pSetValComponentLinkDescrMaxLspBandwidthPrio6)
#define nmhSetComponentLinkDescrMaxLspBandwidthPrio7(i4IfIndex , u4ComponentLinkDescrId ,pSetValComponentLinkDescrMaxLspBandwidthPrio7) \
 nmhSetCmn(ComponentLinkDescrMaxLspBandwidthPrio7, 12, ComponentLinkDescrMaxLspBandwidthPrio7Set, TlmLock, TlmUnLock, 0, 0, 2, "%i %u %s", i4IfIndex , u4ComponentLinkDescrId ,pSetValComponentLinkDescrMaxLspBandwidthPrio7)
#define nmhSetComponentLinkDescrInterfaceMtu(i4IfIndex , u4ComponentLinkDescrId ,u4SetValComponentLinkDescrInterfaceMtu) \
 nmhSetCmn(ComponentLinkDescrInterfaceMtu, 12, ComponentLinkDescrInterfaceMtuSet, TlmLock, TlmUnLock, 0, 0, 2, "%i %u %u", i4IfIndex , u4ComponentLinkDescrId ,u4SetValComponentLinkDescrInterfaceMtu)
#define nmhSetComponentLinkDescrIndication(i4IfIndex , u4ComponentLinkDescrId ,i4SetValComponentLinkDescrIndication) \
 nmhSetCmn(ComponentLinkDescrIndication, 12, ComponentLinkDescrIndicationSet, TlmLock, TlmUnLock, 0, 0, 2, "%i %u %i", i4IfIndex , u4ComponentLinkDescrId ,i4SetValComponentLinkDescrIndication)
#define nmhSetComponentLinkDescrRowStatus(i4IfIndex , u4ComponentLinkDescrId ,i4SetValComponentLinkDescrRowStatus) \
 nmhSetCmn(ComponentLinkDescrRowStatus, 12, ComponentLinkDescrRowStatusSet, TlmLock, TlmUnLock, 0, 1, 2, "%i %u %i", i4IfIndex , u4ComponentLinkDescrId ,i4SetValComponentLinkDescrRowStatus)
#define nmhSetComponentLinkDescrStorageType(i4IfIndex , u4ComponentLinkDescrId ,i4SetValComponentLinkDescrStorageType) \
 nmhSetCmn(ComponentLinkDescrStorageType, 12, ComponentLinkDescrStorageTypeSet, TlmLock, TlmUnLock, 0, 0, 2, "%i %u %i", i4IfIndex , u4ComponentLinkDescrId ,i4SetValComponentLinkDescrStorageType)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 ComponentLinkBandwidthPriority[12];
extern UINT4 ComponentLinkBandwidthRowStatus[12];
extern UINT4 ComponentLinkBandwidthStorageType[12];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetComponentLinkBandwidthRowStatus(i4IfIndex , u4ComponentLinkBandwidthPriority ,i4SetValComponentLinkBandwidthRowStatus) \
 nmhSetCmn(ComponentLinkBandwidthRowStatus, 12, ComponentLinkBandwidthRowStatusSet, TlmLock, TlmUnLock, 0, 1, 2, "%i %u %i", i4IfIndex , u4ComponentLinkBandwidthPriority ,i4SetValComponentLinkBandwidthRowStatus)
#define nmhSetComponentLinkBandwidthStorageType(i4IfIndex , u4ComponentLinkBandwidthPriority ,i4SetValComponentLinkBandwidthStorageType) \
 nmhSetCmn(ComponentLinkBandwidthStorageType, 12, ComponentLinkBandwidthStorageTypeSet, TlmLock, TlmUnLock, 0, 0, 2, "%i %u %i", i4IfIndex , u4ComponentLinkBandwidthPriority ,i4SetValComponentLinkBandwidthStorageType)

#endif
