/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: vlnevcli.h,v 1.7 2016/07/16 11:15:02 siva Exp $
 *
 * Description: This file contains all the CLI function prototypes
 *              exported by the PB Module.
 *
***********************************************************************/
#ifndef __VLNEVCLI_H__
#define __VLNEVCLI_H__

#include "cli.h"

enum {
    CLI_VLAN_EVB_SHUT = 1,
    CLI_VLAN_EVB_NO_SHUT,
    CLI_VLAN_EVB_MOD_STATUS,
    CLI_VLAN_EVB_DEBUG,
    CLI_VLAN_EVB_NO_DEBUG,
    CLI_VLAN_EVB_CLR_STATS,
    CLI_VLAN_EVB_TRAP_STATUS,
    CLI_VLAN_EVB_TRAP_ENABLED,
    CLI_VLAN_EVB_TRAP_DISABLED,
    CLI_VLAN_EVB_CDCP_ENABLE,
    CLI_VLAN_EVB_CDCP_MODE_DYNAMIC,
    CLI_VLAN_EVB_CDCP_MODE_HYBRID,
    CLI_VLAN_EVB_CDCP_DISABLE,
    CLI_VLAN_CDCP_MAX_SCHANNEL,
    CLI_VLAN_EVB_SERV_VLAN_POOL,
    CLI_VLAN_EVB_NO_SERV_VLAN_POOL,
    CLI_VLAN_EVB_STORAGE_TYPE,
    CLI_VLAN_EVB_NO_STORAGE_TYPE,
    CLI_VLAN_EVB_SCH_MODE,
    CLI_VLAN_EVB_NO_SCH_MODE,
    CLI_VLAN_EVB_SCH_SERV_VLAN,
    CLI_VLAN_EVB_NO_SCH_SERV_VLAN,
    CLI_VLAN_EVB_CDCP_TLV_SELECT,
    CLI_VLAN_EVB_CDCP_NO_TLV_SELECT,
    CLI_VLAN_EVB_SCH_L2_FILTER_STATUS, 
    CLI_VLAN_EVB_SHOW_DETAIL,
    CLI_VLAN_EVB_UAP_SHOW_DESC,
    CLI_VLAN_EVB_UAP_SHOW_STATS,
    CLI_VLAN_EVB_SHOW_SCH_INT,
    CLI_VLAN_EVB_SHOW_LOCAL_CDCP_TLV,
    CLI_VLAN_EVB_SHOW_REMOTE_CDCP_TLV,
    CLI_VLAN_EVB_SVID_LOW,
    CLI_VLAN_EVB_SVID_HIGH
};

enum {
    CLI_VLAN_EVB_UNKNOWN_ERR = 1,
    CLI_VLAN_EVB_MODULE_STATUS_ERR,
    CLI_VLAN_EVB_SYSTEM_STATUS_ERR,
    CLI_VLAN_EVB_GET_CONTEXT_FROM_IF_ERR,
    CLI_VLAN_EVB_INVALID_CONTEXT,
    CLI_VLAN_EVB_CONTEXT_INFO_NOT_PRESENT_ERR,
    CLI_VLAN_EVB_UAP_ENTRY_NOT_PRESENT_ERR,
    CLI_VLAN_EVB_SCHANNEL_ENTRY_NOT_PRESENT_ERR,
    CLI_VLAN_EVB_INCORRECT_ROLE,
    CLI_VLAN_EVB_INCORRECT_CHANNEL_CAP,
    CLI_VLAN_EVB_ADD_ENTRY_ERR,
    CLI_VLAN_EVB_DEL_ENTRY_ERR,
    CLI_VLAN_EVB_CDCP_SCH_STATIC_NOT_ALLOWED_ERR,
    CLI_VLAN_EVB_MAX_SCH_VALUE_ERR,
    CLI_VLAN_EVB_CDCP_ENABLE_SCH_NOT_ALLOWED_ERR,
    CLI_VLAN_EVB_SCH_ENTRY_ALREADY_PRESENT_ERR,
    CLI_VLAN_EVB_DEF_SVID_CREATION_NOT_ALLOWED,
    CLI_VLAN_EVB_DEF_SVID_DELETION_NOT_ALLOWED,
    CLI_VLAN_EVB_DEF_SCID_CREATION_NOT_ALLOWED,
    CLI_VLAN_EVB_SVID_SCID_MANUAL_ERR,
    CLI_VLAN_EVB_SCID_AUTO_ERR,
    CLI_VLAN_EVB_SCHANNEL_MODE_ERR,
    CLI_VLAN_EVB_MAX_SCHANNEL_ERR,
    CLI_VLAN_EVB_SCH_FILTER_ENABLED,
    CLI_VLAN_EVB_DEL_ENTRY_NOT_ALLOWED,
    CLI_VLAN_EVB_MAX_ERR
};

#ifdef __VLNEVCLI_C__
CONST CHR1  *EvbCliErrString [] = {
    NULL,
    "Unknown Error\r\n",
    "% EVB Module is shutdown\r\n",
    "% EVB System is shutdown\r\n",
    "% Failed to get EVB Context Id for the given Interface\r\n",
    "% Given Context is invalid\r\n",
    "% EVB Context information is not available for the given Context\r\n",
    "% EVB UAP entry is not present for the given Interface\r\n",
    "% EVB S-Channel entry is not present for the given Interface\r\n",
    "% Role can not be set as station\r\n",
    "% Invalid Channel Cap value. Supported range (1..167)\r\n",
    "% Failed to Add S-channel Entry\r\n",
    "% Failed to Delete S-channel Entry\r\n",
    "% Failed to enable CDCP as S-channel Mode is static\r\n",
    "% Max S-Channel Value can't be set since S-Channel entries are already present on this UAP\r\n",
    "% S-Channel Entries Cannnot be Created Manually in Dynamic Mode\r\n",
    "% Configured S-Channel entry is already present\r\n",
    "% Configuring default SVID is not allowed\r\n",
    "% Deleting default SVID is not allowed\r\n",
    "% Configuring default S-Channel Entry is not allowed\r\n",
    "% S-Channel Id or SVID should not be zero in manual mode configuration\r\n",
    "% S-Channel Id should be zero in auto mode configuration\r\n",
    "% Unable to change S-Channel mode since non-default S-Channel entries are present\r\n",
    "% Unable to configure S-Channel entry since maximum number of S-channels per UAP is reached\r\n",
    "% S-Channel Filter Already enabled for this S-Channel Interface\r\n",
    "% Dynamically Created S-Channel Cannot be Deleted Manually\r\n",
};
#endif


INT4
cli_process_vlan_evb_cmd(tCliHandle CliHandle, UINT4 u4Command, ...);
INT4
cli_process_vlan_evb_show_cmd(tCliHandle CliHandle, UINT4 u4Command, ...);

INT4 
cli_process_evb_test_cmd(tCliHandle CliHandle, UINT4 u4Command, ...);
INT4
VlanEvbCliSetSysStatus(tCliHandle CliHandle, INT4 i4Status);
INT4
VlanEvbCliModStatus(tCliHandle CliHandle, INT4 i4Status);
INT4
VlanEvbCliSetDebug(tCliHandle CliHandle, UINT4 u4TrcLevel);
INT4
VlanEvbCliSetTrapStatus(tCliHandle CliHandle, INT4 i4TrapFlg);
INT4
VlanEvbCliMaxSchannel(tCliHandle CliHandle, INT4 i4CdcpChannelCap,UINT4 u4IfIndex);
INT4
VlanEvbServVlanPoolConfig(tCliHandle CliHandle, UINT4 u4IfIndex,
                    UINT4 u4PoolLow, UINT4 u4PoolHigh);

INT4
VlanEvbStorageType(tCliHandle CliHandle, UINT4 u4IfIndex, INT4 i4StorageType);
INT4
VlanEvbConfigSchMode(tCliHandle CliHandle, INT4 i4SchMode);
INT4
VlanEvbAddScidEntry (tCliHandle CliHandle, UINT4 u4IfIndex,
                        UINT4 u4SChannelId, UINT4 u4SChId);
INT4
VlanEvbDelScidEntry (tCliHandle CliHandle, UINT4 u4IfIndex,
                        UINT4 u4SChannelId, UINT4 u4SChId);
INT4
VlanEvbTlvSelect(tCliHandle CliHandle );
INT4
VlanEvbNoTlvSlct(tCliHandle CliHandle);

INT4
VlanEvbConfigSChL2FilterStatus(tCliHandle CliHandle ,INT4 i4Status );

INT4
VlanEvbShowEvbDetail(tCliHandle CliHandle,UINT4 u4ContextId);
INT4
VlanEvbShow(tCliHandle CliHandle, UINT4 u4IfIndex,UINT4 u4Command);
INT4
VlanEvbShowCdcpLocTlvInfo(tCliHandle CliHandle, UINT4 u4IfIndex);
INT4
VlanEvbShowCdcpRemTlvInfo(tCliHandle CliHandle, UINT4 u4IfIndex);
INT4
VlanEvbShowEvbStats(tCliHandle CliHandle, UINT4 u4IfIndex);
INT4
VlanEvbShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module, ...);
INT4 
VlanEvbShowRunningConfigInterface(tCliHandle CliHandle,UINT4 u4IfIndex);
INT4
VlanEvbShowRunningConfigTables (tCliHandle CliHandle,UINT4 u4IfIndex);
INT4
VlanEvbShowRunningConfigScalars(tCliHandle CliHandle);

INT4
VlnEvbCliSelectContextOnMode (tCliHandle CliHandle, UINT4 u4Cmd,
                            UINT4 *pu4Context, UINT2 *pu2LocalPort);

INT4 VlanEvbCdcpDisable(tCliHandle CliHandle);
INT4
VlanEvbCdcpEnable(tCliHandle CliHandle, INT4 i4CdcpMode);

INT4
VlanEvbCliClearEvbStats (tCliHandle CliHandle);
INT4
VlanEvbShowSChannel (tCliHandle CliHandle ,UINT4 u4IfIndex);
INT4
VlanEvbShowEvbDesc (tCliHandle CliHandle ,UINT4 u4IfIndex);
#endif


