/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: sd3lv2cli.h,v 1.1 2013/03/28 12:53:02 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 LldpV2Xdot3PortConfigTLVsTxEnable[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetLldpV2Xdot3PortConfigTLVsTxEnable(i4LldpV2PortConfigIfIndex , u4LldpV2PortConfigDestAddressIndex ,pSetValLldpV2Xdot3PortConfigTLVsTxEnable)	\
	nmhSetCmn(LldpV2Xdot3PortConfigTLVsTxEnable, 13, LldpV2Xdot3PortConfigTLVsTxEnableSet, NULL, NULL, 0, 0, 2, "%i %u %s", i4LldpV2PortConfigIfIndex , u4LldpV2PortConfigDestAddressIndex ,pSetValLldpV2Xdot3PortConfigTLVsTxEnable)

#endif
