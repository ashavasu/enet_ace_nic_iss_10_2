/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmioscli.h,v 1.10 2015/01/21 10:33:26 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIOspfGlobalTraceLevel[10];
extern UINT4 FsMIOspfVrfSpfInterval[10];
extern UINT4 FsMIOspfRFC1583Compatibility[12];
extern UINT4 FsMIOspfTraceLevel[12];
extern UINT4 FsMIOspfMinLsaInterval[12];
extern UINT4 FsMIOspfABRType[12];
extern UINT4 FsMIOspfNssaAsbrDefRtTrans[12];
extern UINT4 FsMIOspfDefaultPassiveInterface[12];
extern UINT4 FsMIOspfSpfHoldtime[12];
extern UINT4 FsMIOspfSpfDelay[12];
extern UINT4 FsMIOspfRestartSupport[12];
extern UINT4 FsMIOspfRestartInterval [12];
extern UINT4 FsMIOspfRestartStrictLsaChecking [12];
extern UINT4 FsMIOspfHelperSupport [12];
extern UINT4 FsMIOspfExtTraceLevel [12];
extern UINT4 FsMIOspfGlobalExtTraceLevel [10];
extern UINT4 FsMIOspfHelperGraceTimeLimit [12];
extern UINT4 FsMIOspfRestartAckState [12];
extern UINT4 FsMIOspfGraceLsaRetransmitCount [12];
extern UINT4 FsMIOspfRestartReason [12];
extern UINT4 FsMIOspfGrShutdown [12];
extern UINT4 FsMIOspfRTStaggeringInterval[12];
extern UINT4 FsMIOspfBfdStatus[12];
extern UINT4 FsMIOspfBfdAllIfState[12];
extern UINT4 FsMIOspfRouterIdPermanence[12];
extern UINT4 FsMIOspfRTStaggeringStatus[10];
extern UINT4 FsMIOspfAreaId[11];
extern UINT4 FsMIOspfAreaNSSATranslatorRole[11];
extern UINT4 FsMIOspfAreaNSSATranslatorStabilityInterval[11];
extern UINT4 FsMIOspfAreaDfInfOriginate[11];
extern UINT4 FsMIOspfHostIpAddress[11];
extern UINT4 FsMIOspfHostTOS[11];
extern UINT4 FsMIOspfHostRouteIfIndex[11];
extern UINT4 FsMIOspfIfIpAddress[11];
extern UINT4 FsMIOspfAddressLessIf[11];
extern UINT4 FsMIOspfIfPassive[11];
extern UINT4 FsMIOspfIfBfdState[11];
extern UINT4 FsMIOspfIfMD5AuthIpAddress[11];
extern UINT4 FsMIOspfIfMD5AuthAddressLessIf[11];
extern UINT4 FsMIOspfIfMD5AuthKeyId[11];
extern UINT4 FsMIOspfIfMD5AuthKey[11];
extern UINT4 FsMIOspfIfMD5AuthKeyStartAccept[11];
extern UINT4 FsMIOspfIfMD5AuthKeyStartGenerate[11];
extern UINT4 FsMIOspfIfMD5AuthKeyStopGenerate[11];
extern UINT4 FsMIOspfIfMD5AuthKeyStopAccept[11];
extern UINT4 FsMIOspfIfMD5AuthKeyStatus[11];
extern UINT4 FsMIOspfVirtIfMD5AuthAreaId[11];
extern UINT4 FsMIOspfVirtIfMD5AuthNeighbor[11];
extern UINT4 FsMIOspfVirtIfMD5AuthKeyId[11];
extern UINT4 FsMIOspfVirtIfMD5AuthKey[11];
extern UINT4 FsMIOspfVirtIfMD5AuthKeyStartAccept[11];
extern UINT4 FsMIOspfVirtIfMD5AuthKeyStartGenerate[11];
extern UINT4 FsMIOspfVirtIfMD5AuthKeyStopGenerate[11];
extern UINT4 FsMIOspfVirtIfMD5AuthKeyStopAccept[11];
extern UINT4 FsMIOspfVirtIfMD5AuthKeyStatus[11];
extern UINT4 FsMIOspfPrimIpAddr[11];
extern UINT4 FsMIOspfPrimAddresslessIf[11];
extern UINT4 FsMIOspfSecIpAddr[11];
extern UINT4 FsMIOspfSecIpAddrMask[11];
extern UINT4 FsMIOspfSecIfStatus[11];
extern UINT4 FsMIOspfAreaAggregateAreaID[11];
extern UINT4 FsMIOspfAreaAggregateLsdbType[11];
extern UINT4 FsMIOspfAreaAggregateNet[11];
extern UINT4 FsMIOspfAreaAggregateMask[11];
extern UINT4 FsMIOspfAreaAggregateExternalTag[11];
extern UINT4 FsMIOspfAsExternalAggregationNet[11];
extern UINT4 FsMIOspfAsExternalAggregationMask[11];
extern UINT4 FsMIOspfAsExternalAggregationAreaId[11];
extern UINT4 FsMIOspfAsExternalAggregationEffect[11];
extern UINT4 FsMIOspfAsExternalAggregationTranslation[11];
extern UINT4 FsMIOspfAsExternalAggregationStatus[11];
extern UINT4 FsMIOspfOpaqueOption[13];
extern UINT4 FsMIOspfAreaIDValid[13];
extern UINT4 FsMIOspfRRDStatus[13];
extern UINT4 FsMIOspfRRDSrcProtoMaskEnable[13];
extern UINT4 FsMIOspfRRDSrcProtoMaskDisable[13];
extern UINT4 FsMIOspfRRDRouteMapEnable[13];
extern UINT4 FsMIOspfRRDRouteDest[12];
extern UINT4 FsMIOspfRRDRouteMask[12];
extern UINT4 FsMIOspfRRDRouteMetric[12];
extern UINT4 FsMIOspfRRDRouteMetricType[12];
extern UINT4 FsMIOspfRRDRouteTagType[12];
extern UINT4 FsMIOspfRRDRouteTag[12];
extern UINT4 FsMIOspfRRDRouteStatus[12];
extern UINT4 FsMIOspfDistInOutRouteMapName[12];
extern UINT4 FsMIOspfDistInOutRouteMapType[12];
extern UINT4 FsMIOspfDistInOutRouteMapValue[12];
extern UINT4 FsMIOspfDistInOutRouteMapRowStatus[12];
extern UINT4 FsMIOspfPreferenceValue[12];
extern UINT4 FsMIOspfRRDProtocolId[12];
extern UINT4 FsMIOspfRRDMetricValue[12];
extern UINT4 FsMIOspfRRDMetricType[12];
