/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: ofccli.h,v 1.7 2014/03/01 11:36:08 siva Exp $
*
* Description: This file contains the Ofc CLI related prototypes,enum and macros
*********************************************************************/

#include "ofctrc.h"

INT4 cli_process_Ofc_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);
INT4 cli_process_Ofc_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);
INT4 OfcCliGetVlanIfIndex (UINT4 u4PortId, UINT4 *pu4IfIndex);

#define OFC_LOCK  OfcMainTaskLock ()
#define OFC_UNLOCK  OfcMainTaskUnLock ()

enum
{
 CLI_OFC_FSOFCCFGTABLE,
 CLI_OFC_FSOFCCONTROLLERCONNTABLE,
 CLI_OFC_FSOFCIFTABLE,
 CLI_OFC_PORT_MAP,
 CLI_OFC_NO_PORT_MAP,
 CLI_OFC_VLAN_MAP,
 CLI_OFC_NO_VLAN_MAP,
 CLI_OFC_SHOW_INTERFACE_INFO,
 CLI_OFC_SHOW_CLIENTCFG_INFO,
 CLI_OFC_SHOW_CNTRLRCFG_INFO,
 CLI_OFC_SHOW_FLOWS,
 CLI_OFC_SHOW_GROUPS,
 CLI_OFC_SHOW_METERS,
};

#define CLI_ERR_START_ID_OFC 1


enum
{
        CLI_OFC_INVALID_CONTEXT_ID = CLI_ERR_START_ID_OFC,
        CLI_OFC_UNSUPPORTED_SET_REQ,
        CLI_OFC_MAX_CNTLR_CONN_EXCEEDS,
        CLI_OFC_INCONSISTENT_VALUE,
        CLI_OFC_NOT_SUPPORTED_VALUE,
        CLI_OFC_PRIMARY_CONN_ERROR,
        CLI_OFC_AUXILARY_CONN_ERROR,
        CLI_OFC_INVALID_ENTRY,
        CLI_OFC_SELF_IP,
        CLI_OFC_MAX_CONTEXTS_EXCEEDS,
        CLI_OFC_INVALID_MAPPING,
        CLI_OFC_INTF_MAPPING_EXISTS,
        CLI_OFC_CNTLR_CONN_STATE,
        CLI_OFC_HYBRID_MODE_ERROR,
        CLI_OFC_PORT_MAPPING_EXISTS,
        CLI_OFC_HYBRID_STATUS_ERROR,
        CLI_OFC_HYBRID_PORT_ERROR,
        CLI_OFC_MAX_ERR = 18,
};

#define OFC_CLI_MAX_ARGS   15

/* Accessing the ErrString with ErrCode value.
 * Since the ErrCode's starting Id is changed, this forumla is
 * used to access the Err String */

#define CLI_ERR_OFFSET_OFC(u4ErrCode) (u4ErrCode - CLI_ERR_START_ID_OFC + 1)

#if defined  (__OFCCLIG_C__)
CONST CHR1  *OfcCliErrString [] = {
        NULL,
        "\r\n%% Entry with the given context id is Invalid !!!\r\n",
        "\r\n%% CLI Set Request for un-supported/read-only object !!!\r\n",
        "\r\n%% Max number of controller connections exceeded !!!\r\n",
        "\r\n%% Supplied with In consistent Value !!!\r\n",
        "\r\n%% Supplied value is not supported currently !!!\r\n",
        "\r\n%% Auxiliary Connections can not be created, as Primary Conn is down or not present !!!\r\n",
        "\r\n%% Primary Connection can not be deleted, as auxiliary Connections are present !!!\r\n",
        "\r\n%% Entry Doesn't Exists !!!\r\n",
        "\r\n%% Self IP can't be Used !!!\r\n",
        "\r\n%% Max number of openflow client contexts exceeded !!!\r\n",
        "\r\n%% Interface already mapped to a different Context !!!\r\n",
        "\r\n%% Some Ports/Cntlrs are mapped to Context, un-map Ports/Cntlrs to delete the Context !!!\r\n",
        "\r\n%% Controller Entry is not in Not Connected State. Close the Conn and Create a new Conn !!!\r\n",
        "\r\n%% Fail standalone mode cannot be enabled, When the hybrid mode is disabled !!!\r\n",
        "\r\n%% Ports are mapped, un-map Ports to un-map the Context !!!\r\n",
        "\r\n%% Hybrid configuration cannot be changed, when hybrid is already enabled !!!\r\n",
        "\r\n%% Tunnel(L3) or Port-Channel(L2) should be created to enable Hybrid !!!\r\n",
        NULL,
};
#else
PUBLIC CHR1* OfcCliErrString;
#endif

#define CLI_OFC_NO_DEBUG                      0
#define CLI_OFC_DEBUG_ALL                     OFC_ALL_TRC
#define CLI_OFC_DEBUG_FN_ENTRY                OFC_FN_ENTRY
#define CLI_OFC_DEBUG_FN_EXIT                 OFC_FN_EXIT
#define CLI_OFC_DEBUG_FUNC                    CLI_OFC_DEBUG_FN_ENTRY | CLI_OFC_DEBUG_FN_EXIT
#define CLI_OFC_DEBUG_MAIN                    OFC_MAIN_TRC
#define CLI_OFC_DEBUG_QUE                     OFC_QUE_TRC
#define CLI_OFC_DEBUG_TASK                    OFC_TASK_TRC
#define CLI_OFC_DEBUG_TMR                     OFC_TMR_TRC
#define CLI_OFC_DEBUG_UTIL                    OFC_UTIL_TRC
#define CLI_OFC_DEBUG_CLI                     OFC_CLI_TRC
#define CLI_OFC_DEBUG_PKT                     OFC_PKT_TRC
#define CLI_OFC_DEBUG_FLOW                    OFC_FLOWTBL_TRC
#define CLI_OFC_DEBUG_ACTS                    OFC_ACTS_TRC
#define CLI_OFC_DEBUG_GRP                     OFC_GRPTBL_TRC
#define CLI_OFC_DEBUG_METER                   OFC_METER_TRC

#define CLI_OFC_NO_DEBUG_FN_ENTRY             OFC_NO_FN_ENTRY
#define CLI_OFC_NO_DEBUG_FN_EXIT              OFC_NO_FN_EXIT
#define CLI_OFC_NO_DEBUG_FUNC                 CLI_OFC_NO_DEBUG_FN_ENTRY | CLI_OFC_NO_DEBUG_FN_EXIT
#define CLI_OFC_NO_DEBUG_MAIN                 OFC_NO_MAIN_TRC
#define CLI_OFC_NO_DEBUG_QUE                  OFC_NO_QUE_TRC
#define CLI_OFC_NO_DEBUG_TASK                 OFC_NO_TASK_TRC
#define CLI_OFC_NO_DEBUG_TMR                  OFC_NO_TMR_TRC
#define CLI_OFC_NO_DEBUG_UTIL                 OFC_NO_UTIL_TRC
#define CLI_OFC_NO_DEBUG_CLI                  OFC_NO_CLI_TRC
#define CLI_OFC_NO_DEBUG_PKT                  OFC_NO_PKT_TRC
#define CLI_OFC_NO_DEBUG_FLOW                 OFC_NO_FLOWTBL_TRC
#define CLI_OFC_NO_DEBUG_ACTS                 OFC_NO_ACTS_TRC
#define CLI_OFC_NO_DEBUG_GRP                  OFC_NO_GRPTBL_TRC
#define CLI_OFC_NO_DEBUG_METER                OFC_NO_METER_TRC


#define   OPENFLOW_MINUS_ONE        -1
#define   OFC_LEN_THREE             3
#define   OFC_LEN_FOUR              4
#define   OFC_MAX_IP_LEN            20
#define   OPENFLOW_CONNPROTO_TCP    1
#define   OPENFLOW_CONNPROTO_SSL    2
#define   OPENFLOW_CONNROLE_EQUAL    1
#define   OPENFLOW_CONNROLE_MASTER    2
#define   OPENFLOW_CONNROLE_SLAVE    3
#define   OPENFLOW_CONNBAND_OOB    1
#define   OPENFLOW_CONNBAND_INB    2
#define   OPENFLOW_ENABLE      1
#define   OPENFLOW_DISABLE     2
#define   OPENFLOW_NONE        0
#define   OPENFLOW_L2          1 
#define   OPENFLOW_L3          2 
#define   OPENFLOW_VER_V100    1
#define   OPENFLOW_VER_V131    2
#define   OPENFLOW_DROP    1
#define   OPENFLOW_CONTROLLER    2
#define   OPENFLOW_FAIL_SECURE    1
#define   OPENFLOW_FAIL_STANDALONE    2

