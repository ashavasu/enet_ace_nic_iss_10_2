/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mbsm.h,v 1.47 2017/01/17 14:10:28 siva Exp $
 *
 * Description:This file contains the exported definitions and  
 *             macros of MBSM                                   
 *
 *******************************************************************/
#ifndef _MBSM_H
#define _MBSM_H

#include "fssnmp.h"

#define  MBSM_TASK_NAME                 "MBSM"
#define  MBSM_TRUE                      1
#define  MBSM_FALSE                     2
#define  MBSM_SUCCESS                   OSIX_SUCCESS
#define  MBSM_FAILURE                   OSIX_FAILURE

#define  MBSM_ONE                       1

#define  MBSM_DEF_MAX_SLOTS             0 
#define  MBSM_DEF_MAX_LC_SLOTS          0
#define  MBSM_DEF_MAX_PORTS_PER_SLOT    0

#define MBSM_SLOT_INDEX_START           0

#define  MBSM_PORTS_PER_BYTE            8

/* Align size of each Mem Block to 4 bytes. */
#define MBSM_ALIGNED_ARRAY_SIZE(x)     (((x) + 3) & 0xfffffffc)

/* MBSM_MAX_PORTS_PER_SLOT is assumed to be multiples of 8 !!!!!*/
#define MBSM_PORT_LIST_SIZE          \
 (MBSM_ALIGNED_ARRAY_SIZE(MBSM_MAX_PORTS_PER_SLOT/MBSM_PORTS_PER_BYTE))

#define  MBSM_TRC_CRITICAL              0X01
#define  MBSM_TRC_ERR                   0X02
#define  MBSM_PROTO_TRC                 0x04
#define  MBSM_RED_TRC                   0x05

#define MBSM_MAX_LC_TYPES               13 /* Number of differnt Line Card
                                              supported */

#define MBSM_MAX_CARD_NAME_LEN          100 /* Size for Card name */

/* Used for CLI commands */
#define MBSM_PORT_SPEED_NAME_LEN        30 /* Length for speed definitions
                           like fe, ge etc... */
#define MBSM_MAX_PORTLIST_LEN           200 /* Length for CLI port range
                           token */

#define MBSM_MAX_PORT_SPEED_TYPES       10 /* Number of port speeds   
           supported - PortInfoTable */

#define MBSM_INVALID_SLOT_ID            -1
#define MBSM_INVALID_PORTCOUNT           0
#define MBSM_INVALID_CARDTYPE           -1
#define MBSM_INVALID_IFINDEX             0

#define MBSM_MAX_PORTS_PER_LC     (((MBSM_MAX_ALLOWED_PORTS_IN_LC + 31)/32) * 4)

#define MBSM_ENT_PHY_SERIAL_NUM           1
#define MBSM_ENT_PHY_ASSET_ID             2
#define MBSM_ENT_PHY_URIS                 4
#define MBSM_ENT_PHY_ALIAS_NAME           5

#define MBSM_ENT_PHY_SER_NUM_LEN         32
#define MBSM_ENT_PHY_ALIAS_LEN           32
#define MBSM_ENT_PHY_ASSET_ID_LEN        32
#define MBSM_ENT_PHY_URIS_LEN            255
#define ISS_SWITCH_PRIMARY_SLOTNO        0
#define ISS_SWITCH_SECONDARY_SLOTNO      1
/********************** Structure definitions *******************/

/* The portlist definition used between Hardware and MBSM */
typedef tPortList tMbsmPortList;

typedef UINT1 tMbsmCardPortList [MBSM_MAX_PORTS_PER_LC];
/* The structure containing Slot information used for messages 
 * passed to protocols */
typedef struct
{
    UINT4           au4ConnectingPort[SYS_DEF_MAX_INFRA_SYS_PORT_COUNT]; 
                                        /* Port used to Interconnect 2 Boards
                                         in Dual Node Stacking */
    UINT4           au4UnitId[SYS_DEF_MAX_INFRA_SYS_PORT_COUNT];
                                        /* Unit ID of Connecting Ports */

    UINT4           u4ConnectingPortCount;

    

}tMbsmConnectingPortInfo;

typedef struct
{
   tMbsmConnectingPortInfo MbsmConnectingPortInfo;
    INT4            i4SlotId;
    INT4            i4ModuleType;     /* Line Card or Control Card */
    INT4            i4CardType;       /* Specific Card type under Module Type */
    UINT4           u4NumPorts;       /* Number of ports in the card */
    UINT4           u4StartIfIndex;   /* Starting IfIndex for ports
      int the card */

    UINT1           u1SwFabricType; /* Switch fabric type.If the module type is
                                       LC and number of ports is 0, this will be set 
                                       to True. Else, this will be set to False.
                                     */
    INT1            i1Status;         /* Active, Down, Not Present,Inactive */
    INT1            i1PrevStatus;     /* Previous Status */
    INT1            i1IsPreConfigured;
    UINT1           au1SerialNum[MBSM_ENT_PHY_SER_NUM_LEN];
    UINT1           au1Alias[MBSM_ENT_PHY_ALIAS_LEN];
    UINT1           au1AssetId[MBSM_ENT_PHY_ASSET_ID_LEN];
    UINT1           au1Uris[MBSM_ENT_PHY_URIS_LEN];
    UINT1           u1IsPortMsg; /* Specifies wether port Insert/Remove message or not*/
    UINT1           u1IsRmPeerUpMsg; /* Specifies whether Msg is a Peer Up Msg
                                        from RM */
    UINT1           au1SlotCardName[MBSM_MAX_CARD_NAME_LEN+1];
    tMacAddr        SlotBaseMacAddr; /*Base mac address for this slot*/
} tMbsmSlotInfo;


/* The structure containing Port information for a Slot */
typedef struct
{
   UINT4           u4PortCount; /* Number of Ports */
   UINT4           u4ConnectingPortCount; /* Number of Connecting Ports */
   UINT4           u4StartIfIndex; /* Start If Index for the slot */
   tPortList       PortList; /* Bitmap of Interface Index */
   tPortList       PortListStatus; /* Bitmap of Interface Index Status */
} tMbsmPortInfo;


/* The structure combining both Slot and port info */
typedef struct
{
   tMbsmSlotInfo   MbsmSlotInfo;
   tMbsmPortInfo   MbsmPortInfo;
} tMbsmSlotPortInfo;

/* The structure to be used by MBSM to post messages to protocols */
typedef struct
{
    INT4            i4ProtoCookie;   /* Unique Protocol Identifier */
    tMbsmSlotInfo   MbsmSlotInfo;
    tMbsmPortInfo   MbsmPortInfo;
} tMbsmProtoMsg;


/* The structure to be used for the Ack messages from protocols to MBSM */
typedef struct
{
    INT4            i4ProtoCookie;   /* Unique protocol identifier */
    INT4            i4SlotId;
    INT4            i4RetStatus; /* SUCCESS or FAILURE */
    
} tMbsmProtoAckMsg;

/* The structure to be used for the Ack messages, on Port Creation on all the 
 * protocols from CFA to MBSM */
typedef struct
{
    INT4            i4SlotId;
    INT4            i4RetStatus; /* SUCCESS or FAILURE */
    
} tMbsmSyncAckMsg;

/* The structure containing Slot information from Hardware to MBSM */
typedef struct
{
    INT4            i4SlotId;
    INT4            i4ModType;
    INT4            i4CardType;  /* Line Card or Control Card */
    INT1            ai1CardName[MBSM_MAX_CARD_NAME_LEN];
    UINT4           u4NumPorts;  /* Number of ports in the Card */
    tMbsmPortList   MbsmPortList;/* Bitmap of ports present in the slot*/
    INT1            i1Status;    /* Inserted, Removed, Active, Inactive, 
        Standby */
    tMacAddr        SlotBaseMacAddr;  
    INT1            ai1Padding[1];

} tMbsmHwSlotInfo;


/* The structure of the message passed from hardware to MBSM during
 * module insertion/deletion events */
typedef struct
{
    INT4            i4NumEntries; /* Number of Slot Info entries */
    tMbsmHwSlotInfo FirstEntry;    /* Array contains entry for each
         slot */
} tMbsmHwMsg;


/* The structure of the message passed from MBSM to CFA for 
 * interface creation in CFA */
typedef struct
{
    INT4               i4NumEntries;      /* Number of entries in message */
    tMbsmSlotPortInfo  FirstEntry;        /* Array of entries for each slot */ 
} tMbsmCfaHwInfoMsg;


/* Structures used for card type table entry creation through
 * CLI */
typedef struct
{
    UINT1                au1Type[MBSM_PORT_SPEED_NAME_LEN];
    UINT1                au1PortList[MBSM_MAX_PORTLIST_LEN]; 
    UINT2 u2Pad;
} tCardPortSpeed;

typedef struct
{
    INT1                 ai1CardName[MBSM_MAX_CARD_NAME_LEN];
    UINT4                u4NumPorts;
    tCardPortSpeed       PortSpeed[MBSM_MAX_PORT_SPEED_TYPES];
} tMbsmCardInfo;


typedef enum
{
    MBSM_STATUS_DOWN = 0,  /* Card Oper Down */
    MBSM_STATUS_ACTIVE,    /* Card Present and working */
    MBSM_STATUS_INACTIVE,  /* Card Not present and interface not created */
    MBSM_STATUS_NP         /* Card Not Present and interfaces created */
} tMbsmCardStatus;


/*************   Definitions for messages and events  **************/

/* Message types posted to MBSM */
typedef enum
{
    /* We start with a high value, so as to avoid mixup
     * with the protocol message values */
    MBSM_MSG_UNKOWN = 100,
    MBSM_MSG_HW_LC_INSERT,
    MBSM_MSG_HW_LC_REMOVE,
    MBSM_MSG_HW_CC_INSERT,
    MBSM_MSG_HW_CC_REMOVE,
    MBSM_MSG_HW_LC_PORT_INSERT,
    MBSM_MSG_HW_LC_PORT_REMOVE,
    MBSM_MSG_PROTO_ACK,
    MBSM_MSG_BOOTUP_DISC,
    MBSM_MSG_INTF_CTRL,
    MBSM_MSG_HW_CONFIG,
    MBSM_MSG_CARD_INSERT,
    MBSM_MSG_CARD_REMOVE,
    MBSM_MSG_PRECONFIG_DEL,
    MBSM_MSG_MGMT_CONFIG,
    MBSM_MSG_SYNC_ACK,
    MBSM_MSG_LOAD_SHARING_ENABLE,
    MBSM_MSG_LOAD_SHARING_DISABLE,
    MBSM_MSG_LINK_STATUS_UPDATE,
    MBSM_RM_QMSG,
    MBSM_MSG_SELF_CARD_INSERT
} tMbsmMsgTypes;

/*************   Card Type Definitions      ************************/

typedef enum
{
    MBSM_INVALID_MODTYPE,
    MBSM_LINE_CARD,
    MBSM_CONTROL_CARD,
    MBSM_LINE_CARD_PORT,
    MBSM_CARD_NULL        /* this should always be the last entry */
} tMbsmCardTypes;
#define MBSM_INVALID_CARDINDEX           -1

/*************      Port speed types     ************************/
typedef enum
{
    MBSM_SPEED_INVALID = 0,
    MBSM_SPEED_ETHER,
    MBSM_SPEED_FAST_ETHER,
    MBSM_SPEED_GIG_ETHER,
    MBSM_SPEED_10GIG_ETHER,
    MBSM_SPEED_40GIG_ETHER,
    MBSM_SPEED_56GIG_ETHER,
    MBSM_SPEED_NULL      /* This should always be the last entry */
} tMbsmSpeedType;

/* The structure used to extract MBSM system information from NVRAM */
typedef struct
{
    int       u4MbsmMaxSlots;        /* Max. Slots supported in the system */
    int       u4MbsmMaxLcSlots;      /* Max. Line Card slots in the system */
    int       u4MbsmMaxPortsPerSlot; /* Max. Ports supported in a Slot */
} tMbsmSysInfo;

/**********************   EXTERNS  ******************************/
extern UINT1           gau1MbsmPortBitMask[MBSM_PORTS_PER_BYTE];
extern UINT4           gu4MbsmTrcLevel;

/**********************   MACROS   ******************************/

#define MBSM_IS_MEMBER_PORT(au1PortArray, u2Port, u1Result) \
        {\
           UINT2 u2PortBytePos;\
           UINT2 u2PortBitPos;\
           u2PortBytePos = (UINT2)(u2Port / MBSM_PORTS_PER_BYTE);\
           u2PortBitPos  = (UINT2)(u2Port % MBSM_PORTS_PER_BYTE);\
      if (u2PortBitPos  == 0) {u2PortBytePos -= 1;} \
           \
           if ((au1PortArray[u2PortBytePos] \
                & gau1MbsmPortBitMask[u2PortBitPos]) != 0) {\
           \
              u1Result = MBSM_TRUE;\
           }\
           else {\
           \
              u1Result = MBSM_FALSE; \
           } \
        }

 /* Warning!!! - Do not call the macro with u2Port as 0 or Invalid Port.*/
#define MBSM_SET_MEMBER_PORTLIST(au1PortArray, u2Port) \
           {\
              UINT2 u2PortBytePos;\
              UINT2 u2PortBitPos;\
              u2PortBytePos = (UINT2)(u2Port / MBSM_PORTS_PER_BYTE);\
              u2PortBitPos  = (UINT2)(u2Port % MBSM_PORTS_PER_BYTE);\
       if (u2PortBitPos  == 0) {u2PortBytePos -= 1;} \
           \
              au1PortArray[u2PortBytePos] = (UINT1)(au1PortArray[u2PortBytePos] \
                             | gau1MbsmPortBitMask[u2PortBitPos]);\
           }

#define MBSM_RESET_MEMBER_PORTLIST(au1PortArray, u2Port) \
           {\
              UINT2 u2PortBytePos;\
              UINT2 u2PortBitPos;\
              u2PortBytePos = (UINT2)(u2Port / MBSM_PORTS_PER_BYTE);\
              u2PortBitPos  = (UINT2)(u2Port % MBSM_PORTS_PER_BYTE);\
       if (u2PortBitPos  == 0) {u2PortBytePos -= 1;} \
           \
              au1PortArray[u2PortBytePos] = (UINT1)(au1PortArray[u2PortBytePos] \
                             & ~gau1MbsmPortBitMask[u2PortBitPos]);\
           }
/* Set/reset the portliststatus bits */
#define MBSM_SET_MEMBER_PORTLIST_STATUS     MBSM_SET_MEMBER_PORTLIST
#define MBSM_RESET_MEMBER_PORTLIST_STATUS   MBSM_RESET_MEMBER_PORTLIST


#define MBSM_SLOT_INFO_SLOT_ID(pSlotInfo)  (pSlotInfo)->i4SlotId

#define MBSM_SLOT_INFO_CARDTYPE(pSlotInfo) (pSlotInfo)->i4CardType

#define MBSM_SLOT_INFO_NUMPORTS(pSlotInfo) (pSlotInfo)->u4NumPorts

#define MBSM_SLOT_INFO_STARTINDEX(pSlotInfo) (pSlotInfo)->u4StartIfIndex

#define MBSM_SLOT_INFO_STATUS(pSlotInfo) (pSlotInfo)->i1Status
#define MBSM_SLOT_INFO_ISPORTMSG(pSlotInfo) ((pSlotInfo)->u1IsPortMsg)
#define MBSM_SLOT_INFO_IS_RM_PEER_UP_MSG(pSlotInfo) ((pSlotInfo)->u1IsRmPeerUpMsg)

/* Protocols use this ISPRECONF for a different purpose, hence we use
 * a different macro inside MBSM from what is used for protocols */
#define MBSM_SLOT_INFO_ISPRECONF(pSlotInfo) \
  ((((pSlotInfo)->i1PrevStatus == MBSM_STATUS_INACTIVE) && \
  ((pSlotInfo)->i1Status == MBSM_STATUS_ACTIVE) ) ? MBSM_FALSE: MBSM_TRUE)

#define MBSM_PORT_INFO_PORTLIST(pPortInfo) (pPortInfo)->PortList
#define MBSM_PORT_INFO_PORTLIST_STATUS(pPortInfo) (pPortInfo)->PortListStatus

#define MBSM_PORT_INFO_PORTCOUNT(pPortInfo) (pPortInfo)->u4PortCount

#define MBSM_PORT_INFO_STARTINDEX(pPortInfo) (pPortInfo)->u4StartIfIndex

#define MBSM_GLB_TRC()             gu4MbsmTrcLevel

#ifdef TRACE_WANTED
#define   MBSM_DBG(Flag, Module, Fmt) \
          UtlTrcLog(MBSM_GLB_TRC(), Flag, Module, Fmt)

#define MBSM_DBG_ARG1(Flag, Module, Fmt, Arg1) \
        UtlTrcLog(MBSM_GLB_TRC(), Flag, Module, Fmt, Arg1)

#define MBSM_DBG_ARG2(Flag, Module, Fmt, Arg1, Arg2) \
        UtlTrcLog(MBSM_GLB_TRC(), Flag, Module, Fmt, Arg1, Arg2)

#define MBSM_DBG_ARG3(Flag, Module, Fmt, Arg1, Arg2, Arg3) \
        UtlTrcLog(MBSM_GLB_TRC(), Flag, Module, Fmt, Arg1, Arg2, Arg3)
#else /* TRACE_WANTED */
#define MBSM_DBG(Flag, Module, Fmt)
#define MBSM_DBG_ARG1(Flag, Module, Fmt, Arg1)
#define MBSM_DBG_ARG2(Flag, Module, Fmt, Arg1, Arg2)
#define MBSM_DBG_ARG3(Flag, Module, Fmt, Arg1, Arg2, Arg3)
#endif

#define  MBSM_GET_SYS_INFO_FROM_NVRAM(NvRamData) \
  NvRamData.MbsmSysInfo

#define  MBSM_GET_MAX_SLOTS_FROM_NVRAM(pNvRamData) \
  pNvRamData->MbsmSysInfo.u4MbsmMaxSlots

#define  MBSM_GET_MAX_LC_SLOTS_FROM_NVRAM(pNvRamData) \
  pNvRamData->MbsmSysInfo.u4MbsmMaxLcSlots

#define  MBSM_GET_MAX_PORTS_PER_SLOT_FROM_NVRAM(pNvRamData) \
  pNvRamData->MbsmSysInfo.u4MbsmMaxPortsPerSlot
/**********************   Exported APIs  ***********************/

/* Function to send Ack messages from protocols to MBSM */
INT4 MbsmSendAckFromProto PROTO((tMbsmProtoAckMsg *pAckMsg));

/* Function to send events from protocols to MBSM */
UINT4 MbsmSendEventToMbsmTask PROTO ((UINT4 u4Event));

/* Function returns Slot information for Slot Id passed */
INT4 MbsmGetSlotInfo PROTO ((INT4 i4SlotId, tMbsmSlotInfo *pMbsmSlotInfo));

/* Function returns Port information for Slot Id passed */
INT4 MbsmGetPortInfo PROTO ((INT4 i4SlotId, tMbsmPortInfo * pPortInfo));
/* Function to get Port MTU from the Port Map table */
INT4 MbsmGetPortMtu PROTO ((UINT4 u4IfIndex, UINT4 *pu4IfMtu));

/* Function to get Port Encap type from the Port Map table */
INT4 MbsmGetPortEncap PROTO ((UINT4 u4IfIndex, UINT4 *pu4IfEncap));

/* Function that notifies Mbsm Module about the insertion and deletion of
 * Control/Line Cards as indicated by the Driver */
INT4 MbsmNotifyMbsmFromDriver PROTO ((tMbsmHwMsg * pData, INT4 i4Event));

/* Function to get the Slot Id from the If Index */
INT4 MbsmGetSlotFromPort PROTO ((UINT4 u4IfIndex, INT4 *pi4SlotId));

VOID MbsmMain PROTO ((INT1 *));

INT4 MbsmGetStartIfIndex PROTO ((INT4 i4SlotId, UINT4 *pu4StartIfIndex));

INT4 MbsmGetCardIndexFromCardName PROTO ((INT1 *pi1CardName, 
   INT4 *pi4CardIndex));

/* Function to get Port Speed Type of a card from the Card Index */
INT4 MbsmGetPortSpeedTypeFromCardIndex PROTO ((INT4 i4CardIndex,
                UINT4 u4PortNum, UINT1  *pu1SpeedType));

/* Function to get Port Speed Type of a Interface from the IfIndex */
INT4 MbsmGetPortSpeedTypeFromIfIndex PROTO ((UINT4 u4IfIndex, 
                                             UINT1 *pu1SpeedType));

INT4 MbsmIsDefaultInit PROTO ((VOID));

INT4 MbsmGetBootFlag PROTO ((VOID));

VOID MbsmSetBootFlag PROTO ((UINT4));

INT4
MbsmSendSyncAckFromCfa PROTO ((tMbsmSyncAckMsg * pAckMsg));

INT4 MbsmWaitForSelfAttachCompletion PROTO ((INT4 i4SlotId));
INT4  MbsmGetCtrlCardCount PROTO ((VOID));
UINT1 MbsmGetLoadSharingStatus PROTO ((VOID));

INT4 MbsmLock (VOID);
INT4 MbsmUnLock (VOID);


VOID MbsmRedPreConfSlotUpdateNotifyToCfa PROTO ((VOID));
INT4 MbsmRedRmInitAndDeRegister PROTO ((VOID));
INT4 MbsmRedRmInitAndRegister PROTO ((VOID));
INT4 MbsmSendSelfAttachAckFromCfa  (VOID);
/* Semaphores for Data Protection from Protocols */



#define MBSM_SYNC_SEM                  (const UINT1 *) "MSYC"

#define MBSM_SYNC_SEMID                gMbsmGlobalInfo.gMbsmSyncSemId

#define MBSM_SYNC_CREATE_SEM(SemName, InitCnt, Flags, pSemId) \
   OsixCreateSem (SemName, InitCnt, Flags, pSemId)
#define MBSM_SYNC_TAKE_SEM()          \
       (OsixTakeSem (SELF, (CONST UINT1 *)MBSM_SYNC_SEM, 0, 0))
#define MBSM_SYNC_GIVE_SEM()          \
       (OsixGiveSem (SELF, (CONST UINT1 *)MBSM_SYNC_SEM))
#define MBSM_SYNC_DELETE_SEM()          \
       (OsixDeleteSem (SELF, (CONST UINT1 *)MBSM_SYNC_SEM))
#define MBSM_LOCK()     MbsmLock ()
#define MBSM_UNLOCK()   MbsmUnLock ()
PUBLIC INT4 MbsmSetSlotInfo PROTO ((INT4 i4SlotId, tMbsmSlotInfo *pMbsmSlotInfo));
PUBLIC INT4 MbsmGetSelfAttachStatus PROTO((VOID));
PUBLIC INT4 MbsmGetSlotIndexStatus PROTO ((INT4 i4SlotId));
PUBLIC INT4 MbsmGetSlotIdFromRemotePort PROTO ((UINT4 u4RemotePort, UINT4 *pu4SlotId));
PUBLIC VOID MbsmGetSlotsFromRemotePorts PROTO ((UINT1 *pPortList, UINT1 *pu1SlotList));
PUBLIC VOID MbsmGetDissRoleFromSlotId PROTO ((UINT4 u4SlotId, UINT1 *pu1DissRole));
PUBLIC VOID MbsmGetDissMasterSlotId PROTO ((UINT4 *pu4SlotId));
PUBLIC VOID MbsmGetPrevSlotsTotalPortCount PROTO ((INT4 i4SelfSlot, UINT4 *pu4PortCount));
PUBLIC INT4 MbsmSetMacAddressToSlotId PROTO ((tMacAddr DestMacAddr, UINT4 u4SlotId));
PUBLIC INT4 MbsmGetMacAddressFromSlotId PROTO ((UINT4 u4SlotId, tMacAddr *pDestMacAddr));
PUBLIC INT4 MbsmGetSlotIdFromMacAddress PROTO (( tMacAddr MacAddr,UINT4 *pu4SlotId));
PUBLIC INT4 MbsmGetPeerNodeCount PROTO (( VOID));
PUBLIC INT4 MbsmGetBridgeIndexFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4BrgIndex);
PUBLIC INT4 MbsmGetIfIndexFromBridgeIndex (UINT4 u4BrgIndex, UINT4 *pu4IfIndex);
PUBLIC UINT1 MbsmIsNpBulkSyncInProgress PROTO ((UINT4 u4NpModId));
PUBLIC VOID MbsmInformRestorationComplete (VOID);
PUBLIC VOID
MbsmGetNpSyncProgressStatus PROTO ((UINT4 *pu4Status));
#endif /* _MBSM_H */
