/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dns.h,v 1.24 2017/12/19 10:00:03 siva Exp $
 * 
 * Description: This file contains exported definitions and functions
 *              of DNS module.
 *********************************************************************/
#ifndef _DNS_H
#define _DNS_H


/* Macro for denoting DNS system control */
#define DNS_START       1
#define DNS_SHUTDOWN    2

/* Macro for denoting DNS module status */
#define DNS_ENABLE      1
#define DNS_DISABLE     2

/* Default Dns Cache Record TTL */
#define DNS_CACHE_DEFAULT_TTL     100


/* Macros for Cache Ststus */
#define DNS_CACHE_ENABLE          1
#define DNS_CACHE_DISABLE         2
#define DNS_CACHE_CLEAR           3
#define DNS_MAX_QUERY_LEN         63
#define DNS_MAX_RESOLVER_LEN  (2*DNS_MAX_DOMAIN_NAME_LEN)

/*IP Resolved Status*/
#define DNS_RESOLVED              1
#define DNS_NOT_RESOLVED          -1
#define DNS_IN_PROGRESS           2
#define DNS_CACHE_FULL            3


#define DNS_BLOCK                 0
#define DNS_NONBLOCK              1
/* Index of Default Name Server */
#define DNS_DEF_NAME_SERVER_INDEX   0

/* Index of Default Domain name */
#define DNS_DEF_DOMAIN_NAME_INDEX   0
#define DNS_ZERO                    0

#define DNS_SUCCESS    OSIX_SUCCESS
#define DNS_FAILURE    OSIX_FAILURE
#define DNS_SKIP       2 

#define DNS_INITSHUT_TRC           0x01
#define DNS_CTRL_TRC               0x02
#define DNS_QUERY_TRC              0x04
#define DNS_RESP_TRC               0x08
#define DNS_CACHE_TRC              0x10
#define DNS_FAILURE_TRC            0x20
#define DNS_ALL_TRC                (DNS_INITSHUT_TRC | DNS_CTRL_TRC | DNS_QUERY_TRC | DNS_RESP_TRC | DNS_CACHE_TRC | DNS_FAILURE_TRC)


#define DNS_v4_RESOLVE           0x01
#define DNS_v6_RESOLVE           0x02
#define DNS_v4v6_RESOLVE         (DNS_v4_RESOLVE | DNS_v6_RESOLVE)
#define DNS_MAX_DATA_LEN            64

typedef struct _tDNSResolvedIpInfo
{
    tIPvXAddr           Resolv4Addr;
    tIPvXAddr           Resolv6Addr;
}
tDNSResolvedIpInfo;

typedef struct _tDnsCacheRRInfo
{
    tRBNodeEmbd    CacheNode;
    tTmrBlk        CacheExpTmrNode; 
    UINT1          au1RRName[DNS_MAX_QUERY_LEN];
    UINT1          au1Pad[1];
    UINT4          u4RRClass;
    UINT4          u4RRType;
    UINT4          u4RRIndex;
    UINT4          u4RRTTL;
    tIPvXAddr      SourceIP;
    UINT1          au1RRData[DNS_MAX_DATA_LEN];
    UINT1          au1RRPrettyName[DNS_MAX_DATA_LEN];
    UINT4          u4RRStatus;
 UINT4          u4HitCount;
}tDnsCacheRRInfo;


/* Main Task Function */
PUBLIC VOID DnsResolverMainTask PROTO ((INT1 *));

/* Function to Resolve IP Address from a Name */
PUBLIC INT4 DNSResolveIPFromName PROTO ((UINT1 *, tIPvXAddr *, UINT4 *));
PUBLIC INT4 DNSResolveIPFromNameServer PROTO ((UINT1 *, UINT1 *));
PUBLIC INT4 DNSApiResolveIPvXFromName PROTO ((UINT1 *, UINT1 , tDNSResolvedIpInfo *, UINT4 *));
PUBLIC INT4 DnsLock (VOID); 
PUBLIC INT4 DnsUnLock (VOID);
PUBLIC INT4 DNSApiResolverCacheAndAct PROTO ((UINT1 *, UINT1 , tDNSResolvedIpInfo *));
#endif /* _DNS_H */ 
