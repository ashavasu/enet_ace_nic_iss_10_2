
/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: pimnpwr.h,v 1.1 2014/04/10 13:32:40 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              for PIMV6 wrappers
 *              
 ********************************************************************/
#ifndef __PIM_NP_WR_H__
#define __PIM_NP_WR_H__


#ifdef PIMV6_WANTED
UINT1 PimFsNpIpv6McAddRouteEntry PROTO ((UINT4 u4VrId, UINT1 * pGrpAddr, UINT4 u4GrpPrefix, UINT1 * pSrcIpAddr, UINT4 u4SrcIpPrefix, UINT1 u1CallerId, tMc6RtEntry rtEntry, UINT2 u2NoOfDownStreamIf, tMc6DownStreamIf * pDownStreamIf));
UINT1 PimFsNpIpv6McDelRouteEntry PROTO ((UINT4 u4VrId, UINT1 * pGrpAddr, UINT4 u4GrpPrefix, UINT1 * pSrcIpAddr, UINT4 u4SrcIpPrefix, tMc6RtEntry rtEntry, UINT2 u2NoOfDownStreamIf, tMc6DownStreamIf * pDownStreamIf));
UINT1 PimFsNpIpv6McGetHitStatus PROTO ((UINT1 * pSrcIpAddr, UINT1 * pGrpAddr, UINT4 u4Iif, UINT2 u2VlanId, UINT4 * pu4HitStatus));
UINT1 PimFsNpIpv6McAddCpuPort PROTO ((UINT1 u1GenRtrId, UINT1 * pGrpAddr, UINT4 u4GrpPrefix, UINT1 * pSrcIpAddr, UINT4 u4SrcIpPrefix, tMc6RtEntry rtEntry, UINT2 u2NoOfDownStreamIf, tMc6DownStreamIf * pDownStreamIf));
UINT1 PimFsNpIpv6McDelCpuPort PROTO ((UINT1 u1GenRtrId, UINT1 * pGrpAddr, UINT4 u4GrpPrefix, UINT1 * pSrcIpAddr, UINT4 u4SrcIpPrefix, tMc6RtEntry rtEntry));
UINT1 PimFsNpIpv6GetMRouteStats PROTO ((UINT4 u4VrId, UINT1 * pu1GrpAddr, UINT1 * pu1SrcAddr, INT4 i4StatType, UINT4 * pu4RetValue));
UINT1 PimFsNpIpv6GetMRouteHCStats PROTO ((UINT4 u4VrId, UINT1 * pu1GrpAddr, UINT1 * pu1SrcAddr, INT4 i4StatType, tSNMP_COUNTER64_TYPE * pu8RetValue));
UINT1 PimFsNpIpv6GetMNextHopStats PROTO ((UINT4 u4VrId, UINT1 * pu1GrpAddr, UINT1 * pu1SrcAddr, INT4 i4OutIfIndex, INT4 i4StatType, UINT4 * pu4RetValue));
UINT1 PimFsNpIpv6GetMIfaceStats PROTO ((UINT4 u4VrId, INT4 i4IfIndex, INT4 i4StatType, UINT4 * pu4RetValue));
UINT1 PimFsNpIpv6GetMIfaceHCStats PROTO ((UINT4 u4VrId, INT4 i4IfIndex, INT4 i4StatType, tSNMP_COUNTER64_TYPE * pu8RetValue));
UINT1 PimFsNpIpv6SetMIfaceTtlTreshold PROTO ((UINT4 u4VrId, INT4 i4IfIndex, INT4 i4TtlTreshold));
UINT1 PimFsNpIpv6SetMIfaceRateLimit PROTO ((UINT4 u4VrId, INT4 i4IfIndex, INT4 i4RateLimit));
UINT1 PimFsPimv6NpInitHw PROTO ((VOID));
UINT1 PimFsPimv6NpDeInitHw PROTO ((VOID));
UINT1 PimFsNpIpv6McClearHitBit PROTO ((UINT2 u2VlanId, UINT1 * pSrcIpAddr, UINT1 * pGrpAddr));
#endif /* PIMV6_WANTED */
#endif /* __PIM_NP_WR_H__ */
