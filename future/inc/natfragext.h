#ifndef _NATFRAGEXT_H
#define _NATFRAGEXT_H

PUBLIC tTMO_DLL       gNatFragList;
PUBLIC tTmrDesc       gaNatTimerDesc[]; /*[NAT_MAX_TIMERS];*/
PUBLIC tTimerListId   gNatTimerListId;

#define   NAT_FRAG_TIMER_EXPIRY_EVENT              0x00000020
#define   NAT_FRG_REASM_TIMER                      0 /* index for gaNatTimerDesc */

#define   NAT_MAX_NO_OF_FRAGMENTS_PER_FRAME        20
#define   NAT_MAX_NO_OF_FRAGMENT_STREAM            100
#define   NAT_REASM_MAX_SIZE                       20480

#define   NAT_ZERO_MTU                             0
PUBLIC VOID NatFragProcessTimeOut (VOID);
PUBLIC UINT4 NatFragInit(VOID);
PUBLIC INT4 NatFragDeInit(VOID);
PUBLIC tCRU_BUF_CHAIN_HEADER*  NatFragReassemble ( tCRU_BUF_CHAIN_HEADER* pBuf, UINT4 u4Port, UINT4 u4Direction);
PUBLIC UINT4  NatIsFragReq(tCRU_BUF_CHAIN_HEADER *pBuf, UINT2 u2IfIndex, UINT4 *pu4MTU);
PUBLIC UINT4 NatFragment( tCRU_BUF_CHAIN_HEADER *pBuf,UINT2 u2IfIndex, UINT4 u4MTU, tTMO_DLL *pFragList);
PUBLIC tCRU_BUF_CHAIN_HEADER *NatGetFragFrmQueToSend(tTMO_DLL *pFragList);
PUBLIC UINT4 NatFreeOutFragList(tTMO_DLL *pFragList);
PUBLIC VOID NatTmrHandleExpiry (VOID);
#endif
