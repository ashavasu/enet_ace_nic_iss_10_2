/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: netdev.h,v 1.5 2010/10/13 08:59:32 prabuc Exp $
 *
 * Description: This file contains Macro's, Constants, Globals and Structure
 *              Definition's related to the ISS Netdevice Module
 *
 **************************************************************************/
#ifndef _NET_DEV_H_
#define _NET_DEV_H_

/* For Linux Version related Macro's */
#include <linux/version.h>

#include "lr.h"
#include "cfa.h"

#include "ip.h"
#include "lnxip.h"

/* ----------------- Constant & Macro Definitions ------------------ */

/* Macros related to Character Special File (or Device) Interface b/w ISS & 
 * ISS Netdevice Module */

#define NET_DEVICE_FILE_NAME     "/dev/IssNetDev"
#define NET_DEVICE_MAJOR_NUMBER  105
#define NET_DEVICE_MINOR_NUMBER  0

/* ISS Netdevice Module Name */

#define NETDEV_MODULE_NAME        "ISSnet.ko"

#define NET_DEV_INTF_IOCTL        _IOWR(NET_DEVICE_MAJOR_NUMBER, 1, char *)

/* FD used for performing ioctl ()'s from user land */
#if defined(KERNEL_WANTED) && !defined (NP_KERNEL_WANTED)
#define GDD_CHAR_DEV_FD           gi4NetDevFd
#define GDD_INTERFACE             NET_DEV_INTF_IOCTL 
#endif

#ifdef LINUX_KERN

/* Network Device's Private Data Structure */

typedef struct
{
   tNetDevStats      NetDevStats;     /* Statistics for the Logical Device */
} tNetDevPrivData;

#endif /* LINUX_KERN */

/* ----------------------- Global Declarations --------------------- */

/* File Descriptor for Character Special File b/w ISS & IssNetDevice Module */
extern INT4 gi4NetDevFd;

#endif /* _NET_DEV_H_ */

/*-----------------------------------------------------------------------*/
/*                       End of the File netdev.h                        */
/*-----------------------------------------------------------------------*/
