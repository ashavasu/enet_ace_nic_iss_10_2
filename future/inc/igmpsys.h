/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igmpsys.h,v 1.1 2015/02/13 11:16:24 siva Exp $
 *
 * Description:This file contains the exported definitions and  
 *             macros of IGMP                                    
 *
 *******************************************************************/
#ifndef _IGMPSYS_H
#define _IGMPSYS_H
#include "utltrc.h"

enum {
SYS_LOG_MEM_ALLOC_FAILURE = 1,
SYS_LOG_BUFFER_MEM_ALLOC_FAIL,
SYS_LOG_BUFFER_REL_FAIL,
SYS_LOG_RB_TREE_ADD_FAIL,
SYS_LOG_RB_TREE_CREATE_FAIL,
SYS_LOG_RB_TREE_DEL_FAIL,
NP_ENABLE_FILTER_FAIL,
NP_DISABLE_FILTER_FAIL,
SYS_LOG_MEM_REL_FAIL,
IGMP_NP_SET_IF_JOIN_RATE_FAILED,
IGMP_NP_DISABLE_FAILED,
IGMP_NP_ENABLE_FAILED,
SYS_LOG_QRY_SEND_FAILURE,
QUEUE_SEND_FAIL,
EVENT_SEND_FAIL,
SYS_LOG_IGMP_GRP_DEL_FAILURE,
SYS_LOG_MLD_NP_ENABLE_FAILED,
SYS_LOG_MLD_NP_DISABLE_FAILED,
SYS_LOG_MLD_NP_DESTROY_FILTER_FAIL,
SYS_LOG_MLD_NP_DESTROY_FP_FAIL,
MLD_NP_SET_IF_JOIN_RATE_FAILED,
MLD_NP_MBSM_ENABLE_FAILED,
SYS_LOG_IGMP_NP_SET_TTL_FAILED,
SYS_LOG_IGMP_NP_CRT_FRWD_ENTRY_FAILED,
SYS_LOG_IGMP_NP_DEL_FRWD_ENTRY_FAILED,
SYS_LOG_IGMP_NP_DEL_EXIST_ENTRY_FAILED,
SYS_LOG_IGMP_NP_ADD_EXIST_ENTRY_FAILED,
SYS_LOG_IGMP_NP_ADD_ENTRY_MEM_FAILED,
SYS_LOG_IGMP_NP_IPMC_ENTRY_FAILED,
SYS_LOG_IGMP_NP_IP_MULTICAST_ROUTE_FAILED,
SYS_LOG_RB_TREE_CREATE_MCAST_FRWD_FAIL,
SYS_LOG_HASH_TBL_CREATE_UPSTRM_FAIL,
SYS_LOG_MEM_ALLOC_SRC_INFO_FAIL,
SYS_LOG_IGMP_NP_IP_MCAST_ROUTE_ENABLE_FAIL,
SYS_LOG_IGMP_NP_IP_MCAST_ROUTE_DISABLE_FAIL,
SYS_LOG_IGMP_NP_MEM_ALLOT_FAIL,
SYS_LOG_V3_ENCODING_FAIL,
SYS_LOG_PROCESS_GEN_QUERY_FAIL,
SYS_LOG_PROCESS_GROUP_QUERY_FAIL,
SYS_LOG_PROCESS_GROUP_SRC_QUERY_FAIL,
SYS_LOG_V1_CONSTRUCT_FAIL,
SYS_LOG_V2_CONSTRUCT_FAIL,
SYS_LOG_UPD_ROUTE_FAIL,
SYS_LOG_MEM_ALLOC_BITLIST_FAIL
};

#ifdef __IGMPMAIN_C__
CONST CHR1 *IgmpSysErrString [] = 
{
   NULL,
   "Memory Allocation Failure\r\n",
     "Buffer Allocation Failure\r\n",
     "Buffer Release Failure\r\n",
     "RB Tree Addition Failure\r\n",
     "RB Tree Creation Failure\r\n",
     "RB Tree Deletion Failure\r\n",
     "Enabling Filter on Hardware Failed\r\n",
     "Disbling Filter on Hardware Failed\r\n",
     "Memory Release Failure\r\n",
     "Setiing Interface Join rate on Hardware Failed\r\n",
     "Disabling IGMP on Hardware Failed\\r\n",
     "Enabling IGMP on Hardware Failed\\r\n",
     "Query Send Failure\r\n",
     "Queue Send Failure\r\n",
     "Event Send Failure\r\n",
     "IGMP Group Deletion Failure\r\n",
     "Enabling MLD on Hardware Failed\r\n",
     "Disabling MLD on Hardware Failed\r\n",
     "Destroying MLD Filter on Hardware Failed\r\n",
     "Destroying MLD FP on Hardware Failed\r\n",
     "Setiing Interface Join rate for MLD on Hardware Failed\r\n",
     "Enabling MLD MBSM on Hardware Failed\r\n",
     "Setiing IGMP TTL on Hardware Failed\r\n",
     "Creating IGMP Forward Entry on Hardware Failed\r\n",
     "Deleting IGMP Forward Entry on Hardware Failed\r\n",
     "Adding IGMP Existing Entry on Hardware Failed\r\n",
     "Adding IGMP Memory Entry on Hardware Failed\r\n",
     "Adding IGMP IPMC Entry on Hardware Failed\r\n",
     "Adding IGMP Multicast Route on Hardware Failed\r\n",
     "Multicast Forward entry for RB tree Failed\r\n",
     "Upstream Hash Table Creation Failed\r\n",
     "Memory Allocation for Source Info Failed\r\n",
     "Enabling IGMP Multicast route on Hardware Failed\r\n",
     "Disabling IGMP Multicast route on Hardware Failed\r\n",
     "IGMP Memory ALlocation on Hardware Failed\r\n",
     "V3 Message Encoding Failed\r\n",
     "Processing of General Query Message Failed\r\n",
     "Processing of Group Query Message Failed\r\n",
     "Processing of Group source Query Message Failed\r\n",
     "V1 Message Construction Failed\r\n",
     "V2 Message Construction Failed\r\n",
     "SYS_LOG_MEM_ALLOC_BITLIST_FAIL\r\n",
     "UPD Route Failed\r\n"

};
#else
extern CONST CHR1 *IgmpSysErrString [];
#endif

#endif /* _IGMP_H */
