/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: igmpnpwr.h,v 1.1 2014/12/18 12:24:58 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              for IGMP wrappers
 *              
 ********************************************************************/
#ifndef _IGMPNP_WR_H_
#define _IGMPNP_WR_H_

#include "igmpnp.h"

#define  FS_IGMP_HW_ENABLE_IGMP                                 1
#define  FS_IGMP_HW_DISABLE_IGMP                                2
#define  FS_NP_IPV4_SET_IGMP_IFACE_JOIN_RATE                    3
#ifdef MBSM_WANTED
#define  FS_IGMP_MBSM_HW_ENABLE_IGMP                            4
#endif

/* Required arguments list for the igmp NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

typedef struct {
    INT4  i4IfIndex;
    INT4  i4RateLimit;
} tIgmpNpWrFsNpIpv4SetIgmpIfaceJoinRate;

#ifdef MBSM_WANTED
typedef struct {
    tMbsmSlotInfo *  pSlotInfo;
} tIgmpNpWrFsIgmpMbsmHwEnableIgmp;
#endif

typedef struct IgmpNpModInfo {
union {
    tIgmpNpWrFsNpIpv4SetIgmpIfaceJoinRate  sFsNpIpv4SetIgmpIfaceJoinRate;
#ifdef MBSM_WANTED
    tIgmpNpWrFsIgmpMbsmHwEnableIgmp  sFsIgmpMbsmHwEnableIgmp;
#endif
    }unOpCode;

#define  IgmpNpFsNpIpv4SetIgmpIfaceJoinRate  unOpCode.sFsNpIpv4SetIgmpIfaceJoinRate;
#ifdef MBSM_WANTED
#define  IgmpNpFsIgmpMbsmHwEnableIgmp  unOpCode.sFsIgmpMbsmHwEnableIgmp;
#endif
} tIgmpNpModInfo;


/* Function Prototype for Generic NP OpCode specific wrappers
 * defined in Igmpnpapi.c */

UINT1 IgmpFsIgmpHwEnableIgmp PROTO ((VOID));
UINT1 IgmpFsIgmpHwDisableIgmp PROTO ((VOID));
UINT1 IgmpFsNpIpv4SetIgmpIfaceJoinRate PROTO ((INT4 i4IfIndex, INT4 i4RateLimit));
#ifdef MBSM_WANTED
UINT1 IgmpFsIgmpMbsmHwEnableIgmp PROTO ((tMbsmSlotInfo * pSlotInfo));
#endif
#endif
