/********************************************************************
 * Copyright (C) Aricent Technology Holdings Limited, 2008 - 2009
 *
 * $Id: rmn2npwr.h,v 1.2 2015/09/13 10:36:23 siva Exp $
 *
 * Description: This file contains the structure definitions
 *              of RMON2
 *
 *******************************************************************/

#ifndef __RMN2_NP_WR_H
#define __RMN2_NP_WR_H

#include "rmon2.h"
#include "rmon2np.h"


/* OPER ID */

#define  FS_RMONV2_ADD_FLOW_STATS                            1
#define  FS_RMONV2_COLLECT_STATS                             2
#define  FS_RMONV2_DISABLE_PROBE                             3
#define  FS_RMONV2_DISABLE_PROTOCOL                          4
#define  FS_RMONV2_ENABLE_PROBE                              5
#define  FS_RMONV2_ENABLE_PROTOCOL                           6
#define  FS_RMONV2_HW_SET_STATUS                             7
#define  FS_RMONV2_HW_GET_STATUS                             8
#define  FS_RMONV2_NP_DE_INIT                                9
#define  FS_RMONV2_NP_INIT                                   10
#define  FS_RMONV2_REMOVE_FLOW_STATS                         11
#define  FS_RMONV2_GET_PORT_ID                               12

/* Required arguments list for the rmon NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

typedef struct {
    tPktHeader  FlowStatsTuple;
} tRmonv2NpWrFsRMONv2AddFlowStats;

typedef struct {
    tPktHeader     FlowStatsTuple;
    tRmon2Stats *  pStatsCnt;
} tRmonv2NpWrFsRMONv2CollectStats;

typedef struct {
    UINT1 *  PortList;
    UINT4      u4InterfaceIdx;
    UINT2      u2VlanId;
    UINT1      au1pad[2];
} tRmonv2NpWrFsRMONv2DisableProbe;

typedef struct {
    UINT4  u4ProtocolLclIdx;
} tRmonv2NpWrFsRMONv2DisableProtocol;

typedef struct {
    UINT1 *  PortList;
    UINT4      u4InterfaceIdx;
    UINT2      u2VlanId;
    UINT1      au1pad[2];
} tRmonv2NpWrFsRMONv2EnableProbe;

typedef struct {
    tRmon2ProtocolIdfr  Rmon2ProtoIdfr;
} tRmonv2NpWrFsRMONv2EnableProtocol;

typedef struct {
    UINT4  u4RMONv2Status;
} tRmonv2NpWrFsRMONv2HwSetStatus;

typedef struct {
    tPktHeader  FlowStatsTuple;
} tRmonv2NpWrFsRMONv2RemoveFlowStats;


typedef struct {

 INT4       *i4Index;
 UINT1      *au1Mac;
 tVlanId    u4VlanIndex;
 UINT1      au1pad[2];
} tRmonv2NpWrFsRMONv2GetPortID;

typedef struct Rmonv2NpModInfo {
union {
    tRmonv2NpWrFsRMONv2AddFlowStats  sFsRMONv2AddFlowStats;
    tRmonv2NpWrFsRMONv2CollectStats  sFsRMONv2CollectStats;
    tRmonv2NpWrFsRMONv2DisableProbe  sFsRMONv2DisableProbe;
    tRmonv2NpWrFsRMONv2DisableProtocol  sFsRMONv2DisableProtocol;
    tRmonv2NpWrFsRMONv2EnableProbe  sFsRMONv2EnableProbe;
    tRmonv2NpWrFsRMONv2EnableProtocol  sFsRMONv2EnableProtocol;
    tRmonv2NpWrFsRMONv2HwSetStatus  sFsRMONv2HwSetStatus;
    tRmonv2NpWrFsRMONv2RemoveFlowStats  sFsRMONv2RemoveFlowStats;
    tRmonv2NpWrFsRMONv2GetPortID  sFsRMONv2GetPortID;
    }unOpCode;

#define  Rmonv2NpFsRMONv2AddFlowStats  unOpCode.sFsRMONv2AddFlowStats;
#define  Rmonv2NpFsRMONv2CollectStats  unOpCode.sFsRMONv2CollectStats;
#define  Rmonv2NpFsRMONv2DisableProbe  unOpCode.sFsRMONv2DisableProbe;
#define  Rmonv2NpFsRMONv2DisableProtocol  unOpCode.sFsRMONv2DisableProtocol;
#define  Rmonv2NpFsRMONv2EnableProbe  unOpCode.sFsRMONv2EnableProbe;
#define  Rmonv2NpFsRMONv2EnableProtocol  unOpCode.sFsRMONv2EnableProtocol;
#define  Rmonv2NpFsRMONv2HwSetStatus  unOpCode.sFsRMONv2HwSetStatus;
#define  Rmonv2NpFsRMONv2RemoveFlowStats  unOpCode.sFsRMONv2RemoveFlowStats;
#define  Rmonv2NpFsRMONv2GetPortID  unOpCode.sFsRMONv2GetPortID;
} tRmonv2NpModInfo;


/* Function Prototype for Generic NP OpCode specific wrappers
 * defined in rmn2npapi.c */

UINT1 Rmonv2FsRMONv2AddFlowStats PROTO ((tPktHeader FlowStatsTuple));
UINT1 Rmonv2FsRMONv2CollectStats PROTO ((tPktHeader FlowStatsTuple, tRmon2Stats * pStatsCnt));
UINT1 Rmonv2FsRMONv2DisableProbe PROTO ((UINT4 u4InterfaceIdx, UINT2 u2VlanId, tPortList PortList));
UINT1 Rmonv2FsRMONv2DisableProtocol PROTO ((UINT4 u4ProtocolLclIdx));
UINT1 Rmonv2FsRMONv2EnableProbe PROTO ((UINT4 u4InterfaceIdx, UINT2 u2VlanId, tPortList PortList));
UINT1 Rmonv2FsRMONv2EnableProtocol PROTO ((tRmon2ProtocolIdfr Rmon2ProtoIdfr));
UINT1 Rmonv2FsRMONv2HwSetStatus PROTO ((UINT4 u4RMONv2Status));
UINT1 Rmonv2FsRMONv2HwGetStatus PROTO ((VOID));
UINT1 Rmonv2FsRMONv2NPDeInit PROTO ((VOID));
UINT1 Rmonv2FsRMONv2NPInit PROTO ((VOID));
UINT1 Rmonv2FsRMONv2RemoveFlowStats PROTO ((tPktHeader FlowStatsTuple));
UINT1
Rmonv2FsRMONv2GetPortID (UINT1 *au1Mac,UINT4  u4VlanIndex, INT4 *i4Index);



#endif /* __RMN2_NP_WR_H */

