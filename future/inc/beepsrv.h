#ifndef _BEEP_SRV_H__
#define _BEEP_SRV_H__

INT4 BeepSrvLock PROTO((VOID));
INT4 BeepSrvUnLock PROTO((VOID));


VOID BpSrvTaskMain PROTO((INT1 *));
INT4  BeepServerRegister PROTO((UINT2 u2RegnId,
              INT4 (*SendToApplication) (UINT1 *pu1Msg)));
INT4 BeepServerDeRegister PROTO((UINT2 u2RegnId));
VOID BeepSrvShutDown PROTO((VOID));


typedef struct BpSrvCltMsg
{
        INT4    i4SockId;
}tBeepSrvCltMsg;


#define  BPSRV_SYSLOG_ID 1
#define  BPSRV_DHCP_ID 2



#endif
