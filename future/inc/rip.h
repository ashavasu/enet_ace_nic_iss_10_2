/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rip.h,v 1.22 2013/06/19 13:29:31 siva Exp $ 
 *
 * Description: FutureRIP interfaces.
 *
 *******************************************************************/
#ifndef _RIP_H
#define _RIP_H

/********************************************************************
 * Function Prototypes
 *******************************************************************/
#define RIP_DEFAULT_CXT                 0
#define RIP_INVALID_CXT_ID              0xFFFFFFFF
#define RIP_INVALID_IFINDEX             0xFFFFFFFF

           /********************************************************
           *            Extern Declaration Variables               *
           ********************************************************/
#define   RIP_DEFAULT_METRIC           1
#define   RIP_NO_DEFAULT_ROUTE_METRIC  0


           /********************************************************
           * The following definitions are related to the control  *
           * of RIP operation at global level                      *
           ********************************************************/
#define   RIP_ADMIN_ENABLE     1
#define   RIP_ADMIN_DISABLE    2
#define   RIP_ADMIN_PASSIVE    3
#define   RIP_ADMIN_RESET      4

#define  RIP_AUTHKEY_STATUS_VALID       1
#define  RIP_AUTHKEY_STATUS_DELETE      2

INT4   RipTaskInit                      ARG_LIST ((VOID));
INT4   RipGetMaxRoutes                  ARG_LIST ((VOID));
INT4   RegisterStdRIPwithFutureSNMP     ARG_LIST ((VOID));
INT4   RegisterFSRIPwithFutureSNMP      ARG_LIST ((VOID));

PUBLIC INT4 RipLock (VOID);
PUBLIC INT4 RipUnLock (VOID);
PUBLIC INT4 RipSetContext(UINT4);
PUBLIC INT4 RipReleaseContext(VOID);
PUBLIC VOID RipResetContext(VOID);
VOID RIPTaskMain ARG_LIST ((INT1 *));
VOID RipDelMemInit (VOID);
INT4 RipProtoInit (VOID);

#endif /* _RIP_H */
