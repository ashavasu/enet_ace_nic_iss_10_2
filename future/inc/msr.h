/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: msr.h,v 1.114 2017/12/28 10:40:15 siva Exp $
 *
 * Description:This file contains the exported definitions and  
 *             macros of MSR module
 *
 *******************************************************************/
#ifndef _MSR_H_
#define _MSR_H_
#include "iss.h"
#include "utilipvx.h"

#define MSR_SUCCESS                   SUCCESS
#define MSR_FAILURE                   FAILURE
                                       
#define MSR_TRUE                      0
#define MSR_FALSE                     1

#define SET_FLAG                      2
#define UNSET_FLAG                    1
#define ISS_CONFIG_FILE2              ISS_CONFIG_CUST_FILE2


/* MSR Task related constants */
#define MSR_TASK_NAME                  "MSR"
#define MSR_TASK_PRIORITY             90
#define MSR_TASK_STACK_SIZE           OSIX_DEFAULT_STACK_SIZE
#define MSR_TASK_MODE                  OSIX_DEFAULT_TASK_MODE
#define MIB_SAVE_EVENT                0x01
#define MIB_RESTORE_EVENT             0x02
#define MIB_SAVE_TO_REMOTE_EVENT      0x04
#define MIB_RESTORE_FROM_REMOTE_EVENT 0x08
#define ISS_IMAGE_DOWNLOAD_EVENT      0x10
#define ISS_UPLOAD_LOG_EVENT          0x20
#define ISS_RESTART_EVENT             0x40
#define ISS_UPLOAD_FILE_EVENT         0x80
#define ISS_DWLOAD_FILE_EVENT         0x10000000
#define MSR_RM_GO_ACTIVE              0x100
#define MSR_RM_GO_STANDBY             0x200
#define MSR_RM_MESSAGE                0x400
#define CFA_BOOT_COMPLETE_EVENT       0x800
#define MSR_RESTORE_COMPLETE_EVENT    0x1000
#define MIB_ERASE_EVENT               0x2000
#define MSR_RM_FILE_TRANSFER_COMPLETE 0x4000
#define MSR_SYS_DEF_IFUP_TMR_EXP_EVT  0x8000
#define MSR_UPDATE_EVENT              0x10000
#define MSR_SYNCUP_EVENT              0x20000
#define HTTP_MULTI_DATA_EVENT         0x40000
#define MSR_RM_NEW_CONNECTION         0x100000 /* Event to indicate the active
                                                * node regarding a new
                                                * connection request
                                                */
#define MSR_RM_PACKET_ARRIVAL         0x200000 /* Event to indicate the arrival
                                                * of RM packet
                                                */
#define MSR_RM_SUB_BULK_UPDATE        0x400000 /* Event to send the next static
                                                * bulk update
                                                */
#define MSR_RM_SEND_REM_UPDATE        0x800000 /* Event to send the remaining
                                                * bulk update resulted from the
                                                * unsuccessful write operation
                                                */
#define MSR_RM_TMR_EVENT              0x1000000/* Rm Timer expiry event */
#define AUDIT_EVENT                   0x2000000
#define SYSLOG_FILEWRITE_EVENT        0x4000000

#define MSR_RM_STANDBY_DOWN_EVT       0x8000000 /* Msr Standby down event to
                                                   break the static bulk update
                                                   loop */
#define MSR_REMOTE_RESTORE_EVENT      0x20000000

#define MSR_INFORM_HTTP_EVENT         0x2000 /* To initimate the completion
                                                of MSR restore to
                                                webnm module */
#define ISS_LOAD_VERSION_RESTART_EVENT 0x40000000 /* Event to restart the ISS
                                                     in ISSU mode */

#define MIB_SAVE_GEN_MASK             (MIB_SAVE_EVENT | MIB_RESTORE_EVENT | \
                                       MIB_SAVE_TO_REMOTE_EVENT | \
                                       MIB_RESTORE_FROM_REMOTE_EVENT | \
                                       MIB_ERASE_EVENT | \
                                       ISS_IMAGE_DOWNLOAD_EVENT | \
                                       ISS_RESTART_EVENT | \
                                       ISS_UPLOAD_LOG_EVENT| \
                                       MSR_RM_GO_ACTIVE | \
                                       MSR_RM_GO_STANDBY | \
                                       MSR_RM_MESSAGE | \
                                       MSR_SYS_DEF_IFUP_TMR_EXP_EVT | \
                                       MSR_RM_FILE_TRANSFER_COMPLETE | \
                                       CFA_BOOT_COMPLETE_EVENT | \
                                       HTTP_MULTI_DATA_EVENT | \
                                       MSR_UPDATE_EVENT | \
                                       MSR_SYNCUP_EVENT | \
                                       MSR_RM_NEW_CONNECTION | \
                                       MSR_RM_PACKET_ARRIVAL | \
                                       MSR_RM_SUB_BULK_UPDATE | \
                                       MSR_RM_SEND_REM_UPDATE | \
                                       MSR_RM_TMR_EVENT | \
                                       AUDIT_EVENT| \
                                       ISS_UPLOAD_FILE_EVENT| \
                                       ISS_DWLOAD_FILE_EVENT | \
                                       MSR_RM_STANDBY_DOWN_EVT | \
                                       MSR_REMOTE_RESTORE_EVENT | \
                                       ISS_LOAD_VERSION_RESTART_EVENT )

#define MIB_SAVE_DUMMY_EVT            (0x01000000)
#define MIB_SAVE_DUMMY_TOUT           (50)
#define MSR_AC_TASK_EVT               (0x08000000)
#define MSR_IP_LINUKP_PROTO_NUM        0x0
#define MSR_RESTORE_TIMEOUT            20   /* In Sec */
#define MSR_REMOTE_RESTORE_TIMEOUT     22   /* RSTP max age time + RST forward delay In Sec */

#define CLI_MSR_STATUS_EVT             0x04
#define CLI_MSR_BUSY_EVT               0x08
#define CLI_MSR_STATUS_MASK            (CLI_MSR_STATUS_EVT | CLI_MSR_BUSY_EVT)
#define CLI_DUMMY_EVT                  0x03

/* The following constant is used for allocating the memory 
 * required for saving the MIB 
 */
#define MEM_REQUIRED                  1000
#define MSR_MAX_APPEND_OBJECTS        10
#define FLASH_START_ADDRESS           0x41e0000
#define FLASH_SIZE                    0x10000

/* Constants for the Mib Restore Status */
#define  MIB_RESTORE_IN_PROGRESS      1
#define  LAST_MIB_RESTORE_SUCCESSFUL  2
#define  LAST_MIB_RESTORE_FAILED      3
#define  MIB_RESTORE_NOT_INITIATED    4

#define  MIB_RESTORE_INVALID_VALUE    0

#define CLI_SAVE_RESTORE_SHOW_CMD "show running-config > "
/* Constants for the Mib Save Status */
#define  MIB_SAVE_IN_PROGRESS         1
#define  LAST_MIB_SAVE_SUCCESSFUL     2
#define  LAST_MIB_SAVE_FAILED         3
#define  MIB_SAVE_NOT_INITIATED       4

/* System Files */
#define MSR_SYS_EXTN_STRG_PATH  ISS_CUST_SYS_EXTN_STRG_PATH
#define ISS_CONFIG_FILE          FLASH "iss.conf"
#ifdef LION
#define ISS_IMAGE_PATH         "/bin/" 
#define ISS_IMAGE_FILE         "vmlinux"
#define ISS_COPY_TO_FLASH(i4Status) \
        i4Status = system ("/bin/fw_update.sh /bin/vmlinux");\
        if (i4Status != 0)\
        {\
            i4Status = ISS_FAILURE;\
        }\
        else\
        {\
        i4Status = ISS_SUCCESS;\
        }


#else
#define ISS_COPY_TO_FLASH(i4Status)\
        i4Status = ISS_SUCCESS;
#ifdef OS_VXWORKS
#define ISS_IMAGE_PATH           FLASH 
#define ISS_IMAGE_FILE           "iss.Z"
#else
#define ISS_IMAGE_PATH           ISS_CUST_IMAGE_PATH
#define ISS_IMAGE_FILE           ISS_CUST_IMAGE_FILE
#endif
#endif

#define ISS_CONFIG_FILE_NAME_LEN      128 
#define ISS_CLR_CONFIG_FILE_NAME_LEN  15
#define ISS_DIR_SIZE_CMD_LEN (ISS_CONFIG_FILE_NAME_LEN  + \
                              ISS_CONFIG_FILE_NAME_LEN)
#define ISS_RM_DIR_CMD_LEN (ISS_CONFIG_FILE_NAME_LEN + 20)
#define ISS_CONFIG_USER_NAME_LEN      20
#define ISS_AUDIT_FILE_NAME_LENGTH    (ISS_CONFIG_FILE_NAME_LEN + 1)

#define AUDIT_UID_STRING_LEN      10

#define AUDIT_FILE_NAME_EXT       "auditlog.txt"

/* Maximum log message length is the maximum amongst the messages 
 * that is written in the log file for CLI/SNMP/SYSLOG.
 * As of now, SNMP message can be the maximum. It needs to be revised
 * if the limit for the log message length in other cases changes. 
 * The value 6 is added for the spaces added while constructing message*/
#define ISS_MAX_LOG_MSG_LENGTH  (AUDIT_UID_STRING_LEN + AUDIT_LEN + \
                                 DATA_LENGTH + OID_LENGTH + \
                                 AUDIT_IP_LEN + MAX_AUDIT_TIME_VAL + 6)
         

#define ISS_CONFIG_PASSWD_LEN         20
#define OID_LENGTH                    265
#define DATA_LENGTH                   1024
#define MAX_AUDIT_USER_NAME_LEN       30
#define AUDIT_CMD_LEN                 72
#define MAX_AUDIT_TIME_VAL            30
#define AUDIT_LEN                     10
#define AUDIT_ZERO                    0
#define ISS_FILE_VERSION_OID_BEFORE_ENT_ID "1.3.6.1.4.1."
#define ISS_FILE_VERSION_OID_AFTER_ENT_ID ".81.1.39.0"
#define ISS_RESTORE_FILE_VERSION_OID  "1.3.6.1.4.1.2076.81.1.39.0"
#define ISS_RESTORE_FILE_FORMAT_OID   "1.3.6.1.4.1.2076.81.1.76.0"

/* Restore File Version has modified after adding
 * File Format OID. */

#define ISS_RESTORE_FILE_VERSION      "1.32"

/* The below macro is used To indicate the iss.conf file version
 * before and after adding File Format OID */

#define ISS_RES_FILE_VERSION_FORMAT   "1.31"


/*
 * To support older versions of iss.conf files values are mentioned as below
 *            ISS 5.1.1 value should be 1.28
 *            ISS 5.2.0 value should be 1.30
 *            ISS 6.0.0 value shoule be 1.31
 *            ISS 6.1.0 value shoule be 1.31
 */

#define ISS_OLD_RESTORE_FILE_VERSION  "1.31"
 /* Restore File Format Version Value. */
#define ISS_RESTORE_FILE_FORMAT      "1.1"
/*SNMP update data structure added */
#define MSR_RBNODE_OFFSET(StructType,rbnode)  FSAP_OFFSETOF(StructType,rbnode)
#define MSR_FAILURE_OID_LEN       10
#define MSR_SNMPQ_NAME   "MSRSNMPQ"
#define MSR_Q_AUDIT               "CLI_SNMP_AUDIT_QUEUE"
#define MAX_FILE_SIZE             (1024 * 1024)
#define MAX_TRACE_LEN             256
#define AUDIT_IP_LEN              25
#define AUDIT_LOG_SIZE            256
#define MODE_CONSOLE              1
#define MODE_TELNET               2
#define MODE_SSH                  3
#define LOG_IN                    1
#define LOG_OUT                   2
#define FORCED_LOG_OUT            3
#define AUDIT_SNMP_MSG                1
#define AUDIT_CLI_MSG                 2
#define AUDIT_SYSLOG_MSG              3
#define AUDIT_EXT_CRYPTO_MSG          4

#define AUDIT_TRAP_TIME_STR_LEN        24
#define AUDIT_TRAP_STR_LEN             24
#define AUDIT_TRAP_EVENT                1

#define AUDIT_CRITICAL_LEVEL      2
#define AUDIT_NORMAL_LEVEL        5
#define AUDIT_INFO_LEVEL          6

enum
{
    AUDIT_TRAP_OPEN_FAILED = 1,
    AUDIT_TRAP_WRITE_FAILED,       
    AUDIT_TRAP_FILE_SIZE_EXCEEDED,
    AUDIT_TRAP_LOG_SIZE_THRESHOLD_HIT
};
/* enum for the failure reason of the restoration failure*/
enum
{
 MIB_RES_INVALID_FAILURE = 1,
 MIB_RES_MEMORY_FAILURE,
 MIB_RES_RBTREE_POPULATION_FAILURE,
 MIB_RES_FLAG_UNSET,
 MIB_RES_INCOMPATABLE_FILE,
 MIB_RES_RBTREE_UPDATION_FAILURE,
 MIB_RES_RBTREE_INSERTION_FAILURE,
 MIB_RES_CHKSUM_FAILURE,
 MIB_RES_FILE_OPEN_FAILURE,
 MIB_RES_FILE_VERSION_FAILURE,
 MIB_RES_FILE_COPY_FAILURE,
 MIB_RES_CUST_VALIDATION_FAILURE
};
/* enum for the priority storage of save/restore*/
typedef enum {
    FLASH_STORAGE = 1,
    EXTERN_STORAGE
}tMsrSavResOption;

typedef  struct cmdInfo
{
    INT1        ai1UserName[MAX_AUDIT_USER_NAME_LEN];
    INT1 ai1Pad[2];
    UINT1       au1TempCmd[AUDIT_CMD_LEN];
    UINT4       u4CmdStatus;
    UINT4       u4CliMode;
    UINT4       u4ClientIpAddr;

}tCmdInfo;
typedef struct snmpInfo
{
    UINT4                 au4_OidList[OID_LENGTH];
    UINT1                 *pu1ObjVal;
    UINT4                 u4MngrIpAddr;
    INT1                  ai1SnmpUser[MAX_AUDIT_USER_NAME_LEN];
    INT1                  ai1Status[AUDIT_LEN];
}tSnmpInfo;

typedef struct AuditTrapMsg
{
    UINT1       au1IssLogFileName[ISS_AUDIT_FILE_NAME_LENGTH];      /* AuditLog File*/
    UINT1 au1Pad[3];
    CHR1        ac1DateTime[AUDIT_TRAP_TIME_STR_LEN];             /* Event Time */
    UINT4       u4Event;                                          /* Event Type */
}tAuditTrapMsg;

typedef struct auditInfo
{
    
    tCmdInfo    TempCmd;
    tSnmpInfo   TempSnmp;
    UINT1       au1Time[MAX_AUDIT_TIME_VAL];
    INT2        i2Flag;
    UINT1       au1IssLogFileName[ISS_CONFIG_FILE_NAME_LEN];
    UINT1       au1SyslogBuf[1024];
    UINT4       u4SeverityLevel;
    UINT4       u4ModuleId;
}tAuditInfo;
/* convert a hex ip address to string */
#define IPADDR_HEX2STR(u4IpAddr, ai1IpAddr)         \
{                                                   \
            UINT4 u4Mask=0xFF000000;                        \
            UINT4 u4Num;                                    \
            UINT4 ai4Addr[4];                               \
            ai4Addr[0] = u4IpAddr;                          \
            for (u4Num=0; u4Num < 4; u4Num++)               \
            {                                               \
                (ai4Addr)[u4Num] = (((u4IpAddr & u4Mask) >> \
                            (24 - (u4Num*8))) & 0x000000FF);\
                u4Mask >>= 8;                               \
            }                                               \
            SPRINTF ((CHR1 *)ai1IpAddr, "%d.%d.%d.%d", ai4Addr[0],\
                        ai4Addr[1], ai4Addr[2], ai4Addr[3]);\
}

#ifdef MSR_WANTED
#define MSR_NOTIFY_PROTOCOL_SHUT(ppu1Oid, u2OidNo, i4ContextId) \
MsrNotifyProtocolShutdown (ppu1Oid, u2OidNo, i4ContextId) 
#else
#define MSR_NOTIFY_PROTOCOL_SHUT(ppu1Oid, u2OidNo, i4ContextId) \
{ \
    UNUSED_PARAM (ppu1Oid); \
    UNUSED_PARAM (u2OidNo); \
    UNUSED_PARAM (i4ContextId); \
}
#endif



/* This Data structure is used to store TFTP server related information 
 * for saving the configuration to remote
 */
typedef struct _MsrPacket {

    tIPvXAddr ServerAddr;  /* IP Address of the TFTP/SFTP server 
                                  * residing on the remote host
                                  */
    UINT1 au1FileName[ISS_CONFIG_FILE_NAME_LEN + 1]; 
                                 /* The name of the file to which the 
                                  * configuration is to be stored 
                                  */
    UINT1  au1UserName[ISS_CONFIG_USER_NAME_LEN +1];
    
    UINT1  au1Password[ISS_CONFIG_PASSWD_LEN +1];
    UINT1  au1Pad[1];
    UINT4  u4TransMode;
}tMsrPacket;
/*structure for RB tree*/
typedef struct MSRRbtree
{
    tRBNodeEmbd                     RBNode;
    UINT1                       *pData;
    UINT4                     u4pDataSize;
    UINT4                  MSRArrayIndex;
    UINT4                  MSRRowStatusType;
       tSNMP_OID_TYPE                      *pOidValue;
    UINT1               u1IsDataToBeSaved; /* Flag to indicate whether the
                                            * needs to be set to non-volatile
                                            * storage.
                                            * MSR_TRUE  - Needs to be stored
                                            * MSR_FALSE - Should not be stored
                                            */
    UINT1               au1Reserved[3];
}tMSRRbtree;


/* Globals */
#ifdef _MSR_C_
UINT4 gu4MsrAuditLogSize = 0;
#else
PUBLIC UINT4 gu4MsrAuditLogSize;
#endif

extern tOsixTaskId  MsrId;
extern tOsixSemId   MsrSemId;
extern tOsixSemId   TftSemId;
extern tOsixQId     MsrHttpQId;
#ifdef RM_WANTED
extern tOsixQId     MsrQId;
extern tOsixQId     MsrsnmpQId;
#endif

/* externs */
extern UINT1 *gpu1StrFlash;          /* Points to the character array to be
                                      * stored in / retrieved from flash */

extern UINT1 *gpu1StrCurrent;        /* Current position in the StrFlash array */

extern UINT4  gu4Length;             /* The length(in bytes) of gStrFlash array */

extern UINT1 *gpu1StrTempCurr;       /* A temporary pointer to current location */


/* PUBLIC functions */
INT4  FlashRead PROTO ((UINT1 **, UINT4 *, UINT4));

INT4  FlashWrite PROTO ((UINT1 *, UINT4 , UINT4,CONST CHR1 *));

INT4  FlashFileExists PROTO ((CONST CHR1 * FileName));

INT4  FlashRemoveFile PROTO ((CONST CHR1 *));

INT4
FlashReadCksum (UINT2 *);
    
INT4  IssRestartNotification PROTO ((void));

INT4
IssLoadVersionRestartNotification PROTO ((VOID));

VOID  IssMsrInit PROTO ((VOID));

#ifdef RM_WANTED
INT4 MsrRmInit PROTO ((VOID));

VOID MsrRmDeInit PROTO ((VOID));
#endif

UINT1 MsrRMGetNodeStatus PROTO ((VOID));

VOID MibSaveTaskMain PROTO ((INT1 *));
VOID MSRMibRestorefromRBtree(VOID);
tOsixQId *MsrQueueId (VOID);

INT4 IssTftpImageDownload PROTO((UINT1 *));
INT4 IssImageDownload PROTO((UINT1 *));
INT4 IssUploadLogs PROTO((UINT1 *));
VOID IssUploadFile PROTO((UINT1 *));
VOID IssDownloadFile PROTO((UINT1 *));
INT4 MsrSendEventToMsrTask (UINT4 u4Event);

INT1 MsrIsMibRestoreInProgress PROTO ((VOID));
INT1 MsrClearConfig PROTO ((VOID));
/*prototype corresponding to snmp udate functions*/
VOID MSRDeleteAllRBNodesinRBTree(VOID);
 VOID MSRFreeSnmpDataRcvfromQ PROTO ((tMSRUpdatedata *));
UINT4 MSRCompareRBNodes (tMSRRbtree *RBNode1, tMSRRbtree *RBNode2);
INT4 MSRSearchNodeinRBTree(tSNMP_OID_TYPE *pOid,
        UINT4  MSRArrayIndex,
        UINT4  MSRRowStatusType,
        tMSRRbtree **psearchRes);
INT4 MSRCompareSnmpMultiDataValues(tSNMP_MULTI_DATA_TYPE *, tSNMP_MULTI_DATA_TYPE *);
VOID MSRDeleteNodefromRBTree(tSNMP_OID_TYPE *pOid,UINT4  MSRArrayIndex,
        UINT4  MSRRowStatusType);

INT4 MSRGetArryIndexWithOID( UINT1 *pOidStr,UINT4 *pu4Index,UINT1  type,UINT4 prevIndex);
    
INT4 getMSRindexwithRowstatusOID( UINT1 *pOid,UINT4 *pu4Index,UINT1  type,UINT4 prevIndex);

INT4 MSRInsertRBNodeinRBTree(tSNMP_OID_TYPE *pOid,
                             tSNMP_MULTI_DATA_TYPE  *pData, 
                             UINT4  MSRArrayIndex,UINT4  MSRRowStatusType,
                             UINT1 u1IsDataToBeSaved);
INT4 MSRUpdateISSConfFile(UINT4 u4FlashDataType, CONST CHR1 * pu1FlashFileName);
VOID MSRFreeRBTreeNode(tMSRRbtree *ptobefreed,UINT4 u4arg);
VOID MSRSendUpdateFailureNotification(tMSRUpdatedata *pQData);
INT4 MSRModifyRBNodeinRBTree(tSNMP_MULTI_DATA_TYPE  *pModData,tMSRRbtree *pRBNode);

VOID MSRUpdateMemFree(tSnmpIndex *pMultiIndex, tRetVal *pMultiData, UINT1 *pOid);

INT1 MSRNotifyConfChg (tSnmpIndex *pMultiIndex, BOOL1   is_row_status_present,
                       tRetVal    *pMultiData, UINT1 *pOid);
VOID  MsrSetPhysicalPortAdminStatus (VOID);
VOID  MsrSetHigPortAdminStatus (VOID);
VOID  MsrUpdateStaticConfig (UINT4 u4PrevSwitchId, UINT4 u4CurrSwitchId);
VOID  MsrInitiateConfigSave(VOID);
INT1 MsrNotifyPortDeletion PROTO ((UINT4));
INT4  MsrVerifyRestFileChecksum PROTO ((INT4 i4ConfFd));
tIssBool  MsrGetSaveStatus PROTO ((VOID));
tIssBool  MsrIsSaveComplete PROTO ((VOID));
tIssBool MsrGetIncrementalSaveStatus PROTO ((VOID));
tIssBool MsrGetRestorationStatus PROTO ((VOID));
tIssBool MsrGetClearConfigStatus PROTO ((VOID));
INT1 MSRSendAuditInfo PROTO((tAuditInfo *));

VOID MsrAuditSendLogInfo(UINT1 *, UINT4);
/* For giving access to other modules, this trap sending function
 * prototype and corresponding variables are exported here
 * This is done because of a custom requirement */
VOID AuditTrapSendNotifications (
                   tAuditTrapMsg * pNotifyInfo);
#ifdef SNMP_2_WANTED
extern CONST CHR1         *gau1TrapEvent[];
#endif
typedef struct _MsrRegParams
{
    /* MSR  notification  Call back function pointer */
 INT4 (*pMsrCompleteStatusCallBack) (INT4 i4MsrCompleStatus);
    /* Registration flag - Will be set after registration */
 UINT1 bFlag;
 UINT1 au1padding[3];  /* Padding for the structure */

 }tMsrRegParams;
typedef enum {
    MSR_SECURE_PROCESS = 1,
 MSR_CONF_FILE_VALIDATE,
    MSR_MAX_CALLBACK_EVENTS
}eMsrCallBackEvents;

INT4 MsrUtilCallBackRegister PROTO((UINT4 u4Event, tFsCbInfo * pFsCbInfo));

VOID MsrConvertDataToString PROTO((tSNMP_MULTI_DATA_TYPE * pData,
        UINT1 * pu1String, UINT2 u2Type));

tSNMP_MULTI_DATA_TYPE *MsrConvertStringToData PROTO ((UINT1 * pu1String,
            UINT2 u2Type, UINT4 u4Len));

tSNMP_OID_TYPE * MsrAllocOid PROTO ((VOID));
tSNMP_OID_TYPE * MsrClrCfgAllocOid PROTO ((VOID));

VOID MsrDeAllocOid PROTO ((tSNMP_OID_TYPE **ppOid));
INT4 MsrUtilIsClrCfgSupported PROTO ((VOID));

#endif /* _MSR_H */
