/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: poe.h,v 1.9 2014/04/11 09:17:19 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              by  external modules.
 *
 *******************************************************************/
#ifndef  _POE_H
#define  _POE_H

#include "fssnmp.h"

#define POE_LOCK() PoeLock ()
#define POE_UNLOCK() PoeUnLock ()

/* Defining linked list for PoE */
typedef tTMO_SLL            tPoeSll;
typedef tTMO_SLL_NODE       tPoeSllNode;

/* Defining enumerated data types */

typedef enum {
   POE_LOCK_FALSE = 0,
   POE_LOCK_TRUE = 1
}tPoeBoolean;

typedef enum {
   POE_TRUE = 1,
   POE_FALSE = 2
}tPoeAdminStatus;

typedef enum {
   CRITICAL  = 1,
   HIGH = 2,
   LOW  = 3
}tPoePortPriority;

typedef enum {
   POE_DISABLED  = 1,
   POE_SEARCHING = 2,
   POE_DELIVERING_POWER  = 3,
   POE_FAULT = 4,
   POE_TEST = 5,
   POE_OTHERFAULT = 6
}tPoePortDetectionStatus;

typedef enum {
   CLASS_0  = 1,
   CLASS_1  = 2,
   CLASS_2  = 3,
   CLASS_3  = 4,
   CLASS_4  = 5
}tPoePortclassification;

/*For PoE PSE Port Entry Table */
typedef struct PoePsePortEntry {
   tPoeSllNode             NextPortEntry;   /* Next entry in the list */
   UINT4                   u4PseGroupIndex;
   tPoeAdminStatus         PsePortAdminEnable;
   tPoePortDetectionStatus PsePortDetectionStatus; 
   tPoePortPriority        PsePortPowerPriority;
   tPoePortclassification  PsePortPowerClassification;
   UINT2                   u2PsePortIndex;
   UINT2                   u2Reserved;
} tPoePsePortEntry;


/* Message types to process Q event*/
#define    POE_MAC_LEARNING            41
#define    POE_MAC_AGING               42

INT4 SendMsgToQAndEventToPoE (UINT2 u2Port, tMacAddr macAddr, UINT4 u4Event);
VOID PoeTaskMain (INT1 *pi1Param);
INT4 PoeCreatePort (UINT2 u2PortIndex);
INT1 nmhGetPethNotificationControlEnable ARG_LIST((INT4 ,INT4 *));
INT1 nmhSetPethNotificationControlEnable ARG_LIST((INT4  ,INT4 ));
INT1 nmhTestv2PethNotificationControlEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
INT4 PethNotificationControlEnableSet(tSnmpIndex *, tRetVal *);
INT4 PoeGetModuleStatus (INT4 *pi4RetValFsPoeGlobalAdminStatus);
#endif /* _POE_H */
