/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fssyslog.h,v 1.22 2017/12/28 10:40:15 siva Exp $
 *
 * Description:This file contains the exported definitions and  
 *             macros of SYSLOG module
 *
 *******************************************************************/
#ifndef _ISSLOG_H
#define _ISSLOG_H
#include "utilipvx.h"
#include "cust.h"

#define SYSLOG_ENABLE              1  /* enable syslog feature */
#define SYSLOG_DISABLE             2  /* disable syslog feature */
#define SYSLOG_MAX_HOSTNAME        80

/* Definition of System Log level (0-7) with 0 being the higest level */

#define SYSLOG_DEBUG_LEVEL         7   /* Used for logging debug messages */

#define SYSLOG_INFO_LEVEL          6   /* Used for logging informational 
                                        *  messages
                                        */

#define SYSLOG_NOTICE_LEVEL        5   /* Used for logging messages that 
                                        *  require attention but are not errors
                                        */   

#define SYSLOG_WARN_LEVEL          4   /* Used for logging warning messages */

#define SYSLOG_ERROR_LEVEL         3   /* Used for error messages */

#define SYSLOG_CRITICAL_LEVEL      2   /* Used for logging critical errors */

#define SYSLOG_ALERT_LEVEL         1   /* Used for logging messages that 
                                        * require immediate attention.
                                        */

#define SYSLOG_EMERG_LEVEL         0   /* Used for logging messages that are
                                        * equivalent to a panic condition.
                                        */

#define SYSLOG_INVAL_LEVEL       0xff /*Invalid level which should not be logged */

#define DEF_SYSLOG_FACILITY       128 

/* Facility Levels */

#define SYSLG_FACILITY_LOCAL0      DEF_SYSLOG_FACILITY
#define SYSLG_FACILITY_LOCAL1      136 
#define SYSLG_FACILITY_LOCAL2      144 
#define SYSLG_FACILITY_LOCAL3      152 
#define SYSLG_FACILITY_LOCAL4      160 
#define SYSLG_FACILITY_LOCAL5      168 
#define SYSLG_FACILITY_LOCAL6      176 
#define SYSLG_FACILITY_LOCAL7      184 


/******** Macros for syslog APIs *******/

#ifdef SYSLOG_WANTED

/* Registration for syslog feature */
#define SYS_LOG_REGISTER(pu1Name, u4Level)  SysLogRegister(pu1Name, u4Level)

/* De-Registration for syslog feature */
#define SYS_LOG_DEREGISTER(u4ModId)         SysLogDeRegister(u4ModId)

/* For logging messages  */
#define SYS_LOG_MSG(args)                   SysLogMsg args

/* For logging cli commands */
#define SYS_LOG_CMD(args)                   SysLogCmd args

#define SYSLOG_DEF_USER_INDEX               0 

/* For logging trace messages */
#define SYS_LOG_TRC(pi1Name,ai1LogMsgBuf) \
    SysLogMessage (SYSLOG_DEBUG_LEVEL,(const INT1 *) pi1Name,ai1LogMsgBuf)

/* For logging trace messages with syslog levels*/
#define SYS_LOG_TRC1(u4SysLevel,pi1Name,ai1LogMsgBuf) \
    SysLogMessage (u4SysLevel,(const INT1 *) pi1Name,ai1LogMsgBuf)

#else /* SYSLOG_WANTED */
/* SYS_LOG_REGISTER will return value 1 when SYSLOG_WANTED is not defined, 
 * to avoid failure in various Module initialisation */
#define SYS_LOG_REGISTER(pu1Name, u4Level)     (1)
#define SYS_LOG_DEREGISTER(u4ModId)
#define SYS_LOG_MSG(args)
#define SYS_LOG_CMD(args)
#define SYS_LOG_TRC(pi1Name,ai1LogMsgBuf)
#define SYS_LOG_TRC1(u4SysLevel,pi1Name,ai1LogMsgBuf)

#endif /* SYSLOG_WANTED */

/* Used by various other protocols. Maximum string length. */
#define SYSLOG_MAX_STRING   1024

#define SYSLOG_APP_ID  1 

#define    BEEP_CLIENT_MODULE             1
#define    BEEP_SERVER_MODULE             2 
#define    TCP_CLIENT_MODULE              3
#define    TCP_SERVER_MODULE              4


#define    SYSLOG_CURRENT_ROLE_EVENT      1
#define    SYSLOG_CURRENT_PROFILE_EVENT   2
#define    SYSLOG_TRANS_MSG_EVENT         3

typedef struct _SyslogParams {
    union SyslogParams{
     
        struct RoleParam
        {
            UINT4  u4SysLogRole;     /* Current Syslog Role     */
        }Role;
        
        struct ProfileParam
        {
            UINT4  u4SysLogProfile;  /* Current Syslog Profile  */
        }Profile;
       
        struct ServerParam
        {
            UINT4      u4Port;           /* Destination port */
            tIPvXAddr  ServerInfo;
        }Server;

    }u;

#define SYSLOG_ROLE       u.Role
#define SYSLOG_PROFILE    u.Profile
#define SYSLOG_MSG        u.Server

} tSysLogTransMsg;


VOID SysLogSmtpMain    PROTO ((INT1 *));
INT4  SysLogInit       PROTO ((VOID));
INT4  SysLogRegister   PROTO ((CONST UINT1 *, UINT4));
INT4  SysLogDeRegister PROTO ((UINT4));
VOID  SysLogMsg        PROTO ((UINT4, UINT4 , const CHR1 *,...));
VOID  SysLogCmd        PROTO ((UINT4, UINT4 , const CHR1 *,...));
UINT4 SysLogValidateMailId PROTO ((UINT1 *, INT4));
VOID SysLogMessage     PROTO ((UINT4, const INT1*, CHR1*));

INT4
SysLogRegisterTransModule (UINT1 ,
                           VOID (*pTransFuncPtr) (UINT4, tSysLogTransMsg, UINT1*));


INT4 SysLogDeRegisterTransModule PROTO ((UINT1));

INT4 SysLogCallBackRegister PROTO ((UINT4 , tFsCbInfo *));

INT4 SyslogUtilCheckServerConfig PROTO ((UINT4 u4SeverityLevel));
INT4 SysLogGetMemPoolId (VOID);

#define SYSLOG_SUCCESS 0
#define SYSLOG_FAILURE -1

#endif /* _ISSLOG_H */

