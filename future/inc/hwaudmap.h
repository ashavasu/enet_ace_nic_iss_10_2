/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: hwaudmap.h,v 1.11 2014/06/03 13:12:51 siva Exp $
 *
 * Description: This file defines the prototypes of the functions 
 *              used in the NP syncup for high availability
 ****************************************************************************/

#ifndef _HWAUDMAP_H
#define _HWAUDMAP_H

#define FsMiPbbNpInitHw()  NpSyncFsMiPbbNpInitHw()
#define FsMiPbbNpDeInitHw()  NpSyncFsMiPbbNpDeInitHw()
#define FsMiPbbHwSetVipAttributes(u4ContextId, u4VipIfIndex, VipAttribute)  NpSyncFsMiPbbHwSetVipAttributes(u4ContextId, u4VipIfIndex, VipAttribute)
#define FsMiPbbHwDelVipAttributes(u4ContextId, u4VipIfIndex)  NpSyncFsMiPbbHwDelVipAttributes(u4ContextId, u4VipIfIndex)
#define FsMiPbbHwSetVipPipMap(u4ContextId, u4VipIfIndex, VipPipMap)  NpSyncFsMiPbbHwSetVipPipMap(u4ContextId, u4VipIfIndex, VipPipMap)
#define FsMiPbbHwDelVipPipMap(u4ContextId, u4VipIfIndex)  NpSyncFsMiPbbHwDelVipPipMap(u4ContextId, u4VipIfIndex)
#define FsMiPbbHwSetBackboneServiceInstEntry(u4ContextId, u4CbpIfIndex, u4BSid, BackboneServiceInstEntry)  NpSyncFsMiPbbHwSetBackboneServiceInstEntry(u4ContextId, u4CbpIfIndex, u4BSid, BackboneServiceInstEntry)
#define FsMiPbbHwDelBackboneServiceInstEntry(u4ContextId, u4CbpIfIndex, u4BSid)  NpSyncFsMiPbbHwDelBackboneServiceInstEntry(u4ContextId, u4CbpIfIndex, u4BSid)
#define FsMiBrgHwCreateControlPktFilter(u4ContextId, FilterEntry)  NpSyncFsMiBrgHwCreateControlPktFilter(u4ContextId, FilterEntry)
#define FsMiBrgHwDeleteControlPktFilter(u4ContextId, FilterEntry)  NpSyncFsMiBrgHwDeleteControlPktFilter(u4ContextId, FilterEntry)
#define FsMiPbbHwSetAllToOneBundlingService(u4ContextId, u4IfIndex, u4Isid)  NpSyncFsMiPbbHwSetAllToOneBundlingService(u4ContextId, u4IfIndex, u4Isid)
#define FsMiPbbHwDelAllToOneBundlingService(u4ContextId, u4IfIndex, u4Isid)  NpSyncFsMiPbbHwDelAllToOneBundlingService(u4ContextId, u4IfIndex, u4Isid)
#define FsCfaHwCreateInternalPort(u4IfIndex)  NpSyncFsCfaHwCreateInternalPort(u4IfIndex)
#define FsCfaHwDeleteInternalPort(u4IfIndex)  NpSyncFsCfaHwDeleteInternalPort(u4IfIndex)
#define FsCfaHwCreateILan(u4ILanIndex, ILanPortArray)  NpSyncFsCfaHwCreateILan(u4ILanIndex, ILanPortArray)
#define FsCfaHwDeleteILan(u4ILanIndex)  NpSyncFsCfaHwDeleteILan(u4ILanIndex)
#define FsCfaHwRemovePortFromILan(u4ILanIndex, u4PortIndex)  NpSyncFsCfaHwRemovePortFromILan(u4ILanIndex, u4PortIndex)
#define FsCfaHwAddPortToILan(u4ILanIndex, u4PortIndex)  NpSyncFsCfaHwAddPortToILan(u4ILanIndex, u4PortIndex)
#define FsMiPbbTeHwSetEspVid(u4ContextId, EspVlanId)  NpSyncFsMiPbbTeHwSetEspVid(u4ContextId, EspVlanId)
#define FsMiPbbTeHwResetEspVid(u4ContextId, EspVlanId)  NpSyncFsMiPbbTeHwResetEspVid(u4ContextId, EspVlanId)
#ifdef NPAPI_WANTED
#define QosxQoSHwInit(VOID)  NpSyncQoSHwInit(VOID)
#define QosxQoSHwMapClassToPolicy(pClsMapEntry, pPlyMapEntry, pInProActEntry, pOutProActEntry, pMeterEntry, u1Flag)  NpSyncQoSHwMapClassToPolicy(pClsMapEntry, pPlyMapEntry, pInProActEntry, pOutProActEntry, pMeterEntry, u1Flag)
#define QosxQoSHwUpdatePolicyMapForClass(pClsMapEntry, pPlyMapEntry, pInProActEntry, pOutProActEntry, pMeterEntry, u1Flag)  NpSyncQoSHwUpdatePolicyMapForClass(pClsMapEntry, pPlyMapEntry, pInProActEntry, pOutProActEntry, pMeterEntry, u1Flag)
#define QosxQoSHwUnmapClassFromPolicy(pClsMapEntry, pPlyMapEntry, pMeterEntry, u1Flag)  NpSyncQoSHwUnmapClassFromPolicy(pClsMapEntry, pPlyMapEntry, pMeterEntry, u1Flag)
#define QosxQoSHwDeleteClassMapEntry(pClsMapEntry)  NpSyncQoSHwDeleteClassMapEntry(pClsMapEntry)
#define QosxQoSHwMeterCreate(pMeterEntry)  NpSyncQoSHwMeterCreate(pMeterEntry)
#define QosxQoSHwMeterDelete(u4MeterId)  NpSyncQoSHwMeterDelete(u4MeterId)
#define QosxQoSHwSchedulerAdd(pSchedEntry)  NpSyncQoSHwSchedulerAdd(pSchedEntry)
#define QosxQoSHwSchedulerUpdateParams(pSchedEntry)  NpSyncQoSHwSchedulerUpdateParams(pSchedEntry)
#define QosxQoSHwSchedulerDelete(u4IfIndex, u4SchedId)  NpSyncQoSHwSchedulerDelete(u4IfIndex, u4SchedId)
#define QosxQoSHwQueueCreate(u4IfIndex, u4QId, pQEntry, pQTypeEntry, papRDCfgEntry, u2HL)  NpSyncQoSHwQueueCreate(u4IfIndex, u4QId, pQEntry, pQTypeEntry, papRDCfgEntry, u2HL)
#define QosxQoSHwQueueDelete(u4IfIndex, u4Id)  NpSyncQoSHwQueueDelete(u4IfIndex, u4Id)
#define QosxQoSHwMapClassToQueue(u4IfIndex, u4ClsOrPriType, u4ClsOrPri, u4QId, u1Flag)  NpSyncQoSHwMapClassToQueue(u4IfIndex, u4ClsOrPriType, u4ClsOrPri, u4QId, u1Flag)
#define QosxQoSHwSchedulerHierarchyMap(u4IfIndex, u4SchedId, u2Sweight, u1Spriority, u4NextSchedId, u4NextQId, u2HL, u1Flag)  NpSyncQoSHwSchedulerHierarchyMap(u4IfIndex, u4SchedId, u2Sweight, u1Spriority, u4NextSchedId, u4NextQId, u2HL, u1Flag)
#define QosxQoSHwSetDefUserPriority(u4Port, u4DefPriority)  NpSyncQoSHwSetDefUserPriority(u4Port, u4DefPriority)
#define QosxQoSHwMapClassToQueueId(pClassMapEntry, u4IfIndex, u4ClsOrPriType, u4ClsOrPri, u4QId, u1Flag)  NpSyncQoSHwMapClassToQueueId(pClassMapEntry, u4IfIndex, u4ClsOrPriType, u4ClsOrPri, u4QId, u1Flag)
#define QosxFsQosHwConfigPfc(pQosPfcHwEntry)  NpSyncFsQosHwConfigPfc(pQosPfcHwEntry)
#define IsssysIssHwUpdateL2Filter(pIssL2FilterEntry, i4Value)  NpSyncIssHwUpdateL2Filter(pIssL2FilterEntry, i4Value)
#define IsssysIssHwUpdateL3Filter(pIssL3FilterEntry, i4Value)  NpSyncIssHwUpdateL3Filter(pIssL3FilterEntry, i4Value)
#define IsssysIssHwSetRateLimitingValue(u4IfIndex, u1PacketType, i4RateLimitVal)  NpSyncIssHwSetRateLimitingValue(u4IfIndex, u1PacketType, i4RateLimitVal)
#define IsssysIssHwSetPortEgressPktRate(u4IfIndex, i4PktRate, i4BurstRate)  NpSyncIssHwSetPortEgressPktRate(u4IfIndex, i4PktRate, i4BurstRate)
#define IsssysIssHwConfigPortIsolationEntry(pIssHwUpdtPortIsolation)  NpSyncIssHwConfigPortIsolationEntry(pIssHwUpdtPortIsolation)
#endif
#define QosxQoSHwSetCpuRateLimit(i4CpuQueueId, u4MinRate, u4MaxRate)  NpSyncQoSHwSetCpuRateLimit(i4CpuQueueId, u4MinRate, u4MaxRate)

/* If Async NPAPI is enabled, FsErpsHwRingConfig will be defined
  as AsyncFsErpsHwRingConfig , which will conflict with the definition
  (ErpsSyncNpFsErpsHwRingConfig) that is given when L2RED_WANTED 
  switch is enabled
  So the following switch condition (ifndef NPSIM_ASYNC_WANTED) is 
  given to avoid multiple definitions.*/ 
#ifndef NPSIM_ASYNC_WANTED
#define FsErpsHwRingConfig(ErpsHwRingInfo)  ErpsSyncNpFsErpsHwRingConfig(ErpsHwRingInfo)
#endif
PUBLIC INT4
NpSyncFsMiPbbNpInitHw PROTO ((VOID));

PUBLIC VOID
NpSyncFsMiPbbNpDeInitHw PROTO ((VOID));

PUBLIC INT4
NpSyncFsMiPbbHwSetVipAttributes PROTO ((UINT4         u4ContextId,
                                UINT4         u4VipIfIndex,
                                tVipAttribute VipAttribute));

PUBLIC INT4
NpSyncFsMiPbbHwDelVipAttributes PROTO ((UINT4 u4ContextId, UINT4 u4VipIfIndex));

PUBLIC INT4
NpSyncFsMiPbbHwSetVipPipMap PROTO ((UINT4      u4ContextId,
                            UINT4      u4VipIfIndex,
                            tVipPipMap VipPipMap));

PUBLIC INT4
NpSyncFsMiPbbHwDelVipPipMap PROTO ((UINT4 u4ContextId, UINT4 u4VipIfIndex));

PUBLIC INT4
NpSyncFsMiPbbHwSetBackboneServiceInstEntry PROTO ((UINT4                     u4ContextId,
                                           UINT4                     u4CbpIfIndex,
                                           UINT4                     u4BSid,
                                           tBackboneServiceInstEntry BackboneServiceInstEntry));

PUBLIC INT4
NpSyncFsMiPbbHwDelBackboneServiceInstEntry PROTO ((UINT4 u4ContextId,
                                           UINT4 u4CbpIfIndex,
                                           UINT4 u4BSid));

PUBLIC INT4
NpSyncFsMiBrgHwCreateControlPktFilter PROTO ((UINT4 u4ContextId, tFilterEntry FilterEntry));

PUBLIC INT4
NpSyncFsMiBrgHwDeleteControlPktFilter PROTO ((UINT4 u4ContextId, tFilterEntry FilterEntry));

PUBLIC INT4
NpSyncFsMiPbbHwSetAllToOneBundlingService PROTO ((UINT4 u4ContextId,
                                          UINT4 u4IfIndex,
                                          UINT4 u4Isid));

PUBLIC INT4
NpSyncFsMiPbbHwDelAllToOneBundlingService PROTO ((UINT4 u4ContextId,
                                          UINT4 u4IfIndex,
                                          UINT4 u4Isid));

PUBLIC INT4
NpSyncFsCfaHwCreateInternalPort PROTO ((UINT4 u4IfIndex));

PUBLIC INT4
NpSyncFsCfaHwDeleteInternalPort PROTO ((UINT4 u4IfIndex));

PUBLIC INT4
NpSyncFsCfaHwCreateILan PROTO ((UINT4 u4ILanIndex, tHwPortArray ILanPortArray));

PUBLIC INT4
NpSyncFsCfaHwDeleteILan PROTO ((UINT4 u4ILanIndex));

PUBLIC INT4
NpSyncFsCfaHwRemovePortFromILan PROTO ((UINT4 u4ILanIndex, UINT4 u4PortIndex));

PUBLIC INT4
NpSyncFsCfaHwAddPortToILan PROTO ((UINT4 u4ILanIndex, UINT4 u4PortIndex));

PUBLIC INT4
NpSyncFsMiPbbTeHwSetEspVid PROTO ((UINT4 u4ContextId, tVlanId EspVlanId));

PUBLIC INT4
NpSyncFsMiPbbTeHwResetEspVid PROTO ((UINT4 u4ContextId, tVlanId EspVlanId));

#ifdef QOSX_WANTED

PUBLIC INT4
NpSyncQoSHwInit PROTO ((VOID));

PUBLIC INT4
NpSyncQoSHwMapClassToPolicy PROTO ((tQoSClassMapEntry *         pClsMapEntry,
                            tQoSPolicyMapEntry *        pPlyMapEntry,
                            tQoSInProfileActionEntry *  pInProActEntry,
                            tQoSOutProfileActionEntry * pOutProActEntry,
                            tQoSMeterEntry *            pMeterEntry,
                            UINT1                       u1Flag));

PUBLIC INT4
NpSyncQoSHwUpdatePolicyMapForClass PROTO ((tQoSClassMapEntry *         pClsMapEntry,
                                   tQoSPolicyMapEntry *        pPlyMapEntry,
                                   tQoSInProfileActionEntry *  pInProActEntry,
                                   tQoSOutProfileActionEntry * pOutProActEntry,
                                   tQoSMeterEntry *            pMeterEntry,
                                   UINT1                       u1Flag));

PUBLIC INT4
NpSyncQoSHwUnmapClassFromPolicy PROTO ((tQoSClassMapEntry *  pClsMapEntry,
                                tQoSPolicyMapEntry * pPlyMapEntry,
                                tQoSMeterEntry *     pMeterEntry,
                                UINT1                u1Flag));

PUBLIC INT4
NpSyncQoSHwDeleteClassMapEntry PROTO ((tQoSClassMapEntry * pClsMapEntry));

PUBLIC INT4
NpSyncQoSHwMeterCreate PROTO ((tQoSMeterEntry * pMeterEntry));

PUBLIC INT4
NpSyncQoSHwMeterDelete PROTO ((INT4 u4MeterId));

PUBLIC INT4
NpSyncQoSHwSchedulerAdd PROTO ((tQoSSchedulerEntry * pSchedEntry));

PUBLIC INT4
NpSyncQoSHwSchedulerUpdateParams PROTO ((tQoSSchedulerEntry * pSchedEntry));

PUBLIC INT4
NpSyncQoSHwSchedulerDelete PROTO ((INT4 u4IfIndex, UINT4 u4SchedId));

PUBLIC INT4
NpSyncQoSHwQueueCreate PROTO ((INT4              u4IfIndex,
                       UINT4             u4QId,
                       tQoSQEntry *      pQEntry,
                       tQoSQtypeEntry *  pQTypeEntry,
                       tQoSREDCfgEntry *papRDCfgEntry[],
                       INT2              u2HL));

PUBLIC INT4
NpSyncQoSHwQueueDelete PROTO ((INT4 u4IfIndex, UINT4 u4Id));

PUBLIC INT4
NpSyncQoSHwMapClassToQueue PROTO ((INT4  u4IfIndex,
                           INT4  u4ClsOrPriType,
                           UINT4 u4ClsOrPri,
                           UINT4 u4QId,
                           UINT1 u1Flag));

PUBLIC INT4
NpSyncQoSHwSchedulerHierarchyMap PROTO ((INT4  u4IfIndex,
                                 UINT4 u4SchedId,
                                 UINT2 u2Sweight,
                                 UINT1 u1Spriority,
                                 UINT4 u4NextSchedId,
                                 UINT4 u4NextQId,
                                 INT2  u2HL,
                                 UINT1 u1Flag));

PUBLIC INT4
NpSyncQoSHwSetDefUserPriority PROTO ((INT4 u4Port, INT4 u4DefPriority));

PUBLIC INT4
NpSyncQoSHwMapClassToQueueId PROTO ((tQoSClassMapEntry * pClassMapEntry,
                             INT4                u4IfIndex,
                             INT4                u4ClsOrPriType,
                             UINT4               u4ClsOrPri,
                             UINT4               u4QId,
                             UINT1               u1Flag));
PUBLIC INT4 
NpSyncFsQosHwConfigPfc PROTO ((tQosPfcHwEntry * pQosPfcHwEntry));

PUBLIC INT4
NpSyncQoSHwSetCpuRateLimit PROTO ((INT4 i4CpuQueueId, UINT4 u4MinRate, UINT4 u4MaxRate));
#endif

PUBLIC INT4
NpSyncIssHwUpdateL2Filter PROTO ((tIssL2FilterEntry * pIssL2FilterEntry, INT4 i4Value));

PUBLIC INT4
NpSyncIssHwUpdateL3Filter PROTO ((tIssL3FilterEntry * pIssL3FilterEntry, INT4 i4Value));

PUBLIC INT4
NpSyncIssHwSetRateLimitingValue PROTO ((UINT4 u4IfIndex,
                                UINT1 u1PacketType,
                                INT4  i4RateLimitVal));

PUBLIC INT4
NpSyncIssHwSetPortEgressPktRate PROTO ((UINT4 u4IfIndex,
                                INT4  i4PktRate,
                                INT4  i4BurstRate));

PUBLIC INT4
ErpsSyncNpFsErpsHwRingConfig PROTO ((tErpsHwRingInfo *pErpsHwRingInfo));

PUBLIC INT4
NpSyncIssHwConfigPortIsolationEntry PROTO ((tIssHwUpdtPortIsolation * pIssHwUpdtPortIsolation));

#endif /* _HWAUDMAP_H */
