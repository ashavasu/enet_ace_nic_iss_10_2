/******************************************************************** 
* Copyright (C) Future Software,2002 
* 
* $Id: sec.h,v 1.31 2015/03/13 12:43:19 siva Exp $ 
* 
* Description: This file contains the proto type definitions required 
*              common to ipsecv4 and ipsecv6. 
* 
*******************************************************************/ 
 
#ifndef _SEC_H_ 
#define _SEC_H_ 
 

#ifdef _SECINIT_C_  
UINT4        gSecMaxSA; 
UINT1        gu1SecAhAlgoDigestSize;
#else 
extern UINT4        gSecMaxSA; 
extern UINT1        gu1SecAhAlgoDigestSize;
#endif 

/*Message pool ID being used in ike ipsecv4 and ipsecv6*/
#ifndef SECURITY_KERNEL_MAKE_WANTED
#ifdef _SECINIT_C_
tMemPoolId   gIkeMsgPoolId;
tMemPoolId   Secv4IpsecMsgToIkeMemPoolId;
#else
extern tMemPoolId   gIkeMsgPoolId;
extern tMemPoolId   Secv4IpsecMsgToIkeMemPoolId;
#endif
#else
#ifdef __IKE_INIT_C__
tMemPoolId   gIkeMsgPoolId;
tMemPoolId   Secv4IpsecMsgToIkeMemPoolId;
#else
extern tMemPoolId   gIkeMsgPoolId;
extern tMemPoolId   Secv4IpsecMsgToIkeMemPoolId;
#endif
#endif



VOID Secv4SendIcmpErrMsg PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, t_ICMP * pIcmp));
 
/* System sizing parameter */ 
#ifdef REVISIT /* -MSAD  - IPSEC/IKE */
#define MAX_NUMBER_OF_SA                gSecMaxSA   
#endif

#define MAX_NUMBER_OF_SA                200   
#define SEC_MAX_IKE_IPSEC_CONFIG_MSG                  5

/* To compute Option Length */
#define IPSEC_OLEN(Ver4Hlen4)\
          (((Ver4Hlen4 & 0x0f) * 4) - SEC_IPV4_HEADER_SIZE)
#define SEC_SRC_PORT_NUMBER_OFFSET             20
 
/* Buffer related macros */ 
#define IPSEC_COPY_OVER_BUF_AT_END         CRU_BUF_Copy_OverBufChain_AtEnd
#define IPSEC_COPY_OVER_BUF                CRU_BUF_Copy_OverBufChain 
#define IPSEC_COPY_FROM_BUF                CRU_BUF_Copy_FromBufChain 
#define IPSEC_MEMCMP                       MEMCMP 
#define IPSEC_BUF_Allocate                 CRU_BUF_Allocate_MsgBufChain 
#define IPSEC_BUF_Release                  CRU_BUF_Release_MsgBufChain 
#define IPSEC_BUF_Concat                   CRU_BUF_Concat_MsgBufChains 
#define IPSEC_BUF_Fragment                 CRU_BUF_Fragment_BufChain 
#define IPSEC_BUF_Prepend                  CRU_BUF_Prepend_BufChain 
#define IPSEC_BUF_MoveOffset               CRU_BUF_Move_ValidOffset 
#define IPSEC_BUF_DeleteAtEnd              CRU_BUF_Delete_BufChainAtEnd 
#define IPSEC_BUF_GetValidBytes            CRU_BUF_Get_ChainValidByteCount 
#define IPSEC_BUF_Get_DataPtr_IfLinear     CRU_BUF_Get_DataPtr_IfLinear 
#define IPSEC_OSIX_GETSYSTIME              OsixGetSysTime 
#define IPSEC_BUF_READ_OFFSET(buf)         CB_READ_OFFSET(buf) 
#define BUF_FAILURE                        CRU_FAILURE 
 
 
#define IPSEC_HTONL                        OSIX_HTONL 
#define IPSEC_NTOHL                        OSIX_NTOHL 
#define IPSEC_HTONS(x)                     (UINT2) OSIX_HTONS(x) 
#define IPSEC_NTOHS(x)                     (UINT2) OSIX_NTOHS(x) 
#define IPSEC_MEMCMP                       MEMCMP 
#define IPSEC_MEMCPY                       MEMCPY 
#define IPSEC_MEMSET                       MEMSET 
#define IPSEC_MALLOC                       MEM_MALLOC 
#define IPSEC_CALLOC                       MEM_CALLOC  
#define IPSEC_MEMFREE                      MEM_FREE 
#define IPSEC_STRCPY                       STRCPY 
#define IPSEC_STRCMP                       STRCMP 
 
#define SEC_ZERO                           0
 
#define IKE_MSG_MEMPOOL_ID                 gIkeMsgPoolId
#define SECv4_IPSEC_MSG_TO_IKE_MEMPOOl      Secv4IpsecMsgToIkeMemPoolId
/* Constants specified in secv4.mib  and secv6.mib */ 
 
#define  SEC_FILTER                          1 
#define  SEC_ALLOW                           2 
#define  SEC_APPLY                           3 
#define  SEC_BYPASS                          4 
#define  SEC_IKE                             5 
#define  SEC_REJECT                         -1 
#define  SEC_INBOUND                         1 
#define  SEC_OUTBOUND                        2 
#define  SEC_MANUAL                          1 
#define  SEC_AUTOMATIC                       2 

#define  SEC_NULLAHALGO                      0 
#define  SEC_MD5                             4 
#define  SEC_KEYEDMD5                        3 
#define  SEC_HMACMD5                         1 
#define  SEC_HMACSHA1                        2 
#define  SEC_XCBCMAC                         5
#define  HMAC_SHA_256                       12
#define  HMAC_SHA_384                       13
#define  HMAC_SHA_512                       14
#define  SEC_NULLESPALGO                     11 
#define  SEC_DES_CBC                         4 
#define  SEC_3DES_CBC                        5
#define  SEC_AES                             12
#define  SEC_AES192                          13
#define  SEC_AES256                          14
#define  SEC_AESCTR                          15
#define  SEC_AESCTR192                       16
#define  SEC_AESCTR256                       17
#define  SEC_DH_GROUP_14                     14

/* IKEv1 Phase 2 transform ESP authentication Algorithm values */
#define  HMAC_PH2_SHA_256                    5
#define  HMAC_PH2_SHA_384                    6
#define  HMAC_PH2_SHA_512                    7
#define  HMAC_PH2_XCBCMAC                    9

/* IKEv1 Phase 1 transform authentication Algorithm values */
#define  HMAC_PH1_SHA_256                    4
#define  HMAC_PH1_SHA_384                    5
#define  HMAC_PH1_SHA_512                    6

#define  SEC_TRANSPORT                       2 
#define  SEC_TUNNEL                          1 

#define  SEC_ENABLE                          1 
#define  SEC_DISABLE                         2 
#define  SEC_MAX_STAT_COUNT                100  /* Maximum statistics entries */ 
#define  SEC_MIN_STAT_COUNT                  1  /* Minimum ststistics entries */ 
#define  SEC_ANY_PROTOCOL                 9000 
#define  SEC_ANY_PORT                     9000 
#define  SEC_ANY_DIRECTION                   3 
#define  SEC_ANY_IFINDEX                     0 
#define  SEC_MIN_SA_BUNDLE_LEN               1 
#define  SEC_MAX_SA_BUNDLE_LEN             100 
#define  SEC_MAX_INTEGER            2147483647  /* The Max Range of INT4 */ 
#define  SEC_MIN_INTEGER                     1 
#define  SEC_HMACMD5_KEY_LEN                16 
#define  SEC_HMACSHA1_KEY_LEN               20 
#define  SEC_ESP_DES_KEY_LEN                 8   /* For DES-CBC Key Length */
#define  SEC_ESP_AES_KEY1_LEN               16   /* For AES Key Length */
#define  SEC_ESP_AES_KEY2_LEN               24   /* For AES Key2 Length */
#define  SEC_ESP_AES_KEY3_LEN               32   /* For AES Key3 Length */
#define  SEC_HMAC_SHA_MAX_LEN               64   /* For HMAC_SHA_512 Key Length */
#define  SEC_MAX_AH_KEY_LEN                 128
#define  SEC_MAX_ESP_KEY_LEN                32
#define  SEC_MIN_SPI                       255 
#define  SEC_MAX_SPI                2147483647  /* The Max Range of INT4 */
#define  SEC_MIN_LIFE_BYTES_VALUE            0 
#define  SEC_MAX_LIFE_BYTES_VALUE   2147483647   /* The Max Range of INT4 */ 
#define  SEC_DEFAULT_LIFE_TIME_VALUE         0 
#define  SEC_MIN_LIFE_TIME_VALUE           300 
#define  SEC_MAX_LIFE_TIME_VALUE       2592000 
#define  SEC_MAX_BUNDLE_SA                   8 
#define  SEC_TCP                             6 
#define  SEC_UDP                            17 
#define  SEC_ANTI_REPLAY_ENABLE              1 
#define  SEC_ANTI_REPLAY_DISABLE             2 
#define SEC_INVALID_HASH_INDEX              -1
#define SEC_MAX_NUMBER_OF_BUCKETS          100   
#define SEC_CONTEXT_ENTRY_FOUND              0   
#define SEC_CONTEXT_ENTRY_NOT_FOUND          1 
#define SEC_DEFAULT_DUMMY_PKT_LEN           25
#define SEC_MAX_DUMMY_PKT_LEN               100
 
#define SEC_MIN_PORT                        0
#define SEC_MAX_PORT                        65535

#define   IPSEC_TIMER_EXPIRY_EVENT    0x40000000
#define   IPSEC_NTFY_EVENT            0x80000000

/* Macros for AES-CTR module */

#define IPSEC_AES_CTR_NONCE_LEN           4
#define IPSEC_AES_CTR_COUNTER_BLOCK_LEN   16
#define IPSEC_AES_CTR_INIT_VECT_SIZE      8
#define IPSEC_AES_CTR_COUNTER_LEN         4
#define IPSEC_AES_BLOCK_SIZE              16
 
/* For Trace Implemntations used by secv4low.c,secv6low.c and secv4cli.c */ 
 
#define SEC_DISABLE_ALL                 0 
#define SEC_ENABLE_ALL                  1 
#define SEC_INIT_SHUT                   2 
#define SEC_MGMT                        3  
#define SEC_DATA_PATH                   4  
#define SEC_CNTRL_PATH                  5  
#define SEC_DUMP                        6    
#define SEC_OS_RESOURCE                 7   
#define SEC_ALL_FAILURE                 8  
#define SEC_BUFFER                      9  
 
/* Constants related to AH and ESP Protocol */ 
 
#define  SEC_AUTH_DIGEST_SIZE           gu1SecAhAlgoDigestSize 
#define  SEC_DEF_AUTH_DIGEST_SIZE       12
#define  SEC_AUTH_MAX_DIGEST_SIZE       32
#define  SEC_SEQ_NUMBER_LEN              4 
#define  SEC_SPI_LEN                     4 
#define  SEC_PORT_NUMBER_LEN             2 
#define  SEC_ALGO_DIGEST_SIZE           64

#define  HMAC_SHA_256_AUTH_DIGEST_SIZE  16
#define  HMAC_SHA_384_AUTH_DIGEST_SIZE  24
#define  HMAC_SHA_512_AUTH_DIGEST_SIZE  32

 
/* Constants used by secv4io.c.secv6io,c and secv4conf.c,secv6conf.c */ 
 
#define  SEC_MY_PACKET                   0 
#define  SEC_NOT_MY_PACKET               1 
#define  SEC_MAX_HOP_LIMIT               255 
#define  SEC_UNUSED(x) (x=x) 
 
/* Constants for MsgType in the IPSecIfParam structure */

#define  IPSEC_TASK                       1

/* Macros related to Messages between IKE & IPSEC */
#define IKE_IPSEC_INSTALL_SA              1
#define IKE_IPSEC_DUPLICATE_SPI_REQ       2
#define IKE_IPSEC_PROCESS_DELETE_SA       3
#define IKE_IPSEC_PROCESS_DEL_SA_BY_ADDR  4 
#define IKE_IPSEC_PROCESS_DEL_SA_FOR_LOCAL_CONF_CHG 5
#define IKE_IPSEC_PROCESS_ADD_TIME_DEL_SA  6 
#define  SEC_ESP_KEY_LEN               33
#define  SEC_AH_KEY_LEN                65
#endif /* _SEC_H_ */ 

