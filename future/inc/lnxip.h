/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: lnxip.h,v 1.28 2016/02/27 10:13:58 siva Exp $
 *
 * Description:This file contains the exported definitions and                 
 *             macros of LINUXIP.
 *
 *******************************************************************/
#ifndef _LNXIP_H
#define _LNXIP_H

#define   RT_FS_PROTO  20

#ifdef IP6_WANTED
/* Linux IP6 configuration files.
 * These macro definitions are system dependent and needs update according
 * to the target system environment */
#define LIP6_PROC_CONF            "/proc/sys/net/ipv6/conf"
#define LIP6_PROC_FWD             "forwarding"
#define LIP6_PROC_DADENABLE       "accept_dad"
#define LIP6_PROC_DISABLEIPV6     "disable_ipv6"
#define LIP6_SRC_ROUTE            "accept_source_route"
#define LIP6_PROC_HOPLIMIT        "hop_limit"
#define LIP6_PROC_DADATTEMPT      "dad_transmits"
#define LIP6_PROC_IFINET6         (const char *) "/proc/net/if_inet6"
#define LIP6_PROC_DEVSNMP         (const UINT1 *) "/proc/net/dev_snmp6"

#define LIP6_RADVD_CONF_FILE      (const UINT1 *) "/etc/radvd.conf"

#define LIP6_IPTBL_CREATE_CHAIN   "ip6tables -N ARICENT"
#define LIP6_IPTBL_DELETE_CHAIN   "ip6tables -X ARICENT"
#define LIP6_IPTBL_LINK_CHAIN     "ip6tables -A INPUT -j ARICENT"
#define LIP6_IPTBL_DELINK_CHAIN   "ip6tables -D INPUT -j ARICENT"
#define LIP6_IPTBL_FLUSH_CHAIN    "ip6tables -F ARICENT"
#define LIP6_IPTBL_ADD            "ip6tables -A ARICENT"
#define LIP6_IPTBL_DEL            "ip6tables -D ARICENT"
#define LIP6_IPTBL_DEST           "-d"
#define LIP6_IPTBL_SRC            "-s"
#define LIP6_IPTBL_DROP           "-j DROP"

#define LIP6_IPTBL_INTF           "-i"
#define LIP6_IPTBL_PROT_ICMP6     "-p icmpv6"
#define LIP6_IPTBL_ICMP_TYPE_NS   "--icmpv6-type neighbour-solicitation"

#endif

#define LNX_MAX_COMMAND_LEN       80

#define LIP4_PROC_CONF            "/proc/sys/net/ipv4/conf"
#define LIP4_PROC_ACCEPTLOCAL     "accept_local"
#define LIP4_PROC_RPFILTER        "rp_filter"
#define LIP4_PROC_ARPIGNORE       "arp_ignore"
#define LIP4_PROC_ARPFILTER       "arp_filter"
#define LIP4_PROC_FWD             "forwarding"

#define LNX_RPF_ENABLE         1
#define LNX_RPF_DISABLE        0

#define LIP4_IPTBL_CREATE_CHAIN   "iptables -N ARICENT"
#define LIP4_IPTBL_DELETE_CHAIN   "iptables -X ARICENT"
#define LIP4_IPTBL_LINK_CHAIN     "iptables -A INPUT -j ARICENT"
#define LIP4_IPTBL_DELINK_CHAIN   "iptables -D INPUT -j ARICENT"
#define LIP4_IPTBL_FLUSH_CHAIN    "iptables -F ARICENT"
#define LIP4_IPTBL_ADD            "iptables -A ARICENT -d"
#define LIP4_IPTBL_DEL            "iptables -D ARICENT -d"
#define LIP4_IPTBL_DROP           "-j DROP"

#define LIP4_LINE_LEN             200

#define NLMSG_TAIL(nmsg) ((struct rtattr *) (((void *) (nmsg)) + NLMSG_ALIGN((nmsg)->nlmsg_len)))

#define MAX_NETNS_PATH          48

PUBLIC INT4 
LnxIpInit PROTO((INT1 *));

PUBLIC INT4 
LnxIpMain PROTO((INT1 *));

VOID LnxIpTapFdCbkFunc(INT4 i4SockDesc);
VOID
LnxDeInit(VOID);

INT4
LnxIpMcastTaskMain PROTO ((VOID));

typedef struct _LnxIpPortMapNode{
    tTMO_HASH_NODE SllNode;
    UINT4 u4CfaIfIndex;
    UINT4 u4IpPortNum;
    UINT4 u4IpPortSpeed;
    UINT4 u4IpPortMtu;
    UINT2 u2VifId;             /*Virtual Interface Id(for multicast)
                                *mapped to u4IpPortNum*/
    UINT1 u1McastProtocol;     /*Multicast Protocol enabled on the Vif */ 
    UINT1 u1Ipv4EnableStatus;
    UINT1 u1OperState;
    UINT1 u1IfType;
    UINT1 u1BytePad[2];
}tLnxIpPortMapNode;

/* Structure required for passing Logical Interface related parameters from
 * User Land via ioctl () for Interface creation or Deletion */
typedef struct _LnxIpIntfParams
{
    INT4                u4Action;      /* Flag to create/delete an interface */
    UINT1              *pu1IntfName;   /* L3 Interface name */
    UINT4               u4IfIndex;     /* IP Port Information over which 
                                          this L3 Interface is stacked */
    union
    {
        UINT1           au1MacAddress[MAC_ADDR_LEN]; /* MAC Address */
        UINT4           u4IpAddr;      /* IP Address of the interface */
        UINT1           *pu1NetMask;   /* Subnet Mask */
        UINT1           u1OperStatus;  /* Operational Status */
        UINT1           au1Pad[8];  /* Padded with 8 bytes because it is a union*/
    } InterfaceSet;
} tLnxIpIntfParams;

#if defined(LNXIP6_WANTED)
PUBLIC INT4  Lip6MainInit PROTO((VOID));
PUBLIC INT4 LnxIp6McastTaskMain PROTO((INT1 *pParam));
PUBLIC VOID LnxVrfIpv6Initializations PROTO ((UINT4 u4VcmCxtId));
PUBLIC VOID LnxVrfIpv6DeInitializations PROTO ((UINT4 u4VcmCxtId));
PUBLIC VOID Lip6KernelDeleteAddrForIndex PROTO ((UINT4 u4Index));
UINT4 Lip6GetCurrentContext PROTO ((VOID));
VOID LnxVrfIpv6UpdateGlobSock PROTO ((UINT4 u4VcmCxtId, INT4 i4SockFd));
VOID LnxVrfUpdateCxtIdforIpv6Int PROTO ((UINT4 u4VcmCxtId, INT4 u4IfIndex,INT4 i4PortNo));
VOID LnxVrfEnableIpv6Forwarding PROTO ((VOID));
#endif

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
#define VCM_TASK_MAX_LEN        16
#define VCM_NS_MAX_LEN          8
#define VCM_QUEUE_MAX_LEN       16
#define LNX_VRF_UDP_SOCKET_STATUS_EVENT         0x00000010
#define LNX_VRF_FAILURE_STATUS_EVENT            0x00000020
#define LNX_VRF_DEFAULT_NS             1
#define LNX_VRF_NON_DEFAULT_NS         2
#define LNX_VRF_DEFAULT_NS_PID         "1"

typedef enum VrfLnxMsgType
{
LNX_VRF_CREATE_CONTEXT =1,
LNX_VRF_DELETE_CONTEXT,
LNX_VRF_IF_MAP_CONTEXT,
LNX_VRF_IF_UNMAP_CONTEXT,
LNX_VRF_ADD_ARP_ENTRY,
LNX_VRF_DEL_ARP_ENTRY,
LNX_VRF_ADD_ROUTE_ENTRY,
LNX_VRF_DEL_ROUTE_ENTRY,
LNX_VRF_OPEN_RAW_SOCKET,
LNX_VRF_CLOSE_RAW_SOCKET,
LNX_VRF_OPEN_TCP_SOCKET,
LNX_VRF_CLOSE_TCP_SOCKET,
LNX_VRF_OPEN_UDP_SOCKET,
LNX_VRF_CLOSE_UDP_SOCKET,
LNX_VRF_START_RADVD,
LNX_VRF_STOP_RADVD,
LNX_VRF_IPv6_ADD_ROUTE_ENTRY,
LNX_VRF_IPv6_DEL_ROUTE_ENTRY,
LNX_VRF_ADD_ND_ENTRY,
LNX_VRF_DEL_ND_ENTRY,
LNX_VRF_OPEN_SOCKET,
LNX_VRF_MAX_MESSAGES
} eVrfLnxMsgType;


typedef struct LnxVrfInfo
{
        tRBNodeEmbd     LnxVrfRbNode;
        INT4 i4SockDgramId;
        INT4 i4RawSockFd;
        UINT4 u4VrfId;
        UINT4 u4TskId;
        tOsixSemId LnxVrfSemId;
        tOsixQId LnxVrfQueId;
        UINT1 au1TaskName[VCM_TASK_MAX_LEN];
        UINT1 au1NameSpace[VCM_NS_MAX_LEN];
        UINT1 au1QueueName[VCM_QUEUE_MAX_LEN];
        UINT1 au1SemName[VCM_QUEUE_MAX_LEN];
}tLnxVrfInfo;
typedef struct LnxVcmInfo
{
    UINT4 u4IfIndex;
    UINT4 u4VrfId;
}tLnxVcmInfo;
typedef struct LnxVrfEventInfo
{
    UINT4 u4ContextId;
    UINT4 i4Sockdomain;
    UINT4 i4SockType;
    UINT4 i4SockProto;
    INT4    i4SockId;
    UINT4  u4IfIndex;
    UINT1  u1MsgType;
    UINT1  u1IfType;
    UINT1  au1Pad[2];
}tLnxVrfEventInfo;

typedef struct LnxVrfIfInfo
{
    tRBNodeEmbd LnxVrfIfInfoNode;
    UINT4 u4IfIndex;
    UINT4 u4ContextId;
    UINT1 au1IfName[24];
}tLnxVrfIfInfo;

#endif
tLnxIpPortMapNode  *
LnxIpGetPortMapNode PROTO ((UINT4 u4IpPortNum));

INT4  LnxIpUpdateSecondaryIpInfo PROTO ((UINT1 *pu1DevName,
                                      tIpConfigInfo *pIpInfo, UINT4 u4Flag));
PUBLIC INT4
LinuxIpUpdateInterfaceStatus PROTO ((UINT1* pu1DevName,UINT1, UINT1 u1Action));

VOID LnxIpDeleteIpAddrOnRestart(VOID);
INT4 LnxIpVrrpVifConfigInLnx PROTO ((tVrrpNwIntf *pVrrpNwIntf));
INT4 LnxIpVrrpVifDeleteInLnx PROTO ((tVrrpNwIntf *pVrrpNwIntf));

VOID LnxIpVrrpAddSecondaryIp (tVrrpNwIntf *pVrrpNwIntf);
VOID LnxIpVrrpDelSecondaryIp (tVrrpNwIntf *pVrrpNwIntf);
VOID LnxIpVrrpAddDropFilter (tIPvXAddr IpAddr);
VOID LnxIpVrrpDelDropFilter (tIPvXAddr IpAddr);

INT4 Lip6VrrpVifConfigInLnx PROTO ((tVrrpNwIntf *pVrrpNwIntf));
INT4 Lip6VrrpVifDeleteInLnx PROTO ((tVrrpNwIntf *pVrrpNwIntf));
VOID Lip6VrrpAddDropFilter (tIPvXAddr IpAddr);
VOID Lip6VrrpDelDropFilter (tIPvXAddr IpAddr);
INT4 LnxIpSetReversePathFilter (UINT1 *pu1DevName, INT4 i4SetVal);


extern UINT1      gu1LnxTapEnabled;
/* IPvx Macros */
extern UINT4 gu4Ip4IfTableLastChange;
#define LNXIP_SET_IF_TBL_CHANGE_TIME() OsixGetSysTime (&(gu4Ip4IfTableLastChange))
#define LNXIP_GET_IF_TBL_CHANGE_TIME() (gu4Ip4IfTableLastChange)

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
UINT4 LnxVrfCreateContext (UINT4 u4ContextId);
UINT4 LnxVrfDeleteContext (UINT4 u4ContextId);
VOID LnxVrfAuditTaskMain PROTO ((INT1 * pi1Arg));
tLnxVrfInfo *
LnxVrfInfoGet PROTO ((UINT4 u4VrfId));
tLnxVrfIfInfo *
LnxVrfIfInfoGet(UINT1 *pu1IfName);

INT4 LnxVrfChangeCxt(INT4 type,  CHR1 *ns);

UINT4 LnxVrfUpdateInterface(UINT4,INT4, UINT1 *, UINT4,UINT4);
UINT4 LnxVrfOpenSock_Dgrem(UINT4 u4ContextId,UINT4 u4CfaIfIndex);
PUBLIC VOID
LnxVrfCallbackFn (UINT4 u4IpIfIndex,UINT4 u4VcmCxtId, UINT1 u1BitMap);
INT4 LnxVrfOpenSockDgram (UINT4 u4ContextId, INT4 *pi4SockFd, UINT4 u4TskId);
INT4 LnxVrfEventHandling(tLnxVrfEventInfo *pLnxVrfEventInfo,INT4 *pi4SockFd);
INT4 LnxVrfGetSocketFd(UINT4 u4ContextId, INT4 i4Sockdomain, INT4 i4SockType,
                        INT4 i4SockProto, UINT1 u1MsgType);
INT4 LnxVrfSockLock(VOID);
INT4 LnxVrfSockUnLock(VOID);
VOID LnxVrfNLSockInit(VOID);
INT4 LnxIpMcastInitInCxt PROTO ((UINT4 u4ContextId, INT4 i4SockId));
VOID LnxIpMcastDeInitInCxt PROTO ((UINT4 u4ContextId));
#endif
#endif

