/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: pvrstnpwr.h,v 1.4 2016/08/20 09:40:10 siva Exp $
 *
 * Description:This file contains the necessary data strucutures
 *             for packet handling in NPAPI layer
 *             <Part of dual node stacking model>
 *
 *******************************************************************/

#ifndef __PVRST_NP_WR_H__
#define __PVRST_NP_WR_H__

#include "pvrst.h"
#include "fsvlan.h"

/* OPER ID */
enum 
{
 FS_MI_PVRST_NP_CREATE_VLAN_SPANNING_TREE = 1,
 FS_MI_PVRST_NP_DELETE_VLAN_SPANNING_TREE,
 FS_MI_PVRST_NP_INIT_HW,
 FS_MI_PVRST_NP_DE_INIT_HW,
 FS_MI_PVRST_NP_SET_VLAN_PORT_STATE,
 FS_MI_PVRST_NP_GET_VLAN_PORT_STATE,
#ifdef  MBSM_WANTED
 FS_MI_PVRST_MBSM_NP_SET_VLAN_PORT_STATE,
 FS_MI_PVRST_MBSM_NP_INIT_HW,
#endif /* MBSM_WANTED */
 FS_NP_PVRST_MAX_NP
};

/* Required arguments list for the pvrst NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

typedef struct {
    UINT4    u4ContextId;
    tVlanId  VlanId;
 UINT1    au1Pad[2];
} tPvrstNpWrFsMiPvrstNpCreateVlanSpanningTree;

typedef struct {
    UINT4    u4ContextId;
    tVlanId  VlanId;
 UINT1    au1Pad[2];
} tPvrstNpWrFsMiPvrstNpDeleteVlanSpanningTree;

typedef struct {
    UINT4  u4ContextId;
} tPvrstNpWrFsMiPvrstNpInitHw;

typedef struct {
    UINT4  u4ContextId;
} tPvrstNpWrFsMiPvrstNpDeInitHw;

typedef struct {
    UINT4    u4IfIndex;
    UINT4    u4ContextId;
    tVlanId  VlanId;
    UINT1    u1PortState;
 UINT1    au1Pad[1];
} tPvrstNpWrFsMiPvrstNpSetVlanPortState;

typedef struct {
    UINT4    u4IfIndex;
    UINT1 *  pu1Status;
    UINT4    u4ContextId;
    tVlanId  VlanId;
 UINT1    au1Pad[2];
} tPvrstNpWrFsMiPvrstNpGetVlanPortState;

#ifdef  MBSM_WANTED
typedef struct {
    UINT4            u4IfIndex;
    tMbsmSlotInfo *  pSlotInfo;
    UINT4            u4ContextId;
    tVlanId          VlanId;
    UINT1            u1PortState;
 UINT1            au1Pad[1];
} tPvrstNpWrFsMiPvrstMbsmNpSetVlanPortState;

typedef struct {
    tMbsmSlotInfo *  pSlotInfo;
    UINT4            u4ContextId;
} tPvrstNpWrFsMiPvrstMbsmNpInitHw;

#endif /* MBSM_WANTED */
typedef struct PvrstNpModInfo {
union {
    tPvrstNpWrFsMiPvrstNpCreateVlanSpanningTree  sFsMiPvrstNpCreateVlanSpanningTree;
    tPvrstNpWrFsMiPvrstNpDeleteVlanSpanningTree  sFsMiPvrstNpDeleteVlanSpanningTree;
    tPvrstNpWrFsMiPvrstNpInitHw  sFsMiPvrstNpInitHw;
    tPvrstNpWrFsMiPvrstNpDeInitHw  sFsMiPvrstNpDeInitHw;
    tPvrstNpWrFsMiPvrstNpSetVlanPortState  sFsMiPvrstNpSetVlanPortState;
    tPvrstNpWrFsMiPvrstNpGetVlanPortState  sFsMiPvrstNpGetVlanPortState;
#ifdef  MBSM_WANTED
    tPvrstNpWrFsMiPvrstMbsmNpSetVlanPortState  sFsMiPvrstMbsmNpSetVlanPortState;
    tPvrstNpWrFsMiPvrstMbsmNpInitHw  sFsMiPvrstMbsmNpInitHw;
#endif /* MBSM_WANTED */
    }unOpCode;

#define  PvrstNpFsMiPvrstNpCreateVlanSpanningTree  unOpCode.sFsMiPvrstNpCreateVlanSpanningTree;
#define  PvrstNpFsMiPvrstNpDeleteVlanSpanningTree  unOpCode.sFsMiPvrstNpDeleteVlanSpanningTree;
#define  PvrstNpFsMiPvrstNpInitHw  unOpCode.sFsMiPvrstNpInitHw;
#define  PvrstNpFsMiPvrstNpDeInitHw  unOpCode.sFsMiPvrstNpDeInitHw;
#define  PvrstNpFsMiPvrstNpSetVlanPortState  unOpCode.sFsMiPvrstNpSetVlanPortState;
#define  PvrstNpFsMiPvrstNpGetVlanPortState  unOpCode.sFsMiPvrstNpGetVlanPortState;
#ifdef  MBSM_WANTED
#define  PvrstNpFsMiPvrstMbsmNpSetVlanPortState  unOpCode.sFsMiPvrstMbsmNpSetVlanPortState;
#define  PvrstNpFsMiPvrstMbsmNpInitHw  unOpCode.sFsMiPvrstMbsmNpInitHw;
#endif /* MBSM_WANTED */
} tPvrstNpModInfo;


/* Function Prototype for Generic NP OpCode specific wrappers
 * defined in Pvrstnpapi.c */

UINT1 PvrstFsMiPvrstNpCreateVlanSpanningTree PROTO ((UINT4 u4ContextId, tVlanId VlanId));
UINT1 PvrstFsMiPvrstNpDeleteVlanSpanningTree PROTO ((UINT4 u4ContextId, tVlanId VlanId));
UINT1 PvrstFsMiPvrstNpInitHw PROTO ((UINT4 u4ContextId));
UINT1 PvrstFsMiPvrstNpDeInitHw PROTO ((UINT4 u4ContextId));
UINT1 PvrstFsMiPvrstNpSetVlanPortState PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId VlanId, UINT1 u1PortState));
UINT1 PvrstFsMiPvrstNpGetVlanPortState PROTO ((UINT4 u4ContextId, tVlanId VlanId, UINT4 u4IfIndex, UINT1 * pu1Status));
#ifdef  MBSM_WANTED
UINT1 PvrstFsMiPvrstMbsmNpSetVlanPortState PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId VlanId, UINT1 u1PortState, tMbsmSlotInfo * pSlotInfo));
UINT1 PvrstFsMiPvrstMbsmNpInitHw PROTO ((UINT4 u4ContextId, tMbsmSlotInfo * pSlotInfo));
#endif /* MBSM_WANTED */

#endif /* __PVRST_NP_WR_H__ */
