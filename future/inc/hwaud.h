/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: hwaud.h,v 1.10 2012/01/24 13:28:56 siva Exp $
 *
 * Description: This file defines the enumerations and structures
 *              used in the NP syncup for high availability
 ****************************************************************************/

#ifndef _HWAUD_H
#define _HWAUD_H
#include "rmgr.h"
#include "ecfm.h"
#include "erps.h"
#include "iss.h"
#include "isspi.h"
#include "qosxnp.h"


typedef enum { 
    NPSYNC_FS_MI_PBB_NP_INIT_HW                          = 1,
    NPSYNC_FS_MI_PBB_NP_DE_INIT_HW                       = 2,
    NPSYNC_FS_MI_PBB_HW_SET_VIP_ATTRIBUTES               = 3,
    NPSYNC_FS_MI_PBB_HW_DEL_VIP_ATTRIBUTES               = 4,
    NPSYNC_FS_MI_PBB_HW_SET_VIP_PIP_MAP                  = 5,
    NPSYNC_FS_MI_PBB_HW_DEL_VIP_PIP_MAP                  = 6,
    NPSYNC_FS_MI_PBB_HW_SET_BACKBONE_SERVICE_INST_ENTRY  = 7,
    NPSYNC_FS_MI_PBB_HW_DEL_BACKBONE_SERVICE_INST_ENTRY  = 8,
    NPSYNC_FS_MI_BRG_HW_CREATE_CONTROL_PKT_FILTER        = 9,
    NPSYNC_FS_MI_BRG_HW_DELETE_CONTROL_PKT_FILTER        = 10,
    NPSYNC_FS_MI_PBB_HW_SET_ALL_TO_ONE_BUNDLING_SERVICE  = 11,
    NPSYNC_FS_MI_PBB_HW_DEL_ALL_TO_ONE_BUNDLING_SERVICE  = 12,
    NPSYNC_FS_CFA_HW_CREATE_INTERNAL_PORT                = 13,
    NPSYNC_FS_CFA_HW_DELETE_INTERNAL_PORT                = 14,
    NPSYNC_FS_CFA_HW_CREATE_I_LAN                        = 15,
    NPSYNC_FS_CFA_HW_DELETE_I_LAN                        = 16,
    NPSYNC_FS_CFA_HW_REMOVE_PORT_FROM_I_LAN              = 17,
    NPSYNC_FS_CFA_HW_ADD_PORT_TO_I_LAN                   = 18,
    NPSYNC_FS_MI_PBB_TE_HW_SET_ESP_VID                   = 19,
    NPSYNC_FS_MI_PBB_TE_HW_RESET_ESP_VID                 = 20,
    NPSYNC_QO_S_HW_INIT                             = 21,
    NPSYNC_QO_S_HW_MAP_CLASS_TO_POLICY            = 22,
    NPSYNC_QO_S_HW_UPDATE_POLICY_MAP_FOR_CLASS           = 23,
    NPSYNC_QO_S_HW_UNMAP_CLASS_FROM_POLICY               = 24,
    NPSYNC_QO_S_HW_DELETE_CLASS_MAP_ENTRY                = 25,
    NPSYNC_QO_S_HW_METER_CREATE                          = 26,
    NPSYNC_QO_S_HW_METER_DELETE                          = 27,
    NPSYNC_QO_S_HW_SCHEDULER_ADD                         = 28,
    NPSYNC_QO_S_HW_SCHEDULER_UPDATE_PARAMS               = 29,
    NPSYNC_QO_S_HW_SCHEDULER_DELETE                      = 30,
    NPSYNC_QO_S_HW_QUEUE_CREATE                          = 31,
    NPSYNC_QO_S_HW_QUEUE_DELETE                          = 32,
    NPSYNC_QO_S_HW_MAP_CLASS_TO_QUEUE                    = 33,
    NPSYNC_QO_S_HW_SCHEDULER_HIERARCHY_MAP               = 34,
    NPSYNC_QO_S_HW_SET_DEF_USER_PRIORITY                 = 35,
    NPSYNC_QO_S_HW_MAP_CLASS_TO_QUEUE_ID                 = 36,
    NPSYNC_FS_QOS_HW_CONFIG_PFC                          = 37,
    NPSYNC_ISS_HW_UPDATE_L2_FILTER                       = 38,
    NPSYNC_ISS_HW_UPDATE_L3_FILTER                       = 39,
    NPSYNC_ISS_HW_SET_RATE_LIMITING_VALUE                = 40,
    NPSYNC_ISS_HW_SET_PORT_EGRESS_PKT_RATE               = 41,
    NPSYNC_FS_ERPS_HW_RING_CONFIG                        = 42,
    NPSYNC_ISS_HW_CONFIG_PORT_ISOLATION_ENTRY            = 43,
    NPSYNC_QO_S_HW_SET_CPU_RATE_LIMIT                    = 44,
}eNpSyncId;

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
} tNpSyncPortIdx;

VOID NpSyncPortIdxSync PROTO ((tNpSyncPortIdx PortIdx, UINT4 u4AppId, UINT4 u4NpApiId));

typedef struct {
    UINT4 u4ContextId;
    tVlanId VlanId;
    UINT1  au1Pad[2];
} tNpSyncVlanIdx;
  
VOID NpSyncVlanIdxSync PROTO ((tNpSyncVlanIdx VlanIdx, UINT4 u4AppId, UINT4 u4NpApiId));

typedef struct {
    UINT4 u4ContextId;
} tNpSyncContextIdx;

VOID NpSyncContextIdxSync PROTO ((tNpSyncContextIdx ContextIdx, UINT4 u4AppIdi, UINT4 u4NpApiId));

typedef struct {
    UINT4 u4ContextId;
    UINT4 u4VipIfIndex;
} tNpSyncPbbPortIdx;

VOID NpSyncPbbPortIdxSync PROTO ((tNpSyncPbbPortIdx PbbPortIdx, UINT4 u4AppId, UINT4 u4NpApiId));

typedef struct {
    UINT4 u4ContextId;
    UINT4 u4CbpIfIndex;
    UINT4 u4BSid;
} tNpSyncIsidIdx;

VOID NpSyncIsidIdxSync PROTO ((tNpSyncIsidIdx IsidIdx, UINT4 u4AppId, UINT4 u4NpApiId));

VOID NpSyncFsMiPbbNpInitHwSync PROTO ((UINT4 u4AppId, UINT4 u4NpApiId));

VOID NpSyncFsMiPbbNpDeInitHwSync PROTO ((UINT4 u4AppId, UINT4 u4NpApiId));

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
    UINT4  u4Isid;
} tNpSyncFsMiPbbHwSetAllToOneBundlingService;

VOID NpSyncFsMiPbbHwSetAllToOneBundlingServiceSync PROTO ((tNpSyncFsMiPbbHwSetAllToOneBundlingService NpSyncFsMiPbbHwSetAllToOneBundlingService, UINT4 u4AppId, UINT4 u4NpApiId));

typedef struct {
    UINT4  u4ContextId;
    UINT4  u4IfIndex;
    UINT4  u4Isid;
} tNpSyncFsMiPbbHwDelAllToOneBundlingService;

VOID NpSyncFsMiPbbHwDelAllToOneBundlingServiceSync PROTO ((tNpSyncFsMiPbbHwDelAllToOneBundlingService NpSyncFsMiPbbHwDelAllToOneBundlingService, UINT4 u4AppId, UINT4 u4NpApiId));

typedef struct {
    UINT4  u4IfIndex;
} tNpSyncFsCfaHwCreateInternalPort;

VOID NpSyncFsCfaHwCreateInternalPortSync PROTO ((tNpSyncFsCfaHwCreateInternalPort NpSyncFsCfaHwCreateInternalPort, UINT4 u4AppId, UINT4 u4NpApiId));

typedef struct {
    UINT4  u4IfIndex;
} tNpSyncFsCfaHwDeleteInternalPort;

VOID NpSyncFsCfaHwDeleteInternalPortSync PROTO ((tNpSyncFsCfaHwDeleteInternalPort NpSyncFsCfaHwDeleteInternalPort, UINT4 u4AppId, UINT4 u4NpApiId));

typedef struct {
    UINT4         u4ILanIndex;
} tNpSyncFsCfaHwCreateILan;

VOID NpSyncFsCfaHwCreateILanSync PROTO ((tNpSyncFsCfaHwCreateILan NpSyncFsCfaHwCreateILan, UINT4 u4AppId, UINT4 u4NpApiId));

typedef struct {
    UINT4  u4ILanIndex;
} tNpSyncFsCfaHwDeleteILan;

VOID NpSyncFsCfaHwDeleteILanSync PROTO ((tNpSyncFsCfaHwDeleteILan NpSyncFsCfaHwDeleteILan, UINT4 u4AppId, UINT4 u4NpApiId));

typedef struct {
    tErpsHwRingInfo  ErpsHwRingInfo;
} tNpSyncFsErpsHwRingConfig;

VOID ErpsSyncNpFsErpsHwRingConfigSync PROTO ((tNpSyncFsErpsHwRingConfig NpSyncFsErpsHwRingConfig, UINT4 u4AppId, UINT4 u4NpApiId));

typedef struct {
    UINT4  u4ILanIndex;
    UINT4  u4PortIndex;
} tNpSyncFsCfaHwRemovePortFromILan;

VOID NpSyncFsCfaHwRemovePortFromILanSync PROTO ((tNpSyncFsCfaHwRemovePortFromILan NpSyncFsCfaHwRemovePortFromILan, UINT4 u4AppId, UINT4 u4NpApiId));

typedef struct {
    UINT4  u4ILanIndex;
    UINT4  u4PortIndex;
} tNpSyncFsCfaHwAddPortToILan;

VOID NpSyncFsCfaHwAddPortToILanSync PROTO ((tNpSyncFsCfaHwAddPortToILan NpSyncFsCfaHwAddPortToILan, UINT4 u4AppId, UINT4 u4NpApiId));

typedef struct {
    UINT4    u4ContextId;
    tVlanId  EspVlanId;
    UINT1  au1Pad[2];
} tNpSyncFsMiPbbTeHwSetEspVid;

VOID NpSyncFsMiPbbTeHwSetEspVidSync PROTO ((tNpSyncFsMiPbbTeHwSetEspVid NpSyncFsMiPbbTeHwSetEspVid, UINT4 u4AppId, UINT4 u4NpApiId));

typedef struct {
    UINT4    u4ContextId;
    tVlanId  EspVlanId;
    UINT1    au1Pad[2];
} tNpSyncFsMiPbbTeHwResetEspVid;

VOID NpSyncFsMiPbbTeHwResetEspVidSync PROTO ((tNpSyncFsMiPbbTeHwResetEspVid NpSyncFsMiPbbTeHwResetEspVid, UINT4 u4AppId, UINT4 u4NpApiId));

/* Manually added strcutures for the events */
typedef struct {
    UINT1 au1OUI [3];
} tNpSyncPbbGlbOui;

typedef struct {
    UINT4 u4ContextId;
    UINT4 u4IfIndex; 
    UINT4 u4DstPort;
} tNpSyncPbbPortProperty;
/*Start  QOS hardware audit definition and proto type declaration*/
typedef struct _tQoSNpSyncClassMapNode
{
     /* input ACL/Priority-Map */
    UINT4 u4L2FilterId;                 /* L2(MAC) Filter Id                */
    UINT4 u4L3FilterId;                 /* L3(IP) Filter Id                 */
    UINT4 u4PriorityMapId;              /* priority Map Id from            
                                           tQoSInPriorityMapNode           */
    /* outputs */
    UINT4 u4ClassId;                    /* Class Id to the L2 and or     
                                         L3/PriorityMapId                */
} tQoSNpSyncClassMapNode;

VOID NpSyncQoSHwInitSync PROTO ((UINT4 u4AppId, UINT4 u4NpApiId));

typedef struct {    
    tQoSNpSyncClassMapNode  ClassMapNode;
    UINT4 u4PlyMapId;
} tNpSyncQoSHwMapClassToPolicy;

VOID NpSyncQoSHwMapClassToPolicySync PROTO ((tNpSyncQoSHwMapClassToPolicy NpSyncQoSHwMapClassToPolicy, UINT4 u4AppId, UINT4 u4NpApiId));

typedef struct {    
    tQoSNpSyncClassMapNode  ClassMapNode;
    UINT4 u4PlyMapId;
} tNpSyncQoSHwUpdatePolicyMapForClass;

VOID NpSyncQoSHwUpdatePolicyMapForClassSync PROTO ((tNpSyncQoSHwUpdatePolicyMapForClass NpSyncQoSHwUpdatePolicyMapForClass, UINT4 u4AppId, UINT4 u4NpApiId));

typedef struct {    
    tQoSNpSyncClassMapNode  ClassMapNode;
    UINT4 u4PlyMapId;
} tNpSyncQoSHwUnmapClassFromPolicy;


VOID NpSyncQoSHwUnmapClassFromPolicySync PROTO ((tNpSyncQoSHwUnmapClassFromPolicy NpSyncQoSHwUnmapClassFromPolicy, UINT4 u4AppId, UINT4 u4NpApiId));

typedef struct {
   tQoSNpSyncClassMapNode  ClassMapNode;
} tNpSyncQoSHwDeleteClassMapEntry;

VOID NpSyncQoSHwDeleteClassMapEntrySync PROTO ((tNpSyncQoSHwDeleteClassMapEntry NpSyncQoSHwDeleteClassMapEntry, UINT4 u4AppId, UINT4 u4NpApiId));

typedef struct {
   INT4  u4MeterId;
} tNpSyncQoSHwMeterCreate;

VOID NpSyncQoSHwMeterCreateSync PROTO ((tNpSyncQoSHwMeterCreate NpSyncQoSHwMeterCreate, UINT4 u4AppId, UINT4 u4NpApiId));

typedef struct {
    INT4  u4MeterId;
} tNpSyncQoSHwMeterDelete;

VOID NpSyncQoSHwMeterDeleteSync PROTO ((tNpSyncQoSHwMeterDelete NpSyncQoSHwMeterDelete, UINT4 u4AppId, UINT4 u4NpApiId));

typedef struct {
   INT4   u4IfIndex;
    UINT4  u4SchedId;
} tNpSyncQoSHwSchedulerAdd;

VOID NpSyncQoSHwSchedulerAddSync PROTO ((tNpSyncQoSHwSchedulerAdd NpSyncQoSHwSchedulerAdd, UINT4 u4AppId, UINT4 u4NpApiId));

typedef struct {
     INT4   u4IfIndex;
    UINT4  u4SchedId;
} tNpSyncQoSHwSchedulerUpdateParams;

VOID NpSyncQoSHwSchedulerUpdateParamsSync PROTO ((tNpSyncQoSHwSchedulerUpdateParams NpSyncQoSHwSchedulerUpdateParams, UINT4 u4AppId, UINT4 u4NpApiId));

typedef struct {
    INT4   u4IfIndex;
    UINT4  u4SchedId;
} tNpSyncQoSHwSchedulerDelete;

VOID NpSyncQoSHwSchedulerDeleteSync PROTO ((tNpSyncQoSHwSchedulerDelete NpSyncQoSHwSchedulerDelete, UINT4 u4AppId, UINT4 u4NpApiId));

typedef struct {
    INT4   u4IfIndex;
    UINT4  u4Id;
} tNpSyncQoSHwQueueCreate;

VOID NpSyncQoSHwQueueCreateSync PROTO ((tNpSyncQoSHwQueueCreate NpSyncQoSHwQueueCreate, UINT4 u4AppId, UINT4 u4NpApiId));

typedef struct {
    INT4   u4IfIndex;
    UINT4  u4Id;
} tNpSyncQoSHwQueueDelete;

VOID NpSyncQoSHwQueueDeleteSync PROTO ((tNpSyncQoSHwQueueDelete NpSyncQoSHwQueueDelete, UINT4 u4AppId, UINT4 u4NpApiId));

typedef struct {
    INT4   u4IfIndex;
    INT4   u4ClsOrPriType;
    UINT4  u4ClsOrPri;
    UINT4  u4QId;
    UINT1  u1Flag;
    UINT1  au1Pad[3];
} tNpSyncQoSHwMapClassToQueue;

VOID NpSyncQoSHwMapClassToQueueSync PROTO ((tNpSyncQoSHwMapClassToQueue NpSyncQoSHwMapClassToQueue, UINT4 u4AppId, UINT4 u4NpApiId));

typedef struct {
    INT4   u4IfIndex;
    UINT4  u4SchedId;
    UINT4  u4NextSchedId;
    UINT4  u4NextQId;
    INT2   u2HL;
    UINT2  u2Sweight;
    UINT1  u1Spriority;
    UINT1  u1Flag;
    UINT1  au1Pad[2];
} tNpSyncQoSHwSchedulerHierarchyMap;

VOID NpSyncQoSHwSchedulerHierarchyMapSync PROTO ((tNpSyncQoSHwSchedulerHierarchyMap NpSyncQoSHwSchedulerHierarchyMap, UINT4 u4AppId, UINT4 u4NpApiId));

typedef struct {
    INT4  u4Port;
    INT4  u4DefPriority;
} tNpSyncQoSHwSetDefUserPriority;

VOID NpSyncQoSHwSetDefUserPrioritySync PROTO ((tNpSyncQoSHwSetDefUserPriority NpSyncQoSHwSetDefUserPriority, UINT4 u4AppId, UINT4 u4NpApiId));

typedef struct {
    UINT4    u4ClassMapId;
    INT4                 u4IfIndex;
    INT4                 u4ClsOrPriType;
    UINT4                u4ClsOrPri;
    UINT4                u4QId;
    UINT1                u1Flag;
    UINT1                au1Pad[3];
} tNpSyncQoSHwMapClassToQueueId;

VOID NpSyncQoSHwMapClassToQueueIdSync PROTO ((tNpSyncQoSHwMapClassToQueueId NpSyncQoSHwMapClassToQueueId, UINT4 u4AppId, UINT4 u4NpApiId));
typedef struct {   
 INT4   i4IfIndex;
 UINT1  u1PfcPriority;
 UINT1  u1Profile;
 UINT1  u1Flag;
   UINT1  au1Pad[1];
 } tNpSyncFsQosHwConfigPfc;
VOID NpSyncFsQosHwConfigPfcSync PROTO ((tNpSyncFsQosHwConfigPfc NpSyncFsQosHwConfigPfc, UINT4 u4AppId, UINT4 u4NpApiId));

typedef struct {
    INT4   i4CpuQueueId;
    UINT4  u4MinRate;
    UINT4  u4MaxRate;
} tNpSyncQoSHwSetCpuRateLimit;

VOID NpSyncQoSHwSetCpuRateLimitSync PROTO ((tNpSyncQoSHwSetCpuRateLimit NpSyncQoSHwSetCpuRateLimitSync, UINT4 u4AppId, UINT4 u4NpApiId));

/*End of QOS Hw Audit definitions and proto type declarations*/

/* ELPS Np sync-up Structures */
typedef struct {
    UINT4 u4ContextId; /* Context Identifier */
    UINT4 u4PgId; /* Protection Group Identifier */
    UINT4 u4PgStatus; /* Action of NP Call - Create/Delete/Switch to working/
                         switch to protection */
} tElpsNpSyncPgEntry;

typedef struct {
    UINT4 u4ContextId; /* Context Identifier */
    UINT4 u4ModStatus; /* Module Status - ShutDown/Enable/Disable */
} tElpsNpSyncCxtEntry;

typedef struct {
    INT4                 i4IssL2FilterNo;
    tIssFilterShadowTable  IssFilterShadowTable;
    INT4                 i4Value;
} tNpSyncIssHwUpdateL2Filter;

VOID NpSyncIssHwUpdateL2FilterSync PROTO ((tNpSyncIssHwUpdateL2Filter NpSyncIssHwUpdateL2Filter, UINT4 u4AppId, UINT4 u4NpApiId, UINT4 u4Status));

typedef struct {
    INT4                 i4IssL3FilterNo;
    tIssFilterShadowTable  IssFilterShadowTable;
    INT4                 i4Value;
} tNpSyncIssHwUpdateL3Filter;

VOID NpSyncIssHwUpdateL3FilterSync PROTO ((tNpSyncIssHwUpdateL3Filter NpSyncIssHwUpdateL3Filter, UINT4 u4AppId, UINT4 u4NpApiId, UINT4 u4Status));

typedef struct {
    INT4   i4RateLimitVal;
    UINT4  u4IfIndex;
    UINT1  u1PacketType;
    UINT1  au1Pad[3];
} tNpSyncIssHwSetRateLimitingValue;

VOID NpSyncIssHwSetRateLimitingValueSync PROTO ((tNpSyncIssHwSetRateLimitingValue NpSyncIssHwSetRateLimitingValue, UINT4 u4AppId, UINT4 u4NpApiId));

typedef struct {
    UINT4  u4IfIndex;
    INT4   i4PktRate;
    INT4   i4BurstRate;
} tNpSyncIssHwSetPortEgressPktRate;

VOID NpSyncIssHwSetPortEgressPktRateSync PROTO ((tNpSyncIssHwSetPortEgressPktRate NpSyncIssHwSetPortEgressPktRate, UINT4 u4AppId, UINT4 u4NpApiId));


typedef struct {
    tIssHwUpdtPortIsolation IssHwUpdtPortIsolation;
} tNpSyncIssHwConfigPortIsolationEntry;

VOID NpSyncIssHwConfigPortIsolationEntrySync 
     PROTO ((tNpSyncIssHwConfigPortIsolationEntry NpSyncIssHwConfigPortIsolationEntry, 
             UINT4 u4AppId, UINT4 u4NpApiId));

typedef union {
    tNpSyncPortIdx PortIdx;
    tNpSyncVlanIdx VlanIdx;
    tNpSyncContextIdx ContextIdx;
    tNpSyncPbbPortIdx PbbPortIdx;
    tNpSyncPbbGlbOui PbbGlbOui;
    tNpSyncPbbPortProperty PbbPortProperty;
    tNpSyncIsidIdx IsidIdx;
    tElpsNpSyncCxtEntry ElpsNpSyncCxtEntry;
    tElpsNpSyncPgEntry ElpsNpSyncPgEntry;
    tNpSyncFsMiPbbHwSetAllToOneBundlingService FsMiPbbHwSetAllToOneBundlingService;
    tNpSyncFsMiPbbHwDelAllToOneBundlingService FsMiPbbHwDelAllToOneBundlingService;
    tNpSyncFsCfaHwCreateInternalPort FsCfaHwCreateInternalPort;
    tNpSyncFsCfaHwDeleteInternalPort FsCfaHwDeleteInternalPort;
    tNpSyncFsCfaHwCreateILan FsCfaHwCreateILan;
    tNpSyncFsCfaHwDeleteILan FsCfaHwDeleteILan;
    tNpSyncFsCfaHwRemovePortFromILan FsCfaHwRemovePortFromILan;
    tNpSyncFsCfaHwAddPortToILan FsCfaHwAddPortToILan;
    tNpSyncFsMiPbbTeHwSetEspVid FsMiPbbTeHwSetEspVid;
    tNpSyncFsMiPbbTeHwResetEspVid FsMiPbbTeHwResetEspVid;
    tNpSyncQoSHwMapClassToPolicy QoSHwMapClassToPolicy;
    tNpSyncQoSHwUpdatePolicyMapForClass QoSHwUpdatePolicyMapForClass;
    tNpSyncQoSHwUnmapClassFromPolicy QoSHwUnmapClassFromPolicy;
    tNpSyncQoSHwDeleteClassMapEntry QoSHwDeleteClassMapEntry;
    tNpSyncQoSHwMeterCreate QoSHwMeterCreate;
    tNpSyncQoSHwMeterDelete QoSHwMeterDelete;
    tNpSyncQoSHwSchedulerAdd QoSHwSchedulerAdd;
    tNpSyncQoSHwSchedulerUpdateParams QoSHwSchedulerUpdateParams;
    tNpSyncQoSHwSchedulerDelete QoSHwSchedulerDelete;
    tNpSyncQoSHwQueueCreate QoSHwQueueCreate;
    tNpSyncQoSHwQueueDelete QoSHwQueueDelete;
    tNpSyncQoSHwMapClassToQueue QoSHwMapClassToQueue;
    tNpSyncQoSHwSchedulerHierarchyMap QoSHwSchedulerHierarchyMap;
    tNpSyncQoSHwSetDefUserPriority QoSHwSetDefUserPriority;
    tNpSyncQoSHwMapClassToQueueId QoSHwMapClassToQueueId;
    tNpSyncFsQosHwConfigPfc FsQosHwConfigPfc;
    tNpSyncIssHwUpdateL2Filter IssHwUpdateL2Filter;
    tNpSyncIssHwUpdateL3Filter IssHwUpdateL3Filter;
    tNpSyncIssHwSetRateLimitingValue IssHwSetRateLimitingValue;
    tNpSyncIssHwSetPortEgressPktRate IssHwSetPortEgressPktRate;    
    tNpSyncFsErpsHwRingConfig FsErpsHwRingConfig;
    tNpSyncIssHwConfigPortIsolationEntry IssHwConfigPortIsolationEntry;
    tNpSyncQoSHwSetCpuRateLimit QoSHwSetCpuRateLimit;
}unNpSync;

typedef struct NpSyncBufferEntry
{
    tTMO_SLL_NODE Node;
    unNpSync      unNpData;
    UINT4         u4NpApiId;
    UINT4         u4EventId;
}tNpSyncBufferEntry;

#define NPSYNC_MESSAGE             100
#define EVENTSYNC_MESSAGE          101
#define NPSYNC_MSGTYPE_SIZE        1
#define NPSYNC_MSG_LENGTH          2
#define NPSYNC_MSG_NPAPI_ID_SIZE   4 

/* Macros to write in to RM buffer. */
#define NPSYNC_RM_PUT_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do {\
    RM_DATA_ASSIGN_1_BYTE (pMsg, *(pu4Offset), u1MesgType);\
        *(pu4Offset) += 1;\
}while (0)
#define NPSYNC_RM_PUT_2_BYTE(pMsg, pu4Offset, u2MesgType)\
do {\
    RM_DATA_ASSIGN_2_BYTE (pMsg, *(pu4Offset), u2MesgType);\
        *(pu4Offset) += 2;\
}while (0)
#define NPSYNC_RM_PUT_4_BYTE(pMsg, pu4Offset, u4MesgType)\
do {\
    RM_DATA_ASSIGN_4_BYTE (pMsg, *(pu4Offset), u4MesgType);\
        *(pu4Offset) += 4;\
}while (0)
#define NPSYNC_RM_PUT_N_BYTE(pdest, psrc, pu4Offset, u4Size)\
do {\
    RM_COPY_TO_OFFSET(pdest, psrc, *(pu4Offset), u4Size);\
        *(pu4Offset) +=u4Size;\
}while (0)
#define NPSYNC_RM_GET_1_BYTE(pMsg, pu4Offset, u1MesgType)\
do {\
    RM_GET_DATA_1_BYTE (pMsg, *(pu4Offset), u1MesgType);\
        *(pu4Offset) += 1;\
}while (0)
#define NPSYNC_RM_GET_2_BYTE(pMsg, pu4Offset, u2MesgType)\
do {\
    RM_GET_DATA_2_BYTE (pMsg, *(pu4Offset), u2MesgType);\
        *(pu4Offset) += 2;\
}while (0)
#define NPSYNC_RM_GET_4_BYTE(pMsg, pu4Offset, u4MesgType)\
do {\
    RM_GET_DATA_4_BYTE (pMsg, *(pu4Offset), u4MesgType);\
        *(pu4Offset) += 4;\
}while (0)
#define NPSYNC_RM_GET_N_BYTE(psrc, pdest, pu4Offset, u4Size)\
do {\
    RM_GET_DATA_N_BYTE (psrc, pdest, *(pu4Offset), u4Size);\
        *(pu4Offset) += u4Size;\
}while (0)
#endif /*_HWAUD_H */
