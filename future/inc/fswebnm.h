/********************************************************************

 * $Id: fswebnm.h,v 1.75 2017/12/11 10:02:03 siva Exp $
 *
 * Description: Webnm module exported macros, typedefs and prototypes.
 *******************************************************************/
#ifndef _FSWEBNM_H
#define _FSWEBNM_H

#include "fsvlan.h"
#ifdef PBB_WANTED
#include "pbb.h"
#endif
#include "params.h"
#include "l2iwf.h"
#include "iss.h"

/* ENM Data Types */
#define                   ENM_UINTEGER32       1
#define                   ENM_INTEGER32        2
#define                   ENM_INTEGER          2
#define                   ENM_GAUGE            3
#define                   ENM_TIMETICKS        4
#define                   ENM_COUNTER64        5
#define                   ENM_IPADDRESS        6
#define                   ENM_OCTETSTRING      7
#define                   ENM_OIDTYPE          8
#define                   ENM_OBJECTIDENTIFIER 8
#define                   ENM_DISPLAYSTRING    9
#define                   ENM_COUNTER32        10
#define                   ENM_COUNTER          10
#define                   ENM_BITS             11
#define                   ENM_HEX32            12
#define                   ENM_IMP_DISPLAYSTRING 13
#define                   ENM_STR_IPADDRESS     14
#define                   ENM_PORT_LIST         15
#define                   ENM_OCTETBITS         16
#define                   ENM_IMP_OIDTYPE       17
/* Enm Sizing Parameter   */
#define                   ENM_MAX_SOC_BUF_LEN  1024
#define                   ENM_MAX_URL_LEN      1024
#define                   ENM_MAX_NAME_LEN     1024
#define                   ENM_MAX_AUTH_HEADER_LEN 128
#define                   ENM_MAX_HEADER_LENGTH 512
#define                   ENM_MAX_SOC_BUFFER_LENGTH  128
#define                   ENM_MAX_METHOD_LENGTH 8
#define                   ENM_MAX_NAME_LENGTH   200

/*Http authentication related definitions*/
#define                   ENM_MAX_BASE64_LEN        128
#define                   ENM_MAX_NONCE_LEN         (32+4)
#define                   ENM_MAX_USERNAME_LEN      64
#define                   ENM_MAX_PASSWORD_LEN      64
#define                   ENM_MAX_REALM_LEN         128
#define                   ENM_MAX_METHOD_LEN        12
#define                   ENM_MAX_DIGEST_LEN        (32+4)
#define                   ENM_MAX_STALE_LEN         8
#define                   ENM_MAX_STALEFLG_LEN      16
#define                   ENM_MAX_ALGO_LEN          12
#define                   ENM_MAX_CNONCE_LEN        (128)
#define                   ENM_MAX_QOP_LEN           12
#define                   ENM_MAX_NC_LEN            (8+4)
#define                   ENM_MAX_TIME_LEN          12
#define                   ENM_MAX_HA1_LEN           32
#define                   ENM_MAX_HA2_LEN           32

/* MACROs used in web and webnm modules*/
#define READONLY      1 
#define READWRITE     2 
#define NOACCESS      3 
#define READCREATE    4 
#define INDEX         5 

#define WEB_MAX_LEN_FOR_PAGE    5000 

#define MAX_NUM_PROCESS      3

#define WEBNM_MEMBLK_SIZE 1024
#define WEBNM_MEMBLK_COUNT 10
#define WEBNM_INIT_VAL 0
#define BUFFER_SIZE WEBNM_MEMBLK_SIZE
/*Memory pool related macros*/
#define   WEBNM_MEMORY_TYPE      MEM_DEFAULT_MEMORY_TYPE

/* Macros for creating memory pools */

#define   WEBNM_CREATE_MEM_POOL(x,y,pPoolId)\
             MemCreateMemPool(x,y,WEBNM_MEMORY_TYPE,(UINT4 *)pPoolId)


#define WEB_CONVERT_IPADDR_TO_STR(pString,u4Value)\
{\
    tUtlInAddr      IpAddr;\
\
     IpAddr.u4Addr = OSIX_NTOHL ((UINT4)u4Value);\
\
      pString = (UINT1 *)UtlInetNtoa(IpAddr);\
\
}

/* Success/Failure Macros */
#define                   ENM_SUCCESS          SUCCESS
#define                   ENM_FAILURE          FAILURE
#define                   ENM_NO_DATA          -2

#define  WENM_MAX_MULTI_DATA                 40
#define  NO_SUCH_OBJECT                      128 
#define  NO_SUCH_INSTANCE                    129
#define  ENM_MAX_ETAG_LEN                    24

/* String Macros          */
#ifdef WEBNM_WANTED
#define                   HTTP_STRLEN(x)       Httpstrlen((UINT1 *)x)
#define                   HTTP_STRCPY(x,y)     Httpstrcpy((UINT1*)x, (UINT1*)y)
#define                   HTTP_STRCAT(x,y)     Httpstrcat((UINT1*)x, (UINT1*)y)
#define                   HTTP_STRNCAT(x,y,z)  HttpStrnCat((UINT1*)x, (UINT1*)y, (UINT4)z)
#else
#define                   HTTP_STRLEN(x)       STRLEN((UINT1 *)x)
#define                   HTTP_STRCPY(x,y)     STRCPY((UINT1*)x, (UINT1*)y)
#define                   HTTP_STRCAT(x,y)     STRCAT((UINT1*)x, (UINT1*)y)
#define                   HTTP_STRNCAT(x,y,z)  HttpStrnCat((UINT1*)x, (UINT1*)y, (UINT4)z)
#endif /* WEBNM_WANTED */

#define                   WEBNM_SYSLOG_ID     WebnmGetWebSysLogId()

#define                   ENM_IPADDRESS_LEN    4
#define                   ENM_BYTE_LEN         8

#define                   ENM_HEX_DATA         0
#define                   ENM_DEC_DATA         1

#define HTTP_TASK_NAME                "HST"
#define HTTPS_TASK_NAME_PREFIX        "H"
#define HTTP_MAX_URL_LEN       100

#define ISS_GET                       1
#define ISS_SET                       2 

extern UINT1 ISS_TABLE_FLAG[];
extern UINT1 ISS_TABLE_STOP_FLAG[];
extern INT4  HttpGetValuebyName PROTO ((UINT1 *, UINT1 *, UINT1 *));

/* Trace message Macro */
#define HTTP_TRC        gu4HttpMask

#define   SESSION_TRC_FLG  gi4HttpsTrcLevel

#ifdef TRACE_WANTED
#define ENM_TRACE(x)      MOD_TRC(HTTP_TRC,1,"WEBNM",x)
#define ENM_TRACE1(x,y)   MOD_TRC_ARG1(HTTP_TRC,1,"WEBNM",x,y)
#define ENM_TRACE2(x,y,z) MOD_TRC_ARG2(HTTP_TRC,1,"WEBNM",x,y,z)
#define WEB_SESSION_TRACE(mod, modname, fmt) \
                        MOD_TRC_ARG(SESSION_TRC_FLG, mod, modname, fmt)
#define WEB_SESSION_TRACE1(mod, modname, fmt, Arg) \
                        MOD_TRC_ARG1(SESSION_TRC_FLG, mod, modname, fmt, Arg)
#else
#define ENM_TRACE(x)
#define ENM_TRACE1(x,y)
#define ENM_TRACE2(x,y,z)
#define WEB_SESSION_TRACE(mod, modname, fmt) 
#define WEB_SESSION_TRACE1(mod, modname, fmt, Arg) 
#endif
#ifdef VRRP_WANTED
#define WEB_INET_ATON6(s, pin)  UtlInetAton6((const CHR1 *)s, (tUtlIn6Addr *)pin)
#define SYSLOG_NTOHL   (UINT4 )OSIX_NTOHL
#endif

#define HTTP_DBG_TRC (mask, fmt)\
      MOD_TRC(HTTP_TRC, mask, "WEBNM", fmt)
#define HTTP_DBG_TRC1(mask,fmt,arg1)\
      MOD_TRC_ARG1(HTTP_TRC,mask,"WEBNM",fmt,arg1)
#define HTTP_DBG_TRC2(mask,fmt,arg1,arg2)\
      MOD_TRC_ARG2(HTTP_TRC,mask,"WEBNM",fmt,arg1,arg2)
#define HTTP_DBG_TRC3(mask,fmt,arg1,arg2,arg3)\
      MOD_TRC_ARG3(HTTP_TRC,mask,"WEBNM",fmt,arg1,arg2,arg3)
#define HTTP_DBG_TRC4(mask,fmt,arg1,arg2,arg3,arg4)\
      MOD_TRC_ARG4(HTTP_TRC,mask,"WEBNM",fmt,arg1,arg2,arg3,arg4)
#define HTTP_DBG_TRC5(mask,fmt,arg1,arg2,arg3,arg4,arg5)\
      MOD_TRC_ARG5(HTTP_TRC,mask,"WEBNM",fmt,arg1,arg2,arg3,arg4,arg5)
#define HTTP_DBG_TRC6(mask,fmt,arg1,arg2,arg3,arg4,arg5,arg6)\
      MOD_TRC_ARG6(HTTP_TRC,mask,"WEBNM",fmt,arg1,arg2,arg3,arg4,arg5,arg6)

enum {
    ISS_WEB_WAN_TYPE_PRIVATE = 1,
    ISS_WEB_WAN_TYPE_PUBLIC,
    ISS_WEB_WAN_TYPE_ALL
};
enum {
    ISS_WEB_NETWORK_TYPE_LAN = 1,
    ISS_WEB_NETWORK_TYPE_WAN,
    ISS_WEB_NETWORK_TYPE_ALL
};

/* Authentication scheme values */
enum {
         HTTP_AUTH_DEFAULT = 0,
         HTTP_AUTH_BASIC,
         HTTP_AUTH_DIGEST
};

/* The Global Nonce array */
typedef struct HttpServerNonceInfo{
  UINT4 u4TimeInSecs;                  /* Time stamp */
  UINT1 au1Nonce[ENM_MAX_NONCE_LEN];   /* Nonce value */
}tHttpServerNonceInfo;

/* Data structure to Store Http Connection informations */
typedef struct THTTP{
  tOsixTaskId    ProcessingTaskId;     /* The task id of the process          */
  tSNMP_OID_TYPE *pOid;                /* Pointer to the Oid List             */
  INT4 (*fpLock)(VOID);                /* fpLock, fpUnLock - Protocol         */
                                       /* Lock/UnLock fns                     */
  INT4 (*fpUnLock)(VOID);
  INT4 (*fpSelectContext)(UINT4);      /* Protocol Select and Release         */
  INT4 (*fpReleaseContext)(VOID);      /* Context Functions Pointers          */
  INT1  *pi1Html;                      /* Pointer to the Html Buffer          */
  UINT1 *pu1Type;                      /* Access Type of Each Element         */
  UINT1 *pu1Access;                    /* Access Type of Each Element         */
  UINT1 *pu1Buff;
  UINT1 *pu1BuffCurr;
  VOID  *pSslConnId;
  INT1 ai1Url[ENM_MAX_URL_LEN];        /* Url buffer                          */
  INT1 ai1Sockbuffer[ENM_MAX_SOC_BUF_LEN]; 
                                       /* Socket buffer                       */ 
  INT1 ai1Version[ENM_MAX_NAME_LEN];   /* HTTP Version Number                 */
  INT1 ai1HtmlName[ENM_MAX_NAME_LEN];  /* Name of the Html page               */
  INT1 ai1StaticSockbuffer[ENM_MAX_SOC_BUF_LEN];
                                       /* When MultiPartData arrives          */
                                       /* store fully in a static memory      */
                                       /* to avoid partial reading during     */
                                       /* handling of Multidata               */

  /*HTTP authentication related params*/
  INT1 ai1Authorisation[ENM_MAX_NAME_LEN]; /* Authorisation header            */
  INT1 ai1Username[ENM_MAX_USERNAME_LEN];  /* Username                        */
  INT1 ai1Password[ENM_MAX_PASSWORD_LEN];  /* Password sent/derived           */
  UINT1 au1Realm[ENM_MAX_REALM_LEN];       /* Realm                           */
  UINT1 au1Uri[ENM_MAX_URL_LEN];           /* Uri sent in Authorisation header*/
                                           /* for digest auth                 */
  UINT1 au1RequestDigest[ENM_MAX_DIGEST_LEN]; 
                                           /* Request digest sent in          */
                                           /* Authorisation header for        */
                                           /* digest auth                     */
  UINT1 au1Algorithm[ENM_MAX_ALGO_LEN];    /* Algorithm sent in               */
                                           /* Authorisation header            */
                                           /* for digest auth                 */
  UINT1 au1Cnonce[ENM_MAX_CNONCE_LEN];     /* Cnonce sent in Authorisation    */
                                           /* header for digest auth          */
  UINT1 au1Snonce[ENM_MAX_NONCE_LEN];      /* Server generated nonce sent in  */
                                           /* Authorisation header            */
                                           /* for digest auth                 */
  UINT1 au1ResponseQop[ENM_MAX_QOP_LEN];   /* QoP sent in Authorisation header*/
                                           /* for digest auth                 */
  UINT1 au1NonceCount[ENM_MAX_NC_LEN];     /* Nonce Count value sent in       */
                                           /* Authorisation header            */
                                           /* for digest auth                 */ 
  UINT1 au1HA1Digest[ENM_MAX_DIGEST_LEN];  /* Contains the computed HA1       */ 
                                           /* value for the                   */
                                           /* given realm and username        */
  UINT1 au1Digest[ENM_MAX_DIGEST_LEN];     /* Calculated Request/Response     */ 
                                           /* digest                          */
  UINT1 au1Stale[ENM_MAX_STALE_LEN];       /* The stale flag to               */
                                           /* indicate nonce expiry           */
  /*End of HTTP Auth params*/

  UINT1  au1PostQuery[ENM_MAX_NAME_LEN];   /* Array contains query message    */
                                           /* from client. This is required   */
                                           /* only for webnm                  */
  UINT1 au1Name[ENM_MAX_NAME_LEN];
  UINT1 au1Value[ENM_MAX_NAME_LEN];
  UINT1 au1DataString[ENM_MAX_NAME_LEN];
  UINT1 au1Instance[ENM_MAX_NAME_LEN];
  UINT1 au1KeyString[ENM_MAX_NAME_LEN];
  UINT1 au1Array[ENM_MAX_NAME_LEN];
  UINT1 au1Etag [ENM_MAX_ETAG_LEN];        /* Entity Tag a unique identifier  */
                                           /* for a resource                  */
  UINT1 au1RedirectLocation [HTTP_MAX_URL_LEN]; 
  INT4 i4Sockid;                           /* Socket ID                       */
  INT4 i4Left;                             /* Remaining data in the socket 
                                              buffer                          */
  INT4 i4Read;                             /* Read Offset in socket buffer    */
  INT4 i4Write;                            /* Webnm Write Offset              */
  INT4 i4Objreq;                           /* Request Flag for Webnm or Http  */
  INT4 i4Method;                           /* GET/POST method                 */
  INT4 i4AuthScheme;                       /* The authentication scheme       */
                                           /* used in the request             */
  INT4 i4HtmlSize;                         /* Html Page Buffer Size           */
  INT4 i4ContentLen;                       /* Html Page Buffer Size           */
  INT4 i4ResHeadsize;                      /* Response Header Size            */
  INT4 i4MibType;                          /* Scalar or Table                 */
  INT4 i4NoOfSnmpOid;                      /* No. of SNMP MIB Objects in 
                                              the file                        */
  UINT4 u4GroupID;                         /* User Group ID                   */
  UINT4 u4PageAccess;                      /* Access permission in the page   */
  UINT4 i4ResType;
  UINT2 u2RespCode;                        /* Response Code to be 
                                              send in response                */
  UINT2 u2ResourceType;
  UINT1 u1RequestType;                     /* HTTP / HTTPS Request.           */
  UINT1 u1IsMgmtLock;
  BOOL1 bConnClose;                        /* Connection flag used to         */
                                           /* signal the Main Task to close   */
                                           /* the socket                      */ 
  BOOL1 bIsReqPipelined;                   /* flag to know whether            */ 
                                           /* request is pipelined or not     */
  BOOL1 bIsMultiData;                      /* Flag to indicate buffer         */
                                           /* is of MultiData type            */
  UINT1 au1Padding[3];                     /* Padding.                        */
} tHttp;

typedef struct HttpMsgNode {

    tHttp           *pHttp;
    INT4            i4ContentLen;
    UINT4           u4IpAddr;

}tHttpMsgNode;

typedef struct IssWebSystem {
    BOOL1 b1System;
    BOOL1 b1SysInfo;
    BOOL1 b1SysResource;
    BOOL1 b1SysNVRAMSetting;
    BOOL1 b1SysCPUOverloadProtect;
    BOOL1 b1SysACL;
    BOOL1 b1SysQoSInGress;
    BOOL1 b1SysQoSEgress;
    BOOL1 b1SysDCB;
    BOOL1 b1SysCN;
    BOOL1 b1SysDCBX;
    BOOL1 b1SysIPAuthorizedManager;
    BOOL1 b1SysFIPS;
    BOOL1 b1SysPortIsolation;
    BOOL1 b1SysSaveAndRestore;
    BOOL1 b1SysLogTransfer;
    BOOL1 b1SysImageDownload;
    BOOL1 b1SysFileTransfer;
    BOOL1 b1SysAuditLog;
    BOOL1 b1SysReboot;
    BOOL1 b1SysTACACS;
    BOOL1 b1SysSNTP;
    BOOL1 b1SysSSH;
    BOOL1 b1SysSSL;
    BOOL1 b1SysMIB;
    BOOL1 b1SysHTTP;
    BOOL1 b1SysBeep;
    BOOL1 b1SysSNMP;
    BOOL1 b1SysSYSLOG;
    BOOL1 b1SysRM;
    UINT1 au1Padding[2];
}tIssWebSystem;

typedef struct IssWebLayer2Mgmt {
    BOOL1  b1Layer2Mgmt;
    BOOL1  b1L2PortManager;
    BOOL1  b1L2VLAN;
    BOOL1  b1L2GARP;
    BOOL1  b1L2DynamicVLAN;
    BOOL1  b1L2MSTP;
    BOOL1  b1L2RSTP;
    BOOL1  b1L2LA;
    BOOL1  b1L2LLDP;
    BOOL1  b1L2PBVLAN;
    BOOL1  b1L2PBVLANExt;
    BOOL1  b1L28021x;
    BOOL1  b1L2Filters;
    BOOL1  b1L2Mirroring;
    BOOL1  b1L2PVRST;
    BOOL1  b1L2MRP;
    BOOL1  b1L2ELMI;
    BOOL1  b1L2ECFM;
    BOOL1  b1L2ERPS;
    BOOL1  b1L2ELPS;
    BOOL1  b1L2VXLAN;
    BOOL1  b1L2SH;
    BOOL1  b1L2UFD;
}tIssWebLayer2Mgmt;

typedef struct IssWebLayer3Mgmt {
    BOOL1  b1Layer3Mgmt;
    BOOL1  b1L3IP;
    BOOL1  b1L3IPContd;
    BOOL1  b1L3IPv6;
    BOOL1  b1L3IPSecv6;
    BOOL1  b1L3Tunnel;
    BOOL1  b1L3DHCPServer;
    BOOL1  b1L3DHCPRelay;
    BOOL1  b1L3DHCPClient;
    BOOL1  b1L3RIP;
    BOOL1  b1L3RIP6;
    BOOL1  b1L3RouteMap;
    BOOL1  b1L3OSPF;
    BOOL1  b1L3OSPF3;
    BOOL1  b1L3DHCP6Client;
    BOOL1  b1L3DHCP6Relay;
    BOOL1  b1L3DHCP6Server;
    BOOL1  b1L3ISIS;
    BOOL1  b1L3BGP;
    BOOL1  b1L3RRD;
    BOOL1  b1L3MPLS;
    BOOL1  b1L3RRD6;
    BOOL1  b1L3VRRP;
    BOOL1  b1L3NAT;
    BOOL1  b1L3DNS;
    BOOL1  b1L3Filtering;
    BOOL1  b1L3BFD;

}tIssWebLayer3Mgmt;

typedef struct IssWebLayer4Mgmt {
    BOOL1  b1Layer4Mgmt;
}tIssWebLayer4Mgmt;

typedef struct IssWebNetworkOverlay {
    BOOL1  b1NetworkOverlay;
    BOOL1  b1NetOverlayVXLAN;
}tIssWebNetworkOverlay;


typedef struct IssWebMEFconformance {
    BOOL1  b1MEFconformance;
}tIssWebMEFconformance;

typedef struct IssWebMulticast{
    BOOL1  b1Multicast;
    BOOL1  b1MultiIGMPSnoop;
    BOOL1  b1MultiMLDSnoop;
    BOOL1  b1MultiDynamicMulticast;
    BOOL1  b1MultiIGMP;
    BOOL1  b1MultiMLD;
    BOOL1  b1MultiIGMPProxy;
    BOOL1  b1MultiPIM;
    BOOL1  b1MultiMSDP;
    BOOL1  b1MultiDVMRP;
    BOOL1  b1MultiIPv4;
    BOOL1  b1MultiTAC;
}tIssWebMulticast;

typedef struct IssWebStatistics{
    BOOL1  b1Statistics;
    BOOL1  b1StatInterface;
    BOOL1  b1StatTCPUDPStats;
    BOOL1  b1StatIPv6;
    BOOL1  b1StatBridge;
    BOOL1  b1StatVLAN;
    BOOL1  b1StatMSTP;
    BOOL1  b1StatRSTP;
    BOOL1  b1StatPVRST;
    BOOL1  b1StatPBRSTP;
    BOOL1  b1StatLA;
    BOOL1  b1StatLLDP;
    BOOL1  b1Stat8021x;
    BOOL1  b1StatRadius;
    BOOL1  b1StatMRP;
    BOOL1  b1StatQoS;
    BOOL1  b1StatIGMPSnooping;
    BOOL1  b1StatMLDSnooping;
    BOOL1  b1StatIP;
    BOOL1  b1StatRIP;
    BOOL1  b1StatRIP6;
    BOOL1  b1StatIPSECv6;
    BOOL1  b1StatOSPF;
    BOOL1  b1StatVRRP;
    BOOL1  b1StatIGMP;
    BOOL1  b1StatMLD;
    BOOL1  b1StatIGMPProxy;
    BOOL1  b1StatIPv4Multicasting;
    BOOL1  b1StatRMON;
    BOOL1  b1StatEthernetOAM;
    BOOL1  b1StatDCB;
    BOOL1  b1StatDNS;
    BOOL1  b1StatELMI;
    BOOL1  b1StatPTP;
    BOOL1  b1StatECFM;
    BOOL1  b1StatBGP;
    BOOL1  b1StatOSPF3;
    BOOL1  b1StatDHCP6Client;
    BOOL1  b1StatDHCP6Relay;
    BOOL1  b1StatDHCP6Server;
    BOOL1  b1StatVPN;
    BOOL1  b1StatFWL;
    BOOL1  b1StatHTTP;
    BOOL1  b1StatSNMP;
    BOOL1  b1Monitor;
    BOOL1  b1Radios;
    UINT1  au1Pad[2];
}tIssWebStatistics;

typedef struct IssWebOthers{
  BOOL1              b1Context;
  BOOL1              b1EthernetOAM;
  BOOL1              b1RMON;
  BOOL1              b1RMONv2;
  BOOL1              b1Clock;
  BOOL1              b1PTP;
  BOOL1              b1DSMON;
  BOOL1              b1Stacking;
  BOOL1              b1SecurityMgmt;
  BOOL1              b1Firewall;
  BOOL1              b1VPN;
  BOOL1              b1IDS;
}tIssWebOthers;

typedef struct IssWebWss{
BOOL1               b1Wss;
BOOL1               b1Capwap;
BOOL1               b1Wlan;
BOOL1               b1WlanHome;
BOOL1               b1WlanProfiles;
BOOL1               b1WlanRsna;
BOOL1               b1WlanWpa;
BOOL1               b1Controller;
BOOL1               b1Wireless;
BOOL1               b1RFManagement;
BOOL1               b1Ac;
BOOL1               b1AllAP;
BOOL1               b1ThirdpartyAP;
BOOL1               b180211an;
BOOL1               b180211an1;
BOOL1               b180211bgn;
BOOL1               b180211bgn1;
BOOL1               b180211ac;
BOOL1               b180211ac1;
BOOL1               b1qos;
BOOL1               b1Rogues;
BOOL1               b1RogueAP;
BOOL1               b1KnownrogueAP;
BOOL1               b1Rogueclients;
BOOL1               b1Adhocrogues;
BOOL1               b1Radios;
BOOL1               b1Globalconfig;
BOOL1               b1BindingTable;
BOOL1               b1WlanWebAuth;
BOOL1               b1WlanWebLogin;
BOOL1               b1WlanWebAuthUser;
BOOL1               b1UserRole;
BOOL1               b1WlanWps;
BOOL1      b1WlanBandSelect; 
BOOL1      b1dot11h;
BOOL1      b1dot11hTPC;
BOOL1               b1Multicast;
}tIssWebWss;


typedef struct IssWebObjTree {
  tIssWebSystem      WebSystems;
  tIssWebLayer2Mgmt  WebL2Mgmt;
  tIssWebLayer3Mgmt  WebL3Mgmt;
  tIssWebLayer4Mgmt  WebL4Mgmt;
  tIssWebNetworkOverlay WebNetOverlay;
  tIssWebMulticast   WebMulticast;
  tIssWebStatistics  WebStatistics;
  tIssWebMEFconformance WebMEFconformance;    
  tIssWebOthers      WebOthers;
  tIssWebWss         WebWss;
}tIssWebObjTree;

VOID  IssHttpObjTreeInit         PROTO ((tIssWebObjTree *));
VOID  IssHttpSetObjTree          PROTO ((tIssWebObjTree *));


VOID  HSLaunch                  PROTO ((VOID));
VOID  Httpstrcat                PROTO ((UINT1 *, UINT1 *));
VOID  HttpStrnCat               PROTO ((UINT1 *, UINT1 *, UINT4));  
VOID  Httpstrcpy                PROTO ((UINT1 *, UINT1 *));
UINT4 Httpstrlen                PROTO ((UINT1 *));
VOID  HttpMain                  PROTO ((INT1 *));
INT4  HttpGetSourcePort         PROTO ((VOID));
INT4  HttpSetSourcePort         PROTO ((INT4));
VOID  HttpEnable                PROTO ((VOID));
VOID  HttpDisable               PROTO ((VOID));
VOID  HttpGetHttpStatus         PROTO ((INT4 *));
PUBLIC UINT1 ISS_PARAM_STOP_FLAG[];
/* Socket Related Proto Type */
INT4 SockWrite                  PROTO ((tHttp *, INT1 *, INT4));
INT4 SockWriteStatic            PROTO ((tHttp *, INT1 *, INT4));
INT4 WebnmSendString            PROTO ((tHttp *, UINT1 *));
VOID WebnmSockWrite             PROTO ((tHttp *, UINT1 *, INT4));
tSNMP_MULTI_DATA_TYPE *
WebnmAllocMultiData             PROTO ((VOID));

VOID HttpHandleUpgradeFromMSR (tHttpMsgNode *);
INT4 HttpHandleMultiPart (tHttp *,UINT4);

VOID
WebnmSendToSocket (tHttp *, CHR1 *, tSNMP_MULTI_DATA_TYPE *, UINT1);

tSNMP_MULTI_DATA_TYPE * 
WebnmReadFromSocket (tHttp *, CHR1 *,  UINT1);
tSNMP_MULTI_DATA_TYPE *
WebnmReadNonZeroFromSocket (tHttp *, CHR1 *,  UINT1);
VOID
WebnmRegisterLock  PROTO ((tHttp *pHttp, INT4 (* fpLock) (VOID),
                           INT4 (* fpUnLock) (VOID)));

VOID
WebnmUnRegisterLock PROTO ((tHttp *pHttp));

VOID
WebnmRegisterContext  PROTO ((tHttp *pHttp, INT4 (* fpSelectContext) (UINT4),
                           INT4 (* fpReleaseContext) (VOID)));

VOID
WebnmUnRegisterContext PROTO ((tHttp *pHttp));

INT4
WebnmApiGetContextId PROTO ((tHttp *, UINT4 *));
/*VRF Change*/
INT4
WebnmApiGetL3ContextId PROTO ((tHttp *, UINT4 *));

/* Prototypes used to convert to corresponding port lists */
INT4
IssConvertToLocalPortList (tHttp *pHttp,tPortList IfPortList, 
                         tLocalPortList LocalPortList);
VOID
IssConvertToIfPortList (tHttp *pHttp,tSNMP_OCTET_STRING_TYPE * LocalPortList, 
                     tPortList IfPortList);

VOID issDecodeSpecialChar(UINT1 *pu1Str);

VOID issDecodeMacAddr(UINT1 *pu1Str);

VOID IssConvertOctetToPort (UINT1 *pOctet, UINT4 u4OctetLen, UINT1 *pu1Temp);

VOID IssProcessConfigStatusPage (tHttp * pHttp);

INT4 HttpIssCtrlSockInit (VOID);

INT4 HttpIssSendCtrlMsg (tISSCtrlMsg * pIssCtrlMsg);


VOID HttpIssProcessCtrlMsgs PROTO ((VOID));

INT4 HttpChildProcessIndex (tOsixTaskId ProcessingTaskId, INT4 *i4Index);

VOID IssSendError (tHttp * pHttp, CONST INT1 *pi1Error);
VOID IssSendSpecificError (tHttp * pHttp, CONST INT1 *pi1Error, INT1 *pi1FileName);
VOID
IssPrintMacAddress (UINT1 *pMacAddr, UINT1 *pu1Temp);

VOID HttpSetSourceBindAddr (UINT4 u4BindAddr);
VOID HttpSetTimeOutMode (UINT1 u1TimeOutMode);

VOID HttpSetLoginReq (BOOL1 b1LoginReq);
VOID WebPagePrivInit PROTO ((VOID));
/* This API is invoked by the msr module to post an event 
 * to HTTP Task */
INT4 HttpSendEventToHttpTask (UINT4 u4Event);

VOID HttpsSetTraceLevel PROTO((INT4 i4TraceLevel));

/* This API is invoked to get the global syslog message ID */
INT4 WebnmGetWebSysLogId PROTO ((VOID));

/* This API is invoked to set/get the WebUsername for syslog Message */
INT1 WebnmGetWebUsrNameForSysLogMsg PROTO ((UINT1 *pu1UserName));
INT1 WebnmSetWebUsrNameForSysLogMsg PROTO ((UINT1 *pu1UserName));
VOID IssPrintAvailableIpInterfacesAndIPAddr(tHttp *pHttp);
/* This API is invoked in SNMPSet function to know if the request is
 * from SNMP manager or from web specific page. */
UINT1 WebnmGetSnmpQueryFromWebStatus PROTO ((VOID));
UINT1 WebnmSetSnmpQueryFromWebStatus PROTO ((UINT1 u1WebnmSnmpQueryFromWebStatus));
#ifdef RADIUS_WANTED
VOID WebnmHandleRadiusResponse PROTO ((VOID *));
#endif

extern INT4  IssSkipString(tHttp *,UINT1 *);
extern INT4  HttpGetNameValue (UINT1 *pu1FirstPointer,
                               UINT1 *pu1NextPointer, UINT1 *pu1Array, 
                               UINT4 u4Index);


#endif /* _FSWEBNM_H_ */
