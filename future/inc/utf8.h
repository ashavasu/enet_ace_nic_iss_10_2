/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: utf8.h,v 1.1 2015/04/28 11:56:01 siva Exp $
 *
 * Description: This file contains the header files required for UTF8
 * module
 *********************************************************************/
#ifndef _UTF8_H
#define _UTF8_H

#define UTF8_ENABLE 0x01
#define UTF8_DISABLE 0x00

/* Required for encoding and decoding operation */
#define MASKBITS        0x3F
#define MASKBYTE        0x80
#define MASK2BYTES      0xC0
#define MASK3BYTES      0xE0
#define MASK4BYTES      0xF0
#define MASK5BYTES      0xF8
#define MASK6BYTES      0xFC
#define UTF8_SUCCESS    1
#define UTF8_FAILURE    0

#ifdef  UTF8_DEBUG_LOG
#define UTF8_DEBUG_PRINT(fmt)  UtlTrcPrint(fmt)
#else
#define UTF8_DEBUG_PRINT
#endif

/* API */
/* Used for encoding into UTF-8 */
PUBLIC UINT1
UnicodeToUtf8 (UINT4 * pu4Unicode, UINT4 u4UniLength,
UINT1 * pu1Utf8, UINT4 u4Utf8Length);

/* Used for decoding from UTF-8 */
PUBLIC UINT1
Utf8ToUnicode (UINT1 * pu1Utf8, UINT4 u4Utf8Length,
UINT4 * pu4Unicode, UINT4 u4UniLength);
#endif
