
/********************************************************************
 ** Copyright (C) 2006 Aricent Inc . All Rights Reserved
 **
 ** $Id: csr.h,v 1.9 2016/05/11 11:41:57 siva Exp $
 **
 ** Description:This file contains the exported definitions and
 **             macros of OSPF-GR-CSR
 **
 ********************************************************************/



#define CSR_SUCCESS                   OSIX_SUCCESS
#define CSR_FAILURE                   OSIX_FAILURE

#define CSR_MEMBLK_SIZE          15000

/* System Files */
#define CSR_CONFIG_FILE           (const CHR1 *)"OspfCSRrestore.conf"
#define OSPF3_CSR_CONFIG_FILE     (const CHR1 *)"Ospf3CSRrestore.conf"
#define BGP4_CSR_CONFIG_FILE     (const CHR1 *)"Bgp4CSRrestore.conf"
#define ISIS_CSR_FILE             (const CHR1 *)"IsisCSR.conf"
#define RSVPTE_CSR_FILE           (const CHR1 *)"RsvpteCSR.conf"
#define LDP_CSR_FILE              (const CHR1 *)"LdpCSR.conf"

#define ISS_CONFIG_FILE_NAME_LEN      128

#define CSR_MEMSET                       MEMSET

#define CSR_MEMBLK_COUNT                 4

#define CSR_MEMORY_TYPE                  MEM_DEFAULT_MEMORY_TYPE

#define CSR_APPLICATION_HANDLER  1

#define CSR_OSPF_FAILURE   -1
#define CSR_ISIS_FAILURE               -1

/* MEM Pool Id Definition */
#define CSR_POOL_ID(ProtoId)             gCsrPoolId[ProtoId - 1]

/* Macro for creating memory pool */

#define  CSR_CREATE_MEM_POOL(x,y,pCsrPoolId)\
         MemCreateMemPool(x,y,CSR_MEMORY_TYPE,(tMemPoolId*)pCsrPoolId)

/* Macro for deleting Memory Pool */

#define  CSR_DELETE_MEM_POOL(CsrPoolId)\
         MemDeleteMemPool((tMemPoolId) CsrPoolId)

/* Macro for Memory Block Allocation from Memory Pool */

#define  CSR_ALLOC_MEM_BLOCK(u1ProtoId) \
         ((UINT1 *) MemAllocMemBlk (CSR_POOL_ID(u1ProtoId)))         

/* Macro for Freeing Memory Blocks from Memory Pool */

#define  CSR_FREE_MEM_BLOCK(u1ProtoId, pu1Msg)\
         MemReleaseMemBlock(CSR_POOL_ID(u1ProtoId), (UINT1 *)pu1Msg)

PUBLIC INT4 IssCsrSaveCli PROTO((UINT1 *u1TaskName));
PUBLIC INT4 IssCsrCliRestore PROTO((UINT1 *pu1TaskName));
PUBLIC INT4 CsrMemAllocation PROTO ((UINT1 u1ProtoId));
PUBLIC INT4 CsrMemDeAllocation PROTO ((UINT1 u1ProtoId));

PUBLIC INT4 FilePrintf PROTO((tCliHandle CliHandle, CONST CHR1 * fmt, ...));
PUBLIC INT4 CSRCliGetShowCmdOutputToFile PROTO ((UINT1 *pu1FileName));
PUBLIC INT4 CSRCliExecuteCmdFromFile PROTO ((VOID));




