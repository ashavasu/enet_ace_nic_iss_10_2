/********************************************************************
 * Copyright Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppp.h,v 1.25 2014/12/16 10:49:12 siva Exp $
 *
 * Description:This file contains the exported definitions and  macros of PPP.
 *
 *******************************************************************/
#ifndef _PPP_H_
#define _PPP_H_

#include "rmgr.h"

/* Task */
#define  PPP_TASK_NAME              (CONST UINT1 *)"PPP"      
#define  PPP_TASK_PRIORITY          15                        
/* Lock */
#define  PPP_PROTOCOL_LOCK          (CONST UINT1 *)"PPPL"
#define  PPP_INITIAL_COUNT          1
#define  PPP_FLAGS                  0

/*********************************************
 *   definiton of Macros used in PPP         *
 *********************************************/   

#define   PPP_SUCCESS          1
#define   PPP_FAILURE          0
#define   PPP_ADD_LINK         1
#define   PPP_DELETE_LINK      2
#define   PPP_UP               0
#define   PPP_DOWN             1
#define   PPP_OPEN             2
#define   PPP_CLOSE            3
#define   PPP_ENABLE           2
#define   PPP_DISABLE          1
/* 
 * This event is used in PPPSendEventToPPPTask. This value 
 * should not be within the PPP state event machine (0-20).
 */ 
#define  PPP_DESTROY             100  

#define   PPP_LOCAL_AUTH_SERVER    1
#define   PPP_REMOTE_AUTH_SERVER   2

#define   PPTP_AUTH_PAP_MASK       0x01
#define   PPTP_AUTH_CHAP_MASK      0x02
#define   PPTP_AUTH_MSCHAP_MASK    0x04

#define  MAX_IDLE_TIMEOUT    28800 /* VPND : 8 Hours */
/* MSAD ADD -S- */
#define  PPP_AUTH_MAX_USER_SIZE   32 /*AUTH_MAX_USER_SIZE*/
/* MSAD END -E- */
#define  PPPOE_DEF_MODE            PPPOE_CLIENT
#define  PPPOE_SERVER              1
#define  PPPOE_CLIENT              2
#define  PPPOE_ENABLE              1
#define  MAX_SECURITY_LEN         256
#define  PPP_LOCAL_TO_REMOTE    1
#define  PPP_REMOTE_TO_LOCAL    2

#define L2TP_ON_DEMAND  1
#define L2TP_MANUAL     2


#define  SERIAL_IF_TYPE                118   

#define  ASYNC_IF_TYPE                 84    
#define  CFA_IF_UNK                    4                    
#define  X25_IF_TYPE                   5     
#define  ISDN_IF_TYPE                  20    
#define  FR_IF_TYPE                    32    
#define  SONET_IF_TYPE                 39
#define  PPTP_IF_TYPE                  40
#define  L2TP_IF_TYPE                  41

#define  ATM_IF_TYPE                   134   
#define  PPP_OE_IFTYPE                 6
#define  NONE_IF_TYPE                  0
#define  PPP_OE_OUSB_IFTYPE            160

/* Moved from snmpcons.h */
#define  PPP_IF_TYPE                   23    

#define  PPPOE_DEF_MRU                1492
/* MRU value for PPPoA link */
#define  PPPOA_DEF_MRU                1500

#define LOCAL_TO_REMOTE 1
#define LOC_TO_REMOTE_DIRECTION 1
#define REMOTE_TO_LOC_DIRECTION 2


typedef struct _StatEth
{
    UINT4               u4PhysIfIndex;
    UINT4               u4PhysIpAddr;
}
tStatEth;

typedef struct _DynEth
{
    UINT4               u4PhysIfIndex;
    UINT4               u4PhysIpAddr;
}
tDynEth;

typedef struct _PPPoE
{
    UINT4               u4PhysIfIndex;
    UINT4               u4PhysIpAddr;
    UINT4               u4PppIfIndex;
    UINT4               u4PppIpAddr;
}
tPPPoE;

typedef struct _Pptp
{
    UINT4               u4PhysIfIndex;
    UINT4               u4PhysIpAddr;
    UINT4               u4PppIfIndex;
    UINT4               u4PppIpAddr;
}
tPptp;

typedef struct _L2tp
{
    UINT4               u4PhysIfIndex;
    UINT4               u4PhysIpAddr;
    UINT4               u4PppIfIndex;
    UINT4               u4PppIpAddr;
}
tL2tp;

typedef struct _WanInfo
{
    UINT1               u1AccessMethod;
    UINT1               u1Status;
    UINT2               u2Rsvd;
    union
    {
        tStatEth            StatEth;
        tDynEth             DynEth;
        tPPPoE              Pppoe;
        tPptp               Pptp;
        tL2tp               L2tp;
    }
    AccessMethod;
}
tWanInfo;

typedef enum {
   PPPOE_ADD_SESSION = 1,
   PPPOE_DELETE_SESSION = 2,
   PPPOE_TRAP_UPDATE = 3,
   PPPOE_UPDATE_NAT = 4,
} ePPPoESession;

/*********************************************
 *   Prototypes for exported functions       *
 *********************************************/   

/* To create and schedule ppp task */
/* MSAD ADD -S- */
VOID PPPMain(INT1 *);
INT4 PppLock (VOID);
INT4 PppUnlock (VOID);


VOID PPPSendEventToPPPTask(UINT4,UINT1);
/* MSAD ADD -E- */

/* To create  ppp interface */
INT4   PppCreatePppInterface         PROTO((UINT4 u4IfIndex,
                                           tIfLayerInfo * pHighInterface,
                                           tIfLayerInfo * pLowInterface,
                                           UINT4 u4LocalIpAddr,
                                           UINT4 u4PeerIpAddr, UINT4 u4IfMtu));

/* To create mp interface */
INT4   PppCreateMpInterface          PROTO((UINT2 u2IfIndex,
                                            tIfLayerInfo * pHighInterface,
                                            UINT4 u4LocalIpAddr,
                                            UINT4 u4PeerIpAddr, UINT4 u4IfMtu));

/* To update ppp interface about
 * the higher and lower layers
 */
INT4   PppUpdatePppInterface         PROTO((UINT4 u4IfIndex,
                                            tIfLayerInfo * pHighInterface,
                                            tIfLayerInfo * pLowInterface,
                                            UINT4 u4LocalIpAddr,
                                            UINT4 u4PeerIpAddr, UINT4 u4IfMtu));

/* To update mp interface about
 * the higher and lower layer
 */ 
INT4   PppUpdateMpInterface          PROTO((UINT2 u2IfIndex,
                                            tIfLayerInfo * pHighInterface,
                                            UINT4 u4LocalIpAddr,
                                            UINT4 u4PeerIpAddr, UINT4 u4IfMtu));
/* Link the ppp interface to
 * the  bundle
 */
INT4   PppComposeMpBundle            PROTO((UINT2 u2MpIfIndex, 
                                           UINT4 u4PppifIndex,
                                           UINT1 u1Command));
/* To delete ppp interface */
INT4   PppDeRegisterInterface        PROTO((UINT4 u4IfIndex));

/* To bring the ppp interface up
 * by giving admin open and
 * driver up  event
 */ 

INT4   PppUpdateInterfaceStatus      PROTO((UINT4 u4IfIndex, 
                                            UINT1 u1AdminStatus,
                                            UINT1 u1OperStatus));

/* To enqueue packet from higher
 * layer to ppp task
 */
INT4   PppHandlePacketFromHl         PROTO((tCRU_BUF_CHAIN_HEADER *pBuf, 
                                             UINT4 u4OutgoingIfIndex, 
          UINT4 u4PktSize,UINT2 
          u2Protocol));
 
/* To enqueue packet from lower 
 * layer to ppp task
 */
INT4   PppHandlePacketFromLl         PROTO((tCRU_BUF_CHAIN_HEADER *pBuf, 
                                             UINT4 u4IncomingIfIndex, 
                                             UINT4 u4PktSize, 
                                             UINT1 u1EncapType));
/* To enqueue pppoe packet to 
 * ppp task
 */
INT4   PPPHandlePPPoEPkt             PROTO((tCRU_BUF_CHAIN_HEADER *pBuf, 
                                             UINT2 Length, UINT2 u2Port));

VOID PPPSetAsyncVars                 PROTO((UINT4 IfIndex, UINT1 *pTxACCM, 
                        UINT1 *pRxACCM));

INT1 PPPAddFraming                   PROTO((UINT4 IfIndex, tCRU_BUF_CHAIN_HEADER **ppOutPkt,
                                             UINT2 *pLength));

VOID PPPSetAuthOver                  PROTO((UINT4 IfIndex));

INT1 PPPGenerateLinkUpEvt            PROTO((UINT4 u4IfIndex, UINT1 u1AdminStatus, 
                        UINT1 u1OperStatus));

INT1 PPPProcessLastSentConfReq       PROTO((UINT4 IfIndex,tCRU_BUF_CHAIN_HEADER *pInPkt,
                        UINT1 ConfMode));

INT1 PPPProcessLastRcvdConfReq       PROTO((UINT4 IfIndex,tCRU_BUF_CHAIN_HEADER *pInPkt,
                        UINT1 ConfMode));

INT1 PPPProcessProxyAuth             PROTO((UINT4 IfIndex , 
                       UINT2 AuthProtocol, UINT2 AuthID,
         UINT1 *pAuthName, UINT2 AuthLength,
         UINT1 *pAuthChallenge, UINT2 ChalLength,
                              UINT1 *pAuthResp,UINT2 RespLen));

UINT4 PPPGetOrCreatePPPIf            PROTO(( UINT4 u4IfNum , UINT4 u4Mode));

INT1 PPPRemoveFraming                PROTO((UINT4 IfIndex, tCRU_BUF_CHAIN_HEADER *pInPkt, 
                       UINT2 *pLength));

INT4 pppCheckFreeIPAddresses         PROTO((VOID));

INT4
PppEnableBridging (UINT4 u4IfIndex);
INT4
PppEnableRouting (UINT4 u4IfIndex, UINT4 u4LocalIpAddr, UINT4 u4PeerIpAddr);

INT4 IPCPValidateServerAddress(INT4 i4ServAddress);
INT4 PptpValidateIpcpAddressPool(UINT4 u4StartAddress, UINT4 u4EndAddress);
INT4 L2TPValidateIpcpAddressPool(UINT4 u4StartAddress, UINT4 u4EndAddress);
UINT4 PPPIsLLIL2TP (UINT4 u4IfIndex);
UINT4 PPPGetFreePoolIndex( UINT4 Index );
INT4
PPPportGetIfIndexFromSessionId PROTO ((UINT2 u2SessionId, UINT4* pu4IfPppIndex));
INT1 PPPIPCPRegisterOptions (UINT4 u4Option, 
                              VOID (*pPPPIPCPOptionCallBack)
                              (UINT4,UINT4,UINT4,VOID *));
VOID PPPIPCPUnRegisterOptions (UINT4 u4PPPIPCPOption);
VOID PPPIPCPParseRegisterOptions (UINT4 u4PPPIPCPOption, UINT4 u4OptionType,
                                  UINT4 u4Length, VOID * pValue);
INT4 PppInitForIfType(UINT4 u4IfIndex);
VOID PppGetPPPoEMode(INT4 *pi4RetValPPPoEMode);
VOID PPPoEBindingGetIndex (UINT2 u2Port,UINT4 *u4Index);
INT4 PppGetPeerIpAddress (UINT4 u4IfIndex, UINT4 *pu4PeerIpAddr);
#ifdef HDLC_WANTED
INT4 CfaPppGetPhyIndexFromHdlcIndex (UINT4 u4HdlcIfIndex, UINT4 *pu4PhysIfIndex);
#endif
#endif /* _PPP_H */
