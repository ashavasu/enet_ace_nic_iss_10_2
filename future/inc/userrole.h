/********************************************************************
 * Copyright (C) Future Software Limited, 2001
 *
 * Description: This file contains exported definitions and macros
 *              of wssuser module
 * $Id: userrole.h,v 1.11 2015/06/27 11:40:16 siva Exp $
 *******************************************************************/
#ifndef __USER_H__
#define __USER_H__

#define  WSSUSER_DEFAULT_GROUP_ID       1

#define  WSSUSER_MODULE_ENABLED         1

#define  WSSUSER_MODULE_DISABLED        2

#define  WSSUSER_DEFAULT_MODULE_STATUS  WSSUSER_MODULE_DISABLED

#define  MAX_USER_NAME_LENGTH           32
#define  MAX_VOLUME_LENGTH              10

#define PNAC_EAP_RESPONSE               1
#define PNAC_EAP_RESPONSE_OFFSET        4
#define USERROLE_WEBNM_REJECT_EVENT     2

typedef enum {
    USER_REQUEST = 1,
    LOST_CARRIER,
    LOST_SERVICE,
    IDLE_TIMEOUT,
    SESSION_TIMEOUT,
    ADMIN_RESET,
    ADMIN_REBOOT,
    PORT_ERROR,
    NAS_ERROR,
    NAS_REQUEST,
    NAS_REBOOT,
    PORT_UNNEEDED,
    PORT_PREEMPTED,
    PORT_SUSPENDED,
    SERVICE_UNAVAILABLE,
    CALLBACK,
    USER_ERROR,
    HOST_REQUEST
} eUserTerminateCause;


typedef enum {
    WSSUSER_SESSION_ADD = 0,
    WSSUSER_SESSION_DELETE,
    WSSUSER_VOLUME_CONSUMED_IND
} enWssUserNotifyType;


typedef struct  WssUserNotifyParams {
    enWssUserNotifyType eWssUserNotifyType;
    tMacAddr     staMac;                                /* Station Mac */
    UINT1        u1EAPType;
    UINT1        au1Pad[1];                             /* Structure Padding variable */
    UINT1        au1UserName[MAX_USER_NAME_LENGTH];     /* User Name */
    UINT4        u4UserNameLen;                         /* User Name Length */
    UINT4        u4Bandwidth;                           /* Bandwidth */
    UINT4        u4DLBandwidth;                         /* Download Bandwidth */
    UINT4        u4ULBandwidth;                         /* Upload Bandwidth */
    UINT4        u4Volume;                              /* Volume */
    FLT4         f4TxBytes;                             /* Tx Bytes */
    FLT4         f4RxBytes;                             /* Rx Bytes*/
    UINT4        u4Time;                                /* Time */
    UINT4        u4TerminateCause;
    INT4         i4NasPort;
}tWssUserNotifyParams;

UINT4 WssUserProcessNotification PROTO  ((tWssUserNotifyParams *pWssUserNotifyParams));
BOOL1 WssUserIsRestrictedUser PROTO((tMacAddr staMacAddr, UINT1 *pu1Username));
VOID WssUserProcessRestrictedUser PROTO ((tMacAddr staMacAddr, UINT1 *pu1Username, BOOL1 *pbRestricted));
UINT4 
WssUserGetSessionAttributes PROTO ((tMacAddr staMacAddr, UINT1 *pu1UserName, 
    UINT4 *pu4UsedVolume,UINT4 *pu4BandWidth, 
                UINT4 *pu4DLBandWidth, UINT4 *pu4ULBandWidth));
UINT4 WssUserGetSessionEntry PROTO ((tMacAddr staMacAddr));
UINT1 WssUserRoleModuleStatus PROTO ((VOID));

UINT1
WssUserSetBandWidth PROTO ((tMacAddr StationMacAddr, UINT4 u4BandWidth, 
                            UINT4 u4DLBandWidth, UINT4 u4ULBandWidth, 
                            UINT1 u1DeleteFlag));

/* Creates MemPools WssUser Module */
UINT4 WssUserInitModuleStart (VOID);
INT4 UserLock PROTO ((VOID));
INT4 UserUnLock PROTO ((VOID));
#endif /* _USER_H */
/********** END OF USER.H *****************/


