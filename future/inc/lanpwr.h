/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: lanpwr.h,v 1.8 2015/02/18 11:23:00 siva Exp $
 *
 * Description:This file contains the necessary data strucutures
 *             for packet handling in NPAPI layer
 *             <Part of dual node stacking model>
 *
 *******************************************************************/

#ifndef __LA_NP_WR_H__
#define __LA_NP_WR_H__

#include "la.h"
#include "lanp.h"
#ifdef MBSM_WANTED
#include "mbsm.h"
#endif /* MBSM_WANTED */

/* OPER ID */
/* when new NP call is added in LA, the following files 
 * has to be updated 
 * 1. npgensup.h 
 * 2. npbcmsup.h
*/

enum
{
    FS_LA_HW_CREATE_AGG_GROUP            = 1              , 
    FS_LA_HW_ADD_LINK_TO_AGG_GROUP                        , 
    FS_LA_HW_SET_SELECTION_POLICY                         , 
    FS_LA_HW_SET_SELECTION_POLICY_BIT_LIST                , 
    FS_LA_HW_REMOVE_LINK_FROM_AGG_GROUP                   , 
    FS_LA_HW_DELETE_AGGREGATOR                            , 
    FS_LA_HW_INIT                                         , 
    FS_LA_HW_DE_INIT                                      , 
    FS_LA_HW_ENABLE_COLLECTION                            , 
    FS_LA_HW_ENABLE_DISTRIBUTION                          , 
    FS_LA_HW_DISABLE_COLLECTION                           , 
    FS_LA_HW_SET_PORT_CHANNEL_STATUS                      , 
    FS_LA_HW_ADD_PORT_TO_CONF_AGG_GROUP                   , 
    FS_LA_HW_REMOVE_PORT_FROM_CONF_AGG_GROUP              , 
    FS_LA_HW_CLEAN_AND_DELETE_AGGREGATOR                  , 
    FS_LA_GET_NEXT_AGGREGATOR                             , 
    FS_LA_RED_HW_INIT                                     , 
    FS_LA_RED_HW_NP_DELETE_AGGREGATOR                     , 
    FS_LA_RED_HW_UPDATE_D_B_FOR_AGGREGATOR                , 
    FS_LA_RED_HW_NP_UPDATE_DLF_MC_IPMC_PORT               , 
    FS_LA_HW_DLAG_STATUS                                  , 
    FS_LA_HW_DLAG_ADD_LINK_TO_AGG_GROUP                   , 
    FS_LA_HW_DLAG_REMOVE_LINK_FROM_AGG_GROUP              , 
    FS_LA_MBSM_HW_CREATE_AGG_GROUP                        , 
    FS_LA_MBSM_HW_ADD_PORT_TO_CONF_AGG_GROUP              , 
    FS_LA_MBSM_HW_ADD_LINK_TO_AGG_GROUP                   , 
    FS_LA_MBSM_HW_SET_SELECTION_POLICY                    , 
    FS_LA_MBSM_HW_INIT                                    , 
    FS_LA_HW_MAX_NP  /* Max Count should be the Last entry */ 
};


/* Required arguments list for the la NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

typedef struct {
    UINT4    u4AggIndex;
    UINT2 *  pu2HwAggId;
} tLaNpWrFsLaHwCreateAggGroup;

typedef struct {
    UINT4    u4PortNumber;
    UINT2 *  pu2HwAggId;
    UINT4    u4AggIndex;
} tLaNpWrFsLaHwAddLinkToAggGroup;

typedef struct {
    UINT4  u4AggIndex;
    UINT1  u1SelectionPolicy;
    UINT1  au1Pad[3];
} tLaNpWrFsLaHwSetSelectionPolicy;

typedef struct {
    UINT4  u4AggIndex;
    UINT4  u4SelectionPolicyBitList;
} tLaNpWrFsLaHwSetSelectionPolicyBitList;

typedef struct {
    UINT4  u4PortNumber;
    UINT4  u4AggIndex;
} tLaNpWrFsLaHwRemoveLinkFromAggGroup;

typedef struct {
    UINT4    u4PortNumber;
    UINT2 *  pu2HwAggId;
    UINT4    u4AggIndex;
} tLaNpWrFsLaHwDlagAddLinkToAggGroup;

typedef struct {
    UINT4  u4PortNumber;
    UINT4  u4AggIndex;
} tLaNpWrFsLaHwDlagRemoveLinkFromAggGroup;

typedef struct {
    UINT4  u4AggIndex;
} tLaNpWrFsLaHwDeleteAggregator;

typedef struct {
    UINT4  u4PortNumber;
    UINT4  u4AggIndex;
} tLaNpWrFsLaHwEnableCollection;

typedef struct {
    UINT4  u4PortNumber;
    UINT4  u4AggIndex;
} tLaNpWrFsLaHwEnableDistribution;

typedef struct {
    UINT4  u4PortNumber;
    UINT4  u4AggIndex;
} tLaNpWrFsLaHwDisableCollection;

typedef struct {
    UINT4  u4AggIndex;
    UINT2  u2Inst;
    UINT1  u1StpState;
    UINT1  au1Pad[1];
} tLaNpWrFsLaHwSetPortChannelStatus;

typedef struct {
    UINT4  u4PortNumber;
    UINT4  u4AggIndex;
} tLaNpWrFsLaHwAddPortToConfAggGroup;

typedef struct {
    UINT4  u4PortNumber;
    UINT4  u4AggIndex;
} tLaNpWrFsLaHwRemovePortFromConfAggGroup;

typedef struct {
    UINT1  u1DlagStatus;
    UINT1  au1Pad[3];
} tLaNpWrFsLaHwDlagStatus;


#ifdef L2RED_WANTED
typedef struct {
    UINT4  u4HwAggIndex;
} tLaNpWrFsLaHwCleanAndDeleteAggregator;

typedef struct {
    tLaHwInfo *  pHwInfo;
    INT4         i4HwAggIndex;
    INT4         i4NextHwAggIndex;
} tLaNpWrFsLaGetNextAggregator;

typedef struct {
    UINT4  u4AggIndex;
} tLaNpWrFsLaRedHwNpDeleteAggregator;

typedef struct {
    UINT1 *    ConfigPorts;
    UINT1 *    ActivePorts;
    UINT4      u4AggIndex;
    UINT1      u1SelectionPolicy;
    UINT1      au1Pad[3];
} tLaNpWrFsLaRedHwUpdateDBForAggregator;

typedef struct {
    UINT4  u4AggIndex;
} tLaNpWrFsLaRedHwNpUpdateDlfMcIpmcPort;

#endif /*  L2RED_WANTED */ 
#ifdef MBSM_WANTED
typedef struct {
    UINT4            u4AggIndex;
    tMbsmSlotInfo *  pSlotInfo;
} tLaNpWrFsLaMbsmHwCreateAggGroup;

typedef struct {
    UINT4            u4PortNumber;
    UINT4            u4AggIndex;
    tMbsmSlotInfo *  pSlotInfo;
} tLaNpWrFsLaMbsmHwAddPortToConfAggGroup;

typedef struct {
    UINT4            u4PortNumber;
    UINT4            u4AggIndex;
    tMbsmSlotInfo *  pSlotInfo;
} tLaNpWrFsLaMbsmHwAddLinkToAggGroup;

typedef struct {
    UINT4            u4AggIndex;
    tMbsmSlotInfo *  pSlotInfo;
    UINT1            u1SelectionPolicy;
    UINT1            au1Pad[3];
} tLaNpWrFsLaMbsmHwSetSelectionPolicy;

typedef struct {
    tMbsmSlotInfo *  pSlotInfo;
} tLaNpWrFsLaMbsmHwInit;

#endif /*  MBSM_WANTED */
typedef struct LaNpModInfo {
union {
    tLaNpWrFsLaHwCreateAggGroup  sFsLaHwCreateAggGroup;
    tLaNpWrFsLaHwAddLinkToAggGroup  sFsLaHwAddLinkToAggGroup;
    tLaNpWrFsLaHwSetSelectionPolicy  sFsLaHwSetSelectionPolicy;
    tLaNpWrFsLaHwSetSelectionPolicyBitList  sFsLaHwSetSelectionPolicyBitList;
    tLaNpWrFsLaHwRemoveLinkFromAggGroup  sFsLaHwRemoveLinkFromAggGroup;
    tLaNpWrFsLaHwDeleteAggregator  sFsLaHwDeleteAggregator;
    tLaNpWrFsLaHwEnableCollection  sFsLaHwEnableCollection;
    tLaNpWrFsLaHwEnableDistribution  sFsLaHwEnableDistribution;
    tLaNpWrFsLaHwDisableCollection  sFsLaHwDisableCollection;
    tLaNpWrFsLaHwSetPortChannelStatus  sFsLaHwSetPortChannelStatus;
    tLaNpWrFsLaHwAddPortToConfAggGroup  sFsLaHwAddPortToConfAggGroup;
    tLaNpWrFsLaHwRemovePortFromConfAggGroup  sFsLaHwRemovePortFromConfAggGroup;
    tLaNpWrFsLaHwDlagStatus sFsLaHwDlagStatus;
    tLaNpWrFsLaHwDlagAddLinkToAggGroup  sFsLaHwDlagAddLinkToAggGroup;
    tLaNpWrFsLaHwDlagRemoveLinkFromAggGroup  sFsLaHwDlagRemoveLinkFromAggGroup;
#ifdef L2RED_WANTED
    tLaNpWrFsLaHwCleanAndDeleteAggregator  sFsLaHwCleanAndDeleteAggregator;
    tLaNpWrFsLaGetNextAggregator  sFsLaGetNextAggregator;
    tLaNpWrFsLaRedHwNpDeleteAggregator  sFsLaRedHwNpDeleteAggregator;
    tLaNpWrFsLaRedHwUpdateDBForAggregator  sFsLaRedHwUpdateDBForAggregator;
    tLaNpWrFsLaRedHwNpUpdateDlfMcIpmcPort  sFsLaRedHwNpUpdateDlfMcIpmcPort;
#endif /* L2RED_WANTED */
#ifdef MBSM_WANTED
    tLaNpWrFsLaMbsmHwCreateAggGroup  sFsLaMbsmHwCreateAggGroup;
    tLaNpWrFsLaMbsmHwAddPortToConfAggGroup  sFsLaMbsmHwAddPortToConfAggGroup;
    tLaNpWrFsLaMbsmHwAddLinkToAggGroup  sFsLaMbsmHwAddLinkToAggGroup;
    tLaNpWrFsLaMbsmHwSetSelectionPolicy  sFsLaMbsmHwSetSelectionPolicy;
    tLaNpWrFsLaMbsmHwInit  sFsLaMbsmHwInit;
#endif /* MBSM_WANTED */
    }unOpCode;

#define  LaNpFsLaHwCreateAggGroup  unOpCode.sFsLaHwCreateAggGroup;
#define  LaNpFsLaHwAddLinkToAggGroup  unOpCode.sFsLaHwAddLinkToAggGroup;
#define  LaNpFsLaHwDlagAddLinkToAggGroup  unOpCode.sFsLaHwDlagAddLinkToAggGroup;
#define  LaNpFsLaHwSetSelectionPolicy  unOpCode.sFsLaHwSetSelectionPolicy;
#define  LaNpFsLaHwSetSelectionPolicyBitList  unOpCode.sFsLaHwSetSelectionPolicyBitList;
#define  LaNpFsLaHwRemoveLinkFromAggGroup  unOpCode.sFsLaHwRemoveLinkFromAggGroup;
#define  LaNpFsLaHwDlagRemoveLinkFromAggGroup  unOpCode.sFsLaHwDlagRemoveLinkFromAggGroup;
#define  LaNpFsLaHwDeleteAggregator  unOpCode.sFsLaHwDeleteAggregator;
#define  LaNpFsLaHwEnableCollection  unOpCode.sFsLaHwEnableCollection;
#define  LaNpFsLaHwEnableDistribution  unOpCode.sFsLaHwEnableDistribution;
#define  LaNpFsLaHwDisableCollection  unOpCode.sFsLaHwDisableCollection;
#define  LaNpFsLaHwSetPortChannelStatus  unOpCode.sFsLaHwSetPortChannelStatus;
#define  LaNpFsLaHwAddPortToConfAggGroup  unOpCode.sFsLaHwAddPortToConfAggGroup;
#define  LaNpFsLaHwRemovePortFromConfAggGroup  unOpCode.sFsLaHwRemovePortFromConfAggGroup;
#define  LaNpFsLaHwDlagStatus  unOpCode.sFsLaHwDlagStatus;
#ifdef L2RED_WANTED
#define  LaNpFsLaHwCleanAndDeleteAggregator  unOpCode.sFsLaHwCleanAndDeleteAggregator;
#define  LaNpFsLaGetNextAggregator  unOpCode.sFsLaGetNextAggregator;
#define  LaNpFsLaRedHwNpDeleteAggregator  unOpCode.sFsLaRedHwNpDeleteAggregator;
#define  LaNpFsLaRedHwUpdateDBForAggregator  unOpCode.sFsLaRedHwUpdateDBForAggregator;
#define  LaNpFsLaRedHwNpUpdateDlfMcIpmcPort  unOpCode.sFsLaRedHwNpUpdateDlfMcIpmcPort;
#endif /* L2RED_WANTED */
#ifdef MBSM_WANTED
#define  LaNpFsLaMbsmHwCreateAggGroup  unOpCode.sFsLaMbsmHwCreateAggGroup;
#define  LaNpFsLaMbsmHwAddPortToConfAggGroup  unOpCode.sFsLaMbsmHwAddPortToConfAggGroup;
#define  LaNpFsLaMbsmHwAddLinkToAggGroup  unOpCode.sFsLaMbsmHwAddLinkToAggGroup;
#define  LaNpFsLaMbsmHwSetSelectionPolicy  unOpCode.sFsLaMbsmHwSetSelectionPolicy;
#define  LaNpFsLaMbsmHwInit  unOpCode.sFsLaMbsmHwInit;
#endif /* MBSM_WANTED */
} tLaNpModInfo;


/* Function Prototype for Generic NP OpCode specific wrappers
 * defined in Lanputil.c */

UINT1 LaFsLaHwCreateAggGroup PROTO ((UINT2 u2AggIndex, UINT2 * pu2HwAggId));
UINT1 LaFsLaHwAddLinkToAggGroup PROTO ((UINT2 u2AggIndex, UINT2 u2PortNumber, UINT2 * pu2HwAggId));
UINT1 LaFsLaHwSetSelectionPolicy PROTO ((UINT2 u2AggIndex, UINT1 u1SelectionPolicy));
UINT1 LaFsLaHwSetSelectionPolicyBitList PROTO ((UINT2 u2AggIndex, UINT4 u4SelectionPolicyBitList));
UINT1 LaFsLaHwRemoveLinkFromAggGroup PROTO ((UINT2 u2AggIndex, UINT2 u2PortNumber));
UINT1 LaFsLaHwDeleteAggregator PROTO ((UINT2 u2AggIndex));
UINT1 LaFsLaHwInit PROTO ((VOID));
UINT1 LaFsLaHwDeInit PROTO ((VOID));
UINT1 LaFsLaHwEnableCollection PROTO ((UINT2 u2AggIndex, UINT2 u2PortNumber));
UINT1 LaFsLaHwEnableDistribution PROTO ((UINT2 u2AggIndex, UINT2 u2PortNumber));
UINT1 LaFsLaHwDisableCollection PROTO ((UINT2 u2AggIndex, UINT2 u2PortNumber));
UINT1 LaFsLaHwSetPortChannelStatus PROTO ((UINT2 u2AggIndex, UINT2 u2Inst, UINT1 u1StpState));
UINT1 LaFsLaHwAddPortToConfAggGroup PROTO ((UINT2 u2AggIndex, UINT2 u2PortNumber));
UINT1 LaFsLaHwRemovePortFromConfAggGroup PROTO ((UINT2 u2AggIndex, UINT2 u2PortNumber));
UINT1 LaFsLaHwDlagStatus PROTO ((UINT1 u1DlagStatus));
UINT1 LaFsLaHwDlagAddLinkToAggGroup PROTO ((UINT2 u2AggIndex, UINT2 u2PortNumber, UINT2 * pu2HwAggId));
UINT1 LaFsLaHwDlagRemoveLinkFromAggGroup PROTO ((UINT2 u2AggIndex, UINT2 u2PortNumber));
#ifdef L2RED_WANTED
UINT1 LaFsLaHwCleanAndDeleteAggregator PROTO ((UINT2 u2HwAggIndex));
INT4 LaFsLaGetNextAggregator PROTO ((INT4 i4HwAggIndex, tLaHwInfo * pHwInfo));
UINT1 LaFsLaRedHwInit PROTO ((VOID));
UINT1 LaFsLaRedHwNpDeleteAggregator PROTO ((UINT2 u2AggIndex));
UINT1 LaFsLaRedHwUpdateDBForAggregator PROTO ((UINT2 u2AggIndex, tPortList ConfigPorts, tPortList ActivePorts, UINT1 u1SelectionPolicy));
UINT1 LaFsLaRedHwNpUpdateDlfMcIpmcPort PROTO ((UINT2 u2AggIndex));
#endif /* L2RED_WANTED */
#ifdef MBSM_WANTED
UINT1 LaFsLaMbsmHwCreateAggGroup PROTO ((UINT2 u2AggIndex, tMbsmSlotInfo * pSlotInfo));
UINT1 LaFsLaMbsmHwAddPortToConfAggGroup PROTO ((UINT2 u2AggIndex, UINT2 u2PortNumber, tMbsmSlotInfo * pSlotInfo));
UINT1 LaFsLaMbsmHwAddLinkToAggGroup PROTO ((UINT2 u2AggIndex, UINT2 u2PortNumber, tMbsmSlotInfo * pSlotInfo));
UINT1 LaFsLaMbsmHwSetSelectionPolicy PROTO ((UINT2 u2AggIndex, UINT1 u1SelectionPolicy, tMbsmSlotInfo * pSlotInfo));
UINT1 LaFsLaMbsmHwInit PROTO ((tMbsmSlotInfo * pSlotInfo));
#endif /* MBSM_WANTED */

#endif /*__LA_NP_WR_H__ */
