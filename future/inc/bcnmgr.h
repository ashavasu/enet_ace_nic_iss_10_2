/* Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * $Id: bcnmgr.h,v 1.2 2014/03/26 20:35:23 siva Exp
 * Description:This file contains the exported definitions and
 *             macros of WLC Hanlder module
 *
 *******************************************************************/
#ifndef _BCNMGR_H
#define _BCNMGR_H

#include "wssmac.h"

#define BCNMGR_RECV_CONF_UPDATE         2

VOID BcnMgrMainTask (INT1 *);

typedef struct {

        /* WLAN Beacon Attributes */
        UINT2       u2Capability;
        UINT2       u2Edca_AC_BE_TxOpLimit;
        UINT2       u2Edca_AC_BK_TxOpLimit;
        UINT2       u2Edca_AC_VI_TxOpLimit;
        UINT2       u2Edca_AC_VO_TxOpLimit;
        UINT2       u2BeaconPeriod;

        UINT1       au1Ssid[WSSMAC_MAX_SSID_LEN];
        UINT1       u1DtimPeriod;
        UINT1       u1LocPowConstraint;
        UINT1       u1EdcaQosInfo;
        UINT1       u1Edca_AC_BE_AciAfsn;
        UINT1       u1Edca_AC_BE_EcwMinMax;
        UINT1       u1Edca_AC_BK_AciAfsn;
        UINT1       u1Edca_AC_BK_EcwMinMax;
        UINT1       u1Edca_AC_VI_AciAfsn;
        UINT1       u1Edca_AC_VI_EcwMinMax;
        UINT1       u1Edca_AC_VO_AciAfsn;
        UINT1       u1Edca_AC_VO_EcwMinMax;
        UINT1       u1QosInfo;
        /* RADIO-IF Beacon Attributes */
        UINT1       au1SuppRate[WSSMAC_MAX_SUPP_RATE_LEN];
        UINT1       u1DsCurChannel;
        UINT1       au1CountryStr[WSSMAC_MAX_COUNTRY_STR_LEN];
        UINT1       u1FirstChanlNum;
        UINT1       u1NumOfChanls;
        UINT1       u1MaxTxPowerLevel;
        UINT1       u1ExtSupport;
        UINT1       au1ExtSuppRates[WSSMAC_MAX_EXT_SUPP_RATES];
 UINT1       u1Pad;
        /* MAC Management Parameters*/
        UINT1       u1Bssid[WSSMAC_MAC_ADDR_LEN];
 UINT1      au1Pad[2];
        /*802.11 n support*/
        tHTCapabilities             HTCapabilities;
        tHTOperation                HTOperation;
        UINT1                 u1Dot11Support;
 /* RSNA Parameters */
 UINT1   u1KeyStatus;
 UINT2       u2RSN_Version;
 UINT4       u4RSN_GrpCipherSuit;
 UINT2       u2RSN_PairCiphSuitCnt;
 tWssMacRSN_CiphSuit aRSN_PairCiphSuitList[WSSMAC_MAX_CIPH_SUIT_CNT];
 UINT2       u2RSN_AKMSuitCnt;
 UINT4       au4RSN_AKMSuitList[WSSMAC_MAX_AKM_SUIT_CNT];
 UINT2       u2RSN_Capability;
 UINT2       u2RSN_PMKIdCnt;

}tBcnMgrBcnParams;

typedef struct {

    tCRU_BUF_CHAIN_DESC *pBcnMacPdu;    /* Pointer to the Beacon Frame PDU Buffer */

}tBcnMgrBcnMacFrame;

typedef struct {
        UINT4       u4MsgType;  /* Message Type */
        union
        {
            tBcnMgrBcnParams    BcnParams;      /* Conf update from WLAN/Radio-IF */
            tBcnMgrBcnMacFrame  BcnMacFrame;    /* Beacon Message from WSS MAC Module */
        }unMsgParam;

}tBcnMgrQMsg;

enum {
 WSS_BCNMGR_CONF_UPDATE

};

typedef struct {

 union {
  tBcnMgrBcnParams BcnMgrBcnParams;
        tBcnMgrQMsg         BcnMgrQMsg;
 }unBcn;
    UINT2       u2TimerStart;
    UINT1       au1pad[2];
}tBcnMgrMsgStruct;
#endif
