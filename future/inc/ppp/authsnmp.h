/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: authsnmp.h,v 1.3 2014/03/11 14:02:39 siva Exp $
 *
 * Description:
 *
 *******************************************************************/
#ifndef __PPP_AUTHSNMP_H__
#define __PPP_AUTHSNMP_H__

#define   AUTH_DIRECTION_MASK     0x01
#define   AUTH_PROTOCOL_MASK      0x02
#define   AUTH_IDENTITY_MASK      0x04
#define   AUTH_SECRET_MASK        0x08
#define   AUTH_ALL_SET            0x0f

#define   AUTH_VALID              2
#define   AUTH_INVALID            1

#define   DEPENDENCY_FREE(x)      FREE(x);\
                                  x=NULL;

#define   MAX_SECRET_SIZE         255
typedef struct {
   UINT4      IfIndex;
   UINT4      IdIndex;
   UINT2      Protocol;
   UINT1      Direction;
   UINT1      IdentityLen;
   UINT1      Identity[MAX_SECRET_SIZE];
   UINT1      SecretLen;
   UINT1      Secret[MAX_SECRET_SIZE];
   UINT1      SecretLmHash[MAX_SECRET_SIZE];
   UINT1      Status;
   UINT1      StatusValid;
}  t_SECRET_DEPENDENCY;

#endif  /* __PPP_AUTHSNMP_H__ */
