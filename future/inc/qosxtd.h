/*$Id: qosxtd.h,v 1.52 2017/12/15 09:51:56 siva Exp $*/
/****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                     */
/*                                                                          */
/*  FILE NAME             : qos.h                                           */
/*  PRINCIPAL AUTHOR      : Aricent                                         */
/*  SUBSYSTEM NAME        : QoS                                             */
/*  MODULE NAME           : QoS-Ext                                         */
/*  LANGUAGE              : C                                               */
/*  TARGET ENVIRONMENT    : Linux                                           */
/*  DATE OF FIRST RELEASE :                                                 */
/*  AUTHOR                : Aricent                                         */
/*  FUNCTIONS DEFINED(Applicable for Source files) :                        */
/*  DESCRIPTION           : This file contains the exported definitions and */
/*                          macros of Aricent QoS Module.                   */
/****************************************************************************/
/*                                                                          */
/*  Change History                                                          */
/*  Version               :                                                 */
/*  Date(DD/MM/YYYY)      :                                                 */
/*  Modified by           :                                                 */
/*  Description of change :                                                 */
/****************************************************************************/

#ifndef _QOSX_H
#define _QOSX_H

#define QOS_FAILURE          0
#define QOS_SUCCESS          1
#define QOS_NP_NOT_SUPPORTED 2

#define QOS_NP_ADD          1
#define QOS_NP_DEL          2

#define QOS_L2FILTER      1
#define QOS_L3FILTER      2

#define QOS_INCR          1    /* Incrment RefCount  */
#define QOS_DECR          2    /* Decrement RefCount */

#define QOS_PLY_MAP       1    /* Map Class and Policy     */
#define QOS_PLY_ADD       2    /* Add a Policy             */
#define QOS_PLY_UNMAP     3    /* UnMap Class From Policy  */
#define QOS_PLY_DEL       4    /* Delete the Policy        */

#define QOS_VLANQ_MAP       1    /* Map Class and Policy     */
#define QOS_VLANQ_ADD       2    /* Add a Policy             */
#define QOS_VLANQ_UNMAP     3    /* UnMap Class From Policy  */
#define QOS_VLANQ_DEL       4    /* Delete the Policy        */


#define  QOS_TASK_NAME                 "QOS"

#define QOS_UPDATE_PLY_IF                      1
#define QOS_UPDATE_PLY_PHB_TYPE                2 
#define QOS_UPDATE_PLY_PHB_VAL                 3
#define QOS_UPDATE_PLY_METER_ID                4
#define QOS_UPDATE_PLY_ACT_CONF_FLAG           5
#define QOS_UPDATE_PLY_ACT_CONF_PORT           6
#define QOS_UPDATE_PLY_ACT_CONF_IPTOS          7
#define QOS_UPDATE_PLY_ACT_CONF_IPDSCP         8 
#define QOS_UPDATE_PLY_ACT_CONF_VLAN_PRI       9
#define QOS_UPDATE_PLY_ACT_CONF_VLAN_DE        10
#define QOS_UPDATE_PLY_ACT_CONF_INNER_VLAN_PRI 11
#define QOS_UPDATE_PLY_ACT_CONF_MPLS_EXP       12
#define QOS_UPDATE_PLY_ACT_EXC_FLAG            13
#define QOS_UPDATE_PLY_ACT_EXC_IPTOS           14
#define QOS_UPDATE_PLY_ACT_EXC_IPDSCP          15 
#define QOS_UPDATE_PLY_ACT_EXC_INNER_VLAN_PRI  16
#define QOS_UPDATE_PLY_ACT_EXC_VLAN_PRI        17
#define QOS_UPDATE_PLY_ACT_EXC_VLAN_DE         18
#define QOS_UPDATE_PLY_ACT_EXC_MPLS_EXP        19
#define QOS_UPDATE_PLY_ACT_VIO_ACT_FLAG        20
#define QOS_UPDATE_PLY_ACT_VIO_IPTOS           21
#define QOS_UPDATE_PLY_ACT_VIO_IPDSCP          22
#define QOS_UPDATE_PLY_ACT_VIO_INNER_VLAN_PRI  23
#define QOS_UPDATE_PLY_ACT_VIO_VLAN_PRI        24
#define QOS_UPDATE_PLY_ACT_VIO_VLAN_DE         25
#define QOS_UPDATE_PLY_ACT_VIO_MPLS_EXP        26
#define QOS_UPDATE_PLY_ACT_CONF_INNER_VLAN_DE  27
#define QOS_UPDATE_PLY_ACT_EXC_INNER_VLAN_DE   28
#define QOS_UPDATE_PLY_ACT_VIO_INNER_VLAN_DE   29
#define QOS_UPDATE_PLY_DEFAULT_VLAN_DE         30
#define QOS_MAX_VLAN_PRI  8


/* QoS Rate Information Units */
#define QOS_RATE_UNIT_BPS             (1)
#define QOS_RATE_UNIT_KBPS            (2)
#define QOS_RATE_UNIT_MBPS            (3)
#define QOS_RATE_UNIT_GBPS            (4)

#define QOS_STATS_ENABLE                        1
#define QOS_STATS_DISABLE                       0


/* QoS COSQ's and schedulers that can be created */
#define QOS_Q_UNICAST_TYPE             1
#define QOS_Q_MULTICAST_TYPE           2
#define QOS_Q_SUBSCRIBER_TYPE          3

#define QOS_S1_SCHEDULER               1
#define QOS_S2_SCHEDULER               2
#define QOS_S3_SCHEDULER               3

/* QoS Vlan Queueing status */
#define    VLAN_QUEUEING_SYS_STATUS_DISABLE      0  /* 0 - Vlan queueing is disabled */
#define    VLAN_QUEUEING_SYS_STATUS_ENABLE       1  /* 1 - Vlan Queueing is enabled */


/*****************************************************************************/ 
/*                           COMMON MACROS                                   */
/*****************************************************************************/ 
/*Mem Pool Size */
/* It will be used to allocate mem pool (Type Heap) at module start up 
 * the size of the each table mem pool is 
 *  TblMemPoolSize =  MaxTblEnt/MemPoolSizeFactor 
 * */ 
#define QOS_MEM_POOL_SIZE_FACTOR  (1)


/*  1. Priority Map Table  */
#define QOS_PRI_MAP_HASH_TBL_MAX_ENTRIES      16

#define QOS_DEFAULT_PRI_MAP_MIN_VAL           1
#define QOS_DEFAULT_PRI_MAP_MAX_VAL           8

#define QOS_DEFAULT_PRI_MAX_VAL               7

#define QOS_PLY_DEFAULT_DEI_VAL               0
#define QOS_PRI_MAP_TBL_INVLD_INREGPRI        8

#define QOS_IN_PRI_TYPE_VLAN_PRI              0
#define QOS_IN_PRI_TYPE_IP_TOS                1
#define QOS_IN_PRI_TYPE_IP_DSCP               2
#define QOS_IN_PRI_TYPE_MPLS_EXP              3
#define QOS_IN_PRI_TYPE_VLAN_DEI              4
#define QOS_IN_PRI_TYPE_DOT1P                 5
#define QOS_IN_PRI_TYPE_VLAN_ID               6
#define QOS_IN_PRI_TYPE_SRC_MAC               7
#define QOS_IN_PRI_TYPE_DEST_MAC              8

#define QOS_PRI_REGEN_COLOR_GREEN             0
#define QOS_PRI_REGEN_COLOR_YELLOW            1
#define QOS_PRI_REGEN_COLOR_RED               2
/* Class Map Table */
/* Pre Color */
#define QOS_CLS_COLOR_NONE                    0
#define QOS_CLS_COLOR_GREEN                   1
#define QOS_CLS_COLOR_YELLOW                  2
#define QOS_CLS_COLOR_RED                     3

/* Meter Table */
/* Color Mode */
#define QOS_METER_COLOR_AWARE                 1
#define QOS_METER_COLOR_BLIND                 2
/* Meters Types */
#define QOS_METER_TYPE_SIMPLE_TB              1
#define QOS_METER_TYPE_AVG_RATE               2
#define QOS_METER_TYPE_SRTCM                  3
#define QOS_METER_TYPE_TRTCM                  4
#define QOS_METER_TYPE_TSWTCM                 5
#define QOS_METER_TYPE_MEF_DECOUPLED          6
#define QOS_METER_TYPE_MEF_COUPLED            7

/* Policy Map Table  */
/* PHB Type */
#define QOS_PLY_PHB_TYPE_NONE                   0
#define QOS_PLY_PHB_TYPE_VLAN_PRI               1
#define QOS_PLY_PHB_TYPE_IP_TOS                 2
#define QOS_PLY_PHB_TYPE_IP_DSCP                3
#define QOS_PLY_PHB_TYPE_MPLS_EXP               4
#define QOS_PLY_PHB_TYPE_DOT1P                  5
#define QOS_POLICY_ACTION_FLAG_LENGTH    1

/* In Profile Conform Action Flags */
#define QOS_PLY_IN_PROF_DROP                 0x0001
#define QOS_PLY_IN_PROF_CONF_INNER_VLAN_DE   0x0002
#define QOS_PLY_IN_PROF_CONF_IP_TOS          0x0004
#define QOS_PLY_IN_PROF_CONF_IP_DSCP         0x0008
#define QOS_PLY_IN_PROF_CONF_VLAN_PRI        0x0010
#define QOS_PLY_IN_PROF_CONF_VLAN_DE         0x0020
#define QOS_PLY_IN_PROF_CONF_INNER_VLAN_PRI  0x0040
#define QOS_PLY_IN_PROF_CONF_MPLS_EXP        0x0080
#define QOS_PLY_IN_PROF_PORT                 0x0100

/*Rang of In Profile Conform Action Flags */
#define QOS_PLY_IN_PROF_CONF_ACTION_MIN_VAL  0x0001
#define QOS_PLY_IN_PROF_CONF_ACTION_MAX_VAL  0x00FE

/* In Profile Exceed Action Flags */
#define QOS_PLY_IN_PROF_EXC_DROP             0x0001
#define QOS_PLY_IN_PROF_EXC_IP_TOS           0x0002
#define QOS_PLY_IN_PROF_EXC_IP_DSCP          0x0004
#define QOS_PLY_IN_PROF_EXC_VLAN_PRI         0x0008
#define QOS_PLY_IN_PROF_EXC_VLAN_DE          0x0010
#define QOS_PLY_IN_PROF_EXC_INNER_VLAN_PRI   0x0020
#define QOS_PLY_IN_PROF_EXC_MPLS_EXP         0x0040
#define QOS_PLY_IN_PROF_EXC_INNER_VLAN_DE    0x0080
    
/*Rang of In Profile Action Flags */
#define QOS_PLY_IN_PROF_EXC_ACTION_MIN_VAL   0x0001
#define QOS_PLY_IN_PROF_EXC_ACTION_MAX_VAL   0x00FE

/* Out Profile Action Flags */
#define QOS_PLY_OUT_PROF_VIO_DROP            0x0001
#define QOS_PLY_OUT_PROF_VIO_IP_TOS          0x0002
#define QOS_PLY_OUT_PROF_VIO_IP_DSCP         0x0004
#define QOS_PLY_OUT_PROF_VIO_VLAN_PRI        0x0008
#define QOS_PLY_OUT_PROF_VIO_VLAN_DE         0x0010
#define QOS_PLY_OUT_PROF_VIO_INNER_VLAN_PRI  0x0020
#define QOS_PLY_OUT_PROF_VIO_MPLS_EXP        0x0040
#define QOS_PLY_OUT_PROF_VIO_INNER_VLAN_DE   0x0080
/*Rang of Out Profile Action Flags */
#define QOS_PLY_OUT_PROF_ACTION_MIN_VAL      0x0001  
#define QOS_PLY_OUT_PROF_ACTION_MAX_VAL      0x00FE


/* Q Template Table */
#define QOS_Q_TEMP_DROP_ALGO_ENABLE  1
#define QOS_Q_TEMP_DROP_ALGO_DISABLE 2



#define QOS_Q_TEMP_DROP_TYPE_OTHER   1
#define QOS_Q_TEMP_DROP_TYPE_TAIL    2
#define QOS_Q_TEMP_DROP_TYPE_HEAD    3
#define QOS_Q_TEMP_DROP_TYPE_RED     4
#define QOS_Q_TEMP_DROP_TYPE_ALWAYS  5
#define QOS_Q_TEMP_DROP_TYPE_WRED    6

#define QOS_Q_TEMP_DROP_THRESH_TYPE_PKTS  1
#define QOS_Q_TEMP_DROP_THRESH_TYPE_BYTES 2

/* RD Config Action Flags */
#define QOS_Q_TEMP_FLAG_NONE                0x0000
#define QOS_Q_TEMP_FLAG_CAP_AVERAGE         0x0001
#define QOS_Q_TEMP_FLAG_MARK_ECN            0x0002
/*Range of RD Cfg Action Flags */
#define QOS_Q_TEMP_FLAG_MIN_VAL      0x0000
#define QOS_Q_TEMP_FLAG_MAX_VAL      0x0003

/* Drop Profiles */
#define QOS_Q_TEMP_DISCARD_TCP_GREEN      0
#define QOS_Q_TEMP_DISCARD_TCP_YELLOW     1
#define QOS_Q_TEMP_DISCARD_TCP_RED        2
#define QOS_Q_TEMP_DISCARD_NONTCP_GREEN   3
#define QOS_Q_TEMP_DISCARD_NONTCP_YELLOW  4
#define QOS_Q_TEMP_DISCARD_NONTCP_RED     5

#define QOS_Q_TEMP_DISCARD_PROF_MIN    0
#define QOS_Q_TEMP_DISCARD_PROF_MAX    5

/* Hierarchy Table Flag to Add/Del HW Entry  */
#define QOS_HIERARCHY_ADD                      1
#define QOS_HIERARCHY_DEL                      2
                                              
/* 9. Scheduler Table */
#define QOS_SCHED_ADDED                        1
#define QOS_SCHED_NOT_ADDED                    2
/*Scheduler updated param Type */
#define QOS_SCHED_UPDATE_ALGO                  1
#define QOS_SCHED_UPDATE_SHAPE                 2
#define QOS_SCHED_UPDATE_HL                    3

#define QOS_SCHED_ALGO_SP                      1
#define QOS_SCHED_ALGO_RR                      2
#define QOS_SCHED_ALGO_WRR                     3
#define QOS_SCHED_ALGO_WFQ                     4
#define QOS_SCHED_ALGO_SRR                     5
#define QOS_SCHED_ALGO_SWRR                    6
#define QOS_SCHED_ALGO_SWFQ                    7
#define QOS_SCHED_ALGO_DRR                     8
#define QOS_SCHED_BOUNDED_DELAY                9  
#define QOS_SCHED_DEFICIT_RR                   10
#define QOS_SCHED_NOT_SUPPORTED_ALGO           11 

#define DS_COSQ_STRICT_WEIGHT        0
#define DS_COSQ_MIN_WEIGHT           1
#define DS_COSQ_MAX_WEIGHT           15
#define DS_COSQ_MIN_BW               1
#define DS_COSQ_DEF_BW               2047
#define DS_COSQ_MAX_BW               262143
#define DS_COSQ_FLAG_EXS             1
#define DS_COSQ_FLAG_MIN             2
/* 11. QMap Table */
#define QOS_QMAP_TYPE_CLASS                    1
#define QOS_QMAP_TYPE_PRIO                     2
#define QOS_QMAP_ADD                           1
#define QOS_QMAP_DEL                           2

#define QOS_QMAP_PRI_TYPE_NONE                 0
#define QOS_QMAP_PRI_TYPE_VLAN_PRI             1
#define QOS_QMAP_PRI_TYPE_IP_TOS               2  
#define QOS_QMAP_PRI_TYPE_IP_DSCP              3
#define QOS_QMAP_PRI_TYPE_MPLS_EXP             4
#define QOS_QMAP_PRI_TYPE_VLAN_DEI             5
#define QOS_QMAP_PRI_TYPE_INT_PRI              6 /*Internal priority */
#define QOS_QMAP_PRI_TYPE_DOT1P                7
#define QOS_QMAP_PRI_TYPE_L2_FIL               8
#define QOS_QMAP_PRI_TYPE_L3_FIL               9
#define QOS_QMAP_PRI_TYPE_VLAN_MAP             10
#define QOS_DEFAULT_REGENPRI(u4QId)            (u4QId+1)


#define QOS_QMAP_COLOR_GREEN                 0
#define QOS_QMAP_COLOR_YELLOW                1
#define QOS_QMAP_COLOR_RED                   2

/* 13. Meter Stats Table */
#define QOS_POLICER_STATS_CONF_PKTS            1
#define QOS_POLICER_STATS_CONF_OCTETS          2
#define QOS_POLICER_STATS_EXC_PKTS             3
#define QOS_POLICER_STATS_EXC_OCTETS           4
#define QOS_POLICER_STATS_VIO_PKTS             5
#define QOS_POLICER_STATS_VIO_OCTETS           6
                                              
/* 14. CoS Q Scheduler Stats Table */         
#define QOS_COSQ_STATS_ENQ_PKTS                1
#define QOS_COSQ_STATS_ENQ_OCTETS              2
#define QOS_COSQ_STATS_DEQ_PKTS                3
#define QOS_COSQ_STATS_DEQ_OCTETS              4
#define QOS_COSQ_STATS_DISCARD_PKTS            5
#define QOS_COSQ_STATS_DISCARD_OCTETS          6
#define QOS_COSQ_STATS_OCCUPANCY_BYTES         7
#define QOS_COSQ_STATS_CONG_MGNT_DROP_BYTES    8
#define QOS_COSQ_STATS_CLEAR       9 /* MACRO to clear queue stats during QOS enable/disable */

/*15. Port Default User Priority (PDUP) Table */
#define QOS_PDUP_TBL_MIN_PRI_VAL               0
#define QOS_PDUP_TBL_MAX_PRI_VAL               7

UINT4 QOSSendEventToQOSTask PROTO ((UINT4 u4Event));
/* Default Table Entry Macros */
/* 1.Priority Map Table  */
#define QOS_INTERFACE_DEF_ENTRY_MAX            24
#define QOS_PM_TBL_DEF_ENTRY_TYPE              QOS_IN_PRI_TYPE_VLAN_PRI
/* (0- to QOS_PM_TBL_DEF_ENTRY_MAX) */
#define QOS_PM_TBL_DEF_ENTRY_MAX               7

/* 2.Class Map Table  */
#define QOS_CM_TBL_DEF_ENTRY_MAX               8
#define QOS_CM_TBL_DEF_CLASS                   1

/* 3.Policy Map Table  */
#define QOS_POL_TBL_DEF_ENTRY_MAX              1
/* 4.QueueTemplate Table  */
#define QOS_QT_TBL_DEF_ENTRY_MAX               1
/* 5.QueueMap Table  */


#define DEFAULT_MAX_ARRAYSIZE_PRI              8
#define DEFAULT_MAX_ARRAYSIZE_AVIALABLEQUEUE   8
#define DEFAULT_MAX_ARRAYSIZE_PCP_MODEL        4
#define DEFAULT_MAX_ARRAYSIZE_DOT1P            16
/* 6.PCP Table */
/*Selection Row*/
#define QOS_PCP_SEL_ROW_8P0D                   0
#define QOS_PCP_SEL_ROW_7P1D                   1 
#define QOS_PCP_SEL_ROW_6P2D                   2
#define QOS_PCP_SEL_ROW_5P3D                   3
/*Packet Type*/
#define QOS_PCP_PKT_TYPE_DOT1P                 0
#define QOS_PCP_PKT_TYPE_IP                    1
#define QOS_PCP_PKT_TYPE_MPLS                  2
/*Entry Type*/
#define QOS_PCP_ENTRY_TYPE_IMPLICIT            1
#define QOS_PCP_ENTRY_TYPE_EXPLICIT            2
/*Action */
#define QOS_PCP_TBL_ACTION_CREATE              1
#define QOS_PCP_TBL_ACTION_MODIFY              2
#define QOS_PCP_TBL_ACTION_DELETE              3

/* Priority Map Table */
/*DOT1P - [Vlan pri(8)+ CFI (2)] * MaxPorts(24)]=384*/
/*IP    - [IP DSCP(64) * MaxPorts(24)] = 1536*/
/*MPLS  - [MPLS EXP (8) * MaxPorts(24)] = 192*/

#define QOS_PCP_1P_TBL_DEF_ENTRY_MIN        30001
#define QOS_PCP_1P_TBL_DEF_ENTRY_MAX        30384
#define QOS_PCP_IP_TBL_DEF_ENTRY_MIN        30385
#define QOS_PCP_IP_TBL_DEF_ENTRY_MAX        31920
#define QOS_PCP_MPLS_TBL_DEF_ENTRY_MIN      31921
#define QOS_PCP_MPLS_TBL_DEF_ENTRY_MAX      32112

/*Macros For Web */

PUBLIC INT4 QoSLock       PROTO ((VOID));
PUBLIC INT4 QoSUnLock     PROTO ((VOID));
#define PRIORITYTYPE 2
#define IPORMACTYPE  1
#define QOS_PLY_IN_PROF_CONF_VLAN_PRI_DE 0x0030
#define QOS_PLY_IN_PROF_EXC_VLAN_PRI_DE 0x0018
#define QOS_PLY_OUT_PROF_VIO_VLAN_PRI_DE 0x0018
#define QOS_PLY_OUT_PROF_VIO_INNER_VLAN_PRI_DE 0x00A0
#define QOS_PLY_IN_PROF_EXC_INNER_VLAN_PRI_DE 0x00A0
#define QOS_PLY_IN_PROF_CONF_INNER_VLAN_PRI_DE 0x0042

#define QOS_IN_PROF_NONE                 0
#define QOS_IN_PROF_PORT                 2
#define QOS_IN_PROF_CONF_IP_TOS          4
#define QOS_IN_PROF_CONF_IP_DSCP         8
#define QOS_IN_PROF_CONF_VLAN_PRI        16
#define QOS_IN_PROF_CONF_VLAN_DE         32
#define QOS_IN_PROF_CONF_VLAN_PRI_DE     48
#define QOS_IN_PROF_CONF_INNER_VLAN_PRI  64
#define QOS_IN_PROF_CONF_MPLS_EXP        128
#define QOS_IN_PROF_EXC_IP_TOS           2
#define QOS_IN_PROF_EXC_IP_DSCP          4
#define QOS_IN_PROF_EXC_VLAN_PRI         8
#define QOS_IN_PROF_EXC_VLAN_DE          16
#define QOS_IN_PROF_EXC_VLAN_PRI_DE      24
#define QOS_IN_PROF_EXC_INNER_VLAN_PRI   32
#define QOS_IN_PROF_EXC_MPLS_EXP         64

#define QOS_GLOBAL_QID_FROM_IF_QID(InterfaceId, InterfaceQid) ((UINT4)((InterfaceId - 1) * QOS_QUEUE_ENTRY_MAX) + InterfaceQid)

#define QOS_OUT_PROF_VIO_DROP            0
#define QOS_OUT_PROF_VIO_IP_TOS          2
#define QOS_OUT_PROF_VIO_IP_DSCP         4
#define QOS_OUT_PROF_VIO_VLAN_PRI        8
#define QOS_OUT_PROF_VIO_VLAN_DE         16
#define QOS_OUT_PROF_VIO_VLAN_PRI_DE     24
#define QOS_OUT_PROF_VIO_INNER_VLAN_PRI  32
#define QOS_OUT_PROF_VIO_MPLS_EXP        64
#define STD_QOS_OID_LEN                  12
#define QOS_IPV4_LEN                    4
#define QOS_IPV6_LEN                    16
#define QOS_MF_PERFIX_IPV4_MIN          0
#define QOS_MF_PERFIX_IPV4_MAX          32
#define QOS_MF_PERFIX_IPV6_MIN          0
#define QOS_MF_PERFIX_IPV6_MAX          128
#define QOS_FLOW_ID_MIN                 0
#define QOS_FLOW_ID_MAX                 1048575

/* DCB Maximum Priorities */
#define QOS_DCB_MAX_PRIORITIES          8
#define QOS_PFC_PORT_CFG        0x01
#define QOS_PFC_DEL_PROFILE     0x02
#define QOS_PFC_THR_CFG         0x04
#define QOS_PFC_INVALID_ID        -1


/* DCB PFC Priority Counters */
enum {
    PFC_STATS_RX_PFC_FRAME_PRI0 = 0,
    PFC_STATS_RX_PFC_FRAME_PRI1,
    PFC_STATS_RX_PFC_FRAME_PRI2,
    PFC_STATS_RX_PFC_FRAME_PRI3,
    PFC_STATS_RX_PFC_FRAME_PRI4,
    PFC_STATS_RX_PFC_FRAME_PRI5,
    PFC_STATS_RX_PFC_FRAME_PRI6,
    PFC_STATS_RX_PFC_FRAME_PRI7,
    PFC_STATS_TX_PFC_FRAME_PRI0,
    PFC_STATS_TX_PFC_FRAME_PRI1,
    PFC_STATS_TX_PFC_FRAME_PRI2,
    PFC_STATS_TX_PFC_FRAME_PRI3,
    PFC_STATS_TX_PFC_FRAME_PRI4,
    PFC_STATS_TX_PFC_FRAME_PRI5,
    PFC_STATS_TX_PFC_FRAME_PRI6,
    PFC_STATS_TX_PFC_FRAME_PRI7,
    PFC_STATS_ING_UTILIZATION,
    PFC_STATS_EGR_UTILIZATION,
    PFC_STATS_HDRM_UTILIZATION,
    PFC_STATS_PG_MIN,
    PFC_STATS_UC_Q_MIN,
    PFC_STATS_MC_Q_MIN,
    PFC_STATS_ING_TOTAL_BUFFER_LIMIT,
    PFC_STATS_EGR_TOTAL_BUFFER_LIMIT,
    PFC_STATS_ING_AVAILABLE_BUFFER_LIMIT,
    PFC_STATS_EGR_AVAILABLE_BUFFER_LIMIT,
    PFC_STATS_ING_BUFFER_UTILIZATION,
    PFC_STATS_EGR_BUFFER_UTILIZATION

};
/* Macros used by other modules to use priority map 
 * utils */
#define QOS_ADD_PRIORITY_MAP            1
#define QOS_DEL_PRIORITY_MAP            2
#define QOS_UPDATE_PRIORITY_MAP         3
#define QOS_GET_PRIORITY_MAP            4
/* Traffic Separation and Control */
#define  QOS_ACL_METER_INTERVAL         (1000) /* 1000 Millisecond */
#define  QOS_ACL_L2_CONFIG              (1)
#define  QOS_ACL_L3_CONFIG              (2)

/* Preference for DSCP over P-Bit */
#define QOS_PBIT_PREF_ENABLE 1
#define QOS_PBIT_PREF_DISABLE 2

/*  1. Priority Map Table  - Regen Inner Priority Value*/
#define QOS_REGEN_INNER_PRIORITY_MIN_VAL       0
#define QOS_REGEN_INNER_PRIORITY_MAX_VAL       8
#define QOS_IN_DEI_MIN_VAL       0
#define QOS_IN_DEI_MAX_VAL       1
#define QOS_REGEN_COLOR_MIN_VAL       0
#define QOS_REGEN_COLOR_MAX_VAL       2
#if defined(VTSS_SERVAL_1) || defined(VTSS_SERVAL_2)
#define QOS_MAX_SCHED_PER_PORT         (2)
#else
#define QOS_MAX_SCHED_PER_PORT         (0xff)
#endif

/*The order of this enum should not be disturbed
 * To add any new macro please add below the last
 * macro in enum . The order is important because
 * in the array (QosFuncBlockTbl) index search will yield
 * different result if order is changed. */
typedef enum
{
 DS_CLFR = 1,
 DS_CLFR_ELEM,
 DS_MF_CLFR,
 DS_CLFR_FLOW,
 DS_METER,
 DS_TBPARM,
 DS_ACTION,
 DS_COUNT_ACT,
 DS_ALG_DROP,
 DS_RANDOM_DROP,
 DS_QUEUE,
 DS_SCHEDULER,
 DS_MINRATE,
 DS_STDDSCP,
 DS_MAXRATE,
 DS_DATAPATH,
 DS_NONE
} eQosFuncBlockIds;

typedef enum
{
DS_TB_STB = 1,
DS_TB_AVGRATE,
DS_TB_SR_TCM_BLIND,
DS_TB_SR_TCM_AWARE,
DS_TB_TR_TCM_BLIND,
DS_TB_TR_TCM_AWARE,
DS_TB_TSQ_TCM
} eQosTBParamTypes;

typedef enum
{
DS_SCHED_SP = 1,
DS_SCHED_WRR,
DS_SCHED_WFQ
} eQosSchedMethods;

/***********************************************
 *     Traffic Separation and Control          *
 **********************************************/
/* Data structure for QoS parameters*/
typedef struct _tQoSId
{
    UINT4   u4ClassMapId;       /* Class-map identifier */
    UINT4   u4ClassId;          /* Class identifier */
    UINT4   u4MeterId;          /* Meter identifier */
    UINT4   u4PolicyMapId;      /* Policy Map identifier */
} tQoSId;

/* Data structure for ACL and QoS MFC mapping */
typedef  struct  _tAclQosMap
{
    tQoSId    QosIds;          /* QOS Id'd */
    UINT4     u4FilterId;      /* L2 or L3 Filter ID */
    UINT4     u4ProtocolRate;  /* Meter rate per protocol */
    UINT1     u1CpuQueueNo;    /* H/w queue no which cp to go */
    UINT1     au1dummy [3];    /* Padding */
}tAclQosMapInfo;
INT4
QosGetTypeIdFromOid PROTO ((tSNMP_OID_TYPE *Oid, UINT4 *u4Type, UINT4 *u4Id));


INT4
QosGetOidFromTypeId PROTO ((UINT4 u4Id,UINT4 u4Type,tSNMP_OID_TYPE *Oid));

INT4
QosSchedOidFromType PROTO ((UINT4 u4Type, tSNMP_OID_TYPE * Oid));

/* Used to verify the Filter is or not associated with Class Map. */
INT4
QoSUtlIsFilterMapToClsMapEntry PROTO ((UINT1 u1FilterType, UINT4 u4FilterId));

INT4
QoSUtlIsClassMaptoQMapEntry PROTO ((UINT4 u4CLASS));

INT4
QoSDelDefDataPathTblEntries PROTO ((INT4 i4IfIndex, INT4 i4Direction));

/*15. Port Default User Priority (PDUP) Table */


/*****************************************************************************/
/* FileName : qosapi.c  api functions prototype                              */
/*****************************************************************************/

INT4
QosPortOperIndication  PROTO ((UINT4 u2Port, UINT1 u1Status));

INT4
QosDeletePort PROTO ((UINT4 u2Port));

INT4
QosCreatePort PROTO ((UINT4 u2Port));
INT4 QosApiAddMfcRateLimiter PROTO 
     ((UINT4 u4FilterType, tAclQosMapInfo *pAclQosMapInfo));
INT4 QosApiDeleteMfcRateLimiter PROTO 
     ((UINT4 u4FilterType, tAclQosMapInfo *pAclQosMapInfo));

INT4 QosUpdateDiffServClfrNextFree PROTO ((UINT4 u4Index , INT4 i4Status));
INT4 QosUpdDSClfrElementNextFree PROTO ((UINT4 u4Index ,INT4 i4Status));
INT4 QosUpdateDiffServMFCNextFree PROTO ((UINT4 u4Index , INT4 i4Status));
INT4 QosUpdateDiffServMeterNextFree PROTO ((UINT4 u4Index , INT4 i4Status));
INT4 QosUpdateDiffServTBParamNextFree PROTO ((UINT4 u4Index , INT4 i4Status));
INT4 QosUpdateDiffServActionNextFree PROTO ((UINT4 u4Index , INT4 i4Status));
INT4 QosUpdateDiffServCountActNextFree PROTO ((UINT4 u4Index , INT4 i4Status));
INT4 QosUpdateDiffServAlgoDropNextFree PROTO ((UINT4 u4Index , INT4 i4Status));
INT4 QosUpdateDiffServRandomDropNextFree PROTO ((UINT4 u4Index , INT4 i4Status));
INT4 QosUpdateDiffServQNextFree PROTO ((UINT4 u4Index , INT4 i4Status));
INT4 QosUpdateDiffServMinRateNextFree PROTO ((UINT4 u4Index , INT4 i4Status));
INT4 QosUpdateDiffServMaxRateNextFree PROTO ((UINT4 u4Index , UINT4 u4Level,  INT4 i4Status));
INT4 QosUpdateDiffServSchedulerNextFree  PROTO ((UINT4 u4Index , INT4 i4Status));
INT4 QosShapeTemplateUpdateNextFree PROTO ((UINT4 u4Index, INT4 i4Status)); 
INT4 QosGetSchedTypeFromOid PROTO ((tSNMP_OID_TYPE *Oid, UINT4 *u4Type));
INT4 QosGetTBparamTypeFromOid PROTO ((tSNMP_OID_TYPE *Oid, UINT4 *u4Type));
#ifdef MBSM_WANTED
INT4 QosxProcessMbsmMessage PROTO ((tMbsmProtoMsg * pProtoMsg, INT4 i4Event));
#endif
UINT1 QoSRegenerateVlanPriority PROTO ((UINT4 u4IfIndex,UINT2 u2VlanId,
                                              UINT1 u1Priority));
INT4 QoSApiModuleStart PROTO((VOID));
INT4 QoSApiModuleShutdown PROTO((VOID));

UINT4 
QosApiGetPortPriToTCMapping PROTO ((UINT4 u4Port, UINT1 u1VlanPri,
                                   UINT4 *pu4TrafficClass));
UINT4  
QosApiHandlePriorityMapRequest PROTO ((UINT4 u4Port, UINT1 u1InPri,
                                       UINT1 *pu1OutPri, UINT1 u1Request));
typedef struct StdQosHwEntry{
    tPortListExt IngresPortList;
    INT4     i4IssL3MultiFieldClfrAddrType;
    UINT4    u4IssL3FilterDstIpAddr;
    UINT4    u4IssL3FilterMultiFieldClfrDstPrefixLength;
    UINT4    u4IssL3FilterSrcIpAddr;
    UINT4    u4IssL3FilterMultiFieldClfrSrcPrefixLength;
    INT4     i4IssL3FilterDscp;
    UINT4    u4IssL3MultiFieldClfrFlowId;
    UINT4    u4IssL3FilterProtocol;
    UINT4    u4IssL3FilterMinDstProtPort;
    UINT4    u4IssL3FilterMaxDstProtPort;
    UINT4    u4IssL3FilterMinSrcProtPort;
    UINT4    u4IssL3FilterMaxSrcProtPort;
    UINT4    u4MeterType;
    UINT4    u4MeterCIR;
    UINT4    u4MeterCBS;
    UINT4    u4MeterInterval;
    UINT4    u4ActionType;
    UINT4    u4DscpValue;
    UINT4    u4ConformActionType;
    UINT4    u4ConformDscpValue;
    UINT4    u4ViolateActionType;
    UINT4    u4ViolateDscpValue;
    UINT4    u4ExceedActionType;
    UINT4    u4ExceedDscpValue;
    UINT4    u4MeterEIR;
    UINT4    u4MeterEBS;
    UINT4    u4FPHandle;
}tStdQosHwEntry;
typedef struct _tQosPfcHwEntry
{
    UINT4 u4Ifindex;          /* Interface Index */
    UINT4 au4PriToQueMap[QOS_DCB_MAX_PRIORITIES]; 
                                        /* Queue Id for the Priorities */
    UINT4 u4PfcMinThreshold;    /* Minimum Threshold */
    UINT4 u4PfcMaxThreshold;    /* Maximum Threshold */
    UINT4 u4PfcMaxFrameSize;    /* Maximum Frame Size */
    INT4  i4PfcHwProfileId;     /* Hardware Profile Id */
    UINT1 u1PfcProfileBmp;     /* Profile for this port */
    UINT1 u1PfcPriority;         /* Priority for which 
                                            PFC is enabled/disabled */
    UINT1 u1PfcHwCfgFlag;           /* Hardware Flag */
    UINT1 u1RefCount;
}tQosPfcHwEntry;

typedef struct _tQosClassMapPriInfo
{
    UINT4       u4IfIndex;     /* Port Number*/
    UINT4       u4FilterId;    /* Filter Id created for Application 
                                  to Priority Mapping */
    UINT4       u4ClassMapId;  /* Class Map Id corresponding to the
                                  Filter Id */
    UINT1       u1FilterType;  /*ISS_L2_FILTER or ISS_L3_FILTER */
    UINT1       u1Priority;    /* Priority */
    UINT1       au1Padding[2]; /* Padding */
}tQosClassMapPriInfo;

typedef struct _tQosEtsHwEntry
{
   UINT4 u4IfIndex;     /* Interface Index */
   UINT4 u4PriIdx;      /* Priority Index*/
   UINT1 u1ETSBw;       /* BW to be configured */
   UINT1 u1ETSTsa;      /* TSA to be configured */
   UINT1 u1EtsPortStatus; /* Enable/Disable status */
   UINT1 u1EtsTCGID; /* TCG ID*/
}tQosEtsHwEntry;

typedef struct _tQosPfcStats
{
    UINT4 u4IfIndex;       /* Interface Index */
    UINT4 u4PfcPauseCount; /* PFC Pause Count */
    UINT1 u1PfcStatsType;  /* PFC Statistics Type */
    UINT1 au1Padding[3];   /* Padding */
}tQosPfcStats;

/*API's Called by DCB Module*/
INT4 QosApiConfigEts PROTO
                ((UINT4 u4IfIndex,   /*Interface Index*/
                  UINT4 u4PriIdx,    /*Prioirty Index - To find Queue*/
                  UINT1 u1EtsTCGID,  /*TCGID - one to one mapping with queue*/
                  UINT2 u2EtsBw,     /*Bandwidth for Queue(TCGID) */
                  UINT1 u1EtsPortStatus)); /*ETS Enabled Status*/
INT4 QosApiConfigPfc PROTO ((
                  tQosPfcHwEntry *pPfcHwEntry));  /*PFC Hardware Entry*/
INT4 QosApiGetMaxTCSupport PROTO ((
                  UINT4 u4IfIndex,    /*Interface Index*/
                  UINT4 *pu4MaxNumTcSupport));/*No of Traffic Class Supported*/

INT4
QosApiConfigAppPri PROTO ((tQosClassMapPriInfo *pClassMapPriInfo, 
                           UINT1 u1FilterAction));
INT4
QosApiConfigEtsTsa (tQosEtsHwEntry * pPortEntry);  /* Api to Confugure TSA to Hw */

INT4
QosApiGetQoSPortDefUsrPri(INT4 i4IfIndex, UINT1 *u1UDPri);


INT4 QoSDeleteTables PROTO ((VOID));
INT4 QoSCreateTables PROTO ((VOID));
INT4 QoSAddDefTblEntries PROTO ((VOID));
INT4 QoSAddDefPcpTblEntries PROTO ((INT4 i4IfIndex));
INT4 QoSAddDefPcpPriMapTblEntries PROTO ((INT4 i4IfIndex, UINT4 u4StartIndex));
INT4 QoSAddDefPcpPlyMapTblEntries PROTO ((INT4 i4IfIndex, UINT4 u4StartIndex,
                                          UINT4 u4PriMapIdx));
INT4 QoSAddDefPcpQMapTblEntries PROTO ((INT4 i4IfIdx));
    
    
INT4 QoSSetDefCpuQTblEntries PROTO ((VOID));
INT4 QosGetMplsExpProfileForPortAtIngress PROTO ((UINT4 u4IfNum,
                                             INT4 *pi4HwExpMapId));
INT4 QosGetMplsExpProfileForPortAtEgress PROTO ((UINT4 u4IfNum,
                                             INT4 *pi4HwExpMapId));

INT4
QosApiCreateSchedular (UINT4 u4IfIndex, UINT4 u4SchedulerId,
                       UINT4 u4HierarLevel, UINT1 u1Tsa);
INT4
QosApiUpdateSchedularAlgo (UINT4 u4IfIndex, UINT4 u4SchedulerId, UINT1 u1Tsa);
INT4
QosApiDeleteSchedular (UINT4 u4IfIndex, UINT4 u4SchedulerId);
INT4
QosApiAttachQueue (UINT4 u4IfIndex, UINT4 u4QueueId,
                   UINT4 u4SchedulerId, UINT1 u1EtsTsa, UINT2 u2EtsBw);
INT4
QosApiDeAttachQueue (UINT4 u4IfIndex, UINT4 u4QueueId);
INT4
QosApiQoSAddDefHierarchyTblEntries (UINT4 u4IfIndex);

INT4
QosApiQosHwGetPfcStats PROTO ((tQosPfcStats *pQosPfcStats));
VOID 
QosApiValidatePlyMapConfigFlag PROTO ((UINT4 u4QoSPolicyMapId, BOOL1 *pb1PlyMapFlg));
VOID
QosApiValidateClsMapConfigFlag PROTO ((UINT4 u4QoSClassMapId, BOOL1 *pb1ClsMapFlg));
#endif /* _QOSX_H */

