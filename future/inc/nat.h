/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: nat.h,v 1.26 2012/12/07 10:12:13 siva Exp $
 *
 * Description:This file contains the exported definitions and  
 *             macros of NAT                                   
 *
 *******************************************************************/
#ifndef _NAT_H
#define _NAT_H

#define   NAT_TIMER_EXPIRY_EVENT    0x00000008
/* UPNP NAPT TIMER */
#define   NAPT_TIMER_EXPIRY_EVENT    0x00800000
#define NAPT_TIMER_LIST_ID           321      
/* END */

/* SIPLAG Timer related */
#define   NAT_SIPALG_TIMER_EXPIRY_EVENT    0x08000000
#define NAT_SIPALG_TIMER_LIST_ID           322      
/* SIPALG */

/* NAT Partial link flags */
#define NAT_SIP_PERSISTENT  3
#define NAT_PERSISTENT      4
#define NAT_NON_PERSISTENT  5
#define APP_DYNAMIC_ENTRY_NOT_CREATED 0
#define APP_DYNAMIC_ENTRY_CREATED     1
#define NATIPC_PORT_NUM_ZERO          0

/* End NAT Partial link flags */

/* NAT routine's return codes */
#define   NAT_SUCCESS                        1
#define   NAT_FAILURE                        2

#define   NAT_INBOUND                             1
#define   NAT_OUTBOUND                            2
#define   NAT_ENABLE                              1
#define   NAT_DISABLE                             2
#define   NAT_FORWARD                             3
#define   NAT_PROTO_ANY                          255

#define NAT_ANY                                0
#define NAT_OUT_PARTIAL                        1
#define NAT_IN_PARTIAL                         2
#define NAT_NOT_ON_HOLD                        1
#define NAT_ON_HOLD                            2

#define   NAT_SEQ_ACK_HISTORY          1    /* Size of Delta table*/

#define  NAT_MAX_PORT_ALLOWED     16
#define  NAT_MAX_APP_ALLOWED      32 
#define  NAT_TRIG_ENTRY_FREE      0
#define  NAT_TRIG_ENTRY_AVAIL     1
#define  NAT_TRIG_ENTRY_RSVD      2
#define  NAT_MAX_APP_NAME_LEN     64
#define  NAT_MAX_PORT_STRING_LEN  256

#define  NAT_TCP                  6
#define  NAT_UDP                  17

/* Nat Time Out Values */
#define   NAT_DEF_IDLE_TIMEOUT       60
#define   NAT_DEF_TCP_TIMEOUT        3600
#define   NAT_DEF_UDP_TIMEOUT        300

#define   NAT_MIN_TCP_TIMEOUT        300
#define   NAT_MAX_IDLE_TIMEOUT       86400
#define   NAT_MAX_TCP_TIMEOUT        86400
#define   NAT_MAX_UDP_TIMEOUT        86400

/* Sip Alg Partial Entry Timer*/
#define   NAT_SIP_ALG_PARTIAL_ENTRY_TIMER 212 
#define   NAT_MIN_SIP_ALG_TIMEOUT    212 
#define   NAT_MAX_SIP_ALG_TIMEOUT    86400
/* Sip Alg Port*/
#define   NAT_SIP_DEFAULT_ALG_PORT   5060
#define   NAT_MIN_SIP_ALG_PORT       1024
#define   NAT_MAX_SIP_ALG_PORT       65535

/* Nat debug levels */
#define   NAT_DBG_ENTRY       0x00010000
#define   NAT_DBG_EXIT        0x00020000
#define   NAT_DBG_INTMD       0x00040000
#define   NAT_DBG_ABNX        0x00080000
#define   NAT_DBG_PKT_FLOW    0x00100000
#define   NAT_DBG_TABLE       0x00200000
#define   NAT_DBG_DNS         0x00400000
#define   NAT_DBG_FTP         0x00800000
#define   NAT_DBG_HTTP        0x01000000
#define   NAT_DBG_SMTP        0x02000000
#define   NAT_DBG_ICMP        0x04000000
#define   NAT_DBG_MEM         0x08000000
#define   NAT_DBG_LLR         0x10000000
#define   NAT_DBG_PPTP        0x20000000
#define   NAT_DBG_ALL           UTL_DBG_ALL         

#define   NAT_LOCAL                               1
#define   NAT_GLOBAL                              2
#define   NAT_OUTSIDE                             3
#define   NAT_INSIDE                              4

/**** Type Definition for Global Hash Table Node *****/

typedef struct GlobalHash {
    tTMO_SLL_NODE  GlobalHash;
    UINT4          u4TranslatedLocIpAddr;
    UINT2          u2TranslatedLocPort;
    UINT2          u2Reserved;
    tTMO_SLL       pIidList;
}tGlobalHashNode;

typedef struct sNatSeqAckHistory {
   UINT4         u4InSeq;
   UINT4         u4InSeqDelta;
   UINT4         u4OutSeq;
   UINT4         u4OutSeqDelta;
   INT4          i4InDelta;
   INT4          i4OutDelta;
   UINT1         u1Status;
   UINT1         au1Pad[3]; /* For Padding only */
   UINT4         u4Direction;
}tNatSeqAckHistory;


typedef struct DynamicTableEntry {
    tTMO_SLL_NODE  DynamicTableEntry;
    UINT4          u4IfNum;
    UINT4          u4LocIpAddr;
    UINT4          u4TranslatedLocIpAddr;
    UINT4          u4OutIpAddr;
    UINT4          u4TimeStamp;
    UINT2          u2LocPort;
    UINT2          u2TranslatedLocPort;
    UINT2          u2OutPort;
    UINT1          u1PktType;
    UINT1          u1NaptEnable;
    struct NatAppDefn *pAppRec;
    tNatSeqAckHistory aNatSeqAckHistory[NAT_SEQ_ACK_HISTORY];
    UINT1          u1DeltaChangeFlag;
    UINT1          u1DeltaIndex;
    UINT1          u1AppCallStatus;
    UINT1          u1Type;
    UINT1          u1UsedFlag;
    UINT1          u1Direction;
    /*
     * u1RetainOnTmrExpiry Flag is used by SIP. If This Flag is Set to TRUE
     * NAT Will Not Delete This Dynamic Entry on Timer Expiry
     */
    UINT1          u1RetainOnTmrExpiry;
    UINT1          u1Pad;
}
tDynamicEntry;

/**** Type Definition for Nat Partial links *****/

typedef struct sNatPartialLinkNode {
    tTMO_SLL_NODE  sNatPartialLinkNode;
    UINT4          u4TranslatedLocIpAddr;
    UINT4          u4OutIpAddr;
    UINT4          u4LocIpAddr;
    UINT4          u4TimeStamp;
    UINT2          u2TranslatedLocPort;
    UINT2          u2OutPort;
    UINT2          u2LocPort;
    UINT2          u2Reserved;
    UINT1          u1PktType;
    UINT1          u1Direction;
    UINT1          u1PersistFlag;
    UINT1          u1AppCallStatus;      /* Flag to indicate call is placed in hold or not */
}tNatPartialLinkNode ;

typedef struct portrange {
   UINT2 u2StartPort;
   UINT2 u2EndPort;
}tPortRange;   

typedef struct triggerinfo {
   UINT1 au1AppName[NAT_MAX_APP_NAME_LEN];
   tPortRange  aOutPortRange[NAT_MAX_PORT_ALLOWED];
   tPortRange  aInPortRange[NAT_MAX_PORT_ALLOWED];
   UINT1       au1OutPortString[NAT_MAX_PORT_STRING_LEN];
   UINT1       au1InPortString[NAT_MAX_PORT_STRING_LEN];
   UINT2       u2Protocol; 
   UINT1       u1Status; 
   UINT1       u1RsvdInfoStatus;
}tTriggerInfo;   

typedef struct reservedtriginfo {
   tTriggerInfo *pTrigInfo;
   UINT4         u4LocalIpAddr;
   UINT4         u4RemoteIpAddr;
   UINT4         u4TimeStamp;
   UINT1         u1Status;
   UINT1         au1Pad[3];
}tRsvdTrigInfo;   

typedef enum {
   PARITY_ANY,      /* 0 */
   PARITY_EVEN,     /* 1 */
   PARITY_ODD       /* 2 */
} eParity;

typedef enum {
  BIND_TYPE_SIGNAL,   /* 0 */
  BIND_TYPE_MEDIA     /* 1 */
} eBindType;

typedef struct NatWanUaHashNode
{
    tTMO_HASH_NODE NatWanUaHashNode;
    UINT4  u4OutIpAdd;
    UINT2  u2OutBasePort;
    UINT2  u2TotalBindings;
    eParity eTransBasePortParity;
    UINT2  u2OutProtocol;
    UINT1  au1Padding[2];
} tNatWanUaHashNode;

typedef struct sSipAlgTasklet
{
    tTMO_SLL_NODE SipAlgTaskletNode;
    UINT4               u4Arg;
}tSipAlgTaskletData;

#define   NAT_DEL_ALL_HASH_BINDINGS        0
#define   NAT_SIZE_OF_BLK                  1500

PUBLIC VOID
SipUpdateInterfaceStatus PROTO((UINT2 u4IfIndex, UINT4 u4IpAddr, UINT1 u1NwType,
                                 UINT1 *pu1InterfaceName, UINT4 u4Status));
extern tTriggerInfo aTrigInfo[];
extern tRsvdTrigInfo aRsvdTrigInfo[];
extern tTMO_HASH_TABLE    *gpNatWanUaHashTbl;    /* Used to store IP,BasePort Passed
                                                   During OpenPinHoles */


INT4 NatInit (INT1 *pi1Dummy);
INT4 CfaGetWanInterfaceIndex PROTO ((VOID));  /* MSAD ADD */

INT4 NatClearAllInterfaceBindings PROTO(( INT4 i4IfIndex ));
PUBLIC UINT4 NatApiGetPortMapTimeout PROTO((VOID));
PUBLIC INT4  NatSipNotifyPinholeTuple PROTO ((CONST UINT1 **ppu1VarName, 
                                              CONST UINT1 **ppu1VarVal,
                                              UINT1 u1Count));

INT4
NatUtilUpdateStaticNaptEntry      PROTO ((UINT4, INT4, UINT2, UINT4,
                                          UINT2, UINT1, UINT1));
UINT4
NatUpdatePartialList (tNatWanUaHashNode *pNatWanUaHashNode, UINT4 u4Direction,
                       UINT4 u4NatIpAdd, UINT2 u2NatBasePort, UINT1 u1PktType);

extern  UINT4 NatCheckIfNatEnable ARG_LIST ((UINT4 u4IfNum));
extern  UINT4 NatSearchStaticTable ARG_LIST((UINT4 u4IpAddr, 
                                             UINT4 u4Direction,
                                             UINT4 u4IfNum));
extern  UINT4 NatSearchGlobalIpAddrTable ARG_LIST((UINT4 u4GlobalIpAddr,
                                                         UINT4 u4IfNum ));
INT1 NatHandleInterfaceIndication PROTO ((UINT4 u4Interface, UINT4 u4IpAddress,
                                           UINT4 u4Status));
PUBLIC INT1
NatSetAutoVirSerForWanInterface PROTO ((UINT4 u4Interface, UINT4 u4IpAddress,
                                        UINT2 u2ServerPort, UINT2 u2Protocol));
PUBLIC INT1 
NatRemoveVirSerOnWanInterface PROTO ((UINT4 u4Interface, UINT4 u4IpAddress,
                                      UINT2 u2ServerPort, 
                                      UINT2 u2ProtocolNumber)); 

#ifdef UPNP_WANTED
PUBLIC VOID
NatUpdateInterfaceStatus PROTO((UINT2 u4IfIndex, UINT4 u4IpAddr, UINT1 u1NwType, 
                                UINT1 u1OperStatus, UINT1 *pu1InterfaceName,
                                UINT4 u4Status));
#endif

INT1 NatAutoVirtualServerConfig PROTO ((UINT4 u4IfIndex, UINT4 u4IpAddress));

/* natapi.c */
UINT4
NatApiHandleConfigTimeout       PROTO ((UINT4));
UINT4
NatApiHandleCleanAllBinds       PROTO ((VOID));
UINT4
NatApiGetWanLinkStatus          PROTO ((UINT4, UINT1*, UINT4*, UINT4*));
UINT4
NatApiGetNoOfWanLinks           PROTO ((UINT4*));
UINT4
NatApiGetAllWanLinkStatus       PROTO ((UINT4*, UINT1**, UINT4*, UINT4*));
INT4
NatApiCheckInterfaceNatEnabled  PROTO ((INT4*, UINT4));
UINT4
NatApiAddPortMappingEntry       PROTO ((UINT4, UINT4, INT4, INT4, 
                                        UINT4, INT4*, INT4, UINT2, INT4, 
                                        UINT1, UINT1*, UINT4*, tDynamicEntry*));
UINT4
NatApiDelPortMappingEntry      PROTO ((INT4, UINT4, UINT4, UINT4, INT4, UINT2, UINT2, 
                                       UINT4 *));

INT4
NatApiHandleOpenPinhole         PROTO ((INT4, UINT4, UINT2,
                                        UINT2, UINT2, UINT4, 
                                        UINT2, UINT1, UINT4*));
INT4
NatApiHandleClosePinhole        PROTO ((INT4, UINT4, UINT2,
                                        UINT2, UINT2, UINT4, 
                                        UINT2, UINT1, UINT4*));
UINT2
NatApiNaptSearchDynamicList     PROTO ((UINT4, UINT2, UINT4, UINT1, tDynamicEntry** ));

UINT4
NatApiUpdateDynamicCallStatus   PROTO ((UINT4, UINT2, UINT4, UINT2, UINT1, 
                                       UINT1, UINT1));
INT4
CasGetIpRouteInfo               PROTO ((UINT4, UINT4*, UINT4*, UINT1*));    

PUBLIC INT4
NatGetFreeGlobalPortGroup (UINT2 *, UINT4 , eParity , eBindType );

PUBLIC INT4
NatReleaseGlobalPort            PROTO ((UINT2));
 
INT1
NatDeleteSpecifiedEntry         PROTO ((UINT4 u4IfNum, UINT4 u4LocIpAddr, 
                                        UINT4 u4TransIpAddr, UINT2 u2LocPort, 
                                        UINT2 u2TransPort));
   
PUBLIC UINT4 
NatUtilIsVPNPassthroughTraffic PROTO ((UINT1 *pu1InitiatorCookie));

PUBLIC UINT4
NatUtilIsIkeNattPassthroughTraffic PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf));

#define NAT_NAPT_DESCRN_LEN 20

#ifdef FLOWMGR_WANTED

#define NAT_FL_UTIL_UPDATE_FLOWTYPE_FLOWDATA(pbuf) \
FlUtilUpdateFlowTypeFlowData(pbuf, FLOW_CTRL_FLOW)

#define FL_NAT_UPDATE_FLOWDATA_NAT_TUPLE(pBuf)    FlNatUpdateFlowDataNatTuple(pBuf)

#define FL_NAT_DEL_HW_ENTRIES(u4LocIp,u4OutIp,u4TransIp,u2LocPort,u2OutPort,u2TransPort,u1PktType) \
FlNatDelHwEntries(u4LocIp,u4OutIp,u4TransIp,u2LocPort,u2OutPort,u2TransPort,u1PktType)

#define NAT_FL_UTIL_GET_TIMESTAMP(u4LocIp,u4OutIp,u2LocPort,u2OutPort,u1PktType) FlUtilGetTimestamp(u4LocIp,u4OutIp,u2LocPort,u2OutPort,u1PktType)

#define FL_NAT_DEL_HW_PORTTRIGGER_ENTRIES(RsvdTrigInfo) \
FlNatDelHwPortTriggerEntries(RsvdTrigInfo)
    
#define FL_NAT_GET_PORTTRIG_TIMESTAMP() FlNatGetPortTrigTimestamp()
    
#define FL_NAT_DEL_HW_NFS_ENTRIES(u4LocIp,u4OutIp,u2LocPort,u2OutPort) \
FlNatDelHwNfsEntries(u4LocIp,u4OutIp,u2LocPort,u2OutPort)
    
#define NAT_FL_UTIL_GET_NFS_TIMESTAMP(u4LocIp,u4OutIp,u2LocPort,u2OutPort) \
FlNatGetNfsTimestamp(u4LocIp,u4OutIp,u2LocPort,u2OutPort)

#define NAT_FL_UTIL_DEL_IPSEC_HW_ENTRIES(u4LocIp,u4OutIp,u4SPI,u1Proto) FlUtilDelIpsecEntry(u4LocIp,u4OutIp,u4SPI,u1Proto)

#define NAT_FL_UTIL_GET_IPSEC_TIMESTAMP(u4LocIp,u4OutIp,u4SPI,u1Proto) FlUtilGetIpsecTimestamp(u4LocIp,u4OutIp,u4SPI,u1Proto)

#define FL_NAT_GET_PPTP_TIMESTAMP(u4LocIp,u4OutIp,u4TransIp,u2LocPort,u2OutPort,u2TransPort) FlNatGetPptpTimestamp(u4LocIp,u4OutIp,u4TransIp,u2LocPort,u2OutPort,u2TransPort)

#else

#define NAT_FL_UTIL_UPDATE_FLOWTYPE_FLOWDATA(pbuf) UNUSED_PARAM(pbuf)
#define FL_NAT_UPDATE_FLOWDATA_NAT_TUPLE(pBuf)
#define FL_NAT_DEL_HW_ENTRIES(u4LocIp,u4OutIp,u4TransIp,u2LocPort,u2OutPort,u2TransPort,u1PktType)  
#define NAT_FL_UTIL_GET_TIMESTAMP(u4LocIp,u4OutIp,u2LocPort,u2OutPort,u1PktType)  OSIX_FAILURE
#define FL_NAT_DEL_HW_PORTTRIGGER_ENTRIES(RsvdTrigInfo)  
#define FL_NAT_GET_PORTTRIG_TIMESTAMP()   
#define FL_NAT_DEL_HW_NFS_ENTRIES(u4LocIp,u4OutIp,u2LocPort,u2OutPort)  
#define NAT_FL_UTIL_GET_NFS_TIMESTAMP(u4LocIp,u4OutIp,u2LocPort,u2OutPort) \
    OSIX_FAILURE
#define NAT_FL_UTIL_DEL_IPSEC_HW_ENTRIES(u4LocIp,u4OutIp,u4SPI,u1Proto)

#define NAT_FL_UTIL_GET_IPSEC_TIMESTAMP(u4LocIp,u4OutIp,u4SPI,u1Proto) OSIX_FAILURE
#define FL_NAT_GET_PPTP_TIMESTAMP(u4LocIp,u4OutIp,u4TransIp,u2LocPort,u2OutPort,u2TransPort) OSIX_FAILURE
#endif /* FLOWMGR_WANTED */
INT4
NatUtilGetGlobalNatStatus PROTO((VOID));
INT1
NatInterfaceEntryCreate PROTO((UINT4 , UINT4 )); 
PUBLIC UINT4
NatFormHashKey ARG_LIST ((UINT4 u4IpAddr, UINT2 u2Port, UINT4 u4Type));

PUBLIC VOID
NatDeleteDynamicEntry ARG_LIST((UINT4 u4IfNum));

PUBLIC VOID NatDeInitForIf ARG_LIST((UINT4 u4IfNum));
PUBLIC  UINT4 NatCheckIfNaptEnable ARG_LIST ((UINT4 u4IfNum));
PUBLIC INT4 NatTranslateInBoundPkt (tCRU_BUF_CHAIN_HEADER ** ppBuf, 
                  UINT4 u4InIf,INT1 *pi1FrgReass);
PUBLIC INT4 NatTranslateOutBoundPkt (tCRU_BUF_CHAIN_HEADER ** ppBuf, UINT4 u4OutIf,INT1 *pi1FrgReass);
PUBLIC VOID NatProcessTimeOut (VOID);
UINT4 NatCheckIfAddMcastEntry PROTO ((UINT4));
VOID NaptProcessTimeOut PROTO((tTimerListId));
#ifndef SECURITY_KERNEL_WANTED
PUBLIC VOID NatSipAlgProcessTimeOut (tTimerListId TimerListId);
#else
PUBLIC VOID NatSipAlgProcessTimeOut (FS_ULONG);
#endif
#define NAT_LOCK()               NatLock ()
#define NAT_UNLOCK()             NatUnLock ()
INT4 NatLock PROTO((VOID));
INT4 NatUnLock PROTO((VOID));
INT1 NatApiSearchPolicyTable PROTO((UINT4 ));
#endif /* _NAT_H */
