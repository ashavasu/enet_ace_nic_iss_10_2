
/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: mplsnpwr.h,v 1.8 2015/11/12 12:25:28 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              for MPLS wrappers
 *              
 ********************************************************************/

#ifndef __MPLS_NP_WR_H__
#define __MPLS_NP_WR_H__

#include "ipnp.h"
#include "ip6np.h"
#include "mplsnp.h"

UINT1 MplsFsNpIpv4UcDelRoute PROTO ((UINT4 u4VrId, UINT4 u4IpDestAddr, UINT4 u4IpSubNetMask, tFsNpNextHopInfo routeEntry, INT4 *pi4FreeDefIpB4Del));



/* OPER ID */
#ifdef MPLS_WANTED
#ifndef _MPLSNP_C_
#define  FS_MPLS_HW_ENABLE_MPLS                                 1
#define  FS_MPLS_HW_DISABLE_MPLS                                2
#define  FS_MPLS_HW_GET_PW_CTRL_CHNL_CAPABILITIES               3
#define  FS_MPLS_HW_CREATE_PW_VC                                4
#define  FS_MPLS_HW_TRAVERSE_PW_VC                              5
#define  FS_MPLS_HW_DELETE_PW_VC                                6
#define  FS_MPLS_HW_CREATE_I_L_M                                7
#define  FS_MPLS_HW_TRAVERSE_I_L_M                              8
#define  FS_MPLS_HW_DELETE_I_L_M                                9
#define  FS_MPLS_HW_CREATE_L3_F_T_N                             10
#define  FS_MPLS_HW_TRAVERSE_L3_F_T_N                           11
#ifdef NP_BACKWD_COMPATIBILITY
#define  FS_MPLS_HW_DELETE_L3_F_T_N                             12
#else
#define  FS_MPLS_HW_DELETE_L3_F_T_N                             13
#endif
#define  FS_MPLS_WP_HW_DELETE_L3_F_T_N                          14
#define  FS_MPLS_HW_ENABLE_MPLS_IF                              15
#define  FS_MPLS_HW_DISABLE_MPLS_IF                             16
#define  FS_MPLS_HW_CREATE_VPLS_VPN                             17
#define  FS_MPLS_HW_DELETE_VPLS_VPN                             18
#define  FS_MPLS_HW_VPLS_ADD_PW_VC                              19
#define  FS_MPLS_HW_VPLS_DELETE_PW_VC                           20
#define  FS_MPLS_HW_VPLS_FLUSH_MAC                              21
#ifdef HVPLS_WANTED
#define  FS_MPLS_REGISTER_FWD_ALARM                             22
#define  FS_MPLS_DE_REGISTER_FWD_ALARM                          23
#endif
#define  NP_MPLS_CREATE_MPLS_INTERFACE                          24
#define  FS_MPLS_HW_ADD_MPLS_ROUTE                              25
#define  FS_MPLS_HW_ADD_MPLS_IPV6_ROUTE                         26
#define  FS_MPLS_HW_DELETE_MPLS_IPV6_ROUTE                      27
#define  FS_MPLS_HW_GET_MPLS_STATS                              28
#define  FS_MPLS_HW_ADD_VPN_AC                                  29
#define  FS_MPLS_HW_DELETE_VPN_AC                               30
#define  FS_MPLS_HW_P2MP_ADD_I_L_M                              31
#define  FS_MPLS_HW_P2MP_REMOVE_I_L_M                           32
#define  FS_MPLS_HW_P2MP_TRAVERSE_I_L_M                         33
#define  FS_MPLS_HW_P2MP_ADD_BUD                                34
#define  FS_MPLS_HW_P2MP_REMOVE_BUD                             35
#define  FS_MPLS_HW_MODIFY_PW_VC                                36
#ifdef MPLS_L3VPN_WANTED
#define  FS_MPLS_HW_L3VPN_INGRESS_MAP                           37
#define  FS_MPLS_HW_L3VPN_EGRESS_MAP                            38
#endif /* MPLS_L3VPN_WANTED */
#define  FS_MPLS_HW_GET_PW_VC                                   39
#define  FS_MPLS_HW_GET_I_L_M                                   40
#define  FS_MPLS_HW_GET_L3_F_T_N                                41
#define  FS_MPLS_HW_GET_VPN_AC                                 42

#endif
#endif
#ifdef MPLS_WANTED
#ifndef _MPLSNPX_C_
#define  FS_MPLS_MBSM_HW_ENABLE_MPLS                            43
#define  FS_MPLS_MBSM_HW_DISABLE_MPLS                           44
#define  FS_MPLS_MBSM_HW_GET_PW_CTRL_CHNL_CAPABILITIES          45
#define  FS_MPLS_MBSM_HW_CREATE_PW_VC                           46
#define  FS_MPLS_MBSM_HW_DELETE_PW_VC                           47
#define  FS_MPLS_MBSM_HW_CREATE_I_L_M                           48
#define  FS_MPLS_MBSM_HW_DELETE_I_L_M                           49
#define  FS_MPLS_MBSM_HW_CREATE_L3_F_T_N                        50
#define  FS_MPLS_MBSM_HW_DELETE_L3_F_T_N                        51
#define  FS_MPLS_MBSM_HW_CREATE_VPLS_VPN                        52
#define  FS_MPLS_MBSM_HW_DELETE_VPLS_VPN                        53
#define  FS_MPLS_MBSM_HW_VPLS_ADD_PW_VC                         54
#define  FS_MPLS_MBSM_HW_VPLS_DELETE_PW_VC                      55
#define  FS_MPLS_MBSM_HW_ADD_VPN_AC                             56
#define  FS_MPLS_MBSM_HW_DELETE_VPN_AC                          57
#define  NP_MPLS_MBSM_CREATE_MPLS_INTERFACE                     58
#define  FS_MPLS_MBSM_HW_P2MP_ADD_I_L_M                         59
#define  FS_MPLS_MBSM_HW_P2MP_REMOVE_I_L_M                      60
#define  FS_MPLS_MBSM_HW_GET_VPN_AC                                                   61
#define  FS_MPLS_MBSM_HW_GET_PW_VC                                                    62
#define  FS_MPLS_MBSM_HW_GET_I_L_M                                                    63
#define  FS_MPLS_MBSM_HW_GET_L3_F_T_N                                                 64

#ifdef MPLS_L3VPN_WANTED
#define  FS_MPLS_MBSM_HW_L3VPN_INGRESS_MAP                      65
#define  FS_MPLS_MBSM_HW_L3VPN_EGRESS_MAP                       66

#endif /* MPLS_WANTED */
#endif
#endif

/* Required arguments list for the mpls NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

#ifdef MPLS_WANTED
#ifndef _MPLSNP_C_
typedef struct {
    UINT1 *  pu1PwCCTypeCapabs;
} tMplsNpWrFsMplsHwGetPwCtrlChnlCapabilities;

typedef struct {
    UINT4               u4VpnId;
    tMplsHwVcTnlInfo *  pMplsHwVcInfo;
    UINT4               u4Action;
} tMplsNpWrFsMplsHwCreatePwVc;

typedef struct {
    tMplsHwVcTnlInfo *  pMplsHwVcInfo;
    tMplsHwVcTnlInfo *  pNextMplsHwVcInfo;
} tMplsNpWrFsMplsHwTraversePwVc;

typedef struct {
    UINT4               u4VpnId;
    tMplsHwVcTnlInfo *  pMplsHwDelVcInfo;
    UINT4               u4Action;
} tMplsNpWrFsMplsHwDeletePwVc;

typedef struct {
    tMplsHwIlmInfo *  pMplsHwIlmInfo;
    UINT4             u4Action;
} tMplsNpWrFsMplsHwCreateILM;

typedef struct {
    tMplsHwIlmInfo *  pMplsHwIlmInfo;
    tMplsHwIlmInfo *  pNextMplsHwIlmInfo;
} tMplsNpWrFsMplsHwTraverseILM;

typedef struct {
    tMplsHwIlmInfo *  pMplsHwDelIlmParams;
    UINT4             u4Action;
} tMplsNpWrFsMplsHwDeleteILM;

typedef struct {
    UINT4               u4VpnId;
    tMplsHwL3FTNInfo *  pMplsHwL3FTNInfo;
} tMplsNpWrFsMplsHwCreateL3FTN;

typedef struct {
    tMplsHwL3FTNInfo *  pMplsHwL3FTNInfo;
    tMplsHwL3FTNInfo *  pNextMplsHwL3FTNInfo;
} tMplsNpWrFsMplsHwTraverseL3FTN;
typedef struct {
    UINT4               u4VpnId;
    UINT4               u4IfIndex;
 tMplsHwL3FTNInfo *  pMplsHwL3FTNInfo;
    tMplsHwL3FTNInfo *  pNextMplsHwL3FTNInfo;
} tMplsNpWrFsMplsHwGetL3FTN;



#ifdef NP_BACKWD_COMPATIBILITY
typedef struct {
    UINT4  u4L3EgrIntf;
} tMplsNpWrFsMplsHwDeleteL3FTN;

#else
typedef struct {
    UINT4               u4VpnId;
    tMplsHwL3FTNInfo *  pMplsHwL3FTNInfo;
} tMplsNpWrFsMplsHwDeleteL3FTN;

#endif
typedef struct {
    UINT4               u4VpnId;
    tMplsHwL3FTNInfo *  pMplsHwL3FTNInfo;
} tMplsNpWrFsMplsWpHwDeleteL3FTN;

typedef struct {
    UINT4                 u4Intf;
    tMplsIntfParamsSet *  pMplsIntfParamsSet;
} tMplsNpWrFsMplsHwEnableMplsIf;

typedef struct {
    UINT4  u4Intf;
} tMplsNpWrFsMplsHwDisableMplsIf;

typedef struct {
    UINT4              u4VplsInstance;
    UINT4              u4VpnId;
    tMplsHwVplsInfo *  pMplsHwVplsVpnInfo;
} tMplsNpWrFsMplsHwCreateVplsVpn;

typedef struct {
    UINT4              u4VplsInstance;
    UINT4              u4VpnId;
    tMplsHwVplsInfo *  pMplsHwVplsVpnInfo;
} tMplsNpWrFsMplsHwDeleteVplsVpn;

typedef struct {
    UINT4              u4VplsInstance;
    UINT4              u4VpnId;
    tMplsHwVplsInfo *  pMplsHwVplsVcInfo;
    UINT4              u4Action;
} tMplsNpWrFsMplsHwVplsAddPwVc;

typedef struct {
    UINT4              u4VplsInstance;
    UINT4              u4VpnId;
    tMplsHwVplsInfo *  pMplsHwVplsVcInfo;
    UINT4              u4Action;
} tMplsNpWrFsMplsHwVplsDeletePwVc;

typedef struct {
    UINT4              u4VplsInstance;
    UINT4              u4VpnId;
    tMplsHwVplsInfo *  pMplsHwVplsVpnInfo;
} tMplsNpWrFsMplsHwVplsFlushMac;
#ifdef HVPLS_WANTED
typedef struct {
    UINT4              u4VpnId;
    tMplsHwVplsInfo *  pMplsHwVplsVpnInfo;
} tMplsNpWrFsMplsRegisterFwdAlarm;
typedef struct {
    UINT4              u4VpnId;
} tMplsNpWrFsMplsDeRegisterFwdAlarm;
#endif
typedef struct {
    tMplsHwMplsIntInfo *  pMplsHwMplsIntInfo; 
} tMplsNpWrNpMplsCreateMplsInterface;

typedef struct {
    UINT4             u4VrId;
    UINT4             u4IpDestAddr;
    UINT4             u4IpSubNetMask;
    tFsNpNextHopInfo  routeEntry;
    UINT1 *           pbu1TblFull;
} tMplsNpWrFsMplsHwAddMplsRoute;

typedef struct {
    UINT4           u4VrId;
    UINT1 *         pu1Ip6Prefix;
    UINT1 *         pu1NextHop;
    UINT4           u4NHType;
    tFsNpIntInfo *  pIntInfo;
    UINT1           u1PrefixLen;
    UINT1      au1pad[3];
} tMplsNpWrFsMplsHwAddMplsIpv6Route;

typedef struct {
    UINT4    u4IfIndex;
    UINT4    u4VrId;
    UINT1 *  pu1Ip6Prefix;
    UINT1 *  pu1NextHop;
    tFsNpRouteInfo * pRouteInfo;
    UINT1    u1PrefixLen;
    UINT1   au1pad[3];
} tMplsNpWrFsMplsHwDeleteMplsIpv6Route;



typedef struct {
    tMplsInputParams *  pInputParams;
    tStatsType          StatsType;
    tStatsInfo *        pStatsInfo;
} tMplsNpWrFsMplsHwGetMplsStats;

typedef struct {
    UINT4              u4VplsInstance;
    UINT4              u4VpnId;
    tMplsHwVplsInfo *  pMplsHwVplsVcInfo;
    UINT4              u4Action;
} tMplsNpWrFsMplsHwAddVpnAc;

typedef struct {
    UINT4              u4VplsInstance;
    UINT4              u4VpnId;
    tMplsHwVplsInfo *  pMplsHwVplsVcInfo;
    UINT4              u4Action;
} tMplsNpWrFsMplsHwDeleteVpnAc;
typedef struct {
    UINT4              u4VplsInstance;
    UINT4              u4VpnId;
    tMplsHwVplsInfo *  pMplsHwVplsVcInfo;
} tMplsNpWrFsMplsHwGetVpnAc;

typedef struct {
    UINT4             u4P2mpId;
    tMplsHwIlmInfo *  pMplsHwP2mpIlmInfo;
} tMplsNpWrFsMplsHwP2mpAddILM;

typedef struct {
    UINT4             u4P2mpId;
    tMplsHwIlmInfo *  pMplsHwP2mpIlmInfo;
} tMplsNpWrFsMplsHwP2mpRemoveILM;

typedef struct {
    UINT4             u4P2mpId;
    tMplsHwIlmInfo *  pMplsHwP2mpIlmInfo;
    tMplsHwIlmInfo *  pNextMplsHwP2mpIlmInfo;
} tMplsNpWrFsMplsHwP2mpTraverseILM;

typedef struct {
    UINT4  u4P2mpId;
    UINT4  u4Action;
} tMplsNpWrFsMplsHwP2mpAddBud;

typedef struct {
    UINT4  u4P2mpId;
    UINT4  u4Action;
} tMplsNpWrFsMplsHwP2mpRemoveBud;

typedef struct {
    UINT4               u4VpnId;
    UINT4               u4OperActVpnId;
    tMplsHwVcTnlInfo *  pMplsHwVcInfo;
} tMplsNpWrFsMplsHwModifyPwVc;

#ifdef MPLS_L3VPN_WANTED
typedef struct {
    tMplsHwL3vpnIgressInfo *  pMplsHwL3vpnIgressInfo;
} tMplsNpWrFsMplsHwL3vpnIngressMap;

typedef struct {
    tMplsHwL3vpnEgressInfo *  pMplsHwL3vpnEgressInfo;
} tMplsNpWrFsMplsHwL3vpnEgressMap;

#endif /* MPLS_WANTED */
#endif
#endif
#ifdef MPLS_WANTED
#ifndef _MPLSNPX_C_
typedef struct {
    tMbsmSlotInfo *  pSlotInfo;
} tMplsNpWrFsMplsMbsmHwEnableMpls;

typedef struct {
    tMbsmSlotInfo *  pSlotInfo;
} tMplsNpWrFsMplsMbsmHwDisableMpls;

typedef struct {
    UINT1 *  pu1PwCCTypeCapabs;
} tMplsNpWrFsMplsMbsmHwGetPwCtrlChnlCapabilities;

typedef struct {
    UINT4               u4VpnId;
    tMplsHwVcTnlInfo *  pMplsHwVcInfo;
    UINT4               u4Action;
    tMbsmSlotInfo *     pSlotInfo;
} tMplsNpWrFsMplsMbsmHwCreatePwVc;

typedef struct {
    UINT4               u4VpnId;
    tMplsHwVcTnlInfo *  pMplsHwDelVcInfo;
    UINT4               u4Action;
    tMbsmSlotInfo *     pSlotInfo;
} tMplsNpWrFsMplsMbsmHwDeletePwVc;

typedef struct {
    tMplsHwIlmInfo *  pMplsHwIlmInfo;
    UINT4             u4Action;
    tMbsmSlotInfo *   pSlotInfo;
} tMplsNpWrFsMplsMbsmHwCreateILM;

typedef struct {
    tMplsHwIlmInfo *  pMplsHwDelIlmParams;
    UINT4             u4Action;
    tMbsmSlotInfo *   pSlotInfo;
} tMplsNpWrFsMplsMbsmHwDeleteILM;

typedef struct {
    UINT4               u4VpnId;
    tMplsHwL3FTNInfo *  pMplsHwL3FTNInfo;
    tMbsmSlotInfo *     pSlotInfo;
} tMplsNpWrFsMplsMbsmHwCreateL3FTN;

typedef struct {
    UINT4            u4L3EgrIntf;
    tMbsmSlotInfo *  pSlotInfo;
} tMplsNpWrFsMplsMbsmHwDeleteL3FTN;

typedef struct {
    UINT4              u4VplsInstance;
    UINT4              u4VpnId;
    tMplsHwVplsInfo *  pMplsHwVplsVpnInfo;
    tMbsmSlotInfo *    pSlotInfo;
} tMplsNpWrFsMplsMbsmHwCreateVplsVpn;

typedef struct {
    UINT4              u4VplsInstance;
    UINT4              u4VpnId;
    tMplsHwVplsInfo *  pMplsHwVplsVpnInfo;
    tMbsmSlotInfo *    pSlotInfo;
} tMplsNpWrFsMplsMbsmHwDeleteVplsVpn;

typedef struct {
    UINT4              u4VplsInstance;
    UINT4              u4VpnId;
    tMplsHwVplsInfo *  pMplsHwVplsVcInfo;
    UINT4              u4Action;
    tMbsmSlotInfo *    pSlotInfo;
} tMplsNpWrFsMplsMbsmHwVplsAddPwVc;

typedef struct {
    UINT4              u4VplsInstance;
    UINT4              u4VpnId;
    tMplsHwVplsInfo *  pMplsHwVplsVcInfo;
    UINT4              u4Action;
    tMbsmSlotInfo *    pSlotInfo;
} tMplsNpWrFsMplsMbsmHwVplsDeletePwVc;

typedef struct {
    UINT4              u4VplsInstance;
    UINT4              u4VpnId;
    tMplsHwVplsInfo *  pMplsHwVplsVcInfo;
    UINT4              u4Action;
    tMbsmSlotInfo *    pSlotInfo;
} tMplsNpWrFsMplsMbsmHwAddVpnAc;

typedef struct {
    UINT4              u4VplsInstance;
    UINT4              u4VpnId;
    tMplsHwVplsInfo *  pMplsHwVplsVcInfo;
    UINT4              u4Action;
    tMbsmSlotInfo *    pSlotInfo;
} tMplsNpWrFsMplsMbsmHwDeleteVpnAc;

typedef struct {
    tMplsHwMplsIntInfo *  pMplsHwMplsIntInfo;
    tMbsmSlotInfo *  pSlotInfo;
} tMplsNpWrNpMplsMbsmCreateMplsInterface;

typedef struct {
    UINT4             u4P2mpId;
    tMplsHwIlmInfo *  pMplsHwP2mpIlmInfo;
    tMbsmSlotInfo *   pSlotInfo;
} tMplsNpWrFsMplsMbsmHwP2mpAddILM;

typedef struct {
    UINT4             u4P2mpId;
    tMplsHwIlmInfo *  pMplsHwP2mpIlmInfo;
    tMbsmSlotInfo *   pSlotInfo;
} tMplsNpWrFsMplsMbsmHwP2mpRemoveILM;

typedef struct {
    UINT4              u4VplsInstance;
    UINT4              u4VpnId;
    tMplsHwVplsInfo *  pMplsHwVplsVcInfo;
    tMbsmSlotInfo *    pSlotInfo;
} tMplsNpWrFsMplsMbsmHwGetVpnAc;

typedef struct {
    tMplsHwVcTnlInfo *  pMplsHwVcInfo;
    tMplsHwVcTnlInfo *  pNextMplsHwVcInfo;
    tMbsmSlotInfo *     pSlotInfo;
} tMplsNpWrFsMplsMbsmHwTraversePwVc;

typedef struct {
    tMplsHwIlmInfo *  pMplsHwIlmInfo;
    tMplsHwIlmInfo *  pNextMplsHwIlmInfo;
    tMbsmSlotInfo *   pSlotInfo;
} tMplsNpWrFsMplsMbsmHwTraverseILM;

typedef struct {
    UINT4             u4VpnId;
    UINT4             u4IfIndex;
    tMplsHwL3FTNInfo  *pMplsHwL3FTNInfo;
    tMbsmSlotInfo *   pSlotInfo;
} tMplsNpWrFsMplsMbsmHwGetL3FTN;

#ifdef MPLS_L3VPN_WANTED
typedef struct {
    tMplsHwL3vpnIgressInfo *  pMplsHwL3vpnIgressInfo;
    tMbsmSlotInfo *           pSlotInfo;
} tMplsNpWrFsMplsMbsmHwL3vpnIngressMap;

typedef struct {
    tMplsHwL3vpnEgressInfo *  pMplsHwL3vpnEgressInfo;
    tMbsmSlotInfo *           pSlotInfo;
} tMplsNpWrFsMplsMbsmHwL3vpnEgressMap;

#endif /* MPLS_WANTED */
#endif
#endif
typedef struct MplsNpModInfo {
union {
#ifdef MPLS_WANTED
#ifndef _MPLSNP_C_
    tMplsNpWrFsMplsHwGetPwCtrlChnlCapabilities  sFsMplsHwGetPwCtrlChnlCapabilities;
    tMplsNpWrFsMplsHwCreatePwVc  sFsMplsHwCreatePwVc;
    tMplsNpWrFsMplsHwTraversePwVc  sFsMplsHwTraversePwVc;
    tMplsNpWrFsMplsHwDeletePwVc  sFsMplsHwDeletePwVc;
    tMplsNpWrFsMplsHwCreateILM  sFsMplsHwCreateILM;
    tMplsNpWrFsMplsHwTraverseILM  sFsMplsHwTraverseILM;
    tMplsNpWrFsMplsHwDeleteILM  sFsMplsHwDeleteILM;
    tMplsNpWrFsMplsHwCreateL3FTN  sFsMplsHwCreateL3FTN;
    tMplsNpWrFsMplsHwTraverseL3FTN  sFsMplsHwTraverseL3FTN;
    tMplsNpWrFsMplsHwGetL3FTN       sFsMplsHwGetL3FTN; 
#ifdef NP_BACKWD_COMPATIBILITY
    tMplsNpWrFsMplsHwDeleteL3FTN  sFsMplsHwDeleteL3FTN;
#else
    tMplsNpWrFsMplsHwDeleteL3FTN  sFsMplsHwDeleteL3FTN;
#endif
    tMplsNpWrFsMplsWpHwDeleteL3FTN  sFsMplsWpHwDeleteL3FTN;
    tMplsNpWrFsMplsHwEnableMplsIf  sFsMplsHwEnableMplsIf;
    tMplsNpWrFsMplsHwDisableMplsIf  sFsMplsHwDisableMplsIf;
    tMplsNpWrFsMplsHwCreateVplsVpn  sFsMplsHwCreateVplsVpn;
    tMplsNpWrFsMplsHwDeleteVplsVpn  sFsMplsHwDeleteVplsVpn;
    tMplsNpWrFsMplsHwVplsAddPwVc  sFsMplsHwVplsAddPwVc;
    tMplsNpWrFsMplsHwVplsDeletePwVc  sFsMplsHwVplsDeletePwVc;
    tMplsNpWrFsMplsHwVplsFlushMac  sFsMplsHwVplsFlushMac;
#ifdef HVPLS_WANTED
    tMplsNpWrFsMplsRegisterFwdAlarm sFsMplsRegisterFwdAlarm;
    tMplsNpWrFsMplsDeRegisterFwdAlarm sFsMplsDeRegisterFwdAlarm;
#endif
    tMplsNpWrNpMplsCreateMplsInterface  sNpMplsCreateMplsInterface;
    tMplsNpWrFsMplsHwAddMplsRoute  sFsMplsHwAddMplsRoute;
    tMplsNpWrFsMplsHwAddMplsIpv6Route  sFsMplsHwAddMplsIpv6Route;
    tMplsNpWrFsMplsHwDeleteMplsIpv6Route  sFsMplsHwDeleteMplsIpv6Route;
    tMplsNpWrFsMplsHwGetMplsStats  sFsMplsHwGetMplsStats;
    tMplsNpWrFsMplsHwAddVpnAc  sFsMplsHwAddVpnAc;
    tMplsNpWrFsMplsHwDeleteVpnAc  sFsMplsHwDeleteVpnAc;
    tMplsNpWrFsMplsHwGetVpnAc  sFsMplsHwGetVpnAc;
    tMplsNpWrFsMplsHwP2mpAddILM  sFsMplsHwP2mpAddILM;
    tMplsNpWrFsMplsHwP2mpRemoveILM  sFsMplsHwP2mpRemoveILM;
    tMplsNpWrFsMplsHwP2mpTraverseILM  sFsMplsHwP2mpTraverseILM;
    tMplsNpWrFsMplsHwP2mpAddBud  sFsMplsHwP2mpAddBud;
    tMplsNpWrFsMplsHwP2mpRemoveBud  sFsMplsHwP2mpRemoveBud;
    tMplsNpWrFsMplsHwModifyPwVc  sFsMplsHwModifyPwVc;
#ifdef MPLS_L3VPN_WANTED
    tMplsNpWrFsMplsHwL3vpnIngressMap  sFsMplsHwL3vpnIngressMap;
    tMplsNpWrFsMplsHwL3vpnEgressMap  sFsMplsHwL3vpnEgressMap;
#endif /* MPLS_WANTED */
#endif
#endif
#ifdef MPLS_WANTED
#ifndef _MPLSNPX_C_
    tMplsNpWrFsMplsMbsmHwEnableMpls  sFsMplsMbsmHwEnableMpls;
    tMplsNpWrFsMplsMbsmHwDisableMpls  sFsMplsMbsmHwDisableMpls;
    tMplsNpWrFsMplsMbsmHwGetPwCtrlChnlCapabilities  sFsMplsMbsmHwGetPwCtrlChnlCapabilities;
    tMplsNpWrFsMplsMbsmHwCreatePwVc  sFsMplsMbsmHwCreatePwVc;
    tMplsNpWrFsMplsMbsmHwDeletePwVc  sFsMplsMbsmHwDeletePwVc;
    tMplsNpWrFsMplsMbsmHwCreateILM  sFsMplsMbsmHwCreateILM;
    tMplsNpWrFsMplsMbsmHwDeleteILM  sFsMplsMbsmHwDeleteILM;
    tMplsNpWrFsMplsMbsmHwCreateL3FTN  sFsMplsMbsmHwCreateL3FTN;
    tMplsNpWrFsMplsMbsmHwDeleteL3FTN  sFsMplsMbsmHwDeleteL3FTN;
    tMplsNpWrFsMplsMbsmHwCreateVplsVpn  sFsMplsMbsmHwCreateVplsVpn;
    tMplsNpWrFsMplsMbsmHwDeleteVplsVpn  sFsMplsMbsmHwDeleteVplsVpn;
    tMplsNpWrFsMplsMbsmHwVplsAddPwVc  sFsMplsMbsmHwVplsAddPwVc;
    tMplsNpWrFsMplsMbsmHwVplsDeletePwVc  sFsMplsMbsmHwVplsDeletePwVc;
    tMplsNpWrFsMplsMbsmHwAddVpnAc  sFsMplsMbsmHwAddVpnAc;
    tMplsNpWrFsMplsMbsmHwDeleteVpnAc  sFsMplsMbsmHwDeleteVpnAc;
    tMplsNpWrNpMplsMbsmCreateMplsInterface  sNpMplsMbsmCreateMplsInterface;
    tMplsNpWrFsMplsMbsmHwP2mpAddILM  sFsMplsMbsmHwP2mpAddILM;
    tMplsNpWrFsMplsMbsmHwP2mpRemoveILM  sFsMplsMbsmHwP2mpRemoveILM;
    tMplsNpWrFsMplsMbsmHwGetVpnAc       sFsMplsMbsmHwGetVpnAc;
    tMplsNpWrFsMplsMbsmHwTraversePwVc   sFsMplsMbsmHwTraversePwVc;
    tMplsNpWrFsMplsMbsmHwTraverseILM    sFsMplsMbsmHwTraverseILM;
    tMplsNpWrFsMplsMbsmHwGetL3FTN       sFsMplsMbsmHwGetL3FTN;
#ifdef MPLS_L3VPN_WANTED
    tMplsNpWrFsMplsMbsmHwL3vpnIngressMap  sFsMplsMbsmHwL3vpnIngressMap;
    tMplsNpWrFsMplsMbsmHwL3vpnEgressMap  sFsMplsMbsmHwL3vpnEgressMap;
#endif /* MPLS_WANTED */
#endif
#endif
    }unOpCode;

#ifdef MPLS_WANTED
#ifndef _MPLSNP_C_
#define  MplsNpFsMplsHwGetPwCtrlChnlCapabilities  unOpCode.sFsMplsHwGetPwCtrlChnlCapabilities;
#define  MplsNpFsMplsHwCreatePwVc  unOpCode.sFsMplsHwCreatePwVc;
#define  MplsNpFsMplsHwTraversePwVc  unOpCode.sFsMplsHwTraversePwVc;
#define  MplsNpFsMplsHwDeletePwVc  unOpCode.sFsMplsHwDeletePwVc;
#define  MplsNpFsMplsHwCreateILM  unOpCode.sFsMplsHwCreateILM;
#define  MplsNpFsMplsHwTraverseILM  unOpCode.sFsMplsHwTraverseILM;
#define  MplsNpFsMplsHwDeleteILM  unOpCode.sFsMplsHwDeleteILM;
#define  MplsNpFsMplsHwCreateL3FTN  unOpCode.sFsMplsHwCreateL3FTN;
#define  MplsNpFsMplsHwTraverseL3FTN  unOpCode.sFsMplsHwTraverseL3FTN;
#define  MplsNpFsMplsHwGetL3FTN       unOpCode.sFsMplsHwGetL3FTN
#ifdef NP_BACKWD_COMPATIBILITY
#define  MplsNpFsMplsHwDeleteL3FTN  unOpCode.sFsMplsHwDeleteL3FTN;
#else
#define  MplsNpFsMplsHwDeleteL3FTN  unOpCode.sFsMplsHwDeleteL3FTN;
#endif
#define  MplsNpFsMplsWpHwDeleteL3FTN  unOpCode.sFsMplsWpHwDeleteL3FTN;
#define  MplsNpFsMplsHwEnableMplsIf  unOpCode.sFsMplsHwEnableMplsIf;
#define  MplsNpFsMplsHwDisableMplsIf  unOpCode.sFsMplsHwDisableMplsIf;
#define  MplsNpFsMplsHwCreateVplsVpn  unOpCode.sFsMplsHwCreateVplsVpn;
#define  MplsNpFsMplsHwDeleteVplsVpn  unOpCode.sFsMplsHwDeleteVplsVpn;
#define  MplsNpFsMplsHwVplsAddPwVc  unOpCode.sFsMplsHwVplsAddPwVc;
#define  MplsNpFsMplsHwVplsDeletePwVc  unOpCode.sFsMplsHwVplsDeletePwVc;
#define  MplsNpFsMplsHwVplsFlushMac  unOpCode.sFsMplsHwVplsFlushMac;
#ifdef HVPLS_WANTED
#define  MplsNpFsMplsRegisterFwdAlarm   unOpCode.sFsMplsRegisterFwdAlarm;
#define MplsNpFsMplsDeRegisterFwdAlarm    unOpCode.sFsMplsDeRegisterFwdAlarm;
#endif
#define  MplsNpNpMplsCreateMplsInterface  unOpCode.sNpMplsCreateMplsInterface;
#define  MplsNpFsMplsHwAddMplsRoute  unOpCode.sFsMplsHwAddMplsRoute;
#define  MplsNpFsMplsHwAddMplsIpv6Route  unOpCode.sFsMplsHwAddMplsIpv6Route;
#define  MplsNpFsMplsHwDeleteMplsIpv6Route  unOpCode.sFsMplsHwDeleteMplsIpv6Route;
#define  MplsNpFsMplsHwGetMplsStats  unOpCode.sFsMplsHwGetMplsStats;
#define  MplsNpFsMplsHwAddVpnAc  unOpCode.sFsMplsHwAddVpnAc;
#define  MplsNpFsMplsHwDeleteVpnAc  unOpCode.sFsMplsHwDeleteVpnAc;
#define  MplsNpFsMplsHwGetVpnAc  unOpCode.sFsMplsHwGetVpnAc;
#define  MplsNpFsMplsHwP2mpAddILM  unOpCode.sFsMplsHwP2mpAddILM;
#define  MplsNpFsMplsHwP2mpRemoveILM  unOpCode.sFsMplsHwP2mpRemoveILM;
#define  MplsNpFsMplsHwP2mpTraverseILM  unOpCode.sFsMplsHwP2mpTraverseILM;
#define  MplsNpFsMplsHwP2mpAddBud  unOpCode.sFsMplsHwP2mpAddBud;
#define  MplsNpFsMplsHwP2mpRemoveBud  unOpCode.sFsMplsHwP2mpRemoveBud;
#define  MplsNpFsMplsHwModifyPwVc  unOpCode.sFsMplsHwModifyPwVc;
#ifdef MPLS_L3VPN_WANTED
#define  MplsNpFsMplsHwL3vpnIngressMap  unOpCode.sFsMplsHwL3vpnIngressMap;
#define  MplsNpFsMplsHwL3vpnEgressMap  unOpCode.sFsMplsHwL3vpnEgressMap;
#endif /* MPLS_WANTED */
#endif
#endif
#ifdef MPLS_WANTED
#ifndef _MPLSNPX_C_
#define  MplsNpFsMplsMbsmHwEnableMpls  unOpCode.sFsMplsMbsmHwEnableMpls;
#define  MplsNpFsMplsMbsmHwDisableMpls  unOpCode.sFsMplsMbsmHwDisableMpls;
#define  MplsNpFsMplsMbsmHwGetPwCtrlChnlCapabilities  unOpCode.sFsMplsMbsmHwGetPwCtrlChnlCapabilities;
#define  MplsNpFsMplsMbsmHwCreatePwVc  unOpCode.sFsMplsMbsmHwCreatePwVc;
#define  MplsNpFsMplsMbsmHwDeletePwVc  unOpCode.sFsMplsMbsmHwDeletePwVc;
#define  MplsNpFsMplsMbsmHwCreateILM  unOpCode.sFsMplsMbsmHwCreateILM;
#define  MplsNpFsMplsMbsmHwDeleteILM  unOpCode.sFsMplsMbsmHwDeleteILM;
#define  MplsNpFsMplsMbsmHwCreateL3FTN  unOpCode.sFsMplsMbsmHwCreateL3FTN;
#define  MplsNpFsMplsMbsmHwDeleteL3FTN  unOpCode.sFsMplsMbsmHwDeleteL3FTN;
#define  MplsNpFsMplsMbsmHwCreateVplsVpn  unOpCode.sFsMplsMbsmHwCreateVplsVpn;
#define  MplsNpFsMplsMbsmHwDeleteVplsVpn  unOpCode.sFsMplsMbsmHwDeleteVplsVpn;
#define  MplsNpFsMplsMbsmHwVplsAddPwVc  unOpCode.sFsMplsMbsmHwVplsAddPwVc;
#define  MplsNpFsMplsMbsmHwVplsDeletePwVc  unOpCode.sFsMplsMbsmHwVplsDeletePwVc;
#define  MplsNpFsMplsMbsmHwAddVpnAc  unOpCode.sFsMplsMbsmHwAddVpnAc;
#define  MplsNpFsMplsMbsmHwDeleteVpnAc  unOpCode.sFsMplsMbsmHwDeleteVpnAc;
#define  MplsNpNpMplsMbsmCreateMplsInterface  unOpCode.sNpMplsMbsmCreateMplsInterface;
#define  MplsNpFsMplsMbsmHwP2mpAddILM  unOpCode.sFsMplsMbsmHwP2mpAddILM;
#define  MplsNpFsMplsMbsmHwP2mpRemoveILM  unOpCode.sFsMplsMbsmHwP2mpRemoveILM;
#define  MplsNpFsMplsMbsmHwGetVpnAc     unOpCode.sFsMplsMbsmHwGetVpnAc;
#define  MplsNpFsMplsMbsmHwTraversePwVc unOpCode.sFsMplsMbsmHwTraversePwVc;
#define  MplsNpFsMplsMbsmHwTraverseILM  unOpCode.sFsMplsMbsmHwTraverseILM;
#define  MplsNpFsMplsMbsmHwGetL3FTN     unOpCode.sFsMplsMbsmHwGetL3FTN;
#ifdef MPLS_L3VPN_WANTED
#define  MplsNpFsMplsMbsmHwL3vpnIngressMap  unOpCode.sFsMplsMbsmHwL3vpnIngressMap;
#define  MplsNpFsMplsMbsmHwL3vpnEgressMap  unOpCode.sFsMplsMbsmHwL3vpnEgressMap;
#endif /* MPLS_WANTED */
#endif
#endif
} tMplsNpModInfo;


/* Function Prototype for Generic NP OpCode specific wrappers
 * defined in Mplsnpapi.c */

#ifdef MPLS_WANTED
#ifndef _MPLSNP_C_
UINT1 MplsFsMplsHwEnableMpls PROTO ((VOID));
UINT1 MplsFsMplsHwDisableMpls PROTO ((VOID));
UINT1 MplsFsMplsHwGetPwCtrlChnlCapabilities PROTO ((UINT1 * pu1PwCCTypeCapabs));
UINT1 MplsFsMplsHwCreatePwVc PROTO ((UINT4 u4VpnId, tMplsHwVcTnlInfo * pMplsHwVcInfo, UINT4 u4Action));
UINT1 MplsFsMplsHwTraversePwVc PROTO ((tMplsHwVcTnlInfo * pMplsHwVcInfo, tMplsHwVcTnlInfo * pNextMplsHwVcInfo));
UINT1 MplsFsMplsHwGetPwVc PROTO ((tMplsHwVcTnlInfo * pMplsHwVcInfo, tMplsHwVcTnlInfo * pNextMplsHwVcInfo));
UINT1 MplsFsMplsHwDeletePwVc PROTO ((UINT4 u4VpnId, tMplsHwVcTnlInfo * pMplsHwDelVcInfo, UINT4 u4Action));
UINT1 MplsFsMplsHwCreateILM PROTO ((tMplsHwIlmInfo * pMplsHwIlmInfo, UINT4 u4Action));
UINT1 MplsFsMplsHwTraverseILM PROTO ((tMplsHwIlmInfo * pMplsHwIlmInfo, tMplsHwIlmInfo * pNextMplsHwIlmInfo));
UINT1 MplsFsMplsHwGetILM PROTO ((tMplsHwIlmInfo * pMplsHwIlmInfo, tMplsHwIlmInfo * pNextMplsHwIlmInfo));
UINT1 MplsFsMplsHwDeleteILM PROTO ((tMplsHwIlmInfo * pMplsHwDelIlmParams, UINT4 u4Action));

UINT1 MplsFsMplsHwGetL3FTN PROTO ((UINT4 u4VpnId, UINT4 u4IfIndex, tMplsHwL3FTNInfo * pMplsHwL3FTNInfo));
UINT1 MplsFsMplsHwCreateL3FTN PROTO ((UINT4 u4VpnId, tMplsHwL3FTNInfo * pMplsHwL3FTNInfo));
UINT1 MplsFsMplsHwTraverseL3FTN PROTO ((tMplsHwL3FTNInfo * pMplsHwL3FTNInfo, tMplsHwL3FTNInfo * pNextMplsHwL3FTNInfo));
#ifdef NP_BACKWD_COMPATIBILITY
UINT1 MplsFsMplsHwDeleteL3FTN PROTO ((UINT4 u4L3EgrIntf));
#else
UINT1 MplsFsMplsHwDeleteL3FTN PROTO ((UINT4 u4VpnId, tMplsHwL3FTNInfo * pMplsHwL3FTNInfo));
#endif
UINT1 MplsFsMplsWpHwDeleteL3FTN PROTO ((UINT4 u4VpnId, tMplsHwL3FTNInfo * pMplsHwL3FTNInfo));
UINT1 MplsFsMplsHwEnableMplsIf PROTO ((UINT4 u4Intf, tMplsIntfParamsSet * pMplsIntfParamsSet));
UINT1 MplsFsMplsHwDisableMplsIf PROTO ((UINT4 u4Intf));
UINT1 MplsFsMplsHwCreateVplsVpn PROTO ((UINT4 u4VplsInstance, UINT4 u4VpnId, tMplsHwVplsInfo * pMplsHwVplsVpnInfo));
UINT1 MplsFsMplsHwDeleteVplsVpn PROTO ((UINT4 u4VplsInstance, UINT4 u4VpnId, tMplsHwVplsInfo * pMplsHwVplsVpnInfo));
UINT1 MplsFsMplsHwVplsAddPwVc PROTO ((UINT4 u4VplsInstance, UINT4 u4VpnId, tMplsHwVplsInfo * pMplsHwVplsVcInfo, UINT4 u4Action));
UINT1 MplsFsMplsHwVplsDeletePwVc PROTO ((UINT4 u4VplsInstance, UINT4 u4VpnId, tMplsHwVplsInfo * pMplsHwVplsVcInfo, UINT4 u4Action));
UINT1 MplsFsMplsHwVplsFlushMac PROTO ((UINT4 u4VplsInstance, UINT4 u4VpnId, tMplsHwVplsInfo * pMplsHwVplsVpnInfo));
#ifdef HVPLS_WANTED
UINT1 MplsFsMplsRegisterFwdAlarm PROTO ((UINT4 u4VpnId, tMplsHwVplsInfo * pMplsHwVplsVpnInfo));
UINT1 MplsFsMplsDeRegisterFwdAlarm PROTO ((UINT4 u4VpnId));
#endif
UINT1 MplsNpMplsCreateMplsInterface PROTO ((tMplsHwMplsIntInfo  * pMplsHwMplsIntInfo));
UINT1 MplsFsMplsHwAddMplsRoute PROTO ((UINT4 u4VrId, UINT4 u4IpDestAddr, UINT4 u4IpSubNetMask, tFsNpNextHopInfo routeEntry, UINT1 * pbu1TblFull));
UINT1 MplsFsMplsHwAddMplsIpv6Route PROTO ((UINT4 u4VrId, UINT1 * pu1Ip6Prefix, UINT1 u1PrefixLen, UINT1 * pu1NextHop, UINT4 u4NHType, tFsNpIntInfo * pIntInfo));
UINT1 MplsFsMplsHwDeleteMplsIpv6Route PROTO ((UINT4 u4VrId, UINT1 * pu1Ip6Prefix, UINT1 u1PrefixLen, UINT1 * pu1NextHop, UINT4 u4IfIndex,tFsNpRouteInfo *pRouteInfo));
UINT1 MplsFsMplsHwGetMplsStats PROTO ((tMplsInputParams * pInputParams, tStatsType StatsType, tStatsInfo * pStatsInfo));
UINT1 MplsFsMplsHwAddVpnAc PROTO ((UINT4 u4VplsInstance, UINT4 u4VpnId, tMplsHwVplsInfo * pMplsHwVplsVcInfo, UINT4 u4Action));
UINT1 MplsFsMplsHwDeleteVpnAc PROTO ((UINT4 u4VplsInstance, UINT4 u4VpnId, tMplsHwVplsInfo * pMplsHwVplsVcInfo, UINT4 u4Action));
UINT1 MplsFsMplsHwGetVpnAc PROTO ((UINT4 u4VplsInstance, UINT4 u4VpnId, tMplsHwVplsInfo * pMplsHwVplsVcInfo));
UINT1 MplsFsMplsHwP2mpAddILM PROTO ((UINT4 u4P2mpId, tMplsHwIlmInfo * pMplsHwP2mpIlmInfo));
UINT1 MplsFsMplsHwP2mpRemoveILM PROTO ((UINT4 u4P2mpId, tMplsHwIlmInfo * pMplsHwP2mpIlmInfo));
UINT1 MplsFsMplsHwP2mpTraverseILM PROTO ((UINT4 u4P2mpId, tMplsHwIlmInfo * pMplsHwP2mpIlmInfo, tMplsHwIlmInfo * pNextMplsHwP2mpIlmInfo));
UINT1 MplsFsMplsHwP2mpAddBud PROTO ((UINT4 u4P2mpId, UINT4 u4Action));
UINT1 MplsFsMplsHwP2mpRemoveBud PROTO ((UINT4 u4P2mpId, UINT4 u4Action));
UINT1 MplsFsMplsHwModifyPwVc PROTO ((UINT4 u4VpnId, UINT4 u4OperActVpnId, tMplsHwVcTnlInfo * pMplsHwVcInfo));
#ifdef MPLS_L3VPN_WANTED
UINT1 MplsFsMplsHwL3vpnIngressMap PROTO ((tMplsHwL3vpnIgressInfo * pMplsHwL3vpnIgressInfo));
UINT1 MplsFsMplsHwL3vpnEgressMap PROTO ((tMplsHwL3vpnEgressInfo * pMplsHwL3vpnEgressInfo));
#endif /* MPLS_WANTED */
#endif
#endif
#ifdef MPLS_WANTED
#ifndef _MPLSNPX_C_
UINT1 MplsFsMplsMbsmHwEnableMpls PROTO ((tMbsmSlotInfo * pSlotInfo));
UINT1 MplsFsMplsMbsmHwDisableMpls PROTO ((tMbsmSlotInfo * pSlotInfo));
UINT1 MplsFsMplsMbsmHwGetPwCtrlChnlCapabilities PROTO ((UINT1 * pu1PwCCTypeCapabs));
UINT1 MplsFsMplsMbsmHwCreatePwVc PROTO ((UINT4 u4VpnId, tMplsHwVcTnlInfo * pMplsHwVcInfo, UINT4 u4Action, tMbsmSlotInfo * pSlotInfo));
UINT1 MplsFsMplsMbsmHwDeletePwVc PROTO ((UINT4 u4VpnId, tMplsHwVcTnlInfo * pMplsHwDelVcInfo, UINT4 u4Action, tMbsmSlotInfo * pSlotInfo));
UINT1 MplsFsMplsMbsmHwCreateILM PROTO ((tMplsHwIlmInfo * pMplsHwIlmInfo, UINT4 u4Action, tMbsmSlotInfo * pSlotInfo));
UINT1 MplsFsMplsMbsmHwDeleteILM PROTO ((tMplsHwIlmInfo * pMplsHwDelIlmParams, UINT4 u4Action, tMbsmSlotInfo * pSlotInfo));
UINT1 MplsFsMplsMbsmHwCreateL3FTN PROTO ((UINT4 u4VpnId, tMplsHwL3FTNInfo * pMplsHwL3FTNInfo, tMbsmSlotInfo * pSlotInfo));
UINT1 MplsFsMplsMbsmHwDeleteL3FTN PROTO ((UINT4 u4L3EgrIntf, tMbsmSlotInfo * pSlotInfo));
UINT1 MplsFsMplsMbsmHwCreateVplsVpn PROTO ((UINT4 u4VplsInstance, UINT4 u4VpnId, tMplsHwVplsInfo * pMplsHwVplsVpnInfo, tMbsmSlotInfo * pSlotInfo));
UINT1 MplsFsMplsMbsmHwDeleteVplsVpn PROTO ((UINT4 u4VplsInstance, UINT4 u4VpnId, tMplsHwVplsInfo * pMplsHwVplsVpnInfo, tMbsmSlotInfo * pSlotInfo));
UINT1 MplsFsMplsMbsmHwVplsAddPwVc PROTO ((UINT4 u4VplsInstance, UINT4 u4VpnId, tMplsHwVplsInfo * pMplsHwVplsVcInfo, UINT4 u4Action, tMbsmSlotInfo * pSlotInfo));
UINT1 MplsFsMplsMbsmHwVplsDeletePwVc PROTO ((UINT4 u4VplsInstance, UINT4 u4VpnId, tMplsHwVplsInfo * pMplsHwVplsVcInfo, UINT4 u4Action, tMbsmSlotInfo * pSlotInfo));
UINT1 MplsFsMplsMbsmHwAddVpnAc PROTO ((UINT4 u4VplsInstance, UINT4 u4VpnId, tMplsHwVplsInfo * pMplsHwVplsVcInfo, UINT4 u4Action, tMbsmSlotInfo * pSlotInfo));
UINT1 MplsFsMplsMbsmHwDeleteVpnAc PROTO ((UINT4 u4VplsInstance, UINT4 u4VpnId, tMplsHwVplsInfo * pMplsHwVplsVcInfo, UINT4 u4Action, tMbsmSlotInfo * pSlotInfo));
UINT1 MplsNpMplsMbsmCreateMplsInterface PROTO ((tMplsHwMplsIntInfo  * pMplsHwMplsIntInfo, tMbsmSlotInfo * pSlotInfo));
UINT1 MplsFsMplsMbsmHwP2mpAddILM PROTO ((UINT4 u4P2mpId, tMplsHwIlmInfo * pMplsHwP2mpIlmInfo, tMbsmSlotInfo * pSlotInfo));
UINT1 MplsFsMplsMbsmHwP2mpRemoveILM PROTO ((UINT4 u4P2mpId, tMplsHwIlmInfo * pMplsHwP2mpIlmInfo, tMbsmSlotInfo * pSlotInfo));
UINT1 MplsFsMplsMbsmHwGetVpnAc (UINT4 u4VplsInstance, UINT4 u4VpnId, tMplsHwVplsInfo * pMplsHwVplsVcInfo, tMbsmSlotInfo * pSlotInfo);
UINT1 MplsFsMplsMbsmHwGetPwVc (tMplsHwVcTnlInfo * pMplsHwVcInfo, tMplsHwVcTnlInfo * pNextMplsHwVcInfo, tMbsmSlotInfo * pSlotInfo);
UINT1 MplsFsMplsMbsmHwGetILM (tMplsHwIlmInfo * pMplsHwIlmInfo, tMplsHwIlmInfo * pNextMplsHwIlmInfo, tMbsmSlotInfo * pSlotInfo);
UINT1 MplsFsMplsMbsmHwGetL3FTN (UINT4 u4VpnId, UINT4 u4IfIndex, tMplsHwL3FTNInfo * pMplsHwL3FTNInfo, tMbsmSlotInfo * pSlotInfo);
#ifdef MPLS_L3VPN_WANTED
UINT1 MplsFsMplsMbsmHwL3vpnIngressMap PROTO ((tMplsHwL3vpnIgressInfo * pMplsHwL3vpnIgressInfo, tMbsmSlotInfo * pSlotInfo));
UINT1 MplsFsMplsMbsmHwL3vpnEgressMap PROTO ((tMplsHwL3vpnEgressInfo * pMplsHwL3vpnEgressInfo, tMbsmSlotInfo * pSlotInfo));
#endif /* MPLS_WANTED */
#endif
#endif
#endif /* __MPLS_NP_WR_H__ */
