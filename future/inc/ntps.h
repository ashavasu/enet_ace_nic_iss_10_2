/********************************************************************
 *  * Copyright (C) Future Sotware,1997-98,2001
 *  *
 *  * $Id: ntps.h,v 1.1 2017/04/03 15:16:25 siva Exp $
 *  *
 *  * Description: This file contains the constants, gloabl structures,
 *    typedefs, enums uased in  SNTP  module.
 *  *
 *  *******************************************************************/

#ifndef _NTPS_H
#define _NTPS_H


#define NTP_TASK_NAME "NTP"
#define NTP_TASK_PRIORITY 150

VOID
NtpsMain (INT1 *pParam);
#endif
