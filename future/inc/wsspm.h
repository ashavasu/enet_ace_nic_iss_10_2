/*******************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved            * 
 *                                                                 *
 *  $Id: wsspm.h,v 1.10 2015/05/06 12:30:26 siva Exp $              *
 *                                                                 *
 * Description: This file contains all the typedefs used by the    *
 *              CAPWAP module                                      *
 *******************************************************************/

#ifndef __PM_DEFN_H__
#define __PM_DEFN_H__

#include "lr.h"
#include "radioif.h"
#include "wsswlan.h"
#include "tcp.h"
#include "fsvlan.h"
#include "wsssta.h"

#define WSS_PM_DEF_STATS_TIMER_PERIOD            30

UINT4 PmTaskInit (VOID);

enum{
    WSS_PM_WTPPRF_ADD,
    WSS_PM_WTPPRF_DEL,
    WSS_PM_BSSID_ADD,
    WSS_PM_BSSID_DEL,
    WSS_PM_RADIO_ADD,
    WSS_PM_RADIO_DEL,
    WSS_PM_SSID_ADD,
    WSS_PM_SSID_DEL,
    WSS_PM_CLIENT_ADD,
    WSS_PM_CLIENT_DEL,
    WSS_PM_CAPW_ATP_ADD,
    WSS_PM_CAPW_ATP_DEL,
    WSS_PM_WTPPRF_STAT_TIMER_RESET,
    WSS_PM_WTP_EVT_REBOOT_STATS_SET,
    WSS_PM_WTP_EVT_RADIO_STATS_SET,
    WSS_PM_WTP_EVT_WTP_802_11_STATS_SET,
    WSS_PM_WTP_EVT_WTP_CAPWAP_SESS_STATS_SET,
    WSS_PM_WTP_EVT_VSP_BSSID_STATS_SET,
    WSS_PM_WTP_EVT_VSP_SSID_STATS_SET,
    WSS_PM_WTP_LCL_STAT_TIME_EXP_CPWP_STATS_GET,
    WSS_PM_WTP_LCL_STAT_TIME_EXP_STN_STATS_GET,
    WSS_PM_WTP_EVT_REBOOT_STATS_GET,
    WSS_PM_WLC_CAPWAP_STATS_SET,
    WSS_PM_WLC_CAPWAP_RUN_STATS_SET,
    WSS_PM_WTP_EVT_RADIO_STATS_GET,
    WSS_PM_WTP_EVT_WTP_802_11_STATS_GET,
    WSS_PM_WTP_EVT_WTP_CAPWAP_SESS_STATS_GET,
    WSS_PM_WTP_EVT_VSP_BSSID_STATS_GET,
    WSS_PM_WTP_EVT_VSP_SSID_STATS_GET,
    WSS_PM_CLI_WTP_ID_STATS_GET,    /* Stats for all radios of this WTP */
    WSS_PM_CLI_CPWP_DISC_STATS_GET, /*Disc. Stats w.r.t AC */ 
    WSS_PM_CLI_CPWP_JOIN_STATS_GET,  /* Join and Config. Stats w.r.t. AC */
    WSS_PM_CLI_CPWP_JOIN_STATS_CLEAR,  /* Join and Config. Stats w.r.t. AC */
    WSS_PM_CLI_CAPWAP_CONFIG_STATS_GET,
    WSS_PM_CLI_CAPWAP_CONFIG_STATS_CLEAR,
    WSS_PM_CLI_CAPWAP_JOIN_STATS_CLEAR,
    WSS_PM_CLI_CAPWAP_DISC_STATS_CLEAR,
    WSS_PM_CLI_CAPWAP_RUN_STATS_GET,
    WSS_PM_CLI_CAPWAP_RUN_STATS_CLEAR,
    WSS_PM_CLI_CAPWAP_AP_RADIO_STATS_GET,
    WSS_PM_CLI_CAPWAP_AP_RADIO_STATS_CLEAR,
    WSS_PM_CLI_CAPWAP_SHOW_AP_IEEE_80211_STATS_GET,
    WSS_PM_CLI_CAPWAP_SHOW_AP_IEEE_80211_STATS_CLEAR,
    WSS_PM_CLI_WTP_REBOOT_STATS_GET,    /* Stats for all radios of this WTP */
    WSS_PM_CLI_WTP_REBOOT_STATS_CLEAR,    /* Stats for all radios of this WTP */
    WSS_PM_CLI_WTP_ID_STATS_CLEAR,
    WSS_PM_CLI_ALL_WTP_CAPWAP_STATS_GET,
    WSS_PM_CLI_ALL_WTP_CAPWAP_STATS_CLEAR,
    WSS_PM_CLI_ALL_WTP_STATS_GET,
    WSS_PM_CLI_ALL_WTP_STATS_CLEAR,
    WSS_PM_CLI_BSS_ID_STATS_GET,
    WSS_PM_CLI_BSS_ID_STATS_CLEAR,
    WSS_PM_CLI_RADIO_STATS_CLEAR,
    WSS_PM_CLI_CLIENT_STATS_CLEAR,
    WSS_PM_CLI_PM_CLEAR_STATS_SET,
    WSS_PM_WTP_EVT_VSP_CAPWAP_STATS_SET,
    WSS_PM_WTP_EVT_VSP_CAPWAP_STATS_GET,
    WSS_PM_WTP_EVT_VSP_RADIO_STATS_SET,
    WSS_PM_WTP_EVT_VSP_RADIO_STATS_GET,
    WSS_PM_WTP_EVT_VSP_CLIENT_STATS_SET,
    WSS_PM_WTP_EVT_VSP_CLIENT_STATS_GET,
    WSS_PM_WTP_EVT_VSP_AP_STATS_SET,
    WSS_PM_WTP_EVT_VSP_AP_STATS_GET,
    WSS_PM_CAPW_ATP_STATS_SET,
    WSS_PM_CLI_CAPW_ATP_STATS_GET,
    WSS_PM_RADIO_STATS_SET,
    WSS_PM_SSID_STATS_SET,
    WSS_PM_CLIENT_STATS_SET,
    WSS_PM_CLI_CLIENT_STATS_GET,
    WSS_PM_CLI_RADIO_STATS_GET,
    WSS_PM_CLI_SSID_STATS_GET
};

typedef struct{
    tOsixTaskId         pmTaskId;
    tOsixQId            pmStatsRxMsgQId;             /* PM Stats Queue */
    tOsixSemId          SemId;
    UINT1               u1IsPmTaskInitialized;
    UINT1               au1pad[3];
}tPmTaskGlobals;


typedef struct {
    UINT4 u4RunUpdateReqReceived;
    UINT4 u4RunUpdateRspReceived;
    UINT4 u4RunUpdateReqTransmitted;
    UINT4 u4RunUpdateRspTransmitted;
    UINT4 u4RunUpdateunsuccessfulProcessed;
    UINT1 au1RunUpdateReasonLastUnsuccAttempt[256];
    UINT1 au1RunUpdateLastSuccAttemptTime[256];
    UINT1 au1RunUpdateLastUnsuccessfulAttemptTime[256];
}tWssPmWTPRunStats;

typedef struct {
  tRBNodeEmbd       nextWssPmWTPRunEventsNode;
  tWssPmWTPRunStats ConfigUpdateStats;
  tWssPmWTPRunStats StaConfigStats;
  tWssPmWTPRunStats ClearConfigStats;
  tWssPmWTPRunStats DataTransferStats;
  tWssPmWTPRunStats ResetStats;
  tWssPmWTPRunStats PrimaryDiscStats;
  tWssPmWTPRunStats EchoStats;
  UINT4             u4CapwapWtpProfileId;
  UINT1             u1RowStatus;
  UINT1             au1Pad[3];
}tWssPmWTPRunEvents; 



#endif

