/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rtphc.h,v 1.14 2007/02/01 14:52:30 iss Exp $
 *
 * Description:This file contains the exported definitions and  
 *             macros of RTPHC                                 
 *
 *******************************************************************/
#ifndef _RTPHC_H
#define _RTPHC_H

/* RTPHC routine's return codes */
#define   RTPHC_SUCCESS   1 
#define   RTPHC_FAILURE   0 

#endif /* _RTPHC_H */
