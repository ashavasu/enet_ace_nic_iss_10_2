/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l2tp.h,v 1.16 2011/10/13 10:38:29 siva Exp $
 *
 * Description: This file contains the exported definitions and  
 *              macros of L2TP                                  
 *
 *******************************************************************/
#ifndef _L2TP_H
#define _L2TP_H

#ifdef __PPPL2TP_C__
#define EXTERN 
#else
#define EXTERN extern
#endif

EXTERN UINT4 gu4L2tpAuthValue;

/* MSAD ADD -S-- */
#define  L2TP_LOCK()    L2tpLock ()                              
#define  L2TP_UNLOCK()  L2tpUnlock ()                                     

/* MSAD ADD -E-- */

#define L2TP_PPP_DEF_POOL_INDEX 1

/* L2TP routine's constants */
#define  L2TP_USER                    2
#define  PPP_USER                     1
#define  LAC_CALL_IND                 0
#define  LAC_PPPPKT_IND               1

/* L2TP return Codes */
#define  L2TP_SUCCESS                 0
#define  L2TP_FAILURE                 1
#define L2TP_DEFAULT_MAX_INTERFACES          100

void L2TPStart PROTO ((void));

#define  INT_NOT_RCVD                 1
#define  INT_AUTH_SUCC                2
#define  INT_AUTH_FAILED              3
#define  INT_PPP_HDR_LEN              2
#define  INT_AUTH_HDR_LEN             4
#define  INT_AUTH_MSG_LEN            32
#define  MAX_MSG_LENGTH              55

/* L2TP authentication protocol masks */
#define  L2TP_AUTH_PAP_MASK           0x01
#define  L2TP_AUTH_CHAP_MASK          0x02
#define  L2TP_AUTH_MSCHAP_MASK        0x04

/* Possible LCP Conf-Req packet codes sent by PPP */
#define  L2TP_RCVD_LCP_CONF_REQ       1
#define  L2TP_SENT_LCP_CONF_REQ       2

/* Possible values for PPPConfMode */
#define  L2TP_MUST_NEGOTIATE          1
#define  L2TP_NEVER_NEGOTIATE         2
#define  L2TP_DEPENDS_ON_PEER_INFO    3

#define L2TP_ENABLE                   (1)
#define L2TP_DISABLE                  (2)


/*  Possible values of AuthProtocol */
#define  L2TP_TEXTUAL_AUTH            1
#define  L2TP_CHAP_AUTH               2
#define  L2TP_PAP_AUTH                3
#define  L2TP_NO_AUTH                 4
#define  L2TP_MSCHAP_AUTH             5

/* Structure for authentication */
#define L2TP_MAX_PROTONAME_SIZE 8 
#define L2TP_MAX_SECRET_SIZE    32

/* Possible modes (SysType) an L2TP end-point can operate in */
#define     L2TP_NONE           0
#define     L2TP_LAC_IF         1
#define     L2TP_LNS_IF         2
#define     L2TP_BOTH           3


INT4 L2tpUnlock PROTO ((VOID));
INT4 L2tpLock   PROTO ((VOID));

/* Exported function prototypes */
BOOLEAN L2TPIsTunnelModeLAC       PROTO ((UINT4 u4IfIndex));

BOOLEAN L2TPIsTunnelVoluntary     PROTO ((UINT4 u4IfIndex));

INT1  LACHandlePPPAuthOverEvt     PROTO ((UINT4    u4IfIndex,
                                          UINT2    u2AuthType,
                                          UINT2    Id, 
                                          UINT1    PeerId[],
                                          UINT2    IdLen, 
                                          UINT1    Passwd[],
                                          UINT2    PasswdLen,
                                          UINT1    Challenge[],
                                          UINT2    ChalLen
                                         ));

VOID LACPPPConfReqPktInd          PROTO ((UINT4 u4IfIndex, 
                                          tCRU_BUF_CHAIN_HEADER *pPPPPkt, 
                                          UINT1 Length, 
                                          UINT1 Type
                                         ));

VOID L2TPHandlePktFromPPP         PROTO((UINT4 u4IfIndex, 
                                         tCRU_BUF_CHAIN_HEADER *pOutPDU, 
                                         UINT2 Length
                                        ));

VOID L2TPPPPLCPUpInd              PROTO((UINT4 u4IfIndex, 
                                         UINT1 *pTxACCM, 
                                         UINT1 *pRxACCM
                                        ));

VOID L2TPPPPLCPDownInd            PROTO((UINT4 u4IfIndex));

BOOLEAN L2TPIdentifyTunnelMode    PROTO((VOID));
/* MSAD ADD -S- */
VOID L2TPSetRemoteServerAddr PROTO ((UINT4 u4IpAddr));
UINT4 LACDialOut PROTO ((VOID));
/* MSAD END -E- */
#endif /* _L2TP_H */
