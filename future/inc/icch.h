/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: icch.h,v 1.1
 *
 * Description:This file contains prototypes and structure definitions for the ICCH function
 *             responsible for Hardware Programming
 *
 *******************************************************************/

#ifndef _ICCH_H
#define _ICCH_H
#include "lr.h"
#ifdef NPAPI_WANTED
#include "cfanp.h"
#endif
#include "fssnmp.h"

#define ICCH_APPID_SIZE  4
#define ICCH_SLOTID_SIZE 4
#define ICCH_TO_ALL_SLOTS 0xff 
#define ICCH_MSG_ETHERTYPE  0x88b5
#define ICCH_DEFAULT_INST 0

#define  MCAG_NAME      "[MAC AGING] "

typedef enum
{
 ICCH_PNAC = 1,
 ICCH_LA 
} tICCHApplicationId;

typedef struct ICCHInfo {
  UINT1 *pu1Data;
  UINT4 u4SlotId;
  UINT2 u2PktLength;
  UINT2 u2AppId;
} tIcchInfo;


#ifdef NPAPI_WANTED
INT4 ICCHHandOverTxFrame (tIcchInfo *pIcchInfo);
INT4 ICCHProcessRxMessage (tHwInfo *pHwInfo);
#endif

/* MCLAG - ICCH related parameters */

#define ICCH_APPID_SIZE  4
#define ICCH_SLOTID_SIZE 4
#define ICCH_TO_ALL_SLOTS 0xff

#define ICCH_MAC_ADDR_LEN    6
#define VLAN_ICCH_SYNC_PKT_LEN      69
#define ICCH_SYNC_PKT_LEN 51
/* Allocate CRU buf and set the valid offset to point to ICCH data.
 *  * i.e., reserve space for ICCH hdr  */
#define  ICCH_ALLOC_TX_BUF(size) IcchApiAllocTxBuf (size)

/* ICCH module events */
#define ICCH_CTRL_QMSG_EVENT            0x000001
#define ICCH_TMR_EXPIRY_EVENT           0x000004
#define ICCH_PKT_FROM_APP               0x000008
#define ICCH_TCP_NEW_CONN_RCVD          0x000010
#define ICCH_PROCS_RX_BUFF_SELF_EVENT   0x000020
#define ICCH_INIT_BULK_REQUEST          0x000040
#define ICCH_GO_MASTER                  0x000080
#define ICCH_GO_SLAVE                   0x000100
#define ICCH_PROCESS_PENDING_SYNC_MSG   0x000200
#define ICCH_MCLAG_ENABLED             0x000400
#define ICCH_MCLAG_DISABLED             0x000800
#define ICCH_GO_INIT                    0x001000
#define ICCH_SOCK_CREATE                0x002000

#define ICCH_LA_MCLAG_ENABLED LA_MCLAG_ENABLED
#define ICCH_LA_MCLAG_DISABLED LA_MCLAG_DISABLED



/* Protocol sync-up message block on ICCL */
#define ICCH_BLOCK_ALL_SYNC      0x00000000
#define ICCH_VLAN_FDB_SYNC       0x00000001
#define ICCH_ARP_CACHE_SYNC      0x00000002
#define ICCH_ENABLE_ALL_SYNC     0xFFFFFFFF

#define ICCH_TCP_SRV_SOCK_FD()     (gIcchInfo.i4SrvSockFd)

#define ICCH_MODULE_EVENTS (ICCH_CTRL_QMSG_EVENT | \
                            ICCH_PKT_FROM_APP | ICCH_TMR_EXPIRY_EVENT | \
                            ICCH_TCP_NEW_CONN_RCVD | ICCH_PROCS_RX_BUFF_SELF_EVENT | \
                            ICCH_INIT_BULK_REQUEST | ICCH_GO_MASTER | ICCH_GO_SLAVE | \
                            ICCH_PROCESS_PENDING_SYNC_MSG | ICCH_GO_INIT | \
                            ICCH_MCLAG_ENABLED | ICCH_MCLAG_DISABLED | ICCH_SOCK_CREATE)

/* Return values of ICCH module */
#define ICCH_SUCCESS       OSIX_SUCCESS
#define ICCH_FAILURE       OSIX_FAILURE

#define ICCH_TRUE          TRUE
#define ICCH_FALSE         FALSE

/* Status of the protocol operation */
#define ICCH_STARTED             1
#define ICCH_COMPLETED           2
#define ICCH_FAILED              3

/* ICCH Notifications */
/* Protocol operation */
#define ICCH_NOTIF_SYNC_UP             1

/* Level of trace for ICCH */
#define  ICCH_MGMT_TRC          0x00000001
#define  ICCH_PKT_DUMP_TRC      0x00000002
#define  ICCH_ALL_FAILURE_TRC   0x00000004
#define  ICCH_CRITICAL_TRC      0x00000008
#define  ICCH_SYNC_EVT_TRC      0x00000010
#define  ICCH_SYNC_MSG_TRC      0x00000020
#define  ICCH_NOTIF_TRC         0x00000040
#define ICCH_ALL_TRC            (ICCH_MGMT_TRC | \
                                 ICCH_ALL_FAILURE_TRC | \
                                 ICCH_CRITICAL_TRC | \
                                 ICCH_SYNC_EVT_TRC | \
                                 ICCH_SYNC_MSG_TRC | \
                                 ICCH_NOTIF_TRC)



/*Buffer related macros*/
#define ICCH_FREE(size)     CRU_BUF_Release_MsgBufChain (size, FALSE)

#define ICCH_COPY(CruBuf, LinBuf, size)  \
        CRU_BUF_Copy_OverBufChain (CruBuf, LinBuf, 0, size)

/* The Maximum size of sync packet can be received
 * in a single socket read call */
#define ICCH_MAX_SYNC_PKT_LEN 1568

/* Get and assign bytes of data from message buffer */
#define ICCH_GET_DATA_4_BYTE(pMsg, u4Offset, u4Value) \
{\
   CRU_BUF_Copy_FromBufChain(pMsg, ((UINT1 *) &u4Value), u4Offset, 4);\
   u4Value = OSIX_NTOHL ((UINT4)u4Value);\
}

#define ICCH_PKT_MOVE_TO_DATA(pBuf, u1HLen) \
       { \
        CRU_BUF_Move_ValidOffset (pBuf, u1HLen);\
       }

#define ICCH_GET_DATA_1_BYTE(pMsg, u4Offset, u1Value) \
        CRU_BUF_Copy_FromBufChain(pMsg, (UINT1 *)&u1Value, u4Offset, 1)

#define ICCH_GET_DATA_4_BYTE(pMsg, u4Offset, u4Value) \
{\
   CRU_BUF_Copy_FromBufChain(pMsg, ((UINT1 *) &u4Value), u4Offset, 4);\
   u4Value = OSIX_NTOHL ((UINT4)u4Value);\
}

#define ICCH_GET_DATA_2_BYTE(pMsg, u4Offset, u2Value) \
{\
   CRU_BUF_Copy_FromBufChain(pMsg, ((UINT1 *) &u2Value), u4Offset, 2);\
   u2Value = OSIX_NTOHS (u2Value);\
}
#define ICCH_GET_DATA_N_BYTE(pMsg, pdst, u4Offset, u4size) \
{\
   CRU_BUF_Copy_FromBufChain(pMsg, ((UINT1 *) pdst), u4Offset, u4size);\
}

#define ICCH_DATA_ASSIGN_1_BYTE(pMsg, u4Offset, u1Value) \
{\
   UINT1  u1LinearBuf = (UINT1) u1Value;\
   CRU_BUF_Copy_OverBufChain(pMsg, ((UINT1 *) &u1LinearBuf), u4Offset, 1);\
}

#define ICCH_DATA_ASSIGN_2_BYTE(pMsg, u4Offset, u2Value) \
{\
   UINT2  u2LinearBuf = OSIX_HTONS ((UINT2) u2Value);\
   CRU_BUF_Copy_OverBufChain(pMsg, ((UINT1 *) &u2LinearBuf), u4Offset, 2);\
}

#define ICCH_DATA_ASSIGN_4_BYTE(pMsg, u4Offset, u4Value) \
{\
   UINT4  u4LinearBuf = OSIX_HTONL ((UINT4) u4Value);\
   CRU_BUF_Copy_OverBufChain(pMsg, ((UINT1 *) &u4LinearBuf), u4Offset, 4);\
}

#define ICCH_COPY_TO_OFFSET(dest, src, offset, size) \
        CRU_BUF_Copy_OverBufChain (dest, (UINT1 *) src, offset, size)

/*copy ICCH msg to linear buffer*/
#define ICCH_COPY_TO_LINEAR_BUF(CruBuf, LinBuf, offset, Len) \
        CRU_BUF_Copy_FromBufChain (CruBuf, (UINT1 *)LinBuf, offset,Len)

/* Protocols registered with ICCH, uses this macro to get the sequence
 * number from ICCH Header */
#define ICCH_PKT_GET_SEQNUM(pBuf, pu4SeqNum) \
{\
    if (pBuf != NULL)\
    {\
        ICCH_GET_DATA_4_BYTE (pBuf, ICCH_HDR_OFF_SEQ_NO, *pu4SeqNum);\
    }\
    else\
    {\
        *pu4SeqNum = 0;\
    }\
}

#define ICCH_STRIP_OFF_ICCH_HDR(pBuf, u2PktLen) \
{\
    if (pBuf != NULL) \
    {\
        ICCH_PKT_MOVE_TO_DATA (pBuf, ICCH_HDR_LENGTH);\
        u2PktLen = (UINT2)(u2PktLen - ICCH_HDR_LENGTH);\
    }\
}

#define  STRTOL(s,d,n)  strtol  ((const char *) (s), (char **) (d), (int) (n))

#define ICCH_DATE_TIME_STR_LEN    24
#define ICCH_ERROR_STR_LEN        256

typedef tCRU_BUF_CHAIN_HEADER  tIcchMsg;

typedef struct {
   UINT4 u4NodeId;    /* Node Id of the node which generates the trap */
   UINT4 u4State;     /* Node State(ACTIVE/STANDBY) of the node which generates
                         the trap */
   UINT4 u4AppId;     /* Module Identifier which calls this API*/
   UINT4 u4Operation; /* Protocol operation (BULK_UPDATE / SWITCHOVER ) */
   UINT4 u4Status; /* The status of  protocol operation ( started/ completed /
                      failure) */
   UINT4 u4Error;    /* Error code (MEMALLOC_FAIL from protocols / SENDTO_
                        FAIL from protocols / NONE) */
   CHR1  ac1DateTime[ICCH_DATE_TIME_STR_LEN];   /* Date and Time when the
                                                 trap gets generated*/
   CHR1 ac1ErrorStr[ICCH_ERROR_STR_LEN];
} tIcchNotificationMsg;

/* For sending the standby node count to the protocols*/
typedef struct IcchNodeInfo
{
    UINT1 u1NumPeers;
    UINT1 au1Reserved [3];
}
tIcchNodeInfo;

/* Application identifier */
enum
{
  ICCH_APP_ID = 0,
  ICCH_LA_APP_ID,
  ICCH_VLAN_APP_ID,
  ICCH_SNOOP_APP_ID,
  ICCH_ARP_APP_ID,
  ICCH_MAX_APPS
};

#ifdef _ICCHMAIN_C_
CHR1  *gapc1IcchAppName [] = {
    /*  0 */ "ICCH",
    /*  1 */ "LA",
    /*  2 */ "VLAN",
    /*  3 */ "SNOOP",
    /*  4 */ "ARP",
    /*  5 */ "ALL"
};
#else
extern CHR1  *gapc1IcchAppName[];
#endif

/* Clear statistics value */
enum
{
    ICCH_CLEAR_STATS_TRUE = 1,
    ICCH_CLEAR_STATS_FALSE
};

enum
{
    ICCH_STATS_ENABLE=1,
    ICCH_STATS_DISABLE
};

enum
{
    ICCH_MSR_L3VLAN=1,
    ICCH_MSR_L2VLAN,
    ICCH_MSR_LAGG
};

#define ICCH_APP_LIST_SIZE   ((ICCH_MAX_APPS + 31)/32 * 4)

/* Size of the peer node count msg sent to protocols */
#define ICCH_NODE_COUNT_MSG_SIZE          1

typedef UINT1 tIcchAppList [ICCH_APP_LIST_SIZE];

/* List of events sent to protocols */
enum
{
    ICCH_EVENT_UNKNOWN = 0,
    GO_MASTER,
    GO_SLAVE,
    ICCH_PEER_UP,
    ICCH_PEER_DOWN,
    ICCH_COMM_LOST_AND_RESTORED,
    ICCH_PROC_PENDING_SYNC_MSG,
    ICCH_CONFIG_RESTORE_COMPLETE,
    ICCH_MESSAGE,
    ICCH_NOTIFICATION_INFO,
    L2_INIT_BULK_UPDATES,
 MCLAG_ENABLED,
 MCLAG_DISABLED,
    GO_INIT,
    ICCH_TCP_SOCK_CREATE

};

/* To initiate bulk updates in non-l2 module, a new event with the
 * same value is created */
#define ICCH_INITIATE_BULK_UPDATES L2_INIT_BULK_UPDATES

/* The following array should be udpated with proper string to
 * display proper value in notification messages */
#ifdef _ICCHMAIN_C_
CONST CHR1  *gapc1IcchEvntName [] = {
    /*  0 */ "ICCH_EVENT_UNKNOWN",
    /*  1 */ "ICCH_PEER_UP",
    /*  2 */ "ICCH_PEER_DOWN",
    /*  3 */ "ICCH_CONFIG_RESTORE_COMPLETE",
    /*  4 */ "ICCH_MESSAGE",
    /*  5 */ "ICCH_NOTIFICATION_INFO",
    /*  6 */ "L2_INIT_BULK_UPDATES"
};
CHR1  *gaIcchAppName [] = {
    /*  0 */ "ICCH",
    /*  1 */ "LA",
    /*  2 */ "VLAN",
    /*  3 */ "SNOOP",
    /*  4 */ "ARP",
    /*  5 */ "ALL",
};
#else
extern CONST CHR1  *gapc1IcchEvntName [];
extern CHR1  *gaIcchAppName[];
#endif

/* ICCH states */
enum {
   ICCH_INIT = 0,
   ICCH_MASTER,
   ICCH_SLAVE
};

/* Message types used in the static configuration sync-up message */
enum
{
    ICCH_UNUSED = 0, /* Macro for 0 */
    ICCH_CONFIG_VALID_MSG = 1, /* Used if the configuration was successful */
    ICCH_CONFIG_DUMMY_MSG = 2,  /* Used if the configuration failed */
    ICCH_CONFIG_IGNORE_ABORT = 3,  /* Used to ignore abort messages */
    ICCH_CONFIG_ACCEPT_ABORT = 4  /* Used to the accept abort messages */
};

#define ICCH_NONE                1
#define ICCH_MEMALLOC_FAIL       2
#define ICCH_SENDTO_FAIL         3
#define ICCH_PROCESS_FAIL        4

/* List of events sent from ICCH */
enum
{
   ICCH_INITIATE_BULK_UPDATE = 1,
   ICCH_PROTOCOL_BULK_UPDT_COMPLETION,
   ICCH_BULK_UPDT_ABORT,
   ICCH_PROTOCOL_SEND_EVENT
};

/*Bulk update identifier */
enum
{
   ICCH_BULK_UPDT_REQ_MSG = 1,
   ICCH_BULK_UPDATE_MSG,
   ICCH_BULK_UPDT_TAIL_MSG
};

/* Protocol events */
typedef struct {
    UINT4 u4AppId;
    UINT4 u4Event;
    UINT4 u4SendEvent;
    UINT4 u4Error;
}tIcchProtoEvt;

/* Structure used by HB module to interface with ICCH module */
typedef struct {
    UINT4 u4Evt;
    UINT4 u4PeerAddr;
}tIcchHbMsg;

/* Structure used by management modules to send the configuration information
 * to the ICCH module*/
typedef struct {
    tSnmpIndex            *pMultiIndex; /* Index for the OID */
    tSNMP_MULTI_DATA_TYPE *pMultiData;  /* OID Data to be configured */
    UINT4                 u4SeqNo;  /* Sequence number for this message*/
    INT4                  i4OidLen; /* OID length */
    UINT1                 *pu1ObjectId; /* Actual OID */
    INT1                  i1ConfigSetStatus; /* return value of nmhSet SNMP_SUCCESS/
                                                SNMP_FAILURE */
    INT1                  i1MsgType; /* ICCH_CONFIG_VALID_MSG/ICCH_CONFIG_IGNORE_ABORT/
                                        ICCH_CONFIG_ACCEPT_ABORT/ICCH_CONFIG_DUMMY_MSG*/
    UINT1                 au1Pad [2];
}tIcchNotifConfChg;

/*control messages from peer*/
typedef struct  {
   UINT4         u4PeerId; /*Peer information*/
   UINT4         u4MsgType;                             /* Query or Response*/
   UINT4         u4Switchid;                            /* slot number*/
   UINT1         au1SwitchBaseMac[ICCH_MAC_ADDR_LEN];     /* switch hw address*/
   UINT1         u1PrevState;
   UINT1         u1CurrState;
   UINT1         u1PeerState;  /*Peer status . Peer up/Peer down*/
   UINT1         au1Pad[3];
}tIcchPeerCtrlMsg ;

/* This structure is used by external protocols for sending the
 * Acknowledgement to ICCH after processing of sync-up message. */
typedef struct _IcchProtoAck
{
   UINT4  u4AppId;      /* Application Identifier */
   UINT4  u4SeqNumber;  /* Ack for this Sequence numbered packet */
}tIcchProtoAck;

/* Structure used by protocols to register with ICCH */
typedef struct {
   UINT4 u4EntId;
   INT4 (*pFnRcvPkt) (UINT1 u1Event, tCRU_BUF_CHAIN_HEADER *pBuf, UINT2 u2PktLen);
}tIcchRegParams;

/* ICCH header */
typedef struct {
    UINT2 u2Version;
    UINT2 u2Chksum;
    UINT4 u4TotLen;
    UINT4 u4MsgType;
    UINT4 u4SrcEntId;
    UINT4 u4DestEntId;
    UINT4 u4SeqNo;
#define ICCH_HDR_OFF_VERSION      0
#define ICCH_HDR_OFF_CKSUM        2
#define ICCH_HDR_OFF_TOT_LENGTH   4
#define ICCH_HDR_OFF_MSG_TYPE     8
#define ICCH_HDR_OFF_SRC_ENTID   12
#define ICCH_HDR_OFF_DEST_ENTID  16
#define ICCH_HDR_OFF_SEQ_NO       20
#define ICCH_HDR_LENGTH    sizeof(tIcchHdr)
}tIcchHdr;

VOID IcchTaskMain (INT1 *pArg);
UINT4 IcchApiSendHbEvtToIcch (tIcchHbMsg * pIcchHbMsg );
UINT4 IcchRegisterProtocols (tIcchRegParams * pIcchReg );
UINT4 IcchDeRegisterProtocols (UINT4 u4SrcEntId );
UINT4 IcchEnqMsgToIcchFromAppl (tIcchMsg * pIcchMsg , UINT2 u2DataLen , UINT4 u4SrcEntId , UINT4 u4DestEntId );
UINT4 IcchReleaseMemoryForMsg (UINT1 *pu1Block );
UINT1 IcchApiHandleProtocolEvent (tIcchProtoEvt * pEvt );
VOID IcchSendEventToAppln (UINT4 u4AppId );
UINT4 IcchSendEventToIcchTask (UINT4 u4Event );
INT4 IcchSetBulkUpdatesStatus (UINT4 u4AppId );
INT4 IcchGetBulkUpdatesStatus (UINT4 u4AppId );
UINT4 IcchGetNodeState  PROTO ((VOID));
UINT4 IcchGetPeerNodeState  PROTO ((VOID));
UINT4 IcchGetNodePrevState  PROTO ((VOID));
VOID IcchApiGetSeqNumForSyncMsg (UINT4 *pu4SeqNum );
INT4 IcchApiSendProtoAckToICCH (tIcchProtoAck * pProtoAck );
VOID IcchApiTrapSendNotifications (tIcchNotificationMsg * pNotifyInfo );
UINT4 IcchApiInitiateBulkRequest  PROTO ((VOID));
tIcchMsg * IcchApiAllocTxBuf (UINT4 u4SyncMsgSize );
INT4 IcchIsBulkUpdateComplete  PROTO ((VOID));
VOID IcchApiTcpSrvSockInit  PROTO ((INT4 *));
VOID IcchApiTcpSrvSockDeInit  PROTO ((INT4 *));
VOID HbApiSendHbEvtToIcch (tIcchHbMsg *IcchHbMsg);
INT1 IcchApiIsMcLagInterface (UINT4 u4IfIndex);
VOID IcchGetIcclIfIndex (UINT4 *pu4IfIndex);
VOID IcchGetIcclL3IfIndex (UINT4 *pu4IfIndex);
UINT4 IcchApiGetRolePlayed (VOID);
UINT4 IcchApiGetPeerAddress (VOID);
UINT4 IcchApiSyslogRegister (VOID);
INT1 IcchSetIcclIfIndex (UINT4 u4IfIndex);
VOID IcchSetIcclL3IfIndex (UINT4 u4IfIndex);
CHR1* IcchApiGetIcclIpInterface  (UINT2 u2Index);
UINT4 IcchApiGetIcclIpMask  (UINT2 u2Index);
UINT4 IcchApiGetIcclIpAddr  (UINT2 u2Index);
UINT4 IcchApiGetIcclVlanId  (UINT2 u2Index);
UINT4 IcchApiGetTotalNumOfInst  (VOID);
UINT4  IcchApiCheckProtoSyncEnabled (VOID);
UINT4 IcchIsIcclVlan (UINT2 u2VlanId);
INT4 IcchApiSendMCLAGStatus (INT4 i4MCLAGSystemStatus);
INT1 IcchIsIcclInterface (UINT4 u4IfIndex);
INT1 IcchIsIcclL3Interface (UINT4 u4IfIndex);
INT4 IcchApiGetSrvSockId (VOID); 
INT4 IcchApiGetOverrideLocalAffinity (VOID);
UINT4 IcchApiIsIcclAddrRedistributable (UINT4 u4DestNetAddr);
UINT4 IsIcchNextIpAddress (UINT4 u4IpAddr, UINT4 u4NetMask);
UINT4 IcchApiIsSessionAvailable (UINT4 u4PeerAddr);
INT4 VlanApiFormPortBulkRequest (UINT4 u4IfIndex);
UINT4 IcchApiSetProtoSync (UINT4 u4ProtoSync);

#ifdef NPAPI_WANTED
UINT4 
IcchVlanNpPortWrite (UINT1 * pu1Data);
#endif
UINT1
IcchCheckRestorationIcclConf (UINT4 u4IfIndex, UINT1 u1Type);
UINT1
IcchCheckRestoreSubnetMask (UINT4 u4IfIndex, UINT4 u4SubnetMask);
INT4
IcchUtilAddEthHeader (UINT1 *pu1DataBuf, UINT4 u4PktSize);
VOID IcchResetIcclParameters (UINT4 u4InstanceId);

#endif


