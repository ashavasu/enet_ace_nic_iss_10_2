/********************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *  
 *  $Id: wsswlan.h,v 1.27 2017/11/24 10:36:59 siva Exp $
 *  
 *  Description: This file contains type definitions for wsswlan module.
 *  *********************************************************************/

#ifndef __WSSWLAN_H__
#define __WSSWLAN_H__

#include "wssmac.h"
#include "wsssta.h"
#if defined(WPS_WANTED) || defined(WPS_WTP_WANTED)
#include "wps.h"
#endif
#ifdef WLC_WANTED
#include "radius.h"
#endif

/* Array lengths */
#define WSSWLAN_WEP_KEY_LEN     104
#define WSSWLAN_RSNA_KEY_LEN    16
#define WSSWLAN_GROUP_TSC_LEN   6
#define WSSWLAN_SSID_NAME_LEN   32
#define WSSWLAN_BR_INTERFACE_NAME_LEN   24
#define WSSWLAN_STATION_ID_LEN  64
#define WSSWLAN_DOMAIN_NAME_LEN              32
#define WSSWLAN_IP_NAME_LEN                  4
#define WSSWLAN_WLAN_NAME_LEN                5
#define VEND_CONF_MAX                        5
#define WSSWLAN_VENDOR_DATA_LEN              2048
#define WSSWLAN_EXTERNAL_URL_LENGTH         120

/* Wlan Index states */
#define WSSWLAN_RS_NOT_READY 3
#define WSSWLAN_RS_NOT_IN_SERVICE 2
#define WSSWLAN_RS_ACTIVE 1
#define WSSWLAN_RS_CREATE 5
#define WSSWLAN_RS_DELETE 6

/* Authentication related macros */
#define WSSWLAN_AUTH_TYPE_OPEN  0
#define WSSWLAN_AUTH_TYPE_WEP   1
#define WSSWLAN_AUTH_TYPE_WEB   255
#define WSSWLAN_AUTH_TYPE_WEB_EXT   254

#define WSSWLAN_AUTH_METHOD_OPEN    1
#define WSSWLAN_AUTH_METHOD_SHARED  2

/* Macros for Authentication Algorithm Selectors */

#define AUTH_ALGO_OPEN    1
#define AUTH_ALGO_SHARED  2

/* Range related macros */
#define WSSWLAN_START_WLANID_PER_RADIO  1
#define WSSWLAN_END_WLANID_PER_RADIO    16

/* Mac Type and Tunnel modes */
#define WSSWLAN_MAC_LOCAL                    0
#define WSSWLAN_MAC_SPLIT                    1
#define WSSWLAN_MAC_EMPTY                    3

#define WSSWLAN_TUNNEL_MODE_BRIDGE           0
#define WSSWLAN_TUNNEL_MODE_DOT3             1
#define WSSWLAN_TUNNEL_MODE_NATIVE           2

#define BEST_EFFORT_ACI 0
#define VIDEO_ACI 1
#define VOICE_ACI 2
#define BACKGROUND_ACI 3

#define WSSWLAN_ENABLE 1
#define WSSWLAN_DISABLE 0
#ifdef BAND_SELECT_WANTED
#define WLAN_DISABLE 2
#endif

#define WSSWLAN_ADD_REQ    1
#define WSSWLAN_DEL_REQ    2
#define WSSWLAN_UPDATE_REQ    3

#define APGROUP_ENABLE 1
#define APGROUP_DISABLE 2

typedef enum 
{
    SHIFT_VALUE_1 = 1,
    SHIFT_VALUE_2 ,
    SHIFT_VALUE_3 ,
    SHIFT_VALUE_4 ,
    SHIFT_VALUE_5 ,
    SHIFT_VALUE_6 ,
    SHIFT_VALUE_7 ,
    SHIFT_VALUE_8 ,
    SHIFT_VALUE_9 ,
    SHIFT_VALUE_10 ,
    SHIFT_VALUE_11 ,
    SHIFT_VALUE_12 ,
    SHIFT_VALUE_13 ,
    SHIFT_VALUE_14 ,
    SHIFT_VALUE_15
} eShiftValue;

/* Start Additional Parameters of tWssWlanAddReq */
typedef struct {
    UINT1       u1ElementId;
    UINT1       u1Length;
    UINT1       u1LocalPowerConstraint;
    UINT1       au1pad[1];
}tWssWlanPowerConstraint;

typedef struct {
    UINT4       u4WlanSnoopTimer;
    UINT4       u4WlanSnoopTimeout;
    UINT2       u2WlanMulticastMode;
    UINT2       u2WlanSnoopTableLength;
    UINT1       u1ElementId;
    UINT1       u1Length;
    UINT1       au1pad[2];
}tWssWlanMulticast;
typedef struct {
    UINT2       u2TxOpLimit;    
    UINT1       u1ACI_AIFSN;
    UINT1       u1ECWmin_ECWmax;
}tAC_BE_ParameterRecord;

typedef struct  {
    UINT2       u2TxOpLimit;    
    UINT1       u1ACI_AIFSN;
    UINT1       u1ECWmin_ECWmax;
}tAC_BK_ParameterRecord;

typedef struct  {
    UINT2       u2TxOpLimit;    
    UINT1       u1ACI_AIFSN;
    UINT1       u1ECWmin_ECWmax;
}tAC_VI_ParameterRecord;

typedef struct  {
    UINT2       u2TxOpLimit;    
    UINT1       u1ACI_AIFSN;
    UINT1       u1ECWmin_ECWmax;
    UINT1       u1pad;
    UINT1       au1Pad[3];
}tAC_VO_ParameterRecord;

typedef struct {
    tAC_BE_ParameterRecord AC_BE_ParameterRecord;
    tAC_BK_ParameterRecord AC_BK_ParameterRecord;
    tAC_VI_ParameterRecord AC_VI_ParameterRecord;
    tAC_VO_ParameterRecord AC_VO_ParameterRecord;
    UINT1                     u1ElementId;
    UINT1                     u1Length;
    UINT1                     u1QosInfo;
    UINT1                     u1Reserved;

}tWssWlanEdcaParam;

typedef struct {
    UINT1       u1ElementId;
    UINT1       u1Length;
    UINT1       u1QosInfo;
    UINT1       u1pad;
}tWssWlanQosCapab;
typedef struct {
    UINT1       u1OUI;
    UINT1       u1SuiteType;
    UINT1       au1pad[2];
}tGroupCipherSuite;
                             
/* End Additional Parameters of tWssWlanAddReq */
typedef struct {
    tWssWlanPowerConstraint WssWlanPowerConstraint;
    tWssWlanEdcaParam       WssWlanEdcaParam;
    tWssWlanQosCapab        WssWlanQosCapab;
    tRsnaIEElements         RsnaIEElements;
    tWssWlanMulticast       WssWlanMulticast;
#ifdef WPS_WANTED    
    tWpsIEElements     WpsIEElements;
#endif    
    UINT2                   u2MessageType;
    UINT2                   u2MessageLength;
    UINT2                   u2Capability;
    UINT2                   u2WtpInternalId;
    UINT2                   u2KeyLength;
    UINT1                   u1RadioId;
    UINT1                   u1WlanId;
    UINT1                   au1Key [WSSWLAN_WEP_KEY_LEN];
    UINT1                   u1KeyIndex;
    UINT1                   u1KeyStatus;
    UINT1                   u1Qos;
    UINT1                   u1KeyType; /*PMF_WANTED*/
}tWssWlanUpdateReq;

typedef struct {
    UINT2       u2MessageType;
    UINT2       u2MessageLength;
    UINT2       u2WtpInternalId;
    UINT1       u1RadioId;
    UINT1       u1WlanId;
}tWssWlanDeleteReq;

typedef struct {
    tMacAddr    BssId;
    UINT2       u2MessageType;
    UINT2       u2Length;
    UINT1       u1RadioId;
    UINT1       u1WlanId;
    UINT1       u1IsPresent;
    UINT1       au1pad[3];
}tWssWlanRsp;

typedef struct {
    UINT4       u4ResultCode;
    UINT2       u2MessageType;
    UINT2       u2Length;
    UINT1       u1IsPresent;
    UINT1       au1Pad[3];
}tWssWlanResultCode;

typedef struct {
    tWssWlanRsp      WssWlanRsp;
    tWssWlanResultCode  WssWlanResultCode;
    UINT2               u2WtpInternalId;
    BOOL1               bIsSetAddRsp;
    BOOL1               bIsSetDeleteRsp;
    BOOL1               bIsSetUpdateRsp;
    UINT1               au1pad[3];
}tWssWlanConfRsp;

typedef struct
{
    UINT4       u4IfIndex;
    UINT4       u4IfType;
    UINT1       u1AdminStatus;
    UINT1       u1OperStatus;
    UINT1       au1pad[2];
}tWssWlanSetAdminOperStatus;


typedef struct{
    UINT4    vendorId;
    UINT2    u2MsgEleType;
    UINT2    u2MsgEleLen;
    UINT1    u1Val;
    BOOL1    isOptional;
    UINT1    au1Pad[2];
}tvendorDiscoveryType;


typedef struct{
    UINT4    vendorId;
    UINT4    u4Val;
    UINT2    u2MsgEleType;
    UINT2    u2MsgEleLen;
    BOOL1    isOptional;
    UINT1    au1Pad[3];
}tvendorUdpServerPort;

typedef struct{
    UINT4    vendorId;
    UINT2    u2MsgEleType;
    UINT2    u2MsgEleLen;
    UINT1    u1Val;
    BOOL1    isOptional;
    UINT1    au1Pad[2];
}tvendorControlDTLS;

typedef struct{
    UINT4    vendorId;
    UINT2    u2MsgEleType;
    UINT2    u2MsgEleLen;
    UINT1    u1Val;
    UINT1    u1ChannelWidth;
    UINT1    u1ShortGi20;
    UINT1    u1ShortGi40;
    UINT1    au1g1MCS20MhzRateSet[4];
    UINT1    au1g2MCS20MhzRateSet[4];
    UINT1    au1g3MCS20MhzRateSet[4];
    UINT1    au1g4MCS20MhzRateSet[4];
    UINT1    au1g5MCS20MhzRateSet[4];
    UINT1    au1g6MCS20MhzRateSet[4];
    UINT1    au1g7MCS20MhzRateSet[4];
    UINT1    au1g8MCS20MhzRateSet[4];
    UINT1    au1g1MCS40MhzRateSet[4];
    UINT1    au1g2MCS40MhzRateSet[4];
    UINT1    au1g3MCS40MhzRateSet[4];
    UINT1    au1g4MCS40MhzRateSet[4];
    UINT1    au1g5MCS40MhzRateSet[4];
    UINT1    au1g6MCS40MhzRateSet[4];
    UINT1    au1g7MCS40MhzRateSet[4];
    UINT1    au1g8MCS40MhzRateSet[4];
    UINT1    u1OperationalRate;
    BOOL1    isOptional;
    UINT1    au1Pad[2];
}tDot11nvendorType;

typedef struct{
    UINT4    vendorId;
    UINT2    u2MsgEleType;
    UINT2    u2MsgEleLen;
    UINT1    u1Val;
    UINT1    u1RadioId;
    UINT1    u1WlanId;
    BOOL1    isOptional;
}tvendorSsidIsolation;

#ifdef WTP_WANTED
typedef struct{
    UINT4    vendorId;
    UINT2    u2MsgEleType;
    UINT2    u2MsgEleLen;
    UINT2    u2WlanMulticastMode;
    UINT2    u2WlanSnoopTableLength;
    UINT4    u4WlanSnoopTimer;
    UINT4    u4WlanSnoopTimeout;
    UINT1    u1RadioId;
    UINT1    u1WlanId;
    BOOL1    isOptional;
    UINT1    au1Pad[1];
}tVendorMulticast;
#endif

typedef struct{
    UINT4    u4IpAddr;
    UINT4    u4SubMask;
    UINT2    u2MsgEleType;
    UINT2    u2MsgEleLen;
    BOOL1    b1AdminStatus;
    BOOL1    isOptional;
    UINT1    au1Pad[2];
}tVendorWlanInterface;

typedef struct{
    UINT4    vendorId;
    UINT4    u4QosUpStreamCIR;
    UINT4    u4QosUpStreamCBS;
    UINT4    u4QosUpStreamEIR;
    UINT4    u4QosUpStreamEBS;
    UINT4    u4QosDownStreamCIR;
    UINT4    u4QosDownStreamCBS;
    UINT4    u4QosDownStreamEIR;
    UINT4    u4QosDownStreamEBS;
    UINT2    u2MsgEleType;
    UINT2    u2MsgEleLen;
    UINT1    u1QosRateLimit;
    UINT1    u1RadioId;
    UINT1    u1WlanId;
    UINT1    u1PassengerTrustMode;
    BOOL1    isOptional;
    UINT1    au1Pad[3];
}tvendorSsidRateLimit;

typedef struct{
    UINT4    vendorId;
    UINT4    wlanBSSIDBeaconsSentCount;
    UINT4    wlanBSSIDProbeReqRcvdCount;
    UINT4    wlanBSSIDProbeRespSentCount;
    UINT4    wlanBSSIDDataPktRcvdCount;
    UINT4    wlanBSSIDDataPktSentCount;
    UINT2    u2MsgEleType;
    UINT2    u2MsgEleLen;
    BOOL1    isOptional;
    UINT1    au1Pad[3];
}tvendorWlanBssIdStats;

typedef struct{
    UINT4    vendorId;
    UINT2    u2MsgEleType;
    UINT2    u2MsgEleLen;
    UINT1    au1DomainName[WSSWLAN_DOMAIN_NAME_LEN];
    UINT1    au1ServerIp[WSSWLAN_IP_NAME_LEN];
    BOOL1    isOptional;
    UINT1    au1Pad[3];
}tvendorDNS;

typedef struct{
    UINT4    vendorId;
    UINT2    u2MsgEleType;
    UINT2    u2MsgEleLen;
    UINT2    u2VlanId;
    BOOL1    isOptional;
    UINT1    au1Pad[1];
}tvendorNativeVlan;

typedef struct{
    UINT4    vendorId;
    UINT2    u2MsgEleType;
    UINT2    u2MsgEleLen;
    UINT1    au1Ssid[WSSWLAN_SSID_NAME_LEN];
    BOOL1    isOptional;
    UINT1    au1Pad[3];
}tvendorMgmtSsid;

/* Wlan Vlan Info */
typedef struct{
    UINT4    vendorId;
    UINT2    u2MsgEleType;
    UINT2    u2MsgEleLen;
    UINT2    u2VlanId;
    UINT1    u1RadioId;
    UINT1    u1WlanId;
    BOOL1    isOptional;
    UINT1    au1Pad[3];
}tvendorWlanVlan;

#ifdef WTP_WANTED
typedef struct{
    UINT2  u2MsgEleType;
    UINT2  u2MsgEleLen;
    UINT4  u4IpAddr;
    UINT4  u4VendorId;
    UINT4  u4SubMask;
    BOOL1  b1AdminStatus;
    UINT1  au1Pad[3];
}tVendorWlanInterfaceIp;

typedef struct {
  UINT4     u4VendorId;
  UINT2     u2MsgEleLen;
  UINT2     u2MsgEleType;
  UINT1     u1RadioId;
  UINT1     u1WlanId;
  UINT1     u1InPriority;
  UINT1     u1OutDscp;
  UINT1     u1EntryStatus;
  BOOL1     isOptional;
  UINT1     au1Pad[2];
}tVendorSpecDiffServ;
#endif

/* Vendor Speciic Payload, Type = 37 , Len >= 7*/
typedef struct{
    UINT4    vendorId;
    UINT2    u2MsgEleType; 
    UINT2    u2MsgEleLen;
    UINT2    elementId;
    /* This is used during assembling the packet */
    UINT1    data[WSSWLAN_VENDOR_DATA_LEN];
    BOOL1    isOptional;
    UINT1    au1Pad[1];
    union {
    tvendorDiscoveryType  VendDiscType;
    tvendorUdpServerPort  VendUdpPort;
    tvendorControlDTLS    VendorContDTLS;
    tDot11nvendorType     Dot11nVendor;
    tvendorSsidIsolation  VendSsidIsolation;
#ifdef WLC_WANTED
    tVendorWlanInterface VendorWlanInterfaceIp;
#endif
#ifdef WTP_WANTED
    tVendorMulticast      VendorMulticast;
#endif
    tvendorSsidRateLimit  VendSsidRate;
    tvendorWlanBssIdStats VendWlanBssIdStats;
    tvendorDNS            VendDns;
    tvendorNativeVlan     VendVlan;
    tvendorMgmtSsid       VendSsid;
#ifdef WTP_WANTED
    tVendorWlanInterfaceIp VendorWlanInterfaceIp;
    tVendorSpecDiffServ VendorSpecDiffServ;
#endif
    tvendorWlanVlan       VendWlanVlan;
    tWpaIEElements  WpaIEElements;
    }unVendorSpec;
}tvendorSpecPayload;

/* The below data structures will not store any 
 * WLAN related information and are
 * used to only construct the message elements and 
 * send to the external modules */
typedef struct {
    tWssWlanPowerConstraint WssWlanPowerConstraint;
    tWssWlanEdcaParam        WssWlanEdcaParam;
    tWssWlanQosCapab         WssWlanQosCapab;
    tRsnaIEElements  RsnaIEElements;
    tWssWlanMulticast       WssWlanMulticast;
    UINT2       u2WtpInternalId;
    UINT2       u2MessageType;
    UINT2       u2MessageLength;
    UINT2       u2Capability;
    UINT2       u2KeyLength;
    UINT2       u2VlanId;
    UINT1       u1RadioId;
    UINT1       u1WlanId;
    UINT1       au1Key [WSSWLAN_WEP_KEY_LEN];
    UINT1       au1GroupTsc [WSSWLAN_GROUP_TSC_LEN];
    UINT1       u1KeyIndex;
    UINT1       u1KeyStatus;
    UINT1       u1Qos;
    UINT1       u1AuthType;
    UINT1       u1MacMode;
    UINT1       u1TunnelMode;
    UINT1       au1Ssid [WSSWLAN_SSID_NAME_LEN];
    UINT1       u1SupressSsid;
    UINT1       u1pad;
}tWssWlanAddReq;
#if defined(WPS_WANTED) || defined(WPS_WTP_WANTED)
typedef struct{
    union {
 tWpsIEElements pWpsIEElements;
    }unInfoElem;
    UINT2    u2MessageType; /* 1029 */
    UINT2    u2MessageLength; /* >=4 */
    UINT1    u1EleId;
    BOOL1    isPresent;
    UINT1    u1RadioId;
    UINT1    u1WlanId;
    UINT1    u1BPFlag;
    UINT1    au1Pad[3];
}tIeeeRadioIfInfoElement;
#endif
typedef struct{
    union {
        tWssWlanAddReq    WssWlanAddReq;
        tWssWlanUpdateReq WssWlanUpdateReq;
        tWssWlanDeleteReq WssWlanDeleteReq;
}unWlanConfReq;
#if defined(WPS_WANTED) || defined(WPS_WTP_WANTED)
tIeeeRadioIfInfoElement RadioIfInfoElement;
#endif
tvendorSpecPayload vendSpec[VEND_CONF_MAX];
    UINT1 u1WlanOption;
    UINT1 au1Pad[3];
}tWssWlanConfReq;

typedef struct
{
    UINT2   u2WtpInternalId;
    UINT1   u1NoOfRadios;
    UINT1   au1pad[1];
}tWssWlanPendingConfReq;

#ifdef BAND_SELECT_WANTED
typedef struct
{
    UINT1   u1RadioId;
    UINT1   u1WlanId;
    UINT1   u1BandSelectStatus;
    UINT1   u1AgeOutSuppression;
    BOOL1   isPresent;
    UINT1   au1Pad[3]; 
}tWssWlanBandSelect;
#endif

typedef struct {
    union 
    { 
        tWssWlanConfReq           WssWlanConfigReq;
        tWssWlanConfRsp           WssWlanConfigRsp;
        tWssWlanSetAdminOperStatus  WssWlanSetAdminOperStatus;
        tDot11ProbReqMacFrame     ProbReqMacFrame;
        tWssStaMsgStruct          WssStaMsgStruct;
        tWssWlanPendingConfReq      WssWlanPendingConfReq;
#ifdef BAND_SELECT_WANTED
        tWssWlanBandSelect        WssWlanBandSelect;
#endif
        UINT1                     au1ManagmentSSID[WSSWLAN_SSID_NAME_LEN];
    } unWssWlanMsg;
}tWssWlanMsgStruct;

/* Enum for Database Operations */
enum {
    /* WSS WLAN Module Msgs */
    WSS_WLAN_INIT_MSG,
    WSS_WLAN_PROFILE_ADMIN_STATUS_CHG_MSG,
    WSS_WLAN_BSS_ADMIN_STATUS_CHG_MSG,
    WSS_WLAN_TEST_CONFIG_REQ,
    WSS_WLAN_MAC_PROBE_REQUEST,
    WSS_WLAN_CONFIG_REQ,
    WSS_WLAN_CONFIG_RSP,
    WSS_WLAN_CLEAR_CONFIG,
    WSS_WLAN_SET_MGMT_SSID,
    WSS_WLAN_GET_MGMT_SSID,
    WSS_WLAN_PROCESS_ADD_BIND_REQ,
    WSS_WLAN_PROCESS_DELETE_BIND_REQ,
#ifdef WPS_WTP_WANTED    
    WSS_WLAN_UNSET_WPS_PBC,
#endif    
    WSS_WLAN_SET_BANDSELECT,
    WSS_WLAN_SET_RF_GROUP_ID,
    WSS_WLAN_SET_VLAN_MAPPING = 50
};

/* Prototypes - WSSWLAN Module */
UINT1 WssIfProcessWssWlanMsg PROTO ((UINT4, tWssWlanMsgStruct *));
UINT1 WssIfGetProfileIfIndex PROTO ((UINT4 u4BssIfIndex, UINT4 * pu4WlanProfileIndex));
UINT1 WssIfGetAuthenticationAlgoDetails PROTO ((INT4 i4IfIndex, UINT1 * pu1Dot11AuthAlgo , UINT1 * pu1WebAuthStatus));
#ifdef WPS_WANTED
UINT4 WssSendDeleteWlanMsgfromWps PROTO ((UINT4));
UINT4     
WssSendUpdateWlanMsgfromWps (UINT4 u4WlanIfIndex, UINT2 u2WtpInternalId, UINT1 statusOrAddIE);
#endif
UINT4 WssSendDeleteWlanMsgfromRsna PROTO ((UINT4));
UINT4 WssSendUpdateWlanMsgfromRsna PROTO ((UINT4,UINT1));
BOOL1 WssUtilIsProfileIndexApBinded PROTO ((INT4,UINT4*, UINT1*));

/*Profile index to wlan index conversion utilities */
UINT1 WssGetCapwapWlanId PROTO((UINT4 u4profileId,UINT4 * u4WlanId));

INT4 WssCreateVlan(UINT4 u4IfIndex, UINT2 u2VlanId);
INT4 WssCreateVlanInterface(UINT2 u2VlanId);
INT4 WssDeleteVlan(UINT2 u2VlanId);

/*API to retrieve RSNA authentication */
UINT1 WssGetIsRsnaAuthentication PROTO ((UINT2 u2BssIfIndex));

VOID WssCfGetDot11DesiredSSID PROTO(
     (UINT4 u4WlanIfIndex,
      UINT1 * pu1DesiredSSID));

#ifdef WLC_WANTED
INT4
WSSRadiusAuthentication (tRADIUS_INPUT_AUTH * p_RadiusInputAuth,
                          tRADIUS_SERVER * p_RadiusServerAuth,
                          VOID (*CallBackfPtr) (VOID *),
                          UINT4 ifIndex, UINT1 u1ReqId);

VOID
WSSHandleRadResponse (VOID *pRadRespRcvd);
UINT1 WssifAddRemoveTcFilter(UINT4 u4CapwapBaseWtpProfileId,UINT1 u1DeleteFlag);

#endif


#endif  /* __WSSWLAN_H__ */


