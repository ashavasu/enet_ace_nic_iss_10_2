/********************************************************************
 * * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * *
 * * $Id: netconf.h,v 1.1 2015/07/14 09:40:24 siva Exp $
 * *
 * * Description:
 * *
 * *******************************************************************/


#ifndef _NETCONF_H
#define _NETCONF_H

extern void *NetConfTaskMain(void *);

#endif
