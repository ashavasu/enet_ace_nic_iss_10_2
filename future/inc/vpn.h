/*$Id: vpn.h,v 1.23 2015/07/17 09:49:17 siva Exp $*/
#ifndef __VPN__H_
#define __VPN__H_

#include "iss.h"
#define VPN_MAX_NAME_LENGTH           63
#define VPN_MIN_POLICY_NAME_LENGTH     1
#define VPN_MAX_POLICY_NAME_LENGTH    63
#define RA_USER_NAME_LENGTH           31
#define RA_USER_SECRET_LENGTH         31
#define RA_ADDRESS_POOL_NAME_LENGTH   31
#define VPN_RA_MIN_ADDR_POOL_NAME_LEN  1
#define RA_ADDRESS_POOL_RANGE_LEN     32 /* aaa.bbb.ccc.ddd-www.xxx.yyy.zzz */
#define VPN_SYS_MAX_NUM_TUNNELS      100 
#define VPN_SYS_MAX_REMOTE_IDS       VPN_SYS_MAX_NUM_TUNNELS
#define VPN_IP_ADDR_LEN                    16 
#define VPN_IPV6_ADDR_LEN             39 

#define CLI_RAVPN_SERVER                 "server"
#define CLI_RAVPN_CLIENT                   "client"
#define RAVPN_CLIENT                       0 
#define RAVPN_SERVER                       1 

#define KEY_FILE_LINE_LENGTH        338
#define PRESHARED_KEY_SAVE_FILE       "presharedkey"

#define VPN_SKIP_NAT        1 
#define VPN_SKIP_FWL        2 
#define SKIP_VPN             4 

#define VPN_LOGBUFSIZ        1360
#define VPN_MAXPAGECNT                3

#define VPN_TASK_NAME                "VPN"

#define VPN_EVENT_QUEUE             (const UINT1 *)"SECQ"
#define VPN_INTF_INFO_CHANGE_EVENT   0x00000001

#define VPN_FAILURE                  OSIX_FAILURE
#define VPN_SUCCESS                  OSIX_SUCCESS

#define  VPN_TRANSPORT                       2
#define  VPN_TUNNEL                          1

#define VPN_IKE_PORT                    500
#define VPN_IKE_NATT_PORT              4500

#define VPN_P7_LINK_GET_STAT_FAIL      0
#define VPN_P7_LINK_GET_STAT_UP        1
#define VPN_P7_LINK_GET_STAT_DOWN      2

#define IPSEC_MANUAL_KEY_LEN                 68
#define IPSEC_ESP_KEY_LEN                   512
#define IPSEC_AH_KEY_LEN                    132
 
#define VPN_MAX_PRESHARED_KEY_LEN            31
#define VPN_MIN_PRESHARED_KEY_LEN             8
#define VPN_MAX_KEY_NAME_LEN                 31

#define  VPN_DES_CBC                         4 
#define  VPN_3DES_CBC                        5
#define  VPN_NULLESPALGO                     11 
#define  VPN_AES_128                         12
#define  VPN_AES_192                         13
#define  VPN_AES_256                         14

#define VPN_ID_TYPE_IPV4                    1
#define VPN_ID_TYPE_FQDN                    2
#define VPN_ID_TYPE_EMAIL                    3
#define VPN_ID_TYPE_IPV6                    5                     
#define VPN_ID_TYPE_DN                        9
#define VPN_ID_TYPE_KEYID                   11

#define  VPN_NULLAHALGO                      0 
#define  VPN_MD5                             4 
#define  VPN_HMACMD5                         1 
#define  VPN_HMACSHA1                        2 
#define  VPN_KEYEDMD5                        3 

#define  VPN_IPSEC_MANUAL                    1
#define  VPN_IKE_PRESHAREDKEY                2 
#define  VPN_IKE_CERTIFICATE                 3
#define  VPN_XAUTH                           4
#define  VPN_IKE_RA_PRESHAREDKEY             5
#define  VPN_IKE_RA_CERT                     6
#define  VPN_IKE_XAUTH_CERT                  7

#define VPN_NONE                             0 

#define  VPN_AUTH_RSA                        2
#define  VPN_AUTH_DSA                        3

#define  VPN_CERT_ENCODE_PEM      1
#define  VPN_CERT_ENCODE_DER      2

#define  VPN_CERT_KEY_FILE_NAME_LEN  63
#define  VPN_CERT_FILE_NAME_LEN      63
#define  VPN_DOMAIN_NAME_MAX_LEN     63
#define  VPN_SYS_MAX_CERTS  10

#define VPN_CERT_KEY_VALUE(pVpnCertInfo) (pVpnCertInfo->ac1CertKeyId) 
#define VPN_CA_CERT_KEY_VALUE(pVpnCaCertInfo) (pVpnCaCertInfo->ac1CaCertKeyId) 

#define  SEC_AH                   51 
#define  SEC_ESP                  50
#define  SEC_NO_NEXT_HDR          59

#define  VPN_INBOUND                         1 
#define  VPN_OUTBOUND                        2 

#define  SEC_DFBIT_CLEAR             2  /*To clear the df bit */
#define  VPN_DEFAULT_PORT 9000 

#define  SEC_HMACMD5_KEY_LEN                16 
#define  SEC_HMACSHA1_KEY_LEN               20 
#define  SEC_ESP_DES_KEY_LEN                 8   /* For DES-CBC Key Length */
#define  SEC_ESP_AES_KEY1_LEN               16   /* For AES Key Length */
#define  SEC_ESP_AES_KEY2_LEN               24   /* For AES Key2 Length */
#define  SEC_ESP_AES_KEY3_LEN               32   /* For AES Key3 Length */

#define MAX_LEN_ARRAY                      255

#define  VPN_MANUAL                          1 
#define  VPN_AUTOMATIC                       2 

#define  SEC_NULLESPALGO                     11 
#define  SEC_3DES_CBC                        5
#define  SEC_AES                             12

#define VPN_AH                           SEC_AH
#define VPN_ESP                          SEC_ESP

enum {
        VPN_ADD_REMOTE_ID_INFO = 1,
        VPN_DEL_REMOTE_ID_INFO,
        VPN_ADD_RA_USER_INFO,
        VPN_DEL_RA_USER_INFO
    };


#define VPN_POLICY_ANTI_REPLAY_STATUS(pVpnPolicy) (pVpnPolicy->u1AntiReplay)

/* Remote identity information table */
#define VPN_ID_TYPE(pVpnIdInfo)        (pVpnIdInfo->i2IdType)
#define VPN_ID_VALUE(pVpnIdInfo)       (pVpnIdInfo->ac1IdValue)
#define VPN_ID_KEY(pVpnIdInfo)         (pVpnIdInfo->ac1IdKey)
#define VPN_ID_STATUS(pVpnIdInfo)      (pVpnIdInfo->i1IdStatus)

#define   VPN_WORD_LEN                 2
#define   VPN_IP_CHKSUM_OFFSET        10
#define   VPN_IP_PROT_FIELD_LEN        1
#define   VPN_PROT_OFFSET_IN_IPHEADER  9
#define   VPN_IP_TOL_LEN_FIELD         2
#define   VPN_IP_TOT_LEN_OFFSET        2
#define   VPN_UDP_HEADER_LENGTH        8
#define   VPN_IP_TOL_LEN_FIELD         2


/*****************************************************************************/
/*       Structure for UDP header info                                       */
/*****************************************************************************/

typedef struct VPNUDPHDR
{
   UINT2                u2LocalPort;
   UINT2                u2RemotePort;
   UINT2                u2Len;
   UINT2                u2CkSum;
} tVpnUdpHdr;



#define IPSEC_POLICY_NAME_LEN 64
#define IPSEC_WEB_ARG_LEN 16
#define IPSEC_MAX_KEY_LEN 512
#define  WEB_PORT_RANGE_LENGTH 12
#define  WEB_SEC_AUTH_KEY_SIZE 44
#define IPSEC_IPADDR_LEN 16
#define WEB_PRESHARED_KEY 50
#define WEB_POL_BUFF 2500

/*Structures for web policy database */

typedef struct _tWebIpsecPolicy{
    UINT1 au1IpSecPolicyName[IPSEC_POLICY_NAME_LEN];
    UINT1 au1IpsecGwIpAddr[IPSEC_IPADDR_LEN];
    UINT1 au1IpsecLocalIpAddr[IPSEC_IPADDR_LEN];
    UINT1 au1IpsecLocalIpMask[IPSEC_IPADDR_LEN];
    UINT1 au1IpsecRemoteIpAddr[IPSEC_IPADDR_LEN];
    UINT1 au1IpsecRemoteIpMask[IPSEC_IPADDR_LEN];
    UINT1 au1IpSecMode[IPSEC_WEB_ARG_LEN];
    UINT1 au1IpSecProtocol[IPSEC_WEB_ARG_LEN];
    UINT1 au1IpSecAuthenticator[IPSEC_WEB_ARG_LEN];
    UINT1 au1IpSecAuthenticationKey[WEB_SEC_AUTH_KEY_SIZE];
    UINT1 au1IpSecEncryption[IPSEC_WEB_ARG_LEN];
    UINT1 au1IpSecEncryptionKey1[IPSEC_MAX_KEY_LEN];
    UINT1 au1IpSecEncryptionKey2[IPSEC_MAX_KEY_LEN];
    UINT1 au1IpSecEncryptionKey3[IPSEC_MAX_KEY_LEN];
    UINT1 au1IpSecOutgoingSPI[IPSEC_WEB_ARG_LEN];
    UINT1 au1IpSecIncomingSPI[IPSEC_WEB_ARG_LEN];
    UINT1 au1IpSecTransportProtocol[IPSEC_WEB_ARG_LEN];
    UINT1 au1IpSecAntiReplay[IPSEC_WEB_ARG_LEN];
}tWebIpSecPolicy;

typedef struct _tWebIkePolicy{
    UINT1 au1IpSecPolicyName[IPSEC_POLICY_NAME_LEN];
    UINT1 au1PresharedKey[WEB_PRESHARED_KEY];
    UINT1 au1IpsecGwIpAddr[IPSEC_IPADDR_LEN];
    UINT1 au1IpsecLocalIpAddr[IPSEC_IPADDR_LEN];
    UINT1 au1IpsecLocalIpMask[IPSEC_IPADDR_LEN];
    UINT1 au1IpSecLocalPortRange[WEB_PORT_RANGE_LENGTH];
    UINT1 au1IpsecRemoteIpAddr[IPSEC_IPADDR_LEN];
    UINT1 au1IpsecRemoteIpMask[IPSEC_IPADDR_LEN];
    UINT1 au1IpSecRemotePortRange[WEB_PORT_RANGE_LENGTH];
    UINT1 au1IpSecTransportProtocol[IPSEC_WEB_ARG_LEN];
    UINT1 au1Phase1Encryption[IPSEC_WEB_ARG_LEN];
    UINT1 au1Phase1Authentication[IPSEC_WEB_ARG_LEN];
    UINT1 au1Phase1DHGroup[IPSEC_WEB_ARG_LEN];
    UINT1 au1Phase1ExchangeType[IPSEC_WEB_ARG_LEN];
    UINT1 au1Phase1LifeTimeType[IPSEC_WEB_ARG_LEN];
    UINT1 au1Phase1LifeTimeValue[IPSEC_WEB_ARG_LEN];
    UINT1 au1Phase2Protocol[IPSEC_WEB_ARG_LEN];
    UINT1 au1Phase2Encryption[IPSEC_WEB_ARG_LEN];
    UINT1 au1Phase2Authentication[IPSEC_WEB_ARG_LEN];
    UINT1 au1Phase2Mode[IPSEC_WEB_ARG_LEN];
    UINT1 au1Phase2DHGroup[IPSEC_WEB_ARG_LEN];
    UINT1 au1Phase2LifeTimeType[IPSEC_WEB_ARG_LEN];
    UINT1 au1Phase2LifeTimeValue[IPSEC_WEB_ARG_LEN];
}tWebIkePolicy;




#define   VPN_IPSEC_PACKET             0
#define   VPN_IKE_PACKET               1
#define   VPN_DROP_PACKET              2
#define   VPN_OTHER_PACKET             3

#define NON_ESP_HDR_LEN                4
#define VPN_UDP_LEN_OFFSET             4
#define VPN_UDP_CHKSUM_OFFSET          2

#define IPSEC_IPADDR_LEN 16

#define VPN_REF_INCR 1
#define VPN_REF_DECR 2

#ifdef KERNEL_PROGRAMMING_WANTED
VOID
VpnKernelDeInit PROTO ((VOID));
INT1
VpnKernelInit PROTO ((VOID));
#endif

UINT4 VpnIsIkeTraffic PROTO ((t_IP_HEADER *pIPHdr, tCRU_BUF_CHAIN_HEADER * pBuf));
UINT4 VpnIsIpsecTraffic PROTO ((tCRU_BUF_CHAIN_HEADER *, UINT1));

UINT4
VpnGetTunnelEndpoint PROTO ((UINT2 u2PolicyIndex, UINT4 u4SrcAddr, 
                             UINT4 u4DestAddr, UINT4 u4Protocol, 
                             UINT4 *u4TunFlag,
                             UINT4 *u4RuleFlag,
                             UINT4 *pu4Src,
                             UINT4 *pu4Dest));

UINT4
VpnGetTunnelEndpointForIp6 PROTO ((tIp6Addr *pIp6SrcAddr,
                            tIp6Addr *pIp6DstAddr, UINT4 u4Protocol,
                            UINT4 *u4TunFlag, UINT4 *u4RuleFlag));

PUBLIC INT4
VpnUtilMatchAclDestIP PROTO ((UINT4 u4QueryIpAddr, INT4 *i4IfIndex));

PUBLIC INT4
VpnUtilGetLogBuffer PROTO ((CHR1 *pVpnLogBuff, UINT4 *u4RetNumBytes, UINT4 u4PgCnt));

PUBLIC INT4
VpnUtilGetSafnetVersion PROTO ((CHR1 *));

PUBLIC INT4
VpnUtilSetSafnetSysTime PROTO ((VOID));

PUBLIC INT4
VpnUtilGetPort7LinkStatus PROTO((INT4 *pi4P7LinkStatus));

/* lrmain.c */
VOID VpnMain(VOID);

INT4
VpnPostMsgToQAndNotify ARG_LIST ((tOsixMsg *pQMsg, UINT4 u4Event));

PUBLIC INT4 VpnUtilGetVpnHealthStatus PROTO ((VOID));

PUBLIC INT4
VpnIntfChangeCallBack PROTO ((tCfaRegInfo *pCfaRegInfo));
/* Statistics Structures */
typedef struct _tVpnGlobPktStats
{
    UINT4 u4PktsIn;
    UINT4 u4PktsOut;
    UINT4 u4PktsSecured;
    UINT4 u4PktsDropped;

} tVpnGlobPktStats;

typedef struct _tVpnGlobIkeSAsStats
{
    UINT4 u4SAsActive;
    UINT4 u4Negotiations;
    UINT4 u4Rekeys;
    UINT4 u4NegoFailed;

} tVpnGlobIkeSAsStats;

typedef struct _tVpnGlobIPSecSAsStats
{
    UINT4 u4SAsActive;
    UINT4 u4Negotiations;
    UINT4 u4NegoFailed;
    
} tVpnGlobIPSecSAsStats;

typedef struct _tVpnGlobSAsStats
{
    tVpnGlobIkeSAsStats   IKE;
    tVpnGlobIPSecSAsStats IPSec;
    UINT4                 u4TotalRekeys;

} tVpnGlobSAsStats;

/* Structure to inform the interface information to VPN */
typedef struct _tVpnIntfInfoChgEvtMsg {
    UINT4 u4IfAddrType;/*IPVX_ADDR_FMLY_IPV4/IPVX_ADDR_FMLY_IPV6 */
    UINT4 u4IfIndex;   /* Interface for which changes have been done */
    UINT4 u4IpAddr;    /* Present ip address */
    UINT1 u1Status;    /* Interface is DOWN/IP_ADDR_CHANGED/DELETED */
    UINT1 u1NwType;    /* WAN/LAN */
    UINT1 u1WanType;   /* PUBLIC/PRIVATE */
    UINT1 i1Pad;
} tVpnIntfInfoChgEvtMsg;

#define  tIp4Addr                           UINT4

typedef struct VPNIPADDR {
    union
    {
    tIp4Addr  Ip4Addr;
       tIp6Addr Ip6Addr;
    } uIpAddr;
    UINT4    u4AddrType;
} tVpnIpAddr;

typedef struct VPNNETWORK {
    tVpnIpAddr IpAddr;
    UINT4     u4AddrPrefixLen;
    UINT2     u2StartPort;
    UINT2     u2EndPort;
} tVpnNetwork;

typedef struct IkeUserId {
    INT2    i2IdType;
    UINT2   u2Length;
    union
    {
        tIp4Addr Ip4Addr;
        tIp6Addr Ip6Addr;
        UINT1    au1Email[VPN_MAX_NAME_LENGTH + 1];   
        UINT1    au1Fqdn[VPN_MAX_NAME_LENGTH + 1]; /* Fully qualified domain 
                                                      name */
        UINT1    au1Dn[VPN_MAX_NAME_LENGTH + 1]; /* X.500 Distinguished Name */ 
        UINT1    au1KeyId[VPN_MAX_NAME_LENGTH + 1];   /* Key Id  */ 
    } uID;
} tIkeUserID;


typedef struct IkePhase1 {
    tIkeUserID    PolicyPeerID;            /* peer ID for policy  */
    tIkeUserID    PolicyLocalID;           /* Local ID for policy  */
    UINT4         u4HashAlgo;              /* Hash Algo, md5|sha */
    UINT4         u4EncryptionAlgo;        /* Encr Algo, des|3des|aes */
    UINT4         u4DHGroup;               /* Diffie-Helman Group 1|2 */
    UINT4         u4LifeTimeType;          /* secs, mins, hrs or days */
    UINT4         u4LifeTime;              /* Lifetime in units of 
                                              LifeTimeType */
    UINT4         u4Status;                /* Row Status */
    UINT4         u4Mode;                  /* Main | Aggressive mode */
    
} tIkePhase1;

typedef struct IkePhase2 {
    UINT1         u1AuthAlgo;              /* Hash Algo, md5|sha */
    UINT1         u1EncryptionAlgo;        /* Encr Algo, des|3des|aes */
    UINT1         u1LifeTimeType;          /* secs, mins, hrs or days */
    UINT1         u1DHGroup;               /* Diffie-Helman Group 1|2 */
    UINT4         u4LifeTime;              /* Lifetime in units of 
                                              LifeTimeType */
} tIkePhase2;

typedef struct VpnIkeDb {
    tIkePhase1 IkePhase1;    /* IKE Phase I Proposal */
    tIkePhase2 IkePhase2;    /* IPSec Phase II Proposal */
}tVpnIkeDb;


/* Keying mode: manual */
typedef struct VPNMANUALKEY {

    UINT1        au1VpnAhKey[IPSEC_AH_KEY_LEN];
    UINT1        au2VpnEspKey[IPSEC_ESP_KEY_LEN] ; /* des/3des/aes */
    UINT1        u1VpnAuthAlgo;    /* Auth Algo: HMAC-MD5 | HMAC-SHA1 */

    UINT1        u1VpnEncryptionAlgo; /* Encr Algo: DES|3-DES|AES128|192|256 */
    UINT1        au1Pad[2];          /* Pad bytes */
    UINT4        u4VpnAhInboundSpi;      /* Inbound Security Parameter Index */
    UINT4        u4VpnAhOutboundSpi;     /* Outbound Security Parameter Index */
    UINT4        u4VpnEspInboundSpi;     /* Inbound Security Parameter Index */
    UINT4        u4VpnEspOutboundSpi;    /* Outbound Security Parameter Index */
} tVpnManualKey;

/* VPN Policy Database */
typedef struct VPNPOLICY {
    tTMO_SLL_NODE    NextVpnPolicy;

    /* Policy Name used as index to identify a policy in db */
    UINT1        au1VpnPolicyName[VPN_MAX_POLICY_NAME_LENGTH + 1];
    UINT4        u4VpnPolicyIndex; /* Index to identify the vpn policy
                                      and also as a mapping between SA,
                                      selector,Policy and Access List */
    UINT4        u4VpnPolicyType;  /* VPN POLICY type, used as index
                                      (ipsec,ike,xauth,rapsk) */
    UINT4        u4VpnSecurityProtocol;  /* AH or ESP */
    UINT4        u4IfIndex;      /* Interface id on which policy is applied */

    union
    {
        tVpnManualKey   IpsecManualKey;  /* Mode: Manual Keying */
        tVpnIkeDb       VpnIkeDb;     /* IKE phase 1 and IPSec phase 2 proposals*/
    } uVpnKeyMode;

    UINT4          u4VpnProtocol;     /* tcp, udp, icmp, any, etc. */
    UINT4          u4VpnPolicyFlag;   /* apply / bypass / filter */
    UINT4          u4VpnPolicyPriority; /* To identify the priority of
                                           the policy */
    UINT4          u4ProtectNetType;   /* Protected n/w address type (v4/v6) */
    UINT4          u4TunTermAddrType; /* Tunnel termination address type
                                        (v4/v6) */                                          
    tVpnNetwork    LocalProtectNetwork;      /* Local Network */
    tVpnNetwork    RemoteProtectNetwork;     /* Remote Network*/

    tVpnIpAddr     LocalTunnTermAddr;  /* Local tunnel termination addr(v4/v6)  */
    tVpnIpAddr     RemoteTunnTermAddr;  /* Remote tunnel termination addr(v4/v6) */

    UINT1        u1VpnMode;      /* tunnel or transport */
    UINT1        u1AntiReplay;   /* To check antiReplay is enabled or not */
    UINT1        u1VpnPolicyRowStatus;  /* Row status (for SNMP) */
    UINT1        u1VpnIkeVer;
    UINT1        u1AuthAlgoType; /* Authentication Algorithm Type (RSA/DSA) */
    UINT1        au1Pad[3];
} tVpnPolicy;


/* Vpn Remote Access Users Database */
typedef struct VpnRaUserInfo {

    tTMO_SLL_NODE    VpnRaUserInfo;
    UINT1        au1VpnRaUserName[RA_USER_NAME_LENGTH + 1]; /* VPN User Name
                                                          used as index */
    UINT1        au1VpnRaUserSecret[RA_USER_SECRET_LENGTH + 1]; /* VPN User 
                                                          Password */
    INT4         i4RowStatus; /* Row Status for RA User Entry */

} tVpnRaUserInfo;

/* Vpn Remote Access Address Pool Database */
typedef struct VpnRaAddressPool {
    tTMO_SLL_NODE    VpnRaAddressPool;
    /* VPN Address Pool Name Used as Index */
    UINT1        au1VpnRaAddressPoolName[RA_ADDRESS_POOL_NAME_LENGTH + 1];
    tVpnIpAddr   VpnRaAddressPoolStart;   /*Starting Ip Address of pool 
                                            for the remote users. */ 
    tVpnIpAddr   VpnRaAddressPoolEnd;     /*End Ip Address of pool 
                                            for the remote users.*/ 
    UINT4        u4VpnRaAddressPoolPrefixLen;  /*Netmask for the  pool 
                                            for the remote users.*/
    tTMO_SLL     VpnAllocAddrList;         /* List to hold the assigned
                                             * Address Pool. */                                                 
    INT4         i4RefCnt;  /*No of active policy related with the address pool */
    INT4         i4RowStatus;   /* Row Status for RA Address Pool Entry */ 
} tVpnRaAddressPool;

/* Structure to hold the list of ip addresses assigned from the list. */
typedef struct VpnAllocRaAddrNode{
    tTMO_SLL_NODE *pNextAllocAddrNode; /* Pointer to the next node in the list */
    tVpnIpAddr  IpAddr;            /* IP address assigned from the pool*/
    tVpnIpAddr  PeerIpAddr;       /* Peer IP address to which IP is assigned */
}tVpnAllocRaAddrNode;

/* Data strucuture represents a single remote id information node */
typedef struct {
    tTMO_SLL_NODE  IdNode; /* Link to find the index to fetch key */

    INT2           i2IdType; /* IPv4/Fqdn/Email/KeyId */
    INT1           i1IdStatus;
    INT1           i1Pad;
    UINT4          u4AuthType; /*Added to Identify the authentication type */
    CHR1           ac1IdValue[VPN_MAX_NAME_LENGTH + 1]; /* NULL terminated */
    CHR1           ac1IdKey[VPN_MAX_PRESHARED_KEY_LEN + 1]; /* Preshared Key */
    INT4           i4RefCnt;
} tVpnIdInfo;

/* Data strucuture represents a Certificate information node */
typedef struct {
    tTMO_SLL_NODE  IdNode; /* Link to find the index to fetch key */

    INT4           i4CertEncType; /* Identify the encoding type */
    INT4           i4CertKeyType; /*Identify the algorithm type */
    INT4           i4RefCnt; /* Number of policies referring the certificate */
    CHR1           ac1CertKeyId[VPN_MAX_PRESHARED_KEY_LEN + 1]; /* Certificate identity Key */
    CHR1           ac1CertKeyFileName[VPN_MAX_PRESHARED_KEY_LEN + 1]; /* Certificate Key Filename*/
    CHR1           ac1CertFileName[VPN_MAX_PRESHARED_KEY_LEN + 1]; /* Certificate Key Filename*/
    INT1           i1IdStatus;
    INT1           i1Pad[3];
} tVpnCertInfo;

/* Data strucuture represents a CA Certificate information node */
typedef struct {
    tTMO_SLL_NODE  IdNode; /* Link to find the index to fetch key */

    INT4           i4CaCertEncType; /* Identify the encoding type */   
    INT4           i4CaRefCnt; /* Number of policies referring the certificate */
    CHR1           ac1CaCertKeyId[VPN_MAX_PRESHARED_KEY_LEN + 1]; /* CA Certificate identity Key */
    CHR1           ac1CaCertFileName[VPN_MAX_PRESHARED_KEY_LEN + 1]; /* CA Certificate Key Filename*/
    INT1           i1IdStatus;
    INT1           i1Pad[3];
} tVpnCaCertInfo;


/* Vpn callback entries */
typedef enum VpnCallBackEvents
{
    VPN_READ_PRE_SHARED_KEY_FROM_NVRAM = 1,  
    VPN_ACCESS_SECURE_MEMORY,
    VPN_MAX_CALLBACK_EVENTS
}eVpnCallBackEvents;

/* To handle CallBack for VPN module */
typedef union VpnCallBackEntry {
    INT4 (*pVpnReadPreSharedKeyFromNVRAM) (VOID);
    INT4 (*pVpnCustSecureMemAccess) PROTO((UINT4, UINT4, ...));
}unVpnCallBackEntry;

/* Prototypes of the functions to interface VPN*/
INT4   Secv6VpnFillIpsecParams PROTO ((tVpnPolicy * pVpnPolicy));
INT4   Secv6VpnDeleteIpsecParams PROTO ((tVpnPolicy * pVpnPolicy));
VOID   Secv6VpnGetIpsecSecurityStats PROTO ((INT4 i4StatType,
                                             UINT4 * pu4Stats));
VOID   Secv6VpnGetIpsecPktStats PROTO ((INT4 i4StatType, UINT4 * pu4Stats));
INT4   VpnUtilRegCallBackforVPNModule (UINT4 , tFsCbInfo * ); 
INT4   VpnReadPreSharedKeyFromNVRAM (VOID);
INT4   VpnApiRestorePreSharedKeyFromNvRam PROTO((VOID));
INT4   VPNCheckIpInRAAddrPool(UINT4 pu4IpAddr);
UINT4  VpnGetAssoIntfForVpnc PROTO ((tIP_BUF_CHAIN_HEADER * pCBuf,
                                     UINT4 *pu4AssoIfIndex, UINT4 *pu4Dest));
VOID   Secv6VpnGetIpsecSaEntryCount PROTO ((UINT4 *u4IPSecSAsActive));

INT4 VpnUtilPolicyRestore PROTO((VOID));

INT4 VpnIkeCheckAndCreateEngine PROTO((tVpnPolicy *pVpnPolicy));

tVpnIdInfo         *
VpnGetRemoteIdInfo ARG_LIST ((INT4 i4IdType, CONST UINT1 *pu1IdValue, INT4 i4IdLen));
tVpnCertInfo         *
VpnGetCertInfoFromKey ARG_LIST ((CONST UINT1 *pu1IdValue, INT4 i4IdLen));
tVpnCaCertInfo         *
VpnGetCaCertInfoFromKey ARG_LIST ((CONST UINT1 *pu1IdValue, INT4 i4IdLen));

#endif /* __VPN__H_ */

