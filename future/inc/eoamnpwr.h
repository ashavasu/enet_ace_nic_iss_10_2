
/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: eoamnpwr.h,v 1.2 2012/08/08 10:02:24 siva Exp $
 * 
 * Description: This file contains the necessary data strucutures
 *              for Hardware Programming.
 *              <Part of dual node stacking model>
 *
 * *********************************************************************/

#ifndef _EOAM_NP_WR_H
#define _EOAM_NP_WR_H

#include "eoam.h"
#ifdef MBSM_WANTED
#include "mbsm.h"
#endif /* MBSM_WANTED */

/* OPER ID */

#define  EOAM_LM_NP_GET_STAT                                    1
#define  EOAM_LM_NP_GET_STAT64                                  2
#define  EOAM_NP_DE_INIT                                        3
#define  EOAM_NP_GET_UNI_DIRECTIONAL_CAPABILITY                 4
#define  EOAM_NP_HANDLE_LOCAL_LOOP_BACK                         5
#define  EOAM_NP_HANDLE_REMOTE_LOOP_BACK                        6
#define  EOAM_NP_INIT                                           7
#define  EOAM_LM_NP_REGISTER_LINK_MONITOR                       8
#define  EOAM_LM_NP_CONFIGURE_PARAMS                            9
#ifdef MBSM_WANTED
#define  EOAM_MBSM_HW_INIT                                      10
#endif /* MBSM_WANTED */

/* Required arguments list for the eoam NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

typedef struct {
    UINT4    u4IfIndex;
    UINT4 *  pu4Value;
    UINT1    u1StatType;
    UINT1    au1pad[3];
} tEoamNpWrEoamLmNpGetStat;

typedef struct {
    UINT4       u4IfIndex;
    FS_UINT8 *  pu8Value;
    UINT1       u1StatType;
    UINT1    au1pad[3];
} tEoamNpWrEoamLmNpGetStat64;

typedef struct {
    UINT4    u4IfIndex;
    UINT1 *  pu1Capability;
} tEoamNpWrEoamNpGetUniDirectionalCapability;

typedef struct {
    UINT4     u4IfIndex;
    UINT1 *  SrcMac;
    UINT1 *  DestMac;
    UINT1     u1Status;
    UINT1    au1pad[3];
} tEoamNpWrEoamNpHandleLocalLoopBack;

typedef struct {
    UINT4     u4IfIndex;
    UINT1 *  SrcMac;
    UINT1 *  PeerMac;
    UINT1     u1Status;
    UINT1    au1pad[3];
} tEoamNpWrEoamNpHandleRemoteLoopBack;

typedef struct {
    UINT4     u4IfIndex;
    FS_UINT8  Window;
    FS_UINT8  Threshold;
    UINT2     u2Event;
    UINT1    au1pad[2];
} tEoamNpWrEoamLmNpConfigureParams;

#ifdef MBSM_WANTED
typedef struct {
    tMbsmSlotInfo *  pSlotInfo;
} tEoamNpWrEoamMbsmHwInit;

#endif /* MBSM_WANTED */
typedef struct EoamNpModInfo {
union {
    tEoamNpWrEoamLmNpGetStat  sEoamLmNpGetStat;
    tEoamNpWrEoamLmNpGetStat64  sEoamLmNpGetStat64;
    tEoamNpWrEoamNpGetUniDirectionalCapability  sEoamNpGetUniDirectionalCapability;
    tEoamNpWrEoamNpHandleLocalLoopBack  sEoamNpHandleLocalLoopBack;
    tEoamNpWrEoamNpHandleRemoteLoopBack  sEoamNpHandleRemoteLoopBack;
    tEoamNpWrEoamLmNpConfigureParams  sEoamLmNpConfigureParams;
#ifdef MBSM_WANTED
    tEoamNpWrEoamMbsmHwInit  sEoamMbsmHwInit;
#endif /* MBSM_WANTED */
    }unOpCode;

#define  EoamNpEoamLmNpGetStat  unOpCode.sEoamLmNpGetStat;
#define  EoamNpEoamLmNpGetStat64  unOpCode.sEoamLmNpGetStat64;
#define  EoamNpEoamNpGetUniDirectionalCapability  unOpCode.sEoamNpGetUniDirectionalCapability;
#define  EoamNpEoamNpHandleLocalLoopBack  unOpCode.sEoamNpHandleLocalLoopBack;
#define  EoamNpEoamNpHandleRemoteLoopBack  unOpCode.sEoamNpHandleRemoteLoopBack;
#define  EoamNpEoamLmNpConfigureParams  unOpCode.sEoamLmNpConfigureParams;
#ifdef MBSM_WANTED
#define  EoamNpEoamMbsmHwInit  unOpCode.sEoamMbsmHwInit;
#endif /* MBSM_WANTED */
} tEoamNpModInfo;


/* Function Prototype for Generic NP OpCode specific wrappers
 * defined in Eoamnpapi.c */

UINT1 EoamEoamLmNpGetStat PROTO ((UINT4 u4IfIndex, UINT1 u1StatType, UINT4 * pu4Value));
UINT1 EoamEoamLmNpGetStat64 PROTO ((UINT4 u4IfIndex, UINT1 u1StatType, FS_UINT8 * pu8Value));
UINT1 EoamEoamNpDeInit PROTO ((VOID));
UINT1 EoamEoamNpGetUniDirectionalCapability PROTO ((UINT4 u4IfIndex, UINT1 * pu1Capability));
UINT1 EoamEoamNpHandleLocalLoopBack PROTO ((UINT4 u4IfIndex, tMacAddr SrcMac, tMacAddr DestMac, UINT1 u1Status));
UINT1 EoamEoamNpHandleRemoteLoopBack PROTO ((UINT4 u4IfIndex, tMacAddr SrcMac, tMacAddr PeerMac, UINT1 u1Status));
UINT1 EoamEoamNpInit PROTO ((VOID));
UINT1 EoamEoamLmNpRegisterLinkMonitor PROTO ((VOID));
UINT1 EoamEoamLmNpConfigureParams PROTO ((UINT4 u4IfIndex, UINT2 u2Event, FS_UINT8 Window, FS_UINT8 Threshold));
#ifdef MBSM_WANTED
UINT1 EoamEoamMbsmHwInit PROTO ((tMbsmSlotInfo * pSlotInfo));
#endif /* MBSM_WANTED */

#endif /* _EOAM_NP_WR_H */

