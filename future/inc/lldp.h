/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: lldp.h,v 1.52 2017/12/14 10:30:23 siva Exp $
 *
 * Description: Contains definitions, macros and functions to be used
 *              by  external modules.
 ********************************************************************/
#ifndef _LLDP_H_
#define _LLDP_H_

#include "ip.h"
#include "ipv6.h"
#include "iss.h"
#include "rmgr.h"
#include "l2iwf.h" /* l2iwf.h contains the LLDP application related
                      definitions. So this file needs to be included
                      before using LLDP include file */

#define LLDP_ENET_TYPE                      0x88cc
#define LLDP_AGG_NOT_CAPABLE                0
#define LLDP_AGG_CAPABLE                    1

#define LLDP_AGG_NOT_ACTIVE_PORT            0
#define LLDP_AGG_ACTIVE_PORT                1

#define LLDP_SRCADDR_OFFSET                 6
#define LLDP_PROTOCOL_TYPE_OFFSET           12
#define LLDP_SNAP_ENCODED_PROTO_TYPE_OFFSET 18

#define LLDP_MED_DEF_TLV_SUPPORTED          0x002F
#define LLDP_MED_MAX_TLV_SUPPORTED          0x003F


/* Basic TLVs TxEnable
 *      0          1          2           3        4  5  6  7
 * port-descr   sys-name   sys-descr   sys-capab   -  -  -  -
 * */
#define LLDP_PORT_DESC_TLV_ENABLED         0x80
#define LLDP_SYS_NAME_TLV_ENABLED          0x40
#define LLDP_SYS_DESC_TLV_ENABLED          0x20
#define LLDP_SYS_CAPAB_TLV_ENABLED         0x10

#define LLDP_MAN_ADDR_TLV_ENABLED          0x08

#define LLDP_MAC_PHY_TLV_ENABLED           0x80
#define LLDP_PWR_VIA_MDI_TLV_ENABLED       0x40
#define LLDP_LINK_AGG_TLV_ENABLED          0x20
#define LLDP_MAX_FRAME_SIZE_TLV_ENABLED    0x10
#define LLDP_MAX_LEN_PROTOID               255
#define LLDP_MAX_LEN_ORGDEF_INFO           508
#define LLDP_MAX_LEN_OUI                   L2IWF_LLDP_MAX_LEN_OUI
#define LLDP_MAX_LEN_UNKNOWN_TLV_INFO      512
#define LLDP_MAX_VID_DIGEST_LEN            128

#define LLDP_MAX_DEST_INDEX                255

/* Each byte(8bits) represents 8 ports, hence the following macro
 *  * represents (maximum number of ports supported)/8 */
#define LLDP_MAN_ADDR_TX_EN_MAX_BYTES      MAX_LLDP_LOC_PORT_TABLE


/* System control status */
#define  LLDP_START                        1
#define  LLDP_SHUTDOWN_INPROGRESS          2 /* This is a read-only status
                                              * which LLDP module shutdown is
                                              * initiated. System variable is
                                              * set to shutdown after the global
                                              * timer expires */
#define  LLDP_SHUTDOWN                     3

/* Module status */
#define  LLDP_ENABLED                      1
#define  LLDP_DISABLED                     2

/* LLDP SYSTEM Tag Status */
#define LLDP_TAG_ENABLE                    1
#define LLDP_TAG_DISABLE                   2


/* Version Status */
#define LLDP_VERSION_05                    1
#define LLDP_VERSION_09                    2

#define LLDP_TRUE                          1
#define LLDP_FALSE                         2
 
#define LLDP_ONE                           1 
#define LLDP_MED_TRUE                      1
#define LLDP_MED_FALSE                     0

#define LLDP_PORTS_PER_BYTE                8
#define LLDP_BIT8                          0x80


#define  LLDP_TRC_FLAG  gLldpGlobalInfo.u4TraceOption

#define  LLDP_NAME                         "[LLDP] "
#define LLDP_TRC(TraceType, Str)                                              \
            MOD_TRC(LLDP_TRC_FLAG, TraceType, LLDP_NAME, Str)

#define LLDP_TRC_MAX_SIZE                  288

#define LLDP_MAX_LEN_PORTID                255
#define LLDP_MAX_LEN_CHASSISID             255
#define LLDP_DESTADDR_OFFSET               0


#define LLDP_MIN_PORTS                     1

/* LLDP_MAX_PORTS is scalable by adjusting MAX_LLDP_LOC_PORT_TABLE in system.size*/
/*MAX_LLDP_LOC_PORT_TABLE should always less than LLDP_MAX_SCAL_PORTS*/
#define LLDP_MAX_PORTS    FsLLDPSizingParams[MAX_LLDP_LOC_PORT_TABLE_SIZING_ID].u4PreAllocatedUnits
#define LLDP_MAX_SCAL_PORTS                1000 /* This macro definition is not used */
        /* for sizing purpose.This is used in */
        /* Array and array of pointer size */
/* LLDP Tx Port Config Default Values */
#define  LLDP_MSG_TX_INTERVAL              30 /* Message transmit interval */
#define  LLDP_MSG_TX_HOLD_MULTIPLIER       4  /* Message hold time multiplier */
#define  LLDP_REINIT_DELAY                 2  /* Reinit delay */
#define  LLDP_TX_DELAY                     2  /* Transmit delay */
#define  LLDP_NOTIFICATION_INTERVAL        5   /* Trap Notification Interval */
#define  LLDPV2_NOTIFICATION_INTERVAL      30   /* Trap Notification Interval */
#define LLDP_MAX_LEN_SYSNAME               255
#define LLDP_MAX_LEN_SYSDESC               255
#define LLDP_MAX_LEN_PORTDESC              255
#define LLDP_MAX_LEN_MAN_OID               128
#define LLDP_MAX_LEN_MAN_ADDR              31  /* Max 31 Bytes possible */
#define LLDP_MAX_LEN_ADVT_CAPAB            4   /* Advertised Capability Bits */
#define LLDP_MAX_DUP_TLV_ARRAY_SIZE        1  /* Useful to set or reset the bits
                                                of a duplicate TLVs check flag */
#define LLDP_MAX_PDU_SIZE                  1500

/* LLDP_MED Module status */
#define  LLDP_MED_ENABLED                      1
#define  LLDP_MED_DISABLED                     2

#define LLDP_MED_HW_REV_LEN                     ISS_ENT_PHY_HW_REV_LEN 
#define LLDP_MED_FW_REV_LEN                     ISS_ENT_PHY_FW_REV_LEN
#define LLDP_MED_SW_REV_LEN                     ISS_ENT_PHY_SW_REV_LEN
#define LLDP_MED_MFG_NAME_LEN                   ISS_ENT_PHY_MFG_NAME_LEN
#define LLDP_MED_MODEL_NAME_LEN                 ISS_ENT_PHY_MODEL_NAME_LEN
#define LLDP_MED_SER_NUM_LEN                    ISS_ENT_PHY_SER_NUM_LEN
#define LLDP_MED_ASSET_ID_LEN                   ISS_ENT_PHY_ASSET_ID_LEN
#define LLDP_MED_MAX_LOC_LENGTH               256
#define LLDP_MED_MAX_COORDINATE_LOC_LENGTH    16
#define LLDP_MED_MIN_ELIN_LOC_LENGTH          10
#define LLDP_MED_MAX_ELIN_LOC_LENGTH          25
#define LLDP_MED_MIN_CIVIC_LOC_LENGTH         6
#define LLDP_MED_MAX_CIVIC_LOC_LENGTH         256
#define LLDP_MED_LOC_DATA_FORMAT_LEN          1
#define LLDP_MED_MAX_DISPLAY_STRING           256
#define LLDP_MED_MAX_LATITUDE                 90
#define LLDP_MED_MIN_LATITUDE                 -90
#define LLDP_MED_MAX_LONGITUDE                180
#define LLDP_MED_MIN_LONGITUDE                -180
#define LLDP_MED_MAX_ALTITUDE                 2097151
#define LLDP_MED_MIN_ALTITUDE                 -2097151
#define LLDP_MED_MAX_CA_TYPE_LENGTH           250
#define LLDP_MED_COUNTRY_CODE_LENGTH          2
#define LLDP_MED_CIVIC_WHAT_LENGTH            1
#define LLDP_MED_DEFAULT_STRING               "Not Applicable"


#define   LLDP_CHK_NULL_PTR_RET(ptr,RetVal)                   \
          if(ptr == NULL)                                     \
          {                                                   \
              return RetVal;                                  \
          }

/* This Macro has to be moved to inc/params.h */
#define MAX_LLDP_VLAN_NAME_TLV_COUNTER     1

/* LLDP-MED Macros */
#define LLDP_MED_CAPABILITY_TLV            0x0001
#define LLDP_MED_NETWORK_POLICY_TLV        0x0002
#define LLDP_MED_LOCATION_ID_TLV           0x0004
#define LLDP_MED_PW_MDI_PSE_TLV            0x0008
#define LLDP_MED_PW_MDI_PD_TLV             0x0010
#define LLDP_MED_INVENTORY_MGMT_TLV        0x0020

/* LLDP MED Application Types */
enum
{
    LLDP_MED_VOICE_APP                     = 1, /* Voice Application */
    LLDP_MED_VOICE_SIGNALING_APP           = 2, /* Voice Signaling Application */
    LLDP_MED_GUEST_VOICE_APP               = 3, /* Guest Voice Application */
    LLDP_MED_GUEST_VOICE_SIGNALING_APP     = 4, /* Guest Voice Signaling Application */
    LLDP_MED_SOFT_PHONE_VOICE_APP          = 5, /* Soft Phone Voice Application */
    LLDP_MED_VIDEO_CONFERENCING_APP        = 6, /* Video Conferencing Application */
    LLDP_MED_STREAMING_VIDEO_APP           = 7, /* Streaming Video Application */
    LLDP_MED_VIDEO_SIGNALING_APP           = 8  /* Video Signaling Application */
};

/* LLDP MED Location Subtypes */
enum
{
    LLDP_MED_COORDINATE_LOC                = 2, /* Co-ordinate Location Subtype */
    LLDP_MED_CIVIC_LOC                     = 3, /* Civic Location Subtype */
    LLDP_MED_ELIN_LOC                      = 4  /* Elin Location Subtype */
};


/* LLDP MED Altitude Types */
enum
{
    LLDP_MED_ALT_METER                     = 1, /* Altitude in Meters */
    LLDP_MED_ALT_FLOOR                     = 2  /* Altitude in Floors */
};


/* LLDP MED CA Types */
enum
{
    LLDP_MED_CA_TYPE_LANG                   = 0,   /* Language */
    LLDP_MED_CA_TYPE_STATE                  = 1,   /* National sub division */
    LLDP_MED_CA_TYPE_DISTRICT               = 2,   /* District */
    LLDP_MED_CA_TYPE_CITY                   = 3,   /* City */
    LLDP_MED_CA_TYPE_CITY_DIVISION          = 4,   /* City Division */
    LLDP_MED_CA_TYPE_BLOCK                  = 5,   /* Block */
    LLDP_MED_CA_TYPE_STREET                 = 6,   /* Street */
    LLDP_MED_CA_TYPE_LEAD_STREET_DIR        = 16,  /* Street-direction */
    LLDP_MED_CA_TYPE_TRAIL_STREET_DIR       = 17,  /* Trailing street suffix */
    LLDP_MED_CA_TYPE_STREET_SUFFIX          = 18,  /* Street suffix */
    LLDP_MED_CA_TYPE_HOUSE_NO               = 19,  /* House-number */
    LLDP_MED_CA_TYPE_HOUSE_NO_SUFFIX        = 20,  /* House-number-suffix */
    LLDP_MED_CA_TYPE_LANDMARK               = 21,  /* Landmark */
    LLDP_MED_CA_TYPE_ADD_LOC_INFO           = 22,  /* Additional-location-information */
    LLDP_MED_CA_TYPE_NAME                   = 23,  /* Name */
    LLDP_MED_CA_TYPE_PINCODE                = 24,  /* Pin-code */
    LLDP_MED_CA_TYPE_BUILDING               = 25,  /* Building */
    LLDP_MED_CA_TYPE_APARTMENT              = 26,  /* Apartment */
    LLDP_MED_CA_TYPE_FLOOR                  = 27,  /* Floor */
    LLDP_MED_CA_TYPE_ROOM_NUMBER            = 28,  /* Room Number */
    LLDP_MED_CA_TYPE_PLACE_TYPE             = 29,  /* Place Type */
    LLDP_MED_CA_TYPE_POSTAL_NAME            = 30,  /* Postal Name */
    LLDP_MED_CA_TYPE_POST_BOX_NUMBER        = 31,  /* Post Box Number */
    LLDP_MED_CA_TYPE_ADDITIONAL_CODE        = 32,  /* Additional Code */
    LLDP_MED_CA_TYPE_SCRIPT                 = 128  /* Script */
};


/* LLDP MED Civic Client location types */
enum
{
    LLDP_MED_CIVIC_DHCP_SERVER              = 0, /* DHCP server location */
    LLDP_MED_CIVIC_CLOSE_DEVICE             = 1, /* Location of the device which is close to client */
    LLDP_MED_CIVIC_CLIENT                   = 2  /* Clinet location */
};


/* LLDP Timer Types */
enum
{
    LLDP_TX_TMR_SHUT_WHILE                 = 1, /* ShutdownWhile/Reinit delay*/
    LLDP_TX_TMR_TX_DELAY                   = 2, /* DelayWhile/Transmit delay*/
    LLDP_TX_TMR_TTR                        = 3, /* TTR/Message Interval*/
    LLDP_RX_TMR_RX_INFO_TTL                = 4, /* InfoTTL/Recvd info age*/
    LLDP_NOTIF_INT_TIMER                   = 5, /* Notification interval timer*/
    LLDP_TX_TMR_GLOBAL_SHUT_WHILE          = 6, /* Global ShutdownWhile delay*/
    LLDP_MAX_TIMER_TYPES                   = 7  /* Maxmum types of Timers */
};

/* LLDP Admin Status */
enum
{
    LLDP_ADM_STAT_TX_ONLY                  = 1, /* AdminStatus - Tx Only */
    LLDP_ADM_STAT_RX_ONLY                  = 2, /* AdminStatus - Rx Only */
    LLDP_ADM_STAT_TX_AND_RX                = 3, /* AdminStatus - Tx and Rx */
    LLDP_ADM_STAT_DISABLED                 = 4, /* AdminStatus - Disabled */
    LLDP_MAX_ADMIN_STAT                    = 5  /* Max */
};


typedef struct LldpManAddrOid
{
    UINT4  u4_Length;
    UINT4  au4_OidList[LLDP_MAX_LEN_MAN_OID];
}tLldpManAddrOid;

typedef struct __LldpAgent
{
  INT4          i4IfIndex;
  tMacAddr      MacAddr;
  UINT1         u1RowStatus;
  UINT1         au1Pad[5];
}tLldpAgent;

typedef struct
{
        UINT4 u4IfIndex; /* UAP Interface Index */
        UINT4 u4Svid;
} tLldpUpdateSvidOnUap;

/* LLDP timer structure */
typedef struct _LldpTmr {
    tTmrBlk    LldpTmrBlk;
    UINT1      u1Status;
    UINT1      au1Pad[3];
} tLldpTmr;

/* LLDP-MED Data strcutures */

/* LLDP-MED Local Capabilities Data strucure: 
 * This data structure contains the LLDP-MED configuration information that
 * controls the selection of LLDP-MED TLVs transmission on particular port.
 * This node is included in tLldpLocPortInfo of LldpLocPortAgentInfoRBTree.
 */ 
typedef struct LldpMedLocCapInfo
{
     UINT2   u2MedLocCapTxSupported;    /* Bit map to represent supported LLDP-MED
                                           TLV in the system.*/ 
     UINT2   u2MedLocCapTxEnable;       /* Bit map to represent selected LLDP-MED
                                           TLV to be transmitted in the LLDP PDU.
                                           It contains the below values:
                                           LLDP_MED_CAPABILITY_TLV       0x0001
                                           LLDP_MED_NETWORK_POLICY_TLV   0x0002
                                           LLDP_MED_LOCATION_ID_TLV      0x0004
                                           LLDP_MED_PW_MDI_PSE_TLV       0x0008
                                           LLDP_MED_PW_MDI_PD_TLV        0x0010
                                           LLDP_MED_INVENTORY_MGMT_TLV   0x0020
                                         */
} tLldpMedLocCapInfo;


/* LLDP-MED Remote Capability Information:
 * This data structure contains the LLDP MED Capability information that is
 * advertised by the Remote end point device on a particular port. 
 * This Node is included in tLldpRemoteNode of RemMSAPRBTree.*/

typedef struct LldpMedRemCapInfo
{
    UINT2       u2MedRemCapSupported;  /* Capabilities supported by the remote device */  
    UINT2       u2MedRemCapEnable;     /* Capabilitues enabled by the remote device */
    UINT1       u1MedRemDeviceClass;   /* Type of the remote device */
    UINT1       au1Pad[3];
} tLldpMedRemCapInfo;

/*LLDP-MED Local Inventory Data structure:
 * This data structure contains the LLDP MED Inventory information about the 
 * physical device that is used for Inventory TLV advertisements. 
 * This Node is included in tLldpLocSysInfo of tLldpGlobalInfo.*/

typedef struct LldpMedLocInventoryInfo
{
    UINT1    au1MedLocHardwareRev[LLDP_MED_HW_REV_LEN+1];  /* Local System Hardware revision */
    UINT1    au1MedLocFirmwareRev[LLDP_MED_FW_REV_LEN+1];  /* Local System Firmware revision */
    UINT1    au1MedLocSoftwareRev[LLDP_MED_SW_REV_LEN+1];  /* Local System Software revision */
    UINT1    au1MedLocMfgName[LLDP_MED_MFG_NAME_LEN+1];    /* Local System Manufature Name */
    UINT1    au1MedLocModelName[LLDP_MED_MODEL_NAME_LEN+1];/* Local System Modela Name */   
    UINT1    au1MedLocSerialNum[LLDP_MED_SER_NUM_LEN];     /* Local System Serial Number */
    UINT1    au1MedLocAssetId[LLDP_MED_ASSET_ID_LEN];      /* Local System Asset ID */
} tLldpMedLocInventoryInfo;

/* LLDP-MED Remote Inventory Information:
 * This data structure contains the LLDP MED Inventory information about the 
 * remote end point device that is advertised by LLDP MED Inventory TLV. 
 * This Node is included in tLldpRemoteNode of RemMSAPRBTree.*/

typedef struct LldpMeRemInventoryInfo
{ 
    UINT1    au1MedRemHardwareRev[LLDP_MED_HW_REV_LEN+1];   /* Remote System Hardware revision */
    UINT1    au1MedRemFirmwareRev[LLDP_MED_FW_REV_LEN+1];   /* Remote System Firmware revision */ 
    UINT1    au1MedRemSoftwareRev[LLDP_MED_SW_REV_LEN+1];   /* Remote System Software revision */
    UINT1    au1MedRemMfgName[LLDP_MED_MFG_NAME_LEN+1];     /* Remote System Manufature Name */
    UINT1    au1MedRemModelName[LLDP_MED_MODEL_NAME_LEN+1]; /* Remote System Modela Name */
    UINT1    au1MedRemSerialNum[LLDP_MED_SER_NUM_LEN];      /* Remote System Serial Number */
    UINT1    au1MedRemAssetId[LLDP_MED_ASSET_ID_LEN];       /* Remote System Asset ID */
} tLldpMedRemInventoryInfo;

/* LLDP-MED Local Network policy data structure:
 * This Embedded RB Tree Node is used to maintain LLDP MED Network policy 
 * configuration information that is used for network policy TLV 
 * transmission on a particular port.*/

typedef struct LldpMedLocNwPolicyInfo
{
    tRBNodeEmbd     LocPolicyNode;     
    UINT4           u4IfIndex;            /* INDEX of RB Tree */     
    INT4            i4RowStatus;
    UINT2           u2LocPolicyVlanId;               
    UINT1           u1LocPolicyAppType;   /* INDEX of RB Tree */            
    UINT1           u1LocPolicyPriority;              
    UINT1           u1LocPolicyDscp;                           
    BOOL1           bLocPolicyUnKnown;              
    BOOL1           bLocPolicyTagged;                
    UINT1           u1LocPad;
} tLldpMedLocNwPolicyInfo;


/* LLDP-MED Remote Network Policy Info structure: 
 * This data structure contains the LLDP MED Inventory information about
 * the remote end point device that is advertised by LLDP MED Inventory TLV.
 * This Node is included in tLldpRemoteNode of RemMSAPRBTree.*/

typedef struct LldpMedRemNwPolicyInfo
{
     tRBNodeEmbd       RemPolicyNode;   
     UINT4             u4RemLastUpdateTime;   /* INDEX of RB Tree */
     UINT4             u4RemDestAddrTblIndex;    /* INDEX of RB Tree */
     INT4              i4RemLocalPortNum;     /* INDEX of RB Tree */
     INT4              i4RemIndex;            /* INDEX of RB Tree */
     UINT2             u2RemPolicyVlanId;            
     UINT1             u1RemPolicyAppType;       /* INDEX of RB Tree */ 
     UINT1             u1RemPolicyPriority;         
     UINT1             u1RemPolicyDscp;                   
     BOOL1             bRemPolicyUnKnown;          
     BOOL1             bRemPolicyTagged;              
     UINT1             u1Pad;
} tLldpMedRemNwPolicyInfo;

/* LLDP-MED Local Location Info strcuture:
 * This data structure contains the LLDP-MED Location information about
 * the local device that is advertised by LLDP-MED Location TLV. */
typedef struct LldpMedLocLocationInfo
{
    tRBNodeEmbd        LocLocationNode;   
    UINT4              u4IfIndex;            /* INDEX of RB Tree */ 
    INT4               i4RowStatus;
    UINT1              au1LocationID[LLDP_MED_MAX_LOC_LENGTH]; /* Local Device Location Information */   
    UINT1              u1LocationDataFormat;               /* Local Device Location Data Format Type - Index */
    UINT1              au1pad[3];
} tLldpMedLocLocationInfo;

/* LLDP-MED Remote Location Info strcuture:
 * This data structure contains the LLDP-MED Remote information about
 * the local device that is advertised by LLDP-MED Location TLV. */

typedef struct LldpMedRemLocationInfo
{
     tRBNodeEmbd       RemLocationNode;   
     UINT4             u4RemLastUpdateTime;   /* INDEX of RB Tree */
     UINT4             u4RemDestAddrTblIndex;    /* INDEX of RB Tree */
     INT4              i4RemLocalPortNum;     /* INDEX of RB Tree */
     INT4              i4RemIndex;            /* INDEX of RB Tree */
     UINT1             au1RemLocationID[LLDP_MED_MAX_LOC_LENGTH]; /* Remote Device Location Information */   
     UINT1             u1RemLocationSubType;               /* Remote Device Location Data Format Type-Index */
     UINT1             au1pad[3];
} tLldpMedRemLocationInfo;


/* LLDP-MED Local Power via MDI Info strcuture:
 * This data structure contains the LLDP-MED Power Via MDI information about
 * the local device that is advertised by LLDP-MED Power Via MDI TLV.
 * This node is included in tLldpLocPortTable of PortNode */
typedef struct LldpMedLocPSEPowerInfo
{
   UINT2               u2PsePowerAv;      /* Power availability on local PSE device */
   UINT1               u1PsePowerPriority;/* Local Device Power Priority */
   UINT1               u1pad;
}tLldpMedLocPSEPowerInfo;

/* LLDP-MED Remote Power via MDI Info strcuture:
 * This data structure contains the LLDP-MED Power Via MDI information about
 * the end point device that is advertised by LLDP-MED Power Via MDI TLV.
 * This Node is included in tLldpRemoteNode of RemMSAPRBTree.*/
typedef struct LldpMedRemPowerMDIInfo
{
   UINT2               u2PowerValue;      /* Power availability or Power requested by the End device */
   UINT1               u1PoEDeviceType;   /* Remote PoE device type */
   UINT1               u1PowerSource;     /* Remote Power Source  */ 
   UINT1               u1PowerPriority;   /* Remote Device Power Priority */
   UINT1               au1pad[3];
}tLldpMedRemPowerMDIInfo;

/* LLDP-MED Statistics Information :
 * This node contains the LLDP MED TLV related statistics for individual 
 * port and destination address combinations. This node is included in 
 * tLldpLocPortInfo of LldpLocPortAgentInfoRBTree. */

typedef struct LldpMedStatsInfo
{
    UINT4    u4TxMedFramesTotal;          
    UINT4    u4RxMedFramesTotal;           
    UINT4    u4RxMedFramesDiscardedTotal;
    UINT4    u4RxMedTLVsDiscardedTotal;
    UINT4    u4RxMedCapTLVsDiscarded;          
    UINT4    u4RxMedPolicyTLVsDiscarded;          
    UINT4    u4RxMedInventoryTLVsDiscarded; 
    UINT4    u4RxMedLocationTLVsDiscarded;
    UINT4    u4RxMedExPowerMDITLVsDiscarded;
} tLldpMedStatsInfo;

/*LLDP-MED Data structures -END*/

/* LLDP Port Configuration */
/* This structure contains port configuration parameters */
typedef struct LldpPortConfigTable
{
    INT4     i4AdminStatus;          /* LLDP Agent AdminStatus */
    UINT1    u1DstMac[MAC_ADDR_LEN]; /*Destination MAC address to be used for
                                       the LLDP agent on this port */
    UINT1    u1TLVsTxEnable;         /* Basic TLVs Enable Flag(bit map) */     
    UINT1    u1NotificationEnable;   /* Trap Notification Enable Flag */
    UINT1    u1FsConfigNotificationType; /* Trap Notofication to NMS*/
    UINT1    au1Pad[3];
} tLldpPortConfigTable;

/* LLDP Tx Statistics */
typedef struct LldpStatsTxPortTable
{
    UINT4    u4TxFramesTotal; /* Count of Total Frames transmitted */
    UINT4    u4TxTaggedFramesTotal;/* Count of Total tagged Frames transmitted */
    UINT4    u4LldpPDULengthErrors; /* Count of number of frames whose 
                                       length restriction is violated */
} tLldpStatsTxPortTable;

/* LLDP Rx Statistics */
typedef struct LldpStatsRxPortTable
{
    UINT4    u4FramesErrors;          /* Count of Total Frames Errors */
    UINT4    u4RxFramesTotal;         /* Count of Total Frames Received */
    UINT4    u4TLVsDiscardedTotal;    /* Count of Total TLVs Discarded */
    UINT4    u4TLVsUnrecognizedTotal; /* Count of Total Unrecognized TLVs */
    UINT4    u4AgeoutsTotal;          /* Count of Total Remote Table Entries 
                                         Aged Out */
    INT4     i4FramesDiscardedTotal;  /* Count of Total Frames Discarded */
} tLldpStatsRxPortTable;

/* LLDP Local System Data - Port Specific */
typedef struct LldpLocPortTable
{
   tRBNodeEmbd  PortNode; /*Node information for this port*/
   tLldpMedLocPSEPowerInfo MedPSEPowerInfo;
    INT4        i4IfIndex; /*Interface Index */
    INT4        i4MedAdminStatus;   /* LLDP-MED Admin status (Enable or Disable)*/
    INT4        i4LocPortIdSubtype;  /* Port Id Subtype */
    UINT4       u4LocVidUsageDigest; /* VidUsageDigest for this port */
    UINT4       u4LocMgmtVid;        /* Mgmt Vid for this port */
    UINT1       u1VidUsageDigestTlvTxEnabled;
    UINT1       u1ManVidTlvTxEnabled;
    UINT1       au1LocPortId[LLDP_MAX_LEN_PORTID + 1];    /* Port Id */
    UINT1       au1LocPortDesc[LLDP_MAX_LEN_PORTDESC + 1];/* Port Description */
    UINT1       au1DstMac[MAC_ADDR_LEN]; /*Destination MAC address */
    UINT2       au2SVlanIdArray[VLAN_EVB_MAX_SBP_PER_UAP]; /* Array of SVLAN
                                                            * when the port is 
                                                            * made UAP */
} tLldpLocPortTable;

/* ----- IEEE 802.3 Org Specific Info ----- */
/* Structure common to both Loc and Remote organizationally specific Info*/
typedef struct Lldpxdot3AutoNegInfo
{
    UINT2       u2OperMauType;
    UINT2       u2AutoNegAdvtCap;
    UINT1       u1AutoNegSupport;
    UINT1       u1AutoNegEnabled;
    UINT1       au1Pad[2];
}tLldpxdot3AutoNegInfo;


typedef struct Lldpxdot3PowerInfo
{

    UINT1       u1PowerInfo; /* bit 0 represents port class
                              * bit 1 represents support
                              * bit 2 represents status (enabled/ disabled)
                              * bit 3 represents control abitiliy
                              */
    UINT1       u1PowPairs;
    UINT1       u1PowClass;
    UINT1       au1Pad[1];
}tLldpxdot3PowerInfo;

/* Local Dot3 Oraganizationally specific info*/
typedef struct Lldpxdot3LocPortInfo
{
    tLldpxdot3AutoNegInfo Dot3AutoNegInfo;
    tLldpxdot3PowerInfo   Dot3PowerInfo;
    UINT4                 u4AggPortId;
    UINT2                 u2MaxFrameSize;
    UINT1                 u1TLVTxEnable;
    UINT1                 u1AggStatus;
}tLldpxdot3LocPortInfo;

typedef struct Lldpxdot1RemVlanNameInfo
{
    tRBNodeEmbd  RemVlanNameRBNode;
    UINT4      u4RemLastUpdateTime; /* INDEX: Last Update Time Stamp */
    INT4       i4RemLocalPortNum;   /* INDEX: Local Port Where this Remote 
                                       information is received */
    UINT4      u4DestAddrTblIndex;  /*INDEX : combined with IfIndex to denote the
                                      remote agent from where the information 
                                      is received */
    INT4       i4RemIndex;          /* INDEX: Remote Index */
    UINT1      au1VlanName[VLAN_STATIC_MAX_NAME_LEN];
    UINT2      u2VlanId;            /* INDEX: VLAN Id */
    UINT1      u1VlanNameLen;
    UINT1      au1Pad[1];
}tLldpxdot1RemVlanNameInfo;

/* ----- IEEE 802.1 Org Specific Info ----- */
/* Remote Dot1 organizationally specific Info*/
typedef struct Lldpxdot1RemProtoVlanInfo
{
    tRBNodeEmbd  RemProtoVlanRBNode;
    UINT4      u4RemLastUpdateTime;  /* INDEX: Last Update Time Stamp */
    INT4       i4RemLocalPortNum;    /* INDEX: Local Port Where this Remote 
                                        information is received */
    UINT4      u4DestAddrTblIndex;  /*INDEX : combined with IfIndex to denote the
                                      remote agent from where the information 
                                      is received */
    INT4       i4RemIndex;           /* INDEX: Remote Index */
    UINT2      u2ProtoVlanId;        /* INDEX: Protocol Vlan Id */
    UINT1      u1ProtoVlanSupported;
    UINT1      u1ProtoVlanEnabled;
}tLldpxdot1RemProtoVlanInfo;



typedef struct Lldpxdot1RemProtocolInfo
{
    tRBNodeEmbd  RemProtoIdRBNode;
    UINT4      u4RemLastUpdateTime;/* INDEX: Last Update Time Stamp */
    UINT4      u4ProtocolIndex;    /* INDEX: Protocol Index */
    INT4       i4RemLocalPortNum;  /* INDEX: Local Port Where this Remote 
                                      information is received */
    UINT4      u4DestAddrTblIndex;  /*INDEX : combined with IfIndex to denote the
                                      remote agent from where the information 
                                      is received */
    INT4       i4RemIndex;         /* INDEX: Remote Index */
    UINT1      au1ProtocolId [LLDP_MAX_LEN_PROTOID + 1];
}tLldpxdot1RemProtoIdInfo;


/* Remote Dot3 organizationally specific Info*/
typedef struct Lldpxdot3RemPortInfo
{
    tLldpxdot3AutoNegInfo RemDot3AutoNegInfo;
    tLldpxdot3PowerInfo   RemDot3PowerInfo;
    UINT4                 u4AggPortId;
    UINT2                 u2MaxFrameSize;
    UINT1                 u1AggStatus;
    UINT1                 au1Pad[1];
}tLldpxdot3RemPortInfo;


/* Remote Management Address Table */
typedef struct LldpRemManAddressTable
{
    tRBNodeEmbd     RemManAddrRBNode;
    tLldpManAddrOid RemManAddrOID;       /* Management Address OID */
    UINT4           u4RemLastUpdateTime; /* INDEX: Last Update Time
                                            Stamp */
    INT4            i4RemLocalPortNum;   /* INDEX: Local Port Where this Remote 
                                            information is received */
    UINT4      u4DestAddrTblIndex;  /*INDEX : combined with IfIndex to denote the
                                      remote agent from where the information 
                                      is received */
    INT4            i4RemIndex;          /* INDEX: Remote index.*/
    INT4            i4RemManAddrIfSubtype;
    INT4            i4RemManAddrIfId;
    INT4            i4RemManAddrSubtype; /* INDEX: Management addr subtype */
    UINT1           au1RemManAddr[LLDP_MAX_LEN_MAN_ADDR + 1];/* INDEX: Management 
                                                          Address */
}tLldpRemManAddressTable;


/*--------------------END: Local System MIB-------------------------------- */



/* ----- Per port basis structures ----- */


/* Remote Unknown TLV Table */
typedef struct LldpRemUnknownTLVTable
{
    tRBNodeEmbd     RemUnknownTLVRBNode; /* Next node ptr for the SLL */
    UINT4           u4RemLastUpdateTime; /* INDEX: Last Update Time Stamp */
    INT4            i4RemLocalPortNum;   /* INDEX: Local Port Where this Remote
                                            information is received */
    UINT4      u4DestAddrTblIndex;  /*INDEX : combined with IfIndex to denote the
                                      remote agent from where the information 
                                      is received */
    INT4            i4RemIndex;          /* INDEX: Remote index */
    INT4            i4RemUnknownTLVType; /* INDEX: TLV Type */
    UINT1           au1RemUnknownTLVInfo[LLDP_MAX_LEN_UNKNOWN_TLV_INFO];
    UINT2           u2RemUnknownTLVInfoLen;
    UINT1           au1Pad[2];
}tLldpRemUnknownTLVTable;


/* Remote Organizationalally Defined Info Table */
typedef struct LldpRemOrgDefInfoTable
{
    tRBNodeEmbd RemOrgDefInfoRBNode;
    UINT4      u4RemLastUpdateTime;    /* INDEX: Last Update Time Stamp */
    INT4       i4RemLocalPortNum;      /* INDEX: Local Port Where this Remote
                                          information is received */
    UINT4      u4DestAddrTblIndex;  /*INDEX : combined with IfIndex to denote the
                                      remote agent from where the information 
                                      is received */
    INT4       i4RemIndex;             /* INDEX: Remote index */
    INT4       i4RemOrgDefInfoSubtype; /* INDEX: Organizationally defined
                                          information subtype */
    INT4       i4RemOrgDefInfoIndex;   /* INDEX: Organizationlly defined
                                          information index */
    UINT1      au1RemOrgDefInfo[LLDP_MAX_LEN_ORGDEF_INFO]; /* Organizationally
                                                              defined info */
    UINT1      au1RemOrgDefInfoOUI[LLDP_MAX_LEN_OUI + 1]; /* INDEX: OUI value */
    UINT2      u2RemOrgDefInfoLen;
    UINT1      au1Pad[2];
}tLldpRemOrgDefInfoTable;



/*--------------------END: Remote System MIB-------------------------------- */

/* --------   Global Structures   ----------- */

/* ------------------------------------------------------------------
 *                      Remote Node
 * This Table contains All the information about a remote 
 * device (Neighbor)
 * ----------------------------------------------------------------- */
typedef struct LldpRemoteSystemData
{    /* Embeded RB Tree Node */
    tRBNodeEmbd   RemMSAPRBNode;   /* Useful for searching a node with MSAP id */
    tRBNodeEmbd   RemSysDataRBNode;/* Useful for SNMP Access routines */
    /* Timer Node for Ageout Timer */
    tLldpTmr     RxInfoAgeTmrNode;
    /* LLDP-MED Organizally defined TLVs*/
    tLldpMedRemCapInfo        RemMedCapableInfo; /* LLDP-MED remote
                                                    device port config
                                                    information */
    tLldpMedRemInventoryInfo  RemInventoryInfo;  /* LLDP-MED remote 
                                                    inventory information*/
    tLldpMedRemPowerMDIInfo   RemMedPowerMDIInfo; /* LLDP-MED Remote device 
                                                     Power MDI information*/

    tLldpxdot1RemVlanNameInfo *pDot1RemVlanNameInfo; /* Point to the first 
                                                        node of the remote 
                                                        vlan info for this
                                                        MSAP from the
                                                        RemVlanRbtree*/
    tLldpxdot1RemProtoVlanInfo *pDot1RemProtoVlanInfo;
    tLldpxdot1RemProtoIdInfo   *pDot1RemProtocolInfo;

    /* IEEE802.3 organizationally specific information */
    tLldpxdot3RemPortInfo      *pDot3RemPortInfo; 
                                                  
    /* Management Address TLVs */
    tLldpRemManAddressTable    *pRemManAddr; /* Points to the first Management 
                                               Address node related
                                               to this particular remote entry
                                               in the Global ManAddr RBTree*/
    tLldpRemUnknownTLVTable    *pRemUnknownTLV;/* Points to the first UnknownTlv
                                                 node related to this 
                                                 particular remote entry in the
                                                 global UnknownTLV RBTree*/
               
    /* Organizationally Defined Unrecognized TLVs */
    tLldpRemOrgDefInfoTable    *pRemOrgDefInfo;  /* Points to the first 
                                                    Unrecognized OrgSpecific
                                                    Information entry in the 
                                                    RemOrgDefInfoRBTree*/

    UINT4    u4RxTTL;     /* TTL Value of the received LLDPDU */
#ifdef L2RED_WANTED    
    /* store the expected expiry time of rxinfo age out timer in 
     * standby node */
    UINT4    u4RxInfoAgeTmrExpTime;
#endif    
    UINT4    u4RemLastUpdateTime; /* INDEX: Last Update Time Stamp */
    INT4     i4RemLocalPortNum;   /* INDEX: Local Port Where this Remote 
                                     information is received */
    UINT4     u4DestAddrTblIndex;  /*INDEX : combined with IfIndex to denote the
                                      remote agent from where the information 
                                      is received */
    INT4     i4RemIndex;          /* INDEX: Remote index */
    /* TLV Information base */
    INT4     i4RemChassisIdSubtype;  /* Remote ChassisId Subtype */
    INT4     i4RemPortIdSubtype;     /* Remote Port Id Subtype */
    UINT4    u4RemVidUsageDigest;
    UINT2    u2RemMgmtVid;
    /* IEEE802.1 organizationally specific information */
    /* 0-15 BITMAP entry represent the supported system capabilities */
    UINT1    au1RemSysCapSupported[ISS_SYS_CAPABILITIES_LEN];
    /* 0-15 BITMAP entry represent the supported system capabilities enabled */
    UINT1    au1RemSysCapEnabled[ISS_SYS_CAPABILITIES_LEN];
    UINT1    au1PduMsgDigest[16]; /* Msg Digest value of 
                                                          LLDPDU */

    UINT1    au1RemChassisId[LLDP_MAX_LEN_CHASSISID + 1];  /* Remote Chassis Id */
    UINT1    au1RemPortId[LLDP_MAX_LEN_PORTID + 1];        /* Remote Port Id */
    UINT1    au1RemPortDesc[LLDP_MAX_LEN_PORTDESC + 1];/* Remote Port Description*/
    UINT1    au1RemSysName[LLDP_MAX_LEN_SYSNAME + 1];  /* Remote System Name */
    UINT1    au1RemSysDesc[LLDP_MAX_LEN_SYSDESC + 1];  /* Remote System Description*/
    UINT2    u2PVId;                 /* Port Vlan Id */
    BOOL1    bRcvdPVidTlv;    /* Indicates PVid tlv is received */
    BOOL1    bRcvdMacPhyTlv;  /* Indicates Mac/Phy tlv is received */
    BOOL1    bRcvdLinkAggTlv; /* Indicates Link Agg tlv is received */
    BOOL1    bRcvdMaxFrameTlv;/* Indicates Max Frame tlv is received */
    BOOL1    bRcvdVidUsageDigestTlv;/* Indicates VidUsageDigest tlv is received */
    BOOL1    bRcvdMgmtVidTlv;/* Indicates Mgmt Vid tlv is received */
    BOOL1    bRemoteChanges;  /* Indicates if there are any remote changes */
    BOOL1    bTooManyNeighbors; /* Indicates if there are too many neighbors */
   
#define RxInfoAgeTmr RxInfoAgeTmrNode.LldpTmrBlk
}tLldpRemoteNode;

/* ------------------------------------------------------------------
 *                 Local Port Table
 * This Table contains All the information associated with a local port
 * ----------------------------------------------------------------- */
typedef struct LldpLocPortInfo
{
    /* Protocol VLAN */
    tTMO_DLL                 LocProtoVlanDllList; /* DLL list to maintain the 
                                                     protocol vlan ids mapped 
                                                     to this port. Points to 
                                                     structure 
                                                     tLldpxdot1LocProtoVlanInfo */
    tRBNodeEmbd              LocPortAgentRBNode;  /* Node pointing to 
                                                     For Destination MAC address entry */
   /* Tmr Node */
    tLldpTmr                 ShutWhileTmrNode;
    tLldpTmr                 TxDelayTmrNode;
    tLldpTmr                 TtrTmrNode;
    /* per agent */
    tLldpMedLocCapInfo       LocMedCapInfo;       /* LLDP Media related organizationally
                                                     specific port config Information */
    tLldpPortConfigTable     PortConfigTable;
    tLldpStatsTxPortTable    StatsTxPortTable;
    tLldpStatsRxPortTable    StatsRxPortTable;
    /* LLDP-MED Tx and Rx statistics info - per system */
    tLldpMedStatsInfo        MedStatsInfo;
    tLldpxdot3LocPortInfo    Dot3LocPortInfo;      /* IEEE802.3 Organizationally
                                                      specific info */
    tLldpRemoteNode          *pBackPtrRemoteNode;
    /* Back Pointer to neighbor info table. Useful for the timer 
     * implimentation .As at the time of timer expiration we will be sending
     * the tLldpLocPortInfo ptr to SEM, So we need to have a ptr to reach the
     * remote node for which the timer is expired*/
    /* Pre-formed Buffer for LLDPDU */
    UINT1                    *pPreFormedLldpdu;    /* if admin status=RxOnly that
                                                     time this is not needed */
    UINT4                    u4PreFormedLldpduLen; /* Length of PreFormedLldpdu(Linear Buffer) */
    UINT1                    *pu1RxLldpdu;
    UINT4                    u4LocPortNum;         /*INDEX OF THIS TREE */
    UINT4                    u4LocVidUsageDigest;    
    UINT4                    u4MsgInterval; 
    INT4                     i4IfIndex;             /* Interface Index on which the agent resides */
    UINT4                    u4DstMacAddrTblIndex;  /*Dest Mac Address Index */ 
    INT4                     i4TxSemCurrentState;   /* TX SEM current state */
    INT4                     i4RxSemCurrentState;   /* RX SEM current state */
    INT4                     i4TxTimerSemCurrentState; /* TX Timer SEM current state */
    INT4                     i4RxTtl;                  /* RX SEM related variable */
    UINT4                    u4FastTxSysUpTime;
    
    UINT1       au1LstRcvdSrcMacAddr[MAC_ADDR_LEN];/*Src Mac Address of the sender 
          obtained from last received PDU*/
    UINT2                    u2PVid;                  /* Port Vlan ID */ 
    UINT2                    u2TlvRcvdBmp;            /* Rx-SEM: Track the received TLV type*/
    UINT2                    u2RxPduLen;              /* Len of the rcvd pdu */
    UINT2                    u2NoOfNeighbors;         /* Current active neighbors */
    UINT2                    u2MedTlvRcvdBmp;         /* Track the received LLDP-MED TLVs */
    UINT2                    u2PolicyBmp;             /* Track the received LLDP-MED 
                                                         Network policy App Types */
    UINT1                    u1RefreshNotify;         /* Flag to indicate neighbor count
                                                       * reached 1 from multiple neighbors*/
    UINT1                    u1RxFrameType;           /* Received frame type: 
                                                         (New/Update/Refresh) Frame */
    UINT1                    u1OperStatus;            /* Port oper status */
    UINT1                    u1PortVlanTxEnable;      /* Determines if PVID TLV needs 
                                                       * to be transmitted on this
                                                       * port */
    UINT1                    u1ProtoVlanEnabled;      /* Determines if protocol based
                                                       * vlan classification is
                                                       * enabled on this port */
    /* This variable is set to LLDP_TRUE if all management addresses are
     * enabled for transmission on a port */
    UINT1                    u1ManAddrTlvTxEnabled;
    UINT1                    u1LinkAggTlvTxEnabled;
    UINT1                    u1Credit;                /* Value of txCredit variable for this agent */
    UINT1                    u1TxFast;                /* Value of txFast variable for this agent */
    INT1                     i1FastTxRateLimit;
    BOOL1                    bMaxPduSizeExceeded;
    BOOL1                    bSomethingChangedLocal;  /* If any local data 
                                                         changed */
    BOOL1                    bMsgIntvalJittered;
    BOOL1                    bIsAnyPendingApp;        /* Flag to indicate whether 
                                                         any application is newly 
                                                         registered on this port
                                                         so that event will be sent
                                                         to application when REFRESH
                                                         frame is received */
    BOOL1                    bTxNow;                  /* txNow for this variable*/
    BOOL1                    bNeighborDetected;       /* Neighbor detected flag */
    BOOL1                    bTxTick;                 /* TxTick flag */
    BOOL1                    bstatsFramesInerrorsTotal; /* Flag to increment
                                                           the counter statsFramesInerrorsTotal
                                                           ONLY ONCE, if there is one or more
                                                           detectable errors per frame */
    BOOL1                     bMedCapable;              /* Flag to indicate whether 
                                                           LLDP-MED is enabled for the 
                                                           particular agent*/
    BOOL1                     bMedFrameDiscarded;       /* Flag to increment LLDP-MED PDU
                                                           discarded counter */
    /* The bits of the following flag is used to verify the duplicate TLVs
     * present in an LLDPDU; Useful during parsing of REFRESH frames to 
     * increment the error counters */
    /*   BIT8   BIT7 BIT6 BIT5 BIT4         BIT3      BIT2      BIT1 
     * -------- ---- ---- ---- --------  ---------  --------  --------
     * Reserved Res. Res. Res. Mgmt VID   VID Usage   Mgmt.   VLAN Name
     *                            TLV    Digest TLV  Addr TLV    TLV     */

    UINT1                     au1DupTlvChkFlag[LLDP_MAX_DUP_TLV_ARRAY_SIZE];
    UINT1                     au1Pad[1];
#define ShutWhileTmr ShutWhileTmrNode.LldpTmrBlk
#define TxDelayTmr   TxDelayTmrNode.LldpTmrBlk
#define TtrTmr       TtrTmrNode.LldpTmrBlk
} tLldpLocPortInfo;

/* ------------------------------------------------------------------------ */
/*                   Remote System Mib Structures                           */
/* ------------------------------------------------------------------------ */

/* LLDP Remote system data Table Statistics - per system*/
typedef struct LldpStatsRemTabInfo
{
    UINT4   u4LastChgTime;  /* Remote Table Last Change Time */
    UINT4   u4Inserts;      /* Remote Table Inserts Count */
    UINT4   u4Deletes;      /* Remote Table Deletes Count */
    UINT4   u4Drops;        /* Remote Table Drops Count */
    UINT4   u4AgeOuts;      /* Remote Table EntryAgeOuts Count */
    /* proprietary mib object */
    UINT4   u4Updates;
} tLldpStatsRemTabInfo;
/*------------------------------------------------------------------------
 *           Local System MIB Structures
 *----------------------------------------------------------------------- */

/* LLDP System Configuration */
/* This structure contains system configuration parameters */
typedef struct LldpSysConfigInfo
{
    INT4    i4MsgTxInterval;        /* Message Transmit Interval */
    INT4    i4MsgTxHoldMultiplier;  /* Message hold time multiplier */
    INT4    i4ReInitDelay;          /* Port/LLDP Agent Reinit delay */
    INT4    i4TxDelay;              /* Transmit delay */
    INT4    i4NotificationInterval; /* Trap Notification Interval */
    UINT4   u4TxCreditMax; /* CreditMax variable indicating number of 
                              consecutive LLDPDUs that can be transmitted
                              at a time */
   UINT4    u4MessageFastTx; /* No.of seconds at which LLDP frames
                                are transmitted on behalf of this
                                LLDP agent during fast transmission period */
   UINT4    u4TxFastInit; /* The initial value used to initialize the txFast
                             variable which determines the number of 
                             transmissions that are made in fast transmission mode.*/
   UINT4    u4MedFastStart; 
   
} tLldpSysConfigInfo;
/* LLDP Local System Data - System Specific */
typedef struct LldpLocSysInfo
{
    tLldpMedLocInventoryInfo     LocInventoryInfo;            /* LLDP-MED Inventory information */
    UINT1    au1LocSysCapSupported[ISS_SYS_CAPABILITIES_LEN]; /* System Capabilities supported (bitmap) */
    UINT1    au1LocSysCapEnabled[ISS_SYS_CAPABILITIES_LEN];   /* System Capabilities enabled (bitmap) */
    UINT1    au1LocChassisId[LLDP_MAX_LEN_CHASSISID + 1];     /* Chassis Id */
    UINT1    au1LocSysName[LLDP_MAX_LEN_SYSNAME + 1];         /* System Name */
    UINT1    au1LocSysDesc[LLDP_MAX_LEN_SYSDESC + 1];         /* System Description */
    INT4     i4LocChassisIdSubtype;                           /* Chassis Id Subtype */
    UINT1    u1MedLocDeviceClass;                             /* LLDP-MED Local Device class type */
    UINT1    u1MedLocPSEPowerSource;                          /* LLDP-MED Local Device power source type*/
    UINT1    u1MedLocPoEDeviceType;                           /* LLDP-MED Local PoE Device type */
    UINT1    u1Pad;
} tLldpLocSysInfo;

/* ------------------------------------------------------------------
 *                Global Information 
 * This structure contains all the Global Data required for LLDP 
 * Operation .
 * ----------------------------------------------------------------- */
typedef struct LldpGlobalInfo
{
    /* Task Ids */
    tOsixTaskId     TaskId;
    /* MsgQ Id */
    tOsixQId        MsgQId;
    tOsixQId        RxPduQId;
    tOsixSemId      SemId;
    /* Global Timer Node */
    
    /* Before adding new entry to the remote table, 
     * Remote Table entry count is retreived, and if the count is equal to
     * the maximum allowed neighbor entries (i.e, LLDP_MAX_NEIGHBORS), 
     * further addition to the remote table is not allowed. Thus the 
     * TooManyNeighbor situation is handled without using TooManyNeighborTmr. */
    
    tLldpTmr    NotifIntervalTmrNode; /* Timer which controls the transmission
                                       * of LLDP notifications. If this timer 
                                       * is running then the notifications will
                                       * not be send*/
    tLldpTmr    GlobalShutWhileTmrNode; /* This timer does the same
                                         * operation of ShutWhileTmr in
                                         * tLldpLocPortInfo structure.
                                         * This timer is used while shutting
                                         * LLDP or making the module down
                                         * instead of starting timer for all
                                         * the ports. This reduces the no. of
                                         * timers running when a large no.
                                         * ports is operationally up. */
    /* MemPool Ids */
    /*----------------------------------*/
    tMemPoolId      LocPortInfoPoolId;  /* For structure tLldpLocPortInfo - per agent*/
    tMemPoolId      LocPortTablePoolId;  /* For structure tLldpLocPortTable - per Port*/
    tMemPoolId      LocManAddrPoolId;   /* For structure tLldpLocManAddrTable */ 
    tMemPoolId      RemNodePoolId;      /* For structure tLldpRemoteNode*/
    tMemPoolId      RemManAddrPoolId;   /*For structure tLldpRemManAddressTable*/
    tMemPoolId      RemUnknownTLVPoolId;    /*For structure 
                                              tLldpRemUnknownTLVTable*/
    tMemPoolId      RemOrgDefInfoPoolId;    /* For structure 
                                               tLldpRemOrgDefInfoTable */
    tMemPoolId      LocProtoIdInfoPoolId;   /* For structure 
                                               tLldpxdot1LocProtoIdInfo */
    tMemPoolId      LocProtoVlanInfoPoolId; /* For structure
                                             * tLldpxdot1LocProtoVlanInfo*/
    tMemPoolId      RemVlanNameInfoPoolId;  /* For structure 
                                               tLldpxdot1RemVlanNameInfo */
    tMemPoolId      RemProtoVlanPoolId;     /* For structure 
                                               tLldpxdot1RemProtoVlanInfo */
    tMemPoolId      RemProtocolPoolId;      /* For structure 
                                               tLldpxdot1RemProtoIdInfo */
    tMemPoolId      RemDot3PortInfoPoolId;  /* For structure 
                                               tLldpxdot3RemPortInfo */ 
    tMemPoolId      QMsgPoolId;             /* mempool id for tLldpQMsg */
    tMemPoolId      RxPduQPoolId;           /* mempool id for packet recevied */
    tMemPoolId      AppTblPoolId;          /* mempool id for application
                                               table */
    tMemPoolId      AppTlvPoolId;           /* mempool id for application 
                                               TLV */
    tMemPoolId      LldpCliSizingNeighPoolId; /*Mempool id for Neigh Info*/
    tMemPoolId      LldpArrayInfoPoolId;    /*Mempool id for Array Info*/
    tMemPoolId      LldpManAddrOidPoolId;   /*Mempool id for Man Addr OID*/
    tMemPoolId      LldpDestMacAddrPoolId;  /* For DestMacAddrTbl */
    tMemPoolId      LldpAgentToLocPortMappingPoolId; /*For AgentToLocPortMappingTbl */
    tMemPoolId      LldpVlanNameTlvCounter; /*Mempool id for tLldpVlanNameTlvCounter*/
    tMemPoolId      LldpMedLocNwPolicyPoolId;  /* Mempool id for tLldpMedLocNwPolicyInfo*/
    tMemPoolId      LldpMedRemNwPolicyPoolId;  /* Mempool id for tLldpMedRemNwPolicyInfo*/
    tMemPoolId      LldpMedLocLocationPoolId;  /* Mempool id for tLldpMedLocLocationInfo*/
    tMemPoolId      LldpMedRemLocationPoolId;  /* Mempool id for tLldpMedRemLocationInfo*/
    tMemPoolId      LldpPduInfoPoolId; /* Mempool id for PduInfo */
    /* Timers*/
    tTmrDesc        aTmrDesc[LLDP_MAX_TIMER_TYPES];
                          /* Timer data struct that contains
                             func ptrs for timer handling and
                             offsets to identify the data
                             struct containing timer block.
                             Timer ID is the index to this
                             data structure */
    /* LLDP Timer List ID */
    tTimerListId    TmrListId;

    /* Global RBTree Headers */ 
    /*----------------------------------*/
    tRBTree         LocProtoIdInfoRBTree;/* RBTree Head for 
                                            tLldpxdot1LocProtoIdInfo */
    /* Local Management address Table RBTree Head */
    tRBTree         LocManAddrRBTree; /* RB Root for 
                                         tLldpLocManAddrTable */
    tRBTree         RemMSAPRBTree;    /* Root for Neighbor Info Table 
                                         tLldpRemoteNode */
    tRBTree         RemSysDataRBTree; /* Root for Remote 
                                         Remote system data table usefull 
                                         for GetBulk Request 
                                         tLldpRemoteNode*/
    tRBTree         RemManAddrRBTree; /* RB Root for Rem Managemnet Table
                                         tLldpRemManAddressTable*/
    tRBTree         RemUnknownTLVRBTree; /* RBTree Head for 
                                            tLldpRemUnknownTLVTable */
    /* Organizationally Defined Unrecognized TLVs */
    tRBTree         RemOrgDefInfoRBTree;    /* RBTree Head for
                                               tLldpRemOrgDefInfoTable */
    tRBTree         RemVlanNameInfoRBTree;  /* Root for VlanName table   
                                               tLldpxdot1RemVlanNameInfo */
    tRBTree         RemProtoVlanRBTree;     /* Root for Protocol Vlan Table
                                               tLldpxdot1RemProtoVlanInfo */
    tRBTree         RemProtoIdRBTree;       /* Root for Protocol Identity Table
                                               tLldpxdot1RemProtoIdInfo */

    /* Application table - Contains the applications registered
     *                     on each port */
    tRBTree         AppRBTree; /* Root for application table
                                    tLldpAppInfo */

    /* Local system info(Local system data - per system) */
    tLldpLocSysInfo         LocSysInfo;
    /* Local system configuration info */
    tLldpSysConfigInfo      SysConfigInfo;
    /* Remote table statistics info - per system */
    tLldpStatsRemTabInfo    StatsRemTabInfo;
    /* This tree contain Per port information */
    tRBTree              LldpPortInfoRBTree;
    /* This tree contains per port per agent basis local information */
    tRBTree               LldpLocPortAgentInfoRBTree;
    
    /* This tree is used to hold information about destination 
     * MAC address table */
    tRBTree               Lldpv2DestMacAddrTblRBTree;
    
    /* This table is used to hold the ifIndex, destination MAC to 
     * Local Port Index table */
    tRBTree               Lldpv2AgentToLocPortMapTblRBTree;
    /* This table is used to get the local port number using 
     * If Index and destination Mac Index as input */
    tRBTree               Lldpv2AgentMapTblRBTree;

    /* This tree per port basis Local policy Information */
    tRBTree               LldpMedLocNwPolicyInfoRBTree;

    /* This tree contains Remote Policy Information */
    tRBTree               LldpMedRemNwPolicyInfoRBTree;

    /* This tree per port basis Local Location Information */
    tRBTree               LldpMedLocLocationInfoRBTree;

    /* This tree contains Remote Location Information */
    tRBTree               LldpMedRemLocationInfoRBTree;


    /* Management Address to be included when the TLV mode is set as 
     * CONFIGURED  */
    tIPvXAddr  MgmtAddr;
    /* IPv6 Management Address to be included when the TLV mode is set as 
     * CONFIGURED  */
    tIPvXAddr  MgmtAddr6;

/* prepreitary mib object */
    UINT4           u4TraceOption; /* For Trace Options */
#ifndef FM_WANTED
    /* Module Id registered with Syslog */
    UINT4           u4SysLogId;
#endif
#ifdef L2RED_WANTED
    /* Linux system time at which ACTIVE node boots up
     * for the first time is stored in this variable. Value of this
     * variable is retained even if LLDP module is shutdown/disabled */
    UINT4           u4FirstActiveUpTime;
    /* This variable stores the time interval with which the 
     * notification timer is started in active node */
    UINT4           u4NotifTmrStartVal;
#endif
    INT4           i4Version; /* LLDP Version Supported */
    /* Error Counter */
    INT4            i4MemAllocFailure;  /* Total memory allocation failure */
    INT4            i4InputQOverFlows;  /* Total Queue Overflow 
                                           (AppMsgQ + RxPduQ) */
    /* Next Free Indices- usefull for remote tables */
    INT4            i4NextFreeRemIndex;
    INT4            i4NextFreeRemProtocolIndex;
    INT4            i4NextFreeRemOrgDeInfoIndex;
    INT4            i4TagStatus; /* Tagging Status of LLDP System */
    /* prepreitary mib objects */
    UINT1           au1TraceInput[LLDP_TRC_MAX_SIZE];
    UINT1           u1LldpSystemControl;
    UINT1           u1ModuleStatus;
    /* Flags */
    UINT1           u1ProtoVlanSupported; /* Determines whether support for 
                                             protocol based vlan 
                                             classification is there in the 
                                             device. */
    BOOL1           bNotificationEnable;  /* Indicates whether the notification
                                             is enabled or disabled */
    BOOL1           bSendRemTblNotif;     /* Indicates whether the remote table chg
                                           * notification need to be sent or not 
                                           */
    BOOL1           bSendTopChgNotif;     /* Indicates whether the Topology 
                                           * Change Notification need to be sent 
                                           * or not */
     /* HITLESS RESTART */
    /* To check whether steady state request received from RM*/
    UINT1           u1StdyStReqRcvd;
    BOOL1           bMedClearStats;        /* LLD-MED Clear counter */
    BOOL1           bClearStats;           /* LLDP Clear counter */
    UINT1           au1Pad[3];
#define NotifIntervalTmr   NotifIntervalTmrNode.LldpTmrBlk
#define GlobalShutWhileTmr GlobalShutWhileTmrNode.LldpTmrBlk
}tLldpGlobalInfo;


PUBLIC tLldpGlobalInfo gLldpGlobalInfo;
#define  LLDP_GET_LOC_PORT_INFO(u4PortNum) LldpGetLocPortInfo (u4PortNum)
typedef struct Lldpv2DestAddrTbl
{
   tRBNodeEmbd DestMacAddrTblNode;
   UINT4       u4LlldpV2DestAddrTblIndex;
   tMacAddr    Lldpv2DestMacAddress;
   UINT1       u1RowStatus;
   UINT1       au1Pad[1];
}tLldpv2DestAddrTbl;

typedef struct Lldpv2AgentToLocPort
{
   tRBNodeEmbd  LldpAgentToLocPortNode; /* RBTree node */
   tRBNodeEmbd  LldpAgentToLocPortIndexNode; /* RBTree node for searching 
                                               entry based on If index and Mac Index */
   INT4         i4IfIndex; /* INDEX: Interface Index */
   UINT4        u4LldpLocPort; /* LLDP Local Port number */
   UINT4        u4DstMacAddrTblIndex; /* Index for LldpAgentToLocPortIndexNode */
   tMacAddr     Lldpv2DestMacAddress; /*INDEX: Destination MAC address */
   UINT1        u1RowStatus;
   UINT1        au1Pad[1];
}tLldpv2AgentToLocPort;

/* LLDP Chassis Id Sub Type */
enum
{
    LLDP_CHASS_ID_SUB_CHASSIS_COMP = 1, /* Chassis component*/
    LLDP_CHASS_ID_SUB_IF_ALIAS     = 2, /* Interface alias  */
    LLDP_CHASS_ID_SUB_PORT_COMP    = 3, /* Port component   */
    LLDP_CHASS_ID_SUB_MAC_ADDR     = 4, /* MAC address      */
    LLDP_CHASS_ID_SUB_NW_ADDR      = 5, /* Network address  */
    LLDP_CHASS_ID_SUB_IF_NAME      = 6, /* Interface name   */
    LLDP_CHASS_ID_SUB_LOCAL        = 7,  /* Locally assigned */
    LLDP_MAX_CHASS_ID              = 8  /* Max Chassis Id*/
};

/* LLDP Port Id Sub Type */
enum
{
    LLDP_PORT_ID_SUB_IF_ALIAS       = 1, /* Interface alias  */
    LLDP_PORT_ID_SUB_PORT_COMP      = 2, /* Port component   */
    LLDP_PORT_ID_SUB_MAC_ADDR       = 3, /* MAC address      */
    LLDP_PORT_ID_SUB_NW_ADDR        = 4, /* Network address  */
    LLDP_PORT_ID_SUB_IF_NAME        = 5, /* Interface name   */
    LLDP_PORT_ID_SUB_AGENT_CKT_ID   = 6, /* Agent circuit ID */
    LLDP_PORT_ID_SUB_LOCAL          = 7, /* Locally assigned */
    LLDP_MAX_PORT_ID                = 8  /* Max Port Id */
};

/* LLDP Notification Type*/
enum
{
    LLDP_REMOTE_CHG_NOTIFICATION           = 1,
    LLDP_MIS_CFG_NOTIFICATION              = 2,
    LLDP_REMOTE_CHG_MIS_CFG_NOTIFICATION   = 3,
    LLDP_MAX_NOTIFY_TYPE                   = 4

};

/* LLDP DOT1 ORG SPEC TLV SubTypes */
enum
{
    LLDP_PORT_VLAN_ID_TLV       = 1,
    LLDP_PROTO_VLAN_ID_TLV      = 2,
    LLDP_VLAN_NAME_TLV          = 3,
    LLDP_PROTO_ID_TLV           = 4,
    LLDP_VID_USAGE_DIGEST_TLV   = 5,
    LLDP_MGMT_VID_TLV           = 6,
 LLDP_V2_LINK_AGG_TLV        = 7
};

enum
{
   LLDP_MED_NOT_DEF_DEVICE = 0,    /* Device class is not defined */
   LLDP_MED_CLASS_1_DEVICE,        /* End point Class-1 Device */
   LLDP_MED_CLASS_2_DEVICE,        /* End point Class-2 Device */
   LLDP_MED_CLASS_3_DEVICE,        /* End point Class-3 Device */
   LLDP_MED_NW_CONNECTIVITY_DEVICE /* Network Connectivity Device */
};


/* Trace type  definitions */
#define   LLDP_INVALID_TRC        0x00000000
#define   LLDP_MED_INV_MGMT_TRC   0x00000100
#define   LLDP_VID_DIGEST_TRC     0x00000200
#define   LLDP_MGMT_VID_TRC       0x00000400
#define   LLDP_MED_CAPAB_TRC      0x00000800
#define   LLDP_MED_NW_POL_TRC     0x00001000
#define   LLDP_CRITICAL_TRC       0x00002000
#define   LLDP_RED_TRC            0x00004000
#define   LLDP_MANDATORY_TLV_TRC  0x00008000

#define LLDP_MED_LOC_ID_TRC       0x00010000
#define LLDP_MED_EX_POW_TRC       0x00020000
#define LLDP_PORT_DESC_TRC        0x00040000
#define LLDP_SYS_NAME_TRC         0x00080000
#define LLDP_SYS_DESC_TRC         0x00100000
#define LLDP_SYS_CAPAB_TRC        0x00200000
#define LLDP_MAN_ADDR_TRC         0x00400000
#define LLDP_PORT_VLAN_TRC        0x00800000

#define LLDP_PPVLAN_TRC           0x01000000
#define LLDP_VLAN_NAME_TRC        0x02000000
#define LLDP_PROTO_ID_TRC         0x04000000
#define LLDP_MAC_PHY_TRC          0x08000000
#define LLDP_PWR_MDI_TRC          0x10000000
#define LLDP_LAGG_TRC             0x20000000
#define LLDP_MAX_FRAME_TRC        0x40000000
#define LLDP_MAX_TRC              0x7fffffff

#define LLDP_VLAN_EVB_CDCP_TLV_TYPE  127
#define LLDP_VLAN_EVB_CDCP_TLV_SUBTYPE 0x0e

#define LLDP_ALL_TLV_TRC      (LLDP_MANDATORY_TLV_TRC |\
                               LLDP_MED_LOC_ID_TRC   |\
                               LLDP_MED_EX_POW_TRC   |\
                               LLDP_PORT_DESC_TRC    |\
                               LLDP_SYS_NAME_TRC     |\
                               LLDP_SYS_DESC_TRC     |\
                               LLDP_SYS_CAPAB_TRC    |\
                               LLDP_MAN_ADDR_TRC     |\
                               LLDP_PORT_VLAN_TRC    |\
                               LLDP_PPVLAN_TRC       |\
                               LLDP_VLAN_NAME_TRC    |\
                               LLDP_PROTO_ID_TRC     |\
                               LLDP_MAC_PHY_TRC      |\
                               LLDP_PWR_MDI_TRC      |\
                               LLDP_LAGG_TRC         |\
                               LLDP_MAX_FRAME_TRC    |\
                               LLDP_MED_CAPAB_TRC    |\
                               LLDP_MED_NW_POL_TRC   |\
                               LLDP_MED_INV_MGMT_TRC)

#define LLDP_ALL_TRC          (INIT_SHUT_TRC        |\
                               MGMT_TRC             |\
                               DATA_PATH_TRC        |\
                               CONTROL_PLANE_TRC    |\
                               DUMP_TRC             |\
                               OS_RESOURCE_TRC      |\
                               ALL_FAILURE_TRC      |\
                               BUFFER_TRC           |\
                               LLDP_CRITICAL_TRC    |\
                               LLDP_ALL_TLV_TRC     |\
                               LLDP_RED_TRC)



/*--------------------------------------------------------------------------*/
/*                       lldmain.c                                          */
/*--------------------------------------------------------------------------*/
PUBLIC VOID LldpMainTask           PROTO ((INT1 *pArg));

/*--------------------------------------------------------------------------*/
/*                       lldvlndb.c                                         */
/*--------------------------------------------------------------------------*/
PUBLIC INT4 LldpVlndbNotifyVlanInfoChg PROTO ((tPortList PortList));
PUBLIC INT4 LldpVlndbNotifyVlnInfoChgForPort PROTO ((UINT4 u4IfIndex));

/*--------------------------------------------------------------------------*/
/*                       lldapi.c                                          */
/*--------------------------------------------------------------------------*/
PUBLIC INT4 LldpApiNotifyIfAlias PROTO ((UINT4 u4IfIndex, UINT1 *pu1IfAlias));
PUBLIC INT4 LldpApiNotifyAgentCircuitId PROTO ((UINT4 u4IfIndex, 
                                                UINT4 u4AgentCircuitId));
PUBLIC INT4 LldpApiNotifyIfCreate PROTO ((UINT4 u4IfIndex));
PUBLIC INT4 LldpApiNotifyIfDelete PROTO ((UINT4 u4IfIndex));
PUBLIC INT4 LldpApiNotifyIfMap    PROTO ((UINT4 u4IfIndex));
PUBLIC INT4 LldpApiNotifyIfUnMap  PROTO ((UINT4 u4IfIndex));
PUBLIC INT4 LldpApiNotifyIfOperStatusChg PROTO ((UINT4 u4IfIndex, 
                                                 UINT1 u1OperStatus));
PUBLIC INT4 LldpApiNotifyPortVlanId PROTO ((UINT4 u4IfIndex, UINT2 u2VlanId));
PUBLIC INT4 LldpApiNotifyProtoVlanStatus PROTO ((UINT4 u4IfIndex, 
                                              UINT1 u1ProtVlanStatus));
PUBLIC INT4 LldpApiNotifyProtoVlanId PROTO ((UINT4 u4IfIndex,
                                             UINT2 u2VlanId, 
                                             UINT1 u1ActionFlag,
                                             UINT2 u2OldVlanId));
PUBLIC INT4 LldpApiNotifyPortAggCapability PROTO ((UINT4 u4IfIndex, 
                                                   UINT1 u1AggCap));
PUBLIC INT4 LldpApiNotifyAggStatus PROTO ((UINT4 u4IfIndex, UINT1 u1AggStatus));
PUBLIC INT4 LldpApiIsLldpCtrlFrame PROTO ((tCRU_BUF_CHAIN_DESC *pBuf,
                                           UINT4 u4IfIndex));
PUBLIC INT4 LldpApiEnqIncomingFrame PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf, 
                                               UINT4 u4IfIndex));
PUBLIC INT4 LldpApiNotifyResetAggCapability PROTO ((tPortList AggConfPorts));
PUBLIC INT4 LldpApiNotifySysName            PROTO ((UINT1 *pu1SysName));
PUBLIC INT4 LldpApiNotifySysDesc            PROTO ((UINT1 *pu1SysDesc));
PUBLIC INT4 LldpApiNotifyPortDesc          PROTO ((UINT1 *pu1PortDesc, UINT4 u4IfIndex));

INT4 LldpIsLldpStarted PROTO ((VOID));

/* Callback for IPv4 Module */
PUBLIC VOID LldpApiNotifyIpv4IfStatusChange PROTO ((tNetIpv4IfInfo 
                                                    *pNetIpIfInfo, 
                                                    UINT4 u4Mask));
/* Callback for IPv6 Module */
PUBLIC VOID LldpApiNotifyIpv6IfStatusChange PROTO ((tNetIpv6HliParams 
                                                    *pNetIpv6HlParams));
PUBLIC INT4 LldpApiModuleStart PROTO ((VOID));
PUBLIC VOID LldpApiModuleShutDown PROTO ((VOID));
PUBLIC VOID LldpApiResetAgentVariables PROTO((VOID));

INT4
LldpApiUpdateDigestInput PROTO((INT4 i4IfIndex,  tMacAddr *pAgentMacAddr,
                          UINT4 u4CRC32,  UINT1 *pu1VidDigestInput));

#ifdef L2RED_WANTED
PUBLIC INT4 LldpRedRcvPktFromRm PROTO ((UINT1 u1Event, tRmMsg * pData,
                                        UINT2 u2DataLen));
#endif

/* APIs provided by LLDP to L2IWF - These APIs are to be used only by L2IWF */
PUBLIC INT4
LldpApiHandleApplPortRequest PROTO ((UINT4 u4IfIndex,
                                     tLldpAppPortMsg *pLldpAppPortMsg,
                                     UINT1 u1ApplPortRequest));


PUBLIC INT4 LldpLock                     PROTO ((VOID));
PUBLIC INT4 LldpUnLock                   PROTO ((VOID));
PUBLIC tLldpLocPortInfo* LldpGetLocPortInfo PROTO ((UINT4 u4LocPortNum));
PUBLIC INT4 LldpApiGetInstanceId PROTO ((UINT1* pu1Mac, UINT4 *pu4InstanceId));
PUBLIC INT4
LldpApiSetDstMac PROTO ((INT4 i4IfIndex, UINT1 *pu1MacAddr,
                  UINT1 u1RowStatus));
INT4
LLdpApiUpdateLLdpStatusOnIccl (UINT4 u4IfIndex, UINT1 u1AggCap);
VOID
LldpUpdateMtu (UINT4 u4IfIndex, UINT4 u4Mtu);

VOID
LldpUpdateNegStatus (UINT4 u4IfIndex);

INT4 LldpApiUpdateSvidOnUap (UINT4, UINT4, UINT4);

PUBLIC BOOL1
LldpApiIsMultipleAgentsOnInterface PROTO ((INT4 i4IfIndex));


#endif /* _LLDP_H_ */
