/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: evcpro.h,v 1.4 2007/12/19 09:50:34 iss Exp $
 *
 * Description: This file contains all global variables used by
 *              RSTP and MSTP modules. 
 *
 *******************************************************************/


#ifndef _EVCPRO_H_
#define _EVCPRO_H_

#define EVCPRO_QUEUE_DEPTH  (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG) * 2
#define EVCPRO_CFG_Q_DEPTH  (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG) * 2

#define EVCPRO_MOD_NAME                  "EVCPRO"
#define EVCPRO_TASK_NAME                 "EvcT"
#define EVCPRO_TASK_PRIORITY             99
#define EVCPRO_QUEUE_NAME                ((UINT1*)"EvcQ")
#define EVCPRO_CFG_QUEUE                 ((UINT1*)"EvcC")

#define EVCPRO_MAX_EVC_IDENTIFIER_LENGTH       100
#define EVCPRO_FAILURE                   SNMP_FAILURE
#define EVCPRO_SUCCESS                   SNMP_SUCCESS

#define EVCPRO_LOCK()     EvcProLock()
#define EVCPRO_UNLOCK()   EvcProUnLock()

#define EVCPRO_MAX_EVC_PER_PORT           SYS_DEF_MAX_EVC_PER_PORT
#define EVCPRO_MAX_NUM_EVC_SUPPORTED      SYS_MAX_NUM_EVCS_SUPPORTED

VOID    EvcProTaskMain(INT1 *pi1Param);
#endif
