/*************************************************************************
 * Copyright (C) 2007-2015 Aricent Group . All Rights Reserved
 *
 * $Id: fsbnpwr.h,v 1.4 2017/10/09 13:13:59 siva Exp $
 *
 * Description: This file contains the necessary data strucutures for
 *              NPAPI layer
 *
 * *************************************************************************/

/* OPER ID */

#ifndef _FSBNPWR_H
#define _FSBNPWR_H

#include "fsb.h"

enum {
FS_MI_NP_FSB_HW_INIT = 1,
FS_FSB_HW_CREATE_FILTER,
FS_FSB_HW_DELETE_FILTER,
FS_MI_NP_FSB_HW_DE_INIT,
FS_FSB_HW_CREATE_S_CHANNEL_FILTER,
FS_FSB_HW_DELETE_S_CHANNEL_FILTER,
#ifdef MBSM_WANTED
FS_MI_NP_FSB_MBSM_HW_INIT,
FS_FSB_MBSM_HW_CREATE_FILTER,
#endif  /* MBSM_WANTED */
FS_FSB_HW_SET_FCOE_PARAMS,
FS_NP_FSB_MAX_NP
};

/* Required arguments list for the fsb NP wrappers
 * presented as union of structures for function
 * handling each OPER ID */

/* NOTE :
 * First and second members of the structures
 * are always ports and port list params
 * In other words,
 * Other function arguments should follow the
 * ports & port list params */

typedef struct {
    tFsbHwFilterEntry *   pFsbHwFilterEntry;
    tFsbLocRemFilterId *  pFsbLocRemFilterId;
} tFsbNpWrFsMiNpFsbHwInit;

typedef struct {
    tFsbHwFilterEntry *  pFsbHwFilterEntry;
    UINT4 *              pu4FilterId;
} tFsbNpWrFsFsbHwCreateFilter;

typedef struct {
    tFsbHwFilterEntry *  pFsbHwFilterEntry;
    UINT4                u4FilterId;
} tFsbNpWrFsFsbHwDeleteFilter;

typedef struct {
    tFsbHwFilterEntry *  pFsbHwFilterEntry;
    tFsbLocRemFilterId   FsbLocRemFilterId;
} tFsbNpWrFsMiNpFsbHwDeInit;

typedef struct {
    tFsbHwSChannelFilterEntry *  pFsbHwSChannelFilterEntry;
    UINT4 *                      pu4FilterId;
} tFsbNpWrFsFsbHwCreateSChannelFilter;

typedef struct {
    tFsbHwSChannelFilterEntry *  pFsbHwSChannelFilterEntry;
    UINT4                        u4FilterId;
} tFsbNpWrFsFsbHwDeleteSChannelFilter;

typedef struct {
    tFsbHwVlanEntry *pFsbHwVlanEntry;
} tFsbNpWrFsFsbHwSetFCoEParams;
#ifdef MBSM_WANTED
typedef struct {
    tFsbHwFilterEntry *   pFsbHwFilterEntry;
    tFsbLocRemFilterId *  pFsbLocRemFilterId;
} tFsbNpWrFsMiNpFsbMbsmHwInit;

typedef struct {
    tFsbHwFilterEntry *  pFsbHwFilterEntry;
    UINT4 *              pu4FilterId;
} tFsbNpWrFsFsbMbsmHwCreateFilter;
#endif  /* MBSM_WANTED */
typedef struct FsbNpModInfo {
union {
    tFsbNpWrFsMiNpFsbHwInit  sFsMiNpFsbHwInit;
    tFsbNpWrFsFsbHwCreateFilter  sFsFsbHwCreateFilter;
    tFsbNpWrFsFsbHwDeleteFilter  sFsFsbHwDeleteFilter;
    tFsbNpWrFsMiNpFsbHwDeInit  sFsMiNpFsbHwDeInit;
    tFsbNpWrFsFsbHwCreateSChannelFilter  sFsFsbHwCreateSChannelFilter;
    tFsbNpWrFsFsbHwDeleteSChannelFilter  sFsFsbHwDeleteSChannelFilter;
    tFsbNpWrFsFsbHwSetFCoEParams sFsFsbHwSetFCoEParams;
#ifdef MBSM_WANTED
    tFsbNpWrFsMiNpFsbMbsmHwInit  sFsMiNpFsbMbsmHwInit;
    tFsbNpWrFsFsbMbsmHwCreateFilter  sFsFsbMbsmHwCreateFilter;
#endif  /* MBSM_WANTED */
    }unOpCode;

#define  FsbNpFsMiNpFsbHwInit  unOpCode.sFsMiNpFsbHwInit;
#define  FsbNpFsFsbHwCreateFilter  unOpCode.sFsFsbHwCreateFilter;
#define  FsbNpFsFsbHwDeleteFilter  unOpCode.sFsFsbHwDeleteFilter;
#define  FsbNpFsMiNpFsbHwDeInit  unOpCode.sFsMiNpFsbHwDeInit;
#define  FsbNpFsFsbHwCreateSChannelFilter  unOpCode.sFsFsbHwCreateSChannelFilter;
#define  FsbNpFsFsbHwDeleteSChannelFilter  unOpCode.sFsFsbHwDeleteSChannelFilter;
#define  FsbNpFsFsbHwSetFCoEParams  unOpCode.sFsFsbHwSetFCoEParams;
#ifdef MBSM_WANTED
#define  FsbNpFsMiNpFsbMbsmHwInit  unOpCode.sFsMiNpFsbMbsmHwInit;
#define  FsbNpFsFsbMbsmHwCreateFilter  unOpCode.sFsFsbMbsmHwCreateFilter;
#endif  /* MBSM_WANTED */
} tFsbNpModInfo;


/* Function Prototype for Generic NP OpCode specific wrappers
 * defined in Fsbnpapi.c */

UINT1 FsbFsMiNpFsbHwInit PROTO ((tFsbHwFilterEntry * pFsbHwFilterEntry, tFsbLocRemFilterId * pFsbLocRemFilterId));
UINT1 FsbFsFsbHwCreateFilter PROTO ((tFsbHwFilterEntry * pFsbHwFilterEntry, UINT4 * pu4FilterId));
UINT1 FsbFsFsbHwDeleteFilter PROTO ((tFsbHwFilterEntry * pFsbHwFilterEntry, UINT4 u4FilterId));
UINT1 FsbFsMiNpFsbHwDeInit PROTO ((tFsbHwFilterEntry * pFsbHwFilterEntry, tFsbLocRemFilterId FsbLocRemFilterId));
UINT1 FsbFsFsbHwCreateSChannelFilter PROTO ((tFsbHwSChannelFilterEntry * pFsbHwSChannelFilterEntry, UINT4 * pu4FilterId));
UINT1 FsbFsFsbHwDeleteSChannelFilter PROTO ((tFsbHwSChannelFilterEntry * pFsbHwSChannelFilterEntry, UINT4 u4FilterId));
UINT1 FsbFsFsbHwSetFCoEParams PROTO ((tFsbHwVlanEntry *pFsbHwVlanEntry));
UINT1 FsbFsMiNpFsbMbsmHwInit PROTO ((tFsbHwFilterEntry * pFsbHwFilterEntry, tFsbLocRemFilterId * pFsbLocRemFilterId));
UINT1 FsbFsFsbMbsmHwCreateFilter PROTO ((tFsbHwFilterEntry * pFsbHwFilterEntry, UINT4 * pu4FilterId));
#endif
