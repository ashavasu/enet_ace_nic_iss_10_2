/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipv6.h,v 1.130 2017/12/26 13:34:20 siva Exp $
 *
 * Description:This file contains the exported definitions and  
 *             macros of IPV6                                    
 *
 *******************************************************************/
#ifndef _FSIPV6_H
#define _FSIPV6_H

#ifndef NETIP6_WANTED
#define NETIP6_WANTED
#endif

#include "mbsm.h"
#include "fssnmp.h"
#include "utilipvx.h"

/* Max number of interface related macro. */

#ifdef TUNNEL_WANTED
#define  IP6_MAX_LOGICAL_IFACES    (IP_DEV_MAX_L3VLAN_INTF + IP_MAX_RPORT + \
                                    SYS_DEF_MAX_TUNL_IFACES)

#else
#define  IP6_MAX_LOGICAL_IFACES    (IP_DEV_MAX_L3VLAN_INTF + IP_MAX_RPORT)

#endif

/* SEND Macros */
#define SEND_MODE_FULL          1
#define SEND_MODE_DISABLED      2
#define SEND_MODE_MIXED         3

#define ND6_SEND_CGA_MOD_LEN             16
#define ND6_SEND_CGA_PREFIX_LEN           8
#define ND6_SEND_CPS_IDENTIFIER     2 
#define RMAP_MAX_COMM               16
#define IF_SEND_ENABLED(pIp6If) (((ND6_SECURE_ENABLE == pIp6If->u1SeNDStatus) || \
                                  (ND6_SECURE_MIXED == pIp6If->u1SeNDStatus)) ? \
                                  IP6_TRUE : IP6_FALSE)

/* -------------------------- */
/* Data Structure Definitions */
/* -------------------------- */

/* IPv6 Address definition */
typedef struct _IP6_ADDRESS
{
    union
    {
        UINT1  u1ByteAddr[16];
        UINT2  u2ShortAddr[8];
        UINT4  u4WordAddr[4];
    }
    ip6_addr_u;

#define  u4_addr  ip6_addr_u.u4WordAddr  
#define  u2_addr  ip6_addr_u.u2ShortAddr 
#define  u1_addr  ip6_addr_u.u1ByteAddr  

} tIp6Addr;


/* Structure for Higherlayer parameters */
typedef struct _HL_IP6_PARAMETERS
{
    tCRU_BUF_CHAIN_HEADER * pBuf;
    tIp6Addr  Ip6SrcAddr;
    tIp6Addr  Ip6DstAddr;
    UINT4     u4Index;
    UINT4     u4Len;
    UINT4     u4ContextId; /* Context Identifier of VRF */  
    UINT1     u1Cmd;
    UINT1     u1Hlim;
    UINT1     u1Proto;
    UINT1     u1Reserved;  /* for alignment */
 } tHlToIp6Params;
/* Structure for Higherlayer Multicast parameters */
typedef struct _HL_IP6_MCAST_PARAMETERS
{
    tIp6Addr  Ip6SrcAddr;
    tIp6Addr  Ip6DstAddr;
    UINT4    *pu4OIfList;
    UINT4     u4Len;
    UINT1     u1Cmd;
    UINT1     u1Hlim;
    UINT1     u1Proto;
    UINT1     u1Reserved;  /* for alignment */
} tHlToIp6McastParams;

/* Structure for exchanging ICMP related parameters */
typedef struct _ICMP6_PARAMETERS
{
    tIp6Addr    ip6Src;     /* Source address of the packet */
    tIp6Addr    ip6Dst;     /* Dest address of the packet */
    UINT4       u4Index;    /* Interface index number */
    UINT4       u4ContextId;/* Virtual router Id */
    UINT2       u2Len;      /* Length of the ping data */
    UINT2       u2Id;       /* Identification number of the packet */
    UINT2       u2Seq;      /* Sequence number of the packet */
    UINT2       u2Reserved; /* For Alignment */
    UINT1       u1Cmd;      /* Command to differentiate type of data */
    UINT1       u1Type;     /* Type of the ICMP6 message in ping packet */
    UINT1       u1Code;     /* Code of the message */
    UINT1       u1Reserved; /* for alignment */
} tIcmp6Params;


/*
 * The following strcuture defines IP6 interface statistics. The IP6
 * interface definition follows after this.
 */

typedef struct _IP6_INTERFACE_STATISTICS
{

    /* Recv stats */
    
    UINT4  u4InRcvs;        /* Total datagrams */
    UINT4  u4InMcasts;      /* Total multicast pkts */
    UINT4  u4InDelivers;    /* Total datagrams given to upper layer
                             * - ICMP/UDP  */
    UINT4  u4InDiscards;    /* Datagrams discarded despite no errors */
    UINT4  u4InHdrerrs;     /* Datagrams with error in IPv6/Option
                             * headers */
    UINT4  u4InAddrerrs;    /* Datagrams with error in dest addr */
    UINT4  u4InUnkprots;    /* Datagrams with unknown/unsupported
                             * protocol */
    UINT4  u4InTruncs;      /* Datagrams with insufficient data */
    UINT4  u4InNorts;       /* Input Datagrams discarded due to no routes */
    
    /* Send/Forward stats */
    
    UINT4  u4OutMcasts;     /* Total multicast pkts */
    UINT4  u4ForwDgrams;    /* Datagrams attempted to be forwarded */
    UINT4  u4TooBigerrs;    /* Datagrams which could not be forwarded
                             * as their size exceeds (Path) MTU */
    UINT4  u4OutReqs;       /* Datagrams attempted to be sent */
    UINT4  u4OutDiscards;   /* Outgoing Datagrams discarded despite
                             * no errors */
    UINT4  u4OutNorts;      /* Datagrams discarded due to no routes */

    /* Reasm stats */

    UINT4  u4Reasmreqs;     /* Datagrams needing reassembly */
    UINT4  u4Reasmoks;      /* Datagrams successfully reassembled */
    UINT4  u4Reasmfails;    /* Reassembly failures */
    UINT4  u4Fragoks;       /* Datagrams successfully fragmented */
    UINT4  u4Fragfails;     /* Fragmentation failures */
    UINT4  u4Fragcreates;   /* Fragments created */

    /* ND Recv stats */

    UINT4  u4InNsols;       /* No.of recd Neighbor Solicitations */
    UINT4  u4InNadvs;       /* No.of recd Neighbor Advertisements */
    UINT4  u4InRoutsols;    /* No.of recd Router Solicitations */
    UINT4  u4InRoutadvs;    /* No.of recd Router Advertisements */
    UINT4  u4InRoutRedirs;  /* No.of recd Router Redirect messages */

    /* ND Send stats */

    UINT4  u4OutNsols;      /* No.of sent Neighbor Solicitations */
    UINT4  u4OutNadvs;      /* No.of sent Neighbor Advertisements */
    UINT4  u4OutRoutadvs;   /* No.of sent Router Advertisements */
    UINT4  u4OutRedirs;     /* No.of sent Redirects */

    /* ND Timer stats */

    UINT4  u4OutRoutSols;   /* No.of sent Router Solicitations */
    UINT4  u4RsSchedTime;   /* Next RS scheduled
                                * time */
    UINT4  u4RsSentTime;    /* Last RS sent time */
    UINT4  u4RsInitialCnt;  /* Initial RSs sent */
    UINT4  u4RaSchedTime;   /* Next multicast RA scheduled 
                                * time */
    UINT4  u4RaSentTime;    /* Last RA sent time */
    UINT4  u4RaInitialCnt;  /* Initial RAs sent */
    /* IPvx Counters */
    UINT4 u4HCInReceives;     /* HC counter for Total Pkt Rx */
    UINT4 u4InOctets;         /* Total Octets Rx */
    UINT4 u4HCInOctets;       /* HC counter for Total Octets Rx */
    UINT4 u4HCInForwDgrams;   /* HC counter for pkt considered eligible 
                                 for fwd */
    UINT4 u4HCInDelivers;     /* HC counter for Pkts sent to high level 
                                 protocols*/ 
    UINT4 u4HCOutRequests;    /* HC counter for Total send requests
                                 made to IPv6 */
    UINT4 u4OutForwds;        /* Pkts considered eligible for fwd form 
                                 local IPv6 */
    UINT4 u4HCOutForwds;      /* HC counter for Pkts considered eligible 
                                 for fwd form local IPv6 */
    UINT4 u4OutFragReqds;     /*  No of outgoing pkts fragmented */ 
    UINT4 u4OutTrans;         /* Total no of IPv6 Pkts to LL for Tx */ 
    UINT4 u4HCOutTrans;       /* HC counter for Total no of IPv6 Pkts 
                                 to LL for Tx */
    UINT4 u4OutOctets;        /* Total no of IPv6 Octets to LL for Tx */ 
    UINT4 u4HCOutOctets;      /* HC counter for Total no of IPv6 Octets to 
                                LL for Tx */
    UINT4 u4HCInMcasts;       /* Number of IPv6 multicast Pkts Rx */
    UINT4 u4InMcastOctets;    /* Number of IPv6 multicast Octets Rx */
    UINT4 u4HCInMcastOctets;  /* HC counter for Number of IPv6 multicast
                                Octets Rx */
    UINT4 u4HCOutMcastPkts;   /* HC counter for No of IPv6 multicast Pkts Tx */ 
    UINT4 u4OutMcastOctets;   /* Number of IPv6 multicast Octets Tx */
    UINT4 u4HCOutMcastOctets; /* HC counter for No of IPv6 multicast 
                                 Octets Tx */
    UINT4 u4Icmp6RLErrMsgCnt; /* Rate limited Err msg count */
    UINT4 u4NdSecureDroppedPkts; /* No of Secure ND Dropped packets */
    UINT4 u4NdSecureInvalidPkts; /* No of Secure ND Invalid Packets */    
}
tIp6IfStats;

typedef struct _IP6_INTERFACE_ND6_STATISTICS
{
    UINT4   u4CgaOptPkts;       /* Packets with CGA option */
    UINT4   u4CertOptPkts;      /* Packets with Certificate option */
    UINT4   u4MtuOptPkts;       /* Packets with MTU option */
    UINT4   u4NonceOptPkts;     /* Packets with nonce option */
    UINT4   u4PrefixOptPkts;    /* Packets with Prefix option */
    UINT4   u4RedirHrPkts;      /* Packets with Redirect Header */
    UINT4   u4RsaOptPkts;       /* Packets with RSA option  */
    UINT4   u4SrcLinkAddrPkts;  /* Packets with Soure Link Layer Address */
    UINT4   u4TgtLinkAddrPkts;  /* Packets with Target Link Layer Address */
    UINT4   u4TaOptPkts;        /* Packets with TA Option */   
    UINT4   u4TimeStampOptPkts; /* Packets with Time Stamp Option */
}
tIp6NdSecStats;

/* SeND CGA Options Structure */
typedef struct _sCgaOptions
{
    UINT1       au1Modifier [ND6_SEND_CGA_MOD_LEN];
    UINT1       au1Prefix [ND6_SEND_CGA_PREFIX_LEN];
    UINT1       *pu1DerPubKey;
    UINT2       u2PubKeyLen;
    UINT1       u1Collisions;
    UINT1       u1Rsvd;
}tCgaOptions;

/*
 * Structure of an IPv6 header.
 */
typedef struct
{
    UINT4  u4Head;        /* the first 4 bits are for version, next
                            * 8 bits for priority and the last 20 bits
                            * are for flow label
                            */
    UINT2  u2Len;         /* 16-bit unsigned interger. It is the length
                            * of the rest of the packet following the 
                            * IPv6 header, in octets. If 0 indicates that
                            * the payload length is carried in a jumbo
                            *  payload hop-by-hop option
                            */
    UINT1  u1Nh;          /* Identifies the type of header
                            * immediately  following the IPv6 header
                            */
    UINT1     u1Hlim;   /* Hop limit in the packet */
    tIp6Addr  srcAddr;  /* 128-bit source address */
    tIp6Addr  dstAddr;  /* 128-bit destination address */

}tIp6Hdr;

/*NdData is used for sending event from RTM6 to Netip6 for resolving ND cache */
typedef struct NdData
{
      UINT1  u1Cmd;
      UINT1  u1LinkType;
      UINT2  u2Reserved;
      UINT4  u4IfIndex;
      UINT2  u2_SubReferenceNum;
      UINT1  u1_InterfaceType;
      UINT1  u1_InterfaceNum;
      tIp6Addr NextHop;
}tNdData;
/*
 * The following structure defines the IP6 Timer structure. Timer nodes
 * started have this structure. A pointer of this type is added in the timer
 * list and upon timeout, the pointer will be returned
 */

typedef struct _IP6_TIMER_NODE
{
    tTmrAppTimer  appTimer;
    UINT1         u1Id;          /* Timer Id */
    UINT1         u1Reserved[3];  /* Padding  */
}
tIp6Timer;


typedef struct _IP6_IS_SCOPE_ZONE_VALID
{
    tTMO_SLL_NODE           NextNode;               /*Contain the pointer of the next node*/
    INT4                    i4ZoneIndex;            /*ipv6 Zone index*/
    INT1                    u1Ip6IsZoneIndexValid;   /*ipv6 interface index zone is valid or not.*/
    UINT1                   u1pad[3];
}tIp6IsScopeZoneValid;



#define IP6_EUI_ADDRESS_LEN              8
#define IP6_EUI_ADDRESS_PREFIX_LEN       64
#define IP6_IF_DESCR_LEN                 64  /* Interface Descriptor Length */
#define IP6_MAX_IF_NAME_LEN              64  /* Interface name length */
#define IP6_MAX_PROTOCOLS                200
#define IP6_MAX_MCAST_PROTOCOLS          1 
#define  IP6_MAX_SYS_TIME  0xFFFFFFFF 
 #define IP6_MAX_PREFIX_LEN              128
#define  IP6_DNS_LIFETIME_INFINITY       0xFFFFFFFF


/* Multiple interfaces attached to a link
   The following macros are used by other external modules like ospf */

#define IPV6_ACTIVE_ADD                  1  /*To add the active interface */
#define IPV6_ACTIVE_DELETE               2  /*To delete the active interface*/
#define IPV6_STANDBY_MODIFY              3  /* modification on standby interfaces */
#define IPV6_MAX_IF_OVER_LINK            8  /*Maximum number of interfaces
                                              over the link */

#define IPV6_IF_ACTIVE                  1   /*Active interface */
#define IPV6_IF_STANDBY                 2   /*Standby interface */
#define RTM6_ND6_RESOLVE_EVENT          23
/*
 * INTERFACE TYPE specific constants
 */
#define  IP6_MAX_USER_DATA_LEN        6    
#define  IP6_MAX_ENET_ADDR_LEN        6    
#define  CRU_ENET_TYPE_ENCAPSULATION  0x01 
#define  CRU_COMMON_FORWARD_MODULE    0x08 
#define  IP6_ENET_INTERFACE_TYPE      0x06 
#define  IP6_X25_INTERFACE_TYPE       0x26 
#define  IP6_FR_INTERFACE_TYPE        0x20 
#define  IP6_PPP_INTERFACE_TYPE       0x17 
#define  IP6_TUNNEL_INTERFACE_TYPE    0x83
#define  IP6_L3VLAN_INTERFACE_TYPE    0x88
#define  IP6_LAGG_INTERFACE_TYPE      0xA1
#define  IP6_LOOPBACK_INTERFACE_TYPE  0x18
#define  IP6_PSEUDO_WIRE_INTERFACE_TYPE  0xF6
#define  IP6_L3SUB_INTF_TYPE          0x89

/* ND cache timeout  */
#define   ND_DEF_CACHE_TIMEOUT    30
#define   ND_MIN_CACHE_TIMEOUT    30
#define   ND_MAX_CACHE_TIMEOUT    86400

#define  IP6_MOD_TRC      0x010000ff    /* to be substitued for cmod argument */
#define  ICMP6_MOD_TRC    0x020000ff
#define  UDP6_MOD_TRC     0x080000ff
#define  ND6_MOD_TRC      0x100000ff
#define  PING6_MOD_TRC    0x200000ff
#define  V6_OVER_V4_TUNL  0x800000ff
#define  MIP6_MOD_TRC     0x040000ff
#define  NETIP6_MOD_TRC   0x400000ff
#define  IP6_NO_MOD_TRC   0x00000000


#define IP6_TRUE       1
#define IP6_FALSE      2

/* Address configuration method */
#define  IP6_ADDR_STATIC    1   /* STATIC Configuration. */ 
#define  IP6_ADDR_VIRTUAL   5   /* Virtual Configuration, Keeping this value
                                   as 5 since other methods like stateless,
                                   stateful and dynamic are possible. */

#define  IP6_ADDR_AUTO_SL   2
/*
 * zone Configuration constants - RFC 4007
 */

/*****************************************************************************/
  /*Various Possible ROW STATUS*/
  #define   IP6_ZONE_ROW_STATUS_ACTIVE             ACTIVE
  #define   IP6_ZONE_ROW_STATUS_NOT_IN_SERVICE     NOT_IN_SERVICE
  #define   IP6_ZONE_ROW_STATUS_NOT_READY          NOT_READY
  #define   IP6_ZONE_ROW_STATUS_CREATE_AND_GO      CREATE_AND_GO
  #define   IP6_ZONE_ROW_STATUS_CREATE_AND_WAIT    CREATE_AND_WAIT
  #define   IP6_ZONE_ROW_STATUS_DESTROY            DESTROY
/*****************************************************************************/
#define   IP6_ZONE_INVALID                  -1

#define   IP6_ZONE_VALID                    1

#define   IP6_SCOPE_ZONE_NAME_LEN           32

#define   IP6_MAX_SCOPE_ZONE_TYPES          15

#define   IP6_MAX_SCOPE_ZONE                60

#define   IP6_DEFAULT_SCOPE_ZONE_INDEX      0 

#define   IP6_DOMAIN_NAME_LEN           20

#define   IP6_SCOPE_ZONE_LIST_SIZE          ((IP6_MAX_SCOPE_ZONE/8) + 1)

#define   IP6_MAX_ZONE_INTERFACE            (((IP6_MAX_LOGICAL_IFACES/4)*4) + 4)

#define   IP6_MAX_ZONE_INT_LIST_SIZE        ((IP6_MAX_ZONE_INTERFACE + 31)/32 * 4)


#define IP6_SCOPE_ZONE_INDEX  0
#define IP6_SCOPE_ZONE_NAME   1
#define IP6_SCOPE_ZONE_SCOPE  2
#define IP6_SCOPE_ZONE_COUNT   FsIP6SizingParams[MAX_IP6_SCOPE_ZONES_VALID_SIZING_ID].u4PreAllocatedUnits 
typedef UINT1 tInterfaceList [IP6_MAX_ZONE_INTERFACE];
typedef UINT1 tZoneList[IP6_SCOPE_ZONE_LIST_SIZE];

/* RFC 4007 Scope Values Constants - Start */
enum scope{
ADDR6_SCOPE_RESERVED0,
ADDR6_SCOPE_INTLOCAL,
ADDR6_SCOPE_LLOCAL,
ADDR6_SCOPE_SUBNETLOCAL,
ADDR6_SCOPE_ADMINLOCAL,
ADDR6_SCOPE_SITELOCAL,
ADDR6_SCOPE_UNASSIGN6,
ADDR6_SCOPE_UNASSIGN7,
ADDR6_SCOPE_ORGLOCAL,
ADDR6_SCOPE_UNASSIGN9,
ADDR6_SCOPE_UNASSIGNA,
ADDR6_SCOPE_UNASSIGNB,
ADDR6_SCOPE_UNASSIGNC,
ADDR6_SCOPE_UNASSIGND,
ADDR6_SCOPE_GLOBAL,
ADDR6_SCOPE_RESERVEDF,
ADDR6_SCOPE_INVALID
};
/* RFC 4007 Scope Values Constants - End */


/* Structure for Storing the list of prefix advertised in the route
 * advertisement message send over this interface. */
typedef struct _IP6_PREFIX_NODE
{
    tTMO_SLL_NODE   NextPrefix;   /* Next Prefix in the list. */
    tIp6Addr    Ip6Prefix;          /* IP6 Prefix Address */
    tIp6Timer   ValidTimer;         /* Variable Valid Timer */
    tIp6Timer   PreferTimer;        /* Variable Prefer Timer */
    UINT4       u4IfIndex;          /* Interface over which this prefix is
                                     * to be advertised. */
    UINT2       u2ProfileIndex;     /* Profile List Index for this prefix */
    UINT1       u1PrefixLen;        /* IP6 Prefix Address Length */
    UINT1       u1AdminStatus;      /* Prefix's Administrative Status */
    UINT1       u1EmbdRpValid;      /* Prefix is a valid RP address*/
    UINT1       u1DefPrefFlag;
    UINT1       au1Reserved[2];
}
tIp6PrefixNode;

/*  Structure for storing the Route information which are advertised 
 *  in the router advertisment message send over this interface. */

typedef struct _IP6_RA_ROUTE_INFO_NODE
{
    tRBNodeEmbd RoutInfoRBNode;
    tIp6Addr    Ip6RARoutePrefix;
    INT4       i4RARouteIfIndex;
    INT4       i4RARoutePrefixLen;
    INT4       i4RARoutePref;
    UINT4      u4RARouteLifetime;
    INT4       i4RARouteRowStatus;
}
tIp6RARouteInfoNode;

typedef struct 
{
    tIp6Addr RpAddr;  /* RP address - RP prefix derived from multicast addr and interface identifier */ 
    UINT4  u4OutIfIndex;  /* Logical interface through which the next hop for given RP address is reachable */
    UINT1 u1IsSelfNode;  /* Flag to specify whether the given RP address exists in the self node */
    UINT1       au1Reserved[3];
}tIp6RpAddrInfo;
/*
 * ICMPv6 global statistics
 */

typedef struct
{
    /* Incoming messages statistics */
    UINT4  u4InMsgs;
    UINT4  u4InErrs;
    UINT4  u4InBadcode;
    UINT4  u4InToobig;
    UINT4  u4InDstUnreach;
    UINT4  u4InTmexceeded;
    UINT4  u4InParamprob;
    UINT4  u4InEchoReq;
    UINT4  u4InEchoResp;
    /* Outgoing messages statistics */
    UINT4  u4OutMsgs;
    UINT4  u4OutErrs;
    UINT4  u4OutDstUnreach;
    UINT4  u4OutToobig;
    UINT4  u4OutTmexceeded;
    UINT4  u4OutParamprob;
    UINT4  u4OutEchoReq;
    UINT4  u4OutEchoResp;
    UINT4  u4IcmpOutRateLimit; 
    /* ND6 statistics */
    UINT4  u4InRouterSol;
    UINT4  u4InRouterAdv;
    UINT4  u4InNeighSol;
    UINT4  u4InNeighAdv;
    UINT4  u4InRedir;
    UINT4  u4OutRouterSol;
    UINT4  u4OutRouterAdv;
    UINT4  u4OutNeighSol;
    UINT4  u4OutNeighAdv;
    UINT4  u4OutRedir;
    /*To verify R, S, O Flags in Neighbor Advertisement*/
    UINT4  u4InNARouterFlag;
    UINT4  u4InNASolicitedFlag;
    UINT4  u4InNAOverrideFlag;
    UINT4  u4OutNARouterFlag;
    UINT4  u4OutNASolicitedFlag;
    UINT4  u4OutNAOverrideFlag;
    /* MLD statistics */
    UINT4  u4InMLDQuery;
    UINT4  u4InMLDReport;
    UINT4  u4InMLDDone;
    UINT4  u4OutMLDQuery;
    UINT4  u4OutMLDReport;
    UINT4  u4OutMLDDone;
    tIp6NdSecStats Nd6SecOutStats[8];          /* Statistics of sent packets in SeND  */
    tIp6NdSecStats Nd6SecInStats[8];           /* Statistics of received packets in SeND */
    tIp6NdSecStats Nd6SecDropStats[8];         /* Statistics of dropped packets in SeND */
}
tIcmp6Stats;

typedef struct
{
    tIp6Timer   ErrIntervalTimer;   /* Icmp6 Ratelimit Timer */
    INT4        i4Icmp6ErrRLFlag;   /* Flag to Inform Icmp6Err Rate Limiting
                                       enabled in the Interface */
    INT4        i4Icmp6ErrInterval; /* Icmp6 Err Msg Interval */
    INT4        i4Icmp6BucketSize;  /* Token Bucket size ie. no of Err pkt to
                                       be sent in the given Err interval */
    INT4        i4Icmp6ErrMsgCnt;   /* No of Err Msgs Sent  */
    INT4        i4Icmp6DstUnReachable; /* Flag for Destination unreachable msg
                                         allowed to send */
    INT4        i4Icmp6RedirectMsg;  /* Icmp6 Redirect message enable or disable status */
}
tIcmp6ErrRLInfo; /* Icmp6 Error Rate Limit Info */


/*
 * The following structure defines a Reassembly Entry. An entry is identified
 * by the combination of Source Address, Destination Address and Fragment Id.
 * The fragments received are queued in a doubly-linked list on this entry.
 * This is an ordered list based on the fragment offsets. When all fragments
 * are received, they are reassembled and handed for further processing and
 * the Reassembly entry is removed. When we timeout without receiving all the
 * fragments, the buffers are released and the Reassembly entry is removed.
 */

typedef struct _IP6_REASM_ENTRY
{
    tIp6Addr     ip6Src;
    tIp6Addr     ip6Dst;
    UINT4        u4Id;
    tTMO_DLL     fragQue;
    UINT4        u4PktLen;
    UINT4        u4CurLen;
    tIp6Timer    timer;
    UINT1        u1State;
    UINT1        au1Reserved[3]; /* Padding */
    UINT4        u4Index;
}
tIp6ReasmEntry;

typedef struct _IP6_UDP_GLOBAL_STATISTICS
{
    UINT4  u4InDgrams;   /* No. of received datagrams */
    UINT4  u4InErrs;     /* No. of error encountered during 
                            * processing of packet */
    UINT4  u4OutDgrams;  /* No. of datagrams passed to IP6 layer */
    UINT4  u4NumPorts;   /* No. of ports that are currently open */
    FS_UINT8  u8HcInDgrams; /* No. of received datagrams */ 
    FS_UINT8     u8HcOutDgrams;/* No. of datagrams passed to IP6 layer*/ 
}
tIp6Udp6Stats;

typedef struct Ip6AddrLst
{
    tTMO_SLL_NODE  SllNode;      /* SLL Node */
    tIp6Addr       Ip6Addr;      /* Ip6 address */
    INT4           i4PrefixLen;
    UINT2          u2Addr6Profile;
    UINT1          u1AddrType;   /* Unicast/Anycast */
    UINT1          u1AdminStatus; /* Address status */
    UINT1          u1Status;
    UINT1          u1Pad[3];
}tLip6AddrNode;

/*
 * Structure for UDP Port - the following structure contains information
 * about opened UDP ports
 */

typedef struct _UDP_PORT_CONTROL_BLOCK
{
    tTMO_SLL_NODE  NextCBNode;
    UINT1       au1TaskName[4];
    UINT1       au1RcvQName[4];
    UINT2       u2UPort;              /* UDP port number */
    UINT2       u2Remote_port;     /* Remote Port of the connections */
    INT4        i4SockDesc;          /* Socket identifier of this socket */
    INT4        i4LocalAddrType;   /* Local IP address type */
    INT4        i4RemoteAddrType;  /* Remote IP address type */
    tIp6Addr    addr;             /* IPv6 address for which application 
                                         * is listening on this UDP port */
    tIp6Addr    Remote_addr;       /* Remote IP address */
    UINT4       u4Instance;        /* Instance associated with the conn */
        
    VOID        (*rcv_fnc) (UINT4  u4contextId,
                            tIp6Addr srcAddr,  
                            tIp6Addr dstAddr, 
                            UINT2 u2SrcPort,  
                            UINT2 u2DstPort,  
                            tCRU_BUF_CHAIN_HEADER *pBuf,
                            UINT4 u4Index,
                            UINT4 u4Len,
                            UINT1 u1HLim);  /* Function pointer for direct 
                                             * procedure call to application. 
                                             *  This is for future use */
}
tUdp6Port;

/* The IP6 context information */
typedef struct _IP6_CONTEXT
{
    tIp6IfStats        Ip6SysStats;
    tIcmp6Stats        Icmp6Stats;
    tIp6ReasmEntry    *apIp6Ream[MAX_IP6_REASM_ENTRIES_LIMIT];
    tTMO_SLL           Nd6ProxyList;
    tTMO_SLL           Udp6CxtCbEntry;
    tIp6Udp6Stats      Udp6Stats;
    UINT4              u4Ip6Dbg;
    UINT4              u4ForwFlag;
    UINT4              u4Ip6DiscaredRoutes;
    UINT4              u4DgramFragId;
    UINT4              u4Ipv6HopLimit;
    UINT4              u4ContextId;
    UINT4              u4Nd6CacheEntries;
    UINT4              u4CfgTimeOut;
    UINT4              u4MaxNonGblZoneCount; /* Added for RFC 4007 to keep
                                                track of non-global zone*/
    INT4               i4Nd6CacheMaxRetries;
    INT4               ai4Ip6LlocalZoneId[IP6_MAX_ZONE_INTERFACE]; /*Zone id added for RFC 4007*/
    INT4               ai4Ip6IntLocalZoneId[IP6_MAX_ZONE_INTERFACE]; /*Zone id added for RFC 4007*/
    UINT2              u2JmbSendPkts;
    UINT2              u2JmbRecdPkts;
    UINT2              u2JmbErrPkts;
    UINT1              au1HLRegTable[IP6_MAX_PROTOCOLS];
    UINT1              au1HLMcastRegTable[IP6_MAX_MCAST_PROTOCOLS];
    UINT1              u1PmtuEnable;
    UINT1              u1JmbCfgStatus;
    UINT1              au1Reserved[3];
} tIp6Cxt;

/* The IP6 Zone to Interface Mapping table -RFC 4007*/

typedef struct  _IP6_IF_ZONE_MAP_INFO
{
    tTMO_SLL_NODE   nextZone;     /* Pointer to next scope-zone 
                                          on this interface */
    UINT4           u4ContextId;       /* Context id associted with 
                                          this interface */
    INT4            i4ZoneId;          /* Zone Id for this scope-zone */
    INT4            i4ZoneIndex;       /* Zone Index for the scope-zone */
    UINT1           au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN]; /*Zone Name */
    UINT1           u1Scope;           /* Scope of the zone*/
    UINT1           u1ConfigStatus;    /* Configuration status 
                                          (i.e) auto or manual */
    UINT1           au1Reserved[2];     /* padding bits */
} tIp6IfZoneMapInfo; 

/* The Scope-Zone information RBTree Structure - RFC 4007 */

typedef struct _IP6_SCOPE_ZONE_INFO
{
   tRBNodeEmbd     zoneRbNode;         /* RBnode for the unicast/anycast 
                                          RBTree */
   tInterfaceList  InterfaceList;      /* List of Interfaces associated 
                                          with this scope-zone node*/
   INT4            i4ZoneId;           /* ZoneId for this scope-zone */
   INT4            i4ZoneIndex;        /* ZoneIndex for this scope-zone */
   UINT4           u4ContextId;        /* context-id for this scope-zone*/
   UINT4           u4ZoneCount;        /* zoneCount added to keep track of 
                                          no of zones getting created */
   UINT1           u1Scope;            /* scope associate with this scope-zone*/
   UINT1           u1CreateStatus;     /* Creation status for this scope-zone 
                                          (i.e) auto or manual or overridden */
   UINT1           u1IsDefaultZone;    /* Is this zone default zone for a particular scope */
   UINT1           au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN]; /*Zone name for this scope-zone*/                
   UINT1           u1Reserved;      /* Padding bits*/ 
}tIp6ScopeZoneInfo;

/*
 * The following strcuture defines IPv6 Logical Interface. All the modules of
 * the IPv6 protocol perate on logical interfaces. There is a one-to-one
 * mapping between logical interface and ifTable interfaces.
 *
 * The routing protocol is configured over this struct. This structure contains
 * the linked list of link-local addresses and unicast/anycast addresses on the
 * interface. The interface structures are maintained in a hash table.
 */
typedef struct _IP6_INTERFACE
{
    /* Runtime parameters (status, statistics ...) */
    tIp6Timer  Timer;                       /* Timer Node for Router 
                                             * Advertisement or router
                                             * solicitations if it is host */
    tTMO_SLL        lla6Ilist;              /* Link local address information
                                             * list */
    tTMO_SLL        addr6Ilist;             /* Unicast/Anycast address
                                             * information list */
    tTMO_SLL        mcastIlist;             /* Multicast address information
                                             * list */
    tTMO_SLL        prefixlist;             /* List of IP6 Prefix, that are
                                             * advertised in the RA message
                                             * sent over this interface. */
    tTMO_SLL        zoneIlist;              /* List of scope-zones that are
                                               created on an interface*/
    tTMO_HASH_NODE  ifHash;                 /* Link into next logical interface
                                             * in the hash bucket */
    tIp6IfStats  stats;                     /* Interface Statistics */
    tIcmp6Stats  *pIfIcmp6Stats;            /* Interace Statistics for ICMPv6*/

#ifdef TUNNEL_WANTED
    struct _IP6_TUNNEL_INTERFACE    *pTunlIf;
                                            /* If interface type is Tunnel,
                                             * then this pointer holds
                                             * additional configuration
                                             * value for the tunnel. */
#endif
    tIp6Cxt    *pIp6Cxt; 
    tIcmp6ErrRLInfo  Icmp6ErrRLInfo;        /* Icmp6 Error Rate Limit Info */

   /* Configurable parameters */
    UINT1  ifaceTok[IP6_EUI_ADDRESS_LEN];   /* for EUI-64 */
    UINT1  u1Descr[IP6_IF_DESCR_LEN];       /* Interface Description */
    UINT4  u4Index;                         /* Index of this entry in the
                                             * ip6If[] array */
    UINT4  u4IpPort;                        /* Ip port of the Index */ 
    UINT4  u4IfLastUpdate;                  /* Lastest interface update */
    UINT4  u4Reachtime;                     /* Reachable time */
    UINT4  u4Rettime;                       /* Retransmit time */
    UINT4  u4Pdelaytime;                    /* Delay first probe time */
    UINT4  u4MaxRaTime;                     /* Max Router Adv interval */
    UINT4  u4MinRaTime;                     /* Min Router Adv interval */
    UINT4  u4Mtu;                           /* MTU on this interface. */
    UINT4  u4IfSpeed;                       /* Interface Speed in BPS */
    UINT4  u4IfHighSpeed;                   /* Interface Speed in MBPS */
    UINT4  u4OnLinkActiveIfId;                        /* Cfa ifindex of the active
                                               interface on the link*/
    UINT4  u4CurOperStatus;
    UINT4  u4UnnumAssocIPv6If;              /* Associated unnumbered interface index */
    UINT2  u2Deftime;                       /* Default router lifetime */
    UINT2  u2DadSend;                       /* Max Number of DAD NS transmits
                                             * for an address on the
                                             * interface */
    UINT2  u2CktIndex;                      /* Circuit index */
    UINT2  u2OnlinkIfCount;                 /* The no of interfaces that are on link
                                               with this interface. This will be updated
                                               at the time of onlink notificatio from OSPF*/
    UINT1  au1OnLinkIfaces[IPV6_MAX_IF_OVER_LINK]; /* This array has the list 
                                              of standby interfaces this is updated
                                              after receiving onlink notification
                                              from OSPF*/
                                              
    UINT1  u1IfType;                        /* Interface type */
    UINT1  u1TokLen;                        /* Interface token length */
    UINT1  u1AdminConfigFlag;               /* Value indicating whether the
                                             * ADMIN_UP status is enabled
                                             * by explicit configuration or
                                             * by IPv6 Address Configuration.
                                             * If TRUE, then enabled via
                                             * explict configruation else
                                             * other case. */
    UINT1  u1AdminStatus;                   /* Admin Status of the ipv6
                                             * interface */
    UINT1  u1OperStatus;                    /* Oper Status of the ipv6
                                             * interface */
    UINT1  u1LLOperStatus;                  /* Oper Status of the interface as
                                             * indicated by the lower layer.
                                             */
    UINT1  u1RaCnf;                         /* Configuration whether to send 
                                             * Router Advertisements and M, O
                                             * bits in RA */
    UINT1  u1RsFlag;                        /* Flag deciding whether to send
                                             * Router solicitations
                                             * if 1 - RS can be sent
                                             * if 0 - RS must be desisted */
    UINT1  u1Hoplmt;                        /* Hop limit value */
    UINT1  u1PrefAdv;                       /* Prefix advertise status */
    UINT1  u1Ipv6IfFwdOperStatus;           /* Interface's forwarding status  - Operation status*/
    UINT1  u1Ipv6IfFwdStatusConfigured;     /* Interface's forwarding status  as per the MIB configuration*/
    UINT1  u1IfStatusOnLink;                /* Status of interface over the link- active/standby */
    UINT1  u1ZoneCreationStatus;            /* The creation status of Scope-Zone on
                                               this interface RFC4007 */
    UINT1  u1ZoneRowStatus;                 /* The row status of the Scope-Zone RFC4007*/
    UINT1  u1TooBigMsgReceived;             /* Too Big message status */ 
    UINT1  u1NDProxyAdminStatus;     /* Admin status of the Proxy enabled ipv6 interface */
    UINT1  u1NDProxyOperStatus;            /* Operational status of Proxy enabled ipv6 interface */
    UINT1  u1NDProxyMode;      /* Global or local Proxy Status */
    BOOL1  b1NDProxyUpStream;      /* Upstream or Downstream Proxy interface */
    tIp6Timer  proxyLoopTimer;      /* Timer for Proxy Loop Prevention */
    UINT1  u1PbitRtrAdvTxCount;             /* Number of Rtr Advt sent on this interface
                                               since Oper status was disabled for loop prevention */
    UINT1  u1SeNDStatus;                    /* Secure ND Status */
    UINT2  u2SeNDDeltaTime;                 /* SeND Delta time stamp. Max 1000s */

    UINT2  u2SeNDFuzzTime;                  /* SeND Fuzz time stamp. Max 10000ms */
    UINT1  u1SeNDDriftTime;                 /* SeND Drift time stamp. Max 100% */
    UINT1  au1Rsvd[1];                      /* for padding */
    UINT1  au1RcvdNonce[6];
    UINT1  au1SentNonce[6];
    UINT4  u4TSLast;
    UINT4  u4RDLast;

    UINT2  u2Preference;                     /* RFC 4191 Default Router Preference */
    UINT1  au1Rsvd1[2];                      /* for padding */

    tRBTree Ip6RARouteInfoTree;
}
tIp6If;

typedef struct _IP6_ADDRESS_INFORMATION
{
    tRBNodeEmbd     unicastRbNode; /* RBnode for the unicast/anycast RBTree */
    tRBNodeEmbd     anycastRbNode; /* RBnode for the subnet-router anycast 
                                      RBTree */
    /*  configurable parameters  */
    tIp6Addr    ip6Addr;        /* IPv6 address */
    tIp6If        *pIf6;        /* Pointer to interface */
    tTMO_SLL_NODE  nextAif;     /* Pointer to next unicast/anycast
                                 * address on this interface */
    tIp6Timer   dadTimer;       /* Timer Node for DAD */
    tIp6Timer   AddrPrefTimer;  /* Address invalidation timer */
    tIp6Timer   AddrValidTimer; /* Address invalidation timer */
#ifdef MIP6_WANTED
    tIp6Addr    ip6SrcAddr;     /* IPv6 address */
    tTMO_SLL_NODE  nextMnHaif;  /* Pointer to next mnha
                                 * address on this interface */
#endif
    UINT4       u4IpCreated;    /*Time at which IP add is created*/ 
    UINT4       u4IpLastChanged;/*Latest time at which IP add is changed*/
    UINT2       u2Addr6Profile; /* Index into Address Profile 
                                 * Table */
    /*  Runtime parameters (status, statistics ...)  */
    UINT2       u2DadSent;   /* Number of DAD NS sent */
#ifdef MIP6_WANTED
    UINT2       u2AddrFlag;     /* field specifying whether it is 
                                 * home address or coa */
#else
    UINT2       u2Reserved;     /* Reserved */
#endif
    UINT1       u1Status;       /* Status of the address */
    UINT1       u1PrefLen;      /* Prefix length of address */
    UINT1       u1AddrType;     /* Unicast or Anycast */
    UINT1       u1Reserved;     /* Reserved */
    UINT1       u1ConfigMethod; /* Variable indicating how the address is
                                 * created. Auto or static configuration */
    UINT1       u1AdminStatus;  /* Administrative Status of the address */
    UINT1       u1IpStatus;     /*Status of the IP address*/
    UINT1       u1AddrRowStatus;/*New address table rowstatus*/
    UINT1       u1IpStorageType;/*Storage type of IP address*/
    UINT1       u1ProfileIndex; /*Profile Index*/
    UINT1       u1PrefixRowStatus; /*Prefix table rowstatus */
    UINT1       u1Origin;       /*Origin of the IP address*/ 
    UINT1       u1AddrScope;   /*Scope of the Unicast/Anycast address- Added for RFC4007*/
    BOOL1       b1SeNDCgaStatus; /* SeND Cga Status */

    tCgaOptions cgaParams;      /* CGA Parameter Data Structure */
                       
}
tIp6AddrInfo;
typedef struct _NetIp6RmapComm
{
    UINT4   au4Community[RMAP_MAX_COMM];
} tRt6RmapComm;
/* 
 * The routing table entry structure 
 */
typedef struct rte_entry
{
    tTMO_SLL_NODE       NextEntry;   /* Pointer to the next route in List */
    struct rte_entry   *pNextRtEntry;
                                     /* Pointer to the next identical route
                                      * from other protocol. This SLL will be
                                      * sorted based on protocol id.
                                      */
    struct rte_entry   *pNextAlternatepath;
                                     /* Pointer to the next identical route
                                      * from the same protocol. This SLL will
                                      * be sorted based on the metric
                                      */
    struct rte_entry   *pBestRoute;
    struct _IP6_TIMER_NODE rtEntryTimer;
                                     /* For route timeouts and for garbage
                                      * collection
                                      */
    tIp6Addr            dst;         /* The destination address prefix */
    tIp6Addr            nexthop;     /* The next hop address */
    tRt6RmapComm       *pCommunity;/*pointer to route-map community structure*/
    UINT4               u4Index;     /* The index to the interface table
                                      * telling on which logical interface
                                      * the route was learnt
                                      */
    UINT4               u4Metric;    /* Metrics associated with the route */
    UINT4               u4SetFlag;    /* Metrics associated with the route */
    UINT4               u4RowStatus; /* Status of the route. */
    UINT4               u4ChangeTime;/* The time of change of this entry */
    UINT4               u4RouteTag;  /* Route Tag used for BGP-IGP
                                      * Redistribution. The Lower 2 Bytes
                                      * specifies the next-hop AS number
                                      * and the first 2 Bytes specifies
                                      * the TAG value.
                                      */
    INT4                i4RMapFlag; /*Used to check if routemap is added to route*/
    UINT2               u2RedisMask; /* Info about the protocols to which this
                                      * route has been redistributed. */
    UINT1               u1BitMask;   /* Bit mask for controlling route
                                      * redistribution.
                                      */
    UINT1               u1Prefixlen; /* The number of significant bits in 
                                      * the prefix
                                      */
    INT1                i1Type;      /* To tell that the route is learnt 
                                      * DIRECT or INDIRECT means
                                      */
    INT1                i1Proto;     /* Learnt through which protocol 
                                      * - STATIC or LOCAL or Dynamic Routing
                                      * protocols
                                      */
    INT1                i1AddrCnt;   /* To tell the number of Direct routes
                                      * with similar prefix.
                                      */
    INT1                i1Refcnt;    /* To tell the number of places this
                                      * route entry is present.
                                      */
    INT1                i1DefRtrFlag; /* Default Route Type */   
    UINT1               u1Preference; /* Contains preference value*/
    UINT1               u1MetricType; /* OSPF path type, The Same is used for ISIS 
                                         Level Info L1 or L2*/
    UINT1               u1Flag;       /* Flag used to mark stale entries 
                                         during graceful restart */
    UINT1               u1NHpreference; /* Preference for nexthop */
    UINT1               u1AddrType;     /* Unicast/Anycast destination address */
    UINT1               u1HwStatus;     /* Hardware status. Whether present in
                                         * hardware or not */
    INT1                i1MetricType5;  /*used for metrictype5 */
    UINT4               u4Flag;
    UINT4               u4HwIntfId[2];/*To store Hardware IntfId for active and Standby*/
    UINT4               au4Community[RMAP_MAX_COMM];
    UINT1               u1EcmpCount;/*Used for evaluating ECMP route count*/
    UINT1              u1NullFlag;
    UINT1               u1CommCount;
    UINT1               u1IfType;
}
tIp6RtEntry;

#ifdef  TUNNEL_WANTED
/*
 * The following strcuture defines a tunnel interface [RFC 1933].
 * Currently, the software supports only IPv4 tunnels (i.e only
 * IPv6-over-IPv4 tunneling is supported) but the structure is generic
 * to support IPv6 tunnels also.
 */
typedef struct _IP6_TUNNEL_INTERFACE
{
    /* configurable parameters */
    tIp6Addr  tunlSrc;       /* Tunnel source endpoint address,
                              * for a IPv4 tunnel, the last 4
                              * bytes are used */
    tIp6Addr  tunlDst;       /* Tunnel dest endpoint address,
                              * for a IPv4 tunnel, the last 4
                              * bytes are used */
#define  IPV6_OVER_IPV4_TUNNEL    TNL_TYPE_IPV6IP   /* Configured IPv4 Tunnel */
#define  IPV6_OVER_IPV6_TUNNEL    TNL_TYPE_OTHER    /* Configured IPv6 Tunnel */
#define  IPV6_AUTO_COMPAT         TNL_TYPE_COMPAT   /* Automatic Compatible
                                                     * Tunnel */
#define  IPV6_SIX_TO_FOUR         TNL_TYPE_SIXTOFOUR /* Automatic 6to4 Tunnel */
#define  IPV6_GRE_TUNNEL          TNL_TYPE_GRE      /* Configured GRE Tunnel */
#define  IPV6_ISATAP_TUNNEL       TNL_TYPE_ISATAP   /* ISATAP Tunnel */

    UINT2 u2HopLimit;        /*Hop Limit for tunnel*/
    UINT1 u1TunlType;        /* Type of tunnel - For Fs Code, Tunnel Type
                              * is as defined in inc/cfa.h. */
    UINT1 u1TunlFlag;        /* Flag Defining whether tunnel
                              * is unidirectional or bidirectional.
                              * if 1 = Unidirectional.
                              * else if 2 = Bidirectional.
                              */
    UINT1 u1TunlDir;         /* Incase if tunnel is unidirectional 
                              * flag defining it is out going or incoming.
                              * if 1 = Incoming
                              * else if 2 = Outgoing.
                              */
    UINT1 u1TunlEncaplmt;    /* Encapsulation limit for tunnels*/
    UINT1 u1EncapFlag;       /* Flag indicating if Encapsulation option needs
                              * to be added or not in a tunnel packet
                              */
#define IPV6_ENCAP_OPT_YES  1
#define IPV6_ENCAP_OPT_NO   2
#define IPV6_DEF_ENCAP_LMT  4
   
    UINT1 u1Reserved[1];
}
tIp6TunlIf;
#endif /* TUNNEL_WANTED */

/*The following structure is used for creating policy table (RFC 3484)*/
typedef struct _IP6_ADDR_SEL_POLICY
{
     tRBNodeEmbd     PolicyPrefixRbNode;
     tIp6Addr        Ip6PolicyPrefix;          /* IP6 Prefix Address */
     UINT4           u4IfIndex;                /* Interface of the prefix */
     UINT1           u1PrefixLen;              /* Prefix Length */
     UINT1           u1Scope;                  /* Scope of the prefix */
     UINT1           u1Label;                  /* Label of the prefix */
     UINT1           u1Precedence;             /* Precedence value */
     UINT1           u1AddrType;               /*Type of the address*/
     UINT1           u1IsSelfAddr;             /* Identifies whether it refers the source address or destination address*/
     UINT1           u1IsPublicAddr;           /* Identifies whether the address referes to public address or private address*/
     UINT1           u1ReachabilityStatus;     /* Identifies whether the prefix is reachable or not*/
     UINT1           u1CreateStatus;           /* Identifies whether the entry in the table is default or configurable */
     UINT1           u1RowStatus;              /* Address Selection table rowstatus */
     UINT1           u1Reserved[2];
}
tIp6AddrSelPolicy;

/*The following structure is used for source selection algorithm (RFC 3484)*/
typedef struct _IP6_SRC_ADDR
{
    tTMO_SLL_NODE SrcNode;                   
    tIp6Addr  srcaddr;                         /*Ipv6 source address */
    UINT4     u4IfIndex;                       /*Interface Index*/
    UINT1     u1CumulativeScore;               /*Cumulative Score  */
    UINT1     u1MatchLen;                      /*Matching prefixlen */
    UINT1     u1Reserved[2];
}
tIp6SrcAddr;


typedef struct _IP6_DST
{
    tIp6Addr  dstaddr;                       /* Ipv6 destination address */
    UINT4     u4IfIndex;                     /* Interface index*/
    UINT1     u1CumulativeScore;             /* Cumulative score */
    UINT1     u1PrefixLen;
    UINT1     au1Reserved[2];
}
tIp6DstInfo;


#define    IP6_INVALID_CXT_ID   0xFFFFFFFF
#define    IP6IF_INVALID_INDEX   0xFFFFFFFF
 
#define   IP6_ALLOC_CXT_ENTRY(x)\
          (x = (tIp6Cxt*) IP_ALLOCATE_MEM_BLOCK(gIp6GblInfo.i4Ip6CxtPoolId)) 

#define   IP6_FREE_CXT_ENTRY(x)\
          IP_RELEASE_MEM_BLOCK (gIp6GblInfo.i4Ip6CxtPoolId, x)
/* ---------------------- */
/* Prototype declarations */
/* ---------------------- */
#ifdef LNXIP6_WANTED
#define IPV6_OSPF_DEF_HDR_VAL (CRU_HTONL(0x60000000))
#endif

INT4 Ip6Lanif       PROTO ((UINT4 u4Port, UINT4 u4Speed, UINT4 u4HighSpeed, 
                            UINT1 u1Type, UINT1 u1AdminStatus,
                            UINT1 u1OperStatus,tIp6Addr pIp6Addr,UINT1 u1PrefixLenth));

INT4 Ip6ProtoInit   PROTO ((VOID));

INT4 Ip6LanifUpdate PROTO ((UINT4 u4port, UINT4 u4mtu, UINT4 u4Speed,
                            UINT4 u4ipv4_addr));
INT1 Ip6UtlGetUnnumIfEntry PROTO ((UINT4 u4IfIndex));


#ifdef TUNNEL_WANTED
INT4 Ip6TunlifUpdate PROTO ((UINT4 u4IfIndex, UINT1 u1TnlDir,
                             UINT1 u1TnlDirFlag, UINT1 u1EncapOption,
                             UINT1 u1EncapLimit, UINT1 u1HopLimit,UINT4 u4Mask, 
                             UINT4 * pu4LocalAddr, UINT4 * pu4RemoteAddr, 
                             UINT4 u4Type));
#endif
    
INT4 Udp6OpenInCxt       PROTO ((UINT4 u4ContextId, UINT1 u1Mode, 
                            INT4 i4SockDesc, UINT2 *pu2UPort, 
                            UINT1 au1TaskName[4], UINT1 au1QName[4],
                            tIp6Addr * pIp6Addr,  UINT2 *pu2RPort,
                            tIp6Addr * pIp6RemAddr, VOID (*rcv_fnc) ));
INT4
Udp6GetPortCBInCxt (UINT4 u4ContextId, UINT2 u2UPort, tIp6Addr * pIp6Addr,
                tUdp6Port ** pUdp6PortEntry, UINT1 *pu1Udp6CBMode);

INT4 Udp6CloseInCxt      PROTO ((UINT4 u4contextId,
                             UINT2 u2UPort, tIp6Addr * pIp6Addr));

INT4 Udp6SendInCxt       PROTO ((UINT4 u4ContextId,
                            UINT2 u2SrcPort, UINT2 u2DstPort,
                            UINT4 u4Len, UINT2 u2Index,
                            tIp6Addr * pSrc, tIp6Addr * pDst,
                            tCRU_BUF_CHAIN_HEADER * pBuf, 
                            UINT1 u1Hlim));

UINT4 Ip6GetMtu     PROTO ((UINT4 u4Index));

UINT1 Ip6GetIfType  PROTO ((UINT4 u4IfIndex));

UINT1 udp6GetNextIndexIpvxUdp6Table PROTO ((UINT2 u2LPort,
                                 UINT2 *u2NextLPort,
                                 tSNMP_OCTET_STRING_TYPE * pIp6LAddr,
                                 tSNMP_OCTET_STRING_TYPE * pIp6NextLAddr,
                                 UINT2 u2RPort, UINT2 *u2NextRPort,
                                 tSNMP_OCTET_STRING_TYPE * pIp6RAddr,
                                 tSNMP_OCTET_STRING_TYPE * pIp6NextRAddr));
tIp6Udp6Stats *
Udp6GetCurrCxtStatEntry (VOID);

tIp6Udp6Stats *
Udp6GetStatEntry (VOID);

#ifdef TUNNEL_WANTED
struct _IP6_TUNNEL_INTERFACE * Ip6GetTunlIf PROTO ((UINT4 u4TunlIfIndex));
#endif

INT4 Ip6PmtuRegHl   PROTO (( INT4 (*PmtuHandler)(UINT4,tIp6Addr,UINT4) ));

INT4 Ip6TaskEnqueuePkt         PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf, 
                                       UINT4 u4IfIndex,
                                       UINT1 *pu1MacAddr));

VOID Ip6RcvIcmpv4ErrMsgInCxt   PROTO ((UINT4 u4ContextId, 
                                       tCRU_BUF_CHAIN_HEADER *pBuf, 
                                       UINT4 u4Ipv4Src, UINT4 u4Ipv4Dst,
                                       INT1 i1Type, INT1 i1Code));

VOID Ip6RcvIcmpErrorPkt        PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf,
                                       tIcmp6Params *pIcmpParams));

INT4 Ip6RcvFromHl              PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf, 
                                       tHlToIp6Params *pParams));

INT4 Ip6SrcAddrForDestAddr     PROTO ((tIp6Addr *pDstAddr, tIp6Addr *pSrcAddr));
INT4 Ip6SrcAddrForDestAddrInCxt PROTO ((UINT4 u4ContextId, tIp6Addr *pDstAddr,
                                                           tIp6Addr *pSrcAddr));

INT4 Ip6IsOurAddr              PROTO ((tIp6Addr *pIp6Addr, UINT4 *pu4Index));

INT1 Ip6ifEntryExists          PROTO ((UINT4 u4Index));
INT4 Ip6GetHlProtocol          PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf, 
                                       UINT1 *pu1Nhdr, UINT2 *pu2Offset));

tIp6Addr * Ip6GetGlobalAddr    PROTO ((UINT4 u4Index, tIp6Addr * pDst));

tIp6Addr * Ip6GetLlocalAddr    PROTO ((UINT4 u4Index));

INT4 Ip6Secv6GetHlProtocol     PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                                       UINT1 *pu1Nhdr, UINT2 *pu2Offset));

tIp6Addr * Ip6Secv6GetGlobalAddr PROTO ((UINT4 u4Index, tIp6Addr * pAddr6));

INT4 Ip6Secv6IsOurAddr  PROTO  ((tIp6Addr * pIp6Addr, UINT4 *pu4Index));

INT4 Ip6IsOurAddrInCxt  PROTO ((UINT4 u4Context, tIp6Addr *pIp6Addr, UINT4 *pu4Index));



/* ------------------ */
/* Macro declarations */
/* ------------------ */

#define  IP6_MINUS_ONE  -1
#define  IP6_ZERO        0
#define  IP6_ONE         1
#define  IP6_TWO         2
#define  IP6_THREE       3
#define  IP6_FOUR        4
#define  IP6_FIVE        5
#define  IP6_SIX      6
#define  IP6_EIGHT       8
#define  IP6_NINE        9
#define  IP6_SIXTEEN     16
#define  IP6_FIFTY       50
#define  IP6_HUNDRED     100
#define  IP6_THOUSAND    1000
#define  IP6_EIGHT_OCTETS 64 

/* Used to set all 1's in a byte  */
#define IP6_UINT1_ALL_ONE        0xFF



/* Address related macros */
#define IS_ADDR_UNSPECIFIED(a) ( (a).u4_addr[3] == 0 && \
                                 (a).u4_addr[2] == 0 && \
                                 (a).u4_addr[1] == 0 && \
                                 (a).u4_addr[0] == 0 )

#define SET_ADDR_UNSPECIFIED(a) {(a).u4_addr[0] = 0;\
                                 (a).u4_addr[1] = 0;\
                                 (a).u4_addr[2] = 0;\
                                 (a).u4_addr[3] = 0;}
#define IS_ADDR_LLOCAL(a)      \
   (((a).u4_addr[0] & OSIX_HTONL(0xFFC00000)) == OSIX_HTONL(0xFE800000))

#define IS_ADDR_MULTI(a)       \
   (((a).u4_addr[0] & OSIX_HTONL(0xff000000)) == OSIX_HTONL(0xff000000))

#define IS_ADDR_BROAD(a)       \
             (((a).u4_addr[0] & OSIX_HTONL(0xffffffff)) == OSIX_HTONL(0xffffffff))

#define IS_ADDR_LINK_SCOPE_MULTI(a)       \
   (((a).u4_addr[0] & OSIX_HTONL(0xff020000)) == OSIX_HTONL(0xff020000))

/* 6to4 address or not: RFC 3056 */
#define IS_ADDR_6to4(a) ((a).u2_addr[0] == OSIX_HTONS(0x2002)) 

/* To find whether IPv4 address belongs to private address space or not */
#define IS_IP_ADDR_PRIVATE(a) \
            ((((((a) >= 0x0a000000) && ((a) <= 0x0affffff)) || \
               (((a) >= 0xac100000) && ((a) <= 0xac10ffff)) || \
               (((a) >= 0xc0a80000) && ((a) <= 0x0c0a8ffff))) ? \
               OSIX_TRUE : OSIX_FALSE))
        
#define IS_ADDR_LOOPBACK(a)    ( (a).u4_addr[3] == OSIX_HTONL(1) && \
                                 (a).u4_addr[2] == 0 && \
                                 (a).u4_addr[1] == 0 && \
                                 (a).u4_addr[0] == 0 )

#define IS_ADDR_V4_COMPAT(a)   ( (a).u4_addr[3] != 0 && \
                                 (a).u4_addr[2] == 0 && \
                                 (a).u4_addr[1] == 0 && \
                                 (a).u4_addr[0] == 0 )
#define IS_ADDR_ISATAP(a)      ( (a).u4_addr[3] != 0 && \
                                 (a).u2_addr[5] == CRU_HTONS(0x5efe) && \
                                 (((a).u2_addr[4] == 0) || \
                                  ((a).u2_addr[4] == CRU_HTONS(0x0200))) )
#define IS_ALL_ROUTERS(a) (((a).u4_addr[0] == CRU_HTONL(0xff020000)) && \
                               ((a).u4_addr[1] == 0)          && \
                               ((a).u4_addr[2] == 0)          && \
                               ((a).u4_addr[3] == CRU_HTONL(9)))

#define IS_RESERVED_MULTI(a)  \
   ((a).u4_addr[1] == 0 && (a).u4_addr[2] == 0 && (a).u4_addr[3] == 0 \
   && (((a).u4_addr[0] >> 16) >= CRU_HTONS(0xff00)) \
   && (((a).u4_addr[0] >> 16) <= CRU_HTONS(0xff0f)))

#define IS_CONSTANT_MULTI(a)  \
  (IS_RESERVED_MULTI(a) || IS_ALL_NODES_MULTI(a) || IS_ALL_ROUTERS_MULTI(a) ||\
   IS_ALL_ROUTERS(a))

#define IS_ADDR_SOLICITED(a)   (((a).u4_addr[0] == CRU_HTONL(0xff020000)) && \
                                ((a).u4_addr[1] == 0)          && \
                                ((a).u4_addr[2] == CRU_HTONL(1)))

#define GET_ADDR_SOLICITED(a,b) {(b).u4_addr[0] = CRU_HTONL(0xff020000) ;\
                                 (b).u4_addr[1] = 0          ;\
                                 (b).u4_addr[2] = CRU_HTONL(1) ;\
                                 (b).u1_addr[12] = 0xff;\
                                 (b).u1_addr[13] = (a).u1_addr[13] ;\
                                 (b).u2_addr[7] = (a).u2_addr[7] ; }

#define SET_ALL_NODES_MULTI(a)   {(a).u4_addr[0] = CRU_HTONL(0xff020000) ;\
                                  (a).u4_addr[1] = 0 ;\
                                  (a).u4_addr[2] = 0 ;\
                                  (a).u4_addr[3] = CRU_HTONL(1) ; }

#define IS_ALL_NODES_MULTI(a)    (((a).u4_addr[0] == CRU_HTONL(0xff020000)) && \
                                  ((a).u4_addr[1] == 0 )         && \
                                  ((a).u4_addr[2] == 0 )         && \
                                  ((a).u4_addr[3] == CRU_HTONL(1)))

#define SET_ALL_ROUTERS_MULTI(a) {(a).u4_addr[0] = CRU_HTONL(0xff020000) ; \
                                  (a).u4_addr[1] = 0 ;\
                                  (a).u4_addr[2] = 0 ;\
                                  (a).u4_addr[3] = CRU_HTONL(2) ; }

#define IS_ALL_ROUTERS_MULTI(a) (((a).u4_addr[0] == CRU_HTONL(0xff020000))  && \
                                 ((a).u4_addr[1] == 0 )          && \
                                 ((a).u4_addr[2] == 0 )          && \
                                 ((a).u4_addr[3] == CRU_HTONL(2)))

#define IS_RESERVED_LINK_SCOPE_MULTI(a)  (((a).u4_addr[0] == CRU_HTONL(0xff020000)) && \
                                  ((a).u4_addr[1] == 0 )         && \
                                  ((a).u4_addr[2] == 0 )         && \
                                  ((((a).u4_addr[3] >= CRU_HTONL(1)) && \
                                  ((a).u4_addr[3] <= CRU_HTONL(0x0000000E))) || \
                                  ((a).u4_addr[3] == CRU_HTONL(0x00010001)) ||\
                                  ((a).u4_addr[3] == CRU_HTONL(0x00010002))))

#define IS_RESERVED_NODE_SCOPE_MULTI(a)  (((a).u4_addr[0] == CRU_HTONL(0xff010000)) && \
                                  ((a).u4_addr[1] == 0 )         && \
                                  ((a).u4_addr[2] == 0 )         && \
                                  (((a).u4_addr[3] == CRU_HTONL(1)) || \
                                  ((a).u4_addr[3] == CRU_HTONL(2))))

#define IS_ALL_INVALID_MULTI(a)        (((a).u4_addr[0] == CRU_HTONL(0xfff00000)) || \
                                       ((a).u4_addr[0] == CRU_HTONL(0xff400000)) || \
                                       ((a).u4_addr[0] == CRU_HTONL(0xff200000)) || \
                                       ((a).u4_addr[0] == CRU_HTONL(0xff500000)) || \
                                       ((a).u4_addr[0] == CRU_HTONL(0xff600000)) || \
                                       ((a).u4_addr[0] >= CRU_HTONL(0xff800000)))

#define IS_ALL_PREFIX_SSM_MULTI(a)    ((CRU_HTONL ((a)->u4_addr[0]) & 0xff300000) == (0xff300000))
#define IS_ALL_PREFIX_MULTI(a)        ((((a)->u4_addr[0] >> 12) & 0x3) == (0x3))
#define IS_ALL_RP_PREFIX_MULTI(a)     ((((a)->u4_addr[0] >> 12) & 0x7) == (0x7))
#define IS_IP6_RIID_IN_RANGE(a)       ((a < 1) || (a > 15))

#define IP6_GET_UNICAST_PREFIXLEN_FROM_MCASTADDR(a)        ((a)->u4_addr[0] >> 24)
#define IP6_GET_RIID_FROM_MCAST_ADDR(a)                    ((a)->u4_addr[0] >> 16)

#define GET_EUI_64_IF_ID(a,b)   b[0]=a[0];             \
                                b[1]= a[1];            \
                                b[2]=a[2];             \
                                b[3]=0xff;             \
                                b[4]=0xfe;             \
                                b[5]=a[3];             \
                                b[6]= a[4];            \
                                b[7]=a[5];             \
                                (b[0]=b[0]^(0x2)) ;


#define  IP6_FAILURE                -1          
#define  IP6_SUCCESS                 0
#define  IP6_ACCEPTED                1

#define  IP6_NO_ROOM                 1
#define  IP6_ROUTE_FOUND             2

#define  IP6_EXACT_ROUTE             1
#define  IP6_BEST_ROUTE              2
#define  IP6_ROUTE_NOT_FOUND         3

/* IP6 Protocol Value in GRE Header */
#define  IP6_TUNNEL_INTERFACE_TYPE  0x83
#define  IP6_GRE_PTCL              CFA_ENET_IPV6
                                
#define  IP6_ADMIN_DOWN             0x02        

#define  IP6_LANIF_OPER_UP          CFA_IF_UP   
#define  IP6_LANIF_OPER_DOWN        CFA_IF_DOWN 

#define  IP6_MULTICAST_LEAVE        0x07
#define  IP6_MULTICAST_JOIN         0x06


#define  IP6_LANIF_MTU_CHANGE       0x05
#define  IP6_LANIF_ENTRY_VALID      0x04  /* Same as ADMIN_VALID */
#define  IP6_LANIF_ENTRY_INVALID    0x03  /* Same as ADMIN_INVALID */
#define  IP6_LANIF_ENTRY_DOWN       0x02  /* Same as ADMIN_DOWN */
#define  IP6_LANIF_ENTRY_UP         0x01  /* Same as ADMIN_UP */

#define  IP6_ADDR_MIN_PREFIX             0
#define  IP6_ADDR_MAX_PREFIX             128

#define  IP6_LABEL_MIN    0
#define  IP6_LABEL_MAX    255 
#define IPV6_ADDR_SEL_DEFAULT_POLICY_LABEL 2

#define  IP6_PRECEDENCE_MIN   1
#define  IP6_PRECEDENCE_MAX   128 
#define IPV6_ADDR_SEL_DEFAULT_POLICY_PRECEDENCE 30
#define  IP6_ADDR_MIN_PROFILES   0
#define  IP6_ADDR_SIZE_IN_WORDS          4
#define  IP6_ADDR_SIZE_IN_BITS           128
#define  IP6_BITS_IN_OCTET               8

#define   RTM6_IP6_ROUTE_MAX_DISTANCE             255
#define   RTM6_IP6_ROUTE_MIN_DISTANCE             1

/* IPv6 default MTU equals to CFA's ENET MTU */
#define  IP6_DEFAULT_MTU                 CFA_ENET_MTU        

/* IPv6 default MTU equals to CFA's ENET SPEED */
#define  IP6_DEFAULT_SPEED               10000000  /* 10 MPS */
#define  IP6_DEFAULT_HIGH_SPEED          0

#define  MCAST_MAC_ADDR_OCTET_1     0x33
#define  MCAST_MAC_ADDR_OCTET_2     0x33

#define  IP6_DEFALUT_HDR_VALUE                CRU_HTONL(0x60000000)


#define IPV6_OFF_PAYLOAD_LEN             4
#define IPV6_HEADER_VERSION              6   /* IPV6 vresion */
#define IPV6_HEADER_LEN                  40
#define IPV6_HOP_BY_HOP_HEADER_LEN       8
#define IP6_OFFSET_FOR_NEXT_HDR_FIELD    6   /* Next Header field offset */
#define IP6_OFFSET_FOR_HOPLIMIT_FIELD    7   /* Hoplimit field offset */
#define IP6_OFFSET_FOR_SRCADDR_FIELD     8
#define IP6_OFFSET_FOR_DESTADDR_FIELD    24
#define IP6_ADDR_SIZE                    16

#define IP6_DEF_HOP_LIMIT                64
#define IP6_MAX_HLIM                    255

/* ICMPv6 Destination Unreachable Message Codes */

#define ICMP6_DEST_UNREACHABLE            1  
#define ICMP6_NO_ROUTE_TO_DEST            0 
#define ICMP6_COMM_ADM_PROHIBITED         1 
#define ICMP6_NOT_NEIGHBOUR               2 
#define ICMP6_ADDRESS_UNREACHABLE         3 
#define ICMP6_PORT_UNREACHABLE            4 
#define ICMP_SOURCE_ADDRESS_FAILED        5
#define ICMP_DESTINATION_ROUTE_REJECT     6
#define ICMP6_REDIRECT_ENABLE             1
#define ICMP6_REDIRECT_DISABLE            2

#define  IP6_LAYER4_DATA                 03
#define  IP6_RAW_DATA                    04

#define  IP6_MCAST_DATA_PACKET_FROM_MRM  05

#define  IP6_HDR_INC                     01

#ifdef TUNNEL_WANTED
#define IP6_TNL_CHG_MASK_DIR            0x01
#define IP6_TNL_CHG_MASK_DIR_FLAG       0x02
#define IP6_TNL_CHG_MASK_ENCAP_OPT      0x04
#define IP6_TNL_CHG_MASK_ENCAP_LMT      0x08
#define IP6_TNL_CHG_MASK_END_POINT      0x10
#define IP6_TNL_CHG_MASK_HOP_LIMIT      0x20
#endif

/* Route's Protocol Identifier */
#define  IP6_OTHER_PROTOID    1  /* Aggregate Routes */
#define  IP6_LOCAL_PROTOID    2  /* Local Interface */
#define  IP6_NETMGMT_PROTOID  3  /* Configured Static Route */
#define  IP6_NDISC_PROTOID    4  /* Redirect Routes */
#define  IP6_RIP_PROTOID      5  /* RIPNG Routes */
#define  IP6_OSPF_PROTOID     6  /* OSPF Routes */
#define  IP6_BGP_PROTOID      7  /* BGP Routes */
#define  IP6_IDRP_PROTOID     8  /* IDRP Routes */
#define  IP6_IGRP_PROTOID     9  /* IGRP Routes */
#define  IP6_PTP_PROTOID      10 /* PTP Routes */
#define  IP6_ISIS_PROTOID     9  /* ISIS Routes - This is as per stdipvx.mib 
                                   as there is no equavalent for isis in stdipv6.mib  */
 
#define  IP6_MAX_ROUTING_PROTOCOLS        9   /* As per ipv6RouteProtocol in
                                              * stdipv6.mib */
/* Protocol default Preference Value. !!! moved from ip6snmp.h */
#define IP6_PREFERENCE_OTHERS               255
#define IP6_PREFERENCE_LOCAL                1
#define IP6_PREFERENCE_NETMGMT              20
#define IP6_PREFERENCE_NDISC                25
#define IP6_PREFERENCE_RIP                  120
#define IP6_PREFERENCE_OSPF                 110
#define IP6_PREFERENCE_BGP                  105
#define IP6_PREFERENCE_IDRP                 120
#define IP6_PREFERENCE_IGRP                 100

/*other values are reserved for future use */
#define  IP6_TCP_PROTOID     20   
#define  IP6_DHCP6_SRV_PROTOID    21
#define  IP6_DHCP6_CLNT_PROTOID   22   
#define  IP6_DHCP6_RLY_PROTOID    23   

#define  IPV6_ID                     0x98 /* Till 150 - 0x96 is the standard values. 
                                             0x97 is used for IP_ID.
                                             This is proprietary for IPv6. */

/* The IP6 registration id for registering with VCM. 
 * As IP uses till 152 next value is assigned for IP6 task */
#define  IP6_REG_ID          153

/* Values for IP6 Route ADD/MODIFY/DEL */
#define IP6_ROUTE_ADD         1
#define IP6_ROUTE_DEL         2
#define IP6_ROUTE_MODIFY      3

#define  IP6_ROUTE_TYPE_OTHER        1   
#define  IP6_ROUTE_TYPE_DISCARD      2   
#define  IP6_ROUTE_TYPE_DIRECT       3   
#define  IP6_ROUTE_TYPE_INDIRECT     4

#define  IP6_DEF_RTR_FLAG            1 
#define  IP6_ROUTE_TYPE_REDIRECT     2

#define  IP6_PMTU_ENABLE           1   
#define  IP6_PMTU_DISABLE          2   

#define  IP6_RFC5095_COMPATIBLE      1   
#define  IP6_RFC5095_NOT_COMPATIBLE  2   
#define  IP6_RFC5942_COMPATIBLE      1   
#define  IP6_RFC5942_NOT_COMPATIBLE  2  

#define IP6_ECMP_MIN_PRT_INTERVAL 0
#define IP6_ECMP_MAX_PRT_INTERVAL 60

/* IPv6 Forwarding enable/disable on a node*/
#define IP6_FORW_ENABLE       1
#define IP6_FORW_DISABLE      2
/* IPv6 Forwarding enable/disable on an interface*/
#define IP6_IF_FORW_ENABLE       1
#define IP6_IF_FORW_DISABLE      2
/* ND Proxy Admin status enable/disable on an interface*/
#define ND6_IF_PROXY_ADMIN_UP    1
#define ND6_IF_PROXY_ADMIN_DOWN    2
/* ND Proxy Admin status enable/disable on an interface*/
#define ND6_PROXY_MODE_GLOBAL    1
#define ND6_PROXY_MODE_LOCAL    2
/* ND Proxy UpStream enable/disable on an interface*/
#define ND6_PROXY_IF_UPSTREAM    1
#define ND6_PROXY_IF_DOWNSTREAM    2
/* ND Proxy Operational status enable/disable on an interface*/
#define ND6_PROXY_OPER_UP    1
#define ND6_PROXY_OPER_UP_IN_PROG    2
#define ND6_PROXY_OPER_DOWN          3

/* SeND Prefix Check */
#define   ND6_PREFIX_CHECK_ENABLE   1
#define   ND6_PREFIX_CHECK_DISABLE  2

/* SeND Security Status*/
#define   ND6_SECURE_ENABLE         1
#define   ND6_SECURE_MIXED          3
#define   ND6_SECURE_DISABLE        2

/* SeND TimeStamp */
#define ND6_TIME_DELTA              1
#define ND6_TIME_FUZZ               2
#define ND6_TIME_DRIFT              3

/* SeND timestamp default values */
#define ND6_SECURE_DELTA_DEFAULT    300 
#define ND6_SECURE_FUZZ_DEFAULT     1
#define ND6_SECURE_DRIFT_DEFAULT    1

/* SeND DAD Status */
#define ND6_ACCEPT_UNSEC_ADV_ENABLE     1
#define ND6_ACCEPT_UNSEC_ADV_DISABLE    2

/*SeND Authorisation Type */
#define ND6_AUTH_CGA                0
#define ND6_AUTH_TA                 1
#define ND6_AUTH_CGA_TA             2
#define ND6_AUTH_ANYONE             3

/* Values for the type of address */
enum {
ADDR6_UNICAST  = 1,
ADDR6_ANYCAST, 
ADDR6_LLOCAL,
ADDR6_UNSPECIFIED,
ADDR6_MULTI,
ADDR6_V4_COMPAT,
ADDR6_INVALID,
ADDR6_LOOPBACK                   
};

#define   IPV6_MIN_AS_NUM                0
#define   IPV6_MAX_AS_NUM            65535
#define   ND6_SEND_ALL_COMPONENT   0x0001

/* Protocol ID for route added to IPv6 Fwd table */
#define   NDISC_ID                 IP6_NDISC_PROTOID
/* Following are ID for dynamic protocols updating IPv6
 * Forwarding Table. */
#define   RIPNG_ID                 IP6_RIP_PROTOID
#define   OSPF6_ID                 IP6_OSPF_PROTOID
#define   BGP6_ID                  IP6_BGP_PROTOID
#define   IDRP6_ID                 IP6_IDRP_PROTOID
#define   IGRP6_ID                 IP6_IGRP_PROTOID
#define   ISIS6_ID                 IP6_ISIS_PROTOID

/* these definitions are used by IpRtChgNotify () */
#define   IP6_BIT_NXTHOP       0x0001        /* Next hop        */
#define   IP6_BIT_INTRFAC      0x0002        /* Interface       */
#define   IP6_BIT_STATUS       0x0004        /* Route status    */
#define   IP6_BIT_METRIC       0x0008        /* Metric          */
#define   IP6_BIT_RT_TYPE      0x0010        /* Route Type      */
#define   IP6_BIT_GR           0x0020        /* Graceful Restart*/
#define   IP6_BIT_ALL          0xFFFF        /* the parameters  */

#define   IPV6_SET_BIT_FOR_CHANGED_PARAM(u2BitMask, ChangedBit) \
          (u2BitMask) |= (ChangedBit)       /* do logical OR and *
                                             *  take the result  */
#define   IPV6_GET_BIT_FROM_CHANGED_PARAM(u2BitMask, ChangedBit) \
          (u2BitMask) &= (ChangedBit)       /* do logical AND and *
                                             *  take the result  */

/* NETIPV4 Macros */

#define NETIPV6_ADD_ROUTE                1
#define NETIPV6_DELETE_ROUTE             2
#define NETIPV6_MODIFY_ROUTE             3

#define NETIPV6_EXACT_ROUTE              1
#define NETIPV6_BEST_ROUTE               2
#define NETIPV6_NO_ROUTE                 3

#define NETIPV6_SUCCESS                  0
#define NETIPV6_FAILURE                 -1

#define NETIPV6_ALL_PROTO               0xFFFFFFFF    

#define NETIPV6_ENABLED                  1
#define NETIPV6_DISABLED                 2   

/* Added for the pim scoped addr API -RFC4007 */
#define NETIPV6_INVALID_ZONE_NAME        1
#define NETIPV6_INVALID_ZONE_LEN         2
#define NETIPV6_INVALID_ZONE_ID          3

#define  ICMPV6_PROTOCOL_ID             58
/* Row status */
#define   IP6FWD_ROW_DOES_NOT_EXIST          0
#define   IP6FWD_ACTIVE                      1
#define   IP6FWD_NOT_IN_SERVICE              2
#define   IP6FWD_NOT_READY                   3
#define   IP6FWD_CREATE_AND_GO               4
#define   IP6FWD_CREATE_AND_WAIT             5
#define   IP6FWD_DESTROY                     6

/*RTM6 related macros*/

#define RTM6_RT_REACHABLE               0x01 /*Indicates whether the route is
                                              reachable best route */

#define RTM6_NOTIFIED_RT                0x02 /*In ECMP6 case,this flag indicates whether
                                              the route is  notified route*/

#define RTM6_RT_IN_PRT                  0x04 /* Indicates whether route exists */

#define RTM6_ECMP_RT                    0x08 /* Indicates whether the route is an
                                               ECMP6 route */

#define RTM6_RT_IN_ECMPPRT              0x10 /* Indicates whether the route is
                                               available in ECMP6PRT */

#define IP6_ECMP6PRT_TIMER_INTERVAL 60  /*Time Interval between two successive neighbor solicitations.*/

#define RTM6_DELETE_INSTALLED_RT       3

/*Macros needed for neighbor discovery related functions in RTM6*/
#define  ND6_NAME         "ND6"

#define IP6_ECMP6_PRT_RESOLVE_QUEUE  "ECMP6_Q"

#define IP6_MULTICAST_REG_QUEUE      "MC6_Q"


/* Macro definitions */
typedef   tCRU_BUF_CHAIN_HEADER        tIp6Buf;

#define IP6_CREATE_MEM_POOL(u4BlkSize, u4NumBlks, pPoolId) \
   MemCreateMemPool((u4BlkSize),              \
                    (u4NumBlks),              \
                    MEM_DEFAULT_MEMORY_TYPE,  \
                    (pPoolId))
    
#define   IP6_ALLOCATE_BUF(u4Size, u4ValidOffset)                 \
          CRU_BUF_Allocate_MsgBufChain((u4Size), (u4ValidOffset))

#define   IP6_RELEASE_BUF(pBuf, u1ForceFlag)                      \
          CRU_BUF_Release_MsgBufChain((pBuf), (u1ForceFlag))

#define   IP6_COPY_TO_BUF(pBuf, pSrc, u4Offset, u4Size)           \
          CRU_BUF_Copy_OverBufChain((pBuf),(UINT1 *)(pSrc),(u4Offset),(u4Size))

#define   IP6_GET_MODULE_DATA_PTR(pBuf) \
          &(pBuf->ModuleData)

#define   IP6_COPY_FROM_BUF(pBuf, pDst, u4Offset, u4Size)         \
          CRU_BUF_Copy_FromBufChain((pBuf),(UINT1 *)(pDst),(u4Offset), (u4Size))

#define   ND6_NS_SRC_MEM_POOL (gIp6GblInfo.i4Ip6Nd6NsSrcPoolId)

typedef struct _ScopeZoneInfo
{
    UINT1            au1ZoneName[IP6_SCOPE_ZONE_NAME_LEN];
    INT4             i4ZoneIndex;
    UINT1            u1Scope;
    UINT1            au1Pad[3];

}tIp6ZoneInfo;


typedef struct
{
    tIp6Addr  Ip6Addr;                        /* Interface Link Local Addres */
    UINT4     u4IfIndex;                      /* Physical Interface Index */
    UINT4     u4Mtu;                          /* Interface Path MTU */
    UINT4     u4IfSpeed;                      /* Speed of the Interface */
    UINT4     u4IfHighSpeed;                  /* High Speed of the Interface */
    UINT4     u4Admin;                        /* Administration Status */
    UINT4     u4Oper;                         /* Operational Status */
    UINT4     u4InterfaceType;                /* Interface Type */
    UINT4     u4IpPort;                      /* Physical Interface Index */
    UINT4     u4AddressLessIf;                /**IfIndex of Associated IP*/
    UINT1     au1IfName[IP6_MAX_IF_NAME_LEN]; /* Interface Name */
}
tNetIpv6IfInfo;

typedef struct
{
    tIp6Addr  Ip6Addr;                        /* Interface IPv6 Address */
    UINT4     u4PrefixLength;                 /* Address Mask */
    UINT4     u4Type;                         /* Address Type */
}
tNetIpv6AddrInfo;

typedef struct
{
    tCRU_BUF_CHAIN_HEADER    *pBuf;           /* Data Buffer */
    tIp6Addr                 Ip6SrcAddr;      /* Source address */
    UINT4                    u4PktLength;     /* Packet Length */
    UINT4                    u4Index;         /* Interface Index */
    UINT4                    u4Type;          /* Address Type */
}
tNetIpv6AppRcv;

typedef struct
{
    tNetIpv6AddrInfo         Ipv6AddrInfo;    /* IP6 Address Information */
    UINT4                    u4Index;         /* Interface Index */
    UINT4                    u4Mask;          /* Address Status Mask */
#define NETIPV6_ADDRESS_ADD            0x1
#define NETIPV6_ADDRESS_DELETE         0x2
}
tNetIpv6AddrChange;

typedef struct
{
    tIp6Addr            Ip6Dst;     /* The destination address prefix */
    tIp6Addr            NextHop;    /* The next hop address */
    UINT4               u4ContextId; /* RTM6 Context ID */
    UINT4               u4Index;    /* The index to the interface table
                                     * telling on which logical interface
                                     * the route is reachable.
                                     */ 
    UINT4               u4Metric;   /* Metric Associated with this route. */
    UINT4               u4SetFlag;    /* Metrics associated with the route */
    UINT4               u4RowStatus;/* Status of the route. */
    UINT4               u4RouteTag; /* Route Tag used for BGP-IGP
                                     * Redistribution.
                                     */
    UINT4               u4ChangeTime;/* The time of change of this entry */
    UINT4               u4IsPDRoute;
    INT4                i4RMapFlag; /*To check if routemap is applied to route*/
    UINT2               u2ChgBit;   /* Change status indicating the attribute
                                     * that has been modified. */
    UINT1               u1Prefixlen;/* The number of significant bits in
                                     * the prefix
                                     */
    INT1                i1Proto;    /* Protocol from which this route is
                                     * learnt. */
    INT1                i1Type;     /* Route Type */
    INT1                i1DefRtrFlag; /* Default Route Status */
    UINT1               u1Preference; /* Contains preference value */
    UINT1               u1MetricType; /* OSPF path type */
                                      /* the same is used for ISIS Level Info*/
    UINT1               u1AddrType; /*UNICAST or ANYCAST*/
    UINT1               u1HwStatus;     /* Hardware status. Whether present in
                                         * hardware or not */
    INT1                i1MetricType5; /*used for metric type 5 */
    UINT1               u1EcmpCount;/*Used for evaluating ECMP route count*/
    UINT4               u4HwIntfId[2];/*To store Hardware IntfId for active and Standby*/
    tRt6RmapComm       *pCommunity;/*pointer to route-map community structure*/
    UINT1               u1CommCount;               /* Community Count */
     UINT1               u1NullFlag;
    UINT1               u1Pad[2];


}
tNetIpv6RtInfo;

typedef struct
{
    tIp6Addr            Ip6Dst;               /* Destination Address Prefix */
    UINT4               u4ContextId; /* RTM6 Context ID */
    UINT2               u2AppIds;             /* Protocol ID */
    UINT1               u1Prefixlen;          /* Significant bits in prefix */
    UINT1               u1QueryFlag;          /* Best or Exact route */
#define NETIPV6_QUERIED_FOR_NEXT_HOP    0x01  /* Best Match */
#define NETIPV6_QUERIED_FOR_SYNC        0x02  /* Exact Match */
}
tNetIpv6RtInfoQueryMsg;

typedef struct
{
    UINT4    u4Index;                         /* Interface Index */
    UINT4    u4IpPort;                        /* IP Port number */ 
    UINT4    u4Mtu;                           /* Interface Path MTU  Change */
    UINT4    u4IfSpeed;                       /* Speed of Interface Change */
    UINT4    u4IfStat;                        /* Create or Delete Interface */
    UINT4    u4OperStatus;                    /* Operational Status Change */
    UINT4    u4Mask;                          /* Interface Status Mask */
#define NETIPV6_INTERFACE_MTU_CHANGE           0x1
#define NETIPV6_INTERFACE_SPEED_CHANGE         0x2
#define NETIPV6_INTERFACE_STATUS_CHANGE        0x4
#define NETIPV6_INTERFACE_HIGH_SPEED_CHANGE    0x8
}
tNetIpv6IfStatChange;

typedef struct
{
    tCRU_BUF_CHAIN_HEADER    *pBuf;           /* Data Buffer */
    tIp6Addr                 Ip6Addr;         /* IP6 Address */
    UINT4                    u4Type;          /* ICMP Error Type */
    UINT4                    u4Code;          /* ICMP Error Code */
    UINT4                    u4PktLength;     /* Packet Length */
    UINT4                    u4Index;         /* Interface Index */
}
tNetIcmpv6ErrorSend;


/* Structure added to initmate the non-global zone 
   creation and non-global zone deletion to PIM RFC4007*/
typedef struct
{
    INT4     i4ZoneIndex;       /* Zone Index for which the status change 
                                   is received */
    UINT4    u4ZoneMask;        /* Type of Zone status change */
    UINT4    u4IfIndex;         /* If Index on which the zone status 
                                   is changed */

#define NETIPV6_INTERFACE_MAPPED_TO_ZONE       0x01 
#define NETIPV6_INTERFACE_UNMAPPED_FROM_ZONE   0x02 
#define NETIPV6_NON_GLOBAL_ZONE_CREATION       0x03
#define NETIPV6_NON_GLOBAL_ZONE_DELETION       0x04
#define NETIPV6_FIRST_NON_GLOBAL_ZONE_ADD      0x05
#define NETIPV6_LAST_NON_GLOBAL_ZONE_DEL       0x06
}
tNetIpv6ZoneChange;

typedef struct
{
    union
    {
        tNetIpv6AppRcv       AppRcv;          /* Application Send & Receive */
        tNetIpv6AddrChange   AddrChange;      /* Address status change */
        tNetIpv6RtInfo       RouteChange;     /* Route status change */
        tNetIpv6IfStatChange IfStatChange;    /* Interface status change */
        tNetIcmpv6ErrorSend  Icmpv6ErrorSend; /* ICMPv6 packet update */
        tNetIpv6ZoneChange   ZoneChange;      /* Zone Change Update -RFC 4007*/
    } unIpv6HlCmdType;
    UINT4  u4Command;                         /* HLI Interface Command */
#define NETIPV6_APPLICATION_RECEIVE        0x1
#define NETIPV6_ADDRESS_CHANGE             0x2
#define NETIPV6_ROUTE_CHANGE               0x4
#define NETIPV6_INTERFACE_PARAMETER_CHANGE 0x8
#define NETIPV6_ICMPV6_ERROR_SEND          0x10
#define NETIPV6_ZONE_CHANGE                0x12 /*Zone Change Indication- RFC4007 */
}
tNetIpv6HliParams;

#define  ND6_DEFAULT_ROUTER         0x80000000  /* Router flag */
#define  ND6_SOLICITED_FLAG         0x40000000  /* Solicited flag */
#define  ND6_OVERRIDE_FLAG          0x20000000  /* Override flag */

#define  IP6_MAX_LLA_LEN             6/* Max LinkLayerAddr Len */

#define ND6_SOLICITED_MSG           1   /* sending response to solicited */
#define ND6_UNSOLICITED_MSG         2   /* sending unsolicited advertisements */
#define ND6_PROXY_MSG               3   /* ND messages proxy exchanged */

#define ND6_SEND_MSG                0x10   /* sending solicited requests */
#define ND6_SEND_ADD          0x20   /* sending CPS and CPA messages */
#define ND6_SEND_RS_MSG       0x30
#define ND6_SEND_SOL_RA_MSG   0x40
#define ND6_SEND_RA_MSG          0x50
#define ND6_SEND_NA_MSG          0x60

/* 
 * ND6_SEND_OPTIONS_LENGTH  is based on the following
 *  -> CGA         - 192 Bytes (Variable)
 *  -> RSA         - 152 Bytes (Variable)
 *  -> Timestamp   -  16 Bytes
 *  -> Nonce       -   8 Bytes 
 *                   
 *  Any changes in the SEND options length
 *  should result in change in the below value
 */
#define ND6_SEND_OPTIONS_LENGTH     512 
#define ND6_SEND_SOL_OPT_COUNT       4
#define ND6_SEND_UNSOL_OPT_COUNT      3 

/*
 * ND6_SEND_CPA_LENGTH is based on the following
 * TA   - 48 Bytes (Variable)
 * Certificiate - 704 Bytes (Variable)
 * 
 * Any changes should result in change in the
 * below value
 */

#define ND6_SEND_CPS_LENGTH    128
#define ND6_SEND_CPA_LENGTH       1024

/* ND Message Constants */
#define  ND6_ROUTER_SOLICITATION     133     /* Router Solicitation */
#define  ND6_ROUTER_ADVERTISEMENT    134     /* Router Advertisement */
#define  ND6_NEIGHBOR_SOLICITATION   135     /* Neighbor Solicitation */
#define  ND6_NEIGHBOR_ADVERTISEMENT  136     /* Neighbor Advertisement */
#define  ND6_REDIRECT                137     /* Redirect */

/* Secure ND Message Constants */
#define  ND6_SEND_CPS                148    /* Certificate Path Solicitaion */
#define  ND6_SEND_CPA                149    /* Certificate Path Advertisement */
#define  ND6_SEND_TIMESTAMP           13    /* TimeStamp Option */
#define  ND6_SEND_NONCE               14    /* Nonce option */
#define  ND6_SEND_TRUST_ANCHOR        15    /* Trust Anchor option */
#define  ND6_SEND_CERTIFICATE         16    /* Certificate  option */
#define  ND6_SEND_TA_NAMETYPE          1    /* DER X.501 name*/
#define  ND6_SEND_CERT_NAMETYPE        1    /* DER X.501 name*/
#define  ND6_TA_MAX_NAME_LEN   128

/* ND Extension Constants */
#define  ND6_EXT_HDR_LEN               2     /* Type, Len ; 1 byte each*/

#define  ADV_INTERVAL_OPTION           7 
#define  HA_INFO_OPTION                8
#define  ND6_SRC_LLA_EXT               1     /* Source link layer option */
#define  ND6_TARG_LLA_EXT              2     /* Target link layer option */
#define  ND6_PREFIX_EXT                3     /* Prefix option */
#define  ND6_REDIRECT_EXT              4     /* Redirect extension option */
#define  ND6_MTU_EXT                   5     /* MTU option */

#define  ND6_ADV_INTERVAL_OPT          7
#define  ND6_SEND_CGA_OPT              11
#define  ND6_SEND_RSA_SIGN_OPT         12
#define  ND6_SEND_TIMESTAMP_OPT        13
#define  ND6_SEND_NONCE_OPT            14
#define  ND6_SEND_TA_LEN               6  /*  TA option length */
#define  ND6_SEND_CERT_LEN             88  /* Cert  option length */

/* RFC 4191 Constants */
#define ND6_RA_ROUTE_INFO_EXT          24 

#define  ND6_ADV_INTERVAL_OPTION_LEN   1  /* Adv Interval option length */
#define  ND6_HA_INFO_OPTION_LEN        1  /* HA Info option length */
#define  ND6_LLA_EXT_LEN               1  /* Link layer option length */
#define  ND6_PREFIX_EXT_LEN            4  /* Prefix option length */
#define  ND6_MTU_EXT_LEN               1  /* MTU option length */
#define  ND6_PREFIX_CNT                4  /* multiple prefix options in a packet */

#define  ND6_RSVD_CODE                 0     /* ICMP Code for ND */

#define  ND6_CACHE_ENTRY_STATIC      1 
#define  ND6_CACHE_ENTRY_REACHABLE   2 
#define  ND6_CACHE_ENTRY_INCOMPLETE  3 
#define  ND6_CACHE_ENTRY_STALE       4 
#define  ND6_CACHE_ENTRY_DELAY       5 
#define  ND6_CACHE_ENTRY_PROBE       6 
#define  ND6_CACHE_ENTRY_EVPN        7



/* RARoutInfo Table Various RowStatus */
#define   IP6_RA_ROUTE_ROW_STATUS_ACTIVE             1 
#define   IP6_RA_ROUTE_ROW_STATUS_NOT_IN_SERVICE     2 
#define   IP6_RA_ROUTE_ROW_STATUS_NOT_READY          3
#define   IP6_RA_ROUTE_ROW_STATUS_CREATE_AND_GO      4
#define   IP6_RA_ROUTE_ROW_STATUS_CREATE_AND_WAIT    5
#define   IP6_RA_ROUTE_ROW_STATUS_DESTROY            6

/* RFC 4191 Def Router Preferenfe Macros */
#define IP6_RA_ROUTE_PREF_LOW         0
#define IP6_RA_ROUTE_PREF_MED         1
#define IP6_RA_ROUTE_PREF_HIGH        2 
#define IP6_RA_MAX_RIO_PER_INT        17
#define ND6_MAX_RA_ROUTE_LENGTH       24

#define IP6_RA_ROUTE_MAX_LIFETIME      0xffffffff

#define MAX_IP6_PREF_TIME_LEN      9
#define MAX_IP6_RA_PREFERENCE      10


typedef struct
{
    VOID  (*pAppRcv)      (tNetIpv6HliParams *);
    VOID  (*pAddrChange)  (tNetIpv6HliParams *);
    VOID  (*pRouteChange) (tNetIpv6HliParams *);
    VOID  (*pIfChange)    (tNetIpv6HliParams *);
    VOID  (*pIcmp6ErrSnd) (tNetIpv6HliParams *);
    VOID  (*pZoneChange)  (tNetIpv6HliParams *); /* Function pointer to initmate                                                                                     zone change indication to Hlayers */    
    UINT1 u1NumOfReg;
    UINT1 au1Reserved[3];
} tNetIpv6RegTbl;

typedef struct
{
    VOID (*pAppRcv)     ( tCRU_BUF_CHAIN_HEADER   * );
    UINT1     u1RegFlag;
    UINT1     u1RegCntr;
    UINT2     u2AlignmentByte;    /* 2 bytes to ensure a 4-byte boundary */
} tNetIpv6MCastProtoRegTbl;

typedef struct _NetIp6McastInfo 
{ 
    UINT4 u4IfIndex;          /*Interface index */ 
    UINT2 u2McastProtocol;    /*Multicast Protocol:PIM_ID/ICMPV6*/ 
    UINT1 u1BytePad[2]; 
}tNetIp6McastInfo; 
 
typedef struct _NetIp6Oif 
{ 
    UINT4  u4IfIndex;        /* Interface index  */ 
    UINT2  u2HopLimit;       /* HopLimit value */ 
    UINT1  u1BytePad[2]; 
}tNetIp6Oif; 
 
typedef struct _NetIp6McRouteInfo 
{ 
    tNetIp6Oif  *pOIf;       /* Downstram Outgoing interface */ 
    UINT4       u4OifCnt;    /* No. of Oifs */ 
    tIp6Addr    SrcAddr;     /* Multicast Src Ip */ 
    tIp6Addr    GrpAddr;     /* Multicast Grp Address */ 
    UINT4       u4Iif;       /* Incoming Interface */ 
}tNetIp6McRouteInfo;

typedef struct _Ip6MultIfToIp6Link
{
          UINT4             u4OnLinkActiveIfId;
          UINT2             u2OnLinkIfCount;
          UINT2             u2LinkOperation;
          UINT4             apIp6If[IPV6_MAX_IF_OVER_LINK];

}tIp6MultIfToIp6Link;

typedef struct
{
    tIp6Addr   SrcAddr;
    tIp6Addr   NbrAddr;
    UINT4     u4IfIndex;
    UINT4     u4PathStatus;
    UINT1     u1AddrType;
    BOOL1     bIsSessCtrlPlaneInDep;
    UINT1     u1Pad [2];
}tClientNbrIp6PathInfo;

typedef struct
{
    UINT1  u1Type;    /* type of ICMPv6 message */
    UINT1  u1Code;    /* addl. error code for the message */
    UINT2  u2Chksum;  /* checksum of ICMPv6 header with
                       * prepended pseudo header */
}
tIcmp6PktHdr;
/*
 * The following structure is used to form and send the Neighbor 
 * advertisements in response to Neighbor Solicitations and also to send 
 * unsolicited advertisements in order to progate changes quickly. It is also 
 * used to interpret the Neighbor advertisements received from a neighbor
 */

typedef struct Ip6If
{
    tTMO_SLL     Ip6AddrList;               /* Unicast address list */
    tTMO_SLL     PrefixList;                /* Prefix list sent in RA */
    tTMO_SLL     Ip6LLAddrList;             /* Link local address list */
    tTMO_SLL        mcastIlist;             /* Multicast address information
                                                * list */
    tIp6Addr     Ip6RdnssAddrOne;             /* IPv6 unicast/anycast address */
    tIp6Addr     Ip6RdnssAddrTwo;             /* IPv6 unicast/anycast address */
    tIp6Addr     Ip6RdnssAddrThree;             /* IPv6 unicast/anycast address */
    tIcmp6ErrRLInfo  Icmp6ErrRLInfo;        /* Icmp6 Error Rate Limit Info */
    tIcmp6Stats  *pIfIcmp6Stats;            /* Interace Statistics for ICMPv6*/
#ifdef TUNNEL_WANTED
    tIp6TunlIf   *pTunlIf;                  /* If interface type is Tunnel,
                                             * then this pointer holds
                                             * additional configuration
                                             * value for the tunnel. */
#endif
    UINT1  ifaceTok[IP6_EUI_ADDRESS_LEN];   /* for EUI-64 */
    UINT1  u1Descr[IP6_IF_DESCR_LEN];       /* Interface Description */
    UINT1  au1IfName[IP6_MAX_IF_NAME_LEN];  /* Interface name */
    UINT1  au1OnLinkIfaces[IPV6_MAX_IF_OVER_LINK]; /* This array has the list
                                              of standby interfaces this is updated
                                              after receiving onlink notification
                                              from OSPF*/
    UINT4  u4IpPort;                        /* IP Port number of interface */
    UINT4  u4IfIndex;                       /* Interface Index of this IP
                                               interface */
    UINT4  u4IfLastUpdate;                  /* Lastest interface update */
    UINT4  u4MaxRaTime;                     /* Max Router Adv interval */
    UINT4  u4MinRaTime;                     /* Min Router Adv interval */
    UINT4  u4DefRaLifetime;                 /* RA Lifetime */
    UINT4  u4RaMtu;                         /* MTU advertised in RA */
    UINT4  u4RAReachableTime;               /* RA reachable time in ms */
    UINT4  u4RARetransTimer;                /* RA AdvRetransTimer in ms */
    UINT4  u4RACurHopLimit;                 /* RA AdvCurHopLimit */
    UINT4  u4Mtu;                           /* MTU on this interface. */
    UINT4  u4IfSpeed;                       /* Interface Speed in BPS */
    UINT4  u4IfHighSpeed;                   /* Interface Speed in MBPS */
    UINT4  u4HopLimit;                      /* Hop limit value */
    UINT4  u4DadAttempts;                   /* Number of DAD attempts */
    UINT4  u4McProtocols;                   /* Multicast protocol bitmap that
                                               are enabled on this interface */
    UINT4  u4OnLinkActiveIfId;                        /* Cfa ifindex of the active
                                               interface on the link*/
    UINT4  u4UnnumAssocIPv6If;              /* Associated unnumbered interface index */
    UINT4  u4RDNSSPreference;               /* RDNSS Preference value*/
    UINT4  u4RDNSSLifetime;                 /* RDNSS Lifetime value*/
    UINT4  u4RDNSSLifetimeOne;                 /* RDNSS Lifetime value*/
    UINT4  u4RDNSSLifetimeTwo;                 /* RDNSS Lifetime value*/
    UINT4  u4RDNSSLifetimeThree;                 /* RDNSS Lifetime value*/
    UINT4  u4ContextId;                     /* ContextId*/
    UINT4  u4DnsLifeTime;                   /* DNS Life time value*/
    UINT4  u4DnsLifeTimeOne;                   /* DNS server one Life time value*/
    UINT4  u4DnsLifeTimeTwo;                   /* DNS server two Life time value*/
    UINT4  u4DnsLifeTimeThree;                   /* DNS server three Life time value*/
    UINT2  u2OnlinkIfCount;                 /* The no of interfaces that are on link
                                               with this interface. This will be updated
                                               at the time of onlink notificatio from OSPF*/
    UINT2  u2CktIndex;                      /* Circuit index */
    UINT2  u2Mif;                           /* Multicast MIF number */
    UINT1  u1IfType;                        /* Interface type */
    UINT1  u1TokLen;                        /* Interface token length */
    UINT1  u1AdminStatus;                   /* Admin Status of the ipv6
                                             * interface */
    UINT1  u1OperStatus;                    /* Oper Status of the ipv6
                                             * interface */
    UINT1  u1RAdvStatus;                    /* Router Advertisement status */
    UINT1  u1PrefAdv;                       /* Prefix advertise status */
    UINT1  u1RAAdvFlags;                    /* AdvManagedFlag and AdvOtherFlag
                                               status in RA */
    UINT1  u1Ipv6IfFwdOperStatus;           /* Interface's forwarding status  - Operation status*/
    UINT1  u1ZoneCreationStatus;            /* The creation status of Scope-Zone on
                                               this interface RFC4007 */
    UINT1  u1ZoneRowStatus;                 /* The row status of the Scope-Zone RFC4007*/
    UINT1  u1RAdvRDNSS;                     /* RDNSS is available */
    UINT1  u1RAdvRDNSSStatus;               /* Router Advertisement RDNSS status */
    UINT1  u1RALinkLocalStatus;      /* Router Advertisement Link Local Address Flag Status*/
    UINT1  u1RAInterval;                    /* Router Advertisement Interval Iption Flag Status */
    UINT1  au1DomainNameOne[IP6_DOMAIN_NAME_LEN + 1]; /*DNS domain name one*/ 
    UINT1  au1DomainNameTwo[IP6_DOMAIN_NAME_LEN + 1]; /*DNS domain name two*/
    UINT1  au1DomainNameThree[IP6_DOMAIN_NAME_LEN + 1]; /*DNS domain name three*/
    UINT1  u1RADNSStateActive;             /* DNS State -ACTIVE/DISABLED */
    UINT2  u2Preference;    /*Router-preference*/
    UINT1  au1Pad[2];
}tLip6If; 

typedef struct t_ND6_NEIGHBOR_ADVERTISEMENT
{

    tIcmp6PktHdr  icmp6Hdr;    /* ICMP Main Packet Header */
    UINT4            u4AdvFlag;  /* Contains the sub-fields,
                                    * R-bit - Router flag 
                                    * S-bit - Solicited flag
                                    * O-bit - Override flag
                                    * 'Reserved' (Unused) */

    tIp6Addr       targAddr6;   /* IPv6 address of the target 
                                    * of the solicitation that 
                                    * prompted this advertisement */

}
tNd6NeighAdv;
/*
 * The following structure is used in identifying the link-layer address
 * of the interface from/to which the ND messages are sent
 */

typedef struct t_ND6_ADDRESS_EXTENSION_OPTION
{
    UINT1  u1Type;  /* Value indicating the 
                      * type of the ND Option
                      * messages */
    UINT1  u1Len;   /* Length of the option
                      * in units of 8 octets */


    UINT1          lladdr[IP6_MAX_LLA_LEN];  /* Source link-layer address
                                              * option contains link-layer 
                                              * address of the sender of 
                                              * the packet,
                                              * Target link-layer address
                                              * option contains the
                                              * link-layer address of the 
                                              * target */

}
tNd6AddrExt;
VOID         Rtm6TaskMain PROTO ((INT1 *pDummy));
VOID         Ip6TaskMain PROTO ((INT1 *pDummy));
VOID         Ping6TaskMain PROTO ((INT1 *pDummy));
VOID         Ip6Shutdown PROTO ((VOID));
INT4         Ip6TaskInit PROTO ((VOID));

VOID         RTM6_Process_Route_Msg_Event (VOID);

tLip6If      * Lip6UtlGetIfEntry PROTO ((UINT4 u4IfIndex));
tIp6If       *Ip6ifGetEntry PROTO ((UINT4 u4Index)); 

INT4
NetIpv6GetIfIndexFromIp6Address PROTO ((tSNMP_OCTET_STRING_TYPE *, UINT4 *));
INT4 NetIpv6GetIfInfo PROTO ((UINT4, tNetIpv6IfInfo*));
INT4 NetIpv6GetFirstIfInfo PROTO ((tNetIpv6IfInfo*));
INT4 NetIpv6GetNextIfInfo PROTO ((UINT4, tNetIpv6IfInfo*));
INT4 NetIpv6GetIfIndexFromName PROTO ((UINT1 *, UINT4 *));
VOID NetIpv6InformSpeedChange PROTO ((UINT4, UINT4));
VOID NetIpv6InformHighSpeedChange PROTO ((UINT4, UINT4));
INT4 NetIpv6GetFirstIfAddr PROTO ((UINT4, tNetIpv6AddrInfo*));
INT4 NetIpv6GetNextIfAddr PROTO ((UINT4, tNetIpv6AddrInfo*, tNetIpv6AddrInfo*));
INT4 NetIpv6DeRegisterHigherLayerProtocol PROTO ((UINT4));
INT4 NetIpv6RegisterHigherLayerProtocol PROTO ((UINT4, UINT4, VOID *));

INT4 NetIpv6DeRegisterHigherLayerProtocolInCxt PROTO ((UINT4, UINT4));
INT4 NetIpv6RegisterHigherLayerProtocolInCxt PROTO ((UINT4, UINT4,
                                                     UINT4, VOID *));
INT4 NetIpv6LeakRoute PROTO ((UINT1, tNetIpv6RtInfo *));
INT4 NetIpv6GetRoute PROTO ((tNetIpv6RtInfoQueryMsg *, tNetIpv6RtInfo *));
INT4 NetIpv6GetUnicastRpPrefix PROTO ((UINT4 u4ContextId, tIp6Addr * pAddr, 
                                       tIp6RpAddrInfo * pIp6RpAddrInfo));
INT4 NetIpv6ValidateUnicastPrefix PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, 
                                          tIp6Addr * pMcastAddr));
INT4 NetIpv6ValidateRpPrefixInCxt PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, 
                                         tIp6Addr * pMcastAddr)); 
INT4 NetIpv6GetFwdTableRouteEntry PROTO ((tIp6Addr *, UINT1, tNetIpv6RtInfo *));
INT4 NetIpv6GetFirstFwdTableRouteEntry PROTO ((tNetIpv6RtInfo *));
INT4 NetIpv6GetNextFwdTableRouteEntry PROTO ((tNetIpv6RtInfo *,
                                              tNetIpv6RtInfo *));
INT4 NetIpv6GetFwdTableRouteEntryInCxt PROTO ((UINT4 u4ContextId, tIp6Addr *, UINT1, tNetIpv6RtInfo *));
INT4 NetIpv6GetFirstFwdTableRouteEntryInCxt PROTO ((UINT4 u4ContextId, tNetIpv6RtInfo *));

INT4 NetIpv6InvokeApplicationReceive PROTO ((tCRU_BUF_CHAIN_HEADER *,
                                             UINT4, UINT4, UINT4, UINT4));
INT4 NetIpv6InvokeAddressChange PROTO ((tIp6Addr  *, UINT4, UINT4,
                                        UINT4, UINT4));
INT4 NetIpv6InvokeRouteChange PROTO ((tNetIpv6RtInfo *, UINT4));
INT4
Rtm6AddOrDelRouteFromKernel (UINT4 u4CxtId, tIp6RtEntry *pIp6RtEntry,  UINT1 u1RtCommand);
INT4 NetIpv6InvokeInterfaceStatusChange PROTO ((UINT4, UINT4, UINT4, UINT4));
INT4 NetIpv6InvokeIcmpv6ErrorSend PROTO ((tCRU_BUF_CHAIN_HEADER *,
                                          tIp6Addr *, UINT4,
                                          UINT4, UINT4, UINT4, UINT4));
INT4 NetIpv6McastJoin PROTO ((UINT4  u4IfIndex, tIp6Addr  *pMcastAddr));
INT4 NetIpv6OspfMcastJoin PROTO ((UINT4  u4IfIndex, tIp6Addr  *pMcastAddr));
INT4 NetIpv6McastLeave PROTO ((UINT4  u4IfIndex, tIp6Addr  *pMcastAddr));
INT4 NetIpv6OspfMcastLeave PROTO ((UINT4  u4IfIndex, tIp6Addr  *pMcastAddr));
INT4 NetIpv6McastJoinOnSocket PROTO ((INT4 ,UINT4  , tIp6Addr  *));
VOID NetIpv6McastMsgHandle PROTO((VOID));

INT4 NetIpv6McastLeaveOnSocket PROTO ((INT4 ,UINT4  , tIp6Addr  *));
INT4 NetIpv6GetSrcForDest PROTO ((UINT4 u4IfIndex,
                                  tIp6Addr *pDstAddr,
                                  tIp6Addr *pSrcAddr));
INT4 NetIpv6IsOurAddress PROTO ((tIp6Addr *pAddr, UINT4 *pu4IfIndex));
INT4 NetIpv6IsOurAddressInCxt PROTO ((UINT4 u4ContextId, tIp6Addr *pAddr,
                                      UINT4 *pu4IfIndex));
INT4 NetIpv6GetCxtId PROTO ((UINT4 u4IfIndex, UINT4 *pu4CxtId));

INT4 NetIp6SetIPv6Addr PROTO ((UINT4 u4Ip6IfIndex,
                               tNetIpv6AddrInfo *pNetIp6Addr));
INT4 NetIp6DeleteIPv6Addr PROTO ((UINT4 u4Ip6IfIndex,
                                  tNetIpv6AddrInfo *pNetIp6Addr));
INT4 NetIpv6InvokeNonGlobalZoneChange PROTO ((UINT4 u4Mask,
                       INT4 i4ZoneIndex,UINT4 u4IfIndex));
#ifdef MBSM_WANTED /* IP6_CHASSIS_MERGE */
INT4 Ip6FwdMbsmUpdateCardStatus PROTO((tMbsmProtoMsg *, UINT1));
INT4 Rtm6MbsmUpdateCardStatus PROTO ((tMbsmProtoMsg *, UINT1));
VOID Ip6MbsmUpdateLCStatus PROTO((tCRU_BUF_CHAIN_HEADER *, UINT1));
VOID Rtm6MbsmUpdateRtTable PROTO((tCRU_BUF_CHAIN_HEADER *, UINT1));
#endif

INT4    Ip6Lock                 PROTO ((VOID));
INT4    Ip6UnLock               PROTO ((VOID));

INT4    Ip6GLock                 PROTO ((VOID));
INT4    Ip6GUnLock               PROTO ((VOID));

INT4 NetIpv6RegisterHLProtocolForMCastPkts PROTO (
                              (VOID (*pAppRcv) (tCRU_BUF_CHAIN_HEADER *)));

INT4 NetIpv6DeRegisterHLProtocolForMCastPkts PROTO ((UINT1 u1AppId));

INT4
NetIpv6RegisterHLProtocolForMCastPktsInCxt PROTO ((UINT4 u4ContextId,
                                                   UINT1 u1Proto,
                                                   VOID (*pAppRcv)
                                                   (tCRU_BUF_CHAIN_HEADER *)));

INT4
NetIpv6DeRegisterHLProtocolForMCastPktsInCxt PROTO ((UINT4 u4ContextId,
                                                     UINT1 u1AppId));

INT4 NetIpv6GetECMP6PRTTime (VOID);

INT4
NetIpv6GetSrcAddrForDestAddr PROTO ((UINT4 u4ContextId, tIp6Addr *pDstAddr,
                                     tIp6Addr *pSrcAddr));

VOID
NetIpv6UpdateNDCache PROTO ((UINT4 u4IfIndex, tIp6Addr *pIp6Addr, 
                             tMacAddr *pMacAddr, UINT1 u1NdState,
                             UINT2 u2NlMsgType));

PUBLIC VOID
NetIpv6AddNdCache (UINT4 u4IfIndex, tIp6Addr * pAddr6, UINT1 *pLladdr,
                UINT1 u1LlaLen, UINT1 u1State);

VOID
NetIpv6UpdateIntfAddrStatus PROTO ((UINT4 u4IfIndex, tIp6Addr Ip6Addr, UINT1 u1AddrState));


INT4 NetIpv6InvokeMCastApplReceive  PROTO ((tIp6Cxt * pIp6Cxt, 
                                            tCRU_BUF_CHAIN_HEADER * pBuf));

INT4 Ip6RcvMcastPktFromHl PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                                  tHlToIp6McastParams * pMcastParams));
VOID
IPvxSetIPv6Addr PROTO ((UINT4 u4Ip6IfIndex, tIp6Addr Ip6Addr, 
                  INT4 i4PrefixLen, INT4 i4AddrType));

VOID
IPvxDeleteIPv6Add  PROTO ((UINT4 u4Ip6IfIndex, tIp6Addr Ip6Addr,
                   UINT4 i4PrefixLen, INT4 i4AddrType));


INT4 
Nd6IsCacheReachable PROTO ((UINT4 u4IfIndex, tIp6Addr * pAddr6));


INT1
Ip6IfaceMapping PROTO ((UINT4, UINT4, UINT1));

VOID
Ip6NotifyContextStatus PROTO ((UINT4, UINT4, UINT1));

INT4 
Ip6SelectContext PROTO ((UINT4));

VOID
Ip6ReleaseContext PROTO ((VOID));

INT4
NetIpv6GetIfIndexFromNameInCxt PROTO ((UINT4 u4L2ContextId,
                                       UINT1 *au1IfName, UINT4 *pu4Index));


/* This IP6 API should be used only after taking IP6 Lock */
tIp6IfZoneMapInfo *
Ip6ZoneGetIfScopeZoneEntry PROTO ((UINT4 u4Index,UINT1 u1Scope));
INT4
Ip6GetCxtId PROTO ((UINT4 u4IfIndex, UINT4 *pu4ContextId));

INT4  NetIpv6McastSetMcStatusOnIface PROTO ((tNetIp6McastInfo * pNetIp6McastInfo,
                                            UINT4 u4McastStatus));
INT4  NetIpv6McastRouteUpdate PROTO ((tNetIp6McRouteInfo * pIp6McastRoute, 
                                      UINT4 u4RouteFlag));
INT4  NetIpv6McastUpdateCpuPortStatus PROTO ((UINT4 u4CpuPortStatus));
INT4  NetIpv6McastUpdateRouteCpuPort PROTO ((tNetIp6McRouteInfo *pIp6McastRoute,
                                             UINT4 u4CpuPortStatus));
VOID NetIpv6NotifySelfIfOnLink PROTO ((UINT1  u1ProtocolId, 
                                       tIp6MultIfToIp6Link *pMultIfPtr));
INT4 NetIpv6GetIfScopeZoneIndex PROTO ((UINT1 u1Scope, UINT4 u4IfIndex));

INT4 NetIpv6GetScopeId (UINT1 *pu1ZoneName, UINT1 *pu1Scope);
INT4
NetIpv6FindZoneIndex (UINT4 u4ContextId, UINT1 u1Scope,
                      UINT1 *au1ZoneName, INT4 *pi4ZoneIndex);
INT4
NetIpv6CheckIfAnyZoneExistForGivenScope(UINT4 u4ContextId, UINT1 u1Scope,
                             UINT1 *au1ZoneName, INT4 *pi4ZoneIndex);
INT4
NetIpv6ValidateZoneName PROTO ((UINT1 *pu1ScopeZone,UINT1  *pu1ErrVal));

INT4
NetIpv6GetIfScopeZoneInfo (UINT4 u4IfIndex, UINT1 u1Action, 
                         tIp6ZoneInfo *pInScopeInfo,
                         tIp6ZoneInfo *pOutScopeInfo);
/* Prototypes for RFC4007 API's */

#ifdef LNXIP6_WANTED
tIp6Addr *  NetIpv6SelectSrcAddrForDstAddr PROTO ((tIp6Addr *pIp6DestAddr, tLip6If * pIf6));
#else
tIp6Addr *  NetIpv6SelectSrcAddrForDstAddr PROTO ((tIp6Addr *pIp6DestAddr, tIp6If * pIf6));
#endif

INT4  NetIpv6SelectDestAddr PROTO ((tIp6DstInfo **pDstInfo, UINT1 u1Count));


INT4 NetIpv6GetPortFromCfaIfIndex PROTO ((UINT4 u4Port, UINT4 *pu4IfIndex));

INT4 NetIpv6GetCfaIfIndexFromPort PROTO ((UINT4 u4IfIndex, UINT4 *pu4Port));

INT4 NetIpv6GetNbrllAddrFromIfIndex PROTO ((UINT4 u4IfIndex, tIp6Addr * pIp6LLAddr));

#ifdef VRRP_WANTED

INT4 NetIpv6CreateVrrpInterface PROTO ((tVrrpNwIntf *pVrrpNwIntf));

INT4 NetIpv6DeleteVrrpInterface PROTO ((tVrrpNwIntf *pVrrpNwIntf));

INT4 NetIpv6GetVrrpInterface PROTO ((tVrrpNwIntf *pVrrpNwInIntf,
                                     tVrrpNwIntf *pVrrpNwOutIntf));

#endif

VOID Ip6UpdateIfaceOutDiscards PROTO ((UINT4 u4IfIndex));
VOID Ip6UpdateIfaceInDiscards PROTO ((UINT4 u4IfIndex));

PUBLIC INT1 Ip6HandlePathStatusChange PROTO((UINT4 u4ContextId,
                                             tClientNbrIp6PathInfo *));
VOID Ip6ProcessPathStatusNotification (tCRU_BUF_CHAIN_HEADER * pBuf);
INT4
Ipv6NdEventSend(tCRU_BUF_CHAIN_HEADER *pNdData);



#ifdef LNXIP6_WANTED
VOID
Ip6SetIfAddr PROTO ((UINT4 u4Ip6IfIndex,tIp6Addr Ip6Addr, INT4 i4PrefixLen, INT4 i4AddrType));

INT1
Ip6EnableInvalidRoute PROTO ((tNetIpv6RtInfo * pNetIp6RtInfo));
#endif

#define NETIPV6_IF_CREATE   1
#define NETIPV6_IF_DELETE   2
#define NETIPV6_IF_UP       3
#define NETIPV6_IF_DOWN     4

/* Comparison constants */

#define  IP6_RB_GREATER               1
#define  IP6_RB_LESSER               -1
#define  IP6_RB_EQUAL                 0

/* Administrative Status */
#define  NETIPV6_ADMIN_UP       0x01
#define  NETIPV6_ADMIN_DOWN     0x02
#define  NETIPV6_ADMIN_INVALID  0x03
#define  NETIPV6_ADMIN_VALID    0x04

/* Oper Status */
#define  NETIPV6_OPER_UP        0x01
#define  NETIPV6_OPER_DOWN      0x02
#define  NETIPV6_OPER_NOIFIDENTIFIER 0x03
#define  NETIPV6_OPER_DOWN_INPROGRESS 0x04

#define IP_ISIS_LEVEL1            7
#define IP_ISIS_LEVEL2            8
#define IP_ISIS_INTER_AREA   9

#define  IPV6_OSPFV3_INTRA_AREA         1
#define  IPV6_OSPFV3_INTER_AREA         2
#define  IPV6_OSPFV3_TYPE_1_EXT         3
#define  IPV6_OSPFV3_TYPE_2_EXT         4
#define  IPV6_OSPFV3_TYPE_1_NSSA_EXT    5
#define  IPV6_OSPFV3_TYPE_2_NSSA_EXT    6




#define  IP6_TASK_LOCK                  Ip6Lock
#define  IP6_TASK_UNLOCK                Ip6UnLock

/* Macro set to get the Interface link MTU value for RA mtu*/
#define  IP6_INTERFACE_LINK_MTU              1

#define    IP6_INVALID_CXT_ID   0xFFFFFFFF

#define  NETIPV6_DEFAULT_LABEL 2
 
#define   IP6_ALLOC_CXT_ENTRY(x)\
          (x = (tIp6Cxt*) IP_ALLOCATE_MEM_BLOCK(gIp6GblInfo.i4Ip6CxtPoolId)) 

#define   IP6_FREE_CXT_ENTRY(x)\
          IP_RELEASE_MEM_BLOCK (gIp6GblInfo.i4Ip6CxtPoolId, x)

UINT1
udp6CxtGetFirstIndexIpvxUdp6Table (tSNMP_OCTET_STRING_TYPE *pIp6FirstLAddr,
                                  UINT4 *pu4FirstPort,
                                  tSNMP_OCTET_STRING_TYPE *pIp6FirstRAddr,
                                  UINT4 *pu4FirstRemotePort,
                                  UINT4 *pu4FirstInstance);
UINT1
udp6CxtGetNextIndexIpvxUdp6Table (tSNMP_OCTET_STRING_TYPE *pIp6LAddr,
                             tSNMP_OCTET_STRING_TYPE *pIp6NextLAddr,
                             UINT4 u4LPort,
                             UINT4 *pu4NextLPort,
                             tSNMP_OCTET_STRING_TYPE *pIp6RAddr,
                             tSNMP_OCTET_STRING_TYPE *pIp6NextRAddr,
                             UINT4 u4RPort,
                             UINT4 *pu4NextRPort,
                             UINT4 u4Instance,
                             UINT4 *pu4Instance);
INT4
Ip6CheckForCacheForAddr(UINT4 , tIp6Addr * );

INT4
NetIpv6InvokeRouteChangeForEcmp(tNetIpv6RtInfo * pNetIpv6RtInfo, UINT4 u4Proto);

VOID
Ip6SendNS(tCRU_BUF_CHAIN_HEADER *pBuf);

PUBLIC INT4
Lip6NlResolveNd(UINT4 u4IfIndex, tIp6Addr *pNextHop);
INT4 Nd6IsIpv6AddrCacheReachable(tIp6If *pIf6, tIp6Addr *pAddr6);
#ifdef LNXIP6_WANTED
PUBLIC VOID
Lip6StopRadvd (UINT4);
INT4
Lip6GetNdStateForRouteNH PROTO ((UINT4 u4ContextId, tIp6RtEntry  *pIp6RtEntry));
#endif

#ifdef _LIP6RA_C
#if (defined(VRF_WANTED)) && (defined(LINUX_310_WANTED))
UINT1   gau1RadvdRunning[SYS_DEF_MAX_NUM_CONTEXTS];
#else
UINT1   gu1RadvdRunning = OSIX_FALSE;
#endif
#else
#if (defined(VRF_WANTED)) && (defined(LINUX_310_WANTED))
extern UINT1   gau1RadvdRunning[SYS_DEF_MAX_NUM_CONTEXTS];
#else
extern UINT1   gu1RadvdRunning;
#endif
#endif /* _LIP6RA_C */

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
PUBLIC VOID
Lip6StopAllRadvd(VOID);
#endif

PUBLIC VOID
Lip6StartRadvd (UINT4);

INT4
NetIpv6Nd6Resolve(UINT4 u4DstIntfIdx,
                  tIp6Addr *pDstAddr6,
                  UINT1 *pu1HwAddr);

INT4  Rtm6UtilGetFirstFwdTableRtEntry PROTO ((UINT4 u4ContextId,
                                       tNetIpv6RtInfo * pNetIpv6RtInfo));
INT4  Rtm6UtilGetNextFwdTableRtEntry PROTO ((UINT4 u4ContextId,
                                      tIp6Addr * pFsipv6RouteDest,
                                      INT4 i4Fsipv6RoutePfxLength,
                                      INT4 i4Fsipv6RouteProtocol,
                                      tIp6Addr * pFsipv6RouteNextHop,
                                      tNetIpv6RtInfo * pNetIpv6RtInfo));
INT4 Rtm6UtilGetFirstInActiveTableRtEntry PROTO ((tNetIpv6RtInfo * pNetIpv6RtInfo));

INT4
Rtm6UtilGetNextInActiveTableRtEntry PROTO ((tIp6Addr * pIp6Addr,
                                     INT4 i4PrefixLen,tIp6Addr *
                                     pIp6NextHopAddr,
                                     tNetIpv6RtInfo * pNetIpv6RtInfo));
INT4
Rtm6UtilGetInActiveTableRtEntry PROTO ((tIp6Addr * pIp6Addr,
                                     INT4 i4PrefixLen,tIp6Addr *
                                     pIp6NextHopAddr,
                                     tNetIpv6RtInfo * pNetIpv6RtInfo));

INT4
Rtm6UpdateRouteInInvdRouteList PROTO ((tIp6Addr * pIp6Addr,
                                       INT4 i4PrefixLen,
                                tIp6Addr * pIp6NextHopAddr,
                                tNetIpv6RtInfo *pNetIp6RtInfo));
INT4 Rtm6DeleteInvdRouteNode PROTO ((tIp6Addr * pIp6Addr, 
                                     INT4 i4PrefixLen, 
                                     tIp6Addr * pIp6NextHopAddr));
INT4
Rtm6GetReachableNextHopInCxt (UINT4 u4ContextId, tIp6Addr  *pNextHopAddr6);

UINT4 NetIpv6GetIpForwardingInCxt(UINT4 u4ContextId);
#ifdef EVPN_VXLAN_WANTED
INT4
Ip6UpdateCacheForAddrFromEvpn (UINT4, tIp6Addr *, UINT1 *);
#endif
#endif /* _IPV6_H */

