/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: clkreg.c,v 1.3 2010/11/09 13:26:23 prabuc Exp $
 *
 * Description: This file contains indicating the clock properties to 
 *              modules registered with Clock.
 *********************************************************************/
#ifndef _CLKREG_C
#define _CLKREG_C

#include "clkincs.h"
/*****************************************************************************/
/* Function                  : ClkRegRegisterProtocol                        */
/*                                                                           */
/* Description               : This routine is used by the external modules  */
/*                             to register with the CLOCKIWF module.         */
/*                                                                           */
/* Input                     : pClkRegInfo - Register info for modules.      */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
ClkRegRegisterProtocol (tClkRegInfo * pClkRegInfo)
{
    tClkRegEntry       *pClkRegEntry = NULL;
    UINT1               u1NewEntry = OSIX_FALSE;

    CLK_LOCK ();

    TMO_SLL_Scan (&(gClkCommonInfo.ClkRegModules), pClkRegEntry, tClkRegEntry *)
    {
        if (pClkRegEntry->u1ModuleId == pClkRegInfo->u1ModuleId)
        {
            break;
        }
    }

    if (pClkRegEntry == NULL)
    {
        /* Entry should be allocated newly and to be added to the
         * data structure
         * */
        pClkRegEntry = (tClkRegEntry *)
            MemAllocMemBlk (gClkCommonInfo.ClockRegPoolId);

        if (pClkRegEntry == NULL)
        {
            return;
        }

        u1NewEntry = OSIX_TRUE;
    }

    pClkRegEntry->u1ModuleId = pClkRegInfo->u1ModuleId;
    pClkRegEntry->u1Event = pClkRegInfo->u1Event;
    pClkRegEntry->pClkChgIndiFn = pClkRegInfo->pClkChgIndiFn;

    if (u1NewEntry == OSIX_TRUE)
    {
        /* Insert into the data base */
        TMO_SLL_Insert (&(gClkCommonInfo.ClkRegModules),
                        &(gClkCommonInfo.ClkRegModules.Head),
                        &(pClkRegEntry->pNode));
    }
    CLK_UNLOCK ();
}

/*****************************************************************************/
/* Function                  : ClkRegDeRegisterProtocol                      */
/*                                                                           */
/* Description               : This routine is used by the external modules  */
/*                             to register with the CLOCKIWF module.         */
/*                                                                           */
/* Input                     : u1ModuleId - Module Identifier.               */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
ClkRegDeRegisterProtocol (UINT1 u1ModuleId)
{
    tClkRegEntry       *pClkRegEntry = NULL;

    CLK_LOCK ();

    TMO_SLL_Scan (&(gClkCommonInfo.ClkRegModules), pClkRegEntry, tClkRegEntry *)
    {
        if (pClkRegEntry->u1ModuleId == u1ModuleId)
        {
            break;
        }
    }

    if (pClkRegEntry != NULL)
    {
        TMO_SLL_Delete (&(gClkCommonInfo.ClkRegModules),
                        (tTMO_SLL_NODE *) & (pClkRegEntry->pNode));

        MemReleaseMemBlock (gClkCommonInfo.ClockRegPoolId,
                            (UINT1 *) &(pClkRegEntry->pNode));
        pClkRegEntry = NULL;
    }

    CLK_UNLOCK ();
}
#endif /* _CLKREG_C */
