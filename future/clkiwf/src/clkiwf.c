/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: clkiwf.c,v 1.22 2016/07/20 09:54:19 siva Exp $
 *
 * Description: This file contains clk main loop and initialization
 *              routines.
 *********************************************************************/
#ifndef _CLKIWF_C
#define _CLKIWF_C

#include "clkincs.h"
extern UINT4        FsClkIwfUtcOffset[12];
extern tIssBool MsrGetRestorationStatus PROTO ((VOID));
/*Varibales Used to set UTC offset value In Linux*/
UINT1 au1SysUtcOffSet[CLK_MAX_UTC_OFFSET][CLK_MAX_UTC_OFFSET_SIZE]=
                        {"+00:00","+01:00","+02:00","+03:00","+03:30","+04:00",
                       "+04:30","+05:00","+05:30","+05:45","+06:00","+06:30",
                       "+07:00","+08:00","+08:30","+09:00","+09:30","+10:00",
                       "+11:00","+12:00","+13:00","+14:00","-01:00","-02:00",
                       "-03:00","-04:00","-05:00","-06:00","-07:00","-08:00",
                       "-09:00","-10:00","-11:00","-12:00"};

UINT1 au1SysTZ[CLK_MAX_UTC_OFFSET][CLK_MAX_UTC_OFFSET_SIZE]=
                {"Africa/Abidjan","Africa/Algiers","Africa/Blantyre","Africa/Asmera","Asia/Tehran",
                "Asia/Baku","Asia/Kabul","Asia/Ashkhabad","Asia/Calcutta","Asia/Kathmandu","Asia/Dacca",
                "Asia/Rangoon","Asia/Ho_Chi_Minh","Asia/Shanghai","Asia/Pyongyang","Asia/Tokyo",
                "Australia/Adelaide","Antarctica/DumontDUrville","Antarctica/Macquarie","Antarctica/McMurdo",
                "Pacific/Apia","Pacific/Kiritimati","America/Scoresbysund","America/Noronha",
                "America/Araguaina","America/Anguilla","America/Atikokan","America/Bahia_Banderas",
                "America/Boise","America/Dawson","America/Anchorage","America/Adak","Pacific/Midway","GMT+12"};

/*****************************************************************************/
/* Function                  : ClkIwfInitClock                                */
/*                                                                           */
/* Description               : This routine is used to initialize the clock  */
/*                             quality of the clock                          */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : gClkCommonInfo.                               */
/*                                                                           */
/* Global Variables Modified : gClkCommonInfo.                               */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
ClkIwfInitClock (VOID)
{
#ifdef WLC_WANTED
    tFsCbInfo           FsCbInfo;
    MEMSET (&FsCbInfo, 0, sizeof (tFsCbInfo));
#endif

    gClkCommonInfo.u1Class = CLK_DEF_CLK_CLASS;
    gClkCommonInfo.u2OffsetScaledLogVariance = CLK_DEF_CLK_VARIANCE;
    gClkCommonInfo.u1Accuracy = CLK_DEF_CLK_ACCURACY;
    gClkCommonInfo.bIsHoldOver = CLK_ENABLED;
    gClkCommonInfo.i4ClockTimeSource = CLK_PTP_CLK;

    /* Register the MIBs with SNMP */
    RegisterFSCLKI ();
#ifdef WLC_WANTED
    FsCbInfo.pClkIwfSetCustClock = &(WlcHdlrProcessWssClkUpdateMsg);
    IssCustRegisterEvtCallBk (ISS_CLKIWF_MODULE, CLKIWF_WSS_CLOCK_UPDATION,
                              &FsCbInfo);
#endif

}

/*****************************************************************************/
/* Function                  : ClkIwfGetClock                                */
/*                                                                           */
/* Description               : This routine is used to acquire the current   */
/*                             time by the external modules. This should be  */
/*                             invoked only in case of Software timestamping.*/
/*                                                                           */
/* Input                     : u1ModuleId - Module Identifier.               */
/*                                                                           */
/* Output                    : pTimeInfo - Time filled outoutp               */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
ClkIwfGetClock (UINT1 u1ModuleId, VOID *pTimeInfo)
{
    INT4                i4RetVal = OSIX_SUCCESS;
    tUtlSysPreciseTime  SysPreciseTime;
    tClkSysTimeInfo    *pClkSysTimeInfo = NULL;
    tUtlTm              tm;

    MEMSET (&tm, 0, sizeof (tUtlTm));
    MEMSET (&SysPreciseTime, 0, sizeof (tUtlSysPreciseTime));

    pClkSysTimeInfo = ((tClkSysTimeInfo *) pTimeInfo);

    /* As different registered modules can follow different timings, it is
     * better to obtain the module identifier and decode the structure
     * based on the module identifier.
     * */
    switch (u1ModuleId)
    {
        case CLK_PROTO_PTP:

            /* Assumption here is that this call will be done from CLK only
             * for Software assisted time stamping techniques
             * */
            UtlGetPreciseSysTime (&SysPreciseTime);

            pClkSysTimeInfo->FsClkTimeVal.u8Sec.u4Lo = SysPreciseTime.u4Sec;
            pClkSysTimeInfo->FsClkTimeVal.u4Nsec = SysPreciseTime.u4NanoSec;
            break;

            /*To handle SNTP module's GetTime Request */
        case CLK_PROTO_NTP:
        case CLK_PROTO_SYS:

            /* Assumption here is that this call will be done from CLK only
             * for Software assisted time stamping techniques               
             * */
            UtlGetTime (&tm);

            pClkSysTimeInfo->FsClkTimeVal.UtlTmVal.tm_sec = tm.tm_sec;
            pClkSysTimeInfo->FsClkTimeVal.UtlTmVal.tm_min = tm.tm_min;
            pClkSysTimeInfo->FsClkTimeVal.UtlTmVal.tm_hour = tm.tm_hour;
            pClkSysTimeInfo->FsClkTimeVal.UtlTmVal.tm_mday = tm.tm_mday;
            pClkSysTimeInfo->FsClkTimeVal.UtlTmVal.tm_mon = tm.tm_mon;
            pClkSysTimeInfo->FsClkTimeVal.UtlTmVal.tm_year = tm.tm_year;
            pClkSysTimeInfo->FsClkTimeVal.UtlTmVal.tm_wday = tm.tm_wday;
            pClkSysTimeInfo->FsClkTimeVal.UtlTmVal.tm_yday = tm.tm_yday;

            break;

        default:

            i4RetVal = OSIX_FAILURE;
            break;
    }

    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : ClkIwfSetClock                                */
/*                                                                           */
/* Description               : This routine is used by the external modules  */
/*                             to register with the CLOCKIWF module.         */
/*                                                                           */
/* Input                     : pClkSysTimeInfo - System timing info.         */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
ClkIwfSetClock (tClkSysTimeInfo * pClkSysTimeInfo, INT4 i4TimeSource)
{
    tUtlSysPreciseTime  SysPreciseTime;
    tUtlTm              tm;

    /* Assumption:
     * This should be invoked only in case of Software timestamping */
    MEMSET (&SysPreciseTime, 0, sizeof (tUtlSysPreciseTime));
    MEMSET (&tm, 0, sizeof (tUtlTm));
    /* CLK_HANDSET_CLK is used for user configuration of time. So this can be avoided  
       in the Time Source check. */
    if ((i4TimeSource != CLK_HANDSET_CLK) &&
        (i4TimeSource != gClkCommonInfo.i4ClockTimeSource))
    {
        return OSIX_FAILURE;
    }

    switch (i4TimeSource)
    {
        case CLK_PTP_CLK:
            SysPreciseTime.u4Sec = pClkSysTimeInfo->FsClkTimeVal.u8Sec.u4Lo;
            SysPreciseTime.u4NanoSec = pClkSysTimeInfo->FsClkTimeVal.u4Nsec;
            /* system time to be updated */
            SysPreciseTime.u4SysTimeUpdate = OSIX_TRUE;

            UtlSetPreciseSysTime (&SysPreciseTime);
            break;

            /*To handle SNTP module's/User configured SetTime Request */
        case CLK_NTP_CLK:
        case CLK_HANDSET_CLK:
            tm.tm_sec = pClkSysTimeInfo->FsClkTimeVal.UtlTmVal.tm_sec;
            tm.tm_min = pClkSysTimeInfo->FsClkTimeVal.UtlTmVal.tm_min;
            tm.tm_hour = pClkSysTimeInfo->FsClkTimeVal.UtlTmVal.tm_hour;
            tm.tm_mday = pClkSysTimeInfo->FsClkTimeVal.UtlTmVal.tm_mday;
            tm.tm_mon = pClkSysTimeInfo->FsClkTimeVal.UtlTmVal.tm_mon;
            tm.tm_year = pClkSysTimeInfo->FsClkTimeVal.UtlTmVal.tm_year;
            tm.tm_wday = pClkSysTimeInfo->FsClkTimeVal.UtlTmVal.tm_wday;
            tm.tm_yday = pClkSysTimeInfo->FsClkTimeVal.UtlTmVal.tm_yday;

            UtlSetTime (&tm);
            break;

        default:
            return OSIX_FAILURE;
    }
    /* Call the call back to set the Real Time Clock */
    if (CLKIWF_CALLBACK[CLKIWF_CUST_CLOCK_UPDATION].pClkIwfSetCustClock != NULL)
    {
        if (OSIX_SUCCESS !=
            CLKIWF_CALLBACK[CLKIWF_CUST_CLOCK_UPDATION].
            pClkIwfSetCustClock (i4TimeSource, &SysPreciseTime, &tm))
        {
            return OSIX_FAILURE;
        }
    }

    /* Call the call back to set the Real Time Clock for WSS */
    if (CLKIWF_CALLBACK[CLKIWF_WSS_CLOCK_UPDATION].pClkIwfSetCustClock != NULL)
    {
        if (OSIX_SUCCESS !=
            CLKIWF_CALLBACK[CLKIWF_WSS_CLOCK_UPDATION].
            pClkIwfSetCustClock (i4TimeSource, &SysPreciseTime, &tm))
        {
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : ClkIwfGetParams                               */
/*                                                                           */
/* Description               : This routine is used to get the primary       */
/*                             parameters for the clock. The parameters      */
/*                             include Variance, Class, Accuracy.            */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : pClkIwfPrimParams - Primary Parameters.       */
/*                                                                           */
/* Global Variables Referred : gClkCommonInfo.                               */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
ClkIwfGetParams (tClkIwfPrimParams * pClkIwfPrimParams)
{
    pClkIwfPrimParams->u4ContextId = 0;
    pClkIwfPrimParams->u1Class = gClkCommonInfo.u1Class;
    pClkIwfPrimParams->u2OffsetScaledLogVariance =
        gClkCommonInfo.u2OffsetScaledLogVariance;
    pClkIwfPrimParams->u1Accuracy = gClkCommonInfo.u1Accuracy;
    pClkIwfPrimParams->bIsHoldOver = gClkCommonInfo.bIsHoldOver;
}

/*****************************************************************************/
/* Function                  : ClkIwfCbUpdateParams                          */
/*                                                                           */
/* Description               : This routine is call back used to update      */
/*                             parameters for the clock. The parameters      */
/*                             include Variance, Class, Accuracy.            */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gClkCommonInfo.                               */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
ClkIwfCbUpdateParams ()
{
    tClkRegEntry       *pClkRegEntry = NULL;
    tClkIwfPrimParams   ClkIwfPrimParams;

    ClkIwfPrimParams.u8CurrentUtcOffset = gClkCommonInfo.u8CurrentUtcOffset;
    ClkIwfPrimParams.i4ClockTimeSource = gClkCommonInfo.i4ClockTimeSource;
    ClkIwfPrimParams.u2OffsetScaledLogVariance =
        gClkCommonInfo.u2OffsetScaledLogVariance;
    ClkIwfPrimParams.u1Class = gClkCommonInfo.u1Class;
    ClkIwfPrimParams.u1Accuracy = gClkCommonInfo.u1Accuracy;
    ClkIwfPrimParams.bIsHoldOver = gClkCommonInfo.bIsHoldOver;

    /* Call Back */
    TMO_SLL_Scan (&(gClkCommonInfo.ClkRegModules), pClkRegEntry, tClkRegEntry *)
    {
        if (pClkRegEntry->pClkChgIndiFn != NULL)
        {
            (*(pClkRegEntry->pClkChgIndiFn)) (&ClkIwfPrimParams);
        }
    }

}

/*****************************************************************************/
/* Function Name      : ClkIwfValidateUtcOffset                              */
/*                                                                           */
/* Description        : This validates the CurrentUtc Offset Value provided  */
/*                      through the user interface.                          */
/*                                                                           */
/* Input(s)           : pFsClkIwfCurrentUtcOffset - Value to be validated.   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE.                           */
/*****************************************************************************/
PUBLIC INT4
ClkIwfValidateUtcOffset (tSNMP_OCTET_STRING_TYPE * pFsClkIwfCurrentUtcOffset)
{
    INT4                i4Index = 0;
    INT4                i4Len = 0;

    if (pFsClkIwfCurrentUtcOffset->pu1_OctetList[0] == 0)
    {
        return OSIX_SUCCESS;
    }
    i4Len = STRLEN (pFsClkIwfCurrentUtcOffset->pu1_OctetList);

    if ((pFsClkIwfCurrentUtcOffset->pu1_OctetList[i4Index] != '+') &&
        (pFsClkIwfCurrentUtcOffset->pu1_OctetList[i4Index] != '-'))
    {
        return OSIX_FAILURE;
    }
    if ((pFsClkIwfCurrentUtcOffset->pu1_OctetList[i4Index + 3] != ':'))
    {
        return OSIX_FAILURE;
    }
    if (pFsClkIwfCurrentUtcOffset->pu1_OctetList[i4Index] == '+')
    {
        /* Check whether  input hour is less than mAX offset  */
        if ((((pFsClkIwfCurrentUtcOffset->pu1_OctetList[i4Index + 1] -
               '0') * 10) + ((pFsClkIwfCurrentUtcOffset->pu1_OctetList[i4Index +
                                                                       2])) -
             '0') < CLK_MAX_UTC_OFFSET1)
        {
            /*Check whether input minute is less than mAX value */
            if ((((pFsClkIwfCurrentUtcOffset->pu1_OctetList[i4Index + 4] -
                   '0') * 10) +
                 (pFsClkIwfCurrentUtcOffset->pu1_OctetList[i4Index + 5]) -
                 '0') > CLK_MAX_MINUTES)
            {
                return OSIX_FAILURE;
            }
        }
        else if ((((pFsClkIwfCurrentUtcOffset->pu1_OctetList[i4Index + 1] -
                    '0') * 10) +
                  ((pFsClkIwfCurrentUtcOffset->pu1_OctetList[i4Index + 2])) -
                  '0') == CLK_MAX_UTC_OFFSET1)
        {
            /*Check whether input minute is less than mAX value */
            if ((((pFsClkIwfCurrentUtcOffset->pu1_OctetList[i4Index + 4] -
                   '0') * 10) +
                 (pFsClkIwfCurrentUtcOffset->pu1_OctetList[i4Index + 5]) -
                 '0') > 0)
            {
                return OSIX_FAILURE;
            }
        }
        else
        {
            return OSIX_FAILURE;
        }
    }
    if (pFsClkIwfCurrentUtcOffset->pu1_OctetList[i4Index] == '-')
    {
        /* Check whether  input hour is less than mAX offset  */
        if ((((pFsClkIwfCurrentUtcOffset->pu1_OctetList[i4Index + 1] -
               '0') * 10) + ((pFsClkIwfCurrentUtcOffset->pu1_OctetList[i4Index +
                                                                       2])) -
             '0') < CLK_MAX_UTC_OFFSET2)
        {
            /*Check whether input minute is less than mAX value */
            if ((((pFsClkIwfCurrentUtcOffset->pu1_OctetList[i4Index + 4] -
                   '0') * 10) +
                 (pFsClkIwfCurrentUtcOffset->pu1_OctetList[i4Index + 5]) -
                 '0') > CLK_MAX_MINUTES)
            {
                return OSIX_FAILURE;
            }
        }
        else if ((((pFsClkIwfCurrentUtcOffset->pu1_OctetList[i4Index + 1] -
                    '0') * 10) +
                  ((pFsClkIwfCurrentUtcOffset->pu1_OctetList[i4Index + 2])) -
                  '0') == CLK_MAX_UTC_OFFSET2)
        {
            /*Check whether input minute is less than mAX value */
            if ((((pFsClkIwfCurrentUtcOffset->pu1_OctetList[i4Index + 4] -
                   '0') * 10) +
                 (pFsClkIwfCurrentUtcOffset->pu1_OctetList[i4Index + 5]) -
                 '0') > 0)
            {
                return OSIX_FAILURE;
            }
        }
        else
        {
            return OSIX_FAILURE;
        }
    }
    /* Check Whether it is Digit  */
    while (i4Index < i4Len)
    {
        if ((pFsClkIwfCurrentUtcOffset->pu1_OctetList[i4Index] == '+') ||
            ((pFsClkIwfCurrentUtcOffset->pu1_OctetList[i4Index]) == '-') ||
            ((pFsClkIwfCurrentUtcOffset->pu1_OctetList[i4Index]) == ':'))
        {
            i4Index++;
            continue;
        }
        if ((pFsClkIwfCurrentUtcOffset->pu1_OctetList[i4Index] - '0' >
             CLK_DIGIT_MAX)
            || (pFsClkIwfCurrentUtcOffset->pu1_OctetList[i4Index] - '0' <
                CLK_DIGIT_MIN))
        {
            return OSIX_FAILURE;
        }
        i4Index++;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ClkIwfCallBackRegister                               */
/*                                                                           */
/* Description        : This function registers the call backs from          */
/*                      application                                          */
/*                                                                           */
/* Input(s)           : u4Event - Event for which callback is registered     */
/*                      pFsCbInfo - Call Back Function                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*****************************************************************************/
PUBLIC INT4
ClkIwfCallBackRegister (UINT4 u4Event, tFsCbInfo * pFsCbInfo)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    switch (u4Event)
    {
        case CLKIWF_CUST_CLOCK_UPDATION:
            CLKIWF_CALLBACK[u4Event].pClkIwfSetCustClock
                = pFsCbInfo->pClkIwfSetCustClock;
            break;

        case CLKIWF_WSS_CLOCK_UPDATION:
            CLKIWF_CALLBACK[u4Event].pClkIwfSetCustClock
                = pFsCbInfo->pClkIwfSetCustClock;
            break;

        default:
            i4RetVal = OSIX_FAILURE;
            break;
    }
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : ClkIwfUpdateClkTime                               */
/*                                                                           */
/* Description        : This function updates the ClkIwf clock based         */
/*                      on the current UTC offset                            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*****************************************************************************/
PUBLIC VOID
ClkIwfUpdateClkTime ()
{
    tUtlTm              tm;
    tUtlSysPreciseTime  pSysPreciseTime;
    tSNMP_OCTET_STRING_TYPE clockUtcOffset;
    UINT4               u4Totalmin, u4Sec;
    INT4                i4Index = 0;
    UINT1               au1ClockUtcOffset[CLK_U8_STR_LEN];

    MEMSET (&pSysPreciseTime, 0, sizeof (tUtlSysPreciseTime));
    MEMSET (&clockUtcOffset, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ClockUtcOffset, 0, CLK_U8_STR_LEN);

    clockUtcOffset.pu1_OctetList = au1ClockUtcOffset;
    clockUtcOffset.i4_Length = sizeof (FS_UINT8);

    if (MsrGetRestorationStatus () == ISS_TRUE)
    {
        /* During restoration, updating the clock with offset can be skipped
         * as the system time will be updated with the offset time */
        return;
    }

    ClkIwfGetClock (CLK_PROTO_SYS, &tm);
    pSysPreciseTime.u4Sec = UtlGetSecondsForTime (&tm);
    pSysPreciseTime.u4NanoSec = 0;
    clockUtcOffset.i4_Length = sizeof (FS_UINT8);

    MEMCPY (clockUtcOffset.pu1_OctetList,
            gClkCommonInfo.au1ClkIwfUtcOffset,
            sizeof (gClkCommonInfo.au1ClkIwfUtcOffset));
    if (clockUtcOffset.pu1_OctetList[0] == 0)
    {
        u4Totalmin = 0;
    }
    else
    {
        u4Totalmin = (((((clockUtcOffset.pu1_OctetList[i4Index + 1] - '0') * 10)
                        + (clockUtcOffset.pu1_OctetList[i4Index + 2] -
                           '0')) * 60) +
                      (((clockUtcOffset.pu1_OctetList[i4Index + 4] -
                         '0') * 10) + (clockUtcOffset.pu1_OctetList[i4Index +
                                                                    5] - '0')));

    }
    u4Sec = (u4Totalmin * 60);
    if (clockUtcOffset.pu1_OctetList[i4Index] == '+')
    {
        pSysPreciseTime.u4Sec += u4Sec;
    }
    else
    {
        pSysPreciseTime.u4Sec -= u4Sec;
    }

    UtlGetTimeForSeconds (pSysPreciseTime.u4Sec, (tUtlTm *) & tm);
    ClkIwfSetClock ((tClkSysTimeInfo *) & tm, CLK_HANDSET_CLK);
}

/*****************************************************************************/
/* Function Name      : ClkIwfGetUtcOffset                                   */
/*                                                                           */
/* Description        : This function returns the ClkIwf utc-offset set      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pSetValFsClkIwfUtcOffset -The Utc offset that was set*/
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
ClkIwfGetUtcOffset (tSNMP_OCTET_STRING_TYPE * pRetValFsClkIwfUtcOffset)
{
    pRetValFsClkIwfUtcOffset->i4_Length = sizeof (FS_UINT8);

    MEMCPY (pRetValFsClkIwfUtcOffset->pu1_OctetList,
            gClkCommonInfo.au1ClkIwfUtcOffset,
            sizeof (gClkCommonInfo.au1ClkIwfUtcOffset));
    return;

}
/*****************************************************************************/
/* Function Name      : ClkIwfSetSystemUtcOffset                             */
/*                                                                           */
/* Description        : This function sets the given ClkIwf utc-offset       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
ClkIwfSetSystemUtcOffset (VOID)
{
#if (!defined LINUXSIM_WANTED) && (!defined NPSIM_WANTED)
   UINT1               au1SystemCommand[128];
   INT4               i4Count = 0;
   INT4               i4Index = 0;
   INT4               i4RetValue = 0; 
   
   MEMSET ((UINT1 *) au1SystemCommand, 0, sizeof (au1SystemCommand));
    
   for(i4Count =0;i4Count<CLK_MAX_UTC_OFFSET;i4Count++)
   {
           if((STRCMP((const INT1 *)gClkCommonInfo.au1ClkIwfUtcOffset,
                                (const INT1 *)au1SysUtcOffSet[i4Count])) == 0)
           {
                   i4Index = i4Count;
                   break;
           }
   }
   remove("/etc/localtime");
   SNPRINTF((CHR1 *)au1SystemCommand,sizeof(au1SystemCommand),
               "ln -s /usr/share/zoneinfo/%s /etc/localtime",au1SysTZ[i4Index] );
   i4RetValue = system((const char *) au1SystemCommand);
#endif
    return;
}
/*****************************************************************************/
/* Function Name      : ClkIwfSetUtcOffset                                   */
/*                                                                           */
/* Description        : This function sets the given ClkIwf utc-offset       */
/*                                                                           */
/* Input(s)           : pSetValFsClkIwfUtcOffset - The Utc offset to be set  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
ClkIwfSetUtcOffset (tSNMP_OCTET_STRING_TYPE * pSetValFsClkIwfUtcOffset)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;
    UINT1               au1OctetString[CLK_MAX_LEN];
    FS_UINT8            u8TmpCurrentUtcOffset;
    FS_UINT8            u8Val1 = { 0, 0 }, u8Val2 =
    {
    0, 0}, u8Result =
    {
    0, 0};
    FS_UINT8            u8TmpVal1 = { 0, 0 }, u8TmpVal2 =
    {
    0, 0};
    UINT4               u4TotalSec, u4Totalmin, u4Sec;
    INT4                i4Index = 0;

    if (STRNCMP (pSetValFsClkIwfUtcOffset->pu1_OctetList,
                 gClkCommonInfo.au1ClkIwfUtcOffset, CLK_MAX_LEN) == 0)
    {
        return;
    }
    MEMSET (&au1OctetString, 0, CLK_MAX_LEN);
    FSAP_U8_CLR (&u8TmpCurrentUtcOffset);

    if (pSetValFsClkIwfUtcOffset->pu1_OctetList[0] == 0)
    {
        MEMSET (gClkCommonInfo.au1ClkIwfUtcOffset, 0,
                sizeof (gClkCommonInfo.au1ClkIwfUtcOffset));
        gClkCommonInfo.u8CurrentUtcOffset.u4Lo = 0;
        gClkCommonInfo.u8CurrentUtcOffset.u4Hi = 0;
    }
    else
    {
        /* Because new UTC offset is set, we need to update the clock
         * by removing any previously set UTC offset*/
        if (gClkCommonInfo.au1ClkIwfUtcOffset[0] == '+')
        {
            gClkCommonInfo.au1ClkIwfUtcOffset[0] = '-';
            ClkIwfUpdateClkTime ();
        }
        else if (gClkCommonInfo.au1ClkIwfUtcOffset[0] == '-')
        {
            gClkCommonInfo.au1ClkIwfUtcOffset[0] = '+';
            ClkIwfUpdateClkTime ();
        }

        MEMCPY (au1OctetString, pSetValFsClkIwfUtcOffset->pu1_OctetList,
                pSetValFsClkIwfUtcOffset->i4_Length);

        u4Totalmin = ((((au1OctetString[i4Index + 1] - '0') * 10)
                       + (au1OctetString[i4Index + 2] - '0') * 60)
                      + (((au1OctetString[i4Index + 4] - '0') * 10)
                         + (au1OctetString[i4Index + 5] - '0')));

        u4Sec = (u4Totalmin * 60) * MIN_VAL_SECONDS;

        u8Val1.u4Lo = u4Sec;
        u4TotalSec = MAX_VAL_SECONDS;
        u8Val2.u4Lo = u4TotalSec;
        UtlU8Mul (&u8Result, &u8Val1, &u8Val2);
        if (au1OctetString[i4Index] == '+')
        {
            u8TmpCurrentUtcOffset = u8Result;
        }
        else                    /*Two's complement has to be done */
        {
            u8TmpVal1.u4Lo = (~u8Result.u4Lo);
            u8TmpVal1.u4Hi = (~u8Result.u4Hi);
            u8TmpVal2.u4Lo = 1;
            u8TmpVal2.u4Hi = 0;
            FSAP_U8_ADD (&u8TmpCurrentUtcOffset, &u8TmpVal1, &u8TmpVal2);

        }

        if (FSAP_U8_CMP (&(u8TmpCurrentUtcOffset),
                         &(gClkCommonInfo.u8CurrentUtcOffset)) != 0)
        {
            FSAP_U8_ASSIGN (&(gClkCommonInfo.u8CurrentUtcOffset),
                            &(u8TmpCurrentUtcOffset));
        }

        MEMCPY (&(gClkCommonInfo.au1ClkIwfUtcOffset),
                pSetValFsClkIwfUtcOffset->pu1_OctetList,
                pSetValFsClkIwfUtcOffset->i4_Length);
        /* Update the time by adding the UTC offset */
        ClkIwfUpdateClkTime ();
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsClkIwfUtcOffset;
    SnmpNotifyInfo.u4OidLen = sizeof (FsClkIwfUtcOffset) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = 0;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s", pSetValFsClkIwfUtcOffset));

    /* Call Back */
    ClkIwfCbUpdateParams ();
    return;

}

/*****************************************************************************/
/* Function Name      : ClkIwUtlforARBTime                                   */
/*                                                                           */
/* Description        : Converts the user input to nano seconds              */
/*                                                                           */
/* Input(s)           : CliHandle     -  CLI context                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS or CLI_FAILURE                           */
/*****************************************************************************/

PUBLIC INT4
ClkIwUtlforARBTime (tSNMP_OCTET_STRING_TYPE * pSetValFsClkIwfARBTime)
{
    UINT1               au1OctetString[CLK_MAX_LEN];
    FS_UINT8            u8TmpCurrentUtcOffset;
    FS_UINT8            u8Val1 = { 0, 0 }, u8Val2 =
    {
    0, 0}, u8Result =
    {
    0, 0};
    FS_UINT8            u8TmpVal1 = { 0, 0 }, u8TmpVal2 =
    {
    0, 0};
    UINT4               u4TotalSec, u4Totalmin, u4Sec;
    INT4                i4Index = 0;

    MEMSET (&au1OctetString, 0, CLK_MAX_LEN);
    FSAP_U8_CLR (&u8TmpCurrentUtcOffset);

    if (pSetValFsClkIwfARBTime->pu1_OctetList[0] != 0)
    {
        MEMCPY (au1OctetString, pSetValFsClkIwfARBTime->pu1_OctetList,
                pSetValFsClkIwfARBTime->i4_Length);

        u4Totalmin = (UINT4) ((((au1OctetString[i4Index + 1] - '0') * 10)
                               + (au1OctetString[i4Index + 2] - '0') * 60)
                              + (((au1OctetString[i4Index + 4] - '0') * 10)
                                 + (au1OctetString[i4Index + 5] - '0')));

        u4Sec = (u4Totalmin * 60) * MIN_VAL_SECONDS;

        u8Val1.u4Lo = u4Sec;
        u4TotalSec = MAX_VAL_SECONDS;
        u8Val2.u4Lo = u4TotalSec;
        UtlU8Mul (&u8Result, &u8Val1, &u8Val2);
        if (au1OctetString[i4Index] == '+')
        {
            u8TmpCurrentUtcOffset = u8Result;
        }
        else                    /*Two's complement has to be done */
        {
            u8TmpVal1.u4Lo = (~u8Result.u4Lo);
            u8TmpVal1.u4Hi = (~u8Result.u4Hi);
            u8TmpVal2.u4Lo = 1;
            u8TmpVal2.u4Hi = 0;
            FSAP_U8_ADD (&u8TmpCurrentUtcOffset, &u8TmpVal1, &u8TmpVal2);
        }
        if (FSAP_U8_CMP (&(u8TmpCurrentUtcOffset),
                         &(gClkCommonInfo.u8ARBTime)) != 0)
        {
            FSAP_U8_ASSIGN (&(gClkCommonInfo.u8ARBTime),
                            &(u8TmpCurrentUtcOffset));
        }
    }
    return CLI_SUCCESS;
}

#endif /* _CLKIWF_C */
