/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsclkilw.c,v 1.14 2016/03/12 10:31:39 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "clkincs.h"
# include  "sntp.h"
# include  "ptp.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsClkIwfClockVariance
 Input       :  The Indices

                The Object 
                retValFsClkIwfClockVariance
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsClkIwfClockVariance (INT4 *pi4RetValFsClkIwfClockVariance)
{

    *pi4RetValFsClkIwfClockVariance = gClkCommonInfo.u2OffsetScaledLogVariance;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsClkIwfClockClass
 Input       :  The Indices

                The Object 
                retValFsClkIwfClockClass
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsClkIwfClockClass (INT4 *pi4RetValFsClkIwfClockClass)
{
    *pi4RetValFsClkIwfClockClass = gClkCommonInfo.u1Class;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsClkIwfClockAccuracy
 Input       :  The Indices

                The Object 
                retValFsClkIwfClockAccuracy
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsClkIwfClockAccuracy (INT4 *pi4RetValFsClkIwfClockAccuracy)
{
    *pi4RetValFsClkIwfClockAccuracy = gClkCommonInfo.u1Accuracy;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsClkIwfClockTimeSource
 Input       :  The Indices

                The Object 
                retValFsClkIwfClockTimeSource
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsClkIwfClockTimeSource (INT4 *pi4RetValFsClkIwfClockTimeSource)
{
    *pi4RetValFsClkIwfClockTimeSource = gClkCommonInfo.i4ClockTimeSource;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsClkIwfCurrentUtcOffset
 Input       :  The Indices

                The Object 
                retValFsClkIwfCurrentUtcOffset
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsClkIwfCurrentUtcOffset (tSNMP_OCTET_STRING_TYPE *
                                pRetValFsClkIwfCurrentUtcOffset)
{

    FSAP_U8_2STR (&(gClkCommonInfo.u8CurrentUtcOffset),
                  (CHR1 *) (pRetValFsClkIwfCurrentUtcOffset->pu1_OctetList));
    pRetValFsClkIwfCurrentUtcOffset->i4_Length =
        STRLEN (pRetValFsClkIwfCurrentUtcOffset->pu1_OctetList);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsClkIwfARBTime
 Input       :  The Indices

                The Object 
                retValFsClkIwfARBTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsClkIwfARBTime (tSNMP_OCTET_STRING_TYPE * pRetValFsClkIwfARBTime)
{

    MEMCPY (pRetValFsClkIwfARBTime->pu1_OctetList,
            gClkCommonInfo.au1ClkIwfARBTime,
            sizeof (gClkCommonInfo.au1ClkIwfARBTime));

    pRetValFsClkIwfARBTime->i4_Length =
        STRLEN (pRetValFsClkIwfARBTime->pu1_OctetList);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsClkIwfHoldoverSpecification
 Input       :  The Indices

                The Object 
                retValFsClkIwfHoldoverSpecification

 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsClkIwfHoldoverSpecification (INT4
                                     *pi4RetValFsClkIwfHoldoverSpecification)
{
    *pi4RetValFsClkIwfHoldoverSpecification = gClkCommonInfo.bIsHoldOver;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsClkIwfLostSync
 Input       :  The Indices

                The Object 
                retValFsClkIwfLostSync
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsClkIwfLostSync (INT4 *pi4RetValFsClkIwfLostSync)
{
    INT4                i4SyncStatus = 0;
    INT4                i4PtpStatus = 0;

#ifdef SNTP_WANTED
    SntpClientStatus (&i4SyncStatus);
#endif

#ifdef PTP_WANTED
    PtpApiGetStatus (&i4PtpStatus);
#endif
    if ((((i4SyncStatus == CLK_SYNCHRONIZED_TO_SNTP_PRIMARY_SERVER)
          || (i4SyncStatus == CLK_SYNCHRONIZED_TO_SNTP_SECONDARY_SERVER))
         && (gClkCommonInfo.i4ClockTimeSource == CLK_NTP_CLK))
        || ((i4PtpStatus == PTP_ENABLED)
            && (gClkCommonInfo.i4ClockTimeSource == CLK_PTP_CLK)))
    {
        gClkCommonInfo.bLostSync = CLK_ENABLED;
    }
    else
    {
        gClkCommonInfo.bLostSync = CLK_DISABLED;
    }

    *pi4RetValFsClkIwfLostSync = gClkCommonInfo.bLostSync;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsClkIwfUtcOffset
 Input       :  The Indices

                The Object 
                retValFsClkIwfUtcOffset
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsClkIwfUtcOffset (tSNMP_OCTET_STRING_TYPE * pRetValFsClkIwfUtcOffset)
{

    MEMCPY (pRetValFsClkIwfUtcOffset->pu1_OctetList,
            gClkCommonInfo.au1ClkIwfUtcOffset,
            sizeof (gClkCommonInfo.au1ClkIwfUtcOffset));
    pRetValFsClkIwfUtcOffset->i4_Length =
        STRLEN (pRetValFsClkIwfUtcOffset->pu1_OctetList);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsClkIwfClockVariance
 Input       :  The Indices

                The Object 
                setValFsClkIwfClockVariance
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsClkIwfClockVariance (INT4 i4SetValFsClkIwfClockVariance)
{

    if (gClkCommonInfo.u2OffsetScaledLogVariance !=
        i4SetValFsClkIwfClockVariance)
    {
        gClkCommonInfo.u2OffsetScaledLogVariance =
            i4SetValFsClkIwfClockVariance;
        /* Call Back */
        ClkIwfCbUpdateParams ();
        ClkTrapSendTrap (CLK_TRAP_VARIANCE_CHG, CLK_TRAP_TYPE_GEN_TRAP);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsClkIwfClockClass
 Input       :  The Indices

                The Object 
                setValFsClkIwfClockClass
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsClkIwfClockClass (INT4 i4SetValFsClkIwfClockClass)
{
    if (gClkCommonInfo.u1Class != i4SetValFsClkIwfClockClass)
    {
        gClkCommonInfo.u1Class = i4SetValFsClkIwfClockClass;
        /* Call Back */
        ClkIwfCbUpdateParams ();
        ClkTrapSendTrap (CLK_TRAP_CLASS_CHG, CLK_TRAP_TYPE_GEN_TRAP);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsClkIwfClockAccuracy
 Input       :  The Indices

                The Object 
                setValFsClkIwfClockAccuracy
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsClkIwfClockAccuracy (INT4 i4SetValFsClkIwfClockAccuracy)
{
    if (gClkCommonInfo.u1Accuracy != (UINT1) i4SetValFsClkIwfClockAccuracy)
    {
        gClkCommonInfo.u1Accuracy = (UINT1) i4SetValFsClkIwfClockAccuracy;
        /* Call Back */
        ClkIwfCbUpdateParams ();
        ClkTrapSendTrap (CLK_TRAP_ACC_CHG, CLK_TRAP_TYPE_GEN_TRAP);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsClkIwfClockTimeSource
 Input       :  The Indices

                The Object 
                setValFsClkIwfClockTimeSource
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsClkIwfClockTimeSource (INT4 i4SetValFsClkIwfClockTimeSource)
{
    if (gClkCommonInfo.i4ClockTimeSource != i4SetValFsClkIwfClockTimeSource)
    {
        gClkCommonInfo.i4ClockTimeSource = i4SetValFsClkIwfClockTimeSource;
        /* Call Back */
        ClkIwfCbUpdateParams ();
        ClkTrapSendTrap (CLK_TRAP_TIME_SRC_CHG, CLK_TRAP_TYPE_GEN_TRAP);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsClkIwfCurrentUtcOffset
 Input       :  The Indices

                The Object 
                setValFsClkIwfCurrentUtcOffset
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsClkIwfCurrentUtcOffset (tSNMP_OCTET_STRING_TYPE *
                                pSetValFsClkIwfCurrentUtcOffset)
{
    FS_UINT8            u8TmpCurrentUtcOffset;

    FSAP_U8_CLR (&u8TmpCurrentUtcOffset);

    FSAP_STR2_U8 ((CHR1 *) (pSetValFsClkIwfCurrentUtcOffset->
                            pu1_OctetList), &(u8TmpCurrentUtcOffset));

    if (FSAP_U8_CMP (&(u8TmpCurrentUtcOffset),
                     &(gClkCommonInfo.u8CurrentUtcOffset)) != 0)
    {
        FSAP_U8_CLR (&gClkCommonInfo.u8CurrentUtcOffset);
        FSAP_U8_ASSIGN (&(gClkCommonInfo.u8CurrentUtcOffset),
                        &(u8TmpCurrentUtcOffset));
    }

    /* Call Back */
    ClkIwfCbUpdateParams ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsClkIwfARBTime
 Input       :  The Indices

                The Object 
                setValFsClkIwfARBTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsClkIwfARBTime (tSNMP_OCTET_STRING_TYPE * pSetValFsClkIwfARBTime)
{

    ClkIwUtlforARBTime (pSetValFsClkIwfARBTime);

    MEMCPY (&(gClkCommonInfo.au1ClkIwfARBTime),
            pSetValFsClkIwfARBTime->pu1_OctetList,
            pSetValFsClkIwfARBTime->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsClkIwfHoldoverSpecification
 Input       :  The Indices

                The Object 
                setValFsClkIwfHoldoverSpecification
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsClkIwfHoldoverSpecification (INT4 i4SetValFsClkIwfHoldoverSpecification)
{
    if (gClkCommonInfo.bIsHoldOver != i4SetValFsClkIwfHoldoverSpecification)
    {
        gClkCommonInfo.bIsHoldOver = i4SetValFsClkIwfHoldoverSpecification;
        /* Call Back */
        ClkIwfCbUpdateParams ();
        ClkTrapSendTrap (CLK_TRAP_HOLD_OVER_CHG, CLK_TRAP_TYPE_GEN_TRAP);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsClkIwfUtcOffset
 Input       :  The Indices

                The Object 
                setValFsClkIwfUtcOffset
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsClkIwfUtcOffset (tSNMP_OCTET_STRING_TYPE * pSetValFsClkIwfUtcOffset)
{
    UINT1               au1OctetString[CLK_MAX_LEN];
    FS_UINT8            u8TmpCurrentUtcOffset;
    FS_UINT8            u8Val1 = { 0, 0 }, u8Val2 =
    {
    0, 0}, u8Result =
    {
    0, 0};
    FS_UINT8            u8TmpVal1 = { 0, 0 }, u8TmpVal2 =
    {
    0, 0};
    UINT4               u4TotalSec, u4Totalmin, u4Sec;
    INT4                i4Index = 0;

    if (STRCMP
        (pSetValFsClkIwfUtcOffset->pu1_OctetList,
         gClkCommonInfo.au1ClkIwfUtcOffset) == 0)
    {
        return SNMP_SUCCESS;
    }
    MEMSET (&au1OctetString, 0, CLK_MAX_LEN);
    FSAP_U8_CLR (&u8TmpCurrentUtcOffset);

    if (pSetValFsClkIwfUtcOffset->pu1_OctetList[0] == 0)
    {
        /* Because new UTC offset is set, we need to update the clock
         * by removing any previously set UTC offset*/

        if (gClkCommonInfo.au1ClkIwfUtcOffset[0] == '+')
        {
            gClkCommonInfo.au1ClkIwfUtcOffset[0] = '-';
            ClkIwfUpdateClkTime ();
        }
        else if (gClkCommonInfo.au1ClkIwfUtcOffset[0] == '-')
        {
            gClkCommonInfo.au1ClkIwfUtcOffset[0] = '+';
            ClkIwfUpdateClkTime ();
        }

        MEMSET (gClkCommonInfo.au1ClkIwfUtcOffset, 0,
                sizeof (gClkCommonInfo.au1ClkIwfUtcOffset));
        gClkCommonInfo.u8CurrentUtcOffset.u4Lo = 0;
        gClkCommonInfo.u8CurrentUtcOffset.u4Hi = 0;
    }
    else
    {
        /* Because new UTC offset is set, we need to update the clock
         * by removing any previously set UTC offset*/
        if (gClkCommonInfo.au1ClkIwfUtcOffset[0] == '+')
        {
            gClkCommonInfo.au1ClkIwfUtcOffset[0] = '-';
            ClkIwfUpdateClkTime ();
        }
        else if (gClkCommonInfo.au1ClkIwfUtcOffset[0] == '-')
        {
            gClkCommonInfo.au1ClkIwfUtcOffset[0] = '+';
            ClkIwfUpdateClkTime ();
        }

        MEMCPY (au1OctetString, pSetValFsClkIwfUtcOffset->pu1_OctetList,
                MEM_MAX_BYTES ((pSetValFsClkIwfUtcOffset->i4_Length),
                               (CLK_MAX_LEN)));

        u4Totalmin = ((((au1OctetString[i4Index + 1] - '0') * 10)
                       + (au1OctetString[i4Index + 2] - '0') * 60)
                      + (((au1OctetString[i4Index + 4] - '0') * 10)
                         + (au1OctetString[i4Index + 5] - '0')));

        u4Sec = (u4Totalmin * 60) * MIN_VAL_SECONDS;

        u8Val1.u4Lo = u4Sec;
        u4TotalSec = MAX_VAL_SECONDS;
        u8Val2.u4Lo = u4TotalSec;
        UtlU8Mul (&u8Result, &u8Val1, &u8Val2);
        if (au1OctetString[i4Index] == '+')
        {
            u8TmpCurrentUtcOffset = u8Result;
        }
        else                    /*Two's complement has to be done */
        {
            u8TmpVal1.u4Lo = (~u8Result.u4Lo);
            u8TmpVal1.u4Hi = (~u8Result.u4Hi);
            u8TmpVal2.u4Lo = 1;
            u8TmpVal2.u4Hi = 0;
            FSAP_U8_ADD (&u8TmpCurrentUtcOffset, &u8TmpVal1, &u8TmpVal2);

        }

        if (FSAP_U8_CMP (&(u8TmpCurrentUtcOffset),
                         &(gClkCommonInfo.u8CurrentUtcOffset)) != 0)
        {
            FSAP_U8_CLR (&gClkCommonInfo.u8CurrentUtcOffset);
            FSAP_U8_ASSIGN (&(gClkCommonInfo.u8CurrentUtcOffset),
                            &(u8TmpCurrentUtcOffset));
        }
        MEMSET (gClkCommonInfo.au1ClkIwfUtcOffset, 0,
                sizeof (gClkCommonInfo.au1ClkIwfUtcOffset));
        MEMCPY (&(gClkCommonInfo.au1ClkIwfUtcOffset),
                pSetValFsClkIwfUtcOffset->pu1_OctetList,
                pSetValFsClkIwfUtcOffset->i4_Length);
	/*Set utc offset value in system*/
	ClkIwfSetSystemUtcOffset(); 
        /* Update the time by adding the UTC offset */
        ClkIwfUpdateClkTime ();
    }

    /* Call Back */
    ClkIwfCbUpdateParams ();
#ifdef SNTP_WANTED
    /*Set the SNTP TimeZone to match this UTC offset */
    SntpSetTimeZone (pSetValFsClkIwfUtcOffset);
#endif
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsClkIwfClockVariance
 Input       :  The Indices

                The Object 
                testValFsClkIwfClockVariance
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsClkIwfClockVariance (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFsClkIwfClockVariance)
{

    if ((i4TestValFsClkIwfClockVariance < CLK_MIN_VARIANCE)
        || (i4TestValFsClkIwfClockVariance > CLK_MAX_VARIANCE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsClkIwfClockClass
 Input       :  The Indices

                The Object 
                testValFsClkIwfClockClass
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsClkIwfClockClass (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsClkIwfClockClass)
{
    /* Validate the Clock Class for MIN and MAX Values */
    if ((i4TestValFsClkIwfClockClass < CLK_MIN_CLASS) ||
        (i4TestValFsClkIwfClockClass > CLK_MAX_CLASS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsClkIwfClockAccuracy
 Input       :  The Indices

                The Object 
                testValFsClkIwfClockAccuracy
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsClkIwfClockAccuracy (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFsClkIwfClockAccuracy)
{
    /* Validate the Clock Accuracy for MIN and MAX Values */
    if ((i4TestValFsClkIwfClockAccuracy < CLK_ACCURACY_MIN) ||
        (i4TestValFsClkIwfClockAccuracy > CLK_ACCURACY_MAX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsClkIwfClockTimeSource
 Input       :  The Indices

                The Object 
                testValFsClkIwfClockTimeSource
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsClkIwfClockTimeSource (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValFsClkIwfClockTimeSource)
{
    /* Validate Clock Time Sources as per IEEE 1588 PTP */
    if ((i4TestValFsClkIwfClockTimeSource != CLK_ATOMIC_CLK) &&
        (i4TestValFsClkIwfClockTimeSource != CLK_GPS_CLK) &&
        (i4TestValFsClkIwfClockTimeSource != CLK_PTP_CLK) &&
        (i4TestValFsClkIwfClockTimeSource != CLK_NTP_CLK) &&
        (i4TestValFsClkIwfClockTimeSource != CLK_OTHER_CLK) &&
        (i4TestValFsClkIwfClockTimeSource != CLK_INTERNAL_OSCILLATOR_CLK))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsClkIwfCurrentUtcOffset
 Input       :  The Indices

                The Object 
                testValFsClkIwfCurrentUtcOffset
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsClkIwfCurrentUtcOffset (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValFsClkIwfCurrentUtcOffset)
{
    if (ClkIwfValidateUtcOffset (pTestValFsClkIwfCurrentUtcOffset)
        == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (pTestValFsClkIwfCurrentUtcOffset->i4_Length > CLK_U8_STR_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return (INT1) SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsClkIwfARBTime
 Input       :  The Indices

                The Object 
                testValFsClkIwfARBTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsClkIwfARBTime (UINT4 *pu4ErrorCode,
                          tSNMP_OCTET_STRING_TYPE * pTestValFsClkIwfARBTime)
{
    if (ClkIwfValidateUtcOffset (pTestValFsClkIwfARBTime) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_CLKIW_ERR_ARB_TIME);
        return (INT1) SNMP_FAILURE;
    }

    if ((pTestValFsClkIwfARBTime->i4_Length) > (INT4) sizeof (FS_UINT8))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        CLI_SET_ERR (CLI_CLKIW_ERR_ARB_TIME);
        return (INT1) SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsClkIwfHoldoverSpecification
 Input       :  The Indices

                The Object 
                testValFsClkIwfHoldoverSpecification
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsClkIwfHoldoverSpecification (UINT4 *pu4ErrorCode,
                                        INT4
                                        i4TestValFsClkIwfHoldoverSpecification)
{

    /* Validate the Clock Accuracy for MIN and MAX Values */
    if ((i4TestValFsClkIwfHoldoverSpecification != (INT4) CLK_ENABLED) &&
        (i4TestValFsClkIwfHoldoverSpecification != (INT4) CLK_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Check if Clock Lost Sync is TRUE then Hold over can not
     * be false. A Clock Lost its sync should be in hold over mode*/

    if ((gClkCommonInfo.bLostSync == CLK_ENABLED) &&
        (i4TestValFsClkIwfHoldoverSpecification == (INT4) CLK_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsClkIwfUtcOffset
 Input       :  The Indices

                The Object 
                testValFsClkIwfUtcOffset
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsClkIwfUtcOffset (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pTestValFsClkIwfUtcOffset)
{
    if (ClkIwfValidateUtcOffset (pTestValFsClkIwfUtcOffset) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (pTestValFsClkIwfUtcOffset->i4_Length > CLK_U8_STR_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return (INT1) SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsClkIwfClockVariance
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Coder :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsClkIwfClockVariance (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsClkIwfClockClass
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsClkIwfClockClass (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsClkIwfClockAccuracy
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsClkIwfClockAccuracy (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsClkIwfClockTimeSource
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsClkIwfClockTimeSource (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsClkIwfCurrentUtcOffset
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsClkIwfCurrentUtcOffset (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsClkIwfARBTime
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsClkIwfARBTime (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsClkIwfHoldoverSpecification
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsClkIwfHoldoverSpecification (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsClkIwfUtcOffset
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsClkIwfUtcOffset (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsClkIwfGlobalErrTrapType
 Input       :  The Indices

                The Object 
                retValFsClkIwfGlobalErrTrapType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsClkIwfGlobalErrTrapType (INT4 *pi4RetValFsClkIwfGlobalErrTrapType)
{
    *pi4RetValFsClkIwfGlobalErrTrapType = (INT4) gClkCommonInfo.u2TrapOption;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsClkIwfNotification
 Input       :  The Indices

                The Object 
                retValFsClkIwfNotification
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsClkIwfNotification (tSNMP_OCTET_STRING_TYPE *
                            pRetValFsClkIwfNotification)
{
    pRetValFsClkIwfNotification->pu1_OctetList[0] = (UINT1)
        gClkCommonInfo.u2TrapOption;

    if (gClkCommonInfo.u2TrapOption != 0)
    {
        pRetValFsClkIwfNotification->i4_Length = sizeof (UINT1);
    }

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsClkIwfNotification
 Input       :  The Indices

                The Object 
                setValFsClkIwfNotification
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsClkIwfNotification (tSNMP_OCTET_STRING_TYPE *
                            pSetValFsClkIwfNotification)
{
    gClkCommonInfo.u2TrapOption = (UINT2)
        pSetValFsClkIwfNotification->pu1_OctetList[0];

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsClkIwfNotification
 Input       :  The Indices

                The Object 
                testValFsClkIwfNotification
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsClkIwfNotification (UINT4 *pu4ErrorCode,
                               tSNMP_OCTET_STRING_TYPE
                               * pTestValFsClkIwfNotification)
{
    if (pTestValFsClkIwfNotification->i4_Length > CLK_MAX_TRAP_LEN)
    {
        /*Invalid Value */
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return (INT1) SNMP_FAILURE;
    }

    return (INT1) SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */
/****************************************************************************
 Function    :  nmhDepv2FsClkIwfNotification
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsClkIwfNotification (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
