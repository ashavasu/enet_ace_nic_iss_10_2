/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: clkiwcli.c,v 1.13 2014/07/18 12:23:39 siva Exp $
 *
 * Description: This file contains the CLK IWF  CLI related routines and 
 * utility functions.
 *****************************************************************************/

#ifndef _CLKIWCLI_C_
#define _CLKIWCLI_C_

#include "clkincs.h"
#include "clkiwcli.h"
#include "clkclipt.h"
#include "fsclkicli.h"

/***************************************************************************
 * FUNCTION NAME             : cli_process_clkiw_cmd 
 *
 * DESCRIPTION               : This function is exported to CLI module to 
 *                             handle the clock iwf cli commands to take the 
 *                             corresponding action. 
 *                             Only SNMP Low level routines are called 
 *                             from CLI.
 *
 * INPUT                     : Variable arguments
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 * 
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
cli_process_clkiw_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4               u4ErrCode;
    INT4                i4Inst;
    INT4                i4RecvValue = 0;
    INT4                i4InterfaceId = 0;
    INT4                i4RetStatus = 0;
    UINT1              *args[CLKIW_CLI_MAX_ARGS];
    INT1                i1ArgNo = 0;

    CliRegisterLock (CliHandle, ClkMainLock, ClkMainUnLock);

    if (ClkMainLock () == OSIX_FAILURE)
    {
        CliPrintf (CliHandle, "%% CLK : execute command failed\r\n");
        return CLI_FAILURE;
    }

    va_start (ap, u4Command);

    /* Third argument is always interface name/index */

    i4Inst = va_arg (ap, INT4);

    if (i4Inst != 0)
    {
        i4InterfaceId = (UINT4) i4Inst;
    }

    /* Walk through the rest of the arguements and store in args array. 
     * Store CLKIW_CLI_MAX_ARGS arguements at the max. 
     */

    MEMSET (args, 0, sizeof (args));

    while (1)
    {
        args[i1ArgNo++] = va_arg (ap, UINT1 *);
        if (i1ArgNo == CLKIW_CLI_MAX_ARGS)
        {
            break;
        }
    }
    va_end (ap);

    switch (u4Command)
    {
        case CLI_CLKIW_VARIANCE:
            MEMCPY (&i4RecvValue, args[0], sizeof (INT4));
            i4RetStatus = ClkIwCliSetClockVariance (CliHandle, i4RecvValue);
            break;

        case CLI_CLKIW_CLASS:

            /* args[0] - Clock Class */
            MEMCPY (&i4RecvValue, args[0], sizeof (INT4));

            i4RetStatus = ClkIwCliSetClockClass (CliHandle, i4RecvValue);
            break;

        case CLI_CLKIW_ACCURACY:

            MEMCPY (&i4RecvValue, args[0], sizeof (INT4));
            i4RetStatus = ClkIwCliSetClockAccuracy (CliHandle, i4RecvValue);
            break;

        case CLI_CLKIW_TIME_SOURCE:

            i4RetStatus =
                ClkIwCliSetClockTimeSource (CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_CLKIW_UTC_OFFSET:

            i4RetStatus =
                ClkIwCliSetClockUtcOffset (CliHandle, (UINT1 *) args[0]);
            break;

        case CLI_CLKIW_ARB_TIME:

            i4RetStatus =
                ClkIwCliSetClockARBTime (CliHandle, (UINT1 *) args[0]);
            break;

        case CLI_CLKIW_HOLD_OVER_ENABLE:

            i4RetStatus = ClkIwCliSetClockHoldOver (CliHandle, CLK_ENABLED);
            break;

        case CLI_CLKIW_HOLD_OVER_DISABLE:

            i4RetStatus = ClkIwCliSetClockHoldOver (CliHandle, CLK_DISABLED);
            break;

        case CLI_CLKIW_NOTIFY_ENABLE:

            ClkIwCliSetClockGlobalTrapType (CliHandle, CLK_ENABLED,
                                            CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_CLKIW_NOTIFY_DISABLE:

            ClkIwCliSetClockGlobalTrapType (CliHandle, CLK_DISABLED,
                                            CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_CLKIW_SHOW_PROPERTIES:

            i4RetStatus = ClkIwCliShowClockIWFInfo (CliHandle);
            break;

        default:

            CliPrintf (CliHandle, "\r%% CLKIWF : Invalid command\r\n");
            ClkMainUnLock ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_CLKIW_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%% %s", gapc1ClkIwCliErrString[u4ErrCode]);
        }

        CLI_SET_ERR (0);
    }

    CLI_SET_CMD_STATUS (i4RetStatus);

    ClkMainUnLock ();

    CliUnRegisterLock (CliHandle);

    UNUSED_PARAM (i4InterfaceId);

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ClkIwSetClockVariance
 *
 * DESCRIPTION      : This function sets the Clock Variance of CLKIWF
 *                    module as configured from CLI.
 *
 * INPUT            : i4ContextId - Context Identifier
 *                    i4ClkVariance - Clk Variance
 *
 * OUTPUT           : None
 *
 *
 * Global Variables Referred :  None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
ClkIwCliSetClockVariance (tCliHandle CliHandle, INT4 i4ClockVariance)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsClkIwfClockVariance (&u4ErrCode, i4ClockVariance)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsClkIwfClockVariance (i4ClockVariance) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ClkIwSetClockClass
 *
 * DESCRIPTION      : This function sets the Clock Class of CLKIWF
 *                    module as configured from CLI.
 *
 * INPUT            : 
 *                    i4ClkVariance - Clock Class
 *
 * OUTPUT           : None
 *
 *
 * Global Variables Referred :  None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
ClkIwCliSetClockClass (tCliHandle CliHandle, INT4 i4ClockClass)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsClkIwfClockClass (&u4ErrCode, i4ClockClass) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsClkIwfClockClass (i4ClockClass) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ClkIwCliSetClockAccuracy
 *
 * DESCRIPTION      : This function sets the ClockAccuracy  of CLKIWF
 *                    module as configured from CLI.
 *
 * INPUT            : ClockAccuracy 
 *                    
 *
 * OUTPUT           : None
 *
 *
 * Global Variables Referred :  None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
ClkIwCliSetClockAccuracy (tCliHandle CliHandle, INT4 i4ClockAccuracy)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsClkIwfClockAccuracy (&u4ErrCode, i4ClockAccuracy)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsClkIwfClockAccuracy (i4ClockAccuracy) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ClkIwSetClockTimeSource
 *
 * DESCRIPTION      : This function sets the  of CLKIWF
 *                    module as configured from CLI.
 *
 * INPUT            : CliHandle, i4ClockTimeSource
 *                    
 *
 * OUTPUT           : None
 *
 *
 * Global Variables Referred :  None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
ClkIwCliSetClockTimeSource (tCliHandle CliHandle, INT4 i4ClockTimeSource)
{

    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsClkIwfClockTimeSource (&u4ErrCode, i4ClockTimeSource)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsClkIwfClockTimeSource (i4ClockTimeSource) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME    : ClkIwSetClockARBTime
 *
 * DESCRIPTION      : This function sets the  of CLKIWF
 *                    module as configured from CLI.
 *
 * INPUT            : i4ContextId - Context Identifier
 *                    
 *
 * OUTPUT           : None
 *
 *
 * Global Variables Referred :  None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
ClkIwCliSetClockARBTime (tCliHandle CliHandle, UINT1 *au1ClockARBTime)
{
    tSNMP_OCTET_STRING_TYPE ClockARBTime;
    UINT4               u4ErrCode = 0;

    MEMSET (&ClockARBTime, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    ClockARBTime.pu1_OctetList = au1ClockARBTime;
    ClockARBTime.i4_Length = STRLEN (au1ClockARBTime);

    if (nmhTestv2FsClkIwfARBTime (&u4ErrCode, &ClockARBTime) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsClkIwfARBTime (&ClockARBTime) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ClkIwSetClockUtcOffset
 *
 * DESCRIPTION      : This function sets the  of CLKIWF
 *                    module as configured from CLI.
 *
 * INPUT            : i4ContextId - Context Identifier
 *                    
 *
 * OUTPUT           : None
 *
 *
 * Global Variables Referred :  None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
ClkIwCliSetClockUtcOffset (tCliHandle CliHandle, UINT1 *au1ClockUtcOffset)
{
    tSNMP_OCTET_STRING_TYPE ClockUtcOffset;
    UINT4               u4ErrCode = 0;

    MEMSET (&ClockUtcOffset, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    ClockUtcOffset.pu1_OctetList = au1ClockUtcOffset;
    ClockUtcOffset.i4_Length = STRLEN (au1ClockUtcOffset);

    if (nmhTestv2FsClkIwfUtcOffset (&u4ErrCode, &ClockUtcOffset)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid Utc Offset Value\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsClkIwfUtcOffset (&ClockUtcOffset) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ClkIwSetClockHoldOver
 *
 * DESCRIPTION      : This function sets the  of CLKIWF
 *                    module as configured from CLI.
 *
 * INPUT            : i4ContextId - Context Identifier
 *                    
 *
 * OUTPUT           : None
 *
 *
 * Global Variables Referred :  None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
ClkIwCliSetClockHoldOver (tCliHandle CliHandle, INT4 i4Status)
{

    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsClkIwfHoldoverSpecification (&u4ErrCode, i4Status)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsClkIwfHoldoverSpecification (i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ClkIwSetClockGlobalTrapType
 *
 * DESCRIPTION      : This function sets the  of CLKIWF
 *                    module as configured from CLI.
 *
 * INPUT            : i4ContextId - Context Identifier
 *                    
 *
 * OUTPUT           : None
 *
 *
 * Global Variables Referred :  None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
ClkIwCliSetClockGlobalTrapType (tCliHandle CliHandle, INT4 i4TrapFlag,
                                INT4 i4TrapValue)
{
    tSNMP_OCTET_STRING_TYPE TrapOption;
    UINT1               au1OctetList[CLK_MAX_TRAP_LEN];
    INT4                i4GlobalErrTrapType = 0;
    UINT4               u4ErrCode = 0;

    MEMSET (&TrapOption, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, sizeof (au1OctetList));

    UNUSED_PARAM (CliHandle);

    TrapOption.pu1_OctetList = au1OctetList;
    TrapOption.i4_Length = CLK_MAX_TRAP_LEN;

    /* Get the Trap Value */
    nmhGetFsClkIwfGlobalErrTrapType (&i4GlobalErrTrapType);

    if (i4TrapFlag == CLK_ENABLED)    /*Enable Trap */
    {
        i4GlobalErrTrapType |= i4TrapValue;
    }
    else if (i4TrapFlag == CLK_DISABLED)    /*Disable Trap */
    {
        i4GlobalErrTrapType = (i4GlobalErrTrapType & (~i4TrapValue));
    }
    else                        /* Invalid flag */
    {
        return CLI_FAILURE;
    }

    au1OctetList[0] = (UINT1) i4GlobalErrTrapType;

    if (nmhTestv2FsClkIwfNotification (&u4ErrCode, &TrapOption) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsClkIwfNotification (&TrapOption) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ClkIwCliShowClockIWFInfo
 *
 * DESCRIPTION      : This function shows the Configurations of CLKIWF
 *                    module as configured.
 *
 * INPUT            : CliHandle - CliHandle
 *                    
 *
 * OUTPUT           : None
 *
 *
 * Global Variables Referred :  None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
ClkIwCliShowClockIWFInfo (tCliHandle CliHandle)
{

    tSNMP_OCTET_STRING_TYPE ClockUtcOffset;
    INT4                i4ClockVariance = 0;
    INT4                i4ClockAccuracy = 0;
    INT4                i4ClockClass = 0;
    INT4                i4HoldOverStatus = 0;
    INT4                i4TimeSource = 0;
    INT4                i4LostSync = 0;
    UINT1               au1ClockUtcOffset[CLK_U8_STR_LEN];

    MEMSET (&ClockUtcOffset, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ClockUtcOffset, 0, CLK_U8_STR_LEN);
    ClockUtcOffset.pu1_OctetList = au1ClockUtcOffset;
    ClockUtcOffset.i4_Length = sizeof (FS_UINT8);

    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle, " System Clock Information\r\n");
    CliPrintf (CliHandle, " ------------------------\r\n");

    nmhGetFsClkIwfClockVariance (&i4ClockVariance);
    nmhGetFsClkIwfClockAccuracy (&i4ClockAccuracy);
    nmhGetFsClkIwfClockClass (&i4ClockClass);
    nmhGetFsClkIwfHoldoverSpecification (&i4HoldOverStatus);
    nmhGetFsClkIwfClockTimeSource (&i4TimeSource);
    nmhGetFsClkIwfLostSync (&i4LostSync);
    nmhGetFsClkIwfUtcOffset (&ClockUtcOffset);

    CliPrintf (CliHandle, " Variance : %u\r\n", i4ClockVariance);
    CliPrintf (CliHandle, " Class    : %u\r\n", i4ClockClass);
    CliPrintf (CliHandle, " Accuracy : ");

    ClkIwCliPrintAccuracy (CliHandle, i4ClockAccuracy);

    CliPrintf (CliHandle, " Source   : ", i4TimeSource);

    switch (i4TimeSource)
    {

        case CLI_CLKIW_ATOMIC_CLK:

            CliPrintf (CliHandle, "Atomic Clock\r\n");
            break;

        case CLI_CLKIW_GPS_CLK:

            CliPrintf (CliHandle, "GPS\r\n");
            break;

        case CLI_CLKIW_PTP_CLK:

            CliPrintf (CliHandle, "PTP\r\n");
            break;

        case CLI_CLKIW_NTP_CLK:

            CliPrintf (CliHandle, "NTP\r\n");
            break;

        case CLI_CLKIW_INTERNAL_OSCILLATOR_CLK:

            CliPrintf (CliHandle, "Internal Oscillator\r\n");
            break;

        default:

            CliPrintf (CliHandle, "None\r\n");
            break;

    }

    if (ClockUtcOffset.pu1_OctetList[0] == 0)
    {
        STRCPY (ClockUtcOffset.pu1_OctetList, "0");
    }
    CliPrintf (CliHandle, " Offset   : %s (UTC)\r\n",
               ClockUtcOffset.pu1_OctetList);

    CliPrintf (CliHandle, " HoldOver : ");

    if (i4HoldOverStatus == CLK_ENABLED)
    {
        CliPrintf (CliHandle, "Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "Disabled\r\n");
    }

    if (i4LostSync == CLK_ENABLED)
    {
        CliPrintf (CliHandle, "\r\nClock in sync with time source\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r\nClock NOT in sync with time source\r\n");
    }

    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ClkIwCliPrintAccuracy
 *
 * DESCRIPTION      : This function shows the Configurations of CLKIWF
 *                    module as configured.
 *
 * INPUT            : CliHandle - CliHandle
 *                    
 *
 * OUTPUT           : None
 *
 *
 * Global Variables Referred :  None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
ClkIwCliPrintAccuracy (tCliHandle CliHandle, INT4 i4ClockAccuracy)
{

    switch (i4ClockAccuracy)
    {
        case CLI_CLK_25NS_ACCURACY:

            CliPrintf (CliHandle, "within 25 ns");
            break;

        case CLI_CLK_100NS_ACCURACY:

            CliPrintf (CliHandle, "within 100 ns");
            break;

        case CLI_CLK_250NS_ACCURACY:

            CliPrintf (CliHandle, "within 250 ns");
            break;

        case CLI_CLK_1MS_ACCURACY:

            CliPrintf (CliHandle, "within 1 micro second");
            break;

        case CLI_CLK_2PT5MS_ACCURACY:

            CliPrintf (CliHandle, "within 2.5 micro seconds");
            break;

        case CLI_CLK_10MS_ACCURACY:

            CliPrintf (CliHandle, "within 10 micro seconds");
            break;

        case CLI_CLK_25MS_ACCURACY:

            CliPrintf (CliHandle, "within 25 micro seconds");
            break;

        case CLI_CLK_100MS_ACCURACY:

            CliPrintf (CliHandle, "within 100 micro seconds");
            break;

        case CLI_CLK_250MS_ACCURACY:

            CliPrintf (CliHandle, "within 250 micro seconds");
            break;

        case CLI_CLK_1MIS_ACCURACY:

            CliPrintf (CliHandle, "within 1 milli second");
            break;

        case CLI_CLK_2PT5MIS_ACCURACY:

            CliPrintf (CliHandle, "within 2.5 milli seconds");
            break;

        case CLI_CLK_10MIS_ACCURACY:

            CliPrintf (CliHandle, "within 10 milli seconds");
            break;

        case CLI_CLK_25MIS_ACCURACY:

            CliPrintf (CliHandle, "within 25 milli seconds");
            break;

        case CLI_CLK_100MIS_ACCURACY:

            CliPrintf (CliHandle, "within 100 milli seconds");
            break;

        case CLI_CLK_250MIS_ACCURACY:

            CliPrintf (CliHandle, "within 250 milli seconds");
            break;

        case CLI_CLK_1S_ACCURACY:

            CliPrintf (CliHandle, "within 1 second");
            break;

        case CLI_CLK_10S_ACCURACY:

            CliPrintf (CliHandle, "within 10 seconds");
            break;

        case CLI_CLK_GREATER_10S_ACCURACY:

            CliPrintf (CliHandle, "greater than 10 seconds");
            break;

        default:

            CliPrintf (CliHandle, "%d", i4ClockAccuracy);
            break;

    }

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ClkIwShowRunningConfig                               */
/*                                                                           */
/* Description        : Displays configurations done in PTP module           */
/*                                                                           */
/* Input(s)           : CliHandle     -  CLI context                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS or CLI_FAILURE                           */
/*****************************************************************************/

PUBLIC INT4
ClkIwShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module)
{
    UNUSED_PARAM (u4Module);
    if (ClkIwSrcScalars (CliHandle) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

#endif
