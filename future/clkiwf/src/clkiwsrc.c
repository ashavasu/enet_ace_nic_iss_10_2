/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: clkiwsrc.c,v 1.8 2016/04/09 09:57:09 siva Exp $
 *
 * Description: This file contains the Clock IWF SRC related routines and
 * utility functions.
 *****************************************************************************/

#ifndef _CLKIWSRC_C_
#define _CLKIWSRC_C_

#include "clkincs.h"
#include "clksrcpt.h"

/***************************************************************************
 * FUNCTION NAME             : ClkIwSrcScalars
 *
 * DESCRIPTION               : This function displays scalar objects in CLK
 *                             IWF for show running configuration command
 *
 * INPUT                     : tCliHandle - Cli Handle
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
INT4
ClkIwSrcScalars (tCliHandle CliHandle)
{

    INT4                i4ClockVariance = 0;
    INT4                i4ClockClass = 0;
    INT4                i4ClockAccuracy = 0;
    INT4                i4HoldOverStatus = 0;
    INT4                i4TimeSource = 0;
    INT4                i4CurTrapOption = 0;
    tSNMP_OCTET_STRING_TYPE ClockUtcOffset;
    tSNMP_OCTET_STRING_TYPE ClockARBTime;
    UINT1               au1ClockUtcOffset[CLK_U8_STR_LEN];
    UINT1               au1ClockARBTime[CLK_U8_STR_LEN];

    MEMSET (&ClockUtcOffset, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ClockUtcOffset, 0, CLK_U8_STR_LEN);
    MEMSET (&ClockARBTime, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ClockARBTime, 0, CLK_U8_STR_LEN);

    ClockUtcOffset.pu1_OctetList = au1ClockUtcOffset;
    ClockUtcOffset.i4_Length = sizeof (FS_UINT8);
    ClockARBTime.pu1_OctetList = au1ClockARBTime;
    ClockARBTime.i4_Length = sizeof (FS_UINT8);


    nmhGetFsClkIwfClockVariance (&i4ClockVariance);
    nmhGetFsClkIwfClockAccuracy (&i4ClockAccuracy);
    nmhGetFsClkIwfClockClass (&i4ClockClass);
    nmhGetFsClkIwfHoldoverSpecification (&i4HoldOverStatus);
    nmhGetFsClkIwfClockTimeSource (&i4TimeSource);
    nmhGetFsClkIwfGlobalErrTrapType (&i4CurTrapOption);
    nmhGetFsClkIwfARBTime (&ClockARBTime);
    nmhGetFsClkIwfUtcOffset (&ClockUtcOffset);
    
    if (i4ClockVariance != CLI_CLKIW_DFLT_VARIANCE)
    {
        CliPrintf (CliHandle, "clock variance %u\r\n", i4ClockVariance);
    }

    if (i4ClockClass != CLI_CLKIW_DFLT_CLASS)
    {
        CliPrintf (CliHandle, "clock class %u\r\n", i4ClockClass);
    }

    if (i4ClockAccuracy != CLI_CLKIW_DFLT_ACCURACY)
    {
        CliPrintf (CliHandle, "clock accuracy %u\r\n", i4ClockAccuracy);
    }

    if (i4HoldOverStatus != CLK_ENABLED)
    {
        CliPrintf (CliHandle, "no clock hold-over mode \r\n");
    }

    if (i4TimeSource != CLI_CLKIW_DFLT_TIME_SOURCE && i4TimeSource != 0)
    {
        CliPrintf (CliHandle, "clock time source ");

        switch (i4TimeSource)
        {

            case CLI_CLKIW_ATOMIC_CLK:

                CliPrintf (CliHandle, "atomic-clock\r\n");
                break;

            case CLI_CLKIW_GPS_CLK:

                CliPrintf (CliHandle, "gps\r\n");
                break;

            case CLI_CLKIW_NTP_CLK:

                CliPrintf (CliHandle, "ntp\r\n");
                break;

            case CLI_CLKIW_INTERNAL_OSCILLATOR_CLK:

                CliPrintf (CliHandle, "internal-oscillator\r\n");
                break;
        }
    }


    if (i4CurTrapOption != 0)
    {
        if ((i4CurTrapOption & CLI_CLKIW_TRAP_GLOB_ERR)
            == CLI_CLKIW_TRAP_GLOB_ERR)
        {
            CliPrintf (CliHandle, "clock notification global-error\r\n");
        }

        if ((i4CurTrapOption & CLI_CLKIW_TRAP_TIME_SRC_CHNG)
            == CLI_CLKIW_TRAP_TIME_SRC_CHNG)
        {
            CliPrintf (CliHandle, "clock notification time-source-change\r\n");
        }

        if ((i4CurTrapOption & CLI_CLKIW_TRAP_CLASS_CHNG)
            == CLI_CLKIW_TRAP_CLASS_CHNG)
        {
            CliPrintf (CliHandle, "clock notification clock-class-change\r\n");
        }

        if ((i4CurTrapOption & CLI_CLKIW_TRAP_ACCURACY_CHNG)
            == CLI_CLKIW_TRAP_ACCURACY_CHNG)
        {
            CliPrintf (CliHandle,
                       "clock notification clock-accuracy-change\r\n");
        }

        if ((i4CurTrapOption & CLI_CLKIW_TRAP_VARIANCE_CHNG)
            == CLI_CLKIW_TRAP_VARIANCE_CHNG)
        {
            CliPrintf (CliHandle,
                       "clock notification clock-variance-change\r\n");
        }

        if ((i4CurTrapOption & CLI_CLKIW_TRAP_HOLDOVER_CHNG)
            == CLI_CLKIW_TRAP_HOLDOVER_CHNG)
        {
            CliPrintf (CliHandle, "clock notification hold-over-change\r\n");
        }

    }
    if (STRNCMP(ClockARBTime.pu1_OctetList, "\0", 1) != 0)
    {
        if (STRNCMP(ClockARBTime.pu1_OctetList, CLI_CLKIWF_DEF_ARB_TIME, 5) != 0)
        {
            CliPrintf (CliHandle, "clock set time %s\r\n",
                       ClockARBTime.pu1_OctetList);
        }
    }
     
    if (STRNCMP(ClockUtcOffset.pu1_OctetList, "\0", 1) != 0)
    {
        if (STRNCMP(ClockUtcOffset.pu1_OctetList, CLI_CLKIWF_DEF_UTC_OFFSET, 5) != 0)
        {
            CliPrintf (CliHandle, "clock utc-offset %s\r\n",
                       ClockUtcOffset.pu1_OctetList);
        }
    }

    return CLI_SUCCESS;
}

#endif
