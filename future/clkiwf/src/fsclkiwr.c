/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsclkiwr.c,v 1.4 2012/12/05 15:02:40 siva Exp $
*
* Description: Wrapper Routines
*********************************************************************/

# include  "lr.h"
# include  "fssnmp.h"
# include  "fsclkilw.h"
# include  "fsclkiwr.h"
# include  "fsclkidb.h"

VOID
RegisterFSCLKI ()
{
    SNMPRegisterMib (&fsclkiOID, &fsclkiEntry, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fsclkiOID, (const UINT1 *) "fsclkiwf");
}

VOID
UnRegisterFSCLKI ()
{
    SNMPUnRegisterMib (&fsclkiOID, &fsclkiEntry);
    SNMPDelSysorEntry (&fsclkiOID, (const UINT1 *) "fsclkiwf");
}

INT4
FsClkIwfClockVarianceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsClkIwfClockVariance (&(pMultiData->i4_SLongValue)));
}

INT4
FsClkIwfClockClassGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsClkIwfClockClass (&(pMultiData->i4_SLongValue)));
}

INT4
FsClkIwfClockAccuracyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsClkIwfClockAccuracy (&(pMultiData->i4_SLongValue)));
}

INT4
FsClkIwfClockTimeSourceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsClkIwfClockTimeSource (&(pMultiData->i4_SLongValue)));
}

INT4
FsClkIwfCurrentUtcOffsetGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsClkIwfCurrentUtcOffset (pMultiData->pOctetStrValue));
}

INT4
FsClkIwfARBTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsClkIwfARBTime (pMultiData->pOctetStrValue));
}

INT4
FsClkIwfHoldoverSpecificationGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsClkIwfHoldoverSpecification (&(pMultiData->i4_SLongValue)));
}

INT4
FsClkIwfLostSyncGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsClkIwfLostSync (&(pMultiData->i4_SLongValue)));
}

INT4
FsClkIwfUtcOffsetGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsClkIwfUtcOffset (pMultiData->pOctetStrValue));
}

INT4
FsClkIwfClockVarianceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsClkIwfClockVariance (pMultiData->i4_SLongValue));
}

INT4
FsClkIwfClockClassSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsClkIwfClockClass (pMultiData->i4_SLongValue));
}

INT4
FsClkIwfClockAccuracySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsClkIwfClockAccuracy (pMultiData->i4_SLongValue));
}

INT4
FsClkIwfClockTimeSourceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsClkIwfClockTimeSource (pMultiData->i4_SLongValue));
}

INT4
FsClkIwfCurrentUtcOffsetSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsClkIwfCurrentUtcOffset (pMultiData->pOctetStrValue));
}

INT4
FsClkIwfARBTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsClkIwfARBTime (pMultiData->pOctetStrValue));
}

INT4
FsClkIwfHoldoverSpecificationSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsClkIwfHoldoverSpecification (pMultiData->i4_SLongValue));
}

INT4
FsClkIwfUtcOffsetSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsClkIwfUtcOffset (pMultiData->pOctetStrValue));
}

INT4
FsClkIwfClockVarianceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsClkIwfClockVariance
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsClkIwfClockClassTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsClkIwfClockClass (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsClkIwfClockAccuracyTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsClkIwfClockAccuracy
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsClkIwfClockTimeSourceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsClkIwfClockTimeSource
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsClkIwfCurrentUtcOffsetTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsClkIwfCurrentUtcOffset
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsClkIwfARBTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsClkIwfARBTime (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsClkIwfHoldoverSpecificationTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsClkIwfHoldoverSpecification
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsClkIwfUtcOffsetTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsClkIwfUtcOffset (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsClkIwfClockVarianceDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsClkIwfClockVariance
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsClkIwfClockClassDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsClkIwfClockClass
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsClkIwfClockAccuracyDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsClkIwfClockAccuracy
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsClkIwfClockTimeSourceDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsClkIwfClockTimeSource
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsClkIwfCurrentUtcOffsetDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsClkIwfCurrentUtcOffset
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsClkIwfARBTimeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsClkIwfARBTime (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsClkIwfHoldoverSpecificationDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsClkIwfHoldoverSpecification
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsClkIwfUtcOffsetDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsClkIwfUtcOffset
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsClkIwfGlobalErrTrapTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsClkIwfGlobalErrTrapType (&(pMultiData->i4_SLongValue)));
}

INT4
FsClkIwfNotificationGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsClkIwfNotification (pMultiData->pOctetStrValue));
}

INT4
FsClkIwfNotificationSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsClkIwfNotification (pMultiData->pOctetStrValue));
}

INT4
FsClkIwfNotificationTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsClkIwfNotification
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsClkIwfNotificationDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsClkIwfNotification
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
