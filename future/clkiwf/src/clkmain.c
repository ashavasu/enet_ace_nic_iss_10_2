/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * Description: This file contains clk main loop and initialization
 *              routines.
 *********************************************************************/
#ifndef _CLKMAIN_C_
#define _CLKMAIN_C_

#include "clkincs.h"

/*****************************************************************************/
/* Function                  : ClkMainInit                                   */
/*                                                                           */
/* Description               : This routine initializes the memory for the   */
/*                             CLKIWF module. This routine will not wait for */
/*                             events, as the CLKIWF module does not involve */
/*                             processing the events seperately as a task/   */
/*                             thread. Instead this module will act as a     */
/*                             common data base holder for various Clock     */
/*                             synchronization protocols that are present in */
/*                             the system.                                   */
/*                                                                           */
/* Input                     : pArg - Pointer to the Args.                   */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
ClkMainInit (INT1 *pArg)
{
    UNUSED_PARAM (pArg);

    MEMSET (&gClkCommonInfo, 0, sizeof (tClkCommonInfo));

    if ((OsixSemCrt (CLK_SEM_NAME, &(gClkCommonInfo.SemId))) == OSIX_FAILURE)
    {
        return;
    }

    ClkMainUnLock ();

    TMO_SLL_Init (&gClkCommonInfo.ClkRegModules);

    /* Context related Memory pool */
    if (MemCreateMemPool (sizeof (tClkRegEntry), CLK_MAX_PROTOCOLS,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &(gClkCommonInfo.ClockRegPoolId)) == MEM_FAILURE)
    {
        OsixSemDel (gClkCommonInfo.SemId);
        return;
    }
    ClkIwfInitClock ();
    lrInitComplete (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function                  : ClkMainLock                                   */
/*                                                                           */
/* Description               : This API shall be used to take the mutual     */
/*                             protocol semaphore.                           */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gClkCommonInfo.SemId                          */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
ClkMainLock (VOID)
{
    if (OsixSemTake (gClkCommonInfo.SemId) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : ClkMainUnLock                                 */
/*                                                                           */
/* Description               : This API shall be used to release the mutual  */
/*                             protocol semaphore.                           */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gClkCommonInfo.SemId                          */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
ClkMainUnLock (VOID)
{
    if (OsixSemGive (gClkCommonInfo.SemId) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
#endif /* _CLKMAIN_C_ */
