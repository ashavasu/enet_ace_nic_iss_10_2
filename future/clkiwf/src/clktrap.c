/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: clktrap.c,v 1.6 2014/03/09 13:34:14 siva Exp $
 *
 * Description: This file contains clk trap implementation.
 *********************************************************************/
#ifndef _CLKTRAP_C
#define _CLKTRAP_C

#include "clkincs.h"
#include "clktrap.h"
#include "fsclkiwf.h"

/*****************************************************************************/
/* Function Name      : ClkTrapSendTrap                                      */
/*                                                                           */
/* Description        : This function sends the trap for this module. There  */
/*                      is currently only one trap supported and that one    */
/*                      represents the type of Clock parameter that got      */
/*                      changed.                                             */
/*                                                                           */
/* Input(s)           : i4TrapVal - Trap Value. represents the trap type.    */
/*                      u2TrapType - Trap Type.                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE.                           */
/*****************************************************************************/
PUBLIC INT4
ClkTrapSendTrap (INT4 i4TrapVal, UINT2 u2TrapType)
{
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    tSNMP_COUNTER64_TYPE u8CounterVal = { 0, 0 };
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    UINT4               u4GenTrapType = ENTERPRISE_SPECIFIC;
    UINT4               u4SpecTrapType = 0;
    UINT1               au1Buf[CLK_OBJECT_NAME_LEN];

    if (gClkCommonInfo.u2TrapOption == 0)
    {
        return OSIX_SUCCESS;
    }

    if (ClkTrapIsTrapSet (i4TrapVal) == OSIX_FALSE)
    {
        return OSIX_SUCCESS;
    }

    /* Filling the Enterprise OID */
    pEnterpriseOid = alloc_oid (CLK_SNMPV2_TRAP_OID_LEN + 1);
    if (pEnterpriseOid == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMCPY (pEnterpriseOid->pu4_OidList, gau4ClkTrapOid,
            sizeof (gau4ClkTrapOid));
    pEnterpriseOid->u4_Length = (UINT4) CLK_SNMPV2_TRAP_OID_LEN + 1;

    /* gau4ClkTrapOid contains the base CLK trap OID
     * 1.3.6.1.4.1.29601.2.46.2.0
     * Each Trap notification type is extended from the base trap OID
     * The Trap takes the value u2TrapType and inherits the OID from the
     * parent OID. So, the last octet is added with u2TrapType
     */
    pEnterpriseOid->pu4_OidList[CLK_SNMPV2_TRAP_OID_LEN] = u2TrapType;

    pSnmpTrapOid = alloc_oid (CLK_SNMPV2_TRAP_OID_LEN);
    if (pSnmpTrapOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        return OSIX_FAILURE;
    }

    MEMCPY (pSnmpTrapOid->pu4_OidList, gau4ClkSnmpTrapOid,
            sizeof (gau4ClkSnmpTrapOid));
    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                       SNMP_DATA_TYPE_OBJECT_ID,
                                                       0L, 0, NULL,
                                                       pEnterpriseOid,
                                                       u8CounterVal);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        return OSIX_FAILURE;
    }

    pStartVb = pVbList;

    MEMSET (au1Buf, 0, sizeof (au1Buf));
    SPRINTF ((char *) au1Buf, "fsClkIwfGlobalErrTrapType");

    pOid =
        ClkTrapMakeObjIdFrmString ((INT1 *) au1Buf,
                                   (UINT1 *) fs_clk_mib_oid_table);
    if (pOid == NULL)
    {
        SNMP_AGT_FreeVarBindList (pStartVb);
        return OSIX_FAILURE;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *)
        SNMP_AGT_FormVarBind (pOid,
                              SNMP_DATA_TYPE_INTEGER32, 0, i4TrapVal,
                              NULL, NULL, SnmpCnt64Type);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return OSIX_FAILURE;
    }

    pVbList = pVbList->pNextVarBind;

    ClkTrapFmNotifyFaults (pEnterpriseOid, u4GenTrapType, u4SpecTrapType,
                           pStartVb);
    return OSIX_SUCCESS;
}

/**************************************************************************
 *  Function                  : ClkTrapMakeObjIdFrmString
 * 
 *  Description               : This Function retuns the OID  
 *                              of the given string for the
 *                              proprietary MIB.
 * 
 *  Input                     : pi1TextStr - pointer to the string.
 *                              pTableName - TableName has to be fetched.
 * 
 *  Output                    : None
 *
 *  Global Variables Referred : None
 *
 *  Global variables Modified : None
 *
 *  Use of Recursion          : None
 *
 *  Returns                   : pOidPtr or NULL
 * ***********************************************************************/
PRIVATE tSNMP_OID_TYPE *
ClkTrapMakeObjIdFrmString (INT1 *pi1TextStr, UINT1 *pu1TableName)
{
    tSNMP_OID_TYPE     *pOidPtr = NULL;
    INT1               *pi1DotPtr = NULL;
    UINT2               u2Index = 0;
    UINT2               u2DotCount = 0;
    INT1                ai1TempBuffer[CLK_OBJECT_NAME_LEN + 1];
    UINT1              *pu1TempPtr = NULL;
    struct MIB_OID     *pTableName = NULL;
    UINT4               u4Len = 0;
    pTableName = (struct MIB_OID *) (VOID *) pu1TableName;

    MEMSET (ai1TempBuffer, 0, sizeof (ai1TempBuffer));

    /* see if there is an alpha descriptor at begining */
    if (ISALPHA (*pi1TextStr) != 0)
    {
        pi1DotPtr = (INT1 *) STRCHR ((INT1 *) pi1TextStr, '.');

        /* if no dot, point to end of string */
        if (pi1DotPtr == NULL)
        {
            pi1DotPtr = pi1TextStr + STRLEN ((INT1 *) pi1TextStr);
        }
        pu1TempPtr = (UINT1 *) pi1TextStr;

        for (u2Index = 0;
             ((pu1TempPtr < (UINT1 *) pi1DotPtr) &&
              (u2Index < CLK_OBJECT_NAME_LEN)); u2Index++)
        {
            ai1TempBuffer[u2Index] = *pu1TempPtr++;
        }
        ai1TempBuffer[u2Index] = '\0';
        for (u2Index = 0; pTableName[u2Index].pName != NULL; u2Index++)
        {
            if ((STRCMP (pTableName[u2Index].pName, (INT1 *) ai1TempBuffer)
                 == 0) && (STRLEN ((INT1 *) ai1TempBuffer) ==
                           STRLEN (pTableName[u2Index].pName)))
            {
                u4Len =
                    (STRLEN (pTableName[u2Index].pNumber) <
                     sizeof (ai1TempBuffer)) ? STRLEN (pTableName[u2Index].
                                                       pNumber) :
                    sizeof (ai1TempBuffer) - 1;
                STRNCPY ((INT1 *) ai1TempBuffer, pTableName[u2Index].pNumber,
                         u4Len);
                ai1TempBuffer[u4Len] = '\0';
                break;
            }
        }

        if (pTableName[u2Index].pName == NULL)
        {
            return (NULL);
        }
        /* now concatenate the non-alpha part to the begining */
        u4Len =
            (STRLEN (pi1DotPtr) <
             (sizeof (ai1TempBuffer) -
              STRLEN (ai1TempBuffer))) ? STRLEN (pi1DotPtr)
            : (sizeof (ai1TempBuffer) - STRLEN (ai1TempBuffer));
        STRNCAT ((INT1 *) ai1TempBuffer, (INT1 *) pi1DotPtr, u4Len);
    }
    else
    {
        /* is not alpha, so just copy into ai1TempBuffer */
        STRCPY ((INT1 *) ai1TempBuffer, (INT1 *) pi1TextStr);
    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    for (u2Index = 0; ((u2Index <= CLK_OBJECT_NAME_LEN) &&
                       (ai1TempBuffer[u2Index] != '\0')); u2Index++)
    {
        if (ai1TempBuffer[u2Index] == '.')
        {
            u2DotCount++;
        }
    }
    pOidPtr = alloc_oid (SNMP_MAX_OID_LENGTH);
    if (pOidPtr == NULL)
    {
        return (NULL);
    }

    pOidPtr->u4_Length = u2DotCount + 1;

    /* now we convert number.number.... strings */
    pu1TempPtr = (UINT1 *) ai1TempBuffer;
    for (u2Index = 0; u2Index < u2DotCount + 1; u2Index++)
    {
        if (ClkTrapParseSubIdNew (&pu1TempPtr,
                                  &(pOidPtr->pu4_OidList[u2Index]))
            == OSIX_FAILURE)
        {
            free_oid (pOidPtr);
            return (NULL);
        }

        if (*pu1TempPtr == '.')
        {
            pu1TempPtr++;        /* to skip over dot */
        }
        else if (*pu1TempPtr != '\0')
        {
            free_oid (pOidPtr);
            return (NULL);
        }
    }                            /* end of for loop */

    return (pOidPtr);
}

/******************************************************************************
 *  Function                 : ClkTrapParseSubIdNew
 *
 *  Description              : Parse the string format in 
 *                             number.number..format.
 *
 *  Input                    : ppu1TempPtr - pointer to the string.
 *                             pu4Value    - Pointer the OID List value.
 *
 *  Output                   : value of ppu1TempPtr
 *
 *  Global Variables Referred: None
 *
 *  Global variables Modified: None
 *
 *  Use of Recursion         : None
 *
 *  Returns                  : OSIX_SUCCESS or OSIX_FAILURE
*******************************************************************************/
PRIVATE INT4
ClkTrapParseSubIdNew (UINT1 **ppu1TempPtr, UINT4 *pu4Value)
{
    UINT4               u4Value = 0;
    UINT1              *pu1Tmp = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    for (pu1Tmp = *ppu1TempPtr; (((*pu1Tmp >= '0') && (*pu1Tmp <= '9')));
         pu1Tmp++)
    {
        u4Value = (u4Value * 10) + (*pu1Tmp & 0xf);
    }

    if (*ppu1TempPtr == pu1Tmp)
    {
        i4RetVal = OSIX_FAILURE;
    }
    *ppu1TempPtr = pu1Tmp;
    *pu4Value = u4Value;
    return (i4RetVal);
}

/*****************************************************************************/
/* Function                  : ClkTrapFmNotifyFaults                         */
/*                                                                           */
/* Description               : This function Sends the trap message to the   */
/*                             Fault Manager.                                */
/*                                                                           */
/* Input(s)                  : u4port - port identifier                      */
/*                             pu4Ifindex - Interface Index                  */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
ClkTrapFmNotifyFaults (tSNMP_OID_TYPE * pEnterpriseOid, UINT4 u4GenTrapType,
                       UINT4 u4SpecTrapType, tSNMP_VAR_BIND * pTrapMsg)
{
#ifdef FM_WANTED
    tFmFaultMsg         FmFaultMsg;

    MEMSET (&FmFaultMsg, 0, sizeof (tFmFaultMsg));

    UNUSED_PARAM (pEnterpriseOid);
    UNUSED_PARAM (u4GenTrapType);
    UNUSED_PARAM (u4SpecTrapType);

    /* TOS Code
       FmFaultMsg.pEnterpriseOid = pEnterpriseOid;
     */
    FmFaultMsg.pTrapMsg = pTrapMsg;
    FmFaultMsg.pSyslogMsg = (UINT1 *) gac1ClkSyslogMsg[1];
    /*
       FmFaultMsg.u4GenTrapType = u4GenTrapType;
       FmFaultMsg.u4SpecTrapType = u4SpecTrapType;
     */
    FmFaultMsg.u4ModuleId = FM_NOTIFY_MOD_ID_CLK;

    if (FmApiNotifyFaults (&FmFaultMsg) == OSIX_FAILURE)
    {
        SNMP_AGT_FreeVarBindList (pTrapMsg);
    }

#else
    UNUSED_PARAM (pEnterpriseOid);
    UNUSED_PARAM (u4GenTrapType);
    UNUSED_PARAM (u4SpecTrapType);
    SNMP_AGT_FreeVarBindList (pTrapMsg);
#endif
    return;
}

/******************************************************************************
 *  Function                 : ClkTrapIsTrapSet    
 *
 *  Description              : This function identifies whether the given trap
 *                             option is enabled in Clock.
 *
 *  Input                    : i4TrapVal - Trap Value to be identified.
 *
 *  Output                   : None.               
 *
 *  Global Variables Referred: None
 *
 *  Global variables Modified: None
 *
 *  Use of Recursion         : None
 *
 *  Returns                  : OSIX_SUCCESS or OSIX_FAILURE
*******************************************************************************/
PRIVATE INT4
ClkTrapIsTrapSet (INT4 i4TrapVal)
{
    INT4                i4RetVal = OSIX_FALSE;
    UINT2               u2TrapVal = 0;

    switch (i4TrapVal)
    {
        case CLK_TRAP_TIME_SRC_CHG:

            u2TrapVal = CLK_TIME_SRC_TRAP;
            break;

        case CLK_TRAP_CLASS_CHG:

            u2TrapVal = CLK_CLASS_TRAP;
            break;

        case CLK_TRAP_ACC_CHG:

            u2TrapVal = CLK_ACCURACY_TRAP;
            break;

        case CLK_TRAP_VARIANCE_CHG:

            u2TrapVal = CLK_VARIANCE_TRAP;
            break;

        case CLK_TRAP_HOLD_OVER_CHG:

            u2TrapVal = CLK_HOLDOVER_TRAP;
            break;

        default:
            break;
    }

    if ((u2TrapVal & gClkCommonInfo.u2TrapOption) != 0)
    {
        i4RetVal = OSIX_TRUE;
    }

    return i4RetVal;
}
#endif /* _CLKTRAP_C */
