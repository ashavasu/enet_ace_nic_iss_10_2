/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 * $Id: clkincs.h,v 1.4 2014/06/24 11:33:53 siva Exp $
 * Description: This file contains all the necessary includes for the clock
 *              Module.
 *****************************************************************************/
#ifndef _CLKINCS_H
#define _CLKINCS_H

#include "lr.h"
#include "fsclk.h"
#include "fm.h"
#include "trace.h"
#include "utlsll.h"
#include "snmccons.h"
#include "fssnmp.h"
#include "snmputil.h"
#include "ip.h"
#include "l2iwf.h"
#include "fsvlan.h"
#include "vcm.h"
#include "trace.h"
#include "lr.h"
#include "cfa.h"
#include "fsvlan.h"
#include "l2iwf.h"
#include "fsbuddy.h"
#include "ptp.h"
#include "clkiwcli.h"
#include "clksrcpt.h"
#include "fsclkiwr.h"
#include "fsclkilw.h"
#include "clkconst.h"
#include "clkmacs.h"
#include "clktdfs.h"
#ifdef _CLKMAIN_C_
#include "clkglob.h"
#else
#include "clkextn.h"
#endif
#include "clkprot.h"

#ifdef NPAPI_WANTED
#include "npapi.h"
#include "ptpnp.h"
#endif
#ifdef SNTP_WANTED
#include "fsntp.h"
#endif
#ifdef WLC_WANTED
#include "wlchdlr.h"
#endif

#endif /* _CLKINCS_H */
