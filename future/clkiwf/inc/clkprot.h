/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: clkprot.h,v 1.7 2016/03/12 10:31:38 siva Exp $
 *
 * Description: Contains the prototypes for the functions used by the 
 *              CLKIWF  module.
 ********************************************************************/
#ifndef _CLKPROT_H_
#define _CLKPROT_H_

PUBLIC VOID ClkMainInit PROTO ((INT1 *pArg));


PUBLIC VOID ClkIwfInitClock PROTO ((VOID));

PUBLIC INT4 ClkIwfValidateUtcOffset PROTO ((tSNMP_OCTET_STRING_TYPE * 
                                            pFsClkIwfCurrentUtcOffset));

PUBLIC INT4 ClkTrapSendTrap PROTO ((INT4 i4TrapVal, UINT2 u2TrapType));
PUBLIC VOID ClkIwfUpdateClkTime PROTO ((VOID));

PUBLIC INT4
ClkIwUtlforARBTime PROTO ((tSNMP_OCTET_STRING_TYPE * pSetValFsClkIwfARBTime));

PUBLIC VOID
ClkIwfSetSystemUtcOffset PROTO ((VOID));

#endif /* _CLKPROT_H_ */
