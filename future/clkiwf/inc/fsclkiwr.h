/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsclkiwr.h,v 1.3 2012/11/27 10:41:31 siva Exp $
*
* Description: Proto types for Wrapper  Routines
*********************************************************************/

#ifndef _FSCLKIWR_H
#define _FSCLKIWR_H

VOID RegisterFSCLKI(VOID);

VOID UnRegisterFSCLKI(VOID);
INT4 FsClkIwfClockVarianceGet(tSnmpIndex *, tRetVal *);
INT4 FsClkIwfClockClassGet(tSnmpIndex *, tRetVal *);
INT4 FsClkIwfClockAccuracyGet(tSnmpIndex *, tRetVal *);
INT4 FsClkIwfClockTimeSourceGet(tSnmpIndex *, tRetVal *);
INT4 FsClkIwfCurrentUtcOffsetGet(tSnmpIndex *, tRetVal *);
INT4 FsClkIwfARBTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsClkIwfHoldoverSpecificationGet(tSnmpIndex *, tRetVal *);
INT4 FsClkIwfLostSyncGet(tSnmpIndex *, tRetVal *);
INT4 FsClkIwfUtcOffsetGet(tSnmpIndex *, tRetVal *);
INT4 FsClkIwfClockVarianceSet(tSnmpIndex *, tRetVal *);
INT4 FsClkIwfClockClassSet(tSnmpIndex *, tRetVal *);
INT4 FsClkIwfClockAccuracySet(tSnmpIndex *, tRetVal *);
INT4 FsClkIwfClockTimeSourceSet(tSnmpIndex *, tRetVal *);
INT4 FsClkIwfCurrentUtcOffsetSet(tSnmpIndex *, tRetVal *);
INT4 FsClkIwfARBTimeSet(tSnmpIndex *, tRetVal *);
INT4 FsClkIwfHoldoverSpecificationSet(tSnmpIndex *, tRetVal *);
INT4 FsClkIwfUtcOffsetSet(tSnmpIndex *, tRetVal *);
INT4 FsClkIwfClockVarianceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsClkIwfClockClassTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsClkIwfClockAccuracyTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsClkIwfClockTimeSourceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsClkIwfCurrentUtcOffsetTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsClkIwfARBTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsClkIwfHoldoverSpecificationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsClkIwfUtcOffsetTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsClkIwfClockVarianceDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsClkIwfClockClassDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsClkIwfClockAccuracyDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsClkIwfClockTimeSourceDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsClkIwfCurrentUtcOffsetDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsClkIwfARBTimeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsClkIwfHoldoverSpecificationDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsClkIwfUtcOffsetDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsClkIwfGlobalErrTrapTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsClkIwfNotificationGet(tSnmpIndex *, tRetVal *);
INT4 FsClkIwfNotificationSet(tSnmpIndex *, tRetVal *);
INT4 FsClkIwfNotificationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsClkIwfNotificationDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
#endif /* _FSCLKIWR_H */
