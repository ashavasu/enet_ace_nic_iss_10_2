
/**********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: clksrcpt.h,v 1.2 2010/07/08 14:23:50 prabuc Exp $
 *
 * Description: This has prototype definitions for PTP SRC submodule
 *
 ***********************************************************************/
#ifndef __CLKSRCPT_H__
#define __CLKSRCPT_H__


/**********************************************************************
                        clkiwsrc.c
**********************************************************************/



INT4  ClkIwSrcScalars PROTO ((tCliHandle CliHandle));



#endif /* CLKSRCPT_H */
