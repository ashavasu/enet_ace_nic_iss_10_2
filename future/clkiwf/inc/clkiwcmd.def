/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: clkiwcmd.def,v 1.12 2014/03/18 11:56:42 siva Exp $
 *
 * Description:Clock Interworking Function Configuration Commands
 *             
 *******************************************************************/

/* Clock Interworking Function Configuration Commands */

DEFINE GROUP : CLK_IWF_CONFIG_CMDS

    COMMAND: clock variance <short(0-255)> 
    ACTION : cli_process_clkiw_cmd (CliHandle, CLI_CLKIW_VARIANCE, NULL, $2);
    SYNTAX : clock variance <value(0-255)>
    PRVID  : 15
    HELP   : Configure the variance of primary clock.
  CXT_HELP : clock Configures clock related information|
             variance Variance of the primary clock|
             (0-255) Clock variance value|
             <CR> Configure the variance of primary clock.

             
 
    COMMAND: no clock variance
    ACTION : 
          { 
              INT4      i4DefaultVariance = CLI_CLKIW_DFLT_VARIANCE;

              cli_process_clkiw_cmd (CliHandle, CLI_CLKIW_VARIANCE, NULL, &i4DefaultVariance);
          }
    SYNTAX : no clock variance
    PRVID  : 15
    HELP   : Reset the variance of primary clock to default variance.
  CXT_HELP : no Disables the configuration / deletes the entry / resets to default value|
             clock Configures clock related information|
             variance Variance of the primary clock|
             <CR> Reset the variance of primary clock to default variance.


    COMMAND: clock class <short(0-255)>
    ACTION : cli_process_clkiw_cmd (CliHandle, CLI_CLKIW_CLASS, NULL, $2);
    SYNTAX : clock class <value(0-255)>
    PRVID  : 15
    HELP   : Configure the class of primary clock.
  CXT_HELP : clock Configures clock related information|
             class Configuration related to class of primary clock|
             (0-255) Clock class value|
             <CR> Configure the class of primary clock.

             
 
    COMMAND: no clock class
    ACTION : 
          { 
              INT4      i4DefaultClass = CLI_CLKIW_DFLT_CLASS;

              cli_process_clkiw_cmd (CliHandle, CLI_CLKIW_CLASS, NULL, &i4DefaultClass);
          }
    SYNTAX : no clock class
    PRVID  : 15
    HELP   : Reset the class of primary clock to default class.
  CXT_HELP : no Disables the configuration / deletes the entry / resets to default value|
             clock Configures clock related information|
             class Information related to class of primary clock|
             <CR> Reset the class of primary clock to default class.


    COMMAND: clock accuracy <short(32-254)>
    ACTION : cli_process_clkiw_cmd (CliHandle, CLI_CLKIW_ACCURACY, NULL, $2);
    SYNTAX : clock accuracy <value(32-254)>
    PRVID  : 15
    HELP   : Configure the accuracy of primary clock.
   CXT_HELP: clock Configures clock related information|
             accuracy Configuration related to accuracy of primary clock|
             (32-254) Accuracy value|
             <CR> Configure the accuracy of primary clock.

    COMMAND: no clock accuracy
    ACTION : 
           {
               INT4      i4DefaultAccuracy = CLI_CLKIW_DFLT_ACCURACY;

               cli_process_clkiw_cmd (CliHandle, CLI_CLKIW_ACCURACY, NULL, &i4DefaultAccuracy);
           }
    SYNTAX : no clock accuracy
    PRVID  : 15
    HELP   : Reset the accuracy of primary clock to default accuracy.
   CXT_HELP: no Disables the configuration / deletes the entry / resets to default value|
             clock Configures clock related information|
             accuracy Configuration related to accuracy of primary clock|
             <CR> Reset the accuracy of primary clock to default accuracy.


    COMMAND: clock time source { atomic-clock | gps | ptp | ntp | internal-oscillator }
    ACTION : 
          {
              INT4      i4TimeSource = CLI_CLKIW_DFLT_TIME_SOURCE;

                  if ($3 != NULL)
                  {
                      i4TimeSource = CLI_CLKIW_ATOMIC_CLK ;
                  }
                  else if($4 != NULL)
                  {
                      i4TimeSource = CLI_CLKIW_GPS_CLK ;   
                  }
                  else if($5 != NULL)
                  {
                      i4TimeSource = CLI_CLKIW_PTP_CLK ;   
                  }
                  else if($6 != NULL)
                  {
                      i4TimeSource = CLI_CLKIW_NTP_CLK ;   
                  }
                  else
                  {
                      i4TimeSource = CLI_CLKIW_INTERNAL_OSCILLATOR_CLK ;   
                  }

              cli_process_clkiw_cmd (CliHandle, CLI_CLKIW_TIME_SOURCE, NULL, i4TimeSource);

          }
    SYNTAX : clock time source { atomic-clock | gps | ptp | ntp | internal-oscillator }
    PRVID  : 15
    HELP   : Configure the time source of primary clock.
   CXT_HELP: clock Configures clock related information|
             time Primary clock time related configuration|
             source Primary clock time source related configuration|
             atomic-clock Primary time source of the system is atomic-clock |
             gps Primary time source of the system is gps|
             ptp Primary time source of the system is ptp|
             ntp Primary time source of the system is ntp|
             internal-oscillator Primary time source of the system is internal-oscillator|
             <CR> Configure the time source of primary clock.


    COMMAND: no clock time source
    ACTION : 
          {
              INT4      i4DefaultTimeSource = CLI_CLKIW_DFLT_TIME_SOURCE;

              cli_process_clkiw_cmd (CliHandle, CLI_CLKIW_TIME_SOURCE, NULL, i4DefaultTimeSource);
          }
    SYNTAX : no clock time source
    PRVID  : 15
    HELP   : Reset time source of the primary clock to default time source
   CXT_HELP: no Disables the configuration / deletes the entry / resets to default value|
             clock Configures clock related information|
             time Primary clock time related configuration|
             source Primary clock time source related configuration|
             <CR> Reset time source of the primary clock to default time source.

    COMMAND: clock utc-offset <string>
    ACTION : cli_process_clkiw_cmd (CliHandle, CLI_CLKIW_UTC_OFFSET, NULL, $2);
    SYNTAX : clock utc-offset <offset>
    PRVID  : 15
    HELP   : Configure current utc-offset in scaled nano seconds.
   CXT_HELP: clock Configures clock related information|
             utc-offset Offset of the PTP timescale with UTC time scale|
             <string> Current utc-offset value as (+HH:MM /-HH:MM)(+00:00 to +14:00)/ (-00:00 to -12:00)|
             <CR> Configure current utc-offset in scaled nano seconds.


    COMMAND: no clock utc-offset
    ACTION : 
          {
              UINT1     au1UtcStr[CLK_U8_STR_LEN];
              MEMSET (au1UtcStr,0,CLK_U8_STR_LEN);

              cli_process_clkiw_cmd (CliHandle, CLI_CLKIW_UTC_OFFSET, NULL, 
                                     au1UtcStr);
          }
    SYNTAX : no clock utc-offset
    PRVID  : 15
    HELP   : Reset current UTC offset to default utc-offset.
   CXT_HELP: no Disables the configuration / deletes the entry / resets to default value|
             clock Configures clock related information|
             utc-offset Offset of the PTP timescale with UTC time scale|
             <CR> Reset current UTC offset to default utc-offset.
    

    COMMAND: clock hold-over mode
    ACTION : cli_process_clkiw_cmd (CliHandle, CLI_CLKIW_HOLD_OVER_ENABLE, NULL);
    SYNTAX : clock hold-over mode
    PRVID  : 15
    HELP   : Enable hold over specification mode in clock.
   CXT_HELP: clock Configures clock related information|
             hold-over Hold over specification related configuration|
             mode The primary clock is in hold-over mode|
             <CR> Enable hold over specification mode in clock.



    COMMAND: no clock hold-over mode
    ACTION : cli_process_clkiw_cmd (CliHandle, CLI_CLKIW_HOLD_OVER_DISABLE, NULL);
    SYNTAX : no clock hold-over mode
    PRVID  : 15
    HELP   : Disable hold over specification mode in clock.
   CXT_HELP: no Disables the configuration / deletes the entry / resets to default value|
             clock Configures clock related information|
             hold-over Hold over specification related configuration|
             mode The primary clock is in hold-over mode|
             <CR> Disable hold over specification mode in clock.
   

    COMMAND: clock set time <string> 
    ACTION : cli_process_clkiw_cmd (CliHandle, CLI_CLKIW_ARB_TIME, NULL, $3);
    SYNTAX : clock set time <time-nanoseconds>
    PRVID  : 15
    HELP   : Configure the time of primary clock
   CXT_HELP: clock Configures clock related information|
             set Sets the primary clock related configuration| 
             time Primary clock time related configuration|
             <string> Clock set time value as (+HH:MM /-HH:MM)(+00:00 to +14:00)/ (-00:00 to -12:00)|
             <CR> Configure the time of primary clock.


    COMMAND: clock notification { [global-error] [time-source-change] [clock-class-change] [clock-accuracy-change][clock-variance-change] [hold-over-change] } 
    ACTION : 
          {
              INT4		i4TrapType = 0;

              if ($2 != NULL)
              {
                  i4TrapType |= CLI_CLKIW_TRAP_GLOB_ERR;
              }
              if ($3 != NULL)
              {
                  i4TrapType |= CLI_CLKIW_TRAP_TIME_SRC_CHNG;
              }
              if ($4 != NULL)
              {
                  i4TrapType |= CLI_CLKIW_TRAP_CLASS_CHNG;
              }
              if ($5 != NULL)
              {
                  i4TrapType |= CLI_CLKIW_TRAP_ACCURACY_CHNG;
              }
              if ($6 != NULL)
              {
                  i4TrapType |= CLI_CLKIW_TRAP_VARIANCE_CHNG;
              }
              if ($7 != NULL)
              {
                  i4TrapType |= CLI_CLKIW_TRAP_HOLDOVER_CHNG;
              }

              cli_process_clkiw_cmd (CliHandle, CLI_CLKIW_NOTIFY_ENABLE, NULL, 
                                     i4TrapType);
          }
    SYNTAX : clock notification { [global-error] [time-source-change] [clock-class-change] [clock-accuracy-change][clock-variance-change] [hold-over-change] }
    PRVID  : 15
    HELP   : Enable traps for notifying failures and property changes.
   CXT_HELP: clock Configures clock related information|
             notification ClockIwf failure notification related configuration|
             global-error Notification message is generated in ClockIwf when memory allocation/buffer allocation failure error occurs|
             time-source-change Notification message is generated in ClockIwf when primary time source changes|
             clock-class-change Notification message is generated in ClockIwf when class of the clock changes|
             clock-accuracy-change Notification message is generated in ClockIwf when accuracy of the clock changes|
             clock-variance-change Notification message is generated in ClockIwf when variance of the clock changes|
             hold-over-change Notification message is generated in ClockIwf when hold-over specification of the clock changes|
             <CR> Enable traps for notifying failures and property changes.



    COMMAND: no clock notification { [global-error] [time-source-change] [clock-class-change] [clock-accuracy-change][clock-variance-change] [hold-over-change] } 
    ACTION : 
          {
              INT4 		i4TrapType = 0;

              if ($3 != NULL)
              {
                  i4TrapType |= CLI_CLKIW_TRAP_GLOB_ERR;
              }
              if ($4 != NULL)
              {
                  i4TrapType |= CLI_CLKIW_TRAP_TIME_SRC_CHNG;
              }
              if ($5 != NULL)
              {
                  i4TrapType |= CLI_CLKIW_TRAP_CLASS_CHNG;
              }
              if ($6 != NULL)
              {
                  i4TrapType |= CLI_CLKIW_TRAP_ACCURACY_CHNG;
              }
              if ($7 != NULL)
              {
                  i4TrapType |= CLI_CLKIW_TRAP_VARIANCE_CHNG;
              }
              if ($8 != NULL)
              {
                  i4TrapType |= CLI_CLKIW_TRAP_HOLDOVER_CHNG;
              }

              cli_process_clkiw_cmd (CliHandle, CLI_CLKIW_NOTIFY_DISABLE, NULL, i4TrapType);
          }
    SYNTAX : no clock notification { [global-error] [time-source-change] [clock-class-change] [clock-accuracy-change][clock-variance-change] [hold-over-change] }
    PRVID  : 15
    HELP   : Disable traps for notifying failures and property changes.
   CXT_HELP: no Disables the configuration / deletes the entry / resets to default value|
             clock Configures clock related information|
             notification ClockIwf failure notification related configuration|
             global-error Notification message is generated in ClockIwf when memory allocation/buffer allocation failure error occurs|
             time-source-change Notification message is generated in ClockIwf when primary time source changes|
             clock-class-change Notification message is generated in ClockIwf when class of the clock changes|
             clock-accuracy-change Notification message is generated in ClockIwf when accuracy of the clock changes|
             clock-variance-change Notification message is generated in ClockIwf when variance of the clock changes|
             hold-over-change Notification message is generated in ClockIwf when hold-over specification of the clock changes|
             <CR> Disable traps for notifying failures and property changes.



END GROUP

DEFINE GROUP : CLK_IWF_SHOW_CMDS

    COMMAND: show clock properties
    ACTION : cli_process_clkiw_cmd (CliHandle, CLI_CLKIW_SHOW_PROPERTIES);
    SYNTAX : show clock properties
    PRVID  : 1
    HELP   : Display primary system clock properties.
  CXT_HELP : show Displays the configuration / statistics / general information|
             clock Configures clock related information|
             properties Clock properties related Information|
             <CR> Display primary system clock properties.


END GROUP

/* End of CLKIWF Commands */
