/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: clkglob.h,v 1.3 2012/01/10 13:07:06 siva Exp $
 *
 * Description: This file contains global definitions  
 *
 *****************************************************************************/
#ifndef _CLKGLOB_H_
#define _CLKGLOB_H_

tClkCommonInfo     gClkCommonInfo;
/* global variable for call back routine */
tClkIwfCallBackEntry     gClkIwfCallBack[CLKIWF_MAX_CALLBACK_EVENTS];
#endif /* _CLKGLOB_H_ */
