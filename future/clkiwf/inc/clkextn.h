/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: clkextn.h,v 1.3 2012/01/10 13:07:07 siva Exp $
 *
 * Description: This file contains global definitions  
 *
 *****************************************************************************/
#ifndef _CLKEXTN_H_
#define _CLKEXTN_H_

extern tClkCommonInfo     gClkCommonInfo;
extern tClkIwfCallBackEntry     gClkIwfCallBack[CLKIWF_MAX_CALLBACK_EVENTS];
#endif /* _CLKEXTN_H_ */
