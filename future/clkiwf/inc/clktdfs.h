/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved]
 *
 * $Id: clktdfs.h,v 1.5 2013/07/06 13:44:32 siva Exp $
 *
 * Description: This file contains all the typedefs used by the clk 
 *              module.
 *******************************************************************/
#ifndef _CLKTDFS_H
#define _CLKTDFS_H 

typedef struct _ClkCommonInfo {
    tOsixSemId          SemId;  
                          /* Clock Semaphore Id */
    tTMO_SLL            ClkRegModules;
                          /* Contains the list of modules that are
                           * registered with this CLKIWF module */
    FS_UINT8            u8CurrentUtcOffset;
                          /* UTC Offset in Scaled nano seconds 
                           * with respect to the system time*/
    FS_UINT8            u8ARBTime;
                          /* Arbitrary time in Scaled nano seconds
                           * to system clock*/
    tMemPoolId          ClockRegPoolId;
                          /* Memory pool identifiers to create
                           * registration entries
                           * */
    INT4                i4ClockTimeSource;
                         /* Clock Time Source */
    UINT1         au1ClkIwfUtcOffset[CLK_MAX_LEN];
    UINT2               u2OffsetScaledLogVariance;
                          /* Variance of the clock */
    UINT2               u2CurrentUtcOffset;
                          /* Offset from current UTC */ 
    UINT2               u2TrapOption;
                          /* Traps enabled for Clockiwf */
    UINT1               u1Class;
                          /* Class of the clock */
    UINT1               u1Accuracy;
                          /* Accuracy of the clock */
    BOOL1               bIsHoldOver;
                          /* If OSIX_TRUE, it indicates that the clock
                           * is within hold over specifications
                           * */
    BOOL1               bLostSync;
                          /* If OSIX_TRUE, it indicates that the clock
                           * has lost sync with the primary refeence
                           * */
    UINT1               au1ClkIwfARBTime[CLK_MAX_LEN];
    UINT1               au1Pad[2];
}tClkCommonInfo;

typedef struct _ClkRegEntry
{
    tTMO_SLL_NODE  pNode;
    INT4           (*pClkChgIndiFn)
                        (tClkIwfPrimParams * pClkIwfPrimParams);
    UINT1          u1ModuleId; /* Protocol Id */
    UINT1          u1Event; /* Event id */
    UINT1          au1Pad[2]; /* Padding */
}tClkRegEntry;

/* Call back structure for clkiwf  */
typedef union ClkIwfCallBackEntry { 
    INT4 (*pClkIwfSetCustClock) (INT4 i4TimeSource,  
          tUtlSysPreciseTime *pSysPreciseTime, tUtlTm *pUtlTm); 
} tClkIwfCallBackEntry; 

#endif /* _CLKTDFS_H_ */
