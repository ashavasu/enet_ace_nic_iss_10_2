/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsclkilw.h,v 1.3 2012/11/27 10:41:31 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsClkIwfClockVariance ARG_LIST((INT4 *));

INT1
nmhGetFsClkIwfClockClass ARG_LIST((INT4 *));

INT1
nmhGetFsClkIwfClockAccuracy ARG_LIST((INT4 *));

INT1
nmhGetFsClkIwfClockTimeSource ARG_LIST((INT4 *));

INT1
nmhGetFsClkIwfCurrentUtcOffset ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsClkIwfARBTime ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsClkIwfHoldoverSpecification ARG_LIST((INT4 *));

INT1
nmhGetFsClkIwfLostSync ARG_LIST((INT4 *));

INT1
nmhGetFsClkIwfUtcOffset ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsClkIwfClockVariance ARG_LIST((INT4 ));

INT1
nmhSetFsClkIwfClockClass ARG_LIST((INT4 ));

INT1
nmhSetFsClkIwfClockAccuracy ARG_LIST((INT4 ));

INT1
nmhSetFsClkIwfClockTimeSource ARG_LIST((INT4 ));

INT1
nmhSetFsClkIwfCurrentUtcOffset ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsClkIwfARBTime ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsClkIwfHoldoverSpecification ARG_LIST((INT4 ));

INT1
nmhSetFsClkIwfUtcOffset ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsClkIwfClockVariance ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsClkIwfClockClass ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsClkIwfClockAccuracy ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsClkIwfClockTimeSource ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsClkIwfCurrentUtcOffset ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsClkIwfARBTime ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsClkIwfHoldoverSpecification ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsClkIwfUtcOffset ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsClkIwfClockVariance ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsClkIwfClockClass ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsClkIwfClockAccuracy ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsClkIwfClockTimeSource ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsClkIwfCurrentUtcOffset ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsClkIwfARBTime ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsClkIwfHoldoverSpecification ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsClkIwfUtcOffset ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsClkIwfGlobalErrTrapType ARG_LIST((INT4 *));

INT1
nmhGetFsClkIwfNotification ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsClkIwfNotification ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsClkIwfNotification ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsClkIwfNotification ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
