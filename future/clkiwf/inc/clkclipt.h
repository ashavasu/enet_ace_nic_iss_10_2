
/**********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 *
 * Description: This has prototype definitions for CLKIWF CLI submodule
 *
 ***********************************************************************/
#ifndef __CLKCLIPT_H__
#define __CLKCLIPT_H__

/**********************************************************************
                        Clkiwcli.c
**********************************************************************/
PRIVATE INT4
ClkIwCliSetClockVariance PROTO ((tCliHandle CliHandle, INT4 i4ClockVariance));

PRIVATE INT4
ClkIwCliSetClockAccuracy PROTO ((tCliHandle CliHandle, INT4 i4ClockAccuracy));

PRIVATE INT4
ClkIwCliSetClockTimeSource PROTO ((tCliHandle CliHandle, INT4 i4ClockTimeSource));

PRIVATE INT4
ClkIwCliSetClockUtcOffset PROTO ((tCliHandle CliHandle, UINT1 * au1ClockUtcOffset));

PRIVATE INT4
ClkIwCliSetClockHoldOver PROTO ((tCliHandle CliHandle, INT4 i4Status));

PRIVATE INT4
ClkIwCliSetClockGlobalTrapType PROTO ((tCliHandle CliHandle, INT4 i4TrapFlag,
                                       INT4 i4TrapValue));

PRIVATE INT4
ClkIwCliShowClockIWFInfo PROTO ((tCliHandle CliHandle));

PRIVATE INT4
ClkIwCliSetClockClass PROTO ((tCliHandle CliHandle, INT4 i4ClockClass));


PRIVATE INT4
ClkIwCliSetClockARBTime PROTO ((tCliHandle CliHandle, UINT1 * au1ClockARBTime));

PRIVATE INT4
ClkIwCliPrintAccuracy PROTO ((tCliHandle CliHandle, INT4 i4ClockAccuracy));

#endif
