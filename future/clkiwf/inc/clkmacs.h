/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 * 
 * $Id: clkmacs.h,v 1.4 2012/01/10 13:07:07 siva Exp $
 *
 * Description: This file contains constant MACROS used in clk Module.
 *
 *****************************************************************************/
#ifndef _CLKMACS_H
#define _CLKMACS_H

#define CLK_DIGIT_MAX   9
#define CLK_DIGIT_MIN   0

#define CLKIWF_CALLBACK       gClkIwfCallBack

#endif /* _CLKMACS_H */
