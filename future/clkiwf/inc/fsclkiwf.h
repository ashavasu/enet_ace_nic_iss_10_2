/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsclkiwf.h,v 1.4 2012/11/27 10:41:31 siva Exp $
*
* Description: MIB OID MAPPING
*********************************************************************/

/*
 * Automatically generated by FuturePostmosy
 * Do not Edit!!!
 */

#ifndef _SNMP_MIB_H
#define _SNMP_MIB_H

/* SNMP-MIB translation table. */
static struct MIB_OID fs_clk_mib_oid_table[] = {
    {"ccitt",        "0"},
    {"iso",        "1"},
    {"lldpExtensions",        "1.0.8802.1.1.2.1.5"},
    {"org",        "1.3"},
    {"dod",        "1.3.6"},
    {"internet",        "1.3.6.1"},
    {"directory",        "1.3.6.1.1"},
    {"mgmt",        "1.3.6.1.2"},
    {"mib-2",        "1.3.6.1.2.1"},
    {"transmission",        "1.3.6.1.2.1.10"},
    {"mplsStdMIB",        "1.3.6.1.2.1.10.166"},
    {"dot1dBridge",        "1.3.6.1.2.1.17"},
    {"dot1dStp",        "1.3.6.1.2.1.17.2"},
    {"dot1dTp",        "1.3.6.1.2.1.17.4"},
    {"vrrpOperEntry",        "1.3.6.1.2.1.18.1.3.1"},
    {"experimental",        "1.3.6.1.3"},
    {"private",        "1.3.6.1.4"},
    {"enterprises",        "1.3.6.1.4.1"},
    {"issExt",        "1.3.6.1.4.1.2076.81.8"},
    {"fsDot1dBridge",        "1.3.6.1.4.1.2076.116"},
    {"fsDot1dStp",        "1.3.6.1.4.1.2076.116.2"},
    {"fsDot1dTp",        "1.3.6.1.4.1.2076.116.4"},
    {"fsClkIwfMIB",        "1.3.6.1.4.1.29601.2.46"},
    {"fsClkIwfObjects",        "1.3.6.1.4.1.29601.2.46.1"},
    {"fsClkIwfGeneralGroup",        "1.3.6.1.4.1.29601.2.46.1.1"},
    {"fsClkIwfClockVariance",        "1.3.6.1.4.1.29601.2.46.1.1.1"},
    {"fsClkIwfClockClass",        "1.3.6.1.4.1.29601.2.46.1.1.2"},
    {"fsClkIwfClockAccuracy",        "1.3.6.1.4.1.29601.2.46.1.1.3"},
    {"fsClkIwfClockTimeSource",        "1.3.6.1.4.1.29601.2.46.1.1.4"},
    {"fsClkIwfCurrentUtcOffset",        "1.3.6.1.4.1.29601.2.46.1.1.5"},
    {"fsClkIwfARBTime",        "1.3.6.1.4.1.29601.2.46.1.1.6"},
    {"fsClkIwfHoldoverSpecification",        "1.3.6.1.4.1.29601.2.46.1.1.7"},
    {"fsClkIwfLostSync",        "1.3.6.1.4.1.29601.2.46.1.1.8"},
    {"fsClkIwfUtcOffset",        "1.3.6.1.4.1.29601.2.46.1.1.9"},
    {"fsClkIwfNotifications",        "1.3.6.1.4.1.29601.2.46.2"},
    {"fsClkIwfTrap",        "1.3.6.1.4.1.29601.2.46.2.0"},
    {"fsClkIwfGlobalErrTrapType",        "1.3.6.1.4.1.29601.2.46.2.1"},
    {"fsClkIwfNotification",        "1.3.6.1.4.1.29601.2.46.2.2"},
    {"security",        "1.3.6.1.5"},
    {"snmpV2",        "1.3.6.1.6"},
    {"snmpDomains",        "1.3.6.1.6.1"},
    {"snmpProxys",        "1.3.6.1.6.2"},
    {"snmpModules",        "1.3.6.1.6.3"},
    {"snmpAuthProtocols",        "1.3.6.1.6.3.10.1.1"},
    {"snmpPrivProtocols",        "1.3.6.1.6.3.10.1.2"},
    {"ieee802dot1mibs",        "1.111.2.802.1"},
    {"joint-iso-ccitt",        "2"},
    {0,0}
};

#endif /* _SNMP_MIB_H */
