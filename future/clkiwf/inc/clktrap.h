/********************************************************************
 * Copyright (C) Aricent Inc . All Rights Reserved
 *
 * $Id: clktrap.h,v 1.3 2013/12/18 12:50:11 siva Exp $
 *
 * Description: This file contains trap data structures defined for
 *              CLK module.
 *********************************************************************/

#ifndef _CLK_TRAP_H
#define _CLK_TRAP_H

UINT4              gau4ClkSnmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
UINT4              gau4ClkTrapOid[] = { 1, 3, 6, 1, 4, 1, 29601, 2, 46, 2, 0 };

CHR1               *gac1ClkSyslogMsg[] = {
    NULL,
    "Clock Parameters Changed",
};

#define CLK_SNMPV2_TRAP_OID_LEN 11
#define CLK_OBJECT_NAME_LEN     256
#define CLK_TRAP_TYPE           1

PRIVATE tSNMP_OID_TYPE *
ClkTrapMakeObjIdFrmString (INT1 *pi1TextStr, UINT1 *pu1TableName);

PRIVATE INT4
ClkTrapParseSubIdNew (UINT1 **ppu1TempPtr, UINT4 *pu4Value);

PRIVATE VOID
ClkTrapFmNotifyFaults (tSNMP_OID_TYPE * pEnterpriseOid, UINT4 u4GenTrapType,
                       UINT4 u4SpecTrapType, tSNMP_VAR_BIND * pTrapMsg);

PRIVATE INT4
ClkTrapIsTrapSet (INT4 i4TrapVal);
#endif /* _CLK_TRAP_H */
