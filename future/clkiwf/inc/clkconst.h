/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: clkconst.h,v 1.9 2016/03/12 10:31:38 siva Exp $
 *
 * Description: This file contains constant definitions used in clk Module.
 *              
 ****************************************************************************/

#ifndef _CLKCONST_H
#define _CLKCONST_H

#define CLK_SEM_NAME    ((UINT1 *)"CLKS")

#define CLK_CLASS        1
#define CLK_VARIANCE     2
#define CLK_ACCURACY     3
#define CLK_HOLDOVER     4

#define CLK_ENABLED  1
#define CLK_DISABLED 2

#define CLK_SYNCHRONIZED_TO_LOCAL  4
#define CLK_SYNCHRONIZED_TO_SNTP_PRIMARY_SERVER 5
#define CLK_SYNCHRONIZED_TO_SNTP_SECONDARY_SERVER 6

#define  CLK_MIN_CLASS    0
#define  CLK_MAX_CLASS    255
#define  CLK_MIN_VARIANCE 0
#define  CLK_MAX_VARIANCE 255

#define  CLK_DEF_CLK_CLASS    248
#define  CLK_DEF_CLK_VARIANCE 0 
#define  CLK_DEF_CLK_ACCURACY 254

/* Clock Accuracy as Per Table 6 IEEE 1588 */

#define CLK_ACCURACY_MIN 0x20  /* Decimal 32 */
#define CLK_ACCURACY_MAX 0xFE  /* Decimal 254 */

#define CLK_MAX_STR_LEN 25

#define CLK_MAX_TRAP_LEN   1

#define CLK_MAX_LEN   6

#define CLK_MAX_MINUTES   59
#define CLK_MAX_UTC_OFFSET1      14
#define CLK_MAX_UTC_OFFSET2    12
#define MAX_VAL_SECONDS       10000000
#define MIN_VAL_SECONDS       100


#define CLK_TRAP_TYPE_GEN_TRAP  1
#define CLK_TRAP_TIME_SRC_CHG   3
#define CLK_TRAP_CLASS_CHG      4
#define CLK_TRAP_ACC_CHG        5
#define CLK_TRAP_VARIANCE_CHG   6
#define CLK_TRAP_HOLD_OVER_CHG  7
#define CLK_MAX_UTC_OFFSET 50
#define CLK_MAX_UTC_OFFSET_SIZE 30

#endif /* _CLKCONST_H */
