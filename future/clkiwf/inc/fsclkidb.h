/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsclkidb.h,v 1.3 2012/11/27 10:41:31 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSCLKIDB_H
#define _FSCLKIDB_H


UINT4 fsclki [] ={1,3,6,1,4,1,29601,2,46};
tSNMP_OID_TYPE fsclkiOID = {9, fsclki};


UINT4 FsClkIwfClockVariance [ ] ={1,3,6,1,4,1,29601,2,46,1,1,1};
UINT4 FsClkIwfClockClass [ ] ={1,3,6,1,4,1,29601,2,46,1,1,2};
UINT4 FsClkIwfClockAccuracy [ ] ={1,3,6,1,4,1,29601,2,46,1,1,3};
UINT4 FsClkIwfClockTimeSource [ ] ={1,3,6,1,4,1,29601,2,46,1,1,4};
UINT4 FsClkIwfCurrentUtcOffset [ ] ={1,3,6,1,4,1,29601,2,46,1,1,5};
UINT4 FsClkIwfARBTime [ ] ={1,3,6,1,4,1,29601,2,46,1,1,6};
UINT4 FsClkIwfHoldoverSpecification [ ] ={1,3,6,1,4,1,29601,2,46,1,1,7};
UINT4 FsClkIwfLostSync [ ] ={1,3,6,1,4,1,29601,2,46,1,1,8};
UINT4 FsClkIwfUtcOffset [ ] ={1,3,6,1,4,1,29601,2,46,1,1,9};
UINT4 FsClkIwfGlobalErrTrapType [ ] ={1,3,6,1,4,1,29601,2,46,2,1};
UINT4 FsClkIwfNotification [ ] ={1,3,6,1,4,1,29601,2,46,2,2};


tMbDbEntry fsclkiMibEntry[]= {

{{12,FsClkIwfClockVariance}, NULL, FsClkIwfClockVarianceGet, FsClkIwfClockVarianceSet, FsClkIwfClockVarianceTest, FsClkIwfClockVarianceDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{12,FsClkIwfClockClass}, NULL, FsClkIwfClockClassGet, FsClkIwfClockClassSet, FsClkIwfClockClassTest, FsClkIwfClockClassDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "248"},

{{12,FsClkIwfClockAccuracy}, NULL, FsClkIwfClockAccuracyGet, FsClkIwfClockAccuracySet, FsClkIwfClockAccuracyTest, FsClkIwfClockAccuracyDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "254"},

{{12,FsClkIwfClockTimeSource}, NULL, FsClkIwfClockTimeSourceGet, FsClkIwfClockTimeSourceSet, FsClkIwfClockTimeSourceTest, FsClkIwfClockTimeSourceDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "64"},

{{12,FsClkIwfCurrentUtcOffset}, NULL, FsClkIwfCurrentUtcOffsetGet, FsClkIwfCurrentUtcOffsetSet, FsClkIwfCurrentUtcOffsetTest, FsClkIwfCurrentUtcOffsetDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,FsClkIwfARBTime}, NULL, FsClkIwfARBTimeGet, FsClkIwfARBTimeSet, FsClkIwfARBTimeTest, FsClkIwfARBTimeDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,FsClkIwfHoldoverSpecification}, NULL, FsClkIwfHoldoverSpecificationGet, FsClkIwfHoldoverSpecificationSet, FsClkIwfHoldoverSpecificationTest, FsClkIwfHoldoverSpecificationDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{12,FsClkIwfLostSync}, NULL, FsClkIwfLostSyncGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, "2"},

{{12,FsClkIwfUtcOffset}, NULL, FsClkIwfUtcOffsetGet, FsClkIwfUtcOffsetSet, FsClkIwfUtcOffsetTest, FsClkIwfUtcOffsetDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsClkIwfGlobalErrTrapType}, NULL, FsClkIwfGlobalErrTrapTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsClkIwfNotification}, NULL, FsClkIwfNotificationGet, FsClkIwfNotificationSet, FsClkIwfNotificationTest, FsClkIwfNotificationDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData fsclkiEntry = { 11, fsclkiMibEntry };

#endif /* _FSCLKIDB_H */

