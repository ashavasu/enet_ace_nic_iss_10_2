
/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secv6tmr.c,v 1.4 2011/07/19 11:35:45 siva Exp $
 *
 * Description: This has functions for Secv6 Timer Submodule
 *
 ***********************************************************************/

#include "secv6com.h"
#include "srmtmri.h"
#define  IP6_TASK_NAME    "IP6"

/***********************************************************************/
/*  Function Name : Secv6InitTimer                                     */
/*  Description   : This function initializes the timer list.          */
/*  Parameter(s)  : VOID                                               */
/*  Return Values : VOID                                               */
/***********************************************************************/
VOID
Secv6InitTimer (VOID)
{
    UINT1               au1TaskName[10];
    IPSEC_STRCPY (au1TaskName, (CONST UINT1 *) IP6_TASK_NAME);
    if (TmrCreateTimerList
        (NULL, (UINT4) IPSEC_TIMER_EXPIRY_EVENT, Secv6ProcessTimeOut,
         &gSecv6TimerListId) != TMR_SUCCESS)
    {
        SECv6_TRC (SECv6_OS_RESOURCE, "Timer Init Failed\n");
    }
    return;
}

/**********************************************************************/
/*  Function Name : Secv6DeInitTimer                                  */
/*  Description   :                                                   */
/*                : This function de-initializes the timer list.      */
/*  Parameter(s)  : VOID                                              */
/*  Return Values : VOID                                              */
/**********************************************************************/

VOID
Secv6DeInitTimer (VOID)
{

    if (TmrDeleteTimerList (gSecv6TimerListId) != TMR_SUCCESS)
    {
        SECv6_TRC (SECv6_OS_RESOURCE, "Timer DeInit Failed\n");
    }
    return;
}

/************************************************************************/
/*  Function Name : Secv6StartTimer                                     */
/*  Description   : This function  starts the timer for the given value.*/
/*  Parameter(s)  : pTimer - Pointer to the timer to be started         */
/*                : TimeOut -  Time out value                           */
/*  Return Values : VOID                                                */
/************************************************************************/
VOID
Secv6StartTimer (tSecv6Timer * pTimer, UINT4 u4TimeOut)
{
    if (TmrStartTimer
        (gSecv6TimerListId, &pTimer->TimerNode,
         (SYS_NUM_OF_TIME_UNITS_IN_A_SEC * u4TimeOut)) != TMR_SUCCESS)
    {
        SECv6_TRC (SECv6_OS_RESOURCE, " Start Timer Failed\n");
    }
    return;

}

/**********************************************************************/
/*  Function Name : Secv6StopTimer                                    */
/*  Description   :                                                   */
/*                : This procedure stops the timer 'pTimer' .         */
/*  Parameter(s)  :                                                   */
/*                : pTimer  -  pointer to the Timer block             */
/*  Return Values : VOID                                              */
/**********************************************************************/

VOID
Secv6StopTimer (tSecv6Timer * pTimer)
{

    if (TmrStopTimer (gSecv6TimerListId, &pTimer->TimerNode) != TMR_SUCCESS)
    {
        SECv6_TRC (SECv6_OS_RESOURCE, " Stop Timer Failed\n");
    }
    return;
}

/*******************************************************************************/
/*  Function Name : Secv6ProcessTimeOut                                        */
/*  Description   :                                                            */
/*                : This procedure calls the appropriate procedures to handle  */
/*                : the timer expiry condition.                                */
/*  Parameter(s)  : None                                                       */
/*  Return Values : VOID                                                       */
/*******************************************************************************/
VOID
Secv6ProcessTimeOut (tTimerListId Secv6TimerListId)
{

    tTimer             *pListHead = NULL;
    tTimer             *pTimer = NULL;
    tSecv6Timer        *pSecv6Tmr = NULL;
    tSecv6Policy       *pSecv6Policy = NULL;

    UNUSED_PARAM (Secv6TimerListId);
#ifndef IKE_WANTED
    UNUSED_PARAM (pSecv6Policy);
#endif

    pListHead = TmrGetNextExpiredTimer (gSecv6TimerListId);
    while (pListHead != NULL)
    {
        pTimer = pListHead;
        pSecv6Tmr = (tSecv6Timer *) pTimer;
        switch (pSecv6Tmr->u1TimerId)
        {

#ifdef IKE_WANTED
            case SECv6_SA_HARD_TIMER_ID:
                SECv6_TRC (SECv6_OS_RESOURCE, "SA Hard Life Timer Expired\n");
                pSecv6Tmr->u1TimerStatus = SEC_TIMER_INACTIVE;
                Secv6IntimateIkeDeletionOfSa ((VOID *) pSecv6Tmr->u4Param1,
                                              pSecv6Tmr->u4SaIndex);
                break;

            case SECv6_SA_SOFT_TIMER_ID:
                SECv6_TRC (SECv6_OS_RESOURCE, "SA Soft Life Timer Expired\n");
                pSecv6Tmr->u1TimerStatus = SEC_TIMER_INACTIVE;
                Secv6RekeySa (pSecv6Tmr);
                break;

            case SECv6_SA_TRIGGER_TIMER_ID:
                SECv6_TRC (SECv6_OS_RESOURCE, "Trigger IKE Timer Expired\n");
                pSecv6Policy = (tSecv6Policy *) pSecv6Tmr->u4Param1;
                pSecv6Policy->u1ReqIkeFlag = SEC_RE_KEY;
                pSecv6Tmr->u1TimerStatus = SEC_TIMER_INACTIVE;
                break;
#endif
            default:
                SECv6_TRC (SECv6_OS_RESOURCE, "Secv6ProcessTimeOut:"
                           "Invalid Timer Id\n");
                break;
        }
        pListHead = TmrGetNextExpiredTimer (gSecv6TimerListId);
    }
    return;
}

#ifdef IKE_WANTED
/*****************************************************************************/
/*  Function Name : Secv6RekeySa                                             */
/*  Description   :                                                          */
/*                : This procedure is used for Rekey ing SA                  */
/*  Parameter(s)  : None                                                     */
/*  Return Values : VOID                                                     */
/*****************************************************************************/
VOID
Secv6RekeySa (tSecv6Timer * pTimer)
{

    tSecv6Selector     *pEntry = NULL;
    tSecv6Policy       *pPolicy = NULL;
    UINT1               u1Flag = SEC_NOT_FOUND;

    pPolicy = (tSecv6Policy *) pTimer->u4Param1;

    TMO_SLL_Scan (&Secv6SelFormattedList, pEntry, tSecv6Selector *)
    {
        if (pEntry->pPolicyEntry->u4PolicyIndex == pPolicy->u4PolicyIndex)
        {
            u1Flag = SEC_FOUND;
            break;
        }
    }
    if (u1Flag == SEC_NOT_FOUND)
    {
        return;
    }

    Secv6RequeStIkeForNewOrReKey (pEntry->u4IfIndex,
                                  &(pEntry->TunnelTermAddr),
                                  pEntry->u4ProtoId,
                                  pEntry->u4Port, pEntry->u4SelAccessIndex,
                                  pEntry->u4PktDirection, SEC_RE_KEY,
                                  pPolicy->u1IkeVersion);
    return;
}
#endif
