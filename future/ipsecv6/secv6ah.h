
/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: secv6ah.h,v 1.3 2013/01/11 12:34:10 siva Exp $
* 
* Description:This file contains the macros required for AH Module.
*
*******************************************************************/


#ifndef _SECV6AH_H_
#define _SECV6AH_H_

/* Macros for authtication header */

#define SEC_AUTH_HEADER_SIZE                    12
#define SEC_AUTH_SEQ_NUM_OFFSET                 8
#define SEC_AUTH_LEN                            1
#define SEC_AUTH_RSVD                           0
#define SEC_AH_FACTOR                           4 

/* ProtoTypes for the functions used in secv4ah.c */

static tBufChainHeader *
       Secv6AhTransportEncode PROTO ((tBufChainHeader * pBuf,
                                      tSecv6Assoc * pSaEntry,UINT4 u4IfIndex));
static tBufChainHeader *
       Secv6AhTunnelEncode PROTO ((tBufChainHeader * pBuf,
                                 tSecv6Assoc * pSaEntry,UINT4 u4Interface));
static tBufChainHeader *
       Secv6AhTransportDecode PROTO ((tBufChainHeader * pBuf,
                              tSecv6Assoc * pSaEntry,UINT4 *pu4ProcessedLen));
static tBufChainHeader *
       Secv6AhTunnelDecode PROTO ((tBufChainHeader * pBuf,
                             tSecv6Assoc * pSaEntry,UINT4 *pu4ProcessedLen));

/* Authentication header */

typedef struct tAUTHHDR {
    UINT1 u1NxtHdr;
    UINT1 u1Len;
    UINT2 u2Rsvd;
    UINT4 u4SPI;
    UINT4 u4SeqNumber;
}tv6AuthHdr;

/* ------------ Global Variables -------- */

UINT1 gau1v6AuthPadding[SEC_ALGO_DIGEST_SIZE] =
      { 0,0,0,0,0,0,0,0,0,0,0,0};

#endif
