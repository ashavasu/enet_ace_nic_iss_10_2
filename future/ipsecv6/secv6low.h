/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: secv6low.h
*
* Description:This file contains the #defines and low level routines
*              for sec module.
*
*******************************************************************/


/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv6SecGlobalStatus ARG_LIST((INT4 *));

INT1
nmhGetFsipv6SecVersion ARG_LIST((UINT4 *));

INT1
nmhGetFsipv6SecGlobalDebug ARG_LIST((INT4 *));

INT1
nmhGetFsipv6SecMaxSA ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsipv6SecGlobalStatus ARG_LIST((INT4 ));

INT1
nmhSetFsipv6SecGlobalDebug ARG_LIST((INT4 ));

INT1
nmhSetFsipv6SecMaxSA ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsipv6SecGlobalStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsipv6SecGlobalDebug ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsipv6SecMaxSA ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsipv6SecGlobalStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsipv6SecGlobalDebug ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsipv6SecMaxSA ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsipv6SecSelectorTable. */
INT1
nmhValidateIndexInstanceFsipv6SecSelectorTable ARG_LIST((INT4  , INT4  , INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsipv6SecSelectorTable  */

INT1
nmhGetFirstIndexFsipv6SecSelectorTable ARG_LIST((INT4 * , INT4 * , INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsipv6SecSelectorTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv6SelFilterFlag ARG_LIST((INT4  , INT4  , INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsipv6SelPolicyIndex ARG_LIST((INT4  , INT4  , INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsipv6SelIfIpAddress ARG_LIST((INT4  , INT4  , INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6SelStatus ARG_LIST((INT4  , INT4  , INT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsipv6SelFilterFlag ARG_LIST((INT4  , INT4  , INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsipv6SelPolicyIndex ARG_LIST((INT4  , INT4  , INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsipv6SelIfIpAddress ARG_LIST((INT4  , INT4  , INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv6SelStatus ARG_LIST((INT4  , INT4  , INT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsipv6SelFilterFlag ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6SelPolicyIndex ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6SelIfIpAddress ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv6SelStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsipv6SecSelectorTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsipv6SecAccessTable. */
INT1
nmhValidateIndexInstanceFsipv6SecAccessTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsipv6SecAccessTable  */

INT1
nmhGetFirstIndexFsipv6SecAccessTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsipv6SecAccessTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv6SecAccessStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6SecSrcNet ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6SecSrcAddrPrefixLen ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6SecDestNet ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6SecDestAddrPrefixLen ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsipv6SecAccessStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6SecSrcNet ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv6SecSrcAddrPrefixLen ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6SecDestNet ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv6SecDestAddrPrefixLen ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsipv6SecAccessStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6SecSrcNet ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv6SecSrcAddrPrefixLen ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6SecDestNet ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv6SecDestAddrPrefixLen ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsipv6SecAccessTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsipv6SecPolicyTable. */
INT1
nmhValidateIndexInstanceFsipv6SecPolicyTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsipv6SecPolicyTable  */

INT1
nmhGetFirstIndexFsipv6SecPolicyTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsipv6SecPolicyTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv6SecPolicyFlag ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6SecPolicyMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6SecPolicySaBundle ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6SecPolicyOptionsIndex ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6SecPolicyStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsipv6SecPolicyFlag ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6SecPolicyMode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6SecPolicySaBundle ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv6SecPolicyOptionsIndex ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6SecPolicyStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsipv6SecPolicyFlag ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6SecPolicyMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6SecPolicySaBundle ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv6SecPolicyOptionsIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6SecPolicyStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsipv6SecPolicyTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsipv6SecAssocTable. */
INT1
nmhValidateIndexInstanceFsipv6SecAssocTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsipv6SecAssocTable  */

INT1
nmhGetFirstIndexFsipv6SecAssocTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsipv6SecAssocTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv6SecAssocDstAddr ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6SecAssocProtocol ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6SecAssocSpi ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6SecAssocMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6SecAssocAhAlgo ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6SecAssocAhKey ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6SecAssocEspAlgo ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6SecAssocEspKey ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6SecAssocEspKey2 ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6SecAssocEspKey3 ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6SecAssocLifetimeInBytes ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6SecAssocLifetime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6SecAssocAntiReplay ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6SecAssocStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsipv6SecAssocDstAddr ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv6SecAssocProtocol ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6SecAssocSpi ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6SecAssocMode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6SecAssocAhAlgo ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6SecAssocAhKey ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv6SecAssocEspAlgo ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6SecAssocEspKey ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv6SecAssocEspKey2 ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv6SecAssocEspKey3 ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv6SecAssocLifetimeInBytes ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6SecAssocLifetime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6SecAssocAntiReplay ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv6SecAssocStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsipv6SecAssocDstAddr ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv6SecAssocProtocol ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6SecAssocSpi ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6SecAssocMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6SecAssocAhAlgo ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6SecAssocAhKey ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv6SecAssocEspAlgo ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6SecAssocEspKey ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv6SecAssocEspKey2 ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv6SecAssocEspKey3 ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv6SecAssocLifetimeInBytes ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6SecAssocLifetime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6SecAssocAntiReplay ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv6SecAssocStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsipv6SecAssocTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsipv6SecIfStatsTable. */
INT1
nmhValidateIndexInstanceFsipv6SecIfStatsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsipv6SecIfStatsTable  */

INT1
nmhGetFirstIndexFsipv6SecIfStatsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsipv6SecIfStatsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv6SecIfInPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6SecIfOutPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6SecIfPktsApply ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6SecIfPktsDiscard ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6SecIfPktsBypass ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for Fsipv6SecAhEspStatsTable. */
INT1
nmhValidateIndexInstanceFsipv6SecAhEspStatsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsipv6SecAhEspStatsTable  */

INT1
nmhGetFirstIndexFsipv6SecAhEspStatsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsipv6SecAhEspStatsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv6SecInAhPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6SecOutAhPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6SecAhPktsAllow ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6SecAhPktsDiscard ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6SecInEspPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6SecOutEspPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6SecEspPktsAllow ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv6SecEspPktsDiscard ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for Fsipv6SecAhEspIntruTable. */
INT1
nmhValidateIndexInstanceFsipv6SecAhEspIntruTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsipv6SecAhEspIntruTable  */

INT1
nmhGetFirstIndexFsipv6SecAhEspIntruTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsipv6SecAhEspIntruTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv6SecAhEspIntruIfIndex ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6SecAhEspIntruSrcAddr ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6SecAhEspIntruDestAddr ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6SecAhEspIntruProto ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv6SecAhEspIntruTime ARG_LIST((INT4 ,UINT4 *));
