/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: secv6ah.c,v 1.10 2013/10/29 11:42:37 siva Exp $
 *  
 * Description: This has functions for AH SubModule
 * 
 ***********************************************************************/

#include "secv6com.h"
#include "secv6ah.h"

/**************************************************************************/
/*  Function Name : Secv6AhEncode                                         */
/*  Description   : This function gets the outgoing packet and            */
/*                : calls either one of the mode of encode                */
/*                : based on SA Entry                                     */
/*  Input(s)      : pBuf - buffer contains IP packet                      */
/*                : pSaEntry - Pointer to a SA Entry                      */
/*                : u4Interface : The Interface on which the packet has   */
/*                :  arrived                                              */
/*  Output(s)     : pBuf - buffer contains IP Secured packet              */
/*  Returns       : Null or Secured Buffer                                */
/***************************************************************************/

tBufChainHeader    *
Secv6AhEncode (tBufChainHeader * pBuf, tSecv6Assoc * pSaEntry,
               UINT4 u4Interface)
{
    switch (pSaEntry->u1SecAssocMode)
    {
        case SEC_TRANSPORT:
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6AhEncode :SecAhTransportEncode Entered \n ");
            pBuf = Secv6AhTransportEncode (pBuf, pSaEntry, u4Interface);
            if (pBuf == NULL)
            {
                SECv6_TRC (SECv6_DATA_PATH,
                           "Secv6AhEncode :SecAhTransportEncode returns NULL \n ");
                return NULL;
            }
            break;

        case SEC_TUNNEL:
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6AhEncode :Secv6AhTunnelEncode Entered \n ");
            pBuf = Secv6AhTunnelEncode (pBuf, pSaEntry, u4Interface);
            if (pBuf == NULL)
            {
                SECv6_TRC (SECv6_DATA_PATH,
                           "Secv6AhEncode :Secv6AhTunnelEncode returns NULL \n ");
                return NULL;
            }
            break;

        default:
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6AhEncode : Neither Transport Nor Tunnel \n");
            return NULL;
    }

    switch (pSaEntry->u1SecAssocMode)
    {
        case SEC_TRANSPORT:
            SECv6_TRC (SECv6_MUST, "\t\t[Secured with AH]\tMode:Transport");
            break;
        case SEC_TUNNEL:
            SECv6_TRC (SECv6_MUST, "\t\t[Secured with AH]\tMode:Tunnel");
            break;
        default:
            break;
    }

    return pBuf;
}

/**********************************************************************/
/*  Function Name : Secv6AhDecode                                    */
/*  Description   : This function gets the incomming packet and       */
/*                : calls either one of the mode of decode            */
/*                : based on SA Entry                                 */
/*  Input(s)      : pBuf - buffer contains IP Secured packet          */
/*                : pSaEntry - Pointer to a SA Entry                  */
/*                : pu4ProcessedLen : Indicates the length up to which*/
/*                : is processed                                      */
/*  Output(s)     : pBuf - buffer contains the Decoded IP packet      */
/*  Return        : NULL or Secured Buffer                            */
/***********************************************************************/
tBufChainHeader    *
Secv6AhDecode (tBufChainHeader * pBuf, tSecv6Assoc * pSaEntry,
               UINT4 *pu4ProcessedLen)
{
    switch (pSaEntry->u1SecAssocMode)
    {
        case SEC_TRANSPORT:
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6AhDecode :Secv6AhTransportDecode Entered \n ");
            pBuf = Secv6AhTransportDecode (pBuf, pSaEntry, pu4ProcessedLen);
            if (pBuf == NULL)
            {
                SECv6_TRC (SECv6_DATA_PATH,
                           "Secv6AhDecode :Secv6AhTransportDecode returns NULL \n ");
                return NULL;
            }
            break;

        case SEC_TUNNEL:
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6AhDecode :Secv6AhTunnelDecode Entered \n ");
            pBuf = Secv6AhTunnelDecode (pBuf, pSaEntry, pu4ProcessedLen);
            if (pBuf == NULL)
            {
                SECv6_TRC (SECv6_DATA_PATH,
                           "Secv6AhDecode : Secv6AhTunnelDecode returns NULL \n");
                return NULL;
            }
            break;

        default:
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6AhDecode : Neither Transport Nor Tunnel \n");
            return NULL;
    }

    switch (pSaEntry->u1SecAssocMode)
    {
        case SEC_TRANSPORT:
            SECv6_TRC (SECv6_MUST,
                       "\t\t[Ah secured pkt decoded]\tMode:Transport");
            break;
        case SEC_TUNNEL:
            SECv6_TRC (SECv6_MUST, "\t\t[Ah secured pkt decoded]\tMode:Tunnel");
            break;
        default:
            break;
    }

    return pBuf;
}

/***********************************************************************/
/*  Function Name : Secv6AhTunnelEncode                                */
/*  Description   : This function gets the outgoing packet and         */
/*                : does tunnel mode encode based on SA Entry          */
/*                :                                                    */
/*  Input(s)      : pBuf - Buffer contains IP packet to be secured     */
/*                : pSaEntry - Pointer to a SA Entry                   */
/*                : u4Interface - Interface on which the packet arrives*/
/*                :                                                    */
/*  Output(s)     : pBuf - buffer contains IP Secured packet           */
/*  Return        : NULL or Secured Buffer                             */
/***********************************************************************/
static tBufChainHeader *Secv6AhTunnelEncode
    (tBufChainHeader * pBuf, tSecv6Assoc * pSaEntry, UINT4 u4Interface)
{
    UINT1               au1AuthDigest[SEC_ALGO_DIGEST_SIZE];
    UINT4               u4Size, u4AuthBufSize = 0;
    UINT1               u1AuthPadding = 4;
    tIp6Hdr             Ip6Hdr, TmpIp6Hdr;
    tv6AuthHdr          AuthHdr;
    UINT1              *pu1Buf = NULL, *pu1AuthBuf = NULL;
    tIp6Addr           *pIp6SrcAddr = NULL;
    UINT1               u1Algorithm = 0;
    unUtilAlgo          UtilAlgo;

    IPSEC_MEMSET (&UtilAlgo, 0, sizeof (unUtilAlgo));
    IPSEC_MEMSET (au1AuthDigest, 0, SEC_ALGO_DIGEST_SIZE);
    IPSEC_MEMSET (&TmpIp6Hdr, 0, sizeof (TmpIp6Hdr));
    IPSEC_MEMSET (&Ip6Hdr, 0, sizeof (Ip6Hdr));
    IPSEC_MEMSET (au1AuthDigest, 0, SEC_ALGO_DIGEST_SIZE);

    /* Extract IPv6 Header from Buffer */
    if (IPSEC_COPY_FROM_BUF (pBuf, (UINT1 *) &TmpIp6Hdr, 0,
                             SEC_IPV6_HEADER_SIZE) == BUF_FAILURE)
    {
        return NULL;
    }

    if (pBuf == NULL)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6AhTunnelEncode : Input Buffer is NULL\n");
        return NULL;
    }

    if (pSaEntry == NULL)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6AhTunnelEncode : Input SA Entry is NULL\n");
        return NULL;
    }

    /* Memory Allocated for Ah header and Auth data */
    if (pSaEntry->u1SecAssocAhAlgo != SEC_NULLAHALGO)
    {
        u1AuthPadding = SEC_AUTH_DIGEST_SIZE;
    }

    u4Size =
        (UINT4) (SEC_AUTH_HEADER_SIZE + u1AuthPadding + SEC_IPV6_HEADER_SIZE);
    SECv6_TRC1 (SECv6_DATA_PATH,
                "Secv6AhTunnelEncode:Ip Hdr+AuthData+Padding= %d\n", u4Size);
    pu1Buf = IPSECv6_MALLOC (u4Size, UINT1);
    if (pu1Buf == NULL)
    {
        SECv6_TRC (SECv6_BUFFER,
                   "Secv6AhTunnelEncode : Malloc fails for pu1Buf \n");
        return NULL;
    }
    SECv6_TRC (SECv6_DATA_PATH,
               "Secv6AhTunnelEncode:Memory Allocated for pu1Buf\n");

    /* Construct New IPv6 Header with all mutable fields to zero */
    Ip6Hdr.u4Head = IPSEC_HEAD;
    Ip6Hdr.u1Nh = SEC_AH;
    Ip6Hdr.u2Len =
        (UINT2) (OSIX_HTONS ((IPSEC_BUF_GetValidBytes
                              (pBuf) + SEC_AUTH_HEADER_SIZE + u1AuthPadding)));
    Ip6Hdr.u1Hlim = 0;
    if ((pIp6SrcAddr =
         Ip6Secv6GetGlobalAddr (u4Interface, &pSaEntry->SecAssocDestAddr))
        == NULL)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6AhTunnelEncode : Error in Getting src addr for a dest addr\n ");
        IPSECv6_MEMFREE (pu1Buf);
        return NULL;
    }
    Ip6AddrCopy (&Ip6Hdr.srcAddr, pIp6SrcAddr);
    Ip6AddrCopy (&Ip6Hdr.dstAddr, &pSaEntry->SecAssocDestAddr);
    /* Construct AH Header */
    AuthHdr.u1NxtHdr = SEC_IP6_ENCAP;
    AuthHdr.u1Len =
        (UINT1) (((SEC_AUTH_HEADER_SIZE + u1AuthPadding) / SEC_AH_FACTOR) - 2);
    AuthHdr.u4SPI = IPSEC_HTONL (pSaEntry->u4SecAssocSpi);
    AuthHdr.u2Rsvd = SEC_AUTH_RSVD;
    pSaEntry->u4SecAssocSeqNumber = pSaEntry->u4SecAssocSeqNumber + 1;
    AuthHdr.u4SeqNumber = IPSEC_HTONL (pSaEntry->u4SecAssocSeqNumber);

    /* Copy New Ipv6 Header, Auth Header and padding to linear buf */
    IPSEC_MEMCPY (pu1Buf, &Ip6Hdr, SEC_IPV6_HEADER_SIZE);
    IPSEC_MEMCPY (pu1Buf + SEC_IPV6_HEADER_SIZE, &AuthHdr,
                  SEC_AUTH_HEADER_SIZE);
    IPSEC_MEMCPY (pu1Buf + SEC_IPV6_HEADER_SIZE + SEC_AUTH_HEADER_SIZE,
                  gau1v6AuthPadding, u1AuthPadding);

    if (IPSEC_BUF_Prepend (pBuf, pu1Buf, u4Size) == BUF_FAILURE)
    {
        IPSECv6_MEMFREE (pu1Buf);
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6AhTunnelEncode :Unable to prepend Auth and New IP Header to Packet \n ");
        return NULL;
    }

    IPSECv6_MEMFREE (pu1Buf);

    u4AuthBufSize = IPSEC_BUF_GetValidBytes (pBuf);

    if ((pu1AuthBuf = IPSECv6_MALLOC (u4AuthBufSize, UINT1)) == NULL)
    {

        return NULL;
    }

    if (IPSEC_COPY_FROM_BUF (pBuf, pu1AuthBuf, 0, u4AuthBufSize) == BUF_FAILURE)
    {
        IPSECv6_MEMFREE (pu1AuthBuf);
        return NULL;
    }

    /* Generate Auth Data */
    switch (pSaEntry->u1SecAssocAhAlgo)
    {

        case SEC_HMACMD5:
            u1Algorithm = ISS_UTIL_ALGO_HMAC_MD5;
            break;

        case SEC_HMACSHA1:
            u1Algorithm = ISS_UTIL_ALGO_HMAC_SHA1;
            UtilAlgo.UtilHmacAlgo.HmacShaVersion = AR_SHA1_ALGO;
            break;

        case HMAC_SHA_256:
            u1Algorithm = ISS_UTIL_ALGO_HMAC_SHA2;
            UtilAlgo.UtilHmacAlgo.HmacShaVersion = AR_SHA256_ALGO;
            break;

        case HMAC_SHA_384:
            u1Algorithm = ISS_UTIL_ALGO_HMAC_SHA2;
            UtilAlgo.UtilHmacAlgo.HmacShaVersion = AR_SHA384_ALGO;
            break;

        case HMAC_SHA_512:
            u1Algorithm = ISS_UTIL_ALGO_HMAC_SHA2;
            UtilAlgo.UtilHmacAlgo.HmacShaVersion = AR_SHA512_ALGO;
            break;
        default:
            break;
    }

    switch (pSaEntry->u1SecAssocAhAlgo)
    {
        case SEC_HMACMD5:
        case SEC_HMACSHA1:
        case HMAC_SHA_256:
        case HMAC_SHA_384:
        case HMAC_SHA_512:
            UtilAlgo.UtilHmacAlgo.pu1HmacKey = pSaEntry->pu1SecAssocAhKey1;
            UtilAlgo.UtilHmacAlgo.u4HmacKeyLen =
                pSaEntry->u1SecAssocAhKeyLength;
            UtilAlgo.UtilHmacAlgo.pu1HmacPktDataPtr = pu1AuthBuf;
            UtilAlgo.UtilHmacAlgo.i4HmacPktLength = (INT4) u4AuthBufSize;
            UtilAlgo.UtilHmacAlgo.pu1HmacOutDigest = au1AuthDigest;
            UtilAlgo.UtilHmacAlgo.u4HmacOutDigestLen = SEC_AUTH_DIGEST_SIZE;
            UtilHash (u1Algorithm, &UtilAlgo);
            break;

        case SEC_XCBCMAC:
            UtilAlgo.UtilAesAlgo.pu1AesInUserKey = pSaEntry->pu1SecAssocAhKey1;
            UtilAlgo.UtilAesAlgo.u4AesInKeyLen =
                pSaEntry->u1SecAssocAhKeyLength;
            UtilAlgo.UtilAesAlgo.pu1AesInBuf = pu1AuthBuf;
            UtilAlgo.UtilAesAlgo.u4AesInBufSize = u4AuthBufSize;
            UtilAlgo.UtilAesAlgo.pu1AesMac = au1AuthDigest;
            UtilMac (ISS_UTIL_ALGO_AES_CBC_MAC96, &UtilAlgo);
            break;

        case SEC_MD5:
            UtilAlgo.UtilMd5Algo.pu1Md5InBuf = pu1AuthBuf;
            UtilAlgo.UtilMd5Algo.u4Md5InBufLen = u4AuthBufSize;
            UtilAlgo.UtilMd5Algo.pu1Md5OutDigest = au1AuthDigest;
            UtilMac (ISS_UTIL_ALGO_MD5_ALGO, &UtilAlgo);
            break;

        case SEC_KEYEDMD5:
            KeyedMd5 (pu1AuthBuf, (INT4) u4AuthBufSize,
                      pSaEntry->pu1SecAssocAhKey1,
                      pSaEntry->u1SecAssocAhKeyLength, au1AuthDigest);
            break;

        case SEC_NULLAHALGO:
            break;

        default:
            SECv6_TRC (SECv6_DATA_PATH, " Unknown Auth Algo \n");
            IPSECv6_MEMFREE (pu1AuthBuf);
            return NULL;

    }

    IPSECv6_MEMFREE (pu1AuthBuf);

    /* Copy the Authentication data to the buffer */
    if (pSaEntry->u1SecAssocAhAlgo != SEC_NULLAHALGO)
    {
        if (IPSEC_COPY_OVER_BUF (pBuf, (UINT1 *) &au1AuthDigest,
                                 SEC_IPV6_HEADER_SIZE + SEC_AUTH_HEADER_SIZE,
                                 u1AuthPadding) == BUF_FAILURE)
        {
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6AhTunnelEncode : Unable to write Auth Data to Pakcet \n");
            return NULL;
        }
    }

    /* Modify muted fields content in the new IP heade with 
     * the original IP header content */

    Ip6Hdr.u1Hlim = TmpIp6Hdr.u1Hlim;
    Ip6Hdr.u4Head = TmpIp6Hdr.u4Head;

    if (IPSEC_COPY_OVER_BUF (pBuf, (UINT1 *) &Ip6Hdr, 0,
                             SEC_IPV6_HEADER_SIZE) == BUF_FAILURE)
    {
        return NULL;
    }

    return pBuf;
}

/************************************************************************/
/*  Function Name : Secv6AhTunnelDecode                                 */
/*  Description   : This function gets the incomming packet and         */
/*                : does tunnel mode decode                             */
/*                : based on SA Entry                                   */
/*  Input(s)      : pBuf - buffer contains IP Secured packet            */
/*                : pSaEntry - Pointer to a SA Entry                    */
/*                : pu4ProcessedLen : Inicates the length upto which    */
/*                : packet is processed                                 */
/*                :                                                     */
/*  Output(s)     : pBuf - Buffer contains Decoded IP packet            */
/*  Return Values : NULL or Secured Buffer                              */
/***********************************************************************/

static tBufChainHeader *
Secv6AhTunnelDecode (tBufChainHeader * pBuf, tSecv6Assoc * pSaEntry,
                     UINT4 *pu4ProcessedLen)
{
    tv6AuthHdr          AuthHdr;
    UINT4               u4Head = IPSEC_HEAD, u4AuthBufSize = 0;
    UINT1               u1Hlim = 0;
    UINT4               u4AuthDataSize;
    UINT4               u4SeqNumber;
    UINT1              *pu1AuthData = NULL;
    UINT1              *pu1AuthPadding = NULL;
    UINT1               au1AuthDigest[SEC_ALGO_DIGEST_SIZE] = { 0 };
    UINT1              *pu1AuthBuf = NULL;
    UINT1               u1Algorithm = 0;
    tIp6Hdr             Ipv6Hdr;
    unUtilAlgo          UtilAlgo;

    IPSEC_MEMSET (&UtilAlgo, 0, sizeof (unUtilAlgo));

    if (IPSEC_COPY_FROM_BUF (pBuf, (UINT1 *) &Ipv6Hdr, 0,
                             SEC_IPV6_HEADER_SIZE) == BUF_FAILURE)
    {
        return NULL;
    }

    if (IPSEC_COPY_FROM_BUF (pBuf, (UINT1 *)
                             &AuthHdr, (*pu4ProcessedLen),
                             SEC_AUTH_HEADER_SIZE) == BUF_FAILURE)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6AhTunnelDecode : Unable to read Auth Header from Packet \n");
        return NULL;
    }

    if (pSaEntry->u1SecAssocAhAlgo == SEC_NULLAHALGO)
    {
        if (AuthHdr.u1Len != 2)
        {
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6AhTunnelDecode : Invalid Length in Packet\n");
            return NULL;
        }
    }

    u4AuthDataSize =
        (UINT4) (((AuthHdr.u1Len + 2) * SEC_AH_FACTOR) - SEC_AUTH_HEADER_SIZE);

    /* Verify Sequence Number */
    if (IPSEC_COPY_FROM_BUF (pBuf, (UINT1 *) &u4SeqNumber, (*pu4ProcessedLen)
                             + SEC_AUTH_SEQ_NUM_OFFSET,
                             SEC_SEQ_NUMBER_LEN) == BUF_FAILURE)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6AhTunnelDecode : Unable to read SeqNumber from Packet\n");
        return NULL;
    }

    if (pSaEntry->u1AntiReplayStatus == SEC_ANTI_REPLAY_ENABLE)
    {
        if (Secv6SeqNumberVerification
            (OSIX_NTOHL (u4SeqNumber), pSaEntry) == SEC_FAILURE)
        {
            SECv6_TRC1 (SECv6_DATA_PATH,
                        "Secv6AhTunnelDecode : Replay attack from %s \n",
                        Ip6PrintAddr (&Ipv6Hdr.srcAddr));
            return NULL;
        }
    }

    Ipv6Hdr.u1Hlim = u1Hlim;
    Ipv6Hdr.u4Head = u4Head;

    if (IPSEC_COPY_OVER_BUF (pBuf, (UINT1 *) &Ipv6Hdr, 0,
                             SEC_IPV6_HEADER_SIZE) == BUF_FAILURE)
    {
        return NULL;
    }

    /* Copy Auth data to local linear buffer */
    if (pSaEntry->u1SecAssocAhAlgo != SEC_NULLAHALGO)
    {
        pu1AuthData = IPSECv6_MALLOC (u4AuthDataSize, UINT1);

        if (pu1AuthData == NULL)
        {
            SECv6_TRC (BUFFER_TRC,
                       "Secv6AhTunnelDecode : Malloc fails for pu1AuthData\n");
            return NULL;
        }

        /* Make Auth data Fields Zero in the Packet */
        pu1AuthPadding = IPSECv6_MALLOC (u4AuthDataSize, UINT1);
        if (pu1AuthPadding == NULL)
        {
            SECv6_TRC (BUFFER_TRC,
                       "Secv6AhTunnelDecode : Malloc fails for AuthPadding\n");
            IPSECv6_MEMFREE (pu1AuthData);
            return NULL;
        }
        IPSEC_MEMSET (pu1AuthPadding, 0, u4AuthDataSize);
        if (IPSEC_COPY_FROM_BUF (pBuf, pu1AuthData, (*pu4ProcessedLen)
                                 + SEC_AUTH_HEADER_SIZE,
                                 u4AuthDataSize) == BUF_FAILURE)
        {
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6AhTunnelDecode : Unable to read Auth data from Packet\n");
            IPSECv6_MEMFREE (pu1AuthData);
            IPSECv6_MEMFREE (pu1AuthPadding);
            return NULL;
        }
        if (IPSEC_COPY_OVER_BUF (pBuf, pu1AuthPadding, (*pu4ProcessedLen)
                                 + SEC_AUTH_HEADER_SIZE,
                                 u4AuthDataSize) == BUF_FAILURE)
        {
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6AhTunnelDecode : Unable to write Auth Padding to Packet\n");
            IPSECv6_MEMFREE (pu1AuthData);
            IPSECv6_MEMFREE (pu1AuthPadding);
            return NULL;
        }

        IPSECv6_MEMFREE (pu1AuthPadding);

        u4AuthBufSize = IPSEC_BUF_GetValidBytes (pBuf);

        if ((pu1AuthBuf = IPSECv6_MALLOC (u4AuthBufSize, UINT1)) == NULL)
        {

            IPSECv6_MEMFREE (pu1AuthData);
            return NULL;
        }

        if (IPSEC_COPY_FROM_BUF (pBuf, pu1AuthBuf, 0,
                                 u4AuthBufSize) == BUF_FAILURE)
        {
            IPSECv6_MEMFREE (pu1AuthBuf);
            IPSECv6_MEMFREE (pu1AuthData);
            return NULL;
        }

        /* Generate Auth Data */
        switch (pSaEntry->u1SecAssocAhAlgo)
        {

            case SEC_HMACMD5:
                u1Algorithm = ISS_UTIL_ALGO_HMAC_MD5;
                break;

            case SEC_HMACSHA1:
                u1Algorithm = ISS_UTIL_ALGO_HMAC_SHA1;
                UtilAlgo.UtilHmacAlgo.HmacShaVersion = AR_SHA1_ALGO;
                break;

            case HMAC_SHA_256:
                u1Algorithm = ISS_UTIL_ALGO_HMAC_SHA2;
                UtilAlgo.UtilHmacAlgo.HmacShaVersion = AR_SHA256_ALGO;
                break;

            case HMAC_SHA_384:
                u1Algorithm = ISS_UTIL_ALGO_HMAC_SHA2;
                UtilAlgo.UtilHmacAlgo.HmacShaVersion = AR_SHA384_ALGO;
                break;

            case HMAC_SHA_512:
                u1Algorithm = ISS_UTIL_ALGO_HMAC_SHA2;
                UtilAlgo.UtilHmacAlgo.HmacShaVersion = AR_SHA512_ALGO;
                break;
            default:
                break;
        }

        switch (pSaEntry->u1SecAssocAhAlgo)
        {
            case SEC_HMACMD5:
            case SEC_HMACSHA1:
            case HMAC_SHA_256:
            case HMAC_SHA_384:
            case HMAC_SHA_512:
                UtilAlgo.UtilHmacAlgo.pu1HmacKey = pSaEntry->pu1SecAssocAhKey1;
                UtilAlgo.UtilHmacAlgo.u4HmacKeyLen =
                    pSaEntry->u1SecAssocAhKeyLength;
                UtilAlgo.UtilHmacAlgo.pu1HmacPktDataPtr = pu1AuthBuf;
                UtilAlgo.UtilHmacAlgo.i4HmacPktLength = (INT4) u4AuthBufSize;
                UtilAlgo.UtilHmacAlgo.pu1HmacOutDigest = au1AuthDigest;
                UtilAlgo.UtilHmacAlgo.u4HmacOutDigestLen = SEC_AUTH_DIGEST_SIZE;
                UtilHash (u1Algorithm, &UtilAlgo);
                break;

            case SEC_XCBCMAC:
                UtilAlgo.UtilAesAlgo.pu1AesInUserKey =
                    pSaEntry->pu1SecAssocAhKey1;
                UtilAlgo.UtilAesAlgo.u4AesInKeyLen =
                    pSaEntry->u1SecAssocAhKeyLength;
                UtilAlgo.UtilAesAlgo.pu1AesInBuf = pu1AuthBuf;
                UtilAlgo.UtilAesAlgo.u4AesInBufSize = u4AuthBufSize;
                UtilAlgo.UtilAesAlgo.pu1AesMac = au1AuthDigest;
                UtilMac (ISS_UTIL_ALGO_AES_CBC_MAC96, &UtilAlgo);
                break;

            case SEC_MD5:
                UtilAlgo.UtilMd5Algo.pu1Md5InBuf = pu1AuthBuf;
                UtilAlgo.UtilMd5Algo.u4Md5InBufLen = u4AuthBufSize;
                UtilAlgo.UtilMd5Algo.pu1Md5OutDigest = au1AuthDigest;
                UtilMac (ISS_UTIL_ALGO_MD5_ALGO, &UtilAlgo);
                break;

            case SEC_KEYEDMD5:
                KeyedMd5 (pu1AuthBuf, (INT4) u4AuthBufSize,
                          pSaEntry->pu1SecAssocAhKey1,
                          pSaEntry->u1SecAssocAhKeyLength, au1AuthDigest);
                break;

            case SEC_NULLAHALGO:
                break;

            default:
                SECv6_TRC (SECv6_DATA_PATH, " Unknown Auth Algo \n");
                IPSECv6_MEMFREE (pu1AuthData);
                IPSECv6_MEMFREE (pu1AuthBuf);
                return NULL;

        }

        IPSECv6_MEMFREE (pu1AuthBuf);

        /* Verify Authentication Data */
        if (IPSEC_MEMCMP (au1AuthDigest, pu1AuthData, u4AuthDataSize) != 0)
        {

            SECv6_TRC (SECv6_MUST, "Ah transport decode:\t icv check failed\n");
            IPSECv6_MEMFREE (pu1AuthData);
            return NULL;
        }
        IPSECv6_MEMFREE (pu1AuthData);
    }

    /* Remove IP header and AH header from the Packet */
    if (IPSEC_BUF_MoveOffset (pBuf, SEC_IPV6_HEADER_SIZE
                              + SEC_AUTH_HEADER_SIZE + u4AuthDataSize) ==
        BUF_FAILURE)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6AhTunnelDecode : Unable Remove Ah and Ip Header from PAcket\n");
        return NULL;
    }
    *pu4ProcessedLen = SEC_IPV6_HEADER_SIZE;
    return (pBuf);
}

/**********************************************************************/
/*  Function Name : Secv6AhTransportEncode                            */
/*  Description   : This function gets the outgoing packet and        */
/*                : does transport mode encode based on sa entry      */
/*                :                                                   */
/*  Input(s)      : pBuf - buffer contains IP packet                  */
/*                : pSaEntry - Pointer to a SA Entry                  */
/*                :                                                   */
/*  Output(s)     : pBuf - buffer contains secured Ip packet          */
/*  Return Values : NULL or Secured Buffer                            */
/***********************************************************************/

static tBufChainHeader *
Secv6AhTransportEncode (tBufChainHeader * pBuf, tSecv6Assoc * pSaEntry,
                        UINT4 u4IfIndex)
{
    UINT1               u1HopLimit;
    UINT4               u4Head;
    UINT2               u2Len;
    UINT4               u4AuthBufSize = 0;
    UINT1               u1NxtHdr;
    UINT1               u1AuthPadding = 4;
    UINT1               au1AuthDigest[SEC_ALGO_DIGEST_SIZE];
    tv6AuthHdr          AuthHdr;
    tIp6Hdr             Ipv6Hdr;
    UINT1              *pu1AuthBuf = NULL;
    UINT1               u1OptLen = 0;
    UINT1              *pu1OptBuf = NULL;
    UINT1               u1Algorithm = 0;
    unUtilAlgo          UtilAlgo;

    IPSEC_MEMSET (&UtilAlgo, 0, sizeof (unUtilAlgo));

    if (IPSEC_COPY_FROM_BUF (pBuf, (UINT1 *) &Ipv6Hdr, 0,
                             SEC_IPV6_HEADER_SIZE) == BUF_FAILURE)
    {
        return NULL;
    }

    if (Ip6Secv6IsOurAddr (&(Ipv6Hdr.srcAddr), &(u4IfIndex)) != IP6_SUCCESS)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6AhTransportEncode Attempting to Apply Transport Mode SA"
                   "for the System acting as a Gateway\n");
        return SEC_FAILURE;
    }

    /* Mute the mutable options fields and copy the AH protocol in 
       the last option field header */

    if (Secv6GetOptionsParams (pBuf, &Ipv6Hdr, SEC_AH, &u1NxtHdr,
                               &u1OptLen) != SEC_SUCCESS)
    {
        return NULL;
    }

    if (u1OptLen > 0)
    {
        pu1OptBuf = IPSECv6_MALLOC (u1OptLen, UINT1);

        if (pu1OptBuf == NULL)
        {
            return NULL;
        }

        /* Make a copy of the options before muting */
        if (IPSEC_COPY_FROM_BUF (pBuf, pu1OptBuf, SEC_IPV6_HEADER_SIZE,
                                 u1OptLen) == BUF_FAILURE)
        {
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6AhTransportEncode : Unable to read Next Hdr from Packet\n");
            IPSECv6_MEMFREE (pu1OptBuf);
            return NULL;
        }

        /* mute the mutable option fields in the buffer */
        if (Secv6ProcessOptions (pBuf, &Ipv6Hdr, SEC_AH,
                                 &u1NxtHdr, &u1OptLen) != SEC_SUCCESS)
        {
            IPSECv6_MEMFREE (pu1OptBuf);
            return NULL;
        }
    }

    /* Store the original contents and mute all mutable fields in the 
       IP header */
    u1HopLimit = Ipv6Hdr.u1Hlim;
    Ipv6Hdr.u1Hlim = 0;
    u4Head = Ipv6Hdr.u4Head;
    Ipv6Hdr.u4Head = IPSEC_HEAD;
    Ipv6Hdr.u1Nh = SEC_AH;

    if (IPSEC_COPY_OVER_BUF (pBuf, (UINT1 *) &Ipv6Hdr, 0,
                             SEC_IPV6_HEADER_SIZE) == BUF_FAILURE)
    {
        IPSECv6_MEMFREE (pu1OptBuf);
        return NULL;
    }

    if (pSaEntry->u1SecAssocAhAlgo != SEC_NULLAHALGO)
    {
        u1AuthPadding = SEC_AUTH_DIGEST_SIZE;
    }

    /* Form AH header  */
    AuthHdr.u1NxtHdr = u1NxtHdr;
    AuthHdr.u1Len =
        (UINT1) (((SEC_AUTH_HEADER_SIZE + u1AuthPadding) / SEC_AH_FACTOR) - 2);
    AuthHdr.u2Rsvd = SEC_AUTH_RSVD;
    AuthHdr.u4SPI = IPSEC_HTONL (pSaEntry->u4SecAssocSpi);
    pSaEntry->u4SecAssocSeqNumber = pSaEntry->u4SecAssocSeqNumber + 1;
    AuthHdr.u4SeqNumber = IPSEC_HTONL (pSaEntry->u4SecAssocSeqNumber);

    /* Add AuthHdr and Padding to pBuf  */
    if (Secv6AddExtraHeaderToIP (pBuf, (UINT1 *) &AuthHdr, u1OptLen,
                                 gau1v6AuthPadding, u1AuthPadding) ==
        SEC_FAILURE)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6AhTransportEncode : Unable to add Auth header\n");
        if (pu1OptBuf != NULL)
        {
            IPSECv6_MEMFREE (pu1OptBuf);
        }
        return NULL;
    }

    u2Len = (UINT2) (IPSEC_BUF_GetValidBytes (pBuf));
    u2Len = IPSEC_HTONS ((u2Len - SEC_IPV6_HEADER_SIZE));
    Ipv6Hdr.u2Len = u2Len;
    if (IPSEC_COPY_OVER_BUF (pBuf, (UINT1 *) &u2Len, SEC_IPV6_HEADER_LEN_OFFSET,
                             SEC_IPV6_HEADER_LEN) == BUF_FAILURE)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6AhTransportEncode : Unable to write HopLimit to Packet\n");
        if (pu1OptBuf)
        {
            IPSECv6_MEMFREE (pu1OptBuf);
        }
        return NULL;
    }

    u4AuthBufSize = IPSEC_BUF_GetValidBytes (pBuf);

    if ((pu1AuthBuf = IPSECv6_MALLOC (u4AuthBufSize, UINT1)) == NULL)
    {
        IPSECv6_MEMFREE (pu1OptBuf);
        return NULL;
    }

    if (IPSEC_COPY_FROM_BUF (pBuf, pu1AuthBuf, 0, u4AuthBufSize) == BUF_FAILURE)
    {
        if (pu1OptBuf)
        {
            IPSECv6_MEMFREE (pu1OptBuf);
        }
        IPSECv6_MEMFREE (pu1AuthBuf);
        return NULL;
    }

    /* Generate Authentication data and insert to pBuf */
    switch (pSaEntry->u1SecAssocAhAlgo)
    {
        case SEC_HMACMD5:
            u1Algorithm = ISS_UTIL_ALGO_HMAC_MD5;
            break;

        case SEC_HMACSHA1:
            u1Algorithm = ISS_UTIL_ALGO_HMAC_SHA1;
            UtilAlgo.UtilHmacAlgo.HmacShaVersion = AR_SHA1_ALGO;
            break;

        case HMAC_SHA_256:
            u1Algorithm = ISS_UTIL_ALGO_HMAC_SHA2;
            UtilAlgo.UtilHmacAlgo.HmacShaVersion = AR_SHA256_ALGO;
            break;

        case HMAC_SHA_384:
            u1Algorithm = ISS_UTIL_ALGO_HMAC_SHA2;
            UtilAlgo.UtilHmacAlgo.HmacShaVersion = AR_SHA384_ALGO;
            break;

        case HMAC_SHA_512:
            u1Algorithm = ISS_UTIL_ALGO_HMAC_SHA2;
            UtilAlgo.UtilHmacAlgo.HmacShaVersion = AR_SHA512_ALGO;
            break;
        default:
            break;
    }

    switch (pSaEntry->u1SecAssocAhAlgo)
    {
        case SEC_HMACMD5:
        case SEC_HMACSHA1:
        case HMAC_SHA_256:
        case HMAC_SHA_384:
        case HMAC_SHA_512:
            UtilAlgo.UtilHmacAlgo.pu1HmacKey = pSaEntry->pu1SecAssocAhKey1;
            UtilAlgo.UtilHmacAlgo.u4HmacKeyLen =
                pSaEntry->u1SecAssocAhKeyLength;
            UtilAlgo.UtilHmacAlgo.pu1HmacPktDataPtr = pu1AuthBuf;
            UtilAlgo.UtilHmacAlgo.i4HmacPktLength = (INT4) u4AuthBufSize;
            UtilAlgo.UtilHmacAlgo.pu1HmacOutDigest = au1AuthDigest;
            UtilAlgo.UtilHmacAlgo.u4HmacOutDigestLen = SEC_AUTH_DIGEST_SIZE;
            UtilHash (u1Algorithm, &UtilAlgo);
            break;

        case SEC_XCBCMAC:
            UtilAlgo.UtilAesAlgo.pu1AesInUserKey = pSaEntry->pu1SecAssocAhKey1;
            UtilAlgo.UtilAesAlgo.u4AesInKeyLen =
                pSaEntry->u1SecAssocAhKeyLength;
            UtilAlgo.UtilAesAlgo.pu1AesInBuf = pu1AuthBuf;
            UtilAlgo.UtilAesAlgo.u4AesInBufSize = u4AuthBufSize;
            UtilAlgo.UtilAesAlgo.pu1AesMac = au1AuthDigest;
            UtilMac (ISS_UTIL_ALGO_AES_CBC_MAC96, &UtilAlgo);
            break;

        case SEC_MD5:
            UtilAlgo.UtilMd5Algo.pu1Md5InBuf = pu1AuthBuf;
            UtilAlgo.UtilMd5Algo.u4Md5InBufLen = u4AuthBufSize;
            UtilAlgo.UtilMd5Algo.pu1Md5OutDigest = au1AuthDigest;
            UtilMac (ISS_UTIL_ALGO_MD5_ALGO, &UtilAlgo);
            break;

        case SEC_KEYEDMD5:
            KeyedMd5 (pu1AuthBuf, (INT4) u4AuthBufSize,
                      pSaEntry->pu1SecAssocAhKey1,
                      pSaEntry->u1SecAssocAhKeyLength, au1AuthDigest);
            break;

        case SEC_NULLAHALGO:
            break;

        default:
            SECv6_TRC (SECv6_DATA_PATH, " Unknown Auth Algo \n");
            if (pu1OptBuf != NULL)
            {
                IPSECv6_MEMFREE (pu1OptBuf);
            }
            IPSECv6_MEMFREE (pu1AuthBuf);
            return NULL;

    }

    IPSECv6_MEMFREE (pu1AuthBuf);

    /* Copy Auth data to pBuf */
    if (pSaEntry->u1SecAssocAhAlgo != SEC_NULLAHALGO)
    {
        if (IPSEC_COPY_OVER_BUF
            (pBuf,
             au1AuthDigest,
             SEC_IPV6_HEADER_SIZE
             + SEC_AUTH_HEADER_SIZE, u1AuthPadding) == BUF_FAILURE)
        {
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6AhTransportEncode : Unable to write Auth Data to Packet\n");
            if (pu1OptBuf != NULL)
            {
                IPSECv6_MEMFREE (pu1OptBuf);
            }
            return NULL;
        }
    }

    /* Restore muted fields */
    Ipv6Hdr.u1Hlim = u1HopLimit;
    Ipv6Hdr.u4Head = u4Head;

    IPSEC_COPY_OVER_BUF (pBuf, (UINT1 *) &Ipv6Hdr, 0, SEC_IPV6_HEADER_SIZE);

    /* Restore option fields if present */
    if (u1OptLen > 0)
    {
        if (IPSEC_COPY_OVER_BUF (pBuf, pu1OptBuf, SEC_IPV6_HEADER_SIZE,
                                 u1OptLen) == BUF_FAILURE)
        {
            SECv6_TRC
                (SECv6_DATA_PATH,
                 "Secv6AhTransportEncode : Unable to write options to Packet\n");
            IPSECv6_MEMFREE (pu1OptBuf);
            return NULL;
        }
    }
    IPSECv6_MEMFREE (pu1OptBuf);
    return pBuf;
}

/********************************************************************/
/*  Function Name : Secv6AhTransportDecode                          */
/*  Description   : This function gets the incomming packet and     */
/*                : does transport mode decode                      */
/*                : based on SA Entry                               */
/*  Input(s)      : pBuf - buffer contains IP Secured packet        */
/*                : pSaEntry - Pointer to a SA Entry                */
/*  Output(s)     : pBuf - buffer contains IP packet                */
/*  Global(s)     : None                                            */
/*  Return Values : NULL or Secured Buffer                          */
/*********************************************************************/

static tBufChainHeader *
Secv6AhTransportDecode (tBufChainHeader * pBuf, tSecv6Assoc * pSaEntry,
                        UINT4 *pu4ProcessedLen)
{
    UINT1              *pu1Buf = NULL, *pu1AuthBuf = NULL;
    UINT1              *pu1PaddingBuf = NULL, *pu1OptBuf = NULL;
    UINT1               u1Len;
    UINT4               u4AuthDataSize;
    UINT1               au1AuthDigest[SEC_ALGO_DIGEST_SIZE] = { 0 };
    UINT1               u1HopLimit;
    UINT4               u4Head, u4OptLen;
    UINT2               u2Len;
    UINT4               u4SeqNumber, u4AuthBufSize = 0;
    tIp6Hdr             Ipv6Hdr;
    UINT1               u1NxtHdr, u1OptLen, u1HlHdr;
    UINT1               u1Algorithm = 0;
    unUtilAlgo          UtilAlgo;

    IPSEC_MEMSET (&UtilAlgo, 0, sizeof (unUtilAlgo));

    /* Get the IPv6 Header from the Buffer */
    if (IPSEC_COPY_FROM_BUF (pBuf, (UINT1 *) &Ipv6Hdr, 0,
                             SEC_IPV6_HEADER_SIZE) == BUF_FAILURE)
    {
        return NULL;
    }

    if (IPSEC_COPY_FROM_BUF (pBuf, (UINT1 *) &u1Len, (*pu4ProcessedLen) +
                             SEC_AUTH_LEN, SEC_AUTH_LEN) == BUF_FAILURE)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6AhTransportDecode : Unable to read u1Length from Packet\n");
        return NULL;
    }

    if (pSaEntry->u1SecAssocAhAlgo == SEC_NULLAHALGO)
    {
        if (u1Len != 2)
        {
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6AhTransportDecode : Invalid Length in Packet\n");
            return NULL;
        }
    }

    /* Verify SeqNumber */
    if (IPSEC_COPY_FROM_BUF (pBuf, (UINT1 *) &u4SeqNumber, (*pu4ProcessedLen)
                             + SEC_AUTH_SEQ_NUM_OFFSET,
                             SEC_SEQ_NUMBER_LEN) == BUF_FAILURE)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6AhTransportDecode : Unable to read SeqNumber from Packet\n");
        return NULL;
    }

    if (pSaEntry->u1AntiReplayStatus == SEC_ANTI_REPLAY_ENABLE)
    {
        if (Secv6SeqNumberVerification (IPSEC_NTOHL (u4SeqNumber),
                                        pSaEntry) == SEC_FAILURE)
        {
            SECv6_TRC1 (SECv6_DATA_PATH,
                        "Secv6AhTransportDecode : Replay attack from %s\n",
                        Ip6PrintAddr (&Ipv6Hdr.srcAddr));
            return NULL;
        }
    }

    /* Make a copy of the mutable options and mute the options */
    u1HopLimit = Ipv6Hdr.u1Hlim;
    Ipv6Hdr.u1Hlim = 0;
    u4Head = Ipv6Hdr.u4Head;
    Ipv6Hdr.u4Head = IPSEC_HEAD;

    if (IPSEC_COPY_OVER_BUF (pBuf, (UINT1 *) &Ipv6Hdr, 0,
                             SEC_IPV6_HEADER_SIZE) == BUF_FAILURE)
    {
        return NULL;
    }

    u4OptLen = (*pu4ProcessedLen) - SEC_IPV6_HEADER_SIZE;

    if (u4OptLen > 0)
    {
        if ((pu1OptBuf = IPSECv6_MALLOC (u4OptLen, UINT1)) == NULL)
        {
            return NULL;
        }

        if (IPSEC_COPY_FROM_BUF (pBuf, pu1OptBuf,
                                 SEC_IPV6_HEADER_SIZE, u4OptLen) == BUF_FAILURE)
        {
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6AhTransportDecode : Unable to read options from Packet\n");
            IPSECv6_MEMFREE (pu1OptBuf);
            return NULL;
        }

        /* Mute the mutable option fields in the buffer */
        if (Secv6ProcessOptions (pBuf, &Ipv6Hdr, SEC_AH,
                                 &u1NxtHdr, &u1OptLen) != SEC_SUCCESS)
        {
            IPSECv6_MEMFREE (pu1OptBuf);
            return NULL;
        }
    }

    if (IPSEC_COPY_FROM_BUF (pBuf, &u1NxtHdr, (*pu4ProcessedLen),
                             SEC_IPV6_HEADER_NXTHDR) == BUF_FAILURE)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6AhTransportDecode : Unable to read Next Header from Packet\n");
        if (pu1OptBuf != NULL)
        {
            IPSECv6_MEMFREE (pu1OptBuf);
        }
        return NULL;
    }

    u4AuthDataSize =
        (UINT4) (((u1Len + 2) * SEC_AH_FACTOR) - SEC_AUTH_HEADER_SIZE);

    /* Memory allocation for Auth Data */
    pu1Buf = IPSECv6_MALLOC (u4AuthDataSize, UINT1);

    if (pu1Buf == NULL)
    {
        SECv6_TRC (BUFFER_TRC,
                   "Secv6AhTransportDecode : Malloc fails for Auth Data\n");
        if (pu1OptBuf != NULL)
        {
            IPSECv6_MEMFREE (pu1OptBuf);
        }
        return NULL;
    }

    /* Memory Allocation for Auth Padding Data */
    pu1PaddingBuf = IPSECv6_MALLOC (u4AuthDataSize, UINT1);

    if (pu1PaddingBuf == NULL)
    {
        SECv6_TRC (BUFFER_TRC,
                   "Secv6AhTransportDecode : Malloc fails for Padding Data\n");
        IPSECv6_MEMFREE (pu1Buf);
        if (pu1OptBuf != NULL)
        {
            IPSECv6_MEMFREE (pu1OptBuf);
        }
        return NULL;
    }

    IPSEC_MEMSET (pu1PaddingBuf, 0, u4AuthDataSize);

    /* Copy Authentication Data from pBuf to local linear buffer */
    if (pSaEntry->u1SecAssocAhAlgo != SEC_NULLAHALGO)
    {
        if (IPSEC_COPY_FROM_BUF (pBuf, pu1Buf, (*pu4ProcessedLen)
                                 + SEC_AUTH_HEADER_SIZE,
                                 u4AuthDataSize) == BUF_FAILURE)
        {
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6AhTransportDecode : Unable to read Auth Data from Packet\n");
            if (pu1OptBuf != NULL)
            {
                IPSECv6_MEMFREE (pu1OptBuf);
            }
            IPSECv6_MEMFREE (pu1Buf);
            IPSECv6_MEMFREE (pu1PaddingBuf);
            return NULL;
        }

        /* Make Authentication Data in pBuf to NULL */
        if (IPSEC_COPY_OVER_BUF (pBuf, pu1PaddingBuf,
                                 (*pu4ProcessedLen)
                                 + SEC_AUTH_HEADER_SIZE,
                                 u4AuthDataSize) == BUF_FAILURE)
        {
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6AhTransportDecode : Unable to write Padding bytes to Packet\n");
            if (pu1OptBuf != NULL)
            {
                IPSECv6_MEMFREE (pu1OptBuf);
            }
            IPSECv6_MEMFREE (pu1Buf);
            IPSECv6_MEMFREE (pu1PaddingBuf);
            return NULL;
        }
    }
    IPSECv6_MEMFREE (pu1PaddingBuf);

    u4AuthBufSize = IPSEC_BUF_GetValidBytes (pBuf);

    if ((pu1AuthBuf = IPSECv6_MALLOC (u4AuthBufSize, UINT1)) == NULL)
    {
        if (pu1OptBuf != NULL)
        {
            IPSECv6_MEMFREE (pu1OptBuf);
        }
        IPSECv6_MEMFREE (pu1Buf);
        return NULL;
    }

    if (IPSEC_COPY_FROM_BUF (pBuf, pu1AuthBuf, 0, u4AuthBufSize) == BUF_FAILURE)
    {
        if (pu1OptBuf != NULL)
        {
            IPSECv6_MEMFREE (pu1OptBuf);
        }
        IPSECv6_MEMFREE (pu1AuthBuf);
        IPSECv6_MEMFREE (pu1Buf);
        return NULL;
    }

    /* Generate Authentication Data */
    switch (pSaEntry->u1SecAssocAhAlgo)
    {

        case SEC_HMACMD5:
            u1Algorithm = ISS_UTIL_ALGO_HMAC_MD5;
            break;

        case SEC_HMACSHA1:
            u1Algorithm = ISS_UTIL_ALGO_HMAC_SHA1;
            UtilAlgo.UtilHmacAlgo.HmacShaVersion = AR_SHA1_ALGO;
            break;

        case HMAC_SHA_256:
            u1Algorithm = ISS_UTIL_ALGO_HMAC_SHA2;
            UtilAlgo.UtilHmacAlgo.HmacShaVersion = AR_SHA256_ALGO;
            break;

        case HMAC_SHA_384:
            u1Algorithm = ISS_UTIL_ALGO_HMAC_SHA2;
            UtilAlgo.UtilHmacAlgo.HmacShaVersion = AR_SHA384_ALGO;
            break;

        case HMAC_SHA_512:
            u1Algorithm = ISS_UTIL_ALGO_HMAC_SHA2;
            UtilAlgo.UtilHmacAlgo.HmacShaVersion = AR_SHA512_ALGO;
            break;
        default:
            break;
    }

    switch (pSaEntry->u1SecAssocAhAlgo)
    {
        case SEC_HMACMD5:
        case SEC_HMACSHA1:
        case HMAC_SHA_256:
        case HMAC_SHA_384:
        case HMAC_SHA_512:
            UtilAlgo.UtilHmacAlgo.pu1HmacKey = pSaEntry->pu1SecAssocAhKey1;
            UtilAlgo.UtilHmacAlgo.u4HmacKeyLen =
                pSaEntry->u1SecAssocAhKeyLength;
            UtilAlgo.UtilHmacAlgo.pu1HmacPktDataPtr = pu1AuthBuf;
            UtilAlgo.UtilHmacAlgo.i4HmacPktLength = (INT4) u4AuthBufSize;
            UtilAlgo.UtilHmacAlgo.pu1HmacOutDigest = au1AuthDigest;
            UtilAlgo.UtilHmacAlgo.u4HmacOutDigestLen = SEC_AUTH_DIGEST_SIZE;
            UtilHash (u1Algorithm, &UtilAlgo);
            break;

        case SEC_XCBCMAC:
            UtilAlgo.UtilAesAlgo.pu1AesInUserKey = pSaEntry->pu1SecAssocAhKey1;
            UtilAlgo.UtilAesAlgo.u4AesInKeyLen =
                pSaEntry->u1SecAssocAhKeyLength;
            UtilAlgo.UtilAesAlgo.pu1AesInBuf = pu1AuthBuf;
            UtilAlgo.UtilAesAlgo.u4AesInBufSize = u4AuthBufSize;
            UtilAlgo.UtilAesAlgo.pu1AesMac = au1AuthDigest;
            UtilMac (ISS_UTIL_ALGO_AES_CBC_MAC96, &UtilAlgo);
            break;

        case SEC_MD5:
            UtilAlgo.UtilMd5Algo.pu1Md5InBuf = pu1AuthBuf;
            UtilAlgo.UtilMd5Algo.u4Md5InBufLen = u4AuthBufSize;
            UtilAlgo.UtilMd5Algo.pu1Md5OutDigest = au1AuthDigest;
            UtilMac (ISS_UTIL_ALGO_MD5_ALGO, &UtilAlgo);
            break;

        case SEC_KEYEDMD5:
            KeyedMd5 (pu1AuthBuf, (INT4) u4AuthBufSize,
                      pSaEntry->pu1SecAssocAhKey1,
                      pSaEntry->u1SecAssocAhKeyLength, au1AuthDigest);
            break;

        case SEC_NULLAHALGO:
            break;

        default:
            SECv6_TRC (SECv6_DATA_PATH, " Unknown Auth Algo \n");
            if (pu1OptBuf != NULL)
            {
                IPSECv6_MEMFREE (pu1OptBuf);
            }
            IPSECv6_MEMFREE (pu1Buf);
            IPSECv6_MEMFREE (pu1AuthBuf);
            return NULL;
    }

    IPSECv6_MEMFREE (pu1AuthBuf);

    /* Verify Authentication data */
    if (pSaEntry->u1SecAssocAhAlgo != SEC_NULLAHALGO)
    {
        if (IPSEC_MEMCMP (au1AuthDigest, pu1Buf, u4AuthDataSize) != 0)
        {

            SECv6_TRC (SECv6_MUST, "AhTunnelDecode:\t icv check failed\n");
            if (pu1OptBuf != NULL)
            {
                IPSECv6_MEMFREE (pu1OptBuf);
            }
            IPSECv6_MEMFREE (pu1Buf);
            return NULL;
        }
    }

    IPSECv6_MEMFREE (pu1Buf);

    /* Restore muted fields */
    if (u4OptLen > 0)
    {
        /* restore option fields in the buffer */
        if (IPSEC_COPY_OVER_BUF (pBuf, pu1OptBuf,
                                 SEC_IPV6_HEADER_SIZE, u4OptLen) == BUF_FAILURE)
        {
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6AhTransportDecode : Unable to restore options to Packet\n");
            IPSECv6_MEMFREE (pu1OptBuf);
            return NULL;
        }
        IPSECv6_MEMFREE (pu1OptBuf);

        /* Copy the higher layer header in the last extention header's *
         * next header field */

        if (Secv6GetOptionsParams (pBuf, &Ipv6Hdr, u1NxtHdr, &u1HlHdr,
                                   &u1OptLen) != SEC_SUCCESS)
        {
            return (SEC_FAILURE);
        }
    }
    else
    {
        if (IPSEC_COPY_OVER_BUF (pBuf, &u1NxtHdr,
                                 SEC_IPV6_HEADER_NXTHDR_OFFSET,
                                 SEC_IPV6_HEADER_NXTHDR) == BUF_FAILURE)
        {
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6AhTransportDecode : Unable to write Next Hdr to Packet\n");
            return NULL;
        }
    }

    Ipv6Hdr.u1Hlim = u1HopLimit;
    Ipv6Hdr.u4Head = u4Head;
    Ipv6Hdr.u1Nh = u1NxtHdr;

    if (IPSEC_COPY_OVER_BUF (pBuf, (UINT1 *) &Ipv6Hdr, 0,
                             SEC_IPV6_HEADER_SIZE) == BUF_FAILURE)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6AhTransportDecode : Unable to write Ipv6 Hdr to Packet\n");
        return NULL;
    }

    /* Remove AH header from pBuf */
    if (Secv6RemoveExtraHeaderFromIP (pBuf, (*pu4ProcessedLen),
                                      u4AuthDataSize + SEC_AUTH_HEADER_SIZE) ==
        SEC_FAILURE)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6AhTransportDecode : Unable to Remove AH Header from Packet\n");
        return NULL;
    }

    u2Len = (UINT2) (OSIX_HTONS
                     ((IPSEC_BUF_GetValidBytes (pBuf) - SEC_IPV6_HEADER_SIZE)));
    if (IPSEC_COPY_OVER_BUF
        (pBuf, (UINT1 *) &u2Len, SEC_IPV6_HEADER_LEN_OFFSET,
         SEC_IPV6_HEADER_LEN) == BUF_FAILURE)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6AhTransportDecode : Unable to u2len to Packet\n");
        return NULL;
    }

    return pBuf;
}
