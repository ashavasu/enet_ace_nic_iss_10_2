/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secv6main.c,v 1.3 2013/12/18 12:50:10 siva Exp $
 *
 * Description: This has functions for SecMain SubModule 
 *
 ***********************************************************************/

#include "secv6com.h"

/*
 * ---------------------------------------------------------------
 *  Function Name : Secv6InMain
 *  Description   : This function gets the incomming packet and 
 *                  calls either one of the Sec Decoder(AH/ESP) 
 *                  algorithm based on SA Entry
 *  Input(s)      : pBuf -  buffer contains Secured IP packet
 *                  pSaEntry - Pointer to a SA Entry 
 *  Output(s)     : pBuf - buffer contains IP packet
 *  Global(s)     : None  
 *  Return Values : NULL or SecuredBuffer
 * ---------------------------------------------------------------
 */

tBufChainHeader    *
Secv6InMain (tBufChainHeader * pBuf, tSecv6Assoc * pSaEntry,
             UINT4 *pu4ProcessedLen)
{
    if (pBuf == NULL || pSaEntry == NULL)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6InMain : Input parameters are NULL \n");
        return NULL;
    }

    /* Call Decoder based on protocol specified in SA Entry */
    switch (pSaEntry->u1SecAssocProtocol)
    {
        case SEC_AH:
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6InMain : Secv6AhDecode Entered\n");
            pBuf = Secv6AhDecode (pBuf, pSaEntry, pu4ProcessedLen);
            if (pBuf == NULL)
            {
                SECv6_TRC (SECv6_DATA_PATH,
                           "Secv6InMain : Secv6AhDecode returns null \n");
                return NULL;
            }
            break;
        case SEC_ESP:
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6InMain : Secv6EspDecode Entered\n");
            pBuf = Secv6EspDecode (pBuf, pSaEntry, pu4ProcessedLen);
            if (pBuf == NULL)
            {
                SECv6_TRC (SECv6_DATA_PATH,
                           "Secv6InMain : Secv6EspDecode returns null \n");
                return NULL;
            }
            break;
        default:
            SECv6_TRC1 (SECv6_DATA_PATH,
                        "Secv6InMain : Protocol Number is (%d) wrong \n",
                        pSaEntry->u1SecAssocProtocol);
            return NULL;
    }
    SECv6_TRC (SECv6_DATA_PATH, "Secv6InMain : Returning Decoded Packet \n");
    return pBuf;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : Secv6OutMain
 *  Description   : This function gets the Outgoing packet and 
 *                  calls either one of the Sec Encoder(AH/ESP)
 *                  algorithm based on SA Entry
 *  Input(s)      : pBuf - buffer contains IP packet
 *                  pSaEntry - Pointer to a SA Entry
 *  Output(s)     : pBuf - buffer contains Secured IP packet
 *  Global(s)     : None  
 *  Return Values : NULL or SecuredBuffer
 * ---------------------------------------------------------------
 */

tBufChainHeader    *
Secv6OutMain (tBufChainHeader * pBuf, tSecv6Assoc * pSaEntry, UINT4 u4Interface)
{
    if (pBuf == NULL || pSaEntry == NULL)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6OutMain : Input parameters are NULL \n");
        return NULL;
    }

    /* Call Encoder based on protocol specified in SA Entry */
    switch (pSaEntry->u1SecAssocProtocol)
    {
        case SEC_AH:
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6OutMain : Secv6AhEncode Entered \n");
            pBuf = Secv6AhEncode (pBuf, pSaEntry, u4Interface);
            if (pBuf == NULL)
            {
                SECv6_TRC (SECv6_DATA_PATH,
                           "Secv6OutMain : Secv6AhEncode returns null \n");
                return NULL;
            }
            break;
        case SEC_ESP:
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6OutMain : Secv6EspEncode Entered \n");
            pBuf = Secv6EspEncode (pBuf, pSaEntry, u4Interface);
            if (pBuf == NULL)
            {
                SECv6_TRC (SECv6_DATA_PATH,
                           "Secv6OutMain : Secv6EspEncode returns null \n");
                return NULL;
            }
            break;
        default:
            SECv6_TRC1 (SECv6_DATA_PATH,
                        "Secv6OutMain : Protocol Number is (%d) wrong \n",
                        pSaEntry->u1SecAssocProtocol);
            return NULL;
    }
    SECv6_TRC (SECv6_DATA_PATH, "Secv6OutMain : Returning Securied Packet \n");
    return pBuf;
}
