/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved               
*                                                                    
* $Id: secv6cmd.def,v 1.6 2014/11/04 13:14:05 siva Exp $                                                         
*                                                                    
*********************************************************************/
/***************************************************************************/
/*                          IPSECv6 Configuration                          */
/***************************************************************************/

DEFINE GROUP: CRYPT_GLOB_CONF_GRP

COMMAND : crypto ipsecv6
ACTION  : cli_process_ipsecv6_cmd (CliHandle,CLI_CRYPTO_TRANSFORM_CONF,NULL);
SYNTAX  : crypto ipsecv6
PRVID   : 15
HELP    : Move in to Crypto Transform Configuration mode
CXT_HELP : crypto Configures encryption related details | 
           ipsecv6 IPSecv6 configuration | 
           <CR> Move in to Crypto Transform Configuration mode

END GROUP

DEFINE GROUP: CRYPT_TRAN_CONF_GRP

COMMAND :ipsecv6 admin {enable|disable}
ACTION  : 
{
    if($2 != NULL)
        cli_process_ipsecv6_cmd (CliHandle,CLI_SET_IPSECv6_ADMIN,NULL,CLI_ENABLE);
    if($3 != NULL)
        cli_process_ipsecv6_cmd (CliHandle,CLI_SET_IPSECv6_ADMIN,NULL,CLI_DISABLE);
}
SYNTAX  : ipsecv6 admin <enable/disable>
PRVID   : 15
HELP    : To activate IPSecv6 Administratively
CXT_HELP : ipsecv6 Configures IPSecv6 related information | 
           admin IPSecv6 admin status configuration | 
           enable Enables IPSecv6 | 
           disable Disables IPsecv6 | 
           <CR> To activate IPSecv6 Administratively

COMMAND : ipsecv6 policy <num_str> {apply|bypass} {manual|automatic} <num_str>
ACTION  : 
{
    if(($3 != NULL) && ($5 != NULL))
        cli_process_ipsecv6_cmd (CliHandle,CLI_CREATE_IPSECv6_POLICY,NULL,$2,CLI_APPLY,CLI_MANUAL,$7);

    if(($3 != NULL) && ($6 != NULL))
        cli_process_ipsecv6_cmd (CliHandle,CLI_CREATE_IPSECv6_POLICY,NULL,$2,CLI_APPLY,CLI_AUTOMATIC,$7);

    if(($4 != NULL) && ($5 != NULL))
        cli_process_ipsecv6_cmd (CliHandle,CLI_CREATE_IPSECv6_POLICY,NULL,$2,CLI_BYPASS,CLI_MANUAL,$7);

    if(($4 != NULL) && ($6 != NULL))
        cli_process_ipsecv6_cmd (CliHandle,CLI_CREATE_IPSECv6_POLICY,NULL,$2,CLI_BYPASS,CLI_AUTOMATIC,$7);
}
SYNTAX  : ipsecv6 policy <policyindex> {apply|bypass} {manual|automatic} <sa-index>
PRVID   : 15
HELP    : Creates Policy Entry and maps the same to Security Association Entry
CXT_HELP : ipsecv6 Configures IPSecv6 related information | 
           policy Policy related configuration | 
           <num_str> Policy index | 
           apply Applies the policy | 
           bypass Bypasses the policy | 
           manual Manual configuration of the policy | 
           automatic Automatic configuration of the policy | 
           <num_str> Index to the security association table | 
           <CR> Creates Policy Entry and maps the same to Security Association Entry

COMMAND : ipsecv6 sa <num_str> <random_str> <num_str> {transport|tunnel} [antireplay-enable]
ACTION  :
{
    if(($5 != NULL) && ($7 != NULL))
        cli_process_ipsecv6_cmd (CliHandle,CLI_CREATE_IPSECv6_SA,NULL,$2,$3,$4,CLI_TRANSPORT,CLI_ANTI_REPLAY_ENABLE);

    if(($6 != NULL) && ($7 != NULL))
        cli_process_ipsecv6_cmd (CliHandle,CLI_CREATE_IPSECv6_SA,NULL,$2,$3,$4,CLI_TUNNEL,CLI_ANTI_REPLAY_ENABLE);

    if(($5 != NULL) && ($7 == NULL))
        cli_process_ipsecv6_cmd (CliHandle,CLI_CREATE_IPSECv6_SA,NULL,$2,$3,$4,CLI_TRANSPORT,CLI_ANTI_REPLAY_DISABLE);

    if(($6 != NULL) && ($7 == NULL))
        cli_process_ipsecv6_cmd (CliHandle,CLI_CREATE_IPSECv6_SA,NULL,$2,$3,$4,CLI_TUNNEL,CLI_ANTI_REPLAY_DISABLE);
}
SYNTAX  : ipsecv6 sa <sa-index> <peeraddress> <spi> {transport|tunnel} [antireplay-enable]
PRVID   : 15
HELP    : Creates Security Association Entry with the configured parameters
CXT_HELP : ipsecv6 Configures IPSecv6 related information | 
           sa Security association related configuration | 
           <num_str> Index to the security association table | 
           AAAA::BBBB Peer address | 
           <num_str> Security policy index | 
           transport Security mode is set as transport | 
           tunnel Security mode is set as tunnel | 
           antireplay-enable Rekeying of tunnel | 
           <CR> Creates Security Association Entry with the configured parameters

COMMAND : ipsecv6 sa proto <num_str> <random_str> [auth <random_str> [<num_str>]] [encr <random_str> [<num_str>] [<num_str>] [<num_str>]]
ACTION  :
{
    if(($5 == NULL) && ($8 == NULL))
    {
        CliPrintf (CliHandle,"\r%% Either AUTH or ENCR Algo must be specified\r\n");
    }

    else if(($5 != NULL) && ((CLI_STRCASECMP ($6, "null") == 0 ) || (CLI_STRCASECMP ($6, "md5") == 0)) && ($7 != NULL))
    {
        CliPrintf (CliHandle,"\r%% Key not required for NULL-AH and MD5 Algo\r\n");
    }
  
    else if(($5 != NULL) && ((CLI_STRCASECMP ($6, "keyedmd5") == 0 ) || (CLI_STRCASECMP ($6, "hmacmd5") == 0) || (CLI_STRCASECMP ($6, "hmacsha1") == 0) || (CLI_STRCASECMP ($6, "xcbcmac") == 0) || (CLI_STRCASECMP ($6, "hmacsha256") == 0) || (CLI_STRCASECMP ($6, "hmacsha384") == 0) || (CLI_STRCASECMP ($6, "hmacsha512") == 0)) && ($7 == NULL))
    {
        CliPrintf (CliHandle,"\r%% Key required for %s Algo\r\n",$6);
    }

    else if(($8 != NULL) && (CLI_STRCASECMP ($9, "null") == 0 ) && ($10 != NULL))
    {
        CliPrintf (CliHandle,"\r%% Key not required for NULL-ESP Algo\r\n");
    }

    else if(($8 != NULL) && ((CLI_STRCASECMP ($9, "des-cbc") == 0 ) || (CLI_STRCASECMP ($9, "aes") == 0 )) && ($11 != NULL))
    {
        CliPrintf (CliHandle,"\r%% One Key is sufficient for DES-CBC and AES\r\n");
    }

    else if(($8 != NULL) && ((CLI_STRCASECMP ($9, "des-cbc") == 0 ) || (CLI_STRCASECMP ($9, "aes") == 0 )) && ($10 == NULL))
    {
        CliPrintf (CliHandle,"\r%% Key required for DES-CBC and AES\r\n");
    }

    else if(($8 != NULL) && (CLI_STRCASECMP ($9, "3des-cbc") == 0 ) && (($10 == NULL) || ($11 == NULL) || ($12 == NULL)))
    {
        CliPrintf (CliHandle,"\r%% 3 Keys are required for 3DES-CBC\r\n");
    }

    else if ((CLI_STRCASECMP ($4, "esp") == 0) && ($8 == NULL))
    {
        CliPrintf (CliHandle,"\r%% Encryption required for ESP\r\n");
    }

    else if ((CLI_STRCASECMP ($4, "ah") == 0) && ($8 != NULL))
    {
        CliPrintf (CliHandle,"\r%% Encryption NOT required for AUTH\r\n");
    }

    else if(($5 != NULL) && ($7 != NULL) && ($8 == NULL) && ($10 == NULL))
    {
        cli_process_ipsecv6_cmd (CliHandle,CLI_SET_IPSECv6_SA_PARAMS,NULL,$3,$4,$5,$6,$7);
    }

    else if(($5 != NULL) && ($7 == NULL) && ($8 == NULL) && ($10 == NULL))
    {
        cli_process_ipsecv6_cmd (CliHandle,CLI_SET_IPSECv6_SA_PARAMS,NULL,$3,$4,$5,$6);
    }

    else if(($5 == NULL) && ($7 == NULL) && ($8 != NULL) && ($10 != NULL) && ($11 == NULL) && ($12 == NULL))
    {
        cli_process_ipsecv6_cmd (CliHandle,CLI_SET_IPSECv6_SA_PARAMS,NULL,$3,$4,$8,$9,$10);
    }

    else if(($5 == NULL) && ($7 == NULL) && ($8 != NULL) && ($10 != NULL) && ($11 != NULL) && ($12 != NULL))
    {
        cli_process_ipsecv6_cmd (CliHandle,CLI_SET_IPSECv6_SA_PARAMS,NULL,$3,$4,$8,$9,$10,$11,$12);
    }

    else if(($5 == NULL) && ($7 == NULL) && ($8 != NULL) && ($10 == NULL))
    {
        cli_process_ipsecv6_cmd (CliHandle,CLI_SET_IPSECv6_SA_PARAMS,NULL,$3,$4,$8,$9);
    }

    else if(($5 != NULL) && ($7 != NULL) && ($8 != NULL) && ($10 != NULL) && ($11 == NULL) && ($12 == NULL))
    {
        cli_process_ipsecv6_cmd (CliHandle,CLI_SET_IPSECv6_SA_PARAMS,NULL,$3,$4,$5,$6,$7,$8,$9,$10);
    }

    else if(($5 != NULL) && ($7 != NULL) && ($8 != NULL) && ($10 != NULL) && ($11 != NULL) && ($12 != NULL))
    {
        cli_process_ipsecv6_cmd (CliHandle,CLI_SET_IPSECv6_SA_PARAMS,NULL,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12);
    }

    else if(($5 != NULL) && ($7 != NULL) && ($8 != NULL) && ($10 == NULL))
    {
        cli_process_ipsecv6_cmd (CliHandle,CLI_SET_IPSECv6_SA_PARAMS,NULL,$3,$4,$5,$6,$7,$8,$9);
    }

    else if(($5 != NULL) && ($7 == NULL) && ($8 != NULL) && ($10 != NULL) && ($11 == NULL) && ($12 == NULL))
    {
        cli_process_ipsecv6_cmd (CliHandle,CLI_SET_IPSECv6_SA_PARAMS,NULL,$3,$4,$5,$6,$8,$9,$10);
    }

    else if(($5 != NULL) && ($7 == NULL) && ($8 != NULL) && ($10 != NULL) && ($11 != NULL) && ($12 != NULL))
    {
        cli_process_ipsecv6_cmd (CliHandle,CLI_SET_IPSECv6_SA_PARAMS,NULL,$3,$4,$5,$6,$8,$9,$10,$11,$12);
    }

    else if(($5 != NULL) && ($7 == NULL) && ($8 != NULL) && ($10 == NULL))
    {
        cli_process_ipsecv6_cmd (CliHandle,CLI_SET_IPSECv6_SA_PARAMS,NULL,$3,$4,$5,$6,$8,$9);
    }
}
SYNTAX  : ipsecv6 sa proto <sa-index> {ah/esp} [auth {null|md5|keyedmd5|hmacmd5|hmacsha1|xcbcmac|hmacsha256|hmacsha384|hmacsha512} [<key> ]] [encr {null|des-cbc|3des-cbc|aes} [<espkey>] [<espkey2> ][<espkey3]]
PRVID   : 15
HELP    : The Security Association Entry is updated with Algorithm and Keys
CXT_HELP : ipsecv6 Configures IPSecv6 related information | 
           sa Security association related configuration | 
           proto Protocol related configuration | 
           <num_str> Index to the security association table | 
           (ah/esp) Authentication header / Encapsulated security payload | 
           auth Authentication algorithm configuration | 
           (null/md5/keyedmd5/hmacmd5/hmacsha1|xcbcmac|hmacsha256|hmacsha384|hmacsha512) Authentication algorithm is set as NULL / MD5 / keyed MD5 / hashing algorithm for MD5 / hashing algorithm for secure hash algorithm / XCBCMAC / HMACSHA256 / HMACSHA384 / HMACSHA512 | 
           <num_str> Key string used for authentication | 
           encr Encryption configuration | 
           (null/des-cbc/3des-cbc/aes) ESP algorithm is set as NULL / Data Encryption Standard -Cipher Block Chaining / Triple DES-CBC / Advanced Encryption Standard | 
           <num_str> Key string used for encryption | 
           <num_str> Key string used for encryption | 
           <num_str> Key string used for encryption | 
           <CR> The Security Association Entry is updated with Algorithm and Keys

COMMAND : v6access-list <num_str> {any | <num_str> <random_str> } {any | <num_str> <random_str>}
ACTION  : 
{
    UINT1 au1AnySrc[20],au1AnySrcMask[20];
    UINT1 au1AnyDest[20],au1AnyDestMask[20];
    CLI_STRCPY(&au1AnySrc, "0000::0000");
    CLI_STRCPY(&au1AnySrcMask, "128");
    CLI_STRCPY(&au1AnyDest, "0000::0000");
    CLI_STRCPY(&au1AnyDestMask, "128");
 
    if(($3 != NULL) && ($6 != NULL))
    {
        cli_process_ipsecv6_cmd (CliHandle,CLI_CREATE_IPSECv6_ACCESS_LIST,NULL,$1,$4,$3,$7,$6);
    }

    if(($3 == NULL) && ($6 == NULL))
    {
        cli_process_ipsecv6_cmd (CliHandle,CLI_CREATE_IPSECv6_ACCESS_LIST,NULL,$1,au1AnySrc,au1AnySrcMask,au1AnyDest,au1AnyDestMask);
    }

    if(($3 == NULL) && ($6 != NULL))
    {
        cli_process_ipsecv6_cmd (CliHandle,CLI_CREATE_IPSECv6_ACCESS_LIST,NULL,$1,au1AnySrc,au1AnySrcMask,$7,$6);
    }

    if(($3 != NULL) && ($6 == NULL))
    {
        cli_process_ipsecv6_cmd (CliHandle,CLI_CREATE_IPSECv6_ACCESS_LIST,NULL,$1,$4,$3,au1AnyDest,au1AnyDestMask);
    }
}
SYNTAX  : v6access-list <accesslist-index> {<any> |<src-netmask> <src-addr} {<any> | <dest-netmask> <dest-addr>}
PRVID   : 15
HELP    : Creates Access List with the configured parameters
CXT_HELP : v6access-list Configures access list information | 
           <num_str> Access list index | 
           any Any source | 
           <num_str> Source net mask | 
           AAAA::BBBB Source address | 
           any Any destination | 
           <num_str> Destination net mask | 
           AAAA::BBBB Destination address | 
           <CR> Creates Access List with the configured parameters

COMMAND : ipsecv6 selector {vlan <short (1-4094)>|tunnel <short>|any} <random_str> {<num_str>|any} <random_str> <num_str> <num_str> <random_str> [<random_str>]
ACTION  :
{
    UINT4 u4IfIndex = 0;
    if ($2 != NULL)
    {
        if (CfaCliGetIfIndex ($2, $3, &u4IfIndex) == CLI_FAILURE)
        {
            CliPrintf(CliHandle,"\r%% Invalid Interface Index \r\n");
            return CLI_FAILURE;
        }
    }
    else if ($4 != NULL)
    {
        if (CfaCliGetIfIndex ($4, $5, &u4IfIndex) == CLI_FAILURE)
        {
            CliPrintf(CliHandle,"\r%% Invalid Interface Index \r\n");
            return CLI_FAILURE;
        }
    }
    else if ($6 != NULL)
    {
        u4IfIndex = 0;
    }

    if((u4IfIndex != 0) && ($8 != NULL) && ($14 != NULL))
    {
        cli_process_ipsecv6_cmd (CliHandle,CLI_CREATE_IPSECv6_SELECTOR,NULL,u4IfIndex,$7,$8,$10,$11,$12,$13,$14);
    }

    if((u4IfIndex != 0) && ($8 != NULL) && ($14 == NULL))
    {
        cli_process_ipsecv6_cmd (CliHandle,CLI_CREATE_IPSECv6_SELECTOR,NULL,u4IfIndex,$7,$8,$10,$11,$12,$13,CLI_IPSEC_MANUAL);
    }

    if((u4IfIndex == 0) && ($8 != NULL) && ($14 != NULL))
    {
        cli_process_ipsecv6_cmd (CliHandle,CLI_CREATE_IPSECv6_SELECTOR,NULL,u4IfIndex,$7,$8,$10,$11,$12,$13,$14);
    }

    if((u4IfIndex == 0) && ($8 != NULL) && ($14 == NULL))
    {
        cli_process_ipsecv6_cmd (CliHandle,CLI_CREATE_IPSECv6_SELECTOR,NULL,u4IfIndex,$7,$8,$10,$11,$12,$13,CLI_IPSEC_MANUAL);
    }

    if((u4IfIndex != 0) && ($8 == NULL) && ($14 != NULL))
    {
        cli_process_ipsecv6_cmd (CliHandle,CLI_CREATE_IPSECv6_SELECTOR,NULL,u4IfIndex,$7,$9,$10,$11,$12,$13,$14);
    }

    if((u4IfIndex != 0) && ($8 == NULL) && ($14 == NULL))
    {
        cli_process_ipsecv6_cmd (CliHandle,CLI_CREATE_IPSECv6_SELECTOR,NULL,u4IfIndex,$7,$9,$10,$11,$12,$13,CLI_IPSEC_MANUAL);
    }

    if((u4IfIndex == 0) && ($8 == NULL) && ($14 != NULL))
    {
        cli_process_ipsecv6_cmd (CliHandle,CLI_CREATE_IPSECv6_SELECTOR,NULL,u4IfIndex,$7,$9,$10,$11,$12,$13,$14);
    }

    if((u4IfIndex == 0) && ($8 == NULL) && ($14 == NULL))
    {
        cli_process_ipsecv6_cmd (CliHandle,CLI_CREATE_IPSECv6_SELECTOR,NULL,u4IfIndex,$7,$9,$10,$11,$12,$13,CLI_IPSEC_MANUAL);
    }
}
SYNTAX  : ipsecv6 selector {vlan <id>|tunnel <id>|any} {tcp|udp|icmpv6|ah|esp|any} {port-no|any} {inbound|outbound|any} <accesslist-index> <policy-index> {filter|allow} [{LocalTunnelIP}]
PRVID   : 15
HELP    : Create Selector Entry and map it to Access List and Policy List Entry
CXT_HELP : ipsecv6 Configures IPSecv6 related information | 
           selector Selector related configuration | 
           vlan VLAN related configuration | 
           (1-4094) VLAN ID | 
           tunnel Tunnel interface related configuration | 
           <short> Tunnel number | 
           any Entry is used for any interface | 
           (tcp/udp/icmpv6/ah/esp/any) Protocol is set as Transmission Control Protocol / User Datagram Protocol / Internet Control Message Protocol v6 Protocol / Authentication Header / Encapsulating Security Payload / any protocol | 
           <num_str> Port number | 
           any Entry is used for packets on any port | 
           (inbound/outbound/any) Packet flow direction is set as inward / outward / any direction | 
           <num_str> Access list index | 
           <num_str> Policy index | 
           (filter/allow) Filters / Allows the packets | 
           AAAA::BBBB Local tunnel IP address | 
           <CR> Create Selector Entry and map it to Access List and Policy List Entry

COMMAND : delete ipsecv6 {policy|selector|sa|access} [index <num_str>]
ACTION  : 
{
    if($6 != NULL)
	{
        if($2 != NULL)
            cli_process_ipsecv6_cmd (CliHandle,CLI_DELETE_IPSECv6_POLICY,NULL,$7);

        else if($3 != NULL)
            cli_process_ipsecv6_cmd (CliHandle,CLI_DELETE_IPSECv6_SELECTOR,NULL,$7);

        else if($4 != NULL)
            cli_process_ipsecv6_cmd (CliHandle,CLI_DELETE_IPSECv6_SA,NULL,$7);

        else if($5 != NULL)
            cli_process_ipsecv6_cmd (CliHandle,CLI_DELETE_IPSECv6_ACCESS,NULL,$7);
    }

    if($6 == NULL)
    {
        if($2 != NULL)
            cli_process_ipsecv6_cmd (CliHandle,CLI_DELETE_IPSECv6_POLICY,NULL,ZERO_INST);

        else if($3 != NULL)
            cli_process_ipsecv6_cmd (CliHandle,CLI_DELETE_IPSECv6_SELECTOR,NULL,ZERO_INST);

        else if($4 != NULL)
            cli_process_ipsecv6_cmd (CliHandle,CLI_DELETE_IPSECv6_SA,NULL,ZERO_INST);

        else if($5 != NULL)
            cli_process_ipsecv6_cmd (CliHandle,CLI_DELETE_IPSECv6_ACCESS,NULL,ZERO_INST);
    }
}
SYNTAX  : delete ipsecv6 {policy|selector|sa|access} [index <num_str>]
PRVID   : 15
HELP    : Delete entries of the configured list
CXT_HELP : delete Deletes the entry | 
           ipsecv6 IPSecv6 configuration | 
           policy Policy related configuration | 
           selector Selector related configuration | 
           sa Security association related configuration | 
           access Access list related configuration | 
           index Index of the table | 
           <num_str> Index value | 
           <CR> Delete entries of the configured list
  
COMMAND : exit
ACTION  : CliChangePath ("..");
SYNTAX  : exit
PRVID   : 15
HELP    : Exit from Crypto Transform Configuration mode
CXT_HELP : exit Exit to the global configuration (config)# mode | 
           <CR> Exit from Crypto Transform Configuration mode

END GROUP

DEFINE GROUP: CRYPT_USER_EXEC_GRP

COMMAND : show ipsecv6 stat {if|ah-esp|intruder}
ACTION  :
{
    if($3 != NULL)
        cli_process_ipsecv6_cmd (CliHandle,CLI_SHOW_IPSECv6_IF_STAT,NULL);

    if($4 != NULL)
        cli_process_ipsecv6_cmd (CliHandle,CLI_SHOW_IPSECv6_AH_ESP_STAT,NULL);

    if($5 != NULL)
        cli_process_ipsecv6_cmd (CliHandle,CLI_SHOW_IPSECv6_INTRUDER_STAT,NULL);
}
SYNTAX  :  show ipsecv6 stat <if/ah-esp/intruder>
PRVID   : 1
HELP    : Display the statistics of Interface Specific / Ah Specific / Esp Specific / Intruder Specific 
CXT_HELP : show Displays the configuration / statistics / general information | 
           ipsecv6 IPSecv6 configuration | 
           stat Statistics related information | 
           if Interface identifier | 
           ah-esp Authentication or encryption statistics | 
           intruder Intruder statistics | 
           <CR> Display the statistics of Interface Specific / Ah Specific / Esp Specific / Intruder Specific

COMMAND : show ipsecv6 {policy|selector|sa|access} [index <num_str>]
ACTION  : 
{
    if($6 == NULL)
    {
        if($2 != NULL)
            cli_process_ipsecv6_cmd (CliHandle,CLI_SHOW_IPSECv6_POLICY,NULL,ZERO_INST);

        if($3 != NULL)
            cli_process_ipsecv6_cmd (CliHandle,CLI_SHOW_IPSECv6_SELECTOR,NULL,ZERO_INST);

        if($4 != NULL)
            cli_process_ipsecv6_cmd (CliHandle,CLI_SHOW_IPSECv6_SA,NULL,ZERO_INST);

        if($5 != NULL)
            cli_process_ipsecv6_cmd (CliHandle,CLI_SHOW_IPSECv6_ACCESS,NULL,ZERO_INST);
    }

    if($6 != NULL)
    {
        if($2 != NULL)
            cli_process_ipsecv6_cmd (CliHandle,CLI_SHOW_IPSECv6_POLICY,NULL,$7);

        if($3 != NULL)
            cli_process_ipsecv6_cmd (CliHandle,CLI_SHOW_IPSECv6_SELECTOR,NULL,$7);

        if($4 != NULL)
            cli_process_ipsecv6_cmd (CliHandle,CLI_SHOW_IPSECv6_SA,NULL,$7);

        if($5 != NULL)
            cli_process_ipsecv6_cmd (CliHandle,CLI_SHOW_IPSECv6_ACCESS,NULL,$7);
    }
}
SYNTAX  : show ipsecv6 {policy|selector|sa|access} [index <num_str>]
PRVID   : 1
HELP    : Displays entries of the configured list
CXT_HELP : show Displays the configuration / statistics / general information | 
           ipsecv6 IPSecv6 configuration | 
           policy Policy related configuration | 
           selector Selector related configuration | 
           sa Security association related configuration | 
           access Access list related configuration | 
           index Index of the table | 
           <num_str> Index value | 
           <CR> Displays entries of the configured list

COMMAND : show ipsecv6 config
ACTION  : cli_process_ipsecv6_cmd (CliHandle,CLI_SHOW_IPSECv6_CONFIG,NULL);
SYNTAX  : show ipsecv6 config
PRVID   : 1
HELP    : Displays the global values of IPSecv6
CXT_HELP : show Displays the configuration / statistics / general information | 
           ipsecv6 IPSecv6 configuration | 
           config Global configuration | 
           <CR> Displays the global values of IPSecv6

END GROUP



DEFINE GROUP: CRYPT_PRIV_EXEC_GRP

COMMAND : ipsecv6 trace <num_str>
ACTION  : cli_process_ipsecv6_cmd (CliHandle,CLI_SET_IPSECv6_TRACE_LEVEL,NULL,$2);
SYNTAX  : ipsecv6 trace <trace option mask>
PRVID   : 15
HELP    : Set the Trace levels in IPSecv6
CXT_HELP : ipsecv6 Configures IPSecv6 related information | 
           trace Trace related configuration | 
           <num_str> Trace option mask value | 
           <CR> Set the Trace levels in IPSecv6

END GROUP

