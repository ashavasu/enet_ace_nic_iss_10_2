
/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: secv6com.h,v 1.19 2013/10/29 11:42:37 siva Exp $
*
* Description:This file contains the prototypes, type definitions 
*             and #define constants required for IPSecv6.
*
*******************************************************************/
#ifndef _IPSECV6_H_
#define _IPSECV6_H_

#include "lr.h"
#include "ip.h"
#include "vpn.h"

/* Import From IPv6 */
#include "ipv6.h"
#include "ip6util.h"

#include "secv6.h"
#include "secv6trace.h"
#include "utilalgo.h"
#include "arMD5_api.h"
#include "arHmac_api.h"
#include "desarinc.h"
#include "aesarinc.h"
#include "sec.h"
#include "fsutil.h"
#include "fsike.h"
#include "fips.h"
#define  SEC_MAX_PREFIX_LEN                   128
#define  SEC_MIN_PREFIX_LEN                   1
#define  SEC_KEY_CHANGE                       0x5C
#define  SEC_CALLOC                           calloc
#define  IPSECv6_MALLOC(size,type)            Secv6ApiAllocateMemory()
#define  IPSECv6_MEMFREE                      Secv6ApiReleaseMemory
#define  SEC_INIT_VAL                         1
#define  SEC_BACK_COUNT                      -1
#define  SEC_TENS                            10
#define  SEC_ESP_SIZE                        15 
#define  SEC_AH_SIZE                         32 

/* ND Message Constants */

#define  SEC_ND6_ROUTER_SOLICITATION     133     /* Router Solicitation */
#define  SEC_ND6_ROUTER_ADVERTISEMENT    134     /* Router Advertisement */
#define  SEC_ND6_NEIGHBOR_SOLICITATION   135     /* Neighbor Solicitation */
#define  SEC_ND6_NEIGHBOR_ADVERTISEMENT  136     /* Neighbor Advertisement */
#define  SEC_ND6_REDIRECT                137     /* Redirect */


/* Used for flag value in SA life time in seconds */
#define  SEC_DELETE_SA                    1  /*Indicates whether SA Can Be Deleted */
#define  SEC_BYTES_SECURED_EXCEEDED      -1  /* Lifetime in bytes return value */
#define  SEC_BYTES_SECURED_NOT_EXCEEDED   1 /* Lifetime in bytes return value */
#define  SEC_THRESHOLD_REACHED            2   /* Lifetime in bytes return value */

#define  SEC_FOUND                        1
#define  SEC_NOT_FOUND                    0
#define  SECv6_SA_LIFETIME_TIMER_ID            1    /* Indicates SA LifeTime Timer */

/* Constants related to Ipv6 headersize and position of the fields */
#define SEC_IPV6_HEADER_SIZE            40
#define SEC_IPV6_HEADER_HEAD_OFFSET     0
#define SEC_IPV6_HEADER_LEN_OFFSET      4
#define SEC_IPV6_HEADER_LEN             2
#define SEC_IPV6_HEADER_NXTHDR_OFFSET   6
#define SEC_IPV6_HEADER_NXTHDR          1
#define SEC_IPV6_HEADER_HEAD            4
#define SEC_ADDR_LEN                    16
#define SEC_V6                          6 
#define IPSEC_HEAD                      IPSEC_HTONL(0x60000000)
#define IPSEC_ADDR_UNICAST              1
#define SEC_ICMPv6                      58 

#define SEC_NH_H_BY_H                   0
#define SEC_NH_DEST_HDR                 60
#define SEC_NH_ROUTING_HDR              43
#define SEC_NH_FRAGMENT_HDR             44
#define SEC_IP6_ENCAP                   41

#define SEC_TIMER_ACTIVE                1
#define SEC_TIMER_INACTIVE              0

#define   SECv6_SA_HARD_TIMER_ID                1    /* Indicates SA LifeTime Timer */
#define   SECv6_SA_SOFT_TIMER_ID                2    /* Indicates SA Hard Timer */
#define   SECv6_SA_TRIGGER_TIMER_ID             3    /* Timer on whose expiry IKE is
                                                        trigrred for SA establishment
                                                        in case of no sa */


#define SEC_TRIGGER_IKE_TIME_INTERVAL       5     /* Time out Val for 
              Triggering Ike in case of
              SA establishment failure*/
#define SEC_MAX_SA_TO_POLICY                8     /* Max Rekeyed SA attached to 
                                                     Policy */
#define  SEC_MIN_SOFTLIFETIME_FACTOR        80
#define  SEC_MAX_SOFTLIFETIME_FACTOR        90


#define  SECv6_SNPRINTF             SNPRINTF    


#define SEC_AUTH_HEADER_SIZE            12

/* Threshold for life time of SA */
#define  SEC_THRESHOLD_PERCENT            75 /*Percentage of the Thresh Hold Time */ 
#define SEC_EXH_HDR_CONTINUE            1
#define SEC_HIGHER_LAYER                2

#define SEC_BYTES_IN_OCTET            8
#define SEC_NH_FRAGMENT_HDR_SIZE   8

/* Used to add new entries to SecSelectorTable in sorted order.
 *  SecSelIfTbl indices - 4B(IfIndex)+4B(ProtoId)+4B(SelAccessIndex)+
 *                          4B(Port)+4B(PktDirection) = 20Bytes */
#define SIZE_OF_SELECTOR_TBL_INDICES 20

/* MemPool related constants */

#define SECv6_SEL_MEMPOOL        Secv6SelectorMemPoolId
#define SECv6_ACC_MEMPOOL        Secv6AccessMemPoolId
#define SECv6_POL_MEMPOOL        Secv6PolicyMemPoolId
#define SECv6_SAD_MEMPOOL        Secv6SADMemPoolId
#define SECv6_SAD_AH_MEMPOOL     Secv6SADAhMemPoolId
#define SECv6_SAD_ESP_MEMPOOL    Secv6SADEspMemPoolId
#define SECV6_UINT1_POOL_ID      Secv6Uint1MemPoolId
/* Constant for converting the receoved key Length in bits */
#define IPSEC_AES_KEY_CONV_FACTOR         8

#ifdef IKE_WANTED
#define SEC_RE_KEY                              0   /* Definitions for IKE interface */
#define SEC_NEW_KEY                             1
#endif /* IKE_WANTED */

#define     IPSECV6_IKE_REQ               1
#define     IPSECV6_ASSOC_OVERHEAD_REQ    2
#define     IPSECV6_DELETE_SA_ENTRIES     3

#ifdef _SECV6INIT_C_
tOsixSemId gSec6SemId;
#else
extern tOsixSemId gSec6SemId;
#endif
#define SECv6_GLOBAL_SEM_COUNT  1
#define SECv6_GLOBAL_SEM_NAME   (const UINT1 *)"SECv6S"
#define SECv6_TAKE_SEM() \
        OsixTakeSem (SELF, SECv6_GLOBAL_SEM_NAME, OSIX_WAIT, 0)
#define SECv6_GIVE_SEM() \
        OsixGiveSem (SELF, SECv6_GLOBAL_SEM_NAME)

#ifdef _SECV6INIT_C_
tMemPoolId   Secv6SelectorMemPoolId;
tMemPoolId   Secv6AccessMemPoolId;
tMemPoolId   Secv6PolicyMemPoolId;
tMemPoolId   Secv6SADMemPoolId;
tMemPoolId   Secv6SADAhMemPoolId;
tMemPoolId   Secv6SADEspMemPoolId;
tMemPoolId   Secv6Uint1MemPoolId;
#else
extern tMemPoolId   Secv6SelectorMemPoolId;
extern tMemPoolId   Secv6AccessMemPoolId;
extern tMemPoolId   Secv6PolicyMemPoolId;
extern tMemPoolId   Secv6SADMemPoolId;
extern tMemPoolId   Secv6SADAhMemPoolId;
extern tMemPoolId   Secv6SADEspMemPoolId;
extern tMemPoolId   Secv6Uint1MemPoolId;
#endif

typedef tTmrAppTimer tTimer;
/* ANANTH */
typedef struct {
        tTimer   TimerNode;
        FS_ULONG u4Param1;         /* For Storing the Policy Pointer */
        UINT4    u4SaIndex;
        UINT1    u1TimerId;
        UINT1    u1TimerStatus;
        UINT2    u2Alignment;
} tSecv6Timer;


/* Data structures for nodes of various data bases */


typedef struct SecAssoc
{
   tTMO_SLL_NODE  NextSecv6AssocIf;      /*pointer to the next node in 
                                          the list*/
   tIp6Addr    SecAssocDestAddr;         /* the secassoc end addr */
   tIp6Addr    SecAssocSrcAddr;         /* the secassoc end addr */
   tSecv6Timer  Secv6AssocHardTimer;     /* SA Timer for LifeTime in Seconds*/
   tSecv6Timer  Secv6AssocSoftTimer;     /* SA Timer for LifeTime in Seconds*/
   UINT1       *pu1SecAssocAhKey;        /* configured ah key */
   UINT1       *pu1SecAssocAhKey1;       /* transformed ah key */  
   UINT1       *pu1SecAssocInitVector;   /* the initialisation vector */
   AR_CRYPTO_UINT8       au8SubKey[16];  /* transformed esp key */
   AR_CRYPTO_UINT8       au8SubKey2[16];  /* transformed 3des-cbc key2 */
   AR_CRYPTO_UINT8       au8SubKey3[16];/* transformed 3des-cbc key3 */
   UINT1       *pu1SecAssocEspKey;       /* configured esp key */
   UINT1       *pu1SecAssocEspKey2;      /* configured esp key2 */
   UINT1       *pu1SecAssocEspKey3;      /* configured esp key3 */
   UINT1       au1AesEncrKey[240];        /* The transformed aes encr key */
   UINT1       au1AesDecrKey[240];        /* The transformed aes decr key */
   UINT1       au1Nonce[IPSEC_AES_CTR_NONCE_LEN];  
   UINT4       u4SecAssocIndex;          /*the sec assoc index */ 
   UINT4       u4SecAssocSeqNumber;      /* 0 */  /* Seq number */
   UINT4       u4SecAssocBitMask;        /* 0 */   /* Bitmask */
   UINT4       u4SecAssocSpi;            /* the spi */
   UINT4       u4LifeTimeInBytes;        /* lifetime in bytes */
   UINT4       u4LifeTime;               /* life time */
   UINT4       u4ByteCount;              /* The no of Bytes secured by the SA */
   UINT4       u4PolicyIndex;            /* Refers to Policy Index */
   UINT2       u2Size;                   /*Extra pay load added */ 
   UINT1       u1SecAssocProtocol;       /* secassoc protocol */
   UINT1       u1SecAssocSeqCounterFlag; /* 32 */
   UINT1       u1SecAssocMode;           /*secassoc mode -transport/tunnel */ 
   UINT1       u1SecAssocAhAlgo;         /*Ah algo */
   UINT1       u1SecAssocAhKeyLength;    /* ah keylen */ 
   UINT1       u1SecAssocEspAlgo;        /* esp algo */
   UINT1       u1SecAssocEspKeyLength;   /* esp key len */
   UINT1       u1SecAssocEspKey2Length;  /* esp key2 len */
   UINT1       u1SecAssocEspKey3Length;  /* esp key3 len */
   UINT1       u1DelSaFlag;              /* Flag for Deleting SA */
   UINT1       u1AntiReplayStatus;       /*configured info of Anti replay 
                                           Status */
   UINT1       u1Allignment;            /* Status of the Timer */  
   UINT2       u2Allignment;             /* For Allignment Purpose */
   UINT4       u4AssocStatus;
}tSecv6Assoc;


typedef struct secpolicy
{

   tTMO_SLL_NODE  NextSecPolicyIf;                       /* pointer to the next entry 
                                                            in the list */

   tSecv6Timer    Secv6PolicyTrigIkeTmr;                 /* Timer for trigering
                                                             IKE to establish SA */

   tSecv6Assoc    *paSaEntry[SEC_MAX_BUNDLE_SA];         /* Array of pointer
                                                            to Sa Entires */
   tSecv6Assoc    *pau1NewSaEntry[SEC_MAX_SA_TO_POLICY][SEC_MAX_BUNDLE_SA];
                                                             /* Array of pointer
                                                             to new sa entries */
   UINT1       au1PolicySaBundle[SEC_MAX_SA_BUNDLE_LEN]; /* stores sa bundle */
   UINT1       au1NewPolicySaBundle[SEC_MAX_SA_TO_POLICY][SEC_MAX_SA_BUNDLE_LEN];                                                                    /* Indices of New sa
                                                             Bundle */
   UINT1       au1NewSaCount[SEC_MAX_SA_TO_POLICY ];     /* Array for the no of sa's 
                                                            in the new sabundle */

   UINT4       u4PolicyIndex;                            /* refers to the policy index */
   UINT4       u4PolicyOptionsIndex;                      /* for automatic configuration */
   UINT1       u1PolicyFlag;                             /* Decides whether to apply sec 
                                                            or bypass */
   UINT1       u1PolicyMode;                             /* specifies automatic or manual 
                                                            mode */
   UINT1       u1SaCount;                                /* the no of sa's in the 
                                                            sabundle */  



   UINT1       u1ReqIkeFlag;                             /* Whether already
                                                            reuested ike for
                                                            new key */
   UINT4       u4PolicyStatus;
   UINT1       u1IkeVersion;                             /* Specifies IKE version */  
   UINT1       au1Pad[3];                                /* Pad bytes */

}tSecv6Policy;

 typedef struct secselector
{
    tTMO_SLL_NODE   NextSecSelIf;    /* Pointer to the next entry in the list */
    tSecv6Policy *pPolicyEntry;      /* Pointer to the Policy Entry */
    tIp6Addr     TunnelTermAddr;           /* Interface IP address */
    UINT4        u4IfIndex;          /* The interface index */
    UINT4        u4ProtoId;          /* Identifies the protocol */
    UINT4        u4SelAccessIndex;   /* Pointer to the Access entry */
    UINT4        u4Port;             /* Identifies the port */  
    UINT4        u4PktDirection;     /* Specifies whether inbound or outbound */
    UINT4        u4Count;            /* Specifies the specific entry */
    UINT4        u4SelPolicyIndex;   /* Points to the policy entry */
    UINT4        u4SelStatus;   
    UINT1        u1SelFilterFlag;    /* Specifies whether to filter or allow */ 
    UINT1        au1Allignment[3];   /* For allignment purpose */
 }tSecv6Selector;


typedef struct  secaccess
{
    tTMO_SLL_NODE   NextSecGroupIf;  /* Pointer to the next entry in the list */
    UINT4       u4GroupIndex;        /* Pointer to access entry */
    tIp6Addr    SrcAddress;          /* Src Address for the inaccess entry*/
    UINT4       u4SrcAddrPrefixLen;  /* Prefix for src address  */
    tIp6Addr    DestAddress;         /* Dest addr for outaccess entry */
    UINT4       u4DestAddrPrefixLen; /* Prefix for dest address */
    UINT4       u4AccStatus;
    UINT4       u4Protocol;          /* Identifies the IP protocol tcp, udp, icmp, any, etc. */   
    UINT2       u2LocalStartPort;    /* Identifies the Local Start Port */
    UINT2       u2LocalEndPort;      /* Identifies the Local End Port */
    UINT2       u2RemoteStartPort;   /* Identifies the Remote Start Port */
    UINT2       u2RemoteEndPort;     /* Identifies the Remote End Port */
}tSecv6Access;

      


typedef struct  SecStat
{
  UINT4   u4Index;         /* refers to the interface index */
  UINT4   u4IfInPkts;      /*count for no of incoming packets */
  UINT4   u4IfOutPkts;     /*count for no of outgoining packets */ 
  UINT4   u4IfPktsApply;   /*count for no of sec-applied packets */   
  UINT4   u4IfPktsDiscard; /*count for no of packets discarded */
  UINT4   u4IfPktsBypass;  /*count for no of packets bypassed */
}tSecv6Stat;

typedef struct SecAhEspStat
{
  UINT4  u4Index;        /*refers to the interface index */
  UINT4  u4InAhPkts;     /*count for no of incoming ah pkts*/ 
  UINT4  u4OutAhPkts;    /*count for no of outgoining ah pkts */ 
  UINT4  u4AhPktsAllow;   /*count for no of ah pkts allow */  
  UINT4  u4AhPktsDiscard;  /*count for no ah pkts discarded */
  UINT4  u4InEspPkts;      /* count for no of incoming esp pkts */
  UINT4  u4OutEspPkts;     /*count for no of outgoining esp pkts */
  UINT4  u4EspPktsAllow;    /*count for no of esp pkts allow */
  UINT4  u4EspPktsDiscard;   /*count for no esp pkts discarded */ 
}tSecv6AhEspStat;

typedef struct SecAhEspIntruderStat
{
  UINT4     u4Index;             /* the index to the Entry*/
  UINT4     u4IfIndex;           /* the interface index */
  tIp6Addr  AhEspIntruSrcAddr;   /* intruder src addr */
  tIp6Addr  AhEspIntruDestAddr;  /* intruder dest addr */
  UINT4     u4AhEspIntruProt;    /* intruder protocol */
  UINT4     u4AhEspIntruTime;    /*intruder time */
}tSecv6AhEspIntruStat;        

typedef struct SecAssocOverHead
{
    tIp6Addr SrcAddr;
    tIp6Addr DestAddr;
    UINT2    u2Index;
    UINT2    u2OverHead;
    UINT1    u1Protocol;
    UINT1    u1Reserved[3];
}tSecAssocOverHead;

/* Follwing structure is used to invoke IOCTL request
 * from userspce to kernel space when the IPSEcv6 is 
 * present in the kernel space */
typedef struct Secv6KernIface
{
    union
    {
       tIkeIPSecQMsg      IkeIPSecMsg;
       tSecAssocOverHead  SecAssocOverHead;
    }uMsg;
    UINT1  u1MsgType;
    UINT1  u1Reserved[3];
}tSecv6KernIface;

/*Global Definitions */

#ifdef _SECV6INIT_C_
UINT1               gSecv6Status;
UINT1               gSecv6Version;
tTMO_SLL            Secv6SelList;
tTMO_SLL            Secv6AccessList;
tTMO_SLL            Secv6PolicyList;
tTMO_SLL            Secv6AssocList;
tTMO_SLL            Secv6SelFormattedList;
UINT4               u4Secv6AhEspIntruStatCount;

tSecv6Stat          gatIpsecv6Stat[SEC_MAX_STAT_COUNT];
tSecv6AhEspStat     gatIpsecv6AhEspStat[SEC_MAX_STAT_COUNT];
tSecv6AhEspIntruStat gatIpsecv6AhEspIntruStat[SEC_MAX_STAT_COUNT];
tSecv6Access       *gpv6AccessEntry;
UINT4               gu4Secv6Debug = 0;
tTimerListId        gSecv6TimerListId;
UINT4               gu4Secv6FreeSaIndex = 5000;
UINT1               gu1Secv6BypassCrypto = BYPASS_DISABLED;
#else
extern  tSecv6Stat     gatIpsecv6Stat[SEC_MAX_STAT_COUNT];
extern  tSecv6AhEspStat   gatIpsecv6AhEspStat[SEC_MAX_STAT_COUNT];
extern  tSecv6AhEspIntruStat gatIpsecv6AhEspIntruStat[SEC_MAX_STAT_COUNT];
extern  UINT1 gSecv6Status;
extern  UINT1 gSecv6Version;
extern  tTMO_SLL  Secv6SelList;      /* selector database */
extern  tTMO_SLL  Secv6AccessList;  /* inaccess database */
extern  tTMO_SLL  Secv6PolicyList;     /* policy database */
extern  tTMO_SLL  Secv6AssocList;        /* secassoc database */
extern  tTMO_SLL  Secv6SelFormattedList; /* formatted selector database */

extern UINT4        u4Secv6AhEspIntruStatCount; /*the count for ahespintru 
                                               entries */

extern UINT4               gu4Secv6Debug;  /* Stores the level of trace */
extern tTimerListId      gSecv6TimerListId;
extern UINT4             gu4Secv6FreeSaIndex ;
PUBLIC UINT1             gu1Secv6BypassCrypto;
#endif



/* Prototypes  of the functions used across IPSec */

/* -------------- Sec Init -------------------*/
INT1
Secv6MemPoolInit  PROTO((VOID));

VOID Secv6MemPoolDeInit PROTO((VOID));

 /* ----------------  Sec SecMain ------------- */
tBufChainHeader * Secv6InMain PROTO (( tBufChainHeader *pBuf,
                         tSecv6Assoc *pSaEntry,UINT4 *pu4ProcessedLen));
tBufChainHeader * Secv6OutMain PROTO (( tBufChainHeader *pBuf,
                         tSecv6Assoc *pSaEntry,UINT4 u4Interface));

/* --------------- Sec AH -------------------- */
tBufChainHeader * Secv6AhEncode PROTO (( tBufChainHeader *pBuf,
                         tSecv6Assoc *pSaEntry,UINT4 u4Interface));
tBufChainHeader * Secv6AhDecode PROTO (( tBufChainHeader *pBuf,
                   tSecv6Assoc *pSaEntry,UINT4 *pu4ProcessedLen));

/* ---------------- Sec ESP ----------------  */
tBufChainHeader * Secv6EspEncode PROTO (( tBufChainHeader *pBuf,
                         tSecv6Assoc *pSaEntry,UINT4 u4Interface));
tBufChainHeader * Secv6EspDecode PROTO (( tBufChainHeader *pBuf,
                  tSecv6Assoc *pSaEntry,UINT4 *pu4ProcessedLen));

/* ---------------- Sec Util ---------------- */
INT1 Secv6SeqNumberVerification PROTO((UINT4 u4SeqNumber,
                              tSecv6Assoc * pSaEntry));

INT1
Secv6RemoveExtraHeaderFromIP PROTO ((tBufChainHeader * pBuf,
            UINT4 u4Offset,UINT4 u4Size));
INT1
Secv6AddExtraHeaderToIP PROTO ((tBufChainHeader *pBuf,
UINT1 * pLinear1,UINT4 u4Len1,UINT1 * pLinear2,UINT4 u4Len2));

INT1 Secv6ConstructKey(UINT1 *pu1InKey, UINT1 u1Len , UINT1 *pu1OutKey);

UINT1 Secv6ProcessOptions PROTO((tBufChainHeader *pBuf, tIp6Hdr *pIp6Hdr,
            UINT1 u1Proto, UINT1 *pu1NextHdr, UINT1 *pu1OptLen));
UINT1 Secv6GetOptionsParams PROTO((tBufChainHeader *pBuf, tIp6Hdr *pIp6Hdr,
        UINT1 u1Proto ,UINT1 *pu1NextHdr, UINT1 *pu1OptLen));
UINT1 Secv6GetHLProtocol PROTO((tBufChainHeader *pBuf, tIp6Hdr *pIp6Hdr,
        UINT1 u1Proto ,UINT1 *pu1NextHdr));

INT1
Secv6CheckNoOfOutBoundBytesSecured PROTO((tSecv6Policy *pPolicy, 
                                          tBufChainHeader *pBufDesc));
INT1
Secv6CheckNoOfInBoundBytesSecured PROTO((tSecv6Assoc *pSecAssoc,
                                         tBufChainHeader *pBufDesc));

VOID
Secv6UtilGetMaskFromPrefix PROTO((UINT4 u4PrefixLen, tIp6Addr *pIpv6Mask));
UINT1 *Secv6ApiAllocateMemory (VOID);
VOID  Secv6ApiReleaseMemory (VOID *pNoctets);

/* --------------- Sec Conf ---------------- */
INT1  Secv6AssocOutLookUp PROTO ((tIp6Addr SrcAddr,tIp6Addr DestAddr,
            UINT4 u4Protocol,UINT4 u4Index,
            tSecv6Policy **pSecv6Policy,UINT4 u4Port,tBufChainHeader*));

tSecv6Assoc * Secv6AssocInLookUp PROTO ((UINT4 Spi,tIp6Addr DestAddr,
            UINT1 u1Protocol));
INT4 Secv6AssocVerify PROTO ((tIp6Addr SrcAddr,tIp6Addr DestAddr,
               UINT4 u4Protocol,UINT4 u4Index,UINT4 *pu4SaIndexes,UINT4 u4Port));

UINT4 Secv6GetIfIndexFromAddr PROTO((tIp6Addr *pIp6Addr));
/*--------------------Sec Stat------------------------------------------*/

VOID   Secv6UpdateIfStats PROTO((UINT4 u4Index,UINT1 u1Direction ,
       UINT1 u1Flag));
VOID   Secv6UpdateAhEspStats PROTO((UINT4 u4Index,UINT1 u1Algo,
                            UINT1 u1Direction, UINT1 u1Flag));

VOID   Secv6UpdateAhEspIntruStats PROTO((UINT4 u4Index,tIp6Addr SrcAddr,
                                 tIp6Addr DesAddr,
                                 UINT1 u1Prot , UINT4 u4Time)); 
 
#ifdef IKE_WANTED
/* -------------------- IKE releated Function protos-----*/

VOID Secv6RequeStIkeForNewOrReKey PROTO((UINT4 u4IfIndex, tIp6Addr *pIpAddr, UINT4 u4Protocol,UINT4 u4Port,
                                         UINT4 u4AccessIndex, UINT4 u4PktDir,
                                         UINT1 u1NewOrReKeyFlag,UINT1 u1IkeVersion));

VOID Secv6IntimateIkeInvalidSpi PROTO((UINT4 u4Spi, tIp6Addr DestAddr, tIp6Addr SrcAddr,
                            UINT1 u1Protocol));
VOID
Secv6IntimateIkeDeletionOfSa PROTO ((tSecv6Policy *pSecv6Policy,
                                     UINT4 u4SecAssocIndex));

VOID 
Secv6DeleteSa PROTO ((UINT4 u4SecAssocIndex));


#endif

/*-------------------------- Sec Tmr --------------------------------------*/

VOID Secv6InitTimer PROTO ((VOID));
VOID Secv6DeInitTimer PROTO((VOID));
VOID Secv6StartTimer PROTO ((tSecv6Timer * pTimer,UINT4 TimeOut));
VOID Secv6RekeySa PROTO ((tSecv6Timer *pTimer));
VOID Secv6StopTimer PROTO ((tSecv6Timer * pTimer));
VOID Secv6ProcessTimeOut PROTO((tTimerListId Secv6TimerListId));



 /* --------------------------SecLow-------------------------------------*/

tSecv6Selector *  Secv6SelGetEntry PROTO ((UINT4 Index,UINT4 ProtId,
                           UINT4 u4AccessIndex, INT4 i4Port,INT4 i4PktDirection ));

tSecv6Selector *  Secv6LowSelGetEntry PROTO ((UINT4 Index,UINT4 ProtId,
                           UINT4 u4AccessIndex, INT4 i4Port,INT4 i4PktDirection
));

tSecv6Selector *  Secv6SelGetFormattedEntry PROTO ((UINT4 Index,UINT4 ProtId,
                            UINT4 u4AccessIndex,INT4 i4Port,INT4 i4PktDirection));
tSecv6Selector *  Secv6LowSelGetFormattedEntry PROTO ((UINT4 Index,UINT4 ProtId,
                            UINT4 u4AccessIndex,INT4 i4Port,INT4 i4PktDirection));

tSecv6Access *  Secv6AccessGetEntry PROTO ((UINT4 u4AccessIndex));
tSecv6Policy *  Secv6PolicyGetEntry PROTO ((UINT4 Index));
tSecv6Access *  Secv6LowAccessGetEntry PROTO ((UINT4 u4AccessIndex));
tSecv6Policy *  Secv6LowPolicyGetEntry PROTO ((UINT4 Index));
INT4  Secv6SelectorCompare PROTO ((tSecv6Selector *pEntry ,
tSecv6Selector *pNextEntry));
tSecv6Assoc * Secv6AssocGetEntry PROTO ((UINT4 Index));
tSecv6Assoc * Secv6LowAssocGetEntry PROTO ((UINT4 Index));
UINT4 Secv6GetAccessIndex PROTO ((tIp6Addr SrcAddr, tIp6Addr DestAddr));
tSecv6Assoc *
Secv6GetAssocEntry PROTO ((UINT4 u4SpiIndex,tIp6Addr DestAddr,
UINT1 u1Protocol ));
INT1 Secv6ConstSecList PROTO ((VOID));
INT1 Secv6ConstAccessList PROTO ((VOID));
VOID Secv6UpdateSecAssocSize PROTO ((VOID));
INT1 Secv6DeleteFormattedSelectorEntries PROTO((VOID));
VOID Secv6DeletePolicyPtrInSel PROTO ((UINT4 Index));
UINT1  Secv6CheckPolicySaBundle PROTO ((tSecv6Policy * pEntry,UINT4 *pu4SaIndexes));
VOID Secv6DeleteSAPtrInPolicy PROTO ((tSecv6Assoc *pSecAssoc));
UINT4 Secv6GetFreeSAIdx PROTO((VOID));
VOID 
Secv6AttachNewSaToPolicy PROTO ((tSecv6Policy *pu1Policy,
                                UINT1 au1SaBundle[SEC_MAX_SA_BUNDLE_LEN]));
INT1 Secv6DestroySecAssoc (INT4 );

/* Function Exported By Cli */
VOID IPSecv6CliCmdActionRoutine (UINT1 *, UINT1 **);

INT1 Secv6GetBypassCapability PROTO ((INT4 * pi4RetValSecv4BypassCapability));
INT1 Secv6SetBypassCapability PROTO ((INT4 pi4SetValSecv4BypassCapability));
#endif
