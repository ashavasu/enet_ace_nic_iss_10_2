/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secv6low.c,v 1.32 2014/03/07 12:07:23 siva Exp $
 *
 *******************************************************************/

# include  "include.h"
# include  "secv6con.h"
# include  "secv6ogi.h"
# include  "midconst.h"
# include  "secv6com.h"
# include  "secv6low.h"

INT4                Secv6SelectorCompareGetNext (tSecv6Selector * pEntry,
                                                 INT4 i4IfIndex, INT4 i4ProtId,
                                                 INT4 i4AccIndex,
                                                 INT4 i4Fsipv6SelPort,
                                                 INT4 i4Fsipv6SelPktDirection);

/* LOW LEVEL Routines for Table : Fsipv6SecSelectorTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6SecSelectorTable
 Input       :  The Indices
                Fsipv6SelIfIndex
                Fsipv6SelProtoIndex
                Fsipv6SelAccessIndex
                Fsipv6SelPort
                Fsipv6SelPktDirection
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsipv6SecSelectorTable (INT4 i4Fsipv6SelIfIndex,
                                                INT4 i4Fsipv6SelProtoIndex,
                                                INT4 i4Fsipv6SelAccessIndex,
                                                INT4 i4Fsipv6SelPort,
                                                INT4 i4Fsipv6SelPktDirection)
{
    /* Check whether all the Indicies are with the specified Range */

    if ((i4Fsipv6SelIfIndex < 0)
        || (i4Fsipv6SelProtoIndex < SEC_MIN_INTEGER)
        || (i4Fsipv6SelAccessIndex < SEC_MIN_INTEGER)
        || (i4Fsipv6SelPort < SEC_MIN_INTEGER)
        || ((i4Fsipv6SelPktDirection != SEC_INBOUND) &&
            (i4Fsipv6SelPktDirection != SEC_OUTBOUND) &&
            (i4Fsipv6SelPktDirection != SEC_ANY_DIRECTION)))
    {

        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6SecSelectorTable
 Input       :  The Indices
                Fsipv6SelIfIndex
                Fsipv6SelProtoIndex
                Fsipv6SelAccessIndex
                Fsipv6SelPort
                Fsipv6SelPktDirection
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv6SecSelectorTable (INT4 *pi4Fsipv6SelIfIndex,
                                        INT4 *pi4Fsipv6SelProtoIndex,
                                        INT4 *pFsipv6SelAccessIndex,
                                        INT4 *pi4Fsipv6SelPort,
                                        INT4 *pi4Fsipv6SelPktDirection)
{

    tSecv6Selector     *pSecSelIf = NULL, *pFirstSecSelIf = NULL;

    pFirstSecSelIf = (tSecv6Selector *) TMO_SLL_First (&Secv6SelList);
    if (pFirstSecSelIf == NULL)
        return SNMP_FAILURE;

    /*  Get the First Lexicographical ordered Index */

    TMO_SLL_Scan (&Secv6SelList, pSecSelIf, tSecv6Selector *)
    {
        if (Secv6SelectorCompareGetNext (pSecSelIf,
                                         (INT4) pFirstSecSelIf->u4IfIndex,
                                         (INT4) pFirstSecSelIf->u4ProtoId,
                                         (INT4) pFirstSecSelIf->
                                         u4SelAccessIndex,
                                         (INT4) pFirstSecSelIf->u4Port,
                                         (INT4) pFirstSecSelIf->
                                         u4PktDirection) < 0)
        {
            pFirstSecSelIf = pSecSelIf;
        }
    }

    *pi4Fsipv6SelIfIndex = (INT4) pFirstSecSelIf->u4IfIndex;
    *pi4Fsipv6SelProtoIndex = (INT4) pFirstSecSelIf->u4ProtoId;
    *pi4Fsipv6SelPort = (INT4) pFirstSecSelIf->u4Port;
    *pi4Fsipv6SelPktDirection = (INT4) pFirstSecSelIf->u4PktDirection;
    *pFsipv6SelAccessIndex = (INT4) pFirstSecSelIf->u4SelAccessIndex;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6SecSelectorTable
 Input       :  The Indices
                Fsipv6SelIfIndex
                nextFsipv6SelIfIndex
                Fsipv6SelProtoIndex
                nextFsipv6SelProtoIndex
                Fsipv6SelAccessIndex
                nextFsipv6SelAccessIndex
                Fsipv6SelPort
                nextFsipv6SelPort
                Fsipv6SelPktDirection
                nextFsipv6SelPktDirection
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6SecSelectorTable (INT4 i4Fsipv6SelIfIndex,
                                       INT4 *pi4NextFsipv6SelIfIndex,
                                       INT4 i4Fsipv6SelProtoIndex,
                                       INT4 *pi4NextFsipv6SelProtoIndex,
                                       INT4 i4Fsipv6SelAccessIndex,
                                       INT4 *pi4NextFsipv6SelAccessIndex,
                                       INT4 i4Fsipv6SelPort,
                                       INT4 *pi4NextFsipv6SelPort,
                                       INT4 i4Fsipv6SelPktDirection,
                                       INT4 *pi4NextFsipv6SelPktDirection)
{

    tSecv6Selector     *pSecSelIf = NULL;
    INT4                Flag = SEC_NOT_FOUND;

    pSecSelIf = (tSecv6Selector *) TMO_SLL_First (&Secv6SelList);

    if (pSecSelIf == NULL)
        return (SNMP_FAILURE);

    /*  To Get the Next lexicographically ordered Index for the given Index */

    TMO_SLL_Scan (&Secv6SelList, pSecSelIf, tSecv6Selector *)
    {

        if ((Secv6SelectorCompareGetNext (pSecSelIf, i4Fsipv6SelIfIndex,
                                          i4Fsipv6SelProtoIndex,
                                          i4Fsipv6SelAccessIndex,
                                          i4Fsipv6SelPort,
                                          i4Fsipv6SelPktDirection)) > 0)
        {

            if (Flag == SEC_NOT_FOUND)
            {
                Flag = SEC_FOUND;

                *pi4NextFsipv6SelIfIndex = (INT4) pSecSelIf->u4IfIndex;
                *pi4NextFsipv6SelProtoIndex = (INT4) pSecSelIf->u4ProtoId;
                *pi4NextFsipv6SelPort = (INT4) pSecSelIf->u4Port;
                *pi4NextFsipv6SelPktDirection =
                    (INT4) pSecSelIf->u4PktDirection;
                *pi4NextFsipv6SelAccessIndex =
                    (INT4) pSecSelIf->u4SelAccessIndex;
            }
            else
            {

                if ((Secv6SelectorCompareGetNext (pSecSelIf,
                                                  *pi4NextFsipv6SelIfIndex,
                                                  *pi4NextFsipv6SelProtoIndex,
                                                  *pi4NextFsipv6SelAccessIndex,
                                                  *pi4NextFsipv6SelPort,
                                                  *pi4NextFsipv6SelPktDirection))
                    < 0)
                {
                    *pi4NextFsipv6SelIfIndex = (INT4) pSecSelIf->u4IfIndex;
                    *pi4NextFsipv6SelProtoIndex = (INT4) pSecSelIf->u4ProtoId;
                    *pi4NextFsipv6SelPort = (INT4) pSecSelIf->u4Port;
                    *pi4NextFsipv6SelPktDirection =
                        (INT4) pSecSelIf->u4PktDirection;
                    *pi4NextFsipv6SelAccessIndex =
                        (INT4) pSecSelIf->u4SelAccessIndex;
                }
            }
        }
    }
    if (Flag == SEC_NOT_FOUND)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6SelFilterFlag
 Input       :  The Indices
                Fsipv6SelIfIndex
                Fsipv6SelProtoIndex
                Fsipv6SelAccessIndex
                Fsipv6SelPort
                Fsipv6SelPktDirection
                               

                The Object 
                retValFsipv6SelFilterFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SelFilterFlag (INT4 i4Fsipv6SelIfIndex, INT4 i4Fsipv6SelProtoIndex,
                           INT4 i4Fsipv6SelAccessIndex,
                           INT4 i4Fsipv6SelPort,
                           INT4 i4Fsipv6SelPktDirection,
                           INT4 *pi4RetValFsipv6SelFilterFlag)
{
    tSecv6Selector     *pSecSelIf = NULL;

    /* get the entry for the given indicies from the selector database */
    pSecSelIf = Secv6LowSelGetEntry ((UINT4) i4Fsipv6SelIfIndex,
                                     (UINT4) i4Fsipv6SelProtoIndex,
                                     (UINT4) i4Fsipv6SelAccessIndex,
                                     i4Fsipv6SelPort, i4Fsipv6SelPktDirection);

    if (pSecSelIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFsipv6SelFilterFlag = (INT4) pSecSelIf->u1SelFilterFlag;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsipv6SelPolicyIndex
 Input       :  The Indices
                Fsipv6SelIfIndex
                Fsipv6SelProtoIndex
                Fsipv6SelAccessIndex
                Fsipv6SelPort
                Fsipv6SelPktDirection

                The Object 
                retValFsipv6SelPolicyIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SelPolicyIndex (INT4 i4Fsipv6SelIfIndex, INT4 i4Fsipv6SelProtoIndex,
                            INT4 i4Fsipv6SelAccessIndex,
                            INT4 i4Fsipv6SelPort,
                            INT4 i4Fsipv6SelPktDirection,
                            INT4 *pi4RetValFsipv6SelPolicyIndex)
{

    tSecv6Selector     *pSecSelIf = NULL;

    /* get the entry for the given indicies from the selector database */
    pSecSelIf = Secv6LowSelGetEntry ((UINT4) i4Fsipv6SelIfIndex,
                                     (UINT4) i4Fsipv6SelProtoIndex,
                                     (UINT4) i4Fsipv6SelAccessIndex,
                                     i4Fsipv6SelPort, i4Fsipv6SelPktDirection);

    if (pSecSelIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValFsipv6SelPolicyIndex = (INT4) pSecSelIf->u4SelPolicyIndex;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsipv6SelIfIpAddress
 Input       :  The Indices
                Fsipv6SelIfIndex
                Fsipv6SelProtoIndex
                Fsipv6SelAccessIndex
                Fsipv6SelPort
                Fsipv6SelPktDirection

                The Object 
                retValFsipv6SelIfIpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SelIfIpAddress (INT4 i4Fsipv6SelIfIndex, INT4 i4Fsipv6SelProtoIndex,
                            INT4 i4Fsipv6SelAccessIndex, INT4 i4Fsipv6SelPort,
                            INT4 i4Fsipv6SelPktDirection,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFsipv6SelIfIpAddress)
{
    tSecv6Selector     *pSecSelIf = NULL;

    /* get the entry for the given indicies from the selector database */
    pSecSelIf = Secv6LowSelGetEntry ((UINT4) i4Fsipv6SelIfIndex,
                                     (UINT4) i4Fsipv6SelProtoIndex,
                                     (UINT4) i4Fsipv6SelAccessIndex,
                                     i4Fsipv6SelPort, i4Fsipv6SelPktDirection);

    if (pSecSelIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    else
    {
        Ip6AddrCopy ((tIp6Addr *) (VOID *) pRetValFsipv6SelIfIpAddress->
                     pu1_OctetList, &pSecSelIf->TunnelTermAddr);
        pRetValFsipv6SelIfIpAddress->i4_Length = SEC_ADDR_LEN;
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv6SelStatus
 Input       :  The Indices
                Fsipv6SelIfIndex
                Fsipv6SelProtoIndex
                Fsipv6SelAccessIndex
                Fsipv6SelPort
                Fsipv6SelPktDirection

                The Object 
                retValFsipv6SelStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SelStatus (INT4 i4Fsipv6SelIfIndex, INT4 i4Fsipv6SelProtoIndex,
                       INT4 i4Fsipv6SelAccessIndex,
                       INT4 i4Fsipv6SelPort,
                       INT4 i4Fsipv6SelPktDirection,
                       INT4 *pi4RetValFsipv6SelStatus)
{

    tSecv6Selector     *pSecSelIf = NULL;
    /* get the entry for the given indicies from the selector database */
    pSecSelIf = Secv6LowSelGetEntry ((UINT4) i4Fsipv6SelIfIndex,
                                     (UINT4) i4Fsipv6SelProtoIndex,
                                     (UINT4) i4Fsipv6SelAccessIndex,
                                     i4Fsipv6SelPort, i4Fsipv6SelPktDirection);

    if (pSecSelIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    else
    {
        *pi4RetValFsipv6SelStatus = (INT4) pSecSelIf->u4SelStatus;
    }
    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv6SelFilterFlag
 Input       :  The Indices
                Fsipv6SelIfIndex
                Fsipv6SelProtoIndex
                Fsipv6SelAccessIndex
                Fsipv6SelPort
                Fsipv6SelPktDirection

                The Object 
                setValFsipv6SelFilterFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SelFilterFlag (INT4 i4Fsipv6SelIfIndex, INT4 i4Fsipv6SelProtoIndex,
                           INT4 i4Fsipv6SelAccessIndex,
                           INT4 i4Fsipv6SelPort, INT4 i4Fsipv6SelPktDirection,
                           INT4 i4SetValFsipv6SelFilterFlag)
{

    tSecv6Selector     *pSecSelIf = NULL;

    /* get the entry for the given indicies from the selector database */
    pSecSelIf = Secv6LowSelGetEntry ((UINT4) i4Fsipv6SelIfIndex,
                                     (UINT4) i4Fsipv6SelProtoIndex,
                                     (UINT4) i4Fsipv6SelAccessIndex,
                                     i4Fsipv6SelPort, i4Fsipv6SelPktDirection);

    if (pSecSelIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    pSecSelIf->u1SelFilterFlag = (UINT1) i4SetValFsipv6SelFilterFlag;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsipv6SelPolicyIndex
 Input       :  The Indices
                Fsipv6SelIfIndex
                Fsipv6SelProtoIndex
                Fsipv6SelAccessIndex
                Fsipv6SelPort
                Fsipv6SelPktDirection

                The Object 
                setValFsipv6SelPolicyIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SelPolicyIndex (INT4 i4Fsipv6SelIfIndex, INT4 i4Fsipv6SelProtoIndex,
                            INT4 i4Fsipv6SelAccessIndex,
                            INT4 i4Fsipv6SelPort, INT4 i4Fsipv6SelPktDirection,
                            INT4 i4SetValFsipv6SelPolicyIndex)
{

    tSecv6Selector     *pSecSelIf = NULL;

    /* get the entry for the given indicies from the selector database */
    pSecSelIf = Secv6LowSelGetEntry ((UINT4) i4Fsipv6SelIfIndex,
                                     (UINT4) i4Fsipv6SelProtoIndex,
                                     (UINT4) i4Fsipv6SelAccessIndex,
                                     i4Fsipv6SelPort, i4Fsipv6SelPktDirection);
    if (pSecSelIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    pSecSelIf->u4SelPolicyIndex = (UINT4) i4SetValFsipv6SelPolicyIndex;
    pSecSelIf->pPolicyEntry =
        Secv6LowPolicyGetEntry (pSecSelIf->u4SelPolicyIndex);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsipv6SelIfIpAddress
 Input       :  The Indices
                Fsipv6SelIfIndex
                Fsipv6SelProtoIndex
                Fsipv6SelAccessIndex
                Fsipv6SelPort
                Fsipv6SelPktDirection

                The Object 
                setValFsipv6SelIfIpAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SelIfIpAddress (INT4 i4Fsipv6SelIfIndex, INT4 i4Fsipv6SelProtoIndex,
                            INT4 i4Fsipv6SelAccessIndex, INT4 i4Fsipv6SelPort,
                            INT4 i4Fsipv6SelPktDirection,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValFsipv6SelIfIpAddress)
{
    tSecv6Selector     *pSecSelIf = NULL;

    /* get the entry for the given indicies from the selector database */
    pSecSelIf = Secv6LowSelGetEntry ((UINT4) i4Fsipv6SelIfIndex,
                                     (UINT4) i4Fsipv6SelProtoIndex,
                                     (UINT4) i4Fsipv6SelAccessIndex,
                                     i4Fsipv6SelPort, i4Fsipv6SelPktDirection);
    if (pSecSelIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    Ip6AddrCopy (&pSecSelIf->TunnelTermAddr,
                 (tIp6Addr *) (VOID *) pSetValFsipv6SelIfIpAddress->
                 pu1_OctetList);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv6SelStatus
 Input       :  The Indices
                Fsipv6SelIfIndex
                Fsipv6SelProtoIndex
                Fsipv6SelAccessIndex
                Fsipv6SelPort
                Fsipv6SelPktDirection

                The Object 
                setValFsipv6SelStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SelStatus (INT4 i4Fsipv6SelIfIndex, INT4 i4Fsipv6SelProtoIndex,
                       INT4 i4Fsipv6SelAccessIndex,
                       INT4 i4Fsipv6SelPort, INT4 i4Fsipv6SelPktDirection,
                       INT4 i4SetValFsipv6SelStatus)
{

    tSecv6Selector     *pSecSelIf = NULL;
    tSecv6Selector     *pScanNode = NULL;
    tSecv6Selector     *pPrevNode = NULL;
    tSecv6Selector     *pSecFormSelIf = NULL;
    PRIVATE UINT4       u4Count = 0;

    if (i4SetValFsipv6SelStatus == CREATE_AND_GO)
    {

        /* Create an entry in the Selector List */

        if (MemAllocateMemBlock (SECv6_SEL_MEMPOOL, (UINT1 **) &pSecSelIf)
            != MEM_SUCCESS)
        {
            SECv6_TRC (SECv6_OS_RESOURCE,
                       "nmhSetFsipv6SelStatus : Unable to allocate memory from pool for Selector Table \n");
            return (SNMP_FAILURE);
        }

        IPSEC_MEMSET (pSecSelIf, 0, sizeof (tSecv6Selector));

        u4Count++;
        pSecSelIf->u4Count = u4Count;
        pSecSelIf->u4IfIndex = (UINT4) i4Fsipv6SelIfIndex;
        pSecSelIf->u4ProtoId = (UINT4) i4Fsipv6SelProtoIndex;
        pSecSelIf->u4Port = (UINT4) i4Fsipv6SelPort;
        pSecSelIf->u4PktDirection = (UINT4) i4Fsipv6SelPktDirection;
        pSecSelIf->u4SelAccessIndex = (UINT4) i4Fsipv6SelAccessIndex;
        pSecSelIf->u4SelStatus = ACTIVE;

        TMO_SLL_Scan (&Secv6SelList, pScanNode, tSecv6Selector *)
        {
            if (Secv6SelectorCompare (pScanNode, pSecSelIf) > 0)
            {
                break;
            }
            else
            {
                pPrevNode = pScanNode;
            }
        }

        if (pPrevNode == NULL)
        {
            /* Node to be inserted in first position */
            TMO_SLL_Insert (&Secv6SelList, NULL,
                            (tTMO_SLL_NODE *) & (pSecSelIf->NextSecSelIf));
        }
        else if (pScanNode == NULL)
        {
            /* Node to be inserted in last position */
            TMO_SLL_Insert (&Secv6SelList,
                            (tTMO_SLL_NODE *) & (pPrevNode->NextSecSelIf),
                            (tTMO_SLL_NODE *) & (pSecSelIf->NextSecSelIf));
        }
        else
        {
            /* Node to be inserted in middle */
            TMO_SLL_Insert_In_Middle (&Secv6SelList,
                                      (tTMO_SLL_NODE *) & (pPrevNode->
                                                           NextSecSelIf),
                                      (tTMO_SLL_NODE *) & (pSecSelIf->
                                                           NextSecSelIf),
                                      (tTMO_SLL_NODE *) & (pScanNode->
                                                           NextSecSelIf));
        }

    }

    if (i4SetValFsipv6SelStatus == CREATE_AND_WAIT)
    {

        /* Create an entry in the Selector List */

        if (MemAllocateMemBlock (SECv6_SEL_MEMPOOL, (UINT1 **) &pSecSelIf)
            != MEM_SUCCESS)
        {
            SECv6_TRC (SECv6_OS_RESOURCE,
                       "nmhSetFsipv6SelStatus : Unable to allocate memory from pool for Selector Table \n");
            return (SNMP_FAILURE);
        }

        IPSEC_MEMSET (pSecSelIf, 0, sizeof (tSecv6Selector));

        u4Count++;
        pSecSelIf->u4Count = u4Count;
        pSecSelIf->u4IfIndex = (UINT4) i4Fsipv6SelIfIndex;
        pSecSelIf->u4ProtoId = (UINT4) i4Fsipv6SelProtoIndex;
        pSecSelIf->u4Port = (UINT4) i4Fsipv6SelPort;
        pSecSelIf->u4PktDirection = (UINT4) i4Fsipv6SelPktDirection;
        pSecSelIf->u4SelAccessIndex = (UINT4) i4Fsipv6SelAccessIndex;
        pSecSelIf->u4SelStatus = NOT_IN_SERVICE;

        TMO_SLL_Scan (&Secv6SelList, pScanNode, tSecv6Selector *)
        {
            if (Secv6SelectorCompare (pScanNode, pSecSelIf) > 0)
            {
                break;
            }
            else
            {
                pPrevNode = pScanNode;
            }
        }

        if (pPrevNode == NULL)
        {
            /* Node to be inserted in first position */
            TMO_SLL_Insert (&Secv6SelList, NULL,
                            (tTMO_SLL_NODE *) & (pSecSelIf->NextSecSelIf));
        }
        else if (pScanNode == NULL)
        {
            /* Node to be inserted in last position */
            TMO_SLL_Insert (&Secv6SelList,
                            (tTMO_SLL_NODE *) & (pPrevNode->NextSecSelIf),
                            (tTMO_SLL_NODE *) & (pSecSelIf->NextSecSelIf));
        }
        else
        {
            /* Node to be inserted in middle */
            TMO_SLL_Insert_In_Middle (&Secv6SelList,
                                      (tTMO_SLL_NODE *) & (pPrevNode->
                                                           NextSecSelIf),
                                      (tTMO_SLL_NODE *) & (pSecSelIf->
                                                           NextSecSelIf),
                                      (tTMO_SLL_NODE *) & (pScanNode->
                                                           NextSecSelIf));
        }

    }

    if (i4SetValFsipv6SelStatus == ACTIVE)
    {
        pSecSelIf = Secv6LowSelGetEntry ((UINT4) i4Fsipv6SelIfIndex,
                                         (UINT4) i4Fsipv6SelProtoIndex,
                                         (UINT4) i4Fsipv6SelAccessIndex,
                                         i4Fsipv6SelPort,
                                         i4Fsipv6SelPktDirection);
        if (pSecSelIf == NULL)
        {
            return (SNMP_FAILURE);
        }

        pSecSelIf->u4SelStatus = ACTIVE;
    }

    if (i4SetValFsipv6SelStatus == NOT_IN_SERVICE)
    {
        pSecSelIf = Secv6LowSelGetEntry ((UINT4) i4Fsipv6SelIfIndex,
                                         (UINT4) i4Fsipv6SelProtoIndex,
                                         (UINT4) i4Fsipv6SelAccessIndex,
                                         i4Fsipv6SelPort,
                                         i4Fsipv6SelPktDirection);
        if (pSecSelIf == NULL)
        {
            return (SNMP_FAILURE);
        }
        pSecSelIf->u4SelStatus = NOT_IN_SERVICE;
    }

    if (i4SetValFsipv6SelStatus == DESTROY)
    {

        /* get the entry for the given indicies from the selector database */
        pSecSelIf = Secv6LowSelGetEntry ((UINT4) i4Fsipv6SelIfIndex,
                                         (UINT4) i4Fsipv6SelProtoIndex,
                                         (UINT4) i4Fsipv6SelAccessIndex,
                                         i4Fsipv6SelPort,
                                         i4Fsipv6SelPktDirection);

        TMO_SLL_Delete (&Secv6SelList, &pSecSelIf->NextSecSelIf);

        if (MemReleaseMemBlock (SECv6_SEL_MEMPOOL,
                                (UINT1 *) pSecSelIf) != MEM_SUCCESS)
        {
            SECv6_TRC (SECv6_OS_RESOURCE,
                       "nmhSetFsipv6SelStatus : Unable to Release memory to pool for Selector Table\n");
            return (SNMP_FAILURE);
        }

        /* get the entry for the given indicies from the formatted 
           selector database */
        pSecFormSelIf =
            Secv6LowSelGetFormattedEntry ((UINT4) i4Fsipv6SelIfIndex,
                                          (UINT4) i4Fsipv6SelProtoIndex,
                                          (UINT4) i4Fsipv6SelAccessIndex,
                                          i4Fsipv6SelPort,
                                          i4Fsipv6SelPktDirection);
        if (pSecFormSelIf != NULL)
        {

            TMO_SLL_Delete (&Secv6SelFormattedList,
                            &pSecFormSelIf->NextSecSelIf);

            if (MemReleaseMemBlock (SECv6_SEL_MEMPOOL,
                                    (UINT1 *) pSecFormSelIf) != MEM_SUCCESS)
            {
                SECv6_TRC (SECv6_OS_RESOURCE,
                           "nmhSetFsipv6SelStatus : Unable to Release memory to pool for Selector Table\n");
                return (SNMP_FAILURE);
            }

        }
    }
    return (SNMP_SUCCESS);

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SelFilterFlag
 Input       :  The Indices
                Fsipv6SelIfIndex
                Fsipv6SelProtoIndex
                Fsipv6SelAccessIndex
                Fsipv6SelPort
                Fsipv6SelPktDirection
                                

                The Object 
                testValFsipv6SelFilterFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SelFilterFlag (UINT4 *pu4ErrorCode, INT4 i4Fsipv6SelIfIndex,
                              INT4 i4Fsipv6SelProtoIndex,
                              INT4 i4Fsipv6SelAccessIndex,
                              INT4 i4Fsipv6SelPort,
                              INT4 i4Fsipv6SelPktDirection,
                              INT4 i4TestValFsipv6SelFilterFlag)
{

    tSecv6Selector     *pSecSelIf = NULL;

    /* get the entry for the given indicies from the selector database */
    pSecSelIf = Secv6LowSelGetEntry ((UINT4) i4Fsipv6SelIfIndex,
                                     (UINT4) i4Fsipv6SelProtoIndex,
                                     (UINT4) i4Fsipv6SelAccessIndex,
                                     i4Fsipv6SelPort, i4Fsipv6SelPktDirection);

    if (pSecSelIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsipv6SelFilterFlag != SEC_FILTER)
        && (i4TestValFsipv6SelFilterFlag != SEC_ALLOW))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    /* Check whether the Indices are within the range */

    if ((i4Fsipv6SelIfIndex < 0)
        || (i4Fsipv6SelProtoIndex < SEC_MIN_INTEGER)
        || (i4Fsipv6SelAccessIndex < SEC_MIN_INTEGER)
        || (i4Fsipv6SelPort < SEC_MIN_INTEGER)
        || ((i4Fsipv6SelPktDirection != SEC_INBOUND) &&
            (i4Fsipv6SelPktDirection != SEC_OUTBOUND) &&
            (i4Fsipv6SelPktDirection != SEC_ANY_DIRECTION)))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SelPolicyIndex
 Input       :  The Indices
                Fsipv6SelIfIndex
                Fsipv6SelProtoIndex
                Fsipv6SelAccessIndex
                Fsipv6SelPort
                Fsipv6SelPktDirection

                The Object 
                testValFsipv6SelPolicyIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SelPolicyIndex (UINT4 *pu4ErrorCode, INT4 i4Fsipv6SelIfIndex,
                               INT4 i4Fsipv6SelProtoIndex,
                               INT4 i4Fsipv6SelAccessIndex,
                               INT4 i4Fsipv6SelPort,
                               INT4 i4Fsipv6SelPktDirection,
                               INT4 i4TestValFsipv6SelPolicyIndex)
{

    tSecv6Selector     *pSecSelIf = NULL;
    tSecv6Policy       *pSecPolicyIf = NULL;

    /* get the entry for the given indicies from the selector database */
    pSecSelIf = Secv6LowSelGetEntry ((UINT4) i4Fsipv6SelIfIndex,
                                     (UINT4) i4Fsipv6SelProtoIndex,
                                     (UINT4) i4Fsipv6SelAccessIndex,
                                     i4Fsipv6SelPort, i4Fsipv6SelPktDirection);

    if (pSecSelIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    /* Check for the given Policy Index in the Selector List there is an 
       entry in the policy list */

    if ((pSecPolicyIf =
         Secv6LowPolicyGetEntry ((UINT4) i4TestValFsipv6SelPolicyIndex)) ==
        NULL)
    {
        *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
        return (SNMP_FAILURE);
    }
    UNUSED_PARAM (pSecPolicyIf);
    /* Check whether the Indices are within the range */

    if (i4TestValFsipv6SelPolicyIndex <= 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    /* Check whether the Indices are within the range */

    if ((i4Fsipv6SelIfIndex < 0)
        || (i4Fsipv6SelProtoIndex < SEC_MIN_INTEGER)
        || (i4Fsipv6SelAccessIndex < SEC_MIN_INTEGER)
        || (i4Fsipv6SelPort < SEC_MIN_INTEGER)
        || ((i4Fsipv6SelPktDirection != SEC_INBOUND) &&
            (i4Fsipv6SelPktDirection != SEC_OUTBOUND) &&
            (i4Fsipv6SelPktDirection != SEC_ANY_DIRECTION)))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SelIfIpAddress
 Input       :  The Indices
                Fsipv6SelIfIndex
                Fsipv6SelProtoIndex
                Fsipv6SelAccessIndex
                Fsipv6SelPort
                Fsipv6SelPktDirection

                The Object 
                testValFsipv6SelIfIpAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SelIfIpAddress (UINT4 *pu4ErrorCode, INT4 i4Fsipv6SelIfIndex,
                               INT4 i4Fsipv6SelProtoIndex,
                               INT4 i4Fsipv6SelAccessIndex,
                               INT4 i4Fsipv6SelPort,
                               INT4 i4Fsipv6SelPktDirection,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValFsipv6SelIfIpAddress)
{
    tSecv6Selector     *pSecSelIf = NULL;

    /* get the entry for the given indicies from the selector database */
    pSecSelIf = Secv6LowSelGetEntry ((UINT4) i4Fsipv6SelIfIndex,
                                     (UINT4) i4Fsipv6SelProtoIndex,
                                     (UINT4) i4Fsipv6SelAccessIndex,
                                     i4Fsipv6SelPort, i4Fsipv6SelPktDirection);

    if (pSecSelIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((pTestValFsipv6SelIfIpAddress->i4_Length <= 0)
        || (pTestValFsipv6SelIfIpAddress->i4_Length > SEC_ADDR_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SelStatus
 Input       :  The Indices
                Fsipv6SelIfIndex
                Fsipv6SelProtoIndex
                Fsipv6SelAccessIndex
                Fsipv6SelPort
                Fsipv6SelPktDirection

                The Object 
                testValFsipv6SelStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SelStatus (UINT4 *pu4ErrorCode, INT4 i4Fsipv6SelIfIndex,
                          INT4 i4Fsipv6SelProtoIndex,
                          INT4 i4Fsipv6SelAccessIndex,
                          INT4 i4Fsipv6SelPort,
                          INT4 i4Fsipv6SelPktDirection,
                          INT4 i4TestValFsipv6SelStatus)
{

    tSecv6Selector     *pSecSelIf = NULL;
    tSecv6Access       *pSecAccIf = NULL;

    /* get the entry for the given indicies from the selector database */
    pSecSelIf = Secv6LowSelGetEntry ((UINT4) i4Fsipv6SelIfIndex,
                                     (UINT4) i4Fsipv6SelProtoIndex,
                                     (UINT4) i4Fsipv6SelAccessIndex,
                                     i4Fsipv6SelPort, i4Fsipv6SelPktDirection);

    if (gSecv6Status == SEC_ENABLE)
    {
        *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
        return (SNMP_FAILURE);
    }
    if ((i4TestValFsipv6SelStatus != CREATE_AND_GO)
        && (i4TestValFsipv6SelStatus != CREATE_AND_WAIT)
        && (i4TestValFsipv6SelStatus != DESTROY)
        && (i4TestValFsipv6SelStatus != ACTIVE)
        && (i4TestValFsipv6SelStatus != NOT_IN_SERVICE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsipv6SelStatus == CREATE_AND_GO)
    {

        if (pSecSelIf != NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (SNMP_FAILURE);
        }

        /*Check for the given access index there is an entry in the 
           access list */
        pSecAccIf = Secv6LowAccessGetEntry ((UINT4) i4Fsipv6SelAccessIndex);

        if (pSecAccIf == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
            return (SNMP_FAILURE);
        }

    }

    if (i4TestValFsipv6SelStatus == DESTROY)
    {

        if (pSecSelIf == NULL)
        {

            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }

    }

    /* Check whether the Indices are within the range */

    if ((i4Fsipv6SelIfIndex < 0)
        || (i4Fsipv6SelProtoIndex < SEC_MIN_INTEGER)
        || (i4Fsipv6SelAccessIndex < SEC_MIN_INTEGER)
        || (i4Fsipv6SelPort < SEC_MIN_INTEGER)
        || ((i4Fsipv6SelPktDirection != SEC_INBOUND) &&
            (i4Fsipv6SelPktDirection != SEC_OUTBOUND) &&
            (i4Fsipv6SelPktDirection != SEC_ANY_DIRECTION)))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/* Low Level Dependency Routines for All Objects  */
/****************************************************************************
 Function    :  nmhDepv2Fsipv6SecSelectorTable
 Input       :  The Indices
                Fsipv6SelIfIndex
                Fsipv6SelProtoIndex
                Fsipv6SelAccessIndex
                Fsipv6SelPort
                Fsipv6SelPktDirection
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6SecSelectorTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6SecAccessTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6SecAccessTable
 Input       :  The Indices
                Fsipv6SecAccessIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsipv6SecAccessTable (INT4 i4Fsipv6SecAccessIndex)
{

    /* Check whether the Indices are within the range */

    if (i4Fsipv6SecAccessIndex < SEC_MIN_INTEGER)
    {
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6SecAccessTable
 Input       :  The Indices
                Fsipv6SecAccessIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsipv6SecAccessTable (INT4 *pi4Fsipv6SecAccessIndex)
{

    tSecv6Access       *pSecAccIf = NULL;
    UINT4               u4AccessIndex;

    pSecAccIf = (tSecv6Access *) TMO_SLL_First (&Secv6AccessList);

    if (pSecAccIf == NULL)
        return SNMP_FAILURE;

    u4AccessIndex = pSecAccIf->u4GroupIndex;

    /* Get the First lexicographically ordered first index */

    TMO_SLL_Scan (&Secv6AccessList, pSecAccIf, tSecv6Access *)
    {
        if (pSecAccIf->u4GroupIndex < u4AccessIndex)
        {
            u4AccessIndex = pSecAccIf->u4GroupIndex;
        }
    }
    *pi4Fsipv6SecAccessIndex = (INT4) u4AccessIndex;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6SecAccessTable
 Input       :  The Indices
                Fsipv6SecAccessIndex
                nextFsipv6SecAccessIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6SecAccessTable (INT4 i4Fsipv6SecAccessIndex,
                                     INT4 *pi4NextFsipv6SecAccessIndex)
{

    tSecv6Access       *pSecAccIf = NULL;
    INT4                Flag = SEC_NOT_FOUND;

    pSecAccIf = (tSecv6Access *) TMO_SLL_First (&Secv6AccessList);

    if (pSecAccIf == NULL)
        return SNMP_FAILURE;

    /* To get the Next lexicographically orederd index for the given Index */

    TMO_SLL_Scan (&Secv6AccessList, pSecAccIf, tSecv6Access *)
    {
        if (pSecAccIf->u4GroupIndex > (UINT4) i4Fsipv6SecAccessIndex)
        {

            if (Flag == SEC_NOT_FOUND)
            {
                *pi4NextFsipv6SecAccessIndex = (INT4) pSecAccIf->u4GroupIndex;

                Flag = SEC_FOUND;
            }
            else
            {

                if (pSecAccIf->u4GroupIndex <
                    (FS_ULONG) pi4NextFsipv6SecAccessIndex)
                {
                    *pi4NextFsipv6SecAccessIndex =
                        (INT4) pSecAccIf->u4GroupIndex;
                }
            }
        }
    }
    if (Flag == SEC_NOT_FOUND)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsipv6SecAccessStatus
 Input       :  The Indices
                Fsipv6SecAccessIndex

                The Object 
                retValFsipv6SecAccessStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecAccessStatus (INT4 i4Fsipv6SecAccessIndex,
                             INT4 *pi4RetValFsipv6SecAccessStatus)
{
    tSecv6Access       *pSecAccIf = NULL;

    /* get the entry from the inaccess list */
    pSecAccIf = Secv6LowAccessGetEntry ((UINT4) i4Fsipv6SecAccessIndex);

    if (pSecAccIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    else
    {
        *pi4RetValFsipv6SecAccessStatus = (INT4) pSecAccIf->u4AccStatus;
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv6SecSrcNet
 Input       :  The Indices
                Fsipv6SecAccessIndex

                The Object 
                retValFsipv6SecSrcNet
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecSrcNet (INT4 i4Fsipv6SecAccessIndex,
                       tSNMP_OCTET_STRING_TYPE * pRetValFsipv6SecSrcNet)
{

    tSecv6Access       *pSecAccIf = NULL;

    /* get the entry from the inaccess list */
    pSecAccIf = Secv6LowAccessGetEntry ((UINT4) i4Fsipv6SecAccessIndex);

    if (pSecAccIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    Ip6AddrCopy ((tIp6Addr *) (VOID *) pRetValFsipv6SecSrcNet->pu1_OctetList,
                 &pSecAccIf->SrcAddress);
    pRetValFsipv6SecSrcNet->i4_Length = SEC_ADDR_LEN;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsipv6SecSrcAddrPrefixLen
 Input       :  The Indices
                Fsipv6SecAccessIndex

                The Object 
                retValFsipv6SecSrcAddrPrefixLen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecSrcAddrPrefixLen (INT4 i4Fsipv6SecAccessIndex,
                                 INT4 *pi4RetValFsipv6SecSrcAddrPrefixLen)
{

    tSecv6Access       *pSecAccIf = NULL;

    /* get the entry from the inaccess list */
    pSecAccIf = Secv6LowAccessGetEntry ((UINT4) i4Fsipv6SecAccessIndex);

    if (pSecAccIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFsipv6SecSrcAddrPrefixLen = (INT4) pSecAccIf->u4SrcAddrPrefixLen;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsipv6SecDestNet
 Input       :  The Indices
                Fsipv6SecAccessIndex

                The Object 
                retValFsipv6SecDestNet
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecDestNet (INT4 i4Fsipv6SecAccessIndex,
                        tSNMP_OCTET_STRING_TYPE * pRetValFsipv6SecDestNet)
{

    tSecv6Access       *pSecAccIf = NULL;

    /* get an entry from the outaccess list */
    pSecAccIf = Secv6LowAccessGetEntry ((UINT4) i4Fsipv6SecAccessIndex);

    if (pSecAccIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    Ip6AddrCopy ((tIp6Addr *) (VOID *) pRetValFsipv6SecDestNet->pu1_OctetList,
                 &pSecAccIf->DestAddress);
    pRetValFsipv6SecDestNet->i4_Length = SEC_ADDR_LEN;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsipv6SecDestAddrPrefixLen
 Input       :  The Indices
                Fsipv6SecAccessIndex

                The Object 
                retValFsipv6SecDestAddrPrefixLen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecDestAddrPrefixLen (INT4 i4Fsipv6SecAccessIndex,
                                  INT4 *pi4RetValFsipv6SecDestAddrPrefixLen)
{

    tSecv6Access       *pSecAccIf = NULL;

    /* get an entry from the outaccess list */
    pSecAccIf = Secv6LowAccessGetEntry ((UINT4) i4Fsipv6SecAccessIndex);

    if (pSecAccIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFsipv6SecDestAddrPrefixLen =
        (INT4) pSecAccIf->u4DestAddrPrefixLen;
    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv6SecAccessStatus
 Input       :  The Indices
                Fsipv6SecAccessIndex

                The Object 
                setValFsipv6SecAccessStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SecAccessStatus (INT4 i4Fsipv6SecAccessIndex,
                             INT4 i4SetValFsipv6SecAccessStatus)
{

    tSecv6Access       *pSecAccIf = NULL;

    if (i4SetValFsipv6SecAccessStatus == CREATE_AND_GO)
    {

        if (MemAllocateMemBlock (SECv6_ACC_MEMPOOL, (UINT1 **) &pSecAccIf)
            != MEM_SUCCESS)
        {
            SECv6_TRC (SECv6_OS_RESOURCE,
                       "nmhSetFsipv6SecAccessStatus : Unable to allocate memory from pool for Access Table \n");
            return (SNMP_FAILURE);
        }

        IPSEC_MEMSET (pSecAccIf, 0, sizeof (tSecv6Access));
        pSecAccIf->u4GroupIndex = (UINT4) i4Fsipv6SecAccessIndex;
        pSecAccIf->u4AccStatus = ACTIVE;
        TMO_SLL_Add (&Secv6AccessList, &pSecAccIf->NextSecGroupIf);

    }

    if (i4SetValFsipv6SecAccessStatus == CREATE_AND_WAIT)
    {

        if (MemAllocateMemBlock (SECv6_ACC_MEMPOOL, (UINT1 **) &pSecAccIf)
            != MEM_SUCCESS)
        {
            SECv6_TRC (SECv6_OS_RESOURCE,
                       "nmhSetFsipv6SecAccessStatus : Unable to allocate memory from pool for Access Table \n");
            return (SNMP_FAILURE);
        }

        IPSEC_MEMSET (pSecAccIf, 0, sizeof (tSecv6Access));
        pSecAccIf->u4GroupIndex = (UINT4) i4Fsipv6SecAccessIndex;
        pSecAccIf->u4AccStatus = NOT_IN_SERVICE;
        TMO_SLL_Add (&Secv6AccessList, &pSecAccIf->NextSecGroupIf);

    }
    if (i4SetValFsipv6SecAccessStatus == ACTIVE)
    {
        pSecAccIf = Secv6LowAccessGetEntry ((UINT4) i4Fsipv6SecAccessIndex);
        if (pSecAccIf == NULL)
        {
            return (SNMP_FAILURE);
        }

        pSecAccIf->u4AccStatus = ACTIVE;
    }

    if (i4SetValFsipv6SecAccessStatus == NOT_IN_SERVICE)
    {
        pSecAccIf = Secv6LowAccessGetEntry ((UINT4) i4Fsipv6SecAccessIndex);
        if (pSecAccIf == NULL)
        {
            return (SNMP_FAILURE);
        }
        pSecAccIf->u4AccStatus = NOT_IN_SERVICE;
    }

    if (i4SetValFsipv6SecAccessStatus == DESTROY)
    {

        pSecAccIf = Secv6LowAccessGetEntry ((UINT4) i4Fsipv6SecAccessIndex);
        TMO_SLL_Delete (&Secv6AccessList, &pSecAccIf->NextSecGroupIf);

        if (MemReleaseMemBlock (SECv6_ACC_MEMPOOL,
                                (UINT1 *) pSecAccIf) != MEM_SUCCESS)
        {
            SECv6_TRC (SECv6_OS_RESOURCE,
                       "nmhSetFsipv6SecAccessStatus : Unable to Release memory to pool for Access Table\n");
            return (SNMP_FAILURE);
        }

    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsipv6SecSrcNet
 Input       :  The Indices
                Fsipv6SecAccessIndex

                The Object 
                setValFsipv6SecSrcNet
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SecSrcNet (INT4 i4Fsipv6SecAccessIndex,
                       tSNMP_OCTET_STRING_TYPE * pSetValFsipv6SecSrcNet)
{

    tSecv6Access       *pSecAccIf = NULL;

    /* get the entry from the access list */
    pSecAccIf = Secv6LowAccessGetEntry ((UINT4) i4Fsipv6SecAccessIndex);
    if (pSecAccIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    Ip6AddrCopy (&pSecAccIf->SrcAddress,
                 (tIp6Addr *) (VOID *) pSetValFsipv6SecSrcNet->pu1_OctetList);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv6SecSrcAddrPrefixLen
 Input       :  The Indices
                Fsipv6SecAccessIndex

                The Object 
                setValFsipv6SecSrcAddrPrefixLen
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SecSrcAddrPrefixLen (INT4 i4Fsipv6SecAccessIndex,
                                 INT4 i4SetValFsipv6SecSrcAddrPrefixLen)
{

    tSecv6Access       *pSecAccIf = NULL;

    /* get the entry from the access list */
    pSecAccIf = Secv6LowAccessGetEntry ((UINT4) i4Fsipv6SecAccessIndex);
    if (pSecAccIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    pSecAccIf->u4SrcAddrPrefixLen = (UINT4) i4SetValFsipv6SecSrcAddrPrefixLen;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv6SecDestNet
 Input       :  The Indices
                Fsipv6SecAccessIndex

                The Object 
                setValFsipv6SecDestNet
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SecDestNet (INT4 i4Fsipv6SecAccessIndex,
                        tSNMP_OCTET_STRING_TYPE * pSetValFsipv6SecDestNet)
{

    tSecv6Access       *pSecAccIf = NULL;

    /* get an entry from the outaccess list */
    pSecAccIf = Secv6LowAccessGetEntry ((UINT4) i4Fsipv6SecAccessIndex);
    if (pSecAccIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    Ip6AddrCopy (&pSecAccIf->DestAddress,
                 (tIp6Addr *) (VOID *) pSetValFsipv6SecDestNet->pu1_OctetList);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv6SecDestAddrPrefixLen
 Input       :  The Indices
                Fsipv6SecAccessIndex

                The Object 
                setValFsipv6SecDestAddrPrefixLen
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SecDestAddrPrefixLen (INT4 i4Fsipv6SecAccessIndex,
                                  INT4 i4SetValFsipv6SecDestAddrPrefixLen)
{
    tSecv6Access       *pSecAccIf = NULL;

    /* get an entry from the outaccess list */
    pSecAccIf = Secv6LowAccessGetEntry ((UINT4) i4Fsipv6SecAccessIndex);
    if (pSecAccIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    pSecAccIf->u4DestAddrPrefixLen = (UINT4) i4SetValFsipv6SecDestAddrPrefixLen;

    return (SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SecAccessStatus
 Input       :  The Indices
                Fsipv6SecAccessIndex

                The Object 
                testValFsipv6SecAccessStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SecAccessStatus (UINT4 *pu4ErrorCode,
                                INT4 i4Fsipv6SecAccessIndex,
                                INT4 i4TestValFsipv6SecAccessStatus)
{

    tSecv6Access       *pSecAccIf = NULL;

    if (i4Fsipv6SecAccessIndex < SEC_MIN_INTEGER)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    pSecAccIf = Secv6LowAccessGetEntry ((UINT4) i4Fsipv6SecAccessIndex);
    if ((i4TestValFsipv6SecAccessStatus != CREATE_AND_GO)
        && (i4TestValFsipv6SecAccessStatus != CREATE_AND_WAIT)
        && (i4TestValFsipv6SecAccessStatus != DESTROY)
        && (i4TestValFsipv6SecAccessStatus != ACTIVE)
        && (i4TestValFsipv6SecAccessStatus != NOT_IN_SERVICE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (gSecv6Status == SEC_ENABLE)
    {
        *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
        return (SNMP_FAILURE);
    }

    if (i4TestValFsipv6SecAccessStatus == DESTROY)
    {
        if (Secv6LowAccessGetEntry ((UINT4) i4Fsipv6SecAccessIndex) == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
    }

    if (i4TestValFsipv6SecAccessStatus == CREATE_AND_GO)
    {
        if (pSecAccIf != NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SecSrcNet
 Input       :  The Indices
                Fsipv6SecAccessIndex

                The Object 
                testValFsipv6SecSrcNet
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SecSrcNet (UINT4 *pu4ErrorCode,
                          INT4 i4Fsipv6SecAccessIndex,
                          tSNMP_OCTET_STRING_TYPE * pTestValFsipv6SecSrcNet)
{

    tSecv6Access       *pEntry = NULL;

    /* get the entry from the access list */
    pEntry = Secv6LowAccessGetEntry ((UINT4) i4Fsipv6SecAccessIndex);

    if (pEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((pTestValFsipv6SecSrcNet->i4_Length <= 0)
        || (pTestValFsipv6SecSrcNet->i4_Length > SEC_ADDR_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SecSrcAddrPrefixLen
 Input       :  The Indices
                Fsipv6SecAccessIndex

                The Object 
                testValFsipv6SecSrcAddrPrefixLen
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SecSrcAddrPrefixLen (UINT4 *pu4ErrorCode,
                                    INT4 i4Fsipv6SecAccessIndex,
                                    INT4 i4TestValFsipv6SecSrcAddrPrefixLen)
{

    tSecv6Access       *pEntry = NULL;
/* get the entry from the access list */
    pEntry = Secv6LowAccessGetEntry ((UINT4) i4Fsipv6SecAccessIndex);

    if (pEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6SecSrcAddrPrefixLen <= 0)
        || (i4TestValFsipv6SecSrcAddrPrefixLen > SEC_MAX_PREFIX_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SecDestNet
 Input       :  The Indices
                Fsipv6SecAccessIndex

                The Object 
                testValFsipv6SecDestNet
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SecDestNet (UINT4 *pu4ErrorCode,
                           INT4 i4Fsipv6SecAccessIndex,
                           tSNMP_OCTET_STRING_TYPE * pTestValFsipv6SecDestNet)
{
    tSecv6Access       *pEntry = NULL;
/* get the entry from the access list */
    pEntry = Secv6LowAccessGetEntry ((UINT4) i4Fsipv6SecAccessIndex);

    if (pEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((pTestValFsipv6SecDestNet->i4_Length <= 0)
        || (pTestValFsipv6SecDestNet->i4_Length > SEC_ADDR_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SecDestAddrPrefixLen
 Input       :  The Indices
                Fsipv6SecAccessIndex

                The Object 
                testValFsipv6SecDestAddrPrefixLen
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SecDestAddrPrefixLen (UINT4 *pu4ErrorCode,
                                     INT4 i4Fsipv6SecAccessIndex,
                                     INT4 i4TestValFsipv6SecDestAddrPrefixLen)
{

    tSecv6Access       *pEntry = NULL;
/* get the entry from the access list */
    pEntry = Secv6LowAccessGetEntry ((UINT4) i4Fsipv6SecAccessIndex);

    if (pEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv6SecDestAddrPrefixLen <= 0)
        || (i4TestValFsipv6SecDestAddrPrefixLen > SEC_MAX_PREFIX_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/* Low Level Dependency Routines for All Objects  */
/****************************************************************************
 Function    :  nmhDepv2Fsipv6SecAccessTable
 Input       :  The Indices
                Fsipv6SecAccessIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6SecAccessTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6SecPolicyTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6SecPolicyTable
 Input       :  The Indices
                Fsipv6SecPolicyIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsipv6SecPolicyTable (INT4 i4Fsipv6SecPolicyIndex)
{

    if (i4Fsipv6SecPolicyIndex < SEC_MIN_INTEGER)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6SecPolicyTable
 Input       :  The Indices
                Fsipv6SecPolicyIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv6SecPolicyTable (INT4 *pi4Fsipv6SecPolicyIndex)
{
    tSecv6Policy       *pSecPolicyIf = NULL;
    UINT4               u4FirstIndex = SEC_MAX_INTEGER;
    INT4                Flag = SEC_NOT_FOUND;

    /*To get the first lexicographically ordered index */

    TMO_SLL_Scan (&Secv6PolicyList, pSecPolicyIf, tSecv6Policy *)
    {
        Flag = SEC_FOUND;
        if (pSecPolicyIf->u4PolicyIndex < (UINT4) u4FirstIndex)
        {
            u4FirstIndex = pSecPolicyIf->u4PolicyIndex;
        }
    }
    if (!Flag)
    {
        return SNMP_FAILURE;
    }
    *pi4Fsipv6SecPolicyIndex = (INT4) u4FirstIndex;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6SecPolicyTable
 Input       :  The Indices
                Fsipv6SecPolicyIndex
                nextFsipv6SecPolicyIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6SecPolicyTable (INT4 i4Fsipv6SecPolicyIndex,
                                     INT4 *pi4NextFsipv6SecPolicyIndex)
{

    tSecv6Policy       *pSecPolicyIf = NULL;
    INT4                Flag = SEC_NOT_FOUND;

    /* To get the Next lexicographically orederd index for the given index */

    TMO_SLL_Scan (&Secv6PolicyList, pSecPolicyIf, tSecv6Policy *)
    {
        if (pSecPolicyIf->u4PolicyIndex > (UINT4) i4Fsipv6SecPolicyIndex)
        {
            if (Flag == SEC_NOT_FOUND)
            {
                *pi4NextFsipv6SecPolicyIndex =
                    (INT4) pSecPolicyIf->u4PolicyIndex;
                Flag = SEC_FOUND;
            }
            else
            {

                if (pSecPolicyIf->u4PolicyIndex <
                    (UINT4) *pi4NextFsipv6SecPolicyIndex)
                {
                    *pi4NextFsipv6SecPolicyIndex =
                        (INT4) pSecPolicyIf->u4PolicyIndex;
                }
            }
        }
    }
    if (Flag == SEC_NOT_FOUND)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6SecPolicyFlag
 Input       :  The Indices
                Fsipv6SecPolicyIndex

                The Object 
                retValFsipv6SecPolicyFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecPolicyFlag (INT4 i4Fsipv6SecPolicyIndex,
                           INT4 *pi4RetValFsipv6SecPolicyFlag)
{

    tSecv6Policy       *pSecPolicyIf = NULL;

    pSecPolicyIf = Secv6LowPolicyGetEntry ((UINT4) i4Fsipv6SecPolicyIndex);

    if (pSecPolicyIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValFsipv6SecPolicyFlag = pSecPolicyIf->u1PolicyFlag;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsipv6SecPolicyMode
 Input       :  The Indices
                Fsipv6SecPolicyIndex

                The Object 
                retValFsipv6SecPolicyMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecPolicyMode (INT4 i4Fsipv6SecPolicyIndex,
                           INT4 *pi4RetValFsipv6SecPolicyMode)
{

    tSecv6Policy       *pSecPolicyIf = NULL;

    pSecPolicyIf = Secv6LowPolicyGetEntry ((UINT4) i4Fsipv6SecPolicyIndex);

    if (pSecPolicyIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValFsipv6SecPolicyMode = pSecPolicyIf->u1PolicyMode;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsipv6SecPolicySaBundle
 Input       :  The Indices
                Fsipv6SecPolicyIndex

                The Object
                retValFsipv6SecPolicySaBundle
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecPolicySaBundle (INT4 i4Fsipv6SecPolicyIndex,
                               tSNMP_OCTET_STRING_TYPE
                               * pRetValFsipv6SecPolicySaBundle)
{

    tSecv6Policy       *pSecPolicyIf = NULL;
    pSecPolicyIf = Secv6LowPolicyGetEntry ((UINT4) i4Fsipv6SecPolicyIndex);

    if (pSecPolicyIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    IPSEC_STRCPY (pRetValFsipv6SecPolicySaBundle->
                  pu1_OctetList, pSecPolicyIf->au1PolicySaBundle);
    pRetValFsipv6SecPolicySaBundle->i4_Length =
        (INT4) STRLEN (pSecPolicyIf->au1PolicySaBundle);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv6SecPolicyOptionsIndex
 Input       :  The Indices
                Fsipv6SecPolicyIndex

                The Object 
                retValFsipv6SecPolicyOptionsIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecPolicyOptionsIndex (INT4 i4Fsipv6SecPolicyIndex,
                                   INT4 *pi4RetValFsipv6SecPolicyOptionsIndex)
{
    tSecv6Policy       *pSecPolicyIf = NULL;

    pSecPolicyIf = Secv6LowPolicyGetEntry ((UINT4) i4Fsipv6SecPolicyIndex);

    if (pSecPolicyIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFsipv6SecPolicyOptionsIndex =
        (INT4) pSecPolicyIf->u4PolicyOptionsIndex;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv6SecPolicyStatus
 Input       :  The Indices
                Fsipv6SecPolicyIndex

                The Object 
                retValFsipv6SecPolicyStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecPolicyStatus (INT4 i4Fsipv6SecPolicyIndex,
                             INT4 *pi4RetValFsipv6SecPolicyStatus)
{

    tSecv6Policy       *pSecPolicyIf = NULL;

    pSecPolicyIf = Secv6LowPolicyGetEntry ((UINT4) i4Fsipv6SecPolicyIndex);

    if (pSecPolicyIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValFsipv6SecPolicyStatus = (INT4) pSecPolicyIf->u4PolicyStatus;

    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsipv6SecPolicyFlag
 Input       :  The Indices
                Fsipv6SecPolicyIndex

                The Object 
                setValFsipv6SecPolicyFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SecPolicyFlag (INT4 i4Fsipv6SecPolicyIndex,
                           INT4 i4SetValFsipv6SecPolicyFlag)
{

    tSecv6Policy       *pSecPolicyIf = NULL;

    pSecPolicyIf = Secv6LowPolicyGetEntry ((UINT4) i4Fsipv6SecPolicyIndex);
    if (pSecPolicyIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    pSecPolicyIf->u1PolicyFlag = (UINT1) i4SetValFsipv6SecPolicyFlag;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsipv6SecPolicyMode
 Input       :  The Indices
                Fsipv6SecPolicyIndex

                The Object 
                setValFsipv6SecPolicyMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SecPolicyMode (INT4 i4Fsipv6SecPolicyIndex,
                           INT4 i4SetValFsipv6SecPolicyMode)
{

    tSecv6Policy       *pSecPolicyIf = NULL;

    pSecPolicyIf = Secv6LowPolicyGetEntry ((UINT4) i4Fsipv6SecPolicyIndex);
    if (pSecPolicyIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    pSecPolicyIf->u1PolicyMode = (UINT1) i4SetValFsipv6SecPolicyMode;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsipv6SecPolicySaBundle
 Input       :  The Indices
                Fsipv6SecPolicyIndex

                The Object
                setValFsipv6SecPolicySaBundle
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SecPolicySaBundle (INT4 i4Fsipv6SecPolicyIndex,
                               tSNMP_OCTET_STRING_TYPE
                               * pSetValFsipv6SecPolicySaBundle)
{
    tSecv6Policy       *pSecPolicyIf = NULL;
    INT4                i4BackCount = SEC_BACK_COUNT;
    INT4                i4Num = 0;
    INT4                i4Tens = SEC_TENS;
    UINT4               u4Time = 0;

    pSecPolicyIf = Secv6LowPolicyGetEntry ((UINT4) i4Fsipv6SecPolicyIndex);
    if (pSecPolicyIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    IPSEC_MEMSET (pSecPolicyIf->au1PolicySaBundle, 0, SEC_MAX_SA_BUNDLE_LEN);
    IPSEC_MEMSET (pSecPolicyIf->paSaEntry, 0, SEC_MAX_BUNDLE_SA);
    STRNCPY (pSecPolicyIf->au1PolicySaBundle,
             pSetValFsipv6SecPolicySaBundle->pu1_OctetList,
             SEC_MAX_BUNDLE_SA - 1);
    pSecPolicyIf->u1SaCount = 0;
    /* Extract the secassoc indicies form the sa bundle in string format
       and store them as integers */
    do
    {
        i4BackCount++;
        i4Num = 0;
        while ((pSetValFsipv6SecPolicySaBundle->
                pu1_OctetList[i4BackCount] != '.')
               && (pSetValFsipv6SecPolicySaBundle->
                   pu1_OctetList[i4BackCount] != '\0'))
        {
            i4Num = i4Num * i4Tens +
                (pSetValFsipv6SecPolicySaBundle->
                 pu1_OctetList[i4BackCount] - '0');
            i4BackCount++;
        }
        pSecPolicyIf->paSaEntry[pSecPolicyIf->u1SaCount] =
            Secv6LowAssocGetEntry ((UINT4) i4Num);
        pSecPolicyIf->paSaEntry[pSecPolicyIf->u1SaCount]->u4PolicyIndex =
            (UINT4) i4Fsipv6SecPolicyIndex;
        pSecPolicyIf->u1SaCount++;
    }
    while (i4BackCount < pSetValFsipv6SecPolicySaBundle->i4_Length);

    if (pSecPolicyIf->u1PolicyMode == SEC_AUTOMATIC)
    {

        if (pSecPolicyIf->paSaEntry[0]->u4LifeTime != 0)
        {
            OsixGetSysTime (&u4Time);
            u4Time = (SEC_MIN_SOFTLIFETIME_FACTOR + (u4Time % 10));

            u4Time = ((pSecPolicyIf->paSaEntry[0]->u4LifeTime * u4Time) / 100);
            pSecPolicyIf->paSaEntry[0]->Secv6AssocSoftTimer.u4Param1 =
                (FS_ULONG) (pSecPolicyIf);
            pSecPolicyIf->paSaEntry[0]->Secv6AssocSoftTimer.u4SaIndex =
                pSecPolicyIf->paSaEntry[0]->u4SecAssocIndex;
            pSecPolicyIf->paSaEntry[0]->Secv6AssocSoftTimer.u1TimerId =
                SECv6_SA_SOFT_TIMER_ID;
            pSecPolicyIf->paSaEntry[0]->Secv6AssocSoftTimer.u1TimerStatus =
                SEC_TIMER_ACTIVE;
            Secv6StartTimer (&
                             (pSecPolicyIf->paSaEntry[0]->
                              Secv6AssocSoftTimer), u4Time);
            pSecPolicyIf->paSaEntry[0]->Secv6AssocHardTimer.u4Param1 =
                (FS_ULONG) (pSecPolicyIf);
            pSecPolicyIf->paSaEntry[0]->Secv6AssocHardTimer.u4SaIndex =
                (UINT4) pSecPolicyIf->paSaEntry[0]->u4SecAssocIndex;
            pSecPolicyIf->paSaEntry[0]->Secv6AssocHardTimer.u1TimerId =
                SECv6_SA_HARD_TIMER_ID;
            pSecPolicyIf->paSaEntry[0]->Secv6AssocHardTimer.u1TimerStatus =
                SEC_TIMER_ACTIVE;
            Secv6StartTimer (&
                             (pSecPolicyIf->paSaEntry[0]->
                              Secv6AssocHardTimer),
                             pSecPolicyIf->paSaEntry[0]->u4LifeTime);

        }
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv6SecPolicyOptionsIndex
 Input       :  The Indices
                Fsipv6SecPolicyIndex

                The Object 
                setValFsipv6SecPolicyOptionsIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SecPolicyOptionsIndex (INT4 i4Fsipv6SecPolicyIndex,
                                   INT4 i4SetValFsipv6SecPolicyOptionsIndex)
{
    tSecv6Policy       *pSecPolicyIf = NULL;

    pSecPolicyIf = Secv6LowPolicyGetEntry ((UINT4) i4Fsipv6SecPolicyIndex);
    if (pSecPolicyIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    pSecPolicyIf->u4PolicyOptionsIndex =
        (UINT4) i4SetValFsipv6SecPolicyOptionsIndex;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv6SecPolicyStatus
 Input       :  The Indices
                Fsipv6SecPolicyIndex

                The Object 
                setValFsipv6SecPolicyStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SecPolicyStatus (INT4 i4Fsipv6SecPolicyIndex,
                             INT4 i4SetValFsipv6SecPolicyStatus)
{

    tSecv6Policy       *pSecPolicyIf = NULL;

    pSecPolicyIf = Secv6LowPolicyGetEntry ((UINT4) i4Fsipv6SecPolicyIndex);

    if (i4SetValFsipv6SecPolicyStatus == CREATE_AND_GO)
    {

        if (MemAllocateMemBlock (SECv6_POL_MEMPOOL, (UINT1 **) &pSecPolicyIf)
            != MEM_SUCCESS)
        {
            SECv6_TRC (SECv6_OS_RESOURCE,
                       "nmhSetFsipv6SecPolicyStatus : Unable to allocate memory from pool for Policy Table \n");
            return (SNMP_FAILURE);
        }

        if (pSecPolicyIf == NULL)
        {
            SECv6_TRC (SECv6_OS_RESOURCE,
                       "nmhSetFsipv6SecPolicyStatus : Pointer value for Policy Table is NULL \n");
            return (SNMP_FAILURE);

        }
        IPSEC_MEMSET (pSecPolicyIf, 0, sizeof (tSecv6Policy));

        /* Add an entry in the policy list */

        pSecPolicyIf->u4PolicyIndex = (UINT4) i4Fsipv6SecPolicyIndex;
        pSecPolicyIf->u4PolicyStatus = ACTIVE;
        TMO_SLL_Add (&Secv6PolicyList, &pSecPolicyIf->NextSecPolicyIf);
    }
    if (i4SetValFsipv6SecPolicyStatus == CREATE_AND_WAIT)
    {

        if (MemAllocateMemBlock (SECv6_POL_MEMPOOL, (UINT1 **) &pSecPolicyIf)
            != MEM_SUCCESS)
        {
            SECv6_TRC (SECv6_OS_RESOURCE,
                       "nmhSetFsipv6SecPolicyStatus : Unable to allocate memory from pool for Policy Table \n");
            return (SNMP_FAILURE);
        }

        if (pSecPolicyIf == NULL)
        {
            SECv6_TRC (SECv6_OS_RESOURCE,
                       "nmhSetFsipv6SecPolicyStatus : Pointer value  for Policy Table is NULL\n");
            return (SNMP_FAILURE);

        }
        IPSEC_MEMSET (pSecPolicyIf, 0, sizeof (tSecv6Policy));

        /* Add an entry in the policy list */

        pSecPolicyIf->u4PolicyIndex = (UINT4) i4Fsipv6SecPolicyIndex;
        pSecPolicyIf->u4PolicyStatus = NOT_IN_SERVICE;
        TMO_SLL_Add (&Secv6PolicyList, &pSecPolicyIf->NextSecPolicyIf);
    }
    if (i4SetValFsipv6SecPolicyStatus == ACTIVE)
    {
        pSecPolicyIf = Secv6LowPolicyGetEntry ((UINT4) i4Fsipv6SecPolicyIndex);
        if (pSecPolicyIf == NULL)
        {
            return (SNMP_FAILURE);
        }
        pSecPolicyIf->u4PolicyStatus = ACTIVE;
    }
    if (i4SetValFsipv6SecPolicyStatus == NOT_IN_SERVICE)
    {
        pSecPolicyIf = Secv6LowPolicyGetEntry ((UINT4) i4Fsipv6SecPolicyIndex);
        if (pSecPolicyIf == NULL)
        {
            return (SNMP_FAILURE);
        }
        pSecPolicyIf->u4PolicyStatus = NOT_IN_SERVICE;
    }

    if (i4SetValFsipv6SecPolicyStatus == DESTROY)
    {

        pSecPolicyIf = Secv6LowPolicyGetEntry ((UINT4) i4Fsipv6SecPolicyIndex);
        if (pSecPolicyIf == NULL)
        {
            return (SNMP_FAILURE);
        }
        TMO_SLL_Delete (&Secv6PolicyList, &pSecPolicyIf->NextSecPolicyIf);
        if (pSecPolicyIf->Secv6PolicyTrigIkeTmr.u1TimerStatus ==
            SEC_TIMER_ACTIVE)
        {
            pSecPolicyIf->Secv6PolicyTrigIkeTmr.u1TimerStatus =
                SEC_TIMER_INACTIVE;
            Secv6StopTimer (&(pSecPolicyIf->Secv6PolicyTrigIkeTmr));
        }
        Secv6DeletePolicyPtrInSel ((UINT4) i4Fsipv6SecPolicyIndex);
        if (MemReleaseMemBlock (SECv6_POL_MEMPOOL,
                                (UINT1 *) pSecPolicyIf) != MEM_SUCCESS)
        {
            SECv6_TRC (SECv6_OS_RESOURCE,
                       "nmhSetFsipv6SecPolicyStatus : Unable to Release"
                       "memory to pool for Policy Table \n");
            return (SNMP_FAILURE);
        }

    }
    return (SNMP_SUCCESS);

}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2Fsipv6SecPolicyFlag
 Input       :  The Indices
                Fsipv6SecPolicyIndex

                The Object 
                testValFsipv6SecPolicyFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SecPolicyFlag (UINT4 *pu4ErrorCode, INT4 i4Fsipv6SecPolicyIndex,
                              INT4 i4TestValFsipv6SecPolicyFlag)
{

    tSecv6Policy       *pSecPolicyIf = NULL;

    pSecPolicyIf = Secv6LowPolicyGetEntry ((UINT4) i4Fsipv6SecPolicyIndex);

    if (pSecPolicyIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsipv6SecPolicyFlag != SEC_APPLY)
        && (i4TestValFsipv6SecPolicyFlag != SEC_BYPASS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SecPolicyMode
 Input       :  The Indices
                Fsipv6SecPolicyIndex

                The Object 
                testValFsipv6SecPolicyMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SecPolicyMode (UINT4 *pu4ErrorCode, INT4 i4Fsipv6SecPolicyIndex,
                              INT4 i4TestValFsipv6SecPolicyMode)
{

    tSecv6Policy       *pSecPolicyIf = NULL;

    pSecPolicyIf = Secv6LowPolicyGetEntry ((UINT4) i4Fsipv6SecPolicyIndex);

    if (pSecPolicyIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsipv6SecPolicyMode != SEC_MANUAL)
        && (i4TestValFsipv6SecPolicyMode != SEC_AUTOMATIC))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SecPolicySaBundle
 Input       :  The Indices
                Fsipv6SecPolicyIndex

                The Object 
                testValFsipv6SecPolicySaBundle
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SecPolicySaBundle (UINT4 *pu4ErrorCode,
                                  INT4 i4Fsipv6SecPolicyIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValFsipv6SecPolicySaBundle)
{
    tSecv6Policy       *pSecPolicyIf = NULL;
    INT4                i4BackCount = SEC_BACK_COUNT, i4Num, i4Tens = SEC_TENS;

    pSecPolicyIf = Secv6LowPolicyGetEntry ((UINT4) i4Fsipv6SecPolicyIndex);

    if (pSecPolicyIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((pTestValFsipv6SecPolicySaBundle->i4_Length < SEC_MIN_SA_BUNDLE_LEN)
        || (pTestValFsipv6SecPolicySaBundle->i4_Length >=
            SEC_MAX_SA_BUNDLE_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return (SNMP_FAILURE);
    }

    do
    {
        i4BackCount++;
        i4Num = 0;
        while ((pTestValFsipv6SecPolicySaBundle->pu1_OctetList[i4BackCount] !=
                '.')
               && (pTestValFsipv6SecPolicySaBundle->
                   pu1_OctetList[i4BackCount] != '\0'))
        {
            i4Num = i4Num * i4Tens +
                (pTestValFsipv6SecPolicySaBundle->pu1_OctetList[i4BackCount] -
                 '0');
            i4BackCount++;
        }

        if (Secv6LowAssocGetEntry ((UINT4) i4Num) == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
            return (SNMP_FAILURE);
        }
    }
    while (i4BackCount < pTestValFsipv6SecPolicySaBundle->i4_Length);

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SecPolicyOptionsIndex
 Input       :  The Indices
                Fsipv6SecPolicyIndex

                The Object 
                testValFsipv6SecPolicyOptionsIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SecPolicyOptionsIndex (UINT4 *pu4ErrorCode,
                                      INT4 i4Fsipv6SecPolicyIndex,
                                      INT4 i4TestValFsipv6SecPolicyOptionsIndex)
{
    tSecv6Policy       *pSecPolicyIf = NULL;

    /* reserved for IKE */
    pSecPolicyIf = Secv6LowPolicyGetEntry ((UINT4) i4Fsipv6SecPolicyIndex);

    if (pSecPolicyIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValFsipv6SecPolicyOptionsIndex < SEC_MIN_INTEGER)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SecPolicyStatus
 Input       :  The Indices
                Fsipv6SecPolicyIndex

                The Object 
                testValFsipv6SecPolicyStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SecPolicyStatus (UINT4 *pu4ErrorCode,
                                INT4 i4Fsipv6SecPolicyIndex,
                                INT4 i4TestValFsipv6SecPolicyStatus)
{

    tSecv6Policy       *pSecPolicyIf = NULL;

    if (i4Fsipv6SecPolicyIndex < SEC_MIN_INTEGER)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    pSecPolicyIf = Secv6LowPolicyGetEntry ((UINT4) i4Fsipv6SecPolicyIndex);

    if (gSecv6Status == SEC_ENABLE)
    {
        *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
        return (SNMP_FAILURE);
    }
    if ((i4TestValFsipv6SecPolicyStatus != CREATE_AND_GO)
        && (i4TestValFsipv6SecPolicyStatus != CREATE_AND_WAIT)
        && (i4TestValFsipv6SecPolicyStatus != DESTROY)
        && (i4TestValFsipv6SecPolicyStatus != ACTIVE)
        && (i4TestValFsipv6SecPolicyStatus != NOT_IN_SERVICE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsipv6SecPolicyStatus == CREATE_AND_GO)
    {

        if (pSecPolicyIf != NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (SNMP_FAILURE);
        }
    }

    if (i4TestValFsipv6SecPolicyStatus == DESTROY)
    {
        if (pSecPolicyIf == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;

            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsipv6SecPolicyTable
 Input       :  The Indices
                Fsipv6SecPolicyIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6SecPolicyTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv6SecAssocTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6SecAssocTable
 Input       :  The Indices
                Fsipv6SecAssocIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsipv6SecAssocTable (INT4 i4Fsipv6SecAssocIndex)
{

    if (i4Fsipv6SecAssocIndex < SEC_MIN_INTEGER)
    {
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6SecAssocTable
 Input       :  The Indices
                Fsipv6SecAssocIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv6SecAssocTable (INT4 *pi4Fsipv6SecAssocIndex)
{

    tSecv6Assoc        *pSecAssocIf = NULL;
    UINT4               u4FirstIndex = SEC_MAX_INTEGER;
    INT4                Flag = SEC_NOT_FOUND;

    TMO_SLL_Scan (&Secv6AssocList, pSecAssocIf, tSecv6Assoc *)
    {
        if (pSecAssocIf->u4SecAssocIndex < (UINT4) u4FirstIndex)
        {
            Flag = SEC_FOUND;
            u4FirstIndex = pSecAssocIf->u4SecAssocIndex;
        }
    }
    if (!Flag)
    {
        return SNMP_FAILURE;
    }
    *pi4Fsipv6SecAssocIndex = (INT4) u4FirstIndex;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6SecAssocTable
 Input       :  The Indices
                Fsipv6SecAssocIndex
                nextFsipv6SecAssocIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6SecAssocTable (INT4 i4Fsipv6SecAssocIndex,
                                    INT4 *pi4NextFsipv6SecAssocIndex)
{

    tSecv6Assoc        *pSecAssocIf = NULL;
    INT4                Flag = SEC_NOT_FOUND;

    TMO_SLL_Scan (&Secv6AssocList, pSecAssocIf, tSecv6Assoc *)
    {

        if (pSecAssocIf->u4SecAssocIndex > (UINT4) i4Fsipv6SecAssocIndex)
        {
            if (Flag == SEC_NOT_FOUND)
            {
                *pi4NextFsipv6SecAssocIndex =
                    (INT4) pSecAssocIf->u4SecAssocIndex;
                Flag = SEC_FOUND;
            }
            else
            {

                if (pSecAssocIf->u4SecAssocIndex <
                    (UINT4) *pi4NextFsipv6SecAssocIndex)
                {
                    *pi4NextFsipv6SecAssocIndex =
                        (INT4) pSecAssocIf->u4SecAssocIndex;
                }
            }
        }
    }
    if (Flag == SEC_NOT_FOUND)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6SecAssocDstAddr
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object 
                retValFsipv6SecAssocDstAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecAssocDstAddr (INT4 i4Fsipv6SecAssocIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsipv6SecAssocDstAddr)
{

    tSecv6Assoc        *pSecAssocIf = NULL;

    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);

    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    Ip6AddrCopy ((tIp6Addr *) (VOID *) pRetValFsipv6SecAssocDstAddr->
                 pu1_OctetList, &pSecAssocIf->SecAssocDestAddr);
    pRetValFsipv6SecAssocDstAddr->i4_Length = SEC_ADDR_LEN;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsipv6SecAssocProtocol
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object 
                retValFsipv6SecAssocProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecAssocProtocol (INT4 i4Fsipv6SecAssocIndex,
                              INT4 *pi4RetValFsipv6SecAssocProtocol)
{

    tSecv6Assoc        *pSecAssocIf = NULL;

    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);

    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValFsipv6SecAssocProtocol = (INT4) pSecAssocIf->u1SecAssocProtocol;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsipv6SecAssocSpi
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object 
                retValFsipv6SecAssocSpi
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecAssocSpi (INT4 i4Fsipv6SecAssocIndex,
                         INT4 *pi4RetValFsipv6SecAssocSpi)
{

    tSecv6Assoc        *pSecAssocIf = NULL;

    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);

    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValFsipv6SecAssocSpi = (INT4) pSecAssocIf->u4SecAssocSpi;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsipv6SecAssocMode
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object 
                retValFsipv6SecAssocMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecAssocMode (INT4 i4Fsipv6SecAssocIndex,
                          INT4 *pi4RetValFsipv6SecAssocMode)
{

    tSecv6Assoc        *pSecAssocIf = NULL;

    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);

    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValFsipv6SecAssocMode = (INT4) pSecAssocIf->u1SecAssocMode;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsipv6SecAssocAhAlgo
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object 
                retValFsipv6SecAssocAhAlgo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecAssocAhAlgo (INT4 i4Fsipv6SecAssocIndex,
                            INT4 *pi4RetValFsipv6SecAssocAhAlgo)
{

    tSecv6Assoc        *pSecAssocIf = NULL;

    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);

    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValFsipv6SecAssocAhAlgo = (INT4) pSecAssocIf->u1SecAssocAhAlgo;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsipv6SecAssocAhKey
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object
                retValFsipv6SecAssocAhKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecAssocAhKey (INT4 i4Fsipv6SecAssocIndex,
                           tSNMP_OCTET_STRING_TYPE * pRetValFsipv6SecAssocAhKey)
{
    tSecv6Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pSecAssocIf->pu1SecAssocAhKey != NULL)
    {
        IPSEC_MEMCPY (pRetValFsipv6SecAssocAhKey->
                      pu1_OctetList, pSecAssocIf->pu1SecAssocAhKey,
                      pSecAssocIf->u1SecAssocAhKeyLength);
        pRetValFsipv6SecAssocAhKey->
            i4_Length = pSecAssocIf->u1SecAssocAhKeyLength;
    }
    else
    {
        pRetValFsipv6SecAssocAhKey->i4_Length = 0;
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv6SecAssocEspAlgo
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object 
                retValFsipv6SecAssocEspAlgo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecAssocEspAlgo (INT4 i4Fsipv6SecAssocIndex,
                             INT4 *pi4RetValFsipv6SecAssocEspAlgo)
{

    tSecv6Assoc        *pSecAssocIf = NULL;

    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);

    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValFsipv6SecAssocEspAlgo = (INT4) pSecAssocIf->u1SecAssocEspAlgo;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsipv6SecAssocEspKey
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object
                retValFsipv6SecAssocEspKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecAssocEspKey (INT4 i4Fsipv6SecAssocIndex, tSNMP_OCTET_STRING_TYPE
                            * pRetValFsipv6SecAssocEspKey)
{
    tSecv6Assoc        *pSecAssocIf = NULL;

    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pSecAssocIf->pu1SecAssocEspKey != NULL)
    {
        IPSEC_MEMCPY (pRetValFsipv6SecAssocEspKey->
                      pu1_OctetList, pSecAssocIf->pu1SecAssocEspKey,
                      pSecAssocIf->u1SecAssocEspKeyLength);
        pRetValFsipv6SecAssocEspKey->
            i4_Length = pSecAssocIf->u1SecAssocEspKeyLength;
    }
    else
    {
        pRetValFsipv6SecAssocEspKey->i4_Length = 0;
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv6SecAssocEspKey2
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object
                retValFsipv6SecAssocEspKey2
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecAssocEspKey2 (INT4 i4Fsipv6SecAssocIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsipv6SecAssocEspKey2)
{

    tSecv6Assoc        *pSecAssocIf = NULL;

    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pSecAssocIf->pu1SecAssocEspKey2 != NULL)
    {
        IPSEC_MEMCPY (pRetValFsipv6SecAssocEspKey2->
                      pu1_OctetList, pSecAssocIf->pu1SecAssocEspKey2,
                      pSecAssocIf->u1SecAssocEspKey2Length);
        pRetValFsipv6SecAssocEspKey2->
            i4_Length = pSecAssocIf->u1SecAssocEspKey2Length;
    }
    else
    {
        pRetValFsipv6SecAssocEspKey2->i4_Length = 0;
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsipv6SecAssocEspKey3
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object
                retValFsipv6SecAssocEspKey3
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecAssocEspKey3 (INT4 i4Fsipv6SecAssocIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsipv6SecAssocEspKey3)
{
    tSecv6Assoc        *pSecAssocIf = NULL;

    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pSecAssocIf->pu1SecAssocEspKey3 != NULL)
    {
        IPSEC_MEMCPY (pRetValFsipv6SecAssocEspKey3->
                      pu1_OctetList, pSecAssocIf->pu1SecAssocEspKey3,
                      pSecAssocIf->u1SecAssocEspKey3Length);
        pRetValFsipv6SecAssocEspKey3->
            i4_Length = pSecAssocIf->u1SecAssocEspKey3Length;
    }
    else
    {
        pRetValFsipv6SecAssocEspKey3->i4_Length = 0;
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsipv6SecAssocLifetimeInBytes
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object 
                retValFsipv6SecAssocLifetimeInBytes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecAssocLifetimeInBytes (INT4 i4Fsipv6SecAssocIndex,
                                     INT4
                                     *pi4RetValFsipv6SecAssocLifetimeInBytes)
{

    tSecv6Assoc        *pSecAssocIf = NULL;

    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);

    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValFsipv6SecAssocLifetimeInBytes =
        (INT4) pSecAssocIf->u4LifeTimeInBytes;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsipv6SecAssocLifetime
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object 
                retValFsipv6SecAssocLifetime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecAssocLifetime (INT4 i4Fsipv6SecAssocIndex,
                              INT4 *pi4RetValFsipv6SecAssocLifetime)
{

    tSecv6Assoc        *pSecAssocIf = NULL;

    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);

    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValFsipv6SecAssocLifetime = (INT4) pSecAssocIf->u4LifeTime;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsipv6SecAssocAntiReplay
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object 
                retValFsipv6SecAssocAntiReplay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecAssocAntiReplay (INT4 i4Fsipv6SecAssocIndex,
                                INT4 *pi4RetValFsipv6SecAssocAntiReplay)
{

    tSecv6Assoc        *pSecAssocIf = NULL;

    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValFsipv6SecAssocAntiReplay = (INT4) pSecAssocIf->u1AntiReplayStatus;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsipv6SecAssocStatus
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object 
                retValFsipv6SecAssocStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecAssocStatus (INT4 i4Fsipv6SecAssocIndex,
                            INT4 *pi4RetValFsipv6SecAssocStatus)
{

    tSecv6Assoc        *pSecAssocIf = NULL;

    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);

    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValFsipv6SecAssocStatus = (INT4) pSecAssocIf->u4AssocStatus;

    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv6SecAssocDstAddr
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object 
                setValFsipv6SecAssocDstAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SecAssocDstAddr (INT4 i4Fsipv6SecAssocIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsipv6SecAssocDstAddr)
{

    tSecv6Assoc        *pSecAssocIf = NULL;

    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    Ip6AddrCopy (&pSecAssocIf->SecAssocDestAddr,
                 (tIp6Addr *) (VOID *) pSetValFsipv6SecAssocDstAddr->
                 pu1_OctetList);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsipv6SecAssocProtocol
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object 
                setValFsipv6SecAssocProtocol
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SecAssocProtocol (INT4 i4Fsipv6SecAssocIndex,
                              INT4 i4SetValFsipv6SecAssocProtocol)
{

    tSecv6Assoc        *pSecAssocIf = NULL;

    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    pSecAssocIf->u1SecAssocProtocol = (UINT1) i4SetValFsipv6SecAssocProtocol;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsipv6SecAssocSpi
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object 
                setValFsipv6SecAssocSpi
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SecAssocSpi (INT4 i4Fsipv6SecAssocIndex,
                         INT4 i4SetValFsipv6SecAssocSpi)
{

    tSecv6Assoc        *pSecAssocIf = NULL;

    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    pSecAssocIf->u4SecAssocSpi = (UINT4) i4SetValFsipv6SecAssocSpi;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsipv6SecAssocMode
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object 
                setValFsipv6SecAssocMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SecAssocMode (INT4 i4Fsipv6SecAssocIndex,
                          INT4 i4SetValFsipv6SecAssocMode)
{

    tSecv6Assoc        *pSecAssocIf = NULL;

    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    pSecAssocIf->u1SecAssocMode = (UINT1) i4SetValFsipv6SecAssocMode;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsipv6SecAssocAhAlgo
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object 
                setValFsipv6SecAssocAhAlgo
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SecAssocAhAlgo (INT4 i4Fsipv6SecAssocIndex,
                            INT4 i4SetValFsipv6SecAssocAhAlgo)
{

    tSecv6Assoc        *pSecAssocIf = NULL;

    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    pSecAssocIf->u1SecAssocAhAlgo = (UINT1) i4SetValFsipv6SecAssocAhAlgo;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsipv6SecAssocAhKey
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object
                setValFsipv6SecAssocAhKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SecAssocAhKey (INT4
                           i4Fsipv6SecAssocIndex,
                           tSNMP_OCTET_STRING_TYPE * pSetValFsipv6SecAssocAhKey)
{
    tSecv6Assoc        *pSecAssocIf = NULL;
    UINT1               Count = 0;
#ifdef SECv6_PRIVATE_KEY
    UINT1               Temp = SEC_KEY_CHANGE;
#endif
    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (pSecAssocIf->pu1SecAssocAhKey == NULL)
    {
        if (MemAllocateMemBlock
            (SECv6_SAD_AH_MEMPOOL,
             (UINT1 **) &pSecAssocIf->pu1SecAssocAhKey) != MEM_SUCCESS)
        {
            return (SNMP_FAILURE);
        }
    }
    IPSEC_MEMSET (pSecAssocIf->pu1SecAssocAhKey, 0, (SEC_HMAC_SHA_MAX_LEN + 1));

    IPSEC_MEMCPY (pSecAssocIf->pu1SecAssocAhKey,
                  pSetValFsipv6SecAssocAhKey->pu1_OctetList,
                  pSetValFsipv6SecAssocAhKey->i4_Length);

    pSecAssocIf->u1SecAssocAhKeyLength =
        (UINT1) pSetValFsipv6SecAssocAhKey->i4_Length;

    if (pSecAssocIf->pu1SecAssocAhKey1 == NULL)
    {
        if (MemAllocateMemBlock
            (SECv6_SAD_AH_MEMPOOL,
             (UINT1 **) &pSecAssocIf->pu1SecAssocAhKey1) != MEM_SUCCESS)
        {
            return (SNMP_FAILURE);
        }
    }

    for (Count = 0; Count < pSetValFsipv6SecAssocAhKey->i4_Length; Count++)
    {
#ifdef SECv6_PRIVATE_KEY
        pSecAssocIf->
            pu1SecAssocAhKey1[Count] =
            pSetValFsipv6SecAssocAhKey->pu1_OctetList[Count] ^ Temp;
        Temp = pSecAssocIf->pu1SecAssocAhKey1[Count];
#else
        pSecAssocIf->
            pu1SecAssocAhKey1[Count] =
            pSetValFsipv6SecAssocAhKey->pu1_OctetList[Count];
#endif
    }
    pSecAssocIf->pu1SecAssocAhKey1[Count] = '\0';
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv6SecAssocEspAlgo
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object 
                setValFsipv6SecAssocEspAlgo
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SecAssocEspAlgo (INT4 i4Fsipv6SecAssocIndex,
                             INT4 i4SetValFsipv6SecAssocEspAlgo)
{

    tSecv6Assoc        *pSecAssocIf = NULL;

    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    pSecAssocIf->u1SecAssocEspAlgo = (UINT1) i4SetValFsipv6SecAssocEspAlgo;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsipv6SecAssocEspKey
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object
                setValFsipv6SecAssocEspKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SecAssocEspKey (INT4 i4Fsipv6SecAssocIndex,
                            tSNMP_OCTET_STRING_TYPE
                            * pSetValFsipv6SecAssocEspKey)
{
    tSecv6Assoc        *pSecAssocIf = NULL;
    unArCryptoKey       ArCryptoKey;

    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);
    if (pSecAssocIf != NULL)
    {
        if (pSecAssocIf->pu1SecAssocEspKey == NULL)
        {
            if (MemAllocateMemBlock
                (SECv6_SAD_ESP_MEMPOOL,
                 (UINT1 **) &pSecAssocIf->pu1SecAssocEspKey) != MEM_SUCCESS)
            {
                return (SNMP_FAILURE);
            }
        }
        IPSEC_MEMSET (pSecAssocIf->pu1SecAssocEspKey, 0,
                      (SEC_ESP_AES_KEY3_LEN + 1));
        IPSEC_MEMCPY (pSecAssocIf->pu1SecAssocEspKey,
                      pSetValFsipv6SecAssocEspKey->pu1_OctetList,
                      pSetValFsipv6SecAssocEspKey->i4_Length);

        pSecAssocIf->u1SecAssocEspKeyLength =
            (UINT1) pSetValFsipv6SecAssocEspKey->i4_Length;

        if ((pSecAssocIf->u1SecAssocEspAlgo == SEC_DES_CBC) ||
            (pSecAssocIf->u1SecAssocEspAlgo == SEC_3DES_CBC))
        {
            if (pSecAssocIf->pu1SecAssocInitVector == NULL)
            {
                if (MemAllocateMemBlock (SECv6_SAD_ESP_MEMPOOL, (UINT1 **)
                                         &pSecAssocIf->pu1SecAssocInitVector) !=
                    MEM_SUCCESS)
                {
                    return (SNMP_FAILURE);
                }
            }
            IPSEC_STRCPY (pSecAssocIf->pu1SecAssocInitVector, "INITVECT");
            DesArSubKeys (pSecAssocIf->au8SubKey,
                          pSecAssocIf->pu1SecAssocEspKey);
        }
        else if ((pSecAssocIf->u1SecAssocEspAlgo == SEC_AES)
                 || (pSecAssocIf->u1SecAssocEspAlgo == SEC_AESCTR)
                 || (pSecAssocIf->u1SecAssocEspAlgo == SEC_AESCTR192)
                 || (pSecAssocIf->u1SecAssocEspAlgo == SEC_AESCTR256))
        {
            if (pSecAssocIf->pu1SecAssocInitVector == NULL)
            {
                if (MemAllocateMemBlock (SECv6_SAD_ESP_MEMPOOL, (UINT1 **)
                                         &pSecAssocIf->pu1SecAssocInitVector) !=
                    MEM_SUCCESS)
                {
                    return (SNMP_FAILURE);
                }
            }
            if (pSecAssocIf->u1SecAssocEspAlgo == SEC_AES)
            {
                IPSEC_STRCPY (pSecAssocIf->pu1SecAssocInitVector,
                              "AESCBCINITVECTOR");
            }
            if ((pSecAssocIf->u1SecAssocEspAlgo == SEC_AESCTR)
                || (pSecAssocIf->u1SecAssocEspAlgo == SEC_AESCTR192)
                || (pSecAssocIf->u1SecAssocEspAlgo == SEC_AESCTR256))
            {
                FsUtlGetUniqueRandom (pSecAssocIf->pu1SecAssocInitVector,
                                      IPSEC_AES_CTR_INIT_VECT_SIZE);
            }
            IPSEC_MEMSET (ArCryptoKey.tArAes.au1ArSubkey, 0,
                          sizeof (ArCryptoKey.tArAes.au1ArSubkey));
            IPSEC_MEMCPY (ArCryptoKey.tArAes.au1ArSubkey,
                          pSecAssocIf->au1AesEncrKey,
                          sizeof (ArCryptoKey.tArAes.au1ArSubkey));
            AesArSetEncryptKey (pSecAssocIf->pu1SecAssocEspKey,
                                (UINT2) (pSecAssocIf->u1SecAssocEspKeyLength *
                                         IPSEC_AES_KEY_CONV_FACTOR),
                                &ArCryptoKey);
            IPSEC_MEMCPY (pSecAssocIf->au1AesEncrKey,
                          ArCryptoKey.tArAes.au1ArSubkey,
                          sizeof (ArCryptoKey.tArAes.au1ArSubkey));
            AesArSetDecryptKey (pSecAssocIf->pu1SecAssocEspKey,
                                (UINT2) (pSecAssocIf->u1SecAssocEspKeyLength *
                                         IPSEC_AES_KEY_CONV_FACTOR),
                                &ArCryptoKey);
            IPSEC_MEMCPY (pSecAssocIf->au1AesDecrKey,
                          ArCryptoKey.tArAes.au1ArSubkey,
                          sizeof (ArCryptoKey.tArAes.au1ArSubkey));
        }

    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv6SecAssocEspKey2
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object
                setValFsipv6SecAssocEspKey2
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SecAssocEspKey2 (INT4 i4Fsipv6SecAssocIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsipv6SecAssocEspKey2)
{

    tSecv6Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);
    if (pSecAssocIf != NULL)
    {
        if (pSecAssocIf->pu1SecAssocEspKey2 == NULL)
        {
            if (MemAllocateMemBlock
                (SECv6_SAD_ESP_MEMPOOL,
                 (UINT1 **) &pSecAssocIf->pu1SecAssocEspKey2) != MEM_SUCCESS)
            {
                return (SNMP_FAILURE);
            }
        }
        IPSEC_MEMSET (pSecAssocIf->pu1SecAssocEspKey2, 0,
                      (SEC_ESP_AES_KEY3_LEN + 1));
        IPSEC_MEMCPY (pSecAssocIf->pu1SecAssocEspKey2,
                      pSetValFsipv6SecAssocEspKey2->pu1_OctetList,
                      pSetValFsipv6SecAssocEspKey2->i4_Length);

        pSecAssocIf->u1SecAssocEspKey2Length =
            (UINT1) pSetValFsipv6SecAssocEspKey2->i4_Length;

        DesArSubKeys (pSecAssocIf->au8SubKey2, pSecAssocIf->pu1SecAssocEspKey2);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsipv6SecAssocEspKey3
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object
                setValFsipv6SecAssocEspKey3
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SecAssocEspKey3 (INT4 i4Fsipv6SecAssocIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsipv6SecAssocEspKey3)
{
    tSecv6Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);
    if (pSecAssocIf != NULL)
    {
        if (pSecAssocIf->pu1SecAssocEspKey3 == NULL)
        {
            if (MemAllocateMemBlock
                (SECv6_SAD_ESP_MEMPOOL,
                 (UINT1 **) &pSecAssocIf->pu1SecAssocEspKey3) != MEM_SUCCESS)
            {
                return (SNMP_FAILURE);
            }
        }
        IPSEC_MEMSET (pSecAssocIf->pu1SecAssocEspKey3, 0,
                      (SEC_ESP_AES_KEY3_LEN + 1));
        IPSEC_MEMCPY (pSecAssocIf->pu1SecAssocEspKey3,
                      pSetValFsipv6SecAssocEspKey3->pu1_OctetList,
                      pSetValFsipv6SecAssocEspKey3->i4_Length);

        pSecAssocIf->u1SecAssocEspKey3Length =
            (UINT1) pSetValFsipv6SecAssocEspKey3->i4_Length;
        DesArSubKeys (pSecAssocIf->au8SubKey3, pSecAssocIf->pu1SecAssocEspKey3);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsipv6SecAssocLifetimeInBytes
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object 
                setValFsipv6SecAssocLifetimeInBytes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SecAssocLifetimeInBytes (INT4
                                     i4Fsipv6SecAssocIndex,
                                     INT4 i4SetValFsipv6SecAssocLifetimeInBytes)
{

    tSecv6Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    pSecAssocIf->u4LifeTimeInBytes =
        (UINT4) i4SetValFsipv6SecAssocLifetimeInBytes;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv6SecAssocLifetime
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object 
                setValFsipv6SecAssocLifetime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SecAssocLifetime (INT4 i4Fsipv6SecAssocIndex,
                              INT4 i4SetValFsipv6SecAssocLifetime)
{

    tSecv6Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    pSecAssocIf->u4LifeTime = (UINT4) i4SetValFsipv6SecAssocLifetime;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv6SecAssocAntiReplay
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object 
                setValFsipv6SecAssocAntiReplay
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SecAssocAntiReplay (INT4 i4Fsipv6SecAssocIndex,
                                INT4 i4SetValFsipv6SecAssocAntiReplay)
{

    tSecv6Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    pSecAssocIf->u1AntiReplayStatus = (UINT1) i4SetValFsipv6SecAssocAntiReplay;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsipv6SecAssocStatus
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object 
                setValFsipv6SecAssocStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SecAssocStatus (INT4 i4Fsipv6SecAssocIndex,
                            INT4 i4SetValFsipv6SecAssocStatus)
{
    tSecv6Assoc        *pSecAssocIf = NULL;
    if (i4SetValFsipv6SecAssocStatus == CREATE_AND_GO)
    {

        if (MemAllocateMemBlock
            (SECv6_SAD_MEMPOOL, (UINT1 **) &pSecAssocIf) != MEM_SUCCESS)
        {
            SECv6_TRC (SECv6_OS_RESOURCE,
                       "nmhSetFsipv6SecAssocStatus : Unable to allocate "
                       "memory from pool for SAD Table \n");
            return (SNMP_FAILURE);
        }

        IPSEC_MEMSET (pSecAssocIf, 0, sizeof (tSecv6Assoc));
        pSecAssocIf->u1SecAssocSeqCounterFlag = 32;
        pSecAssocIf->u4SecAssocIndex = (UINT4) i4Fsipv6SecAssocIndex;
        pSecAssocIf->u4AssocStatus = ACTIVE;
        TMO_SLL_Add (&Secv6AssocList, &pSecAssocIf->NextSecv6AssocIf);
    }
    if (i4SetValFsipv6SecAssocStatus == CREATE_AND_WAIT)
    {

        if (MemAllocateMemBlock
            (SECv6_SAD_MEMPOOL, (UINT1 **) &pSecAssocIf) != MEM_SUCCESS)
        {
            SECv6_TRC (SECv6_OS_RESOURCE,
                       "nmhSetFsipv6SecAssocStatus : Unable to allocate "
                       "memory from pool for SAD Table \n");
            return (SNMP_FAILURE);
        }

        IPSEC_MEMSET (pSecAssocIf, 0, sizeof (tSecv6Assoc));
        pSecAssocIf->u1SecAssocSeqCounterFlag = 32;
        pSecAssocIf->u4SecAssocIndex = (UINT4) i4Fsipv6SecAssocIndex;
        pSecAssocIf->u4AssocStatus = NOT_IN_SERVICE;
        TMO_SLL_Add (&Secv6AssocList, &pSecAssocIf->NextSecv6AssocIf);
    }
    if (i4SetValFsipv6SecAssocStatus == ACTIVE)
    {
        pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);
        if (pSecAssocIf == NULL)
        {
            return (SNMP_FAILURE);
        }
        pSecAssocIf->u4AssocStatus = ACTIVE;
    }
    if (i4SetValFsipv6SecAssocStatus == NOT_IN_SERVICE)
    {
        pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);
        if (pSecAssocIf == NULL)
        {
            return (SNMP_FAILURE);
        }
        pSecAssocIf->u4AssocStatus = NOT_IN_SERVICE;
    }

    if (i4SetValFsipv6SecAssocStatus == DESTROY)
    {
        return (Secv6DestroySecAssoc (i4Fsipv6SecAssocIndex));
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SecAssocDstAddr
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object 
                testValFsipv6SecAssocDstAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SecAssocDstAddr (UINT4 *pu4ErrorCode,
                                INT4 i4Fsipv6SecAssocIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsipv6SecAssocDstAddr)
{
    tSecv6Assoc        *pSecAssocIf = NULL;
    INT4                i4Type;
    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
    if (pTestValFsipv6SecAssocDstAddr->i4_Length > IP6_ADDR_SIZE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return (SNMP_FAILURE);
    }

    i4Type = Ip6AddrType ((tIp6Addr *) (VOID *)
                          pTestValFsipv6SecAssocDstAddr->pu1_OctetList);

    if ((i4Type != IPSEC_ADDR_UNICAST) && (i4Type != ADDR6_LLOCAL))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SecAssocProtocol
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object 
                testValFsipv6SecAssocProtocol
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SecAssocProtocol (UINT4 *pu4ErrorCode,
                                 INT4 i4Fsipv6SecAssocIndex,
                                 INT4 i4TestValFsipv6SecAssocProtocol)
{

    tSecv6Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsipv6SecAssocProtocol != SEC_AH)
        && (i4TestValFsipv6SecAssocProtocol != SEC_ESP))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SecAssocSpi
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object 
                testValFsipv6SecAssocSpi
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SecAssocSpi (UINT4 *pu4ErrorCode,
                            INT4 i4Fsipv6SecAssocIndex,
                            INT4 i4TestValFsipv6SecAssocSpi)
{
    tSecv6Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValFsipv6SecAssocSpi <= SEC_MIN_SPI)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SecAssocMode
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object 
                testValFsipv6SecAssocMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SecAssocMode (UINT4 *pu4ErrorCode,
                             INT4 i4Fsipv6SecAssocIndex,
                             INT4 i4TestValFsipv6SecAssocMode)
{
    tSecv6Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsipv6SecAssocMode != SEC_TRANSPORT)
        && (i4TestValFsipv6SecAssocMode != SEC_TUNNEL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SecAssocAhAlgo
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object 
                testValFsipv6SecAssocAhAlgo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SecAssocAhAlgo (UINT4 *pu4ErrorCode,
                               INT4 i4Fsipv6SecAssocIndex,
                               INT4 i4TestValFsipv6SecAssocAhAlgo)
{
    tSecv6Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (!
        (((i4TestValFsipv6SecAssocAhAlgo >= SEC_NULLAHALGO)
          && (i4TestValFsipv6SecAssocAhAlgo <= SEC_XCBCMAC))
         || ((i4TestValFsipv6SecAssocAhAlgo >= HMAC_SHA_256)
             && (i4TestValFsipv6SecAssocAhAlgo <= HMAC_SHA_512))))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
#ifndef SECURITY_KERNEL_WANTED
    if (FIPS_MODE == FipsGetFipsCurrOperMode ())
    {
        if ((i4TestValFsipv6SecAssocAhAlgo == SEC_HMACMD5)
            || (i4TestValFsipv6SecAssocAhAlgo == SEC_MD5)
            || (i4TestValFsipv6SecAssocAhAlgo == SEC_KEYEDMD5))
        {
            return (SNMP_FAILURE);
        }
    }
#endif
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SecAssocAhKey
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object 
                testValFsipv6SecAssocAhKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SecAssocAhKey (UINT4 *pu4ErrorCode,
                              INT4 i4Fsipv6SecAssocIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pTestValFsipv6SecAssocAhKey)
{
    tSecv6Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    switch (pSecAssocIf->u1SecAssocAhAlgo)
    {
        case SEC_XCBCMAC:
        case SEC_HMACMD5:
        case SEC_KEYEDMD5:
            if (pTestValFsipv6SecAssocAhKey->i4_Length != SEC_HMACMD5_KEY_LEN)
            {

                pSecAssocIf->u1SecAssocAhAlgo = SEC_NULLAHALGO;
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return (SNMP_FAILURE);
            }
            break;
        case SEC_HMACSHA1:
            if (pTestValFsipv6SecAssocAhKey->i4_Length != SEC_HMACSHA1_KEY_LEN)
            {

                pSecAssocIf->u1SecAssocAhAlgo = SEC_NULLAHALGO;
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return (SNMP_FAILURE);
            }
            break;
        case HMAC_SHA_256:
            if (pTestValFsipv6SecAssocAhKey->i4_Length != SHA256_HASH_SIZE)
            {
                pSecAssocIf->u1SecAssocAhAlgo = SEC_NULLAHALGO;
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return (SNMP_FAILURE);
            }
            break;
        case HMAC_SHA_384:
            if (pTestValFsipv6SecAssocAhKey->i4_Length != SHA384_HASH_SIZE)
            {
                pSecAssocIf->u1SecAssocAhAlgo = SEC_NULLAHALGO;
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return (SNMP_FAILURE);
            }
            break;
        case HMAC_SHA_512:
            if (pTestValFsipv6SecAssocAhKey->i4_Length != SHA512_HASH_SIZE)
            {
                pSecAssocIf->u1SecAssocAhAlgo = SEC_NULLAHALGO;
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return (SNMP_FAILURE);
            }
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (SNMP_FAILURE);

    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SecAssocEspAlgo
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object 
                testValFsipv6SecAssocEspAlgo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SecAssocEspAlgo (UINT4 *pu4ErrorCode,
                                INT4 i4Fsipv6SecAssocIndex,
                                INT4 i4TestValFsipv6SecAssocEspAlgo)
{
    tSecv6Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
    if (pSecAssocIf->u1SecAssocProtocol == SEC_AH)
    {
        *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsipv6SecAssocEspAlgo != SEC_DES_CBC)
        && (i4TestValFsipv6SecAssocEspAlgo != SEC_3DES_CBC)
        && (i4TestValFsipv6SecAssocEspAlgo != SEC_AES)
        && (i4TestValFsipv6SecAssocEspAlgo != SEC_AESCTR)
        && (i4TestValFsipv6SecAssocEspAlgo != SEC_AESCTR192)
        && (i4TestValFsipv6SecAssocEspAlgo != SEC_AESCTR256)
        && (i4TestValFsipv6SecAssocEspAlgo != SEC_NULLESPALGO))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
#ifndef SECURITY_KERNEL_WANTED
    if (FIPS_MODE == FipsGetFipsCurrOperMode ())
    {
        if (i4TestValFsipv6SecAssocEspAlgo == SEC_DES_CBC)
        {
            return (SNMP_FAILURE);
        }
    }
#endif
    if ((i4TestValFsipv6SecAssocEspAlgo == SEC_NULLESPALGO) &&
        (pSecAssocIf->u1SecAssocAhAlgo == SEC_NULLAHALGO))
    {

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SecAssocEspKey
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object 
                testValFsipv6SecAssocEspKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SecAssocEspKey (UINT4 *pu4ErrorCode,
                               INT4 i4Fsipv6SecAssocIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValFsipv6SecAssocEspKey)
{
    tSecv6Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);

    if (pSecAssocIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (pSecAssocIf->u1SecAssocProtocol == SEC_AH)
    {
        *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
        return (SNMP_FAILURE);
    }

    if (pSecAssocIf->u1SecAssocEspAlgo == SEC_NULLESPALGO)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    if (((pSecAssocIf->u1SecAssocEspAlgo == SEC_DES_CBC) ||
         (pSecAssocIf->u1SecAssocEspAlgo == SEC_3DES_CBC)) &&
        (pTestValFsipv6SecAssocEspKey->i4_Length != SEC_ESP_DES_KEY_LEN))
    {

        pSecAssocIf->u1SecAssocEspAlgo = SEC_NULLESPALGO;
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return (SNMP_FAILURE);
    }

    if (((pSecAssocIf->u1SecAssocEspAlgo == SEC_AES) ||
         (pSecAssocIf->u1SecAssocEspAlgo == SEC_AESCTR) ||
         (pSecAssocIf->u1SecAssocEspAlgo == SEC_AESCTR192) ||
         (pSecAssocIf->u1SecAssocEspAlgo == SEC_AESCTR256)) &&
        ((pTestValFsipv6SecAssocEspKey->i4_Length != SEC_ESP_AES_KEY1_LEN) &&
         (pTestValFsipv6SecAssocEspKey->i4_Length != SEC_ESP_AES_KEY2_LEN) &&
         (pTestValFsipv6SecAssocEspKey->i4_Length != SEC_ESP_AES_KEY3_LEN)))
    {
        pSecAssocIf->u1SecAssocEspAlgo = SEC_NULLESPALGO;
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SecAssocEspKey2
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object 
                testValFsipv6SecAssocEspKey2
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SecAssocEspKey2 (UINT4 *pu4ErrorCode,
                                INT4 i4Fsipv6SecAssocIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsipv6SecAssocEspKey2)
{

    tSecv6Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);

    if (pSecAssocIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (pSecAssocIf->u1SecAssocProtocol == SEC_AH)
    {
        *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
        return (SNMP_FAILURE);
    }

    if ((pSecAssocIf->u1SecAssocEspAlgo == SEC_NULLESPALGO) ||
        (pSecAssocIf->u1SecAssocEspAlgo != SEC_3DES_CBC))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    if (pTestValFsipv6SecAssocEspKey2->i4_Length != SEC_ESP_DES_KEY_LEN)
    {

        pSecAssocIf->u1SecAssocEspAlgo = SEC_NULLESPALGO;
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SecAssocEspKey3
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object 
                testValFsipv6SecAssocEspKey3
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SecAssocEspKey3 (UINT4 *pu4ErrorCode,
                                INT4 i4Fsipv6SecAssocIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsipv6SecAssocEspKey3)
{
    tSecv6Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);

    if (pSecAssocIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (pSecAssocIf->u1SecAssocProtocol == SEC_AH)
    {
        *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
        return (SNMP_FAILURE);
    }

    if ((pSecAssocIf->u1SecAssocEspAlgo == SEC_NULLESPALGO) ||
        (pSecAssocIf->u1SecAssocEspAlgo != SEC_3DES_CBC))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    if (pTestValFsipv6SecAssocEspKey3->i4_Length != SEC_ESP_DES_KEY_LEN)
    {

        pSecAssocIf->u1SecAssocEspAlgo = SEC_NULLESPALGO;
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SecAssocLifetimeInBytes
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object 
                testValFsipv6SecAssocLifetimeInBytes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SecAssocLifetimeInBytes (UINT4 *pu4ErrorCode,
                                        INT4
                                        i4Fsipv6SecAssocIndex,
                                        INT4
                                        i4TestValFsipv6SecAssocLifetimeInBytes)
{
    tSecv6Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValFsipv6SecAssocLifetimeInBytes < SEC_MIN_LIFE_BYTES_VALUE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SecAssocLifetime
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object 
                testValFsipv6SecAssocLifetime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SecAssocLifetime (UINT4 *pu4ErrorCode,
                                 INT4 i4Fsipv6SecAssocIndex,
                                 INT4 i4TestValFsipv6SecAssocLifetime)
{
    tSecv6Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
    if (((i4TestValFsipv6SecAssocLifetime < SEC_MIN_LIFE_TIME_VALUE) &&
         (i4TestValFsipv6SecAssocLifetime != SEC_DEFAULT_LIFE_TIME_VALUE))
        || (i4TestValFsipv6SecAssocLifetime > SEC_MAX_LIFE_TIME_VALUE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SecAssocAntiReplay
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object 
                testValFsipv6SecAssocAntiReplay
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SecAssocAntiReplay (UINT4 *pu4ErrorCode,
                                   INT4 i4Fsipv6SecAssocIndex,
                                   INT4 i4TestValFsipv6SecAssocAntiReplay)
{

    tSecv6Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsipv6SecAssocAntiReplay != SEC_ANTI_REPLAY_ENABLE)
        && (i4TestValFsipv6SecAssocAntiReplay != SEC_ANTI_REPLAY_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SecAssocStatus
 Input       :  The Indices
                Fsipv6SecAssocIndex

                The Object 
                testValFsipv6SecAssocStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SecAssocStatus (UINT4 *pu4ErrorCode,
                               INT4 i4Fsipv6SecAssocIndex,
                               INT4 i4TestValFsipv6SecAssocStatus)
{

    tSecv6Assoc        *pSecAssocIf = NULL;
    if (i4Fsipv6SecAssocIndex < SEC_MIN_INTEGER)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);

    if ((i4TestValFsipv6SecAssocStatus != CREATE_AND_GO)
        && (i4TestValFsipv6SecAssocStatus != CREATE_AND_WAIT)
        && (i4TestValFsipv6SecAssocStatus != DESTROY)
        && (i4TestValFsipv6SecAssocStatus != ACTIVE)
        && (i4TestValFsipv6SecAssocStatus != NOT_IN_SERVICE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsipv6SecAssocStatus == DESTROY)
    {
        if (pSecAssocIf == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
    }

    if (i4TestValFsipv6SecAssocStatus == CREATE_AND_GO)
    {
        if (pSecAssocIf != NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (SNMP_FAILURE);
        }
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsipv6SecAssocTable
 Input       :  The Indices
                Fsipv6SecAssocIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6SecAssocTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6SecIfStatsTable
 Input       :  The Indices
                Fsipv6SecIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsipv6SecIfStatsTable (INT4 i4Fsipv6SecIfIndex)
{

    if ((i4Fsipv6SecIfIndex < SEC_MIN_STAT_COUNT) ||
        (i4Fsipv6SecIfIndex > SEC_MAX_STAT_COUNT))
    {

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6SecIfStatsTable
 Input       :  The Indices
                Fsipv6SecIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv6SecIfStatsTable (INT4 *pi4Fsipv6SecIfIndex)
{

    tSecv6Stat         *pSecStatsIf = NULL;
    UINT4               u4FirstIndex = SEC_MAX_INTEGER, u4Count = 0;
    INT4                Flag = SEC_NOT_FOUND;

    for (u4Count = 0; u4Count < SEC_MAX_STAT_COUNT; u4Count++)
    {
        pSecStatsIf = &gatIpsecv6Stat[u4Count];
        if ((pSecStatsIf->u4Index < (UINT4) u4FirstIndex) &&
            (pSecStatsIf->u4Index != 0))
        {
            Flag = SEC_FOUND;
            u4FirstIndex = pSecStatsIf->u4Index;
        }
    }
    if ((Flag == SEC_NOT_FOUND) || (u4FirstIndex < SEC_MIN_STAT_COUNT))
    {
        return SNMP_FAILURE;
    }
    *pi4Fsipv6SecIfIndex = (INT4) u4FirstIndex;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6SecIfStatsTable
 Input       :  The Indices
                Fsipv6SecIfIndex
                nextFsipv6SecIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6SecIfStatsTable (INT4
                                      i4Fsipv6SecIfIndex,
                                      INT4 *pi4NextFsipv6SecIfIndex)
{

    tSecv6Stat         *pSecStatsIf = NULL;
    UINT4               u4Count = 0;
    INT4                Flag = SEC_NOT_FOUND;

    for (u4Count = 0; u4Count < SEC_MAX_STAT_COUNT; u4Count++)
    {

        pSecStatsIf = &gatIpsecv6Stat[u4Count];
        if (pSecStatsIf->u4Index > (UINT4) i4Fsipv6SecIfIndex)
        {
            if (Flag == SEC_NOT_FOUND)
            {
                *pi4NextFsipv6SecIfIndex = (INT4) pSecStatsIf->u4Index;
                Flag = SEC_FOUND;
            }
            else
            {

                if (pSecStatsIf->u4Index < (UINT4) *pi4NextFsipv6SecIfIndex)
                {
                    *pi4NextFsipv6SecIfIndex = (INT4) pSecStatsIf->u4Index;
                }
            }
        }
    }
    if (Flag == SEC_NOT_FOUND)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6SecIfInPkts
 Input       :  The Indices
                Fsipv6SecIfIndex

                The Object 
                retValFsipv6SecIfInPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecIfInPkts (INT4 i4Fsipv6SecIfIndex,
                         UINT4 *pu4RetValFsipv6SecIfInPkts)
{
    *pu4RetValFsipv6SecIfInPkts = gatIpsecv6Stat[i4Fsipv6SecIfIndex].u4IfInPkts;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv6SecIfOutPkts
 Input       :  The Indices
                Fsipv6SecIfIndex

                The Object 
                retValFsipv6SecIfOutPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecIfOutPkts (INT4 i4Fsipv6SecIfIndex,
                          UINT4 *pu4RetValFsipv6SecIfOutPkts)
{

    *pu4RetValFsipv6SecIfOutPkts =
        gatIpsecv6Stat[i4Fsipv6SecIfIndex].u4IfOutPkts;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv6SecIfPktsApply
 Input       :  The Indices
                Fsipv6SecIfIndex

                The Object 
                retValFsipv6SecIfPktsApply
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecIfPktsApply (INT4 i4Fsipv6SecIfIndex,
                            UINT4 *pu4RetValFsipv6SecIfPktsApply)
{

    *pu4RetValFsipv6SecIfPktsApply =
        gatIpsecv6Stat[i4Fsipv6SecIfIndex].u4IfPktsApply;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv6SecIfPktsDiscard
 Input       :  The Indices
                Fsipv6SecIfIndex

                The Object 
                retValFsipv6SecIfPktsDiscard
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecIfPktsDiscard (INT4 i4Fsipv6SecIfIndex,
                              UINT4 *pu4RetValFsipv6SecIfPktsDiscard)
{

    *pu4RetValFsipv6SecIfPktsDiscard =
        gatIpsecv6Stat[i4Fsipv6SecIfIndex].u4IfPktsDiscard;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv6SecIfPktsBypass
 Input       :  The Indices
                Fsipv6SecIfIndex

                The Object 
                retValFsipv6SecIfPktsBypass
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecIfPktsBypass (INT4 i4Fsipv6SecIfIndex,
                             UINT4 *pu4RetValFsipv6SecIfPktsBypass)
{

    *pu4RetValFsipv6SecIfPktsBypass =
        gatIpsecv6Stat[i4Fsipv6SecIfIndex].u4IfPktsBypass;

    return (SNMP_SUCCESS);
}

/* LOW LEVEL Routines for Table : Fsipv6SecAhEspStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6SecAhEspStatsTable
 Input       :  The Indices
                Fsipv6SecAhEspIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsipv6SecAhEspStatsTable (INT4 i4Fsipv6SecAhEspIfIndex)
{

    if ((i4Fsipv6SecAhEspIfIndex < SEC_MIN_STAT_COUNT) ||
        (i4Fsipv6SecAhEspIfIndex > SEC_MAX_STAT_COUNT))
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6SecAhEspStatsTable
 Input       :  The Indices
                Fsipv6SecAhEspIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv6SecAhEspStatsTable (INT4 *pi4Fsipv6SecAhEspIfIndex)
{

    tSecv6AhEspStat    *pSecAhEspStatsIf = NULL;
    UINT4               u4FirstIndex = SEC_MAX_INTEGER, u4Count = 0;
    INT4                Flag = SEC_NOT_FOUND;

    for (u4Count = 0; u4Count < SEC_MAX_STAT_COUNT; u4Count++)
    {
        pSecAhEspStatsIf = &gatIpsecv6AhEspStat[u4Count];
        if ((pSecAhEspStatsIf->u4Index < (UINT4) u4FirstIndex) &&
            (pSecAhEspStatsIf->u4Index != 0))
        {
            Flag = SEC_FOUND;
            u4FirstIndex = pSecAhEspStatsIf->u4Index;
        }
    }
    if ((Flag == SEC_NOT_FOUND) || (u4FirstIndex < SEC_MIN_STAT_COUNT))
    {
        return SNMP_FAILURE;
    }
    *pi4Fsipv6SecAhEspIfIndex = (INT4) u4FirstIndex;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6SecAhEspStatsTable
 Input       :  The Indices
                Fsipv6SecAhEspIfIndex
                nextFsipv6SecAhEspIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6SecAhEspStatsTable (INT4
                                         i4Fsipv6SecAhEspIfIndex,
                                         INT4 *pi4NextFsipv6SecAhEspIfIndex)
{

    tSecv6AhEspStat    *pSecAhEspStatsIf = NULL;
    UINT4               u4Count = 0;
    INT4                Flag = SEC_NOT_FOUND;

    for (u4Count = 0; u4Count < SEC_MAX_STAT_COUNT; u4Count++)
    {

        pSecAhEspStatsIf = &gatIpsecv6AhEspStat[u4Count];
        if (pSecAhEspStatsIf->u4Index > (UINT4) i4Fsipv6SecAhEspIfIndex)
        {
            if (Flag == SEC_NOT_FOUND)
            {
                *pi4NextFsipv6SecAhEspIfIndex =
                    (INT4) pSecAhEspStatsIf->u4Index;
                Flag = SEC_FOUND;
            }
            else
            {
                if (pSecAhEspStatsIf->u4Index <
                    (UINT4) *pi4NextFsipv6SecAhEspIfIndex)
                {
                    *pi4NextFsipv6SecAhEspIfIndex =
                        (INT4) pSecAhEspStatsIf->u4Index;
                }
            }
        }
    }
    if (Flag == SEC_NOT_FOUND)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6SecInAhPkts
 Input       :  The Indices
                Fsipv6SecAhEspIfIndex

                The Object 
                retValFsipv6SecInAhPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecInAhPkts (INT4 i4Fsipv6SecAhEspIfIndex,
                         UINT4 *pu4RetValFsipv6SecInAhPkts)
{

    *pu4RetValFsipv6SecInAhPkts =
        gatIpsecv6AhEspStat[i4Fsipv6SecAhEspIfIndex].u4InAhPkts;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv6SecOutAhPkts
 Input       :  The Indices
                Fsipv6SecAhEspIfIndex

                The Object 
                retValFsipv6SecOutAhPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecOutAhPkts (INT4 i4Fsipv6SecAhEspIfIndex,
                          UINT4 *pu4RetValFsipv6SecOutAhPkts)
{

    *pu4RetValFsipv6SecOutAhPkts =
        gatIpsecv6AhEspStat[i4Fsipv6SecAhEspIfIndex].u4OutAhPkts;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv6SecAhPktsAllow
 Input       :  The Indices
                Fsipv6SecAhEspIfIndex

                The Object 
                retValFsipv6SecAhPktsAllow
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecAhPktsAllow (INT4 i4Fsipv6SecAhEspIfIndex,
                            UINT4 *pu4RetValFsipv6SecAhPktsAllow)
{

    *pu4RetValFsipv6SecAhPktsAllow =
        gatIpsecv6AhEspStat[i4Fsipv6SecAhEspIfIndex].u4AhPktsAllow;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv6SecAhPktsDiscard
 Input       :  The Indices
                Fsipv6SecAhEspIfIndex

                The Object 
                retValFsipv6SecAhPktsDiscard
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecAhPktsDiscard (INT4 i4Fsipv6SecAhEspIfIndex,
                              UINT4 *pu4RetValFsipv6SecAhPktsDiscard)
{

    *pu4RetValFsipv6SecAhPktsDiscard =
        gatIpsecv6AhEspStat[i4Fsipv6SecAhEspIfIndex].u4AhPktsDiscard;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv6SecInEspPkts
 Input       :  The Indices
                Fsipv6SecAhEspIfIndex

                The Object 
                retValFsipv6SecInEspPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecInEspPkts (INT4 i4Fsipv6SecAhEspIfIndex,
                          UINT4 *pu4RetValFsipv6SecInEspPkts)
{

    *pu4RetValFsipv6SecInEspPkts =
        gatIpsecv6AhEspStat[i4Fsipv6SecAhEspIfIndex].u4InEspPkts;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv6SecOutEspPkts
 Input       :  The Indices
                Fsipv6SecAhEspIfIndex

                The Object 
                retValFsipv6SecOutEspPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecOutEspPkts (INT4 i4Fsipv6SecAhEspIfIndex,
                           UINT4 *pu4RetValFsipv6SecOutEspPkts)
{

    *pu4RetValFsipv6SecOutEspPkts =
        gatIpsecv6AhEspStat[i4Fsipv6SecAhEspIfIndex].u4OutEspPkts;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv6SecEspPktsDiscard
 Input       :  The Indices
                Fsipv6SecAhEspIfIndex

                The Object 
                retValFsipv6SecEspPktsDiscard
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecEspPktsDiscard (INT4 i4Fsipv6SecAhEspIfIndex,
                               UINT4 *pu4RetValFsipv6SecEspPktsDiscard)
{

    *pu4RetValFsipv6SecEspPktsDiscard =
        gatIpsecv6AhEspStat[i4Fsipv6SecAhEspIfIndex].u4EspPktsDiscard;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv6SecEspPktsAllow
 Input       :  The Indices
                Fsipv6SecAhEspIfIndex

                The Object 
                retValFsipv6SecEspPktsAllow
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecEspPktsAllow (INT4 i4Fsipv6SecAhEspIfIndex,
                             UINT4 *pu4RetValFsipv6SecEspPktsAllow)
{

    *pu4RetValFsipv6SecEspPktsAllow =
        gatIpsecv6AhEspStat[i4Fsipv6SecAhEspIfIndex].u4EspPktsAllow;
    return (SNMP_SUCCESS);
}

/* LOW LEVEL Routines for Table : Fsipv6SecAhEspIntruTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv6SecAhEspIntruTable
 Input       :  The Indices
                Fsipv6SecAhEspIntruIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhValidateIndexInstanceFsipv6SecAhEspIntruTable (INT4
                                                  i4Fsipv6SecAhEspIntruIndex)
{

    if (i4Fsipv6SecAhEspIntruIndex < SEC_MIN_INTEGER)
    {
        return (SNMP_FAILURE);
    }

    if (i4Fsipv6SecAhEspIntruIndex > SEC_MAX_STAT_COUNT)

    {
        return (SNMP_FAILURE);

    }
    if (gatIpsecv6AhEspIntruStat
        [i4Fsipv6SecAhEspIntruIndex - SEC_MIN_STAT_COUNT].u4Index == 0)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv6SecAhEspIntruTable
 Input       :  The Indices
                Fsipv6SecAhEspIntruIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsipv6SecAhEspIntruTable (INT4 *pi4Fsipv6SecAhEspIntruIndex)
{
    tSecv6AhEspIntruStat *pSecAhEspIntruStatsIf = NULL;
    UINT4               u4FirstIndex = SEC_MAX_INTEGER, u4Count = 0;
    INT4                Flag = SEC_NOT_FOUND;
    for (u4Count = 0; u4Count < SEC_MAX_STAT_COUNT; u4Count++)
    {
        pSecAhEspIntruStatsIf = &gatIpsecv6AhEspIntruStat[u4Count];
        if ((pSecAhEspIntruStatsIf->u4Index < (UINT4) u4FirstIndex)
            && (pSecAhEspIntruStatsIf->u4Index != 0))
        {
            Flag = SEC_FOUND;
            u4FirstIndex = pSecAhEspIntruStatsIf->u4Index;
        }
    }
    if ((Flag == SEC_NOT_FOUND) || (u4FirstIndex < SEC_MIN_STAT_COUNT))
    {
        return SNMP_FAILURE;
    }
    *pi4Fsipv6SecAhEspIntruIndex = (INT4) u4FirstIndex;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv6SecAhEspIntruTable
 Input       :  The Indices
                Fsipv6SecAhEspIntruIndex
                nextFsipv6SecAhEspIntruIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv6SecAhEspIntruTable (INT4
                                         i4Fsipv6SecAhEspIntruIndex,
                                         INT4 *pi4NextFsipv6SecAhEspIntruIndex)
{

    tSecv6AhEspIntruStat *pSecAhEspIntruStatsIf = NULL;
    UINT4               u4Count = 0;
    INT4                Flag = SEC_NOT_FOUND;
    for (u4Count = 0; u4Count < SEC_MAX_STAT_COUNT; u4Count++)
    {
        pSecAhEspIntruStatsIf = &gatIpsecv6AhEspIntruStat[u4Count];
        if (pSecAhEspIntruStatsIf->u4Index > (UINT4) i4Fsipv6SecAhEspIntruIndex)
        {
            if (Flag == SEC_NOT_FOUND)
            {
                *pi4NextFsipv6SecAhEspIntruIndex =
                    (INT4) pSecAhEspIntruStatsIf->u4Index;
                Flag = SEC_FOUND;
            }
            else
            {
                if (pSecAhEspIntruStatsIf->u4Index <
                    (UINT4) *pi4NextFsipv6SecAhEspIntruIndex)
                {
                    *pi4NextFsipv6SecAhEspIntruIndex =
                        (INT4) pSecAhEspIntruStatsIf->u4Index;
                }
            }
        }
    }
    if (Flag == SEC_NOT_FOUND)
    {
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv6SecAhEspIntruIfIndex
 Input       :  The Indices
                Fsipv6SecAhEspIntruIndex

                The Object 
                retValFsipv6SecAhEspIntruIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecAhEspIntruIfIndex (INT4
                                  i4Fsipv6SecAhEspIntruIndex,
                                  INT4 *pi4RetValFsipv6SecAhEspIntruIfIndex)
{
    *pi4RetValFsipv6SecAhEspIntruIfIndex =
        (INT4) gatIpsecv6AhEspIntruStat
        [i4Fsipv6SecAhEspIntruIndex - SEC_MIN_STAT_COUNT].u4IfIndex;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv6SecAhEspIntruSrcAddr
 Input       :  The Indices
                Fsipv6SecAhEspIntruIndex

                The Object 
                retValFsipv6SecAhEspIntruSrcAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecAhEspIntruSrcAddr (INT4
                                  i4Fsipv6SecAhEspIntruIndex,
                                  tSNMP_OCTET_STRING_TYPE
                                  * pRetValFsipv6SecAhEspIntruSrcAddr)
{

    Ip6AddrCopy ((tIp6Addr *) (VOID *)
                 pRetValFsipv6SecAhEspIntruSrcAddr->
                 pu1_OctetList,
                 &gatIpsecv6AhEspIntruStat
                 [i4Fsipv6SecAhEspIntruIndex -
                  SEC_MIN_STAT_COUNT].AhEspIntruSrcAddr);
    pRetValFsipv6SecAhEspIntruSrcAddr->i4_Length = SEC_ADDR_LEN;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv6SecAhEspIntruDestAddr
 Input       :  The Indices
                Fsipv6SecAhEspIntruIndex

                The Object 
                retValFsipv6SecAhEspIntruDestAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecAhEspIntruDestAddr (INT4
                                   i4Fsipv6SecAhEspIntruIndex,
                                   tSNMP_OCTET_STRING_TYPE
                                   * pRetValFsipv6SecAhEspIntruDestAddr)
{

    Ip6AddrCopy ((tIp6Addr *) (VOID *)
                 pRetValFsipv6SecAhEspIntruDestAddr->
                 pu1_OctetList,
                 &gatIpsecv6AhEspIntruStat
                 [i4Fsipv6SecAhEspIntruIndex -
                  SEC_MIN_STAT_COUNT].AhEspIntruDestAddr);
    pRetValFsipv6SecAhEspIntruDestAddr->i4_Length = SEC_ADDR_LEN;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv6SecAhEspIntruProto
 Input       :  The Indices
                Fsipv6SecAhEspIntruIndex

                The Object 
                retValFsipv6SecAhEspIntruProto
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecAhEspIntruProto (INT4
                                i4Fsipv6SecAhEspIntruIndex,
                                INT4 *pi4RetValFsipv6SecAhEspIntruProto)
{

    *pi4RetValFsipv6SecAhEspIntruProto =
        (INT4) gatIpsecv6AhEspIntruStat
        [i4Fsipv6SecAhEspIntruIndex - SEC_MIN_STAT_COUNT].u4AhEspIntruProt;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv6SecAhEspIntruTime
 Input       :  The Indices
                Fsipv6SecAhEspIntruIndex

                The Object 
                retValFsipv6SecAhEspIntruTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecAhEspIntruTime (INT4
                               i4Fsipv6SecAhEspIntruIndex,
                               UINT4 *pu4RetValFsipv6SecAhEspIntruTime)
{

    *pu4RetValFsipv6SecAhEspIntruTime =
        gatIpsecv6AhEspIntruStat
        [i4Fsipv6SecAhEspIntruIndex - SEC_MIN_STAT_COUNT].u4AhEspIntruTime;
    return (SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsipv6SecGlobalStatus
 Input       :  The Indices

                The Object 
                retValFsipv6SecGlobalStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecGlobalStatus (INT4 *pi4RetValFsipv6SecGlobalStatus)
{

    *pi4RetValFsipv6SecGlobalStatus = gSecv6Status;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv6SecVersion
 Input       :  The Indices

                The Object 
                retValFsipv6SecVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecVersion (UINT4 *pu4RetValFsipv6SecVersion)
{

    *pu4RetValFsipv6SecVersion = gSecv6Version;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv6SecGlobalDebug
 Input       :  The Indices

                The Object 
                retValFsipv6SecGlobalDebug
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecGlobalDebug (INT4 *pi4RetValFsipv6SecGlobalDebug)
{
    *pi4RetValFsipv6SecGlobalDebug = (INT4) gu4Secv6Debug;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv6SecMaxSA
 Input       :  The Indices

                The Object 
                retValFsipv6SecMaxSA
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv6SecMaxSA (INT4 *pi4RetValFsipv6SecMaxSA)
{
    *pi4RetValFsipv6SecMaxSA = MAX_IPSEC6_MAX_SA;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv6SecGlobalStatus
 Input       :  The Indices

                The Object 
                setValFsipv6SecGlobalStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SecGlobalStatus (INT4 i4SetValFsipv6SecGlobalStatus)
{

    gSecv6Status = (UINT1) i4SetValFsipv6SecGlobalStatus;

    if (gSecv6Status == SEC_ENABLE)
    {

        Secv6UpdateSecAssocSize ();
        if (Secv6ConstSecList () != SEC_SUCCESS)
        {
            return (SNMP_FAILURE);
        }

        if (Secv6ConstAccessList () != SEC_SUCCESS)
        {
            return (SNMP_FAILURE);
        }
    }
    else
    {

        if (Secv6DeleteFormattedSelectorEntries () != SEC_SUCCESS)
        {
            return SNMP_FAILURE;
        }

    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsipv6SecGlobalDebug
 Input       :  The Indices

                The Object 
                setValFsipv6SecGlobalDebug
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SecGlobalDebug (INT4 i4SetValFsipv6SecGlobalDebug)
{
    gu4Secv6Debug = (UINT4) i4SetValFsipv6SecGlobalDebug;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv6SecMaxSA
 Input       :  The Indices

                The Object 
                setValFsipv6SecMaxSA
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv6SecMaxSA (INT4 i4SetValFsipv6SecMaxSA)
{
    SEC_UNUSED (i4SetValFsipv6SecMaxSA);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SecGlobalStatus
 Input       :  The Indices

                The Object 
                testValFsipv6SecGlobalStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SecGlobalStatus (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFsipv6SecGlobalStatus)
{
    if ((i4TestValFsipv6SecGlobalStatus != SEC_ENABLE) &&
        (i4TestValFsipv6SecGlobalStatus != SEC_DISABLE))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SecGlobalDebug
 Input       :  The Indices

                The Object 
                testValFsipv6SecGlobalDebug
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SecGlobalDebug (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsipv6SecGlobalDebug)
{
    if ((i4TestValFsipv6SecGlobalDebug < 0) ||
        (i4TestValFsipv6SecGlobalDebug > 9))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv6SecMaxSA
 Input       :  The Indices

                The Object 
                testValFsipv6SecMaxSA
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv6SecMaxSA (UINT4 *pu4ErrorCode, INT4 i4TestValFsipv6SecMaxSA)
{
    SEC_UNUSED (i4TestValFsipv6SecMaxSA);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsipv6SecGlobalStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6SecGlobalStatus (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6SecGlobalDebug
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6SecGlobalDebug (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsipv6SecMaxSA
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsipv6SecMaxSA (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************/
/*  Function Name   : Secv6SelGetEntry                                       */
/*  Description     : This function returns the Selector Entry from the     */
/*                  : Selector List based on the Inputs                    */
/*                  : SecurityAssociation List using the SPI as Index      */
/*  Input(s)        : Index :Refers to the Incoming Index                  */
/*                  : ProtId:Refers to the Protocol                        */
/*                  : AccessIndex:Refers to the InaccessIndex            */
/*                  : i4Port:Refers to the application of IP               */
/*                  : i4Direction :Refers to the Direction of the Packet   */
/*  Output(s)       : Selector DataBase                                    */
/*  Returns         : Pointer to Selector Entry or Null                     */
/***************************************************************************/

tSecv6Selector     *
Secv6SelGetEntry (UINT4 Index, UINT4 ProtId,
                  UINT4 u4AccessIndex, INT4 i4Port, INT4 i4PktDirection)
{
    tSecv6Selector     *pEntry = NULL;
    TMO_SLL_Scan (&Secv6SelList, pEntry, tSecv6Selector *)
    {
        if ((pEntry->u4IfIndex == Index)
            && (pEntry->u4ProtoId == ProtId)
            && (u4AccessIndex == pEntry->u4SelAccessIndex)
            && (pEntry->u4Port == (UINT4) i4Port)
            && (pEntry->u4PktDirection == (UINT4) i4PktDirection))
        {
            if (pEntry->u4SelStatus == ACTIVE)
            {
                return pEntry;
            }

        }
    }
    return NULL;
}

/****************************************************************************/
/*  Function Name   : Secv6LowSelGetEntry                                       */
/*  Description     : This function returns the Selector Entry from the     */
/*                  : Selector List based on the Inputs                    */
/*                  : SecurityAssociation List using the SPI as Index      */
/*  Input(s)        : Index :Refers to the Incoming Index                  */
/*                  : ProtId:Refers to the Protocol                        */
/*                  : AccessIndex:Refers to the InaccessIndex            */
/*                  : i4Port:Refers to the application of IP               */
/*                  : i4Direction :Refers to the Direction of the Packet   */
/*  Output(s)       : Selector DataBase                                    */
/*  Returns         : Pointer to Selector Entry or Null                     */
/***************************************************************************/

tSecv6Selector     *
Secv6LowSelGetEntry (UINT4 Index, UINT4 ProtId,
                     UINT4 u4AccessIndex, INT4 i4Port, INT4 i4PktDirection)
{
    tSecv6Selector     *pEntry = NULL;
    TMO_SLL_Scan (&Secv6SelList, pEntry, tSecv6Selector *)
    {
        if ((pEntry->u4IfIndex == Index)
            && (pEntry->u4ProtoId == ProtId)
            && (u4AccessIndex == pEntry->u4SelAccessIndex)
            && (pEntry->u4Port == (UINT4) i4Port)
            && (pEntry->u4PktDirection == (UINT4) i4PktDirection))
        {
            return pEntry;

        }
    }
    return NULL;
}

/****************************************************************************/
/*  Function Name   : Secv6LowSelGetFormattedEntry                                */
/*  Description     : This function returns the Selector Entry from the     */
/*                  : Formatted Selector List based on the Inputs          */
/*                  : SecurityAssociation List using the SPI as Index      */
/*  Input(s)        : Index :Refers to the Incoming Index                  */
/*                  : ProtId:Refers to the Protocol                        */
/*                  : AccessIndex:Refers to the InaccessIndex            */
/*                  : i4Port:Refers to the application of IP               */
/*                  : i4Direction :Refers to the Direction of the Packet   */
/*  Output(s)       : Formatted Selector DataBase                          */
/*  Returns         : Pointer to Selector Entry or NULL                     */
/***************************************************************************/

tSecv6Selector     *
Secv6LowSelGetFormattedEntry (UINT4 Index, UINT4 ProtId,
                              UINT4 u4AccessIndex, INT4 i4Port,
                              INT4 i4PktDirection)
{
    tSecv6Selector     *pEntry = NULL;
    TMO_SLL_Scan (&Secv6SelFormattedList, pEntry, tSecv6Selector *)
    {
        if (((pEntry->u4IfIndex == Index)
             || (pEntry->u4IfIndex == SEC_ANY_IFINDEX))
            && ((pEntry->u4ProtoId == ProtId)
                || (pEntry->u4ProtoId == SEC_ANY_PROTOCOL))
            && (u4AccessIndex == pEntry->u4SelAccessIndex)
            && ((pEntry->u4Port == (UINT4) i4Port)
                || (pEntry->u4Port == SEC_ANY_PORT))
            && ((pEntry->u4PktDirection == (UINT4) i4PktDirection)
                || (pEntry->u4PktDirection == SEC_ANY_DIRECTION)))
        {
            return pEntry;
        }
    }
    return NULL;
}

/****************************************************************************/
/*  Function Name   : Secv6SelGetFormattedEntry                                */
/*  Description     : This function returns the Selector Entry from the     */
/*                  : Formatted Selector List based on the Inputs          */
/*                  : SecurityAssociation List using the SPI as Index      */
/*  Input(s)        : Index :Refers to the Incoming Index                  */
/*                  : ProtId:Refers to the Protocol                        */
/*                  : AccessIndex:Refers to the InaccessIndex            */
/*                  : i4Port:Refers to the application of IP               */
/*                  : i4Direction :Refers to the Direction of the Packet   */
/*  Output(s)       : Formatted Selector DataBase                          */
/*  Returns         : Pointer to Selector Entry or NULL                     */
/***************************************************************************/

tSecv6Selector     *
Secv6SelGetFormattedEntry (UINT4 Index, UINT4 ProtId,
                           UINT4 u4AccessIndex, INT4 i4Port,
                           INT4 i4PktDirection)
{
    tSecv6Selector     *pEntry = NULL;
    TMO_SLL_Scan (&Secv6SelFormattedList, pEntry, tSecv6Selector *)
    {
        if (((pEntry->u4IfIndex == Index)
             || (pEntry->u4IfIndex == SEC_ANY_IFINDEX))
            && ((pEntry->u4ProtoId == ProtId)
                || (pEntry->u4ProtoId == SEC_ANY_PROTOCOL))
            && (u4AccessIndex == pEntry->u4SelAccessIndex)
            && ((pEntry->u4Port == (UINT4) i4Port)
                || (pEntry->u4Port == SEC_ANY_PORT))
            && ((pEntry->u4PktDirection == (UINT4) i4PktDirection)
                || (pEntry->u4PktDirection == SEC_ANY_DIRECTION)))
        {
            if (pEntry->u4SelStatus == ACTIVE)
            {
                return pEntry;
            }
        }
    }
    return NULL;
}

/****************************************************************************/
/*  Function Name   :  Secv6AccessGetEntry                                 */
/*  Description     : This function returns the Access Entry from the     */
/*                  : Access List based on the Inputs                    */
/*  Input(s)        : u4AccessIndex                                      */
/*  Output(s)       : AccessDataBase                                     */
/*  Returns         : Pointer to Access Entry or NULL                     */
/***************************************************************************/

tSecv6Access       *
Secv6AccessGetEntry (UINT4 u4AccessIndex)
{

    tSecv6Access       *pEntry = NULL;
    TMO_SLL_Scan (&Secv6AccessList, pEntry, tSecv6Access *)
    {
        if (u4AccessIndex == pEntry->u4GroupIndex)
        {
            if (pEntry->u4AccStatus == ACTIVE)
            {
                return pEntry;
            }
        }
    }
    return NULL;
}

/****************************************************************************/
/*  Function Name   :  Secv6LowAccessGetEntry                                 */
/*  Description     : This function returns the Access Entry from the     */
/*                  : Access List based on the Inputs                    */
/*  Input(s)        : u4AccessIndex                                      */
/*  Output(s)       : AccessDataBase                                     */
/*  Returns         : Pointer to Access Entry or NULL                     */
/***************************************************************************/

tSecv6Access       *
Secv6LowAccessGetEntry (UINT4 u4AccessIndex)
{

    tSecv6Access       *pEntry = NULL;
    TMO_SLL_Scan (&Secv6AccessList, pEntry, tSecv6Access *)
    {
        if (u4AccessIndex == pEntry->u4GroupIndex)
        {
            return pEntry;
        }
    }
    return NULL;
}

/****************************************************************************/
/*  Function Name   :  Secv6PolicyGetEntry                                   */
/*  Description     : This function returns the Policy Entry from the       */
/*                  : Policy List based on the Inputs                      */
/*  Input(s)        :                                                      */
/*                  : u4Index:Refers to the PolicyIndex                    */
/*  Output(s)       : Policy DataBase                                      */
/*  Returns         : Pointer to Policy Entry or NULL                       */
/***************************************************************************/

tSecv6Policy       *
Secv6PolicyGetEntry (UINT4 Index)
{
    tSecv6Policy       *pEntry = NULL;
    TMO_SLL_Scan (&Secv6PolicyList, pEntry, tSecv6Policy *)
    {
        if (pEntry->u4PolicyIndex == Index)
        {
            if (pEntry->u4PolicyStatus == ACTIVE)
            {
                return pEntry;
            }
        }
    }
    return NULL;
}

/****************************************************************************/
/*  Function Name   :  Secv6LowPolicyGetEntry                                   */
/*  Description     : This function returns the Policy Entry from the       */
/*                  : Policy List based on the Inputs                      */
/*  Input(s)        :                                                      */
/*                  : u4Index:Refers to the PolicyIndex                    */
/*  Output(s)       : Policy DataBase                                      */
/*  Returns         : Pointer to Policy Entry or NULL                       */
/***************************************************************************/

tSecv6Policy       *
Secv6LowPolicyGetEntry (UINT4 Index)
{
    tSecv6Policy       *pEntry = NULL;
    TMO_SLL_Scan (&Secv6PolicyList, pEntry, tSecv6Policy *)
    {
        if (pEntry->u4PolicyIndex == Index)
        {
            return pEntry;
        }
    }
    return NULL;
}

/****************************************************************************/
/*  Function Name   :  Secv6AssocGetEntry                                 */
/*  Description     : This function returns the SecAssoc Entry from the     */
/*                  : SecAssoc List based on the Inputs                    */
/*  Input(s)        :                                                      */
/*                  : u4Index:Refers to the SecurityParameterIndex         */
/*  Output(s)       : SecurityAssoc DataBase                               */
/*  Returns         : Pointer to SecAssoc Entry or NULL                     */
/***************************************************************************/

tSecv6Assoc        *
Secv6AssocGetEntry (UINT4 Index)
{
    tSecv6Assoc        *pEntry = NULL;
    TMO_SLL_Scan (&Secv6AssocList, pEntry, tSecv6Assoc *)
    {
        if (pEntry->u4SecAssocIndex == Index)
        {
            if (pEntry->u4AssocStatus == ACTIVE)
            {
                return pEntry;
            }
        }
    }
    return NULL;
}

/****************************************************************************/
/*  Function Name   :  Secv6LowAssocGetEntry                                 */
/*  Description     : This function returns the SecAssoc Entry from the     */
/*                  : SecAssoc List based on the Inputs                    */
/*  Input(s)        :                                                      */
/*                  : u4Index:Refers to the SecurityParameterIndex         */
/*  Output(s)       : SecurityAssoc DataBase                               */
/*  Returns         : Pointer to SecAssoc Entry or NULL                     */
/***************************************************************************/

tSecv6Assoc        *
Secv6LowAssocGetEntry (UINT4 Index)
{
    tSecv6Assoc        *pEntry = NULL;
    TMO_SLL_Scan (&Secv6AssocList, pEntry, tSecv6Assoc *)
    {
        if (pEntry->u4SecAssocIndex == Index)
        {
            return pEntry;
        }
    }
    return NULL;
}

/****************************************************************************/
/*  Function Name   :  Secv6SelectorCompareGetNext                           */
/*  Description     : This function is used by the GetNext routine of a    */
/*                  : Table to the get the Lexicographically smallest      */
/*                  : entry                                                */
/*  Input(s)        :                                                      */
/*                  : pEntry:Pointer to the Selector Entry                   */
/*                  : i4IfIndex:Refers to the Interface Index              */
/*                  : i4ProtId:Refers to the Protocol                      */
/*                  : i4AccIndex:Refers to the AccessIndex              */
/*                  : i4Fsipv6SelPort:Refers to the application ofIP       */
/*                  : i4Fsipv6SelPktDirection :Refers to the Packet        */
/*                  : Direction                                            */
/*  Output(s)       : The Next Small Index                                 */
/*  Returns         : Returns the Difference between the first mismatch    */
/*                  : Index                                                */
/***************************************************************************/
INT4
Secv6SelectorCompareGetNext (tSecv6Selector * pEntry,
                             INT4 i4IfIndex, INT4 i4ProtId,
                             INT4 i4AccIndex,
                             INT4 i4Fsipv6SelPort, INT4 i4Fsipv6SelPktDirection)
{

    if (pEntry->u4IfIndex != (UINT4) i4IfIndex)
        return (INT4) (pEntry->u4IfIndex - (UINT4) i4IfIndex);
    if (pEntry->u4ProtoId != (UINT4) i4ProtId)
        return (INT4) (pEntry->u4ProtoId - (UINT4) i4ProtId);
    if (pEntry->u4SelAccessIndex != (UINT4) i4AccIndex)
        return (INT4) (pEntry->u4SelAccessIndex - (UINT4) i4AccIndex);
    if (pEntry->u4Port != (UINT4) i4Fsipv6SelPort)
        return (INT4) (pEntry->u4Port - (UINT4) i4Fsipv6SelPort);
    if (pEntry->u4PktDirection != (UINT4) i4Fsipv6SelPktDirection)
        return (INT4) (pEntry->u4PktDirection -
                       (UINT4) i4Fsipv6SelPktDirection);
    /* Lexicographically Equal */
    return (0);
}

/****************************************************************************/
/*  Function Name   :  Secv6SelectorCompare                                  */
/*  Description     : This function is used by the GetFirst routine of a   */
/*                  : Table to the get the Lexicographically smallest      */
/*                  : entry                                                */
/*  Input(s)        :                                                      */
/*                  : pEntry:Pointer to the Selector Entry                   */
/*                  : pNextEntry:Refers to the outgoing node                */
/*                  :                                                      */
/*  Output(s)       : The Next Small Index                                 */
/*  Returns         : Returns the Difference between the first mismatch    */
/*                  : Index                                                */
/***************************************************************************/
INT4
Secv6SelectorCompare (tSecv6Selector * pEntry, tSecv6Selector * pNextEntry)
{
    if (pEntry->u4IfIndex != pNextEntry->u4IfIndex)
        return (INT4) (pEntry->u4IfIndex - pNextEntry->u4IfIndex);
    if (pEntry->u4ProtoId != pNextEntry->u4ProtoId)
        return (INT4) (pEntry->u4ProtoId - pNextEntry->u4ProtoId);
    if (pEntry->u4SelAccessIndex != pNextEntry->u4SelAccessIndex)
        return (INT4) (pEntry->u4SelAccessIndex - pNextEntry->u4SelAccessIndex);
    if (pEntry->u4Port != pNextEntry->u4Port)
        return (INT4) (pEntry->u4Port - pNextEntry->u4Port);
    if (pEntry->u4PktDirection != pNextEntry->u4PktDirection)
        return (INT4) (pEntry->u4PktDirection - pNextEntry->u4PktDirection);
    /* Lexicographically Equal */
    return (0);
}

/****************************************************************************/
/*  Function Name   :  Secv6GetAssocEntry                              */
/*  Description     : This function returns the SecAssoc Entry from the     */
/*                  : SecAssoc List based on the Inputs                    */
/*  Input(s)        :                                                      */
/*                  : u4Index:Refers to the SecurityParameterIndex         */
/*                  : DestAddr:Refers to Ipv6 Destination Address          */
/*                  : u1Protocol:Refers to the Protocol                    */
/*  Output(s)       : SecurityAssoc DataBase                               */
/*  Returns         : Pointer to SecAssoc Entry or NULL                     */
/***************************************************************************/
tSecv6Assoc        *
Secv6GetAssocEntry (UINT4 u4SpiIndex, tIp6Addr DestAddr, UINT1 u1Protocol)
{
    tSecv6Assoc        *pEntry = NULL;
    TMO_SLL_Scan (&Secv6AssocList, pEntry, tSecv6Assoc *)
    {
        if ((pEntry->u4SecAssocSpi == u4SpiIndex) &&
            (pEntry->u1SecAssocProtocol == u1Protocol) &&
            (IPSEC_MEMCMP
             (&pEntry->SecAssocDestAddr, &DestAddr, SEC_ADDR_LEN) == 0))
        {
            if (pEntry->u4AssocStatus == ACTIVE)
            {
                return pEntry;
            }
        }
    }
    return NULL;
}

/****************************************************************************/
/*  Function Name   : Secv6UpdateSecAssocSize                              */
/*  Description     : This function updates the size for each secasso      */
/*                  : entry in the SecAssoc List                           */
/*  Inputs          :None                                                  */
/*  Outputs         :Updates the size in secassoc entry base on algo       */
/*  Returns         :None                                                  */
/***************************************************************************/
VOID
Secv6UpdateSecAssocSize (VOID)
{
    tSecv6Assoc        *pEntry = NULL;
    TMO_SLL_Scan (&Secv6AssocList, pEntry, tSecv6Assoc *)
    {

        if (pEntry->u1SecAssocMode == SEC_TUNNEL)
        {
            pEntry->u2Size = (UINT2) (pEntry->u2Size + SEC_IPV6_HEADER_SIZE);
        }
        if (pEntry->u1SecAssocProtocol == SEC_ESP)
        {
            if (pEntry->u1SecAssocAhAlgo == SEC_DES_CBC)
            {
                pEntry->u2Size = (UINT2) (pEntry->u2Size + SEC_ESP_DES_KEY_LEN);
            }
            pEntry->u2Size = (UINT2) (pEntry->u2Size + SEC_ESP_SIZE);
        }
        else
        {
            pEntry->u2Size = (UINT2) (pEntry->u2Size + SEC_AH_SIZE);
        }
        if (pEntry->u1SecAssocAhAlgo != SEC_NULLAHALGO)
            pEntry->u2Size = (UINT2) (pEntry->u2Size + SEC_AUTH_DIGEST_SIZE);
    }
}

/****************************************************************************/
/*  Function Name   :  Secv6ConstSecList                                   */
/*  Description     : This function is used to construct a                 */
/*                  : Lexicographically ordered selector list              */
/*                  :                                                      */
/*  Input(s)        : None                                                 */
/*                  :                                                      */
/*  Output(s)       : Formatted Selector List                              */
/*  Returns         : SEC_SUCCESS or SEC_FAILURE                           */
/***************************************************************************/
INT1
Secv6ConstSecList (VOID)
{

    tSecv6Selector     *pSecSelIf = NULL, *pNextSecSelIf = NULL,
        *pFirstSecSelIf = NULL;
    UINT4               u4Count = 0;
    UINT4               u4SelCount = 0;
    UINT1               Flag = 0;

    /* Get the First Node in the Selector List */
    pSecSelIf = (tSecv6Selector *) TMO_SLL_First (&Secv6SelList);

    /* If the List is empty return success */
    if (pSecSelIf == NULL)
    {
        return (SEC_SUCCESS);
    }

    /* Get the Count of the entries in the selector list */
    u4SelCount = (TMO_SLL_Count (&Secv6SelList));

    /* Allocate memory for storing the first entry */
    if (MemAllocateMemBlock (SECv6_SEL_MEMPOOL,
                             (UINT1 **) &pFirstSecSelIf) != MEM_SUCCESS)
    {
        SECv6_TRC (SECv6_OS_RESOURCE,
                   "Unable to allocate memory from pool for Selector Table \n ");
        return (SEC_FAILURE);
    }
    pFirstSecSelIf->u4IfIndex = pSecSelIf->u4IfIndex;
    pFirstSecSelIf->u4ProtoId = pSecSelIf->u4ProtoId;
    pFirstSecSelIf->u4Port = pSecSelIf->u4Port;
    pFirstSecSelIf->u4PktDirection = pSecSelIf->u4PktDirection;
    pFirstSecSelIf->u4SelPolicyIndex = pSecSelIf->u4SelPolicyIndex;
    pFirstSecSelIf->pPolicyEntry = pSecSelIf->pPolicyEntry;
    pFirstSecSelIf->u1SelFilterFlag = pSecSelIf->u1SelFilterFlag;
    pFirstSecSelIf->u4SelAccessIndex = pSecSelIf->u4SelAccessIndex;
    pFirstSecSelIf->u4SelStatus = pSecSelIf->u4SelStatus;
    IPSEC_MEMCPY (&pFirstSecSelIf->TunnelTermAddr, &pSecSelIf->TunnelTermAddr,
                  sizeof (tIp6Addr));

    if (u4SelCount == 1)
    {
        TMO_SLL_Add (&Secv6SelFormattedList, &pFirstSecSelIf->NextSecSelIf);
        return (SEC_SUCCESS);
    }

    /* If there are more than one entry find the smallest entry in the 
       list */

    TMO_SLL_Scan (&Secv6SelList, pSecSelIf, tSecv6Selector *)
    {
        if (Secv6SelectorCompare (pSecSelIf, pFirstSecSelIf) < 0)
        {
            pFirstSecSelIf->u4IfIndex = pSecSelIf->u4IfIndex;
            pFirstSecSelIf->u4ProtoId = pSecSelIf->u4ProtoId;
            pFirstSecSelIf->u4Port = pSecSelIf->u4Port;
            pFirstSecSelIf->u4PktDirection = pSecSelIf->u4PktDirection;
            pFirstSecSelIf->u4SelPolicyIndex = pSecSelIf->u4SelPolicyIndex;
            pFirstSecSelIf->pPolicyEntry = pSecSelIf->pPolicyEntry;
            pFirstSecSelIf->u1SelFilterFlag = pSecSelIf->u1SelFilterFlag;
            pFirstSecSelIf->u4SelAccessIndex = pSecSelIf->u4SelAccessIndex;
            pFirstSecSelIf->u4SelStatus = pSecSelIf->u4SelStatus;
            IPSEC_MEMCPY (&pFirstSecSelIf->TunnelTermAddr,
                          &pSecSelIf->TunnelTermAddr, sizeof (tIp6Addr));
        }
    }

    /* Add the Least entry Found */
    TMO_SLL_Add (&Secv6SelFormattedList, &pFirstSecSelIf->NextSecSelIf);

    /* Proceed for the next smallest entry */
    for (u4Count = 0; u4Count < u4SelCount; u4Count++)
    {

        Flag = SEC_NOT_FOUND;

        TMO_SLL_Scan (&Secv6SelList, pSecSelIf, tSecv6Selector *)
        {

            if ((Secv6SelectorCompare (pSecSelIf, pFirstSecSelIf)) > 0)
            {

                if (Flag == SEC_NOT_FOUND)
                {
                    Flag = SEC_FOUND;
                    if (pNextSecSelIf != NULL)
                    {
                        if (MemReleaseMemBlock (SECv6_SEL_MEMPOOL,
                                                (UINT1 *) pNextSecSelIf) !=
                            MEM_SUCCESS)
                        {
                            SECv6_TRC (SECv6_OS_RESOURCE,
                                       "Unable to Release memory to pool for Selector Table\n");
                            return (SEC_FAILURE);
                        }
                    }

                    if (MemAllocateMemBlock (SECv6_SEL_MEMPOOL,
                                             (UINT1 **) &pNextSecSelIf) !=
                        MEM_SUCCESS)
                    {
                        SECv6_TRC (SECv6_OS_RESOURCE,
                                   "Unable to allocate memory from pool for Selector Table \n ");
                        return (SEC_FAILURE);
                    }

                    pNextSecSelIf->u4IfIndex = pSecSelIf->u4IfIndex;
                    pNextSecSelIf->u4ProtoId = pSecSelIf->u4ProtoId;
                    pNextSecSelIf->u4Port = pSecSelIf->u4Port;
                    pNextSecSelIf->u4PktDirection = pSecSelIf->u4PktDirection;
                    pNextSecSelIf->u4SelPolicyIndex =
                        pSecSelIf->u4SelPolicyIndex;
                    pNextSecSelIf->pPolicyEntry = pSecSelIf->pPolicyEntry;
                    pNextSecSelIf->u1SelFilterFlag = pSecSelIf->u1SelFilterFlag;
                    pNextSecSelIf->u4SelAccessIndex =
                        pSecSelIf->u4SelAccessIndex;
                    pNextSecSelIf->u4SelStatus = pSecSelIf->u4SelStatus;
                    IPSEC_MEMCPY (&pNextSecSelIf->TunnelTermAddr,
                                  &pSecSelIf->TunnelTermAddr,
                                  sizeof (tIp6Addr));
                }
                else
                {

                    if (pNextSecSelIf == NULL)
                    {
                        return (SEC_FAILURE);
                    }
                    if ((Secv6SelectorCompare (pSecSelIf, pNextSecSelIf)) < 0)
                    {
                        if (pNextSecSelIf != NULL)
                        {
                            if (MemReleaseMemBlock (SECv6_SEL_MEMPOOL,
                                                    (UINT1 *) pNextSecSelIf) !=
                                MEM_SUCCESS)
                            {
                                SECv6_TRC (SECv6_OS_RESOURCE,
                                           "Unable to Release memory to pool for Selector Table\n");
                                return (SEC_FAILURE);
                            }
                        }

                        if (MemAllocateMemBlock (SECv6_SEL_MEMPOOL,
                                                 (UINT1 **) &pNextSecSelIf) !=
                            MEM_SUCCESS)
                        {
                            SECv6_TRC (SECv6_OS_RESOURCE,
                                       "Unable to allocate memory from pool for Selector Table \n");
                            return (SEC_FAILURE);
                        }
                        pNextSecSelIf->u4IfIndex = pSecSelIf->u4IfIndex;
                        pNextSecSelIf->u4ProtoId = pSecSelIf->u4ProtoId;
                        pNextSecSelIf->u4Port = pSecSelIf->u4Port;
                        pNextSecSelIf->u4PktDirection =
                            pSecSelIf->u4PktDirection;
                        pNextSecSelIf->u4SelPolicyIndex =
                            pSecSelIf->u4SelPolicyIndex;
                        pNextSecSelIf->pPolicyEntry = pSecSelIf->pPolicyEntry;
                        pNextSecSelIf->u1SelFilterFlag =
                            pSecSelIf->u1SelFilterFlag;
                        pNextSecSelIf->u4SelAccessIndex =
                            pSecSelIf->u4SelAccessIndex;
                        pNextSecSelIf->u4SelStatus = pSecSelIf->u4SelStatus;
                        IPSEC_MEMCPY (&pNextSecSelIf->TunnelTermAddr,
                                      &pSecSelIf->TunnelTermAddr,
                                      sizeof (tIp6Addr));
                    }
                }
            }
        }

        if ((Flag == SEC_NOT_FOUND) && (u4Count >= u4SelCount))
            return (SEC_FAILURE);
        if (Flag == SEC_NOT_FOUND)
            continue;

        TMO_SLL_Add (&Secv6SelFormattedList, &pNextSecSelIf->NextSecSelIf);

        pFirstSecSelIf = pNextSecSelIf;
        pNextSecSelIf = NULL;
    }
    pFirstSecSelIf = NULL;
    return (SEC_SUCCESS);
}

/***************************************************************************/
/*  Function Name   : Secv6ConstAccessList                                 */
/*  Description     : This function is used to construct a                 */
/*                  : Lexicographically ordered Access list. The ordering  */
/*                  : is based on the access index                         */
/*                  :                                                      */
/*  Input(s)        : None                                                 */
/*                  :                                                      */
/*  Output(s)       : Formatted Access List                                */
/*  Returns         : SEC_SUCCESS or SEC_FAILURE                           */
/***************************************************************************/
INT1
Secv6ConstAccessList (VOID)
{
    tSecv6Access       *pEntry;
    tSecv6Access       *pPrevEntry = NULL;
    tSecv6Access        TmpEntry;
    UINT4               u4AccessCount = 0;
    UINT4               u4Count = 0;
    /* Check if any nodes are present in the list */
    pPrevEntry = (tSecv6Access *) TMO_SLL_First (&Secv6AccessList);
    if (pPrevEntry == NULL)
        return (SEC_SUCCESS);
    /* Find out the number of entries in the access list */
    u4AccessCount = (TMO_SLL_Count (&Secv6AccessList));
    /* Order the access list entries basing on the access index */
    for (u4Count = 0; u4Count < u4AccessCount; u4Count++)
    {
        pPrevEntry = (tSecv6Access *) TMO_SLL_First (&Secv6AccessList);
        TMO_SLL_Scan (&Secv6AccessList, pEntry, tSecv6Access *)
        {
            if (pEntry->u4GroupIndex < pPrevEntry->u4GroupIndex)
            {
                TmpEntry.u4GroupIndex = pEntry->u4GroupIndex;
                TmpEntry.SrcAddress = pEntry->SrcAddress;
                TmpEntry.u4SrcAddrPrefixLen = pEntry->u4SrcAddrPrefixLen;
                TmpEntry.DestAddress = pEntry->DestAddress;
                TmpEntry.u4DestAddrPrefixLen = pEntry->u4DestAddrPrefixLen;
                pEntry->u4GroupIndex = pPrevEntry->u4GroupIndex;
                pEntry->SrcAddress = pPrevEntry->SrcAddress;
                pEntry->u4SrcAddrPrefixLen = pPrevEntry->u4SrcAddrPrefixLen;
                pEntry->DestAddress = pPrevEntry->DestAddress;
                pEntry->u4DestAddrPrefixLen = pPrevEntry->u4DestAddrPrefixLen;
                pPrevEntry->u4GroupIndex = TmpEntry.u4GroupIndex;
                pPrevEntry->SrcAddress = TmpEntry.SrcAddress;
                pPrevEntry->u4SrcAddrPrefixLen = TmpEntry.u4SrcAddrPrefixLen;
                pPrevEntry->DestAddress = TmpEntry.DestAddress;
                pPrevEntry->u4DestAddrPrefixLen = TmpEntry.u4DestAddrPrefixLen;
            }
            pPrevEntry = pEntry;
        }
    }

    return (SEC_SUCCESS);
}

/***************************************************************************/
/*  Function Name   : Secv6DeleteFormattedSelectorEntries                  */
/*  Description     : This function is used to delete formatted            */
/*                  : selector entries                                     */
/*                  :                                                      */
/*  Input(s)        : None                                                 */
/*                  :                                                      */
/*  Output(s)       : Empty Formatted Selector List                        */
/*  Returns         : SEC_SUCCESS or SEC_FAILURE                           */
/***************************************************************************/

INT1
Secv6DeleteFormattedSelectorEntries ()
{

    tSecv6Selector     *pSecv6FormatSel = NULL;
    UINT4               u4Entries = 0, u4Count = 0;

    u4Entries = TMO_SLL_Count (&Secv6SelFormattedList);
    for (u4Count = 0; u4Count < u4Entries; u4Count++)
    {

        TMO_SLL_Scan (&Secv6SelFormattedList, pSecv6FormatSel, tSecv6Selector *)
        {

            if (pSecv6FormatSel != NULL)
            {
                break;
            }
        }
        if (pSecv6FormatSel != NULL)
        {

            TMO_SLL_Delete (&Secv6SelFormattedList,
                            &pSecv6FormatSel->NextSecSelIf);
            if (MemReleaseMemBlock (SECv6_SEL_MEMPOOL,
                                    (UINT1 *) pSecv6FormatSel) != MEM_SUCCESS)
            {
                SECv6_TRC (SECv6_OS_RESOURCE,
                           "Unable to Release memory to pool for Selector Table\n");
                return (SEC_FAILURE);
            }

        }
    }
    return (SEC_SUCCESS);
}

/****************************************************************************/
/*  Function Name   :  Secv6DeletePolicyPtrInSel                            */
/*  Description     : This function makes the policy pointer in selector    */
/*                  : table to be null whose policy index is Index          */
/*  Input(s)        :                                                       */
/*                  : u4Index:Refers to the PolicyIndex                     */
/*  Output(s)       : Void                                                  */
/*  Returns         : Null                                                  */
/****************************************************************************/

VOID
Secv6DeletePolicyPtrInSel (UINT4 Index)
{
    tSecv6Selector     *pEntry = NULL;
    TMO_SLL_Scan (&Secv6SelList, pEntry, tSecv6Selector *)
    {
        if (pEntry->u4SelPolicyIndex == Index)
            pEntry->pPolicyEntry = NULL;
    }
}

/****************************************************************************/
/*  Function Name   :  Secv6DeleteSAPtrInPolicy                             */
/*  Description     : This function makes the SA pointer in Policy          */
/*                  : table to be null.                                     */
/*  Input(s)        :                                                       */
/*                  : u4Index:Refers to the PolicyIndex                     */
/*  Output(s)       : Void                                                  */
/*  Returns         : Null                                                  */
/****************************************************************************/

VOID
Secv6DeleteSAPtrInPolicy (tSecv6Assoc * pSecAssoc)
{
    tSecv6Policy       *pEntry = NULL;
    UINT1               u1ReKeySaCount = 0;
    UINT1               u1SaCount = 0;
    UINT1               u1Count = 0;

    TMO_SLL_Scan (&Secv6PolicyList, pEntry, tSecv6Policy *)
    {
        if (pEntry->paSaEntry[u1SaCount] == pSecAssoc)
        {
            IPSEC_MEMCPY (pEntry->paSaEntry,
                          &(pEntry->pau1NewSaEntry[0][0]), SEC_MAX_BUNDLE_SA);
            IPSEC_MEMCPY (&(pEntry->au1PolicySaBundle),
                          &(pEntry->au1NewPolicySaBundle[0][0]),
                          SEC_MAX_SA_BUNDLE_LEN);
            pEntry->u1SaCount = pEntry->au1NewSaCount[0];
            for (u1ReKeySaCount = 0;
                 u1ReKeySaCount < (SEC_MAX_SA_TO_POLICY - 1); u1ReKeySaCount++)
            {
                IPSEC_MEMCPY (&(pEntry->pau1NewSaEntry[u1ReKeySaCount][0]),
                              &(pEntry->pau1NewSaEntry[u1ReKeySaCount + 1]
                                [0]), SEC_MAX_BUNDLE_SA);
                IPSEC_MEMCPY (&(pEntry->au1NewPolicySaBundle
                                [u1ReKeySaCount][0]),
                              &(pEntry->au1NewPolicySaBundle
                                [u1ReKeySaCount + 1][0]),
                              SEC_MAX_SA_BUNDLE_LEN);
                pEntry->au1NewSaCount[u1ReKeySaCount] =
                    pEntry->au1NewSaCount[u1ReKeySaCount + 1];
            }
            IPSEC_MEMSET (&(pEntry->
                            pau1NewSaEntry[SEC_MAX_SA_TO_POLICY - 1][0]),
                          0, SEC_MAX_BUNDLE_SA);
            IPSEC_MEMSET (&(pEntry->
                            au1NewPolicySaBundle[SEC_MAX_SA_TO_POLICY -
                                                 1][0]), 0,
                          SEC_MAX_SA_BUNDLE_LEN);
            pEntry->au1NewSaCount[SEC_MAX_SA_TO_POLICY - 1] = 0;
            return;
        }
    }
    TMO_SLL_Scan (&Secv6PolicyList, pEntry, tSecv6Policy *)
    {
        for (u1ReKeySaCount = 0; u1ReKeySaCount < SEC_MAX_SA_TO_POLICY;
             u1ReKeySaCount++)
        {
            if ((pEntry->pau1NewSaEntry[u1ReKeySaCount][0]) == pSecAssoc)
            {
                for (u1Count = u1ReKeySaCount; u1Count <
                     (SEC_MAX_SA_TO_POLICY - 1); u1Count++)
                {
                    IPSEC_MEMCPY (&(pEntry->pau1NewSaEntry[u1Count]
                                    [0]),
                                  &(pEntry->pau1NewSaEntry[u1Count + 1][0]),
                                  SEC_MAX_BUNDLE_SA);
                    IPSEC_MEMCPY (&(pEntry->
                                    au1NewPolicySaBundle[u1Count][0]),
                                  &(pEntry->
                                    au1NewPolicySaBundle[u1Count + 1][0]),
                                  SEC_MAX_SA_BUNDLE_LEN);
                    pEntry->au1NewSaCount[u1Count] =
                        pEntry->au1NewSaCount[u1Count + 1];

                }
                IPSEC_MEMSET (&
                              (pEntry->
                               pau1NewSaEntry[SEC_MAX_SA_TO_POLICY - 1][0]),
                              0, SEC_MAX_BUNDLE_SA);
                IPSEC_MEMSET (&
                              (pEntry->
                               au1NewPolicySaBundle[SEC_MAX_SA_TO_POLICY -
                                                    1][0]), 0,
                              SEC_MAX_SA_BUNDLE_LEN);
                pEntry->au1NewSaCount[SEC_MAX_SA_TO_POLICY - 1] = 0;
                return;
            }

        }
    }
    return;
}

/****************************************************************************/
/*  Function Name   : Secv6DestroySecAssoc                                  */
/*  Description     : This function deletes the SA present in the list.     */
/*  Input(s)        : i4Fsipv6SecAssocIndex - SA index to be deleted        */
/*                  :                                                       */
/*  Output(s)       : Void                                                  */
/*  Returns         : SNMP_SUCCESS or SNMP_FAILURE                          */
/****************************************************************************/

INT1
Secv6DestroySecAssoc (INT4 i4Fsipv6SecAssocIndex)
{
    tSecv6Assoc        *pSecAssocIf = NULL;

    pSecAssocIf = Secv6LowAssocGetEntry ((UINT4) i4Fsipv6SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        SECv6_TRC (SECv6_OS_RESOURCE, "SA Not Found\n");
        return (SNMP_FAILURE);

    }

    if (pSecAssocIf->Secv6AssocHardTimer.u1TimerStatus == SEC_TIMER_ACTIVE)
    {
        Secv6StopTimer (&(pSecAssocIf->Secv6AssocHardTimer));
        pSecAssocIf->Secv6AssocHardTimer.u1TimerStatus = SEC_TIMER_INACTIVE;
    }

    if (pSecAssocIf->Secv6AssocSoftTimer.u1TimerStatus == SEC_TIMER_ACTIVE)
    {
        pSecAssocIf->Secv6AssocSoftTimer.u1TimerStatus = SEC_TIMER_INACTIVE;
        Secv6StopTimer (&(pSecAssocIf->Secv6AssocSoftTimer));
    }

    TMO_SLL_Delete (&Secv6AssocList, &pSecAssocIf->NextSecv6AssocIf);

    if (pSecAssocIf->u1SecAssocProtocol == SEC_ESP)
    {
        if (pSecAssocIf->pu1SecAssocEspKey != NULL)
        {

            /* FIPS compliance - Cryptographic keys zeroed at the
             * end of their lifecycle */
            IPSEC_MEMSET (pSecAssocIf->pu1SecAssocEspKey, 0,
                          (SEC_ESP_AES_KEY3_LEN + 1));

            if (MemReleaseMemBlock (SECv6_SAD_ESP_MEMPOOL,
                                    (UINT1 *) pSecAssocIf->
                                    pu1SecAssocEspKey) != MEM_SUCCESS)
            {
                SECv6_TRC (SECv6_OS_RESOURCE,
                           "Unable to Release memory to pool for SAD Table\n");
                return (SNMP_FAILURE);
            }
            if ((pSecAssocIf->u1SecAssocEspAlgo == SEC_DES_CBC) ||
                (pSecAssocIf->u1SecAssocEspAlgo == SEC_3DES_CBC))
            {
                if (MemReleaseMemBlock (SECv6_SAD_ESP_MEMPOOL,
                                        (UINT1 *) pSecAssocIf->
                                        pu1SecAssocInitVector) != MEM_SUCCESS)
                {
                    SECv6_TRC (SECv6_OS_RESOURCE,
                               "Unable to Release memory to pool for SAD Table\n");
                    return (SNMP_FAILURE);
                }
            }
        }
        if (pSecAssocIf->pu1SecAssocEspKey2 != NULL)
        {

            /* FIPS compliance - Cryptographic keys zeroed at the
             * end of their lifecycle */
            IPSEC_MEMSET (pSecAssocIf->pu1SecAssocEspKey2, 0,
                          (SEC_ESP_AES_KEY3_LEN + 1));

            if (MemReleaseMemBlock (SECv6_SAD_ESP_MEMPOOL,
                                    (UINT1 *) pSecAssocIf->
                                    pu1SecAssocEspKey2) != MEM_SUCCESS)
            {
                SECv6_TRC (SECv6_OS_RESOURCE,
                           "Unable to Release memory to pool for SAD Table\n");
                return (SNMP_FAILURE);
            }
        }
        if (pSecAssocIf->pu1SecAssocEspKey3 != NULL)
        {

            /* FIPS compliance - Cryptographic keys zeroed at the
             * end of their lifecycle */
            IPSEC_MEMSET (pSecAssocIf->pu1SecAssocEspKey3, 0,
                          (SEC_ESP_AES_KEY3_LEN + 1));

            if (MemReleaseMemBlock (SECv6_SAD_ESP_MEMPOOL,
                                    (UINT1 *) pSecAssocIf->
                                    pu1SecAssocEspKey3) != MEM_SUCCESS)
            {
                SECv6_TRC (SECv6_OS_RESOURCE,
                           "Unable to Release memory to pool for SAD Table\n");
                return (SNMP_FAILURE);
            }
        }
    }

    if (pSecAssocIf->pu1SecAssocAhKey != NULL)
    {

        /* FIPS compliance - Cryptographic keys zeroed at the
         * end of their lifecycle */
        IPSEC_MEMSET (pSecAssocIf->pu1SecAssocAhKey1, 0,
                      (SEC_HMAC_SHA_MAX_LEN + 1));

        if (MemReleaseMemBlock (SECv6_SAD_AH_MEMPOOL,
                                (UINT1 *) pSecAssocIf->
                                pu1SecAssocAhKey1) != MEM_SUCCESS)
        {
            SECv6_TRC (SECv6_OS_RESOURCE,
                       "nmhSetFsipv6SecAssocStatus : Unable to Release memory to pool for SAD Table\n");
            return (SNMP_FAILURE);
        }

        /* FIPS compliance - Cryptographic keys zeroed at the
         * end of their lifecycle */

        IPSEC_MEMSET (pSecAssocIf->pu1SecAssocAhKey, 0,
                      (SEC_HMAC_SHA_MAX_LEN + 1));

        if (MemReleaseMemBlock (SECv6_SAD_AH_MEMPOOL,
                                (UINT1 *) pSecAssocIf->
                                pu1SecAssocAhKey) != MEM_SUCCESS)
        {
            SECv6_TRC (SECv6_OS_RESOURCE,
                       "nmhSetFsipv6SecAssocStatus : Unable to Release memory to pool for SAD Table\n");
            return (SNMP_FAILURE);
        }
    }
    Secv6DeleteSAPtrInPolicy (pSecAssocIf);

    /* FIPS compliance - Cryptographic keys zeroed at the
     * end of their lifecycle */
    IPSEC_MEMSET (pSecAssocIf, 0, sizeof (tSecv6Assoc));

    if (MemReleaseMemBlock (SECv6_SAD_MEMPOOL,
                            (UINT1 *) pSecAssocIf) != MEM_SUCCESS)
    {
        SECv6_TRC (SECv6_OS_RESOURCE,
                   "nmhSetFsipv6SecAssocStatus : Unable to Release memory to pool for SAD Table\n");
        return (SNMP_FAILURE);
    }
    return SNMP_SUCCESS;
}
