/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secv6esp.c,v 1.14 2013/10/29 11:42:37 siva Exp $
 *
 * Description: This has functions for ESP SubModule 
 *
 ***********************************************************************/

#include "secv6com.h"
#include "secv6esp.h"

/************************************************************************/
/*  Function Name : Secv6EspEncode                                        */
/*  Description   : This function gets the outgoing packet and          */
/*                : calls either one of the mode of encode based on     */
/*                : SA Entry                                            */
/*  Input(s)      : pBuf - buffer contains IP packet                    */
/*                : pSaEntry - Pointer to a SA Entry                    */
/*                : u4Interface : Interface on which packet arrives     */
/*                :                                                     */
/*  Output(s)     : pBuf - buffer contains secured Ip packet            */
/*  Return Values : NULL or SecuredBuffer                            */
/*************************************************************************/
tBufChainHeader    *
Secv6EspEncode (tBufChainHeader * pBuf, tSecv6Assoc * pSaEntry,
                UINT4 u4Interface)
{
    switch (pSaEntry->u1SecAssocMode)
    {
        case SEC_TRANSPORT:
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6EspEncode :Secv6EspTransportEncode Entered \n");
            pBuf = Secv6EspTransportEncode (pBuf, pSaEntry, u4Interface);
            if (pBuf == NULL)
            {
                SECv6_TRC (SECv6_DATA_PATH,
                           "Secv6EspEncode :Secv6EspTransportEncode returns NULL \n");
                return NULL;
            }
            break;

        case SEC_TUNNEL:
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6EspEncode : Secv6EspTunnelEncode Entered \n");
            pBuf = Secv6EspTunnelEncode (pBuf, pSaEntry, u4Interface);
            if (pBuf == NULL)
            {
                SECv6_TRC (SECv6_DATA_PATH,
                           "Secv6EspEncode :Secv6EspTunnelEncode returns NULL \n");
                return NULL;
            }
            break;

        default:
            SECv6_TRC1 (SECv6_DATA_PATH,
                        "Secv6EspEncode : Sec Mode Number Fails(%d) \n",
                        pSaEntry->u1SecAssocMode);
            return NULL;
    }

    switch (pSaEntry->u1SecAssocMode)
    {
        case SEC_TRANSPORT:
            SECv6_TRC (SECv6_MUST, "\t\t[Secured with ESP]\tMode:Transport");
            break;
        case SEC_TUNNEL:
            SECv6_TRC (SECv6_MUST, "\t\t[Secured with ESP]\tMode:Tunnel");
            break;
        default:
            break;
    }
    return pBuf;
}

/***********************************************************************/
/*  Function Name : Secv6EspDecode                                       */
/*  Description   : This function gets the incomming packet and        */
/*                : calls either one of the mode of decode based on    */
/*                : SA Entry                                           */
/*  Input(s)      : pBuf - buffer contains IP Secured packet           */
/*                : pSaEntry - Pointer to a SA Entry                   */
/*                : pu4ProcessedLen : Indicates the length up to which */
/*                : the packet is processed                            */
/*                :                                                    */
/*  Output(s)     : pBuf - buffer contains IP packet                   */
/*  Return Values : NULL or SecuredBuffer                           */
/***********************************************************************/
tBufChainHeader    *
Secv6EspDecode (tBufChainHeader * pBuf, tSecv6Assoc * pSaEntry,
                UINT4 *pu4ProcessedLen)
{

    switch (pSaEntry->u1SecAssocMode)
    {
        case SEC_TRANSPORT:
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6EspDecode :Secv6EspTransportDecode Entered \n");
            pBuf = Secv6EspTransportDecode (pBuf, pSaEntry, pu4ProcessedLen);
            if (pBuf == NULL)
            {
                SECv6_TRC (SECv6_DATA_PATH,
                           "Secv6EspDecode :Secv6EspTransportDecode returns NULL \n");
                return NULL;
            }
            break;

        case SEC_TUNNEL:
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6EspDecode :Secv6EspTunnelDecode Entered \n");
            pBuf = Secv6EspTunnelDecode (pBuf, pSaEntry, pu4ProcessedLen);
            if (pBuf == NULL)
            {
                SECv6_TRC (SECv6_DATA_PATH,
                           "Secv6EspDecode :Secv6EspTunnelDecode returns NULL \n");
                return NULL;
            }
            break;

        default:
            SECv6_TRC1 (SECv6_DATA_PATH,
                        "Secv6EspDecode : Sec Mode Number Fails (%d)\n",
                        pSaEntry->u1SecAssocMode);
            return NULL;
    }

    switch (pSaEntry->u1SecAssocMode)
    {
        case SEC_TRANSPORT:
            SECv6_TRC (SECv6_MUST,
                       "\t\t[Esp secured pkt decoded]\tMode:Transport");
            break;
        case SEC_TUNNEL:
            SECv6_TRC (SECv6_MUST,
                       "\t\t[Esp secured pkt decoded]\tMode:Tunnel");
            break;
        default:
            break;
    }

    return pBuf;
}

/************************************************************************/
/*  Function Name : Secv6EspTunnelEncode                                  */
/*  Description   : This function gets the outgoing packet and          */
/*                : does tunnel mode encode based on SA Entry           */
/*                :                                                     */
/*  Input(s)      : pBuf - buffer contains IP packet to be secured      */
/*                : pSaEntry - Pointer to a SA Entry                    */
/*                : u4Interface : The interface on which packet arrives */
/*                :                                                     */
/*  Output(s)     : pBuf - buffer contains secured IP packet            */
/*  Return Values : NULL or SecuredBuffer                            */
/*************************************************************************/

static tBufChainHeader *
Secv6EspTunnelEncode (tBufChainHeader * pBuf, tSecv6Assoc * pSaEntry,
                      UINT4 u4Interface)
{
    unArCryptoKey       ArCryptoKey;
    UINT4               u4Size, u4AuthBufSize = 0;
    UINT1               u1Size = 0;
    UINT1              *pu1Buf = NULL;
    UINT4               au4EspHeader[SEC_ESP_HEADER];
    UINT1               au1AuthDigest[SEC_ALGO_DIGEST_SIZE];
    tIp6Hdr             Ip6Hdr;
    tIp6Addr           *pIp6SrcAddr = NULL;
    UINT1              *pu1AuthBuf = NULL;
    UINT1               au1SavedInitVector[AES_INIT_VECT_SIZE];
    UINT1               au1AesCounterBlock[IPSEC_AES_CTR_COUNTER_BLOCK_LEN];
    UINT1               au1AesCounter[IPSEC_AES_CTR_COUNTER_LEN] =
        { 0x00, 0x00, 0x00, 0x01 };
    UINT1               au1ArEncryptBuf[IPSEC_AES_BLOCK_SIZE];
    UINT4               u4AesCtrNum = 0;
    UINT1               u1Algorithm = 0;
    unUtilAlgo          UtilAlgo;

    IPSEC_MEMSET (&UtilAlgo, 0, sizeof (unUtilAlgo));
    IPSEC_MEMSET (au1AuthDigest, 0, sizeof (au1AuthDigest));

    IPSEC_MEMSET (&ArCryptoKey, 0, sizeof (unArCryptoKey));
    IPSEC_MEMSET (au1AesCounterBlock, SEC_ZERO,
                  IPSEC_AES_CTR_COUNTER_BLOCK_LEN);
    IPSEC_MEMSET (au1ArEncryptBuf, SEC_ZERO, IPSEC_AES_BLOCK_SIZE);

    /* Add Padding, Padlen and next header */
    IPSEC_MEMSET (&Ip6Hdr, 0, sizeof (tIp6Hdr));

    u4Size = IPSEC_BUF_GetValidBytes (pBuf);

    /* Calculate the no of Padding Bytes to be added
     * For DES-CBC and 3DES-CBC the Pkt  must be multiple of 8 Bytes
     * For AES the Pkt must be multiple of 16 Bytes 
     * If the encryption algorithm is NULL, the data has to be aligned
     * by 4 octet boundary */
    if ((pSaEntry->u1SecAssocEspAlgo == SEC_DES_CBC) ||
        (pSaEntry->u1SecAssocEspAlgo == SEC_3DES_CBC))
    {
        u1Size = (UINT1) ((u4Size + SEC_ESP_TRAILER_LEN) %
                          SEC_ESP_MULTIPLY_FACTOR);

        if (u1Size != 0)
        {
            u1Size = (UINT1) (SEC_ESP_MULTIPLY_FACTOR - u1Size);
        }

    }
    else if ((pSaEntry->u1SecAssocEspAlgo == SEC_AES)
             || (pSaEntry->u1SecAssocEspAlgo == SEC_AESCTR)
             || (pSaEntry->u1SecAssocEspAlgo == SEC_AESCTR192)
             || (pSaEntry->u1SecAssocEspAlgo == SEC_AESCTR256))
    {

        u1Size = (UINT1) ((u4Size + SEC_ESP_TRAILER_LEN) %
                          (2 * SEC_ESP_MULTIPLY_FACTOR));

        if (u1Size != 0)
        {
            u1Size = (UINT1) ((2 * SEC_ESP_MULTIPLY_FACTOR) - u1Size);
        }

    }
    else if (pSaEntry->u1SecAssocEspAlgo == SEC_NULLESPALGO)
    {
        /* If the encryption algorithm is NULL, the data has to be aligned 
         * by 4 octet boundary */
        u1Size = (UINT1) ((u4Size + SEC_ESP_TRAILER_LEN) %
                          SEC_ESP_NULL_MULTIPLY_FACTOR);

        if (u1Size != 0)
        {
            u1Size = (UINT1) (SEC_ESP_NULL_MULTIPLY_FACTOR - u1Size);
        }
    }

    pu1Buf = IPSECv6_MALLOC ((size_t) (u1Size + SEC_ESP_TRAILER_LEN), UINT1);

    if (pu1Buf == NULL)
    {
        SECv6_TRC (BUFFER_TRC,
                   "Secv6EspTunnelEncode : Malloc Fails for pu1Buf \n");
        return NULL;
    }

    IPSEC_MEMSET (pu1Buf, 0, (size_t) (u1Size + SEC_ESP_TRAILER_LEN));

    pu1Buf[u1Size] = u1Size;
    pu1Buf[u1Size + 1] = SEC_IP6_ENCAP;

    if (IPSEC_COPY_OVER_BUF_AT_END
        (pBuf, pu1Buf, u4Size,
         (UINT4) (u1Size + SEC_ESP_TRAILER_LEN)) == BUF_FAILURE)
    {
        SECv6_TRC2 (SECv6_DATA_PATH,
                    "Secv6EspTunnelEncode : Error in writing to CRU offset = %d size =%d \n ",
                    u4Size, u1Size + SEC_ESP_TRAILER_LEN);
        IPSECv6_MEMFREE (pu1Buf);
        return NULL;
    }

    IPSECv6_MEMFREE (pu1Buf);
    /* Data Encryption */
    if (pSaEntry->u1SecAssocEspAlgo != SEC_NULLESPALGO)
    {
        /* Extract the packet length */
        u4Size = IPSEC_BUF_GetValidBytes (pBuf);
        if ((pSaEntry->u1SecAssocEspAlgo == SEC_DES_CBC) ||
            (pSaEntry->u1SecAssocEspAlgo == SEC_3DES_CBC))
        {
            /* Allocate buffer for passing to the DES algorithm */
            pu1Buf = IPSECv6_MALLOC (u4Size, UINT1);

            if (pu1Buf == NULL)
            {
                return (NULL);
            }

            /* Copy the buffer contents to the linear buffer */
            IPSEC_COPY_FROM_BUF (pBuf, pu1Buf, 0, u4Size);

            if (pSaEntry->u1SecAssocEspAlgo == SEC_DES_CBC)
            {
                IPSEC_MEMCPY (au1SavedInitVector,
                              pSaEntry->pu1SecAssocInitVector, DES_IV_SIZE);
                IPSEC_MEMCPY (ArCryptoKey.tArDes.au8ArSubkey,
                              pSaEntry->au8SubKey,
                              sizeof (pSaEntry->au8SubKey));

                if (gu1Secv6BypassCrypto != BYPASS_ENABLED)
                {
                    UtilAlgo.UtilDesAlgo.i4DesEnc = ISS_UTIL_ENCRYPT;
                    UtilAlgo.UtilDesAlgo.pu1DesInBuffer = pu1Buf;
                    UtilAlgo.UtilDesAlgo.u4DesInBufSize = u4Size;
                    UtilAlgo.UtilDesAlgo.u4DesInitVectLen = DES_IV_SIZE;
                    UtilAlgo.UtilDesAlgo.pu1DesInitVect =
                        pSaEntry->pu1SecAssocInitVector;
                    UtilAlgo.UtilDesAlgo.u4DesInKeyLen =
                        pSaEntry->u1SecAssocEspKeyLength;
                    UtilAlgo.UtilDesAlgo.pu1DesInUserKey =
                        pSaEntry->pu1SecAssocEspKey;
                    UtilAlgo.UtilDesAlgo.punDesArCryptoKey = &ArCryptoKey;

                    if (UtilEncrypt (ISS_UTIL_ALGO_DES_CBC, &UtilAlgo)
                        == OSIX_FAILURE)
                    {
                        SECv6_TRC (SECv6_DATA_PATH, "DES Encrypt Failed\n");
                        IPSECv6_MEMFREE (pu1Buf);
                        return NULL;
                    }
                }
            }
            else if (pSaEntry->u1SecAssocEspAlgo == SEC_3DES_CBC)
            {
                IPSEC_MEMCPY (au1SavedInitVector,
                              pSaEntry->pu1SecAssocInitVector, DES_IV_SIZE);
                IPSEC_MEMCPY (ArCryptoKey.tArTdes.ArKey0.au8ArSubkey,
                              pSaEntry->au8SubKey,
                              sizeof (pSaEntry->au8SubKey));
                IPSEC_MEMCPY (ArCryptoKey.tArTdes.ArKey1.au8ArSubkey,
                              pSaEntry->au8SubKey2,
                              sizeof (pSaEntry->au8SubKey2));
                IPSEC_MEMCPY (ArCryptoKey.tArTdes.ArKey2.au8ArSubkey,
                              pSaEntry->au8SubKey3,
                              sizeof (pSaEntry->au8SubKey3));

                if (gu1Secv6BypassCrypto != BYPASS_ENABLED)
                {
                    UtilAlgo.UtilDesAlgo.i4DesEnc = ISS_UTIL_ENCRYPT;
                    UtilAlgo.UtilDesAlgo.pu1DesInBuffer = pu1Buf;
                    UtilAlgo.UtilDesAlgo.u4DesInBufSize = u4Size;
                    UtilAlgo.UtilDesAlgo.u4DesInitVectLen = DES_IV_SIZE;
                    UtilAlgo.UtilDesAlgo.pu1DesInitVect =
                        pSaEntry->pu1SecAssocInitVector;
                    UtilAlgo.UtilDesAlgo.u4DesInKeyLen =
                        pSaEntry->u1SecAssocEspKeyLength;
                    UtilAlgo.UtilDesAlgo.pu1DesInUserKey =
                        pSaEntry->pu1SecAssocEspKey;
                    UtilAlgo.UtilDesAlgo.pu1DesInUserKey2 =
                        pSaEntry->pu1SecAssocEspKey2;
                    UtilAlgo.UtilDesAlgo.pu1DesInUserKey3 =
                        pSaEntry->pu1SecAssocEspKey3;
                    UtilAlgo.UtilDesAlgo.punDesArCryptoKey = &ArCryptoKey;

                    if (UtilEncrypt (ISS_UTIL_ALGO_TDES_CBC, &UtilAlgo)
                        == OSIX_FAILURE)
                    {
                        SECv6_TRC (SECv6_DATA_PATH, "TDES Encrypt Failed\n");
                        IPSECv6_MEMFREE (pu1Buf);
                        return NULL;
                    }
                }
            }
            if (IPSEC_COPY_OVER_BUF (pBuf, pu1Buf, 0, u4Size) == BUF_FAILURE)
            {
                IPSECv6_MEMFREE (pu1Buf);
                return NULL;
            }

            IPSECv6_MEMFREE (pu1Buf);

            if (IPSEC_BUF_Prepend
                (pBuf, au1SavedInitVector, DES_IV_SIZE) == BUF_FAILURE)
            {
                return NULL;
            }
        }
        else if (pSaEntry->u1SecAssocEspAlgo == SEC_AES)
        {
            /* Allocate buffer for passing to the AES algorithm */

            pu1Buf = IPSECv6_MALLOC (u4Size, UINT1);

            if (pu1Buf == NULL)
            {
                return (NULL);
            }

            /* Copy the buffer contents to the linear buffer */
            IPSEC_COPY_FROM_BUF (pBuf, pu1Buf, 0, u4Size);
            IPSEC_MEMCPY (au1SavedInitVector,
                          pSaEntry->pu1SecAssocInitVector, AES_INIT_VECT_SIZE);

            if (gu1Secv6BypassCrypto != BYPASS_ENABLED)
            {
                UtilAlgo.UtilAesAlgo.i4AesEnc = ISS_UTIL_ENCRYPT;
                UtilAlgo.UtilAesAlgo.u4AesInBufSize = u4Size;
                UtilAlgo.UtilAesAlgo.pu1AesInBuf = pu1Buf;
                UtilAlgo.UtilAesAlgo.u4AesInKeyLen =
                    (UINT4) (pSaEntry->u1SecAssocEspKeyLength *
                             IPSEC_AES_KEY_CONV_FACTOR);
                UtilAlgo.UtilAesAlgo.pu1AesInScheduleKey =
                    pSaEntry->au1AesEncrKey;
                UtilAlgo.UtilAesAlgo.pu1AesInUserKey =
                    pSaEntry->pu1SecAssocEspKey;
                UtilAlgo.UtilAesAlgo.u4AesInitVectLen = AES_INIT_VECT_SIZE;
                UtilAlgo.UtilAesAlgo.pu1AesInitVector =
                    pSaEntry->pu1SecAssocInitVector;

                if (UtilEncrypt (ISS_UTIL_ALGO_AES_CBC, &UtilAlgo)
                    == OSIX_FAILURE)
                {
                    SECv6_TRC (SECv6_DATA_PATH, "AESCbcEncrypt Fails\n");
                    IPSECv6_MEMFREE (pu1Buf);
                    return NULL;
                }
            }

            if (IPSEC_COPY_OVER_BUF (pBuf, pu1Buf, 0, u4Size) == BUF_FAILURE)
            {
                IPSECv6_MEMFREE (pu1Buf);
                return NULL;
            }
            IPSECv6_MEMFREE (pu1Buf);

            if (IPSEC_BUF_Prepend
                (pBuf, au1SavedInitVector, AES_INIT_VECT_SIZE) == BUF_FAILURE)
            {
                return NULL;
            }

        }

        else if ((pSaEntry->u1SecAssocEspAlgo == SEC_AESCTR)
                 || (pSaEntry->u1SecAssocEspAlgo == SEC_AESCTR192)
                 || (pSaEntry->u1SecAssocEspAlgo == SEC_AESCTR256))
        {

            /* Allocate memory to linear buffer for passing to the
             *                AES algorithm */

            pu1Buf = IPSECv6_MALLOC (u4Size, UINT1);

            if (pu1Buf == NULL)
            {
                return (NULL);
            }
            /* Copy the buffer contents to the linear buffer */

            IPSEC_COPY_FROM_BUF (pBuf, pu1Buf, 0, u4Size);
            IPSEC_MEMCPY (au1SavedInitVector,
                          pSaEntry->pu1SecAssocInitVector,
                          IPSEC_AES_CTR_INIT_VECT_SIZE);

            u4AesCtrNum = 0;
            MEMCPY (au1AesCounterBlock, pSaEntry->au1Nonce,
                    IPSEC_AES_CTR_NONCE_LEN);
            MEMCPY (au1AesCounterBlock + IPSEC_AES_CTR_NONCE_LEN,
                    au1SavedInitVector, IPSEC_AES_CTR_INIT_VECT_SIZE);
            MEMCPY (au1AesCounterBlock + IPSEC_AES_CTR_NONCE_LEN +
                    IPSEC_AES_CTR_INIT_VECT_SIZE, au1AesCounter,
                    IPSEC_AES_CTR_COUNTER_LEN);
            AesArSetEncryptKey (pSaEntry->au1AesEncrKey,
                                (UINT2) ((pSaEntry->u1SecAssocEspKeyLength) *
                                         IPSEC_AES_KEY_CONV_FACTOR),
                                &ArCryptoKey);

            if (gu1Secv6BypassCrypto != BYPASS_ENABLED)
            {
                UtilAlgo.UtilAesAlgo.i4AesEnc = ISS_UTIL_ENCRYPT;
                UtilAlgo.UtilAesAlgo.u4AesInBufSize = u4Size;
                UtilAlgo.UtilAesAlgo.pu1AesInBuf = pu1Buf;
                UtilAlgo.UtilAesAlgo.pu1AesOutBuf = pu1Buf;
                UtilAlgo.UtilAesAlgo.punAesArCryptoKey = &ArCryptoKey;
                UtilAlgo.UtilAesAlgo.u4AesInKeyLen =
                    (UINT4) (pSaEntry->u1SecAssocEspKeyLength *
                             IPSEC_AES_KEY_CONV_FACTOR);
                UtilAlgo.UtilAesAlgo.pu1AesInUserKey =
                    pSaEntry->pu1SecAssocEspKey;
                UtilAlgo.UtilAesAlgo.pu1AesInitVector = au1AesCounterBlock;
                UtilAlgo.UtilAesAlgo.u4AesInitVectLen =
                    IPSEC_AES_CTR_INIT_VECT_SIZE;
                UtilAlgo.UtilAesAlgo.pu1AesEncryptBuf = au1ArEncryptBuf;
                UtilAlgo.UtilAesAlgo.pu4AesNum = &u4AesCtrNum;

                if (UtilEncrypt (ISS_UTIL_ALGO_AES_CTR_MODE, &UtilAlgo)
                    == OSIX_FAILURE)
                {
                    SECv6_TRC (SECv6_DATA_PATH, "AES-CTR Encrypt Fails\n");
                    IPSECv6_MEMFREE (pu1Buf);
                    return NULL;
                }
            }

            if (IPSEC_COPY_OVER_BUF (pBuf, pu1Buf, 0, u4Size) == BUF_FAILURE)
            {
                IPSECv6_MEMFREE (pu1Buf);
                return NULL;
            }
            IPSECv6_MEMFREE (pu1Buf);

            if (IPSEC_BUF_Prepend
                (pBuf, au1SavedInitVector,
                 IPSEC_AES_CTR_INIT_VECT_SIZE) == BUF_FAILURE)
            {
                return NULL;
            }

        }

    }
    /* Form ESP Header and Prepend to pBuf */
    au4EspHeader[0] = IPSEC_HTONL (pSaEntry->u4SecAssocSpi);
    pSaEntry->u4SecAssocSeqNumber = pSaEntry->u4SecAssocSeqNumber + 1;
    au4EspHeader[1] = IPSEC_HTONL (pSaEntry->u4SecAssocSeqNumber);

    if (IPSEC_BUF_Prepend (pBuf, (UINT1 *) au4EspHeader,
                           SEC_ESP_HEADER_LEN) == BUF_FAILURE)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6EspTunnelEncode : Esp Header Prepend fails\n");
        return NULL;
    }

    if (pSaEntry->u1SecAssocAhAlgo != SEC_NULLAHALGO)
    {

        IPSEC_MEMSET (&UtilAlgo, 0, sizeof (unUtilAlgo));
        u4AuthBufSize = IPSEC_BUF_GetValidBytes (pBuf);
        if ((pu1AuthBuf = IPSECv6_MALLOC (u4AuthBufSize, UINT1)) == NULL)
        {

            return NULL;
        }

        if (IPSEC_COPY_FROM_BUF (pBuf, pu1AuthBuf, 0,
                                 u4AuthBufSize) == BUF_FAILURE)
        {
            IPSECv6_MEMFREE (pu1AuthBuf);
            return NULL;
        }

        switch (pSaEntry->u1SecAssocAhAlgo)
        {
                /* Generate Auth Data and append to pBuf */

            case SEC_HMACMD5:
                u1Algorithm = ISS_UTIL_ALGO_HMAC_MD5;
                break;

            case SEC_HMACSHA1:
                u1Algorithm = ISS_UTIL_ALGO_HMAC_SHA1;
                UtilAlgo.UtilHmacAlgo.HmacShaVersion = AR_SHA1_ALGO;
                break;

            case HMAC_SHA_256:
                u1Algorithm = ISS_UTIL_ALGO_HMAC_SHA2;
                UtilAlgo.UtilHmacAlgo.HmacShaVersion = AR_SHA256_ALGO;
                break;

            case HMAC_SHA_384:
                u1Algorithm = ISS_UTIL_ALGO_HMAC_SHA2;
                UtilAlgo.UtilHmacAlgo.HmacShaVersion = AR_SHA384_ALGO;
                break;

            case HMAC_SHA_512:
                u1Algorithm = ISS_UTIL_ALGO_HMAC_SHA2;
                UtilAlgo.UtilHmacAlgo.HmacShaVersion = AR_SHA512_ALGO;
                break;
            default:
                break;
        }

        switch (pSaEntry->u1SecAssocAhAlgo)
        {
            case SEC_HMACMD5:
            case SEC_HMACSHA1:
            case HMAC_SHA_256:
            case HMAC_SHA_384:
            case HMAC_SHA_512:
                UtilAlgo.UtilHmacAlgo.pu1HmacKey = pSaEntry->pu1SecAssocAhKey1;
                UtilAlgo.UtilHmacAlgo.u4HmacKeyLen =
                    pSaEntry->u1SecAssocAhKeyLength;
                UtilAlgo.UtilHmacAlgo.pu1HmacPktDataPtr = pu1AuthBuf;
                UtilAlgo.UtilHmacAlgo.i4HmacPktLength = (INT4) u4AuthBufSize;
                UtilAlgo.UtilHmacAlgo.pu1HmacOutDigest = au1AuthDigest;
                UtilAlgo.UtilHmacAlgo.u4HmacOutDigestLen = SEC_AUTH_DIGEST_SIZE;
                UtilHash (u1Algorithm, &UtilAlgo);
                break;

            case SEC_XCBCMAC:
                UtilAlgo.UtilAesAlgo.pu1AesInUserKey =
                    pSaEntry->pu1SecAssocAhKey1;
                UtilAlgo.UtilAesAlgo.u4AesInKeyLen =
                    pSaEntry->u1SecAssocAhKeyLength;
                UtilAlgo.UtilAesAlgo.pu1AesInBuf = pu1AuthBuf;
                UtilAlgo.UtilAesAlgo.u4AesInBufSize = u4AuthBufSize;
                UtilAlgo.UtilAesAlgo.pu1AesMac = au1AuthDigest;
                UtilAlgo.UtilHmacAlgo.u4HmacOutDigestLen = SEC_AUTH_DIGEST_SIZE;
                UtilMac (ISS_UTIL_ALGO_AES_CBC_MAC96, &UtilAlgo);
                break;

            case SEC_MD5:
                UtilAlgo.UtilMd5Algo.pu1Md5InBuf = pu1AuthBuf;
                UtilAlgo.UtilMd5Algo.u4Md5InBufLen = u4AuthBufSize;
                UtilAlgo.UtilMd5Algo.pu1Md5OutDigest = au1AuthDigest;
                UtilAlgo.UtilHmacAlgo.u4HmacOutDigestLen = SEC_AUTH_DIGEST_SIZE;
                UtilMac (ISS_UTIL_ALGO_MD5_ALGO, &UtilAlgo);
                break;

            case SEC_KEYEDMD5:
                KeyedMd5 (pu1AuthBuf, (INT4) u4AuthBufSize,
                          pSaEntry->pu1SecAssocAhKey1,
                          (INT4) pSaEntry->u1SecAssocAhKeyLength,
                          au1AuthDigest);
                break;

            default:
                SECv6_TRC (SECv6_DATA_PATH, " Unknown Auth Algo \n");
                IPSECv6_MEMFREE (pu1AuthBuf);
                return NULL;

        }

        u1Size = SEC_AUTH_DIGEST_SIZE;

        IPSECv6_MEMFREE (pu1AuthBuf);

        u4Size = IPSEC_BUF_GetValidBytes (pBuf);

        if (IPSEC_COPY_OVER_BUF_AT_END (pBuf, au1AuthDigest,
                                        u4Size, u1Size) == BUF_FAILURE)
        {
            SECv6_TRC2 (SECv6_DATA_PATH,
                        "Secv6EspTunnelEncode : Error in writing to"
                        "offset = %d size = %d \n ", u4Size, u1Size);
            return NULL;
        }
    }

    /* Form New IP Header and Prepend to pBuf */
    u4Size = IPSEC_BUF_GetValidBytes (pBuf);
    Ip6Hdr.u4Head = IPSEC_HEAD;
    Ip6Hdr.u2Len = (UINT2) (IPSEC_HTONS (u4Size));
    Ip6Hdr.u1Nh = SEC_ESP;
    Ip6Hdr.u1Hlim = SEC_MAX_HOP_LIMIT;

    if ((pIp6SrcAddr =
         Ip6Secv6GetGlobalAddr (u4Interface, &pSaEntry->SecAssocDestAddr))
        == NULL)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6EspTunnelEncode : Error in Getting src addr for a dest addr \n");
        return NULL;
    }
    Ip6AddrCopy (&Ip6Hdr.srcAddr, pIp6SrcAddr);
    Ip6AddrCopy (&Ip6Hdr.dstAddr, &pSaEntry->SecAssocDestAddr);

    if (IPSEC_BUF_Prepend
        (pBuf, (UINT1 *) &Ip6Hdr, SEC_IPV6_HEADER_SIZE) == BUF_FAILURE)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   " Secv6EspTunnelEncode : Esp Header Prepend fails\n");
        return NULL;
    }

    return pBuf;
}

/**********************************************************************/
/*  Function Name : Secv6EspTunnelDecode                                */
/*  Description   : This function gets the incomming packet and       */
/*                  does tunnel mode decode                           */
/*                  based on SA Entry                                 */
/*  Input(s)      : pBuf - buffer contains IP Secured packet          */
/*                  pSaEntry - Pointer to a SA Entry                  */
/*  Output(s)     : pBuf - buffer contains IP packet                  */
/*  Global(s)     : None                                              */
/*  Return Values : NULL or SecuredBuffer                          */
/**********************************************************************/
static tBufChainHeader *Secv6EspTunnelDecode
    (tBufChainHeader * pBuf, tSecv6Assoc * pSaEntry, UINT4 *pu4ProcessedLen)
{
    UINT4               u4SeqNumber;
    UINT4               u4Size, u4AuthBufSize = 0;
    UINT1              *pu1AuthData = NULL, *pu1Buf = NULL;
    tIp6Hdr             Ip6Hdr;
    UINT1               u1Size;
    UINT1               au1AuthDigest[SEC_ALGO_DIGEST_SIZE];
    UINT1              *pu1AuthBuf = NULL;
    UINT1               au1InitVect[AES_INIT_VECT_SIZE];
    UINT1               au1AesCounterBlock[IPSEC_AES_CTR_COUNTER_BLOCK_LEN];
    UINT1               au1AesCounter[IPSEC_AES_CTR_COUNTER_LEN] =
        { 0x00, 0x00, 0x00, 0x01 };
    UINT1               au1ArEncryptBuf[IPSEC_AES_BLOCK_SIZE];
    UINT4               u4AesCtrNum = 0;
    unArCryptoKey       ArCryptoKey;
    UINT1               u1Algorithm = 0;
    unUtilAlgo          UtilAlgo;

    IPSEC_MEMSET (&UtilAlgo, 0, sizeof (unUtilAlgo));
    IPSEC_MEMSET (au1AuthDigest, 0, sizeof (au1AuthDigest));

    IPSEC_MEMSET (&ArCryptoKey, 0, sizeof (unArCryptoKey));
    IPSEC_MEMSET (au1AesCounterBlock, SEC_ZERO,
                  IPSEC_AES_CTR_COUNTER_BLOCK_LEN);
    IPSEC_MEMSET (au1ArEncryptBuf, SEC_ZERO, IPSEC_AES_BLOCK_SIZE);

    /* Verify Sequence Number */
    if (IPSEC_COPY_FROM_BUF (pBuf, (UINT1 *) &u4SeqNumber,
                             (*pu4ProcessedLen) + SEC_ESP_SEQ_OFFSET,
                             SEC_SEQ_NUMBER_LEN) == BUF_FAILURE)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6EspTunnelDecode : Seq Num read From Buffer Fails\n");
        return NULL;
    }

    /* Remove IP Header */
    if (IPSEC_COPY_FROM_BUF (pBuf, (UINT1 *) &Ip6Hdr,
                             SEC_IPV6_HEADER_HEAD_OFFSET,
                             (*pu4ProcessedLen)) == BUF_FAILURE)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6EspTunnelDecode : Ip6 Heade read From packet Fails\n");
        return NULL;
    }

    if (IPSEC_BUF_MoveOffset (pBuf, (*pu4ProcessedLen)) == BUF_FAILURE)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6EspTunnelDecode : Move Offset Fails to remove Ip Header\n");
        return NULL;
    }

    Ip6Hdr.u2Len = IPSEC_NTOHS (Ip6Hdr.u2Len);

    if (pSaEntry->u1SecAssocAhAlgo != SEC_NULLAHALGO)
    {
        u1Size = SEC_AUTH_DIGEST_SIZE;

        /* Read Auth data and delete */
        u4Size = IPSEC_BUF_GetValidBytes (pBuf);
        pu1AuthData = IPSECv6_MALLOC (u1Size, UINT1);

        if (pu1AuthData == NULL)
        {
            SECv6_TRC (BUFFER_TRC,
                       "Secv6EspTunnelDecode : Malloc Fails for AuthData\n");
            return NULL;
        }

        if (IPSEC_COPY_FROM_BUF
            (pBuf, pu1AuthData, u4Size - u1Size, u1Size) == BUF_FAILURE)
        {
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6EspTunnelDecode : read From Packet Fails for AuthData\n");
            IPSECv6_MEMFREE (pu1AuthData);
            return NULL;
        }

        if (IPSEC_BUF_DeleteAtEnd (pBuf, u1Size) == NULL)
        {
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6EspTunnelDecode : Move Offset at end Fails to remove AuthData\n");
            IPSECv6_MEMFREE (pu1AuthData);
            return NULL;
        }

        u4AuthBufSize = IPSEC_BUF_GetValidBytes (pBuf);

        if ((pu1AuthBuf = IPSECv6_MALLOC (u4AuthBufSize, UINT1)) == NULL)
        {

            IPSECv6_MEMFREE (pu1AuthData);
            return NULL;
        }

        if (IPSEC_COPY_FROM_BUF (pBuf, pu1AuthBuf, 0,
                                 u4AuthBufSize) == BUF_FAILURE)
        {
            IPSECv6_MEMFREE (pu1AuthBuf);
            IPSECv6_MEMFREE (pu1AuthData);
            return NULL;
        }

        /* Generate Auth Data */
        switch (pSaEntry->u1SecAssocAhAlgo)
        {

            case SEC_HMACMD5:
                u1Algorithm = ISS_UTIL_ALGO_HMAC_MD5;
                break;

            case SEC_HMACSHA1:
                u1Algorithm = ISS_UTIL_ALGO_HMAC_SHA1;
                break;

            case HMAC_SHA_256:
                u1Algorithm = ISS_UTIL_ALGO_HMAC_SHA2;
                UtilAlgo.UtilHmacAlgo.HmacShaVersion = AR_SHA256_ALGO;
                break;

            case HMAC_SHA_384:
                u1Algorithm = ISS_UTIL_ALGO_HMAC_SHA2;
                UtilAlgo.UtilHmacAlgo.HmacShaVersion = AR_SHA384_ALGO;
                break;

            case HMAC_SHA_512:
                u1Algorithm = ISS_UTIL_ALGO_HMAC_SHA2;
                UtilAlgo.UtilHmacAlgo.HmacShaVersion = AR_SHA512_ALGO;
                break;
            default:
                break;
        }

        switch (pSaEntry->u1SecAssocAhAlgo)
        {
            case SEC_HMACMD5:
            case SEC_HMACSHA1:
            case HMAC_SHA_256:
            case HMAC_SHA_384:
            case HMAC_SHA_512:
                UtilAlgo.UtilHmacAlgo.pu1HmacKey = pSaEntry->pu1SecAssocAhKey1;
                UtilAlgo.UtilHmacAlgo.u4HmacKeyLen =
                    pSaEntry->u1SecAssocAhKeyLength;
                UtilAlgo.UtilHmacAlgo.pu1HmacPktDataPtr = pu1AuthBuf;
                UtilAlgo.UtilHmacAlgo.i4HmacPktLength = (INT4) u4AuthBufSize;
                UtilAlgo.UtilHmacAlgo.pu1HmacOutDigest = au1AuthDigest;
                UtilAlgo.UtilHmacAlgo.u4HmacOutDigestLen = SEC_AUTH_DIGEST_SIZE;
                UtilHash (u1Algorithm, &UtilAlgo);
                break;

            case SEC_XCBCMAC:
                UtilAlgo.UtilAesAlgo.pu1AesInUserKey =
                    pSaEntry->pu1SecAssocAhKey1;
                UtilAlgo.UtilAesAlgo.u4AesInKeyLen =
                    pSaEntry->u1SecAssocAhKeyLength;
                UtilAlgo.UtilAesAlgo.pu1AesInBuf = pu1AuthBuf;
                UtilAlgo.UtilAesAlgo.u4AesInBufSize = u4AuthBufSize;
                UtilAlgo.UtilAesAlgo.pu1AesMac = au1AuthDigest;
                UtilAlgo.UtilHmacAlgo.u4HmacOutDigestLen = SEC_AUTH_DIGEST_SIZE;
                UtilMac (ISS_UTIL_ALGO_AES_CBC_MAC96, &UtilAlgo);
                break;

            case SEC_MD5:
                UtilAlgo.UtilMd5Algo.pu1Md5InBuf = pu1AuthBuf;
                UtilAlgo.UtilMd5Algo.u4Md5InBufLen = u4AuthBufSize;
                UtilAlgo.UtilMd5Algo.pu1Md5OutDigest = au1AuthDigest;
                UtilAlgo.UtilHmacAlgo.u4HmacOutDigestLen = SEC_AUTH_DIGEST_SIZE;
                UtilMac (ISS_UTIL_ALGO_MD5_ALGO, &UtilAlgo);
                break;

            case SEC_KEYEDMD5:
                KeyedMd5 (pu1AuthBuf, (INT4) u4AuthBufSize,
                          pSaEntry->pu1SecAssocAhKey1,
                          (UINT4) pSaEntry->u1SecAssocAhKeyLength,
                          au1AuthDigest);
                break;

            default:
                SECv6_TRC (SECv6_DATA_PATH, " Unknown Auth Algo \n");
                IPSECv6_MEMFREE (pu1AuthData);
                IPSECv6_MEMFREE (pu1AuthBuf);
                return NULL;

        }

        IPSECv6_MEMFREE (pu1AuthBuf);

        /* Verify Authentication Data */
        if (IPSEC_MEMCMP (au1AuthDigest, pu1AuthData, u1Size) != 0)
        {
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6EspTunnelDecode : Authentication Check Fails\n");
            IPSECv6_MEMFREE (pu1AuthData);
            return NULL;
        }
        IPSECv6_MEMFREE (pu1AuthData);
    }

    /* Remove ESP Header */
    if (IPSEC_BUF_MoveOffset (pBuf, SEC_ESP_HEADER_LEN) == BUF_FAILURE)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6EspTunnelDecode : Move Valid Offset Fails to remove ESP\n");
        return NULL;
    }

    /* Decrypt the data */
    if (pSaEntry->u1SecAssocEspAlgo != SEC_NULLESPALGO)
    {
        IPSEC_MEMSET (&UtilAlgo, 0, sizeof (unUtilAlgo));

        if ((pSaEntry->u1SecAssocEspAlgo == SEC_DES_CBC) ||
            (pSaEntry->u1SecAssocEspAlgo == SEC_3DES_CBC))
        {
            if (IPSEC_COPY_FROM_BUF (pBuf, au1InitVect, 0, DES_IV_SIZE) ==
                BUF_FAILURE)
            {
                return NULL;
            }

            if (IPSEC_BUF_MoveOffset (pBuf, DES_IV_SIZE) == BUF_FAILURE)
            {
                return NULL;
            }

            u4Size = IPSEC_BUF_GetValidBytes (pBuf);

            /* Allocate buffer for passing to the DES algorithm */
            pu1Buf = IPSECv6_MALLOC (u4Size, UINT1);

            if (pu1Buf == NULL)
            {
                SECv6_PRINT_MEM_ALLOC_FAILURE;
                return NULL;
            }

            /* Copy the buffer contents to the linear buffer */
            IPSEC_COPY_FROM_BUF (pBuf, pu1Buf, 0, u4Size);

            if (pSaEntry->u1SecAssocEspAlgo == SEC_DES_CBC)
            {
                IPSEC_MEMCPY (ArCryptoKey.tArDes.au8ArSubkey,
                              pSaEntry->au8SubKey,
                              sizeof (ArCryptoKey.tArDes.au8ArSubkey));

                if (gu1Secv6BypassCrypto != BYPASS_ENABLED)
                {
                    UtilAlgo.UtilDesAlgo.i4DesEnc = ISS_UTIL_DECRYPT;
                    UtilAlgo.UtilDesAlgo.pu1DesInBuffer = pu1Buf;
                    UtilAlgo.UtilDesAlgo.u4DesInBufSize = u4Size;
                    UtilAlgo.UtilDesAlgo.u4DesInitVectLen = DES_IV_SIZE;
                    UtilAlgo.UtilDesAlgo.pu1DesInitVect = au1InitVect;
                    UtilAlgo.UtilDesAlgo.u4DesInKeyLen =
                        pSaEntry->u1SecAssocEspKeyLength;
                    UtilAlgo.UtilDesAlgo.pu1DesInUserKey =
                        pSaEntry->pu1SecAssocEspKey;
                    UtilAlgo.UtilDesAlgo.punDesArCryptoKey = &ArCryptoKey;

                    if (UtilDecrypt (ISS_UTIL_ALGO_DES_CBC, &UtilAlgo)
                        == OSIX_FAILURE)
                    {
                        SECv6_TRC (SECv6_DATA_PATH, "DESEDecrypt Fails\n");
                        IPSECv6_MEMFREE (pu1Buf);
                        return NULL;
                    }
                }
            }
            else if (pSaEntry->u1SecAssocEspAlgo == SEC_3DES_CBC)
            {
                IPSEC_MEMCPY (ArCryptoKey.tArTdes.ArKey0.au8ArSubkey,
                              pSaEntry->au8SubKey,
                              sizeof (ArCryptoKey.tArTdes.ArKey0.au8ArSubkey));
                IPSEC_MEMCPY (ArCryptoKey.tArTdes.ArKey1.au8ArSubkey,
                              pSaEntry->au8SubKey2,
                              sizeof (ArCryptoKey.tArTdes.ArKey1.au8ArSubkey));
                IPSEC_MEMCPY (ArCryptoKey.tArTdes.ArKey2.au8ArSubkey,
                              pSaEntry->au8SubKey3,
                              sizeof (ArCryptoKey.tArTdes.ArKey2.au8ArSubkey));

                if (gu1Secv6BypassCrypto != BYPASS_ENABLED)
                {
                    UtilAlgo.UtilDesAlgo.i4DesEnc = ISS_UTIL_DECRYPT;
                    UtilAlgo.UtilDesAlgo.pu1DesInBuffer = pu1Buf;
                    UtilAlgo.UtilDesAlgo.u4DesInBufSize = u4Size;
                    UtilAlgo.UtilDesAlgo.u4DesInitVectLen = DES_IV_SIZE;
                    UtilAlgo.UtilDesAlgo.pu1DesInitVect = au1InitVect;
                    UtilAlgo.UtilDesAlgo.u4DesInKeyLen =
                        pSaEntry->u1SecAssocEspKeyLength;
                    UtilAlgo.UtilDesAlgo.pu1DesInUserKey =
                        pSaEntry->pu1SecAssocEspKey;
                    UtilAlgo.UtilDesAlgo.pu1DesInUserKey2 =
                        pSaEntry->pu1SecAssocEspKey2;
                    UtilAlgo.UtilDesAlgo.pu1DesInUserKey3 =
                        pSaEntry->pu1SecAssocEspKey3;
                    UtilAlgo.UtilDesAlgo.punDesArCryptoKey = &ArCryptoKey;

                    if (UtilDecrypt (ISS_UTIL_ALGO_TDES_CBC, &UtilAlgo)
                        == OSIX_FAILURE)
                    {
                        SECv6_TRC (SECv6_DATA_PATH, "TDES Decrypt Fails\n");
                        IPSECv6_MEMFREE (pu1Buf);
                        return NULL;
                    }
                }
            }
            if (IPSEC_COPY_OVER_BUF (pBuf, pu1Buf, 0, u4Size) == BUF_FAILURE)
            {
                IPSECv6_MEMFREE (pu1Buf);
                return NULL;
            }

            IPSECv6_MEMFREE (pu1Buf);
        }
        else if (pSaEntry->u1SecAssocEspAlgo == SEC_AES)
        {
            if (IPSEC_COPY_FROM_BUF (pBuf, au1InitVect, 0, AES_INIT_VECT_SIZE)
                == BUF_FAILURE)
            {
                return NULL;
            }

            if (IPSEC_BUF_MoveOffset (pBuf, AES_INIT_VECT_SIZE) == BUF_FAILURE)
            {
                return NULL;
            }

            u4Size = IPSEC_BUF_GetValidBytes (pBuf);

            /* Allocate buffer for passing to the AES algorithm */
            pu1Buf = IPSECv6_MALLOC (u4Size, UINT1);

            if (pu1Buf == NULL)
            {
                SECv6_PRINT_MEM_ALLOC_FAILURE;
                return NULL;
            }

            /* Copy the buffer contents to the linear buffer */
            IPSEC_COPY_FROM_BUF (pBuf, pu1Buf, 0, u4Size);

            if (gu1Secv6BypassCrypto != BYPASS_ENABLED)
            {
                UtilAlgo.UtilAesAlgo.i4AesEnc = ISS_UTIL_DECRYPT;
                UtilAlgo.UtilAesAlgo.u4AesInBufSize = u4Size;
                UtilAlgo.UtilAesAlgo.pu1AesInBuf = pu1Buf;
                UtilAlgo.UtilAesAlgo.u4AesInKeyLen =
                    (UINT4) (pSaEntry->u1SecAssocEspKeyLength *
                             IPSEC_AES_KEY_CONV_FACTOR);
                UtilAlgo.UtilAesAlgo.pu1AesInScheduleKey =
                    pSaEntry->au1AesEncrKey;
                UtilAlgo.UtilAesAlgo.pu1AesInUserKey =
                    pSaEntry->pu1SecAssocEspKey;
                UtilAlgo.UtilAesAlgo.u4AesInitVectLen = AES_INIT_VECT_SIZE;
                UtilAlgo.UtilAesAlgo.pu1AesInitVector = au1InitVect;

                if (UtilDecrypt (ISS_UTIL_ALGO_AES_CBC, &UtilAlgo)
                    == OSIX_FAILURE)
                {
                    SECv6_TRC (SECv6_DATA_PATH, "AESCbcEncrypt Fails\n");
                    IPSECv6_MEMFREE (pu1Buf);
                    return NULL;
                }
            }

            if (IPSEC_COPY_OVER_BUF (pBuf, pu1Buf, 0, u4Size) == BUF_FAILURE)
            {
                IPSECv6_MEMFREE (pu1Buf);
                return NULL;
            }

            IPSECv6_MEMFREE (pu1Buf);
        }
        else if ((pSaEntry->u1SecAssocEspAlgo == SEC_AESCTR)
                 || (pSaEntry->u1SecAssocEspAlgo == SEC_AESCTR192)
                 || (pSaEntry->u1SecAssocEspAlgo == SEC_AESCTR256))
        {

            if (IPSEC_COPY_FROM_BUF
                (pBuf, au1InitVect, 0,
                 IPSEC_AES_CTR_INIT_VECT_SIZE) == BUF_FAILURE)
            {
                return NULL;
            }

            if (IPSEC_BUF_MoveOffset (pBuf, IPSEC_AES_CTR_INIT_VECT_SIZE) ==
                BUF_FAILURE)
            {
                return NULL;
            }

            u4Size = IPSEC_BUF_GetValidBytes (pBuf);

            /* Allocate buffer for passing to the AES algorithm */
            pu1Buf = IPSECv6_MALLOC (u4Size, UINT1);

            if (pu1Buf == NULL)
            {
                SECv6_PRINT_MEM_ALLOC_FAILURE;
                return NULL;
            }

            IPSEC_COPY_FROM_BUF (pBuf, pu1Buf, 0, u4Size);

            u4AesCtrNum = 0;
            MEMCPY (au1AesCounterBlock, pSaEntry->au1Nonce,
                    IPSEC_AES_CTR_NONCE_LEN);
            MEMCPY (au1AesCounterBlock + IPSEC_AES_CTR_NONCE_LEN, au1InitVect,
                    IPSEC_AES_CTR_INIT_VECT_SIZE);
            MEMCPY (au1AesCounterBlock + IPSEC_AES_CTR_NONCE_LEN +
                    IPSEC_AES_CTR_INIT_VECT_SIZE, au1AesCounter,
                    IPSEC_AES_CTR_COUNTER_LEN);
            AesArSetEncryptKey (pSaEntry->au1AesDecrKey,
                                (UINT2) ((pSaEntry->u1SecAssocEspKeyLength) *
                                         IPSEC_AES_KEY_CONV_FACTOR),
                                &ArCryptoKey);

            if (gu1Secv6BypassCrypto != BYPASS_ENABLED)
            {
                UtilAlgo.UtilAesAlgo.i4AesEnc = ISS_UTIL_DECRYPT;
                UtilAlgo.UtilAesAlgo.u4AesInBufSize = u4Size;
                UtilAlgo.UtilAesAlgo.pu1AesInBuf = pu1Buf;
                UtilAlgo.UtilAesAlgo.pu1AesOutBuf = pu1Buf;
                UtilAlgo.UtilAesAlgo.punAesArCryptoKey = &ArCryptoKey;
                UtilAlgo.UtilAesAlgo.u4AesInKeyLen =
                    (UINT4) (pSaEntry->u1SecAssocEspKeyLength *
                             IPSEC_AES_KEY_CONV_FACTOR);
                UtilAlgo.UtilAesAlgo.pu1AesInUserKey =
                    pSaEntry->pu1SecAssocEspKey;
                UtilAlgo.UtilAesAlgo.pu1AesInitVector = au1AesCounterBlock;
                UtilAlgo.UtilAesAlgo.u4AesInitVectLen =
                    IPSEC_AES_CTR_INIT_VECT_SIZE;
                UtilAlgo.UtilAesAlgo.pu1AesEncryptBuf = au1ArEncryptBuf;
                UtilAlgo.UtilAesAlgo.pu4AesNum = &u4AesCtrNum;

                if (UtilDecrypt (ISS_UTIL_ALGO_AES_CTR_MODE, &UtilAlgo)
                    == OSIX_FAILURE)
                {
                    SECv6_TRC (SECv6_DATA_PATH, "AES-CTR Decrypt Fails\n");
                    IPSECv6_MEMFREE (pu1Buf);
                    return NULL;
                }
            }

            if (IPSEC_COPY_OVER_BUF (pBuf, pu1Buf, 0, u4Size) == BUF_FAILURE)
            {
                IPSECv6_MEMFREE (pu1Buf);
                return NULL;
            }

            IPSECv6_MEMFREE (pu1Buf);
        }

    }
    /* Remove Padding bytes */

    u4Size = IPSEC_BUF_GetValidBytes (pBuf);
    if (IPSEC_COPY_FROM_BUF
        (pBuf, &u1Size,
         u4Size - SEC_ESP_TRAILER_LEN, SEC_ESP_TRAILER_SIZE_LEN) == BUF_FAILURE)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6EspTunnelDecode : Read Padding Length From Packet Fails\n");
        return NULL;
    }

    /* Padding for AES the Pkt must be multiple of 16 Bytes */
    if ((pSaEntry->u1SecAssocEspAlgo == SEC_AES)
        || (pSaEntry->u1SecAssocEspAlgo == SEC_AESCTR)
        || (pSaEntry->u1SecAssocEspAlgo == SEC_AESCTR192)
        || (pSaEntry->u1SecAssocEspAlgo == SEC_AESCTR256))
    {
        if (u1Size > (2 * SEC_ESP_MULTIPLY_FACTOR))
        {
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6EspTunnelDecode : Error in data after Decryption");
            return NULL;
        }
    }
    else
    {
        if (u1Size > SEC_ESP_MULTIPLY_FACTOR)
        {
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6EspTunnelDecode : Error in data after Decryption\n");
            return NULL;
        }
    }

    if (IPSEC_BUF_DeleteAtEnd (pBuf, (UINT4) (u1Size + SEC_ESP_TRAILER_LEN)) ==
        NULL)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6EspTunnelDecode : Move Valid Offset at end "
                   "Fails to remove Padding Bytes \n ");
        return NULL;
    }
    *pu4ProcessedLen = SEC_IPV6_HEADER_SIZE;
    if (pSaEntry->u1AntiReplayStatus == SEC_ANTI_REPLAY_ENABLE)
    {
        if (Secv6SeqNumberVerification
            (OSIX_NTOHL (u4SeqNumber), pSaEntry) == SEC_FAILURE)
        {
            SECv6_TRC1 (SECv6_DATA_PATH,
                        " Secv6EspTunnelDecode:Replay attack from %s\n",
                        Ip6PrintAddr (&Ip6Hdr.srcAddr));
            return NULL;
        }
    }

    return pBuf;
}

/**************************************************************************/
/*  Function Name : Secv6EspTransportEncode                              */
/*  Description   : This function gets the outgoing packet and           */
/*                : does transport mode encode based on SA entry         */
/*                :                                                      */
/*  Input(s)      : pBuf - buffer contains IP packet                     */
/*                : pSaEntry - Pointer to a SA Entry                     */
/*  Output(s)     : pBuf - buffer contains Secured IP packet             */
/*  Return Values : NULL or SecuredBuffer                             */
/************************************************************************/
static
    tBufChainHeader *Secv6EspTransportEncode
    (tBufChainHeader * pBuf, tSecv6Assoc * pSaEntry, UINT4 u4IfIndex)
{
    unArCryptoKey       ArCryptoKey;
    UINT4               au4EspHeader[SEC_ESP_HEADER];
    UINT4               u4Size, u4AuthBufSize = 0;
    UINT1               u1Size = 0;
    UINT2               u2Len;
    UINT1              *pu1Buf = NULL, *pu1OptBuf = NULL;
    tIp6Hdr             Ip6Hdr;
    UINT1               au1AuthDigest[SEC_ALGO_DIGEST_SIZE];
    UINT1              *pu1AuthBuf = NULL;
    UINT1               u1NxtHdr, u1OptLen = 0;
    UINT1               au1SavedInitVector[AES_INIT_VECT_SIZE];
    UINT4               u4AesCtrNum = 0;
    UINT1               au1AesCounterBlock[IPSEC_AES_CTR_COUNTER_BLOCK_LEN];
    UINT1               au1AesCounter[IPSEC_AES_CTR_COUNTER_LEN] =
        { 0x00, 0x00, 0x00, 0x01 };
    UINT1               au1ArEncryptBuf[IPSEC_AES_BLOCK_SIZE];
    UINT1               u1Algorithm = 0;
    unUtilAlgo          UtilAlgo;

    IPSEC_MEMSET (&UtilAlgo, 0, sizeof (unUtilAlgo));
    IPSEC_MEMSET (au1AuthDigest, 0, sizeof (au1AuthDigest));

    IPSEC_MEMSET (&ArCryptoKey, 0, sizeof (unArCryptoKey));
    IPSEC_MEMSET (au1AesCounterBlock, SEC_ZERO,
                  IPSEC_AES_CTR_COUNTER_BLOCK_LEN);
    IPSEC_MEMSET (au1ArEncryptBuf, SEC_ZERO, IPSEC_AES_BLOCK_SIZE);
    if (IPSEC_COPY_FROM_BUF (pBuf, (UINT1 *) &Ip6Hdr,
                             SEC_IPV6_HEADER_HEAD_OFFSET,
                             SEC_IPV6_HEADER_SIZE) == BUF_FAILURE)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6EspTransportEncode : Unable to read Ip Header from Packet\n");
        return NULL;
    }

    if (Ip6Secv6IsOurAddr (&(Ip6Hdr.srcAddr), &(u4IfIndex)) != IP6_SUCCESS)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6AhTransportEncode Attempting to Apply Transport Mode SA"
                   "for the System acting as a Host\n");
        return SEC_FAILURE;
    }

    /* Get the options parameters. u1NxtHdr will have higher layer protocol */
    if (Secv6GetOptionsParams (pBuf, &Ip6Hdr, SEC_ESP, &u1NxtHdr, &u1OptLen) !=
        SEC_SUCCESS)
    {
        return NULL;
    }

    if (u1OptLen > 0)
    {
        if ((pu1OptBuf =
             IPSECv6_MALLOC (((size_t) (SEC_IPV6_HEADER_SIZE + u1OptLen)),
                             UINT1)) == NULL)
        {
            return NULL;
        }

        if (IPSEC_COPY_FROM_BUF (pBuf, pu1OptBuf,
                                 SEC_IPV6_HEADER_HEAD_OFFSET,
                                 (UINT4) (SEC_IPV6_HEADER_SIZE + u1OptLen)) ==
            BUF_FAILURE)
        {
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6EspTransportEncode : Unable to read Ip Header from Packet\n");
            IPSECv6_MEMFREE (pu1OptBuf);
            return NULL;
        }
    }

    if (IPSEC_BUF_MoveOffset (pBuf, (UINT4) (SEC_IPV6_HEADER_SIZE + u1OptLen))
        == BUF_FAILURE)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6EspTransportEncode : Move Valid offset Fails to remove Ip Header \n");
        if (pu1OptBuf)
        {
            IPSECv6_MEMFREE (pu1OptBuf);
        }
        return NULL;
    }

    u4Size = IPSEC_BUF_GetValidBytes (pBuf);
    /* Calculate the size of esp trailer */
    if ((pSaEntry->u1SecAssocEspAlgo == SEC_DES_CBC) ||
        (pSaEntry->u1SecAssocEspAlgo == SEC_3DES_CBC))
    {
        u1Size = (UINT1) ((u4Size + SEC_ESP_TRAILER_LEN) %
                          SEC_ESP_MULTIPLY_FACTOR);
        if (u1Size != 0)
        {
            u1Size = (UINT1) (SEC_ESP_MULTIPLY_FACTOR - u1Size);
        }
    }
    else if ((pSaEntry->u1SecAssocEspAlgo == SEC_AES)
             || (pSaEntry->u1SecAssocEspAlgo == SEC_AESCTR)
             || (pSaEntry->u1SecAssocEspAlgo == SEC_AESCTR192)
             || (pSaEntry->u1SecAssocEspAlgo == SEC_AESCTR256))
    {
        u1Size = (UINT1) ((u4Size + SEC_ESP_TRAILER_LEN) %
                          (2 * SEC_ESP_MULTIPLY_FACTOR));
        if (u1Size != 0)
        {
            u1Size = (UINT1) ((2 * SEC_ESP_MULTIPLY_FACTOR) - u1Size);
        }
    }
    else if (pSaEntry->u1SecAssocEspAlgo == SEC_NULLESPALGO)
    {
        /* If the encryption algorithm is NULL, the data has to be aligned 
         * by 4 octet boundary */
        u1Size = (UINT1) ((u4Size + SEC_ESP_TRAILER_LEN) %
                          SEC_ESP_NULL_MULTIPLY_FACTOR);

        if (u1Size != 0)
        {
            u1Size = (UINT1) (SEC_ESP_NULL_MULTIPLY_FACTOR - u1Size);
        }
    }

    /* Allocate Memory to Linear Buffer to size of Esp Trailer */
    pu1Buf = IPSECv6_MALLOC ((size_t) (u1Size + SEC_ESP_TRAILER_LEN), UINT1);

    if (pu1Buf == NULL)
    {
        SECv6_TRC (BUFFER_TRC,
                   "Secv6EspTransportEncode : Malloc Fails for pu1Buf\n");
        if (pu1OptBuf)
        {
            IPSECv6_MEMFREE (pu1OptBuf);
        }
        return NULL;
    }

    IPSEC_MEMSET (pu1Buf, 0, (size_t) (u1Size + SEC_ESP_TRAILER_LEN));
    pu1Buf[u1Size] = u1Size;

    /* If the extention headers are present, NextHdr is Ip6Hdr points to *
     * the options field else it points to the higher layer protocol */
    if (u1OptLen == 0)
    {
        pu1Buf[u1Size + 1] = Ip6Hdr.u1Nh;
    }
    else
    {
        pu1Buf[u1Size + 1] = u1NxtHdr;
    }

    if (IPSEC_COPY_OVER_BUF_AT_END (pBuf, pu1Buf, u4Size,
                                    (UINT4) (u1Size + SEC_ESP_TRAILER_LEN)) ==
        BUF_FAILURE)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6EspTransportEncode : Unable to write ESP Trailer to Packet\n");
        if (pu1OptBuf != NULL)
        {
            IPSECv6_MEMFREE (pu1OptBuf);
        }
        IPSECv6_MEMFREE (pu1Buf);
        return NULL;
    }

    IPSECv6_MEMFREE (pu1Buf);

    /* Compute Encryption */
    if (pSaEntry->u1SecAssocEspAlgo != SEC_NULLESPALGO)
    {
        /* Extract the packet length */
        u4Size = IPSEC_BUF_GetValidBytes (pBuf);

        if ((pSaEntry->u1SecAssocEspAlgo == SEC_DES_CBC) ||
            (pSaEntry->u1SecAssocEspAlgo == SEC_3DES_CBC))
        {

            /* Allocate memory to linear buffer for passing to the 
               DES algorithm */
            pu1Buf = IPSECv6_MALLOC (u4Size, UINT1);

            if (pu1Buf == NULL)
            {
                SECv6_PRINT_MEM_ALLOC_FAILURE;
                IPSECv6_MEMFREE (pu1OptBuf);
                return (NULL);
            }

            /* Copy the buffer contents to the linear buffer */
            IPSEC_COPY_FROM_BUF (pBuf, pu1Buf, 0, u4Size);

            if (pSaEntry->u1SecAssocEspAlgo == SEC_DES_CBC)
            {
                IPSEC_MEMCPY (au1SavedInitVector,
                              pSaEntry->pu1SecAssocInitVector, DES_IV_SIZE);
                IPSEC_MEMCPY (ArCryptoKey.tArDes.au8ArSubkey,
                              pSaEntry->au8SubKey,
                              sizeof (pSaEntry->au8SubKey));

                if (gu1Secv6BypassCrypto != BYPASS_ENABLED)
                {
                    UtilAlgo.UtilDesAlgo.i4DesEnc = ISS_UTIL_ENCRYPT;
                    UtilAlgo.UtilDesAlgo.pu1DesInBuffer = pu1Buf;
                    UtilAlgo.UtilDesAlgo.u4DesInBufSize = u4Size;
                    UtilAlgo.UtilDesAlgo.u4DesInitVectLen = DES_IV_SIZE;
                    UtilAlgo.UtilDesAlgo.pu1DesInitVect =
                        pSaEntry->pu1SecAssocInitVector;
                    UtilAlgo.UtilDesAlgo.u4DesInKeyLen =
                        pSaEntry->u1SecAssocEspKeyLength;
                    UtilAlgo.UtilDesAlgo.pu1DesInUserKey =
                        pSaEntry->pu1SecAssocEspKey;
                    UtilAlgo.UtilDesAlgo.punDesArCryptoKey = &ArCryptoKey;

                    if (UtilEncrypt (ISS_UTIL_ALGO_DES_CBC, &UtilAlgo)
                        == OSIX_FAILURE)
                    {
                        SECv6_TRC (SECv6_DATA_PATH, "DES Encrypt Failed\n");
                        IPSECv6_MEMFREE (pu1Buf);
                        IPSECv6_MEMFREE (pu1OptBuf);
                        return NULL;
                    }
                }
            }
            else if (pSaEntry->u1SecAssocEspAlgo == SEC_3DES_CBC)
            {

                IPSEC_MEMCPY (au1SavedInitVector,
                              pSaEntry->pu1SecAssocInitVector, DES_IV_SIZE);
                IPSEC_MEMCPY (ArCryptoKey.tArTdes.ArKey0.au8ArSubkey,
                              pSaEntry->au8SubKey,
                              sizeof (pSaEntry->au8SubKey));
                IPSEC_MEMCPY (ArCryptoKey.tArTdes.ArKey1.au8ArSubkey,
                              pSaEntry->au8SubKey2,
                              sizeof (pSaEntry->au8SubKey2));
                IPSEC_MEMCPY (ArCryptoKey.tArTdes.ArKey2.au8ArSubkey,
                              pSaEntry->au8SubKey3,
                              sizeof (pSaEntry->au8SubKey3));

                if (gu1Secv6BypassCrypto != BYPASS_ENABLED)
                {
                    UtilAlgo.UtilDesAlgo.i4DesEnc = ISS_UTIL_ENCRYPT;
                    UtilAlgo.UtilDesAlgo.pu1DesInBuffer = pu1Buf;
                    UtilAlgo.UtilDesAlgo.u4DesInBufSize = u4Size;
                    UtilAlgo.UtilDesAlgo.u4DesInitVectLen = DES_IV_SIZE;
                    UtilAlgo.UtilDesAlgo.pu1DesInitVect =
                        pSaEntry->pu1SecAssocInitVector;
                    UtilAlgo.UtilDesAlgo.u4DesInKeyLen =
                        pSaEntry->u1SecAssocEspKeyLength;
                    UtilAlgo.UtilDesAlgo.pu1DesInUserKey =
                        pSaEntry->pu1SecAssocEspKey;
                    UtilAlgo.UtilDesAlgo.pu1DesInUserKey2 =
                        pSaEntry->pu1SecAssocEspKey2;
                    UtilAlgo.UtilDesAlgo.pu1DesInUserKey3 =
                        pSaEntry->pu1SecAssocEspKey3;
                    UtilAlgo.UtilDesAlgo.punDesArCryptoKey = &ArCryptoKey;

                    if (UtilEncrypt (ISS_UTIL_ALGO_TDES_CBC, &UtilAlgo)
                        == OSIX_FAILURE)
                    {
                        SECv6_TRC (SECv6_DATA_PATH, "TDES Encrypt Failed\n");
                        IPSECv6_MEMFREE (pu1Buf);
                        IPSECv6_MEMFREE (pu1OptBuf);
                        return NULL;
                    }
                }

            }

            if (IPSEC_COPY_OVER_BUF (pBuf, au1SavedInitVector,
                                     0, DES_IV_SIZE) == BUF_FAILURE)
            {
                IPSECv6_MEMFREE (pu1Buf);
                IPSECv6_MEMFREE (pu1OptBuf);
                return NULL;
            }

            if (IPSEC_COPY_OVER_BUF (pBuf, pu1Buf, DES_IV_SIZE, u4Size) ==
                BUF_FAILURE)
            {
                IPSECv6_MEMFREE (pu1Buf);
                IPSECv6_MEMFREE (pu1OptBuf);
                return NULL;
            }

            IPSECv6_MEMFREE (pu1Buf);
        }
        else if (pSaEntry->u1SecAssocEspAlgo == SEC_AES)
        {

            /* Allocate memory to linear buffer for passing to the 
               AES algorithm */

            pu1Buf = IPSECv6_MALLOC (u4Size, UINT1);

            if (pu1Buf == NULL)
            {
                SECv6_PRINT_MEM_ALLOC_FAILURE;
                IPSECv6_MEMFREE (pu1OptBuf);
                return (NULL);
            }
            IPSEC_MEMCPY (au1SavedInitVector,
                          pSaEntry->pu1SecAssocInitVector, AES_INIT_VECT_SIZE);

            /* Copy the buffer contents to the linear buffer */
            IPSEC_COPY_FROM_BUF (pBuf, pu1Buf, 0, u4Size);

            if (gu1Secv6BypassCrypto != BYPASS_ENABLED)
            {
                UtilAlgo.UtilAesAlgo.i4AesEnc = ISS_UTIL_ENCRYPT;
                UtilAlgo.UtilAesAlgo.u4AesInBufSize = u4Size;
                UtilAlgo.UtilAesAlgo.pu1AesInBuf = pu1Buf;
                UtilAlgo.UtilAesAlgo.u4AesInKeyLen =
                    (UINT4) ((pSaEntry->u1SecAssocEspKeyLength *
                              IPSEC_AES_KEY_CONV_FACTOR));
                UtilAlgo.UtilAesAlgo.pu1AesInScheduleKey =
                    pSaEntry->au1AesEncrKey;
                UtilAlgo.UtilAesAlgo.pu1AesInUserKey =
                    pSaEntry->pu1SecAssocEspKey;
                UtilAlgo.UtilAesAlgo.u4AesInitVectLen = AES_INIT_VECT_SIZE;
                UtilAlgo.UtilAesAlgo.pu1AesInitVector =
                    pSaEntry->pu1SecAssocInitVector;

                if (UtilEncrypt (ISS_UTIL_ALGO_AES_CBC, &UtilAlgo)
                    == OSIX_FAILURE)
                {
                    SECv6_TRC (SECv6_DATA_PATH, "AESCbcEncrypt Fails\n");
                    IPSECv6_MEMFREE (pu1Buf);
                    IPSECv6_MEMFREE (pu1OptBuf);
                    return NULL;
                }
            }

            if (IPSEC_COPY_OVER_BUF (pBuf, au1SavedInitVector,
                                     0, AES_INIT_VECT_SIZE) == BUF_FAILURE)
            {
                IPSECv6_MEMFREE (pu1Buf);
                IPSECv6_MEMFREE (pu1OptBuf);
                return NULL;
            }

            if (IPSEC_COPY_OVER_BUF
                (pBuf, pu1Buf, AES_INIT_VECT_SIZE, u4Size) == BUF_FAILURE)
            {
                IPSECv6_MEMFREE (pu1Buf);
                IPSECv6_MEMFREE (pu1OptBuf);
                return NULL;
            }
            IPSECv6_MEMFREE (pu1Buf);
        }
        else if ((pSaEntry->u1SecAssocEspAlgo == SEC_AESCTR)
                 || (pSaEntry->u1SecAssocEspAlgo == SEC_AESCTR192)
                 || (pSaEntry->u1SecAssocEspAlgo == SEC_AESCTR256))
        {

            /* Allocate memory to linear buffer for passing to the
             *                AES algorithm */

            pu1Buf = IPSECv6_MALLOC (u4Size, UINT1);

            if (pu1Buf == NULL)
            {
                SECv6_PRINT_MEM_ALLOC_FAILURE;
                IPSECv6_MEMFREE (pu1OptBuf);
                return (NULL);
            }
            IPSEC_MEMCPY (au1SavedInitVector,
                          pSaEntry->pu1SecAssocInitVector,
                          IPSEC_AES_CTR_INIT_VECT_SIZE);

            /* Copy the buffer contents to the linear buffer */
            IPSEC_COPY_FROM_BUF (pBuf, pu1Buf, 0, u4Size);

            u4AesCtrNum = 0;
            MEMCPY (au1AesCounterBlock, pSaEntry->au1Nonce,
                    IPSEC_AES_CTR_NONCE_LEN);
            MEMCPY (au1AesCounterBlock + IPSEC_AES_CTR_NONCE_LEN,
                    au1SavedInitVector, IPSEC_AES_CTR_INIT_VECT_SIZE);
            MEMCPY (au1AesCounterBlock + IPSEC_AES_CTR_NONCE_LEN +
                    IPSEC_AES_CTR_INIT_VECT_SIZE, au1AesCounter,
                    IPSEC_AES_CTR_COUNTER_LEN);
            AesArSetEncryptKey (pSaEntry->au1AesEncrKey,
                                (UINT2) ((pSaEntry->u1SecAssocEspKeyLength) *
                                         IPSEC_AES_KEY_CONV_FACTOR),
                                &ArCryptoKey);

            if (gu1Secv6BypassCrypto != BYPASS_ENABLED)
            {
                UtilAlgo.UtilAesAlgo.i4AesEnc = ISS_UTIL_ENCRYPT;
                UtilAlgo.UtilAesAlgo.u4AesInBufSize = u4Size;
                UtilAlgo.UtilAesAlgo.pu1AesInBuf = pu1Buf;
                UtilAlgo.UtilAesAlgo.pu1AesOutBuf = pu1Buf;
                UtilAlgo.UtilAesAlgo.punAesArCryptoKey = &ArCryptoKey;
                UtilAlgo.UtilAesAlgo.u4AesInKeyLen =
                    (UINT4) ((pSaEntry->u1SecAssocEspKeyLength *
                              IPSEC_AES_KEY_CONV_FACTOR));
                UtilAlgo.UtilAesAlgo.pu1AesInUserKey =
                    pSaEntry->pu1SecAssocEspKey;
                UtilAlgo.UtilAesAlgo.pu1AesInitVector = au1AesCounterBlock;
                UtilAlgo.UtilAesAlgo.u4AesInitVectLen =
                    IPSEC_AES_CTR_INIT_VECT_SIZE;
                UtilAlgo.UtilAesAlgo.pu1AesEncryptBuf = au1ArEncryptBuf;
                UtilAlgo.UtilAesAlgo.pu4AesNum = &u4AesCtrNum;

                if (UtilEncrypt (ISS_UTIL_ALGO_AES_CTR_MODE, &UtilAlgo)
                    == OSIX_FAILURE)
                {
                    SECv6_TRC (SECv6_DATA_PATH, "AES-CTR Encrypt Fails\n");
                    IPSECv6_MEMFREE (pu1Buf);
                    IPSECv6_MEMFREE (pu1OptBuf);
                    return NULL;
                }
            }

            if (IPSEC_COPY_OVER_BUF (pBuf, pu1Buf, 0, u4Size) == BUF_FAILURE)
            {
                IPSECv6_MEMFREE (pu1Buf);
                IPSECv6_MEMFREE (pu1OptBuf);
                return NULL;
            }
            if (IPSEC_BUF_Prepend
                (pBuf, au1SavedInitVector,
                 IPSEC_AES_CTR_INIT_VECT_SIZE) == BUF_FAILURE)
            {
                IPSECv6_MEMFREE (pu1Buf);
                IPSECv6_MEMFREE (pu1OptBuf);
                return NULL;
            }

            IPSECv6_MEMFREE (pu1Buf);
        }

    }
    /* Add ESP Header to the Packet */
    au4EspHeader[0] = IPSEC_HTONL (pSaEntry->u4SecAssocSpi);
    pSaEntry->u4SecAssocSeqNumber = pSaEntry->u4SecAssocSeqNumber + 1;
    au4EspHeader[1] = IPSEC_HTONL (pSaEntry->u4SecAssocSeqNumber);

    if (IPSEC_BUF_Prepend
        (pBuf, (UINT1 *) au4EspHeader, SEC_ESP_HEADER_LEN) == BUF_FAILURE)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6EspTransportEncode : Prepend ESP Header Fails\n");
        if (pu1OptBuf != NULL)
        {
            IPSECv6_MEMFREE (pu1OptBuf);
        }
        return NULL;
    }

    if (pSaEntry->u1SecAssocAhAlgo != SEC_NULLAHALGO)
    {
        IPSEC_MEMSET (&UtilAlgo, 0, sizeof (unUtilAlgo));
        u4AuthBufSize = IPSEC_BUF_GetValidBytes (pBuf);

        if ((pu1AuthBuf = IPSECv6_MALLOC (u4AuthBufSize, UINT1)) == NULL)
        {
            if (pu1OptBuf != NULL)
            {
                IPSECv6_MEMFREE (pu1OptBuf);
            }
            return NULL;
        }

        if (IPSEC_COPY_FROM_BUF (pBuf, pu1AuthBuf, 0,
                                 u4AuthBufSize) == BUF_FAILURE)
        {
            if (pu1OptBuf != NULL)
            {
                IPSECv6_MEMFREE (pu1OptBuf);
            }
            IPSECv6_MEMFREE (pu1AuthBuf);
            return NULL;
        }

        /* Generate Auth Data and append to pBuf */
        switch (pSaEntry->u1SecAssocAhAlgo)
        {

            case SEC_HMACMD5:
                u1Algorithm = ISS_UTIL_ALGO_HMAC_MD5;
                break;

            case SEC_HMACSHA1:
                u1Algorithm = ISS_UTIL_ALGO_HMAC_SHA1;
                break;

            case HMAC_SHA_256:
                u1Algorithm = ISS_UTIL_ALGO_HMAC_SHA2;
                UtilAlgo.UtilHmacAlgo.HmacShaVersion = AR_SHA256_ALGO;
                break;

            case HMAC_SHA_384:
                u1Algorithm = ISS_UTIL_ALGO_HMAC_SHA2;
                UtilAlgo.UtilHmacAlgo.HmacShaVersion = AR_SHA384_ALGO;
                break;

            case HMAC_SHA_512:
                u1Algorithm = ISS_UTIL_ALGO_HMAC_SHA2;
                UtilAlgo.UtilHmacAlgo.HmacShaVersion = AR_SHA512_ALGO;
                break;
            default:
                break;
        }

        switch (pSaEntry->u1SecAssocAhAlgo)
        {
            case SEC_HMACMD5:
            case SEC_HMACSHA1:
            case HMAC_SHA_256:
            case HMAC_SHA_384:
            case HMAC_SHA_512:
                UtilAlgo.UtilHmacAlgo.pu1HmacKey = pSaEntry->pu1SecAssocAhKey1;
                UtilAlgo.UtilHmacAlgo.u4HmacKeyLen =
                    pSaEntry->u1SecAssocAhKeyLength;
                UtilAlgo.UtilHmacAlgo.pu1HmacPktDataPtr = pu1AuthBuf;
                UtilAlgo.UtilHmacAlgo.i4HmacPktLength = (INT4) u4AuthBufSize;
                UtilAlgo.UtilHmacAlgo.pu1HmacOutDigest = au1AuthDigest;
                UtilAlgo.UtilHmacAlgo.u4HmacOutDigestLen = SEC_AUTH_DIGEST_SIZE;
                UtilHash (u1Algorithm, &UtilAlgo);
                break;

            case SEC_XCBCMAC:
                UtilAlgo.UtilAesAlgo.pu1AesInUserKey =
                    pSaEntry->pu1SecAssocAhKey1;
                UtilAlgo.UtilAesAlgo.u4AesInKeyLen =
                    pSaEntry->u1SecAssocAhKeyLength;
                UtilAlgo.UtilAesAlgo.pu1AesInBuf = pu1AuthBuf;
                UtilAlgo.UtilAesAlgo.u4AesInBufSize = u4AuthBufSize;
                UtilAlgo.UtilAesAlgo.pu1AesMac = au1AuthDigest;
                UtilAlgo.UtilHmacAlgo.u4HmacOutDigestLen = SEC_AUTH_DIGEST_SIZE;
                UtilMac (ISS_UTIL_ALGO_AES_CBC_MAC96, &UtilAlgo);
                break;

            case SEC_MD5:
                UtilAlgo.UtilMd5Algo.pu1Md5InBuf = pu1AuthBuf;
                UtilAlgo.UtilMd5Algo.u4Md5InBufLen = u4AuthBufSize;
                UtilAlgo.UtilMd5Algo.pu1Md5OutDigest = au1AuthDigest;
                UtilAlgo.UtilHmacAlgo.u4HmacOutDigestLen = SEC_AUTH_DIGEST_SIZE;
                UtilMac (ISS_UTIL_ALGO_MD5_ALGO, &UtilAlgo);
                break;

            case SEC_KEYEDMD5:
                KeyedMd5 (pu1AuthBuf, (INT4) u4AuthBufSize,
                          pSaEntry->pu1SecAssocAhKey1,
                          (INT4) pSaEntry->u1SecAssocAhKeyLength,
                          au1AuthDigest);
                break;

            default:
                SECv6_TRC (SECv6_DATA_PATH, " Unknown Auth Algo \n");
                if (pu1OptBuf != NULL)
                {
                    IPSECv6_MEMFREE (pu1OptBuf);
                }
                IPSECv6_MEMFREE (pu1AuthBuf);
                return NULL;
        }

        IPSECv6_MEMFREE (pu1AuthBuf);

        u1Size = SEC_AUTH_DIGEST_SIZE;

        u4Size = IPSEC_BUF_GetValidBytes (pBuf);

        if (IPSEC_COPY_OVER_BUF_AT_END (pBuf, au1AuthDigest, u4Size, u1Size)
            == BUF_FAILURE)
        {
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6EspTransportEncode : Unable to write AuthData to Packet\n");
            if (pu1OptBuf)
            {
                IPSECv6_MEMFREE (pu1OptBuf);
            }
            return NULL;
        }
    }

    u2Len = (UINT2) (CRU_BUF_Get_ChainValidByteCount (pBuf));
    Ip6Hdr.u2Len = IPSEC_HTONS (u2Len);

    /* Check if the options are present, if so prepend the buffer having esp *
     * header with v6 header along with extention headers else prepent with *
     * v6 header alone */
    if (u1OptLen > 0)
    {
        IPSEC_MEMCPY (pu1OptBuf, &Ip6Hdr, sizeof (tIp6Hdr));

        /* Prepend IP Header to the Packet */
        if (IPSEC_BUF_Prepend
            (pBuf, pu1OptBuf,
             (UINT4) (SEC_IPV6_HEADER_SIZE + u1OptLen)) == BUF_FAILURE)
        {
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6EspTransportEncode : Prepend IP Header Fails\n");
            IPSECv6_MEMFREE (pu1OptBuf);
            return NULL;
        }

        IPSECv6_MEMFREE (pu1OptBuf);
    }
    else
    {
        Ip6Hdr.u1Nh = SEC_ESP;
        if (IPSEC_BUF_Prepend (pBuf, (UINT1 *) &Ip6Hdr,
                               SEC_IPV6_HEADER_SIZE) == BUF_FAILURE)
        {
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6EspTransportEncode : Prepend IP Header Fails\n");
            return NULL;
        }

    }
    return pBuf;
}

/************************************************************************/
/*  Function Name : Secv6EspTransportDecode                               */
/*  Description   : This function gets the incomming packet and         */
/*                : does transport mode decode based on sa entry        */
/*                :                                                     */
/*  Input(s)      : pBuf - buffer contains IP Secured packet            */
/*                : pSaEntry - Pointer to a SA Entry                    */
/*                : pu4ProcessedLen : Indicates the length upto which   */
/*                : the packet is processed                             */
/*  Output(s)     : pBuf - buffer contains IP packet                    */
/*  Global(s)     : None                                                */
/*  Return Values : NULL or SecuredBuffer                            */
/*************************************************************************/
static
    tBufChainHeader *Secv6EspTransportDecode
    (tBufChainHeader * pBuf, tSecv6Assoc * pSaEntry, UINT4 *pu4ProcessedLen)
{
    UINT4               u4SeqNumber, u4AuthBufSize = 0;
    UINT4               u4Size;
    UINT1               u1Size;
    UINT2               u2Len;
    UINT1               u1Nh;
    UINT1              *pu1AuthData = NULL;
    UINT1               au1AuthDigest[SEC_ALGO_DIGEST_SIZE];
    tIp6Hdr             Ip6Hdr;
    UINT1              *pu1AuthBuf = NULL, *pu1Buf = NULL;
    UINT1               au1InitVect[AES_INIT_VECT_SIZE];
    UINT1              *pu1OptBuf = NULL;
    UINT1               u1NxtHdr, u1OptLen;

    UINT1               au1AesCounterBlock[IPSEC_AES_CTR_COUNTER_BLOCK_LEN];
    UINT1               au1AesCounter[IPSEC_AES_CTR_COUNTER_LEN] =
        { 0x00, 0x00, 0x00, 0x01 };
    UINT1               au1ArEncryptBuf[IPSEC_AES_BLOCK_SIZE];
    UINT4               u4AesCtrNum = 0;
    unArCryptoKey       ArCryptoKey;
    UINT1               u1Algorithm = 0;
    unUtilAlgo          UtilAlgo;

    IPSEC_MEMSET (&UtilAlgo, 0, sizeof (unUtilAlgo));
    IPSEC_MEMSET (au1AuthDigest, 0, sizeof (au1AuthDigest));

    IPSEC_MEMSET (&ArCryptoKey, 0, sizeof (unArCryptoKey));
    IPSEC_MEMSET (au1AesCounterBlock, SEC_ZERO,
                  IPSEC_AES_CTR_COUNTER_BLOCK_LEN);
    IPSEC_MEMSET (au1ArEncryptBuf, SEC_ZERO, IPSEC_AES_BLOCK_SIZE);

    /* Verify SeqNumber */
    if (IPSEC_COPY_FROM_BUF (pBuf, (UINT1 *) &u4SeqNumber,
                             (*pu4ProcessedLen) + SEC_ESP_SEQ_OFFSET,
                             SEC_SEQ_NUMBER_LEN) == BUF_FAILURE)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6EspTransportDecode : Unable read SeqNumber\n");
        return NULL;
    }

    /* Remove IP header */
    if (IPSEC_COPY_FROM_BUF (pBuf, (UINT1 *) &Ip6Hdr,
                             SEC_IPV6_HEADER_HEAD_OFFSET,
                             SEC_IPV6_HEADER_SIZE) == BUF_FAILURE)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6EspTransportDecode : Remove IP Header Fails\n");
        return NULL;
    }

    if ((*pu4ProcessedLen) > SEC_IPV6_HEADER_SIZE)
    {
        pu1OptBuf = IPSECv6_MALLOC ((*pu4ProcessedLen), UINT1);

        if (pu1OptBuf == NULL)
        {
            return NULL;
        }

        /* Read Extn Header along with ipv6 header */
        if (IPSEC_COPY_FROM_BUF (pBuf, pu1OptBuf, SEC_IPV6_HEADER_HEAD_OFFSET,
                                 (*pu4ProcessedLen)) == BUF_FAILURE)
        {
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6EspTransportDecode : Read IP Extn Header Fails\n");
            IPSECv6_MEMFREE (pu1OptBuf);
            return NULL;
        }
    }

    if (IPSEC_BUF_MoveOffset (pBuf, (*pu4ProcessedLen)) == BUF_FAILURE)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6EspTransportDecode : Move Valid offset Fails to Remove IP Header \n");
        if (pu1OptBuf != NULL)
        {
            IPSECv6_MEMFREE (pu1OptBuf);
        }
        return NULL;
    }

    if (pSaEntry->u1SecAssocAhAlgo != SEC_NULLAHALGO)
    {
        u1Size = SEC_AUTH_DIGEST_SIZE;

        u4Size = IPSEC_BUF_GetValidBytes (pBuf);
        pu1AuthData = IPSECv6_MALLOC (u1Size, UINT1);

        if (pu1AuthData == NULL)
        {
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6EspTransportDecode : Malloc Fails for AuthData\n");
            if (pu1OptBuf != NULL)
            {
                IPSECv6_MEMFREE (pu1OptBuf);
            }
            return NULL;
        }

        if (IPSEC_COPY_FROM_BUF (pBuf, pu1AuthData, u4Size - u1Size, u1Size)
            == BUF_FAILURE)
        {
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6EspTransportDecode : reading Fails for AuthData\n");
            if (pu1OptBuf != NULL)
            {
                IPSECv6_MEMFREE (pu1OptBuf);
            }
            IPSECv6_MEMFREE (pu1AuthData);
            return NULL;
        }

        if (IPSEC_BUF_DeleteAtEnd (pBuf, u1Size) == NULL)
        {
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6EspTransportDecode : Delete at End Fails to Remove AuthData\n");
            if (pu1OptBuf != NULL)
            {
                IPSECv6_MEMFREE (pu1OptBuf);
            }
            IPSECv6_MEMFREE (pu1AuthData);
            return NULL;
        }

        u4AuthBufSize = IPSEC_BUF_GetValidBytes (pBuf);

        if ((pu1AuthBuf = IPSECv6_MALLOC (u4AuthBufSize, UINT1)) == NULL)
        {
            if (pu1OptBuf != NULL)
            {
                IPSECv6_MEMFREE (pu1OptBuf);
            }
            IPSECv6_MEMFREE (pu1AuthData);
            return NULL;
        }

        if (IPSEC_COPY_FROM_BUF (pBuf, pu1AuthBuf, 0,
                                 u4AuthBufSize) == BUF_FAILURE)
        {
            if (pu1OptBuf != NULL)
            {
                IPSECv6_MEMFREE (pu1OptBuf);
            }
            IPSECv6_MEMFREE (pu1AuthBuf);
            IPSECv6_MEMFREE (pu1AuthData);
            return NULL;
        }

        /* Verify Auth Data */
        switch (pSaEntry->u1SecAssocAhAlgo)
        {

            case SEC_HMACMD5:
                u1Algorithm = ISS_UTIL_ALGO_HMAC_MD5;
                break;

            case SEC_HMACSHA1:
                u1Algorithm = ISS_UTIL_ALGO_HMAC_SHA1;
                break;

            case HMAC_SHA_256:
                u1Algorithm = ISS_UTIL_ALGO_HMAC_SHA2;
                UtilAlgo.UtilHmacAlgo.HmacShaVersion = AR_SHA256_ALGO;
                break;

            case HMAC_SHA_384:
                u1Algorithm = ISS_UTIL_ALGO_HMAC_SHA2;
                UtilAlgo.UtilHmacAlgo.HmacShaVersion = AR_SHA384_ALGO;
                break;

            case HMAC_SHA_512:
                u1Algorithm = ISS_UTIL_ALGO_HMAC_SHA2;
                UtilAlgo.UtilHmacAlgo.HmacShaVersion = AR_SHA512_ALGO;
                break;
            default:
                break;
        }

        switch (pSaEntry->u1SecAssocAhAlgo)
        {
            case SEC_HMACMD5:
            case SEC_HMACSHA1:
            case HMAC_SHA_256:
            case HMAC_SHA_384:
            case HMAC_SHA_512:
                UtilAlgo.UtilHmacAlgo.pu1HmacKey = pSaEntry->pu1SecAssocAhKey1;
                UtilAlgo.UtilHmacAlgo.u4HmacKeyLen =
                    pSaEntry->u1SecAssocAhKeyLength;
                UtilAlgo.UtilHmacAlgo.pu1HmacPktDataPtr = pu1AuthBuf;
                UtilAlgo.UtilHmacAlgo.i4HmacPktLength = (INT4) u4AuthBufSize;
                UtilAlgo.UtilHmacAlgo.pu1HmacOutDigest = au1AuthDigest;
                UtilAlgo.UtilHmacAlgo.u4HmacOutDigestLen = SEC_AUTH_DIGEST_SIZE;
                UtilHash (u1Algorithm, &UtilAlgo);
                break;

            case SEC_XCBCMAC:
                UtilAlgo.UtilAesAlgo.pu1AesInUserKey =
                    pSaEntry->pu1SecAssocAhKey1;
                UtilAlgo.UtilAesAlgo.u4AesInKeyLen =
                    pSaEntry->u1SecAssocAhKeyLength;
                UtilAlgo.UtilAesAlgo.pu1AesInBuf = pu1AuthBuf;
                UtilAlgo.UtilAesAlgo.u4AesInBufSize = u4AuthBufSize;
                UtilAlgo.UtilAesAlgo.pu1AesMac = au1AuthDigest;
                UtilAlgo.UtilHmacAlgo.u4HmacOutDigestLen = SEC_AUTH_DIGEST_SIZE;
                UtilMac (ISS_UTIL_ALGO_AES_CBC_MAC96, &UtilAlgo);
                break;

            case SEC_MD5:
                UtilAlgo.UtilMd5Algo.pu1Md5InBuf = pu1AuthBuf;
                UtilAlgo.UtilMd5Algo.u4Md5InBufLen = u4AuthBufSize;
                UtilAlgo.UtilMd5Algo.pu1Md5OutDigest = au1AuthDigest;
                UtilAlgo.UtilHmacAlgo.u4HmacOutDigestLen = SEC_AUTH_DIGEST_SIZE;
                UtilMac (ISS_UTIL_ALGO_MD5_ALGO, &UtilAlgo);
                break;

            case SEC_KEYEDMD5:
                KeyedMd5 (pu1AuthBuf, (INT4) u4AuthBufSize,
                          pSaEntry->pu1SecAssocAhKey1,
                          (INT4) pSaEntry->u1SecAssocAhKeyLength,
                          au1AuthDigest);
                break;

            default:
                SECv6_TRC (SECv6_DATA_PATH, " Unknown Auth Algo \n");
                if (pu1OptBuf != NULL)
                {
                    IPSECv6_MEMFREE (pu1OptBuf);
                }
                IPSECv6_MEMFREE (pu1AuthBuf);
                IPSECv6_MEMFREE (pu1AuthData);
                return NULL;
        }

        IPSECv6_MEMFREE (pu1AuthBuf);

        if (IPSEC_MEMCMP (au1AuthDigest, pu1AuthData, u1Size) != 0)
        {
            IPSECv6_MEMFREE (pu1AuthData);
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6EspTransportDecode : Check Authentication Fails\n");
            if (pu1OptBuf != NULL)
            {
                IPSECv6_MEMFREE (pu1OptBuf);
            }
            return NULL;
        }

        IPSECv6_MEMFREE (pu1AuthData);
    }

    /* Remove Esp Header */
    if (IPSEC_BUF_MoveOffset (pBuf, SEC_ESP_HEADER_LEN) == BUF_FAILURE)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6EspTransportDecode : Move Valid offset Fails to Remove ESP\n");
        if (pu1OptBuf != NULL)
        {
            IPSECv6_MEMFREE (pu1OptBuf);
        }
        return NULL;
    }

    if (pSaEntry->u1SecAssocEspAlgo != SEC_NULLESPALGO)
    {
        /* Decrypt the pBuf */
        IPSEC_MEMSET (&UtilAlgo, 0, sizeof (unUtilAlgo));

        if ((pSaEntry->u1SecAssocEspAlgo == SEC_DES_CBC) ||
            (pSaEntry->u1SecAssocEspAlgo == SEC_3DES_CBC))
        {
            if (IPSEC_COPY_FROM_BUF (pBuf, au1InitVect, 0, DES_IV_SIZE) ==
                BUF_FAILURE)
            {
                IPSECv6_MEMFREE (pu1OptBuf);
                return NULL;
            }

            if (IPSEC_BUF_MoveOffset (pBuf, DES_IV_SIZE) == BUF_FAILURE)
            {
                IPSECv6_MEMFREE (pu1OptBuf);
                return NULL;
            }

            u4Size = IPSEC_BUF_GetValidBytes (pBuf);

            /* Allocate buffer for passing to the DES algorithm */
            pu1Buf = IPSECv6_MALLOC (u4Size, UINT1);

            if (pu1Buf == NULL)
            {
                SECv6_PRINT_MEM_ALLOC_FAILURE;
                IPSECv6_MEMFREE (pu1OptBuf);
                return NULL;
            }

            /* Copy the buffer contents to the linear buffer */
            if (IPSEC_COPY_FROM_BUF (pBuf, pu1Buf, 0, u4Size) == BUF_FAILURE)
            {

                IPSECv6_MEMFREE (pu1Buf);
                IPSECv6_MEMFREE (pu1OptBuf);
                return NULL;
            }

            if (pSaEntry->u1SecAssocEspAlgo == SEC_DES_CBC)
            {
                IPSEC_MEMCPY (ArCryptoKey.tArDes.au8ArSubkey,
                              pSaEntry->au8SubKey,
                              sizeof (pSaEntry->au8SubKey));

                if (gu1Secv6BypassCrypto != BYPASS_ENABLED)
                {
                    UtilAlgo.UtilDesAlgo.i4DesEnc = ISS_UTIL_DECRYPT;
                    UtilAlgo.UtilDesAlgo.pu1DesInBuffer = pu1Buf;
                    UtilAlgo.UtilDesAlgo.u4DesInBufSize = u4Size;
                    UtilAlgo.UtilDesAlgo.u4DesInitVectLen = DES_IV_SIZE;
                    UtilAlgo.UtilDesAlgo.pu1DesInitVect = au1InitVect;
                    UtilAlgo.UtilDesAlgo.u4DesInKeyLen =
                        pSaEntry->u1SecAssocEspKeyLength;
                    UtilAlgo.UtilDesAlgo.pu1DesInUserKey =
                        pSaEntry->pu1SecAssocEspKey;
                    UtilAlgo.UtilDesAlgo.punDesArCryptoKey = &ArCryptoKey;

                    if (UtilDecrypt (ISS_UTIL_ALGO_DES_CBC, &UtilAlgo)
                        == OSIX_FAILURE)
                    {
                        SECv6_TRC (SECv6_DATA_PATH, "DES Decrypt Fails\n");
                        IPSECv6_MEMFREE (pu1Buf);
                        IPSECv6_MEMFREE (pu1OptBuf);
                        return NULL;
                    }
                }
            }
            else if (pSaEntry->u1SecAssocEspAlgo == SEC_3DES_CBC)
            {
                IPSEC_MEMCPY (ArCryptoKey.tArTdes.ArKey0.au8ArSubkey,
                              pSaEntry->au8SubKey,
                              sizeof (pSaEntry->au8SubKey));
                IPSEC_MEMCPY (ArCryptoKey.tArTdes.ArKey1.au8ArSubkey,
                              pSaEntry->au8SubKey2,
                              sizeof (pSaEntry->au8SubKey2));
                IPSEC_MEMCPY (ArCryptoKey.tArTdes.ArKey2.au8ArSubkey,
                              pSaEntry->au8SubKey3,
                              sizeof (pSaEntry->au8SubKey3));

                if (gu1Secv6BypassCrypto != BYPASS_ENABLED)
                {
                    UtilAlgo.UtilDesAlgo.i4DesEnc = ISS_UTIL_DECRYPT;
                    UtilAlgo.UtilDesAlgo.pu1DesInBuffer = pu1Buf;
                    UtilAlgo.UtilDesAlgo.u4DesInBufSize = u4Size;
                    UtilAlgo.UtilDesAlgo.u4DesInitVectLen = DES_IV_SIZE;
                    UtilAlgo.UtilDesAlgo.pu1DesInitVect = au1InitVect;
                    UtilAlgo.UtilDesAlgo.u4DesInKeyLen =
                        pSaEntry->u1SecAssocEspKeyLength;
                    UtilAlgo.UtilDesAlgo.pu1DesInUserKey =
                        pSaEntry->pu1SecAssocEspKey;
                    UtilAlgo.UtilDesAlgo.pu1DesInUserKey2 =
                        pSaEntry->pu1SecAssocEspKey2;
                    UtilAlgo.UtilDesAlgo.pu1DesInUserKey3 =
                        pSaEntry->pu1SecAssocEspKey3;
                    UtilAlgo.UtilDesAlgo.punDesArCryptoKey = &ArCryptoKey;

                    if (UtilDecrypt (ISS_UTIL_ALGO_TDES_CBC, &UtilAlgo)
                        == OSIX_FAILURE)
                    {
                        SECv6_TRC (SECv6_DATA_PATH, "TDES Decrypt Fails\n");
                        IPSECv6_MEMFREE (pu1Buf);
                        IPSECv6_MEMFREE (pu1OptBuf);
                        return NULL;
                    }
                }

            }

            if (IPSEC_COPY_OVER_BUF (pBuf, pu1Buf, 0, u4Size) == BUF_FAILURE)
            {
                IPSECv6_MEMFREE (pu1Buf);
                IPSECv6_MEMFREE (pu1OptBuf);
                return NULL;
            }

            IPSECv6_MEMFREE (pu1Buf);
        }
        else if (pSaEntry->u1SecAssocEspAlgo == SEC_AES)
        {
            if (IPSEC_COPY_FROM_BUF (pBuf, au1InitVect, 0, AES_INIT_VECT_SIZE)
                == BUF_FAILURE)
            {
                IPSECv6_MEMFREE (pu1OptBuf);
                return NULL;
            }

            if (IPSEC_BUF_MoveOffset (pBuf, AES_INIT_VECT_SIZE) == BUF_FAILURE)
            {
                IPSECv6_MEMFREE (pu1OptBuf);
                return NULL;
            }

            u4Size = IPSEC_BUF_GetValidBytes (pBuf);
            /*  Allocate buffer for passing to the AES algorithm */
            pu1Buf = IPSECv6_MALLOC (u4Size, UINT1);

            if (pu1Buf == NULL)
            {
                SECv6_PRINT_MEM_ALLOC_FAILURE;
                IPSECv6_MEMFREE (pu1OptBuf);
                return NULL;
            }

            /* Copy the buffer contents to the linear buffer */
            if (IPSEC_COPY_FROM_BUF (pBuf, pu1Buf, 0, u4Size) == BUF_FAILURE)
            {
                IPSECv6_MEMFREE (pu1Buf);
                IPSECv6_MEMFREE (pu1OptBuf);
                return NULL;
            }

            if (gu1Secv6BypassCrypto != BYPASS_ENABLED)
            {
                UtilAlgo.UtilAesAlgo.i4AesEnc = ISS_UTIL_DECRYPT;
                UtilAlgo.UtilAesAlgo.u4AesInBufSize = u4Size;
                UtilAlgo.UtilAesAlgo.pu1AesInBuf = pu1Buf;
                UtilAlgo.UtilAesAlgo.u4AesInKeyLen =
                    (UINT4) (pSaEntry->u1SecAssocEspKeyLength *
                             IPSEC_AES_KEY_CONV_FACTOR);
                UtilAlgo.UtilAesAlgo.pu1AesInScheduleKey =
                    pSaEntry->au1AesEncrKey;
                UtilAlgo.UtilAesAlgo.pu1AesInUserKey =
                    pSaEntry->pu1SecAssocEspKey;
                UtilAlgo.UtilAesAlgo.u4AesInitVectLen = AES_INIT_VECT_SIZE;
                UtilAlgo.UtilAesAlgo.pu1AesInitVector = au1InitVect;

                if (UtilDecrypt (ISS_UTIL_ALGO_AES_CBC, &UtilAlgo)
                    == OSIX_FAILURE)
                {
                    SECv6_TRC (SECv6_DATA_PATH, "AESCbcEncrypt Fails\n");
                    IPSECv6_MEMFREE (pu1Buf);
                    IPSECv6_MEMFREE (pu1OptBuf);
                    return NULL;
                }
            }

            if (IPSEC_COPY_OVER_BUF (pBuf, pu1Buf, 0, u4Size) == BUF_FAILURE)
            {
                IPSECv6_MEMFREE (pu1OptBuf);
                IPSECv6_MEMFREE (pu1Buf);
                return NULL;
            }
            IPSECv6_MEMFREE (pu1Buf);
        }
        else if ((pSaEntry->u1SecAssocEspAlgo == SEC_AESCTR)
                 || (pSaEntry->u1SecAssocEspAlgo == SEC_AESCTR192)
                 || (pSaEntry->u1SecAssocEspAlgo == SEC_AESCTR256))
        {
            if (IPSEC_COPY_FROM_BUF
                (pBuf, au1InitVect, 0,
                 IPSEC_AES_CTR_INIT_VECT_SIZE) == BUF_FAILURE)
            {
                IPSECv6_MEMFREE (pu1OptBuf);
                return NULL;
            }

            if (IPSEC_BUF_MoveOffset (pBuf, IPSEC_AES_CTR_INIT_VECT_SIZE) ==
                BUF_FAILURE)
            {
                IPSECv6_MEMFREE (pu1OptBuf);
                return NULL;
            }

            u4Size = IPSEC_BUF_GetValidBytes (pBuf);

            /* Allocate buffer for passing to the AES algorithm */
            pu1Buf = IPSECv6_MALLOC (u4Size, UINT1);

            if (pu1Buf == NULL)
            {
                SECv6_PRINT_MEM_ALLOC_FAILURE;
                IPSECv6_MEMFREE (pu1OptBuf);
                return NULL;
            }

            /* Copy the buffer contents to the linear buffer */
            IPSEC_COPY_FROM_BUF (pBuf, pu1Buf, 0, u4Size);

            u4AesCtrNum = 0;
            MEMCPY (au1AesCounterBlock, pSaEntry->au1Nonce,
                    IPSEC_AES_CTR_NONCE_LEN);
            MEMCPY (au1AesCounterBlock + IPSEC_AES_CTR_NONCE_LEN, au1InitVect,
                    IPSEC_AES_CTR_INIT_VECT_SIZE);
            MEMCPY (au1AesCounterBlock + IPSEC_AES_CTR_NONCE_LEN +
                    IPSEC_AES_CTR_INIT_VECT_SIZE, au1AesCounter,
                    IPSEC_AES_CTR_COUNTER_LEN);
            AesArSetEncryptKey (pSaEntry->au1AesDecrKey,
                                (UINT2) ((pSaEntry->u1SecAssocEspKeyLength) *
                                         IPSEC_AES_KEY_CONV_FACTOR),
                                &ArCryptoKey);

            if (gu1Secv6BypassCrypto != BYPASS_ENABLED)
            {
                UtilAlgo.UtilAesAlgo.i4AesEnc = ISS_UTIL_DECRYPT;
                UtilAlgo.UtilAesAlgo.u4AesInBufSize = u4Size;
                UtilAlgo.UtilAesAlgo.pu1AesInBuf = pu1Buf;
                UtilAlgo.UtilAesAlgo.pu1AesOutBuf = pu1Buf;
                UtilAlgo.UtilAesAlgo.punAesArCryptoKey = &ArCryptoKey;
                UtilAlgo.UtilAesAlgo.u4AesInKeyLen =
                    (UINT4) (pSaEntry->u1SecAssocEspKeyLength *
                             IPSEC_AES_KEY_CONV_FACTOR);
                UtilAlgo.UtilAesAlgo.pu1AesInUserKey =
                    pSaEntry->pu1SecAssocEspKey;
                UtilAlgo.UtilAesAlgo.pu1AesInitVector = au1AesCounterBlock;
                UtilAlgo.UtilAesAlgo.u4AesInitVectLen =
                    IPSEC_AES_CTR_INIT_VECT_SIZE;
                UtilAlgo.UtilAesAlgo.pu1AesEncryptBuf = au1ArEncryptBuf;
                UtilAlgo.UtilAesAlgo.pu4AesNum = &u4AesCtrNum;

                if (UtilDecrypt (ISS_UTIL_ALGO_AES_CTR_MODE, &UtilAlgo)
                    == OSIX_FAILURE)
                {
                    SECv6_TRC (SECv6_DATA_PATH, "AES-CTR Decrypt Fails\n");
                    IPSECv6_MEMFREE (pu1Buf);
                    IPSECv6_MEMFREE (pu1OptBuf);
                    return NULL;
                }
            }

            if (IPSEC_COPY_OVER_BUF (pBuf, pu1Buf, 0, u4Size) == BUF_FAILURE)
            {
                IPSECv6_MEMFREE (pu1Buf);
                IPSECv6_MEMFREE (pu1OptBuf);
                return NULL;
            }

            IPSECv6_MEMFREE (pu1Buf);
        }

    }
    /* Remove Padding Bytes */
    u4Size = IPSEC_BUF_GetValidBytes (pBuf);

    if (IPSEC_COPY_FROM_BUF (pBuf, &u1Size,
                             u4Size - SEC_ESP_TRAILER_LEN,
                             SEC_ESP_TRAILER_SIZE_LEN) == BUF_FAILURE)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6EspTransportDecode : Unable to read Padding bytes Length\n");
        if (pu1OptBuf != NULL)
        {
            IPSECv6_MEMFREE (pu1OptBuf);
        }
        return NULL;
    }

    if (u1Size > SEC_ESP_MULTIPLY_FACTOR)
    {

        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6EspTransportDecode : Invalid Padding Bytes\n");
        if (pu1OptBuf != NULL)
        {
            IPSECv6_MEMFREE (pu1OptBuf);
        }
        return NULL;
    }

    if (IPSEC_COPY_FROM_BUF (pBuf, &u1Nh, u4Size - SEC_ESP_TRAILER_SIZE_LEN,
                             SEC_ESP_TRAILER_SIZE_LEN) == BUF_FAILURE)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6EspTransportDecode : Unable to read Next Header from Pkt\n");
        if (pu1OptBuf != NULL)
        {
            IPSECv6_MEMFREE (pu1OptBuf);
        }
        return NULL;
    }

    if (IPSEC_BUF_DeleteAtEnd (pBuf, (UINT4) (u1Size + SEC_ESP_TRAILER_LEN)) ==
        NULL)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6EspTransportDecode : Delete at End Fails to remove Padding\n");
        if (pu1OptBuf != NULL)
        {
            IPSECv6_MEMFREE (pu1OptBuf);
        }
        return NULL;
    }

    u2Len = (UINT2) (IPSEC_BUF_GetValidBytes (pBuf));
    Ip6Hdr.u2Len = IPSEC_HTONS (u2Len);

    /* Prepend the IP Header */
    if ((*pu4ProcessedLen) > SEC_IPV6_HEADER_SIZE)
    {
        IPSEC_MEMCPY (pu1OptBuf, &Ip6Hdr, sizeof (tIp6Hdr));

        /* Add Extn Header */
        if (IPSEC_BUF_Prepend (pBuf, pu1OptBuf,
                               (*pu4ProcessedLen)) == BUF_FAILURE)
        {
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6EspTransportDecode : IP Extn Header prepend Fails\n");
            IPSECv6_MEMFREE (pu1OptBuf);
            return NULL;
        }
        IPSECv6_MEMFREE (pu1OptBuf);

        /* Store the next header field */
        if (Secv6GetOptionsParams (pBuf, &Ip6Hdr, u1Nh, &u1NxtHdr, &u1OptLen) !=
            SEC_SUCCESS)
        {
            return NULL;
        }
    }
    else
    {
        Ip6Hdr.u1Nh = u1Nh;

        if (IPSEC_BUF_Prepend
            (pBuf, (UINT1 *) &Ip6Hdr, SEC_IPV6_HEADER_SIZE) == BUF_FAILURE)
        {
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6EspTransportDecode : Ip Header Prepend Fails\n");
            return NULL;
        }
    }
    if (pSaEntry->u1AntiReplayStatus == SEC_ANTI_REPLAY_ENABLE)
    {
        if (Secv6SeqNumberVerification
            (OSIX_NTOHL (u4SeqNumber), pSaEntry) == SEC_FAILURE)
        {
            SECv6_TRC1 (SECv6_DATA_PATH,
                        "Secv6EspTransportDecode :Replay attack from %s\n",
                        Ip6PrintAddr (&Ip6Hdr.srcAddr));
            return NULL;
        }
    }

    return pBuf;
}
