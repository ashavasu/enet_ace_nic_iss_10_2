/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: secv6cli.c,v 1.26 2014/02/14 13:57:48 siva Exp $
*
* Description:This file contains the Action Routines for IPSECv6 CLI
*             Commands
*
********************************************************************/

#ifndef __IP6SECV6CLI_C__
#define __IP6SECV6CLI_C__
#include "secv6com.h"
#include "cli.h"
#include "fssnmp.h"
#include "secv6low.h"
#include "seccli.h"
#include "secv6cli.h"
#include "secv6cliprot.h"
#include "utilcli.h"
#include "secv6wr.h"
#include "fssecvcli.h"

UINT1               cli_get_ipsecv6_secassoc_protocol (UINT1 *);
UINT1               cli_get_ipsecv6_secassoc_ahalgo (UINT1 *);
UINT1               cli_get_ipsecv6_secassoc_espalgo (UINT1 *);
UINT4               cli_get_ipsecv6_selector_protocol (UINT1 *);

#define CLI_CRYPTO_CONFIG_MODE "CryptoConfig"

VOID
cli_process_ipsecv6_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    UINT1              *args[CLI_MAX_ARGS];
    INT1                i1argno = 0;
    INT4                i4IfIndex = 0;
    va_list             ap;
    UINT4               u4ErrorCode = 0;
    UINT4               u4PolicyIndex = 0;
    UINT4               u4SecAssocIndex = 0;
    UINT4               u4SecAssocSpi = 0;
    UINT4               u4AccessIndex = 0;
    UINT4               u4SrcMask = 0;
    UINT4               u4DestMask = 0;
    UINT4               u4Index = 0;
    UINT4               u4ProtoId = 0;
    UINT4               u4Port = 0;
    UINT4               u4PktDirection = 0;
    UINT4               u4SelAccessIndex = 0;
    UINT4               u4SelPolicyIndex = 0;
    UINT4               u4CliIPSecv6Debug = 0;
    UINT1               u1PolicyFlag = 0;
    UINT1               u1PolicyMode = 0;
    UINT1               u1SecAssocMode = 0;
    UINT1               u1AntiReplayStatus = 0;
    UINT1               u1SecAssocProtocol = 0;
    UINT1               u1SecAssocAhAlgo = 0;
    UINT1               u1SecAssocAhKeyLength = 0;
    UINT1               u1SecAssocEspKeyLength = 0;
    UINT1               u1SecAssocEspKey2Length = 0;
    UINT1               u1SecAssocEspKey3Length = 0;
    UINT1               u1SecAssocEspAlgo = 0;
    UINT1               u1SelFilterFlag = 0;
    UINT1               bAddrSet = 0;
    UINT1               u1CliIPSecv6AdminStatus = 0;
    UINT1               au1SecAssocAhKey[SEC_MAX_AH_KEY_LEN + 1];
    UINT1               au1SecAssocEspKey[SEC_MAX_ESP_KEY_LEN + 1];
    UINT1               au1SecAssocEspKey2[SEC_MAX_ESP_KEY_LEN + 1];
    UINT1               au1SecAssocEspKey3[SEC_MAX_ESP_KEY_LEN + 1];
    UINT1              *pu1PolicySaBundle = NULL;
    UINT1              *pu1SecAssocAhKey = NULL;
    UINT1              *pu1SecAssocEspKey = NULL;
    UINT1              *pu1SecAssocEspKey2 = NULL;
    UINT1              *pu1SecAssocEspKey3 = NULL;
    UINT1               au1CliToken[256];
    tIp6Addr            SecAssocDestAddr;
    tIp6Addr            SrcAddress;
    tIp6Addr            DestAddress;
    tIp6Addr            TunnelTermAddr;
    tIp6Addr           *pip6addr = NULL;
    CLI_SET_CMD_STATUS (CLI_SUCCESS);

    va_start (ap, u4Command);

    if ((u4Command == 0) || (u4Command > SECv6_MAX_CLI_COMMANDS))
    {
        CliPrintf (CliHandle, "% Invalid command \r\n");
        va_end (ap);
        return;
    }

    i4IfIndex = va_arg (ap, INT4);

    if (i4IfIndex == CLI_ERROR)
    {
        CliPrintf (CliHandle, "% Specify valid interface \r\n");
        va_end (ap);
        return;
    }

/* Walk through the rest of the arguements and store in args array. 
 * Store ten arguements at the max. This is because ipsec commands do not
 * take more than ten inputs from the command line. Another reason to
 * store is in some cases first input may be optional but user may give 
 * second input. In that case first arg will be null and second arg only 
 * has value */

    while (1)
    {
        args[i1argno++] = va_arg (ap, UINT1 *);
        if (i1argno == CLI_MAX_ARGS)
            break;
    }

    va_end (ap);

    CliRegisterLock (CliHandle, IpSecv6Lock, IpSecv6UnLock);
    IpSecv6Lock ();

    MEMSET (au1CliToken, 0, sizeof (au1CliToken));
    MEMSET (au1SecAssocAhKey, 0, sizeof (au1SecAssocAhKey));
    MEMSET (au1SecAssocEspKey, 0, sizeof (au1SecAssocEspKey));
    MEMSET (au1SecAssocEspKey2, 0, sizeof (au1SecAssocEspKey2));
    MEMSET (au1SecAssocEspKey3, 0, sizeof (au1SecAssocEspKey3));
    switch (u4Command)
    {
        case CLI_CREATE_IPSECv6_POLICY:
            u4PolicyIndex = (UINT4) CLI_ATOL (args[0]);
            u1PolicyFlag = (UINT1) CLI_ATOL (args[1]);
            u1PolicyMode = (UINT1) CLI_ATOI (args[2]);
            pu1PolicySaBundle = &au1CliToken[0];
            CLI_MEMSET (pu1PolicySaBundle, 0, STRLEN (pu1PolicySaBundle) + 1);
            CLI_STRCPY (pu1PolicySaBundle, args[3]);
            IPSecv6CreatePolicy (CliHandle,
                                 u4PolicyIndex,
                                 u1PolicyFlag, u1PolicyMode, pu1PolicySaBundle);
            break;

        case CLI_CREATE_IPSECv6_SA:
            u4SecAssocIndex = (UINT4) CLI_ATOL (args[0]);
            if ((pip6addr = str_to_ip6addr (args[1])) == NULL)
            {
                CLI_SET_ERR (CLI_IPSECV6_INVALID_ADDRESS);
                break;
            }
            Ip6AddrCopy (&(SecAssocDestAddr), pip6addr);
            u4SecAssocSpi = (UINT4) CLI_ATOL (args[2]);
            u1SecAssocMode = (UINT1) CLI_ATOI (args[3]);
            u1AntiReplayStatus = (UINT1) CLI_ATOI (args[4]);
            IPSecv6CreateSecAssoc (CliHandle,
                                   u4SecAssocIndex,
                                   &SecAssocDestAddr,
                                   u4SecAssocSpi,
                                   u1SecAssocMode, u1AntiReplayStatus);
            break;

        case CLI_SET_IPSECv6_SA_PARAMS:
            u4SecAssocIndex = (UINT4) (UINT4) CLI_ATOL (args[0]);
            u1SecAssocProtocol = cli_get_ipsecv6_secassoc_protocol (args[1]);
            if (u1SecAssocProtocol == 0)
            {
                CLI_SET_ERR (CLI_IPSECV6_PROTO_AH_ESP);
                break;
            }
            if (CLI_STRCASECMP (args[2], "auth") == 0)
            {
                u1SecAssocAhAlgo = cli_get_ipsecv6_secassoc_ahalgo (args[3]);
                if (u1SecAssocAhAlgo == 5)
                {
                    CLI_SET_ERR (CLI_IPSECV6_INVALID_ALGORITHM);
                    break;
                }
                if ((u1SecAssocAhAlgo != (UINT1) CLI_ATOI (CLI_NULL)) &&
                    (u1SecAssocAhAlgo != (UINT1) CLI_ATOI (CLI_MD5)))
                {
                    pu1SecAssocAhKey = &au1SecAssocAhKey[0];
                    CLI_STRCPY (pu1SecAssocAhKey, args[4]);
                    u1SecAssocAhKeyLength = (UINT1) CLI_STRLEN (args[4]);
                }
                if ((u1SecAssocProtocol == (UINT1) CLI_ATOI (CLI_ESP)) &&
                    (CLI_STRCASECMP (args[5], "encr") == 0))
                {
                    if ((u1SecAssocEspAlgo =
                         cli_get_ipsecv6_secassoc_espalgo (args[6])) == 13)
                    {
                        CLI_SET_ERR (CLI_IPSECV6_INVALID_ALGORITHM);
                        break;
                    }
                    if (u1SecAssocEspAlgo != (UINT1) CLI_ATOI (CLI_ESP_NULL))
                    {
                        pu1SecAssocEspKey = &au1SecAssocEspKey[0];
                        CLI_STRCPY (pu1SecAssocEspKey, args[7]);
                        u1SecAssocEspKeyLength = (UINT1) CLI_STRLEN (args[7]);

                        if ((u1SecAssocEspAlgo
                             == (UINT1) CLI_ATOI (CLI_3DESCBC)))
                        {
                            pu1SecAssocEspKey2 = &au1SecAssocEspKey2[0];
                            CLI_STRCPY (pu1SecAssocEspKey2, args[8]);

                            u1SecAssocEspKey2Length =
                                (UINT1) CLI_STRLEN (args[8]);

                            pu1SecAssocEspKey3 = &au1SecAssocEspKey3[0];
                            CLI_STRCPY (pu1SecAssocEspKey3, args[9]);

                            u1SecAssocEspKey3Length =
                                (UINT1) CLI_STRLEN (args[9]);
                        }
                    }
                }
                if ((u1SecAssocProtocol == (UINT1) CLI_ATOI (CLI_ESP)) &&
                    (CLI_STRCASECMP (args[4], "encr") == 0))
                {
                    if ((u1SecAssocEspAlgo =
                         cli_get_ipsecv6_secassoc_espalgo (args[5])) == 13)
                    {
                        CLI_SET_ERR (CLI_IPSECV6_INVALID_ALGORITHM);
                        break;
                    }
                    if (u1SecAssocEspAlgo != (UINT1) CLI_ATOI (CLI_ESP_NULL))
                    {
                        pu1SecAssocEspKey = &au1SecAssocEspKey[0];
                        CLI_STRCPY (pu1SecAssocEspKey, args[6]);

                        u1SecAssocEspKeyLength = (UINT1) CLI_STRLEN (args[6]);

                        if (u1SecAssocEspAlgo == (UINT1) CLI_ATOI (CLI_3DESCBC))
                        {
                            pu1SecAssocEspKey2 = &au1SecAssocEspKey2[0];
                            CLI_STRCPY (pu1SecAssocEspKey2, args[7]);

                            u1SecAssocEspKey2Length =
                                (UINT1) CLI_STRLEN (args[7]);
                            pu1SecAssocEspKey3 = &au1SecAssocEspKey3[0];
                            CLI_STRCPY (pu1SecAssocEspKey3, args[8]);

                            u1SecAssocEspKey3Length =
                                (UINT1) CLI_STRLEN (args[8]);
                        }
                    }
                }
            }
            else if (CLI_STRCASECMP (args[2], "encr") == 0)
            {
                u1SecAssocAhAlgo = (UINT1) CLI_ATOI (CLI_NULL);
                if (u1SecAssocProtocol == (UINT1) CLI_ATOI (CLI_ESP))
                {
                    if ((u1SecAssocEspAlgo =
                         cli_get_ipsecv6_secassoc_espalgo (args[3])) == 13)
                    {
                        CLI_SET_ERR (CLI_IPSECV6_INVALID_ALGORITHM);
                        break;
                    }
                    if (u1SecAssocEspAlgo != (UINT1) CLI_ATOI (CLI_ESP_NULL))
                    {
                        pu1SecAssocEspKey = &au1SecAssocEspKey[0];
                        CLI_STRCPY (pu1SecAssocEspKey, args[4]);

                        u1SecAssocEspKeyLength = (UINT1) CLI_STRLEN (args[4]);

                        if (u1SecAssocEspAlgo == (UINT1) CLI_ATOI (CLI_3DESCBC))
                        {
                            pu1SecAssocEspKey2 = &au1SecAssocEspKey2[0];
                            CLI_STRCPY (pu1SecAssocEspKey2, args[5]);

                            u1SecAssocEspKey2Length =
                                (UINT1) CLI_STRLEN (args[5]);
                            pu1SecAssocEspKey3 = &au1SecAssocEspKey3[0];
                            CLI_STRCPY (pu1SecAssocEspKey3, args[6]);

                            u1SecAssocEspKey3Length =
                                (UINT1) CLI_STRLEN (args[6]);
                        }
                    }
                }
                else
                {
                    CLI_SET_ERR (CLI_IPSECV6_ENCR_ALGO_FOR_ESP);
                    break;
                }
            }
            else
            {
                CLI_SET_ERR (CLI_IPSECV6_AUTH_OR_ENCR);
                break;
            }
            IPSecv6SetSecAssocParams (CliHandle,
                                      u4SecAssocIndex,
                                      pu1SecAssocAhKey,
                                      pu1SecAssocEspKey,
                                      pu1SecAssocEspKey2,
                                      pu1SecAssocEspKey3,
                                      u1SecAssocProtocol,
                                      u1SecAssocAhAlgo,
                                      u1SecAssocEspAlgo,
                                      u1SecAssocAhKeyLength,
                                      u1SecAssocEspKeyLength,
                                      u1SecAssocEspKey2Length,
                                      u1SecAssocEspKey3Length);
            break;

        case CLI_CREATE_IPSECv6_ACCESS_LIST:
            u4AccessIndex = (UINT4) CLI_ATOL (args[0]);

            if ((pip6addr = str_to_ip6addr (args[1])) == NULL)
            {
                CLI_SET_ERR (CLI_IPSECV6_INVALID_ADDRESS);
                break;
            }
            Ip6AddrCopy (&(SrcAddress), pip6addr);

            u4SrcMask = (UINT4) CLI_ATOL (args[2]);

            if ((pip6addr = str_to_ip6addr (args[3])) == NULL)
            {
                CLI_SET_ERR (CLI_IPSECV6_INVALID_ADDRESS);
                break;
            }
            Ip6AddrCopy (&(DestAddress), pip6addr);

            u4DestMask = (UINT4) CLI_ATOL (args[4]);
            IPSecv6CreateAccess (CliHandle,
                                 u4AccessIndex,
                                 &SrcAddress,
                                 u4SrcMask, &DestAddress, u4DestMask);
            break;

        case CLI_CREATE_IPSECv6_SELECTOR:
            u4Index = CLI_PTR_TO_U4 ((args[0]));
            if ((u4ProtoId = cli_get_ipsecv6_selector_protocol (args[1])) == 0)
            {
                CLI_SET_ERR (CLI_IPSECV6_INVALID_PROTOCOL);
                break;
            }
            if (CLI_STRCASECMP (args[2], "any") == 0)
            {
                u4Port = (UINT4) CLI_ATOL (CLI_ANY_PORT);
            }
            else
            {
                u4Port = (UINT4) CLI_ATOL (args[2]);
            }
            if (CLI_STRCASECMP (args[3], "inbound") == 0)
            {
                u4PktDirection = (UINT4) CLI_ATOL (CLI_INBOUND);
            }
            else if (CLI_STRCASECMP (args[3], "outbound") == 0)
            {
                u4PktDirection = (UINT4) CLI_ATOL (CLI_OUTBOUND);
            }
            else if (CLI_STRCASECMP (args[3], "any") == 0)
            {
                u4PktDirection = (UINT4) CLI_ATOL (CLI_ANY_DIRECTION);
            }
            else
            {
                CLI_SET_ERR (CLI_IPSECV6_DIR_IN_OUT_BOUND);
                break;
            }
            u4SelAccessIndex = (UINT4) CLI_ATOL (args[4]);
            u4SelPolicyIndex = (UINT4) CLI_ATOL (args[5]);
            if (CLI_STRCASECMP (args[6], "filter") == 0)
            {
                u1SelFilterFlag = (UINT1) CLI_ATOI (CLI_FILTER);
            }
            else if (CLI_STRCASECMP (args[6], "allow") == 0)
            {
                u1SelFilterFlag = (UINT1) CLI_ATOI (CLI_ALLOW);
            }
            else
            {
                CLI_SET_ERR (CLI_IPSECV6_FLAG_ALLOW_FILTER);
                break;
            }

            if (CLI_STRCMP (args[7], CLI_IPSEC_MANUAL) != 0)
            {
                if ((pip6addr = str_to_ip6addr (args[7])) == NULL)
                {
                    CLI_SET_ERR (CLI_IPSECV6_INVALID_ADDRESS);
                    break;
                }
                Ip6AddrCopy (&(TunnelTermAddr), pip6addr);
                bAddrSet = TRUE;
            }
            else
            {
                bAddrSet = FALSE;
            }
            IPSecv6CreateSelector (CliHandle,
                                   u4Index,
                                   u4ProtoId,
                                   u4Port,
                                   u4PktDirection,
                                   u4SelAccessIndex,
                                   u4SelPolicyIndex,
                                   u1SelFilterFlag, &TunnelTermAddr, bAddrSet);
            break;

        case CLI_SET_IPSECv6_ADMIN:
            u1CliIPSecv6AdminStatus = (UINT1) (CLI_PTR_TO_U4 (args[0]));
            IPSecv6SetAdminStatus (CliHandle, u1CliIPSecv6AdminStatus);
            break;
        case CLI_SET_IPSECv6_TRACE_LEVEL:
            u4CliIPSecv6Debug = (UINT4) CLI_ATOL (args[0]);
            IPSecv6SetTraceLevel (CliHandle, u4CliIPSecv6Debug);
            break;
        case CLI_SHOW_IPSECv6_IF_STAT:
            IPSecv6ShowIfStat (CliHandle);
            break;
        case CLI_SHOW_IPSECv6_AH_ESP_STAT:
            IPSecv6ShowAhEspStat (CliHandle);
            break;
        case CLI_SHOW_IPSECv6_INTRUDER_STAT:
            IPSecv6ShowIntruderStat (CliHandle);
            break;
        case CLI_SHOW_IPSECv6_CONFIG:
            IPSecv6ShowGlobalEntries (CliHandle);
            break;
        case CLI_SHOW_IPSECv6_POLICY:
            u4Index = (UINT4) CLI_ATOL (args[0]);
            IPSecv6ShowPolicyEntries (CliHandle, u4Index);
            break;
        case CLI_SHOW_IPSECv6_SELECTOR:
            u4Index = (UINT4) CLI_ATOL (args[0]);
            IPSecv6ShowSelectorEntries (CliHandle, u4Index);
            break;
        case CLI_SHOW_IPSECv6_SA:
            u4Index = (UINT4) CLI_ATOL (args[0]);
            IPSecv6ShowSecAssocEntries (CliHandle, u4Index);
            break;
        case CLI_SHOW_IPSECv6_ACCESS:
            u4Index = (UINT4) CLI_ATOL (args[0]);
            IPSecv6ShowAccessEntries (CliHandle, u4Index);
            break;
        case CLI_DELETE_IPSECv6_POLICY:
            u4Index = (UINT4) CLI_ATOL (args[0]);
            IPSecv6DeletePolicyEntries (CliHandle, u4Index);
            break;
        case CLI_DELETE_IPSECv6_SELECTOR:
            u4Index = (UINT4) CLI_ATOL (args[0]);
            IPSecv6DeleteSelectorEntries (CliHandle, u4Index);
            break;
        case CLI_DELETE_IPSECv6_SA:
            u4Index = (UINT4) CLI_ATOL (args[0]);
            IPSecv6DeleteSecAssocEntries (CliHandle, u4Index);
            break;
        case CLI_DELETE_IPSECv6_ACCESS:
            u4Index = (UINT4) CLI_ATOL (args[0]);
            IPSecv6DeleteAccessEntries (CliHandle, u4Index);
            break;
        case CLI_CRYPTO_TRANSFORM_CONF:
            IPSecv6CryptoConfigMode (CliHandle);
            break;
        default:
            CLI_SET_ERR (CLI_IPSECV6_INVALID_COMMAND);
            break;
    }

    if ((CLI_GET_ERR (&u4ErrorCode)) == CLI_SUCCESS)
    {
        if ((u4ErrorCode > 0) && (u4ErrorCode < CLI_IPSECV6_MAX_ERROR))
        {
            CLI_SET_CMD_STATUS (CLI_FAILURE);
            CliPrintf (CliHandle, "\r%s", IpSecv6CliErrString[u4ErrorCode]);
        }
        CLI_SET_ERR (0);
    }

    IpSecv6UnLock ();
    CliUnRegisterLock (CliHandle);
    return;
}

/***************************************************************************  
 * FUNCTION NAME : IPSecv6CreatePolicy 
 * DESCRIPTION   : This function creates an entry in the policy list 
 * RETURNS       : NONE
 ***************************************************************************/
VOID
IPSecv6CreatePolicy (tCliHandle CliHandle, UINT4 u4PolicyIndex,
                     UINT1 u1PolicyFlag, UINT1 u1PolicyMode,
                     UINT1 *pu1PolicySaBundle)
{
    UINT4               u4SnmpErrorStatus;
    tSNMP_OCTET_STRING_TYPE OctetStr;

    OctetStr.pu1_OctetList = pu1PolicySaBundle;
    OctetStr.i4_Length = (INT4) STRLEN (pu1PolicySaBundle);

    if (nmhTestv2Fsipv6SecPolicyStatus (&u4SnmpErrorStatus,
                                        (INT4) u4PolicyIndex, CREATE_AND_GO) ==
        SNMP_FAILURE)
    {
        switch (u4SnmpErrorStatus)
        {
            case SNMP_ERR_WRONG_VALUE:
                CLI_SET_ERR (CLI_IPSECV6_INVALID_INDEX_RANGE);
                break;
            case SNMP_ERR_COMMIT_FAILED:
                CLI_SET_ERR (CLI_IPSECV6_ENABLE_IPSEC);
                break;
            case SNMP_ERR_INCONSISTENT_VALUE:
                CLI_SET_ERR (CLI_IPSECV6_INDEX_EXIST);
                break;
            default:
                break;
        }
        return;
    }

    if (nmhSetFsipv6SecPolicyStatus (u4PolicyIndex,
                                     CREATE_AND_GO) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return;
    }

    if (nmhTestv2Fsipv6SecPolicyFlag (&u4SnmpErrorStatus, (INT4) u4PolicyIndex,
                                      u1PolicyFlag) == SNMP_FAILURE)
    {
        switch (u4SnmpErrorStatus)
        {
            case SNMP_ERR_WRONG_VALUE:
                CLI_SET_ERR (CLI_IPSECV6_APPLY_BYPASS);
                break;

            case SNMP_ERR_NO_CREATION:
                CLI_SET_ERR (CLI_IPSECV6_INVALID_INDEX);
                break;
            default:
                break;
        }
        return;
    }

    nmhSetFsipv6SecPolicyFlag (u4PolicyIndex, u1PolicyFlag);

    if (nmhTestv2Fsipv6SecPolicyMode (&u4SnmpErrorStatus,
                                      (INT4) u4PolicyIndex,
                                      u1PolicyMode) == SNMP_FAILURE)
    {
        switch (u4SnmpErrorStatus)
        {
            case SNMP_ERR_WRONG_VALUE:
                CLI_SET_ERR (CLI_IPSECV6_MANUAL_AUTOMATIC);
                break;

            case SNMP_ERR_NO_CREATION:
                CLI_SET_ERR (CLI_IPSECV6_INVALID_INDEX);
                break;

            default:
                break;
        }
        return;
    }

    nmhSetFsipv6SecPolicyMode (u4PolicyIndex, u1PolicyMode);

    if (nmhTestv2Fsipv6SecPolicySaBundle (&u4SnmpErrorStatus,
                                          (INT4) u4PolicyIndex,
                                          &OctetStr) == SNMP_FAILURE)

    {
        switch (u4SnmpErrorStatus)
        {
            case SNMP_ERR_WRONG_VALUE:
                CLI_SET_ERR (CLI_IPSECV6_INVALID_BUNDLE_LENGTH);
                break;

            case SNMP_ERR_NO_CREATION:
                CLI_SET_ERR (CLI_IPSECV6_INVALID_INDEX);
                break;

            case SNMP_ERR_COMMIT_FAILED:
                CLI_SET_ERR (CLI_IPSECV6_SA_NOT_CREATE);
                break;

            default:
                break;
        }
        return;
    }

    nmhSetFsipv6SecPolicySaBundle (u4PolicyIndex, &OctetStr);

}

/***************************************************************************  
 * FUNCTION NAME : IPSecv6CreateSecAssoc 
 * DESCRIPTION   : This function is used to create an entry in security assoc
 *               : data base  
 * RETURNS       : NONE
 ***************************************************************************/
VOID
IPSecv6CreateSecAssoc (tCliHandle CliHandle, UINT4 u4SecAssocIndex,
                       tIp6Addr * SecAssocDestAddr,
                       UINT4 u4SecAssocSpi, UINT1 u1SecAssocMode,
                       UINT1 u1AntiReplayStatus)
{
    UINT4               u4SnmpErrorStatus;
    tSNMP_OCTET_STRING_TYPE OctetStr;
    UINT1               au1Secv6Addr[SEC_ADDR_LEN];

    if (nmhTestv2Fsipv6SecAssocStatus (&u4SnmpErrorStatus,
                                       (INT4) u4SecAssocIndex,
                                       CREATE_AND_GO) == SNMP_FAILURE)
    {
        switch (u4SnmpErrorStatus)
        {
            case SNMP_ERR_WRONG_VALUE:
                CLI_SET_ERR (CLI_IPSECV6_INVALID_INDEX_RANGE);
                break;
            case SNMP_ERR_INCONSISTENT_VALUE:
                CLI_SET_ERR (CLI_IPSECV6_INDEX_EXIST);
                break;
            default:
                break;
        }
        return;
    }

    if (nmhSetFsipv6SecAssocStatus (u4SecAssocIndex, CREATE_AND_GO) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
    }

    Ip6AddrCopy ((tIp6Addr *) (VOID *) au1Secv6Addr,
                 (tIp6Addr *) (VOID *) ((SecAssocDestAddr)));
    OctetStr.pu1_OctetList = au1Secv6Addr;
    OctetStr.i4_Length = SEC_ADDR_LEN;

    if (nmhTestv2Fsipv6SecAssocDstAddr (&u4SnmpErrorStatus,
                                        (INT4) u4SecAssocIndex,
                                        &OctetStr) == SNMP_FAILURE)
    {
        switch (u4SnmpErrorStatus)
        {
            case SNMP_ERR_WRONG_VALUE:
            case SNMP_ERR_WRONG_LENGTH:
                CLI_SET_ERR (CLI_IPSECV6_INVALID_ADDRESS);
                break;

            case SNMP_ERR_NO_CREATION:
                CLI_SET_ERR (CLI_IPSECV6_INVALID_INDEX);
                break;
            default:
                break;
        }

        nmhSetFsipv6SecAssocStatus (u4SecAssocIndex, DESTROY);
        return;
    }

    nmhSetFsipv6SecAssocDstAddr (u4SecAssocIndex, &OctetStr);

    if (nmhTestv2Fsipv6SecAssocSpi (&u4SnmpErrorStatus,
                                    (INT4) u4SecAssocIndex,
                                    (INT4) u4SecAssocSpi) == SNMP_FAILURE)
    {
        switch (u4SnmpErrorStatus)
        {
            case SNMP_ERR_WRONG_VALUE:
                CLI_SET_ERR (CLI_IPSECV6_INVALID_SPI_RANGE);
                break;

            case SNMP_ERR_NO_CREATION:
                CLI_SET_ERR (CLI_IPSECV6_INVALID_INDEX);
                break;
            default:
                break;

        }

        nmhSetFsipv6SecAssocStatus (u4SecAssocIndex, DESTROY);
        return;
    }

    nmhSetFsipv6SecAssocSpi (u4SecAssocIndex, u4SecAssocSpi);

    if (nmhTestv2Fsipv6SecAssocMode (&u4SnmpErrorStatus,
                                     (INT4) u4SecAssocIndex,
                                     u1SecAssocMode) == SNMP_FAILURE)
    {
        switch (u4SnmpErrorStatus)
        {
            case SNMP_ERR_WRONG_VALUE:
                CLI_SET_ERR (CLI_IPSECV6_TRANSPORT_TUNNEL);
                break;
            case SNMP_ERR_NO_CREATION:
                CLI_SET_ERR (CLI_IPSECV6_INVALID_INDEX);
                break;
            default:
                break;
        }

        return;
    }

    nmhSetFsipv6SecAssocMode (u4SecAssocIndex, u1SecAssocMode);

    if (nmhTestv2Fsipv6SecAssocAntiReplay (&u4SnmpErrorStatus,
                                           (INT4) u4SecAssocIndex,
                                           u1AntiReplayStatus) == SNMP_FAILURE)
    {
        switch (u4SnmpErrorStatus)
        {
            case SNMP_ERR_WRONG_VALUE:
                CLI_SET_ERR (CLI_IPSECV6_REPLY_EANBLE_DISABLE);
                break;
            case SNMP_ERR_NO_CREATION:
                CLI_SET_ERR (CLI_IPSECV6_INVALID_INDEX);
                break;
            default:
                break;
        }

        return;
    }

    nmhSetFsipv6SecAssocAntiReplay (u4SecAssocIndex, u1AntiReplayStatus);

}

/***************************************************************************  
 * FUNCTION NAME : IPSecv6SetSecAssocParams 
 * DESCRIPTION   : This function process the SAD command 
 * RETURNS       : NONE
 ***************************************************************************/
VOID
IPSecv6SetSecAssocParams (tCliHandle CliHandle, UINT4 u4SecAssocIndex,
                          UINT1 *pu1SecAssocAhKey,
                          UINT1 *pu1SecAssocEspKey, UINT1 *pu1SecAssocEspKey2,
                          UINT1 *pu1SecAssocEspKey3, UINT1 u1SecAssocProtocol,
                          UINT1 u1SecAssocAhAlgo, UINT1 u1SecAssocEspAlgo,
                          UINT1 u1SecAssocAhKeyLength,
                          UINT1 u1SecAssocEspKeyLength,
                          UINT1 u1SecAssocEspKey2Length,
                          UINT1 u1SecAssocEspKey3Length)
{
    UINT4               u4SnmpErrorStatus;
    tSNMP_OCTET_STRING_TYPE OctetStr;
    UINT1               au1OutKey[SEC_HMAC_SHA_MAX_LEN + 1];
    UINT1               u1Len = 0;

    SEC_UNUSED (u1SecAssocAhKeyLength);
    SEC_UNUSED (u1SecAssocEspKeyLength);
    SEC_UNUSED (u1SecAssocEspKey2Length);
    SEC_UNUSED (u1SecAssocEspKey3Length);
    SEC_UNUSED (CliHandle);

    IPSEC_MEMSET (au1OutKey, 0, sizeof (au1OutKey));

    if (nmhTestv2Fsipv6SecAssocProtocol
        (&u4SnmpErrorStatus, (INT4) u4SecAssocIndex,
         u1SecAssocProtocol) == SNMP_FAILURE)
    {
        switch (u4SnmpErrorStatus)
        {
            case SNMP_ERR_WRONG_VALUE:
                CLI_SET_ERR (CLI_IPSECV6_PROTO_AH_ESP);
                break;
            case SNMP_ERR_NO_CREATION:
                CLI_SET_ERR (CLI_IPSECV6_INVALID_INDEX);
                break;
            default:
                break;
        }

        return;
    }

    nmhSetFsipv6SecAssocProtocol (u4SecAssocIndex, u1SecAssocProtocol);

    if (u1SecAssocProtocol == SEC_AH)
    {
        if (nmhTestv2Fsipv6SecAssocAhAlgo (&u4SnmpErrorStatus,
                                           (INT4) u4SecAssocIndex,
                                           u1SecAssocAhAlgo) == SNMP_FAILURE)
        {
            switch (u4SnmpErrorStatus)
            {
                case SNMP_ERR_WRONG_VALUE:
                    CLI_SET_ERR (CLI_IPSECV6_INVALID_AH_ALGO);
                    break;
                case SNMP_ERR_NO_CREATION:
                    CLI_SET_ERR (CLI_IPSECV6_INVALID_INDEX);
                    break;
                default:
                    break;
            }

            return;
        }

        nmhSetFsipv6SecAssocAhAlgo (u4SecAssocIndex, u1SecAssocAhAlgo);

        if ((u1SecAssocAhAlgo != SEC_MD5)
            && (u1SecAssocAhAlgo != SEC_NULLAHALGO))
        {
            if (pu1SecAssocAhKey == NULL)
            {
                CLI_SET_ERR (CLI_IPSECV6_INVALID_KEY_LENGTH);
                return;
            }
            u1Len = (UINT1) STRLEN (pu1SecAssocAhKey);
            IPSEC_MEMSET (au1OutKey, 0, sizeof (au1OutKey));
            if (Secv6ConstructKey (pu1SecAssocAhKey,
                                   u1Len, au1OutKey) == SEC_FAILURE)
            {
                CLI_SET_ERR (CLI_IPSECV6_INVALID_KEY_LENGTH);
            }
            OctetStr.i4_Length = (INT4) STRLEN (au1OutKey);
            OctetStr.pu1_OctetList = au1OutKey;

            if (nmhTestv2Fsipv6SecAssocAhKey
                (&u4SnmpErrorStatus,
                 (INT4) u4SecAssocIndex, &OctetStr) == SNMP_FAILURE)
            {
                switch (u4SnmpErrorStatus)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                        if (u1SecAssocAhAlgo == SEC_HMACSHA1)
                        {
                            CLI_SET_ERR
                                (CLI_IPSECV6_INVALID_KEY_LENGTH_HMACSHA1);
                        }
                        else if (u1SecAssocAhAlgo == SEC_HMACMD5)
                        {
                            CLI_SET_ERR
                                (CLI_IPSECV6_INVALID_KEY_LENGTH_HMACMD5);
                        }
                        else if (u1SecAssocAhAlgo == SEC_XCBCMAC)
                        {
                            CLI_SET_ERR
                                (CLI_IPSECV6_INVALID_KEY_LENGTH_XCBCMAC);
                        }
                        else if (u1SecAssocAhAlgo == HMAC_SHA_256)
                        {
                            CLI_SET_ERR
                                (CLI_IPSECV6_INVALID_KEY_LENGTH_HMACSHA256);
                        }
                        else if (u1SecAssocAhAlgo == HMAC_SHA_384)
                        {
                            CLI_SET_ERR
                                (CLI_IPSECV6_INVALID_KEY_LENGTH_HMACSHA384);
                        }
                        else if (u1SecAssocAhAlgo == HMAC_SHA_512)
                        {
                            CLI_SET_ERR
                                (CLI_IPSECV6_INVALID_KEY_LENGTH_HMACSHA512);
                        }

                        break;
                    case SNMP_ERR_NO_CREATION:
                        CLI_SET_ERR (CLI_IPSECV6_INVALID_INDEX);
                        break;
                    default:
                        break;
                }

                return;
            }

            nmhSetFsipv6SecAssocAhKey (u4SecAssocIndex, &OctetStr);

        }
    }

    if (u1SecAssocProtocol == SEC_ESP)
    {
        if (u1SecAssocAhAlgo != SEC_NULLAHALGO)
        {
            if (nmhTestv2Fsipv6SecAssocAhAlgo
                (&u4SnmpErrorStatus,
                 (INT4) u4SecAssocIndex, u1SecAssocAhAlgo) == SNMP_FAILURE)
            {
                switch (u4SnmpErrorStatus)
                {
                    case SNMP_ERR_WRONG_VALUE:
                        CLI_SET_ERR (CLI_IPSECV6_INVALID_AH_ALGO);
                        break;
                    case SNMP_ERR_NO_CREATION:
                        CLI_SET_ERR (CLI_IPSECV6_INVALID_INDEX);
                        break;
                    default:
                        break;
                }

                return;
            }

            nmhSetFsipv6SecAssocAhAlgo (u4SecAssocIndex, u1SecAssocAhAlgo);
            if (u1SecAssocAhAlgo != SEC_MD5)
            {
                if (pu1SecAssocAhKey == NULL)
                {
                    CLI_SET_ERR (CLI_IPSECV6_INVALID_KEY_LENGTH);
                    return;
                }
                u1Len = (UINT1) STRLEN (pu1SecAssocAhKey);
                IPSEC_MEMSET (au1OutKey, 0, sizeof (au1OutKey));
                if (Secv6ConstructKey (pu1SecAssocAhKey,
                                       u1Len, au1OutKey) == SEC_FAILURE)
                {
                    CLI_SET_ERR (CLI_IPSECV6_INVALID_KEY_LENGTH);
                }

                OctetStr.i4_Length = (INT4) STRLEN (au1OutKey);
                OctetStr.pu1_OctetList = au1OutKey;

                if (nmhTestv2Fsipv6SecAssocAhKey
                    (&u4SnmpErrorStatus,
                     (INT4) u4SecAssocIndex, &OctetStr) == SNMP_FAILURE)
                {
                    switch (u4SnmpErrorStatus)
                    {

                        case SNMP_ERR_WRONG_LENGTH:

                            if (u1SecAssocAhAlgo == SEC_HMACSHA1)
                            {
                                CLI_SET_ERR
                                    (CLI_IPSECV6_INVALID_KEY_LENGTH_HMACSHA1);
                            }
                            else if (u1SecAssocAhAlgo == SEC_HMACMD5)
                            {
                                CLI_SET_ERR
                                    (CLI_IPSECV6_INVALID_KEY_LENGTH_HMACMD5);
                            }
                            else if (u1SecAssocAhAlgo == SEC_XCBCMAC)
                            {
                                CLI_SET_ERR
                                    (CLI_IPSECV6_INVALID_KEY_LENGTH_XCBCMAC);
                            }
                            else if (u1SecAssocAhAlgo == HMAC_SHA_256)
                            {
                                CLI_SET_ERR
                                    (CLI_IPSECV6_INVALID_KEY_LENGTH_HMACSHA256);
                            }
                            else if (u1SecAssocAhAlgo == HMAC_SHA_384)
                            {
                                CLI_SET_ERR
                                    (CLI_IPSECV6_INVALID_KEY_LENGTH_HMACSHA384);
                            }
                            else if (u1SecAssocAhAlgo == HMAC_SHA_512)
                            {
                                CLI_SET_ERR
                                    (CLI_IPSECV6_INVALID_KEY_LENGTH_HMACSHA512);
                            }

                            break;

                        case SNMP_ERR_NO_CREATION:
                            CLI_SET_ERR (CLI_IPSECV6_INVALID_INDEX);
                            break;
                        default:
                            break;
                    }

                    return;
                }

                nmhSetFsipv6SecAssocAhKey (u4SecAssocIndex, &OctetStr);

            }
        }

        if (nmhTestv2Fsipv6SecAssocEspAlgo
            (&u4SnmpErrorStatus,
             (INT4) u4SecAssocIndex, u1SecAssocEspAlgo) == SNMP_FAILURE)
        {
            switch (u4SnmpErrorStatus)
            {
                case SNMP_ERR_WRONG_VALUE:
                    CLI_SET_ERR (CLI_IPSECV6_INVALID_ESP_ALGO);
                    break;
                case SNMP_ERR_NO_CREATION:
                    CLI_SET_ERR (CLI_IPSECV6_INVALID_INDEX);
                    break;
                case SNMP_ERR_COMMIT_FAILED:
                    CLI_SET_ERR (CLI_IPSECV6_ESP_FOR_ENCR_ALGO);
                    break;
                case SNMP_ERR_INCONSISTENT_VALUE:
                    CLI_SET_ERR (CLI_IPSECV6_AH_ESP_NULL);
                    break;
                default:
                    break;
            }
            return;
        }

        nmhSetFsipv6SecAssocEspAlgo (u4SecAssocIndex, u1SecAssocEspAlgo);

        if (u1SecAssocEspAlgo != SEC_NULLESPALGO)
        {
            if (pu1SecAssocEspKey == NULL)
            {
                CLI_SET_ERR (CLI_IPSECV6_INVALID_KEY_LENGTH);
                return;
            }
            u1Len = (UINT1) STRLEN (pu1SecAssocEspKey);
            IPSEC_MEMSET (au1OutKey, 0, 33);
            if (Secv6ConstructKey (pu1SecAssocEspKey,
                                   u1Len, au1OutKey) == SEC_FAILURE)
            {
                CLI_SET_ERR (CLI_IPSECV6_INVALID_KEY_LENGTH);
            }
            OctetStr.i4_Length = (INT4) STRLEN (au1OutKey);
            OctetStr.pu1_OctetList = au1OutKey;

            if (nmhTestv2Fsipv6SecAssocEspKey
                (&u4SnmpErrorStatus,
                 (INT4) u4SecAssocIndex, &OctetStr) == SNMP_FAILURE)
            {
                switch (u4SnmpErrorStatus)
                {
                    case SNMP_ERR_WRONG_VALUE:
                        if ((u1SecAssocEspAlgo == SEC_DES_CBC) ||
                            (u1SecAssocEspAlgo == SEC_3DES_CBC))
                        {
                            CLI_SET_ERR (CLI_IPSECV6_INVALID_DES_KEY_LENGTH);
                        }
                        else if (u1SecAssocEspAlgo == SEC_AES)
                        {
                            CLI_SET_ERR (CLI_IPSECV6_INVALID_AES_KEY_LENGTH);
                        }
                        break;
                    case SNMP_ERR_NO_CREATION:
                        CLI_SET_ERR (CLI_IPSECV6_INVALID_INDEX);
                        break;
                    case SNMP_ERR_COMMIT_FAILED:
                        CLI_SET_ERR (CLI_IPSECV6_AH_KEY_FAIL);
                        break;
                    case SNMP_ERR_INCONSISTENT_VALUE:
                        CLI_SET_ERR (CLI_IPSECV6_NULL_ESP_KEY_FAIL);
                        break;
                    default:
                        break;
                }

                return;
            }

            nmhSetFsipv6SecAssocEspKey (u4SecAssocIndex, &OctetStr);

            if (u1SecAssocEspAlgo == SEC_3DES_CBC)
            {
                if (pu1SecAssocEspKey2 == NULL)
                {
                    CLI_SET_ERR (CLI_IPSECV6_INVALID_KEY_LENGTH);
                    return;
                }
                u1Len = (UINT1) STRLEN (pu1SecAssocEspKey2);
                IPSEC_MEMSET (au1OutKey, 0, 33);
                if (Secv6ConstructKey (pu1SecAssocEspKey2,
                                       u1Len, au1OutKey) == SEC_FAILURE)
                {
                    CLI_SET_ERR (CLI_IPSECV6_INVALID_KEY_LENGTH);
                }
                OctetStr.i4_Length = (INT4) STRLEN (au1OutKey);
                OctetStr.pu1_OctetList = au1OutKey;

                if (nmhTestv2Fsipv6SecAssocEspKey2
                    (&u4SnmpErrorStatus,
                     (INT4) u4SecAssocIndex, &OctetStr) == SNMP_FAILURE)
                {
                    switch (u4SnmpErrorStatus)
                    {
                        case SNMP_ERR_WRONG_LENGTH:
                            CLI_SET_ERR (CLI_IPSECV6_INVALID_DES_KEY_LENGTH);
                            break;
                        case SNMP_ERR_NO_CREATION:
                            CLI_SET_ERR (CLI_IPSECV6_INVALID_INDEX);
                            break;
                        case SNMP_ERR_COMMIT_FAILED:
                            CLI_SET_ERR (CLI_IPSECV6_AH_KEY_FAIL);
                            break;
                        case SNMP_ERR_INCONSISTENT_VALUE:
                            CLI_SET_ERR (CLI_IPSECV6_NULL_ESP_KEY_FAIL);
                            break;
                        default:
                            break;
                    }

                    return;
                }

                nmhSetFsipv6SecAssocEspKey2 (u4SecAssocIndex, &OctetStr);
                if (pu1SecAssocEspKey3 == NULL)
                {
                    CLI_SET_ERR (CLI_IPSECV6_INVALID_KEY_LENGTH);
                    return;
                }
                u1Len = (UINT1) STRLEN (pu1SecAssocEspKey3);
                IPSEC_MEMSET (au1OutKey, 0, 33);
                if (Secv6ConstructKey (pu1SecAssocEspKey3,
                                       u1Len, au1OutKey) == SEC_FAILURE)
                {
                    CLI_SET_ERR (CLI_IPSECV6_INVALID_KEY_LENGTH);
                }
                OctetStr.i4_Length = (INT4) STRLEN (au1OutKey);
                OctetStr.pu1_OctetList = au1OutKey;

                if (nmhTestv2Fsipv6SecAssocEspKey3
                    (&u4SnmpErrorStatus,
                     (INT4) u4SecAssocIndex, &OctetStr) == SNMP_FAILURE)
                {
                    switch (u4SnmpErrorStatus)
                    {
                        case SNMP_ERR_WRONG_LENGTH:
                            CLI_SET_ERR (CLI_IPSECV6_INVALID_DES_KEY_LENGTH);
                            break;
                        case SNMP_ERR_NO_CREATION:
                            CLI_SET_ERR (CLI_IPSECV6_INVALID_INDEX);
                            break;
                        case SNMP_ERR_COMMIT_FAILED:
                            CLI_SET_ERR (CLI_IPSECV6_AH_KEY_FAIL);
                            break;
                        case SNMP_ERR_INCONSISTENT_VALUE:
                            CLI_SET_ERR (CLI_IPSECV6_NULL_ESP_KEY_FAIL);
                            break;
                        default:
                            break;
                    }

                    return;
                }

                nmhSetFsipv6SecAssocEspKey3 (u4SecAssocIndex, &OctetStr);
            }
        }
    }

}

/***************************************************************************  
 * FUNCTION NAME : IPSecv6CreateAccess 
 * DESCRIPTION   : This function used to create an entry in access list 
 * RETURNS       : NONE
 ***************************************************************************/
VOID
IPSecv6CreateAccess (tCliHandle CliHandle, UINT4 u4AccessIndex,
                     tIp6Addr * SrcAddress, UINT4 u4SrcMask,
                     tIp6Addr * DestAddress, UINT4 u4DestMask)
{
    UINT4               u4SnmpErrorStatus;
    tSNMP_OCTET_STRING_TYPE OctetStr;
    UINT1               au1Secv6Addr[SEC_ADDR_LEN];

    if (nmhTestv2Fsipv6SecAccessStatus
        (&u4SnmpErrorStatus, (INT4) u4AccessIndex,
         CREATE_AND_GO) == SNMP_FAILURE)
    {
        switch (u4SnmpErrorStatus)
        {
            case SNMP_ERR_WRONG_VALUE:
                CLI_SET_ERR (CLI_IPSECV6_INVALID_INDEX_RANGE);
                break;
            case SNMP_ERR_COMMIT_FAILED:
                CLI_SET_ERR (CLI_IPSECV6_ENABLE_IPSEC);
                break;
            case SNMP_ERR_INCONSISTENT_VALUE:
                CLI_SET_ERR (CLI_IPSECV6_INDEX_EXIST);
                break;
            default:
                break;
        }

        return;
    }

    if (nmhSetFsipv6SecAccessStatus (u4AccessIndex,
                                     CREATE_AND_GO) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
    }

    Ip6AddrCopy ((tIp6Addr *) (VOID *) au1Secv6Addr,
                 (tIp6Addr *) (VOID *) ((SrcAddress)));

    OctetStr.pu1_OctetList = au1Secv6Addr;
    OctetStr.i4_Length = SEC_ADDR_LEN;

    if (nmhTestv2Fsipv6SecSrcNet
        (&u4SnmpErrorStatus, (INT4) u4AccessIndex, &OctetStr) == SNMP_FAILURE)
    {
        switch (u4SnmpErrorStatus)
        {
            case SNMP_ERR_WRONG_LENGTH:
                CLI_SET_ERR (CLI_IPSECV6_INVALID_ADDRESS);
                break;
            case SNMP_ERR_NO_CREATION:
                CLI_SET_ERR (CLI_IPSECV6_INVALID_INDEX);
                break;
            default:
                break;
        }

        return;
    }
    nmhSetFsipv6SecSrcNet (u4AccessIndex, &OctetStr);

    if (nmhTestv2Fsipv6SecSrcAddrPrefixLen
        (&u4SnmpErrorStatus, (INT4) u4AccessIndex,
         (INT4) u4SrcMask) == SNMP_FAILURE)
    {
        switch (u4SnmpErrorStatus)
        {
            case SNMP_ERR_WRONG_VALUE:
                CLI_SET_ERR (CLI_IPSECV6_INVALID_MASK);
                break;
            case SNMP_ERR_NO_CREATION:
                CLI_SET_ERR (CLI_IPSECV6_INVALID_INDEX);
                break;
            default:
                break;
        }

        return;
    }
    nmhSetFsipv6SecSrcAddrPrefixLen (u4AccessIndex, u4SrcMask);

    Ip6AddrCopy ((tIp6Addr *) (VOID *) au1Secv6Addr,
                 (tIp6Addr *) (VOID *) ((DestAddress)));

    OctetStr.pu1_OctetList = au1Secv6Addr;
    OctetStr.i4_Length = SEC_ADDR_LEN;

    if (nmhTestv2Fsipv6SecDestNet
        (&u4SnmpErrorStatus, (INT4) u4AccessIndex, &OctetStr) == SNMP_FAILURE)
    {
        switch (u4SnmpErrorStatus)
        {
            case SNMP_ERR_WRONG_LENGTH:
                CLI_SET_ERR (CLI_IPSECV6_INVALID_ADDRESS);
                break;
            case SNMP_ERR_NO_CREATION:
                CLI_SET_ERR (CLI_IPSECV6_INVALID_INDEX);
                break;
            default:
                break;
        }

        return;
    }

    nmhSetFsipv6SecDestNet (u4AccessIndex, &OctetStr);

    if (nmhTestv2Fsipv6SecDestAddrPrefixLen
        (&u4SnmpErrorStatus, (INT4) u4AccessIndex,
         (INT4) u4DestMask) == SNMP_FAILURE)
    {
        switch (u4SnmpErrorStatus)
        {
            case SNMP_ERR_WRONG_VALUE:
                CLI_SET_ERR (CLI_IPSECV6_INVALID_MASK);
                break;
            case SNMP_ERR_NO_CREATION:
                CLI_SET_ERR (CLI_IPSECV6_INVALID_INDEX);
                break;
            default:
                break;
        }

        return;
    }

    nmhSetFsipv6SecDestAddrPrefixLen (u4AccessIndex, u4DestMask);

}

/***************************************************************************  
 * FUNCTION NAME : IPSecv6CreateSelector 
 * DESCRIPTION   : This function process the InAccess command 
 * RETURNS       : NONE
 ***************************************************************************/
VOID
IPSecv6CreateSelector (tCliHandle CliHandle, UINT4 u4Index, UINT4 u4ProtoId,
                       UINT4 u4Port, UINT4 u4PktDirection,
                       UINT4 u4SelAccessIndex, UINT4 u4SelPolicyIndex,
                       UINT1 u1SelFilterFlag, tIp6Addr * TunnelTermAddr,
                       UINT1 bAddrSet)
{
    UINT4               u4SnmpErrorStatus;
    UINT1               au1Secv6Addr[SEC_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE OctetStr;

    if (nmhTestv2Fsipv6SelStatus
        (&u4SnmpErrorStatus,
         (INT4) u4Index,
         (INT4) u4ProtoId,
         (INT4) u4SelAccessIndex,
         (INT4) u4Port, (INT4) u4PktDirection, CREATE_AND_GO) == SNMP_FAILURE)
    {
        switch (u4SnmpErrorStatus)
        {
            case SNMP_ERR_COMMIT_FAILED:
                CLI_SET_ERR (CLI_IPSECV6_ENABLE_IPSEC);
                break;
            case SNMP_ERR_INCONSISTENT_VALUE:
                CLI_SET_ERR (CLI_IPSECV6_INDEX_EXIST);
                break;
            case SNMP_ERR_WRONG_VALUE:
                CLI_SET_ERR (CLI_IPSECV6_INVALID_SEL_INDEX);
                break;
            default:
                break;
        }

        return;
    }

    if (nmhSetFsipv6SelStatus ((INT4) u4Index,
                               (INT4) u4ProtoId,
                               (INT4) u4SelAccessIndex,
                               (INT4) u4Port,
                               (INT4) u4PktDirection,
                               CREATE_AND_GO) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
    }
    if (nmhTestv2Fsipv6SelFilterFlag (&u4SnmpErrorStatus,
                                      (INT4) u4Index,
                                      (INT4) u4ProtoId,
                                      (INT4) u4SelAccessIndex,
                                      (INT4) u4Port,
                                      (INT4) u4PktDirection,
                                      u1SelFilterFlag) == SNMP_FAILURE)
    {
        switch (u4SnmpErrorStatus)
        {
            case SNMP_ERR_WRONG_VALUE:
                CLI_SET_ERR (CLI_IPSECV6_FLAG_ALLOW_FILTER);
                break;
            case SNMP_ERR_NO_CREATION:
                CLI_SET_ERR (CLI_IPSECV6_INVALID_INDEX);
                break;
            default:
                break;
        }
        return;
    }

    nmhSetFsipv6SelFilterFlag ((INT4) u4Index,
                               (INT4) u4ProtoId,
                               (INT4) u4SelAccessIndex,
                               (INT4) u4Port, (INT4) u4PktDirection,
                               u1SelFilterFlag);

    if (nmhTestv2Fsipv6SelPolicyIndex
        (&u4SnmpErrorStatus,
         (INT4) u4Index,
         (INT4) u4ProtoId,
         (INT4) u4SelAccessIndex,
         (INT4) u4Port, (INT4) u4PktDirection,
         (INT4) u4SelPolicyIndex) == SNMP_FAILURE)
    {
        switch (u4SnmpErrorStatus)
        {
            case SNMP_ERR_WRONG_VALUE:
                CLI_SET_ERR (CLI_IPSECV6_INVALID_SA_INDEX_RANGE);
                break;
            case SNMP_ERR_NO_CREATION:
                CLI_SET_ERR (CLI_IPSECV6_INVALID_INDEX);
                break;
            case SNMP_ERR_COMMIT_FAILED:
                CLI_SET_ERR (CLI_IPSECV6_CREATE_POLICY);
                break;
            default:
                break;
        }
        return;
    }

    nmhSetFsipv6SelPolicyIndex ((INT4) u4Index,
                                (INT4) u4ProtoId,
                                (INT4) u4SelAccessIndex,
                                (INT4) u4Port, (INT4) u4PktDirection,
                                (INT4) u4SelPolicyIndex);

    Ip6AddrCopy ((tIp6Addr *) (VOID *) au1Secv6Addr,
                 (tIp6Addr *) (VOID *) ((TunnelTermAddr)));

    OctetStr.pu1_OctetList = au1Secv6Addr;
    OctetStr.i4_Length = SEC_ADDR_LEN;

    if (bAddrSet == TRUE)
    {
        if (nmhTestv2Fsipv6SelIfIpAddress
            (&u4SnmpErrorStatus,
             (INT4) u4Index,
             (INT4) u4ProtoId,
             (INT4) u4SelAccessIndex,
             (INT4) u4Port, (INT4) u4PktDirection, &OctetStr) == SNMP_FAILURE)
        {
            switch (u4SnmpErrorStatus)
            {
                case SNMP_ERR_WRONG_LENGTH:
                    CLI_SET_ERR (CLI_IPSECV6_INVALID_ADDRESS);
                    break;
                case SNMP_ERR_NO_CREATION:
                    CLI_SET_ERR (CLI_IPSECV6_INVALID_INDEX);
                    break;
                default:
                    break;
            }
            return;
        }

        nmhSetFsipv6SelIfIpAddress (u4Index,
                                    u4ProtoId,
                                    u4SelAccessIndex,
                                    u4Port, u4PktDirection, &OctetStr);
    }

}

/***************************************************************************  
 * FUNCTION NAME : IPSecv6SetAdminStatus 
 * DESCRIPTION   : This function is used to set the admin status of ipsec
 * RETURNS       : NONE
 ***************************************************************************/
VOID
IPSecv6SetAdminStatus (tCliHandle CliHandle, UINT1 u1CliIPSecv6AdminStatus)
{
    UINT4               u4SnmpErrorStatus;

    SEC_UNUSED (CliHandle);

    if (nmhTestv2Fsipv6SecGlobalStatus
        (&u4SnmpErrorStatus, u1CliIPSecv6AdminStatus) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IPSECV6_REPLY_EANBLE_DISABLE);
        return;
    }

    nmhSetFsipv6SecGlobalStatus (u1CliIPSecv6AdminStatus);
}

/***************************************************************************  
 * FUNCTION NAME : IPSecv6SetTraceLevel 
 * DESCRIPTION   : This function is used to set the admin status of ipsec
 * RETURNS       : NONE
 ***************************************************************************/
VOID
IPSecv6SetTraceLevel (tCliHandle CliHandle, UINT4 u4CliIPSecv6Debug)
{
    UINT4               u4SnmpErrorStatus;
    SEC_UNUSED (CliHandle);
    if (nmhTestv2Fsipv6SecGlobalDebug
        (&u4SnmpErrorStatus, (INT4) u4CliIPSecv6Debug) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IPSECV6_INVALID_TRACE);
        return;
    }

    nmhSetFsipv6SecGlobalDebug (u4CliIPSecv6Debug);
}

 /***************************************************************************  
 * FUNCTION NAME : IPSecv6ShowIfStat 
 * DESCRIPTION   : This function is invoked to get Interface specific 
 *               : statistics and store it in the output buffer  
 * RETURNS       : NONE
 ***************************************************************************/
VOID
IPSecv6ShowIfStat (tCliHandle CliHandle)
{
    UINT4               u4Val = 0;

    CliPrintf (CliHandle, "\nIPSec Interface Specific Statistics");

    CliPrintf (CliHandle,
               "\nIface InPkts OutPkts SecuredPkts Discarded Bypassed");

    CliPrintf (CliHandle,
               "\n----- ----- -------- ---------- --------- --------\r\n");

    for (u4Val = 0; u4Val < SEC_MAX_STAT_COUNT; u4Val++)
    {
        if (gatIpsecv6Stat[u4Val].u4Index != 0)
        {

            CliPrintf (CliHandle, "\n%2u", gatIpsecv6Stat[u4Val].u4Index);
            CliPrintf (CliHandle, "%8u", gatIpsecv6Stat[u4Val].u4IfInPkts);
            CliPrintf (CliHandle, "%8u", gatIpsecv6Stat[u4Val].u4IfOutPkts);
            CliPrintf (CliHandle, "%8u", gatIpsecv6Stat[u4Val].u4IfPktsApply);
            CliPrintf (CliHandle, "%10u",
                       gatIpsecv6Stat[u4Val].u4IfPktsDiscard);
            CliPrintf (CliHandle, "%10u", gatIpsecv6Stat[u4Val].u4IfPktsBypass);
        }

    }

    CliPrintf (CliHandle, "\r\n");
}

/***************************************************************************  
 * FUNCTION NAME : IPSecv6ShowAhEspStat 
 * DESCRIPTION   : This function is invoked to get Ah/Esp specific 
 *               : statistics and store it in the output buffer  
 * RETURNS       : NONE
 ***************************************************************************/
VOID
IPSecv6ShowAhEspStat (tCliHandle CliHandle)
{
    UINT4               u4Val = 0;

    CliPrintf (CliHandle, "\nIPSec AH Specific Statistics");

    CliPrintf (CliHandle, "\nIface InAh OutAh AllowAh DiscrdAh");

    CliPrintf (CliHandle, "\n----- ---- ----- ------ --------\r\n");

    for (u4Val = 0; u4Val < SEC_MAX_STAT_COUNT; u4Val++)
    {
        if (gatIpsecv6AhEspStat[u4Val].u4Index != 0)
        {

            CliPrintf (CliHandle, "\n%2u", gatIpsecv6AhEspStat[u4Val].u4Index);
            CliPrintf (CliHandle, "%6u", gatIpsecv6AhEspStat[u4Val].u4InAhPkts);
            CliPrintf (CliHandle, "%6u",
                       gatIpsecv6AhEspStat[u4Val].u4OutAhPkts);
            CliPrintf (CliHandle, "%6u",
                       gatIpsecv6AhEspStat[u4Val].u4AhPktsAllow);
            CliPrintf (CliHandle, "%6u",
                       gatIpsecv6AhEspStat[u4Val].u4AhPktsDiscard);
        }

    }

    CliPrintf (CliHandle, "\nIPSec Esp Specific Statistics");

    CliPrintf (CliHandle, "\nIface InEsp OutEsp AllowEsp DiscrdEsp");

    CliPrintf (CliHandle, "\n----- ----- ------ ------- ---------\r\n");

    for (u4Val = 0; u4Val < SEC_MAX_STAT_COUNT; u4Val++)
    {
        if (gatIpsecv6AhEspStat[u4Val].u4Index != 0)
        {

            CliPrintf (CliHandle, "\n%2u", gatIpsecv6AhEspStat[u4Val].u4Index);
            CliPrintf (CliHandle, "%6u",
                       gatIpsecv6AhEspStat[u4Val].u4InEspPkts);
            CliPrintf (CliHandle, "%6u",
                       gatIpsecv6AhEspStat[u4Val].u4OutEspPkts);
            CliPrintf (CliHandle, "%6u",
                       gatIpsecv6AhEspStat[u4Val].u4EspPktsAllow);
            CliPrintf (CliHandle, "%9u",
                       gatIpsecv6AhEspStat[u4Val].u4EspPktsDiscard);
        }

    }

    CliPrintf (CliHandle, "\r\n");
}

/***************************************************************************  
 * FUNCTION NAME : IPSecv6ShowIntruderStat 
 * DESCRIPTION   : This function is invoked to get intruder specific 
 *               : statistics and store it in the output buffer  
 * RETURNS       : NONE
 ***************************************************************************/
VOID
IPSecv6ShowIntruderStat (tCliHandle CliHandle)
{
    UINT4               u4Val = 0;

    CliPrintf (CliHandle, "\nIPSec Intruder Statistics");

    CliPrintf (CliHandle,
               "\nIface IntruSrcAddr IntruDestAddr IntruProtocol Time");

    CliPrintf (CliHandle,
               "\n----- ------------ ------------- ------------- ----\r\n");

    for (u4Val = 0; u4Val < SEC_MAX_STAT_COUNT; u4Val++)
    {
        if (gatIpsecv6AhEspIntruStat[u4Val].u4Index != 0)
        {

            CliPrintf (CliHandle, "\n%3u",
                       gatIpsecv6AhEspIntruStat[u4Val].u4IfIndex);

            CliPrintf (CliHandle, "%16s",
                       Ip6PrintNtop (&(gatIpsecv6AhEspIntruStat[u4Val].
                                       AhEspIntruSrcAddr)));

            CliPrintf (CliHandle, "%16s",
                       Ip6PrintNtop (&(gatIpsecv6AhEspIntruStat[u4Val].
                                       AhEspIntruDestAddr)));

            CliPrintf (CliHandle, "%6u", gatIpsecv6AhEspIntruStat[u4Val].
                       u4AhEspIntruProt);

            CliPrintf (CliHandle, "%10u", gatIpsecv6AhEspIntruStat[u4Val].
                       u4AhEspIntruTime);
        }

    }

    CliPrintf (CliHandle, "\r\n");
}

/***************************************************************************  
 * FUNCTION NAME : IPSecv6ShowPolicyEntries 
 * DESCRIPTION   : This function is invoked to get policy 
 *               : entries and store it in the output buffer  
 * RETURNS       : NONE
 ***************************************************************************/
VOID
IPSecv6ShowPolicyEntries (tCliHandle CliHandle, UINT4 u4Index)
{
    tSecv6Policy       *pSecv6Policy = NULL;

    CliPrintf (CliHandle, "\nIPSec Policy Table");

    if (u4Index == 0)
    {
        TMO_SLL_Scan (&Secv6PolicyList, pSecv6Policy, tSecv6Policy *)
        {

            CliPrintf (CliHandle, "\n%6u", pSecv6Policy->u4PolicyIndex);
            if (pSecv6Policy->u1PolicyFlag == SEC_APPLY)
            {
                CliPrintf (CliHandle, "%12s", "APPLY");
            }
            else
            {

                CliPrintf (CliHandle, "%12s", "BYPASS");
            }

            if (pSecv6Policy->u1PolicyMode == SEC_MANUAL)
            {
                CliPrintf (CliHandle, "%12s", "MANUAL");
            }
            else
            {

                CliPrintf (CliHandle, "%12s", "AUTOMATIC");
            }

            CliPrintf (CliHandle, "%5s", pSecv6Policy->au1PolicySaBundle);
        }
    }
    else
    {
        TMO_SLL_Scan (&Secv6PolicyList, pSecv6Policy, tSecv6Policy *)
        {

            if (pSecv6Policy->u4PolicyIndex == u4Index)
            {
                CliPrintf (CliHandle, "\n%6u", pSecv6Policy->u4PolicyIndex);

                if (pSecv6Policy->u1PolicyFlag == SEC_APPLY)
                {
                    CliPrintf (CliHandle, "%12s", "APPLY");
                }
                else
                {

                    CliPrintf (CliHandle, "%12s", "BYPASS");
                }

                if (pSecv6Policy->u1PolicyMode == SEC_MANUAL)
                {
                    CliPrintf (CliHandle, "%12s", "MANUAL");
                }
                else
                {

                    CliPrintf (CliHandle, "%12s", "AUTOMATIC");
                }

                CliPrintf (CliHandle, "%5s", pSecv6Policy->au1PolicySaBundle);
            }

        }

    }
    CliPrintf (CliHandle, "\r\n");
}

/***************************************************************************  
 * FUNCTION NAME : IPSecv6ShowSelectorEntries 
 * DESCRIPTION   : This function is invoked to get selector 
 *               : entries and store it in the output buffer  
 * RETURNS       : NONE
 ***************************************************************************/
VOID
IPSecv6ShowSelectorEntries (tCliHandle CliHandle, UINT4 u4Index)
{
    tSecv6Selector     *pSecv6Selector = NULL;
    UINT1               au1TempAddr[SEC_ADDR_LEN] =
        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

    CliPrintf (CliHandle, "\nIPSec Selector Table");

    if (u4Index == 0)
    {
        TMO_SLL_Scan (&Secv6SelList, pSecv6Selector, tSecv6Selector *)
        {

            CliPrintf (CliHandle, "\n Position of entry in List:\t %u",
                       pSecv6Selector->u4Count);
            if (pSecv6Selector->u4IfIndex != 0)
            {
                CliPrintf (CliHandle, "\n%7u", pSecv6Selector->u4IfIndex);
            }
            else
            {

                CliPrintf (CliHandle, "\n%7s", "ANY");

            }

            switch (pSecv6Selector->u4ProtoId)
            {
                case SEC_TCP:
                    CliPrintf (CliHandle, "%7s", "TCP");
                    break;
                case SEC_UDP:
                    CliPrintf (CliHandle, "%7s", "UDP");
                    break;
                case SEC_ICMPv6:
                    CliPrintf (CliHandle, "%7s", "ICMPv6");
                    break;
                case SEC_AH:
                    CliPrintf (CliHandle, "%7s", "AH");
                    break;
                case SEC_ESP:
                    CliPrintf (CliHandle, "%7s", "ESP");
                    break;
                case SEC_ANY_PROTOCOL:
                    CliPrintf (CliHandle, "%7s", "ANY");
                    break;
                default:
                    CliPrintf (CliHandle, "%7s", "Unknown");
                    break;
            }

            if (pSecv6Selector->u4Port == SEC_ANY_PORT)
            {
                CliPrintf (CliHandle, "%7s", "ANY");
            }
            else
            {

                CliPrintf (CliHandle, "%7u", pSecv6Selector->u4Port);
            }

            if (pSecv6Selector->u4PktDirection == SEC_INBOUND)
            {
                CliPrintf (CliHandle, "%7s", "IN");
            }
            if (pSecv6Selector->u4PktDirection == SEC_OUTBOUND)
            {

                CliPrintf (CliHandle, "%7s", "OUT");
            }

            if (pSecv6Selector->u4PktDirection == SEC_ANY_DIRECTION)
            {
                CliPrintf (CliHandle, "%7s", "ANY");
            }

            CliPrintf (CliHandle, "%7u", pSecv6Selector->u4SelAccessIndex);

            CliPrintf (CliHandle, "%7u", pSecv6Selector->u4SelPolicyIndex);

            if (pSecv6Selector->u1SelFilterFlag == SEC_FILTER)
            {
                CliPrintf (CliHandle, "%8s", "filter");
            }
            else
            {

                CliPrintf (CliHandle, "%8s", "allow");
            }

            if (IPSEC_MEMCMP (pSecv6Selector->TunnelTermAddr.u1_addr,
                              au1TempAddr, SEC_ADDR_LEN) != 0)
            {
                CliPrintf (CliHandle, "%16s",
                           Ip6PrintNtop (&(pSecv6Selector->TunnelTermAddr)));
            }
        }
    }
    else
    {

        TMO_SLL_Scan (&Secv6SelList, pSecv6Selector, tSecv6Selector *)
        {
            if (u4Index == pSecv6Selector->u4Count)
            {
                if (pSecv6Selector->u4IfIndex != 0)
                {
                    CliPrintf (CliHandle, "\n%7u", pSecv6Selector->u4IfIndex);
                }

                else
                {

                    CliPrintf (CliHandle, "\n%7s", "ANY");

                }

                switch (pSecv6Selector->u4ProtoId)
                {
                    case SEC_TCP:
                        CliPrintf (CliHandle, "%7s", "TCP");
                        break;
                    case SEC_UDP:
                        CliPrintf (CliHandle, "%7s", "UDP");
                        break;
                    case SEC_ICMPv6:
                        CliPrintf (CliHandle, "%7s", "ICMPv6");
                        break;
                    case SEC_AH:
                        CliPrintf (CliHandle, "%7s", "AH");
                        break;
                    case SEC_ESP:
                        CliPrintf (CliHandle, "%7s", "ESP");
                        break;
                    case SEC_ANY_PROTOCOL:
                        CliPrintf (CliHandle, "%7s", "ANY");
                        break;
                    default:
                        CliPrintf (CliHandle, "%7s", "Unknown");
                        break;
                }

                if (pSecv6Selector->u4Port == SEC_ANY_PORT)
                {
                    CliPrintf (CliHandle, "%7s", "ANY");
                }
                else
                {

                    CliPrintf (CliHandle, "%7u", pSecv6Selector->u4Port);
                }

                if (pSecv6Selector->u4PktDirection == SEC_INBOUND)
                {
                    CliPrintf (CliHandle, "%7s", "IN");
                }
                if (pSecv6Selector->u4PktDirection == SEC_OUTBOUND)
                {

                    CliPrintf (CliHandle, "%7s", "OUT");
                }

                if (pSecv6Selector->u4PktDirection == SEC_ANY_DIRECTION)
                {
                    CliPrintf (CliHandle, "%7s", "ANY");
                }

                CliPrintf (CliHandle, "%7u", pSecv6Selector->u4SelAccessIndex);

                CliPrintf (CliHandle, "%7u", pSecv6Selector->u4SelPolicyIndex);

                if (pSecv6Selector->u1SelFilterFlag == SEC_FILTER)
                {
                    CliPrintf (CliHandle, "%8s", "filter");
                }
                else
                {

                    CliPrintf (CliHandle, "%8s", "allow");
                }

                if (IPSEC_MEMCMP (pSecv6Selector->TunnelTermAddr.u1_addr,
                                  au1TempAddr, SEC_ADDR_LEN) != 0)
                {
                    CliPrintf (CliHandle, "%16s",
                               Ip6PrintNtop (&
                                             (pSecv6Selector->TunnelTermAddr)));
                }
            }
        }
    }
    CliPrintf (CliHandle, "\r\n");
}

/***************************************************************************  
 * FUNCTION NAME : IPSecv6ShowSecAssocEntries 
 * DESCRIPTION   : This function is invoked to get security association 
 *               : entries and store it in the output buffer  
 * RETURNS       : NONE
 ***************************************************************************/
VOID
IPSecv6ShowSecAssocEntries (tCliHandle CliHandle, UINT4 u4Index)
{
    tSecv6Assoc        *pSecv6SecAssoc = NULL;

    CliPrintf (CliHandle, "\nIPSec SecAssoc Table");
    if (u4Index == 0)
    {
        TMO_SLL_Scan (&Secv6AssocList, pSecv6SecAssoc, tSecv6Assoc *)
        {

            CliPrintf (CliHandle, "\n%6u", pSecv6SecAssoc->u4SecAssocIndex);
            CliPrintf (CliHandle, "\t0x%x", pSecv6SecAssoc->u4SecAssocSpi);

            CliPrintf (CliHandle, "%16s",
                       Ip6PrintNtop (&(pSecv6SecAssoc->SecAssocDestAddr)));

            if (pSecv6SecAssoc->u1SecAssocMode == SEC_TRANSPORT)
            {
                CliPrintf (CliHandle, "%12s", "TRANSPORT");
            }

            if (pSecv6SecAssoc->u1SecAssocMode == SEC_TUNNEL)
            {
                CliPrintf (CliHandle, "%12s", "TUNNEL");
            }

            if (pSecv6SecAssoc->u1AntiReplayStatus == SEC_ANTI_REPLAY_ENABLE)
            {
                CliPrintf (CliHandle, "%12s", "AR_ENABLE");
            }

            if (pSecv6SecAssoc->u1AntiReplayStatus == SEC_ANTI_REPLAY_DISABLE)
            {
                CliPrintf (CliHandle, "%12s", "AR_DISABLE");
            }

            if (pSecv6SecAssoc->u1SecAssocProtocol == SEC_AH)
            {
                CliPrintf (CliHandle, "%12s", "AH");
            }
            if (pSecv6SecAssoc->u1SecAssocProtocol == SEC_ESP)
            {

                CliPrintf (CliHandle, "%12s", "ESP");
            }

            switch (pSecv6SecAssoc->u1SecAssocAhAlgo)
            {
                case SEC_NULLAHALGO:
                    CliPrintf (CliHandle, "%12s", "NULL");
                    break;
                case SEC_MD5:
                    CliPrintf (CliHandle, "%12s", "MD5");
                    break;
                case SEC_KEYEDMD5:
                    CliPrintf (CliHandle, "%12s", "KeyedMD5");
                    break;
                case SEC_HMACMD5:
                    CliPrintf (CliHandle, "%12s", "HmacMD5");
                    break;
                case SEC_HMACSHA1:
                    CliPrintf (CliHandle, "%12s", "HmacSha");
                    break;
                case SEC_XCBCMAC:
                    CliPrintf (CliHandle, "%12s", "XcbcMac");
                    break;
                case HMAC_SHA_256:
                    CliPrintf (CliHandle, "%12s", "HmacSha256");
                    break;
                case HMAC_SHA_384:
                    CliPrintf (CliHandle, "%12s", "HmacSha384");
                    break;
                case HMAC_SHA_512:
                    CliPrintf (CliHandle, "%12s", "HmacSha512");
                    break;
                default:
                    CliPrintf (CliHandle, "%7s", "Unknown");
                    break;
            }

            if (pSecv6SecAssoc->u1SecAssocProtocol == SEC_ESP)
            {

                switch (pSecv6SecAssoc->u1SecAssocEspAlgo)
                {
                    case SEC_NULLESPALGO:
                        CliPrintf (CliHandle, "%12s", "NULL");
                        break;
                    case SEC_DES_CBC:
                        CliPrintf (CliHandle, "%12s", "DES-CBC");
                        break;
                    case SEC_3DES_CBC:
                        CliPrintf (CliHandle, "%12s", "3DES-CBC");
                        break;
                    case SEC_AES:
                        CliPrintf (CliHandle, "%12s", "AES");
                        break;
                    default:
                        CliPrintf (CliHandle, "%7s", "Unknown");
                        break;
                }
            }

            CliPrintf (CliHandle, "\r\n");
        }
    }
    else
    {
        TMO_SLL_Scan (&Secv6AssocList, pSecv6SecAssoc, tSecv6Assoc *)
        {

            if (u4Index == pSecv6SecAssoc->u4SecAssocIndex)
            {
                CliPrintf (CliHandle, "\n%6u", pSecv6SecAssoc->u4SecAssocIndex);
                CliPrintf (CliHandle, "\t0x%x", pSecv6SecAssoc->u4SecAssocSpi);

                CliPrintf (CliHandle, "%16s",
                           Ip6PrintNtop (&(pSecv6SecAssoc->SecAssocDestAddr)));

                if (pSecv6SecAssoc->u1SecAssocMode == SEC_TRANSPORT)
                {
                    CliPrintf (CliHandle, "%12s", "TRANSPORT");
                }

                if (pSecv6SecAssoc->u1SecAssocMode == SEC_TUNNEL)
                {
                    CliPrintf (CliHandle, "%12s", "TUNNEL");
                }

                if (pSecv6SecAssoc->u1SecAssocProtocol == SEC_AH)
                {
                    CliPrintf (CliHandle, "%12s", "AH");
                }
                if (pSecv6SecAssoc->u1SecAssocProtocol == SEC_ESP)
                {

                    CliPrintf (CliHandle, "%12s", "ESP");
                }

                switch (pSecv6SecAssoc->u1SecAssocAhAlgo)
                {
                    case SEC_NULLAHALGO:
                        CliPrintf (CliHandle, "%12s", "NULL");
                        break;
                    case SEC_MD5:
                        CliPrintf (CliHandle, "%12s", "MD5");
                        break;
                    case SEC_KEYEDMD5:
                        CliPrintf (CliHandle, "%12s", "KeyedMD5");
                        break;
                    case SEC_HMACMD5:
                        CliPrintf (CliHandle, "%12s", "HmacMD5");
                        break;
                    case SEC_HMACSHA1:
                        CliPrintf (CliHandle, "%12s", "HmacSha");
                        break;
                    default:
                        CliPrintf (CliHandle, "%7s", "Unknown");
                        break;
                }

                if (pSecv6SecAssoc->u1SecAssocProtocol == SEC_ESP)
                {

                    switch (pSecv6SecAssoc->u1SecAssocEspAlgo)
                    {
                        case SEC_NULLESPALGO:
                            CliPrintf (CliHandle, "%12s", "NULL");
                            break;
                        case SEC_DES_CBC:
                            CliPrintf (CliHandle, "%12s", "DES-CBC");
                            break;
                        case SEC_3DES_CBC:
                            CliPrintf (CliHandle, "%12s", "3DES-CBC");
                            break;
                        case SEC_AES:
                            CliPrintf (CliHandle, "%12s", "AES");
                            break;
                        default:
                            CliPrintf (CliHandle, "%7s", "Unknown");
                            break;
                    }
                }

            }

            CliPrintf (CliHandle, "\r\n");
        }
    }
    CliPrintf (CliHandle, "\n");
}

/***************************************************************************  
 * FUNCTION NAME : IPSecv6ShowAccessEntries 
 * DESCRIPTION   : This function is invoked to get access 
 *               : entries and store it in the output buffer  
 * RETURNS       : NONE
 ***************************************************************************/
VOID
IPSecv6ShowAccessEntries (tCliHandle CliHandle, UINT4 u4Index)
{
    tSecv6Access       *pSecv6Access = NULL;

    CliPrintf (CliHandle, "\nIPSec Access Table");
    if (u4Index == 0)
    {
        TMO_SLL_Scan (&Secv6AccessList, pSecv6Access, tSecv6Access *)
        {

            CliPrintf (CliHandle, "\n%3u", pSecv6Access->u4GroupIndex);

            CliPrintf (CliHandle, "%16s",
                       Ip6PrintNtop (&(pSecv6Access->SrcAddress)));

            CliPrintf (CliHandle, "%16d", pSecv6Access->u4SrcAddrPrefixLen);

            CliPrintf (CliHandle, "%16s",
                       Ip6PrintNtop (&(pSecv6Access->DestAddress)));

            CliPrintf (CliHandle, "%16d", pSecv6Access->u4DestAddrPrefixLen);

        }

    }

    else
    {

        TMO_SLL_Scan (&Secv6AccessList, pSecv6Access, tSecv6Access *)
        {

            if (u4Index == pSecv6Access->u4GroupIndex)
            {
                CliPrintf (CliHandle, "\n%3u", pSecv6Access->u4GroupIndex);

                CliPrintf (CliHandle, "%16s",
                           Ip6PrintNtop (&(pSecv6Access->SrcAddress)));

                CliPrintf (CliHandle, "%16d", pSecv6Access->u4SrcAddrPrefixLen);

                CliPrintf (CliHandle, "%16s",
                           Ip6PrintNtop (&(pSecv6Access->DestAddress)));

                CliPrintf (CliHandle, "%16d",
                           pSecv6Access->u4DestAddrPrefixLen);

            }

        }

    }
    CliPrintf (CliHandle, "\r\n");
}

/****************************************************************************/
/* FUNCTION NAME : IPSecv6ShowGlobalEntries                                 */
/* DESCRIPTION   : This function is invoked to get access                   */
/*               : entries and store it in the output buffer                */
/* RETURNS       : NONE                                                     */
/****************************************************************************/
VOID
IPSecv6ShowGlobalEntries (tCliHandle CliHandle)
{

    CliPrintf (CliHandle, "\nIPSec Global Configurations\r\n");
    if (gSecv6Status == SEC_ENABLE)
    {
        CliPrintf (CliHandle, "IPSec AdminStatus\t: %s\r\n", "ENABLE");
    }
    else
    {
        CliPrintf (CliHandle, "IPSec AdminStatus\t: %s\r\n", "DISABLE");
    }

    switch (gu4Secv6Debug)
    {

        case SECv6_DISABLE_ALL_TRC:
            CliPrintf (CliHandle, "IPSec Trace Level\t: %s\r\n", "DISABLE-ALL");
            break;

        case SECv6_ENABLE_ALL_TRC:
            CliPrintf (CliHandle, "IPSec Trace Level\t: %s\r\n", "ENABLE-ALL");
            break;
        case INIT_SHUT_TRC:
            CliPrintf (CliHandle, "IPSec Trace Level\t: %s\r\n", "INIT-SHUT");
            break;
        case MGMT_TRC:
            CliPrintf (CliHandle, "IPSec Trace Level\t: %s\r\n", "MGMT");
            break;
        case DATA_PATH_TRC:
            CliPrintf (CliHandle, "IPSec Trace Level\t: %s\r\n", "DATA-PATH");
            break;
        case CONTROL_PLANE_TRC:
            CliPrintf (CliHandle, "IPSec Trace Level\t: %s\r\n", "CTRL-PLANE");
            break;
        case DUMP_TRC:
            CliPrintf (CliHandle, "IPSec Trace Level\t: %s\r\n", "DUMP");
            break;
        case OS_RESOURCE_TRC:
            CliPrintf (CliHandle, "IPSec Trace Level\t: %s\r\n", "OS-RESOURCE");
            break;
        case ALL_FAILURE_TRC:
            CliPrintf (CliHandle, "IPSec Trace Level\t: %s\r\n", "ALL-FAILURE");
            break;
        case BUFFER_TRC:
            CliPrintf (CliHandle, "IPSec Trace Level\t: %s\r\n", "BUFFER-TRC");
            break;
        default:
            CliPrintf (CliHandle, "IPSec Trace Level\t: 0x%x\r\n",
                       gu4Secv6Debug);
            break;
    }

    CliPrintf (CliHandle, "IPSec Version\t: %u\r\n", gSecv6Version);
    CliPrintf (CliHandle, "IPSec Max SA \t: %u\r\n", MAX_IPSEC6_MAX_SA);
    CliPrintf (CliHandle, "\r\n");
}

/***************************************************************************  
 * FUNCTION NAME : IPSecv6DeletePolicyEntries 
 * DESCRIPTION   : This function is invoked to delete policy 
 *               : entries   
 * RETURNS       : NONE
 ***************************************************************************/
VOID
IPSecv6DeletePolicyEntries (tCliHandle CliHandle, UINT4 u4Index)
{
    UINT4               u4SnmpErrorStatus;
    SEC_UNUSED (CliHandle);

    if (nmhTestv2Fsipv6SecPolicyStatus
        (&u4SnmpErrorStatus, (INT4) u4Index, DESTROY) == SNMP_FAILURE)
    {
        switch (u4SnmpErrorStatus)
        {

            case SNMP_ERR_WRONG_VALUE:
                CLI_SET_ERR (CLI_IPSECV6_INVALID_INDEX);
                break;
            case SNMP_ERR_COMMIT_FAILED:
                CLI_SET_ERR (CLI_IPSECV6_ENABLE_IPSEC);
                break;
            case SNMP_ERR_NO_CREATION:
                CLI_SET_ERR (CLI_IPSECV6_INVALID_SELECTOR);
                break;
            default:
                break;
        }
        return;

    }

    nmhSetFsipv6SecPolicyStatus ((INT4) u4Index, DESTROY);
}

/***************************************************************************  
 * FUNCTION NAME : IPSecv6DeleteSelectorEntries 
 * DESCRIPTION   : This function is invoked to delete selector 
 *               : entries   
 * RETURNS       : NONE
 ***************************************************************************/
VOID
IPSecv6DeleteSelectorEntries (tCliHandle CliHandle, UINT4 u4Index)
{
    tSecv6Selector     *pSecv6Selector = NULL;
    UINT4               u4SnmpErrorStatus;
    UINT4               u4Entries = 0, u4Count = 0;

    SEC_UNUSED (CliHandle);

    if (u4Index != 0)
    {

        TMO_SLL_Scan (&Secv6SelList, pSecv6Selector, tSecv6Selector *)
        {
            if (u4Index == pSecv6Selector->u4Count)
            {

                break;
            }
        }

        if (pSecv6Selector != NULL)
        {
            if (nmhTestv2Fsipv6SelStatus (&u4SnmpErrorStatus,
                                          (INT4) pSecv6Selector->u4IfIndex,
                                          (INT4) pSecv6Selector->u4ProtoId,
                                          (INT4) pSecv6Selector->
                                          u4SelAccessIndex,
                                          (INT4) pSecv6Selector->u4Port,
                                          (INT4) pSecv6Selector->u4PktDirection,
                                          DESTROY) == SNMP_FAILURE)
            {
                switch (u4SnmpErrorStatus)
                {

                    case SNMP_ERR_COMMIT_FAILED:
                        CLI_SET_ERR (CLI_IPSECV6_ENABLE_IPSEC);
                        break;
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                        CLI_SET_ERR (CLI_IPSECV6_INVALID_INDEX);
                        break;
                    default:
                        break;
                }

                return;
            }

            nmhSetFsipv6SelStatus (pSecv6Selector->u4IfIndex,
                                   (INT4) pSecv6Selector->u4ProtoId,
                                   (INT4) pSecv6Selector->u4SelAccessIndex,
                                   (INT4) pSecv6Selector->u4Port,
                                   (INT4) pSecv6Selector->u4PktDirection,
                                   DESTROY);
        }
    }
    else
    {

        u4Entries = TMO_SLL_Count (&Secv6SelList);
        for (u4Count = 0; u4Count < u4Entries; u4Count++)
        {

            TMO_SLL_Scan (&Secv6SelList, pSecv6Selector, tSecv6Selector *)
            {

                if (pSecv6Selector != NULL)
                {
                    break;
                }
            }
            if (pSecv6Selector != NULL)
            {
                if (nmhTestv2Fsipv6SelStatus (&u4SnmpErrorStatus,
                                              (INT4) pSecv6Selector->u4IfIndex,
                                              (INT4) pSecv6Selector->u4ProtoId,
                                              (INT4) pSecv6Selector->
                                              u4SelAccessIndex,
                                              (INT4) pSecv6Selector->u4Port,
                                              (INT4) pSecv6Selector->
                                              u4PktDirection,
                                              DESTROY) == SNMP_FAILURE)
                {
                    switch (u4SnmpErrorStatus)
                    {
                        case SNMP_ERR_COMMIT_FAILED:
                            CLI_SET_ERR (CLI_IPSECV6_ENABLE_IPSEC);
                            break;
                        case SNMP_ERR_WRONG_VALUE:
                        case SNMP_ERR_NO_CREATION:
                            CLI_SET_ERR (CLI_IPSECV6_INVALID_INDEX);
                            break;
                        default:
                            break;
                    }

                    return;
                }
                nmhSetFsipv6SelStatus ((INT4) pSecv6Selector->u4IfIndex,
                                       (INT4) pSecv6Selector->u4ProtoId,
                                       (INT4) pSecv6Selector->u4SelAccessIndex,
                                       (INT4) pSecv6Selector->u4Port,
                                       (INT4) pSecv6Selector->u4PktDirection,
                                       DESTROY);
            }

        }

    }

}

/***************************************************************************/
/* FUNCTION NAME : IPSecv6DeleteSecAssocEntries                            */
/* DESCRIPTION   : This function is invoked to delete security             */
/*               : association entries                                     */
/* RETURNS       : NONE                                                    */
/***************************************************************************/
VOID
IPSecv6DeleteSecAssocEntries (tCliHandle CliHandle, UINT4 u4Index)
{
    UINT4               u4SnmpErrorStatus = SNMP_FAILURE;
    UINT4               u4Entries = 0, u4Count = 0;
    tSecv6Assoc        *pSecv6Assoc = NULL;

    SEC_UNUSED (CliHandle);

    if (u4Index != 0)
    {
        if (nmhTestv2Fsipv6SecAssocStatus (&u4SnmpErrorStatus,
                                           (INT4) u4Index,
                                           DESTROY) == SNMP_FAILURE)
        {

            switch (u4SnmpErrorStatus)
            {

                case SNMP_ERR_WRONG_VALUE:
                case SNMP_ERR_NO_CREATION:
                    CLI_SET_ERR (CLI_IPSECV6_INVALID_INDEX);
                    break;
                default:
                    break;
            }

            return;

        }

        nmhSetFsipv6SecAssocStatus (u4Index, DESTROY);
    }
    else
    {
        u4Entries = TMO_SLL_Count (&Secv6AssocList);
        for (u4Count = 0; u4Count < u4Entries; u4Count++)
        {
            pSecv6Assoc = (tSecv6Assoc *) TMO_SLL_First (&Secv6AssocList);
            if (pSecv6Assoc == NULL)
            {
                break;
            }
            if (nmhTestv2Fsipv6SecAssocStatus (&u4SnmpErrorStatus,
                                               (INT4) pSecv6Assoc->
                                               u4SecAssocIndex,
                                               DESTROY) == SNMP_FAILURE)
            {
                switch (u4SnmpErrorStatus)
                {

                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                        CLI_SET_ERR (CLI_IPSECV6_INVALID_INDEX);
                        break;
                    default:
                        break;
                }

                return;
            }

            nmhSetFsipv6SecAssocStatus (pSecv6Assoc->u4SecAssocIndex, DESTROY);
        }

    }
    return;
}

/***************************************************************************  
 * FUNCTION NAME : IPSecv6DeleteAccessEntries 
 * DESCRIPTION   : This function is invoked to delete policy 
 *               : entries   
 * RETURNS       : NONE
 ***************************************************************************/
VOID
IPSecv6DeleteAccessEntries (tCliHandle CliHandle, UINT4 u4Index)
{
    UINT4               u4SnmpErrorStatus;
    SEC_UNUSED (CliHandle);

    if (nmhTestv2Fsipv6SecAccessStatus
        (&u4SnmpErrorStatus, (INT4) u4Index, DESTROY) == SNMP_FAILURE)
    {
        switch (u4SnmpErrorStatus)
        {

            case SNMP_ERR_WRONG_VALUE:
            case SNMP_ERR_NO_CREATION:
                CLI_SET_ERR (CLI_IPSECV6_INVALID_INDEX);
                break;
            case SNMP_ERR_COMMIT_FAILED:
                CLI_SET_ERR (CLI_IPSECV6_ENABLE_IPSEC);
                break;
            default:
                break;
        }
        return;
    }
    nmhSetFsipv6SecAccessStatus (u4Index, DESTROY);

}

/***************************************************************************
 * FUNCTION NAME : IPSecv6CryptoConfigMode
 * DESCRIPTION   : This function is used to enter in to crypto transform
 *               : configuration mode
 * RETURNS       : NONE
 ***************************************************************************/
VOID
IPSecv6CryptoConfigMode (tCliHandle CliHandle)
{
    UINT1               au1IfName[13] = "CryptoConfig";
    SEC_UNUSED (CliHandle);
    CliChangePath ((CHR1 *) au1IfName);
    return;
}

/***************************************************************************
 *     FUNCTION NAME    : IPSecv6GetCryptoPrompt                             
 *     DESCRIPTION      : This function validates the given pi1ModeName     
 *                        and returns the router configuration prompt in    
 *                        pi1DispStr if valid                               
 *                        Returns TRUE if given pi1ModeName is valid        
 *                        Returns FALSE if given pi1ModeName is not valid   
 *                        pi1ModeName is NULL to display the mode tree with 
 *                        mode name and prompt string.                      
 *     INPUT            : pi1ModeName - Mode Name                           
 *     OUTPUT           : pi1DispStr - DIsplay string                       
 *     RETURNS          : True/False                                        
 **************************************************************************/
INT1
IPSecv6GetCryptoPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len;

    if (pi1DispStr == NULL)
    {
        return FALSE;
    }

    if (pi1ModeName == NULL)
    {
        return FALSE;
    }

    u4Len = STRLEN (CLI_CRYPTO_CONFIG_MODE);
    if (STRNCMP (pi1ModeName, CLI_CRYPTO_CONFIG_MODE, u4Len) != 0)
    {
        return FALSE;
    }

    STRCPY (pi1DispStr, "(config-crypto)#");
    return TRUE;
}

UINT1
cli_get_ipsecv6_secassoc_protocol (UINT1 *pu1Prot)
{
    if (CLI_STRCASECMP (pu1Prot, "ah") == 0)
    {
        return (UINT1) CLI_ATOI (CLI_AH);
    }
    if (CLI_STRCASECMP (pu1Prot, "esp") == 0)
    {
        return (UINT1) CLI_ATOI (CLI_ESP);
    }
    return 0;
}

UINT1
cli_get_ipsecv6_secassoc_ahalgo (UINT1 *pu1Prot)
{
    if (CLI_STRCASECMP (pu1Prot, "null") == 0)
    {
        return (UINT1) CLI_ATOI (CLI_NULL);
    }
    if (CLI_STRCASECMP (pu1Prot, "md5") == 0)
    {
        return (UINT1) CLI_ATOI (CLI_MD5);
    }
    if (CLI_STRCASECMP (pu1Prot, "keyedmd5") == 0)
    {
        return (UINT1) CLI_ATOI (CLI_KEYEDMD5);
    }
    if (CLI_STRCASECMP (pu1Prot, "hmacmd5") == 0)
    {
        return (UINT1) CLI_ATOI (CLI_HMACMD5);
    }
    if (CLI_STRCASECMP (pu1Prot, "hmacsha1") == 0)
    {
        return (UINT1) CLI_ATOI (CLI_HMACSHA);
    }
    return 5;
}

UINT1
cli_get_ipsecv6_secassoc_espalgo (UINT1 *pu1Prot)
{
    if (CLI_STRCASECMP (pu1Prot, "null") == 0)
    {
        return (UINT1) CLI_ATOI (CLI_ESP_NULL);
    }
    if (CLI_STRCASECMP (pu1Prot, "des-cbc") == 0)
    {
        return (UINT1) CLI_ATOI (CLI_DESCBC);
    }
    if (CLI_STRCASECMP (pu1Prot, "3des-cbc") == 0)
    {
        return (UINT1) CLI_ATOI (CLI_3DESCBC);
    }
    if (CLI_STRCASECMP (pu1Prot, "aes") == 0)
    {
        return (UINT1) CLI_ATOI (CLI_AES);
    }
    return 13;
}

UINT4
cli_get_ipsecv6_selector_protocol (UINT1 *pu1Prot)
{
    if (CLI_STRCASECMP (pu1Prot, "tcp") == 0)
    {
        return (UINT4) CLI_ATOL (CLI_TCP);
    }
    if (CLI_STRCASECMP (pu1Prot, "udp") == 0)
    {
        return (UINT4) CLI_ATOL (CLI_UDP);
    }
    if (CLI_STRCASECMP (pu1Prot, "icmpv6") == 0)
    {
        return (UINT4) CLI_ATOL (CLI_ICMPV6);
    }
    if (CLI_STRCASECMP (pu1Prot, "ah") == 0)
    {
        return (UINT4) CLI_ATOL (CLI_AH);
    }
    if (CLI_STRCASECMP (pu1Prot, "esp") == 0)
    {
        return (UINT4) CLI_ATOL (CLI_ESP);
    }
    if (CLI_STRCASECMP (pu1Prot, "any") == 0)
    {
        return (UINT4) CLI_ATOL (CLI_ANY_PROTOCOL);
    }
    return 0;
}

/***************************************************************************
 *     FUNCTION NAME    : IPSecv6ShowRunningconfig
 *     DESCRIPTION      : Displays IPSECv6 running configurations      
 *     INPUT            : CliHandle
 *     OUTPUT           : None                       
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            
 **************************************************************************/
INT4
IPSecv6ShowRunningconfig (tCliHandle CliHandle)
{
    INT4                i4Status = 0;

    if (IPSecCheckAnyConfigDone (CliHandle) == CLI_SUCCESS)
    {
        CliPrintf (CliHandle, "crypto ipsecv6\r\n");
    }
    else
    {
        return CLI_SUCCESS;
    }

    /* SecAssocTable */
    SecAssocTableShowRunningconfig (CliHandle);

    /* SecPolicyTable */
    SecPolicyTableShowRunningconfig (CliHandle);

    /* Access Table */
    AccessTableShowRunningconfig (CliHandle);

    /* Selector table */
    SelectorTableShowRunningconfig (CliHandle);

    nmhGetFsipv6SecGlobalStatus (&i4Status);
    if (i4Status == SEC_ENABLE)
    {
        CliPrintf (CliHandle, "ipsecv6 admin enable\r\n");
    }

    CliPrintf (CliHandle, "! \r\n");
    return CLI_SUCCESS;
}

/***************************************************************************
 *     FUNCTION NAME    : IPSecCheckAnyConfigDone
 *     DESCRIPTION      : Displays Global Config running configurations     
 *     INPUT            : CliHandle
 *     OUTPUT           : None                       
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            
 **************************************************************************/
INT4
IPSecCheckAnyConfigDone (tCliHandle CliHandle)
{
    INT4                i4RetVal = SNMP_FAILURE;
    INT4                i4Status = SNMP_FAILURE;
    INT4                i4SelProtoIndex = 0;
    INT4                i4AccessIndex = 0;
    INT4                i4SelPort = 0;
    INT4                i4dir = 0;

    UNUSED_PARAM (CliHandle);

    i4RetVal = nmhGetFirstIndexFsipv6SecPolicyTable (&i4Status);
    if (i4RetVal == SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    i4RetVal = nmhGetFirstIndexFsipv6SecAssocTable (&i4Status);
    if (i4RetVal == SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    i4RetVal = nmhGetFirstIndexFsipv6SecSelectorTable (&i4Status,
                                                       &i4SelProtoIndex,
                                                       &i4AccessIndex,
                                                       &i4SelPort, &i4dir);
    if (i4RetVal == SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    i4RetVal = nmhGetFirstIndexFsipv6SecAccessTable (&i4Status);
    if (i4RetVal == SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    nmhGetFsipv6SecGlobalStatus (&i4Status);
    if (i4Status == SEC_ENABLE)
    {
        return CLI_SUCCESS;
    }

    return CLI_FAILURE;
}

/***************************************************************************
 *     FUNCTION NAME    : SecPolicyTableShowRunningconfig
 *     DESCRIPTION      : Displays policy table running configurations     
 *     INPUT            : CliHandle
 *     OUTPUT           : None                       
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            
 **************************************************************************/
INT4
SecPolicyTableShowRunningconfig (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE OctStr;
    UINT1               au1Buf[SEC_MAX_SA_BUNDLE_LEN];
    INT4                i4Status = 0;
    INT4                i4FirstIndex = 0;
    INT4                i4NextIndex = 0;
    INT4                i4RetVal = SNMP_FAILURE;
    INT4                i4Value = SNMP_SUCCESS;
    INT4                i4Flag = 0;
    INT4                i4Mode = 0;

    MEMSET (au1Buf, 0, SEC_MAX_SA_BUNDLE_LEN);
    OctStr.pu1_OctetList = &au1Buf[0];
    OctStr.i4_Length = 0;

    i4RetVal = nmhGetFirstIndexFsipv6SecPolicyTable (&i4FirstIndex);
    if (i4RetVal != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    while (i4Value != SNMP_FAILURE)
    {
        i4Flag = 0;
        i4Mode = 0;
        i4Status = 0;
        CliPrintf (CliHandle, "ipsecv6 policy %d ", i4FirstIndex);
        nmhGetFsipv6SecPolicyFlag (i4FirstIndex, &i4Flag);
        if (i4Flag == SEC_APPLY)
        {
            CliPrintf (CliHandle, "apply ");
        }
        else if (i4Flag == SEC_BYPASS)
        {
            CliPrintf (CliHandle, "bypass ");
        }

        nmhGetFsipv6SecPolicyMode (i4FirstIndex, &i4Mode);
        if (i4Mode == SEC_MANUAL)
        {
            CliPrintf (CliHandle, "manual ");
        }
        else if (i4Mode == SEC_AUTOMATIC)
        {
            CliPrintf (CliHandle, "automatic ");
        }

        MEMSET (au1Buf, 0, SEC_MAX_SA_BUNDLE_LEN);
        nmhGetFsipv6SecPolicySaBundle (i4FirstIndex, &OctStr);
        CliPrintf (CliHandle, "%s \r\n", OctStr.pu1_OctetList);

        i4Value = nmhGetNextIndexFsipv6SecPolicyTable (i4FirstIndex,
                                                       &i4NextIndex);
        i4FirstIndex = i4NextIndex;
    }
    UNUSED_PARAM (i4Status);
    return CLI_SUCCESS;
}

/******************************************************************************
 *     FUNCTION NAME    : SecAssocTableShowRunningconfig
 *     DESCRIPTION      : Displays Security assoc table running configurations  
 *     INPUT            : CliHandle
 *     OUTPUT           : None                       
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            
 ******************************************************************************/
INT4
SecAssocTableShowRunningconfig (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE Ip6AddrStr;
    tSNMP_OCTET_STRING_TYPE KeyStr;
    UINT1               au1Buf2[SEC_ADDR_LEN];
    UINT1               au1Buf3[SEC_ESP_AES_KEY3_LEN];
    INT4                i4SecAssocAhAlgo = 0;
    INT4                i4SecAssocEspAlgo = 0;
    INT4                i4Status = 0;
    INT4                i4FirstIndex = 0;
    INT4                i4NextIndex = 0;
    INT4                i4RetVal = SNMP_FAILURE;
    INT4                i4Value = SNMP_SUCCESS;
    INT4                i4Flag = 0;
    INT4                i4Mode = 0;

    MEMSET (au1Buf2, 0, SEC_ADDR_LEN);
    Ip6AddrStr.pu1_OctetList = &au1Buf2[0];
    Ip6AddrStr.i4_Length = 0;

    MEMSET (au1Buf3, 0, SEC_ESP_AES_KEY3_LEN);
    KeyStr.pu1_OctetList = &au1Buf3[0];
    KeyStr.i4_Length = 0;
    i4RetVal = nmhGetFirstIndexFsipv6SecAssocTable (&i4FirstIndex);
    if (i4RetVal != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    while (i4Value != SNMP_FAILURE)
    {
        i4Flag = 0;
        i4Mode = 0;
        i4Status = 0;
        MEMSET (au1Buf2, 0, SEC_ADDR_LEN);
        nmhGetFsipv6SecAssocDstAddr (i4FirstIndex, &Ip6AddrStr);
        nmhGetFsipv6SecAssocSpi (i4FirstIndex, &i4Flag);
        nmhGetFsipv6SecAssocMode (i4FirstIndex, &i4Mode);
        nmhGetFsipv6SecAssocAntiReplay (i4FirstIndex, &i4Status);
        CliPrintf (CliHandle, "ipsecv6 sa %d ", i4FirstIndex);
        CliPrintf (CliHandle, " %s ",
                   Ip6PrintNtop ((tIp6Addr *) (VOID *) (Ip6AddrStr.
                                                        pu1_OctetList)));
        CliPrintf (CliHandle, " %d ", i4Flag);
        if (i4Mode == (INT4) CLI_ATOI (CLI_TUNNEL))
        {
            CliPrintf (CliHandle, " tunnel ");
        }
        else if (i4Mode == (INT4) CLI_ATOI (CLI_TRANSPORT))
        {
            CliPrintf (CliHandle, " transport ");
        }

        if (i4Status == (INT4) CLI_ATOI (CLI_ANTI_REPLAY_ENABLE))
        {
            CliPrintf (CliHandle, " antireplay-enable");
        }

        CliPrintf (CliHandle, "\r\n");

        i4Flag = 0;
        i4Mode = 0;
        i4Status = 0;
        i4SecAssocAhAlgo = 0;
        i4SecAssocEspAlgo = 0;

        nmhGetFsipv6SecAssocProtocol (i4FirstIndex, &i4Flag);
        nmhGetFsipv6SecAssocAhAlgo (i4FirstIndex, &i4SecAssocAhAlgo);
        nmhGetFsipv6SecAssocEspAlgo (i4FirstIndex, &i4SecAssocEspAlgo);

        if ((i4Flag == (INT4) CLI_ATOI (CLI_AH))
            || (i4Flag == (INT4) CLI_ATOI (CLI_ESP)))
        {

            CliPrintf (CliHandle, "ipsecv6 sa proto %d ", i4FirstIndex);
            if (i4Flag == (INT4) CLI_ATOI (CLI_AH))
            {
                CliPrintf (CliHandle, "ah ");
            }
            else if (i4Flag == (INT4) CLI_ATOI (CLI_ESP))
            {
                CliPrintf (CliHandle, "esp ");
            }
            if (i4SecAssocAhAlgo == (INT4) CLI_ATOI (CLI_MD5))
                CliPrintf (CliHandle, "auth md5 ");
            else if (i4SecAssocAhAlgo == (INT4) CLI_ATOI (CLI_KEYEDMD5))
                CliPrintf (CliHandle, "auth keyedmd5 ");
            else if (i4SecAssocAhAlgo == (INT4) CLI_ATOI (CLI_HMACMD5))
                CliPrintf (CliHandle, "auth hmacmd5 ");
            else if (i4SecAssocAhAlgo == (INT4) CLI_ATOI (CLI_HMACSHA))
                CliPrintf (CliHandle, "auth hmacsha1 ");
            else if (i4SecAssocAhAlgo == (INT4) CLI_ATOI (CLI_XCBCMAC))
                CliPrintf (CliHandle, "auth xcbcmac ");
            else if (i4SecAssocAhAlgo == (INT4) CLI_ATOI (CLI_HMACSHA256))
                CliPrintf (CliHandle, "auth hmacsha256 ");
            else if (i4SecAssocAhAlgo == (INT4) CLI_ATOI (CLI_HMACSHA384))
                CliPrintf (CliHandle, "auth hmacsha384 ");
            else if (i4SecAssocAhAlgo == (INT4) CLI_ATOI (CLI_HMACSHA512))
                CliPrintf (CliHandle, "auth hmacsha512 ");
            else
                CliPrintf (CliHandle, "auth null ");

            MEMSET (au1Buf3, 0, SEC_ESP_AES_KEY3_LEN);
            nmhGetFsipv6SecAssocAhKey (i4FirstIndex, &KeyStr);
            CliPrintf (CliHandle, "%s ", KeyStr.pu1_OctetList);

            if (i4Flag == (INT4) CLI_ATOI (CLI_ESP))
            {
                if (i4SecAssocEspAlgo == (INT4) CLI_ATOI (CLI_DESCBC))
                    CliPrintf (CliHandle, "encr des-cbc ");
                else if (i4SecAssocEspAlgo == (INT4) CLI_ATOI (CLI_3DESCBC))
                    CliPrintf (CliHandle, "encr 3des-cbc ");
                else if (i4SecAssocEspAlgo == (INT4) CLI_ATOI (CLI_AES))
                    CliPrintf (CliHandle, "encr aes ");
                else
                    CliPrintf (CliHandle, "encr null ");

                MEMSET (au1Buf3, 0, SEC_ESP_AES_KEY3_LEN);
                nmhGetFsipv6SecAssocEspKey (i4FirstIndex, &KeyStr);
                CliPrintf (CliHandle, "%s ", KeyStr.pu1_OctetList);

                MEMSET (au1Buf3, 0, SEC_ESP_AES_KEY3_LEN);
                nmhGetFsipv6SecAssocEspKey2 (i4FirstIndex, &KeyStr);
                CliPrintf (CliHandle, "%s ", KeyStr.pu1_OctetList);

                MEMSET (au1Buf3, 0, SEC_ESP_AES_KEY3_LEN);
                nmhGetFsipv6SecAssocEspKey3 (i4FirstIndex, &KeyStr);
                CliPrintf (CliHandle, "%s ", KeyStr.pu1_OctetList);
            }
            CliPrintf (CliHandle, "\r\n");
        }

        i4Value = nmhGetNextIndexFsipv6SecAssocTable (i4FirstIndex,
                                                      &i4NextIndex);
        i4FirstIndex = i4NextIndex;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 *     FUNCTION NAME    : SelectorTableShowRunningconfig
 *     DESCRIPTION      : Displays selctor table running configuration     
 *     INPUT            : CliHandle
 *     OUTPUT           : None                       
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            
 **************************************************************************/
INT4
SelectorTableShowRunningconfig (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE Ip6AddrStr;
    UINT1               au1Buf2[SEC_ADDR_LEN];
    INT4                i4RetVal = SNMP_FAILURE;
    INT4                i4Value = SNMP_SUCCESS;
    INT4                i4Flag = 0;
    INT4                i4SelIfIndex, i4NextSelIfIndex;
    INT4                i4SelProtoIndex, i4NextSelProtoIndex;
    INT4                i4AccessIndex, i4NextAccessIndex;
    INT4                i4SelPort, i4NextSelPort;
    INT4                i4dir, i4NextDir;
    INT4                i4IfaceType;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT2               u2VlanId;

    i4RetVal = nmhGetFirstIndexFsipv6SecSelectorTable (&i4SelIfIndex,
                                                       &i4SelProtoIndex,
                                                       &i4AccessIndex,
                                                       &i4SelPort, &i4dir);
    if (i4RetVal != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    MEMSET (au1Buf2, 0, SEC_ADDR_LEN);
    Ip6AddrStr.pu1_OctetList = &au1Buf2[0];
    Ip6AddrStr.i4_Length = 0;

    while (i4Value != SNMP_FAILURE)
    {
        MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
        CfaGetIfAlias ((UINT4) i4SelIfIndex, au1IfName);
        nmhGetIfType (i4SelIfIndex, &i4IfaceType);
        i4RetVal = (INT1) CfaGetVlanId ((UINT4) i4SelIfIndex, &u2VlanId);
        if (i4RetVal == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (i4IfaceType == CFA_L3IPVLAN)
            CliPrintf (CliHandle, "ipsecv6 selector vlan %d ", u2VlanId);
        else if (i4IfaceType == CFA_TUNNEL)
            CliPrintf (CliHandle, "ipsecv6 selector %s", au1IfName);
        else
            CliPrintf (CliHandle, "ipsecv6 selector any ");

        if (i4SelProtoIndex == (INT4) CLI_ATOL (CLI_TCP))
            CliPrintf (CliHandle, "tcp ");
        else if (i4SelProtoIndex == (INT4) CLI_ATOL (CLI_UDP))
            CliPrintf (CliHandle, "udp ");
        else if (i4SelProtoIndex == (INT4) CLI_ATOL (CLI_ICMPV6))
            CliPrintf (CliHandle, "icmpv6 ");
        else if (i4SelProtoIndex == (INT4) CLI_ATOL (CLI_AH))
            CliPrintf (CliHandle, "ah ");
        else if (i4SelProtoIndex == (INT4) CLI_ATOL (CLI_ESP))
            CliPrintf (CliHandle, "esp ");
        else
            CliPrintf (CliHandle, "any ");

        if (i4SelPort == (INT4) CLI_ATOL (CLI_ANY_PORT))
            CliPrintf (CliHandle, "any ");
        else
            CliPrintf (CliHandle, "%d ", i4SelPort);

        if (i4dir == (INT4) CLI_ATOI (CLI_INBOUND))
            CliPrintf (CliHandle, "inbound ");
        else if (i4dir == (INT4) CLI_ATOI (CLI_OUTBOUND))
            CliPrintf (CliHandle, "outbound ");
        else
            CliPrintf (CliHandle, "any ");

        CliPrintf (CliHandle, " %d ", i4AccessIndex);

        nmhGetFsipv6SelPolicyIndex (i4SelIfIndex, i4SelProtoIndex,
                                    i4AccessIndex, i4SelPort, i4dir, &i4Flag);
        CliPrintf (CliHandle, " %d ", i4Flag);

        i4Flag = 0;
        nmhGetFsipv6SelFilterFlag (i4SelIfIndex, i4SelProtoIndex,
                                   i4AccessIndex, i4SelPort, i4dir, &i4Flag);

        if (i4Flag == (UINT1) CLI_ATOI (CLI_FILTER))
        {
            CliPrintf (CliHandle, "filter ");
        }
        else
        {
            CliPrintf (CliHandle, "allow ");
        }

        MEMSET (au1Buf2, 0, SEC_ADDR_LEN);
        Ip6AddrStr.i4_Length = 0;
        nmhGetFsipv6SelIfIpAddress (i4SelIfIndex, i4SelProtoIndex,
                                    i4AccessIndex, i4SelPort,
                                    i4dir, &Ip6AddrStr);

        if (Ip6AddrStr.pu1_OctetList != NULL)
        {
            CliPrintf (CliHandle, "%s ",
                       Ip6PrintNtop ((tIp6Addr *) (VOID *) Ip6AddrStr.
                                     pu1_OctetList));
        }

        i4Value = nmhGetNextIndexFsipv6SecSelectorTable
            (i4SelIfIndex, &i4NextSelIfIndex,
             i4SelProtoIndex, &i4NextSelProtoIndex,
             i4AccessIndex, &i4NextAccessIndex,
             i4SelPort, &i4NextSelPort, i4dir, &i4NextDir);

        i4SelIfIndex = i4NextSelIfIndex;
        i4SelProtoIndex = i4NextSelProtoIndex;
        i4AccessIndex = i4NextAccessIndex;
        i4SelPort = i4NextSelPort;
        i4dir = i4NextDir;
        CliPrintf (CliHandle, "\r\n");
    }
    return CLI_SUCCESS;
}

/***************************************************************************
 *     FUNCTION NAME    : AccessTableShowRunningconfig
 *     DESCRIPTION      : Displays access table running configuration     
 *     INPUT            : CliHandle
 *     OUTPUT           : None                       
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            
 **************************************************************************/
INT4
AccessTableShowRunningconfig (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE Ip6AddrStr;
    tIp6Addr            Addr1;
    UINT1               au1Buf2[SEC_ADDR_LEN];
    INT4                i4FirstIndex = 0;
    INT4                i4NextIndex = 0;
    INT4                i4RetVal = SNMP_FAILURE;
    INT4                i4Value = SNMP_SUCCESS;
    INT4                i4SrcMask = 0;
    INT4                i4DestMask = 0;

    MEMSET (au1Buf2, 0, SEC_ADDR_LEN);
    Ip6AddrStr.pu1_OctetList = &au1Buf2[0];
    Ip6AddrStr.i4_Length = 0;
    i4RetVal = nmhGetFirstIndexFsipv6SecAccessTable (&i4FirstIndex);
    if (i4RetVal != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    while (i4Value != SNMP_FAILURE)
    {

        CliPrintf (CliHandle, "v6access-list %d ", i4FirstIndex);
        MEMSET (au1Buf2, 0, SEC_ADDR_LEN);
        MEMSET (&(Addr1), 0, sizeof (tIp6Addr));
        nmhGetFsipv6SecSrcNet (i4FirstIndex, &Ip6AddrStr);
        i4SrcMask = 0;
        nmhGetFsipv6SecSrcAddrPrefixLen (i4FirstIndex, &i4SrcMask);

        if (Ip6AddrStr.pu1_OctetList != NULL)
        {
            CliPrintf (CliHandle, "%d %0s ",
                       i4SrcMask,
                       Ip6PrintNtop ((tIp6Addr *) (VOID *) (Ip6AddrStr.
                                                            pu1_OctetList)));
        }
        else
            CliPrintf (CliHandle, "any ");

        MEMSET (au1Buf2, 0, SEC_ADDR_LEN);
        nmhGetFsipv6SecDestNet (i4FirstIndex, &Ip6AddrStr);
        nmhGetFsipv6SecDestAddrPrefixLen (i4FirstIndex, &i4DestMask);
        if (Ip6AddrStr.pu1_OctetList != NULL)
        {
            CliPrintf (CliHandle, "%d %0s ",
                       i4DestMask,
                       Ip6PrintNtop ((tIp6Addr *) (VOID *) (Ip6AddrStr.
                                                            pu1_OctetList)));
        }
        else
            CliPrintf (CliHandle, "any ");

        CliPrintf (CliHandle, "\r\n");

        i4Value = nmhGetNextIndexFsipv6SecAccessTable (i4FirstIndex,
                                                       &i4NextIndex);
        i4FirstIndex = i4NextIndex;
    }

    return CLI_SUCCESS;
}
#endif /* __IP6SECV6CLI_C__ */
