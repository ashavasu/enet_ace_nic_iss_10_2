/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secv6sz.c,v 1.6 2013/11/29 11:04:13 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _SECV6SZ_C
#include "secv6inc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
Secv6SizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < SECV6_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            (INT4) MemCreateMemPool (FsSECV6SizingParams[i4SizingId].
                                     u4StructSize,
                                     FsSECV6SizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(SECV6MemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            Secv6SizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
Secv6SzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsSECV6SizingParams);
    IssSzRegisterModulePoolId (pu1ModName, SECV6MemPoolIds);
    return OSIX_SUCCESS;
}

VOID
Secv6SizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < SECV6_MAX_SIZING_ID; i4SizingId++)
    {
        if (SECV6MemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (SECV6MemPoolIds[i4SizingId]);
            SECV6MemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
