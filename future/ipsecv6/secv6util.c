/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secv6util.c,v 1.12 2014/01/31 13:10:43 siva Exp $
 *
 * Description: This has util functions for IPSec 
 *
 ***********************************************************************/

#include "secv6com.h"

/*
 * ---------------------------------------------------------------
 *  Function Name : Secv6SeqNumberVerification 
 *  Description   : This function gets the incomming packet 
 *                  SeqNumber Compares it with SA SeqNumber
 *                  window. 
 *  Input(s)      : u4SeqNumber - Pkt SeqNumber. 
 *                  pSaEntry - SA Entry. 
 *  Output(s)     : None.
 *  Global(s)     : None.  
 *  Return Values : SEC_SUCCESS or SEC_FAILURE
 * ---------------------------------------------------------------
 */
INT1
Secv6SeqNumberVerification (UINT4 u4SeqNumber, tSecv6Assoc * pSaEntry)
{
    UINT4               u4Diff;
    if (u4SeqNumber == 0)
    {
        SECv6_TRC (SECv6_ALL_FAILURE, "SeqNumber is Zero \n");
        return SEC_FAILURE;
    }
    if (u4SeqNumber > pSaEntry->u4SecAssocSeqNumber)
    {
        u4Diff = u4SeqNumber - pSaEntry->u4SecAssocSeqNumber;
        if (u4Diff < pSaEntry->u1SecAssocSeqCounterFlag)
        {
            pSaEntry->u4SecAssocBitMask <<= u4Diff;
            pSaEntry->u4SecAssocBitMask |= 1;
        }
        else
        {
            pSaEntry->u4SecAssocBitMask = 1;
        }
        pSaEntry->u4SecAssocSeqNumber = u4SeqNumber;
        return SEC_SUCCESS;
    }
    u4Diff = pSaEntry->u4SecAssocSeqNumber - u4SeqNumber;
    if (u4Diff >= pSaEntry->u1SecAssocSeqCounterFlag)
    {
        SECv6_TRC (SECv6_ALL_FAILURE, "Packet is too old \n");
        return SEC_FAILURE;
    }
    if (pSaEntry->u4SecAssocBitMask & ((UINT4) 1 << u4Diff))
    {
        SECv6_TRC (SECv6_ALL_FAILURE, " Duplicate Packet \n");
        return SEC_FAILURE;
    }
    pSaEntry->u4SecAssocBitMask |= ((UINT4) 1 << u4Diff);
    return SEC_SUCCESS;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : Secv6RemoveExtraHeaderFromIP
 *  Description   : This function removes data from the given 
 *                  offset. 
 *  Input(s)      : pBuf -  buffer contains IP packet
 *                  u4Offset - Count from where deletion
 *                  should start.
 *                  u4Size - Number of Bytes to be deleted. 
 *  Output(s)     : None. 
 *  Global(s)     : None.  
 *  Return Values : SEC_SUCCESS or SEC_FAILURE
 * ---------------------------------------------------------------
 */
INT1
Secv6RemoveExtraHeaderFromIP (tBufChainHeader * pBuf,
                              UINT4 u4Offset, UINT4 u4Size)
{
    UINT1              *pLinear = NULL;
    INT4                i4RetVal = 1;

    pLinear = IPSECv6_MALLOC (u4Offset, UINT1);

    if (pLinear == NULL)
    {
        return (SEC_FAILURE);
    }

    if (u4Offset > 0)
    {
        i4RetVal = IPSEC_COPY_FROM_BUF (pBuf, pLinear, 0, u4Offset);
    }
    if (i4RetVal)
    {
        i4RetVal = IPSEC_BUF_MoveOffset (pBuf, u4Offset + u4Size);
        if (!i4RetVal)
        {
            i4RetVal = IPSEC_BUF_Prepend (pBuf, pLinear, u4Offset);
            if (!i4RetVal)
            {
                IPSECv6_MEMFREE (pLinear);
                return SEC_SUCCESS;
            }
            else
            {
                SECv6_TRC (SECv6_ALL_FAILURE,
                           "Secv6RemoveExtraHeaderFromIP : IPSec Prepend Buffer chain fails \n");
            }
        }
        else
        {
            SECv6_TRC (SECv6_ALL_FAILURE,
                       "Secv6RemoveExtraHeaderFromIP : IPSec Move Valid offset fails \n");

        }
    }
    else
    {
        SECv6_TRC (SECv6_ALL_FAILURE,
                   " Secv6RemoveExtraHeaderFromIP: Copy From Buf chain fails \n");
    }
    IPSECv6_MEMFREE (pLinear);
    return SEC_FAILURE;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : Secv6AddExtraHeaderToIP 
 *  Description   : This function adds data from the given 
 *                  offset. 
 *  Input(s)      : pBuf -  buffer contains IP packet
 *  Output(s)     : None. 
 *  Global(s)     : None.  
 *  Return Values : SEC_SUCCESS or SEC_FAILURE
 * ---------------------------------------------------------------
 */
INT1
Secv6AddExtraHeaderToIP (tBufChainHeader * pBuf,
                         UINT1 *pLinear1, UINT4 u4Len1,
                         UINT1 *pLinear2, UINT4 u4Len2)
{
    UINT1              *pLinear = NULL;
    UINT1              *pIpHdr;
    INT4                i4RetVal;

    if ((pIpHdr =
         IPSECv6_MALLOC (SEC_IPV6_HEADER_SIZE + u4Len1, UINT1)) == NULL)
    {
        return (SEC_FAILURE);
    }

    i4RetVal = IPSEC_COPY_FROM_BUF (pBuf, pIpHdr, 0,
                                    SEC_IPV6_HEADER_SIZE + u4Len1);

    pLinear = IPSECv6_MALLOC (SEC_AUTH_HEADER_SIZE + u4Len1 + u4Len2 +
                              SEC_IPV6_HEADER_SIZE, UINT1);
    if (pLinear == NULL)
    {
        IPSECv6_MEMFREE (pIpHdr);
        return (SEC_FAILURE);
    }

    IPSEC_MEMCPY (pLinear, pIpHdr, SEC_IPV6_HEADER_SIZE + u4Len1);
    IPSEC_MEMCPY (pLinear + SEC_IPV6_HEADER_SIZE + u4Len1, pLinear1,
                  SEC_AUTH_HEADER_SIZE);
    IPSEC_MEMCPY (pLinear + SEC_AUTH_HEADER_SIZE +
                  SEC_IPV6_HEADER_SIZE + u4Len1, pLinear2, u4Len2);

    if (i4RetVal)
    {
        i4RetVal = IPSEC_BUF_MoveOffset (pBuf, SEC_IPV6_HEADER_SIZE + u4Len1);
        if (!i4RetVal)
        {
            i4RetVal = IPSEC_BUF_Prepend (pBuf, pLinear,
                                          SEC_AUTH_HEADER_SIZE + u4Len2 +
                                          SEC_IPV6_HEADER_SIZE + u4Len1);
            if (!i4RetVal)
            {
                IPSECv6_MEMFREE (pLinear);
                IPSECv6_MEMFREE (pIpHdr);
                return SEC_SUCCESS;
            }
            else
            {
                SECv6_TRC (SECv6_ALL_FAILURE,
                           "Secv6AddExtraHeaderToIP : IPSec Prepend Buffer chain fails \n");
            }
        }
        else
        {
            SECv6_TRC (SECv6_ALL_FAILURE,
                       "Secv6AddExtraHeaderToIP : IPSec Move Valid offset fails \n");
        }
    }
    else
    {
        SECv6_TRC (SECv6_ALL_FAILURE,
                   "Secv6AddExtraHeaderToIP : Copy From Buf chain fails \n");
    }
    IPSECv6_MEMFREE (pLinear);
    IPSECv6_MEMFREE (pIpHdr);
    return SEC_FAILURE;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : Secv6ProcessOptions 
 *  Description   : This function is used to process the Options 
 *                : present in the Ipv6 Header   
 *  Input(s)      : pBuf -  buffer contains IP packet
 *                : pIp6Hdr - Refres to IP6 header
 *                : u1Proto - Refres to the immediate extension header
 *                : pu1NextHdr - Stores the Next extension header after
 *                : processing the options
 *                : pu1OptLen - Length of the Options 
 *  Output(s)     : None. 
 *  Global(s)     : None.  
 *  Return Values : SEC_SUCCESS or SEC_FAILURE
 * ---------------------------------------------------------------
 */

UINT1
Secv6ProcessOptions (tBufChainHeader * pBuf, tIp6Hdr * pIp6Hdr, UINT1 u1Proto,
                     UINT1 *pu1NextHdr, UINT1 *pu1OptLen)
{
    UINT4               u4OptOffset, u4Offset, u4Status = SEC_EXH_HDR_CONTINUE;
    UINT2               u2OptHdr;
    UINT1               u1OptType, u1OptLen, u1HdrLen, u1NextHdr;
    UINT1               u1TotOptLen;
    UINT1              *pu1Buf = NULL;
    UINT4               u4LastHdrOffset = 0;

    u1NextHdr = pIp6Hdr->u1Nh;
    *pu1NextHdr = u1NextHdr;

    /* If the next header is not any of the option headers copy the security *
     * protocol as the next header in the ipv6 header */
    if ((u1NextHdr != SEC_NH_H_BY_H) &&
        (u1NextHdr != SEC_NH_DEST_HDR) &&
        (u1NextHdr != SEC_NH_ROUTING_HDR) && (u1NextHdr != SEC_NH_FRAGMENT_HDR))
    {
        /* Copy the NextHeader field as AH or ESP in the last option header field */
        if (IPSEC_COPY_OVER_BUF (pBuf, &u1Proto,
                                 SEC_IPV6_HEADER_NXTHDR_OFFSET,
                                 SEC_IPV6_HEADER_NXTHDR) == BUF_FAILURE)
        {
            SECv6_TRC (SECv6_ALL_FAILURE,
                       "Secv6ProcessOptions : Copy to Buffer Fails\n");
            return (SEC_FAILURE);
        }
        *pu1OptLen = 0;
        return (SEC_SUCCESS);
    }

    u4Offset = SEC_IPV6_HEADER_SIZE;

    while (u4Status == SEC_EXH_HDR_CONTINUE)
    {

        /* Read the header Length for each option header */
        if (IPSEC_COPY_FROM_BUF
            (pBuf, (UINT1 *) &u1HdrLen, u4Offset + 1, sizeof (UINT1))
            == BUF_FAILURE)
        {
            SECv6_TRC (SECv6_ALL_FAILURE,
                       "Secv6ProcessOptions : Copy from Buffer Fails\n");
            return (SEC_FAILURE);
        }

        switch (u1NextHdr)
        {
            case SEC_NH_H_BY_H:
            case SEC_NH_DEST_HDR:
                /* u4Offset points to the extention header beginning 
                 * To get the option header we have to increment by 2
                 * to accomodate nxtHdr and extHdrLen fields */

                u4OptOffset = u4Offset + 2;
                u1TotOptLen = 0;

                while (u1HdrLen >= u1TotOptLen)
                {
                    if (IPSEC_COPY_FROM_BUF (pBuf, (UINT1 *) &u2OptHdr,
                                             u4OptOffset,
                                             sizeof (UINT2)) == BUF_FAILURE)
                    {
                        SECv6_TRC (SECv6_ALL_FAILURE,
                                   "Secv6ProcessOptions : Copy from Buffer Fails\n");
                        return (SEC_FAILURE);
                    }

                    /* For pad1 option there will not be any length and value *
                     * fields */
                    u1OptType = (UINT1) ((u2OptHdr & 0xff00) >> 8);

                    if (u1OptType == 0)
                    {
                        u4OptOffset++;
                        u1TotOptLen++;
                        continue;
                    }

                    u1OptLen = (UINT1) (u2OptHdr & 0x00ff);

                    /* if the third most higher bit is set to zero there will *
                     * not be any change of this option field in the transit. 
                     * If the third most bit is set the value would be 
                     * changed in transit We have to mute the changing options 
                     * on the transit */

                    if (((u1OptType & 0x20) == 0x20) && (u1OptLen != 0))
                    {
                        pu1Buf = IPSECv6_MALLOC (u1OptLen, UINT1);

                        if (pu1Buf == NULL)
                        {
                            return (SEC_FAILURE);
                        }

                        IPSEC_MEMSET (pu1Buf, 0, u1OptLen);

                        /* two bytes are for Option type and 
                           Option length */

                        /* copy the muted option value to buffer */

                        if (IPSEC_COPY_OVER_BUF (pBuf, pu1Buf, u4OptOffset + 2,
                                                 u1OptLen) == BUF_FAILURE)
                        {
                            SECv6_TRC (SECv6_ALL_FAILURE,
                                       "Secv6ProcessOptions : Copy to Buffer Fails\n");
                            IPSECv6_MEMFREE (pu1Buf);
                            return (SEC_FAILURE);
                        }

                        IPSECv6_MEMFREE (pu1Buf);
                    }

                    u4OptOffset = u4OptOffset + u1OptLen;
                    /* Point to the next offset, 2 to accomodate option type *
                     * and length fields */
                    u1TotOptLen = (UINT1) (u1TotOptLen + u1OptLen + 2);
                }
                u4LastHdrOffset = u4Offset;
                u4Offset =
                    u4Offset + (UINT4) (u1HdrLen * SEC_BYTES_IN_OCTET + 8);
                break;

            case SEC_NH_ROUTING_HDR:
                u4LastHdrOffset = u4Offset;
                u4Offset =
                    u4Offset + (UINT4) (u1HdrLen * SEC_BYTES_IN_OCTET + 8);
                break;

            case SEC_NH_FRAGMENT_HDR:
                u4LastHdrOffset = u4Offset;
                u4Offset = u4Offset + SEC_NH_FRAGMENT_HDR_SIZE;
                break;

            default:
                u4Status = SEC_HIGHER_LAYER;
                break;
        }

        if (IPSEC_COPY_FROM_BUF (pBuf, (UINT1 *) &u1NextHdr, u4LastHdrOffset,
                                 sizeof (UINT1)) == BUF_FAILURE)
        {
            SECv6_TRC (SECv6_ALL_FAILURE,
                       "Secv6ProcessOptions : Copy from Buffer Fails\n");
            return (SEC_FAILURE);
        }
    }

    *pu1NextHdr = u1NextHdr;
    *pu1OptLen = (UINT1) (u4Offset - SEC_IPV6_HEADER_SIZE);

    /* Copy the NextHeader field as AH or ESP in the last option header field */
    if (IPSEC_COPY_OVER_BUF (pBuf, (UINT1 *) &u1Proto,
                             u4LastHdrOffset, sizeof (UINT1)) == BUF_FAILURE)
    {
        SECv6_TRC (SECv6_ALL_FAILURE,
                   "Secv6ProcessOptions : Copy to buffer fails\n");
        return (SEC_FAILURE);
    }
    return (SEC_SUCCESS);
}

/*
 * ---------------------------------------------------------------
 *  Function Name : Secv6GetOptionsParams 
 *  Description   : This function is used to get the options 
 *                : present in the Ipv6 Header   
 *  Input(s)      : pBuf -  buffer contains IP packet
 *                : pIp6Hdr - Refres to IP6 header
 *                : u1Proto - Refres to the immediate extension header
 *                : pu1NextHdr - Stores the Next extension header after
 *                : processing the options
 *                : pu1OptLen - Length of the Options 
 *  Output(s)     : None. 
 *  Global(s)     : None.  
 *  Return Values : SEC_SUCCESS or SEC_FAILURE
 * ---------------------------------------------------------------
 */

UINT1
Secv6GetOptionsParams (tBufChainHeader * pBuf, tIp6Hdr * pIp6Hdr,
                       UINT1 u1Proto, UINT1 *pu1NextHdr, UINT1 *pu1OptLen)
{
    UINT4               u4OptOffset, u4Offset, u4Status = SEC_EXH_HDR_CONTINUE;
    UINT2               u2OptHdr;
    UINT1               u1OptType, u1OptLen, u1HdrLen, u1NextHdr;
    UINT1               u1TotOptLen;
    UINT4               u4LastHdrOffset = 0;

    u1NextHdr = pIp6Hdr->u1Nh;
    *pu1NextHdr = u1NextHdr;

    /* If the next header is not any of the option headers copy the security *
     * protocol as the next header in the ipv6 header */
    if ((u1NextHdr != SEC_NH_H_BY_H) &&
        (u1NextHdr != SEC_NH_DEST_HDR) &&
        (u1NextHdr != SEC_NH_ROUTING_HDR) && (u1NextHdr != SEC_NH_FRAGMENT_HDR))
    {
        /* Copy the NextHeader field as AH or ESP in the last option 
           header field */
        if (IPSEC_COPY_OVER_BUF (pBuf, &u1Proto, SEC_IPV6_HEADER_NXTHDR_OFFSET,
                                 SEC_IPV6_HEADER_NXTHDR) == BUF_FAILURE)
        {
            SECv6_TRC (SECv6_ALL_FAILURE,
                       "Secv6GetOptionsParams : Copy to buffer fails\n");
            return (SEC_FAILURE);
        }
        *pu1OptLen = 0;
        return (SEC_SUCCESS);
    }

    u4Offset = SEC_IPV6_HEADER_SIZE;

    while (u4Status == SEC_EXH_HDR_CONTINUE)
    {
        /* Read the header Length for each option header */
        if (IPSEC_COPY_FROM_BUF (pBuf, (UINT1 *) &u1HdrLen, u4Offset + 1,
                                 sizeof (UINT1)) == BUF_FAILURE)
        {
            SECv6_TRC (SECv6_ALL_FAILURE,
                       "Secv6GetOptionsParams : Copy from buffer fails\n");
            return (SEC_FAILURE);
        }

        switch (u1NextHdr)
        {
            case SEC_NH_H_BY_H:
            case SEC_NH_DEST_HDR:
                /* u4Offset points to the extention header beginning *
                 * To get the option header we have to increment by 2*
                 * to accomodate nxtHdr and extHdrLen fields */

                u4OptOffset = u4Offset + 2;
                u1TotOptLen = 0;

                while (u1HdrLen >= u1TotOptLen)
                {
                    if (IPSEC_COPY_FROM_BUF
                        (pBuf, (UINT1 *) &u2OptHdr, u4OptOffset,
                         sizeof (UINT2)) == BUF_FAILURE)
                    {
                        SECv6_TRC (SECv6_ALL_FAILURE,
                                   "Secv6GetOptionsParams : Copy from buffer fails\n");
                        return (SEC_FAILURE);
                    }

                    /* For pad1 option there will not be any length and value *
                     * fields */
                    u1OptType = (UINT1) ((u2OptHdr & 0xff00) >> 8);

                    /* Continue if option type is zero */
                    if (u1OptType == 0)
                    {
                        u4OptOffset++;
                        u1TotOptLen++;
                        continue;
                    }

                    u1OptLen = (UINT1) (u2OptHdr & 0x00ff);

                    u4OptOffset = u4OptOffset + u1OptLen;
                    /* Point to the next offset, 2 to accomodate option type *
                     * and length fields */
                    u1TotOptLen = (UINT1) (u1TotOptLen + u1OptLen + 2);
                }
                u4LastHdrOffset = u4Offset;
                u4Offset =
                    u4Offset + (UINT4) (u1HdrLen * SEC_BYTES_IN_OCTET + 8);
                break;

            case SEC_NH_ROUTING_HDR:
                u4LastHdrOffset = u4Offset;
                u4Offset =
                    u4Offset + (UINT4) (u1HdrLen * SEC_BYTES_IN_OCTET + 8);
                break;

            case SEC_NH_FRAGMENT_HDR:
                u4LastHdrOffset = u4Offset;
                u4Offset = u4Offset + SEC_NH_FRAGMENT_HDR_SIZE;
                break;

            default:
                u4Status = SEC_HIGHER_LAYER;
                break;
        }

        if (IPSEC_COPY_FROM_BUF
            (pBuf, (UINT1 *) &u1NextHdr, u4LastHdrOffset, sizeof (UINT1))
            == BUF_FAILURE)
        {

            SECv6_TRC (SECv6_ALL_FAILURE,
                       "Secv6GetOptionsParams : Copy from buffer fails\n");
            return (SEC_FAILURE);
        }
    }

    *pu1NextHdr = u1NextHdr;
    *pu1OptLen = (UINT1) (u4Offset - SEC_IPV6_HEADER_SIZE);

    /* Copy the NextHeader field as AH or ESP in the last option header field */
    if (IPSEC_COPY_OVER_BUF (pBuf, (UINT1 *) &u1Proto,
                             u4LastHdrOffset, sizeof (UINT1)) == BUF_FAILURE)
    {
        return (SEC_FAILURE);
    }

    return (SEC_SUCCESS);
}

/*
 * ---------------------------------------------------------------
 *  Function Name : Secv6GetHLProtocol 
 *  Description   : This function is used to get the options 
 *                : present in the Ipv6 Header   
 *  Input(s)      : pBuf -  buffer contains IP packet
 *                : pIp6Hdr - Refres to IP6 header
 *                : u1Proto - Refres to the immediate extension header
 *                : pu1NextHdr - Stores the Next extension header after
 *                : processing the options
 *  Output(s)     : None. 
 *  Global(s)     : None.  
 *  Return Values : SEC_SUCCESS or SEC_FAILURE
 * ---------------------------------------------------------------
 */

UINT1
Secv6GetHLProtocol (tBufChainHeader * pBuf, tIp6Hdr * pIp6Hdr,
                    UINT1 u1Proto, UINT1 *pu1NextHdr)
{
    UINT4               u4Offset, u4Status = SEC_EXH_HDR_CONTINUE;
    UINT1               u1HdrLen, u1NextHdr;
    UINT4               u4LastHdrOffset = 0;

    u1NextHdr = pIp6Hdr->u1Nh;
    *pu1NextHdr = u1NextHdr;

    /* If the next header is not any of the option headers copy the security *
     * protocol as the next header in the ipv6 header */
    if ((u1NextHdr != SEC_NH_H_BY_H) &&
        (u1NextHdr != SEC_NH_DEST_HDR) &&
        (u1NextHdr != SEC_NH_ROUTING_HDR) && (u1NextHdr != SEC_NH_FRAGMENT_HDR))
    {
        /* Copy the NextHeader field in the last option 
           header field */
        if (IPSEC_COPY_OVER_BUF (pBuf, &u1Proto, SEC_IPV6_HEADER_NXTHDR_OFFSET,
                                 SEC_IPV6_HEADER_NXTHDR) == BUF_FAILURE)
        {
            SECv6_TRC (SECv6_ALL_FAILURE,
                       "Secv6GetHLProtocol : Copy to buffer fails\n");
            return (SEC_FAILURE);
        }
        return (SEC_SUCCESS);
    }

    u4Offset = SEC_IPV6_HEADER_SIZE;

    while (u4Status == SEC_EXH_HDR_CONTINUE)
    {
        /* Read the header Length for each option header */
        if (IPSEC_COPY_FROM_BUF (pBuf, (UINT1 *) &u1HdrLen, u4Offset + 1,
                                 sizeof (UINT1)) == BUF_FAILURE)
        {
            SECv6_TRC (SECv6_ALL_FAILURE,
                       "Secv6GetHLProtocol : Copy from buffer fails\n");
            return (SEC_FAILURE);
        }

        switch (u1NextHdr)
        {
            case SEC_NH_H_BY_H:
            case SEC_NH_DEST_HDR:
                u4LastHdrOffset = u4Offset;
                u4Offset =
                    u4Offset + (UINT4) (u1HdrLen * SEC_BYTES_IN_OCTET + 8);
                break;

            case SEC_NH_ROUTING_HDR:
                u4LastHdrOffset = u4Offset;
                u4Offset =
                    u4Offset + (UINT4) (u1HdrLen * SEC_BYTES_IN_OCTET + 8);
                break;

            case SEC_NH_FRAGMENT_HDR:
                u4LastHdrOffset = u4Offset;
                u4Offset = u4Offset + SEC_NH_FRAGMENT_HDR_SIZE;
                break;

            default:
                u4Status = SEC_HIGHER_LAYER;
                break;
        }

        if (IPSEC_COPY_FROM_BUF
            (pBuf, (UINT1 *) &u1NextHdr, u4LastHdrOffset, sizeof (UINT1))
            == BUF_FAILURE)
        {

            SECv6_TRC (SECv6_ALL_FAILURE,
                       "Secv6GetHLProtocol : Copy from buffer fails\n");
            return (SEC_FAILURE);
        }
    }

    *pu1NextHdr = u1NextHdr;

    return (SEC_SUCCESS);
}

/*************************************************************************/
/*  Function Name : Secv6ConstructKey                                   */
/*  Description   : This function is used to construct hexa decimal key */
/*                : from display string                                 */
/*                :                                                     */
/*  Input(s)      : pu1InKey : Key in String Format                     */
/*                : u1Len    : Length of the DisplayString              */
/*                : pu1OutKey: Key in Hex Format                        */
/*                :                                                     */
/*  Output(s)     :                                                     */
/*  Global(s)     : None.                                               */
/*  Return Values : SEC_SUCCESS or SEC_FAILURE                          */
/***********************************************************************/
INT1
Secv6ConstructKey (UINT1 *pu1InKey, UINT1 u1Len, UINT1 *pu1OutKey)
{
    INT4                i4Count1 = 0, i4Num1 = 0, i4Num2 = 0, i4Count2 = 0;

    if ((u1Len != 40) && (u1Len != 32) && (u1Len != 16) &&
        (u1Len != 48) && (u1Len != 64) && (u1Len != 96) && (u1Len != 128))
    {
        return SEC_FAILURE;
    }

    while (i4Count1 < u1Len)
    {
        i4Num1 = ((*(pu1InKey + i4Count1)) - '0');
        i4Num2 = ((*(pu1InKey + i4Count1 + 1)) - '0');
        if ((i4Num1 > 54) || (i4Num2 > 54))
        {
            return SEC_FAILURE;
        }
        if (i4Num1 > 48)
        {
            i4Num1 = ((i4Num1 - 48) + 9);
        }
        if (i4Num2 > 48)
        {
            i4Num2 = ((i4Num2 - 48) + 9);
        }
        (*(pu1OutKey + i4Count2)) = (UINT1) ((i4Num1 * 16) + (i4Num2));
        i4Count1 = i4Count1 + 2;
        i4Count2++;
        i4Num1 = 0;
        i4Num2 = 0;
    }
    (*(pu1OutKey + i4Count2)) = '\0';

    return SEC_SUCCESS;
}

/*************************************************************************/
/*  Function Name : Secv6GetFreeSAIdx                                   */
/*  Description   : This function is used to get free sa index          */
/*                :                                                     */
/*  Input(s)      : None                                                */
/*                :                                                     */
/*  Output(s)     :                                                     */
/*  Global(s)     : None.                                               */
/*  Return Values : u4SaIndex                                           */
/***********************************************************************/

UINT4
Secv6GetFreeSAIdx (VOID)
{
    gu4Secv6FreeSaIndex++;
    return (gu4Secv6FreeSaIndex);
}

/*************************************************************************/
/*  Function Name : Secv6AttachNewSaToPolicy                             */
/*  Description   : This function attaches newly negotiated SA's toPolicy*/
/*                :                                                      */
/*  Input(s)      : pPolicy     :- Pointer to Policy                     */
/*                : pu1SaBundle :-Pointer to the Sa Bundle               */
/*  Output(s)     :                                                      */
/*  Global(s)     : None.                                                */
/*  Return Values : u4SaIndex                                            */
/*************************************************************************/
VOID
Secv6AttachNewSaToPolicy (tSecv6Policy * pu1Policy,
                          UINT1 au1SaBundle[SEC_MAX_SA_BUNDLE_LEN])
{

    INT4                i4BackCount = -1;
    INT4                i4Num = 0;
    INT4                i4Tens = 10;
    UINT1               u1SaBundleLength = 0;
    tSecv6Assoc        *pau1NewSaEntry[SEC_MAX_BUNDLE_SA];
    UINT1               u1NewSaCount = 0;
    UINT4               u1Count = 0;
    UINT4               u4Time = 0;

    IPSEC_MEMSET (pau1NewSaEntry, 0, SEC_MAX_BUNDLE_SA);
    u1SaBundleLength = (UINT1) STRLEN (au1SaBundle);

    /* Extract the secassoc indicies form the sa bundle in string format
       and store them as integers */
    do
    {
        i4BackCount++;
        i4Num = 0;
        while ((au1SaBundle[i4BackCount] != '.')
               && (au1SaBundle[i4BackCount] != '\0'))
        {
            i4Num = i4Num * i4Tens + (au1SaBundle[i4BackCount] - '0');
            i4BackCount++;
        }
        pau1NewSaEntry[u1NewSaCount] = Secv6AssocGetEntry ((UINT4) i4Num);

        pau1NewSaEntry[u1NewSaCount]->u4PolicyIndex = pu1Policy->u4PolicyIndex;
        u1NewSaCount++;
    }
    while (i4BackCount < u1SaBundleLength);

    for (u1Count = (SEC_MAX_SA_TO_POLICY - 1); u1Count != 0; u1Count--)
    {
        IPSEC_MEMCPY (&(pu1Policy->pau1NewSaEntry[u1Count][0]),
                      &(pu1Policy->pau1NewSaEntry[u1Count - 1][0]),
                      SEC_MAX_BUNDLE_SA);
        IPSEC_MEMCPY (&(pu1Policy->au1NewPolicySaBundle[u1Count][0]),
                      &(pu1Policy->au1NewPolicySaBundle[u1Count - 1][0]),
                      SEC_MAX_SA_BUNDLE_LEN);
        pu1Policy->au1NewSaCount[u1Count] =
            pu1Policy->au1NewSaCount[u1Count - 1];

    }

    if (pu1Policy->pau1NewSaEntry[0][0] != NULL)
    {
        if (pu1Policy->pau1NewSaEntry[0][0]->Secv6AssocSoftTimer.
            u1TimerStatus == SEC_TIMER_ACTIVE)
        {
            pu1Policy->pau1NewSaEntry[0][0]->Secv6AssocSoftTimer.u1TimerStatus =
                SEC_TIMER_INACTIVE;
            Secv6StopTimer (&
                            (pu1Policy->pau1NewSaEntry[0][0]->
                             Secv6AssocSoftTimer));
        }
    }

    IPSEC_MEMCPY (&(pu1Policy->pau1NewSaEntry[0][0]),
                  pu1Policy->paSaEntry, SEC_MAX_BUNDLE_SA);
    IPSEC_MEMCPY (&(pu1Policy->au1NewPolicySaBundle[0][0]),
                  pu1Policy->au1PolicySaBundle, SEC_MAX_SA_BUNDLE_LEN);
    pu1Policy->au1NewSaCount[0] = pu1Policy->u1SaCount;

    if (pu1Policy->pau1NewSaEntry[0][0]->Secv6AssocSoftTimer.u1TimerStatus ==
        SEC_TIMER_ACTIVE)
    {
        pu1Policy->pau1NewSaEntry[0][0]->Secv6AssocSoftTimer.u1TimerStatus =
            SEC_TIMER_INACTIVE;
        Secv6StopTimer (&
                        (pu1Policy->pau1NewSaEntry[0][0]->Secv6AssocSoftTimer));
    }
    IPSEC_MEMCPY (pu1Policy->paSaEntry, pau1NewSaEntry, SEC_MAX_BUNDLE_SA);
    IPSEC_MEMCPY (pu1Policy->au1PolicySaBundle, au1SaBundle,
                  SEC_MAX_SA_BUNDLE_LEN);
    pu1Policy->u1SaCount = u1NewSaCount;

    OsixGetSysTime (&u4Time);
    u4Time = (SEC_MIN_SOFTLIFETIME_FACTOR + (u4Time % 10));
    u4Time = ((pu1Policy->paSaEntry[0]->u4LifeTime * u4Time) / 100);
    pu1Policy->paSaEntry[0]->Secv6AssocSoftTimer.u4Param1 =
        (FS_ULONG) pu1Policy;
    pu1Policy->paSaEntry[0]->Secv6AssocSoftTimer.u4SaIndex =
        pu1Policy->paSaEntry[0]->u4SecAssocIndex;
    pu1Policy->paSaEntry[0]->Secv6AssocSoftTimer.u1TimerId =
        SECv6_SA_SOFT_TIMER_ID;
    pu1Policy->paSaEntry[0]->Secv6AssocSoftTimer.u1TimerStatus =
        SEC_TIMER_ACTIVE;
    Secv6StartTimer (&(pu1Policy->paSaEntry[0]->Secv6AssocSoftTimer), u4Time);

    pu1Policy->paSaEntry[0]->Secv6AssocHardTimer.u4Param1 =
        (FS_ULONG) pu1Policy;
    pu1Policy->paSaEntry[0]->Secv6AssocHardTimer.u4SaIndex =
        pu1Policy->paSaEntry[0]->u4SecAssocIndex;
    pu1Policy->paSaEntry[0]->Secv6AssocHardTimer.u1TimerId =
        SECv6_SA_HARD_TIMER_ID;
    pu1Policy->paSaEntry[0]->Secv6AssocHardTimer.u1TimerStatus =
        SEC_TIMER_ACTIVE;
    Secv6StartTimer (&(pu1Policy->paSaEntry[0]->Secv6AssocHardTimer),
                     pu1Policy->paSaEntry[0]->u4LifeTime);

}

/*************************************************************************/
/*  Function Name : Secv6CheckNoOfOutBoundBytesSecured                   */
/*  Description   : This function checks whether the no of bytes secured */
/*                : exceeded                                             */
/*                :                                                      */
/*  Input(s)      : pPolicy :- Pointer to Policy                         */
/*                : pBufDesc :-Pointer to the Buffer                     */
/*  Output(s)     :                                                      */
/*  Global(s)     : None.                                                */
/*  Return Values : u4SaIndex                                            */
/*************************************************************************/

INT1
Secv6CheckNoOfOutBoundBytesSecured (tSecv6Policy * pPolicy,
                                    tBufChainHeader * pBufDesc)
{

    UINT4               u4ByteCount = 0;
    UINT4               u4ThresHoldByteCount = 0;

    u4ByteCount = CRU_BUF_Get_ChainValidByteCount (pBufDesc);

    if (pPolicy->paSaEntry[0] == NULL)
    {
        return (SEC_REJECT);
    }

    if (pPolicy->paSaEntry[0]->u4LifeTimeInBytes != 0)
    {
        u4ThresHoldByteCount = ((pPolicy->paSaEntry[0]->u4LifeTimeInBytes *
                                 SEC_THRESHOLD_PERCENT) / 100);
        pPolicy->paSaEntry[0]->u4ByteCount =
            (pPolicy->paSaEntry[0]->u4ByteCount + u4ByteCount);
        if (pPolicy->paSaEntry[0]->u4ByteCount >
            pPolicy->paSaEntry[0]->u4LifeTimeInBytes)
        {
            if (pPolicy->paSaEntry[0]->Secv6AssocSoftTimer.u1TimerStatus ==
                SEC_TIMER_ACTIVE)
            {
                pPolicy->paSaEntry[0]->Secv6AssocSoftTimer.u1TimerStatus =
                    SEC_TIMER_INACTIVE;
                Secv6StopTimer (&(pPolicy->paSaEntry[0]->Secv6AssocSoftTimer));
            }
            if (pPolicy->paSaEntry[0]->Secv6AssocHardTimer.u1TimerStatus ==
                SEC_TIMER_ACTIVE)
            {
                pPolicy->paSaEntry[0]->Secv6AssocHardTimer.u1TimerStatus =
                    SEC_TIMER_INACTIVE;
                Secv6StopTimer (&(pPolicy->paSaEntry[0]->Secv6AssocHardTimer));
            }
            return (SEC_BYTES_SECURED_EXCEEDED);
        }

        else if (pPolicy->paSaEntry[0]->u4ByteCount >= u4ThresHoldByteCount)
        {
            if (pPolicy->paSaEntry[0]->Secv6AssocSoftTimer.u1TimerStatus ==
                SEC_TIMER_ACTIVE)
            {
                pPolicy->paSaEntry[0]->Secv6AssocSoftTimer.u1TimerStatus =
                    SEC_TIMER_INACTIVE;
                Secv6StopTimer (&(pPolicy->paSaEntry[0]->Secv6AssocSoftTimer));
            }

            return (SEC_THRESHOLD_REACHED);
        }
    }

    return (SEC_BYTES_SECURED_NOT_EXCEEDED);
}

/*************************************************************************/
/*  Function Name : Secv6CheckNoOfInBoundBytesSecured                    */
/*  Description   : This function checks whether the no of bytes secured */
/*                : exceeded                                             */
/*                :                                                      */
/*  Input(s)      : pPolicy :- Pointer to Policy                         */
/*                : pBufDesc :-Pointer to the Buffer                     */
/*  Output(s)     :                                                      */
/*  Global(s)     : None.                                                */
/*  Return Values : u4SaIndex                                            */
/*************************************************************************/
INT1
Secv6CheckNoOfInBoundBytesSecured (tSecv6Assoc * pSecv6Assoc,
                                   tBufChainHeader * pBufDesc)
{

    UINT4               u4ByteCount = 0;

    u4ByteCount = CRU_BUF_Get_ChainValidByteCount (pBufDesc);

    if (pSecv6Assoc->u4LifeTimeInBytes != 0)
    {
        pSecv6Assoc->u4ByteCount = pSecv6Assoc->u4ByteCount + u4ByteCount;
        if (pSecv6Assoc->u4ByteCount > pSecv6Assoc->u4LifeTimeInBytes)
        {
            if (pSecv6Assoc->Secv6AssocSoftTimer.u1TimerStatus ==
                SEC_TIMER_ACTIVE)
            {
                pSecv6Assoc->Secv6AssocSoftTimer.u1TimerStatus =
                    SEC_TIMER_INACTIVE;
                Secv6StopTimer (&(pSecv6Assoc->Secv6AssocSoftTimer));
            }
            if (pSecv6Assoc->Secv6AssocHardTimer.u1TimerStatus ==
                SEC_TIMER_ACTIVE)
            {
                pSecv6Assoc->Secv6AssocHardTimer.u1TimerStatus =
                    SEC_TIMER_INACTIVE;
                Secv6StopTimer (&(pSecv6Assoc->Secv6AssocHardTimer));
            }

            return (SEC_BYTES_SECURED_EXCEEDED);
        }
    }

    return (SEC_BYTES_SECURED_NOT_EXCEEDED);

}

/*************************************************************************/
/*  Function Name : Secv6DeleteAllSecAssocEntries                        */
/*  Description   : This function deletes all security association       */
/*                : structures maintained in Secv6Assoclist              */
/*                :                                                      */
/*  Input(s)      : None                                                    */
/*                                                                        */
/*  Output(s)     : None                                                 */
/*                                                                          */
/*  Return Values : SEC_SUCCESS or SEC_FAILURE                           */
/*************************************************************************/
INT1
Secv6DeleteAllSecAssocEntries (VOID)
{
    UINT4               u4NoSecAssoEntries = 0;
    UINT4               u4Index = 0;
    tSecv6Assoc        *pSecv6Assoc = NULL;
    INT1                i1RetVal = SNMP_FAILURE;

    SECv6_TAKE_SEM ();
    u4NoSecAssoEntries = TMO_SLL_Count (&Secv6AssocList);
    for (u4Index = 0; u4Index < u4NoSecAssoEntries; u4Index++)
    {
        pSecv6Assoc = (tSecv6Assoc *) TMO_SLL_First (&Secv6AssocList);
        if (pSecv6Assoc == NULL)
        {
            break;
        }
        i1RetVal = Secv6DestroySecAssoc ((INT4) pSecv6Assoc->u4SecAssocIndex);
        if (i1RetVal == SNMP_FAILURE)
        {
            SECv6_GIVE_SEM ();
            return SEC_FAILURE;
        }
    }
    SECv6_GIVE_SEM ();
    return SEC_SUCCESS;
}

/***************************************************************************
 * Function Name    :  Secv6UtilGetGlobalStatus
 * Description      :  This function returns the global IPSECv6 status
 *
 * Input (s)        :  None.
 *
 * Output (s)       :  None.
 * Returns          :  SEC_ENABLE/SEC_DISABLE.
 ***************************************************************************/
INT1
Secv6UtilGetGlobalStatus (VOID)
{
    return (INT1) gSecv6Status;
}

/*************************************************************************/
/*  Function Name : Secv6UtilGetMaskFromPrefix                           */
/*  Description   : This function converts prefix to mask as required by */
/*                  IKE message format.                                  */
/*                :                                                      */
/*  Input(s)      : u4PrefixLen - Prefix length.                         */
/*                                                                       */
/*  Output(s)     : pIpv6Mask - Ipv6 address.                            */
/*                                                                       */
/*  Return Values : None                                                 */
/*************************************************************************/
VOID
Secv6UtilGetMaskFromPrefix (UINT4 u4PrefixLen, tIp6Addr * pIp6Mask)
{
    GET_IPV6_MASK (u4PrefixLen, pIp6Mask->ip6_addr_u.u4WordAddr);

    pIp6Mask->ip6_addr_u.u4WordAddr[0] =
        OSIX_NTOHL (pIp6Mask->ip6_addr_u.u4WordAddr[0]);
    pIp6Mask->ip6_addr_u.u4WordAddr[1] =
        OSIX_NTOHL (pIp6Mask->ip6_addr_u.u4WordAddr[1]);
    pIp6Mask->ip6_addr_u.u4WordAddr[2] =
        OSIX_NTOHL (pIp6Mask->ip6_addr_u.u4WordAddr[2]);
    pIp6Mask->ip6_addr_u.u4WordAddr[3] =
        OSIX_NTOHL (pIp6Mask->ip6_addr_u.u4WordAddr[3]);

    return;
}

/*****************************************************************************/
/* Function Name      : Secv6GetBypassCapability                             */
/*                                                                           */
/* Description        : This function is used to get the bypass capability   */
/*                      of IPsecv6                                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu1Secv6BypassCrypto                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT1
Secv6GetBypassCapability (INT4 *pi4RetValFsVpnGlobalStatus)
{
    *pi4RetValFsVpnGlobalStatus = gu1Secv6BypassCrypto;
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : Secv6SetBypassCapability                             */
/*                                                                           */
/* Description        : This function is used to set the IPsecv6 bypass      */
/*                      capability                                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gu1Secv6BypassCrypto                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT1
Secv6SetBypassCapability (INT4 i4SetValFsVpnGlobalStatus)
{
    gu1Secv6BypassCrypto = (UINT1) i4SetValFsVpnGlobalStatus;
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   : Secv6ApiAllocateMemory                                   */
/*  Description     : This function will allocate memory pool.               */
/*  Input(s)        : u2Noctets:       The number of bytes of memory to be   */
/*  Output(s)       : None                                                   */
/*                  :                                                        */
/*  Returns         : NAT_SUCCESS if the search succeeds.       */
/*                    NAT_FAILURE if the search fails.          */
/*****************************************************************************/
UINT1              *
Secv6ApiAllocateMemory (VOID)
{
    UINT1              *pu1Block = NULL;

    if (MemAllocateMemBlock
        (SECV6_UINT1_POOL_ID, (UINT1 **) (VOID *) &(pu1Block)) == MEM_SUCCESS)
    {
        return pu1Block;

    }
    return NULL;
}

/*****************************************************************************/
/*  Function Name   : Secv6ApiAllocateMemory                                   */
/*  Description     : This function will Release memory pool.               */
/*  Input(s)        : noctets(IN):       The number of bytes of memory to be */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  NAT_SUCCESS if the search succeeds.       */
/*                                 NAT_FAILURE if the search fails.          */
/*****************************************************************************/
VOID
Secv6ApiReleaseMemory (VOID *pNoctets)
{
    MemReleaseMemBlock (SECV6_UINT1_POOL_ID, (UINT1 *) pNoctets);
}
