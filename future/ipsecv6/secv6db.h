/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: secv6db.h,v 1.3 2008/08/20 15:12:51 iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSSECVDB_H
#define _FSSECVDB_H

UINT1 Fsipv6SecSelectorTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 Fsipv6SecAccessTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Fsipv6SecPolicyTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Fsipv6SecAssocTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Fsipv6SecIfStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Fsipv6SecAhEspStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Fsipv6SecAhEspIntruTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};

UINT4 fssecv [] ={1,3,6,1,4,1,2076,29};
tSNMP_OID_TYPE fssecvOID = {8, fssecv};


UINT4 Fsipv6SecGlobalStatus [ ] ={1,3,6,1,4,1,2076,29,1,1};
UINT4 Fsipv6SecVersion [ ] ={1,3,6,1,4,1,2076,29,1,2};
UINT4 Fsipv6SecGlobalDebug [ ] ={1,3,6,1,4,1,2076,29,1,3};
UINT4 Fsipv6SecMaxSA [ ] ={1,3,6,1,4,1,2076,29,1,4};
UINT4 Fsipv6SelIfIndex [ ] ={1,3,6,1,4,1,2076,29,2,1,1,1};
UINT4 Fsipv6SelProtoIndex [ ] ={1,3,6,1,4,1,2076,29,2,1,1,2};
UINT4 Fsipv6SelAccessIndex [ ] ={1,3,6,1,4,1,2076,29,2,1,1,3};
UINT4 Fsipv6SelPort [ ] ={1,3,6,1,4,1,2076,29,2,1,1,4};
UINT4 Fsipv6SelPktDirection [ ] ={1,3,6,1,4,1,2076,29,2,1,1,5};
UINT4 Fsipv6SelFilterFlag [ ] ={1,3,6,1,4,1,2076,29,2,1,1,6};
UINT4 Fsipv6SelPolicyIndex [ ] ={1,3,6,1,4,1,2076,29,2,1,1,7};
UINT4 Fsipv6SelIfIpAddress [ ] ={1,3,6,1,4,1,2076,29,2,1,1,8};
UINT4 Fsipv6SelStatus [ ] ={1,3,6,1,4,1,2076,29,2,1,1,9};
UINT4 Fsipv6SecAccessIndex [ ] ={1,3,6,1,4,1,2076,29,2,2,1,1};
UINT4 Fsipv6SecAccessStatus [ ] ={1,3,6,1,4,1,2076,29,2,2,1,2};
UINT4 Fsipv6SecSrcNet [ ] ={1,3,6,1,4,1,2076,29,2,2,1,3};
UINT4 Fsipv6SecSrcAddrPrefixLen [ ] ={1,3,6,1,4,1,2076,29,2,2,1,4};
UINT4 Fsipv6SecDestNet [ ] ={1,3,6,1,4,1,2076,29,2,2,1,5};
UINT4 Fsipv6SecDestAddrPrefixLen [ ] ={1,3,6,1,4,1,2076,29,2,2,1,6};
UINT4 Fsipv6SecPolicyIndex [ ] ={1,3,6,1,4,1,2076,29,2,3,1,1};
UINT4 Fsipv6SecPolicyFlag [ ] ={1,3,6,1,4,1,2076,29,2,3,1,2};
UINT4 Fsipv6SecPolicyMode [ ] ={1,3,6,1,4,1,2076,29,2,3,1,3};
UINT4 Fsipv6SecPolicySaBundle [ ] ={1,3,6,1,4,1,2076,29,2,3,1,4};
UINT4 Fsipv6SecPolicyOptionsIndex [ ] ={1,3,6,1,4,1,2076,29,2,3,1,5};
UINT4 Fsipv6SecPolicyStatus [ ] ={1,3,6,1,4,1,2076,29,2,3,1,6};
UINT4 Fsipv6SecAssocIndex [ ] ={1,3,6,1,4,1,2076,29,2,4,1,1};
UINT4 Fsipv6SecAssocDstAddr [ ] ={1,3,6,1,4,1,2076,29,2,4,1,2};
UINT4 Fsipv6SecAssocProtocol [ ] ={1,3,6,1,4,1,2076,29,2,4,1,3};
UINT4 Fsipv6SecAssocSpi [ ] ={1,3,6,1,4,1,2076,29,2,4,1,4};
UINT4 Fsipv6SecAssocMode [ ] ={1,3,6,1,4,1,2076,29,2,4,1,5};
UINT4 Fsipv6SecAssocAhAlgo [ ] ={1,3,6,1,4,1,2076,29,2,4,1,6};
UINT4 Fsipv6SecAssocAhKey [ ] ={1,3,6,1,4,1,2076,29,2,4,1,7};
UINT4 Fsipv6SecAssocEspAlgo [ ] ={1,3,6,1,4,1,2076,29,2,4,1,8};
UINT4 Fsipv6SecAssocEspKey [ ] ={1,3,6,1,4,1,2076,29,2,4,1,9};
UINT4 Fsipv6SecAssocEspKey2 [ ] ={1,3,6,1,4,1,2076,29,2,4,1,10};
UINT4 Fsipv6SecAssocEspKey3 [ ] ={1,3,6,1,4,1,2076,29,2,4,1,11};
UINT4 Fsipv6SecAssocLifetimeInBytes [ ] ={1,3,6,1,4,1,2076,29,2,4,1,12};
UINT4 Fsipv6SecAssocLifetime [ ] ={1,3,6,1,4,1,2076,29,2,4,1,13};
UINT4 Fsipv6SecAssocAntiReplay [ ] ={1,3,6,1,4,1,2076,29,2,4,1,14};
UINT4 Fsipv6SecAssocStatus [ ] ={1,3,6,1,4,1,2076,29,2,4,1,15};
UINT4 Fsipv6SecIfIndex [ ] ={1,3,6,1,4,1,2076,29,3,1,1,1};
UINT4 Fsipv6SecIfInPkts [ ] ={1,3,6,1,4,1,2076,29,3,1,1,2};
UINT4 Fsipv6SecIfOutPkts [ ] ={1,3,6,1,4,1,2076,29,3,1,1,3};
UINT4 Fsipv6SecIfPktsApply [ ] ={1,3,6,1,4,1,2076,29,3,1,1,4};
UINT4 Fsipv6SecIfPktsDiscard [ ] ={1,3,6,1,4,1,2076,29,3,1,1,5};
UINT4 Fsipv6SecIfPktsBypass [ ] ={1,3,6,1,4,1,2076,29,3,1,1,6};
UINT4 Fsipv6SecAhEspIfIndex [ ] ={1,3,6,1,4,1,2076,29,3,2,1,1};
UINT4 Fsipv6SecInAhPkts [ ] ={1,3,6,1,4,1,2076,29,3,2,1,2};
UINT4 Fsipv6SecOutAhPkts [ ] ={1,3,6,1,4,1,2076,29,3,2,1,3};
UINT4 Fsipv6SecAhPktsAllow [ ] ={1,3,6,1,4,1,2076,29,3,2,1,4};
UINT4 Fsipv6SecAhPktsDiscard [ ] ={1,3,6,1,4,1,2076,29,3,2,1,5};
UINT4 Fsipv6SecInEspPkts [ ] ={1,3,6,1,4,1,2076,29,3,2,1,6};
UINT4 Fsipv6SecOutEspPkts [ ] ={1,3,6,1,4,1,2076,29,3,2,1,7};
UINT4 Fsipv6SecEspPktsAllow [ ] ={1,3,6,1,4,1,2076,29,3,2,1,8};
UINT4 Fsipv6SecEspPktsDiscard [ ] ={1,3,6,1,4,1,2076,29,3,2,1,9};
UINT4 Fsipv6SecAhEspIntruIndex [ ] ={1,3,6,1,4,1,2076,29,3,3,1,1};
UINT4 Fsipv6SecAhEspIntruIfIndex [ ] ={1,3,6,1,4,1,2076,29,3,3,1,2};
UINT4 Fsipv6SecAhEspIntruSrcAddr [ ] ={1,3,6,1,4,1,2076,29,3,3,1,3};
UINT4 Fsipv6SecAhEspIntruDestAddr [ ] ={1,3,6,1,4,1,2076,29,3,3,1,4};
UINT4 Fsipv6SecAhEspIntruProto [ ] ={1,3,6,1,4,1,2076,29,3,3,1,5};
UINT4 Fsipv6SecAhEspIntruTime [ ] ={1,3,6,1,4,1,2076,29,3,3,1,6};


tMbDbEntry fssecvMibEntry[]= {

{{10,Fsipv6SecGlobalStatus}, NULL, Fsipv6SecGlobalStatusGet, Fsipv6SecGlobalStatusSet, Fsipv6SecGlobalStatusTest, Fsipv6SecGlobalStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,Fsipv6SecVersion}, NULL, Fsipv6SecVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsipv6SecGlobalDebug}, NULL, Fsipv6SecGlobalDebugGet, Fsipv6SecGlobalDebugSet, Fsipv6SecGlobalDebugTest, Fsipv6SecGlobalDebugDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,Fsipv6SecMaxSA}, NULL, Fsipv6SecMaxSAGet, Fsipv6SecMaxSASet, Fsipv6SecMaxSATest, Fsipv6SecMaxSADep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,Fsipv6SelIfIndex}, GetNextIndexFsipv6SecSelectorTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsipv6SecSelectorTableINDEX, 5, 0, 0, NULL},

{{12,Fsipv6SelProtoIndex}, GetNextIndexFsipv6SecSelectorTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsipv6SecSelectorTableINDEX, 5, 0, 0, NULL},

{{12,Fsipv6SelAccessIndex}, GetNextIndexFsipv6SecSelectorTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsipv6SecSelectorTableINDEX, 5, 0, 0, NULL},

{{12,Fsipv6SelPort}, GetNextIndexFsipv6SecSelectorTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsipv6SecSelectorTableINDEX, 5, 0, 0, NULL},

{{12,Fsipv6SelPktDirection}, GetNextIndexFsipv6SecSelectorTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsipv6SecSelectorTableINDEX, 5, 0, 0, NULL},

{{12,Fsipv6SelFilterFlag}, GetNextIndexFsipv6SecSelectorTable, Fsipv6SelFilterFlagGet, Fsipv6SelFilterFlagSet, Fsipv6SelFilterFlagTest, Fsipv6SecSelectorTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6SecSelectorTableINDEX, 5, 0, 0, NULL},

{{12,Fsipv6SelPolicyIndex}, GetNextIndexFsipv6SecSelectorTable, Fsipv6SelPolicyIndexGet, Fsipv6SelPolicyIndexSet, Fsipv6SelPolicyIndexTest, Fsipv6SecSelectorTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsipv6SecSelectorTableINDEX, 5, 0, 0, NULL},

{{12,Fsipv6SelIfIpAddress}, GetNextIndexFsipv6SecSelectorTable, Fsipv6SelIfIpAddressGet, Fsipv6SelIfIpAddressSet, Fsipv6SelIfIpAddressTest, Fsipv6SecSelectorTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv6SecSelectorTableINDEX, 5, 0, 0, NULL},

{{12,Fsipv6SelStatus}, GetNextIndexFsipv6SecSelectorTable, Fsipv6SelStatusGet, Fsipv6SelStatusSet, Fsipv6SelStatusTest, Fsipv6SecSelectorTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6SecSelectorTableINDEX, 5, 0, 1, NULL},

{{12,Fsipv6SecAccessIndex}, GetNextIndexFsipv6SecAccessTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsipv6SecAccessTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecAccessStatus}, GetNextIndexFsipv6SecAccessTable, Fsipv6SecAccessStatusGet, Fsipv6SecAccessStatusSet, Fsipv6SecAccessStatusTest, Fsipv6SecAccessTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6SecAccessTableINDEX, 1, 0, 1, NULL},

{{12,Fsipv6SecSrcNet}, GetNextIndexFsipv6SecAccessTable, Fsipv6SecSrcNetGet, Fsipv6SecSrcNetSet, Fsipv6SecSrcNetTest, Fsipv6SecAccessTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv6SecAccessTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecSrcAddrPrefixLen}, GetNextIndexFsipv6SecAccessTable, Fsipv6SecSrcAddrPrefixLenGet, Fsipv6SecSrcAddrPrefixLenSet, Fsipv6SecSrcAddrPrefixLenTest, Fsipv6SecAccessTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsipv6SecAccessTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecDestNet}, GetNextIndexFsipv6SecAccessTable, Fsipv6SecDestNetGet, Fsipv6SecDestNetSet, Fsipv6SecDestNetTest, Fsipv6SecAccessTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv6SecAccessTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecDestAddrPrefixLen}, GetNextIndexFsipv6SecAccessTable, Fsipv6SecDestAddrPrefixLenGet, Fsipv6SecDestAddrPrefixLenSet, Fsipv6SecDestAddrPrefixLenTest, Fsipv6SecAccessTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsipv6SecAccessTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecPolicyIndex}, GetNextIndexFsipv6SecPolicyTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsipv6SecPolicyTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecPolicyFlag}, GetNextIndexFsipv6SecPolicyTable, Fsipv6SecPolicyFlagGet, Fsipv6SecPolicyFlagSet, Fsipv6SecPolicyFlagTest, Fsipv6SecPolicyTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6SecPolicyTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecPolicyMode}, GetNextIndexFsipv6SecPolicyTable, Fsipv6SecPolicyModeGet, Fsipv6SecPolicyModeSet, Fsipv6SecPolicyModeTest, Fsipv6SecPolicyTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6SecPolicyTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecPolicySaBundle}, GetNextIndexFsipv6SecPolicyTable, Fsipv6SecPolicySaBundleGet, Fsipv6SecPolicySaBundleSet, Fsipv6SecPolicySaBundleTest, Fsipv6SecPolicyTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv6SecPolicyTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecPolicyOptionsIndex}, GetNextIndexFsipv6SecPolicyTable, Fsipv6SecPolicyOptionsIndexGet, Fsipv6SecPolicyOptionsIndexSet, Fsipv6SecPolicyOptionsIndexTest, Fsipv6SecPolicyTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsipv6SecPolicyTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecPolicyStatus}, GetNextIndexFsipv6SecPolicyTable, Fsipv6SecPolicyStatusGet, Fsipv6SecPolicyStatusSet, Fsipv6SecPolicyStatusTest, Fsipv6SecPolicyTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6SecPolicyTableINDEX, 1, 0, 1, NULL},

{{12,Fsipv6SecAssocIndex}, GetNextIndexFsipv6SecAssocTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsipv6SecAssocTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecAssocDstAddr}, GetNextIndexFsipv6SecAssocTable, Fsipv6SecAssocDstAddrGet, Fsipv6SecAssocDstAddrSet, Fsipv6SecAssocDstAddrTest, Fsipv6SecAssocTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv6SecAssocTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecAssocProtocol}, GetNextIndexFsipv6SecAssocTable, Fsipv6SecAssocProtocolGet, Fsipv6SecAssocProtocolSet, Fsipv6SecAssocProtocolTest, Fsipv6SecAssocTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6SecAssocTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecAssocSpi}, GetNextIndexFsipv6SecAssocTable, Fsipv6SecAssocSpiGet, Fsipv6SecAssocSpiSet, Fsipv6SecAssocSpiTest, Fsipv6SecAssocTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsipv6SecAssocTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecAssocMode}, GetNextIndexFsipv6SecAssocTable, Fsipv6SecAssocModeGet, Fsipv6SecAssocModeSet, Fsipv6SecAssocModeTest, Fsipv6SecAssocTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6SecAssocTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecAssocAhAlgo}, GetNextIndexFsipv6SecAssocTable, Fsipv6SecAssocAhAlgoGet, Fsipv6SecAssocAhAlgoSet, Fsipv6SecAssocAhAlgoTest, Fsipv6SecAssocTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6SecAssocTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecAssocAhKey}, GetNextIndexFsipv6SecAssocTable, Fsipv6SecAssocAhKeyGet, Fsipv6SecAssocAhKeySet, Fsipv6SecAssocAhKeyTest, Fsipv6SecAssocTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv6SecAssocTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecAssocEspAlgo}, GetNextIndexFsipv6SecAssocTable, Fsipv6SecAssocEspAlgoGet, Fsipv6SecAssocEspAlgoSet, Fsipv6SecAssocEspAlgoTest, Fsipv6SecAssocTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6SecAssocTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecAssocEspKey}, GetNextIndexFsipv6SecAssocTable, Fsipv6SecAssocEspKeyGet, Fsipv6SecAssocEspKeySet, Fsipv6SecAssocEspKeyTest, Fsipv6SecAssocTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv6SecAssocTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecAssocEspKey2}, GetNextIndexFsipv6SecAssocTable, Fsipv6SecAssocEspKey2Get, Fsipv6SecAssocEspKey2Set, Fsipv6SecAssocEspKey2Test, Fsipv6SecAssocTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv6SecAssocTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecAssocEspKey3}, GetNextIndexFsipv6SecAssocTable, Fsipv6SecAssocEspKey3Get, Fsipv6SecAssocEspKey3Set, Fsipv6SecAssocEspKey3Test, Fsipv6SecAssocTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv6SecAssocTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecAssocLifetimeInBytes}, GetNextIndexFsipv6SecAssocTable, Fsipv6SecAssocLifetimeInBytesGet, Fsipv6SecAssocLifetimeInBytesSet, Fsipv6SecAssocLifetimeInBytesTest, Fsipv6SecAssocTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6SecAssocTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecAssocLifetime}, GetNextIndexFsipv6SecAssocTable, Fsipv6SecAssocLifetimeGet, Fsipv6SecAssocLifetimeSet, Fsipv6SecAssocLifetimeTest, Fsipv6SecAssocTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsipv6SecAssocTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecAssocAntiReplay}, GetNextIndexFsipv6SecAssocTable, Fsipv6SecAssocAntiReplayGet, Fsipv6SecAssocAntiReplaySet, Fsipv6SecAssocAntiReplayTest, Fsipv6SecAssocTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6SecAssocTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecAssocStatus}, GetNextIndexFsipv6SecAssocTable, Fsipv6SecAssocStatusGet, Fsipv6SecAssocStatusSet, Fsipv6SecAssocStatusTest, Fsipv6SecAssocTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv6SecAssocTableINDEX, 1, 0, 1, NULL},

{{12,Fsipv6SecIfIndex}, GetNextIndexFsipv6SecIfStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsipv6SecIfStatsTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecIfInPkts}, GetNextIndexFsipv6SecIfStatsTable, Fsipv6SecIfInPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SecIfStatsTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecIfOutPkts}, GetNextIndexFsipv6SecIfStatsTable, Fsipv6SecIfOutPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SecIfStatsTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecIfPktsApply}, GetNextIndexFsipv6SecIfStatsTable, Fsipv6SecIfPktsApplyGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SecIfStatsTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecIfPktsDiscard}, GetNextIndexFsipv6SecIfStatsTable, Fsipv6SecIfPktsDiscardGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SecIfStatsTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecIfPktsBypass}, GetNextIndexFsipv6SecIfStatsTable, Fsipv6SecIfPktsBypassGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SecIfStatsTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecAhEspIfIndex}, GetNextIndexFsipv6SecAhEspStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsipv6SecAhEspStatsTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecInAhPkts}, GetNextIndexFsipv6SecAhEspStatsTable, Fsipv6SecInAhPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SecAhEspStatsTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecOutAhPkts}, GetNextIndexFsipv6SecAhEspStatsTable, Fsipv6SecOutAhPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SecAhEspStatsTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecAhPktsAllow}, GetNextIndexFsipv6SecAhEspStatsTable, Fsipv6SecAhPktsAllowGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SecAhEspStatsTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecAhPktsDiscard}, GetNextIndexFsipv6SecAhEspStatsTable, Fsipv6SecAhPktsDiscardGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SecAhEspStatsTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecInEspPkts}, GetNextIndexFsipv6SecAhEspStatsTable, Fsipv6SecInEspPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SecAhEspStatsTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecOutEspPkts}, GetNextIndexFsipv6SecAhEspStatsTable, Fsipv6SecOutEspPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SecAhEspStatsTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecEspPktsAllow}, GetNextIndexFsipv6SecAhEspStatsTable, Fsipv6SecEspPktsAllowGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SecAhEspStatsTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecEspPktsDiscard}, GetNextIndexFsipv6SecAhEspStatsTable, Fsipv6SecEspPktsDiscardGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SecAhEspStatsTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecAhEspIntruIndex}, GetNextIndexFsipv6SecAhEspIntruTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsipv6SecAhEspIntruTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecAhEspIntruIfIndex}, GetNextIndexFsipv6SecAhEspIntruTable, Fsipv6SecAhEspIntruIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsipv6SecAhEspIntruTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecAhEspIntruSrcAddr}, GetNextIndexFsipv6SecAhEspIntruTable, Fsipv6SecAhEspIntruSrcAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fsipv6SecAhEspIntruTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecAhEspIntruDestAddr}, GetNextIndexFsipv6SecAhEspIntruTable, Fsipv6SecAhEspIntruDestAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fsipv6SecAhEspIntruTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecAhEspIntruProto}, GetNextIndexFsipv6SecAhEspIntruTable, Fsipv6SecAhEspIntruProtoGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsipv6SecAhEspIntruTableINDEX, 1, 0, 0, NULL},

{{12,Fsipv6SecAhEspIntruTime}, GetNextIndexFsipv6SecAhEspIntruTable, Fsipv6SecAhEspIntruTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv6SecAhEspIntruTableINDEX, 1, 0, 0, NULL},
};
tMibData fssecvEntry = { 61, fssecvMibEntry };
#endif /* _FSSECVDB_H */

