/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secv6init.c,v 1.9 2013/10/29 11:42:37 siva Exp $
 *
 * Description: This has functions for Sec Init and Sec SNMP Register. 
 *
 ***********************************************************************/

#ifndef IPSECv4_WANTED
#define _SECINIT_C_
#endif

#define  _SECV6INIT_C_

#include "secv6inc.h"
#include "secv6wr.h"
#include "include.h"

/************************************************************************/
/*  Function Name   : Secv6Initialize                                   */
/*  Description     : This function Initializes Lists and Arrays used   */
/*                  : used for Configuration and Statistics purpose     */
/*                  :                                                   */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : SEC_SUCCESS or SEC_FAILURE                        */
/************************************************************************/
INT1
Secv6Initialize (VOID)
{
    UINT4               u4Count = 0;

    Secv6InitTimer ();
    if (Secv6SizingMemCreateMemPools () != OSIX_SUCCESS)
    {
        SECv6_TRC (SECv6_OS_RESOURCE,
                   "Secv6Initialize: Memory Initialization Failed.\n");
        lrInitComplete (OSIX_FAILURE);
        return (SEC_FAILURE);
    }
    SECv6_SEL_MEMPOOL = SECV6MemPoolIds[MAX_IPSEC6_MAX_SELECTOR_SIZING_ID];
    SECv6_ACC_MEMPOOL = SECV6MemPoolIds[MAX_IPSEC6_MAX_ACCESS_LIST_SIZING_ID];
    SECv6_POL_MEMPOOL = SECV6MemPoolIds[MAX_IPSEC6_MAX_POLICY_SIZING_ID];
    SECv6_SAD_MEMPOOL = SECV6MemPoolIds[MAX_IPSEC6_MAX_ASSOC_ENTRY_SIZING_ID];
    SECv6_SAD_AH_MEMPOOL = SECV6MemPoolIds[MAX_IPSEC6_MAX_AH_KEYS_SIZING_ID];
    SECv6_SAD_ESP_MEMPOOL = SECV6MemPoolIds[MAX_IPSEC6_MAX_ESP_KEYS_SIZING_ID];
    SECV6_UINT1_POOL_ID = SECV6MemPoolIds[MAX_IPSEC6_MAX_UINT_SIZING_ID];

    TMO_SLL_Init (&Secv6SelList);
    TMO_SLL_Init (&Secv6AccessList);
    TMO_SLL_Init (&Secv6PolicyList);
    TMO_SLL_Init (&Secv6AssocList);
    TMO_SLL_Init (&Secv6SelFormattedList);
    TMO_SLL_Init (&Secv6SelList);
    gSecv6Status = SEC_DISABLE;
    gSecv6Version = SEC_V6;
    u4Secv6AhEspIntruStatCount = 0;

    for (u4Count = 0; u4Count < SEC_MAX_STAT_COUNT; u4Count++)
    {
        gatIpsecv6Stat[u4Count].u4Index = 0;
        gatIpsecv6AhEspStat[u4Count].u4Index = 0;
        gatIpsecv6AhEspIntruStat[u4Count].u4Index = 0;
    }

    if (OsixCreateSem (SECv6_GLOBAL_SEM_NAME,
                       SECv6_GLOBAL_SEM_COUNT,
                       OSIX_DEFAULT_SEM_MODE, &gSec6SemId) != OSIX_SUCCESS)
    {
        SECv6_TRC (SECv6_OS_RESOURCE,
                   "Secv6Initialize: Semaphore Creation Failed\n");
        lrInitComplete (OSIX_FAILURE);
        return (SEC_FAILURE);
    }

#ifdef SNMP_2_WANTED
    RegisterFSSECV ();
#endif
    lrInitComplete (OSIX_SUCCESS);
    return (SEC_SUCCESS);
}

/****************************************************************************
* DESCRIPTION : Function for Locking IPSECv6 Task
* INPUTS      : None
* OUTPUTS     : None
* RETURNS     : SNMP_SUCCESS/SNMP_FAILURE
****************************************************************************/
INT4
IpSecv6Lock (VOID)
{
    if (SECv6_TAKE_SEM () != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
* DESCRIPTION : Function for UnLocking IPSECv6 Task
* INPUTS      : None
* OUTPUTS     : None
* RETURNS     : SNMP_SUCCESS/SNMP_FAILURE
****************************************************************************/
INT4
IpSecv6UnLock (VOID)
{
    if (SECv6_GIVE_SEM () != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
