
# ifndef fssecOCON_H
# define fssecOCON_H
/*
 *  The Constant Declarations for
 *  fsipv6SecScalars
 */

# define FSIPV6SECGLOBALSTATUS                             (1)
# define FSIPV6SECVERSION                                  (2)
# define FSIPV6SECGLOBALDEBUG                              (3)
# define FSIPV6SECMAXSA                                    (4)

/*
 *  The Constant Declarations for
 *  fsipv6SecSelectorTable
 */

# define FSIPV6SELIFINDEX                                  (1)
# define FSIPV6SELPROTOINDEX                               (2)
# define FSIPV6SELACCESSINDEX                              (3)
# define FSIPV6SELPORT                                     (4)
# define FSIPV6SELPKTDIRECTION                             (5)
# define FSIPV6SELFILTERFLAG                               (6)
# define FSIPV6SELPOLICYINDEX                              (7)
# define FSIPV6SELIFIPADDRESS                              (8)
# define FSIPV6SELSTATUS                                   (9)

/*
 *  The Constant Declarations for
 *  fsipv6SecAccessTable
 */

# define FSIPV6SECACCESSINDEX                              (1)
# define FSIPV6SECACCESSSTATUS                             (2)
# define FSIPV6SECSRCNET                                   (3)
# define FSIPV6SECSRCADDRPREFIXLEN                         (4)
# define FSIPV6SECDESTNET                                  (5)
# define FSIPV6SECDESTADDRPREFIXLEN                        (6)

/*
 *  The Constant Declarations for
 *  fsipv6SecPolicyTable
 */

# define FSIPV6SECPOLICYINDEX                              (1)
# define FSIPV6SECPOLICYFLAG                               (2)
# define FSIPV6SECPOLICYMODE                               (3)
# define FSIPV6SECPOLICYSABUNDLE                           (4)
# define FSIPV6SECPOLICYOPTIONSINDEX                       (5)
# define FSIPV6SECPOLICYSTATUS                             (6)

/*
 *  The Constant Declarations for
 *  fsipv6SecAssocTable
 */

# define FSIPV6SECASSOCINDEX                               (1)
# define FSIPV6SECASSOCDSTADDR                             (2)
# define FSIPV6SECASSOCPROTOCOL                            (3)
# define FSIPV6SECASSOCSPI                                 (4)
# define FSIPV6SECASSOCMODE                                (5)
# define FSIPV6SECASSOCAHALGO                              (6)
# define FSIPV6SECASSOCAHKEY                               (7)
# define FSIPV6SECASSOCESPALGO                             (8)
# define FSIPV6SECASSOCESPKEY                              (9)
# define FSIPV6SECASSOCESPKEY2                             (10)
# define FSIPV6SECASSOCESPKEY3                             (11)
# define FSIPV6SECASSOCLIFETIMEINBYTES                     (12)
# define FSIPV6SECASSOCLIFETIME                            (13)
# define FSIPV6SECASSOCANTIREPLAY                          (14)
# define FSIPV6SECASSOCSTATUS                              (15)

/*
 *  The Constant Declarations for
 *  fsipv6SecIfStatsTable
 */

# define FSIPV6SECIFINDEX                                  (1)
# define FSIPV6SECIFINPKTS                                 (2)
# define FSIPV6SECIFOUTPKTS                                (3)
# define FSIPV6SECIFPKTSAPPLY                              (4)
# define FSIPV6SECIFPKTSDISCARD                            (5)
# define FSIPV6SECIFPKTSBYPASS                             (6)

/*
 *  The Constant Declarations for
 *  fsipv6SecAhEspStatsTable
 */

# define FSIPV6SECAHESPIFINDEX                             (1)
# define FSIPV6SECINAHPKTS                                 (2)
# define FSIPV6SECOUTAHPKTS                                (3)
# define FSIPV6SECAHPKTSALLOW                              (4)
# define FSIPV6SECAHPKTSDISCARD                            (5)
# define FSIPV6SECINESPPKTS                                (6)
# define FSIPV6SECOUTESPPKTS                               (7)
# define FSIPV6SECESPPKTSALLOW                             (8)
# define FSIPV6SECESPPKTSDISCARD                           (9)

/*
 *  The Constant Declarations for
 *  fsipv6SecAhEspIntruTable
 */

# define FSIPV6SECAHESPINTRUINDEX                          (1)
# define FSIPV6SECAHESPINTRUIFINDEX                        (2)
# define FSIPV6SECAHESPINTRUSRCADDR                        (3)
# define FSIPV6SECAHESPINTRUDESTADDR                       (4)
# define FSIPV6SECAHESPINTRUPROTO                          (5)
# define FSIPV6SECAHESPINTRUTIME                           (6)

#endif /*  fssecv6OCON_H  */
