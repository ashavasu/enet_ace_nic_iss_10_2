/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secv6trace.h
 *
 * Description:This file contains the #defines of the trace needed by 
 *             all the modules of the SEC subsystem
 *
 *******************************************************************/
#ifndef __SEC_SECv6DEBUG_H__
#define __SEC_SECv6DEBUG_H__


#include "trace.h"

#define SECv6_ENABLE_ALL_TRC		0x0000ffff
#define SECv6_DISABLE_ALL_TRC		0

#define SECv6_INIT_SHUT	               INIT_SHUT_TRC
#define SECv6_MGMT		       MGMT_TRC
#define SECv6_DATA_PATH	               DATA_PATH_TRC
#define SECv6_CONTROL_PLANE	       CONTROL_PLANE_TRC
#define SECv6_DUMP		       DUMP_TRC
#define SECv6_OS_RESOURCE	       OS_RESOURCE_TRC	
#define SECv6_ALL_FAILURE 	       ALL_FAILURE_TRC	
#define SECv6_BUFFER		       BUFFER_TRC		
#define SECv6_MUST	               0x00001000		

#define SECv6_TRC_NAME	"\nSEC"
#define SECv6_TRC_LINE_SEPERATOR	"----------------------"



#define SECv6_PKT_DUMP(mask,pBuf,Length,fmt)\
	   MOD_PKT_DUMP(gu4Secv6Debug,mask,SECv6_TRC_NAME,pBuf,Length,fmt)

#define SECv6_TRC(mask,fmt)\
	MOD_TRC(gu4Secv6Debug,mask,SECv6_TRC_NAME,fmt)
#define SECv6_TRC1(mask,fmt,arg1)\
	MOD_TRC_ARG1(gu4Secv6Debug,mask,SECv6_TRC_NAME,fmt,arg1)
#define SECv6_TRC2(mask,fmt,arg1,arg2)\
	MOD_TRC_ARG2(gu4Secv6Debug,mask,SECv6_TRC_NAME,fmt,arg1,arg2)
#define SECv6_TRC3(mask,fmt,arg1,arg2,arg3)\
	MOD_TRC_ARG3(gu4Secv6Debug,mask,SECv6_TRC_NAME,fmt,arg1,arg2,arg3)
#define SECv6_TRC4(mask,fmt,arg1,arg2,arg3,arg4)\
	MOD_TRC_ARG4(gu4Secv6Debug,mask,SECv6_TRC_NAME,fmt,arg1,arg2,arg3,arg4)
#define SECv6_TRC5(mask,fmt,arg1,arg2,arg3,arg4,arg5)\
	MOD_TRC_ARG5(gu4Secv6Debug,mask,SECv6_TRC_NAME,fmt,arg1,arg2,arg3,arg4,arg5)
#define SECv6_TRC6(mask,fmt,arg1,arg2,arg3,arg4,arg5,arg6)\
	MOD_TRC_ARG6(gu4Secv6Debug,mask,SECv6_TRC_NAME,fmt,arg1,arg2,arg3,arg4,arg5,arg6)


#define SECv6_PRINT_MEM_ALLOC_FAILURE\
		SECv6_TRC(OS_RESOURCE_TRC,"Memory Allocation Failed")


#endif  /* __SEC_SECv6DEBUG_H__ */
