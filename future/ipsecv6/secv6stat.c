/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secv6stat.c
 *
 * Description:This file contains the Statistics handler for the
 *  IPSEC  Module.
 *
 *******************************************************************/
#include "secv6com.h"

/************************************************************************/
/*  Function Name   : Secv6UpdateIfStats                                */
/*  Description     : This function is used to update the statistics    */
/*                  : Counter for the Sec Packets                       */
/*                  :                                                   */
/*  Input(s)        : u4Index:Refers to the Interface on which the      */
/*                  : arrives                                           */
/*                  : u1Direction:The direction of the Packet arrival   */
/*                  : u1Flag:Refers to the Policy                       */
/*                  :                                                   */
/*                  :                                                   */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
Secv6UpdateIfStats (UINT4 u4Index, UINT1 u1Direction, UINT1 u1Flag)
{
    /*check whether the index is greater than max allowed entries */
    if (u4Index >= SEC_MAX_STAT_COUNT)
    {
        return;
    }

    gatIpsecv6Stat[u4Index].u4Index = u4Index;
    if (u1Direction == SEC_INBOUND)
    {
        /* increment the Inpkts counter */
        gatIpsecv6Stat[u4Index].u4IfInPkts += 1;
    }
    else
    {
        /* increment the outpkts counter */
        gatIpsecv6Stat[u4Index].u4IfOutPkts += 1;
    }
    switch (u1Flag)
    {
        case SEC_APPLY:
            /* increment the ifapply counter */
            gatIpsecv6Stat[u4Index].u4IfPktsApply += 1;
            break;
        case SEC_FILTER:
            /* increment the filter counter */
            gatIpsecv6Stat[u4Index].u4IfPktsDiscard += 1;
            break;
        case SEC_BYPASS:
            /* increment the bypass-counter */
            gatIpsecv6Stat[u4Index].u4IfPktsBypass += 1;
            break;
        default:
            break;
    }
}

/************************************************************************/
/*  Function Name   : Secv6UpdateAhEspStats                             */
/*  Description     : This function is used to update the statistics    */
/*                  : Counter for the Sec Packets                       */
/*                  :                                                   */
/*  Input(s)        : u4Index:Refers to the Interface on which the      */
/*                  : arrives                                           */
/*                  : u1Protocol:Refers to the protocol                 */
/*                  : u1Direction:The direction of the Packet arrival   */
/*                  : u1Flag:Refers to the Policy                       */
/*                  :                                                   */
/*                  :                                                   */
/*  Output(s)       : None                                                  */
/*  Returns         : None                                              */
/************************************************************************/
VOID
Secv6UpdateAhEspStats (UINT4 u4Index, UINT1 u1Protocol, UINT1 u1Direction,
                       UINT1 u1Flag)
{

    /* check whether the index is greater than max-allowed count */
    if (u4Index >= SEC_MAX_STAT_COUNT)
    {
        return;
    }

    gatIpsecv6AhEspStat[u4Index].u4Index = u4Index;

    if (u1Protocol == SEC_AH)
    {
        if (u1Direction == SEC_INBOUND)
        {
            /* increment the incoming ahpkts count */
            gatIpsecv6AhEspStat[u4Index].u4InAhPkts += 1;
        }
        else
        {
            /* increment the outgoining ahpkts count */
            gatIpsecv6AhEspStat[u4Index].u4OutAhPkts += 1;
        }
        switch (u1Flag)
        {
            case SEC_ALLOW:
                /* increment the ahpkts allow  count */
                gatIpsecv6AhEspStat[u4Index].u4AhPktsAllow += 1;
                break;
            case SEC_FILTER:
                /* increment the ahpktsdisacrd count */
                gatIpsecv6AhEspStat[u4Index].u4AhPktsDiscard += 1;
                break;
            default:
                break;
        }
    }
    if (u1Protocol == SEC_ESP)
    {
        if (u1Direction == SEC_INBOUND)
        {
            /* increment the incoming esp-pkts count */
            gatIpsecv6AhEspStat[u4Index].u4InEspPkts += 1;
        }
        else
        {
            /* increment the outgoining esp-pkts count */
            gatIpsecv6AhEspStat[u4Index].u4OutEspPkts += 1;
        }
        switch (u1Flag)
        {
            case SEC_ALLOW:
                /* increment the esp-pkts allow count */
                gatIpsecv6AhEspStat[u4Index].u4EspPktsAllow += 1;
                break;
            case SEC_FILTER:
                /* increment the esp-pkts discard count */
                gatIpsecv6AhEspStat[u4Index].u4EspPktsDiscard += 1;
                break;
            default:
                break;
        }
    }
}

/************************************************************************/
/*  Function Name   : Secv6UpdateAhEspIntruStats                        */
/*  Description     : This function is used to update the statistics    */
/*                  : Counter for the Sec Packets                       */
/*                  :                                                   */
/*  Input(s)        : u4Index:Refers to the Interface on which the      */
/*                  : arrives                                           */
/*                  : SrcAddr:Refers to the source address              */
/*                  : DestAddr:Refers to the destination address        */
/*                  : u1Prot:Refers to the protocol :AH/ESP             */
/*                  : u1Time:Refers to the time at which intrusion took */
/*                  : Place                                             */
/*                  :                                                   */
/*                  :                                                   */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
Secv6UpdateAhEspIntruStats (UINT4 u4Index, tIp6Addr SrcAddr, tIp6Addr DesAddr,
                            UINT1 u1Prot, UINT4 u4Time)
{

    /* check whether the count  has exceeded the max-val */
    if (u4Secv6AhEspIntruStatCount >= SEC_MAX_STAT_COUNT)
        u4Secv6AhEspIntruStatCount = 0;

    gatIpsecv6AhEspIntruStat[u4Secv6AhEspIntruStatCount].u4Index =
        u4Secv6AhEspIntruStatCount + 1;

    gatIpsecv6AhEspIntruStat[u4Secv6AhEspIntruStatCount].u4IfIndex = u4Index;
    /* copy the intru-src addr */
    Ip6AddrCopy (&gatIpsecv6AhEspIntruStat[u4Secv6AhEspIntruStatCount].
                 AhEspIntruSrcAddr, &SrcAddr);
    /* copy the intru-dest addr */
    Ip6AddrCopy (&gatIpsecv6AhEspIntruStat[u4Secv6AhEspIntruStatCount].
                 AhEspIntruDestAddr, &DesAddr);
    /* update the intruder-protocol */
    gatIpsecv6AhEspIntruStat[u4Secv6AhEspIntruStatCount].u4AhEspIntruProt =
        u1Prot;
    /* update the intruder-time */
    gatIpsecv6AhEspIntruStat[u4Secv6AhEspIntruStatCount].u4AhEspIntruTime =
        u4Time;
    /* increment the ahesp intru count */
    u4Secv6AhEspIntruStatCount++;

}

/******************************************************************************/
