/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: sec6kern.c,v 1.3 2011/11/08 13:51:13 siva Exp $
 *
 * Description : This file contains the IOCTL API defined for IPSecv6
 *               This file is only compiled for the kernel space.
 *
 *******************************************************************/

#include "secv6com.h"
#include "seckgen.h"

extern UINT4        gu4CopyModIdx;

/***********************************************************************/
/*  Function Name : Secv6Ioctl                                         */
/*  Description   : This function handles IOCTL calls IPSEcv6 module   */
/*                  is in the kernel.                                  */
/*                :                                                    */
/*  Input(s)      : P - Message to kernel                              */
/*                :                                                    */
/*  Output(s)     : None                                               */
/*  Return        : None                                               */
/***********************************************************************/
INT4
Secv6Ioctl (FS_ULONG P)
{
    tSecv6KernIface     Secv6KernIface;
    tOsixKernUserInfo   OsixKerUseInfo;
    tSecv6KernIface    *pSecv6KernIface = NULL;

    pSecv6KernIface = (tSecv6KernIface *) P;

    OsixKerUseInfo.pDest = &Secv6KernIface;
    OsixKerUseInfo.pSrc = pSecv6KernIface;

    if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                     sizeof (tSecv6KernIface), 0) == OSIX_FAILURE)
    {
        return -EFAULT;
    }

    switch (Secv6KernIface.u1MsgType)
    {
        case IPSECV6_IKE_REQ:
            Secv6ProcessIKEMsg (&(Secv6KernIface.uMsg.IkeIPSecMsg));
            break;

        case IPSECV6_ASSOC_OVERHEAD_REQ:
            Secv6KernIface.uMsg.SecAssocOverHead.u2OverHead =
                Secv6GetSecAssocOverHead
                ((Secv6KernIface.uMsg.SecAssocOverHead.SrcAddr),
                 (Secv6KernIface.uMsg.SecAssocOverHead.DestAddr),
                 (Secv6KernIface.uMsg.SecAssocOverHead.u2Index),
                 (Secv6KernIface.uMsg.SecAssocOverHead.u1Protocol));

            OsixKerUseInfo.pDest = pSecv6KernIface;
            OsixKerUseInfo.pSrc = &Secv6KernIface;

            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tSecv6KernIface)) == OSIX_FAILURE)
            {
                return -EFAULT;
            }
            break;

        case IPSECV6_DELETE_SA_ENTRIES:
            if (Secv6DeleteAllSecAssocEntries () != SEC_SUCCESS)
            {
                return -EFAULT;
            }
            break;

        default:
            return -EFAULT;
    }
    return IOCTL_SUCCESS;
}
