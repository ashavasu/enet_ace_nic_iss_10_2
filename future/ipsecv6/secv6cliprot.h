/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id:ipsecv6cli.h
*
* Description:This file contains the #define constants and prototypes
*             of ipsec cli functions.
*
*******************************************************************/

#define SEC_MAX_GLOBAL_ENTRIES    4
#define SECv6_MAX_CLI_COMMANDS    21
#define SEC_MAX_CHAR_PER_ENTRY    160
#define SEC_MAX_STAT_ENTRIES     (SEC_MAX_STAT_COUNT + 3)

/* Prototypes of the functions used in secv6cli.c */

VOID IPSecv6CreatePolicy (tCliHandle CliHandle, UINT4 u4PolicyIndex, UINT1 u1PolicyFlag, UINT1 u1PolicyMode, UINT1 *pu1PolicySaBundle);

VOID IPSecv6CreateSecAssoc (tCliHandle CliHandle, UINT4 u4SecAssocIndex, tIp6Addr *SecAssocDestAddr, UINT4 u4SecAssocSpi, UINT1 u1SecAssocMode, UINT1 u1AntiReplayStatus);
        
VOID IPSecv6SetSecAssocParams (tCliHandle CliHandle, UINT4 u4SecAssocIndex, UINT1 *pu1SecAssocAhKey, UINT1 *pu1SecAssocEspKey, UINT1 *pu1SecAssocEspKey2, UINT1 *pu1SecAssocEspKey3, UINT1 u1SecAssocProtocol, UINT1 u1SecAssocAhAlgo, UINT1 u1SecAssocEspAlgo, UINT1 u1SecAssocAhKeyLength, UINT1 u1SecAssocEspKeyLength, UINT1 u1SecAssocEspKey2Length, UINT1 u1SecAssocEspKey3Length);

VOID IPSecv6CreateAccess (tCliHandle CliHandle, UINT4 u4AccessIndex, tIp6Addr *SrcAddress, UINT4 u4SrcMask, tIp6Addr *DestAddress, UINT4 u4DestMask);

VOID IPSecv6CreateSelector (tCliHandle CliHandle, UINT4 u4Index, UINT4 u4ProtoId, UINT4 u4Port, UINT4 u4PktDirection, UINT4 u4SelAccessIndex, UINT4 u4SelPolicyIndex, UINT1 u1SelFilterFlag, tIp6Addr *TunnelTermAddr, UINT1 bAddrSet);

VOID IPSecv6SetAdminStatus (tCliHandle CliHandle,UINT1 u1CliIPSecv6AdminStatus);

VOID IPSecv6SetTraceLevel (tCliHandle CliHandle, UINT4 u4CliIPSecv6Debug);

VOID IPSecv6ShowIfStat (tCliHandle CliHandle);

VOID IPSecv6ShowAhEspStat (tCliHandle CliHandle);

VOID IPSecv6ShowIntruderStat (tCliHandle CliHandle);

VOID IPSecv6ShowPolicyEntries (tCliHandle CliHandle, UINT4 u4Index);

VOID IPSecv6ShowSelectorEntries (tCliHandle CliHandle, UINT4 u4Index);

VOID IPSecv6ShowSecAssocEntries (tCliHandle CliHandle, UINT4 u4Index);

VOID IPSecv6ShowAccessEntries (tCliHandle CliHandle, UINT4 u4Index);

VOID IPSecv6ShowGlobalEntries (tCliHandle CliHandle);

VOID IPSecv6DeletePolicyEntries (tCliHandle CliHandle, UINT4 u4Index);

VOID IPSecv6DeleteSelectorEntries (tCliHandle CliHandle, UINT4 u4Index);

VOID IPSecv6DeleteSecAssocEntries (tCliHandle CliHandle, UINT4 u4Index);

VOID IPSecv6DeleteAccessEntries (tCliHandle CliHandle, UINT4 u4Index);

VOID IPSecv6CryptoConfigMode (tCliHandle CliHandle);

INT4 IPSecv6ShowRunningconfig (tCliHandle CliHandle);

INT4 IPSecCheckAnyConfigDone (tCliHandle CliHandle); 

INT4 SecPolicyTableShowRunningconfig (tCliHandle CliHandle);

INT4 SecAssocTableShowRunningconfig (tCliHandle CliHandle);

INT4 SelectorTableShowRunningconfig (tCliHandle CliHandle);

INT4 AccessTableShowRunningconfig (tCliHandle CliHandle);

extern INT4 CfaGetIfAlias PROTO((UINT4 u4IfIndex, UINT1 *au1IfAlias));

extern INT1 nmhGetIfType PROTO ((INT4 , INT4 *));
