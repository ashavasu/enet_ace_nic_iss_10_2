
/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
 * $Id: secv6ike.h,v 1.4 2011/12/05 14:39:04 siva Exp $*
*
* Description:This file contains the prototypes,type definitions 
*             and #define constants required for ipsec to ike interface.
*
*******************************************************************/

#ifndef _IPSECV6IKE_H_
#define _IPSECV6IKE_H_





#define SEC_BYTES_PER_KB      1024

/* -------------------- IKE releated Function protos-----*/
INT1 Secv6ProcessIkeInstallSAMsg PROTO((tIkeIPSecQMsg *pMsg));
VOID Secv6ProcessIkeDupSPIMsg PROTO((tIkeIPSecQMsg *pInfo));
VOID Secv6ProcessIkeDeleteSAMsg PROTO((tIkeIPSecQMsg *pMsg));
VOID Secv6ProcessIkeInitialContactMsg PROTO((tIkeIPSecQMsg *pMsg));
VOID Secv6ProcessIkeDeleteSAByAddrMsg PROTO((tIkeIPSecQMsg *pMsg));
INT1 Secv6InstallSAFromIke PROTO((tIPSecSA *pIkeSa,tIp6Addr *pDestAddr,
                                  tIp6Addr *pSrcAddr,UINT4 u4SaIndex));

VOID Secv6ConvertIkeIPSecSaToHostOrder PROTO ((tIPSecSA *pIkeIPSecSa));
VOID Secv6ConvertIkeInstallSAMsgToHostOrder PROTO((tIPSecBundle *pIkeIPSecSa));
#endif
