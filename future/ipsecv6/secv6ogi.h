
# ifndef fssecOGP_H
# define fssecOGP_H

 /* The Definitions of the OGP Index Constants.  */

# define SNMP_OGP_INDEX_FSIPV6SECSCALARS                             (0)
# define SNMP_OGP_INDEX_FSIPV6SECSELECTORTABLE                       (1)
# define SNMP_OGP_INDEX_FSIPV6SECACCESSTABLE                         (2)
# define SNMP_OGP_INDEX_FSIPV6SECPOLICYTABLE                         (3)
# define SNMP_OGP_INDEX_FSIPV6SECASSOCTABLE                          (4)
# define SNMP_OGP_INDEX_FSIPV6SECIFSTATSTABLE                        (5)
# define SNMP_OGP_INDEX_FSIPV6SECAHESPSTATSTABLE                     (6)
# define SNMP_OGP_INDEX_FSIPV6SECAHESPINTRUTABLE                     (7)

#endif /*  fssecv6OGP_H  */
