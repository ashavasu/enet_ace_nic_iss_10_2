/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secv6conf.c,v 1.11 2013/10/29 11:42:37 siva Exp $
 *
 * Description:This file contains the configuration handler for the
 *  IPSEC  Module.
 *
 *******************************************************************/
#include "secv6com.h"
#include "secv6.h"
/************************************************************************/
/*  Function Name   :Secv6AssocInLookUp                            */
/*  Description     : This function returns the SA DataBase From the    */
/*                  : SecurityAssociation List using the SPI as Index   */
/*  Input(s)        : SPI:SecurityParameterIndex                        */
/*                  : DestAddr:Refers to the DestAddress                */
/*                  : u1protocol:Refers to the Protocol                 */
/*  Output(s)       : SecurityAssociationDataBase                       */
/*  Returns         : Pointer to SAEntry or NULL                         */
/************************************************************************/
tSecv6Assoc        *
Secv6AssocInLookUp (UINT4 Spi, tIp6Addr DestAddr, UINT1 u1Protocol)
{
    /* Returns the Sec Assoc Entry */
    return (Secv6GetAssocEntry (Spi, DestAddr, u1Protocol));
}

/************************************************************************/
/*  Function Name   :Secv6AssocVerify                              */
/*  Description     : This function returns the Policy Flag  from the   */
/*                  : Policy List using the Input Parameters            */
/*  Input(s)        : SrcAddr:Refers to Ipv6 src Addr                   */
/*                  : DestAddr:Refers to Ipv6 Dest Addr                 */
/*                  : u4Protocol:Refers to the Incoming Protocol        */
/*                  : u4Index:Refers to the Index on which the Packet   */
/*                  : u4AccessIndex:Pointer to the InGroupIndex to   */
/*                  : get the Range of Address                          */
/*                  :u4Port:The Port no refers to the applications of   */
/*                  :IP and its above layer                             */
/*                  :                                                   */
/*  Output(s)       :                                                   */
/*  Returns         : Policy Flag                                       */
/************************************************************************/
INT4
Secv6AssocVerify (tIp6Addr SrcAddr, tIp6Addr DestAddr, UINT4 u4Protocol,
                  UINT4 u4Index, UINT4 *pu4SaIndexes, UINT4 u4Port)
{
    tSecv6Selector     *pSelEntry = NULL;
    UINT1               Flag;
    UINT1               u1PktDirection = SEC_INBOUND;
    UINT4               u4AccessIndex;

    /* To get the Group for which this source address belongs */

    u4AccessIndex = Secv6GetAccessIndex (SrcAddr, DestAddr);

    if (u4AccessIndex == SEC_FAILURE)
    {
        SECv6_TRC (SECv6_CONTROL_PLANE,
                   "SecAssoc Verify Fails :Address Mismatch with Group Address\n");
        return (SEC_FILTER);
    }

    /* Gets the apppropraite selector entry from the selector database */

    pSelEntry =
        Secv6SelGetFormattedEntry (u4Index, u4Protocol, u4AccessIndex,
                                   (INT4) u4Port, (INT4) u1PktDirection);

    if (pSelEntry == NULL)
    {
        SECv6_TRC (SECv6_CONTROL_PLANE,
                   "SecAssoc Verify Fails : No Selector Entry For Given Inputs\n");
        return (SEC_FILTER);
    }

    if (pSelEntry->u1SelFilterFlag == SEC_FILTER)
    {
        SECv6_TRC (SECv6_CONTROL_PLANE,
                   "SecAssoc Verify : Filter the packet\n");
        return (SEC_FILTER);
    }

    /* Check the SaBundle applied is same as in the Sec Assoc Entry */

    Flag = Secv6CheckPolicySaBundle (pSelEntry->pPolicyEntry, pu4SaIndexes);
    if (Flag == 0)
    {
        SECv6_TRC (SECv6_CONTROL_PLANE,
                   "SecAssoc Verify Fails :Mismatch of SaBunlde \n");
        return (SEC_FILTER);
    }

    if (pSelEntry->pPolicyEntry == NULL)
    {
        SECv6_TRC (SECv6_CONTROL_PLANE,
                   "Secv6CheckPolicySaBundle :No Policy Entry \n");
        return (SEC_FAILURE);
    }

    /* Get the Policy Flag Form the Policy Data Base */

    Flag = pSelEntry->pPolicyEntry->u1PolicyFlag;

    if (Flag == 0)
    {
        SECv6_TRC (SECv6_CONTROL_PLANE,
                   "SecAssoc Verify Fails :No Policy Entry \n");
        return (SEC_FILTER);
    }

    return ((INT4) Flag);
}

/************************************************************************/
/*  Function Name   : Secv6AssocOutLookUp                                 */
/*  Description     : This function returns the no of SA and the pointer*/
/*                  : to the SA Database                                */
/*                  :                                                   */
/*  Input(s)        : SrcAddr:Refers to Sec Src Addr                    */
/*                  : DestAddr:Refers to Sec Dest Addr                  */
/*                  : u1Protocol:Refers to the Protocol of the Packet   */
/*                  : u4Index:Refers to the Index on which the PAcket   */
/*                  : Arrives                                           */
/*                  :pu1InAccessIndex:Pointer to the InAccessIndex to   */
/*                  : get the range of address                          */
/*                  :pu1OutAccessIndex:Pointer to the OutAccessIndex to */
/*                  :get the range of address                           */
/*                  :pSecAssoc:Pointers to the Sec Assoc Entrys         */
/*                  :u4Port:Refers to the Application of Ip             */
/*                  :                                                   */
/*  Output(s)       :                                                   */
/*  Returns         : Returns the no of SA's                            */
/************************************************************************/
INT1
Secv6AssocOutLookUp (tIp6Addr SrcAddr, tIp6Addr DestAddr, UINT4 u4Protocol,
                     UINT4 u4Index, tSecv6Policy ** pSecv6Policy, UINT4 u4Port,
                     tBufChainHeader * pBufDesc)
{
    tSecv6Selector     *pSelEntry = NULL;
    INT1                Flag;
    UINT1               u1PktDirection = SEC_OUTBOUND;
    UINT4               u4AccessIndex;
    INT4                i4SrcAddrType;
    INT4                i4DestAddrType;

#ifndef IKE_WANTED
    UNUSED_PARAM (pBufDesc);
#endif
    /* Check the Address Type */
#ifdef IKE_WANTED
    if ((u4Protocol == SEC_UDP) && ((u4Port == IKE_UDP_PORT)
                                    || u4Port == IKE_NATT_UDP_PORT))
    {
        return SEC_FAILURE;
    }
#endif

    i4SrcAddrType = Ip6AddrType (&(SrcAddr));
    i4DestAddrType = Ip6AddrType (&(DestAddr));

    if (((i4SrcAddrType != IPSEC_ADDR_UNICAST) &&
         (i4SrcAddrType != ADDR6_LLOCAL)) ||
        ((i4DestAddrType != IPSEC_ADDR_UNICAST) &&
         (i4DestAddrType != ADDR6_LLOCAL)))
    {
        return SEC_FAILURE;
    }

    /* Get the Group from the Access DataBase for the given DestAddr */

    u4AccessIndex = Secv6GetAccessIndex (SrcAddr, DestAddr);

    if (u4AccessIndex == SEC_FAILURE)
    {
        SECv6_TRC (SECv6_CONTROL_PLANE,
                   "Secv6AssocOutLookUp Fails :Address mismatch with group address \n");
        return SEC_FAILURE;
    }

    /* Get the Selector Entry from the Formatted Selector List */

    pSelEntry =
        Secv6SelGetFormattedEntry (u4Index, u4Protocol, u4AccessIndex,
                                   (INT4) u4Port, (INT4) u1PktDirection);
    if (pSelEntry == NULL)
    {
        SECv6_TRC (SECv6_CONTROL_PLANE,
                   "Secv6AssocOutLookUp Fails :No Selector Entry \n");
        return SEC_FAILURE;
    }

    if (pSelEntry->u1SelFilterFlag == SEC_FILTER)
    {

        SECv6_TRC (SECv6_CONTROL_PLANE,
                   "Secv6AssocOutLookUp :Filter the packet \n");
        return ((INT1) SEC_REJECT);
    }

    /* Get the Policy Flag from the Policy DataBase */
    if (pSelEntry->pPolicyEntry == NULL)
    {

        SECv6_TRC (SECv6_ALL_FAILURE, "Secv6AssocOutLookUp: No Policy Entry\n");
        return ((INT1) SEC_REJECT);
    }

    *pSecv6Policy = pSelEntry->pPolicyEntry;
    Flag = (INT1) pSelEntry->pPolicyEntry->u1PolicyFlag;

    if (Flag == SEC_BYPASS)
    {
        SECv6_TRC (SECv6_CONTROL_PLANE,
                   "Secv6AssocOutLookUp :Bypass the Packet \n");
        return SEC_FAILURE;
    }

    Flag = (INT1) pSelEntry->pPolicyEntry->u1SaCount;

    if ((Flag == 0) && (pSelEntry->pPolicyEntry->u1PolicyMode == SEC_MANUAL))
    {
        SECv6_TRC (SECv6_CONTROL_PLANE,
                   "Secv6AssocOutLookUp: No SA's For Securing the Pkt\n");
        return ((INT1) SEC_REJECT);
    }

#ifdef IKE_WANTED
    else if ((Flag == 0)
             && (pSelEntry->pPolicyEntry->u1PolicyMode == SEC_AUTOMATIC))
    {

        if (pSelEntry->pPolicyEntry->u1ReqIkeFlag != SEC_NEW_KEY)
        {
            SECv6_TRC (SECv6_CONTROL_PLANE,
                       "Secv6AssocOutLookUp: Request Ike For Negotiating SA\n");
            pSelEntry->pPolicyEntry->u1ReqIkeFlag = SEC_NEW_KEY;
            Secv6RequeStIkeForNewOrReKey (u4Index, &pSelEntry->TunnelTermAddr,
                                          u4Protocol, u4Port,
                                          u4AccessIndex, SEC_OUTBOUND,
                                          SEC_NEW_KEY,
                                          pSelEntry->pPolicyEntry->
                                          u1IkeVersion);
/* ANANTH */
            pSelEntry->pPolicyEntry->Secv6PolicyTrigIkeTmr.u4Param1 =
                (FS_ULONG) pSelEntry->pPolicyEntry;
            pSelEntry->pPolicyEntry->Secv6PolicyTrigIkeTmr.u1TimerId =
                SECv6_SA_TRIGGER_TIMER_ID;
            pSelEntry->pPolicyEntry->Secv6PolicyTrigIkeTmr.u1TimerStatus =
                SEC_TIMER_ACTIVE;
            Secv6StartTimer (&(pSelEntry->pPolicyEntry->Secv6PolicyTrigIkeTmr),
                             SEC_TRIGGER_IKE_TIME_INTERVAL);

        }
        return ((INT1) SEC_REJECT);
    }
    else if (Secv6CheckNoOfOutBoundBytesSecured
             (pSelEntry->pPolicyEntry, pBufDesc) == SEC_BYTES_SECURED_EXCEEDED)
    {
        SECv6_TRC (SECv6_CONTROL_PLANE,
                   "Secv6AssocOutLookUp:  No Of Bytes Secured Exceeded\n");
        Secv6IntimateIkeDeletionOfSa (pSelEntry->pPolicyEntry,
                                      pSelEntry->pPolicyEntry->paSaEntry[0]->
                                      u4SecAssocIndex);
        return ((INT1) SEC_REJECT);
    }
    else if (Secv6CheckNoOfOutBoundBytesSecured
             (pSelEntry->pPolicyEntry, pBufDesc) == SEC_THRESHOLD_REACHED)
    {

        SECv6_TRC (SECv6_CONTROL_PLANE,
                   "Secv6AssocOutLookUp:  Request Ike For RE-KEY\n");
        Secv6RequeStIkeForNewOrReKey (u4Index, &pSelEntry->TunnelTermAddr,
                                      u4Protocol, u4Port,
                                      u4AccessIndex, SEC_OUTBOUND, SEC_RE_KEY,
                                      pSelEntry->pPolicyEntry->u1IkeVersion);
    }
#endif

    return (Flag);
}

/************************************************************************/
/*  Function Name   : Secv6CheckPolicySaBundle                          */
/*  Description     : This function checks whether corrcet set of SA's  */
/*                  : are applied for decoding the packet               */
/*                  :                                                   */
/*  Input(s)        : pPolicy      : Pointer to Policy Data Base        */
/*                  : pu4SaIndexes :Pointer to the Array of SA Indexes  */
/*                  : which are used forf decoding the packet           */
/*                  :                                                   */
/*  Output(s)       :                                                   */
/*  Returns         : Returns NULL on wrong SA's applied for decoding   */
/*                  : the packet                                        */
/************************************************************************/

UINT1
Secv6CheckPolicySaBundle (tSecv6Policy * pEntry, UINT4 *pu4SaIndexes)
{
    tSecv6Assoc        *pSecAssoc = NULL;
    UINT1               u1Count = 0;
    UINT1               u1Flag = SEC_FOUND;
    UINT4              *pu4TempSaIndexes = NULL;
    UINT1               u1MaxSaReKeyCount = 0;

    pu4TempSaIndexes = pu4SaIndexes;

    if (pEntry == NULL)
    {
        SECv6_TRC (SECv6_ALL_FAILURE,
                   "Secv6CheckPolicySaBundle: No Policy Entry \n");
        return (0);
    }

    /* Verify the SA's applied for decoding the Pkt are same as
       that in the Policy Data Base */
    pSecAssoc = pEntry->paSaEntry[u1Count];
    if (pSecAssoc == NULL)
    {
        if ((*pu4SaIndexes) != 0)
        {
            u1Flag = SEC_NOT_FOUND;
        }
    }
    while ((pSecAssoc != NULL) && (u1Count < SEC_MAX_BUNDLE_SA - 1))
    {
        if ((*pu4SaIndexes) != pSecAssoc->u4SecAssocIndex)
        {
            u1Flag = SEC_NOT_FOUND;
            break;
        }
        u1Count++;
        pu4SaIndexes++;
        pSecAssoc = pEntry->paSaEntry[u1Count];
    }

    if (u1Flag == SEC_NOT_FOUND)
    {
        for (u1MaxSaReKeyCount = 0; u1MaxSaReKeyCount < SEC_MAX_SA_TO_POLICY;
             u1MaxSaReKeyCount++)
        {
            u1Count = 0;
            pSecAssoc = NULL;
            u1Flag = SEC_FOUND;
            pu4SaIndexes = pu4TempSaIndexes;
            while ((*pu4SaIndexes) != 0)
            {
                pSecAssoc = pEntry->pau1NewSaEntry[u1MaxSaReKeyCount][u1Count];
                if (pSecAssoc == NULL)
                {
                    u1Flag = SEC_NOT_FOUND;
                    break;
                }
                if ((*pu4SaIndexes) != pSecAssoc->u4SecAssocIndex)
                {
                    u1Flag = SEC_NOT_FOUND;
                }
                u1Count++;
                pu4SaIndexes++;
            }
            if ((u1Count > 0) && (u1Flag == SEC_FOUND))
            {
                break;
            }
        }
    }
    if (u1Flag == SEC_NOT_FOUND)
    {
        SECv6_TRC (SECv6_CONTROL_PLANE, "Decoded With Wrong SA\n");
    }
    return (u1Count);
}

/*****************************************************************************/
/*  Function Name   : Secv6GetAccessIndex                                   */
/*  Description     : This function is used return the InAccessIndex        */
/*                  :                                                       */
/*                  :                                                       */
/*                  :                                                       */
/*  Input(s)        : SrcAddr:Refers to the Source Address                  */
/*                  :                                                       */
/*                  :                                                       */
/*  Output(s)       :                                                       */
/*  Returns         :Returns the InAcess Index                              */
/****************************************************************************/
UINT4
Secv6GetAccessIndex (tIp6Addr SrcAddr, tIp6Addr DestAddr)
{

    tSecv6Access       *pEntry = NULL;
    tIp6Addr            TmpIp6Addr;

    IPSEC_MEMSET (&TmpIp6Addr, 0, sizeof (tIp6Addr));

    TMO_SLL_Scan (&Secv6AccessList, pEntry, tSecv6Access *)
    {
        if ((Ip6AddrMatch
             (&SrcAddr, &pEntry->SrcAddress,
              (INT4) pEntry->u4SrcAddrPrefixLen) == TRUE)
            &&
            (Ip6AddrMatch
             (&DestAddr, &pEntry->DestAddress,
              (INT4) pEntry->u4DestAddrPrefixLen) == TRUE))
        {
            if (pEntry->u4AccStatus == ACTIVE)
            {
                return (pEntry->u4GroupIndex);
            }
        }
        else if (((IPSEC_MEMCMP (&(pEntry->SrcAddress), &TmpIp6Addr,
                                 sizeof (tIp6Addr)) == 0) &&
                  (pEntry->u4SrcAddrPrefixLen == 128)) &&
                 ((IPSEC_MEMCMP (&(pEntry->DestAddress), &TmpIp6Addr,
                                 sizeof (tIp6Addr)) == 0) &&
                  (pEntry->u4DestAddrPrefixLen == 128)))
        {
            if (pEntry->u4AccStatus == ACTIVE)
            {
                return (pEntry->u4GroupIndex);
            }
        }
    }
    return (SEC_FAILURE);
}

/*****************************************************************************/
/*  Function Name   : Secv6GetIfIndexFromAddr                               */
/*  Description     : This function is used return the IfIndex using the    */
/*                  : tunnel termination address                            */
/*                  :                                                       */
/*                  :                                                       */
/*  Input(s)        : pIp6Addr:Refers to the tunnel termination Address     */
/*                  :                                                       */
/*                  :                                                       */
/*  Output(s)       :                                                       */
/*  Returns         :Returns the IfIndex or SEC_FAILURE                     */
/****************************************************************************/
UINT4
Secv6GetIfIndexFromAddr (tIp6Addr * pIp6Addr)
{
    tSecv6Selector     *pEntry = NULL;

    TMO_SLL_Scan (&Secv6SelFormattedList, pEntry, tSecv6Selector *)
    {
        if (IPSEC_MEMCMP (&pEntry->TunnelTermAddr, pIp6Addr, sizeof (tIp6Addr))
            == 0)
        {
            if (pEntry->u4SelStatus == ACTIVE)
            {
                return (pEntry->u4IfIndex);
            }

        }
    }
    return (SEC_FAILURE);
}

/******************************************************************************/
