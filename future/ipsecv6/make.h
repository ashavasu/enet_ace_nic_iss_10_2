###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################

SEC_KEY_OPNS  = -USECv6_PRIVATE_KEY

GLOBAL_OPNS = ${SEC_KEY_OPNS} \
              ${GENERAL_COMPILATION_SWITCHES} ${SYSTEM_COMPILATION_SWITCHES}

############################################################################
#                         Directories                                      #
############################################################################

IPSECv6_DIR = ${BASE_DIR}/ipsecv6

CLI_UTL_DIR = ${BASE_DIR}/util/cli

IPSECV6_DPNDS  =  ${IPSECv6_DIR}/secv6com.h\
                  ${IPSECv6_DIR}/secv6inc.h\
                  ${IPSECv6_DIR}/secv6trace.h\
                  ${IPSECv6_DIR}/secv6mdb.h\
                  ${IPSECv6_DIR}/secv6ah.h\
                  ${IPSECv6_DIR}/secv6con.h\
                  ${IPSECv6_DIR}/secv6esp.h\
                  ${IPSECv6_DIR}/secv6low.h\
                  ${IPSECv6_DIR}/secv6mid.h\
                  ${IPSECv6_DIR}/secv6ogi.h\
                  ${IPSECv6_DIR}/secv6cliprot.h\
                  ${IPSECv6_DIR}/secv6db.h\
                  ${IPSECv6_DIR}/secv6wr.h\
                  ${IPSECv6_DIR}/Makefile\
                  ${IPSECv6_DIR}/make.h \
                  ${COMMON_DEPENDENCIES}

#############################################################################

