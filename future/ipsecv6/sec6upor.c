/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: sec6upor.c,v 1.2 2011/08/24 06:36:55 siva Exp $
 *
 * Description : This file contains the funtions defined for the
 *               userspace
 *
 *******************************************************************/
#include "secv6com.h"
#include "secv6.h"

/************************************************************************/
/*  Function Name   : Secv6GetSecv6AssocOverHead                        */
/*  Description     : This function returns the Sec overhead            */
/*                  :                                                   */
/*  Input(s)        : SrcAddr:Refers to Ipv6 src Addr                   */
/*                    DestAddr:Refers to Ipv6 dest Addr                 */
/*                  : u2Index:Refers to the InterfaceIndex              */
/*                  : u2Protocol Refers to the Protocol                 */
/*  Output(s)       :                                                   */
/*  Returns         : Returns SecOverHead                               */
/************************************************************************/
UINT2
Secv6GetSecAssocOverHead (tIp6Addr SrcAddr, tIp6Addr DestAddr, UINT2 u2Index,
                          UINT1 u1Protocol)
{
    tSecv6Selector     *pSelEntry = NULL;
    tSecv6Assoc        *pSaEntry = NULL;
    tSecv6Policy       *pPolicyEntry = NULL;
    UINT4               u4AccessIndex;
    UINT2               u2Size = 0, u2Count = 0;

    if (gSecv6Status != SEC_ENABLE)
    {
        return (u2Size);
    }

    u4AccessIndex = Secv6GetAccessIndex (SrcAddr, DestAddr);

    if (u4AccessIndex == SEC_FAILURE)
    {
        return (u2Size);
    }

    TMO_SLL_Scan (&Secv6SelFormattedList, pSelEntry, tSecv6Selector *)
    {
        if ((pSelEntry->u4IfIndex == (UINT4) u2Index) &&
            ((pSelEntry->u4ProtoId == (UINT4) u1Protocol) ||
             (pSelEntry->u4ProtoId == SEC_ANY_PROTOCOL)) &&
            (pSelEntry->u4SelAccessIndex == u4AccessIndex) &&
            (pSelEntry->u4SelStatus == ACTIVE))
        {

            if (pSelEntry != NULL)
            {
                pPolicyEntry = pSelEntry->pPolicyEntry;
                if (pPolicyEntry == NULL)
                {
                    SECv6_TRC (SECv6_CONTROL_PLANE,
                               "Secv6CheckPolicySaBundle :No Policy Entry \n");
                    return (SEC_FAILURE);
                }
            }
            if (pPolicyEntry != NULL)
            {
                if (pPolicyEntry->u4PolicyStatus != ACTIVE)
                {
                    SECv6_TRC (SECv6_CONTROL_PLANE,
                               "Secv6CheckPolicySaBundle :Policy Entry is not Active\n");
                    return (SEC_FAILURE);
                }
                for (u2Count = 0; u2Count < pPolicyEntry->u1SaCount; u2Count++)
                {
                    pSaEntry = pPolicyEntry->paSaEntry[u2Count];
                    if (pSaEntry != NULL && pSaEntry->u4AssocStatus == ACTIVE)
                    {
                        u2Size = (UINT2) (u2Size + pSaEntry->u2Size);
                    }
                }
            }
        }

    }

    return (u2Size);
}

/************************************************************************/
/*  Function Name   : Secv6AccListExist                                 */
/*  Description     : This function returns whether Access List exist   */
/*                  : for the given Source and Destination              */
/*  Input(s)        : SrcAddr  - Refers to Ipv6 Source Address          */
/*                  : DestAddr - Refers to Ipv6 Destination Address     */
/*  Output(s)       :                                                   */
/*  Returns         : Returns SEC_FAILURE or SEC_SUCCESS                */
/************************************************************************/
INT1
Secv6AccListExist (tIp6Addr SrcAddr, tIp6Addr DestAddr)
{
    UINT4               u4AccessIndex = 0;

    if (gSecv6Status != SEC_ENABLE)
    {
        return (SEC_FAILURE);
    }

    u4AccessIndex = Secv6GetAccessIndex (SrcAddr, DestAddr);

    if (u4AccessIndex == SEC_FAILURE)
    {
        return (SEC_FAILURE);
    }
    return (SEC_SUCCESS);
}
