
/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id:secv6esp.h
*
* Description:This file contains the macros required for ESP Module. 
*
*******************************************************************/


#ifndef _SECV6ESP_H_
#define _SECV6ESP_H_

/* Macros related to esp header */

#define SEC_ESP_TRAILER_LEN                 2
#define SEC_ESP_TRAILER_SIZE_LEN            1
#define SEC_ESP_MULTIPLY_FACTOR             8
#define SEC_ESP_NULL_MULTIPLY_FACTOR        4
#define SEC_ESP_HEADER_LEN                  8
#define SEC_ESP_SEQ_OFFSET                  4
#define SEC_ESP_HEADER                      2


/* Proto Types for the functions used in secv6esp.c */
static tBufChainHeader * 
Secv6EspTransportEncode PROTO ((tBufChainHeader * pBuf,
                                tSecv6Assoc * pSaEntry,UINT4 u4IfIndex));
static  tBufChainHeader *
Secv6EspTunnelEncode PROTO ((tBufChainHeader * pBuf,
                           tSecv6Assoc * pSaEntry,UINT4 u4Interface));
static tBufChainHeader *
Secv6EspTransportDecode PROTO ((tBufChainHeader * pBuf,
                          tSecv6Assoc * pSaEntry,UINT4 *pu4ProcessedLen));
static tBufChainHeader *
Secv6EspTunnelDecode PROTO ((tBufChainHeader * pBuf,
                        tSecv6Assoc * pSaEntry,UINT4 *pu4ProcessedLen));

#endif
