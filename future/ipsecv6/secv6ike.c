/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secv6ike.c,v 1.14 2014/03/16 11:40:29 siva Exp $
 *
 * Description: This has functions for interacting with IKE
 *
 ***********************************************************************/

#include "secv6com.h"
#include "secv6ike.h"
#include "snmcdefn.h"
#include "snmctdfs.h"
#include "snmccons.h"
#include "secv6low.h"
#include "fssecvcli.h"
#include "secv6wr.h"

#ifdef LNXIP4_WANTED
VOID                Secv6ProcessIKEMsg (tIkeIPSecQMsg * pMsg);
#endif

/***********************************************************************/
/*  Function Name : Secv6ProcessIKEMsg                                 */
/*  Description   : This function invokes the appropraite function to  */
/*                : process the IKE Message based on the Command type  */
/*                : of the IKE Message                                 */
/*                :                                                    */
/*  Input(s)      : pMsg - Points to the Message rcvd from IKE         */
/*                :                                                    */
/*  Output(s)     :None                                                */
/*  Return        :None                                                */
/***********************************************************************/

VOID
Secv6ProcessIKEMsg (tIkeIPSecQMsg * pMsg)
{
    switch (IPSEC_NTOHL (pMsg->u4MsgType))
    {
        case IKE_IPSEC_INSTALL_SA:
            Secv6ProcessIkeInstallSAMsg (pMsg);
            break;
        case IKE_IPSEC_DUPLICATE_SPI_REQ:
            Secv6ProcessIkeDupSPIMsg (pMsg);
            break;
        case IKE_IPSEC_PROCESS_DELETE_SA:
            Secv6ProcessIkeDeleteSAMsg (pMsg);
            break;
        case IKE_IPSEC_PROCESS_DEL_SA_BY_ADDR:
            Secv6ProcessIkeDeleteSAByAddrMsg (pMsg);
            break;
        default:
            break;
    }
    return;
}

/***********************************************************************/
/*  Function Name : Secv6ProcessIkeInstallSAMsg                        */
/*  Description   : This function process the Install SA message from  */
/*                : IKE          .                                     */
/*                :                                                    */
/*  Input(s)      : pMsg - Pointer to the Msg Rcvd From IKE            */
/*                :                                                    */
/*  Output(s)     : Creates  SA in SAD                                 */
/*  Return        :SEC_SUCCESS or SEC_FAILURE                          */
/***********************************************************************/

INT1
Secv6ProcessIkeInstallSAMsg (tIkeIPSecQMsg * pMsg)
{

    tIPSecBundle       *pInstallSAMsg = NULL;
    tSecv6Selector     *pSelEntry = NULL;
    UINT4               u4SnmpErrorStatus = SNMP_FAILURE;
    tSNMP_OCTET_STRING_TYPE OctetStr;
    UINT4               u4AccessIndex = SEC_FAILURE;
    UINT1               au1SaBundle[SEC_MAX_SA_BUNDLE_LEN];
    UINT4               u4IfIndex = 0;
    UINT4               u4SaIndex0 = 0;
    UINT4               u4SaIndex1 = 0;
    tIp6Addr           *pIp6StartSrcAddr = NULL;
    tIp6Addr           *pIp6StartDestAddr = NULL;

    IPSEC_MEMSET (&au1SaBundle, 0, SEC_MAX_SA_BUNDLE_LEN);

    pInstallSAMsg = &(pMsg->IkeIfParam.InstallSA);

    Secv6ConvertIkeInstallSAMsgToHostOrder (pInstallSAMsg);

    u4IfIndex =
        Secv6GetIfIndexFromAddr (&pInstallSAMsg->LocTunnTermAddr.uIpAddr.
                                 Ip6Addr);

    if (u4IfIndex == SEC_FAILURE)
    {
        SECv6_TRC (SECv6_CONTROL_PLANE,
                   "Secv6ProcessIkeInstallSAMsg: Unable to get IfIndex\n");
        return (SEC_FAILURE);
    }

    if ((pInstallSAMsg->LocalProxy.u4Type != IKE_IPSEC_ID_IPV6_SUBNET) &&
        (pInstallSAMsg->LocalProxy.u4Type != IKE_IPSEC_ID_IPV6_ADDR))
    {
        SECv6_TRC (SECv6_CONTROL_PLANE,
                   "Secv6ProcessIkeInstallSAMsg: Invalid Local Proxy Type\n");
        return (SEC_FAILURE);
    }

    pIp6StartSrcAddr = &pInstallSAMsg->LocalProxy.uNetwork.Ip6Subnet.Ip6Addr;
    pIp6StartDestAddr = &pInstallSAMsg->RemoteProxy.uNetwork.Ip6Subnet.Ip6Addr;

    u4AccessIndex = Secv6GetAccessIndex (*pIp6StartDestAddr, *pIp6StartSrcAddr);

    if (u4AccessIndex == SEC_FAILURE)
    {
        SECv6_TRC (SECv6_CONTROL_PLANE,
                   "Secv6ProcessIkeInstallSAMsg: Access Look-Up For InBound Failed\n");
        return SEC_FAILURE;
    }

    if (pInstallSAMsg->aInSABundle[0].u4Spi != 0)
    {
        u4SaIndex0 = Secv6GetFreeSAIdx ();
        if (Secv6InstallSAFromIke
            (&pInstallSAMsg->aInSABundle[0],
             &pInstallSAMsg->LocTunnTermAddr.uIpAddr.Ip6Addr,
             &pInstallSAMsg->RemTunnTermAddr.uIpAddr.Ip6Addr,
             u4SaIndex0) != SEC_SUCCESS)
        {
            SECv6_TRC (SECv6_CONTROL_PLANE,
                       "Secv6ProcessIkeInstallSAMsg: Secv6InstallSAFromIke Failed\n");
            return (SEC_FAILURE);
        }
        if (pInstallSAMsg->aInSABundle[1].u4Spi != 0)
        {
            u4SaIndex1 = Secv6GetFreeSAIdx ();
            if (Secv6InstallSAFromIke
                (&pInstallSAMsg->aInSABundle[1],
                 &pInstallSAMsg->LocTunnTermAddr.uIpAddr.Ip6Addr,
                 &pInstallSAMsg->RemTunnTermAddr.uIpAddr.Ip6Addr,
                 u4SaIndex1) != SEC_SUCCESS)
            {
                SECv6_TRC (SECv6_CONTROL_PLANE,
                           "Secv6ProcessIkeInstallSAMsg: Secv6InstallSAFromIke Failed\n");
                return (SEC_FAILURE);
            }
            SECv6_SNPRINTF ((CHR1 *) au1SaBundle, SEC_MAX_SA_BUNDLE_LEN,
                            "%d.%d", u4SaIndex0, u4SaIndex1);
        }
        else
        {
            SECv6_SNPRINTF ((CHR1 *) au1SaBundle, SEC_MAX_SA_BUNDLE_LEN, "%d",
                            u4SaIndex0);
        }
        pSelEntry =
            Secv6SelGetFormattedEntry (u4IfIndex,
                                       pInstallSAMsg->aInSABundle[0].
                                       u2HLProtocol, u4AccessIndex,
                                       pInstallSAMsg->aInSABundle[0].
                                       u2HLPortNumber, SEC_INBOUND);

    }
    else if (pInstallSAMsg->aInSABundle[1].u4Spi != 0)
    {
        u4SaIndex1 = Secv6GetFreeSAIdx ();
        if (Secv6InstallSAFromIke
            (&pInstallSAMsg->aInSABundle[1],
             &pInstallSAMsg->LocTunnTermAddr.uIpAddr.Ip6Addr,
             &pInstallSAMsg->RemTunnTermAddr.uIpAddr.Ip6Addr,
             u4SaIndex1) != SEC_SUCCESS)
        {
            SECv6_TRC (SECv6_CONTROL_PLANE,
                       "Secv6ProcessIkeInstallSAMsg: Secv6InstallSAFromIke Failed\n");
            return (SEC_FAILURE);
        }
        SECv6_SNPRINTF ((CHR1 *) au1SaBundle, SEC_MAX_SA_BUNDLE_LEN, "%d",
                        u4SaIndex1);
        pSelEntry =
            Secv6SelGetFormattedEntry (u4IfIndex,
                                       pInstallSAMsg->aInSABundle[0].
                                       u2HLProtocol, u4AccessIndex,
                                       pInstallSAMsg->aInSABundle[0].
                                       u2HLPortNumber, SEC_INBOUND);

    }

    if (pSelEntry == NULL)
    {
        SECv6_TRC (SECv6_CONTROL_PLANE,
                   "Secv6ProcessIkeInstallSAMsg: Selector Look-Up"
                   "For InBound Failed\n");
        return SEC_FAILURE;
    }

    if (pSelEntry->pPolicyEntry == NULL)
    {

        SECv6_TRC (SECv6_ALL_FAILURE,
                   "Secv6ProcessIkeInstallSAMsg: No InBound Policy Entry\n");
        return SEC_FAILURE;
    }
    if (pSelEntry->pPolicyEntry->paSaEntry[0] == NULL)
    {
        OctetStr.pu1_OctetList = NULL;
        OctetStr.i4_Length = 0;
        OctetStr.pu1_OctetList = au1SaBundle;
        OctetStr.i4_Length = (INT4) STRLEN (au1SaBundle);
        if (nmhTestv2Fsipv6SecPolicySaBundle (&u4SnmpErrorStatus,
                                              (INT4) pSelEntry->pPolicyEntry->
                                              u4PolicyIndex,
                                              &OctetStr) == SNMP_FAILURE)
        {
            SECv6_TRC (SECv6_CONTROL_PLANE,
                       "Secv6ProcessIkeInstallSAMsg: Unable To Attach "
                       "InBound SA's To Policy\n");
            return SEC_FAILURE;
        }

        nmhSetFsipv6SecPolicySaBundle (pSelEntry->pPolicyEntry->u4PolicyIndex,
                                       &OctetStr);
    }
    else
    {

        Secv6AttachNewSaToPolicy (pSelEntry->pPolicyEntry, au1SaBundle);
    }

    pSelEntry = NULL;
    u4AccessIndex = SEC_FAILURE;
    u4SaIndex0 = 0;
    u4SaIndex1 = 0;
    IPSEC_MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    IPSEC_MEMSET (&au1SaBundle, 0, sizeof (au1SaBundle));

    u4AccessIndex = Secv6GetAccessIndex (*pIp6StartSrcAddr, *pIp6StartDestAddr);
    if (u4AccessIndex == SEC_FAILURE)
    {
        SECv6_TRC (SECv6_CONTROL_PLANE,
                   "Secv6ProcessIkeInstallSAMsg: Access Look-Up For OutBound Failed\n");
        return SEC_FAILURE;
    }

    if (pInstallSAMsg->aOutSABundle[0].u4Spi != 0)
    {

        u4SaIndex0 = Secv6GetFreeSAIdx ();
        if (Secv6InstallSAFromIke (&pInstallSAMsg->aOutSABundle[0],
                                   &pInstallSAMsg->RemTunnTermAddr.uIpAddr.
                                   Ip6Addr,
                                   &pInstallSAMsg->LocTunnTermAddr.uIpAddr.
                                   Ip6Addr, u4SaIndex0) != SEC_SUCCESS)
        {
            SECv6_TRC (SECv6_CONTROL_PLANE,
                       "Secv6ProcessIkeInstallSAMsg: Secv6InstallSAFromIke Failed\n");
            return (SEC_FAILURE);
        }
        if (pInstallSAMsg->aOutSABundle[1].u4Spi != 0)
        {
            u4SaIndex1 = Secv6GetFreeSAIdx ();
            if (Secv6InstallSAFromIke (&pInstallSAMsg->aOutSABundle[1],
                                       &pInstallSAMsg->RemTunnTermAddr.uIpAddr.
                                       Ip6Addr,
                                       &pInstallSAMsg->LocTunnTermAddr.uIpAddr.
                                       Ip6Addr, u4SaIndex1) != SEC_SUCCESS)
            {
                SECv6_TRC (SECv6_CONTROL_PLANE,
                           "Secv6ProcessIkeInstallSAMsg: Secv6InstallSAFromIke Failed\n");
                return (SEC_FAILURE);
            }
            SECv6_SNPRINTF ((CHR1 *) au1SaBundle, SEC_MAX_SA_BUNDLE_LEN,
                            "%d.%d", u4SaIndex0, u4SaIndex1);
        }
        else
        {
            SECv6_SNPRINTF ((CHR1 *) au1SaBundle, SEC_MAX_SA_BUNDLE_LEN, "%d",
                            u4SaIndex0);
        }
        pSelEntry =
            Secv6SelGetFormattedEntry (u4IfIndex,
                                       pInstallSAMsg->aOutSABundle[0].
                                       u2HLProtocol, u4AccessIndex,
                                       pInstallSAMsg->aOutSABundle[0].
                                       u2HLPortNumber, SEC_OUTBOUND);

    }
    else if (pInstallSAMsg->aOutSABundle[1].u4Spi != 0)
    {
        u4SaIndex1 = Secv6GetFreeSAIdx ();
        if (Secv6InstallSAFromIke (&pInstallSAMsg->aOutSABundle[1],
                                   &pInstallSAMsg->RemTunnTermAddr.uIpAddr.
                                   Ip6Addr,
                                   &pInstallSAMsg->LocTunnTermAddr.uIpAddr.
                                   Ip6Addr, u4SaIndex1) != SEC_SUCCESS)
        {
            SECv6_TRC (SECv6_CONTROL_PLANE,
                       "Secv6ProcessIkeInstallSAMsg: Secv6InstallSAFromIke Failed\n");
            return (SEC_FAILURE);
        }
        SECv6_SNPRINTF ((CHR1 *) au1SaBundle, SEC_MAX_SA_BUNDLE_LEN, "%d",
                        u4SaIndex1);
        pSelEntry =
            Secv6SelGetFormattedEntry (u4IfIndex,
                                       pInstallSAMsg->aOutSABundle[0].
                                       u2HLProtocol, u4AccessIndex,
                                       pInstallSAMsg->aOutSABundle[0].
                                       u2HLPortNumber, SEC_OUTBOUND);

    }

    if (pSelEntry == NULL)
    {
        SECv6_TRC (SECv6_CONTROL_PLANE,
                   "Secv6ProcessIkeInstallSAMsg: Selector Look-Up"
                   "For OutBound Failed\n");
        return SEC_FAILURE;
    }

    if (pSelEntry->pPolicyEntry == NULL)
    {

        SECv6_TRC (SECv6_ALL_FAILURE,
                   "Secv6ProcessIkeInstallSAMsg: No OutBound Policy Entry \n");
        return SEC_FAILURE;
    }

    if (pSelEntry->pPolicyEntry->paSaEntry[0] == NULL)
    {
        OctetStr.pu1_OctetList = NULL;
        OctetStr.i4_Length = 0;
        OctetStr.pu1_OctetList = au1SaBundle;
        OctetStr.i4_Length = (INT4) STRLEN (au1SaBundle);
        if (nmhTestv2Fsipv6SecPolicySaBundle (&u4SnmpErrorStatus,
                                              (INT4) pSelEntry->pPolicyEntry->
                                              u4PolicyIndex,
                                              &OctetStr) == SNMP_FAILURE)
        {
            SECv6_TRC (SECv6_CONTROL_PLANE,
                       "Secv6ProcessIkeInstallSAMsg: Unable To Attach "
                       "OutBound SA To Policy\n");
            return SEC_FAILURE;
        }

        nmhSetFsipv6SecPolicySaBundle (pSelEntry->pPolicyEntry->u4PolicyIndex,
                                       &OctetStr);
    }
    else
    {
        Secv6AttachNewSaToPolicy (pSelEntry->pPolicyEntry, au1SaBundle);
    }

    return SEC_SUCCESS;
}

/***********************************************************************/
/*  Function Name : Secv6ProcessIkeDupSPIMsg                           */
/*  Description   : This function checks if a given SPI is already in  */
/*                : use or not.                                        */
/*                :                                                    */
/*  Input(s)      : pInfo - Pointer to the message from IKE            */
/*                :                                                    */
/*  Output(s)     : Boolean Flag is Set to TRUE if the Spi is duplicate*/
/*  Return        : None                                               */
/***********************************************************************/

VOID
Secv6ProcessIkeDupSPIMsg (tIkeIPSecQMsg * pInfo)
{
    tIkeDupSpi         *pDupSPIInfo = NULL;
    tIkeQMsg           *pMsg = NULL;

    pDupSPIInfo = &(pInfo->IkeIfParam.DupSpiInfo);

    if ((Secv6GetAssocEntry
         (IPSEC_NTOHL (pDupSPIInfo->u4EspSpi),
          pDupSPIInfo->RemoteAddr.Ipv6Addr,
          SEC_ESP) != NULL) ||
        (Secv6GetAssocEntry
         (IPSEC_NTOHL (pDupSPIInfo->u4AhSpi),
          pDupSPIInfo->RemoteAddr.Ipv6Addr, SEC_AH) != NULL))
    {
        SECv6_TRC (SECv6_CONTROL_PLANE,
                   "Secv6ProcessIkeDupSPIMsg: Rcvd Duplicate Spi\n");
        pDupSPIInfo->bDupSpi = TRUE;
    }

    if (MemAllocateMemBlock (IKE_MSG_MEMPOOL_ID, (UINT1 **) (VOID *) &pMsg) ==
        MEM_FAILURE)
    {
        SECv6_TRC (SECv6_CONTROL_PLANE,
                   "Secv6ProcessIkeDupSPIMsg:- Failed to allocate memblock for "
                   " tIkeQMsg \n");
        return;
    }

    if (pMsg == NULL)
    {
        SECv6_TRC (SECv6_OS_RESOURCE,
                   "Secv6RequeStIkeForNewOrReKey :-Memory Allocation Failure\n");
        return;
    }
    pMsg->IkeIfParam.IkeIPSecParam.u4MsgType =
        IPSEC_HTONL (IKE_IPSEC_DUPLICATE_SPI_REPLY);
    pMsg->u4MsgType = IPSEC_TASK;
    /* Fill the version in the reply message same as what is received 
     * in Duplicate SPI request */
    pMsg->u1IkeVer = pDupSPIInfo->u1IkeVersion;
    IPSEC_MEMCPY (&(pMsg->IkeIfParam.IkeIPSecParam.IPSecReq.DupSpiInfo),
                  pDupSPIInfo, sizeof (tIkeDupSpi));
    IkeHandlePktFromIpsec (pMsg);
}

/***********************************************************************/
/*  Function Name : Secv6ProcessIkeDeleteSAMsg                         */
/*  Description   : This function Deletes all the SA's which match with*/
/*                : the rcvd Spi,DestAddr and Protocol                 */
/*                :                                                    */
/*  Input(s)      : pMsg - Pointer to Mesg rcvd from IKE               */
/*                :                                                    */
/*  Output(s)     : Deletes the Matched SA                             */
/*  Return        : None                                               */
/***********************************************************************/

VOID
Secv6ProcessIkeDeleteSAMsg (tIkeIPSecQMsg * pMsg)
{
    UINT4               u4Counter = 0;
    tIkeDelSpi         *pDelSpiInfo = NULL;
    tSecv6Assoc        *pSecAssoc = NULL;
    UINT4               u4SnmpErrorStatus = SNMP_FAILURE;

    pDelSpiInfo = &pMsg->IkeIfParam.DelSpiInfo;
    pDelSpiInfo->u2NumSPI = IPSEC_NTOHS (pDelSpiInfo->u2NumSPI);
    for (u4Counter = 0; u4Counter < pDelSpiInfo->u2NumSPI; u4Counter++)
    {
        pSecAssoc =
            Secv6GetAssocEntry (IPSEC_NTOHL (pDelSpiInfo->au4SPI[u4Counter]),
                                pDelSpiInfo->RemoteAddr.Ipv6Addr,
                                pDelSpiInfo->u1Protocol);
        if (pSecAssoc == NULL)
        {
            SECv6_TRC (SECv6_CONTROL_PLANE,
                       "Secv6ProcessIkeDeleteSAMsg: Unable to find "
                       "SA specified by IKE\n");
        }
        else
        {
            break;

        }
    }

    if (pSecAssoc != NULL)
    {
        if (nmhTestv2Fsipv6SecAssocStatus (&u4SnmpErrorStatus,
                                           (INT4) pSecAssoc->u4SecAssocIndex,
                                           DESTROY) == SNMP_FAILURE)
        {
            return;
        }
        nmhSetFsipv6SecAssocStatus (pSecAssoc->u4SecAssocIndex, DESTROY);
    }

    return;
}

/***********************************************************************/
/*  Function Name : Secv6ProcessIkeDeleteSAByAddrMsg                   */
/*  Description   : This function Deletes all the SA's present between */
/*                : the given pair of address                          */
/*                :                                                    */
/*  Input(s)      : pMsg - Points to the Msg Rcvd from IKE             */
/*                :                                                    */
/*  Return        : None                                               */
/***********************************************************************/

VOID
Secv6ProcessIkeDeleteSAByAddrMsg (tIkeIPSecQMsg * pMsg)
{

    tIkeDelSAByAddr    *pDelSAByAddr = NULL;
    tSecv6Assoc        *pSecv6Assoc = NULL;
    UINT4               u4SnmpErrorStatus = SNMP_FAILURE;
    UINT1               u1Flag = SEC_NOT_FOUND;

    pDelSAByAddr = &(pMsg->IkeIfParam.DelSAInfo);
    while (TRUE)
    {

        TMO_SLL_Scan (&Secv6AssocList, pSecv6Assoc, tSecv6Assoc *)
        {

            if ((MEMCMP (pSecv6Assoc->SecAssocDestAddr.u1_addr,
                         pDelSAByAddr->RemoteAddr.Ipv6Addr.u1_addr,
                         SEC_ADDR_LEN)
                 && MEMCMP (pSecv6Assoc->SecAssocSrcAddr.u1_addr,
                            pDelSAByAddr->LocalAddr.Ipv6Addr.u1_addr,
                            SEC_ADDR_LEN))
                || (MEMCMP (pSecv6Assoc->SecAssocDestAddr.u1_addr,
                            pDelSAByAddr->LocalAddr.Ipv6Addr.u1_addr,
                            SEC_ADDR_LEN)
                    && MEMCMP (pSecv6Assoc->SecAssocSrcAddr.u1_addr,
                               pDelSAByAddr->RemoteAddr.Ipv6Addr.u1_addr,
                               SEC_ADDR_LEN)))

            {
                if (pSecv6Assoc->u4AssocStatus == ACTIVE)
                {
                    u1Flag = SEC_FOUND;
                    break;
                }
            }
        }

        if (u1Flag == SEC_FOUND)
        {
            u1Flag = SEC_NOT_FOUND;

            if (nmhTestv2Fsipv6SecAssocStatus (&u4SnmpErrorStatus,
                                               (INT4) pSecv6Assoc->
                                               u4SecAssocIndex,
                                               DESTROY) == SNMP_FAILURE)
            {
                SECv6_TRC (SECv6_CONTROL_PLANE,
                           "Secv6ProcessIkeDeleteSAByAddrMsg: Deletion of SA Failed\n");
                return;
            }
            nmhSetFsipv6SecAssocStatus (pSecv6Assoc->u4SecAssocIndex, DESTROY);
        }
        else
        {
            break;
        }

    }

}

/***********************************************************************/
/*  Function Name : Secv6InstallSAFromIke                              */
/*  Description   : This function installs the SA rcvd from IKE        */
/*                :                                                    */
/*  Input(s)      : pIkeSA     - Points to the SA that is to be        */
/*                :              installed                             */
/*                : u4DestAddr - Remote Tunnel Termination Address     */
/*                : u4SrcAddr  - Local Tunnel Termination Address      */
/*                : u4SaIndex  - Index of SAD Entry                    */
/*                :                                                    */
/*  Output(s)     :Creates SA Entry in SAD                             */
/*  Return        : SEC_SUCCESS or SEC_FAILURE                         */
/***********************************************************************/

INT1
Secv6InstallSAFromIke (tIPSecSA * pIkeSa, tIp6Addr * pDestAddr,
                       tIp6Addr * pSrcAddr, UINT4 u4SaIndex)
{

    UINT4               u4SnmpErrorStatus = SNMP_FAILURE;
    tSNMP_OCTET_STRING_TYPE OctetStr;
    tSecv6Assoc        *pSecv6Assoc = NULL;
    UINT4               u4Bytes = 0;
    INT4                i4EncrAlgo = 0;

    OctetStr.pu1_OctetList = NULL;
    OctetStr.i4_Length = 0;

    if (nmhTestv2Fsipv6SecAssocStatus
        (&u4SnmpErrorStatus, (INT4) u4SaIndex, CREATE_AND_GO) == SNMP_FAILURE)
    {
        SECv6_TRC (SECv6_CONTROL_PLANE,
                   "Secv6InstallSAFromIke: nmhTestv2Fsipv6SecAssocStatus Failed\n");
        return SEC_FAILURE;
    }

    if (nmhSetFsipv6SecAssocStatus (u4SaIndex, CREATE_AND_GO) == SNMP_FAILURE)
    {

        SECv6_TRC (SECv6_CONTROL_PLANE,
                   "Secv6InstallSAFromIke: nmhSetFsipv6SecAssocStatus Failed\n");
        return SEC_FAILURE;
    }

    OctetStr.pu1_OctetList = IPSECv6_MALLOC (SEC_ADDR_LEN, UINT1);
    if (OctetStr.pu1_OctetList == NULL)
    {
        SECv6_TRC (SECv6_OS_RESOURCE,
                   "Secv6RequeStIkeForNewOrReKey:-Memory Allocation Failure\n");
        return SEC_FAILURE;
    }
    IPSEC_MEMCPY (OctetStr.pu1_OctetList, pDestAddr, SEC_ADDR_LEN);
    OctetStr.i4_Length = SEC_ADDR_LEN;

    if (nmhTestv2Fsipv6SecAssocDstAddr
        (&u4SnmpErrorStatus, (INT4) u4SaIndex, &OctetStr) == SNMP_FAILURE)
    {
        nmhSetFsipv6SecAssocStatus (u4SaIndex, DESTROY);
        SECv6_TRC (SECv6_CONTROL_PLANE,
                   "Secv6InstallSAFromIke: nmhTestv2Fsipv6SecAssocDstAddr Failed\n");
        IPSECv6_MEMFREE (OctetStr.pu1_OctetList);
        return SEC_FAILURE;
    }

    nmhSetFsipv6SecAssocDstAddr (u4SaIndex, &OctetStr);
    IPSECv6_MEMFREE (OctetStr.pu1_OctetList);

    pSecv6Assoc = Secv6AssocGetEntry (u4SaIndex);

    if (pSecv6Assoc != NULL)
    {
        Ip6AddrCopy (&pSecv6Assoc->SecAssocSrcAddr, pSrcAddr);

    }

    if (nmhTestv2Fsipv6SecAssocSpi (&u4SnmpErrorStatus, (INT4) u4SaIndex,
                                    (INT4) pIkeSa->u4Spi) == SNMP_FAILURE)
    {

        nmhSetFsipv6SecAssocStatus (u4SaIndex, DESTROY);
        SECv6_TRC (SECv6_CONTROL_PLANE,
                   "Secv6InstallSAFromIke: nmhTestv2Fsipv6SecAssocSpi Failed\n");
        return SEC_FAILURE;
    }

    nmhSetFsipv6SecAssocSpi (u4SaIndex, pIkeSa->u4Spi);

    if (nmhTestv2Fsipv6SecAssocMode
        (&u4SnmpErrorStatus, (INT4) u4SaIndex, pIkeSa->u1Mode) == SNMP_FAILURE)
    {

        nmhSetFsipv6SecAssocStatus (u4SaIndex, DESTROY);
        SECv6_TRC (SECv6_CONTROL_PLANE,
                   "Secv6InstallSAFromIke: nmhTestv2Fsipv6SecAssocMode Failed\n");
        return SEC_FAILURE;
    }

    nmhSetFsipv6SecAssocMode (u4SaIndex, pIkeSa->u1Mode);

    if (nmhTestv2Fsipv6SecAssocProtocol
        (&u4SnmpErrorStatus, (INT4) u4SaIndex,
         pIkeSa->u1IPSecProtocol) == SNMP_FAILURE)
    {

        nmhSetFsipv6SecAssocStatus (u4SaIndex, DESTROY);
        SECv6_TRC (SECv6_CONTROL_PLANE,
                   "Secv6InstallSAFromIke: nmhTestv2Fsipv6SecAssocProtocol Failed\n");
        return SEC_FAILURE;
    }

    nmhSetFsipv6SecAssocProtocol (u4SaIndex, pIkeSa->u1IPSecProtocol);

    if (nmhTestv2Fsipv6SecAssocAhAlgo (&u4SnmpErrorStatus, (INT4) u4SaIndex,
                                       pIkeSa->u1HashAlgo) == SNMP_FAILURE)
    {

        nmhSetFsipv6SecAssocStatus (u4SaIndex, DESTROY);
        SECv6_TRC (SECv6_CONTROL_PLANE,
                   "Secv6InstallSAFromIke: nmhTestv2Fsipv6SecAssocAhAlgo Failed\n");
        return SEC_FAILURE;
    }

    nmhSetFsipv6SecAssocAhAlgo (u4SaIndex, pIkeSa->u1HashAlgo);

    if ((pIkeSa->u1HashAlgo != SEC_MD5)
        && (pIkeSa->u1HashAlgo != SEC_NULLAHALGO))
    {

        OctetStr.pu1_OctetList = NULL;
        OctetStr.i4_Length = 0;
        OctetStr.i4_Length = pIkeSa->u2AuthKeyLen;
        OctetStr.pu1_OctetList = pIkeSa->au1AuthKey;
        if (nmhTestv2Fsipv6SecAssocAhKey (&u4SnmpErrorStatus, (INT4) u4SaIndex,
                                          &OctetStr) == SNMP_FAILURE)
        {

            nmhSetFsipv6SecAssocStatus (u4SaIndex, DESTROY);
            SECv6_TRC (SECv6_CONTROL_PLANE,
                       "Secv6InstallSAFromIke: nmhTestv2Fsipv6SecAssocAhKey Failed\n");
            return SEC_FAILURE;
        }

        nmhSetFsipv6SecAssocAhKey (u4SaIndex, &OctetStr);

    }

    if (pIkeSa->u1IPSecProtocol == SEC_ESP)
    {
        switch (pIkeSa->u1EncrAlgo)
        {
            case IKE_IPSEC_ESP_NULL:
                i4EncrAlgo = SEC_NULLESPALGO;
                break;
            case IKE_IPSEC_ESP_DES_CBC:
                i4EncrAlgo = SEC_DES_CBC;
                break;
            case IKE_IPSEC_ESP_3DES_CBC:
                i4EncrAlgo = SEC_3DES_CBC;
                break;
            case IKE_IPSEC_ESP_AES:
                i4EncrAlgo = SEC_AES;
                break;
            case IKE_IPSEC_ESP_AES_CTR:
                i4EncrAlgo = SEC_AESCTR;
                break;
            default:
                break;
        }

        if (nmhTestv2Fsipv6SecAssocEspAlgo
            (&u4SnmpErrorStatus, (INT4) u4SaIndex, i4EncrAlgo) == SNMP_FAILURE)
        {
            nmhSetFsipv6SecAssocStatus (u4SaIndex, DESTROY);
            SECv6_TRC (SECv6_CONTROL_PLANE,
                       "Secv6InstallSAFromIke: nmhTestv2Fsipv6SecAssocEspAlgo Failed\n");
            return SEC_FAILURE;
        }

        nmhSetFsipv6SecAssocEspAlgo (u4SaIndex, i4EncrAlgo);
        if (i4EncrAlgo != SEC_NULLESPALGO)
        {
            OctetStr.pu1_OctetList = NULL;
            OctetStr.i4_Length = 0;
            if ((i4EncrAlgo == SEC_DES_CBC) || (i4EncrAlgo == SEC_3DES_CBC))
            {
                OctetStr.i4_Length = SEC_ESP_DES_KEY_LEN;
                OctetStr.pu1_OctetList = pIkeSa->au1EncrKey;
            }
            else if ((i4EncrAlgo == SEC_AES) || (i4EncrAlgo == SEC_AESCTR))
            {
                OctetStr.i4_Length = pIkeSa->u2EncrKeyLen;
                OctetStr.pu1_OctetList = pIkeSa->au1EncrKey;
            }
            if (nmhTestv2Fsipv6SecAssocEspKey
                (&u4SnmpErrorStatus, (INT4) u4SaIndex,
                 &OctetStr) == SNMP_FAILURE)
            {

                SECv6_TRC (SECv6_CONTROL_PLANE,
                           "Secv6InstallSAFromIke: nmhTestv2Fsipv6SecAssocEspKey"
                           "Failed\n");
                nmhSetFsipv6SecAssocStatus (u4SaIndex, DESTROY);
                return SEC_FAILURE;
            }
            nmhSetFsipv6SecAssocEspKey (u4SaIndex, &OctetStr);

            if (i4EncrAlgo == SEC_AESCTR)
            {
                IPSEC_MEMCPY (pSecv6Assoc->au1Nonce, pIkeSa->au1Nonce, 4);
            }

            if (i4EncrAlgo == SEC_3DES_CBC)
            {
                OctetStr.pu1_OctetList = NULL;
                OctetStr.i4_Length = 0;
                if (pIkeSa->u2EncrKeyLen != (3 * SEC_ESP_DES_KEY_LEN))
                {
                    nmhSetFsipv6SecAssocStatus (u4SaIndex, DESTROY);
                    return SEC_FAILURE;
                }

                OctetStr.i4_Length = SEC_ESP_DES_KEY_LEN;
                OctetStr.pu1_OctetList = (&pIkeSa->au1EncrKey[8]);
                if (nmhTestv2Fsipv6SecAssocEspKey2
                    (&u4SnmpErrorStatus, (INT4) u4SaIndex,
                     &OctetStr) == SNMP_FAILURE)
                {

                    SECv6_TRC (SECv6_CONTROL_PLANE,
                               "Secv6InstallSAFromIke: nmhTestv2Fsipv6SecAssocEspKey2"
                               "Failed\n");
                    nmhSetFsipv6SecAssocStatus (u4SaIndex, DESTROY);
                    return SEC_FAILURE;
                }
                nmhSetFsipv6SecAssocEspKey2 (u4SaIndex, &OctetStr);
                OctetStr.pu1_OctetList = NULL;
                OctetStr.i4_Length = 0;
                OctetStr.i4_Length = SEC_ESP_DES_KEY_LEN;
                OctetStr.pu1_OctetList = (&pIkeSa->au1EncrKey[16]);
                if (nmhTestv2Fsipv6SecAssocEspKey3
                    (&u4SnmpErrorStatus, (INT4) u4SaIndex,
                     &OctetStr) == SNMP_FAILURE)
                {

                    SECv6_TRC (SECv6_CONTROL_PLANE,
                               "Secv6InstallSAFromIke: nmhTestv2Fsipv6SecAssocEspKey3"
                               "Failed\n");
                    nmhSetFsipv6SecAssocStatus (u4SaIndex, DESTROY);
                    return SEC_FAILURE;
                }
                nmhSetFsipv6SecAssocEspKey3 (u4SaIndex, &OctetStr);
            }

        }

    }

    if (pIkeSa->u4LifeTime != 0)
    {

        if (nmhTestv2Fsipv6SecAssocLifetime
            (&u4SnmpErrorStatus, (INT4) u4SaIndex,
             (INT4) pIkeSa->u4LifeTime) == SNMP_FAILURE)
        {
            nmhSetFsipv6SecAssocStatus (u4SaIndex, DESTROY);
            SECv6_TRC (SECv6_CONTROL_PLANE,
                       "Secv6InstallSAFromIke: nmhTestv2Fsipv6SecAssocLifetime Failed\n");
            return SEC_FAILURE;
        }
        nmhSetFsipv6SecAssocLifetime (u4SaIndex, pIkeSa->u4LifeTime);
    }
    if (pIkeSa->u4LifeTimeKB != 0)
    {
        u4Bytes = (SEC_BYTES_PER_KB * pIkeSa->u4LifeTimeKB);
        if (nmhTestv2Fsipv6SecAssocLifetimeInBytes
            (&u4SnmpErrorStatus, (INT4) u4SaIndex,
             (INT4) u4Bytes) == SNMP_FAILURE)
        {
            nmhSetFsipv6SecAssocStatus (u4SaIndex, DESTROY);
            SECv6_TRC (SECv6_CONTROL_PLANE,
                       "Secv6InstallSAFromIke: nmhTestv2Fsipv6SecAssocLifetimeInBytes"
                       "Failed\n");
            return SEC_FAILURE;
        }
        nmhSetFsipv6SecAssocLifetimeInBytes (u4SaIndex, u4Bytes);
    }
    Secv6UpdateSecAssocSize ();

    return SEC_SUCCESS;

}

/**************************************************************************/
/*  Function Name : Secv6RequeStIkeForNewOrReKey                          */
/*  Description   : This function is used to request IKE for new SA       */
/*                :                                                       */
/*  Input(s)      : u4IfIndex      - The Index of the Interface on which  */
/*                :                  the packet  arrived                  */
/*                : pIpAddr        - Local tunnel termination address     */
/*                : u4Protocol     - Higher Layer Protocol                */
/*                : u4Port         - Higher Layer Port Number             */
/*                : u4AccessIndex  - Index of the Access Data Base Entry  */
/*                : u4PktDir       - Direction of the Packet              */
/*                : u1NewOrReKeyFlag - Flag indicating Request for New    */
/*                :                    Key or ReKeying                    */
/*                :                                                       */
/*  Output(s)     : None                                                  */
/*  Return        : None                                                  */
/**************************************************************************/

VOID
Secv6RequeStIkeForNewOrReKey (UINT4 u4IfIndex, tIp6Addr * pIpAddr,
                              UINT4 u4Protocol, UINT4 u4Port,
                              UINT4 u4AccessIndex, UINT4 u4PktDir,
                              UINT1 u1NewOrReKeyFlag, UINT1 u1IkeVersion)
{

    tIkeQMsg           *pIkeQMsg = NULL;
    tSecv6Access       *pSecv6Access = NULL;

    SEC_UNUSED (u4IfIndex);

    if (u4PktDir == SEC_INBOUND)
    {
        return;
    }

    if (MemAllocateMemBlock (IKE_MSG_MEMPOOL_ID, (UINT1 **) (VOID *) &pIkeQMsg)
        == MEM_FAILURE)
    {
        SECv6_TRC (SECv6_CONTROL_PLANE,
                   "Secv6RequeStIkeForNewOrReKey:- Failed to allocate memblock for "
                   " tIkeQMsg \n");
        return;
    }

    if (pIkeQMsg == NULL)
    {
        SECv6_TRC (SECv6_OS_RESOURCE,
                   "Secv6RequeStIkeForNewOrReKey:- "
                   "Memory Allocation Failure\n");
        return;
    }

    IPSEC_MEMSET (pIkeQMsg, 0, sizeof (tIkeQMsg));
    pIkeQMsg->u4MsgType = IPSEC_TASK;
    if (u1NewOrReKeyFlag == SEC_NEW_KEY)
    {
        pIkeQMsg->IkeIfParam.IkeIPSecParam.u4MsgType =
            IPSEC_HTONL (IKE_IPSEC_NEW_KEY);
        pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.bRekey =
            FALSE;
    }
    else if (u1NewOrReKeyFlag == SEC_RE_KEY)
    {

        pIkeQMsg->IkeIfParam.IkeIPSecParam.u4MsgType =
            IPSEC_HTONL (IKE_IPSEC_REKEY);
        pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.bRekey = TRUE;
    }
    pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.u1Protocol =
        (UINT1) u4Protocol;
    pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.u2PortNumber =
        IPSEC_HTONS (((UINT2) u4Port));

    pSecv6Access = Secv6AccessGetEntry (u4AccessIndex);
    if (pSecv6Access == NULL)
    {
        SECv6_TRC (SEC_CNTRL_PATH,
                   "Secv6RequeStIkeForNewOrReKey: Access list not found");
        MemReleaseMemBlock (IKE_MSG_MEMPOOL_ID, (UINT1 *) pIkeQMsg);
        return;
    }

    pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.SrcNet.u4Type =
        IPSEC_HTONL (IKE_IPSEC_ID_IPV6_SUBNET);

    pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.DestNet.u4Type =
        IPSEC_HTONL (IKE_IPSEC_ID_IPV6_SUBNET);

    Ip6AddrCopy (&pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.
                 SrcNet.uNetwork.Ip6Subnet.Ip6Addr, &pSecv6Access->SrcAddress);

    Secv6UtilGetMaskFromPrefix (pSecv6Access->u4SrcAddrPrefixLen,
                                &(pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.
                                  SaTriggerInfo.SrcNet.uNetwork.Ip6Subnet.
                                  Ip6SubnetMask));

    Ip6AddrCopy (&pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.
                 DestNet.uNetwork.Ip6Subnet.Ip6Addr,
                 &pSecv6Access->DestAddress);

    Secv6UtilGetMaskFromPrefix (pSecv6Access->u4DestAddrPrefixLen,
                                &(pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.
                                  SaTriggerInfo.DestNet.uNetwork.Ip6Subnet.
                                  Ip6SubnetMask));

    pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.SrcNet.
        u2StartPort = IPSEC_HTONS (pSecv6Access->u2LocalStartPort);

    pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.SrcNet.
        u2EndPort = IPSEC_HTONS (pSecv6Access->u2LocalEndPort);

    pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.DestNet.
        u2StartPort = IPSEC_HTONS (pSecv6Access->u2RemoteStartPort);

    pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.DestNet.
        u2EndPort = IPSEC_HTONS (pSecv6Access->u2RemoteEndPort);

    Ip6AddrCopy (&pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.
                 SaTriggerInfo.LocalTEAddr.Ipv6Addr, pIpAddr);
    pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.LocalTEAddr.
        u4AddrType = IPSEC_HTONL (IKE_IPSEC_ID_IPV6_ADDR);
    pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.u1IkeVersion =
        u1IkeVersion;
    IkeHandlePktFromIpsec (pIkeQMsg);
}

/***********************************************************************/
/*  Function Name : Secv6IntimateIkeInvalidSpi                         */
/*  Description   : This function is used to Intimate IKE the invalid  */
/*                : Spi                                                */
/*                :                                                    */
/*  Input(s)      : u4IfIndex  - The Index of the Interface on which   */
/*                : the packet  arrived                                */
/*                : u1Protocol - AH or ESP                             */
/*                : SrcAddr  - Source Address of the Packet          */
/*                : DestAddr - Dest Address of the Packet            */
/*                : u4Spi      - Spi from the Pkt Rcvd                 */
/*                :                                                    */
/*  Output(s)     : None                                               */
/*  Return        : None                                               */
/***********************************************************************/

VOID
Secv6IntimateIkeInvalidSpi (UINT4 u4Spi, tIp6Addr DestAddr, tIp6Addr SrcAddr,
                            UINT1 u1Protocol)
{
    tIkeQMsg           *pIkeQMsg = NULL;

    if (MemAllocateMemBlock (IKE_MSG_MEMPOOL_ID, (UINT1 **) (VOID *) &pIkeQMsg)
        == MEM_FAILURE)
    {
        SECv6_TRC (SECv6_CONTROL_PLANE,
                   "Secv6IntimateIkeInvalidSpi:- Failed to allocate memblock for "
                   " tIkeQMsg \n");
        return;
    }
    if (pIkeQMsg == NULL)

    {
        SECv6_TRC (SECv6_OS_RESOURCE,
                   "Secv6IntimateIkeInvalidSpi:- Memory Allocation Failure\n");
        return;
    }

    IPSEC_MEMSET (pIkeQMsg, 0, sizeof (tIkeQMsg));
    pIkeQMsg->u4MsgType = IPSEC_TASK;
    pIkeQMsg->IkeIfParam.IkeIPSecParam.u4MsgType =
        IPSEC_HTONL (IKE_IPSEC_INVALID_SPI);
    pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.InvalidSpiInfo.u4Spi =
        IPSEC_HTONL (u4Spi);

    pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.InvalidSpiInfo.u1Protocol =
        u1Protocol;

    pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.InvalidSpiInfo.LocalAddr.
        u4AddrType = IPSEC_HTONL (IKE_IPSEC_ID_IPV6_ADDR);
    pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.InvalidSpiInfo.RemoteAddr.
        u4AddrType = IPSEC_HTONL (IKE_IPSEC_ID_IPV6_ADDR);

    /* Dest of the Incoming Packet is our local address */
    MEMCPY (pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.InvalidSpiInfo.
            LocalAddr.Ipv6Addr.u1_addr, &DestAddr.u1_addr, SEC_ADDR_LEN);
    MEMCPY (pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.InvalidSpiInfo.
            RemoteAddr.Ipv6Addr.u1_addr, &SrcAddr.u1_addr, SEC_ADDR_LEN);

    IkeHandlePktFromIpsec (pIkeQMsg);
}

/***********************************************************************/
/*  Function Name : Secv6IntimateIkeDeletionOfSa                       */
/*  Description   : This function is used to intimate IKE deletion of  */
/*                : SA                                                 */
/*                :                                                    */
/*  Input(s)      : pSecv6Policy - Pointer to Policy Data Base         */
/*                :                                                    */
/*  Output(s)     : Deletion of SA from SAD                            */
/*  Return        : None                                               */
/***********************************************************************/

VOID
Secv6IntimateIkeDeletionOfSa (tSecv6Policy * pSecv6Policy,
                              UINT4 u4SecAssocIndex)
{

    tIkeQMsg           *pAhIkeQMsg = NULL;
    tIkeQMsg           *pEspIkeQMsg = NULL;
    UINT4               u4IfIndex = 0;
    UINT1               u1Flag = SEC_NOT_FOUND;
    tSecv6Selector     *pSecv6Selector = NULL;
    UINT4               u4Count = 0;
    UINT4               u4SnmpErrorStatus = 0;
    UINT4               u4AHCount = 0;
    UINT4               u4ESPCount = 0;
    tSecv6Assoc        *pau1Secv6Assoc[SEC_MAX_BUNDLE_SA];
    UINT1               u1SaCount = 0;
    UINT1               u1MaxReKeySaCount = 0;
    tSNMP_OCTET_STRING_TYPE OctetStr;

    IPSEC_MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    IPSEC_MEMSET (pau1Secv6Assoc, 0, SEC_MAX_BUNDLE_SA);

    TMO_SLL_Scan (&Secv6SelList, pSecv6Selector, tSecv6Selector *)
    {
        if (pSecv6Selector->u4SelStatus == ACTIVE)
        {
            if (pSecv6Selector->pPolicyEntry->u4PolicyIndex ==
                pSecv6Policy->u4PolicyIndex)
            {
                if (pSecv6Policy->u4PolicyStatus == ACTIVE)
                {
                    u1Flag = SEC_FOUND;
                    break;
                }
            }
        }

    }

    if (u1Flag == SEC_NOT_FOUND)
    {
        return;
    }

    u4IfIndex = pSecv6Selector->u4IfIndex;
    u1Flag = SEC_NOT_FOUND;

    if (pSecv6Policy->paSaEntry[0] != NULL)
    {
        for (u4Count = 0; u4Count < pSecv6Policy->u1SaCount; u4Count++)
        {
            if (pSecv6Policy->paSaEntry[u4Count]->u4SecAssocIndex ==
                u4SecAssocIndex)
            {
                IPSEC_MEMCPY (pau1Secv6Assoc, pSecv6Policy->paSaEntry,
                              SEC_MAX_BUNDLE_SA);
                u1SaCount = pSecv6Policy->u1SaCount;
                u1Flag = SEC_FOUND;
                break;
            }
        }
    }
    if (u1Flag == SEC_NOT_FOUND)
    {
        for (u1MaxReKeySaCount = 0; u1MaxReKeySaCount < SEC_MAX_SA_TO_POLICY;
             u1MaxReKeySaCount++)
        {
            if ((pSecv6Policy->pau1NewSaEntry[u1MaxReKeySaCount][0]) != NULL)
            {
                for (u4Count = 0;
                     u4Count < pSecv6Policy->au1NewSaCount[u1MaxReKeySaCount];
                     u4Count++)
                {
                    if (pSecv6Policy->
                        pau1NewSaEntry[u1MaxReKeySaCount][u4Count]->
                        u4SecAssocIndex == u4SecAssocIndex)
                    {
                        IPSEC_MEMCPY (pau1Secv6Assoc, &(pSecv6Policy->
                                                        pau1NewSaEntry
                                                        [u1MaxReKeySaCount][0]),
                                      SEC_MAX_BUNDLE_SA);
                        u1SaCount =
                            pSecv6Policy->au1NewSaCount[u1MaxReKeySaCount];
                        u1Flag = SEC_FOUND;
                        break;
                    }
                }
            }
            if (u1Flag == SEC_FOUND)
            {
                break;
            }
        }
    }

    if (u1Flag == SEC_NOT_FOUND)
    {
        return;
    }

    for (u4Count = 0; u4Count < u1SaCount; u4Count++)
    {

        if (pau1Secv6Assoc[u4Count]->u1SecAssocProtocol == SEC_AH)
        {

            if (pSecv6Selector->u4PktDirection == SEC_INBOUND)
            {

                if (pAhIkeQMsg == NULL)
                {
                    if (MemAllocateMemBlock
                        (IKE_MSG_MEMPOOL_ID,
                         (UINT1 **) (VOID *) &pAhIkeQMsg) == MEM_FAILURE)
                    {
                        SECv6_TRC (SECv6_CONTROL_PLANE,
                                   "Secv6IntimateIkeDeletionOfSa:- Failed to allocate memblock for "
                                   "tIkeQMsg \n");
                        MemReleaseMemBlock (IKE_MSG_MEMPOOL_ID,
                                            (UINT1 *) pEspIkeQMsg);
                        return;
                    }

                    if (pAhIkeQMsg == NULL)
                    {
                        SECv6_TRC (SECv6_OS_RESOURCE,
                                   "Secv6IntimateIkeDeletionOfSa:"
                                   "Memory Allocation Failure\n");
                        if (pEspIkeQMsg != NULL)
                        {
                            MemReleaseMemBlock (IKE_MSG_MEMPOOL_ID,
                                                (UINT1 *) pEspIkeQMsg);
                        }

                        return;
                    }
                    IPSEC_MEMSET (pAhIkeQMsg, 0, sizeof (tIkeQMsg));
                    pAhIkeQMsg->u4MsgType = IPSEC_TASK;
                    pAhIkeQMsg->IkeIfParam.IkeIPSecParam.u4MsgType =
                        IPSEC_HTONL (IKE_IPSEC_SEND_DELETE_SA);
                    pAhIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.DelSpiInfo.
                        LocalAddr.u4AddrType =
                        IPSEC_HTONL (IKE_IPSEC_ID_IPV6_ADDR);
                    pAhIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.DelSpiInfo.
                        RemoteAddr.u4AddrType =
                        IPSEC_HTONL (IKE_IPSEC_ID_IPV6_ADDR);
                    MEMCPY (pAhIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.
                            DelSpiInfo.LocalAddr.Ipv6Addr.u1_addr,
                            pau1Secv6Assoc[u4Count]->SecAssocDestAddr.u1_addr,
                            SEC_ADDR_LEN);

                    MEMCPY (pAhIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.
                            DelSpiInfo.RemoteAddr.Ipv6Addr.u1_addr,
                            pau1Secv6Assoc[u4Count]->SecAssocSrcAddr.
                            u1_addr, SEC_ADDR_LEN);

                    pAhIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.DelSpiInfo.
                        u1Protocol = SEC_AH;

                }

                pAhIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.DelSpiInfo.
                    u2NumSPI = IPSEC_HTONS (((UINT2) (u4AHCount + 1)));

                pAhIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.DelSpiInfo.
                    au4SPI[u4AHCount] =
                    IPSEC_HTONL (pau1Secv6Assoc[u4Count]->u4SecAssocSpi);
            }

            if (nmhTestv2Fsipv6SecAssocStatus (&u4SnmpErrorStatus,
                                               (INT4) pau1Secv6Assoc[u4Count]->
                                               u4SecAssocIndex,
                                               DESTROY) == SNMP_FAILURE)
            {
                SECv6_TRC (SECv6_OS_RESOURCE,
                           "Secv6IntimateIkeDeletionOfSa:"
                           "nmhTestv2Fsipv6SecAssocStatus Failed\n");
                MemReleaseMemBlock (IKE_MSG_MEMPOOL_ID, (UINT1 *) pAhIkeQMsg);
                if (pEspIkeQMsg != NULL)
                {
                    MemReleaseMemBlock (IKE_MSG_MEMPOOL_ID,
                                        (UINT1 *) pEspIkeQMsg);
                }

                return;
            }
            nmhSetFsipv6SecAssocStatus (pau1Secv6Assoc[u4Count]->
                                        u4SecAssocIndex, DESTROY);
            u4AHCount++;

        }
        else if (pau1Secv6Assoc[u4Count]->u1SecAssocProtocol == SEC_ESP)
        {

            if (pSecv6Selector->u4PktDirection == SEC_INBOUND)
            {
                if (pEspIkeQMsg == NULL)
                {
                    if (MemAllocateMemBlock
                        (IKE_MSG_MEMPOOL_ID,
                         (UINT1 **) (VOID *) &pEspIkeQMsg) == MEM_FAILURE)
                    {
                        SECv6_TRC (SECv6_CONTROL_PLANE,
                                   "Secv6IntimateIkeDeletionOfSa:- Failed to allocate memblock for "
                                   " tIkeQMsg \n");
                        MemReleaseMemBlock (IKE_MSG_MEMPOOL_ID,
                                            (UINT1 *) pAhIkeQMsg);
                        if (pEspIkeQMsg != NULL)
                        {
                            MemReleaseMemBlock (IKE_MSG_MEMPOOL_ID,
                                                (UINT1 *) pEspIkeQMsg);
                        }
                        return;
                    }

                    if (pEspIkeQMsg == NULL)
                    {
                        SECv6_TRC (SECv6_OS_RESOURCE,
                                   "Secv6IntimateIkeDeletionOfSa:"
                                   "Memory Allocation Failure\n");
                        MemReleaseMemBlock (IKE_MSG_MEMPOOL_ID,
                                            (UINT1 *) pAhIkeQMsg);
                        if (pEspIkeQMsg != NULL)
                        {
                            MemReleaseMemBlock (IKE_MSG_MEMPOOL_ID,
                                                (UINT1 *) pEspIkeQMsg);
                        }
                        return;
                    }

                    IPSEC_MEMSET (pEspIkeQMsg, 0, sizeof (tIkeQMsg));
                    pEspIkeQMsg->u4MsgType = IPSEC_TASK;
                    pEspIkeQMsg->IkeIfParam.IkeIPSecParam.u4MsgType =
                        IPSEC_HTONL (IKE_IPSEC_SEND_DELETE_SA);

                    pEspIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.DelSpiInfo.
                        LocalAddr.u4AddrType =
                        IPSEC_HTONL (IKE_IPSEC_ID_IPV6_ADDR);
                    pEspIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.DelSpiInfo.
                        RemoteAddr.u4AddrType =
                        IPSEC_HTONL (IKE_IPSEC_ID_IPV6_ADDR);
                    MEMCPY (pEspIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.
                            DelSpiInfo.LocalAddr.Ipv6Addr.u1_addr,
                            pau1Secv6Assoc[u4Count]->
                            SecAssocDestAddr.u1_addr, SEC_ADDR_LEN);
                    MEMCPY (pEspIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.
                            DelSpiInfo.RemoteAddr.Ipv6Addr.u1_addr,
                            pau1Secv6Assoc[u4Count]->SecAssocSrcAddr.
                            u1_addr, SEC_ADDR_LEN);

                    pEspIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.DelSpiInfo.
                        u1Protocol = SEC_ESP;
                }

                pEspIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.DelSpiInfo.
                    u2NumSPI = IPSEC_HTONS (((UINT2) (u4ESPCount + 1)));
                pEspIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.DelSpiInfo.
                    au4SPI[u4ESPCount] =
                    IPSEC_HTONL (pau1Secv6Assoc[u4Count]->u4SecAssocSpi);

            }

            if (nmhTestv2Fsipv6SecAssocStatus (&u4SnmpErrorStatus,
                                               (INT4) pau1Secv6Assoc[u4Count]->
                                               u4SecAssocIndex,
                                               DESTROY) == SNMP_FAILURE)
            {
                SECv6_TRC (SECv6_OS_RESOURCE,
                           "Secv6IntimateIkeDeletionOfSa:"
                           "nmhTestv2Fsipv6SecAssocStatus Failed\n");
                MemReleaseMemBlock (IKE_MSG_MEMPOOL_ID, (UINT1 *) pEspIkeQMsg);
                MemReleaseMemBlock (IKE_MSG_MEMPOOL_ID, (UINT1 *) pAhIkeQMsg);
                return;
            }
            nmhSetFsipv6SecAssocStatus (pau1Secv6Assoc[u4Count]->
                                        u4SecAssocIndex, DESTROY);
            u4ESPCount++;
        }

    }

    if (pAhIkeQMsg != NULL)
    {
        pAhIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.DelSpiInfo.
            u1IkeVersion = pSecv6Policy->u1IkeVersion;
        IkeHandlePktFromIpsec (pAhIkeQMsg);
    }

    if (pEspIkeQMsg != NULL)
    {
        pEspIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.DelSpiInfo.
            u1IkeVersion = pSecv6Policy->u1IkeVersion;
        IkeHandlePktFromIpsec (pEspIkeQMsg);
    }
    UNUSED_PARAM (u4IfIndex);
}

/***********************************************************************/
/*  Function Name : Secv6DeleteSa                                      */
/*  Description   : This function gets the Policy Entry based on the   */
/*                : SecAssocIndex and deletes all SA Matched to policy */
/*                :                                                    */
/*  Input(s)      : u4SecAssocIndex - Index of SAD                     */
/*                :                                                    */
/*  Output(s)     :                                                    */
/*  Return        :                                                    */
/***********************************************************************/

VOID
Secv6DeleteSa (UINT4 u4SecAssocIndex)
{

    tSecv6Policy       *pEntry = NULL;
    UINT1               u1Flag = SEC_NOT_FOUND;

    TMO_SLL_Scan (&Secv6PolicyList, pEntry, tSecv6Policy *)
    {
        if (pEntry->paSaEntry[0]->u4SecAssocIndex == u4SecAssocIndex)
        {
            u1Flag = SEC_FOUND;
            break;
        }

    }
    if (u1Flag == SEC_FOUND)
    {
        Secv6IntimateIkeDeletionOfSa (pEntry, u4SecAssocIndex);
    }

}

/***********************************************************************/
/*  Function Name : Secv6ConvertIkeInstallSAMsgToHostOrder             */
/*  Description   : This function converts the rcvd message to host    */
/*                : order                                              */
/*                :                                                    */
/*  Input(s)      :pInstallSAMsg :- Pointer to IPSec Bundle            */
/*                :                                                    */
/*  Output(s)     :                                                    */
/*  Return        :                                                    */
/***********************************************************************/

VOID
Secv6ConvertIkeInstallSAMsgToHostOrder (tIPSecBundle * pInstallSAMsg)
{

    if (pInstallSAMsg->aInSABundle[0].u4Spi != 0)
    {
        Secv6ConvertIkeIPSecSaToHostOrder (&pInstallSAMsg->aInSABundle[0]);
    }

    if (pInstallSAMsg->aInSABundle[1].u4Spi != 0)
    {
        Secv6ConvertIkeIPSecSaToHostOrder (&pInstallSAMsg->aInSABundle[1]);
    }

    if (pInstallSAMsg->aOutSABundle[0].u4Spi != 0)
    {
        Secv6ConvertIkeIPSecSaToHostOrder (&pInstallSAMsg->aOutSABundle[0]);
    }

    if (pInstallSAMsg->aOutSABundle[1].u4Spi != 0)
    {
        Secv6ConvertIkeIPSecSaToHostOrder (&pInstallSAMsg->aOutSABundle[1]);
    }

}

/***********************************************************************/
/*  Function Name : Secv6ConvertIkeIPSecSaToHostOrder                  */
/*  Description   : This function converts the IPSec Sa to Host Order  */
/*                :                                                    */
/*  Input(s)      :pInstallSAMsg :- Pointer to IPSec Bundle            */
/*                :                                                    */
/*  Output(s)     :                                                    */
/*  Return        :                                                    */
/***********************************************************************/

VOID
Secv6ConvertIkeIPSecSaToHostOrder (tIPSecSA * pIkeIPSecSa)
{

    pIkeIPSecSa->u4Spi = IPSEC_NTOHL (pIkeIPSecSa->u4Spi);

    pIkeIPSecSa->u2HLProtocol = IPSEC_NTOHS (pIkeIPSecSa->u2HLProtocol);

    pIkeIPSecSa->u2HLPortNumber = IPSEC_NTOHS (pIkeIPSecSa->u2HLPortNumber);

    pIkeIPSecSa->u4LifeTime = IPSEC_NTOHL (pIkeIPSecSa->u4LifeTime);

    pIkeIPSecSa->u4LifeTimeKB = IPSEC_NTOHL (pIkeIPSecSa->u4LifeTimeKB);

    pIkeIPSecSa->u2AuthKeyLen = IPSEC_NTOHS (pIkeIPSecSa->u2AuthKeyLen);

    pIkeIPSecSa->u2EncrKeyLen = IPSEC_NTOHS (pIkeIPSecSa->u2EncrKeyLen);
}
