@ECHO OFF
CLS
ECHO .
ECHO ************************************************
ECHO **  Building Nucleus PLUS Demo
ECHO ************************************************
ECHO .
@ECHO ON

set demo=%1
set endian=b
set genelal_option=LR_INTEGRATION,SVID_SOURCE,TRACE_WANTED,INCLUDE_IN_OSS,GNU_CC,PACK_REQUIRED
set system_include=..\inc,..\inc\vlan,..\inc\snmp,..\inc,D:\user\Epilogue\Nucleus\NuPlus,..\fsap2\cmn,..\fsap2\nucleus,../util/ip6,../util,../util/md5/inc,../util/sha1,../util/aes,../util/des,../cli/inc,../inc/openssl
set system_option=__STDC__,IPSECv6_WANTED,FUTURE_SNMP_WANTED,SNMP_WANTED,SLI_WANTED

if "%1"=="" set demo=demo
if "%1"=="f" set demo=flash
if "%1"=="F" set demo=flash
if "%1"==""  set output=-ou=o\plusdemo.abs
if "%1"=="f" set output=-ou=o\flash.mot -fo=stype
if "%1"=="F" set output=-ou=o\flash.mot -fo=stype
@ECHO OFF

IF NOT EXIST "o\*.*" MKDIR "o"
CD ".\"
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%genelal_option%,%system_option% -I=%system_include% -ob=o\secv6ah.o secv6ah.c
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%genelal_option%,%system_option% -I=%system_include% -ob=o\secv6cli.o secv6cli.c
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%genelal_option%,%system_option% -I=%system_include% -ob=o\secv6conf.o secv6conf.c
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%genelal_option%,%system_option% -I=%system_include% -ob=o\secv6esp.o secv6esp.c
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%genelal_option%,%system_option% -I=%system_include% -ob=o\secv6init.o secv6init.c
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%genelal_option%,%system_option% -I=%system_include% -ob=o\secv6io.o secv6io.c
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%genelal_option%,%system_option% -I=%system_include% -ob=o\secv6low.o secv6low.c
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%genelal_option%,%system_option% -I=%system_include% -ob=o\secv6main.o secv6main.c
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%genelal_option%,%system_option% -I=%system_include% -ob=o\secv6stat.o secv6stat.c
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%genelal_option%,%system_option% -I=%system_include% -ob=o\secv6util.o secv6util.c
SHC -c=m -cp=sh4 -op=0 -deb -en=%endian% -Define=%genelal_option%,%system_option% -I=%system_include% -ob=o\secv6mid.o secv6mid.c
@ECHO ON

IF EXIST "ipsec.lib" DEL "ipsec.lib"
ECHO Creating Library ipsec.lib...
OPTLNK o\*.o -OU=ipsec.lib -FO=library

